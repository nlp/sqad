# Simple Question Answering Database (SQAD)
SQAD is a Czech database for question answering developed in **NLP laboratory at Masaryk University**. Database have been harvested from **Czech Wikipedia** articles by students and annotated with appropriate question, answer sentence, exact answer, question type and answer type. Part of speech tagging and lemmatization by [Majka](https://nlp.fi.muni.cz/projekty/ajka/) pipeline.

Each record consist of several files:
* `01question.vert`: contains a question in vertical format
* `03text.vert`: contains full text of Wikipedia article in vertical format. Or can be represented by symbolic link to another record that also uses the same text. This feature is designed for	saving the disk space.
* `04url.txt`: stores original URL address
* `05metadata.txt`: consists of several XML like lines that encodes important metadata
	* `<q_type>`: question type
    * `<a_type>`: answer type
    * `<user>`: name of the user that creates the record
* `06answer.selection.vert`: contains an sentence from the document that can answer the input question in vertical format
* `09answer_extraction.vert`: includes a sub-string from answer selection sentence in vertical format. This sub-string is informative enough to answer the input question.
* `10title.vert`: contains the title of the article itself in vertical format.

SQAD have been developed as a part of dissertation and is used in Automatic Question Answering (AQA) system. 

## SQAD to databse submodule cloning:
```
git submodule init
git submodule update
```

Please cite:
* MEDVEĎ, Marek and Aleš HORÁK. Sentence and Word Embedding Employed in Open Question-Answering. In Proceedings of the 10th International Conference on Agents and Artificial Intelligence (ICAART 2018). Setúbal, Portugal: SCITEPRESS - Science and Technology Publications, 2018. p. 486-492. ISBN 978-989-758-275-2.
```
@inproceedings{1393609,
   author = {Medveď, Marek and Horák, Aleš},
   address = {Setúbal, Portugal},
   booktitle = {Proceedings of the 10th International Conference on Agents and Artificial Intelligence (ICAART 2018)},
   keywords = {question answering; word embedding; word2vec; AQA; Simple Question Answering Database; SQAD},
   howpublished = {print},
   language = {eng},
   location = {Setúbal, Portugal},
   isbn = {978-989-758-275-2},
   pages = {486-492},
   publisher = {SCITEPRESS - Science and Technology Publications},
   title = {Sentence and Word Embedding Employed in Open Question-Answering},
   year = {2018}
}
```

* Marek Medveď, Radoslav Sabol, and Aleš Horák. Czech Question Answering with Extended SQAD v3.0 Benchmark Dataset. In Horák, Aleš and Rychlý, Pavel and Rambousek, Adam. Proceedings of the Thirteenth Workshop on Recent Advances in Slavonic Natural Languages Processing, RASLAN 2019. Brno: Tribun EU, 2019. p. 99-108. ISBN 978-80-263-1530-8.
```
@inproceedings{1591218,
   author = {Sabol, Radoslav and Medveď, Marek and Horák, Aleš},
   address = {Brno},
   booktitle = {Proceedings of the Thirteenth Workshop on Recent Advances in Slavonic Natural Languages Processing, RASLAN 2019},
   editor = {Horák, Aleš and Rychlý, Pavel and Rambousek, Adam},
   keywords = {question answering; QA benchmark dataset; SQAD; Czech},
   howpublished = {tištěná verze "print"},
   language = {eng},
   location = {Brno},
   isbn = {978-80-263-1530-8},
   pages = {99-108},
   publisher = {Tribun EU},
   title = {Czech Question Answering with Extended SQAD v3.0 Benchmark Dataset},
   year = {2019}
}
```

[License](https://nlp.fi.muni.cz/en/LicenceWebCorpus)

[Publications](https://is.muni.cz/person/359226#publikace)

[Project web](https://nlp.fi.muni.cz/projekty/sqad/)
