VRT=$(wildcard data/*/*.vert)
generate_txt: $(VRT:%.vert=%.txt)
%.txt: %.vert
	cat $< | vert2plain -l 10000000 > $@
	
split_q_type_even_distribution:
	./scripts/split_sqad_q_type_even_distribution.py -s "data" -t "$@/60_30_10/" -d 60/30/10

organize:
	python3 ./scripts/organize_sqad.py -s 1 -e 16036

URL=$(wildcard data/*/04url.txt)
do_title: $(URL:%04url.txt=%10title.vert)

%10title.vert: %04url.txt
	 cat $^ | ./scripts/get_title.py | /opt/majka_pipe/majka-czech_v2.sh | awk 'BEGIN {FS="\t"; OFS="\t"} {print $$1, $$3, $$2, $$4}' | python3 ./scripts/remove_lempos.py > $@

stats.txt:
	./scripts/metadata_stat.py -d ./data > $@

as_ae_problems.txt:
	scripts/check_as_in_text_ae_in_as.py -d ./data/ > $@

duplicities.txt:
	./scripts/check_duplicities.py -d ./data/ > $@

json:
	./scripts/sqad2json_one_question_per_doc.py -d ./data -o ./data_json -v 1.0 -p split_q_type_even_distribution/60_30_10/parts.dump --merge_train_eval
