<p>
<s>
Boeing	boeing	k1gInSc1	boeing
(	(	kIx(	(
<g/>
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
označením	označení	k1gNnSc7	označení
The	The	k1gFnSc2	The
Boeing	boeing	k1gInSc4	boeing
Company	Compana	k1gFnSc2	Compana
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
NYSE	NYSE	kA	NYSE
<g/>
:	:	kIx,	:
BA	ba	k9	ba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
výrobců	výrobce	k1gMnPc2	výrobce
letecké	letecký	k2eAgFnSc2d1	letecká
techniky	technika	k1gFnSc2	technika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
též	též	k6eAd1	též
největším	veliký	k2eAgMnSc7d3	veliký
americkým	americký	k2eAgMnSc7d1	americký
exportérem	exportér	k1gMnSc7	exportér
<g/>
.	.	kIx.	.
</s>
<s>
Operuje	operovat	k5eAaImIp3nS	operovat
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sídlo	sídlo	k1gNnSc1	sídlo
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výnosy	výnos	k1gInPc7	výnos
kolem	kolem	k7c2	kolem
30	[number]	k4	30
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
zbraní	zbraň	k1gFnPc2	zbraň
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
nadnárodní	nadnárodní	k2eAgFnSc1d1	nadnárodní
společnost	společnost	k1gFnSc1	společnost
třetí	třetí	k4xOgNnSc4	třetí
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
zbrojních	zbrojní	k2eAgMnPc2d1	zbrojní
dodavatelů	dodavatel	k1gMnPc2	dodavatel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
ale	ale	k9	ale
zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
technika	technika	k1gFnSc1	technika
Boeingu	boeing	k1gInSc2	boeing
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
zhruba	zhruba	k6eAd1	zhruba
polovinu	polovina	k1gFnSc4	polovina
příjmů	příjem	k1gInPc2	příjem
(	(	kIx(	(
<g/>
48	[number]	k4	48
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgInS	být
Boeing	boeing	k1gInSc1	boeing
sloučen	sloučen	k2eAgInSc1d1	sloučen
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
výrobcem	výrobce	k1gMnSc7	výrobce
letadel	letadlo	k1gNnPc2	letadlo
McDonnell	McDonnell	k1gMnSc1	McDonnell
Douglas	Douglas	k1gMnSc1	Douglas
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
s	s	k7c7	s
evropským	evropský	k2eAgInSc7d1	evropský
Airbusem	airbus	k1gInSc7	airbus
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
Boeingem	boeing	k1gInSc7	boeing
a	a	k8xC	a
Airbusem	airbus	k1gInSc7	airbus
je	být	k5eAaImIp3nS	být
především	především	k9	především
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
na	na	k7c4	na
vztah	vztah	k1gInSc4	vztah
pilot-počítač	pilotočítač	k1gMnSc1	pilot-počítač
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
řízení	řízení	k1gNnSc1	řízení
letadla	letadlo	k1gNnSc2	letadlo
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
piloty	pilota	k1gFnPc4	pilota
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Civilní	civilní	k2eAgNnPc1d1	civilní
letadla	letadlo	k1gNnPc1	letadlo
==	==	k?	==
</s>
</p>
<p>
<s>
Boeing	boeing	k1gInSc4	boeing
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
dopravních	dopravní	k2eAgInPc2d1	dopravní
letounů	letoun	k1gInPc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgMnSc1d3	Nejnovější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
verze	verze	k1gFnSc1	verze
-8	-8	k4	-8
<g/>
,	,	kIx,	,
Boeing	boeing	k1gInSc4	boeing
787	[number]	k4	787
a	a	k8xC	a
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
MAX	max	kA	max
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
chystá	chystat	k5eAaImIp3nS	chystat
novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
Boeingu	boeing	k1gInSc2	boeing
777	[number]	k4	777
<g/>
,	,	kIx,	,
Boeing	boeing	k1gInSc4	boeing
777	[number]	k4	777
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
Boeing	boeing	k1gInSc4	boeing
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nejoblíbenější	oblíbený	k2eAgNnSc1d3	nejoblíbenější
letadlo	letadlo	k1gNnSc1	letadlo
světa	svět	k1gInSc2	svět
–	–	k?	–
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
verzích	verze	k1gFnPc6	verze
ho	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
za	za	k7c2	za
50	[number]	k4	50
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
přes	přes	k7c4	přes
9	[number]	k4	9
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uživatelé	uživatel	k1gMnPc1	uživatel
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2019	[number]	k4	2019
provozuje	provozovat	k5eAaImIp3nS	provozovat
letouny	letoun	k1gInPc4	letoun
Boeing	boeing	k1gInSc4	boeing
společnost	společnost	k1gFnSc1	společnost
–	–	k?	–
SmartWings	SmartWings	k1gInSc1	SmartWings
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
typ	typ	k1gInSc4	typ
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
a	a	k8xC	a
737	[number]	k4	737
MAX	max	kA	max
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
businessjet	businessjet	k1gInSc4	businessjet
verzi	verze	k1gFnSc4	verze
vlastní	vlastní	k2eAgInSc1d1	vlastní
Boeing	boeing	k1gInSc1	boeing
737-700	[number]	k4	737-700
také	také	k9	také
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
Boeingy	boeing	k1gInPc1	boeing
737	[number]	k4	737
provozovaly	provozovat	k5eAaImAgFnP	provozovat
například	například	k6eAd1	například
České	český	k2eAgFnPc1d1	Česká
aerolinie	aerolinie	k1gFnPc1	aerolinie
či	či	k8xC	či
Czech	Czech	k1gMnSc1	Czech
Connect	Connect	k1gMnSc1	Connect
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
B	B	kA	B
<g/>
&	&	k?	&
<g/>
W	W	kA	W
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Williamem	William	k1gInSc7	William
Edwardem	Edward	k1gMnSc7	Edward
Boeingem	boeing	k1gInSc7	boeing
a	a	k8xC	a
fregatním	fregatní	k2eAgMnSc7d1	fregatní
kapitánem	kapitán	k1gMnSc7	kapitán
G.	G.	kA	G.
C.	C.	kA	C.
Westerveltem	Westervelt	k1gInSc7	Westervelt
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
byla	být	k5eAaImAgNnP	být
přejmenována	přejmenovat	k5eAaPmNgNnP	přejmenovat
na	na	k7c4	na
Pacific	Pacifice	k1gFnPc2	Pacifice
Aero	aero	k1gNnSc1	aero
Products	Products	k1gInSc4	Products
Company	Compana	k1gFnSc2	Compana
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
Boeing	boeing	k1gInSc4	boeing
Airplane	Airplan	k1gMnSc5	Airplan
Company	Compan	k1gMnPc4	Compan
si	se	k3xPyFc3	se
osvojila	osvojit	k5eAaPmAgFnS	osvojit
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k6eAd1	také
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgFnSc4	první
významnou	významný	k2eAgFnSc4d1	významná
zakázku	zakázka	k1gFnSc4	zakázka
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boeing	boeing	k1gInSc1	boeing
se	se	k3xPyFc4	se
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
doprovodných	doprovodný	k2eAgFnPc2d1	doprovodná
aktivit	aktivita	k1gFnPc2	aktivita
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
osídlení	osídlení	k1gNnSc4	osídlení
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
firma	firma	k1gFnSc1	firma
oslavila	oslavit	k5eAaPmAgFnS	oslavit
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boeing	boeing	k1gInSc4	boeing
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Fotostory	Fotostor	k1gInPc1	Fotostor
<g/>
:	:	kIx,	:
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
Boeing	boeing	k1gInSc1	boeing
<g/>
.	.	kIx.	.
</s>
<s>
Kupovali	kupovat	k5eAaImAgMnP	kupovat
jsme	být	k5eAaImIp1nP	být
letadlo	letadlo	k1gNnSc4	letadlo
za	za	k7c4	za
750	[number]	k4	750
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
</s>
</p>
