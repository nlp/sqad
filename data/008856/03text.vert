<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Slovenska	Slovensko	k1gNnSc2	Slovensko
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
trikolóry	trikolóra	k1gFnSc2	trikolóra
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
slovenské	slovenský	k2eAgFnSc2d1	slovenská
trikolóry	trikolóra	k1gFnSc2	trikolóra
jsou	být	k5eAaImIp3nP	být
odvozené	odvozený	k2eAgInPc1d1	odvozený
jednak	jednak	k8xC	jednak
od	od	k7c2	od
původních	původní	k2eAgFnPc2d1	původní
slovenských	slovenský	k2eAgFnPc2d1	slovenská
barev	barva	k1gFnPc2	barva
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
jednak	jednak	k8xC	jednak
od	od	k7c2	od
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
trikolóry	trikolóra	k1gFnSc2	trikolóra
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
popisuje	popisovat	k5eAaImIp3nS	popisovat
§	§	k?	§
<g/>
9	[number]	k4	9
odstavec	odstavec	k1gInSc1	odstavec
2	[number]	k4	2
Ústavy	ústava	k1gFnSc2	ústava
SR	SR	kA	SR
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Zákon	zákon	k1gInSc1	zákon
NR	NR	kA	NR
SR	SR	kA	SR
č.	č.	k?	č.
63	[number]	k4	63
<g/>
/	/	kIx~	/
<g/>
193	[number]	k4	193
Z.	Z.	kA	Z.
z.	z.	k?	z.
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
poloha	poloha	k1gFnSc1	poloha
štítu	štít	k1gInSc2	štít
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
délky	délka	k1gFnSc2	délka
vlajky	vlajka	k1gFnSc2	vlajka
od	od	k7c2	od
stožáru	stožár	k1gInSc2	stožár
</s>
</p>
<p>
<s>
výška	výška	k1gFnSc1	výška
štítu	štít	k1gInSc2	štít
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
<g/>
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
</s>
</p>
<p>
<s>
šířka	šířka	k1gFnSc1	šířka
lemu	lem	k1gInSc2	lem
štítu	štít	k1gInSc2	štít
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
<g/>
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
délky	délka	k1gFnSc2	délka
vlajky	vlajka	k1gFnSc2	vlajka
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
Československa	Československo	k1gNnSc2	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
během	během	k7c2	během
revoluce	revoluce	k1gFnSc2	revoluce
1848	[number]	k4	1848
<g/>
/	/	kIx~	/
<g/>
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
když	když	k8xS	když
Slováci	Slovák	k1gMnPc1	Slovák
bojovali	bojovat	k5eAaImAgMnP	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Vídně	Vídeň	k1gFnSc2	Vídeň
proti	proti	k7c3	proti
Maďarům	Maďar	k1gMnPc3	Maďar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dubnem	duben	k1gInSc7	duben
a	a	k8xC	a
zářím	září	k1gNnSc7	září
1848	[number]	k4	1848
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
používala	používat	k5eAaImAgFnS	používat
(	(	kIx(	(
<g/>
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
polské	polský	k2eAgFnSc2d1	polská
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
)	)	kIx)	)
červeno-bílá	červenoílý	k2eAgFnSc1d1	červeno-bílá
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
úplně	úplně	k6eAd1	úplně
poprvé	poprvé	k6eAd1	poprvé
"	"	kIx"	"
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
takováto	takovýto	k3xDgFnSc1	takovýto
vlajka	vlajka	k1gFnSc1	vlajka
údajně	údajně	k6eAd1	údajně
použila	použít	k5eAaPmAgFnS	použít
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1848	[number]	k4	1848
v	v	k7c6	v
Brezové	Brezová	k1gFnSc6	Brezová
během	během	k7c2	během
divadelního	divadelní	k2eAgNnSc2d1	divadelní
představení	představení	k1gNnSc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1848	[number]	k4	1848
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgInS	přidat
modrý	modrý	k2eAgInSc1d1	modrý
pás	pás	k1gInSc1	pás
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
nejčastěji	často	k6eAd3	často
vlajka	vlajka	k1gFnSc1	vlajka
červeno-bílo-modrá	červenoíloodrý	k2eAgFnSc1d1	červeno-bílo-modrá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bílo-modro-červená	bíloodro-červený	k2eAgFnSc1d1	bílo-modro-červená
<g/>
.	.	kIx.	.
</s>
<s>
Modrý	modrý	k2eAgInSc1d1	modrý
pás	pás	k1gInSc1	pás
byl	být	k5eAaImAgInS	být
převzatý	převzatý	k2eAgInSc1d1	převzatý
z	z	k7c2	z
ruské	ruský	k2eAgFnSc2d1	ruská
a	a	k8xC	a
chorvatské	chorvatský	k2eAgFnSc2d1	chorvatská
zástavy	zástava	k1gFnSc2	zástava
(	(	kIx(	(
<g/>
Rusové	Rus	k1gMnPc1	Rus
jako	jako	k8xC	jako
patroni	patron	k1gMnPc1	patron
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
,	,	kIx,	,
Chorvati	Chorvat	k1gMnPc1	Chorvat
jako	jako	k8xS	jako
bratrský	bratrský	k2eAgInSc1d1	bratrský
národ	národ	k1gInSc1	národ
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
vlajek	vlajka	k1gFnPc2	vlajka
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
nesly	nést	k5eAaImAgFnP	nést
také	také	k9	také
uherský	uherský	k2eAgInSc4d1	uherský
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
však	však	k8xC	však
často	často	k6eAd1	často
byla	být	k5eAaImAgFnS	být
změněná	změněný	k2eAgFnSc1d1	změněná
barva	barva	k1gFnSc1	barva
třech	tři	k4xCgInPc2	tři
vršků	vršek	k1gInPc2	vršek
na	na	k7c4	na
modrou	modrý	k2eAgFnSc4d1	modrá
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
na	na	k7c4	na
dnešní	dnešní	k2eAgFnPc4d1	dnešní
barvy	barva	k1gFnPc4	barva
slovenského	slovenský	k2eAgInSc2d1	slovenský
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použitím	použití	k1gNnSc7	použití
uherského	uherský	k2eAgInSc2d1	uherský
znaku	znak	k1gInSc2	znak
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Slováci	Slovák	k1gMnPc1	Slovák
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
<g/>
)	)	kIx)	)
nechtějí	chtít	k5eNaImIp3nP	chtít
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
od	od	k7c2	od
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chtějí	chtít	k5eAaImIp3nP	chtít
uznání	uznání	k1gNnSc4	uznání
svého	svůj	k3xOyFgInSc2	svůj
národa	národ	k1gInSc2	národ
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Uher	uher	k1gInSc1	uher
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
pruhů	pruh	k1gInPc2	pruh
se	se	k3xPyFc4	se
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
na	na	k7c6	na
dnešním	dnešní	k2eAgInSc6d1	dnešní
stavu	stav	k1gInSc6	stav
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
používaly	používat	k5eAaImAgInP	používat
slovenské	slovenský	k2eAgInPc1d1	slovenský
spolky	spolek	k1gInPc1	spolek
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
Nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
Československo	Československo	k1gNnSc1	Československo
si	se	k3xPyFc3	se
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
uvažování	uvažování	k1gNnSc6	uvažování
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
tradiční	tradiční	k2eAgFnSc2d1	tradiční
české	český	k2eAgFnSc2d1	Česká
bílo-červené	bílo-červený	k2eAgFnSc2d1	bílo-červená
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
doplnil	doplnit	k5eAaPmAgInS	doplnit
modrý	modrý	k2eAgInSc1d1	modrý
trojúhelníkem	trojúhelník	k1gInSc7	trojúhelník
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgNnSc4d1	symbolizující
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Slováci	Slovák	k1gMnPc1	Slovák
měli	mít	k5eAaImAgMnP	mít
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
znaku	znak	k1gInSc6	znak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
československá	československý	k2eAgFnSc1d1	Československá
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
ze	z	k7c2	z
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
také	také	k9	také
českou	český	k2eAgFnSc7d1	Česká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
používání	používání	k1gNnSc4	používání
symbolů	symbol	k1gInPc2	symbol
Československa	Československo	k1gNnSc2	Československo
nástupnickými	nástupnický	k2eAgInPc7d1	nástupnický
státy	stát	k1gInPc7	stát
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slováci	Slovák	k1gMnPc1	Slovák
však	však	k9	však
už	už	k6eAd1	už
i	i	k9	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
používali	používat	k5eAaImAgMnP	používat
jako	jako	k8xS	jako
svoji	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
trojpruhou	trojpruha	k1gFnSc7	trojpruha
zástavu	zástav	k1gInSc2	zástav
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInPc1d1	dnešní
bez	bez	k7c2	bez
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
její	její	k3xOp3gNnSc1	její
používání	používání	k1gNnSc1	používání
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
oficiálně	oficiálně	k6eAd1	oficiálně
povolené	povolený	k2eAgInPc1d1	povolený
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
slovenské	slovenský	k2eAgFnSc2d1	slovenská
autonomie	autonomie	k1gFnSc2	autonomie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Česko-Slovenska	Česko-Slovensko	k1gNnSc2	Česko-Slovensko
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
oficiální	oficiální	k2eAgInSc4d1	oficiální
charakter	charakter	k1gInSc4	charakter
vlajka	vlajka	k1gFnSc1	vlajka
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
pruhy	pruh	k1gInPc7	pruh
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc2d1	dnešní
bez	bez	k7c2	bez
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938-1939	[number]	k4	1938-1939
Slováci	Slovák	k1gMnPc1	Slovák
zároveň	zároveň	k6eAd1	zároveň
přechodně	přechodně	k6eAd1	přechodně
používali	používat	k5eAaImAgMnP	používat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
boje	boj	k1gInSc2	boj
za	za	k7c4	za
autonomii	autonomie	k1gFnSc4	autonomie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Česko-Slovenska	Česko-Slovensko	k1gNnSc2	Česko-Slovensko
jako	jako	k8xC	jako
vlajku	vlajka	k1gFnSc4	vlajka
také	také	k9	také
červený	červený	k2eAgInSc4d1	červený
dvojitý	dvojitý	k2eAgInSc4d1	dvojitý
kříž	kříž	k1gInSc4	kříž
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
kruhu	kruh	k1gInSc2	kruh
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
první	první	k4xOgFnSc1	první
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
používala	používat	k5eAaImAgFnS	používat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939-1945	[number]	k4	1939-1945
dnešní	dnešní	k2eAgFnSc4d1	dnešní
vlajku	vlajka	k1gFnSc4	vlajka
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
bez	bez	k7c2	bez
slovenského	slovenský	k2eAgInSc2d1	slovenský
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
státní	státní	k2eAgFnPc1d1	státní
i	i	k9	i
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
zavedla	zavést	k5eAaPmAgFnS	zavést
i	i	k9	i
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
vlajku	vlajka	k1gFnSc4	vlajka
(	(	kIx(	(
<g/>
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
byla	být	k5eAaImAgFnS	být
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
dvojitým	dvojitý	k2eAgInSc7d1	dvojitý
černým	černý	k2eAgInSc7d1	černý
křížem	kříž	k1gInSc7	kříž
v	v	k7c6	v
černě	černě	k6eAd1	černě
lemovaném	lemovaný	k2eAgInSc6d1	lemovaný
štítu	štít	k1gInSc6	štít
uprostřed	uprostřed	k7c2	uprostřed
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
opět	opět	k6eAd1	opět
zavedl	zavést	k5eAaPmAgInS	zavést
vlajku	vlajka	k1gFnSc4	vlajka
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
pruhy	pruh	k1gInPc7	pruh
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
znaku	znak	k1gInSc2	znak
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nezbytným	zbytný	k2eNgInSc7d1	zbytný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
záměnám	záměna	k1gFnPc3	záměna
s	s	k7c7	s
podobnými	podobný	k2eAgFnPc7d1	podobná
novými	nový	k2eAgFnPc7d1	nová
vlajkami	vlajka	k1gFnPc7	vlajka
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgInP	popsat
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
hlavě	hlava	k1gFnSc3	hlava
Ústavy	ústava	k1gFnSc2	ústava
SR	SR	kA	SR
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
zavlála	zavlát	k5eAaPmAgFnS	zavlát
nad	nad	k7c7	nad
Bratislavským	bratislavský	k2eAgInSc7d1	bratislavský
hradem	hrad	k1gInSc7	hrad
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1992	[number]	k4	1992
ve	v	k7c6	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
detaily	detail	k1gInPc1	detail
formy	forma	k1gFnSc2	forma
slovenské	slovenský	k2eAgFnSc2d1	slovenská
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
určil	určit	k5eAaPmAgInS	určit
až	až	k9	až
Zákon	zákon	k1gInSc1	zákon
NR	NR	kA	NR
SR	SR	kA	SR
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něho	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
výška	výška	k1gFnSc1	výška
štítu	štít	k1gInSc2	štít
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
rovná	rovnat	k5eAaImIp3nS	rovnat
polovině	polovina	k1gFnSc3	polovina
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
štít	štít	k1gInSc1	štít
je	být	k5eAaImIp3nS	být
olemovaný	olemovaný	k2eAgMnSc1d1	olemovaný
bílým	bílý	k2eAgInSc7d1	bílý
páskem	pásek	k1gInSc7	pásek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
šířka	šířka	k1gFnSc1	šířka
je	být	k5eAaImIp3nS	být
rovná	rovný	k2eAgFnSc1d1	rovná
jedné	jeden	k4xCgFnSc6	jeden
setině	setina	k1gFnSc3	setina
délky	délka	k1gFnSc2	délka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Slovenska	Slovensko	k1gNnSc2	Slovensko
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
prezidenta	prezident	k1gMnSc2	prezident
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
