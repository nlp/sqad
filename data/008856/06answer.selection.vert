<s>
Barvy	barva	k1gFnPc1	barva
slovenské	slovenský	k2eAgFnSc2d1	slovenská
trikolóry	trikolóra	k1gFnSc2	trikolóra
jsou	být	k5eAaImIp3nP	být
odvozené	odvozený	k2eAgInPc1d1	odvozený
jednak	jednak	k8xC	jednak
od	od	k7c2	od
původních	původní	k2eAgFnPc2d1	původní
slovenských	slovenský	k2eAgFnPc2d1	slovenská
barev	barva	k1gFnPc2	barva
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
jednak	jednak	k8xC	jednak
od	od	k7c2	od
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
trikolóry	trikolóra	k1gFnSc2	trikolóra
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
