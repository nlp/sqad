<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Anton	Anton	k1gMnSc1	Anton
Souchon	Souchon	k1gMnSc1	Souchon
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1864	[number]	k4	1864
Lipsko	Lipsko	k1gNnSc1	Lipsko
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
Brémy	Brémy	k1gFnPc1	Brémy
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
německý	německý	k2eAgInSc1d1	německý
a	a	k8xC	a
osmanský	osmanský	k2eAgMnSc1d1	osmanský
admirál	admirál	k1gMnSc1	admirál
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
velitelem	velitel	k1gMnSc7	velitel
německé	německý	k2eAgFnSc2d1	německá
středomořské	středomořský	k2eAgFnSc2d1	středomořská
divize	divize	k1gFnSc2	divize
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
bitevním	bitevní	k2eAgInSc7d1	bitevní
křižníkem	křižník	k1gInSc7	křižník
SMS	SMS	kA	SMS
Goeben	Goeben	k2eAgMnSc1d1	Goeben
a	a	k8xC	a
lehkým	lehký	k2eAgInSc7d1	lehký
křižníkem	křižník	k1gInSc7	křižník
SMS	SMS	kA	SMS
Breslau	Breslaus	k1gInSc2	Breslaus
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
vstupu	vstup	k1gInSc2	vstup
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
do	do	k7c2	do
války	válka	k1gFnSc2	válka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Centrálních	centrální	k2eAgFnPc2d1	centrální
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
plavidla	plavidlo	k1gNnSc2	plavidlo
formálně	formálně	k6eAd1	formálně
zařadili	zařadit	k5eAaPmAgMnP	zařadit
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
(	(	kIx(	(
<g/>
sloužily	sloužit	k5eAaImAgFnP	sloužit
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
posádku	posádka	k1gFnSc4	posádka
<g/>
)	)	kIx)	)
a	a	k8xC	a
země	zem	k1gFnPc1	zem
do	do	k7c2	do
války	válka	k1gFnSc2	válka
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
po	po	k7c6	po
německo-tureckém	německourecký	k2eAgInSc6d1	německo-turecký
nájezdu	nájezd	k1gInSc6	nájezd
na	na	k7c4	na
ruské	ruský	k2eAgInPc4d1	ruský
černomořské	černomořský	k2eAgInPc4d1	černomořský
přístavy	přístav	k1gInPc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Souchon	Souchon	k1gMnSc1	Souchon
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velitelem	velitel	k1gMnSc7	velitel
tureckého	turecký	k2eAgNnSc2d1	turecké
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
obdržel	obdržet	k5eAaPmAgMnS	obdržet
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
německé	německý	k2eAgNnSc4d1	německé
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
Pour	Pour	k1gMnSc1	Pour
le	le	k?	le
Mérite	Mérit	k1gInSc5	Mérit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
viceadmirála	viceadmirál	k1gMnSc4	viceadmirál
<g/>
.	.	kIx.	.
</s>
<s>
Souchon	Souchon	k1gMnSc1	Souchon
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
reformovat	reformovat	k5eAaBmF	reformovat
turecké	turecký	k2eAgNnSc4d1	turecké
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
tam	tam	k6eAd1	tam
převzal	převzít	k5eAaPmAgInS	převzít
velení	velení	k1gNnSc4	velení
4	[number]	k4	4
<g/>
.	.	kIx.	.
eskadry	eskadra	k1gFnPc4	eskadra
křižníků	křižník	k1gInPc2	křižník
během	během	k7c2	během
operace	operace	k1gFnSc2	operace
Albion	Albion	k1gInSc1	Albion
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Brémách	Brémy	k1gFnPc6	Brémy
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Hermann	Hermann	k1gMnSc1	Hermann
Souchon	Souchon	k1gMnSc1	Souchon
zabil	zabít	k5eAaPmAgMnS	zabít
Rosu	Rosa	k1gFnSc4	Rosa
Luxemburgovou	Luxemburgový	k2eAgFnSc4d1	Luxemburgová
<g/>
.	.	kIx.	.
</s>
<s>
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
Řád	řád	k1gInSc1	řád
červené	červená	k1gFnSc2	červená
orlice	orlice	k1gFnSc2	orlice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
s	s	k7c7	s
mit	mit	k?	mit
Stern	sternum	k1gNnPc2	sternum
<g/>
,	,	kIx,	,
Eichenlaub	Eichenlaub	k1gMnSc1	Eichenlaub
und	und	k?	und
Schwertern	Schwertern	k1gMnSc1	Schwertern
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wilhelm	Wilhelma	k1gFnPc2	Wilhelma
Souchon	Souchon	k1gNnSc4	Souchon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
