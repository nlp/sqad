<s>
Hajao	Hajao	k1gNnSc1	Hajao
Mijazaki	Mijazak	k1gFnSc2	Mijazak
(	(	kIx(	(
<g/>
宮	宮	k?	宮
<g/>
,	,	kIx,	,
Mijazaki	Mijazak	k1gMnPc1	Mijazak
Hajao	Hajao	k6eAd1	Hajao
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
a	a	k8xC	a
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
tvůrců	tvůrce	k1gMnPc2	tvůrce
japonských	japonský	k2eAgInPc2d1	japonský
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
–	–	k?	–
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Isao	Isao	k6eAd1	Isao
Takahatou	Takahatý	k2eAgFnSc7d1	Takahatý
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
vede	vést	k5eAaImIp3nS	vést
animátorské	animátorský	k2eAgNnSc1d1	animátorské
studio	studio	k1gNnSc1	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
produkovalo	produkovat	k5eAaImAgNnS	produkovat
jeho	jeho	k3xOp3gInPc4	jeho
nejznámější	známý	k2eAgInPc4d3	nejznámější
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Mijazaki	Mijazaki	k6eAd1	Mijazaki
je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
mnoha	mnoho	k4c2	mnoho
populárních	populární	k2eAgInPc2d1	populární
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
anime	animat	k5eAaPmIp3nS	animat
i	i	k9	i
několika	několik	k4yIc2	několik
japonských	japonský	k2eAgInPc2d1	japonský
komiksů	komiks	k1gInPc2	komiks
(	(	kIx(	(
<g/>
mangy	mango	k1gNnPc7	mango
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
slavily	slavit	k5eAaImAgInP	slavit
velké	velký	k2eAgInPc1d1	velký
úspěchy	úspěch	k1gInPc1	úspěch
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
hodnocení	hodnocení	k1gNnSc1	hodnocení
kritiky	kritika	k1gFnSc2	kritika
i	i	k8xC	i
výdělečnosti	výdělečnost	k1gFnSc2	výdělečnost
dříve	dříve	k6eAd2	dříve
zejména	zejména	k9	zejména
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
na	na	k7c4	na
západ	západ	k1gInSc4	západ
Miramax	Miramax	k1gInSc1	Miramax
uvedl	uvést	k5eAaPmAgInS	uvést
film	film	k1gInSc1	film
Princezna	princezna	k1gFnSc1	princezna
Mononoke	Mononoke	k1gFnSc1	Mononoke
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
krátce	krátce	k6eAd1	krátce
stal	stát	k5eAaPmAgInS	stát
nejvýdělečnejším	výdělečný	k2eAgInSc7d3	nejvýdělečnější
filmem	film	k1gInSc7	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
překonán	překonat	k5eAaPmNgInS	překonat
Titanikem	Titanic	k1gInSc7	Titanic
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
ale	ale	k9	ale
opět	opět	k6eAd1	opět
překonal	překonat	k5eAaPmAgMnS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
jiný	jiný	k2eAgInSc4d1	jiný
Mijazakiho	Mijazaki	k1gMnSc2	Mijazaki
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Mijazakiho	Mijazakize	k6eAd1	Mijazakize
tvorba	tvorba	k1gFnSc1	tvorba
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
opakujícími	opakující	k2eAgInPc7d1	opakující
se	se	k3xPyFc4	se
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
vztah	vztah	k1gInSc1	vztah
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
obtížnost	obtížnost	k1gFnSc1	obtížnost
jednání	jednání	k1gNnSc2	jednání
podle	podle	k7c2	podle
pacifistické	pacifistický	k2eAgFnSc2d1	pacifistická
etiky	etika	k1gFnSc2	etika
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pouť	pouť	k1gFnSc1	pouť
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
za	za	k7c7	za
nezávislostí	nezávislost	k1gFnSc7	nezávislost
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc7d1	vlastní
identitou	identita	k1gFnSc7	identita
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinové	Hrdinová	k1gFnPc1	Hrdinová
jeho	jeho	k3xOp3gInPc2	jeho
filmů	film	k1gInPc2	film
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
dívky	dívka	k1gFnPc1	dívka
nebo	nebo	k8xC	nebo
mladé	mladý	k2eAgFnPc1d1	mladá
ženy	žena	k1gFnPc1	žena
<g/>
;	;	kIx,	;
ze	z	k7c2	z
"	"	kIx"	"
<g/>
zloduchů	zloduch	k1gMnPc2	zloduch
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
však	však	k9	však
často	často	k6eAd1	často
stávají	stávat	k5eAaImIp3nP	stávat
neurčité	určitý	k2eNgFnPc4d1	neurčitá
postavy	postava	k1gFnPc4	postava
s	s	k7c7	s
vlastními	vlastní	k2eAgFnPc7d1	vlastní
kvalitami	kvalita	k1gFnPc7	kvalita
-	-	kIx~	-
postavy	postava	k1gFnPc1	postava
se	se	k3xPyFc4	se
málokdy	málokdy	k6eAd1	málokdy
dělí	dělit	k5eAaImIp3nS	dělit
černobíle	černobíle	k6eAd1	černobíle
na	na	k7c4	na
dobré	dobrý	k2eAgFnPc4d1	dobrá
a	a	k8xC	a
zlé	zlý	k2eAgNnSc1d1	zlé
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
v	v	k7c4	v
Mijazakiho	Mijazaki	k1gMnSc4	Mijazaki
filmech	film	k1gInPc6	film
záporné	záporný	k2eAgFnPc1d1	záporná
postavy	postava	k1gFnPc1	postava
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
opakují	opakovat	k5eAaImIp3nP	opakovat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
prvky	prvek	k1gInPc4	prvek
-	-	kIx~	-
Mijazaki	Mijazak	k1gFnPc4	Mijazak
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
náruživým	náruživý	k2eAgMnSc7d1	náruživý
milovníkem	milovník	k1gMnSc7	milovník
létání	létání	k1gNnSc2	létání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
jeho	jeho	k3xOp3gInPc6	jeho
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Mijazaki	Mijazaki	k6eAd1	Mijazaki
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
Walta	Walt	k1gInSc2	Walt
Disneyho	Disney	k1gMnSc2	Disney
japonské	japonský	k2eAgFnSc2d1	japonská
animace	animace	k1gFnSc2	animace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
přirovnání	přirovnání	k1gNnSc1	přirovnání
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
skutečně	skutečně	k6eAd1	skutečně
naplnit	naplnit	k5eAaPmF	naplnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
úspěchy	úspěch	k1gInPc1	úspěch
jeho	jeho	k3xOp3gInPc2	jeho
filmů	film	k1gInPc2	film
ho	on	k3xPp3gMnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Mijazaki	Mijazaki	k6eAd1	Mijazaki
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
však	však	k9	však
nevidí	vidět	k5eNaImIp3nS	vidět
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
budující	budující	k2eAgMnSc1d1	budující
animátorské	animátorský	k2eAgNnSc4d1	animátorské
impérium	impérium	k1gNnSc4	impérium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
animátor	animátor	k1gMnSc1	animátor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgInS	mít
dost	dost	k6eAd1	dost
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dostal	dostat	k5eAaPmAgMnS	dostat
příležitost	příležitost	k1gFnSc4	příležitost
tvořit	tvořit	k5eAaImF	tvořit
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
zanechal	zanechat	k5eAaPmAgInS	zanechat
osobní	osobní	k2eAgFnSc4d1	osobní
stopu	stopa	k1gFnSc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hajao	Hajao	k6eAd1	Hajao
Mijazaki	Mijazake	k1gFnSc4	Mijazake
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Hajao	Hajao	k6eAd1	Hajao
Mijazaki	Mijazak	k1gFnSc2	Mijazak
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Hajao	Hajao	k6eAd1	Hajao
Mijazaki	Mijazaki	k1gNnSc1	Mijazaki
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hajao	Hajao	k6eAd1	Hajao
Mijazaki	Mijazaki	k1gNnSc7	Mijazaki
na	na	k7c6	na
Nausicaa	Nausicaa	k1gFnSc1	Nausicaa
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
-	-	kIx~	-
řada	řada	k1gFnSc1	řada
detailních	detailní	k2eAgFnPc2d1	detailní
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
filmografie	filmografie	k1gFnSc1	filmografie
Hajao	Hajao	k6eAd1	Hajao
Mijazaki	Mijazake	k1gFnSc4	Mijazake
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
