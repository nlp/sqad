<s>
New	New	k?	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1851	[number]	k4	1851
kanadským	kanadský	k2eAgInSc7d1	kanadský
novinářem	novinář	k1gMnSc7	novinář
a	a	k8xC	a
politikem	politik	k1gMnSc7	politik
Henrym	Henry	k1gMnSc7	Henry
Jarvis	Jarvis	k1gFnSc2	Jarvis
Raymondem	Raymond	k1gMnSc7	Raymond
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgMnSc7	druhý
předsedou	předseda	k1gMnSc7	předseda
republikánského	republikánský	k2eAgInSc2d1	republikánský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
bývalým	bývalý	k2eAgMnSc7d1	bývalý
bankéřem	bankéř	k1gMnSc7	bankéř
Georgem	Georg	k1gMnSc7	Georg
Jonesem	Jones	k1gMnSc7	Jones
jako	jako	k8xC	jako
New-York	New-York	k1gInSc4	New-York
Daily	Daila	k1gFnSc2	Daila
Times	Timesa	k1gFnPc2	Timesa
<g/>
.	.	kIx.	.
</s>
