<p>
<s>
The	The	k?	The
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
je	být	k5eAaImIp3nS	být
newyorský	newyorský	k2eAgInSc1d1	newyorský
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
získal	získat	k5eAaPmAgInS	získat
122	[number]	k4	122
Pulitzerových	Pulitzerův	k2eAgFnPc2d1	Pulitzerova
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
kterékoliv	kterýkoliv	k3yIgFnPc1	kterýkoliv
jiné	jiný	k2eAgFnPc1d1	jiná
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
na	na	k7c4	na
350	[number]	k4	350
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
má	mít	k5eAaImIp3nS	mít
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
Tower	Towra	k1gFnPc2	Towra
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgFnPc1d3	veliký
metropolitní	metropolitní	k2eAgFnPc1d1	metropolitní
noviny	novina	k1gFnPc1	novina
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
přezdívány	přezdíván	k2eAgFnPc1d1	přezdívána
"	"	kIx"	"
<g/>
Gray	Gra	k2eAgFnPc1d1	Gra
Lady	Lada	k1gFnPc1	Lada
<g/>
"	"	kIx"	"
–	–	k?	–
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
neobvyklého	obvyklý	k2eNgInSc2d1	neobvyklý
vzhledu	vzhled	k1gInSc2	vzhled
a	a	k8xC	a
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Times	Times	k1gInSc1	Times
vlastní	vlastní	k2eAgFnSc1d1	vlastní
firma	firma	k1gFnSc1	firma
The	The	k1gFnSc2	The
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vydává	vydávat	k5eAaPmIp3nS	vydávat
18	[number]	k4	18
dalších	další	k2eAgFnPc2d1	další
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
International	International	k1gFnSc2	International
Herald	Heralda	k1gFnPc2	Heralda
Tribune	tribun	k1gMnSc5	tribun
a	a	k8xC	a
The	The	k1gMnSc5	The
Boston	Boston	k1gInSc1	Boston
Globe	globus	k1gInSc5	globus
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
Arthur	Arthur	k1gMnSc1	Arthur
Ochs	Ochsa	k1gFnPc2	Ochsa
Sulzberger	Sulzberger	k1gMnSc1	Sulzberger
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rodina	rodina	k1gFnSc1	rodina
má	mít	k5eAaImIp3nS	mít
list	list	k1gInSc1	list
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Motto	motto	k1gNnSc1	motto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
vytištěno	vytisknout	k5eAaPmNgNnS	vytisknout
v	v	k7c6	v
levém	levý	k2eAgInSc6d1	levý
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
the	the	k?	the
News	News	k1gInSc1	News
That	That	k1gInSc1	That
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fit	fit	k2eAgFnSc7d1	fit
to	ten	k3xDgNnSc4	ten
Print	Print	k1gMnSc1	Print
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
jsou	být	k5eAaImIp3nP	být
organizovány	organizovat	k5eAaBmNgFnP	organizovat
do	do	k7c2	do
sekcí	sekce	k1gFnPc2	sekce
<g/>
:	:	kIx,	:
News	News	k1gInSc1	News
(	(	kIx(	(
<g/>
novinky	novinka	k1gFnPc1	novinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Opinions	Opinions	k1gInSc1	Opinions
(	(	kIx(	(
<g/>
názory	názor	k1gInPc1	názor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Business	business	k1gInSc1	business
(	(	kIx(	(
<g/>
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arts	Arts	k1gInSc1	Arts
(	(	kIx(	(
<g/>
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sports	Sports	k1gInSc1	Sports
(	(	kIx(	(
<g/>
sport	sport	k1gInSc1	sport
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Style	styl	k1gInSc5	styl
(	(	kIx(	(
<g/>
styl	styl	k1gInSc4	styl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
and	and	k?	and
Features	Features	k1gInSc1	Features
(	(	kIx(	(
<g/>
reportáže	reportáž	k1gFnPc1	reportáž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Times	Times	k1gMnSc1	Times
zůstal	zůstat	k5eAaPmAgMnS	zůstat
s	s	k7c7	s
osmisloupcovým	osmisloupcový	k2eAgInSc7d1	osmisloupcový
formátem	formát	k1gInSc7	formát
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
většina	většina	k1gFnSc1	většina
novin	novina	k1gFnPc2	novina
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
šest	šest	k4xCc4	šest
sloupců	sloupec	k1gInPc2	sloupec
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přijaly	přijmout	k5eAaPmAgFnP	přijmout
barevné	barevný	k2eAgFnPc4d1	barevná
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
New	New	k1gMnPc2	New
York	York	k1gInSc4	York
Times	Timesa	k1gFnPc2	Timesa
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
nejpopulárnějšími	populární	k2eAgMnPc7d3	nejpopulárnější
americkými	americký	k2eAgMnPc7d1	americký
on-line	onin	k1gInSc5	on-lin
novinami	novina	k1gFnPc7	novina
<g/>
,	,	kIx,	,
za	za	k7c4	za
uvedený	uvedený	k2eAgInSc4d1	uvedený
měsíc	měsíc	k1gInSc4	měsíc
měla	mít	k5eAaImAgFnS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
18	[number]	k4	18
milionů	milion	k4xCgInPc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
New	New	k?	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1851	[number]	k4	1851
kanadským	kanadský	k2eAgInSc7d1	kanadský
novinářem	novinář	k1gMnSc7	novinář
a	a	k8xC	a
politikem	politik	k1gMnSc7	politik
Henrym	Henry	k1gMnSc7	Henry
Jarvis	Jarvis	k1gFnSc2	Jarvis
Raymondem	Raymond	k1gMnSc7	Raymond
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgMnSc7	druhý
předsedou	předseda	k1gMnSc7	předseda
republikánského	republikánský	k2eAgInSc2d1	republikánský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
bývalým	bývalý	k2eAgMnSc7d1	bývalý
bankéřem	bankéř	k1gMnSc7	bankéř
Georgem	Georg	k1gMnSc7	Georg
Jonesem	Jones	k1gMnSc7	Jones
jako	jako	k8xC	jako
New-York	New-York	k1gInSc4	New-York
Daily	Daila	k1gFnSc2	Daila
Times	Timesa	k1gFnPc2	Timesa
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
prodávány	prodávat	k5eAaImNgInP	prodávat
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
jednoho	jeden	k4xCgInSc2	jeden
centu	cent	k1gInSc2	cent
za	za	k7c4	za
kopii	kopie	k1gFnSc4	kopie
<g/>
,	,	kIx,	,
zahajovací	zahajovací	k2eAgNnSc4d1	zahajovací
vydání	vydání	k1gNnSc4	vydání
se	se	k3xPyFc4	se
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
řešit	řešit	k5eAaImF	řešit
různé	různý	k2eAgFnPc4d1	různá
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
záměrech	záměr	k1gInPc6	záměr
a	a	k8xC	a
postojích	postoj	k1gInPc6	postoj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
předcházely	předcházet	k5eAaImAgFnP	předcházet
jeho	jeho	k3xOp3gNnSc4	jeho
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
List	list	k1gInSc1	list
změnil	změnit	k5eAaPmAgInS	změnit
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
na	na	k7c6	na
The	The	k1gFnSc6	The
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
vydávány	vydáván	k2eAgFnPc1d1	vydávána
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
až	až	k9	až
na	na	k7c4	na
neděli	neděle	k1gFnSc4	neděle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1861	[number]	k4	1861
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
denním	denní	k2eAgNnSc6d1	denní
zpravodajství	zpravodajství	k1gNnSc6	zpravodajství
z	z	k7c2	z
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Times	Times	k1gInSc4	Times
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
významnými	významný	k2eAgInPc7d1	významný
deníky	deník	k1gInPc7	deník
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaPmF	vydávat
nedělní	nedělní	k2eAgNnSc4d1	nedělní
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gFnSc2	The
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
The	The	k1gFnSc2	The
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
