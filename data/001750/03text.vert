<s>
Sir	sir	k1gMnSc1	sir
Terence	Terence	k1gFnSc2	Terence
David	David	k1gMnSc1	David
John	John	k1gMnSc1	John
"	"	kIx"	"
<g/>
Terry	Terra	k1gMnSc2	Terra
<g/>
"	"	kIx"	"
Pratchett	Pratchett	k1gMnSc1	Pratchett
<g/>
,	,	kIx,	,
OBE	Ob	k1gInSc5	Ob
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1948	[number]	k4	1948
Beaconsfield	Beaconsfielda	k1gFnPc2	Beaconsfielda
<g/>
,	,	kIx,	,
Buckinghamshire	Buckinghamshir	k1gMnSc5	Buckinghamshir
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
Broad	Broad	k1gInSc4	Broad
Chalke	Chalk	k1gFnSc2	Chalk
<g/>
,	,	kIx,	,
Wiltshire	Wiltshir	k1gMnSc5	Wiltshir
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
věnující	věnující	k2eAgMnSc1d1	věnující
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
žánru	žánr	k1gInSc3	žánr
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
především	především	k9	především
svou	svůj	k3xOyFgFnSc7	svůj
sérií	série	k1gFnSc7	série
knih	kniha	k1gFnPc2	kniha
Úžasná	úžasný	k2eAgFnSc1d1	úžasná
Zeměplocha	Zeměploch	k1gMnSc4	Zeměploch
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
překládal	překládat	k5eAaImAgMnS	překládat
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
.	.	kIx.	.
</s>
<s>
Pratchett	Pratchett	k1gMnSc1	Pratchett
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Beaconsfieldu	Beaconsfield	k1gInSc6	Beaconsfield
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
hrabství	hrabství	k1gNnSc6	hrabství
Buckingham	Buckingham	k1gInSc1	Buckingham
Davidovi	David	k1gMnSc6	David
a	a	k8xC	a
Eileen	Eileen	k1gInSc4	Eileen
Pratchettovým	Pratchettův	k2eAgInSc7d1	Pratchettův
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
vzdělání	vzdělání	k1gNnSc4	vzdělání
připisuje	připisovat	k5eAaImIp3nS	připisovat
High	High	k1gMnSc1	High
Wycombe	Wycomb	k1gInSc5	Wycomb
Technical	Technical	k1gMnPc3	Technical
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc4	School
a	a	k8xC	a
Beaconsfieldské	Beaconsfieldský	k2eAgFnSc6d1	Beaconsfieldský
veřejné	veřejný	k2eAgFnSc6d1	veřejná
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
vydaným	vydaný	k2eAgInSc7d1	vydaný
dílem	díl	k1gInSc7	díl
byla	být	k5eAaImAgFnS	být
krátká	krátký	k2eAgFnSc1d1	krátká
povídka	povídka	k1gFnSc1	povídka
The	The	k1gFnSc2	The
Hades	Hadesa	k1gFnPc2	Hadesa
Business	business	k1gInSc1	business
(	(	kIx(	(
<g/>
Kšeft	Kšeft	k?	Kšeft
v	v	k7c6	v
Hádesu	Hádes	k1gInSc6	Hádes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uveřejněná	uveřejněný	k2eAgFnSc1d1	uveřejněná
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
školním	školní	k2eAgInSc6d1	školní
časopise	časopis	k1gInSc6	časopis
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
13	[number]	k4	13
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Science	Science	k1gFnSc2	Science
Fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
obdržel	obdržet	k5eAaPmAgMnS	obdržet
14	[number]	k4	14
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
uveřejněná	uveřejněný	k2eAgFnSc1d1	uveřejněná
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Night	Night	k2eAgInSc4d1	Night
Dweller	Dweller	k1gInSc4	Dweller
(	(	kIx(	(
<g/>
Noční	noční	k2eAgMnSc1d1	noční
tvor	tvor	k1gMnSc1	tvor
<g/>
)	)	kIx)	)
a	a	k8xC	a
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
156	[number]	k4	156
<g/>
.	.	kIx.	.
čísle	číslo	k1gNnSc6	číslo
časopisu	časopis	k1gInSc2	časopis
New	New	k1gFnSc2	New
Worlds	Worldsa	k1gFnPc2	Worldsa
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
získal	získat	k5eAaPmAgMnS	získat
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
novinách	novina	k1gFnPc6	novina
Bucks	Bucksa	k1gFnPc2	Bucksa
Free	Fre	k1gInSc2	Fre
Press	Pressa	k1gFnPc2	Pressa
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgFnPc6d1	další
regionálních	regionální	k2eAgFnPc6d1	regionální
novinách	novina	k1gFnPc6	novina
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Anglii	Anglie	k1gFnSc6	Anglie
včetně	včetně	k7c2	včetně
Western	Western	kA	Western
Daily	Daila	k1gFnSc2	Daila
Press	Press	k1gInSc4	Press
a	a	k8xC	a
Bath	Bath	k1gInSc4	Bath
Chronicle	Chronicle	k1gFnSc2	Chronicle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
novinářském	novinářský	k2eAgNnSc6d1	novinářské
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
vyslán	vyslán	k2eAgMnSc1d1	vyslán
udělat	udělat	k5eAaPmF	udělat
interview	interview	k1gNnSc4	interview
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Bander	Bandra	k1gFnPc2	Bandra
van	van	k1gInSc1	van
Durenem	Dureno	k1gNnSc7	Dureno
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
ředitelů	ředitel	k1gMnPc2	ředitel
malého	malý	k2eAgNnSc2d1	malé
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
právě	právě	k6eAd1	právě
vycházela	vycházet	k5eAaImAgFnS	vycházet
nová	nový	k2eAgFnSc1d1	nová
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
rozhovoru	rozhovor	k1gInSc2	rozhovor
se	se	k3xPyFc4	se
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
napsal	napsat	k5eAaPmAgMnS	napsat
knihu	kniha	k1gFnSc4	kniha
The	The	k1gMnSc1	The
Carpet	Carpet	k1gMnSc1	Carpet
People	People	k1gMnSc1	People
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Kobercové	kobercový	k2eAgNnSc1d1	kobercové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
slavnostně	slavnostně	k6eAd1	slavnostně
představena	představit	k5eAaPmNgFnS	představit
v	v	k7c6	v
oddělení	oddělení	k1gNnSc6	oddělení
koberců	koberec	k1gInPc2	koberec
obchodního	obchodní	k2eAgInSc2d1	obchodní
domu	dům	k1gInSc2	dům
Heal	Heala	k1gFnPc2	Heala
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
na	na	k7c4	na
Tottenham	Tottenham	k1gInSc4	Tottenham
Court	Court	k1gInSc1	Court
Road	Road	k1gInSc1	Road
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
Pratchett	Pratchett	k1gMnSc1	Pratchett
stal	stát	k5eAaPmAgMnS	stát
tiskovým	tiskový	k2eAgMnSc7d1	tiskový
mluvčím	mluvčí	k1gMnSc7	mluvčí
elektrárenské	elektrárenský	k2eAgFnSc2d1	elektrárenská
společnosti	společnost	k1gFnSc2	společnost
Central	Central	k1gFnSc2	Central
Electricity	Electricita	k1gFnSc2	Electricita
Generating	Generating	k1gInSc1	Generating
Board	Board	k1gInSc1	Board
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
působnosti	působnost	k1gFnSc6	působnost
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vtipkoval	vtipkovat	k5eAaImAgInS	vtipkovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předvedl	předvést	k5eAaPmAgMnS	předvést
svůj	svůj	k3xOyFgInSc4	svůj
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
načasování	načasování	k1gNnSc4	načasování
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
změna	změna	k1gFnSc1	změna
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
následovala	následovat	k5eAaImAgFnS	následovat
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
jaderné	jaderný	k2eAgFnSc6d1	jaderná
havárii	havárie	k1gFnSc6	havárie
v	v	k7c6	v
elektrárně	elektrárna	k1gFnSc6	elektrárna
Three	Thre	k1gFnSc2	Thre
Mile	mile	k6eAd1	mile
Island	Island	k1gInSc4	Island
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
CEGB	CEGB	kA	CEGB
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
vydělávat	vydělávat	k5eAaImF	vydělávat
víc	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
příležitostným	příležitostný	k2eAgNnSc7d1	příležitostné
psaním	psaní	k1gNnSc7	psaní
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
zvýšit	zvýšit	k5eAaPmF	zvýšit
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nejproduktivnějším	produktivní	k2eAgNnSc6d3	nejproduktivnější
období	období	k1gNnSc6	období
napsat	napsat	k5eAaBmF	napsat
dvě	dva	k4xCgFnPc4	dva
knihy	kniha	k1gFnPc4	kniha
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
obdržel	obdržet	k5eAaPmAgInS	obdržet
Řád	řád	k1gInSc1	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
Officer	Officra	k1gFnPc2	Officra
za	za	k7c4	za
službu	služba	k1gFnSc4	služba
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sobě	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc7d1	vlastní
ironií	ironie	k1gFnSc7	ironie
to	ten	k3xDgNnSc1	ten
komentoval	komentovat	k5eAaBmAgInS	komentovat
prohlášením	prohlášení	k1gNnSc7	prohlášení
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
mám	mít	k5eAaImIp1nS	mít
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
'	'	kIx"	'
<g/>
službu	služba	k1gFnSc4	služba
literatuře	literatura	k1gFnSc3	literatura
<g/>
'	'	kIx"	'
prokážu	prokázat	k5eAaPmIp1nS	prokázat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zdržím	zdrž	k1gFnPc3	zdrž
dalších	další	k2eAgInPc2d1	další
pokusů	pokus	k1gInPc2	pokus
nějakou	nějaký	k3yIgFnSc4	nějaký
napsat	napsat	k5eAaPmF	napsat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
ve	v	k7c6	v
Warwicku	Warwick	k1gInSc6	Warwick
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Rhianna	Rhiann	k1gInSc2	Rhiann
Pratchett	Pratchettum	k1gNnPc2	Pratchettum
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
autorkou	autorka	k1gFnSc7	autorka
fantasy	fantas	k1gInPc7	fantas
a	a	k8xC	a
novinářkou	novinářka	k1gFnSc7	novinářka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
svoje	svůj	k3xOyFgFnPc4	svůj
záliby	záliba	k1gFnPc4	záliba
Pratchett	Pratchett	k1gInSc1	Pratchett
uváděl	uvádět	k5eAaImAgInS	uvádět
"	"	kIx"	"
<g/>
psaní	psaní	k1gNnPc1	psaní
<g/>
,	,	kIx,	,
procházení	procházení	k1gNnPc1	procházení
<g/>
,	,	kIx,	,
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
také	také	k9	také
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
zálibě	záliba	k1gFnSc3	záliba
v	v	k7c6	v
pokrývkách	pokrývka	k1gFnPc6	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
stranách	strana	k1gFnPc6	strana
obálek	obálka	k1gFnPc2	obálka
originálních	originální	k2eAgNnPc2d1	originální
vydání	vydání	k1gNnPc2	vydání
většiny	většina	k1gFnSc2	většina
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
Pratchettovi	Pratchett	k1gMnSc3	Pratchett
diagnostikováno	diagnostikován	k2eAgNnSc1d1	diagnostikováno
onemocnění	onemocnění	k1gNnSc1	onemocnění
Alzheimerovou	Alzheimerův	k2eAgFnSc7d1	Alzheimerova
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
jej	on	k3xPp3gInSc4	on
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
rytířem	rytíř	k1gMnSc7	rytíř
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
mohl	moct	k5eAaImAgInS	moct
Pratchett	Pratchett	k1gInSc1	Pratchett
používat	používat	k5eAaImF	používat
titul	titul	k1gInSc4	titul
sir	sir	k1gMnSc1	sir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Pratchett	Pratchett	k1gMnSc1	Pratchett
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
podstoupit	podstoupit	k5eAaPmF	podstoupit
eutanázii	eutanázie	k1gFnSc4	eutanázie
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
kritického	kritický	k2eAgNnSc2d1	kritické
stádia	stádium	k1gNnSc2	stádium
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
přednesl	přednést	k5eAaPmAgInS	přednést
přednášku	přednáška	k1gFnSc4	přednáška
Richarda	Richard	k1gMnSc2	Richard
Dimblebyho	Dimbleby	k1gMnSc2	Dimbleby
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
přednáší	přednášet	k5eAaImIp3nS	přednášet
nějaká	nějaký	k3yIgFnSc1	nějaký
významná	významný	k2eAgFnSc1d1	významná
osobnost	osobnost	k1gFnSc1	osobnost
přednášku	přednáška	k1gFnSc4	přednáška
na	na	k7c4	na
aktuální	aktuální	k2eAgNnSc4d1	aktuální
téma	téma	k1gNnSc4	téma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přednáška	přednáška	k1gFnSc1	přednáška
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Shaking	Shaking	k1gInSc4	Shaking
Hands	Handsa	k1gFnPc2	Handsa
With	With	k1gMnSc1	With
Death	Death	k1gMnSc1	Death
(	(	kIx(	(
<g/>
Potřást	potřást	k5eAaPmF	potřást
si	se	k3xPyFc3	se
rukou	ruka	k1gFnPc6	ruka
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
/	/	kIx~	/
Smrtěm	Smrtě	k1gNnSc7	Smrtě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pratchett	Pratchett	k1gMnSc1	Pratchett
přednášku	přednáška	k1gFnSc4	přednáška
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
zdravotnímu	zdravotní	k2eAgNnSc3d1	zdravotní
stavu	stav	k1gInSc6	stav
ji	on	k3xPp3gFnSc4	on
nemohl	moct	k5eNaImAgMnS	moct
přednést	přednést	k5eAaPmF	přednést
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
ji	on	k3xPp3gFnSc4	on
proto	proto	k8xC	proto
přečetl	přečíst	k5eAaPmAgMnS	přečíst
Pratchettův	Pratchettův	k2eAgMnSc1d1	Pratchettův
přítel	přítel	k1gMnSc1	přítel
herec	herec	k1gMnSc1	herec
Tony	Tony	k1gMnSc1	Tony
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
Baldrick	Baldrick	k1gMnSc1	Baldrick
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Černá	černý	k2eAgFnSc1d1	černá
zmije	zmije	k1gFnSc1	zmije
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
na	na	k7c4	na
následky	následek	k1gInPc4	následek
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
děl	dělo	k1gNnPc2	dělo
"	"	kIx"	"
<g/>
Úžasná	úžasný	k2eAgFnSc1d1	úžasná
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Discworld	Discworlda	k1gFnPc2	Discworlda
Kobercové	kobercový	k2eAgFnPc1d1	kobercová
-	-	kIx~	-
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
Carpet	Carpet	k1gMnSc1	Carpet
People	People	k1gMnSc1	People
<g/>
,	,	kIx,	,
základ	základ	k1gInSc1	základ
příběhů	příběh	k1gInPc2	příběh
o	o	k7c4	o
Nomech	Nomech	k1gInSc4	Nomech
Vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
Nomech	Nom	k1gFnPc6	Nom
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
The	The	k1gFnSc2	The
Nome	Nom	k1gFnSc2	Nom
Trilogy	Triloga	k1gFnSc2	Triloga
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
The	The	k1gFnPc1	The
Bromeliad	Bromeliad	k1gInSc1	Bromeliad
Trilogy	Triloga	k1gFnSc2	Triloga
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
jízda	jízda	k1gFnSc1	jízda
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Truckers	Truckers	k1gInSc4	Truckers
Velký	velký	k2eAgInSc1d1	velký
boj	boj	k1gInSc1	boj
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Na	na	k7c4	na
nepřítele	nepřítel	k1gMnSc4	nepřítel
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Diggers	Diggers	k1gInSc4	Diggers
Velký	velký	k2eAgInSc1d1	velký
let	let	k1gInSc1	let
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Na	na	k7c4	na
shledanou	shledaná	k1gFnSc4	shledaná
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Wings	Wingsa	k1gFnPc2	Wingsa
Odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
slunce	slunce	k1gNnSc2	slunce
-	-	kIx~	-
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
The	The	k1gFnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Sun	Sun	kA	Sun
(	(	kIx(	(
<g/>
sci-fi	scii	k1gNnPc2	sci-fi
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Strata	Strata	k1gFnSc1	Strata
-	-	kIx~	-
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
čistokrevné	čistokrevný	k2eAgNnSc1d1	čistokrevné
sci-fi	scii	k1gNnSc1	sci-fi
o	o	k7c6	o
planetární	planetární	k2eAgFnSc6d1	planetární
inženýrce	inženýrka	k1gFnSc6	inženýrka
Kin	kino	k1gNnPc2	kino
Aradové	Aradová	k1gFnSc2	Aradová
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
motiv	motiv	k1gInSc4	motiv
ploché	plochý	k2eAgFnSc2d1	plochá
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
trochu	trochu	k6eAd1	trochu
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
později	pozdě	k6eAd2	pozdě
napsané	napsaný	k2eAgInPc1d1	napsaný
Zeměplochy	Zeměploch	k1gInPc1	Zeměploch
<g/>
)	)	kIx)	)
Dobrá	dobrá	k1gFnSc1	dobrá
znamení	znamení	k1gNnSc2	znamení
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Good	Gooda	k1gFnPc2	Gooda
Omens	Omensa	k1gFnPc2	Omensa
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
N.	N.	kA	N.
Gaimanem	Gaiman	k1gMnSc7	Gaiman
<g/>
,	,	kIx,	,
humorně	humorně	k6eAd1	humorně
pojatý	pojatý	k2eAgInSc4d1	pojatý
příběh	příběh	k1gInSc4	příběh
o	o	k7c4	o
zrození	zrození	k1gNnSc4	zrození
Antikrista	Antikrist	k1gMnSc2	Antikrist
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
příběhů	příběh	k1gInPc2	příběh
Johnnyho	Johnny	k1gMnSc2	Johnny
Maxwella	Maxwell	k1gMnSc2	Maxwell
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vidí	vidět	k5eAaImIp3nS	vidět
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
,	,	kIx,	,
mimozemšťany	mimozemšťan	k1gMnPc4	mimozemšťan
a	a	k8xC	a
spoustu	spousta	k1gFnSc4	spousta
dalších	další	k2eAgFnPc2d1	další
divných	divný	k2eAgFnPc2d1	divná
věcí	věc	k1gFnPc2	věc
<g/>
:	:	kIx,	:
Johnny	Johnna	k1gFnPc1	Johnna
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ty	ten	k3xDgMnPc4	ten
můžeš	moct	k5eAaImIp2nS	moct
zachránit	zachránit	k5eAaPmF	zachránit
lidstvo	lidstvo	k1gNnSc1	lidstvo
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Only	Onla	k1gMnSc2	Onla
You	You	k1gMnSc2	You
Can	Can	k1gMnSc2	Can
Save	Sav	k1gMnSc2	Sav
Mankind	Mankind	k1gInSc4	Mankind
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc4	sci-fi
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
z	z	k7c2	z
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
stane	stanout	k5eAaPmIp3nS	stanout
skutečnost	skutečnost	k1gFnSc1	skutečnost
a	a	k8xC	a
hráči	hráč	k1gMnPc1	hráč
zjistí	zjistit	k5eAaPmIp3nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
virtuální	virtuální	k2eAgFnSc6d1	virtuální
střílečce	střílečka	k1gFnSc6	střílečka
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
živé	živý	k2eAgFnPc1d1	živá
bytosti	bytost	k1gFnPc1	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Johnny	Johnna	k1gFnPc1	Johnna
a	a	k8xC	a
bomba	bomba	k1gFnSc1	bomba
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Johnny	Johnna	k1gFnSc2	Johnna
and	and	k?	and
the	the	k?	the
Bomb	bomba	k1gFnPc2	bomba
Johnny	Johnna	k1gFnPc4	Johnna
a	a	k8xC	a
mrtví	mrtvý	k1gMnPc1	mrtvý
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Johnny	Johnna	k1gFnSc2	Johnna
and	and	k?	and
the	the	k?	the
Dead	Deada	k1gFnPc2	Deada
Nefalšovaná	falšovaný	k2eNgFnSc1d1	nefalšovaná
kočka	kočka	k1gFnSc1	kočka
-	-	kIx~	-
humorný	humorný	k2eAgInSc1d1	humorný
úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
kočičí	kočičí	k2eAgFnSc2d1	kočičí
povahy	povaha	k1gFnSc2	povaha
Národ	národ	k1gInSc1	národ
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Nation	Nation	k1gInSc1	Nation
Filuta	filuta	k1gMnSc1	filuta
-	-	kIx~	-
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Dodger	Dodgra	k1gFnPc2	Dodgra
</s>
