<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
polednem	poledne	k1gNnSc7	poledne
a	a	k8xC	a
večerem	večer	k1gInSc7	večer
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
slovníky	slovník	k1gInPc1	slovník
definují	definovat	k5eAaBmIp3nP	definovat
odpoledne	odpoledne	k6eAd1	odpoledne
jako	jako	k9	jako
"	"	kIx"	"
<g/>
část	část	k1gFnSc4	část
dne	den	k1gInSc2	den
od	od	k7c2	od
poledne	poledne	k1gNnSc2	poledne
do	do	k7c2	do
podvečera	podvečer	k1gInSc2	podvečer
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
večera	večer	k1gInSc2	večer
<g/>
,	,	kIx,	,
západu	západ	k1gInSc2	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
také	také	k9	také
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
do	do	k7c2	do
páté	pátá	k1gFnSc2	pátá
<g/>
.	.	kIx.	.
</s>
<s>
Příruční	příruční	k2eAgInSc1d1	příruční
slovník	slovník	k1gInSc1	slovník
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
:	:	kIx,	:
Školní	školní	k2eAgNnSc1d1	školní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Trávníček	Trávníček	k1gMnSc1	Trávníček
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Slovanské	slovanský	k2eAgNnSc1d1	slovanské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Havránek	Havránek	k1gMnSc1	Havránek
<g/>
,	,	kIx,	,
B.	B.	kA	B.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Filipec	Filipec	k1gMnSc1	Filipec
<g/>
,	,	kIx,	,
J.	J.	kA	J.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
pro	pro	k7c4	pro
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Faunovo	Faunův	k2eAgNnSc1d1	Faunův
velmi	velmi	k6eAd1	velmi
pozdní	pozdní	k2eAgMnSc1d1	pozdní
odpoledne	odpoledne	k6eAd1	odpoledne
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Odpoledne	odpoledne	k1gNnSc2	odpoledne
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
odpoledne	odpoledne	k1gNnSc2	odpoledne
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
