<s>
Teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Slunce	slunce	k1gNnSc2	slunce
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
5	[number]	k4	5
800	[number]	k4	800
K	K	kA	K
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	on	k3xPp3gMnPc4	on
lidé	člověk	k1gMnPc1	člověk
vnímají	vnímat	k5eAaImIp3nP	vnímat
jako	jako	k9	jako
žluté	žlutý	k2eAgFnPc1d1	žlutá
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
maximum	maximum	k1gNnSc1	maximum
jeho	on	k3xPp3gNnSc2	on
vyzařování	vyzařování	k1gNnSc2	vyzařování
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
části	část	k1gFnSc6	část
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
