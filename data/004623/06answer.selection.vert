<s>
Paměť	paměť	k1gFnSc1	paměť
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
jednak	jednak	k8xC	jednak
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
doby	doba	k1gFnSc2	doba
uchování	uchování	k1gNnSc2	uchování
zapamatovaného	zapamatovaný	k2eAgNnSc2d1	zapamatované
na	na	k7c4	na
senzorickou	senzorický	k2eAgFnSc4d1	senzorická
<g/>
,	,	kIx,	,
krátkodobou	krátkodobý	k2eAgFnSc4d1	krátkodobá
<g/>
,	,	kIx,	,
střednědobou	střednědobý	k2eAgFnSc4d1	střednědobá
a	a	k8xC	a
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
formy	forma	k1gFnSc2	forma
ukládání	ukládání	k1gNnSc2	ukládání
informací	informace	k1gFnPc2	informace
na	na	k7c4	na
vizuální	vizuální	k2eAgFnSc4d1	vizuální
<g/>
,	,	kIx,	,
akustickou	akustický	k2eAgFnSc4d1	akustická
<g/>
,	,	kIx,	,
sémantickou	sémantický	k2eAgFnSc4d1	sémantická
(	(	kIx(	(
<g/>
ukládání	ukládání	k1gNnSc3	ukládání
významu	význam	k1gInSc2	význam
informace	informace	k1gFnSc2	informace
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
