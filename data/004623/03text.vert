<s>
Paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
uchovávat	uchovávat	k5eAaImF	uchovávat
a	a	k8xC	a
používat	používat	k5eAaImF	používat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
předchozích	předchozí	k2eAgFnPc6d1	předchozí
zkušenostech	zkušenost	k1gFnPc6	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
proces	proces	k1gInSc4	proces
vštěpování	vštěpování	k1gNnSc2	vštěpování
(	(	kIx(	(
<g/>
kódování	kódování	k1gNnSc1	kódování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uchovávání	uchovávání	k1gNnSc1	uchovávání
(	(	kIx(	(
<g/>
retence	retence	k1gFnSc1	retence
<g/>
)	)	kIx)	)
a	a	k8xC	a
vybavování	vybavování	k1gNnSc1	vybavování
(	(	kIx(	(
<g/>
reprodukce	reprodukce	k1gFnSc1	reprodukce
<g/>
)	)	kIx)	)
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Paměť	paměť	k1gFnSc1	paměť
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
jednak	jednak	k8xC	jednak
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
doby	doba	k1gFnSc2	doba
uchování	uchování	k1gNnSc2	uchování
zapamatovaného	zapamatovaný	k2eAgNnSc2d1	zapamatované
na	na	k7c4	na
senzorickou	senzorický	k2eAgFnSc4d1	senzorická
<g/>
,	,	kIx,	,
krátkodobou	krátkodobý	k2eAgFnSc4d1	krátkodobá
<g/>
,	,	kIx,	,
střednědobou	střednědobý	k2eAgFnSc4d1	střednědobá
a	a	k8xC	a
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
formy	forma	k1gFnSc2	forma
ukládání	ukládání	k1gNnSc2	ukládání
informací	informace	k1gFnPc2	informace
na	na	k7c4	na
vizuální	vizuální	k2eAgFnSc4d1	vizuální
<g/>
,	,	kIx,	,
akustickou	akustický	k2eAgFnSc4d1	akustická
<g/>
,	,	kIx,	,
sémantickou	sémantický	k2eAgFnSc4d1	sémantická
(	(	kIx(	(
<g/>
ukládání	ukládání	k1gNnSc3	ukládání
významu	význam	k1gInSc2	význam
informace	informace	k1gFnSc2	informace
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
paměti	paměť	k1gFnSc2	paměť
na	na	k7c4	na
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
a	a	k8xC	a
logickou	logický	k2eAgFnSc4d1	logická
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc4	informace
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
ukládáme	ukládat	k5eAaImIp1nP	ukládat
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vizuální	vizuální	k2eAgFnPc4d1	vizuální
<g/>
,	,	kIx,	,
akustické	akustický	k2eAgFnPc4d1	akustická
<g/>
,	,	kIx,	,
sémantické	sémantický	k2eAgFnPc4d1	sémantická
<g/>
.	.	kIx.	.
</s>
<s>
Akustická	akustický	k2eAgFnSc1d1	akustická
forma	forma	k1gFnSc1	forma
bývá	bývat	k5eAaImIp3nS	bývat
při	při	k7c6	při
učení	učení	k1gNnSc6	učení
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
než	než	k8xS	než
vizuální	vizuální	k2eAgMnSc1d1	vizuální
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zdaleka	zdaleka	k6eAd1	zdaleka
nejúčinnější	účinný	k2eAgNnSc4d3	nejúčinnější
je	být	k5eAaImIp3nS	být
zpracování	zpracování	k1gNnSc4	zpracování
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
trvalého	trvalý	k2eAgNnSc2d1	trvalé
vštípení	vštípení	k1gNnSc2	vštípení
informace	informace	k1gFnSc2	informace
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
schopnosti	schopnost	k1gFnSc3	schopnost
přesného	přesný	k2eAgInSc2d1	přesný
eidetického	eidetický	k2eAgInSc2d1	eidetický
obrazu	obraz	k1gInSc2	obraz
–	–	k?	–
obrazu	obraz	k1gInSc2	obraz
ukládaného	ukládaný	k2eAgInSc2d1	ukládaný
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
–	–	k?	–
výhodnější	výhodný	k2eAgFnSc1d2	výhodnější
vizuální	vizuální	k2eAgFnSc1d1	vizuální
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
vynikají	vynikat	k5eAaImIp3nP	vynikat
v	v	k7c6	v
hrách	hra	k1gFnPc6	hra
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
pexeso	pexeso	k1gNnSc4	pexeso
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
zapamatování	zapamatování	k1gNnSc3	zapamatování
a	a	k8xC	a
uchovávání	uchovávání	k1gNnSc3	uchovávání
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
používáme	používat	k5eAaImIp1nP	používat
různé	různý	k2eAgFnPc4d1	různá
paměťové	paměťový	k2eAgFnPc4d1	paměťová
strategie	strategie	k1gFnPc4	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Prostým	prostý	k2eAgNnSc7d1	prosté
opakováním	opakování	k1gNnSc7	opakování
činnosti	činnost	k1gFnSc2	činnost
využíváme	využívat	k5eAaImIp1nP	využívat
tzv.	tzv.	kA	tzv.
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
uspořádáme	uspořádat	k5eAaPmIp1nP	uspořádat
<g/>
-li	i	k?	-li
informace	informace	k1gFnPc4	informace
do	do	k7c2	do
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zapamatování	zapamatování	k1gNnSc3	zapamatování
použijeme	použít	k5eAaPmIp1nP	použít
logickou	logický	k2eAgFnSc4d1	logická
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
účinnost	účinnost	k1gFnSc1	účinnost
mechanické	mechanický	k2eAgFnSc2d1	mechanická
paměti	paměť	k1gFnSc2	paměť
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
logická	logický	k2eAgFnSc1d1	logická
paměť	paměť	k1gFnSc1	paměť
se	se	k3xPyFc4	se
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
nastřádaných	nastřádaný	k2eAgFnPc2d1	nastřádaná
informací	informace	k1gFnPc2	informace
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
vhodné	vhodný	k2eAgNnSc1d1	vhodné
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
výukou	výuka	k1gFnSc7	výuka
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ta	ten	k3xDgFnSc1	ten
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c4	na
schopnosti	schopnost	k1gFnPc4	schopnost
pojmout	pojmout	k5eAaPmF	pojmout
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
informací	informace	k1gFnSc7	informace
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
pamětí	paměť	k1gFnSc7	paměť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ale	ale	k9	ale
také	také	k9	také
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
částečné	částečný	k2eAgNnSc4d1	částečné
zkreslování	zkreslování	k1gNnSc4	zkreslování
vnímání	vnímání	k1gNnSc2	vnímání
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dřívější	dřívější	k2eAgFnSc2d1	dřívější
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
např.	např.	kA	např.
více	hodně	k6eAd2	hodně
všímají	všímat	k5eAaImIp3nP	všímat
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
jejich	jejich	k3xOp3gInPc4	jejich
dosavadní	dosavadní	k2eAgInPc4d1	dosavadní
názory	názor	k1gInPc4	názor
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
konfirmační	konfirmační	k2eAgNnSc1d1	konfirmační
zkreslení	zkreslení	k1gNnSc1	zkreslení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zapamatování	zapamatování	k1gNnSc4	zapamatování
nových	nový	k2eAgFnPc2d1	nová
informací	informace	k1gFnPc2	informace
je	být	k5eAaImIp3nS	být
také	také	k9	také
klíčový	klíčový	k2eAgInSc1d1	klíčový
spánek	spánek	k1gInSc1	spánek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
konsolidaci	konsolidace	k1gFnSc3	konsolidace
(	(	kIx(	(
<g/>
integraci	integrace	k1gFnSc3	integrace
do	do	k7c2	do
již	již	k6eAd1	již
existujících	existující	k2eAgNnPc2d1	existující
schémat	schéma	k1gNnPc2	schéma
<g/>
)	)	kIx)	)
nových	nový	k2eAgFnPc2d1	nová
paměťových	paměťový	k2eAgFnPc2d1	paměťová
stop	stopa	k1gFnPc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uchování	uchování	k1gNnSc4	uchování
informací	informace	k1gFnPc2	informace
je	být	k5eAaImIp3nS	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
informace	informace	k1gFnPc4	informace
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
smysluplném	smysluplný	k2eAgInSc6d1	smysluplný
celku	celek	k1gInSc6	celek
<g/>
.	.	kIx.	.
</s>
<s>
Snadněji	snadno	k6eAd2	snadno
vybavitelné	vybavitelný	k2eAgFnPc1d1	vybavitelná
a	a	k8xC	a
reprodukovatelné	reprodukovatelný	k2eAgFnPc1d1	reprodukovatelná
jsou	být	k5eAaImIp3nP	být
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc1	význam
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
motivací	motivace	k1gFnSc7	motivace
<g/>
,	,	kIx,	,
osobními	osobní	k2eAgFnPc7d1	osobní
potřebami	potřeba	k1gFnPc7	potřeba
či	či	k8xC	či
spojením	spojení	k1gNnSc7	spojení
se	s	k7c7	s
silnějším	silný	k2eAgInSc7d2	silnější
citovým	citový	k2eAgInSc7d1	citový
zážitkem	zážitek	k1gInSc7	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
lépe	dobře	k6eAd2	dobře
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
učí	učit	k5eAaImIp3nS	učit
záměrně	záměrně	k6eAd1	záměrně
systematicky	systematicky	k6eAd1	systematicky
<g/>
,	,	kIx,	,
promýšlením	promýšlení	k1gNnSc7	promýšlení
učiva	učivo	k1gNnSc2	učivo
a	a	k8xC	a
spojováním	spojování	k1gNnSc7	spojování
s	s	k7c7	s
příklady	příklad	k1gInPc7	příklad
a	a	k8xC	a
s	s	k7c7	s
praxí	praxe	k1gFnSc7	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
mechanicky	mechanicky	k6eAd1	mechanicky
vštípené	vštípený	k2eAgFnPc4d1	vštípená
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
rychleji	rychle	k6eAd2	rychle
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
vybavit	vybavit	k5eAaPmF	vybavit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dekódovat	dekódovat	k5eAaBmF	dekódovat
<g/>
.	.	kIx.	.
</s>
<s>
Zapomínání	zapomínání	k1gNnSc1	zapomínání
samotné	samotný	k2eAgNnSc1d1	samotné
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
vyhasínáním	vyhasínání	k1gNnSc7	vyhasínání
nervového	nervový	k2eAgNnSc2d1	nervové
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nedostatečně	dostatečně	k6eNd1	dostatečně
zařazené	zařazený	k2eAgFnSc2d1	zařazená
informace	informace	k1gFnSc2	informace
probíhá	probíhat	k5eAaImIp3nS	probíhat
vytěsnění	vytěsnění	k1gNnSc1	vytěsnění
do	do	k7c2	do
nevědomí	nevědomí	k1gNnSc2	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumem	výzkum	k1gInSc7	výzkum
zapomínání	zapomínání	k1gNnSc2	zapomínání
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
německý	německý	k2eAgMnSc1d1	německý
psycholog	psycholog	k1gMnSc1	psycholog
Ebbinghaus	Ebbinghaus	k1gMnSc1	Ebbinghaus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
bádání	bádání	k1gNnSc4	bádání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
bezesmyslových	bezesmyslový	k2eAgFnPc2d1	bezesmyslový
slabik	slabika	k1gFnPc2	slabika
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
tzv.	tzv.	kA	tzv.
Ebbinghausovy	Ebbinghausův	k2eAgFnPc4d1	Ebbinghausův
křivky	křivka	k1gFnPc4	křivka
zapomínání	zapomínání	k1gNnSc2	zapomínání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
nejvíce	nejvíce	k6eAd1	nejvíce
zapomínáme	zapomínat	k5eAaImIp1nP	zapomínat
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
hodinách	hodina	k1gFnPc6	hodina
po	po	k7c6	po
naučení	naučení	k1gNnSc6	naučení
se	se	k3xPyFc4	se
něčemu	něco	k3yInSc3	něco
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
množství	množství	k1gNnSc1	množství
zapomenutého	zapomenutý	k2eAgNnSc2d1	zapomenuté
po	po	k7c6	po
5	[number]	k4	5
dnech	den	k1gInPc6	den
a	a	k8xC	a
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
se	se	k3xPyFc4	se
už	už	k6eAd1	už
liší	lišit	k5eAaImIp3nS	lišit
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
<g/>
:	:	kIx,	:
10	[number]	k4	10
%	%	kIx~	%
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
čtou	číst	k5eAaImIp3nP	číst
<g/>
,	,	kIx,	,
20	[number]	k4	20
%	%	kIx~	%
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
slyší	slyšet	k5eAaImIp3nP	slyšet
<g/>
,	,	kIx,	,
30	[number]	k4	30
%	%	kIx~	%
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
50	[number]	k4	50
%	%	kIx~	%
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
slyší	slyšet	k5eAaImIp3nS	slyšet
a	a	k8xC	a
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
70	[number]	k4	70
%	%	kIx~	%
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
řeknou	říct	k5eAaPmIp3nP	říct
<g/>
,	,	kIx,	,
a	a	k8xC	a
90	[number]	k4	90
%	%	kIx~	%
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dělají	dělat	k5eAaImIp3nP	dělat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vybavení	vybavení	k1gNnSc3	vybavení
používáme	používat	k5eAaImIp1nP	používat
asociace	asociace	k1gFnPc4	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
:	:	kIx,	:
znovupoznání	znovupoznání	k1gNnSc2	znovupoznání
(	(	kIx(	(
<g/>
rekognice	rekognice	k1gFnSc2	rekognice
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
rozpoznání	rozpoznání	k1gNnSc3	rozpoznání
podnětů	podnět	k1gInPc2	podnět
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
odlišení	odlišení	k1gNnPc2	odlišení
od	od	k7c2	od
podnětů	podnět	k1gInPc2	podnět
nových	nový	k2eAgInPc2d1	nový
a	a	k8xC	a
neznámých	neznámá	k1gFnPc2	neznámá
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
:	:	kIx,	:
proces	proces	k1gInSc4	proces
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
zapamatovaného	zapamatovaný	k2eAgNnSc2d1	zapamatované
<g/>
.	.	kIx.	.
</s>
<s>
Reprodukce	reprodukce	k1gFnSc1	reprodukce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nepřesná	přesný	k2eNgFnSc1d1	nepřesná
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
tendence	tendence	k1gFnSc1	tendence
svoje	svůj	k3xOyFgFnPc4	svůj
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
doplňovat	doplňovat	k5eAaImF	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
senzorická	senzorický	k2eAgFnSc1d1	senzorická
<g/>
,	,	kIx,	,
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
a	a	k8xC	a
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
paměť	paměť	k1gFnSc1	paměť
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
řadí	řadit	k5eAaImIp3nS	řadit
ještě	ještě	k9	ještě
paměť	paměť	k1gFnSc1	paměť
střednědobá	střednědobý	k2eAgFnSc1d1	střednědobá
<g/>
.	.	kIx.	.
</s>
<s>
Senzorická	senzorický	k2eAgFnSc1d1	senzorická
(	(	kIx(	(
<g/>
ultrakrátká	ultrakrátký	k2eAgFnSc1d1	ultrakrátká
<g/>
)	)	kIx)	)
paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
vědomá	vědomý	k2eAgFnSc1d1	vědomá
část	část	k1gFnSc1	část
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
informace	informace	k1gFnSc1	informace
přicházející	přicházející	k2eAgFnSc1d1	přicházející
ze	z	k7c2	z
smyslů	smysl	k1gInPc2	smysl
(	(	kIx(	(
<g/>
člověku	člověk	k1gMnSc6	člověk
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
před	před	k7c7	před
chvilkou	chvilka	k1gFnSc7	chvilka
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
takzvaně	takzvaně	k6eAd1	takzvaně
"	"	kIx"	"
<g/>
zůstane	zůstat	k5eAaPmIp3nS	zůstat
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
podrženy	podržet	k5eAaPmNgInP	podržet
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
nezbytně	nezbytně	k6eAd1	nezbytně
nutnou	nutný	k2eAgFnSc4d1	nutná
ke	k	k7c3	k
zpracování	zpracování	k1gNnSc3	zpracování
a	a	k8xC	a
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
informace	informace	k1gFnPc4	informace
důležité	důležitý	k2eAgFnPc4d1	důležitá
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vhodné	vhodný	k2eAgInPc1d1	vhodný
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
zpracování	zpracování	k1gNnSc3	zpracování
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
postupují	postupovat	k5eAaImIp3nP	postupovat
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
krátkodobé	krátkodobý	k2eAgFnSc2d1	krátkodobá
či	či	k8xC	či
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
paměť	paměť	k1gFnSc1	paměť
pracovní	pracovní	k2eAgFnSc1d1	pracovní
<g/>
,	,	kIx,	,
operativní	operativní	k2eAgFnSc1d1	operativní
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědomá	vědomý	k2eAgFnSc1d1	vědomá
aktivní	aktivní	k2eAgFnSc1d1	aktivní
část	část	k1gFnSc1	část
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
většina	většina	k1gFnSc1	většina
psychických	psychický	k2eAgInPc2d1	psychický
procesů	proces	k1gInPc2	proces
(	(	kIx(	(
<g/>
např.	např.	kA	např.
řešení	řešení	k1gNnSc1	řešení
aktuálních	aktuální	k2eAgInPc2d1	aktuální
problémů	problém	k1gInPc2	problém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
informace	informace	k1gFnSc1	informace
dodané	dodaný	k2eAgFnSc2d1	dodaná
senzorickou	senzorický	k2eAgFnSc4d1	senzorická
pamětí	paměť	k1gFnSc7	paměť
a	a	k8xC	a
informace	informace	k1gFnPc4	informace
vyvolané	vyvolaný	k2eAgFnPc4d1	vyvolaná
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
dostupná	dostupný	k2eAgNnPc4d1	dostupné
vědomě	vědomě	k6eAd1	vědomě
<g/>
.	.	kIx.	.
</s>
<s>
Krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
paměť	paměť	k1gFnSc1	paměť
dokáže	dokázat	k5eAaPmIp3nS	dokázat
uchovat	uchovat	k5eAaPmF	uchovat
vjemy	vjem	k1gInPc4	vjem
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
emoce	emoce	k1gFnPc1	emoce
pomocí	pomocí	k7c2	pomocí
přeměny	přeměna	k1gFnSc2	přeměna
(	(	kIx(	(
<g/>
kódování	kódování	k1gNnSc1	kódování
<g/>
)	)	kIx)	)
v	v	k7c4	v
mentální	mentální	k2eAgFnPc4d1	mentální
reprezentace	reprezentace	k1gFnPc4	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
může	moct	k5eAaImIp3nS	moct
paměť	paměť	k1gFnSc4	paměť
dále	daleko	k6eAd2	daleko
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
a	a	k8xC	a
uchovávat	uchovávat	k5eAaImF	uchovávat
<g/>
.	.	kIx.	.
</s>
<s>
Krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
magické	magický	k2eAgNnSc1d1	magické
číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
<g/>
±	±	k?	±
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
při	při	k7c6	při
zamezení	zamezení	k1gNnSc6	zamezení
opakování	opakování	k1gNnSc2	opakování
uchová	uchovat	k5eAaPmIp3nS	uchovat
na	na	k7c4	na
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Kapacitu	kapacita	k1gFnSc4	kapacita
lze	lze	k6eAd1	lze
zvýšit	zvýšit	k5eAaPmF	zvýšit
spojováním	spojování	k1gNnSc7	spojování
prvků	prvek	k1gInPc2	prvek
do	do	k7c2	do
logických	logický	k2eAgInPc2d1	logický
celků	celek	k1gInPc2	celek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mnemotechnické	mnemotechnický	k2eAgFnPc4d1	mnemotechnická
pomůcky	pomůcka	k1gFnPc4	pomůcka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
informace	informace	k1gFnSc2	informace
v	v	k7c6	v
krátkodobé	krátkodobý	k2eAgFnSc6d1	krátkodobá
paměti	paměť	k1gFnSc6	paměť
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
informaci	informace	k1gFnSc4	informace
opakovat	opakovat	k5eAaImF	opakovat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fonologická	fonologický	k2eAgFnSc1d1	fonologická
smyčka	smyčka	k1gFnSc1	smyčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
paměťová	paměťový	k2eAgFnSc1d1	paměťová
stopa	stopa	k1gFnSc1	stopa
nenávratně	návratně	k6eNd1	návratně
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Tříjednotkový	tříjednotkový	k2eAgInSc1d1	tříjednotkový
model	model	k1gInSc1	model
krátkodobé	krátkodobý	k2eAgFnSc2d1	krátkodobá
paměti	paměť	k1gFnSc2	paměť
udává	udávat	k5eAaImIp3nS	udávat
3	[number]	k4	3
mechanismy	mechanismus	k1gInPc1	mechanismus
zpracování	zpracování	k1gNnSc1	zpracování
<g/>
:	:	kIx,	:
fonologická	fonologický	k2eAgFnSc1d1	fonologická
smyčka	smyčka	k1gFnSc1	smyčka
–	–	k?	–
dočasně	dočasně	k6eAd1	dočasně
ukládá	ukládat	k5eAaImIp3nS	ukládat
zvukové	zvukový	k2eAgFnPc4d1	zvuková
a	a	k8xC	a
řečové	řečový	k2eAgFnPc4d1	řečová
informace	informace	k1gFnPc4	informace
vizuoprostorový	vizuoprostorový	k2eAgInSc1d1	vizuoprostorový
náčrtník	náčrtník	k1gInSc1	náčrtník
–	–	k?	–
dočasně	dočasně	k6eAd1	dočasně
ukládá	ukládat	k5eAaImIp3nS	ukládat
vizuálně	vizuálně	k6eAd1	vizuálně
prostorové	prostorový	k2eAgFnPc4d1	prostorová
informace	informace	k1gFnPc4	informace
centrální	centrální	k2eAgFnSc1d1	centrální
výkonnostní	výkonnostní	k2eAgFnSc1d1	výkonnostní
smyčka	smyčka	k1gFnSc1	smyčka
–	–	k?	–
třídí	třídit	k5eAaImIp3nS	třídit
a	a	k8xC	a
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
krátkodobé	krátkodobý	k2eAgFnPc4d1	krátkodobá
informace	informace	k1gFnPc4	informace
Dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
pasivní	pasivní	k2eAgFnSc1d1	pasivní
část	část	k1gFnSc1	část
paměti	paměť	k1gFnSc2	paměť
uchovávaná	uchovávaný	k2eAgFnSc1d1	uchovávaná
v	v	k7c6	v
nevědomí	nevědomí	k1gNnSc6	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
hypoteticky	hypoteticky	k6eAd1	hypoteticky
neomezená	omezený	k2eNgFnSc1d1	neomezená
<g/>
.	.	kIx.	.
</s>
<s>
Ukládá	ukládat	k5eAaImIp3nS	ukládat
významné	významný	k2eAgFnPc4d1	významná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
poznatky	poznatek	k1gInPc1	poznatek
nutné	nutný	k2eAgInPc1d1	nutný
k	k	k7c3	k
vykonávání	vykonávání	k1gNnSc3	vykonávání
nějaké	nějaký	k3yIgFnSc2	nějaký
činnosti	činnost	k1gFnSc2	činnost
či	či	k8xC	či
poznatky	poznatek	k1gInPc1	poznatek
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgInPc1d1	důležitý
<g/>
.	.	kIx.	.
</s>
<s>
Vštěpování	vštěpování	k1gNnSc1	vštěpování
informace	informace	k1gFnSc2	informace
do	do	k7c2	do
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
paměti	paměť	k1gFnSc2	paměť
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
buďto	buďto	k8xC	buďto
záměrně	záměrně	k6eAd1	záměrně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
opakováním	opakování	k1gNnSc7	opakování
–	–	k?	–
memorováním	memorování	k1gNnSc7	memorování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bezděčně	bezděčně	k6eAd1	bezděčně
<g/>
.	.	kIx.	.
</s>
<s>
Lépe	dobře	k6eAd2	dobře
zapamatovatelné	zapamatovatelný	k2eAgInPc1d1	zapamatovatelný
jsou	být	k5eAaImIp3nP	být
smysluplné	smysluplný	k2eAgInPc1d1	smysluplný
obsahy	obsah	k1gInPc1	obsah
a	a	k8xC	a
logické	logický	k2eAgInPc1d1	logický
celky	celek	k1gInPc1	celek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
zkreslování	zkreslování	k1gNnSc3	zkreslování
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
začleňování	začleňování	k1gNnSc6	začleňování
nových	nový	k2eAgFnPc2d1	nová
informací	informace	k1gFnPc2	informace
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
proměňují	proměňovat	k5eAaImIp3nP	proměňovat
i	i	k9	i
stávající	stávající	k2eAgFnPc1d1	stávající
znalosti	znalost	k1gFnPc1	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
také	také	k9	také
pamatují	pamatovat	k5eAaImIp3nP	pamatovat
poznatky	poznatek	k1gInPc1	poznatek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
citový	citový	k2eAgInSc4d1	citový
nádech	nádech	k1gInSc4	nádech
a	a	k8xC	a
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vybavovány	vybavován	k2eAgInPc1d1	vybavován
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc4	takový
informace	informace	k1gFnPc4	informace
paměť	paměť	k1gFnSc1	paměť
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k8xC	i
nutné	nutný	k2eAgInPc1d1	nutný
k	k	k7c3	k
dlouhodobému	dlouhodobý	k2eAgNnSc3d1	dlouhodobé
uchování	uchování	k1gNnSc3	uchování
<g/>
.	.	kIx.	.
</s>
<s>
Biologicky	biologicky	k6eAd1	biologicky
staví	stavit	k5eAaImIp3nS	stavit
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
paměť	paměť	k1gFnSc1	paměť
na	na	k7c6	na
strukturální	strukturální	k2eAgFnSc6d1	strukturální
nebo	nebo	k8xC	nebo
molekulární	molekulární	k2eAgFnSc6d1	molekulární
změně	změna	k1gFnSc6	změna
dendritů	dendrit	k1gInPc2	dendrit
(	(	kIx(	(
<g/>
části	část	k1gFnSc2	část
neuronu	neuron	k1gInSc2	neuron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
mozek	mozek	k1gInSc1	mozek
si	se	k3xPyFc3	se
informace	informace	k1gFnSc1	informace
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
paměti	paměť	k1gFnSc2	paměť
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
velkou	velký	k2eAgFnSc7d1	velká
chybou	chyba	k1gFnSc7	chyba
mnoha	mnoho	k4c2	mnoho
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nezbytný	zbytný	k2eNgInSc1d1	zbytný
pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
látky	látka	k1gFnSc2	látka
do	do	k7c2	do
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
informace	informace	k1gFnPc1	informace
uložily	uložit	k5eAaPmAgFnP	uložit
do	do	k7c2	do
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
paměti	paměť	k1gFnSc2	paměť
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
přinejmenším	přinejmenším	k6eAd1	přinejmenším
6	[number]	k4	6
<g/>
hodinový	hodinový	k2eAgInSc1d1	hodinový
spánek	spánek	k1gInSc1	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
údajů	údaj	k1gInPc2	údaj
v	v	k7c6	v
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
paměti	paměť	k1gFnSc6	paměť
implicitní	implicitní	k2eAgFnSc6d1	implicitní
(	(	kIx(	(
<g/>
procedurální	procedurální	k2eAgFnSc6d1	procedurální
<g/>
)	)	kIx)	)
paměť	paměť	k1gFnSc4	paměť
–	–	k?	–
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zapamatování	zapamatování	k1gNnSc3	zapamatování
dovedností	dovednost	k1gFnPc2	dovednost
a	a	k8xC	a
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgFnPc2	jenž
lze	lze	k6eAd1	lze
tvořit	tvořit	k5eAaImF	tvořit
nové	nový	k2eAgInPc4d1	nový
smysluplné	smysluplný	k2eAgInPc4d1	smysluplný
celky	celek	k1gInPc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uložené	uložený	k2eAgInPc1d1	uložený
jsou	být	k5eAaImIp3nP	být
hůře	zle	k6eAd2	zle
verbálně	verbálně	k6eAd1	verbálně
vyjádřitelné	vyjádřitelný	k2eAgNnSc1d1	vyjádřitelné
<g/>
.	.	kIx.	.
</s>
<s>
Implicitní	implicitní	k2eAgFnSc1d1	implicitní
paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
vývojově	vývojově	k6eAd1	vývojově
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
odolnější	odolný	k2eAgMnSc1d2	odolnější
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
paměti	paměť	k1gFnSc6	paměť
explicitní	explicitní	k2eAgFnSc6d1	explicitní
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
případy	případ	k1gInPc4	případ
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
amnézií	amnézie	k1gFnSc7	amnézie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vštípení	vštípení	k1gNnSc1	vštípení
do	do	k7c2	do
implicitní	implicitní	k2eAgFnSc2d1	implicitní
paměti	paměť	k1gFnSc2	paměť
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
i	i	k9	i
nevědomě	vědomě	k6eNd1	vědomě
<g/>
.	.	kIx.	.
procedurální	procedurální	k2eAgFnSc1d1	procedurální
paměť	paměť	k1gFnSc1	paměť
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dovednosti	dovednost	k1gFnPc4	dovednost
<g/>
,	,	kIx,	,
postupy	postup	k1gInPc4	postup
a	a	k8xC	a
návyky	návyk	k1gInPc4	návyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jízda	jízda	k1gFnSc1	jízda
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
alokovaná	alokovaný	k2eAgFnSc1d1	alokovaná
v	v	k7c6	v
neuronální	neuronální	k2eAgFnSc6d1	neuronální
oblasti	oblast	k1gFnSc6	oblast
středního	střední	k2eAgInSc2d1	střední
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
nucleus	nucleus	k1gMnSc1	nucleus
caudatus	caudatus	k1gMnSc1	caudatus
a	a	k8xC	a
putamen	putamen	k2eAgMnSc1d1	putamen
<g/>
)	)	kIx)	)
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
klasické	klasický	k2eAgNnSc1d1	klasické
podmiňování	podmiňování	k1gNnSc1	podmiňování
–	–	k?	–
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
informace	informace	k1gFnPc4	informace
pomocí	pomocí	k7c2	pomocí
emočního	emoční	k2eAgNnSc2d1	emoční
podmiňování	podmiňování	k1gNnSc2	podmiňování
(	(	kIx(	(
<g/>
asociace	asociace	k1gFnSc1	asociace
mezi	mezi	k7c7	mezi
emocemi	emoce	k1gFnPc7	emoce
a	a	k8xC	a
vnějšími	vnější	k2eAgInPc7d1	vnější
vlivy	vliv	k1gInPc7	vliv
–	–	k?	–
pokus	pokus	k1gInSc4	pokus
Eduarda	Eduard	k1gMnSc2	Eduard
Claparè	Claparè	k1gFnSc2	Claparè
<g/>
)	)	kIx)	)
či	či	k8xC	či
pohybového	pohybový	k2eAgNnSc2d1	pohybové
podmiňování	podmiňování	k1gNnSc2	podmiňování
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
části	část	k1gFnSc2	část
mozku	mozek	k1gInSc2	mozek
amygdaly	amygdat	k5eAaPmAgInP	amygdat
a	a	k8xC	a
cerebella	cerebella	k1gFnSc1	cerebella
<g/>
.	.	kIx.	.
senzibilizace	senzibilizace	k1gFnSc1	senzibilizace
(	(	kIx(	(
<g/>
primming	primming	k1gInSc1	primming
<g/>
,	,	kIx,	,
roznětková	roznětkový	k2eAgFnSc1d1	roznětkový
paměť	paměť	k1gFnSc1	paměť
<g/>
)	)	kIx)	)
–	–	k?	–
zcitlivění	zcitlivění	k1gNnSc2	zcitlivění
vůči	vůči	k7c3	vůči
relativně	relativně	k6eAd1	relativně
novým	nový	k2eAgInPc3d1	nový
podnětům	podnět	k1gInPc3	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
podnětům	podnět	k1gInPc3	podnět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
i	i	k9	i
v	v	k7c6	v
obměněné	obměněný	k2eAgFnSc6d1	obměněná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
poznány	poznán	k2eAgFnPc1d1	poznána
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
vybavování	vybavování	k1gNnSc1	vybavování
pomocí	pomocí	k7c2	pomocí
nápovědy	nápověda	k1gFnSc2	nápověda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
senzorické	senzorický	k2eAgFnSc2d1	senzorická
oblasti	oblast	k1gFnSc2	oblast
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
(	(	kIx(	(
<g/>
neokortex	neokortex	k1gInSc1	neokortex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
neasociativní	asociativní	k2eNgNnSc1d1	asociativní
učení	učení	k1gNnSc1	učení
–	–	k?	–
reflexní	reflexní	k2eAgFnSc2d1	reflexní
reakce	reakce	k1gFnSc2	reakce
explicitní	explicitní	k2eAgFnSc1d1	explicitní
(	(	kIx(	(
<g/>
deklarativní	deklarativní	k2eAgFnSc1d1	deklarativní
<g/>
)	)	kIx)	)
paměť	paměť	k1gFnSc1	paměť
–	–	k?	–
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
a	a	k8xC	a
faktické	faktický	k2eAgFnPc4d1	faktická
znalosti	znalost	k1gFnPc4	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jsou	být	k5eAaImIp3nP	být
verbálně	verbálně	k6eAd1	verbálně
vyjádřitelné	vyjádřitelný	k2eAgFnPc1d1	vyjádřitelná
a	a	k8xC	a
před	před	k7c7	před
vštípením	vštípení	k1gNnSc7	vštípení
musí	muset	k5eAaImIp3nS	muset
projít	projít	k5eAaPmF	projít
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
.	.	kIx.	.
sémantická	sémantický	k2eAgFnSc1d1	sémantická
paměť	paměť	k1gFnSc1	paměť
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
obecná	obecná	k1gFnSc1	obecná
fakta	faktum	k1gNnSc2	faktum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pythagorova	Pythagorův	k2eAgFnSc1d1	Pythagorova
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
a	a	k8xC	a
času	čas	k1gInSc3	čas
vštípení	vštípení	k1gNnSc2	vštípení
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
informací	informace	k1gFnPc2	informace
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
probíhá	probíhat	k5eAaImIp3nS	probíhat
do	do	k7c2	do
postojů	postoj	k1gInPc2	postoj
<g/>
,	,	kIx,	,
schémat	schéma	k1gNnPc2	schéma
a	a	k8xC	a
scénářů	scénář	k1gInPc2	scénář
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sociální	sociální	k2eAgInSc1d1	sociální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
poté	poté	k6eAd1	poté
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
vštěpování	vštěpování	k1gNnSc4	vštěpování
informací	informace	k1gFnPc2	informace
nových	nový	k2eAgFnPc2d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Vybavování	vybavování	k1gNnSc1	vybavování
poznatků	poznatek	k1gInPc2	poznatek
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
nevědomě	vědomě	k6eNd1	vědomě
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
vnějším	vnější	k2eAgInSc7d1	vnější
situačním	situační	k2eAgInSc7d1	situační
kontextem	kontext	k1gInSc7	kontext
(	(	kIx(	(
<g/>
prostředí	prostředí	k1gNnSc1	prostředí
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zapamatování	zapamatování	k1gNnSc3	zapamatování
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
kontextem	kontext	k1gInSc7	kontext
(	(	kIx(	(
<g/>
nálada	nálada	k1gFnSc1	nálada
<g/>
,	,	kIx,	,
emoce	emoce	k1gFnPc1	emoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vědomosti	vědomost	k1gFnPc1	vědomost
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
lépe	dobře	k6eAd2	dobře
vybavují	vybavovat	k5eAaImIp3nP	vybavovat
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nacházíme	nacházet	k5eAaImIp1nP	nacházet
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
prostředí	prostředí	k1gNnSc6	prostředí
či	či	k8xC	či
emocionálním	emocionální	k2eAgInSc6d1	emocionální
stavu	stav	k1gInSc6	stav
jako	jako	k8xC	jako
při	při	k7c6	při
vštípení	vštípení	k1gNnSc6	vštípení
(	(	kIx(	(
<g/>
efekt	efekt	k1gInSc1	efekt
kongruence	kongruence	k1gFnSc2	kongruence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
epizodická	epizodický	k2eAgFnSc1d1	epizodická
paměť	paměť	k1gFnSc1	paměť
–	–	k?	–
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
rozměr	rozměr	k1gInSc4	rozměr
časový	časový	k2eAgInSc4d1	časový
<g/>
,	,	kIx,	,
prostorový	prostorový	k2eAgInSc4d1	prostorový
a	a	k8xC	a
citový	citový	k2eAgInSc4d1	citový
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
paměť	paměť	k1gFnSc1	paměť
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
osobní	osobní	k2eAgInPc4d1	osobní
zážitky	zážitek	k1gInPc4	zážitek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
doprovázené	doprovázený	k2eAgFnPc1d1	doprovázená
emocemi	emoce	k1gFnPc7	emoce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
schopnost	schopnost	k1gFnSc4	schopnost
znovuvybavení	znovuvybavení	k1gNnSc1	znovuvybavení
(	(	kIx(	(
<g/>
zábleskové	zábleskový	k2eAgFnPc1d1	záblesková
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
ještě	ještě	k9	ještě
střednědobá	střednědobý	k2eAgFnSc1d1	střednědobá
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
informaci	informace	k1gFnSc4	informace
udržet	udržet	k5eAaPmF	udržet
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
asi	asi	k9	asi
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
pro	pro	k7c4	pro
představivost	představivost	k1gFnSc4	představivost
–	–	k?	–
struna	struna	k1gFnSc1	struna
se	se	k3xPyFc4	se
rozechvívá	rozechvívat	k5eAaImIp3nS	rozechvívat
a	a	k8xC	a
řetízek	řetízek	k1gInSc1	řetízek
s	s	k7c7	s
informací	informace	k1gFnSc7	informace
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
stále	stále	k6eAd1	stále
hlubší	hluboký	k2eAgInSc4d2	hlubší
a	a	k8xC	a
hlubší	hluboký	k2eAgInSc4d2	hlubší
otisk	otisk	k1gInSc4	otisk
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
již	již	k6eAd1	již
struna	struna	k1gFnSc1	struna
nerozechvívá	rozechvívat	k5eNaImIp3nS	rozechvívat
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
dále	daleko	k6eAd2	daleko
nepotvrzujeme	potvrzovat	k5eNaImIp1nP	potvrzovat
paměťovou	paměťový	k2eAgFnSc4d1	paměťová
stopu	stopa	k1gFnSc4	stopa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
korálky	korálek	k1gInPc1	korálek
řetízku	řetízek	k1gInSc2	řetízek
se	se	k3xPyFc4	se
rozptýlí	rozptýlit	k5eAaPmIp3nS	rozptýlit
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
pokusu	pokus	k1gInSc6	pokus
o	o	k7c6	o
zachování	zachování	k1gNnSc6	zachování
informace	informace	k1gFnSc2	informace
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
seskupí	seskupit	k5eAaPmIp3nS	seskupit
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
řetízku	řetízek	k1gInSc2	řetízek
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
hlubší	hluboký	k2eAgFnSc4d2	hlubší
paměťovou	paměťový	k2eAgFnSc4d1	paměťová
stopu	stopa	k1gFnSc4	stopa
<g/>
.	.	kIx.	.
</s>
