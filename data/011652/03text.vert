<p>
<s>
Ploskovská	Ploskovský	k2eAgFnSc1d1	Ploskovská
kaštanka	kaštanka	k1gFnSc1	kaštanka
je	být	k5eAaImIp3nS	být
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
chráněné	chráněný	k2eAgNnSc4d1	chráněné
stromořadí	stromořadí	k1gNnSc4	stromořadí
téměř	téměř	k6eAd1	téměř
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
jírovců	jírovec	k1gInPc2	jírovec
a	a	k8xC	a
lip	lípa	k1gFnPc2	lípa
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Lhota	Lhota	k1gFnSc1	Lhota
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
okresu	okres	k1gInSc2	okres
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
uskupení	uskupení	k1gNnSc4	uskupení
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
na	na	k7c6	na
území	území	k1gNnSc6	území
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Křivoklátsko	Křivoklátsko	k1gNnSc1	Křivoklátsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Alej	alej	k1gFnSc1	alej
se	se	k3xPyFc4	se
prostírá	prostírat	k5eAaImIp3nS	prostírat
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
osady	osada	k1gFnSc2	osada
Ploskov	Ploskov	k1gInSc1	Ploskov
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
od	od	k7c2	od
420	[number]	k4	420
do	do	k7c2	do
448	[number]	k4	448
m.	m.	k?	m.
Tvarem	tvar	k1gInSc7	tvar
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
řadu	řada	k1gFnSc4	řada
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
alej	alej	k1gFnSc1	alej
připomíná	připomínat	k5eAaImIp3nS	připomínat
nepravidelné	pravidelný	k2eNgNnSc4d1	nepravidelné
převrácené	převrácený	k2eAgNnSc4d1	převrácené
písmeno	písmeno	k1gNnSc4	písmeno
Y	Y	kA	Y
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
nohu	noha	k1gFnSc4	noha
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgNnSc4d1	jihozápadní
rameno	rameno	k1gNnSc4	rameno
představuje	představovat	k5eAaImIp3nS	představovat
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
116	[number]	k4	116
(	(	kIx(	(
<g/>
Lány	lán	k1gInPc1	lán
–	–	k?	–
Nižbor	Nižbor	k1gInSc1	Nižbor
<g/>
)	)	kIx)	)
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
<g/>
,	,	kIx,	,
začínajícím	začínající	k2eAgNnSc6d1	začínající
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
sousedství	sousedství	k1gNnSc6	sousedství
Ploskova	Ploskův	k2eAgFnSc1d1	Ploskův
a	a	k8xC	a
táhnoucím	táhnoucí	k2eAgMnPc3d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
Ploskova	Ploskův	k2eAgNnSc2d1	Ploskův
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
se	se	k3xPyFc4	se
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
bodě	bod	k1gInSc6	bod
(	(	kIx(	(
<g/>
východní	východní	k2eAgNnSc1d1	východní
úbočí	úbočí	k1gNnSc1	úbočí
vrchu	vrch	k1gInSc2	vrch
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
Mýto	mýto	k1gNnSc1	mýto
<g/>
)	)	kIx)	)
alej	alej	k1gFnSc1	alej
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
rameno	rameno	k1gNnSc4	rameno
pomyslného	pomyslný	k2eAgNnSc2d1	pomyslné
Y	Y	kA	Y
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sleduje	sledovat	k5eAaImIp3nS	sledovat
širokou	široký	k2eAgFnSc4d1	široká
cestu	cesta	k1gFnSc4	cesta
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c6	na
Bratronice	Bratronika	k1gFnSc6	Bratronika
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
stromořadí	stromořadí	k1gNnSc2	stromořadí
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
2,7	[number]	k4	2,7
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Ploskovská	Ploskovský	k2eAgFnSc1d1	Ploskovská
kaštanka	kaštanka	k1gFnSc1	kaštanka
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
majitelů	majitel	k1gMnPc2	majitel
křivoklátského	křivoklátský	k2eAgNnSc2d1	Křivoklátské
panství	panství	k1gNnSc2	panství
Fürstenberků	Fürstenberka	k1gMnPc2	Fürstenberka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
ji	on	k3xPp3gFnSc4	on
správa	správa	k1gFnSc1	správa
CHKO	CHKO	kA	CHKO
Křivoklátsko	Křivoklátsko	k1gNnSc4	Křivoklátsko
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
památné	památný	k2eAgNnSc4d1	památné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
zvláště	zvláště	k6eAd1	zvláště
chráněné	chráněný	k2eAgInPc4d1	chráněný
stromy	strom	k1gInPc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
bylo	být	k5eAaImAgNnS	být
druhové	druhový	k2eAgNnSc4d1	druhové
složení	složení	k1gNnSc4	složení
aleje	alej	k1gFnSc2	alej
tvořené	tvořený	k2eAgFnSc2d1	tvořená
400	[number]	k4	400
stromy	strom	k1gInPc1	strom
následující	následující	k2eAgInPc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jírovec	jírovec	k1gInSc1	jírovec
maďal	maďal	k1gInSc1	maďal
(	(	kIx(	(
<g/>
Aesculus	Aesculus	k1gInSc1	Aesculus
hippocastanum	hippocastanum	k1gNnSc1	hippocastanum
<g/>
)	)	kIx)	)
327	[number]	k4	327
ks	ks	kA	ks
</s>
</p>
<p>
<s>
lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
cordata	cordata	k1gFnSc1	cordata
<g/>
)	)	kIx)	)
63	[number]	k4	63
ks	ks	kA	ks
</s>
</p>
<p>
<s>
jeřáb	jeřáb	k1gInSc1	jeřáb
břek	břek	k1gInSc1	břek
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
torminalis	torminalis	k1gFnSc2	torminalis
<g/>
)	)	kIx)	)
4	[number]	k4	4
ks	ks	kA	ks
</s>
</p>
<p>
<s>
olše	olše	k1gFnSc1	olše
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
(	(	kIx(	(
<g/>
Alnus	Alnus	k1gMnSc1	Alnus
glutinosa	glutinosa	k1gFnSc1	glutinosa
<g/>
)	)	kIx)	)
3	[number]	k4	3
ks	ks	kA	ks
</s>
</p>
<p>
<s>
dub	dub	k1gInSc1	dub
zimní	zimní	k2eAgInSc1d1	zimní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
petraea	petrae	k1gInSc2	petrae
<g/>
)	)	kIx)	)
3	[number]	k4	3
ksStáří	ksStáří	k2eAgFnSc2d1	ksStáří
většiny	většina	k1gFnSc2	většina
stromů	strom	k1gInPc2	strom
přesahovalo	přesahovat	k5eAaImAgNnS	přesahovat
90	[number]	k4	90
až	až	k8xS	až
150	[number]	k4	150
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
obvody	obvod	k1gInPc1	obvod
jejich	jejich	k3xOp3gInPc2	jejich
kmenů	kmen	k1gInPc2	kmen
měřily	měřit	k5eAaImAgFnP	měřit
od	od	k7c2	od
150	[number]	k4	150
do	do	k7c2	do
300	[number]	k4	300
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
až	až	k9	až
20	[number]	k4	20
m	m	kA	m
<g/>
;	;	kIx,	;
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
probíhá	probíhat	k5eAaImIp3nS	probíhat
celkové	celkový	k2eAgNnSc4d1	celkové
ošetřování	ošetřování	k1gNnSc4	ošetřování
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
nová	nový	k2eAgFnSc1d1	nová
výsadba	výsadba	k1gFnSc1	výsadba
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
chybějících	chybějící	k2eAgInPc2d1	chybějící
nebo	nebo	k8xC	nebo
sešlých	sešlý	k2eAgInPc2d1	sešlý
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Během	během	k7c2	během
revize	revize	k1gFnSc2	revize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stav	stav	k1gInSc1	stav
chráněné	chráněný	k2eAgFnSc2d1	chráněná
aleje	alej	k1gFnSc2	alej
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
vyhlašovací	vyhlašovací	k2eAgFnSc4d1	vyhlašovací
dokumentaci	dokumentace	k1gFnSc4	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
zjištěné	zjištěný	k2eAgInPc1d1	zjištěný
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
druhové	druhový	k2eAgFnSc6d1	druhová
skladbě	skladba	k1gFnSc6	skladba
<g/>
,	,	kIx,	,
počtu	počet	k1gInSc6	počet
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
i	i	k9	i
nesrovnalosti	nesrovnalost	k1gFnPc1	nesrovnalost
v	v	k7c6	v
pozemcích	pozemek	k1gInPc6	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
popsaných	popsaný	k2eAgInPc2d1	popsaný
důvodů	důvod	k1gInPc2	důvod
byla	být	k5eAaImAgFnS	být
ochrana	ochrana	k1gFnSc1	ochrana
objektu	objekt	k1gInSc2	objekt
ke	k	k7c3	k
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
ukončena	ukončen	k2eAgFnSc1d1	ukončena
<g/>
,	,	kIx,	,
situace	situace	k1gFnSc2	situace
využito	využít	k5eAaPmNgNnS	využít
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
šesti	šest	k4xCc2	šest
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
havarijním	havarijní	k2eAgInSc6d1	havarijní
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
náhradě	náhrada	k1gFnSc3	náhrada
novými	nový	k2eAgMnPc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
ochrana	ochrana	k1gFnSc1	ochrana
znovu	znovu	k6eAd1	znovu
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
mírně	mírně	k6eAd1	mírně
odlišném	odlišný	k2eAgInSc6d1	odlišný
rozsahu	rozsah	k1gInSc6	rozsah
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
465	[number]	k4	465
stromů	strom	k1gInPc2	strom
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jírovec	jírovec	k1gInSc1	jírovec
maďal	maďal	k1gInSc1	maďal
(	(	kIx(	(
<g/>
Aesculus	Aesculus	k1gInSc1	Aesculus
hippocastanum	hippocastanum	k1gNnSc1	hippocastanum
<g/>
)	)	kIx)	)
399	[number]	k4	399
ks	ks	kA	ks
</s>
</p>
<p>
<s>
lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
cordata	cordata	k1gFnSc1	cordata
<g/>
)	)	kIx)	)
55	[number]	k4	55
ks	ks	kA	ks
</s>
</p>
<p>
<s>
dub	dub	k1gInSc1	dub
letní	letní	k2eAgInSc1d1	letní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
probur	probura	k1gFnPc2	probura
<g/>
)	)	kIx)	)
4	[number]	k4	4
ks	ks	kA	ks
</s>
</p>
<p>
<s>
jeřáb	jeřáb	k1gInSc1	jeřáb
břek	břek	k1gInSc1	břek
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
torminalis	torminalis	k1gFnSc2	torminalis
<g/>
)	)	kIx)	)
3	[number]	k4	3
ks	ks	kA	ks
</s>
</p>
<p>
<s>
olše	olše	k1gFnSc1	olše
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
(	(	kIx(	(
<g/>
Alnus	Alnus	k1gMnSc1	Alnus
glutinosa	glutinosa	k1gFnSc1	glutinosa
<g/>
)	)	kIx)	)
2	[number]	k4	2
ks	ks	kA	ks
</s>
</p>
<p>
<s>
habr	habr	k1gInSc1	habr
obecný	obecný	k2eAgInSc1d1	obecný
(	(	kIx(	(
<g/>
Carpinus	Carpinus	k1gInSc1	Carpinus
betulus	betulus	k1gInSc1	betulus
<g/>
)	)	kIx)	)
1	[number]	k4	1
ks	ks	kA	ks
</s>
</p>
<p>
<s>
buk	buk	k1gInSc4	buk
lesní	lesní	k2eAgInSc4d1	lesní
(	(	kIx(	(
<g/>
Fagus	Fagus	k1gInSc4	Fagus
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
1	[number]	k4	1
ks	ks	kA	ks
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
zajímavosti	zajímavost	k1gFnPc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
Lhota	Lhota	k1gFnSc1	Lhota
pokládá	pokládat	k5eAaImIp3nS	pokládat
Ploskovskou	Ploskovský	k2eAgFnSc4d1	Ploskovská
kaštanku	kaštanka	k1gFnSc4	kaštanka
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
významnou	významný	k2eAgFnSc4d1	významná
pamětihodnost	pamětihodnost	k1gFnSc4	pamětihodnost
–	–	k?	–
na	na	k7c6	na
obecních	obecní	k2eAgInPc6d1	obecní
symbolech	symbol	k1gInPc6	symbol
<g/>
,	,	kIx,	,
vlajce	vlajka	k1gFnSc3	vlajka
a	a	k8xC	a
znaku	znak	k1gInSc3	znak
<g/>
,	,	kIx,	,
udělených	udělený	k2eAgFnPc2d1	udělená
roku	rok	k1gInSc3	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
figuruje	figurovat	k5eAaImIp3nS	figurovat
právě	právě	k6eAd1	právě
jírovcový	jírovcový	k2eAgInSc1d1	jírovcový
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
coby	coby	k?	coby
narážka	narážka	k1gFnSc1	narážka
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
stromořadí	stromořadí	k1gNnSc4	stromořadí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
obec	obec	k1gFnSc1	obec
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
pořádá	pořádat	k5eAaImIp3nS	pořádat
Slavnost	slavnost	k1gFnSc1	slavnost
rozkvetlé	rozkvetlý	k2eAgFnSc2d1	rozkvetlá
kaštanky	kaštanka	k1gFnSc2	kaštanka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Památné	památný	k2eAgInPc4d1	památný
a	a	k8xC	a
významné	významný	k2eAgInPc4d1	významný
stromy	strom	k1gInPc4	strom
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
Dohodový	dohodový	k2eAgInSc1d1	dohodový
dub	dub	k1gInSc1	dub
</s>
</p>
<p>
<s>
Zajícův	Zajícův	k2eAgInSc1d1	Zajícův
dub	dub	k1gInSc1	dub
</s>
</p>
<p>
<s>
Žilinský	žilinský	k2eAgInSc1d1	žilinský
jasan	jasan	k1gInSc1	jasan
</s>
</p>
<p>
<s>
Lhotská	lhotský	k2eAgFnSc1d1	lhotská
lípa	lípa	k1gFnSc1	lípa
</s>
</p>
<p>
<s>
Bratronická	Bratronický	k2eAgFnSc1d1	Bratronický
lípa	lípa	k1gFnSc1	lípa
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
oblast	oblast	k1gFnSc1	oblast
Křivoklátsko	Křivoklátsko	k1gNnSc1	Křivoklátsko
</s>
</p>
<p>
<s>
Lánská	lánský	k2eAgFnSc1d1	lánská
obora	obora	k1gFnSc1	obora
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
