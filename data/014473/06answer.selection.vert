<s>
Toto	tento	k3xDgNnSc4	tento
kosmické	kosmický	k2eAgNnSc4d1	kosmické
těleso	těleso	k1gNnSc4	těleso
astronomové	astronom	k1gMnPc1	astronom
původně	původně	k6eAd1	původně
řadili	řadit	k5eAaImAgMnP	řadit
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
definice	definice	k1gFnSc2	definice
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
planeta	planeta	k1gFnSc1	planeta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgNnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
26	[number]	k4	26
<g/>
.	.	kIx.	.
valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
a	a	k8xC	a
plutoidy	plutoida	k1gFnPc4	plutoida
<g/>
.	.	kIx.	.
</s>
