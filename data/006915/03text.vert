<s>
Marika	Marika	k1gFnSc1	Marika
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
původu	původ	k1gInSc2	původ
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
maďarskou	maďarský	k2eAgFnSc4d1	maďarská
podobu	podoba	k1gFnSc4	podoba
jména	jméno	k1gNnSc2	jméno
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
nositelek	nositelka	k1gFnPc2	nositelka
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgNnSc7d1	podobné
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
Mariko	Marika	k1gFnSc5	Marika
což	což	k3yQnSc1	což
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
jazyku	jazyk	k1gInSc6	jazyk
znamená	znamenat	k5eAaImIp3nS	znamenat
dítě	dítě	k1gNnSc1	dítě
věrné	věrný	k2eAgFnSc2d1	věrná
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
Marika	Marika	k1gFnSc1	Marika
znamená	znamenat	k5eAaImIp3nS	znamenat
květina	květina	k1gFnSc1	květina
jasmínu	jasmín	k1gInSc2	jasmín
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
Marika	Marika	k1gFnSc1	Marika
hanlivým	hanlivý	k2eAgInSc7d1	hanlivý
výrazem	výraz	k1gInSc7	výraz
pro	pro	k7c4	pro
homosexuála	homosexuál	k1gMnSc4	homosexuál
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+3,0	+3,0	k4	+3,0
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Marika	Marika	k1gFnSc1	Marika
Blažková	Blažková	k1gFnSc1	Blažková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
,	,	kIx,	,
editorka	editorka	k1gFnSc1	editorka
a	a	k8xC	a
novinářka	novinářka	k1gFnSc1	novinářka
Marika	Marika	k1gFnSc1	Marika
Domińczyk	Domińczyk	k1gInSc1	Domińczyk
<g/>
,	,	kIx,	,
polsko-americká	polskomerický	k2eAgFnSc1d1	polsko-americký
herečka	herečka	k1gFnSc1	herečka
Marika	Marika	k1gFnSc1	Marika
Gombitová	Gombitový	k2eAgFnSc1d1	Gombitová
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Marika	Marika	k1gFnSc1	Marika
Hanušová	Hanušová	k1gFnSc1	Hanušová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
choreografka	choreografka	k1gFnSc1	choreografka
Marika	Marika	k1gFnSc1	Marika
Johansson	Johansson	k1gInSc1	Johansson
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
Marika	Marika	k1gFnSc1	Marika
Kafková	Kafková	k1gFnSc1	Kafková
<g/>
,	,	kIx,	,
vědkyně	vědkyně	k1gFnSc1	vědkyně
a	a	k8xC	a
profesorka	profesorka	k1gFnSc1	profesorka
Marika	Marika	k1gFnSc1	Marika
Kilius	Kilius	k1gInSc1	Kilius
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
lyžařka	lyžařka	k1gFnSc1	lyžařka
Marika	Marika	k1gFnSc1	Marika
Lagercrantz	Lagercrantz	k1gInSc4	Lagercrantz
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
švédská	švédský	k2eAgFnSc1d1	švédská
herečka	herečka	k1gFnSc1	herečka
Marika	Marika	k1gFnSc1	Marika
Lendl	Lendl	k1gFnSc1	Lendl
<g/>
,	,	kIx,	,
česko-americká	českomerický	k2eAgFnSc1d1	česko-americká
golfistka	golfistka	k1gFnSc1	golfistka
Marika	Marika	k1gFnSc1	Marika
Nemethová	Nemethová	k1gFnSc1	Nemethová
<g/>
,	,	kIx,	,
fotografka	fotografka	k1gFnSc1	fotografka
Marika	Marika	k1gFnSc1	Marika
Ninou	Nina	k1gFnSc7	Nina
<g/>
,	,	kIx,	,
arménsko-česká	arménsko-český	k2eAgFnSc1d1	arménsko-český
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Marika	Marika	k1gFnSc1	Marika
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
sochařka	sochařka	k1gFnSc1	sochařka
Marika	Marika	k1gFnSc1	Marika
Pečená	pečený	k2eAgFnSc1d1	pečená
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
dirigentka	dirigentka	k1gFnSc1	dirigentka
a	a	k8xC	a
umělecká	umělecký	k2eAgFnSc1d1	umělecká
vedoucí	vedoucí	k1gFnSc1	vedoucí
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
starou	starý	k2eAgFnSc4d1	stará
hudbu	hudba	k1gFnSc4	hudba
Marika	Marika	k1gFnSc1	Marika
Pecháčková	Pecháčková	k1gFnSc1	Pecháčková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
režisérka	režisérka	k1gFnSc1	režisérka
Marika	Marika	k1gFnSc1	Marika
Procházková	Procházková	k1gFnSc1	Procházková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Marika	Marika	k1gFnSc1	Marika
Rökk	Rökk	k1gInSc1	Rökk
<g/>
,	,	kIx,	,
maďarská	maďarský	k2eAgFnSc1d1	maďarská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Marika	Marika	k1gFnSc1	Marika
Skopalová	Skopalová	k1gFnSc1	Skopalová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Marika	Marika	k1gFnSc1	Marika
Škultéty	Škultéta	k1gFnSc2	Škultéta
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Marika	Marika	k1gFnSc1	Marika
Šoposká	Šoposká	k1gFnSc1	Šoposká
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Marika	Marika	k1gFnSc1	Marika
Žáková	Žáková	k1gFnSc1	Žáková
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
</s>
