<s>
Vu	Vu	k?
(	(	kIx(
<g/>
týdeník	týdeník	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vu	Vu	k?
byl	být	k5eAaImAgInS
francouzský	francouzský	k2eAgInSc1d1
týdeník	týdeník	k1gInSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Luciena	Lucien	k1gMnSc2
Vogela	Vogel	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
vycházel	vycházet	k5eAaImAgMnS
od	od	k7c2
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1928	#num#	k4
do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1940	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Inspiroval	inspirovat	k5eAaBmAgMnS
se	se	k3xPyFc4
časopisem	časopis	k1gInSc7
Berliner	Berlinra	k1gFnPc2
Illustrierte	Illustriert	k1gInSc5
Zeitung	Zeitung	k1gMnSc1
a	a	k8xC
měl	mít	k5eAaImAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
nezanedbatelné	zanedbatelný	k2eNgNnSc4d1
místo	místo	k1gNnSc4
v	v	k7c6
historii	historie	k1gFnSc6
fotožurnalistiky	fotožurnalistika	k1gFnSc2
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
významný	významný	k2eAgInSc4d1
přínos	přínos	k1gInSc4
v	v	k7c6
publikování	publikování	k1gNnSc6
obrazových	obrazový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vu	Vu	k1gFnSc1
byl	být	k5eAaImAgInS
v	v	k7c6
úzkém	úzký	k2eAgNnSc6d1
spojení	spojení	k1gNnSc6
s	s	k7c7
magazínem	magazín	k1gInSc7
Lu	Lu	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
nabízel	nabízet	k5eAaImAgInS
přehled	přehled	k1gInSc4
mezinárodního	mezinárodní	k2eAgInSc2d1
tisku	tisk	k1gInSc2
přeložený	přeložený	k2eAgInSc4d1
do	do	k7c2
francouzštiny	francouzština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
známých	známý	k2eAgMnPc2d1
zahraničních	zahraniční	k2eAgMnPc2d1
fotografů	fotograf	k1gMnPc2
do	do	k7c2
něj	on	k3xPp3gMnSc4
přispívali	přispívat	k5eAaImAgMnP
André	André	k1gMnPc1
Kertész	Kertész	k1gMnSc1
<g/>
,	,	kIx,
Brassaï	Brassaï	k1gMnSc1
<g/>
,	,	kIx,
Germaine	Germain	k1gInSc5
Krull	Krull	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Capa	capa	k1gFnSc1
<g/>
,	,	kIx,
Serge	Serge	k1gNnSc1
de	de	k?
Sazo	Sazo	k1gMnSc1
<g/>
,	,	kIx,
Gerda	Gerda	k1gMnSc1
Taro	Taro	k1gMnSc1
<g/>
,	,	kIx,
Marcel	Marcel	k1gMnSc1
Ichac	Ichac	k1gInSc1
nebo	nebo	k8xC
novinář	novinář	k1gMnSc1
Lucien	Lucina	k1gFnPc2
Vogel	Vogel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
českých	český	k2eAgMnPc2d1
fotožurnalistů	fotožurnalista	k1gMnPc2
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgMnS
například	například	k6eAd1
Hans	Hans	k1gMnSc1
Ernest	Ernest	k1gMnSc1
Oplatka	oplatka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Novinářská	novinářský	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
VU	VU	kA
(	(	kIx(
<g/>
magazine	magazinout	k5eAaPmIp3nS
<g/>
)	)	kIx)
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Maison	Maison	k1gInSc1
Européenne	Européenn	k1gInSc5
de	de	k?
la	la	k1gNnSc7
Photographie	Photographie	k1gFnSc2
<g/>
.	.	kIx.
www.mep-fr.org	www.mep-fr.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vu	Vu	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
article	article	k6eAd1
sur	sur	k?
photosapiens	photosapiens	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Dossier	Dossier	k1gInSc1
de	de	k?
presse	presse	k1gFnSc1
:	:	kIx,
le	le	k?
front	fronta	k1gFnPc2
populaire	populair	k1gInSc5
des	des	k1gNnPc4
photographes	photographes	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
