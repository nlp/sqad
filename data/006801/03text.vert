<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
je	být	k5eAaImIp3nS	být
seznamem	seznam	k1gInSc7	seznam
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
rekordů	rekord	k1gInPc2	rekord
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
která	který	k3yIgFnSc1	který
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
ap.	ap.	kA	ap.
Zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
rekordů	rekord	k1gInPc2	rekord
je	být	k5eAaImIp3nS	být
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
nemůže	moct	k5eNaImIp3nS	moct
zachytit	zachytit	k5eAaPmF	zachytit
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
proto	proto	k8xC	proto
jen	jen	k9	jen
vybrané	vybraný	k2eAgInPc4d1	vybraný
rekordy	rekord	k1gInPc4	rekord
<g/>
.	.	kIx.	.
historická	historický	k2eAgFnSc1d1	historická
země	země	k1gFnSc1	země
zabírající	zabírající	k2eAgFnSc4d1	zabírající
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
ČR	ČR	kA	ČR
–	–	k?	–
Čechy	Čech	k1gMnPc7	Čech
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
Morava	Morava	k1gFnSc1	Morava
<g/>
;	;	kIx,	;
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
Slezsko	Slezsko	k1gNnSc4	Slezsko
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
jen	jen	k6eAd1	jen
České	český	k2eAgNnSc4d1	české
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
)	)	kIx)	)
nejsevernější	severní	k2eAgNnSc4d3	nejsevernější
město	město	k1gNnSc4	město
–	–	k?	–
Šluknov	Šluknov	k1gInSc1	Šluknov
nejsevernější	severní	k2eAgFnSc1d3	nejsevernější
obec	obec	k1gFnSc1	obec
–	–	k?	–
Lobendava	Lobendava	k1gFnSc1	Lobendava
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
,	,	kIx,	,
4328	[number]	k4	4328
km	km	kA	km
od	od	k7c2	od
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
nejjižnější	jižní	k2eAgNnSc1d3	nejjižnější
město	město	k1gNnSc1	město
–	–	k?	–
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
,	,	kIx,	,
5396	[number]	k4	5396
km	km	kA	km
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
nejjižnější	jižní	k2eAgFnSc1d3	nejjižnější
obec	obec	k1gFnSc1	obec
–	–	k?	–
Horní	horní	k2eAgNnSc4d1	horní
Dvořiště	dvořiště	k1gNnSc4	dvořiště
nejzápadnější	západní	k2eAgNnSc1d3	nejzápadnější
město	město	k1gNnSc1	město
–	–	k?	–
Hranice	hranice	k1gFnSc1	hranice
nejzápadnější	západní	k2eAgFnSc1d3	nejzápadnější
obec	obec	k1gFnSc1	obec
–	–	k?	–
Krásná	krásný	k2eAgFnSc1d1	krásná
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Cheb	Cheb	k1gInSc1	Cheb
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvýchodnější	východní	k2eAgNnSc4d3	nejvýchodnější
město	město	k1gNnSc4	město
–	–	k?	–
Jablunkov	Jablunkov	k1gInSc1	Jablunkov
nejvýchodnější	východní	k2eAgFnSc1d3	nejvýchodnější
obec	obec	k1gFnSc1	obec
–	–	k?	–
Hrčava	Hrčava	k1gFnSc1	Hrčava
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
nejzápadnějším	západní	k2eAgInSc7d3	nejzápadnější
a	a	k8xC	a
nejvýchodnějším	východní	k2eAgInSc7d3	nejvýchodnější
bodem	bod	k1gInSc7	bod
státního	státní	k2eAgNnSc2d1	státní
území	území	k1gNnSc2	území
–	–	k?	–
493	[number]	k4	493
km	km	kA	km
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
nejsevernějším	severní	k2eAgInSc7d3	nejsevernější
a	a	k8xC	a
nejjižnějším	jižní	k2eAgInSc7d3	nejjižnější
bodem	bod	k1gInSc7	bod
státního	státní	k2eAgNnSc2d1	státní
území	území	k1gNnSc2	území
–	–	k?	–
278	[number]	k4	278
km	km	kA	km
nejmenší	malý	k2eAgFnSc1d3	nejmenší
šířka	šířka	k1gFnSc1	šířka
státního	státní	k2eAgNnSc2d1	státní
území	území	k1gNnSc2	území
–	–	k?	–
143	[number]	k4	143
km	km	kA	km
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
Mikulovem	Mikulov	k1gInSc7	Mikulov
a	a	k8xC	a
Králíky	Králík	k1gMnPc4	Králík
<g/>
)	)	kIx)	)
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
máme	mít	k5eAaImIp1nP	mít
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
část	část	k1gFnSc4	část
státních	státní	k2eAgFnPc2d1	státní
hranic	hranice	k1gFnPc2	hranice
–	–	k?	–
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
815	[number]	k4	815
km	km	kA	km
nebo	nebo	k8xC	nebo
810	[number]	k4	810
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
762	[number]	k4	762
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
712,5	[number]	k4	712,5
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
máme	mít	k5eAaImIp1nP	mít
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
část	část	k1gFnSc4	část
státních	státní	k2eAgFnPc2d1	státní
hranic	hranice	k1gFnPc2	hranice
–	–	k?	–
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
265	[number]	k4	265
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
242	[number]	k4	242
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
466	[number]	k4	466
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
442,8	[number]	k4	442,8
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
nejsevernějšího	severní	k2eAgInSc2d3	nejsevernější
bodu	bod	k1gInSc2	bod
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
–	–	k?	–
290	[number]	k4	290
km	km	kA	km
(	(	kIx(	(
<g/>
od	od	k7c2	od
Štětínského	štětínský	k2eAgInSc2d1	štětínský
zálivu	záliv	k1gInSc2	záliv
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
)	)	kIx)	)
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
nejjižnějšího	jižní	k2eAgInSc2d3	nejjižnější
bodu	bod	k1gInSc2	bod
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
–	–	k?	–
326	[number]	k4	326
km	km	kA	km
(	(	kIx(	(
<g/>
od	od	k7c2	od
Terstského	terstský	k2eAgInSc2d1	terstský
zálivu	záliv	k1gInSc2	záliv
v	v	k7c6	v
Jaderském	jaderský	k2eAgNnSc6d1	Jaderské
moři	moře	k1gNnSc6	moře
<g/>
)	)	kIx)	)
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
systém	systém	k1gInSc1	systém
zabírající	zabírající	k2eAgFnSc1d1	zabírající
největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
ČR	ČR	kA	ČR
–	–	k?	–
Hercynský	hercynský	k2eAgInSc4d1	hercynský
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
největší	veliký	k2eAgNnSc4d3	veliký
území	území	k1gNnSc4	území
zabírá	zabírat	k5eAaImIp3nS	zabírat
Česká	český	k2eAgFnSc1d1	Česká
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
Alpsko-himálajský	alpskoimálajský	k2eAgInSc1d1	alpsko-himálajský
systém	systém	k1gInSc1	systém
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
celek	celek	k1gInSc4	celek
zabírající	zabírající	k2eAgInSc4d1	zabírající
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
ČR	ČR	kA	ČR
–	–	k?	–
Nízký	nízký	k2eAgInSc4d1	nízký
Jeseník	Jeseník	k1gInSc4	Jeseník
(	(	kIx(	(
<g/>
2894	[number]	k4	2894
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
celek	celek	k1gInSc1	celek
zabírající	zabírající	k2eAgFnSc4d1	zabírající
nejmenší	malý	k2eAgFnSc4d3	nejmenší
část	část	k1gFnSc4	část
ČR	ČR	kA	ČR
–	–	k?	–
Jablunkovské	jablunkovský	k2eAgNnSc4d1	Jablunkovské
mezihoří	mezihoří	k1gNnSc4	mezihoří
(	(	kIx(	(
<g/>
26	[number]	k4	26
km	km	kA	km
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
přírodní	přírodní	k2eAgInSc4d1	přírodní
bod	bod	k1gInSc4	bod
–	–	k?	–
bod	bod	k1gInSc1	bod
na	na	k7c6	na
česko-polské	českoolský	k2eAgFnSc6d1	česko-polská
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
3,5	[number]	k4	3,5
m	m	kA	m
od	od	k7c2	od
polského	polský	k2eAgInSc2d1	polský
vrcholu	vrchol	k1gInSc2	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
,	,	kIx,	,
1603,2	[number]	k4	1603,2
m	m	kA	m
<g/>
;	;	kIx,	;
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
umělý	umělý	k2eAgInSc1d1	umělý
bod	bod	k1gInSc1	bod
–	–	k?	–
vrchol	vrchol	k1gInSc4	vrchol
vysílače	vysílač	k1gMnSc4	vysílač
na	na	k7c6	na
Pradědu	praděd	k1gMnSc6	praděd
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
<g/>
,	,	kIx,	,
1638	[number]	k4	1638
m	m	kA	m
nejhlubší	hluboký	k2eAgInSc4d3	nejhlubší
důl	důl	k1gInSc4	důl
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
nejspíše	nejspíše	k9	nejspíše
i	i	k9	i
nejnižší	nízký	k2eAgNnSc1d3	nejnižší
dosažené	dosažený	k2eAgNnSc1d1	dosažené
místo	místo	k1gNnSc1	místo
ČR	ČR	kA	ČR
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
Jáma	jáma	k1gFnSc1	jáma
16	[number]	k4	16
bývalých	bývalý	k2eAgInPc2d1	bývalý
Uranových	Uranův	k2eAgInPc2d1	Uranův
dolů	dol	k1gInPc2	dol
Příbram	Příbram	k1gFnSc1	Příbram
-	-	kIx~	-
hloubka	hloubka	k1gFnSc1	hloubka
1838	[number]	k4	1838
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
-1250	-1250	k4	-1250
m.	m.	k?	m.
<g/>
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nejhlubší	hluboký	k2eAgInSc1d3	nejhlubší
vrt	vrt	k1gInSc1	vrt
ČR	ČR	kA	ČR
-	-	kIx~	-
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Jablůnka	Jablůnka	k1gFnSc1	Jablůnka
<g/>
,	,	kIx,	,
vrt	vrt	k1gInSc1	vrt
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hloubky	hloubka	k1gFnSc2	hloubka
6	[number]	k4	6
506	[number]	k4	506
m	m	kA	m
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
geomorfologický	geomorfologický	k2eAgInSc4d1	geomorfologický
celek	celek	k1gInSc4	celek
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
střední	střední	k2eAgFnSc7d1	střední
výškou	výška	k1gFnSc7	výška
–	–	k?	–
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
(	(	kIx(	(
<g/>
930,9	[number]	k4	930,9
m	m	kA	m
<g/>
)	)	kIx)	)
přírodní	přírodní	k2eAgInSc4d1	přírodní
bod	bod	k1gInSc4	bod
s	s	k7c7	s
nejmenší	malý	k2eAgFnSc7d3	nejmenší
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
–	–	k?	–
hladina	hladina	k1gFnSc1	hladina
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
u	u	k7c2	u
Hřenska	Hřensko	k1gNnSc2	Hřensko
<g/>
,	,	kIx,	,
128	[number]	k4	128
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
116	[number]	k4	116
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
nebo	nebo	k8xC	nebo
115	[number]	k4	115
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
umělý	umělý	k2eAgInSc4d1	umělý
bod	bod	k1gInSc4	bod
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
země	zem	k1gFnSc2	zem
s	s	k7c7	s
nejmenší	malý	k2eAgFnSc7d3	nejmenší
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
–	–	k?	–
dno	dno	k1gNnSc4	dno
hnědouhelného	hnědouhelný	k2eAgInSc2d1	hnědouhelný
lomu	lom	k1gInSc2	lom
Bílina	Bílina	k1gFnSc1	Bílina
u	u	k7c2	u
Bíliny	Bílina	k1gFnSc2	Bílina
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Teplice	teplice	k1gFnSc2	teplice
<g/>
,	,	kIx,	,
20,4	[number]	k4	20,4
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
nejvýše	nejvýše	k6eAd1	nejvýše
ležící	ležící	k2eAgFnSc1d1	ležící
obec	obec	k1gFnSc1	obec
–	–	k?	–
Horská	horský	k2eAgFnSc1d1	horská
Kvilda	Kvilda	k1gFnSc1	Kvilda
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Prachatice	Prachatice	k1gFnPc1	Prachatice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1070	[number]	k4	1070
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
nejvýše	nejvýše	k6eAd1	nejvýše
ležící	ležící	k2eAgNnSc4d1	ležící
sídlo	sídlo	k1gNnSc4	sídlo
–	–	k?	–
Filipova	Filipův	k2eAgFnSc1d1	Filipova
Huť	huť	k1gFnSc1	huť
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Klatovy	Klatovy	k1gInPc1	Klatovy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1093	[number]	k4	1093
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejníže	nízce	k6eAd3	nízce
ležící	ležící	k2eAgFnSc1d1	ležící
obec	obec	k1gFnSc1	obec
–	–	k?	–
Hřensko	Hřensko	k1gNnSc1	Hřensko
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
130	[number]	k4	130
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
–	–	k?	–
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
430	[number]	k4	430
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
430,2	[number]	k4	430,2
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
433	[number]	k4	433
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
440	[number]	k4	440
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
440,25	[number]	k4	440,25
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
–	–	k?	–
Labe	Labe	k1gNnSc2	Labe
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
die	die	k?	die
Elbe	Elbe	k1gInSc1	Elbe
<g/>
)	)	kIx)	)
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
1154	[number]	k4	1154
km	km	kA	km
–	–	k?	–
kratší	krátký	k2eAgFnSc1d2	kratší
část	část	k1gFnSc1	část
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
357	[number]	k4	357
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
358,3	[number]	k4	358,3
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
370	[number]	k4	370
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
379	[number]	k4	379
km	km	kA	km
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgFnSc4d2	delší
část	část	k1gFnSc4	část
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
2	[number]	k4	2
nejdelší	dlouhý	k2eAgFnPc1d3	nejdelší
řeky	řeka	k1gFnPc1	řeka
nebo	nebo	k8xC	nebo
části	část	k1gFnPc1	část
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
–	–	k?	–
Vltava	Vltava	k1gFnSc1	Vltava
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
částí	část	k1gFnSc7	část
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
542	[number]	k4	542
km	km	kA	km
nejvodnatější	vodnatý	k2eAgFnSc1d3	nejvodnatější
řeka	řeka	k1gFnSc1	řeka
–	–	k?	–
Labe	Labe	k1gNnSc1	Labe
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
Hřensku	Hřensko	k1gNnSc6	Hřensko
308	[number]	k4	308
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
česká	český	k2eAgFnSc1d1	Česká
řeka	řeka	k1gFnSc1	řeka
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
povodím	povodí	k1gNnSc7	povodí
–	–	k?	–
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
144	[number]	k4	144
055	[number]	k4	055
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
část	část	k1gFnSc1	část
povodí	povodit	k5eAaPmIp3nS	povodit
49	[number]	k4	49
933	[number]	k4	933
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
49	[number]	k4	49
964	[number]	k4	964
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
51	[number]	k4	51
103,9	[number]	k4	103,9
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
51	[number]	k4	51
394	[number]	k4	394
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
51	[number]	k4	51
406	[number]	k4	406
km	km	kA	km
<g/>
2	[number]	k4	2
největší	veliký	k2eAgInSc4d3	veliký
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Černé	Černé	k2eAgNnSc4d1	Černé
jezero	jezero	k1gNnSc4	jezero
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
plochu	plocha	k1gFnSc4	plocha
hladiny	hladina	k1gFnSc2	hladina
má	mít	k5eAaImIp3nS	mít
18,4	[number]	k4	18,4
ha	ha	kA	ha
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
18,5	[number]	k4	18,5
ha	ha	kA	ha
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Čertovo	čertův	k2eAgNnSc1d1	Čertovo
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
10,3	[number]	k4	10,3
ha	ha	kA	ha
<g/>
;	;	kIx,	;
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Plešné	Plešný	k2eAgNnSc1d1	Plešné
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
7,5	[number]	k4	7,5
ha	ha	kA	ha
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
jezero	jezero	k1gNnSc1	jezero
–	–	k?	–
Černé	Černé	k2eAgNnSc4d1	Černé
jezero	jezero	k1gNnSc4	jezero
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
39,8	[number]	k4	39,8
m	m	kA	m
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
40,6	[number]	k4	40,6
m	m	kA	m
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
rybník	rybník	k1gInSc4	rybník
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
objemem	objem	k1gInSc7	objem
vody	voda	k1gFnSc2	voda
–	–	k?	–
Staňkovský	Staňkovský	k2eAgInSc4d1	Staňkovský
rybník	rybník	k1gInSc4	rybník
6	[number]	k4	6
600	[number]	k4	600
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
rybník	rybník	k1gInSc1	rybník
–	–	k?	–
Staňkovský	Staňkovský	k2eAgInSc1d1	Staňkovský
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
6	[number]	k4	6
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
nejhlubší	hluboký	k2eAgInSc1d3	nejhlubší
rybník	rybník	k1gInSc1	rybník
–	–	k?	–
Staňkovský	Staňkovský	k2eAgInSc1d1	Staňkovský
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
18	[number]	k4	18
m	m	kA	m
hluboký	hluboký	k2eAgInSc4d1	hluboký
rybník	rybník	k1gInSc4	rybník
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
plochou	plocha	k1gFnSc7	plocha
hladiny	hladina	k1gFnSc2	hladina
–	–	k?	–
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
(	(	kIx(	(
<g/>
u	u	k7c2	u
Třeboně	Třeboň	k1gFnSc2	Třeboň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4,89	[number]	k4	4,89
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
objem	objem	k1gInSc1	objem
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
200	[number]	k4	200
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
rybník	rybník	k1gInSc4	rybník
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
plochou	plocha	k1gFnSc7	plocha
hladiny	hladina	k1gFnSc2	hladina
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
objemem	objem	k1gInSc7	objem
vody	voda	k1gFnSc2	voda
–	–	k?	–
Orlík	orlík	k1gMnSc1	orlík
(	(	kIx(	(
<g/>
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
a	a	k8xC	a
Otavě	Otava	k1gFnSc6	Otava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
704	[number]	k4	704
000	[number]	k4	000
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
720	[number]	k4	720
000	[number]	k4	000
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
necelých	celý	k2eNgInPc2d1	necelý
717	[number]	k4	717
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
konkrétněji	konkrétně	k6eAd2	konkrétně
716	[number]	k4	716
600	[number]	k4	600
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
vody	voda	k1gFnSc2	voda
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
plochou	plocha	k1gFnSc7	plocha
hladiny	hladina	k1gFnSc2	hladina
–	–	k?	–
Lipno	Lipno	k1gNnSc1	Lipno
<g/>
,	,	kIx,	,
48,7	[number]	k4	48,7
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
objem	objem	k1gInSc1	objem
je	být	k5eAaImIp3nS	být
309	[number]	k4	309
502	[number]	k4	502
000	[number]	k4	000
km	km	kA	km
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
přehradní	přehradní	k2eAgFnSc4d1	přehradní
hráz	hráz	k1gFnSc4	hráz
–	–	k?	–
Dalešice	Dalešice	k1gFnSc2	Dalešice
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Jihlavě	Jihlava	k1gFnSc6	Jihlava
(	(	kIx(	(
<g/>
sypaná	sypaný	k2eAgFnSc1d1	sypaná
hráz	hráz	k1gFnSc1	hráz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
100	[number]	k4	100
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Orlík	Orlík	k1gInSc1	Orlík
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
a	a	k8xC	a
Otavě	Otava	k1gFnSc6	Otava
(	(	kIx(	(
<g/>
betonová	betonový	k2eAgFnSc1d1	betonová
hráz	hráz	k1gFnSc1	hráz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
91,5	[number]	k4	91,5
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
betonová	betonový	k2eAgFnSc1d1	betonová
přehradní	přehradní	k2eAgFnSc1d1	přehradní
hráz	hráz	k1gFnSc1	hráz
–	–	k?	–
Orlík	orlík	k1gMnSc1	orlík
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
a	a	k8xC	a
Otavě	Otava	k1gFnSc6	Otava
<g/>
,	,	kIx,	,
91,5	[number]	k4	91,5
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
sypaná	sypaný	k2eAgFnSc1d1	sypaná
přehradní	přehradní	k2eAgFnSc1d1	přehradní
hráz	hráz	k1gFnSc1	hráz
–	–	k?	–
Nechranice	Nechranice	k1gFnSc1	Nechranice
(	(	kIx(	(
<g/>
na	na	k7c6	na
Ohři	Ohře	k1gFnSc6	Ohře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3286	[number]	k4	3286
m	m	kA	m
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
3280	[number]	k4	3280
m	m	kA	m
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
přehradní	přehradní	k2eAgFnSc1d1	přehradní
hráz	hráz	k1gFnSc1	hráz
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
šířkou	šířka	k1gFnSc7	šířka
základů	základ	k1gInPc2	základ
–	–	k?	–
Nechranice	Nechranice	k1gFnSc1	Nechranice
(	(	kIx(	(
<g/>
na	na	k7c6	na
Ohři	Ohře	k1gFnSc6	Ohře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
základy	základ	k1gInPc1	základ
jsou	být	k5eAaImIp3nP	být
široké	široký	k2eAgFnPc1d1	široká
800	[number]	k4	800
m	m	kA	m
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vodopád	vodopád	k1gInSc1	vodopád
–	–	k?	–
Pančavský	Pančavský	k2eAgInSc1d1	Pančavský
vodopád	vodopád	k1gInSc1	vodopád
v	v	k7c6	v
Labském	labský	k2eAgInSc6d1	labský
dole	dol	k1gInSc6	dol
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
,	,	kIx,	,
148	[number]	k4	148
m	m	kA	m
nejteplejší	teplý	k2eAgInSc1d3	nejteplejší
minerální	minerální	k2eAgInSc1d1	minerální
pramen	pramen	k1gInSc1	pramen
–	–	k?	–
Vřídlo	vřídlo	k1gNnSc1	vřídlo
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
,	,	kIx,	,
72	[number]	k4	72
°	°	k?	°
<g/>
C	C	kA	C
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
73	[number]	k4	73
°	°	k?	°
<g/>
C	C	kA	C
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
73,4	[number]	k4	73,4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
podnebná	podnebný	k2eAgFnSc1d1	podnebná
oblast	oblast	k1gFnSc1	oblast
zabírající	zabírající	k2eAgFnSc1d1	zabírající
největší	veliký	k2eAgNnSc4d3	veliký
území	území	k1gNnSc4	území
ČR	ČR	kA	ČR
–	–	k?	–
mírně	mírně	k6eAd1	mírně
teplá	teplý	k2eAgFnSc1d1	teplá
podnebná	podnebný	k2eAgFnSc1d1	podnebná
oblast	oblast	k1gFnSc1	oblast
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
10,1	[number]	k4	10,1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Klementinum	Klementinum	k1gNnSc4	Klementinum
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
0,2	[number]	k4	0,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Sněžka	Sněžka	k1gFnSc1	Sněžka
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
40,4	[number]	k4	40,4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g />
.	.	kIx.	.
</s>
<s>
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Dobřichovice	Dobřichovice	k1gFnPc1	Dobřichovice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Praha-západ	Prahaápad	k1gInSc1	Praha-západ
<g/>
)	)	kIx)	)
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
-42,2	-42,2	k4	-42,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Litvínovice	Litvínovice	k1gFnSc1	Litvínovice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
–	–	k?	–
-41,6	-41,6	k4	-41,6
<g />
.	.	kIx.	.
</s>
<s>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
Kvilda-Perla	Kvilda-Perla	k1gFnSc1	Kvilda-Perla
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Prachatice	Prachatice	k1gFnPc1	Prachatice
<g/>
)	)	kIx)	)
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
–	–	k?	–
1705	[number]	k4	1705
mm	mm	kA	mm
<g/>
,	,	kIx,	,
Bílý	bílý	k2eAgInSc1d1	bílý
Potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
v	v	k7c6	v
Jizerských	jizerský	k2eAgFnPc6d1	Jizerská
horách	hora	k1gFnPc6	hora
<g/>
)	)	kIx)	)
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
průměrné	průměrný	k2eAgFnPc4d1	průměrná
roční	roční	k2eAgFnPc4d1	roční
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
410	[number]	k4	410
mm	mm	kA	mm
<g/>
,	,	kIx,	,
Libědice	Libědice	k1gFnSc1	Libědice
u	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
Žatce	Žatec	k1gInPc1	Žatec
místo	místo	k1gNnSc1	místo
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
napršených	napršený	k2eAgInPc2d1	napršený
za	za	k7c2	za
24	[number]	k4	24
h	h	k?	h
–	–	k?	–
Nová	nový	k2eAgFnSc1d1	nová
Louka	louka	k1gFnSc1	louka
(	(	kIx(	(
<g/>
v	v	k7c6	v
Jizerských	jizerský	k2eAgFnPc6d1	Jizerská
horách	hora	k1gFnPc6	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
345	[number]	k4	345
mm	mm	kA	mm
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
30.7	[number]	k4	30.7
<g/>
.	.	kIx.	.
1897	[number]	k4	1897
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
–	–	k?	–
216	[number]	k4	216
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Sněžka	Sněžka	k1gFnSc1	Sněžka
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
198	[number]	k4	198
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Klínovec	Klínovec	k1gInSc1	Klínovec
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
půdní	půdní	k2eAgInSc1d1	půdní
typ	typ	k1gInSc1	typ
–	–	k?	–
kambizem	kambiz	k1gInSc7	kambiz
(	(	kIx(	(
<g/>
hnědá	hnědý	k2eAgFnSc1d1	hnědá
půda	půda	k1gFnSc1	půda
<g/>
)	)	kIx)	)
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
systém	systém	k1gInSc1	systém
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
<g/>
,	,	kIx,	,
34,9	[number]	k4	34,9
km	km	kA	km
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Býčí	býčí	k2eAgFnSc1d1	býčí
skála	skála	k1gFnSc1	skála
a	a	k8xC	a
Rudické	rudický	k2eAgNnSc1d1	Rudické
propadání	propadání	k1gNnSc1	propadání
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgFnSc4	obojí
okolo	okolo	k7c2	okolo
13	[number]	k4	13
km	km	kA	km
nejhlubší	hluboký	k2eAgInSc1d3	nejhlubší
jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
systém	systém	k1gInSc1	systém
-	-	kIx~	-
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
Býčí	býčí	k2eAgFnSc1d1	býčí
skála	skála	k1gFnSc1	skála
a	a	k8xC	a
Rudické	rudický	k2eAgNnSc1d1	Rudické
propadání	propadání	k1gNnSc1	propadání
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
okolo	okolo	k7c2	okolo
200	[number]	k4	200
m	m	kA	m
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
propast	propast	k1gFnSc1	propast
–	–	k?	–
Hranická	hranický	k2eAgFnSc1d1	Hranická
<g />
.	.	kIx.	.
</s>
<s>
propast	propast	k1gFnSc1	propast
(	(	kIx(	(
<g/>
u	u	k7c2	u
města	město	k1gNnSc2	město
Hranice	hranice	k1gFnSc2	hranice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Teplic	Teplice	k1gFnPc2	Teplice
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
minimálně	minimálně	k6eAd1	minimálně
473,5	[number]	k4	473,5
m	m	kA	m
<g/>
;	;	kIx,	;
propast	propast	k1gFnSc1	propast
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
zatopena	zatopen	k2eAgFnSc1d1	zatopena
Hranickým	hranický	k2eAgNnSc7d1	hranické
jezerem	jezero	k1gNnSc7	jezero
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
suché	suchý	k2eAgFnSc2d1	suchá
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
69,5	[number]	k4	69,5
m	m	kA	m
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
zatopené	zatopený	k2eAgFnSc2d1	zatopená
části	část	k1gFnSc2	část
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
minimálně	minimálně	k6eAd1	minimálně
však	však	k9	však
<g />
.	.	kIx.	.
</s>
<s>
404	[number]	k4	404
m	m	kA	m
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
dóm	dóm	k1gInSc1	dóm
–	–	k?	–
Obří	obří	k2eAgInSc1d1	obří
dóm	dóm	k1gInSc1	dóm
(	(	kIx(	(
<g/>
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
150	[number]	k4	150
×	×	k?	×
40	[number]	k4	40
×	×	k?	×
70	[number]	k4	70
m	m	kA	m
největší	veliký	k2eAgInSc1d3	veliký
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
–	–	k?	–
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
690,3	[number]	k4	690,3
km2	km2	k4	km2
nejstarší	starý	k2eAgInSc1d3	nejstarší
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
–	–	k?	–
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
zřízen	zřízen	k2eAgInSc1d1	zřízen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g />
.	.	kIx.	.
</s>
<s>
největší	veliký	k2eAgFnSc1d3	veliký
chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
–	–	k?	–
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Beskydy	Beskyd	k1gInPc4	Beskyd
<g/>
,	,	kIx,	,
1160	[number]	k4	1160
km2	km2	k4	km2
nejstarší	starý	k2eAgFnSc1d3	nejstarší
chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
–	–	k?	–
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
<g/>
,	,	kIx,	,
zřízen	zřízen	k2eAgInSc1d1	zřízen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
největší	veliký	k2eAgFnSc1d3	veliký
skalní	skalní	k2eAgFnSc1d1	skalní
brána	brána	k1gFnSc1	brána
–	–	k?	–
Pravčická	Pravčický	k2eAgFnSc1d1	Pravčická
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
Národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
České	český	k2eAgNnSc1d1	české
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
25	[number]	k4	25
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
16	[number]	k4	16
m	m	kA	m
široká	široký	k2eAgFnSc1d1	široká
<g />
.	.	kIx.	.
</s>
<s>
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
strom	strom	k1gInSc1	strom
–	–	k?	–
smrk	smrk	k1gInSc1	smrk
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
50	[number]	k4	50
%	%	kIx~	%
stromů	strom	k1gInPc2	strom
<g/>
)	)	kIx)	)
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
železniční	železniční	k2eAgInSc1d1	železniční
tunel	tunel	k1gInSc1	tunel
–	–	k?	–
Březenský	Březenský	k2eAgInSc1d1	Březenský
tunel	tunel	k1gInSc1	tunel
(	(	kIx(	(
<g/>
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Lužná	Lužná	k1gFnSc1	Lužná
u	u	k7c2	u
Rakovníka	Rakovník	k1gInSc2	Rakovník
–	–	k?	–
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
m	m	kA	m
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Špičácký	Špičácký	k2eAgInSc1d1	Špičácký
tunel	tunel	k1gInSc1	tunel
(	(	kIx(	(
<g/>
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Klatovy	Klatovy	k1gInPc1	Klatovy
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Železná	železný	k2eAgFnSc1d1	železná
Ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
1747	[number]	k4	1747
m	m	kA	m
nejstarší	starý	k2eAgInSc1d3	nejstarší
provoz	provoz	k1gInSc1	provoz
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
železnice	železnice	k1gFnSc2	železnice
–	–	k?	–
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
–	–	k?	–
<g/>
Linec	Linec	k1gInSc1	Linec
(	(	kIx(	(
<g/>
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
již	již	k6eAd1	již
nefunguje	fungovat	k5eNaImIp3nS	fungovat
nejstarší	starý	k2eAgFnSc1d3	nejstarší
parostrojní	parostrojní	k2eAgFnSc1d1	parostrojní
železnice	železnice	k1gFnSc1	železnice
–	–	k?	–
trať	trať	k1gFnSc1	trať
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
–	–	k?	–
<g/>
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
parostrojní	parostrojní	k2eAgFnSc1d1	parostrojní
železnice	železnice	k1gFnSc1	železnice
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
nejstarší	starý	k2eAgInSc1d3	nejstarší
provoz	provoz	k1gInSc1	provoz
síť	síť	k1gFnSc1	síť
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
–	–	k?	–
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
tramvaj	tramvaj	k1gFnSc1	tramvaj
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
parní	parní	k2eAgFnSc7d1	parní
dráhou	dráha	k1gFnSc7	dráha
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
elektrickými	elektrický	k2eAgInPc7d1	elektrický
<g />
.	.	kIx.	.
</s>
<s>
tramvajemi	tramvaj	k1gFnPc7	tramvaj
nejstarší	starý	k2eAgFnSc1d3	nejstarší
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
–	–	k?	–
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
mezi	mezi	k7c7	mezi
lázněmi	lázeň	k1gFnPc7	lázeň
Bohdaneč	Bohdaneč	k1gMnSc1	Bohdaneč
a	a	k8xC	a
Pardubicemi	Pardubice	k1gInPc7	Pardubice
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
visutá	visutý	k2eAgFnSc1d1	visutá
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
–	–	k?	–
sedačková	sedačkový	k2eAgFnSc1d1	sedačková
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
Zoologické	zoologický	k2eAgFnSc6d1	zoologická
zahradě	zahrada	k1gFnSc6	zahrada
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
–	–	k?	–
<g/>
Troji	troje	k4xRgFnSc4	troje
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
osobní	osobní	k2eAgFnSc1d1	osobní
kabinová	kabinový	k2eAgFnSc1d1	kabinová
jednolanová	jednolanový	k2eAgFnSc1d1	jednolanová
dráha	dráha	k1gFnSc1	dráha
Punkevní	punkevní	k2eAgFnSc2d1	punkevní
<g />
.	.	kIx.	.
</s>
<s>
jeskyně	jeskyně	k1gFnSc1	jeskyně
–	–	k?	–
Macocha	Macocha	k1gFnSc1	Macocha
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
kabinová	kabinový	k2eAgFnSc1d1	kabinová
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
–	–	k?	–
osobní	osobní	k2eAgFnSc1d1	osobní
visutá	visutý	k2eAgFnSc1d1	visutá
jednolanová	jednolanový	k2eAgFnSc1d1	jednolanová
dráha	dráha	k1gFnSc1	dráha
Punkevní	punkevní	k2eAgFnSc2d1	punkevní
jeskyně	jeskyně	k1gFnSc2	jeskyně
–	–	k?	–
Macocha	Macocha	k1gFnSc1	Macocha
nejstrmější	strmý	k2eAgFnSc1d3	nejstrmější
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
–	–	k?	–
osobní	osobní	k2eAgFnSc1d1	osobní
kabinová	kabinový	k2eAgFnSc1d1	kabinová
visutá	visutý	k2eAgFnSc1d1	visutá
jednolanová	jednolanový	k2eAgFnSc1d1	jednolanová
dráha	dráha	k1gFnSc1	dráha
Punkevní	punkevní	k2eAgFnSc2d1	punkevní
jeskyně	jeskyně	k1gFnSc2	jeskyně
–	–	k?	–
Macocha	Macocha	k1gFnSc1	Macocha
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
sklon	sklon	k1gInSc1	sklon
má	mít	k5eAaImIp3nS	mít
63,41	[number]	k4	63,41
%	%	kIx~	%
nejstarší	starý	k2eAgFnSc1d3	nejstarší
kabinová	kabinový	k2eAgFnSc1d1	kabinová
lanovka	lanovka	k1gFnSc1	lanovka
–	–	k?	–
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Janské	janský	k2eAgFnSc2d1	Janská
Lázně	lázeň	k1gFnSc2	lázeň
<g/>
–	–	k?	–
<g/>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
provoz	provoz	k1gInSc1	provoz
zahájen	zahájen	k2eAgInSc1d1	zahájen
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
nejstarší	starý	k2eAgNnSc4d3	nejstarší
letiště	letiště	k1gNnSc4	letiště
–	–	k?	–
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
–	–	k?	–
<g/>
Borech	bor	k1gInPc6	bor
<g/>
,	,	kIx,	,
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
zrušeno	zrušen	k2eAgNnSc1d1	zrušeno
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
největší	veliký	k2eAgNnSc4d3	veliký
letiště	letiště	k1gNnSc4	letiště
–	–	k?	–
Letiště	letiště	k1gNnSc2	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
–	–	k?	–
<g/>
Ruzyni	Ruzyně	k1gFnSc6	Ruzyně
nejstarší	starý	k2eAgInSc1d3	nejstarší
automobil	automobil	k1gInSc1	automobil
zkonstruovaný	zkonstruovaný	k2eAgInSc1d1	zkonstruovaný
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
–	–	k?	–
automobil	automobil	k1gInSc1	automobil
značky	značka	k1gFnSc2	značka
Präsident	Präsident	k1gMnSc1	Präsident
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
v	v	k7c6	v
Kopřivnici	Kopřivnice	k1gFnSc6	Kopřivnice
nejvýše	nejvýše	k6eAd1	nejvýše
ležící	ležící	k2eAgFnSc2d1	ležící
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
–	–	k?	–
Kubova	Kubův	k2eAgFnSc1d1	Kubova
Huť	huť	k1gFnSc1	huť
(	(	kIx(	(
<g/>
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
995	[number]	k4	995
m	m	kA	m
největší	veliký	k2eAgInSc1d3	veliký
sportovní	sportovní	k2eAgInSc1d1	sportovní
stadion	stadion	k1gInSc1	stadion
–	–	k?	–
Strahovský	strahovský	k2eAgInSc1d1	strahovský
stadion	stadion	k1gInSc1	stadion
(	(	kIx(	(
<g/>
až	až	k9	až
240	[number]	k4	240
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
250	[number]	k4	250
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zapsán	zapsat	k5eAaPmNgInS	zapsat
v	v	k7c6	v
Guinnessově	Guinnessův	k2eAgFnSc6d1	Guinnessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
)	)	kIx)	)
největší	veliký	k2eAgInSc1d3	veliký
hrad	hrad	k1gInSc1	hrad
–	–	k?	–
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
dosud	dosud	k6eAd1	dosud
existující	existující	k2eAgInSc4d1	existující
hrad	hrad	k1gInSc4	hrad
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
zapsán	zapsán	k2eAgMnSc1d1	zapsán
v	v	k7c6	v
Guinnessově	Guinnessův	k2eAgFnSc6d1	Guinnessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
)	)	kIx)	)
nejstarší	starý	k2eAgInSc1d3	nejstarší
hrad	hrad	k1gInSc1	hrad
–	–	k?	–
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
románská	románský	k2eAgFnSc1d1	románská
Přimda	Přimda	k1gFnSc1	Přimda
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
stáří	stáří	k1gNnSc1	stáří
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
)	)	kIx)	)
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
most	most	k1gInSc1	most
-	-	kIx~	-
Radotínský	radotínský	k2eAgInSc1d1	radotínský
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
2280	[number]	k4	2280
m	m	kA	m
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
obloukový	obloukový	k2eAgInSc1d1	obloukový
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
most	most	k1gInSc1	most
−	−	k?	−
most	most	k1gInSc1	most
přes	přes	k7c4	přes
Studenou	studený	k2eAgFnSc4d1	studená
Vltavu	Vltava	k1gFnSc4	Vltava
u	u	k7c2	u
Černého	Černého	k2eAgInSc2d1	Černého
Kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
36	[number]	k4	36
m	m	kA	m
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
<g />
.	.	kIx.	.
</s>
<s>
most	most	k1gInSc1	most
–	–	k?	–
Žďákovský	Žďákovský	k2eAgInSc1d1	Žďákovský
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
u	u	k7c2	u
Orlíku	Orlík	k1gInSc2	Orlík
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Vltavu	Vltava	k1gFnSc4	Vltava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
90	[number]	k4	90
m	m	kA	m
od	od	k7c2	od
nejhlubšího	hluboký	k2eAgInSc2d3	nejhlubší
bodu	bod	k1gInSc2	bod
údolí	údolí	k1gNnSc2	údolí
nejstarší	starý	k2eAgInSc1d3	nejstarší
krytý	krytý	k2eAgInSc1d1	krytý
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
most	most	k1gInSc1	most
–	–	k?	–
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
most	most	k1gInSc1	most
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Pernštejně	Pernštejn	k1gInSc6	Pernštejn
<g/>
,	,	kIx,	,
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nejstarší	starý	k2eAgInSc4d3	nejstarší
krytý	krytý	k2eAgInSc4d1	krytý
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
most	most	k1gInSc4	most
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
most	most	k1gInSc4	most
v	v	k7c6	v
Kynšperku	Kynšperk	k1gInSc6	Kynšperk
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
(	(	kIx(	(
<g/>
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
)	)	kIx)	)
nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
most	most	k1gInSc1	most
–	–	k?	–
gotický	gotický	k2eAgInSc1d1	gotický
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
(	(	kIx(	(
<g/>
kamenný	kamenný	k2eAgInSc4d1	kamenný
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Otavu	Otava	k1gFnSc4	Otava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postaven	postaven	k2eAgInSc1d1	postaven
roku	rok	k1gInSc2	rok
1263	[number]	k4	1263
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgInSc1d3	nejstarší
kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
nejstarší	starý	k2eAgMnSc1d3	nejstarší
dosud	dosud	k6eAd1	dosud
existující	existující	k2eAgInSc1d1	existující
klášter	klášter	k1gInSc1	klášter
–	–	k?	–
benediktinský	benediktinský	k2eAgInSc1d1	benediktinský
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášter	klášter	k1gInSc1	klášter
(	(	kIx(	(
<g/>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
–	–	k?	–
<g/>
Břevnově	Břevnov	k1gInSc6	Břevnov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
993	[number]	k4	993
nejvýše	nejvýše	k6eAd1	nejvýše
umístěná	umístěný	k2eAgFnSc1d1	umístěná
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
plošina	plošina	k1gFnSc1	plošina
rozhledny	rozhledna	k1gFnSc2	rozhledna
–	–	k?	–
Žižkovský	žižkovský	k2eAgInSc1d1	žižkovský
vysílač	vysílač	k1gInSc1	vysílač
(	(	kIx(	(
<g/>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
93	[number]	k4	93
m	m	kA	m
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
největší	veliký	k2eAgFnSc1d3	veliký
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Temelín	Temelín	k1gInSc1	Temelín
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
měla	mít	k5eAaImAgFnS	mít
elektrický	elektrický	k2eAgInSc4d1	elektrický
výkon	výkon	k1gInSc4	výkon
2000	[number]	k4	2000
MW	MW	kA	MW
<g/>
)	)	kIx)	)
největší	veliký	k2eAgMnSc1d3	veliký
územně	územně	k6eAd1	územně
správní	správní	k2eAgInSc1d1	správní
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
nejmenší	malý	k2eAgInSc1d3	nejmenší
územně	územně	k6eAd1	územně
správní	správní	k2eAgInSc1d1	správní
kraj	kraj	k1gInSc1	kraj
–	–	k?	–
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Liberecký	liberecký	k2eAgInSc1d1	liberecký
kraj	kraj	k1gInSc1	kraj
<g/>
;	;	kIx,	;
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
územně	územně	k6eAd1	územně
správní	správní	k2eAgInSc1d1	správní
kraj	kraj	k1gInSc1	kraj
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
–	–	k?	–
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
územně	územně	k6eAd1	územně
správní	správní	k2eAgInSc1d1	správní
kraj	kraj	k1gInSc1	kraj
s	s	k7c7	s
nejmenším	malý	k2eAgInSc7d3	nejmenší
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
–	–	k?	–
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
největší	veliký	k2eAgFnSc1d3	veliký
obec	obec	k1gFnSc1	obec
–	–	k?	–
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
–	–	k?	–
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
1	[number]	k4	1
280	[number]	k4	280
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
1	[number]	k4	1
195	[number]	k4	195
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
nejmenším	malý	k2eAgInSc7d3	nejmenší
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
vojenských	vojenský	k2eAgInPc2d1	vojenský
újezdů	újezd	k1gInPc2	újezd
<g/>
)	)	kIx)	)
–	–	k?	–
Čilá	čilý	k2eAgFnSc1d1	čilá
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Rokycany	Rokycany	k1gInPc1	Rokycany
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
21	[number]	k4	21
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
hustotou	hustota	k1gFnSc7	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
–	–	k?	–
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3	nejnavštěvovanější
obec	obec	k1gFnSc1	obec
–	–	k?	–
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
až	až	k9	až
<g />
.	.	kIx.	.
</s>
<s>
90	[number]	k4	90
%	%	kIx~	%
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
navštíví	navštívit	k5eAaPmIp3nP	navštívit
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
navštíví	navštívit	k5eAaPmIp3nS	navštívit
i	i	k9	i
Prahu	Praha	k1gFnSc4	Praha
<g/>
)	)	kIx)	)
české	český	k2eAgNnSc4d1	české
dvojměstí	dvojměstí	k1gNnSc4	dvojměstí
s	s	k7c7	s
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
–	–	k?	–
Brandýs	Brandýs	k1gInSc1	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
–	–	k?	–
Stará	starat	k5eAaImIp3nS	starat
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
28	[number]	k4	28
písmen	písmeno	k1gNnPc2	písmeno
a	a	k8xC	a
28	[number]	k4	28
hlásek	hláska	k1gFnPc2	hláska
česká	český	k2eAgFnSc1d1	Česká
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
českým	český	k2eAgInSc7d1	český
jednoslovným	jednoslovný	k2eAgInSc7d1	jednoslovný
názvem	název	k1gInSc7	název
–	–	k?	–
Prostředkovice	Prostředkovice	k1gFnSc2	Prostředkovice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
14	[number]	k4	14
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
české	český	k2eAgNnSc1d1	české
město	město	k1gNnSc1	město
s	s	k7c7	s
nejkratším	krátký	k2eAgInSc7d3	nejkratší
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
–	–	k?	–
Aš	Aš	k1gInSc1	Aš
(	(	kIx(	(
<g/>
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
písmena	písmeno	k1gNnSc2	písmeno
a	a	k8xC	a
2	[number]	k4	2
hlásky	hláska	k1gFnSc2	hláska
česká	český	k2eAgFnSc1d1	Česká
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
nejkratším	krátký	k2eAgInSc7d3	nejkratší
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
–	–	k?	–
Aš	Aš	k1gFnSc1	Aš
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eš	Eš	k1gFnSc1	Eš
(	(	kIx(	(
<g/>
vesnice	vesnice	k1gFnSc1	vesnice
na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgInPc1	obojí
2	[number]	k4	2
písmena	písmeno	k1gNnPc1	písmeno
a	a	k8xC	a
2	[number]	k4	2
hlásky	hláska	k1gFnSc2	hláska
největší	veliký	k2eAgNnSc4d3	veliký
náměstí	náměstí	k1gNnSc4	náměstí
–	–	k?	–
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
náměstí	náměstí	k1gNnSc4	náměstí
(	(	kIx(	(
<g/>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
plocha	plocha	k1gFnSc1	plocha
měří	měřit	k5eAaImIp3nS	měřit
8,5	[number]	k4	8,5
ha	ha	kA	ha
(	(	kIx(	(
<g/>
80	[number]	k4	80
550	[number]	k4	550
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
největší	veliký	k2eAgNnSc1d3	veliký
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
–	–	k?	–
Morávka	Morávek	k1gMnSc2	Morávek
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
<g/>
,	,	kIx,	,
87,29	[number]	k4	87,29
km	km	kA	km
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
nejmenší	malý	k2eAgFnSc1d3	nejmenší
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
–	–	k?	–
Stráž	stráž	k1gFnSc1	stráž
u	u	k7c2	u
Hradiště	Hradiště	k1gNnSc2	Hradiště
II	II	kA	II
<g/>
,	,	kIx,	,
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
obce	obec	k1gFnSc2	obec
Stráž	stráž	k1gFnSc1	stráž
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
2336	[number]	k4	2336
m	m	kA	m
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
jedinou	jediný	k2eAgFnSc4d1	jediná
parcelu	parcela	k1gFnSc4	parcela
<g/>
)	)	kIx)	)
nejstarší	starý	k2eAgInSc1d3	nejstarší
městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
–	–	k?	–
znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
přesné	přesný	k2eAgNnSc1d1	přesné
stáří	stáří	k1gNnSc1	stáří
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doklady	doklad	k1gInPc1	doklad
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
užívání	užívání	k1gNnSc4	užívání
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
nejstarší	starý	k2eAgFnSc1d3	nejstarší
mince	mince	k1gFnSc1	mince
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
–	–	k?	–
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
denár	denár	k1gInSc1	denár
<g/>
,	,	kIx,	,
konec	konec	k1gInSc1	konec
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nejstarší	starý	k2eAgFnSc1d3	nejstarší
zlatá	zlatý	k2eAgFnSc1d1	zlatá
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medaile	medaile	k1gFnSc1	medaile
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
získal	získat	k5eAaPmAgMnS	získat
Čech	Čech	k1gMnSc1	Čech
–	–	k?	–
zlatá	zlatá	k1gFnSc1	zlatá
medaile	medaile	k1gFnSc1	medaile
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
získal	získat	k5eAaPmAgMnS	získat
Bedřich	Bedřich	k1gMnSc1	Bedřich
Šupčík	Šupčík	k1gMnSc1	Šupčík
za	za	k7c4	za
šplh	šplh	k1gInSc4	šplh
na	na	k7c6	na
laně	lano	k1gNnSc6	lano
největší	veliký	k2eAgInSc1d3	veliký
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
podnik	podnik	k1gInSc1	podnik
–	–	k?	–
Škoda	škoda	k6eAd1	škoda
Auto	auto	k1gNnSc1	auto
(	(	kIx(	(
<g/>
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
činil	činit	k5eAaImAgInS	činit
roční	roční	k2eAgInSc1d1	roční
obrat	obrat	k1gInSc1	obrat
161	[number]	k4	161
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
nejstarší	starý	k2eAgFnSc1d3	nejstarší
městské	městský	k2eAgNnSc1d1	Městské
elektrické	elektrický	k2eAgNnSc1d1	elektrické
osvětlení	osvětlení	k1gNnSc1	osvětlení
–	–	k?	–
v	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
největší	veliký	k2eAgFnSc1d3	veliký
česká	český	k2eAgFnSc1d1	Česká
záhada	záhada	k1gFnSc1	záhada
–	–	k?	–
záhada	záhada	k1gFnSc1	záhada
kolem	kolem	k7c2	kolem
tvorby	tvorba	k1gFnSc2	tvorba
Ďáblovy	ďáblův	k2eAgFnSc2d1	Ďáblova
bible	bible	k1gFnSc2	bible
nejstarší	starý	k2eAgFnSc1d3	nejstarší
známá	známý	k2eAgFnSc1d1	známá
tištěná	tištěný	k2eAgFnSc1d1	tištěná
kniha	kniha	k1gFnSc1	kniha
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
–	–	k?	–
Kronika	kronika	k1gFnSc1	kronika
trojánská	trojánský	k2eAgFnSc1d1	Trojánská
<g/>
,	,	kIx,	,
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vytištěna	vytištěn	k2eAgFnSc1d1	vytištěna
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
to	ten	k3xDgNnSc1	ten
prokázané	prokázaný	k2eAgNnSc1d1	prokázané
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
prvotisk	prvotisk	k1gInSc1	prvotisk
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
)	)	kIx)	)
panovník	panovník	k1gMnSc1	panovník
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
nejdéle	dlouho	k6eAd3	dlouho
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
–	–	k?	–
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgInS	vládnout
68	[number]	k4	68
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
nejčastější	častý	k2eAgNnSc4d3	nejčastější
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
–	–	k?	–
Marie	Maria	k1gFnSc2	Maria
nejčastější	častý	k2eAgNnSc1d3	nejčastější
příjmení	příjmení	k1gNnSc1	příjmení
–	–	k?	–
Novák	Novák	k1gMnSc1	Novák
či	či	k8xC	či
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
;	;	kIx,	;
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
Novotný	Novotný	k1gMnSc1	Novotný
či	či	k8xC	či
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
;	;	kIx,	;
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
Svoboda	Svoboda	k1gMnSc1	Svoboda
či	či	k8xC	či
Svobodová	Svobodová	k1gFnSc1	Svobodová
<g/>
;	;	kIx,	;
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
Dvořák	Dvořák	k1gMnSc1	Dvořák
či	či	k8xC	či
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
<g/>
;	;	kIx,	;
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
–	–	k?	–
Černý	Černý	k1gMnSc1	Černý
či	či	k8xC	či
Černá	černat	k5eAaImIp3nS	černat
Zeměpisné	zeměpisný	k2eAgInPc4d1	zeměpisný
rekordy	rekord	k1gInPc4	rekord
světa	svět	k1gInSc2	svět
Rekordy	rekord	k1gInPc4	rekord
pozemské	pozemský	k2eAgFnSc2d1	pozemská
neživé	živý	k2eNgFnSc2d1	neživá
přírody	příroda	k1gFnSc2	příroda
Česká	český	k2eAgFnSc1d1	Česká
kniha	kniha	k1gFnSc1	kniha
rekordů	rekord	k1gInPc2	rekord
</s>
