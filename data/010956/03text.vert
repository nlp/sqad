<p>
<s>
Alan	Alan	k1gMnSc1	Alan
Jelizbarovič	Jelizbarovič	k1gMnSc1	Jelizbarovič
Dzagojev	Dzagojev	k1gMnSc1	Dzagojev
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
А	А	k?	А
Е	Е	k?	Е
Д	Д	k?	Д
<g/>
;	;	kIx,	;
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
Beslan	Beslan	k1gInSc1	Beslan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruský	ruský	k2eAgMnSc1d1	ruský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hraje	hrát	k5eAaImIp3nS	hrát
za	za	k7c4	za
klub	klub	k1gInSc4	klub
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klubová	klubový	k2eAgFnSc1d1	klubová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Krylja	Krylj	k2eAgFnSc1d1	Krylj
Sovetov-SOK	Sovetov-SOK	k1gFnSc1	Sovetov-SOK
===	===	k?	===
</s>
</p>
<p>
<s>
Dzagojev	Dzagojet	k5eAaPmDgInS	Dzagojet
začínal	začínat	k5eAaImAgInS	začínat
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
kariérou	kariéra	k1gFnSc7	kariéra
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
druholigovém	druholigový	k2eAgInSc6d1	druholigový
týmu	tým	k1gInSc6	tým
Křídla	křídlo	k1gNnSc2	křídlo
Sovětů	Sovět	k1gMnPc2	Sovět
(	(	kIx(	(
<g/>
Krylja	Krylja	k1gMnSc1	Krylja
Sovetov-SOK	Sovetov-SOK	k1gMnSc1	Sovetov-SOK
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
profesionální	profesionální	k2eAgInSc1d1	profesionální
debut	debut	k1gInSc1	debut
si	se	k3xPyFc3	se
odbyl	odbýt	k5eAaPmAgInS	odbýt
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
domácí	domácí	k2eAgFnSc7d1	domácí
porážkou	porážka	k1gFnSc7	porážka
1-2	[number]	k4	1-2
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
FK	FK	kA	FK
Tjumen	Tjumen	k1gInSc1	Tjumen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klubu	klub	k1gInSc6	klub
strávil	strávit	k5eAaPmAgMnS	strávit
Dzagojev	Dzagojev	k1gFnSc2	Dzagojev
dvě	dva	k4xCgFnPc4	dva
sezony	sezona	k1gFnPc1	sezona
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
odehrál	odehrát	k5eAaPmAgMnS	odehrát
37	[number]	k4	37
utkání	utkání	k1gNnSc2	utkání
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
šest	šest	k4xCc4	šest
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
===	===	k?	===
</s>
</p>
<p>
<s>
Alan	Alan	k1gMnSc1	Alan
před	před	k7c7	před
sezonou	sezona	k1gFnSc7	sezona
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
zamířil	zamířit	k5eAaPmAgMnS	zamířit
do	do	k7c2	do
prvoligového	prvoligový	k2eAgInSc2d1	prvoligový
týmu	tým	k1gInSc2	tým
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
debut	debut	k1gInSc1	debut
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
v	v	k7c4	v
utkání	utkání	k1gNnSc4	utkání
proti	proti	k7c3	proti
FK	FK	kA	FK
Chimki	Chimki	k1gNnPc4	Chimki
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
jednu	jeden	k4xCgFnSc4	jeden
branku	branka	k1gFnSc4	branka
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
dvě	dva	k4xCgFnPc4	dva
asistence	asistence	k1gFnPc4	asistence
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
skvělý	skvělý	k2eAgInSc1d1	skvělý
výkon	výkon	k1gInSc1	výkon
podal	podat	k5eAaPmAgInS	podat
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
hlavnímu	hlavní	k2eAgMnSc3d1	hlavní
rivalovi	rival	k1gMnSc3	rival
Spartaku	Spartak	k1gInSc2	Spartak
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
svými	svůj	k3xOyFgMnPc7	svůj
třemi	tři	k4xCgFnPc7	tři
asistencemi	asistence	k1gFnPc7	asistence
dopomohl	dopomoct	k5eAaPmAgInS	dopomoct
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
výkonu	výkon	k1gInSc3	výkon
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
pevnou	pevný	k2eAgFnSc4d1	pevná
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
týmu	tým	k1gInSc2	tým
CSKA	CSKA	kA	CSKA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2008	[number]	k4	2008
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
poprvé	poprvé	k6eAd1	poprvé
ruský	ruský	k2eAgInSc4d1	ruský
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezony	sezona	k1gFnSc2	sezona
byl	být	k5eAaImAgInS	být
Ruskou	ruský	k2eAgFnSc7d1	ruská
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
unií	unie	k1gFnSc7	unie
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
mladý	mladý	k2eAgMnSc1d1	mladý
hráč	hráč	k1gMnSc1	hráč
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezoně	sezona	k1gFnSc6	sezona
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
Ligu	liga	k1gFnSc4	liga
Mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
gólem	gól	k1gInSc7	gól
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
svému	svůj	k3xOyFgInSc3	svůj
týmu	tým	k1gInSc3	tým
k	k	k7c3	k
remíze	remíza	k1gFnSc3	remíza
3-3	[number]	k4	3-3
s	s	k7c7	s
Manchesterem	Manchester	k1gInSc7	Manchester
United	United	k1gMnSc1	United
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
do	do	k7c2	do
osmifinále	osmifinále	k1gNnSc2	osmifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
duelu	duel	k1gInSc6	duel
na	na	k7c6	na
domácím	domácí	k2eAgNnSc6d1	domácí
hřišti	hřiště	k1gNnSc6	hřiště
remizovali	remizovat	k5eAaPmAgMnP	remizovat
s	s	k7c7	s
Realem	Real	k1gInSc7	Real
Madrid	Madrid	k1gInSc4	Madrid
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dzagojev	Dzagojet	k5eAaPmDgInS	Dzagojet
v	v	k7c6	v
onom	onen	k3xDgNnSc6	onen
utkání	utkání	k1gNnSc1	utkání
svým	svůj	k3xOyFgNnSc7	svůj
centrem	centrum	k1gNnSc7	centrum
přihrál	přihrát	k5eAaPmAgMnS	přihrát
na	na	k7c4	na
vyrovnávací	vyrovnávací	k2eAgInSc4d1	vyrovnávací
gól	gól	k1gInSc4	gól
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
nadějný	nadějný	k2eAgInSc4d1	nadějný
výsledek	výsledek	k1gInSc4	výsledek
CSKA	CSKA	kA	CSKA
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
odvetném	odvetný	k2eAgNnSc6d1	odvetné
utkání	utkání	k1gNnSc6	utkání
prohráli	prohrát	k5eAaPmAgMnP	prohrát
vysoko	vysoko	k6eAd1	vysoko
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
CSKA	CSKA	kA	CSKA
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgInS	představit
i	i	k9	i
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
D	D	kA	D
Ligy	liga	k1gFnPc1	liga
mistrů	mistr	k1gMnPc2	mistr
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgInSc1d1	ruský
tým	tým	k1gInSc1	tým
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
obsadil	obsadit	k5eAaPmAgMnS	obsadit
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
3	[number]	k4	3
bodů	bod	k1gInPc2	bod
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Viktorii	Viktoria	k1gFnSc3	Viktoria
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
postup	postup	k1gInSc4	postup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
vedl	vést	k5eAaImAgInS	vést
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Dzagojev	Dzagojet	k5eAaPmDgInS	Dzagojet
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
pětadvacetiminutovce	pětadvacetiminutovka	k1gFnSc6	pětadvacetiminutovka
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
a	a	k8xC	a
nesl	nést	k5eAaImAgMnS	nést
tak	tak	k6eAd1	tak
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
konané	konaný	k2eAgFnSc2d1	konaná
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
otevíral	otevírat	k5eAaImAgMnS	otevírat
skóre	skóre	k1gNnSc4	skóre
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
soupeř	soupeř	k1gMnSc1	soupeř
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
nešlo	jít	k5eNaImAgNnS	jít
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
ani	ani	k8xC	ani
Německo	Německo	k1gNnSc1	Německo
již	již	k6eAd1	již
nemohlo	moct	k5eNaImAgNnS	moct
postoupit	postoupit	k5eAaPmF	postoupit
do	do	k7c2	do
vyřazovací	vyřazovací	k2eAgFnSc2d1	vyřazovací
fáze	fáze	k1gFnSc2	fáze
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
skončilo	skončit	k5eAaPmAgNnS	skončit
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
porážkami	porážka	k1gFnPc7	porážka
a	a	k8xC	a
bez	bez	k7c2	bez
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
posledním	poslední	k2eAgNnSc6d1	poslední
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
A-mužstvo	Aužstvo	k1gNnSc1	A-mužstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skvělých	skvělý	k2eAgInPc6d1	skvělý
výkonech	výkon	k1gInPc6	výkon
na	na	k7c6	na
klubové	klubový	k2eAgFnSc6d1	klubová
úrovni	úroveň	k1gFnSc6	úroveň
byl	být	k5eAaImAgMnS	být
Dzagojev	Dzagojev	k1gMnSc1	Dzagojev
poprvé	poprvé	k6eAd1	poprvé
povolán	povolat	k5eAaPmNgMnS	povolat
do	do	k7c2	do
ruské	ruský	k2eAgFnSc2d1	ruská
reprezentace	reprezentace	k1gFnSc2	reprezentace
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Rusko	Rusko	k1gNnSc1	Rusko
hrálo	hrát	k5eAaImAgNnS	hrát
kvalifikační	kvalifikační	k2eAgInSc4d1	kvalifikační
duel	duel	k1gInSc4	duel
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
o	o	k7c4	o
postup	postup	k1gInSc4	postup
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Světa	svět	k1gInSc2	svět
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Dzagojev	Dzagojet	k5eAaPmDgInS	Dzagojet
se	se	k3xPyFc4	se
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
dostal	dostat	k5eAaPmAgMnS	dostat
jako	jako	k9	jako
náhradník	náhradník	k1gMnSc1	náhradník
při	při	k7c6	při
poločasovém	poločasový	k2eAgNnSc6d1	poločasové
střídání	střídání	k1gNnSc6	střídání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
116	[number]	k4	116
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
historicky	historicky	k6eAd1	historicky
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
hráčem	hráč	k1gMnSc7	hráč
ruské	ruský	k2eAgFnSc2d1	ruská
reprezentace	reprezentace	k1gFnSc2	reprezentace
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
druhým	druhý	k4xOgMnSc7	druhý
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
z	z	k7c2	z
CSKA	CSKA	kA	CSKA
brankáři	brankář	k1gMnSc3	brankář
Igoru	Igor	k1gMnSc3	Igor
Akinfejevovi	Akinfejeva	k1gMnSc3	Akinfejeva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
v	v	k7c6	v
reprezentaci	reprezentace	k1gFnSc6	reprezentace
dal	dát	k5eAaPmAgInS	dát
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
ve	v	k7c6	v
vítězném	vítězný	k2eAgNnSc6d1	vítězné
utkání	utkání	k1gNnSc6	utkání
3-2	[number]	k4	3-2
proti	proti	k7c3	proti
Irsku	Irsko	k1gNnSc3	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
EURO	euro	k1gNnSc4	euro
2012	[number]	k4	2012
přidal	přidat	k5eAaPmAgInS	přidat
ještě	ještě	k9	ještě
tři	tři	k4xCgFnPc4	tři
branky	branka	k1gFnPc4	branka
–	–	k?	–
jednu	jeden	k4xCgFnSc4	jeden
při	při	k7c6	při
vítězství	vítězství	k1gNnSc6	vítězství
1-0	[number]	k4	1-0
nad	nad	k7c7	nad
Slovenskem	Slovensko	k1gNnSc7	Slovensko
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
při	při	k7c6	při
vítězství	vítězství	k1gNnSc6	vítězství
6-0	[number]	k4	6-0
nad	nad	k7c7	nad
Andorrou	Andorra	k1gFnSc7	Andorra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
celkovými	celkový	k2eAgFnPc7d1	celková
4	[number]	k4	4
góly	gól	k1gInPc7	gól
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
společně	společně	k6eAd1	společně
s	s	k7c7	s
Romanem	Roman	k1gMnSc7	Roman
Pavljučenkem	Pavljučenek	k1gMnSc7	Pavljučenek
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
cyklu	cyklus	k1gInSc6	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
s	s	k7c7	s
23	[number]	k4	23
body	bod	k1gInPc7	bod
na	na	k7c6	na
konečném	konečný	k2eAgNnSc6d1	konečné
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
tabulky	tabulka	k1gFnSc2	tabulka
a	a	k8xC	a
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
šampionát	šampionát	k1gInSc4	šampionát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
EURO	euro	k1gNnSc4	euro
2012	[number]	k4	2012
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
finálovém	finálový	k2eAgInSc6d1	finálový
turnaji	turnaj	k1gInSc6	turnaj
EURO	euro	k1gNnSc1	euro
2012	[number]	k4	2012
konaném	konaný	k2eAgNnSc6d1	konané
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dvěma	dva	k4xCgFnPc7	dva
brankami	branka	k1gFnPc7	branka
dopomohl	dopomoct	k5eAaPmAgInS	dopomoct
k	k	k7c3	k
porážce	porážka	k1gFnSc3	porážka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
domácímu	domácí	k2eAgNnSc3d1	domácí
Polsku	Polsko	k1gNnSc3	Polsko
otevíral	otevírat	k5eAaImAgInS	otevírat
skóre	skóre	k1gNnSc4	skóre
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
utkání	utkání	k1gNnSc1	utkání
skončilo	skončit	k5eAaPmAgNnS	skončit
pouze	pouze	k6eAd1	pouze
remízou	remíza	k1gFnSc7	remíza
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
výborném	výborný	k2eAgInSc6d1	výborný
startu	start	k1gInSc6	start
nepostoupilo	postoupit	k5eNaPmAgNnS	postoupit
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
zápase	zápas	k1gInSc6	zápas
podlehlo	podlehnout	k5eAaPmAgNnS	podlehnout
Řecku	Řecko	k1gNnSc6	Řecko
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
vstřelenými	vstřelený	k2eAgFnPc7d1	vstřelená
brankami	branka	k1gFnPc7	branka
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
šestici	šestice	k1gFnSc6	šestice
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
střelců	střelec	k1gMnPc2	střelec
EURA	euro	k1gNnSc2	euro
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gNnSc4	on
ještě	ještě	k6eAd1	ještě
Mario	Mario	k1gMnSc1	Mario
Gómez	Gómez	k1gMnSc1	Gómez
<g/>
,	,	kIx,	,
Mario	Mario	k1gMnSc1	Mario
Balotelli	Balotelle	k1gFnSc4	Balotelle
<g/>
,	,	kIx,	,
Mario	Mario	k1gMnSc1	Mario
Mandžukić	Mandžukić	k1gMnSc1	Mandžukić
<g/>
,	,	kIx,	,
Cristiano	Cristiana	k1gFnSc5	Cristiana
Ronaldo	Ronalda	k1gFnSc5	Ronalda
<g/>
,	,	kIx,	,
Fernando	Fernanda	k1gFnSc5	Fernanda
Torres	Torresa	k1gFnPc2	Torresa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Klubové	klubový	k2eAgMnPc4d1	klubový
===	===	k?	===
</s>
</p>
<p>
<s>
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
Premier	Premier	k1gInSc1	Premier
Liga	liga	k1gFnSc1	liga
<g/>
:	:	kIx,	:
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
</s>
</p>
<p>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
pohár	pohár	k1gInSc1	pohár
<g/>
:	:	kIx,	:
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
</s>
</p>
<p>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
Superpohár	superpohár	k1gInSc1	superpohár
<g/>
:	:	kIx,	:
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
===	===	k?	===
Individuální	individuální	k2eAgFnSc1d1	individuální
===	===	k?	===
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
Premier	Premier	k1gInSc1	Premier
Liga	liga	k1gFnSc1	liga
–	–	k?	–
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
mladý	mladý	k2eAgMnSc1d1	mladý
hráč	hráč	k1gMnSc1	hráč
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alan	Alan	k1gMnSc1	Alan
Dzagojev	Dzagojev	k1gFnSc7	Dzagojev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c4	na
Transfermarkt	Transfermarkt	k1gInSc4	Transfermarkt
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c4	na
National-Football-Teams	National-Football-Teams	k1gInSc4	National-Football-Teams
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
–	–	k?	–
reprezentační	reprezentační	k2eAgFnPc1d1	reprezentační
statistiky	statistika	k1gFnPc1	statistika
v	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
<g/>
,	,	kIx,	,
eu-football	euootball	k1gMnSc1	eu-football
<g/>
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
