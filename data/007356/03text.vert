<s>
Smoking	smoking	k1gInSc1	smoking
je	být	k5eAaImIp3nS	být
pánský	pánský	k2eAgInSc4d1	pánský
večerní	večerní	k2eAgInSc4d1	večerní
formální	formální	k2eAgInSc4d1	formální
oblek	oblek	k1gInSc4	oblek
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglosaské	anglosaský	k2eAgFnSc6d1	anglosaská
terminologii	terminologie	k1gFnSc6	terminologie
semi-formal	semiormat	k5eAaBmAgMnS	semi-format
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
formal	formal	k1gInSc1	formal
je	být	k5eAaImIp3nS	být
frak	frak	k1gInSc4	frak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
forma	forma	k1gFnSc1	forma
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zejména	zejména	k9	zejména
černé	černý	k2eAgNnSc1d1	černé
vlněné	vlněný	k2eAgNnSc1d1	vlněné
sako	sako	k1gNnSc1	sako
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
tmavě	tmavě	k6eAd1	tmavě
modré	modrý	k2eAgNnSc1d1	modré
<g/>
)	)	kIx)	)
a	a	k8xC	a
kalhoty	kalhoty	k1gFnPc4	kalhoty
stejné	stejný	k2eAgFnSc2d1	stejná
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Specifikem	specifikon	k1gNnSc7	specifikon
smokingu	smoking	k1gInSc2	smoking
oproti	oproti	k7c3	oproti
běžnému	běžný	k2eAgInSc3d1	běžný
černému	černý	k2eAgInSc3d1	černý
obleku	oblek	k1gInSc3	oblek
je	být	k5eAaImIp3nS	být
lesklý	lesklý	k2eAgMnSc1d1	lesklý
(	(	kIx(	(
<g/>
tradičně	tradičně	k6eAd1	tradičně
hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
<g/>
)	)	kIx)	)
límec	límec	k1gInSc1	límec
saka	sako	k1gNnSc2	sako
a	a	k8xC	a
lampasy	lampas	k1gInPc7	lampas
na	na	k7c6	na
kalhotách	kalhoty	k1gFnPc6	kalhoty
<g/>
.	.	kIx.	.
</s>
<s>
Límec	límec	k1gInSc1	límec
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
buď	buď	k8xC	buď
špičatý	špičatý	k2eAgInSc1d1	špičatý
nebo	nebo	k8xC	nebo
šálový	šálový	k2eAgInSc1d1	šálový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
nosí	nosit	k5eAaImIp3nP	nosit
smokingy	smoking	k1gInPc1	smoking
s	s	k7c7	s
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
oblekovým	oblekový	k2eAgInSc7d1	oblekový
límcem	límec	k1gInSc7	límec
<g/>
.	.	kIx.	.
</s>
<s>
Zapínání	zapínání	k1gNnSc1	zapínání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
jednořadové	jednořadový	k2eAgInPc4d1	jednořadový
nebo	nebo	k8xC	nebo
dvouřadové	dvouřadový	k2eAgInPc4d1	dvouřadový
<g/>
.	.	kIx.	.
</s>
<s>
Jednořadové	jednořadový	k2eAgNnSc1d1	jednořadové
zapínání	zapínání	k1gNnSc1	zapínání
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
knoflík	knoflík	k1gInSc4	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Knoflíky	knoflík	k1gInPc1	knoflík
smokingu	smoking	k1gInSc2	smoking
jsou	být	k5eAaImIp3nP	být
potaženy	potáhnout	k5eAaPmNgFnP	potáhnout
stejnou	stejný	k2eAgFnSc7d1	stejná
lesklou	lesklý	k2eAgFnSc7d1	lesklá
látkou	látka	k1gFnSc7	látka
jako	jako	k8xC	jako
límec	límec	k1gInSc1	límec
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
smokingu	smoking	k1gInSc3	smoking
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
patří	patřit	k5eAaImIp3nS	patřit
černý	černý	k2eAgInSc1d1	černý
hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
motýlek	motýlek	k1gInSc1	motýlek
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
black	black	k1gInSc1	black
tie	tie	k?	tie
<g/>
)	)	kIx)	)
a	a	k8xC	a
vesta	vesta	k1gFnSc1	vesta
nebo	nebo	k8xC	nebo
smokingový	smokingový	k2eAgInSc1d1	smokingový
pás	pás	k1gInSc1	pás
sloužící	sloužící	k2eAgInSc1d1	sloužící
k	k	k7c3	k
zakrytí	zakrytí	k1gNnSc3	zakrytí
pasu	pas	k1gInSc2	pas
<g/>
.	.	kIx.	.
</s>
<s>
Smokingová	smokingový	k2eAgFnSc1d1	smokingová
vesta	vesta	k1gFnSc1	vesta
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
s	s	k7c7	s
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
vykrojením	vykrojení	k1gNnSc7	vykrojení
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
U	U	kA	U
nebo	nebo	k8xC	nebo
V	V	kA	V
a	a	k8xC	a
mívá	mívat	k5eAaImIp3nS	mívat
šálové	šálový	k2eAgFnPc4d1	Šálová
klopy	klopa	k1gFnPc4	klopa
<g/>
.	.	kIx.	.
</s>
<s>
Smokingový	smokingový	k2eAgInSc1d1	smokingový
pás	pás	k1gInSc1	pás
je	být	k5eAaImIp3nS	být
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
z	z	k7c2	z
hedvábí	hedvábí	k1gNnSc2	hedvábí
a	a	k8xC	a
krom	krom	k7c2	krom
klasické	klasický	k2eAgFnSc2d1	klasická
černé	černá	k1gFnSc2	černá
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
jinou	jiný	k2eAgFnSc4d1	jiná
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vínová	vínový	k2eAgFnSc1d1	vínová
nebo	nebo	k8xC	nebo
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smokingová	smokingový	k2eAgFnSc1d1	smokingová
košile	košile	k1gFnSc1	košile
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
s	s	k7c7	s
dvojitými	dvojitý	k2eAgFnPc7d1	dvojitá
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
<g/>
francouzskými	francouzský	k2eAgFnPc7d1	francouzská
manžetami	manžeta	k1gFnPc7	manžeta
na	na	k7c4	na
manžetové	manžetový	k2eAgInPc4d1	manžetový
knoflíčky	knoflíček	k1gInPc4	knoflíček
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
zpevněnou	zpevněný	k2eAgFnSc4d1	zpevněná
náprsenku	náprsenka	k1gFnSc4	náprsenka
a	a	k8xC	a
buď	buď	k8xC	buď
krytou	krytý	k2eAgFnSc4d1	krytá
légu	léga	k1gFnSc4	léga
nebo	nebo	k8xC	nebo
ozdobné	ozdobný	k2eAgInPc1d1	ozdobný
knoflíčky	knoflíček	k1gInPc1	knoflíček
zapínání	zapínání	k1gNnSc2	zapínání
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
je	on	k3xPp3gInPc4	on
nezakrývá	zakrývat	k5eNaImIp3nS	zakrývat
kravata	kravata	k1gFnSc1	kravata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Límeček	límeček	k1gInSc1	límeček
je	být	k5eAaImIp3nS	být
standardně	standardně	k6eAd1	standardně
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
ohrnutý	ohrnutý	k2eAgMnSc1d1	ohrnutý
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
ale	ale	k9	ale
nosit	nosit	k5eAaImF	nosit
i	i	k9	i
frakový	frakový	k2eAgInSc4d1	frakový
límeček	límeček	k1gInSc4	límeček
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odepínací	odepínací	k2eAgMnSc1d1	odepínací
<g/>
.	.	kIx.	.
</s>
<s>
Kalhoty	kalhoty	k1gFnPc1	kalhoty
drží	držet	k5eAaImIp3nP	držet
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
klasických	klasický	k2eAgFnPc2d1	klasická
šlí	šle	k1gFnPc2	šle
na	na	k7c4	na
knoflíčky	knoflíček	k1gInPc4	knoflíček
<g/>
.	.	kIx.	.
</s>
<s>
Pozor	pozor	k1gInSc1	pozor
<g/>
,	,	kIx,	,
u	u	k7c2	u
smokingu	smoking	k1gInSc2	smoking
je	být	k5eAaImIp3nS	být
lampas	lampas	k1gInSc1	lampas
na	na	k7c6	na
kalhotech	kalhot	k1gInPc6	kalhot
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgMnSc1	jeden
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
dvěma	dva	k4xCgFnPc7	dva
na	na	k7c6	na
fraku	frak	k1gInSc2	frak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ponožky	ponožka	k1gFnPc1	ponožka
bývají	bývat	k5eAaImIp3nP	bývat
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
a	a	k8xC	a
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
z	z	k7c2	z
černého	černý	k2eAgNnSc2d1	černé
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnějšími	vhodný	k2eAgFnPc7d3	nejvhodnější
botami	bota	k1gFnPc7	bota
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
lakýrky	lakýrka	k1gFnPc1	lakýrka
(	(	kIx(	(
<g/>
lakované	lakovaný	k2eAgFnPc1d1	lakovaná
polobotky	polobotka	k1gFnPc1	polobotka
s	s	k7c7	s
uzavřeným	uzavřený	k2eAgNnSc7d1	uzavřené
šněrováním	šněrování	k1gNnSc7	šněrování
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
operní	operní	k2eAgInPc4d1	operní
střevíce	střevíc	k1gInPc4	střevíc
připomínající	připomínající	k2eAgFnSc2d1	připomínající
dámské	dámský	k2eAgFnSc2d1	dámská
lodičky	lodička	k1gFnSc2	lodička
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ke	k	k7c3	k
smokingu	smoking	k1gInSc3	smoking
obouvat	obouvat	k5eAaImF	obouvat
i	i	k9	i
dobře	dobře	k6eAd1	dobře
vyleštěné	vyleštěný	k2eAgFnPc4d1	vyleštěná
polobotky	polobotka	k1gFnPc4	polobotka
z	z	k7c2	z
nelakované	lakovaný	k2eNgFnSc2d1	nelakovaná
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
v	v	k7c6	v
konzervativním	konzervativní	k2eAgNnSc6d1	konzervativní
prostředí	prostředí	k1gNnSc6	prostředí
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
lakýrky	lakýrka	k1gFnPc1	lakýrka
nejlepším	dobrý	k2eAgNnPc3d3	nejlepší
řešením	řešení	k1gNnPc3	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
chladného	chladný	k2eAgNnSc2d1	chladné
počasí	počasí	k1gNnSc2	počasí
ke	k	k7c3	k
smokingu	smoking	k1gInSc3	smoking
patří	patřit	k5eAaImIp3nS	patřit
klasický	klasický	k2eAgInSc4d1	klasický
černý	černý	k2eAgInSc4d1	černý
svrchní	svrchní	k2eAgInSc4d1	svrchní
kabát	kabát	k1gInSc4	kabát
s	s	k7c7	s
klopami	klopa	k1gFnPc7	klopa
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
chesterfield	chesterfield	k1gInSc1	chesterfield
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
lze	lze	k6eAd1	lze
vzít	vzít	k5eAaPmF	vzít
černý	černý	k2eAgInSc4d1	černý
klobouk	klobouk	k1gInSc4	klobouk
typu	typ	k1gInSc2	typ
homburg	homburg	k1gInSc1	homburg
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgFnPc1d1	vhodná
jsou	být	k5eAaImIp3nP	být
šedé	šedý	k2eAgFnPc1d1	šedá
semišové	semišový	k2eAgFnPc1d1	semišová
rukavice	rukavice	k1gFnPc1	rukavice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kapsičky	kapsička	k1gFnSc2	kapsička
saka	sako	k1gNnSc2	sako
patří	patřit	k5eAaImIp3nS	patřit
kapesníček	kapesníček	k1gInSc1	kapesníček
–	–	k?	–
tradičně	tradičně	k6eAd1	tradičně
bílý	bílý	k2eAgInSc1d1	bílý
lněný	lněný	k2eAgInSc1d1	lněný
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
a	a	k8xC	a
barevný	barevný	k2eAgInSc1d1	barevný
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
klopy	klopa	k1gFnSc2	klopa
lze	lze	k6eAd1	lze
vetknout	vetknout	k5eAaPmF	vetknout
květinu	květina	k1gFnSc4	květina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
červený	červený	k2eAgInSc1d1	červený
nebo	nebo	k8xC	nebo
bílý	bílý	k2eAgInSc1d1	bílý
karafiát	karafiát	k1gInSc1	karafiát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
letní	letní	k2eAgNnPc4d1	letní
období	období	k1gNnPc4	období
a	a	k8xC	a
teplé	teplý	k2eAgInPc4d1	teplý
kraje	kraj	k1gInPc4	kraj
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
záměna	záměna	k1gFnSc1	záměna
klasického	klasický	k2eAgNnSc2d1	klasické
černého	černé	k1gNnSc2	černé
smokingového	smokingový	k2eAgNnSc2d1	smokingový
saka	sako	k1gNnSc2	sako
za	za	k7c4	za
bílé	bílý	k2eAgNnSc4d1	bílé
nebo	nebo	k8xC	nebo
krémové	krémový	k2eAgNnSc4d1	krémové
smokingové	smokingový	k2eAgNnSc4d1	smokingový
sako	sako	k1gNnSc4	sako
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
tzv.	tzv.	kA	tzv.
bílý	bílý	k2eAgInSc1d1	bílý
smoking	smoking	k1gInSc1	smoking
nemá	mít	k5eNaImIp3nS	mít
lesklé	lesklý	k2eAgFnPc4d1	lesklá
klopy	klopa	k1gFnPc4	klopa
<g/>
.	.	kIx.	.
</s>
<s>
Smoking	smoking	k1gInSc1	smoking
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
na	na	k7c4	na
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
večerní	večerní	k2eAgFnPc4d1	večerní
společenské	společenský	k2eAgFnPc4d1	společenská
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
večeře	večeře	k1gFnPc4	večeře
<g/>
,	,	kIx,	,
galavečery	galavečer	k1gInPc4	galavečer
<g/>
,	,	kIx,	,
předávání	předávání	k1gNnSc1	předávání
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
zahajování	zahajování	k1gNnSc1	zahajování
festivalů	festival	k1gInPc2	festival
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc2d1	divadelní
premiéry	premiéra	k1gFnSc2	premiéra
<g/>
,	,	kIx,	,
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
plesy	ples	k1gInPc4	ples
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c4	o
stupeň	stupeň	k1gInSc4	stupeň
slavnostnější	slavnostní	k2eAgInSc4d2	slavnostnější
a	a	k8xC	a
formálnější	formální	k2eAgInSc4d2	formálnější
než	než	k8xS	než
společenský	společenský	k2eAgInSc4d1	společenský
oblek	oblek	k1gInSc4	oblek
a	a	k8xC	a
o	o	k7c4	o
stupeň	stupeň	k1gInSc4	stupeň
méně	málo	k6eAd2	málo
než	než	k8xS	než
frak	frak	k1gInSc4	frak
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
oblékat	oblékat	k5eAaImF	oblékat
před	před	k7c7	před
šestou	šestý	k4xOgFnSc7	šestý
hodinou	hodina	k1gFnSc7	hodina
večerní	večerní	k2eAgFnSc7d1	večerní
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
smoking	smoking	k1gInSc1	smoking
postupně	postupně	k6eAd1	postupně
nahradil	nahradit	k5eAaPmAgInS	nahradit
frak	frak	k1gInSc1	frak
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
smoking	smoking	k1gInSc4	smoking
sám	sám	k3xTgMnSc1	sám
vytlačován	vytlačovat	k5eAaImNgInS	vytlačovat
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
tmavým	tmavý	k2eAgInSc7d1	tmavý
oblekem	oblek	k1gInSc7	oblek
s	s	k7c7	s
kravatou	kravata	k1gFnSc7	kravata
<g/>
.	.	kIx.	.
</s>
<s>
Jakýmsi	jakýsi	k3yIgInSc7	jakýsi
mezistupněm	mezistupeň	k1gInSc7	mezistupeň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
nedávno	nedávno	k6eAd1	nedávno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
smoking	smoking	k1gInSc1	smoking
nošený	nošený	k2eAgInSc1d1	nošený
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
kravatou	kravata	k1gFnSc7	kravata
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Smoking	smoking	k1gInSc1	smoking
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
smoking	smoking	k1gInSc1	smoking
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
http://twogentlemen.cz/244/smoking-a-frak/	[url]	k4	http://twogentlemen.cz/244/smoking-a-frak/
</s>
