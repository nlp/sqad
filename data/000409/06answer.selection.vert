<s>
Bronz	bronz	k1gInSc1	bronz
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
jako	jako	k8xS	jako
např.	např.	kA	např.
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
slitina	slitina	k1gFnSc1	slitina
nazývá	nazývat	k5eAaImIp3nS	nazývat
mosaz	mosaz	k1gFnSc4	mosaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
