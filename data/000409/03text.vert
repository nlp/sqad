<s>
Bronz	bronz	k1gInSc1	bronz
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
jako	jako	k8xC	jako
např.	např.	kA	např.
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
slitina	slitina	k1gFnSc1	slitina
nazývá	nazývat	k5eAaImIp3nS	nazývat
mosaz	mosaz	k1gFnSc4	mosaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
pro	pro	k7c4	pro
bronz	bronz	k1gInSc4	bronz
je	být	k5eAaImIp3nS	být
spěž	spěž	k1gFnSc1	spěž
<g/>
.	.	kIx.	.
</s>
<s>
Bronz	bronz	k1gInSc1	bronz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vlastnosti	vlastnost	k1gFnPc1	vlastnost
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
již	již	k6eAd1	již
v	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dominantní	dominantní	k2eAgNnSc4d1	dominantní
použití	použití	k1gNnSc4	použití
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
ozdob	ozdoba	k1gFnPc2	ozdoba
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
předmětů	předmět	k1gInPc2	předmět
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
jedna	jeden	k4xCgFnSc1	jeden
epocha	epocha	k1gFnSc1	epocha
pravěku	pravěk	k1gInSc2	pravěk
nazývá	nazývat	k5eAaImIp3nS	nazývat
dobou	doba	k1gFnSc7	doba
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
bronzu	bronz	k1gInSc2	bronz
znamenal	znamenat	k5eAaImAgInS	znamenat
velký	velký	k2eAgInSc1d1	velký
technologický	technologický	k2eAgInSc1d1	technologický
pokrok	pokrok	k1gInSc1	pokrok
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
v	v	k7c6	v
nemalé	malý	k2eNgFnSc6d1	nemalá
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
čistým	čistý	k2eAgInPc3d1	čistý
kovům	kov	k1gInPc3	kov
se	se	k3xPyFc4	se
bronz	bronz	k1gInSc4	bronz
totiž	totiž	k9	totiž
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vyšší	vysoký	k2eAgFnSc7d2	vyšší
tvrdostí	tvrdost	k1gFnSc7	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
bronz	bronz	k1gInSc4	bronz
využívá	využívat	k5eAaPmIp3nS	využívat
jen	jen	k9	jen
pro	pro	k7c4	pro
speciální	speciální	k2eAgInPc4d1	speciální
účely	účel	k1gInPc4	účel
a	a	k8xC	a
v	v	k7c6	v
sochařství	sochařství	k1gNnSc6	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Cínový	cínový	k2eAgInSc4d1	cínový
bronz	bronz	k1gInSc4	bronz
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejvýše	nejvýše	k6eAd1	nejvýše
33	[number]	k4	33
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
součet	součet	k1gInSc1	součet
(	(	kIx(	(
<g/>
Cu	Cu	k1gFnSc1	Cu
+	+	kIx~	+
Sn	Sn	k1gFnSc1	Sn
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
99	[number]	k4	99
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Cínové	cínový	k2eAgInPc1d1	cínový
bronzy	bronz	k1gInPc1	bronz
používané	používaný	k2eAgInPc1d1	používaný
technicky	technicky	k6eAd1	technicky
mají	mít	k5eAaImIp3nP	mít
cínu	cín	k1gInSc3	cín
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
cínového	cínový	k2eAgInSc2d1	cínový
bronzu	bronz	k1gInSc2	bronz
je	být	k5eAaImIp3nS	být
složitá	složitý	k2eAgFnSc1d1	složitá
a	a	k8xC	a
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
rovnovážného	rovnovážný	k2eAgInSc2d1	rovnovážný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc1	slitina
obsahující	obsahující	k2eAgFnPc1d1	obsahující
až	až	k9	až
asi	asi	k9	asi
16	[number]	k4	16
%	%	kIx~	%
Sn	Sn	k1gFnPc1	Sn
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
při	při	k7c6	při
520	[number]	k4	520
°	°	k?	°
<g/>
C	C	kA	C
z	z	k7c2	z
tuhého	tuhý	k2eAgInSc2d1	tuhý
roztoku	roztok	k1gInSc2	roztok
α	α	k?	α
Pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
teplotou	teplota	k1gFnSc7	teplota
se	se	k3xPyFc4	se
rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
cínu	cín	k1gInSc2	cín
v	v	k7c6	v
mědi	měď	k1gFnSc6	měď
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
však	však	k8xC	však
nenastávají	nastávat	k5eNaImIp3nP	nastávat
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc4	žádný
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
značného	značný	k2eAgNnSc2d1	značné
rozpětí	rozpětí	k1gNnSc2	rozpětí
mezi	mezi	k7c7	mezi
likvidem	likvid	k1gInSc7	likvid
a	a	k8xC	a
solidem	solid	k1gInSc7	solid
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
normálním	normální	k2eAgNnSc6d1	normální
chladnutí	chladnutí	k1gNnSc6	chladnutí
značné	značný	k2eAgNnSc1d1	značné
odlučování	odlučování	k1gNnSc1	odlučování
v	v	k7c6	v
krystalech	krystal	k1gInPc6	krystal
α	α	k?	α
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyrovnání	vyrovnání	k1gNnSc1	vyrovnání
struktury	struktura	k1gFnSc2	struktura
difuzí	difuze	k1gFnPc2	difuze
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
jen	jen	k9	jen
dlouhodobým	dlouhodobý	k2eAgNnSc7d1	dlouhodobé
žíháním	žíhání	k1gNnSc7	žíhání
při	při	k7c6	při
550	[number]	k4	550
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
750	[number]	k4	750
°	°	k?	°
<g/>
C.	C.	kA	C.
Slitiny	slitina	k1gFnPc4	slitina
obsahující	obsahující	k2eAgFnPc4d1	obsahující
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
(	(	kIx(	(
<g/>
asi	asi	k9	asi
do	do	k7c2	do
8	[number]	k4	8
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
zpracovat	zpracovat	k5eAaPmF	zpracovat
tvářením	tváření	k1gNnSc7	tváření
<g/>
,	,	kIx,	,
slitiny	slitina	k1gFnSc2	slitina
s	s	k7c7	s
deseti	deset	k4xCc7	deset
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
procenty	procento	k1gNnPc7	procento
cínu	cín	k1gInSc6	cín
se	se	k3xPyFc4	se
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
litím	lití	k1gNnSc7	lití
<g/>
.	.	kIx.	.
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1	měrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
technicky	technicky	k6eAd1	technicky
použitelných	použitelný	k2eAgInPc2d1	použitelný
tvářených	tvářený	k2eAgInPc2d1	tvářený
cínových	cínový	k2eAgInPc2d1	cínový
bronzů	bronz	k1gInPc2	bronz
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
8,8	[number]	k4	8,8
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
dm	dm	kA	dm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
u	u	k7c2	u
bronzů	bronz	k1gInPc2	bronz
litých	litý	k2eAgNnPc2d1	Lité
asi	asi	k9	asi
8,6	[number]	k4	8,6
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
dm	dm	kA	dm
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
vodivost	vodivost	k1gFnSc1	vodivost
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
cín	cín	k1gInSc1	cín
ji	on	k3xPp3gFnSc4	on
značně	značně	k6eAd1	značně
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bronzů	bronz	k1gInPc2	bronz
s	s	k7c7	s
5	[number]	k4	5
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
10	[number]	k4	10
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
Ω	Ω	k?	Ω
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
u	u	k7c2	u
bronzů	bronz	k1gInPc2	bronz
s	s	k7c7	s
15	[number]	k4	15
%	%	kIx~	%
Sn	Sn	k1gFnSc1	Sn
asi	asi	k9	asi
5	[number]	k4	5
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
Ω	Ω	k?	Ω
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
cínových	cínový	k2eAgInPc2d1	cínový
bronzů	bronz	k1gInPc2	bronz
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
je	být	k5eAaImIp3nS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
jako	jako	k9	jako
u	u	k7c2	u
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Cínové	cínový	k2eAgInPc1d1	cínový
bronzy	bronz	k1gInPc1	bronz
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ve	v	k7c6	v
slévárenství	slévárenství	k1gNnSc6	slévárenství
a	a	k8xC	a
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
kluzných	kluzný	k2eAgNnPc2d1	kluzné
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Tvářené	tvářený	k2eAgInPc1d1	tvářený
cínové	cínový	k2eAgInPc1d1	cínový
bronzy	bronz	k1gInPc1	bronz
mívají	mívat	k5eAaImIp3nP	mívat
nejčastěji	často	k6eAd3	často
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
bronzu	bronz	k1gInSc2	bronz
dochází	docházet	k5eAaImIp3nS	docházet
často	často	k6eAd1	často
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
(	(	kIx(	(
<g/>
okysličení	okysličení	k1gNnSc2	okysličení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nepříznivě	příznivě	k6eNd1	příznivě
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
nežádoucí	žádoucí	k2eNgNnSc1d1	nežádoucí
kyslík	kyslík	k1gInSc4	kyslík
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Odkysličují	odkysličovat	k5eAaImIp3nP	odkysličovat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
fosforem	fosfor	k1gInSc7	fosfor
(	(	kIx(	(
<g/>
bronz	bronz	k1gInSc1	bronz
fosforový	fosforový	k2eAgInSc1d1	fosforový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
fosforovou	fosforový	k2eAgFnSc7d1	fosforová
mědí	měď	k1gFnSc7	měď
P	P	kA	P
<g/>
–	–	k?	–
<g/>
Cu	Cu	k1gMnSc1	Cu
(	(	kIx(	(
<g/>
42	[number]	k4	42
3018	[number]	k4	3018
asi	asi	k9	asi
s	s	k7c7	s
10	[number]	k4	10
%	%	kIx~	%
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
přísada	přísada	k1gFnSc1	přísada
několika	několik	k4yIc2	několik
setin	setina	k1gFnPc2	setina
procenta	procento	k1gNnSc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
vlastnosti	vlastnost	k1gFnPc4	vlastnost
má	mít	k5eAaImIp3nS	mít
fosforový	fosforový	k2eAgInSc1d1	fosforový
bronz	bronz	k1gInSc1	bronz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
odkysličení	odkysličení	k1gNnSc6	odkysličení
co	co	k9	co
nejméně	málo	k6eAd3	málo
fosforu	fosfor	k1gInSc2	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
požaduje	požadovat	k5eAaImIp3nS	požadovat
co	co	k9	co
největší	veliký	k2eAgFnSc4d3	veliký
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
opotřebení	opotřebení	k1gNnSc3	opotřebení
(	(	kIx(	(
<g/>
pružiny	pružina	k1gFnPc4	pružina
<g/>
,	,	kIx,	,
trubky	trubka	k1gFnPc4	trubka
na	na	k7c4	na
ložisková	ložiskový	k2eAgNnPc1d1	ložiskové
pouzdra	pouzdro	k1gNnPc1	pouzdro
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
asi	asi	k9	asi
0,3	[number]	k4	0,3
%	%	kIx~	%
fosforu	fosfor	k1gInSc2	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Plechy	plech	k1gInPc1	plech
a	a	k8xC	a
pásy	pás	k1gInPc1	pás
se	se	k3xPyFc4	se
válcují	válcovat	k5eAaImIp3nP	válcovat
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
z	z	k7c2	z
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
obsah	obsah	k1gInSc1	obsah
fosforu	fosfor	k1gInSc2	fosfor
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
nejvýše	nejvýše	k6eAd1	nejvýše
0,15	[number]	k4	0,15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc4d2	vhodnější
válcování	válcování	k1gNnSc4	válcování
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
<g/>
.	.	kIx.	.
</s>
<s>
Desky	deska	k1gFnPc1	deska
se	se	k3xPyFc4	se
před	před	k7c7	před
válcováním	válcování	k1gNnSc7	válcování
homogenizují	homogenizovat	k5eAaBmIp3nP	homogenizovat
žíháním	žíhání	k1gNnSc7	žíhání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válcování	válcování	k1gNnSc6	válcování
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
plechy	plech	k1gInPc4	plech
opět	opět	k6eAd1	opět
vyžíhat	vyžíhat	k5eAaBmF	vyžíhat
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
ochladit	ochladit	k5eAaPmF	ochladit
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	s	k7c7	s
plechy	plech	k1gInPc7	plech
tloušťky	tloušťka	k1gFnSc2	tloušťka
až	až	k6eAd1	až
0,1	[number]	k4	0,1
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Pérově	pérově	k6eAd1	pérově
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
plechy	plech	k1gInPc1	plech
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
Cu	Cu	k1gFnSc2	Cu
<g/>
–	–	k?	–
<g/>
Sn	Sn	k1gMnSc1	Sn
6	[number]	k4	6
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
k	k	k7c3	k
zhotovování	zhotovování	k1gNnSc3	zhotovování
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
kovově	kovově	k6eAd1	kovově
čisté	čistý	k2eAgNnSc1d1	čisté
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
plechy	plech	k1gInPc4	plech
žíhat	žíhat	k5eAaImF	žíhat
za	za	k7c2	za
nepřístupu	nepřístup	k1gInSc2	nepřístup
vzduchu	vzduch	k1gInSc2	vzduch
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
po	po	k7c6	po
žíhání	žíhání	k1gNnSc6	žíhání
moří	moře	k1gNnPc2	moře
ve	v	k7c6	v
zředěné	zředěný	k2eAgFnSc6d1	zředěná
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Bronzové	bronzový	k2eAgInPc1d1	bronzový
dráty	drát	k1gInPc1	drát
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
až	až	k9	až
do	do	k7c2	do
průměru	průměr	k1gInSc2	průměr
0,03	[number]	k4	0,03
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Cínové	cínový	k2eAgInPc1d1	cínový
bronzy	bronz	k1gInPc1	bronz
odolávají	odolávat	k5eAaImIp3nP	odolávat
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
opotřebení	opotřebení	k1gNnSc4	opotřebení
<g/>
.	.	kIx.	.
</s>
<s>
Ohřevem	ohřev	k1gInSc7	ohřev
bronzu	bronz	k1gInSc2	bronz
tvářeného	tvářený	k2eAgInSc2d1	tvářený
za	za	k7c7	za
studena	studeno	k1gNnSc2	studeno
na	na	k7c4	na
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
pomalu	pomalu	k6eAd1	pomalu
klesá	klesat	k5eAaImIp3nS	klesat
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnPc4d3	veliký
měkkosti	měkkost	k1gFnPc4	měkkost
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
žíháním	žíhání	k1gNnSc7	žíhání
na	na	k7c4	na
650	[number]	k4	650
až	až	k9	až
700	[number]	k4	700
°	°	k?	°
<g/>
C.	C.	kA	C.
Cínové	cínový	k2eAgInPc1d1	cínový
bronzy	bronz	k1gInPc1	bronz
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
litém	litý	k2eAgInSc6d1	litý
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
litých	litý	k2eAgInPc2d1	litý
bronzů	bronz	k1gInPc2	bronz
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
nestejnoměrná	stejnoměrný	k2eNgFnSc1d1	nestejnoměrná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chladnutí	chladnutí	k1gNnSc1	chladnutí
po	po	k7c6	po
odlití	odlití	k1gNnSc6	odlití
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rychlé	rychlý	k2eAgNnSc1d1	rychlé
<g/>
.	.	kIx.	.
</s>
<s>
Bronzy	bronz	k1gInPc1	bronz
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
v	v	k7c6	v
kelímkových	kelímkový	k2eAgFnPc6d1	kelímková
nebo	nebo	k8xC	nebo
plamenných	plamenný	k2eAgFnPc6d1	plamenná
pecích	pec	k1gFnPc6	pec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roztavené	roztavený	k2eAgFnSc2d1	roztavená
mědi	měď	k1gFnSc2	měď
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
ohřátý	ohřátý	k2eAgInSc1d1	ohřátý
cín	cín	k1gInSc1	cín
<g/>
.	.	kIx.	.
</s>
<s>
Taveninu	tavenina	k1gFnSc4	tavenina
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
účinky	účinek	k1gInPc7	účinek
vzduchu	vzduch	k1gInSc2	vzduch
vrstvou	vrstva	k1gFnSc7	vrstva
práškového	práškový	k2eAgNnSc2d1	práškové
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Odkysličuje	odkysličovat	k5eAaImIp3nS	odkysličovat
se	se	k3xPyFc4	se
fosforem	fosfor	k1gInSc7	fosfor
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
hořčíkem	hořčík	k1gInSc7	hořčík
<g/>
,	,	kIx,	,
křemíkem	křemík	k1gInSc7	křemík
<g/>
,	,	kIx,	,
manganem	mangan	k1gInSc7	mangan
aj.	aj.	kA	aj.
Hliníkový	hliníkový	k2eAgInSc4d1	hliníkový
bronz	bronz	k1gInSc4	bronz
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejčastěji	často	k6eAd3	často
5	[number]	k4	5
%	%	kIx~	%
Al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Hliník	hliník	k1gInSc1	hliník
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
tvrdost	tvrdost	k1gFnSc4	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
hliníku	hliník	k1gInSc2	hliník
asi	asi	k9	asi
do	do	k7c2	do
9	[number]	k4	9
%	%	kIx~	%
<g/>
,	,	kIx,	,
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
mědi	měď	k1gFnSc6	měď
a	a	k8xC	a
struktura	struktura	k1gFnSc1	struktura
slitiny	slitina	k1gFnSc2	slitina
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
krystalů	krystal	k1gInPc2	krystal
α	α	k?	α
Při	při	k7c6	při
větším	veliký	k2eAgInSc6d2	veliký
obsahu	obsah	k1gInSc6	obsah
Al	ala	k1gFnPc2	ala
vznikají	vznikat	k5eAaImIp3nP	vznikat
také	také	k9	také
křehké	křehký	k2eAgInPc1d1	křehký
krystaly	krystal	k1gInPc1	krystal
γ	γ	k?	γ
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
Cu	Cu	k1gFnPc2	Cu
<g/>
9	[number]	k4	9
<g/>
Al	ala	k1gFnPc2	ala
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
a	a	k8xC	a
slitina	slitina	k1gFnSc1	slitina
je	být	k5eAaImIp3nS	být
tvrdší	tvrdý	k2eAgFnSc1d2	tvrdší
a	a	k8xC	a
křehčí	křehký	k2eAgFnSc1d2	křehčí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vlastnosti	vlastnost	k1gFnPc4	vlastnost
hliníkových	hliníkový	k2eAgInPc2d1	hliníkový
bronzů	bronz	k1gInPc2	bronz
se	s	k7c7	s
strukturou	struktura	k1gFnSc7	struktura
složenou	složený	k2eAgFnSc7d1	složená
z	z	k7c2	z
krystalů	krystal	k1gInPc2	krystal
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
+	+	kIx~	+
<g/>
γ	γ	k?	γ
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
rychlost	rychlost	k1gFnSc1	rychlost
chladnutí	chladnutí	k1gNnSc2	chladnutí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rychlém	rychlý	k2eAgNnSc6d1	rychlé
ochlazení	ochlazení	k1gNnSc6	ochlazení
z	z	k7c2	z
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
má	mít	k5eAaImIp3nS	mít
slitina	slitina	k1gFnSc1	slitina
velkou	velký	k2eAgFnSc4d1	velká
pevnost	pevnost	k1gFnSc4	pevnost
(	(	kIx(	(
<g/>
asi	asi	k9	asi
80	[number]	k4	80
kp	kp	k?	kp
<g/>
/	/	kIx~	/
<g/>
mm	mm	kA	mm
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepatrnou	patrný	k2eNgFnSc4d1	patrný
tažnost	tažnost	k1gFnSc4	tažnost
a	a	k8xC	a
kontrakci	kontrakce	k1gFnSc4	kontrakce
<g/>
.	.	kIx.	.
</s>
<s>
Tvářená	tvářený	k2eAgFnSc1d1	tvářená
slitina	slitina	k1gFnSc1	slitina
Cu	Cu	k1gFnPc2	Cu
<g/>
–	–	k?	–
<g/>
Al	ala	k1gFnPc2	ala
5	[number]	k4	5
má	mít	k5eAaImIp3nS	mít
měrnou	měrný	k2eAgFnSc4d1	měrná
hmotnost	hmotnost	k1gFnSc4	hmotnost
asi	asi	k9	asi
8,2	[number]	k4	8,2
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
dm	dm	kA	dm
<g/>
3	[number]	k4	3
a	a	k8xC	a
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
vodivost	vodivost	k1gFnSc4	vodivost
asi	asi	k9	asi
7	[number]	k4	7
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
mm	mm	kA	mm
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
odolná	odolný	k2eAgFnSc1d1	odolná
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
,	,	kIx,	,
žáru	žár	k1gInSc6	žár
odolává	odolávat	k5eAaImIp3nS	odolávat
asi	asi	k9	asi
do	do	k7c2	do
800	[number]	k4	800
°	°	k?	°
<g/>
C.	C.	kA	C.
Kromě	kromě	k7c2	kromě
podvojných	podvojný	k2eAgFnPc2d1	podvojná
slitin	slitina	k1gFnPc2	slitina
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
bronzy	bronz	k1gInPc1	bronz
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
přísadovými	přísadový	k2eAgInPc7d1	přísadový
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
kyselinám	kyselina	k1gFnPc3	kyselina
a	a	k8xC	a
louhům	louh	k1gInPc3	louh
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
v	v	k7c6	v
agresivním	agresivní	k2eAgNnSc6d1	agresivní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
také	také	k6eAd1	také
potrubí	potrubí	k1gNnSc4	potrubí
a	a	k8xC	a
kohouty	kohout	k1gInPc4	kohout
pro	pro	k7c4	pro
přehřátou	přehřátý	k2eAgFnSc4d1	přehřátá
páru	pára	k1gFnSc4	pára
<g/>
.	.	kIx.	.
</s>
<s>
Manganové	manganový	k2eAgInPc1d1	manganový
bronzy	bronz	k1gInPc1	bronz
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
materiály	materiál	k1gInPc1	materiál
na	na	k7c4	na
měřicí	měřicí	k2eAgInPc4d1	měřicí
odpory	odpor	k1gInPc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Resistin	Resistin	k1gInSc1	Resistin
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc1d1	obsahující
asi	asi	k9	asi
15	[number]	k4	15
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
měrný	měrný	k2eAgInSc4d1	měrný
elektrický	elektrický	k2eAgInSc4d1	elektrický
odpor	odpor	k1gInSc4	odpor
asi	asi	k9	asi
0,5	[number]	k4	0,5
Ω	Ω	k?	Ω
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
m.	m.	k?	m.
Známější	známý	k2eAgInSc1d2	známější
je	být	k5eAaImIp3nS	být
manganin	manganin	k1gInSc1	manganin
(	(	kIx(	(
<g/>
Cu	Cu	k1gFnPc1	Cu
<g/>
–	–	k?	–
<g/>
Mn	Mn	k1gFnSc7	Mn
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
ČSN	ČSN	kA	ČSN
42	[number]	k4	42
3056	[number]	k4	3056
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
měrný	měrný	k2eAgInSc1d1	měrný
odpor	odpor	k1gInSc1	odpor
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
0,43	[number]	k4	0,43
Ω	Ω	k?	Ω
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
teplotní	teplotní	k2eAgInSc1d1	teplotní
součinitel	součinitel	k1gInSc1	součinitel
odporu	odpor	k1gInSc2	odpor
asi	asi	k9	asi
1,5	[number]	k4	1,5
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
měrná	měrný	k2eAgFnSc1d1	měrná
termoelektrická	termoelektrický	k2eAgFnSc1d1	termoelektrická
síla	síla	k1gFnSc1	síla
proti	proti	k7c3	proti
mědi	měď	k1gFnSc3	měď
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
(	(	kIx(	(
<g/>
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
V	V	kA	V
na	na	k7c4	na
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
správném	správný	k2eAgNnSc6d1	správné
umělém	umělý	k2eAgNnSc6d1	umělé
vystárnutí	vystárnutí	k1gNnSc6	vystárnutí
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
hodnota	hodnota	k1gFnSc1	hodnota
odporu	odpor	k1gInSc2	odpor
stálou	stálý	k2eAgFnSc4d1	stálá
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
úzkých	úzký	k2eAgFnPc6d1	úzká
mezích	mez	k1gFnPc6	mez
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Manganinové	manganinový	k2eAgInPc1d1	manganinový
odpory	odpor	k1gInPc1	odpor
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nP	hodit
proto	proto	k8xC	proto
na	na	k7c4	na
nejpřesnější	přesný	k2eAgInPc4d3	nejpřesnější
měřicí	měřicí	k2eAgInPc4d1	měřicí
odpory	odpor	k1gInPc4	odpor
(	(	kIx(	(
<g/>
etalony	etalon	k1gInPc4	etalon
<g/>
,	,	kIx,	,
kompenzační	kompenzační	k2eAgNnSc1d1	kompenzační
měření	měření	k1gNnSc1	měření
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
i	i	k8xC	i
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
malých	malý	k2eAgFnPc2d1	malá
hodnot	hodnota	k1gFnPc2	hodnota
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Umělým	umělý	k2eAgNnSc7d1	umělé
stárnutím	stárnutí	k1gNnSc7	stárnutí
se	se	k3xPyFc4	se
odstraní	odstranit	k5eAaPmIp3nS	odstranit
vliv	vliv	k1gInSc1	vliv
tváření	tváření	k1gNnSc2	tváření
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
a	a	k8xC	a
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
se	se	k3xPyFc4	se
ustálení	ustálení	k1gNnSc1	ustálení
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Umělé	umělý	k2eAgNnSc1d1	umělé
stárnutí	stárnutí	k1gNnSc1	stárnutí
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
ohřevu	ohřev	k1gInSc6	ohřev
na	na	k7c4	na
400	[number]	k4	400
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
1	[number]	k4	1
hodiny	hodina	k1gFnSc2	hodina
v	v	k7c6	v
neutrálním	neutrální	k2eAgNnSc6d1	neutrální
ovzduší	ovzduší	k1gNnSc6	ovzduší
(	(	kIx(	(
<g/>
v	v	k7c6	v
argonu	argon	k1gInSc6	argon
nebo	nebo	k8xC	nebo
dusíku	dusík	k1gInSc2	dusík
<g/>
)	)	kIx)	)
za	za	k7c2	za
vyššího	vysoký	k2eAgInSc2d2	vyšší
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
opatřením	opatření	k1gNnSc7	opatření
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
omezit	omezit	k5eAaPmF	omezit
vypařování	vypařování	k1gNnSc4	vypařování
manganu	mangan	k1gInSc2	mangan
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
drátu	drát	k1gInSc2	drát
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
projevovat	projevovat	k5eAaImF	projevovat
již	již	k6eAd1	již
od	od	k7c2	od
350	[number]	k4	350
°	°	k?	°
<g/>
C.	C.	kA	C.
Po	po	k7c6	po
pomalém	pomalý	k2eAgNnSc6d1	pomalé
vychladnutí	vychladnutí	k1gNnSc6	vychladnutí
se	se	k3xPyFc4	se
drát	drát	k1gInSc1	drát
moří	mořit	k5eAaImIp3nS	mořit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odstranila	odstranit	k5eAaPmAgFnS	odstranit
povrchová	povrchový	k2eAgFnSc1d1	povrchová
vrstva	vrstva	k1gFnSc1	vrstva
ochuzená	ochuzený	k2eAgFnSc1d1	ochuzená
o	o	k7c4	o
mangan	mangan	k1gInSc4	mangan
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
drát	drát	k1gInSc1	drát
uloží	uložit	k5eAaPmIp3nS	uložit
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
novějším	nový	k2eAgInSc7d2	novější
způsobem	způsob	k1gInSc7	způsob
umělého	umělý	k2eAgNnSc2d1	umělé
stárnutí	stárnutí	k1gNnSc2	stárnutí
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
ještě	ještě	k9	ještě
menší	malý	k2eAgFnSc1d2	menší
hodnota	hodnota	k1gFnSc1	hodnota
teplotního	teplotní	k2eAgMnSc2d1	teplotní
činitele	činitel	k1gMnSc2	činitel
odporu	odpor	k1gInSc2	odpor
než	než	k8xS	než
při	při	k7c6	při
starším	starý	k2eAgInSc6d2	starší
způsobu	způsob	k1gInSc6	způsob
umělého	umělý	k2eAgNnSc2d1	umělé
stárnutí	stárnutí	k1gNnSc2	stárnutí
(	(	kIx(	(
<g/>
ohřev	ohřev	k1gInSc1	ohřev
na	na	k7c4	na
140	[number]	k4	140
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manganin	manganin	k1gInSc1	manganin
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nS	hodit
i	i	k9	i
na	na	k7c4	na
odporové	odporový	k2eAgInPc4d1	odporový
manometry	manometr	k1gInPc4	manometr
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
princip	princip	k1gInSc1	princip
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tlakem	tlak	k1gInSc7	tlak
mění	měnit	k5eAaImIp3nS	měnit
odpor	odpor	k1gInSc4	odpor
slitiny	slitina	k1gFnSc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Tlakový	tlakový	k2eAgMnSc1d1	tlakový
činitel	činitel	k1gMnSc1	činitel
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2,60	[number]	k4	2,60
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
na	na	k7c4	na
1	[number]	k4	1
kp	kp	k?	kp
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
po	po	k7c6	po
umělém	umělý	k2eAgNnSc6d1	umělé
stárnutí	stárnutí	k1gNnSc6	stárnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgInPc1d1	čistý
kovy	kov	k1gInPc1	kov
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
nehodí	hodit	k5eNaPmIp3nS	hodit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gInSc1	jejich
odpor	odpor	k1gInSc1	odpor
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Isabelin	Isabelin	k2eAgMnSc1d1	Isabelin
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
13	[number]	k4	13
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
3	[number]	k4	3
%	%	kIx~	%
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
měď	měď	k1gFnSc4	měď
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
hliníku	hliník	k1gInSc2	hliník
znesnadňuje	znesnadňovat	k5eAaImIp3nS	znesnadňovat
měkké	měkký	k2eAgNnSc1d1	měkké
pájení	pájení	k1gNnSc1	pájení
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
i	i	k8xC	i
vlastnosti	vlastnost	k1gFnPc4	vlastnost
má	mít	k5eAaImIp3nS	mít
novokonstant	novokonstant	k1gMnSc1	novokonstant
(	(	kIx(	(
<g/>
12	[number]	k4	12
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
4	[number]	k4	4
%	%	kIx~	%
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
1,5	[number]	k4	1,5
%	%	kIx~	%
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
měď	měď	k1gFnSc1	měď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodné	pozoruhodný	k2eAgFnPc1d1	pozoruhodná
jsou	být	k5eAaImIp3nP	být
slitiny	slitina	k1gFnPc1	slitina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
při	při	k7c6	při
20	[number]	k4	20
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
procentech	procento	k1gNnPc6	procento
manganu	mangan	k1gInSc2	mangan
přes	přes	k7c4	přes
9	[number]	k4	9
%	%	kIx~	%
Al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tzv.	tzv.	kA	tzv.
Heuslerovy	Heuslerův	k2eAgFnPc4d1	Heuslerův
slitiny	slitina	k1gFnPc4	slitina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
feromagnetické	feromagnetický	k2eAgFnPc1d1	feromagnetická
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádné	žádný	k3yNgNnSc4	žádný
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
odporové	odporový	k2eAgFnPc1d1	odporová
slitiny	slitina	k1gFnPc1	slitina
na	na	k7c4	na
měřicí	měřicí	k2eAgInPc4d1	měřicí
odpory	odpor	k1gInPc4	odpor
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
bronzy	bronz	k1gInPc4	bronz
niklové	niklový	k2eAgInPc4d1	niklový
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
konstantan	konstantan	k1gInSc4	konstantan
(	(	kIx(	(
<g/>
Cu	Cu	k1gMnSc3	Cu
<g/>
–	–	k?	–
<g/>
Ni	on	k3xPp3gFnSc4	on
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
Mn	Mn	k1gFnPc2	Mn
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
ČSN	ČSN	kA	ČSN
42	[number]	k4	42
3065	[number]	k4	3065
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měrný	měrný	k2eAgInSc1d1	měrný
elektrický	elektrický	k2eAgInSc1d1	elektrický
odpor	odpor	k1gInSc1	odpor
konstantanu	konstantan	k1gInSc2	konstantan
se	se	k3xPyFc4	se
ve	v	k7c6	v
značném	značný	k2eAgInSc6d1	značný
teplotním	teplotní	k2eAgInSc6d1	teplotní
rozsahu	rozsah	k1gInSc6	rozsah
mění	měnit	k5eAaImIp3nS	měnit
velmi	velmi	k6eAd1	velmi
nepatrně	patrně	k6eNd1	patrně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
zpočátku	zpočátku	k6eAd1	zpočátku
poněkud	poněkud	k6eAd1	poněkud
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgMnSc1d1	teplotní
součinitel	součinitel	k1gMnSc1	součinitel
elektrického	elektrický	k2eAgInSc2d1	elektrický
odporu	odpor	k1gInSc2	odpor
mezi	mezi	k7c7	mezi
0	[number]	k4	0
a	a	k8xC	a
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
konstantanu	konstantan	k1gInSc2	konstantan
nemá	mít	k5eNaImIp3nS	mít
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
500	[number]	k4	500
°	°	k?	°
<g/>
C.	C.	kA	C.
Konstantan	konstantan	k1gInSc1	konstantan
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
regulační	regulační	k2eAgFnPc4d1	regulační
a	a	k8xC	a
méně	málo	k6eAd2	málo
náročné	náročný	k2eAgInPc1d1	náročný
měřicí	měřicí	k2eAgInPc1d1	měřicí
odpory	odpor	k1gInPc1	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
materiálem	materiál	k1gInSc7	materiál
na	na	k7c4	na
termoelektrické	termoelektrický	k2eAgInPc4d1	termoelektrický
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
proti	proti	k7c3	proti
mědi	měď	k1gFnSc3	měď
velkou	velký	k2eAgFnSc4d1	velká
termoelektrickou	termoelektrický	k2eAgFnSc4d1	termoelektrická
sílu	síla	k1gFnSc4	síla
40	[number]	k4	40
V	V	kA	V
<g/>
/	/	kIx~	/
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
však	však	k9	však
nelze	lze	k6eNd1	lze
konstantanu	konstantan	k1gInSc2	konstantan
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
přístrojích	přístroj	k1gInPc6	přístroj
na	na	k7c4	na
přesné	přesný	k2eAgNnSc4d1	přesné
měření	měření	k1gNnSc4	měření
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgNnPc2d1	malé
elektrických	elektrický	k2eAgNnPc2d1	elektrické
napětí	napětí	k1gNnPc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
obsahem	obsah	k1gInSc7	obsah
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
nikelin	nikelin	k1gInSc4	nikelin
(	(	kIx(	(
<g/>
např.	např.	kA	např.
typu	typ	k1gInSc2	typ
Cu	Cu	k1gMnSc3	Cu
<g/>
–	–	k?	–
<g/>
Ni	on	k3xPp3gFnSc4	on
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
Mn	Mn	k1gFnPc2	Mn
podle	podle	k7c2	podle
ČSN	ČSN	kA	ČSN
42	[number]	k4	42
3064	[number]	k4	3064
<g/>
,	,	kIx,	,
s	s	k7c7	s
30	[number]	k4	30
%	%	kIx~	%
Ni	on	k3xPp3gFnSc4	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
do	do	k7c2	do
400	[number]	k4	400
°	°	k?	°
<g/>
C.	C.	kA	C.
Jejich	jejich	k3xOp3gFnSc1	jejich
měrný	měrný	k2eAgInSc4d1	měrný
odpor	odpor	k1gInSc4	odpor
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
<g/>
,	,	kIx,	,
asi	asi	k9	asi
0,4	[number]	k4	0,4
Ω	Ω	k?	Ω
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
m.	m.	k?	m.
Levnější	levný	k2eAgInPc1d2	levnější
jsou	být	k5eAaImIp3nP	být
slitiny	slitina	k1gFnPc1	slitina
obsahující	obsahující	k2eAgFnSc1d1	obsahující
i	i	k8xC	i
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
však	však	k9	však
horší	zlý	k2eAgFnSc4d2	horší
časovou	časový	k2eAgFnSc4d1	časová
stálost	stálost	k1gFnSc4	stálost
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
chemickou	chemický	k2eAgFnSc4d1	chemická
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
slitina	slitina	k1gFnSc1	slitina
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
55	[number]	k4	55
%	%	kIx~	%
Cu	Cu	k1gMnSc1	Cu
<g/>
,	,	kIx,	,
30	[number]	k4	30
%	%	kIx~	%
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
15	[number]	k4	15
%	%	kIx~	%
Zn.	zn.	kA	zn.
Tyto	tento	k3xDgFnPc1	tento
slitiny	slitina	k1gFnPc4	slitina
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nP	hodit
k	k	k7c3	k
méně	málo	k6eAd2	málo
náročnému	náročný	k2eAgNnSc3d1	náročné
použití	použití	k1gNnSc3	použití
(	(	kIx(	(
<g/>
spouštěcí	spouštěcí	k2eAgInPc1d1	spouštěcí
odpory	odpor	k1gInPc1	odpor
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
tvrdost	tvrdost	k1gFnSc4	tvrdost
bronzu	bronz	k1gInSc2	bronz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
niklových	niklový	k2eAgInPc2d1	niklový
bronzů	bronz	k1gInPc2	bronz
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
kondenzační	kondenzační	k2eAgFnPc4d1	kondenzační
trubky	trubka	k1gFnPc4	trubka
pro	pro	k7c4	pro
agresivní	agresivní	k2eAgFnPc4d1	agresivní
vody	voda	k1gFnPc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
slitiny	slitina	k1gFnSc2	slitina
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
přísadou	přísada	k1gFnSc7	přísada
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ve	v	k7c6	v
slitině	slitina	k1gFnSc6	slitina
velký	velký	k2eAgInSc4d1	velký
obsah	obsah	k1gInSc4	obsah
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
trubky	trubka	k1gFnPc1	trubka
mají	mít	k5eAaImIp3nP	mít
barvu	barva	k1gFnSc4	barva
světlou	světlý	k2eAgFnSc4d1	světlá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Niklové	Niklové	k2eAgInPc4d1	Niklové
bronzy	bronz	k1gInPc4	bronz
s	s	k7c7	s
přísadou	přísada	k1gFnSc7	přísada
křemíku	křemík	k1gInSc2	křemík
jsou	být	k5eAaImIp3nP	být
vytvrzovatelné	vytvrzovatelný	k2eAgFnPc1d1	vytvrzovatelný
(	(	kIx(	(
<g/>
cuprodur	cuprodur	k1gMnSc1	cuprodur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytvrzený	vytvrzený	k2eAgMnSc1d1	vytvrzený
cuprodur	cuprodur	k1gMnSc1	cuprodur
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
tažnost	tažnost	k1gFnSc4	tažnost
nejen	nejen	k6eAd1	nejen
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
za	za	k7c4	za
teploty	teplota	k1gFnPc4	teplota
vyšší	vysoký	k2eAgFnPc4d2	vyšší
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc2d1	nízká
<g/>
,	,	kIx,	,
až	až	k9	až
–	–	k?	–
<g/>
200	[number]	k4	200
°	°	k?	°
<g/>
C.	C.	kA	C.
Jeho	jeho	k3xOp3gFnSc1	jeho
elektrická	elektrický	k2eAgFnSc1d1	elektrická
vodivost	vodivost	k1gFnSc1	vodivost
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
až	až	k9	až
26	[number]	k4	26
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
Ω	Ω	k?	Ω
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
vyšších	vysoký	k2eAgFnPc2d2	vyšší
hodnot	hodnota	k1gFnPc2	hodnota
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
u	u	k7c2	u
slitiny	slitina	k1gFnSc2	slitina
vytvrzené	vytvrzený	k2eAgFnSc2d1	vytvrzená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cuprodur	Cuprodur	k1gMnSc1	Cuprodur
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
odolný	odolný	k2eAgMnSc1d1	odolný
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
tvárný	tvárný	k2eAgInSc1d1	tvárný
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
šrouby	šroub	k1gInPc4	šroub
a	a	k8xC	a
matice	matice	k1gFnPc4	matice
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc4d1	nízká
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Elektrovodné	Elektrovodný	k2eAgInPc1d1	Elektrovodný
bronzy	bronz	k1gInPc1	bronz
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
sdělovací	sdělovací	k2eAgNnSc4d1	sdělovací
vedení	vedení	k1gNnSc4	vedení
<g/>
,	,	kIx,	,
na	na	k7c4	na
elektrody	elektroda	k1gFnPc4	elektroda
bodových	bodový	k2eAgInPc2d1	bodový
a	a	k8xC	a
švových	švový	k2eAgInPc2d1	švový
svařovacích	svařovací	k2eAgInPc2d1	svařovací
strojů	stroj	k1gInPc2	stroj
apod.	apod.	kA	apod.
Jako	jako	k8xS	jako
telegrafních	telegrafní	k2eAgInPc2d1	telegrafní
čili	čili	k8xC	čili
poštovních	poštovní	k2eAgInPc2d1	poštovní
bronzů	bronz	k1gInPc2	bronz
(	(	kIx(	(
<g/>
42	[number]	k4	42
3019	[number]	k4	3019
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
slitin	slitina	k1gFnPc2	slitina
s	s	k7c7	s
kadmiem	kadmium	k1gNnSc7	kadmium
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
při	při	k7c6	při
značné	značný	k2eAgFnSc6d1	značná
pevnosti	pevnost	k1gFnSc6	pevnost
i	i	k9	i
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vodivost	vodivost	k1gFnSc4	vodivost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
kadmiový	kadmiový	k2eAgInSc4d1	kadmiový
bronz	bronz	k1gInSc4	bronz
s	s	k7c7	s
1	[number]	k4	1
%	%	kIx~	%
Cd	cd	kA	cd
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tvářením	tváření	k1gNnSc7	tváření
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
pevnosti	pevnost	k1gFnSc2	pevnost
asi	asi	k9	asi
70	[number]	k4	70
kp	kp	k?	kp
<g/>
/	/	kIx~	/
<g/>
mm	mm	kA	mm
<g/>
2	[number]	k4	2
při	při	k7c6	při
vodivosti	vodivost	k1gFnSc6	vodivost
asi	asi	k9	asi
45	[number]	k4	45
m	m	kA	m
<g/>
/	/	kIx~	/
Ω	Ω	k?	Ω
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
vodivosti	vodivost	k1gFnSc2	vodivost
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Kadmium	kadmium	k1gNnSc1	kadmium
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
dobrým	dobrý	k2eAgNnSc7d1	dobré
odkysličovadlem	odkysličovadlo	k1gNnSc7	odkysličovadlo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
zejména	zejména	k9	zejména
na	na	k7c4	na
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
opotřebení	opotřebení	k1gNnSc3	opotřebení
při	při	k7c6	při
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
vodivosti	vodivost	k1gFnSc6	vodivost
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
elektrod	elektroda	k1gFnPc2	elektroda
pro	pro	k7c4	pro
svařovací	svařovací	k2eAgInPc4d1	svařovací
stroje	stroj	k1gInPc4	stroj
bodové	bodový	k2eAgInPc4d1	bodový
i	i	k8xC	i
švové	švový	k2eAgInPc4d1	švový
<g/>
.	.	kIx.	.
</s>
<s>
Náležitá	náležitý	k2eAgFnSc1d1	náležitá
pevnost	pevnost	k1gFnSc1	pevnost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
opotřebení	opotřebení	k1gNnSc3	opotřebení
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
i	i	k9	i
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
elektrody	elektroda	k1gFnPc4	elektroda
pro	pro	k7c4	pro
svařování	svařování	k1gNnSc4	svařování
oceli	ocel	k1gFnSc2	ocel
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
bronzů	bronz	k1gInPc2	bronz
Cu	Cu	k1gFnSc1	Cu
<g/>
–	–	k?	–
<g/>
Ag	Ag	k1gFnSc1	Ag
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
Cd	cd	kA	cd
(	(	kIx(	(
<g/>
42	[number]	k4	42
3290	[number]	k4	3290
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vodivost	vodivost	k1gFnSc4	vodivost
asi	asi	k9	asi
48	[number]	k4	48
<g/>
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
Ω	Ω	k?	Ω
<g/>
2	[number]	k4	2
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
opotřebení	opotřebení	k1gNnSc4	opotřebení
si	se	k3xPyFc3	se
udržují	udržovat	k5eAaImIp3nP	udržovat
asi	asi	k9	asi
do	do	k7c2	do
300	[number]	k4	300
°	°	k?	°
<g/>
C.	C.	kA	C.
Jinou	jiný	k2eAgFnSc7d1	jiná
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
slitinou	slitina	k1gFnSc7	slitina
je	být	k5eAaImIp3nS	být
vytvrzovatelný	vytvrzovatelný	k2eAgInSc4d1	vytvrzovatelný
chromový	chromový	k2eAgInSc4d1	chromový
bronz	bronz	k1gInSc4	bronz
asi	asi	k9	asi
s	s	k7c7	s
1	[number]	k4	1
%	%	kIx~	%
Cr	cr	k0	cr
<g/>
,	,	kIx,	,
s	s	k7c7	s
obdobnými	obdobný	k2eAgFnPc7d1	obdobná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Vytvrzuje	vytvrzovat	k5eAaImIp3nS	vytvrzovat
se	se	k3xPyFc4	se
zakalením	zakalení	k1gNnSc7	zakalení
z	z	k7c2	z
teploty	teplota	k1gFnSc2	teplota
asi	asi	k9	asi
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
umělým	umělý	k2eAgNnSc7d1	umělé
stárnutím	stárnutí	k1gNnSc7	stárnutí
při	při	k7c6	při
450	[number]	k4	450
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
2	[number]	k4	2
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Bronzy	bronz	k1gInPc4	bronz
obsahující	obsahující	k2eAgInPc4d1	obsahující
asi	asi	k9	asi
2,5	[number]	k4	2,5
%	%	kIx~	%
kobaltu	kobalt	k1gInSc2	kobalt
a	a	k8xC	a
0,5	[number]	k4	0,5
%	%	kIx~	%
berylia	berylium	k1gNnSc2	berylium
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nS	hodit
pro	pro	k7c4	pro
provozní	provozní	k2eAgFnPc4d1	provozní
teploty	teplota	k1gFnPc4	teplota
do	do	k7c2	do
400	[number]	k4	400
°	°	k?	°
<g/>
C.	C.	kA	C.
Jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vytvrzovatelné	vytvrzovatelný	k2eAgFnPc1d1	vytvrzovatelný
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
asi	asi	k9	asi
poloviční	poloviční	k2eAgFnSc4d1	poloviční
vodivost	vodivost	k1gFnSc4	vodivost
než	než	k8xS	než
měď	měď	k1gFnSc4	měď
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
značně	značně	k6eAd1	značně
drahé	drahý	k2eAgNnSc1d1	drahé
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
osvědčují	osvědčovat	k5eAaImIp3nP	osvědčovat
elektrody	elektroda	k1gFnPc1	elektroda
ze	z	k7c2	z
spékaného	spékaný	k2eAgInSc2d1	spékaný
wolframového	wolframový	k2eAgInSc2d1	wolframový
prášku	prášek	k1gInSc2	prášek
<g/>
,	,	kIx,	,
napuštěného	napuštěný	k2eAgInSc2d1	napuštěný
mědí	měď	k1gFnSc7	měď
(	(	kIx(	(
<g/>
80	[number]	k4	80
%	%	kIx~	%
W	W	kA	W
<g/>
+	+	kIx~	+
+	+	kIx~	+
20	[number]	k4	20
%	%	kIx~	%
Cu	Cu	k1gFnSc2	Cu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
nepatří	patřit	k5eNaImIp3nP	patřit
mezi	mezi	k7c4	mezi
bronzy	bronz	k1gInPc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
také	také	k9	také
spadá	spadat	k5eAaPmIp3nS	spadat
tzv.	tzv.	kA	tzv.
trolejový	trolejový	k2eAgInSc4d1	trolejový
bronz	bronz	k1gInSc4	bronz
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
trolejových	trolejový	k2eAgNnPc2d1	trolejové
vedení	vedení	k1gNnPc2	vedení
užívaných	užívaný	k2eAgNnPc2d1	užívané
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Beryliové	beryliový	k2eAgInPc1d1	beryliový
bronzy	bronz	k1gInPc1	bronz
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgInPc4d1	vysoký
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
vodivosti	vodivost	k1gFnSc6	vodivost
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
slitin	slitina	k1gFnPc2	slitina
podvojných	podvojný	k2eAgInPc2d1	podvojný
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
0,5	[number]	k4	0,5
až	až	k9	až
2,3	[number]	k4	2,3
%	%	kIx~	%
berylia	berylium	k1gNnSc2	berylium
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
také	také	k9	také
nikl	nikl	k1gInSc4	nikl
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
kobalt	kobalt	k1gInSc1	kobalt
<g/>
,	,	kIx,	,
chrom	chrom	k1gInSc1	chrom
aj.	aj.	kA	aj.
Beryliové	beryliový	k2eAgInPc1d1	beryliový
bronzy	bronz	k1gInPc4	bronz
jsou	být	k5eAaImIp3nP	být
vytvrzovatelné	vytvrzovatelný	k2eAgInPc1d1	vytvrzovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
864	[number]	k4	864
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
v	v	k7c6	v
mědi	měď	k1gFnSc6	měď
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
asi	asi	k9	asi
2,7	[number]	k4	2,7
%	%	kIx~	%
Be	Be	k1gFnSc2	Be
<g/>
,	,	kIx,	,
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
však	však	k9	však
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,2	[number]	k4	0,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vytvrzují	vytvrzovat	k5eAaImIp3nP	vytvrzovat
se	se	k3xPyFc4	se
ochlazením	ochlazení	k1gNnSc7	ochlazení
z	z	k7c2	z
800	[number]	k4	800
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
může	moct	k5eAaImIp3nS	moct
následovat	následovat	k5eAaImF	následovat
tváření	tváření	k1gNnSc4	tváření
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
<g/>
)	)	kIx)	)
a	a	k8xC	a
umělým	umělý	k2eAgNnSc7d1	umělé
stárnutím	stárnutí	k1gNnSc7	stárnutí
při	při	k7c6	při
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c6	po
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vytvrzený	vytvrzený	k2eAgInSc1d1	vytvrzený
pérový	pérový	k2eAgInSc1d1	pérový
bronz	bronz	k1gInSc1	bronz
má	mít	k5eAaImIp3nS	mít
pevnost	pevnost	k1gFnSc4	pevnost
asi	asi	k9	asi
140	[number]	k4	140
kp	kp	k?	kp
<g/>
/	/	kIx~	/
<g/>
mm	mm	kA	mm
<g/>
2	[number]	k4	2
při	při	k7c6	při
tažnosti	tažnost	k1gFnSc6	tažnost
asi	asi	k9	asi
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
velké	velký	k2eAgFnSc2d1	velká
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c2	za
vyšších	vysoký	k2eAgFnPc2d2	vyšší
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
beryliové	beryliový	k2eAgInPc4d1	beryliový
bronzy	bronz	k1gInPc4	bronz
značně	značně	k6eAd1	značně
odolné	odolný	k2eAgInPc4d1	odolný
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
a	a	k8xC	a
proti	proti	k7c3	proti
opotřebení	opotřebení	k1gNnSc3	opotřebení
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
mez	mez	k1gFnSc4	mez
únavy	únava	k1gFnSc2	únava
i	i	k9	i
v	v	k7c6	v
korodujícím	korodující	k2eAgNnSc6d1	korodující
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc4d1	vysoká
mez	mez	k1gFnSc4	mez
tečení	tečení	k1gNnSc2	tečení
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nemagnetické	magnetický	k2eNgFnPc1d1	nemagnetická
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
elektricky	elektricky	k6eAd1	elektricky
vodivé	vodivý	k2eAgFnPc4d1	vodivá
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vodivost	vodivost	k1gFnSc4	vodivost
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
slitina	slitina	k1gFnSc1	slitina
se	s	k7c7	s
2	[number]	k4	2
%	%	kIx~	%
Ni	on	k3xPp3gFnSc4	on
a	a	k8xC	a
0,5	[number]	k4	0,5
%	%	kIx~	%
Be	Be	k1gFnSc2	Be
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
pružiny	pružina	k1gFnPc4	pružina
pracující	pracující	k2eAgFnPc4d1	pracující
v	v	k7c6	v
korozním	korozní	k2eAgNnSc6d1	korozní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
na	na	k7c4	na
ventily	ventil	k1gInPc4	ventil
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
na	na	k7c4	na
louhy	louh	k1gInPc4	louh
<g/>
,	,	kIx,	,
na	na	k7c4	na
kuličky	kulička	k1gFnPc4	kulička
korozivzdorných	korozivzdorný	k2eAgNnPc2d1	korozivzdorné
kuličkových	kuličkový	k2eAgNnPc2d1	kuličkové
ložisek	ložisko	k1gNnPc2	ložisko
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2	[number]	k4	2
%	%	kIx~	%
Be	Be	k1gFnSc2	Be
+	+	kIx~	+
Ni	on	k3xPp3gFnSc4	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
nesmějí	smát	k5eNaImIp3nP	smát
jiskřit	jiskřit	k5eAaImF	jiskřit
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2	[number]	k4	2
%	%	kIx~	%
Be	Be	k1gFnSc1	Be
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
namáhané	namáhaný	k2eAgFnPc4d1	namáhaná
elektrody	elektroda	k1gFnPc4	elektroda
bodových	bodový	k2eAgFnPc2d1	bodová
a	a	k8xC	a
švových	švový	k2eAgFnPc2d1	švová
svářeček	svářečka	k1gFnPc2	svářečka
apod.	apod.	kA	apod.
Olověné	olověný	k2eAgInPc1d1	olověný
bronzy	bronz	k1gInPc4	bronz
jsou	být	k5eAaImIp3nP	být
slitiny	slitina	k1gFnPc1	slitina
mědi	měď	k1gFnSc2	měď
s	s	k7c7	s
olovem	olovo	k1gNnSc7	olovo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
bývá	bývat	k5eAaImIp3nS	bývat
nejvýše	vysoce	k6eAd3	vysoce
asi	asi	k9	asi
38	[number]	k4	38
%	%	kIx~	%
<g/>
.	.	kIx.	.
a	a	k8xC	a
popř.	popř.	kA	popř.
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
kovy	kov	k1gInPc7	kov
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
cínem	cín	k1gInSc7	cín
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
Zn	zn	kA	zn
nebo	nebo	k8xC	nebo
Mn	Mn	k1gFnPc1	Mn
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
zlepšit	zlepšit	k5eAaPmF	zlepšit
stejnoměrnost	stejnoměrnost	k1gFnSc4	stejnoměrnost
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Olověné	olověný	k2eAgInPc1d1	olověný
bronzy	bronz	k1gInPc1	bronz
slouží	sloužit	k5eAaImIp3nP	sloužit
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
kovy	kov	k1gInPc1	kov
ložiskové	ložiskový	k2eAgInPc1d1	ložiskový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tuhém	tuhý	k2eAgInSc6d1	tuhý
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
mědi	měď	k1gFnSc6	měď
nepatrná	nepatrný	k2eAgFnSc1d1	nepatrná
a	a	k8xC	a
pro	pro	k7c4	pro
značný	značný	k2eAgInSc4d1	značný
rozdíl	rozdíl	k1gInSc4	rozdíl
měrných	měrný	k2eAgFnPc2d1	měrná
hmotností	hmotnost	k1gFnPc2	hmotnost
obou	dva	k4xCgInPc2	dva
kovů	kov	k1gInPc2	kov
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
odměšování	odměšování	k1gNnSc4	odměšování
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ložiskových	ložiskový	k2eAgInPc2d1	ložiskový
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
pevné	pevný	k2eAgFnSc6d1	pevná
základní	základní	k2eAgFnSc6d1	základní
měděné	měděný	k2eAgFnSc6d1	měděná
hmotě	hmota	k1gFnSc6	hmota
bylo	být	k5eAaImAgNnS	být
olovo	olovo	k1gNnSc1	olovo
jemně	jemně	k6eAd1	jemně
rozptýleno	rozptýlit	k5eAaPmNgNnS	rozptýlit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
olověných	olověný	k2eAgInPc2d1	olověný
bronzů	bronz	k1gInPc2	bronz
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
odlévají	odlévat	k5eAaImIp3nP	odlévat
celé	celý	k2eAgFnPc1d1	celá
ložiskové	ložiskový	k2eAgFnPc1d1	ložisková
pánve	pánev	k1gFnPc1	pánev
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
vylévají	vylévat	k5eAaImIp3nP	vylévat
pánve	pánev	k1gFnPc1	pánev
ocelové	ocelový	k2eAgFnPc1d1	ocelová
<g/>
.	.	kIx.	.
</s>
<s>
Olověné	olověný	k2eAgInPc4d1	olověný
bronzy	bronz	k1gInPc4	bronz
s	s	k7c7	s
přísadou	přísada	k1gFnSc7	přísada
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
Pb	Pb	k1gFnSc1	Pb
se	se	k3xPyFc4	se
hodí	hodit	k5eAaPmIp3nS	hodit
jak	jak	k6eAd1	jak
na	na	k7c4	na
vylévání	vylévání	k1gNnSc4	vylévání
opěrných	opěrný	k2eAgFnPc2d1	opěrná
ocelových	ocelový	k2eAgFnPc2d1	ocelová
pánví	pánev	k1gFnPc2	pánev
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c4	na
odlitky	odlitek	k1gInPc4	odlitek
celých	celý	k2eAgFnPc2d1	celá
pánví	pánev	k1gFnPc2	pánev
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
olověných	olověný	k2eAgInPc2d1	olověný
bronzů	bronz	k1gInPc2	bronz
bez	bez	k7c2	bez
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
s	s	k7c7	s
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
Pb	Pb	k1gFnSc2	Pb
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
jen	jen	k9	jen
na	na	k7c4	na
vylévání	vylévání	k1gNnSc4	vylévání
opěrných	opěrný	k2eAgFnPc2d1	opěrná
ocelových	ocelový	k2eAgFnPc2d1	ocelová
pánví	pánev	k1gFnPc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Vylévané	vylévaný	k2eAgFnPc1d1	vylévaná
pánve	pánev	k1gFnPc1	pánev
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
tenkostěnné	tenkostěnný	k2eAgFnPc1d1	tenkostěnná
<g/>
,	,	kIx,	,
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
tenkou	tenký	k2eAgFnSc7d1	tenká
výstelkou	výstelka	k1gFnSc7	výstelka
z	z	k7c2	z
olověného	olověný	k2eAgInSc2d1	olověný
bronzu	bronz	k1gInSc2	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
lepší	dobrý	k2eAgNnSc4d2	lepší
než	než	k8xS	než
plnostěnné	plnostěnný	k2eAgNnSc4d1	plnostěnný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
spojují	spojovat	k5eAaImIp3nP	spojovat
velkou	velký	k2eAgFnSc4d1	velká
pevnost	pevnost	k1gFnSc4	pevnost
ocelové	ocelový	k2eAgFnSc2d1	ocelová
opěrné	opěrný	k2eAgFnSc2d1	opěrná
pánve	pánev	k1gFnSc2	pánev
s	s	k7c7	s
dobrými	dobrý	k2eAgFnPc7d1	dobrá
kluznými	kluzný	k2eAgFnPc7d1	kluzná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
olověného	olověný	k2eAgInSc2d1	olověný
bronzu	bronz	k1gInSc2	bronz
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
uspoří	uspořit	k5eAaPmIp3nP	uspořit
neželezné	železný	k2eNgInPc1d1	neželezný
kovy	kov	k1gInPc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Pánve	pánev	k1gFnPc1	pánev
vylité	vylitý	k2eAgMnPc4d1	vylitý
olověným	olověný	k2eAgInSc7d1	olověný
bronzem	bronz	k1gInSc7	bronz
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
namáhaná	namáhaný	k2eAgNnPc4d1	namáhané
ložiska	ložisko	k1gNnPc4	ložisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
měrný	měrný	k2eAgInSc4d1	měrný
tlak	tlak	k1gInSc4	tlak
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
100	[number]	k4	100
kp	kp	k?	kp
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc1d3	Nejvyšší
přípustné	přípustný	k2eAgInPc1d1	přípustný
měrné	měrný	k2eAgInPc1d1	měrný
tlaky	tlak	k1gInPc1	tlak
jsou	být	k5eAaImIp3nP	být
300	[number]	k4	300
až	až	k9	až
400	[number]	k4	400
kp	kp	k?	kp
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc3d3	veliký
obvodové	obvodový	k2eAgFnSc3d1	obvodová
rychlosti	rychlost	k1gFnSc3	rychlost
do	do	k7c2	do
10	[number]	k4	10
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Červené	Červené	k2eAgInPc4d1	Červené
bronzy	bronz	k1gInPc4	bronz
jsou	být	k5eAaImIp3nP	být
slitiny	slitina	k1gFnPc1	slitina
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
často	často	k6eAd1	často
též	též	k6eAd1	též
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
obrobitelnost	obrobitelnost	k1gFnSc1	obrobitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
odlitků	odlitek	k1gInPc2	odlitek
používaných	používaný	k2eAgInPc2d1	používaný
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
šedá	šedý	k2eAgFnSc1d1	šedá
litina	litina	k1gFnSc1	litina
pro	pro	k7c4	pro
malou	malý	k2eAgFnSc4d1	malá
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
apod.	apod.	kA	apod.
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
druhem	druh	k1gInSc7	druh
bronzu	bronz	k1gInSc2	bronz
je	být	k5eAaImIp3nS	být
dělovina	dělovina	k1gFnSc1	dělovina
<g/>
,	,	kIx,	,
slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
houževnatých	houževnatý	k2eAgInPc2d1	houževnatý
odlitků	odlitek	k1gInPc2	odlitek
dělových	dělový	k2eAgInPc2d1	dělový
hlavní	hlavní	k2eAgFnSc7d1	hlavní
historických	historický	k2eAgFnPc2d1	historická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
slitinou	slitina	k1gFnSc7	slitina
je	být	k5eAaImIp3nS	být
také	také	k9	také
zvonovina	zvonovina	k1gFnSc1	zvonovina
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kostelních	kostelní	k2eAgInPc2d1	kostelní
zvonů	zvon	k1gInPc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Dělovina	dělovina	k1gFnSc1	dělovina
Zvonovina	zvonovina	k1gFnSc1	zvonovina
Ložisko	ložisko	k1gNnSc1	ložisko
Měď	měď	k1gFnSc1	měď
Doba	doba	k1gFnSc1	doba
bronzová	bronzový	k2eAgFnSc1d1	bronzová
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bronz	bronz	k1gInSc1	bronz
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
bronz	bronz	k1gInSc4	bronz
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
