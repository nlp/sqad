<p>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
Německý	německý	k2eAgInSc1d1	německý
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Deutschbrod	Deutschbrod	k1gInSc1	Deutschbrod
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
okrese	okres	k1gInSc6	okres
<g/>
,	,	kIx,	,
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
23	[number]	k4	23
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Jihlavy	Jihlava	k1gFnSc2	Jihlava
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
významný	významný	k2eAgInSc4d1	významný
dopravní	dopravní	k2eAgInSc4d1	dopravní
uzel	uzel	k1gInSc4	uzel
ležící	ležící	k2eAgInSc4d1	ležící
zhruba	zhruba	k6eAd1	zhruba
uprostřed	uprostřed	k7c2	uprostřed
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
23	[number]	k4	23
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
zde	zde	k6eAd1	zde
stála	stát	k5eAaImAgFnS	stát
hornická	hornický	k2eAgFnSc1d1	hornická
osada	osada	k1gFnSc1	osada
u	u	k7c2	u
brodu	brod	k1gInSc2	brod
přes	přes	k7c4	přes
Sázavu	Sázava	k1gFnSc4	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
Smilův	Smilův	k2eAgInSc1d1	Smilův
Brod	Brod	k1gInSc1	Brod
podle	podle	k7c2	podle
zakladatele	zakladatel	k1gMnSc2	zakladatel
Smila	Smil	k1gMnSc2	Smil
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
existují	existovat	k5eAaImIp3nP	existovat
nejstarší	starý	k2eAgFnPc1d3	nejstarší
zmínky	zmínka	k1gFnPc1	zmínka
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1234	[number]	k4	1234
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vlastním	vlastní	k2eAgNnSc6d1	vlastní
městě	město	k1gNnSc6	město
pochází	pocházet	k5eAaImIp3nS	pocházet
ale	ale	k9	ale
z	z	k7c2	z
listiny	listina	k1gFnSc2	listina
datované	datovaný	k2eAgFnSc2d1	datovaná
k	k	k7c3	k
26	[number]	k4	26
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
1256	[number]	k4	1256
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nS	řešit
spor	spor	k1gInSc1	spor
brodského	brodský	k2eAgMnSc2d1	brodský
plebána	plebán	k1gMnSc2	plebán
s	s	k7c7	s
vilémovským	vilémovský	k2eAgInSc7d1	vilémovský
klášterem	klášter	k1gInSc7	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
dělal	dělat	k5eAaImAgMnS	dělat
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
území	území	k1gNnSc4	území
kolem	kolem	k7c2	kolem
Brodu	Brod	k1gInSc2	Brod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
královskou	královský	k2eAgFnSc4d1	královská
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tady	tady	k6eAd1	tady
dolovala	dolovat	k5eAaImAgFnS	dolovat
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
ruda	ruda	k1gFnSc1	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
vážných	vážný	k2eAgInPc2d1	vážný
sporů	spor	k1gInPc2	spor
se	s	k7c7	s
Smilem	smil	k1gInSc7	smil
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yIgFnPc6	který
ale	ale	k9	ale
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
pozemků	pozemek	k1gInPc2	pozemek
připadla	připadnout	k5eAaPmAgFnS	připadnout
králi	král	k1gMnSc6	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1274	[number]	k4	1274
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
povýšena	povýšit	k5eAaPmNgFnS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc1	město
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1308	[number]	k4	1308
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
Německý	německý	k2eAgInSc4d1	německý
Brod	Brod	k1gInSc4	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1422	[number]	k4	1422
hledal	hledat	k5eAaImAgMnS	hledat
útočiště	útočiště	k1gNnSc4	útočiště
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Husitské	husitský	k2eAgInPc1d1	husitský
oddíly	oddíl	k1gInPc1	oddíl
vedené	vedený	k2eAgFnSc2d1	vedená
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
Brod	Brod	k1gInSc4	Brod
dobyly	dobýt	k5eAaPmAgFnP	dobýt
a	a	k8xC	a
císařské	císařský	k2eAgNnSc4d1	císařské
vojsko	vojsko	k1gNnSc4	vojsko
porazily	porazit	k5eAaPmAgFnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
vítězství	vítězství	k1gNnSc2	vítězství
Žižky	Žižka	k1gMnPc4	Žižka
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
současně	současně	k6eAd1	současně
definitivně	definitivně	k6eAd1	definitivně
ukončila	ukončit	k5eAaPmAgFnS	ukončit
těžbu	těžba	k1gFnSc4	těžba
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1637	[number]	k4	1637
byl	být	k5eAaImAgInS	být
Brod	Brod	k1gInSc1	Brod
povýšen	povýšit	k5eAaPmNgInS	povýšit
na	na	k7c4	na
královské	královský	k2eAgNnSc4d1	královské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
1874	[number]	k4	1874
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
zdejším	zdejší	k2eAgNnSc6d1	zdejší
gymnáziu	gymnázium	k1gNnSc6	gymnázium
Vilém	Vilém	k1gMnSc1	Vilém
Kurz	Kurz	k1gMnSc1	Kurz
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
iniciátorů	iniciátor	k1gMnPc2	iniciátor
stavby	stavba	k1gFnSc2	stavba
Petřínské	petřínský	k2eAgFnSc2d1	Petřínská
rozhledny	rozhledna	k1gFnSc2	rozhledna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vysídlení	vysídlení	k1gNnSc2	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Havlíčkův	Havlíčkův	k2eAgInSc4d1	Havlíčkův
Brod	Brod	k1gInSc4	Brod
podle	podle	k7c2	podle
Karla	Karel	k1gMnSc2	Karel
Havlíčka	Havlíček	k1gMnSc4	Havlíček
Borovského	Borovský	k1gMnSc4	Borovský
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pivo	pivo	k1gNnSc4	pivo
Rebel	rebel	k1gMnSc1	rebel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
populace	populace	k1gFnSc2	populace
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
847	[number]	k4	847
domech	dům	k1gInPc6	dům
8	[number]	k4	8
986	[number]	k4	986
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
4	[number]	k4	4
798	[number]	k4	798
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
8	[number]	k4	8
853	[number]	k4	853
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
51	[number]	k4	51
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
2	[number]	k4	2
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
5	[number]	k4	5
087	[number]	k4	087
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
166	[number]	k4	166
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
2	[number]	k4	2
953	[number]	k4	953
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
160	[number]	k4	160
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
1223	[number]	k4	1223
domech	dům	k1gInPc6	dům
10	[number]	k4	10
760	[number]	k4	760
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
10	[number]	k4	10
506	[number]	k4	506
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
142	[number]	k4	142
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
6	[number]	k4	6
668	[number]	k4	668
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
267	[number]	k4	267
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
2	[number]	k4	2
803	[number]	k4	803
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
146	[number]	k4	146
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
13	[number]	k4	13
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
a	a	k8xC	a
ze	z	k7c2	z
14	[number]	k4	14
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Březinka	březinka	k1gFnSc1	březinka
u	u	k7c2	u
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
–	–	k?	–
část	část	k1gFnSc1	část
Březinka	březinka	k1gFnSc1	březinka
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
–	–	k?	–
části	část	k1gFnSc3	část
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Jilemník	Jilemník	k1gMnSc1	Jilemník
–	–	k?	–
část	část	k1gFnSc1	část
Jilemník	Jilemník	k1gMnSc1	Jilemník
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Klanečná	Klanečný	k2eAgFnSc1d1	Klanečná
–	–	k?	–
část	část	k1gFnSc1	část
Klanečná	Klanečný	k2eAgFnSc1d1	Klanečná
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Květnov	Květnov	k1gInSc1	Květnov
–	–	k?	–
část	část	k1gFnSc1	část
Květnov	Květnovo	k1gNnPc2	Květnovo
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Mírovka	Mírovka	k1gFnSc1	Mírovka
–	–	k?	–
část	část	k1gFnSc1	část
Mírovka	Mírovka	k1gFnSc1	Mírovka
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Perknov	Perknov	k1gInSc1	Perknov
–	–	k?	–
část	část	k1gFnSc4	část
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Poděbaby	Poděbaba	k1gFnSc2	Poděbaba
–	–	k?	–
části	část	k1gFnSc3	část
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poděbaby	Poděbab	k1gInPc1	Poděbab
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Suchá	Suchá	k1gFnSc1	Suchá
u	u	k7c2	u
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
–	–	k?	–
části	část	k1gFnSc2	část
Suchá	Suchá	k1gFnSc1	Suchá
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Kříž	Kříž	k1gMnSc1	Kříž
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Šmolovy	Šmolův	k2eAgFnPc1d1	Šmolův
u	u	k7c2	u
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
–	–	k?	–
část	část	k1gFnSc1	část
Šmolovy	Šmolův	k2eAgFnPc1d1	Šmolův
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Termesivy	Termesiva	k1gFnSc2	Termesiva
–	–	k?	–
části	část	k1gFnSc2	část
Herlify	Herlif	k1gInPc4	Herlif
<g/>
,	,	kIx,	,
Termesivy	Termesiv	k1gInPc4	Termesiv
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Veselice	veselice	k1gFnSc2	veselice
u	u	k7c2	u
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
–	–	k?	–
část	část	k1gFnSc1	část
Veselice	veselice	k1gFnSc1	veselice
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Zbožice	Zbožice	k1gFnSc2	Zbožice
–	–	k?	–
část	část	k1gFnSc1	část
ZbožiceDříve	ZbožiceDříev	k1gFnSc2	ZbožiceDříev
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
také	také	k9	také
dnes	dnes	k6eAd1	dnes
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
Bartoušov	Bartoušov	k1gInSc1	Bartoušov
<g/>
,	,	kIx,	,
Břevnice	Břevnice	k1gFnSc1	Břevnice
<g/>
,	,	kIx,	,
Hurtova	Hurtův	k2eAgFnSc1d1	Hurtova
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
,	,	kIx,	,
Knyk	Knyk	k1gInSc1	Knyk
<g/>
,	,	kIx,	,
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
,	,	kIx,	,
Michalovice	Michalovice	k1gFnSc1	Michalovice
<g/>
,	,	kIx,	,
Vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
Ždírec	Ždírec	k1gInSc1	Ždírec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnPc1d1	správní
území	území	k1gNnPc1	území
==	==	k?	==
</s>
</p>
<p>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Havlíčkův	Havlíčkův	k2eAgInSc4d1	Havlíčkův
Brod	Brod	k1gInSc4	Brod
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
120	[number]	k4	120
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
z	z	k7c2	z
56	[number]	k4	56
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Samospráva	samospráva	k1gFnSc1	samospráva
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
starosta	starosta	k1gMnSc1	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
má	mít	k5eAaImIp3nS	mít
25	[number]	k4	25
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
dohody	dohoda	k1gFnSc2	dohoda
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
sedm	sedm	k4xCc4	sedm
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
starostou	starosta	k1gMnSc7	starosta
města	město	k1gNnSc2	město
Jan	Jan	k1gMnSc1	Jan
Tecl	Tecl	k1gMnSc1	Tecl
z	z	k7c2	z
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Radu	rada	k1gFnSc4	rada
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zástupci	zástupce	k1gMnPc1	zástupce
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnPc1	hnutí
Broďáci	Broďák	k1gMnPc1	Broďák
a	a	k8xC	a
Společně	společně	k6eAd1	společně
pro	pro	k7c4	pro
Brod	Brod	k1gInSc4	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
starostou	starosta	k1gMnSc7	starosta
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Tecl	Tecl	k1gMnSc1	Tecl
zvolen	zvolit	k5eAaPmNgMnS	zvolit
po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmičlenné	sedmičlenný	k2eAgFnSc6d1	sedmičlenná
městské	městský	k2eAgFnSc6d1	městská
radě	rada	k1gFnSc6	rada
má	mít	k5eAaImIp3nS	mít
ODS	ODS	kA	ODS
nově	nově	k6eAd1	nově
tři	tři	k4xCgMnPc4	tři
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
,	,	kIx,	,
kandidátka	kandidátka	k1gFnSc1	kandidátka
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
a	a	k8xC	a
Žen	žena	k1gFnPc2	žena
za	za	k7c4	za
Brod	Brod	k1gInSc4	Brod
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Starostové	Starostové	k2eAgInSc2d1	Starostové
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Holenda	Holend	k1gMnSc4	Holend
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Václav	Václav	k1gMnSc1	Václav
Šrámek	Šrámek	k1gMnSc1	Šrámek
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kruntorád	Kruntoráda	k1gFnPc2	Kruntoráda
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
SNK	SNK	kA	SNK
–	–	k?	–
Broďáci	Broďák	k1gMnPc1	Broďák
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Jana	Jana	k1gFnSc1	Jana
Fischerová	Fischerová	k1gFnSc1	Fischerová
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mgr.	Mgr.	kA	Mgr.
Jan	Jan	k1gMnSc1	Jan
Tecl	Tecl	k1gMnSc1	Tecl
<g/>
,	,	kIx,	,
MBA	MBA	kA	MBA
od	od	k7c2	od
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Školství	školství	k1gNnSc2	školství
==	==	k?	==
</s>
</p>
<p>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Korálky	korálek	k1gInPc4	korálek
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Wolkerova	Wolkerův	k2eAgFnSc1d1	Wolkerova
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Štáflova	Štáflův	k2eAgFnSc1d1	Štáflova
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Konečná	Konečná	k1gFnSc1	Konečná
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
V	v	k7c6	v
Sadech	sad	k1gInPc6	sad
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Nuselská	nuselský	k2eAgFnSc1d1	Nuselská
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Praktická	praktický	k2eAgFnSc1d1	praktická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
U	u	k7c2	u
Trojice	trojice	k1gFnSc2	trojice
</s>
</p>
<p>
<s>
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
a	a	k8xC	a
Hotelová	hotelový	k2eAgFnSc1d1	hotelová
škola	škola	k1gFnSc1	škola
Havlíčkův	Havlíčkův	k2eAgInSc4d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
stavební	stavební	k2eAgFnSc1d1	stavební
akademika	akademik	k1gMnSc4	akademik
Stanislava	Stanislav	k1gMnSc2	Stanislav
Bechyně	Bechyně	k1gMnSc2	Bechyně
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
</s>
</p>
<p>
<s>
Pedagogicko	Pedagogicko	k1gNnSc1	Pedagogicko
-	-	kIx~	-
psychologická	psychologický	k2eAgFnSc1d1	psychologická
poradna	poradna	k1gFnSc1	poradna
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
J.	J.	kA	J.
V.	V.	kA	V.
Stamice	Stamika	k1gFnSc3	Stamika
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
</s>
</p>
<p>
<s>
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
==	==	k?	==
Muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc2	galerie
==	==	k?	==
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc4	muzeum
Vysočiny	vysočina	k1gFnSc2	vysočina
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
Havlíčkovo	Havlíčkův	k2eAgNnSc1d1	Havlíčkovo
náměstí	náměstí	k1gNnSc1	náměstí
19	[number]	k4	19
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Havlíčkově	Havlíčkův	k2eAgInSc6d1	Havlíčkův
Brodě	Brod	k1gInSc6	Brod
(	(	kIx(	(
<g/>
Havlíčkovo	Havlíčkův	k2eAgNnSc1d1	Havlíčkovo
náměstí	náměstí	k1gNnSc1	náměstí
18	[number]	k4	18
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
BK	BK	kA	BK
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
–	–	k?	–
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
hrající	hrající	k2eAgInSc1d1	hrající
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
česká	český	k2eAgFnSc1d1	Česká
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FC	FC	kA	FC
Pramen	pramen	k1gInSc4	pramen
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
futsalový	futsalový	k2eAgInSc1d1	futsalový
klub	klub	k1gInSc1	klub
hrající	hrající	k2eAgInSc1d1	hrající
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
ligu	liga	k1gFnSc4	liga
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
orientačního	orientační	k2eAgInSc2d1	orientační
běhu	běh	k1gInSc2	běh
</s>
</p>
<p>
<s>
Hroši	hroch	k1gMnPc1	hroch
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
softballový	softballový	k2eAgInSc1d1	softballový
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
všechna	všechen	k3xTgNnPc4	všechen
mužstva	mužstvo	k1gNnPc4	mužstvo
hrají	hrát	k5eAaImIp3nP	hrát
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SRC	SRC	kA	SRC
Fanatic	Fanatice	k1gFnPc2	Fanatice
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
aerobik	aerobik	k1gInSc1	aerobik
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
týmy	tým	k1gInPc1	tým
několikrát	několikrát	k6eAd1	několikrát
získaly	získat	k5eAaPmAgInP	získat
titul	titul	k1gInSc4	titul
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TJ	tj	kA	tj
Sokol	Sokol	k1gMnSc1	Sokol
H.	H.	kA	H.
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
florbalový	florbalový	k2eAgInSc1d1	florbalový
klub	klub	k1gInSc1	klub
hrající	hrající	k2eAgMnSc1d1	hrající
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
Divizi	divize	k1gFnSc4	divize
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
česká	český	k2eAgFnSc1d1	Česká
soutěž	soutěž	k1gFnSc1	soutěž
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
-	-	kIx~	-
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TJ	tj	kA	tj
Jiskra	jiskra	k1gFnSc1	jiskra
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
–	–	k?	–
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
tým	tým	k1gInSc1	tým
</s>
</p>
<p>
<s>
TJ	tj	kA	tj
Jiskra	jiskra	k1gFnSc1	jiskra
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
zápas	zápas	k1gInSc1	zápas
(	(	kIx(	(
<g/>
několikanásobný	několikanásobný	k2eAgMnSc1d1	několikanásobný
mistr	mistr	k1gMnSc1	mistr
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
má	mít	k5eAaImIp3nS	mít
celostátní	celostátní	k2eAgInSc1d1	celostátní
význam	význam	k1gInSc1	význam
jako	jako	k8xC	jako
silniční	silniční	k2eAgFnSc1d1	silniční
i	i	k8xC	i
železniční	železniční	k2eAgFnSc1d1	železniční
křižovatka	křižovatka	k1gFnSc1	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Protínají	protínat	k5eAaImIp3nP	protínat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
dálkové	dálkový	k2eAgFnPc1d1	dálková
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
34	[number]	k4	34
a	a	k8xC	a
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
zde	zde	k6eAd1	zde
vyúsťuje	vyúsťovat	k5eAaImIp3nS	vyúsťovat
ještě	ještě	k9	ještě
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
a	a	k8xC	a
několik	několik	k4yIc1	několik
silnic	silnice	k1gFnPc2	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
havlíčkobrodském	havlíčkobrodský	k2eAgInSc6d1	havlíčkobrodský
železničním	železniční	k2eAgInSc6d1	železniční
uzlu	uzel	k1gInSc6	uzel
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
setkávají	setkávat	k5eAaImIp3nP	setkávat
celostátní	celostátní	k2eAgFnPc4d1	celostátní
tratě	trať	k1gFnPc4	trať
č.	č.	k?	č.
225	[number]	k4	225
od	od	k7c2	od
Jihlavy	Jihlava	k1gFnSc2	Jihlava
a	a	k8xC	a
Veselí	veselí	k1gNnSc2	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
230	[number]	k4	230
od	od	k7c2	od
Kolína	Kolín	k1gInSc2	Kolín
a	a	k8xC	a
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
238	[number]	k4	238
od	od	k7c2	od
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
,	,	kIx,	,
250	[number]	k4	250
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
lokální	lokální	k2eAgFnSc4d1	lokální
Trať	trať	k1gFnSc4	trať
237	[number]	k4	237
do	do	k7c2	do
Humpolce	Humpolec	k1gInSc2	Humpolec
<g/>
.	.	kIx.	.
</s>
<s>
Rychlíky	rychlík	k1gInPc1	rychlík
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Brnem	Brno	k1gNnSc7	Brno
tudy	tudy	k6eAd1	tudy
jezdí	jezdit	k5eAaImIp3nS	jezdit
ve	v	k7c6	v
dvouhodinovém	dvouhodinový	k2eAgInSc6d1	dvouhodinový
taktu	takt	k1gInSc6	takt
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
uzlem	uzel	k1gInSc7	uzel
lokální	lokální	k2eAgFnSc2d1	lokální
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
zde	zde	k6eAd1	zde
i	i	k9	i
městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Církevní	církevní	k2eAgFnPc1d1	církevní
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Svaté	svatý	k2eAgFnSc2d1	svatá
Rodiny	rodina	k1gFnSc2	rodina
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
</s>
</p>
<p>
<s>
tři	tři	k4xCgInPc4	tři
kříže	kříž	k1gInPc4	kříž
na	na	k7c4	na
Kalvárii	Kalvárie	k1gFnSc4	Kalvárie
a	a	k8xC	a
jedno	jeden	k4xCgNnSc4	jeden
zastavení	zastavení	k1gNnPc2	zastavení
Křížové	Křížové	k2eAgFnSc2d1	Křížové
cesty	cesta	k1gFnSc2	cesta
</s>
</p>
<p>
<s>
===	===	k?	===
Světské	světský	k2eAgFnPc1d1	světská
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
dům	dům	k1gInSc1	dům
</s>
</p>
<p>
<s>
Kašna	kašna	k1gFnSc1	kašna
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Tritona	triton	k1gMnSc2	triton
</s>
</p>
<p>
<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
</s>
</p>
<p>
<s>
Štáflova	Štáflův	k2eAgFnSc1d1	Štáflova
bašta	bašta	k1gFnSc1	bašta
</s>
</p>
<p>
<s>
Štáflova	Štáflův	k2eAgFnSc1d1	Štáflova
chalupa	chalupa	k1gFnSc1	chalupa
</s>
</p>
<p>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Ambrož	Ambrož	k1gMnSc1	Ambrož
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lední	lední	k2eAgMnSc1d1	lední
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Josef	Josef	k1gMnSc1	Josef
Barvitius	Barvitius	k1gMnSc1	Barvitius
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
,	,	kIx,	,
první	první	k4xOgMnSc1	první
předseda	předseda	k1gMnSc1	předseda
OSA	osa	k1gFnSc1	osa
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
František	František	k1gMnSc1	František
Beckovský	Beckovský	k2eAgMnSc1d1	Beckovský
(	(	kIx(	(
<g/>
1658	[number]	k4	1658
<g/>
–	–	k?	–
<g/>
1725	[number]	k4	1725
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
náboženské	náboženský	k2eAgFnSc2d1	náboženská
literatury	literatura	k1gFnSc2	literatura
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Benák	Benák	k1gMnSc1	Benák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Bezouška	Bezouška	k1gMnSc1	Bezouška
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Blažej	Blažej	k1gMnSc1	Blažej
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
typograf	typograf	k1gMnSc1	typograf
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Brzorád	Brzoráda	k1gFnPc2	Brzoráda
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
de	de	k?	de
Causis	Causis	k1gInSc1	Causis
(	(	kIx(	(
<g/>
1380	[number]	k4	1380
<g/>
–	–	k?	–
<g/>
1432	[number]	k4	1432
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
,	,	kIx,	,
prokurátor	prokurátor	k1gMnSc1	prokurátor
papežské	papežský	k2eAgFnSc2d1	Papežská
kurie	kurie	k1gFnSc2	kurie
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
žaloby	žaloba	k1gFnSc2	žaloba
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
Husa	Hus	k1gMnSc4	Hus
</s>
</p>
<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Čermák	Čermák	k1gMnSc1	Čermák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Bohumír	Bohumír	k1gMnSc1	Bohumír
Dufek	Dufek	k1gMnSc1	Dufek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborový	odborový	k2eAgMnSc1d1	odborový
předák	předák	k1gMnSc1	předák
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Exnar	Exnar	k1gMnSc1	Exnar
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sklář	sklář	k1gMnSc1	sklář
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Felix	Felix	k1gMnSc1	Felix
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Fischerová	Fischerová	k1gFnSc1	Fischerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
politička	politička	k1gFnSc1	politička
,	,	kIx,	,
starostka	starostka	k1gFnSc1	starostka
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
-	-	kIx~	-
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
<g/>
)	)	kIx)	)
,	,	kIx,	,
poslankyně	poslankyně	k1gFnSc2	poslankyně
PS	PS	kA	PS
PČR	PČR	kA	PČR
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
-	-	kIx~	-
2017	[number]	k4	2017
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Fišera	Fišer	k1gMnSc2	Fišer
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Formánek	Formánek	k1gMnSc1	Formánek
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Hamza	Hamz	k1gMnSc2	Hamz
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Holík	Holík	k1gMnSc1	Holík
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Holík	Holík	k1gMnSc1	Holík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Hrabalik	Hrabalik	k1gMnSc1	Hrabalik
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Humpál	Humpál	k1gMnSc1	Humpál
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Jahoda	Jahoda	k1gMnSc1	Jahoda
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Jajtner	Jajtner	k1gMnSc1	Jajtner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
,	,	kIx,	,
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
u	u	k7c2	u
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Jakeš	Jakeš	k1gMnSc1	Jakeš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
</s>
</p>
<p>
<s>
Eugen	Eugen	k2eAgInSc1d1	Eugen
Kadeřávek	kadeřávek	k1gInSc1	kadeřávek
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Kalina	Kalina	k1gMnSc1	Kalina
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Klán	klán	k2eAgMnSc1d1	klán
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pilot	pilot	k1gMnSc1	pilot
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Klofáč	klofáč	k1gInSc1	klofáč
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
,	,	kIx,	,
první	první	k4xOgMnSc1	první
československý	československý	k2eAgMnSc1d1	československý
ministr	ministr	k1gMnSc1	ministr
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
</s>
</p>
<p>
<s>
Rut	rout	k5eAaImNgInS	rout
Kolínská	kolínský	k2eAgNnPc1d1	kolínské
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aktivistka	aktivistka	k1gFnSc1	aktivistka
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Koubský	Koubský	k2eAgMnSc1d1	Koubský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
informatik	informatik	k1gMnSc1	informatik
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
analytik	analytik	k1gMnSc1	analytik
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Králíček	Králíček	k1gMnSc1	Králíček
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
secesní	secesní	k2eAgMnSc1d1	secesní
a	a	k8xC	a
kubistický	kubistický	k2eAgMnSc1d1	kubistický
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Kučera	Kučera	k1gMnSc1	Kučera
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
a	a	k8xC	a
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kučera	Kučera	k1gMnSc1	Kučera
(	(	kIx(	(
<g/>
*	*	kIx~	*
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Marcel	Marcel	k1gMnSc1	Marcel
Kříž	Kříž	k1gMnSc1	Kříž
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
písničkář	písničkář	k1gMnSc1	písničkář
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Kurz	Kurz	k1gMnSc1	Kurz
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kut	kut	k2eAgMnSc1d1	kut
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kuttelwascher	Kuttelwaschra	k1gFnPc2	Kuttelwaschra
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
,	,	kIx,	,
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
československý	československý	k2eAgMnSc1d1	československý
letec	letec	k1gMnSc1	letec
v	v	k7c6	v
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Landovský	Landovský	k2eAgMnSc1d1	Landovský
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
disident	disident	k1gMnSc1	disident
<g/>
,	,	kIx,	,
signatář	signatář	k1gMnSc1	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Liška	Liška	k1gMnSc1	Liška
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvěrolékař	zvěrolékař	k1gMnSc1	zvěrolékař
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Machač	Machač	k1gMnSc1	Machač
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odbojář	odbojář	k1gMnSc1	odbojář
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Jarmila	Jarmila	k1gFnSc1	Jarmila
Machačová	Machačová	k1gFnSc1	Machačová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklistka	cyklistka	k1gFnSc1	cyklistka
</s>
</p>
<p>
<s>
Božena	Božena	k1gFnSc1	Božena
Machačová-Dostálová	Machačová-Dostálový	k2eAgFnSc1d1	Machačová-Dostálová
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Marha	Marha	k1gMnSc1	Marha
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Radek	Radek	k1gMnSc1	Radek
Martínek	Martínek	k1gMnSc1	Martínek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Matoušek	Matoušek	k1gMnSc1	Matoušek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
violista	violista	k1gMnSc1	violista
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
</s>
</p>
<p>
<s>
Stella	Stella	k1gFnSc1	Stella
Májová	májová	k1gFnSc1	májová
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
operetní	operetní	k2eAgFnSc1d1	operetní
a	a	k8xC	a
muzikálová	muzikálový	k2eAgFnSc1d1	muzikálová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Med	med	k1gInSc1	med
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Mertlík	Mertlík	k1gInSc1	Mertlík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
-	-	kIx~	-
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Mikula	Mikula	k1gMnSc1	Mikula
(	(	kIx(	(
<g/>
*	*	kIx~	*
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Riedl	Riedl	k1gMnSc1	Riedl
Německobrodský	německobrodský	k2eAgMnSc1d1	německobrodský
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Noll	Noll	k1gMnSc1	Noll
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Radek	Radek	k1gMnSc1	Radek
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Orgoníková	Orgoníková	k1gFnSc1	Orgoníková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Poc	Poc	k1gMnSc1	Poc
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Polreich	Polreich	k1gMnSc1	Polreich
(	(	kIx(	(
<g/>
*	*	kIx~	*
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozvědčík	rozvědčík	k1gMnSc1	rozvědčík
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Rýdlová	Rýdlová	k1gFnSc1	Rýdlová
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Rostya	Rostya	k1gMnSc1	Rostya
Gordon	Gordon	k1gMnSc1	Gordon
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
světově	světově	k6eAd1	světově
špičková	špičkový	k2eAgFnSc1d1	špičková
personalistka	personalistka	k1gFnSc1	personalistka
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Slabý	Slabý	k1gMnSc1	Slabý
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
</s>
</p>
<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Slabý	Slabý	k1gMnSc1	Slabý
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Sodomka	Sodomek	k1gMnSc2	Sodomek
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
sbormistr	sbormistr	k1gMnSc1	sbormistr
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Sochor	Sochor	k1gMnSc1	Sochor
(	(	kIx(	(
<g/>
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Souček	Souček	k1gMnSc1	Souček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Stamic	Stamic	k1gMnSc1	Stamic
(	(	kIx(	(
<g/>
1750	[number]	k4	1750
<g/>
–	–	k?	–
<g/>
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
violista	violista	k1gMnSc1	violista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Václav	Václav	k1gMnSc1	Václav
Stamic	Stamic	k1gMnSc1	Stamic
(	(	kIx(	(
<g/>
1717	[number]	k4	1717
<g/>
–	–	k?	–
<g/>
1757	[number]	k4	1757
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
houslista	houslista	k1gMnSc1	houslista
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Staněk	Staněk	k1gMnSc1	Staněk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Stibral	Stibral	k1gMnSc1	Stibral
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
estetik	estetik	k1gMnSc1	estetik
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Suchý	Suchý	k1gMnSc1	Suchý
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šerých	šerý	k2eAgMnPc2d1	šerý
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
-	-	kIx~	-
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
</s>
</p>
<p>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
Šmídová	Šmídová	k1gFnSc1	Šmídová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jachtařka	jachtařka	k1gFnSc1	jachtařka
</s>
</p>
<p>
<s>
Marika	Marika	k1gFnSc1	Marika
Šoposká	Šoposká	k1gFnSc1	Šoposká
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Štáfl	Štáfl	k1gMnSc1	Štáfl
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Štefáček	Štefáček	k1gMnSc1	Štefáček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Štochl	Štochl	k1gMnSc1	Štochl
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
vědec	vědec	k1gMnSc1	vědec
</s>
</p>
<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Švec	Švec	k1gMnSc1	Švec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reprezentant	reprezentant	k1gMnSc1	reprezentant
řecko-římského	řecko-římský	k2eAgInSc2d1	řecko-římský
zápasu	zápas	k1gInSc2	zápas
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Vít	Vít	k1gMnSc1	Vít
Tajovský	Tajovský	k1gMnSc1	Tajovský
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
,	,	kIx,	,
opat	opat	k1gMnSc1	opat
premonstrátského	premonstrátský	k2eAgInSc2d1	premonstrátský
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Želivě	Želiv	k1gInSc6	Želiv
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
-	-	kIx~	-
1950	[number]	k4	1950
a	a	k8xC	a
1991	[number]	k4	1991
-	-	kIx~	-
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
Tolman	Tolman	k1gMnSc1	Tolman
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
vodního	vodní	k2eAgNnSc2d1	vodní
stavitelství	stavitelství	k1gNnSc2	stavitelství
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
-	-	kIx~	-
1935	[number]	k4	1935
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Vala	Vala	k1gMnSc1	Vala
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
</s>
</p>
<p>
<s>
Vítek	Vítek	k1gMnSc1	Vítek
Vaněček	Vaněček	k1gMnSc1	Vaněček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Vašíček	Vašíček	k1gMnSc1	Vašíček
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
,	,	kIx,	,
tragicky	tragicky	k6eAd1	tragicky
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
letadla	letadlo	k1gNnSc2	letadlo
u	u	k7c2	u
Jaroslavle	Jaroslavle	k1gFnSc2	Jaroslavle
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Weidenhoffer	Weidenhoffer	k1gMnSc1	Weidenhoffer
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zdechovský	Zdechovský	k2eAgMnSc1d1	Zdechovský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
aktivista	aktivista	k1gMnSc1	aktivista
<g/>
,	,	kIx,	,
analytik	analytik	k1gMnSc1	analytik
<g/>
,	,	kIx,	,
europoslanec	europoslanec	k1gMnSc1	europoslanec
za	za	k7c2	za
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
</s>
</p>
<p>
<s>
Ladislava	Ladislava	k1gFnSc1	Ladislava
Zelenková	Zelenková	k1gFnSc1	Zelenková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Zimprich	Zimprich	k1gMnSc1	Zimprich
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válečný	válečný	k2eAgMnSc1d1	válečný
letec	letec	k1gMnSc1	letec
<g/>
,	,	kIx,	,
stíhací	stíhací	k2eAgMnSc1d1	stíhací
pilot	pilot	k1gMnSc1	pilot
</s>
</p>
<p>
<s>
Hynek	Hynek	k1gMnSc1	Hynek
Zohorna	Zohorno	k1gNnSc2	Zohorno
(	(	kIx(	(
<g/>
*	*	kIx~	*
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zohorna	Zohorno	k1gNnSc2	Zohorno
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gFnSc1	Vlasta
Žehrová	Žehrová	k1gFnSc1	Žehrová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
dabérka	dabérka	k1gFnSc1	dabérka
</s>
</p>
<p>
<s>
Kamila	Kamila	k1gFnSc1	Kamila
Ženatá	ženatý	k2eAgFnSc1d1	Ženatá
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Nebesář	Nebesář	k1gMnSc1	Nebesář
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
-	-	kIx~	-
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
-	-	kIx~	-
1950	[number]	k4	1950
guvernér	guvernér	k1gMnSc1	guvernér
Státní	státní	k2eAgFnSc2d1	státní
banky	banka	k1gFnSc2	banka
československé	československý	k2eAgFnSc2d1	Československá
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Laška	Laška	k1gMnSc1	Laška
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válečný	válečný	k2eAgMnSc1d1	válečný
letec	letec	k1gMnSc1	letec
<g/>
,	,	kIx,	,
stíhací	stíhací	k2eAgMnSc1d1	stíhací
pilot	pilot	k1gMnSc1	pilot
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Brielle	Brielle	k1gNnSc1	Brielle
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
</s>
</p>
<p>
<s>
Brixen	Brixen	k1gInSc1	Brixen
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Spišská	spišský	k2eAgFnSc1d1	Spišská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
Široký	široký	k2eAgInSc1d1	široký
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Železný	železný	k2eAgInSc1d1	železný
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
BK	BK	kA	BK
Havlíčkův	Havlíčkův	k2eAgMnSc1d1	Havlíčkův
BrodFC	BrodFC	k1gMnSc1	BrodFC
Slovan	Slovan	k1gMnSc1	Slovan
Havlíčkův	Havlíčkův	k2eAgInSc4d1	Havlíčkův
Brod	Brod	k1gInSc4	Brod
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kamenů	kámen	k1gInPc2	kámen
zmizelých	zmizelý	k2eAgInPc2d1	zmizelý
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Havlíčkův	Havlíčkův	k2eAgInSc4d1	Havlíčkův
Brod	Brod	k1gInSc4	Brod
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc4	obec
Havlíčkův	Havlíčkův	k2eAgInSc4d1	Havlíčkův
Brod	Brod	k1gInSc4	Brod
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
http://www.muhb.cz/	[url]	k?	http://www.muhb.cz/
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
Mapový	mapový	k2eAgInSc1d1	mapový
server	server	k1gInSc1	server
</s>
</p>
<p>
<s>
Webová	webový	k2eAgFnSc1d1	webová
kamera	kamera	k1gFnSc1	kamera
–	–	k?	–
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
Havlíčkovo	Havlíčkův	k2eAgNnSc4d1	Havlíčkovo
náměstí	náměstí	k1gNnSc4	náměstí
</s>
</p>
<p>
<s>
MHD	MHD	kA	MHD
–	–	k?	–
jízdní	jízdní	k2eAgInPc4d1	jízdní
řády	řád	k1gInPc4	řád
<g/>
,	,	kIx,	,
trasy	trasa	k1gFnPc4	trasa
linek	linka	k1gFnPc2	linka
</s>
</p>
