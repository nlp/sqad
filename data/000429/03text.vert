<s>
Jiří	Jiří	k1gMnSc1	Jiří
Orten	Orten	k2eAgMnSc1d1	Orten
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jiří	Jiří	k1gMnSc1	Jiří
Ohrenstein	Ohrenstein	k1gMnSc1	Ohrenstein
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1919	[number]	k4	1919
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
jako	jako	k8xC	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
manželů	manžel	k1gMnPc2	manžel
Eduarda	Eduard	k1gMnSc2	Eduard
a	a	k8xC	a
Berty	Berta	k1gFnSc2	Berta
Ohrensteinových	Ohrensteinová	k1gFnPc2	Ohrensteinová
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
malý	malý	k2eAgInSc4d1	malý
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
střižním	střižní	k2eAgNnSc7d1	střižní
a	a	k8xC	a
galanterním	galanterní	k2eAgNnSc7d1	galanterní
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
místní	místní	k2eAgFnSc4d1	místní
reálku	reálka	k1gFnSc4	reálka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
na	na	k7c4	na
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
studoval	studovat	k5eAaImAgMnS	studovat
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
archivář	archivář	k1gMnSc1	archivář
karlínské	karlínský	k2eAgFnSc2d1	Karlínská
firmy	firma	k1gFnSc2	firma
Crediton	Crediton	k1gInSc1	Crediton
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dramatickém	dramatický	k2eAgNnSc6d1	dramatické
oddělení	oddělení	k1gNnSc6	oddělení
Státní	státní	k2eAgFnSc2d1	státní
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
tři	tři	k4xCgInPc4	tři
ročníky	ročník	k1gInPc4	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
přerušil	přerušit	k5eAaPmAgMnS	přerušit
koncem	koncem	k7c2	koncem
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
židovskému	židovský	k2eAgInSc3d1	židovský
původu	původ	k1gInSc3	původ
nucen	nutit	k5eAaImNgMnS	nutit
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vyloučení	vyloučení	k1gNnSc6	vyloučení
publikoval	publikovat	k5eAaBmAgMnS	publikovat
pod	pod	k7c7	pod
cizími	cizí	k2eAgNnPc7d1	cizí
jmény	jméno	k1gNnPc7	jméno
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
recitačních	recitační	k2eAgInPc2d1	recitační
večerů	večer	k1gInPc2	večer
a	a	k8xC	a
studentského	studentský	k2eAgNnSc2d1	studentské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
Jednotném	jednotný	k2eAgInSc6d1	jednotný
svazu	svaz	k1gInSc6	svaz
na	na	k7c6	na
Zbořenci	Zbořenec	k1gMnSc6	Zbořenec
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Umělecké	umělecký	k2eAgFnSc6d1	umělecká
besedě	beseda	k1gFnSc6	beseda
jako	jako	k9	jako
Divadlo	divadlo	k1gNnSc1	divadlo
mladých	mladý	k2eAgMnPc2d1	mladý
a	a	k8xC	a
uvedlo	uvést	k5eAaPmAgNnS	uvést
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
Ortenovu	Ortenův	k2eAgFnSc4d1	Ortenova
dramatizaci	dramatizace	k1gFnSc4	dramatizace
Jammesova	Jammesův	k2eAgNnSc2d1	Jammesův
Anýzového	anýzový	k2eAgNnSc2d1	anýzové
jablka	jablko	k1gNnSc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Apollinairových	Apollinairův	k2eAgInPc6d1	Apollinairův
Prsech	prs	k1gInPc6	prs
Tiresiových	Tiresiový	k2eAgInPc6d1	Tiresiový
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Nezvalova	Nezvalův	k2eAgMnSc2d1	Nezvalův
Podivuhodného	podivuhodný	k2eAgMnSc2d1	podivuhodný
kouzelníka	kouzelník	k1gMnSc2	kouzelník
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
ještě	ještě	k9	ještě
v	v	k7c6	v
jevištní	jevištní	k2eAgFnSc6d1	jevištní
úpravě	úprava	k1gFnSc6	úprava
Horova	Horův	k2eAgMnSc2d1	Horův
Jana	Jan	k1gMnSc2	Jan
houslisty	houslista	k1gMnSc2	houslista
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nastudoval	nastudovat	k5eAaBmAgMnS	nastudovat
režisér	režisér	k1gMnSc1	režisér
Gustav	Gustav	k1gMnSc1	Gustav
Schorsch	Schorsch	k1gMnSc1	Schorsch
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
básnickou	básnický	k2eAgFnSc4d1	básnická
činnost	činnost	k1gFnSc4	činnost
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgInS	zvolit
pseudonym	pseudonym	k1gInSc1	pseudonym
Jiří	Jiří	k1gMnSc1	Jiří
Orten	Orten	k1gInSc1	Orten
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
Haló	haló	k0	haló
novinách	novina	k1gFnPc6	novina
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c4	po
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
redigoval	redigovat	k5eAaImAgInS	redigovat
rubriku	rubrika	k1gFnSc4	rubrika
mladých	mladý	k2eAgMnPc2d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
přispíval	přispívat	k5eAaImAgInS	přispívat
do	do	k7c2	do
časopisů	časopis	k1gInPc2	časopis
Hej	hej	k6eAd1	hej
rup	rupět	k5eAaImRp2nS	rupět
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
Rozhledy	rozhled	k1gInPc7	rozhled
<g/>
,	,	kIx,	,
Čteme	číst	k5eAaImIp1nP	číst
<g/>
,	,	kIx,	,
Kritický	kritický	k2eAgInSc4d1	kritický
měsíčník	měsíčník	k1gInSc4	měsíčník
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
halasovské	halasovský	k2eAgFnSc6d1	halasovská
edici	edice	k1gFnSc6	edice
První	první	k4xOgFnPc1	první
knížky	knížka	k1gFnPc1	knížka
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Václava	Václav	k1gMnSc4	Václav
Petra	Petr	k1gMnSc4	Petr
mu	on	k3xPp3gMnSc3	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
prvotina	prvotina	k1gFnSc1	prvotina
Čítanka	čítanka	k1gFnSc1	čítanka
jaro	jaro	k6eAd1	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
ji	on	k3xPp3gFnSc4	on
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Karel	Karel	k1gMnSc1	Karel
Jílek	Jílek	k1gMnSc1	Jílek
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
spojil	spojit	k5eAaPmAgMnS	spojit
i	i	k9	i
další	další	k2eAgFnSc4d1	další
sbírku	sbírka	k1gFnSc4	sbírka
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
Petrovo	Petrův	k2eAgNnSc1d1	Petrovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
vydalo	vydat	k5eAaPmAgNnS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
mrazu	mráz	k1gInSc3	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1	soukromý
tisk	tisk	k1gInSc1	tisk
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Urbánka	Urbánek	k1gMnSc2	Urbánek
k	k	k7c3	k
Novému	nový	k2eAgInSc3d1	nový
roku	rok	k1gInSc3	rok
1941	[number]	k4	1941
-	-	kIx~	-
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
báseň	báseň	k1gFnSc4	báseň
Jeremiášův	Jeremiášův	k2eAgInSc4d1	Jeremiášův
pláč	pláč	k1gInSc4	pláč
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
podepsán	podepsat	k5eAaPmNgInS	podepsat
jménem	jméno	k1gNnSc7	jméno
Jiří	Jiří	k1gMnSc1	Jiří
Jakub	Jakub	k1gMnSc1	Jakub
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
sbírka	sbírka	k1gFnSc1	sbírka
Ohnice	ohnice	k1gFnSc1	ohnice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
do	do	k7c2	do
edičního	ediční	k2eAgInSc2d1	ediční
plánu	plán	k1gInSc2	plán
Melantrichu	Melantrich	k1gInSc2	Melantrich
zařadil	zařadit	k5eAaPmAgMnS	zařadit
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
A.	A.	kA	A.
M.	M.	kA	M.
Píša	Píša	k1gMnSc1	Píša
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
Ohnice	ohnice	k1gFnSc2	ohnice
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
poslední	poslední	k2eAgFnSc7d1	poslední
knížkou	knížka	k1gFnSc7	knížka
<g/>
,	,	kIx,	,
jejíhož	jejíž	k3xOyRp3gNnSc2	jejíž
uvedení	uvedení	k1gNnSc2	uvedení
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
dožil	dožít	k5eAaPmAgMnS	dožít
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
sice	sice	k8xC	sice
ještě	ještě	k6eAd1	ještě
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
dvě	dva	k4xCgFnPc4	dva
sbírky	sbírka	k1gFnPc4	sbírka
-	-	kIx~	-
Elegie	elegie	k1gFnPc4	elegie
a	a	k8xC	a
Scestí	scestí	k1gNnSc4	scestí
-	-	kIx~	-
ale	ale	k8xC	ale
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
objevit	objevit	k5eAaPmF	objevit
na	na	k7c6	na
knižních	knižní	k2eAgInPc6d1	knižní
pultech	pult	k1gInPc6	pult
až	až	k8xS	až
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
zemřel	zemřít	k5eAaPmAgMnS	zemřít
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
co	co	k3yQnSc1	co
jeho	jeho	k3xOp3gNnSc4	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Ota	Ota	k1gMnSc1	Ota
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Jiří	Jiří	k1gMnSc1	Jiří
oporou	opora	k1gFnSc7	opora
matce	matka	k1gFnSc3	matka
a	a	k8xC	a
malému	malý	k2eAgMnSc3d1	malý
bratrovi	bratr	k1gMnSc3	bratr
Zdeňkovi	Zdeněk	k1gMnSc3	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
toto	tento	k3xDgNnSc1	tento
pouto	pouto	k1gNnSc1	pouto
bylo	být	k5eAaImAgNnS	být
částečně	částečně	k6eAd1	částečně
narušené	narušený	k2eAgNnSc1d1	narušené
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Orten	Orten	k1gInSc1	Orten
nesměl	smět	k5eNaImAgInS	smět
opouštět	opouštět	k5eAaImF	opouštět
území	území	k1gNnSc4	území
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Rozchod	rozchod	k1gInSc1	rozchod
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
cele	cele	k6eAd1	cele
upnul	upnout	k5eAaPmAgInS	upnout
<g/>
,	,	kIx,	,
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
zbytek	zbytek	k1gInSc1	zbytek
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
i	i	k9	i
nadále	nadále	k6eAd1	nadále
udržoval	udržovat	k5eAaImAgMnS	udržovat
styky	styk	k1gInPc4	styk
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
kolegy	kolega	k1gMnPc7	kolega
sdruženými	sdružený	k2eAgFnPc7d1	sdružená
v	v	k7c6	v
neoficiálním	neoficiální	k2eAgInSc6d1	neoficiální
klubu	klub	k1gInSc6	klub
Noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
osamělejší	osamělý	k2eAgInSc1d2	osamělejší
<g/>
.	.	kIx.	.
</s>
<s>
Přinucen	přinucen	k2eAgInSc1d1	přinucen
okolnostmi	okolnost	k1gFnPc7	okolnost
střídal	střídat	k5eAaImAgMnS	střídat
byty	byt	k1gInPc4	byt
<g/>
,	,	kIx,	,
střídal	střídat	k5eAaImAgMnS	střídat
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
básníkem	básník	k1gMnSc7	básník
Hanušem	Hanuš	k1gMnSc7	Hanuš
Bonnem	Bonn	k1gInSc7	Bonn
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Židovské	židovský	k2eAgFnSc6d1	židovská
náboženské	náboženský	k2eAgFnSc6d1	náboženská
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodným	pozoruhodný	k2eAgNnSc7d1	pozoruhodné
svědectvím	svědectví	k1gNnSc7	svědectví
o	o	k7c6	o
Ortenově	Ortenův	k2eAgInSc6d1	Ortenův
životě	život	k1gInSc6	život
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc1	tři
knihy	kniha	k1gFnPc1	kniha
jeho	jeho	k3xOp3gMnPc2	jeho
deníků	deník	k1gInPc2	deník
-	-	kIx~	-
Modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
Žíhaná	žíhaný	k2eAgFnSc1d1	žíhaná
a	a	k8xC	a
Červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
si	se	k3xPyFc3	se
zaznamenával	zaznamenávat	k5eAaImAgInS	zaznamenávat
své	svůj	k3xOyFgInPc4	svůj
postřehy	postřeh	k1gInPc4	postřeh
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc4	báseň
i	i	k8xC	i
výpisky	výpiska	k1gFnPc4	výpiska
z	z	k7c2	z
četby	četba	k1gFnSc2	četba
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1938	[number]	k4	1938
až	až	k9	až
do	do	k7c2	do
osudného	osudný	k2eAgMnSc2d1	osudný
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
přejet	přejet	k5eAaPmNgInS	přejet
projíždějící	projíždějící	k2eAgFnSc7d1	projíždějící
německou	německý	k2eAgFnSc7d1	německá
sanitkou	sanitka	k1gFnSc7	sanitka
<g/>
.	.	kIx.	.
</s>
<s>
Následkům	následek	k1gInPc3	následek
těžkého	těžký	k2eAgNnSc2d1	těžké
zranění	zranění	k1gNnSc2	zranění
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1941	[number]	k4	1941
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
dvaadvaceti	dvaadvacet	k4xCc2	dvaadvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ortenova	Ortenův	k2eAgFnSc1d1	Ortenova
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Jiřího	Jiří	k1gMnSc2	Jiří
Ortena	Orten	k1gMnSc2	Orten
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
festival	festival	k1gInSc1	festival
Ortenova	Ortenův	k2eAgFnSc1d1	Ortenova
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
i	i	k8xC	i
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
literární	literární	k2eAgFnSc1d1	literární
soutěž	soutěž	k1gFnSc1	soutěž
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
autory	autor	k1gMnPc4	autor
poezie	poezie	k1gFnSc2	poezie
do	do	k7c2	do
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
také	také	k6eAd1	také
pražské	pražský	k2eAgNnSc1d1	Pražské
Ortenovo	Ortenův	k2eAgNnSc1d1	Ortenovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
velký	velký	k2eAgInSc4d1	velký
talent	talent	k1gInSc4	talent
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
členem	člen	k1gMnSc7	člen
žádné	žádný	k3yNgFnSc2	žádný
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
existencialismem	existencialismus	k1gInSc7	existencialismus
<g/>
.	.	kIx.	.
</s>
<s>
Ortenovo	Ortenův	k2eAgNnSc1d1	Ortenovo
dílo	dílo	k1gNnSc1	dílo
velmi	velmi	k6eAd1	velmi
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
českou	český	k2eAgFnSc4d1	Česká
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poezie	poezie	k1gFnSc1	poezie
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
pocity	pocit	k1gInPc4	pocit
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
zaskočených	zaskočený	k2eAgFnPc2d1	zaskočená
a	a	k8xC	a
zraněných	zraněný	k2eAgFnPc2d1	zraněná
těžkou	těžký	k2eAgFnSc7d1	těžká
dobou	doba	k1gFnSc7	doba
<g/>
.	.	kIx.	.
</s>
<s>
Čítanka	čítanka	k1gFnSc1	čítanka
jaro	jaro	k1gNnSc4	jaro
-	-	kIx~	-
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
jako	jako	k8xS	jako
e-kniha	eniha	k1gFnSc1	e-kniha
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
mrazu	mráz	k1gInSc3	mráz
-	-	kIx~	-
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc4d1	původní
láskyplný	láskyplný	k2eAgInSc4d1	láskyplný
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
okolí	okolí	k1gNnSc3	okolí
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
beznaděj	beznaděj	k1gFnSc4	beznaděj
a	a	k8xC	a
opuštěnost	opuštěnost	k1gFnSc4	opuštěnost
-	-	kIx~	-
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
motivy	motiv	k1gInPc7	motiv
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1	vydáno
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
jako	jako	k8xS	jako
e-kniha	eniha	k1gMnSc1	e-kniha
Jeremiášův	Jeremiášův	k2eAgInSc4d1	Jeremiášův
pláč	pláč	k1gInSc4	pláč
-	-	kIx~	-
vyšla	vyjít	k5eAaPmAgFnS	vyjít
též	též	k9	též
pod	pod	k7c7	pod
cizím	cizí	k2eAgNnSc7d1	cizí
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
jako	jako	k8xS	jako
e-kniha	eniha	k1gFnSc1	e-kniha
Ohnice	ohnice	k1gFnSc1	ohnice
-	-	kIx~	-
1941	[number]	k4	1941
-	-	kIx~	-
tuší	tušit	k5eAaImIp3nS	tušit
blížící	blížící	k2eAgMnSc1d1	blížící
se	se	k3xPyFc4	se
konec	konec	k1gInSc1	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
jako	jako	k8xC	jako
e-kniha	eniha	k1gMnSc1	e-kniha
Eta	Eta	k1gMnSc1	Eta
<g/>
,	,	kIx,	,
Eta	Eta	k1gMnPc1	Eta
žlutí	žlutit	k5eAaImIp3nP	žlutit
ptáci	pták	k1gMnPc1	pták
-	-	kIx~	-
próza	próza	k1gFnSc1	próza
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
jako	jako	k8xC	jako
e-kniha	eniha	k1gFnSc1	e-kniha
Většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k6eAd1	až
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
,	,	kIx,	,
po	po	k7c6	po
r.	r.	kA	r.
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
Elegie	elegie	k1gFnSc1	elegie
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
jako	jako	k8xC	jako
e-kniha	eniha	k1gFnSc1	e-kniha
Dílo	dílo	k1gNnSc1	dílo
Jiřího	Jiří	k1gMnSc2	Jiří
Ortena	Orten	k1gMnSc2	Orten
Deníky	deník	k1gInPc1	deník
Jiřího	Jiří	k1gMnSc2	Jiří
Ortena	Orten	k1gMnSc2	Orten
-	-	kIx~	-
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k9	až
v	v	k7c6	v
r.	r.	kA	r.
1958	[number]	k4	1958
-	-	kIx~	-
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
deníky	deník	k1gInPc1	deník
byly	být	k5eAaImAgInP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
desek	deska	k1gFnPc2	deska
sešitů	sešit	k1gInPc2	sešit
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
si	se	k3xPyFc3	se
své	svůj	k3xOyFgInPc4	svůj
zápisy	zápis	k1gInPc4	zápis
psal	psát	k5eAaImAgInS	psát
-	-	kIx~	-
Modrá	modrat	k5eAaImIp3nS	modrat
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
Žíhaná	žíhaný	k2eAgFnSc1d1	žíhaná
kniha	kniha	k1gFnSc1	kniha
a	a	k8xC	a
Červená	červený	k2eAgFnSc1d1	červená
kniha	kniha	k1gFnSc1	kniha
</s>
