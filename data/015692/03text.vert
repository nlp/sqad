<s>
Ewa	Ewa	k?
Sowińska	Sowińska	k1gFnSc1
</s>
<s>
Ewa	Ewa	k?
Sowińska	Sowińska	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1944	#num#	k4
(	(	kIx(
<g/>
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Bydhošť	Bydhošť	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Medical	Medicat	k5eAaPmAgMnS
University	universita	k1gFnSc2
of	of	k?
Białystok	Białystok	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
politička	politička	k1gFnSc1
Politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
</s>
<s>
Liga	liga	k1gFnSc1
polských	polský	k2eAgMnPc2d1
rodinPolská	rodinPolský	k2eAgFnSc1d1
sjednocená	sjednocený	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
Member	Member	k1gInSc1
of	of	k?
the	the	k?
Sejm	Sejm	k1gInSc1
of	of	k?
the	the	k?
Republic	Republice	k1gFnPc2
of	of	k?
Poland	Polanda	k1gFnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Eva	Eva	k1gFnSc1
Barbara	Barbara	k1gFnSc1
Sowińska	Sowińska	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1944	#num#	k4
Bydhošť	Bydhošť	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
polská	polský	k2eAgFnSc1d1
lékařka	lékařka	k1gFnSc1
a	a	k8xC
politička	politička	k1gFnSc1
z	z	k7c2
Ligy	liga	k1gFnSc2
polských	polský	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
je	být	k5eAaImIp3nS
poslankyní	poslankyně	k1gFnSc7
Sejmu	Sejm	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
jara	jaro	k1gNnSc2
2006	#num#	k4
zastává	zastávat	k5eAaImIp3nS
úřad	úřad	k1gInSc4
Mluvčí	mluvčí	k1gFnSc1
práv	právo	k1gNnPc2
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
Ewa	Ewa	k1gFnSc1
Sowińska	Sowińska	k1gFnSc1
ukončila	ukončit	k5eAaPmAgFnS
studia	studio	k1gNnPc4
na	na	k7c6
Lékařské	lékařský	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Akademie	akademie	k1gFnSc2
medicíny	medicína	k1gFnSc2
v	v	k7c6
Białymstoku	Białymstok	k1gInSc6
(	(	kIx(
<g/>
Akademia	Akademia	k1gFnSc1
Medyczna	Medyczna	k1gFnSc1
w	w	k?
Białymstoku	Białymstok	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
1978	#num#	k4
získala	získat	k5eAaPmAgFnS
II	II	kA
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
specializace	specializace	k1gFnSc2
v	v	k7c6
oboru	obor	k1gInSc6
vnitřních	vnitřní	k2eAgFnPc2d1
chorob	choroba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
studií	studie	k1gFnPc2
pracovala	pracovat	k5eAaImAgFnS
jako	jako	k9
lékařka	lékařka	k1gFnSc1
<g/>
:	:	kIx,
v	v	k7c6
letech	let	k1gInPc6
1968	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
jako	jako	k8xS,k8xC
mladší	mladý	k2eAgMnSc1d2
asistent	asistent	k1gMnSc1
ve	v	k7c6
vesnickém	vesnický	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
zdraví	zdraví	k1gNnSc2
ve	v	k7c6
vojvodství	vojvodství	k1gNnSc6
pomořském	pomořský	k2eAgNnSc6d1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1972	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
jako	jako	k8xC,k8xS
asistent	asistent	k1gMnSc1
ve	v	k7c6
vesnickém	vesnický	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
zdraví	zdraví	k1gNnSc2
ve	v	k7c6
vojvodství	vojvodství	k1gNnSc6
lodžském	lodžský	k2eAgNnSc6d1
<g/>
,	,	kIx,
od	od	k7c2
1977	#num#	k4
do	do	k7c2
1982	#num#	k4
jako	jako	k8xC,k8xS
vedoucí	vedoucí	k1gMnSc1
zdravotního	zdravotní	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
č.	č.	k?
52	#num#	k4
w	w	k?
Lodži	Lodž	k1gFnSc3
a	a	k8xC
od	od	k7c2
1982	#num#	k4
do	do	k7c2
2004	#num#	k4
jako	jako	k8xC,k8xS
starší	starý	k2eAgMnSc1d2
asistent	asistent	k1gMnSc1
ve	v	k7c6
Vojvodském	Vojvodský	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
medicíny	medicína	k1gFnSc2
v	v	k7c6
Lodži	Lodž	k1gFnSc6
(	(	kIx(
<g/>
Wojewódzki	Wojewódzk	k1gFnSc6
Ośrodek	Ośrodka	k1gFnPc2
Medycyny	Medycyna	k1gMnSc2
Pracy	Praca	k1gMnSc2
w	w	k?
Łodzi	Łodze	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1977	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
byla	být	k5eAaImAgFnS
členkou	členka	k1gFnSc7
komunistické	komunistický	k2eAgFnSc2d1
Polské	polský	k2eAgFnSc2d1
sjednocené	sjednocený	k2eAgFnSc2d1
dělnické	dělnický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
Polska	Polska	k1gFnSc1
Zjednoczona	Zjednoczona	k1gFnSc1
Partia	Partia	k1gFnSc1
Robotnicza	Robotnicza	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
2003	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
pracovala	pracovat	k5eAaImAgFnS
v	v	k7c4
Katolicko-národní	katolicko-národní	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
(	(	kIx(
<g/>
Ruch	ruch	k1gInSc1
Katolicko-Narodowy	Katolicko-Narodowa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
je	být	k5eAaImIp3nS
členkou	členka	k1gFnSc7
Ligy	liga	k1gFnSc2
polských	polský	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Sejmu	Sejm	k1gInSc2
v	v	k7c6
září	září	k1gNnSc6
2001	#num#	k4
kandidovala	kandidovat	k5eAaImAgFnS
za	za	k7c4
LPR	LPR	kA
v	v	k7c6
okrese	okres	k1gInSc6
lodžském	lodžský	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandát	mandát	k1gInSc1
získala	získat	k5eAaPmAgFnS
v	v	k7c6
létě	léto	k1gNnSc6
2004	#num#	k4
jako	jako	k9
náhradnice	náhradnice	k1gFnPc1
za	za	k7c4
Urszulu	Urszula	k1gFnSc4
Krupovou	Krupová	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
mandátu	mandát	k1gInSc3
vzdala	vzdát	k5eAaPmAgFnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
byla	být	k5eAaImAgFnS
zvolena	zvolit	k5eAaPmNgFnS
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
Sowińska	Sowińsk	k1gInSc2
poslanecký	poslanecký	k2eAgInSc4d1
mandát	mandát	k1gInSc4
obhájila	obhájit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2006	#num#	k4
ji	on	k3xPp3gFnSc4
Sejm	Sejm	k1gInSc1
vybral	vybrat	k5eAaPmAgInS
do	do	k7c2
úřadu	úřad	k1gInSc2
Mluvčí	mluvčí	k1gMnSc1
práv	právo	k1gNnPc2
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
potvrdil	potvrdit	k5eAaPmAgMnS
volbu	volba	k1gFnSc4
i	i	k8xC
Senát	senát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřad	úřad	k1gInSc1
převzala	převzít	k5eAaPmAgFnS
po	po	k7c6
složení	složení	k1gNnSc6
slibu	slib	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
navrhovala	navrhovat	k5eAaImAgFnS
povinnou	povinný	k2eAgFnSc4d1
registraci	registrace	k1gFnSc4
každého	každý	k3xTgInSc2
svazku	svazek	k1gInSc2
spočívajícího	spočívající	k2eAgInSc2d1
ve	v	k7c6
společném	společný	k2eAgNnSc6d1
bydlení	bydlení	k1gNnSc6
či	či	k8xC
provozováním	provozování	k1gNnSc7
společného	společný	k2eAgNnSc2d1
hospodaření	hospodaření	k1gNnSc2
v	v	k7c6
domácnosti	domácnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
březnu	březen	k1gInSc6
2007	#num#	k4
roku	rok	k1gInSc2
požadovala	požadovat	k5eAaImAgFnS
vytvoření	vytvoření	k1gNnSc4
seznamu	seznam	k1gInSc2
povolání	povolání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
by	by	kYmCp3nP
nesměli	smět	k5eNaImAgMnP
vykonávat	vykonávat	k5eAaImF
homosexuálové	homosexuál	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
na	na	k7c6
něm	on	k3xPp3gMnSc6
být	být	k5eAaImF
povolání	povolání	k1gNnSc1
přicházející	přicházející	k2eAgNnSc1d1
do	do	k7c2
kontaktu	kontakt	k1gInSc2
s	s	k7c7
dětmi	dítě	k1gFnPc7
a	a	k8xC
mládeží	mládež	k1gFnSc7
<g/>
,	,	kIx,
např.	např.	kA
učitelé	učitel	k1gMnPc1
a	a	k8xC
vychovatelé	vychovatel	k1gMnPc1
na	na	k7c6
internátech	internát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
dubnu	duben	k1gInSc6
2007	#num#	k4
zveřejnila	zveřejnit	k5eAaPmAgFnS
televize	televize	k1gFnSc1
TVN	TVN	kA
její	její	k3xOp3gInSc4
dopis	dopis	k1gInSc4
papeži	papež	k1gMnSc3
Benediktu	Benedikt	k1gMnSc3
XVI	XVI	kA
<g/>
.	.	kIx.
na	na	k7c4
podporu	podpora	k1gFnSc4
arcibiskupa	arcibiskup	k1gMnSc2
Stanisława	Stanisławus	k1gMnSc2
Wielguse	Wielguse	k1gFnSc2
<g/>
,	,	kIx,
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
kritizovaného	kritizovaný	k2eAgInSc2d1
za	za	k7c4
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
komunistickou	komunistický	k2eAgFnSc7d1
tajnou	tajný	k2eAgFnSc7d1
policií	policie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontroverze	kontroverze	k1gFnSc1
vzbudil	vzbudit	k5eAaPmAgInS
nejen	nejen	k6eAd1
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
byly	být	k5eAaImAgInP
požadavky	požadavek	k1gInPc1
na	na	k7c4
Wielgusovo	Wielgusův	k2eAgNnSc4d1
odstoupení	odstoupení	k1gNnSc4
projevem	projev	k1gInSc7
války	válka	k1gFnSc2
laických	laický	k2eAgNnPc2d1
médií	médium	k1gNnPc2
a	a	k8xC
laicko-zednářských	laicko-zednářský	k2eAgInPc2d1
spolků	spolek	k1gInPc2
proti	proti	k7c3
Církvi	církev	k1gFnSc3
<g/>
,	,	kIx,
Jejím	její	k3xOp3gMnPc3
pastýřům	pastýř	k1gMnPc3
a	a	k8xC
lidu	lido	k1gNnSc3
Božímu	boží	k2eAgNnSc3d1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
dopis	dopis	k1gInSc1
byl	být	k5eAaImAgInS
odeslán	odeslat	k5eAaPmNgInS
na	na	k7c6
hlavičkovém	hlavičkový	k2eAgInSc6d1
papíře	papír	k1gInSc6
jejího	její	k3xOp3gInSc2
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1
práv	právo	k1gNnPc2
dítěte	dítě	k1gNnSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Paweł	Paweł	k1gMnSc1
Jaros	Jaros	k1gMnSc1
</s>
<s>
od	od	k7c2
2006	#num#	k4
Ewa	Ewa	k1gFnPc2
Sowińska	Sowińsk	k1gInSc2
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Marek	Marek	k1gMnSc1
Michalak	Michalak	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
Ewa	Ewa	k1gFnSc1
Sowińska	Sowińska	k1gFnSc1
-	-	kIx~
Ewa	Ewa	k1gFnSc1
Sowińska	Sowińska	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Sejmu	sejmout	k5eAaPmIp1nS
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ewa	Ewa	k1gMnSc2
Sowińska	Sowińsek	k1gMnSc2
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Rzecznik	Rzecznik	k1gMnSc1
Praw	Praw	k1gMnSc2
Dziecka	Dziecko	k1gNnSc2
o	o	k7c4
masońskich	masońskich	k1gInSc4
atakach	atakacha	k1gFnPc2
na	na	k7c4
abp	abp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wielgusa	Wielgus	k1gMnSc2
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2007	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1712	#num#	k4
4446	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
163115971	#num#	k4
</s>
