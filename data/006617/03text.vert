<s>
Erich	Erich	k1gMnSc1	Erich
Maria	Maria	k1gFnSc1	Maria
Remarque	Remarque	k1gFnSc1	Remarque
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Erich	Erich	k1gMnSc1	Erich
Paul	Paul	k1gMnSc1	Paul
Remark	Remark	k1gInSc1	Remark
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Osnabrück	Osnabrück	k1gInSc1	Osnabrück
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
Locarno	Locarno	k1gNnSc1	Locarno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
pacifistický	pacifistický	k2eAgMnSc1d1	pacifistický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
napsal	napsat	k5eAaBmAgMnS	napsat
mnoho	mnoho	k4c1	mnoho
děl	dělo	k1gNnPc2	dělo
o	o	k7c6	o
hrůzách	hrůza	k1gFnPc6	hrůza
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
románem	román	k1gInSc7	román
je	být	k5eAaImIp3nS	být
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc1	klid
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
německých	německý	k2eAgMnPc6d1	německý
vojácích	voják	k1gMnPc6	voják
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
později	pozdě	k6eAd2	pozdě
posloužila	posloužit	k5eAaPmAgFnS	posloužit
za	za	k7c4	za
předlohu	předloha	k1gFnSc4	předloha
oscarového	oscarový	k2eAgInSc2d1	oscarový
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pacifismus	pacifismus	k1gInSc1	pacifismus
a	a	k8xC	a
protiválečný	protiválečný	k2eAgInSc1d1	protiválečný
přístup	přístup	k1gInSc1	přístup
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
udělal	udělat	k5eAaPmAgMnS	udělat
nepřítele	nepřítel	k1gMnSc4	nepřítel
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1898	[number]	k4	1898
v	v	k7c6	v
dolnosaském	dolnosaský	k2eAgInSc6d1	dolnosaský
Osnabrücku	Osnabrück	k1gInSc6	Osnabrück
Anně	Anna	k1gFnSc6	Anna
Marii	Maria	k1gFnSc3	Maria
Remarkové	Remarková	k1gFnSc3	Remarková
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Stallknecht	Stallknecht	k1gInSc1	Stallknecht
<g/>
)	)	kIx)	)
a	a	k8xC	a
knihvazači	knihvazač	k1gMnSc3	knihvazač
Peteru	Peter	k1gMnSc3	Peter
Franzi	Franze	k1gFnSc4	Franze
Remarkovi	Remarek	k1gMnSc3	Remarek
jako	jako	k8xC	jako
druhé	druhý	k4xOgFnSc2	druhý
nejstarší	starý	k2eAgFnSc2d3	nejstarší
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
učitelském	učitelský	k2eAgInSc6d1	učitelský
ústavu	ústav	k1gInSc6	ústav
v	v	k7c6	v
Münsteru	Münster	k1gInSc6	Münster
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
odešel	odejít	k5eAaPmAgMnS	odejít
jako	jako	k9	jako
18	[number]	k4	18
<g/>
letý	letý	k2eAgInSc4d1	letý
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
byl	být	k5eAaImAgInS	být
brzy	brzy	k6eAd1	brzy
vážně	vážně	k6eAd1	vážně
raněn	raněn	k2eAgInSc1d1	raněn
a	a	k8xC	a
konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
lazaretu	lazaret	k1gInSc6	lazaret
v	v	k7c6	v
Duisburgu	Duisburg	k1gInSc6	Duisburg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
války	válka	k1gFnSc2	válka
měl	mít	k5eAaImAgMnS	mít
problémy	problém	k1gInPc4	problém
začlenit	začlenit	k5eAaPmF	začlenit
se	se	k3xPyFc4	se
do	do	k7c2	do
normální	normální	k2eAgFnSc2d1	normální
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
řadu	řada	k1gFnSc4	řada
povolání	povolání	k1gNnSc2	povolání
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
hudebníkem	hudebník	k1gMnSc7	hudebník
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
malířem	malíř	k1gMnSc7	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
též	též	k9	též
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgMnSc1d1	obchodní
cestující	cestující	k1gMnSc1	cestující
aj.	aj.	kA	aj.
V	v	k7c6	v
letech	let	k1gInPc6	let
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
časopisů	časopis	k1gInPc2	časopis
"	"	kIx"	"
<g/>
Sport	sport	k1gInSc1	sport
im	im	k?	im
Bild	Bild	k1gInSc1	Bild
<g/>
"	"	kIx"	"
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
"	"	kIx"	"
<g/>
Echo	echo	k1gNnSc1	echo
Continental	Continental	k1gMnSc1	Continental
<g/>
"	"	kIx"	"
v	v	k7c6	v
Hannoveru	Hannover	k1gInSc6	Hannover
<g/>
.	.	kIx.	.
</s>
<s>
Cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Balkáně	Balkán	k1gInSc6	Balkán
a	a	k8xC	a
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Ilse	Ilse	k1gFnSc7	Ilse
Juttou	Jutta	k1gFnSc7	Jutta
Zambonovou	Zambonová	k1gFnSc7	Zambonová
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc1	manželství
však	však	k9	však
trvalo	trvat	k5eAaImAgNnS	trvat
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
let	léto	k1gNnPc2	léto
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
spolu	spolu	k6eAd1	spolu
letěli	letět	k5eAaImAgMnP	letět
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Zambonová	Zambonová	k1gFnSc1	Zambonová
nemusela	muset	k5eNaImAgFnS	muset
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
stali	stát	k5eAaPmAgMnP	stát
naturalizovanými	naturalizovaný	k2eAgMnPc7d1	naturalizovaný
občany	občan	k1gMnPc7	občan
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
definitivně	definitivně	k6eAd1	definitivně
skončilo	skončit	k5eAaPmAgNnS	skončit
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc4	několik
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Prosadit	prosadit	k5eAaPmF	prosadit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
ale	ale	k9	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
vydáním	vydání	k1gNnSc7	vydání
knihy	kniha	k1gFnSc2	kniha
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
rok	rok	k1gInSc4	rok
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
ji	on	k3xPp3gFnSc4	on
zfilmoval	zfilmovat	k5eAaPmAgMnS	zfilmovat
režisér	režisér	k1gMnSc1	režisér
Lewis	Lewis	k1gFnSc2	Lewis
Mileston	Mileston	k1gInSc1	Mileston
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
tři	tři	k4xCgFnPc4	tři
a	a	k8xC	a
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacismu	nacismus	k1gInSc2	nacismus
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgMnPc2d1	zakázaný
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgMnS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
německého	německý	k2eAgNnSc2d1	německé
občanství	občanství	k1gNnSc2	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Nacistická	nacistický	k2eAgFnSc1d1	nacistická
propaganda	propaganda	k1gFnSc1	propaganda
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Erich	Erich	k1gMnSc1	Erich
Maria	Mario	k1gMnSc2	Mario
Remarque	Remarque	k1gNnSc2	Remarque
je	být	k5eAaImIp3nS	být
doopravdy	doopravdy	k6eAd1	doopravdy
Paul	Paul	k1gMnSc1	Paul
Kramer	Kramer	k1gMnSc1	Kramer
(	(	kIx(	(
<g/>
Remarque	Remarque	k1gInSc1	Remarque
pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Žid	Žid	k1gMnSc1	Žid
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
první	první	k4xOgFnPc4	první
světové	světový	k2eAgFnPc4d1	světová
války	válka	k1gFnPc4	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
ji	on	k3xPp3gFnSc4	on
nemůže	moct	k5eNaImIp3nS	moct
popisovat	popisovat	k5eAaImF	popisovat
<g/>
.	.	kIx.	.
</s>
<s>
Označila	označit	k5eAaPmAgFnS	označit
ho	on	k3xPp3gInSc4	on
za	za	k7c2	za
"	"	kIx"	"
<g/>
literárního	literární	k2eAgMnSc2d1	literární
zrádce	zrádce	k1gMnSc2	zrádce
<g/>
"	"	kIx"	"
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
veřejně	veřejně	k6eAd1	veřejně
páleny	pálen	k2eAgInPc1d1	pálen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc1	jeho
popis	popis	k1gInSc1	popis
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
militantní	militantní	k2eAgInSc4d1	militantní
režim	režim	k1gInSc4	režim
krajně	krajně	k6eAd1	krajně
nevhodný	vhodný	k2eNgMnSc1d1	nevhodný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
sester	sestra	k1gFnPc2	sestra
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gNnSc4	on
později	pozdě	k6eAd2	pozdě
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
knihy	kniha	k1gFnSc2	kniha
Jiskra	jiskra	k1gFnSc1	jiskra
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
žil	žít	k5eAaImAgMnS	žít
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
filmovou	filmový	k2eAgFnSc7d1	filmová
hvězdou	hvězda	k1gFnSc7	hvězda
Paulette	Paulett	k1gInSc5	Paulett
Goddard	Goddarda	k1gFnPc2	Goddarda
<g/>
,	,	kIx,	,
bývalou	bývalý	k2eAgFnSc7d1	bývalá
manželkou	manželka	k1gFnSc7	manželka
Charlieho	Charlie	k1gMnSc2	Charlie
Chaplina	Chaplin	k2eAgFnSc1d1	Chaplina
<g/>
.	.	kIx.	.
</s>
<s>
Remarque	Remarque	k6eAd1	Remarque
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
srdeční	srdeční	k2eAgFnSc4d1	srdeční
chorobu	choroba	k1gFnSc4	choroba
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1970	[number]	k4	1970
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
v	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Locarnu	Locarno	k1gNnSc6	Locarno
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
spisovatelů	spisovatel	k1gMnPc2	spisovatel
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Otevřeně	otevřeně	k6eAd1	otevřeně
popisoval	popisovat	k5eAaImAgMnS	popisovat
hrůzy	hrůza	k1gFnPc4	hrůza
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
nikdy	nikdy	k6eAd1	nikdy
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
ztracené	ztracený	k2eAgFnSc3d1	ztracená
generaci	generace	k1gFnSc3	generace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgInS	přihlásit
(	(	kIx(	(
<g/>
nepatřil	patřit	k5eNaImAgInS	patřit
však	však	k9	však
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nebyl	být	k5eNaImAgMnS	být
Američan	Američan	k1gMnSc1	Američan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
téma	téma	k1gNnSc4	téma
automobilových	automobilový	k2eAgInPc2d1	automobilový
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
Remarque	Remarque	k1gFnSc1	Remarque
se	se	k3xPyFc4	se
o	o	k7c4	o
závody	závod	k1gInPc4	závod
nejen	nejen	k6eAd1	nejen
zajímal	zajímat	k5eAaImAgInS	zajímat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sám	sám	k3xTgInSc1	sám
i	i	k8xC	i
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
aktivně	aktivně	k6eAd1	aktivně
závodil	závodit	k5eAaImAgMnS	závodit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
romány	román	k1gInPc1	román
jsou	být	k5eAaImIp3nP	být
protiválečné	protiválečný	k2eAgInPc1d1	protiválečný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
pacifistické	pacifistický	k2eAgNnSc4d1	pacifistické
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
silně	silně	k6eAd1	silně
antifašisticky	antifašisticky	k6eAd1	antifašisticky
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
se	s	k7c7	s
zlatýma	zlatý	k2eAgNnPc7d1	Zlaté
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
překládáno	překládán	k2eAgNnSc1d1	překládáno
jako	jako	k8xC	jako
Gam	Gam	k1gFnSc7	Gam
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
Domov	domov	k1gInSc4	domov
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
též	též	k9	též
překládáno	překládán	k2eAgNnSc1d1	překládáno
jako	jako	k8xC	jako
Říše	říše	k1gFnSc1	říše
snů	sen	k1gInPc2	sen
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
–	–	k?	–
Příběh	příběh	k1gInSc1	příběh
automobilového	automobilový	k2eAgMnSc2d1	automobilový
závodníka	závodník	k1gMnSc2	závodník
Kaie	Kai	k1gMnSc2	Kai
<g/>
,	,	kIx,	,
nevyrovnaného	vyrovnaný	k2eNgMnSc2d1	nevyrovnaný
a	a	k8xC	a
nepokojného	pokojný	k2eNgMnSc2d1	nepokojný
mladého	mladý	k2eAgMnSc2d1	mladý
světáka	světák	k1gMnSc2	světák
a	a	k8xC	a
bonvivána	bonviván	k1gMnSc2	bonviván
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyprávěním	vyprávění	k1gNnSc7	vyprávění
o	o	k7c6	o
chlapském	chlapský	k2eAgNnSc6d1	chlapské
kamarádství	kamarádství	k1gNnSc6	kamarádství
a	a	k8xC	a
rivalitě	rivalita	k1gFnSc6	rivalita
<g/>
,	,	kIx,	,
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
<g/>
,	,	kIx,	,
o	o	k7c6	o
těkavém	těkavý	k2eAgInSc6d1	těkavý
neklidu	neklid	k1gInSc6	neklid
<g/>
,	,	kIx,	,
výstřelcích	výstřelek	k1gInPc6	výstřelek
a	a	k8xC	a
povyraženích	povyražení	k1gNnPc6	povyražení
bohémské	bohémský	k2eAgFnSc2d1	bohémská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
původně	původně	k6eAd1	původně
vycházel	vycházet	k5eAaImAgMnS	vycházet
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
-	-	kIx~	-
1928	[number]	k4	1928
v	v	k7c6	v
berlínském	berlínský	k2eAgInSc6d1	berlínský
magazínu	magazín	k1gInSc6	magazín
Sport	sport	k1gInSc1	sport
im	im	k?	im
Bild	Bild	k1gInSc1	Bild
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
podobě	podoba	k1gFnSc6	podoba
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
poprvé	poprvé	k6eAd1	poprvé
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
Ikar	Ikar	k1gInSc1	Ikar
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc1	klid
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
–	–	k?	–
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
závažnější	závažný	k2eAgFnSc4d2	závažnější
knihou	kniha	k1gFnSc7	kniha
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
zájem	zájem	k1gInSc4	zájem
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
obsah	obsah	k1gInSc1	obsah
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
její	její	k3xOp3gNnSc1	její
motto	motto	k1gNnSc1	motto
"	"	kIx"	"
<g/>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
pokusem	pokus	k1gInSc7	pokus
podat	podat	k5eAaPmF	podat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c4	o
generaci	generace	k1gFnSc4	generace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
válkou	válka	k1gFnSc7	válka
–	–	k?	–
i	i	k8xC	i
když	když	k8xS	když
unikla	uniknout	k5eAaPmAgFnS	uniknout
jejím	její	k3xOp3gInPc3	její
granátům	granát	k1gInPc3	granát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
Bäumer	Bäumer	k1gMnSc1	Bäumer
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
válečných	válečný	k2eAgInPc6d1	válečný
zážitcích	zážitek	k1gInPc6	zážitek
a	a	k8xC	a
příkořích	příkoř	k1gFnPc6	příkoř
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jim	on	k3xPp3gMnPc3	on
válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
zpátky	zpátky	k6eAd1	zpátky
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
–	–	k?	–
Kniha	kniha	k1gFnSc1	kniha
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
politický	politický	k2eAgInSc4d1	politický
<g/>
,	,	kIx,	,
mravní	mravní	k2eAgInSc4d1	mravní
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
vojenský	vojenský	k2eAgInSc4d1	vojenský
rozklad	rozklad	k1gInSc4	rozklad
Německa	Německo	k1gNnSc2	Německo
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jakési	jakýsi	k3yIgNnSc4	jakýsi
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
románu	román	k1gInSc2	román
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
kamarádi	kamarád	k1gMnPc1	kamarád
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
–	–	k?	–
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgMnPc1	tři
kamarádi	kamarád	k1gMnPc1	kamarád
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
a	a	k8xC	a
Gottfried	Gottfried	k1gMnSc1	Gottfried
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
společně	společně	k6eAd1	společně
vlastní	vlastní	k2eAgFnSc4d1	vlastní
autodílnu	autodílna	k1gFnSc4	autodílna
<g/>
.	.	kIx.	.
</s>
<s>
Autodílnu	autodílna	k1gFnSc4	autodílna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
nijak	nijak	k6eAd1	nijak
zvláště	zvláště	k6eAd1	zvláště
neprosperuje	prosperovat	k5eNaImIp3nS	prosperovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jejich	jejich	k3xOp3gNnSc4	jejich
milovaný	milovaný	k2eAgInSc4d1	milovaný
automobil	automobil	k1gInSc4	automobil
jménem	jméno	k1gNnSc7	jméno
Karel	Karla	k1gFnPc2	Karla
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
–	–	k?	–
auto	auto	k1gNnSc4	auto
s	s	k7c7	s
ošuntělou	ošuntělý	k2eAgFnSc7d1	ošuntělá
<g/>
,	,	kIx,	,
pobouchanou	pobouchaný	k2eAgFnSc7d1	pobouchaná
a	a	k8xC	a
zastaralou	zastaralý	k2eAgFnSc7d1	zastaralá
karoserií	karoserie	k1gFnSc7	karoserie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
špičkového	špičkový	k2eAgInSc2d1	špičkový
závodního	závodní	k2eAgInSc2d1	závodní
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc4	jejich
hlavní	hlavní	k2eAgFnSc4d1	hlavní
vášeň	vášeň	k1gFnSc4	vášeň
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vedle	vedle	k7c2	vedle
dobrého	dobrý	k2eAgNnSc2d1	dobré
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
pití	pití	k1gNnSc2	pití
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
dobrého	dobrý	k2eAgInSc2d1	dobrý
rumu	rum	k1gInSc2	rum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
dojde	dojít	k5eAaPmIp3nS	dojít
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
při	při	k7c6	při
projížďce	projížďka	k1gFnSc6	projížďka
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
stavují	stavovat	k5eAaImIp3nP	stavovat
v	v	k7c6	v
příměstské	příměstský	k2eAgFnSc6d1	příměstská
zahradní	zahradní	k2eAgFnSc6d1	zahradní
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
kamarádů	kamarád	k1gMnPc2	kamarád
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
krásnou	krásný	k2eAgFnSc7d1	krásná
dívkou	dívka	k1gFnSc7	dívka
jménem	jméno	k1gNnSc7	jméno
Pat	pata	k1gFnPc2	pata
<g/>
.	.	kIx.	.
</s>
<s>
Pat	pat	k1gInSc1	pat
<g/>
,	,	kIx,	,
dívce	dívka	k1gFnSc6	dívka
z	z	k7c2	z
lepší	dobrý	k2eAgFnSc2d2	lepší
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
líbí	líbit	k5eAaImIp3nS	líbit
bohémský	bohémský	k2eAgInSc1d1	bohémský
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
vedený	vedený	k2eAgInSc1d1	vedený
třemi	tři	k4xCgNnPc7	tři
kamarády	kamarád	k1gMnPc7	kamarád
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
ho	on	k3xPp3gMnSc4	on
žít	žít	k5eAaImF	žít
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Naplno	naplno	k6eAd1	naplno
propuká	propukat	k5eAaImIp3nS	propukat
láska	láska	k1gFnSc1	láska
mezi	mezi	k7c4	mezi
Pat	pat	k1gInSc4	pat
a	a	k8xC	a
Robertem	Robert	k1gMnSc7	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
první	první	k4xOgFnSc6	první
společné	společný	k2eAgFnSc6d1	společná
dovolené	dovolená	k1gFnSc6	dovolená
Robert	Robert	k1gMnSc1	Robert
s	s	k7c7	s
hrůzou	hrůza	k1gFnSc7	hrůza
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
milovaná	milovaný	k2eAgFnSc1d1	milovaná
Pat	pat	k1gInSc4	pat
je	být	k5eAaImIp3nS	být
těžce	těžce	k6eAd1	těžce
nemocná	mocný	k2eNgFnSc1d1	mocný
a	a	k8xC	a
už	už	k6eAd1	už
roky	rok	k1gInPc4	rok
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
kamarádů	kamarád	k1gMnPc2	kamarád
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
snaží	snažit	k5eAaImIp3nS	snažit
všemožně	všemožně	k6eAd1	všemožně
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poradě	porada	k1gFnSc6	porada
s	s	k7c7	s
lékařem	lékař	k1gMnSc7	lékař
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
jediným	jediný	k2eAgNnSc7d1	jediné
řešením	řešení	k1gNnSc7	řešení
léčení	léčení	k1gNnSc2	léčení
v	v	k7c6	v
nákladném	nákladný	k2eAgNnSc6d1	nákladné
horském	horský	k2eAgNnSc6d1	horské
sanatoriu	sanatorium	k1gNnSc6	sanatorium
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Pat	pat	k1gInSc1	pat
také	také	k9	také
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Gottfried	Gottfried	k1gMnSc1	Gottfried
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
romantik	romantik	k1gMnSc1	romantik
<g/>
,	,	kIx,	,
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
do	do	k7c2	do
protifašistického	protifašistický	k2eAgNnSc2d1	protifašistické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
střetu	střet	k1gInSc6	střet
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
SA	SA	kA	SA
je	být	k5eAaImIp3nS	být
Gottfried	Gottfried	k1gInSc1	Gottfried
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgMnPc1d1	zbývající
dva	dva	k4xCgMnPc1	dva
kamarádi	kamarád	k1gMnPc1	kamarád
začínají	začínat	k5eAaImIp3nP	začínat
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
vrahovi	vrah	k1gMnSc6	vrah
a	a	k8xC	a
hodlají	hodlat	k5eAaImIp3nP	hodlat
se	se	k3xPyFc4	se
pomstít	pomstít	k5eAaPmF	pomstít
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
za	za	k7c4	za
pomoci	pomoct	k5eAaPmF	pomoct
přítele	přítel	k1gMnSc2	přítel
Alfonse	Alfons	k1gMnSc2	Alfons
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
již	již	k9	již
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
především	především	k9	především
peníze	peníz	k1gInPc1	peníz
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
vyléčení	vyléčení	k1gNnSc3	vyléčení
Pat	pata	k1gFnPc2	pata
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
rozprodávají	rozprodávat	k5eAaImIp3nP	rozprodávat
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Pat	pat	k1gInSc4	pat
mohla	moct	k5eAaImAgFnS	moct
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
v	v	k7c6	v
sanatoriu	sanatorium	k1gNnSc6	sanatorium
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poslední	poslední	k2eAgFnSc4d1	poslední
věc	věc	k1gFnSc4	věc
prodají	prodat	k5eAaPmIp3nP	prodat
i	i	k9	i
milovaného	milovaný	k2eAgMnSc2d1	milovaný
Karla	Karel	k1gMnSc2	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
veškerou	veškerý	k3xTgFnSc4	veškerý
snahu	snaha	k1gFnSc4	snaha
Pat	pat	k1gInSc1	pat
nakonec	nakonec	k6eAd1	nakonec
nemoci	nemoc	k1gFnSc2	nemoc
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Miluj	milovat	k5eAaImRp2nS	milovat
bližního	bližní	k1gMnSc2	bližní
svého	své	k1gNnSc2	své
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
–	–	k?	–
Příběh	příběh	k1gInSc1	příběh
mladého	mladý	k1gMnSc2	mladý
hocha	hoch	k1gMnSc2	hoch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
utíká	utíkat	k5eAaImIp3nS	utíkat
před	před	k7c7	před
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Přijde	přijít	k5eAaPmIp3nS	přijít
o	o	k7c4	o
rodiče	rodič	k1gMnPc4	rodič
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
jako	jako	k9	jako
psanec	psanec	k1gMnSc1	psanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
utéci	utéct	k5eAaPmF	utéct
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Vítězný	vítězný	k2eAgInSc1d1	vítězný
oblouk	oblouk	k1gInSc1	oblouk
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
–	–	k?	–
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
nacistickou	nacistický	k2eAgFnSc7d1	nacistická
okupací	okupace	k1gFnSc7	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgMnSc1d1	německý
lékař	lékař	k1gMnSc1	lékař
–	–	k?	–
chirurg	chirurg	k1gMnSc1	chirurg
Ravic	Ravic	k1gMnSc1	Ravic
<g/>
.	.	kIx.	.
</s>
<s>
Ravic	Ravic	k1gMnSc1	Ravic
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
před	před	k7c7	před
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
bez	bez	k7c2	bez
dokladů	doklad	k1gInPc2	doklad
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
ilegálně	ilegálně	k6eAd1	ilegálně
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
klinik	klinika	k1gFnPc2	klinika
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc6	jeden
noci	noc	k1gFnSc6	noc
potkává	potkávat	k5eAaImIp3nS	potkávat
dívku	dívka	k1gFnSc4	dívka
Joan	Joana	k1gFnPc2	Joana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
umírá	umírat	k5eAaImIp3nS	umírat
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Pomůže	pomoct	k5eAaPmIp3nS	pomoct
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
rozporuplný	rozporuplný	k2eAgInSc1d1	rozporuplný
<g/>
.	.	kIx.	.
</s>
<s>
Ravic	Ravic	k1gMnSc1	Ravic
pak	pak	k6eAd1	pak
náhodně	náhodně	k6eAd1	náhodně
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
potkává	potkávat	k5eAaImIp3nS	potkávat
Haakeho	Haake	k1gMnSc4	Haake
<g/>
,	,	kIx,	,
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
lidi	člověk	k1gMnPc4	člověk
vyslýchal	vyslýchat	k5eAaImAgInS	vyslýchat
<g/>
,	,	kIx,	,
týral	týrat	k5eAaImAgInS	týrat
a	a	k8xC	a
mučil	mučit	k5eAaImAgInS	mučit
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
přelud	přelud	k1gInSc4	přelud
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
zmocňuje	zmocňovat	k5eAaImIp3nS	zmocňovat
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
pomstě	pomsta	k1gFnSc6	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nP	podařit
Ravikovi	Ravikův	k2eAgMnPc1d1	Ravikův
Haakeho	Haake	k1gMnSc4	Haake
zabít	zabít	k5eAaPmF	zabít
a	a	k8xC	a
značně	značně	k6eAd1	značně
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
uleví	ulevit	k5eAaPmIp3nS	ulevit
a	a	k8xC	a
smíří	smířit	k5eAaPmIp3nS	smířit
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
se	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
Joan	Joana	k1gFnPc2	Joana
a	a	k8xC	a
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
mobilizace	mobilizace	k1gFnSc2	mobilizace
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jiskra	jiskra	k1gFnSc1	jiskra
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
–	–	k?	–
Kniha	kniha	k1gFnSc1	kniha
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
hrůzách	hrůza	k1gFnPc6	hrůza
života	život	k1gInSc2	život
v	v	k7c6	v
nacistickém	nacistický	k2eAgInSc6d1	nacistický
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Čas	čas	k1gInSc4	čas
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
čas	čas	k1gInSc4	čas
umírat	umírat	k5eAaImF	umírat
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
–	–	k?	–
Kniha	kniha	k1gFnSc1	kniha
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
německém	německý	k2eAgMnSc6d1	německý
vojákovi	voják	k1gMnSc6	voják
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vrací	vracet	k5eAaImIp3nS	vracet
domů	domů	k6eAd1	domů
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
fronty	fronta	k1gFnSc2	fronta
do	do	k7c2	do
zničeného	zničený	k2eAgNnSc2d1	zničené
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
obelisk	obelisk	k1gInSc1	obelisk
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
–	–	k?	–
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Německo	Německo	k1gNnSc4	Německo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
období	období	k1gNnSc4	období
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kurs	kurs	k1gInSc1	kurs
marky	marka	k1gFnSc2	marka
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
dvakrát	dvakrát	k6eAd1	dvakrát
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
jsou	být	k5eAaImIp3nP	být
kamarádi	kamarád	k1gMnPc1	kamarád
z	z	k7c2	z
I.	I.	kA	I.
<g/>
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
vlastní	vlastní	k2eAgInSc4d1	vlastní
pohřební	pohřební	k2eAgInSc4d1	pohřební
ústav	ústav	k1gInSc4	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
jejich	jejich	k3xOp3gFnPc4	jejich
radosti	radost	k1gFnPc4	radost
<g/>
,	,	kIx,	,
lásky	láska	k1gFnPc4	láska
a	a	k8xC	a
starosti	starost	k1gFnPc4	starost
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
osudu	osud	k1gInSc6	osud
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
politické	politický	k2eAgFnPc4d1	politická
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
těžkosti	těžkost	k1gFnPc4	těžkost
výmarského	výmarský	k2eAgNnSc2d1	výmarské
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
důvody	důvod	k1gInPc4	důvod
pro	pro	k7c4	pro
nástup	nástup	k1gInSc4	nástup
nacismu	nacismus	k1gInSc2	nacismus
(	(	kIx(	(
<g/>
nacionálního	nacionální	k2eAgInSc2d1	nacionální
socialismu	socialismus	k1gInSc2	socialismus
<g/>
)	)	kIx)	)
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nebe	nebe	k1gNnSc1	nebe
nezná	neznat	k5eAaImIp3nS	neznat
vyvolených	vyvolená	k1gFnPc2	vyvolená
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
–	–	k?	–
Milostný	milostný	k2eAgInSc4d1	milostný
román	román	k1gInSc4	román
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
automobilových	automobilový	k2eAgInPc2d1	automobilový
závodů	závod	k1gInPc2	závod
Noc	noc	k1gFnSc4	noc
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
–	–	k?	–
Příběh	příběh	k1gInSc1	příběh
Josefa	Josef	k1gMnSc2	Josef
Baumanna	Baumann	k1gMnSc2	Baumann
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Heleny	Helena	k1gFnSc2	Helena
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
prchají	prchat	k5eAaImIp3nP	prchat
z	z	k7c2	z
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Zaslíbená	zaslíbený	k2eAgFnSc1d1	zaslíbená
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
–	–	k?	–
Kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
německých	německý	k2eAgMnPc2d1	německý
uprchlíků	uprchlík	k1gMnPc2	uprchlík
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
židovský	židovský	k2eAgMnSc1d1	židovský
emigrant	emigrant	k1gMnSc1	emigrant
vypořádává	vypořádávat	k5eAaImIp3nS	vypořádávat
s	s	k7c7	s
krutou	krutý	k2eAgFnSc7d1	krutá
minulostí	minulost	k1gFnSc7	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Stíny	stín	k1gInPc1	stín
v	v	k7c6	v
ráji	ráj	k1gInSc6	ráj
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
–	–	k?	–
Vyprávění	vyprávění	k1gNnSc2	vyprávění
o	o	k7c6	o
německém	německý	k2eAgMnSc6d1	německý
emigrantovi-novináři	emigrantoviovinář	k1gMnSc6	emigrantovi-novinář
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
osud	osud	k1gInSc1	osud
zavál	zavát	k5eAaPmAgInS	zavát
až	až	k9	až
do	do	k7c2	do
dalekého	daleký	k2eAgMnSc2d1	daleký
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
stanice	stanice	k1gFnSc1	stanice
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
–	–	k?	–
O	o	k7c6	o
bombardování	bombardování	k1gNnSc6	bombardování
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítel	nepřítel	k1gMnSc1	nepřítel
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Feind	Feind	k1gInSc1	Feind
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
6	[number]	k4	6
povídek	povídka	k1gFnPc2	povídka
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
