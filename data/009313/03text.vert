<p>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
a	a	k8xC	a
přímá	přímý	k2eAgFnSc1d1	přímá
demokracie	demokracie	k1gFnSc1	demokracie
–	–	k?	–
Tomio	Tomio	k1gNnSc1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
(	(	kIx(	(
<g/>
SPD	SPD	kA	SPD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SPD	SPD	kA	SPD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc1d1	české
pravicově	pravicově	k6eAd1	pravicově
populistické	populistický	k2eAgNnSc1d1	populistické
politické	politický	k2eAgNnSc1d1	politické
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
poslanci	poslanec	k1gMnPc1	poslanec
Tomiem	Tomium	k1gNnSc7	Tomium
Okamurou	Okamura	k1gFnSc7	Okamura
a	a	k8xC	a
Radimem	Radim	k1gMnSc7	Radim
Fialou	Fiala	k1gMnSc7	Fiala
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
po	po	k7c6	po
roztržce	roztržka	k1gFnSc6	roztržka
v	v	k7c6	v
poslaneckém	poslanecký	k2eAgInSc6d1	poslanecký
klubu	klub	k1gInSc6	klub
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
Úsvit	úsvit	k1gInSc1	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
samo	sám	k3xTgNnSc1	sám
sebe	sebe	k3xPyFc4	sebe
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
vlastenecké	vlastenecký	k2eAgNnSc4d1	vlastenecké
<g/>
,	,	kIx,	,
v	v	k7c6	v
programu	program	k1gInSc6	program
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
přímou	přímý	k2eAgFnSc4d1	přímá
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc4	omezení
nelegální	legální	k2eNgFnSc2d1	nelegální
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
,	,	kIx,	,
odvolatelnost	odvolatelnost	k1gFnSc1	odvolatelnost
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
obecné	obecný	k2eAgFnSc2d1	obecná
referendu	referendum	k1gNnSc6	referendum
včetně	včetně	k7c2	včetně
referend	referendum	k1gNnPc2	referendum
o	o	k7c6	o
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
otázkách	otázka	k1gFnPc6	otázka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vystoupení	vystoupení	k1gNnSc1	vystoupení
z	z	k7c2	z
EU	EU	kA	EU
nebo	nebo	k8xC	nebo
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
SPD	SPD	kA	SPD
podporu	podpora	k1gFnSc4	podpora
například	například	k6eAd1	například
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
někdejší	někdejší	k2eAgFnSc2d1	někdejší
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
Marine	Marin	k1gInSc5	Marin
Le	Le	k1gMnSc4	Le
Penová	Penový	k2eAgFnSc1d1	Penový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravo-levém	pravoevý	k2eAgNnSc6d1	pravo-levé
politickém	politický	k2eAgNnSc6d1	politické
spektru	spektrum	k1gNnSc6	spektrum
je	být	k5eAaImIp3nS	být
řazeno	řadit	k5eAaImNgNnS	řadit
ke	k	k7c3	k
krajní	krajní	k2eAgFnSc3d1	krajní
pravici	pravice	k1gFnSc3	pravice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
Svoboda	Svoboda	k1gMnSc1	Svoboda
a	a	k8xC	a
přímá	přímý	k2eAgFnSc1d1	přímá
demokracie	demokracie	k1gFnSc1	demokracie
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
několikaměsíční	několikaměsíční	k2eAgFnSc2d1	několikaměsíční
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
působišti	působiště	k1gNnSc6	působiště
obou	dva	k4xCgMnPc2	dva
zakladatelů	zakladatel	k1gMnPc2	zakladatel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
vedení	vedení	k1gNnSc4	vedení
převzal	převzít	k5eAaPmAgMnS	převzít
poslanec	poslanec	k1gMnSc1	poslanec
Marek	Marek	k1gMnSc1	Marek
Černoch	Černoch	k1gMnSc1	Černoch
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
znesvářené	znesvářený	k2eAgFnPc1d1	znesvářená
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
vinily	vinit	k5eAaImAgFnP	vinit
z	z	k7c2	z
údajného	údajný	k2eAgNnSc2d1	údajné
vyvádění	vyvádění	k1gNnSc2	vyvádění
peněz	peníze	k1gInPc2	peníze
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
Tomio	Tomio	k6eAd1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
své	svůj	k3xOyFgMnPc4	svůj
oponenty	oponent	k1gMnPc4	oponent
označoval	označovat	k5eAaImAgMnS	označovat
za	za	k7c4	za
pučisty	pučista	k1gMnPc4	pučista
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Úsvitu	úsvit	k1gInSc2	úsvit
se	se	k3xPyFc4	se
však	však	k9	však
postavila	postavit	k5eAaPmAgFnS	postavit
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Okamura	Okamura	k1gFnSc1	Okamura
s	s	k7c7	s
Radimem	Radim	k1gMnSc7	Radim
Fialou	Fiala	k1gMnSc7	Fiala
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
zbytek	zbytek	k1gInSc1	zbytek
hnutí	hnutí	k1gNnSc2	hnutí
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
vlastní	vlastní	k2eAgNnPc4d1	vlastní
hnutí	hnutí	k1gNnPc4	hnutí
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
Svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
u	u	k7c2	u
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přípravném	přípravný	k2eAgInSc6d1	přípravný
výboru	výbor	k1gInSc6	výbor
kromě	kromě	k7c2	kromě
této	tento	k3xDgFnSc2	tento
dvojice	dvojice	k1gFnSc2	dvojice
figuroval	figurovat	k5eAaImAgMnS	figurovat
ještě	ještě	k9	ještě
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Staník	Staník	k1gMnSc1	Staník
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
hnutí	hnutí	k1gNnSc1	hnutí
získalo	získat	k5eAaPmAgNnS	získat
registraci	registrace	k1gFnSc4	registrace
a	a	k8xC	a
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
nábor	nábor	k1gInSc4	nábor
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Tomio	Tomio	k6eAd1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
tisíce	tisíc	k4xCgInPc4	tisíc
zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Program	program	k1gInSc1	program
==	==	k?	==
</s>
</p>
<p>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
programově	programově	k6eAd1	programově
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Úsvit	úsvit	k1gInSc4	úsvit
<g/>
.	.	kIx.	.
</s>
<s>
Poslanec	poslanec	k1gMnSc1	poslanec
Fiala	Fiala	k1gMnSc1	Fiala
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
hnutí	hnutí	k1gNnSc2	hnutí
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
SPD	SPD	kA	SPD
pravicové	pravicový	k2eAgNnSc1d1	pravicové
hnutí	hnutí	k1gNnSc1	hnutí
s	s	k7c7	s
jasnými	jasný	k2eAgInPc7d1	jasný
politickými	politický	k2eAgInPc7d1	politický
cíli	cíl	k1gInPc7	cíl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
zásadní	zásadní	k2eAgFnPc4d1	zásadní
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
systému	systém	k1gInSc6	systém
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc7	jeho
základním	základní	k2eAgInSc7d1	základní
programem	program	k1gInSc7	program
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
prosazení	prosazení	k1gNnSc3	prosazení
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
politiků	politik	k1gMnPc2	politik
do	do	k7c2	do
politického	politický	k2eAgInSc2d1	politický
systému	systém	k1gInSc2	systém
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
důsledná	důsledný	k2eAgFnSc1d1	důsledná
obhajoba	obhajoba	k1gFnSc1	obhajoba
národních	národní	k2eAgInPc2d1	národní
zájmů	zájem	k1gInPc2	zájem
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
chtělo	chtít	k5eAaImAgNnS	chtít
také	také	k9	také
"	"	kIx"	"
<g/>
důsledně	důsledně	k6eAd1	důsledně
vystupovat	vystupovat	k5eAaImF	vystupovat
proti	proti	k7c3	proti
zvyšování	zvyšování	k1gNnSc3	zvyšování
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
hájit	hájit	k5eAaImF	hájit
zájmy	zájem	k1gInPc4	zájem
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
poctivě	poctivě	k6eAd1	poctivě
pracují	pracovat	k5eAaImIp3nP	pracovat
nebo	nebo	k8xC	nebo
podnikají	podnikat	k5eAaImIp3nP	podnikat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
usiluje	usilovat	k5eAaImIp3nS	usilovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
obecného	obecný	k2eAgNnSc2d1	obecné
referenda	referendum	k1gNnSc2	referendum
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
o	o	k7c6	o
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
otázkách	otázka	k1gFnPc6	otázka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
vystoupení	vystoupení	k1gNnSc4	vystoupení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
se	se	k3xPyFc4	se
projednávala	projednávat	k5eAaImAgFnS	projednávat
petice	petice	k1gFnSc1	petice
za	za	k7c4	za
referendum	referendum	k1gNnSc4	referendum
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc4	návrh
však	však	k9	však
byl	být	k5eAaImAgMnS	být
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
zamítnut	zamítnout	k5eAaPmNgMnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
SPD	SPD	kA	SPD
je	být	k5eAaImIp3nS	být
také	také	k9	také
zakázat	zakázat	k5eAaPmF	zakázat
propagaci	propagace	k1gFnSc4	propagace
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
posílit	posílit	k5eAaPmF	posílit
cizineckou	cizinecký	k2eAgFnSc4d1	cizinecká
policii	policie	k1gFnSc4	policie
nebo	nebo	k8xC	nebo
zpřísnit	zpřísnit	k5eAaPmF	zpřísnit
imigrační	imigrační	k2eAgInPc4d1	imigrační
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
zavedly	zavést	k5eAaPmAgInP	zavést
nulovou	nulový	k2eAgFnSc4d1	nulová
toleranci	tolerance	k1gFnSc4	tolerance
vůči	vůči	k7c3	vůči
ilegálním	ilegální	k2eAgMnPc3d1	ilegální
imigrantům	imigrant	k1gMnPc3	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přistěhovalecké	přistěhovalecký	k2eAgFnSc6d1	přistěhovalecká
problematice	problematika	k1gFnSc6	problematika
má	mít	k5eAaImIp3nS	mít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Národní	národní	k2eAgFnSc7d1	národní
demokracií	demokracie	k1gFnSc7	demokracie
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
nejtvrdší	tvrdý	k2eAgFnSc4d3	nejtvrdší
rétoriku	rétorika	k1gFnSc4	rétorika
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
sociologa	sociolog	k1gMnSc2	sociolog
Daniela	Daniel	k1gMnSc2	Daniel
Prokopa	Prokop	k1gMnSc2	Prokop
z	z	k7c2	z
agentury	agentura	k1gFnSc2	agentura
Median	Mediana	k1gFnPc2	Mediana
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
hnutí	hnutí	k1gNnSc1	hnutí
zařadit	zařadit	k5eAaPmF	zařadit
v	v	k7c6	v
politickém	politický	k2eAgNnSc6d1	politické
spektru	spektrum	k1gNnSc6	spektrum
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
doleva	doleva	k6eAd1	doleva
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
"	"	kIx"	"
<g/>
oslovováním	oslovování	k1gNnSc7	oslovování
ublížené	ublížený	k2eAgFnSc2d1	ublížená
části	část	k1gFnSc2	část
populace	populace	k1gFnSc2	populace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
napravo	napravo	k6eAd1	napravo
"	"	kIx"	"
<g/>
rétorikou	rétorika	k1gFnSc7	rétorika
proti	proti	k7c3	proti
takzvaným	takzvaný	k2eAgMnPc3d1	takzvaný
nepřizpůsobivým	přizpůsobivý	k2eNgMnPc3d1	nepřizpůsobivý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Politolog	politolog	k1gMnSc1	politolog
Kamil	Kamil	k1gMnSc1	Kamil
Švec	Švec	k1gMnSc1	Švec
z	z	k7c2	z
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
ho	on	k3xPp3gMnSc4	on
řadil	řadit	k5eAaImAgInS	řadit
k	k	k7c3	k
extrémní	extrémní	k2eAgFnSc3d1	extrémní
pravici	pravice	k1gFnSc3	pravice
po	po	k7c4	po
bok	bok	k1gInSc4	bok
DSSS	DSSS	kA	DSSS
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
hnutí	hnutí	k1gNnSc2	hnutí
cílí	cílit	k5eAaImIp3nS	cílit
na	na	k7c4	na
"	"	kIx"	"
<g/>
voliče	volič	k1gMnSc4	volič
extrémně	extrémně	k6eAd1	extrémně
nespokojené	spokojený	k2eNgInPc1d1	nespokojený
<g/>
,	,	kIx,	,
názorově	názorově	k6eAd1	názorově
asi	asi	k9	asi
ne	ne	k9	ne
úplně	úplně	k6eAd1	úplně
konsenzuální	konsenzuální	k2eAgNnSc1d1	konsenzuální
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Politolog	politolog	k1gMnSc1	politolog
Pavel	Pavel	k1gMnSc1	Pavel
Šaradín	Šaradín	k1gMnSc1	Šaradín
z	z	k7c2	z
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
hnutí	hnutí	k1gNnSc2	hnutí
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
radikálnější	radikální	k2eAgMnPc4d2	radikálnější
protestní	protestní	k2eAgMnPc4d1	protestní
voliče	volič	k1gMnPc4	volič
<g/>
,	,	kIx,	,
o	o	k7c4	o
lidi	člověk	k1gMnPc4	člověk
nespokojené	spokojený	k2eNgMnPc4d1	nespokojený
se	se	k3xPyFc4	se
společenským	společenský	k2eAgInSc7d1	společenský
pořádkem	pořádek	k1gInSc7	pořádek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
"	"	kIx"	"
<g/>
politiky	politika	k1gFnSc2	politika
silných	silný	k2eAgNnPc2d1	silné
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
rychlých	rychlý	k2eAgNnPc2d1	rychlé
řešení	řešení	k1gNnPc2	řešení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
komentátoři	komentátor	k1gMnPc1	komentátor
označují	označovat	k5eAaImIp3nP	označovat
hnutí	hnutí	k1gNnSc4	hnutí
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
program	program	k1gInSc1	program
za	za	k7c4	za
nacionalistické	nacionalistický	k2eAgNnSc4d1	nacionalistické
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
2015	[number]	k4	2015
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2015	[number]	k4	2015
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
hnutí	hnutí	k1gNnSc2	hnutí
stal	stát	k5eAaPmAgMnS	stát
poslanec	poslanec	k1gMnSc1	poslanec
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Holík	Holík	k1gMnSc1	Holík
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zvolený	zvolený	k2eAgMnSc1d1	zvolený
jako	jako	k8xC	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
Úsvit	úsvit	k1gInSc4	úsvit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
počet	počet	k1gInSc1	počet
poslanců	poslanec	k1gMnPc2	poslanec
SPD	SPD	kA	SPD
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2015	[number]	k4	2015
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
první	první	k4xOgFnSc1	první
volební	volební	k2eAgFnSc1d1	volební
konference	konference	k1gFnSc1	konference
SPD	SPD	kA	SPD
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
hlasování	hlasování	k1gNnSc2	hlasování
členů	člen	k1gMnPc2	člen
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
hnutí	hnutí	k1gNnSc2	hnutí
Tomio	Tomio	k1gMnSc1	Tomio
Okamura	Okamur	k1gMnSc2	Okamur
a	a	k8xC	a
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Radim	Radim	k1gMnSc1	Radim
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
poslanec	poslanec	k1gMnSc1	poslanec
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Holík	Holík	k1gMnSc1	Holík
a	a	k8xC	a
místostarosta	místostarosta	k1gMnSc1	místostarosta
obce	obec	k1gFnSc2	obec
Všechlapy	Všechlapa	k1gFnSc2	Všechlapa
Radek	Radek	k1gMnSc1	Radek
Rozvoral	Rozvoral	k1gMnSc1	Rozvoral
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
září	zářit	k5eAaImIp3nS	zářit
2015	[number]	k4	2015
při	při	k7c6	při
mimořádném	mimořádný	k2eAgNnSc6d1	mimořádné
projednávání	projednávání	k1gNnSc6	projednávání
evropské	evropský	k2eAgFnSc2d1	Evropská
migrační	migrační	k2eAgFnSc2d1	migrační
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
avizovali	avizovat	k5eAaBmAgMnP	avizovat
zástupci	zástupce	k1gMnPc1	zástupce
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
požadovat	požadovat	k5eAaImF	požadovat
ukončení	ukončení	k1gNnSc4	ukončení
členství	členství	k1gNnSc2	členství
ČR	ČR	kA	ČR
v	v	k7c6	v
schengenském	schengenský	k2eAgInSc6d1	schengenský
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
zahájení	zahájení	k1gNnSc4	zahájení
referenda	referendum	k1gNnSc2	referendum
o	o	k7c6	o
odchodu	odchod	k1gInSc6	odchod
země	zem	k1gFnSc2	zem
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
i	i	k8xC	i
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
předseda	předseda	k1gMnSc1	předseda
SPD	SPD	kA	SPD
Tomio	Tomio	k6eAd1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
záměr	záměr	k1gInSc1	záměr
vyvolat	vyvolat	k5eAaPmF	vyvolat
hlasování	hlasování	k1gNnSc4	hlasování
o	o	k7c6	o
nedůvěře	nedůvěra	k1gFnSc6	nedůvěra
vládě	vláda	k1gFnSc6	vláda
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
akceptovala	akceptovat	k5eAaBmAgFnS	akceptovat
hlasování	hlasování	k1gNnSc4	hlasování
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
uprchlíků	uprchlík	k1gMnPc2	uprchlík
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
nebránit	bránit	k5eNaImF	bránit
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
žalobou	žaloba	k1gFnSc7	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tří	tři	k4xCgInPc2	tři
podpisů	podpis	k1gInPc2	podpis
vlastních	vlastní	k2eAgMnPc2d1	vlastní
poslanců	poslanec	k1gMnPc2	poslanec
však	však	k9	však
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
<g/>
Představitelé	představitel	k1gMnPc1	představitel
hnutí	hnutí	k1gNnSc4	hnutí
pořádali	pořádat	k5eAaImAgMnP	pořádat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
různých	různý	k2eAgFnPc2d1	různá
protiislámských	protiislámský	k2eAgFnPc2d1	protiislámská
a	a	k8xC	a
protiimigračních	protiimigrační	k2eAgFnPc2d1	protiimigrační
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
s	s	k7c7	s
následným	následný	k2eAgInSc7d1	následný
pochodem	pochod	k1gInSc7	pochod
k	k	k7c3	k
Úřadu	úřad	k1gInSc3	úřad
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
i	i	k9	i
17	[number]	k4	17
<g/>
<g />
.	.	kIx.	.
</s>
<s>
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
s	s	k7c7	s
mottem	motto	k1gNnSc7	motto
"	"	kIx"	"
<g/>
Za	za	k7c4	za
naši	náš	k3xOp1gFnSc4	náš
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
zem	zem	k1gFnSc4	zem
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
nebo	nebo	k8xC	nebo
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
opět	opět	k6eAd1	opět
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gMnPc4	on
podpořil	podpořit	k5eAaPmAgMnS	podpořit
senátor	senátor	k1gMnSc1	senátor
SPO	SPO	kA	SPO
Jan	Jan	k1gMnSc1	Jan
Veleba	Veleba	k1gMnSc1	Veleba
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
Aleš	Aleš	k1gMnSc1	Aleš
Brichta	Brichta	k1gMnSc1	Brichta
či	či	k8xC	či
herec	herec	k1gMnSc1	herec
Ivan	Ivan	k1gMnSc1	Ivan
Vyskočil	Vyskočil	k1gMnSc1	Vyskočil
<g/>
.	.	kIx.	.
<g/>
Hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
dodalo	dodat	k5eAaPmAgNnS	dodat
kontrolnímu	kontrolní	k2eAgInSc3d1	kontrolní
výboru	výbor	k1gInSc3	výbor
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
neúplnou	úplný	k2eNgFnSc4d1	neúplná
výroční	výroční	k2eAgFnSc4d1	výroční
finanční	finanční	k2eAgFnSc4d1	finanční
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
požádal	požádat	k5eAaPmAgMnS	požádat
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
správnímu	správní	k2eAgInSc3d1	správní
soudu	soud	k1gInSc3	soud
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
pozastavit	pozastavit	k5eAaPmF	pozastavit
činnost	činnost	k1gFnSc4	činnost
SPD	SPD	kA	SPD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2016	[number]	k4	2016
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
SPD	SPD	kA	SPD
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
koalici	koalice	k1gFnSc4	koalice
pro	pro	k7c4	pro
krajské	krajský	k2eAgFnPc4d1	krajská
volby	volba	k1gFnPc4	volba
se	se	k3xPyFc4	se
Stranou	strana	k1gFnSc7	strana
práv	právo	k1gNnPc2	právo
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
SPD	SPD	kA	SPD
získala	získat	k5eAaPmAgFnS	získat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
celkem	celkem	k6eAd1	celkem
18	[number]	k4	18
mandátů	mandát	k1gInPc2	mandát
v	v	k7c6	v
zastupitelstvech	zastupitelstvo	k1gNnPc6	zastupitelstvo
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
na	na	k7c6	na
samostatné	samostatný	k2eAgFnSc6d1	samostatná
kandidátce	kandidátka	k1gFnSc6	kandidátka
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
koalice	koalice	k1gFnSc1	koalice
SPD-SPO	SPD-SPO	k1gMnPc2	SPD-SPO
bude	být	k5eAaImBp3nS	být
vládnout	vládnout	k5eAaImF	vládnout
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
Celostátní	celostátní	k2eAgFnSc6d1	celostátní
konferenci	konference	k1gFnSc6	konference
přijalo	přijmout	k5eAaPmAgNnS	přijmout
hnutí	hnutí	k1gNnSc1	hnutí
usnesení	usnesení	k1gNnSc2	usnesení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
mj.	mj.	kA	mj.
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
následujících	následující	k2eAgFnPc2d1	následující
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
PS	PS	kA	PS
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
půjde	jít	k5eAaImIp3nS	jít
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2017	[number]	k4	2017
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
jako	jako	k9	jako
jediní	jediný	k2eAgMnPc1d1	jediný
političtí	politický	k2eAgMnPc1d1	politický
zástupci	zástupce	k1gMnPc1	zástupce
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
členové	člen	k1gMnPc1	člen
SPD	SPD	kA	SPD
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
konference	konference	k1gFnSc2	konference
frakce	frakce	k1gFnSc2	frakce
ENF	ENF	kA	ENF
konané	konaný	k2eAgFnSc2d1	konaná
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
městě	město	k1gNnSc6	město
Koblenz	Koblenza	k1gFnPc2	Koblenza
<g/>
.	.	kIx.	.
</s>
<s>
SPD	SPD	kA	SPD
se	se	k3xPyFc4	se
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
účastní	účastnit	k5eAaImIp3nS	účastnit
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
i	i	k9	i
následujících	následující	k2eAgFnPc2d1	následující
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
konferencí	konference	k1gFnPc2	konference
ENF	ENF	kA	ENF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
hnutí	hnutí	k1gNnSc1	hnutí
kandidovalo	kandidovat	k5eAaImAgNnS	kandidovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
krajích	kraj	k1gInPc6	kraj
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
s	s	k7c7	s
10,64	[number]	k4	10,64
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
22	[number]	k4	22
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
poslanci	poslanec	k1gMnPc7	poslanec
zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
všechny	všechen	k3xTgInPc1	všechen
kraje	kraj	k1gInPc1	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
SPD	SPD	kA	SPD
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pořádalo	pořádat	k5eAaImAgNnS	pořádat
konferenci	konference	k1gFnSc4	konference
pro	pro	k7c4	pro
evropskou	evropský	k2eAgFnSc4d1	Evropská
stranu	strana	k1gFnSc4	strana
Hnutí	hnutí	k1gNnSc2	hnutí
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
národů	národ	k1gInPc2	národ
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
SPD	SPD	kA	SPD
členem	člen	k1gInSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
dorazili	dorazit	k5eAaPmAgMnP	dorazit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
francouzská	francouzský	k2eAgFnSc1d1	francouzská
Národní	národní	k2eAgFnSc1d1	národní
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
Strana	strana	k1gFnSc1	strana
pro	pro	k7c4	pro
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
strana	strana	k1gFnSc1	strana
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
italská	italský	k2eAgFnSc1d1	italská
Liga	liga	k1gFnSc1	liga
Severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2018	[number]	k4	2018
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
konala	konat	k5eAaImAgFnS	konat
5	[number]	k4	5
<g/>
.	.	kIx.	.
celostátní	celostátní	k2eAgFnSc2d1	celostátní
konference	konference	k1gFnSc2	konference
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
volilo	volit	k5eAaImAgNnS	volit
pětičlenné	pětičlenný	k2eAgNnSc1d1	pětičlenné
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
hnutí	hnutí	k1gNnSc2	hnutí
pro	pro	k7c4	pro
roky	rok	k1gInPc4	rok
2018	[number]	k4	2018
až	až	k8xS	až
2021	[number]	k4	2021
<g/>
.	.	kIx.	.
</s>
<s>
Post	post	k1gInSc1	post
předsedy	předseda	k1gMnSc2	předseda
hnutí	hnutí	k1gNnSc2	hnutí
obhájil	obhájit	k5eAaPmAgMnS	obhájit
Tomio	Tomio	k1gMnSc1	Tomio
Okamura	Okamur	k1gMnSc2	Okamur
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
150	[number]	k4	150
ze	z	k7c2	z
152	[number]	k4	152
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místopředsedou	místopředseda	k1gMnSc7	místopředseda
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Radim	Radim	k1gMnSc1	Radim
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
149	[number]	k4	149
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
řadových	řadový	k2eAgMnPc2d1	řadový
členů	člen	k1gMnPc2	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
hnutí	hnutí	k1gNnSc2	hnutí
obhájili	obhájit	k5eAaPmAgMnP	obhájit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Holík	Holík	k1gMnSc1	Holík
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
140	[number]	k4	140
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
a	a	k8xC	a
Radek	Radek	k1gMnSc1	Radek
Rozvoral	Rozvoral	k1gMnSc1	Rozvoral
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
137	[number]	k4	137
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
stal	stát	k5eAaPmAgMnS	stát
Radovan	Radovan	k1gMnSc1	Radovan
Vích	Vích	k1gMnSc1	Vích
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
138	[number]	k4	138
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tak	tak	k6eAd1	tak
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Miloslava	Miloslav	k1gMnSc4	Miloslav
Roznera	Rozner	k1gMnSc4	Rozner
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
už	už	k9	už
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
nekandidoval	kandidovat	k5eNaImAgInS	kandidovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
podal	podat	k5eAaPmAgMnS	podat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
brněnský	brněnský	k2eAgMnSc1d1	brněnský
člen	člen	k1gMnSc1	člen
hnutí	hnutí	k1gNnSc2	hnutí
Kamil	Kamil	k1gMnSc1	Kamil
Papežík	Papežík	k1gMnSc1	Papežík
návrh	návrh	k1gInSc4	návrh
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
správnímu	správní	k2eAgInSc3d1	správní
soudu	soud	k1gInSc3	soud
na	na	k7c4	na
pozastavení	pozastavení	k1gNnSc4	pozastavení
činnosti	činnost	k1gFnSc2	činnost
nebo	nebo	k8xC	nebo
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
porušení	porušení	k1gNnSc1	porušení
stanov	stanova	k1gFnPc2	stanova
a	a	k8xC	a
jednacího	jednací	k2eAgInSc2d1	jednací
řádu	řád	k1gInSc2	řád
při	při	k7c6	při
organizování	organizování	k1gNnSc6	organizování
volebních	volební	k2eAgFnPc2d1	volební
konferencí	konference	k1gFnPc2	konference
hnutí	hnutí	k1gNnPc2	hnutí
v	v	k7c6	v
několika	několik	k4yIc6	několik
krajích	kraj	k1gInPc6	kraj
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
absence	absence	k1gFnSc1	absence
demokraticky	demokraticky	k6eAd1	demokraticky
ustanovených	ustanovený	k2eAgInPc2d1	ustanovený
orgánů	orgán	k1gInPc2	orgán
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
však	však	k9	však
takový	takový	k3xDgInSc4	takový
návrh	návrh	k1gInSc4	návrh
může	moct	k5eAaImIp3nS	moct
soudu	soud	k1gInSc2	soud
podat	podat	k5eAaPmF	podat
jen	jen	k9	jen
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
<g/>
Počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
navýšil	navýšit	k5eAaPmAgInS	navýšit
téměř	téměř	k6eAd1	téměř
čtyřnásobně	čtyřnásobně	k6eAd1	čtyřnásobně
a	a	k8xC	a
SPD	SPD	kA	SPD
tak	tak	k9	tak
předstihlo	předstihnout	k5eAaPmAgNnS	předstihnout
v	v	k7c6	v
početnosti	početnost	k1gFnSc6	početnost
členské	členský	k2eAgFnSc2d1	členská
základny	základna	k1gFnSc2	základna
i	i	k8xC	i
vládní	vládní	k2eAgNnSc4d1	vládní
hnutí	hnutí	k1gNnSc4	hnutí
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
Tomio	Tomio	k1gMnSc1	Tomio
Okamura	Okamur	k1gMnSc2	Okamur
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
SPD	SPD	kA	SPD
7	[number]	k4	7
tisíc	tisíc	k4xCgInSc1	tisíc
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
tisíc	tisíc	k4xCgInPc2	tisíc
žadatelů	žadatel	k1gMnPc2	žadatel
o	o	k7c6	o
členství	členství	k1gNnSc6	členství
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1	mluvčí
hnutí	hnutí	k1gNnPc4	hnutí
však	však	k9	však
upřesnila	upřesnit	k5eAaPmAgFnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mělo	mít	k5eAaImAgNnS	mít
hnutí	hnutí	k1gNnSc4	hnutí
jen	jen	k6eAd1	jen
1	[number]	k4	1
400	[number]	k4	400
členů	člen	k1gInPc2	člen
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
5	[number]	k4	5
500	[number]	k4	500
čekatelů	čekatel	k1gMnPc2	čekatel
na	na	k7c4	na
členství	členství	k1gNnSc4	členství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
podle	podle	k7c2	podle
stanov	stanova	k1gFnPc2	stanova
hnutí	hnutí	k1gNnSc2	hnutí
značně	značně	k6eAd1	značně
odlišné	odlišný	k2eAgInPc4d1	odlišný
statusy	status	k1gInPc4	status
<g/>
.	.	kIx.	.
</s>
<s>
Oněch	onen	k3xDgInPc2	onen
13	[number]	k4	13
tisíc	tisíc	k4xCgInPc2	tisíc
žadatelů	žadatel	k1gMnPc2	žadatel
pak	pak	k6eAd1	pak
představovaly	představovat	k5eAaImAgInP	představovat
vyplněné	vyplněný	k2eAgInPc1d1	vyplněný
formuláře	formulář	k1gInPc1	formulář
z	z	k7c2	z
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2019	[number]	k4	2019
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2019	[number]	k4	2019
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
hnutí	hnutí	k1gNnSc2	hnutí
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
svůj	svůj	k3xOyFgInSc4	svůj
regionální	regionální	k2eAgInSc4d1	regionální
klub	klub	k1gInSc4	klub
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
odebralo	odebrat	k5eAaPmAgNnS	odebrat
mu	on	k3xPp3gMnSc3	on
licenci	licence	k1gFnSc4	licence
<g/>
.	.	kIx.	.
</s>
<s>
Zdůvodnilo	zdůvodnit	k5eAaPmAgNnS	zdůvodnit
to	ten	k3xDgNnSc1	ten
porušováním	porušování	k1gNnSc7	porušování
stanov	stanova	k1gFnPc2	stanova
a	a	k8xC	a
krácením	krácení	k1gNnSc7	krácení
demokratických	demokratický	k2eAgNnPc2d1	demokratické
práv	právo	k1gNnPc2	právo
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Krajský	krajský	k2eAgInSc4d1	krajský
klub	klub	k1gInSc4	klub
vedl	vést	k5eAaImAgMnS	vést
poslanec	poslanec	k1gMnSc1	poslanec
Lubomír	Lubomír	k1gMnSc1	Lubomír
Volný	volný	k2eAgMnSc1d1	volný
<g/>
,	,	kIx,	,
kritizovaný	kritizovaný	k2eAgMnSc1d1	kritizovaný
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
některými	některý	k3yIgFnPc7	některý
členy	člen	k1gMnPc7	člen
za	za	k7c4	za
"	"	kIx"	"
<g/>
diktátorský	diktátorský	k2eAgInSc4d1	diktátorský
<g/>
"	"	kIx"	"
způsob	způsob	k1gInSc4	způsob
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
byla	být	k5eAaImAgFnS	být
SPD	SPD	kA	SPD
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
volebně	volebně	k6eAd1	volebně
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Volný	volný	k2eAgMnSc1d1	volný
oznámil	oznámit	k5eAaPmAgInS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
kandidovat	kandidovat	k5eAaImF	kandidovat
proti	proti	k7c3	proti
Okamurovi	Okamur	k1gMnSc3	Okamur
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
proti	proti	k7c3	proti
zrušení	zrušení	k1gNnSc3	zrušení
krajského	krajský	k2eAgInSc2d1	krajský
klubu	klub	k1gInSc2	klub
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2019	[number]	k4	2019
poslanci	poslanec	k1gMnPc1	poslanec
Lubomír	Lubomír	k1gMnSc1	Lubomír
Volný	volný	k2eAgMnSc1d1	volný
<g/>
,	,	kIx,	,
Marian	Marian	k1gMnSc1	Marian
Bojko	Bojko	k1gNnSc4	Bojko
a	a	k8xC	a
poslankyně	poslankyně	k1gFnSc1	poslankyně
Ivana	Ivana	k1gFnSc1	Ivana
Nevludová	Nevludová	k1gFnSc1	Nevludová
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
SPD	SPD	kA	SPD
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
za	za	k7c4	za
Moravskoslezský	moravskoslezský	k2eAgInSc4d1	moravskoslezský
kraj	kraj	k1gInSc4	kraj
<g/>
,	,	kIx,	,
odejít	odejít	k5eAaPmF	odejít
se	se	k3xPyFc4	se
prý	prý	k9	prý
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
kvůli	kvůli	k7c3	kvůli
postupu	postup	k1gInSc3	postup
nového	nový	k2eAgNnSc2d1	nové
vedení	vedení	k1gNnSc2	vedení
krajské	krajský	k2eAgFnSc2d1	krajská
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
Volného	volný	k2eAgInSc2d1	volný
přijala	přijmout	k5eAaPmAgFnS	přijmout
do	do	k7c2	do
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
usvědčeného	usvědčený	k2eAgMnSc4d1	usvědčený
rasistu	rasista	k1gMnSc4	rasista
a	a	k8xC	a
neonacistu	neonacista	k1gMnSc4	neonacista
<g/>
.	.	kIx.	.
</s>
<s>
SPD	SPD	kA	SPD
Volného	volný	k2eAgInSc2d1	volný
vyzvalo	vyzvat	k5eAaPmAgNnS	vyzvat
ke	k	k7c3	k
složení	složení	k1gNnSc3	složení
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
<g/>
Pořad	pořad	k1gInSc4	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Reportéři	reportér	k1gMnPc1	reportér
ČT	ČT	kA	ČT
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2019	[number]	k4	2019
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedení	vedení	k1gNnSc1	vedení
SPD	SPD	kA	SPD
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
nařizovalo	nařizovat	k5eAaImAgNnS	nařizovat
svým	svůj	k3xOyFgMnPc3	svůj
členům	člen	k1gMnPc3	člen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přispívali	přispívat	k5eAaImAgMnP	přispívat
do	do	k7c2	do
diskusí	diskuse	k1gFnPc2	diskuse
na	na	k7c6	na
Novinkách	novinka	k1gFnPc6	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
Parlamentních	parlamentní	k2eAgInPc6d1	parlamentní
listech	list	k1gInPc6	list
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zveřejněných	zveřejněný	k2eAgFnPc6d1	zveřejněná
nahrávkách	nahrávka	k1gFnPc6	nahrávka
poslanec	poslanec	k1gMnSc1	poslanec
Marian	Marian	k1gMnSc1	Marian
Bojko	Bojko	k1gNnSc4	Bojko
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
neuvěřitelná	uvěřitelný	k2eNgFnSc1d1	neuvěřitelná
mediální	mediální	k2eAgFnSc1d1	mediální
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nám	my	k3xPp1nPc3	my
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
voliče	volič	k1gMnPc4	volič
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Volný	volný	k2eAgMnSc1d1	volný
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnSc2d1	stará
nahrávky	nahrávka	k1gFnSc2	nahrávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Financování	financování	k1gNnSc1	financování
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislostí	souvislost	k1gFnPc2	souvislost
s	s	k7c7	s
podzimními	podzimní	k2eAgFnPc7d1	podzimní
sněmovními	sněmovní	k2eAgFnPc7d1	sněmovní
volbami	volba	k1gFnPc7	volba
2017	[number]	k4	2017
společnost	společnost	k1gFnSc1	společnost
Transparency	Transparenca	k1gFnSc2	Transparenca
International	International	k1gFnSc1	International
posuzovala	posuzovat	k5eAaImAgFnS	posuzovat
průhlednost	průhlednost	k1gFnSc4	průhlednost
financování	financování	k1gNnSc3	financování
významných	významný	k2eAgFnPc2d1	významná
kandidujících	kandidující	k2eAgFnPc2d1	kandidující
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
kampaní	kampaň	k1gFnSc7	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
SPD	SPD	kA	SPD
od	od	k7c2	od
ní	on	k3xPp3gFnSc3	on
dostala	dostat	k5eAaPmAgFnS	dostat
celkovou	celkový	k2eAgFnSc4d1	celková
hodnotící	hodnotící	k2eAgFnSc4d1	hodnotící
známku	známka	k1gFnSc4	známka
3,2	[number]	k4	3,2
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
školní	školní	k2eAgFnSc2d1	školní
škály	škála	k1gFnSc2	škála
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
nejhorší	zlý	k2eAgFnSc4d3	nejhorší
po	po	k7c6	po
Straně	strana	k1gFnSc6	strana
práv	právo	k1gNnPc2	právo
občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
3,6	[number]	k4	3,6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
hnutí	hnutí	k1gNnSc2	hnutí
Tomio	Tomio	k1gMnSc1	Tomio
Okamura	Okamur	k1gMnSc2	Okamur
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
hodnocení	hodnocení	k1gNnSc3	hodnocení
ohradil	ohradit	k5eAaPmAgMnS	ohradit
<g/>
.	.	kIx.	.
<g/>
Později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
média	médium	k1gNnSc2	médium
informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
SPD	SPD	kA	SPD
2	[number]	k4	2
dny	den	k1gInPc4	den
před	před	k7c7	před
termínem	termín	k1gInSc7	termín
pro	pro	k7c4	pro
vyúčtování	vyúčtování	k1gNnSc4	vyúčtování
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
7,4	[number]	k4	7,4
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
za	za	k7c2	za
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
public	publicum	k1gNnPc2	publicum
relations	relationsa	k1gFnPc2	relationsa
bez	bez	k7c2	bez
upřesnění	upřesnění	k1gNnSc2	upřesnění
<g/>
,	,	kIx,	,
komu	kdo	k3yQnSc3	kdo
tyto	tento	k3xDgInPc4	tento
peníze	peníz	k1gInPc4	peníz
vyplatila	vyplatit	k5eAaPmAgFnS	vyplatit
<g/>
.	.	kIx.	.
</s>
<s>
Suma	suma	k1gFnSc1	suma
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
dvojnásobku	dvojnásobek	k1gInSc2	dvojnásobek
částky	částka	k1gFnSc2	částka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
na	na	k7c4	na
podobné	podobný	k2eAgInPc4d1	podobný
účely	účel	k1gInPc4	účel
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
vítězné	vítězný	k2eAgNnSc1d1	vítězné
hnutí	hnutí	k1gNnSc1	hnutí
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Tomio	Tomio	k1gMnSc1	Tomio
Okamura	Okamur	k1gMnSc2	Okamur
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
platba	platba	k1gFnSc1	platba
směřovala	směřovat	k5eAaImAgFnS	směřovat
společnosti	společnost	k1gFnSc2	společnost
Play	play	k0	play
Net	Net	k1gFnSc6	Net
zajišťující	zajišťující	k2eAgFnPc4d1	zajišťující
analýzy	analýza	k1gFnPc4	analýza
voličů	volič	k1gMnPc2	volič
a	a	k8xC	a
kampaně	kampaň	k1gFnSc2	kampaň
či	či	k8xC	či
strategické	strategický	k2eAgNnSc1d1	strategické
plánování	plánování	k1gNnSc1	plánování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
serveru	server	k1gInSc2	server
Info	Info	k1gNnSc1	Info
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
byl	být	k5eAaImAgMnS	být
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společnosti	společnost	k1gFnSc2	společnost
Astro	astra	k1gFnSc5	astra
Capital	Capital	k1gMnSc1	Capital
vlastníkem	vlastník	k1gMnSc7	vlastník
společnosti	společnost	k1gFnSc2	společnost
Jan	Jan	k1gMnSc1	Jan
Fulín	Fulín	k1gMnSc1	Fulín
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgMnSc1d1	spojený
s	s	k7c7	s
televizí	televize	k1gFnSc7	televize
Barrandov	Barrandov	k1gInSc1	Barrandov
a	a	k8xC	a
agenturou	agentura	k1gFnSc7	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
SANEP	SANEP	kA	SANEP
<g/>
.	.	kIx.	.
</s>
<s>
Statutárním	statutární	k2eAgMnSc7d1	statutární
zástupcem	zástupce	k1gMnSc7	zástupce
Play	play	k0	play
Net	Net	k1gMnPc4	Net
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
Radomír	Radomír	k1gMnSc1	Radomír
Pekárek	Pekárek	k1gMnSc1	Pekárek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zároveň	zároveň	k6eAd1	zároveň
řídil	řídit	k5eAaImAgMnS	řídit
server	server	k1gInSc4	server
První	první	k4xOgFnSc2	první
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
také	také	k9	také
figurovali	figurovat	k5eAaImAgMnP	figurovat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Easy	Easa	k1gMnSc2	Easa
Communications	Communications	k1gInSc4	Communications
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
měla	mít	k5eAaImAgFnS	mít
vlastnický	vlastnický	k2eAgInSc4d1	vlastnický
podíl	podíl	k1gInSc4	podíl
i	i	k8xC	i
firma	firma	k1gFnSc1	firma
Our	Our	k1gFnSc2	Our
Media	medium	k1gNnSc2	medium
provozující	provozující	k2eAgInSc1d1	provozující
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
Parlamentní	parlamentní	k2eAgInPc4d1	parlamentní
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
obchodnímu	obchodní	k2eAgInSc3d1	obchodní
vztahu	vztah	k1gInSc3	vztah
s	s	k7c7	s
SPD	SPD	kA	SPD
nebo	nebo	k8xC	nebo
agenturami	agentura	k1gFnPc7	agentura
ohradila	ohradit	k5eAaPmAgFnS	ohradit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fulín	Fulín	k1gMnSc1	Fulín
i	i	k8xC	i
Pekárek	Pekárek	k1gMnSc1	Pekárek
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
pracovali	pracovat	k5eAaImAgMnP	pracovat
ještě	ještě	k9	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
podnikatel	podnikatel	k1gMnSc1	podnikatel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
.	.	kIx.	.
<g/>
Server	server	k1gInSc1	server
Seznam	seznam	k1gInSc1	seznam
Zprávy	zpráva	k1gFnSc2	zpráva
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hnutí	hnutí	k1gNnSc1	hnutí
vybralo	vybrat	k5eAaPmAgNnS	vybrat
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
zákonné	zákonný	k2eAgFnSc2d1	zákonná
lhůty	lhůta	k1gFnSc2	lhůta
z	z	k7c2	z
transparentního	transparentní	k2eAgInSc2d1	transparentní
účtu	účet	k1gInSc2	účet
pro	pro	k7c4	pro
volební	volební	k2eAgNnSc4d1	volební
kampaň	kampaň	k1gFnSc1	kampaň
všechny	všechen	k3xTgInPc4	všechen
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
peníze	peníz	k1gInPc1	peníz
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
účet	účet	k1gInSc4	účet
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
s	s	k7c7	s
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
omylu	omyl	k1gInSc3	omyl
a	a	k8xC	a
tajemník	tajemník	k1gMnSc1	tajemník
peníze	peníz	k1gInPc4	peníz
předčasně	předčasně	k6eAd1	předčasně
převedl	převést	k5eAaPmAgInS	převést
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
běžný	běžný	k2eAgInSc4d1	běžný
účet	účet	k1gInSc4	účet
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
hospodařením	hospodaření	k1gNnSc7	hospodaření
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
politických	politický	k2eAgNnPc2d1	politické
hnutí	hnutí	k1gNnPc2	hnutí
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
udělil	udělit	k5eAaPmAgInS	udělit
hnutí	hnutí	k1gNnSc4	hnutí
pokutu	pokuta	k1gFnSc4	pokuta
<g/>
.	.	kIx.	.
<g/>
Investigativní	investigativní	k2eAgMnPc1d1	investigativní
reportéři	reportér	k1gMnPc1	reportér
Jiří	Jiří	k1gMnSc1	Jiří
Kubík	Kubík	k1gMnSc1	Kubík
a	a	k8xC	a
Sabina	Sabina	k1gFnSc1	Sabina
Slonková	Slonková	k1gFnSc1	Slonková
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2018	[number]	k4	2018
na	na	k7c6	na
serveru	server	k1gInSc6	server
Seznam	seznam	k1gInSc1	seznam
Zprávy	zpráva	k1gFnSc2	zpráva
informovali	informovat	k5eAaBmAgMnP	informovat
o	o	k7c6	o
policejním	policejní	k2eAgNnSc6d1	policejní
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zabývalo	zabývat	k5eAaImAgNnS	zabývat
stranickým	stranický	k2eAgInSc7d1	stranický
e-shopem	ehop	k1gInSc7	e-shop
<g/>
,	,	kIx,	,
vynucenými	vynucený	k2eAgInPc7d1	vynucený
nákupy	nákup	k1gInPc7	nákup
členy	člen	k1gInPc7	člen
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
Moravskoslezkém	Moravskoslezký	k2eAgInSc6d1	Moravskoslezký
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
netransparentním	transparentní	k2eNgInSc7d1	netransparentní
účtem	účet	k1gInSc7	účet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
údajně	údajně	k6eAd1	údajně
k	k	k7c3	k
potřebě	potřeba	k1gFnSc3	potřeba
předsedovy	předsedův	k2eAgFnSc2d1	předsedova
krajské	krajský	k2eAgFnSc2d1	krajská
organizace	organizace	k1gFnSc2	organizace
hnutí	hnutí	k1gNnSc2	hnutí
SPD	SPD	kA	SPD
Lubomíru	Lubomír	k1gMnSc3	Lubomír
Volnému	volný	k2eAgMnSc3d1	volný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnutí	hnutí	k1gNnSc1	hnutí
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
krajské	krajský	k2eAgFnPc4d1	krajská
volby	volba	k1gFnPc4	volba
2016	[number]	k4	2016
hnutí	hnutí	k1gNnSc1	hnutí
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
Stranou	strana	k1gFnSc7	strana
práv	právo	k1gNnPc2	právo
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
koalice	koalice	k1gFnSc2	koalice
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
krajích	kraj	k1gInPc6	kraj
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Pardubického	pardubický	k2eAgNnSc2d1	pardubické
a	a	k8xC	a
Zlínského	zlínský	k2eAgNnSc2d1	zlínské
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
SPO	SPO	kA	SPO
uspěla	uspět	k5eAaPmAgFnS	uspět
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
volbách	volba	k1gFnPc6	volba
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
koaličních	koaliční	k2eAgFnPc2d1	koaliční
krajských	krajský	k2eAgFnPc2d1	krajská
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Okamura	Okamura	k1gFnSc1	Okamura
při	při	k7c6	při
ohlášení	ohlášení	k1gNnSc6	ohlášení
spojenectví	spojenectví	k1gNnSc2	spojenectví
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přes	přes	k7c4	přes
"	"	kIx"	"
<g/>
drobné	drobný	k2eAgInPc4d1	drobný
programové	programový	k2eAgInPc4d1	programový
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
"	"	kIx"	"
obě	dva	k4xCgNnPc4	dva
politická	politický	k2eAgNnPc4d1	politické
uskupení	uskupení	k1gNnPc4	uskupení
spojuje	spojovat	k5eAaImIp3nS	spojovat
kontinuální	kontinuální	k2eAgFnSc1d1	kontinuální
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
prezidenta	prezident	k1gMnSc4	prezident
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
,	,	kIx,	,
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
muslimské	muslimský	k2eAgFnSc3d1	muslimská
kolonizaci	kolonizace	k1gFnSc3	kolonizace
<g/>
"	"	kIx"	"
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
proti	proti	k7c3	proti
ilegální	ilegální	k2eAgFnSc3d1	ilegální
migraci	migrace	k1gFnSc3	migrace
či	či	k8xC	či
prosazování	prosazování	k1gNnSc6	prosazování
přímé	přímý	k2eAgFnSc2d1	přímá
volby	volba	k1gFnSc2	volba
hejtmanů	hejtman	k1gMnPc2	hejtman
<g/>
.	.	kIx.	.
<g/>
Hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
podařilo	podařit	k5eAaPmAgNnS	podařit
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
10	[number]	k4	10
krajských	krajský	k2eAgNnPc2d1	krajské
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
celkově	celkově	k6eAd1	celkově
34	[number]	k4	34
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
v	v	k7c6	v
krajích	kraj	k1gInPc6	kraj
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získalo	získat	k5eAaPmAgNnS	získat
7,33	[number]	k4	7,33
<g/>
%	%	kIx~	%
a	a	k8xC	a
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
7,02	[number]	k4	7,02
<g/>
%	%	kIx~	%
a	a	k8xC	a
šest	šest	k4xCc4	šest
zastupitelských	zastupitelský	k2eAgNnPc2d1	zastupitelské
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
2016	[number]	k4	2016
hnutí	hnutí	k1gNnPc2	hnutí
vyslalo	vyslat	k5eAaPmAgNnS	vyslat
tři	tři	k4xCgMnPc4	tři
kandidáty	kandidát	k1gMnPc4	kandidát
<g/>
:	:	kIx,	:
podnikatele	podnikatel	k1gMnSc4	podnikatel
Jiřího	Jiří	k1gMnSc4	Jiří
Kobzu	kobza	k1gFnSc4	kobza
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
Mělník	Mělník	k1gInSc1	Mělník
<g/>
,	,	kIx,	,
učitelku	učitelka	k1gFnSc4	učitelka
na	na	k7c4	na
penzi	penze	k1gFnSc4	penze
Věru	Věra	k1gFnSc4	Věra
Příhodovou	Příhodová	k1gFnSc4	Příhodová
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
Praha	Praha	k1gFnSc1	Praha
11	[number]	k4	11
a	a	k8xC	a
jednatele	jednatel	k1gMnSc2	jednatel
společnosti	společnost	k1gFnSc2	společnost
Petra	Petr	k1gMnSc2	Petr
Paula	Paul	k1gMnSc2	Paul
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Žádnému	žádný	k3yNgNnSc3	žádný
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
kandidátů	kandidát	k1gMnPc2	kandidát
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
postoupit	postoupit	k5eAaPmF	postoupit
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
10,64	[number]	k4	10,64
%	%	kIx~	%
získalo	získat	k5eAaPmAgNnS	získat
hnutí	hnutí	k1gNnSc1	hnutí
22	[number]	k4	22
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pětiprocentní	pětiprocentní	k2eAgFnSc4d1	pětiprocentní
hranici	hranice	k1gFnSc4	hranice
překonalo	překonat	k5eAaPmAgNnS	překonat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
krajích	kraj	k1gInPc6	kraj
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Piráty	pirát	k1gMnPc7	pirát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
senátních	senátní	k2eAgFnPc6d1	senátní
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
hnutí	hnutí	k1gNnSc2	hnutí
SPD	SPD	kA	SPD
samostatně	samostatně	k6eAd1	samostatně
kandiduje	kandidovat	k5eAaImIp3nS	kandidovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
27	[number]	k4	27
volebních	volební	k2eAgInPc6d1	volební
obvodech	obvod	k1gInPc6	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
2018	[number]	k4	2018
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
SPD	SPD	kA	SPD
slibuje	slibovat	k5eAaImIp3nS	slibovat
například	například	k6eAd1	například
urychlenou	urychlený	k2eAgFnSc4d1	urychlená
organizaci	organizace	k1gFnSc4	organizace
výkupu	výkup	k1gInSc2	výkup
potřebných	potřebný	k2eAgInPc2d1	potřebný
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
výstavbu	výstavba	k1gFnSc4	výstavba
metra	metro	k1gNnSc2	metro
D.	D.	kA	D.
Zároveň	zároveň	k6eAd1	zároveň
chce	chtít	k5eAaImIp3nS	chtít
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
města	město	k1gNnSc2	město
postavit	postavit	k5eAaPmF	postavit
tisíc	tisíc	k4xCgInSc4	tisíc
nových	nový	k2eAgInPc2d1	nový
bytů	byt	k1gInPc2	byt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volební	volební	k2eAgInPc1d1	volební
výsledky	výsledek	k1gInPc1	výsledek
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Politici	politik	k1gMnPc1	politik
SPD	SPD	kA	SPD
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poslanci	poslanec	k1gMnPc1	poslanec
PČR	PČR	kA	PČR
===	===	k?	===
</s>
</p>
<p>
<s>
předseda	předseda	k1gMnSc1	předseda
klubu	klub	k1gInSc2	klub
–	–	k?	–
Radim	Radim	k1gMnSc1	Radim
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
Hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
výboru	výbor	k1gInSc2	výbor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
místopředseda	místopředseda	k1gMnSc1	místopředseda
klubu	klub	k1gInSc2	klub
–	–	k?	–
Radek	Radek	k1gMnSc1	Radek
Rozvoral	Rozvoral	k1gMnSc1	Rozvoral
</s>
</p>
<p>
<s>
členové	člen	k1gMnPc1	člen
klubu	klub	k1gInSc2	klub
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dvořák	Dvořák	k1gMnSc1	Dvořák
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Holík	Holík	k1gMnSc1	Holík
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hrnčíř	Hrnčíř	k1gMnSc1	Hrnčíř
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
Hyťhová	Hyťhová	k1gFnSc1	Hyťhová
</s>
</p>
<p>
<s>
Monika	Monika	k1gFnSc1	Monika
Jarošová	Jarošová	k1gFnSc1	Jarošová
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Jelínek	Jelínek	k1gMnSc1	Jelínek
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobza	kobza	k1gFnSc1	kobza
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kohoutek	Kohoutek	k1gMnSc1	Kohoutek
</s>
</p>
<p>
<s>
Radek	Radek	k1gMnSc1	Radek
Koten	Koten	k2eAgMnSc1d1	Koten
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Levová	Levová	k1gFnSc1	Levová
</s>
</p>
<p>
<s>
Karla	Karla	k1gFnSc1	Karla
Maříková	Maříková	k1gFnSc1	Maříková
</s>
</p>
<p>
<s>
Tomio	Tomio	k6eAd1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podal	podat	k5eAaPmAgMnS	podat
</s>
</p>
<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Rozner	Rozner	k1gMnSc1	Rozner
</s>
</p>
<p>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Šafránková	Šafránková	k1gFnSc1	Šafránková
</s>
</p>
<p>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Španěl	Španěl	k1gMnSc1	Španěl
</s>
</p>
<p>
<s>
Radovan	Radovan	k1gMnSc1	Radovan
Vích	Vích	k1gMnSc1	Vích
</s>
</p>
<p>
<s>
===	===	k?	===
Poslanci	poslanec	k1gMnPc1	poslanec
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
Hynek	Hynek	k1gMnSc1	Hynek
Blaško	Blaška	k1gFnSc5	Blaška
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
David	David	k1gMnSc1	David
</s>
</p>
<p>
<s>
===	===	k?	===
Krajští	krajský	k2eAgMnPc1d1	krajský
zastupitelé	zastupitel	k1gMnPc1	zastupitel
===	===	k?	===
</s>
</p>
<p>
<s>
Koalice	koalice	k1gFnSc1	koalice
SPD	SPD	kA	SPD
a	a	k8xC	a
SPO	SPO	kA	SPO
v	v	k7c6	v
říjnových	říjnový	k2eAgFnPc6d1	říjnová
volbách	volba	k1gFnPc6	volba
2016	[number]	k4	2016
získala	získat	k5eAaPmAgFnS	získat
celkem	celkem	k6eAd1	celkem
34	[number]	k4	34
zastupitelských	zastupitelský	k2eAgInPc2d1	zastupitelský
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
18	[number]	k4	18
kandidátů	kandidát	k1gMnPc2	kandidát
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
hnutím	hnutí	k1gNnSc7	hnutí
SPD	SPD	kA	SPD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vedení	vedení	k1gNnSc1	vedení
hnutí	hnutí	k1gNnSc2	hnutí
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
8	[number]	k4	8
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc3	květen
2019	[number]	k4	2019
mělo	mít	k5eAaImAgNnS	mít
hnutí	hnutí	k1gNnSc1	hnutí
SPD	SPD	kA	SPD
toto	tento	k3xDgNnSc4	tento
vedení	vedení	k1gNnSc4	vedení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
===	===	k?	===
</s>
</p>
<p>
<s>
předseda	předseda	k1gMnSc1	předseda
–	–	k?	–
Tomio	Tomio	k1gMnSc1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
</s>
</p>
<p>
<s>
místopředseda	místopředseda	k1gMnSc1	místopředseda
–	–	k?	–
Radim	Radim	k1gMnSc1	Radim
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
SPD	SPD	kA	SPD
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
člen	člen	k1gMnSc1	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Holík	Holík	k1gMnSc1	Holík
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
PČR	PČR	kA	PČR
</s>
</p>
<p>
<s>
člen	člen	k1gMnSc1	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
–	–	k?	–
Radek	Radek	k1gMnSc1	Radek
Rozvoral	Rozvoral	k1gMnSc1	Rozvoral
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
SPD	SPD	kA	SPD
<g/>
,	,	kIx,	,
místostarosta	místostarosta	k1gMnSc1	místostarosta
obce	obec	k1gFnSc2	obec
Všechlapy	Všechlapa	k1gFnSc2	Všechlapa
</s>
</p>
<p>
<s>
člen	člen	k1gMnSc1	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
–	–	k?	–
Radovan	Radovan	k1gMnSc1	Radovan
Vích	Vích	k1gMnSc1	Vích
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
===	===	k?	===
Krajští	krajský	k2eAgMnPc1d1	krajský
předsedové	předseda	k1gMnPc1	předseda
===	===	k?	===
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Novák	Novák	k1gMnSc1	Novák
</s>
</p>
<p>
<s>
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Radek	Radek	k1gMnSc1	Radek
Rozvoral	Rozvoral	k1gMnSc1	Rozvoral
</s>
</p>
<p>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1	jihočeský
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Wachtfeitl	Wachtfeitl	k1gMnSc1	Wachtfeitl
</s>
</p>
<p>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Marie	Marie	k1gFnSc1	Marie
Pošarová	Pošarová	k1gFnSc1	Pošarová
</s>
</p>
<p>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Karla	Karel	k1gMnSc2	Karel
Maříková	Maříková	k1gFnSc1	Maříková
</s>
</p>
<p>
<s>
Ústecký	ústecký	k2eAgInSc1d1	ústecký
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Dominik	Dominik	k1gMnSc1	Dominik
Hanko	Hanka	k1gFnSc5	Hanka
</s>
</p>
<p>
<s>
Liberecký	liberecký	k2eAgInSc1d1	liberecký
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Radovan	Radovan	k1gMnSc1	Radovan
Vích	Vích	k1gMnSc1	Vích
</s>
</p>
<p>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Andrea	Andrea	k1gFnSc1	Andrea
Pajgerová	Pajgerový	k2eAgFnSc1d1	Pajgerový
(	(	kIx(	(
<g/>
pověřena	pověřit	k5eAaPmNgFnS	pověřit
vedením	vedení	k1gNnSc7	vedení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Kohoutek	Kohoutek	k1gMnSc1	Kohoutek
</s>
</p>
<p>
<s>
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Fink	Fink	k1gMnSc1	Fink
</s>
</p>
<p>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Hrnčíř	Hrnčíř	k1gMnSc1	Hrnčíř
</s>
</p>
<p>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
<g/>
:	:	kIx,	:
Radim	Radim	k1gMnSc1	Radim
Fiala	Fiala	k1gMnSc1	Fiala
</s>
</p>
<p>
<s>
Zlínský	zlínský	k2eAgMnSc1d1	zlínský
<g/>
:	:	kIx,	:
Radek	Radek	k1gMnSc1	Radek
Henner	Henner	k1gMnSc1	Henner
</s>
</p>
<p>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
<g/>
:	:	kIx,	:
Irena	Irena	k1gFnSc1	Irena
Bláhová	bláhový	k2eAgFnSc1d1	bláhová
(	(	kIx(	(
<g/>
pověřena	pověřit	k5eAaPmNgFnS	pověřit
vedením	vedení	k1gNnSc7	vedení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
výroční	výroční	k2eAgFnSc6d1	výroční
zprávě	zpráva	k1gFnSc6	zpráva
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2019	[number]	k4	2019
uvedlo	uvést	k5eAaPmAgNnS	uvést
stranu	strana	k1gFnSc4	strana
SPD	SPD	kA	SPD
jako	jako	k8xC	jako
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
šíří	šířit	k5eAaImIp3nS	šířit
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
nenávist	nenávist	k1gFnSc4	nenávist
k	k	k7c3	k
jiným	jiný	k2eAgMnPc3d1	jiný
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
dezinformace	dezinformace	k1gFnSc1	dezinformace
či	či	k8xC	či
konspirační	konspirační	k2eAgFnSc1d1	konspirační
teorie	teorie	k1gFnSc1	teorie
štěpící	štěpící	k2eAgFnSc1d1	štěpící
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Napsalo	napsat	k5eAaPmAgNnS	napsat
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
hnutí	hnutí	k1gNnSc1	hnutí
cílí	cílit	k5eAaImIp3nS	cílit
na	na	k7c4	na
skupiny	skupina	k1gFnPc4	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
manipulativní	manipulativní	k2eAgFnPc1d1	manipulativní
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jim	on	k3xPp3gMnPc3	on
soustavně	soustavně	k6eAd1	soustavně
předkládá	předkládat	k5eAaImIp3nS	předkládat
<g/>
,	,	kIx,	,
nebudou	být	k5eNaImBp3nP	být
podrobovat	podrobovat	k5eAaImF	podrobovat
kritické	kritický	k2eAgFnSc3d1	kritická
reflexi	reflexe	k1gFnSc3	reflexe
<g/>
.	.	kIx.	.
</s>
<s>
Stranické	stranický	k2eAgFnPc1d1	stranická
volební	volební	k2eAgFnPc1d1	volební
noviny	novina	k1gFnPc1	novina
SPD	SPD	kA	SPD
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nepravdy	nepravda	k1gFnSc2	nepravda
<g/>
,	,	kIx,	,
neověřitelná	ověřitelný	k2eNgNnPc4d1	neověřitelné
fakta	faktum	k1gNnPc4	faktum
<g/>
,	,	kIx,	,
lži	lež	k1gFnPc4	lež
či	či	k8xC	či
manipulace	manipulace	k1gFnPc4	manipulace
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Tomio	Tomio	k1gMnSc1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
podle	podle	k7c2	podle
webu	web	k1gInSc2	web
Demagog	demagog	k1gMnSc1	demagog
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
rozhovorech	rozhovor	k1gInPc6	rozhovor
či	či	k8xC	či
debatách	debata	k1gFnPc6	debata
často	často	k6eAd1	často
argumentuje	argumentovat	k5eAaImIp3nS	argumentovat
zavádějícími	zavádějící	k2eAgInPc7d1	zavádějící
či	či	k8xC	či
nepravdivými	pravdivý	k2eNgInPc7d1	nepravdivý
fakty	fakt	k1gInPc7	fakt
<g/>
.	.	kIx.	.
<g/>
Podporovatel	podporovatel	k1gMnSc1	podporovatel
SPD	SPD	kA	SPD
Jaromír	Jaromír	k1gMnSc1	Jaromír
Balda	balda	k1gMnSc1	balda
také	také	k9	také
spáchal	spáchat	k5eAaPmAgMnS	spáchat
Teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
vlaky	vlak	k1gInPc4	vlak
na	na	k7c6	na
Mladoboleslavsku	Mladoboleslavsko	k1gNnSc6	Mladoboleslavsko
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
dvakrát	dvakrát	k6eAd1	dvakrát
pokácel	pokácet	k5eAaPmAgInS	pokácet
strom	strom	k1gInSc1	strom
na	na	k7c4	na
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
činu	čin	k1gInSc2	čin
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
mohou	moct	k5eAaImIp3nP	moct
muslimští	muslimský	k2eAgMnPc1d1	muslimský
teroristé	terorista	k1gMnPc1	terorista
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
způsobit	způsobit	k5eAaPmF	způsobit
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
strach	strach	k1gInSc4	strach
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
migrační	migrační	k2eAgFnSc7d1	migrační
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svoboda	Svoboda	k1gMnSc1	Svoboda
a	a	k8xC	a
přímá	přímý	k2eAgFnSc1d1	přímá
demokracie	demokracie	k1gFnSc1	demokracie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
hnutí	hnutí	k1gNnSc2	hnutí
</s>
</p>
