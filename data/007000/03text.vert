<s>
Sputnik	sputnik	k1gInSc1	sputnik
3	[number]	k4	3
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
sovětská	sovětský	k2eAgFnSc1d1	sovětská
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
družice	družice	k1gFnSc1	družice
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
vypuštěná	vypuštěný	k2eAgFnSc1d1	vypuštěná
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1958	[number]	k4	1958
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Bajkonur	Bajkonura	k1gFnPc2	Bajkonura
pomocí	pomocí	k7c2	pomocí
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
rakety	raketa	k1gFnSc2	raketa
R-	R-	k1gFnSc2	R-
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
SS-	SS-	k1gFnSc2	SS-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
vypuštění	vypuštění	k1gNnSc4	vypuštění
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
skončil	skončit	k5eAaPmAgMnS	skončit
explozí	exploze	k1gFnSc7	exploze
rakety	raketa	k1gFnSc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInPc1d1	počáteční
parametry	parametr	k1gInPc1	parametr
orbitální	orbitální	k2eAgFnSc2d1	orbitální
dráhy	dráha	k1gFnSc2	dráha
<g/>
:	:	kIx,	:
výška	výška	k1gFnSc1	výška
214	[number]	k4	214
<g/>
×	×	k?	×
<g/>
1860	[number]	k4	1860
km	km	kA	km
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc1	sklon
65,18	[number]	k4	65,18
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
105,90	[number]	k4	105,90
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Družice	družice	k1gFnSc1	družice
měla	mít	k5eAaImAgFnS	mít
kónický	kónický	k2eAgInSc4d1	kónický
tvar	tvar	k1gInSc4	tvar
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
3,57	[number]	k4	3,57
m	m	kA	m
a	a	k8xC	a
průměru	průměr	k1gInSc6	průměr
základny	základna	k1gFnSc2	základna
1,73	[number]	k4	1,73
m.	m.	k?	m.
Její	její	k3xOp3gInSc4	její
hmotnost	hmotnost	k1gFnSc1	hmotnost
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
327	[number]	k4	327
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Napájení	napájení	k1gNnSc1	napájení
bylo	být	k5eAaImAgNnS	být
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
pomocí	pomocí	k7c2	pomocí
slunečních	sluneční	k2eAgInPc2d1	sluneční
článků	článek	k1gInPc2	článek
rozmístěných	rozmístěný	k2eAgInPc2d1	rozmístěný
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
pláště	plášť	k1gInSc2	plášť
družice	družice	k1gFnPc1	družice
<g/>
.	.	kIx.	.
</s>
<s>
Přístrojové	přístrojový	k2eAgNnSc1d1	přístrojové
vybavení	vybavení	k1gNnSc1	vybavení
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
měřily	měřit	k5eAaImAgInP	měřit
<g/>
:	:	kIx,	:
tlak	tlak	k1gInSc1	tlak
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
vysokých	vysoký	k2eAgFnPc2d1	vysoká
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
koncentraci	koncentrace	k1gFnSc4	koncentrace
kladně	kladně	k6eAd1	kladně
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
intenzitu	intenzita	k1gFnSc4	intenzita
korpuskulárního	korpuskulární	k2eAgNnSc2d1	korpuskulární
záření	záření	k1gNnSc2	záření
Slunce	slunce	k1gNnSc2	slunce
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
variace	variace	k1gFnSc1	variace
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
elektrostatický	elektrostatický	k2eAgInSc4d1	elektrostatický
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
elektrické	elektrický	k2eAgNnSc4d1	elektrické
pole	pole	k1gNnSc4	pole
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Země	zem	k1gFnSc2	zem
intenzitu	intenzita	k1gFnSc4	intenzita
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
dopady	dopad	k1gInPc4	dopad
mikrometeoritů	mikrometeorit	k1gInPc2	mikrometeorit
teploty	teplota	k1gFnSc2	teplota
uvnitř	uvnitř	k7c2	uvnitř
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
družice	družice	k1gFnSc2	družice
Družice	družice	k1gFnSc2	družice
detekovala	detekovat	k5eAaImAgFnS	detekovat
vnější	vnější	k2eAgNnSc4d1	vnější
radiační	radiační	k2eAgNnSc4d1	radiační
pole	pole	k1gNnSc4	pole
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
rozložení	rozložení	k1gNnSc1	rozložení
záření	záření	k1gNnSc2	záření
Van	vana	k1gFnPc2	vana
Allenových	Allenová	k1gFnPc2	Allenová
radiačních	radiační	k2eAgInPc2d1	radiační
pásů	pás	k1gInPc2	pás
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
změřit	změřit	k5eAaPmF	změřit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
selhal	selhat	k5eAaPmAgInS	selhat
magnetofon	magnetofon	k1gInSc1	magnetofon
pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Aktivace	aktivace	k1gFnSc1	aktivace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
vysílání	vysílání	k1gNnSc1	vysílání
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
činnosti	činnost	k1gFnSc2	činnost
družice	družice	k1gFnSc1	družice
byly	být	k5eAaImAgFnP	být
řízeny	řídit	k5eAaImNgFnP	řídit
pomocí	pomocí	k7c2	pomocí
elektronického	elektronický	k2eAgInSc2d1	elektronický
sekvenčního	sekvenční	k2eAgInSc2d1	sekvenční
automatu	automat	k1gInSc2	automat
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
.	.	kIx.	.
</s>
<s>
Družice	družice	k1gFnSc1	družice
sloužila	sloužit	k5eAaImAgFnS	sloužit
až	až	k6eAd1	až
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pozvolného	pozvolný	k2eAgNnSc2d1	pozvolné
snižování	snižování	k1gNnSc2	snižování
dráhy	dráha	k1gFnSc2	dráha
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
zániku	zánik	k1gInSc3	zánik
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
vydržela	vydržet	k5eAaPmAgFnS	vydržet
692	[number]	k4	692
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
