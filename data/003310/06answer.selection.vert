<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
průkopníky	průkopník	k1gMnPc4	průkopník
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
moderního	moderní	k2eAgInSc2d1	moderní
hard	hard	k6eAd1	hard
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
oni	onen	k3xDgMnPc1	onen
sami	sám	k3xTgMnPc1	sám
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
za	za	k7c4	za
heavymetalovou	heavymetalový	k2eAgFnSc4d1	heavymetalová
skupinu	skupina	k1gFnSc4	skupina
nepovažovali	považovat	k5eNaImAgMnP	považovat
<g/>
.	.	kIx.	.
</s>
