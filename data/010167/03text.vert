<p>
<s>
Kilometr	kilometr	k1gInSc1	kilometr
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
km	km	kA	km
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
délky	délka	k1gFnSc2	délka
v	v	k7c6	v
metrické	metrický	k2eAgFnSc6d1	metrická
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
tisíci	tisíc	k4xCgInPc7	tisíc
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
notací	notace	k1gFnSc7	notace
jako	jako	k9	jako
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
3	[number]	k4	3
m	m	kA	m
(	(	kIx(	(
<g/>
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
notace	notace	k1gFnSc1	notace
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
1	[number]	k4	1
E	E	kA	E
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
m	m	kA	m
(	(	kIx(	(
<g/>
exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
notace	notace	k1gFnSc1	notace
<g/>
)	)	kIx)	)
–	–	k?	–
oboje	oboj	k1gFnPc4	oboj
znamená	znamenat	k5eAaImIp3nS	znamenat
1	[number]	k4	1
000	[number]	k4	000
×	×	k?	×
1	[number]	k4	1
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Kilometr	kilometr	k1gInSc1	kilometr
se	se	k3xPyFc4	se
jako	jako	k9	jako
jednotka	jednotka	k1gFnSc1	jednotka
délky	délka	k1gFnSc2	délka
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
v	v	k7c6	v
nejbližším	blízký	k2eAgNnSc6d3	nejbližší
vesmírném	vesmírný	k2eAgNnSc6d1	vesmírné
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
odvozen	odvozen	k2eAgInSc1d1	odvozen
kilometr	kilometr	k1gInSc4	kilometr
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
používaná	používaný	k2eAgFnSc1d1	používaná
jednotka	jednotka	k1gFnSc1	jednotka
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
jednotka	jednotka	k1gFnSc1	jednotka
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
kilometr	kilometr	k1gInSc4	kilometr
(	(	kIx(	(
<g/>
=	=	kIx~	=
milion	milion	k4xCgInSc4	milion
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
a	a	k8xC	a
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
jednotka	jednotka	k1gFnSc1	jednotka
objemu	objem	k1gInSc2	objem
je	být	k5eAaImIp3nS	být
krychlový	krychlový	k2eAgInSc4d1	krychlový
kilometr	kilometr	k1gInSc4	kilometr
(	(	kIx(	(
<g/>
=	=	kIx~	=
miliarda	miliarda	k4xCgFnSc1	miliarda
krychlových	krychlový	k2eAgInPc2d1	krychlový
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poměr	poměr	k1gInSc1	poměr
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
jednotkám	jednotka	k1gFnPc3	jednotka
délky	délka	k1gFnSc2	délka
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
kilometr	kilometr	k1gInSc1	kilometr
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
1	[number]	k4	1
metr	metr	k1gInSc1	metr
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
0,001	[number]	k4	0,001
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
cca	cca	kA	cca
0,621	[number]	k4	0,621
(	(	kIx(	(
<g/>
angloamerických	angloamerický	k2eAgFnPc2d1	angloamerická
<g/>
)	)	kIx)	)
mílí	míle	k1gFnSc7	míle
(	(	kIx(	(
<g/>
1	[number]	k4	1
míle	míle	k1gFnSc1	míle
=	=	kIx~	=
1,609	[number]	k4	1,609
<g/>
344	[number]	k4	344
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
cca	cca	kA	cca
0,540	[number]	k4	0,540
námořních	námořní	k2eAgFnPc2d1	námořní
mílí	míle	k1gFnSc7	míle
(	(	kIx(	(
<g/>
1	[number]	k4	1
NM	NM	kA	NM
=	=	kIx~	=
1,852	[number]	k4	1,852
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
cca	cca	kA	cca
0,9374	[number]	k4	0,9374
verst	versta	k1gFnPc2	versta
(	(	kIx(	(
<g/>
1	[number]	k4	1
versta	versta	k1gFnSc1	versta
=	=	kIx~	=
1,066	[number]	k4	1,066
<g/>
781	[number]	k4	781
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
cca	cca	kA	cca
1	[number]	k4	1
094	[number]	k4	094
yardů	yard	k1gInPc2	yard
(	(	kIx(	(
<g/>
1	[number]	k4	1
yrd	yrd	k?	yrd
=	=	kIx~	=
0,0009144	[number]	k4	0,0009144
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
cca	cca	kA	cca
3	[number]	k4	3
281	[number]	k4	281
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
1	[number]	k4	1
stopa	stopa	k1gFnSc1	stopa
=	=	kIx~	=
0,0003048	[number]	k4	0,0003048
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
1	[number]	k4	1
světelný	světelný	k2eAgInSc4d1	světelný
rok	rok	k1gInSc4	rok
činí	činit	k5eAaImIp3nS	činit
9	[number]	k4	9
460	[number]	k4	460
730	[number]	k4	730
472	[number]	k4	472
580	[number]	k4	580
km	km	kA	km
(	(	kIx(	(
<g/>
cca	cca	kA	cca
10	[number]	k4	10
bilionů	bilion	k4xCgInPc2	bilion
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
násobkům	násobek	k1gInPc3	násobek
metru	metr	k1gInSc2	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
značka	značka	k1gFnSc1	značka
<	<	kIx(	<
znamená	znamenat	k5eAaImIp3nS	znamenat
rozdíl	rozdíl	k1gInSc1	rozdíl
jednoho	jeden	k4xCgInSc2	jeden
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pikometr	pikometr	k1gInSc1	pikometr
<<<	<<<	k?	<<<
nanometr	nanometr	k1gInSc1	nanometr
<<<	<<<	k?	<<<
mikrometr	mikrometr	k1gInSc1	mikrometr
<<<	<<<	k?	<<<
milimetr	milimetr	k1gInSc1	milimetr
<	<	kIx(	<
centimetr	centimetr	k1gInSc1	centimetr
<	<	kIx(	<
decimetr	decimetr	k1gInSc1	decimetr
<	<	kIx(	<
metr	metr	k1gInSc1	metr
<	<	kIx(	<
dekametr	dekametr	k1gInSc1	dekametr
<	<	kIx(	<
hektametr	hektametr	k1gInSc1	hektametr
<	<	kIx(	<
kilometr	kilometr	k1gInSc1	kilometr
<<<	<<<	k?	<<<
megametr	megametr	k1gInSc1	megametr
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Předpona	předpona	k1gFnSc1	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	se	k3xPyFc3	se
</s>
</p>
<p>
<s>
Řádová	řádový	k2eAgFnSc1d1	řádová
velikost	velikost	k1gFnSc1	velikost
</s>
</p>
<p>
<s>
Kilometrovník	kilometrovník	k1gInSc1	kilometrovník
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kilometr	kilometr	k1gInSc4	kilometr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
kilometr	kilometr	k1gInSc4	kilometr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
