<s>
Pentagon	Pentagon	k1gInSc1	Pentagon
je	být	k5eAaImIp3nS	být
sídlo	sídlo	k1gNnSc4	sídlo
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
nacházející	nacházející	k2eAgFnSc7d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Arlington	Arlington	k1gInSc1	Arlington
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc4	termín
Pentagon	Pentagon	k1gInSc4	Pentagon
metonymicky	metonymicky	k6eAd1	metonymicky
vztahován	vztahován	k2eAgMnSc1d1	vztahován
k	k	k7c3	k
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
obrany	obrana	k1gFnSc2	obrana
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
spíše	spíše	k9	spíše
než	než	k8xS	než
k	k	k7c3	k
samotné	samotný	k2eAgFnSc3d1	samotná
budově	budova	k1gFnSc3	budova
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
navržena	navržen	k2eAgFnSc1d1	navržena
architektem	architekt	k1gMnSc7	architekt
Georgem	Georg	k1gMnSc7	Georg
Bergstromem	Bergstrom	k1gInSc7	Bergstrom
a	a	k8xC	a
dozorován	dozorován	k2eAgInSc1d1	dozorován
generálem	generál	k1gMnSc7	generál
Lesliem	Leslium	k1gNnSc7	Leslium
Grovesem	Groves	k1gMnSc7	Groves
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
a	a	k8xC	a
dokončena	dokončen	k2eAgFnSc1d1	dokončena
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Pentagon	Pentagon	k1gInSc1	Pentagon
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
kancelářská	kancelářský	k2eAgFnSc1d1	kancelářská
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
600	[number]	k4	600
000	[number]	k4	000
m2	m2	k4	m2
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
je	být	k5eAaImIp3nS	být
240	[number]	k4	240
000	[number]	k4	000
využíváno	využívat	k5eAaImNgNnS	využívat
jako	jako	k9	jako
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c4	na
28	[number]	k4	28
000	[number]	k4	000
vojenských	vojenský	k2eAgMnPc2d1	vojenský
a	a	k8xC	a
civilních	civilní	k2eAgMnPc2d1	civilní
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
3000	[number]	k4	3000
lidí	člověk	k1gMnPc2	člověk
kteří	který	k3yQgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
podzemní	podzemní	k2eAgFnSc1d1	podzemní
a	a	k8xC	a
5	[number]	k4	5
nadzemních	nadzemní	k2eAgNnPc2d1	nadzemní
podlaží	podlaží	k1gNnPc2	podlaží
a	a	k8xC	a
pět	pět	k4xCc4	pět
prstencových	prstencový	k2eAgInPc2d1	prstencový
koridorů	koridor	k1gInPc2	koridor
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
28	[number]	k4	28
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
Pentagonu	Pentagon	k1gInSc2	Pentagon
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
centrální	centrální	k2eAgNnSc4d1	centrální
náměstí	náměstí	k1gNnSc4	náměstí
neoficiálně	neoficiálně	k6eAd1	neoficiálně
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
"	"	kIx"	"
<g/>
Ground	Ground	k1gMnSc1	Ground
zero	zero	k1gMnSc1	zero
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
přezdívka	přezdívka	k1gFnSc1	přezdívka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
během	během	k7c2	během
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nukleární	nukleární	k2eAgFnSc2d1	nukleární
války	válka	k1gFnSc2	válka
by	by	kYmCp3nS	by
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
přinejmenším	přinejmenším	k6eAd1	přinejmenším
jednu	jeden	k4xCgFnSc4	jeden
jadernou	jaderný	k2eAgFnSc4d1	jaderná
hlavici	hlavice	k1gFnSc4	hlavice
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
narazil	narazit	k5eAaPmAgMnS	narazit
unesený	unesený	k2eAgInSc4d1	unesený
Boeing	boeing	k1gInSc4	boeing
757	[number]	k4	757
Let	léto	k1gNnPc2	léto
American	American	k1gMnSc1	American
Airlines	Airlines	k1gMnSc1	Airlines
77	[number]	k4	77
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
189	[number]	k4	189
<g />
.	.	kIx.	.
</s>
<s>
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
5	[number]	k4	5
únosců	únosce	k1gMnPc2	únosce
<g/>
,	,	kIx,	,
59	[number]	k4	59
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
a	a	k8xC	a
125	[number]	k4	125
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
názvu	název	k1gInSc2	název
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
pětiúhelník	pětiúhelník	k1gInSc4	pětiúhelník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
mohutná	mohutný	k2eAgFnSc1d1	mohutná
čtyřpatrová	čtyřpatrový	k2eAgFnSc1d1	čtyřpatrová
stavba	stavba	k1gFnSc1	stavba
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
pěti	pět	k4xCc2	pět
soustředných	soustředný	k2eAgInPc2d1	soustředný
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
pětiúhelníkových	pětiúhelníkový	k2eAgInPc2d1	pětiúhelníkový
okruhů	okruh	k1gInPc2	okruh
(	(	kIx(	(
<g/>
rings	ringsa	k1gFnPc2	ringsa
<g/>
,	,	kIx,	,
značených	značený	k2eAgFnPc2d1	značená
A	a	k9	a
až	až	k9	až
E	E	kA	E
<g/>
,	,	kIx,	,
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
strany	strana	k1gFnSc2	strana
přes	přes	k7c4	přes
200	[number]	k4	200
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protnuté	protnutý	k2eAgNnSc4d1	protnuté
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
třemi	tři	k4xCgFnPc7	tři
spojnicemi	spojnice	k1gFnPc7	spojnice
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
celé	celý	k2eAgFnSc2d1	celá
struktury	struktura	k1gFnSc2	struktura
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zahrada	zahrada	k1gFnSc1	zahrada
s	s	k7c7	s
několika	několik	k4yIc7	několik
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
<s>
Zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
plocha	plocha	k1gFnSc1	plocha
objektu	objekt	k1gInSc2	objekt
zabírá	zabírat	k5eAaImIp3nS	zabírat
11,6	[number]	k4	11,6
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
2	[number]	k4	2
hektary	hektar	k1gInPc4	hektar
je	být	k5eAaImIp3nS	být
výměra	výměra	k1gFnSc1	výměra
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Prostory	prostora	k1gFnPc1	prostora
Pentagonu	Pentagon	k1gInSc2	Pentagon
jsou	být	k5eAaImIp3nP	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určeny	určit	k5eAaPmNgFnP	určit
kódem	kód	k1gInSc7	kód
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
snadno	snadno	k6eAd1	snadno
vyčíst	vyčíst	k5eAaPmF	vyčíst
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
3D426	[number]	k4	3D426
znamená	znamenat	k5eAaImIp3nS	znamenat
3	[number]	k4	3
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
strany	strana	k1gFnSc2	strana
D	D	kA	D
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
chodba	chodba	k1gFnSc1	chodba
<g/>
,	,	kIx,	,
místnost	místnost	k1gFnSc1	místnost
č.	č.	k?	č.
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Pentagonu	Pentagon	k1gInSc2	Pentagon
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
dálničního	dálniční	k2eAgInSc2d1	dálniční
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Potomacu	Potomacum	k1gNnSc3	Potomacum
též	též	k9	též
malé	malý	k2eAgNnSc1d1	malé
přístaviště	přístaviště	k1gNnSc1	přístaviště
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
známý	známý	k2eAgInSc1d1	známý
Arlingtonský	Arlingtonský	k2eAgInSc1d1	Arlingtonský
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
postavit	postavit	k5eAaPmF	postavit
jedno	jeden	k4xCgNnSc4	jeden
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
koordinovány	koordinován	k2eAgFnPc1d1	koordinována
válečné	válečný	k2eAgFnPc1d1	válečná
operace	operace	k1gFnPc1	operace
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
vzešla	vzejít	k5eAaPmAgFnS	vzejít
za	za	k7c2	za
úřadu	úřad	k1gInSc2	úřad
Franklina	Franklina	k1gFnSc1	Franklina
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
znepokojovala	znepokojovat	k5eAaImAgFnS	znepokojovat
expanze	expanze	k1gFnSc1	expanze
Hitlera	Hitler	k1gMnSc2	Hitler
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
Pentagonu	Pentagon	k1gInSc2	Pentagon
bylo	být	k5eAaImAgNnS	být
17	[number]	k4	17
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
plánovala	plánovat	k5eAaImAgFnS	plánovat
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
budovy	budova	k1gFnSc2	budova
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
83	[number]	k4	83
milionů	milion	k4xCgInPc2	milion
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
úspory	úspora	k1gFnPc4	úspora
vzešlé	vzešlý	k2eAgFnPc4d1	vzešlá
ze	z	k7c2	z
soustředění	soustředění	k1gNnSc2	soustředění
velení	velení	k1gNnSc2	velení
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
převýšily	převýšit	k5eAaPmAgInP	převýšit
náklady	náklad	k1gInPc1	náklad
za	za	k7c4	za
stavbu	stavba	k1gFnSc4	stavba
(	(	kIx(	(
<g/>
jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
touto	tento	k3xDgFnSc7	tento
úsporou	úspora	k1gFnSc7	úspora
si	se	k3xPyFc3	se
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
Pentagon	Pentagon	k1gInSc4	Pentagon
vydělal	vydělat	k5eAaPmAgInS	vydělat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bombovému	bombový	k2eAgInSc3d1	bombový
útoku	útok	k1gInSc3	útok
v	v	k7c4	v
Oklahoma	Oklahoma	k1gNnSc4	Oklahoma
City	City	k1gFnSc2	City
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
prošel	projít	k5eAaPmAgInS	projít
Pentagon	Pentagon	k1gInSc4	Pentagon
významnější	významný	k2eAgFnSc7d2	významnější
renovací	renovace	k1gFnSc7	renovace
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
strany	strana	k1gFnSc2	strana
byly	být	k5eAaImAgInP	být
ztvrzeny	ztvrdit	k5eAaPmNgInP	ztvrdit
ocelí	ocel	k1gFnSc7	ocel
<g/>
,	,	kIx,	,
do	do	k7c2	do
vnějších	vnější	k2eAgNnPc2d1	vnější
oken	okno	k1gNnPc2	okno
byla	být	k5eAaImAgNnP	být
nasazena	nasazen	k2eAgNnPc1d1	nasazeno
skla	sklo	k1gNnPc1	sklo
schopná	schopný	k2eAgNnPc1d1	schopné
odolat	odolat	k5eAaPmF	odolat
vysokým	vysoký	k2eAgFnPc3d1	vysoká
změnám	změna	k1gFnPc3	změna
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
částech	část	k1gFnPc6	část
byly	být	k5eAaImAgFnP	být
nainstalovány	nainstalován	k2eAgFnPc1d1	nainstalována
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
automaticky	automaticky	k6eAd1	automaticky
měly	mít	k5eAaImAgFnP	mít
otevřít	otevřít	k5eAaPmF	otevřít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
prováděli	provádět	k5eAaImAgMnP	provádět
pracovníci	pracovník	k1gMnPc1	pracovník
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
internisté	internista	k1gMnPc1	internista
Pentagonu	Pentagon	k1gInSc2	Pentagon
cvičení	cvičení	k1gNnSc1	cvičení
nacvičující	nacvičující	k2eAgFnSc4d1	nacvičující
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
krizové	krizový	k2eAgFnPc4d1	krizová
situace	situace	k1gFnPc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
scénář	scénář	k1gInSc4	scénář
byl	být	k5eAaImAgMnS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
mělo	mít	k5eAaImAgNnS	mít
narazit	narazit	k5eAaPmF	narazit
komerční	komerční	k2eAgNnSc1d1	komerční
dopravní	dopravní	k2eAgNnSc1d1	dopravní
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
přesně	přesně	k6eAd1	přesně
60	[number]	k4	60
let	léto	k1gNnPc2	léto
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
stavby	stavba	k1gFnSc2	stavba
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Pentagon	Pentagon	k1gInSc1	Pentagon
stal	stát	k5eAaPmAgInS	stát
cílem	cíl	k1gInSc7	cíl
teroristického	teroristický	k2eAgInSc2d1	teroristický
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
jižní	jižní	k2eAgFnSc2d1	jižní
stěny	stěna	k1gFnSc2	stěna
narazil	narazit	k5eAaPmAgMnS	narazit
unesený	unesený	k2eAgInSc4d1	unesený
Boeing	boeing	k1gInSc4	boeing
757	[number]	k4	757
letu	let	k1gInSc2	let
77	[number]	k4	77
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
nebýt	být	k5eNaImF	být
odvahy	odvaha	k1gFnPc4	odvaha
pasažérů	pasažér	k1gMnPc2	pasažér
vzbouřit	vzbouřit	k5eAaPmF	vzbouřit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
únoscům	únosce	k1gMnPc3	únosce
způsobil	způsobit	k5eAaPmAgInS	způsobit
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnPc4d2	veliký
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
