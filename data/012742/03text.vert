<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
tragédie	tragédie	k1gFnSc1	tragédie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
An	An	k1gFnSc1	An
American	Americany	k1gInPc2	Americany
Tragedy	Trageda	k1gMnSc2	Trageda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Theodora	Theodor	k1gMnSc2	Theodor
Dreisera	Dreiser	k1gMnSc2	Dreiser
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
splnění	splnění	k1gNnSc6	splnění
tzv.	tzv.	kA	tzv.
amerického	americký	k2eAgInSc2d1	americký
snu	sen	k1gInSc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnPc1d1	základní
fakta	faktum	k1gNnPc1	faktum
románu	román	k1gInSc2	román
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kapitola	kapitola	k1gFnSc1	kapitola
I	i	k9	i
===	===	k?	===
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
mladíkovi	mladík	k1gMnSc6	mladík
Clydu	Clyd	k1gMnSc6	Clyd
Griffithsovi	Griffiths	k1gMnSc6	Griffiths
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
jako	jako	k9	jako
pouliční	pouliční	k2eAgMnPc1d1	pouliční
kazatelé	kazatel	k1gMnPc1	kazatel
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
jim	on	k3xPp3gMnPc3	on
v	v	k7c4	v
jejich	jejich	k3xOp3gFnSc4	jejich
práci	práce	k1gFnSc4	práce
musí	muset	k5eAaImIp3nP	muset
pomáhat	pomáhat	k5eAaImF	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
velice	velice	k6eAd1	velice
nepříjemné	příjemný	k2eNgNnSc1d1	nepříjemné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
práci	práce	k1gFnSc3	práce
často	často	k6eAd1	často
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
a	a	k8xC	a
nenavštěvuje	navštěvovat	k5eNaImIp3nS	navštěvovat
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Připadá	připadat	k5eAaImIp3nS	připadat
si	se	k3xPyFc3	se
tak	tak	k9	tak
odstrčený	odstrčený	k2eAgMnSc1d1	odstrčený
a	a	k8xC	a
ukřivděný	ukřivděný	k2eAgInSc1d1	ukřivděný
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
mladíkům	mladík	k1gMnPc3	mladík
jeho	on	k3xPp3gInSc2	on
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Problémy	problém	k1gInPc1	problém
v	v	k7c6	v
silně	silně	k6eAd1	silně
pobožné	pobožný	k2eAgFnSc6d1	pobožná
rodině	rodina	k1gFnSc6	rodina
Clyda	Clydo	k1gNnSc2	Clydo
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Ester	Ester	k1gFnSc2	Ester
uteče	utéct	k5eAaPmIp3nS	utéct
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnSc3d1	mladá
dívce	dívka	k1gFnSc3	dívka
nevyhovovala	vyhovovat	k5eNaImAgFnS	vyhovovat
striktní	striktní	k2eAgFnSc1d1	striktní
výchova	výchova	k1gFnSc1	výchova
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
utekla	utéct	k5eAaPmAgFnS	utéct
s	s	k7c7	s
mladíkem	mladík	k1gMnSc7	mladík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
nasliboval	naslibovat	k5eAaBmAgMnS	naslibovat
skvělou	skvělý	k2eAgFnSc4d1	skvělá
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Clyde	Clyde	k6eAd1	Clyde
získá	získat	k5eAaPmIp3nS	získat
práci	práce	k1gFnSc4	práce
za	za	k7c7	za
sodovým	sodový	k2eAgInSc7d1	sodový
výčepem	výčep	k1gInSc7	výčep
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chce	chtít	k5eAaImIp3nS	chtít
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Dostane	dostat	k5eAaPmIp3nS	dostat
místo	místo	k1gNnSc4	místo
poslíčka	poslíček	k1gMnSc2	poslíček
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Green-Davidson	Green-Davidson	k1gInSc4	Green-Davidson
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
city	city	k1gFnSc2	city
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
práce	práce	k1gFnSc1	práce
velmi	velmi	k6eAd1	velmi
zamlouvá	zamlouvat	k5eAaImIp3nS	zamlouvat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
musí	muset	k5eAaImIp3nS	muset
nosit	nosit	k5eAaImF	nosit
pěknou	pěkný	k2eAgFnSc4d1	pěkná
uniformu	uniforma	k1gFnSc4	uniforma
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
jídlo	jídlo	k1gNnSc1	jídlo
a	a	k8xC	a
dobré	dobrý	k2eAgNnSc1d1	dobré
spropitné	spropitné	k1gNnSc1	spropitné
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
může	moct	k5eAaImIp3nS	moct
nechávat	nechávat	k5eAaImF	nechávat
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
poslíčky	poslíček	k1gMnPc7	poslíček
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
mají	mít	k5eAaImIp3nP	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Berou	brát	k5eAaImIp3nP	brát
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
barů	bar	k1gInPc2	bar
<g/>
,	,	kIx,	,
na	na	k7c4	na
večeře	večeře	k1gFnPc4	večeře
i	i	k9	i
do	do	k7c2	do
hanbince	hanbinec	k1gInSc2	hanbinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
na	na	k7c4	na
Clyda	Clyd	k1gMnSc4	Clyd
zapůsobí	zapůsobit	k5eAaPmIp3nS	zapůsobit
Rotter	Rotter	k1gInSc1	Rotter
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
volnomyšlenkářská	volnomyšlenkářský	k2eAgFnSc1d1	volnomyšlenkářská
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
na	na	k7c4	na
tancovačku	tancovačka	k1gFnSc4	tancovačka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potká	potkat	k5eAaPmIp3nS	potkat
Hortensii	Hortensie	k1gFnSc4	Hortensie
Briggsovou	Briggsový	k2eAgFnSc4d1	Briggsový
<g/>
.	.	kIx.	.
</s>
<s>
Zalíbí	zalíbit	k5eAaPmIp3nS	zalíbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
domluví	domluvit	k5eAaPmIp3nS	domluvit
si	se	k3xPyFc3	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
Horzensie	Horzensie	k1gFnSc1	Horzensie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
upovídaná	upovídaný	k2eAgFnSc1d1	upovídaná
a	a	k8xC	a
sebestředná	sebestředný	k2eAgFnSc1d1	sebestředná
<g/>
.	.	kIx.	.
</s>
<s>
Setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
více	hodně	k6eAd2	hodně
chlapci	chlapec	k1gMnPc7	chlapec
najednou	najednou	k6eAd1	najednou
a	a	k8xC	a
Clyda	Clyda	k1gFnSc1	Clyda
pouze	pouze	k6eAd1	pouze
využívá	využívat	k5eAaPmIp3nS	využívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Clyde	Clyd	k1gInSc5	Clyd
na	na	k7c6	na
vrcholu	vrchol	k1gInSc3	vrchol
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
styků	styk	k1gInPc2	styk
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
vyzná	vyznat	k5eAaPmIp3nS	vyznat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
zpozoruje	zpozorovat	k5eAaPmIp3nS	zpozorovat
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nenápadně	nápadně	k6eNd1	nápadně
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Clyde	Clyde	k6eAd1	Clyde
počká	počkat	k5eAaPmIp3nS	počkat
<g/>
,	,	kIx,	,
až	až	k9	až
vyjde	vyjít	k5eAaPmIp3nS	vyjít
<g/>
,	,	kIx,	,
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
podívat	podívat	k5eAaImF	podívat
<g/>
.	.	kIx.	.
</s>
<s>
Nalezne	nalézt	k5eAaBmIp3nS	nalézt
zde	zde	k6eAd1	zde
Estu	Est	k1gMnSc3	Est
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jí	on	k3xPp3gFnSc3	on
přítel	přítel	k1gMnSc1	přítel
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
těhotná	těhotný	k2eAgFnSc1d1	těhotná
a	a	k8xC	a
bez	bez	k7c2	bez
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohužel	bohužel	k9	bohužel
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
si	se	k3xPyFc3	se
Hortensie	Hortensie	k1gFnPc1	Hortensie
usmyslí	usmyslet	k5eAaPmIp3nP	usmyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
koupit	koupit	k5eAaPmF	koupit
velmi	velmi	k6eAd1	velmi
drahý	drahý	k2eAgInSc4d1	drahý
kabát	kabát	k1gInSc4	kabát
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
matka	matka	k1gFnSc1	matka
prosí	prosit	k5eAaImIp3nS	prosit
Clyda	Clydo	k1gNnPc4	Clydo
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
pro	pro	k7c4	pro
Estu	Est	k1gMnSc3	Est
<g/>
,	,	kIx,	,
a	a	k8xC	a
on	on	k3xPp3gInSc1	on
ji	on	k3xPp3gFnSc4	on
dá	dát	k5eAaPmIp3nS	dát
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
výplaty	výplata	k1gFnSc2	výplata
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
dost	dost	k6eAd1	dost
peněz	peníze	k1gInPc2	peníze
pro	pro	k7c4	pro
Hortensiin	Hortensiin	k2eAgInSc4d1	Hortensiin
kabát	kabát	k1gInSc4	kabát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
kapitola	kapitola	k1gFnSc1	kapitola
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
kamarádi	kamarád	k1gMnPc1	kamarád
Clyda	Clyd	k1gMnSc2	Clyd
z	z	k7c2	z
hotelu	hotel	k1gInSc2	hotel
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
vyjedou	vyjet	k5eAaPmIp3nP	vyjet
autem	auto	k1gNnSc7	auto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tancovačce	tancovačka	k1gFnSc6	tancovačka
Hortensie	Hortensie	k1gFnSc2	Hortensie
tancuje	tancovat	k5eAaImIp3nS	tancovat
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
kamarádů	kamarád	k1gMnPc2	kamarád
Clyda	Clydo	k1gNnSc2	Clydo
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zalíbí	zalíbit	k5eAaPmIp3nS	zalíbit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
kloužou	klouzat	k5eAaImIp3nP	klouzat
na	na	k7c6	na
zamrznuté	zamrznutý	k2eAgFnSc6d1	zamrznutá
řece	řeka	k1gFnSc6	řeka
a	a	k8xC	a
Hortensie	Hortensie	k1gFnSc1	Hortensie
opět	opět	k6eAd1	opět
Clydovi	Clyda	k1gMnSc3	Clyda
nevěnuje	věnovat	k5eNaImIp3nS	věnovat
moc	moc	k1gFnSc4	moc
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Clyde	Clyde	k6eAd1	Clyde
žárlí	žárlit	k5eAaImIp3nS	žárlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tráví	trávit	k5eAaImIp3nS	trávit
zde	zde	k6eAd1	zde
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
a	a	k8xC	a
k	k	k7c3	k
ránu	ráno	k1gNnSc3	ráno
pospíchají	pospíchat	k5eAaImIp3nP	pospíchat
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
práce	práce	k1gFnSc1	práce
poslíčka	poslíček	k1gMnSc2	poslíček
si	se	k3xPyFc3	se
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vždy	vždy	k6eAd1	vždy
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
včas	včas	k6eAd1	včas
a	a	k8xC	a
upravění	upravění	k1gNnSc4	upravění
<g/>
.	.	kIx.	.
</s>
<s>
Všem	všecek	k3xTgMnPc3	všecek
hrozil	hrozit	k5eAaImAgInS	hrozit
vyhazov	vyhazov	k1gInSc4	vyhazov
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jedou	jet	k5eAaImIp3nP	jet
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
až	až	k9	až
ve	v	k7c6	v
městě	město	k1gNnSc6	město
srazí	srazit	k5eAaPmIp3nS	srazit
za	za	k7c7	za
zatáčkou	zatáčka	k1gFnSc7	zatáčka
jedno	jeden	k4xCgNnSc1	jeden
malé	malý	k2eAgNnSc1d1	malé
děvčátko	děvčátko	k1gNnSc1	děvčátko
<g/>
.	.	kIx.	.
</s>
<s>
Nachvíli	Nachvíl	k1gMnPc1	Nachvíl
zastaví	zastavit	k5eAaPmIp3nP	zastavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
začnou	začít	k5eAaPmIp3nP	začít
ujíždět	ujíždět	k5eAaImF	ujíždět
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tma	tma	k6eAd1	tma
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vypnutá	vypnutý	k2eAgNnPc1d1	vypnuté
světla	světlo	k1gNnPc1	světlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gNnSc4	on
neviděla	vidět	k5eNaImAgFnS	vidět
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
když	když	k8xS	když
projíždí	projíždět	k5eAaImIp3nS	projíždět
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
jednou	jeden	k4xCgFnSc7	jeden
temnou	temný	k2eAgFnSc7d1	temná
uličkou	ulička	k1gFnSc7	ulička
<g/>
,	,	kIx,	,
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
dlažební	dlažební	k2eAgFnPc4d1	dlažební
kostky	kostka	k1gFnPc4	kostka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
zanechali	zanechat	k5eAaPmAgMnP	zanechat
dělníci	dělník	k1gMnPc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
zranění	zraněný	k2eAgMnPc1d1	zraněný
v	v	k7c6	v
autě	auto	k1gNnSc6	auto
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
se	se	k3xPyFc4	se
rozprchnou	rozprchnout	k5eAaPmIp3nP	rozprchnout
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Clyda	Clydo	k1gNnSc2	Clydo
<g/>
.	.	kIx.	.
<g/>
Tlačil	tlačit	k5eAaImAgMnS	tlačit
se	se	k3xPyFc4	se
na	na	k7c4	na
ostatní	ostatní	k2eAgInPc4d1	ostatní
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
jen	jen	k9	jen
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
píď	píď	k1gFnSc4	píď
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
Šestnácté	šestnáctý	k4xOgFnSc2	šestnáctý
a	a	k8xC	a
Washingtonovy	Washingtonův	k2eAgFnSc2d1	Washingtonova
ulice	ulice	k1gFnSc2	ulice
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlevo	vlevo	k6eAd1	vlevo
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
jednoho	jeden	k4xCgInSc2	jeden
bloku	blok	k1gInSc2	blok
dost	dost	k6eAd1	dost
volno	volno	k6eAd1	volno
<g/>
,	,	kIx,	,
zatočil	zatočit	k5eAaPmAgMnS	zatočit
tam	tam	k6eAd1	tam
a	a	k8xC	a
hnal	hnát	k5eAaImAgMnS	hnát
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
Wyandottskou	Wyandottský	k2eAgFnSc4d1	Wyandottský
ulici	ulice	k1gFnSc4	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
když	když	k8xS	když
se	se	k3xPyFc4	se
blížil	blížit	k5eAaImAgInS	blížit
k	k	k7c3	k
nároží	nároží	k1gNnSc3	nároží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chystal	chystat	k5eAaImAgMnS	chystat
ve	v	k7c6	v
veliké	veliký	k2eAgFnSc6d1	veliká
rychlosti	rychlost	k1gFnSc6	rychlost
zatočit	zatočit	k5eAaPmF	zatočit
a	a	k8xC	a
zajel	zajet	k5eAaPmAgMnS	zajet
proto	proto	k8xC	proto
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
okraji	okraj	k1gInSc3	okraj
chodníku	chodník	k1gInSc2	chodník
<g/>
,	,	kIx,	,
skočilo	skočit	k5eAaPmAgNnS	skočit
přímo	přímo	k6eAd1	přímo
před	před	k7c4	před
jedoucí	jedoucí	k2eAgInSc4d1	jedoucí
automobil	automobil	k1gInSc4	automobil
asi	asi	k9	asi
devítileté	devítiletý	k2eAgNnSc1d1	devítileté
děvčátko	děvčátko	k1gNnSc1	děvčátko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
tudy	tudy	k6eAd1	tudy
běželo	běžet	k5eAaImAgNnS	běžet
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
už	už	k6eAd1	už
neměl	mít	k5eNaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
zatočit	zatočit	k5eAaPmF	zatočit
a	a	k8xC	a
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
vůz	vůz	k1gInSc4	vůz
děvčátko	děvčátko	k1gNnSc4	děvčátko
povalil	povalit	k5eAaPmAgInS	povalit
a	a	k8xC	a
vlekl	vleknout	k5eAaImAgInS	vleknout
několik	několik	k4yIc4	několik
stop	stopa	k1gFnPc2	stopa
<g/>
,	,	kIx,	,
než	než	k8xS	než
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
ozvaly	ozvat	k5eAaPmAgInP	ozvat
pronikavé	pronikavý	k2eAgInPc1d1	pronikavý
výkřiky	výkřik	k1gInPc1	výkřik
nejméně	málo	k6eAd3	málo
půl	půl	k1xP	půl
tuctu	tucet	k1gInSc2	tucet
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
zavolání	zavolání	k1gNnSc6	zavolání
právě	právě	k9	právě
tolika	tolik	k4yIc2	tolik
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
svědky	svědek	k1gMnPc7	svědek
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
<g/>
Aniž	aniž	k8xS	aniž
se	se	k3xPyFc4	se
poradil	poradit	k5eAaPmAgInS	poradit
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dosud	dosud	k6eAd1	dosud
zpola	zpola	k6eAd1	zpola
stáli	stát	k5eAaImAgMnP	stát
a	a	k8xC	a
strachy	strach	k1gInPc4	strach
takřka	takřka	k6eAd1	takřka
oněměli	oněmět	k5eAaPmAgMnP	oněmět
<g/>
,	,	kIx,	,
hodil	hodit	k5eAaPmAgMnS	hodit
tam	tam	k6eAd1	tam
první	první	k4xOgFnSc4	první
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
druhou	druhý	k4xOgFnSc7	druhý
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
<g/>
,	,	kIx,	,
šlápl	šlápnout	k5eAaPmAgMnS	šlápnout
na	na	k7c4	na
plyn	plyn	k1gInSc4	plyn
až	až	k9	až
k	k	k7c3	k
podlaze	podlaha	k1gFnSc3	podlaha
a	a	k8xC	a
zahnul	zahnout	k5eAaPmAgMnS	zahnout
za	za	k7c4	za
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kapitola	kapitola	k1gFnSc1	kapitola
II	II	kA	II
===	===	k?	===
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
falešným	falešný	k2eAgNnSc7d1	falešné
jménem	jméno	k1gNnSc7	jméno
Clyde	Clyd	k1gInSc5	Clyd
postupně	postupně	k6eAd1	postupně
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
několik	několik	k4yIc4	několik
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dostal	dostat	k5eAaPmAgMnS	dostat
místo	místo	k1gNnSc4	místo
poslíčka	poslíček	k1gMnSc2	poslíček
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Unionistické	unionistický	k2eAgFnSc2d1	unionistická
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
poznal	poznat	k5eAaPmAgMnS	poznat
tady	tady	k6eAd1	tady
i	i	k9	i
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
velmi	velmi	k6eAd1	velmi
zapůsobila	zapůsobit	k5eAaPmAgFnS	zapůsobit
a	a	k8xC	a
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
dokonce	dokonce	k9	dokonce
podiv	podiv	k1gInSc4	podiv
a	a	k8xC	a
uctivou	uctivý	k2eAgFnSc4d1	uctivá
bázeň	bázeň	k1gFnSc4	bázeň
-	-	kIx~	-
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tady	tady	k6eAd1	tady
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
v	v	k7c6	v
nejmenší	malý	k2eAgFnSc6d3	nejmenší
míře	míra	k1gFnSc6	míra
přítomen	přítomen	k2eAgInSc1d1	přítomen
pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
živel	živel	k1gInSc1	živel
<g/>
,	,	kIx,	,
charakterizující	charakterizující	k2eAgNnSc1d1	charakterizující
takřka	takřka	k6eAd1	takřka
všechen	všechen	k3xTgInSc4	všechen
život	život	k1gInSc4	život
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Green-Davidson	Green-Davidsona	k1gFnPc2	Green-Davidsona
a	a	k8xC	a
nedávno	nedávno	k6eAd1	nedávno
i	i	k9	i
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Great	Great	k1gInSc1	Great
Norther	Northra	k1gFnPc2	Northra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
pamatoval	pamatovat	k5eAaImAgInS	pamatovat
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
mu	on	k3xPp3gNnSc3	on
vlastně	vlastně	k9	vlastně
připadalo	připadat	k5eAaPmAgNnS	připadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
živel	živel	k1gInSc4	živel
proniká	pronikat	k5eAaImIp3nS	pronikat
a	a	k8xC	a
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
takřka	takřka	k6eAd1	takřka
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
ba	ba	k9	ba
zcela	zcela	k6eAd1	zcela
všechny	všechen	k3xTgInPc1	všechen
životní	životní	k2eAgInPc1d1	životní
děje	děj	k1gInPc1	děj
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tady	tady	k6eAd1	tady
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
stykům	styk	k1gInPc3	styk
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
-	-	kIx~	-
nebyla	být	k5eNaImAgFnS	být
zde	zde	k6eAd1	zde
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
ani	ani	k8xC	ani
stopa	stopa	k1gFnSc1	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
klubu	klub	k1gInSc2	klub
ženy	žena	k1gFnPc1	žena
nesměly	smět	k5eNaImAgFnP	smět
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
vynikající	vynikající	k2eAgFnPc1d1	vynikající
osobnosti	osobnost	k1gFnPc1	osobnost
přicházely	přicházet	k5eAaImAgFnP	přicházet
zpravidla	zpravidla	k6eAd1	zpravidla
samy	sám	k3xTgFnPc1	sám
a	a	k8xC	a
s	s	k7c7	s
onou	onen	k3xDgFnSc7	onen
nehlučnou	hlučný	k2eNgFnSc7d1	nehlučná
rázností	ráznost	k1gFnSc7	ráznost
a	a	k8xC	a
uzavřeností	uzavřenost	k1gFnSc7	uzavřenost
<g/>
,	,	kIx,	,
příznačnou	příznačný	k2eAgFnSc7d1	příznačná
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
svrchovaně	svrchovaně	k6eAd1	svrchovaně
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
sami	sám	k3xTgMnPc1	sám
jedli	jíst	k5eAaImAgMnP	jíst
<g/>
,	,	kIx,	,
v	v	k7c6	v
dvojicích	dvojice	k1gFnPc6	dvojice
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
nehlučně	hlučně	k6eNd1	hlučně
rozmlouvali	rozmlouvat	k5eAaImAgMnP	rozmlouvat
-	-	kIx~	-
četli	číst	k5eAaImAgMnP	číst
noviny	novina	k1gFnPc4	novina
nebo	nebo	k8xC	nebo
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
jezdili	jezdit	k5eAaImAgMnP	jezdit
tam	tam	k6eAd1	tam
nebo	nebo	k8xC	nebo
onam	onam	k6eAd1	onam
v	v	k7c6	v
rychlých	rychlý	k2eAgInPc6d1	rychlý
automobilech	automobil	k1gInPc6	automobil
-	-	kIx~	-
ale	ale	k8xC	ale
živel	živel	k1gInSc4	živel
vášně	vášeň	k1gFnSc2	vášeň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
až	až	k9	až
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
připadalo	připadat	k5eAaPmAgNnS	připadat
Clydově	Clydův	k2eAgFnSc3d1	Clydův
nezralé	zralý	k2eNgFnSc3d1	nezralá
mysli	mysl	k1gFnSc3	mysl
<g/>
,	,	kIx,	,
vyvolával	vyvolávat	k5eAaImAgInS	vyvolávat
k	k	k7c3	k
životu	život	k1gInSc3	život
a	a	k8xC	a
rozvracel	rozvracet	k5eAaImAgInS	rozvracet
tak	tak	k6eAd1	tak
mnohé	mnohý	k2eAgFnPc4d1	mnohá
věci	věc	k1gFnPc4	věc
v	v	k7c6	v
obyčejném	obyčejný	k2eAgInSc6d1	obyčejný
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
až	až	k9	až
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
patřil	patřit	k5eAaImAgMnS	patřit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
většinou	většinou	k6eAd1	většinou
ani	ani	k8xC	ani
neuvědomovali	uvědomovat	k5eNaImAgMnP	uvědomovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aspoň	aspoň	k9	aspoň
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nepůsobil	působit	k5eNaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
podivuhodném	podivuhodný	k2eAgInSc6d1	podivuhodný
světě	svět	k1gInSc6	svět
člověk	člověk	k1gMnSc1	člověk
patrně	patrně	k6eAd1	patrně
nemůže	moct	k5eNaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nějakého	nějaký	k3yIgNnSc2	nějaký
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
zachovat	zachovat	k5eAaPmF	zachovat
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
není-l	není	k1gInSc4	není-l
lhostejný	lhostejný	k2eAgInSc4d1	lhostejný
k	k	k7c3	k
pohlaví	pohlaví	k1gNnSc3	pohlaví
<g/>
,	,	kIx,	,
k	k	k7c3	k
vášni	vášeň	k1gFnSc3	vášeň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
ohavná	ohavný	k2eAgFnSc1d1	ohavná
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
a	a	k8xC	a
před	před	k7c4	před
zraky	zrak	k1gInPc4	zrak
takových	takový	k3xDgMnPc2	takový
lidí	člověk	k1gMnPc2	člověk
jednat	jednat	k5eAaImF	jednat
a	a	k8xC	a
vypadat	vypadat	k5eAaImF	vypadat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
člověku	člověk	k1gMnSc3	člověk
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
hony	hon	k1gInPc4	hon
vzdáleny	vzdálit	k5eAaPmNgFnP	vzdálit
všechny	všechen	k3xTgFnPc1	všechen
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jím	jíst	k5eAaImIp1nS	jíst
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
zmítají	zmítat	k5eAaImIp3nP	zmítat
<g/>
.	.	kIx.	.
<g/>
Abychom	aby	kYmCp1nP	aby
totiž	totiž	k9	totiž
řekli	říct	k5eAaPmAgMnP	říct
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
Clydově	Clydův	k2eAgFnSc3d1	Clydův
duši	duše	k1gFnSc3	duše
nebylo	být	k5eNaImAgNnS	být
souzeno	souzen	k2eAgNnSc1d1	souzeno
dozrát	dozrát	k5eAaPmF	dozrát
<g/>
.	.	kIx.	.
</s>
<s>
Naprosto	naprosto	k6eAd1	naprosto
mu	on	k3xPp3gMnSc3	on
chyběla	chybět	k5eAaImAgFnS	chybět
cílevědomost	cílevědomost	k1gFnSc4	cílevědomost
a	a	k8xC	a
nesoustředil	soustředit	k5eNaPmAgMnS	soustředit
se	se	k3xPyFc4	se
dost	dost	k6eAd1	dost
na	na	k7c4	na
jasný	jasný	k2eAgInSc4d1	jasný
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tak	tak	k6eAd1	tak
mnohým	mnohý	k2eAgMnPc3d1	mnohý
lidem	člověk	k1gMnPc3	člověk
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
vybírat	vybírat	k5eAaImF	vybírat
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
skutečnostmi	skutečnost	k1gFnPc7	skutečnost
a	a	k8xC	a
cestami	cesta	k1gFnPc7	cesta
života	život	k1gInSc2	život
právě	právě	k6eAd1	právě
věc	věc	k1gFnSc4	věc
nebo	nebo	k8xC	nebo
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jim	on	k3xPp3gMnPc3	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
klubu	klub	k1gInSc6	klub
se	se	k3xPyFc4	se
náhodou	náhodou	k6eAd1	náhodou
ubytuje	ubytovat	k5eAaPmIp3nS	ubytovat
i	i	k9	i
Clydův	Clydův	k2eAgMnSc1d1	Clydův
strýc	strýc	k1gMnSc1	strýc
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
bohatým	bohatý	k2eAgMnSc7d1	bohatý
vlastníkem	vlastník	k1gMnSc7	vlastník
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
límce	límec	k1gInPc4	límec
<g/>
.	.	kIx.	.
</s>
<s>
Strýc	strýc	k1gMnSc1	strýc
zde	zde	k6eAd1	zde
potká	potkat	k5eAaPmIp3nS	potkat
Clyda	Clyda	k1gMnSc1	Clyda
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
hned	hned	k6eAd1	hned
zalíbí	zalíbit	k5eAaPmIp3nS	zalíbit
<g/>
,	,	kIx,	,
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
mu	on	k3xPp3gMnSc3	on
proto	proto	k6eAd1	proto
místo	místo	k6eAd1	místo
u	u	k7c2	u
sebe	se	k3xPyFc2	se
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Clyde	Clyde	k6eAd1	Clyde
nabídku	nabídka	k1gFnSc4	nabídka
příjme	příjem	k1gInSc5	příjem
a	a	k8xC	a
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
za	za	k7c7	za
strýcem	strýc	k1gMnSc7	strýc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
však	však	k9	však
nevrle	nevrle	k6eAd1	nevrle
uvítán	uvítat	k5eAaPmNgMnS	uvítat
svým	svůj	k3xOyFgMnSc7	svůj
bratrancem	bratranec	k1gMnSc7	bratranec
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
je	být	k5eAaImIp3nS	být
vzhledově	vzhledově	k6eAd1	vzhledově
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Bratranec	bratranec	k1gMnSc1	bratranec
je	být	k5eAaImIp3nS	být
však	však	k9	však
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
nadřízený	nadřízený	k1gMnSc1	nadřízený
a	a	k8xC	a
Clyde	Clyd	k1gInSc5	Clyd
je	být	k5eAaImIp3nS	být
nucen	nutit	k5eAaImNgMnS	nutit
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgMnPc4d1	ostatní
pracovníky	pracovník	k1gMnPc4	pracovník
nikdy	nikdy	k6eAd1	nikdy
nezapadl	zapadnout	k5eNaPmAgInS	zapadnout
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
jimi	on	k3xPp3gMnPc7	on
zčásti	zčásti	k6eAd1	zčásti
opovrhoval	opovrhovat	k5eAaImAgMnS	opovrhovat
<g/>
.	.	kIx.	.
</s>
<s>
Oni	onen	k3xDgMnPc1	onen
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
zase	zase	k9	zase
měli	mít	k5eAaImAgMnP	mít
respekt	respekt	k1gInSc4	respekt
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gMnSc3	jeho
strýci	strýc	k1gMnSc3	strýc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
On	on	k3xPp3gMnSc1	on
zase	zase	k9	zase
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
několika	několik	k4yIc6	několik
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
sedával	sedávat	k5eAaImAgMnS	sedávat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
místnosti	místnost	k1gFnSc6	místnost
a	a	k8xC	a
jedl	jíst	k5eAaImAgMnS	jíst
oběd	oběd	k1gInSc4	oběd
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
dobře	dobře	k6eAd1	dobře
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
mu	on	k3xPp3gMnSc3	on
připadají	připadat	k5eAaPmIp3nP	připadat
tak	tak	k9	tak
nudné	nudný	k2eAgNnSc1d1	nudné
a	a	k8xC	a
nezajímavé	zajímavý	k2eNgNnSc1d1	nezajímavé
-	-	kIx~	-
o	o	k7c4	o
jakost	jakost	k1gFnSc4	jakost
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sem	sem	k6eAd1	sem
přinášeli	přinášet	k5eAaImAgMnP	přinášet
ve	v	k7c6	v
svitcích	svitek	k1gInPc6	svitek
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
o	o	k7c4	o
jakési	jakýsi	k3yIgFnPc4	jakýsi
nepatrné	patrný	k2eNgFnPc4d1	patrný
chyby	chyba	k1gFnPc4	chyba
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
váze	váha	k1gFnSc6	váha
nebo	nebo	k8xC	nebo
osnově	osnova	k1gFnSc6	osnova
-	-	kIx~	-
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
posledních	poslední	k2eAgInPc2d1	poslední
dvacet	dvacet	k4xCc4	dvacet
svitků	svitek	k1gInPc2	svitek
nebylo	být	k5eNaImAgNnS	být
tak	tak	k9	tak
dobře	dobře	k6eAd1	dobře
sraženo	sražen	k2eAgNnSc1d1	sraženo
jako	jako	k8xC	jako
předchozích	předchozí	k2eAgInPc2d1	předchozí
šestnáct	šestnáct	k4xCc1	šestnáct
-	-	kIx~	-
nebo	nebo	k8xC	nebo
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cranstonova	Cranstonův	k2eAgFnSc1d1	Cranstonův
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
drátěných	drátěný	k2eAgNnPc2d1	drátěné
pletiv	pletivo	k1gNnPc2	pletivo
nezaměstnává	zaměstnávat	k5eNaImIp3nS	zaměstnávat
teď	teď	k6eAd1	teď
už	už	k9	už
tolik	tolik	k4xDc1	tolik
lidí	člověk	k1gMnPc2	člověk
jako	jako	k8xC	jako
před	před	k7c7	před
měsícem	měsíc	k1gInSc7	měsíc
-	-	kIx~	-
že	že	k8xS	že
Anthonyova	Anthonyův	k2eAgFnSc1d1	Anthonyův
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
zboží	zboží	k1gNnSc2	zboží
vylepila	vylepit	k5eAaPmAgFnS	vylepit
vyhlášku	vyhláška	k1gFnSc4	vyhláška
o	o	k7c6	o
sobotním	sobotní	k2eAgNnSc6d1	sobotní
odpoledním	odpolední	k2eAgNnSc6d1	odpolední
volnu	volno	k1gNnSc6	volno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
začne	začít	k5eAaPmIp3nS	začít
teprve	teprve	k6eAd1	teprve
prvního	první	k4xOgInSc2	první
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
loni	loni	k6eAd1	loni
začalo	začít	k5eAaPmAgNnS	začít
již	již	k6eAd1	již
uprostřed	uprostřed	k7c2	uprostřed
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
ztraceni	ztracen	k2eAgMnPc1d1	ztracen
v	v	k7c6	v
jednotvárnosti	jednotvárnost	k1gFnSc6	jednotvárnost
a	a	k8xC	a
navyklé	navyklý	k2eAgFnSc3d1	navyklá
pravidelnosti	pravidelnost	k1gFnSc3	pravidelnost
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
delší	dlouhý	k2eAgFnSc6d2	delší
době	doba	k1gFnSc6	doba
Clyda	Clydo	k1gNnSc2	Clydo
povýší	povýšit	k5eAaPmIp3nS	povýšit
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
vedoucího	vedoucí	k1gMnSc2	vedoucí
oddělní	oddělný	k2eAgMnPc1d1	oddělný
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
pracují	pracovat	k5eAaImIp3nP	pracovat
samé	samý	k3xTgFnPc4	samý
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Přijme	přijmout	k5eAaPmIp3nS	přijmout
sem	sem	k6eAd1	sem
novou	nový	k2eAgFnSc4d1	nová
dívku	dívka	k1gFnSc4	dívka
Robertu	Roberta	k1gFnSc4	Roberta
Aldenovou	Aldenová	k1gFnSc4	Aldenová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zalíbí	zalíbit	k5eAaPmIp3nS	zalíbit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c4	mezi
zaměstnaci	zaměstnace	k1gFnSc4	zaměstnace
firmy	firma	k1gFnSc2	firma
jsou	být	k5eAaImIp3nP	být
zakázané	zakázaný	k2eAgFnPc1d1	zakázaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náhodou	náhodou	k6eAd1	náhodou
ji	on	k3xPp3gFnSc4	on
jednou	jednou	k6eAd1	jednou
potká	potkat	k5eAaPmIp3nS	potkat
při	při	k7c6	při
projížďce	projížďka	k1gFnSc6	projížďka
na	na	k7c6	na
kánoi	kánoe	k1gFnSc6	kánoe
<g/>
.	.	kIx.	.
</s>
<s>
Sveze	svézt	k5eAaPmIp3nS	svézt
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
domluví	domluvit	k5eAaPmIp3nP	domluvit
si	se	k3xPyFc3	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
hned	hned	k6eAd1	hned
vyznají	vyznat	k5eAaBmIp3nP	vyznat
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Vyjedou	vyjet	k5eAaPmIp3nP	vyjet
si	se	k3xPyFc3	se
i	i	k9	i
na	na	k7c4	na
kratší	krátký	k2eAgInSc4d2	kratší
výlet	výlet	k1gInSc4	výlet
do	do	k7c2	do
zábavního	zábavní	k2eAgInSc2d1	zábavní
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
ovšem	ovšem	k9	ovšem
přijde	přijít	k5eAaPmIp3nS	přijít
spolubydlící	spolubydlící	k2eAgFnPc4d1	spolubydlící
Roberty	Roberta	k1gFnPc4	Roberta
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Roberta	Roberta	k1gFnSc1	Roberta
přestěhuje	přestěhovat	k5eAaPmIp3nS	přestěhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
už	už	k6eAd1	už
nemusela	muset	k5eNaImAgFnS	muset
poslouchat	poslouchat	k5eAaImF	poslouchat
svou	svůj	k3xOyFgFnSc4	svůj
spolubydlící	spolubydlící	k1gFnSc4	spolubydlící
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
schůzce	schůzka	k1gFnSc6	schůzka
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
hádat	hádat	k5eAaImF	hádat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Roberta	Robert	k1gMnSc4	Robert
nechce	chtít	k5eNaImIp3nS	chtít
pustit	pustit	k5eAaPmF	pustit
Clyda	Clydo	k1gNnPc4	Clydo
k	k	k7c3	k
sobě	se	k3xPyFc3	se
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gMnSc4	on
tam	tam	k6eAd1	tam
však	však	k9	však
pustí	pustit	k5eAaPmIp3nS	pustit
a	a	k8xC	a
"	"	kIx"	"
<g/>
vzdá	vzdát	k5eAaPmIp3nS	vzdát
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
potká	potkat	k5eAaPmIp3nS	potkat
i	i	k9	i
Sondru	Sondr	k1gInSc2	Sondr
Finchleovou	Finchleův	k2eAgFnSc7d1	Finchleův
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gNnSc4	on
sveze	svézt	k5eAaPmIp3nS	svézt
autem	auto	k1gNnSc7	auto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
spletla	splést	k5eAaPmAgFnS	splést
za	za	k7c2	za
jeho	jeho	k3xOp3gMnSc2	jeho
bratrance	bratranec	k1gMnSc2	bratranec
<g/>
.	.	kIx.	.
</s>
<s>
Sondra	Sondra	k1gFnSc1	Sondra
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgNnSc4d1	dobré
společenské	společenský	k2eAgNnSc4d1	společenské
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bratrancem	bratranec	k1gMnSc7	bratranec
Clyda	Clydo	k1gNnSc2	Clydo
však	však	k9	však
moc	moc	k6eAd1	moc
nevychází	vycházet	k5eNaImIp3nS	vycházet
a	a	k8xC	a
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
nemá	mít	k5eNaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
Clyda	Clyda	k1gMnSc1	Clyda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
bohatými	bohatý	k2eAgFnPc7d1	bohatá
dívkami	dívka	k1gFnPc7	dívka
domluví	domluvit	k5eAaPmIp3nP	domluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijmou	přijmout	k5eAaPmIp3nP	přijmout
Clyda	Clyd	k1gMnSc4	Clyd
mezi	mezi	k7c4	mezi
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
naštvaly	naštvat	k5eAaBmAgFnP	naštvat
jeho	on	k3xPp3gMnSc2	on
bratrance	bratranec	k1gMnSc2	bratranec
<g/>
.	.	kIx.	.
</s>
<s>
Clyde	Clyde	k6eAd1	Clyde
tak	tak	k9	tak
začne	začít	k5eAaPmIp3nS	začít
chodit	chodit	k5eAaImF	chodit
na	na	k7c4	na
spoustu	spousta	k1gFnSc4	spousta
večírků	večírek	k1gInPc2	večírek
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
stoupá	stoupat	k5eAaImIp3nS	stoupat
na	na	k7c6	na
společenském	společenský	k2eAgInSc6d1	společenský
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
doví	dovědět	k5eAaPmIp3nS	dovědět
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
a	a	k8xC	a
také	také	k9	také
ho	on	k3xPp3gMnSc4	on
pozve	pozvat	k5eAaPmIp3nS	pozvat
na	na	k7c4	na
večeři	večeře	k1gFnSc4	večeře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
nevypadalo	vypadat	k5eNaImAgNnS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zanedbává	zanedbávat	k5eAaImIp3nS	zanedbávat
svého	svůj	k3xOyFgMnSc4	svůj
synovce	synovec	k1gMnSc4	synovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
Clydovo	Clydův	k2eAgNnSc4d1	Clydův
zalíbení	zalíbení	k1gNnSc4	zalíbení
v	v	k7c6	v
Sondře	Sondra	k1gFnSc6	Sondra
<g/>
,	,	kIx,	,
chladne	chladnout	k5eAaImIp3nS	chladnout
jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
Robertě	Roberta	k1gFnSc3	Roberta
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
s	s	k7c7	s
Robertou	Roberta	k1gFnSc7	Roberta
nejspíš	nejspíš	k9	nejspíš
rozešel	rozejít	k5eAaPmAgMnS	rozejít
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gNnSc3	on
ona	onen	k3xDgFnSc1	onen
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těhotná	těhotný	k2eAgFnSc1d1	těhotná
a	a	k8xC	a
žádá	žádat	k5eAaImIp3nS	žádat
ho	on	k3xPp3gInSc4	on
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Clyde	Clyde	k6eAd1	Clyde
si	se	k3xPyFc3	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
neví	vědět	k5eNaImIp3nS	vědět
rady	rada	k1gFnPc4	rada
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
nalézt	nalézt	k5eAaPmF	nalézt
nějaký	nějaký	k3yIgInSc4	nějaký
lék	lék	k1gInSc4	lék
na	na	k7c4	na
potrat	potrat	k1gInSc4	potrat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
drogériích	drogérie	k1gFnPc6	drogérie
<g/>
.	.	kIx.	.
<g/>
Chodil	chodit	k5eAaImAgMnS	chodit
nahoru	nahoru	k6eAd1	nahoru
dolů	dolů	k6eAd1	dolů
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
Hlavní	hlavní	k2eAgFnSc6d1	hlavní
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
i	i	k9	i
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
hodinu	hodina	k1gFnSc4	hodina
jasně	jasně	k6eAd1	jasně
osvětlena	osvětlit	k5eAaPmNgFnS	osvětlit
<g/>
,	,	kIx,	,
nahlížel	nahlížet	k5eAaImAgInS	nahlížet
do	do	k7c2	do
výkladních	výkladní	k2eAgFnPc2d1	výkladní
skříní	skříň	k1gFnPc2	skříň
několika	několik	k4yIc2	několik
drogerií	drogerie	k1gFnPc2	drogerie
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
si	se	k3xPyFc3	se
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
pomyslel	pomyslet	k5eAaPmAgMnS	pomyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
tahle	tenhle	k3xDgFnSc1	tenhle
není	být	k5eNaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
pravá	pravý	k2eAgFnSc1d1	pravá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
zahlédl	zahlédnout	k5eAaPmAgMnS	zahlédnout
urostlého	urostlý	k2eAgInSc2d1	urostlý
<g/>
,	,	kIx,	,
rozvážného	rozvážný	k2eAgInSc2d1	rozvážný
<g/>
,	,	kIx,	,
hladce	hladko	k6eAd1	hladko
oholeného	oholený	k2eAgMnSc4d1	oholený
padesátiletého	padesátiletý	k2eAgMnSc4d1	padesátiletý
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgInSc2	jenž
oči	oko	k1gNnPc4	oko
za	za	k7c7	za
brýlemi	brýle	k1gFnPc7	brýle
a	a	k8xC	a
kovově	kovově	k6eAd1	kovově
šedé	šedý	k2eAgInPc4d1	šedý
vlasy	vlas	k1gInPc4	vlas
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
v	v	k7c6	v
Clydovi	Clyda	k1gMnSc6	Clyda
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
člověk	člověk	k1gMnSc1	člověk
by	by	kYmCp3nS	by
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
mladého	mladý	k2eAgMnSc4d1	mladý
zákazníka	zákazník	k1gMnSc4	zákazník
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
by	by	kYmCp3nS	by
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
nepřiznal	přiznat	k5eNaPmAgMnS	přiznat
by	by	kYmCp3nS	by
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
potřebný	potřebný	k2eAgInSc4d1	potřebný
lék	lék	k1gInSc4	lék
<g/>
,	,	kIx,	,
a	a	k8xC	a
podezříval	podezřívat	k5eAaImAgMnS	podezřívat
by	by	kYmCp3nS	by
ho	on	k3xPp3gMnSc4	on
navíc	navíc	k6eAd1	navíc
z	z	k7c2	z
nedovolených	dovolený	k2eNgInPc2d1	nedovolený
styků	styk	k1gInPc2	styk
s	s	k7c7	s
nějakou	nějaký	k3yIgFnSc7	nějaký
mladou	mladý	k2eAgFnSc7d1	mladá
neprovdanou	provdaný	k2eNgFnSc7d1	neprovdaná
dívkou	dívka	k1gFnSc7	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Vypadal	vypadat	k5eAaPmAgMnS	vypadat
tak	tak	k9	tak
rozvážně	rozvážně	k6eAd1	rozvážně
<g/>
,	,	kIx,	,
bohabojně	bohabojně	k6eAd1	bohabojně
<g/>
,	,	kIx,	,
nadmíru	nadmíru	k6eAd1	nadmíru
úctyhodně	úctyhodně	k6eAd1	úctyhodně
a	a	k8xC	a
morálně	morálně	k6eAd1	morálně
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
<g/>
,	,	kIx,	,
nemělo	mít	k5eNaImAgNnS	mít
by	by	kYmCp3nS	by
smysl	smysl	k1gInSc4	smysl
ptát	ptát	k5eAaImF	ptát
se	se	k3xPyFc4	se
tohoto	tento	k3xDgMnSc2	tento
drogisty	drogista	k1gMnSc2	drogista
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
odvahu	odvaha	k1gFnSc4	odvaha
vstoupit	vstoupit	k5eAaPmF	vstoupit
a	a	k8xC	a
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
takovému	takový	k3xDgMnSc3	takový
člověku	člověk	k1gMnSc3	člověk
tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
něco	něco	k3yInSc1	něco
sehnal	sehnat	k5eAaPmAgMnS	sehnat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezabralo	zabrat	k5eNaPmAgNnS	zabrat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
to	ten	k3xDgNnSc1	ten
Robertě	Roberta	k1gFnSc3	Roberta
přitížilo	přitížit	k5eAaPmAgNnS	přitížit
<g/>
.	.	kIx.	.
</s>
<s>
Domluvil	domluvit	k5eAaPmAgMnS	domluvit
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
sama	sám	k3xTgFnSc1	sám
zajde	zajít	k5eAaPmIp3nS	zajít
k	k	k7c3	k
doktorovi	doktor	k1gMnSc3	doktor
a	a	k8xC	a
požádá	požádat	k5eAaPmIp3nS	požádat
ho	on	k3xPp3gInSc4	on
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
ji	on	k3xPp3gFnSc4	on
však	však	k8xC	však
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Roberta	Robert	k1gMnSc4	Robert
proto	proto	k8xC	proto
začne	začít	k5eAaPmIp3nS	začít
Clyda	Clyda	k1gFnSc1	Clyda
přemlouvat	přemlouvat	k5eAaImF	přemlouvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
však	však	k9	však
odmítá	odmítat	k5eAaImIp3nS	odmítat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
chodí	chodit	k5eAaImIp3nP	chodit
na	na	k7c4	na
večírky	večírek	k1gInPc4	večírek
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
šanci	šance	k1gFnSc4	šance
oženit	oženit	k5eAaPmF	oženit
se	se	k3xPyFc4	se
se	s	k7c7	s
Sondrou	Sondra	k1gFnSc7	Sondra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Definitivní	definitivní	k2eAgNnSc1d1	definitivní
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
žanami	žana	k1gFnPc7	žana
učiní	učinit	k5eAaPmIp3nS	učinit
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
přečte	přečíst	k5eAaPmIp3nS	přečíst
dopisy	dopis	k1gInPc4	dopis
od	od	k7c2	od
každé	každý	k3xTgFnSc2	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Sondra	Sondra	k1gFnSc1	Sondra
mu	on	k3xPp3gMnSc3	on
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
samé	samý	k3xTgFnSc6	samý
zábavě	zábava	k1gFnSc6	zábava
<g/>
,	,	kIx,	,
večírcích	večírek	k1gInPc6	večírek
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
příjemných	příjemný	k2eAgFnPc6d1	příjemná
věcech	věc	k1gFnPc6	věc
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
Robertina	Robertin	k2eAgInSc2d1	Robertin
dopisu	dopis	k1gInSc2	dopis
je	být	k5eAaImIp3nS	být
cítit	cítit	k5eAaImF	cítit
zoufalost	zoufalost	k1gFnSc4	zoufalost
a	a	k8xC	a
smutek	smutek	k1gInSc4	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
si	se	k3xPyFc3	se
přečte	přečíst	k5eAaPmIp3nS	přečíst
článek	článek	k1gInSc4	článek
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
o	o	k7c6	o
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
utonuli	utonout	k5eAaPmAgMnP	utonout
dva	dva	k4xCgMnPc1	dva
milenci	milenec	k1gMnPc1	milenec
na	na	k7c6	na
loďkách	loďka	k1gFnPc6	loďka
<g/>
,	,	kIx,	,
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
ho	on	k3xPp3gMnSc4	on
napadane	napadanout	k5eAaPmIp3nS	napadanout
zavraždit	zavraždit	k5eAaPmF	zavraždit
Robertu	Roberta	k1gFnSc4	Roberta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ale	ale	k9	ale
pouhé	pouhý	k2eAgNnSc4d1	pouhé
pomyšlení	pomyšlení	k1gNnSc4	pomyšlení
na	na	k7c4	na
podobnou	podobný	k2eAgFnSc4d1	podobná
nehodu	nehoda	k1gFnSc4	nehoda
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Robertou	Roberta	k1gFnSc7	Roberta
(	(	kIx(	(
<g/>
proč	proč	k6eAd1	proč
ji	on	k3xPp3gFnSc4	on
jenom	jenom	k9	jenom
jeho	jeho	k3xOp3gFnSc4	jeho
mysl	mysl	k1gFnSc4	mysl
stále	stále	k6eAd1	stále
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojuje	spojovat	k5eAaImIp3nS	spojovat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
hrozné	hrozný	k2eAgNnSc1d1	hrozné
-	-	kIx~	-
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
připustit	připustit	k5eAaPmF	připustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
takové	takový	k3xDgFnSc6	takový
myšlenky	myšlenka	k1gFnPc1	myšlenka
napadaly	napadat	k5eAaImAgFnP	napadat
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
<g/>
!	!	kIx.	!
</s>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
děsivé	děsivý	k2eAgNnSc1d1	děsivé
<g/>
!	!	kIx.	!
</s>
<s>
Hrozné	hrozný	k2eAgNnSc1d1	hrozné
<g/>
!	!	kIx.	!
</s>
<s>
Vždyť	vždyť	k9	vždyť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přímo	přímo	k6eAd1	přímo
myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c4	na
vraždu	vražda	k1gFnSc4	vražda
<g/>
!	!	kIx.	!
</s>
<s>
Na	na	k7c4	na
vraždu	vražda	k1gFnSc4	vražda
<g/>
?!!!	?!!!	k?	?!!!
Ale	ale	k8xC	ale
tolik	tolik	k6eAd1	tolik
ho	on	k3xPp3gMnSc4	on
podráždil	podráždit	k5eAaPmAgMnS	podráždit
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dráždivě	dráždivě	k6eAd1	dráždivě
působil	působit	k5eAaImAgMnS	působit
onen	onen	k3xDgInSc4	onen
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
Roberty	Roberta	k1gFnSc2	Roberta
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
dopisem	dopis	k1gInSc7	dopis
Sondřiným	Sondřin	k2eAgInSc7d1	Sondřin
-	-	kIx~	-
tak	tak	k6eAd1	tak
rozkošný	rozkošný	k2eAgInSc1d1	rozkošný
a	a	k8xC	a
svůdný	svůdný	k2eAgInSc1d1	svůdný
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
Sondřina	Sondřin	k2eAgMnSc2d1	Sondřin
i	i	k8xC	i
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
Sondra	Sondra	k1gFnSc1	Sondra
popisovala	popisovat	k5eAaImAgFnS	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
nic	nic	k3yNnSc4	nic
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zbavit	zbavit	k5eAaPmF	zbavit
myšlenky	myšlenka	k1gFnSc2	myšlenka
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
snadné	snadný	k2eAgNnSc1d1	snadné
řešení	řešení	k1gNnSc1	řešení
celé	celý	k2eAgNnSc1d1	celé
své	svůj	k3xOyFgFnPc4	svůj
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
přirozené	přirozený	k2eAgNnSc1d1	přirozené
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
jemu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
Robertě	Roberta	k1gFnSc3	Roberta
stala	stát	k5eAaPmAgFnS	stát
taková	takový	k3xDgFnSc1	takový
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Vždyť	vždyť	k9	vždyť
přece	přece	k9	přece
nezamýšlí	zamýšlet	k5eNaImIp3nS	zamýšlet
žádný	žádný	k3yNgInSc4	žádný
zločin	zločin	k1gInSc4	zločin
<g/>
?	?	kIx.	?
</s>
<s>
Vždyť	vždyť	k9	vždyť
myslí	myslet	k5eAaImIp3nS	myslet
jen	jen	k9	jen
na	na	k7c4	na
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
stát	stát	k5eAaPmF	stát
<g/>
...	...	k?	...
Ach	ach	k0	ach
-	-	kIx~	-
právě	právě	k9	právě
tohle	tenhle	k3xDgNnSc1	tenhle
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
temný	temný	k2eAgInSc1d1	temný
a	a	k8xC	a
zlý	zlý	k2eAgInSc1d1	zlý
nápad	nápad	k1gInSc1	nápad
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
nesmí	smět	k5eNaImIp3nS	smět
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
myslet	myslet	k5eAaImF	myslet
<g/>
.	.	kIx.	.
</s>
<s>
NESMÍ	smět	k5eNaImIp3nS	smět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přece	přece	k9	přece
-	-	kIx~	-
a	a	k8xC	a
přece	přece	k9	přece
<g/>
...	...	k?	...
Umí	umět	k5eAaImIp3nS	umět
skvěle	skvěle	k6eAd1	skvěle
plavat	plavat	k5eAaImF	plavat
a	a	k8xC	a
doplaval	doplavat	k5eAaPmAgMnS	doplavat
by	by	kYmCp3nS	by
nepochybně	pochybně	k6eNd1	pochybně
ke	k	k7c3	k
břehu	břeh	k1gInSc3	břeh
-	-	kIx~	-
z	z	k7c2	z
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Kdežto	kdežto	k8xS	kdežto
Roberta	Robert	k1gMnSc4	Robert
neumí	umět	k5eNaImIp3nS	umět
plavat	plavat	k5eAaImF	plavat
<g/>
,	,	kIx,	,
poznal	poznat	k5eAaPmAgMnS	poznat
to	ten	k3xDgNnSc4	ten
loni	loni	k6eAd1	loni
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
koupával	koupávat	k5eAaImAgInS	koupávat
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
pobřežích	pobřeží	k1gNnPc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pak	pak	k6eAd1	pak
-	-	kIx~	-
a	a	k8xC	a
pak	pak	k6eAd1	pak
-	-	kIx~	-
pak	pak	k6eAd1	pak
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
jí	on	k3xPp3gFnSc3	on
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nechtěl	chtít	k5eNaImAgMnS	chtít
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Clyde	Clyde	k6eAd1	Clyde
vezme	vzít	k5eAaPmIp3nS	vzít
Robertu	Roberta	k1gFnSc4	Roberta
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
na	na	k7c4	na
kánoi	kánoe	k1gFnSc4	kánoe
dopluje	doplout	k5eAaPmIp3nS	doplout
k	k	k7c3	k
zapadlé	zapadlý	k2eAgFnSc3d1	zapadlá
zátoce	zátoka	k1gFnSc3	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
zavraždit	zavraždit	k5eAaPmF	zavraždit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
viděl	vidět	k5eAaImAgInS	vidět
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
líto	líto	k6eAd1	líto
<g/>
,	,	kIx,	,
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
rozumně	rozumně	k6eAd1	rozumně
rozejít	rozejít	k5eAaPmF	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Roberta	Robert	k1gMnSc2	Robert
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
naklonila	naklonit	k5eAaPmAgFnS	naklonit
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
neúmyslně	úmyslně	k6eNd1	úmyslně
udeřil	udeřit	k5eAaPmAgInS	udeřit
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Omráčil	omráčit	k5eAaPmAgMnS	omráčit
ji	on	k3xPp3gFnSc4	on
tak	tak	k6eAd1	tak
a	a	k8xC	a
převrátil	převrátit	k5eAaPmAgMnS	převrátit
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Roberta	Roberta	k1gFnSc1	Roberta
neschopná	schopný	k2eNgFnSc1d1	neschopná
plavat	plavat	k5eAaImF	plavat
se	se	k3xPyFc4	se
topí	topit	k5eAaImIp3nS	topit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Clyde	Clyd	k1gInSc5	Clyd
plave	plavat	k5eAaImIp3nS	plavat
ke	k	k7c3	k
břehu	břeh	k1gInSc3	břeh
neochoten	ochoten	k2eNgInSc1d1	neochoten
ji	on	k3xPp3gFnSc4	on
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hlavě	hlava	k1gFnSc6	hlava
to	ten	k3xDgNnSc1	ten
celé	celý	k2eAgNnSc1d1	celé
bere	brát	k5eAaImIp3nS	brát
jako	jako	k9	jako
nehodu	nehoda	k1gFnSc4	nehoda
a	a	k8xC	a
ne	ne	k9	ne
vraždu	vražda	k1gFnSc4	vražda
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kapitola	kapitola	k1gFnSc1	kapitola
III	III	kA	III
===	===	k?	===
</s>
</p>
<p>
<s>
Policie	policie	k1gFnSc1	policie
se	se	k3xPyFc4	se
doví	dovědět	k5eAaPmIp3nS	dovědět
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
utonutých	utonutý	k2eAgFnPc6d1	utonutý
<g/>
,	,	kIx,	,
naleznou	nalézt	k5eAaBmIp3nP	nalézt
však	však	k9	však
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
tělo	tělo	k1gNnSc1	tělo
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
těhotnou	těhotná	k1gFnSc4	těhotná
ženu	hnát	k5eAaImIp1nS	hnát
s	s	k7c7	s
ránou	rána	k1gFnSc7	rána
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kabátě	kabát	k1gInSc6	kabát
Roberty	Roberta	k1gFnSc2	Roberta
naleznou	naleznout	k5eAaPmIp3nP	naleznout
věnování	věnování	k1gNnSc4	věnování
od	od	k7c2	od
Clyda	Clydo	k1gNnSc2	Clydo
Griffithse	Griffithse	k1gFnSc2	Griffithse
a	a	k8xC	a
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
doma	doma	k6eAd1	doma
poté	poté	k6eAd1	poté
objeví	objevit	k5eAaPmIp3nS	objevit
dopisy	dopis	k1gInPc4	dopis
od	od	k7c2	od
Roberty	Roberta	k1gFnSc2	Roberta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
usvědčující	usvědčující	k2eAgInSc4d1	usvědčující
důkaz	důkaz	k1gInSc4	důkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
si	se	k3xPyFc3	se
Clyde	Clyd	k1gInSc5	Clyd
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
Sondoru	Sondor	k1gMnSc3	Sondor
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
trápí	trápit	k5eAaImIp3nS	trápit
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
myšlenky	myšlenka	k1gFnPc1	myšlenka
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
dopadení	dopadení	k1gNnSc6	dopadení
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gMnSc4	on
zatknout	zatknout	k5eAaPmF	zatknout
a	a	k8xC	a
když	když	k8xS	když
před	před	k7c4	před
něj	on	k3xPp3gMnSc4	on
doloží	doložit	k5eAaPmIp3nS	doložit
důkazy	důkaz	k1gInPc7	důkaz
<g/>
,	,	kIx,	,
přizná	přiznat	k5eAaPmIp3nS	přiznat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vylovili	vylovit	k5eAaPmAgMnP	vylovit
také	také	k9	také
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
a	a	k8xC	a
podstrčili	podstrčit	k5eAaPmAgMnP	podstrčit
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
vlasy	vlas	k1gInPc1	vlas
Roberty	Roberta	k1gFnSc2	Roberta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gMnSc4	on
mohly	moct	k5eAaImAgInP	moct
použít	použít	k5eAaPmF	použít
proti	proti	k7c3	proti
Clydovi	Clyda	k1gMnSc3	Clyda
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
obecné	obecný	k2eAgNnSc4d1	obecné
pobouření	pobouření	k1gNnSc4	pobouření
proti	proti	k7c3	proti
Clydovi	Clyda	k1gMnSc3	Clyda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
mu	on	k3xPp3gMnSc3	on
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
právníky	právník	k1gMnPc4	právník
Belknopa	Belknop	k1gMnSc4	Belknop
a	a	k8xC	a
Jephsona	Jephson	k1gMnSc4	Jephson
a	a	k8xC	a
Clyde	Clyd	k1gInSc5	Clyd
jim	on	k3xPp3gMnPc3	on
řekne	říct	k5eAaPmIp3nS	říct
vše	všechen	k3xTgNnSc1	všechen
popravdě	popravdě	k6eAd1	popravdě
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
nesmí	smět	k5eNaImIp3nS	smět
říct	říct	k5eAaPmF	říct
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
zaujatou	zaujatý	k2eAgFnSc7d1	zaujatá
<g/>
,	,	kIx,	,
konzervativní	konzervativní	k2eAgFnSc7d1	konzervativní
porotou	porota	k1gFnSc7	porota
<g/>
.	.	kIx.	.
</s>
<s>
Přijdou	přijít	k5eAaPmIp3nP	přijít
tedy	tedy	k9	tedy
s	s	k7c7	s
vymyšlenou	vymyšlený	k2eAgFnSc7d1	vymyšlená
obhajobou	obhajoba	k1gFnSc7	obhajoba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
Clyde	Clyd	k1gInSc5	Clyd
učí	učit	k5eAaImIp3nP	učit
nazpaměť	nazpaměť	k6eAd1	nazpaměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
začátku	začátek	k1gInSc6	začátek
přelíčení	přelíčení	k1gNnSc2	přelíčení
mluví	mluvit	k5eAaImIp3nP	mluvit
Mason	mason	k1gMnSc1	mason
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
odhodlaný	odhodlaný	k2eAgMnSc1d1	odhodlaný
dostat	dostat	k5eAaPmF	dostat
Clyda	Clydo	k1gNnPc4	Clydo
na	na	k7c4	na
elektrické	elektrický	k2eAgNnSc4d1	elektrické
křeslo	křeslo	k1gNnSc4	křeslo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
přilepšil	přilepšit	k5eAaPmAgMnS	přilepšit
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
postavení	postavení	k1gNnSc6	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Předloží	předložit	k5eAaPmIp3nS	předložit
veškeré	veškerý	k3xTgInPc4	veškerý
důkazy	důkaz	k1gInPc4	důkaz
a	a	k8xC	a
přijde	přijít	k5eAaPmIp3nS	přijít
spoustu	spousta	k1gFnSc4	spousta
svědků	svědek	k1gMnPc2	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Clyde	Clyd	k1gInSc5	Clyd
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
svůj	svůj	k3xOyFgInSc4	svůj
poupravený	poupravený	k2eAgInSc4d1	poupravený
příběh	příběh	k1gInSc4	příběh
před	před	k7c7	před
soudcem	soudce	k1gMnSc7	soudce
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
Masonovi	mason	k1gMnSc3	mason
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Chytil	Chytil	k1gMnSc1	Chytil
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
prospekty	prospekt	k1gInPc4	prospekt
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgFnPc6	který
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
vyzvedl	vyzvednout	k5eAaPmAgMnS	vyzvednout
někde	někde	k6eAd1	někde
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
skutečně	skutečně	k6eAd1	skutečně
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Clyde	Clyde	k6eAd1	Clyde
byl	být	k5eAaImAgInS	být
porotou	porota	k1gFnSc7	porota
shledán	shledat	k5eAaPmNgMnS	shledat
vinným	vinný	k1gMnSc7	vinný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
vezení	vezení	k1gNnPc2	vezení
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
přijde	přijít	k5eAaPmIp3nS	přijít
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
kazatel	kazatel	k1gMnSc1	kazatel
Duncan	Duncan	k1gMnSc1	Duncan
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
Clyde	Clyd	k1gInSc5	Clyd
vše	všechen	k3xTgNnSc1	všechen
řekne	říct	k5eAaPmIp3nS	říct
popravdě	popravdě	k6eAd1	popravdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Guvernér	guvernér	k1gMnSc1	guvernér
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
další	další	k2eAgInSc4d1	další
soud	soud	k1gInSc4	soud
s	s	k7c7	s
Clydem	Clyd	k1gInSc7	Clyd
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Clyda	Clyda	k1gFnSc1	Clyda
čeká	čekat	k5eAaImIp3nS	čekat
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
popraven	popravit	k5eAaPmNgMnS	popravit
na	na	k7c6	na
elektrickém	elektrický	k2eAgNnSc6d1	elektrické
křesle	křeslo	k1gNnSc6	křeslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Stanislav	Stanislav	k1gMnSc1	Stanislav
Párnický	Párnický	k2eAgMnSc1d1	Párnický
třídílnou	třídílný	k2eAgFnSc4d1	třídílná
filmovou	filmový	k2eAgFnSc4d1	filmová
adaptaci	adaptace	k1gFnSc4	adaptace
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Soňa	Soňa	k1gFnSc1	Soňa
Valentová	Valentová	k1gFnSc1	Valentová
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Horváth	Horváth	k1gMnSc1	Horváth
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americkou	americký	k2eAgFnSc7d1	americká
tragédií	tragédie	k1gFnSc7	tragédie
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
inspirován	inspirován	k2eAgInSc1d1	inspirován
film	film	k1gInSc1	film
Woodyho	Woody	k1gMnSc2	Woody
Allena	Allen	k1gMnSc2	Allen
Match	Match	k1gMnSc1	Match
Point	pointa	k1gFnPc2	pointa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americkou	americký	k2eAgFnSc7d1	americká
tragédií	tragédie	k1gFnSc7	tragédie
je	být	k5eAaImIp3nS	být
i	i	k9	i
inspirován	inspirován	k2eAgInSc1d1	inspirován
film	film	k1gInSc1	film
Georga	Georg	k1gMnSc2	Georg
Stevense	Stevens	k1gMnSc2	Stevens
A	a	k8xC	a
Place	plac	k1gInSc6	plac
in	in	k?	in
the	the	k?	the
Sun	sun	k1gInSc1	sun
s	s	k7c7	s
Liz	liz	k1gInSc4	liz
Taylor	Taylora	k1gFnPc2	Taylora
a	a	k8xC	a
Montgomery	Montgomera	k1gFnSc2	Montgomera
Cliftem	Clifto	k1gNnSc7	Clifto
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Americká	americký	k2eAgFnSc1d1	americká
tragédie	tragédie	k1gFnSc1	tragédie
</s>
</p>
