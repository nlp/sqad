<s>
Americká	americký	k2eAgFnSc1d1	americká
tragédie	tragédie	k1gFnSc1	tragédie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
An	An	k1gFnSc1	An
American	Americany	k1gInPc2	Americany
Tragedy	Trageda	k1gMnSc2	Trageda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Theodora	Theodor	k1gMnSc2	Theodor
Dreisera	Dreiser	k1gMnSc2	Dreiser
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
