<s>
Železniční	železniční	k2eAgInSc1d1	železniční
vůz	vůz	k1gInSc1	vůz
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
kolejové	kolejový	k2eAgNnSc4d1	kolejové
vozidlo	vozidlo	k1gNnSc4	vozidlo
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
železnici	železnice	k1gFnSc4	železnice
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
<g/>
.	.	kIx.	.
tažené	tažený	k2eAgNnSc1d1	tažené
železniční	železniční	k2eAgNnSc1d1	železniční
vozidlo	vozidlo	k1gNnSc1	vozidlo
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vlastního	vlastní	k2eAgInSc2d1	vlastní
pohonu	pohon	k1gInSc2	pohon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
vagon	vagon	k1gInSc4	vagon
(	(	kIx(	(
<g/>
vagón	vagón	k1gInSc1	vagón
<g/>
)	)	kIx)	)
hnací	hnací	k2eAgNnSc1d1	hnací
vozidlo	vozidlo	k1gNnSc1	vozidlo
(	(	kIx(	(
<g/>
vozidlo	vozidlo	k1gNnSc1	vozidlo
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
pohonem	pohon	k1gInSc7	pohon
<g/>
)	)	kIx)	)
určené	určený	k2eAgFnPc4d1	určená
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
tažení	tažení	k1gNnSc2	tažení
dalších	další	k2eAgNnPc2d1	další
železničních	železniční	k2eAgNnPc2d1	železniční
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
posledně	posledně	k6eAd1	posledně
jmenované	jmenovaný	k2eAgNnSc1d1	jmenované
není	být	k5eNaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
vozidlo	vozidlo	k1gNnSc1	vozidlo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
motorový	motorový	k2eAgInSc1d1	motorový
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc1d1	elektrický
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
parní	parní	k2eAgInSc1d1	parní
vůz	vůz	k1gInSc1	vůz
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
použitého	použitý	k2eAgInSc2d1	použitý
pohonu	pohon	k1gInSc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stavby	stavba	k1gFnSc2	stavba
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
vozy	vůz	k1gInPc7	vůz
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
vozy	vůz	k1gInPc1	vůz
osobní	osobní	k2eAgInPc1d1	osobní
vozy	vůz	k1gInPc1	vůz
nákladní	nákladní	k2eAgInPc1d1	nákladní
Podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
jsou	být	k5eAaImIp3nP	být
rozlišovány	rozlišován	k2eAgInPc1d1	rozlišován
vozy	vůz	k1gInPc1	vůz
<g/>
:	:	kIx,	:
osobní	osobní	k2eAgInSc1d1	osobní
<g/>
,	,	kIx,	,
oddílové	oddílový	k2eAgFnSc2d1	oddílová
<g/>
,	,	kIx,	,
velkoprostorové	velkoprostorový	k2eAgFnSc2d1	velkoprostorová
<g/>
,	,	kIx,	,
lehátkové	lehátkový	k2eAgFnSc2d1	lehátková
<g/>
,	,	kIx,	,
lůžkové	lůžkový	k2eAgFnSc2d1	lůžková
<g/>
,	,	kIx,	,
restaurační	restaurační	k2eAgFnSc2d1	restaurační
<g/>
,	,	kIx,	,
služební	služební	k2eAgFnSc2d1	služební
<g/>
,	,	kIx,	,
poštovní	poštovní	k2eAgMnSc1d1	poštovní
<g/>
,	,	kIx,	,
s	s	k7c7	s
kombinovanými	kombinovaný	k2eAgFnPc7d1	kombinovaná
funkcemi	funkce	k1gFnPc7	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
osobní	osobní	k2eAgFnSc2d1	osobní
a	a	k8xC	a
služební	služební	k2eAgFnSc2d1	služební
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vozy	vůz	k1gInPc1	vůz
se	s	k7c7	s
stanovištěm	stanoviště	k1gNnSc7	stanoviště
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgMnSc2	který
lze	lze	k6eAd1	lze
ovládat	ovládat	k5eAaImF	ovládat
hnací	hnací	k2eAgNnSc4d1	hnací
vozidlo	vozidlo	k1gNnSc4	vozidlo
(	(	kIx(	(
<g/>
řídicí	řídicí	k2eAgInPc4d1	řídicí
vozy	vůz	k1gInPc4	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInSc1d1	speciální
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
stavěné	stavěný	k2eAgInPc1d1	stavěný
osobní	osobní	k2eAgInPc1d1	osobní
vozy	vůz	k1gInPc1	vůz
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
těmito	tento	k3xDgFnPc7	tento
konstrukčními	konstrukční	k2eAgFnPc7d1	konstrukční
charakteristikami	charakteristika	k1gFnPc7	charakteristika
<g/>
:	:	kIx,	:
samonosnou	samonosný	k2eAgFnSc7d1	samonosná
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
nosnou	nosný	k2eAgFnSc7d1	nosná
vozovou	vozový	k2eAgFnSc7d1	vozová
skříní	skříň	k1gFnSc7	skříň
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
nebo	nebo	k8xC	nebo
slitiny	slitina	k1gFnSc2	slitina
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
mezivozovými	mezivozův	k2eAgInPc7d1	mezivozův
přechody	přechod	k1gInPc7	přechod
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgInPc7	dva
dvounápravovými	dvounápravový	k2eAgInPc7d1	dvounápravový
podvozky	podvozek	k1gInPc7	podvozek
s	s	k7c7	s
dvojstupňovým	dvojstupňový	k2eAgNnSc7d1	dvojstupňové
vypružením	vypružení	k1gNnSc7	vypružení
a	a	k8xC	a
s	s	k7c7	s
hydraulickými	hydraulický	k2eAgInPc7d1	hydraulický
tlumiči	tlumič	k1gInPc7	tlumič
kmitů	kmit	k1gInPc2	kmit
<g/>
,	,	kIx,	,
s	s	k7c7	s
pryžovými	pryžový	k2eAgInPc7d1	pryžový
prvky	prvek	k1gInPc7	prvek
k	k	k7c3	k
tlumení	tlumení	k1gNnSc3	tlumení
hluku	hluk	k1gInSc2	hluk
a	a	k8xC	a
vibrací	vibrace	k1gFnSc7	vibrace
<g/>
,	,	kIx,	,
bezrozsochovým	bezrozsochův	k2eAgNnSc7d1	bezrozsochův
vedením	vedení	k1gNnSc7	vedení
dvojkolí	dvojkolí	k1gNnSc2	dvojkolí
a	a	k8xC	a
kotoučovými	kotoučový	k2eAgFnPc7d1	kotoučová
brzdami	brzda	k1gFnPc7	brzda
<g/>
,	,	kIx,	,
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
sítí	síť	k1gFnSc7	síť
s	s	k7c7	s
elektronickým	elektronický	k2eAgInSc7d1	elektronický
zdrojem	zdroj	k1gInSc7	zdroj
napájeným	napájený	k2eAgInSc7d1	napájený
z	z	k7c2	z
hnacího	hnací	k2eAgNnSc2d1	hnací
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
ještě	ještě	k6eAd1	ještě
s	s	k7c7	s
alternátorem	alternátor	k1gInSc7	alternátor
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
vozy	vůz	k1gInPc7	vůz
lehké	lehký	k2eAgFnSc2d1	lehká
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
přívěsné	přívěsný	k2eAgNnSc4d1	přívěsné
k	k	k7c3	k
motorovým	motorový	k2eAgInPc3d1	motorový
nebo	nebo	k8xC	nebo
elektrickým	elektrický	k2eAgInPc3d1	elektrický
vozům	vůz	k1gInPc3	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
omezení	omezení	k1gNnSc3	omezení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
skříň	skříň	k1gFnSc1	skříň
stavěná	stavěný	k2eAgFnSc1d1	stavěná
na	na	k7c4	na
přenos	přenos	k1gInSc4	přenos
menších	malý	k2eAgFnPc2d2	menší
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
také	také	k9	také
jejich	jejich	k3xOp3gNnSc1	jejich
spřáhlo	spřáhlo	k1gNnSc1	spřáhlo
bývá	bývat	k5eAaImIp3nS	bývat
drobnější	drobný	k2eAgNnSc1d2	drobnější
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
opatřeny	opatřen	k2eAgInPc1d1	opatřen
nárazníky	nárazník	k1gInPc1	nárazník
a	a	k8xC	a
šroubovkou	šroubovka	k1gFnSc7	šroubovka
(	(	kIx(	(
<g/>
také	také	k6eAd1	také
odlehčenou	odlehčený	k2eAgFnSc4d1	odlehčená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ČR	ČR	kA	ČR
označovány	označovat	k5eAaImNgInP	označovat
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Vůz	vůz	k1gInSc1	vůz
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
jen	jen	k9	jen
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vlaku	vlak	k1gInSc2	vlak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
nákladní	nákladní	k2eAgInPc1d1	nákladní
bývají	bývat	k5eAaImIp3nP	bývat
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
přepravovaným	přepravovaný	k2eAgInPc3d1	přepravovaný
nákladům	náklad	k1gInPc3	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
dvounápravové	dvounápravový	k2eAgInPc1d1	dvounápravový
nebo	nebo	k8xC	nebo
podvozkové	podvozkový	k2eAgInPc1d1	podvozkový
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
dvounápravovými	dvounápravový	k2eAgInPc7d1	dvounápravový
podvozky	podvozek	k1gInPc7	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
stavěny	stavit	k5eAaImNgInP	stavit
na	na	k7c6	na
nižší	nízký	k2eAgFnSc6d2	nižší
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
jednostupňovým	jednostupňový	k2eAgNnSc7d1	jednostupňové
vypružením	vypružení	k1gNnSc7	vypružení
<g/>
,	,	kIx,	,
rozsochovým	rozsochův	k2eAgNnSc7d1	rozsochův
vedením	vedení	k1gNnSc7	vedení
dvojkolí	dvojkolí	k1gNnSc2	dvojkolí
<g/>
,	,	kIx,	,
ocelovými	ocelový	k2eAgFnPc7d1	ocelová
pružinami	pružina	k1gFnPc7	pružina
nebo	nebo	k8xC	nebo
pružnicemi	pružnice	k1gFnPc7	pružnice
a	a	k8xC	a
špalíkovou	špalíkový	k2eAgFnSc7d1	špalíková
brzdou	brzda	k1gFnSc7	brzda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
konkurenceschopnosti	konkurenceschopnost	k1gFnSc2	konkurenceschopnost
<g/>
)	)	kIx)	)
nákladních	nákladní	k2eAgInPc2d1	nákladní
vlaků	vlak	k1gInPc2	vlak
začínají	začínat	k5eAaImIp3nP	začínat
prosazovat	prosazovat	k5eAaImF	prosazovat
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
známé	známý	k2eAgInPc4d1	známý
z	z	k7c2	z
vozů	vůz	k1gInPc2	vůz
osobní	osobní	k2eAgFnSc2d1	osobní
stavby	stavba	k1gFnSc2	stavba
-	-	kIx~	-
hydraulické	hydraulický	k2eAgInPc1d1	hydraulický
tlumiče	tlumič	k1gInPc1	tlumič
<g/>
,	,	kIx,	,
kotoučové	kotoučový	k2eAgFnPc1d1	kotoučová
brzdy	brzda	k1gFnPc1	brzda
<g/>
,	,	kIx,	,
bezrozsochové	bezrozsochový	k2eAgNnSc1d1	bezrozsochový
vedení	vedení	k1gNnSc1	vedení
dvojkolí	dvojkolí	k1gNnSc2	dvojkolí
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Držitelé	držitel	k1gMnPc1	držitel
vozů	vůz	k1gInPc2	vůz
je	on	k3xPp3gNnSc4	on
obvykle	obvykle	k6eAd1	obvykle
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
očíslováním	očíslování	k1gNnSc7	očíslování
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
provozovateli	provozovatel	k1gMnSc3	provozovatel
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
vozy	vůz	k1gInPc1	vůz
mají	mít	k5eAaImIp3nP	mít
jezdit	jezdit	k5eAaImF	jezdit
po	po	k7c6	po
drahách	draha	k1gFnPc6	draha
více	hodně	k6eAd2	hodně
různých	různý	k2eAgMnPc2d1	různý
provozovatelů	provozovatel	k1gMnPc2	provozovatel
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
v	v	k7c6	v
mezistátním	mezistátní	k2eAgInSc6d1	mezistátní
provozu	provoz	k1gInSc6	provoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
nutnost	nutnost	k1gFnSc1	nutnost
definovat	definovat	k5eAaBmF	definovat
číslovací	číslovací	k2eAgInSc4d1	číslovací
systém	systém	k1gInSc4	systém
úmluvou	úmluva	k1gFnSc7	úmluva
(	(	kIx(	(
<g/>
či	či	k8xC	či
mocenským	mocenský	k2eAgNnSc7d1	mocenské
nařízením	nařízení	k1gNnSc7	nařízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgFnPc1d1	Evropská
železniční	železniční	k2eAgFnPc1d1	železniční
správy	správa	k1gFnPc1	správa
sdružené	sdružený	k2eAgFnPc1d1	sdružená
v	v	k7c6	v
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
železniční	železniční	k2eAgFnSc6d1	železniční
unii	unie	k1gFnSc6	unie
(	(	kIx(	(
<g/>
UIC	UIC	kA	UIC
<g/>
)	)	kIx)	)
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zavedly	zavést	k5eAaPmAgFnP	zavést
(	(	kIx(	(
<g/>
nejprve	nejprve	k6eAd1	nejprve
pro	pro	k7c4	pro
normálněrozchodné	normálněrozchodný	k2eAgInPc4d1	normálněrozchodný
vozy	vůz	k1gInPc4	vůz
<g/>
)	)	kIx)	)
jednotné	jednotný	k2eAgNnSc4d1	jednotné
značení	značení	k1gNnSc4	značení
pomocí	pomoc	k1gFnPc2	pomoc
dvanáctimístných	dvanáctimístný	k2eAgNnPc2d1	dvanáctimístné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Úmluvy	úmluva	k1gFnPc1	úmluva
také	také	k9	také
určují	určovat	k5eAaImIp3nP	určovat
-	-	kIx~	-
vedle	vedle	k7c2	vedle
způsobu	způsob	k1gInSc2	způsob
zacházení	zacházení	k1gNnPc2	zacházení
s	s	k7c7	s
cizími	cizí	k2eAgInPc7d1	cizí
vozy	vůz	k1gInPc7	vůz
a	a	k8xC	a
účtování	účtování	k1gNnSc1	účtování
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c4	za
jejich	jejich	k3xOp3gNnSc4	jejich
používání	používání	k1gNnSc4	používání
-	-	kIx~	-
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
rozsah	rozsah	k1gInSc1	rozsah
<g/>
,	,	kIx,	,
vzhled	vzhled	k1gInSc1	vzhled
a	a	k8xC	a
umístění	umístění	k1gNnSc1	umístění
nápisů	nápis	k1gInPc2	nápis
a	a	k8xC	a
značek	značka	k1gFnPc2	značka
na	na	k7c6	na
vozech	vůz	k1gInPc6	vůz
<g/>
,	,	kIx,	,
informujících	informující	k2eAgNnPc6d1	informující
o	o	k7c6	o
jejich	jejich	k3xOp3gInPc6	jejich
technických	technický	k2eAgInPc6d1	technický
a	a	k8xC	a
provozních	provozní	k2eAgInPc6d1	provozní
parametrech	parametr	k1gInPc6	parametr
a	a	k8xC	a
zvláštnostech	zvláštnost	k1gFnPc6	zvláštnost
ovládání	ovládání	k1gNnSc2	ovládání
a	a	k8xC	a
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgInPc1d1	důležitý
údaje	údaj	k1gInPc1	údaj
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
vozech	vůz	k1gInPc6	vůz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vozy	vůz	k1gInPc1	vůz
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
např.	např.	kA	např.
kontejnery	kontejner	k1gInPc4	kontejner
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
automobilů	automobil	k1gInPc2	automobil
<g/>
)	)	kIx)	)
nejsou	být	k5eNaImIp3nP	být
doprovázeny	doprovázet	k5eAaImNgFnP	doprovázet
obsluhou	obsluha	k1gFnSc7	obsluha
<g/>
.	.	kIx.	.
</s>
