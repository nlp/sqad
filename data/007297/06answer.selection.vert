<s>
Železniční	železniční	k2eAgInSc1d1	železniční
vůz	vůz	k1gInSc1	vůz
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
kolejové	kolejový	k2eAgNnSc4d1	kolejové
vozidlo	vozidlo	k1gNnSc4	vozidlo
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
železnici	železnice	k1gFnSc4	železnice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
není	být	k5eNaImIp3nS	být
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
<g/>
.	.	kIx.	.
tažené	tažený	k2eAgNnSc1d1	tažené
železniční	železniční	k2eAgNnSc1d1	železniční
vozidlo	vozidlo	k1gNnSc1	vozidlo
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vlastního	vlastní	k2eAgInSc2d1	vlastní
pohonu	pohon	k1gInSc2	pohon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
vagon	vagon	k1gInSc4	vagon
(	(	kIx(	(
<g/>
vagón	vagón	k1gInSc1	vagón
<g/>
)	)	kIx)	)
hnací	hnací	k2eAgNnSc1d1	hnací
vozidlo	vozidlo	k1gNnSc1	vozidlo
(	(	kIx(	(
<g/>
vozidlo	vozidlo	k1gNnSc1	vozidlo
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
pohonem	pohon	k1gInSc7	pohon
<g/>
)	)	kIx)	)
určené	určený	k2eAgFnPc4d1	určená
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
tažení	tažení	k1gNnSc2	tažení
dalších	další	k2eAgNnPc2d1	další
železničních	železniční	k2eAgNnPc2d1	železniční
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
posledně	posledně	k6eAd1	posledně
jmenované	jmenovaný	k2eAgNnSc1d1	jmenované
není	být	k5eNaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
