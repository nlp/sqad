<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Halama	Halama	k1gMnSc1	Halama
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hráč	hráč	k1gMnSc1	hráč
klubu	klub	k1gInSc2	klub
FK	FK	kA	FK
Bohemians	Bohemians	k1gInSc1	Bohemians
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hostuje	hostovat	k5eAaImIp3nS	hostovat
z	z	k7c2	z
Dynama	dynamo	k1gNnSc2	dynamo
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
kariéru	kariéra	k1gFnSc4	kariéra
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Jablonci	Jablonec	k1gInSc6	Jablonec
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
a	a	k8xC	a
v	v	k7c6	v
dorostu	dorost	k1gInSc6	dorost
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
stal	stát	k5eAaPmAgMnS	stát
stoper	stoper	k1gMnSc1	stoper
i	i	k9	i
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vyspělou	vyspělý	k2eAgFnSc4d1	vyspělá
postavu	postava	k1gFnSc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
FK	FK	kA	FK
Jablonec	Jablonec	k1gInSc1	Jablonec
působil	působit	k5eAaImAgInS	působit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
vyhlédla	vyhlédnout	k5eAaPmAgFnS	vyhlédnout
pražská	pražský	k2eAgFnSc1d1	Pražská
Sparta	Sparta	k1gFnSc1	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
s	s	k7c7	s
ročníkem	ročník	k1gInSc7	ročník
1987	[number]	k4	1987
získal	získat	k5eAaPmAgInS	získat
mistrovský	mistrovský	k2eAgInSc1d1	mistrovský
dorostenecký	dorostenecký	k2eAgInSc1d1	dorostenecký
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
FK	FK	kA	FK
Marila	Marila	k1gFnSc1	Marila
Příbram	Příbram	k1gFnSc1	Příbram
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
trenérem	trenér	k1gMnSc7	trenér
Tobiášem	Tobiáš	k1gMnSc7	Tobiáš
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
odbyl	odbýt	k5eAaPmAgMnS	odbýt
svůj	svůj	k3xOyFgInSc4	svůj
prvoligový	prvoligový	k2eAgInSc4d1	prvoligový
debut	debut	k1gInSc4	debut
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
Zlína	Zlín	k1gInSc2	Zlín
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2007	[number]	k4	2007
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
základní	základní	k2eAgFnSc2d1	základní
sestavy	sestava	k1gFnSc2	sestava
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
boku	bok	k1gInSc6	bok
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
Tomáše	Tomáš	k1gMnSc2	Tomáš
Hunala	Hunal	k1gMnSc2	Hunal
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sezóně	sezóna	k1gFnSc6	sezóna
15	[number]	k4	15
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Příbram	Příbram	k1gFnSc1	Příbram
však	však	k9	však
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
sezóně	sezóna	k1gFnSc6	sezóna
sestoupila	sestoupit	k5eAaPmAgFnS	sestoupit
z	z	k7c2	z
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
Halama	Halama	k1gMnSc1	Halama
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
FC	FC	kA	FC
Viktoria	Viktoria	k1gFnSc1	Viktoria
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
hráčem	hráč	k1gMnSc7	hráč
byl	být	k5eAaImAgInS	být
následující	následující	k2eAgInPc4d1	následující
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
trenérem	trenér	k1gMnSc7	trenér
Levým	levý	k2eAgMnSc7d1	levý
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
sezóny	sezóna	k1gFnSc2	sezóna
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
stal	stát	k5eAaPmAgMnS	stát
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
základní	základní	k2eAgFnSc2d1	základní
sestavy	sestava	k1gFnSc2	sestava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
pod	pod	k7c7	pod
trenérem	trenér	k1gMnSc7	trenér
Šilhavým	šilhavý	k2eAgMnSc7d1	šilhavý
se	se	k3xPyFc4	se
do	do	k7c2	do
sestavy	sestava	k1gFnSc2	sestava
neprosazoval	prosazovat	k5eNaImAgMnS	prosazovat
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
příležitostí	příležitost	k1gFnPc2	příležitost
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
trenér	trenér	k1gMnSc1	trenér
Pavel	Pavel	k1gMnSc1	Pavel
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
v	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
hostování	hostování	k1gNnSc4	hostování
do	do	k7c2	do
FK	FK	kA	FK
Bohemians	Bohemians	k1gInSc1	Bohemians
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odehrál	odehrát	k5eAaPmAgMnS	odehrát
podzimní	podzimní	k2eAgFnSc4d1	podzimní
část	část	k1gFnSc4	část
Gambrinus	gambrinus	k1gInSc4	gambrinus
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zimní	zimní	k2eAgFnSc6d1	zimní
přípravě	příprava	k1gFnSc6	příprava
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
testech	test	k1gInPc6	test
v	v	k7c6	v
FC	FC	kA	FC
Nitra	Nitra	k1gFnSc1	Nitra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
,	,	kIx,	,
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
přestupního	přestupní	k2eAgNnSc2d1	přestupní
období	období	k1gNnSc2	období
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
vybral	vybrat	k5eAaPmAgMnS	vybrat
nováček	nováček	k1gMnSc1	nováček
slovenské	slovenský	k2eAgFnSc2d1	slovenská
Corgoň	Corgoň	k1gFnSc2	Corgoň
ligy	liga	k1gFnSc2	liga
FK	FK	kA	FK
Senica	Senicus	k1gMnSc2	Senicus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
hostoval	hostovat	k5eAaImAgInS	hostovat
v	v	k7c6	v
FC	FC	kA	FC
Vysočina	vysočina	k1gFnSc1	vysočina
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2011	[number]	k4	2011
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
do	do	k7c2	do
SK	Sk	kA	Sk
Dynamo	dynamo	k1gNnSc1	dynamo
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
tréninku	trénink	k1gInSc6	trénink
se	se	k3xPyFc4	se
nepříjemně	příjemně	k6eNd1	příjemně
zranil	zranit	k5eAaPmAgMnS	zranit
a	a	k8xC	a
neabsolvoval	absolvovat	k5eNaPmAgMnS	absolvovat
letní	letní	k2eAgFnSc4d1	letní
přípravu	příprava	k1gFnSc4	příprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
prvoligovému	prvoligový	k2eAgInSc3d1	prvoligový
utkání	utkání	k1gNnSc6	utkání
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
však	však	k9	však
odešel	odejít	k5eAaPmAgMnS	odejít
hostovat	hostovat	k5eAaImF	hostovat
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
druholigové	druholigový	k2eAgFnSc2d1	druholigová
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
postup	postup	k1gInSc1	postup
do	do	k7c2	do
Gambrinus	gambrinus	k1gInSc4	gambrinus
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2012	[number]	k4	2012
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgInS	zatknout
Policií	policie	k1gFnSc7	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
údajnému	údajný	k2eAgInSc3d1	údajný
podílu	podíl	k1gInSc3	podíl
na	na	k7c4	na
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Milan	Milan	k1gMnSc1	Milan
Halama	Halama	k1gMnSc1	Halama
a	a	k8xC	a
bratr	bratr	k1gMnSc1	bratr
Michal	Michal	k1gMnSc1	Michal
Halama	Halama	k1gMnSc1	Halama
také	také	k6eAd1	také
hráli	hrát	k5eAaImAgMnP	hrát
fotbal	fotbal	k1gInSc4	fotbal
na	na	k7c6	na
ligové	ligový	k2eAgFnSc6d1	ligová
úrovni	úroveň	k1gFnSc6	úroveň
v	v	k7c6	v
FK	FK	kA	FK
Jablonec	Jablonec	k1gInSc1	Jablonec
<g/>
.	.	kIx.	.
</s>
<s>
Strýc	strýc	k1gMnSc1	strýc
Václav	Václav	k1gMnSc1	Václav
Halama	Halama	k1gMnSc1	Halama
byl	být	k5eAaImAgMnS	být
fotbalistou	fotbalista	k1gMnSc7	fotbalista
<g/>
,	,	kIx,	,
trenérem	trenér	k1gMnSc7	trenér
a	a	k8xC	a
managerem	manager	k1gMnSc7	manager
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentace	reprezentace	k1gFnSc2	reprezentace
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
postupně	postupně	k6eAd1	postupně
povoláván	povoláván	k2eAgInSc1d1	povoláván
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
mládežnických	mládežnický	k2eAgInPc2d1	mládežnický
výběrů	výběr	k1gInPc2	výběr
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
16	[number]	k4	16
<g/>
-U	-U	k?	-U
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
širšího	široký	k2eAgInSc2d2	širší
kádru	kádr	k1gInSc2	kádr
reprezentace	reprezentace	k1gFnSc2	reprezentace
U-	U-	k1gFnSc2	U-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c6	na
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
