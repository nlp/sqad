<s>
Lemon	lemon	k1gInSc1	lemon
Jelly	Jella	k1gFnSc2	Jella
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc4d1	hrající
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
Nick	Nick	k1gMnSc1	Nick
Franglen	Franglen	k2eAgMnSc1d1	Franglen
a	a	k8xC	a
Fred	Fred	k1gMnSc1	Fred
Deakin	Deakin	k1gMnSc1	Deakin
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
Mercury	Mercura	k1gFnSc2	Mercura
Music	Musice	k1gFnPc2	Musice
Prize	Prize	k1gFnSc2	Prize
a	a	k8xC	a
BRIT	Brit	k1gMnSc1	Brit
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Bath	Bath	k1gInSc1	Bath
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Yellow	Yellow	k1gMnSc1	Yellow
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Midnight	Midnight	k1gMnSc1	Midnight
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Lemonjelly	Lemonjella	k1gFnSc2	Lemonjella
<g/>
.	.	kIx.	.
<g/>
ky	ky	k?	ky
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Lost	Lost	k2eAgInSc1d1	Lost
Horizons	Horizons	k1gInSc1	Horizons
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
'	'	kIx"	'
<g/>
64	[number]	k4	64
<g/>
–	–	k?	–
<g/>
'	'	kIx"	'
<g/>
95	[number]	k4	95
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
