<p>
<s>
Obec	obec	k1gFnSc1	obec
Bludov	Bludov	k1gInSc1	Bludov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Blauda	Blaud	k1gMnSc2	Blaud
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
100	[number]	k4	100
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
obce	obec	k1gFnSc2	obec
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1663	[number]	k4	1663
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
4	[number]	k4	4
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
,	,	kIx,	,
8	[number]	k4	8
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
město	město	k1gNnSc1	město
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
<g/>
,	,	kIx,	,
18	[number]	k4	18
km	km	kA	km
jižně	jižně	k6eAd1	jižně
město	město	k1gNnSc1	město
Mohelnice	Mohelnice	k1gFnSc2	Mohelnice
a	a	k8xC	a
23	[number]	k4	23
km	km	kA	km
západně	západně	k6eAd1	západně
město	město	k1gNnSc1	město
Lanškroun	Lanškroun	k1gInSc1	Lanškroun
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nazván	nazvat	k5eAaBmNgInS	nazvat
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
majiteli	majitel	k1gMnSc6	majitel
Bludu	blud	k1gInSc2	blud
z	z	k7c2	z
Bludova	Bludův	k2eAgNnSc2d1	Bludův
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
Blauda	Blaudo	k1gNnSc2	Blaudo
byl	být	k5eAaImAgInS	být
převzat	převzat	k2eAgInSc1d1	převzat
z	z	k7c2	z
názvu	název	k1gInSc2	název
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
znaku	znak	k1gInSc6	znak
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
srdce	srdce	k1gNnSc2	srdce
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
květinami	květina	k1gFnPc7	květina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
nový	nový	k2eAgInSc1d1	nový
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgMnSc1d1	připomínající
svou	svůj	k3xOyFgFnSc7	svůj
symbolikou	symbolika	k1gFnSc7	symbolika
bývalou	bývalý	k2eAgFnSc4d1	bývalá
vrchnost	vrchnost	k1gFnSc4	vrchnost
-	-	kIx~	-
pány	pan	k1gMnPc4	pan
ze	z	k7c2	z
Žerotína	Žerotín	k1gMnSc2	Žerotín
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc2	jejich
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
předka	předek	k1gMnSc2	předek
Bluda	Blud	k1gMnSc2	Blud
<g/>
,	,	kIx,	,
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
štítě	štít	k1gInSc6	štít
ze	z	k7c2	z
stříbrného	stříbrný	k2eAgNnSc2d1	stříbrné
trojvrší	trojvrší	k1gNnSc2	trojvrší
vyrůstající	vyrůstající	k2eAgInSc4d1	vyrůstající
černý	černý	k2eAgInSc4d1	černý
korunovaný	korunovaný	k2eAgInSc4d1	korunovaný
lev	lev	k1gInSc4	lev
až	až	k9	až
po	po	k7c4	po
kýty	kýta	k1gFnPc4	kýta
<g/>
,	,	kIx,	,
držící	držící	k2eAgMnPc4d1	držící
v	v	k7c6	v
předních	přední	k2eAgFnPc6d1	přední
tlapách	tlapa	k1gFnPc6	tlapa
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
majuskulní	majuskulní	k2eAgFnSc2d1	majuskulní
B.	B.	kA	B.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
jesenického	jesenický	k2eAgNnSc2d1	Jesenické
předhůří	předhůří	k1gNnSc2	předhůří
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
Mohelnické	mohelnický	k2eAgFnSc2d1	Mohelnická
brázdy	brázda	k1gFnSc2	brázda
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
stoupá	stoupat	k5eAaImIp3nS	stoupat
do	do	k7c2	do
svahu	svah	k1gInSc2	svah
pod	pod	k7c4	pod
kopec	kopec	k1gInSc4	kopec
Háj	háj	k1gInSc4	háj
<g/>
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
Senová	senový	k2eAgFnSc1d1	Senová
<g/>
;	;	kIx,	;
631	[number]	k4	631
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
jižní	jižní	k2eAgInSc1d1	jižní
svah	svah	k1gInSc1	svah
nese	nést	k5eAaImIp3nS	nést
podle	podle	k7c2	podle
bývalého	bývalý	k2eAgInSc2d1	bývalý
bludovského	bludovský	k2eAgInSc2d1	bludovský
hradu	hrad	k1gInSc2	hrad
název	název	k1gInSc1	název
Hradisko	hradisko	k1gNnSc1	hradisko
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
stojí	stát	k5eAaImIp3nS	stát
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
306	[number]	k4	306
m	m	kA	m
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
poslední	poslední	k2eAgInPc4d1	poslední
domy	dům	k1gInPc4	dům
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
obce	obec	k1gFnSc2	obec
leží	ležet	k5eAaImIp3nS	ležet
už	už	k6eAd1	už
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
375	[number]	k4	375
m.	m.	k?	m.
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
vrch	vrch	k1gInSc1	vrch
Brusná	brusný	k2eAgFnSc1d1	brusná
(	(	kIx(	(
<g/>
366	[number]	k4	366
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgMnSc7	jenž
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
leží	ležet	k5eAaImIp3nS	ležet
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Bludov	Bludov	k1gInSc1	Bludov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
rozbíhají	rozbíhat	k5eAaImIp3nP	rozbíhat
tratě	trať	k1gFnPc1	trať
Šumperk	Šumperk	k1gInSc1	Šumperk
–	–	k?	–
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
a	a	k8xC	a
Šumperk	Šumperk	k1gInSc1	Šumperk
–	–	k?	–
Hanušovice	Hanušovice	k1gFnPc1	Hanušovice
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInSc4d2	veliký
význam	význam	k1gInSc4	význam
má	mít	k5eAaImIp3nS	mít
Bludov	Bludov	k1gInSc1	Bludov
jako	jako	k8xS	jako
křižovatka	křižovatka	k1gFnSc1	křižovatka
silničních	silniční	k2eAgFnPc2d1	silniční
tras	trasa	k1gFnPc2	trasa
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
potkává	potkávat	k5eAaImIp3nS	potkávat
hlavní	hlavní	k2eAgInSc1d1	hlavní
tah	tah	k1gInSc1	tah
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
–	–	k?	–
Šumperk	Šumperk	k1gInSc1	Šumperk
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
44	[number]	k4	44
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
Šumperk-Jeseník	Šumperk-Jeseník	k1gInSc1	Šumperk-Jeseník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
obce	obec	k1gFnSc2	obec
leží	ležet	k5eAaImIp3nP	ležet
Lázně	lázeň	k1gFnPc1	lázeň
Bludov	Bludovo	k1gNnPc2	Bludovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
blízkosti	blízkost	k1gFnSc6	blízkost
je	být	k5eAaImIp3nS	být
i	i	k9	i
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Bludov-lázně	Bludovázeň	k1gFnSc2	Bludov-lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Odlehle	odlehle	k6eAd1	odlehle
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
na	na	k7c6	na
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
straně	strana	k1gFnSc6	strana
nachází	nacházet	k5eAaImIp3nS	nacházet
koupaliště	koupaliště	k1gNnSc1	koupaliště
Vlčí	vlčí	k2eAgNnSc1d1	vlčí
Důl	důl	k1gInSc4	důl
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Moravě	Morava	k1gFnSc6	Morava
Habermannův	Habermannův	k2eAgInSc4d1	Habermannův
mlýn	mlýn	k1gInSc4	mlýn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
<g/>
,	,	kIx,	,
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Kostelíček	kostelíček	k1gInSc1	kostelíček
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
Kostel	kostel	k1gInSc1	kostel
Božího	boží	k2eAgNnSc2d1	boží
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
3150	[number]	k4	3150
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
1	[number]	k4	1
567	[number]	k4	567
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
1583	[number]	k4	1583
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
<g/>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
činila	činit	k5eAaImAgFnS	činit
11,2	[number]	k4	11,2
procenta	procento	k1gNnSc2	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
Bludov	Bludovo	k1gNnPc2	Bludovo
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1200	[number]	k4	1200
<g/>
–	–	k?	–
<g/>
1216	[number]	k4	1216
u	u	k7c2	u
markraběcího	markraběcí	k2eAgMnSc2d1	markraběcí
úředníka	úředník	k1gMnSc2	úředník
Bluda	Blud	k1gMnSc2	Blud
z	z	k7c2	z
Bludova	Bludův	k2eAgInSc2d1	Bludův
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zakladatelem	zakladatel	k1gMnSc7	zakladatel
vsi	ves	k1gFnSc2	ves
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hrad	hrad	k1gInSc4	hrad
postavil	postavit	k5eAaPmAgMnS	postavit
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
potomků	potomek	k1gMnPc2	potomek
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Bludov	Bludov	k1gInSc1	Bludov
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
moravského	moravský	k2eAgMnSc2d1	moravský
markraběte	markrabě	k1gMnSc2	markrabě
Jana	Jan	k1gMnSc2	Jan
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
odkázal	odkázat	k5eAaPmAgMnS	odkázat
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
pěti	pět	k4xCc7	pět
dalšími	další	k2eAgFnPc7d1	další
obcemi	obec	k1gFnPc7	obec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
zárodkem	zárodek	k1gInSc7	zárodek
nevelkého	velký	k2eNgNnSc2d1	nevelké
bludovského	bludovský	k2eAgNnSc2d1	bludovské
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
svému	svůj	k1gMnSc3	svůj
synovi	syn	k1gMnSc3	syn
Prokopovi	Prokop	k1gMnSc3	Prokop
Lucemburskému	lucemburský	k2eAgMnSc3d1	lucemburský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1410	[number]	k4	1410
se	se	k3xPyFc4	se
Bludov	Bludov	k1gInSc1	Bludov
stal	stát	k5eAaPmAgInS	stát
majetkem	majetek	k1gInSc7	majetek
Pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Zikmundova	Zikmundův	k2eAgMnSc2d1	Zikmundův
stoupence	stoupenec	k1gMnSc2	stoupenec
Beneše	Beneš	k1gMnSc2	Beneš
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Tunklů	Tunkl	k1gInPc2	Tunkl
z	z	k7c2	z
Brníčka	Brníček	k1gInSc2	Brníček
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
u	u	k7c2	u
Bludova	Bludův	k2eAgInSc2d1	Bludův
šest	šest	k4xCc4	šest
velkých	velký	k2eAgInPc2d1	velký
rybníků	rybník	k1gInPc2	rybník
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
ten	ten	k3xDgInSc1	ten
největší	veliký	k2eAgMnSc1d3	veliký
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
Špalek	špalek	k1gInSc4	špalek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rybníků	rybník	k1gInPc2	rybník
se	se	k3xPyFc4	se
nedochoval	dochovat	k5eNaPmAgMnS	dochovat
ani	ani	k9	ani
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
panství	panství	k1gNnSc4	panství
Jiřího	Jiří	k1gMnSc2	Jiří
st.	st.	kA	st.
Tunkla	Tunkla	k1gMnSc1	Tunkla
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1468	[number]	k4	1468
Matyášem	Matyáš	k1gMnSc7	Matyáš
Korvínem	Korvín	k1gMnSc7	Korvín
za	za	k7c2	za
česko-uherské	českoherský	k2eAgFnSc2d1	česko-uherská
války	válka	k1gFnSc2	válka
dobyt	dobyt	k2eAgInSc4d1	dobyt
a	a	k8xC	a
zničen	zničen	k2eAgInSc4d1	zničen
hrad	hrad	k1gInSc4	hrad
Bludov	Bludovo	k1gNnPc2	Bludovo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1496	[number]	k4	1496
získali	získat	k5eAaPmAgMnP	získat
Bludov	Bludov	k1gInSc4	Bludov
páni	pan	k1gMnPc1	pan
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
<g/>
,	,	kIx,	,
odvozující	odvozující	k2eAgFnPc1d1	odvozující
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
od	od	k7c2	od
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
Bludoviců	Bludovice	k1gMnPc2	Bludovice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
měnil	měnit	k5eAaImAgInS	měnit
Bludov	Bludov	k1gInSc1	Bludov
rychle	rychle	k6eAd1	rychle
majitele	majitel	k1gMnPc4	majitel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
těmi	ten	k3xDgFnPc7	ten
konečnými	konečná	k1gFnPc7	konečná
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
Lichtenštejn-Kastelkornové	Lichtenštejn-Kastelkornová	k1gFnSc2	Lichtenštejn-Kastelkornová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
bludovských	bludovský	k2eAgInPc6d1	bludovský
panských	panský	k2eAgInPc6d1	panský
dvorech	dvůr	k1gInPc6	dvůr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
Dolního	dolní	k2eAgInSc2d1	dolní
dvora	dvůr	k1gInSc2	dvůr
byla	být	k5eAaImAgFnS	být
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
postavena	postaven	k2eAgFnSc1d1	postavena
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
postaven	postaven	k2eAgInSc4d1	postaven
dvoukřídlý	dvoukřídlý	k2eAgInSc4d1	dvoukřídlý
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
tvaru	tvar	k1gInSc2	tvar
písmenu	písmeno	k1gNnSc6	písmeno
"	"	kIx"	"
<g/>
U	u	k7c2	u
<g/>
"	"	kIx"	"
roku	rok	k1gInSc2	rok
1708	[number]	k4	1708
<g/>
.	.	kIx.	.
</s>
<s>
Ves	ves	k1gFnSc1	ves
dále	daleko	k6eAd2	daleko
rostla	růst	k5eAaImAgFnS	růst
i	i	k9	i
za	za	k7c2	za
posledních	poslední	k2eAgMnPc2d1	poslední
majitelů	majitel	k1gMnPc2	majitel
<g/>
,	,	kIx,	,
kterými	který	k3yIgMnPc7	který
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Žerotínové	Žerotín	k1gMnPc1	Žerotín
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
z	z	k7c2	z
bludovského	bludovský	k2eAgInSc2d1	bludovský
zámku	zámek	k1gInSc2	zámek
udělali	udělat	k5eAaPmAgMnP	udělat
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
sloužilo	sloužit	k5eAaImAgNnS	sloužit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
Žerotínům	Žerotín	k1gInPc3	Žerotín
vrácen	vrátit	k5eAaPmNgInS	vrátit
a	a	k8xC	a
ti	ty	k3xPp2nSc3	ty
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
dnes	dnes	k6eAd1	dnes
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
v	v	k7c6	v
Bludově	Bludův	k2eAgFnSc6d1	Bludova
stálo	stát	k5eAaImAgNnS	stát
318	[number]	k4	318
domů	dům	k1gInPc2	dům
s	s	k7c7	s
2070	[number]	k4	2070
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
obce	obec	k1gFnSc2	obec
stoupnul	stoupnout	k5eAaPmAgInS	stoupnout
po	po	k7c6	po
vybudování	vybudování	k1gNnSc6	vybudování
císařské	císařský	k2eAgFnSc2d1	císařská
silnice	silnice	k1gFnSc2	silnice
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
výstavbě	výstavba	k1gFnSc6	výstavba
trati	trať	k1gFnSc6	trať
Olomouc-Praha	Olomouc-Praha	k1gFnSc1	Olomouc-Praha
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
Sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zbořen	zbořit	k5eAaPmNgMnS	zbořit
a	a	k8xC	a
postaven	postavit	k5eAaPmNgInS	postavit
nový	nový	k2eAgInSc1d1	nový
společně	společně	k6eAd1	společně
s	s	k7c7	s
žerotínskou	žerotínský	k2eAgFnSc7d1	žerotínská
hrobkou	hrobka	k1gFnSc7	hrobka
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
uloženy	uložen	k2eAgInPc1d1	uložen
i	i	k8xC	i
ostatky	ostatek	k1gInPc1	ostatek
nejslavnějšího	slavný	k2eAgMnSc2d3	nejslavnější
Žerotína	Žerotín	k1gMnSc2	Žerotín
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
staršího	starší	k1gMnSc2	starší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následky	následek	k1gInPc1	následek
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgInP	být
rychle	rychle	k6eAd1	rychle
zahlazeny	zahlazen	k2eAgInPc1d1	zahlazen
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
lánového	lánový	k2eAgInSc2d1	lánový
rejstříku	rejstřík	k1gInSc2	rejstřík
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1677	[number]	k4	1677
tu	tu	k6eAd1	tu
bylo	být	k5eAaImAgNnS	být
72	[number]	k4	72
usedlíků	usedlík	k1gMnPc2	usedlík
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
poustka	poustka	k1gFnSc1	poustka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
Bludov	Bludov	k1gInSc1	Bludov
stal	stát	k5eAaPmAgInS	stát
centrem	centrum	k1gNnSc7	centrum
českého	český	k2eAgInSc2d1	český
národního	národní	k2eAgInSc2d1	národní
života	život	k1gInSc2	život
v	v	k7c6	v
jinak	jinak	k6eAd1	jinak
německém	německý	k2eAgInSc6d1	německý
Šumpersku	Šumpersek	k1gInSc6	Šumpersek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Bludov	Bludov	k1gInSc1	Bludov
výrazně	výrazně	k6eAd1	výrazně
industrializoval	industrializovat	k5eAaBmAgInS	industrializovat
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
zdejších	zdejší	k2eAgMnPc2d1	zdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
byla	být	k5eAaImAgFnS	být
zaměstnána	zaměstnat	k5eAaPmNgFnS	zaměstnat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
blízkém	blízký	k2eAgInSc6d1	blízký
Šumperku	Šumperk	k1gInSc6	Šumperk
a	a	k8xC	a
sudkovské	sudkovský	k2eAgFnSc3d1	sudkovský
textilce	textilka	k1gFnSc3	textilka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
zdejších	zdejší	k2eAgFnPc6d1	zdejší
hedvábnických	hedvábnický	k2eAgFnPc6d1	hedvábnická
továrnách	továrna	k1gFnPc6	továrna
<g/>
,	,	kIx,	,
dvou	dva	k4xCgInPc2	dva
mlýnech	mlýn	k1gInPc6	mlýn
<g/>
,	,	kIx,	,
pile	pila	k1gFnSc3	pila
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc3d2	menší
strojírně	strojírna	k1gFnSc3	strojírna
a	a	k8xC	a
ve	v	k7c6	v
vápence	vápenka	k1gFnSc6	vápenka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
tu	tu	k6eAd1	tu
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
první	první	k4xOgFnSc1	první
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
vyučovacím	vyučovací	k2eAgInSc7d1	vyučovací
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
Šumpersku	Šumpersko	k1gNnSc6	Šumpersko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
Bludově	Bludův	k2eAgFnSc6d1	Bludova
politicky	politicky	k6eAd1	politicky
vedli	vést	k5eAaImAgMnP	vést
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
sociálové	sociál	k1gMnPc1	sociál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
meziválečné	meziválečný	k2eAgFnSc2d1	meziválečná
lidovci	lidovec	k1gMnPc1	lidovec
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
a	a	k8xC	a
komunisté	komunista	k1gMnPc1	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Zaniklé	zaniklý	k2eAgFnPc1d1	zaniklá
textilky	textilka	k1gFnPc1	textilka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
nahradila	nahradit	k5eAaPmAgFnS	nahradit
družstevní	družstevní	k2eAgFnSc1d1	družstevní
mlékárna	mlékárna	k1gFnSc1	mlékárna
<g/>
,	,	kIx,	,
parní	parní	k2eAgFnSc1d1	parní
pila	pila	k1gFnSc1	pila
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
beden	bedna	k1gFnPc2	bedna
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
slévárna	slévárna	k1gFnSc1	slévárna
a	a	k8xC	a
lihovar	lihovar	k1gInSc1	lihovar
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
byly	být	k5eAaImAgFnP	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
místní	místní	k2eAgFnPc1d1	místní
lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
převážně	převážně	k6eAd1	převážně
českou	český	k2eAgFnSc4d1	Česká
obec	obec	k1gFnSc4	obec
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgFnSc6d1	mluvící
oblasti	oblast	k1gFnSc6	oblast
Šumperska	Šumpersk	k1gInSc2	Šumpersk
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
připojena	připojit	k5eAaPmNgFnS	připojit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Sudet	Sudety	k1gFnPc2	Sudety
k	k	k7c3	k
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Bludov	Bludov	k1gInSc1	Bludov
obcí	obec	k1gFnPc2	obec
aktivně	aktivně	k6eAd1	aktivně
zapojenou	zapojený	k2eAgFnSc4d1	zapojená
do	do	k7c2	do
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Taroková	tarokový	k2eAgFnSc1d1	taroková
partie	partie	k1gFnSc1	partie
<g/>
,	,	kIx,	,
maskovaná	maskovaný	k2eAgFnSc1d1	maskovaná
jako	jako	k8xC	jako
karetní	karetní	k2eAgInSc1d1	karetní
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
psala	psát	k5eAaImAgFnS	psát
anonymní	anonymní	k2eAgNnSc4d1	anonymní
udání	udání	k1gNnSc4	udání
na	na	k7c4	na
příliš	příliš	k6eAd1	příliš
horlivé	horlivý	k2eAgMnPc4d1	horlivý
německé	německý	k2eAgMnPc4d1	německý
správce	správce	k1gMnPc4	správce
bludovské	bludovský	k2eAgMnPc4d1	bludovský
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kordas	Kordas	k1gMnSc1	Kordas
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
zakladatel	zakladatel	k1gMnSc1	zakladatel
bludovské	bludovský	k2eAgFnSc2d1	bludovská
Obce	obec	k1gFnSc2	obec
Sokolské	sokolská	k1gFnSc2	sokolská
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
úřednickou	úřednický	k2eAgFnSc7d1	úřednická
činností	činnost	k1gFnSc7	činnost
zmírňoval	zmírňovat	k5eAaImAgInS	zmírňovat
nacistické	nacistický	k2eAgFnPc4d1	nacistická
represe	represe	k1gFnPc4	represe
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInPc4	jeho
nekomforní	komforní	k2eNgInPc4d1	komforní
postoje	postoj	k1gInPc4	postoj
několikrát	několikrát	k6eAd1	několikrát
vyhrožováno	vyhrožován	k2eAgNnSc1d1	vyhrožováno
koncentračním	koncentrační	k2eAgInSc7d1	koncentrační
táborem	tábor	k1gInSc7	tábor
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
JZD	JZD	kA	JZD
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
sloučilo	sloučit	k5eAaPmAgNnS	sloučit
s	s	k7c7	s
JZD	JZD	kA	JZD
Bohutín	Bohutín	k1gInSc1	Bohutín
<g/>
,	,	kIx,	,
Chromeč	Chromeč	k1gInSc1	Chromeč
<g/>
,	,	kIx,	,
Sudkov	Sudkov	k1gInSc1	Sudkov
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Malín	Malín	k1gInSc1	Malín
a	a	k8xC	a
Temenice	Temenice	k1gFnSc1	Temenice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
se	se	k3xPyFc4	se
družstevní	družstevní	k2eAgFnSc1d1	družstevní
mlékárna	mlékárna	k1gFnSc1	mlékárna
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c4	v
sodovkárnu	sodovkárna	k1gFnSc4	sodovkárna
(	(	kIx(	(
<g/>
Nealko	nealko	k1gNnSc2	nealko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
závodem	závod	k1gInSc7	závod
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pobočka	pobočka	k1gFnSc1	pobočka
STS	STS	kA	STS
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Agregu	Agrega	k1gFnSc4	Agrega
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
výstavba	výstavba	k1gFnSc1	výstavba
koupaliště	koupaliště	k1gNnSc2	koupaliště
ve	v	k7c6	v
Vlčím	vlčí	k2eAgInSc6d1	vlčí
Dole	dol	k1gInSc6	dol
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
sodovkárna	sodovkárna	k1gFnSc1	sodovkárna
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
koupaliště	koupaliště	k1gNnSc1	koupaliště
bojovalo	bojovat	k5eAaImAgNnS	bojovat
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
nacházelo	nacházet	k5eAaImAgNnS	nacházet
zdravotní	zdravotní	k2eAgNnSc1d1	zdravotní
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
úplná	úplný	k2eAgFnSc1d1	úplná
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc4	dva
sportovní	sportovní	k2eAgNnPc4d1	sportovní
hřiště	hřiště	k1gNnPc4	hřiště
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
asi	asi	k9	asi
8	[number]	k4	8
restauračních	restaurační	k2eAgNnPc2d1	restaurační
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecní	obecní	k2eAgFnSc1d1	obecní
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
sídelní	sídelní	k2eAgFnPc4d1	sídelní
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
:	:	kIx,	:
Bludov	Bludov	k1gInSc1	Bludov
a	a	k8xC	a
Bludov-Zámeček	Bludov-Zámeček	k1gInSc1	Bludov-Zámeček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
jsou	být	k5eAaImIp3nP	být
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Bludov	Bludov	k1gInSc1	Bludov
–	–	k?	–
skromné	skromný	k2eAgInPc4d1	skromný
zbytky	zbytek	k1gInPc4	zbytek
hradu	hrad	k1gInSc2	hrad
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postaveného	postavený	k2eAgInSc2d1	postavený
Bludem	blud	k1gInSc7	blud
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Zanikl	zaniknout	k5eAaPmAgInS	zaniknout
za	za	k7c2	za
uherských	uherský	k2eAgFnPc2d1	uherská
válek	válka	k1gFnPc2	válka
po	po	k7c6	po
dobytí	dobytí	k1gNnSc2	dobytí
Matyášem	Matyáš	k1gMnSc7	Matyáš
Korvínem	Korvín	k1gMnSc7	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
hradního	hradní	k2eAgNnSc2d1	hradní
zdiva	zdivo	k1gNnSc2	zdivo
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgInPc1d1	použit
v	v	k7c6	v
pol.	pol.	k?	pol.
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Bludov	Bludov	k1gInSc1	Bludov
s	s	k7c7	s
areálem	areál	k1gInSc7	areál
parku	park	k1gInSc2	park
–	–	k?	–
trojkřídlý	trojkřídlý	k2eAgInSc1d1	trojkřídlý
renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
obce	obec	k1gFnSc2	obec
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
třetiny	třetina	k1gFnSc2	třetina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1708	[number]	k4	1708
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
zámku	zámek	k1gInSc2	zámek
je	být	k5eAaImIp3nS	být
park	park	k1gInSc1	park
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
9,16	[number]	k4	9,16
ha	ha	kA	ha
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
hodnotných	hodnotný	k2eAgFnPc2d1	hodnotná
dřevin	dřevina	k1gFnPc2	dřevina
a	a	k8xC	a
několika	několik	k4yIc7	několik
památnými	památný	k2eAgInPc7d1	památný
stromy	strom	k1gInPc7	strom
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejstarší	starý	k2eAgFnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
lípa	lípa	k1gFnSc1	lípa
srdčitá	srdčitý	k2eAgFnSc1d1	srdčitá
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
věk	věk	k1gInSc4	věk
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
rodiny	rodina	k1gFnSc2	rodina
Mornstein-Zierotin	Mornstein-Zierotina	k1gFnPc2	Mornstein-Zierotina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Božího	boží	k2eAgNnSc2d1	boží
těla	tělo	k1gNnSc2	tělo
s	s	k7c7	s
areálem	areál	k1gInSc7	areál
–	–	k?	–
poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednolodní	jednolodní	k2eAgFnSc4d1	jednolodní
barokní	barokní	k2eAgFnSc4d1	barokní
stavbu	stavba	k1gFnSc4	stavba
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
Lourdské	Lourdský	k2eAgNnSc1d1	Lourdský
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
pramenem	pramen	k1gInSc7	pramen
<g/>
)	)	kIx)	)
–	–	k?	–
drobná	drobný	k2eAgFnSc1d1	drobná
barokní	barokní	k2eAgFnSc1d1	barokní
architektura	architektura	k1gFnSc1	architektura
ze	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
sloupková	sloupkový	k2eAgFnSc1d1	sloupková
boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
</s>
</p>
<p>
<s>
kříž	kříž	k1gInSc1	kříž
–	–	k?	–
drobná	drobný	k2eAgFnSc1d1	drobná
kamenická	kamenický	k2eAgFnSc1d1	kamenická
práce	práce	k1gFnSc1	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
</s>
</p>
<p>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Nejsvětějsí	Nejsvětějse	k1gFnPc2	Nejsvětějse
Trojice	trojice	k1gFnSc2	trojice
–	–	k?	–
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
mezi	mezi	k7c7	mezi
Bludovem	Bludovo	k1gNnSc7	Bludovo
a	a	k8xC	a
Šumperkem	Šumperk	k1gInSc7	Šumperk
<g/>
,	,	kIx,	,
drobná	drobný	k2eAgFnSc1d1	drobná
barokní	barokní	k2eAgFnSc1d1	barokní
architektura	architektura	k1gFnSc1	architektura
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Venkovská	venkovský	k2eAgFnSc1d1	venkovská
usedlost	usedlost	k1gFnSc1	usedlost
čp.	čp.	k?	čp.
118	[number]	k4	118
</s>
</p>
<p>
<s>
Venkovský	venkovský	k2eAgInSc1d1	venkovský
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
120	[number]	k4	120
</s>
</p>
<p>
<s>
Venkovská	venkovský	k2eAgFnSc1d1	venkovská
usedlost	usedlost	k1gFnSc1	usedlost
čp.	čp.	k?	čp.
125	[number]	k4	125
</s>
</p>
<p>
<s>
Venkovská	venkovský	k2eAgFnSc1d1	venkovská
usedlost	usedlost	k1gFnSc1	usedlost
čp.	čp.	k?	čp.
126	[number]	k4	126
se	s	k7c7	s
stodolou	stodola	k1gFnSc7	stodola
–	–	k?	–
lidová	lidový	k2eAgFnSc1d1	lidová
architektura	architektura	k1gFnSc1	architektura
s	s	k7c7	s
arkádovým	arkádový	k2eAgNnSc7d1	arkádové
náspím	náspí	k1gNnSc7	náspí
s	s	k7c7	s
dvouvjezdovou	dvouvjezdový	k2eAgFnSc7d1	dvouvjezdový
stodolou	stodola	k1gFnSc7	stodola
<g/>
,	,	kIx,	,
datované	datovaný	k2eAgNnSc1d1	datované
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
</s>
</p>
<p>
<s>
Venkovská	venkovský	k2eAgFnSc1d1	venkovská
usedlost	usedlost	k1gFnSc1	usedlost
čp.	čp.	k?	čp.
127	[number]	k4	127
</s>
</p>
<p>
<s>
Sýpka	sýpka	k1gFnSc1	sýpka
v	v	k7c6	v
Horním	horní	k2eAgMnSc6d1	horní
(	(	kIx(	(
<g/>
Bludovském	Bludovský	k2eAgInSc6d1	Bludovský
<g/>
)	)	kIx)	)
dvoře	dvůr	k1gInSc6	dvůr
čp.	čp.	k?	čp.
193	[number]	k4	193
severozápadně	severozápadně	k6eAd1	severozápadně
obce	obec	k1gFnPc1	obec
</s>
</p>
<p>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
–	–	k?	–
kamenická	kamenický	k2eAgFnSc1d1	kamenická
práce	práce	k1gFnSc1	práce
z	z	k7c2	z
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
novějším	nový	k2eAgInSc7d2	novější
podstavcem	podstavec	k1gInSc7	podstavec
<g/>
,	,	kIx,	,
ikonograficky	ikonograficky	k6eAd1	ikonograficky
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
sochy	socha	k1gFnSc2	socha
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
v	v	k7c6	v
Šumperku	Šumperk	k1gInSc6	Šumperk
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
–	–	k?	–
kamenická	kamenický	k2eAgFnSc1d1	kamenická
práce	práce	k1gFnSc1	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
</s>
</p>
<p>
<s>
Socha	Socha	k1gMnSc1	Socha
sv.	sv.	kA	sv.
Rocha	Rocha	k1gMnSc1	Rocha
u	u	k7c2	u
cesty	cesta	k1gFnSc2	cesta
na	na	k7c4	na
BludovečekMimo	BludovečekMima	k1gFnSc5	BludovečekMima
památkově	památkově	k6eAd1	památkově
chráněných	chráněný	k2eAgMnPc2d1	chráněný
objektů	objekt	k1gInPc2	objekt
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
další	další	k2eAgFnSc2d1	další
kulturní	kulturní	k2eAgFnSc2d1	kulturní
památky	památka	k1gFnSc2	památka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
</s>
</p>
<p>
<s>
Hrobka	hrobka	k1gFnSc1	hrobka
Žerotínů	Žerotín	k1gInPc2	Žerotín
pod	pod	k7c4	pod
sakristii	sakristie	k1gFnSc4	sakristie
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pochován	pochován	k2eAgMnSc1d1	pochován
Karel	Karel	k1gMnSc1	Karel
starší	starší	k1gMnSc1	starší
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
zajímavosti	zajímavost	k1gFnPc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Teplý	teplý	k2eAgInSc4d1	teplý
pramen	pramen	k1gInSc4	pramen
léčivých	léčivý	k2eAgInPc2d1	léčivý
účinků	účinek	k1gInPc2	účinek
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
Žerotíny	Žerotína	k1gFnPc1	Žerotína
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc1	lázeň
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
vody	voda	k1gFnPc1	voda
z	z	k7c2	z
podhůří	podhůří	k1gNnSc2	podhůří
Jeseníků	Jeseník	k1gInPc2	Jeseník
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
spjaté	spjatý	k2eAgFnPc1d1	spjatá
s	s	k7c7	s
obcí	obec	k1gFnSc7	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Jarolím	Jarolit	k5eAaImIp1nS	Jarolit
Adámek	Adámek	k1gMnSc1	Adámek
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Emanuel	Emanuel	k1gMnSc1	Emanuel
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1850	[number]	k4	1850
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
místodržící	místodržící	k1gMnSc1	místodržící
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Kašpar	Kašpar	k1gMnSc1	Kašpar
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1877	[number]	k4	1877
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Hlásný	hlásný	k2eAgMnSc1d1	hlásný
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
voják	voják	k1gMnSc1	voják
v	v	k7c6	v
odboji	odboj	k1gInSc6	odboj
</s>
</p>
<p>
<s>
PhDr.	PhDr.	kA	PhDr.
Jan	Jan	k1gMnSc1	Jan
Březina	Březina	k1gMnSc1	Březina
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
příruček	příručka	k1gFnPc2	příručka
a	a	k8xC	a
učebnic	učebnice	k1gFnPc2	učebnice
O	o	k7c6	o
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O	o	k7c6	o
povětrnosti	povětrnost	k1gFnSc6	povětrnost
a	a	k8xC	a
jejím	její	k3xOp3gNnSc6	její
vědeckém	vědecký	k2eAgNnSc6d1	vědecké
předvídání	předvídání	k1gNnSc6	předvídání
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O	o	k7c6	o
řešení	řešení	k1gNnSc6	řešení
slovních	slovní	k2eAgInPc2d1	slovní
příkladů	příklad	k1gInPc2	příklad
z	z	k7c2	z
fysiky	fysika	k1gFnSc2	fysika
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praktické	praktický	k2eAgNnSc1d1	praktické
cvičení	cvičení	k1gNnSc1	cvičení
z	z	k7c2	z
fysiky	fysika	k1gFnSc2	fysika
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
fysikální	fysikální	k2eAgFnSc2d1	fysikální
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc4	několik
hesel	heslo	k1gNnPc2	heslo
v	v	k7c6	v
Technickém	technický	k2eAgInSc6d1	technický
naučném	naučný	k2eAgInSc6d1	naučný
slovníku	slovník	k1gInSc6	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
historické	historický	k2eAgFnPc1d1	historická
práce	práce	k1gFnPc1	práce
o	o	k7c6	o
Šumpersku	Šumpersko	k1gNnSc6	Šumpersko
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
vlastivědných	vlastivědný	k2eAgInPc2d1	vlastivědný
pramenů	pramen	k1gInPc2	pramen
okresu	okres	k1gInSc2	okres
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kordas	Kordas	k1gMnSc1	Kordas
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náčelník	náčelník	k1gMnSc1	náčelník
Severomoravské	severomoravský	k2eAgFnSc2d1	Severomoravská
župy	župa	k1gFnSc2	župa
sokolské	sokolská	k1gFnSc2	sokolská
<g/>
,	,	kIx,	,
buditel	buditel	k1gMnSc1	buditel
kulturního	kulturní	k2eAgInSc2d1	kulturní
bludovského	bludovský	k2eAgInSc2d1	bludovský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
protektorátu	protektorát	k1gInSc2	protektorát
vězněn	věznit	k5eAaImNgInS	věznit
gestapem	gestapo	k1gNnSc7	gestapo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
doc.	doc.	kA	doc.
Ing.	ing.	kA	ing.
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pohl	Pohl	k1gMnSc1	Pohl
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
-	-	kIx~	-
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
a	a	k8xC	a
vážený	vážený	k2eAgMnSc1d1	vážený
kantor	kantor	k1gMnSc1	kantor
na	na	k7c6	na
VŠ	vš	k0	vš
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
mnoha	mnoho	k4c2	mnoho
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
ohledně	ohledně	k7c2	ohledně
zahrad	zahrada	k1gFnPc2	zahrada
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
o	o	k7c4	o
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bludov	Bludovo	k1gNnPc2	Bludovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Bludov	Bludov	k1gInSc1	Bludov
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
obce	obec	k1gFnSc2	obec
</s>
</p>
