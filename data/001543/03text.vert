<s>
Niels	Niels	k6eAd1	Niels
Henrik	Henrik	k1gMnSc1	Henrik
David	David	k1gMnSc1	David
Bohr	Bohr	k1gMnSc1	Bohr
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1885	[number]	k4	1885
Kodaň	Kodaň	k1gFnSc1	Kodaň
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
Kodaň	Kodaň	k1gFnSc1	Kodaň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dánský	dánský	k2eAgMnSc1d1	dánský
myslitel	myslitel	k1gMnSc1	myslitel
<g/>
,	,	kIx,	,
filantrop	filantrop	k1gMnSc1	filantrop
a	a	k8xC	a
vědec	vědec	k1gMnSc1	vědec
působící	působící	k2eAgMnSc1d1	působící
především	především	k6eAd1	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
atomové	atomový	k2eAgFnSc2d1	atomová
a	a	k8xC	a
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
struktury	struktura	k1gFnSc2	struktura
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
Bohr	Bohr	k1gInSc1	Bohr
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
představ	představa	k1gFnPc2	představa
Ernesta	Ernest	k1gMnSc4	Ernest
Rutherforda	Rutherford	k1gMnSc2	Rutherford
(	(	kIx(	(
<g/>
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
<g/>
)	)	kIx)	)
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc2d1	kvantová
hypotézy	hypotéza	k1gFnSc2	hypotéza
(	(	kIx(	(
<g/>
energie	energie	k1gFnSc1	energie
záření	záření	k1gNnSc2	záření
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c6	po
určitých	určitý	k2eAgInPc6d1	určitý
"	"	kIx"	"
<g/>
žmolcích	žmolec	k1gInPc6	žmolec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kvantech	kvantum	k1gNnPc6	kvantum
<g/>
)	)	kIx)	)
Maxe	Max	k1gMnSc2	Max
Plancka	Plancka	k1gFnSc1	Plancka
<g/>
,	,	kIx,	,
již	jenž	k3xRgFnSc4	jenž
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
dále	daleko	k6eAd2	daleko
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
první	první	k4xOgInSc4	první
kvantový	kvantový	k2eAgInSc4d1	kvantový
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
model	model	k1gInSc1	model
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
o	o	k7c4	o
princip	princip	k1gInSc4	princip
korespondence	korespondence	k1gFnSc2	korespondence
(	(	kIx(	(
<g/>
nárok	nárok	k1gInSc1	nárok
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
být	být	k5eAaImF	být
racionální	racionální	k2eAgFnSc7d1	racionální
generalizací	generalizace	k1gFnSc7	generalizace
klasické	klasický	k2eAgFnSc2d1	klasická
teorie	teorie	k1gFnSc2	teorie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
svou	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
prosadit	prosadit	k5eAaPmF	prosadit
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
starší	starý	k2eAgFnSc1d2	starší
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
založil	založit	k5eAaPmAgInS	založit
Ústav	ústav	k1gInSc1	ústav
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
byl	být	k5eAaImAgMnS	být
celoživotním	celoživotní	k2eAgMnSc7d1	celoživotní
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
teoreticky	teoreticky	k6eAd1	teoreticky
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
periodickou	periodický	k2eAgFnSc4d1	periodická
soustavu	soustava	k1gFnSc4	soustava
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgInSc2	svůj
modelu	model	k1gInSc2	model
atomu	atom	k1gInSc2	atom
rozšířeného	rozšířený	k2eAgInSc2d1	rozšířený
Arnoldem	Arnold	k1gMnSc7	Arnold
Sommerfeldem	Sommerfeld	k1gMnSc7	Sommerfeld
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
zejména	zejména	k9	zejména
s	s	k7c7	s
Wernerem	Werner	k1gMnSc7	Werner
Heisenbergem	Heisenberg	k1gMnSc7	Heisenberg
předběžně	předběžně	k6eAd1	předběžně
ukončili	ukončit	k5eAaPmAgMnP	ukončit
vývoj	vývoj	k1gInSc4	vývoj
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
vznikem	vznik	k1gInSc7	vznik
tzv.	tzv.	kA	tzv.
kodaňské	kodaňský	k2eAgFnSc2d1	Kodaňská
školy	škola	k1gFnSc2	škola
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
náročných	náročný	k2eAgFnPc6d1	náročná
diskusích	diskuse	k1gFnPc6	diskuse
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
a	a	k8xC	a
1927	[number]	k4	1927
nakonec	nakonec	k6eAd1	nakonec
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
Bohrovým	Bohrův	k2eAgInSc7d1	Bohrův
názorem	názor	k1gInSc7	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomové	atomový	k2eAgInPc1d1	atomový
jevy	jev	k1gInPc1	jev
nabývají	nabývat	k5eAaImIp3nP	nabývat
jak	jak	k6eAd1	jak
částicových	částicový	k2eAgNnPc2d1	částicové
<g/>
,	,	kIx,	,
tak	tak	k9	tak
vlnových	vlnový	k2eAgFnPc2d1	vlnová
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mají	mít	k5eAaImIp3nP	mít
duální	duální	k2eAgMnPc1d1	duální
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tzv.	tzv.	kA	tzv.
dualismem	dualismus	k1gInSc7	dualismus
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
století	století	k1gNnSc2	století
již	již	k9	již
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
pro	pro	k7c4	pro
záření	záření	k1gNnSc4	záření
(	(	kIx(	(
<g/>
světlo	světlo	k1gNnSc4	světlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
de	de	k?	de
Broglie	Broglie	k1gFnSc2	Broglie
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
pro	pro	k7c4	pro
hmotu	hmota	k1gFnSc4	hmota
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
elektrony	elektron	k1gInPc1	elektron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Heisenberg	Heisenberg	k1gInSc1	Heisenberg
původně	původně	k6eAd1	původně
"	"	kIx"	"
<g/>
nesnášel	snášet	k5eNaImAgInS	snášet
<g/>
"	"	kIx"	"
E.	E.	kA	E.
Schrödingerovu	Schrödingerův	k2eAgFnSc4d1	Schrödingerova
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
mechaniku	mechanika	k1gFnSc4	mechanika
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
a	a	k8xC	a
Schrödinger	Schrödinger	k1gInSc4	Schrödinger
naopak	naopak	k6eAd1	naopak
"	"	kIx"	"
<g/>
nesnášel	snášet	k5eNaImAgInS	snášet
<g/>
"	"	kIx"	"
částicové	částicový	k2eAgNnSc4d1	částicové
pojetí	pojetí	k1gNnSc4	pojetí
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
Heisenberg-Born-Jordanovu	Heisenberg-Born-Jordanův	k2eAgFnSc4d1	Heisenberg-Born-Jordanův
maticovou	maticový	k2eAgFnSc4d1	maticová
mechaniku	mechanika	k1gFnSc4	mechanika
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Heisenbergovy	Heisenbergův	k2eAgFnSc2d1	Heisenbergova
relace	relace	k1gFnSc2	relace
neurčitosti	neurčitost	k1gFnSc2	neurčitost
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
a	a	k8xC	a
M.	M.	kA	M.
Bornovu	Bornův	k2eAgFnSc4d1	Bornova
pravděpodobnostní	pravděpodobnostní	k2eAgFnSc4d1	pravděpodobnostní
interpretaci	interpretace	k1gFnSc4	interpretace
vlnové	vlnový	k2eAgFnSc2d1	vlnová
mechaniky	mechanika	k1gFnSc2	mechanika
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
věřil	věřit	k5eAaImAgMnS	věřit
na	na	k7c4	na
hmotné	hmotný	k2eAgNnSc4d1	hmotné
vlnění	vlnění	k1gNnSc4	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Kodaňská	kodaňský	k2eAgFnSc1d1	Kodaňská
škola	škola	k1gFnSc1	škola
též	též	k9	též
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
princip	princip	k1gInSc1	princip
komplementarity	komplementarita	k1gFnSc2	komplementarita
(	(	kIx(	(
<g/>
Bohr	Bohr	k1gInSc1	Bohr
pojem	pojem	k1gInSc1	pojem
princip	princip	k1gInSc4	princip
nepoužíval	používat	k5eNaImAgInS	používat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jevy	jev	k1gInPc1	jev
v	v	k7c6	v
mikrosvětě	mikrosvět	k1gInSc6	mikrosvět
jsou	být	k5eAaImIp3nP	být
neurčité	určitý	k2eNgFnPc1d1	neurčitá
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
pozorování	pozorování	k1gNnSc6	pozorování
striktně	striktně	k6eAd1	striktně
rozlišit	rozlišit	k5eAaPmF	rozlišit
a	a	k8xC	a
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
patří	patřit	k5eAaImIp3nS	patřit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
či	či	k8xC	či
měřicích	měřicí	k2eAgMnPc2d1	měřicí
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
co	co	k9	co
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
měřeného	měřený	k2eAgInSc2d1	měřený
jevu	jev	k1gInSc2	jev
či	či	k8xC	či
částice	částice	k1gFnSc2	částice
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nekontrolovatelné	kontrolovatelný	k2eNgInPc4d1	nekontrolovatelný
interagující	interagující	k2eAgInPc4d1	interagující
systémy	systém	k1gInPc4	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutné	nutný	k2eAgInPc4d1	nutný
mikroskopické	mikroskopický	k2eAgInPc4d1	mikroskopický
jevy	jev	k1gInPc4	jev
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
nikoli	nikoli	k9	nikoli
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
tradičního	tradiční	k2eAgNnSc2d1	tradiční
subjekt-objektového	subjektbjektový	k2eAgNnSc2d1	subjekt-objektový
rozlišení	rozlišení	k1gNnSc2	rozlišení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
výlučných	výlučný	k2eAgInPc2d1	výlučný
experimentů	experiment	k1gInPc2	experiment
a	a	k8xC	a
protikladných	protikladný	k2eAgInPc2d1	protikladný
nebo	nebo	k8xC	nebo
neslučitelných	slučitelný	k2eNgInPc2d1	neslučitelný
popisů	popis	k1gInPc2	popis
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vyčerpávající	vyčerpávající	k2eAgInSc4d1	vyčerpávající
a	a	k8xC	a
smysluplný	smysluplný	k2eAgInSc4d1	smysluplný
popis	popis	k1gInSc4	popis
zkoumaného	zkoumaný	k2eAgInSc2d1	zkoumaný
jevu	jev	k1gInSc2	jev
musí	muset	k5eAaImIp3nS	muset
rovnocenně	rovnocenně	k6eAd1	rovnocenně
doplňovat	doplňovat	k5eAaImF	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Bohr	Bohr	k1gMnSc1	Bohr
tento	tento	k3xDgInSc4	tento
komplementární	komplementární	k2eAgInSc4d1	komplementární
epistemologický	epistemologický	k2eAgInSc4d1	epistemologický
rámec	rámec	k1gInSc4	rámec
posléze	posléze	k6eAd1	posléze
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
například	například	k6eAd1	například
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
<g/>
,	,	kIx,	,
antropologii	antropologie	k1gFnSc6	antropologie
<g/>
,	,	kIx,	,
theologii	theologie	k1gFnSc6	theologie
a	a	k8xC	a
biologii	biologie	k1gFnSc6	biologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zabývat	zabývat	k5eAaImF	zabývat
jadernou	jaderný	k2eAgFnSc7d1	jaderná
fyzikou	fyzika	k1gFnSc7	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
model	model	k1gInSc1	model
složeného	složený	k2eAgNnSc2d1	složené
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
tzv.	tzv.	kA	tzv.
kapkový	kapkový	k2eAgInSc1d1	kapkový
model	model	k1gInSc1	model
jádra	jádro	k1gNnSc2	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
fyziky	fyzik	k1gMnPc7	fyzik
objevil	objevit	k5eAaPmAgMnS	objevit
pro	pro	k7c4	pro
praxi	praxe	k1gFnSc4	praxe
významné	významný	k2eAgInPc1d1	významný
procesy	proces	k1gInPc1	proces
štěpení	štěpení	k1gNnSc2	štěpení
jádra	jádro	k1gNnSc2	jádro
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Johnem	John	k1gMnSc7	John
A.	A.	kA	A.
Wheelerem	Wheeler	k1gMnSc7	Wheeler
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
napsali	napsat	k5eAaBmAgMnP	napsat
významný	významný	k2eAgInSc4d1	významný
článek	článek	k1gInSc4	článek
Mechanismus	mechanismus	k1gInSc1	mechanismus
jaderného	jaderný	k2eAgNnSc2d1	jaderné
štěpení	štěpení	k1gNnSc2	štěpení
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c4	v
den	den	k1gInSc4	den
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
Phys	Physa	k1gFnPc2	Physa
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
56	[number]	k4	56
<g/>
,	,	kIx,	,
426	[number]	k4	426
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1943	[number]	k4	1943
až	až	k9	až
1945	[number]	k4	1945
žil	žíla	k1gFnPc2	žíla
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zapojen	zapojit	k5eAaPmNgMnS	zapojit
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Bohrovi	Bohrův	k2eAgMnPc1d1	Bohrův
jsou	být	k5eAaImIp3nP	být
věnovány	věnovat	k5eAaImNgFnP	věnovat
české	český	k2eAgFnPc1d1	Česká
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
Niels	Nielsa	k1gFnPc2	Nielsa
Bohr	Bohra	k1gFnPc2	Bohra
jako	jako	k8xS	jako
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
občan	občan	k1gMnSc1	občan
http://nielsbohr.webnode.cz/.	[url]	k?	http://nielsbohr.webnode.cz/.
</s>
