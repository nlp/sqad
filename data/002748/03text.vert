<s>
Elbrus	Elbrus	k1gInSc1	Elbrus
(	(	kIx(	(
<g/>
balkarsky	balkarsky	k6eAd1	balkarsky
М	М	k?	М
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Э	Э	k?	Э
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Mount	Mount	k1gMnSc1	Mount
Elbrus	Elbrus	k1gMnSc1	Elbrus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
5642	[number]	k4	5642
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
horou	hora	k1gFnSc7	hora
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
se	se	k3xPyFc4	se
vrchol	vrchol	k1gInSc1	vrchol
Elbrusu	Elbrus	k1gInSc2	Elbrus
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k9	ještě
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
činilo	činit	k5eAaImAgNnS	činit
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
evropskou	evropský	k2eAgFnSc4d1	Evropská
horu	hora	k1gFnSc4	hora
(	(	kIx(	(
<g/>
podrobněji	podrobně	k6eAd2	podrobně
tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
článek	článek	k1gInSc1	článek
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elbrus	Elbrus	k1gInSc1	Elbrus
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
kavkazském	kavkazský	k2eAgInSc6d1	kavkazský
hřebeni	hřeben	k1gInSc6	hřeben
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Velký	velký	k2eAgInSc1d1	velký
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
paralelně	paralelně	k6eAd1	paralelně
se	se	k3xPyFc4	se
táhnoucím	táhnoucí	k2eAgInSc6d1	táhnoucí
bočním	boční	k2eAgInSc6d1	boční
kavkazském	kavkazský	k2eAgInSc6d1	kavkazský
hřbetu	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Elbrus	Elbrus	k1gInSc1	Elbrus
je	být	k5eAaImIp3nS	být
neaktivní	aktivní	k2eNgFnSc7d1	neaktivní
sopkou	sopka	k1gFnSc7	sopka
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
vrcholy	vrchol	k1gInPc7	vrchol
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc4d1	západní
5642	[number]	k4	5642
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
východní	východní	k2eAgInPc1d1	východní
5621	[number]	k4	5621
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
obou	dva	k4xCgInPc2	dva
vrcholů	vrchol	k1gInPc2	vrchol
je	být	k5eAaImIp3nS	být
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Svahy	svah	k1gInPc1	svah
sopky	sopka	k1gFnSc2	sopka
jsou	být	k5eAaImIp3nP	být
pokryté	pokrytý	k2eAgInPc4d1	pokrytý
ledovci	ledovec	k1gInPc7	ledovec
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
ploše	plocha	k1gFnSc6	plocha
138	[number]	k4	138
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Sněžná	sněžný	k2eAgFnSc1d1	sněžná
čára	čára	k1gFnSc1	čára
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Elbrusu	Elbrus	k1gInSc6	Elbrus
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3700	[number]	k4	3700
až	až	k9	až
4000	[number]	k4	4000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Elbrus	Elbrus	k1gInSc1	Elbrus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Elbrus	Elbrus	k1gInSc4	Elbrus
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Elbrus	Elbrus	k1gInSc4	Elbrus
na	na	k7c4	na
Treking	Treking	k1gInSc4	Treking
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Elbrus	Elbrus	k1gMnSc1	Elbrus
na	na	k7c6	na
topografické	topografický	k2eAgFnSc6d1	topografická
mapě	mapa	k1gFnSc6	mapa
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
Elbrus	Elbrus	k1gInSc4	Elbrus
na	na	k7c4	na
Peakware	Peakwar	k1gMnSc5	Peakwar
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
