<s>
Zvykové	zvykový	k2eAgNnSc1d1	zvykové
právo	právo	k1gNnSc1	právo
je	být	k5eAaImIp3nS	být
právní	právní	k2eAgInSc1d1	právní
systém	systém	k1gInSc1	systém
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
právních	právní	k2eAgInPc6d1	právní
obyčejích	obyčej	k1gInPc6	obyčej
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
společenství	společenství	k1gNnSc6	společenství
ustálily	ustálit	k5eAaPmAgFnP	ustálit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
uznávány	uznáván	k2eAgFnPc1d1	uznávána
<g/>
,	,	kIx,	,
sankcionovány	sankcionován	k2eAgFnPc1d1	sankcionována
a	a	k8xC	a
dodržovány	dodržován	k2eAgFnPc1d1	dodržována
<g/>
.	.	kIx.	.
</s>
