<p>
<s>
Zvykové	zvykový	k2eAgNnSc1d1	zvykové
právo	právo	k1gNnSc1	právo
je	být	k5eAaImIp3nS	být
právní	právní	k2eAgInSc1d1	právní
systém	systém	k1gInSc1	systém
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
právních	právní	k2eAgInPc6d1	právní
obyčejích	obyčej	k1gInPc6	obyčej
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
společenství	společenství	k1gNnSc6	společenství
ustálily	ustálit	k5eAaPmAgFnP	ustálit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
uznávány	uznáván	k2eAgFnPc1d1	uznávána
<g/>
,	,	kIx,	,
sankcionovány	sankcionován	k2eAgFnPc1d1	sankcionována
a	a	k8xC	a
dodržovány	dodržován	k2eAgFnPc1d1	dodržována
<g/>
.	.	kIx.	.
</s>
<s>
Zvykové	zvykový	k2eAgNnSc1d1	zvykové
právo	právo	k1gNnSc1	právo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
psané	psaný	k2eAgNnSc1d1	psané
i	i	k8xC	i
nepsané	psaný	k2eNgNnSc1d1	nepsané
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvykovým	zvykový	k2eAgNnSc7d1	zvykové
právem	právo	k1gNnSc7	právo
se	se	k3xPyFc4	se
řídily	řídit	k5eAaImAgInP	řídit
například	například	k6eAd1	například
barbarské	barbarský	k2eAgInPc1d1	barbarský
kmeny	kmen	k1gInPc1	kmen
v	v	k7c6	v
době	doba	k1gFnSc6	doba
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc2	vznik
prvních	první	k4xOgInPc2	první
státních	státní	k2eAgInPc2d1	státní
útvarů	útvar	k1gInPc2	útvar
–	–	k?	–
barbarských	barbarský	k2eAgInPc2d1	barbarský
říší	říš	k1gFnSc7	říš
–	–	k?	–
nechali	nechat	k5eAaPmAgMnP	nechat
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jejich	jejich	k3xOp3gMnPc1	jejich
panovníci	panovník	k1gMnPc1	panovník
pořídit	pořídit	k5eAaPmF	pořídit
soupis	soupis	k1gInSc1	soupis
tohoto	tento	k3xDgNnSc2	tento
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tzv.	tzv.	kA	tzv.
barbarské	barbarský	k2eAgInPc1d1	barbarský
zákoníky	zákoník	k1gInPc1	zákoník
však	však	k9	však
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
již	již	k6eAd1	již
římským	římský	k2eAgNnSc7d1	římské
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
důsledkem	důsledek	k1gInSc7	důsledek
sžívání	sžívání	k1gNnSc2	sžívání
se	se	k3xPyFc4	se
barbarů	barbar	k1gMnPc2	barbar
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
římským	římský	k2eAgNnSc7d1	římské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
zákoníků	zákoník	k1gInPc2	zákoník
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
Zákoník	zákoník	k1gInSc1	zákoník
sálských	sálský	k2eAgInPc2d1	sálský
Franků	Franky	k1gInPc2	Franky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
právu	právo	k1gNnSc6	právo
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
práva	právo	k1gNnSc2	právo
angloamerického	angloamerický	k2eAgInSc2d1	angloamerický
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
právní	právní	k2eAgInPc4d1	právní
obyčeje	obyčej	k1gInPc4	obyčej
pramenem	pramen	k1gInSc7	pramen
práva	právo	k1gNnSc2	právo
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
obchodní	obchodní	k2eAgFnPc4d1	obchodní
zvyklosti	zvyklost	k1gFnPc4	zvyklost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
ustanovení	ustanovení	k1gNnSc2	ustanovení
§	§	k?	§
1	[number]	k4	1
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
obchodního	obchodní	k2eAgInSc2d1	obchodní
zákoníku	zákoník	k1gInSc2	zákoník
použijí	použít	k5eAaPmIp3nP	použít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
věc	věc	k1gFnSc4	věc
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgInSc1d1	obchodní
případ	případ	k1gInSc1	případ
<g/>
)	)	kIx)	)
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
ani	ani	k8xC	ani
ustanovení	ustanovení	k1gNnSc2	ustanovení
obchodního	obchodní	k2eAgNnSc2d1	obchodní
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
věc	věc	k1gFnSc1	věc
není	být	k5eNaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
obyčej	obyčej	k1gInSc1	obyčej
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
pramenů	pramen	k1gInPc2	pramen
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
veřejného	veřejný	k2eAgNnSc2d1	veřejné
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
smluvním	smluvní	k2eAgNnSc6d1	smluvní
právu	právo	k1gNnSc6	právo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Nepsané	nepsaný	k2eAgNnSc1d1	nepsaný
právo	právo	k1gNnSc1	právo
</s>
</p>
<p>
<s>
Psané	psaný	k2eAgNnSc1d1	psané
právo	právo	k1gNnSc1	právo
</s>
</p>
