<p>
<s>
Jack	Jack	k1gInSc1	Jack
Kerouac	Kerouac	k1gInSc1	Kerouac
[	[	kIx(	[
<g/>
dʒ	dʒ	k?	dʒ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jean-Louis	Jean-Louis	k1gFnSc2	Jean-Louis
Lebris	Lebris	k1gFnSc2	Lebris
de	de	k?	de
Kerouac	Kerouac	k1gInSc1	Kerouac
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Lowell	Lowell	k1gInSc1	Lowell
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Petersburg	Petersburg	k1gInSc1	Petersburg
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
představitelů	představitel	k1gMnPc2	představitel
beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
především	především	k9	především
svými	svůj	k3xOyFgInPc7	svůj
autobiografickými	autobiografický	k2eAgInPc7d1	autobiografický
romány	román	k1gInPc7	román
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
dílem	dílo	k1gNnSc7	dílo
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Narození	narození	k1gNnSc2	narození
a	a	k8xC	a
dětství	dětství	k1gNnSc2	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc1	původ
===	===	k?	===
</s>
</p>
<p>
<s>
Předci	předek	k1gMnPc1	předek
Jacka	Jacka	k1gMnSc1	Jacka
Kerouaca	Kerouaca	k1gMnSc1	Kerouaca
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
<g/>
,	,	kIx,	,
tesař	tesař	k1gMnSc1	tesař
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Kirouack	Kirouacka	k1gFnPc2	Kirouacka
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Clémentine	Clémentin	k1gInSc5	Clémentin
Bernierovou	Bernierová	k1gFnSc7	Bernierová
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
do	do	k7c2	do
Nashui	Nashui	k1gNnSc2	Nashui
<g/>
,	,	kIx,	,
města	město	k1gNnPc1	město
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státu	stát	k1gInSc6	stát
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Léon	Léon	k1gMnSc1	Léon
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
Kerouacův	Kerouacův	k2eAgMnSc1d1	Kerouacův
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Léon	Léon	k1gInSc1	Léon
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
reportérem	reportér	k1gMnSc7	reportér
a	a	k8xC	a
obchodníkem	obchodník	k1gMnSc7	obchodník
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgInS	vzít
Gabrielle	Gabrielle	k1gInSc4	Gabrielle
Lévesqueovou	Lévesqueův	k2eAgFnSc7d1	Lévesqueův
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgFnSc4d1	budoucí
Kerouacovu	Kerouacův	k2eAgFnSc4d1	Kerouacova
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodilo	narodit	k5eAaPmAgNnS	narodit
první	první	k4xOgNnSc1	první
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Francis	Francis	k1gFnSc2	Francis
Gerard	Gerard	k1gMnSc1	Gerard
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
pak	pak	k6eAd1	pak
dcera	dcera	k1gFnSc1	dcera
Caroline	Carolin	k1gInSc5	Carolin
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
třetí	třetí	k4xOgMnSc1	třetí
a	a	k8xC	a
poslední	poslední	k2eAgNnSc4d1	poslední
dítě	dítě	k1gNnSc4	dítě
Gabrielle	Gabrielle	k1gNnSc2	Gabrielle
Kerouacové	Kerouacová	k1gFnSc2	Kerouacová
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
Jean-Louis	Jean-Louis	k1gFnSc2	Jean-Louis
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
mu	on	k3xPp3gMnSc3	on
říkali	říkat	k5eAaImAgMnP	říkat
"	"	kIx"	"
<g/>
Ti	ten	k3xDgMnPc1	ten
Jean	Jean	k1gMnSc1	Jean
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
Malý	malý	k2eAgMnSc1d1	malý
Jan	Jan	k1gMnSc1	Jan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Jean-Luis	Jean-Luis	k1gInSc1	Jean-Luis
začal	začít	k5eAaPmAgInS	začít
stýkat	stýkat	k5eAaImF	stýkat
s	s	k7c7	s
nefrancouzskými	francouzský	k2eNgMnPc7d1	francouzský
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
stal	stát	k5eAaPmAgMnS	stát
"	"	kIx"	"
<g/>
Jack	Jack	k1gInSc1	Jack
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
Jackovi	Jackův	k2eAgMnPc1d1	Jackův
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
mu	on	k3xPp3gMnSc3	on
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
devíti	devět	k4xCc2	devět
let	léto	k1gNnPc2	léto
na	na	k7c4	na
revmatickou	revmatický	k2eAgFnSc4d1	revmatická
horečku	horečka	k1gFnSc4	horečka
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starší	k1gMnSc1	starší
bratr	bratr	k1gMnSc1	bratr
Gerard	Gerard	k1gMnSc1	Gerard
<g/>
.	.	kIx.	.
</s>
<s>
Kerouaca	Kerouaca	k6eAd1	Kerouaca
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
hluboce	hluboko	k6eAd1	hluboko
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
,	,	kIx,	,
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
jeho	jeho	k3xOp3gFnSc4	jeho
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
matce	matka	k1gFnSc6	matka
a	a	k8xC	a
promítla	promítnout	k5eAaPmAgFnS	promítnout
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
rodina	rodina	k1gFnSc1	rodina
Kerouacových	Kerouacův	k2eAgMnPc2d1	Kerouacův
mluvila	mluvit	k5eAaImAgFnS	mluvit
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
francouzských	francouzský	k2eAgInPc2d1	francouzský
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Kerouacův	Kerouacův	k2eAgInSc4d1	Kerouacův
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
francouzský	francouzský	k2eAgInSc4d1	francouzský
<g/>
.	.	kIx.	.
</s>
<s>
Angličtinu	angličtina	k1gFnSc4	angličtina
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
učit	učit	k5eAaImF	učit
až	až	k9	až
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
docela	docela	k6eAd1	docela
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
bojoval	bojovat	k5eAaImAgMnS	bojovat
<g/>
.	.	kIx.	.
<g/>
Otec	otec	k1gMnSc1	otec
Jacka	Jacka	k1gMnSc1	Jacka
Leo	Leo	k1gMnSc1	Leo
Kerouac	Kerouac	k1gInSc4	Kerouac
byl	být	k5eAaImAgMnS	být
v	v	k7c4	v
Lowellu	Lowella	k1gFnSc4	Lowella
známou	známý	k2eAgFnSc7d1	známá
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Vznětlivý	vznětlivý	k2eAgMnSc1d1	vznětlivý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
šlechetný	šlechetný	k2eAgMnSc1d1	šlechetný
mírně	mírně	k6eAd1	mírně
výstřední	výstřední	k2eAgMnSc1d1	výstřední
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
náklonností	náklonnost	k1gFnSc7	náklonnost
k	k	k7c3	k
barům	bar	k1gInPc3	bar
a	a	k8xC	a
dostihovým	dostihový	k2eAgInPc3d1	dostihový
závodům	závod	k1gInPc3	závod
si	se	k3xPyFc3	se
zakládal	zakládat	k5eAaImAgInS	zakládat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
nezávislosti	nezávislost	k1gFnSc6	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnil	vlastnit	k5eAaImAgInS	vlastnit
malou	malý	k2eAgFnSc4d1	malá
tiskárnu	tiskárna	k1gFnSc4	tiskárna
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
mohl	moct	k5eAaImAgInS	moct
zaplatit	zaplatit	k5eAaPmF	zaplatit
prostorný	prostorný	k2eAgInSc1d1	prostorný
starý	starý	k2eAgInSc1d1	starý
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
vylila	vylít	k5eAaPmAgFnS	vylít
řeka	řeka	k1gFnSc1	řeka
Merrimack	Merrimacka	k1gFnPc2	Merrimacka
z	z	k7c2	z
břehů	břeh	k1gInPc2	břeh
a	a	k8xC	a
zatopila	zatopit	k5eAaPmAgFnS	zatopit
otcovy	otcův	k2eAgFnPc4d1	otcova
dílny	dílna	k1gFnPc4	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
prodat	prodat	k5eAaPmF	prodat
dům	dům	k1gInSc4	dům
a	a	k8xC	a
hledat	hledat	k5eAaImF	hledat
si	se	k3xPyFc3	se
práci	práce	k1gFnSc4	práce
jako	jako	k8xS	jako
tiskař	tiskař	k1gMnSc1	tiskař
<g/>
,	,	kIx,	,
kdekoli	kdekoli	k6eAd1	kdekoli
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
otcovy	otcův	k2eAgFnSc2d1	otcova
role	role	k1gFnSc2	role
živitele	živitel	k1gMnSc2	živitel
domu	dům	k1gInSc2	dům
a	a	k8xC	a
hlavy	hlava	k1gFnSc2	hlava
rodiny	rodina	k1gFnSc2	rodina
sílil	sílit	k5eAaImAgMnS	sílit
matčin	matčin	k2eAgInSc4d1	matčin
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
zdravotní	zdravotní	k2eAgInPc1d1	zdravotní
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
z	z	k7c2	z
části	část	k1gFnSc2	část
způsobené	způsobený	k2eAgFnSc2d1	způsobená
pitím	pití	k1gNnSc7	pití
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Nenosil	nosit	k5eNaImAgMnS	nosit
domů	domů	k6eAd1	domů
moc	moc	k6eAd1	moc
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
vzrůstající	vzrůstající	k2eAgNnSc1d1	vzrůstající
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Kerouacovými	Kerouacův	k2eAgMnPc7d1	Kerouacův
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
<g/>
Kerouacova	Kerouacův	k2eAgFnSc1d1	Kerouacova
matka	matka	k1gFnSc1	matka
Gabrielle	Gabrielle	k1gFnSc1	Gabrielle
Ange	Ange	k1gFnSc1	Ange
Kerouac	Kerouac	k1gFnSc1	Kerouac
byla	být	k5eAaImAgFnS	být
hluboce	hluboko	k6eAd1	hluboko
věřící	věřící	k2eAgFnSc1d1	věřící
katolička	katolička	k1gFnSc1	katolička
a	a	k8xC	a
o	o	k7c6	o
nedělích	neděle	k1gFnPc6	neděle
Kerouaca	Kerouac	k1gInSc2	Kerouac
a	a	k8xC	a
Caroline	Carolin	k1gInSc5	Carolin
vodila	vodit	k5eAaImAgNnP	vodit
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pawtucktetville	Pawtucktetvilla	k1gFnSc6	Pawtucktetvilla
<g/>
,	,	kIx,	,
frankokanadské	frankokanadský	k2eAgFnSc6d1	frankokanadský
komunitě	komunita	k1gFnSc6	komunita
v	v	k7c6	v
massachusettském	massachusettský	k2eAgNnSc6d1	massachusettský
Lowellu	Lowello	k1gNnSc6	Lowello
nikdy	nikdy	k6eAd1	nikdy
nenašla	najít	k5eNaPmAgFnS	najít
skutečné	skutečný	k2eAgMnPc4d1	skutečný
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Leo	Leo	k1gMnSc1	Leo
Kerouac	Kerouac	k1gFnSc4	Kerouac
pozvolna	pozvolna	k6eAd1	pozvolna
umíral	umírat	k5eAaImAgMnS	umírat
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
ona	onen	k3xDgFnSc1	onen
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
příjem	příjem	k1gInSc4	příjem
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Našla	najít	k5eAaPmAgFnS	najít
si	se	k3xPyFc3	se
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Jack	Jack	k1gMnSc1	Jack
trpěl	trpět	k5eAaImAgMnS	trpět
pocity	pocit	k1gInPc4	pocit
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
nahradit	nahradit	k5eAaPmF	nahradit
otce	otec	k1gMnSc4	otec
v	v	k7c6	v
úloze	úloha	k1gFnSc6	úloha
živitele	živitel	k1gMnSc2	živitel
<g/>
.	.	kIx.	.
</s>
<s>
Strávil	strávit	k5eAaPmAgMnS	strávit
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
nejvíc	nejvíc	k6eAd1	nejvíc
času	čas	k1gInSc2	čas
během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
závěrečného	závěrečný	k2eAgNnSc2d1	závěrečné
stadia	stadion	k1gNnSc2	stadion
choroby	choroba	k1gFnSc2	choroba
a	a	k8xC	a
staral	starat	k5eAaImAgMnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Leo	Leo	k1gMnSc1	Leo
si	se	k3xPyFc3	se
na	na	k7c6	na
svém	svůj	k3xOyFgMnSc6	svůj
synovi	syn	k1gMnSc6	syn
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
slib	slib	k1gInSc1	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
matku	matka	k1gFnSc4	matka
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
postará	postarat	k5eAaPmIp3nS	postarat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
vidět	vidět	k5eAaImF	vidět
pozdější	pozdní	k2eAgFnPc4d2	pozdější
zesílené	zesílený	k2eAgFnPc4d1	zesílená
vazby	vazba	k1gFnPc4	vazba
Jacka	Jacko	k1gNnSc2	Jacko
na	na	k7c4	na
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
<g/>
Jack	Jack	k1gMnSc1	Jack
Kerouac	Kerouac	k1gFnSc4	Kerouac
nejdříve	dříve	k6eAd3	dříve
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
sv.	sv.	kA	sv.
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Francouzského	francouzský	k2eAgMnSc2d1	francouzský
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
do	do	k7c2	do
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
školy	škola	k1gFnSc2	škola
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
lowellskou	lowellský	k2eAgFnSc4d1	lowellský
High	High	k1gMnSc1	High
school	schoolit	k5eAaPmRp2nS	schoolit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
společenské	společenský	k2eAgNnSc4d1	společenské
postavení	postavení	k1gNnSc4	postavení
mezi	mezi	k7c7	mezi
spolužáky	spolužák	k1gMnPc7	spolužák
výbornými	výborný	k2eAgInPc7d1	výborný
výkony	výkon	k1gInPc7	výkon
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
fotbale	fotbal	k1gInSc6	fotbal
a	a	k8xC	a
atletice	atletika	k1gFnSc6	atletika
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gMnSc1	Jack
se	se	k3xPyFc4	se
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
jazzem	jazz	k1gInSc7	jazz
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
hodně	hodně	k6eAd1	hodně
číst	číst	k5eAaImF	číst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
ročníku	ročník	k1gInSc6	ročník
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Mary	Mary	k1gFnSc7	Mary
Caneyovou	Caneyová	k1gFnSc7	Caneyová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
předlouhou	předlouhý	k2eAgFnSc4d1	předlouhá
jeho	jeho	k3xOp3gInSc2	jeho
románu	román	k1gInSc2	román
Maggie	Maggie	k1gFnSc1	Maggie
Cassidyová	Cassidyová	k1gFnSc1	Cassidyová
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
fotbalovým	fotbalový	k2eAgInPc3d1	fotbalový
výkonům	výkon	k1gInPc3	výkon
dostal	dostat	k5eAaPmAgMnS	dostat
dvě	dva	k4xCgFnPc4	dva
nabídky	nabídka	k1gFnPc4	nabídka
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
stipendium	stipendium	k1gNnSc4	stipendium
a	a	k8xC	a
Kerouac	Kerouac	k1gInSc1	Kerouac
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
Kolumbijskou	kolumbijský	k2eAgFnSc4d1	kolumbijská
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
Kerouacovi	Kerouaec	k1gMnSc6	Kerouaec
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
dařilo	dařit	k5eAaImAgNnS	dařit
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInPc3	jeho
výborným	výborný	k2eAgInPc3d1	výborný
esejům	esej	k1gInPc3	esej
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
spolužáci	spolužák	k1gMnPc1	spolužák
najímali	najímat	k5eAaImAgMnP	najímat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
psal	psát	k5eAaImAgMnS	psát
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
<g/>
Jackovo	Jackův	k2eAgNnSc1d1	Jackovo
prvotní	prvotní	k2eAgNnSc1d1	prvotní
nadšení	nadšení	k1gNnSc1	nadšení
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
začalo	začít	k5eAaPmAgNnS	začít
vytrácet	vytrácet	k5eAaImF	vytrácet
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc4d1	Dlouhé
cestování	cestování	k1gNnSc4	cestování
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
celodenní	celodenní	k2eAgNnSc1d1	celodenní
vyučování	vyučování	k1gNnSc1	vyučování
a	a	k8xC	a
večerní	večerní	k2eAgInSc1d1	večerní
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
trénink	trénink	k1gInSc1	trénink
ho	on	k3xPp3gInSc4	on
příliš	příliš	k6eAd1	příliš
zaměstnávaly	zaměstnávat	k5eAaImAgInP	zaměstnávat
a	a	k8xC	a
Jack	Jack	k1gInSc1	Jack
začal	začít	k5eAaPmAgInS	začít
vyučování	vyučování	k1gNnSc4	vyučování
vynechávat	vynechávat	k5eAaImF	vynechávat
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
klubů	klub	k1gInPc2	klub
poslouchat	poslouchat	k5eAaImF	poslouchat
jazz	jazz	k1gInSc4	jazz
a	a	k8xC	a
na	na	k7c4	na
večírky	večírek	k1gInPc4	večírek
spolužáků	spolužák	k1gMnPc2	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
scházel	scházet	k5eAaImAgMnS	scházet
s	s	k7c7	s
Mary	Mary	k1gFnSc7	Mary
Caneyovou	Caneyový	k2eAgFnSc7d1	Caneyový
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gInSc1	jejich
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
dost	dost	k6eAd1	dost
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
odloučení	odloučení	k1gNnSc3	odloučení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
pak	pak	k6eAd1	pak
Kerouac	Kerouac	k1gFnSc4	Kerouac
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
žádnou	žádný	k3yNgFnSc4	žádný
dívku	dívka	k1gFnSc4	dívka
nemiloval	milovat	k5eNaImAgMnS	milovat
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
Mary	Mary	k1gFnSc1	Mary
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Beat	beat	k1gInSc4	beat
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1942	[number]	k4	1942
se	se	k3xPyFc4	se
Kerouac	Kerouac	k1gInSc1	Kerouac
nepohodl	pohodnout	k5eNaPmAgInS	pohodnout
s	s	k7c7	s
trenérem	trenér	k1gMnSc7	trenér
fotbalu	fotbal	k1gInSc2	fotbal
a	a	k8xC	a
opustil	opustit	k5eAaPmAgMnS	opustit
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Lowellu	Lowell	k1gInSc2	Lowell
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
obdržel	obdržet	k5eAaPmAgInS	obdržet
povolávací	povolávací	k2eAgInSc1d1	povolávací
rozkaz	rozkaz	k1gInSc1	rozkaz
do	do	k7c2	do
Námořnictva	námořnictvo	k1gNnSc2	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
byl	být	k5eAaImAgInS	být
však	však	k9	však
brzy	brzy	k6eAd1	brzy
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
pro	pro	k7c4	pro
psychické	psychický	k2eAgInPc4d1	psychický
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
proto	proto	k8xC	proto
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
jako	jako	k8xC	jako
pomocná	pomocný	k2eAgFnSc1d1	pomocná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
do	do	k7c2	do
Liverpoolu	Liverpool	k1gInSc2	Liverpool
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
Kerouac	Kerouac	k1gFnSc1	Kerouac
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Allenem	Allen	k1gMnSc7	Allen
Ginsbergem	Ginsberg	k1gMnSc7	Ginsberg
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
i	i	k9	i
s	s	k7c7	s
Williamem	William	k1gInSc7	William
Sewardem	Seward	k1gMnSc7	Seward
Burroughsem	Burroughs	k1gMnSc7	Burroughs
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
Burroughs	Burroughs	k1gInSc1	Burroughs
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
starší	starší	k1gMnSc1	starší
než	než	k8xS	než
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
i	i	k9	i
Kerouac	Kerouac	k1gInSc4	Kerouac
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
"	"	kIx"	"
<g/>
učitelem	učitel	k1gMnSc7	učitel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Frankie	Frankie	k1gFnSc1	Frankie
Edith	Edith	k1gInSc4	Edith
Parkerovou	Parkerová	k1gFnSc7	Parkerová
ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byl	být	k5eAaImAgMnS	být
propuštěn	propustit	k5eAaPmNgMnS	propustit
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
za	za	k7c4	za
krytí	krytí	k1gNnSc4	krytí
vraždy	vražda	k1gFnSc2	vražda
Davida	David	k1gMnSc2	David
Kammerera	Kammerero	k1gNnSc2	Kammerero
Lucienem	Lucien	k1gMnSc7	Lucien
Carrem	Carr	k1gMnSc7	Carr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znamenalo	znamenat	k5eAaImAgNnS	znamenat
odcizení	odcizení	k1gNnSc1	odcizení
se	se	k3xPyFc4	se
rodině	rodina	k1gFnSc6	rodina
a	a	k8xC	a
pasování	pasování	k1gNnSc6	pasování
se	se	k3xPyFc4	se
do	do	k7c2	do
role	role	k1gFnSc2	role
psance	psanec	k1gMnSc2	psanec
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Edie	Edie	k1gFnSc7	Edie
Parkerovou	Parkerová	k1gFnSc7	Parkerová
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
jen	jen	k9	jen
2	[number]	k4	2
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
Jack	Jack	k1gMnSc1	Jack
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Nealem	Neal	k1gMnSc7	Neal
Cassadym	Cassadym	k1gInSc4	Cassadym
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
navždy	navždy	k6eAd1	navždy
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gFnSc7	jeho
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
Joan	Joano	k1gNnPc2	Joano
Havertyová	Havertyová	k1gFnSc1	Havertyová
<g/>
,	,	kIx,	,
servírka	servírka	k1gFnSc1	servírka
jež	jenž	k3xRgNnSc4	jenž
předtím	předtím	k6eAd1	předtím
žila	žít	k5eAaImAgFnS	žít
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Cannastrou	Cannastra	k1gFnSc7	Cannastra
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
toto	tento	k3xDgNnSc1	tento
manželství	manželství	k1gNnSc1	manželství
dlouho	dlouho	k6eAd1	dlouho
nevydrželo	vydržet	k5eNaPmAgNnS	vydržet
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
jen	jen	k9	jen
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
neslo	nést	k5eAaImAgNnS	nést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nepříjemné	příjemný	k2eNgInPc4d1	nepříjemný
následky	následek	k1gInPc7	následek
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
o	o	k7c4	o
určení	určení	k1gNnSc4	určení
otcovství	otcovství	k1gNnSc2	otcovství
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Joan	Joan	k1gInSc4	Joan
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
vyšel	vyjít	k5eAaPmAgInS	vyjít
román	román	k1gInSc1	román
Maloměsto	maloměsto	k1gNnSc4	maloměsto
<g/>
,	,	kIx,	,
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
<g/>
,	,	kIx,	,
Kerouac	Kerouac	k1gInSc4	Kerouac
jezdil	jezdit	k5eAaImAgMnS	jezdit
stopem	stop	k1gInSc7	stop
po	po	k7c6	po
USA	USA	kA	USA
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
brzdař	brzdař	k1gMnSc1	brzdař
(	(	kIx(	(
<g/>
a	a	k8xC	a
dělal	dělat	k5eAaImAgMnS	dělat
také	také	k9	také
jiné	jiný	k2eAgNnSc4d1	jiné
málo	málo	k1gNnSc4	málo
kvalifikované	kvalifikovaný	k2eAgFnSc2d1	kvalifikovaná
pomocné	pomocný	k2eAgFnSc2d1	pomocná
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
také	také	k9	také
podruhé	podruhé	k6eAd1	podruhé
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
s	s	k7c7	s
flebitidou	flebitida	k1gFnSc7	flebitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
žil	žíla	k1gFnPc2	žíla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vydal	vydat	k5eAaPmAgMnS	vydat
román	román	k1gInSc4	román
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
veřejně	veřejně	k6eAd1	veřejně
známou	známý	k2eAgFnSc7d1	známá
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
finančně	finančně	k6eAd1	finančně
zajištěným	zajištěný	k2eAgMnSc7d1	zajištěný
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
pít	pít	k5eAaImF	pít
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdykoli	kdykoli	k6eAd1	kdykoli
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
alkoholismu	alkoholismus	k1gInSc2	alkoholismus
<g/>
,	,	kIx,	,
na	na	k7c4	na
krvácení	krvácení	k1gNnSc4	krvácení
jícnových	jícnový	k2eAgFnPc2d1	jícnová
křečových	křečový	k2eAgFnPc2d1	křečová
žil	žíla	k1gFnPc2	žíla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jack	Jack	k1gMnSc1	Jack
Kerouac	Kerouac	k1gInSc4	Kerouac
byl	být	k5eAaImAgMnS	být
extrémně	extrémně	k6eAd1	extrémně
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
a	a	k8xC	a
citlivý	citlivý	k2eAgMnSc1d1	citlivý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc4	jeho
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
pramenily	pramenit	k5eAaImAgFnP	pramenit
mj.	mj.	kA	mj.
i	i	k8xC	i
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
novátorská	novátorský	k2eAgFnSc1d1	novátorská
tvorba	tvorba	k1gFnSc1	tvorba
nebyla	být	k5eNaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
způsobilou	způsobilý	k2eAgFnSc4d1	způsobilá
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
touhu	touha	k1gFnSc4	touha
poznávat	poznávat	k5eAaImF	poznávat
a	a	k8xC	a
vstřebávat	vstřebávat	k5eAaImF	vstřebávat
stále	stále	k6eAd1	stále
něco	něco	k3yInSc4	něco
nového	nový	k2eAgNnSc2d1	nové
zažil	zažít	k5eAaPmAgMnS	zažít
i	i	k9	i
období	období	k1gNnSc4	období
těžkých	těžký	k2eAgFnPc2d1	těžká
depresí	deprese	k1gFnPc2	deprese
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
osamění	osamění	k1gNnSc2	osamění
<g/>
.	.	kIx.	.
</s>
<s>
Domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemře	zemřít	k5eAaPmIp3nS	zemřít
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnPc1	jeho
oblíbení	oblíbený	k2eAgMnPc1d1	oblíbený
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Emily	Emil	k1gMnPc4	Emil
Dickinsonová	Dickinsonový	k2eAgFnSc1d1	Dickinsonová
a	a	k8xC	a
Henry	Henry	k1gMnSc1	Henry
David	David	k1gMnSc1	David
Thoreau	Thoreaus	k1gInSc2	Thoreaus
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
jako	jako	k8xS	jako
o	o	k7c6	o
zuboženém	zubožený	k2eAgMnSc6d1	zubožený
papírovém	papírový	k2eAgMnSc6d1	papírový
žebrákovi	žebrák	k1gMnSc6	žebrák
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
období	období	k1gNnSc1	období
života	život	k1gInSc2	život
mu	on	k3xPp3gMnSc3	on
kromě	kromě	k7c2	kromě
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
četby	četba	k1gFnSc2	četba
a	a	k8xC	a
podpory	podpora	k1gFnSc2	podpora
přátel	přítel	k1gMnPc2	přítel
pomohl	pomoct	k5eAaPmAgMnS	pomoct
překonávat	překonávat	k5eAaImF	překonávat
buddhismus	buddhismus	k1gInSc4	buddhismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Kerouac	Kerouac	k6eAd1	Kerouac
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
beatnici	beatnik	k1gMnPc1	beatnik
<g/>
)	)	kIx)	)
o	o	k7c4	o
maximální	maximální	k2eAgFnSc4d1	maximální
upřímnost	upřímnost	k1gFnSc4	upřímnost
a	a	k8xC	a
spontánnost	spontánnost	k1gFnSc4	spontánnost
díla	dílo	k1gNnSc2	dílo
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
umělecké	umělecký	k2eAgInPc4d1	umělecký
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Pohrdal	pohrdat	k5eAaImAgInS	pohrdat
korigováním	korigování	k1gNnSc7	korigování
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
způsobem	způsob	k1gInSc7	způsob
druhotné	druhotný	k2eAgFnSc2d1	druhotná
morální	morální	k2eAgFnPc4d1	morální
cenzury	cenzura	k1gFnPc4	cenzura
vnucované	vnucovaný	k2eAgFnPc4d1	vnucovaná
podvědomím	podvědomí	k1gNnSc7	podvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Literáta	literát	k1gMnSc4	literát
přirovnával	přirovnávat	k5eAaImAgMnS	přirovnávat
k	k	k7c3	k
jazzovému	jazzový	k2eAgMnSc3d1	jazzový
saxofonistovi	saxofonista	k1gMnSc3	saxofonista
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
hledat	hledat	k5eAaImF	hledat
jazykové	jazykový	k2eAgNnSc4d1	jazykové
vyjádření	vyjádření	k1gNnSc4	vyjádření
nerušeného	rušený	k2eNgNnSc2d1	nerušené
proudění	proudění	k1gNnSc2	proudění
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
lze	lze	k6eAd1	lze
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
vycítit	vycítit	k5eAaPmF	vycítit
pronikavý	pronikavý	k2eAgInSc4d1	pronikavý
pocit	pocit	k1gInSc4	pocit
stísněnosti	stísněnost	k1gFnSc2	stísněnost
<g/>
,	,	kIx,	,
zoufalství	zoufalství	k1gNnSc2	zoufalství
a	a	k8xC	a
bolesti	bolest	k1gFnSc2	bolest
žití	žití	k1gNnSc2	žití
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
většinou	většinou	k6eAd1	většinou
převažuje	převažovat	k5eAaImIp3nS	převažovat
nad	nad	k7c7	nad
entuziasmem	entuziasmus	k1gInSc7	entuziasmus
a	a	k8xC	a
občas	občas	k6eAd1	občas
násilným	násilný	k2eAgNnSc7d1	násilné
veselím	veselí	k1gNnSc7	veselí
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Clellon	Clellon	k1gInSc4	Clellon
Holmes	Holmes	k1gInSc1	Holmes
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k9	a
najednou	najednou	k6eAd1	najednou
mnou	já	k3xPp1nSc7	já
zachvěl	zachvět	k5eAaPmAgMnS	zachvět
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kerouac	Kerouac	k1gInSc1	Kerouac
svou	svůj	k3xOyFgFnSc4	svůj
čtyřicítku	čtyřicítka	k1gFnSc4	čtyřicítka
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
nepřežije	přežít	k5eNaPmIp3nS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
hltavost	hltavost	k1gFnSc1	hltavost
<g/>
,	,	kIx,	,
psychická	psychický	k2eAgFnSc1d1	psychická
zranitelnost	zranitelnost	k1gFnSc1	zranitelnost
<g/>
,	,	kIx,	,
taková	takový	k3xDgFnSc1	takový
jednostrannost	jednostrannost	k1gFnSc1	jednostrannost
musí	muset	k5eAaImIp3nS	muset
člověka	člověk	k1gMnSc4	člověk
nakonec	nakonec	k6eAd1	nakonec
vymačkat	vymačkat	k5eAaPmF	vymačkat
a	a	k8xC	a
Kerouac	Kerouac	k1gFnSc4	Kerouac
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
odvrátit	odvrátit	k5eAaPmF	odvrátit
se	se	k3xPyFc4	se
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
zničujícího	zničující	k2eAgNnSc2d1	zničující
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
poezie	poezie	k1gFnSc1	poezie
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
asijskými	asijský	k2eAgInPc7d1	asijský
vlivy	vliv	k1gInPc7	vliv
<g/>
,	,	kIx,	,
především	především	k9	především
zen-buddhismem	zenuddhismus	k1gInSc7	zen-buddhismus
a	a	k8xC	a
starou	starý	k2eAgFnSc7d1	stará
čínskou	čínský	k2eAgFnSc7d1	čínská
poezií	poezie	k1gFnSc7	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
ho	on	k3xPp3gNnSc4	on
především	především	k9	především
forma	forma	k1gFnSc1	forma
haiku	haik	k1gInSc2	haik
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
báseň	báseň	k1gFnSc1	báseň
o	o	k7c6	o
17	[number]	k4	17
slabikách	slabika	k1gFnPc6	slabika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ucelenou	ucelený	k2eAgFnSc4d1	ucelená
vizi	vize	k1gFnSc4	vize
života	život	k1gInSc2	život
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
krátkých	krátký	k2eAgInPc6d1	krátký
řádcích	řádek	k1gInPc6	řádek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
blues	blues	k1gNnSc4	blues
(	(	kIx(	(
<g/>
tříveršové	tříveršový	k2eAgFnSc2d1	tříveršová
sloky	sloka	k1gFnSc2	sloka
s	s	k7c7	s
opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
nepatrně	nepatrně	k6eAd1	nepatrně
variabilním	variabilní	k2eAgInSc7d1	variabilní
prvním	první	k4xOgInSc7	první
veršem	verš	k1gInSc7	verš
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
smutné	smutný	k2eAgFnPc1d1	smutná
či	či	k8xC	či
melancholické	melancholický	k2eAgFnPc1d1	melancholická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kerouac	Kerouac	k6eAd1	Kerouac
přesně	přesně	k6eAd1	přesně
nedodržoval	dodržovat	k5eNaImAgMnS	dodržovat
tyto	tento	k3xDgFnPc4	tento
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
mu	on	k3xPp3gMnSc3	on
spíše	spíše	k9	spíše
o	o	k7c4	o
dodržení	dodržení	k1gNnSc4	dodržení
nálady	nálada	k1gFnSc2	nálada
a	a	k8xC	a
rytmu	rytmus	k1gInSc2	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
ho	on	k3xPp3gMnSc4	on
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
básně	báseň	k1gFnPc4	báseň
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
chorusy	chorus	k1gInPc4	chorus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poezie	poezie	k1gFnSc1	poezie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
San	San	k?	San
Francisco	Francisco	k6eAd1	Francisco
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
</s>
</p>
<p>
<s>
Mexico	Mexico	k6eAd1	Mexico
City	city	k1gNnSc1	city
Blues	blues	k1gNnSc1	blues
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
</s>
</p>
<p>
<s>
Rozprášené	rozprášený	k2eAgFnPc4d1	rozprášená
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
(	(	kIx(	(
<g/>
sestaveno	sestavit	k5eAaPmNgNnS	sestavit
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
)	)	kIx)	)
<g/>
Prozaická	prozaický	k2eAgFnSc1d1	prozaická
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mnohem	mnohem	k6eAd1	mnohem
významnější	významný	k2eAgFnSc1d2	významnější
než	než	k8xS	než
jeho	jeho	k3xOp3gFnSc1	jeho
básnická	básnický	k2eAgFnSc1d1	básnická
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
tvorba	tvorba	k1gFnSc1	tvorba
prozaická	prozaický	k2eAgFnSc1d1	prozaická
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
čerpá	čerpat	k5eAaImIp3nS	čerpat
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
zážitků	zážitek	k1gInPc2	zážitek
<g/>
,	,	kIx,	,
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
toulal	toulat	k5eAaImAgMnS	toulat
po	po	k7c6	po
Státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
velmi	velmi	k6eAd1	velmi
uvolněně	uvolněně	k6eAd1	uvolněně
a	a	k8xC	a
jak	jak	k6eAd1	jak
sám	sám	k3xTgMnSc1	sám
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
vcelku	vcelku	k6eAd1	vcelku
věrohodné	věrohodný	k2eAgNnSc1d1	věrohodné
<g/>
.	.	kIx.	.
</s>
<s>
Toulal	toulal	k1gMnSc1	toulal
se	se	k3xPyFc4	se
beze	beze	k7c2	beze
všeho	všecek	k3xTgNnSc2	všecek
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
batohem	batoh	k1gInSc7	batoh
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
vandráka	vandrák	k1gMnSc4	vandrák
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
významech	význam	k1gInPc6	význam
toho	ten	k3xDgNnSc2	ten
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maloměsto	maloměsto	k1gNnSc1	maloměsto
<g/>
,	,	kIx,	,
velkoměsto	velkoměsto	k1gNnSc1	velkoměsto
-	-	kIx~	-
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
též	též	k9	též
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Městečko	městečko	k1gNnSc1	městečko
a	a	k8xC	a
velkoměsto	velkoměsto	k1gNnSc1	velkoměsto
</s>
</p>
<p>
<s>
Doctor	Doctor	k1gInSc1	Doctor
Sax	sax	k1gInSc1	sax
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
</s>
</p>
<p>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
-	-	kIx~	-
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jeho	jeho	k3xOp3gInSc1	jeho
častý	častý	k2eAgInSc1d1	častý
motiv	motiv	k1gInSc1	motiv
-	-	kIx~	-
toulání	toulání	k1gNnSc1	toulání
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
cesta	cesta	k1gFnSc1	cesta
od	od	k7c2	od
tahu	tah	k1gInSc2	tah
k	k	k7c3	k
tahu	tah	k1gInSc3	tah
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
Joan	Joana	k1gFnPc2	Joana
Andersové	Andersové	k2eAgFnPc2d1	Andersové
<g/>
,	,	kIx,	,
co	co	k9	co
vlastně	vlastně	k9	vlastně
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
dělali	dělat	k5eAaImAgMnP	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
údajně	údajně	k6eAd1	údajně
napsal	napsat	k5eAaPmAgInS	napsat
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
týdnů	týden	k1gInPc2	týden
na	na	k7c4	na
30	[number]	k4	30
metrovou	metrový	k2eAgFnSc4d1	metrová
roli	role	k1gFnSc4	role
japonského	japonský	k2eAgInSc2d1	japonský
kreslicího	kreslicí	k2eAgInSc2d1	kreslicí
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
členění	členění	k1gNnSc2	členění
na	na	k7c4	na
odstavce	odstavec	k1gInPc4	odstavec
<g/>
,	,	kIx,	,
kapitoly	kapitola	k1gFnPc4	kapitola
a	a	k8xC	a
bez	bez	k7c2	bez
interpunkčních	interpunkční	k2eAgNnPc2d1	interpunkční
znamének	znaménko	k1gNnPc2	znaménko
<g/>
.	.	kIx.	.
6	[number]	k4	6
let	léto	k1gNnPc2	léto
pak	pak	k6eAd1	pak
trvalo	trvat	k5eAaImAgNnS	trvat
<g/>
,	,	kIx,	,
než	než	k8xS	než
dílo	dílo	k1gNnSc4	dílo
přepsal	přepsat	k5eAaPmAgInS	přepsat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
vydáno	vydat	k5eAaPmNgNnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
manifestem	manifest	k1gInSc7	manifest
beatniků	beatnik	k1gMnPc2	beatnik
<g/>
.	.	kIx.	.
</s>
<s>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
začínajícím	začínající	k2eAgMnSc6d1	začínající
spisovateli	spisovatel	k1gMnSc6	spisovatel
jménem	jméno	k1gNnSc7	jméno
Sal	Sal	k1gMnSc2	Sal
Paradise	Paradise	k1gFnSc2	Paradise
(	(	kIx(	(
<g/>
paradise	paradise	k1gFnSc1	paradise
=	=	kIx~	=
ráj	ráj	k1gInSc1	ráj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Deanem	Dean	k1gMnSc7	Dean
toulá	toulat	k5eAaImIp3nS	toulat
po	po	k7c6	po
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
vzrušení	vzrušení	k1gNnSc1	vzrušení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
hledají	hledat	k5eAaImIp3nP	hledat
podstatu	podstata	k1gFnSc4	podstata
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
postavách	postava	k1gFnPc6	postava
lze	lze	k6eAd1	lze
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
další	další	k2eAgMnPc4d1	další
beatniky	beatnik	k1gMnPc4	beatnik
pod	pod	k7c7	pod
pozměněnými	pozměněný	k2eAgNnPc7d1	pozměněné
jmény	jméno	k1gNnPc7	jméno
<g/>
.	.	kIx.	.
<g/>
Tato	tento	k3xDgFnSc1	tento
tři	tři	k4xCgFnPc4	tři
díla	dílo	k1gNnPc4	dílo
psaná	psaný	k2eAgNnPc4d1	psané
na	na	k7c6	na
konci	konec	k1gInSc6	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
chtěl	chtít	k5eAaImAgMnS	chtít
původně	původně	k6eAd1	původně
spojit	spojit	k5eAaPmF	spojit
do	do	k7c2	do
trilogie	trilogie	k1gFnSc2	trilogie
Duluozova	Duluozův	k2eAgFnSc1d1	Duluozův
legenda	legenda	k1gFnSc1	legenda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Duluozovy	Duluozův	k2eAgFnPc1d1	Duluozův
marnosti	marnost	k1gFnPc1	marnost
-	-	kIx~	-
román	román	k1gInSc1	román
zhruba	zhruba	k6eAd1	zhruba
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vize	vize	k1gFnSc1	vize
Codyho	Cody	k1gMnSc2	Cody
-	-	kIx~	-
též	též	k9	též
Codyho	Cody	k1gMnSc2	Cody
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaPmNgNnS	napsat
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
zařazováno	zařazován	k2eAgNnSc1d1	zařazováno
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Codyho	Cody	k1gMnSc2	Cody
postupně	postupně	k6eAd1	postupně
zosobňuje	zosobňovat	k5eAaImIp3nS	zosobňovat
některé	některý	k3yIgFnPc4	některý
velké	velký	k2eAgFnPc4d1	velká
postavy	postava	k1gFnPc4	postava
amerických	americký	k2eAgFnPc2d1	americká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
:	:	kIx,	:
kovboje	kovboj	k1gMnPc4	kovboj
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc1	soudce
"	"	kIx"	"
<g/>
lynče	lynč	k1gInSc2	lynč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
F.	F.	kA	F.
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
jazzu	jazz	k1gInSc2	jazz
Lestera	Lester	k1gMnSc2	Lester
Younga	Young	k1gMnSc2	Young
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
ztotožní	ztotožnit	k5eAaPmIp3nP	ztotožnit
i	i	k9	i
vypravěč	vypravěč	k1gMnSc1	vypravěč
Jack	Jack	k1gMnSc1	Jack
Duoloz	Duoloz	k1gMnSc1	Duoloz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
plno	plno	k6eAd1	plno
narážek	narážka	k1gFnPc2	narážka
na	na	k7c4	na
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
Kerouac	Kerouac	k1gInSc1	Kerouac
obdivoval	obdivovat	k5eAaImAgInS	obdivovat
(	(	kIx(	(
<g/>
Jack	Jack	k1gMnSc1	Jack
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
,	,	kIx,	,
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Genet	Genet	k1gMnSc1	Genet
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
Kerouac	Kerouac	k1gFnSc4	Kerouac
dále	daleko	k6eAd2	daleko
napsal	napsat	k5eAaBmAgMnS	napsat
několik	několik	k4yIc4	několik
cestopisných	cestopisný	k2eAgNnPc2d1	cestopisné
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
reportáže	reportáž	k1gFnSc2	reportáž
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popisuje	popisovat	k5eAaImIp3nS	popisovat
své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
USA	USA	kA	USA
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Osamělý	osamělý	k2eAgMnSc1d1	osamělý
poutník	poutník	k1gMnSc1	poutník
-	-	kIx~	-
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
osmi	osm	k4xCc2	osm
povídek	povídka	k1gFnPc2	povídka
reflektujících	reflektující	k2eAgFnPc2d1	reflektující
autorovy	autorův	k2eAgFnPc4d1	autorova
cesty	cesta	k1gFnPc4	cesta
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dharmoví	Dharmový	k2eAgMnPc1d1	Dharmový
tuláci	tulák	k1gMnPc1	tulák
-	-	kIx~	-
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
velice	velice	k6eAd1	velice
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
hnutí	hnutí	k1gNnSc3	hnutí
hippies	hippies	k1gInSc1	hippies
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
často	často	k6eAd1	často
cestovali	cestovat	k5eAaImAgMnP	cestovat
s	s	k7c7	s
ruksakem	ruksak	k1gInSc7	ruksak
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
lze	lze	k6eAd1	lze
právem	právem	k6eAd1	právem
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
klidnější	klidný	k2eAgFnSc1d2	klidnější
než	než	k8xS	než
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
patrně	patrně	k6eAd1	patrně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kerouac	Kerouac	k1gFnSc1	Kerouac
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
přívržencem	přívrženec	k1gMnSc7	přívrženec
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
střetávají	střetávat	k5eAaImIp3nP	střetávat
myšlenky	myšlenka	k1gFnPc1	myšlenka
západní	západní	k2eAgFnSc2d1	západní
civilizace	civilizace	k1gFnSc2	civilizace
reprezentované	reprezentovaný	k2eAgNnSc4d1	reprezentované
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
východní	východní	k2eAgFnSc2d1	východní
civilizace	civilizace	k1gFnSc2	civilizace
reprezentované	reprezentovaný	k2eAgFnSc2d1	reprezentovaná
buddhismem	buddhismus	k1gInSc7	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
hledali	hledat	k5eAaImAgMnP	hledat
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
učení	učení	k1gNnSc6	učení
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
našeho	náš	k3xOp1gInSc2	náš
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
složitá	složitý	k2eAgFnSc1d1	složitá
problematika	problematika	k1gFnSc1	problematika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
spíše	spíše	k9	spíše
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
dharmoví	dharmový	k2eAgMnPc1d1	dharmový
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
dharma	dharma	k1gFnSc1	dharma
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pro	pro	k7c4	pro
autora	autor	k1gMnSc4	autor
znamená	znamenat	k5eAaImIp3nS	znamenat
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinou	Hrdina	k1gMnSc7	Hrdina
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
je	on	k3xPp3gInPc4	on
Japhy	Japh	k1gInPc4	Japh
Ryder	Rydra	k1gFnPc2	Rydra
(	(	kIx(	(
<g/>
předlohou	předloha	k1gFnSc7	předloha
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
znalec	znalec	k1gMnSc1	znalec
východní	východní	k2eAgFnSc2d1	východní
filosofie	filosofie	k1gFnSc2	filosofie
Gary	Gara	k1gMnSc2	Gara
Snyder	Snydra	k1gFnPc2	Snydra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
vypravěč	vypravěč	k1gMnSc1	vypravěč
utíká	utíkat	k5eAaImIp3nS	utíkat
do	do	k7c2	do
lesů	les	k1gInPc2	les
od	od	k7c2	od
naší	náš	k3xOp1gFnSc2	náš
civilizace	civilizace	k1gFnSc2	civilizace
(	(	kIx(	(
<g/>
samozřejmě	samozřejmě	k6eAd1	samozřejmě
s	s	k7c7	s
ruksakem	ruksak	k1gInSc7	ruksak
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říjen	říjen	k1gInSc1	říjen
v	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
zemi	zem	k1gFnSc6	zem
</s>
</p>
<p>
<s>
Podzemníci	Podzemník	k1gMnPc1	Podzemník
-	-	kIx~	-
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
během	během	k7c2	během
3	[number]	k4	3
dnů	den	k1gInPc2	den
v	v	k7c6	v
benzendrinovém	benzendrinový	k2eAgNnSc6d1	benzendrinový
opojení	opojení	k1gNnSc6	opojení
<g/>
.	.	kIx.	.
</s>
<s>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
autorově	autorův	k2eAgInSc6d1	autorův
milostném	milostný	k2eAgInSc6d1	milostný
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
černoškou	černoška	k1gFnSc7	černoška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
směšnými	směšný	k2eAgNnPc7d1	směšné
jmény	jméno	k1gNnPc7	jméno
<g/>
,	,	kIx,	,
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
přední	přední	k2eAgNnPc4d1	přední
jména	jméno	k1gNnPc4	jméno
beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
(	(	kIx(	(
<g/>
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
<g/>
,	,	kIx,	,
Gregory	Gregor	k1gMnPc4	Gregor
Corso	Corsa	k1gFnSc5	Corsa
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
muži	muž	k1gMnPc1	muž
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neumějí	umět	k5eNaImIp3nP	umět
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
udržet	udržet	k5eAaPmF	udržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maggie	Maggie	k1gFnSc1	Maggie
Cassidyová	Cassidyová	k1gFnSc1	Cassidyová
-	-	kIx~	-
1959	[number]	k4	1959
</s>
</p>
<p>
<s>
Andělé	anděl	k1gMnPc1	anděl
pustiny	pustina	k1gFnSc2	pustina
-	-	kIx~	-
psáno	psát	k5eAaImNgNnS	psát
v	v	k7c6	v
letech	let	k1gInPc6	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
knihy	kniha	k1gFnSc2	kniha
autor	autor	k1gMnSc1	autor
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
v	v	k7c6	v
pustině	pustina	k1gFnSc6	pustina
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc3	část
spisovatel	spisovatel	k1gMnSc1	spisovatel
popisuje	popisovat	k5eAaImIp3nS	popisovat
putování	putování	k1gNnSc4	putování
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
,	,	kIx,	,
New	New	k1gFnSc7	New
Yorkem	York	k1gInSc7	York
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Londýnem	Londýn	k1gInSc7	Londýn
-	-	kIx~	-
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
sám	sám	k3xTgMnSc1	sám
uprostřed	uprostřed	k7c2	uprostřed
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
společných	společný	k2eAgInPc2d1	společný
rysů	rys	k1gInPc2	rys
s	s	k7c7	s
Dharmovými	Dharmův	k2eAgMnPc7d1	Dharmův
tuláky	tulák	k1gMnPc7	tulák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Big	Big	k?	Big
Sur	Sur	k1gFnSc1	Sur
-	-	kIx~	-
1962	[number]	k4	1962
-	-	kIx~	-
podrobný	podrobný	k2eAgInSc1d1	podrobný
přepis	přepis	k1gInSc1	přepis
propadání	propadání	k1gNnSc2	propadání
šílenství	šílenství	k1gNnSc2	šílenství
a	a	k8xC	a
paranoie	paranoia	k1gFnSc2	paranoia
<g/>
,	,	kIx,	,
totálního	totální	k2eAgInSc2d1	totální
rozkladu	rozklad	k1gInSc2	rozklad
a	a	k8xC	a
rozpadu	rozpad	k1gInSc2	rozpad
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Satori	Satori	k6eAd1	Satori
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
-	-	kIx~	-
1966	[number]	k4	1966
</s>
</p>
<p>
<s>
Mag	Maga	k1gFnPc2	Maga
-	-	kIx~	-
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Pic	pic	k0	pic
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
-	-	kIx~	-
Další	další	k2eAgFnSc1d1	další
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
toulání	toulání	k1gNnSc6	toulání
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
o	o	k7c6	o
malém	malý	k2eAgNnSc6d1	malé
černošském	černošský	k2eAgNnSc6d1	černošské
chlapci	chlapec	k1gMnPc1	chlapec
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
umře	umřít	k5eAaPmIp3nS	umřít
milovaný	milovaný	k2eAgMnSc1d1	milovaný
děda	děda	k1gMnSc1	děda
<g/>
.	.	kIx.	.
</s>
<s>
Mag	Maga	k1gFnPc2	Maga
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
opatrování	opatrování	k1gNnSc2	opatrování
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
tetě	teta	k1gFnSc3	teta
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ho	on	k3xPp3gNnSc4	on
unese	unést	k5eAaPmIp3nS	unést
jeho	jeho	k3xOp3gMnSc1	jeho
světaznalý	světaznalý	k2eAgMnSc1d1	světaznalý
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
roztáčí	roztáčet	k5eAaImIp3nS	roztáčet
koloběh	koloběh	k1gInSc1	koloběh
stopování	stopování	k1gNnSc2	stopování
<g/>
,	,	kIx,	,
improvizace	improvizace	k1gFnSc2	improvizace
a	a	k8xC	a
života	život	k1gInSc2	život
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bratry	bratr	k1gMnPc4	bratr
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
bojem	boj	k1gInSc7	boj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
víc	hodně	k6eAd2	hodně
cítí	cítit	k5eAaImIp3nS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Próza	próza	k1gFnSc1	próza
===	===	k?	===
</s>
</p>
<p>
<s>
Atop	Atop	k1gInSc1	Atop
an	an	k?	an
Underwood	underwood	k1gInSc1	underwood
<g/>
:	:	kIx,	:
Early	earl	k1gMnPc4	earl
Stories	Storiesa	k1gFnPc2	Storiesa
and	and	k?	and
Other	Other	k1gInSc1	Other
Writings	Writings	k1gInSc1	Writings
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
670	[number]	k4	670
<g/>
-	-	kIx~	-
<g/>
88822	[number]	k4	88822
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Orpheus	Orpheus	k1gMnSc1	Orpheus
Emerged	Emerged	k1gMnSc1	Emerged
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7434	[number]	k4	7434
<g/>
-	-	kIx~	-
<g/>
7514	[number]	k4	7514
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
And	Anda	k1gFnPc2	Anda
the	the	k?	the
Hippos	Hippos	k1gMnSc1	Hippos
Were	Wer	k1gFnSc2	Wer
Boiled	Boiled	k1gMnSc1	Boiled
in	in	k?	in
Their	Their	k1gInSc1	Their
Tanks	Tanks	k1gInSc1	Tanks
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
publikované	publikovaný	k2eAgNnSc4d1	publikované
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Kerouac	Kerouac	k1gInSc1	Kerouac
napsal	napsat	k5eAaPmAgInS	napsat
s	s	k7c7	s
William	William	k1gInSc1	William
S.	S.	kA	S.
Burroughsem	Burroughs	k1gMnSc7	Burroughs
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Town	Town	k1gInSc1	Town
and	and	k?	and
the	the	k?	the
City	city	k1gNnSc1	city
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
690790	[number]	k4	690790
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
On	on	k3xPp3gInSc1	on
the	the	k?	the
Road	Road	k1gInSc1	Road
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
4259	[number]	k4	4259
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Visions	Visions	k1gInSc1	Visions
of	of	k?	of
Cody	coda	k1gFnSc2	coda
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
17907	[number]	k4	17907
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pic	pic	k0	pic
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
&	&	k?	&
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7043	[number]	k4	7043
<g/>
-	-	kIx~	-
<g/>
1122	[number]	k4	1122
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
výtisk	výtisk	k1gInSc1	výtisk
úplně	úplně	k6eAd1	úplně
rozebrán	rozebrán	k2eAgInSc1d1	rozebrán
<g/>
,	,	kIx,	,
dostupný	dostupný	k2eAgInSc1d1	dostupný
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8021	[number]	k4	8021
<g/>
-	-	kIx~	-
<g/>
3061	[number]	k4	3061
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Book	Book	k1gMnSc1	Book
of	of	k?	of
Sketches	Sketches	k1gMnSc1	Sketches
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
200215	[number]	k4	200215
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Book	Book	k1gInSc1	Book
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
87286	[number]	k4	87286
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Doctor	Doctor	k1gInSc1	Doctor
Sax	sax	k1gInSc1	sax
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8021	[number]	k4	8021
<g/>
-	-	kIx~	-
<g/>
3049	[number]	k4	3049
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maggie	Maggie	k1gFnSc1	Maggie
Cassidy	Cassida	k1gFnSc2	Cassida
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
17906	[number]	k4	17906
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Subterraneans	Subterraneans	k1gInSc1	Subterraneans
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8021	[number]	k4	8021
<g/>
-	-	kIx~	-
<g/>
3186	[number]	k4	3186
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Good	Good	k6eAd1	Good
Blonde	blond	k1gInSc5	blond
&	&	k?	&
Others	Others	k1gInSc1	Others
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
912516	[number]	k4	912516
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tristessa	Tristessa	k1gFnSc1	Tristessa
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
16811	[number]	k4	16811
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Visions	Visions	k1gInSc1	Visions
of	of	k?	of
Gerard	Gerard	k1gInSc1	Gerard
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
14452	[number]	k4	14452
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Old	Olda	k1gFnPc2	Olda
Angel	angel	k1gMnSc1	angel
Midnight	Midnight	k1gMnSc1	Midnight
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
912516	[number]	k4	912516
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Desolation	Desolation	k1gInSc1	Desolation
Angels	Angels	k1gInSc1	Angels
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
&	&	k?	&
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
57322	[number]	k4	57322
<g/>
-	-	kIx~	-
<g/>
505	[number]	k4	505
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Dharma	dharma	k1gFnSc1	dharma
Bums	bums	k0	bums
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
4252	[number]	k4	4252
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lonesome	Lonesom	k1gInSc5	Lonesom
Traveler	Traveler	k1gMnSc1	Traveler
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8021	[number]	k4	8021
<g/>
-	-	kIx~	-
<g/>
3074	[number]	k4	3074
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Big	Big	k?	Big
Sur	Sur	k1gFnSc1	Sur
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
16812	[number]	k4	16812
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Satori	Satori	k6eAd1	Satori
in	in	k?	in
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
394	[number]	k4	394
<g/>
-	-	kIx~	-
<g/>
17437	[number]	k4	17437
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
výtisk	výtisk	k1gInSc1	výtisk
úplně	úplně	k6eAd1	úplně
rozebrán	rozebrán	k2eAgInSc1d1	rozebrán
<g/>
,	,	kIx,	,
dostupný	dostupný	k2eAgInSc1d1	dostupný
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8021	[number]	k4	8021
<g/>
-	-	kIx~	-
<g/>
3061	[number]	k4	3061
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vanity	Vanita	k1gFnPc1	Vanita
of	of	k?	of
Duluoz	Duluoz	k1gMnSc1	Duluoz
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
23639	[number]	k4	23639
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
Říjen	říjen	k1gInSc1	říjen
v	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
zemi	zem	k1gFnSc6	zem
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Mag	Maga	k1gFnPc2	Maga
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Podzemníci	Podzemník	k1gMnPc1	Podzemník
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Dharmoví	Dharmový	k2eAgMnPc1d1	Dharmový
tuláci	tulák	k1gMnPc1	tulák
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
Osamělý	osamělý	k2eAgInSc1d1	osamělý
poutník	poutník	k1gMnSc1	poutník
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Andělé	anděl	k1gMnPc1	anděl
pustiny	pustina	k1gFnSc2	pustina
(	(	kIx(	(
<g/>
r.	r.	kA	r.
<g/>
2005	[number]	k4	2005
román	román	k1gInSc1	román
vydán	vydán	k2eAgInSc1d1	vydán
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Andělé	anděl	k1gMnPc1	anděl
zoufalství	zoufalství	k1gNnSc1	zoufalství
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Big	Big	k1gMnSc1	Big
Sur	Sur	k1gMnSc1	Sur
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Maggie	Maggie	k1gFnSc1	Maggie
Cassidyová	Cassidyová	k1gFnSc1	Cassidyová
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Vize	vize	k1gFnSc1	vize
Gerarda	Gerarda	k1gFnSc1	Gerarda
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
Tristessa	Tristessa	k1gFnSc1	Tristessa
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
Maloměsto	maloměsto	k1gNnSc1	maloměsto
<g/>
,	,	kIx,	,
velkoměsto	velkoměsto	k1gNnSc1	velkoměsto
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Satori	Satori	k1gNnSc7	Satori
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Zjevení	zjevení	k1gNnSc1	zjevení
Orfea	Orfeus	k1gMnSc4	Orfeus
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Doktor	doktor	k1gMnSc1	doktor
Sax	sax	k1gInSc4	sax
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Kniha	kniha	k1gFnSc1	kniha
snů	sen	k1gInPc2	sen
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Vize	vize	k1gFnSc1	vize
Codyho	Codyha	k1gFnSc5	Codyha
</s>
</p>
<p>
<s>
===	===	k?	===
Poezie	poezie	k1gFnPc1	poezie
<g/>
,	,	kIx,	,
korespondence	korespondence	k1gFnPc1	korespondence
<g/>
,	,	kIx,	,
audionahrávky	audionahrávka	k1gFnPc1	audionahrávka
apod.	apod.	kA	apod.
===	===	k?	===
</s>
</p>
<p>
<s>
Mexico	Mexico	k6eAd1	Mexico
City	city	k1gNnSc1	city
Blues	blues	k1gNnSc1	blues
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Scattered	Scattered	k1gInSc1	Scattered
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Heaven	Heaven	k2eAgInSc1d1	Heaven
and	and	k?	and
Other	Other	k1gInSc1	Other
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trip	Trip	k1gInSc1	Trip
Trap	trap	k1gInSc1	trap
<g/>
:	:	kIx,	:
Haiku	Haik	k1gInSc2	Haik
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Road	Road	k1gMnSc1	Road
from	from	k1gMnSc1	from
SF	SF	kA	SF
to	ten	k3xDgNnSc4	ten
NY	NY	kA	NY
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
Saijoem	Saijo	k1gInSc7	Saijo
and	and	k?	and
Lewemem	Lewem	k1gInSc7	Lewem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pomes	Pomes	k1gMnSc1	Pomes
All	All	k1gMnSc1	All
Sizes	Sizes	k1gMnSc1	Sizes
(	(	kIx(	(
<g/>
sestaveny	sestaven	k2eAgFnPc4d1	sestavena
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
San	San	k?	San
Francisco	Francisco	k6eAd1	Francisco
Blues	blues	k1gNnSc1	blues
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Book	Book	k1gInSc1	Book
of	of	k?	of
Blues	blues	k1gNnSc1	blues
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Book	Book	k1gMnSc1	Book
of	of	k?	of
Haikus	Haikus	k1gMnSc1	Haikus
</s>
</p>
<p>
<s>
Dear	Dear	k1gMnSc1	Dear
Carolyn	Carolyn	k1gMnSc1	Carolyn
<g/>
:	:	kIx,	:
Letters	Letters	k1gInSc1	Letters
to	ten	k3xDgNnSc4	ten
Carolyn	Carolyn	k1gNnSc4	Carolyn
Cassady	Cassada	k1gFnSc2	Cassada
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-934660-06-9	[number]	k4	0-934660-06-9
</s>
</p>
<p>
<s>
The	The	k?	The
Scripture	Scriptur	k1gMnSc5	Scriptur
of	of	k?	of
the	the	k?	the
Golden	Goldna	k1gFnPc2	Goldna
Eternity	eternit	k1gInPc1	eternit
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
meditace	meditace	k1gFnPc1	meditace
<g/>
,	,	kIx,	,
kóany	kóan	k1gInPc1	kóan
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-87286-291-7	[number]	k4	0-87286-291-7
</s>
</p>
<p>
<s>
Wake	Wake	k1gFnSc1	Wake
Up	Up	k1gFnSc1	Up
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Some	Some	k1gFnSc1	Some
of	of	k?	of
the	the	k?	the
Dharma	dharma	k1gFnSc1	dharma
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beat	beat	k1gInSc1	beat
Generation	Generation	k1gInSc1	Generation
(	(	kIx(	(
<g/>
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Kerouac	Kerouac	k1gFnSc4	Kerouac
napsal	napsat	k5eAaPmAgMnS	napsat
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
až	až	k9	až
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jack	Jack	k1gInSc1	Jack
Kerouac	Kerouac	k1gInSc1	Kerouac
<g/>
:	:	kIx,	:
Selected	Selected	k1gInSc1	Selected
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
,	,	kIx,	,
1940-1956	[number]	k4	1940-1956
</s>
</p>
<p>
<s>
Jack	Jack	k1gInSc1	Jack
Kerouac	Kerouac	k1gInSc1	Kerouac
<g/>
:	:	kIx,	:
Selected	Selected	k1gInSc1	Selected
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
,	,	kIx,	,
1957-1969	[number]	k4	1957-1969
</s>
</p>
<p>
<s>
Windblown	Windblown	k1gMnSc1	Windblown
World	World	k1gMnSc1	World
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Journals	Journals	k1gInSc1	Journals
of	of	k?	of
Jack	Jack	k1gInSc1	Jack
Kerouac	Kerouac	k1gFnSc1	Kerouac
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Safe	safe	k1gInSc1	safe
In	In	k1gFnSc2	In
Heaven	Heaven	k2eAgInSc1d1	Heaven
Dead	Dead	k1gInSc1	Dead
(	(	kIx(	(
<g/>
Rozhovory	rozhovor	k1gInPc1	rozhovor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Conversations	Conversations	k6eAd1	Conversations
with	with	k1gMnSc1	with
Jack	Jack	k1gMnSc1	Jack
Kerouac	Kerouac	k1gFnSc1	Kerouac
(	(	kIx(	(
<g/>
Rozhovory	rozhovor	k1gInPc1	rozhovor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Empty	Empt	k1gInPc1	Empt
Phantoms	Phantomsa	k1gFnPc2	Phantomsa
(	(	kIx(	(
<g/>
Rozhovory	rozhovor	k1gInPc1	rozhovor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Departed	Departed	k1gInSc1	Departed
Angels	Angels	k1gInSc1	Angels
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Lost	Lost	k1gMnSc1	Lost
Paintings	Paintings	k1gInSc1	Paintings
</s>
</p>
<p>
<s>
Readings	Readings	k6eAd1	Readings
by	by	kYmCp3nS	by
Jack	Jack	k1gInSc1	Jack
Kerouac	Kerouac	k1gInSc4	Kerouac
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Beat	beat	k1gInSc1	beat
Generation	Generation	k1gInSc1	Generation
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
LP	LP	kA	LP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poetry	Poetr	k1gMnPc4	Poetr
For	forum	k1gNnPc2	forum
The	The	k1gFnSc1	The
Beat	beat	k1gInSc4	beat
Generation	Generation	k1gInSc1	Generation
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
LP	LP	kA	LP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Blues	blues	k1gFnSc1	blues
And	Anda	k1gFnPc2	Anda
Haikus	Haikus	k1gMnSc1	Haikus
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
LP	LP	kA	LP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Jack	Jack	k1gInSc1	Jack
Kerouac	Kerouac	k1gInSc1	Kerouac
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Audio	audio	k2eAgFnSc1d1	audio
CD	CD	kA	CD
kompilace	kompilace	k1gFnSc1	kompilace
tří	tři	k4xCgInPc2	tři
LP	LP	kA	LP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Jack	Jack	k1gInSc1	Jack
Kerouac	Kerouac	k1gFnSc1	Kerouac
Romnibus	Romnibus	k1gMnSc1	Romnibus
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
multimediální	multimediální	k2eAgFnPc4d1	multimediální
CD-ROM	CD-ROM	k1gFnPc4	CD-ROM
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
edit	edit	k1gInSc1	edit
<g/>
.	.	kIx.	.
</s>
<s>
Ralph	Ralph	k1gInSc1	Ralph
Lombreglia	Lombreglium	k1gNnSc2	Lombreglium
a	a	k8xC	a
Kate	kat	k1gInSc5	kat
Bernhardt	Bernhardt	k2eAgMnSc1d1	Bernhardt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Reads	Reads	k6eAd1	Reads
on	on	k3xPp3gInSc1	on
the	the	k?	the
Road	Road	k1gInSc1	Road
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Audio	audio	k2eAgMnSc1d1	audio
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Doctor	Doctor	k1gInSc1	Doctor
Sax	sax	k1gInSc1	sax
&	&	k?	&
Great	Great	k2eAgInSc1d1	Great
World	World	k1gInSc1	World
Snake	Snak	k1gFnSc2	Snak
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Audio	audio	k2eAgMnSc1d1	audio
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Door	Door	k1gMnSc1	Door
Wide	Wid	k1gFnSc2	Wid
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
edit	edit	k1gInSc1	edit
<g/>
.	.	kIx.	.
</s>
<s>
Joyce	Joyce	k1gMnSc1	Joyce
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
)	)	kIx)	)
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rozprášené	rozprášený	k2eAgFnPc4d1	rozprášená
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Interview	interview	k1gNnSc1	interview
s	s	k7c7	s
Jackem	Jacek	k1gMnSc7	Jacek
Kerouakem	Kerouak	k1gMnSc7	Kerouak
pro	pro	k7c4	pro
Paris	Paris	k1gMnSc1	Paris
Review	Review	k1gFnPc4	Review
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
Ted	Ted	k1gMnSc1	Ted
Berrigan	Berrigan	k1gMnSc1	Berrigan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Písmo	písmo	k1gNnSc1	písmo
zlaté	zlatý	k2eAgFnSc2d1	zlatá
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
NICOSIA	NICOSIA	kA	NICOSIA
<g/>
,	,	kIx,	,
Gerald	Gerald	k1gMnSc1	Gerald
<g/>
.	.	kIx.	.
</s>
<s>
Memory	Memora	k1gFnPc1	Memora
Babe	babit	k5eAaImSgInS	babit
<g/>
:	:	kIx,	:
kritická	kritický	k2eAgFnSc1d1	kritická
biografie	biografie	k1gFnSc1	biografie
Jacka	Jacek	k1gMnSc2	Jacek
Kerouaka	Kerouak	k1gMnSc2	Kerouak
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85885	[number]	k4	85885
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TURNER	turner	k1gMnSc1	turner
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
<g/>
.	.	kIx.	.
</s>
<s>
Hipster	Hipster	k1gInSc1	Hipster
s	s	k7c7	s
andělskou	andělský	k2eAgFnSc7d1	Andělská
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Jota	jota	k1gFnSc1	jota
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARTERS	CHARTERS	kA	CHARTERS
<g/>
,	,	kIx,	,
Ann	Ann	k1gFnSc1	Ann
<g/>
.	.	kIx.	.
</s>
<s>
Kerouac	Kerouac	k1gFnSc1	Kerouac
<g/>
:	:	kIx,	:
A	a	k9	a
biography	biographa	k1gMnSc2	biographa
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
André	André	k1gMnSc1	André
Deutsch	Deutsch	k1gMnSc1	Deutsch
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3396	[number]	k4	3396
<g/>
-	-	kIx~	-
<g/>
516	[number]	k4	516
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TYTELL	TYTELL	kA	TYTELL
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Nazí	nahý	k2eAgMnPc1d1	nahý
andělé	anděl	k1gMnPc1	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7198	[number]	k4	7198
<g/>
-	-	kIx~	-
<g/>
104	[number]	k4	104
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
amerických	americký	k2eAgMnPc2d1	americký
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jack	Jacka	k1gFnPc2	Jacka
Kerouac	Kerouac	k1gFnSc1	Kerouac
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jack	Jack	k1gMnSc1	Jack
Kerouac	Kerouac	k1gFnSc4	Kerouac
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jack	Jack	k1gMnSc1	Jack
Kerouac	Kerouac	k1gInSc1	Kerouac
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
Jacka	Jacek	k1gMnSc2	Jacek
Kerouaca	Kerouacus	k1gMnSc2	Kerouacus
</s>
</p>
