<s>
Černobylská	černobylský	k2eAgFnSc1d1	Černobylská
havárie	havárie	k1gFnSc1	havárie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
v	v	k7c6	v
Černobylské	černobylský	k2eAgFnSc6d1	Černobylská
jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
část	část	k1gFnSc1	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
