<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Potštát	Potštát	k1gInSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Potštát	Potštát	k1gInSc4
farní	farní	k2eAgInSc1d1
kostelZákladní	kostelZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Děkanát	děkanát	k1gInSc1
</s>
<s>
Hranice	hranice	k1gFnSc1
Diecéze	diecéze	k1gFnSc2
</s>
<s>
arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
moravská	moravský	k2eAgFnSc1d1
Administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Ing.	ing.	kA
Radomír	Radomír	k1gMnSc1
Šidleja	Šidlej	k1gInSc2
Území	území	k1gNnSc2
farnosti	farnost	k1gFnSc2
</s>
<s>
Potštát	Potštát	k1gInSc4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
753	#num#	k4
62	#num#	k4
Potštát	Potštát	k1gInSc1
37	#num#	k4
Webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
farka	farka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
antiochia	antiochia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
E-mail	e-mail	k1gInSc1
</s>
<s>
fapotstat@ado.cz	fapotstat@ado.cz	k1gInSc1
Rejstřík	rejstřík	k1gInSc1
evidovaných	evidovaný	k2eAgFnPc2d1
právnických	právnický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
</s>
<s>
Databáze	databáze	k1gFnSc1
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Údaje	údaj	k1gInPc1
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
0	#num#	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Potštát	Potštát	k1gInSc4
je	být	k5eAaImIp3nS
územní	územní	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
římských	římský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
s	s	k7c7
farním	farní	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
svatého	svatý	k2eAgMnSc2d1
Bartoloměje	Bartoloměj	k1gMnSc2
v	v	k7c6
děkanátu	děkanát	k1gInSc6
Hranice	hranice	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
farnosti	farnost	k1gFnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Bartoloměje	Bartoloměj	k1gMnSc2
za	za	k7c7
jihozápadním	jihozápadní	k2eAgInSc7d1
koutem	kout	k1gInSc7
náměstí	náměstí	k1gNnSc2
se	se	k3xPyFc4
připomíná	připomínat	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
1408	#num#	k4
jako	jako	k8xS,k8xC
farní	farní	k2eAgInSc1d1
<g/>
,	,	kIx,
původně	původně	k6eAd1
gotický	gotický	k2eAgMnSc1d1
<g/>
,	,	kIx,
upravován	upravován	k2eAgMnSc1d1
postupně	postupně	k6eAd1
renesančně	renesančně	k6eAd1
<g/>
,	,	kIx,
barokně	barokně	k6eAd1
a	a	k8xC
pseudoslohově	pseudoslohově	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Duchovní	duchovní	k2eAgMnPc1d1
správci	správce	k1gMnPc1
</s>
<s>
Současným	současný	k2eAgMnSc7d1
duchovním	duchovní	k2eAgMnSc7d1
správcem	správce	k1gMnSc7
(	(	kIx(
<g/>
červen	červen	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jako	jako	k8xC,k8xS
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k1gNnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Ing.	ing.	kA
Radomír	Radomír	k1gMnSc1
Šidleja	Šidleja	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Bohoslužby	bohoslužba	k1gFnPc1
</s>
<s>
Seznam	seznam	k1gInSc1
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
ve	v	k7c6
farnosti	farnost	k1gFnSc6
<g/>
,	,	kIx,
pořad	pořad	k1gInSc1
bohoslužeb	bohoslužba	k1gFnPc2
</s>
<s>
KostelMístoBohoslužba	KostelMístoBohoslužba	k1gFnSc1
(	(	kIx(
<g/>
den	den	k1gInSc1
<g/>
)	)	kIx)
<g/>
HodinaPoznámka	HodinaPoznámka	k1gFnSc1
</s>
<s>
svatého	svatý	k2eAgMnSc4d1
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
Potštát	Potštát	k1gInSc1
</s>
<s>
nedělestředa	nedělestředa	k1gMnSc1
</s>
<s>
11.001	11.001	k4
<g/>
7.15	7.15	k4
</s>
<s>
farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
</s>
<s>
Aktivity	aktivita	k1gFnPc1
ve	v	k7c6
farnosti	farnost	k1gFnSc6
</s>
<s>
Ve	v	k7c6
farnosti	farnost	k1gFnSc6
se	se	k3xPyFc4
pravidelně	pravidelně	k6eAd1
<g/>
̟	̟	k?
koná	konat	k5eAaImIp3nS
tříkrálová	tříkrálový	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
vybralo	vybrat	k5eAaPmAgNnS
40	#num#	k4
000	#num#	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Farnost	farnost	k1gFnSc1
Potštát	Potštát	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ado	ado	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Výsledky	výsledek	k1gInPc1
Tříkrálové	tříkrálový	k2eAgFnSc2d1
sbírky	sbírka	k1gFnSc2
2017	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charita	charita	k1gFnSc1
Hranice	hranice	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Farnost	farnost	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
olomoucké	olomoucký	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
</s>
<s>
Webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
farnosti	farnost	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Farnosti	farnost	k1gFnSc2
Děkanátu	děkanát	k1gInSc2
Hranice	hranice	k1gFnSc2
</s>
<s>
Bělotín	Bělotín	k1gInSc1
•	•	k?
Černotín	Černotín	k1gInSc1
•	•	k?
Dolní	dolní	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
u	u	k7c2
Lipníka	Lipník	k1gInSc2
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
•	•	k?
Drahotuše	Drahotuše	k1gFnSc1
•	•	k?
Hlinsko	Hlinsko	k1gNnSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
u	u	k7c2
Všechovic	Všechovice	k1gFnPc2
•	•	k?
Hranice	hranice	k1gFnSc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
Hustopeče	Hustopeč	k1gFnSc2
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
•	•	k?
Jezernice	Jezernice	k1gFnSc2
•	•	k?
Jindřichov	Jindřichov	k1gInSc1
u	u	k7c2
Hranic	Hranice	k1gFnPc2
•	•	k?
Lipník	Lipník	k1gInSc1
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
•	•	k?
Loučka	loučka	k1gFnSc1
u	u	k7c2
Lipníka	Lipník	k1gInSc2
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
•	•	k?
Osek	Osek	k1gInSc4
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
•	•	k?
Paršovice	Paršovice	k1gFnSc2
•	•	k?
Partutovice	Partutovice	k1gFnSc1
•	•	k?
Podhoří	podhoří	k1gNnSc2
•	•	k?
Potštát	Potštát	k1gInSc4
•	•	k?
Soběchleby	Soběchleb	k1gInPc4
u	u	k7c2
Hranic	Hranice	k1gFnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
Střítež	Střítež	k1gFnSc1
nad	nad	k7c7
Ludinou	Ludina	k1gFnSc7
•	•	k?
Špičky	špička	k1gFnSc2
•	•	k?
Týn	Týn	k1gInSc1
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
•	•	k?
Všechovice	Všechovice	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
