<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Potštát	Potštát	k1gInSc4
je	být	k5eAaImIp3nS
územní	územní	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
římských	římský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
s	s	k7c7
farním	farní	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
svatého	svatý	k2eAgMnSc2d1
Bartoloměje	Bartoloměj	k1gMnSc2
v	v	k7c6
děkanátu	děkanát	k1gInSc6
Hranice	hranice	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>