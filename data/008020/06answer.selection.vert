<s>
Koalové	Koal	k1gMnPc1	Koal
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
původních	původní	k2eAgNnPc2d1	původní
australských	australský	k2eAgNnPc2d1	Australské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
chováni	chovat	k5eAaImNgMnP	chovat
jako	jako	k8xC	jako
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
ani	ani	k8xC	ani
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
kdekoli	kdekoli	k6eAd1	kdekoli
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
