<s>
Aktinium	aktinium	k1gNnSc1
</s>
<s>
Aktinium	aktinium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
6	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
227	#num#	k4
</s>
<s>
Ac	Ac	k?
</s>
<s>
89	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
atom	atom	k1gInSc1
aktinia	aktinium	k1gNnSc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Aktinium	aktinium	k1gNnSc1
<g/>
,	,	kIx,
Ac	Ac	k1gFnSc1
<g/>
,	,	kIx,
89	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Actinium	Actinium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
10	#num#	k4
ppm	ppm	k?
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbrný	stříbrný	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-34-8	7440-34-8	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
227,027	227,027	k4
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
187,8	187,8	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
203	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
Ac	Ac	k1gFnPc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
118	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
6	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
III	III	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,1	1,1	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
5,70	5,70	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
11,48	11,48	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
18,90	18,90	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
krychlová	krychlový	k2eAgFnSc1d1
a	a	k8xC
<g/>
=	=	kIx~
531,1	531,1	k4
pm	pm	k?
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
22,55	22,55	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
10,062	10,062	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
12	#num#	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Molární	molární	k2eAgFnSc1d1
atomizační	atomizační	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
</s>
<s>
385,2	385,2	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
56,5	56,5	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
187,94	187,94	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
plyn	plyn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1	#num#	k4
050	#num#	k4
±	±	k?
50	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
323,15	323,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
2	#num#	k4
750	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
0	#num#	k4
<g/>
23,15	23,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
tání	tání	k1gNnSc2
</s>
<s>
46,1	46,1	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
406	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
varu	var	k1gInSc2
</s>
<s>
1	#num#	k4
290	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
0,120	0,120	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
0,091	0,091	k4
8	#num#	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
plyn	plyn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
Ac	Ac	k1gFnPc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
→	→	k?
Ac	Ac	k1gFnSc7
<g/>
0	#num#	k4
<g/>
)	)	kIx)
-2,6	-2,6	k4
V	v	k7c6
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
La	la	k1gNnSc1
<g/>
⋏	⋏	k?
</s>
<s>
Radium	radium	k1gNnSc1
≺	≺	k?
<g/>
Ac	Ac	k1gMnSc2
<g/>
≻	≻	k?
Thorium	thorium	k1gNnSc1
</s>
<s>
Aktinium	aktinium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Ac	Ac	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Actinium	Actinium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prvním	první	k4xOgMnSc7
členem	člen	k1gMnSc7
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Aktinium	aktinium	k1gNnSc1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
stabilní	stabilní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Ac	Ac	k1gFnSc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
a	a	k8xC
svým	svůj	k3xOyFgNnSc7
chemickým	chemický	k2eAgNnSc7d1
chováním	chování	k1gNnSc7
se	se	k3xPyFc4
podobá	podobat	k5eAaImIp3nS
prvkům	prvek	k1gInPc3
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čistý	čistý	k2eAgInSc1d1
kov	kov	k1gInSc1
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
redukcí	redukce	k1gFnSc7
fluoridu	fluorid	k1gInSc2
aktinitého	aktinitý	k2eAgInSc2d1
parami	para	k1gFnPc7
lithia	lithium	k1gNnSc2
při	při	k7c6
teplotě	teplota	k1gFnSc6
1	#num#	k4
100	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
300	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Aktinium	aktinium	k1gNnSc1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
<g/>
,	,	kIx,
září	zářit	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
150	#num#	k4
<g/>
krát	krát	k6eAd1
intenzivněji	intenzivně	k6eAd2
než	než	k8xS
radium	radium	k1gNnSc1
a	a	k8xC
ve	v	k7c6
tmě	tma	k1gFnSc6
proto	proto	k8xC
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
namodralé	namodralý	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS
jej	on	k3xPp3gInSc2
roku	rok	k1gInSc2
1899	#num#	k4
francouzský	francouzský	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
André-Louis	André-Louis	k1gFnSc2
Debierne	Debiern	k1gInSc5
v	v	k7c6
uranové	uranový	k2eAgFnSc6d1
rudě	ruda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
<g/>
,	,	kIx,
izotopy	izotop	k1gInPc1
a	a	k8xC
využití	využití	k1gNnSc1
</s>
<s>
Přestože	přestože	k8xS
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
řada	řada	k1gFnSc1
izotopů	izotop	k1gInPc2
aktinia	aktinium	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
se	se	k3xPyFc4
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
pouze	pouze	k6eAd1
s	s	k7c7
izotopem	izotop	k1gInSc7
227	#num#	k4
<g/>
Ac	Ac	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vzniká	vznikat	k5eAaImIp3nS
radioaktivním	radioaktivní	k2eAgInSc7d1
rozpadem	rozpad	k1gInSc7
uranu	uran	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poločas	poločas	k1gInSc1
přeměny	přeměna	k1gFnSc2
tohoto	tento	k3xDgInSc2
izotopu	izotop	k1gInSc2
je	být	k5eAaImIp3nS
21,772	21,772	k4
roku	rok	k1gInSc2
a	a	k8xC
uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jedna	jeden	k4xCgFnSc1
tuna	tuna	k1gFnSc1
uranové	uranový	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
desetinu	desetina	k1gFnSc4
gramu	gram	k1gInSc2
aktinia	aktinium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
dalších	další	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
stojí	stát	k5eAaImIp3nS
za	za	k7c4
zmínku	zmínka	k1gFnSc4
např.	např.	kA
226	#num#	k4
<g/>
Ac	Ac	k1gFnPc1
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
29,37	29,37	k4
hodiny	hodina	k1gFnSc2
nebo	nebo	k8xC
225	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
přibližně	přibližně	k6eAd1
10	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
<g/>
,	,	kIx,
s	s	k7c7
nukleonovými	nukleonův	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
od	od	k7c2
205	#num#	k4
po	po	k7c4
237	#num#	k4
se	se	k3xPyFc4
rozpadají	rozpadat	k5eAaImIp3nP,k5eAaPmIp3nP
mnohem	mnohem	k6eAd1
rychleji	rychle	k6eAd2
<g/>
:	:	kIx,
</s>
<s>
IzotopPoločas	IzotopPoločas	k1gMnSc1
přeměnyDruh	přeměnyDruh	k1gMnSc1
přeměnyProdukt	přeměnyProdukt	k1gInSc4
rozpadu	rozpad	k1gInSc2
</s>
<s>
205	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
20	#num#	k4
msα	msα	k?
<g/>
201	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
206	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
22	#num#	k4
msα	msα	k?
<g/>
202	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
207	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
27	#num#	k4
msα	msα	k?
<g/>
203	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
208	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
95	#num#	k4
msα	msα	k?
(	(	kIx(
<g/>
99	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
1	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
204	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
208	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
209	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
87	#num#	k4
msα	msα	k?
(	(	kIx(
<g/>
99	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
1	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
205	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
209	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
210	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
350	#num#	k4
msα	msα	k?
/	/	kIx~
ε	ε	k?
<g/>
206	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
210	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
211	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
210	#num#	k4
msα	msα	k?
<g/>
207	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
212	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
930	#num#	k4
msα	msα	k?
(	(	kIx(
<g/>
57	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
43	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
208	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
212	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
213	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
738	#num#	k4
msα	msα	k?
<g/>
209	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
214	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
8,2	8,2	k4
sα	sα	k?
(	(	kIx(
<g/>
89	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
11	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
210	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
214	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
215	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
170	#num#	k4
msα	msα	k?
(	(	kIx(
<g/>
99,91	99,91	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
0,09	0,09	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
211	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
215	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
216	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
440	#num#	k4
μ	μ	k?
<g/>
212	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
217	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
69	#num#	k4
nsα	nsα	k?
(	(	kIx(
<g/>
≥	≥	k?
<g/>
98	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
≤	≤	k?
<g/>
2	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
213	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
217	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
218	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
1,08	1,08	k4
μ	μ	k?
<g/>
214	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
219	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
11,8	11,8	k4
μ	μ	k?
<g/>
215	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
220	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
26,4	26,4	k4
msα	msα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
4	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
216	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
220	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
221	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
52	#num#	k4
msα	msα	k?
<g/>
217	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
222	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
5,0	5,0	k4
sα	sα	k?
(	(	kIx(
<g/>
99	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
1	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
218	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
222	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
223	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
2,10	2,10	k4
minα	minα	k?
(	(	kIx(
<g/>
99	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
1	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
219	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
223	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
224	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
2,78	2,78	k4
hε	hε	k?
(	(	kIx(
<g/>
90,9	90,9	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
9,1	9,1	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
224	#num#	k4
<g/>
Ra	ra	k0
<g/>
/	/	kIx~
<g/>
220	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
225	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
10,0	10,0	k4
dα	dα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
C	C	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
12	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
221	#num#	k4
<g/>
Fr	fr	k0
<g/>
/	/	kIx~
<g/>
211	#num#	k4
<g/>
Bi	Bi	k1gFnPc2
</s>
<s>
226	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
29,37	29,37	k4
hβ	hβ	k?
<g/>
−	−	k?
(	(	kIx(
<g/>
83	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
17	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
0,006	0,006	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
226	#num#	k4
<g/>
Th	Th	k1gFnPc2
<g/>
/	/	kIx~
226	#num#	k4
<g/>
Ra	ra	k0
<g/>
/	/	kIx~
222	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
227	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
21,772	21,772	k4
rβ	rβ	k?
<g/>
−	−	k?
(	(	kIx(
<g/>
98,62	98,62	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
1,38	1,38	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
227	#num#	k4
<g/>
Th	Th	k1gFnPc2
<g/>
/	/	kIx~
223	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
228	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
6,15	6,15	k4
hβ	hβ	k?
<g/>
−	−	k?
<g/>
228	#num#	k4
<g/>
Th	Th	k1gFnPc2
</s>
<s>
229	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
62,7	62,7	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
229	#num#	k4
<g/>
Th	Th	k1gFnSc1
</s>
<s>
230	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
122	#num#	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
230	#num#	k4
<g/>
Th	Th	k1gFnSc1
</s>
<s>
231	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
7,5	7,5	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
231	#num#	k4
<g/>
Th	Th	k1gFnPc2
</s>
<s>
232	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
119	#num#	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
232	#num#	k4
<g/>
Th	Th	k1gFnPc2
</s>
<s>
233	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
145	#num#	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
233	#num#	k4
<g/>
Th	Th	k1gFnSc1
</s>
<s>
234	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
44	#num#	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
234	#num#	k4
<g/>
Th	Th	k1gFnSc1
</s>
<s>
235	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
62	#num#	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
235	#num#	k4
<g/>
Th	Th	k1gFnSc1
</s>
<s>
236	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
<g/>
1,2	1,2	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
236	#num#	k4
<g/>
Th	Th	k1gFnSc1
</s>
<s>
237	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
?	?	kIx.
</s>
<s desamb="1">
<g/>
β	β	k?
<g/>
−	−	k?
<g/>
237	#num#	k4
<g/>
Th	Th	k1gFnPc2
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Všechny	všechen	k3xTgInPc1
byly	být	k5eAaImAgInP
připraveny	připravit	k5eAaPmNgInP
uměle	uměle	k6eAd1
bombardováním	bombardování	k1gNnSc7
jader	jádro	k1gNnPc2
těžkých	těžký	k2eAgInPc2d1
prvků	prvek	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
radia	radio	k1gNnPc4
<g/>
)	)	kIx)
neutrony	neutron	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Praktický	praktický	k2eAgInSc1d1
význam	význam	k1gInSc1
aktinia	aktinium	k1gNnSc2
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
minimální	minimální	k2eAgMnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
jej	on	k3xPp3gMnSc4
použít	použít	k5eAaPmF
například	například	k6eAd1
jako	jako	k8xS,k8xC
silný	silný	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
neutronů	neutron	k1gInPc2
při	při	k7c6
experimentech	experiment	k1gInPc6
s	s	k7c7
jadernými	jaderný	k2eAgFnPc7d1
přeměnami	přeměna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.nndc.bnl.gov/chart/	http://www.nndc.bnl.gov/chart/	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aktiniová	aktiniový	k2eAgFnSc1d1
rozpadová	rozpadový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnPc1
aktinium	aktinium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
aktinium	aktinium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
aktinium	aktinium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4523163-1	4523163-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5759	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85000706	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85000706	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
