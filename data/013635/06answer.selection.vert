<s>
Aktinium	aktinium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Ac	Ac	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Actinium	Actinium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prvním	první	k4xOgMnSc7
členem	člen	k1gMnSc7
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
.	.	kIx.
</s>