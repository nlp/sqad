<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1412	[number]	k4	1412
<g/>
,	,	kIx,	,
na	na	k7c4	na
město	město	k1gNnSc4	město
byly	být	k5eAaImAgFnP	být
povýšeny	povýšit	k5eAaPmNgFnP	povýšit
teprve	teprve	k6eAd1	teprve
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
