<s>
Zeměpisné	zeměpisný	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
světa	svět	k1gInSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
seznamem	seznam	k1gInSc7
zeměpisných	zeměpisný	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
planety	planeta	k1gFnSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rekordy	rekord	k1gInPc1
podle	podle	k7c2
kontinentů	kontinent	k1gInPc2
</s>
<s>
Kontinent	kontinent	k1gInSc1
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
Rekordní	rekordní	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1
roční	roční	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
</s>
<s>
Nejnižší	nízký	k2eAgFnPc1d3
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
</s>
<s>
Nejnižší	nízký	k2eAgFnPc1d3
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
</s>
<s>
Nejnižší	nízký	k2eAgFnPc1d3
</s>
<s>
Asie	Asie	k1gFnSc1
</s>
<s>
8850	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
<g/>
,	,	kIx,
Nepál	Nepál	k1gInSc1
–	–	k?
Tibet	Tibet	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
−	−	k?
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
hladina	hladina	k1gFnSc1
Mrtvého	mrtvý	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
–	–	k?
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
53,9	53,9	k4
°	°	k?
<g/>
CTirat	CTirat	k1gMnSc1
Cvi	Cvi	k1gMnSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
<g/>
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
</s>
<s>
−	−	k?
°	°	k?
<g/>
C	C	kA
ZměřenoVerchojansk	ZměřenoVerchojansk	k1gInSc1
<g/>
,	,	kIx,
Sibiř	Sibiř	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1892	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
430	#num#	k4
mmČérápuňdží	mmČérápuňdž	k1gFnPc2
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc2
</s>
<s>
45,7	45,7	k4
mmAden	mmAdna	k1gFnPc2
<g/>
,	,	kIx,
Jemen	Jemen	k1gInSc1
</s>
<s>
−	−	k?
°	°	k?
<g/>
C	C	kA
VypočtenoOjmjakon	VypočtenoOjmjakon	k1gNnSc1
<g/>
,	,	kIx,
Sibiř	Sibiř	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1926	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
5895	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Kilimandžáro	Kilimandžáro	k1gNnSc1
<g/>
,	,	kIx,
Tanzanie	Tanzanie	k1gFnSc1
</s>
<s>
−	−	k?
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
jezero	jezero	k1gNnSc1
Asal	Asalum	k1gNnPc2
<g/>
,	,	kIx,
Džibutsko	Džibutsko	k1gNnSc1
</s>
<s>
55,5	55,5	k4
°	°	k?
<g/>
CKebili	CKebili	k1gFnSc1
<g/>
,	,	kIx,
Tunisko	Tunisko	k1gNnSc1
<g/>
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1931	#num#	k4
</s>
<s>
−	−	k?
°	°	k?
<g/>
CIfrane	CIfran	k1gInSc5
<g/>
,	,	kIx,
Maroko	Maroko	k1gNnSc1
<g/>
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1935	#num#	k4
</s>
<s>
10	#num#	k4
470	#num#	k4
mmDebunsha	mmDebunsha	k1gFnSc1
<g/>
,	,	kIx,
Kamerun	Kamerun	k1gInSc1
</s>
<s>
0,5	0,5	k4
mmDachla	mmDachnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
6168	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Denali	Denali	k1gFnSc1
(	(	kIx(
<g/>
Mount	Mount	k1gInSc1
McKinley	McKinlea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Aljaška	Aljaška	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
−	−	k?
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Údolí	údolí	k1gNnSc4
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc2
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
56,7	56,7	k4
°	°	k?
<g/>
CÚdolí	CÚdolí	k1gNnSc4
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc2
<g/>
,	,	kIx,
USA	USA	kA
<g/>
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
−	−	k?
°	°	k?
<g/>
CSnag	CSnag	k1gMnSc1
<g/>
,	,	kIx,
Yukon	Yukon	k1gMnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1947	#num#	k4
</s>
<s>
6657	#num#	k4
mmHenderson	mmHenderson	k1gNnSc1
Lake	Lake	k1gFnPc2
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
</s>
<s>
30,5	30,5	k4
mmLos	mmLos	k1gMnSc1
Bataques	Bataques	k1gMnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
−	−	k?
°	°	k?
<g/>
CNorth	CNorth	k1gMnSc1
Ice	Ice	k1gMnSc1
<g/>
,	,	kIx,
Grónsko	Grónsko	k1gNnSc1
<g/>
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1954	#num#	k4
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
6959	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Aconcagua	Aconcagua	k1gFnSc1
<g/>
,	,	kIx,
Mendoza	Mendoza	k1gFnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
−	−	k?
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Laguna	laguna	k1gFnSc1
del	del	k?
Carbón	Carbón	k1gInSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
48,9	48,9	k4
°	°	k?
<g/>
CRivadavia	CRivadavia	k1gFnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
<g/>
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1905	#num#	k4
</s>
<s>
−	−	k?
°	°	k?
<g/>
CSarmiento	CSarmiento	k1gNnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1907	#num#	k4
</s>
<s>
8892	#num#	k4
mmQuibdó	mmQuibdó	k?
<g/>
,	,	kIx,
Kolumbie	Kolumbie	k1gFnSc1
</s>
<s>
0,8	0,8	k4
mmArica	mmAric	k1gInSc2
<g/>
,	,	kIx,
Atacama	Atacamum	k1gNnSc2
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc2
</s>
<s>
Antarktida	Antarktida	k1gFnSc1
</s>
<s>
4897	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Vinson	Vinson	k1gMnSc1
Massif	Massif	k1gMnSc1
</s>
<s>
−	−	k?
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
Bentley	Bentlea	k1gMnSc2
Subglacial	Subglacial	k1gMnSc1
Trench	Trench	k1gMnSc1
</s>
<s>
18,3	18,3	k4
°	°	k?
<g/>
CEsperanza	CEsperanza	k1gFnSc1
<g/>
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
</s>
<s>
−	−	k?
°	°	k?
<g/>
CDome	CDom	k1gInSc5
A	a	k9
<g/>
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
5642	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Elbrus	Elbrus	k1gInSc1
<g/>
,	,	kIx,
Kavkaz	Kavkaz	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
−	−	k?
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
hladina	hladina	k1gFnSc1
Kaspického	kaspický	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc4
–	–	k?
Írán	Írán	k1gInSc1
–	–	k?
Turkmenistán	Turkmenistán	k1gInSc1
–	–	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
–	–	k?
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
52,0	52,0	k4
°	°	k?
<g/>
CAmareleja	CAmareleja	k1gFnSc1
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
<g/>
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
</s>
<s>
−	−	k?
°	°	k?
<g/>
CUsť-Ščugor	CUsť-Ščugor	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1978	#num#	k4
</s>
<s>
4624	#num#	k4
mmCrkvice	mmCrkvice	k1gFnSc1
<g/>
,	,	kIx,
Boka	boka	k1gFnSc1
Kotorska	Kotorska	k1gFnSc1
<g/>
,	,	kIx,
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
163	#num#	k4
mmAstrachaň	mmAstrachanit	k5eAaPmRp2nS
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
4807	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Mont	Mont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
<g/>
,	,	kIx,
Alpy	Alpy	k1gFnPc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
–	–	k?
Itálie	Itálie	k1gFnSc2
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
2228	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Mount	Mount	k1gInSc1
Kosciuszko	Kosciuszka	k1gFnSc5
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Jižní	jižní	k2eAgInSc1d1
Wales	Wales	k1gInSc1
</s>
<s>
−	−	k?
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Eyreovo	Eyreův	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
50,7	50,7	k4
°	°	k?
<g/>
CCloncurry	CCloncurra	k1gFnPc1
<g/>
,	,	kIx,
Queensland	Queensland	k1gInSc1
<g/>
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1889	#num#	k4
<g/>
*	*	kIx~
</s>
<s>
−	−	k?
°	°	k?
<g/>
CCharlotte	CCharlott	k1gInSc5
Pass	Pass	k1gInSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Jižní	jižní	k2eAgInSc1d1
Wales	Wales	k1gInSc1
<g/>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1994	#num#	k4
</s>
<s>
11	#num#	k4
680	#num#	k4
mmWaialeale	mmWaialeale	k6eAd1
<g/>
,	,	kIx,
Havajské	havajský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
103	#num#	k4
mmMulka	mmMulka	k1gFnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Oceánie	Oceánie	k1gFnSc1
</s>
<s>
4884	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Carstensz	Carstensz	k1gInSc1
Pyramid	pyramid	k1gInSc1
(	(	kIx(
<g/>
Puncak	Puncak	k1gMnSc1
Jaya	Jaya	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
</s>
<s>
hladina	hladina	k1gFnSc1
moře	moře	k1gNnSc2
</s>
<s>
42,4	42,4	k4
°	°	k?
<g/>
CAwatere	CAwater	k1gInSc5
River	River	k1gInSc1
a	a	k8xC
Rangiora	Rangiora	k1gFnSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1973	#num#	k4
</s>
<s>
−	−	k?
°	°	k?
<g/>
COphir	COphir	k1gInSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1995	#num#	k4
</s>
<s>
Tučný	tučný	k2eAgInSc1d1
text	text	k1gInSc1
označuje	označovat	k5eAaImIp3nS
největší	veliký	k2eAgInPc4d3
extrémy	extrém	k1gInPc4
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
sopka	sopka	k1gFnSc1
</s>
<s>
6893	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
Ojos	Ojos	k1gInSc1
del	del	k?
Salado	Salada	k1gFnSc5
<g/>
,	,	kIx,
Andy	Anda	k1gFnPc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
–	–	k?
Chile	Chile	k1gNnSc2
</s>
<s>
Největší	veliký	k2eAgFnSc1d3
poušť	poušť	k1gFnSc1
</s>
<s>
14	#num#	k4
200	#num#	k4
000	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
Antarktická	antarktický	k2eAgFnSc1d1
poušť	poušť	k1gFnSc1
</s>
<s>
Největší	veliký	k2eAgInSc1d3
ostrov	ostrov	k1gInSc1
</s>
<s>
2	#num#	k4
130	#num#	k4
750	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
Grónsko	Grónsko	k1gNnSc4
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc4
</s>
<s>
Největší	veliký	k2eAgInSc1d3
neobydlený	obydlený	k2eNgInSc1d1
ostrov	ostrov	k1gInSc1
</s>
<s>
55	#num#	k4
247	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
Devon	devon	k1gInSc1
<g/>
,	,	kIx,
Kanadské	kanadský	k2eAgNnSc1d1
arktické	arktický	k2eAgNnSc1d1
souostroví	souostroví	k1gNnSc1
<g/>
,	,	kIx,
Nunavut	Nunavut	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
</s>
<s>
Největší	veliký	k2eAgInSc1d3
poloostrov	poloostrov	k1gInSc1
</s>
<s>
2	#num#	k4
780	#num#	k4
000	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
Arabský	arabský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
<g/>
,	,	kIx,
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
–	–	k?
Omán	Omán	k1gInSc1
–	–	k?
Jemen	Jemen	k1gInSc4
–	–	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
jezero	jezero	k1gNnSc1
</s>
<s>
371	#num#	k4
000	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
Kaspické	kaspický	k2eAgFnSc6d1
moře	mora	k1gFnSc6
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc4
–	–	k?
Írán	Írán	k1gInSc1
–	–	k?
Turkmenistán	Turkmenistán	k1gInSc1
–	–	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
–	–	k?
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
sladkovodní	sladkovodní	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
</s>
<s>
82	#num#	k4
414	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
Hořejší	Hořejší	k1gFnSc6
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
–	–	k?
USA	USA	kA
</s>
<s>
Nejhlubší	hluboký	k2eAgNnSc1d3
jezero	jezero	k1gNnSc1
</s>
<s>
1637	#num#	k4
mBajkal	mBajkal	k1gMnSc1
<g/>
,	,	kIx,
Sibiř	Sibiř	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Řeka	řeka	k1gFnSc1
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
průtokem	průtok	k1gInSc7
vody	voda	k1gFnSc2
</s>
<s>
220	#num#	k4
000	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
sAmazonka	sAmazonka	k1gFnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Řeka	řeka	k1gFnSc1
s	s	k7c7
největším	veliký	k2eAgNnSc7d3
povodím	povodí	k1gNnSc7
</s>
<s>
7	#num#	k4
050	#num#	k4
000	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
Amazonka	Amazonka	k1gFnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
–	–	k?
Kolumbie	Kolumbie	k1gFnSc1
–	–	k?
Peru	prát	k5eAaImIp1nS
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3
řeka	řeka	k1gFnSc1
</s>
<s>
7025	#num#	k4
kmAmazonka	kmAmazonka	k1gFnSc1
–	–	k?
Ucayali	Ucayali	k1gFnSc2
–	–	k?
Tambo	Tamba	k1gFnSc5
–	–	k?
Ene	Ene	k1gMnSc7
–	–	k?
Apurímac	Apurímac	k1gFnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
–	–	k?
Kolumbie	Kolumbie	k1gFnSc1
–	–	k?
Peru	prát	k5eAaImIp1nS
</s>
<s>
Nejdéle	dlouho	k6eAd3
zamrzající	zamrzající	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
</s>
<s>
203	#num#	k4
dnídoba	dnídoba	k1gFnSc1
trvání	trvání	k1gNnSc2
ledového	ledový	k2eAgInSc2d1
příkrovu	příkrov	k1gInSc2
<g/>
,	,	kIx,
řeka	řeka	k1gFnSc1
Lena	Lena	k1gFnSc1
<g/>
,	,	kIx,
Sibiř	Sibiř	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
vodopád	vodopád	k1gInSc1
</s>
<s>
979	#num#	k4
mSalto	mSalto	k1gNnSc1
Ángel	Ángela	k1gFnPc2
<g/>
,	,	kIx,
řeka	řeka	k1gFnSc1
Churum	Churum	k1gInSc1
<g/>
,	,	kIx,
Venezuela	Venezuela	k1gFnSc1
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
nejvyššího	vysoký	k2eAgInSc2d3
stupně	stupeň	k1gInSc2
807	#num#	k4
m	m	kA
</s>
<s>
Vodopád	vodopád	k1gInSc1
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
vodním	vodní	k2eAgInSc7d1
průtokem	průtok	k1gInSc7
</s>
<s>
35	#num#	k4
110	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
sChutes	sChutes	k1gInSc1
Livingstone	Livingston	k1gInSc5
<g/>
,	,	kIx,
řeka	řeka	k1gFnSc1
Kongo	Kongo	k1gNnSc1
<g/>
,	,	kIx,
Demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Kongo	Kongo	k1gNnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3
naměřené	naměřený	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
za	za	k7c4
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
</s>
<s>
22	#num#	k4
987	#num#	k4
mmČérápuňdží	mmČérápuňdž	k1gFnPc2
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
1860	#num#	k4
<g/>
–	–	k?
<g/>
1861	#num#	k4
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3
naměřené	naměřený	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
za	za	k7c4
jeden	jeden	k4xCgInSc4
měsíc	měsíc	k1gInSc4
</s>
<s>
9300	#num#	k4
mmČérápuňdží	mmČérápuňdží	k1gNnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
červenec	červenec	k1gInSc1
1861	#num#	k4
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
zcela	zcela	k6eAd1
svislá	svislý	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
</s>
<s>
1250	#num#	k4
mMount	mMount	k1gMnSc1
Thor	Thor	k1gMnSc1
<g/>
,	,	kIx,
Auyuittuq	Auyuittuq	k1gMnSc1
National	National	k1gFnSc2
Park	park	k1gInSc1
<g/>
,	,	kIx,
Baffinův	Baffinův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
,	,	kIx,
Nunavut	Nunavut	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
takřka	takřka	k6eAd1
svislá	svislý	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
</s>
<s>
1340	#num#	k4
mGreat	mGreat	k1gInSc1
Trango	Trango	k6eAd1
Tower	Tower	k1gInSc4
<g/>
,	,	kIx,
Pákistán	Pákistán	k1gInSc4
(	(	kIx(
<g/>
vrchol	vrchol	k1gInSc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
6286	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Podzemí	podzemí	k1gNnSc1
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
579	#num#	k4
364	#num#	k4
mMamutí	mMamutí	k1gNnPc2
jeskyně	jeskyně	k1gFnSc2
<g/>
,	,	kIx,
Kentucky	Kentucka	k1gFnSc2
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3
sádrovcová	sádrovcový	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
214	#num#	k4
000	#num#	k4
mjeskyně	mjeskyně	k1gFnSc1
Optymistyčna	Optymistyčno	k1gNnSc2
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Nejhlubší	hluboký	k2eAgFnSc1d3
zatopená	zatopený	k2eAgFnSc1d1
sladkovodní	sladkovodní	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
zatím	zatím	k6eAd1
473,5	473,5	k4
mHranická	mHranický	k2eAgFnSc1d1
propast	propast	k1gFnSc1
<g/>
,	,	kIx,
Hranice	hranice	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Česko	Česko	k1gNnSc1
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3
zatopená	zatopený	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
61	#num#	k4
143	#num#	k4
mNohoch	mNohoch	k1gMnSc1
Nah	naho	k1gNnPc2
Chich	Chicha	k1gFnPc2
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Nejhlubší	hluboký	k2eAgFnSc1d3
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
2140	#num#	k4
mjeskyně	mjeskyně	k1gFnSc1
Voronija	Voronija	k1gFnSc1
<g/>
,	,	kIx,
masív	masív	k1gInSc1
Arabika	Arabika	k1gFnSc1
<g/>
,	,	kIx,
západní	západní	k2eAgInSc1d1
Kavkaz	Kavkaz	k1gInSc1
<g/>
,	,	kIx,
Abcházie	Abcházie	k1gFnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Nejhlubší	hluboký	k2eAgInSc1d3
svislý	svislý	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
</s>
<s>
1	#num#	k4
026	#num#	k4
mjeskyně	mjeskyně	k1gFnSc1
Tian	Tiana	k1gFnPc2
Xing	Xinga	k1gFnPc2
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
Největší	veliký	k2eAgFnSc1d3
jeskynní	jeskynní	k2eAgFnSc1d1
prostora	prostora	k1gFnSc1
</s>
<s>
700	#num#	k4
×	×	k?
396	#num#	k4
×	×	k?
70	#num#	k4
mSarawak	mSarawak	k1gMnSc1
Chamber	Chamber	k1gMnSc1
<g/>
,	,	kIx,
jeskyně	jeskyně	k1gFnSc1
Gua	Gua	k1gMnSc1
Nasib	Nasib	k1gMnSc1
Bagus	Bagus	k1gMnSc1
<g/>
,	,	kIx,
Borneo	Borneo	k1gNnSc1
<g/>
,	,	kIx,
Malajsie	Malajsie	k1gFnSc1
</s>
<s>
Nejhlubší	hluboký	k2eAgNnPc1d3
místa	místo	k1gNnPc1
oceánů	oceán	k1gInPc2
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
</s>
<s>
8648	#num#	k4
mprohlubeň	mprohlubeň	k1gFnSc1
Milwaukee	Milwaukee	k1gFnSc1
<g/>
,	,	kIx,
Portorický	portorický	k2eAgInSc1d1
příkop	příkop	k1gInSc1
</s>
<s>
Severní	severní	k2eAgInSc1d1
ledový	ledový	k2eAgInSc1d1
oceán	oceán	k1gInSc1
</s>
<s>
5450	#num#	k4
mLitkeho	mLitkeze	k6eAd1
hlubina	hlubina	k1gFnSc1
<g/>
,	,	kIx,
Euroasijská	euroasijský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
</s>
<s>
Indický	indický	k2eAgInSc1d1
oceán	oceán	k1gInSc1
</s>
<s>
8047	#num#	k4
mprohlubeň	mprohlubeň	k1gFnSc1
Diamantina	Diamantina	k1gFnSc1
<g/>
,	,	kIx,
příkop	příkop	k1gInSc1
Diamantina	Diamantina	k1gFnSc1
<g/>
,	,	kIx,
jihovýchodní	jihovýchodní	k2eAgFnSc1d1
Indická	indický	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
</s>
<s>
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
</s>
<s>
5267	#num#	k4
mprohlubeň	mprohlubeň	k1gFnSc1
Calypso	Calypsa	k1gFnSc5
<g/>
,	,	kIx,
Jónská	jónský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
</s>
<s>
Tichý	tichý	k2eAgInSc1d1
oceán	oceán	k1gInSc1
</s>
<s>
10	#num#	k4
994	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
prohlubeň	prohlubeň	k1gFnSc1
Challenger	Challenger	k1gInSc1
<g/>
,	,	kIx,
Marianský	Marianský	k2eAgInSc1d1
příkop	příkop	k1gInSc1
</s>
<s>
Tučně	tučně	k6eAd1
je	být	k5eAaImIp3nS
označeno	označit	k5eAaPmNgNnS
nejhlubší	hluboký	k2eAgNnSc1d3
místo	místo	k1gNnSc1
ve	v	k7c6
světových	světový	k2eAgInPc6d1
oceánech	oceán	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Nejchladnější	chladný	k2eAgNnPc1d3
a	a	k8xC
nejteplejší	teplý	k2eAgNnPc1d3
obydlená	obydlený	k2eAgNnPc1d1
místa	místo	k1gNnPc1
na	na	k7c6
Zemi	zem	k1gFnSc6
</s>
<s>
Nejteplejší	teplý	k2eAgNnSc1d3
obydlené	obydlený	k2eAgNnSc1d1
místo	místo	k1gNnSc1
</s>
<s>
Mekka	Mekka	k1gFnSc1
<g/>
,	,	kIx,
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
30,8	30,8	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Nejchladnější	chladný	k2eAgNnSc1d3
obydlené	obydlený	k2eAgNnSc1d1
místo	místo	k1gNnSc1
</s>
<s>
Eureka	Eureka	k1gMnSc1
<g/>
,	,	kIx,
Nunavut	Nunavut	k1gMnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
−	−	k?
°	°	k?
<g/>
C	C	kA
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
Ojmjakon	Ojmjakon	k1gNnSc1
<g/>
,	,	kIx,
nejnižší	nízký	k2eAgFnSc1d3
naměřená	naměřený	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
−	−	k?
°	°	k?
<g/>
C	C	kA
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
průměrná	průměrný	k2eAgFnSc1d1
−	−	k?
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Nejsevernější	severní	k2eAgFnSc1d3
a	a	k8xC
nejjižnější	jižní	k2eAgFnSc1d3
pevnina	pevnina	k1gFnSc1
na	na	k7c6
Zemi	zem	k1gFnSc6
</s>
<s>
Nejsevernější	severní	k2eAgNnSc1d3
místo	místo	k1gNnSc1
na	na	k7c6
zemi	zem	k1gFnSc6
</s>
<s>
ostrov	ostrov	k1gInSc1
Kaffeklubben	Kaffeklubbna	k1gFnPc2
<g/>
,	,	kIx,
východně	východně	k6eAd1
od	od	k7c2
Grónska	Grónsko	k1gNnSc2
<g/>
,	,	kIx,
83	#num#	k4
<g/>
°	°	k?
40	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
29	#num#	k4
<g/>
°	°	k?
50	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
<g/>
potenciálně	potenciálně	k6eAd1
83-42	83-42	k4
</s>
<s>
Nejjižnější	jižní	k2eAgNnSc1d3
místo	místo	k1gNnSc1
na	na	k7c6
zemi	zem	k1gFnSc6
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
pól	pól	k1gInSc1
</s>
<s>
Nejjižnější	jižní	k2eAgNnSc1d3
místo	místo	k1gNnSc1
na	na	k7c6
zemi	zem	k1gFnSc6
mimo	mimo	k7c4
Antarktidu	Antarktida	k1gFnSc4
</s>
<s>
mys	mys	k1gInSc1
Froward	Frowarda	k1gFnPc2
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc2
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3
přímá	přímý	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3
přímá	přímý	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
po	po	k7c6
moři	moře	k1gNnSc6
měří	měřit	k5eAaImIp3nS
32	#num#	k4
089	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
;	;	kIx,
po	po	k7c6
souši	souš	k1gFnSc6
11	#num#	k4
241	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Average	Average	k1gInSc1
conditions	conditionsa	k1gFnPc2
for	forum	k1gNnPc2
Verkhoyansk	Verkhoyansk	k1gInSc1
from	from	k1gInSc4
the	the	k?
BBC	BBC	kA
<g/>
.	.	kIx.
www.bbc.co.uk	www.bbc.co.uk	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Life	Life	k1gFnSc1
Is	Is	k1gFnSc1
a	a	k8xC
Chilling	Chilling	k1gInSc1
Challenge	Challeng	k1gInSc2
in	in	k?
Subzero	Subzero	k1gNnSc1
Siberia	Siberium	k1gNnSc2
from	froma	k1gFnPc2
the	the	k?
National	National	k1gFnPc2
Geographic	Geographice	k1gInPc2
<g/>
↑	↑	k?
EL	Ela	k1gFnPc2
FADLI	FADLI	kA
<g/>
,	,	kIx,
Khalid	Khalid	k1gInSc1
I.	I.	kA
<g/>
;	;	kIx,
CERVENY	CERVENY	kA
<g/>
,	,	kIx,
Randall	Randall	k1gMnSc1
S.	S.	kA
<g/>
;	;	kIx,
BURT	BURT	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
C.	C.	kA
World	World	k1gMnSc1
Meteorological	Meteorological	k1gFnSc2
Organization	Organization	k1gInSc1
Assessment	Assessment	k1gMnSc1
of	of	k?
the	the	k?
Purported	Purported	k1gMnSc1
World	World	k1gMnSc1
Record	Record	k1gMnSc1
58	#num#	k4
°	°	k?
<g/>
C	C	kA
Temperature	Temperatur	k1gMnSc5
Extreme	Extrem	k1gInSc5
at	at	k?
El	Ela	k1gFnPc2
Azizia	Azizia	k1gFnSc1
<g/>
,	,	kIx,
Libya	Libya	k1gFnSc1
(	(	kIx(
<g/>
13	#num#	k4
September	September	k1gInSc1
1922	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Meteorological	Meteorological	k1gMnSc4
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
94	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
199	#num#	k4
<g/>
–	–	k?
<g/>
204	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.117	10.117	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
bams-d-	bams-d-	k?
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
93.1	93.1	k4
<g/>
.	.	kIx.
↑	↑	k?
Nejhlubší	hluboký	k2eAgNnSc4d3
místo	místo	k1gNnSc4
na	na	k7c6
Zemi	zem	k1gFnSc6
je	být	k5eAaImIp3nS
10	#num#	k4
994	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
mořem	moře	k1gNnSc7
(	(	kIx(
<g/>
technet	technet	k1gMnSc1
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Extrémní	extrémní	k2eAgInPc1d1
trvale	trvale	k6eAd1
obydlená	obydlený	k2eAgNnPc4d1
místa	místo	k1gNnPc4
světa	svět	k1gInSc2
<g/>
↑	↑	k?
Nejextrémnější	extrémní	k2eAgNnPc1d3
obydlená	obydlený	k2eAgNnPc1d1
místa	místo	k1gNnPc1
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
↑	↑	k?
Canadian	Canadiana	k1gFnPc2
Climate	Climat	k1gInSc5
Normals	Normalsa	k1gFnPc2
1971	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
–	–	k?
Eureka	Eureka	k1gMnSc1
<g/>
.	.	kIx.
www.climate.weatheroffice.ec.gc.ca	www.climate.weatheroffice.ec.gc.ca	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Life	Life	k1gFnSc1
Is	Is	k1gFnSc1
a	a	k8xC
Chilling	Chilling	k1gInSc1
Challenge	Challeng	k1gInSc2
in	in	k?
Subzero	Subzero	k1gNnSc1
Siberia	Siberium	k1gNnSc2
from	froma	k1gFnPc2
the	the	k?
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
<g/>
↑	↑	k?
https://phys.org/news/2018-05-longest-straight-line-ocean-path-planet.html	https://phys.org/news/2018-05-longest-straight-line-ocean-path-planet.html	k1gMnSc1
–	–	k?
Longest	Longest	k1gMnSc1
straight-line	straight-lin	k1gInSc5
ocean	ocean	k1gMnSc1
path	path	k1gMnSc1
on	on	k3xPp3gMnSc1
planet	planeta	k1gFnPc2
Earth	Earth	k1gMnSc1
calculated	calculated	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Rekordy	rekord	k1gInPc1
pozemské	pozemský	k2eAgFnSc2d1
neživé	živý	k2eNgFnSc2d1
přírody	příroda	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
zeměpisných	zeměpisný	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
Česka	Česko	k1gNnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
teplotních	teplotní	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zemské	zemský	k2eAgInPc1d1
oceány	oceán	k1gInPc1
</s>
<s>
Extrémní	extrémní	k2eAgFnPc1d1
teploty	teplota	k1gFnPc1
na	na	k7c6
světě	svět	k1gInSc6
</s>
