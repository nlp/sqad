<p>
<s>
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
trůny	trůn	k1gInPc4	trůn
(	(	kIx(	(
<g/>
A	a	k9	a
Game	game	k1gInSc1	game
of	of	k?	of
Thrones	Thrones	k1gInSc1	Thrones
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
z	z	k7c2	z
fantasy	fantas	k1gInPc4	fantas
série	série	k1gFnSc1	série
Píseň	píseň	k1gFnSc1	píseň
ledu	led	k1gInSc2	led
a	a	k8xC	a
ohně	oheň	k1gInSc2	oheň
od	od	k7c2	od
amerického	americký	k2eAgMnSc2d1	americký
autora	autor	k1gMnSc2	autor
George	Georg	k1gMnSc2	Georg
R.	R.	kA	R.
R.	R.	kA	R.
Martina	Martin	k1gMnSc2	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
cenu	cena	k1gFnSc4	cena
Locus	Locus	k1gInSc1	Locus
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
World	World	k1gInSc4	World
Fantasy	fantas	k1gInPc4	fantas
Award	Awardo	k1gNnPc2	Awardo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Nebula	nebula	k1gFnSc1	nebula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
svazcích	svazek	k1gInPc6	svazek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
adaptována	adaptovat	k5eAaBmNgFnS	adaptovat
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
série	série	k1gFnSc1	série
seriálu	seriál	k1gInSc2	seriál
HBO	HBO	kA	HBO
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nese	nést	k5eAaImIp3nS	nést
stejné	stejný	k2eAgNnSc4d1	stejné
jméno	jméno	k1gNnSc4	jméno
-	-	kIx~	-
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
trůny	trůn	k1gInPc4	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
série	série	k1gFnSc2	série
začíná	začínat	k5eAaImIp3nS	začínat
po	po	k7c6	po
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc4	král
Roberta	Robert	k1gMnSc4	Robert
Baratheona	Baratheon	k1gMnSc4	Baratheon
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
3	[number]	k4	3
hlavní	hlavní	k2eAgFnPc1d1	hlavní
dějové	dějový	k2eAgFnPc1d1	dějová
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
neodehrávají	odehrávat	k5eNaImIp3nP	odehrávat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Západozemí	Západozemí	k1gNnSc6	Západozemí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
obrovském	obrovský	k2eAgInSc6d1	obrovský
východním	východní	k2eAgInSc6d1	východní
kontinentu	kontinent	k1gInSc6	kontinent
zvaném	zvaný	k2eAgInSc6d1	zvaný
Essos	Essos	k1gInSc4	Essos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prolog	prolog	k1gInSc1	prolog
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
obří	obří	k2eAgFnSc2d1	obří
Zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
chránit	chránit	k5eAaImF	chránit
Sedm	sedm	k4xCc4	sedm
království	království	k1gNnPc2	království
Západozemí	Západozemí	k1gNnSc2	Západozemí
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
Noční	noční	k2eAgFnSc2d1	noční
hlídky	hlídka	k1gFnSc2	hlídka
<g/>
,	,	kIx,	,
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
tuto	tento	k3xDgFnSc4	tento
Zeď	zeď	k1gFnSc4	zeď
chránit	chránit	k5eAaImF	chránit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
za	za	k7c7	za
Zdí	zeď	k1gFnSc7	zeď
na	na	k7c6	na
stopě	stopa	k1gFnSc6	stopa
nájezdníků	nájezdník	k1gMnPc2	nájezdník
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
říkají	říkat	k5eAaImIp3nP	říkat
Divocí	divoký	k2eAgMnPc1d1	divoký
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nečekaně	nečekaně	k6eAd1	nečekaně
zmasakrováni	zmasakrovat	k5eAaPmNgMnP	zmasakrovat
nemrtvými	mrtvý	k2eNgMnPc7d1	nemrtvý
tvory	tvor	k1gMnPc7	tvor
<g/>
,	,	kIx,	,
Jinými	jiný	k2eAgNnPc7d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Jediného	jediný	k2eAgInSc2d1	jediný
přeživšího	přeživší	k2eAgInSc2d1	přeživší
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
dezertéra	dezertér	k1gMnSc4	dezertér
z	z	k7c2	z
Noční	noční	k2eAgFnSc2d1	noční
hlídky	hlídka	k1gFnSc2	hlídka
popraví	popravit	k5eAaPmIp3nS	popravit
lord	lord	k1gMnSc1	lord
Eddard	Eddarda	k1gFnPc2	Eddarda
Stark	Stark	k1gInSc1	Stark
<g/>
,	,	kIx,	,
strážce	strážce	k1gMnSc1	strážce
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
dějová	dějový	k2eAgFnSc1d1	dějová
linie	linie	k1gFnSc1	linie
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Západozemí	Západozemí	k1gNnSc6	Západozemí
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
o	o	k7c4	o
politický	politický	k2eAgInSc4d1	politický
boj	boj	k1gInSc4	boj
o	o	k7c6	o
moc	moc	k6eAd1	moc
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojené	spojený	k2eAgFnPc4d1	spojená
intriky	intrika	k1gFnSc2	intrika
<g/>
.	.	kIx.	.
</s>
<s>
Lorda	lord	k1gMnSc2	lord
Starka	starka	k1gFnSc1	starka
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
navštíví	navštívit	k5eAaPmIp3nS	navštívit
jeho	jeho	k3xOp3gNnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Robert	Robert	k1gMnSc1	Robert
Baratheon	Baratheon	k1gMnSc1	Baratheon
<g/>
,	,	kIx,	,
a	a	k8xC	a
žádá	žádat	k5eAaImIp3nS	žádat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
úřadu	úřad	k1gInSc3	úřad
králova	králův	k2eAgMnSc2d1	králův
pobočníka	pobočník	k1gMnSc2	pobočník
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
Králově	Králův	k2eAgNnSc6d1	Královo
přístavišti	přístaviště	k1gNnSc6	přístaviště
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
předchozí	předchozí	k2eAgMnSc1d1	předchozí
pobočník	pobočník	k1gMnSc1	pobočník
Jon	Jon	k1gMnSc1	Jon
Arryn	Arryn	k1gMnSc1	Arryn
<g/>
.	.	kIx.	.
</s>
<s>
Stark	Stark	k1gInSc1	Stark
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
dopisu	dopis	k1gInSc2	dopis
od	od	k7c2	od
Arrynovy	Arrynův	k2eAgFnSc2d1	Arrynův
manželky	manželka	k1gFnSc2	manželka
(	(	kIx(	(
<g/>
sestry	sestra	k1gFnSc2	sestra
lady	lady	k1gFnSc2	lady
Stark	Stark	k1gInSc1	Stark
<g/>
)	)	kIx)	)
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Arryn	Arryn	k1gMnSc1	Arryn
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
,	,	kIx,	,
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc4	jaký
okolnosti	okolnost	k1gFnPc4	okolnost
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
vedly	vést	k5eAaImAgFnP	vést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lord	lord	k1gMnSc1	lord
Stark	Stark	k1gInSc4	Stark
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
do	do	k7c2	do
Králova	Králův	k2eAgNnSc2d1	Královo
přístaviště	přístaviště	k1gNnSc2	přístaviště
bere	brát	k5eAaImIp3nS	brát
své	svůj	k3xOyFgFnPc4	svůj
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
starší	starý	k2eAgFnSc1d2	starší
Sansa	Sansa	k1gFnSc1	Sansa
je	být	k5eAaImIp3nS	být
zasnoubena	zasnoubit	k5eAaPmNgFnS	zasnoubit
s	s	k7c7	s
korunním	korunní	k2eAgMnSc7d1	korunní
princem	princ	k1gMnSc7	princ
Joffrey	Joffrea	k1gFnSc2	Joffrea
Baratheonem	Baratheon	k1gMnSc7	Baratheon
a	a	k8xC	a
na	na	k7c6	na
rodinném	rodinný	k2eAgNnSc6d1	rodinné
sídle	sídlo	k1gNnSc6	sídlo
Zimohradu	Zimohrad	k1gInSc2	Zimohrad
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
ženu	žena	k1gFnSc4	žena
a	a	k8xC	a
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
přístavišti	přístaviště	k1gNnSc6	přístaviště
Eddard	Eddarda	k1gFnPc2	Eddarda
Stark	Stark	k1gInSc4	Stark
vypátrá	vypátrat	k5eAaPmIp3nS	vypátrat
co	co	k9	co
stálo	stát	k5eAaImAgNnS	stát
život	život	k1gInSc4	život
Jona	Jonus	k1gMnSc2	Jonus
Arryna	Arryn	k1gMnSc2	Arryn
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
totiž	totiž	k9	totiž
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
otcem	otec	k1gMnSc7	otec
tří	tři	k4xCgFnPc2	tři
dětí	dítě	k1gFnPc2	dítě
královny	královna	k1gFnSc2	královna
Cersei	Cerse	k1gFnSc2	Cerse
Lannister	Lannister	k1gMnSc1	Lannister
není	být	k5eNaImIp3nS	být
král	král	k1gMnSc1	král
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
produktem	produkt	k1gInSc7	produkt
incestu	incest	k1gInSc2	incest
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
bratrem	bratr	k1gMnSc7	bratr
Jaimem	Jaim	k1gMnSc7	Jaim
<g/>
.	.	kIx.	.
</s>
<s>
Povaha	povaha	k1gFnSc1	povaha
Eddarda	Eddarda	k1gFnSc1	Eddarda
Starka	starka	k1gFnSc1	starka
mu	on	k3xPp3gMnSc3	on
velí	velet	k5eAaImIp3nS	velet
královnu	královna	k1gFnSc4	královna
Cersei	Cerse	k1gFnSc2	Cerse
soukromě	soukromě	k6eAd1	soukromě
konfrontovat	konfrontovat	k5eAaBmF	konfrontovat
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
kompromitovat	kompromitovat	k5eAaBmF	kompromitovat
a	a	k8xC	a
dát	dát	k5eAaPmF	dát
jí	on	k3xPp3gFnSc7	on
tak	tak	k9	tak
možnost	možnost	k1gFnSc4	možnost
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
utéct	utéct	k5eAaPmF	utéct
před	před	k7c7	před
královým	králův	k2eAgInSc7d1	králův
hněvem	hněv	k1gInSc7	hněv
<g/>
,	,	kIx,	,
než	než	k8xS	než
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Robert	Robert	k1gMnSc1	Robert
však	však	k9	však
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
zranění	zranění	k1gNnSc4	zranění
z	z	k7c2	z
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
zatímco	zatímco	k8xS	zatímco
Eddard	Eddard	k1gInSc1	Eddard
Stark	Stark	k1gInSc1	Stark
se	se	k3xPyFc4	se
co	co	k9	co
by	by	k9	by
pobočník	pobočník	k1gMnSc1	pobočník
snaží	snažit	k5eAaImIp3nS	snažit
zasadit	zasadit	k5eAaPmF	zasadit
o	o	k7c4	o
právo	právo	k1gNnSc4	právo
Robertova	Robertův	k2eAgMnSc2d1	Robertův
skutečného	skutečný	k2eAgMnSc2d1	skutečný
dědice	dědic	k1gMnSc2	dědic
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnSc4	jeho
bratra	bratr	k1gMnSc4	bratr
Stannise	Stannise	k1gFnSc2	Stannise
Baratheona	Baratheona	k1gFnSc1	Baratheona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cersei	Cersee	k1gFnSc4	Cersee
využije	využít	k5eAaPmIp3nS	využít
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
nechává	nechávat	k5eAaImIp3nS	nechávat
Starka	starka	k1gFnSc1	starka
uvěznit	uvěznit	k5eAaPmF	uvěznit
a	a	k8xC	a
korunovat	korunovat	k5eAaBmF	korunovat
syna	syn	k1gMnSc4	syn
Joffreyho	Joffrey	k1gMnSc4	Joffrey
<g/>
.	.	kIx.	.
</s>
<s>
Eddarda	Eddarda	k1gFnSc1	Eddarda
Starka	starka	k1gFnSc1	starka
nakonec	nakonec	k6eAd1	nakonec
nechá	nechat	k5eAaPmIp3nS	nechat
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
Sansy	Sansa	k1gFnSc2	Sansa
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
Joffrey	Joffrea	k1gFnSc2	Joffrea
popravit	popravit	k5eAaPmF	popravit
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
i	i	k9	i
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
Cersei	Cerse	k1gFnSc2	Cerse
a	a	k8xC	a
královské	královský	k2eAgFnSc2d1	královská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
popudí	popudit	k5eAaPmIp3nS	popudit
celý	celý	k2eAgInSc4d1	celý
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnSc1d2	mladší
z	z	k7c2	z
dcer	dcera	k1gFnPc2	dcera
Eddarda	Eddarda	k1gFnSc1	Eddarda
Starka	starka	k1gFnSc1	starka
Arya	Arya	k1gFnSc1	Arya
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
před	před	k7c7	před
zajetím	zajetí	k1gNnSc7	zajetí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
Králova	Králův	k2eAgNnSc2d1	Královo
přístaviště	přístaviště	k1gNnSc2	přístaviště
vydává	vydávat	k5eAaPmIp3nS	vydávat
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
za	za	k7c4	za
chlapce	chlapec	k1gMnPc4	chlapec
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Dědic	Dědice	k1gFnPc2	Dědice
Eddarda	Eddarda	k1gFnSc1	Eddarda
Starka	starka	k1gFnSc1	starka
<g/>
,	,	kIx,	,
patnáctiletý	patnáctiletý	k2eAgInSc4d1	patnáctiletý
Robb	Robb	k1gInSc4	Robb
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
svými	svůj	k3xOyFgMnPc7	svůj
vazaly	vazal	k1gMnPc7	vazal
prohlášem	prohláš	k1gMnSc7	prohláš
Králem	Král	k1gMnSc7	Král
severu	sever	k1gInSc2	sever
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
osamostatnit	osamostatnit	k5eAaPmF	osamostatnit
své	svůj	k3xOyFgNnSc4	svůj
mladé	mladý	k2eAgNnSc4d1	mladé
království	království	k1gNnSc4	království
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
Králova	Králův	k2eAgNnSc2d1	Královo
přístaviště	přístaviště	k1gNnSc2	přístaviště
a	a	k8xC	a
pomstít	pomstít	k5eAaPmF	pomstít
nespravedlivou	spravedlivý	k2eNgFnSc4d1	nespravedlivá
smrt	smrt	k1gFnSc4	smrt
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
dějová	dějový	k2eAgFnSc1d1	dějová
linie	linie	k1gFnSc1	linie
ságy	sága	k1gFnSc2	sága
se	se	k3xPyFc4	se
také	také	k9	také
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Západozemí	Západozemí	k1gNnSc6	Západozemí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
Zdi	zeď	k1gFnPc4	zeď
a	a	k8xC	a
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
a	a	k8xC	a
nejde	jít	k5eNaImIp3nS	jít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
o	o	k7c4	o
politické	politický	k2eAgInPc4d1	politický
boje	boj	k1gInPc4	boj
o	o	k7c4	o
Sedm	sedm	k4xCc4	sedm
království	království	k1gNnPc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
protagonistou	protagonista	k1gMnSc7	protagonista
je	být	k5eAaImIp3nS	být
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
Eddarda	Eddarda	k1gFnSc1	Eddarda
Starka	starka	k1gFnSc1	starka
Jon	Jon	k1gFnSc1	Jon
Sníh	sníh	k1gInSc4	sníh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
Jon	Jon	k1gMnSc1	Jon
rozloučí	rozloučit	k5eAaPmIp3nS	rozloučit
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	se	k3xPyFc4	se
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
novými	nový	k2eAgMnPc7d1	nový
bratry	bratr	k1gMnPc7	bratr
z	z	k7c2	z
Noční	noční	k2eAgFnSc2d1	noční
hlídky	hlídka	k1gFnSc2	hlídka
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
doprovodu	doprovod	k1gInSc6	doprovod
za	za	k7c4	za
Zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
dějová	dějový	k2eAgFnSc1d1	dějová
linie	linie	k1gFnSc1	linie
popisuje	popisovat	k5eAaImIp3nS	popisovat
osud	osud	k1gInSc4	osud
Daenerys	Daenerysa	k1gFnPc2	Daenerysa
<g/>
,	,	kIx,	,
dědičky	dědička	k1gFnSc2	dědička
královské	královský	k2eAgFnSc2d1	královská
dynastie	dynastie	k1gFnSc2	dynastie
rodu	rod	k1gInSc2	rod
Targaryenů	Targaryen	k1gInPc2	Targaryen
<g/>
.	.	kIx.	.
</s>
<s>
Targaryenové	Targaryen	k1gMnPc1	Targaryen
vládli	vládnout	k5eAaImAgMnP	vládnout
staletí	staletý	k2eAgMnPc1d1	staletý
v	v	k7c6	v
Západozemí	Západozemí	k1gNnSc6	Západozemí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
svrženi	svržen	k2eAgMnPc1d1	svržen
při	při	k7c6	při
povstání	povstání	k1gNnSc6	povstání
vedeném	vedený	k2eAgNnSc6d1	vedené
Robertem	Robert	k1gMnSc7	Robert
Baratheonem	Baratheon	k1gMnSc7	Baratheon
<g/>
.	.	kIx.	.
</s>
<s>
Daenerys	Daenerys	k6eAd1	Daenerys
musela	muset	k5eAaImAgFnS	muset
ještě	ještě	k6eAd1	ještě
jako	jako	k8xC	jako
dítě	dítě	k1gNnSc4	dítě
prchnout	prchnout	k5eAaPmF	prchnout
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Viserysem	Viserys	k1gMnSc7	Viserys
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
na	na	k7c4	na
kontinent	kontinent	k1gInSc4	kontinent
Essos	Essosa	k1gFnPc2	Essosa
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Západozemí	Západozemí	k1gNnSc2	Západozemí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uplatnila	uplatnit	k5eAaPmAgFnS	uplatnit
svůj	svůj	k3xOyFgInSc4	svůj
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
psychicky	psychicky	k6eAd1	psychicky
labilní	labilní	k2eAgMnSc1d1	labilní
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Viserys	Viserysa	k1gFnPc2	Viserysa
<g/>
,	,	kIx,	,
dědic	dědic	k1gMnSc1	dědic
Targaryenů	Targaryen	k1gMnPc2	Targaryen
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
prodá	prodat	k5eAaPmIp3nS	prodat
Khalu	Khala	k1gFnSc4	Khala
Drogovi	Drog	k1gMnSc3	Drog
<g/>
,	,	kIx,	,
vůdci	vůdce	k1gMnSc3	vůdce
divokých	divoký	k2eAgInPc6d1	divoký
nájezdníků	nájezdník	k1gMnPc2	nájezdník
Dothraků	Dothrak	k1gInPc2	Dothrak
<g/>
,	,	kIx,	,
za	za	k7c4	za
příslib	příslib	k1gInSc4	příslib
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Viseryse	Viseryse	k6eAd1	Viseryse
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gFnSc1	jeho
netrpělivost	netrpělivost	k1gFnSc1	netrpělivost
a	a	k8xC	a
neúcta	neúcta	k1gFnSc1	neúcta
stojí	stát	k5eAaImIp3nS	stát
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
Khal	Khal	k1gInSc1	Khal
Drogo	droga	k1gFnSc5	droga
ho	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Daenerys	Daenerys	k6eAd1	Daenerys
nakonec	nakonec	k6eAd1	nakonec
přijde	přijít	k5eAaPmIp3nS	přijít
i	i	k9	i
o	o	k7c4	o
svého	svůj	k1gMnSc4	svůj
milovaného	milovaný	k2eAgMnSc2d1	milovaný
manžela	manžel	k1gMnSc2	manžel
Droga	droga	k1gFnSc1	droga
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jen	jen	k9	jen
s	s	k7c7	s
hrstkou	hrstka	k1gFnSc7	hrstka
věrných	věrný	k2eAgMnPc2d1	věrný
a	a	k8xC	a
svými	svůj	k3xOyFgInPc7	svůj
čerstvě	čerstvě	k6eAd1	čerstvě
vylíhnutými	vylíhnutý	k2eAgInPc7d1	vylíhnutý
draky	drak	k1gInPc7	drak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vypravěči	vypravěč	k1gMnPc5	vypravěč
==	==	k?	==
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
kapitola	kapitola	k1gFnSc1	kapitola
je	být	k5eAaImIp3nS	být
vyprávěna	vyprávět	k5eAaImNgFnS	vyprávět
v	v	k7c6	v
er-formě	erorma	k1gFnSc6	er-forma
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ne	ne	k9	ne
všechny	všechen	k3xTgInPc1	všechen
dějové	dějový	k2eAgInPc1d1	dějový
zvraty	zvrat	k1gInPc1	zvrat
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
vypravěči	vypravěč	k1gMnPc1	vypravěč
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
stav	stav	k1gInSc1	stav
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
názory	názor	k1gInPc4	názor
a	a	k8xC	a
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
silně	silně	k6eAd1	silně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
styl	styl	k1gInSc4	styl
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
pohledů	pohled	k1gInPc2	pohled
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
nezaujatý	zaujatý	k2eNgMnSc1d1	nezaujatý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Hře	hra	k1gFnSc6	hra
o	o	k7c4	o
trůny	trůn	k1gInPc4	trůn
je	být	k5eAaImIp3nS	být
osm	osm	k4xCc1	osm
takových	takový	k3xDgFnPc2	takový
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vypravěčem	vypravěč	k1gMnSc7	vypravěč
prologu	prolog	k1gInSc2	prolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prolog	prolog	k1gInSc1	prolog
<g/>
:	:	kIx,	:
Will	Will	k1gMnSc1	Will
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Noční	noční	k2eAgFnSc2d1	noční
hlídky	hlídka	k1gFnSc2	hlídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lord	lord	k1gMnSc1	lord
Eddard	Eddard	k1gMnSc1	Eddard
Stark	Stark	k1gInSc4	Stark
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Catelyn	Catelyna	k1gFnPc2	Catelyna
Stark	Stark	k1gInSc4	Stark
</s>
</p>
<p>
<s>
Sansa	Sansa	k1gFnSc1	Sansa
Stark	Stark	k1gInSc1	Stark
</s>
</p>
<p>
<s>
Arya	Arya	k6eAd1	Arya
Stark	Stark	k1gInSc1	Stark
</s>
</p>
<p>
<s>
Bran	brána	k1gFnPc2	brána
Stark	Stark	k1gInSc1	Stark
<g/>
,	,	kIx,	,
osmiletý	osmiletý	k2eAgMnSc1d1	osmiletý
syn	syn	k1gMnSc1	syn
Eddarda	Eddarda	k1gFnSc1	Eddarda
a	a	k8xC	a
Catelyn	Catelyn	k1gInSc1	Catelyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jon	Jon	k?	Jon
Sníh	sníh	k1gInSc1	sníh
</s>
</p>
<p>
<s>
Tyrion	Tyrion	k1gInSc1	Tyrion
Lannister	Lannistra	k1gFnPc2	Lannistra
</s>
</p>
<p>
<s>
Daenerys	Daenerys	k6eAd1	Daenerys
Targaryen	Targaryen	k2eAgInSc1d1	Targaryen
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
trůny	trůn	k1gInPc4	trůn
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
bibliografické	bibliografický	k2eAgFnSc6d1	bibliografická
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
