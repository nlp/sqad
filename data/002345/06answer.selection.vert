<s>
Klinicky	klinicky	k6eAd1	klinicky
zjevná	zjevný	k2eAgFnSc1d1	zjevná
infekce	infekce	k1gFnSc1	infekce
bakterií	bakterie	k1gFnPc2	bakterie
Mycoplasma	Mycoplasma	k1gFnSc1	Mycoplasma
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
(	(	kIx(	(
<g/>
MG	mg	kA	mg
<g/>
)	)	kIx)	)
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
chronická	chronický	k2eAgFnSc1d1	chronická
respirační	respirační	k2eAgFnSc1d1	respirační
nemoc	nemoc	k1gFnSc1	nemoc
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
chronic	chronice	k1gFnPc2	chronice
respiratory	respirator	k1gInPc1	respirator
disease	disease	k6eAd1	disease
<g/>
,	,	kIx,	,
CRD	CRD	kA	CRD
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
jako	jako	k8xC	jako
infekční	infekční	k2eAgFnSc1d1	infekční
sinusitida	sinusitida	k1gFnSc1	sinusitida
<g/>
.	.	kIx.	.
</s>
