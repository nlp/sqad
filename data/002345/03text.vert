<s>
Klinicky	klinicky	k6eAd1	klinicky
zjevná	zjevný	k2eAgFnSc1d1	zjevná
infekce	infekce	k1gFnSc1	infekce
bakterií	bakterie	k1gFnPc2	bakterie
Mycoplasma	Mycoplasma	k1gFnSc1	Mycoplasma
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
(	(	kIx(	(
<g/>
MG	mg	kA	mg
<g/>
)	)	kIx)	)
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
chronická	chronický	k2eAgFnSc1d1	chronická
respirační	respirační	k2eAgFnSc1d1	respirační
nemoc	nemoc	k1gFnSc1	nemoc
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
chronic	chronice	k1gFnPc2	chronice
respiratory	respirator	k1gInPc1	respirator
disease	disease	k6eAd1	disease
<g/>
,	,	kIx,	,
CRD	CRD	kA	CRD
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
jako	jako	k8xS	jako
infekční	infekční	k2eAgFnSc1d1	infekční
sinusitida	sinusitida	k1gFnSc1	sinusitida
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
respiračními	respirační	k2eAgInPc7d1	respirační
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
kýcháním	kýchání	k1gNnSc7	kýchání
<g/>
,	,	kIx,	,
výtokem	výtok	k1gInSc7	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
také	také	k9	také
často	často	k6eAd1	často
sinusitidou	sinusitida	k1gFnSc7	sinusitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
dutin	dutina	k1gFnPc2	dutina
nosních	nosní	k2eAgFnPc2d1	nosní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
nemoc	nemoc	k1gFnSc1	nemoc
probíhá	probíhat	k5eAaImIp3nS	probíhat
chronicky	chronicky	k6eAd1	chronicky
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
morbiditou	morbidita	k1gFnSc7	morbidita
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
nízkou	nízký	k2eAgFnSc7d1	nízká
mortalitou	mortalita	k1gFnSc7	mortalita
<g/>
.	.	kIx.	.
</s>
<s>
Vnímavost	vnímavost	k1gFnSc1	vnímavost
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
je	být	k5eAaImIp3nS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
v	v	k7c6	v
nevyhovujících	vyhovující	k2eNgFnPc6d1	nevyhovující
zoohygienických	zoohygienický	k2eAgFnPc6d1	zoohygienický
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
při	při	k7c6	při
nekvalitní	kvalitní	k2eNgFnSc6d1	nekvalitní
výživě	výživa	k1gFnSc6	výživa
<g/>
,	,	kIx,	,
po	po	k7c6	po
vakcinacích	vakcinace	k1gFnPc6	vakcinace
<g/>
,	,	kIx,	,
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
při	při	k7c6	při
všech	všecek	k3xTgFnPc6	všecek
stresových	stresový	k2eAgFnPc6d1	stresová
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
i	i	k8xC	i
veterinární	veterinární	k2eAgInSc1d1	veterinární
<g/>
;	;	kIx,	;
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
význam	význam	k1gInSc1	význam
CRD	CRD	kA	CRD
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
ztráty	ztráta	k1gFnPc1	ztráta
vznikají	vznikat	k5eAaImIp3nP	vznikat
zejména	zejména	k9	zejména
konfiskací	konfiskace	k1gFnSc7	konfiskace
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
<g/>
,	,	kIx,	,
zhoršenou	zhoršený	k2eAgFnSc7d1	zhoršená
konverzí	konverze	k1gFnSc7	konverze
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
,	,	kIx,	,
sníženou	snížený	k2eAgFnSc7d1	snížená
snáškou	snáška	k1gFnSc7	snáška
a	a	k8xC	a
finančními	finanční	k2eAgInPc7d1	finanční
náklady	náklad	k1gInPc7	náklad
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
výskytu	výskyt	k1gInSc2	výskyt
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
léčbu	léčba	k1gFnSc4	léčba
a	a	k8xC	a
eliminaci	eliminace	k1gFnSc4	eliminace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
Mycoplasma	Mycoplasmum	k1gNnSc2	Mycoplasmum
spp	spp	k?	spp
<g/>
.	.	kIx.	.
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
se	se	k3xPyFc4	se
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
<g/>
.	.	kIx.	.
</s>
<s>
Dodd	Dodd	k1gInSc1	Dodd
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
popsal	popsat	k5eAaPmAgMnS	popsat
"	"	kIx"	"
<g/>
oteklou	oteklý	k2eAgFnSc4d1	oteklá
hlavu	hlava	k1gFnSc4	hlava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
příznaků	příznak	k1gInPc2	příznak
onemocnění	onemocnění	k1gNnPc2	onemocnění
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Nelson	Nelson	k1gMnSc1	Nelson
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
při	při	k7c6	při
rýmě	rýma	k1gFnSc6	rýma
drůbeže	drůbež	k1gFnSc2	drůbež
nález	nález	k1gInSc1	nález
kokobacilárních	kokobacilární	k2eAgNnPc2d1	kokobacilární
tělísek	tělísko	k1gNnPc2	tělísko
v	v	k7c6	v
nosním	nosní	k2eAgInSc6d1	nosní
odměšku	odměšek	k1gInSc6	odměšek
a	a	k8xC	a
pěstoval	pěstovat	k5eAaImAgMnS	pěstovat
je	on	k3xPp3gFnPc4	on
v	v	k7c6	v
kuřecích	kuřecí	k2eAgNnPc6d1	kuřecí
embryích	embryo	k1gNnPc6	embryo
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
buněčných	buněčný	k2eAgFnPc6d1	buněčná
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
byly	být	k5eAaImAgInP	být
první	první	k4xOgInPc1	první
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
autoři	autor	k1gMnPc1	autor
pracovali	pracovat	k5eAaImAgMnP	pracovat
s	s	k7c7	s
bakterií	bakterie	k1gFnPc2	bakterie
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gNnSc4	gallisepticum
(	(	kIx(	(
<g/>
MG	mg	kA	mg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
onemocnění	onemocnění	k1gNnSc1	onemocnění
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
popsali	popsat	k5eAaPmAgMnP	popsat
Dickinson	Dickinson	k1gMnSc1	Dickinson
a	a	k8xC	a
Hinshaw	Hinshaw	k1gMnSc1	Hinshaw
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
také	také	k9	také
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
infekční	infekční	k2eAgFnSc1d1	infekční
sinusitida	sinusitida	k1gFnSc1	sinusitida
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
původce	původce	k1gMnPc4	původce
sinusitidy	sinusitida	k1gFnSc2	sinusitida
<g/>
,	,	kIx,	,
považovaného	považovaný	k2eAgInSc2d1	považovaný
nejdříve	dříve	k6eAd3	dříve
za	za	k7c4	za
virus	virus	k1gInSc4	virus
<g/>
,	,	kIx,	,
kultivovat	kultivovat	k5eAaImF	kultivovat
v	v	k7c6	v
kuřecích	kuřecí	k2eAgNnPc6d1	kuřecí
embryích	embryo	k1gNnPc6	embryo
a	a	k8xC	a
vyvolat	vyvolat	k5eAaPmF	vyvolat
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
mykoplasmózu	mykoplasmóza	k1gFnSc4	mykoplasmóza
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc4	nemoc
označenou	označený	k2eAgFnSc4d1	označená
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
chronická	chronický	k2eAgFnSc1d1	chronická
respirační	respirační	k2eAgFnSc1d1	respirační
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
"	"	kIx"	"
popsali	popsat	k5eAaPmAgMnP	popsat
Delaplane	Delaplan	k1gMnSc5	Delaplan
a	a	k8xC	a
Stuart	Stuarta	k1gFnPc2	Stuarta
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Markham	Markham	k1gInSc1	Markham
a	a	k8xC	a
Wong	Wong	k1gInSc1	Wong
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
kultivovali	kultivovat	k5eAaImAgMnP	kultivovat
mykoplasmata	mykoplasma	k1gNnPc4	mykoplasma
z	z	k7c2	z
kuřat	kuře	k1gNnPc2	kuře
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
na	na	k7c6	na
pevných	pevný	k2eAgFnPc6d1	pevná
půdách	půda	k1gFnPc6	půda
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
van	van	k1gInSc1	van
Roekelem	Roekel	k1gInSc7	Roekel
a	a	k8xC	a
Olesiukem	Olesiuk	k1gInSc7	Olesiuk
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
poukázali	poukázat	k5eAaPmAgMnP	poukázat
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
podobnost	podobnost	k1gFnSc4	podobnost
<g/>
.	.	kIx.	.
</s>
<s>
Jungherr	Jungherr	k1gInSc1	Jungherr
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
popsali	popsat	k5eAaPmAgMnP	popsat
kultivační	kultivační	k2eAgInSc4d1	kultivační
a	a	k8xC	a
biochemické	biochemický	k2eAgFnPc4d1	biochemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
mykoplasmat	mykoplasma	k1gNnPc2	mykoplasma
izolovaných	izolovaný	k2eAgNnPc2d1	izolované
z	z	k7c2	z
kuřat	kuře	k1gNnPc2	kuře
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
celosvětové	celosvětový	k2eAgFnPc4d1	celosvětová
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
intenzivních	intenzivní	k2eAgInPc6d1	intenzivní
chovech	chov	k1gInPc6	chov
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
intenzivním	intenzivní	k2eAgInPc3d1	intenzivní
preventivním	preventivní	k2eAgInPc3d1	preventivní
a	a	k8xC	a
eradikačním	eradikační	k2eAgInPc3d1	eradikační
programům	program	k1gInPc3	program
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
25	[number]	k4	25
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
výskyt	výskyt	k1gInSc1	výskyt
onemocnění	onemocnění	k1gNnSc2	onemocnění
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
velkochovech	velkochov	k1gInPc6	velkochov
s	s	k7c7	s
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
věkovými	věkový	k2eAgFnPc7d1	věková
kategoriemi	kategorie	k1gFnPc7	kategorie
drůbeže	drůbež	k1gFnSc2	drůbež
problém	problém	k1gInSc4	problém
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
MG	mg	kA	mg
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
drobnochovech	drobnochov	k1gInPc6	drobnochov
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gNnSc1	gallisepticum
(	(	kIx(	(
<g/>
MG	mg	kA	mg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
patogenní	patogenní	k2eAgInSc4d1	patogenní
druh	druh	k1gInSc4	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Mycoplasma	Mycoplasmum	k1gNnSc2	Mycoplasmum
čeledě	čeleď	k1gFnSc2	čeleď
Mycoplasmataceae	Mycoplasmatacea	k1gFnSc2	Mycoplasmatacea
<g/>
.	.	kIx.	.
</s>
<s>
MG	mg	kA	mg
se	se	k3xPyFc4	se
barví	barvit	k5eAaImIp3nS	barvit
dobře	dobře	k6eAd1	dobře
podle	podle	k7c2	podle
Giemsy	Giemsa	k1gFnSc2	Giemsa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
slabě	slabě	k6eAd1	slabě
gramnegativní	gramnegativní	k2eAgNnSc1d1	gramnegativní
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
kokoidní	kokoidní	k2eAgInSc1d1	kokoidní
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnSc2d1	průměrná
velikosti	velikost	k1gFnSc2	velikost
0,25	[number]	k4	0,25
<g/>
-	-	kIx~	-
<g/>
0,5	[number]	k4	0,5
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Replikační	Replikační	k2eAgInSc1d1	Replikační
cyklus	cyklus	k1gInSc1	cyklus
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
více	hodně	k6eAd2	hodně
způsobů	způsob	k1gInPc2	způsob
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
hostitelské	hostitelský	k2eAgFnSc6d1	hostitelská
buňce	buňka	k1gFnSc6	buňka
a	a	k8xC	a
podmínkách	podmínka	k1gFnPc6	podmínka
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
MG	mg	kA	mg
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
komplexní	komplexní	k2eAgNnSc4d1	komplexní
médium	médium	k1gNnSc4	médium
obohacené	obohacený	k2eAgNnSc4d1	obohacené
10-15	[number]	k4	10-15
%	%	kIx~	%
teplem	teplo	k1gNnSc7	teplo
inaktivovovaného	inaktivovovaný	k2eAgInSc2d1	inaktivovovaný
prasečího	prasečí	k2eAgInSc2d1	prasečí
<g/>
,	,	kIx,	,
ptačího	ptačí	k2eAgMnSc2d1	ptačí
nebo	nebo	k8xC	nebo
koňského	koňský	k2eAgNnSc2d1	koňské
séra	sérum	k1gNnSc2	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ptačí	ptačí	k2eAgNnPc4d1	ptačí
mykoplasmata	mykoplasma	k1gNnPc4	mykoplasma
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tekuté	tekutý	k2eAgFnPc4d1	tekutá
i	i	k8xC	i
pevné	pevný	k2eAgFnPc4d1	pevná
kultivační	kultivační	k2eAgFnPc4d1	kultivační
půdy	půda	k1gFnPc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgFnPc1d1	optimální
podmínky	podmínka	k1gFnPc1	podmínka
růstu	růst	k1gInSc2	růst
jsou	být	k5eAaImIp3nP	být
pH	ph	kA	ph
7.8	[number]	k4	7.8
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
37-38	[number]	k4	37-38
°	°	k?	°
<g/>
C.	C.	kA	C.
Tvorba	tvorba	k1gFnSc1	tvorba
kolonií	kolonie	k1gFnPc2	kolonie
na	na	k7c6	na
agaru	agar	k1gInSc6	agar
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
delší	dlouhý	k2eAgFnSc4d2	delší
inkubaci	inkubace	k1gFnSc4	inkubace
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlhkou	vlhký	k2eAgFnSc4d1	vlhká
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
kolonie	kolonie	k1gFnPc1	kolonie
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgFnPc1d1	drobná
<g/>
,	,	kIx,	,
hladké	hladký	k2eAgFnPc1d1	hladká
<g/>
,	,	kIx,	,
kruhovité	kruhovitý	k2eAgFnPc1d1	kruhovitá
<g/>
,	,	kIx,	,
průhledné	průhledný	k2eAgFnPc1d1	průhledná
masy	masa	k1gFnPc1	masa
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
centrální	centrální	k2eAgFnSc7d1	centrální
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
0,2	[number]	k4	0,2
<g/>
-	-	kIx~	-
<g/>
0,3	[number]	k4	0,3
mm	mm	kA	mm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
;	;	kIx,	;
často	často	k6eAd1	často
splývají	splývat	k5eAaImIp3nP	splývat
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gNnSc1	gallisepticum
se	se	k3xPyFc4	se
také	také	k9	také
pomnožuje	pomnožovat	k5eAaImIp3nS	pomnožovat
v	v	k7c6	v
kuřecích	kuřecí	k2eAgNnPc6d1	kuřecí
embryích	embryo	k1gNnPc6	embryo
<g/>
.	.	kIx.	.
</s>
<s>
MG	mg	kA	mg
fermentuje	fermentovat	k5eAaBmIp3nS	fermentovat
glukózu	glukóza	k1gFnSc4	glukóza
a	a	k8xC	a
maltózu	maltóza	k1gFnSc4	maltóza
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Nefermentuje	fermentovat	k5eNaBmIp3nS	fermentovat
laktózu	laktóza	k1gFnSc4	laktóza
<g/>
,	,	kIx,	,
dulcitol	dulcitol	k1gInSc4	dulcitol
ani	ani	k8xC	ani
salicin	salicin	k1gInSc4	salicin
<g/>
.	.	kIx.	.
</s>
<s>
Sacharóza	sacharóza	k1gFnSc1	sacharóza
je	být	k5eAaImIp3nS	být
vzácně	vzácně	k6eAd1	vzácně
fermentována	fermentován	k2eAgFnSc1d1	fermentována
<g/>
;	;	kIx,	;
výsledky	výsledek	k1gInPc1	výsledek
s	s	k7c7	s
galaktózou	galaktóza	k1gFnSc7	galaktóza
<g/>
,	,	kIx,	,
fruktózou	fruktóza	k1gFnSc7	fruktóza
<g/>
,	,	kIx,	,
trehalózou	trehalóza	k1gFnSc7	trehalóza
a	a	k8xC	a
manitolem	manitol	k1gInSc7	manitol
jsou	být	k5eAaImIp3nP	být
variabilní	variabilní	k2eAgFnPc1d1	variabilní
<g/>
.	.	kIx.	.
<g/>
Nehydrolyzuje	hydrolyzovat	k5eNaBmIp3nS	hydrolyzovat
arginin	arginin	k2eAgMnSc1d1	arginin
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
fosfatáza	fosfatáza	k1gFnSc1	fosfatáza
negativní	negativní	k2eAgFnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Redukuje	redukovat	k5eAaBmIp3nS	redukovat
tetrazolium	tetrazolium	k1gNnSc1	tetrazolium
<g/>
.	.	kIx.	.
</s>
<s>
MG	mg	kA	mg
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
kompletní	kompletní	k2eAgFnSc4d1	kompletní
hemolýzu	hemolýza	k1gFnSc4	hemolýza
koňských	koňský	k2eAgInPc2d1	koňský
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
inkorporovaných	inkorporovaný	k2eAgInPc2d1	inkorporovaný
do	do	k7c2	do
agarového	agarový	k2eAgNnSc2d1	agarový
média	médium	k1gNnSc2	médium
<g/>
;	;	kIx,	;
aglutinuje	aglutinovat	k5eAaImIp3nS	aglutinovat
erytrocyty	erytrocyt	k1gInPc7	erytrocyt
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dezinfekčních	dezinfekční	k2eAgInPc2d1	dezinfekční
prostředků	prostředek	k1gInPc2	prostředek
inaktivuje	inaktivovat	k5eAaBmIp3nS	inaktivovat
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
rezistentní	rezistentní	k2eAgMnSc1d1	rezistentní
na	na	k7c4	na
penicilin	penicilin	k1gInSc4	penicilin
a	a	k8xC	a
octan	octan	k1gInSc4	octan
thalný	thalný	k2eAgInSc4d1	thalný
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4000	[number]	k4	4000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
inhibitory	inhibitor	k1gInPc4	inhibitor
bakteriální	bakteriální	k2eAgInPc4d1	bakteriální
a	a	k8xC	a
plísňové	plísňový	k2eAgFnPc4d1	plísňová
kontaminace	kontaminace	k1gFnPc4	kontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
MG	mg	kA	mg
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
životné	životný	k2eAgFnPc1d1	životná
v	v	k7c6	v
kuřecím	kuřecí	k2eAgInSc6d1	kuřecí
trusu	trus	k1gInSc6	trus
1-3	[number]	k4	1-3
dny	den	k1gInPc4	den
při	při	k7c6	při
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
ve	v	k7c6	v
vaječném	vaječný	k2eAgInSc6d1	vaječný
žloutku	žloutek	k1gInSc6	žloutek
18	[number]	k4	18
týdnů	týden	k1gInPc2	týden
při	při	k7c6	při
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
nebo	nebo	k8xC	nebo
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
při	při	k7c6	při
20	[number]	k4	20
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
infikovaných	infikovaný	k2eAgNnPc6d1	infikované
násadových	násadový	k2eAgNnPc6d1	násadový
vejcích	vejce	k1gNnPc6	vejce
je	být	k5eAaImIp3nS	být
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
inaktivována	inaktivovat	k5eAaBmNgFnS	inaktivovat
za	za	k7c4	za
12-14	[number]	k4	12-14
hodin	hodina	k1gFnPc2	hodina
při	při	k7c6	při
45,6	[number]	k4	45,6
°	°	k?	°
<g/>
C.	C.	kA	C.
Izoláty	izolát	k1gInPc4	izolát
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc4	gallisepticum
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
patogenitou	patogenita	k1gFnSc7	patogenita
pro	pro	k7c4	pro
kuřecí	kuřecí	k2eAgNnPc4d1	kuřecí
embrya	embryo	k1gNnPc4	embryo
i	i	k8xC	i
kuřata	kuře	k1gNnPc4	kuře
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
povaze	povaha	k1gFnSc6	povaha
izolátu	izolát	k1gInSc2	izolát
<g/>
,	,	kIx,	,
způsobů	způsob	k1gInPc2	způsob
kultivace	kultivace	k1gFnSc2	kultivace
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
pasáží	pasáž	k1gFnPc2	pasáž
in	in	k?	in
vitro	vitro	k1gNnSc4	vitro
<g/>
.	.	kIx.	.
</s>
<s>
Žloutek	žloutek	k1gInSc1	žloutek
kuřecího	kuřecí	k2eAgNnSc2d1	kuřecí
embrya	embryo	k1gNnSc2	embryo
infikovaného	infikovaný	k2eAgNnSc2d1	infikované
MG	mg	kA	mg
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
virulentnější	virulentní	k2eAgFnSc1d2	virulentní
než	než	k8xS	než
bujónová	bujónový	k2eAgFnSc1d1	bujónová
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Mortalita	mortalita	k1gFnSc1	mortalita
embryí	embryo	k1gNnPc2	embryo
je	být	k5eAaImIp3nS	být
inhibována	inhibovat	k5eAaBmNgFnS	inhibovat
přítomností	přítomnost	k1gFnSc7	přítomnost
mateřských	mateřský	k2eAgFnPc2d1	mateřská
protilátek	protilátka	k1gFnPc2	protilátka
ve	v	k7c6	v
žloutku	žloutek	k1gInSc6	žloutek
<g/>
.	.	kIx.	.
</s>
<s>
Krůty	krůta	k1gFnPc1	krůta
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
vnímavější	vnímavý	k2eAgFnSc2d2	vnímavější
k	k	k7c3	k
experimentální	experimentální	k2eAgFnSc3d1	experimentální
infekci	infekce	k1gFnSc3	infekce
než	než	k8xS	než
kur	kura	k1gFnPc2	kura
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc2d1	vyskytující
infekce	infekce	k1gFnSc2	infekce
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc4	gallisepticum
jsou	být	k5eAaImIp3nP	být
nejčastější	častý	k2eAgInPc1d3	nejčastější
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácí	domácí	k1gFnSc2	domácí
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
MG	mg	kA	mg
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
také	také	k9	také
izolována	izolován	k2eAgFnSc1d1	izolována
u	u	k7c2	u
bažantů	bažant	k1gMnPc2	bažant
(	(	kIx(	(
<g/>
Phasianus	Phasianus	k1gMnSc1	Phasianus
colchicus	colchicus	k1gMnSc1	colchicus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orebic	orebice	k1gFnPc2	orebice
(	(	kIx(	(
<g/>
Alectoris	Alectoris	k1gFnSc1	Alectoris
gracea	gracea	k1gMnSc1	gracea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pávů	páv	k1gMnPc2	páv
(	(	kIx(	(
<g/>
Pavo	Pavo	k1gMnSc1	Pavo
cristatus	cristatus	k1gMnSc1	cristatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
virginských	virginský	k2eAgFnPc2d1	virginská
křepelek	křepelka	k1gFnPc2	křepelka
(	(	kIx(	(
<g/>
Colinus	Colinus	k1gMnSc1	Colinus
virginianus	virginianus	k1gMnSc1	virginianus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
japonských	japonský	k2eAgFnPc2d1	japonská
křepelek	křepelka	k1gFnPc2	křepelka
(	(	kIx(	(
<g/>
Coturnix	Coturnix	k1gInSc1	Coturnix
coturnix	coturnix	k1gInSc1	coturnix
japonica	japonica	k1gFnSc1	japonica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
zlatého	zlatý	k1gInSc2	zlatý
bažanta	bažant	k1gMnSc2	bažant
(	(	kIx(	(
<g/>
Chrysolophus	Chrysolophus	k1gMnSc1	Chrysolophus
pictus	pictus	k1gMnSc1	pictus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlutohlavého	žlutohlavý	k2eAgMnSc4d1	žlutohlavý
amazoňana	amazoňan	k1gMnSc4	amazoňan
(	(	kIx(	(
<g/>
Amazona	amazona	k1gFnSc1	amazona
ochrocephala	ochrocephal	k1gMnSc2	ochrocephal
auropalliata	auropalliat	k1gMnSc2	auropalliat
<g/>
)	)	kIx)	)
i	i	k9	i
kachen	kachna	k1gFnPc2	kachna
a	a	k8xC	a
husí	husa	k1gFnPc2	husa
<g/>
.	.	kIx.	.
</s>
<s>
Izolace	izolace	k1gFnSc1	izolace
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gNnSc1	gallisepticum
z	z	k7c2	z
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
sice	sice	k8xC	sice
byly	být	k5eAaImAgFnP	být
popsány	popsat	k5eAaPmNgFnP	popsat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
signifikantnost	signifikantnost	k1gFnSc1	signifikantnost
těchto	tento	k3xDgFnPc2	tento
zpráv	zpráva	k1gFnPc2	zpráva
nebyla	být	k5eNaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
patogenita	patogenita	k1gFnSc1	patogenita
MG	mg	kA	mg
pro	pro	k7c4	pro
takové	takový	k3xDgMnPc4	takový
ptáky	pták	k1gMnPc4	pták
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Andulky	Andulka	k1gFnPc4	Andulka
experimentálně	experimentálně	k6eAd1	experimentálně
infikované	infikovaný	k2eAgFnPc4d1	infikovaná
MG	mg	kA	mg
reagovaly	reagovat	k5eAaBmAgInP	reagovat
klinicky	klinicky	k6eAd1	klinicky
i	i	k9	i
patologicky	patologicky	k6eAd1	patologicky
(	(	kIx(	(
<g/>
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
průdušnici	průdušnice	k1gFnSc6	průdušnice
a	a	k8xC	a
vzdušných	vzdušný	k2eAgInPc6d1	vzdušný
vacích	vak	k1gInPc6	vak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
mortality	mortalita	k1gFnSc2	mortalita
<g/>
.	.	kIx.	.
</s>
<s>
MG	mg	kA	mg
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
horizontálně	horizontálně	k6eAd1	horizontálně
přímým	přímý	k2eAgInSc7d1	přímý
kontaktem	kontakt	k1gInSc7	kontakt
vnímavých	vnímavý	k2eAgNnPc2d1	vnímavé
kuřat	kuře	k1gNnPc2	kuře
nebo	nebo	k8xC	nebo
krůťat	krůtě	k1gNnPc2	krůtě
s	s	k7c7	s
infikovanými	infikovaný	k2eAgMnPc7d1	infikovaný
jedinci	jedinec	k1gMnPc7	jedinec
(	(	kIx(	(
<g/>
aerogenně	aerogenně	k6eAd1	aerogenně
<g/>
,	,	kIx,	,
kapénková	kapénkový	k2eAgFnSc1d1	kapénková
infekce	infekce	k1gFnSc1	infekce
<g/>
,	,	kIx,	,
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
latentně	latentně	k6eAd1	latentně
infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
nosiči	nosič	k1gMnPc1	nosič
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
kontaminovaným	kontaminovaný	k2eAgNnSc7d1	kontaminované
prostředím	prostředí	k1gNnSc7	prostředí
(	(	kIx(	(
<g/>
krmivo	krmivo	k1gNnSc1	krmivo
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
při	při	k7c6	při
horizontální	horizontální	k2eAgFnSc6d1	horizontální
infekci	infekce	k1gFnSc6	infekce
je	být	k5eAaImIp3nS	být
respirační	respirační	k2eAgInSc4d1	respirační
trakt	trakt	k1gInSc4	trakt
s	s	k7c7	s
následným	následný	k2eAgInSc7d1	následný
katarálním	katarální	k2eAgInSc7d1	katarální
zánětem	zánět	k1gInSc7	zánět
sliznic	sliznice	k1gFnPc2	sliznice
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
,	,	kIx,	,
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
nosních	nosní	k2eAgFnPc2d1	nosní
dutin	dutina	k1gFnPc2	dutina
a	a	k8xC	a
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
epizootologický	epizootologický	k2eAgInSc1d1	epizootologický
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
vertikální	vertikální	k2eAgInSc1d1	vertikální
přenos	přenos	k1gInSc1	přenos
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
gallisepticum	gallisepticum	k1gInSc1	gallisepticum
byla	být	k5eAaImAgFnS	být
izolována	izolovat	k5eAaBmNgFnS	izolovat
z	z	k7c2	z
vejcovodu	vejcovod	k1gInSc2	vejcovod
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
slepic	slepice	k1gFnPc2	slepice
a	a	k8xC	a
semene	semeno	k1gNnSc2	semeno
kohoutů	kohout	k1gInPc2	kohout
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
vejcem	vejce	k1gNnSc7	vejce
byl	být	k5eAaImAgInS	být
opakovaně	opakovaně	k6eAd1	opakovaně
experimentálně	experimentálně	k6eAd1	experimentálně
prokázán	prokázat	k5eAaPmNgInS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
infikována	infikovat	k5eAaBmNgFnS	infikovat
transovariálně	transovariálně	k6eAd1	transovariálně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
dobré	dobrý	k2eAgFnSc6d1	dobrá
odolnosti	odolnost	k1gFnSc6	odolnost
hostitele	hostitel	k1gMnSc2	hostitel
přechází	přecházet	k5eAaImIp3nS	přecházet
proces	proces	k1gInSc4	proces
v	v	k7c6	v
inaparentní	inaparentní	k2eAgFnSc6d1	inaparentní
infekci	infekce	k1gFnSc6	infekce
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
latentními	latentní	k2eAgInPc7d1	latentní
nosiči	nosič	k1gInPc7	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
průběh	průběh	k1gInSc1	průběh
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
věkem	věk	k1gInSc7	věk
a	a	k8xC	a
konstitucí	konstituce	k1gFnSc7	konstituce
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
množstvím	množství	k1gNnSc7	množství
a	a	k8xC	a
virulencí	virulence	k1gFnSc7	virulence
původce	původce	k1gMnSc2	původce
a	a	k8xC	a
především	především	k9	především
predispozičními	predispoziční	k2eAgInPc7d1	predispoziční
faktory	faktor	k1gInPc7	faktor
(	(	kIx(	(
<g/>
vysoká	vysoká	k1gFnSc1	vysoká
koncentrace	koncentrace	k1gFnSc2	koncentrace
drůbeže	drůbež	k1gFnSc2	drůbež
v	v	k7c6	v
halách	hala	k1gFnPc6	hala
<g/>
,	,	kIx,	,
nepříznivé	příznivý	k2eNgFnPc1d1	nepříznivá
podmínky	podmínka	k1gFnPc1	podmínka
chovu	chov	k1gInSc2	chov
jako	jako	k8xC	jako
vysoká	vysoký	k2eAgFnSc1d1	vysoká
prašnost	prašnost	k1gFnSc1	prašnost
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
škodlivéplyny	škodlivéplyna	k1gFnSc2	škodlivéplyna
<g/>
,	,	kIx,	,
přehřátí	přehřátí	k1gNnSc2	přehřátí
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
dosažení	dosažení	k1gNnSc4	dosažení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
,	,	kIx,	,
vakcinace	vakcinace	k1gFnSc1	vakcinace
<g/>
,	,	kIx,	,
transport	transport	k1gInSc1	transport
<g/>
,	,	kIx,	,
dietetické	dietetický	k2eAgFnPc1d1	dietetická
závady	závada	k1gFnPc1	závada
<g/>
,	,	kIx,	,
různá	různý	k2eAgNnPc1d1	různé
onemocnění	onemocnění	k1gNnPc1	onemocnění
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
stresory	stresor	k1gInPc1	stresor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
po	po	k7c6	po
vertikální	vertikální	k2eAgFnSc6d1	vertikální
infekci	infekce	k1gFnSc6	infekce
je	být	k5eAaImIp3nS	být
4-6	[number]	k4	4-6
dní	den	k1gInPc2	den
<g/>
;	;	kIx,	;
při	při	k7c6	při
horizontálním	horizontální	k2eAgInSc6d1	horizontální
přenosu	přenos	k1gInSc6	přenos
14	[number]	k4	14
dní	den	k1gInPc2	den
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
predispozičními	predispoziční	k2eAgInPc7d1	predispoziční
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
přesnější	přesný	k2eAgFnSc4d2	přesnější
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
praktických	praktický	k2eAgFnPc6d1	praktická
podmínkách	podmínka	k1gFnPc6	podmínka
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
stanovit	stanovit	k5eAaPmF	stanovit
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
MG	mg	kA	mg
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
.	.	kIx.	.
</s>
<s>
Subklinické	Subklinický	k2eAgFnPc1d1	Subklinická
infekce	infekce	k1gFnPc1	infekce
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
převážně	převážně	k6eAd1	převážně
nižšími	nízký	k2eAgInPc7d2	nižší
hmotnostními	hmotnostní	k2eAgInPc7d1	hmotnostní
přírůstky	přírůstek	k1gInPc7	přírůstek
<g/>
,	,	kIx,	,
sníženým	snížený	k2eAgInSc7d1	snížený
příjmem	příjem	k1gInSc7	příjem
krmiva	krmivo	k1gNnSc2	krmivo
a	a	k8xC	a
redukcí	redukce	k1gFnPc2	redukce
snášky	snáška	k1gFnSc2	snáška
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
morbiditou	morbidita	k1gFnSc7	morbidita
<g/>
,	,	kIx,	,
mortalita	mortalita	k1gFnSc1	mortalita
je	být	k5eAaImIp3nS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
interkurentními	interkurentní	k2eAgFnPc7d1	interkurentní
infekcemi	infekce	k1gFnPc7	infekce
(	(	kIx(	(
<g/>
E.	E.	kA	E.
coli	coli	k1gNnSc1	coli
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnSc1d1	infekční
bronchitida	bronchitida	k1gFnSc1	bronchitida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chovech	chov	k1gInPc6	chov
brojlerů	brojler	k1gMnPc2	brojler
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
výskytům	výskyt	k1gInPc3	výskyt
nejčastěji	často	k6eAd3	často
mezi	mezi	k7c4	mezi
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
týdnem	týden	k1gInSc7	týden
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
3-5	[number]	k4	3-5
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mladších	mladý	k2eAgMnPc2d2	mladší
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
průběh	průběh	k1gInSc4	průběh
nemoci	nemoc	k1gFnSc2	nemoc
horší	zlý	k2eAgMnSc1d2	horší
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
také	také	k9	také
častější	častý	k2eAgInSc1d2	častější
v	v	k7c6	v
chladnějším	chladný	k2eAgNnSc6d2	chladnější
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Líhnivost	Líhnivost	k1gFnSc1	Líhnivost
násadových	násadový	k2eAgNnPc2d1	násadový
vajec	vejce	k1gNnPc2	vejce
z	z	k7c2	z
postižených	postižený	k2eAgInPc2d1	postižený
rozmnožovacích	rozmnožovací	k2eAgInPc2d1	rozmnožovací
chovů	chov	k1gInPc2	chov
(	(	kIx(	(
<g/>
RCH	RCH	kA	RCH
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zhoršená	zhoršený	k2eAgFnSc1d1	zhoršená
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgInPc7d1	typický
příznaky	příznak	k1gInPc7	příznak
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
jsou	být	k5eAaImIp3nP	být
tracheální	tracheální	k2eAgInPc4d1	tracheální
chrapoty	chrapot	k1gInPc4	chrapot
<g/>
,	,	kIx,	,
hlenovitý	hlenovitý	k2eAgInSc4d1	hlenovitý
až	až	k8xS	až
hnisavý	hnisavý	k2eAgInSc4d1	hnisavý
výtok	výtok	k1gInSc4	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
kýchání	kýchání	k1gNnSc2	kýchání
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
zvířata	zvíře	k1gNnPc1	zvíře
snaží	snažit	k5eAaImIp3nP	snažit
zbavit	zbavit	k5eAaPmF	zbavit
hlenu	hlen	k1gInSc3	hlen
nahromaděného	nahromaděný	k2eAgMnSc2d1	nahromaděný
v	v	k7c6	v
dýchacích	dýchací	k2eAgFnPc6d1	dýchací
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
postižení	postižení	k1gNnSc6	postižení
paranazálních	paranazální	k2eAgFnPc2d1	paranazální
dutin	dutina	k1gFnPc2	dutina
<g/>
,	,	kIx,	,
podočnicových	podočnicový	k2eAgInPc2d1	podočnicový
sinusů	sinus	k1gInPc2	sinus
a	a	k8xC	a
spojivkového	spojivkový	k2eAgInSc2d1	spojivkový
vaku	vak	k1gInSc2	vak
se	se	k3xPyFc4	se
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
zduření	zduření	k1gNnSc1	zduření
kůže	kůže	k1gFnSc2	kůže
na	na	k7c6	na
tvářích	tvář	k1gFnPc6	tvář
a	a	k8xC	a
očních	oční	k2eAgNnPc6d1	oční
víčkách	víčko	k1gNnPc6	víčko
<g/>
,	,	kIx,	,
slzení	slzení	k1gNnSc1	slzení
a	a	k8xC	a
překrvení	překrvení	k1gNnSc1	překrvení
cév	céva	k1gFnPc2	céva
ve	v	k7c6	v
spojivce	spojivka	k1gFnSc6	spojivka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
akutní	akutní	k2eAgFnSc2d1	akutní
fáze	fáze	k1gFnSc2	fáze
přechází	přecházet	k5eAaImIp3nS	přecházet
nemoc	nemoc	k1gFnSc4	nemoc
do	do	k7c2	do
subklinicky	subklinicky	k6eAd1	subklinicky
probíhajícího	probíhající	k2eAgInSc2d1	probíhající
chronického	chronický	k2eAgInSc2d1	chronický
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
hlavně	hlavně	k9	hlavně
snížením	snížení	k1gNnSc7	snížení
užitkovosti	užitkovost	k1gFnSc2	užitkovost
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
lze	lze	k6eAd1	lze
nemoc	nemoc	k1gFnSc4	nemoc
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
pouze	pouze	k6eAd1	pouze
sérologicky	sérologicky	k6eAd1	sérologicky
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
jako	jako	k8xS	jako
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácí	domácí	k2eAgInSc1d1	domácí
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
zduření	zduření	k1gNnSc1	zduření
infraorbitálních	infraorbitální	k2eAgInPc2d1	infraorbitální
sinusů	sinus	k1gInPc2	sinus
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
výraznější	výrazný	k2eAgMnSc1d2	výraznější
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
až	až	k9	až
k	k	k7c3	k
deformacím	deformace	k1gFnPc3	deformace
tvaru	tvar	k1gInSc2	tvar
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
onemocnění	onemocnění	k1gNnPc2	onemocnění
nejsou	být	k5eNaImIp3nP	být
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
změny	změna	k1gFnPc4	změna
příliš	příliš	k6eAd1	příliš
výrazné	výrazný	k2eAgFnPc4d1	výrazná
<g/>
.	.	kIx.	.
</s>
<s>
Zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
se	se	k3xPyFc4	se
katarální	katarální	k2eAgMnSc1d1	katarální
<g/>
,	,	kIx,	,
hlenovitý	hlenovitý	k2eAgInSc1d1	hlenovitý
až	až	k8xS	až
hnisavý	hnisavý	k2eAgInSc1d1	hnisavý
zánět	zánět	k1gInSc1	zánět
nosní	nosní	k2eAgFnSc2d1	nosní
dutiny	dutina	k1gFnSc2	dutina
<g/>
,	,	kIx,	,
paranazálních	paranazální	k2eAgInPc2d1	paranazální
sinusů	sinus	k1gInPc2	sinus
<g/>
,	,	kIx,	,
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
,	,	kIx,	,
průdušek	průduška	k1gFnPc2	průduška
a	a	k8xC	a
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
exsudát	exsudát	k1gInSc1	exsudát
houstne	houstnout	k5eAaImIp3nS	houstnout
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dutiny	dutina	k1gFnPc4	dutina
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
vyplňovat	vyplňovat	k5eAaImF	vyplňovat
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
konjunktivální	konjunktivální	k2eAgInSc4d1	konjunktivální
vak	vak	k1gInSc4	vak
<g/>
,	,	kIx,	,
a	a	k8xC	a
zatlačit	zatlačit	k5eAaPmF	zatlačit
oční	oční	k2eAgInSc4d1	oční
bulbus	bulbus	k1gInSc4	bulbus
<g/>
.	.	kIx.	.
</s>
<s>
Sliznice	sliznice	k1gFnSc1	sliznice
průdušnice	průdušnice	k1gFnSc2	průdušnice
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
šedým	šedý	k2eAgInSc7d1	šedý
hlenem	hlen	k1gInSc7	hlen
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
i	i	k8xC	i
žlutými	žlutý	k2eAgFnPc7d1	žlutá
pablánami	pablána	k1gFnPc7	pablána
<g/>
.	.	kIx.	.
</s>
<s>
Serózy	seróza	k1gFnPc1	seróza
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
svůj	svůj	k3xOyFgInSc4	svůj
průhledný	průhledný	k2eAgInSc4d1	průhledný
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
stávají	stávat	k5eAaImIp3nP	stávat
se	s	k7c7	s
mléčně	mléčně	k6eAd1	mléčně
zakalenými	zakalený	k2eAgInPc7d1	zakalený
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgFnP	pokrýt
vločkami	vločka	k1gFnPc7	vločka
bledě	bledě	k6eAd1	bledě
žlutého	žlutý	k2eAgInSc2d1	žlutý
exsudátu	exsudát	k1gInSc2	exsudát
až	až	k9	až
souvislými	souvislý	k2eAgFnPc7d1	souvislá
fibrinovými	fibrinův	k2eAgFnPc7d1	fibrinův
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Fibrinové	Fibrinový	k2eAgInPc1d1	Fibrinový
povlaky	povlak	k1gInPc1	povlak
se	se	k3xPyFc4	se
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
i	i	k9	i
na	na	k7c6	na
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
orgánech	orgán	k1gInPc6	orgán
(	(	kIx(	(
<g/>
perikarditida	perikarditida	k1gFnSc1	perikarditida
<g/>
,	,	kIx,	,
perihepatitida	perihepatitida	k1gFnSc1	perihepatitida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
kontaminace	kontaminace	k1gFnSc2	kontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
záněty	zánět	k1gInPc1	zánět
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
šlachových	šlachový	k2eAgFnPc2d1	šlachová
pochev	pochva	k1gFnPc2	pochva
<g/>
.	.	kIx.	.
</s>
<s>
MG	mg	kA	mg
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
způsobovat	způsobovat	k5eAaImF	způsobovat
zánět	zánět	k1gInSc4	zánět
vejcovodu	vejcovod	k1gInSc2	vejcovod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prodělané	prodělaný	k2eAgFnSc6d1	prodělaná
infekci	infekce	k1gFnSc6	infekce
vzniká	vznikat	k5eAaImIp3nS	vznikat
buněčná	buněčný	k2eAgFnSc1d1	buněčná
i	i	k8xC	i
humorální	humorální	k2eAgFnSc1d1	humorální
imunita	imunita	k1gFnSc1	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
imunní	imunní	k2eAgNnPc1d1	imunní
zvířata	zvíře	k1gNnPc1	zvíře
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nosiči	nosič	k1gInSc3	nosič
MG	mg	kA	mg
a	a	k8xC	a
přenášet	přenášet	k5eAaImF	přenášet
infekci	infekce	k1gFnSc4	infekce
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
vnímavá	vnímavý	k2eAgNnPc4d1	vnímavé
zvířata	zvíře	k1gNnPc4	zvíře
kontaktem	kontakt	k1gInSc7	kontakt
nebo	nebo	k8xC	nebo
vejcem	vejce	k1gNnSc7	vejce
na	na	k7c4	na
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
Protilátky	protilátka	k1gFnPc1	protilátka
v	v	k7c6	v
respiračních	respirační	k2eAgInPc6d1	respirační
sekretech	sekret	k1gInPc6	sekret
sliznice	sliznice	k1gFnPc1	sliznice
inhibují	inhibovat	k5eAaBmIp3nP	inhibovat
přilnutí	přilnutí	k1gNnSc4	přilnutí
MG	mg	kA	mg
k	k	k7c3	k
tracheálním	tracheální	k2eAgFnPc3d1	tracheální
epiteliálním	epiteliální	k2eAgFnPc3d1	epiteliální
buňkám	buňka	k1gFnPc3	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezření	k1gNnSc4	podezření
na	na	k7c6	na
CRD	CRD	kA	CRD
vzbuzují	vzbuzovat	k5eAaImIp3nP	vzbuzovat
jednak	jednak	k8xC	jednak
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
krátké	krátká	k1gFnSc2	krátká
ID	Ida	k1gFnPc2	Ida
při	při	k7c6	při
hemofilové	hemofil	k1gMnPc1	hemofil
rýmě	rýma	k1gFnSc3	rýma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pitevní	pitevní	k2eAgInSc1d1	pitevní
nález	nález	k1gInSc1	nález
není	být	k5eNaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc1d2	veliký
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
histologické	histologický	k2eAgFnPc4d1	histologická
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
lymfofolikulární	lymfofolikulární	k2eAgFnPc1d1	lymfofolikulární
reakce	reakce	k1gFnPc1	reakce
ve	v	k7c6	v
sliznici	sliznice	k1gFnSc6	sliznice
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
a	a	k8xC	a
ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
izolace	izolace	k1gFnSc1	izolace
mykoplasmat	mykoplasma	k1gNnPc2	mykoplasma
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
sérologická	sérologický	k2eAgFnSc1d1	sérologická
typizace	typizace	k1gFnSc1	typizace
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
antimikrobiálních	antimikrobiální	k2eAgFnPc2d1	antimikrobiální
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
krmivu	krmivo	k1gNnSc6	krmivo
<g/>
,	,	kIx,	,
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
parenterálně	parenterálně	k6eAd1	parenterálně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
klinicky	klinicky	k6eAd1	klinicky
postižených	postižený	k2eAgNnPc2d1	postižené
hejn	hejno	k1gNnPc2	hejno
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
i	i	k9	i
případné	případný	k2eAgFnSc2d1	případná
ztráty	ztráta	k1gFnSc2	ztráta
úhynem	úhyn	k1gInSc7	úhyn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezajišťuje	zajišťovat	k5eNaImIp3nS	zajišťovat
vymizení	vymizení	k1gNnSc4	vymizení
mykoplasmat	mykoplasma	k1gNnPc2	mykoplasma
z	z	k7c2	z
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
léčby	léčba	k1gFnSc2	léčba
nejsou	být	k5eNaImIp3nP	být
vždy	vždy	k6eAd1	vždy
jednoznačné	jednoznačný	k2eAgFnPc1d1	jednoznačná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ovlivňovány	ovlivňovat	k5eAaImNgFnP	ovlivňovat
podmínkami	podmínka	k1gFnPc7	podmínka
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
chovů	chov	k1gInPc2	chov
<g/>
,	,	kIx,	,
věkem	věk	k1gInSc7	věk
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
také	také	k9	také
interkurentními	interkurentní	k2eAgFnPc7d1	interkurentní
infekcemi	infekce	k1gFnPc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
aplikaci	aplikace	k1gFnSc6	aplikace
navíc	navíc	k6eAd1	navíc
vzniká	vznikat	k5eAaImIp3nS	vznikat
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
vzniku	vznik	k1gInSc2	vznik
rezistence	rezistence	k1gFnSc2	rezistence
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
častému	častý	k2eAgInSc3d1	častý
vertikálnímu	vertikální	k2eAgInSc3d1	vertikální
přenosu	přenos	k1gInSc3	přenos
mykoplasmat	mykoplasma	k1gNnPc2	mykoplasma
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc3	jejich
subklinickému	subklinický	k2eAgInSc3d1	subklinický
výskytu	výskyt	k1gInSc3	výskyt
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
s	s	k7c7	s
negativním	negativní	k2eAgInSc7d1	negativní
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
užitkovost	užitkovost	k1gFnSc4	užitkovost
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
provádět	provádět	k5eAaImF	provádět
komplexní	komplexní	k2eAgInPc4d1	komplexní
eradikační	eradikační	k2eAgInPc4d1	eradikační
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
chovných	chovný	k2eAgNnPc2d1	chovné
hejn	hejno	k1gNnPc2	hejno
prostých	prostý	k2eAgNnPc2d1	prosté
mykoplasmat	mykoplasma	k1gNnPc2	mykoplasma
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
zajištění	zajištění	k1gNnSc6	zajištění
optimálních	optimální	k2eAgFnPc2d1	optimální
zoohygienických	zoohygienický	k2eAgFnPc2d1	zoohygienický
podmínek	podmínka	k1gFnPc2	podmínka
chovných	chovný	k2eAgNnPc2d1	chovné
hejn	hejno	k1gNnPc2	hejno
<g/>
,	,	kIx,	,
ochraně	ochrana	k1gFnSc6	ochrana
před	před	k7c7	před
zavlečením	zavlečení	k1gNnSc7	zavlečení
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
provádění	provádění	k1gNnSc4	provádění
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
zkoušek	zkouška	k1gFnPc2	zkouška
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
snášky	snáška	k1gFnSc2	snáška
<g/>
,	,	kIx,	,
dodržování	dodržování	k1gNnSc1	dodržování
technologických	technologický	k2eAgInPc2d1	technologický
postupů	postup	k1gInPc2	postup
a	a	k8xC	a
turnusového	turnusový	k2eAgInSc2d1	turnusový
způsobu	způsob	k1gInSc2	způsob
chovu	chov	k1gInSc2	chov
<g/>
,	,	kIx,	,
pečlivě	pečlivě	k6eAd1	pečlivě
prováděné	prováděný	k2eAgFnSc3d1	prováděná
dezinfekci	dezinfekce	k1gFnSc3	dezinfekce
a	a	k8xC	a
asanaci	asanace	k1gFnSc3	asanace
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vytvoření	vytvoření	k1gNnSc1	vytvoření
hejn	hejno	k1gNnPc2	hejno
prostých	prostý	k2eAgFnPc2d1	prostá
mykoplasmat	mykoplasma	k1gNnPc2	mykoplasma
v	v	k7c4	v
dodržování	dodržování	k1gNnSc4	dodržování
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
chovů	chov	k1gInPc2	chov
vakcinace	vakcinace	k1gFnSc2	vakcinace
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
komerční	komerční	k2eAgFnSc2d1	komerční
vakcíny	vakcína	k1gFnSc2	vakcína
živé	živý	k2eAgMnPc4d1	živý
i	i	k8xC	i
inaktivované	inaktivovaný	k2eAgMnPc4d1	inaktivovaný
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
používání	používání	k1gNnSc6	používání
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nS	různit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
