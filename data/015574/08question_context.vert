<s>
Západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
(	(	kIx(
<g/>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
moderní	moderní	k2eAgNnSc4d1
označení	označení	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c4
vnímání	vnímání	k1gNnSc4
současníků	současník	k1gMnPc2
existovala	existovat	k5eAaImAgFnS
jen	jen	k9
jedna	jeden	k4xCgFnSc1
Římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
Imperium	Imperium	k1gNnSc1
Romanum	Romanum	k?
<g/>
)	)	kIx)
v	v	k7c6
čele	čelo	k1gNnSc6
se	s	k7c7
dvěma	dva	k4xCgInPc7
císaři	císař	k1gMnPc7
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
po	po	k7c6
rozdělení	rozdělení	k1gNnSc6
Římské	římský	k2eAgFnSc2d1
říše	říše	k1gFnSc2
<g/>
.	.	kIx.
</s>