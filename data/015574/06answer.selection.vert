<s>
Západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
476	#num#	k4
abdikací	abdikace	k1gFnPc2
mladistvého	mladistvý	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Romula	Romulus	k1gMnSc2
Augustula	Augustul	k1gMnSc2
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
byl	být	k5eAaImAgMnS
přinucen	přinutit	k5eAaPmNgMnS
Odoakerem	Odoaker	k1gInSc7
–	–	k?
germánským	germánský	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
římské	římský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>