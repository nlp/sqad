<s>
Západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Římská	římský	k2eAgFnSc1d1
říšeSenatus	říšeSenatus	k1gInSc4
Populusque	Populusqu	k1gInSc2
Romanus	Romanus	k1gInSc1
(	(	kIx(
<g/>
la	la	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Imperium	Imperium	k1gNnSc1
Romanum	Romanum	k?
(	(	kIx(
<g/>
la	la	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
←	←	k?
</s>
<s>
395	#num#	k4
<g/>
–	–	k?
<g/>
476	#num#	k4
<g/>
/	/	kIx~
<g/>
480	#num#	k4
</s>
<s>
↓	↓	k?
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
Západořímské	západořímský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
395	#num#	k4
n.	n.	k?
l.	l.	k?
po	po	k7c6
smrti	smrt	k1gFnSc6
císaře	císař	k1gMnSc2
Theodosia	Theodosium	k1gNnSc2
I.	I.	kA
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Mediolanum	Mediolanum	k1gNnSc1
(	(	kIx(
<g/>
395	#num#	k4
<g/>
–	–	k?
<g/>
402	#num#	k4
<g/>
)	)	kIx)
<g/>
Ravenna	Ravenn	k1gInSc2
(	(	kIx(
<g/>
402	#num#	k4
<g/>
–	–	k?
<g/>
455	#num#	k4
<g/>
,	,	kIx,
473	#num#	k4
<g/>
–	–	k?
<g/>
476	#num#	k4
<g/>
)	)	kIx)
<g/>
Řím	Řím	k1gInSc1
(	(	kIx(
<g/>
455	#num#	k4
<g/>
–	–	k?
<g/>
473	#num#	k4
<g/>
)	)	kIx)
<g/>
Salona	Salona	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Spalatum	Spalatum	k1gNnSc1
(	(	kIx(
<g/>
475	#num#	k4
<g/>
–	–	k?
<g/>
480	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
000	#num#	k4
000	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
rok	rok	k1gInSc1
395	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Římané	Říman	k1gMnPc1
<g/>
,	,	kIx,
Germáni	Germán	k1gMnPc1
<g/>
,	,	kIx,
Galové	Gal	k1gMnPc1
<g/>
,	,	kIx,
Iberové	Iber	k1gMnPc1
<g/>
,	,	kIx,
Židé	Žid	k1gMnPc1
<g/>
,	,	kIx,
Britanové	Britanový	k2eAgFnPc1d1
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
latina	latina	k1gFnSc1
<g/>
,	,	kIx,
regionální	regionální	k2eAgFnSc1d1
/	/	kIx~
lokální	lokální	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
polyteistické	polyteistický	k2eAgInPc4d1
(	(	kIx(
<g/>
do	do	k7c2
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
nikajské	nikajský	k2eAgNnSc1d1
křesťanství	křesťanství	k1gNnSc1
(	(	kIx(
<g/>
státní	státní	k2eAgMnSc1d1
od	od	k7c2
380	#num#	k4
<g/>
)	)	kIx)
<g/>
ariánské	ariánský	k2eAgNnSc1d1
křesťanství	křesťanství	k1gNnSc1
(	(	kIx(
<g/>
zavržené	zavržený	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
autokracie	autokracie	k1gFnSc1
<g/>
,	,	kIx,
tetrarchie	tetrarchie	k1gFnSc1
(	(	kIx(
<g/>
286	#num#	k4
<g/>
–	–	k?
<g/>
402	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Odoakerovo	Odoakerův	k2eAgNnSc1d1
Italské	italský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Dalmácie	Dalmácie	k1gFnSc1
</s>
<s>
Syagriova	Syagriův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Východořímská	východořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Vizigótské	vizigótský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Vandalské	vandalský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Franské	franský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Svébské	Svébský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Burgundské	burgundský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Alemánie	Alemánie	k1gFnSc1
</s>
<s>
Armorica	Armorica	k6eAd1
</s>
<s>
Římsko-maurské	římsko-maurský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Post-římská	Post-římský	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
(	(	kIx(
<g/>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
moderní	moderní	k2eAgNnSc4d1
označení	označení	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c4
vnímání	vnímání	k1gNnSc4
současníků	současník	k1gMnPc2
existovala	existovat	k5eAaImAgFnS
jen	jen	k9
jedna	jeden	k4xCgFnSc1
Římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
Imperium	Imperium	k1gNnSc1
Romanum	Romanum	k?
<g/>
)	)	kIx)
v	v	k7c6
čele	čelo	k1gNnSc6
se	s	k7c7
dvěma	dva	k4xCgInPc7
císaři	císař	k1gMnPc7
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
po	po	k7c6
rozdělení	rozdělení	k1gNnSc6
Římské	římský	k2eAgFnSc2d1
říše	říše	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
395	#num#	k4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
ji	on	k3xPp3gFnSc4
císař	císař	k1gMnSc1
Theodosius	Theodosius	k1gMnSc1
I.	I.	kA
odkázal	odkázat	k5eAaPmAgMnS
svým	svůj	k3xOyFgInSc7
dvěma	dva	k4xCgFnPc7
synům	syn	k1gMnPc3
<g/>
:	:	kIx,
západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
Flaviu	Flavius	k1gMnSc3
Honoriovi	Honorius	k1gMnSc3
a	a	k8xC
východní	východní	k2eAgFnSc3d1
Flaviu	Flavius	k1gMnSc6
Arcadiovi	Arcadius	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
476	#num#	k4
abdikací	abdikace	k1gFnPc2
mladistvého	mladistvý	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Romula	Romulus	k1gMnSc2
Augustula	Augustul	k1gMnSc2
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
byl	být	k5eAaImAgMnS
přinucen	přinutit	k5eAaPmNgMnS
Odoakerem	Odoaker	k1gInSc7
–	–	k?
germánským	germánský	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
římské	římský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
vojenský	vojenský	k2eAgInSc4d1
převrat	převrat	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ponechal	ponechat	k5eAaPmAgInS
stát	stát	k5eAaImF,k5eAaPmF
nedotčen	dotknout	k5eNaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římská	římský	k2eAgFnSc1d1
moc	moc	k1gFnSc1
na	na	k7c6
západě	západ	k1gInSc6
byla	být	k5eAaImAgNnP
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
omezena	omezit	k5eAaPmNgFnS
už	už	k6eAd1
jen	jen	k6eAd1
na	na	k7c4
Itálii	Itálie	k1gFnSc4
a	a	k8xC
alpský	alpský	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
jen	jen	k9
osmdesát	osmdesát	k4xCc4
jedna	jeden	k4xCgFnSc1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
celkem	celkem	k6eAd1
dvanáct	dvanáct	k4xCc4
legitimních	legitimní	k2eAgMnPc2d1
vládců	vládce	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
ještě	ještě	k6eAd1
tři	tři	k4xCgFnPc4
(	(	kIx(
<g/>
podle	podle	k7c2
některých	některý	k3yIgInPc2
názorů	názor	k1gInPc2
čtyři	čtyři	k4xCgMnPc4
<g/>
)	)	kIx)
uzurpátory	uzurpátor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Honoriova	Honoriův	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
</s>
<s>
Západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
s	s	k7c7
vyznačenými	vyznačený	k2eAgNnPc7d1
městy	město	k1gNnPc7
Římem	Řím	k1gInSc7
a	a	k8xC
Ravennou	Ravenný	k2eAgFnSc4d1
v	v	k7c6
roce	rok	k1gInSc6
395	#num#	k4
n.	n.	k?
l.	l.	k?
</s>
<s>
Honorius	Honorius	k1gMnSc1
byl	být	k5eAaImAgMnS
mladším	mladý	k2eAgMnSc7d2
synem	syn	k1gMnSc7
císaře	císař	k1gMnSc2
Theodosia	Theodosium	k1gNnSc2
I.	I.	kA
<g/>
,	,	kIx,
posledního	poslední	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
vládl	vládnout	k5eAaImAgInS
oběma	dva	k4xCgFnPc3
částem	část	k1gFnPc3
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flavius	Flavius	k1gMnSc1
Honorius	Honorius	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
době	doba	k1gFnSc6
nástupu	nástup	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
ještě	ještě	k9
dítě	dítě	k1gNnSc4
<g/>
,	,	kIx,
z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
ustanovil	ustanovit	k5eAaPmAgMnS
Theodosius	Theodosius	k1gMnSc1
velitele	velitel	k1gMnSc2
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
Stilichona	Stilichon	k1gMnSc2
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
vandalského	vandalský	k2eAgMnSc2d1
šlechtice	šlechtic	k1gMnSc2
a	a	k8xC
Římanky	Římanka	k1gFnPc4
<g/>
,	,	kIx,
Honoriovým	Honoriový	k2eAgMnSc7d1
poručníkem	poručník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Honoriova	Honoriův	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
nestabilní	stabilní	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
397	#num#	k4
se	se	k3xPyFc4
vzbouřil	vzbouřit	k5eAaPmAgInS
Gildo	gilda	k1gFnSc5
<g/>
,	,	kIx,
správce	správce	k1gMnSc1
provincie	provincie	k1gFnSc2
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
invazi	invaze	k1gFnSc6
Vizigótů	Vizigót	k1gMnPc2
do	do	k7c2
Itálie	Itálie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
402	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Honorius	Honorius	k1gInSc1
přemístil	přemístit	k5eAaPmAgInS
se	s	k7c7
svým	svůj	k3xOyFgInSc7
dvorem	dvůr	k1gInSc7
z	z	k7c2
Milána	Milán	k1gInSc2
do	do	k7c2
Ravenny	Ravenna	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
nedobytnou	dobytný	k2eNgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
posádky	posádka	k1gFnSc2
v	v	k7c6
Británii	Británie	k1gFnSc6
se	se	k3xPyFc4
vzbouřili	vzbouřit	k5eAaPmAgMnP
nejprve	nejprve	k6eAd1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Marca	Marcum	k1gNnSc2
(	(	kIx(
<g/>
406	#num#	k4
<g/>
-	-	kIx~
<g/>
407	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
pak	pak	k6eAd1
Konstantina	Konstantin	k1gMnSc4
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
407	#num#	k4
<g/>
-	-	kIx~
<g/>
411	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojsko	vojsko	k1gNnSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
přemístilo	přemístit	k5eAaPmAgNnS
z	z	k7c2
Británie	Británie	k1gFnSc2
do	do	k7c2
Galie	Galie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
ponechalo	ponechat	k5eAaPmAgNnS
tak	tak	k6eAd1
zdejší	zdejší	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
pokládalo	pokládat	k5eAaImAgNnS
za	za	k7c4
Římany	Říman	k1gMnPc4
<g/>
,	,	kIx,
svému	svůj	k3xOyFgInSc3
osudu	osud	k1gInSc3
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
napospas	napospas	k6eAd1
vpádům	vpád	k1gInPc3
germánských	germánský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
Anglů	Angl	k1gMnPc2
a	a	k8xC
Sasů	Sas	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
původně	původně	k6eAd1
přicházeli	přicházet	k5eAaImAgMnP
do	do	k7c2
Británie	Británie	k1gFnSc2
v	v	k7c6
malých	malý	k2eAgFnPc6d1
skupinkách	skupinka	k1gFnPc6
především	především	k6eAd1
jako	jako	k8xC,k8xS
římští	římský	k2eAgMnPc1d1
žoldnéři	žoldnér	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Vedle	vedle	k7c2
toho	ten	k3xDgNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prolomení	prolomení	k1gNnSc3
rýnské	rýnský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
406	#num#	k4
vpadli	vpadnout	k5eAaPmAgMnP
Vandalové	Vandal	k1gMnPc1
<g/>
,	,	kIx,
Svébové	Svébové	k2eAgMnPc1d1
a	a	k8xC
íránští	íránský	k2eAgMnPc1d1
Alani	Alan	k1gMnPc1
přes	přes	k7c4
zamrzlou	zamrzlý	k2eAgFnSc4d1
řeku	řeka	k1gFnSc4
do	do	k7c2
severní	severní	k2eAgFnSc2d1
Galie	Galie	k1gFnSc2
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
byli	být	k5eAaImAgMnP
na	na	k7c6
útěku	útěk	k1gInSc6
před	před	k7c7
Huny	Hun	k1gMnPc7
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Mohuče	Mohuč	k1gFnSc2
porazili	porazit	k5eAaPmAgMnP
franské	franský	k2eAgFnPc4d1
foederati	foederat	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
postavili	postavit	k5eAaPmAgMnP
na	na	k7c4
odpor	odpor	k1gInSc4
a	a	k8xC
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
nakonec	nakonec	k6eAd1
poraženi	poražen	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
převážně	převážně	k6eAd1
gótské	gótský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
vedené	vedený	k2eAgNnSc1d1
Radagaisem	Radagais	k1gInSc7
napadlo	napadnout	k5eAaPmAgNnS
samotnou	samotný	k2eAgFnSc4d1
Itálii	Itálie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vojsko	vojsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
však	však	k9
zničeno	zničen	k2eAgNnSc1d1
Stilichonem	Stilichon	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
kvůli	kvůli	k7c3
obavě	obava	k1gFnSc3
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
přílišné	přílišný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
nechal	nechat	k5eAaPmAgInS
Honorius	Honorius	k1gInSc1
svého	svůj	k3xOyFgMnSc4
rádce	rádce	k1gMnSc4
a	a	k8xC
velitele	velitel	k1gMnSc4
vojska	vojsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
408	#num#	k4
zavraždit	zavraždit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
však	však	k9
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
loupeživým	loupeživý	k2eAgNnPc3d1
tažením	tažení	k1gNnPc3
Vizigótů	Vizigót	k1gMnPc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
náčelníka	náčelník	k1gMnSc2
Alaricha	Alarich	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
roce	rok	k1gInSc6
410	#num#	k4
nakonec	nakonec	k6eAd1
vydrancoval	vydrancovat	k5eAaPmAgInS
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mělo	mít	k5eAaImAgNnS
neblahý	blahý	k2eNgInSc4d1
účinek	účinek	k1gInSc4
na	na	k7c4
římskou	římský	k2eAgFnSc4d1
psychiku	psychika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proběhlo	proběhnout	k5eAaPmAgNnS
také	také	k6eAd1
několik	několik	k4yIc1
uzurpací	uzurpace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
Konstantinově	Konstantinův	k2eAgFnSc3d1
vzpouře	vzpoura	k1gFnSc3
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
výše	vysoce	k6eAd2
a	a	k8xC
později	pozdě	k6eAd2
k	k	k7c3
uzurpaci	uzurpace	k1gFnSc3
senátora	senátor	k1gMnSc2
Prisca	Priscus	k1gMnSc2
Attala	Attala	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
409-410	409-410	k4
a	a	k8xC
414	#num#	k4
<g/>
-	-	kIx~
<g/>
415	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvouletém	dvouletý	k2eAgNnSc6d1
drancování	drancování	k1gNnSc6
a	a	k8xC
plenění	plenění	k1gNnSc6
Galie	Galie	k1gFnSc2
napadli	napadnout	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
409	#num#	k4
Svébové	Svébová	k1gFnSc2
<g/>
,	,	kIx,
Vandalové	Vandal	k1gMnPc1
a	a	k8xC
Alani	Alaeň	k1gFnSc3
Hispánii	Hispánie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
však	však	k9
Vizigóti	Vizigót	k1gMnPc1
část	část	k1gFnSc4
Vandalů	Vandal	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
pronikli	proniknout	k5eAaPmAgMnP
do	do	k7c2
Hispánie	Hispánie	k1gFnSc2
<g/>
,	,	kIx,
zničili	zničit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vizigóti	Vizigót	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
roce	rok	k1gInSc6
418	#num#	k4
dokonce	dokonce	k9
usazeni	usazen	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
federáti	federát	k1gMnPc1
v	v	k7c6
Akvitánii	Akvitánie	k1gFnSc6
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vláda	vláda	k1gFnSc1
v	v	k7c6
Ravenně	Ravenně	k1gFnSc6
získala	získat	k5eAaPmAgFnS
vnitřní	vnitřní	k2eAgFnSc1d1
hráz	hráz	k1gFnSc1
proti	proti	k7c3
povstáním	povstání	k1gNnPc3
a	a	k8xC
rovněž	rovněž	k9
silný	silný	k2eAgInSc4d1
bojový	bojový	k2eAgInSc4d1
svaz	svaz	k1gInSc4
proti	proti	k7c3
vnějším	vnější	k2eAgMnPc3d1
nepřátelům	nepřítel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vizigóti	Vizigót	k1gMnPc1
si	se	k3xPyFc3
počínali	počínat	k5eAaImAgMnP
celkem	celkem	k6eAd1
loajálně	loajálně	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jim	on	k3xPp3gMnPc3
však	však	k9
nebránilo	bránit	k5eNaImAgNnS
v	v	k7c6
občasném	občasný	k2eAgNnSc6d1
narušování	narušování	k1gNnSc6
římského	římský	k2eAgNnSc2d1
území	území	k1gNnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
ke	k	k7c3
zvětšování	zvětšování	k1gNnSc3
vlastní	vlastní	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
opravdovému	opravdový	k2eAgNnSc3d1
porušení	porušení	k1gNnSc3
smlouvy	smlouva	k1gFnSc2
však	však	k9
došlo	dojít	k5eAaPmAgNnS
teprve	teprve	k6eAd1
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1
mocenské	mocenský	k2eAgInPc1d1
boje	boj	k1gInPc1
<g/>
,	,	kIx,
útok	útok	k1gInSc1
Hunů	Hun	k1gMnPc2
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
Honoria	Honorium	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
423	#num#	k4
vládl	vládnout	k5eAaImAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
423	#num#	k4
až	až	k9
425	#num#	k4
uzurpátor	uzurpátor	k1gMnSc1
Johannes	Johannes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
425	#num#	k4
byl	být	k5eAaImAgInS
poražen	porazit	k5eAaPmNgInS
a	a	k8xC
císařem	císař	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Valentinianus	Valentinianus	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
synovec	synovec	k1gMnSc1
Honoria	Honorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
posledním	poslední	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
theodosiánské	theodosiánský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
byl	být	k5eAaImAgInS
zatím	zatím	k6eAd1
v	v	k7c6
dětském	dětský	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
vládní	vládní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
na	na	k7c6
starosti	starost	k1gFnSc6
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
Galla	Gallo	k1gNnSc2
Placidia	Placidium	k1gNnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
brzo	brzo	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
bojům	boj	k1gInPc3
mezi	mezi	k7c7
různými	různý	k2eAgMnPc7d1
veliteli	velitel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitel	velitel	k1gMnSc1
vojska	vojsko	k1gNnSc2
Flavius	Flavius	k1gMnSc1
Felix	Felix	k1gMnSc1
a	a	k8xC
comes	comes	k1gMnSc1
Africae	Africa	k1gFnSc2
Bonifatius	Bonifatius	k1gInSc1
podporovali	podporovat	k5eAaImAgMnP
až	až	k6eAd1
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
více	hodně	k6eAd2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
méně	málo	k6eAd2
<g/>
)	)	kIx)
politiku	politika	k1gFnSc4
Gally	Galla	k1gFnSc2
Placidie	Placidie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
Bonifatia	Bonifatium	k1gNnSc2
však	však	k8xC
říši	říše	k1gFnSc4
vládl	vládnout	k5eAaImAgInS
patricius	patricius	k1gMnSc1
a	a	k8xC
velitel	velitel	k1gMnSc1
vojska	vojsko	k1gNnSc2
Flavius	Flavius	k1gMnSc1
Aetius	Aetius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valentinianus	Valentinianus	k1gInSc1
sídlil	sídlit	k5eAaImAgInS
střídavě	střídavě	k6eAd1
v	v	k7c6
Ravenně	Ravenně	k1gFnSc6
a	a	k8xC
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
dobu	doba	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
ztráta	ztráta	k1gFnSc1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
severozápadní	severozápadní	k2eAgFnSc1d1
Hispánie	Hispánie	k1gFnSc1
připadla	připadnout	k5eAaPmAgFnS
Svévům	Svév	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Galii	galium	k1gNnPc7
se	se	k3xPyFc4
Aetiovi	Aetius	k1gMnSc3
i	i	k9
přes	přes	k7c4
tlak	tlak	k1gInSc4
Vizigótů	Vizigót	k1gMnPc2
a	a	k8xC
Burgundů	Burgund	k1gMnPc2
podařilo	podařit	k5eAaPmAgNnS
uhájit	uhájit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Burgundská	burgundský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
na	na	k7c6
Rýně	Rýn	k1gInSc6
byla	být	k5eAaImAgNnP
zničena	zničit	k5eAaPmNgNnP
v	v	k7c6
roce	rok	k1gInSc6
436	#num#	k4
Aetiovými	Aetiový	k2eAgInPc7d1
hunskými	hunský	k2eAgInPc7d1
pomocnými	pomocný	k2eAgInPc7d1
sbory	sbor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c6
roce	rok	k1gInSc6
451	#num#	k4
podařilo	podařit	k5eAaPmAgNnS
zastavit	zastavit	k5eAaPmF
hunského	hunský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Attilu	Attila	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vpadl	vpadnout	k5eAaPmAgMnS
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
do	do	k7c2
Galie	Galie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
bitvě	bitva	k1gFnSc6
na	na	k7c6
Katalaunských	Katalaunský	k2eAgNnPc6d1
polích	pole	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
454	#num#	k4
však	však	k9
Valentinianus	Valentinianus	k1gInSc1
nechal	nechat	k5eAaPmAgInS
Aetia	Aetius	k1gMnSc4
zavraždit	zavraždit	k5eAaPmF
<g/>
,	,	kIx,
jen	jen	k9
aby	aby	kYmCp3nS
se	se	k3xPyFc4
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
stal	stát	k5eAaPmAgInS
sám	sám	k3xTgInSc1
obětí	oběť	k1gFnSc7
úkladné	úkladný	k2eAgFnSc2d1
vraždy	vražda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Dalmácii	Dalmácie	k1gFnSc6
si	se	k3xPyFc3
zatím	zatím	k6eAd1
generál	generál	k1gMnSc1
Marcellinus	Marcellinus	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
fakticky	fakticky	k6eAd1
samostatnou	samostatný	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc1d1
význam	význam	k1gInSc1
náleží	náležet	k5eAaImIp3nS
definitivní	definitivní	k2eAgFnSc3d1
ztrátě	ztráta	k1gFnSc3
provincie	provincie	k1gFnSc2
Afriky	Afrika	k1gFnSc2
dobytím	dobytí	k1gNnSc7
Kartága	Kartágo	k1gNnSc2
a	a	k8xC
získáním	získání	k1gNnSc7
tam	tam	k6eAd1
ukotvené	ukotvený	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
439	#num#	k4
Vandaly	Vandal	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
Germánů	Germán	k1gMnPc2
<g/>
)	)	kIx)
odmítli	odmítnout	k5eAaPmAgMnP
formální	formální	k2eAgFnSc4d1
podřízenost	podřízenost	k1gFnSc4
císaři	císař	k1gMnSc3
a	a	k8xC
založili	založit	k5eAaPmAgMnP
tak	tak	k8xS,k8xC
první	první	k4xOgInSc1
nezávislý	závislý	k2eNgInSc1d1
stát	stát	k1gInSc1
na	na	k7c6
římském	římský	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
Vizigóti	Vizigót	k1gMnPc1
<g/>
,	,	kIx,
Svébové	Svéb	k1gMnPc1
<g/>
,	,	kIx,
Burgundi	Burgund	k1gMnPc1
a	a	k8xC
Frankové	Frank	k1gMnPc1
byli	být	k5eAaImAgMnP
federáti	federát	k1gMnPc1
a	a	k8xC
vládli	vládnout	k5eAaImAgMnP
provinciálnímu	provinciální	k2eAgNnSc3d1
římskému	římský	k2eAgNnSc3d1
obyvatelstvu	obyvatelstvo	k1gNnSc3
jménem	jméno	k1gNnSc7
císaře	císař	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
zachovávalo	zachovávat	k5eAaImAgNnS
zdání	zdání	k1gNnSc1
pokračování	pokračování	k1gNnSc2
římské	římský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
a	a	k8xC
také	také	k9
zvyšovalo	zvyšovat	k5eAaImAgNnS
možnost	možnost	k1gFnSc4
politických	politický	k2eAgFnPc2d1
a	a	k8xC
vojenských	vojenský	k2eAgFnPc2d1
intervencí	intervence	k1gFnPc2
Ravenny	Ravenna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
stínových	stínový	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
</s>
<s>
Solidus	solidus	k1gInSc1
vyražený	vyražený	k2eAgInSc1d1
v	v	k7c6
Soluni	Soluň	k1gFnSc6
na	na	k7c4
oslavu	oslava	k1gFnSc4
svatby	svatba	k1gFnSc2
Valentiniana	Valentinian	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
s	s	k7c7
dcerou	dcera	k1gFnSc7
Theodosia	Theodosium	k1gNnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Licinií	Licinie	k1gFnSc7
Eudokiíi	Eudokiíi	k1gNnSc2
</s>
<s>
Stínoví	stínový	k2eAgMnPc1d1
císaři	císař	k1gMnPc1
panovali	panovat	k5eAaImAgMnP
obvykle	obvykle	k6eAd1
jen	jen	k9
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
a	a	k8xC
sotva	sotva	k6eAd1
ještě	ještě	k6eAd1
dokázali	dokázat	k5eAaPmAgMnP
aktivně	aktivně	k6eAd1
čelit	čelit	k5eAaImF
blížící	blížící	k2eAgFnSc3d1
se	se	k3xPyFc4
katastrofě	katastrofa	k1gFnSc3
západořímského	západořímský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západ	západ	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
území	území	k1gNnPc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Germánů	Germán	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
vytvářeli	vytvářet	k5eAaImAgMnP
státy	stát	k1gInPc4
ve	v	k7c6
státě	stát	k1gInSc6
a	a	k8xC
tím	ten	k3xDgNnSc7
Západořímskou	západořímský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
zbavovali	zbavovat	k5eAaImAgMnP
důležitých	důležitý	k2eAgInPc2d1
daňových	daňový	k2eAgInPc2d1
příjmů	příjem	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgFnP
nutné	nutný	k2eAgInPc1d1
k	k	k7c3
udržování	udržování	k1gNnSc3
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
z	z	k7c2
tzv.	tzv.	kA
stínových	stínový	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
se	se	k3xPyFc4
po	po	k7c6
Valentinianově	Valentinianův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
stal	stát	k5eAaPmAgInS
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
455	#num#	k4
senátor	senátor	k1gMnSc1
<g/>
,	,	kIx,
Petronius	Petronius	k1gMnSc1
Maximus	Maximus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
jeho	jeho	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
jen	jen	k9
krátce	krátce	k6eAd1
a	a	k8xC
skončila	skončit	k5eAaPmAgFnS
násilně	násilně	k6eAd1
během	během	k7c2
dobytí	dobytí	k1gNnSc2
Říma	Řím	k1gInSc2
Vandaly	Vandal	k1gMnPc4
pod	pod	k7c4
vedením	vedení	k1gNnSc7
krále	král	k1gMnSc2
Geisericha	Geiserich	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
něm	on	k3xPp3gMnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
císařský	císařský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
Galoromán	Galoromán	k2eAgMnSc1d1
Eparchius	Eparchius	k1gMnSc1
Avitus	Avitus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předtím	předtím	k6eAd1
působil	působit	k5eAaImAgMnS
jako	jako	k9
vyslanec	vyslanec	k1gMnSc1
mezi	mezi	k7c7
Římany	Říman	k1gMnPc7
a	a	k8xC
Vizigóty	Vizigót	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvé	prvý	k4xOgFnSc6
řadě	řada	k1gFnSc6
však	však	k9
musel	muset	k5eAaImAgMnS
uklidnit	uklidnit	k5eAaPmF
vztahy	vztah	k1gInPc4
s	s	k7c7
Marcianem	Marcian	k1gInSc7
<g/>
,	,	kIx,
východořímským	východořímský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
udržet	udržet	k5eAaPmF
v	v	k7c6
klidu	klid	k1gInSc6
velitele	velitel	k1gMnSc2
vojska	vojsko	k1gNnSc2
Ricimera	Ricimer	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
stal	stát	k5eAaPmAgInS
skutečným	skutečný	k2eAgMnSc7d1
vládcem	vládce	k1gMnSc7
Západu	západ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Avitovi	Avit	k1gMnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
dubnu	duben	k1gInSc6
457	#num#	k4
na	na	k7c4
trůn	trůn	k1gInSc4
Maiorianus	Maiorianus	k1gMnSc1
a	a	k8xC
zastával	zastávat	k5eAaImAgMnS
jej	on	k3xPp3gInSc4
pak	pak	k6eAd1
po	po	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
letech	let	k1gInPc6
porážek	porážka	k1gFnPc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
konečně	konečně	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Itálií	Itálie	k1gFnSc7
a	a	k8xC
velkou	velký	k2eAgFnSc7d1
částí	část	k1gFnSc7
Galie	Galie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vizigóti	Vizigót	k1gMnPc1
a	a	k8xC
Burgundi	Burgund	k1gMnPc1
byli	být	k5eAaImAgMnP
přinejmenším	přinejmenším	k6eAd1
dočasně	dočasně	k6eAd1
zpacifikováni	zpacifikovat	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
v	v	k7c6
části	část	k1gFnSc6
Hispánie	Hispánie	k1gFnSc2
byl	být	k5eAaImAgMnS
nastolen	nastolen	k2eAgInSc4d1
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vnitřní	vnitřní	k2eAgFnSc6d1
politice	politika	k1gFnSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
naklonit	naklonit	k5eAaPmF
si	se	k3xPyFc3
senát	senát	k1gInSc4
ústupky	ústupek	k1gInPc4
a	a	k8xC
lid	lid	k1gInSc1
zlepšenou	zlepšená	k1gFnSc4
hospodářskou	hospodářský	k2eAgFnSc7d1
situací	situace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
ale	ale	k9
selhala	selhat	k5eAaPmAgFnS
trestná	trestný	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
proti	proti	k7c3
Vandalům	Vandal	k1gMnPc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
na	na	k7c6
začátku	začátek	k1gInSc6
srpna	srpen	k1gInSc2
461	#num#	k4
Ricimerem	Ricimer	k1gMnSc7
svržen	svrhnout	k5eAaPmNgMnS
a	a	k8xC
krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ricimer	Ricimer	k1gMnSc1
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
však	však	k9
jako	jako	k9
arián	arián	k1gMnSc1
a	a	k8xC
Germán	Germán	k1gMnSc1
nemohl	moct	k5eNaImAgMnS
stát	stát	k5eAaPmF,k5eAaImF
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Místo	místo	k7c2
něho	on	k3xPp3gNnSc2
se	se	k3xPyFc4
bez	bez	k7c2
pomazání	pomazání	k1gNnSc2
papeže	papež	k1gMnSc2
stal	stát	k5eAaPmAgMnS
císařem	císař	k1gMnSc7
Západořímské	západořímský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
Libius	Libius	k1gMnSc1
Severus	Severus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
Vandalové	Vandal	k1gMnPc1
obnovili	obnovit	k5eAaPmAgMnP
drancování	drancování	k1gNnSc4
západního	západní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Galii	Galie	k1gFnSc6
mezitím	mezitím	k6eAd1
proti	proti	k7c3
vládě	vláda	k1gFnSc3
v	v	k7c6
Ravenně	Ravenně	k1gFnSc6
povstal	povstat	k5eAaPmAgMnS
velitel	velitel	k1gMnSc1
vojska	vojsko	k1gNnSc2
a	a	k8xC
Maiorianův	Maiorianův	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
Aegidius	Aegidius	k1gMnSc1
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
galorománskou	galorománský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
na	na	k7c6
severu	sever	k1gInSc6
země	zem	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
přetrvala	přetrvat	k5eAaPmAgFnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
486	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
čtyřech	čtyři	k4xCgInPc6
letech	léto	k1gNnPc6
vlády	vláda	k1gFnSc2
(	(	kIx(
<g/>
465	#num#	k4
<g/>
)	)	kIx)
Libius	Libius	k1gMnSc1
Severus	Severus	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
(	(	kIx(
<g/>
údajně	údajně	k6eAd1
byl	být	k5eAaImAgInS
otráven	otrávit	k5eAaPmNgInS
Ricimerem	Ricimer	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
nastoupil	nastoupit	k5eAaPmAgMnS
východní	východní	k2eAgMnSc1d1
Říman	Říman	k1gMnSc1
Procopius	Procopius	k1gMnSc1
Anthemius	Anthemius	k1gMnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
v	v	k7c6
letech	léto	k1gNnPc6
465-467	465-467	k4
nastalo	nastat	k5eAaPmAgNnS
krátké	krátký	k2eAgNnSc1d1
Interregnum	interregnum	k1gNnSc1
(	(	kIx(
<g/>
mezivládí	mezivládí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anthemius	Anthemius	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
obnovit	obnovit	k5eAaPmF
vládu	vláda	k1gFnSc4
a	a	k8xC
vystupoval	vystupovat	k5eAaImAgMnS
proti	proti	k7c3
Vandalům	Vandal	k1gMnPc3
a	a	k8xC
především	především	k9
proti	proti	k7c3
Vizigótům	Vizigót	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Euricha	Eurich	k1gMnSc2
porušili	porušit	k5eAaPmAgMnP
smlouvu	smlouva	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
418	#num#	k4
a	a	k8xC
zahájili	zahájit	k5eAaPmAgMnP
systematické	systematický	k2eAgNnSc4d1
dobývání	dobývání	k1gNnSc4
Hispánie	Hispánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flotila	flotila	k1gFnSc1
generála	generál	k1gMnSc2
Basiliska	Basiliska	k1gFnSc1
<g/>
,	,	kIx,
vyslaná	vyslaný	k2eAgFnSc1d1
proti	proti	k7c3
Vandalům	Vandal	k1gMnPc3
<g/>
,	,	kIx,
zpočátku	zpočátku	k6eAd1
dosáhla	dosáhnout	k5eAaPmAgFnS
velkých	velká	k1gFnPc2
úspěchů	úspěch	k1gInPc2
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
ale	ale	k8xC
Vandalové	Vandal	k1gMnPc1
Basiliska	Basilisko	k1gNnSc2
porazili	porazit	k5eAaPmAgMnP
a	a	k8xC
vytlačili	vytlačit	k5eAaPmAgMnP
z	z	k7c2
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
finančního	finanční	k2eAgNnSc2d1
a	a	k8xC
vojenského	vojenský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
naprostá	naprostý	k2eAgFnSc1d1
katastrofa	katastrofa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
tažení	tažení	k1gNnSc1
proti	proti	k7c3
Vizigótům	Vizigót	k1gMnPc3
zůstalo	zůstat	k5eAaPmAgNnS
neúspěšné	úspěšný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konflikt	konflikt	k1gInSc1
mezi	mezi	k7c7
Anthemiem	Anthemium	k1gNnSc7
a	a	k8xC
Ricimerem	Ricimero	k1gNnSc7
vyústil	vyústit	k5eAaPmAgInS
nakonec	nakonec	k6eAd1
v	v	k7c4
občanskou	občanský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
z	z	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
vyšel	vyjít	k5eAaPmAgInS
vítězně	vítězně	k6eAd1
dodnes	dodnes	k6eAd1
blíže	blízce	k6eAd2
neznámý	známý	k2eNgMnSc1d1
Anicius	Anicius	k1gMnSc1
Olybrius	Olybrius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
Západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
a	a	k8xC
Syagriova	Syagriův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
(	(	kIx(
<g/>
modře	modro	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
476	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Olybriovi	Olybriův	k2eAgMnPc1d1
však	však	k9
nebyl	být	k5eNaImAgInS
dopřán	dopřát	k5eAaPmNgInS
ani	ani	k8xC
rok	rok	k1gInSc1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
trůn	trůn	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
dubnu	duben	k1gInSc6
nebo	nebo	k8xC
květnu	květen	k1gInSc6
472	#num#	k4
a	a	k8xC
již	již	k6eAd1
o	o	k7c4
sedm	sedm	k4xCc4
měsíců	měsíc	k1gInPc2
později	pozdě	k6eAd2
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sotva	sotva	k6eAd1
úspěšnější	úspěšný	k2eAgFnSc1d2
byla	být	k5eAaImAgFnS
vláda	vláda	k1gFnSc1
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
nástupce	nástupce	k1gMnSc2
Glyceria	Glycerium	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vládl	vládnout	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
473	#num#	k4
až	až	k9
474	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
byl	být	k5eAaImAgInS
diplomaticky	diplomaticky	k6eAd1
i	i	k9
vojensky	vojensky	k6eAd1
talentovaný	talentovaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
se	se	k3xPyFc4
podrobit	podrobit	k5eAaPmF
Ostrogótům	Ostrogót	k1gMnPc3
a	a	k8xC
nakonec	nakonec	k6eAd1
také	také	k9
veliteli	velitel	k1gMnSc3
vojska	vojsko	k1gNnSc2
Juliu	Julius	k1gMnSc3
Nepotovi	Nepot	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Julius	Julius	k1gMnSc1
Nepos	Nepos	k1gMnSc1
vládl	vládnout	k5eAaImAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
474	#num#	k4
až	až	k9
475	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konflikt	konflikt	k1gInSc1
s	s	k7c7
Vizigóty	Vizigót	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
dobyli	dobýt	k5eAaPmAgMnP
Provence	Provence	k1gFnPc4
<g/>
,	,	kIx,
chtěl	chtít	k5eAaImAgMnS
řešit	řešit	k5eAaImF
především	především	k9
diplomatickou	diplomatický	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
Vandalové	Vandal	k1gMnPc1
získali	získat	k5eAaPmAgMnP
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
celým	celý	k2eAgNnSc7d1
západním	západní	k2eAgNnSc7d1
Středomořím	středomoří	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
pro	pro	k7c4
něho	on	k3xPp3gMnSc4
zhoršila	zhoršit	k5eAaPmAgFnS
situace	situace	k1gFnSc1
také	také	k9
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
:	:	kIx,
Jeho	jeho	k3xOp3gMnSc1
patron	patron	k1gMnSc1
Orestes	Orestes	k1gMnSc1
(	(	kIx(
<g/>
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
sekretářem	sekretář	k1gMnSc7
hunského	hunský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Attily	Attila	k1gMnSc2
<g/>
)	)	kIx)
převzal	převzít	k5eAaPmAgInS
velení	velení	k1gNnSc4
vojska	vojsko	k1gNnSc2
a	a	k8xC
vyhnal	vyhnat	k5eAaPmAgMnS
Nepota	Nepot	k1gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
475	#num#	k4
z	z	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Romulus	Romulus	k1gMnSc1
Augustulus	Augustulus	k1gMnSc1
byl	být	k5eAaImAgMnS
posledním	poslední	k2eAgMnSc7d1
západořímským	západořímský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
(	(	kIx(
<g/>
ačkoliv	ačkoliv	k8xS
Nepos	Nepos	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
480	#num#	k4
posledním	poslední	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
,	,	kIx,
uznaným	uznaný	k2eAgInSc7d1
Východořímskou	východořímský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nástupu	nástup	k1gInSc6
na	na	k7c4
trůn	trůn	k1gInSc4
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
teprve	teprve	k6eAd1
10	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
476	#num#	k4
byl	být	k5eAaImAgMnS
konečně	konečně	k6eAd1
sesazen	sesadit	k5eAaPmNgMnS
germánským	germánský	k2eAgMnSc7d1
vojevůdcem	vojevůdce	k1gMnSc7
Odoakerem	Odoaker	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
předchůdce	předchůdce	k1gMnSc2
Ricimera	Ricimer	k1gMnSc2
nedosadil	dosadit	k5eNaPmAgInS
Odoaker	Odoaker	k1gInSc1
žádného	žádný	k3yNgMnSc4
loutkového	loutkový	k2eAgMnSc4d1
císaře	císař	k1gMnSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
poslal	poslat	k5eAaPmAgMnS
senátorskou	senátorský	k2eAgFnSc4d1
delegaci	delegace	k1gFnSc4
k	k	k7c3
císaři	císař	k1gMnSc3
Zenonovi	Zenon	k1gMnSc3
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
tam	tam	k6eAd1
vyhlásit	vyhlásit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jeden	jeden	k4xCgMnSc1
císař	císař	k1gMnSc1
je	být	k5eAaImIp3nS
dostačující	dostačující	k2eAgNnSc4d1
pro	pro	k7c4
obě	dva	k4xCgFnPc4
poloviny	polovina	k1gFnPc4
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odoaker	Odoaker	k1gInSc1
chápal	chápat	k5eAaImAgInS
svoji	svůj	k3xOyFgFnSc4
vládu	vláda	k1gFnSc4
pravděpodobně	pravděpodobně	k6eAd1
jako	jako	k9
pokračování	pokračování	k1gNnSc4
římské	římský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
a	a	k8xC
od	od	k7c2
Zenona	Zenon	k1gMnSc2
obdržel	obdržet	k5eAaPmAgMnS
nakonec	nakonec	k6eAd1
také	také	k9
oficiální	oficiální	k2eAgInSc4d1
titul	titul	k1gInSc4
patricia	patricius	k1gMnSc2
a	a	k8xC
císařského	císařský	k2eAgMnSc2d1
místodržitele	místodržitel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
tím	ten	k3xDgNnSc7
ale	ale	k9
zanikla	zaniknout	k5eAaPmAgNnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
konstatoval	konstatovat	k5eAaBmAgMnS
i	i	k9
comes	comes	k1gMnSc1
Marcellinus	Marcellinus	k1gMnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
ovšem	ovšem	k9
aktu	akt	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
476	#num#	k4
sotva	sotva	k6eAd1
dostalo	dostat	k5eAaPmAgNnS
zvláštní	zvláštní	k2eAgFnSc3d1
pozornosti	pozornost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatně	ostatně	k6eAd1
v	v	k7c6
Konstantinopoli	Konstantinopol	k1gInSc6
byl	být	k5eAaImAgInS
ještě	ještě	k6eAd1
jeden	jeden	k4xCgMnSc1
císař	císař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
k	k	k7c3
faktickému	faktický	k2eAgInSc3d1
rozpadu	rozpad	k1gInSc3
západní	západní	k2eAgFnSc2d1
říše	říš	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
již	již	k6eAd1
dříve	dříve	k6eAd2
ztrátou	ztráta	k1gFnSc7
většiny	většina	k1gFnSc2
původního	původní	k2eAgNnSc2d1
území	území	k1gNnSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Germánů	Germán	k1gMnPc2
a	a	k8xC
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
letech	let	k1gInPc6
její	její	k3xOp3gFnSc2
existence	existence	k1gFnSc2
se	se	k3xPyFc4
státní	státní	k2eAgFnSc1d1
moc	moc	k1gFnSc1
omezovala	omezovat	k5eAaImAgFnS
v	v	k7c6
podstatě	podstata	k1gFnSc6
už	už	k6eAd1
jen	jen	k9
na	na	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tremissis	Tremissis	k1gInSc1
s	s	k7c7
portrétem	portrét	k1gInSc7
Julia	Julius	k1gMnSc2
Nepota	Nepot	k1gMnSc2
</s>
<s>
Mimoto	mimoto	k6eAd1
nechal	nechat	k5eAaPmAgMnS
Odoaker	Odoaker	k1gInSc4
nadále	nadále	k6eAd1
razit	razit	k5eAaImF
mince	mince	k1gFnPc4
se	s	k7c7
jménem	jméno	k1gNnSc7
císaře	císař	k1gMnSc2
Julia	Julius	k1gMnSc2
Nepota	Nepot	k1gMnSc2
(	(	kIx(
<g/>
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
480	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
stále	stále	k6eAd1
ještě	ještě	k6eAd1
vládl	vládnout	k5eAaImAgInS
v	v	k7c6
Dalmácii	Dalmácie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
Galii	Galie	k1gFnSc6
<g/>
,	,	kIx,
vklíněn	vklíněn	k2eAgInSc4d1
mezi	mezi	k7c7
Vizigóty	Vizigót	k1gMnPc7
a	a	k8xC
Franky	Frank	k1gMnPc7
a	a	k8xC
odříznut	odříznout	k5eAaPmNgMnS
od	od	k7c2
zbytku	zbytek	k1gInSc2
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
dokázal	dokázat	k5eAaPmAgMnS
udržet	udržet	k5eAaPmF
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
486	#num#	k4
římský	římský	k2eAgMnSc1d1
místodržící	místodržící	k1gMnSc1
Syagrius	Syagrius	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
výše	výše	k1gFnSc2,k1gFnSc2wB
zmíněného	zmíněný	k2eAgNnSc2d1
Aegidia	Aegidium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
území	území	k1gNnSc2
Franky	Franky	k1gInPc1
byl	být	k5eAaImAgInS
jimi	on	k3xPp3gMnPc7
popraven	popraven	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
části	část	k1gFnPc1
někdejší	někdejší	k2eAgFnSc2d1
západořímské	západořímský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
byly	být	k5eAaImAgFnP
na	na	k7c6
konci	konec	k1gInSc6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ovládány	ovládán	k2eAgMnPc4d1
Vandaly	Vandal	k1gMnPc4
a	a	k8xC
Vizigóty	Vizigót	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apeninský	apeninský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
setrvával	setrvávat	k5eAaImAgInS
pod	pod	k7c7
vládou	vláda	k1gFnSc7
Odoakera	Odoakero	k1gNnSc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
489	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
sem	sem	k6eAd1
vpadl	vpadnout	k5eAaPmAgMnS
z	z	k7c2
podnětu	podnět	k1gInSc2
Zenona	Zenona	k1gFnSc1
Theodorich	Theodorich	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
<g/>
,	,	kIx,
náčelník	náčelník	k1gInSc1
Ostrogótů	Ostrogót	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
De	De	k?
iure	iur	k1gFnSc2
podléhala	podléhat	k5eAaImAgFnS
západní	západní	k2eAgNnSc4d1
území	území	k1gNnSc4
nyní	nyní	k6eAd1
svrchovanosti	svrchovanost	k1gFnSc2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
východořímského	východořímský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Justiniana	Justinian	k1gMnSc2
I.	I.	kA
byly	být	k5eAaImAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
533-553	533-553	k4
ještě	ještě	k9
jednou	jednou	k6eAd1
vojensky	vojensky	k6eAd1
podrobeny	podroben	k2eAgFnPc1d1
velké	velký	k2eAgFnPc1d1
části	část	k1gFnPc1
dřívější	dřívější	k2eAgFnSc2d1
západořímské	západořímský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
severní	severní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
jižní	jižní	k2eAgNnSc1d1
Španělsko	Španělsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
už	už	k9
pouze	pouze	k6eAd1
o	o	k7c4
poslední	poslední	k2eAgFnSc4d1
krátkou	krátký	k2eAgFnSc4d1
epizodu	epizoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Důsledky	důsledek	k1gInPc1
pro	pro	k7c4
město	město	k1gNnSc4
Řím	Řím	k1gInSc1
</s>
<s>
Zánik	zánik	k1gInSc1
západořímské	západořímský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
byl	být	k5eAaImAgInS
patrný	patrný	k2eAgInSc1d1
i	i	k9
ve	v	k7c6
městě	město	k1gNnSc6
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
kolem	kolem	k7c2
roku	rok	k1gInSc2
250	#num#	k4
n.	n.	k?
l.	l.	k?
žilo	žít	k5eAaImAgNnS
na	na	k7c4
1	#num#	k4
milion	milion	k4xCgInSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
po	po	k7c6
ztrátě	ztráta	k1gFnSc6
postavení	postavení	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
říše	říše	k1gFnSc1
zmenšilo	zmenšit	k5eAaPmAgNnS
asi	asi	k9
na	na	k7c4
400	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
okolo	okolo	k7c2
roku	rok	k1gInSc2
400	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoutýdenní	dvoutýdenní	k2eAgInSc4d1
drancování	drancování	k1gNnSc1
Vandaly	Vandal	k1gMnPc7
v	v	k7c6
roce	rok	k1gInSc6
455	#num#	k4
zmenšilo	zmenšit	k5eAaPmAgNnS
bohatství	bohatství	k1gNnSc1
města	město	k1gNnSc2
a	a	k8xC
mor	mora	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
472	#num#	k4
zdecimoval	zdecimovat	k5eAaPmAgInS
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
také	také	k9
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
Řím	Řím	k1gInSc1
současníky	současník	k1gMnPc4
považován	považován	k2eAgInSc1d1
za	za	k7c4
kulturní	kulturní	k2eAgFnSc4d1
a	a	k8xC
politicky	politicky	k6eAd1
významnou	významný	k2eAgFnSc4d1
metropoli	metropole	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
pobývali	pobývat	k5eAaImAgMnP
i	i	k9
přes	přes	k7c4
přeložení	přeložení	k1gNnSc4
rezidence	rezidence	k1gFnSc2
do	do	k7c2
Ravenny	Ravenna	k1gFnSc2
častěji	často	k6eAd2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
439	#num#	k4
se	se	k3xPyFc4
město	město	k1gNnSc1
zvětšilo	zvětšit	k5eAaPmAgNnS
díky	díky	k7c3
přívalu	příval	k1gInSc2
uprchlíků	uprchlík	k1gMnPc2
z	z	k7c2
Galie	Galie	k1gFnSc2
a	a	k8xC
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
470	#num#	k4
je	být	k5eAaImIp3nS
Řím	Řím	k1gInSc1
popisován	popisovat	k5eAaImNgInS
jako	jako	k8xS,k8xC
významné	významný	k2eAgNnSc1d1
město	město	k1gNnSc1
s	s	k7c7
velkými	velký	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
a	a	k8xC
s	s	k7c7
rušnou	rušný	k2eAgFnSc7d1
divadelní	divadelní	k2eAgFnSc7d1
scénou	scéna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koloseum	Koloseum	k1gNnSc1
bylo	být	k5eAaImAgNnS
využíváno	využívat	k5eAaPmNgNnS,k5eAaImNgNnS
minimálně	minimálně	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
523	#num#	k4
<g/>
,	,	kIx,
velké	velký	k2eAgFnPc4d1
lázně	lázeň	k1gFnPc4
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
535	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
534	#num#	k4
mělo	mít	k5eAaImAgNnS
mít	mít	k5eAaImF
město	město	k1gNnSc1
stále	stále	k6eAd1
kolem	kolem	k7c2
100	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Válka	válka	k1gFnSc1
východořímského	východořímský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Justiniana	Justinian	k1gMnSc2
I.	I.	kA
proti	proti	k7c3
Ostrogótům	Ostrogót	k1gMnPc3
znamenala	znamenat	k5eAaImAgFnS
zlom	zlom	k1gInSc4
v	v	k7c6
dějinách	dějiny	k1gFnPc6
města	město	k1gNnSc2
<g/>
:	:	kIx,
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
535	#num#	k4
a	a	k8xC
549	#num#	k4
byl	být	k5eAaImAgInS
Řím	Řím	k1gInSc1
vícekrát	vícekrát	k6eAd1
obležen	oblehnout	k5eAaPmNgInS
<g/>
,	,	kIx,
životně	životně	k6eAd1
důležité	důležitý	k2eAgInPc1d1
akvadukty	akvadukt	k1gInPc1
zničeny	zničen	k2eAgInPc1d1
a	a	k8xC
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
senátorů	senátor	k1gMnPc2
deportována	deportován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmínky	zmínka	k1gFnSc2
o	o	k7c6
západořímském	západořímský	k2eAgInSc6d1
senátu	senát	k1gInSc6
zmizely	zmizet	k5eAaPmAgInP
brzo	brzo	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
(	(	kIx(
<g/>
kolem	kolem	k7c2
roku	rok	k1gInSc2
580	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
dosahoval	dosahovat	k5eAaImAgInS
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
stoletích	století	k1gNnPc6
nejvýše	nejvýše	k6eAd1,k6eAd3
20	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Foru	forum	k1gNnSc6
Romanu	Romana	k1gFnSc4
bylo	být	k5eAaImAgNnS
provozováno	provozován	k2eAgNnSc1d1
zemědělství	zemědělství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antické	antický	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
sloužily	sloužit	k5eAaImAgFnP
jako	jako	k9
kamenolom	kamenolom	k1gInSc4
nebo	nebo	k8xC
byly	být	k5eAaImAgInP
znehodnoceny	znehodnotit	k5eAaPmNgInP
různými	různý	k2eAgFnPc7d1
stavebními	stavební	k2eAgFnPc7d1
úpravami	úprava	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Římě	Řím	k1gInSc6
nastal	nastat	k5eAaPmAgInS
středověk	středověk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Bednaříková	Bednaříková	k1gFnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Stěhování	stěhování	k1gNnSc1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
COLLINS	COLLINS	kA
<g/>
,	,	kIx,
Roger	Roger	k1gInSc1
<g/>
:	:	kIx,
Evropa	Evropa	k1gFnSc1
raného	raný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
300	#num#	k4
<g/>
-	-	kIx~
<g/>
1000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
479	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
660	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ČEŠKA	Češka	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
Zánik	zánik	k1gInSc1
antického	antický	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7021-386-8	80-7021-386-8	k4
</s>
<s>
ČORNEJ	ČORNEJ	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
Dějepis	dějepis	k1gInSc1
2	#num#	k4
pro	pro	k7c4
gymnázia	gymnázium	k1gNnPc4
a	a	k8xC
střední	střední	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
SPN	SPN	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7235-152-4	80-7235-152-4	k4
</s>
<s>
Doležal	Doležal	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
<g/>
:	:	kIx,
Interakce	interakce	k1gFnSc1
Gótů	Gót	k1gMnPc2
a	a	k8xC
římského	římský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
ve	v	k7c6
3	#num#	k4
<g/>
.	.	kIx.
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2008	#num#	k4
</s>
<s>
Goldsworthy	Goldsworth	k1gInPc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
:	:	kIx,
Armáda	armáda	k1gFnSc1
starého	starý	k2eAgInSc2d1
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GRANT	grant	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
,	,	kIx,
Pád	Pád	k1gInSc1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86070-32-8	80-86070-32-8	k4
</s>
<s>
HEER	HEER	kA
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
<g/>
:	:	kIx,
Evropské	evropský	k2eAgFnPc1d1
duchovní	duchovní	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
2000	#num#	k4
</s>
<s>
Todd	Todd	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
:	:	kIx,
Germáni	Germán	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NLN	NLN	kA
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
psané	psaný	k2eAgNnSc1d1
Římem	Řím	k1gInSc7
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Perfekt	perfektum	k1gNnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-8046-297-6	80-8046-297-6	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
římských	římský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
</s>
<s>
Starověký	starověký	k2eAgInSc1d1
Řím	Řím	k1gInSc1
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Pád	Pád	k1gInSc1
Západořímské	západořímský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Západořímská	západořímský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Západořímské	západořímský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Pád	Pád	k1gInSc1
Západořímské	západořímský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
stránky	stránka	k1gFnPc4
Antika	antika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4107101-3	4107101-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85115160	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
262852029	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85115160	#num#	k4
</s>
