<s>
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
jsou	být	k5eAaImIp3nP	být
známí	známý	k1gMnPc1	známý
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
filosofickým	filosofický	k2eAgInPc3d1	filosofický
textům	text	k1gInPc3	text
<g/>
,	,	kIx,	,
klasickým	klasický	k2eAgFnPc3d1	klasická
rockovým	rockový	k2eAgFnPc3d1	rocková
melodiím	melodie	k1gFnPc3	melodie
<g/>
,	,	kIx,	,
zvukovým	zvukový	k2eAgFnPc3d1	zvuková
experimentům	experiment	k1gInPc3	experiment
<g/>
,	,	kIx,	,
inovativním	inovativní	k2eAgInPc3d1	inovativní
obalům	obal	k1gInPc3	obal
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
propracovaným	propracovaný	k2eAgNnSc7d1	propracované
vystoupením	vystoupení	k1gNnSc7	vystoupení
<g/>
.	.	kIx.	.
</s>
