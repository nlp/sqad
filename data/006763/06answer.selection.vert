<s>
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
zvažovali	zvažovat	k5eAaImAgMnP	zvažovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
operaci	operace	k1gFnSc4	operace
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1940	[number]	k4	1940
podepsána	podepsán	k2eAgFnSc1d1	podepsána
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
muselo	muset	k5eAaImAgNnS	muset
Finsko	Finsko	k1gNnSc4	Finsko
odstoupit	odstoupit	k5eAaPmF	odstoupit
Sovětům	Sovět	k1gMnPc3	Sovět
část	část	k1gFnSc4	část
Karélie	Karélie	k1gFnSc2	Karélie
<g/>
.	.	kIx.	.
</s>
