<s>
Jako	jako	k8xC	jako
konec	konec	k1gInSc1	konec
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
udáván	udáván	k2eAgMnSc1d1	udáván
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
oslavován	oslavován	k2eAgMnSc1d1	oslavován
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
od	od	k7c2	od
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
zavládlo	zavládnout	k5eAaPmAgNnS	zavládnout
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
frontách	fronta	k1gFnPc6	fronta
příměří	příměří	k1gNnSc2	příměří
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
v	v	k7c4	v
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podepsané	podepsaný	k2eAgFnSc3d1	podepsaná
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
v	v	k7c4	v
5.05	[number]	k4	5.05
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
německou	německý	k2eAgFnSc7d1	německá
generalitou	generalita	k1gFnSc7	generalita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
štábním	štábní	k2eAgInSc6d1	štábní
vagóně	vagón	k1gInSc6	vagón
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
dohodových	dohodový	k2eAgFnPc2d1	dohodová
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgMnSc4d1	francouzský
maršála	maršál	k1gMnSc4	maršál
Foche	Foch	k1gFnSc2	Foch
v	v	k7c6	v
Compiè	Compiè	k1gFnSc6	Compiè
<g/>
.	.	kIx.	.
</s>
