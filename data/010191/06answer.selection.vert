<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Juděnič	Juděnič	k1gMnSc1	Juděnič
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Н	Н	k?	Н
Н	Н	k?	Н
Ю	Ю	k?	Ю
<g/>
;	;	kIx,	;
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
jul	jul	k?	jul
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1862	[number]	k4	1862
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
generálů	generál	k1gMnPc2	generál
carské	carský	k2eAgFnSc2d1	carská
armády	armáda	k1gFnSc2	armáda
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
