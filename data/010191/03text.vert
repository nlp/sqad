<p>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Juděnič	Juděnič	k1gMnSc1	Juděnič
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Н	Н	k?	Н
Н	Н	k?	Н
Ю	Ю	k?	Ю
<g/>
;	;	kIx,	;
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
jul	jul	k?	jul
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1862	[number]	k4	1862
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
generálů	generál	k1gMnPc2	generál
carské	carský	k2eAgFnSc2d1	carská
armády	armáda	k1gFnSc2	armáda
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918-1920	[number]	k4	1918-1920
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Judenič	Judeničit	k5eAaPmRp2nS	Judeničit
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
jako	jako	k8xC	jako
úředník	úředník	k1gMnSc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
Alexandrovskou	Alexandrovský	k2eAgFnSc4d1	Alexandrovská
vysokou	vysoký	k2eAgFnSc4d1	vysoká
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
Akademii	akademie	k1gFnSc4	akademie
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
imperiální	imperiální	k2eAgFnSc2d1	imperiální
gardy	garda	k1gFnSc2	garda
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1892	[number]	k4	1892
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
do	do	k7c2	do
Turkestánu	Turkestán	k1gInSc2	Turkestán
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
poručíka	poručík	k1gMnSc4	poručík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
byl	být	k5eAaImAgMnS	být
Judenič	Judenič	k1gMnSc1	Judenič
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
18	[number]	k4	18
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
bojoval	bojovat	k5eAaImAgMnS	bojovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
raněn	ranit	k5eAaPmNgInS	ranit
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
konci	konec	k1gInSc6	konec
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
generála	generál	k1gMnSc4	generál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
sloužil	sloužit	k5eAaImAgMnS	sloužit
Judenič	Judenič	k1gMnSc1	Judenič
jako	jako	k8xC	jako
proviantní	proviantní	k2eAgMnSc1d1	proviantní
důstojník	důstojník	k1gMnSc1	důstojník
na	na	k7c6	na
generálním	generální	k2eAgInSc6d1	generální
štábu	štáb	k1gInSc6	štáb
kavkazského	kavkazský	k2eAgInSc2d1	kavkazský
vojenského	vojenský	k2eAgInSc2d1	vojenský
okruhu	okruh	k1gInSc2	okruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
velitelem	velitel	k1gMnSc7	velitel
ruských	ruský	k2eAgMnPc6d1	ruský
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
několik	několik	k4yIc4	několik
obdivuhodných	obdivuhodný	k2eAgNnPc2d1	obdivuhodné
vítězství	vítězství	k1gNnPc2	vítězství
nad	nad	k7c4	nad
vojsky	vojsky	k6eAd1	vojsky
tureckého	turecký	k2eAgMnSc2d1	turecký
velitele	velitel	k1gMnSc2	velitel
Envera	Enver	k1gMnSc2	Enver
Paši	paša	k1gMnSc2	paša
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Sarikamiše	Sarikamiš	k1gInSc2	Sarikamiš
<g/>
,	,	kIx,	,
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Erzurum	Erzurum	k1gInSc4	Erzurum
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgMnS	dobýt
město	město	k1gNnSc4	město
Trabzon	Trabzon	k1gInSc1	Trabzon
a	a	k8xC	a
porazil	porazit	k5eAaPmAgInS	porazit
Turky	Turek	k1gMnPc4	Turek
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Erzicanan	Erzicanan	k1gInSc4	Erzicanan
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
úspěchy	úspěch	k1gInPc1	úspěch
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
také	také	k9	také
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
místních	místní	k2eAgMnPc2d1	místní
Arménů	Armén	k1gMnPc2	Armén
vedly	vést	k5eAaImAgFnP	vést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
k	k	k7c3	k
arménské	arménský	k2eAgFnSc3d1	arménská
genocidě	genocida	k1gFnSc3	genocida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgMnS	být
Judenič	Judenič	k1gFnSc4	Judenič
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
rozkaz	rozkaz	k1gInSc4	rozkaz
vůdce	vůdce	k1gMnSc2	vůdce
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
Alexandra	Alexandr	k1gMnSc2	Alexandr
Kerenského	Kerenský	k2eAgMnSc2d1	Kerenský
odvolán	odvolán	k2eAgInSc4d1	odvolán
a	a	k8xC	a
předčasně	předčasně	k6eAd1	předčasně
penzionován	penzionován	k2eAgInSc1d1	penzionován
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
dopraven	dopravit	k5eAaPmNgInS	dopravit
z	z	k7c2	z
Tbilisi	Tbilisi	k1gNnSc2	Tbilisi
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podporoval	podporovat	k5eAaImAgInS	podporovat
Kornilovův	Kornilovův	k2eAgInSc4d1	Kornilovův
puč	puč	k1gInSc4	puč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ruská	ruský	k2eAgFnSc1d1	ruská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
budování	budování	k1gNnSc2	budování
bělogvardějských	bělogvardějský	k2eAgFnPc2d1	bělogvardějská
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1919	[number]	k4	1919
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
s	s	k7c7	s
americkými	americký	k2eAgMnPc7d1	americký
a	a	k8xC	a
britskými	britský	k2eAgMnPc7d1	britský
diplomaty	diplomat	k1gMnPc7	diplomat
a	a	k8xC	a
dojednal	dojednat	k5eAaPmAgMnS	dojednat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
omezenou	omezený	k2eAgFnSc7d1	omezená
podporu	podpora	k1gFnSc4	podpora
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
Britů	Brit	k1gMnPc2	Brit
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1919	[number]	k4	1919
navázal	navázat	k5eAaPmAgInS	navázat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
admirálem	admirál	k1gMnSc7	admirál
Kolčakem	Kolčak	k1gMnSc7	Kolčak
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Omsku	Omsk	k1gInSc6	Omsk
všeruskou	všeruský	k2eAgFnSc4d1	Všeruská
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Judenič	Judenič	k1gMnSc1	Judenič
dostal	dostat	k5eAaPmAgMnS	dostat
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
boji	boj	k1gInSc6	boj
při	při	k7c6	při
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
potřebné	potřebný	k2eAgInPc4d1	potřebný
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Talinu	Talin	k1gInSc2	Talin
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Rodzjankem	Rodzjanek	k1gMnSc7	Rodzjanek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
bělogvardějskou	bělogvardějský	k2eAgFnSc4d1	bělogvardějská
Severní	severní	k2eAgFnSc4d1	severní
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaBmAgMnP	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc7	jejich
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bude	být	k5eAaImBp3nS	být
dobýt	dobýt	k5eAaPmF	dobýt
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
a	a	k8xC	a
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
vojsko	vojsko	k1gNnSc1	vojsko
bude	být	k5eAaImBp3nS	být
formálně	formálně	k6eAd1	formálně
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
estonské	estonský	k2eAgFnSc2d1	Estonská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1919	[number]	k4	1919
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
klíčového	klíčový	k2eAgMnSc2d1	klíčový
spojence	spojenec	k1gMnSc2	spojenec
Estonska	Estonsko	k1gNnSc2	Estonsko
musel	muset	k5eAaImAgMnS	muset
vytvořit	vytvořit	k5eAaPmF	vytvořit
protirevoluční	protirevoluční	k2eAgFnSc4d1	protirevoluční
Severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
monarchistů	monarchista	k1gMnPc2	monarchista
<g/>
,	,	kIx,	,
socialistů	socialist	k1gMnPc2	socialist
a	a	k8xC	a
menševiků	menševik	k1gMnPc2	menševik
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc4d1	následující
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
strávil	strávit	k5eAaPmAgMnS	strávit
výcvikem	výcvik	k1gInSc7	výcvik
své	svůj	k3xOyFgFnSc2	svůj
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1919	[number]	k4	1919
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dobře	dobře	k6eAd1	dobře
vycvičená	vycvičený	k2eAgFnSc1d1	vycvičená
armáda	armáda	k1gFnSc1	armáda
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
17	[number]	k4	17
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
s	s	k7c7	s
53	[number]	k4	53
děly	dělo	k1gNnPc7	dělo
a	a	k8xC	a
6	[number]	k4	6
tanky	tank	k1gInPc7	tank
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Útok	útok	k1gInSc1	útok
na	na	k7c4	na
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1919	[number]	k4	1919
se	se	k3xPyFc4	se
vrhla	vrhnout	k5eAaPmAgFnS	vrhnout
jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
na	na	k7c4	na
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jen	jen	k9	jen
lehce	lehko	k6eAd1	lehko
bráněno	bráněn	k2eAgNnSc1d1	bráněno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Rudou	rudý	k2eAgFnSc4d1	rudá
armádu	armáda	k1gFnSc4	armáda
zaměstnával	zaměstnávat	k5eAaImAgMnS	zaměstnávat
boj	boj	k1gInSc4	boj
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
frontách	fronta	k1gFnPc6	fronta
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Judeničův	Judeničův	k2eAgMnSc1d1	Judeničův
přítel	přítel	k1gMnSc1	přítel
z	z	k7c2	z
carské	carský	k2eAgFnSc2d1	carská
armády	armáda	k1gFnSc2	armáda
finský	finský	k2eAgMnSc1d1	finský
generál	generál	k1gMnSc1	generál
Mannerheim	Mannerheim	k1gMnSc1	Mannerheim
naléhal	naléhat	k5eAaImAgMnS	naléhat
na	na	k7c4	na
finskou	finský	k2eAgFnSc4d1	finská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
připojila	připojit	k5eAaPmAgFnS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podmínila	podmínit	k5eAaPmAgFnS	podmínit
účast	účast	k1gFnSc4	účast
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
potvrzením	potvrzení	k1gNnSc7	potvrzení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Finska	Finsko	k1gNnSc2	Finsko
bělogvardějci	bělogvardějec	k1gMnSc3	bělogvardějec
<g/>
.	.	kIx.	.
</s>
<s>
Admirál	admirál	k1gMnSc1	admirál
Kolčak	Kolčak	k1gMnSc1	Kolčak
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
hlavou	hlava	k1gFnSc7	hlava
bělogvardějců	bělogvardějec	k1gMnPc2	bělogvardějec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tak	tak	k6eAd1	tak
nehodlal	hodlat	k5eNaImAgMnS	hodlat
učinit	učinit	k5eAaImF	učinit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
Mannerheimova	Mannerheimův	k2eAgFnSc1d1	Mannerheimova
prosba	prosba	k1gFnSc1	prosba
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
armáda	armáda	k1gFnSc1	armáda
zatím	zatím	k6eAd1	zatím
dobyla	dobýt	k5eAaPmAgFnS	dobýt
Kingisepp	Kingisepp	k1gInSc4	Kingisepp
a	a	k8xC	a
Gatčinu	Gatčina	k1gFnSc4	Gatčina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přerušit	přerušit	k5eAaPmF	přerušit
vlakové	vlakový	k2eAgNnSc4d1	vlakové
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
Moskvou	Moskva	k1gFnSc7	Moskva
a	a	k8xC	a
Petrohradem	Petrohrad	k1gInSc7	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Bolševici	bolševik	k1gMnPc1	bolševik
tak	tak	k9	tak
mohli	moct	k5eAaImAgMnP	moct
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
města	město	k1gNnSc2	město
početné	početný	k2eAgFnSc2d1	početná
posily	posila	k1gFnSc2	posila
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
zastavily	zastavit	k5eAaPmAgFnP	zastavit
postup	postup	k1gInSc4	postup
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
armády	armáda	k1gFnSc2	armáda
na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
1919	[number]	k4	1919
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1919	[number]	k4	1919
byli	být	k5eAaImAgMnP	být
už	už	k6eAd1	už
značně	značně	k6eAd1	značně
demoralizovaní	demoralizovaný	k2eAgMnPc1d1	demoralizovaný
bělogvardějci	bělogvardějec	k1gMnPc1	bělogvardějec
zatlačeni	zatlačit	k5eAaPmNgMnP	zatlačit
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c6	na
území	území	k1gNnSc6	území
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgNnSc1d1	vrchní
velení	velení	k1gNnSc1	velení
estonské	estonský	k2eAgFnSc2d1	Estonská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
zápornému	záporný	k2eAgInSc3d1	záporný
vztahu	vztah	k1gInSc3	vztah
bělogvardějců	bělogvardějec	k1gMnPc2	bělogvardějec
k	k	k7c3	k
nezávislosti	nezávislost	k1gFnSc3	nezávislost
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc1	zbytek
ustupující	ustupující	k2eAgFnSc2d1	ustupující
Judeničovy	Judeničův	k2eAgFnSc2d1	Judeničův
armády	armáda	k1gFnSc2	armáda
odzbrojili	odzbrojit	k5eAaPmAgMnP	odzbrojit
a	a	k8xC	a
internovali	internovat	k5eAaBmAgMnP	internovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1920	[number]	k4	1920
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
Estonsko	Estonsko	k1gNnSc1	Estonsko
příměří	příměří	k1gNnSc2	příměří
se	s	k7c7	s
sovětským	sovětský	k2eAgNnSc7d1	sovětské
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
bolševici	bolševik	k1gMnPc1	bolševik
se	se	k3xPyFc4	se
zavázali	zavázat	k5eAaPmAgMnP	zavázat
uznat	uznat	k5eAaPmF	uznat
estonskou	estonský	k2eAgFnSc4d1	Estonská
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
se	se	k3xPyFc4	se
Judenič	Judenič	k1gMnSc1	Judenič
pokusil	pokusit	k5eAaPmAgMnS	pokusit
neúspěšně	úspěšně	k6eNd1	úspěšně
uprchnout	uprchnout	k5eAaPmF	uprchnout
z	z	k7c2	z
Estonska	Estonsko	k1gNnSc2	Estonsko
s	s	k7c7	s
veškerými	veškerý	k3xTgFnPc7	veškerý
financemi	finance	k1gFnPc7	finance
Severozápadní	severozápadní	k2eAgFnPc1d1	severozápadní
armády	armáda	k1gFnPc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zadržen	zadržet	k5eAaPmNgInS	zadržet
generálem	generál	k1gMnSc7	generál
Bułak-Bałachowiczem	Bułak-Bałachowicz	k1gMnSc7	Bułak-Bałachowicz
společně	společně	k6eAd1	společně
s	s	k7c7	s
estonskou	estonský	k2eAgFnSc7d1	Estonská
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zadržení	zadržení	k1gNnSc6	zadržení
měl	mít	k5eAaImAgMnS	mít
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
227	[number]	k4	227
000	[number]	k4	000
britských	britský	k2eAgFnPc2d1	britská
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
250	[number]	k4	250
000	[number]	k4	000
estonských	estonský	k2eAgFnPc2d1	Estonská
marek	marka	k1gFnPc2	marka
a	a	k8xC	a
110	[number]	k4	110
000	[number]	k4	000
000	[number]	k4	000
finských	finský	k2eAgFnPc2d1	finská
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
byly	být	k5eAaImAgInP	být
zabaveny	zabavit	k5eAaPmNgInP	zabavit
a	a	k8xC	a
vyplaceny	vyplatit	k5eAaPmNgInP	vyplatit
zbývajícím	zbývající	k2eAgMnPc3d1	zbývající
vojákům	voják	k1gMnPc3	voják
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
armády	armáda	k1gFnSc2	armáda
jako	jako	k8xS	jako
poslední	poslední	k2eAgInSc4d1	poslední
plat	plat	k1gInSc4	plat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgInS	být
záhy	záhy	k6eAd1	záhy
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
a	a	k8xC	a
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poválečný	poválečný	k2eAgInSc4d1	poválečný
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
nehrál	hrát	k5eNaImAgInS	hrát
žádnou	žádný	k3yNgFnSc4	žádný
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
mezi	mezi	k7c7	mezi
ruskou	ruský	k2eAgFnSc7d1	ruská
emigrací	emigrace	k1gFnSc7	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Usídlil	usídlit	k5eAaPmAgInS	usídlit
se	se	k3xPyFc4	se
na	na	k7c6	na
Francouzské	francouzský	k2eAgFnSc6d1	francouzská
riviéře	riviéra	k1gFnSc6	riviéra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
taky	taky	k6eAd1	taky
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
nedaleko	nedaleko	k7c2	nedaleko
Cannes	Cannes	k1gNnSc2	Cannes
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
71	[number]	k4	71
let	léto	k1gNnPc2	léto
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Nikolai	Nikola	k1gFnSc2	Nikola
Yudenich	Yudenicha	k1gFnPc2	Yudenicha
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
LAINOVÁ	LAINOVÁ	kA	LAINOVÁ
<g/>
,	,	kIx,	,
Radka	Radka	k1gFnSc1	Radka
<g/>
.	.	kIx.	.
</s>
<s>
Judenič	Judeničit	k5eAaImRp2nS	Judeničit
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
rudého	rudý	k2eAgInSc2d1	rudý
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
8	[number]	k4	8
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
Zubov	Zubov	k1gInSc1	Zubov
(	(	kIx(	(
<g/>
red	red	k?	red
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k6eAd1	Argo
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Judenič	Judenič	k1gFnSc7	Judenič
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Judenič	Judeničit	k5eAaPmRp2nS	Judeničit
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
::	::	k?	::
J	J	kA	J
::	::	k?	::
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
SOV	sova	k1gFnPc2	sova
<g/>
/	/	kIx~	/
<g/>
RUS	Rus	k1gFnSc1	Rus
<g/>
)	)	kIx)	)
na	na	k7c4	na
forum	forum	k1gNnSc4	forum
<g/>
.	.	kIx.	.
<g/>
valka	valka	k1gFnSc1	valka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Biografie	biografie	k1gFnSc1	biografie
na	na	k7c4	na
hrono	hrono	k1gNnSc4	hrono
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
</s>
</p>
