<s>
Carrie	Carrie	k1gFnSc1
Snodgress	Snodgressa	k1gFnPc2
</s>
<s>
Carrie	Carrie	k1gFnSc2
Snodgress	Snodgressa	k1gFnPc2
Narození	narození	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1945	#num#	k4
<g/>
Park	park	k1gInSc1
Ridge	Ridg	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2004	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
58	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Los	los	k1gInSc1
Angeles	Angelesa	k1gFnPc2
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
selhání	selhání	k1gNnSc1
ledvin	ledvina	k1gFnPc2
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Forest	Forest	k1gInSc1
Lawn	Lawn	k1gInSc1
Memorial	Memorial	k1gInSc1
Park	park	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Maine	Mainout	k5eAaImIp3nS,k5eAaPmIp3nS
East	East	k2eAgMnSc1d1
High	High	k1gMnSc1
SchoolNorthern	SchoolNortherna	k1gFnPc2
Illinois	Illinois	k1gInSc4
UniversityThe	UniversityThe	k1gFnSc4
Theatre	Theatr	k1gInSc5
School	School	k1gInSc4
at	at	k?
DePaul	DePaul	k1gInSc1
University	universita	k1gFnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
televizní	televizní	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
filmová	filmový	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
a	a	k8xC
divadelní	divadelní	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
Golden	Goldna	k1gFnPc2
Globe	globus	k1gInSc5
Award	Award	k1gInSc4
for	forum	k1gNnPc2
New	New	k1gMnSc2
Star	Star	kA
of	of	k?
the	the	k?
Year	Year	k1gInSc1
–	–	k?
Actress	Actress	k1gInSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Neil	Neil	k1gMnSc1
Young	Young	k1gMnSc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
Jack	Jack	k1gInSc1
Nitzsche	Nitzsch	k1gInSc2
(	(	kIx(
<g/>
do	do	k7c2
1979	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Carrie	Carrie	k1gFnSc1
Snodgress	Snodgressa	k1gFnPc2
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1945	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2004	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
americká	americký	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
měla	mít	k5eAaImAgFnS
malou	malý	k2eAgFnSc4d1
roli	role	k1gFnSc4
(	(	kIx(
<g/>
neuvedenou	uvedený	k2eNgFnSc4d1
v	v	k7c6
titulcích	titulek	k1gInPc6
<g/>
)	)	kIx)
ve	v	k7c6
filmu	film	k1gInSc6
Bezstarostná	bezstarostný	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
hrála	hrát	k5eAaImAgNnP
i	i	k9
v	v	k7c6
dalších	další	k2eAgInPc6d1
filmech	film	k1gInPc6
<g/>
,	,	kIx,
například	například	k6eAd1
Diary	Diara	k1gFnPc1
of	of	k?
a	a	k8xC
Mad	Mad	k1gMnSc5
Housewife	Housewif	k1gMnSc5
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
byla	být	k5eAaImAgFnS
nominována	nominován	k2eAgFnSc1d1
na	na	k7c4
Oscara	Oscar	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herectví	herectví	k1gNnPc2
se	se	k3xPyFc4
přestala	přestat	k5eAaPmAgFnS
věnovat	věnovat	k5eAaImF,k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začala	začít	k5eAaPmAgFnS
žít	žít	k5eAaImF
s	s	k7c7
kanadským	kanadský	k2eAgMnSc7d1
hudebníkem	hudebník	k1gMnSc7
Neilem	Neil	k1gMnSc7
Youngem	Young	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodil	narodit	k5eAaPmAgMnS
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
vážné	vážný	k2eAgInPc4d1
zdravotní	zdravotní	k2eAgInPc4d1
problémy	problém	k1gInPc4
a	a	k8xC
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
místo	místo	k7c2
herectví	herectví	k1gNnSc2
starala	starat	k5eAaImAgFnS
o	o	k7c4
něj	on	k3xPp3gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Youngem	Young	k1gInSc7
se	se	k3xPyFc4
rozešla	rozejít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
herectví	herectví	k1gNnSc3
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
s	s	k7c7
filmem	film	k1gInSc7
Zuřivost	zuřivost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
chodila	chodit	k5eAaImAgFnS
s	s	k7c7
hudebníkem	hudebník	k1gMnSc7
Jackem	Jacek	k1gMnSc7
Nitzschem	Nitzsch	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gInSc1
vztah	vztah	k1gInSc1
brzy	brzy	k6eAd1
skončil	skončit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1981	#num#	k4
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c2
malíře	malíř	k1gMnSc2
Roberta	Robert	k1gMnSc2
Jonese	Jonese	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
tento	tento	k3xDgInSc1
vztah	vztah	k1gInSc1
neměl	mít	k5eNaImAgInS
dlouhého	dlouhý	k2eAgNnSc2d1
trvání	trvání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
řadě	řada	k1gFnSc6
dalších	další	k2eAgInPc2d1
snímků	snímek	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
posledním	poslední	k2eAgMnSc6d1
z	z	k7c2
nich	on	k3xPp3gInPc2
byl	být	k5eAaImAgInS
Andělé	anděl	k1gMnPc1
s	s	k7c7
ocelovým	ocelový	k2eAgInSc7d1
hlasem	hlas	k1gInSc7
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřela	zemřít	k5eAaPmAgFnS
na	na	k7c4
srdeční	srdeční	k2eAgNnSc4d1
a	a	k8xC
jaterní	jaterní	k2eAgNnSc4d1
selhání	selhání	k1gNnSc4
ve	v	k7c6
věku	věk	k1gInSc6
58	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Associated	Associated	k1gInSc1
Press	Press	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carrie	Carrie	k1gFnSc1
Snodgress	Snodgressa	k1gFnPc2
<g/>
,	,	kIx,
57	#num#	k4
<g/>
,	,	kIx,
Dies	Dies	k1gInSc1
<g/>
;	;	kIx,
Starred	Starred	k1gInSc1
as	as	k1gNnSc1
'	'	kIx"
<g/>
Mad	Mad	k1gMnSc5
Housewife	Housewif	k1gMnSc5
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
2004-04-10	2004-04-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BERGAN	BERGAN	kA
<g/>
,	,	kIx,
Ronald	Ronald	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carrie	Carrie	k1gFnSc1
Snodgress	Snodgressa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
2004-04-14	2004-04-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Carrie	Carrie	k1gFnSc1
Snodgress	Snodgressa	k1gFnPc2
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Zlatý	zlatý	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
(	(	kIx(
<g/>
komedie	komedie	k1gFnSc1
/	/	kIx~
muzikál	muzikál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Judy	judo	k1gNnPc7
Hollidayová	Hollidayová	k1gFnSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
June	jun	k1gMnSc5
Allysonová	Allysonová	k1gFnSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Susan	Susan	k1gInSc1
Haywardová	Haywardová	k1gFnSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ethel	Ethel	k1gInSc1
Mermanová	Mermanová	k1gFnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Judy	judo	k1gNnPc7
Garlandová	Garlandový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jean	Jean	k1gMnSc1
Simmonsová	Simmonsová	k1gFnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Deborah	Deborah	k1gInSc1
Kerrová	Kerrová	k1gFnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Kay	Kay	k?
Kendallová	Kendallová	k1gFnSc1
/	/	kIx~
Taina	Taien	k2eAgFnSc1d1
Elgová	Elgová	k1gFnSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Rosalind	Rosalinda	k1gFnPc2
Russellová	Russellová	k1gFnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Marilyn	Marilyn	k1gFnSc1
Monroe	Monroe	k1gFnSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Shirley	Shirlea	k1gFnPc1
MacLaine	MacLain	k1gInSc5
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Rosalind	Rosalinda	k1gFnPc2
Russellová	Russellová	k1gFnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Rosalind	Rosalinda	k1gFnPc2
Russellová	Russellová	k1gFnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Shirley	Shirlea	k1gFnPc1
MacLaine	MacLain	k1gInSc5
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Julie	Julie	k1gFnSc1
Andrewsová	Andrewsová	k1gFnSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Julie	Julie	k1gFnSc1
Andrewsová	Andrewsová	k1gFnSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lynn	Lynn	k1gInSc1
Redgraveová	Redgraveová	k1gFnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Anne	Anne	k1gFnSc1
Bancroftová	Bancroftová	k1gFnSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Barbra	Barbra	k6eAd1
Streisandová	Streisandový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Patty	Patt	k1gInPc1
Dukeová	Dukeová	k1gFnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Carrie	Carrie	k1gFnSc1
Snodgress	Snodgressa	k1gFnPc2
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Twiggy	Twigga	k1gFnPc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Liza	Liza	k6eAd1
Minnelli	Minnell	k1gMnPc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Glenda	Glenda	k1gFnSc1
Jacksonová	Jacksonová	k1gFnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Raquel	Raquel	k1gInSc1
Welchová	Welchová	k1gFnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ann-Margret	Ann-Margret	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
180752	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
172681421	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1675	#num#	k4
5799	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83161730	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
76515242	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83161730	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
