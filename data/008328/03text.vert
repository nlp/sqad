<p>
<s>
Sul	sout	k5eAaImAgMnS	sout
ponticello	ponticello	k1gNnSc4	ponticello
(	(	kIx(	(
<g/>
na	na	k7c6	na
kobylce	kobylka	k1gFnSc6	kobylka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
strunné	strunný	k2eAgInPc4d1	strunný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
smyčcem	smyčec	k1gInSc7	smyčec
<g/>
)	)	kIx)	)
na	na	k7c6	na
strunách	struna	k1gFnPc6	struna
u	u	k7c2	u
kobylky	kobylka	k1gFnSc2	kobylka
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Struna	struna	k1gFnSc1	struna
rozeznívaná	rozeznívaný	k2eAgFnSc1d1	rozeznívaný
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
kobylky	kobylka	k1gFnSc2	kobylka
vydává	vydávat	k5eAaImIp3nS	vydávat
typický	typický	k2eAgInSc1d1	typický
ostrý	ostrý	k2eAgInSc1d1	ostrý
tón	tón	k1gInSc1	tón
s	s	k7c7	s
potlačenou	potlačený	k2eAgFnSc7d1	potlačená
základní	základní	k2eAgFnSc7d1	základní
harmonickou	harmonický	k2eAgFnSc7d1	harmonická
složkou	složka	k1gFnSc7	složka
a	a	k8xC	a
zvýrazněnými	zvýrazněný	k2eAgFnPc7d1	zvýrazněná
vyššími	vysoký	k2eAgFnPc7d2	vyšší
harmonickými	harmonický	k2eAgFnPc7d1	harmonická
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
smyčcových	smyčcový	k2eAgInPc2d1	smyčcový
nástrojů	nástroj	k1gInPc2	nástroj
využíván	využívat	k5eAaImNgInS	využívat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
sólové	sólový	k2eAgFnSc6d1	sólová
<g/>
,	,	kIx,	,
komorní	komorní	k2eAgFnSc3d1	komorní
i	i	k8xC	i
orchestrální	orchestrální	k2eAgFnSc3d1	orchestrální
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technik	k1gMnSc4	technik
sul	sout	k5eAaImAgMnS	sout
ponticello	ponticello	k1gNnSc1	ponticello
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
trsátkem	trsátko	k1gNnSc7	trsátko
či	či	k8xC	či
prsty	prst	k1gInPc7	prst
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
strunné	strunný	k2eAgInPc4d1	strunný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostřejší	ostrý	k2eAgInSc1d2	ostřejší
tón	tón	k1gInSc1	tón
smyčců	smyčec	k1gInPc2	smyčec
někdy	někdy	k6eAd1	někdy
až	až	k6eAd1	až
děsivé	děsivý	k2eAgFnPc4d1	děsivá
barvy	barva	k1gFnPc4	barva
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
hodí	hodit	k5eAaImIp3nS	hodit
například	například	k6eAd1	například
do	do	k7c2	do
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tremolem	tremolo	k1gNnSc7	tremolo
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
notografii	notografie	k1gFnSc6	notografie
používáno	používán	k2eAgNnSc1d1	používáno
zkrácené	zkrácený	k2eAgNnSc1d1	zkrácené
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
pont	pont	k1gInSc1	pont
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sul	sout	k5eAaImAgMnS	sout
tasto	tasto	k1gNnSc4	tasto
</s>
</p>
<p>
<s>
Col	cola	k1gFnPc2	cola
legno	legno	k6eAd1	legno
</s>
</p>
<p>
<s>
Con	Con	k?	Con
sordino	sordina	k1gFnSc5	sordina
</s>
</p>
