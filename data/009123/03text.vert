<p>
<s>
Northampton	Northampton	k1gInSc1	Northampton
Town	Town	k1gInSc1	Town
FC	FC	kA	FC
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Northampton	Northampton	k1gInSc1	Northampton
Town	Towna	k1gFnPc2	Towna
Football	Footballa	k1gFnPc2	Footballa
Club	club	k1gInSc1	club
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgInSc1d1	anglický
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Northampton	Northampton	k1gInSc1	Northampton
v	v	k7c6	v
nemetropolitním	metropolitní	k2eNgNnSc6d1	nemetropolitní
hrabství	hrabství	k1gNnSc6	hrabství
Northamptonshire	Northamptonshir	k1gMnSc5	Northamptonshir
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
anglické	anglický	k2eAgFnSc6d1	anglická
soutěži	soutěž	k1gFnSc6	soutěž
EFL	EFL	kA	EFL
League	Leagu	k1gMnSc2	Leagu
Two	Two	k1gMnSc2	Two
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Sixfields	Sixfieldsa	k1gFnPc2	Sixfieldsa
Stadium	stadium	k1gNnSc1	stadium
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
7	[number]	k4	7
653	[number]	k4	653
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
v	v	k7c6	v
domácích	domácí	k2eAgInPc6d1	domácí
pohárech	pohár	k1gInPc6	pohár
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
FA	fa	k1gNnSc1	fa
Cup	cup	k1gInSc1	cup
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
:	:	kIx,	:
1933	[number]	k4	1933
<g/>
/	/	kIx~	/
<g/>
34	[number]	k4	34
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
/	/	kIx~	/
<g/>
70	[number]	k4	70
</s>
</p>
<p>
<s>
EFL	EFL	kA	EFL
Cup	cup	k1gInSc1	cup
</s>
</p>
<p>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
<g/>
:	:	kIx,	:
1964	[number]	k4	1964
<g/>
/	/	kIx~	/
<g/>
65	[number]	k4	65
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
</s>
</p>
<p>
<s>
EFL	EFL	kA	EFL
Trophy	Troph	k1gInPc4	Troph
</s>
</p>
<p>
<s>
Semifinále	semifinále	k1gNnSc1	semifinále
(	(	kIx(	(
<g/>
Jih	jih	k1gInSc1	jih
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
==	==	k?	==
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
<g/>
:	:	kIx,	:
Midland	Midlanda	k1gFnPc2	Midlanda
Football	Football	k1gInSc1	Football
League	Leagu	k1gInSc2	Leagu
</s>
</p>
<p>
<s>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
:	:	kIx,	:
Southern	Southerna	k1gFnPc2	Southerna
Football	Football	k1gInSc1	Football
League	Leagu	k1gInSc2	Leagu
(	(	kIx(	(
<g/>
Division	Division	k1gInSc1	Division
One	One	k1gFnSc2	One
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
<g/>
:	:	kIx,	:
Football	Football	k1gInSc1	Football
League	Leagu	k1gFnSc2	Leagu
Third	Thirda	k1gFnPc2	Thirda
Division	Division	k1gInSc1	Division
South	South	k1gMnSc1	South
</s>
</p>
<p>
<s>
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Fourth	Fourth	k1gMnSc1	Fourth
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Third	Third	k1gMnSc1	Third
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Second	Second	k1gMnSc1	Second
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
First	First	k1gMnSc1	First
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Second	Second	k1gMnSc1	Second
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Third	Third	k1gMnSc1	Third
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Fourth	Fourth	k1gMnSc1	Fourth
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Third	Third	k1gMnSc1	Third
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Fourth	Fourth	k1gMnSc1	Fourth
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Third	Third	k1gMnSc1	Third
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Fourth	Fourth	k1gMnSc1	Fourth
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Third	Third	k1gMnSc1	Third
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Second	Second	k1gMnSc1	Second
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Third	Third	k1gMnSc1	Third
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Second	Second	k1gMnSc1	Second
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gInSc1	League
Third	Third	k1gMnSc1	Third
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Football	Footballum	k1gNnPc2	Footballum
League	League	k1gNnSc7	League
Two	Two	k1gFnSc1	Two
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Football	Footballum	k1gNnPc2	Footballum
League	League	k1gNnSc7	League
One	One	k1gFnSc1	One
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Football	Footballum	k1gNnPc2	Footballum
League	League	k1gNnSc7	League
Two	Two	k1gFnSc1	Two
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
:	:	kIx,	:
English	English	k1gInSc1	English
Football	Football	k1gInSc1	Football
League	Leagu	k1gMnSc2	Leagu
One	One	k1gMnSc2	One
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
–	–	k?	–
:	:	kIx,	:
English	English	k1gMnSc1	English
Football	Football	k1gInSc4	Football
League	Leagu	k1gInPc4	Leagu
TwoJednotlivé	TwoJednotlivý	k2eAgInPc4d1	TwoJednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	Z	kA	Z
-	-	kIx~	-
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
-	-	kIx~	-
výhry	výhra	k1gFnPc4	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
-	-	kIx~	-
remízy	remíz	k1gInPc1	remíz
<g/>
,	,	kIx,	,
P	P	kA	P
-	-	kIx~	-
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
-	-	kIx~	-
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
-	-	kIx~	-
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
-	-	kIx~	-
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
-	-	kIx~	-
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Northampton	Northampton	k1gInSc1	Northampton
Town	Town	k1gNnSc4	Town
FC	FC	kA	FC
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
