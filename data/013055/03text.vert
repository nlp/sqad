<p>
<s>
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
La	la	k0	la
Sorbonne	Sorbonn	k1gMnSc5	Sorbonn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
monumentální	monumentální	k2eAgInSc4d1	monumentální
komplex	komplex	k1gInSc4	komplex
v	v	k7c6	v
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
latinské	latinský	k2eAgFnSc6d1	Latinská
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc2d3	nejstarší
francouzské	francouzský	k2eAgFnSc2d1	francouzská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nejspíše	nejspíše	k9	nejspíše
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1160	[number]	k4	1160
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
teologické	teologický	k2eAgFnSc2d1	teologická
koleje	kolej	k1gFnSc2	kolej
této	tento	k3xDgFnSc2	tento
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1257	[number]	k4	1257
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc2	svůj
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byl	být	k5eAaImAgMnS	být
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Sorbon	Sorbon	k1gMnSc1	Sorbon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
neexistuje	existovat	k5eNaImIp3nS	existovat
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
reformou	reforma	k1gFnSc7	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
byla	být	k5eAaImAgFnS	být
včleněna	včlenit	k5eAaPmNgFnS	včlenit
do	do	k7c2	do
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
byla	být	k5eAaImAgFnS	být
rozčleněna	rozčlenit	k5eAaPmNgFnS	rozčlenit
do	do	k7c2	do
Univerzity	univerzita	k1gFnSc2	univerzita
Paříž	Paříž	k1gFnSc1	Paříž
I	I	kA	I
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
I	i	k9	i
Panthéon-Sorbonne	Panthéon-Sorbonn	k1gMnSc5	Panthéon-Sorbonn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Univerzity	univerzita	k1gFnSc2	univerzita
Paříž	Paříž	k1gFnSc1	Paříž
III	III	kA	III
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
III	III	kA	III
Sorbonne	Sorbonn	k1gInSc5	Sorbonn
Nouvelle	Nouvelle	k1gNnPc2	Nouvelle
<g/>
)	)	kIx)	)
a	a	k8xC	a
Univerzity	univerzita	k1gFnSc2	univerzita
Paříž	Paříž	k1gFnSc1	Paříž
IV	IV	kA	IV
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
IV	IV	kA	IV
Paris-Sorbonne	Paris-Sorbonn	k1gInSc5	Paris-Sorbonn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
sídlí	sídlet	k5eAaImIp3nS	sídlet
částečně	částečně	k6eAd1	částečně
i	i	k9	i
Univerzita	univerzita	k1gFnSc1	univerzita
Paříž	Paříž	k1gFnSc1	Paříž
V	v	k7c4	v
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
École	École	k1gFnSc1	École
nationale	nationale	k6eAd1	nationale
des	des	k1gNnSc7	des
chartes	chartesa	k1gFnPc2	chartesa
a	a	k8xC	a
společný	společný	k2eAgInSc1d1	společný
rektorát	rektorát	k1gInSc1	rektorát
(	(	kIx(	(
<g/>
Chancellerie	Chancellerie	k1gFnSc1	Chancellerie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
==	==	k?	==
</s>
</p>
<p>
<s>
1253	[number]	k4	1253
Sorbonnu	Sorbonna	k1gFnSc4	Sorbonna
založil	založit	k5eAaPmAgInS	založit
kaplan	kaplan	k1gMnSc1	kaplan
a	a	k8xC	a
zpovědník	zpovědník	k1gMnSc1	zpovědník
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Sorbon	Sorbon	k1gMnSc1	Sorbon
původně	původně	k6eAd1	původně
jako	jako	k9	jako
kolegium	kolegium	k1gNnSc4	kolegium
pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
studenty	student	k1gMnPc4	student
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
do	do	k7c2	do
střediska	středisko	k1gNnSc2	středisko
pro	pro	k7c4	pro
teologické	teologický	k2eAgNnSc4d1	teologické
studia	studio	k1gNnPc4	studio
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
na	na	k7c6	na
Sorboně	Sorbona	k1gFnSc6	Sorbona
studovalo	studovat	k5eAaImAgNnS	studovat
15	[number]	k4	15
000	[number]	k4	000
žáků	žák	k1gMnPc2	žák
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
70	[number]	k4	70
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
1328	[number]	k4	1328
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1340	[number]	k4	1340
rektorem	rektor	k1gMnSc7	rektor
Jean	Jean	k1gMnSc1	Jean
Buridan	Buridan	k1gMnSc1	Buridan
<g/>
1355	[number]	k4	1355
rektorem	rektor	k1gMnSc7	rektor
Čech	Čech	k1gMnSc1	Čech
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Raňkův	Raňkův	k2eAgMnSc1d1	Raňkův
z	z	k7c2	z
Ježova	Ježův	k2eAgInSc2d1	Ježův
<g/>
1380	[number]	k4	1380
zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc1	doktorát
teologie	teologie	k1gFnSc2	teologie
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Ailly	Ailla	k1gFnSc2	Ailla
<g/>
1469	[number]	k4	1469
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
první	první	k4xOgFnSc1	první
tiskárna	tiskárna	k1gFnSc1	tiskárna
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
1624	[number]	k4	1624
<g/>
–	–	k?	–
<g/>
1642	[number]	k4	1642
nechal	nechat	k5eAaPmAgMnS	nechat
kardinál	kardinál	k1gMnSc1	kardinál
Richelieu	Richelieu	k1gMnSc1	Richelieu
přestavět	přestavět	k5eAaPmF	přestavět
většinu	většina	k1gFnSc4	většina
budov	budova	k1gFnPc2	budova
<g/>
1806	[number]	k4	1806
(	(	kIx(	(
<g/>
za	za	k7c4	za
Napoleona	Napoleon	k1gMnSc4	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
)	)	kIx)	)
další	další	k2eAgFnPc1d1	další
stavební	stavební	k2eAgFnPc1d1	stavební
úpravy	úprava	k1gFnPc1	úprava
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
univerzita	univerzita	k1gFnSc1	univerzita
dostala	dostat	k5eAaPmAgFnS	dostat
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
<g/>
;	;	kIx,	;
v	v	k7c6	v
univerzitním	univerzitní	k2eAgInSc6d1	univerzitní
kostele	kostel	k1gInSc6	kostel
je	být	k5eAaImIp3nS	být
hrobka	hrobka	k1gFnSc1	hrobka
kardinála	kardinál	k1gMnSc2	kardinál
Richelieua	Richelieuus	k1gMnSc2	Richelieuus
</s>
</p>
<p>
<s>
==	==	k?	==
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
učitelé	učitel	k1gMnPc1	učitel
a	a	k8xC	a
studenti	student	k1gMnPc1	student
==	==	k?	==
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Aljechin	Aljechin	k1gMnSc1	Aljechin
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
</s>
</p>
<p>
<s>
Antoine	Antoinout	k5eAaPmIp3nS	Antoinout
Arnauld	Arnauld	k1gMnSc1	Arnauld
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Raymond	Raymond	k1gMnSc1	Raymond
Aron	Aron	k1gMnSc1	Aron
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
sociolog	sociolog	k1gMnSc1	sociolog
</s>
</p>
<p>
<s>
Georges	Georges	k1gMnSc1	Georges
Bataille	Bataille	k1gNnSc2	Bataille
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
</s>
</p>
<p>
<s>
Honoré	Honorý	k2eAgInPc1d1	Honorý
de	de	k?	de
Balzac	Balzac	k1gMnSc1	Balzac
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
Henri	Henri	k6eAd1	Henri
Bergson	Bergson	k1gMnSc1	Bergson
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Claude	Claude	k6eAd1	Claude
Bernard	Bernard	k1gMnSc1	Bernard
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
</s>
</p>
<p>
<s>
Sarah	Sarah	k1gFnSc7	Sarah
Biasini	Biasin	k2eAgMnPc1d1	Biasin
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
dcera	dcera	k1gFnSc1	dcera
Romy	Rom	k1gMnPc4	Rom
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Siger	Siger	k1gInSc1	Siger
z	z	k7c2	z
Brabantu	Brabant	k1gInSc2	Brabant
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Luigi	Luigi	k6eAd1	Luigi
Colani	Colan	k1gMnPc1	Colan
<g/>
,	,	kIx,	,
průmyslový	průmyslový	k2eAgMnSc1d1	průmyslový
designér	designér	k1gMnSc1	designér
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
de	de	k?	de
Coubertin	Coubertin	k1gMnSc1	Coubertin
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
novodobých	novodobý	k2eAgFnPc2d1	novodobá
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
</s>
</p>
<p>
<s>
Victor	Victor	k1gMnSc1	Victor
Cousin	cousina	k1gFnPc2	cousina
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Curie	curie	k1gNnSc2	curie
<g/>
,	,	kIx,	,
fyzička	fyzička	k1gFnSc1	fyzička
<g/>
,	,	kIx,	,
nositelka	nositelka	k1gFnSc1	nositelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Curie	Curie	k1gMnSc5	Curie
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
</s>
</p>
<p>
<s>
Marina	Marina	k1gFnSc1	Marina
Cvětajevová	Cvětajevová	k1gFnSc1	Cvětajevová
<g/>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
</s>
</p>
<p>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Denis	Denisa	k1gFnPc2	Denisa
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Jacques	Jacques	k1gMnSc1	Jacques
Derrida	Derrida	k1gFnSc1	Derrida
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Mistr	mistr	k1gMnSc1	mistr
Eckhart	Eckharta	k1gFnPc2	Eckharta
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
dominikán	dominikán	k1gMnSc1	dominikán
</s>
</p>
<p>
<s>
Erasmus	Erasmus	k1gMnSc1	Erasmus
Rotterdamský	rotterdamský	k2eAgMnSc1d1	rotterdamský
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Lawrence	Lawrence	k1gFnSc1	Lawrence
Ferlinghetti	Ferlinghetť	k1gFnSc2	Ferlinghetť
</s>
</p>
<p>
<s>
Jean-Luc	Jean-Luc	k6eAd1	Jean-Luc
Godard	Godard	k1gMnSc1	Godard
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Bouvier	Bouvier	k1gInSc1	Bouvier
<g/>
,	,	kIx,	,
studentka	studentka	k1gFnSc1	studentka
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
USA	USA	kA	USA
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Klein	Klein	k1gMnSc1	Klein
<g/>
,	,	kIx,	,
francouzský-americký	francouzskýmerický	k2eAgMnSc1d1	francouzský-americký
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Ignác	Ignác	k1gMnSc1	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgInSc2d1	Ježíšův
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Xaverský	xaverský	k2eAgMnSc1d1	xaverský
<g/>
,	,	kIx,	,
misionář	misionář	k1gMnSc1	misionář
<g/>
,	,	kIx,	,
Jezuita	jezuita	k1gMnSc1	jezuita
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Janet	Janet	k1gMnSc1	Janet
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Janet	Janet	k1gMnSc1	Janet
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
</s>
</p>
<p>
<s>
Frédéric	Frédéric	k1gMnSc1	Frédéric
Joliot-Curie	Joliot-Curie	k1gFnSc2	Joliot-Curie
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
</s>
</p>
<p>
<s>
Irè	Irè	k?	Irè
Joliot-Curie	Joliot-Curie	k1gFnSc1	Joliot-Curie
<g/>
,	,	kIx,	,
vědkyně	vědkyně	k1gFnSc1	vědkyně
<g/>
,	,	kIx,	,
držitelka	držitelka	k1gFnSc1	držitelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kalvín	Kalvín	k1gMnSc1	Kalvín
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Jacques	Jacques	k1gMnSc1	Jacques
Lefè	Lefè	k1gMnSc1	Lefè
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Étaples	Étaples	k1gMnSc1	Étaples
<g/>
,	,	kIx,	,
reformátor	reformátor	k1gMnSc1	reformátor
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Bible	bible	k1gFnSc2	bible
</s>
</p>
<p>
<s>
André	André	k1gMnSc1	André
Leroi-Gourhan	Leroi-Gourhan	k1gMnSc1	Leroi-Gourhan
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
etnolog	etnolog	k1gMnSc1	etnolog
</s>
</p>
<p>
<s>
Claude	Claude	k6eAd1	Claude
Lévi-Strauss	Lévi-Strauss	k1gInSc1	Lévi-Strauss
<g/>
,	,	kIx,	,
anthropolog	anthropolog	k1gMnSc1	anthropolog
</s>
</p>
<p>
<s>
Ignác	Ignác	k1gMnSc1	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
jezuitů	jezuita	k1gMnPc2	jezuita
</s>
</p>
<p>
<s>
Norman	Norman	k1gMnSc1	Norman
Mailer	Mailer	k1gMnSc1	Mailer
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Martin	Martin	k1gMnSc1	Martin
du	du	k?	du
Gard	gard	k1gInSc1	gard
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
François	François	k1gFnSc1	François
Mauriac	Mauriac	k1gFnSc1	Mauriac
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Marsilius	Marsilius	k1gInSc1	Marsilius
z	z	k7c2	z
Padovy	Padova	k1gFnSc2	Padova
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Mounier	Mounier	k1gMnSc1	Mounier
<g/>
,	,	kIx,	,
personalistický	personalistický	k2eAgMnSc1d1	personalistický
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Patočka	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
filosof	filosof	k1gMnSc1	filosof
</s>
</p>
<p>
<s>
Henri	Henri	k6eAd1	Henri
Poincaré	Poincarý	k2eAgFnPc1d1	Poincarý
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
</s>
</p>
<p>
<s>
Roland	Roland	k1gInSc1	Roland
Pröll	Pröll	k1gInSc1	Pröll
<g/>
,	,	kIx,	,
pianista	pianista	k1gMnSc1	pianista
</s>
</p>
<p>
<s>
Raymond	Raymond	k1gMnSc1	Raymond
Queneau	Queneaus	k1gInSc2	Queneaus
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Raňkův	Raňkův	k2eAgMnSc1d1	Raňkův
z	z	k7c2	z
Ježova	Ježův	k2eAgInSc2d1	Ježův
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
</s>
</p>
<p>
<s>
Ibrahim	Ibrahim	k1gInSc1	Ibrahim
Rugova	Rugov	k1gInSc2	Rugov
<g/>
,	,	kIx,	,
kosovský	kosovský	k2eAgMnSc1d1	kosovský
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Françoise	Françoise	k1gFnSc1	Françoise
Saganová	Saganová	k1gFnSc1	Saganová
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Léopold	Léopold	k1gMnSc1	Léopold
Sédar	Sédar	k1gMnSc1	Sédar
Senghor	Senghor	k1gMnSc1	Senghor
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
Senegalu	Senegal	k1gInSc2	Senegal
</s>
</p>
<p>
<s>
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Joseph	Joseph	k1gMnSc1	Joseph
Sieyè	Sieyè	k1gMnSc1	Sieyè
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
revolucionář	revolucionář	k1gMnSc1	revolucionář
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Teilhard	Teilhard	k1gMnSc1	Teilhard
de	de	k?	de
Chardin	Chardin	k2eAgMnSc1d1	Chardin
<g/>
,	,	kIx,	,
geolog	geolog	k1gMnSc1	geolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
</s>
</p>
<p>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Trudeau	Trudeaum	k1gNnSc6	Trudeaum
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
premiér	premiér	k1gMnSc1	premiér
</s>
</p>
<p>
<s>
Anne	Anne	k6eAd1	Anne
Robert	Robert	k1gMnSc1	Robert
Jacques	Jacques	k1gMnSc1	Jacques
Turgot	Turgot	k1gMnSc1	Turgot
<g/>
,	,	kIx,	,
státník	státník	k1gMnSc1	státník
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
encyklopedista	encyklopedista	k1gMnSc1	encyklopedista
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Turner	turner	k1gMnSc1	turner
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
premiér	premiér	k1gMnSc1	premiér
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
z	z	k7c2	z
Pauly	Paula	k1gFnSc2	Paula
<g/>
,	,	kIx,	,
kazatel	kazatel	k1gMnSc1	kazatel
a	a	k8xC	a
misionář	misionář	k1gMnSc1	misionář
</s>
</p>
<p>
<s>
François	François	k1gInSc1	François
Villon	Villon	k1gInSc1	Villon
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Elie	Elie	k6eAd1	Elie
Wiesel	Wiesel	k1gMnSc1	Wiesel
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
