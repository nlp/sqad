<s>
Vracov	Vracov	k1gInSc1	Vracov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wratzow	Wratzow	k1gFnSc1	Wratzow
<g/>
,	,	kIx,	,
hist.	hist.	k?	hist.
též	též	k9	též
Pracov	Pracov	k1gInSc1	Pracov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
8	[number]	k4	8
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
4	[number]	k4	4
484	[number]	k4	484
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
1	[number]	k4	1
565	[number]	k4	565
domech	dům	k1gInPc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
osadě	osada	k1gFnSc6	osada
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
nynějšího	nynější	k2eAgNnSc2d1	nynější
města	město	k1gNnSc2	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1201	[number]	k4	1201
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1230	[number]	k4	1230
a	a	k8xC	a
1240	[number]	k4	1240
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
místní	místní	k2eAgInSc1d1	místní
kostel	kostel	k1gInSc1	kostel
<g/>
;	;	kIx,	;
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1565	[number]	k4	1565
přestavěn	přestavět	k5eAaPmNgInS	přestavět
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1604	[number]	k4	1604
a	a	k8xC	a
1612	[number]	k4	1612
doplněn	doplněn	k2eAgInSc4d1	doplněn
zvony	zvon	k1gInPc4	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Vracov	Vracov	k1gInSc1	Vracov
byl	být	k5eAaImAgInS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
lokálním	lokální	k2eAgInSc7d1	lokální
centrem	centr	k1gInSc7	centr
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Břeclavi	Břeclav	k1gFnSc2	Břeclav
přestěhovány	přestěhován	k2eAgInPc1d1	přestěhován
i	i	k8xC	i
krajské	krajský	k2eAgInPc1d1	krajský
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
a	a	k8xC	a
břeclavský	břeclavský	k2eAgInSc1d1	břeclavský
kraj	kraj	k1gInSc1	kraj
se	se	k3xPyFc4	se
po	po	k7c4	po
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
nazýval	nazývat	k5eAaImAgInS	nazývat
krajem	krajem	k6eAd1	krajem
vracovským	vracovský	k2eAgInSc7d1	vracovský
(	(	kIx(	(
<g/>
pracovským	pracovský	k2eAgInSc7d1	pracovský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1274	[number]	k4	1274
je	být	k5eAaImIp3nS	být
Vracov	Vracov	k1gInSc1	Vracov
nazýván	nazývat	k5eAaImNgInS	nazývat
městečkem	městečko	k1gNnSc7	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
obce	obec	k1gFnSc2	obec
začal	začít	k5eAaPmAgInS	začít
klesat	klesat	k5eAaImF	klesat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
okolo	okolo	k7c2	okolo
nově	nově	k6eAd1	nově
založeného	založený	k2eAgInSc2d1	založený
hradu	hrad	k1gInSc2	hrad
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
městečko	městečko	k1gNnSc1	městečko
Bzenec	Bzenec	k1gInSc1	Bzenec
<g/>
.	.	kIx.	.
</s>
<s>
Krajské	krajský	k2eAgInPc1d1	krajský
úřady	úřad	k1gInPc1	úřad
se	se	k3xPyFc4	se
přesunuly	přesunout	k5eAaPmAgInP	přesunout
do	do	k7c2	do
Bzence	Bzenec	k1gInSc2	Bzenec
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1335	[number]	k4	1335
byl	být	k5eAaImAgInS	být
Vracov	Vracov	k1gInSc1	Vracov
přičleněn	přičleněn	k2eAgInSc1d1	přičleněn
k	k	k7c3	k
bzeneckému	bzenecký	k2eAgNnSc3d1	Bzenecké
panství	panství	k1gNnSc3	panství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1518	[number]	k4	1518
obdržel	obdržet	k5eAaPmAgInS	obdržet
Vracov	Vracov	k1gInSc1	Vracov
právo	právo	k1gNnSc1	právo
várečné	várečný	k2eAgNnSc1d1	várečné
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
městysem	městys	k1gInSc7	městys
<g/>
.	.	kIx.	.
</s>
<s>
Urbář	urbář	k1gInSc1	urbář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
podrobností	podrobnost	k1gFnPc2	podrobnost
o	o	k7c6	o
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
např.	např.	kA	např.
výši	výše	k1gFnSc4	výše
odváděných	odváděný	k2eAgInPc2d1	odváděný
poplatků	poplatek	k1gInPc2	poplatek
z	z	k7c2	z
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
vinic	vinice	k1gFnPc2	vinice
(	(	kIx(	(
<g/>
130	[number]	k4	130
rýnských	rýnské	k1gNnPc2	rýnské
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Urbář	urbář	k1gInSc1	urbář
také	také	k9	také
uvádí	uvádět	k5eAaImIp3nS	uvádět
několik	několik	k4yIc4	několik
příjmení	příjmení	k1gNnPc2	příjmení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
začátkem	začátkem	k7c2	začátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
-	-	kIx~	-
Mívalt	Mívalt	k1gMnSc1	Mívalt
<g/>
,	,	kIx,	,
Nikl	Nikl	k1gMnSc1	Nikl
nebo	nebo	k8xC	nebo
Repík	Repík	k1gMnSc1	Repík
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
přišel	přijít	k5eAaPmAgInS	přijít
Vracov	Vracov	k1gInSc1	Vracov
o	o	k7c4	o
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
kolonizaci	kolonizace	k1gFnSc3	kolonizace
a	a	k8xC	a
přírůstku	přírůstek	k1gInSc2	přírůstek
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
Vracov	Vracov	k1gInSc1	Vracov
povýšen	povýšit	k5eAaPmNgInS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1656	[number]	k4	1656
měl	mít	k5eAaImAgInS	mít
Vracov	Vracov	k1gInSc1	Vracov
pouhých	pouhý	k2eAgInPc2d1	pouhý
114	[number]	k4	114
domů	dům	k1gInPc2	dům
a	a	k8xC	a
740	[number]	k4	740
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
však	však	k9	však
město	město	k1gNnSc4	město
postupně	postupně	k6eAd1	postupně
rostlo	růst	k5eAaImAgNnS	růst
až	až	k9	až
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
cca	cca	kA	cca
<g/>
.	.	kIx.	.
4	[number]	k4	4
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
2163	[number]	k4	2163
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
2321	[number]	k4	2321
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
48	[number]	k4	48
<g/>
%	%	kIx~	%
ku	k	k7c3	k
52	[number]	k4	52
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásila	hlásit	k5eAaImAgFnS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
nebo	nebo	k8xC	nebo
moravské	moravský	k2eAgFnSc3d1	Moravská
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
národnost	národnost	k1gFnSc1	národnost
neuvedla	uvést	k5eNaPmAgFnS	uvést
<g/>
;	;	kIx,	;
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
národnostní	národnostní	k2eAgFnSc7d1	národnostní
menšinou	menšina	k1gFnSc7	menšina
byli	být	k5eAaImAgMnP	být
Slováci	Slovák	k1gMnPc1	Slovák
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
36	[number]	k4	36
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
1067	[number]	k4	1067
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
k	k	k7c3	k
náboženské	náboženský	k2eAgFnSc3d1	náboženská
víře	víra	k1gFnSc3	víra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
902	[number]	k4	902
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
2	[number]	k4	2
208	[number]	k4	208
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
obyvatel	obyvatel	k1gMnPc2	obyvatel
jich	on	k3xPp3gMnPc2	on
666	[number]	k4	666
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
do	do	k7c2	do
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Dungel	Dungel	k1gMnSc1	Dungel
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
Josef	Josef	k1gMnSc1	Josef
Somr	Somr	k1gMnSc1	Somr
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Filip	Filip	k1gMnSc1	Filip
Prechtl	Prechtl	k1gMnSc1	Prechtl
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
skupiny	skupina	k1gFnSc2	skupina
Cimballica	Cimballica	k1gMnSc1	Cimballica
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kořínek	Kořínek	k1gMnSc1	Kořínek
<g/>
,	,	kIx,	,
operní	operní	k2eAgMnSc1d1	operní
zpěvák	zpěvák	k1gMnSc1	zpěvák
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
-	-	kIx~	-
raně	raně	k6eAd1	raně
gotická	gotický	k2eAgFnSc1d1	gotická
stavba	stavba	k1gFnSc1	stavba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
burgundsko-cisterciácké	burgundskoisterciácký	k2eAgFnSc2d1	burgundsko-cisterciácký
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
jeho	on	k3xPp3gInSc4	on
presbytář	presbytář	k1gInSc4	presbytář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1240	[number]	k4	1240
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
barokní	barokní	k2eAgFnSc3d1	barokní
přestavbě	přestavba	k1gFnSc3	přestavba
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Schweigla	Schweigl	k1gMnSc2	Schweigl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
kostelem	kostel	k1gInSc7	kostel
křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
se	s	k7c7	s
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
vitrážemi	vitráž	k1gFnPc7	vitráž
a	a	k8xC	a
sochou	socha	k1gFnSc7	socha
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
ulice	ulice	k1gFnSc1	ulice
-	-	kIx~	-
nejstarší	starý	k2eAgFnSc7d3	nejstarší
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
1604	[number]	k4	1604
<g/>
)	)	kIx)	)
Baráky	barák	k1gInPc1	barák
-	-	kIx~	-
skupina	skupina	k1gFnSc1	skupina
domů	dům	k1gInPc2	dům
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Habánské	habánský	k2eAgInPc4d1	habánský
vinné	vinný	k2eAgInPc4d1	vinný
sklepy	sklep	k1gInPc4	sklep
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
několik	několik	k4yIc4	několik
folklórních	folklórní	k2eAgInPc2d1	folklórní
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
folklórní	folklórní	k2eAgInSc1d1	folklórní
soubor	soubor	k1gInSc1	soubor
Marýnka	marýnka	k1gFnSc1	marýnka
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
folklórním	folklórní	k2eAgInSc7d1	folklórní
souborem	soubor	k1gInSc7	soubor
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
Lipina	lipina	k1gFnSc1	lipina
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
dětský	dětský	k2eAgInSc1d1	dětský
soubor	soubor	k1gInSc1	soubor
Lipinka	Lipinka	k1gFnSc1	Lipinka
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladším	mladý	k2eAgNnSc7d3	nejmladší
folklórním	folklórní	k2eAgNnSc7d1	folklórní
sdružením	sdružení	k1gNnSc7	sdružení
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
soubor	soubor	k1gInSc4	soubor
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
Vracovjan	Vracovjana	k1gFnPc2	Vracovjana
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
i	i	k9	i
dechová	dechový	k2eAgFnSc1d1	dechová
hudba	hudba	k1gFnSc1	hudba
Vracovjáci	Vracovják	k1gMnPc5	Vracovják
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
třetí	třetí	k4xOgInSc4	třetí
říjnový	říjnový	k2eAgInSc4d1	říjnový
víkend	víkend	k1gInSc4	víkend
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
konají	konat	k5eAaImIp3nP	konat
tradiční	tradiční	k2eAgInPc1d1	tradiční
krojované	krojovaný	k2eAgInPc1d1	krojovaný
císařské	císařský	k2eAgInPc1d1	císařský
hody	hod	k1gInPc1	hod
"	"	kIx"	"
<g/>
s	s	k7c7	s
věncem	věnec	k1gInSc7	věnec
a	a	k8xC	a
káčerem	káčer	k1gMnSc7	káčer
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počtem	počet	k1gInSc7	počet
krojovaných	krojovaný	k2eAgInPc2d1	krojovaný
(	(	kIx(	(
<g/>
660	[number]	k4	660
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgInPc4d3	veliký
hody	hod	k1gInPc4	hod
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hodovém	hodový	k2eAgInSc6d1	hodový
průvodu	průvod	k1gInSc6	průvod
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
566	[number]	k4	566
účastníky	účastník	k1gMnPc4	účastník
ustanoven	ustanoven	k2eAgInSc4d1	ustanoven
oficiální	oficiální	k2eAgInSc4d1	oficiální
český	český	k2eAgInSc4d1	český
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
krojovaných	krojovaný	k2eAgInPc2d1	krojovaný
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
státní	státní	k2eAgFnSc1d1	státní
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
prvního	první	k4xOgInSc2	první
i	i	k8xC	i
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
budově	budova	k1gFnSc6	budova
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
sídlí	sídlet	k5eAaImIp3nS	sídlet
městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
budovy	budova	k1gFnSc2	budova
radnice	radnice	k1gFnSc1	radnice
slouží	sloužit	k5eAaImIp3nS	sloužit
pobočce	pobočka	k1gFnSc3	pobočka
České	český	k2eAgFnSc2d1	Česká
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
dvorním	dvorní	k2eAgInSc6d1	dvorní
traktu	trakt	k1gInSc6	trakt
jsou	být	k5eAaImIp3nP	být
ordinace	ordinace	k1gFnPc1	ordinace
praktických	praktický	k2eAgMnPc2d1	praktický
a	a	k8xC	a
zubních	zubní	k2eAgMnPc2d1	zubní
lékařů	lékař	k1gMnPc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
dům	dům	k1gInSc1	dům
s	s	k7c7	s
divadelním	divadelní	k2eAgInSc7d1	divadelní
a	a	k8xC	a
estrádním	estrádní	k2eAgInSc7d1	estrádní
sálem	sál	k1gInSc7	sál
<g/>
.	.	kIx.	.
</s>
<s>
Kino	kino	k1gNnSc1	kino
není	být	k5eNaImIp3nS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
digitální	digitální	k2eAgFnSc7d1	digitální
promítací	promítací	k2eAgFnSc7d1	promítací
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
bez	bez	k7c2	bez
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
stojí	stát	k5eAaImIp3nS	stát
katolický	katolický	k2eAgInSc1d1	katolický
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Okružní	okružní	k2eAgMnSc1d1	okružní
pak	pak	k6eAd1	pak
Husův	Husův	k2eAgInSc4d1	Husův
sbor	sbor	k1gInSc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
areál	areál	k1gInSc1	areál
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
města	město	k1gNnSc2	město
s	s	k7c7	s
tenisovými	tenisový	k2eAgInPc7d1	tenisový
kurty	kurt	k1gInPc7	kurt
a	a	k8xC	a
fotbalovým	fotbalový	k2eAgNnSc7d1	fotbalové
hřištěm	hřiště	k1gNnSc7	hřiště
klubu	klub	k1gInSc2	klub
FC	FC	kA	FC
Vracov	Vracov	k1gInSc1	Vracov
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1	veřejné
koupaliště	koupaliště	k1gNnSc1	koupaliště
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
bazény	bazén	k1gInPc7	bazén
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Vracov	Vracov	k1gInSc1	Vracov
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
hranici	hranice	k1gFnSc6	hranice
lesní	lesní	k2eAgFnSc2d1	lesní
oblasti	oblast	k1gFnSc2	oblast
Doubrava	Doubrava	k1gFnSc1	Doubrava
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc1d1	zvané
též	též	k9	též
Moravská	moravský	k2eAgFnSc1d1	Moravská
Sahara	Sahara	k1gFnSc1	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
obcemi	obec	k1gFnPc7	obec
Bzenec	Bzenec	k1gInSc1	Bzenec
<g/>
,	,	kIx,	,
Vlkoš	Vlkoš	k1gMnSc1	Vlkoš
a	a	k8xC	a
Vacenovice	Vacenovice	k1gFnSc1	Vacenovice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
je	být	k5eAaImIp3nS	být
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
zaplavením	zaplavení	k1gNnSc7	zaplavení
naleziště	naleziště	k1gNnSc2	naleziště
rašeliny	rašelina	k1gFnSc2	rašelina
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
Vracovský	Vracovský	k2eAgInSc1d1	Vracovský
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
54	[number]	k4	54
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Vracov	Vracov	k1gInSc1	Vracov
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
340	[number]	k4	340
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgNnSc1d1	spojující
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
zde	zde	k6eAd1	zde
zastavovaly	zastavovat	k5eAaImAgInP	zastavovat
všechny	všechen	k3xTgInPc1	všechen
osobní	osobní	k2eAgInPc1d1	osobní
vlaky	vlak	k1gInPc1	vlak
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
spěšných	spěšný	k2eAgNnPc2d1	spěšné
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
obcemi	obec	k1gFnPc7	obec
je	být	k5eAaImIp3nS	být
zajišťováno	zajišťován	k2eAgNnSc1d1	zajišťováno
také	také	k9	také
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
685	[number]	k4	685
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
<g/>
.	.	kIx.	.
</s>
