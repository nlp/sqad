<s>
Celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
hydrosféry	hydrosféra	k1gFnSc2	hydrosféra
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1,4	[number]	k4	1,4
<g/>
×	×	k?	×
<g/>
1021	[number]	k4	1021
kg	kg	kA	kg
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
0,023	[number]	k4	0,023
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
