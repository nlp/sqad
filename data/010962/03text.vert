<p>
<s>
Šíp	šíp	k1gInSc1	šíp
je	být	k5eAaImIp3nS	být
střela	střela	k1gFnSc1	střela
vystřelovaná	vystřelovaný	k2eAgFnSc1d1	vystřelovaná
lukem	luk	k1gInSc7	luk
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
nalezl	naleznout	k5eAaPmAgInS	naleznout
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
kultur	kultura	k1gFnPc2	kultura
i	i	k8xC	i
sportovní	sportovní	k2eAgFnSc3d1	sportovní
lukostřelbě	lukostřelba	k1gFnSc3	lukostřelba
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
byl	být	k5eAaImAgInS	být
převažujícím	převažující	k2eAgInSc7d1	převažující
typem	typ	k1gInSc7	typ
projektilu	projektil	k1gInSc2	projektil
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
šípu	šíp	k1gInSc2	šíp
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
heraldice	heraldika	k1gFnSc6	heraldika
(	(	kIx(	(
<g/>
heraldická	heraldický	k2eAgFnSc1d1	heraldická
figura	figura	k1gFnSc1	figura
–	–	k?	–
samotný	samotný	k2eAgInSc1d1	samotný
nebo	nebo	k8xC	nebo
zavinutá	zavinutý	k2eAgFnSc1d1	zavinutá
střela	střela	k1gFnSc1	střela
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
odřivous	odřivous	k1gInSc1	odřivous
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
hovoru	hovor	k1gInSc6	hovor
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
rčení	rčení	k1gNnSc1	rčení
rychlý	rychlý	k2eAgInSc4d1	rychlý
jako	jako	k9	jako
šíp	šíp	k1gInSc4	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Šíp	šíp	k1gInSc1	šíp
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
známého	známý	k2eAgNnSc2d1	známé
loga	logo	k1gNnSc2	logo
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
Škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
šípu	šíp	k1gInSc2	šíp
==	==	k?	==
</s>
</p>
<p>
<s>
Šíp	šíp	k1gInSc1	šíp
je	být	k5eAaImIp3nS	být
podlouhlého	podlouhlý	k2eAgInSc2d1	podlouhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
části	část	k1gFnPc1	část
šípu	šíp	k1gInSc2	šíp
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
přiloženém	přiložený	k2eAgInSc6d1	přiložený
obrázku	obrázek	k1gInSc6	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
šíp	šíp	k1gInSc4	šíp
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
potřeby	potřeba	k1gFnSc2	potřeba
zásahu	zásah	k1gInSc2	zásah
cíle	cíl	k1gInSc2	cíl
a	a	k8xC	a
způsobení	způsobení	k1gNnSc2	způsobení
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
zásahu	zásah	k1gInSc2	zásah
je	být	k5eAaImIp3nS	být
požadováno	požadován	k2eAgNnSc1d1	požadováno
aby	aby	kYmCp3nP	aby
všechny	všechen	k3xTgInPc4	všechen
šípy	šíp	k1gInPc4	šíp
používané	používaný	k2eAgInPc4d1	používaný
pro	pro	k7c4	pro
střelbu	střelba	k1gFnSc4	střelba
měly	mít	k5eAaImAgFnP	mít
shodné	shodný	k2eAgFnPc1d1	shodná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgFnPc7d1	důležitá
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
šípu	šíp	k1gInSc2	šíp
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
a	a	k8xC	a
tuhost	tuhost	k1gFnSc1	tuhost
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
náročnosti	náročnost	k1gFnSc3	náročnost
výroby	výroba	k1gFnSc2	výroba
dobrého	dobrý	k2eAgInSc2d1	dobrý
šípu	šíp	k1gInSc2	šíp
se	se	k3xPyFc4	se
šípy	šíp	k1gInPc1	šíp
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
aby	aby	kYmCp3nS	aby
šíp	šíp	k1gInSc4	šíp
vydržel	vydržet	k5eAaPmAgMnS	vydržet
opakované	opakovaný	k2eAgNnSc1d1	opakované
použití	použití	k1gNnSc1	použití
a	a	k8xC	a
nezničil	zničit	k5eNaPmAgMnS	zničit
se	se	k3xPyFc4	se
například	například	k6eAd1	například
při	při	k7c6	při
zásahu	zásah	k1gInSc6	zásah
do	do	k7c2	do
terče	terč	k1gFnSc2	terč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dřík	dřík	k1gInSc1	dřík
šípu	šíp	k1gInSc2	šíp
===	===	k?	===
</s>
</p>
<p>
<s>
Též	též	k9	též
šípové	šípový	k2eAgNnSc1d1	šípové
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
násada	násada	k1gFnSc1	násada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tyč	tyč	k1gFnSc4	tyč
kruhového	kruhový	k2eAgInSc2d1	kruhový
průřezu	průřez	k1gInSc2	průřez
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Cedr	cedr	k1gInSc1	cedr
-	-	kIx~	-
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
stabilní	stabilní	k2eAgFnSc1d1	stabilní
hustota	hustota	k1gFnSc1	hustota
<g/>
,	,	kIx,	,
trvanlivé	trvanlivý	k2eAgNnSc1d1	trvanlivé
<g/>
,	,	kIx,	,
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
pružné	pružný	k2eAgNnSc1d1	pružné
</s>
</p>
<p>
<s>
Jedle	jedle	k1gFnSc1	jedle
-	-	kIx~	-
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hustota	hustota	k1gFnSc1	hustota
než	než	k8xS	než
cedr	cedr	k1gInSc1	cedr
<g/>
,	,	kIx,	,
hůře	zle	k6eAd2	zle
převezme	převzít	k5eAaPmIp3nS	převzít
energii	energie	k1gFnSc4	energie
při	při	k7c6	při
výstřelu	výstřel	k1gInSc6	výstřel
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
trvanlivé	trvanlivý	k2eAgFnPc1d1	trvanlivá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
-	-	kIx~	-
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
cedr	cedr	k1gInSc1	cedr
<g/>
,	,	kIx,	,
trvanlivé	trvanlivý	k2eAgFnPc1d1	trvanlivá
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
používané	používaný	k2eAgInPc4d1	používaný
<g/>
,	,	kIx,	,
tuhé	tuhý	k2eAgInPc4d1	tuhý
šípy	šíp	k1gInPc4	šíp
</s>
</p>
<p>
<s>
Tropická	tropický	k2eAgFnSc1d1	tropická
bříza	bříza	k1gFnSc1	bříza
-	-	kIx~	-
těžký	těžký	k2eAgInSc1d1	těžký
šíp	šíp	k1gInSc1	šíp
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
(	(	kIx(	(
<g/>
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
povoleno	povolen	k2eAgNnSc1d1	povoleno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
energii	energie	k1gFnSc4	energie
</s>
</p>
<p>
<s>
Jasan	jasan	k1gInSc1	jasan
-	-	kIx~	-
pevné	pevný	k2eAgInPc1d1	pevný
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgInPc1d1	těžký
šípy	šíp	k1gInPc1	šíp
</s>
</p>
<p>
<s>
Smrk	smrk	k1gInSc1	smrk
-	-	kIx~	-
lehké	lehký	k2eAgNnSc1d1	lehké
<g/>
,	,	kIx,	,
dostatečně	dostatečně	k6eAd1	dostatečně
tuhé	tuhý	k2eAgNnSc1d1	tuhé
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Levnější	levný	k2eAgFnSc1d2	levnější
náhrada	náhrada	k1gFnSc1	náhrada
cedru	cedr	k1gInSc2	cedr
<g/>
.	.	kIx.	.
<g/>
Problémem	problém	k1gInSc7	problém
dřeva	dřevo	k1gNnSc2	dřevo
je	být	k5eAaImIp3nS	být
variabilita	variabilita	k1gFnSc1	variabilita
jeho	jeho	k3xOp3gFnPc2	jeho
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nemají	mít	k5eNaImIp3nP	mít
společně	společně	k6eAd1	společně
používané	používaný	k2eAgInPc4d1	používaný
šípy	šíp	k1gInPc4	šíp
shodné	shodný	k2eAgFnSc2d1	shodná
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
negativnímu	negativní	k2eAgNnSc3d1	negativní
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
přesnosti	přesnost	k1gFnSc2	přesnost
střelby	střelba	k1gFnSc2	střelba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novější	nový	k2eAgInPc1d2	novější
materiály	materiál	k1gInPc1	materiál
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
trubek	trubka	k1gFnPc2	trubka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
dutinou	dutina	k1gFnSc7	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
nových	nový	k2eAgInPc2d1	nový
materiálů	materiál	k1gInPc2	materiál
je	být	k5eAaImIp3nS	být
jednodužší	jednodužší	k2eAgNnSc4d1	jednodužší
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
stálých	stálý	k2eAgFnPc2d1	stálá
vlastností	vlastnost	k1gFnPc2	vlastnost
dříků	dřík	k1gInPc2	dřík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dural	dural	k1gInSc1	dural
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
slitiny	slitina	k1gFnPc1	slitina
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
levná	levný	k2eAgNnPc4d1	levné
těla	tělo	k1gNnPc4	tělo
šípů	šíp	k1gInPc2	šíp
s	s	k7c7	s
vyrovnanými	vyrovnaný	k2eAgFnPc7d1	vyrovnaná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
trvalého	trvalý	k2eAgNnSc2d1	trvalé
ohnutí	ohnutí	k1gNnSc2	ohnutí
při	při	k7c6	při
překonání	překonání	k1gNnSc6	překonání
meze	mez	k1gFnSc2	mez
pružnosti	pružnost	k1gFnSc2	pružnost
například	například	k6eAd1	například
při	při	k7c6	při
špatném	špatný	k2eAgInSc6d1	špatný
zásahu	zásah	k1gInSc6	zásah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sklolamináty	sklolaminát	k1gInPc1	sklolaminát
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
již	již	k6eAd1	již
spíše	spíše	k9	spíše
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
karbon	karbon	k1gInSc1	karbon
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyrábět	vyrábět	k5eAaImF	vyrábět
velmi	velmi	k6eAd1	velmi
pevné	pevný	k2eAgInPc4d1	pevný
a	a	k8xC	a
lehké	lehký	k2eAgInPc4d1	lehký
šípy	šíp	k1gInPc4	šíp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměry	průměr	k1gInPc1	průměr
šípů	šíp	k1gInPc2	šíp
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
sjednocovány	sjednocován	k2eAgFnPc1d1	sjednocována
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
používané	používaný	k2eAgInPc1d1	používaný
jsou	být	k5eAaImIp3nP	být
průměry	průměr	k1gInPc1	průměr
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
32	[number]	k4	32
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
32	[number]	k4	32
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
"	"	kIx"	"
a	a	k8xC	a
32	[number]	k4	32
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
extrémě	extrémě	k6eAd1	extrémě
silné	silný	k2eAgInPc4d1	silný
luky	luk	k1gInPc4	luk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Končík	končík	k1gInSc1	končík
šípu	šíp	k1gInSc2	šíp
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
drážku	drážka	k1gFnSc4	drážka
na	na	k7c6	na
zadním	zadní	k2eAgInSc6d1	zadní
konci	konec	k1gInSc6	konec
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
šíp	šíp	k1gInSc1	šíp
silně	silně	k6eAd1	silně
namáhán	namáhat	k5eAaImNgInS	namáhat
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
způsob	způsob	k1gInSc1	způsob
spočíval	spočívat	k5eAaImAgInS	spočívat
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
drážky	drážka	k1gFnSc2	drážka
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
dříku	dřík	k1gInSc2	dřík
šípu	šíp	k1gInSc2	šíp
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tělo	tělo	k1gNnSc1	tělo
šípu	šíp	k1gInSc2	šíp
se	se	k3xPyFc4	se
zpevňovalo	zpevňovat	k5eAaImAgNnS	zpevňovat
omotávkou	omotávka	k1gFnSc7	omotávka
proti	proti	k7c3	proti
rozštípnutí	rozštípnutí	k1gNnSc3	rozštípnutí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
samostatné	samostatný	k2eAgInPc1d1	samostatný
končíky	končík	k1gInPc1	končík
z	z	k7c2	z
tvrdšího	tvrdý	k2eAgNnSc2d2	tvrdší
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
lépe	dobře	k6eAd2	dobře
odolávaly	odolávat	k5eAaImAgFnP	odolávat
opakovanému	opakovaný	k2eAgNnSc3d1	opakované
namáhání	namáhání	k1gNnSc3	namáhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plastové	plastový	k2eAgInPc1d1	plastový
končíky	končík	k1gInPc1	končík
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
vytlačily	vytlačit	k5eAaPmAgFnP	vytlačit
původní	původní	k2eAgFnSc4d1	původní
výrobu	výroba	k1gFnSc4	výroba
drážek	drážka	k1gFnPc2	drážka
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
dříků	dřík	k1gInPc2	dřík
šípů	šíp	k1gInPc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Plastové	plastový	k2eAgInPc1d1	plastový
končíky	končík	k1gInPc1	končík
i	i	k8xC	i
jejich	jejich	k3xOp3gFnPc1	jejich
drážky	drážka	k1gFnPc1	drážka
mají	mít	k5eAaImIp3nP	mít
stabilní	stabilní	k2eAgInSc4d1	stabilní
tvar	tvar	k1gInSc4	tvar
i	i	k8xC	i
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
dříků	dřík	k1gInPc2	dřík
se	se	k3xPyFc4	se
plastové	plastový	k2eAgInPc1d1	plastový
končíky	končík	k1gInPc1	končík
obvykle	obvykle	k6eAd1	obvykle
lepí	lepit	k5eAaImIp3nP	lepit
na	na	k7c4	na
kónus	kónus	k1gInSc4	kónus
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dříku	dřík	k1gInSc2	dřík
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
stejné	stejný	k2eAgFnPc4d1	stejná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
šípů	šíp	k1gInPc2	šíp
s	s	k7c7	s
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
násadou	násada	k1gFnSc7	násada
je	být	k5eAaImIp3nS	být
umístění	umístění	k1gNnSc1	umístění
končíku	končík	k1gInSc2	končík
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vlákna	vlákno	k1gNnPc1	vlákno
dřeva	dřevo	k1gNnSc2	dřevo
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Křidélka	křidélko	k1gNnSc2	křidélko
šípu	šíp	k1gInSc2	šíp
===	===	k?	===
</s>
</p>
<p>
<s>
Nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
též	též	k9	též
kormidla	kormidlo	k1gNnSc2	kormidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
vlastné	vlastný	k2eAgNnSc4d1	vlastné
o	o	k7c4	o
letové	letový	k2eAgFnPc4d1	letová
stabilizační	stabilizační	k2eAgFnPc4d1	stabilizační
plochy	plocha	k1gFnPc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
bylo	být	k5eAaImAgNnS	být
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
schopnost	schopnost	k1gFnSc1	schopnost
deformace	deformace	k1gFnSc2	deformace
při	při	k7c6	při
dotyku	dotyk	k1gInSc6	dotyk
kormidla	kormidlo	k1gNnSc2	kormidlo
šípu	šíp	k1gInSc2	šíp
s	s	k7c7	s
lučištěm	lučiště	k1gNnSc7	lučiště
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nově	nově	k6eAd1	nově
používaných	používaný	k2eAgNnPc2d1	používané
kormidel	kormidlo	k1gNnPc2	kormidlo
z	z	k7c2	z
plastů	plast	k1gInPc2	plast
bývá	bývat	k5eAaImIp3nS	bývat
toto	tento	k3xDgNnSc1	tento
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
menší	malý	k2eAgNnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tři	tři	k4xCgNnPc1	tři
kormidla	kormidlo	k1gNnPc1	kormidlo
lepená	lepený	k2eAgFnSc1d1	lepená
k	k	k7c3	k
šípu	šíp	k1gInSc3	šíp
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
úhlech	úhel	k1gInPc6	úhel
120	[number]	k4	120
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Kormidla	kormidlo	k1gNnPc1	kormidlo
jsou	být	k5eAaImIp3nP	být
stejná	stejný	k2eAgNnPc1d1	stejné
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
orienace	orienace	k1gFnSc1	orienace
jejich	jejich	k3xOp3gInPc2	jejich
úhlů	úhel	k1gInPc2	úhel
vůči	vůči	k7c3	vůči
drážce	drážka	k1gFnSc3	drážka
v	v	k7c6	v
končíku	končík	k1gInSc6	končík
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
kormidel	kormidlo	k1gNnPc2	kormidlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
90	[number]	k4	90
<g/>
°	°	k?	°
vůči	vůči	k7c3	vůči
této	tento	k3xDgFnSc3	tento
drážce	drážka	k1gFnSc3	drážka
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
odlišeno	odlišen	k2eAgNnSc4d1	odlišeno
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Šíp	Šíp	k1gMnSc1	Šíp
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
zakládá	zakládat	k5eAaImIp3nS	zakládat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
toto	tento	k3xDgNnSc1	tento
kormidlo	kormidlo	k1gNnSc1	kormidlo
bylo	být	k5eAaImAgNnS	být
odvráceno	odvrátit	k5eAaPmNgNnS	odvrátit
od	od	k7c2	od
lučiště	lučiště	k1gNnSc2	lučiště
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
také	také	k9	také
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
letu	let	k1gInSc2	let
šípu	šíp	k1gInSc2	šíp
při	při	k7c6	při
případném	případný	k2eAgInSc6d1	případný
dotyku	dotyk	k1gInSc6	dotyk
kormidel	kormidlo	k1gNnPc2	kormidlo
s	s	k7c7	s
lučištěm	lučiště	k1gNnSc7	lučiště
při	při	k7c6	při
výstřelu	výstřel	k1gInSc6	výstřel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hrot	hrot	k1gInSc1	hrot
šípu	šíp	k1gInSc2	šíp
===	===	k?	===
</s>
</p>
<p>
<s>
Hrot	hrot	k1gInSc1	hrot
šípu	šíp	k1gInSc2	šíp
bývá	bývat	k5eAaImIp3nS	bývat
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
materiálu	materiál	k1gInSc2	materiál
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
těleso	těleso	k1gNnSc1	těleso
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
jsou	být	k5eAaImIp3nP	být
kamenné	kamenný	k2eAgInPc1d1	kamenný
hroty	hrot	k1gInPc1	hrot
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
kovové	kovový	k2eAgInPc1d1	kovový
<g/>
.	.	kIx.	.
</s>
<s>
Kov	kov	k1gInSc1	kov
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nejobvyklejším	obvyklý	k2eAgInSc7d3	nejobvyklejší
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
hrotů	hrot	k1gInPc2	hrot
šípů	šíp	k1gInPc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
velká	velký	k2eAgFnSc1d1	velká
škála	škála	k1gFnSc1	škála
hrotů	hrot	k1gInPc2	hrot
podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
použití	použití	k1gNnSc2	použití
šípů	šíp	k1gInPc2	šíp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
vlastností	vlastnost	k1gFnSc7	vlastnost
hrotů	hrot	k1gInPc2	hrot
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
váha	váha	k1gFnSc1	váha
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
hrotu	hrot	k1gInSc2	hrot
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
šípu	šíp	k1gInSc2	šíp
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
stabilitu	stabilita	k1gFnSc4	stabilita
jeho	on	k3xPp3gInSc2	on
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
jsou	být	k5eAaImIp3nP	být
sjednoceny	sjednocen	k2eAgFnPc1d1	sjednocena
váhy	váha	k1gFnPc1	váha
hrotů	hrot	k1gInPc2	hrot
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
průměry	průměr	k1gInPc4	průměr
násad	násada	k1gFnPc2	násada
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
hrotu	hrot	k1gInSc2	hrot
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
šípu	šíp	k1gInSc2	šíp
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
jednotce	jednotka	k1gFnSc6	jednotka
grain	grain	k1gInSc4	grain
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
speciálních	speciální	k2eAgInPc2d1	speciální
hrotů	hrot	k1gInPc2	hrot
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
váha	váha	k1gFnSc1	váha
lišit	lišit	k5eAaImF	lišit
výrazněji	výrazně	k6eAd2	výrazně
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
hrotu	hrot	k1gInSc2	hrot
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
vyvážení	vyvážení	k1gNnSc4	vyvážení
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
šípů	šíp	k1gInPc2	šíp
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
dobrých	dobrý	k2eAgInPc2d1	dobrý
výsledků	výsledek	k1gInPc2	výsledek
střelby	střelba	k1gFnSc2	střelba
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
vlastností	vlastnost	k1gFnSc7	vlastnost
rovné	rovný	k2eAgNnSc4d1	rovné
tělo	tělo	k1gNnSc4	tělo
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
šíp	šíp	k1gInSc1	šíp
mírně	mírně	k6eAd1	mírně
deformovaný	deformovaný	k2eAgInSc1d1	deformovaný
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc2	jeho
rovnání	rovnání	k1gNnSc2	rovnání
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
rovnání	rovnání	k1gNnSc4	rovnání
šípů	šíp	k1gInPc2	šíp
lze	lze	k6eAd1	lze
provádět	provádět	k5eAaImF	provádět
i	i	k9	i
v	v	k7c6	v
prstech	prst	k1gInPc6	prst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgFnPc2d1	různá
pomůcek	pomůcka	k1gFnPc2	pomůcka
pro	pro	k7c4	pro
rovnání	rovnání	k1gNnSc4	rovnání
šípů	šíp	k1gInPc2	šíp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Požadované	požadovaný	k2eAgFnPc1d1	požadovaná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
šípu	šíp	k1gInSc2	šíp
jsou	být	k5eAaImIp3nP	být
dány	dát	k5eAaPmNgFnP	dát
jednak	jednak	k8xC	jednak
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
lukem	luk	k1gInSc7	luk
-	-	kIx~	-
hlavně	hlavně	k9	hlavně
jeho	jeho	k3xOp3gFnSc7	jeho
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
konstrukcí	konstrukce	k1gFnSc7	konstrukce
a	a	k8xC	a
nátahovou	nátahový	k2eAgFnSc7d1	nátahový
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
délky	délka	k1gFnSc2	délka
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
vlastností	vlastnost	k1gFnSc7	vlastnost
šípu	šíp	k1gInSc2	šíp
jeho	jeho	k3xOp3gFnSc4	jeho
tuhost	tuhost	k1gFnSc4	tuhost
<g/>
.	.	kIx.	.
</s>
<s>
Tuhost	tuhost	k1gFnSc1	tuhost
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
lukostřeleckým	lukostřelecký	k2eAgInSc7d1	lukostřelecký
paradoxem	paradox	k1gInSc7	paradox
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInSc1	její
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
u	u	k7c2	u
moderních	moderní	k2eAgInPc2d1	moderní
luků	luk	k1gInPc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Tuhost	tuhost	k1gFnSc1	tuhost
šípu	šíp	k1gInSc2	šíp
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
spine	spin	k1gInSc5	spin
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
podle	podle	k7c2	podle
průhybu	průhyb	k1gInSc2	průhyb
šípu	šíp	k1gInSc2	šíp
zatíženého	zatížený	k2eAgInSc2d1	zatížený
definovanou	definovaný	k2eAgFnSc7d1	definovaná
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
podepřeného	podepřený	k2eAgNnSc2d1	podepřené
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
bodech	bod	k1gInPc6	bod
dané	daný	k2eAgFnSc2d1	daná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
jde	jít	k5eAaImIp3nS	jít
takto	takto	k6eAd1	takto
naměřit	naměřit	k5eAaBmF	naměřit
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
statický	statický	k2eAgInSc4d1	statický
spine	spin	k1gInSc5	spin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
ještě	ještě	k6eAd1	ještě
i	i	k9	i
dynamický	dynamický	k2eAgInSc4d1	dynamický
spine	spin	k1gInSc5	spin
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
měření	měření	k1gNnSc2	měření
je	být	k5eAaImIp3nS	být
komplikovanější	komplikovaný	k2eAgFnSc1d2	komplikovanější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
hodnotu	hodnota	k1gFnSc4	hodnota
vyvážení	vyvážení	k1gNnSc2	vyvážení
šípu	šíp	k1gInSc2	šíp
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
%	%	kIx~	%
<g/>
FOC	FOC	kA	FOC
<g/>
,	,	kIx,	,
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
pojmu	pojem	k1gInSc2	pojem
Forward	Forward	k1gMnSc1	Forward
of	of	k?	of
Centre	centr	k1gInSc5	centr
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
posunutí	posunutí	k1gNnSc4	posunutí
těžiště	těžiště	k1gNnSc1	těžiště
šípu	šíp	k1gInSc2	šíp
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
geometrický	geometrický	k2eAgInSc1d1	geometrický
střed	střed	k1gInSc1	střed
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
L	L	kA	L
délka	délka	k1gFnSc1	délka
šípu	šíp	k1gInSc2	šíp
a	a	k8xC	a
A	a	k9	a
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
těžiště	těžiště	k1gNnSc2	těžiště
od	od	k7c2	od
končíku	končík	k1gInSc2	končík
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
lze	lze	k6eAd1	lze
FOC	FOC	kA	FOC
spočíst	spočíst	k5eAaPmF	spočíst
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
%	%	kIx~	%
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
100	[number]	k4	100
</s>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
FOC	FOC	kA	FOC
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
100	[number]	k4	100
<g/>
*	*	kIx~	*
<g/>
(	(	kIx(	(
<g/>
A-	A-	k1gFnSc1	A-
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
*	*	kIx~	*
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
L	L	kA	L
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
správná	správný	k2eAgFnSc1d1	správná
hodnota	hodnota	k1gFnSc1	hodnota
bývá	bývat	k5eAaImIp3nS	bývat
uváděna	uváděn	k2eAgFnSc1d1	uváděna
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
%	%	kIx~	%
FOC	FOC	kA	FOC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
šipka	šipka	k1gFnSc1	šipka
(	(	kIx(	(
<g/>
projektil	projektil	k1gInSc1	projektil
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
šíp	šíp	k1gInSc1	šíp
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
šíp	šíp	k1gInSc1	šíp
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
