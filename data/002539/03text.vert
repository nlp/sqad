<s>
Prof.	prof.	kA	prof.
MUDr.	MUDr.	kA	MUDr.
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
Květoslav	Květoslava	k1gFnPc2	Květoslava
Šipr	Šipra	k1gFnPc2	Šipra
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
gerontologii	gerontologie	k1gFnSc4	gerontologie
a	a	k8xC	a
bioetiku	bioetika	k1gFnSc4	bioetika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
Kolegia	kolegium	k1gNnSc2	kolegium
katolických	katolický	k2eAgMnPc2d1	katolický
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
tajemníkem	tajemník	k1gMnSc7	tajemník
Rady	Rada	k1gMnSc2	Rada
České	český	k2eAgFnSc2d1	Česká
biskupské	biskupský	k2eAgFnSc2d1	biskupská
konference	konference	k1gFnSc2	konference
pro	pro	k7c4	pro
bioetiku	bioetika	k1gFnSc4	bioetika
a	a	k8xC	a
čestným	čestný	k2eAgMnSc7d1	čestný
členem	člen	k1gMnSc7	člen
České	český	k2eAgFnSc2d1	Česká
geriatrické	geriatrický	k2eAgFnSc2d1	geriatrická
a	a	k8xC	a
gerontologické	gerontologický	k2eAgFnSc2d1	gerontologická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
zástupcem	zástupce	k1gMnSc7	zástupce
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
European	Europeana	k1gFnPc2	Europeana
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
General	General	k1gFnSc2	General
Practice	Practice	k1gFnSc2	Practice
Teachers	Teachersa	k1gFnPc2	Teachersa
(	(	kIx(	(
<g/>
8	[number]	k4	8
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
výboru	výbor	k1gInSc2	výbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
časopisu	časopis	k1gInSc2	časopis
Scripta	Scripta	k1gFnSc1	Scripta
bioethica	bioethica	k1gMnSc1	bioethica
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
časopisu	časopis	k1gInSc2	časopis
European	European	k1gInSc1	European
Journal	Journal	k1gFnSc1	Journal
of	of	k?	of
General	General	k1gFnSc1	General
Practice	Practice	k1gFnSc1	Practice
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
jej	on	k3xPp3gInSc4	on
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
řádným	řádný	k2eAgMnSc7d1	řádný
členem	člen	k1gMnSc7	člen
Papežské	papežský	k2eAgFnSc2d1	Papežská
akademie	akademie	k1gFnSc2	akademie
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Přednášel	přednášet	k5eAaImAgMnS	přednášet
bioetiku	bioetika	k1gFnSc4	bioetika
na	na	k7c4	na
CMTF	CMTF	kA	CMTF
UP	UP	kA	UP
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
díla	dílo	k1gNnPc4	dílo
zde	zde	k6eAd1	zde
uvedená	uvedený	k2eAgNnPc4d1	uvedené
je	být	k5eAaImIp3nS	být
profesor	profesor	k1gMnSc1	profesor
Šipr	Šipr	k1gMnSc1	Šipr
autor	autor	k1gMnSc1	autor
stovky	stovka	k1gFnSc2	stovka
odborných	odborný	k2eAgFnPc2d1	odborná
a	a	k8xC	a
populárně	populárně	k6eAd1	populárně
naučných	naučný	k2eAgInPc2d1	naučný
článků	článek	k1gInPc2	článek
a	a	k8xC	a
spoluautor	spoluautor	k1gMnSc1	spoluautor
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
,	,	kIx,	,
skript	skripta	k1gNnPc2	skripta
a	a	k8xC	a
odborných	odborný	k2eAgFnPc2d1	odborná
publikací	publikace	k1gFnPc2	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Monografie	monografie	k1gFnSc1	monografie
<g/>
:	:	kIx,	:
Přirozené	přirozený	k2eAgNnSc1d1	přirozené
plánování	plánování	k1gNnSc1	plánování
rodičovství	rodičovství	k1gNnSc1	rodičovství
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
a	a	k8xC	a
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Hovory	hovor	k1gInPc1	hovor
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
zdravě	zdravě	k6eAd1	zdravě
stárnout	stárnout	k5eAaImF	stárnout
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Učebnice	učebnice	k1gFnSc1	učebnice
Přirozené	přirozený	k2eAgNnSc1d1	přirozené
plánování	plánování	k1gNnSc1	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
s	s	k7c7	s
H.	H.	kA	H.
Šiprovou	Šiprová	k1gFnSc7	Šiprová
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Květoslav	Květoslava	k1gFnPc2	Květoslava
Šipr	Šipr	k1gMnSc1	Šipr
Začátek	začátek	k1gInSc1	začátek
individuálního	individuální	k2eAgInSc2d1	individuální
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
a	a	k8xC	a
lidská	lidský	k2eAgFnSc1d1	lidská
důstojnost	důstojnost	k1gFnSc1	důstojnost
-	-	kIx~	-
záznam	záznam	k1gInSc1	záznam
přednášky	přednáška	k1gFnSc2	přednáška
prof.	prof.	kA	prof.
Šipra	Šipr	k1gMnSc2	Šipr
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
"	"	kIx"	"
<g/>
Nejmenší	malý	k2eAgMnSc1d3	nejmenší
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
<g/>
:	:	kIx,	:
Právní	právní	k2eAgFnSc1d1	právní
ochrana	ochrana	k1gFnSc1	ochrana
osob	osoba	k1gFnPc2	osoba
před	před	k7c7	před
narozením	narození	k1gNnSc7	narození
<g/>
"	"	kIx"	"
</s>
