<s>
Různá	různý	k2eAgNnPc1d1
nastavení	nastavení	k1gNnPc1
clony	clona	k1gFnSc2
a	a	k8xC
doby	doba	k1gFnSc2
expozice	expozice	k1gFnSc2
mohou	moct	k5eAaImIp3nP
umožnit	umožnit	k5eAaPmF
pořizování	pořizování	k1gNnSc4
snímků	snímek	k1gInPc2
na	na	k7c6
určitých	určitý	k2eAgFnPc6d1
hodnotách	hodnota	k1gFnPc6
citlivosti	citlivost	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
různých	různý	k2eAgFnPc6d1
světelných	světelný	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
,	,	kIx,
při	při	k7c6
pohybu	pohyb	k1gInSc6
subjektu	subjekt	k1gInSc2
či	či	k8xC
fotoaparátu	fotoaparát	k1gInSc2
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
i	i	k9
k	k	k7c3
dosažení	dosažení	k1gNnSc3
požadované	požadovaný	k2eAgFnSc2d1
hloubky	hloubka	k1gFnSc2
ostrosti	ostrost	k1gFnSc2
<g/>
.	.	kIx.
</s>