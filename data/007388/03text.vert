<s>
Bloody	Blood	k1gInPc1	Blood
Mary	Mary	k1gFnSc1	Mary
alias	alias	k9	alias
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
Marie	Marie	k1gFnSc1	Marie
je	být	k5eAaImIp3nS	být
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
koktejl	koktejl	k1gInSc4	koktejl
namíchaný	namíchaný	k2eAgInSc4d1	namíchaný
z	z	k7c2	z
vodky	vodka	k1gFnSc2	vodka
<g/>
,	,	kIx,	,
rajčatového	rajčatový	k2eAgInSc2d1	rajčatový
džusu	džus	k1gInSc2	džus
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
dalších	další	k2eAgNnPc2d1	další
koření	koření	k1gNnPc2	koření
nebo	nebo	k8xC	nebo
příchutí	příchuť	k1gFnPc2	příchuť
jako	jako	k8xC	jako
Worcestrová	Worcestrový	k2eAgFnSc1d1	Worcestrový
omáčka	omáčka	k1gFnSc1	omáčka
<g/>
,	,	kIx,	,
Tabasco	Tabasco	k1gNnSc1	Tabasco
<g/>
,	,	kIx,	,
hovězího	hovězí	k2eAgInSc2d1	hovězí
vývaru	vývar	k1gInSc2	vývar
nebo	nebo	k8xC	nebo
bujónu	bujón	k1gInSc2	bujón
<g/>
,	,	kIx,	,
křenu	křen	k1gInSc2	křen
<g/>
,	,	kIx,	,
celeru	celer	k1gInSc2	celer
<g/>
,	,	kIx,	,
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
černého	černý	k2eAgInSc2d1	černý
pepře	pepř	k1gInSc2	pepř
<g/>
,	,	kIx,	,
kajenského	kajenský	k2eAgInSc2d1	kajenský
pepře	pepř	k1gInSc2	pepř
<g/>
,	,	kIx,	,
citrónové	citrónový	k2eAgFnSc2d1	citrónová
šťávy	šťáva	k1gFnSc2	šťáva
<g/>
,	,	kIx,	,
vaječného	vaječný	k2eAgInSc2d1	vaječný
bílku	bílek	k1gInSc2	bílek
a	a	k8xC	a
celerového	celerový	k2eAgInSc2d1	celerový
salátu	salát	k1gInSc2	salát
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgInSc2	tento
koktejlu	koktejl	k1gInSc2	koktejl
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Fernand	Fernand	k1gInSc1	Fernand
Petiot	Petiot	k1gInSc1	Petiot
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
Bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
Harry	Harr	k1gInPc4	Harr
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
New	New	k1gFnSc7	New
York	York	k1gInSc1	York
Bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgInSc1d1	známý
bar	bar	k1gInSc1	bar
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
tradice	tradice	k1gFnSc1	tradice
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
herec	herec	k1gMnSc1	herec
George	Georg	k1gMnSc2	Georg
Jessel	Jessel	k1gMnSc1	Jessel
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tento	tento	k3xDgInSc4	tento
drink	drink	k1gInSc4	drink
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c4	o
Bloody	Blooda	k1gFnPc4	Blooda
Mary	Mary	k1gFnSc2	Mary
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
sloupku	sloupek	k1gInSc6	sloupek
Lucia	Lucius	k1gMnSc2	Lucius
Beebea	Beebeus	k1gMnSc2	Beebeus
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
"	"	kIx"	"
<g/>
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Herald	Herald	k1gMnSc1	Herald
Tribune	tribun	k1gMnSc5	tribun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
otištěném	otištěný	k2eAgInSc6d1	otištěný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
originálním	originální	k2eAgInSc7d1	originální
receptem	recept	k1gInSc7	recept
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejnovější	nový	k2eAgInSc1d3	nejnovější
lomcovák	lomcovák	k1gInSc1	lomcovák
George	Georg	k1gMnSc2	Georg
Jessela	Jessel	k1gMnSc2	Jessel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
pozornost	pozornost	k1gFnSc4	pozornost
městských	městský	k2eAgMnPc2d1	městský
pisatelů	pisatel	k1gMnPc2	pisatel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Bloody	Blooda	k1gFnPc4	Blooda
Mary	Mary	k1gFnSc2	Mary
<g/>
:	:	kIx,	:
napůl	napůl	k6eAd1	napůl
rajčatový	rajčatový	k2eAgInSc4d1	rajčatový
džus	džus	k1gInSc4	džus
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
vodka	vodka	k1gFnSc1	vodka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Málo	málo	k6eAd1	málo
kdo	kdo	k3yQnSc1	kdo
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
drink	drink	k1gInSc1	drink
je	být	k5eAaImIp3nS	být
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
podle	podle	k7c2	podle
anglické	anglický	k2eAgFnSc2d1	anglická
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
Tudorovny	Tudorovna	k1gFnSc2	Tudorovna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
kruté	krutý	k2eAgFnSc3d1	krutá
a	a	k8xC	a
labilní	labilní	k2eAgFnSc3d1	labilní
povaze	povaha	k1gFnSc3	povaha
nechala	nechat	k5eAaPmAgFnS	nechat
za	za	k7c4	za
5	[number]	k4	5
let	léto	k1gNnPc2	léto
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
popravit	popravit	k5eAaPmF	popravit
přes	přes	k7c4	přes
300	[number]	k4	300
nevinných	vinný	k2eNgMnPc2d1	nevinný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
