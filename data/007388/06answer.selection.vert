<s>
Bloody	Blood	k1gInPc1	Blood
Mary	Mary	k1gFnSc1	Mary
alias	alias	k9	alias
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
Marie	Marie	k1gFnSc1	Marie
je	být	k5eAaImIp3nS	být
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
koktejl	koktejl	k1gInSc4	koktejl
namíchaný	namíchaný	k2eAgInSc4d1	namíchaný
z	z	k7c2	z
vodky	vodka	k1gFnSc2	vodka
<g/>
,	,	kIx,	,
rajčatového	rajčatový	k2eAgInSc2d1	rajčatový
džusu	džus	k1gInSc2	džus
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
dalších	další	k2eAgNnPc2d1	další
koření	koření	k1gNnPc2	koření
nebo	nebo	k8xC	nebo
příchutí	příchuť	k1gFnPc2	příchuť
jako	jako	k8xC	jako
Worcestrová	Worcestrový	k2eAgFnSc1d1	Worcestrový
omáčka	omáčka	k1gFnSc1	omáčka
<g/>
,	,	kIx,	,
Tabasco	Tabasco	k1gNnSc1	Tabasco
<g/>
,	,	kIx,	,
hovězího	hovězí	k2eAgInSc2d1	hovězí
vývaru	vývar	k1gInSc2	vývar
nebo	nebo	k8xC	nebo
bujónu	bujón	k1gInSc2	bujón
<g/>
,	,	kIx,	,
křenu	křen	k1gInSc2	křen
<g/>
,	,	kIx,	,
celeru	celer	k1gInSc2	celer
<g/>
,	,	kIx,	,
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
černého	černý	k2eAgInSc2d1	černý
pepře	pepř	k1gInSc2	pepř
<g/>
,	,	kIx,	,
kajenského	kajenský	k2eAgInSc2d1	kajenský
pepře	pepř	k1gInSc2	pepř
<g/>
,	,	kIx,	,
citrónové	citrónový	k2eAgFnSc2d1	citrónová
šťávy	šťáva	k1gFnSc2	šťáva
<g/>
,	,	kIx,	,
vaječného	vaječný	k2eAgInSc2d1	vaječný
bílku	bílek	k1gInSc2	bílek
a	a	k8xC	a
celerového	celerový	k2eAgInSc2d1	celerový
salátu	salát	k1gInSc2	salát
<g/>
.	.	kIx.	.
</s>
