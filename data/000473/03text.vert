<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
článek	článek	k1gInSc4	článek
je	být	k5eAaImIp3nS	být
přesměrováno	přesměrován	k2eAgNnSc4d1	přesměrováno
heslo	heslo	k1gNnSc4	heslo
Hawaii	Hawaie	k1gFnSc4	Hawaie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
státu	stát	k1gInSc6	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
významy	význam	k1gInPc1	význam
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Havaj	Havaj	k1gFnSc4	Havaj
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc4	rozcestník
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hawaii	Hawaie	k1gFnSc4	Hawaie
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc4	rozcestník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Havaj	Havaj	k1gFnSc1	Havaj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Hawaii	Hawaie	k1gFnSc4	Hawaie
[	[	kIx(	[
<g/>
hə	hə	k?	hə
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Hawaii	Hawaium	k1gNnPc7	Hawaium
<g/>
,	,	kIx,	,
havajsky	havajsky	k6eAd1	havajsky
Hawaiʻ	Hawaiʻ	k1gFnSc1	Hawaiʻ
[	[	kIx(	[
<g/>
hə	hə	k?	hə
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Mokuʻ	Mokuʻ	k1gMnPc1	Mokuʻ
o	o	k7c6	o
Hawaiʻ	Hawaiʻ	k1gFnSc6	Hawaiʻ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc1	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
nacházející	nacházející	k2eAgFnSc7d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
řazený	řazený	k2eAgMnSc1d1	řazený
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
pacifických	pacifický	k2eAgInPc2d1	pacifický
států	stát	k1gInPc2	stát
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
3200	[number]	k4	3200
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Havaj	Havaj	k1gFnSc1	Havaj
je	být	k5eAaImIp3nS	být
nejjižnějším	jižní	k2eAgInSc7d3	nejjižnější
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
28	[number]	k4	28
311	[number]	k4	311
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Havaj	Havaj	k1gFnSc1	Havaj
osmým	osmý	k4xOgInSc7	osmý
nejmenším	malý	k2eAgInSc7d3	nejmenší
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
<g/>
.	.	kIx.	.
nejlidnatěším	jlidnatěší	k2eNgInSc7d1	jlidnatěší
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
83	[number]	k4	83
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Honolulu	Honolulu	k1gNnSc1	Honolulu
s	s	k7c7	s
350	[number]	k4	350
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
největšími	veliký	k2eAgMnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
East	Easta	k1gFnPc2	Easta
Honolulu	Honolulu	k1gNnSc2	Honolulu
(	(	kIx(	(
<g/>
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pearl	Pearl	k1gMnSc1	Pearl
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hilo	Hilo	k1gMnSc1	Hilo
(	(	kIx(	(
<g/>
45	[number]	k4	45
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kailua	Kailua	k1gMnSc1	Kailua
(	(	kIx(	(
<g/>
40	[number]	k4	40
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Waipahu	Waipaha	k1gFnSc4	Waipaha
(	(	kIx(	(
<g/>
40	[number]	k4	40
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
délky	délka	k1gFnSc2	délka
1207	[number]	k4	1207
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Mauna	Mauno	k1gNnSc2	Mauno
Kea	Kea	k1gFnSc2	Kea
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
4207	[number]	k4	4207
m.	m.	k?	m.
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Wailuku	Wailuk	k1gInSc2	Wailuk
a	a	k8xC	a
Kaukonahua	Kaukonahu	k1gInSc2	Kaukonahu
<g/>
.	.	kIx.	.
</s>
<s>
Havajské	havajský	k2eAgInPc4d1	havajský
ostrovy	ostrov	k1gInPc4	ostrov
obydleli	obydlet	k5eAaPmAgMnP	obydlet
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
Polynésané	Polynésan	k1gMnPc1	Polynésan
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Evropany	Evropan	k1gMnPc4	Evropan
objevil	objevit	k5eAaPmAgMnS	objevit
souostroví	souostroví	k1gNnSc3	souostroví
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
pomocí	pomoc	k1gFnSc7	pomoc
následně	následně	k6eAd1	následně
všechny	všechen	k3xTgInPc4	všechen
ostrovy	ostrov	k1gInPc4	ostrov
král	král	k1gMnSc1	král
Kamehameha	Kamehameha	k1gMnSc1	Kamehameha
I.	I.	kA	I.
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
do	do	k7c2	do
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
převzalo	převzít	k5eAaPmAgNnS	převzít
havajský	havajský	k2eAgInSc4d1	havajský
název	název	k1gInSc4	název
největšího	veliký	k2eAgInSc2d3	veliký
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
amerických	americký	k2eAgFnPc2d1	americká
sil	síla	k1gFnPc2	síla
proveden	provést	k5eAaPmNgInS	provést
převrat	převrat	k1gInSc1	převrat
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Havajská	havajský	k2eAgFnSc1d1	Havajská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
území	území	k1gNnSc1	území
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
anektovaly	anektovat	k5eAaBmAgInP	anektovat
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
zde	zde	k6eAd1	zde
zřídily	zřídit	k5eAaPmAgFnP	zřídit
havajské	havajský	k2eAgNnSc4d1	havajské
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
existovalo	existovat	k5eAaImAgNnS	existovat
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Havaj	Havaj	k1gFnSc1	Havaj
se	s	k7c7	s
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1959	[number]	k4	1959
stala	stát	k5eAaPmAgFnS	stát
posledním	poslední	k2eAgMnSc6d1	poslední
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgInP	být
Havajské	havajský	k2eAgInPc1d1	havajský
ostrovy	ostrov	k1gInPc1	ostrov
obydleny	obydlet	k5eAaPmNgInP	obydlet
nejpozději	pozdě	k6eAd3	pozdě
zhruba	zhruba	k6eAd1	zhruba
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
Polynésany	Polynésan	k1gMnPc7	Polynésan
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
ze	z	k7c2	z
souostroví	souostroví	k1gNnSc2	souostroví
Markézy	Markéza	k1gFnSc2	Markéza
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
antropologové	antropolog	k1gMnPc1	antropolog
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaPmF	stát
již	již	k6eAd1	již
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
300	[number]	k4	300
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
náhled	náhled	k1gInSc1	náhled
však	však	k9	však
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
podložen	podložen	k2eAgInSc1d1	podložen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
žili	žít	k5eAaImAgMnP	žít
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Havaje	Havaj	k1gFnSc2	Havaj
v	v	k7c6	v
izolované	izolovaný	k2eAgFnSc6d1	izolovaná
kastovní	kastovní	k2eAgFnSc6d1	kastovní
společnosti	společnost	k1gFnSc6	společnost
vedené	vedený	k2eAgMnPc4d1	vedený
náčelníky	náčelník	k1gMnPc4	náčelník
a	a	k8xC	a
šamany	šaman	k1gMnPc4	šaman
<g/>
.	.	kIx.	.
</s>
<s>
Řídili	řídit	k5eAaImAgMnP	řídit
se	se	k3xPyFc4	se
nábožensko-společenským	náboženskopolečenský	k2eAgInSc7d1	nábožensko-společenský
systémem	systém	k1gInSc7	systém
různých	různý	k2eAgNnPc2d1	různé
tabu	tabu	k1gNnPc2	tabu
celkově	celkově	k6eAd1	celkově
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
kapu	kapa	k1gFnSc4	kapa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
ostrovy	ostrov	k1gInPc4	ostrov
objevil	objevit	k5eAaPmAgMnS	objevit
pro	pro	k7c4	pro
evropskou	evropský	k2eAgFnSc4d1	Evropská
civilizaci	civilizace	k1gFnSc4	civilizace
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Endeavour	Endeavour	k1gInSc1	Endeavour
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
Jamese	Jamese	k1gFnSc2	Jamese
Cooka	Cooek	k1gInSc2	Cooek
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
abdikoval	abdikovat	k5eAaBmAgInS	abdikovat
náčelník	náčelník	k1gInSc1	náčelník
Havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
mocenských	mocenský	k2eAgInPc2d1	mocenský
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
uvolněné	uvolněný	k2eAgNnSc4d1	uvolněné
místo	místo	k1gNnSc4	místo
byl	být	k5eAaImAgMnS	být
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1780	[number]	k4	1780
až	až	k8xS	až
1795	[number]	k4	1795
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
bitev	bitva	k1gFnPc2	bitva
podařilo	podařit	k5eAaPmAgNnS	podařit
ovládnout	ovládnout	k5eAaPmF	ovládnout
ostrovy	ostrov	k1gInPc4	ostrov
náčelníkovi	náčelník	k1gMnSc6	náčelník
Kamehamehovi	Kamehameh	k1gMnSc6	Kamehameh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
havajských	havajský	k2eAgInPc2d1	havajský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ovládnutí	ovládnutí	k1gNnSc6	ovládnutí
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
násilném	násilný	k2eAgNnSc6d1	násilné
připojení	připojení	k1gNnSc6	připojení
ostrova	ostrov	k1gInSc2	ostrov
Kauai	Kaua	k1gFnSc2	Kaua
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
všechny	všechen	k3xTgInPc4	všechen
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c4	v
Havajské	havajský	k2eAgNnSc4d1	havajské
království	království	k1gNnSc4	království
a	a	k8xC	a
sebe	sebe	k3xPyFc4	sebe
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
králem	král	k1gMnSc7	král
Kamehameha	Kamehameh	k1gMnSc2	Kamehameh
I.	I.	kA	I.
(	(	kIx(	(
<g/>
označovaným	označovaný	k2eAgInPc3d1	označovaný
také	také	k9	také
jako	jako	k9	jako
Kamehameha	Kamehameha	k1gMnSc1	Kamehameha
Veliký	veliký	k2eAgMnSc1d1	veliký
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gNnSc4	jeho
potomek	potomek	k1gMnSc1	potomek
Kamehameha	Kamehameh	k1gMnSc2	Kamehameh
V.	V.	kA	V.
a	a	k8xC	a
havajská	havajský	k2eAgFnSc1d1	Havajská
vláda	vláda	k1gFnSc1	vláda
zvolila	zvolit	k5eAaPmAgFnS	zvolit
králem	král	k1gMnSc7	král
Lunalila	Lunalila	k1gFnSc2	Lunalila
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nato	nato	k6eAd1	nato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
tedy	tedy	k8xC	tedy
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
další	další	k2eAgFnPc1d1	další
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc2	který
byl	být	k5eAaImAgMnS	být
45	[number]	k4	45
<g/>
člennou	členný	k2eAgFnSc7d1	členná
komisí	komise	k1gFnSc7	komise
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
krále	král	k1gMnSc4	král
Kalā	Kalā	k1gMnSc4	Kalā
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
volbou	volba	k1gFnSc7	volba
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
domorodí	domorodý	k2eAgMnPc1d1	domorodý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Havaje	Havaj	k1gFnSc2	Havaj
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kalā	Kalā	k1gMnSc1	Kalā
je	být	k5eAaImIp3nS	být
zastánce	zastánce	k1gMnSc4	zastánce
zájmů	zájem	k1gInPc2	zájem
amerických	americký	k2eAgMnPc2d1	americký
plantážníků	plantážník	k1gMnPc2	plantážník
<g/>
.	.	kIx.	.
</s>
<s>
Kalā	Kalā	k?	Kalā
v	v	k7c6	v
královském	královský	k2eAgInSc6d1	královský
úřadu	úřad	k1gInSc6	úřad
překvapil	překvapit	k5eAaPmAgMnS	překvapit
a	a	k8xC	a
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
zastával	zastávat	k5eAaImAgMnS	zastávat
postoje	postoj	k1gInSc2	postoj
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
tvrdě	tvrdě	k6eAd1	tvrdě
hájil	hájit	k5eAaImAgMnS	hájit
havajskou	havajský	k2eAgFnSc4d1	Havajská
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
zájmy	zájem	k1gInPc1	zájem
chudých	chudý	k2eAgMnPc2d1	chudý
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1881	[number]	k4	1881
a	a	k8xC	a
1883	[number]	k4	1883
vyrazil	vyrazit	k5eAaPmAgInS	vyrazit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
ve	v	k7c6	v
světových	světový	k2eAgFnPc6d1	světová
dějinách	dějiny	k1gFnPc6	dějiny
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
prestiž	prestiž	k1gFnSc4	prestiž
a	a	k8xC	a
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c4	o
havajské	havajský	k2eAgFnPc4d1	Havajská
státnosti	státnost	k1gFnPc4	státnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
předal	předat	k5eAaPmAgMnS	předat
vládu	vláda	k1gFnSc4	vláda
své	svůj	k3xOyFgFnSc3	svůj
sestře	sestra	k1gFnSc3	sestra
Liliuokalani	Liliuokalaň	k1gFnSc3	Liliuokalaň
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zakázala	zakázat	k5eAaPmAgFnS	zakázat
imigraci	imigrace	k1gFnSc4	imigrace
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
do	do	k7c2	do
země	zem	k1gFnSc2	zem
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
neštovice	neštovice	k1gFnSc1	neštovice
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
rozčeřila	rozčeřit	k5eAaPmAgFnS	rozčeřit
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
americkými	americký	k2eAgMnPc7d1	americký
plantážníky	plantážník	k1gMnPc7	plantážník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Číňany	Číňan	k1gMnPc4	Číňan
zaměstnávali	zaměstnávat	k5eAaImAgMnP	zaměstnávat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
mezi	mezi	k7c7	mezi
Kalā	Kalā	k1gMnSc7	Kalā
a	a	k8xC	a
německým	německý	k2eAgMnSc7d1	německý
kancléřem	kancléř	k1gMnSc7	kancléř
Bismarckem	Bismarck	k1gInSc7	Bismarck
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Kalā	Kalā	k1gMnSc1	Kalā
snažil	snažit	k5eAaImAgMnS	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
unii	unie	k1gFnSc4	unie
se	s	k7c7	s
Samoou	Samoa	k1gFnSc7	Samoa
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
kolonizaci	kolonizace	k1gFnSc6	kolonizace
se	se	k3xPyFc4	se
Německé	německý	k2eAgNnSc1d1	německé
císařství	císařství	k1gNnSc1	císařství
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
pokoušelo	pokoušet	k5eAaImAgNnS	pokoušet
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
Kalā	Kalā	k1gFnSc4	Kalā
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
násilím	násilí	k1gNnSc7	násilí
donucen	donutit	k5eAaPmNgMnS	donutit
přijmout	přijmout	k5eAaPmF	přijmout
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
omezila	omezit	k5eAaPmAgFnS	omezit
jeho	jeho	k3xOp3gFnPc4	jeho
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vyčerpaný	vyčerpaný	k2eAgMnSc1d1	vyčerpaný
Kalā	Kalā	k1gMnSc1	Kalā
už	už	k6eAd1	už
jen	jen	k9	jen
sledoval	sledovat	k5eAaImAgMnS	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
pronesl	pronést	k5eAaPmAgMnS	pronést
slavnou	slavný	k2eAgFnSc4d1	slavná
větu	věta	k1gFnSc4	věta
"	"	kIx"	"
<g/>
Vyřiďte	vyřídit	k5eAaPmRp2nP	vyřídit
mému	můj	k3xOp1gNnSc3	můj
lidu	lido	k1gNnSc3	lido
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
Kalā	Kalā	k1gFnSc6	Kalā
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Liliuokalani	Liliuokalaň	k1gFnSc3	Liliuokalaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
zrušily	zrušit	k5eAaPmAgFnP	zrušit
USA	USA	kA	USA
clo	clo	k1gNnSc4	clo
na	na	k7c6	na
cukr	cukr	k1gInSc1	cukr
všem	všecek	k3xTgInPc3	všecek
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přivodilo	přivodit	k5eAaBmAgNnS	přivodit
Havaji	Havaj	k1gFnSc3	Havaj
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dříve	dříve	k6eAd2	dříve
měla	mít	k5eAaImAgFnS	mít
monopol	monopol	k1gInSc4	monopol
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k6eAd1	především
zdejším	zdejší	k2eAgMnPc3d1	zdejší
plantážníkům	plantážník	k1gMnPc3	plantážník
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Havajská	havajský	k2eAgFnSc1d1	Havajská
liga	liga	k1gFnSc1	liga
vedená	vedený	k2eAgFnSc1d1	vedená
plantážníky	plantážník	k1gMnPc4	plantážník
poté	poté	k6eAd1	poté
žádala	žádat	k5eAaImAgFnS	žádat
USA	USA	kA	USA
o	o	k7c4	o
intervenci	intervence	k1gFnSc4	intervence
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
svržení	svržení	k1gNnSc1	svržení
královny	královna	k1gFnSc2	královna
Liliuokalani	Liliuokalaň	k1gFnSc3	Liliuokalaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
na	na	k7c6	na
území	území	k1gNnSc6	území
Havaje	Havaj	k1gFnSc2	Havaj
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1893	[number]	k4	1893
se	se	k3xPyFc4	se
Liliuokalani	Liliuokalan	k1gMnPc1	Liliuokalan
vzdala	vzdát	k5eAaPmAgFnS	vzdát
trůnu	trůn	k1gInSc2	trůn
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
jejího	její	k3xOp3gInSc2	její
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Havajská	havajský	k2eAgFnSc1d1	Havajská
liga	liga	k1gFnSc1	liga
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
revoluční	revoluční	k2eAgFnSc4d1	revoluční
Havajskou	havajský	k2eAgFnSc4d1	Havajská
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
požádala	požádat	k5eAaPmAgFnS	požádat
USA	USA	kA	USA
o	o	k7c4	o
anexi	anexe	k1gFnSc4	anexe
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
anexi	anexe	k1gFnSc3	anexe
Havajské	havajský	k2eAgFnSc2d1	Havajská
republiky	republika	k1gFnSc2	republika
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Newlandsovy	Newlandsův	k2eAgFnSc2d1	Newlandsův
rezoluce	rezoluce	k1gFnSc2	rezoluce
<g/>
.	.	kIx.	.
</s>
<s>
Státem	stát	k1gInSc7	stát
USA	USA	kA	USA
se	se	k3xPyFc4	se
Havaj	Havaj	k1gFnSc1	Havaj
stala	stát	k5eAaPmAgFnS	stát
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Havajské	havajský	k2eAgInPc4d1	havajský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
v	v	k7c6	v
Havajském	havajský	k2eAgNnSc6d1	havajské
souostroví	souostroví	k1gNnSc6	souostroví
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
splynutím	splynutí	k1gNnSc7	splynutí
dvou	dva	k4xCgFnPc2	dva
velkých	velká	k1gFnPc2	velká
vulkánů	vulkán	k1gInPc2	vulkán
-	-	kIx~	-
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
(	(	kIx(	(
<g/>
4207	[number]	k4	4207
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Loa	Loa	k1gFnSc1	Loa
(	(	kIx(	(
<g/>
4160	[number]	k4	4160
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
úpatím	úpatí	k1gNnSc7	úpatí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
dně	dno	k1gNnSc6	dno
5500	[number]	k4	5500
m	m	kA	m
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
horou	hora	k1gFnSc7	hora
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
absolutní	absolutní	k2eAgFnSc7d1	absolutní
výškou	výška	k1gFnSc7	výška
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
základnou	základna	k1gFnSc7	základna
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
výška	výška	k1gFnSc1	výška
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
je	být	k5eAaImIp3nS	být
sopka	sopka	k1gFnSc1	sopka
v	v	k7c6	v
období	období	k1gNnSc6	období
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
snad	snad	k9	snad
vyhaslá	vyhaslý	k2eAgFnSc1d1	vyhaslá
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Mauna	Mauna	k1gFnSc1	Mauna
Loa	Loa	k1gFnSc2	Loa
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejčinnějších	činný	k2eAgFnPc2d3	nejčinnější
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
nejaktivnější	aktivní	k2eAgInSc1d3	nejaktivnější
sopouch	sopouch	k1gInSc1	sopouch
<g/>
,	,	kIx,	,
Kilauea	Kilauea	k1gFnSc1	Kilauea
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
svahu	svah	k1gInSc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
vnějšího	vnější	k2eAgInSc2d1	vnější
okraje	okraj	k1gInSc2	okraj
kaldery	kaldera	k1gFnSc2	kaldera
Kilauea	Kilaue	k1gInSc2	Kilaue
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
13	[number]	k4	13
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Havajské	havajský	k2eAgInPc1d1	havajský
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
souostroví	souostroví	k1gNnSc4	souostroví
mnoha	mnoho	k4c2	mnoho
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc4d1	hlavní
ostrovy	ostrov	k1gInPc4	ostrov
však	však	k9	však
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
osm	osm	k4xCc4	osm
následujících	následující	k2eAgInPc2d1	následující
(	(	kIx(	(
<g/>
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Niihau	Niiha	k1gMnSc3	Niiha
<g/>
,	,	kIx,	,
Kauai	Kaua	k1gMnSc3	Kaua
<g/>
,	,	kIx,	,
Oahu	Oah	k1gMnSc3	Oah
<g/>
,	,	kIx,	,
Molokai	Moloka	k1gMnSc3	Moloka
<g/>
,	,	kIx,	,
Lanai	Lana	k1gMnSc3	Lana
<g/>
,	,	kIx,	,
Maui	Mau	k1gMnSc3	Mau
<g/>
,	,	kIx,	,
Kahoolawe	Kahoolawe	k1gFnSc1	Kahoolawe
a	a	k8xC	a
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
souostroví	souostroví	k1gNnSc2	souostroví
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
státu	stát	k1gInSc2	stát
Havaj	Havaj	k1gFnSc1	Havaj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
tzv.	tzv.	kA	tzv.
Severozápadní	severozápadní	k2eAgInPc4d1	severozápadní
Havajské	havajský	k2eAgInPc4d1	havajský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
řetězec	řetězec	k1gInSc1	řetězec
malých	malý	k2eAgInPc2d1	malý
neobydlených	obydlený	k2eNgInPc2d1	neobydlený
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
od	od	k7c2	od
Kauai	Kaua	k1gFnSc2	Kaua
další	další	k2eAgFnSc2d1	další
stovky	stovka	k1gFnSc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
Midway	Midwa	k1gMnPc4	Midwa
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nespadá	spadat	k5eNaImIp3nS	spadat
do	do	k7c2	do
státu	stát	k1gInSc2	stát
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
360	[number]	k4	360
301	[number]	k4	301
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Havaj	Havaj	k1gFnSc4	Havaj
představuje	představovat	k5eAaImIp3nS	představovat
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
států	stát	k1gInPc2	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
nepředstavují	představovat	k5eNaImIp3nP	představovat
většinu	většina	k1gFnSc4	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
asijského	asijský	k2eAgInSc2d1	asijský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
podíl	podíl	k1gInSc1	podíl
míšenců	míšenec	k1gMnPc2	míšenec
mezi	mezi	k7c7	mezi
rasami	rasa	k1gFnPc7	rasa
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
předkům	předek	k1gInPc3	předek
mezi	mezi	k7c7	mezi
původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
ostrovů	ostrov	k1gInPc2	ostrov
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
něco	něco	k3yInSc1	něco
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
22	[number]	k4	22
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
24,7	[number]	k4	24,7
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
1,6	[number]	k4	1,6
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
38,6	[number]	k4	38,6
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
10,0	[number]	k4	10,0
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
1,2	[number]	k4	1,2
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
23,6	[number]	k4	23,6
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
8,9	[number]	k4	8,9
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
je	být	k5eAaImIp3nS	být
protestantů	protestant	k1gMnPc2	protestant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
roztříštěni	roztříštěn	k2eAgMnPc1d1	roztříštěn
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
malých	malý	k2eAgFnPc2d1	malá
církví	církev	k1gFnPc2	církev
a	a	k8xC	a
denominací	denominace	k1gFnPc2	denominace
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc7d3	veliký
protestantskou	protestantský	k2eAgFnSc7d1	protestantská
církví	církev	k1gFnSc7	církev
je	být	k5eAaImIp3nS	být
United	United	k1gMnSc1	United
Church	Church	k1gMnSc1	Church
of	of	k?	of
Christ	Christ	k1gMnSc1	Christ
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
asi	asi	k9	asi
3	[number]	k4	3
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
největší	veliký	k2eAgFnSc4d3	veliký
organizovanou	organizovaný	k2eAgFnSc4d1	organizovaná
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
skupinu	skupina	k1gFnSc4	skupina
představuje	představovat	k5eAaImIp3nS	představovat
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
křesťané	křesťan	k1gMnPc1	křesťan
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
68	[number]	k4	68
%	%	kIx~	%
protestanti	protestant	k1gMnPc1	protestant
-	-	kIx~	-
42	[number]	k4	42
%	%	kIx~	%
United	United	k1gMnSc1	United
Church	Church	k1gMnSc1	Church
of	of	k?	of
Christ	Christ	k1gMnSc1	Christ
-	-	kIx~	-
3	[number]	k4	3
%	%	kIx~	%
baptisté	baptista	k1gMnPc1	baptista
-	-	kIx~	-
2	[number]	k4	2
%	%	kIx~	%
metodisté	metodista	k1gMnPc1	metodista
-	-	kIx~	-
2	[number]	k4	2
%	%	kIx~	%
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
-	-	kIx~	-
24	[number]	k4	24
%	%	kIx~	%
buddhisté	buddhista	k1gMnPc1	buddhista
-	-	kIx~	-
9	[number]	k4	9
%	%	kIx~	%
mormoni	mormon	k1gMnPc1	mormon
-	-	kIx~	-
2	[number]	k4	2
%	%	kIx~	%
ostatní	ostatní	k2eAgMnPc4d1	ostatní
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
šintoisté	šintoista	k1gMnPc1	šintoista
<g/>
,	,	kIx,	,
taoisté	taoista	k1gMnPc1	taoista
<g/>
,	,	kIx,	,
paganisté	paganista	k1gMnPc1	paganista
<g/>
)	)	kIx)	)
-	-	kIx~	-
5	[number]	k4	5
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
-	-	kIx~	-
18	[number]	k4	18
%	%	kIx~	%
Patronem	patron	k1gInSc7	patron
Havaje	Havaj	k1gFnSc2	Havaj
je	být	k5eAaImIp3nS	být
svatý	svatý	k2eAgMnSc1d1	svatý
Jozef	Jozef	k1gMnSc1	Jozef
de	de	k?	de
Veuster	Veuster	k1gMnSc1	Veuster
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
otec	otec	k1gMnSc1	otec
Damien	Damien	k2eAgMnSc1d1	Damien
nebo	nebo	k8xC	nebo
Otec	otec	k1gMnSc1	otec
malomocných	malomocný	k1gMnPc2	malomocný
<g/>
,	,	kIx,	,
belgický	belgický	k2eAgMnSc1d1	belgický
katolický	katolický	k2eAgMnSc1d1	katolický
misionář	misionář	k1gMnSc1	misionář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
1889	[number]	k4	1889
působil	působit	k5eAaImAgInS	působit
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
mezi	mezi	k7c7	mezi
nakaženými	nakažený	k2eAgMnPc7d1	nakažený
malomocenstvím	malomocenství	k1gNnPc3	malomocenství
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
touto	tento	k3xDgFnSc7	tento
nemocí	nemoc	k1gFnSc7	nemoc
nakazil	nakazit	k5eAaPmAgMnS	nakazit
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejváženějších	vážený	k2eAgFnPc2d3	nejváženější
osobností	osobnost	k1gFnPc2	osobnost
historie	historie	k1gFnSc2	historie
Havaje	Havaj	k1gFnPc1	Havaj
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
sochy	socha	k1gFnPc1	socha
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
sídlem	sídlo	k1gNnSc7	sídlo
vlády	vláda	k1gFnSc2	vláda
Havaje	Havaj	k1gFnSc2	Havaj
a	a	k8xC	a
před	před	k7c7	před
Kapitolem	Kapitol	k1gInSc7	Kapitol
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
síti	síť	k1gFnSc6	síť
Havaje	Havaj	k1gFnSc2	Havaj
existují	existovat	k5eAaImIp3nP	existovat
dálnice	dálnice	k1gFnSc1	dálnice
H-	H-	k1gFnSc1	H-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
H-2	H-2	k1gFnSc1	H-2
a	a	k8xC	a
H-	H-	k1gFnSc1	H-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
přídomek	přídomek	k1gInSc4	přídomek
"	"	kIx"	"
<g/>
mezistátní	mezistátní	k2eAgInPc4d1	mezistátní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Havaj	Havaj	k1gFnSc1	Havaj
s	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
jiným	jiný	k2eAgInSc7d1	jiný
státem	stát	k1gInSc7	stát
nesousedí	sousedit	k5eNaImIp3nP	sousedit
<g/>
.	.	kIx.	.
</s>
<s>
Mottem	motto	k1gNnSc7	motto
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Ua	Ua	k1gMnSc1	Ua
man	man	k1gMnSc1	man
ka	ka	k?	ka
ea	ea	k?	ea
o	o	k7c6	o
ka	ka	k?	ka
aina	aina	k1gMnSc1	aina
i	i	k8xC	i
ka	ka	k?	ka
pono	pono	k1gMnSc1	pono
–	–	k?	–
the	the	k?	the
life	life	k1gInSc1	life
of	of	k?	of
the	the	k?	the
land	land	k1gMnSc1	land
endureth	endureth	k1gMnSc1	endureth
in	in	k?	in
righteousness	righteousness	k1gInSc1	righteousness
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
květinou	květina	k1gFnSc7	květina
žlutý	žlutý	k2eAgInSc1d1	žlutý
ibišek	ibišek	k1gInSc1	ibišek
<g/>
,	,	kIx,	,
stromem	strom	k1gInSc7	strom
tungovník	tungovník	k1gInSc1	tungovník
molucký	molucký	k2eAgInSc1d1	molucký
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
berneška	berneška	k1gFnSc1	berneška
havajská	havajský	k2eAgFnSc1d1	Havajská
a	a	k8xC	a
písní	píseň	k1gFnSc7	píseň
Hawaii	Hawaie	k1gFnSc4	Hawaie
Ponoi	Pono	k1gFnSc2	Pono
<g/>
.	.	kIx.	.
</s>
<s>
Havaj	Havaj	k1gFnSc1	Havaj
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
stát	stát	k1gInSc1	stát
USA	USA	kA	USA
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
své	svůj	k3xOyFgFnSc2	svůj
vlajky	vlajka	k1gFnSc2	vlajka
vlajku	vlajka	k1gFnSc4	vlajka
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
