<s>
Havaj	Havaj	k1gFnSc1	Havaj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Hawaii	Hawaie	k1gFnSc4	Hawaie
[	[	kIx(	[
<g/>
hə	hə	k?	hə
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Hawaii	Hawaium	k1gNnPc7	Hawaium
<g/>
,	,	kIx,	,
havajsky	havajsky	k6eAd1	havajsky
Hawaiʻ	Hawaiʻ	k1gFnSc1	Hawaiʻ
[	[	kIx(	[
<g/>
hə	hə	k?	hə
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Mokuʻ	Mokuʻ	k1gMnPc1	Mokuʻ
o	o	k7c6	o
Hawaiʻ	Hawaiʻ	k1gFnSc6	Hawaiʻ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc1	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
nacházející	nacházející	k2eAgFnSc7d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
řazený	řazený	k2eAgMnSc1d1	řazený
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
pacifických	pacifický	k2eAgInPc2d1	pacifický
států	stát	k1gInPc2	stát
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
