<s>
Dijkstrův	Dijkstrův	k2eAgInSc1d1	Dijkstrův
algoritmus	algoritmus	k1gInSc1	algoritmus
je	být	k5eAaImIp3nS	být
algoritmus	algoritmus	k1gInSc4	algoritmus
sloužící	sloužící	k2eAgInSc4d1	sloužící
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
nejkratší	krátký	k2eAgFnSc2d3	nejkratší
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
grafu	graf	k1gInSc6	graf
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
konečný	konečný	k2eAgInSc1d1	konečný
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
konečný	konečný	k2eAgInSc4d1	konečný
vstup	vstup	k1gInSc4	vstup
algoritmus	algoritmus	k1gInSc1	algoritmus
skončí	skončit	k5eAaPmIp3nS	skončit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
průchodu	průchod	k1gInSc6	průchod
cyklu	cyklus	k1gInSc2	cyklus
se	se	k3xPyFc4	se
do	do	k7c2	do
množiny	množina	k1gFnSc2	množina
navštívených	navštívený	k2eAgInPc2d1	navštívený
uzlů	uzel	k1gInPc2	uzel
přidá	přidat	k5eAaPmIp3nS	přidat
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
uzel	uzel	k1gInSc1	uzel
<g/>
,	,	kIx,	,
průchodů	průchod	k1gInPc2	průchod
cyklem	cyklus	k1gInSc7	cyklus
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nejvýše	nejvýše	k6eAd1	nejvýše
tolik	tolik	k4xDc1	tolik
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc1	kolik
má	mít	k5eAaImIp3nS	mít
graf	graf	k1gInSc1	graf
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
nad	nad	k7c4	nad
hranově	hranově	k6eAd1	hranově
kladně	kladně	k6eAd1	kladně
ohodnoceným	ohodnocený	k2eAgInSc7d1	ohodnocený
grafem	graf	k1gInSc7	graf
(	(	kIx(	(
<g/>
neohodnocený	ohodnocený	k2eNgInSc1d1	ohodnocený
graf	graf	k1gInSc1	graf
lze	lze	k6eAd1	lze
však	však	k9	však
na	na	k7c4	na
ohodnocený	ohodnocený	k2eAgInSc1d1	ohodnocený
snadno	snadno	k6eAd1	snadno
převést	převést	k5eAaPmF	převést
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
grafy	graf	k1gInPc4	graf
s	s	k7c7	s
hranami	hrana	k1gFnPc7	hrana
se	s	k7c7	s
záporným	záporný	k2eAgNnSc7d1	záporné
ohodnocením	ohodnocení	k1gNnSc7	ohodnocení
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
pomalejší	pomalý	k2eAgInSc4d2	pomalejší
Bellmanův-Fordův	Bellmanův-Fordův	k2eAgInSc4d1	Bellmanův-Fordův
algoritmus	algoritmus	k1gInSc4	algoritmus
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmus	algoritmus	k1gInSc4	algoritmus
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
informatik	informatik	k1gMnSc1	informatik
Edsger	Edsger	k1gMnSc1	Edsger
Dijkstra	Dijkstra	k1gFnSc1	Dijkstra
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
graf	graf	k1gInSc4	graf
G	G	kA	G
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hledáme	hledat	k5eAaImIp1nP	hledat
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Řekněme	říct	k5eAaPmRp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
V	V	kA	V
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
vrcholů	vrchol	k1gInPc2	vrchol
grafu	graf	k1gInSc2	graf
G	G	kA	G
a	a	k8xC	a
množina	množina	k1gFnSc1	množina
E	E	kA	E
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
hrany	hrana	k1gFnPc4	hrana
grafu	graf	k1gInSc2	graf
G.	G.	kA	G.
Algoritmus	algoritmus	k1gInSc1	algoritmus
pracuje	pracovat	k5eAaImIp3nS	pracovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
vrchol	vrchol	k1gInSc4	vrchol
v	v	k7c6	v
z	z	k7c2	z
V	V	kA	V
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
délku	délka	k1gFnSc4	délka
nejkratší	krátký	k2eAgFnSc2d3	nejkratší
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
dá	dát	k5eAaPmIp3nS	dát
dostat	dostat	k5eAaPmF	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Označme	označit	k5eAaPmRp1nP	označit
tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
jako	jako	k8xS	jako
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc4	všechen
vrcholy	vrchol	k1gInPc4	vrchol
v	v	k7c4	v
hodnotu	hodnota	k1gFnSc4	hodnota
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
<g/>
=	=	kIx~	=
<g/>
∞	∞	k?	∞
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
počátečního	počáteční	k2eAgInSc2d1	počáteční
vrcholu	vrchol	k1gInSc2	vrchol
s	s	k7c7	s
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
]	]	kIx)	]
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečno	nekonečno	k1gNnSc1	nekonečno
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neznáme	znát	k5eNaImIp1nP	znát
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
si	se	k3xPyFc3	se
algoritmus	algoritmus	k1gInSc1	algoritmus
udržuje	udržovat	k5eAaImIp3nS	udržovat
množiny	množina	k1gFnPc4	množina
Z	Z	kA	Z
a	a	k8xC	a
N	N	kA	N
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Z	Z	kA	Z
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
už	už	k6eAd1	už
navštívené	navštívený	k2eAgInPc4d1	navštívený
vrcholy	vrchol	k1gInPc4	vrchol
a	a	k8xC	a
N	N	kA	N
dosud	dosud	k6eAd1	dosud
nenavštívené	navštívený	k2eNgNnSc4d1	nenavštívené
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmus	algoritmus	k1gInSc1	algoritmus
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
N	N	kA	N
není	být	k5eNaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
průchodu	průchod	k1gInSc6	průchod
cyklu	cyklus	k1gInSc2	cyklus
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
jeden	jeden	k4xCgInSc1	jeden
vrchol	vrchol	k1gInSc1	vrchol
vmin	vmina	k1gFnPc2	vmina
z	z	k7c2	z
N	N	kA	N
do	do	k7c2	do
Z	z	k7c2	z
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgFnSc4d3	nejmenší
hodnotu	hodnota	k1gFnSc4	hodnota
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
vrcholů	vrchol	k1gInPc2	vrchol
v	v	k7c6	v
z	z	k7c2	z
N.	N.	kA	N.
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
vrchol	vrchol	k1gInSc4	vrchol
u	u	k7c2	u
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
vede	vést	k5eAaImIp3nS	vést
hrana	hrana	k1gFnSc1	hrana
(	(	kIx(	(
<g/>
označme	označit	k5eAaPmRp1nP	označit
<g />
.	.	kIx.	.
</s>
<s>
její	její	k3xOp3gFnSc4	její
délku	délka	k1gFnSc4	délka
jako	jako	k8xS	jako
l	l	kA	l
<g/>
(	(	kIx(	(
<g/>
vmin	vmin	k1gMnSc1	vmin
<g/>
,	,	kIx,	,
<g/>
u	u	k7c2	u
<g/>
))	))	k?	))
z	z	k7c2	z
vmin	vmina	k1gFnPc2	vmina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
následující	následující	k2eAgFnPc4d1	následující
operace	operace	k1gFnPc4	operace
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
vmin	vmin	k1gMnSc1	vmin
<g/>
]	]	kIx)	]
+	+	kIx~	+
l	l	kA	l
<g/>
(	(	kIx(	(
<g/>
vmin	vmin	k1gMnSc1	vmin
<g/>
,	,	kIx,	,
<g/>
u	u	k7c2	u
<g/>
))	))	k?	))
<	<	kIx(	<
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
u	u	k7c2	u
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
do	do	k7c2	do
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
u	u	k7c2	u
<g/>
]	]	kIx)	]
přiřaď	přiřadit	k5eAaPmRp2nS	přiřadit
hodnotu	hodnota	k1gFnSc4	hodnota
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
vmin	vmin	k1gMnSc1	vmin
<g/>
]	]	kIx)	]
+	+	kIx~	+
l	l	kA	l
<g/>
(	(	kIx(	(
<g/>
vmin	vmin	k1gMnSc1	vmin
<g/>
,	,	kIx,	,
<g/>
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
neprováděj	provádět	k5eNaImRp2nS	provádět
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
algoritmus	algoritmus	k1gInSc1	algoritmus
skončí	skončit	k5eAaPmIp3nS	skončit
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
vrchol	vrchol	k1gInSc4	vrchol
v	v	k7c6	v
z	z	k7c2	z
V	V	kA	V
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
jeho	jeho	k3xOp3gFnSc2	jeho
nejkratší	krátký	k2eAgFnSc2d3	nejkratší
cesty	cesta	k1gFnSc2	cesta
od	od	k7c2	od
počátečního	počáteční	k2eAgInSc2d1	počáteční
vrcholu	vrchol	k1gInSc2	vrchol
s	s	k7c7	s
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
1	[number]	k4	1
function	function	k1gInSc1	function
Dijkstra	Dijkstrum	k1gNnSc2	Dijkstrum
<g/>
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
for	forum	k1gNnPc2	forum
each	each	k1gInSc1	each
vertex	vertex	k1gInSc1	vertex
v	v	k7c6	v
in	in	k?	in
V	V	kA	V
<g/>
:	:	kIx,	:
//	//	k?	//
Inicializace	inicializace	k1gFnSc1	inicializace
3	[number]	k4	3
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
:	:	kIx,	:
<g/>
=	=	kIx~	=
infinity	infinit	k1gInPc1	infinit
//	//	k?	//
Zatím	zatím	k6eAd1	zatím
neznámá	známý	k2eNgFnSc1d1	neznámá
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
s	s	k7c7	s
do	do	k7c2	do
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
4	[number]	k4	4
p	p	k?	p
<g/>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
:	:	kIx,	:
<g/>
=	=	kIx~	=
undefined	undefined	k1gInSc1	undefined
//	//	k?	//
Předchozí	předchozí	k2eAgInSc1d1	předchozí
vrchol	vrchol	k1gInSc1	vrchol
na	na	k7c6	na
nejkratší	krátký	k2eAgFnSc6d3	nejkratší
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
s	s	k7c7	s
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
5	[number]	k4	5
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
]	]	kIx)	]
:	:	kIx,	:
<g/>
=	=	kIx~	=
0	[number]	k4	0
//	//	k?	//
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
z	z	k7c2	z
s	s	k7c7	s
do	do	k7c2	do
s	s	k7c7	s
6	[number]	k4	6
N	N	kA	N
:	:	kIx,	:
<g/>
=	=	kIx~	=
V	V	kA	V
//	//	k?	//
Množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
dosud	dosud	k6eAd1	dosud
nenavštívených	navštívený	k2eNgInPc2d1	nenavštívený
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
všech	všecek	k3xTgInPc2	všecek
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
7	[number]	k4	7
while	while	k1gNnPc2	while
N	N	kA	N
is	is	k?	is
not	nota	k1gFnPc2	nota
empty	empta	k1gFnSc2	empta
<g/>
:	:	kIx,	:
//	//	k?	//
Samotný	samotný	k2eAgInSc1d1	samotný
algoritmus	algoritmus	k1gInSc1	algoritmus
8	[number]	k4	8
u	u	k7c2	u
:	:	kIx,	:
<g/>
=	=	kIx~	=
extract_min	extract_min	k1gInSc1	extract_min
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
//	//	k?	//
Vezměme	vzít	k5eAaPmRp1nP	vzít
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
<g/>
"	"	kIx"	"
vrchol	vrchol	k1gInSc4	vrchol
9	[number]	k4	9
for	forum	k1gNnPc2	forum
each	each	k1gMnSc1	each
neighbor	neighbor	k1gMnSc1	neighbor
v	v	k7c6	v
of	of	k?	of
u	u	k7c2	u
<g/>
:	:	kIx,	:
10	[number]	k4	10
alt	alt	k1gInSc1	alt
=	=	kIx~	=
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s>
<g/>
[	[	kIx(	[
<g/>
u	u	k7c2	u
<g/>
]	]	kIx)	]
+	+	kIx~	+
l	l	kA	l
<g/>
(	(	kIx(	(
<g/>
u	u	k7c2	u
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g/>
)	)	kIx)	)
//	//	k?	//
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
smyčce	smyčec	k1gInSc2	smyčec
cyklu	cyklus	k1gInSc2	cyklus
u	u	k7c2	u
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
u	u	k7c2	u
<g/>
]	]	kIx)	]
=	=	kIx~	=
0	[number]	k4	0
11	[number]	k4	11
if	if	k?	if
alt	alt	k1gInSc1	alt
<	<	kIx(	<
d	d	k?	d
<g/>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
12	[number]	k4	12
d	d	k?	d
<g/>
<g />
.	.	kIx.	.
</s>
<s>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
:	:	kIx,	:
<g/>
=	=	kIx~	=
alt	alt	k1gInSc1	alt
13	[number]	k4	13
p	p	k?	p
<g/>
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
:	:	kIx,	:
<g/>
=	=	kIx~	=
u	u	k7c2	u
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
nás	my	k3xPp1nPc4	my
zajímá	zajímat	k5eAaImIp3nS	zajímat
pouze	pouze	k6eAd1	pouze
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
cesta	cesta	k1gFnSc1	cesta
mezi	mezi	k7c7	mezi
zdrojovým	zdrojový	k2eAgInSc7d1	zdrojový
a	a	k8xC	a
cílovým	cílový	k2eAgInSc7d1	cílový
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
ukončit	ukončit	k5eAaPmF	ukončit
hledání	hledání	k1gNnSc4	hledání
na	na	k7c6	na
řádce	řádka	k1gFnSc6	řádka
9	[number]	k4	9
if	if	k?	if
u	u	k7c2	u
=	=	kIx~	=
target	targeta	k1gFnPc2	targeta
<g/>
.	.	kIx.	.
</s>
<s>
Cestu	cesta	k1gFnSc4	cesta
ze	z	k7c2	z
zdroje	zdroj	k1gInSc2	zdroj
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
pak	pak	k6eAd1	pak
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
cyklem	cyklus	k1gInSc7	cyklus
<g/>
:	:	kIx,	:
1	[number]	k4	1
S	s	k7c7	s
:	:	kIx,	:
<g/>
=	=	kIx~	=
empty	empt	k1gInPc1	empt
sequence	sequenec	k1gInSc2	sequenec
2	[number]	k4	2
u	u	k7c2	u
:	:	kIx,	:
<g/>
=	=	kIx~	=
target	target	k1gInSc1	target
3	[number]	k4	3
while	while	k1gNnPc2	while
defined	defined	k1gInSc1	defined
p	p	k?	p
<g/>
[	[	kIx(	[
<g/>
u	u	k7c2	u
<g/>
]	]	kIx)	]
4	[number]	k4	4
insert	insert	k1gInSc1	insert
u	u	k7c2	u
at	at	k?	at
the	the	k?	the
beginning	beginning	k1gInSc1	beginning
of	of	k?	of
S	s	k7c7	s
5	[number]	k4	5
u	u	k7c2	u
:	:	kIx,	:
<g/>
=	=	kIx~	=
p	p	k?	p
<g/>
[	[	kIx(	[
<g/>
u	u	k7c2	u
<g/>
]	]	kIx)	]
Obecnější	obecní	k2eAgInSc1d2	obecní
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
nalezení	nalezení	k1gNnSc4	nalezení
nejkratších	krátký	k2eAgFnPc2d3	nejkratší
cest	cesta	k1gFnPc2	cesta
mezi	mezi	k7c7	mezi
zdrojovým	zdrojový	k2eAgInSc7d1	zdrojový
vrcholem	vrchol	k1gInSc7	vrchol
a	a	k8xC	a
všemi	všecek	k3xTgFnPc7	všecek
ostatními	ostatní	k2eAgFnPc7d1	ostatní
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
nějakou	nějaký	k3yIgFnSc7	nějaký
jejich	jejich	k3xOp3gFnSc7	jejich
podmnožinou	podmnožina	k1gFnSc7	podmnožina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
p	p	k?	p
<g/>
[	[	kIx(	[
<g/>
]	]	kIx)	]
ukládat	ukládat	k5eAaImF	ukládat
všechny	všechen	k3xTgInPc1	všechen
vrcholy	vrchol	k1gInPc1	vrchol
splňující	splňující	k2eAgFnSc4d1	splňující
relaxační	relaxační	k2eAgFnSc4d1	relaxační
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
implementace	implementace	k1gFnSc1	implementace
Dijkstrova	Dijkstrův	k2eAgInSc2d1	Dijkstrův
algoritmu	algoritmus	k1gInSc2	algoritmus
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
prioritní	prioritní	k2eAgFnSc2d1	prioritní
fronty	fronta	k1gFnSc2	fronta
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
operace	operace	k1gFnSc2	operace
Extract-Min	Extract-Min	k1gMnSc1	Extract-Min
<g/>
(	(	kIx(	(
<g/>
Q	Q	kA	Q
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lineární	lineární	k2eAgNnSc4d1	lineární
prohledávání	prohledávání	k1gNnSc4	prohledávání
všech	všecek	k3xTgInPc2	všecek
vrcholů	vrchol	k1gInPc2	vrchol
v	v	k7c6	v
Q.	Q.	kA	Q.
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
asymptotická	asymptotický	k2eAgFnSc1d1	asymptotická
časová	časový	k2eAgFnSc1d1	časová
složitost	složitost	k1gFnSc1	složitost
O	o	k7c4	o
<g/>
(	(	kIx(	(
<g/>
|	|	kIx~	|
<g/>
V	V	kA	V
<g/>
|	|	kIx~	|
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
E	E	kA	E
<g/>
|	|	kIx~	|
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
|	|	kIx~	|
<g/>
V	V	kA	V
<g/>
|	|	kIx~	|
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
vrcholů	vrchol	k1gInPc2	vrchol
a	a	k8xC	a
|	|	kIx~	|
<g/>
E	E	kA	E
<g/>
|	|	kIx~	|
počet	počet	k1gInSc1	počet
hran	hrana	k1gFnPc2	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
řídké	řídký	k2eAgInPc4d1	řídký
grafy	graf	k1gInPc4	graf
(	(	kIx(	(
<g/>
grafy	graf	k1gInPc4	graf
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
hran	hrana	k1gFnPc2	hrana
mnohem	mnohem	k6eAd1	mnohem
menším	menšit	k5eAaImIp1nS	menšit
než	než	k8xS	než
|	|	kIx~	|
<g/>
V	v	k7c6	v
<g/>
|	|	kIx~	|
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Dijkstrův	Dijkstrův	k2eAgInSc1d1	Dijkstrův
algoritmus	algoritmus	k1gInSc1	algoritmus
implementován	implementován	k2eAgInSc1d1	implementován
mnohem	mnohem	k6eAd1	mnohem
efektivněji	efektivně	k6eAd2	efektivně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
graf	graf	k1gInSc1	graf
ukládá	ukládat	k5eAaImIp3nS	ukládat
pomocí	pomocí	k7c2	pomocí
seznamu	seznam	k1gInSc2	seznam
sousedů	soused	k1gMnPc2	soused
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
Extract-Min	Extract-Mina	k1gFnPc2	Extract-Mina
se	se	k3xPyFc4	se
implementuje	implementovat	k5eAaImIp3nS	implementovat
pomocí	pomocí	k7c2	pomocí
binární	binární	k2eAgFnSc2d1	binární
nebo	nebo	k8xC	nebo
Fibonacciho	Fibonacci	k1gMnSc2	Fibonacci
haldy	halda	k1gFnSc2	halda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
algoritmus	algoritmus	k1gInSc1	algoritmus
běží	běžet	k5eAaImIp3nS	běžet
v	v	k7c6	v
čase	čas	k1gInSc6	čas
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
(	(	kIx(	(
(	(	kIx(	(
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
)	)	kIx)	)
log	log	kA	log
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
O	o	k7c6	o
<g/>
((	((	k?	((
<g/>
|	|	kIx~	|
<g/>
E	E	kA	E
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
|	|	kIx~	|
<g/>
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
V	V	kA	V
<g/>
|	|	kIx~	|
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
|	|	kIx~	|
<g/>
V	V	kA	V
<g/>
|	|	kIx~	|
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
Fibonacciho	Fibonacci	k1gMnSc2	Fibonacci
halda	halda	k1gFnSc1	halda
čas	čas	k1gInSc1	čas
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
(	(	kIx(	(
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
log	log	kA	log
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
O	o	k7c6	o
<g/>
(	(	kIx(	(
<g/>
|	|	kIx~	|
<g/>
E	E	kA	E
<g/>
|	|	kIx~	|
<g/>
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
V	V	kA	V
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
|	|	kIx~	|
<g/>
V	V	kA	V
<g/>
|	|	kIx~	|
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
A	a	k9	a
<g/>
*	*	kIx~	*
Bellmanův-Fordův	Bellmanův-Fordův	k2eAgInSc1d1	Bellmanův-Fordův
algoritmus	algoritmus	k1gInSc1	algoritmus
Floydův-Warshallův	Floydův-Warshallův	k2eAgInSc1d1	Floydův-Warshallův
algoritmus	algoritmus	k1gInSc1	algoritmus
E.	E.	kA	E.
W.	W.	kA	W.
Dijkstra	Dijkstra	k1gFnSc1	Dijkstra
<g/>
:	:	kIx,	:
A	a	k9	a
note	notat	k5eAaPmIp3nS	notat
on	on	k3xPp3gInSc1	on
two	two	k?	two
problems	problems	k1gInSc1	problems
in	in	k?	in
connexion	connexion	k1gInSc1	connexion
with	with	k1gMnSc1	with
graphs	graphs	k1gInSc1	graphs
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Numerische	Numerische	k1gFnSc1	Numerische
Mathematik	Mathematik	k1gMnSc1	Mathematik
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
S.	S.	kA	S.
269	[number]	k4	269
<g/>
-	-	kIx~	-
<g/>
271	[number]	k4	271
Thomas	Thomas	k1gMnSc1	Thomas
H.	H.	kA	H.
Cormen	Cormen	k1gInSc1	Cormen
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
E.	E.	kA	E.
Leiserson	Leiserson	k1gMnSc1	Leiserson
<g/>
,	,	kIx,	,
Ronald	Ronald	k1gMnSc1	Ronald
L.	L.	kA	L.
Rivest	Rivest	k1gMnSc1	Rivest
<g/>
,	,	kIx,	,
a	a	k8xC	a
Clifford	Clifford	k1gMnSc1	Clifford
Stein	Stein	k1gMnSc1	Stein
<g/>
.	.	kIx.	.
</s>
<s>
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
Algorithms	Algorithms	k1gInSc1	Algorithms
<g/>
,	,	kIx,	,
Second	Second	k1gInSc1	Second
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
MIT	MIT	kA	MIT
Press	Press	k1gInSc1	Press
and	and	k?	and
McGraw-Hill	McGraw-Hill	k1gInSc1	McGraw-Hill
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
262	[number]	k4	262
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3293	[number]	k4	3293
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Sekce	sekce	k1gFnSc1	sekce
24.3	[number]	k4	24.3
<g/>
:	:	kIx,	:
Dijkstra	Dijkstrum	k1gNnSc2	Dijkstrum
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
algorithm	algorith	k1gNnSc7	algorith
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.595	.595	k4	.595
<g/>
-	-	kIx~	-
<g/>
601	[number]	k4	601
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Nejkratší	krátký	k2eAgFnSc1d3	nejkratší
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
grafu	graf	k1gInSc6	graf
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Základní	základní	k2eAgInPc1d1	základní
grafové	grafový	k2eAgInPc1d1	grafový
algoritmy	algoritmus	k1gInPc1	algoritmus
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dijkstrův	Dijkstrův	k2eAgInSc4d1	Dijkstrův
algoritmus	algoritmus	k1gInSc4	algoritmus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Nejkratší	krátký	k2eAgFnSc1d3	nejkratší
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
ohodnoceném	ohodnocený	k2eAgInSc6d1	ohodnocený
grafu	graf	k1gInSc6	graf
-	-	kIx~	-
popis	popis	k1gInSc1	popis
Dijkstrova	Dijkstrův	k2eAgInSc2d1	Dijkstrův
algoritmu	algoritmus	k1gInSc2	algoritmus
Halda	halda	k1gFnSc1	halda
<g/>
,	,	kIx,	,
Dijkstrův	Dijkstrův	k2eAgInSc1d1	Dijkstrův
algoritmus	algoritmus	k1gInSc1	algoritmus
</s>
