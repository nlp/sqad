<s>
Prokaryotická	Prokaryotický	k2eAgFnSc1d1	Prokaryotická
buňka	buňka	k1gFnSc1	buňka
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
buňky	buňka	k1gFnSc2	buňka
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
pro	pro	k7c4	pro
bakterie	bakterie	k1gFnPc4	bakterie
a	a	k8xC	a
archea	archea	k6eAd1	archea
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
souhrnně	souhrnně	k6eAd1	souhrnně
Prokaryota	Prokaryota	k1gFnSc1	Prokaryota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
