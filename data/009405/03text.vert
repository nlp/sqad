<p>
<s>
Prokaryotická	Prokaryotický	k2eAgFnSc1d1	Prokaryotická
buňka	buňka	k1gFnSc1	buňka
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
buňky	buňka	k1gFnSc2	buňka
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
pro	pro	k7c4	pro
bakterie	bakterie	k1gFnPc4	bakterie
a	a	k8xC	a
archea	archea	k6eAd1	archea
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
souhrnně	souhrnně	k6eAd1	souhrnně
Prokaryota	Prokaryota	k1gFnSc1	Prokaryota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c4	o
řád	řád	k1gInSc4	řád
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
buňka	buňka	k1gFnSc1	buňka
eukaryotická	eukaryotický	k2eAgFnSc1d1	eukaryotická
<g/>
,	,	kIx,	,
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
také	také	k9	také
jednodušší	jednoduchý	k2eAgFnSc7d2	jednodušší
organizací	organizace	k1gFnSc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Prokaryotické	Prokaryotický	k2eAgFnPc1d1	Prokaryotická
buňky	buňka	k1gFnPc1	buňka
mají	mít	k5eAaImIp3nP	mít
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
<g/>
Prokaryota	Prokaryot	k1gMnSc2	Prokaryot
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
jednobuněčné	jednobuněčný	k2eAgInPc1d1	jednobuněčný
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
netvoří	tvořit	k5eNaImIp3nP	tvořit
tedy	tedy	k9	tedy
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typické	typický	k2eAgFnPc1d1	typická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
prokaryot	prokaryota	k1gFnPc2	prokaryota
==	==	k?	==
</s>
</p>
<p>
<s>
Uvádí	uvádět	k5eAaImIp3nP	uvádět
se	se	k3xPyFc4	se
tři	tři	k4xCgFnPc1	tři
klíčové	klíčový	k2eAgFnPc1d1	klíčová
charakteristiky	charakteristika	k1gFnPc1	charakteristika
prokaryotických	prokaryotický	k2eAgFnPc2d1	prokaryotická
buněk	buňka	k1gFnPc2	buňka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
organizace	organizace	k1gFnSc1	organizace
nukleoidu	nukleoid	k1gInSc2	nukleoid
(	(	kIx(	(
<g/>
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
obdoby	obdoba	k1gFnSc2	obdoba
jádra	jádro	k1gNnSc2	jádro
<g/>
)	)	kIx)	)
–	–	k?	–
nukleoid	nukleoid	k1gInSc1	nukleoid
není	být	k5eNaImIp3nS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
okolní	okolní	k2eAgFnSc2d1	okolní
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
jen	jen	k9	jen
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
velké	velký	k2eAgFnSc2d1	velká
molekuly	molekula	k1gFnSc2	molekula
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
nejsou	být	k5eNaImIp3nP	být
histony	histon	k1gInPc1	histon
ani	ani	k8xC	ani
jiné	jiný	k2eAgFnPc1d1	jiná
bazické	bazický	k2eAgFnPc1d1	bazická
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
haploidní	haploidní	k2eAgNnSc1d1	haploidní
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
organel	organela	k1gFnPc2	organela
–	–	k?	–
v	v	k7c6	v
prokaryotické	prokaryotický	k2eAgFnSc6d1	prokaryotická
buňce	buňka	k1gFnSc6	buňka
nejsou	být	k5eNaImIp3nP	být
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
<g/>
,	,	kIx,	,
plastidy	plastid	k1gInPc1	plastid
<g/>
,	,	kIx,	,
endoplazmatické	endoplazmatický	k2eAgNnSc1d1	endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
ani	ani	k8xC	ani
jiná	jiný	k2eAgFnSc1d1	jiná
organela	organela	k1gFnSc1	organela
s	s	k7c7	s
membránou	membrána	k1gFnSc7	membrána
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
vlastnosti	vlastnost	k1gFnPc1	vlastnost
ribozomů	ribozom	k1gInPc2	ribozom
–	–	k?	–
ribozomy	ribozom	k1gInPc1	ribozom
prokaryot	prokaryot	k1gInSc1	prokaryot
se	se	k3xPyFc4	se
od	od	k7c2	od
eukaryot	eukaryota	k1gFnPc2	eukaryota
liší	lišit	k5eAaImIp3nP	lišit
svou	svůj	k3xOyFgFnSc7	svůj
hmotností	hmotnost	k1gFnSc7	hmotnost
i	i	k8xC	i
velikostí	velikost	k1gFnSc7	velikost
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
specifické	specifický	k2eAgFnPc1d1	specifická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
typickým	typický	k2eAgInPc3d1	typický
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
nutně	nutně	k6eAd1	nutně
všeobecným	všeobecný	k2eAgNnSc7d1	všeobecné
<g/>
,	,	kIx,	,
vlastnostem	vlastnost	k1gFnPc3	vlastnost
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
peptidoglykan	peptidoglykan	k1gInSc1	peptidoglykan
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
mykoplazmy	mykoplazma	k1gFnPc1	mykoplazma
a	a	k8xC	a
archebakterie	archebakterie	k1gFnPc1	archebakterie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
stavba	stavba	k1gFnSc1	stavba
bičíků	bičík	k1gInPc2	bičík
než	než	k8xS	než
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
(	(	kIx(	(
<g/>
nemusí	muset	k5eNaImIp3nS	muset
však	však	k9	však
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
anaerobióza	anaerobióza	k1gFnSc1	anaerobióza
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
žít	žít	k5eAaImF	žít
anaerobně	anaerobně	k6eAd1	anaerobně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
častější	častý	k2eAgFnSc2d2	častější
než	než	k8xS	než
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
</s>
</p>
<p>
<s>
schopnost	schopnost	k1gFnSc1	schopnost
vázat	vázat	k5eAaImF	vázat
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
unikátní	unikátní	k2eAgMnSc1d1	unikátní
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
prokaryota	prokaryota	k1gFnSc1	prokaryota
</s>
</p>
<p>
<s>
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
pinocytózy	pinocytóza	k1gFnSc2	pinocytóza
<g/>
,	,	kIx,	,
fagocytózy	fagocytóza	k1gFnSc2	fagocytóza
a	a	k8xC	a
exocytózy	exocytóza	k1gFnSc2	exocytóza
</s>
</p>
<p>
<s>
asi	asi	k9	asi
desetkrát	desetkrát	k6eAd1	desetkrát
menší	malý	k2eAgFnSc1d2	menší
velikost	velikost	k1gFnSc1	velikost
než	než	k8xS	než
eukaryotická	eukaryotický	k2eAgFnSc1d1	eukaryotická
buňka	buňka	k1gFnSc1	buňka
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Eukaryota	Eukaryota	k1gFnSc1	Eukaryota
</s>
</p>
<p>
<s>
Prokaryota	Prokaryota	k1gFnSc1	Prokaryota
</s>
</p>
<p>
<s>
Eukaryotická	Eukaryotický	k2eAgFnSc1d1	Eukaryotická
buňka	buňka	k1gFnSc1	buňka
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
prokaryotická	prokaryotický	k2eAgFnSc1d1	prokaryotická
buňka	buňka	k1gFnSc1	buňka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Procaryota	Procaryota	k1gFnSc1	Procaryota
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Procaryota	Procaryota	k1gFnSc1	Procaryota
cell	cello	k1gNnPc2	cello
</s>
</p>
