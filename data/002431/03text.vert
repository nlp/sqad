<s>
Alkoholové	alkoholový	k2eAgNnSc1d1	alkoholové
kvašení	kvašení	k1gNnSc1	kvašení
je	být	k5eAaImIp3nS	být
biochemický	biochemický	k2eAgInSc4d1	biochemický
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
sacharidy	sacharid	k1gInPc4	sacharid
přeměňovány	přeměňován	k2eAgInPc4d1	přeměňován
na	na	k7c4	na
alkohol	alkohol	k1gInSc4	alkohol
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc1	kvasinka
vlastní	vlastní	k2eAgFnPc1d1	vlastní
enzymy	enzym	k1gInPc4	enzym
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
sacharidy	sacharid	k1gInPc4	sacharid
na	na	k7c4	na
ethanol	ethanol	k1gInSc4	ethanol
a	a	k8xC	a
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
glykolýze	glykolýza	k1gFnSc6	glykolýza
je	být	k5eAaImIp3nS	být
spotřebováváno	spotřebováván	k2eAgNnSc4d1	spotřebováváno
NAD	NAD	kA	NAD
<g/>
+	+	kIx~	+
na	na	k7c6	na
NADH	NADH	kA	NADH
<g/>
,	,	kIx,	,
NAD	NAD	kA	NAD
<g/>
+	+	kIx~	+
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
recyklaci	recyklace	k1gFnSc3	recyklace
NADH	NADH	kA	NADH
na	na	k7c4	na
NAD	NAD	kA	NAD
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přísunu	přísun	k1gInSc6	přísun
kyslíku	kyslík	k1gInSc2	kyslík
se	se	k3xPyFc4	se
NADH	NADH	kA	NADH
předává	předávat	k5eAaImIp3nS	předávat
mitochondriím	mitochondrie	k1gFnPc3	mitochondrie
na	na	k7c4	na
reoxidaci	reoxidace	k1gFnSc4	reoxidace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
anaerobní	anaerobní	k2eAgFnPc1d1	anaerobní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
NAD	nad	k7c4	nad
<g/>
+	+	kIx~	+
doplňována	doplňován	k2eAgFnSc1d1	doplňována
redukcí	redukce	k1gFnSc7	redukce
pyruvátu	pyruvát	k1gInSc2	pyruvát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
glykolýze	glykolýza	k1gFnSc6	glykolýza
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
anaerobní	anaerobní	k2eAgNnSc1d1	anaerobní
odbourávání	odbourávání	k1gNnSc1	odbourávání
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
jako	jako	k9	jako
mléčné	mléčný	k2eAgNnSc4d1	mléčné
kvašení	kvašení	k1gNnSc4	kvašení
(	(	kIx(	(
<g/>
svaly	sval	k1gInPc4	sval
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
alkoholové	alkoholový	k2eAgNnSc4d1	alkoholové
kvašení	kvašení	k1gNnSc4	kvašení
(	(	kIx(	(
<g/>
kvasinky	kvasinka	k1gFnSc2	kvasinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
anaerobních	anaerobní	k2eAgFnPc6d1	anaerobní
podmínkách	podmínka	k1gFnPc6	podmínka
kvasinky	kvasinka	k1gFnSc2	kvasinka
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
glukosu	glukosa	k1gFnSc4	glukosa
na	na	k7c4	na
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lidstvo	lidstvo	k1gNnSc4	lidstvo
již	již	k9	již
po	po	k7c4	po
několik	několik	k4yIc4	několik
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
produkt	produkt	k1gInSc1	produkt
<g/>
,	,	kIx,	,
ethanol	ethanol	k1gInSc1	ethanol
a	a	k8xC	a
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Kvasný	kvasný	k2eAgInSc1d1	kvasný
proces	proces	k1gInSc1	proces
kvasinek	kvasinka	k1gFnPc2	kvasinka
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
krocích	krok	k1gInPc6	krok
<g/>
:	:	kIx,	:
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
se	se	k3xPyFc4	se
pyruvát	pyruvát	k1gInSc1	pyruvát
dekarboxyluje	dekarboxylovat	k5eAaBmIp3nS	dekarboxylovat
na	na	k7c4	na
acetaldehyd	acetaldehyd	k1gInSc4	acetaldehyd
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
katalyzována	katalyzovat	k5eAaPmNgFnS	katalyzovat
enzymem	enzym	k1gInSc7	enzym
pyruvátdekarboxylásou	pyruvátdekarboxylása	k1gFnSc7	pyruvátdekarboxylása
<g/>
.	.	kIx.	.
</s>
<s>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
C	C	kA	C
<g/>
(	(	kIx(	(
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
COOH	COOH	kA	COOH
→	→	k?	→
CH3CHO	CH3CHO	k1gMnSc1	CH3CHO
+	+	kIx~	+
CO2	CO2	k1gMnSc1	CO2
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kroku	krok	k1gInSc6	krok
je	být	k5eAaImIp3nS	být
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
acetaldehyd	acetaldehyd	k1gInSc1	acetaldehyd
redukován	redukovat	k5eAaBmNgInS	redukovat
na	na	k7c4	na
ethanol	ethanol	k1gInSc4	ethanol
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
reakci	reakce	k1gFnSc6	reakce
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
enzym	enzym	k1gInSc4	enzym
alkoholdehydrogenáza	alkoholdehydrogenáza	k1gFnSc1	alkoholdehydrogenáza
(	(	kIx(	(
<g/>
ADH	ADH	kA	ADH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
CH3CHO	CH3CHO	k4	CH3CHO
+	+	kIx~	+
NADH	NADH	kA	NADH
+	+	kIx~	+
H	H	kA	H
<g/>
+	+	kIx~	+
→	→	k?	→
CH3CH2OH	CH3CH2OH	k1gMnSc1	CH3CH2OH
+	+	kIx~	+
NAD	NAD	kA	NAD
<g/>
+	+	kIx~	+
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
reakci	reakce	k1gFnSc6	reakce
je	být	k5eAaImIp3nS	být
NADH	NADH	kA	NADH
přeměn	přeměna	k1gFnPc2	přeměna
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
NAD	NAD	kA	NAD
<g/>
+	+	kIx~	+
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
buňce	buňka	k1gFnSc3	buňka
opět	opět	k6eAd1	opět
doplní	doplnit	k5eAaPmIp3nS	doplnit
oxidovaná	oxidovaný	k2eAgFnSc1d1	oxidovaná
forma	forma	k1gFnSc1	forma
této	tento	k3xDgFnSc2	tento
molekuly	molekula	k1gFnSc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Fermentace	fermentace	k1gFnSc1	fermentace
sumárně	sumárně	k6eAd1	sumárně
<g/>
:	:	kIx,	:
glukosa	glukosa	k1gFnSc1	glukosa
→	→	k?	→
2CO2	[number]	k4	2CO2
+	+	kIx~	+
2	[number]	k4	2
<g/>
ethanol	ethanol	k1gInSc1	ethanol
Δ	Δ	k?	Δ
<g/>
°	°	k?	°
<g/>
'	'	kIx"	'
=	=	kIx~	=
-	-	kIx~	-
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
Jako	jako	k8xC	jako
další	další	k2eAgInSc1d1	další
hlavní	hlavní	k2eAgInSc1d1	hlavní
produkt	produkt	k1gInSc1	produkt
alkoholového	alkoholový	k2eAgNnSc2d1	alkoholové
kvašení	kvašení	k1gNnSc2	kvašení
vznikají	vznikat	k5eAaImIp3nP	vznikat
dvě	dva	k4xCgFnPc1	dva
molekuly	molekula	k1gFnPc1	molekula
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
teoreticky	teoreticky	k6eAd1	teoreticky
jen	jen	k9	jen
asi	asi	k9	asi
26	[number]	k4	26
<g/>
%	%	kIx~	%
využití	využití	k1gNnSc4	využití
celkové	celkový	k2eAgFnSc2d1	celková
možné	možný	k2eAgFnSc2d1	možná
energie	energie	k1gFnSc2	energie
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
přeměna	přeměna	k1gFnSc1	přeměna
jednoho	jeden	k4xCgInSc2	jeden
molu	mol	k1gInSc2	mol
ADP	ADP	kA	ADP
na	na	k7c6	na
ATP	atp	kA	atp
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
30,5	[number]	k4	30,5
kJ	kJ	k?	kJ
volné	volný	k2eAgFnSc2d1	volná
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbylá	zbylý	k2eAgFnSc1d1	zbylá
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
uvolňována	uvolňovat	k5eAaImNgFnS	uvolňovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
reakce	reakce	k1gFnSc1	reakce
stává	stávat	k5eAaImIp3nS	stávat
ireverzibilní	ireverzibilní	k2eAgFnSc1d1	ireverzibilní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
fysiologických	fysiologický	k2eAgFnPc6d1	fysiologická
podmínkách	podmínka	k1gFnPc6	podmínka
je	být	k5eAaImIp3nS	být
účinnost	účinnost	k1gFnSc4	účinnost
přeměny	přeměna	k1gFnSc2	přeměna
volné	volný	k2eAgFnSc2d1	volná
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ATP	atp	kA	atp
asi	asi	k9	asi
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
koncentracemi	koncentrace	k1gFnPc7	koncentrace
složek	složka	k1gFnPc2	složka
za	za	k7c2	za
fysiologického	fysiologický	k2eAgInSc2d1	fysiologický
a	a	k8xC	a
standardního	standardní	k2eAgInSc2d1	standardní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
anaerobní	anaerobní	k2eAgFnSc6d1	anaerobní
fermentaci	fermentace	k1gFnSc6	fermentace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
aerobním	aerobní	k2eAgNnSc7d1	aerobní
odbouráváním	odbourávání	k1gNnSc7	odbourávání
velice	velice	k6eAd1	velice
plýtváno	plýtván	k2eAgNnSc4d1	plýtváno
glukosou	glukosa	k1gFnSc7	glukosa
-	-	kIx~	-
při	při	k7c6	při
anaerobním	anaerobní	k2eAgNnSc6d1	anaerobní
odbourávání	odbourávání	k1gNnSc6	odbourávání
glukosy	glukosa	k1gFnSc2	glukosa
se	se	k3xPyFc4	se
získají	získat	k5eAaPmIp3nP	získat
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
molekuly	molekula	k1gFnPc1	molekula
ATP	atp	kA	atp
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
molekulu	molekula	k1gFnSc4	molekula
glukosy	glukosa	k1gFnSc2	glukosa
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
aerobním	aerobní	k2eAgNnSc7d1	aerobní
odbouráváním	odbourávání	k1gNnSc7	odbourávání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
glukózy	glukóza	k1gFnSc2	glukóza
36	[number]	k4	36
molekul	molekula	k1gFnPc2	molekula
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
anaerobní	anaerobní	k2eAgInSc4d1	anaerobní
způsob	způsob	k1gInSc4	způsob
vzniku	vznik	k1gInSc2	vznik
energie	energie	k1gFnSc2	energie
18	[number]	k4	18
<g/>
×	×	k?	×
méně	málo	k6eAd2	málo
účinný	účinný	k2eAgInSc1d1	účinný
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
Pasteurovo	Pasteurův	k2eAgNnSc1d1	Pasteurovo
pozorování	pozorování	k1gNnSc1	pozorování
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Pasteurův	Pasteurův	k2eAgInSc1d1	Pasteurův
efekt	efekt	k1gInSc1	efekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
kvasinky	kvasinka	k1gFnPc1	kvasinka
za	za	k7c2	za
anaerobních	anaerobní	k2eAgFnPc2d1	anaerobní
podmínek	podmínka	k1gFnPc2	podmínka
spotřebovávají	spotřebovávat	k5eAaImIp3nP	spotřebovávat
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
glukosy	glukosa	k1gFnPc1	glukosa
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
úbytkem	úbytek	k1gInSc7	úbytek
za	za	k7c2	za
aerobních	aerobní	k2eAgFnPc2d1	aerobní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Alkoholové	alkoholový	k2eAgNnSc1d1	alkoholové
kvašení	kvašení	k1gNnSc1	kvašení
však	však	k9	však
nemůže	moct	k5eNaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
do	do	k7c2	do
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
koncentrace	koncentrace	k1gFnSc1	koncentrace
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
sacharidu	sacharid	k1gInSc2	sacharid
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
speciálně	speciálně	k6eAd1	speciálně
vyšlechtěných	vyšlechtěný	k2eAgFnPc2d1	vyšlechtěná
kvasinek	kvasinka	k1gFnPc2	kvasinka
až	až	k9	až
16	[number]	k4	16
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
<g/>
-li	i	k?	-li
koncentrace	koncentrace	k1gFnSc1	koncentrace
alkoholu	alkohol	k1gInSc2	alkohol
nad	nad	k7c4	nad
přípustnou	přípustný	k2eAgFnSc4d1	přípustná
mez	mez	k1gFnSc4	mez
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
namnožit	namnožit	k5eAaPmF	namnožit
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
meziprodukt	meziprodukt	k1gInSc4	meziprodukt
fermentace	fermentace	k1gFnSc2	fermentace
<g/>
,	,	kIx,	,
acetaldehyd	acetaldehyd	k1gInSc4	acetaldehyd
<g/>
,	,	kIx,	,
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
<g/>
,	,	kIx,	,
a	a	k8xC	a
místo	místo	k7c2	místo
alkoholu	alkohol	k1gInSc2	alkohol
dostaneme	dostat	k5eAaPmIp1nP	dostat
vysoceprocentní	vysoceprocentní	k2eAgInSc4d1	vysoceprocentní
ocet	ocet	k1gInSc4	ocet
<g/>
.	.	kIx.	.
</s>
<s>
Ethanol	ethanol	k1gInSc1	ethanol
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
pro	pro	k7c4	pro
kvasinky	kvasinka	k1gFnPc4	kvasinka
velice	velice	k6eAd1	velice
toxický	toxický	k2eAgMnSc1d1	toxický
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
rozmnožení	rozmnožení	k1gNnSc1	rozmnožení
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
kvasinky	kvasinka	k1gFnSc2	kvasinka
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
otrávily	otrávit	k5eAaPmAgFnP	otrávit
ethanolem	ethanol	k1gInSc7	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Alkoholické	alkoholický	k2eAgInPc4d1	alkoholický
nápoje	nápoj	k1gInPc4	nápoj
vznikající	vznikající	k2eAgFnSc1d1	vznikající
kvašením	kvašení	k1gNnSc7	kvašení
a	a	k8xC	a
mající	mající	k2eAgInSc1d1	mající
obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
třeba	třeba	k6eAd1	třeba
i	i	k9	i
30	[number]	k4	30
%	%	kIx~	%
vznikají	vznikat	k5eAaImIp3nP	vznikat
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
cestou	cesta	k1gFnSc7	cesta
také	také	k6eAd1	také
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
asi	asi	k9	asi
jen	jen	k6eAd1	jen
14	[number]	k4	14
%	%	kIx~	%
obsahu	obsah	k1gInSc2	obsah
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
koncentrace	koncentrace	k1gFnSc1	koncentrace
ethanolu	ethanol	k1gInSc2	ethanol
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
nadstavením	nadstavení	k1gNnSc7	nadstavení
alkoholu	alkohol	k1gInSc2	alkohol
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
koncentraci	koncentrace	k1gFnSc4	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Alkoholismus	alkoholismus	k1gInSc1	alkoholismus
Pivo	pivo	k1gNnSc1	pivo
Víno	víno	k1gNnSc1	víno
</s>
