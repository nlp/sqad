<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
biochemický	biochemický	k2eAgInSc4d1	biochemický
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
polysacharidy	polysacharid	k1gInPc4	polysacharid
přeměňovány	přeměňován	k2eAgInPc4d1	přeměňován
na	na	k7c4	na
alkohol	alkohol	k1gInSc4	alkohol
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
?	?	kIx.	?
</s>
