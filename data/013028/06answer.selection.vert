<s>
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Imperium	Imperium	k1gNnSc1	Imperium
Romanum	Romanum	k?	Romanum
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
existující	existující	k2eAgFnPc4d1	existující
v	v	k7c6	v
letech	let	k1gInPc6	let
27	[number]	k4	27
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
–	–	k?	–
<g/>
395	[number]	k4	395
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
územně	územně	k6eAd1	územně
nejrozsáhlejších	rozsáhlý	k2eAgFnPc2d3	nejrozsáhlejší
říší	říš	k1gFnPc2	říš
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
