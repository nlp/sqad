<p>
<s>
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Imperium	Imperium	k1gNnSc1	Imperium
Romanum	Romanum	k?	Romanum
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
existující	existující	k2eAgFnPc4d1	existující
v	v	k7c6	v
letech	let	k1gInPc6	let
27	[number]	k4	27
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
–	–	k?	–
<g/>
395	[number]	k4	395
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
územně	územně	k6eAd1	územně
nejrozsáhlejších	rozsáhlý	k2eAgFnPc2d3	nejrozsáhlejší
říší	říš	k1gFnPc2	říš
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Caesara	Caesar	k1gMnSc2	Caesar
císařem	císař	k1gMnSc7	císař
Oktavianem	Oktavian	k1gMnSc7	Oktavian
Augustem	August	k1gMnSc7	August
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přijal	přijmout	k5eAaPmAgMnS	přijmout
titul	titul	k1gInSc4	titul
imperátora	imperátor	k1gMnSc2	imperátor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
byl	být	k5eAaImAgInS	být
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zrod	zrod	k1gInSc1	zrod
principátu	principát	k1gInSc2	principát
==	==	k?	==
</s>
</p>
<p>
<s>
Octavianus	Octavianus	k1gMnSc1	Octavianus
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Caesar	Caesar	k1gMnSc1	Caesar
docílil	docílit	k5eAaPmAgMnS	docílit
samovlády	samovláda	k1gFnPc4	samovláda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Caesara	Caesar	k1gMnSc2	Caesar
se	se	k3xPyFc4	se
nepokoušel	pokoušet	k5eNaImAgMnS	pokoušet
potvrdit	potvrdit	k5eAaPmF	potvrdit
svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
získáním	získání	k1gNnSc7	získání
úřadu	úřad	k1gInSc2	úřad
diktátora	diktátor	k1gMnSc2	diktátor
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zastřel	zastřít	k5eAaPmAgInS	zastřít
své	svůj	k3xOyFgNnSc4	svůj
autokratické	autokratický	k2eAgNnSc4d1	autokratické
postavení	postavení	k1gNnSc4	postavení
samovládce	samovládce	k1gMnSc2	samovládce
do	do	k7c2	do
hávu	háv	k1gInSc2	háv
republikánských	republikánský	k2eAgFnPc2d1	republikánská
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Čistě	čistě	k6eAd1	čistě
formálně	formálně	k6eAd1	formálně
ponechal	ponechat	k5eAaPmAgMnS	ponechat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
starou	starý	k2eAgFnSc4d1	stará
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
převzetím	převzetí	k1gNnSc7	převzetí
výkonu	výkon	k1gInSc2	výkon
různých	různý	k2eAgInPc2d1	různý
republikánských	republikánský	k2eAgInPc2d1	republikánský
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
nabytím	nabytí	k1gNnSc7	nabytí
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
plných	plný	k2eAgFnPc2d1	plná
mocí	moc	k1gFnPc2	moc
a	a	k8xC	a
především	především	k9	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
kontrole	kontrola	k1gFnSc3	kontrola
klíčových	klíčový	k2eAgFnPc2d1	klíčová
provincií	provincie	k1gFnPc2	provincie
s	s	k7c7	s
početnými	početný	k2eAgFnPc7d1	početná
legiemi	legie	k1gFnPc7	legie
<g/>
.	.	kIx.	.
</s>
<s>
Jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
se	s	k7c7	s
principem	princip	k1gInSc7	princip
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
prvním	první	k4xOgMnSc7	první
občanem	občan	k1gMnSc7	občan
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
názorně	názorně	k6eAd1	názorně
tak	tak	k6eAd1	tak
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c2	za
rovného	rovný	k2eAgNnSc2d1	rovné
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
senátory	senátor	k1gMnPc7	senátor
(	(	kIx(	(
<g/>
princeps	princeps	k6eAd1	princeps
inter	inter	k1gInSc1	inter
pares	paresa	k1gFnPc2	paresa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
úředníci	úředník	k1gMnPc1	úředník
<g/>
,	,	kIx,	,
soudci	soudce	k1gMnPc1	soudce
<g/>
,	,	kIx,	,
správci	správce	k1gMnPc1	správce
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
velitelé	velitel	k1gMnPc1	velitel
vojsk	vojsko	k1gNnPc2	vojsko
měli	mít	k5eAaImAgMnP	mít
spolupodílet	spolupodílet	k5eAaImF	spolupodílet
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Octavianem	Octavian	k1gInSc7	Octavian
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
vládní	vládní	k2eAgInSc4d1	vládní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatných	podstatný	k2eAgInPc6d1	podstatný
rysech	rys	k1gInPc6	rys
lišil	lišit	k5eAaImAgInS	lišit
od	od	k7c2	od
staré	starý	k2eAgFnSc2d1	stará
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nazývá	nazývat	k5eAaImIp3nS	nazývat
principát	principát	k1gInSc4	principát
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
mu	on	k3xPp3gMnSc3	on
kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
titulů	titul	k1gInPc2	titul
udělil	udělit	k5eAaPmAgInS	udělit
čestné	čestný	k2eAgNnSc4d1	čestné
jméno	jméno	k1gNnSc4	jméno
Augustus	Augustus	k1gInSc4	Augustus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vznešený	vznešený	k2eAgMnSc1d1	vznešený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesto	přesto	k8xC	přesto
i	i	k9	i
za	za	k7c2	za
principátu	principát	k1gInSc2	principát
zůstaly	zůstat	k5eAaPmAgInP	zůstat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
zpočátku	zpočátku	k6eAd1	zpočátku
zachovány	zachován	k2eAgFnPc4d1	zachována
tradiční	tradiční	k2eAgFnPc4d1	tradiční
republikánské	republikánský	k2eAgFnPc4d1	republikánská
instituce	instituce	k1gFnPc4	instituce
<g/>
:	:	kIx,	:
veškeré	veškerý	k3xTgFnPc1	veškerý
magistratury	magistratura	k1gFnPc1	magistratura
<g/>
,	,	kIx,	,
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnSc1	organizace
správy	správa	k1gFnSc2	správa
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
úřad	úřad	k1gInSc1	úřad
pontifika	pontifex	k1gMnSc2	pontifex
však	však	k8xC	však
zastával	zastávat	k5eAaImAgMnS	zastávat
císař	císař	k1gMnSc1	císař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
význam	význam	k1gInSc1	význam
těchto	tento	k3xDgFnPc2	tento
ctihodných	ctihodný	k2eAgFnPc2d1	ctihodná
a	a	k8xC	a
starobylých	starobylý	k2eAgFnPc2d1	starobylá
institucí	instituce	k1gFnPc2	instituce
postupně	postupně	k6eAd1	postupně
upadal	upadat	k5eAaPmAgMnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgInPc1d1	rozhodující
politické	politický	k2eAgInPc1d1	politický
orgány	orgán	k1gInPc1	orgán
začaly	začít	k5eAaPmAgInP	začít
brzy	brzy	k6eAd1	brzy
fungovat	fungovat	k5eAaImF	fungovat
jako	jako	k9	jako
pouhé	pouhý	k2eAgInPc1d1	pouhý
správní	správní	k2eAgInPc1d1	správní
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Instituce	instituce	k1gFnSc2	instituce
vytvářené	vytvářený	k2eAgFnSc2d1	vytvářená
císařem	císař	k1gMnSc7	císař
přebíraly	přebírat	k5eAaImAgFnP	přebírat
krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
kompetence	kompetence	k1gFnSc2	kompetence
republikánských	republikánský	k2eAgFnPc2d1	republikánská
magistratur	magistratura	k1gFnPc2	magistratura
<g/>
.	.	kIx.	.
</s>
<s>
Jezdci	jezdec	k1gMnPc1	jezdec
a	a	k8xC	a
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
měrou	míra	k1gFnSc7wR	míra
také	také	k9	také
propuštěnci	propuštěnec	k1gMnPc1	propuštěnec
zaujímali	zaujímat	k5eAaImAgMnP	zaujímat
klíčová	klíčový	k2eAgNnPc4d1	klíčové
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
úřadech	úřad	k1gInPc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
císaři	císař	k1gMnPc1	císař
obsazovali	obsazovat	k5eAaImAgMnP	obsazovat
svými	svůj	k3xOyFgMnPc7	svůj
oblíbenci	oblíbenec	k1gMnPc7	oblíbenec
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
mocenskou	mocenský	k2eAgFnSc4d1	mocenská
roli	role	k1gFnSc4	role
převzala	převzít	k5eAaPmAgFnS	převzít
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
především	především	k9	především
pretoriáni	pretorián	k1gMnPc1	pretorián
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sloužili	sloužit	k5eAaImAgMnP	sloužit
jako	jako	k9	jako
císařova	císařův	k2eAgFnSc1d1	císařova
tělesná	tělesný	k2eAgFnSc1d1	tělesná
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
sociální	sociální	k2eAgFnSc1d1	sociální
struktura	struktura	k1gFnSc1	struktura
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
proměňovat	proměňovat	k5eAaImF	proměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
Augustovy	Augustův	k2eAgFnSc2d1	Augustova
vlády	vláda	k1gFnSc2	vláda
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
příslušníků	příslušník	k1gMnPc2	příslušník
nových	nový	k2eAgFnPc2d1	nová
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Italů	Ital	k1gMnPc2	Ital
a	a	k8xC	a
provinciálů	provinciál	k1gMnPc2	provinciál
<g/>
.	.	kIx.	.
</s>
<s>
Jezdci	jezdec	k1gMnPc1	jezdec
byli	být	k5eAaImAgMnP	být
preferováni	preferovat	k5eAaImNgMnP	preferovat
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
fluktuaci	fluktuace	k1gFnSc4	fluktuace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
společenských	společenský	k2eAgFnPc2d1	společenská
tříd	třída	k1gFnPc2	třída
a	a	k8xC	a
odstraňovalo	odstraňovat	k5eAaImAgNnS	odstraňovat
sociální	sociální	k2eAgFnPc4d1	sociální
přehrady	přehrada	k1gFnPc4	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
měli	mít	k5eAaImAgMnP	mít
nyní	nyní	k6eAd1	nyní
navíc	navíc	k6eAd1	navíc
mnohem	mnohem	k6eAd1	mnohem
snadnější	snadný	k2eAgFnSc4d2	snazší
možnost	možnost	k1gFnSc4	možnost
nabýt	nabýt	k5eAaPmF	nabýt
římské	římský	k2eAgNnSc4d1	římské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legitimita	legitimita	k1gFnSc1	legitimita
Augustovy	Augustův	k2eAgFnSc2d1	Augustova
vlády	vláda	k1gFnSc2	vláda
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
roli	role	k1gFnSc6	role
ochránce	ochránce	k1gMnSc2	ochránce
míru	mír	k1gInSc2	mír
před	před	k7c7	před
chaosem	chaos	k1gInSc7	chaos
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Imperium	Imperium	k1gNnSc1	Imperium
Romanum	Romanum	k?	Romanum
ovládalo	ovládat	k5eAaImAgNnS	ovládat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
římská	římský	k2eAgFnSc1d1	římská
expanze	expanze	k1gFnSc1	expanze
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
za	za	k7c4	za
Augusta	August	k1gMnSc4	August
<g/>
:	:	kIx,	:
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
až	až	k9	až
k	k	k7c3	k
Dunaji	Dunaj	k1gInSc3	Dunaj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
dobytí	dobytí	k1gNnSc1	dobytí
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
završeno	završen	k2eAgNnSc1d1	završeno
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Germánii	Germánie	k1gFnSc6	Germánie
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
zde	zde	k6eAd1	zde
plány	plán	k1gInPc4	plán
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
provincie	provincie	k1gFnSc2	provincie
ztroskotaly	ztroskotat	k5eAaPmAgInP	ztroskotat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
římská	římský	k2eAgFnSc1d1	římská
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
9	[number]	k4	9
n.	n.	k?	n.
l.	l.	k?	l.
poražena	poražen	k2eAgFnSc1d1	poražena
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Teutoburském	Teutoburský	k2eAgInSc6d1	Teutoburský
lese	les	k1gInSc6	les
Cherusky	Cherusky	k1gFnSc2	Cherusky
vedenými	vedený	k2eAgFnPc7d1	vedená
náčelníkem	náčelník	k1gInSc7	náčelník
Arminiem	Arminium	k1gNnSc7	Arminium
<g/>
.	.	kIx.	.
</s>
<s>
Augustus	Augustus	k1gMnSc1	Augustus
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
omezil	omezit	k5eAaPmAgInS	omezit
na	na	k7c4	na
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
stávajících	stávající	k2eAgFnPc2d1	stávající
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
kterých	který	k3yRgInPc2	který
bylo	být	k5eAaImAgNnS	být
rozmístěno	rozmístěn	k2eAgNnSc1d1	rozmístěno
vojsko	vojsko	k1gNnSc1	vojsko
čítající	čítající	k2eAgNnSc1d1	čítající
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přecházela	přecházet	k5eAaImAgFnS	přecházet
do	do	k7c2	do
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
soustřeďovala	soustřeďovat	k5eAaImAgFnS	soustřeďovat
svůj	svůj	k3xOyFgInSc4	svůj
vojenský	vojenský	k2eAgInSc4d1	vojenský
potenciál	potenciál	k1gInSc4	potenciál
k	k	k7c3	k
opevnění	opevnění	k1gNnSc3	opevnění
hranic	hranice	k1gFnPc2	hranice
(	(	kIx(	(
<g/>
Limes	Limes	k1gMnSc1	Limes
Romanus	Romanus	k1gMnSc1	Romanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
císařova	císařův	k2eAgNnPc1d1	císařovo
opatření	opatření	k1gNnPc1	opatření
přispěla	přispět	k5eAaPmAgNnP	přispět
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
tzv.	tzv.	kA	tzv.
římského	římský	k2eAgInSc2d1	římský
míru	mír	k1gInSc2	mír
–	–	k?	–
Pax	Pax	k1gMnSc2	Pax
Romana	Roman	k1gMnSc2	Roman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Augustova	Augustův	k2eAgFnSc1d1	Augustova
vláda	vláda	k1gFnSc1	vláda
přinesla	přinést	k5eAaPmAgFnS	přinést
státu	stát	k1gInSc3	stát
prospěch	prospěch	k1gInSc4	prospěch
a	a	k8xC	a
blahobyt	blahobyt	k1gInSc4	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
Nastolením	nastolení	k1gNnSc7	nastolení
principátu	principát	k1gInSc2	principát
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
epocha	epocha	k1gFnSc1	epocha
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
i	i	k8xC	i
vnější	vnější	k2eAgFnSc1d1	vnější
konsolidace	konsolidace	k1gFnSc1	konsolidace
<g/>
:	:	kIx,	:
císař	císař	k1gMnSc1	císař
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
síť	síť	k1gFnSc4	síť
nových	nový	k2eAgFnPc2d1	nová
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
zažily	zažít	k5eAaPmAgFnP	zažít
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
válek	válka	k1gFnPc2	válka
nový	nový	k2eAgInSc4d1	nový
rozkvět	rozkvět	k1gInSc4	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
podporována	podporován	k2eAgFnSc1d1	podporována
urbanizace	urbanizace	k1gFnSc1	urbanizace
provincií	provincie	k1gFnPc2	provincie
zřizováním	zřizování	k1gNnSc7	zřizování
kolonií	kolonie	k1gFnPc2	kolonie
veteránů	veterán	k1gMnPc2	veterán
a	a	k8xC	a
velkorysým	velkorysý	k2eAgNnSc7d1	velkorysé
udělováním	udělování	k1gNnSc7	udělování
římského	římský	k2eAgNnSc2d1	římské
občanství	občanství	k1gNnSc2	občanství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
rychlému	rychlý	k2eAgInSc3d1	rychlý
rozšiřovaní	rozšiřovaný	k2eAgMnPc1d1	rozšiřovaný
řecko-římské	řecko-římský	k2eAgFnSc2d1	řecko-římská
kultury	kultura	k1gFnSc2	kultura
do	do	k7c2	do
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
všem	všecek	k3xTgInPc3	všecek
opatřením	opatření	k1gNnSc7	opatření
k	k	k7c3	k
uchování	uchování	k1gNnSc3	uchování
tradičních	tradiční	k2eAgFnPc2d1	tradiční
římských	římský	k2eAgFnPc2d1	římská
institucí	instituce	k1gFnPc2	instituce
započal	započnout	k5eAaPmAgInS	započnout
již	již	k6eAd1	již
za	za	k7c2	za
Augustovy	Augustův	k2eAgFnSc2d1	Augustova
vlády	vláda	k1gFnSc2	vláda
pozvolný	pozvolný	k2eAgInSc1d1	pozvolný
proces	proces	k1gInSc1	proces
provincionalizace	provincionalizace	k1gFnSc2	provincionalizace
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
postupně	postupně	k6eAd1	postupně
klesal	klesat	k5eAaImAgMnS	klesat
význam	význam	k1gInSc4	význam
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
ostatních	ostatní	k2eAgInPc2d1	ostatní
leckdy	leckdy	k6eAd1	leckdy
teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
dobytých	dobytý	k2eAgFnPc2d1	dobytá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
vzestupu	vzestup	k1gInSc2	vzestup
provinciálů	provinciál	k1gMnPc2	provinciál
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Claudia	Claudia	k1gFnSc1	Claudia
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
stali	stát	k5eAaPmAgMnP	stát
senátory	senátor	k1gMnPc4	senátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Augustově	Augustův	k2eAgFnSc6d1	Augustova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
14	[number]	k4	14
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
jeho	jeho	k3xOp3gFnSc4	jeho
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
syn	syn	k1gMnSc1	syn
Tiberius	Tiberius	k1gMnSc1	Tiberius
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
lidsky	lidsky	k6eAd1	lidsky
komplikovanou	komplikovaný	k2eAgFnSc7d1	komplikovaná
osobností	osobnost	k1gFnSc7	osobnost
a	a	k8xC	a
vnitřně	vnitřně	k6eAd1	vnitřně
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
cítil	cítit	k5eAaImAgMnS	cítit
spíše	spíše	k9	spíše
republikánem	republikán	k1gMnSc7	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgInS	starat
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
hranic	hranice	k1gFnPc2	hranice
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
naplnění	naplnění	k1gNnSc2	naplnění
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
událostí	událost	k1gFnSc7	událost
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Tiberiova	Tiberiův	k2eAgNnSc2d1	Tiberiovo
panování	panování	k1gNnSc2	panování
bylo	být	k5eAaImAgNnS	být
ukřižování	ukřižování	k1gNnSc1	ukřižování
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Judea	Judea	k1gFnSc1	Judea
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
třetí	třetí	k4xOgInSc4	třetí
princeps	princeps	k1gInSc4	princeps
<g/>
,	,	kIx,	,
Caligula	Caligulum	k1gNnPc4	Caligulum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
tyranem	tyran	k1gMnSc7	tyran
par	para	k1gFnPc2	para
excellance	excellance	k1gFnPc4	excellance
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
skončila	skončit	k5eAaPmAgFnS	skončit
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
pretoriány	pretorián	k1gMnPc7	pretorián
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Claudia	Claudia	k1gFnSc1	Claudia
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
říši	říš	k1gFnSc3	říš
připojena	připojen	k2eAgFnSc1d1	připojena
Británie	Británie	k1gFnSc1	Británie
později	pozdě	k6eAd2	pozdě
následovaná	následovaný	k2eAgFnSc1d1	následovaná
Thrákií	Thrákie	k1gFnSc7	Thrákie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
status	status	k1gInSc4	status
klientského	klientský	k2eAgInSc2d1	klientský
státu	stát	k1gInSc2	stát
závislého	závislý	k2eAgInSc2d1	závislý
na	na	k7c6	na
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Claudiovi	Claudius	k1gMnSc6	Claudius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
otráven	otráven	k2eAgMnSc1d1	otráven
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Agrippinou	Agrippina	k1gFnSc7	Agrippina
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
vládu	vláda	k1gFnSc4	vláda
císařovnin	císařovnin	k2eAgMnSc1d1	císařovnin
syn	syn	k1gMnSc1	syn
Nero	Nero	k1gMnSc1	Nero
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zpočátku	zpočátku	k6eAd1	zpočátku
nadějné	nadějný	k2eAgNnSc1d1	nadějné
panování	panování	k1gNnSc1	panování
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c4	v
teror	teror	k1gInSc4	teror
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
nechal	nechat	k5eAaPmAgMnS	nechat
císař	císař	k1gMnSc1	císař
povraždit	povraždit	k5eAaPmF	povraždit
mnoho	mnoho	k4c1	mnoho
členů	člen	k1gInPc2	člen
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dal	dát	k5eAaPmAgInS	dát
zapálit	zapálit	k5eAaPmF	zapálit
Řím	Řím	k1gInSc1	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
pronásledování	pronásledování	k1gNnSc3	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
hrůzovládě	hrůzovláda	k1gFnSc3	hrůzovláda
vzbouřily	vzbouřit	k5eAaPmAgFnP	vzbouřit
legie	legie	k1gFnPc4	legie
<g/>
,	,	kIx,	,
spáchal	spáchat	k5eAaPmAgMnS	spáchat
Nero	Nero	k1gMnSc1	Nero
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Neronova	Neronův	k2eAgFnSc1d1	Neronova
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
68	[number]	k4	68
znamenala	znamenat	k5eAaImAgFnS	znamenat
konec	konec	k1gInSc4	konec
vlády	vláda	k1gFnSc2	vláda
julsko-klaudijské	julskolaudijský	k2eAgFnSc2d1	julsko-klaudijský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
také	také	k9	také
představuje	představovat	k5eAaImIp3nS	představovat
určitý	určitý	k2eAgInSc4d1	určitý
zlom	zlom	k1gInSc4	zlom
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
totiž	totiž	k9	totiž
jen	jen	k9	jen
málokterý	málokterý	k3yIgMnSc1	málokterý
císař	císař	k1gMnSc1	císař
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
staré	starý	k2eAgFnSc2d1	stará
římské	římský	k2eAgFnSc2d1	římská
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrchol	vrcholit	k5eAaImRp2nS	vrcholit
císařství	císařství	k1gNnSc4	císařství
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Neronově	Neronův	k2eAgFnSc6d1	Neronova
smrti	smrt	k1gFnSc6	smrt
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
chaos	chaos	k1gInSc1	chaos
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
rok	rok	k1gInSc4	rok
čtyř	čtyři	k4xCgMnPc2	čtyři
císařů	císař	k1gMnPc2	císař
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bojů	boj	k1gInPc2	boj
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
uchazeči	uchazeč	k1gMnPc7	uchazeč
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
vyšel	vyjít	k5eAaPmAgMnS	vyjít
vítězně	vítězně	k6eAd1	vítězně
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Vespasianus	Vespasianus	k1gMnSc1	Vespasianus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
druhou	druhý	k4xOgFnSc4	druhý
císařskou	císařský	k2eAgFnSc4d1	císařská
dynastii	dynastie	k1gFnSc4	dynastie
Flaviovců	Flaviovec	k1gMnPc2	Flaviovec
<g/>
.	.	kIx.	.
</s>
<s>
Vespasianus	Vespasianus	k1gInSc1	Vespasianus
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
uspořádat	uspořádat	k5eAaPmF	uspořádat
rozvrácené	rozvrácený	k2eAgFnPc4d1	rozvrácená
státní	státní	k2eAgFnPc4d1	státní
finance	finance	k1gFnPc4	finance
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
hranice	hranice	k1gFnPc4	hranice
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
východě	východ	k1gInSc6	východ
před	před	k7c7	před
pronikáním	pronikání	k1gNnSc7	pronikání
Parthů	Parth	k1gInPc2	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
vesměs	vesměs	k6eAd1	vesměs
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
vládě	vláda	k1gFnSc6	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
79	[number]	k4	79
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
císařem	císař	k1gMnSc7	císař
jeho	jeho	k3xOp3gMnSc7	jeho
syn	syn	k1gMnSc1	syn
Titus	Titus	k1gMnSc1	Titus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
70	[number]	k4	70
převzal	převzít	k5eAaPmAgMnS	převzít
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
otci	otec	k1gMnSc3	otec
velení	velení	k1gNnSc2	velení
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Titovi	Titův	k2eAgMnPc1d1	Titův
bylo	být	k5eAaImAgNnS	být
dopřáno	dopřát	k5eAaPmNgNnS	dopřát
jen	jen	k6eAd1	jen
krátké	krátký	k2eAgNnSc1d1	krátké
vládnutí	vládnutí	k1gNnSc1	vládnutí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
převážně	převážně	k6eAd1	převážně
katastrofami	katastrofa	k1gFnPc7	katastrofa
(	(	kIx(	(
<g/>
výbuch	výbuch	k1gInSc1	výbuch
Vesuvu	Vesuv	k1gInSc2	Vesuv
<g/>
,	,	kIx,	,
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
80	[number]	k4	80
dokončen	dokončen	k2eAgInSc4d1	dokončen
velkolepý	velkolepý	k2eAgInSc4d1	velkolepý
Flaviovský	Flaviovský	k2eAgInSc4d1	Flaviovský
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Titově	Titův	k2eAgInSc6d1	Titův
předčasném	předčasný	k2eAgInSc6d1	předčasný
skonu	skon	k1gInSc6	skon
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Domitianus	Domitianus	k1gMnSc1	Domitianus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Tacitem	Tacit	k1gInSc7	Tacit
<g/>
,	,	kIx,	,
vylíčena	vylíčen	k2eAgFnSc1d1	vylíčena
v	v	k7c6	v
temných	temný	k2eAgFnPc6d1	temná
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Domitianus	Domitianus	k1gMnSc1	Domitianus
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
prokazována	prokazován	k2eAgFnSc1d1	prokazována
božská	božský	k2eAgFnSc1d1	božská
úcta	úcta	k1gFnSc1	úcta
a	a	k8xC	a
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
se	se	k3xPyFc4	se
dominem	domino	k1gNnSc7	domino
(	(	kIx(	(
<g/>
pánem	pán	k1gMnSc7	pán
nad	nad	k7c4	nad
otroky	otrok	k1gMnPc4	otrok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
stylem	styl	k1gInSc7	styl
vlády	vláda	k1gFnSc2	vláda
provokoval	provokovat	k5eAaImAgMnS	provokovat
staré	starý	k2eAgFnPc4d1	stará
elity	elita	k1gFnPc4	elita
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
uznat	uznat	k5eAaPmF	uznat
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
nepochybné	pochybný	k2eNgInPc4d1	nepochybný
úspěchy	úspěch	k1gInPc4	úspěch
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
proti	proti	k7c3	proti
Britanům	Britan	k1gInPc3	Britan
<g/>
,	,	kIx,	,
Germánům	Germán	k1gMnPc3	Germán
a	a	k8xC	a
Dákům	Dák	k1gMnPc3	Dák
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgInSc1d1	efektivní
správní	správní	k2eAgInSc1d1	správní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
96	[number]	k4	96
však	však	k9	však
padl	padnout	k5eAaImAgInS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
palácovému	palácový	k2eAgInSc3d1	palácový
převratu	převrat	k1gInSc3	převrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgNnSc1d1	následující
období	období	k1gNnSc1	období
adoptivních	adoptivní	k2eAgMnPc2d1	adoptivní
císařů	císař	k1gMnPc2	císař
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
impéria	impérium	k1gNnSc2	impérium
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kulturního	kulturní	k2eAgNnSc2d1	kulturní
<g/>
,	,	kIx,	,
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
i	i	k8xC	i
mocenského	mocenský	k2eAgNnSc2d1	mocenské
<g/>
.	.	kIx.	.
</s>
<s>
Císaři	císař	k1gMnPc1	císař
vládli	vládnout	k5eAaImAgMnP	vládnout
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
respektoval	respektovat	k5eAaImAgMnS	respektovat
státní	státní	k2eAgNnSc4d1	státní
uspořádání	uspořádání	k1gNnSc4	uspořádání
principátu	principát	k1gInSc2	principát
<g/>
.	.	kIx.	.
</s>
<s>
Senátem	senát	k1gInSc7	senát
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
dosazený	dosazený	k2eAgMnSc1d1	dosazený
císař	císař	k1gMnSc1	císař
Nerva	rvát	k5eNaImSgInS	rvát
adoptoval	adoptovat	k5eAaPmAgInS	adoptovat
vojevůdce	vojevůdce	k1gMnSc4	vojevůdce
Traiana	Traian	k1gMnSc4	Traian
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jej	on	k3xPp3gMnSc4	on
učinil	učinit	k5eAaPmAgMnS	učinit
svým	svůj	k3xOyFgMnSc7	svůj
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Traianus	Traianus	k1gMnSc1	Traianus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgMnSc7	první
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nepocházel	pocházet	k5eNaImAgMnS	pocházet
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
z	z	k7c2	z
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
oslavován	oslavován	k2eAgInSc1d1	oslavován
jako	jako	k8xS	jako
optimus	optimus	k1gInSc1	optimus
princeps	princepsa	k1gFnPc2	princepsa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
císař	císař	k1gMnSc1	císař
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
117	[number]	k4	117
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
územního	územní	k2eAgInSc2d1	územní
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězných	vítězný	k2eAgFnPc6d1	vítězná
válkách	válka	k1gFnPc6	válka
s	s	k7c7	s
Dáky	Dák	k1gMnPc7	Dák
a	a	k8xC	a
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
východním	východní	k2eAgNnSc6d1	východní
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
Parthům	Parth	k1gInPc3	Parth
sahala	sahat	k5eAaImAgFnS	sahat
římská	římský	k2eAgFnSc1d1	římská
moc	moc	k1gFnSc1	moc
od	od	k7c2	od
Skotska	Skotsko	k1gNnSc2	Skotsko
až	až	k9	až
po	po	k7c6	po
Núbii	Núbie	k1gFnSc6	Núbie
v	v	k7c6	v
severojižním	severojižní	k2eAgInSc6d1	severojižní
směru	směr	k1gInSc6	směr
a	a	k8xC	a
od	od	k7c2	od
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
až	až	k9	až
k	k	k7c3	k
Mezopotámii	Mezopotámie	k1gFnSc3	Mezopotámie
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Traianův	Traianův	k2eAgMnSc1d1	Traianův
nástupce	nástupce	k1gMnSc1	nástupce
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
se	se	k3xPyFc4	se
však	však	k9	však
zisků	zisk	k1gInPc2	zisk
z	z	k7c2	z
výbojů	výboj	k1gInPc2	výboj
východně	východně	k6eAd1	východně
od	od	k7c2	od
Eufratu	Eufrat	k1gInSc2	Eufrat
prozíravě	prozíravě	k6eAd1	prozíravě
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byly	být	k5eAaImAgInP	být
neudržitelné	udržitelný	k2eNgInPc1d1	neudržitelný
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
tohoto	tento	k3xDgMnSc2	tento
vzdělaného	vzdělaný	k2eAgMnSc2d1	vzdělaný
helénofila	helénofil	k1gMnSc2	helénofil
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
konsolidaci	konsolidace	k1gFnSc3	konsolidace
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
k	k	k7c3	k
civilizačnímu	civilizační	k2eAgNnSc3d1	civilizační
<g/>
,	,	kIx,	,
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
a	a	k8xC	a
technickému	technický	k2eAgInSc3d1	technický
rozkvětu	rozkvět	k1gInSc3	rozkvět
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
napomohl	napomoct	k5eAaPmAgMnS	napomoct
rozšíření	rozšíření	k1gNnSc4	rozšíření
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
mladého	mladý	k2eAgMnSc2d1	mladý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k9	již
značně	značně	k6eAd1	značně
rostoucího	rostoucí	k2eAgNnSc2d1	rostoucí
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
především	především	k9	především
výstavbě	výstavba	k1gFnSc3	výstavba
efektivního	efektivní	k2eAgNnSc2d1	efektivní
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
opevnění	opevnění	k1gNnSc2	opevnění
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Hadriánův	Hadriánův	k2eAgInSc1d1	Hadriánův
val	val	k1gInSc1	val
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
moderní	moderní	k2eAgMnPc1d1	moderní
historikové	historik	k1gMnPc1	historik
mu	on	k3xPp3gMnSc3	on
vytýkají	vytýkat	k5eAaImIp3nP	vytýkat
silné	silný	k2eAgNnSc1d1	silné
zadlužení	zadlužení	k1gNnSc4	zadlužení
státních	státní	k2eAgFnPc2d1	státní
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
začaly	začít	k5eAaPmAgFnP	začít
projevovat	projevovat	k5eAaImF	projevovat
první	první	k4xOgInPc1	první
příznaky	příznak	k1gInPc1	příznak
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nevyžadovaly	vyžadovat	k5eNaImAgFnP	vyžadovat
přijetí	přijetí	k1gNnSc4	přijetí
zvláštních	zvláštní	k2eAgNnPc2d1	zvláštní
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Antonina	Antonin	k2eAgMnSc2d1	Antonin
Pia	Pius	k1gMnSc2	Pius
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
impérium	impérium	k1gNnSc1	impérium
vrcholu	vrchol	k1gInSc2	vrchol
svého	svůj	k3xOyFgInSc2	svůj
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
"	"	kIx"	"
<g/>
filozofa	filozof	k1gMnSc2	filozof
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
<g/>
"	"	kIx"	"
Marca	Marc	k2eAgFnSc1d1	Marca
Aurelia	Aurelia	k1gFnSc1	Aurelia
(	(	kIx(	(
<g/>
161	[number]	k4	161
<g/>
–	–	k?	–
<g/>
180	[number]	k4	180
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
první	první	k4xOgInPc1	první
závažné	závažný	k2eAgInPc1d1	závažný
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ohromná	ohromný	k2eAgFnSc1d1	ohromná
masa	masa	k1gFnSc1	masa
germánských	germánský	k2eAgInPc2d1	germánský
a	a	k8xC	a
sarmatských	sarmatský	k2eAgInPc2d1	sarmatský
kmenů	kmen	k1gInPc2	kmen
překročila	překročit	k5eAaPmAgNnP	překročit
takřka	takřka	k6eAd1	takřka
podél	podél	k7c2	podél
celého	celý	k2eAgInSc2d1	celý
Dunaje	Dunaj	k1gInSc2	Dunaj
hranice	hranice	k1gFnSc1	hranice
říše	říše	k1gFnSc1	říše
a	a	k8xC	a
ohrožovala	ohrožovat	k5eAaImAgFnS	ohrožovat
i	i	k9	i
samotnou	samotný	k2eAgFnSc4d1	samotná
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Markomanů	Markoman	k1gMnPc2	Markoman
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
boje	boj	k1gInPc1	boj
nazývány	nazýván	k2eAgInPc1d1	nazýván
markomanskými	markomanský	k2eAgFnPc7d1	markomanská
válkami	válka	k1gFnPc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
Parthové	Parth	k1gMnPc1	Parth
napadli	napadnout	k5eAaPmAgMnP	napadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
161	[number]	k4	161
východní	východní	k2eAgFnSc2d1	východní
provincie	provincie	k1gFnSc2	provincie
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Vítězná	vítězný	k2eAgNnPc1d1	vítězné
římská	římský	k2eAgNnPc1d1	římské
vojska	vojsko	k1gNnPc1	vojsko
zavlekla	zavleknout	k5eAaPmAgNnP	zavleknout
z	z	k7c2	z
východu	východ	k1gInSc2	východ
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
mor	mor	k1gInSc1	mor
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
antoniniánská	antoniniánský	k2eAgFnSc1d1	antoniniánský
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgNnPc2	tento
vážných	vážný	k2eAgNnPc2d1	vážné
vnějších	vnější	k2eAgNnPc2d1	vnější
ohrožení	ohrožení	k1gNnPc2	ohrožení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
vynaložení	vynaložení	k1gNnSc4	vynaložení
četných	četný	k2eAgInPc2d1	četný
zdrojů	zdroj	k1gInPc2	zdroj
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
také	také	k9	také
první	první	k4xOgFnPc1	první
známky	známka	k1gFnPc1	známka
rozkladu	rozklad	k1gInSc2	rozklad
uvnitř	uvnitř	k7c2	uvnitř
římského	římský	k2eAgInSc2d1	římský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnPc1d1	státní
finance	finance	k1gFnPc1	finance
se	se	k3xPyFc4	se
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
v	v	k7c6	v
nerovnováze	nerovnováha	k1gFnSc6	nerovnováha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
napravena	napravit	k5eAaPmNgFnS	napravit
jen	jen	k6eAd1	jen
zhoršením	zhoršení	k1gNnSc7	zhoršení
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Marcu	Marc	k1gMnSc6	Marc
Aureliovi	Aurelius	k1gMnSc6	Aurelius
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Commodus	Commodus	k1gMnSc1	Commodus
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
Nero	Nero	k1gMnSc1	Nero
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
192	[number]	k4	192
<g/>
,	,	kIx,	,
když	když	k8xS	když
senátoři	senátor	k1gMnPc1	senátor
nebyli	být	k5eNaImAgMnP	být
nadále	nadále	k6eAd1	nadále
schopní	schopný	k2eAgMnPc1d1	schopný
snášet	snášet	k5eAaImF	snášet
jeho	jeho	k3xOp3gFnSc4	jeho
tyranii	tyranie	k1gFnSc4	tyranie
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
následovalo	následovat	k5eAaImAgNnS	následovat
další	další	k2eAgNnSc1d1	další
kolo	kolo	k1gNnSc1	kolo
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Severovci	Severovec	k1gMnPc1	Severovec
a	a	k8xC	a
vojenští	vojenský	k2eAgMnPc1d1	vojenský
císaři	císař	k1gMnPc1	císař
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
193	[number]	k4	193
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
Septimius	Septimius	k1gMnSc1	Septimius
Severus	Severus	k1gMnSc1	Severus
–	–	k?	–
první	první	k4xOgNnSc4	první
císař	císař	k1gMnSc1	císař
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c4	nad
Parthy	Partha	k1gFnPc4	Partha
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
říši	říš	k1gFnSc3	říš
dočasně	dočasně	k6eAd1	dočasně
navrátil	navrátit	k5eAaPmAgMnS	navrátit
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
ale	ale	k8xC	ale
započal	započnout	k5eAaPmAgInS	započnout
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
vzrůst	vzrůst	k1gInSc1	vzrůst
moci	moc	k1gFnSc2	moc
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Caracalla	Caracalla	k1gMnSc1	Caracalla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
navzdory	navzdory	k7c3	navzdory
svému	svůj	k3xOyFgNnSc3	svůj
krutému	krutý	k2eAgNnSc3d1	kruté
chování	chování	k1gNnSc3	chování
k	k	k7c3	k
senátu	senát	k1gInSc3	senát
a	a	k8xC	a
k	k	k7c3	k
vlastním	vlastní	k2eAgMnPc3d1	vlastní
příbuzným	příbuzný	k1gMnPc3	příbuzný
těšil	těšit	k5eAaImAgInS	těšit
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
vojáky	voják	k1gMnPc7	voják
jisté	jistý	k2eAgFnSc3d1	jistá
popularitě	popularita	k1gFnSc3	popularita
<g/>
,	,	kIx,	,
padl	padnout	k5eAaImAgInS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
atentátu	atentát	k1gInSc2	atentát
během	během	k7c2	během
parthského	parthský	k2eAgNnSc2d1	parthský
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
krátká	krátký	k2eAgFnSc1d1	krátká
vláda	vláda	k1gFnSc1	vláda
Elagabala	Elagabal	k1gMnSc2	Elagabal
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c6	o
zavedení	zavedení	k1gNnSc6	zavedení
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
orientálního	orientální	k2eAgNnSc2d1	orientální
božstva	božstvo	k1gNnSc2	božstvo
jako	jako	k8xC	jako
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
státního	státní	k2eAgNnSc2d1	státní
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
222	[number]	k4	222
byl	být	k5eAaImAgInS	být
nenáviděný	nenáviděný	k2eAgInSc1d1	nenáviděný
a	a	k8xC	a
zhýralý	zhýralý	k2eAgInSc1d1	zhýralý
Elagabalus	Elagabalus	k1gInSc1	Elagabalus
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Alexander	Alexandra	k1gFnPc2	Alexandra
Severus	Severus	k1gMnSc1	Severus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
snažil	snažit	k5eAaImAgMnS	snažit
osvědčit	osvědčit	k5eAaPmF	osvědčit
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
proti	proti	k7c3	proti
Sásánovcům	Sásánovec	k1gMnPc3	Sásánovec
a	a	k8xC	a
Germánům	Germán	k1gMnPc3	Germán
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
235	[number]	k4	235
byl	být	k5eAaImAgInS	být
nespokojenými	spokojený	k2eNgFnPc7d1	nespokojená
vojáky	voják	k1gMnPc4	voják
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
císaři	císař	k1gMnPc1	císař
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
zajistili	zajistit	k5eAaPmAgMnP	zajistit
loajalitu	loajalita	k1gFnSc4	loajalita
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
udělovali	udělovat	k5eAaImAgMnP	udělovat
vojákům	voják	k1gMnPc3	voják
peněžité	peněžitý	k2eAgFnPc1d1	peněžitá
dary	dar	k1gInPc4	dar
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
výše	výše	k1gFnSc1	výše
neustále	neustále	k6eAd1	neustále
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Commoda	Commoda	k1gFnSc1	Commoda
bylo	být	k5eAaImAgNnS	být
císařství	císařství	k1gNnSc1	císařství
regulérně	regulérně	k6eAd1	regulérně
vydražováno	vydražovat	k5eAaImNgNnS	vydražovat
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
především	především	k6eAd1	především
pretoriánská	pretoriánský	k2eAgFnSc1d1	pretoriánská
garda	garda	k1gFnSc1	garda
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
při	při	k7c6	při
destabilizaci	destabilizace	k1gFnSc6	destabilizace
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Septimius	Septimius	k1gMnSc1	Septimius
Severus	Severus	k1gMnSc1	Severus
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
snažil	snažit	k5eAaImAgMnS	snažit
čelit	čelit	k5eAaImF	čelit
zrušením	zrušení	k1gNnSc7	zrušení
těchto	tento	k3xDgInPc2	tento
darů	dar	k1gInPc2	dar
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
drastickým	drastický	k2eAgInSc7d1	drastický
způsobem	způsob	k1gInSc7	způsob
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
žold	žold	k1gInSc1	žold
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
staly	stát	k5eAaPmAgFnP	stát
nesnesitelnými	snesitelný	k2eNgNnPc7d1	nesnesitelné
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
žoldu	žold	k1gInSc2	žold
rozhoupala	rozhoupat	k5eAaPmAgFnS	rozhoupat
spirálu	spirála	k1gFnSc4	spirála
zvyšování	zvyšování	k1gNnSc2	zvyšování
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
znehodnocování	znehodnocování	k1gNnSc3	znehodnocování
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
byl	být	k5eAaImAgMnS	být
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
úpadek	úpadek	k1gInSc4	úpadek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
ničivý	ničivý	k2eAgInSc4d1	ničivý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
stabilitu	stabilita	k1gFnSc4	stabilita
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
a	a	k8xC	a
mocenské	mocenský	k2eAgNnSc1d1	mocenské
těžiště	těžiště	k1gNnSc1	těžiště
říše	říš	k1gFnSc2	říš
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
do	do	k7c2	do
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
rekrutovaly	rekrutovat	k5eAaImAgFnP	rekrutovat
římské	římský	k2eAgFnPc4d1	římská
elity	elita	k1gFnPc4	elita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
posílen	posílit	k5eAaPmNgInS	posílit
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Septimia	Septimius	k1gMnSc2	Septimius
Severa	Severa	k1gMnSc1	Severa
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
důstojnické	důstojnický	k2eAgFnPc4d1	důstojnická
hodnosti	hodnost	k1gFnPc4	hodnost
v	v	k7c6	v
armádě	armáda	k1gFnSc3	armáda
všem	všecek	k3xTgMnPc3	všecek
vojákům	voják	k1gMnPc3	voják
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
pozvolna	pozvolna	k6eAd1	pozvolna
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
své	svůj	k3xOyFgNnSc4	svůj
privilegované	privilegovaný	k2eAgNnSc4d1	privilegované
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
završeno	završit	k5eAaPmNgNnS	završit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
212	[number]	k4	212
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Caracalla	Caracalla	k1gMnSc1	Caracalla
udělil	udělit	k5eAaPmAgMnS	udělit
všem	všecek	k3xTgMnPc3	všecek
svobodným	svobodný	k2eAgMnPc3d1	svobodný
obyvatelům	obyvatel	k1gMnPc3	obyvatel
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgNnSc4d1	římské
občanství	občanství	k1gNnSc4	občanství
(	(	kIx(	(
<g/>
Constitutio	Constitutio	k6eAd1	Constitutio
Antoniniana	Antoninian	k1gMnSc2	Antoninian
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Severovců	Severovec	k1gMnPc2	Severovec
se	s	k7c7	s
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
proměnila	proměnit	k5eAaPmAgFnS	proměnit
také	také	k9	také
zahraničněpolitická	zahraničněpolitický	k2eAgFnSc1d1	zahraničněpolitická
pozice	pozice	k1gFnSc1	pozice
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
:	:	kIx,	:
na	na	k7c6	na
východě	východ	k1gInSc6	východ
nahradila	nahradit	k5eAaPmAgFnS	nahradit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
224	[number]	k4	224
slabou	slabý	k2eAgFnSc4d1	slabá
parthskou	parthský	k2eAgFnSc4d1	parthský
říši	říše	k1gFnSc4	říše
vláda	vláda	k1gFnSc1	vláda
dynastie	dynastie	k1gFnSc1	dynastie
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
nastal	nastat	k5eAaPmAgInS	nastat
mezi	mezi	k7c7	mezi
Rýnem	Rýn	k1gInSc7	Rýn
a	a	k8xC	a
Dunajem	Dunaj	k1gInSc7	Dunaj
proces	proces	k1gInSc1	proces
konstituování	konstituování	k1gNnSc2	konstituování
mohutných	mohutný	k2eAgInPc2d1	mohutný
germánských	germánský	k2eAgInPc2d1	germánský
kmenových	kmenový	k2eAgInPc2d1	kmenový
svazů	svaz	k1gInPc2	svaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
útočily	útočit	k5eAaImAgInP	útočit
na	na	k7c4	na
římské	římský	k2eAgFnPc4d1	římská
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
pronikaly	pronikat	k5eAaImAgFnP	pronikat
hluboko	hluboko	k6eAd1	hluboko
na	na	k7c4	na
římské	římský	k2eAgNnSc4d1	římské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rokem	rok	k1gInSc7	rok
235	[number]	k4	235
začalo	začít	k5eAaPmAgNnS	začít
neblahé	blahý	k2eNgNnSc1d1	neblahé
období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
vojenských	vojenský	k2eAgMnPc2d1	vojenský
císařů	císař	k1gMnPc2	císař
známé	známá	k1gFnSc2	známá
jako	jako	k8xS	jako
krize	krize	k1gFnSc2	krize
třetího	třetí	k4xOgInSc2	třetí
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nepřetržitá	přetržitý	k2eNgFnSc1d1	nepřetržitá
série	série	k1gFnSc1	série
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
uzurpací	uzurpace	k1gFnPc2	uzurpace
<g/>
,	,	kIx,	,
vzpour	vzpoura	k1gFnPc2	vzpoura
a	a	k8xC	a
vraždění	vraždění	k1gNnPc2	vraždění
císařů	císař	k1gMnPc2	císař
oslabila	oslabit	k5eAaPmAgFnS	oslabit
obranu	obrana	k1gFnSc4	obrana
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
učinila	učinit	k5eAaPmAgFnS	učinit
jí	on	k3xPp3gFnSc3	on
tak	tak	k6eAd1	tak
bezmocnou	bezmocný	k2eAgFnSc7d1	bezmocná
vůči	vůči	k7c3	vůči
útokům	útok	k1gInPc3	útok
jejích	její	k3xOp3gMnPc2	její
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
mezi	mezi	k7c7	mezi
zavražděním	zavraždění	k1gNnSc7	zavraždění
Alexandra	Alexandr	k1gMnSc2	Alexandr
Severa	Severa	k1gMnSc1	Severa
a	a	k8xC	a
nástupem	nástup	k1gInSc7	nástup
Diocletiana	Diocletian	k1gMnSc2	Diocletian
se	se	k3xPyFc4	se
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
celkem	celkem	k6eAd1	celkem
dvacet	dvacet	k4xCc4	dvacet
jedna	jeden	k4xCgFnSc1	jeden
legitimních	legitimní	k2eAgMnPc2d1	legitimní
císařů	císař	k1gMnPc2	císař
a	a	k8xC	a
nesčíslné	sčíslný	k2eNgNnSc1d1	nesčíslné
množství	množství	k1gNnSc1	množství
nelegitimních	legitimní	k2eNgMnPc2d1	nelegitimní
uzurpátorů	uzurpátor	k1gMnPc2	uzurpátor
<g/>
.	.	kIx.	.
</s>
<s>
Vojenští	vojenský	k2eAgMnPc1d1	vojenský
císaři	císař	k1gMnPc1	císař
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
vypořádávat	vypořádávat	k5eAaImF	vypořádávat
s	s	k7c7	s
útoky	útok	k1gInPc7	útok
Germánů	Germán	k1gMnPc2	Germán
na	na	k7c6	na
Rýně	Rýn	k1gInSc6	Rýn
a	a	k8xC	a
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Gótové	Gót	k1gMnPc1	Gót
sídlící	sídlící	k2eAgMnPc1d1	sídlící
při	při	k7c6	při
severních	severní	k2eAgInPc6d1	severní
březích	břeh	k1gInPc6	břeh
Černého	Černý	k1gMnSc2	Černý
moře	moře	k1gNnSc2	moře
podnikali	podnikat	k5eAaImAgMnP	podnikat
ničivé	ničivý	k2eAgInPc4d1	ničivý
nájezdy	nájezd	k1gInPc4	nájezd
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
a	a	k8xC	a
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
útočili	útočit	k5eAaImAgMnP	útočit
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c4	na
města	město	k1gNnPc4	město
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
Alamani	Alaman	k1gMnPc1	Alaman
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Galii	Galie	k1gFnSc3	Galie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejtěžší	těžký	k2eAgInPc1d3	nejtěžší
boje	boj	k1gInPc1	boj
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
s	s	k7c7	s
novoperskou	novoperský	k2eAgFnSc7d1	novoperská
sásánovskou	sásánovský	k2eAgFnSc7d1	sásánovská
říší	říš	k1gFnSc7	říš
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
mnohem	mnohem	k6eAd1	mnohem
vážnější	vážní	k2eAgMnSc1d2	vážnější
protivník	protivník	k1gMnSc1	protivník
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
než	než	k8xS	než
jakým	jaký	k3yQgMnPc3	jaký
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
Parthové	Parth	k1gMnPc1	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Perský	perský	k2eAgMnSc1d1	perský
velkokrál	velkokrál	k1gMnSc1	velkokrál
Šápúr	Šápúr	k1gMnSc1	Šápúr
I.	I.	kA	I.
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
porážel	porážet	k5eAaImAgMnS	porážet
římské	římský	k2eAgFnSc2d1	římská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
260	[number]	k4	260
padl	padnout	k5eAaImAgInS	padnout
do	do	k7c2	do
jeho	jeho	k3xOp3gFnPc2	jeho
rukou	ruka	k1gFnPc2	ruka
císař	císař	k1gMnSc1	císař
Valerianus	Valerianus	k1gMnSc1	Valerianus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poté	poté	k6eAd1	poté
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
perském	perský	k2eAgNnSc6d1	perské
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
snažili	snažit	k5eAaImAgMnP	snažit
udržet	udržet	k5eAaPmF	udržet
východní	východní	k2eAgFnPc4d1	východní
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
římská	římský	k2eAgFnSc1d1	římská
vláda	vláda	k1gFnSc1	vláda
rozkládat	rozkládat	k5eAaImF	rozkládat
i	i	k9	i
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Správci	správce	k1gMnPc1	správce
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
veleli	velet	k5eAaImAgMnP	velet
mnoha	mnoho	k4c2	mnoho
legiím	legie	k1gFnPc3	legie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
častěji	často	k6eAd2	často
využívali	využívat	k5eAaPmAgMnP	využívat
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
neustálým	neustálý	k2eAgFnPc3d1	neustálá
uzurpacím	uzurpace	k1gFnPc3	uzurpace
či	či	k8xC	či
k	k	k7c3	k
odpadnutím	odpadnutí	k1gNnSc7	odpadnutí
celých	celý	k2eAgFnPc2d1	celá
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
galské	galský	k2eAgNnSc1d1	galské
císařství	císařství	k1gNnSc1	císařství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
konflikty	konflikt	k1gInPc1	konflikt
a	a	k8xC	a
vnější	vnější	k2eAgNnSc1d1	vnější
ohrožení	ohrožení	k1gNnSc1	ohrožení
přivedly	přivést	k5eAaPmAgInP	přivést
říši	říše	k1gFnSc4	říše
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
záhuby	záhuba	k1gFnSc2	záhuba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prohlubování	prohlubování	k1gNnSc1	prohlubování
chaosu	chaos	k1gInSc2	chaos
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k9	až
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Claudia	Claudia	k1gFnSc1	Claudia
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
269	[number]	k4	269
podařilo	podařit	k5eAaPmAgNnS	podařit
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c7	nad
Góty	Gót	k1gMnPc7	Gót
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Naissu	Naiss	k1gInSc2	Naiss
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Niš	Niš	k1gFnSc1	Niš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
272	[number]	k4	272
si	se	k3xPyFc3	se
císař	císař	k1gMnSc1	císař
Aurelianus	Aurelianus	k1gMnSc1	Aurelianus
podrobil	podrobit	k5eAaPmAgMnS	podrobit
Palmýru	Palmýra	k1gFnSc4	Palmýra
<g/>
,	,	kIx,	,
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
římského	římský	k2eAgMnSc2d1	římský
spojence	spojenec	k1gMnSc2	spojenec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
královna	královna	k1gFnSc1	královna
Zenobia	Zenobium	k1gNnSc2	Zenobium
využila	využít	k5eAaPmAgFnS	využít
římské	římský	k2eAgFnPc4d1	římská
slabosti	slabost	k1gFnPc4	slabost
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
se	se	k3xPyFc4	se
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
východních	východní	k2eAgFnPc2d1	východní
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Aurelianus	Aurelianus	k1gMnSc1	Aurelianus
porazil	porazit	k5eAaPmAgMnS	porazit
také	také	k9	také
Germány	Germán	k1gMnPc4	Germán
a	a	k8xC	a
obnovil	obnovit	k5eAaPmAgMnS	obnovit
římskou	římský	k2eAgFnSc4d1	římská
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Galií	Galie	k1gFnSc7	Galie
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
značným	značný	k2eAgFnPc3d1	značná
společenským	společenský	k2eAgFnPc3d1	společenská
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
nezasáhly	zasáhnout	k5eNaPmAgFnP	zasáhnout
všechna	všechen	k3xTgNnPc1	všechen
území	území	k1gNnPc1	území
říše	říš	k1gFnSc2	říš
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Římanům	Říman	k1gMnPc3	Říman
se	se	k3xPyFc4	se
tak	tak	k9	tak
podařilo	podařit	k5eAaPmAgNnS	podařit
odvrátit	odvrátit	k5eAaPmF	odvrátit
hrozící	hrozící	k2eAgInSc4d1	hrozící
zánik	zánik	k1gInSc4	zánik
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nestabilita	nestabilita	k1gFnSc1	nestabilita
a	a	k8xC	a
chaos	chaos	k1gInSc4	chaos
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
příznivé	příznivý	k2eAgFnPc1d1	příznivá
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
stoupenci	stoupenec	k1gMnPc1	stoupenec
hlásali	hlásat	k5eAaImAgMnP	hlásat
mír	mír	k1gInSc4	mír
a	a	k8xC	a
spásu	spása	k1gFnSc4	spása
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
náboženství	náboženství	k1gNnPc2	náboženství
a	a	k8xC	a
pohanských	pohanský	k2eAgInPc2d1	pohanský
kultů	kult	k1gInPc2	kult
však	však	k9	však
křesťané	křesťan	k1gMnPc1	křesťan
neprojevovali	projevovat	k5eNaImAgMnP	projevovat
patřičný	patřičný	k2eAgInSc4d1	patřičný
respekt	respekt	k1gInSc4	respekt
k	k	k7c3	k
instituci	instituce	k1gFnSc3	instituce
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Decius	Decius	k1gMnSc1	Decius
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
obnovit	obnovit	k5eAaPmF	obnovit
uctívání	uctívání	k1gNnSc4	uctívání
starých	starý	k2eAgNnPc2d1	staré
božstev	božstvo	k1gNnPc2	božstvo
<g/>
,	,	kIx,	,
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
říše	říš	k1gFnSc2	říš
povinně	povinně	k6eAd1	povinně
obětovali	obětovat	k5eAaBmAgMnP	obětovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tomuto	tento	k3xDgMnSc3	tento
nařízení	nařízený	k2eAgMnPc1d1	nařízený
křesťané	křesťan	k1gMnPc1	křesťan
vzpírali	vzpírat	k5eAaImAgMnP	vzpírat
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgMnS	nařídit
jejich	jejich	k3xOp3gNnPc4	jejich
pronásledování	pronásledování	k1gNnPc4	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Deciovi	Decius	k1gMnSc6	Decius
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
křesťany	křesťan	k1gMnPc7	křesťan
také	také	k6eAd1	také
Valerianus	Valerianus	k1gMnSc1	Valerianus
a	a	k8xC	a
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nástup	nástup	k1gInSc1	nástup
pozdní	pozdní	k2eAgFnSc2d1	pozdní
antiky	antika	k1gFnSc2	antika
==	==	k?	==
</s>
</p>
<p>
<s>
Nástup	nástup	k1gInSc1	nástup
Diocletiana	Diocletian	k1gMnSc2	Diocletian
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
284	[number]	k4	284
symbolizoval	symbolizovat	k5eAaImAgInS	symbolizovat
počátek	počátek	k1gInSc4	počátek
období	období	k1gNnSc2	období
pozdní	pozdní	k2eAgFnSc2d1	pozdní
antiky	antika	k1gFnSc2	antika
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
postupující	postupující	k2eAgFnSc7d1	postupující
centralizací	centralizace	k1gFnSc7	centralizace
a	a	k8xC	a
byrokratizací	byrokratizace	k1gFnSc7	byrokratizace
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
pozdějším	pozdní	k2eAgNnSc7d2	pozdější
vítězstvím	vítězství	k1gNnSc7	vítězství
křesťanství	křesťanství	k1gNnSc2	křesťanství
nad	nad	k7c7	nad
pohanstvím	pohanství	k1gNnSc7	pohanství
<g/>
.	.	kIx.	.
</s>
<s>
Diocletianovi	Diocletian	k1gMnSc3	Diocletian
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
opatření	opatření	k1gNnPc2	opatření
a	a	k8xC	a
reforem	reforma	k1gFnPc2	reforma
odvrátit	odvrátit	k5eAaPmF	odvrátit
hrozící	hrozící	k2eAgInSc4d1	hrozící
zánik	zánik	k1gInSc4	zánik
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
–	–	k?	–
principát	principát	k1gInSc1	principát
–	–	k?	–
přitom	přitom	k6eAd1	přitom
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
nových	nový	k2eAgInPc6d1	nový
základech	základ	k1gInPc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
provedl	provést	k5eAaPmAgMnS	provést
reformu	reforma	k1gFnSc4	reforma
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
níž	jenž	k3xRgFnSc2	jenž
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
oddělena	oddělen	k2eAgFnSc1d1	oddělena
civilní	civilní	k2eAgFnSc1d1	civilní
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
jejich	jejich	k3xOp3gMnPc3	jejich
správcům	správce	k1gMnPc3	správce
zamezit	zamezit	k5eAaPmF	zamezit
ve	v	k7c6	v
vzpouře	vzpoura	k1gFnSc6	vzpoura
proti	proti	k7c3	proti
ústřední	ústřední	k2eAgFnSc3d1	ústřední
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Stanovením	stanovení	k1gNnSc7	stanovení
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
možných	možný	k2eAgFnPc2d1	možná
cen	cena	k1gFnPc2	cena
se	se	k3xPyFc4	se
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
poklesem	pokles	k1gInSc7	pokles
a	a	k8xC	a
utlumit	utlumit	k5eAaPmF	utlumit
inflaci	inflace	k1gFnSc4	inflace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
řádila	řádit	k5eAaImAgFnS	řádit
už	už	k6eAd1	už
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
hranic	hranice	k1gFnPc2	hranice
říše	říš	k1gFnSc2	říš
provedl	provést	k5eAaPmAgMnS	provést
reformu	reforma	k1gFnSc4	reforma
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
ale	ale	k9	ale
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
zvýšení	zvýšení	k1gNnSc4	zvýšení
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
vysokých	vysoký	k2eAgFnPc2d1	vysoká
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
dále	daleko	k6eAd2	daleko
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
vlády	vláda	k1gFnSc2	vláda
zvaný	zvaný	k2eAgMnSc1d1	zvaný
tetrarchie	tetrarchie	k1gFnPc4	tetrarchie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
čtyřvládí	čtyřvládit	k5eAaImIp3nS	čtyřvládit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
kolegu	kolega	k1gMnSc4	kolega
a	a	k8xC	a
druhého	druhý	k4xOgMnSc2	druhý
augusta	august	k1gMnSc2	august
povolal	povolat	k5eAaPmAgMnS	povolat
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
Maximiana	Maximian	k1gMnSc4	Maximian
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
císaři	císař	k1gMnPc1	císař
později	pozdě	k6eAd2	pozdě
jmenovali	jmenovat	k5eAaImAgMnP	jmenovat
po	po	k7c6	po
jednom	jeden	k4xCgMnSc6	jeden
caesarovi	caesar	k1gMnSc6	caesar
jako	jako	k8xS	jako
svých	svůj	k3xOyFgInPc6	svůj
zástupcích	zástupek	k1gInPc6	zástupek
(	(	kIx(	(
<g/>
Constantius	Constantius	k1gMnSc1	Constantius
I.	I.	kA	I.
Chlorus	Chlorus	k1gMnSc1	Chlorus
a	a	k8xC	a
Galerius	Galerius	k1gMnSc1	Galerius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
této	tento	k3xDgFnSc2	tento
reformy	reforma	k1gFnSc2	reforma
byla	být	k5eAaImAgFnS	být
jednak	jednak	k8xC	jednak
snaha	snaha	k1gFnSc1	snaha
zabránit	zabránit	k5eAaPmF	zabránit
uzurpacím	uzurpace	k1gFnPc3	uzurpace
velitelů	velitel	k1gMnPc2	velitel
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
armád	armáda	k1gFnPc2	armáda
a	a	k8xC	a
jednak	jednak	k8xC	jednak
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
silách	síla	k1gFnPc6	síla
jediného	jediný	k2eAgMnSc2d1	jediný
člověka	člověk	k1gMnSc2	člověk
efektivně	efektivně	k6eAd1	efektivně
ovládat	ovládat	k5eAaImF	ovládat
celý	celý	k2eAgInSc4d1	celý
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
rozdělení	rozdělení	k1gNnSc2	rozdělení
vlády	vláda	k1gFnSc2	vláda
nebyla	být	k5eNaImAgFnS	být
úplnou	úplný	k2eAgFnSc7d1	úplná
novinkou	novinka	k1gFnSc7	novinka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
teprve	teprve	k6eAd1	teprve
za	za	k7c4	za
Diocletiana	Diocletian	k1gMnSc4	Diocletian
byla	být	k5eAaImAgFnS	být
důsledně	důsledně	k6eAd1	důsledně
realizována	realizován	k2eAgFnSc1d1	realizována
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jí	on	k3xPp3gFnSc3	on
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
ústup	ústup	k1gInSc4	ústup
od	od	k7c2	od
ideje	idea	k1gFnSc2	idea
říšské	říšský	k2eAgFnSc2d1	říšská
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
zůstával	zůstávat	k5eAaImAgInS	zůstávat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
všichni	všechen	k3xTgMnPc1	všechen
císařové	císař	k1gMnPc1	císař
přemístili	přemístit	k5eAaPmAgMnP	přemístit
své	svůj	k3xOyFgFnPc4	svůj
rezidence	rezidence	k1gFnPc4	rezidence
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohli	moct	k5eAaImAgMnP	moct
lépe	dobře	k6eAd2	dobře
čelit	čelit	k5eAaImF	čelit
nepřátelům	nepřítel	k1gMnPc3	nepřítel
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
snažil	snažit	k5eAaImAgMnS	snažit
utužit	utužit	k5eAaPmF	utužit
autoritu	autorita	k1gFnSc4	autorita
státu	stát	k1gInSc2	stát
a	a	k8xC	a
osoby	osoba	k1gFnPc4	osoba
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
přijal	přijmout	k5eAaPmAgMnS	přijmout
přízvisko	přízvisko	k1gNnSc4	přízvisko
Iovius	Iovius	k1gInSc1	Iovius
podle	podle	k7c2	podle
boha	bůh	k1gMnSc4	bůh
Jova	Jova	k1gMnSc1	Jova
<g/>
.	.	kIx.	.
</s>
<s>
Křesťany	Křesťan	k1gMnPc4	Křesťan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
odmítali	odmítat	k5eAaImAgMnP	odmítat
vzdávat	vzdávat	k5eAaImF	vzdávat
božské	božský	k2eAgFnPc4d1	božská
pocty	pocta	k1gFnPc4	pocta
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
neloajální	loajální	k2eNgNnSc4d1	neloajální
vůči	vůči	k7c3	vůči
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
neváhal	váhat	k5eNaImAgMnS	váhat
rozpoutat	rozpoutat	k5eAaPmF	rozpoutat
jejich	jejich	k3xOp3gNnSc4	jejich
poslední	poslední	k2eAgNnSc4d1	poslední
a	a	k8xC	a
nejtěžší	těžký	k2eAgNnSc4d3	nejtěžší
pronásledování	pronásledování	k1gNnSc4	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
orientálních	orientální	k2eAgMnPc2d1	orientální
despotů	despot	k1gMnPc2	despot
si	se	k3xPyFc3	se
Diocletianus	Diocletianus	k1gInSc4	Diocletianus
vsadil	vsadit	k5eAaPmAgInS	vsadit
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
diadém	diadém	k1gInSc4	diadém
a	a	k8xC	a
obklopil	obklopit	k5eAaPmAgMnS	obklopit
se	se	k3xPyFc4	se
ohromným	ohromný	k2eAgInSc7d1	ohromný
císařským	císařský	k2eAgInSc7d1	císařský
dvorem	dvůr	k1gInSc7	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
principátu	principát	k1gInSc2	principát
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
císaři	císař	k1gMnPc1	císař
snažili	snažit	k5eAaImAgMnP	snažit
alespoň	alespoň	k9	alespoň
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vládnout	vládnout	k5eAaImF	vládnout
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
<g/>
,	,	kIx,	,
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
nebral	brát	k5eNaImAgMnS	brát
na	na	k7c4	na
senát	senát	k1gInSc1	senát
žádné	žádný	k3yNgInPc4	žádný
ohledy	ohled	k1gInPc4	ohled
a	a	k8xC	a
počínal	počínat	k5eAaImAgMnS	počínat
si	se	k3xPyFc3	se
jako	jako	k8xC	jako
neomezený	omezený	k2eNgMnSc1d1	neomezený
panovník	panovník	k1gMnSc1	panovník
(	(	kIx(	(
<g/>
dominus	dominus	k1gMnSc1	dominus
et	et	k?	et
deus	deus	k1gInSc1	deus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
systému	systém	k1gInSc6	systém
vlády	vláda	k1gFnSc2	vláda
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
občané	občan	k1gMnPc1	občan
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
jako	jako	k8xC	jako
poddaní	poddaný	k1gMnPc1	poddaný
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
napomohl	napomoct	k5eAaPmAgMnS	napomoct
také	také	k9	také
sociální	sociální	k2eAgInSc1d1	sociální
fenomén	fenomén	k1gInSc1	fenomén
rozvíjející	rozvíjející	k2eAgInSc1d1	rozvíjející
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
kolonát	kolonát	k1gInSc1	kolonát
<g/>
.	.	kIx.	.
</s>
<s>
Postupný	postupný	k2eAgInSc1d1	postupný
útlum	útlum	k1gInSc1	útlum
přílivu	příliv	k1gInSc2	příliv
otroků	otrok	k1gMnPc2	otrok
nutil	nutit	k5eAaImAgMnS	nutit
velkostatkáře	velkostatkář	k1gMnSc4	velkostatkář
k	k	k7c3	k
pronajímání	pronajímání	k1gNnSc3	pronajímání
svých	svůj	k3xOyFgInPc2	svůj
pozemků	pozemek	k1gInPc2	pozemek
pachtýřům	pachtýř	k1gMnPc3	pachtýř
<g/>
,	,	kIx,	,
kolónům	kolón	k1gMnPc3	kolón
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
pachtovné	pachtovné	k1gNnSc4	pachtovné
museli	muset	k5eAaImAgMnP	muset
kolóni	kolón	k1gMnPc1	kolón
odvádět	odvádět	k5eAaImF	odvádět
statkářům	statkář	k1gMnPc3	statkář
nejprve	nejprve	k6eAd1	nejprve
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
část	část	k1gFnSc1	část
úrody	úroda	k1gFnSc2	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
kolóni	kolóeň	k1gFnSc3	kolóeň
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
byli	být	k5eAaImAgMnP	být
římskými	římský	k2eAgMnPc7d1	římský
občany	občan	k1gMnPc7	občan
<g/>
,	,	kIx,	,
stávali	stávat	k5eAaImAgMnP	stávat
závislými	závislý	k2eAgInPc7d1	závislý
na	na	k7c4	na
velkostatkářích	velkostatkář	k1gMnPc6	velkostatkář
a	a	k8xC	a
ztráceli	ztrácet	k5eAaImAgMnP	ztrácet
svoji	svůj	k3xOyFgFnSc4	svůj
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
prohloubil	prohloubit	k5eAaPmAgMnS	prohloubit
za	za	k7c4	za
Diocletianova	Diocletianův	k2eAgMnSc4d1	Diocletianův
nástupce	nástupce	k1gMnSc4	nástupce
Konstantina	Konstantin	k1gMnSc4	Konstantin
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
kolóni	kolón	k1gMnPc1	kolón
připoutáni	připoutat	k5eAaPmNgMnP	připoutat
k	k	k7c3	k
půdě	půda	k1gFnSc3	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
tetrarchie	tetrarchie	k1gFnSc2	tetrarchie
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Diocletianus	Diocletianus	k1gMnSc1	Diocletianus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
305	[number]	k4	305
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gFnSc2	jeho
autority	autorita	k1gFnSc2	autorita
propukly	propuknout	k5eAaPmAgFnP	propuknout
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
tetrarchy	tetrarch	k1gMnPc7	tetrarch
konflikty	konflikt	k1gInPc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následných	následný	k2eAgInPc2d1	následný
zmatků	zmatek	k1gInPc2	zmatek
a	a	k8xC	a
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
306	[number]	k4	306
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
syn	syn	k1gMnSc1	syn
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
Diocletianových	Diocletianův	k2eAgMnPc2d1	Diocletianův
caesarů	caesar	k1gMnPc2	caesar
Konstantin	Konstantin	k1gMnSc1	Konstantin
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
říše	říše	k1gFnSc1	říše
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
312	[number]	k4	312
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
Konstantinovi	Konstantinův	k2eAgMnPc1d1	Konstantinův
vojáci	voják	k1gMnPc1	voják
s	s	k7c7	s
Kristovým	Kristův	k2eAgInSc7d1	Kristův
monogramem	monogram	k1gInSc7	monogram
na	na	k7c6	na
štítech	štít	k1gInPc6	štít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Milvijského	Milvijský	k2eAgInSc2d1	Milvijský
mostu	most	k1gInSc2	most
nad	nad	k7c7	nad
císařem	císař	k1gMnSc7	císař
Maxentiem	Maxentius	k1gMnSc7	Maxentius
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
vydal	vydat	k5eAaPmAgMnS	vydat
Konstantin	Konstantin	k1gMnSc1	Konstantin
v	v	k7c4	v
Mediolanu	Mediolana	k1gFnSc4	Mediolana
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
edikt	edikt	k1gInSc1	edikt
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
uznal	uznat	k5eAaPmAgMnS	uznat
křesťanství	křesťanství	k1gNnSc4	křesťanství
za	za	k7c4	za
rovnocenné	rovnocenný	k2eAgNnSc4d1	rovnocenné
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Přijetím	přijetí	k1gNnSc7	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
formálně	formálně	k6eAd1	formálně
stalo	stát	k5eAaPmAgNnS	stát
pouze	pouze	k6eAd1	pouze
tolerovaným	tolerovaný	k2eAgNnSc7d1	tolerované
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
preferovaným	preferovaný	k2eAgNnSc7d1	preferované
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
stát	stát	k1gInSc1	stát
získat	získat	k5eAaPmF	získat
novou	nový	k2eAgFnSc4d1	nová
jednotící	jednotící	k2eAgFnSc4d1	jednotící
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
složitou	složitý	k2eAgFnSc7d1	složitá
hierarchií	hierarchie	k1gFnSc7	hierarchie
rychle	rychle	k6eAd1	rychle
prorostla	prorůst	k5eAaPmAgFnS	prorůst
do	do	k7c2	do
římského	římský	k2eAgInSc2d1	římský
správního	správní	k2eAgInSc2d1	správní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
již	již	k6eAd1	již
za	za	k7c2	za
Konstantina	Konstantin	k1gMnSc2	Konstantin
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pilířem	pilíř	k1gInSc7	pilíř
říšské	říšský	k2eAgFnSc3d1	říšská
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
312	[number]	k4	312
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Konstantin	Konstantin	k1gMnSc1	Konstantin
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
západem	západ	k1gInSc7	západ
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
324	[number]	k4	324
se	se	k3xPyFc4	se
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Liciniem	Licinium	k1gNnSc7	Licinium
ustavil	ustavit	k5eAaPmAgMnS	ustavit
jediným	jediný	k2eAgMnSc7d1	jediný
vládcem	vládce	k1gMnSc7	vládce
celého	celý	k2eAgNnSc2d1	celé
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
uznání	uznání	k1gNnSc2	uznání
křesťanství	křesťanství	k1gNnSc2	křesťanství
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
vládnutí	vládnutí	k1gNnSc1	vládnutí
významné	významný	k2eAgNnSc1d1	významné
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
událostí	událost	k1gFnSc7	událost
<g/>
:	:	kIx,	:
založením	založení	k1gNnSc7	založení
Nového	Nového	k2eAgInSc2d1	Nového
Říma	Řím	k1gInSc2	Řím
–	–	k?	–
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
s	s	k7c7	s
definitivní	definitivní	k2eAgFnSc7d1	definitivní
platností	platnost	k1gFnSc7	platnost
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
bohatší	bohatý	k2eAgFnSc2d2	bohatší
a	a	k8xC	a
stabilnější	stabilní	k2eAgFnSc2d2	stabilnější
východní	východní	k2eAgFnSc2d1	východní
poloviny	polovina	k1gFnSc2	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
Diocletianových	Diocletianův	k2eAgFnPc6d1	Diocletianova
správních	správní	k2eAgFnPc6d1	správní
reformách	reforma	k1gFnPc6	reforma
zavedením	zavedení	k1gNnSc7	zavedení
nových	nový	k2eAgInPc2d1	nový
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
prefektur	prefektura	k1gFnPc2	prefektura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
členily	členit	k5eAaImAgFnP	členit
na	na	k7c4	na
třináct	třináct	k4xCc4	třináct
diecézí	diecéze	k1gFnPc2	diecéze
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
výkonnost	výkonnost	k1gFnSc1	výkonnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Diocletianovy	Diocletianův	k2eAgFnPc1d1	Diocletianova
a	a	k8xC	a
Konstantinovy	Konstantinův	k2eAgFnPc1d1	Konstantinova
reformy	reforma	k1gFnPc1	reforma
přispěly	přispět	k5eAaPmAgFnP	přispět
ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
státu	stát	k1gInSc2	stát
nové	nový	k2eAgInPc4d1	nový
finanční	finanční	k2eAgInSc4d1	finanční
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dovolily	dovolit	k5eAaPmAgFnP	dovolit
zvětšit	zvětšit	k5eAaPmF	zvětšit
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
lepší	dobrý	k2eAgFnSc4d2	lepší
obranu	obrana	k1gFnSc4	obrana
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Enormně	enormně	k6eAd1	enormně
vyšroubovaná	vyšroubovaný	k2eAgFnSc1d1	vyšroubovaná
daňová	daňový	k2eAgFnSc1d1	daňová
zátěž	zátěž	k1gFnSc1	zátěž
ovšem	ovšem	k9	ovšem
podlomila	podlomit	k5eAaPmAgFnS	podlomit
hospodářství	hospodářství	k1gNnSc2	hospodářství
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
poddaných	poddaný	k1gMnPc2	poddaný
nutila	nutit	k5eAaImAgFnS	nutit
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
tíživého	tíživý	k2eAgNnSc2d1	tíživé
břemena	břemeno	k1gNnSc2	břemeno
<g/>
.	.	kIx.	.
</s>
<s>
Římskou	římský	k2eAgFnSc4d1	římská
společnost	společnost	k1gFnSc4	společnost
především	především	k9	především
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
provinciích	provincie	k1gFnPc6	provincie
říše	říše	k1gFnSc1	říše
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vlna	vlna	k1gFnSc1	vlna
deurbanizace	deurbanizace	k1gFnSc1	deurbanizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Konstantina	Konstantin	k1gMnSc2	Konstantin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
337	[number]	k4	337
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
třemi	tři	k4xCgMnPc7	tři
syny	syn	k1gMnPc4	syn
rozhořel	rozhořet	k5eAaPmAgInS	rozhořet
boj	boj	k1gInSc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
353	[number]	k4	353
se	se	k3xPyFc4	se
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
Constantius	Constantius	k1gInSc1	Constantius
II	II	kA	II
<g/>
.	.	kIx.	.
stal	stát	k5eAaPmAgMnS	stát
opět	opět	k6eAd1	opět
jediným	jediný	k2eAgMnSc7d1	jediný
vládcem	vládce	k1gMnSc7	vládce
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
361	[number]	k4	361
jeho	jeho	k3xOp3gFnSc4	jeho
bratranec	bratranec	k1gMnSc1	bratranec
Julianus	Julianus	k1gMnSc1	Julianus
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Apostata	apostata	k1gMnSc1	apostata
(	(	kIx(	(
<g/>
Odpadlík	odpadlík	k1gMnSc1	odpadlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
pohanství	pohanství	k1gNnSc3	pohanství
<g/>
.	.	kIx.	.
</s>
<s>
Nezapočal	započnout	k5eNaPmAgMnS	započnout
však	však	k9	však
žádné	žádný	k3yNgNnSc4	žádný
nové	nový	k2eAgNnSc4d1	nové
pronásledování	pronásledování	k1gNnSc4	pronásledování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
podporu	podpora	k1gFnSc4	podpora
původních	původní	k2eAgInPc2d1	původní
kultů	kult	k1gInPc2	kult
a	a	k8xC	a
také	také	k9	také
skrytě	skrytě	k6eAd1	skrytě
podporoval	podporovat	k5eAaImAgMnS	podporovat
rozpory	rozpor	k1gInPc4	rozpor
uvnitř	uvnitř	k7c2	uvnitř
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
řadu	řada	k1gFnSc4	řada
spisů	spis	k1gInPc2	spis
a	a	k8xC	a
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejznámější	známý	k2eAgMnPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
Contra	Contra	k1gFnSc1	Contra
Galileos	Galileosa	k1gFnPc2	Galileosa
<g/>
.	.	kIx.	.
</s>
<s>
Obnova	obnova	k1gFnSc1	obnova
starých	starý	k2eAgInPc2d1	starý
kultů	kult	k1gInPc2	kult
však	však	k9	však
neměla	mít	k5eNaImAgFnS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
Julianovou	Julianův	k2eAgFnSc7d1	Julianova
předčasnou	předčasný	k2eAgFnSc7d1	předčasná
smrtí	smrt	k1gFnSc7	smrt
během	během	k7c2	během
tažení	tažení	k1gNnSc2	tažení
v	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
363	[number]	k4	363
konstantinovská	konstantinovský	k2eAgFnSc1d1	konstantinovská
dynastie	dynastie	k1gFnSc1	dynastie
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Juliánův	Juliánův	k2eAgMnSc1d1	Juliánův
nástupce	nástupce	k1gMnSc1	nástupce
Joviauns	Joviaunsa	k1gFnPc2	Joviaunsa
pak	pak	k6eAd1	pak
většinu	většina	k1gFnSc4	většina
Juliánových	Juliánův	k2eAgNnPc2d1	Juliánův
nařízení	nařízení	k1gNnPc2	nařízení
podporující	podporující	k2eAgNnSc1d1	podporující
pohanství	pohanství	k1gNnSc1	pohanství
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
impériu	impérium	k1gNnSc6	impérium
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Valentiniana	Valentinian	k1gMnSc2	Valentinian
I.	I.	kA	I.
byla	být	k5eAaImAgFnS	být
říše	říše	k1gFnSc1	říše
ze	z	k7c2	z
správních	správní	k2eAgInPc2d1	správní
důvodů	důvod	k1gInPc2	důvod
dočasně	dočasně	k6eAd1	dočasně
(	(	kIx(	(
<g/>
364	[number]	k4	364
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Theodosia	Theodosium	k1gNnSc2	Theodosium
I.	I.	kA	I.
trvale	trvale	k6eAd1	trvale
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
(	(	kIx(	(
<g/>
395	[number]	k4	395
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Valentinianus	Valentinianus	k1gMnSc1	Valentinianus
se	se	k3xPyFc4	se
podělil	podělit	k5eAaPmAgMnS	podělit
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
se	s	k7c7	s
svým	svůj	k1gMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Valentem	Valentem	k?	Valentem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vpádu	vpád	k1gInSc6	vpád
Hunů	Hun	k1gMnPc2	Hun
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
375	[number]	k4	375
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
započalo	započnout	k5eAaPmAgNnS	započnout
stěhování	stěhování	k1gNnSc4	stěhování
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Gótům	Gót	k1gMnPc3	Gót
povoleno	povolen	k2eAgNnSc1d1	povoleno
překročit	překročit	k5eAaPmF	překročit
Dunaj	Dunaj	k1gInSc4	Dunaj
a	a	k8xC	a
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
nesnesitelným	snesitelný	k2eNgFnPc3d1	nesnesitelná
životním	životní	k2eAgFnPc3d1	životní
podmínkám	podmínka	k1gFnPc3	podmínka
brzy	brzy	k6eAd1	brzy
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
<g/>
.	.	kIx.	.
</s>
<s>
Valens	Valens	k6eAd1	Valens
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zpacifikovat	zpacifikovat	k5eAaPmF	zpacifikovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
378	[number]	k4	378
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
a	a	k8xC	a
zabit	zabít	k5eAaPmNgMnS	zabít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Adrianopole	Adrianopole	k1gFnSc2	Adrianopole
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kann	Kanny	k1gFnPc2	Kanny
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
porážku	porážka	k1gFnSc4	porážka
Římanů	Říman	k1gMnPc2	Říman
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
nastolení	nastolení	k1gNnSc2	nastolení
křesťanství	křesťanství	k1gNnSc2	křesťanství
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Nový	nový	k2eAgMnSc1d1	nový
císař	císař	k1gMnSc1	císař
Theodosius	Theodosius	k1gMnSc1	Theodosius
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
uzavřít	uzavřít	k5eAaPmF	uzavřít
s	s	k7c7	s
Góty	Gót	k1gMnPc7	Gót
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
pobývat	pobývat	k5eAaImF	pobývat
na	na	k7c6	na
římském	římský	k2eAgNnSc6d1	římské
území	území	k1gNnSc6	území
jako	jako	k9	jako
foederati	foederat	k1gMnPc1	foederat
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
barbarů	barbar	k1gMnPc2	barbar
na	na	k7c6	na
římském	římský	k2eAgNnSc6d1	římské
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
stala	stát	k5eAaPmAgFnS	stát
trvalým	trvalý	k2eAgInSc7d1	trvalý
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
394	[number]	k4	394
se	se	k3xPyFc4	se
Theodosius	Theodosius	k1gMnSc1	Theodosius
po	po	k7c6	po
zdolání	zdolání	k1gNnSc6	zdolání
poslední	poslední	k2eAgFnSc2d1	poslední
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
uzurpací	uzurpace	k1gFnPc2	uzurpace
a	a	k8xC	a
vzpour	vzpoura	k1gFnPc2	vzpoura
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
posledním	poslední	k2eAgMnSc7d1	poslední
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vládl	vládnout	k5eAaImAgMnS	vládnout
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
územím	území	k1gNnSc7	území
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
panování	panování	k1gNnSc2	panování
bylo	být	k5eAaImAgNnS	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
oficiálně	oficiálně	k6eAd1	oficiálně
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
za	za	k7c4	za
jediné	jediný	k2eAgNnSc4d1	jediné
státní	státní	k2eAgNnSc4d1	státní
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Theodosiově	Theodosiův	k2eAgFnSc6d1	Theodosiova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
395	[number]	k4	395
byla	být	k5eAaImAgFnS	být
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
dva	dva	k4xCgMnPc4	dva
neschopné	schopný	k2eNgMnPc4d1	neschopný
syny	syn	k1gMnPc4	syn
<g/>
:	:	kIx,	:
Honorius	Honorius	k1gMnSc1	Honorius
vládl	vládnout	k5eAaImAgMnS	vládnout
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Arcadiovi	Arcadius	k1gMnSc3	Arcadius
byl	být	k5eAaImAgInS	být
svěřen	svěřen	k2eAgInSc1d1	svěřen
Východ	východ	k1gInSc1	východ
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
se	se	k3xPyFc4	se
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
definitivní	definitivní	k2eAgNnSc1d1	definitivní
<g/>
,	,	kIx,	,
myšlenka	myšlenka	k1gFnSc1	myšlenka
říšské	říšský	k2eAgFnSc2d1	říšská
jednoty	jednota	k1gFnSc2	jednota
přesto	přesto	k8xC	přesto
trvala	trvat	k5eAaImAgFnS	trvat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
–	–	k?	–
zákony	zákon	k1gInPc1	zákon
vydané	vydaný	k2eAgInPc1d1	vydaný
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
císařů	císař	k1gMnPc2	císař
byly	být	k5eAaImAgFnP	být
proto	proto	k6eAd1	proto
běžně	běžně	k6eAd1	běžně
uznávány	uznávat	k5eAaImNgInP	uznávat
i	i	k9	i
druhým	druhý	k4xOgMnSc7	druhý
vládcem	vládce	k1gMnSc7	vládce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zánik	zánik	k1gInSc4	zánik
antického	antický	k2eAgNnSc2d1	antické
impéria	impérium	k1gNnSc2	impérium
===	===	k?	===
</s>
</p>
<p>
<s>
Východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
konsolidovanější	konsolidovaný	k2eAgFnSc1d2	konsolidovanější
a	a	k8xC	a
hustěji	husto	k6eAd2	husto
obydlenou	obydlený	k2eAgFnSc7d1	obydlená
polovinou	polovina	k1gFnSc7	polovina
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bez	bez	k7c2	bez
vážnější	vážní	k2eAgFnSc2d2	vážnější
újmy	újma	k1gFnSc2	újma
přežila	přežít	k5eAaPmAgFnS	přežít
chaos	chaos	k1gInSc4	chaos
období	období	k1gNnSc2	období
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
západořímská	západořímský	k2eAgFnSc1d1	Západořímská
říše	říše	k1gFnSc1	říše
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pozvolna	pozvolna	k6eAd1	pozvolna
upadala	upadat	k5eAaPmAgFnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
Hunů	Hun	k1gMnPc2	Hun
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
spustil	spustit	k5eAaPmAgInS	spustit
dominový	dominový	k2eAgInSc1d1	dominový
efekt	efekt	k1gInSc1	efekt
pohybu	pohyb	k1gInSc2	pohyb
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zásadně	zásadně	k6eAd1	zásadně
změnil	změnit	k5eAaPmAgInS	změnit
podobu	podoba	k1gFnSc4	podoba
celého	celý	k2eAgInSc2d1	celý
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
desetiletích	desetiletí	k1gNnPc6	desetiletí
následujících	následující	k2eAgNnPc6d1	následující
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
u	u	k7c2	u
Adrianopole	Adrianopole	k1gFnSc2	Adrianopole
ztratila	ztratit	k5eAaPmAgFnS	ztratit
říše	říše	k1gFnSc1	říše
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
svých	svůj	k3xOyFgFnPc2	svůj
západních	západní	k2eAgFnPc2d1	západní
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
zabránili	zabránit	k5eAaPmAgMnP	zabránit
vpádům	vpád	k1gInPc3	vpád
Germánů	Germán	k1gMnPc2	Germán
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
Římané	Říman	k1gMnPc1	Říman
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
století	století	k1gNnSc2	století
legie	legie	k1gFnSc2	legie
z	z	k7c2	z
rýnské	rýnský	k2eAgFnSc2d1	Rýnská
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Nechráněný	chráněný	k2eNgInSc1d1	nechráněný
Rýn	Rýn	k1gInSc1	Rýn
poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
406	[number]	k4	406
překročily	překročit	k5eAaPmAgInP	překročit
barbarské	barbarský	k2eAgInPc1d1	barbarský
kmeny	kmen	k1gInPc1	kmen
Svébů	Svéb	k1gMnPc2	Svéb
<g/>
,	,	kIx,	,
Vandalů	Vandal	k1gMnPc2	Vandal
a	a	k8xC	a
Alanů	Alan	k1gMnPc2	Alan
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
let	let	k1gInSc4	let
vyvrátily	vyvrátit	k5eAaPmAgFnP	vyvrátit
římskou	římský	k2eAgFnSc4d1	římská
správu	správa	k1gFnSc4	správa
v	v	k7c6	v
Galii	Galie	k1gFnSc6	Galie
a	a	k8xC	a
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
Británie	Británie	k1gFnSc1	Británie
byla	být	k5eAaImAgFnS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
svému	svůj	k3xOyFgInSc3	svůj
osudu	osud	k1gInSc3	osud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
410	[number]	k4	410
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
Vizigóti	Vizigót	k1gMnPc1	Vizigót
vedení	vedení	k1gNnSc2	vedení
náčelníkem	náčelník	k1gMnSc7	náčelník
Alarichem	Alarich	k1gMnSc7	Alarich
Řím	Řím	k1gInSc4	Řím
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
římskou	římský	k2eAgFnSc4d1	římská
psychiku	psychika	k1gFnSc4	psychika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byli	být	k5eAaImAgMnP	být
sice	sice	k8xC	sice
Vizigóti	Vizigót	k1gMnPc1	Vizigót
vytlačeni	vytlačit	k5eAaPmNgMnP	vytlačit
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
Galie	Galie	k1gFnSc2	Galie
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
dále	daleko	k6eAd2	daleko
představovali	představovat	k5eAaImAgMnP	představovat
závažné	závažný	k2eAgNnSc4d1	závažné
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
Vandalové	Vandal	k1gMnPc1	Vandal
pronikli	proniknout	k5eAaPmAgMnP	proniknout
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
pro	pro	k7c4	pro
říši	říše	k1gFnSc4	říše
tolik	tolik	k6eAd1	tolik
důležitou	důležitý	k2eAgFnSc4d1	důležitá
provincii	provincie	k1gFnSc4	provincie
opanovali	opanovat	k5eAaPmAgMnP	opanovat
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Kartága	Kartágo	k1gNnSc2	Kartágo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
438	[number]	k4	438
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
455	[number]	k4	455
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
pak	pak	k6eAd1	pak
podařilo	podařit	k5eAaPmAgNnS	podařit
podruhé	podruhé	k6eAd1	podruhé
vyplenit	vyplenit	k5eAaPmF	vyplenit
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gNnSc2	jejich
jména	jméno	k1gNnSc2	jméno
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k8xS	jako
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
západní	západní	k2eAgFnSc2d1	západní
říše	říš	k1gFnSc2	říš
měl	mít	k5eAaImAgMnS	mít
řadu	řada	k1gFnSc4	řada
příčin	příčina	k1gFnPc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Jaké	jaký	k3yQgInPc1	jaký
procesy	proces	k1gInPc1	proces
vedly	vést	k5eAaImAgInP	vést
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
v	v	k7c4	v
řadu	řada	k1gFnSc4	řada
germánských	germánský	k2eAgNnPc2d1	germánské
království	království	k1gNnPc2	království
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
velice	velice	k6eAd1	velice
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
předmětem	předmět	k1gInSc7	předmět
diskuzí	diskuze	k1gFnPc2	diskuze
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
pokládána	pokládán	k2eAgFnSc1d1	pokládána
barbarizace	barbarizace	k1gFnSc1	barbarizace
římského	římský	k2eAgNnSc2d1	římské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
převážně	převážně	k6eAd1	převážně
germánskými	germánský	k2eAgMnPc7d1	germánský
žoldnéři	žoldnéř	k1gMnPc7	žoldnéř
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
velikost	velikost	k1gFnSc1	velikost
armády	armáda	k1gFnSc2	armáda
nebyla	být	k5eNaImAgFnS	být
dostačující	dostačující	k2eAgFnSc1d1	dostačující
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázala	dokázat	k5eAaPmAgFnS	dokázat
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
hranice	hranice	k1gFnSc1	hranice
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
rozkladu	rozklad	k1gInSc6	rozklad
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
ekonomika	ekonomika	k1gFnSc1	ekonomika
impéria	impérium	k1gNnSc2	impérium
byla	být	k5eAaImAgFnS	být
rozvrácena	rozvrátit	k5eAaPmNgFnS	rozvrátit
příliš	příliš	k6eAd1	příliš
vysokými	vysoký	k2eAgFnPc7d1	vysoká
daněmi	daň	k1gFnPc7	daň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zbavovaly	zbavovat	k5eAaImAgFnP	zbavovat
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
loajality	loajalita	k1gFnSc2	loajalita
k	k	k7c3	k
římskému	římský	k2eAgInSc3d1	římský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třebaže	třebaže	k8xS	třebaže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
451	[number]	k4	451
dokázali	dokázat	k5eAaPmAgMnP	dokázat
Římané	Říman	k1gMnPc1	Říman
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
Vizigóty	Vizigót	k1gMnPc7	Vizigót
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Aetia	Aetius	k1gMnSc2	Aetius
přemoci	přemoct	k5eAaPmF	přemoct
Huny	Hun	k1gMnPc4	Hun
vedené	vedený	k2eAgFnSc2d1	vedená
Attilou	Attila	k1gMnSc7	Attila
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Katalaunských	Katalaunský	k2eAgNnPc6d1	Katalaunský
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
na	na	k7c6	na
západě	západ	k1gInSc6	západ
spěla	spět	k5eAaImAgFnS	spět
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
rychlému	rychlý	k2eAgInSc3d1	rychlý
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Slabí	slabý	k2eAgMnPc1d1	slabý
císaři	císař	k1gMnPc1	císař
nebyli	být	k5eNaImAgMnP	být
schopní	schopný	k2eAgMnPc1d1	schopný
zabránit	zabránit	k5eAaPmF	zabránit
nevyhnutelnému	vyhnutelný	k2eNgInSc3d1	nevyhnutelný
rozvratu	rozvrat	k1gInSc3	rozvrat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
pouhými	pouhý	k2eAgFnPc7d1	pouhá
loutkami	loutka	k1gFnPc7	loutka
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
velitelů	velitel	k1gMnPc2	velitel
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
476	[number]	k4	476
Germán	Germán	k1gMnSc1	Germán
Odoaker	Odoaker	k1gMnSc1	Odoaker
sesadil	sesadit	k5eAaPmAgMnS	sesadit
císaře	císař	k1gMnSc4	císař
Romula	Romulus	k1gMnSc4	Romulus
Augusta	August	k1gMnSc4	August
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
okamžik	okamžik	k1gInSc4	okamžik
zániku	zánik	k1gInSc2	zánik
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
posledním	poslední	k2eAgMnSc7d1	poslední
uznaným	uznaný	k2eAgMnSc7d1	uznaný
císařem	císař	k1gMnSc7	císař
Západu	západ	k1gInSc2	západ
byl	být	k5eAaImAgMnS	být
Julius	Julius	k1gMnSc1	Julius
Nepos	Nepos	k1gMnSc1	Nepos
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
žil	žít	k5eAaImAgMnS	žít
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
480	[number]	k4	480
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
sám	sám	k3xTgMnSc1	sám
Odoaker	Odoaker	k1gMnSc1	Odoaker
se	se	k3xPyFc4	se
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c7	za
"	"	kIx"	"
<g/>
Germána	Germán	k1gMnSc2	Germán
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
službách	služba	k1gFnPc6	služba
<g/>
"	"	kIx"	"
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
interpretovat	interpretovat	k5eAaBmF	interpretovat
jako	jako	k9	jako
správu	správa	k1gFnSc4	správa
Itálie	Itálie	k1gFnSc2	Itálie
jménem	jméno	k1gNnSc7	jméno
císaře	císař	k1gMnSc2	císař
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Čistě	čistě	k6eAd1	čistě
formálně	formálně	k6eAd1	formálně
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
Itálie	Itálie	k1gFnSc1	Itálie
stále	stále	k6eAd1	stále
součástí	součást	k1gFnSc7	součást
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Odoakerův	Odoakerův	k2eAgMnSc1d1	Odoakerův
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
ostrogótský	ostrogótský	k2eAgMnSc1d1	ostrogótský
král	král	k1gMnSc1	král
Theodorich	Theodorich	k1gMnSc1	Theodorich
Veliký	veliký	k2eAgMnSc1d1	veliký
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
uznání	uznání	k1gNnSc4	uznání
východního	východní	k2eAgMnSc2d1	východní
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Západu	západ	k1gInSc2	západ
východní	východní	k2eAgFnSc2d1	východní
říše	říš	k1gFnSc2	říš
nebyla	být	k5eNaImAgNnP	být
germánskými	germánský	k2eAgFnPc7d1	germánská
nájezdy	nájezd	k1gInPc7	nájezd
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
tak	tak	k6eAd1	tak
ničivě	ničivě	k6eAd1	ničivě
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
disponovala	disponovat	k5eAaBmAgFnS	disponovat
mnohem	mnohem	k6eAd1	mnohem
většími	veliký	k2eAgInPc7d2	veliký
finančními	finanční	k2eAgInPc7d1	finanční
zdroji	zdroj	k1gInPc7	zdroj
a	a	k8xC	a
především	především	k6eAd1	především
obratnou	obratný	k2eAgFnSc7d1	obratná
diplomacií	diplomacie	k1gFnSc7	diplomacie
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
také	také	k9	také
geograficky	geograficky	k6eAd1	geograficky
lépe	dobře	k6eAd2	dobře
hájitelné	hájitelný	k2eAgFnPc4d1	hájitelná
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
mocné	mocný	k2eAgNnSc4d1	mocné
anatolské	anatolský	k2eAgNnSc4d1	anatolský
pohoří	pohoří	k1gNnSc4	pohoří
Taurus	Taurus	k1gInSc1	Taurus
a	a	k8xC	a
Propontida	Propontida	k1gFnSc1	Propontida
tvořily	tvořit	k5eAaImAgFnP	tvořit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
překážku	překážka	k1gFnSc4	překážka
nepřátelským	přátelský	k2eNgFnPc3d1	nepřátelská
invazím	invaze	k1gFnPc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Hunům	Hun	k1gMnPc3	Hun
ani	ani	k8xC	ani
Germánům	Germán	k1gMnPc3	Germán
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepodařilo	podařit	k5eNaPmAgNnS	podařit
překročit	překročit	k5eAaPmF	překročit
Helléspont	Helléspont	k1gInSc4	Helléspont
a	a	k8xC	a
bohaté	bohatý	k2eAgFnSc2d1	bohatá
provincie	provincie	k1gFnSc2	provincie
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
proto	proto	k8xC	proto
zůstaly	zůstat	k5eAaPmAgInP	zůstat
ušetřeny	ušetřen	k2eAgInPc1d1	ušetřen
jejich	jejich	k3xOp3gInPc2	jejich
vpádů	vpád	k1gInPc2	vpád
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
germánského	germánský	k2eAgInSc2d1	germánský
elementu	element	k1gInSc2	element
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
osudově	osudově	k6eAd1	osudově
přispěl	přispět	k5eAaPmAgMnS	přispět
ke	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
Západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
eliminován	eliminovat	k5eAaBmNgMnS	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
Příznačné	příznačný	k2eAgNnSc1d1	příznačné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaImF	stát
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
jiných	jiný	k2eAgMnPc2d1	jiný
barbarů	barbar	k1gMnPc2	barbar
–	–	k?	–
Isaurů	Isaur	k1gInPc2	Isaur
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
těžké	těžký	k2eAgNnSc4d1	těžké
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
vítězné	vítězný	k2eAgInPc4d1	vítězný
boje	boj	k1gInPc4	boj
s	s	k7c7	s
Huny	Hun	k1gMnPc7	Hun
a	a	k8xC	a
Góty	Gót	k1gMnPc7	Gót
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
nepostihl	postihnout	k5eNaPmAgInS	postihnout
východní	východní	k2eAgFnSc3d1	východní
říši	říš	k1gFnSc3	říš
rozklad	rozklad	k1gInSc1	rozklad
tolik	tolik	k6eAd1	tolik
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pro	pro	k7c4	pro
západní	západní	k2eAgFnSc4d1	západní
provincie	provincie	k1gFnPc4	provincie
v	v	k7c4	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgMnSc2d1	poslední
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
byla	být	k5eAaImAgFnS	být
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgMnS	dobýt
východořímský	východořímský	k2eAgMnSc1d1	východořímský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Belisar	Belisar	k1gMnSc1	Belisar
zpět	zpět	k6eAd1	zpět
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
Západu	západ	k1gInSc2	západ
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgNnSc1d1	jižní
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
říše	říše	k1gFnSc1	říše
jen	jen	k9	jen
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
vypětím	vypětí	k1gNnSc7	vypětí
vypořádávala	vypořádávat	k5eAaImAgFnS	vypořádávat
s	s	k7c7	s
agresí	agrese	k1gFnSc7	agrese
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
východních	východní	k2eAgFnPc6d1	východní
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
útoky	útok	k1gInPc1	útok
nabývaly	nabývat	k5eAaImAgInP	nabývat
od	od	k7c2	od
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Peršané	Peršan	k1gMnPc1	Peršan
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Západu	západ	k1gInSc2	západ
cítili	cítit	k5eAaImAgMnP	cítit
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
říše	říš	k1gFnSc2	říš
Achaimenovců	Achaimenovec	k1gMnPc2	Achaimenovec
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jejím	její	k3xOp3gInSc6	její
někdejším	někdejší	k2eAgInSc6d1	někdejší
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
mírové	mírový	k2eAgFnSc2d1	mírová
koexistence	koexistence	k1gFnSc2	koexistence
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
tím	ten	k3xDgNnSc7	ten
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
nastala	nastat	k5eAaPmAgFnS	nastat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
namáhavá	namáhavý	k2eAgFnSc1d1	namáhavá
etapa	etapa	k1gFnSc1	etapa
římsko-perských	římskoerský	k2eAgFnPc2d1	římsko-perský
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc4d1	celé
jedno	jeden	k4xCgNnSc4	jeden
století	století	k1gNnSc4	století
<g/>
.	.	kIx.	.
</s>
<s>
Justinián	Justinián	k1gMnSc1	Justinián
se	se	k3xPyFc4	se
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
stal	stát	k5eAaPmAgMnS	stát
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
vládcem	vládce	k1gMnSc7	vládce
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
kdysi	kdysi	k6eAd1	kdysi
římského	římský	k2eAgNnSc2d1	římské
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Galie	Galie	k1gFnSc2	Galie
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
Justiniánově	Justiniánův	k2eAgFnSc6d1	Justiniánova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
565	[number]	k4	565
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
západní	západní	k2eAgNnPc1d1	západní
území	území	k1gNnPc1	území
ukázala	ukázat	k5eAaPmAgNnP	ukázat
jako	jako	k9	jako
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
neudržitelná	udržitelný	k2eNgFnSc1d1	neudržitelná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
568	[number]	k4	568
si	se	k3xPyFc3	se
Langobardi	Langobard	k1gMnPc1	Langobard
podmanili	podmanit	k5eAaPmAgMnP	podmanit
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
jižní	jižní	k2eAgFnSc2d1	jižní
Hispánie	Hispánie	k1gFnSc2	Hispánie
se	se	k3xPyFc4	se
po	po	k7c6	po
několika	několik	k4yIc6	několik
desetiletích	desetiletí	k1gNnPc6	desetiletí
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
Vizigótů	Vizigót	k1gMnPc2	Vizigót
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zmítána	zmítat	k5eAaImNgFnS	zmítat
náboženskými	náboženský	k2eAgInPc7d1	náboženský
spory	spor	k1gInPc7	spor
mezi	mezi	k7c7	mezi
ortodoxními	ortodoxní	k2eAgMnPc7d1	ortodoxní
křesťany	křesťan	k1gMnPc7	křesťan
a	a	k8xC	a
monofyzity	monofyzita	k1gMnPc7	monofyzita
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
společně	společně	k6eAd1	společně
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
daňovou	daňový	k2eAgFnSc7d1	daňová
zátěží	zátěž	k1gFnSc7	zátěž
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
ustavičnými	ustavičný	k2eAgFnPc7d1	ustavičná
válkami	válka	k1gFnPc7	válka
vzbuzovalo	vzbuzovat	k5eAaImAgNnS	vzbuzovat
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
jeho	jeho	k3xOp3gFnSc2	jeho
loajality	loajalita	k1gFnSc2	loajalita
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
Sásánovci	Sásánovec	k1gMnPc1	Sásánovec
dočasně	dočasně	k6eAd1	dočasně
podrobili	podrobit	k5eAaPmAgMnP	podrobit
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Perské	perský	k2eAgFnSc2d1	perská
armády	armáda	k1gFnSc2	armáda
velkokrále	velkokrála	k1gFnSc3	velkokrála
Husrava	Husrava	k1gFnSc1	Husrava
II	II	kA	II
<g/>
.	.	kIx.	.
dokonce	dokonce	k9	dokonce
dvakrát	dvakrát	k6eAd1	dvakrát
postoupily	postoupit	k5eAaPmAgFnP	postoupit
až	až	k9	až
k	k	k7c3	k
samotné	samotný	k2eAgFnSc3d1	samotná
Konstantinopoli	Konstantinopol	k1gInSc3	Konstantinopol
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
626	[number]	k4	626
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Avary	Avar	k1gMnPc7	Avar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Peršané	Peršan	k1gMnPc1	Peršan
navíc	navíc	k6eAd1	navíc
z	z	k7c2	z
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
odcizili	odcizit	k5eAaPmAgMnP	odcizit
svatý	svatý	k1gMnSc1	svatý
kříž	kříž	k1gInSc4	kříž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
údajně	údajně	k6eAd1	údajně
nalezla	naleznout	k5eAaPmAgFnS	naleznout
matka	matka	k1gFnSc1	matka
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
představoval	představovat	k5eAaImAgMnS	představovat
nejposvátnější	posvátný	k2eAgFnSc4d3	nejposvátnější
relikvii	relikvie	k1gFnSc4	relikvie
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Herakleios	Herakleios	k1gMnSc1	Herakleios
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
a	a	k8xC	a
náročné	náročný	k2eAgFnSc6d1	náročná
válce	válka	k1gFnSc6	válka
nakonec	nakonec	k6eAd1	nakonec
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
říše	říše	k1gFnSc1	říše
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
čelit	čelit	k5eAaImF	čelit
následné	následný	k2eAgFnSc3d1	následná
arabské	arabský	k2eAgFnSc3d1	arabská
expanzi	expanze	k1gFnSc3	expanze
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
definitivně	definitivně	k6eAd1	definitivně
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Sýrií	Sýrie	k1gFnSc7	Sýrie
a	a	k8xC	a
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
ztráta	ztráta	k1gFnSc1	ztráta
bohatého	bohatý	k2eAgInSc2d1	bohatý
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgInS	vzdát
Arabům	Arab	k1gMnPc3	Arab
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
zradou	zrada	k1gFnSc7	zrada
monofyzitského	monofyzitský	k2eAgMnSc2d1	monofyzitský
alexandrijského	alexandrijský	k2eAgMnSc2d1	alexandrijský
patriarchy	patriarcha	k1gMnSc2	patriarcha
Kýra	Kýrus	k1gMnSc2	Kýrus
<g/>
,	,	kIx,	,
východořímskou	východořímský	k2eAgFnSc4d1	Východořímská
říši	říše	k1gFnSc4	říše
rozhodným	rozhodný	k2eAgInSc7d1	rozhodný
způsobem	způsob	k1gInSc7	způsob
oslabila	oslabit	k5eAaPmAgFnS	oslabit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Herakleios	Herakleios	k1gInSc1	Herakleios
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
římskými	římský	k2eAgFnPc7d1	římská
tradicemi	tradice	k1gFnPc7	tradice
přijal	přijmout	k5eAaPmAgInS	přijmout
namísto	namísto	k7c2	namísto
latinského	latinský	k2eAgInSc2d1	latinský
titulu	titul	k1gInSc2	titul
augustus	augustus	k1gInSc1	augustus
starý	starý	k2eAgInSc1d1	starý
řecký	řecký	k2eAgInSc4d1	řecký
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
basileus	basileus	k1gMnSc1	basileus
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
povýšil	povýšit	k5eAaPmAgMnS	povýšit
řečtinu	řečtina	k1gFnSc4	řečtina
na	na	k7c4	na
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postupně	postupně	k6eAd1	postupně
ztratila	ztratit	k5eAaPmAgFnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
římský	římský	k2eAgInSc4d1	římský
a	a	k8xC	a
antický	antický	k2eAgInSc4d1	antický
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Hluboké	hluboký	k2eAgFnPc1d1	hluboká
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
a	a	k8xC	a
vnější	vnější	k2eAgFnPc1d1	vnější
změny	změna	k1gFnPc1	změna
vedly	vést	k5eAaImAgFnP	vést
tudíž	tudíž	k8xC	tudíž
také	také	k6eAd1	také
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
k	k	k7c3	k
nástupu	nástup	k1gInSc3	nástup
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Konstantinopolí	Konstantinopole	k1gFnPc2	Konstantinopole
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
až	až	k6eAd1	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
nadále	nadále	k6eAd1	nadále
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
Rhomaioi	Rhomaioe	k1gFnSc4	Rhomaioe
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Římany	Říman	k1gMnPc4	Říman
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
strukturách	struktura	k1gFnPc6	struktura
říše	říš	k1gFnSc2	říš
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
640	[number]	k4	640
k	k	k7c3	k
zásadní	zásadní	k2eAgFnSc3d1	zásadní
transformaci	transformace	k1gFnSc3	transformace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
celkem	celkem	k6eAd1	celkem
oprávněné	oprávněný	k2eAgNnSc1d1	oprávněné
hovořit	hovořit	k5eAaImF	hovořit
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
o	o	k7c6	o
východořímské	východořímský	k2eAgFnSc6d1	Východořímská
říši	říš	k1gFnSc6	říš
jako	jako	k8xC	jako
o	o	k7c6	o
Byzanci	Byzanc	k1gFnSc6	Byzanc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
Říma	Řím	k1gInSc2	Řím
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GIARDINA	GIARDINA	kA	GIARDINA
<g/>
,	,	kIx,	,
Andrea	Andrea	k1gFnSc1	Andrea
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
312	[number]	k4	312
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
72	[number]	k4	72
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
</s>
</p>
<p>
<s>
Chronologie	chronologie	k1gFnSc1	chronologie
dějin	dějiny	k1gFnPc2	dějiny
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
římských	římský	k2eAgMnPc2d1	římský
konzulů	konzul	k1gMnPc2	konzul
</s>
</p>
<p>
<s>
Starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
</s>
</p>
<p>
<s>
Západořímská	západořímský	k2eAgFnSc1d1	Západořímská
říše	říše	k1gFnSc1	říše
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Římě	Řím	k1gInSc6	Řím
na	na	k7c6	na
sesterském	sesterský	k2eAgInSc6d1	sesterský
projektu	projekt	k1gInSc6	projekt
Wikiknihy	Wikiknih	k1gInPc4	Wikiknih
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Starověký	starověký	k2eAgInSc4d1	starověký
Řím	Řím	k1gInSc4	Řím
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Římské	římský	k2eAgNnSc1d1	římské
císařství	císařství	k1gNnSc1	císařství
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
</s>
</p>
<p>
<s>
Územní	územní	k2eAgInSc1d1	územní
vývoj	vývoj	k1gInSc1	vývoj
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
národy	národ	k1gInPc1	národ
a	a	k8xC	a
civilizace	civilizace	k1gFnSc2	civilizace
–	–	k?	–
Starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Roman	Roman	k1gMnSc1	Roman
Empire	empir	k1gInSc5	empir
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Roman	Roman	k1gMnSc1	Roman
Empire	empir	k1gInSc5	empir
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Politische	Politische	k1gFnSc1	Politische
und	und	k?	und
kulturelle	kulturelle	k1gInSc1	kulturelle
Entwicklung	Entwicklung	k1gMnSc1	Entwicklung
Roms	Roms	k1gInSc1	Roms
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Roman	Roman	k1gMnSc1	Roman
Law	Law	k1gMnSc1	Law
Library	Librara	k1gFnSc2	Librara
</s>
</p>
