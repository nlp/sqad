<s>
Kofola	Kofola	k1gFnSc1	Kofola
je	být	k5eAaImIp3nS	být
sycený	sycený	k2eAgInSc4d1	sycený
nealkoholický	alkoholický	k2eNgInSc4d1	nealkoholický
nápoj	nápoj	k1gInSc4	nápoj
kolového	kolový	k2eAgInSc2d1	kolový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
tehdy	tehdy	k6eAd1	tehdy
nedostupným	dostupný	k2eNgInPc3d1	nedostupný
konkurenčním	konkurenční	k2eAgInPc3d1	konkurenční
nápojům	nápoj	k1gInPc3	nápoj
Coca-Cola	cocaola	k1gFnSc1	coca-cola
nebo	nebo	k8xC	nebo
Pepsi	Pepsi	k1gFnSc1	Pepsi
<g/>
.	.	kIx.	.
</s>
<s>
Nápoj	nápoj	k1gInSc1	nápoj
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
Kofola	Kofola	k1gFnSc1	Kofola
a.s.	a.s.	k?	a.s.
Kofola	Kofola	k1gFnSc1	Kofola
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
státní	státní	k2eAgFnSc2d1	státní
úlohy	úloha	k1gFnSc2	úloha
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
přebytečného	přebytečný	k2eAgInSc2d1	přebytečný
kofeinu	kofein	k1gInSc2	kofein
při	při	k7c6	při
pražení	pražení	k1gNnSc6	pražení
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
PhMr	PhMr	k1gMnSc1	PhMr
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pověřen	pověřen	k2eAgMnSc1d1	pověřen
vyvinout	vyvinout	k5eAaPmF	vyvinout
nápoj	nápoj	k1gInSc4	nápoj
kolového	kolový	k2eAgInSc2d1	kolový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
tohoto	tento	k3xDgInSc2	tento
nápoje	nápoj	k1gInSc2	nápoj
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
tuzemských	tuzemský	k2eAgFnPc6d1	tuzemská
surovinách	surovina	k1gFnPc6	surovina
a	a	k8xC	a
složený	složený	k2eAgMnSc1d1	složený
z	z	k7c2	z
přírodních	přírodní	k2eAgFnPc2d1	přírodní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Namíchal	namíchat	k5eAaPmAgMnS	namíchat
originální	originální	k2eAgInSc4d1	originální
vícesložkový	vícesložkový	k2eAgInSc4d1	vícesložkový
sirup	sirup	k1gInSc4	sirup
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
nápoj	nápoj	k1gInSc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Sladkokyselý	sladkokyselý	k2eAgInSc1d1	sladkokyselý
sirup	sirup	k1gInSc1	sirup
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Kofo	Kofo	k1gMnSc1	Kofo
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgInSc6d1	farmaceutický
podniku	podnik	k1gInSc6	podnik
Galena	Galeno	k1gNnSc2	Galeno
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
Opava-Komárov	Opava-Komárov	k1gInSc1	Opava-Komárov
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
skupiny	skupina	k1gFnSc2	skupina
Teva	Teva	k1gMnSc1	Teva
Pharmaceutical	Pharmaceutical	k1gMnSc1	Pharmaceutical
Industries	Industries	k1gMnSc1	Industries
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
s	s	k7c7	s
kolektivem	kolektiv	k1gInSc7	kolektiv
technologicky	technologicky	k6eAd1	technologicky
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
převedl	převést	k5eAaPmAgMnS	převést
čerstvý	čerstvý	k2eAgMnSc1d1	čerstvý
absolvent	absolvent	k1gMnSc1	absolvent
farmacie	farmacie	k1gFnSc2	farmacie
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Knap	Knap	k1gMnSc1	Knap
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
Blažek	Blažek	k1gMnSc1	Blažek
seznámil	seznámit	k5eAaPmAgMnS	seznámit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
československém	československý	k2eAgInSc6d1	československý
sjezdu	sjezd	k1gInSc6	sjezd
lékárníků	lékárník	k1gMnPc2	lékárník
<g/>
.	.	kIx.	.
</s>
<s>
Sirup	sirup	k1gInSc1	sirup
Kofo	Kofo	k6eAd1	Kofo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
přísadou	přísada	k1gFnSc7	přísada
nealkoholického	alkoholický	k2eNgInSc2d1	nealkoholický
nápoje	nápoj	k1gInSc2	nápoj
Kofola	Kofola	k1gFnSc1	Kofola
<g/>
,	,	kIx,	,
představeného	představený	k2eAgMnSc2d1	představený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
je	být	k5eAaImIp3nS	být
Kofola	Kofola	k1gFnSc1	Kofola
prodávána	prodáván	k2eAgFnSc1d1	prodávána
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Kofola	Kofola	k1gFnSc1	Kofola
v	v	k7c6	v
komunistickém	komunistický	k2eAgNnSc6d1	komunistické
Československu	Československo	k1gNnSc6	Československo
stala	stát	k5eAaPmAgFnS	stát
velmi	velmi	k6eAd1	velmi
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nahradila	nahradit	k5eAaPmAgFnS	nahradit
nedostupné	dostupný	k2eNgInPc4d1	nedostupný
západní	západní	k2eAgInPc4d1	západní
kolové	kolový	k2eAgInPc4d1	kolový
nápoje	nápoj	k1gInPc4	nápoj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Coca-Cola	cocaola	k1gFnSc1	coca-cola
nebo	nebo	k8xC	nebo
Pepsi	Pepsi	k1gFnSc1	Pepsi
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
soupeřila	soupeřit	k5eAaImAgFnS	soupeřit
se	s	k7c7	s
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
značkami	značka	k1gFnPc7	značka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
na	na	k7c4	na
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
atraktivní	atraktivní	k2eAgInSc4d1	atraktivní
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nastalo	nastat	k5eAaPmAgNnS	nastat
období	období	k1gNnSc1	období
úpadku	úpadek	k1gInSc2	úpadek
a	a	k8xC	a
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
o	o	k7c4	o
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
firem	firma	k1gFnPc2	firma
produkovalo	produkovat	k5eAaImAgNnS	produkovat
své	své	k1gNnSc4	své
vlastní	vlastní	k2eAgFnSc2d1	vlastní
"	"	kIx"	"
<g/>
kofoly	kofola	k1gFnSc2	kofola
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
neměly	mít	k5eNaImAgInP	mít
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
a	a	k8xC	a
jen	jen	k6eAd1	jen
použily	použít	k5eAaPmAgInP	použít
zavedenou	zavedený	k2eAgFnSc4d1	zavedená
značku	značka	k1gFnSc4	značka
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
výrobcem	výrobce	k1gMnSc7	výrobce
a	a	k8xC	a
distributorem	distributor	k1gMnSc7	distributor
Kofoly	Kofola	k1gFnSc2	Kofola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
republice	republika	k1gFnSc6	republika
firma	firma	k1gFnSc1	firma
Santa	Sant	k1gInSc2	Sant
nápoje	nápoj	k1gInSc2	nápoj
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Krnově	Krnov	k1gInSc6	Krnov
<g/>
,	,	kIx,	,
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
řeckou	řecký	k2eAgFnSc7d1	řecká
rodinou	rodina	k1gFnSc7	rodina
Samarasových	Samarasová	k1gFnPc2	Samarasová
<g/>
.	.	kIx.	.
</s>
<s>
Odkup	odkup	k1gInSc1	odkup
receptury	receptura	k1gFnSc2	receptura
vyšel	vyjít	k5eAaPmAgInS	vyjít
nové	nový	k2eAgMnPc4d1	nový
majitele	majitel	k1gMnPc4	majitel
na	na	k7c4	na
215	[number]	k4	215
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
postavila	postavit	k5eAaPmAgFnS	postavit
společnost	společnost	k1gFnSc1	společnost
novou	nový	k2eAgFnSc4d1	nová
továrnu	továrna	k1gFnSc4	továrna
v	v	k7c6	v
Rajecké	Rajecká	k1gFnSc6	Rajecká
Lesné	lesný	k2eAgFnSc2d1	Lesná
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stáčí	stáčet	k5eAaImIp3nS	stáčet
pramenitou	pramenitý	k2eAgFnSc4d1	pramenitá
vodu	voda	k1gFnSc4	voda
Rajec	Rajec	k1gMnSc1	Rajec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Kofola	Kofola	k1gFnSc1	Kofola
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Kofoly	Kofola	k1gFnSc2	Kofola
též	též	k9	též
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
další	další	k2eAgInPc4d1	další
nealkoholické	alkoholický	k2eNgInPc4d1	nealkoholický
nápoje	nápoj	k1gInPc4	nápoj
(	(	kIx(	(
<g/>
Top	topit	k5eAaImRp2nS	topit
Topic	Topic	k1gMnSc1	Topic
<g/>
,	,	kIx,	,
Jupí	Jupí	k1gMnSc1	Jupí
<g/>
,	,	kIx,	,
Jupík	Jupík	k1gMnSc1	Jupík
<g/>
,	,	kIx,	,
Chito	Chito	k1gNnSc1	Chito
<g/>
,	,	kIx,	,
Rajec	Rajec	k1gInSc1	Rajec
<g/>
,	,	kIx,	,
v	v	k7c6	v
licenci	licence	k1gFnSc6	licence
RC	RC	kA	RC
Colu	cola	k1gFnSc4	cola
<g/>
,	,	kIx,	,
Capri	Capr	k1gMnSc5	Capr
Sonne	Sonn	k1gMnSc5	Sonn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyváží	vyvázat	k5eAaPmIp3nP	vyvázat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
-	-	kIx~	-
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
především	především	k6eAd1	především
Jupí	Jupí	k2eAgInSc4d1	Jupí
a	a	k8xC	a
Jupík	Jupík	k1gInSc4	Jupík
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
také	také	k9	také
závod	závod	k1gInSc1	závod
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
Kutně	kutně	k6eAd1	kutně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
především	především	k6eAd1	především
džusy	džus	k1gInPc4	džus
Jupí	Jupum	k1gNnPc2	Jupum
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
českých	český	k2eAgFnPc2d1	Česká
investic	investice	k1gFnPc2	investice
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
Kofola	Kofola	k1gFnSc1	Kofola
dodává	dodávat	k5eAaImIp3nS	dodávat
i	i	k9	i
v	v	k7c6	v
0,5	[number]	k4	0,5
a	a	k8xC	a
2	[number]	k4	2
<g/>
litrových	litrový	k2eAgFnPc2d1	litrová
PET	PET	kA	PET
lahvích	lahev	k1gFnPc6	lahev
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
k	k	k7c3	k
0,33	[number]	k4	0,33
<g/>
litrovým	litrový	k2eAgFnPc3d1	litrová
skleněným	skleněný	k2eAgFnPc3d1	skleněná
lahvím	lahev	k1gFnPc3	lahev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtlitrové	čtvrtlitrový	k2eAgFnPc1d1	čtvrtlitrová
plechovky	plechovka	k1gFnPc1	plechovka
byly	být	k5eAaImAgFnP	být
zavedeny	zavést	k5eAaPmNgFnP	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
litrové	litrový	k2eAgNnSc4d1	litrové
PET	PET	kA	PET
lahve	lahev	k1gFnSc2	lahev
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Čepování	čepování	k1gNnSc1	čepování
Kofoly	Kofola	k1gFnSc2	Kofola
z	z	k7c2	z
50	[number]	k4	50
<g/>
litrových	litrový	k2eAgInPc2d1	litrový
sudů	sud	k1gInPc2	sud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
barech	bar	k1gInPc6	bar
a	a	k8xC	a
restauracích	restaurace	k1gFnPc6	restaurace
také	také	k6eAd1	také
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
spustil	spustit	k5eAaPmAgMnS	spustit
výrobce	výrobce	k1gMnSc1	výrobce
mediální	mediální	k2eAgFnSc4d1	mediální
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
,	,	kIx,	,
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
mladé	mladý	k2eAgFnPc4d1	mladá
a	a	k8xC	a
moderní	moderní	k2eAgNnSc1d1	moderní
publikum	publikum	k1gNnSc1	publikum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
sloganu	slogan	k1gInSc6	slogan
"	"	kIx"	"
<g/>
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
miluješ	milovat	k5eAaImIp2nS	milovat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
co	co	k3yQnSc4	co
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
/	/	kIx~	/
Keď	Keď	k1gFnSc1	Keď
ju	ju	k0	ju
miluješ	milovat	k5eAaImIp2nS	milovat
<g/>
,	,	kIx,	,
nie	nie	k?	nie
je	být	k5eAaImIp3nS	být
čo	čo	k?	čo
riešiť	riešiť	k1gFnSc1	riešiť
<g/>
.	.	kIx.	.
/	/	kIx~	/
When	When	k1gInSc1	When
you	you	k?	you
love	lov	k1gInSc5	lov
her	hra	k1gFnPc2	hra
nothing	nothing	k1gInSc4	nothing
else	elsat	k5eAaPmIp3nS	elsat
matters	matters	k6eAd1	matters
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zobrazovalo	zobrazovat	k5eAaImAgNnS	zobrazovat
logo	logo	k1gNnSc1	logo
Kofoly	Kofola	k1gFnSc2	Kofola
kávové	kávový	k2eAgNnSc1d1	kávové
zrno	zrno	k1gNnSc1	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
připomíná	připomínat	k5eAaImIp3nS	připomínat
lístek	lístek	k1gInSc4	lístek
lékořice	lékořice	k1gFnSc2	lékořice
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
výrobců	výrobce	k1gMnPc2	výrobce
nápojů	nápoj	k1gInPc2	nápoj
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
slovenském	slovenský	k2eAgInSc6d1	slovenský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
maďarském	maďarský	k2eAgInSc6d1	maďarský
a	a	k8xC	a
polském	polský	k2eAgInSc6d1	polský
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
tržby	tržba	k1gFnPc1	tržba
hodnoty	hodnota	k1gFnSc2	hodnota
4,4	[number]	k4	4,4
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
Kofola	Kofola	k1gFnSc1	Kofola
sloučení	sloučení	k1gNnSc2	sloučení
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
výrobcem	výrobce	k1gMnSc7	výrobce
sycených	sycený	k2eAgInPc2d1	sycený
nápojů	nápoj	k1gInPc2	nápoj
Hoop	Hoop	k1gInSc4	Hoop
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
řecká	řecký	k2eAgFnSc1d1	řecká
rodina	rodina	k1gFnSc1	rodina
Kostase	Kostasa	k1gFnSc3	Kostasa
Samarase	Samarasa	k1gFnSc3	Samarasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
jméno	jméno	k1gNnSc1	jméno
společnosti	společnost	k1gFnSc2	společnost
Kofola	Kofola	k1gFnSc1	Kofola
-	-	kIx~	-
Hoop	Hoop	k1gInSc1	Hoop
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
na	na	k7c4	na
Kofola	Kofola	k1gFnSc1	Kofola
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
Rozšíření	rozšíření	k1gNnSc1	rozšíření
výrobních	výrobní	k2eAgInPc2d1	výrobní
a	a	k8xC	a
skladovacích	skladovací	k2eAgFnPc2d1	skladovací
prostor	prostora	k1gFnPc2	prostora
v	v	k7c6	v
Rajecké	Rajecká	k1gFnSc6	Rajecká
Lesné	lesný	k2eAgFnSc2d1	Lesná
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Kofola	Kofola	k1gFnSc1	Kofola
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
společnost	společnost	k1gFnSc1	společnost
PINELLI	PINELLI	kA	PINELLI
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
energetický	energetický	k2eAgInSc1d1	energetický
nápoj	nápoj	k1gInSc1	nápoj
Semtex	semtex	k1gInSc1	semtex
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
koupila	koupit	k5eAaPmAgFnS	koupit
Kofola	Kofola	k1gFnSc1	Kofola
slovinskou	slovinský	k2eAgFnSc4d1	slovinská
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
minerálky	minerálka	k1gFnPc4	minerálka
Radenska	Radensko	k1gNnSc2	Radensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
uvedla	uvést	k5eAaPmAgFnS	uvést
firma	firma	k1gFnSc1	firma
novou	nový	k2eAgFnSc4d1	nová
reklamní	reklamní	k2eAgFnSc4d1	reklamní
kampaň	kampaň	k1gFnSc4	kampaň
se	s	k7c7	s
šišlajícím	šišlající	k2eAgMnSc7d1	šišlající
psem	pes	k1gMnSc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Reklamy	reklama	k1gFnPc1	reklama
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
natočené	natočený	k2eAgNnSc1d1	natočené
s	s	k7c7	s
živým	živý	k2eAgNnSc7d1	živé
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nenašel	najít	k5eNaPmAgMnS	najít
vhodný	vhodný	k2eAgMnSc1d1	vhodný
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
použít	použít	k5eAaPmF	použít
místo	místo	k7c2	místo
psa	pes	k1gMnSc2	pes
animaci	animace	k1gFnSc4	animace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
Kofola	Kofola	k1gFnSc1	Kofola
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
burzu	burza	k1gFnSc4	burza
<g/>
.	.	kIx.	.
</s>
<s>
Investorům	investor	k1gMnPc3	investor
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
6,7	[number]	k4	6,7
procenta	procento	k1gNnSc2	procento
akcií	akcie	k1gFnPc2	akcie
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úpisová	Úpisový	k2eAgFnSc1d1	Úpisový
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
510	[number]	k4	510
korun	koruna	k1gFnPc2	koruna
za	za	k7c4	za
akcii	akcie	k1gFnSc4	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
výrobce	výrobce	k1gMnSc1	výrobce
se	se	k3xPyFc4	se
produkce	produkce	k1gFnSc2	produkce
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
především	především	k9	především
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Kofola	Kofola	k1gFnSc1	Kofola
protipólem	protipól	k1gInSc7	protipól
"	"	kIx"	"
<g/>
kapitalistické	kapitalistický	k2eAgFnPc4d1	kapitalistická
<g/>
"	"	kIx"	"
Coca-Coly	cocaola	k1gFnPc4	coca-cola
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
produkce	produkce	k1gFnSc1	produkce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
179	[number]	k4	179
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
Kofola	Kofola	k1gFnSc1	Kofola
ústup	ústup	k1gInSc4	ústup
ze	z	k7c2	z
scény	scéna	k1gFnSc2	scéna
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
hladový	hladový	k2eAgInSc4d1	hladový
trh	trh	k1gInSc4	trh
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
Coca-Cola	cocaola	k1gFnSc1	coca-cola
a	a	k8xC	a
Pepsi	Pepsi	k1gFnSc1	Pepsi
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byla	být	k5eAaImAgFnS	být
produkce	produkce	k1gFnSc1	produkce
na	na	k7c6	na
historickém	historický	k2eAgNnSc6d1	historické
minimu	minimum	k1gNnSc6	minimum
26	[number]	k4	26
miliónů	milión	k4xCgInPc2	milión
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
začala	začít	k5eAaPmAgFnS	začít
produkce	produkce	k1gFnSc1	produkce
stoupat	stoupat	k5eAaImF	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
finanční	finanční	k2eAgInSc1d1	finanční
obrat	obrat	k1gInSc1	obrat
firmy	firma	k1gFnSc2	firma
Kofola	Kofola	k1gFnSc1	Kofola
4,5	[number]	k4	4,5
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
je	být	k5eAaImIp3nS	být
Kofola	Kofola	k1gFnSc1	Kofola
největším	veliký	k2eAgMnSc7d3	veliký
konkurentem	konkurent	k1gMnSc7	konkurent
Coca-Coly	cocaola	k1gFnSc2	coca-cola
a	a	k8xC	a
Pepsi	Pepsi	k1gFnSc2	Pepsi
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
na	na	k7c6	na
slovenském	slovenský	k2eAgInSc6d1	slovenský
trhu	trh	k1gInSc6	trh
prodalo	prodat	k5eAaPmAgNnS	prodat
14,28	[number]	k4	14,28
milionu	milion	k4xCgInSc2	milion
litrů	litr	k1gInPc2	litr
Kofoly	Kofola	k1gFnSc2	Kofola
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
prodej	prodej	k1gInSc1	prodej
hodnoty	hodnota	k1gFnSc2	hodnota
19,44	[number]	k4	19,44
milionu	milion	k4xCgInSc2	milion
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
si	se	k3xPyFc3	se
17	[number]	k4	17
%	%	kIx~	%
slovenských	slovenský	k2eAgMnPc2d1	slovenský
konzumentů	konzument	k1gMnPc2	konzument
kolových	kolový	k2eAgInPc2d1	kolový
nealkoholických	alkoholický	k2eNgInPc2d1	nealkoholický
nápojů	nápoj	k1gInPc2	nápoj
kupuje	kupovat	k5eAaImIp3nS	kupovat
nejčastěji	často	k6eAd3	často
Kofolu	Kofola	k1gFnSc4	Kofola
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
14	[number]	k4	14
%	%	kIx~	%
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
Coca-Colu	cocaola	k1gFnSc4	coca-cola
<g/>
.	.	kIx.	.
</s>
<s>
Tržní	tržní	k2eAgInSc1d1	tržní
podíl	podíl	k1gInSc1	podíl
Kofoly	Kofola	k1gFnSc2	Kofola
se	se	k3xPyFc4	se
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
(	(	kIx(	(
<g/>
4,6	[number]	k4	4,6
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
9,4	[number]	k4	9,4
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kofola	Kofola	k1gFnSc1	Kofola
obsadila	obsadit	k5eAaPmAgFnS	obsadit
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
slovenském	slovenský	k2eAgInSc6d1	slovenský
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
za	za	k7c7	za
Coca-Colou	cocaola	k1gFnSc7	coca-cola
(	(	kIx(	(
<g/>
11,5	[number]	k4	11,5
%	%	kIx~	%
v	v	k7c6	v
roku	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
Walmarkem	Walmarko	k1gNnSc7	Walmarko
(	(	kIx(	(
<g/>
9,6	[number]	k4	9,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
předběhla	předběhnout	k5eAaPmAgFnS	předběhnout
Pepsi	Pepsi	k1gFnPc7	Pepsi
(	(	kIx(	(
<g/>
5,5	[number]	k4	5,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kofola	Kofola	k1gFnSc1	Kofola
SA	SA	kA	SA
je	být	k5eAaImIp3nS	být
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnSc1d1	zabývající
se	se	k3xPyFc4	se
odvětvím	odvětvit	k5eAaPmIp1nS	odvětvit
nealkoholických	alkoholický	k2eNgInPc2d1	nealkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
výrobcem	výrobce	k1gMnSc7	výrobce
a	a	k8xC	a
distributorem	distributor	k1gMnSc7	distributor
sycených	sycený	k2eAgInPc2d1	sycený
a	a	k8xC	a
nesycených	sycený	k2eNgInPc2d1	nesycený
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
džusů	džus	k1gInPc2	džus
<g/>
,	,	kIx,	,
ovocných	ovocný	k2eAgInPc2d1	ovocný
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
dětských	dětský	k2eAgInPc2d1	dětský
nápojů	nápoj	k1gInPc2	nápoj
a	a	k8xC	a
energetických	energetický	k2eAgInPc2d1	energetický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Produktové	produktový	k2eAgNnSc1d1	produktové
portfolio	portfolio	k1gNnSc1	portfolio
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kolových	kolový	k2eAgInPc2d1	kolový
nápojů	nápoj	k1gInPc2	nápoj
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Hoop	Hoop	k1gMnSc1	Hoop
Cola	cola	k1gFnSc1	cola
<g/>
,	,	kIx,	,
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
Artic	Artic	k1gMnSc1	Artic
Classic	Classic	k1gMnSc1	Classic
a	a	k8xC	a
Woda	Wod	k2eAgFnSc1d1	Wod
Grodziska	Grodziska	k1gFnSc1	Grodziska
<g/>
,	,	kIx,	,
sirupy	sirup	k1gInPc1	sirup
Paola	Paol	k1gMnSc2	Paol
<g/>
,	,	kIx,	,
Jarmark	Jarmark	k?	Jarmark
Polski	Polsk	k1gFnPc1	Polsk
<g/>
,	,	kIx,	,
Jupi	Jup	k1gFnPc1	Jup
<g/>
,	,	kIx,	,
Jupík	Jupík	k1gInSc1	Jupík
a	a	k8xC	a
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgMnPc7d1	ostatní
také	také	k9	také
ledové	ledový	k2eAgInPc4d1	ledový
čaje	čaj	k1gInPc4	čaj
jako	jako	k8xS	jako
Pickwick	Pickwick	k1gInSc4	Pickwick
Ice	Ice	k1gFnSc2	Ice
Tea	Tea	k1gFnSc1	Tea
a	a	k8xC	a
Happy	Happa	k1gFnPc1	Happa
Frutti	Frutť	k1gFnSc2	Frutť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Kofola	Kofola	k1gFnSc1	Kofola
Group	Group	k1gInSc4	Group
<g/>
,	,	kIx,	,
i	i	k8xC	i
navzdory	navzdory	k7c3	navzdory
vysokým	vysoký	k2eAgFnPc3d1	vysoká
cenám	cena	k1gFnPc3	cena
komodit	komodita	k1gFnPc2	komodita
a	a	k8xC	a
slabší	slabý	k2eAgFnSc6d2	slabší
poptávce	poptávka	k1gFnSc6	poptávka
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
roční	roční	k2eAgInSc4d1	roční
obrat	obrat	k1gInSc4	obrat
z	z	k7c2	z
1	[number]	k4	1
013	[number]	k4	013
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
PLN	plno	k1gNnPc2	plno
na	na	k7c4	na
1	[number]	k4	1
016	[number]	k4	016
milionů	milion	k4xCgInPc2	milion
PLN	plno	k1gNnPc2	plno
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnPc1	společnost
se	se	k3xPyFc4	se
také	také	k9	také
podařilo	podařit	k5eAaPmAgNnS	podařit
zvýšit	zvýšit	k5eAaPmF	zvýšit
provozní	provozní	k2eAgInSc4d1	provozní
zisk	zisk	k1gInSc4	zisk
(	(	kIx(	(
<g/>
EBIT	EBIT	kA	EBIT
<g/>
)	)	kIx)	)
o	o	k7c4	o
1,6	[number]	k4	1,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
segmentech	segment	k1gInPc6	segment
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
snížila	snížit	k5eAaPmAgFnS	snížit
úroveň	úroveň	k1gFnSc1	úroveň
zadlužení	zadlužení	k1gNnSc4	zadlužení
<g/>
.	.	kIx.	.
</s>
<s>
Kofola	Kofola	k1gFnSc1	Kofola
SA	SA	kA	SA
také	také	k9	také
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zaměří	zaměřit	k5eAaPmIp3nS	zaměřit
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
kromě	kromě	k7c2	kromě
výběrových	výběrový	k2eAgFnPc2d1	výběrová
restaurací	restaurace	k1gFnPc2	restaurace
i	i	k9	i
na	na	k7c4	na
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
trh	trh	k1gInSc4	trh
se	s	k7c7	s
zdravými	zdravý	k2eAgInPc7d1	zdravý
nápoji	nápoj	k1gInPc7	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Kofole	Kofole	k1gFnSc1	Kofole
SA	SA	kA	SA
Group	Group	k1gInSc1	Group
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
Perla	perla	k1gFnSc1	perla
polského	polský	k2eAgNnSc2d1	polské
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
"	"	kIx"	"
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Velké	velký	k2eAgFnSc2d1	velká
perly	perla	k1gFnSc2	perla
11	[number]	k4	11
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
redakcí	redakce	k1gFnSc7	redakce
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
magazínu	magazín	k1gInSc2	magazín
Polish	Polish	k1gInSc1	Polish
Market	market	k1gInSc1	market
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
ústavem	ústav	k1gInSc7	ústav
PAN	Pan	k1gMnSc1	Pan
(	(	kIx(	(
<g/>
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
Kofoly	Kofola	k1gFnSc2	Kofola
je	být	k5eAaImIp3nS	být
sirup	sirup	k1gInSc4	sirup
KOFO	KOFO	kA	KOFO
<g/>
.	.	kIx.	.
</s>
<s>
Sirup	sirup	k1gInSc1	sirup
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nS	mísit
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
Kofola	Kofola	k1gFnSc1	Kofola
meziročně	meziročně	k6eAd1	meziročně
jemné	jemný	k2eAgFnPc4d1	jemná
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c6	v
chuti	chuť	k1gFnSc6	chuť
podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
sezónního	sezónní	k2eAgNnSc2d1	sezónní
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
dozrávání	dozrávání	k1gNnSc2	dozrávání
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Sirup	sirup	k1gInSc1	sirup
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
14	[number]	k4	14
bylinných	bylinný	k2eAgFnPc2d1	bylinná
a	a	k8xC	a
ovocných	ovocný	k2eAgFnPc2d1	ovocná
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
výtažky	výtažek	k1gInPc1	výtažek
z	z	k7c2	z
jablek	jablko	k1gNnPc2	jablko
<g/>
,	,	kIx,	,
třešní	třešeň	k1gFnPc2	třešeň
a	a	k8xC	a
rybízu	rybíz	k1gInSc2	rybíz
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rostlinného	rostlinný	k2eAgNnSc2d1	rostlinné
aroma	aroma	k1gNnSc2	aroma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
karamelu	karamel	k1gInSc2	karamel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
je	být	k5eAaImIp3nS	být
složení	složení	k1gNnSc4	složení
sirupu	sirup	k1gInSc2	sirup
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
Cukr	cukr	k1gInSc1	cukr
rafinovaný	rafinovaný	k2eAgInSc1d1	rafinovaný
<g/>
,	,	kIx,	,
jablečná	jablečný	k2eAgFnSc1d1	jablečná
a	a	k8xC	a
rybízová	rybízový	k2eAgFnSc1d1	rybízová
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
,	,	kIx,	,
extrakt	extrakt	k1gInSc1	extrakt
z	z	k7c2	z
lékořice	lékořice	k1gFnSc2	lékořice
a	a	k8xC	a
maliníku	maliník	k1gInSc2	maliník
<g/>
,	,	kIx,	,
světlý	světlý	k2eAgInSc1d1	světlý
a	a	k8xC	a
tmavý	tmavý	k2eAgInSc1d1	tmavý
karamel	karamel	k1gInSc1	karamel
<g/>
,	,	kIx,	,
silice	silice	k1gFnSc1	silice
z	z	k7c2	z
anýzu	anýz	k1gInSc2	anýz
<g/>
,	,	kIx,	,
kardamomu	kardamom	k1gInSc2	kardamom
<g/>
,	,	kIx,	,
pomeranče	pomeranč	k1gInSc2	pomeranč
a	a	k8xC	a
skořice	skořice	k1gFnSc2	skořice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zveřejněných	zveřejněný	k2eAgInPc2d1	zveřejněný
údajů	údaj	k1gInPc2	údaj
na	na	k7c6	na
etiketě	etiketa	k1gFnSc6	etiketa
od	od	k7c2	od
výrobce	výrobce	k1gMnSc2	výrobce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
Ovocný	ovocný	k2eAgInSc1d1	ovocný
sirup	sirup	k1gInSc1	sirup
<g/>
,	,	kIx,	,
glukózo-fruktózový	glukózoruktózový	k2eAgInSc1d1	glukózo-fruktózový
sirup	sirup	k1gInSc1	sirup
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
karemel-pálený	karemelálený	k2eAgInSc1d1	karemel-pálený
cukr	cukr	k1gInSc1	cukr
<g/>
,	,	kIx,	,
barvivo	barvivo	k1gNnSc1	barvivo
E	E	kA	E
<g/>
150	[number]	k4	150
<g/>
d	d	k?	d
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
citrónovou	citrónový	k2eAgFnSc4d1	citrónová
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc4	chlorid
sodný	sodný	k2eAgInSc4d1	sodný
<g/>
,	,	kIx,	,
esenci	esence	k1gFnSc4	esence
pro	pro	k7c4	pro
KOFO	KOFO	kA	KOFO
(	(	kIx(	(
<g/>
aroma	aroma	k1gNnSc1	aroma
<g/>
,	,	kIx,	,
bylinný	bylinný	k2eAgInSc1d1	bylinný
extrakt	extrakt	k1gInSc1	extrakt
<g/>
,	,	kIx,	,
lékořicový	lékořicový	k2eAgInSc1d1	lékořicový
extrakt	extrakt	k1gInSc1	extrakt
<g/>
,	,	kIx,	,
kofein	kofein	k1gInSc1	kofein
<g/>
,	,	kIx,	,
konzervant	konzervant	k1gInSc1	konzervant
E	E	kA	E
<g/>
211	[number]	k4	211
<g/>
)	)	kIx)	)
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
sirup	sirup	k1gInSc1	sirup
KOFO	KOFO	kA	KOFO
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc4d1	další
aroma	aroma	k1gNnPc4	aroma
u	u	k7c2	u
Kofoly	Kofola	k1gFnSc2	Kofola
s	s	k7c7	s
příchutěmi	příchuť	k1gFnPc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Kofola	Kofola	k1gFnSc1	Kofola
bez	bez	k7c2	bez
cukru	cukr	k1gInSc2	cukr
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
cukr	cukr	k1gInSc4	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c4	o
složení	složení	k1gNnPc4	složení
zveřejněná	zveřejněný	k2eAgNnPc4d1	zveřejněné
na	na	k7c6	na
výrobcích	výrobce	k1gMnPc6	výrobce
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
nová	nový	k2eAgFnSc1d1	nová
Kofola	Kofola	k1gFnSc1	Kofola
Citrus	citrus	k1gInSc1	citrus
<g/>
,	,	kIx,	,
s	s	k7c7	s
citrónovou	citrónový	k2eAgFnSc7d1	citrónová
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
vánoční	vánoční	k2eAgFnSc1d1	vánoční
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
<g/>
,	,	kIx,	,
Kofola	Kofola	k1gFnSc1	Kofola
Skořicová	skořicová	k1gFnSc1	skořicová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skořicovou	skořicový	k2eAgFnSc7d1	skořicová
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
Kofola	Kofola	k1gFnSc1	Kofola
Bez	bez	k7c2	bez
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádný	žádný	k3yNgInSc4	žádný
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
alternativou	alternativa	k1gFnSc7	alternativa
ke	k	k7c3	k
Kofole	Kofola	k1gFnSc3	Kofola
Original	Original	k1gFnSc2	Original
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
další	další	k2eAgFnSc1d1	další
vánoční	vánoční	k2eAgFnSc1d1	vánoční
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
<g/>
,	,	kIx,	,
Kofola	Kofola	k1gFnSc1	Kofola
Barborková	Barborková	k1gFnSc1	Barborková
<g/>
,	,	kIx,	,
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
chutí	chuť	k1gFnSc7	chuť
třešní	třešeň	k1gFnPc2	třešeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xC	jako
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
před	před	k7c7	před
Vánoci	Vánoce	k1gFnPc7	Vánoce
speciální	speciální	k2eAgFnSc1d1	speciální
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
Kofola	Kofola	k1gFnSc1	Kofola
Hvězdičková	hvězdičkový	k2eAgFnSc1d1	hvězdičková
s	s	k7c7	s
příchutí	příchuť	k1gFnSc7	příchuť
granátového	granátový	k2eAgNnSc2d1	granátové
jablka	jablko	k1gNnSc2	jablko
a	a	k8xC	a
vanilky	vanilka	k1gFnSc2	vanilka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
přichuť	přichuť	k1gFnSc1	přichuť
Kofola	Kofola	k1gFnSc1	Kofola
Višňová	višňový	k2eAgFnSc1d1	Višňová
<g/>
,	,	kIx,	,
s	s	k7c7	s
višňovou	višňový	k2eAgFnSc7d1	Višňová
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
příchuť	příchuť	k1gFnSc1	příchuť
Kofola	Kofola	k1gFnSc1	Kofola
Extra	extra	k6eAd1	extra
Bylinková	bylinkový	k2eAgFnSc1d1	bylinková
<g/>
,	,	kIx,	,
s	s	k7c7	s
příchutí	příchuť	k1gFnSc7	příchuť
máty	máta	k1gFnSc2	máta
<g/>
,	,	kIx,	,
hořce	hořec	k1gInSc2	hořec
a	a	k8xC	a
pampelišky	pampeliška	k1gFnSc2	pampeliška
<g/>
.	.	kIx.	.
</s>
<s>
Recepturu	receptura	k1gFnSc4	receptura
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
spolutvůrců	spolutvůrce	k1gMnPc2	spolutvůrce
Kofoly	Kofola	k1gFnSc2	Kofola
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Knap	Knap	k1gMnSc1	Knap
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
nová	nový	k2eAgFnSc1d1	nová
příchuť	příchuť	k1gFnSc1	příchuť
Kofola	Kofola	k1gFnSc1	Kofola
Festivalová	festivalový	k2eAgFnSc1d1	festivalová
<g/>
,	,	kIx,	,
s	s	k7c7	s
příchutí	příchuť	k1gFnSc7	příchuť
guave	guaev	k1gFnSc2	guaev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
nová	nový	k2eAgFnSc1d1	nová
příchuť	příchuť	k1gFnSc1	příchuť
Kofola	Kofola	k1gFnSc1	Kofola
s	s	k7c7	s
Guaranou	Guarana	k1gFnSc7	Guarana
s	s	k7c7	s
příchutí	příchuť	k1gFnSc7	příchuť
guarany	guarana	k1gFnSc2	guarana
v	v	k7c6	v
ergonomicky	ergonomicky	k6eAd1	ergonomicky
tvarované	tvarovaný	k2eAgFnSc6d1	tvarovaná
1	[number]	k4	1
<g/>
l	l	kA	l
lahvi	lahev	k1gFnSc5	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
původní	původní	k2eAgFnSc3d1	původní
Kofole	Kofola	k1gFnSc3	Kofola
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
více	hodně	k6eAd2	hodně
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
vánoční	vánoční	k2eAgFnSc1d1	vánoční
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
<g/>
,	,	kIx,	,
Kofola	Kofola	k1gFnSc1	Kofola
Vanilková	vanilkový	k2eAgFnSc1d1	vanilková
<g/>
,	,	kIx,	,
s	s	k7c7	s
příchutí	příchuť	k1gFnSc7	příchuť
vanilky	vanilka	k1gFnSc2	vanilka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
opět	opět	k6eAd1	opět
vydána	vydán	k2eAgFnSc1d1	vydána
Kofola	Kofola	k1gFnSc1	Kofola
Vanilková	vanilkový	k2eAgFnSc1d1	vanilková
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
trvale	trvale	k6eAd1	trvale
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
limitovaně	limitovaně	k6eAd1	limitovaně
opět	opět	k6eAd1	opět
vánoční	vánoční	k2eAgFnSc1d1	vánoční
Kofola	Kofola	k1gFnSc1	Kofola
Skořicová	skořicová	k1gFnSc1	skořicová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skořicovou	skořicový	k2eAgFnSc7d1	skořicová
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
vánoční	vánoční	k2eAgFnSc1d1	vánoční
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
Kofoly	Kofola	k1gFnSc2	Kofola
Marcipánové	marcipánový	k2eAgFnPc1d1	marcipánová
<g/>
,	,	kIx,	,
s	s	k7c7	s
mandlovou	mandlový	k2eAgFnSc7d1	Mandlová
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
malý	malý	k2eAgInSc4d1	malý
zájem	zájem	k1gInSc4	zájem
kofoly	kofola	k1gFnSc2	kofola
Marcipánové	marcipánový	k2eAgFnSc2d1	marcipánová
vydána	vydán	k2eAgFnSc1d1	vydána
taktéž	taktéž	k?	taktéž
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
vánoční	vánoční	k2eAgFnSc1d1	vánoční
edice	edice	k1gFnSc1	edice
kofoly	kofola	k1gFnSc2	kofola
perníkové	perníkový	k2eAgFnSc2d1	Perníková
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
nová	nový	k2eAgFnSc1d1	nová
Kofola	Kofola	k1gFnSc1	Kofola
Meruňková	meruňkový	k2eAgFnSc1d1	meruňková
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
stáčena	stáčen	k2eAgFnSc1d1	stáčena
i	i	k9	i
do	do	k7c2	do
1,5	[number]	k4	1,5
l	l	kA	l
lahví	lahev	k1gFnPc2	lahev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
vánoční	vánoční	k2eAgFnSc1d1	vánoční
edice	edice	k1gFnSc1	edice
Kofoly	Kofola	k1gFnSc2	Kofola
Čokoládové	čokoládový	k2eAgFnPc1d1	čokoládová
<g/>
,	,	kIx,	,
s	s	k7c7	s
čokoládovou	čokoládový	k2eAgFnSc7d1	čokoládová
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
představena	představen	k2eAgFnSc1d1	představena
Kofola	Kofola	k1gFnSc1	Kofola
Meloun	meloun	k1gInSc4	meloun
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
s	s	k7c7	s
melounovou	melounový	k2eAgFnSc7d1	Melounová
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
představena	představen	k2eAgFnSc1d1	představena
Kofola	Kofola	k1gFnSc1	Kofola
Kokos	kokos	k1gInSc1	kokos
<g/>
,	,	kIx,	,
s	s	k7c7	s
kokosovou	kokosový	k2eAgFnSc7d1	kokosová
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
představena	představen	k2eAgFnSc1d1	představena
Kofola	Kofola	k1gFnSc1	Kofola
Vlašský	vlašský	k2eAgInSc1d1	vlašský
ořech	ořech	k1gInSc1	ořech
<g/>
,	,	kIx,	,
s	s	k7c7	s
ořechovou	ořechový	k2eAgFnSc7d1	ořechová
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Kofola	Kofola	k1gFnSc1	Kofola
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
do	do	k7c2	do
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
míchaných	míchaný	k2eAgInPc2d1	míchaný
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
míchaným	míchaný	k2eAgInSc7d1	míchaný
nápojem	nápoj	k1gInSc7	nápoj
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Kofola	Kofola	k1gFnSc1	Kofola
s	s	k7c7	s
tuzemským	tuzemský	k2eAgInSc7d1	tuzemský
rumem	rum	k1gInSc7	rum
<g/>
,	,	kIx,	,
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
varianta	varianta	k1gFnSc1	varianta
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
"	"	kIx"	"
<g/>
houba	houba	k1gFnSc1	houba
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
bambus	bambus	k1gInSc1	bambus
<g/>
"	"	kIx"	"
-	-	kIx~	-
vína	vína	k1gFnSc1	vína
s	s	k7c7	s
kolou	kola	k1gFnSc7	kola
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
s	s	k7c7	s
Kofolou	Kofola	k1gFnSc7	Kofola
<g/>
.	.	kIx.	.
</s>
