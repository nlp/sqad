<p>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
je	být	k5eAaImIp3nS	být
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
Třináct	třináct	k4xCc1	třináct
kolonií	kolonie	k1gFnPc2	kolonie
deklarovalo	deklarovat	k5eAaBmAgNnS	deklarovat
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
kterým	který	k3yIgMnPc3	který
ospravedlnilo	ospravedlnit	k5eAaPmAgNnS	ospravedlnit
své	své	k1gNnSc1	své
konání	konání	k1gNnSc2	konání
<g/>
.	.	kIx.	.
</s>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
byla	být	k5eAaImAgFnS	být
ratifikována	ratifikovat	k5eAaBmNgFnS	ratifikovat
Kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
Kongresem	kongres	k1gInSc7	kongres
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
slaven	slaven	k2eAgInSc4d1	slaven
jako	jako	k9	jako
Den	den	k1gInSc4	den
Nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
je	být	k5eAaImIp3nS	být
státním	státní	k2eAgInSc7d1	státní
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
Deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pozadí	pozadí	k1gNnSc1	pozadí
deklarace	deklarace	k1gFnSc2	deklarace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
vztahy	vztah	k1gInPc7	vztah
mezi	mezi	k7c7	mezi
třinácti	třináct	k4xCc2	třináct
severoamerickými	severoamerický	k2eAgFnPc7d1	severoamerická
koloniemi	kolonie	k1gFnPc7	kolonie
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
vyostřily	vyostřit	k5eAaPmAgInP	vyostřit
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
propukly	propuknout	k5eAaPmAgInP	propuknout
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
v	v	k7c6	v
Lexingtonu	Lexington	k1gInSc6	Lexington
a	a	k8xC	a
Concordu	Concordo	k1gNnSc6	Concordo
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xS	jako
začátek	začátek	k1gInSc1	začátek
amerického	americký	k2eAgInSc2d1	americký
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
panovaly	panovat	k5eAaImAgFnP	panovat
jisté	jistý	k2eAgFnPc1d1	jistá
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
úplné	úplný	k2eAgFnSc2d1	úplná
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
brožurka	brožurka	k1gFnSc1	brožurka
Common	Common	k1gInSc1	Common
Sense	Sense	k1gFnSc1	Sense
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
přeložitelné	přeložitelný	k2eAgFnPc1d1	přeložitelná
jako	jako	k8xS	jako
selský	selský	k2eAgInSc1d1	selský
rozum	rozum	k1gInSc1	rozum
<g/>
)	)	kIx)	)
od	od	k7c2	od
Thomase	Thomas	k1gMnSc2	Thomas
Paina	Pain	k1gMnSc2	Pain
dokázala	dokázat	k5eAaPmAgFnS	dokázat
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
úplná	úplný	k2eAgFnSc1d1	úplná
nezávislost	nezávislost	k1gFnSc1	nezávislost
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
možná	možný	k2eAgFnSc1d1	možná
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
kolonie	kolonie	k1gFnPc1	kolonie
ubírat	ubírat	k5eAaImF	ubírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Leeovou	Leeový	k2eAgFnSc7d1	Leeová
rezolucí	rezoluce	k1gFnSc7	rezoluce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
na	na	k7c6	na
Kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
kongresu	kongres	k1gInSc6	kongres
Richard	Richard	k1gMnSc1	Richard
Hery	Hera	k1gFnSc2	Hera
Lee	Lea	k1gFnSc3	Lea
z	z	k7c2	z
Virgine	Virgin	k1gInSc5	Virgin
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
toho	ten	k3xDgMnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Návrh	návrh	k1gInSc4	návrh
a	a	k8xC	a
přijetí	přijetí	k1gNnSc4	přijetí
===	===	k?	===
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1776	[number]	k4	1776
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
komise	komise	k1gFnSc1	komise
Kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
Výbor	výbor	k1gInSc1	výbor
pěti	pět	k4xCc2	pět
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
členů	člen	k1gInPc2	člen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
z	z	k7c2	z
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklina	k1gFnPc2	Franklina
z	z	k7c2	z
Pensylvánie	Pensylvánie	k1gFnSc2	Pensylvánie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
)	)	kIx)	)
Kongresu	kongres	k1gInSc2	kongres
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
z	z	k7c2	z
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
R.	R.	kA	R.
Livingston	Livingston	k1gInSc1	Livingston
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Sherman	Sherman	k1gMnSc1	Sherman
z	z	k7c2	z
Connecticutu	Connecticut	k1gInSc2	Connecticut
<g/>
,	,	kIx,	,
<g/>
Komise	komise	k1gFnSc1	komise
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
navrhnout	navrhnout	k5eAaPmF	navrhnout
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
Deklaraci	deklarace	k1gFnSc4	deklarace
zastřešující	zastřešující	k2eAgFnSc4d1	zastřešující
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
rezoluci	rezoluce	k1gFnSc4	rezoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jefferson	Jefferson	k1gMnSc1	Jefferson
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
o	o	k7c4	o
převážnou	převážný	k2eAgFnSc4d1	převážná
většinu	většina	k1gFnSc4	většina
sepsání	sepsání	k1gNnSc2	sepsání
deklarace	deklarace	k1gFnSc2	deklarace
<g/>
,	,	kIx,	,
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
této	tento	k3xDgFnSc2	tento
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
přednesen	přednést	k5eAaPmNgInS	přednést
na	na	k7c6	na
Kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
kongresu	kongres	k1gInSc6	kongres
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úplná	úplný	k2eAgFnSc1d1	úplná
deklarace	deklarace	k1gFnSc1	deklarace
byla	být	k5eAaImAgFnS	být
přepsána	přepsat	k5eAaPmNgFnS	přepsat
na	na	k7c6	na
generálním	generální	k2eAgNnSc6d1	generální
zasedání	zasedání	k1gNnSc6	zasedání
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
přijetí	přijetí	k1gNnSc1	přijetí
Kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
kongresem	kongres	k1gInSc7	kongres
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
ve	v	k7c6	v
vládním	vládní	k2eAgInSc6d1	vládní
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Pensylvanii	Pensylvanie	k1gFnSc6	Pensylvanie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kopie	kopie	k1gFnSc1	kopie
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
pouze	pouze	k6eAd1	pouze
prezidentem	prezident	k1gMnSc7	prezident
kongresu	kongres	k1gInSc2	kongres
Johnem	John	k1gMnSc7	John
Hancockem	Hancocko	k1gNnSc7	Hancocko
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
sekretářem	sekretář	k1gMnSc7	sekretář
Charlesem	Charles	k1gMnSc7	Charles
Thomsonem	Thomson	k1gMnSc7	Thomson
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
ceremonie	ceremonie	k1gFnSc1	ceremonie
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
odehrála	odehrát	k5eAaPmAgFnS	odehrát
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Distribuce	distribuce	k1gFnSc1	distribuce
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
deklarace	deklarace	k1gFnSc1	deklarace
kongresem	kongres	k1gInSc7	kongres
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
kopie	kopie	k1gFnSc1	kopie
odeslána	odeslat	k5eAaPmNgFnS	odeslat
o	o	k7c4	o
pár	pár	k4xCyI	pár
bloků	blok	k1gInPc2	blok
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
tiskárny	tiskárna	k1gFnSc2	tiskárna
Johna	John	k1gMnSc2	John
Dunlapa	Dunlap	k1gMnSc2	Dunlap
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
noci	noc	k1gFnSc2	noc
bylo	být	k5eAaImAgNnS	být
vytištěno	vytisknout	k5eAaPmNgNnS	vytisknout
přes	přes	k7c4	přes
150	[number]	k4	150
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Georgi	Georg	k1gMnSc3	Georg
Washingtonovi	Washington	k1gMnSc3	Washington
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
deklaraci	deklarace	k1gFnSc4	deklarace
přečetl	přečíst	k5eAaPmAgMnS	přečíst
svým	svůj	k3xOyFgFnPc3	svůj
jednotkám	jednotka	k1gFnPc3	jednotka
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
25	[number]	k4	25
stále	stále	k6eAd1	stále
existujících	existující	k2eAgInPc2d1	existující
Dunlapových	Dunlapův	k2eAgInPc2d1	Dunlapův
výtisků	výtisk	k1gInPc2	výtisk
jsou	být	k5eAaImIp3nP	být
nejstarší	starý	k2eAgInPc1d3	nejstarší
exempláře	exemplář	k1gInPc1	exemplář
tohoto	tento	k3xDgInSc2	tento
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1777	[number]	k4	1777
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
že	že	k8xS	že
deklarace	deklarace	k1gFnSc1	deklarace
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
šířeji	šířej	k1gInSc6	šířej
distribuována	distribuovat	k5eAaBmNgFnS	distribuovat
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
tisk	tisk	k1gInSc4	tisk
zajistila	zajistit	k5eAaPmAgFnS	zajistit
Mary	Mary	k1gFnSc1	Mary
Kathrine	Kathrin	k1gInSc5	Kathrin
Goddardová	Goddardová	k1gFnSc1	Goddardová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
tisk	tisk	k1gInSc1	tisk
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
podpisy	podpis	k1gInPc1	podpis
pouze	pouze	k6eAd1	pouze
Johna	John	k1gMnSc4	John
Hancocka	Hancocko	k1gNnSc2	Hancocko
a	a	k8xC	a
Charlese	Charles	k1gMnSc2	Charles
Thomsona	Thomson	k1gMnSc2	Thomson
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
tisk	tisk	k1gInSc1	tisk
už	už	k6eAd1	už
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
všechny	všechen	k3xTgMnPc4	všechen
signatáře	signatář	k1gMnPc4	signatář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
deklaraci	deklarace	k1gFnSc6	deklarace
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1776	[number]	k4	1776
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Signatáři	signatář	k1gMnPc1	signatář
===	===	k?	===
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
Kongres	kongres	k1gInSc1	kongres
nařídil	nařídit	k5eAaPmAgInS	nařídit
ručně	ručně	k6eAd1	ručně
napsat	napsat	k5eAaPmF	napsat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
pak	pak	k6eAd1	pak
měli	mít	k5eAaImAgMnP	mít
delegáti	delegát	k1gMnPc1	delegát
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
kopii	kopie	k1gFnSc4	kopie
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Timothy	Timotha	k1gFnSc2	Timotha
Matlack	Matlack	k1gInSc1	Matlack
<g/>
,	,	kIx,	,
asistent	asistent	k1gMnSc1	asistent
v	v	k7c6	v
sekretariátu	sekretariát	k1gInSc6	sekretariát
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vyslanců	vyslanec	k1gMnPc2	vyslanec
podepsala	podepsat	k5eAaPmAgFnS	podepsat
deklaraci	deklarace	k1gFnSc4	deklarace
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1776	[number]	k4	1776
v	v	k7c6	v
zeměpisném	zeměpisný	k2eAgNnSc6d1	zeměpisné
seřazení	seřazení	k1gNnSc6	seřazení
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
<g/>
,	,	kIx,	,
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vyslanci	vyslanec	k1gMnPc1	vyslanec
nebyli	být	k5eNaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
deklaraci	deklarace	k1gFnSc3	deklarace
podepsat	podepsat	k5eAaPmF	podepsat
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
vyslanci	vyslanec	k1gMnPc1	vyslanec
ji	on	k3xPp3gFnSc4	on
dokonce	dokonce	k9	dokonce
nikdy	nikdy	k6eAd1	nikdy
nepodepsali	podepsat	k5eNaPmAgMnP	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jak	jak	k6eAd1	jak
noví	nový	k2eAgMnPc1d1	nový
vyslanci	vyslanec	k1gMnPc1	vyslanec
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
do	do	k7c2	do
členstva	členstvo	k1gNnSc2	členstvo
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
umožněno	umožněn	k2eAgNnSc1d1	umožněno
deklaraci	deklarace	k1gFnSc4	deklarace
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Deklaraci	deklarace	k1gFnSc4	deklarace
tak	tak	k6eAd1	tak
nakonec	nakonec	k6eAd1	nakonec
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
56	[number]	k4	56
vyslanců	vyslanec	k1gMnPc2	vyslanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
a	a	k8xC	a
také	také	k9	také
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
podpis	podpis	k1gInSc4	podpis
pod	pod	k7c7	pod
Deklarací	deklarace	k1gFnSc7	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
patří	patřit	k5eAaImIp3nS	patřit
Johnu	John	k1gMnSc3	John
Hancockovi	Hancocek	k1gMnSc3	Hancocek
<g/>
,	,	kIx,	,
presidentu	president	k1gMnSc3	president
Kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
budoucí	budoucí	k2eAgMnPc1d1	budoucí
presidenti	president	k1gMnPc1	president
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
a	a	k8xC	a
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
byli	být	k5eAaImAgMnP	být
také	také	k9	také
mezi	mezi	k7c7	mezi
signatáři	signatář	k1gMnPc7	signatář
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
výčet	výčet	k1gInSc4	výčet
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
jejích	její	k3xOp3gMnPc2	její
vyslanců	vyslanec	k1gMnPc2	vyslanec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
New	New	k?	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
–	–	k?	–
Josiah	Josiah	k1gMnSc1	Josiah
Bartlett	Bartlett	k1gMnSc1	Bartlett
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Whipple	Whipple	k1gFnSc1	Whipple
<g/>
,	,	kIx,	,
Matthew	Matthew	k1gFnSc1	Matthew
Thornton	Thornton	k1gInSc1	Thornton
</s>
</p>
<p>
<s>
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
–	–	k?	–
Samuel	Samuel	k1gMnSc1	Samuel
Adams	Adams	k1gInSc1	Adams
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Adams	Adams	k1gInSc1	Adams
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Hancock	Hancock	k1gMnSc1	Hancock
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Treat	Treat	k1gInSc1	Treat
Paine	Pain	k1gInSc5	Pain
<g/>
,	,	kIx,	,
Elbridge	Elbridg	k1gMnSc2	Elbridg
Gerry	Gerra	k1gMnSc2	Gerra
</s>
</p>
<p>
<s>
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
–	–	k?	–
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hopkins	Hopkins	k1gInSc1	Hopkins
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Ellery	Ellera	k1gFnSc2	Ellera
</s>
</p>
<p>
<s>
Connecticut	Connecticut	k1gMnSc1	Connecticut
–	–	k?	–
Roger	Roger	k1gMnSc1	Roger
Sherman	Sherman	k1gMnSc1	Sherman
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Huntington	Huntington	k1gInSc1	Huntington
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
Oliver	Oliver	k1gMnSc1	Oliver
Wolcott	Wolcott	k1gMnSc1	Wolcott
</s>
</p>
<p>
<s>
New	New	k?	New
York	York	k1gInSc1	York
–	–	k?	–
William	William	k1gInSc1	William
Floyd	Floyd	k1gInSc1	Floyd
<g/>
,	,	kIx,	,
Philip	Philip	k1gInSc1	Philip
Livingston	Livingston	k1gInSc1	Livingston
<g/>
,	,	kIx,	,
Francis	Francis	k1gFnSc1	Francis
Lewis	Lewis	k1gFnSc1	Lewis
<g/>
,	,	kIx,	,
Lewis	Lewis	k1gFnSc1	Lewis
Morris	Morris	k1gFnSc1	Morris
</s>
</p>
<p>
<s>
New	New	k?	New
Jersey	Jersea	k1gFnPc4	Jersea
–	–	k?	–
Richard	Richard	k1gMnSc1	Richard
Stockton	Stockton	k1gInSc1	Stockton
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Witherspoon	Witherspoon	k1gMnSc1	Witherspoon
<g/>
,	,	kIx,	,
Francis	Francis	k1gFnSc1	Francis
Hopkinson	Hopkinson	k1gMnSc1	Hopkinson
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Hart	Hart	k1gMnSc1	Hart
<g/>
,	,	kIx,	,
Abraham	Abraham	k1gMnSc1	Abraham
Clark	Clark	k1gInSc1	Clark
</s>
</p>
<p>
<s>
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Morris	Morris	k1gFnSc2	Morris
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Rush	Rush	k1gMnSc1	Rush
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklin	k1gInSc1	Franklin
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Morton	Morton	k1gInSc1	Morton
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Clymer	Clymer	k1gMnSc1	Clymer
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Ross	Rossa	k1gFnPc2	Rossa
</s>
</p>
<p>
<s>
Delaware	Delawar	k1gMnSc5	Delawar
–	–	k?	–
Caesar	Caesar	k1gMnSc1	Caesar
Rodney	Rodnea	k1gFnSc2	Rodnea
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Read	Read	k1gMnSc1	Read
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
McKean	McKean	k1gMnSc1	McKean
</s>
</p>
<p>
<s>
Maryland	Maryland	k1gInSc1	Maryland
–	–	k?	–
Samuel	Samuel	k1gMnSc1	Samuel
Chase	chasa	k1gFnSc3	chasa
<g/>
,	,	kIx,	,
William	William	k1gInSc4	William
Paca	Pac	k1gInSc2	Pac
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Carroll	Carroll	k1gMnSc1	Carroll
of	of	k?	of
Carrollton	Carrollton	k1gInSc1	Carrollton
</s>
</p>
<p>
<s>
Virginia	Virginium	k1gNnPc1	Virginium
–	–	k?	–
George	Georg	k1gMnSc2	Georg
Wythe	Wyth	k1gMnSc2	Wyth
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Henry	Henry	k1gMnSc1	Henry
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Nelson	Nelson	k1gMnSc1	Nelson
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Francis	Francis	k1gInSc1	Francis
Lightfoot	Lightfoot	k1gInSc1	Lightfoot
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Carter	Carter	k1gInSc1	Carter
Braxton	Braxton	k1gInSc1	Braxton
</s>
</p>
<p>
<s>
North	North	k1gMnSc1	North
Carolina	Carolin	k2eAgNnSc2d1	Carolino
–	–	k?	–
William	William	k1gInSc1	William
Hooper	Hooper	k1gInSc1	Hooper
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Hewes	Hewes	k1gMnSc1	Hewes
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Penn	Penn	k1gMnSc1	Penn
</s>
</p>
<p>
<s>
South	South	k1gInSc1	South
Carolina	Carolina	k1gFnSc1	Carolina
–	–	k?	–
Edward	Edward	k1gMnSc1	Edward
Rutledge	Rutledg	k1gInSc2	Rutledg
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Heyward	Heyward	k1gMnSc1	Heyward
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Lynch	Lynch	k1gMnSc1	Lynch
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Middleton	Middleton	k1gInSc1	Middleton
</s>
</p>
<p>
<s>
Georgia	Georgia	k1gFnSc1	Georgia
–	–	k?	–
Button	Button	k1gInSc1	Button
Gwinnett	Gwinnett	k1gInSc1	Gwinnett
<g/>
,	,	kIx,	,
Lyman	Lyman	k1gMnSc1	Lyman
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
George	George	k1gInSc1	George
WaltonZ	WaltonZ	k1gFnSc2	WaltonZ
těchto	tento	k3xDgMnPc2	tento
56	[number]	k4	56
signatářů	signatář	k1gMnPc2	signatář
bylo	být	k5eAaImAgNnS	být
24	[number]	k4	24
právníků	právník	k1gMnPc2	právník
či	či	k8xC	či
advokátů	advokát	k1gMnPc2	advokát
<g/>
,	,	kIx,	,
11	[number]	k4	11
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
9	[number]	k4	9
farmářů	farmář	k1gMnPc2	farmář
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
Rutledge	Rutledg	k1gFnSc2	Rutledg
(	(	kIx(	(
<g/>
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
signatářem	signatář	k1gMnSc7	signatář
a	a	k8xC	a
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklina	k1gFnPc2	Franklina
(	(	kIx(	(
<g/>
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
naopak	naopak	k6eAd1	naopak
nejstarším	starý	k2eAgInSc7d3	nejstarší
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
svým	svůj	k3xOyFgInSc7	svůj
podpisem	podpis	k1gInSc7	podpis
se	se	k3xPyFc4	se
vystavují	vystavovat	k5eAaImIp3nP	vystavovat
nelibosti	nelibost	k1gFnSc3	nelibost
a	a	k8xC	a
možné	možný	k2eAgFnSc3d1	možná
odplatě	odplata	k1gFnSc3	odplata
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
mohla	moct	k5eAaImAgFnS	moct
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
dopadnout	dopadnout	k5eAaPmF	dopadnout
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
chyceno	chytit	k5eAaPmNgNnS	chytit
a	a	k8xC	a
mučeno	mučit	k5eAaImNgNnS	mučit
Brity	Brit	k1gMnPc4	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvanácti	dvanáct	k4xCc6	dvanáct
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
odplata	odplata	k1gFnSc1	odplata
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
žhářství	žhářství	k1gNnSc6	žhářství
spáchaném	spáchaný	k2eAgNnSc6d1	spáchané
na	na	k7c6	na
jejich	jejich	k3xOp3gInPc6	jejich
domech	dům	k1gInPc6	dům
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
ztratili	ztratit	k5eAaPmAgMnP	ztratit
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
v	v	k7c6	v
revoluční	revoluční	k2eAgFnSc6d1	revoluční
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
jednomu	jeden	k4xCgNnSc3	jeden
zajali	zajmout	k5eAaPmAgMnP	zajmout
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
z	z	k7c2	z
podepsaných	podepsaný	k1gMnPc2	podepsaný
následně	následně	k6eAd1	následně
bojovalo	bojovat	k5eAaImAgNnS	bojovat
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
padlo	padnout	k5eAaImAgNnS	padnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Text	text	k1gInSc1	text
Deklarace	deklarace	k1gFnSc2	deklarace
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
lidských	lidský	k2eAgFnPc2d1	lidská
událostí	událost	k1gFnPc2	událost
nastane	nastat	k5eAaPmIp3nS	nastat
některému	některý	k3yIgInSc3	některý
národu	národ	k1gInSc3	národ
nutnost	nutnost	k1gFnSc4	nutnost
zrušit	zrušit	k5eAaPmF	zrušit
politické	politický	k2eAgInPc4d1	politický
závazky	závazek	k1gInPc4	závazek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jej	on	k3xPp3gInSc4	on
poutají	poutat	k5eAaImIp3nP	poutat
s	s	k7c7	s
národem	národ	k1gInSc7	národ
jiným	jiný	k2eAgInSc7d1	jiný
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaujmout	zaujmout	k5eAaPmF	zaujmout
mezi	mezi	k7c7	mezi
mocnostmi	mocnost	k1gFnPc7	mocnost
světa	svět	k1gInSc2	svět
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
a	a	k8xC	a
rovné	rovný	k2eAgNnSc4d1	rovné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgMnSc3	jenž
jej	on	k3xPp3gNnSc4	on
opravňují	opravňovat	k5eAaImIp3nP	opravňovat
zákony	zákon	k1gInPc1	zákon
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
si	se	k3xPyFc3	se
náležitá	náležitý	k2eAgFnSc1d1	náležitá
úcta	úcta	k1gFnSc1	úcta
k	k	k7c3	k
mínění	mínění	k1gNnSc3	mínění
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
takový	takový	k3xDgInSc1	takový
národ	národ	k1gInSc1	národ
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
důvody	důvod	k1gInPc4	důvod
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jej	on	k3xPp3gInSc4	on
k	k	k7c3	k
odtržení	odtržení	k1gNnSc3	odtržení
vedou	vést	k5eAaImIp3nP	vést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokládáme	pokládat	k5eAaImIp1nP	pokládat
za	za	k7c4	za
samozřejmé	samozřejmý	k2eAgFnPc4d1	samozřejmá
pravdy	pravda	k1gFnPc4	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
stvořeni	stvořit	k5eAaPmNgMnP	stvořit
sobě	se	k3xPyFc3	se
rovni	roven	k2eAgMnPc1d1	roven
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
obdařeni	obdařen	k2eAgMnPc1d1	obdařen
svým	svůj	k3xOyFgInSc7	svůj
Stvořitelem	Stvořitel	k1gMnSc7	Stvořitel
určitými	určitý	k2eAgFnPc7d1	určitá
nezcizitelnými	zcizitelný	k2eNgNnPc7d1	nezcizitelné
právy	právo	k1gNnPc7	právo
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
tato	tento	k3xDgNnPc4	tento
práva	právo	k1gNnPc4	právo
náleží	náležet	k5eAaImIp3nS	náležet
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
sledování	sledování	k1gNnSc1	sledování
osobního	osobní	k2eAgNnSc2d1	osobní
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
těchto	tento	k3xDgNnPc2	tento
práv	právo	k1gNnPc2	právo
se	se	k3xPyFc4	se
ustanovují	ustanovovat	k5eAaImIp3nP	ustanovovat
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
odvozující	odvozující	k2eAgNnSc1d1	odvozující
svoje	své	k1gNnSc1	své
oprávněné	oprávněný	k2eAgFnSc2d1	oprávněná
pravomoci	pravomoc	k1gFnSc2	pravomoc
ze	z	k7c2	z
souhlasu	souhlas	k1gInSc2	souhlas
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
vládnou	vládnout	k5eAaImIp3nP	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
kdykoliv	kdykoliv	k6eAd1	kdykoliv
počne	počnout	k5eAaPmIp3nS	počnout
být	být	k5eAaImF	být
některá	některý	k3yIgFnSc1	některý
vláda	vláda	k1gFnSc1	vláda
těmto	tento	k3xDgFnPc3	tento
cílům	cíl	k1gInPc3	cíl
na	na	k7c4	na
překážku	překážka	k1gFnSc4	překážka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
lid	lid	k1gInSc4	lid
právo	práv	k2eAgNnSc1d1	právo
ji	on	k3xPp3gFnSc4	on
změnit	změnit	k5eAaPmF	změnit
nebo	nebo	k8xC	nebo
zrušit	zrušit	k5eAaPmF	zrušit
a	a	k8xC	a
ustanovit	ustanovit	k5eAaPmF	ustanovit
vládu	vláda	k1gFnSc4	vláda
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
takových	takový	k3xDgFnPc6	takový
zásadách	zásada	k1gFnPc6	zásada
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
pravomoc	pravomoc	k1gFnSc4	pravomoc
upravenou	upravený	k2eAgFnSc4d1	upravená
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
uzná	uznat	k5eAaPmIp3nS	uznat
lid	lid	k1gInSc1	lid
za	za	k7c4	za
nejvhodnější	vhodný	k2eAgInSc4d3	nejvhodnější
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
své	svůj	k3xOyFgFnSc2	svůj
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
svého	svůj	k3xOyFgNnSc2	svůj
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvážnost	rozvážnost	k1gFnSc1	rozvážnost
sice	sice	k8xC	sice
přikazuje	přikazovat	k5eAaImIp3nS	přikazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dlouho	dlouho	k6eAd1	dlouho
ustavené	ustavený	k2eAgFnPc1d1	ustavená
vlády	vláda	k1gFnPc1	vláda
nebyly	být	k5eNaImAgFnP	být
měněny	měnit	k5eAaImNgFnP	měnit
pro	pro	k7c4	pro
nejasné	jasný	k2eNgInPc4d1	nejasný
a	a	k8xC	a
prchavé	prchavý	k2eAgInPc4d1	prchavý
důvody	důvod	k1gInPc4	důvod
<g/>
;	;	kIx,	;
a	a	k8xC	a
souhlasně	souhlasně	k6eAd1	souhlasně
veškerá	veškerý	k3xTgFnSc1	veškerý
zkušenost	zkušenost	k1gFnSc1	zkušenost
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidstvo	lidstvo	k1gNnSc1	lidstvo
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
náchylnější	náchylný	k2eAgFnSc1d2	náchylnější
ke	k	k7c3	k
strádání	strádání	k1gNnSc3	strádání
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
zlo	zlo	k1gNnSc1	zlo
snesitelné	snesitelný	k2eAgNnSc1d1	snesitelné
<g/>
,	,	kIx,	,
než	než	k8xS	než
k	k	k7c3	k
narovnání	narovnání	k1gNnSc3	narovnání
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
zavržením	zavržení	k1gNnSc7	zavržení
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
přivyklé	přivyklý	k2eAgNnSc1d1	přivyklé
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
ale	ale	k8xC	ale
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
sled	sled	k1gInSc1	sled
zlořádu	zlořád	k1gInSc2	zlořád
a	a	k8xC	a
uchvacování	uchvacování	k1gNnSc2	uchvacování
<g/>
,	,	kIx,	,
pronásledující	pronásledující	k2eAgInPc4d1	pronásledující
trvale	trvale	k6eAd1	trvale
týž	týž	k3xTgInSc4	týž
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
podřízenosti	podřízenost	k1gFnPc4	podřízenost
naprosté	naprostý	k2eAgFnPc4d1	naprostá
despocii	despocie	k1gFnSc4	despocie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
právem	právem	k6eAd1	právem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc7	jejich
povinností	povinnost	k1gFnSc7	povinnost
<g/>
,	,	kIx,	,
svrhnout	svrhnout	k5eAaPmF	svrhnout
takovou	takový	k3xDgFnSc4	takový
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
obstarat	obstarat	k5eAaPmF	obstarat
nové	nový	k2eAgFnPc4d1	nová
stráže	stráž	k1gFnPc4	stráž
své	svůj	k3xOyFgFnSc2	svůj
budoucí	budoucí	k2eAgFnSc2d1	budoucí
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Takové	takový	k3xDgNnSc1	takový
bylo	být	k5eAaImAgNnS	být
trpělivé	trpělivý	k2eAgNnSc1d1	trpělivé
strádání	strádání	k1gNnSc1	strádání
těchto	tento	k3xDgFnPc2	tento
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
taková	takový	k3xDgFnSc1	takový
je	být	k5eAaImIp3nS	být
i	i	k9	i
nyní	nyní	k6eAd1	nyní
nutnost	nutnost	k1gFnSc4	nutnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gInPc4	on
dohání	dohánět	k5eAaImIp3nS	dohánět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
změnily	změnit	k5eAaPmAgInP	změnit
své	svůj	k3xOyFgFnPc4	svůj
dřívější	dřívější	k2eAgFnSc4d1	dřívější
vládní	vládní	k2eAgNnSc4d1	vládní
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
nynějšího	nynější	k2eAgMnSc2d1	nynější
krále	král	k1gMnSc2	král
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
je	být	k5eAaImIp3nS	být
vládou	vláda	k1gFnSc7	vláda
opakovaných	opakovaný	k2eAgFnPc2d1	opakovaná
křivd	křivda	k1gFnPc2	křivda
a	a	k8xC	a
skutků	skutek	k1gInPc2	skutek
bezpráví	bezpráví	k1gNnSc2	bezpráví
<g/>
,	,	kIx,	,
jednoznačně	jednoznačně	k6eAd1	jednoznačně
směřujících	směřující	k2eAgFnPc6d1	směřující
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
naprostého	naprostý	k2eAgNnSc2d1	naprosté
násilí	násilí	k1gNnSc2	násilí
nad	nad	k7c7	nad
těmito	tento	k3xDgInPc7	tento
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
důkaz	důkaz	k1gInSc4	důkaz
toho	ten	k3xDgNnSc2	ten
nechť	nechť	k9	nechť
jsou	být	k5eAaImIp3nP	být
nezaujatému	zaujatý	k2eNgInSc3d1	nezaujatý
světu	svět	k1gInSc3	svět
předložená	předložený	k2eAgNnPc4d1	předložené
fakta	faktum	k1gNnPc4	faktum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odmítal	odmítat	k5eAaImAgMnS	odmítat
schválit	schválit	k5eAaPmF	schválit
nejprospěšnější	prospěšný	k2eAgInPc4d3	nejprospěšnější
a	a	k8xC	a
nejnutnější	nutný	k2eAgInPc4d3	nejnutnější
zákony	zákon	k1gInPc4	zákon
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
blaho	blaho	k1gNnSc4	blaho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zakázal	zakázat	k5eAaPmAgMnS	zakázat
svým	svůj	k3xOyFgMnPc3	svůj
guvernérům	guvernér	k1gMnPc3	guvernér
schvalovat	schvalovat	k5eAaImF	schvalovat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
a	a	k8xC	a
naléhavě	naléhavě	k6eAd1	naléhavě
důležité	důležitý	k2eAgInPc1d1	důležitý
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
že	že	k8xS	že
by	by	kYmCp3nP	by
nevstoupily	vstoupit	k5eNaPmAgFnP	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
je	on	k3xPp3gFnPc4	on
neschválí	schválit	k5eNaPmIp3nS	schválit
on	on	k3xPp3gMnSc1	on
<g/>
;	;	kIx,	;
a	a	k8xC	a
když	když	k8xS	když
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
zadrženy	zadržet	k5eAaPmNgFnP	zadržet
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
jim	on	k3xPp3gMnPc3	on
opominul	opominout	k5eAaPmAgMnS	opominout
věnovat	věnovat	k5eAaPmF	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odmítal	odmítat	k5eAaImAgMnS	odmítat
schválit	schválit	k5eAaPmF	schválit
zákony	zákon	k1gInPc4	zákon
k	k	k7c3	k
prospěchu	prospěch	k1gInSc3	prospěch
obyvatel	obyvatel	k1gMnPc2	obyvatel
velkých	velký	k2eAgFnPc2d1	velká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
zřekli	zřeknout	k5eAaPmAgMnP	zřeknout
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
zákonodárném	zákonodárný	k2eAgInSc6d1	zákonodárný
sboru	sbor	k1gInSc6	sbor
<g/>
,	,	kIx,	,
práva	právo	k1gNnPc4	právo
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
neocenitelného	ocenitelný	k2eNgNnSc2d1	neocenitelné
a	a	k8xC	a
hrůzu	hrůza	k1gFnSc4	hrůza
snad	snad	k9	snad
nahánějícího	nahánějící	k2eAgInSc2d1	nahánějící
jedině	jedině	k6eAd1	jedině
tyranům	tyran	k1gMnPc3	tyran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svolával	svolávat	k5eAaImAgInS	svolávat
zákonodárná	zákonodárný	k2eAgNnPc4d1	zákonodárné
shromáždění	shromáždění	k1gNnPc4	shromáždění
na	na	k7c4	na
neobvyklá	obvyklý	k2eNgNnPc4d1	neobvyklé
a	a	k8xC	a
nevyhovující	vyhovující	k2eNgNnPc4d1	nevyhovující
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
vzdálená	vzdálený	k2eAgNnPc4d1	vzdálené
jejich	jejich	k3xOp3gInPc2	jejich
archívů	archív	k1gInPc2	archív
<g/>
,	,	kIx,	,
jen	jen	k9	jen
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
tak	tak	k6eAd1	tak
únavou	únava	k1gFnSc7	únava
z	z	k7c2	z
všelijakých	všelijaký	k3yIgFnPc2	všelijaký
obtíží	obtíž	k1gFnPc2	obtíž
přinutil	přinutit	k5eAaPmAgInS	přinutit
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
svých	svůj	k3xOyFgNnPc2	svůj
vlastních	vlastní	k2eAgNnPc2d1	vlastní
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vytrvale	vytrvale	k6eAd1	vytrvale
rozpouštěl	rozpouštět	k5eAaImAgMnS	rozpouštět
sněmovny	sněmovna	k1gFnSc2	sněmovna
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
mužnou	mužný	k2eAgFnSc7d1	mužná
pevností	pevnost	k1gFnSc7	pevnost
stavěly	stavět	k5eAaImAgInP	stavět
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gInPc3	jeho
útokům	útok	k1gInPc3	útok
na	na	k7c6	na
práva	právo	k1gNnPc1	právo
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
takových	takový	k3xDgNnPc6	takový
rozpouštěních	rozpouštění	k1gNnPc6	rozpouštění
pak	pak	k6eAd1	pak
dlouho	dlouho	k6eAd1	dlouho
odmítal	odmítat	k5eAaImAgMnS	odmítat
umožnit	umožnit	k5eAaPmF	umožnit
zvolení	zvolení	k1gNnSc4	zvolení
sněmovny	sněmovna	k1gFnSc2	sněmovna
nové	nový	k2eAgFnSc2d1	nová
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nelze	lze	k6eNd1	lze
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
širokých	široký	k2eAgFnPc2d1	široká
vrstev	vrstva	k1gFnPc2	vrstva
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
všem	všecek	k3xTgInPc3	všecek
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
vpádu	vpád	k1gInSc2	vpád
zvenčí	zvenčí	k6eAd1	zvenčí
a	a	k8xC	a
zmatků	zmatek	k1gInPc2	zmatek
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
bránit	bránit	k5eAaImF	bránit
zalidnění	zalidnění	k1gNnSc4	zalidnění
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mařil	mařit	k5eAaImAgMnS	mařit
vydání	vydání	k1gNnSc4	vydání
zákonů	zákon	k1gInPc2	zákon
o	o	k7c4	o
nabývání	nabývání	k1gNnSc4	nabývání
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítal	odmítat	k5eAaImAgMnS	odmítat
schvalovat	schvalovat	k5eAaImF	schvalovat
zákony	zákon	k1gInPc4	zákon
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
jejich	jejich	k3xOp3gNnSc2	jejich
přistěhovalectví	přistěhovalectví	k1gNnSc2	přistěhovalectví
a	a	k8xC	a
znesnadňoval	znesnadňovat	k5eAaImAgMnS	znesnadňovat
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
nabývání	nabývání	k1gNnSc4	nabývání
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Překážel	překážet	k5eAaImAgMnS	překážet
vykonávání	vykonávání	k1gNnSc4	vykonávání
práva	právo	k1gNnSc2	právo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítal	odmítat	k5eAaImAgMnS	odmítat
schválit	schválit	k5eAaPmF	schválit
zákony	zákon	k1gInPc4	zákon
ustavující	ustavující	k2eAgFnSc2d1	ustavující
soudcovské	soudcovský	k2eAgFnSc2d1	soudcovská
pravomoce	pravomoc	k1gFnSc2	pravomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Učinil	učinit	k5eAaImAgMnS	učinit
soudce	soudce	k1gMnSc1	soudce
závislými	závislý	k2eAgFnPc7d1	závislá
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
jediné	jediný	k2eAgFnPc4d1	jediná
vůli	vůle	k1gFnSc4	vůle
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
přiděloval	přidělovat	k5eAaImAgMnS	přidělovat
do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
určoval	určovat	k5eAaImAgInS	určovat
výši	výše	k1gFnSc4	výše
i	i	k8xC	i
platbu	platba	k1gFnSc4	platba
jejich	jejich	k3xOp3gInPc2	jejich
platů	plat	k1gInPc2	plat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zřídil	zřídit	k5eAaPmAgMnS	zřídit
množství	množství	k1gNnSc4	množství
nových	nový	k2eAgInPc2d1	nový
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
vyslal	vyslat	k5eAaPmAgMnS	vyslat
sem	sem	k6eAd1	sem
hordu	horda	k1gFnSc4	horda
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sužovala	sužovat	k5eAaImAgFnS	sužovat
náš	náš	k3xOp1gInSc4	náš
lid	lid	k1gInSc4	lid
a	a	k8xC	a
vyjídala	vyjídat	k5eAaImAgFnS	vyjídat
ho	on	k3xPp3gMnSc4	on
z	z	k7c2	z
podstaty	podstata	k1gFnSc2	podstata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udržoval	udržovat	k5eAaImAgInS	udržovat
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
zahálející	zahálející	k2eAgFnSc2d1	zahálející
armády	armáda	k1gFnSc2	armáda
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
naší	náš	k3xOp1gFnSc2	náš
legislatury	legislatura	k1gFnSc2	legislatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přičinil	přičinit	k5eAaPmAgInS	přičinit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
na	na	k7c6	na
moci	moc	k1gFnSc6	moc
civilní	civilní	k2eAgInSc1d1	civilní
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
nadřazeno	nadřazen	k2eAgNnSc1d1	nadřazeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojil	spojit	k5eAaPmAgMnS	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nás	my	k3xPp1nPc4	my
podrobil	podrobit	k5eAaPmAgMnS	podrobit
právnímu	právní	k2eAgNnSc3d1	právní
zřízení	zřízení	k1gNnSc3	zřízení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
je	být	k5eAaImIp3nS	být
cizí	cizit	k5eAaImIp3nS	cizit
našemu	náš	k3xOp1gNnSc3	náš
založení	založení	k1gNnSc3	založení
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
našimi	náš	k3xOp1gInPc7	náš
zákony	zákon	k1gInPc7	zákon
<g/>
,	,	kIx,	,
dávaje	dávat	k5eAaImSgInS	dávat
svůj	svůj	k3xOyFgInSc4	svůj
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
činům	čin	k1gInPc3	čin
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
vzešlým	vzešlý	k2eAgMnSc7d1	vzešlý
z	z	k7c2	z
domnělé	domnělý	k2eAgFnSc2d1	domnělá
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
moci	moc	k1gFnSc2	moc
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
K	k	k7c3	k
usazení	usazení	k1gNnSc3	usazení
velkých	velký	k2eAgFnPc2d1	velká
jednotek	jednotka	k1gFnPc2	jednotka
ozbrojeného	ozbrojený	k2eAgNnSc2d1	ozbrojené
vojska	vojsko	k1gNnSc2	vojsko
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
ochraně	ochrana	k1gFnSc3	ochrana
falešnými	falešný	k2eAgInPc7d1	falešný
procesy	proces	k1gInPc7	proces
před	před	k7c7	před
potrestáním	potrestání	k1gNnSc7	potrestání
za	za	k7c4	za
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
vraždy	vražda	k1gFnPc4	vražda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
spáchaly	spáchat	k5eAaPmAgFnP	spáchat
na	na	k7c4	na
občanech	občan	k1gMnPc6	občan
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
zamezení	zamezení	k1gNnSc3	zamezení
našeho	náš	k3xOp1gInSc2	náš
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
částmi	část	k1gFnPc7	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
našemu	náš	k3xOp1gNnSc3	náš
zdanění	zdanění	k1gNnSc3	zdanění
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
bychom	by	kYmCp1nP	by
dali	dát	k5eAaPmAgMnP	dát
svůj	svůj	k3xOyFgInSc4	svůj
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nás	my	k3xPp1nPc4	my
zbavil	zbavit	k5eAaPmAgInS	zbavit
mnoha	mnoho	k4c2	mnoho
případů	případ	k1gInPc2	případ
výsad	výsada	k1gFnPc2	výsada
procesu	proces	k1gInSc2	proces
s	s	k7c7	s
porotou	porota	k1gFnSc7	porota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
byly	být	k5eAaImAgFnP	být
posláni	poslat	k5eAaPmNgMnP	poslat
za	za	k7c4	za
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
tam	tam	k6eAd1	tam
souzeni	soudit	k5eAaImNgMnP	soudit
za	za	k7c4	za
domnělá	domnělý	k2eAgNnPc4d1	domnělé
provinění	provinění	k1gNnPc4	provinění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
zákazu	zákaz	k1gInSc3	zákaz
svobodného	svobodný	k2eAgInSc2d1	svobodný
systému	systém	k1gInSc2	systém
anglických	anglický	k2eAgInPc2d1	anglický
zákonů	zákon	k1gInPc2	zákon
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
provincii	provincie	k1gFnSc6	provincie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ustavil	ustavit	k5eAaPmAgMnS	ustavit
svémocnou	svémocný	k2eAgFnSc4d1	svémocný
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
precedent	precedent	k1gInSc1	precedent
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vhodný	vhodný	k2eAgInSc4d1	vhodný
nástroj	nástroj	k1gInSc4	nástroj
k	k	k7c3	k
uvedení	uvedení	k1gNnSc3	uvedení
téže	tenže	k3xDgFnSc2	tenže
absolutní	absolutní	k2eAgFnSc2d1	absolutní
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
činnosti	činnost	k1gFnSc2	činnost
našich	náš	k3xOp1gInPc2	náš
vlastních	vlastní	k2eAgInPc2d1	vlastní
zákonodárných	zákonodárný	k2eAgInPc2d1	zákonodárný
sborů	sbor	k1gInPc2	sbor
<g/>
,	,	kIx,	,
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
se	se	k3xPyFc4	se
za	za	k7c2	za
oprávněného	oprávněný	k2eAgInSc2d1	oprávněný
dávat	dávat	k5eAaImF	dávat
nám	my	k3xPp1nPc3	my
sám	sám	k3xTgInSc1	sám
jakékoliv	jakýkoliv	k3yIgInPc4	jakýkoliv
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzdal	vzdát	k5eAaPmAgInS	vzdát
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
vlády	vláda	k1gFnSc2	vláda
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
odepřel	odepřít	k5eAaPmAgMnS	odepřít
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nám	my	k3xPp1nPc3	my
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plenil	plenit	k5eAaImAgMnS	plenit
naše	náš	k3xOp1gNnSc4	náš
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
pustošil	pustošit	k5eAaImAgMnS	pustošit
naše	náš	k3xOp1gNnSc4	náš
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
pálil	pálit	k5eAaImAgMnS	pálit
naše	náš	k3xOp1gNnPc4	náš
města	město	k1gNnPc4	město
a	a	k8xC	a
připravoval	připravovat	k5eAaImAgMnS	připravovat
o	o	k7c4	o
život	život	k1gInSc4	život
naše	náš	k3xOp1gMnPc4	náš
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
přepravuje	přepravovat	k5eAaImIp3nS	přepravovat
velké	velký	k2eAgFnPc4d1	velká
armády	armáda	k1gFnPc4	armáda
cizích	cizí	k2eAgMnPc2d1	cizí
námezdníků	námezdník	k1gMnPc2	námezdník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
dílo	dílo	k1gNnSc1	dílo
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
zpustošení	zpustošení	k1gNnSc2	zpustošení
a	a	k8xC	a
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
započaté	započatý	k2eAgFnPc1d1	započatá
událostmi	událost	k1gFnPc7	událost
ukrutnosti	ukrutnost	k1gFnSc2	ukrutnost
a	a	k8xC	a
věrolomnosti	věrolomnost	k1gFnSc2	věrolomnost
stěží	stěží	k6eAd1	stěží
obdobnými	obdobný	k2eAgFnPc7d1	obdobná
i	i	k8xC	i
v	v	k7c6	v
nejbarbarštějších	barbarský	k2eAgNnPc6d3	barbarský
obdobích	období	k1gNnPc6	období
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
nehodnými	hodný	k2eNgFnPc7d1	nehodná
hlavy	hlava	k1gFnPc4	hlava
civilizovaného	civilizovaný	k2eAgInSc2d1	civilizovaný
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nutil	nutit	k5eAaImAgMnS	nutit
naše	náš	k3xOp1gMnPc4	náš
spoluobčany	spoluobčan	k1gMnPc4	spoluobčan
zajaté	zajatý	k2eAgMnPc4d1	zajatý
na	na	k7c6	na
širokém	široký	k2eAgNnSc6d1	široké
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
chopili	chopit	k5eAaPmAgMnP	chopit
zbraní	zbraň	k1gFnPc2	zbraň
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vlasti	vlast	k1gFnSc3	vlast
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
katany	katan	k1gMnPc4	katan
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
bratří	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sami	sám	k3xTgMnPc1	sám
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
jejich	jejich	k3xOp3gFnPc7	jejich
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podněcoval	podněcovat	k5eAaImAgMnS	podněcovat
mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
domácí	domácí	k2eAgFnSc7d1	domácí
povstání	povstání	k1gNnSc4	povstání
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
poslat	poslat	k5eAaPmF	poslat
na	na	k7c4	na
naše	náš	k3xOp1gMnPc4	náš
hraničáře	hraničář	k1gMnPc4	hraničář
kruté	krutý	k2eAgMnPc4d1	krutý
indiánské	indiánský	k2eAgMnPc4d1	indiánský
divochy	divoch	k1gMnPc4	divoch
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
známým	známý	k2eAgNnSc7d1	známé
pravidlem	pravidlo	k1gNnSc7	pravidlo
boje	boj	k1gInSc2	boj
je	být	k5eAaImIp3nS	být
pobíjení	pobíjení	k1gNnSc4	pobíjení
všech	všecek	k3xTgInPc2	všecek
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
stáří	stáří	k1gNnSc4	stáří
<g/>
,	,	kIx,	,
pohlaví	pohlaví	k1gNnSc4	pohlaví
a	a	k8xC	a
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
případů	případ	k1gInPc2	případ
útisku	útisk	k1gInSc2	útisk
jsme	být	k5eAaImIp1nP	být
co	co	k9	co
nejpokorněji	pokorně	k6eAd3	pokorně
prosili	prosít	k5eAaPmAgMnP	prosít
o	o	k7c4	o
nápravu	náprava	k1gFnSc4	náprava
<g/>
;	;	kIx,	;
v	v	k7c6	v
odpověď	odpověď	k1gFnSc1	odpověď
našim	náš	k3xOp1gFnPc3	náš
opakovaným	opakovaný	k2eAgFnPc3d1	opakovaná
prosbám	prosba	k1gFnPc3	prosba
přicházely	přicházet	k5eAaImAgFnP	přicházet
než	než	k8xS	než
opakované	opakovaný	k2eAgFnPc1d1	opakovaná
křivdy	křivda	k1gFnPc1	křivda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladař	vladař	k1gMnSc1	vladař
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
charakter	charakter	k1gInSc1	charakter
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
všemi	všecek	k3xTgInPc7	všecek
činy	čin	k1gInPc7	čin
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
označit	označit	k5eAaPmF	označit
tyran	tyran	k1gMnSc1	tyran
<g/>
,	,	kIx,	,
nehodí	hodit	k5eNaPmIp3nS	hodit
se	se	k3xPyFc4	se
za	za	k7c4	za
vládce	vládce	k1gMnSc4	vládce
svobodného	svobodný	k2eAgInSc2d1	svobodný
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k8xC	a
nespouštěli	spouštět	k5eNaImAgMnP	spouštět
jsme	být	k5eAaImIp1nP	být
ze	z	k7c2	z
zřetele	zřetel	k1gInSc2	zřetel
ani	ani	k8xC	ani
své	svůj	k3xOyFgMnPc4	svůj
britské	britský	k2eAgMnPc4d1	britský
bratry	bratr	k1gMnPc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
jsme	být	k5eAaImIp1nP	být
je	on	k3xPp3gMnPc4	on
upozorňovali	upozorňovat	k5eAaImAgMnP	upozorňovat
na	na	k7c4	na
pokusy	pokus	k1gInPc4	pokus
jejich	jejich	k3xOp3gNnSc2	jejich
zákonodárství	zákonodárství	k1gNnSc2	zákonodárství
podrobit	podrobit	k5eAaPmF	podrobit
nás	my	k3xPp1nPc2	my
neospravedlnitelným	ospravedlnitelný	k2eNgNnSc7d1	neospravedlnitelné
soudním	soudní	k2eAgNnSc7d1	soudní
nařízením	nařízení	k1gNnSc7	nařízení
<g/>
.	.	kIx.	.
</s>
<s>
Připomínali	připomínat	k5eAaImAgMnP	připomínat
jsme	být	k5eAaImIp1nP	být
jim	on	k3xPp3gMnPc3	on
své	svůj	k3xOyFgFnPc4	svůj
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
vystěhovalectvím	vystěhovalectví	k1gNnSc7	vystěhovalectví
a	a	k8xC	a
zdejším	zdejší	k2eAgNnSc7d1	zdejší
usidlováním	usidlování	k1gNnSc7	usidlování
<g/>
.	.	kIx.	.
</s>
<s>
Dovolávali	dovolávat	k5eAaImAgMnP	dovolávat
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc2	jejich
vrozené	vrozený	k2eAgFnSc2d1	vrozená
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
velkomyslnosti	velkomyslnost	k1gFnSc2	velkomyslnost
a	a	k8xC	a
zapřísahali	zapřísahat	k5eAaImAgMnP	zapřísahat
jsme	být	k5eAaImIp1nP	být
je	on	k3xPp3gFnPc4	on
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
svazků	svazek	k1gInPc2	svazek
našeho	náš	k3xOp1gNnSc2	náš
příbuzenství	příbuzenství	k1gNnSc2	příbuzenství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
těmto	tento	k3xDgFnPc3	tento
násilnostem	násilnost	k1gFnPc3	násilnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
povedou	vést	k5eAaImIp3nP	vést
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
našeho	náš	k3xOp1gNnSc2	náš
spojení	spojení	k1gNnSc2	spojení
a	a	k8xC	a
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
oni	onen	k3xDgMnPc1	onen
však	však	k9	však
byl	být	k5eAaImAgInS	být
hluší	hluš	k1gFnSc7wB	hluš
k	k	k7c3	k
hlasu	hlas	k1gInSc3	hlas
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
pokrevenství	pokrevenství	k1gNnSc2	pokrevenství
<g/>
.	.	kIx.	.
</s>
<s>
Nemůžeme	moct	k5eNaImIp1nP	moct
tudíž	tudíž	k8xC	tudíž
než	než	k8xS	než
dát	dát	k5eAaPmF	dát
průchod	průchod	k1gInSc4	průchod
nezbytnosti	nezbytnost	k1gFnSc2	nezbytnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nás	my	k3xPp1nPc4	my
nutí	nutit	k5eAaImIp3nS	nutit
k	k	k7c3	k
odtržení	odtržení	k1gNnSc3	odtržení
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaujmout	zaujmout	k5eAaPmF	zaujmout
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
totéž	týž	k3xTgNnSc1	týž
postavení	postavení	k1gNnSc1	postavení
jako	jako	k8xS	jako
k	k	k7c3	k
ostatnímu	ostatní	k2eAgInSc3d1	ostatní
světu	svět	k1gInSc3	svět
<g/>
:	:	kIx,	:
nepřátelé	nepřítel	k1gMnPc1	nepřítel
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
míru	mír	k1gInSc6	mír
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pročež	pročež	k6eAd1	pročež
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
představitelé	představitel	k1gMnPc1	představitel
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
shromáždění	shromáždění	k1gNnSc3	shromáždění
na	na	k7c6	na
generálním	generální	k2eAgInSc6d1	generální
Kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
dovolávajíce	dovolávat	k5eAaImSgMnP	dovolávat
se	se	k3xPyFc4	se
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
soudce	soudce	k1gMnSc2	soudce
světa	svět	k1gInSc2	svět
o	o	k7c6	o
správnosti	správnost	k1gFnSc6	správnost
svých	svůj	k3xOyFgInPc2	svůj
úmyslů	úmysl	k1gInPc2	úmysl
<g/>
,	,	kIx,	,
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
a	a	k8xC	a
zmocněním	zmocnění	k1gNnPc3	zmocnění
poctivého	poctivý	k2eAgInSc2d1	poctivý
lidu	lid	k1gInSc2	lid
těchto	tento	k3xDgFnPc2	tento
kolonií	kolonie	k1gFnPc2	kolonie
slavnostně	slavnostně	k6eAd1	slavnostně
dáváme	dávat	k5eAaImIp1nP	dávat
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
a	a	k8xC	a
prohlašujeme	prohlašovat	k5eAaImIp1nP	prohlašovat
<g/>
:	:	kIx,	:
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
spojené	spojený	k2eAgFnPc1d1	spojená
kolonie	kolonie	k1gFnPc1	kolonie
jsou	být	k5eAaImIp3nP	být
a	a	k8xC	a
po	po	k7c6	po
právu	právo	k1gNnSc6	právo
<g />
.	.	kIx.	.
</s>
<s>
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
napříště	napříště	k6eAd1	napříště
tvořit	tvořit	k5eAaImF	tvořit
SVOBODNÉ	svobodný	k2eAgInPc4d1	svobodný
A	a	k8xC	a
NEZÁVISLÉ	závislý	k2eNgInPc4d1	nezávislý
STÁTY	stát	k1gInPc4	stát
<g/>
;	;	kIx,	;
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zproštěny	zproštěn	k2eAgFnPc1d1	zproštěna
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
poddanství	poddanství	k1gNnSc2	poddanství
vůči	vůči	k7c3	vůči
britské	britský	k2eAgFnSc3d1	britská
koruně	koruna	k1gFnSc3	koruna
a	a	k8xC	a
že	že	k8xS	že
veškeré	veškerý	k3xTgNnSc4	veškerý
politické	politický	k2eAgNnSc4d1	politické
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gNnPc7	on
a	a	k8xC	a
státem	stát	k1gInSc7	stát
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
naprosto	naprosto	k6eAd1	naprosto
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
;	;	kIx,	;
a	a	k8xC	a
že	že	k8xS	že
jakožto	jakožto	k8xS	jakožto
svobodné	svobodný	k2eAgInPc1d1	svobodný
a	a	k8xC	a
nezávislé	závislý	k2eNgInPc1d1	nezávislý
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
plnou	plný	k2eAgFnSc4d1	plná
moc	moc	k1gFnSc4	moc
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
uzavírat	uzavírat	k5eAaImF	uzavírat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
vstupovat	vstupovat	k5eAaImF	vstupovat
ve	v	k7c4	v
spojenectví	spojenectví	k1gNnSc4	spojenectví
<g/>
,	,	kIx,	,
navazovat	navazovat	k5eAaImF	navazovat
obchodní	obchodní	k2eAgInPc4d1	obchodní
styky	styk	k1gInPc4	styk
a	a	k8xC	a
vykonávat	vykonávat	k5eAaImF	vykonávat
všechny	všechen	k3xTgInPc4	všechen
jednání	jednání	k1gNnSc1	jednání
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nezávislé	závislý	k2eNgInPc1d1	nezávislý
státy	stát	k1gInPc1	stát
po	po	k7c6	po
právu	právo	k1gNnSc6	právo
činí	činit	k5eAaImIp3nS	činit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	k9	aby
pak	pak	k6eAd1	pak
toto	tento	k3xDgNnSc4	tento
prohlášení	prohlášení	k1gNnSc4	prohlášení
co	co	k9	co
nejpevněji	pevně	k6eAd3	pevně
bylo	být	k5eAaImAgNnS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
<g/>
,	,	kIx,	,
za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
důvěrou	důvěra	k1gFnSc7	důvěra
v	v	k7c4	v
ochranu	ochrana	k1gFnSc4	ochrana
Boží	boží	k2eAgFnSc2d1	boží
prozřetelnosti	prozřetelnost	k1gFnSc2	prozřetelnost
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
dáváme	dávat	k5eAaImIp1nP	dávat
v	v	k7c4	v
ochranu	ochrana	k1gFnSc4	ochrana
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
posvátnou	posvátný	k2eAgFnSc4d1	posvátná
nám	my	k3xPp1nPc3	my
čest	čest	k1gFnSc4	čest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Hancock	Hancock	k1gMnSc1	Hancock
</s>
</p>
<p>
<s>
a	a	k8xC	a
55	[number]	k4	55
dalších	další	k2eAgMnPc2d1	další
poslanců	poslanec	k1gMnPc2	poslanec
</s>
</p>
<p>
<s>
==	==	k?	==
Komentář	komentář	k1gInSc4	komentář
k	k	k7c3	k
textu	text	k1gInSc3	text
Deklarace	deklarace	k1gFnSc2	deklarace
==	==	k?	==
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
Deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
oddílů	oddíl	k1gInPc2	oddíl
<g/>
:	:	kIx,	:
úvod	úvod	k1gInSc1	úvod
<g/>
,	,	kIx,	,
preambule	preambule	k1gFnSc1	preambule
<g/>
,	,	kIx,	,
obvinění	obvinění	k1gNnSc1	obvinění
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
,	,	kIx,	,
nařčení	nařčení	k1gNnSc1	nařčení
britského	britský	k2eAgInSc2d1	britský
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
závěr	závěr	k1gInSc4	závěr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
tyto	tento	k3xDgInPc1	tento
nadpisy	nadpis	k1gInPc1	nadpis
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
dokumentu	dokument	k1gInSc2	dokument
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Úvod	úvod	k1gInSc1	úvod
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Preambule	preambule	k1gFnSc2	preambule
===	===	k?	===
</s>
</p>
<p>
<s>
Preambule	preambule	k1gFnSc1	preambule
je	být	k5eAaImIp3nS	být
podávána	podávat	k5eAaImNgFnS	podávat
jako	jako	k8xS	jako
logické	logický	k2eAgNnSc1d1	logické
předvedení	předvedení	k1gNnSc1	předvedení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jedno	jeden	k4xCgNnSc1	jeden
tvrzení	tvrzení	k1gNnSc1	tvrzení
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
stvořeni	stvořit	k5eAaPmNgMnP	stvořit
sobě	se	k3xPyFc3	se
rovnými	rovný	k2eAgFnPc7d1	rovná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
logickým	logický	k2eAgInSc7d1	logický
řetězcem	řetězec	k1gInSc7	řetězec
dostáváme	dostávat	k5eAaImIp1nP	dostávat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
stane	stanout	k5eAaPmIp3nS	stanout
zničujícím	zničující	k2eAgInSc7d1	zničující
elementem	element	k1gInSc7	element
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obvinění	obvinění	k1gNnSc4	obvinění
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Nařčení	nařčení	k1gNnSc6	nařčení
britského	britský	k2eAgInSc2d1	britský
lidu	lid	k1gInSc2	lid
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Závěr	závěr	k1gInSc1	závěr
===	===	k?	===
</s>
</p>
<p>
<s>
Signatáři	signatář	k1gMnPc1	signatář
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kolonie	kolonie	k1gFnSc1	kolonie
musí	muset	k5eAaImIp3nS	muset
nezbytně	zbytně	k6eNd1	zbytně
nutně	nutně	k6eAd1	nutně
oprostit	oprostit	k5eAaPmF	oprostit
od	od	k7c2	od
politických	politický	k2eAgNnPc2d1	politické
pojítek	pojítko	k1gNnPc2	pojítko
s	s	k7c7	s
Britskou	britský	k2eAgFnSc7d1	britská
korunou	koruna	k1gFnSc7	koruna
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
nezávislými	závislý	k2eNgInPc7d1	nezávislý
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
závěr	závěr	k1gInSc1	závěr
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc7	jeho
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
Leeovu	Leeův	k2eAgFnSc4d1	Leeova
resoluci	resoluce	k1gFnSc4	resoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
návrhem	návrh	k1gInSc7	návrh
a	a	k8xC	a
konečnou	konečný	k2eAgFnSc7d1	konečná
podobou	podoba	k1gFnSc7	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
Originální	originální	k2eAgInSc1d1	originální
návrh	návrh	k1gInSc1	návrh
Thomase	Thomas	k1gMnSc2	Thomas
Jeffersona	Jefferson	k1gMnSc2	Jefferson
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
napadení	napadení	k1gNnSc4	napadení
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
(	(	kIx(	(
<g/>
On	on	k3xPp3gMnSc1	on
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgInS	vést
krutou	krutý	k2eAgFnSc4d1	krutá
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
lidské	lidský	k2eAgFnSc3d1	lidská
přirozenosti	přirozenost	k1gFnSc3	přirozenost
samé	samý	k3xTgNnSc4	samý
<g/>
,	,	kIx,	,
porušiv	porušit	k5eAaPmDgInS	porušit
nejsvětější	nejsvětější	k2eAgNnSc4d1	nejsvětější
práva	právo	k1gNnPc4	právo
života	život	k1gInSc2	život
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
ztělesněné	ztělesněný	k2eAgInPc4d1	ztělesněný
v	v	k7c6	v
lidech	člověk	k1gMnPc6	člověk
z	z	k7c2	z
dálných	dálný	k2eAgInPc2d1	dálný
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
nikdy	nikdy	k6eAd1	nikdy
nenapadli	napadnout	k5eNaPmAgMnP	napadnout
<g/>
,	,	kIx,	,
zajímá	zajímat	k5eAaImIp3nS	zajímat
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
odvádí	odvádět	k5eAaImIp3nS	odvádět
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
polokouli	polokoule	k1gFnSc4	polokoule
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jim	on	k3xPp3gMnPc3	on
připravuje	připravovat	k5eAaImIp3nS	připravovat
bídnou	bídný	k2eAgFnSc4d1	bídná
smrt	smrt	k1gFnSc4	smrt
během	během	k7c2	během
jejich	jejich	k3xOp3gNnSc2	jejich
děsivého	děsivý	k2eAgNnSc2d1	děsivé
převážení	převážení	k1gNnSc2	převážení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
bylo	být	k5eAaImAgNnS	být
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
plno	plno	k1gNnSc4	plno
kritik	kritika	k1gFnPc2	kritika
britského	britský	k2eAgInSc2d1	britský
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Jeffersonův	Jeffersonův	k2eAgInSc1d1	Jeffersonův
plán	plán	k1gInSc1	plán
také	také	k9	také
používal	používat	k5eAaImAgInS	používat
frázi	fráze	k1gFnSc4	fráze
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
neodmyslitelná	odmyslitelný	k2eNgNnPc1d1	neodmyslitelné
a	a	k8xC	a
nezadatelná	zadatelný	k2eNgNnPc1d1	nezadatelné
práva	právo	k1gNnPc1	právo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
frází	fráze	k1gFnSc7	fráze
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
zaručená	zaručený	k2eAgNnPc4d1	zaručené
nezcizitelná	zcizitelný	k2eNgNnPc4d1	nezcizitelné
práva	právo	k1gNnPc4	právo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Analýza	analýza	k1gFnSc1	analýza
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historické	historický	k2eAgInPc1d1	historický
vlivy	vliv	k1gInPc1	vliv
===	===	k?	===
</s>
</p>
<p>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
Deklarací	deklarace	k1gFnSc7	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Holandské	holandský	k2eAgFnSc2d1	holandská
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1581	[number]	k4	1581
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Přísaha	přísaha	k1gFnSc1	přísaha
zřeknutí	zřeknutí	k1gNnSc2	zřeknutí
<g/>
.	.	kIx.	.
</s>
<s>
Arbroathská	Arbroathský	k2eAgFnSc1d1	Arbroathský
deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
(	(	kIx(	(
<g/>
1320	[number]	k4	1320
<g/>
)	)	kIx)	)
zas	zas	k6eAd1	zas
byla	být	k5eAaImAgFnS	být
deklarací	deklarace	k1gFnSc7	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Skotského	skotský	k2eAgNnSc2d1	skotské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
nepochybně	pochybně	k6eNd1	pochybně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
Deklaraci	deklarace	k1gFnSc4	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Jeffersonovi	Jefferson	k1gMnSc3	Jefferson
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
připisován	připisovat	k5eAaImNgInS	připisovat
návrh	návrh	k1gInSc1	návrh
Virginské	virginský	k2eAgFnSc2d1	virginská
deklarace	deklarace	k1gFnSc2	deklarace
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filosofické	filosofický	k2eAgNnSc4d1	filosofické
pozadí	pozadí	k1gNnSc4	pozadí
===	===	k?	===
</s>
</p>
<p>
<s>
Preambule	preambule	k1gFnSc1	preambule
Deklarace	deklarace	k1gFnSc1	deklarace
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
osvícenskou	osvícenský	k2eAgFnSc7d1	osvícenská
filosofií	filosofie	k1gFnSc7	filosofie
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pojmy	pojem	k1gInPc4	pojem
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
sebeurčení	sebeurčení	k1gNnSc2	sebeurčení
a	a	k8xC	a
také	také	k9	také
deismu	deismus	k1gInSc2	deismus
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
některé	některý	k3yIgFnPc1	některý
fráze	fráze	k1gFnPc1	fráze
byly	být	k5eAaImAgFnP	být
přejaty	přejmout	k5eAaPmNgFnP	přejmout
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
spisů	spis	k1gInPc2	spis
anglického	anglický	k2eAgMnSc2d1	anglický
filosofa	filosof	k1gMnSc2	filosof
Johna	John	k1gMnSc2	John
Locka	Locek	k1gMnSc2	Locek
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
druhého	druhý	k4xOgNnSc2	druhý
pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
nazvaného	nazvaný	k2eAgMnSc4d1	nazvaný
"	"	kIx"	"
<g/>
Eseje	esej	k1gFnPc4	esej
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
pravého	pravý	k2eAgInSc2d1	pravý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
rozměru	rozměr	k1gInSc2	rozměr
a	a	k8xC	a
konce	konec	k1gInSc2	konec
civilní	civilní	k2eAgFnSc2d1	civilní
vlády	vláda	k1gFnSc2	vláda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
pojednání	pojednání	k1gNnSc6	pojednání
se	se	k3xPyFc4	se
Locke	Locke	k1gInSc1	Locke
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
vlády	vláda	k1gFnSc2	vláda
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Locke	Locke	k6eAd1	Locke
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidské	lidský	k2eAgFnPc1d1	lidská
bytosti	bytost	k1gFnPc1	bytost
mají	mít	k5eAaImIp3nP	mít
zaručená	zaručený	k2eAgNnPc1d1	zaručené
přirozená	přirozený	k2eAgNnPc1d1	přirozené
práva	právo	k1gNnPc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
vlivy	vliv	k1gInPc7	vliv
jsou	být	k5eAaImIp3nP	být
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
sepsali	sepsat	k5eAaPmAgMnP	sepsat
Wawrzyniec	Wawrzyniec	k1gInSc4	Wawrzyniec
Grzymala	Grzymala	k1gFnSc2	Grzymala
Goslicki	Goslick	k1gFnSc2	Goslick
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Paine	Pain	k1gInSc5	Pain
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Jeffersona	Jefferson	k1gMnSc2	Jefferson
účel	účel	k1gInSc1	účel
Deklarace	deklarace	k1gFnSc2	deklarace
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
nehledat	hledat	k5eNaImF	hledat
nové	nový	k2eAgInPc4d1	nový
principy	princip	k1gInPc4	princip
nebo	nebo	k8xC	nebo
nové	nový	k2eAgInPc4d1	nový
argumenty	argument	k1gInPc4	argument
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgNnPc6	který
doposud	doposud	k6eAd1	doposud
nikdo	nikdo	k3yNnSc1	nikdo
nepřemýšlel	přemýšlet	k5eNaImAgMnS	přemýšlet
<g/>
...	...	k?	...
<g/>
ale	ale	k8xC	ale
postavit	postavit	k5eAaPmF	postavit
před	před	k7c4	před
lidstvo	lidstvo	k1gNnSc4	lidstvo
zdravý	zdravý	k2eAgInSc1d1	zdravý
rozum	rozum	k1gInSc1	rozum
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tak	tak	k6eAd1	tak
prosté	prostý	k2eAgNnSc1d1	prosté
a	a	k8xC	a
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bychom	by	kYmCp1nP	by
jim	on	k3xPp3gMnPc3	on
chtěli	chtít	k5eAaImAgMnP	chtít
rozkázat	rozkázat	k5eAaPmF	rozkázat
jejich	jejich	k3xOp3gInSc4	jejich
souhlas	souhlas	k1gInSc4	souhlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
obhájit	obhájit	k5eAaPmF	obhájit
nás	my	k3xPp1nPc4	my
samé	samý	k3xTgFnSc3	samý
v	v	k7c6	v
nezávislém	závislý	k2eNgNnSc6d1	nezávislé
postavení	postavení	k1gNnSc6	postavení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jsme	být	k5eAaImIp1nP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
si	se	k3xPyFc3	se
uzmout	uzmout	k5eAaPmF	uzmout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Praktické	praktický	k2eAgNnSc1d1	praktické
použití	použití	k1gNnSc1	použití
===	===	k?	===
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
historikové	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Deklarace	deklarace	k1gFnSc1	deklarace
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jak	jak	k8xS	jak
nástroj	nástroj	k1gInSc1	nástroj
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
Američané	Američan	k1gMnPc1	Američan
snažili	snažit	k5eAaImAgMnP	snažit
získat	získat	k5eAaPmF	získat
jasné	jasný	k2eAgInPc4d1	jasný
důvody	důvod	k1gInPc4	důvod
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnPc4	jejich
rebelství	rebelství	k1gNnPc4	rebelství
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
by	by	kYmCp3nS	by
mohli	moct	k5eAaImAgMnP	moct
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
váhavé	váhavý	k2eAgMnPc4d1	váhavý
kolonisty	kolonista	k1gMnPc4	kolonista
<g/>
,	,	kIx,	,
a	a	k8xC	a
jimiž	jenž	k3xRgInPc7	jenž
by	by	kYmCp3nS	by
mohli	moct	k5eAaImAgMnP	moct
získat	získat	k5eAaPmF	získat
důvod	důvod	k1gInSc4	důvod
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
vlády	vláda	k1gFnPc4	vláda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
jim	on	k3xPp3gMnPc3	on
mohly	moct	k5eAaImAgInP	moct
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
byla	být	k5eAaImAgFnS	být
také	také	k9	také
použita	použít	k5eAaPmNgFnS	použít
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
členů	člen	k1gInPc2	člen
Kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
se	se	k3xPyFc4	se
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podepsali	podepsat	k5eAaPmAgMnP	podepsat
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gInSc7	jejich
rozsudkem	rozsudek	k1gInSc7	rozsudek
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
revoluce	revoluce	k1gFnSc1	revoluce
nepodařila	podařit	k5eNaPmAgFnS	podařit
<g/>
,	,	kIx,	,
a	a	k8xC	a
Deklarace	deklarace	k1gFnSc1	deklarace
podávala	podávat	k5eAaImAgFnS	podávat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
možnost	možnost	k1gFnSc4	možnost
rychlého	rychlý	k2eAgNnSc2d1	rychlé
vítězství	vítězství	k1gNnSc2	vítězství
revoluce	revoluce	k1gFnSc2	revoluce
za	za	k7c4	za
nemožné	možný	k2eNgNnSc4d1	nemožné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ovlivnění	ovlivnění	k1gNnSc2	ovlivnění
dalších	další	k2eAgInPc2d1	další
dokumentů	dokument	k1gInPc2	dokument
===	===	k?	===
</s>
</p>
<p>
<s>
Markýz	markýz	k1gMnSc1	markýz
de	de	k?	de
La	la	k1gNnPc2	la
Fayette	Fayett	k1gInSc5	Fayett
se	se	k3xPyFc4	se
s	s	k7c7	s
výtiskem	výtisk	k1gInSc7	výtisk
této	tento	k3xDgFnSc2	tento
deklarace	deklarace	k1gFnSc2	deklarace
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vytvořit	vytvořit	k5eAaPmF	vytvořit
francouzské	francouzský	k2eAgNnSc4d1	francouzské
Prohlášení	prohlášení	k1gNnSc4	prohlášení
práv	právo	k1gNnPc2	právo
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
občana	občan	k1gMnSc2	občan
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
dnešních	dnešní	k2eAgFnPc2d1	dnešní
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c1	mnoho
základních	základní	k2eAgInPc2d1	základní
principů	princip	k1gInPc2	princip
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
otců	otec	k1gMnPc2	otec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
poté	poté	k6eAd1	poté
zapsány	zapsat	k5eAaPmNgInP	zapsat
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
modelem	model	k1gInSc7	model
pro	pro	k7c4	pro
Deklaraci	deklarace	k1gFnSc4	deklarace
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
sepsanou	sepsaný	k2eAgFnSc4d1	sepsaná
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
v	v	k7c6	v
Seneca	Seneca	k1gMnSc1	Seneca
Falls	Falls	k1gInSc1	Falls
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
první	první	k4xOgNnPc4	první
ženská	ženský	k2eAgNnPc4d1	ženské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xC	jako
předloha	předloha	k1gFnSc1	předloha
mnoha	mnoho	k4c2	mnoho
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
byla	být	k5eAaImAgFnS	být
Deklarace	deklarace	k1gFnSc1	deklarace
často	často	k6eAd1	často
citována	citován	k2eAgFnSc1d1	citována
v	v	k7c6	v
politických	politický	k2eAgInPc6d1	politický
projevech	projev	k1gInPc6	projev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
