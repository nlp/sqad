<s>
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1338	[number]	k4	1338
(	(	kIx(	(
<g/>
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
privilegia	privilegium	k1gNnSc2	privilegium
uděleného	udělený	k2eAgInSc2d1	udělený
staroměstským	staroměstský	k2eAgFnPc3d1	Staroměstská
měšťanům	měšťan	k1gMnPc3	měšťan
králem	král	k1gMnSc7	král
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
<g/>
.	.	kIx.	.
</s>
