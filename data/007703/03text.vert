<s>
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1338	[number]	k4	1338
(	(	kIx(	(
<g/>
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
privilegia	privilegium	k1gNnSc2	privilegium
uděleného	udělený	k2eAgInSc2d1	udělený
staroměstským	staroměstský	k2eAgFnPc3d1	Staroměstská
měšťanům	měšťan	k1gMnPc3	měšťan
králem	král	k1gMnSc7	král
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
komplex	komplex	k1gInSc1	komplex
několika	několik	k4yIc2	několik
domů	dům	k1gInPc2	dům
přiléhajících	přiléhající	k2eAgInPc2d1	přiléhající
ke	k	k7c3	k
Staroměstskému	staroměstský	k2eAgNnSc3d1	Staroměstské
náměstí	náměstí	k1gNnSc3	náměstí
postupně	postupně	k6eAd1	postupně
připojovaných	připojovaný	k2eAgFnPc2d1	připojovaná
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
celku	celek	k1gInSc2	celek
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
správy	správa	k1gFnSc2	správa
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgInSc7	první
domem	dům	k1gInSc7	dům
je	být	k5eAaImIp3nS	být
nárožní	nárožní	k2eAgInSc1d1	nárožní
raně	raně	k6eAd1	raně
gotický	gotický	k2eAgInSc1d1	gotický
dům	dům	k1gInSc1	dům
z	z	k7c2	z
konce	konec	k1gInSc2	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
měšťané	měšťan	k1gMnPc1	měšťan
získali	získat	k5eAaPmAgMnP	získat
z	z	k7c2	z
majetku	majetek	k1gInSc2	majetek
bohatého	bohatý	k2eAgMnSc4d1	bohatý
kupce	kupec	k1gMnSc4	kupec
Wolfina	Wolfin	k1gMnSc4	Wolfin
od	od	k7c2	od
Kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
Wolflinova	Wolflinův	k2eAgInSc2d1	Wolflinův
domu	dům	k1gInSc2	dům
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
hranolovitá	hranolovitý	k2eAgFnSc1d1	hranolovitá
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1364	[number]	k4	1364
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věži	věž	k1gFnSc6	věž
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
roku	rok	k1gInSc2	rok
1381	[number]	k4	1381
stavební	stavební	k2eAgFnSc1d1	stavební
huť	huť	k1gFnSc1	huť
Petra	Petr	k1gMnSc2	Petr
Parléře	Parléř	k1gMnSc2	Parléř
gotickou	gotický	k2eAgFnSc4d1	gotická
radniční	radniční	k2eAgFnSc4d1	radniční
kapli	kaple	k1gFnSc4	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stejná	stejný	k2eAgFnSc1d1	stejná
huť	huť	k1gFnSc1	huť
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
jižní	jižní	k2eAgFnSc3d1	jižní
stěně	stěna	k1gFnSc3	stěna
věže	věž	k1gFnSc2	věž
stavbu	stavba	k1gFnSc4	stavba
Staroměstského	staroměstský	k2eAgInSc2d1	staroměstský
orloje	orloj	k1gInSc2	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1360	[number]	k4	1360
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
prvního	první	k4xOgInSc2	první
domu	dům	k1gInSc2	dům
přistavěn	přistavěn	k2eAgInSc4d1	přistavěn
druhý	druhý	k4xOgInSc4	druhý
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
radní	radní	k2eAgFnSc1d1	radní
síň	síň	k1gFnSc1	síň
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
síň	síň	k1gFnSc1	síň
s	s	k7c7	s
dřevěným	dřevěný	k2eAgInSc7d1	dřevěný
gotickým	gotický	k2eAgInSc7d1	gotický
stropem	strop	k1gInSc7	strop
dodnes	dodnes	k6eAd1	dodnes
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xS	jako
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
místo	místo	k1gNnSc1	místo
svatebních	svatební	k2eAgInPc2d1	svatební
obřadů	obřad	k1gInPc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1458	[number]	k4	1458
se	se	k3xPyFc4	se
jižní	jižní	k2eAgNnSc1d1	jižní
křídlo	křídlo	k1gNnSc1	křídlo
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
Mikšův	Mikšův	k2eAgInSc1d1	Mikšův
vystavěný	vystavěný	k2eAgInSc1d1	vystavěný
na	na	k7c6	na
románských	románský	k2eAgInPc6d1	románský
základech	základ	k1gInPc6	základ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
radnici	radnice	k1gFnSc3	radnice
odkázala	odkázat	k5eAaPmAgFnS	odkázat
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
kožešníku	kožešník	k1gMnSc6	kožešník
Mikšovi	Mikš	k1gMnSc6	Mikš
Kačka	Kačka	k1gFnSc1	Kačka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1807	[number]	k4	1807
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
věže	věž	k1gFnSc2	věž
umístěny	umístěn	k2eAgFnPc4d1	umístěna
nové	nový	k2eAgFnPc4d1	nová
hodiny	hodina	k1gFnPc4	hodina
a	a	k8xC	a
přistavěn	přistavěn	k2eAgInSc4d1	přistavěn
ochoz	ochoz	k1gInSc4	ochoz
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
dům	dům	k1gInSc1	dům
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
U	u	k7c2	u
Kohouta	Kohout	k1gMnSc2	Kohout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zakoupen	zakoupit	k5eAaPmNgMnS	zakoupit
obcí	obec	k1gFnSc7	obec
a	a	k8xC	a
k	k	k7c3	k
radnici	radnice	k1gFnSc4	radnice
připojen	připojit	k5eAaPmNgInS	připojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
radnici	radnice	k1gFnSc3	radnice
připojeno	připojen	k2eAgNnSc1d1	připojeno
i	i	k8xC	i
východní	východní	k2eAgNnSc1d1	východní
křídlo	křídlo	k1gNnSc1	křídlo
sloužící	sloužící	k1gFnSc3	sloužící
radnici	radnice	k1gFnSc3	radnice
postupně	postupně	k6eAd1	postupně
již	již	k6eAd1	již
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
východního	východní	k2eAgNnSc2d1	východní
křídla	křídlo	k1gNnSc2	křídlo
však	však	k9	však
byly	být	k5eAaImAgInP	být
záhy	záhy	k6eAd1	záhy
zbourány	zbourán	k2eAgInPc1d1	zbourán
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
letech	let	k1gInPc6	let
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
vídeňští	vídeňský	k2eAgMnPc1d1	vídeňský
architekti	architekt	k1gMnPc1	architekt
Peter	Peter	k1gMnSc1	Peter
Nobile	nobile	k1gMnSc1	nobile
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Sprenger	Sprenger	k1gMnSc1	Sprenger
neogotické	ogotický	k2eNgNnSc4d1	neogotické
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
byl	být	k5eAaImAgInS	být
architektem	architekt	k1gMnSc7	architekt
Baumem	Baum	k1gMnSc7	Baum
novorenesančně	novorenesančně	k6eAd1	novorenesančně
přestavěn	přestavěn	k2eAgInSc4d1	přestavěn
Mikšův	Mikšův	k2eAgInSc4d1	Mikšův
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
sloužila	sloužit	k5eAaImAgFnS	sloužit
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
ústředí	ústředí	k1gNnSc2	ústředí
protifašistického	protifašistický	k2eAgInSc2d1	protifašistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
okolí	okolí	k1gNnSc6	okolí
probíhaly	probíhat	k5eAaImAgInP	probíhat
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
povstalci	povstalec	k1gMnPc7	povstalec
a	a	k8xC	a
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
škodám	škoda	k1gFnPc3	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
zničil	zničit	k5eAaPmAgInS	zničit
její	její	k3xOp3gNnSc4	její
novogotické	novogotický	k2eAgNnSc4d1	novogotické
křídlo	křídlo	k1gNnSc4	křídlo
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
stát	stát	k5eAaImF	stát
jen	jen	k9	jen
obvodové	obvodový	k2eAgFnPc4d1	obvodová
zdi	zeď	k1gFnPc4	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Těžké	těžký	k2eAgFnSc2d1	těžká
škody	škoda	k1gFnSc2	škoda
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
také	také	k9	také
věž	věž	k1gFnSc1	věž
a	a	k8xC	a
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
architektonická	architektonický	k2eAgFnSc1d1	architektonická
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
co	co	k9	co
s	s	k7c7	s
obvodovými	obvodový	k2eAgFnPc7d1	obvodová
zdmi	zeď	k1gFnPc7	zeď
novogotického	novogotický	k2eAgInSc2d1	novogotický
křídla	křídlo	k1gNnPc4	křídlo
udělat	udělat	k5eAaPmF	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
zdi	zeď	k1gFnPc4	zeď
strhnout	strhnout	k5eAaPmF	strhnout
a	a	k8xC	a
ponechat	ponechat	k5eAaPmF	ponechat
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
krajní	krajní	k2eAgNnSc4d1	krajní
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
pro	pro	k7c4	pro
statické	statický	k2eAgNnSc4d1	statické
zajištění	zajištění	k1gNnSc4	zajištění
radniční	radniční	k2eAgFnSc2d1	radniční
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
vypsáno	vypsat	k5eAaPmNgNnS	vypsat
několik	několik	k4yIc1	několik
architektonických	architektonický	k2eAgFnPc2d1	architektonická
soutěží	soutěž	k1gFnPc2	soutěž
na	na	k7c4	na
přestavbu	přestavba	k1gFnSc4	přestavba
a	a	k8xC	a
dostavbu	dostavba	k1gFnSc4	dostavba
Staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
soutěž	soutěž	k1gFnSc1	soutěž
byla	být	k5eAaImAgFnS	být
vypsána	vypsat	k5eAaPmNgFnS	vypsat
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
kolech	kolo	k1gNnPc6	kolo
již	již	k6eAd1	již
v	v	k7c6	v
letech	let	k1gInPc6	let
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
zadání	zadání	k1gNnSc1	zadání
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
neogotické	ogotický	k2eNgNnSc1d1	neogotické
křídlo	křídlo	k1gNnSc1	křídlo
zůstane	zůstat	k5eAaPmIp3nS	zůstat
zachováno	zachovat	k5eAaPmNgNnS	zachovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
průčelí	průčelí	k1gNnSc1	průčelí
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
skončila	skončit	k5eAaPmAgFnS	skončit
bez	bez	k7c2	bez
vítěze	vítěz	k1gMnSc2	vítěz
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
další	další	k2eAgFnSc1d1	další
obdobná	obdobný	k2eAgFnSc1d1	obdobná
soutěž	soutěž	k1gFnSc1	soutěž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Vítěze	vítěz	k1gMnSc4	vítěz
měla	mít	k5eAaImAgFnS	mít
až	až	k6eAd1	až
třetí	třetí	k4xOgFnSc4	třetí
soutěž	soutěž	k1gFnSc4	soutěž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
Antonín	Antonín	k1gMnSc1	Antonín
Wiehl	Wiehl	k1gMnSc1	Wiehl
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vítězný	vítězný	k2eAgInSc1d1	vítězný
návrh	návrh	k1gInSc1	návrh
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
většími	veliký	k2eAgInPc7d2	veliký
či	či	k8xC	či
menšími	malý	k2eAgInPc7d2	menší
časovými	časový	k2eAgInPc7d1	časový
odstupy	odstup	k1gInPc7	odstup
pak	pak	k6eAd1	pak
následovaly	následovat	k5eAaImAgFnP	následovat
další	další	k2eAgFnPc1d1	další
soutěže	soutěž	k1gFnPc1	soutěž
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
zejména	zejména	k9	zejména
od	od	k7c2	od
válečného	válečný	k2eAgInSc2d1	válečný
požáru	požár	k1gInSc2	požár
kladou	klást	k5eAaImIp3nP	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zaplnit	zaplnit	k5eAaPmF	zaplnit
prostor	prostor	k1gInSc4	prostor
po	po	k7c6	po
zbouraném	zbouraný	k2eAgNnSc6d1	zbourané
novogotickém	novogotický	k2eAgNnSc6d1	novogotické
křídle	křídlo	k1gNnSc6	křídlo
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
však	však	k9	však
buďto	buďto	k8xC	buďto
skončily	skončit	k5eAaPmAgFnP	skončit
bez	bez	k7c2	bez
vítězů	vítěz	k1gMnPc2	vítěz
nebo	nebo	k8xC	nebo
vítězné	vítězný	k2eAgInPc1d1	vítězný
návrhy	návrh	k1gInPc1	návrh
nebyly	být	k5eNaImAgInP	být
realizovány	realizovat	k5eAaBmNgInP	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
probíhá	probíhat	k5eAaImIp3nS	probíhat
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
vnějšího	vnější	k2eAgInSc2d1	vnější
pláště	plášť	k1gInSc2	plášť
Staromětské	Staromětský	k2eAgFnSc2d1	Staromětský
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
má	mít	k5eAaImIp3nS	mít
navrátit	navrátit	k5eAaPmF	navrátit
autentičtější	autentický	k2eAgFnSc4d2	autentičtější
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
trvat	trvat	k5eAaImF	trvat
do	do	k7c2	do
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
nebude	být	k5eNaImBp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
radniční	radniční	k2eAgFnSc1d1	radniční
kaple	kaple	k1gFnSc1	kaple
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
do	do	k7c2	do
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
bude	být	k5eAaImBp3nS	být
demontován	demontován	k2eAgInSc1d1	demontován
pražský	pražský	k2eAgInSc1d1	pražský
orloj	orloj	k1gInSc1	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Opravy	oprava	k1gFnPc1	oprava
budou	být	k5eAaImBp3nP	být
město	město	k1gNnSc4	město
stát	stát	k1gInSc1	stát
48	[number]	k4	48
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
1338	[number]	k4	1338
založení	založení	k1gNnSc2	založení
radnice	radnice	k1gFnSc2	radnice
1360	[number]	k4	1360
stavba	stavba	k1gFnSc1	stavba
druhého	druhý	k4xOgInSc2	druhý
domu	dům	k1gInSc2	dům
1381	[number]	k4	1381
dostavba	dostavba	k1gFnSc1	dostavba
a	a	k8xC	a
vysvěcení	vysvěcení	k1gNnSc1	vysvěcení
gotické	gotický	k2eAgFnSc2d1	gotická
radniční	radniční	k2eAgFnSc2d1	radniční
kaple	kaple	k1gFnSc2	kaple
1410	[number]	k4	1410
spuštění	spuštění	k1gNnSc2	spuštění
Staroměstského	staroměstský	k2eAgInSc2d1	staroměstský
orloje	orloj	k1gInSc2	orloj
od	od	k7c2	od
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
z	z	k7c2	z
Kadaně	Kadaň	k1gFnSc2	Kadaň
1422	[number]	k4	1422
poprava	poprava	k1gFnSc1	poprava
radikálního	radikální	k2eAgMnSc2d1	radikální
vůdce	vůdce	k1gMnSc2	vůdce
pražské	pražský	k2eAgFnSc2d1	Pražská
husitské	husitský	k2eAgFnSc2d1	husitská
chudiny	chudina	k1gFnSc2	chudina
Jana	Jan	k1gMnSc2	Jan
Želivského	želivský	k2eAgMnSc2d1	želivský
1458	[number]	k4	1458
nákup	nákup	k1gInSc1	nákup
Mikšova	Mikšův	k2eAgInSc2d1	Mikšův
domu	dům	k1gInSc2	dům
1458	[number]	k4	1458
zvolení	zvolení	k1gNnSc4	zvolení
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
českým	český	k2eAgMnSc7d1	český
<g />
.	.	kIx.	.
</s>
<s>
králem	král	k1gMnSc7	král
1520	[number]	k4	1520
umístění	umístění	k1gNnSc1	umístění
renesančního	renesanční	k2eAgNnSc2d1	renesanční
okna	okno	k1gNnSc2	okno
na	na	k7c4	na
jižní	jižní	k2eAgNnSc4d1	jižní
průčelí	průčelí	k1gNnSc4	průčelí
1621	[number]	k4	1621
poprava	poprava	k1gFnSc1	poprava
27	[number]	k4	27
českých	český	k2eAgMnPc2d1	český
pánů	pan	k1gMnPc2	pan
1784	[number]	k4	1784
radnice	radnice	k1gFnSc1	radnice
sídlem	sídlo	k1gNnSc7	sídlo
spojené	spojený	k2eAgFnSc2d1	spojená
pražské	pražský	k2eAgFnSc2d1	Pražská
městské	městský	k2eAgFnSc2d1	městská
správy	správa	k1gFnSc2	správa
1807	[number]	k4	1807
umístění	umístění	k1gNnSc2	umístění
nových	nový	k2eAgFnPc2d1	nová
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
ochozu	ochoz	k1gInSc2	ochoz
na	na	k7c4	na
věž	věž	k1gFnSc4	věž
1848	[number]	k4	1848
dostavba	dostavba	k1gFnSc1	dostavba
neogotického	ogotický	k2eNgNnSc2d1	neogotické
křídla	křídlo	k1gNnSc2	křídlo
1880	[number]	k4	1880
přestavba	přestavba	k1gFnSc1	přestavba
Mikšova	Mikšův	k2eAgInSc2d1	Mikšův
domu	dům	k1gInSc2	dům
1945	[number]	k4	1945
požár	požár	k1gInSc1	požár
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
vyhoření	vyhoření	k1gNnSc2	vyhoření
novogotického	novogotický	k2eAgNnSc2d1	novogotické
východního	východní	k2eAgNnSc2d1	východní
křídla	křídlo	k1gNnSc2	křídlo
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
Pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
Výška	výška	k1gFnSc1	výška
věže	věž	k1gFnSc2	věž
<g/>
:	:	kIx,	:
69,5	[number]	k4	69,5
m	m	kA	m
Šířka	šířka	k1gFnSc1	šířka
věže	věž	k1gFnSc2	věž
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
8,37	[number]	k4	8,37
m	m	kA	m
Hloubka	hloubka	k1gFnSc1	hloubka
věže	věž	k1gFnSc2	věž
(	(	kIx(	(
<g/>
východni	východnit	k5eAaPmRp2nS	východnit
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
7,91	[number]	k4	7,91
m	m	kA	m
Šířka	šířka	k1gFnSc1	šířka
radnice	radnice	k1gFnSc1	radnice
<g/>
:	:	kIx,	:
67,45	[number]	k4	67,45
m	m	kA	m
</s>
