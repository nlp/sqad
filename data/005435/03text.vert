<s>
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
země	zem	k1gFnSc2	zem
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
City	City	k1gFnSc1	City
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
světových	světový	k2eAgNnPc2d1	světové
obchodních	obchodní	k2eAgNnPc2d1	obchodní
center	centrum	k1gNnPc2	centrum
a	a	k8xC	a
Londýn	Londýn	k1gInSc1	Londýn
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
městy	město	k1gNnPc7	město
New	New	k1gMnPc2	New
York	York	k1gInSc4	York
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc4	Tokio
a	a	k8xC	a
Paříž	Paříž	k1gFnSc4	Paříž
označován	označován	k2eAgMnSc1d1	označován
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Londýna	Londýn	k1gInSc2	Londýn
za	za	k7c4	za
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
jeho	jeho	k3xOp3gFnSc2	jeho
historie	historie	k1gFnSc2	historie
zažili	zažít	k5eAaPmAgMnP	zažít
mor	mor	k1gInSc4	mor
<g/>
,	,	kIx,	,
zničující	zničující	k2eAgInSc4d1	zničující
požár	požár	k1gInSc4	požár
<g/>
,	,	kIx,	,
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
vzdušné	vzdušný	k2eAgNnSc4d1	vzdušné
bombardování	bombardování	k1gNnSc4	bombardování
a	a	k8xC	a
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
se	se	k3xPyFc4	se
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
finančních	finanční	k2eAgNnPc2d1	finanční
a	a	k8xC	a
kulturních	kulturní	k2eAgNnPc2d1	kulturní
center	centrum	k1gNnPc2	centrum
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1	středověká
mytologie	mytologie	k1gFnSc1	mytologie
Geoffreye	Geoffreye	k1gFnSc1	Geoffreye
Monmoutha	Monmoutha	k1gFnSc1	Monmoutha
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Trójanem	Trójan	k1gMnSc7	Trójan
Brutem	Brut	k1gMnSc7	Brut
v	v	k7c6	v
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgFnSc6d1	bronzová
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgInSc1d1	nazván
Troia	Troium	k1gNnSc2	Troium
Nova	nova	k1gFnSc1	nova
nebo	nebo	k8xC	nebo
Trinovantum	Trinovantum	k1gNnSc1	Trinovantum
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Lud	Lud	k1gMnSc1	Lud
toto	tento	k3xDgNnSc4	tento
město	město	k1gNnSc4	město
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
CaerLudein	CaerLudein	k1gInSc4	CaerLudein
-	-	kIx~	-
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
dnešní	dnešní	k2eAgInSc1d1	dnešní
název	název	k1gInSc1	název
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Geoffrey	Geoffre	k2eAgInPc1d1	Geoffre
Monmouth	Monmouth	k1gInSc1	Monmouth
opatřil	opatřit	k5eAaPmAgInS	opatřit
prehistorický	prehistorický	k2eAgInSc1d1	prehistorický
Londýn	Londýn	k1gInSc1	Londýn
bohatstvím	bohatství	k1gNnSc7	bohatství
legendárních	legendární	k2eAgMnPc2d1	legendární
králů	král	k1gMnPc2	král
a	a	k8xC	a
zajímavými	zajímavý	k2eAgInPc7d1	zajímavý
příběhy	příběh	k1gInPc7	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
intenzívním	intenzívní	k2eAgFnPc3d1	intenzívní
vykopávkám	vykopávka	k1gFnPc3	vykopávka
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
archeologové	archeolog	k1gMnPc1	archeolog
zatím	zatím	k6eAd1	zatím
nenašli	najít	k5eNaPmAgMnP	najít
žádné	žádný	k3yNgFnPc4	žádný
stopy	stopa	k1gFnPc4	stopa
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
pouze	pouze	k6eAd1	pouze
roztroušené	roztroušený	k2eAgInPc1d1	roztroušený
prehistorické	prehistorický	k2eAgInPc1d1	prehistorický
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
stopy	stopa	k1gFnPc1	stopa
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
a	a	k8xC	a
stopy	stopa	k1gFnSc2	stopa
obydlení	obydlení	k1gNnSc2	obydlení
<g/>
.	.	kIx.	.
</s>
<s>
Považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
málo	málo	k1gNnSc4	málo
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
osídlením	osídlení	k1gNnSc7	osídlení
Římany	Říman	k1gMnPc7	Říman
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prehistorické	prehistorický	k2eAgFnSc6d1	prehistorická
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
Londýna	Londýn	k1gInSc2	Londýn
zřejmě	zřejmě	k6eAd1	zřejmě
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
roztroušenými	roztroušený	k2eAgFnPc7d1	roztroušená
zemědělskými	zemědělský	k2eAgFnPc7d1	zemědělská
usedlostmi	usedlost	k1gFnPc7	usedlost
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Londinium	Londinium	k1gNnSc4	Londinium
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Římany	Říman	k1gMnPc7	Říman
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
invazi	invaze	k1gFnSc6	invaze
roku	rok	k1gInSc2	rok
43	[number]	k4	43
vedené	vedený	k2eAgFnSc2d1	vedená
císařem	císař	k1gMnSc7	císař
Claudiem	Claudium	k1gNnSc7	Claudium
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
Londinium	Londinium	k1gNnSc1	Londinium
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc1	jeho
kořeny	kořen	k1gInPc1	kořen
nejasné	jasný	k2eNgInPc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
verze	verze	k1gFnSc1	verze
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
osobního	osobní	k2eAgNnSc2d1	osobní
jména	jméno	k1gNnSc2	jméno
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
'	'	kIx"	'
<g/>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
poslední	poslední	k2eAgInPc1d1	poslední
výzkumy	výzkum	k1gInPc1	výzkum
vedené	vedený	k2eAgFnSc2d1	vedená
Richardem	Richard	k1gMnSc7	Richard
Coatesem	Coates	k1gMnSc7	Coates
ukázaly	ukázat	k5eAaPmAgInP	ukázat
jako	jako	k8xC	jako
pravděpodobné	pravděpodobný	k2eAgInPc1d1	pravděpodobný
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
předkeltského	předkeltský	k2eAgNnSc2d1	předkeltský
slova	slovo	k1gNnSc2	slovo
Plowonida	Plowonid	k1gMnSc2	Plowonid
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
kořeny	kořen	k1gInPc4	kořen
–	–	k?	–
plew	plew	k?	plew
a	a	k8xC	a
nejd	nejd	k1gMnSc1	nejd
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
proudící	proudící	k2eAgFnSc1d1	proudící
řeka	řeka	k1gFnSc1	řeka
-	-	kIx~	-
Londinium	Londinium	k1gNnSc1	Londinium
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
znamenalo	znamenat	k5eAaImAgNnS	znamenat
osada	osada	k1gFnSc1	osada
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
divoké	divoký	k2eAgFnSc2d1	divoká
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Coates	Coates	k1gMnSc1	Coates
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
řeka	řeka	k1gFnSc1	řeka
byla	být	k5eAaImAgFnS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
Temží	Temže	k1gFnSc7	Temže
v	v	k7c4	v
horní	horní	k2eAgFnPc4d1	horní
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
užší	úzký	k2eAgFnSc1d2	užší
a	a	k8xC	a
Plowonida	Plowonida	k1gFnSc1	Plowonida
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
široká	široký	k2eAgFnSc1d1	široká
pro	pro	k7c4	pro
přebrodění	přebrodění	k1gNnSc4	přebrodění
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
nyní	nyní	k6eAd1	nyní
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
jako	jako	k8xC	jako
civilizovaná	civilizovaný	k2eAgFnSc1d1	civilizovaná
osada	osada	k1gFnSc1	osada
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
odvodnění	odvodnění	k1gNnSc2	odvodnění
u	u	k7c2	u
hlavní	hlavní	k2eAgFnSc2d1	hlavní
<g/>
,	,	kIx,	,
Římany	Říman	k1gMnPc4	Říman
postavené	postavený	k2eAgFnSc2d1	postavená
cesty	cesta	k1gFnSc2	cesta
vykopané	vykopaný	k2eAgFnSc2d1	vykopaná
u	u	k7c2	u
No	no	k9	no
1	[number]	k4	1
Poultry	Poultrum	k1gNnPc7	Poultrum
byly	být	k5eAaImAgInP	být
datovány	datován	k2eAgInPc1d1	datován
do	do	k7c2	do
roku	rok	k1gInSc2	rok
47	[number]	k4	47
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
rok	rok	k1gInSc4	rok
založení	založení	k1gNnSc2	založení
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
10	[number]	k4	10
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
Londinium	Londinium	k1gNnSc1	Londinium
vydrancováno	vydrancovat	k5eAaPmNgNnS	vydrancovat
kmenem	kmen	k1gInSc7	kmen
Iceni	Icen	k1gMnPc1	Icen
vedeným	vedený	k2eAgNnSc7d1	vedené
královnou	královna	k1gFnSc7	královna
jménem	jméno	k1gNnSc7	jméno
Boudicca	Boudicca	k1gMnSc1	Boudicca
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
výzkumy	výzkum	k1gInPc1	výzkum
prokázaly	prokázat	k5eAaPmAgInP	prokázat
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
známky	známka	k1gFnPc4	známka
zkázy	zkáza	k1gFnSc2	zkáza
ohněm	oheň	k1gInSc7	oheň
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
také	také	k9	také
vojenské	vojenský	k2eAgNnSc4d1	vojenské
opevnění	opevnění	k1gNnSc4	opevnění
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
centrem	centrum	k1gNnSc7	centrum
římské	římský	k2eAgFnSc2d1	římská
obrany	obrana	k1gFnSc2	obrana
města	město	k1gNnSc2	město
proti	proti	k7c3	proti
britskému	britský	k2eAgNnSc3d1	Britské
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
vzpamatovalo	vzpamatovat	k5eAaPmAgNnS	vzpamatovat
asi	asi	k9	asi
po	po	k7c6	po
10	[number]	k4	10
letech	let	k1gInPc6	let
a	a	k8xC	a
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
srovnatelného	srovnatelný	k2eAgInSc2d1	srovnatelný
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
před	před	k7c7	před
zničením	zničení	k1gNnSc7	zničení
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
140	[number]	k4	140
-	-	kIx~	-
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
římské	římský	k2eAgFnSc2d1	římská
provincie	provincie	k1gFnSc2	provincie
Británie	Británie	k1gFnSc1	Británie
(	(	kIx(	(
<g/>
předchozím	předchozí	k2eAgInSc7d1	předchozí
centrem	centr	k1gInSc7	centr
byl	být	k5eAaImAgInS	být
blízký	blízký	k2eAgInSc1d1	blízký
Colchester	Colchester	k1gInSc1	Colchester
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nastal	nastat	k5eAaPmAgInS	nastat
mírný	mírný	k2eAgInSc1d1	mírný
útlum	útlum	k1gInSc1	útlum
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
375	[number]	k4	375
byl	být	k5eAaImAgInS	být
Londýn	Londýn	k1gInSc1	Londýn
malou	malý	k2eAgFnSc4d1	malá
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc4d1	bohatá
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
dobře	dobře	k6eAd1	dobře
vybudovanou	vybudovaný	k2eAgFnSc7d1	vybudovaná
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
410	[number]	k4	410
římská	římský	k2eAgFnSc1d1	římská
okupace	okupace	k1gFnSc1	okupace
skončila	skončit	k5eAaPmAgFnS	skončit
a	a	k8xC	a
obyvatelům	obyvatel	k1gMnPc3	obyvatel
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
obranu	obrana	k1gFnSc4	obrana
zajišťovali	zajišťovat	k5eAaImAgMnP	zajišťovat
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
římské	římský	k2eAgNnSc1d1	římské
město	město	k1gNnSc1	město
prakticky	prakticky	k6eAd1	prakticky
opuštěno	opustit	k5eAaPmNgNnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Strategická	strategický	k2eAgFnSc1d1	strategická
pozice	pozice	k1gFnSc1	pozice
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Temži	Temže	k1gFnSc4	Temže
vedla	vést	k5eAaImAgFnS	vést
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
600	[number]	k4	600
Anglosasy	Anglosas	k1gMnPc4	Anglosas
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
obnovili	obnovit	k5eAaPmAgMnP	obnovit
osídlení	osídlení	k1gNnSc4	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
anglosaská	anglosaský	k2eAgFnSc1d1	anglosaská
osada	osada	k1gFnSc1	osada
nebyla	být	k5eNaImAgFnS	být
zbudovaná	zbudovaný	k2eAgFnSc1d1	zbudovaná
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
ohrazené	ohrazený	k2eAgFnSc2d1	ohrazená
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
asi	asi	k9	asi
jeden	jeden	k4xCgInSc4	jeden
kilometr	kilometr	k1gInSc4	kilometr
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
Lundenwic	Lundenwice	k1gInPc2	Lundenwice
-	-	kIx~	-
londýnský	londýnský	k2eAgInSc1d1	londýnský
obvod	obvod	k1gInSc1	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Nedávné	dávný	k2eNgFnPc1d1	nedávná
vykopávky	vykopávka	k1gFnPc1	vykopávka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Covent	Covent	k1gMnSc1	Covent
Garden	Gardna	k1gFnPc2	Gardna
odkryly	odkrýt	k5eAaPmAgFnP	odkrýt
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
anglosaské	anglosaský	k2eAgNnSc1d1	anglosaské
osídlení	osídlení	k1gNnSc1	osídlení
<g/>
,	,	kIx,	,
datované	datovaný	k2eAgNnSc1d1	datované
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
osídlení	osídlení	k1gNnSc1	osídlení
zabíralo	zabírat	k5eAaImAgNnS	zabírat
plochu	plocha	k1gFnSc4	plocha
asi	asi	k9	asi
60	[number]	k4	60
hektarů	hektar	k1gInPc2	hektar
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
od	od	k7c2	od
nynější	nynější	k2eAgFnSc2d1	nynější
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
na	na	k7c6	na
východě	východ	k1gInSc6	východ
až	až	k9	až
po	po	k7c6	po
Aldwych	Aldwy	k1gFnPc6	Aldwy
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
koncem	konec	k1gInSc7	konec
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
centrum	centrum	k1gNnSc1	centrum
osady	osada	k1gFnSc2	osada
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
lokality	lokalita	k1gFnSc2	lokalita
do	do	k7c2	do
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
administrativními	administrativní	k2eAgFnPc7d1	administrativní
změnami	změna	k1gFnPc7	změna
zahájenými	zahájený	k2eAgFnPc7d1	zahájená
Alfrédem	Alfréd	k1gMnSc7	Alfréd
Velikým	veliký	k2eAgNnSc7d1	veliké
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
Guthrumem	Guthrum	k1gInSc7	Guthrum
<g/>
.	.	kIx.	.
</s>
<s>
Alfréd	Alfréd	k1gMnSc1	Alfréd
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
svého	svůj	k3xOyFgMnSc4	svůj
zetě	zeť	k1gMnSc4	zeť
hraběte	hrabě	k1gMnSc4	hrabě
Aethelreda	Aethelred	k1gMnSc4	Aethelred
z	z	k7c2	z
Mercie	Mercie	k1gFnSc2	Mercie
správcem	správce	k1gMnSc7	správce
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
zbudovat	zbudovat	k5eAaPmF	zbudovat
dvě	dva	k4xCgNnPc4	dva
obranná	obranný	k2eAgNnPc4d1	obranné
opevnění	opevnění	k1gNnPc4	opevnění
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
opraven	opravna	k1gFnPc2	opravna
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nazýván	nazývat	k5eAaImNgInS	nazývat
Lundenburgh	Lundenburgh	k1gInSc1	Lundenburgh
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
kolem	kolem	k7c2	kolem
mostu	most	k1gInSc2	most
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
oblast	oblast	k1gFnSc1	oblast
Southwark	Southwark	k1gInSc1	Southwark
nebo	nebo	k8xC	nebo
Suthringa	Suthringa	k1gFnSc1	Suthringa
Geworc	Geworc	k1gFnSc1	Geworc
(	(	kIx(	(
<g/>
obranná	obranný	k2eAgFnSc1d1	obranná
stavba	stavba	k1gFnSc1	stavba
mužů	muž	k1gMnPc2	muž
ze	z	k7c2	z
Surrey	Surrea	k1gFnSc2	Surrea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
invazi	invaze	k1gFnSc3	invaze
Normanů	Norman	k1gMnPc2	Norman
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
obsazením	obsazení	k1gNnSc7	obsazení
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
korunovací	korunovace	k1gFnPc2	korunovace
normanského	normanský	k2eAgMnSc2d1	normanský
vévody	vévoda	k1gMnSc2	vévoda
Viléma	Vilém	k1gMnSc2	Vilém
Dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
několik	několik	k4yIc4	několik
tvrzí	tvrz	k1gFnPc2	tvrz
–	–	k?	–
Tower	Tower	k1gMnSc1	Tower
<g/>
,	,	kIx,	,
Baynard	Baynard	k1gMnSc1	Baynard
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Castle	Castle	k1gFnSc1	Castle
a	a	k8xC	a
Montfichet	Montfichet	k1gInSc1	Montfichet
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Castle	Castle	k1gFnSc1	Castle
jako	jako	k8xC	jako
prevence	prevence	k1gFnSc1	prevence
proti	proti	k7c3	proti
vzpourám	vzpoura	k1gFnPc3	vzpoura
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1067	[number]	k4	1067
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
původní	původní	k2eAgNnPc4d1	původní
saská	saský	k2eAgNnPc4d1	Saské
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
výsady	výsada	k1gFnPc4	výsada
<g/>
.	.	kIx.	.
</s>
<s>
Samospráva	samospráva	k1gFnSc1	samospráva
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
upevněna	upevněn	k2eAgFnSc1d1	upevněna
volebním	volební	k2eAgNnSc6d1	volební
právem	právem	k6eAd1	právem
garantovaným	garantovaný	k2eAgMnSc7d1	garantovaný
králem	král	k1gMnSc7	král
Janem	Jan	k1gMnSc7	Jan
Bezzemkem	bezzemek	k1gMnSc7	bezzemek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1199	[number]	k4	1199
a	a	k8xC	a
1215	[number]	k4	1215
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ryšavý	Ryšavý	k1gMnSc1	Ryšavý
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Viléma	Vilém	k1gMnSc2	Vilém
Dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
stavbu	stavba	k1gFnSc4	stavba
Westminster	Westminster	k1gMnSc1	Westminster
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
základu	základ	k1gInSc2	základ
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
středověku	středověk	k1gInSc2	středověk
hlavním	hlavní	k2eAgNnSc7d1	hlavní
královským	královský	k2eAgNnSc7d1	královské
sídlem	sídlo	k1gNnSc7	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1176	[number]	k4	1176
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
stavba	stavba	k1gFnSc1	stavba
mostu	most	k1gInSc2	most
London	London	k1gMnSc1	London
Bridge	Bridge	k1gFnSc1	Bridge
(	(	kIx(	(
<g/>
dokončena	dokončen	k2eAgFnSc1d1	dokončena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1209	[number]	k4	1209
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
předchozích	předchozí	k2eAgInPc2d1	předchozí
několika	několik	k4yIc2	několik
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
po	po	k7c4	po
dalších	další	k2eAgNnPc2d1	další
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgInSc7d1	jediný
londýnským	londýnský	k2eAgInSc7d1	londýnský
mostem	most	k1gInSc7	most
přes	přes	k7c4	přes
Temži	Temže	k1gFnSc4	Temže
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1739	[number]	k4	1739
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
Londýn	Londýn	k1gInSc1	Londýn
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
Westminsteru	Westminster	k1gInSc2	Westminster
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
královským	královský	k2eAgNnSc7d1	královské
sídlem	sídlo	k1gNnSc7	sídlo
a	a	k8xC	a
vládním	vládní	k2eAgNnSc7d1	vládní
centrem	centrum	k1gNnSc7	centrum
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
City	city	k1gNnSc1	city
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
centrem	centrum	k1gNnSc7	centrum
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc4	území
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
asi	asi	k9	asi
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
neobydleno	obydlet	k5eNaPmNgNnS	obydlet
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
řemesla	řemeslo	k1gNnPc1	řemeslo
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
vytrvale	vytrvale	k6eAd1	vytrvale
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
rostl	růst	k5eAaImAgInS	růst
i	i	k9	i
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1100	[number]	k4	1100
měl	mít	k5eAaImAgInS	mít
Londýn	Londýn	k1gInSc1	Londýn
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1300	[number]	k4	1300
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
na	na	k7c4	na
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Řemesla	řemeslo	k1gNnPc1	řemeslo
byla	být	k5eAaImAgNnP	být
organizována	organizovat	k5eAaBmNgNnP	organizovat
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
cechů	cech	k1gInPc2	cech
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
ovládaly	ovládat	k5eAaImAgInP	ovládat
město	město	k1gNnSc4	město
a	a	k8xC	a
volily	volit	k5eAaImAgFnP	volit
správce	správce	k1gMnSc4	správce
města	město	k1gNnSc2	město
–	–	k?	–
Lord	lord	k1gMnSc1	lord
Mayor	Mayor	k1gMnSc1	Mayor
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Středověký	středověký	k2eAgInSc1d1	středověký
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
protkán	protkat	k5eAaPmNgInS	protkat
úzkými	úzký	k2eAgFnPc7d1	úzká
a	a	k8xC	a
křivolakými	křivolaký	k2eAgFnPc7d1	křivolaká
ulicemi	ulice	k1gFnPc7	ulice
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
budov	budova	k1gFnPc2	budova
byla	být	k5eAaImAgFnS	být
stavěna	stavit	k5eAaImNgFnS	stavit
z	z	k7c2	z
hořlavých	hořlavý	k2eAgInPc2d1	hořlavý
materiálů	materiál	k1gInPc2	materiál
jako	jako	k8xC	jako
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
sláma	sláma	k1gFnSc1	sláma
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
častých	častý	k2eAgInPc2d1	častý
požárů	požár	k1gInPc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Hygiena	hygiena	k1gFnSc1	hygiena
a	a	k8xC	a
kanalizace	kanalizace	k1gFnSc1	kanalizace
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc6d1	nízká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
ztratil	ztratit	k5eAaPmAgInS	ztratit
asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
-	-	kIx~	-
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tzv.	tzv.	kA	tzv.
černé	černý	k2eAgFnSc2d1	černá
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Death	Death	k1gMnSc1	Death
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
rokem	rok	k1gInSc7	rok
1348	[number]	k4	1348
a	a	k8xC	a
Velkým	velký	k2eAgInSc7d1	velký
morem	mor	k1gInSc7	mor
(	(	kIx(	(
<g/>
Great	Great	k2eAgInSc1d1	Great
Plague	Plague	k1gInSc1	Plague
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1665	[number]	k4	1665
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
šestnáctkrát	šestnáctkrát	k6eAd1	šestnáctkrát
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
morová	morový	k2eAgFnSc1d1	morová
nákaza	nákaza	k1gFnSc1	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudor	tudor	k1gInSc1	tudor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1485	[number]	k4	1485
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ukončil	ukončit	k5eAaPmAgInS	ukončit
válku	válka	k1gFnSc4	válka
růží	růž	k1gFnPc2	růž
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
rozhodný	rozhodný	k2eAgMnSc1d1	rozhodný
a	a	k8xC	a
schopný	schopný	k2eAgMnSc1d1	schopný
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
centralizoval	centralizovat	k5eAaBmAgMnS	centralizovat
moc	moc	k6eAd1	moc
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
půjčování	půjčování	k1gNnSc2	půjčování
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
z	z	k7c2	z
City	City	k1gFnSc2	City
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgMnPc2d1	předchozí
králů	král	k1gMnPc2	král
splácel	splácet	k5eAaImAgMnS	splácet
půjčky	půjčka	k1gFnSc2	půjčka
včas	včas	k6eAd1	včas
<g/>
.	.	kIx.	.
</s>
<s>
Málo	málo	k6eAd1	málo
se	se	k3xPyFc4	se
ale	ale	k9	ale
věnoval	věnovat	k5eAaPmAgMnS	věnovat
rozvoji	rozvoj	k1gInSc3	rozvoj
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Poměrná	poměrný	k2eAgFnSc1d1	poměrná
stabilita	stabilita	k1gFnSc1	stabilita
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
si	se	k3xPyFc3	se
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
hájit	hájit	k5eAaImF	hájit
své	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
místo	místo	k1gNnSc4	místo
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
půtek	půtka	k1gFnPc2	půtka
v	v	k7c6	v
provinciích	provincie	k1gFnPc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Tudorovský	tudorovský	k2eAgInSc1d1	tudorovský
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
však	však	k9	však
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
dobou	doba	k1gFnSc7	doba
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1497	[number]	k4	1497
se	se	k3xPyFc4	se
Perkin	Perkin	k1gMnSc1	Perkin
Warbeck	Warbeck	k1gMnSc1	Warbeck
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
mladého	mladý	k2eAgMnSc2d1	mladý
krále	král	k1gMnSc2	král
Eduarda	Eduard	k1gMnSc2	Eduard
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
Richardem	Richard	k1gMnSc7	Richard
<g/>
,	,	kIx,	,
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
utábořil	utábořit	k5eAaPmAgMnS	utábořit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
společníky	společník	k1gMnPc7	společník
u	u	k7c2	u
Blackheath	Blackheatha	k1gFnPc2	Blackheatha
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
zavládla	zavládnout	k5eAaPmAgFnS	zavládnout
panika	panika	k1gFnSc1	panika
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
však	však	k9	však
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
obranu	obrana	k1gFnSc4	obrana
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
povstání	povstání	k1gNnSc2	povstání
potlačil	potlačit	k5eAaPmAgMnS	potlačit
a	a	k8xC	a
Warbeck	Warbeck	k1gMnSc1	Warbeck
byl	být	k5eAaImAgMnS	být
zajat	zajmout	k5eAaPmNgMnS	zajmout
a	a	k8xC	a
popraven	popravit	k5eAaPmNgMnS	popravit
v	v	k7c6	v
Tyburnu	Tyburn	k1gInSc6	Tyburn
<g/>
.	.	kIx.	.
</s>
<s>
Reformace	reformace	k1gFnSc1	reformace
přinesla	přinést	k5eAaPmAgFnS	přinést
za	za	k7c2	za
součinnosti	součinnost	k1gFnSc2	součinnost
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
společnosti	společnost	k1gFnSc2	společnost
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
mnoho	mnoho	k6eAd1	mnoho
krveprolití	krveprolití	k1gNnSc2	krveprolití
a	a	k8xC	a
posunula	posunout	k5eAaPmAgFnS	posunout
společnost	společnost	k1gFnSc1	společnost
k	k	k7c3	k
protestantismu	protestantismus	k1gInSc3	protestantismus
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
reformací	reformace	k1gFnSc7	reformace
tvořily	tvořit	k5eAaImAgFnP	tvořit
větší	veliký	k2eAgFnSc4d2	veliký
polovinu	polovina	k1gFnSc4	polovina
osídlení	osídlení	k1gNnSc2	osídlení
Londýna	Londýn	k1gInSc2	Londýn
kláštery	klášter	k1gInPc1	klášter
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
církevní	církevní	k2eAgFnPc1d1	církevní
stavby	stavba	k1gFnPc1	stavba
a	a	k8xC	a
mniši	mnich	k1gMnPc1	mnich
a	a	k8xC	a
jeptišky	jeptiška	k1gFnSc2	jeptiška
tvořili	tvořit	k5eAaImAgMnP	tvořit
asi	asi	k9	asi
třetinu	třetina	k1gFnSc4	třetina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zrušení	zrušení	k1gNnSc1	zrušení
klášterů	klášter	k1gInPc2	klášter
provedené	provedený	k2eAgFnSc2d1	provedená
Jindřichem	Jindřich	k1gMnSc7	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c4	na
město	město	k1gNnSc4	město
značný	značný	k2eAgInSc1d1	značný
vliv	vliv	k1gInSc1	vliv
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
téměř	téměř	k6eAd1	téměř
veškerý	veškerý	k3xTgInSc4	veškerý
církevní	církevní	k2eAgInSc4d1	církevní
majetek	majetek	k1gInSc4	majetek
změnil	změnit	k5eAaPmAgMnS	změnit
majitele	majitel	k1gMnSc4	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
začal	začít	k5eAaPmAgInS	začít
uprostřed	uprostřed	k7c2	uprostřed
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1538	[number]	k4	1538
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
klášterů	klášter	k1gInPc2	klášter
zrušena	zrušen	k2eAgFnSc1d1	zrušena
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
Jindřich	Jindřich	k1gMnSc1	Jindřich
obnovil	obnovit	k5eAaPmAgInS	obnovit
nemocnici	nemocnice	k1gFnSc4	nemocnice
St	St	kA	St
Bartholomew	Bartholomew	k1gFnSc1	Bartholomew
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
budov	budova	k1gFnPc2	budova
zůstala	zůstat	k5eAaPmAgFnS	zůstat
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1547	[number]	k4	1547
opuštěna	opustit	k5eAaPmNgFnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Eduarda	Eduard	k1gMnSc2	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
cechovním	cechovní	k2eAgFnPc3d1	cechovní
společnostem	společnost	k1gFnPc3	společnost
spláceny	splácen	k2eAgInPc4d1	splácen
dluhy	dluh	k1gInPc4	dluh
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
příjmy	příjem	k1gInPc1	příjem
byly	být	k5eAaImAgInP	být
věnovány	věnovat	k5eAaImNgInP	věnovat
na	na	k7c4	na
charitativní	charitativní	k2eAgInPc4d1	charitativní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1550	[number]	k4	1550
City	City	k1gFnSc1	City
koupila	koupit	k5eAaPmAgFnS	koupit
královský	královský	k2eAgInSc4d1	královský
statek	statek	k1gInSc4	statek
v	v	k7c6	v
Southwarku	Southwark	k1gInSc6	Southwark
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
a	a	k8xC	a
obnovila	obnovit	k5eAaPmAgFnS	obnovit
klášter	klášter	k1gInSc4	klášter
svatého	svatý	k2eAgMnSc2d1	svatý
Tomáše	Tomáš	k1gMnSc2	Tomáš
jako	jako	k8xS	jako
nemocnici	nemocnice	k1gFnSc4	nemocnice
St	St	kA	St
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
také	také	k9	také
založena	založen	k2eAgFnSc1d1	založena
nemocnice	nemocnice	k1gFnSc1	nemocnice
Christ	Christ	k1gMnSc1	Christ
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hospital	Hospital	k1gMnSc1	Hospital
a	a	k8xC	a
Bridewell	Bridewell	k1gMnSc1	Bridewell
Palace	Palace	k1gFnSc2	Palace
byl	být	k5eAaImAgMnS	být
upraven	upravit	k5eAaPmNgMnS	upravit
na	na	k7c4	na
dětský	dětský	k2eAgInSc4d1	dětský
domov	domov	k1gInSc4	domov
a	a	k8xC	a
nápravný	nápravný	k2eAgInSc4d1	nápravný
dům	dům	k1gInSc4	dům
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Eduarda	Eduard	k1gMnSc2	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1553	[number]	k4	1553
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Toweru	Tower	k1gInSc6	Tower
přijata	přijat	k2eAgFnSc1d1	přijata
lady	lady	k1gFnSc1	lady
Jane	Jan	k1gMnSc5	Jan
Greyová	Greyový	k2eAgFnSc1d1	Greyová
jako	jako	k8xS	jako
příští	příští	k2eAgFnSc1d1	příští
královna	královna	k1gFnSc1	královna
-	-	kIx~	-
ale	ale	k8xC	ale
Lord	lord	k1gMnSc1	lord
Mayor	Mayor	k1gMnSc1	Mayor
(	(	kIx(	(
<g/>
primátor	primátor	k1gMnSc1	primátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konšelé	konšel	k1gMnPc1	konšel
a	a	k8xC	a
soudci	soudce	k1gMnPc1	soudce
brzy	brzy	k6eAd1	brzy
změnili	změnit	k5eAaPmAgMnP	změnit
názor	názor	k1gInSc4	názor
a	a	k8xC	a
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
královnou	královna	k1gFnSc7	královna
Marii	Maria	k1gFnSc4	Maria
I.	I.	kA	I.
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
královna	královna	k1gFnSc1	královna
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
provdat	provdat	k5eAaPmF	provdat
za	za	k7c2	za
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
vyprovokovalo	vyprovokovat	k5eAaPmAgNnS	vyprovokovat
povstání	povstání	k1gNnSc1	povstání
vedené	vedený	k2eAgNnSc1d1	vedené
sirem	sir	k1gMnSc7	sir
Tomasem	Tomas	k1gMnSc7	Tomas
Wyatem	Wyat	k1gMnSc7	Wyat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spojenci	spojenec	k1gMnPc7	spojenec
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
Southwark	Southwark	k1gInSc4	Southwark
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dorazil	dorazit	k5eAaPmAgMnS	dorazit
až	až	k9	až
k	k	k7c3	k
Charing	Charing	k1gInSc4	Charing
Cross	Crossa	k1gFnPc2	Crossa
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
v	v	k7c6	v
City	city	k1gNnSc6	city
nemělo	mít	k5eNaImAgNnS	mít
povstání	povstání	k1gNnSc1	povstání
žádnou	žádný	k3yNgFnSc4	žádný
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
tak	tak	k9	tak
Wyatt	Wyatt	k1gInSc4	Wyatt
kapituloval	kapitulovat	k5eAaBmAgMnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
demonstrovala	demonstrovat	k5eAaBmAgFnS	demonstrovat
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
politický	politický	k2eAgInSc4d1	politický
význam	význam	k1gInSc4	význam
City	city	k1gNnSc1	city
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
vliv	vliv	k1gInSc4	vliv
oblastí	oblast	k1gFnPc2	oblast
mimo	mimo	k7c4	mimo
hradby	hradba	k1gFnPc4	hradba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
Londýna	Londýn	k1gInSc2	Londýn
jako	jako	k8xC	jako
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropy	Evropa	k1gFnSc2	Evropa
rychle	rychle	k6eAd1	rychle
rostl	růst	k5eAaImAgInS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
malých	malý	k2eAgFnPc2d1	malá
dílen	dílna	k1gFnPc2	dílna
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
platilo	platit	k5eAaImAgNnS	platit
především	především	k9	především
pro	pro	k7c4	pro
tkalcovny	tkalcovna	k1gFnPc4	tkalcovna
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
výměna	výměna	k1gFnSc1	výměna
mezi	mezi	k7c7	mezi
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
Orientem	Orient	k1gInSc7	Orient
a	a	k8xC	a
Amerikou	Amerika	k1gFnSc7	Amerika
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
období	období	k1gNnSc1	období
vzniku	vznik	k1gInSc2	vznik
monopolních	monopolní	k2eAgFnPc2d1	monopolní
obchodních	obchodní	k2eAgFnPc2d1	obchodní
společností	společnost	k1gFnPc2	společnost
jako	jako	k8xS	jako
například	například	k6eAd1	například
Russia	Russia	k1gFnSc1	Russia
Company	Compana	k1gFnSc2	Compana
(	(	kIx(	(
<g/>
1555	[number]	k4	1555
<g/>
)	)	kIx)	)
a	a	k8xC	a
britské	britský	k2eAgFnSc2d1	britská
Východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
East	East	k1gInSc1	East
India	indium	k1gNnSc2	indium
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
1600	[number]	k4	1600
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
většinu	většina	k1gFnSc4	většina
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgFnPc2d1	klíčová
institucí	instituce	k1gFnPc2	instituce
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgNnPc2	dva
a	a	k8xC	a
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1572	[number]	k4	1572
Španělé	Španěl	k1gMnPc1	Španěl
zničili	zničit	k5eAaPmAgMnP	zničit
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
Antverpy	Antverpy	k1gFnPc1	Antverpy
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
postavila	postavit	k5eAaPmAgFnS	postavit
Londýn	Londýn	k1gInSc4	Londýn
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
obchodními	obchodní	k2eAgInPc7d1	obchodní
přístavy	přístav	k1gInPc7	přístav
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Walesu	Wales	k1gInSc2	Wales
ale	ale	k8xC	ale
i	i	k9	i
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
například	například	k6eAd1	například
Hugenoti	hugenot	k1gMnPc1	hugenot
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vznikaly	vznikat	k5eAaImAgFnP	vznikat
první	první	k4xOgFnPc1	první
mapy	mapa	k1gFnPc1	mapa
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
City	City	k1gFnSc2	City
s	s	k7c7	s
hustotou	hustota	k1gFnSc7	hustota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
pro	pro	k7c4	pro
rozvinutou	rozvinutý	k2eAgFnSc4d1	rozvinutá
společnost	společnost	k1gFnSc4	společnost
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
hlavní	hlavní	k2eAgFnSc1d1	hlavní
spojnice	spojnice	k1gFnSc1	spojnice
City	City	k1gFnSc2	City
s	s	k7c7	s
královským	královský	k2eAgInSc7d1	královský
dvorem	dvůr	k1gInSc7	dvůr
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
–	–	k?	–
Strand	strand	k1gInSc1	strand
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
lemována	lemovat	k5eAaImNgFnS	lemovat
honosnými	honosný	k2eAgInPc7d1	honosný
šlechtickými	šlechtický	k2eAgInPc7d1	šlechtický
sídly	sídlo	k1gNnPc7	sídlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
byly	být	k5eAaImAgFnP	být
odděleny	oddělit	k5eAaPmNgFnP	oddělit
a	a	k8xC	a
Westminster	Westminster	k1gInSc1	Westminster
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
City	City	k1gFnSc7	City
jen	jen	k6eAd1	jen
malou	malý	k2eAgFnSc7d1	malá
enklávou	enkláva	k1gFnSc7	enkláva
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
dopravní	dopravní	k2eAgFnSc7d1	dopravní
tepnou	tepna	k1gFnSc7	tepna
nebyla	být	k5eNaImAgFnS	být
Strand	strand	k1gInSc4	strand
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řeka	řeka	k1gFnSc1	řeka
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
součástí	součást	k1gFnSc7	součást
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
venkovský	venkovský	k2eAgInSc4d1	venkovský
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Covent	Covent	k1gInSc4	Covent
Garden	Gardna	k1gFnPc2	Gardna
byla	být	k5eAaImAgFnS	být
místem	místo	k1gNnSc7	místo
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
trhů	trh	k1gInPc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Nemocnice	nemocnice	k1gFnSc1	nemocnice
a	a	k8xC	a
domy	dům	k1gInPc1	dům
pro	pro	k7c4	pro
rekonvalescenty	rekonvalescent	k1gMnPc4	rekonvalescent
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
v	v	k7c6	v
Holbornu	Holborn	k1gInSc6	Holborn
a	a	k8xC	a
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
vzduch	vzduch	k1gInSc1	vzduch
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Islington	Islington	k1gInSc4	Islington
a	a	k8xC	a
Hoxton	Hoxton	k1gInSc4	Hoxton
byly	být	k5eAaImAgFnP	být
odlehlé	odlehlý	k2eAgFnPc1d1	odlehlá
vesnice	vesnice	k1gFnPc1	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1561	[number]	k4	1561
poškodil	poškodit	k5eAaPmAgInS	poškodit
úder	úder	k1gInSc4	úder
blesku	blesk	k1gInSc2	blesk
původní	původní	k2eAgFnSc4d1	původní
katedrálu	katedrála	k1gFnSc4	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Střecha	střecha	k1gFnSc1	střecha
byla	být	k5eAaImAgFnS	být
opravena	opravit	k5eAaPmNgFnS	opravit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
špička	špička	k1gFnSc1	špička
věže	věž	k1gFnSc2	věž
nebyla	být	k5eNaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1565	[number]	k4	1565
založil	založit	k5eAaPmAgMnS	založit
Thomas	Thomas	k1gMnSc1	Thomas
Green	Green	k1gInSc4	Green
v	v	k7c6	v
City	city	k1gNnSc6	city
obchodní	obchodní	k2eAgFnSc4d1	obchodní
burzu	burza	k1gFnSc4	burza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
od	od	k7c2	od
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
titul	titul	k1gInSc4	titul
Královská	královský	k2eAgFnSc1d1	královská
burza	burza	k1gFnSc1	burza
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
společníky	společník	k1gMnPc7	společník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgNnPc2d3	nejslavnější
kulturních	kulturní	k2eAgNnPc2d1	kulturní
období	období	k1gNnPc2	období
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Existoval	existovat	k5eAaImAgMnS	existovat
však	však	k9	však
značný	značný	k2eAgInSc4d1	značný
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
budování	budování	k1gNnSc3	budování
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgFnSc2d1	veřejná
produkce	produkce	k1gFnSc2	produkce
vytvářely	vytvářet	k5eAaImAgInP	vytvářet
davy	dav	k1gInPc1	dav
a	a	k8xC	a
těch	ten	k3xDgFnPc2	ten
se	se	k3xPyFc4	se
obávaly	obávat	k5eAaImAgFnP	obávat
autority	autorita	k1gFnPc1	autorita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
srocování	srocování	k1gNnSc6	srocování
hrozilo	hrozit	k5eAaImAgNnS	hrozit
větší	veliký	k2eAgNnSc1d2	veliký
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
šíření	šíření	k1gNnSc2	šíření
moru	mor	k1gInSc2	mor
<g/>
.	.	kIx.	.
</s>
<s>
Divadla	divadlo	k1gNnSc2	divadlo
se	se	k3xPyFc4	se
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
vzrůstajícího	vzrůstající	k2eAgInSc2d1	vzrůstající
vlivu	vliv	k1gInSc2	vliv
puritánů	puritán	k1gMnPc2	puritán
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
ráda	rád	k2eAgFnSc1d1	ráda
sledovala	sledovat	k5eAaImAgFnS	sledovat
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
a	a	k8xC	a
svolila	svolit	k5eAaPmAgFnS	svolit
k	k	k7c3	k
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
předvádění	předvádění	k1gNnSc3	předvádění
"	"	kIx"	"
<g/>
takových	takový	k3xDgFnPc2	takový
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přinášejí	přinášet	k5eAaImIp3nP	přinášet
důstojné	důstojný	k2eAgNnSc4d1	důstojné
obveselení	obveselení	k1gNnSc4	obveselení
a	a	k8xC	a
ne	ne	k9	ne
příklady	příklad	k1gInPc1	příklad
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Divadla	divadlo	k1gNnSc2	divadlo
byla	být	k5eAaImAgFnS	být
budována	budovat	k5eAaImNgFnS	budovat
hlavně	hlavně	k9	hlavně
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vytvářely	vytvářet	k5eAaImAgInP	vytvářet
centra	centrum	k1gNnPc1	centrum
zábavy	zábava	k1gFnSc2	zábava
a	a	k8xC	a
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
například	například	k6eAd1	například
medvědí	medvědí	k2eAgInPc4d1	medvědí
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
divadla	divadlo	k1gNnSc2	divadlo
Globe	globus	k1gInSc5	globus
<g/>
,	,	kIx,	,
Rose	Rosa	k1gFnSc6	Rosa
a	a	k8xC	a
Swan	Swan	k1gInSc4	Swan
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Theatre	Theatr	k1gInSc5	Theatr
a	a	k8xC	a
Curtain	Curtaina	k1gFnPc2	Curtaina
stály	stát	k5eAaImAgFnP	stát
u	u	k7c2	u
východních	východní	k2eAgFnPc2d1	východní
hradeb	hradba	k1gFnPc2	hradba
City	city	k1gNnSc1	city
a	a	k8xC	a
Blackfairs	Blackfairs	k1gInSc1	Blackfairs
uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
vlády	vláda	k1gFnSc2	vláda
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
si	se	k3xPyFc3	se
někteří	některý	k3yIgMnPc1	některý
její	její	k3xOp3gMnPc1	její
dvořané	dvořan	k1gMnPc1	dvořan
a	a	k8xC	a
bohatí	bohatý	k2eAgMnPc1d1	bohatý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Londýna	Londýn	k1gInSc2	Londýn
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
venkovská	venkovský	k2eAgNnPc4d1	venkovské
sídla	sídlo	k1gNnPc4	sídlo
v	v	k7c6	v
Middlesexu	Middlesex	k1gInSc6	Middlesex
<g/>
,	,	kIx,	,
Essexu	Essex	k1gInSc6	Essex
a	a	k8xC	a
Surrey	Surrea	k1gFnSc2	Surrea
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
příznak	příznak	k1gInSc1	příznak
expanze	expanze	k1gFnSc2	expanze
osídlení	osídlení	k1gNnSc2	osídlení
města	město	k1gNnSc2	město
mimo	mimo	k7c4	mimo
hradby	hradba	k1gFnPc4	hradba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
době	doba	k1gFnSc6	doba
smrti	smrt	k1gFnSc2	smrt
posledního	poslední	k2eAgMnSc2d1	poslední
panovníka	panovník	k1gMnSc2	panovník
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Tudorů	tudor	k1gInPc2	tudor
byl	být	k5eAaImAgInS	být
Londýn	Londýn	k1gInSc1	Londýn
velmi	velmi	k6eAd1	velmi
kompaktním	kompaktní	k2eAgInSc7d1	kompaktní
<g/>
.	.	kIx.	.
</s>
<s>
Rozpínání	rozpínání	k1gNnSc1	rozpínání
Londýna	Londýn	k1gInSc2	Londýn
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
City	City	k1gFnPc2	City
se	se	k3xPyFc4	se
zrychlilo	zrychlit	k5eAaPmAgNnS	zrychlit
během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
tohoto	tento	k3xDgNnSc2	tento
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
bezprostřední	bezprostřední	k2eAgNnSc4d1	bezprostřední
okolí	okolí	k1gNnSc4	okolí
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
šlechtických	šlechtický	k2eAgNnPc2d1	šlechtické
sídel	sídlo	k1gNnPc2	sídlo
budovaných	budovaný	k2eAgFnPc2d1	budovaná
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Westminsteru	Westminster	k1gInSc3	Westminster
<g/>
,	,	kIx,	,
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
pro	pro	k7c4	pro
obydlení	obydlení	k1gNnSc4	obydlení
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
se	se	k3xPyFc4	se
rozkládaly	rozkládat	k5eAaImAgFnP	rozkládat
slatiny	slatina	k1gFnPc1	slatina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
nedávno	nedávno	k6eAd1	nedávno
odvodněny	odvodněn	k2eAgFnPc1d1	odvodněn
a	a	k8xC	a
nebyly	být	k5eNaImAgFnP	být
ještě	ještě	k6eAd1	ještě
rozparcelované	rozparcelovaný	k2eAgFnPc1d1	rozparcelovaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
plné	plný	k2eAgInPc1d1	plný
žebráků	žebrák	k1gMnPc2	žebrák
a	a	k8xC	a
poutníků	poutník	k1gMnPc2	poutník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
směřovali	směřovat	k5eAaImAgMnP	směřovat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
byly	být	k5eAaImAgFnP	být
Finsbury	Finsbura	k1gFnPc1	Finsbura
Fields	Fields	k1gInSc1	Fields
–	–	k?	–
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
místo	místo	k7c2	místo
cvičení	cvičení	k1gNnSc2	cvičení
lukostřelců	lukostřelec	k1gMnPc2	lukostřelec
<g/>
.	.	kIx.	.
</s>
<s>
Přípravy	příprava	k1gFnPc1	příprava
na	na	k7c6	na
korunovaci	korunovace	k1gFnSc6	korunovace
krále	král	k1gMnSc4	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
byly	být	k5eAaImAgFnP	být
přerušeny	přerušit	k5eAaPmNgFnP	přerušit
několika	několik	k4yIc7	několik
epidemiemi	epidemie	k1gFnPc7	epidemie
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
usmrtily	usmrtit	k5eAaPmAgFnP	usmrtit
asi	asi	k9	asi
30	[number]	k4	30
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
procesí	procesí	k1gNnSc1	procesí
Lord	lord	k1gMnSc1	lord
Mayor	Mayor	k1gMnSc1	Mayor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Show	show	k1gNnSc7	show
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	let	k1gInSc4	let
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
krále	král	k1gMnSc4	král
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1609	[number]	k4	1609
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Tomas	Tomas	k1gInSc1	Tomas
Button	Button	k1gInSc1	Button
koupil	koupit	k5eAaPmAgInS	koupit
zrušený	zrušený	k2eAgInSc1d1	zrušený
klášter	klášter	k1gInSc1	klášter
Charterhouse	Charterhouse	k1gFnSc2	Charterhouse
za	za	k7c4	za
13	[number]	k4	13
000	[number]	k4	000
liber	libra	k1gFnPc2	libra
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1611	[number]	k4	1611
ho	on	k3xPp3gMnSc4	on
začal	začít	k5eAaPmAgInS	začít
přestavovat	přestavovat	k5eAaImF	přestavovat
na	na	k7c6	na
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
kapli	kaple	k1gFnSc6	kaple
a	a	k8xC	a
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
Charterhouse	Charterhouse	k1gFnSc2	Charterhouse
School	Schoola	k1gFnPc2	Schoola
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
veřejných	veřejný	k2eAgFnPc2d1	veřejná
škol	škola	k1gFnPc2	škola
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ve	v	k7c6	v
viktoriánské	viktoriánský	k2eAgFnSc6d1	viktoriánská
době	doba	k1gFnSc6	doba
nepřesídlila	přesídlit	k5eNaPmAgFnS	přesídlit
do	do	k7c2	do
Surrey	Surrea	k1gFnSc2	Surrea
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
budovy	budova	k1gFnPc1	budova
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
používané	používaný	k2eAgInPc1d1	používaný
jako	jako	k8xS	jako
lékařská	lékařský	k2eAgFnSc1d1	lékařská
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
Londýňanů	Londýňan	k1gMnPc2	Londýňan
byla	být	k5eAaImAgFnS	být
kostelní	kostelní	k2eAgFnSc1d1	kostelní
loď	loď	k1gFnSc1	loď
původní	původní	k2eAgFnSc2d1	původní
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Kupci	kupec	k1gMnPc1	kupec
domlouvali	domlouvat	k5eAaImAgMnP	domlouvat
obchody	obchod	k1gInPc4	obchod
v	v	k7c6	v
postranní	postranní	k2eAgFnSc6d1	postranní
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
používajíc	používat	k5eAaImSgFnS	používat
křtitelnici	křtitelnice	k1gFnSc4	křtitelnice
jako	jako	k8xS	jako
přepážku	přepážka	k1gFnSc4	přepážka
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
uskutečňovali	uskutečňovat	k5eAaImAgMnP	uskutečňovat
své	svůj	k3xOyFgFnPc4	svůj
platby	platba	k1gFnPc4	platba
<g/>
,	,	kIx,	,
právníci	právník	k1gMnPc1	právník
se	se	k3xPyFc4	se
setkávali	setkávat	k5eAaImAgMnP	setkávat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
klienty	klient	k1gMnPc7	klient
u	u	k7c2	u
určitých	určitý	k2eAgInPc2d1	určitý
sloupů	sloup	k1gInPc2	sloup
a	a	k8xC	a
nezaměstnaní	nezaměstnaný	k1gMnPc1	nezaměstnaný
zde	zde	k6eAd1	zde
sháněli	shánět	k5eAaImAgMnP	shánět
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc4d1	zdejší
hřbitov	hřbitov	k1gInSc4	hřbitov
byl	být	k5eAaImAgMnS	být
centrem	centrum	k1gNnSc7	centrum
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
knihami	kniha	k1gFnPc7	kniha
a	a	k8xC	a
Fleet	Fleet	k1gInSc4	Fleet
Street	Streeta	k1gFnPc2	Streeta
byla	být	k5eAaImAgFnS	být
místem	místo	k1gNnSc7	místo
veřejné	veřejný	k2eAgFnSc2d1	veřejná
zábavy	zábava	k1gFnSc2	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Divadla	divadlo	k1gNnPc1	divadlo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgNnP	objevovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vlády	vláda	k1gFnSc2	vláda
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
nabývala	nabývat	k5eAaImAgFnS	nabývat
velké	velký	k2eAgFnPc4d1	velká
popularity	popularita	k1gFnPc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnPc1	představení
byla	být	k5eAaImAgNnP	být
doplňována	doplňovat	k5eAaImNgNnP	doplňovat
použitím	použití	k1gNnSc7	použití
pečlivě	pečlivě	k6eAd1	pečlivě
propracovaných	propracovaný	k2eAgFnPc2d1	propracovaná
masek	maska	k1gFnPc2	maska
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1625	[number]	k4	1625
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
aristokracie	aristokracie	k1gFnSc1	aristokracie
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
usazovat	usazovat	k5eAaImF	usazovat
ve	v	k7c4	v
West	West	k1gInSc4	West
Endu	Endus	k1gInSc2	Endus
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
ty	ten	k3xDgInPc4	ten
kdo	kdo	k3yRnSc1	kdo
měli	mít	k5eAaImAgMnP	mít
speciální	speciální	k2eAgInPc4d1	speciální
zájmy	zájem	k1gInPc4	zájem
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
i	i	k9	i
velcí	velký	k2eAgMnPc1d1	velký
vlastnící	vlastnící	k2eAgMnPc1d1	vlastnící
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
trávili	trávit	k5eAaImAgMnP	trávit
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
užili	užít	k5eAaPmAgMnP	užít
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Inn	Inn	k1gFnSc7	Inn
Fields	Fieldsa	k1gFnPc2	Fieldsa
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
a	a	k8xC	a
náměstí	náměstí	k1gNnSc4	náměstí
Covent	Covent	k1gMnSc1	Covent
Garden	Gardna	k1gFnPc2	Gardna
<g/>
,	,	kIx,	,
navržené	navržený	k2eAgInPc4d1	navržený
prvním	první	k4xOgMnSc6	první
klasicky	klasicky	k6eAd1	klasicky
vzdělaným	vzdělaný	k2eAgMnSc7d1	vzdělaný
architektem	architekt	k1gMnSc7	architekt
Inigo	Inigo	k1gMnSc1	Inigo
Jonesem	Jones	k1gMnSc7	Jones
následovaly	následovat	k5eAaImAgInP	následovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1632	[number]	k4	1632
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgFnPc1d1	okolní
ulice	ulice	k1gFnPc1	ulice
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgFnP	vybudovat
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
po	po	k7c6	po
členech	člen	k1gInPc6	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
členů	člen	k1gInPc2	člen
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
král	král	k1gMnSc1	král
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1642	[number]	k4	1642
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
,	,	kIx,	,
našlo	najít	k5eAaPmAgNnS	najít
útočiště	útočiště	k1gNnSc1	útočiště
v	v	k7c6	v
City	city	k1gNnSc6	city
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
král	král	k1gMnSc1	král
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
u	u	k7c2	u
Nottinghamu	Nottingham	k1gInSc2	Nottingham
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Anglické	anglický	k2eAgFnSc2d1	anglická
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
stál	stát	k5eAaImAgInS	stát
Londýn	Londýn	k1gInSc1	Londýn
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
měl	mít	k5eAaImAgMnS	mít
král	král	k1gMnSc1	král
navrch	navrch	k6eAd1	navrch
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
bitvu	bitva	k1gFnSc4	bitva
Brentfordu	Brentforda	k1gFnSc4	Brentforda
<g/>
,	,	kIx,	,
kousek	kousek	k1gInSc4	kousek
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
City	City	k1gFnSc1	City
sestavila	sestavit	k5eAaPmAgFnS	sestavit
prozatímní	prozatímní	k2eAgNnSc4d1	prozatímní
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
zalekl	zaleknout	k5eAaPmAgMnS	zaleknout
a	a	k8xC	a
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
systém	systém	k1gInSc1	systém
opevnění	opevnění	k1gNnSc2	opevnění
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
Londýna	Londýn	k1gInSc2	Londýn
proti	proti	k7c3	proti
dalšímu	další	k2eAgInSc3d1	další
útoku	útok	k1gInSc3	útok
royalistů	royalista	k1gMnPc2	royalista
<g/>
.	.	kIx.	.
</s>
<s>
Obranný	obranný	k2eAgInSc1d1	obranný
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
obrovským	obrovský	k2eAgInSc7d1	obrovský
hliněným	hliněný	k2eAgInSc7d1	hliněný
valem	val	k1gInSc7	val
<g/>
,	,	kIx,	,
doplněným	doplněný	k2eAgInSc7d1	doplněný
baštami	bašta	k1gFnPc7	bašta
a	a	k8xC	a
pevnůstkami	pevnůstka	k1gFnPc7	pevnůstka
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
u	u	k7c2	u
hradeb	hradba	k1gFnPc2	hradba
City	City	k1gFnSc2	City
a	a	k8xC	a
obklopoval	obklopovat	k5eAaImAgInS	obklopovat
celou	celý	k2eAgFnSc4d1	celá
obydlenou	obydlený	k2eAgFnSc4d1	obydlená
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Westminsteru	Westminster	k1gInSc2	Westminster
a	a	k8xC	a
Southwarku	Southwark	k1gInSc2	Southwark
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
nebyl	být	k5eNaImAgInS	být
royalisty	royalista	k1gMnPc4	royalista
znovu	znovu	k6eAd1	znovu
napaden	napaden	k2eAgMnSc1d1	napaden
a	a	k8xC	a
finanční	finanční	k2eAgInPc1d1	finanční
prostředky	prostředek	k1gInPc1	prostředek
City	City	k1gFnSc2	City
podstatně	podstatně	k6eAd1	podstatně
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
zástupců	zástupce	k1gMnPc2	zástupce
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Nezdravý	zdravý	k2eNgInSc1d1	nezdravý
a	a	k8xC	a
přeplněný	přeplněný	k2eAgInSc1d1	přeplněný
Londýn	Londýn	k1gInSc1	Londýn
strádal	strádat	k5eAaImAgInS	strádat
častými	častý	k2eAgFnPc7d1	častá
výskyty	výskyt	k1gInPc4	výskyt
morových	morový	k2eAgFnPc2d1	morová
nákaz	nákaza	k1gFnPc2	nákaza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
největší	veliký	k2eAgFnSc7d3	veliký
pohromou	pohroma	k1gFnSc7	pohroma
je	být	k5eAaImIp3nS	být
událost	událost	k1gFnSc1	událost
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
Velký	velký	k2eAgInSc1d1	velký
mor	mor	k1gInSc1	mor
(	(	kIx(	(
<g/>
Great	Great	k2eAgInSc1d1	Great
Plague	Plague	k1gInSc1	Plague
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postihla	postihnout	k5eAaPmAgFnS	postihnout
Londýn	Londýn	k1gInSc4	Londýn
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1665	[number]	k4	1665
a	a	k8xC	a
1666	[number]	k4	1666
a	a	k8xC	a
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
asi	asi	k9	asi
60	[number]	k4	60
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
pětina	pětina	k1gFnSc1	pětina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
mor	mor	k1gInSc1	mor
byl	být	k5eAaImAgInS	být
bezprostředně	bezprostředně	k6eAd1	bezprostředně
následován	následovat	k5eAaImNgInS	následovat
další	další	k2eAgFnSc7d1	další
pohromou	pohroma	k1gFnSc7	pohroma
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jejím	její	k3xOp3gInSc7	její
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
ukončení	ukončení	k1gNnSc1	ukončení
výskytu	výskyt	k1gInSc2	výskyt
velkých	velký	k2eAgFnPc2d1	velká
morových	morový	k2eAgFnPc2d1	morová
nákaz	nákaza	k1gFnPc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1666	[number]	k4	1666
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Pudding	Pudding	k1gInSc1	Pudding
Lane	Lane	k1gFnPc2	Lane
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Londýna	Londýn	k1gInSc2	Londýn
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
šířit	šířit	k5eAaImF	šířit
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
počátkem	počátkem	k7c2	počátkem
Velkého	velký	k2eAgInSc2d1	velký
požáru	požár	k1gInSc2	požár
Londýna	Londýn	k1gInSc2	Londýn
(	(	kIx(	(
<g/>
Great	Great	k1gInSc1	Great
Fire	Fir	k1gFnSc2	Fir
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podporován	podporován	k2eAgInSc1d1	podporován
východním	východní	k2eAgInSc7d1	východní
větrem	vítr	k1gInSc7	vítr
se	se	k3xPyFc4	se
požár	požár	k1gInSc1	požár
začal	začít	k5eAaPmAgInS	začít
šířit	šířit	k5eAaImF	šířit
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
počáteční	počáteční	k2eAgFnPc1d1	počáteční
snahy	snaha	k1gFnPc1	snaha
zabránit	zabránit	k5eAaPmF	zabránit
jeho	jeho	k3xOp3gNnSc4	jeho
šíření	šíření	k1gNnSc4	šíření
zbořením	zboření	k1gNnSc7	zboření
pásu	pás	k1gInSc2	pás
domů	dům	k1gInPc2	dům
jako	jako	k8xS	jako
požární	požární	k2eAgFnPc1d1	požární
zábrany	zábrana	k1gFnPc1	zábrana
byly	být	k5eAaImAgFnP	být
neorganizované	organizovaný	k2eNgFnPc1d1	neorganizovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
vítr	vítr	k1gInSc1	vítr
začal	začít	k5eAaPmAgInS	začít
polevovat	polevovat	k5eAaImF	polevovat
a	a	k8xC	a
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
požár	požár	k1gInSc1	požár
začal	začít	k5eAaPmAgInS	začít
slábnout	slábnout	k5eAaImF	slábnout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
dopoledne	dopoledne	k6eAd1	dopoledne
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
skoro	skoro	k6eAd1	skoro
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navečer	navečer	k1gInSc4	navečer
plameny	plamen	k1gInPc1	plamen
znovu	znovu	k6eAd1	znovu
vyšlehly	vyšlehnout	k5eAaPmAgInP	vyšlehnout
u	u	k7c2	u
Temple	templ	k1gInSc5	templ
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
odstřeleny	odstřelit	k5eAaPmNgInP	odstřelit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
oheň	oheň	k1gInSc4	oheň
překonán	překonán	k2eAgInSc1d1	překonán
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
zničil	zničit	k5eAaPmAgInS	zničit
asi	asi	k9	asi
60	[number]	k4	60
procent	procento	k1gNnPc2	procento
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
původní	původní	k2eAgFnSc2d1	původní
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
87	[number]	k4	87
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
44	[number]	k4	44
cechovních	cechovní	k2eAgNnPc2d1	cechovní
sídel	sídlo	k1gNnPc2	sídlo
a	a	k8xC	a
královskou	královský	k2eAgFnSc4d1	královská
burzu	burza	k1gFnSc4	burza
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ztráty	ztráta	k1gFnPc1	ztráta
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
byly	být	k5eAaImAgFnP	být
překvapivě	překvapivě	k6eAd1	překvapivě
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
maximálně	maximálně	k6eAd1	maximálně
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
byly	být	k5eAaImAgFnP	být
králi	král	k1gMnSc6	král
předloženy	předložit	k5eAaPmNgFnP	předložit
architekty	architekt	k1gMnPc7	architekt
Christopherem	Christopher	k1gMnSc7	Christopher
Wrenem	Wren	k1gMnSc7	Wren
<g/>
,	,	kIx,	,
Johnem	John	k1gMnSc7	John
Evelynem	Evelyn	k1gMnSc7	Evelyn
a	a	k8xC	a
Robertem	Robert	k1gMnSc7	Robert
Hookem	Hooek	k1gMnSc7	Hooek
plány	plán	k1gInPc4	plán
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Wren	Wren	k1gMnSc1	Wren
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
vybudovat	vybudovat	k5eAaPmF	vybudovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
třídy	třída	k1gFnPc4	třída
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
sever-jih	severih	k1gInSc4	sever-jih
a	a	k8xC	a
východ-západ	východápad	k1gInSc4	východ-západ
<g/>
,	,	kIx,	,
postavit	postavit	k5eAaPmF	postavit
kostely	kostel	k1gInPc1	kostel
na	na	k7c6	na
izolovaných	izolovaný	k2eAgNnPc6d1	izolované
<g/>
,	,	kIx,	,
významných	významný	k2eAgNnPc6d1	významné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
soustředit	soustředit	k5eAaPmF	soustředit
veřejný	veřejný	k2eAgInSc4d1	veřejný
život	život	k1gInSc4	život
na	na	k7c6	na
nově	nově	k6eAd1	nově
postavených	postavený	k2eAgNnPc6d1	postavené
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
soustředit	soustředit	k5eAaPmF	soustředit
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
cechovní	cechovní	k2eAgNnPc4d1	cechovní
sídla	sídlo	k1gNnPc4	sídlo
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
skupiny	skupina	k1gFnSc2	skupina
budov	budova	k1gFnPc2	budova
připojených	připojený	k2eAgFnPc2d1	připojená
ke	k	k7c3	k
Guildhall	Guildhall	k1gInSc4	Guildhall
a	a	k8xC	a
postavit	postavit	k5eAaPmF	postavit
krásné	krásný	k2eAgNnSc4d1	krásné
nábřeží	nábřeží	k1gNnSc4	nábřeží
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
od	od	k7c2	od
Blackfairs	Blackfairsa	k1gFnPc2	Blackfairsa
až	až	k9	až
po	po	k7c4	po
Tower	Tower	k1gInSc4	Tower
<g/>
.	.	kIx.	.
</s>
<s>
Wren	Wren	k1gMnSc1	Wren
chtěl	chtít	k5eAaImAgMnS	chtít
vybudovat	vybudovat	k5eAaPmF	vybudovat
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
ulic	ulice	k1gFnPc2	ulice
s	s	k7c7	s
odstupňovanou	odstupňovaný	k2eAgFnSc7d1	odstupňovaná
šířkou	šířka	k1gFnSc7	šířka
–	–	k?	–
30	[number]	k4	30
<g/>
,	,	kIx,	,
60	[number]	k4	60
a	a	k8xC	a
90	[number]	k4	90
stop	stopa	k1gFnPc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Evelynovy	Evelynův	k2eAgInPc1d1	Evelynův
plány	plán	k1gInPc1	plán
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgInP	lišit
od	od	k7c2	od
Wrenových	Wrenová	k1gFnPc2	Wrenová
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
spojit	spojit	k5eAaPmF	spojit
ulicí	ulice	k1gFnSc7	ulice
St	St	kA	St
Dunstan	Dunstan	k1gInSc1	Dunstan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
na	na	k7c6	na
východě	východ	k1gInSc6	východ
až	až	k9	až
po	po	k7c4	po
katedrálu	katedrála	k1gFnSc4	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenavrhoval	navrhovat	k5eNaImAgMnS	navrhovat
vybudování	vybudování	k1gNnSc4	vybudování
nábřeží	nábřeží	k1gNnSc2	nábřeží
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
plány	plán	k1gInPc1	plán
nebyly	být	k5eNaImAgInP	být
využity	využít	k5eAaPmNgInP	využít
a	a	k8xC	a
přestavba	přestavba	k1gFnSc1	přestavba
zachovala	zachovat	k5eAaPmAgFnS	zachovat
původní	původní	k2eAgInSc4d1	původní
systém	systém	k1gInSc4	systém
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgMnS	dochovat
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
obnovené	obnovený	k2eAgNnSc1d1	obnovené
město	město	k1gNnSc1	město
lišilo	lišit	k5eAaImAgNnS	lišit
od	od	k7c2	od
starého	staré	k1gNnSc2	staré
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
šlechtických	šlechtický	k2eAgFnPc2d1	šlechtická
rezidencí	rezidence	k1gFnPc2	rezidence
nebylo	být	k5eNaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gMnPc1	jejich
majitelé	majitel	k1gMnPc1	majitel
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
postavit	postavit	k5eAaPmF	postavit
své	svůj	k3xOyFgInPc4	svůj
domy	dům	k1gInPc4	dům
ve	v	k7c4	v
West	West	k1gInSc4	West
Endu	Endus	k1gInSc2	Endus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
nové	nový	k2eAgInPc1d1	nový
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
St.	st.	kA	st.
James	Jamesa	k1gFnPc2	Jamesa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
blíže	blíž	k1gFnSc2	blíž
královského	královský	k2eAgNnSc2d1	královské
sídla	sídlo	k1gNnSc2	sídlo
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
než	než	k8xS	než
ho	on	k3xPp3gMnSc2	on
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zničil	zničit	k5eAaPmAgInS	zničit
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
Whitehall	Whitehall	k1gInSc1	Whitehall
Palace	Palace	k1gFnSc2	Palace
<g/>
.	.	kIx.	.
</s>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
střední	střední	k2eAgFnSc2d1	střední
obchodní	obchodní	k2eAgFnSc2d1	obchodní
společenské	společenský	k2eAgFnSc2d1	společenská
třídy	třída	k1gFnSc2	třída
soustředěné	soustředěný	k2eAgFnSc2d1	soustředěná
v	v	k7c6	v
City	City	k1gFnSc6	City
a	a	k8xC	a
světa	svět	k1gInSc2	svět
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
City	City	k1gFnSc6	City
se	se	k3xPyFc4	se
domy	dům	k1gInPc1	dům
přestaly	přestat	k5eAaPmAgInP	přestat
stavět	stavět	k5eAaImF	stavět
z	z	k7c2	z
hořlavých	hořlavý	k2eAgInPc2d1	hořlavý
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
stavbu	stavba	k1gFnSc4	stavba
bylo	být	k5eAaImAgNnS	být
použito	použit	k2eAgNnSc1d1	použito
kamene	kámen	k1gInSc2	kámen
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
snížilo	snížit	k5eAaPmAgNnS	snížit
riziko	riziko	k1gNnSc1	riziko
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
Christopera	Christoper	k1gMnSc2	Christoper
Wrena	Wren	k1gMnSc2	Wren
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
Londýna	Londýn	k1gInSc2	Londýn
nebyly	být	k5eNaImAgFnP	být
využity	využít	k5eAaPmNgFnP	využít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
obnovou	obnova	k1gFnSc7	obnova
spálených	spálený	k2eAgInPc2d1	spálený
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kopulovitá	kopulovitý	k2eAgFnSc1d1	kopulovitá
barokní	barokní	k2eAgFnSc1d1	barokní
kupole	kupole	k1gFnSc1	kupole
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
symbolem	symbol	k1gInSc7	symbol
Londýna	Londýn	k1gInSc2	Londýn
jedno	jeden	k4xCgNnSc1	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Obnovu	obnova	k1gFnSc4	obnova
městské	městský	k2eAgFnSc2d1	městská
zástavby	zástavba	k1gFnSc2	zástavba
City	City	k1gFnSc2	City
prováděl	provádět	k5eAaImAgMnS	provádět
Robert	Robert	k1gMnSc1	Robert
Hooke	Hook	k1gInSc2	Hook
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
po	po	k7c6	po
Velkém	velký	k2eAgInSc6d1	velký
požáru	požár	k1gInSc6	požár
se	s	k7c7	s
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
usídlilo	usídlit	k5eAaPmAgNnS	usídlit
v	v	k7c6	v
území	území	k1gNnSc6	území
East	East	k1gInSc4	East
Endu	Endus	k1gInSc2	Endus
<g/>
,	,	kIx,	,
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Londýnské	londýnský	k2eAgInPc1d1	londýnský
doky	dok	k1gInPc1	dok
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přilákalo	přilákat	k5eAaPmAgNnS	přilákat
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
v	v	k7c6	v
samotných	samotný	k2eAgInPc6d1	samotný
docích	dok	k1gInPc6	dok
nebo	nebo	k8xC	nebo
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
osidlovali	osidlovat	k5eAaImAgMnP	osidlovat
oblasti	oblast	k1gFnPc4	oblast
Whitechapel	Whitechapel	k1gInSc4	Whitechapel
<g/>
,	,	kIx,	,
Wapping	Wapping	k1gInSc4	Wapping
<g/>
,	,	kIx,	,
Stepney	Stepney	k1gInPc4	Stepney
a	a	k8xC	a
Limehouse	Limehouse	k1gFnPc4	Limehouse
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc1	jejich
životní	životní	k2eAgFnPc1d1	životní
podmínky	podmínka	k1gFnPc1	podmínka
byly	být	k5eAaImAgFnP	být
ubohé	ubohý	k2eAgFnPc1d1	ubohá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Londýn	Londýn	k1gInSc1	Londýn
předním	přední	k2eAgMnSc7d1	přední
světovým	světový	k2eAgMnSc7d1	světový
finančním	finanční	k2eAgMnSc7d1	finanční
centrem	centr	k1gMnSc7	centr
<g/>
,	,	kIx,	,
když	když	k8xS	když
překonal	překonat	k5eAaPmAgMnS	překonat
Amsterodam	Amsterodam	k1gInSc4	Amsterodam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1694	[number]	k4	1694
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
Bank	bank	k1gInSc1	bank
of	of	k?	of
England	England	k1gInSc1	England
a	a	k8xC	a
britská	britský	k2eAgFnSc1d1	britská
Východoindická	východoindický	k2eAgFnSc1d1	Východoindická
společnost	společnost	k1gFnSc1	společnost
výrazné	výrazný	k2eAgFnSc2d1	výrazná
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
fungovat	fungovat	k5eAaImF	fungovat
Lloyd	Lloyd	k1gMnSc1	Lloyd
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1700	[number]	k4	1700
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
uskutečňovalo	uskutečňovat	k5eAaImAgNnS	uskutečňovat
80	[number]	k4	80
%	%	kIx~	%
anglického	anglický	k2eAgInSc2d1	anglický
dovozu	dovoz	k1gInSc2	dovoz
<g/>
,	,	kIx,	,
69	[number]	k4	69
%	%	kIx~	%
vývozu	vývoz	k1gInSc2	vývoz
a	a	k8xC	a
86	[number]	k4	86
reexportu	reexport	k1gInSc2	reexport
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
tvořilo	tvořit	k5eAaImAgNnS	tvořit
luxusní	luxusní	k2eAgNnSc1d1	luxusní
zboží	zboží	k1gNnSc1	zboží
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
jako	jako	k8xC	jako
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc4	čaj
a	a	k8xC	a
tabák	tabák	k1gInSc4	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
položka	položka	k1gFnSc1	položka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Londýn	Londýn	k1gInSc1	Londýn
jako	jako	k8xS	jako
důležité	důležitý	k2eAgNnSc1d1	důležité
překladiště	překladiště	k1gNnSc1	překladiště
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
mnoho	mnoho	k6eAd1	mnoho
řemeslníků	řemeslník	k1gMnPc2	řemeslník
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
síla	síla	k1gFnSc1	síla
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
primárně	primárně	k6eAd1	primárně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgNnSc7d1	významné
obchodním	obchodní	k2eAgNnSc7d1	obchodní
a	a	k8xC	a
distribučním	distribuční	k2eAgNnSc7d1	distribuční
střediskem	středisko	k1gNnSc7	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Zboží	zboží	k1gNnSc1	zboží
bylo	být	k5eAaImAgNnS	být
dováženo	dovážet	k5eAaImNgNnS	dovážet
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
rozrůstajícím	rozrůstající	k2eAgInSc7d1	rozrůstající
se	se	k3xPyFc4	se
anglickým	anglický	k2eAgNnSc7d1	anglické
obchodním	obchodní	k2eAgNnSc7d1	obchodní
loďstvem	loďstvo	k1gNnSc7	loďstvo
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
reexport	reexport	k1gInSc4	reexport
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Oranžský	oranžský	k2eAgMnSc1d1	oranžský
se	se	k3xPyFc4	se
o	o	k7c4	o
Londýn	Londýn	k1gInSc4	Londýn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
kouř	kouř	k1gInSc4	kouř
mu	on	k3xPp3gMnSc3	on
působil	působit	k5eAaImAgMnS	působit
záchvaty	záchvat	k1gInPc1	záchvat
astma	astma	k1gNnSc1	astma
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
nestaral	starat	k5eNaImAgMnS	starat
a	a	k8xC	a
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
Whitehall	Whitehall	k1gInSc1	Whitehall
Palace	Palace	k1gFnSc2	Palace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1691	[number]	k4	1691
koupil	koupit	k5eAaPmAgInS	koupit
Nottingham	Nottingham	k1gInSc1	Nottingham
House	house	k1gNnSc1	house
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
ho	on	k3xPp3gMnSc4	on
přestavět	přestavět	k5eAaPmF	přestavět
na	na	k7c4	na
Kensington	Kensington	k1gInSc4	Kensington
Pallace	Pallace	k1gFnSc2	Pallace
<g/>
.	.	kIx.	.
</s>
<s>
Kensington	Kensington	k1gInSc4	Kensington
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bezvýznamná	bezvýznamný	k2eAgFnSc1d1	bezvýznamná
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přestěhování	přestěhování	k1gNnSc1	přestěhování
dvora	dvůr	k1gInSc2	dvůr
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
růst	růst	k1gInSc4	růst
jejího	její	k3xOp3gInSc2	její
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc4d1	založen
Greenwich	Greenwich	k1gInSc4	Greenwich
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
,	,	kIx,	,
obdoba	obdoba	k1gFnSc1	obdoba
Chelsea	Chelsea	k1gMnSc1	Chelsea
Hospital	Hospital	k1gMnSc1	Hospital
–	–	k?	–
založen	založen	k2eAgMnSc1d1	založen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1681	[number]	k4	1681
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
námořníky	námořník	k1gMnPc4	námořník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
výnos	výnos	k1gInSc1	výnos
přikazující	přikazující	k2eAgInSc1d1	přikazující
postavit	postavit	k5eAaPmF	postavit
50	[number]	k4	50
nových	nový	k2eAgInPc2d1	nový
kostelů	kostel	k1gInPc2	kostel
pro	pro	k7c4	pro
rychle	rychle	k6eAd1	rychle
narůstající	narůstající	k2eAgFnSc4d1	narůstající
populaci	populace	k1gFnSc4	populace
žijící	žijící	k2eAgFnSc4d1	žijící
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Století	století	k1gNnSc1	století
18	[number]	k4	18
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
obdobím	období	k1gNnPc3	období
rychlého	rychlý	k2eAgInSc2d1	rychlý
růstu	růst	k1gInSc2	růst
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
následujícího	následující	k2eAgInSc2d1	následující
růst	růst	k1gInSc4	růst
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
počátečních	počáteční	k2eAgInPc2d1	počáteční
projevů	projev	k1gInPc2	projev
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
stoupající	stoupající	k2eAgFnSc1d1	stoupající
role	role	k1gFnSc1	role
Londýna	Londýn	k1gInSc2	Londýn
jako	jako	k8xC	jako
centra	centrum	k1gNnSc2	centrum
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
expandoval	expandovat	k5eAaImAgInS	expandovat
Londýn	Londýn	k1gInSc1	Londýn
za	za	k7c4	za
původní	původní	k2eAgFnPc4d1	původní
hranice	hranice	k1gFnPc4	hranice
zrychleným	zrychlený	k2eAgNnSc7d1	zrychlené
tempem	tempo	k1gNnSc7	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
majetné	majetný	k2eAgFnPc1d1	majetná
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
nové	nový	k2eAgFnPc1d1	nová
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Mayfair	Mayfaira	k1gFnPc2	Mayfaira
ve	v	k7c4	v
West	West	k1gInSc4	West
Endu	Endus	k1gInSc2	Endus
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
nové	nový	k2eAgInPc1d1	nový
mosty	most	k1gInPc1	most
přes	přes	k7c4	přes
Temži	Temže	k1gFnSc4	Temže
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
jižního	jižní	k2eAgInSc2d1	jižní
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
East	Easta	k1gFnPc2	Easta
Endu	Endus	k1gInSc2	Endus
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
rozšiřoval	rozšiřovat	k5eAaImAgInS	rozšiřovat
londýnský	londýnský	k2eAgInSc1d1	londýnský
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
staly	stát	k5eAaPmAgFnP	stát
populárními	populární	k2eAgFnPc7d1	populární
kavárny	kavárna	k1gFnPc4	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Rostl	růst	k5eAaImAgInS	růst
počet	počet	k1gInSc1	počet
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
vydávaných	vydávaný	k2eAgFnPc2d1	vydávaná
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnSc1	ulice
Fleet	Fleeta	k1gFnPc2	Fleeta
Street	Streeta	k1gFnPc2	Streeta
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
místem	místo	k1gNnSc7	místo
zrodu	zrod	k1gInSc2	zrod
britského	britský	k2eAgInSc2d1	britský
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
potýkal	potýkat	k5eAaImAgMnS	potýkat
se	s	k7c7	s
zločinem	zločin	k1gInSc7	zločin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1750	[number]	k4	1750
byla	být	k5eAaImAgFnS	být
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
první	první	k4xOgFnSc1	první
profesionální	profesionální	k2eAgFnSc1d1	profesionální
policejní	policejní	k2eAgFnSc1d1	policejní
jednotka	jednotka	k1gFnSc1	jednotka
-	-	kIx~	-
Bow	Bow	k1gFnSc1	Bow
Street	Street	k1gMnSc1	Street
Runnesrs	Runnesrs	k1gInSc1	Runnesrs
<g/>
.	.	kIx.	.
</s>
<s>
Tresty	trest	k1gInPc1	trest
za	za	k7c4	za
zločiny	zločin	k1gInPc4	zločin
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
přísně	přísně	k6eAd1	přísně
a	a	k8xC	a
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
udělován	udělovat	k5eAaImNgInS	udělovat
i	i	k9	i
za	za	k7c4	za
menší	malý	k2eAgFnPc4d2	menší
provinění	provinění	k1gNnPc4	provinění
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgFnPc4d1	veřejná
popravy	poprava	k1gFnPc4	poprava
oběšením	oběšení	k1gNnSc7	oběšení
byly	být	k5eAaImAgFnP	být
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
života	život	k1gInSc2	život
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
otřáslo	otřást	k5eAaPmAgNnS	otřást
Londýnem	Londýn	k1gInSc7	Londýn
Gordon	Gordona	k1gFnPc2	Gordona
Riots	Riotsa	k1gFnPc2	Riotsa
<g/>
,	,	kIx,	,
vzpoura	vzpoura	k1gFnSc1	vzpoura
protestantů	protestant	k1gMnPc2	protestant
proti	proti	k7c3	proti
katolické	katolický	k2eAgFnSc3d1	katolická
emancipaci	emancipace	k1gFnSc3	emancipace
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
lordem	lord	k1gMnSc7	lord
Georgem	Georg	k1gMnSc7	Georg
Gordonem	Gordon	k1gMnSc7	Gordon
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
katolických	katolický	k2eAgInPc2d1	katolický
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
domů	dům	k1gInPc2	dům
bylo	být	k5eAaImAgNnS	být
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
a	a	k8xC	a
285	[number]	k4	285
buřičů	buřič	k1gMnPc2	buřič
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Londýn	Londýn	k1gInSc1	Londýn
stal	stát	k5eAaPmAgInS	stát
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
světa	svět	k1gInSc2	svět
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
z	z	k7c2	z
1	[number]	k4	1
miliónu	milión	k4xCgInSc2	milión
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
na	na	k7c4	na
6	[number]	k4	6
miliónů	milión	k4xCgInPc2	milión
o	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Londýn	Londýn	k1gInSc1	Londýn
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
globálním	globální	k2eAgInSc7d1	globální
politickým	politický	k2eAgInSc7d1	politický
<g/>
,	,	kIx,	,
finančním	finanční	k2eAgInSc7d1	finanční
a	a	k8xC	a
obchodním	obchodní	k2eAgInSc7d1	obchodní
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k9	až
do	do	k7c2	do
pozdních	pozdní	k2eAgNnPc2d1	pozdní
let	léto	k1gNnPc2	léto
tohoto	tento	k3xDgNnSc2	tento
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc4	York
ohrozily	ohrozit	k5eAaPmAgFnP	ohrozit
jeho	jeho	k3xOp3gFnSc4	jeho
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
město	město	k1gNnSc1	město
bohatlo	bohatnout	k5eAaImAgNnS	bohatnout
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
expanzi	expanze	k1gFnSc6	expanze
<g/>
,	,	kIx,	,
existovaly	existovat	k5eAaImAgInP	existovat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
i	i	k8xC	i
oblasti	oblast	k1gFnSc6	oblast
extrémní	extrémní	k2eAgFnSc2d1	extrémní
chudoby	chudoba	k1gFnSc2	chudoba
a	a	k8xC	a
milióny	milión	k4xCgInPc1	milión
lidí	člověk	k1gMnPc2	člověk
byly	být	k5eAaImAgInP	být
nuceny	nucen	k2eAgInPc1d1	nucen
přežívat	přežívat	k5eAaImF	přežívat
ve	v	k7c6	v
slumech	slum	k1gInPc6	slum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
premiér	premiér	k1gMnSc1	premiér
Robert	Robert	k1gMnSc1	Robert
Peel	Peel	k1gMnSc1	Peel
zřídil	zřídit	k5eAaPmAgMnS	zřídit
policejní	policejní	k2eAgFnSc4d1	policejní
složku	složka	k1gFnSc4	složka
z	z	k7c2	z
pravomocí	pravomoc	k1gFnPc2	pravomoc
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
obydleném	obydlený	k2eAgNnSc6d1	obydlené
území	území	k1gNnSc6	území
Londýna	Londýn	k1gInSc2	Londýn
–	–	k?	–
Metropolitan	metropolitan	k1gInSc1	metropolitan
Police	police	k1gFnSc2	police
Service	Service	k1gFnSc2	Service
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
policisty	policista	k1gMnPc4	policista
se	se	k3xPyFc4	se
tak	tak	k9	tak
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
pocházející	pocházející	k2eAgNnSc1d1	pocházející
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
zakladatele	zakladatel	k1gMnSc2	zakladatel
–	–	k?	–
bobbies	bobbies	k1gInSc1	bobbies
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
peelers	peelers	k6eAd1	peelers
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
Londýna	Londýn	k1gInSc2	Londýn
měla	mít	k5eAaImAgFnS	mít
vznikající	vznikající	k2eAgFnSc1d1	vznikající
železnice	železnice	k1gFnSc1	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
rozvoj	rozvoj	k1gInSc4	rozvoj
předměstí	předměstí	k1gNnSc2	předměstí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
cestovali	cestovat	k5eAaImAgMnP	cestovat
lidé	člověk	k1gMnPc1	člověk
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
bohaté	bohatý	k2eAgFnSc2d1	bohatá
vrstvy	vrstva	k1gFnSc2	vrstva
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
podpořila	podpořit	k5eAaPmAgFnS	podpořit
rychlé	rychlý	k2eAgNnSc4d1	rychlé
rozpínání	rozpínání	k1gNnSc4	rozpínání
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rozdělení	rozdělení	k1gNnSc1	rozdělení
společenských	společenský	k2eAgFnPc2d1	společenská
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
bohatí	bohatý	k2eAgMnPc1d1	bohatý
se	se	k3xPyFc4	se
usidlovali	usidlovat	k5eAaImAgMnP	usidlovat
v	v	k7c6	v
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
chudší	chudý	k2eAgFnSc2d2	chudší
vrstvy	vrstva	k1gFnSc2	vrstva
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
londýnská	londýnský	k2eAgFnSc1d1	londýnská
železniční	železniční	k2eAgFnSc1d1	železniční
trasa	trasa	k1gFnSc1	trasa
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
ze	z	k7c2	z
zastávky	zastávka	k1gFnSc2	zastávka
London	London	k1gMnSc1	London
Bridge	Bridge	k1gFnSc1	Bridge
do	do	k7c2	do
Greenwich	Greenwich	k1gInSc4	Greenwich
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
rychlý	rychlý	k2eAgInSc1d1	rychlý
rozvoj	rozvoj	k1gInSc1	rozvoj
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojila	spojit	k5eAaPmAgFnS	spojit
Londýn	Londýn	k1gInSc4	Londýn
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
oblastmi	oblast	k1gFnPc7	oblast
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnPc1d1	železniční
stanice	stanice	k1gFnPc1	stanice
postavené	postavený	k2eAgFnPc1d1	postavená
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
–	–	k?	–
Euston	Euston	k1gInSc1	Euston
station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paddington	Paddington	k1gInSc1	Paddington
station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fenchurch	Fenchurch	k1gInSc1	Fenchurch
Street	Street	k1gInSc1	Street
station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Waterloo	Waterloo	k1gNnSc1	Waterloo
station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cross	Cross	k1gInSc1	Cross
station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
a	a	k8xC	a
St	St	kA	St
Pancras	Pancras	k1gInSc1	Pancras
station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgFnP	vybudovat
první	první	k4xOgFnPc1	první
linky	linka	k1gFnPc1	linka
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
událostí	událost	k1gFnSc7	událost
byla	být	k5eAaImAgFnS	být
i	i	k9	i
velká	velký	k2eAgFnSc1d1	velká
výstava	výstava	k1gFnSc1	výstava
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Křišťálovém	křišťálový	k2eAgInSc6d1	křišťálový
paláci	palác	k1gInSc6	palác
-	-	kIx~	-
The	The	k1gFnSc1	The
Crystal	Crystal	k1gFnSc1	Crystal
palace	palace	k1gFnSc1	palace
-	-	kIx~	-
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
postaven	postaven	k2eAgInSc1d1	postaven
a	a	k8xC	a
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
význam	význam	k1gInSc4	význam
dominantního	dominantní	k2eAgInSc2d1	dominantní
vlivu	vliv	k1gInSc2	vliv
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Obydlená	obydlený	k2eAgFnSc1d1	obydlená
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
čtvrti	čtvrt	k1gFnPc1	čtvrt
-	-	kIx~	-
Islington	Islington	k1gInSc1	Islington
<g/>
,	,	kIx,	,
Paddington	Paddington	k1gInSc1	Paddington
<g/>
,	,	kIx,	,
Belgravia	Belgravia	k1gFnSc1	Belgravia
<g/>
,	,	kIx,	,
Holborn	Holborn	k1gNnSc1	Holborn
<g/>
,	,	kIx,	,
Finsbury	Finsbura	k1gFnPc1	Finsbura
<g/>
,	,	kIx,	,
Shoreditch	Shoreditch	k1gInSc1	Shoreditch
<g/>
,	,	kIx,	,
Southwark	Southwark	k1gInSc1	Southwark
a	a	k8xC	a
Lambeth	Lambeth	k1gInSc1	Lambeth
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
správa	správa	k1gFnSc1	správa
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
historických	historický	k2eAgInPc6d1	historický
místních	místní	k2eAgInPc6d1	místní
orgánech	orgán	k1gInPc6	orgán
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
efektivně	efektivně	k6eAd1	efektivně
řešit	řešit	k5eAaImF	řešit
narůstající	narůstající	k2eAgInPc4d1	narůstající
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
nový	nový	k2eAgInSc1d1	nový
správní	správní	k2eAgInSc1d1	správní
orgán	orgán	k1gInSc1	orgán
–	–	k?	–
Metropolitan	metropolitan	k1gInSc1	metropolitan
Board	Board	k1gInSc1	Board
of	of	k?	of
Works	Works	kA	Works
(	(	kIx(	(
<g/>
MBW	MBW	kA	MBW
<g/>
)	)	kIx)	)
–	–	k?	–
aby	aby	kYmCp3nS	aby
zajistil	zajistit	k5eAaPmAgMnS	zajistit
vytvoření	vytvoření	k1gNnSc4	vytvoření
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
rozšiřujícímu	rozšiřující	k2eAgInSc3d1	rozšiřující
se	se	k3xPyFc4	se
městu	město	k1gNnSc3	město
a	a	k8xC	a
stoupajícímu	stoupající	k2eAgInSc3d1	stoupající
počtu	počet	k1gInSc3	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
tato	tento	k3xDgFnSc1	tento
instituce	instituce	k1gFnSc1	instituce
řešila	řešit	k5eAaImAgFnS	řešit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
problém	problém	k1gInSc1	problém
s	s	k7c7	s
kanalizací	kanalizace	k1gFnSc7	kanalizace
a	a	k8xC	a
zásobováním	zásobování	k1gNnSc7	zásobování
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
odpadní	odpadní	k2eAgFnPc1d1	odpadní
vody	voda	k1gFnPc1	voda
vypouštěly	vypouštět	k5eAaImAgFnP	vypouštět
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
kulminoval	kulminovat	k5eAaImAgInS	kulminovat
tzv.	tzv.	kA	tzv.
Velkým	velký	k2eAgInSc7d1	velký
zápachem	zápach	k1gInSc7	zápach
(	(	kIx(	(
<g/>
Great	Great	k2eAgInSc1d1	Great
Stink	Stink	k1gInSc1	Stink
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
schválil	schválit	k5eAaPmAgInS	schválit
MBW	MBW	kA	MBW
výstavbu	výstavba	k1gFnSc4	výstavba
důkladného	důkladný	k2eAgInSc2d1	důkladný
systému	systém	k1gInSc2	systém
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
.	.	kIx.	.
</s>
<s>
Pověření	pověření	k1gNnSc1	pověření
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
této	tento	k3xDgFnSc2	tento
stavby	stavba	k1gFnSc2	stavba
dostal	dostat	k5eAaPmAgMnS	dostat
Josef	Josef	k1gMnSc1	Josef
Bazalgete	Bazalge	k1gNnSc2	Bazalge
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozsáhlejší	rozsáhlý	k2eAgFnPc1d3	nejrozsáhlejší
stavební	stavební	k2eAgFnPc1d1	stavební
akce	akce	k1gFnPc1	akce
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
představovala	představovat	k5eAaImAgFnS	představovat
stavbu	stavba	k1gFnSc4	stavba
2	[number]	k4	2
100	[number]	k4	100
km	km	kA	km
tunelů	tunel	k1gInPc2	tunel
a	a	k8xC	a
potrubí	potrubí	k1gNnSc2	potrubí
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
odváděly	odvádět	k5eAaImAgInP	odvádět
splašky	splašky	k1gInPc1	splašky
a	a	k8xC	a
rozváděly	rozvádět	k5eAaImAgInP	rozvádět
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
dokončen	dokončit	k5eAaPmNgInS	dokončit
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc4d1	zaznamenáno
výrazné	výrazný	k2eAgNnSc4d1	výrazné
snížení	snížení	k1gNnSc4	snížení
výskytu	výskyt	k1gInSc2	výskyt
cholery	cholera	k1gFnSc2	cholera
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
nákaz	nákaza	k1gFnPc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
přitahoval	přitahovat	k5eAaImAgMnS	přitahovat
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
z	z	k7c2	z
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
chudších	chudý	k2eAgFnPc2d2	chudší
oblastí	oblast	k1gFnPc2	oblast
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
usídlil	usídlit	k5eAaPmAgMnS	usídlit
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
Irů	Ir	k1gMnPc2	Ir
<g/>
,	,	kIx,	,
především	především	k9	především
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
neúrodě	neúroda	k1gFnSc6	neúroda
a	a	k8xC	a
hladomoru	hladomor	k1gInSc6	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
tvořili	tvořit	k5eAaImAgMnP	tvořit
irští	irský	k2eAgMnPc1d1	irský
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
asi	asi	k9	asi
20	[number]	k4	20
%	%	kIx~	%
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
domovem	domov	k1gInSc7	domov
početné	početný	k2eAgFnSc2d1	početná
židovské	židovský	k2eAgFnSc2d1	židovská
komunity	komunita	k1gFnSc2	komunita
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
bylo	být	k5eAaImAgNnS	být
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
nové	nový	k2eAgNnSc1d1	nové
hrabství	hrabství	k1gNnSc1	hrabství
–	–	k?	–
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
spravované	spravovaný	k2eAgInPc1d1	spravovaný
institucí	instituce	k1gFnSc7	instituce
London	London	k1gMnSc1	London
County	Counta	k1gFnSc2	Counta
Council	Council	k1gMnSc1	Council
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
volený	volený	k2eAgInSc1d1	volený
správní	správní	k2eAgInSc1d1	správní
orgán	orgán	k1gInSc1	orgán
s	s	k7c7	s
působností	působnost	k1gFnSc7	působnost
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
předchozí	předchozí	k2eAgInSc4d1	předchozí
Metropolitan	metropolitan	k1gInSc4	metropolitan
Board	Board	k1gMnSc1	Board
of	of	k?	of
Works	Works	kA	Works
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jmenovanými	jmenovaný	k2eAgInPc7d1	jmenovaný
členy	člen	k1gInPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nové	nový	k2eAgNnSc1d1	nové
hrabství	hrabství	k1gNnSc1	hrabství
tehdy	tehdy	k6eAd1	tehdy
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
všechny	všechen	k3xTgMnPc4	všechen
londýnská	londýnský	k2eAgNnPc1d1	Londýnské
předměstí	předměstí	k1gNnPc1	předměstí
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
Londýna	Londýn	k1gInSc2	Londýn
způsobil	způsobit	k5eAaPmAgMnS	způsobit
jeho	jeho	k3xOp3gFnSc3	jeho
expanzi	expanze	k1gFnSc3	expanze
mimo	mimo	k7c4	mimo
původně	původně	k6eAd1	původně
stanovené	stanovený	k2eAgFnPc4d1	stanovená
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
nebo	nebo	k8xC	nebo
opraveno	opravit	k5eAaPmNgNnS	opravit
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Trafalgarské	Trafalgarský	k2eAgNnSc4d1	Trafalgarské
náměstí	náměstí	k1gNnSc4	náměstí
Westminsterský	Westminsterský	k2eAgInSc1d1	Westminsterský
palác	palác	k1gInSc1	palác
a	a	k8xC	a
Big	Big	k1gFnSc1	Big
Ben	Ben	k1gInSc1	Ben
Royal	Royal	k1gMnSc1	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gMnSc1	Hall
Victoria	Victorium	k1gNnSc2	Victorium
and	and	k?	and
Albert	Albert	k1gMnSc1	Albert
Museum	museum	k1gNnSc1	museum
Tower	Tower	k1gMnSc1	Tower
Bridge	Bridg	k1gInSc2	Bridg
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
počátkem	počátkem	k7c2	počátkem
století	století	k1gNnSc2	století
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
slávy	sláva	k1gFnSc2	sláva
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
největšího	veliký	k2eAgNnSc2d3	veliký
světového	světový	k2eAgNnSc2d1	světové
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nové	nový	k2eAgNnSc1d1	nové
století	století	k1gNnSc1	století
přineslo	přinést	k5eAaPmAgNnS	přinést
významné	významný	k2eAgFnPc4d1	významná
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
bombardování	bombardování	k1gNnSc1	bombardování
Londýna	Londýn	k1gInSc2	Londýn
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
velké	velký	k2eAgNnSc1d1	velké
zděšení	zděšení	k1gNnSc1	zděšení
a	a	k8xC	a
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
smrt	smrt	k1gFnSc4	smrt
asi	asi	k9	asi
700	[number]	k4	700
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
Londýn	Londýn	k1gInSc1	Londýn
růst	růst	k1gInSc1	růst
obydlených	obydlený	k2eAgFnPc2d1	obydlená
oblastí	oblast	k1gFnPc2	oblast
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
kdykoli	kdykoli	k6eAd1	kdykoli
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Londýňané	Londýňan	k1gMnPc1	Londýňan
hledali	hledat	k5eAaImAgMnP	hledat
venkovský	venkovský	k2eAgInSc4d1	venkovský
styl	styl	k1gInSc4	styl
života	život	k1gInSc2	život
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
módním	módní	k2eAgInSc7d1	módní
trendem	trend	k1gInSc7	trend
stala	stát	k5eAaPmAgFnS	stát
nižší	nízký	k2eAgFnSc1d2	nižší
hustota	hustota	k1gFnSc1	hustota
obydlení	obydlení	k1gNnSc2	obydlení
-	-	kIx~	-
typické	typický	k2eAgInPc1d1	typický
dvojdomky	dvojdomek	k1gInPc1	dvojdomek
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
ustupující	ustupující	k2eAgFnSc2d1	ustupující
řadové	řadový	k2eAgFnSc2d1	řadová
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
doprovázeno	doprovázet	k5eAaImNgNnS	doprovázet
další	další	k2eAgFnSc7d1	další
výstavbou	výstavba	k1gFnSc7	výstavba
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nových	nový	k2eAgFnPc2d1	nová
linek	linka	k1gFnPc2	linka
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
a	a	k8xC	a
nově	nově	k6eAd1	nově
i	i	k9	i
automobilovou	automobilový	k2eAgFnSc7d1	automobilová
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
regionech	region	k1gInPc6	region
i	i	k8xC	i
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
se	se	k3xPyFc4	se
během	během	k7c2	během
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
objevovaly	objevovat	k5eAaImAgInP	objevovat
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
svého	svůj	k3xOyFgNnSc2	svůj
maxima	maximum	k1gNnSc2	maximum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
počtem	počet	k1gInSc7	počet
8,6	[number]	k4	8,6
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
pro	pro	k7c4	pro
vytápění	vytápění	k1gNnSc4	vytápění
používáno	používán	k2eAgNnSc4d1	používáno
především	především	k6eAd1	především
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zdrojem	zdroj	k1gInSc7	zdroj
obrovského	obrovský	k2eAgNnSc2d1	obrovské
množství	množství	k1gNnSc2	množství
dýmu	dým	k1gInSc2	dým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
podmínkami	podmínka	k1gFnPc7	podmínka
byl	být	k5eAaImAgInS	být
kouř	kouř	k1gInSc1	kouř
zdrojem	zdroj	k1gInSc7	zdroj
typického	typický	k2eAgInSc2d1	typický
smogu	smog	k1gInSc2	smog
a	a	k8xC	a
Londýn	Londýn	k1gInSc1	Londýn
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
londýnskou	londýnský	k2eAgFnSc7d1	londýnská
mlhou	mlha	k1gFnSc7	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
čistém	čistý	k2eAgInSc6d1	čistý
vzduchu	vzduch	k1gInSc6	vzduch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
vydaný	vydaný	k2eAgInSc1d1	vydaný
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zkušeností	zkušenost	k1gFnPc2	zkušenost
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
velkého	velký	k2eAgInSc2d1	velký
smogu	smog	k1gInSc2	smog
mezi	mezi	k7c7	mezi
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
1952	[number]	k4	1952
a	a	k8xC	a
březnem	březen	k1gInSc7	březen
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
smrt	smrt	k1gFnSc4	smrt
12	[number]	k4	12
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
stanovil	stanovit	k5eAaPmAgInS	stanovit
vytvoření	vytvoření	k1gNnSc4	vytvoření
bezkouřových	bezkouřový	k2eAgFnPc2d1	bezkouřový
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
bylo	být	k5eAaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
pro	pro	k7c4	pro
topení	topení	k1gNnSc4	topení
používat	používat	k5eAaImF	používat
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
dýmu	dým	k1gInSc2	dým
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většina	většina	k1gFnSc1	většina
domácností	domácnost	k1gFnPc2	domácnost
používala	používat	k5eAaImAgFnS	používat
pro	pro	k7c4	pro
topení	topení	k1gNnSc4	topení
krby	krb	k1gInPc4	krb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
mnohá	mnohý	k2eAgNnPc1d1	mnohé
jiná	jiný	k2eAgNnPc1d1	jiné
anglická	anglický	k2eAgNnPc1d1	anglické
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
terčem	terč	k1gInSc7	terč
útoků	útok	k1gInPc2	útok
bombardování	bombardování	k1gNnSc1	bombardování
německým	německý	k2eAgNnSc7d1	německé
letectvem	letectvo	k1gNnSc7	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Stovky	stovka	k1gFnPc1	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
londýnských	londýnský	k2eAgFnPc2d1	londýnská
dětí	dítě	k1gFnPc2	dítě
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
preventivní	preventivní	k2eAgNnSc1d1	preventivní
opatření	opatření	k1gNnSc1	opatření
<g/>
,	,	kIx,	,
odstěhováno	odstěhován	k2eAgNnSc1d1	odstěhován
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
ohrožení	ohrožení	k1gNnSc1	ohrožení
bombardováním	bombardování	k1gNnSc7	bombardování
menší	malý	k2eAgFnSc2d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úkryty	úkryt	k1gInPc4	úkryt
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
před	před	k7c7	před
nálety	nálet	k1gInPc7	nálet
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
zastávky	zastávka	k1gFnPc1	zastávka
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
Londýna	Londýn	k1gInSc2	Londýn
bombardováním	bombardování	k1gNnSc7	bombardování
bylo	být	k5eAaImAgNnS	být
značné	značný	k2eAgNnSc1d1	značné
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
zasaženou	zasažený	k2eAgFnSc7d1	zasažená
částí	část	k1gFnSc7	část
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
Docklands	Docklandsa	k1gFnPc2	Docklandsa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
asi	asi	k9	asi
35	[number]	k4	35
000	[number]	k4	000
Londýňanů	Londýňan	k1gMnPc2	Londýňan
<g/>
,	,	kIx,	,
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
bylo	být	k5eAaImAgNnS	být
vážně	vážně	k6eAd1	vážně
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
,	,	kIx,	,
desítky	desítka	k1gFnSc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
budov	budova	k1gFnPc2	budova
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
a	a	k8xC	a
stovky	stovka	k1gFnPc1	stovka
tisíc	tisíc	k4xCgInSc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
Londýna	Londýn	k1gInSc2	Londýn
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
bez	bez	k7c2	bez
domova	domov	k1gInSc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
zotavoval	zotavovat	k5eAaImAgInS	zotavovat
z	z	k7c2	z
válečných	válečný	k2eAgFnPc2d1	válečná
škod	škoda	k1gFnPc2	škoda
<g/>
,	,	kIx,	,
na	na	k7c6	na
stadiónu	stadión	k1gInSc6	stadión
ve	v	k7c6	v
Wembley	Wemblea	k1gMnSc2	Wemblea
konaly	konat	k5eAaImAgFnP	konat
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgInSc6d1	poválečný
Londýně	Londýn	k1gInSc6	Londýn
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
problém	problém	k1gInSc1	problém
bydlení	bydlení	k1gNnSc2	bydlení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
obytných	obytný	k2eAgFnPc2d1	obytná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
zničeny	zničit	k5eAaPmNgFnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
řešení	řešení	k1gNnSc1	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
stavba	stavba	k1gFnSc1	stavba
vysokých	vysoký	k2eAgInPc2d1	vysoký
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
stěhování	stěhování	k1gNnSc2	stěhování
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
čtvrtí	čtvrt	k1gFnPc2	čtvrt
budovaných	budovaný	k2eAgFnPc2d1	budovaná
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
tvář	tvář	k1gFnSc1	tvář
Londýna	Londýn	k1gInSc2	Londýn
výstavbou	výstavba	k1gFnSc7	výstavba
vysokých	vysoký	k2eAgInPc2d1	vysoký
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
dramaticky	dramaticky	k6eAd1	dramaticky
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
britských	britský	k2eAgFnPc2d1	britská
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xS	jako
Beatles	Beatles	k1gFnPc2	Beatles
nebo	nebo	k8xC	nebo
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
stával	stávat	k5eAaImAgInS	stávat
Londýn	Londýn	k1gInSc1	Londýn
střediskem	středisko	k1gNnSc7	středisko
kultury	kultura	k1gFnSc2	kultura
mládeže	mládež	k1gFnSc2	mládež
označované	označovaný	k2eAgFnSc2d1	označovaná
pojmem	pojem	k1gInSc7	pojem
Swinging	Swinging	k1gInSc4	Swinging
London	London	k1gMnSc1	London
a	a	k8xC	a
Carnaby	Carnaba	k1gFnPc1	Carnaba
Street	Streeta	k1gFnPc2	Streeta
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
světovým	světový	k2eAgMnSc7d1	světový
centrem	centr	k1gMnSc7	centr
mladé	mladý	k2eAgFnSc2d1	mladá
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
Londýna	Londýn	k1gInSc2	Londýn
jako	jako	k8xS	jako
zdroje	zdroj	k1gInPc4	zdroj
inspirací	inspirace	k1gFnPc2	inspirace
pro	pro	k7c4	pro
mladou	mladý	k2eAgFnSc4d1	mladá
módu	móda	k1gFnSc4	móda
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
během	během	k7c2	během
období	období	k1gNnSc2	období
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
éry	éra	k1gFnSc2	éra
punku	punk	k1gInSc2	punk
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
známá	známá	k1gFnSc1	známá
jako	jako	k8xS	jako
doba	doba	k1gFnSc1	doba
obnovy	obnova	k1gFnSc2	obnova
vlivu	vliv	k1gInSc2	vliv
britského	britský	k2eAgInSc2d1	britský
popu	pop	k1gInSc2	pop
<g/>
.	.	kIx.	.
</s>
<s>
Rozpínání	rozpínání	k1gNnSc1	rozpínání
Londýna	Londýn	k1gInSc2	Londýn
bylo	být	k5eAaImAgNnS	být
válkou	válka	k1gFnSc7	válka
zpomaleno	zpomalen	k2eAgNnSc4d1	zpomaleno
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
pás	pás	k1gInSc1	pás
zeleně	zeleň	k1gFnSc2	zeleň
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
lepšího	dobrý	k2eAgNnSc2d2	lepší
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
vymezení	vymezení	k1gNnSc1	vymezení
hrabství	hrabství	k1gNnSc2	hrabství
Londýn	Londýn	k1gInSc4	Londýn
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
jen	jen	k9	jen
část	část	k1gFnSc4	část
z	z	k7c2	z
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgNnPc2d1	vzniklé
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
,	,	kIx,	,
spravovaného	spravovaný	k2eAgNnSc2d1	spravované
institucí	instituce	k1gFnPc2	instituce
London	London	k1gMnSc1	London
County	Counta	k1gFnSc2	Counta
Council	Councila	k1gFnPc2	Councila
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
ustanovena	ustanoven	k2eAgNnPc1d1	ustanoveno
správní	správní	k2eAgNnPc1d1	správní
oblast	oblast	k1gFnSc4	oblast
Velký	velký	k2eAgInSc1d1	velký
Londýn	Londýn	k1gInSc1	Londýn
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
správní	správní	k2eAgInSc1d1	správní
úřad	úřad	k1gInSc1	úřad
–	–	k?	–
Greater	Greater	k1gInSc1	Greater
London	London	k1gMnSc1	London
Council	Council	k1gMnSc1	Council
(	(	kIx(	(
<g/>
GLC	GLC	kA	GLC
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
32	[number]	k4	32
samosprávnými	samosprávný	k2eAgFnPc7d1	samosprávná
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
vlivem	vlivem	k7c2	vlivem
politických	politický	k2eAgInPc2d1	politický
rozporů	rozpor	k1gInPc2	rozpor
mezi	mezi	k7c7	mezi
GLC	GLC	kA	GLC
<g/>
,	,	kIx,	,
vedeném	vedený	k2eAgInSc6d1	vedený
Kenem	Ken	k1gInSc7	Ken
Livingstonem	Livingston	k1gInSc7	Livingston
<g/>
,	,	kIx,	,
a	a	k8xC	a
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
vládou	vláda	k1gFnSc7	vláda
Margaret	Margareta	k1gFnPc2	Margareta
Tchatcerové	Tchatcerové	k2eAgFnPc2d1	Tchatcerové
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
GLC	GLC	kA	GLC
zrušen	zrušen	k2eAgMnSc1d1	zrušen
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
pravomoci	pravomoc	k1gFnPc1	pravomoc
byly	být	k5eAaImAgFnP	být
přiděleny	přidělen	k2eAgFnPc1d1	přidělena
správám	správa	k1gFnPc3	správa
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
labouristickou	labouristický	k2eAgFnSc7d1	labouristická
vládou	vláda	k1gFnSc7	vláda
založen	založen	k2eAgInSc1d1	založen
správní	správní	k2eAgInSc1d1	správní
orgán	orgán	k1gInSc1	orgán
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
nad	nad	k7c7	nad
celou	celý	k2eAgFnSc7d1	celá
oblastí	oblast	k1gFnSc7	oblast
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
–	–	k?	–
Greater	Greater	k1gMnSc1	Greater
London	London	k1gMnSc1	London
Authority	Authorita	k1gFnSc2	Authorita
a	a	k8xC	a
reprezentující	reprezentující	k2eAgFnSc1d1	reprezentující
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
anglických	anglický	k2eAgInPc2d1	anglický
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
v	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
desetiletích	desetiletí	k1gNnPc6	desetiletí
stále	stále	k6eAd1	stále
klesala	klesat	k5eAaImAgFnS	klesat
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
8,6	[number]	k4	8,6
miliónů	milión	k4xCgInPc2	milión
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
k	k	k7c3	k
6,8	[number]	k4	6,8
miliónů	milión	k4xCgInPc2	milión
v	v	k7c4	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
vlivem	vlivem	k7c2	vlivem
silného	silný	k2eAgInSc2d1	silný
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
postupně	postupně	k6eAd1	postupně
zastavoval	zastavovat	k5eAaImAgInS	zastavovat
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
Plan	plan	k1gInSc4	plan
publikovaný	publikovaný	k2eAgInSc4d1	publikovaný
starostou	starosta	k1gMnSc7	starosta
Londýna	Londýn	k1gInSc2	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
počtu	počet	k1gInSc2	počet
asi	asi	k9	asi
8,1	[number]	k4	8,1
miliónů	milión	k4xCgInPc2	milión
a	a	k8xC	a
stabilně	stabilně	k6eAd1	stabilně
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
trendu	trend	k1gInSc6	trend
stavět	stavět	k5eAaImF	stavět
vyšší	vysoký	k2eAgFnPc4d2	vyšší
obytné	obytný	k2eAgFnPc4d1	obytná
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
velké	velký	k2eAgFnPc4d1	velká
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
připomenutí	připomenutí	k1gNnSc4	připomenutí
nového	nový	k2eAgNnSc2d1	nové
milénia	milénium	k1gNnSc2	milénium
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
Millennium	millennium	k1gNnSc1	millennium
Dome	dům	k1gInSc5	dům
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hledá	hledat	k5eAaImIp3nS	hledat
smysluplné	smysluplný	k2eAgNnSc4d1	smysluplné
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Jedením	jedení	k1gNnSc7	jedení
z	z	k7c2	z
daleko	daleko	k6eAd1	daleko
úspěšnějších	úspěšný	k2eAgInPc2d2	úspěšnější
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zamýšlený	zamýšlený	k2eAgInSc1d1	zamýšlený
jen	jen	k9	jen
jako	jako	k8xS	jako
přechodná	přechodný	k2eAgFnSc1d1	přechodná
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
vyhlídkové	vyhlídkový	k2eAgNnSc1d1	vyhlídkové
kolo	kolo	k1gNnSc1	kolo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Millennium	millennium	k1gNnSc1	millennium
Wheel	Wheel	k1gInSc1	Wheel
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
navštíví	navštívit	k5eAaPmIp3nS	navštívit
ročně	ročně	k6eAd1	ročně
až	až	k9	až
4	[number]	k4	4
milióny	milión	k4xCgInPc1	milión
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
loterie	loterie	k1gFnSc1	loterie
k	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
milénia	milénium	k1gNnSc2	milénium
byla	být	k5eAaImAgFnS	být
zdrojem	zdroj	k1gInSc7	zdroj
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
existujících	existující	k2eAgFnPc2d1	existující
atrakcí	atrakce	k1gFnPc2	atrakce
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
fondu	fond	k1gInSc2	fond
bylo	být	k5eAaImAgNnS	být
financováno	financovat	k5eAaBmNgNnS	financovat
například	například	k6eAd1	například
zastřešení	zastřešení	k1gNnSc1	zastřešení
velké	velký	k2eAgFnSc2d1	velká
dvorany	dvorana	k1gFnSc2	dvorana
Britského	britský	k2eAgNnSc2d1	Britské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
Londýn	Londýn	k1gInSc1	Londýn
vybrán	vybrán	k2eAgInSc1d1	vybrán
jako	jako	k9	jako
místo	místo	k7c2	místo
pořádání	pořádání	k1gNnSc2	pořádání
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Oslavy	oslava	k1gFnPc1	oslava
tohoto	tento	k3xDgNnSc2	tento
vítězství	vítězství	k1gNnSc2	vítězství
však	však	k9	však
přerušilo	přerušit	k5eAaPmAgNnS	přerušit
několik	několik	k4yIc1	několik
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
londýnské	londýnský	k2eAgNnSc4d1	Londýnské
metro	metro	k1gNnSc4	metro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bombových	bombový	k2eAgInPc2d1	bombový
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
metro	metro	k1gNnSc4	metro
a	a	k8xC	a
autobus	autobus	k1gInSc4	autobus
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
700	[number]	k4	700
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
1	[number]	k4	1
–	–	k?	–
několik	několik	k4yIc4	několik
zemědělců	zemědělec	k1gMnPc2	zemědělec
50	[number]	k4	50
–	–	k?	–
5000	[number]	k4	5000
–	–	k?	–
10	[number]	k4	10
000	[number]	k4	000
140	[number]	k4	140
–	–	k?	–
45	[number]	k4	45
000	[number]	k4	000
–	–	k?	–
60	[number]	k4	60
000	[number]	k4	000
300	[number]	k4	300
–	–	k?	–
10	[number]	k4	10
000	[number]	k4	000
–	–	k?	–
20	[number]	k4	20
000	[number]	k4	000
400	[number]	k4	400
–	–	k?	–
méně	málo	k6eAd2	málo
než	než	k8xS	než
5000	[number]	k4	5000
<g/>
?	?	kIx.	?
</s>
<s>
500	[number]	k4	500
–	–	k?	–
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
<g/>
?	?	kIx.	?
</s>
<s>
700	[number]	k4	700
–	–	k?	–
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
v	v	k7c4	v
Sasy	Sas	k1gMnPc4	Sas
nově	nově	k6eAd1	nově
vybudovaném	vybudovaný	k2eAgNnSc6d1	vybudované
městě	město	k1gNnSc6	město
Lundenwic	Lundenwice	k1gFnPc2	Lundenwice
900	[number]	k4	900
–	–	k?	–
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
v	v	k7c4	v
Sasy	Sas	k1gMnPc4	Sas
obnoveném	obnovený	k2eAgNnSc6d1	obnovené
městě	město	k1gNnSc6	město
Lundenburgh	Lundenburgh	k1gInSc1	Lundenburgh
1000	[number]	k4	1000
–	–	k?	–
5000	[number]	k4	5000
–	–	k?	–
10	[number]	k4	10
000	[number]	k4	000
1100	[number]	k4	1100
–	–	k?	–
10	[number]	k4	10
000	[number]	k4	000
–	–	k?	–
20	[number]	k4	20
000	[number]	k4	000
1300	[number]	k4	1300
–	–	k?	–
50	[number]	k4	50
000	[number]	k4	000
–	–	k?	–
100	[number]	k4	100
000	[number]	k4	000
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
Dereka	Dereka	k1gMnSc1	Dereka
Keenea	Keenea	k1gMnSc1	Keenea
<g/>
)	)	kIx)	)
1350	[number]	k4	1350
–	–	k?	–
25	[number]	k4	25
<g />
.	.	kIx.	.
</s>
<s>
000	[number]	k4	000
–	–	k?	–
50	[number]	k4	50
000	[number]	k4	000
po	po	k7c6	po
Černé	Černé	k2eAgFnSc6d1	Černé
smrti	smrt	k1gFnSc6	smrt
1500	[number]	k4	1500
–	–	k?	–
50	[number]	k4	50
000	[number]	k4	000
–	–	k?	–
100	[number]	k4	100
000	[number]	k4	000
1600	[number]	k4	1600
–	–	k?	–
100	[number]	k4	100
000	[number]	k4	000
–	–	k?	–
200	[number]	k4	200
000	[number]	k4	000
1700	[number]	k4	1700
–	–	k?	–
550	[number]	k4	550
000	[number]	k4	000
(	(	kIx(	(
<g/>
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Walesu	Wales	k1gInSc2	Wales
<g/>
)	)	kIx)	)
1750	[number]	k4	1750
–	–	k?	–
700	[number]	k4	700
000	[number]	k4	000
1801	[number]	k4	1801
–	–	k?	–
959	[number]	k4	959
300	[number]	k4	300
(	(	kIx(	(
<g/>
Londýn	Londýn	k1gInSc1	Londýn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
<g />
.	.	kIx.	.
</s>
<s>
době	době	k6eAd1	době
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
1831	[number]	k4	1831
–	–	k?	–
1	[number]	k4	1
655	[number]	k4	655
000	[number]	k4	000
1851	[number]	k4	1851
–	–	k?	–
2	[number]	k4	2
363	[number]	k4	363
000	[number]	k4	000
1891	[number]	k4	1891
–	–	k?	–
5	[number]	k4	5
572	[number]	k4	572
012	[number]	k4	012
1901	[number]	k4	1901
–	–	k?	–
6	[number]	k4	6
506	[number]	k4	506
954	[number]	k4	954
1911	[number]	k4	1911
–	–	k?	–
7	[number]	k4	7
160	[number]	k4	160
525	[number]	k4	525
1921	[number]	k4	1921
–	–	k?	–
7	[number]	k4	7
386	[number]	k4	386
848	[number]	k4	848
1931	[number]	k4	1931
–	–	k?	–
8	[number]	k4	8
110	[number]	k4	110
480	[number]	k4	480
1939	[number]	k4	1939
–	–	k?	–
8	[number]	k4	8
615	[number]	k4	615
245	[number]	k4	245
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
<g />
.	.	kIx.	.
</s>
<s>
obyvatel	obyvatel	k1gMnSc1	obyvatel
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Londýna	Londýn	k1gInSc2	Londýn
<g/>
)	)	kIx)	)
1951	[number]	k4	1951
–	–	k?	–
8	[number]	k4	8
196	[number]	k4	196
978	[number]	k4	978
1961	[number]	k4	1961
–	–	k?	–
7	[number]	k4	7
992	[number]	k4	992
616	[number]	k4	616
1971	[number]	k4	1971
–	–	k?	–
7	[number]	k4	7
452	[number]	k4	452
520	[number]	k4	520
1981	[number]	k4	1981
–	–	k?	–
6	[number]	k4	6
805	[number]	k4	805
000	[number]	k4	000
1991	[number]	k4	1991
–	–	k?	–
6	[number]	k4	6
829	[number]	k4	829
300	[number]	k4	300
2001	[number]	k4	2001
–	–	k?	–
7	[number]	k4	7
322	[number]	k4	322
400	[number]	k4	400
2003	[number]	k4	2003
–	–	k?	–
7	[number]	k4	7
387	[number]	k4	387
900	[number]	k4	900
2016	[number]	k4	2016
–	–	k?	–
8	[number]	k4	8
200	[number]	k4	200
000	[number]	k4	000
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
podle	podle	k7c2	podle
<g />
.	.	kIx.	.
</s>
<s>
materiálu	materiál	k1gInSc2	materiál
London	London	k1gMnSc1	London
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Place	plac	k1gInSc6	plac
in	in	k?	in
the	the	k?	the
UK	UK	kA	UK
Economy	Econom	k1gInPc1	Econom
Corporation	Corporation	k1gInSc1	Corporation
of	of	k?	of
London	London	k1gMnSc1	London
září	zářit	k5eAaImIp3nS	zářit
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
První	první	k4xOgNnPc1	první
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
údaje	údaj	k1gInPc1	údaj
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
datem	datum	k1gNnSc7	datum
jsou	být	k5eAaImIp3nP	být
odhady	odhad	k1gInPc1	odhad
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
archeologických	archeologický	k2eAgInPc6d1	archeologický
výzkumech	výzkum	k1gInPc6	výzkum
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
známým	známý	k2eAgInSc7d1	známý
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
londýnské	londýnský	k2eAgFnSc2d1	londýnská
City	City	k1gFnSc2	City
v	v	k7c6	v
letech	let	k1gInPc6	let
1600	[number]	k4	1600
–	–	k?	–
1800	[number]	k4	1800
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1300	[number]	k4	1300
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
s	s	k7c7	s
historických	historický	k2eAgInPc2d1	historický
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
oblast	oblast	k1gFnSc4	oblast
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
Londýn	Londýn	k1gInSc1	Londýn
nebyl	být	k5eNaImAgInS	být
vymezen	vymezit	k5eAaPmNgInS	vymezit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
byly	být	k5eAaImAgFnP	být
rekonstruovány	rekonstruován	k2eAgMnPc4d1	rekonstruován
statistickým	statistický	k2eAgInSc7d1	statistický
úřadem	úřad	k1gInSc7	úřad
z	z	k7c2	z
dat	datum	k1gNnPc2	datum
založených	založený	k2eAgNnPc2d1	založené
na	na	k7c6	na
předchozích	předchozí	k2eAgNnPc6d1	předchozí
sčítáních	sčítání	k1gNnPc6	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tak	tak	k8xS	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
oblasti	oblast	k1gFnSc2	oblast
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
a	a	k8xC	a
předchozí	předchozí	k2eAgFnPc1d1	předchozí
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
průměrům	průměr	k1gInPc3	průměr
uprostřed	uprostřed	k7c2	uprostřed
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lépe	dobře	k6eAd2	dobře
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
průměrnému	průměrný	k2eAgInSc3d1	průměrný
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
