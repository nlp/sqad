<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
MOV	MOV	kA	MOV
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Comité	Comitý	k2eAgFnSc2d1	Comitý
International	International	k1gFnSc2	International
Olympique	Olympiqu	k1gFnSc2	Olympiqu
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
International	International	k1gFnSc1	International
Olympic	Olympice	k1gInPc2	Olympice
Committee	Committe	k1gMnSc2	Committe
<g/>
,	,	kIx,	,
IOC	IOC	kA	IOC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnSc1	organizace
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
Pierrem	Pierr	k1gMnSc7	Pierr
de	de	k?	de
Coubertinem	Coubertin	k1gMnSc7	Coubertin
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Demetriusem	Demetrius	k1gMnSc7	Demetrius
Vikelasem	Vikelas	k1gMnSc7	Vikelas
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Guthem	Guth	k1gInSc7	Guth
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1894	[number]	k4	1894
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
obnovit	obnovit	k5eAaPmF	obnovit
tradici	tradice	k1gFnSc4	tradice
antických	antický	k2eAgFnPc2d1	antická
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
