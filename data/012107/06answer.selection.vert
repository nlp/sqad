<s>
Rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
starším	starý	k2eAgInSc7d2	starší
názvem	název	k1gInSc7	název
záření	záření	k1gNnSc2	záření
X	X	kA	X
či	či	k8xC	či
paprsky	paprsek	k1gInPc1	paprsek
X	X	kA	X
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
o	o	k7c6	o
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
10	[number]	k4	10
nanometrů	nanometr	k1gInPc2	nanometr
až	až	k9	až
1	[number]	k4	1
pikometr	pikometr	k1gInSc1	pikometr
<g/>
.	.	kIx.	.
</s>
