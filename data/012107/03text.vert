<p>
<s>
Rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
starším	starý	k2eAgInSc7d2	starší
názvem	název	k1gInSc7	název
záření	záření	k1gNnSc2	záření
X	X	kA	X
či	či	k8xC	či
paprsky	paprsek	k1gInPc1	paprsek
X	X	kA	X
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
o	o	k7c6	o
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
10	[number]	k4	10
nanometrů	nanometr	k1gInPc2	nanometr
až	až	k9	až
1	[number]	k4	1
pikometr	pikometr	k1gInSc1	pikometr
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
při	při	k7c6	při
lékařských	lékařský	k2eAgNnPc6d1	lékařské
vyšetřeních	vyšetření	k1gNnPc6	vyšetření
a	a	k8xC	a
v	v	k7c6	v
krystalografii	krystalografie	k1gFnSc6	krystalografie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
formu	forma	k1gFnSc4	forma
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
jako	jako	k9	jako
takové	takový	k3xDgNnSc1	takový
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
chemie	chemie	k1gFnSc1	chemie
==	==	k?	==
</s>
</p>
<p>
<s>
Záření	záření	k1gNnSc1	záření
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
0,1	[number]	k4	0,1
nm	nm	k?	nm
je	být	k5eAaImIp3nS	být
nazýváno	nazývat	k5eAaImNgNnS	nazývat
měkké	měkký	k2eAgNnSc1d1	měkké
a	a	k8xC	a
kratší	krátký	k2eAgNnSc1d2	kratší
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
nejenergičtější	energický	k2eAgFnSc2d3	nejenergičtější
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
překrývají	překrývat	k5eAaImIp3nP	překrývat
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
záření	záření	k1gNnSc1	záření
gama	gama	k1gNnSc1	gama
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
je	on	k3xPp3gFnPc4	on
dle	dle	k7c2	dle
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Foton	foton	k1gInSc1	foton
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
interakcích	interakce	k1gFnPc6	interakce
vysoce	vysoce	k6eAd1	vysoce
energického	energický	k2eAgInSc2d1	energický
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
záření	záření	k1gNnSc1	záření
gama	gama	k1gNnSc2	gama
při	při	k7c6	při
procesech	proces	k1gInPc6	proces
uvnitř	uvnitř	k7c2	uvnitř
jádra	jádro	k1gNnSc2	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
též	též	k9	též
využíváno	využívat	k5eAaPmNgNnS	využívat
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnPc1	částice
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
ionizovány	ionizovat	k5eAaBmNgFnP	ionizovat
rentgenovým	rentgenový	k2eAgNnSc7d1	rentgenové
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
sekundární	sekundární	k2eAgNnSc1d1	sekundární
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
analyzováno	analyzován	k2eAgNnSc4d1	analyzováno
detektorem	detektor	k1gInSc7	detektor
a	a	k8xC	a
přiřazeno	přiřazen	k2eAgNnSc1d1	přiřazeno
konkrétním	konkrétní	k2eAgInPc3d1	konkrétní
prvkům	prvek	k1gInPc3	prvek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgMnPc2	který
se	se	k3xPyFc4	se
analyzovaná	analyzovaný	k2eAgFnSc1d1	analyzovaná
látka	látka	k1gFnSc1	látka
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
analytická	analytický	k2eAgFnSc1d1	analytická
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rentgenfluorescenční	rentgenfluorescenční	k2eAgFnSc1d1	rentgenfluorescenční
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
vzniká	vznikat	k5eAaImIp3nS	vznikat
například	například	k6eAd1	například
v	v	k7c6	v
rentgence	rentgenka	k1gFnSc6	rentgenka
na	na	k7c6	na
principu	princip	k1gInSc6	princip
katodového	katodový	k2eAgNnSc2d1	katodové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Katodové	katodový	k2eAgNnSc1d1	katodové
záření	záření	k1gNnSc1	záření
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
katodové	katodový	k2eAgFnSc6d1	katodová
trubici	trubice	k1gFnSc6	trubice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
skleněné	skleněný	k2eAgFnSc6d1	skleněná
vakuované	vakuovaný	k2eAgFnSc6d1	vakuovaná
trubici	trubice	k1gFnSc6	trubice
se	s	k7c7	s
zabudovanou	zabudovaný	k2eAgFnSc7d1	zabudovaná
katodou	katoda	k1gFnSc7	katoda
a	a	k8xC	a
anodou	anoda	k1gFnSc7	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
katodovém	katodový	k2eAgNnSc6d1	katodové
záření	záření	k1gNnSc6	záření
se	se	k3xPyFc4	se
elektrony	elektron	k1gInPc7	elektron
z	z	k7c2	z
katody	katoda	k1gFnSc2	katoda
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
nažhavení	nažhavení	k1gNnSc6	nažhavení
a	a	k8xC	a
po	po	k7c6	po
vložení	vložení	k1gNnSc6	vložení
anodového	anodový	k2eAgNnSc2d1	anodové
napětí	napětí	k1gNnSc2	napětí
vylétnou	vylétnout	k5eAaPmIp3nP	vylétnout
z	z	k7c2	z
katody	katoda	k1gFnSc2	katoda
<g/>
,	,	kIx,	,
prolétnou	prolétnout	k5eAaPmIp3nP	prolétnout
otvorem	otvor	k1gInSc7	otvor
v	v	k7c6	v
anodě	anoda	k1gFnSc6	anoda
a	a	k8xC	a
dopadají	dopadat	k5eAaImIp3nP	dopadat
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
světélkování	světélkování	k1gNnSc4	světélkování
skleněné	skleněný	k2eAgFnSc2d1	skleněná
stěny	stěna	k1gFnSc2	stěna
výbojové	výbojový	k2eAgFnSc2d1	výbojová
trubice	trubice	k1gFnSc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Rentgenka	rentgenka	k1gFnSc1	rentgenka
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
výbojové	výbojový	k2eAgFnSc2d1	výbojová
trubice	trubice	k1gFnSc2	trubice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
katodové	katodový	k2eAgNnSc1d1	katodové
záření	záření	k1gNnSc1	záření
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
kov	kov	k1gInSc4	kov
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
relativní	relativní	k2eAgFnSc7d1	relativní
atomovou	atomový	k2eAgFnSc7d1	atomová
hmotností	hmotnost	k1gFnSc7	hmotnost
např.	např.	kA	např.
wolfram	wolfram	k1gInSc4	wolfram
a	a	k8xC	a
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dopadu	dopad	k1gInSc2	dopad
vzniká	vznikat	k5eAaImIp3nS	vznikat
pronikavé	pronikavý	k2eAgNnSc4d1	pronikavé
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
zejména	zejména	k6eAd1	zejména
tvrdšího	tvrdý	k2eAgNnSc2d2	tvrdší
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
zrcadlit	zrcadlit	k5eAaImF	zrcadlit
běžnými	běžný	k2eAgNnPc7d1	běžné
zrcadly	zrcadlo	k1gNnPc7	zrcadlo
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
usměrňovat	usměrňovat	k5eAaImF	usměrňovat
pomocí	pomocí	k7c2	pomocí
skleněných	skleněný	k2eAgFnPc2d1	skleněná
čoček	čočka	k1gFnPc2	čočka
či	či	k8xC	či
ohýbat	ohýbat	k5eAaImF	ohýbat
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
jako	jako	k8xS	jako
katodové	katodový	k2eAgNnSc1d1	katodové
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zrcadlit	zrcadlit	k5eAaImF	zrcadlit
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
uplným	uplný	k2eAgInSc7d1	uplný
odrazem	odraz	k1gInSc7	odraz
(	(	kIx(	(
<g/>
totální	totální	k2eAgFnSc7d1	totální
reflexí	reflexe	k1gFnSc7	reflexe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
úhel	úhel	k1gInSc1	úhel
dopadu	dopad	k1gInSc2	dopad
blíží	blížit	k5eAaImIp3nS	blížit
tečně	tečně	k6eAd1	tečně
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgNnSc2	tento
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
rentgenové	rentgenový	k2eAgFnSc6d1	rentgenová
mikroskopii	mikroskopie	k1gFnSc6	mikroskopie
(	(	kIx(	(
<g/>
Schwarzschildova	Schwarzschildův	k2eAgFnSc1d1	Schwarzschildova
<g/>
,	,	kIx,	,
Montelova	Montelův	k2eAgFnSc1d1	Montelův
<g/>
,	,	kIx,	,
Kirkpatrick-Baezova	Kirkpatrick-Baezův	k2eAgFnSc1d1	Kirkpatrick-Baezův
či	či	k8xC	či
Wolterova	Wolterův	k2eAgFnSc1d1	Wolterův
optika	optika	k1gFnSc1	optika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vzniku	vznik	k1gInSc2	vznik
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
–	–	k?	–
tzv.	tzv.	kA	tzv.
brzdné	brzdný	k2eAgNnSc4d1	brzdné
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
charakteristické	charakteristický	k2eAgNnSc4d1	charakteristické
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Brzdné	brzdný	k2eAgNnSc4d1	brzdné
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
===	===	k?	===
</s>
</p>
<p>
<s>
Rychle	rychle	k6eAd1	rychle
letící	letící	k2eAgInPc1d1	letící
elektrony	elektron	k1gInPc1	elektron
se	se	k3xPyFc4	se
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
terč	terč	k1gInSc4	terč
brzdí	brzdit	k5eAaImIp3nS	brzdit
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
jejich	jejich	k3xOp3gFnSc2	jejich
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
elektrony	elektron	k1gInPc4	elektron
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
terčem	terč	k1gInSc7	terč
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyzáří	vyzářit	k5eAaPmIp3nS	vyzářit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tzv.	tzv.	kA	tzv.
brzdného	brzdný	k2eAgNnSc2d1	brzdné
rentgenova	rentgenův	k2eAgNnSc2d1	rentgenovo
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
širokým	široký	k2eAgNnSc7d1	široké
<g/>
,	,	kIx,	,
spojitým	spojitý	k2eAgNnSc7d1	spojité
energetickým	energetický	k2eAgNnSc7d1	energetické
spektrem	spektrum	k1gNnSc7	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
)	)	kIx)	)
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
tvrdší	tvrdý	k2eAgNnSc1d2	tvrdší
záření	záření	k1gNnSc1	záření
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
brzdného	brzdný	k2eAgNnSc2d1	brzdné
rentgenova	rentgenův	k2eAgNnSc2d1	rentgenovo
záření	záření	k1gNnSc2	záření
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
materiálu	materiál	k1gInSc6	materiál
terče	terč	k1gFnSc2	terč
(	(	kIx(	(
<g/>
např.	např.	kA	např.
anody	anoda	k1gFnSc2	anoda
rentgenovy	rentgenovy	k?	rentgenovy
trubice	trubice	k1gFnSc2	trubice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
napětí	napětí	k1gNnSc2	napětí
na	na	k7c6	na
anodě	anoda	k1gFnSc6	anoda
rentgenovy	rentgenovy	k?	rentgenovy
trubice	trubice	k1gFnSc2	trubice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
ale	ale	k9	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
urychleny	urychlit	k5eAaPmNgFnP	urychlit
i	i	k8xC	i
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
–	–	k?	–
v	v	k7c6	v
urychlovačích	urychlovač	k1gInPc6	urychlovač
částic	částice	k1gFnPc2	částice
např.	např.	kA	např.
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
lineárním	lineární	k2eAgInSc6d1	lineární
urychlovači	urychlovač	k1gInSc6	urychlovač
<g/>
,	,	kIx,	,
betatronu	betatron	k1gInSc6	betatron
<g/>
,	,	kIx,	,
mikrotronu	mikrotron	k1gInSc6	mikrotron
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výrazně	výrazně	k6eAd1	výrazně
vyšších	vysoký	k2eAgMnPc2d2	vyšší
energii	energie	k1gFnSc4	energie
než	než	k8xS	než
u	u	k7c2	u
rentgenovy	rentgenovy	k?	rentgenovy
trubice	trubice	k1gFnSc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
elektronvoltech	elektronvolt	k1gInPc6	elektronvolt
(	(	kIx(	(
<g/>
eV	eV	k?	eV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brzdné	brzdný	k2eAgNnSc1d1	brzdné
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
lékařské	lékařský	k2eAgFnSc6d1	lékařská
diagnostice	diagnostika	k1gFnSc6	diagnostika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
skiaskopie	skiaskopie	k1gFnSc2	skiaskopie
<g/>
,	,	kIx,	,
skiagrafie	skiagrafie	k1gFnSc2	skiagrafie
<g/>
,	,	kIx,	,
CT	CT	kA	CT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
radioterapii	radioterapie	k1gFnSc6	radioterapie
a	a	k8xC	a
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
v	v	k7c6	v
defektoskopii	defektoskopie	k1gFnSc6	defektoskopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Charakteristické	charakteristický	k2eAgNnSc4d1	charakteristické
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
===	===	k?	===
</s>
</p>
<p>
<s>
Charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
energie	energie	k1gFnSc1	energie
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
anodovém	anodový	k2eAgNnSc6d1	anodové
napětí	napětí	k1gNnSc6	napětí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
na	na	k7c6	na
materiálu	materiál	k1gInSc6	materiál
anody	anoda	k1gFnSc2	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
prvek	prvek	k1gInSc4	prvek
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
vyšší	vysoký	k2eAgFnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
protonové	protonový	k2eAgNnSc4d1	protonové
číslo	číslo	k1gNnSc4	číslo
materiálu	materiál	k1gInSc2	materiál
anody	anoda	k1gFnSc2	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
letící	letící	k2eAgInSc1d1	letící
elektron	elektron	k1gInSc1	elektron
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
odevzdá	odevzdat	k5eAaPmIp3nS	odevzdat
svou	svůj	k3xOyFgFnSc4	svůj
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
elektronu	elektron	k1gInSc2	elektron
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
slupky	slupka	k1gFnSc2	slupka
atomového	atomový	k2eAgInSc2d1	atomový
obalu	obal	k1gInSc2	obal
materiálu	materiál	k1gInSc2	materiál
anody	anoda	k1gFnSc2	anoda
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
excitaci	excitace	k1gFnSc3	excitace
nebo	nebo	k8xC	nebo
ionizaci	ionizace	k1gFnSc3	ionizace
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
základního	základní	k2eAgInSc2d1	základní
energetického	energetický	k2eAgInSc2d1	energetický
stavu	stav	k1gInSc2	stav
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
vyzářením	vyzáření	k1gNnSc7	vyzáření
fotonu	foton	k1gInSc2	foton
charakteristického	charakteristický	k2eAgNnSc2d1	charakteristické
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lékařské	lékařský	k2eAgNnSc1d1	lékařské
využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Röntgen	Röntgen	k1gInSc1	Röntgen
objevil	objevit	k5eAaPmAgInS	objevit
využití	využití	k1gNnSc3	využití
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnPc4	pozorování
kostních	kostní	k2eAgFnPc2d1	kostní
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
jeho	jeho	k3xOp3gNnSc1	jeho
užívání	užívání	k1gNnSc1	užívání
v	v	k7c6	v
lékařském	lékařský	k2eAgNnSc6d1	lékařské
snímkování	snímkování	k1gNnSc6	snímkování
<g/>
.	.	kIx.	.
</s>
<s>
Rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využito	využít	k5eAaPmNgNnS	využít
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
detailů	detail	k1gInPc2	detail
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
zubů	zub	k1gInPc2	zub
(	(	kIx(	(
<g/>
skiagrafie	skiagrafie	k1gFnSc1	skiagrafie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vhodných	vhodný	k2eAgFnPc2d1	vhodná
technik	technika	k1gFnPc2	technika
i	i	k9	i
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
měkké	měkký	k2eAgFnSc2d1	měkká
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
denzitografie	denzitografie	k1gFnSc1	denzitografie
<g/>
,	,	kIx,	,
subtrakční	subtrakční	k2eAgFnSc1d1	subtrakční
skiagrafie	skiagrafie	k1gFnSc1	skiagrafie
<g/>
,	,	kIx,	,
tomografie	tomografie	k1gFnSc1	tomografie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Radiologie	radiologie	k1gFnSc1	radiologie
je	být	k5eAaImIp3nS	být
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
obor	obor	k1gInSc4	obor
lékařství	lékařství	k1gNnSc2	lékařství
využívající	využívající	k2eAgFnSc2d1	využívající
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc3	záření
v	v	k7c6	v
diagnostice	diagnostika	k1gFnSc6	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejčastější	častý	k2eAgNnSc1d3	nejčastější
využití	využití	k1gNnSc1	využití
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
výzkumu	výzkum	k1gInSc2	výzkum
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
uplatnilo	uplatnit	k5eAaPmAgNnS	uplatnit
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgMnPc2d1	významný
vědců	vědec	k1gMnPc2	vědec
jako	jako	k9	jako
Ivan	Ivan	k1gMnSc1	Ivan
Puluj	Puluj	k1gMnSc1	Puluj
<g/>
,	,	kIx,	,
sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Crookes	Crookes	k1gMnSc1	Crookes
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Hittorf	Hittorf	k1gMnSc1	Hittorf
<g/>
,	,	kIx,	,
Eugene	Eugen	k1gInSc5	Eugen
Goldstein	Goldstein	k1gMnSc1	Goldstein
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hertz	hertz	k1gInSc1	hertz
<g/>
,	,	kIx,	,
Philipp	Philipp	k1gMnSc1	Philipp
Lenard	Lenard	k1gMnSc1	Lenard
<g/>
,	,	kIx,	,
Hermann	Hermann	k1gMnSc1	Hermann
von	von	k1gInSc4	von
Helmholtz	Helmholtz	k1gMnSc1	Helmholtz
<g/>
,	,	kIx,	,
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Glover	Glover	k1gMnSc1	Glover
Barkla	Barkla	k1gMnSc1	Barkla
a	a	k8xC	a
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Conrad	Conrada	k1gFnPc2	Conrada
Röntgen	Röntgen	k1gInSc1	Röntgen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fyzik	fyzik	k1gMnSc1	fyzik
Johann	Johann	k1gMnSc1	Johann
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Hittorf	Hittorf	k1gMnSc1	Hittorf
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
vakuovou	vakuový	k2eAgFnSc4d1	vakuová
trubici	trubice	k1gFnSc4	trubice
vyzařující	vyzařující	k2eAgNnSc1d1	vyzařující
záření	záření	k1gNnSc1	záření
na	na	k7c6	na
záporné	záporný	k2eAgFnSc6d1	záporná
elektrodě	elektroda	k1gFnSc6	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
stěnu	stěna	k1gFnSc4	stěna
trubice	trubice	k1gFnSc2	trubice
světélkování	světélkování	k1gNnSc2	světélkování
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
je	být	k5eAaImIp3nS	být
Eugene	Eugen	k1gInSc5	Eugen
Goldstein	Goldstein	k1gInSc4	Goldstein
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
katodové	katodový	k2eAgNnSc1d1	katodové
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
anglický	anglický	k2eAgMnSc1d1	anglický
fyzik	fyzik	k1gMnSc1	fyzik
William	William	k1gInSc4	William
Crookes	Crookes	k1gMnSc1	Crookes
studoval	studovat	k5eAaImAgMnS	studovat
výboje	výboj	k1gInPc4	výboj
v	v	k7c6	v
řídkých	řídký	k2eAgInPc6d1	řídký
plynech	plyn	k1gInPc6	plyn
a	a	k8xC	a
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
Crookesovu	Crookesův	k2eAgFnSc4d1	Crookesův
trubici	trubice	k1gFnSc4	trubice
<g/>
,	,	kIx,	,
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
trubici	trubice	k1gFnSc4	trubice
s	s	k7c7	s
elektrodami	elektroda	k1gFnPc7	elektroda
naplněnou	naplněný	k2eAgFnSc4d1	naplněná
zředěným	zředěný	k2eAgInSc7d1	zředěný
plynem	plyn	k1gInSc7	plyn
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
při	při	k7c6	při
přiložení	přiložení	k1gNnSc6	přiložení
vysokého	vysoký	k2eAgNnSc2d1	vysoké
stejnosměrného	stejnosměrný	k2eAgNnSc2d1	stejnosměrné
napětí	napětí	k1gNnSc2	napětí
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
výboji	výboj	k1gInSc3	výboj
doprovázenému	doprovázený	k2eAgInSc3d1	doprovázený
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
umístil	umístit	k5eAaPmAgMnS	umístit
neexponované	exponovaný	k2eNgFnPc4d1	neexponovaná
fotografické	fotografický	k2eAgFnPc4d1	fotografická
desky	deska	k1gFnPc4	deska
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
trubice	trubice	k1gFnSc2	trubice
<g/>
,	,	kIx,	,
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
šmouhy	šmouha	k1gFnPc1	šmouha
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
nechtěl	chtít	k5eNaImAgInS	chtít
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
Heinrich	Heinrich	k1gMnSc1	Heinrich
Hertz	hertz	k1gInSc4	hertz
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
katodové	katodový	k2eAgNnSc1d1	katodové
záření	záření	k1gNnSc1	záření
může	moct	k5eAaImIp3nS	moct
procházet	procházet	k5eAaImF	procházet
velmi	velmi	k6eAd1	velmi
slabou	slabý	k2eAgFnSc7d1	slabá
kovovou	kovový	k2eAgFnSc7d1	kovová
překážkou	překážka	k1gFnSc7	překážka
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hliníková	hliníkový	k2eAgFnSc1d1	hliníková
destička	destička	k1gFnSc1	destička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Philip	Philip	k1gMnSc1	Philip
Lenard	Lenard	k1gMnSc1	Lenard
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
Heinricha	Heinrich	k1gMnSc2	Heinrich
Hertze	hertz	k1gInSc5	hertz
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
prozkoumával	prozkoumávat	k5eAaImAgInS	prozkoumávat
tento	tento	k3xDgInSc4	tento
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
vlastní	vlastní	k2eAgFnSc4d1	vlastní
verzi	verze	k1gFnSc4	verze
katodové	katodový	k2eAgFnSc2d1	katodová
trubice	trubice	k1gFnSc2	trubice
a	a	k8xC	a
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
průchod	průchod	k1gInSc4	průchod
katodového	katodový	k2eAgNnSc2d1	katodové
záření	záření	k1gNnSc2	záření
rozličnými	rozličný	k2eAgInPc7d1	rozličný
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nezjistil	zjistit	k5eNaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1887	[number]	k4	1887
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
začal	začít	k5eAaPmAgMnS	začít
zkoumat	zkoumat	k5eAaImF	zkoumat
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
pomocí	pomocí	k7c2	pomocí
vysokého	vysoký	k2eAgNnSc2d1	vysoké
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
vakuových	vakuový	k2eAgFnPc2d1	vakuová
trubic	trubice	k1gFnPc2	trubice
vlastní	vlastní	k2eAgFnSc2d1	vlastní
konstrukce	konstrukce	k1gFnSc2	konstrukce
a	a	k8xC	a
Crookesových	Crookesový	k2eAgFnPc2d1	Crookesový
trubic	trubice	k1gFnPc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
technické	technický	k2eAgFnSc2d1	technická
dokumentace	dokumentace	k1gFnSc2	dokumentace
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
a	a	k8xC	a
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
trubici	trubice	k1gFnSc4	trubice
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
elektrodou	elektroda	k1gFnSc7	elektroda
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
trubice	trubice	k1gFnPc1	trubice
na	na	k7c4	na
zkoumání	zkoumání	k1gNnSc4	zkoumání
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
měly	mít	k5eAaImAgFnP	mít
dvě	dva	k4xCgFnPc4	dva
elektrody	elektroda	k1gFnPc4	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
výsledky	výsledek	k1gInPc7	výsledek
shrnul	shrnout	k5eAaPmAgMnS	shrnout
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
přednášce	přednáška	k1gFnSc6	přednáška
pro	pro	k7c4	pro
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
to	ten	k3xDgNnSc1	ten
jev	jev	k1gInSc1	jev
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
brzdné	brzdný	k2eAgNnSc1d1	brzdné
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
elektronů	elektron	k1gInPc2	elektron
<g/>
)	)	kIx)	)
látkou	látka	k1gFnSc7	látka
vzniká	vznikat	k5eAaImIp3nS	vznikat
druhotné	druhotný	k2eAgNnSc4d1	druhotné
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
Tesla	Tesla	k1gMnSc1	Tesla
provedl	provést	k5eAaPmAgMnS	provést
několik	několik	k4yIc4	několik
podobných	podobný	k2eAgInPc2d1	podobný
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezatřídil	zatřídit	k5eNaPmAgInS	zatřídit
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
produkty	produkt	k1gInPc4	produkt
jako	jako	k8xC	jako
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dnes	dnes	k6eAd1	dnes
zveme	zvát	k5eAaImIp1nP	zvát
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
označil	označit	k5eAaPmAgInS	označit
celý	celý	k2eAgInSc1d1	celý
jev	jev	k1gInSc1	jev
jako	jako	k8xC	jako
zářivou	zářivý	k2eAgFnSc4d1	zářivá
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
své	svůj	k3xOyFgInPc4	svůj
výsledky	výsledek	k1gInPc4	výsledek
nezveřejnil	zveřejnit	k5eNaPmAgMnS	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
další	další	k2eAgInPc1d1	další
experimenty	experiment	k1gInPc1	experiment
ho	on	k3xPp3gMnSc4	on
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
varování	varování	k1gNnSc3	varování
vědecké	vědecký	k2eAgFnSc2d1	vědecká
komunity	komunita	k1gFnSc2	komunita
před	před	k7c7	před
biologickými	biologický	k2eAgNnPc7d1	biologické
riziky	riziko	k1gNnPc7	riziko
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hermann	Hermann	k1gMnSc1	Hermann
von	von	k1gInSc1	von
Helmholtz	Helmholtz	k1gInSc4	Helmholtz
formuloval	formulovat	k5eAaImAgInS	formulovat
matematický	matematický	k2eAgInSc1d1	matematický
popis	popis	k1gInSc1	popis
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Předpověděl	předpovědět	k5eAaPmAgInS	předpovědět
disperzní	disperzní	k2eAgFnSc4d1	disperzní
teorii	teorie	k1gFnSc4	teorie
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
Röntgen	Röntgen	k1gInSc4	Röntgen
provedl	provést	k5eAaPmAgMnS	provést
a	a	k8xC	a
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
své	svůj	k3xOyFgInPc4	svůj
pokusy	pokus	k1gInPc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
formulována	formulovat	k5eAaImNgFnS	formulovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
teorie	teorie	k1gFnSc2	teorie
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
Wiedmann	Wiedmann	k1gInSc1	Wiedmann
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Annalen	Annalen	k2eAgInSc4d1	Annalen
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
</s>
<s>
XLVIII	XLVIII	kA	XLVIII
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1895	[number]	k4	1895
Wilhelm	Wilhelma	k1gFnPc2	Wilhelma
Conrad	Conrada	k1gFnPc2	Conrada
Röntgen	Röntgen	k1gInSc1	Röntgen
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
provádět	provádět	k5eAaImF	provádět
a	a	k8xC	a
zaznamenávat	zaznamenávat	k5eAaImF	zaznamenávat
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
rentgenovým	rentgenový	k2eAgNnSc7d1	rentgenové
zářením	záření	k1gNnSc7	záření
ve	v	k7c6	v
vakuové	vakuový	k2eAgFnSc6d1	vakuová
trubici	trubice	k1gFnSc6	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Röntgen	Röntgen	k1gInSc1	Röntgen
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1895	[number]	k4	1895
napsal	napsat	k5eAaPmAgInS	napsat
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
zprávu	zpráva	k1gFnSc4	zpráva
"	"	kIx"	"
<g/>
O	o	k7c6	o
novém	nový	k2eAgInSc6d1	nový
druhu	druh	k1gInSc6	druh
paprsků	paprsek	k1gInPc2	paprsek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poslal	poslat	k5eAaPmAgMnS	poslat
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
žurnálu	žurnál	k1gInSc2	žurnál
Würzburgské	Würzburgský	k2eAgFnSc2d1	Würzburgský
lékařské	lékařský	k2eAgFnSc2d1	lékařská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
formální	formální	k2eAgInSc1d1	formální
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
známá	známý	k2eAgFnSc1d1	známá
kategorizace	kategorizace	k1gFnSc1	kategorizace
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Röntgen	Röntgen	k1gInSc1	Röntgen
o	o	k7c4	o
záření	záření	k1gNnSc4	záření
psal	psát	k5eAaImAgMnS	psát
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
paprscích	paprsek	k1gInPc6	paprsek
X	X	kA	X
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
paprsek	paprsek	k1gInSc1	paprsek
a	a	k8xC	a
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
doposud	doposud	k6eAd1	doposud
neznámé	známý	k2eNgNnSc4d1	neznámé
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mnoho	mnoho	k4c1	mnoho
kolegů	kolega	k1gMnPc2	kolega
se	se	k3xPyFc4	se
domnívalo	domnívat	k5eAaImAgNnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jmenovat	jmenovat	k5eAaImF	jmenovat
po	po	k7c6	po
Röntgenovi	Röntgen	k1gMnSc6	Röntgen
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
češtině	čeština	k1gFnSc3	čeština
<g/>
,	,	kIx,	,
dánštině	dánština	k1gFnSc6	dánština
(	(	kIx(	(
<g/>
Rø	Rø	k1gFnSc6	Rø
<g/>
)	)	kIx)	)
či	či	k8xC	či
němčině	němčina	k1gFnSc6	němčina
(	(	kIx(	(
<g/>
Röntgenstrahlen	Röntgenstrahlen	k2eAgMnSc1d1	Röntgenstrahlen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Röntgen	Röntgen	k1gInSc1	Röntgen
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
objevy	objev	k1gInPc4	objev
obdržel	obdržet	k5eAaPmAgMnS	obdržet
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc4	první
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
schopnost	schopnost	k1gFnSc4	schopnost
materiálů	materiál	k1gInPc2	materiál
fluoreskovat	fluoreskovat	k5eAaImF	fluoreskovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
rentgenovému	rentgenový	k2eAgNnSc3d1	rentgenové
záření	záření	k1gNnSc3	záření
<g/>
,	,	kIx,	,
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
osvědčil	osvědčit	k5eAaPmAgInS	osvědčit
wolframan	wolframan	k1gInSc1	wolframan
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1896	[number]	k4	1896
se	se	k3xPyFc4	se
fluoroskop	fluoroskop	k1gInSc1	fluoroskop
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
standardem	standard	k1gInSc7	standard
lékařských	lékařský	k2eAgInPc2d1	lékařský
vyšetření	vyšetření	k1gNnSc4	vyšetření
rentgenovým	rentgenový	k2eAgNnSc7d1	rentgenové
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Edison	Edison	k1gMnSc1	Edison
ukončil	ukončit	k5eAaPmAgMnS	ukončit
výzkum	výzkum	k1gInSc4	výzkum
záření	záření	k1gNnSc2	záření
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Clarence	Clarenec	k1gMnSc2	Clarenec
Madison	Madison	k1gMnSc1	Madison
Dally	Dalla	k1gFnPc4	Dalla
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
foukačů	foukač	k1gMnPc2	foukač
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Dally	Dall	k1gInPc4	Dall
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
trubice	trubice	k1gFnPc4	trubice
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
přivodil	přivodit	k5eAaBmAgMnS	přivodit
jejich	jejich	k3xOp3gFnSc4	jejich
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
ruce	ruka	k1gFnPc1	ruka
mu	on	k3xPp3gNnSc3	on
byly	být	k5eAaImAgFnP	být
amputovány	amputován	k2eAgFnPc1d1	amputována
v	v	k7c6	v
marné	marný	k2eAgFnSc6d1	marná
snaze	snaha	k1gFnSc6	snaha
ho	on	k3xPp3gNnSc4	on
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlivem	vliv	k1gInSc7	vliv
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
fotografie	fotografia	k1gFnSc2	fotografia
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
rakouský	rakouský	k2eAgMnSc1d1	rakouský
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
Josef	Josef	k1gMnSc1	Josef
Maria	Mario	k1gMnSc4	Mario
Eder	Eder	k1gInSc1	Eder
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
působení	působení	k1gNnSc1	působení
rentgenových	rentgenový	k2eAgInPc2d1	rentgenový
paprsků	paprsek	k1gInPc2	paprsek
na	na	k7c4	na
filmový	filmový	k2eAgInSc4d1	filmový
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Eder	Eder	k1gMnSc1	Eder
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
výzkumu	výzkum	k1gInSc6	výzkum
vylepšení	vylepšení	k1gNnSc2	vylepšení
citlivosti	citlivost	k1gFnSc2	citlivost
fotografického	fotografický	k2eAgInSc2d1	fotografický
materiálu	materiál	k1gInSc2	materiál
na	na	k7c4	na
rentgen	rentgen	k1gInSc4	rentgen
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
fyzik	fyzik	k1gMnSc1	fyzik
Charles	Charles	k1gMnSc1	Charles
Glover	Glover	k1gMnSc1	Glover
Barkla	Barkla	k1gMnSc1	Barkla
objevil	objevit	k5eAaPmAgMnS	objevit
rozptyl	rozptyl	k1gInSc4	rozptyl
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
a	a	k8xC	a
využil	využít	k5eAaPmAgMnS	využít
ho	on	k3xPp3gNnSc4	on
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
vlastností	vlastnost	k1gFnPc2	vlastnost
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Určil	určit	k5eAaPmAgMnS	určit
tak	tak	k9	tak
například	například	k6eAd1	například
počet	počet	k1gInSc1	počet
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
dokázal	dokázat	k5eAaPmAgInS	dokázat
polarizovat	polarizovat	k5eAaImF	polarizovat
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgFnPc4d1	stejná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xS	jako
viditelné	viditelný	k2eAgNnSc4d1	viditelné
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
objevy	objev	k1gInPc4	objev
získal	získat	k5eAaPmAgInS	získat
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lékařské	lékařský	k2eAgNnSc1d1	lékařské
využití	využití	k1gNnSc1	využití
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ozařovací	ozařovací	k2eAgFnSc2d1	ozařovací
terapie	terapie	k1gFnSc2	terapie
<g/>
)	)	kIx)	)
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
John	John	k1gMnSc1	John
Hall-Edwards	Hall-Edwards	k1gInSc4	Hall-Edwards
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
mu	on	k3xPp3gNnSc3	on
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
amputována	amputován	k2eAgFnSc1d1	amputována
ruka	ruka	k1gFnSc1	ruka
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nádoru	nádor	k1gInSc2	nádor
z	z	k7c2	z
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
sestrojen	sestrojen	k2eAgInSc4d1	sestrojen
rentgenový	rentgenový	k2eAgInSc4d1	rentgenový
mikroskop	mikroskop	k1gInSc4	mikroskop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Neher	Neher	k1gMnSc1	Neher
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Ludwig	Ludwig	k1gMnSc1	Ludwig
<g/>
:	:	kIx,	:
Röntgen	Röntgen	k1gInSc1	Röntgen
:	:	kIx,	:
román	román	k1gInSc1	román
badatele	badatel	k1gMnSc2	badatel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
</s>
</p>
<p>
<s>
Vaněrka	Vaněrka	k1gFnSc1	Vaněrka
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
:	:	kIx,	:
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
C.	C.	kA	C.
Rentgen	rentgen	k1gInSc1	rentgen
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Horizont	horizont	k1gInSc1	horizont
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
lebky	lebka	k1gFnPc1	lebka
a	a	k8xC	a
tváře	tvář	k1gFnPc1	tvář
předků	předek	k1gMnPc2	předek
(	(	kIx(	(
<g/>
Několik	několik	k4yIc4	několik
vynálezů	vynález	k1gInPc2	vynález
<g/>
:	:	kIx,	:
rentgen	rentgen	k1gInSc1	rentgen
<g/>
,	,	kIx,	,
ultrazvuk	ultrazvuk	k1gInSc1	ultrazvuk
<g/>
,	,	kIx,	,
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Zneklidňující	zneklidňující	k2eAgInSc4d1	zneklidňující
svět	svět	k1gInSc4	svět
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Malvern	Malvern	k1gInSc1	Malvern
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
spektrum	spektrum	k1gNnSc1	spektrum
</s>
</p>
<p>
<s>
Inverzní	inverzní	k2eAgInSc1d1	inverzní
fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
záření	záření	k1gNnSc4	záření
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
radiologických	radiologický	k2eAgInPc2d1	radiologický
obrázků	obrázek	k1gInPc2	obrázek
na	na	k7c4	na
atlas	atlas	k1gInSc4	atlas
<g/>
.	.	kIx.	.
<g/>
mudr.	mudr.	k?	mudr.
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
radiologických	radiologický	k2eAgInPc2d1	radiologický
obrázků	obrázek	k1gInPc2	obrázek
setříděných	setříděný	k2eAgInPc2d1	setříděný
podle	podle	k7c2	podle
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
vyšetřované	vyšetřovaný	k2eAgFnSc2d1	vyšetřovaná
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
popis	popis	k1gInSc4	popis
patologie	patologie	k1gFnSc2	patologie
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
radiologieplzen	radiologieplzen	k2eAgInSc1d1	radiologieplzen
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
webový	webový	k2eAgInSc1d1	webový
portál	portál	k1gInSc1	portál
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
hledající	hledající	k2eAgFnSc1d1	hledající
více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
zobrazovacích	zobrazovací	k2eAgFnPc6d1	zobrazovací
metodách	metoda	k1gFnPc6	metoda
<g/>
.	.	kIx.	.
</s>
</p>
