<s>
Uzsa	Uzsa	k6eAd1
</s>
<s>
Uzsa	Uzsa	k6eAd1
Letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
část	část	k1gFnSc4
Uzsy	Uzsa	k1gFnSc2
a	a	k8xC
čedičový	čedičový	k2eAgInSc4d1
důl	důl	k1gInSc4
Bazaltbánya	Bazaltbány	k1gInSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
43	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
8	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+1	+1	k4
Stát	stát	k1gInSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
Župa	župa	k1gFnSc1
</s>
<s>
Veszprém	Veszprý	k2eAgNnSc6d1
Okres	okres	k1gInSc1
</s>
<s>
Tapolca	Tapolca	k6eAd1
</s>
<s>
Uzsa	Uzsa	k6eAd1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
11,5	11,5	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
303	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
26,3	26,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
Obec	obec	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
László	László	k?
Táborosi	Táborose	k1gFnSc3
Vznik	vznik	k1gInSc4
</s>
<s>
1287	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
uzsa	uzsa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
hu	hu	k0
PSČ	PSČ	kA
</s>
<s>
8321	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Uzsa	Uzsa	k1gFnSc1
[	[	kIx(
<g/>
uža	uža	k1gFnSc1
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
v	v	k7c6
župě	župa	k1gFnSc6
Veszprém	Veszprý	k2eAgNnSc6d1
<g/>
,	,	kIx,
spadající	spadající	k2eAgMnSc1d1
pod	pod	k7c4
okres	okres	k1gInSc4
Tapolca	Tapolca	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
asi	asi	k9
9	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Tapolcy	Tapolca	k1gFnSc2
<g/>
,	,	kIx,
10	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Sümegu	Sümeg	k1gInSc2
a	a	k8xC
asi	asi	k9
57	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Veszprému	Veszprém	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
303	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
tvoří	tvořit	k5eAaImIp3nS
72,2	72,2	k4
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
Maďaři	Maďar	k1gMnPc1
a	a	k8xC
0,3	0,3	k4
%	%	kIx~
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
27,8	27,8	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnSc1
se	se	k3xPyFc4
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
národnosti	národnost	k1gFnSc3
nevyjádřilo	vyjádřit	k5eNaPmAgNnS
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
</s>
<s>
Sümeg	Sümeg	k1gMnSc1
</s>
<s>
Tapolca	Tapolca	k6eAd1
</s>
<s>
Várvölgy	Várvölga	k1gFnPc1
</s>
<s>
Lesencetomaj	Lesencetomaj	k1gFnSc1
</s>
<s>
Uzsa	Uzsa	k6eAd1
</s>
<s>
Lesenceistvánd	Lesenceistvánd	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Uzsa	Uzs	k1gInSc2
na	na	k7c6
maďarské	maďarský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
někteří	některý	k3yIgMnPc1
obyvatelé	obyvatel	k1gMnPc1
přihlásili	přihlásit	k5eAaPmAgMnP
k	k	k7c3
více	hodně	k6eAd2
národnostem	národnost	k1gFnPc3
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
výsledný	výsledný	k2eAgInSc1d1
počet	počet	k1gInSc1
procent	procento	k1gNnPc2
větší	veliký	k2eAgMnSc1d2
než	než	k8xS
100	#num#	k4
%	%	kIx~
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okres	okres	k1gInSc1
Tapolca	Tapolc	k1gInSc2
</s>
<s>
Ábrahámhegy	Ábrahámhega	k1gFnPc1
•	•	k?
Badacsonytomaj	Badacsonytomaj	k1gMnSc1
•	•	k?
Badacsonytördemic	Badacsonytördemic	k1gMnSc1
•	•	k?
Balatonederics	Balatonederics	k1gInSc1
•	•	k?
Balatonhenye	Balatonhenye	k1gInSc1
•	•	k?
Balatonrendes	Balatonrendes	k1gInSc1
•	•	k?
Gyulakeszi	Gyulakesze	k1gFnSc4
•	•	k?
Hegyesd	Hegyesd	k1gMnSc1
•	•	k?
Hegymagas	Hegymagas	k1gMnSc1
•	•	k?
Kapolcs	Kapolcs	k1gInSc1
•	•	k?
Káptalantóti	Káptalantóť	k1gFnSc2
•	•	k?
Kékkút	Kékkúta	k1gFnPc2
•	•	k?
Kisapáti	Kisapát	k1gMnPc1
•	•	k?
Kővágóörs	Kővágóörs	k1gInSc1
•	•	k?
Köveskál	Köveskál	k1gInSc1
•	•	k?
Lesencefalu	Lesencefal	k1gInSc2
•	•	k?
Lesenceistvánd	Lesenceistvánd	k1gMnSc1
•	•	k?
Lesencetomaj	Lesencetomaj	k1gMnSc1
•	•	k?
Mindszentkálla	Mindszentkálla	k1gMnSc1
•	•	k?
Monostorapáti	Monostorapát	k1gMnPc1
•	•	k?
Nemesgulács	Nemesgulács	k1gInSc1
•	•	k?
Nemesvita	Nemesvita	k1gFnSc1
•	•	k?
Raposka	Raposka	k1gFnSc1
•	•	k?
Révfülöp	Révfülöp	k1gMnSc1
•	•	k?
Salföld	Salföld	k1gMnSc1
•	•	k?
Sáska	Sáska	k1gMnSc1
•	•	k?
Szentbékkálla	Szentbékkálla	k1gMnSc1
•	•	k?
Szigliget	Szigliget	k1gMnSc1
•	•	k?
Taliándörögd	Taliándörögd	k1gMnSc1
•	•	k?
Tapolca	Tapolca	k1gMnSc1
•	•	k?
Uzsa	Uzsa	k1gMnSc1
•	•	k?
Vigántpetend	Vigántpetend	k1gMnSc1
•	•	k?
Zalahaláp	Zalahaláp	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Maďarsko	Maďarsko	k1gNnSc1
</s>
