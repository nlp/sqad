<p>
<s>
Futurologie	futurologie	k1gFnSc1	futurologie
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
též	též	k9	též
zabývá	zabývat	k5eAaImIp3nS	zabývat
výzkumem	výzkum	k1gInSc7	výzkum
možných	možný	k2eAgFnPc2d1	možná
budoucností	budoucnost	k1gFnPc2	budoucnost
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
alternativ	alternativa	k1gFnPc2	alternativa
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
tento	tento	k3xDgMnSc1	tento
termín	termín	k1gInSc4	termín
použil	použít	k5eAaPmAgMnS	použít
Karl	Karl	k1gMnSc1	Karl
Osip	Osip	k1gMnSc1	Osip
Flechtheim	Flechtheim	k1gMnSc1	Flechtheim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Futurologie	futurologie	k1gFnSc1	futurologie
je	být	k5eAaImIp3nS	být
vědním	vědní	k2eAgNnSc7d1	vědní
odvětvím	odvětví	k1gNnSc7	odvětví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
předpovídáním	předpovídání	k1gNnSc7	předpovídání
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Užití	užití	k1gNnSc1	užití
tohoto	tento	k3xDgInSc2	tento
oboru	obor	k1gInSc2	obor
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
,	,	kIx,	,
teologii	teologie	k1gFnSc6	teologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
designu	design	k1gInSc6	design
<g/>
,	,	kIx,	,
technologii	technologie	k1gFnSc6	technologie
a	a	k8xC	a
biologii	biologie	k1gFnSc6	biologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
PETRÁŠEK	PETRÁŠEK	kA	PETRÁŠEK
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Futurologická	futurologický	k2eAgNnPc1d1	Futurologické
studia	studio	k1gNnPc1	studio
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Oeconomica	Oeconomica	k1gMnSc1	Oeconomica
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
272	[number]	k4	272
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
1517	[number]	k4	1517
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Prognostika	prognostika	k1gFnSc1	prognostika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Prognostický	prognostický	k2eAgInSc1d1	prognostický
ústav	ústav	k1gInSc1	ústav
SAV	SAV	kA	SAV
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
federace	federace	k1gFnSc1	federace
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
budoucností	budoucnost	k1gFnSc7	budoucnost
</s>
</p>
<p>
<s>
Futurologia	Futurologia	k1gFnSc1	Futurologia
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
