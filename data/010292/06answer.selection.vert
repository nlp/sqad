<s>
Jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
minerály	minerál	k1gInPc4	minerál
je	být	k5eAaImIp3nS	být
přijímán	přijímat	k5eAaImNgMnS	přijímat
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
nikoli	nikoli	k9	nikoli
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
elektroneutrální	elektroneutrální	k2eAgFnSc1d1	elektroneutrální
kyselina	kyselina	k1gFnSc1	kyselina
boritá	boritý	k2eAgFnSc1d1	boritá
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
BO	BO	k?	BO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
