<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
Kožlany	Kožlana	k1gFnSc2	Kožlana
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1948	[number]	k4	1948
Sezimovo	Sezimův	k2eAgNnSc4d1	Sezimovo
Ústí	ústit	k5eAaImIp3nS	ústit
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgMnSc1	druhý
československý	československý	k2eAgMnSc1d1	československý
prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
Druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
až	až	k9	až
do	do	k7c2	do
osvobození	osvobození	k1gNnSc2	osvobození
Československa	Československo	k1gNnSc2	Československo
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznaný	uznaný	k2eAgMnSc1d1	uznaný
exilový	exilový	k2eAgMnSc1d1	exilový
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
československým	československý	k2eAgMnSc7d1	československý
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
prvního	první	k4xOgInSc2	první
československého	československý	k2eAgInSc2d1	československý
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
československého	československý	k2eAgInSc2d1	československý
odboje	odboj	k1gInSc2	odboj
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Kožlanech	Kožlan	k1gInPc6	Kožlan
na	na	k7c6	na
Rakovnicku	Rakovnicko	k1gNnSc6	Rakovnicko
jako	jako	k8xS	jako
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
dítě	dítě	k1gNnSc1	dítě
<g/>
)	)	kIx)	)
rolníka	rolník	k1gMnSc2	rolník
Matěje	Matěj	k1gMnSc2	Matěj
Beneše	Beneš	k1gMnSc2	Beneš
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Anny	Anna	k1gFnSc2	Anna
Petronily	Petronila	k1gFnSc2	Petronila
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
-	-	kIx~	-
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Benešové	Benešové	k2eAgFnSc1d1	Benešové
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
sourozenců	sourozenec	k1gMnPc2	sourozenec
byl	být	k5eAaImAgInS	být
pozdější	pozdní	k2eAgMnSc1d2	pozdější
politik	politik	k1gMnSc1	politik
Vojta	Vojta	k1gMnSc1	Vojta
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
studoval	studovat	k5eAaImAgInS	studovat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Praze-Vinohradech	Praze-Vinohrad	k1gInPc6	Praze-Vinohrad
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
bydlel	bydlet	k5eAaImAgMnS	bydlet
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
domě	dům	k1gInSc6	dům
rodiny	rodina	k1gFnSc2	rodina
Oličovy	Oličův	k2eAgFnSc2d1	Oličova
<g/>
,	,	kIx,	,
spřátelené	spřátelený	k2eAgFnSc2d1	spřátelená
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc7d2	pozdější
ženou	žena	k1gFnSc7	žena
Hanou	Hana	k1gFnSc7	Hana
Benešovou	Benešová	k1gFnSc7	Benešová
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Annou	Anna	k1gFnSc7	Anna
Vlčkovou	Vlčková	k1gFnSc7	Vlčková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
studoval	studovat	k5eAaImAgMnS	studovat
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
pražské	pražský	k2eAgFnSc2d1	Pražská
Univerzity	univerzita	k1gFnSc2	univerzita
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1	Karlo-Ferdinandova
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
a	a	k8xC	a
Svobodné	svobodný	k2eAgFnSc3d1	svobodná
škole	škola	k1gFnSc3	škola
politických	politický	k2eAgFnPc2d1	politická
nauk	nauka	k1gFnPc2	nauka
(	(	kIx(	(
<g/>
Ecole	Ecol	k1gMnSc5	Ecol
libre	libr	k1gMnSc5	libr
des	des	k1gNnSc7	des
sciences	sciences	k1gInSc1	sciences
politiques	politiques	k1gInSc1	politiques
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgInSc1d1	dnešní
Institut	institut	k1gInSc1	institut
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
études	études	k1gMnSc1	études
politiques	politiques	k1gMnSc1	politiques
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
pak	pak	k6eAd1	pak
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
zasnoubil	zasnoubit	k5eAaPmAgInS	zasnoubit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
pozdější	pozdní	k2eAgFnSc7d2	pozdější
ženou	žena	k1gFnSc7	žena
Hanou	Hana	k1gFnSc7	Hana
a	a	k8xC	a
změnil	změnit	k5eAaPmAgInS	změnit
si	se	k3xPyFc3	se
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
z	z	k7c2	z
Eduarda	Eduard	k1gMnSc2	Eduard
na	na	k7c6	na
Edvard	Edvard	k1gMnSc1	Edvard
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgNnPc1d1	francouzské
studia	studio	k1gNnPc1	studio
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
završil	završit	k5eAaPmAgMnS	završit
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
na	na	k7c6	na
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c4	v
Dijonu	Dijona	k1gFnSc4	Dijona
doktorskou	doktorský	k2eAgFnSc7d1	doktorská
prací	práce	k1gFnSc7	práce
(	(	kIx(	(
<g/>
téma	téma	k1gFnSc1	téma
Problém	problém	k1gInSc4	problém
rakouský	rakouský	k2eAgInSc1d1	rakouský
a	a	k8xC	a
otázka	otázka	k1gFnSc1	otázka
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
o	o	k7c6	o
politických	politický	k2eAgInPc6d1	politický
bojích	boj	k1gInPc6	boj
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
diplom	diplom	k1gInSc1	diplom
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
pražská	pražský	k2eAgFnSc1d1	Pražská
univerzita	univerzita	k1gFnSc1	univerzita
neuznala	uznat	k5eNaPmAgFnS	uznat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
pak	pak	k6eAd1	pak
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
složil	složit	k5eAaPmAgMnS	složit
rigorózní	rigorózní	k2eAgFnPc4d1	rigorózní
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
filosofie	filosofie	k1gFnSc2	filosofie
(	(	kIx(	(
<g/>
práce	práce	k1gFnSc2	práce
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
moderního	moderní	k2eAgInSc2d1	moderní
politického	politický	k2eAgInSc2d1	politický
individualismu	individualismus	k1gInSc2	individualismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
lednu	ledno	k1gNnSc6wB	ledno
zemřela	zemřít	k5eAaPmAgFnS	zemřít
maminka	maminka	k1gFnSc1	maminka
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Hanu	Hana	k1gFnSc4	Hana
<g/>
,	,	kIx,	,
svatba	svatba	k1gFnSc1	svatba
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Ludmily	Ludmila	k1gFnSc2	Ludmila
<g/>
;	;	kIx,	;
v	v	k7c6	v
matrice	matrika	k1gFnSc6	matrika
jsou	být	k5eAaImIp3nP	být
křestní	křestní	k2eAgNnPc1d1	křestní
jména	jméno	k1gNnPc1	jméno
snoubenců	snoubenec	k1gMnPc2	snoubenec
zapsána	zapsat	k5eAaPmNgFnS	zapsat
ještě	ještě	k9	ještě
jako	jako	k9	jako
Eduard	Eduard	k1gMnSc1	Eduard
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
čekali	čekat	k5eAaImAgMnP	čekat
manželé	manžel	k1gMnPc1	manžel
Benešovi	Beneš	k1gMnSc3	Beneš
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
ale	ale	k8xC	ale
dítě	dítě	k1gNnSc4	dítě
nedonosila	donosit	k5eNaPmAgFnS	donosit
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
(	(	kIx(	(
<g/>
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
české	český	k2eAgMnPc4d1	český
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
studia	studio	k1gNnSc2	studio
však	však	k9	však
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
(	(	kIx(	(
<g/>
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
Beneš	Beneš	k1gMnSc1	Beneš
obdržel	obdržet	k5eAaPmAgMnS	obdržet
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
práv	práv	k2eAgInSc4d1	práv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
přednášel	přednášet	k5eAaImAgMnS	přednášet
jako	jako	k9	jako
docent	docent	k1gMnSc1	docent
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1	Karlo-Ferdinandova
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Rakouska	Rakousko	k1gNnSc2	Rakousko
neměly	mít	k5eNaImAgFnP	mít
ani	ani	k9	ani
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
ani	ani	k8xC	ani
její	její	k3xOp3gFnSc1	její
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
i	i	k8xC	i
semináře	seminář	k1gInPc1	seminář
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
musely	muset	k5eAaImAgFnP	muset
konat	konat	k5eAaImF	konat
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Beneš	Beneš	k1gMnSc1	Beneš
přednášel	přednášet	k5eAaImAgMnS	přednášet
v	v	k7c6	v
Krakovské	krakovský	k2eAgFnSc6d1	Krakovská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Klementinu	Klementinum	k1gNnSc6	Klementinum
či	či	k8xC	či
v	v	k7c6	v
Kaulichově	Kaulichův	k2eAgInSc6d1	Kaulichův
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
organizoval	organizovat	k5eAaBmAgMnS	organizovat
Beneš	Beneš	k1gMnSc1	Beneš
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
odboj	odboj	k1gInSc4	odboj
(	(	kIx(	(
<g/>
Maffie	Maffie	k1gFnSc1	Maffie
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
výbor	výbor	k1gInSc1	výbor
odboje	odboj	k1gInSc2	odboj
proti	proti	k7c3	proti
Rakousko-Uherské	rakouskoherský	k2eAgFnSc3d1	rakousko-uherská
monarchii	monarchie	k1gFnSc3	monarchie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
spojení	spojení	k1gNnSc4	spojení
odboje	odboj	k1gInSc2	odboj
s	s	k7c7	s
T.	T.	kA	T.
G.	G.	kA	G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1915	[number]	k4	1915
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Masarykem	Masaryk	k1gMnSc7	Masaryk
a	a	k8xC	a
se	s	k7c7	s
Slovákem	Slovák	k1gMnSc7	Slovák
(	(	kIx(	(
<g/>
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
francouzským	francouzský	k2eAgMnSc7d1	francouzský
leteckým	letecký	k2eAgMnSc7d1	letecký
důstojníkem	důstojník	k1gMnSc7	důstojník
<g/>
)	)	kIx)	)
Milanem	Milan	k1gMnSc7	Milan
Rastislavem	Rastislav	k1gMnSc7	Rastislav
Štefánikem	Štefánik	k1gMnSc7	Štefánik
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
Benešová	Benešová	k1gFnSc1	Benešová
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
politickým	politický	k2eAgFnPc3d1	politická
aktivitám	aktivita	k1gFnPc3	aktivita
manžela	manžel	k1gMnSc2	manžel
byla	být	k5eAaImAgNnP	být
rakouskými	rakouský	k2eAgInPc7d1	rakouský
úřady	úřad	k1gInPc7	úřad
několikrát	několikrát	k6eAd1	několikrát
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úspěch	úspěch	k1gInSc4	úspěch
celé	celý	k2eAgFnSc2d1	celá
revoluční	revoluční	k2eAgFnSc2d1	revoluční
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
rozbití	rozbití	k1gNnSc4	rozbití
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
a	a	k8xC	a
dosažení	dosažení	k1gNnSc4	dosažení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
používán	používán	k2eAgInSc1d1	používán
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
československý	československý	k2eAgInSc1d1	československý
národ	národ	k1gInSc1	národ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
Beneš	Beneš	k1gMnSc1	Beneš
a	a	k8xC	a
Štefánik	Štefánik	k1gMnSc1	Štefánik
měli	mít	k5eAaImAgMnP	mít
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
dřívějších	dřívější	k2eAgInPc2d1	dřívější
pobytů	pobyt	k1gInPc2	pobyt
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
potřebné	potřebný	k2eAgFnSc2d1	potřebná
znalosti	znalost	k1gFnSc2	znalost
poměrů	poměr	k1gInPc2	poměr
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
politické	politický	k2eAgFnSc6d1	politická
situaci	situace	k1gFnSc6	situace
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
hlavních	hlavní	k2eAgMnPc2d1	hlavní
vůdců	vůdce	k1gMnPc2	vůdce
československého	československý	k2eAgInSc2d1	československý
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získal	získat	k5eAaPmAgMnS	získat
mnoho	mnoho	k4c4	mnoho
zkušeností	zkušenost	k1gFnPc2	zkušenost
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
předchozího	předchozí	k2eAgInSc2d1	předchozí
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
za	za	k7c2	za
několikaměsíčního	několikaměsíční	k2eAgInSc2d1	několikaměsíční
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
také	také	k9	také
za	za	k7c2	za
ročního	roční	k2eAgInSc2d1	roční
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
dočasném	dočasný	k2eAgInSc6d1	dočasný
návratu	návrat	k1gInSc6	návrat
domů	domů	k6eAd1	domů
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
dalšímu	další	k2eAgNnSc3d1	další
soustavnému	soustavný	k2eAgNnSc3d1	soustavné
studiu	studio	k1gNnSc3	studio
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
organizoval	organizovat	k5eAaBmAgMnS	organizovat
Beneš	Beneš	k1gMnSc1	Beneš
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
protirakouský	protirakouský	k2eAgInSc4d1	protirakouský
odboj	odboj	k1gInSc4	odboj
a	a	k8xC	a
reorganizoval	reorganizovat	k5eAaBmAgInS	reorganizovat
kurýrní	kurýrní	k2eAgFnSc4d1	kurýrní
službu	služba	k1gFnSc4	služba
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Maffií	Maffie	k1gFnSc7	Maffie
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
cyklus	cyklus	k1gInSc1	cyklus
přednášek	přednáška	k1gFnPc2	přednáška
o	o	k7c6	o
Slovanstvu	Slovanstvo	k1gNnSc6	Slovanstvo
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
a	a	k8xC	a
propagoval	propagovat	k5eAaImAgMnS	propagovat
československý	československý	k2eAgInSc4d1	československý
politický	politický	k2eAgInSc4d1	politický
program	program	k1gInSc4	program
řadou	řada	k1gFnSc7	řada
článků	článek	k1gInPc2	článek
ve	v	k7c6	v
francouzských	francouzský	k2eAgFnPc6d1	francouzská
novinách	novina	k1gFnPc6	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
ustanovení	ustanovení	k1gNnSc4	ustanovení
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zastával	zastávat	k5eAaImAgMnS	zastávat
místo	místo	k7c2	místo
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
Štefánikem	Štefánik	k1gInSc7	Štefánik
získal	získat	k5eAaPmAgInS	získat
souhlas	souhlas	k1gInSc1	souhlas
dohodových	dohodový	k2eAgFnPc2d1	dohodová
mocností	mocnost	k1gFnPc2	mocnost
se	s	k7c7	s
zakládáním	zakládání	k1gNnSc7	zakládání
československých	československý	k2eAgFnPc2d1	Československá
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgInS	přispět
tak	tak	k6eAd1	tak
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
samostatných	samostatný	k2eAgFnPc2d1	samostatná
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
zapojily	zapojit	k5eAaPmAgInP	zapojit
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
výsledkem	výsledek	k1gInSc7	výsledek
Benešovy	Benešův	k2eAgFnSc2d1	Benešova
diplomacie	diplomacie	k1gFnSc2	diplomacie
bylo	být	k5eAaImAgNnS	být
uznání	uznání	k1gNnSc1	uznání
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
jako	jako	k8xS	jako
představitele	představitel	k1gMnSc2	představitel
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
Francií	Francie	k1gFnPc2	Francie
<g/>
,	,	kIx,	,
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
jednání	jednání	k1gNnSc4	jednání
zástupců	zástupce	k1gMnPc2	zástupce
domácího	domácí	k2eAgMnSc2d1	domácí
i	i	k8xC	i
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
o	o	k7c6	o
budoucí	budoucí	k2eAgFnSc6d1	budoucí
podobě	podoba	k1gFnSc6	podoba
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Karla	Karel	k1gMnSc4	Karel
Kramáře	kramář	k1gMnSc4	kramář
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
však	však	k9	však
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k6eAd1	až
v	v	k7c6	v
září	září	k1gNnSc6	září
1919	[number]	k4	1919
po	po	k7c6	po
svých	svůj	k3xOyFgNnPc6	svůj
úspěšných	úspěšný	k2eAgNnPc6d1	úspěšné
jednáních	jednání	k1gNnPc6	jednání
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zajistil	zajistit	k5eAaPmAgMnS	zajistit
nové	nový	k2eAgFnSc2d1	nová
hranice	hranice	k1gFnSc2	hranice
státu	stát	k1gInSc2	stát
vůči	vůči	k7c3	vůči
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pozdější	pozdní	k2eAgNnSc4d2	pozdější
zahájení	zahájení	k1gNnSc4	zahájení
jednání	jednání	k1gNnSc2	jednání
o	o	k7c6	o
smlouvě	smlouva	k1gFnSc6	smlouva
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
pověřil	pověřit	k5eAaPmAgInS	pověřit
dalším	další	k2eAgNnSc7d1	další
zastupováním	zastupování	k1gNnSc7	zastupování
Československa	Československo	k1gNnSc2	Československo
Slováka	Slovák	k1gMnSc2	Slovák
Štefana	Štefan	k1gMnSc2	Štefan
Osuského	Osuský	k1gMnSc2	Osuský
<g/>
,	,	kIx,	,
mimořádného	mimořádný	k2eAgMnSc2d1	mimořádný
a	a	k8xC	a
zplnomocněného	zplnomocněný	k2eAgMnSc2d1	zplnomocněný
vyslance	vyslanec	k1gMnSc2	vyslanec
ČSR	ČSR	kA	ČSR
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vypracováním	vypracování	k1gNnSc7	vypracování
celé	celá	k1gFnSc2	celá
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Podepsána	podepsán	k2eAgFnSc1d1	podepsána
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1920	[number]	k4	1920
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Velký	velký	k2eAgInSc1d1	velký
Trianon	Trianon	k1gInSc1	Trianon
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
Versailles	Versailles	k1gFnSc2	Versailles
poblíž	poblíž	k7c2	poblíž
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Trianonská	trianonský	k2eAgFnSc1d1	Trianonská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
také	také	k9	také
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
zakládat	zakládat	k5eAaImF	zakládat
Společnost	společnost	k1gFnSc4	společnost
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
místopředseda	místopředseda	k1gMnSc1	místopředseda
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Rady	rada	k1gFnSc2	rada
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
předseda	předseda	k1gMnSc1	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Prosazoval	prosazovat	k5eAaImAgInS	prosazovat
politiku	politika	k1gFnSc4	politika
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
orientoval	orientovat	k5eAaBmAgInS	orientovat
na	na	k7c4	na
poválečnou	poválečný	k2eAgFnSc4d1	poválečná
evropskou	evropský	k2eAgFnSc4d1	Evropská
velmoc	velmoc	k1gFnSc4	velmoc
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	nízce	k6eAd2	nízce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
Československo	Československo	k1gNnSc1	Československo
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
země	zem	k1gFnPc4	zem
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
a	a	k8xC	a
Jugoslávii	Jugoslávie	k1gFnSc4	Jugoslávie
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
smluvně	smluvně	k6eAd1	smluvně
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
obranný	obranný	k2eAgInSc1d1	obranný
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Malou	malý	k2eAgFnSc4d1	malá
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
zastával	zastávat	k5eAaImAgMnS	zastávat
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
funkci	funkce	k1gFnSc4	funkce
československého	československý	k2eAgMnSc2d1	československý
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
vláda	vláda	k1gFnSc1	vláda
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslancem	poslanec	k1gMnSc7	poslanec
parlamentu	parlament	k1gInSc2	parlament
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
a	a	k8xC	a
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
místopředsedou	místopředseda	k1gMnSc7	místopředseda
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
národně	národně	k6eAd1	národně
sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
významně	významně	k6eAd1	významně
ovlivňoval	ovlivňovat	k5eAaImAgMnS	ovlivňovat
její	její	k3xOp3gFnSc4	její
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
po	po	k7c6	po
obtížných	obtížný	k2eAgNnPc6d1	obtížné
parlamentních	parlamentní	k2eAgNnPc6d1	parlamentní
jednáních	jednání	k1gNnPc6	jednání
byl	být	k5eAaImAgMnS	být
Beneš	Beneš	k1gMnSc1	Beneš
zvolen	zvolen	k2eAgMnSc1d1	zvolen
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
druhým	druhý	k4xOgNnSc7	druhý
prezidentem	prezident	k1gMnSc7	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
Československo	Československo	k1gNnSc1	Československo
Benešovým	Benešův	k2eAgNnSc7d1	Benešovo
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
(	(	kIx(	(
<g/>
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
přijetí	přijetí	k1gNnSc6	přijetí
do	do	k7c2	do
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
případě	případ	k1gInSc6	případ
konfliktu	konflikt	k1gInSc2	konflikt
ovšem	ovšem	k9	ovšem
smlouva	smlouva	k1gFnSc1	smlouva
vázala	vázat	k5eAaImAgFnS	vázat
na	na	k7c4	na
předchozí	předchozí	k2eAgFnSc4d1	předchozí
pomoc	pomoc	k1gFnSc4	pomoc
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Nigela	Nigel	k1gMnSc2	Nigel
Westa	West	k1gMnSc2	West
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
projektu	projekt	k1gInSc2	projekt
Venona	Venon	k1gMnSc2	Venon
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
údajně	údajně	k6eAd1	údajně
sovětským	sovětský	k2eAgMnSc7d1	sovětský
informátorem	informátor	k1gMnSc7	informátor
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
19	[number]	k4	19
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zmiňovaným	zmiňovaný	k2eAgMnSc7d1	zmiňovaný
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
depeši	depeše	k1gFnSc6	depeše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
prý	prý	k9	prý
patřilo	patřit	k5eAaImAgNnS	patřit
Benešovi	Beneš	k1gMnSc3	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Obdobná	obdobný	k2eAgNnPc1d1	obdobné
tvrzení	tvrzení	k1gNnPc1	tvrzení
uvádějí	uvádět	k5eAaImIp3nP	uvádět
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
Viktor	Viktor	k1gMnSc1	Viktor
Suvorov	Suvorovo	k1gNnPc2	Suvorovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
Benešově	Benešův	k2eAgFnSc6d1	Benešova
předválečné	předválečný	k2eAgFnSc6d1	předválečná
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
tajnou	tajný	k2eAgFnSc7d1	tajná
službou	služba	k1gFnSc7	služba
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zprávy	zpráva	k1gFnSc2	zpráva
ruské	ruský	k2eAgFnSc2d1	ruská
Služby	služba	k1gFnSc2	služba
vnější	vnější	k2eAgFnSc2d1	vnější
rozvědky	rozvědka	k1gFnSc2	rozvědka
i	i	k8xC	i
slovenský	slovenský	k2eAgInSc1d1	slovenský
deník	deník	k1gInSc1	deník
Pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
důvěryhodnosti	důvěryhodnost	k1gFnSc6	důvěryhodnost
sovětských	sovětský	k2eAgInPc2d1	sovětský
archivních	archivní	k2eAgInPc2d1	archivní
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Sovětského	sovětský	k2eAgInSc2d1	sovětský
zdroje	zdroj	k1gInSc2	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
agenta	agent	k1gMnSc4	agent
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
19	[number]	k4	19
<g/>
"	"	kIx"	"
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
autoři	autor	k1gMnPc1	autor
jako	jako	k8xC	jako
Eduard	Eduard	k1gMnSc1	Eduard
Mark	Mark	k1gMnSc1	Mark
a	a	k8xC	a
američtí	americký	k2eAgMnPc1d1	americký
autoři	autor	k1gMnPc1	autor
Herbert	Herbert	k1gMnSc1	Herbert
Romerstein	Romerstein	k1gMnSc1	Romerstein
a	a	k8xC	a
Eric	Eric	k1gFnSc1	Eric
Breindel	Breindlo	k1gNnPc2	Breindlo
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
19	[number]	k4	19
<g/>
"	"	kIx"	"
měl	mít	k5eAaImAgMnS	mít
Rooseveltův	Rooseveltův	k2eAgMnSc1d1	Rooseveltův
poradce	poradce	k1gMnSc1	poradce
Harry	Harra	k1gFnSc2	Harra
Hopkins	Hopkins	k1gInSc1	Hopkins
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
amerických	americký	k2eAgMnPc2d1	americký
autorů	autor	k1gMnPc2	autor
Johna	John	k1gMnSc2	John
Earla	earl	k1gMnSc2	earl
Haynese	Haynese	k1gFnSc2	Haynese
a	a	k8xC	a
Harvey	Harvea	k1gFnSc2	Harvea
Klehra	Klehra	k1gFnSc1	Klehra
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
krycí	krycí	k2eAgNnSc4d1	krycí
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
19	[number]	k4	19
<g/>
"	"	kIx"	"
i	i	k8xC	i
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
britské	britský	k2eAgFnSc2d1	britská
delegace	delegace	k1gFnSc2	delegace
na	na	k7c6	na
Washingtonské	washingtonský	k2eAgFnSc6d1	Washingtonská
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
důstojník	důstojník	k1gMnSc1	důstojník
KGB	KGB	kA	KGB
Alexander	Alexandra	k1gFnPc2	Alexandra
Vasiljev	Vasiljev	k1gFnPc2	Vasiljev
<g/>
,	,	kIx,	,
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
19	[number]	k4	19
<g/>
"	"	kIx"	"
patřilo	patřit	k5eAaImAgNnS	patřit
pracovníkovi	pracovník	k1gMnSc3	pracovník
amerického	americký	k2eAgNnSc2d1	americké
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnSc7	věc
Laurenci	Laurence	k1gFnSc4	Laurence
Dugganovi	Duggan	k1gMnSc3	Duggan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
abdikoval	abdikovat	k5eAaBmAgInS	abdikovat
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
odletěl	odletět	k5eAaPmAgInS	odletět
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
leteckou	letecký	k2eAgFnSc7d1	letecká
linkou	linka	k1gFnSc7	linka
nejdříve	dříve	k6eAd3	dříve
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1939	[number]	k4	1939
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
zaslal	zaslat	k5eAaPmAgMnS	zaslat
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
německé	německý	k2eAgFnSc3d1	německá
okupaci	okupace	k1gFnSc3	okupace
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
vládám	vládat	k5eAaImIp1nS	vládat
hlavních	hlavní	k2eAgFnPc2d1	hlavní
světových	světový	k2eAgFnPc2d1	světová
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
vůdčím	vůdčí	k2eAgMnSc7d1	vůdčí
představitelem	představitel	k1gMnSc7	představitel
československého	československý	k2eAgInSc2d1	československý
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
i	i	k9	i
přes	přes	k7c4	přes
spory	spora	k1gFnPc4	spora
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
dalšími	další	k2eAgMnPc7d1	další
exilovými	exilový	k2eAgMnPc7d1	exilový
představiteli	představitel	k1gMnPc7	představitel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Štefan	Štefan	k1gMnSc1	Štefan
Osuský	Osuský	k1gMnSc1	Osuský
nebo	nebo	k8xC	nebo
Milan	Milan	k1gMnSc1	Milan
Hodža	Hodža	k1gMnSc1	Hodža
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
prozatímně	prozatímně	k6eAd1	prozatímně
uznána	uznán	k2eAgFnSc1d1	uznána
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
plného	plný	k2eAgNnSc2d1	plné
uznání	uznání	k1gNnSc2	uznání
Beneš	Beneš	k1gMnSc1	Beneš
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
SSSR	SSSR	kA	SSSR
i	i	k9	i
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc2	Británie
definitivně	definitivně	k6eAd1	definitivně
uznaly	uznat	k5eAaPmAgFnP	uznat
československou	československý	k2eAgFnSc4d1	Československá
exilovou	exilový	k2eAgFnSc4d1	exilová
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
představitel	představitel	k1gMnSc1	představitel
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
se	se	k3xPyFc4	se
Beneš	Beneš	k1gMnSc1	Beneš
účastnil	účastnit	k5eAaImAgMnS	účastnit
diskuzí	diskuze	k1gFnSc7	diskuze
o	o	k7c6	o
budoucím	budoucí	k2eAgInSc6d1	budoucí
osudu	osud	k1gInSc6	osud
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
nejprve	nejprve	k6eAd1	nejprve
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
pouze	pouze	k6eAd1	pouze
menší	malý	k2eAgFnPc4d2	menší
úpravy	úprava	k1gFnPc4	úprava
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
částečný	částečný	k2eAgInSc4d1	částečný
transfer	transfer	k1gInSc4	transfer
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jeho	jeho	k3xOp3gInPc1	jeho
návrhy	návrh	k1gInPc1	návrh
byly	být	k5eAaImAgInP	být
odmítnuty	odmítnut	k2eAgMnPc4d1	odmítnut
vojenským	vojenský	k2eAgInSc7d1	vojenský
odbojem	odboj	k1gInSc7	odboj
v	v	k7c6	v
protektorátu	protektorát	k1gInSc6	protektorát
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gInSc7	jehož
vlivem	vliv	k1gInSc7	vliv
se	se	k3xPyFc4	se
diskuze	diskuze	k1gFnSc1	diskuze
o	o	k7c6	o
budoucím	budoucí	k2eAgInSc6d1	budoucí
odsunu	odsun	k1gInSc6	odsun
Němců	Němec	k1gMnPc2	Němec
radikalizovaly	radikalizovat	k5eAaBmAgInP	radikalizovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Benešova	Benešův	k2eAgFnSc1d1	Benešova
vláda	vláda	k1gFnSc1	vláda
postupně	postupně	k6eAd1	postupně
prosadila	prosadit	k5eAaPmAgFnS	prosadit
u	u	k7c2	u
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
velmocí	velmoc	k1gFnPc2	velmoc
odsun	odsun	k1gInSc4	odsun
naprosté	naprostý	k2eAgFnSc2d1	naprostá
většiny	většina	k1gFnSc2	většina
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
mocnosti	mocnost	k1gFnPc1	mocnost
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
na	na	k7c6	na
postupimské	postupimský	k2eAgFnSc6d1	Postupimská
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
s	s	k7c7	s
vedoucím	vedoucí	k1gMnSc7	vedoucí
představitelem	představitel	k1gMnSc7	představitel
sudetoněmeckého	sudetoněmecký	k2eAgInSc2d1	sudetoněmecký
sociálně-demokratického	sociálněemokratický	k2eAgInSc2d1	sociálně-demokratický
exilu	exil	k1gInSc2	exil
Wenzelem	Wenzel	k1gMnSc7	Wenzel
Jakschem	Jaksch	k1gMnSc7	Jaksch
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
předmětem	předmět	k1gInSc7	předmět
byl	být	k5eAaImAgInS	být
možný	možný	k2eAgInSc1d1	možný
odsun	odsun	k1gInSc1	odsun
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
Jakschův	Jakschův	k2eAgInSc1d1	Jakschův
názor	názor	k1gInSc1	názor
na	na	k7c4	na
platnost	platnost	k1gFnSc4	platnost
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
rukování	rukování	k1gNnSc2	rukování
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
do	do	k7c2	do
československé	československý	k2eAgFnSc2d1	Československá
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
třech	tři	k4xCgInPc6	tři
letech	léto	k1gNnPc6	léto
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgInP	být
snahy	snaha	k1gFnPc4	snaha
jak	jak	k8xC	jak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
polské	polský	k2eAgFnSc2d1	polská
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Británie	Británie	k1gFnSc2	Británie
o	o	k7c4	o
poválečné	poválečný	k2eAgNnSc4d1	poválečné
federativní	federativní	k2eAgNnSc4d1	federativní
uspořádání	uspořádání	k1gNnSc4	uspořádání
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Beneš	Beneš	k1gMnSc1	Beneš
jen	jen	k6eAd1	jen
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
snahou	snaha	k1gFnSc7	snaha
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
federace	federace	k1gFnSc2	federace
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Československa	Československo	k1gNnSc2	Československo
však	však	k9	však
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
realistická	realistický	k2eAgNnPc1d1	realistické
pro	pro	k7c4	pro
negativní	negativní	k2eAgInSc4d1	negativní
postoj	postoj	k1gInSc4	postoj
polské	polský	k2eAgFnSc2d1	polská
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
a	a	k8xC	a
vice	vika	k1gFnSc6	vika
versa	versa	k1gFnSc1	versa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
vzrůstajícím	vzrůstající	k2eAgInPc3d1	vzrůstající
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
představiteli	představitel	k1gMnPc7	představitel
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
se	se	k3xPyFc4	se
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
snažil	snažit	k5eAaImAgMnS	snažit
budoucí	budoucí	k2eAgMnSc1d1	budoucí
ČSR	ČSR	kA	ČSR
pojistit	pojistit	k5eAaPmF	pojistit
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
smlouvami	smlouva	k1gFnPc7	smlouva
s	s	k7c7	s
hlavními	hlavní	k2eAgFnPc7d1	hlavní
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
šlo	jít	k5eAaImAgNnS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
smlouva	smlouva	k1gFnSc1	smlouva
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
silné	silný	k2eAgFnSc2d1	silná
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
neposouvá	posouvat	k5eNaImIp3nS	posouvat
ČSR	ČSR	kA	ČSR
příliš	příliš	k6eAd1	příliš
do	do	k7c2	do
sovětské	sovětský	k2eAgFnSc2d1	sovětská
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
výrazem	výraz	k1gInSc7	výraz
nové	nový	k2eAgFnSc2d1	nová
zahraničněpolitické	zahraničněpolitický	k2eAgFnSc2d1	zahraničněpolitická
orientace	orientace	k1gFnSc2	orientace
československé	československý	k2eAgFnSc2d1	Československá
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
Beneš	Beneš	k1gMnSc1	Beneš
také	také	k9	také
začal	začít	k5eAaPmAgMnS	začít
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
exilové	exilový	k2eAgFnSc2d1	exilová
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
o	o	k7c6	o
poválečné	poválečný	k2eAgFnSc6d1	poválečná
podobě	podoba	k1gFnSc6	podoba
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
článek	článek	k1gInSc4	článek
Heliodor	Heliodor	k1gInSc1	Heliodor
Píka	píka	k1gFnSc1	píka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1945	[number]	k4	1945
Beneš	Beneš	k1gMnSc1	Beneš
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
přes	přes	k7c4	přes
Moskvu	Moskva	k1gFnSc4	Moskva
na	na	k7c4	na
osvobozené	osvobozený	k2eAgNnSc4d1	osvobozené
území	území	k1gNnSc4	území
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
ve	v	k7c6	v
slovenských	slovenský	k2eAgInPc6d1	slovenský
Košicích	Košice	k1gInPc6	Košice
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
první	první	k4xOgFnSc4	první
poválečnou	poválečný	k2eAgFnSc4d1	poválečná
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
následně	následně	k6eAd1	následně
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Československa	Československo	k1gNnSc2	Československo
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
okupace	okupace	k1gFnSc2	okupace
a	a	k8xC	a
neexistence	neexistence	k1gFnSc2	neexistence
parlamentu	parlament	k1gInSc2	parlament
vydávala	vydávat	k5eAaPmAgFnS	vydávat
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
dekrety	dekret	k1gInPc4	dekret
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
Beneš	Beneš	k1gMnSc1	Beneš
jako	jako	k8xS	jako
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
činitel	činitel	k1gMnSc1	činitel
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
a	a	k8xC	a
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
nepřesně	přesně	k6eNd1	přesně
nazývány	nazýván	k2eAgFnPc4d1	nazývána
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Benešovy	Benešův	k2eAgInPc1d1	Benešův
dekrety	dekret	k1gInPc1	dekret
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poválečné	poválečný	k2eAgInPc1d1	poválečný
dekrety	dekret	k1gInPc1	dekret
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
upravovala	upravovat	k5eAaImAgFnS	upravovat
konfiskace	konfiskace	k1gFnSc1	konfiskace
majetku	majetek	k1gInSc2	majetek
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
,	,	kIx,	,
zrádců	zrádce	k1gMnPc2	zrádce
a	a	k8xC	a
kolaborantů	kolaborant	k1gMnPc2	kolaborant
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
čs	čs	kA	čs
<g/>
.	.	kIx.	.
občanství	občanství	k1gNnPc2	občanství
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
práv	právo	k1gNnPc2	právo
občanů	občan	k1gMnPc2	občan
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
maďarské	maďarský	k2eAgFnSc2d1	maďarská
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
jejich	jejich	k3xOp3gMnPc1	jejich
vysídlení	vysídlený	k2eAgMnPc1d1	vysídlený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
znárodnění	znárodnění	k1gNnSc1	znárodnění
většiny	většina	k1gFnSc2	většina
československého	československý	k2eAgInSc2d1	československý
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
bez	bez	k7c2	bez
náhrady	náhrada	k1gFnSc2	náhrada
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
vyhnanců	vyhnanec	k1gMnPc2	vyhnanec
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Sudetendeutsche	Sudetendeutsche	k1gInSc1	Sudetendeutsche
Landsmannschaft	Landsmannschaft	k2eAgInSc1d1	Landsmannschaft
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
marně	marně	k6eAd1	marně
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
"	"	kIx"	"
<g/>
Benešových	Benešových	k2eAgInPc2d1	Benešových
dekretů	dekret	k1gInPc2	dekret
<g/>
"	"	kIx"	"
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
občanů	občan	k1gMnPc2	občan
Československa	Československo	k1gNnSc2	Československo
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
maďarské	maďarský	k2eAgFnSc2d1	maďarská
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
Československa	Československo	k1gNnSc2	Československo
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgFnP	existovat
sice	sice	k8xC	sice
nadále	nadále	k6eAd1	nadále
různé	různý	k2eAgFnPc4d1	různá
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
však	však	k9	však
sdružené	sdružený	k2eAgInPc1d1	sdružený
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
1946	[number]	k4	1946
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připadlo	připadnout	k5eAaPmAgNnS	připadnout
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
114	[number]	k4	114
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
mandátů	mandát	k1gInPc2	mandát
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
300	[number]	k4	300
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
38	[number]	k4	38
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
předseda	předseda	k1gMnSc1	předseda
KSČ	KSČ	kA	KSČ
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
a	a	k8xC	a
zvolen	zvolit	k5eAaPmNgMnS	zvolit
novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
vzrůstajícím	vzrůstající	k2eAgInSc7d1	vzrůstající
vlivem	vliv	k1gInSc7	vliv
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
také	také	k9	také
díky	díky	k7c3	díky
uzavřené	uzavřený	k2eAgFnSc3d1	uzavřená
smlouvě	smlouva	k1gFnSc3	smlouva
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
směřovalo	směřovat	k5eAaImAgNnS	směřovat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
udržení	udržení	k1gNnSc1	udržení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
podalo	podat	k5eAaPmAgNnS	podat
demisi	demise	k1gFnSc4	demise
12	[number]	k4	12
ministrů	ministr	k1gMnPc2	ministr
nekomunistických	komunistický	k2eNgFnPc2d1	nekomunistická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
podali	podat	k5eAaPmAgMnP	podat
demisi	demise	k1gFnSc4	demise
další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
ministři	ministr	k1gMnPc1	ministr
(	(	kIx(	(
<g/>
z	z	k7c2	z
26	[number]	k4	26
<g/>
členné	členný	k2eAgFnSc2d1	členná
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
chtěli	chtít	k5eAaImAgMnP	chtít
ministři	ministr	k1gMnPc1	ministr
buď	buď	k8xC	buď
donutit	donutit	k5eAaPmF	donutit
komunisty	komunista	k1gMnPc4	komunista
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
podřídili	podřídit	k5eAaPmAgMnP	podřídit
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
vyvolat	vyvolat	k5eAaPmF	vyvolat
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nS	by
řešilo	řešit	k5eAaImAgNnS	řešit
jmenování	jmenování	k1gNnSc4	jmenování
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
vypsání	vypsání	k1gNnSc4	vypsání
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Konečnou	konečný	k2eAgFnSc7d1	konečná
příčinou	příčina	k1gFnSc7	příčina
k	k	k7c3	k
podání	podání	k1gNnSc3	podání
demise	demise	k1gFnSc2	demise
bylo	být	k5eAaImAgNnS	být
náhlé	náhlý	k2eAgNnSc1d1	náhlé
obsazení	obsazení	k1gNnSc1	obsazení
všech	všecek	k3xTgNnPc2	všecek
vedoucích	vedoucí	k2eAgNnPc2d1	vedoucí
míst	místo	k1gNnPc2	místo
ve	v	k7c6	v
Sboru	sbor	k1gInSc6	sbor
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
policie	policie	k1gFnSc2	policie
<g/>
)	)	kIx)	)
komunisty	komunista	k1gMnSc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
představitelů	představitel	k1gMnPc2	představitel
demokratických	demokratický	k2eAgFnPc2d1	demokratická
sil	síla	k1gFnPc2	síla
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gFnPc4	on
Beneš	Beneš	k1gMnSc1	Beneš
postaví	postavit	k5eAaPmIp3nS	postavit
a	a	k8xC	a
demisi	demise	k1gFnSc4	demise
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Nekomunistické	komunistický	k2eNgFnPc1d1	nekomunistická
strany	strana	k1gFnPc1	strana
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
měly	mít	k5eAaImAgInP	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
omezené	omezený	k2eAgInPc4d1	omezený
komunikační	komunikační	k2eAgInPc4d1	komunikační
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
komunisté	komunista	k1gMnPc1	komunista
ovládali	ovládat	k5eAaImAgMnP	ovládat
rozhlas	rozhlas	k1gInSc4	rozhlas
(	(	kIx(	(
<g/>
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
informací	informace	k1gFnPc2	informace
<g/>
)	)	kIx)	)
a	a	k8xC	a
díky	dík	k1gInPc7	dík
přídělu	příděl	k1gInSc2	příděl
papíru	papír	k1gInSc2	papír
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
omezovat	omezovat	k5eAaImF	omezovat
prodemokratický	prodemokratický	k2eAgInSc4d1	prodemokratický
tisk	tisk	k1gInSc4	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
tisku	tisk	k1gInSc6	tisk
předáci	předák	k1gMnPc1	předák
nekomunistických	komunistický	k2eNgFnPc2d1	nekomunistická
stran	strana	k1gFnPc2	strana
místo	místo	k7c2	místo
mobilizace	mobilizace	k1gFnSc2	mobilizace
vlastních	vlastní	k2eAgMnPc2d1	vlastní
příznivců	příznivec	k1gMnPc2	příznivec
vyzývali	vyzývat	k5eAaImAgMnP	vyzývat
ke	k	k7c3	k
klidu	klid	k1gInSc3	klid
a	a	k8xC	a
ujišťovali	ujišťovat	k5eAaImAgMnP	ujišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
legální	legální	k2eAgInSc4d1	legální
<g/>
,	,	kIx,	,
ústavní	ústavní	k2eAgInSc4d1	ústavní
a	a	k8xC	a
parlamentní	parlamentní	k2eAgNnSc4d1	parlamentní
řešení	řešení	k1gNnSc4	řešení
únorové	únorový	k2eAgFnSc2d1	únorová
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tlaku	tlak	k1gInSc3	tlak
komunistů	komunista	k1gMnPc2	komunista
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
únorové	únorový	k2eAgFnSc6d1	únorová
krizi	krize	k1gFnSc6	krize
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
otevřeně	otevřeně	k6eAd1	otevřeně
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
postavili	postavit	k5eAaPmAgMnP	postavit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
pochodu	pochod	k1gInSc6	pochod
na	na	k7c4	na
Hrad	hrad	k1gInSc4	hrad
následně	následně	k6eAd1	následně
surově	surově	k6eAd1	surově
zbiti	zbít	k5eAaPmNgMnP	zbít
v	v	k7c6	v
Nerudově	Nerudův	k2eAgFnSc6d1	Nerudova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Komunisti	Komunisti	k?	Komunisti
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ovládali	ovládat	k5eAaImAgMnP	ovládat
Sbor	sbor	k1gInSc4	sbor
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
policie	policie	k1gFnSc2	policie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
sdělovací	sdělovací	k2eAgInPc1d1	sdělovací
prostředky	prostředek	k1gInPc1	prostředek
(	(	kIx(	(
<g/>
vydali	vydat	k5eAaPmAgMnP	vydat
zákaz	zákaz	k1gInSc4	zákaz
dovozu	dovoz	k1gInSc2	dovoz
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
tisku	tisk	k1gInSc2	tisk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Beneš	Beneš	k1gMnSc1	Beneš
demisi	demise	k1gFnSc4	demise
ministrů	ministr	k1gMnPc2	ministr
po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
nátlaku	nátlak	k1gInSc6	nátlak
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
přijal	přijmout	k5eAaPmAgInS	přijmout
a	a	k8xC	a
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
měl	mít	k5eAaImAgInS	mít
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
nové	nový	k2eAgFnPc4d1	nová
volby	volba	k1gFnPc4	volba
při	při	k7c6	při
rezignaci	rezignace	k1gFnSc6	rezignace
nadpoloviční	nadpoloviční	k2eAgFnSc2d1	nadpoloviční
většiny	většina	k1gFnSc2	většina
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
13	[number]	k4	13
<g/>
,	,	kIx,	,
pověřil	pověřit	k5eAaPmAgMnS	pověřit
předsedu	předseda	k1gMnSc4	předseda
KSČ	KSČ	kA	KSČ
Klementa	Klement	k1gMnSc4	Klement
Gottwalda	Gottwald	k1gMnSc4	Gottwald
opětovným	opětovný	k2eAgNnSc7d1	opětovné
sestavením	sestavení	k1gNnSc7	sestavení
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pak	pak	k6eAd1	pak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
pod	pod	k7c7	pod
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
už	už	k9	už
Beneš	Beneš	k1gMnSc1	Beneš
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
vážně	vážně	k6eAd1	vážně
nemocen	nemocen	k2eAgMnSc1d1	nemocen
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
trpěl	trpět	k5eAaImAgMnS	trpět
Ménierovou	Ménierův	k2eAgFnSc7d1	Ménierova
nemocí	nemoc	k1gFnSc7	nemoc
a	a	k8xC	a
častými	častý	k2eAgFnPc7d1	častá
migrénami	migréna	k1gFnPc7	migréna
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Již	jenž	k3xRgFnSc4	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
postihlo	postihnout	k5eAaPmAgNnS	postihnout
mozkové	mozkový	k2eAgNnSc1d1	mozkové
krvácení	krvácení	k1gNnSc1	krvácení
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1944	[number]	k4	1944
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
poprvé	poprvé	k6eAd1	poprvé
cévní	cévní	k2eAgFnSc4d1	cévní
mozkovou	mozkový	k2eAgFnSc4d1	mozková
příhodu	příhoda	k1gFnSc4	příhoda
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1945	[number]	k4	1945
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
navštívil	navštívit	k5eAaPmAgMnS	navštívit
předseda	předseda	k1gMnSc1	předseda
národních	národní	k2eAgInPc2d1	národní
socialistů	socialist	k1gMnPc2	socialist
Petr	Petr	k1gMnSc1	Petr
Zenkl	Zenkl	k1gInSc1	Zenkl
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Benešovým	Benešův	k2eAgInSc7d1	Benešův
tělesným	tělesný	k2eAgInSc7d1	tělesný
i	i	k8xC	i
duševním	duševní	k2eAgInSc7d1	duševní
stavem	stav	k1gInSc7	stav
zděšen	zděšen	k2eAgMnSc1d1	zděšen
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1947	[number]	k4	1947
jej	on	k3xPp3gMnSc4	on
postihl	postihnout	k5eAaPmAgInS	postihnout
záchvat	záchvat	k1gInSc1	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
;	;	kIx,	;
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
pod	pod	k7c7	pod
stálým	stálý	k2eAgInSc7d1	stálý
lékařským	lékařský	k2eAgInSc7d1	lékařský
dohledem	dohled	k1gInSc7	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
musel	muset	k5eAaImAgMnS	muset
chodit	chodit	k5eAaImF	chodit
o	o	k7c4	o
holi	hole	k1gFnSc4	hole
<g/>
,	,	kIx,	,
trpěl	trpět	k5eAaImAgMnS	trpět
nesnesitelnými	snesitelný	k2eNgFnPc7d1	nesnesitelná
bolestmi	bolest	k1gFnPc7	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
i	i	k8xC	i
ztrátou	ztráta	k1gFnSc7	ztráta
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k9	i
výrazně	výrazně	k6eAd1	výrazně
slinil	slinit	k5eAaImAgMnS	slinit
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
odplivávat	odplivávat	k5eAaImF	odplivávat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
postižení	postižený	k1gMnPc1	postižený
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
výrazněji	výrazně	k6eAd2	výrazně
zhoršovalo	zhoršovat	k5eAaImAgNnS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1948	[number]	k4	1948
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
ještě	ještě	k6eAd1	ještě
pokusil	pokusit	k5eAaPmAgMnS	pokusit
komunistům	komunista	k1gMnPc3	komunista
vzepřít	vzepřít	k5eAaPmF	vzepřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
novou	nový	k2eAgFnSc4d1	nová
československou	československý	k2eAgFnSc4d1	Československá
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
bylo	být	k5eAaImAgNnS	být
zlikvidováno	zlikvidován	k2eAgNnSc1d1	zlikvidováno
demokratické	demokratický	k2eAgNnSc1d1	demokratické
státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
Beneš	Beneš	k1gMnSc1	Beneš
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgMnS	stát
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Sezimově	Sezimův	k2eAgNnSc6d1	Sezimovo
Ústí	ústí	k1gNnSc6	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
v	v	k7c6	v
parku	park	k1gInSc6	park
své	svůj	k3xOyFgFnSc2	svůj
vily	vila	k1gFnSc2	vila
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Hanou	Hana	k1gFnSc7	Hana
pochován	pochovat	k5eAaPmNgMnS	pochovat
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
později	pozdě	k6eAd2	pozdě
o	o	k7c6	o
Benešovi	Beneš	k1gMnSc6	Beneš
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
únorovými	únorový	k2eAgFnPc7d1	únorová
událostmi	událost	k1gFnPc7	událost
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zápolil	zápolit	k5eAaImAgInS	zápolit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
uprostřed	uprostřed	k7c2	uprostřed
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
schválila	schválit	k5eAaPmAgFnS	schválit
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
opakovaně	opakovaně	k6eAd1	opakovaně
přes	přes	k7c4	přes
předchozí	předchozí	k2eAgNnSc4d1	předchozí
veto	veto	k1gNnSc4	veto
Senátu	senát	k1gInSc2	senát
zákon	zákon	k1gInSc1	zákon
292	[number]	k4	292
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
tvořený	tvořený	k2eAgInSc1d1	tvořený
větou	věta	k1gFnSc7	věta
"	"	kIx"	"
<g/>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
tento	tento	k3xDgInSc4	tento
zákon	zákon	k1gInSc4	zákon
nevetoval	vetovat	k5eNaBmAgMnS	vetovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nepodepsal	podepsat	k5eNaPmAgMnS	podepsat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
jisté	jistý	k2eAgFnPc4d1	jistá
spory	spora	k1gFnPc4	spora
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
na	na	k7c4	na
takové	takový	k3xDgNnSc4	takový
jednání	jednání	k1gNnSc4	jednání
dle	dle	k7c2	dle
ústavy	ústava	k1gFnSc2	ústava
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Úloha	úloha	k1gFnSc1	úloha
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
v	v	k7c6	v
československých	československý	k2eAgFnPc6d1	Československá
dějinách	dějiny	k1gFnPc6	dějiny
je	být	k5eAaImIp3nS	být
nazírána	nazírán	k2eAgFnSc1d1	nazírána
některými	některý	k3yIgMnPc7	některý
publicisty	publicista	k1gMnPc7	publicista
negativně	negativně	k6eAd1	negativně
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
přičítán	přičítán	k2eAgInSc4d1	přičítán
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
zločinech	zločin	k1gInPc6	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
až	až	k9	až
příliš	příliš	k6eAd1	příliš
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
úvahy	úvaha	k1gFnSc2	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
"	"	kIx"	"
<g/>
mostem	most	k1gInSc7	most
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
západními	západní	k2eAgFnPc7d1	západní
demokraciemi	demokracie	k1gFnPc7	demokracie
a	a	k8xC	a
napomoci	napomoct	k5eAaPmF	napomoct
tak	tak	k6eAd1	tak
stabilitě	stabilita	k1gFnSc3	stabilita
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
sociolog	sociolog	k1gMnSc1	sociolog
byl	být	k5eAaImAgMnS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
politika	politika	k1gFnSc1	politika
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
praktická	praktický	k2eAgFnSc1d1	praktická
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
politika	politika	k1gFnSc1	politika
je	být	k5eAaImIp3nS	být
vědecká	vědecký	k2eAgFnSc1d1	vědecká
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
Čechoslováci	Čechoslovák	k1gMnPc1	Čechoslovák
<g/>
,	,	kIx,	,
nám	my	k3xPp1nPc3	my
Rusům	Rus	k1gMnPc3	Rus
nedůvěřujete	důvěřovat	k5eNaImIp2nP	důvěřovat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k9	ani
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Beneši	Beneš	k1gMnSc3	Beneš
<g/>
,	,	kIx,	,
nám	my	k3xPp1nPc3	my
nedůvěřujete	důvěřovat	k5eNaImIp2nP	důvěřovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Avšak	avšak	k8xC	avšak
já	já	k3xPp1nSc1	já
vás	vy	k3xPp2nPc4	vy
ujišťuji	ujišťovat	k5eAaImIp1nS	ujišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dodržíme	dodržet	k5eAaPmIp1nP	dodržet
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsme	být	k5eAaImIp1nP	být
s	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
-	-	kIx~	-
že	že	k8xS	že
Československo	Československo	k1gNnSc1	Československo
bude	být	k5eAaImBp3nS	být
svobodná	svobodný	k2eAgFnSc1d1	svobodná
a	a	k8xC	a
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
země	země	k1gFnSc1	země
a	a	k8xC	a
že	že	k8xS	že
my	my	k3xPp1nPc1	my
se	se	k3xPyFc4	se
nebudeme	být	k5eNaImBp1nP	být
vměšovat	vměšovat	k5eAaImF	vměšovat
do	do	k7c2	do
vašich	váš	k3xOp2gFnPc2	váš
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
budete	být	k5eAaImBp2nP	být
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
takové	takový	k3xDgNnSc1	takový
to	ten	k3xDgNnSc1	ten
budete	být	k5eAaImBp2nP	být
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
přípitek	přípitek	k1gInSc4	přípitek
na	na	k7c4	na
večeři	večeře	k1gFnSc4	večeře
na	na	k7c4	na
Benešovu	Benešův	k2eAgFnSc4d1	Benešova
počest	počest	k1gFnSc4	počest
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1945	[number]	k4	1945
vracel	vracet	k5eAaImAgMnS	vracet
z	z	k7c2	z
londýnského	londýnský	k2eAgInSc2d1	londýnský
exilu	exil	k1gInSc2	exil
přes	přes	k7c4	přes
Moskvu	Moskva	k1gFnSc4	Moskva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dlouho	dlouho	k6eAd1	dlouho
jsem	být	k5eAaImIp1nS	být
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
alespoň	alespoň	k9	alespoň
Gottwald	Gottwald	k1gMnSc1	Gottwald
mi	já	k3xPp1nSc3	já
nelže	lhát	k5eNaImIp3nS	lhát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
vidím	vidět	k5eAaImIp1nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
lžou	lhát	k5eAaImIp3nP	lhát
všichni	všechen	k3xTgMnPc1	všechen
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
komunista	komunista	k1gMnSc1	komunista
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lhář	lhář	k1gMnSc1	lhář
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
společný	společný	k2eAgInSc1d1	společný
rys	rys	k1gInSc1	rys
všech	všecek	k3xTgMnPc2	všecek
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
ruských	ruský	k2eAgFnPc2d1	ruská
<g/>
.	.	kIx.	.
</s>
<s>
Mou	můj	k3xOp1gFnSc7	můj
největší	veliký	k2eAgFnSc7d3	veliký
chybou	chyba	k1gFnSc7	chyba
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
doposledka	doposledka	k6eAd1	doposledka
odmítal	odmítat	k5eAaImAgMnS	odmítat
věřit	věřit	k5eAaImF	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mne	já	k3xPp1nSc4	já
i	i	k9	i
Stalin	Stalin	k1gMnSc1	Stalin
chladnokrevně	chladnokrevně	k6eAd1	chladnokrevně
a	a	k8xC	a
cynicky	cynicky	k6eAd1	cynicky
obelhával	obelhávat	k5eAaImAgMnS	obelhávat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
r.	r.	kA	r.
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
ujišťování	ujišťování	k1gNnSc1	ujišťování
mně	já	k3xPp1nSc3	já
i	i	k9	i
Masaryka	Masaryk	k1gMnSc2	Masaryk
bylo	být	k5eAaImAgNnS	být
úmyslným	úmyslný	k2eAgInSc7d1	úmyslný
a	a	k8xC	a
cílevědomým	cílevědomý	k2eAgInSc7d1	cílevědomý
podvodem	podvod	k1gInSc7	podvod
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
rozhovoru	rozhovor	k1gInSc2	rozhovor
s	s	k7c7	s
paní	paní	k1gFnSc7	paní
Posse-Brázdovou	Posse-Brázdový	k2eAgFnSc7d1	Posse-Brázdová
v	v	k7c6	v
Sezimově	Sezimův	k2eAgNnSc6d1	Sezimovo
Ústí	ústí	k1gNnSc6	ústí
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1948	[number]	k4	1948
<g/>
;	;	kIx,	;
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
271	[number]	k4	271
<g/>
)	)	kIx)	)
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Největší	veliký	k2eAgMnSc1d3	veliký
zrádce	zrádce	k1gMnSc1	zrádce
je	být	k5eAaImIp3nS	být
Fierlinger	Fierlinger	k1gInSc4	Fierlinger
<g/>
,	,	kIx,	,
rozdupat	rozdupat	k5eAaPmF	rozdupat
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
hada	had	k1gMnSc4	had
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
musí	muset	k5eAaImIp3nS	muset
viset	viset	k5eAaImF	viset
na	na	k7c6	na
nejbližším	blízký	k2eAgInSc6d3	Nejbližší
stromě	strom	k1gInSc6	strom
<g/>
!	!	kIx.	!
</s>
<s>
Pamatujte	pamatovat	k5eAaImRp2nP	pamatovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fierlinger	Fierlinger	k1gInSc1	Fierlinger
je	být	k5eAaImIp3nS	být
svině	svině	k1gFnSc1	svině
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
svého	svůj	k3xOyFgMnSc4	svůj
bývalého	bývalý	k2eAgMnSc4d1	bývalý
přítele	přítel	k1gMnSc4	přítel
Fierlingera	Fierlinger	k1gMnSc4	Fierlinger
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
kolaboraci	kolaborace	k1gFnSc3	kolaborace
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozhovoru	rozhovor	k1gInSc2	rozhovor
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Černým	Černý	k1gMnSc7	Černý
v	v	k7c6	v
Sezimově	Sezimův	k2eAgNnSc6d1	Sezimovo
Ústí	ústí	k1gNnSc6	ústí
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Lháři	lhář	k1gMnPc1	lhář
<g/>
,	,	kIx,	,
podvodníci	podvodník	k1gMnPc1	podvodník
<g/>
,	,	kIx,	,
ničemové	ničema	k1gMnPc1	ničema
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
O	o	k7c6	o
ruských	ruský	k2eAgFnPc6d1	ruská
a	a	k8xC	a
československých	československý	k2eAgFnPc6d1	Československá
komunistech	komunista	k1gMnPc6	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozhovoru	rozhovor	k1gInSc2	rozhovor
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Černým	Černý	k1gMnSc7	Černý
v	v	k7c6	v
Sezimově	Sezimův	k2eAgNnSc6d1	Sezimovo
Ústí	ústí	k1gNnSc6	ústí
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Edvardu	Edvard	k1gMnSc3	Edvard
Benešovi	Beneš	k1gMnSc3	Beneš
bylo	být	k5eAaImAgNnS	být
věnováno	věnovat	k5eAaImNgNnS	věnovat
několik	několik	k4yIc1	několik
památníků	památník	k1gInPc2	památník
<g/>
:	:	kIx,	:
Aš	Aš	k1gInSc1	Aš
-	-	kIx~	-
památník	památník	k1gInSc1	památník
s	s	k7c7	s
reliéfem	reliéf	k1gInSc7	reliéf
Benešova	Benešův	k2eAgInSc2d1	Benešův
obličeje	obličej	k1gInSc2	obličej
před	před	k7c7	před
ZŠ	ZŠ	kA	ZŠ
na	na	k7c6	na
Okružní	okružní	k2eAgFnSc6d1	okružní
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Aš	Aš	k1gFnSc1	Aš
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Benešův	Benešův	k2eAgInSc1d1	Benešův
palouček	palouček	k1gInSc1	palouček
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
státních	státní	k2eAgFnPc6d1	státní
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Beneš	Beneš	k1gMnSc1	Beneš
utíkal	utíkat	k5eAaImAgMnS	utíkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
před	před	k7c7	před
Právnickou	právnický	k2eAgFnSc7d1	právnická
fakultou	fakulta	k1gFnSc7	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
před	před	k7c7	před
Černínským	Černínský	k2eAgInSc7d1	Černínský
palácem	palác	k1gInSc7	palác
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
-	-	kIx~	-
busta	busta	k1gFnSc1	busta
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
hotelu	hotel	k1gInSc2	hotel
Růže	růž	k1gFnSc2	růž
společně	společně	k6eAd1	společně
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Sezimovo	Sezimův	k2eAgNnSc1d1	Sezimovo
Ústí	ústí	k1gNnSc1	ústí
-	-	kIx~	-
památník	památník	k1gInSc1	památník
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
(	(	kIx(	(
<g/>
na	na	k7c6	na
hrobce	hrobka	k1gFnSc6	hrobka
EB	EB	kA	EB
a	a	k8xC	a
HB	HB	kA	HB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přístupná	přístupný	k2eAgFnSc1d1	přístupná
také	také	k9	také
zrekonstruovaná	zrekonstruovaný	k2eAgFnSc1d1	zrekonstruovaná
Benešova	Benešův	k2eAgFnSc1d1	Benešova
vila	vila	k1gFnSc1	vila
<g/>
.	.	kIx.	.
</s>
<s>
Kožlany	Kožlan	k1gInPc1	Kožlan
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
v	v	k7c6	v
parku	park	k1gInSc6	park
a	a	k8xC	a
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
-	-	kIx~	-
Socha	socha	k1gFnSc1	socha
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
a	a	k8xC	a
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
na	na	k7c6	na
konci	konec	k1gInSc6	konec
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
třídy	třída	k1gFnSc2	třída
uprostřed	uprostřed	k7c2	uprostřed
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Štětí	štětit	k5eAaImIp3nS	štětit
-	-	kIx~	-
sousoší	sousoší	k1gNnSc1	sousoší
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
a	a	k8xC	a
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Znojmo	Znojmo	k1gNnSc1	Znojmo
-	-	kIx~	-
památník	památník	k1gInSc1	památník
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
a	a	k8xC	a
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Žatec	Žatec	k1gInSc1	Žatec
-	-	kIx~	-
busta	busta	k1gFnSc1	busta
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
před	před	k7c7	před
autobusovým	autobusový	k2eAgNnSc7d1	autobusové
nádražím	nádraží	k1gNnSc7	nádraží
postavená	postavený	k2eAgFnSc1d1	postavená
z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
Volyňských	volyňský	k2eAgMnPc2d1	volyňský
Čechů	Čech	k1gMnPc2	Čech
Le	Le	k1gFnSc2	Le
problè	problè	k?	problè
autrichien	autrichien	k2eAgInSc1d1	autrichien
et	et	k?	et
la	la	k1gNnPc2	la
question	question	k1gInSc1	question
tchè	tchè	k?	tchè
-	-	kIx~	-
1908	[number]	k4	1908
Otázka	otázka	k1gFnSc1	otázka
národnostní	národnostní	k2eAgFnSc1d1	národnostní
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1909	[number]	k4	1909
Stručný	stručný	k2eAgInSc4d1	stručný
nástin	nástin	k1gInSc4	nástin
vývoje	vývoj	k1gInSc2	vývoj
moderního	moderní	k2eAgInSc2d1	moderní
socialismu	socialismus	k1gInSc2	socialismus
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
Stranictví	stranictví	k1gNnPc2	stranictví
<g/>
.	.	kIx.	.
</s>
<s>
Sociologická	sociologický	k2eAgFnSc1d1	sociologická
studie	studie	k1gFnSc1	studie
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
Mravní	mravní	k2eAgFnSc2d1	mravní
soustavy	soustava	k1gFnSc2	soustava
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
francouzské	francouzský	k2eAgFnSc3d1	francouzská
filosofii	filosofie	k1gFnSc3	filosofie
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
Le	Le	k1gFnPc2	Le
socialisme	socialismus	k1gInSc5	socialismus
autrichien	autrichien	k2eAgMnSc1d1	autrichien
et	et	k?	et
la	la	k0	la
guerre	guerr	k1gInSc5	guerr
-	-	kIx~	-
Paříž	Paříž	k1gFnSc1	Paříž
1915	[number]	k4	1915
Détruisez	Détruisez	k1gInSc1	Détruisez
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Autriche-Hongrie	Autriche-Hongrie	k1gFnSc1	Autriche-Hongrie
-	-	kIx~	-
Paříž	Paříž	k1gFnSc1	Paříž
1916	[number]	k4	1916
Vers	Vers	k1gInSc1	Vers
la	la	k1gNnSc2	la
paix	paix	k1gInSc1	paix
future	futur	k1gMnSc5	futur
-	-	kIx~	-
Genè	Genè	k1gFnPc2	Genè
1916	[number]	k4	1916
La	la	k1gNnPc2	la
Boemia	Boemium	k1gNnSc2	Boemium
contro	contro	k6eAd1	contro
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Austria-Ungeria	Austria-Ungerium	k1gNnSc2	Austria-Ungerium
-	-	kIx~	-
Locarno	Locarno	k1gNnSc1	Locarno
1917	[number]	k4	1917
<g />
.	.	kIx.	.
</s>
<s>
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Case	Case	k1gFnSc7	Case
for	forum	k1gNnPc2	forum
Independence	Independence	k1gFnSc2	Independence
-	-	kIx~	-
Washington	Washington	k1gInSc1	Washington
1917	[number]	k4	1917
K	k	k7c3	k
budoucímu	budoucí	k2eAgInSc3d1	budoucí
míru	mír	k1gInSc3	mír
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
Válka	válka	k1gFnSc1	válka
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
demokracie	demokracie	k1gFnSc1	demokracie
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1923	[number]	k4	1923
Smysl	smysl	k1gInSc1	smysl
československé	československý	k2eAgFnSc2d1	Československá
revoluce	revoluce	k1gFnSc2	revoluce
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1923	[number]	k4	1923
Nesnáze	nesnáz	k1gFnSc2	nesnáz
demokracie	demokracie	k1gFnSc2	demokracie
(	(	kIx(	(
<g/>
s	s	k7c7	s
T.G.	T.G.	k1gMnSc7	T.G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
<g/>
)	)	kIx)	)
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
Locarno	Locarno	k1gNnSc1	Locarno
a	a	k8xC	a
svaz	svaz	k1gInSc1	svaz
národů	národ	k1gInPc2	národ
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
Problém	problém	k1gInSc4	problém
malých	malý	k2eAgInPc2d1	malý
národů	národ	k1gInPc2	národ
po	po	k7c6	po
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
:	:	kIx,	:
Problém	problém	k1gInSc1	problém
malých	malý	k2eAgInPc2d1	malý
národů	národ	k1gInPc2	národ
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
krisi	krise	k1gFnSc6	krise
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
Světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
naše	náš	k3xOp1gFnSc1	náš
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
a	a	k8xC	a
úvahy	úvaha	k1gFnPc1	úvaha
z	z	k7c2	z
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
I	I	kA	I
<g/>
-	-	kIx~	-
<g/>
III	III	kA	III
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
pacifismus	pacifismus	k1gInSc1	pacifismus
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
La	la	k1gNnPc2	la
France	Franc	k1gMnSc2	Franc
et	et	k?	et
la	la	k1gNnSc1	la
nouvelle	nouvell	k1gMnSc5	nouvell
Europe	Europ	k1gMnSc5	Europ
-	-	kIx~	-
1931	[number]	k4	1931
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
mír	mír	k1gInSc4	mír
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
státu	stát	k1gInSc2	stát
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
Politická	politický	k2eAgFnSc1d1	politická
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
cesta	cesta	k1gFnSc1	cesta
a	a	k8xC	a
odkaz	odkaz	k1gInSc1	odkaz
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
Democracy	Democrac	k2eAgInPc1d1	Democrac
Today	Today	k1gInPc1	Today
and	and	k?	and
Tomorrow	Tomorrow	k1gFnPc2	Tomorrow
-	-	kIx~	-
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
Demokracie	demokracie	k1gFnSc2	demokracie
dnes	dnes	k6eAd1	dnes
a	a	k8xC	a
zítra	zítra	k6eAd1	zítra
-	-	kIx~	-
Londýn	Londýn	k1gInSc1	Londýn
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Projevy	projev	k1gInPc4	projev
a	a	k8xC	a
dokumenty	dokument	k1gInPc4	dokument
z	z	k7c2	z
r.	r.	kA	r.
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
-	-	kIx~	-
Londýn	Londýn	k1gInSc4	Londýn
1942	[number]	k4	1942
Projev	projev	k1gInSc1	projev
presidenta	president	k1gMnSc4	president
republiky	republika	k1gFnSc2	republika
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1945	[number]	k4	1945
Naše	náš	k3xOp1gFnSc1	náš
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
politika	politika	k1gFnSc1	politika
-	-	kIx~	-
Londýn	Londýn	k1gInSc1	Londýn
1943	[number]	k4	1943
<g/>
Úvahy	úvaha	k1gFnSc2	úvaha
o	o	k7c6	o
slovanství	slovanství	k1gNnSc6	slovanství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
problémy	problém	k1gInPc1	problém
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
politiky	politika	k1gFnSc2	politika
-	-	kIx~	-
(	(	kIx(	(
<g/>
Vydal	vydat	k5eAaPmAgInS	vydat
týdeník	týdeník	k1gInSc1	týdeník
<g/>
"	"	kIx"	"
<g/>
Londýn	Londýn	k1gInSc1	Londýn
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
Šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
projevy	projev	k1gInPc4	projev
a	a	k8xC	a
dokumenty	dokument	k1gInPc4	dokument
z	z	k7c2	z
r.	r.	kA	r.
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
-	-	kIx~	-
(	(	kIx(	(
<g/>
Vydal	vydat	k5eAaPmAgInS	vydat
týdeník	týdeník	k1gInSc1	týdeník
"	"	kIx"	"
<g/>
Čechoslovák	Čechoslovák	k1gMnSc1	Čechoslovák
<g/>
"	"	kIx"	"
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
)	)	kIx)	)
Londýn	Londýn	k1gInSc1	Londýn
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
Paměti	paměť	k1gFnPc1	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Mnichova	Mnichov	k1gInSc2	Mnichov
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
vítězství	vítězství	k1gNnSc3	vítězství
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
Mnichovské	mnichovský	k2eAgInPc1d1	mnichovský
dny	den	k1gInPc1	den
-	-	kIx~	-
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
</s>
