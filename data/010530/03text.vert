<p>
<s>
Lesby	lesba	k1gFnPc1	lesba
<g/>
,	,	kIx,	,
gayové	gay	k1gMnPc1	gay
<g/>
,	,	kIx,	,
bisexuálové	bisexuál	k1gMnPc1	bisexuál
a	a	k8xC	a
translidé	translidý	k2eAgFnPc1d1	translidý
čelí	čelit	k5eAaImIp3nP	čelit
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
právním	právní	k2eAgInPc3d1	právní
problémům	problém	k1gInPc3	problém
a	a	k8xC	a
diskriminaci	diskriminace	k1gFnSc6	diskriminace
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
heterosexuální	heterosexuální	k2eAgFnSc1d1	heterosexuální
většina	většina	k1gFnSc1	většina
nepotýká	potýkat	k5eNaImIp3nS	potýkat
<g/>
.	.	kIx.	.
</s>
<s>
Homosexuální	homosexuální	k2eAgInPc1d1	homosexuální
vztahy	vztah	k1gInPc1	vztah
a	a	k8xC	a
sexuální	sexuální	k2eAgFnSc1d1	sexuální
aktivita	aktivita	k1gFnSc1	aktivita
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
legální	legální	k2eAgFnSc1d1	legální
od	od	k7c2	od
r.	r.	kA	r.
1858	[number]	k4	1858
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
zákony	zákon	k1gInPc4	zákon
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
<g/>
"	"	kIx"	"
ani	ani	k8xC	ani
"	"	kIx"	"
<g/>
genderová	genderový	k2eAgFnSc1d1	genderová
identita	identita	k1gFnSc1	identita
<g/>
"	"	kIx"	"
nerozeznává	rozeznávat	k5eNaImIp3nS	rozeznávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
ani	ani	k8xC	ani
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
způsob	způsob	k1gInSc4	způsob
právního	právní	k2eAgNnSc2d1	právní
ošetření	ošetření	k1gNnSc2	ošetření
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1	stejnopohlavní
soužití	soužití	k1gNnSc2	soužití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Valná	valný	k2eAgFnSc1d1	valná
část	část	k1gFnSc1	část
turecké	turecký	k2eAgFnSc2d1	turecká
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
silně	silně	k6eAd1	silně
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
homosexuální	homosexuální	k2eAgFnSc2d1	homosexuální
problematiky	problematika	k1gFnSc2	problematika
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
homofobní	homofobní	k2eAgFnSc1d1	homofobní
diskriminace	diskriminace	k1gFnSc1	diskriminace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
<g/>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
ohledně	ohledně	k7c2	ohledně
práv	právo	k1gNnPc2	právo
LGBT	LGBT	kA	LGBT
osob	osoba	k1gFnPc2	osoba
zažila	zažít	k5eAaPmAgFnS	zažít
veliký	veliký	k2eAgInSc4d1	veliký
rozmach	rozmach	k1gInSc4	rozmach
počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
návrhy	návrh	k1gInPc1	návrh
zákonů	zákon	k1gInPc2	zákon
proti	proti	k7c3	proti
homofobní	homofobní	k2eAgFnSc3d1	homofobní
diskriminaci	diskriminace	k1gFnSc3	diskriminace
a	a	k8xC	a
zločinům	zločin	k1gMnPc3	zločin
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
nenávisti	nenávist	k1gFnSc3	nenávist
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
umožňujícího	umožňující	k2eAgMnSc2d1	umožňující
uzavřít	uzavřít	k5eAaPmF	uzavřít
stejnopohlavní	stejnopohlavní	k2eAgNnPc1d1	stejnopohlavní
manželství	manželství	k1gNnPc1	manželství
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
komise	komise	k1gFnSc1	komise
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
odsouhlasila	odsouhlasit	k5eAaPmAgFnS	odsouhlasit
nové	nový	k2eAgNnSc4d1	nové
znění	znění	k1gNnSc4	znění
Ústavy	ústava	k1gFnSc2	ústava
Turecké	turecký	k2eAgFnSc2d1	turecká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
garantovala	garantovat	k5eAaBmAgFnS	garantovat
ústavní	ústavní	k2eAgFnSc4d1	ústavní
ochranu	ochrana	k1gFnSc4	ochrana
LGBT	LGBT	kA	LGBT
osobám	osoba	k1gFnPc3	osoba
proti	proti	k7c3	proti
diskriminaci	diskriminace	k1gFnSc3	diskriminace
<g/>
.	.	kIx.	.
<g/>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nesouladu	nesoulad	k1gInSc2	nesoulad
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
subjekty	subjekt	k1gInPc7	subjekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
Ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
LGBT	LGBT	kA	LGBT
organizace	organizace	k1gFnSc2	organizace
bojovaly	bojovat	k5eAaImAgInP	bojovat
proti	proti	k7c3	proti
postoji	postoj	k1gInSc3	postoj
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
shromažďování	shromažďování	k1gNnSc2	shromažďování
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
například	například	k6eAd1	například
sdružení	sdružení	k1gNnSc2	sdružení
Lambda	lambda	k1gNnSc1	lambda
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1994	[number]	k4	1994
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
a	a	k8xC	a
solidární	solidární	k2eAgFnSc1d1	solidární
strana	strana	k1gFnSc1	strana
zakázala	zakázat	k5eAaPmAgFnS	zakázat
diskriminaci	diskriminace	k1gFnSc4	diskriminace
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
a	a	k8xC	a
genderové	genderový	k2eAgFnSc2d1	genderová
identity	identita	k1gFnSc2	identita
během	během	k7c2	během
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
nakonec	nakonec	k6eAd1	nakonec
zvítěžil	zvítěžit	k5eAaImAgMnS	zvítěžit
Demet	Demet	k1gMnSc1	Demet
Demir	Demir	k1gMnSc1	Demir
<g/>
,	,	kIx,	,
hl.	hl.	k?	hl.
mluvčí	mluvčí	k1gMnSc1	mluvčí
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
také	také	k9	také
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
zvolení	zvolení	k1gNnSc6	zvolení
stal	stát	k5eAaPmAgInS	stát
první	první	k4xOgInSc4	první
transgender	transgender	k1gInSc4	transgender
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
komunální	komunální	k2eAgFnPc4d1	komunální
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1996	[number]	k4	1996
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
obrátil	obrátit	k5eAaPmAgInS	obrátit
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
něhož	jenž	k3xRgNnSc2	jenž
odebral	odebrat	k5eAaPmAgInS	odebrat
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
lesbických	lesbický	k2eAgFnPc2d1	lesbická
rodin	rodina	k1gFnPc2	rodina
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
údajné	údajný	k2eAgFnSc2d1	údajná
amorálnosti	amorálnost	k1gFnSc2	amorálnost
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
.	.	kIx.	.
<g/>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
LGBT	LGBT	kA	LGBT
organizace	organizace	k1gFnSc1	organizace
dále	daleko	k6eAd2	daleko
vysílají	vysílat	k5eAaImIp3nP	vysílat
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
dochází	docházet	k5eAaImIp3nS	docházet
pravidelně	pravidelně	k6eAd1	pravidelně
k	k	k7c3	k
několika	několik	k4yIc3	několik
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
LGBT	LGBT	kA	LGBT
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2008	[number]	k4	2008
gay	gay	k1gMnSc1	gay
student	student	k1gMnSc1	student
Ahmet	Ahmet	k1gMnSc1	Ahmet
Yildiz	Yildiz	k1gMnSc1	Yildiz
byl	být	k5eAaImAgMnS	být
postřelen	postřelit	k5eAaPmNgMnS	postřelit
při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
kavárny	kavárna	k1gFnSc2	kavárna
a	a	k8xC	a
následně	následně	k6eAd1	následně
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zraněním	zranění	k1gNnSc7	zranění
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Sociologové	sociolog	k1gMnPc1	sociolog
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
Turky	Turek	k1gMnPc4	Turek
jsou	být	k5eAaImIp3nP	být
vrazi	vrah	k1gMnPc1	vrah
gayů	gay	k1gMnPc2	gay
bráni	brán	k2eAgMnPc1d1	brán
jako	jako	k8xS	jako
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
.	.	kIx.	.
<g/>
Snaha	snaha	k1gFnSc1	snaha
Turecka	Turecko	k1gNnSc2	Turecko
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
však	však	k9	však
tlačí	tlačit	k5eAaImIp3nS	tlačit
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgMnSc7	tento
nucená	nucený	k2eAgFnSc1d1	nucená
oficiálně	oficiálně	k6eAd1	oficiálně
uznat	uznat	k5eAaPmF	uznat
LGBT	LGBT	kA	LGBT
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Stanovisko	stanovisko	k1gNnSc1	stanovisko
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
ohledně	ohledně	k7c2	ohledně
otázky	otázka	k1gFnSc2	otázka
rozšíření	rozšíření	k1gNnSc2	rozšíření
EU	EU	kA	EU
o	o	k7c4	o
Turecko	Turecko	k1gNnSc4	Turecko
zní	znět	k5eAaImIp3nS	znět
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Právní	právní	k2eAgInSc1d1	právní
řad	řad	k1gInSc1	řad
není	být	k5eNaImIp3nS	být
adekvátní	adekvátní	k2eAgInSc1d1	adekvátní
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
neslučitelný	slučitelný	k2eNgInSc1d1	neslučitelný
s	s	k7c7	s
právním	právní	k2eAgInSc7d1	právní
řádem	řád	k1gInSc7	řád
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Homofobie	Homofobie	k1gFnSc1	Homofobie
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
psychické	psychický	k2eAgNnSc4d1	psychické
a	a	k8xC	a
sexuální	sexuální	k2eAgNnSc4d1	sexuální
násilí	násilí	k1gNnSc4	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
vražd	vražda	k1gFnPc2	vražda
transexuálů	transexuál	k1gMnPc2	transexuál
a	a	k8xC	a
transvestitů	transvestit	k1gMnPc2	transvestit
je	být	k5eAaImIp3nS	být
znepokujující	znepokujující	k2eAgInSc4d1	znepokujující
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Soudy	soud	k1gInPc1	soud
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
nespravdelivý	spravdelivý	k2eNgInSc4d1	spravdelivý
výrok	výrok	k1gInSc4	výrok
"	"	kIx"	"
<g/>
provokace	provokace	k1gFnSc1	provokace
<g/>
"	"	kIx"	"
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
pachatelů	pachatel	k1gMnPc2	pachatel
zločnů	zločn	k1gMnPc2	zločn
z	z	k7c2	z
nenávisti	nenávist	k1gFnSc2	nenávist
vůči	vůči	k7c3	vůči
transexuálům	transexuál	k1gMnPc3	transexuál
a	a	k8xC	a
transvestitům	transvestit	k1gMnPc3	transvestit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
se	se	k3xPyFc4	se
však	však	k9	však
stalo	stát	k5eAaPmAgNnS	stát
první	první	k4xOgFnSc7	první
zemí	zem	k1gFnSc7	zem
s	s	k7c7	s
muslimskou	muslimský	k2eAgFnSc7d1	muslimská
většinou	většina	k1gFnSc7	většina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
povolila	povolit	k5eAaPmAgFnS	povolit
konání	konání	k1gNnSc2	konání
pochodů	pochod	k1gInPc2	pochod
gay	gay	k1gMnSc1	gay
pride	pride	k6eAd1	pride
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Ankaře	Ankara	k1gFnSc6	Ankara
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pochody	pochod	k1gInPc1	pochod
konají	konat	k5eAaImIp3nP	konat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
se	s	k7c7	s
stále	stále	k6eAd1	stále
vzrůstajícím	vzrůstající	k2eAgInSc7d1	vzrůstající
počtem	počet	k1gInSc7	počet
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
Istanbul	Istanbul	k1gInSc1	Istanbul
pride	pride	k6eAd1	pride
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
r.	r.	kA	r.
2003	[number]	k4	2003
s	s	k7c7	s
pouhými	pouhý	k2eAgNnPc7d1	pouhé
30	[number]	k4	30
účastníky	účastník	k1gMnPc7	účastník
<g/>
,	,	kIx,	,
v	v	k7c6	v
r.	r.	kA	r.
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
další	další	k2eAgInPc1d1	další
festivaly	festival	k1gInPc1	festival
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2011	[number]	k4	2011
a	a	k8xC	a
2012	[number]	k4	2012
už	už	k6eAd1	už
disponovaly	disponovat	k5eAaBmAgInP	disponovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
000	[number]	k4	000
účastníky	účastník	k1gMnPc7	účastník
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
účastníků	účastník	k1gMnPc2	účastník
téměř	téměř	k6eAd1	téměř
stotiscícový	stotiscícový	k2eAgMnSc1d1	stotiscícový
<g/>
.	.	kIx.	.
<g/>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
v	v	k7c6	v
Gezi	Gez	k1gFnSc6	Gez
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
dění	dění	k1gNnSc1	dění
Istanbul	Istanbul	k1gInSc4	Istanbul
Pride	Prid	k1gInSc5	Prid
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
největšího	veliký	k2eAgInSc2d3	veliký
gay	gay	k1gMnSc1	gay
pride	pride	k6eAd1	pride
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
<g/>
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
Izmir	Izmir	k1gInSc1	Izmir
pride	pride	k6eAd1	pride
s	s	k7c7	s
2	[number]	k4	2
000	[number]	k4	000
účastníky	účastník	k1gMnPc7	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
Pride	Prid	k1gInSc5	Prid
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Antalyi	Antaly	k1gFnSc6	Antaly
<g/>
.	.	kIx.	.
</s>
<s>
Politici	politik	k1gMnPc1	politik
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
největších	veliký	k2eAgFnPc2d3	veliký
opoziční	opoziční	k2eAgMnPc4d1	opoziční
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
opoziční	opoziční	k2eAgFnSc2d1	opoziční
strany	strana	k1gFnSc2	strana
Strany	strana	k1gFnSc2	strana
míru	mír	k1gInSc2	mír
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
poslali	poslat	k5eAaPmAgMnP	poslat
svoji	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
demonstrantům	demonstrant	k1gMnPc3	demonstrant
<g/>
.	.	kIx.	.
<g/>
Festival	festival	k1gInSc1	festival
gay	gay	k1gMnSc1	gay
pride	pride	k6eAd1	pride
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
neobdržel	obdržet	k5eNaPmAgMnS	obdržet
žádnou	žádný	k3yNgFnSc4	žádný
záštitu	záštita	k1gFnSc4	záštita
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
obce	obec	k1gFnSc2	obec
nebo	nebo	k8xC	nebo
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
r.	r.	kA	r.
2009	[number]	k4	2009
se	se	k3xPyFc4	se
amatérský	amatérský	k2eAgMnSc1d1	amatérský
fotbalista	fotbalista	k1gMnSc1	fotbalista
veřejně	veřejně	k6eAd1	veřejně
přiznal	přiznat	k5eAaPmAgMnS	přiznat
k	k	k7c3	k
homosexualitě	homosexualita	k1gFnSc3	homosexualita
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
z	z	k7c2	z
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
týmu	tým	k1gInSc2	tým
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
ministryně	ministryně	k1gFnSc2	ministryně
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
politiky	politika	k1gFnSc2	politika
Fatma	Fatma	k1gFnSc1	Fatma
Şahin	Şahin	k1gInSc1	Şahin
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
LGBT	LGBT	kA	LGBT
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
bude	být	k5eAaImBp3nS	být
aktivně	aktivně	k6eAd1	aktivně
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
LGBT	LGBT	kA	LGBT
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Předložila	předložit	k5eAaPmAgFnS	předložit
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
LGBT	LGBT	kA	LGBT
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
Ústavě	ústava	k1gFnSc6	ústava
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
Parlament	parlament	k1gInSc1	parlament
schválí	schválit	k5eAaPmIp3nS	schválit
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Apelovala	apelovat	k5eAaImAgFnS	apelovat
na	na	k7c4	na
členy	člen	k1gMnPc4	člen
Parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
stanovisko	stanovisko	k1gNnSc1	stanovisko
bylo	být	k5eAaImAgNnS	být
takové	takový	k3xDgNnSc4	takový
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
rovnost	rovnost	k1gFnSc1	rovnost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
nechť	nechť	k9	nechť
jsou	být	k5eAaImIp3nP	být
zakázány	zakázán	k2eAgFnPc1d1	zakázána
diskriminace	diskriminace	k1gFnPc1	diskriminace
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
a	a	k8xC	a
uznány	uznán	k2eAgInPc4d1	uznán
práva	právo	k1gNnPc1	právo
pro	pro	k7c4	pro
LGBT	LGBT	kA	LGBT
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
fejetonistů	fejetonista	k1gMnPc2	fejetonista
Serdar	Serdara	k1gFnPc2	Serdara
Arseven	Arseven	k2eAgMnSc1d1	Arseven
islámského	islámský	k2eAgInSc2d1	islámský
konzervativného	konzervativný	k2eAgInSc2d1	konzervativný
časopisu	časopis	k1gInSc2	časopis
Yeni	Yeni	k1gNnSc7	Yeni
Akit	Akit	k1gMnSc1	Akit
vydal	vydat	k5eAaPmAgMnS	vydat
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
označující	označující	k2eAgMnPc1d1	označující
LGBT	LGBT	kA	LGBT
za	za	k7c4	za
perverzní	perverzní	k2eAgInSc4d1	perverzní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgMnSc4	ten
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
dal	dát	k5eAaPmAgInS	dát
časopisu	časopis	k1gInSc3	časopis
Yeni	Yen	k1gFnSc2	Yen
Akit	Akit	k1gInSc1	Akit
pokutu	pokuta	k1gFnSc4	pokuta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
4000	[number]	k4	4000
tureckých	turecký	k2eAgFnPc2d1	turecká
lir	lira	k1gFnPc2	lira
a	a	k8xC	a
Serdaru	Serdar	k1gMnSc3	Serdar
Arsevenovi	Arseven	k1gMnSc3	Arseven
pokutu	pokuta	k1gFnSc4	pokuta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
2000	[number]	k4	2000
tureckých	turecký	k2eAgFnPc2d1	turecká
lir	lira	k1gFnPc2	lira
za	za	k7c4	za
projev	projev	k1gInSc4	projev
nenávisti	nenávist	k1gFnSc2	nenávist
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
Strana	strana	k1gFnSc1	strana
míru	mír	k1gInSc2	mír
a	a	k8xC	a
demokracie	demokracie	k1gFnSc1	demokracie
dala	dát	k5eAaPmAgFnS	dát
žádost	žádost	k1gFnSc4	žádost
tvůrcům	tvůrce	k1gMnPc3	tvůrce
nové	nový	k2eAgFnSc2d1	nová
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zahrnuli	zahrnout	k5eAaPmAgMnP	zahrnout
také	také	k9	také
stejnopohlavní	stejnopohlavní	k2eAgNnSc4d1	stejnopohlavní
manželství	manželství	k1gNnSc4	manželství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
odmítnuto	odmítnout	k5eAaPmNgNnS	odmítnout
největší	veliký	k2eAgFnSc7d3	veliký
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
Turecka	Turecko	k1gNnSc2	Turecko
Stranou	stranou	k6eAd1	stranou
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
opoziční	opoziční	k2eAgFnSc7d1	opoziční
stranou	strana	k1gFnSc7	strana
Turecké	turecký	k2eAgFnSc2d1	turecká
nacionální	nacionální	k2eAgFnSc2d1	nacionální
strany	strana	k1gFnSc2	strana
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
hlavních	hlavní	k2eAgMnPc2d1	hlavní
odpůrců	odpůrce	k1gMnPc2	odpůrce
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
<g/>
.29	.29	k4	.29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
Parlament	parlament	k1gInSc4	parlament
novelu	novela	k1gFnSc4	novela
Ústavy	ústava	k1gFnSc2	ústava
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
a	a	k8xC	a
prodiskutoval	prodiskutovat	k5eAaPmAgInS	prodiskutovat
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k6eAd1	navzdory
podpory	podpora	k1gFnSc2	podpora
kurdské	kurdský	k2eAgFnSc2d1	kurdská
strany	strana	k1gFnSc2	strana
BDP	BDP	kA	BDP
a	a	k8xC	a
neutrálního	neutrální	k2eAgNnSc2d1	neutrální
stanoviska	stanovisko	k1gNnSc2	stanovisko
Turecké	turecký	k2eAgFnSc2d1	turecká
nacionální	nacionální	k2eAgFnSc2d1	nacionální
strany	strana	k1gFnSc2	strana
byla	být	k5eAaImAgFnS	být
novela	novela	k1gFnSc1	novela
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hlasů	hlas	k1gInPc2	hlas
strany	strana	k1gFnSc2	strana
AKP	AKP	kA	AKP
<g/>
.	.	kIx.	.
</s>
<s>
AKP	AKP	kA	AKP
MP	MP	kA	MP
Türkan	Türkan	k1gInSc4	Türkan
Dağ	Dağ	k1gFnSc2	Dağ
citovaly	citovat	k5eAaBmAgInP	citovat
odborné	odborný	k2eAgInPc1d1	odborný
články	článek	k1gInPc1	článek
ohledně	ohledně	k7c2	ohledně
homosexuality	homosexualita	k1gFnSc2	homosexualita
zvřejněné	zvřejněná	k1gFnSc2	zvřejněná
v	v	k7c6	v
USA	USA	kA	USA
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
tvrdící	tvrdící	k2eAgInPc1d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
homosexualita	homosexualita	k1gFnSc1	homosexualita
je	být	k5eAaImIp3nS	být
abnormální	abnormální	k2eAgFnSc1d1	abnormální
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
stejnophlavní	stejnophlavní	k2eAgNnSc1d1	stejnophlavní
manželství	manželství	k1gNnSc1	manželství
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
povoleno	povolit	k5eAaPmNgNnS	povolit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
sociální	sociální	k2eAgFnSc4d1	sociální
degeneraci	degenerace	k1gFnSc4	degenerace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
novely	novela	k1gFnSc2	novela
však	však	k9	však
politik	politik	k1gMnSc1	politik
strany	strana	k1gFnSc2	strana
CHP	CHP	kA	CHP
MP	MP	kA	MP
<g/>
,	,	kIx,	,
Binnaz	Binnaz	k1gInSc1	Binnaz
Toprak	Toprak	k1gInSc1	Toprak
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
USA	USA	kA	USA
hodně	hodně	k6eAd1	hodně
odborníků	odborník	k1gMnPc2	odborník
tvrdících	tvrdící	k2eAgInPc2d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
černoši	černoch	k1gMnPc1	černoch
nejsou	být	k5eNaImIp3nP	být
tak	tak	k9	tak
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
jako	jako	k8xC	jako
běloši	běloch	k1gMnPc1	běloch
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
výzkumy	výzkum	k1gInPc1	výzkum
z	z	k7c2	z
dřívější	dřívější	k2eAgFnSc2d1	dřívější
doby	doba	k1gFnSc2	doba
nelze	lze	k6eNd1	lze
dnes	dnes	k6eAd1	dnes
aplikovat	aplikovat	k5eAaBmF	aplikovat
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
tvrzení	tvrzení	k1gNnPc1	tvrzení
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
komise	komise	k1gFnSc1	komise
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
stran	strana	k1gFnPc2	strana
Kurdů	Kurd	k1gMnPc2	Kurd
<g/>
,	,	kIx,	,
sekularistů	sekularista	k1gMnPc2	sekularista
<g/>
,	,	kIx,	,
islamistů	islamista	k1gMnPc2	islamista
a	a	k8xC	a
nacionalistů	nacionalista	k1gMnPc2	nacionalista
odsouhlasila	odsouhlasit	k5eAaPmAgFnS	odsouhlasit
ústavní	ústavní	k2eAgFnSc4d1	ústavní
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
diskriminací	diskriminace	k1gFnSc7	diskriminace
vůči	vůči	k7c3	vůči
LGBT	LGBT	kA	LGBT
osobám	osoba	k1gFnPc3	osoba
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.17	.17	k4	.17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Turecka	Turecko	k1gNnSc2	Turecko
schválil	schválit	k5eAaPmAgInS	schválit
tvrzení	tvrzení	k1gNnPc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrok	výrok	k1gInSc1	výrok
o	o	k7c6	o
gayích	gay	k1gMnPc6	gay
jako	jako	k8xC	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
perverzních	perverzní	k2eAgFnPc2d1	perverzní
<g/>
"	"	kIx"	"
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
projev	projev	k1gInSc4	projev
nenávisti	nenávist	k1gFnSc2	nenávist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
LGBT	LGBT	kA	LGBT
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
sdružení	sdružení	k1gNnSc2	sdružení
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
LGBT	LGBT	kA	LGBT
organizace	organizace	k1gFnSc1	organizace
je	být	k5eAaImIp3nS	být
KAOS	KAOS	kA	KAOS
GL	GL	kA	GL
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
r.	r.	kA	r.
1994	[number]	k4	1994
v	v	k7c6	v
Ankaře	Ankara	k1gFnSc6	Ankara
<g/>
.	.	kIx.	.
</s>
<s>
Lambdaistanbul	Lambdaistanbul	k1gInSc4	Lambdaistanbul
<g/>
,	,	kIx,	,
člence	členka	k1gFnSc3	členka
ILGA-Europe	ILGA-Europ	k1gInSc5	ILGA-Europ
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
r.	r.	kA	r.
1993	[number]	k4	1993
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
pozastavena	pozastavit	k5eAaPmNgFnS	pozastavit
činnost	činnost	k1gFnSc1	činnost
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
činnost	činnost	k1gFnSc4	činnost
proti	proti	k7c3	proti
právu	právo	k1gNnSc3	právo
a	a	k8xC	a
morálce	morálka	k1gFnSc3	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
HRW	HRW	kA	HRW
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
zrušen	zrušit	k5eAaPmNgInS	zrušit
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
odvolacího	odvolací	k2eAgInSc2d1	odvolací
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
veškeré	veškerý	k3xTgFnPc4	veškerý
snahy	snaha	k1gFnPc4	snaha
organizací	organizace	k1gFnPc2	organizace
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
odmítnuty	odmítnut	k2eAgInPc1d1	odmítnut
Vládní	vládní	k2eAgFnSc7d1	vládní
komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
členové	člen	k1gMnPc1	člen
Lambda	lambda	k1gNnSc2	lambda
Istanbul	Istanbul	k1gInSc1	Istanbul
pozváni	pozvat	k5eAaPmNgMnP	pozvat
na	na	k7c4	na
Národní	národní	k2eAgInSc4d1	národní
kongres	kongres	k1gInSc4	kongres
proti	proti	k7c3	proti
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydalo	vydat	k5eAaPmAgNnS	vydat
doporučení	doporučení	k1gNnSc1	doporučení
povýšit	povýšit	k5eAaPmF	povýšit
turecké	turecký	k2eAgNnSc4d1	turecké
LGBT	LGBT	kA	LGBT
organizace	organizace	k1gFnSc2	organizace
na	na	k7c4	na
vládní	vládní	k2eAgFnSc4d1	vládní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
dekády	dekáda	k1gFnSc2	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
nové	nový	k2eAgFnPc1d1	nová
organizace	organizace	k1gFnPc1	organizace
začaly	začít	k5eAaPmAgFnP	začít
soustřeďovat	soustřeďovat	k5eAaImF	soustřeďovat
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
než	než	k8xS	než
do	do	k7c2	do
Ankary	Ankara	k1gFnSc2	Ankara
a	a	k8xC	a
Istanbulu	Istanbul	k1gInSc2	Istanbul
jako	jako	k8xS	jako
například	například	k6eAd1	například
Pink	pink	k2eAgInSc1d1	pink
Life	Life	k1gInSc1	Life
LGBT	LGBT	kA	LGBT
Association	Association	k1gInSc1	Association
v	v	k7c6	v
Ankaře	Ankara	k1gFnSc6	Ankara
nebo	nebo	k8xC	nebo
Rainbow	Rainbow	k1gFnSc6	Rainbow
Group	Group	k1gInSc1	Group
v	v	k7c6	v
Antalyi	Antalyi	k1gNnSc6	Antalyi
a	a	k8xC	a
Piramid	Piramid	k1gInSc1	Piramid
LGBT	LGBT	kA	LGBT
Diyarbakir	Diyarbakir	k1gInSc1	Diyarbakir
Initiative	Initiativ	k1gInSc5	Initiativ
ve	v	k7c6	v
Diyarbakiru	Diyarbakiro	k1gNnSc3	Diyarbakiro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1996	[number]	k4	1996
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
LGBT	LGBT	kA	LGBT
organizace	organizace	k1gFnSc2	organizace
LEGATO	legato	k6eAd1	legato
sdružující	sdružující	k2eAgMnPc4d1	sdružující
vysokoškolské	vysokoškolský	k2eAgMnPc4d1	vysokoškolský
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
absolventy	absolvent	k1gMnPc7	absolvent
a	a	k8xC	a
členy	člen	k1gMnPc7	člen
akademických	akademický	k2eAgFnPc2d1	akademická
obcí	obec	k1gFnPc2	obec
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
na	na	k7c6	na
Midle	Midl	k1gInSc6	Midl
East	East	k2eAgMnSc1d1	East
Technical	Technical	k1gMnSc1	Technical
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Ankaře	Ankara	k1gFnSc6	Ankara
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
se	se	k3xPyFc4	se
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
i	i	k9	i
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
obory	obor	k1gInPc4	obor
se	s	k7c7	s
vzrůstajícím	vzrůstající	k2eAgInSc7d1	vzrůstající
počtem	počet	k1gInSc7	počet
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
disponovala	disponovat	k5eAaBmAgFnS	disponovat
postupně	postupně	k6eAd1	postupně
2	[number]	k4	2
000	[number]	k4	000
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2007	[number]	k4	2007
studenti	student	k1gMnPc1	student
poprvé	poprvé	k6eAd1	poprvé
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
první	první	k4xOgInSc4	první
studentský	studentský	k2eAgInSc4d1	studentský
klub	klub	k1gInSc4	klub
Rainbow	Rainbow	k1gFnSc2	Rainbow
oficiálně	oficiálně	k6eAd1	oficiálně
schválený	schválený	k2eAgInSc1d1	schválený
Bilgi	Bilg	k1gFnSc3	Bilg
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
první	první	k4xOgInSc1	první
festival	festival	k1gInSc1	festival
hrdosti	hrdost	k1gFnSc2	hrdost
Istanbul	Istanbul	k1gInSc1	Istanbul
pride	pride	k6eAd1	pride
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
Lambdaistanbulem	Lambdaistanbul	k1gInSc7	Lambdaistanbul
na	na	k7c4	na
Istikal	Istikal	k1gFnSc4	Istikal
Caddesi	Caddese	k1gFnSc4	Caddese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2005	[number]	k4	2005
KAOS	KAOS	kA	KAOS
GL	GL	kA	GL
požádal	požádat	k5eAaPmAgMnS	požádat
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
první	první	k4xOgFnPc4	první
LGBT	LGBT	kA	LGBT
organizace	organizace	k1gFnPc1	organizace
získaly	získat	k5eAaPmAgFnP	získat
legální	legální	k2eAgInSc4d1	legální
status	status	k1gInSc4	status
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
září	září	k1gNnSc2	září
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
guvernéra	guvernér	k1gMnSc4	guvernér
Ankary	Ankara	k1gFnSc2	Ankara
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
status	status	k1gInSc1	status
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
na	na	k7c4	na
vyžádání	vyžádání	k1gNnSc4	vyžádání
zrušil	zrušit	k5eAaPmAgMnS	zrušit
prokurátor	prokurátor	k1gMnSc1	prokurátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
gay	gay	k1gMnSc1	gay
pride	pride	k6eAd1	pride
v	v	k7c6	v
Burse	bursa	k1gFnSc6	bursa
s	s	k7c7	s
oficiální	oficiální	k2eAgFnSc7d1	oficiální
záštitou	záštita	k1gFnSc7	záštita
tamního	tamní	k2eAgMnSc2d1	tamní
guvernéra	guvernér	k1gMnSc2	guvernér
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nekonal	konat	k5eNaImAgInS	konat
kvůli	kvůli	k7c3	kvůli
velké	velký	k2eAgFnSc3d1	velká
vlně	vlna	k1gFnSc3	vlna
odporu	odpor	k1gInSc2	odpor
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Organizace	organizace	k1gFnSc1	organizace
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílejí	podílet	k5eAaImIp3nP	podílet
v	v	k7c6	v
osvětě	osvěta	k1gFnSc6	osvěta
na	na	k7c4	na
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	aids	k1gInSc4	aids
problematice	problematika	k1gFnSc3	problematika
a	a	k8xC	a
pochodů	pochod	k1gInPc2	pochod
proti	proti	k7c3	proti
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
Kancelář	kancelář	k1gFnSc1	kancelář
guvernéra	guvernér	k1gMnSc2	guvernér
Ankary	Ankara	k1gFnSc2	Ankara
obvinila	obvinit	k5eAaPmAgFnS	obvinit
KAOS	KAOS	kA	KAOS
GL	GL	kA	GL
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gNnSc1	její
založení	založení	k1gNnSc1	založení
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
právu	právo	k1gNnSc3	právo
a	a	k8xC	a
zásadám	zásada	k1gFnPc3	zásada
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
<g/>
To	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
také	také	k9	také
lidskoprávní	lidskoprávní	k2eAgFnSc4d1	lidskoprávní
organizaci	organizace	k1gFnSc4	organizace
Pink	pink	k2eAgMnSc2d1	pink
Life	Lif	k1gMnSc2	Lif
LGBT	LGBT	kA	LGBT
Association	Association	k1gInSc1	Association
pracující	pracující	k1gFnSc1	pracující
s	s	k7c7	s
transgender	transgendra	k1gFnPc2	transgendra
osobami	osoba	k1gFnPc7	osoba
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
prokurátora	prokurátor	k1gMnSc2	prokurátor
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
morálce	morálka	k1gFnSc3	morálka
a	a	k8xC	a
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
byly	být	k5eAaImAgFnP	být
neprodleně	prodleně	k6eNd1	prodleně
zrušeny	zrušen	k2eAgFnPc1d1	zrušena
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
r.	r.	kA	r.
2006	[number]	k4	2006
Lambda	lambda	k1gNnSc2	lambda
Istanbul	Istanbul	k1gInSc1	Istanbul
byla	být	k5eAaImAgFnS	být
vystěhována	vystěhovat	k5eAaPmNgFnS	vystěhovat
ze	z	k7c2	z
sídla	sídlo	k1gNnSc2	sídlo
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
majitele	majitel	k1gMnSc2	majitel
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nespokojí	spokojit	k5eNaPmIp3nP	spokojit
s	s	k7c7	s
jejími	její	k3xOp3gNnPc7	její
aktivity	aktivita	k1gFnSc2	aktivita
podporující	podporující	k2eAgFnSc2d1	podporující
LGBT	LGBT	kA	LGBT
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
v	v	k7c4	v
soudní	soudní	k2eAgInSc4d1	soudní
spor	spor	k1gInSc4	spor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
skončil	skončit	k5eAaPmAgMnS	skončit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lambda	lambda	k1gNnSc1	lambda
Istanbul	Istanbul	k1gInSc1	Istanbul
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
soud	soud	k1gInSc1	soud
nižšího	nízký	k2eAgInSc2d2	nižší
stupně	stupeň	k1gInSc2	stupeň
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jeho	jeho	k3xOp3gInSc1	jeho
výrok	výrok	k1gInSc1	výrok
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
zrušen	zrušit	k5eAaPmNgInS	zrušit
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožnil	umožnit	k5eAaPmAgInS	umožnit
organizaci	organizace	k1gFnSc4	organizace
opět	opět	k6eAd1	opět
podnikat	podnikat	k5eAaImF	podnikat
své	svůj	k3xOyFgFnPc4	svůj
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
zákony	zákon	k1gInPc1	zákon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Trestní	trestní	k2eAgInSc1d1	trestní
zákoník	zákoník	k1gInSc1	zákoník
===	===	k?	===
</s>
</p>
<p>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgFnSc1d1	Stejnopohlavní
sexuální	sexuální	k2eAgFnSc1d1	sexuální
aktivita	aktivita	k1gFnSc1	aktivita
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
a	a	k8xC	a
věk	věk	k1gInSc4	věk
způsobilosti	způsobilost	k1gFnSc2	způsobilost
k	k	k7c3	k
pohlavnímu	pohlavní	k2eAgInSc3d1	pohlavní
styku	styk	k1gInSc3	styk
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
heterosexuální	heterosexuální	k2eAgFnSc4d1	heterosexuální
i	i	k8xC	i
homosexuální	homosexuální	k2eAgFnSc4d1	homosexuální
orientaci	orientace	k1gFnSc4	orientace
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c6	na
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Trestní	trestní	k2eAgInSc1d1	trestní
zákoník	zákoník	k1gInSc1	zákoník
však	však	k9	však
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
výrok	výrok	k1gInSc1	výrok
"	"	kIx"	"
<g/>
exhibicionismus	exhibicionismus	k1gInSc1	exhibicionismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
přestupek	přestupek	k1gInSc1	přestupek
proti	proti	k7c3	proti
morálce	morálka	k1gFnSc3	morálka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
proti	proti	k7c3	proti
gay	gay	k1gMnSc1	gay
a	a	k8xC	a
transgender	transgender	k1gMnSc1	transgender
osobám	osoba	k1gFnPc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgNnPc1d1	turecké
města	město	k1gNnPc1	město
a	a	k8xC	a
velkoměsta	velkoměsto	k1gNnPc1	velkoměsto
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
veřejné	veřejný	k2eAgFnPc4d1	veřejná
morálky	morálka	k1gFnPc4	morálka
schovívaví	schovívavý	k2eAgMnPc1d1	schovívavý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
soudce	soudce	k1gMnSc1	soudce
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
v	v	k7c6	v
rácmi	rác	k1gFnPc7	rác
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
s	s	k7c7	s
nelegálního	legální	k2eNgInSc2d1	nelegální
prodejcem	prodejce	k1gMnSc7	prodejce
125	[number]	k4	125
DVD	DVD	kA	DVD
s	s	k7c7	s
gay	gay	k1gMnSc1	gay
pornografií	pornografie	k1gFnPc2	pornografie
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
gay	gay	k1gMnSc1	gay
sex	sex	k1gInSc4	sex
za	za	k7c4	za
běžný	běžný	k2eAgInSc4d1	běžný
jev	jev	k1gInSc4	jev
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
respektována	respektován	k2eAgFnSc1d1	respektována
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
uznání	uznání	k1gNnSc4	uznání
stejnopohlavních	stejnopohlavní	k2eAgInPc2d1	stejnopohlavní
sňatků	sňatek	k1gInPc2	sňatek
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
odvolací	odvolací	k2eAgInSc1d1	odvolací
soud	soud	k1gInSc1	soud
v	v	k7c6	v
r.	r.	kA	r.
2012	[number]	k4	2012
označil	označit	k5eAaPmAgMnS	označit
zobrazování	zobrazování	k1gNnSc4	zobrazování
gay	gay	k1gMnSc1	gay
sexu	sex	k1gInSc2	sex
za	za	k7c4	za
protipřírodní	protipřírodní	k2eAgNnSc4d1	protipřírodní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mediální	mediální	k2eAgNnSc4d1	mediální
právo	právo	k1gNnSc4	právo
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
nejsou	být	k5eNaImIp3nP	být
zakázány	zakázán	k2eAgInPc1d1	zakázán
filmy	film	k1gInPc1	film
s	s	k7c7	s
LGBT	LGBT	kA	LGBT
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Zkrocená	zkrocený	k2eAgFnSc1d1	Zkrocená
hora	hora	k1gFnSc1	hora
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
tureckých	turecký	k2eAgInPc6d1	turecký
biografech	biograf	k1gInPc6	biograf
promítána	promítat	k5eAaImNgFnS	promítat
bez	bez	k7c2	bez
vládní	vládní	k2eAgFnSc2d1	vládní
cenzury	cenzura	k1gFnSc2	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
18	[number]	k4	18
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
koupit	koupit	k5eAaPmF	koupit
lístek	lístek	k1gInSc4	lístek
a	a	k8xC	a
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DVD	DVD	kA	DVD
s	s	k7c7	s
LGBT	LGBT	kA	LGBT
tematikou	tematika	k1gFnSc7	tematika
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
volně	volně	k6eAd1	volně
dostupné	dostupný	k2eAgInPc1d1	dostupný
v	v	k7c6	v
tureckých	turecký	k2eAgInPc6d1	turecký
obchodech	obchod	k1gInPc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
sexuální	sexuální	k2eAgInPc1d1	sexuální
explicitní	explicitní	k2eAgInPc1d1	explicitní
je	on	k3xPp3gMnPc4	on
možné	možný	k2eAgNnSc1d1	možné
prodat	prodat	k5eAaPmF	prodat
pouze	pouze	k6eAd1	pouze
osobám	osoba	k1gFnPc3	osoba
starším	starý	k2eAgFnPc3d2	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
pokutován	pokutovat	k5eAaImNgMnS	pokutovat
turecký	turecký	k2eAgMnSc1d1	turecký
prodejce	prodejce	k1gMnSc1	prodejce
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
"	"	kIx"	"
<g/>
amorálních	amorální	k2eAgFnPc2d1	amorální
<g/>
"	"	kIx"	"
DVD	DVD	kA	DVD
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
obsahu	obsah	k1gInSc2	obsah
homosexuálního	homosexuální	k2eAgInSc2d1	homosexuální
sexu	sex	k1gInSc2	sex
<g/>
.	.	kIx.	.
</s>
<s>
Pokutu	pokuta	k1gFnSc4	pokuta
pak	pak	k6eAd1	pak
následně	následně	k6eAd1	následně
zrušil	zrušit	k5eAaPmAgInS	zrušit
odvolací	odvolací	k2eAgInSc1d1	odvolací
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
jménem	jméno	k1gNnSc7	jméno
soudce	soudce	k1gMnSc2	soudce
Mahmuta	Mahmut	k1gMnSc2	Mahmut
Erdemli	Erdemle	k1gFnSc4	Erdemle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
rozsudku	rozsudek	k1gInSc2	rozsudek
stálo	stát	k5eAaImAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
homosexuální	homosexuální	k2eAgInSc1d1	homosexuální
sex	sex	k1gInSc1	sex
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
přirozený	přirozený	k2eAgMnSc1d1	přirozený
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
respektovat	respektovat	k5eAaImF	respektovat
individuální	individuální	k2eAgFnSc4d1	individuální
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
připomněl	připomnět	k5eAaPmAgMnS	připomnět
přístup	přístup	k1gInSc4	přístup
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
ke	k	k7c3	k
stejnopohlavnímu	stejnopohlavní	k2eAgNnSc3d1	stejnopohlavní
manželství	manželství	k1gNnSc3	manželství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Branný	branný	k2eAgInSc1d1	branný
zákon	zákon	k1gInSc1	zákon
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
je	být	k5eAaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
základní	základní	k2eAgFnSc1d1	základní
vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
muže	muž	k1gMnPc4	muž
ve	v	k7c6	v
věkovém	věkový	k2eAgNnSc6d1	věkové
rozmezí	rozmezí	k1gNnSc6	rozmezí
18	[number]	k4	18
až	až	k9	až
41	[number]	k4	41
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Turecká	turecký	k2eAgFnSc1d1	turecká
armáda	armáda	k1gFnSc1	armáda
otevřeně	otevřeně	k6eAd1	otevřeně
diskriminuje	diskriminovat	k5eAaBmIp3nS	diskriminovat
pasivní	pasivní	k2eAgMnPc4d1	pasivní
homosexuální	homosexuální	k2eAgMnPc4d1	homosexuální
muže	muž	k1gMnPc4	muž
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgMnPc1d1	aktivní
homosexuální	homosexuální	k2eAgMnPc1d1	homosexuální
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
bisexuálové	bisexuál	k1gMnPc1	bisexuál
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
sloužit	sloužit	k5eAaImF	sloužit
můžou	můžou	k?	můžou
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tu	tu	k6eAd1	tu
samou	samý	k3xTgFnSc4	samý
dobu	doba	k1gFnSc4	doba
Turecko	Turecko	k1gNnSc4	Turecko
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
nařízením	nařízení	k1gNnSc7	nařízení
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
svobod	svoboda	k1gFnPc2	svoboda
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
umožnit	umožnit	k5eAaPmF	umožnit
brancům	branec	k1gMnPc3	branec
zproštění	zproštění	k1gNnSc4	zproštění
od	od	k7c2	od
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
Branci	branec	k1gMnPc1	branec
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
vyhnout	vyhnout	k5eAaPmF	vyhnout
vojenské	vojenský	k2eAgFnSc3d1	vojenská
službě	služba	k1gFnSc3	služba
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
označit	označit	k5eAaPmF	označit
před	před	k7c7	před
odvodní	odvodní	k2eAgFnSc7d1	odvodní
komisí	komise	k1gFnSc7	komise
za	za	k7c4	za
nemocné	nemocný	k2eAgMnPc4d1	nemocný
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
podstoupit	podstoupit	k5eAaPmF	podstoupit
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
HRW	HRW	kA	HRW
nelidské	lidský	k2eNgFnSc2d1	nelidská
a	a	k8xC	a
degradující	degradující	k2eAgFnSc2d1	degradující
<g/>
)	)	kIx)	)
testy	testa	k1gFnSc2	testa
dokazující	dokazující	k2eAgFnSc1d1	dokazující
jejich	jejich	k3xOp3gFnSc4	jejich
homosexualitu	homosexualita	k1gFnSc4	homosexualita
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
rozšíření	rozšíření	k1gNnSc2	rozšíření
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Turecké	turecký	k2eAgFnPc1d1	turecká
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
stále	stále	k6eAd1	stále
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
nezpůsobilost	nezpůsobilost	k1gFnSc4	nezpůsobilost
homosexuální	homosexuální	k2eAgFnSc4d1	homosexuální
orientaci	orientace	k1gFnSc4	orientace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
označená	označený	k2eAgFnSc1d1	označená
za	za	k7c4	za
psychosexuální	psychosexuální	k2eAgFnSc4d1	psychosexuální
poruchu	porucha	k1gFnSc4	porucha
<g/>
,	,	kIx,	,
a	a	k8xC	a
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
homosexuálům	homosexuál	k1gMnPc3	homosexuál
otevřeně	otevřeně	k6eAd1	otevřeně
sloužit	sloužit	k5eAaImF	sloužit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
homosexuální	homosexuální	k2eAgMnSc1d1	homosexuální
branec	branec	k1gMnSc1	branec
musí	muset	k5eAaImIp3nS	muset
odvodní	odvodní	k2eAgFnSc4d1	odvodní
komisi	komise	k1gFnSc4	komise
dokazovat	dokazovat	k5eAaImF	dokazovat
svoji	svůj	k3xOyFgFnSc4	svůj
homosexualitu	homosexualita	k1gFnSc4	homosexualita
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
fotografií	fotografia	k1gFnPc2	fotografia
zobrazující	zobrazující	k2eAgFnSc4d1	zobrazující
jeho	jeho	k3xOp3gFnSc4	jeho
pasivní	pasivní	k2eAgFnSc4d1	pasivní
roli	role	k1gFnSc4	role
při	při	k7c6	při
stejnopohlavním	stejnopohlavní	k2eAgInSc6d1	stejnopohlavní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
musí	muset	k5eAaImIp3nS	muset
rovněž	rovněž	k9	rovněž
podstoupit	podstoupit	k5eAaPmF	podstoupit
nestoudné	stoudný	k2eNgInPc4d1	nestoudný
lékařské	lékařský	k2eAgInPc4d1	lékařský
testy	test	k1gInPc4	test
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
diskriminací	diskriminace	k1gFnSc7	diskriminace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
právní	právní	k2eAgFnSc1d1	právní
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
diskriminací	diskriminace	k1gFnSc7	diskriminace
v	v	k7c6	v
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnSc3d1	zdravotní
péči	péče	k1gFnSc3	péče
<g/>
,	,	kIx,	,
byldení	byldení	k1gNnSc6	byldení
<g/>
,	,	kIx,	,
zboží	zboží	k1gNnSc6	zboží
a	a	k8xC	a
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
<g/>
Zpráva	zpráva	k1gFnSc1	zpráva
z	z	k7c2	z
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
od	od	k7c2	od
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
ohledně	ohledně	k7c2	ohledně
otázky	otázka	k1gFnSc2	otázka
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
několik	několik	k4yIc1	několik
případů	případ	k1gInPc2	případ
diskriminace	diskriminace	k1gFnSc2	diskriminace
na	na	k7c6	na
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
LGBT	LGBT	kA	LGBT
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
byli	být	k5eAaImAgMnP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jiné	jiný	k2eAgFnSc2d1	jiná
sexuální	sexuální	k2eAgFnSc2d1	sexuální
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovení	ustanovení	k1gNnSc1	ustanovení
Trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
ohledně	ohledně	k7c2	ohledně
exhibicionismu	exhibicionismus	k1gInSc2	exhibicionismus
a	a	k8xC	a
přestupků	přestupek	k1gInPc2	přestupek
proti	proti	k7c3	proti
morálce	morálka	k1gFnSc3	morálka
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
užívány	užívat	k5eAaImNgInP	užívat
k	k	k7c3	k
diskriminaci	diskriminace	k1gFnSc3	diskriminace
LGBT	LGBT	kA	LGBT
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
přestupcích	přestupek	k1gInPc6	přestupek
často	často	k6eAd1	často
ukládá	ukládat	k5eAaImIp3nS	ukládat
transgender	transgender	k1gInSc1	transgender
osobám	osoba	k1gFnPc3	osoba
pokuty	pokuta	k1gFnSc2	pokuta
<g/>
.	.	kIx.	.
<g/>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
opozice	opozice	k1gFnSc1	opozice
CHP	CHP	kA	CHP
dala	dát	k5eAaPmAgFnS	dát
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
návrh	návrh	k1gInSc4	návrh
ohledně	ohledně	k7c2	ohledně
práv	právo	k1gNnPc2	právo
gayů	gay	k1gMnPc2	gay
<g/>
.	.	kIx.	.
<g/>
Lesby	lesba	k1gFnPc1	lesba
<g/>
,	,	kIx,	,
gayové	gay	k1gMnPc1	gay
<g/>
,	,	kIx,	,
bisexuálové	bisexuál	k1gMnPc1	bisexuál
a	a	k8xC	a
transgender	transgender	k1gInSc4	transgender
osoby	osoba	k1gFnSc2	osoba
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
mezi	mezi	k7c7	mezi
nejvíce	hodně	k6eAd3	hodně
ohroženou	ohrožený	k2eAgFnSc7d1	ohrožená
skupinou	skupina	k1gFnSc7	skupina
běženců	běženec	k1gMnPc2	běženec
a	a	k8xC	a
azylantů	azylant	k1gMnPc2	azylant
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
čtyři	čtyři	k4xCgFnPc4	čtyři
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
zahrnující	zahrnující	k2eAgFnPc4d1	zahrnující
Kurdy	Kurd	k1gMnPc7	Kurd
<g/>
,	,	kIx,	,
sekularisty	sekularista	k1gMnPc7	sekularista
<g/>
,	,	kIx,	,
islamisty	islamista	k1gMnPc7	islamista
a	a	k8xC	a
nacionalisty	nacionalista	k1gMnPc7	nacionalista
odsouhlasil	odsouhlasit	k5eAaPmAgInS	odsouhlasit
novelu	novela	k1gFnSc4	novela
Ústavy	ústav	k1gInPc4	ústav
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
LGBT	LGBT	kA	LGBT
osob	osoba	k1gFnPc2	osoba
před	před	k7c7	před
diskriminací	diskriminace	k1gFnSc7	diskriminace
<g/>
.	.	kIx.	.
<g/>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
zrušena	zrušit	k5eAaPmNgFnS	zrušit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nesouladu	nesoulad	k1gInSc2	nesoulad
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
subjekty	subjekt	k1gInPc7	subjekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
Ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rodinné	rodinný	k2eAgNnSc4d1	rodinné
právo	právo	k1gNnSc4	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
dosud	dosud	k6eAd1	dosud
neuznává	uznávat	k5eNaImIp3nS	uznávat
stejnopohlavní	stejnopohlavní	k2eAgNnSc1d1	stejnopohlavní
manželství	manželství	k1gNnSc1	manželství
<g/>
,	,	kIx,	,
jinou	jiný	k2eAgFnSc4d1	jiná
formu	forma	k1gFnSc4	forma
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1	stejnopohlavní
soužití	soužití	k1gNnSc2	soužití
nebo	nebo	k8xC	nebo
neregistrované	registrovaný	k2eNgNnSc4d1	neregistrované
soužití	soužití	k1gNnSc4	soužití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svoboda	svoboda	k1gFnSc1	svoboda
projevu	projev	k1gInSc2	projev
a	a	k8xC	a
shromažďování	shromažďování	k1gNnSc2	shromažďování
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
konání	konání	k1gNnSc4	konání
průvodu	průvod	k1gInSc2	průvod
Istanbul	Istanbul	k1gInSc1	Istanbul
Pride	Prid	k1gMnSc5	Prid
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
konání	konání	k1gNnSc1	konání
se	se	k3xPyFc4	se
krylo	krýt	k5eAaImAgNnS	krýt
s	s	k7c7	s
muslimským	muslimský	k2eAgInSc7d1	muslimský
posvátným	posvátný	k2eAgInSc7d1	posvátný
měsícem	měsíc	k1gInSc7	měsíc
Ramadánem	ramadán	k1gInSc7	ramadán
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
začátkem	začátek	k1gInSc7	začátek
a	a	k8xC	a
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
13	[number]	k4	13
<g/>
leté	letý	k2eAgFnSc6d1	letá
historii	historie	k1gFnSc6	historie
jeho	jeho	k3xOp3gNnSc2	jeho
konání	konání	k1gNnSc2	konání
rozehnala	rozehnat	k5eAaPmAgFnS	rozehnat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
průvod	průvod	k1gInSc1	průvod
zakázán	zakázat	k5eAaPmNgInS	zakázat
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
navzdory	navzdory	k7c3	navzdory
tomuto	tento	k3xDgNnSc3	tento
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
<g/>
,	,	kIx,	,
potrestáni	potrestat	k5eAaPmNgMnP	potrestat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
zakázalo	zakázat	k5eAaPmAgNnS	zakázat
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Ankara	Ankara	k1gFnSc1	Ankara
konání	konání	k1gNnSc1	konání
veškerých	veškerý	k3xTgFnPc2	veškerý
LGBT	LGBT	kA	LGBT
lidskoprávních	lidskoprávní	k2eAgFnPc2d1	lidskoprávní
a	a	k8xC	a
LGBT	LGBT	kA	LGBT
událostí	událost	k1gFnSc7	událost
kvůli	kvůli	k7c3	kvůli
údajné	údajný	k2eAgFnSc3d1	údajná
ochraně	ochrana	k1gFnSc3	ochrana
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Úřady	úřad	k1gInPc1	úřad
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
zákazu	zákaz	k1gInSc2	zákaz
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	k9	by
takové	takový	k3xDgFnSc2	takový
"	"	kIx"	"
<g/>
exhibice	exhibice	k1gFnSc2	exhibice
<g/>
"	"	kIx"	"
mohli	moct	k5eAaImAgMnP	moct
polarizovat	polarizovat	k5eAaImF	polarizovat
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
média	médium	k1gNnSc2	médium
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákaz	zákaz	k1gInSc1	zákaz
jde	jít	k5eAaImIp3nS	jít
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
obecným	obecný	k2eAgNnSc7d1	obecné
potlačováním	potlačování	k1gNnSc7	potlačování
občanských	občanský	k2eAgFnPc2d1	občanská
svobod	svoboda	k1gFnPc2	svoboda
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
==	==	k?	==
</s>
</p>
<p>
<s>
Přístupy	přístup	k1gInPc1	přístup
k	k	k7c3	k
legalizaci	legalizace	k1gFnSc3	legalizace
homosexuálních	homosexuální	k2eAgInPc2d1	homosexuální
svazků	svazek	k1gInPc2	svazek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
smíšené	smíšený	k2eAgInPc1d1	smíšený
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
Ipsos	Ipsosa	k1gFnPc2	Ipsosa
z	z	k7c2	z
r.	r.	kA	r.
2015	[number]	k4	2015
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
legalizaci	legalizace	k1gFnSc4	legalizace
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1	stejnopohlavní
manželství	manželství	k1gNnSc2	manželství
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
27	[number]	k4	27
%	%	kIx~	%
Turků	turek	k1gInPc2	turek
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
19	[number]	k4	19
%	%	kIx~	%
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
registrované	registrovaný	k2eAgNnSc4d1	registrované
partnerství	partnerství	k1gNnSc4	partnerství
<g/>
.	.	kIx.	.
25	[number]	k4	25
%	%	kIx~	%
zkoumaných	zkoumaný	k2eAgInPc2d1	zkoumaný
bylo	být	k5eAaImAgNnS	být
proti	proti	k7c3	proti
jakékoli	jakýkoli	k3yIgFnSc3	jakýkoli
instucionalizaci	instucionalizace	k1gFnSc3	instucionalizace
stejnopohlavních	stejnopohlavní	k2eAgInPc2d1	stejnopohlavní
párů	pár	k1gInPc2	pár
a	a	k8xC	a
29	[number]	k4	29
%	%	kIx~	%
se	se	k3xPyFc4	se
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
LGBT	LGBT	kA	LGBT
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
==	==	k?	==
</s>
</p>
<p>
<s>
LGBT	LGBT	kA	LGBT
osoby	osoba	k1gFnSc2	osoba
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
potýkat	potýkat	k5eAaImF	potýkat
s	s	k7c7	s
diskriminací	diskriminace	k1gFnSc7	diskriminace
<g/>
,	,	kIx,	,
harašmentem	harašment	k1gInSc7	harašment
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
násilím	násilí	k1gNnSc7	násilí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
sousedů	soused	k1gMnPc2	soused
<g/>
,	,	kIx,	,
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
,	,	kIx,	,
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
<g/>
,	,	kIx,	,
učitelů	učitel	k1gMnPc2	učitel
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1	homosexualita
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c4	mezi
Turky	Turek	k1gMnPc4	Turek
široce	široko	k6eAd1	široko
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c7	za
tabu	tabu	k1gNnSc7	tabu
a	a	k8xC	a
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgFnPc1d1	běžná
vraždy	vražda	k1gFnPc1	vražda
ze	z	k7c2	z
cti	čest	k1gFnSc2	čest
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stávají	stávat	k5eAaImIp3nP	stávat
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
ženy	žena	k1gFnSc2	žena
<g/>
)	)	kIx)	)
oběťmi	oběť	k1gFnPc7	oběť
vražd	vražda	k1gFnPc2	vražda
členů	člen	k1gMnPc2	člen
vlastní	vlastní	k2eAgFnSc2d1	vlastní
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nevhodného	vhodný	k2eNgNnSc2d1	nevhodné
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Ahmeda	Ahmed	k1gMnSc2	Ahmed
Yildize	Yildize	k1gFnSc2	Yildize
<g/>
,	,	kIx,	,
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
homosexuální	homosexuální	k2eAgFnSc2d1	homosexuální
oběti	oběť	k1gFnSc2	oběť
vraždy	vražda	k1gFnSc2	vražda
ze	z	k7c2	z
ctiVýzkumy	ctiVýzkum	k1gInPc7	ctiVýzkum
z	z	k7c2	z
období	období	k1gNnSc2	období
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2007-2009	[number]	k4	2007-2009
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
German	German	k1gMnSc1	German
Democratic	Democratice	k1gFnPc2	Democratice
Turkey	Turkea	k1gFnSc2	Turkea
Forum	forum	k1gNnSc1	forum
zveřejnilo	zveřejnit	k5eAaPmAgNnS	zveřejnit
13	[number]	k4	13
vražd	vražda	k1gFnPc2	vražda
v	v	k7c6	v
r.	r.	kA	r.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
5	[number]	k4	5
v	v	k7c6	v
r.	r.	kA	r.
2008	[number]	k4	2008
a	a	k8xC	a
4	[number]	k4	4
v	v	k7c6	v
r.	r.	kA	r.
2008	[number]	k4	2008
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sexuální	sexuální	k2eAgFnSc2d1	sexuální
identity	identita	k1gFnSc2	identita
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
New	New	k1gMnSc1	New
York	York	k1gInSc4	York
založil	založit	k5eAaPmAgMnS	založit
Human	Human	k1gMnSc1	Human
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watch	k1gMnSc1	Watch
(	(	kIx(	(
<g/>
HRW	HRW	kA	HRW
<g/>
)	)	kIx)	)
publikoval	publikovat	k5eAaBmAgMnS	publikovat
zprávu	zpráva	k1gFnSc4	zpráva
s	s	k7c7	s
titulkem	titulek	k1gInSc7	titulek
"	"	kIx"	"
<g/>
We	We	k1gMnSc1	We
Need	Need	k1gMnSc1	Need
a	a	k8xC	a
Law	Law	k1gMnSc1	Law
To	to	k9	to
Liberation	Liberation	k1gInSc4	Liberation
(	(	kIx(	(
<g/>
cz	cz	k?	cz
překld	překld	k1gInSc1	překld
"	"	kIx"	"
<g/>
Požadujem	Požadujem	k?	Požadujem
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
osvobození	osvobození	k1gNnSc4	osvobození
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Zpráva	zpráva	k1gFnSc1	zpráva
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
případy	případ	k1gInPc4	případ
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
policejních	policejní	k2eAgInPc2d1	policejní
harašmentů	harašment	k1gInPc2	harašment
<g/>
,	,	kIx,	,
loupeží	loupež	k1gFnPc2	loupež
a	a	k8xC	a
hrozících	hrozící	k2eAgFnPc2d1	hrozící
vražd	vražda	k1gFnPc2	vražda
gay	gay	k1gMnSc1	gay
mužům	muž	k1gMnPc3	muž
<g/>
.	.	kIx.	.
</s>
<s>
HRW	HRW	kA	HRW
si	se	k3xPyFc3	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
postup	postup	k1gInSc1	postup
tureckých	turecký	k2eAgMnPc2d1	turecký
úřadů	úřad	k1gInPc2	úřad
je	být	k5eAaImIp3nS	být
neadekvátní	adekvátní	k2eNgInSc1d1	neadekvátní
a	a	k8xC	a
nepřiměřený	přiměřený	k2eNgInSc1d1	nepřiměřený
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
vražd	vražda	k1gFnPc2	vražda
z	z	k7c2	z
nenávisti	nenávist	k1gFnSc2	nenávist
proti	proti	k7c3	proti
homosexuálům	homosexuál	k1gMnPc3	homosexuál
soudy	soud	k1gInPc7	soud
často	často	k6eAd1	často
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
jako	jako	k9	jako
přitěžující	přitěžující	k2eAgFnSc4d1	přitěžující
okolnost	okolnost	k1gFnSc4	okolnost
provokaci	provokace	k1gFnSc4	provokace
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
pachatelmů	pachatelm	k1gInPc2	pachatelm
nižší	nízký	k2eAgInPc4d2	nižší
tresty	trest	k1gInPc4	trest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Souhrnný	souhrnný	k2eAgInSc4d1	souhrnný
přehled	přehled	k1gInSc4	přehled
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
LGBT	LGBT	kA	LGBT
rights	rights	k1gInSc1	rights
in	in	k?	in
Turkey	Turkea	k1gFnSc2	Turkea
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgMnPc4d1	související
==	==	k?	==
</s>
</p>
<p>
<s>
LGBT	LGBT	kA	LGBT
práva	práv	k2eAgFnSc1d1	práva
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
Hande	Hande	k6eAd1	Hande
Kaderová	Kaderový	k2eAgFnSc1d1	Kaderový
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
LGBT	LGBT	kA	LGBT
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lambdaistanbul	Lambdaistanbul	k1gInSc1	Lambdaistanbul
LGBT	LGBT	kA	LGBT
Solidarity	solidarita	k1gFnSc2	solidarita
Association	Association	k1gInSc1	Association
<g/>
,	,	kIx,	,
Istanbul	Istanbul	k1gInSc1	Istanbul
based	based	k1gInSc1	based
LGBT	LGBT	kA	LGBT
association	association	k1gInSc1	association
</s>
</p>
<p>
<s>
Gladt	Gladt	k1gInSc1	Gladt
e.V.	e.V.	k?	e.V.
–	–	k?	–
Gays	Gays	k1gInSc1	Gays
&	&	k?	&
Lesbians	Lesbians	k1gInSc1	Lesbians
aus	aus	k?	aus
der	drát	k5eAaImRp2nS	drát
Türkei	Türkei	k1gNnSc4	Türkei
(	(	kIx(	(
<g/>
based	based	k1gInSc1	based
in	in	k?	in
Berlin	berlina	k1gFnPc2	berlina
/	/	kIx~	/
Germany	German	k1gInPc1	German
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TR	TR	kA	TR
Gay	gay	k1gMnSc1	gay
International	International	k1gMnSc1	International
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bilingual	Bilingual	k1gInSc1	Bilingual
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
the	the	k?	the
LGBTQ	LGBTQ	kA	LGBTQ
Life	Lif	k1gFnSc2	Lif
in	in	k?	in
Turkey	Turkea	k1gFnSc2	Turkea
<g/>
.	.	kIx.	.
<g/>
closed	closed	k1gInSc1	closed
</s>
</p>
<p>
<s>
Turk	Turk	k1gMnSc1	Turk
Gay	gay	k1gMnSc1	gay
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
Turkish	Turkish	k1gInSc1	Turkish
LGBT	LGBT	kA	LGBT
Community	Communit	k1gInPc1	Communit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
:	:	kIx,	:
Asia	Asia	k1gFnSc1	Asia
meets	meets	k6eAd1	meets
Europe	Europ	k1gInSc5	Europ
and	and	k?	and
Ancient	Ancient	k1gInSc1	Ancient
meets	meets	k1gInSc1	meets
modern	modern	k1gInSc4	modern
<g/>
,	,	kIx,	,
a	a	k8xC	a
gay	gay	k1gMnSc1	gay
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
travelogue	travelogue	k1gInSc1	travelogue
of	of	k?	of
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
including	including	k1gInSc1	including
a	a	k8xC	a
comprehensive	comprehensivat	k5eAaPmIp3nS	comprehensivat
review	review	k?	review
of	of	k?	of
gay	gay	k1gMnSc1	gay
clubs	clubs	k6eAd1	clubs
and	and	k?	and
tips	tips	k1gInSc1	tips
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LGBTI	LGBTI	kA	LGBTI
News	News	k1gInSc1	News
Turkey	Turkea	k1gMnSc2	Turkea
<g/>
,	,	kIx,	,
English-only	Englishnla	k1gMnSc2	English-onla
LGBTI	LGBTI	kA	LGBTI
news	news	k1gInSc1	news
website	websit	k1gInSc5	websit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lgbti	lgbti	k1gNnSc1	lgbti
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
LGBTI	LGBTI	kA	LGBTI
Union	union	k1gInSc1	union
in	in	k?	in
Turkey	Turkea	k1gFnSc2	Turkea
</s>
</p>
