<p>
<s>
Bollwerk	Bollwerk	k1gInSc1	Bollwerk
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
pozdně	pozdně	k6eAd1	pozdně
gotické	gotický	k2eAgFnSc2d1	gotická
bašty	bašta	k1gFnSc2	bašta
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
velké	velký	k2eAgFnSc2d1	velká
obezděné	obezděný	k2eAgFnSc2d1	obezděná
zemní	zemní	k2eAgFnSc2d1	zemní
plošiny	plošina	k1gFnSc2	plošina
zpravidla	zpravidla	k6eAd1	zpravidla
polygonálního	polygonální	k2eAgInSc2d1	polygonální
půdorysu	půdorys	k1gInSc2	půdorys
tvořící	tvořící	k2eAgNnSc1d1	tvořící
dělostřelecké	dělostřelecký	k2eAgNnSc1d1	dělostřelecké
postavení	postavení	k1gNnSc1	postavení
u	u	k7c2	u
pozdně	pozdně	k6eAd1	pozdně
gotických	gotický	k2eAgNnPc2d1	gotické
opevnění	opevnění	k1gNnPc2	opevnění
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
bastion	bastion	k1gInSc4	bastion
novověkých	novověký	k2eAgFnPc2d1	novověká
pevností	pevnost	k1gFnPc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
výkladů	výklad	k1gInPc2	výklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xS	jako
synonymum	synonymum	k1gNnSc1	synonymum
pro	pro	k7c4	pro
bastion	bastion	k1gInSc4	bastion
nebo	nebo	k8xC	nebo
rondel	rondel	k1gInSc4	rondel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
českých	český	k2eAgInPc2d1	český
hradů	hrad	k1gInPc2	hrad
byl	být	k5eAaImAgInS	být
bollwerk	bollwerk	k1gInSc1	bollwerk
použit	použít	k5eAaPmNgInS	použít
například	například	k6eAd1	například
v	v	k7c6	v
Buštěhradu	Buštěhrad	k1gInSc3	Buštěhrad
<g/>
,	,	kIx,	,
na	na	k7c6	na
Křivoklátu	Křivoklát	k1gInSc2	Křivoklát
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgInPc1d1	vybraný
pojmy	pojem	k1gInPc1	pojem
hradní	hradní	k2eAgFnSc2d1	hradní
architektury	architektura	k1gFnSc2	architektura
</s>
</p>
