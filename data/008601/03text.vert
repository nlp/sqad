<p>
<s>
Jeff	Jeff	k1gInSc1	Jeff
Daryl	Daryl	k1gInSc1	Daryl
Friesen	Friesen	k2eAgInSc1d1	Friesen
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1976	[number]	k4	1976
v	v	k7c6	v
Meadow	Meadow	k1gFnSc6	Meadow
Lake	Lake	k1gFnPc2	Lake
<g/>
,	,	kIx,	,
Saskatchewan	Saskatchewana	k1gFnPc2	Saskatchewana
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
kanadský	kanadský	k2eAgMnSc1d1	kanadský
hokejový	hokejový	k2eAgMnSc1d1	hokejový
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
junior	junior	k1gMnSc1	junior
v	v	k7c6	v
Regině	Regina	k1gFnSc6	Regina
Pats	Patsa	k1gFnPc2	Patsa
ve	v	k7c6	v
Western	Western	kA	Western
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
(	(	kIx(	(
<g/>
WHL	WHL	kA	WHL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
draftován	draftován	k2eAgInSc1d1	draftován
týmem	tým	k1gInSc7	tým
San	San	k1gFnSc2	San
Jose	Jose	k1gNnSc7	Jose
Sharks	Sharksa	k1gFnPc2	Sharksa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
11	[number]	k4	11
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
NHL	NHL	kA	NHL
hrával	hrávat	k5eAaImAgMnS	hrávat
původně	původně	k6eAd1	původně
na	na	k7c6	na
levém	levý	k2eAgNnSc6d1	levé
křídle	křídlo	k1gNnSc6	křídlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
křídle	křídlo	k1gNnSc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
7	[number]	k4	7
sezon	sezona	k1gFnPc2	sezona
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
San	San	k1gFnSc2	San
Jose	Jose	k1gNnSc7	Jose
Sharks	Sharksa	k1gFnPc2	Sharksa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
2000-2001	[number]	k4	2000-2001
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
Anaheim	Anaheima	k1gFnPc2	Anaheima
Ducks	Ducksa	k1gFnPc2	Ducksa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
za	za	k7c4	za
Ducks	Ducks	k1gInSc4	Ducks
nevynechal	vynechat	k5eNaPmAgInS	vynechat
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
sezonu	sezona	k1gFnSc4	sezona
2002-2003	[number]	k4	2002-2003
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
Devils	Devilsa	k1gFnPc2	Devilsa
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
východní	východní	k2eAgFnSc4d1	východní
konferenci	konference	k1gFnSc4	konference
proti	proti	k7c3	proti
Ottawa	Ottawa	k1gFnSc1	Ottawa
Senators	Senators	k1gInSc1	Senators
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Friesen	Friesen	k2eAgMnSc1d1	Friesen
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
vítězný	vítězný	k2eAgInSc4d1	vítězný
gól	gól	k1gInSc4	gól
tři	tři	k4xCgFnPc4	tři
minuty	minuta	k1gFnPc1	minuta
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
utkání	utkání	k1gNnSc2	utkání
a	a	k8xC	a
ponechána	ponechat	k5eAaPmNgFnS	ponechat
na	na	k7c4	na
7	[number]	k4	7
utkáni	utkán	k2eAgMnPc1d1	utkán
playoff	playoff	k1gInSc4	playoff
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gInSc4	jeho
třetí	třetí	k4xOgInSc4	třetí
vítězný	vítězný	k2eAgInSc4d1	vítězný
gól	gól	k1gInSc4	gól
v	v	k7c6	v
sérii	série	k1gFnSc6	série
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
playoff	playoff	k1gInSc1	playoff
v	v	k7c6	v
7	[number]	k4	7
utkání	utkání	k1gNnSc2	utkání
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
výluky	výluka	k1gFnSc2	výluka
v	v	k7c6	v
NHL	NHL	kA	NHL
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
nikde	nikde	k6eAd1	nikde
nehrál	hrát	k5eNaImAgMnS	hrát
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
platový	platový	k2eAgInSc1d1	platový
strop	strop	k1gInSc1	strop
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
Washington	Washington	k1gInSc1	Washington
Capitals	Capitalsa	k1gFnPc2	Capitalsa
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
draft	draft	k1gInSc4	draft
2006	[number]	k4	2006
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
Anaheim	Anaheima	k1gFnPc2	Anaheima
Ducks	Ducksa	k1gFnPc2	Ducksa
za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
kolo	kolo	k1gNnSc4	kolo
draftu	draft	k1gInSc2	draft
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
sezóny	sezóna	k1gFnSc2	sezóna
2005-06	[number]	k4	2005-06
byl	být	k5eAaImAgMnS	být
zraněn	zraněn	k2eAgMnSc1d1	zraněn
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Calgary	Calgary	k1gNnSc2	Calgary
Flames	Flamesa	k1gFnPc2	Flamesa
na	na	k7c4	na
1	[number]	k4	1
rok	rok	k1gInSc4	rok
a	a	k8xC	a
vydělal	vydělat	k5eAaPmAgMnS	vydělat
jsi	být	k5eAaImIp2nS	být
1.600.000	[number]	k4	1.600.000
dolarů	dolar	k1gInPc2	dolar
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
špatné	špatný	k2eAgFnSc6d1	špatná
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
šest	šest	k4xCc4	šest
branek	branka	k1gFnPc2	branka
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
šest	šest	k4xCc4	šest
asistencí	asistence	k1gFnPc2	asistence
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Calgary	Calgary	k1gNnSc2	Calgary
Flames	Flamesa	k1gFnPc2	Flamesa
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
poslat	poslat	k5eAaPmF	poslat
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
sezonu	sezona	k1gFnSc4	sezona
na	na	k7c4	na
farmu	farma	k1gFnSc4	farma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
AHL	AHL	kA	AHL
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
levé	levý	k2eAgNnSc4d1	levé
křídlo	křídlo	k1gNnSc4	křídlo
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Lake	Lak	k1gInSc2	Lak
Erie	Eri	k1gFnSc2	Eri
Monsters	Monstersa	k1gFnPc2	Monstersa
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
v	v	k7c6	v
San	San	k1gFnSc6	San
Jose	Jos	k1gInSc2	Jos
Sharks	Sharks	k1gInSc4	Sharks
předsezónní	předsezónní	k2eAgInSc4d1	předsezónní
tréninkový	tréninkový	k2eAgInSc4d1	tréninkový
kemp	kemp	k1gInSc4	kemp
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
se	s	k7c7	s
Sharks	Sharks	k1gInSc1	Sharks
<g/>
,	,	kIx,	,
výkonný	výkonný	k2eAgMnSc1d1	výkonný
viceprezident	viceprezident	k1gMnSc1	viceprezident
a	a	k8xC	a
generální	generální	k2eAgMnSc1d1	generální
manažer	manažer	k1gMnSc1	manažer
Doug	Doug	k1gMnSc1	Doug
Wilson	Wilson	k1gMnSc1	Wilson
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
z	z	k7c2	z
tréninkového	tréninkový	k2eAgInSc2d1	tréninkový
kempu	kemp	k1gInSc2	kemp
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
sezonu	sezona	k1gFnSc4	sezona
2008-2009	[number]	k4	2008-2009
vynechal	vynechat	k5eAaPmAgInS	vynechat
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Eisbären	Eisbärna	k1gFnPc2	Eisbärna
Berlin	berlina	k1gFnPc2	berlina
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Eisbären	Eisbärna	k1gFnPc2	Eisbärna
Berlin	berlina	k1gFnPc2	berlina
nakonec	nakonec	k6eAd1	nakonec
setrval	setrvat	k5eAaPmAgMnS	setrvat
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
sezoně	sezona	k1gFnSc6	sezona
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
European	Europeany	k1gInPc2	Europeany
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejproduktivnějším	produktivní	k2eAgMnSc7d3	nejproduktivnější
hráčem	hráč	k1gMnSc7	hráč
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
domácí	domácí	k2eAgFnSc4d1	domácí
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInPc4d1	osobní
==	==	k?	==
</s>
</p>
<p>
<s>
Friesen	Friesen	k2eAgMnSc1d1	Friesen
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Rhonda	Rhonda	k1gFnSc1	Rhonda
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
dceru	dcera	k1gFnSc4	dcera
Kaylee	Kayle	k1gFnSc2	Kayle
(	(	kIx(	(
<g/>
narozená	narozený	k2eAgFnSc1d1	narozená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Benjamin	Benjamin	k1gMnSc1	Benjamin
Ben	Ben	k1gInSc1	Ben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnPc1	ocenění
a	a	k8xC	a
úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
CHL	CHL	kA	CHL
-	-	kIx~	-
All-Rookie	All-Rookie	k1gFnSc1	All-Rookie
Team	team	k1gInSc1	team
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
CHL	CHL	kA	CHL
-	-	kIx~	-
Rookie	Rookie	k1gFnSc1	Rookie
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
WHL	WHL	kA	WHL
-	-	kIx~	-
Jim	on	k3xPp3gMnPc3	on
Piggott	Piggott	k2eAgInSc4d1	Piggott
Memorial	Memorial	k1gInSc4	Memorial
Trophy	Tropha	k1gFnSc2	Tropha
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
NHL	NHL	kA	NHL
-	-	kIx~	-
All-Rookie	All-Rookie	k1gFnSc1	All-Rookie
Team	team	k1gInSc1	team
</s>
</p>
<p>
<s>
==	==	k?	==
Prvenství	prvenství	k1gNnSc1	prvenství
==	==	k?	==
</s>
</p>
<p>
<s>
Debut	debut	k1gInSc1	debut
v	v	k7c6	v
NHL	NHL	kA	NHL
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
San	San	k1gFnSc6	San
Jose	Jose	k1gNnSc4	Jose
Sharks	Sharksa	k1gFnPc2	Sharksa
proti	proti	k7c3	proti
St.	st.	kA	st.
Louis	louis	k1gInSc1	louis
Blues	blues	k1gNnSc1	blues
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
v	v	k7c6	v
NHL	NHL	kA	NHL
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
San	San	k1gFnSc1	San
Jose	Jose	k1gInSc1	Jose
Sharks	Sharks	k1gInSc1	Sharks
proti	proti	k7c3	proti
Toronto	Toronto	k1gNnSc1	Toronto
Maple	Maple	k1gFnSc2	Maple
Leafs	Leafsa	k1gFnPc2	Leafsa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
asistence	asistence	k1gFnSc1	asistence
v	v	k7c6	v
NHL	NHL	kA	NHL
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
San	San	k1gFnSc1	San
Jose	Jose	k1gInSc1	Jose
Sharks	Sharks	k1gInSc1	Sharks
proti	proti	k7c3	proti
Dallas	Dallas	k1gMnSc1	Dallas
Stars	Stars	k1gInSc1	Stars
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
hattrick	hattrick	k1gInSc4	hattrick
v	v	k7c6	v
NHL	NHL	kA	NHL
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
Winnipeg	Winnipeg	k1gInSc1	Winnipeg
Jets	Jets	k1gInSc1	Jets
proti	proti	k7c3	proti
San	San	k1gFnSc3	San
Jose	Jos	k1gInSc2	Jos
Sharks	Sharks	k1gInSc1	Sharks
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Klubové	klubový	k2eAgFnPc1d1	klubová
statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentace	reprezentace	k1gFnSc2	reprezentace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
Friesen	Friesen	k2eAgMnSc1d1	Friesen
–	–	k?	–
statistiky	statistika	k1gFnPc1	statistika
na	na	k7c4	na
Hockeydb	Hockeydb	k1gInSc4	Hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
Friesen	Friesen	k2eAgMnSc1d1	Friesen
–	–	k?	–
statistiky	statistika	k1gFnPc1	statistika
na	na	k7c4	na
Eliteprospects	Eliteprospects	k1gInSc4	Eliteprospects
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
Friesen	Friesen	k2eAgMnSc1d1	Friesen
–	–	k?	–
statistiky	statistika	k1gFnPc1	statistika
na	na	k7c4	na
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
Friesen	Friesen	k2eAgMnSc1d1	Friesen
–	–	k?	–
statistiky	statistika	k1gFnPc1	statistika
na	na	k7c4	na
Legendsofhockey	Legendsofhockey	k1gInPc4	Legendsofhockey
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
