<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
Slovensko	Slovensko	k1gNnSc4	Slovensko
členem	člen	k1gMnSc7	člen
NATO	NATO	kA	NATO
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
