<s>
Vodík	vodík	k1gInSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
H	H	kA
latinsky	latinsky	k6eAd1
Hydrogenium	hydrogenium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejlehčí	lehký	k2eAgMnSc1d3
a	a	k8xC
nejjednodušší	jednoduchý	k2eAgInSc1d3
plynný	plynný	k2eAgInSc1d1
chemický	chemický	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
tvořící	tvořící	k2eAgFnSc4d1
převážnou	převážný	k2eAgFnSc4d1
část	část	k1gFnSc4
hmoty	hmota	k1gFnSc2
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
<g/>
.	.	kIx.
</s>