<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Palas	Palas	k1gMnSc1	Palas
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1915	[number]	k4	1915
–	–	k?	–
???	???	k?	???
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
poúnorový	poúnorový	k2eAgMnSc1d1	poúnorový
poslanec	poslanec	k1gMnSc1	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c2	za
KSČ	KSČ	kA	KSČ
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ve	v	k7c6	v
volebním	volební	k2eAgInSc6d1	volební
obvodu	obvod	k1gInSc6	obvod
Bruntál-Šternberk	Bruntál-Šternberk	k1gInSc1	Bruntál-Šternberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
profesně	profesně	k6eAd1	profesně
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
osmileté	osmiletý	k2eAgFnSc2d1	osmiletá
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Dvorce	dvorec	k1gInSc2	dvorec
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
ČSSD	ČSSD	kA	ČSSD
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Palas	Palas	k1gMnSc1	Palas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Palas	Palas	k1gMnSc1	Palas
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
</s>
</p>
