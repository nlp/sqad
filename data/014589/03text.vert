<s>
Sloveso	sloveso	k1gNnSc1
</s>
<s>
Sloveso	sloveso	k1gNnSc1
(	(	kIx(
<g/>
lat.	lat.	kA
verbum	verbum	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ohebný	ohebný	k2eAgInSc1d1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
(	(	kIx(
<g/>
jít	jít	k5eAaImF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stav	stav	k1gInSc1
(	(	kIx(
<g/>
ležet	ležet	k5eAaImF
<g/>
)	)	kIx)
nebo	nebo	k8xC
změnu	změna	k1gFnSc4
stavu	stav	k1gInSc2
(	(	kIx(
<g/>
zčervenat	zčervenat	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
je	být	k5eAaImIp3nS
sloveso	sloveso	k1gNnSc1
ve	v	k7c6
větě	věta	k1gFnSc6
přísudkem	přísudek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gMnPc4
sloves	sloveso	k1gNnPc2
</s>
<s>
plnovýznamová	plnovýznamový	k2eAgNnPc1d1
slovesa	sloveso	k1gNnPc1
mají	mít	k5eAaImIp3nP
vlastní	vlastní	k2eAgInSc4d1
význam	význam	k1gInSc4
-	-	kIx~
oproti	oproti	k7c3
pomocným	pomocný	k2eAgNnPc3d1
nebo	nebo	k8xC
způsobovým	způsobový	k2eAgNnPc3d1
slovesům	sloveso	k1gNnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
mnohá	mnohé	k1gNnPc1
níže	nízce	k6eAd2
uvedená	uvedený	k2eAgNnPc1d1
zvláštní	zvláštní	k2eAgNnPc1d1
slovesa	sloveso	k1gNnPc1
mohou	moct	k5eAaImIp3nP
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
fungovat	fungovat	k5eAaImF
i	i	k9
jako	jako	k9
plnovýznamová	plnovýznamový	k2eAgFnSc1d1
<g/>
,	,	kIx,
</s>
<s>
neplnovýznamová	plnovýznamový	k2eNgNnPc1d1
slovesa	sloveso	k1gNnPc1
</s>
<s>
pomocné	pomocný	k2eAgNnSc4d1
sloveso	sloveso	k1gNnSc4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
nesamostatná	samostatný	k2eNgFnSc1d1
složka	složka	k1gFnSc1
vedle	vedle	k7c2
významového	významový	k2eAgNnSc2d1
slovesa	sloveso	k1gNnSc2
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
složených	složený	k2eAgInPc2d1
slovesných	slovesný	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
české	český	k2eAgFnPc1d1
být	být	k5eAaImF
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
:	:	kIx,
přišel	přijít	k5eAaPmAgInS
jsem	být	k5eAaImIp1nS
<g/>
)	)	kIx)
</s>
<s>
spona	spona	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
přísudku	přísudek	k1gInSc2
jmenného	jmenný	k2eAgMnSc4d1
se	s	k7c7
sponou	spona	k1gFnSc7
(	(	kIx(
<g/>
např.	např.	kA
české	český	k2eAgFnPc1d1
být	být	k5eAaImF
v	v	k7c6
přísudku	přísudek	k1gInSc6
je	být	k5eAaImIp3nS
nemocný	nemocný	k2eAgMnSc1d1,k2eNgMnSc1d1
nebo	nebo	k8xC
byl	být	k5eAaImAgMnS
ředitelem	ředitel	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
kategoriální	kategoriální	k2eAgNnSc1d1
sloveso	sloveso	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
light	light	k1gInSc1
verb	verbum	k1gNnPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
ostatních	ostatní	k2eAgInPc2d1
slovesně-jmenných	slovesně-jmenný	k2eAgInPc2d1
přísudků	přísudek	k1gInPc2
(	(	kIx(
<g/>
verbonominální	verbonominální	k2eAgInSc1d1
predikát	predikát	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
hlavní	hlavní	k2eAgInSc1d1
význam	význam	k1gInSc1
přísudku	přísudek	k1gInSc2
přináší	přinášet	k5eAaImIp3nS
jmenná	jmenný	k2eAgFnSc1d1
složka	složka	k1gFnSc1
<g/>
,	,	kIx,
kategoriální	kategoriální	k2eAgNnSc1d1
sloveso	sloveso	k1gNnSc1
přispívá	přispívat	k5eAaImIp3nS
slovesnými	slovesný	k2eAgFnPc7d1
mluvnickými	mluvnický	k2eAgFnPc7d1
kategoriemi	kategorie	k1gFnPc7
osoby	osoba	k1gFnSc2
<g/>
,	,	kIx,
čísla	číslo	k1gNnSc2
<g/>
,	,	kIx,
času	čas	k1gInSc2
aj.	aj.	kA
(	(	kIx(
<g/>
např.	např.	kA
české	český	k2eAgFnPc1d1
mít	mít	k5eAaImF
v	v	k7c6
mít	mít	k5eAaImF
strach	strach	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
způsobové	způsobový	k2eAgFnSc3d1
(	(	kIx(
<g/>
modální	modální	k2eAgFnSc3d1
<g/>
)	)	kIx)
sloveso	sloveso	k1gNnSc1
se	se	k3xPyFc4
váže	vázat	k5eAaImIp3nS
s	s	k7c7
infinitivem	infinitiv	k1gInSc7
jiného	jiný	k2eAgNnSc2d1
<g/>
,	,	kIx,
plnovýznamového	plnovýznamový	k2eAgNnSc2d1
slovesa	sloveso	k1gNnSc2
a	a	k8xC
přidává	přidávat	k5eAaImIp3nS
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
významu	význam	k1gInSc3
rys	rys	k1gInSc1
nejistoty	nejistota	k1gFnPc4
<g/>
,	,	kIx,
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
povinnosti	povinnost	k1gFnPc4
nebo	nebo	k8xC
schopnosti	schopnost	k1gFnPc4
(	(	kIx(
<g/>
v	v	k7c6
češtině	čeština	k1gFnSc6
chtít	chtít	k5eAaImF
<g/>
,	,	kIx,
moci	moct	k5eAaImF
<g/>
,	,	kIx,
smět	smět	k5eAaImF
<g/>
,	,	kIx,
mít	mít	k5eAaImF
<g/>
,	,	kIx,
muset	muset	k5eAaImF
<g/>
,	,	kIx,
umět	umět	k5eAaImF
<g/>
,	,	kIx,
dát	dát	k5eAaPmF
se	se	k3xPyFc4
<g/>
)	)	kIx)
</s>
<s>
fázová	fázový	k2eAgNnPc1d1
slovesa	sloveso	k1gNnPc1
jsou	být	k5eAaImIp3nP
slovesa	sloveso	k1gNnPc4
vyjadřující	vyjadřující	k2eAgNnSc1d1
nějaký	nějaký	k3yIgInSc4
stav	stav	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
se	se	k3xPyFc4
činnost	činnost	k1gFnSc1
nachází	nacházet	k5eAaImIp3nS
(	(	kIx(
<g/>
např.	např.	kA
začít	začít	k5eAaPmF
<g/>
,	,	kIx,
přestat	přestat	k5eAaPmF
<g/>
,	,	kIx,
zůstat	zůstat	k5eAaPmF
<g/>
)	)	kIx)
</s>
<s>
přechodné	přechodný	k2eAgNnSc1d1
sloveso	sloveso	k1gNnSc1
(	(	kIx(
<g/>
tranzitivní	tranzitivní	k2eAgMnSc1d1
<g/>
)	)	kIx)
rozlišujeme	rozlišovat	k5eAaImIp1nP
podle	podle	k7c2
valence	valence	k1gFnSc2
slovesa	sloveso	k1gNnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
mají	mít	k5eAaImIp3nP
povinnou	povinný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
na	na	k7c4
přímý	přímý	k2eAgInSc4d1
předmět	předmět	k1gInSc4
ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
pádě	pád	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
české	český	k2eAgNnSc1d1
koupit	koupit	k5eAaPmF
[	[	kIx(
<g/>
něco	něco	k3yInSc4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovesný	slovesný	k2eAgInSc1d1
valenční	valenční	k2eAgInSc1d1
systém	systém	k1gInSc1
většiny	většina	k1gFnSc2
jazyků	jazyk	k1gInPc2
včetně	včetně	k7c2
češtiny	čeština	k1gFnSc2
je	být	k5eAaImIp3nS
ale	ale	k9
složitější	složitý	k2eAgMnSc1d2
a	a	k8xC
nevystačí	vystačit	k5eNaBmIp3nS
jen	jen	k9
s	s	k7c7
touto	tento	k3xDgFnSc7
kategorií	kategorie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Mluvnické	mluvnický	k2eAgFnPc1d1
kategorie	kategorie	k1gFnPc1
sloves	sloveso	k1gNnPc2
</s>
<s>
Slovesa	sloveso	k1gNnPc4
lze	lze	k6eAd1
časovat	časovat	k5eAaImF,k5eAaBmF
<g/>
,	,	kIx,
ve	v	k7c6
většině	většina	k1gFnSc6
jazyků	jazyk	k1gInPc2
svým	svůj	k3xOyFgInSc7
tvarem	tvar	k1gInSc7
vyjadřují	vyjadřovat	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
čas	čas	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
slovesný	slovesný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
rod	rod	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
vid	vid	k1gInSc1
</s>
<s>
třída	třída	k1gFnSc1
</s>
<s>
vzor	vzor	k1gInSc1
</s>
<s>
příčestí	příčestí	k1gNnSc1
</s>
<s>
Osoba	osoba	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Osoba	osoba	k1gFnSc1
(	(	kIx(
<g/>
mluvnice	mluvnice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Typicky	typicky	k6eAd1
existují	existovat	k5eAaImIp3nP
tři	tři	k4xCgFnPc1
kategorie	kategorie	k1gFnPc1
osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
rozlišují	rozlišovat	k5eAaImIp3nP
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
podmětem	podmět	k1gInSc7
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
<g/>
:	:	kIx,
</s>
<s>
mluvčí	mluvčí	k1gFnSc1
(	(	kIx(
<g/>
první	první	k4xOgFnSc1
osoba	osoba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
posluchač	posluchač	k1gMnSc1
(	(	kIx(
<g/>
druhá	druhý	k4xOgFnSc1
osoba	osoba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
někdo	někdo	k3yInSc1
jiný	jiný	k1gMnSc1
(	(	kIx(
<g/>
třetí	třetí	k4xOgFnSc1
osoba	osoba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
se	se	k3xPyFc4
od	od	k7c2
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
objevují	objevovat	k5eAaImIp3nP
odchylky	odchylka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
telugštině	telugština	k1gFnSc6
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc1
osoba	osoba	k1gFnSc1
množného	množný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
rozštěpena	rozštěpen	k2eAgFnSc1d1
na	na	k7c4
tvar	tvar	k1gInSc4
inkluzivní	inkluzivnět	k5eAaPmIp3nS
(	(	kIx(
<g/>
já	já	k3xPp1nSc1
+	+	kIx~
ty	ty	k3xPp2nSc1
+	+	kIx~
on	on	k3xPp3gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
exkluzivní	exkluzivní	k2eAgFnSc4d1
(	(	kIx(
<g/>
já	já	k3xPp1nSc1
+	+	kIx~
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
tebe	ty	k3xPp2nSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
mluvnice	mluvnice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
numerus	numerus	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
gramatická	gramatický	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
udává	udávat	k5eAaImIp3nS
počet	počet	k1gInSc4
účastníků	účastník	k1gMnPc2
děje	děj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
většiny	většina	k1gFnSc2
jazyků	jazyk	k1gInPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
češtiny	čeština	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kategorie	kategorie	k1gFnSc1
čísla	číslo	k1gNnSc2
založena	založit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
na	na	k7c6
protikladu	protiklad	k1gInSc6
jednosti	jednost	k1gFnSc2
-	-	kIx~
mnohosti	mnohost	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
je	být	k5eAaImIp3nS
struktura	struktura	k1gFnSc1
složitější	složitý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
jazyky	jazyk	k1gInPc4
naopak	naopak	k6eAd1
nerozlišují	rozlišovat	k5eNaImIp3nP
ani	ani	k8xC
jednotné	jednotný	k2eAgNnSc4d1
a	a	k8xC
množné	množný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
jen	jen	k9
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
-	-	kIx~
například	například	k6eAd1
v	v	k7c6
angličtině	angličtina	k1gFnSc6
je	být	k5eAaImIp3nS
druhá	druhý	k4xOgFnSc1
osoba	osoba	k1gFnSc1
identická	identický	k2eAgFnSc1d1
pro	pro	k7c4
obě	dva	k4xCgNnPc4
čísla	číslo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Čas	čas	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Čas	čas	k1gInSc1
(	(	kIx(
<g/>
mluvnice	mluvnice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čas	čas	k1gInSc1
(	(	kIx(
<g/>
tempus	tempus	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
lingvistice	lingvistika	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
určitý	určitý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
slovesa	sloveso	k1gNnSc2
vyjadřující	vyjadřující	k2eAgNnSc4d1
časové	časový	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
daný	daný	k2eAgInSc4d1
děj	děj	k1gInSc4
probíhal	probíhat	k5eAaImAgInS
<g/>
/	/	kIx~
<g/>
probíhá	probíhat	k5eAaImIp3nS
<g/>
/	/	kIx~
<g/>
bude	být	k5eAaImBp3nS
probíhat	probíhat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
různých	různý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
časy	čas	k1gInPc4
rozlišovat	rozlišovat	k5eAaImF
na	na	k7c4
aktuální	aktuální	k2eAgInSc4d1
(	(	kIx(
<g/>
minulý	minulý	k2eAgInSc4d1
<g/>
,	,	kIx,
přítomný	přítomný	k2eAgInSc4d1
<g/>
,	,	kIx,
budoucí	budoucí	k2eAgInSc4d1
<g/>
)	)	kIx)
a	a	k8xC
relativní	relativní	k2eAgFnSc7d1
(	(	kIx(
<g/>
např.	např.	kA
předminulý	předminulý	k2eAgMnSc1d1
<g/>
,	,	kIx,
předpřítomný	předpřítomný	k2eAgMnSc1d1
<g/>
,	,	kIx,
předbudoucí	předbudoucí	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
slouží	sloužit	k5eAaImIp3nP
k	k	k7c3
vyjádření	vyjádření	k1gNnSc3
předčasnosti	předčasnost	k1gFnSc2
či	či	k8xC
následnosti	následnost	k1gFnSc2
dějů	děj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
též	též	k9
rozlišovat	rozlišovat	k5eAaImF
časy	čas	k1gInPc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
probíhání	probíhání	k1gNnSc2
nebo	nebo	k8xC
opakování	opakování	k1gNnSc2
děje	děj	k1gInSc2
<g/>
,	,	kIx,
stavu	stav	k1gInSc2
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Způsob	způsob	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Slovesný	slovesný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Způsob	způsob	k1gInSc1
(	(	kIx(
<g/>
modus	modus	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mluvnickou	mluvnický	k2eAgFnSc7d1
kategorií	kategorie	k1gFnSc7
slovesa	sloveso	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
popisuje	popisovat	k5eAaImIp3nS
vztah	vztah	k1gInSc4
slovesa	sloveso	k1gNnSc2
a	a	k8xC
skutečnosti	skutečnost	k1gFnSc2
či	či	k8xC
záměru	záměr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
jazyků	jazyk	k1gInPc2
vyjadřuje	vyjadřovat	k5eAaImIp3nS
odlišnost	odlišnost	k1gFnSc1
způsobů	způsob	k1gInPc2
skrze	skrze	k?
morfologii	morfologie	k1gFnSc4
slovesa	sloveso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Čeština	čeština	k1gFnSc1
rozlišuje	rozlišovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
způsob	způsob	k1gInSc4
oznamovací	oznamovací	k2eAgInSc4d1
<g/>
,	,	kIx,
rozkazovací	rozkazovací	k2eAgInPc4d1
a	a	k8xC
podmiňovací	podmiňovací	k2eAgInPc4d1
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
jazyky	jazyk	k1gInPc1
mají	mít	k5eAaImIp3nP
systém	systém	k1gInSc1
slovesných	slovesný	k2eAgInPc2d1
způsobů	způsob	k1gInPc2
složitější	složitý	k2eAgMnSc1d2
(	(	kIx(
<g/>
např.	např.	kA
něnečtina	něnečtina	k1gFnSc1
-	-	kIx~
jazyk	jazyk	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
Ruska	Rusko	k1gNnSc2
-	-	kIx~
má	mít	k5eAaImIp3nS
16	#num#	k4
způsobů	způsob	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Slovesný	slovesný	k2eAgInSc4d1
rod	rod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Slovesný	slovesný	k2eAgInSc1d1
rod	rod	k1gInSc1
(	(	kIx(
<g/>
genus	genus	k1gInSc1
verbi	verb	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mluvnická	mluvnický	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
slovesa	sloveso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovesný	slovesný	k2eAgInSc1d1
rod	rod	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
různých	různý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
obvykle	obvykle	k6eAd1
dvojí	dvojí	k4xRgMnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
činný	činný	k2eAgMnSc1d1
(	(	kIx(
<g/>
aktivum	aktivum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
trpný	trpný	k2eAgMnSc1d1
(	(	kIx(
<g/>
pasivum	pasivum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
aktivum	aktivum	k1gNnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
děj	děj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
podmět	podmět	k1gInSc1
aktivně	aktivně	k6eAd1
vykonává	vykonávat	k5eAaImIp3nS
<g/>
,	,	kIx,
pasivum	pasivum	k1gNnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
děj	děj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
podmět	podmět	k1gInSc1
trpí	trpět	k5eAaImIp3nS
(	(	kIx(
<g/>
tj.	tj.	kA
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
vykonáván	vykonáván	k2eAgInSc1d1
na	na	k7c6
podmětu	podmět	k1gInSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
klasické	klasický	k2eAgFnSc6d1
řečtině	řečtina	k1gFnSc6
<g/>
)	)	kIx)
existuje	existovat	k5eAaImIp3nS
ještě	ještě	k9
třetí	třetí	k4xOgInSc1
rod	rod	k1gInSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
medium	medium	k1gNnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
případ	případ	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
podmět	podmět	k1gInSc1
koná	konat	k5eAaImIp3nS
děj	děj	k1gInSc4
na	na	k7c6
sobě	sebe	k3xPyFc6
nebo	nebo	k8xC
ve	v	k7c4
svůj	svůj	k3xOyFgInSc4
prospěch	prospěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
této	tento	k3xDgFnSc6
kategorii	kategorie	k1gFnSc6
přibližně	přibližně	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
zvratné	zvratný	k2eAgNnSc1d1
sloveso	sloveso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vid	vid	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Slovesný	slovesný	k2eAgInSc4d1
vid	vid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vid	vid	k1gInSc1
(	(	kIx(
<g/>
aspekt	aspekt	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mluvnickou	mluvnický	k2eAgFnSc7d1
kategorií	kategorie	k1gFnSc7
slovesa	sloveso	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
postihuje	postihovat	k5eAaImIp3nS
vztah	vztah	k1gInSc4
k	k	k7c3
plynutí	plynutí	k1gNnSc3
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
vymezit	vymezit	k5eAaPmF
dvě	dva	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Dokonavý	dokonavý	k2eAgMnSc1d1
/	/	kIx~
ukončený	ukončený	k2eAgMnSc1d1
(	(	kIx(
<g/>
perfektum	perfektum	k1gNnSc1
<g/>
)	)	kIx)
vs	vs	k?
<g/>
.	.	kIx.
neukončenost	neukončenost	k1gFnSc1
(	(	kIx(
<g/>
imperfektum	imperfektum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Nedokonavý	dokonavý	k2eNgInSc4d1
/	/	kIx~
průběhový	průběhový	k2eAgInSc4d1
(	(	kIx(
<g/>
durativ	durativum	k1gNnPc2
<g/>
)	)	kIx)
vs	vs	k?
<g/>
.	.	kIx.
neprůběhovost	neprůběhovost	k1gFnSc1
(	(	kIx(
<g/>
stavovost	stavovost	k1gFnSc1
<g/>
,	,	kIx,
punktuál	punktuál	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
Slovesná	slovesný	k2eAgFnSc1d1
třída	třída	k1gFnSc1
je	být	k5eAaImIp3nS
mluvnická	mluvnický	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
sloves	sloveso	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určuje	určovat	k5eAaImIp3nS
se	se	k3xPyFc4
podle	podle	k7c2
koncovky	koncovka	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
do	do	k7c2
pěti	pět	k4xCc2
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Neurčité	určitý	k2eNgInPc1d1
tvary	tvar	k1gInPc1
sloves	sloveso	k1gNnPc2
</s>
<s>
Sloveso	sloveso	k1gNnSc1
v	v	k7c6
neurčitém	určitý	k2eNgInSc6d1
tvaru	tvar	k1gInSc6
nevyjadřuje	vyjadřovat	k5eNaImIp3nS
mluvnické	mluvnický	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
nebo	nebo	k8xC
jen	jen	k6eAd1
některé	některý	k3yIgInPc1
z	z	k7c2
nich	on	k3xPp3gInPc2
(	(	kIx(
<g/>
např.	např.	kA
rod	rod	k1gInSc1
<g/>
,	,	kIx,
vid	vid	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišujeme	rozlišovat	k5eAaImIp1nP
následující	následující	k2eAgInPc4d1
neurčité	určitý	k2eNgInPc4d1
tvary	tvar	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
infinitiv	infinitiv	k1gInSc1
</s>
<s>
příčestí	příčestí	k1gNnSc1
</s>
<s>
přechodník	přechodník	k1gInSc1
</s>
<s>
gerundium	gerundium	k1gNnSc1
</s>
<s>
gerundivum	gerundivum	k1gNnSc1
</s>
<s>
supinum	supinum	k1gNnSc1
</s>
<s>
Infinitiv	infinitiv	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Infinitiv	infinitiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Infinitiv	infinitiv	k1gInSc1
(	(	kIx(
<g/>
zast.	zast.	k?
neurčitek	neurčitek	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
neurčitý	určitý	k2eNgInSc4d1
tvar	tvar	k1gInSc4
slovesa	sloveso	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
nečasuje	časovat	k5eNaBmIp3nS,k5eNaImIp3nS
<g/>
,	,	kIx,
tj.	tj.	kA
nevyjadřuje	vyjadřovat	k5eNaImIp3nS
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
způsob	způsob	k1gInSc4
a	a	k8xC
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokládá	pokládat	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
základní	základní	k2eAgInSc4d1
tvar	tvar	k1gInSc4
slovesa	sloveso	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgMnS
ve	v	k7c6
slovnících	slovník	k1gInPc6
(	(	kIx(
<g/>
reprezentativní	reprezentativní	k2eAgInSc1d1
tvar	tvar	k1gInSc1
neboli	neboli	k8xC
lemma	lemma	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
končí	končit	k5eAaImIp3nS
na	na	k7c4
-t	-t	k?
(	(	kIx(
<g/>
běžně	běžně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
-ti	-ti	k?
(	(	kIx(
<g/>
zast.	zast.	k?
nebo	nebo	k8xC
kniž.	kniž.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
-ci	-ci	k?
(	(	kIx(
<g/>
zast.	zast.	k?
nebo	nebo	k8xC
vyššího	vysoký	k2eAgInSc2d2
stylu	styl	k1gInSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
dělat	dělat	k5eAaImF
<g/>
/	/	kIx~
<g/>
dělati	dělat	k5eAaImF
<g/>
,	,	kIx,
moct	moct	k5eAaImF
<g/>
/	/	kIx~
<g/>
moci	moct	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
jazyky	jazyk	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
arabština	arabština	k1gFnSc1
<g/>
,	,	kIx,
bulharština	bulharština	k1gFnSc1
<g/>
,	,	kIx,
makedonština	makedonština	k1gFnSc1
a	a	k8xC
moderní	moderní	k2eAgFnSc1d1
řečtina	řečtina	k1gFnSc1
<g/>
,	,	kIx,
infinitiv	infinitiv	k1gInSc1
nemají	mít	k5eNaImIp3nP
a	a	k8xC
musejí	muset	k5eAaImIp3nP
ho	on	k3xPp3gInSc4
tedy	tedy	k9
vyjadřovat	vyjadřovat	k5eAaImF
opisně	opisně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Infinitiv	infinitiv	k1gInSc1
sice	sice	k8xC
nevyjadřuje	vyjadřovat	k5eNaImIp3nS
všechny	všechen	k3xTgFnPc4
mluvnické	mluvnický	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
sloves	sloveso	k1gNnPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
však	však	k9
schopnost	schopnost	k1gFnSc1
vyjadřovat	vyjadřovat	k5eAaImF
slovesný	slovesný	k2eAgInSc4d1
rod	rod	k1gInSc4
a	a	k8xC
vid	vid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
existují	existovat	k5eAaImIp3nP
infinitivy	infinitiv	k1gInPc1
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
časy	čas	k1gInPc4
<g/>
,	,	kIx,
např.	např.	kA
v	v	k7c6
angličtině	angličtina	k1gFnSc6
infinitiv	infinitiv	k1gInSc4
minulý	minulý	k2eAgInSc4d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
děj	děj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
začal	začít	k5eAaPmAgInS
nebo	nebo	k8xC
proběhl	proběhnout	k5eAaPmAgInS
před	před	k7c7
jiným	jiný	k2eAgInSc7d1
dějem	děj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Příčestí	příčestí	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Příčestí	příčestí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Příčestí	příčestí	k1gNnSc1
(	(	kIx(
<g/>
participium	participium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
neurčitý	určitý	k2eNgInSc4d1
jmenný	jmenný	k2eAgInSc4d1
tvar	tvar	k1gInSc4
slovesa	sloveso	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
jazyce	jazyk	k1gInSc6
různé	různý	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
účast	účast	k1gFnSc1
na	na	k7c6
tvorbě	tvorba	k1gFnSc6
složených	složený	k2eAgInPc2d1
slovesných	slovesný	k2eAgInPc2d1
časů	čas	k1gInPc2
<g/>
;	;	kIx,
</s>
<s>
účast	účast	k1gFnSc1
na	na	k7c6
tvorbě	tvorba	k1gFnSc6
trpného	trpný	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
;	;	kIx,
</s>
<s>
funkce	funkce	k1gFnSc1
přívlastku	přívlastek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
různé	různý	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
existuje	existovat	k5eAaImIp3nS
různý	různý	k2eAgInSc1d1
inventář	inventář	k1gInSc1
příčestí	příčestí	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
příčestí	příčestí	k1gNnSc2
minulé	minulý	k2eAgFnSc2d1
<g/>
,	,	kIx,
příčestí	příčestí	k1gNnSc1
trpné	trpný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přechodník	přechodník	k1gInSc1
<g/>
,	,	kIx,
gerundium	gerundium	k1gNnSc1
<g/>
,	,	kIx,
gerundivum	gerundivum	k1gNnSc1
a	a	k8xC
supinum	supinum	k1gNnSc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
rovněž	rovněž	k9
řadí	řadit	k5eAaImIp3nS
k	k	k7c3
příčestím	příčestí	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
Přechodník	přechodník	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Přechodník	přechodník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přechodník	přechodník	k1gInSc1
(	(	kIx(
<g/>
transgresiv	transgresiv	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c4
neurčitý	určitý	k2eNgInSc4d1
tvar	tvar	k1gInSc4
slovesa	sloveso	k1gNnSc2
vyjadřující	vyjadřující	k2eAgInSc1d1
současně	současně	k6eAd1
probíhající	probíhající	k2eAgInSc1d1
nebo	nebo	k8xC
navazující	navazující	k2eAgInPc1d1
děje	děj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
postupem	postup	k1gInSc7
času	čas	k1gInSc2
přechodníky	přechodník	k1gInPc1
přestaly	přestat	k5eAaPmAgFnP
běžně	běžně	k6eAd1
používat	používat	k5eAaImF
a	a	k8xC
nyní	nyní	k6eAd1
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
tvary	tvar	k1gInPc4
archaické	archaický	k2eAgInPc4d1
či	či	k8xC
archaizující	archaizující	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
historické	historický	k2eAgFnSc6d1
a	a	k8xC
umělecké	umělecký	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
větě	věta	k1gFnSc6
zastávají	zastávat	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
funkci	funkce	k1gFnSc4
doplňku	doplněk	k1gInSc2
(	(	kIx(
<g/>
vztahují	vztahovat	k5eAaImIp3nP
se	se	k3xPyFc4
k	k	k7c3
přísudku	přísudek	k1gInSc3
i	i	k9
k	k	k7c3
podmětu	podmět	k1gInSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Gerundium	gerundium	k1gNnSc1
</s>
<s>
Gerundium	gerundium	k1gNnSc1
je	být	k5eAaImIp3nS
neurčitý	určitý	k2eNgInSc4d1
tvar	tvar	k1gInSc4
slovesa	sloveso	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
podstatnému	podstatný	k2eAgNnSc3d1
jménu	jméno	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
víceméně	víceméně	k9
mu	on	k3xPp3gNnSc3
odpovídají	odpovídat	k5eAaImIp3nP
podstatná	podstatný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
slovesná	slovesný	k2eAgNnPc1d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
substantivní	substantivní	k2eAgFnSc2d1
deverbativa	deverbativum	k1gNnSc2
(	(	kIx(
<g/>
vedení	vedení	k1gNnSc1
<g/>
,	,	kIx,
krytí	krytí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžné	běžný	k2eAgInPc1d1
je	být	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
angličtině	angličtina	k1gFnSc6
(	(	kIx(
<g/>
cleaning	cleaning	k1gInSc1
-	-	kIx~
„	„	k?
<g/>
uklízení	uklízení	k1gNnSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
v	v	k7c6
latině	latina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Gerundivum	gerundivum	k1gNnSc1
</s>
<s>
Gerundivum	gerundivum	k1gNnSc1
je	být	k5eAaImIp3nS
neurčitý	určitý	k2eNgInSc4d1
tvar	tvar	k1gInSc4
slovesa	sloveso	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
přídavnému	přídavný	k2eAgNnSc3d1
jménu	jméno	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
víceméně	víceméně	k9
mu	on	k3xPp3gNnSc3
odpovídají	odpovídat	k5eAaImIp3nP
přídavná	přídavný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
<g/>
,	,	kIx,
utvořená	utvořený	k2eAgFnSc1d1
ze	z	k7c2
sloves	sloveso	k1gNnPc2
(	(	kIx(
<g/>
sedící	sedící	k2eAgFnSc1d1
<g/>
,	,	kIx,
hladovějící	hladovějící	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžné	běžný	k2eAgInPc1d1
je	být	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
latině	latina	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
plní	plnit	k5eAaImIp3nP
funkci	funkce	k1gFnSc4
příčestí	příčestí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Supinum	supinum	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Supinum	supinum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Supinum	supinum	k1gNnSc1
je	být	k5eAaImIp3nS
neurčitý	určitý	k2eNgInSc4d1
ustrnulý	ustrnulý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
slovesa	sloveso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využití	využití	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
různých	různý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytovalo	vyskytovat	k5eAaImAgNnS
se	se	k3xPyFc4
ve	v	k7c6
staré	starý	k2eAgFnSc6d1
češtině	čeština	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgNnSc7d1
českým	český	k2eAgNnSc7d1
supinem	supinum	k1gNnSc7
knižní	knižní	k2eAgInSc4d1
výraz	výraz	k1gInSc4
jít	jít	k5eAaImF
spat	spát	k5eAaImF
(	(	kIx(
<g/>
místo	místo	k1gNnSc4
spát	spát	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
například	například	k6eAd1
v	v	k7c6
latině	latina	k1gFnSc6
nebo	nebo	k8xC
švédštině	švédština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Česká	český	k2eAgNnPc1d1
slovesa	sloveso	k1gNnPc1
</s>
<s>
Časování	časování	k1gNnSc1
</s>
<s>
Přísudek	přísudek	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
sloveso	sloveso	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
sloveso	sloveso	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Slovesa	sloveso	k1gNnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Sloveso	sloveso	k1gNnSc4
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
https://www.mojecestina.cz/article/2014072702-slovesne-tridy-a-vzory	https://www.mojecestina.cz/article/2014072702-slovesne-tridy-a-vzora	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Slovní	slovní	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
zájmeno	zájmeno	k1gNnSc4
•	•	k?
číslovka	číslovka	k1gFnSc1
•	•	k?
sloveso	sloveso	k1gNnSc4
•	•	k?
příslovce	příslovce	k1gNnSc2
•	•	k?
předložka	předložka	k1gFnSc1
•	•	k?
spojka	spojka	k1gFnSc1
•	•	k?
částice	částice	k1gFnSc1
•	•	k?
citoslovce	citoslovce	k1gNnSc1
jméno	jméno	k1gNnSc4
</s>
<s>
obecné	obecný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
vlastní	vlastní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
bionymum	bionymum	k1gInSc1
</s>
<s>
antroponymum	antroponymum	k1gNnSc1
(	(	kIx(
<g/>
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
/	/	kIx~
křestní	křestní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
hypokoristikon	hypokoristikon	k1gNnSc1
•	•	k?
příjmí	příjmí	k?
•	•	k?
příjmení	příjmení	k1gNnSc2
•	•	k?
přezdívka	přezdívka	k1gFnSc1
•	•	k?
jméno	jméno	k1gNnSc1
po	po	k7c6
chalupě	chalupa	k1gFnSc6
•	•	k?
fiktonymum	fiktonymum	k1gInSc1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
obyvatelské	obyvatelský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodinné	rodinný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
etnonymum	etnonymum	k1gNnSc1
•	•	k?
theonymum	theonymum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
zoonymum	zoonymum	k1gInSc1
•	•	k?
fytonymum	fytonymum	k1gInSc1
abionymum	abionymum	k1gInSc1
</s>
<s>
toponymum	toponymum	k1gNnSc1
(	(	kIx(
<g/>
choronymum	choronymum	k1gInSc1
•	•	k?
oikonymum	oikonymum	k1gInSc1
•	•	k?
anoikonymum	anoikonymum	k1gInSc1
–	–	k?
urbanonymum	urbanonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kosmonymum	kosmonymum	k1gInSc1
/	/	kIx~
astronymum	astronymum	k1gInSc1
•	•	k?
chrématonymum	chrématonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
exonymum	exonymum	k1gInSc1
•	•	k?
endonymum	endonymum	k1gInSc1
•	•	k?
cizí	cizit	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
•	•	k?
standardizované	standardizovaný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4062553-9	4062553-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
6856	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85142794	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85142794	#num#	k4
</s>
