<s>
Sloveso	sloveso	k1gNnSc1
(	(	kIx(
<g/>
lat.	lat.	kA
verbum	verbum	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ohebný	ohebný	k2eAgInSc1d1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
(	(	kIx(
<g/>
jít	jít	k5eAaImF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stav	stav	k1gInSc1
(	(	kIx(
<g/>
ležet	ležet	k5eAaImF
<g/>
)	)	kIx)
nebo	nebo	k8xC
změnu	změna	k1gFnSc4
stavu	stav	k1gInSc2
(	(	kIx(
<g/>
zčervenat	zčervenat	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>