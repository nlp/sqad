<s>
Viktoriiny	Viktoriin	k2eAgInPc1d1	Viktoriin
vodopády	vodopád	k1gInPc1	vodopád
nebo	nebo	k8xC	nebo
Mosi-oa-Tunya	Mosia-Tuny	k2eAgNnPc1d1	Mosi-oa-Tuny
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Victoria	Victorium	k1gNnSc2	Victorium
Falls	Falls	k1gInSc4	Falls
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
vodopádů	vodopád	k1gInPc2	vodopád
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Zambezi	Zambezi	k1gNnSc2	Zambezi
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
mezi	mezi	k7c7	mezi
Zambií	Zambie	k1gFnSc7	Zambie
a	a	k8xC	a
Zimbabwe	Zimbabwe	k1gFnSc7	Zimbabwe
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
vodopádu	vodopád	k1gInSc2	vodopád
je	být	k5eAaImIp3nS	být
1800	[number]	k4	1800
m.	m.	k?	m.
Voda	voda	k1gFnSc1	voda
padá	padat	k5eAaImIp3nS	padat
dolů	dolů	k6eAd1	dolů
z	z	k7c2	z
útesu	útes	k1gInSc2	útes
vysokého	vysoký	k2eAgInSc2d1	vysoký
120	[number]	k4	120
m	m	kA	m
do	do	k7c2	do
úzkého	úzký	k2eAgInSc2d1	úzký
(	(	kIx(	(
<g/>
130	[number]	k4	130
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlubokého	hluboký	k2eAgInSc2d1	hluboký
(	(	kIx(	(
<g/>
140	[number]	k4	140
m	m	kA	m
<g/>
)	)	kIx)	)
kaňonu	kaňon	k1gInSc2	kaňon
v	v	k7c6	v
čediči	čedič	k1gInSc6	čedič
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc1d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
vodopád	vodopád	k1gInSc1	vodopád
nazývá	nazývat	k5eAaImIp3nS	nazývat
dým	dým	k1gInSc4	dým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
hřmí	hřmět	k5eAaImIp3nS	hřmět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
duha	duha	k1gFnSc1	duha
<g/>
.	.	kIx.	.
</s>
<s>
Průtoky	průtok	k1gInPc1	průtok
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
silně	silně	k6eAd1	silně
kolísají	kolísat	k5eAaImIp3nP	kolísat
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
je	být	k5eAaImIp3nS	být
1400	[number]	k4	1400
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Vodopády	vodopád	k1gInPc1	vodopád
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1855	[number]	k4	1855
skotským	skotský	k1gInSc7	skotský
cestovatelem	cestovatel	k1gMnSc7	cestovatel
Davidem	David	k1gMnSc7	David
Livingstonem	Livingston	k1gInSc7	Livingston
a	a	k8xC	a
nazvány	nazván	k2eAgFnPc1d1	nazvána
podle	podle	k7c2	podle
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
je	být	k5eAaImIp3nS	být
prozkoumal	prozkoumat	k5eAaPmAgMnS	prozkoumat
český	český	k2eAgMnSc1d1	český
cestovatel	cestovatel	k1gMnSc1	cestovatel
Emil	Emil	k1gMnSc1	Emil
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
jejich	jejich	k3xOp3gFnSc2	jejich
první	první	k4xOgFnSc2	první
mapy	mapa	k1gFnSc2	mapa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
cestopise	cestopis	k1gInSc6	cestopis
Sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
Holub	Holub	k1gMnSc1	Holub
je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
první	první	k4xOgFnSc2	první
samostatné	samostatný	k2eAgFnSc2d1	samostatná
publikace	publikace	k1gFnSc2	publikace
o	o	k7c6	o
Viktoriiných	Viktoriin	k2eAgInPc6d1	Viktoriin
vodopádech	vodopád	k1gInPc6	vodopád
–	–	k?	–
útlého	útlý	k2eAgInSc2d1	útlý
šestnáctistránkového	šestnáctistránkový	k2eAgInSc2d1	šestnáctistránkový
sešitku	sešitek	k1gInSc2	sešitek
The	The	k1gFnSc2	The
Victoria	Victorium	k1gNnSc2	Victorium
Falls	Fallsa	k1gFnPc2	Fallsa
-	-	kIx~	-
a	a	k8xC	a
few	few	k?	few
pages	pages	k1gMnSc1	pages
from	from	k1gMnSc1	from
a	a	k8xC	a
diary	diara	k1gFnPc1	diara
of	of	k?	of
Emil	Emil	k1gMnSc1	Emil
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
M.	M.	kA	M.
D.	D.	kA	D.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
jihoafrickém	jihoafrický	k2eAgNnSc6d1	jihoafrické
Grahamastownu	Grahamastowno	k1gNnSc6	Grahamastowno
(	(	kIx(	(
<g/>
reprint	reprint	k1gInSc1	reprint
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
В	В	k?	В
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Iguaçu	Iguaçu	k6eAd1	Iguaçu
vodopády	vodopád	k1gInPc1	vodopád
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Viktoriiny	Viktoriin	k2eAgInPc1d1	Viktoriin
vodopády	vodopád	k1gInPc1	vodopád
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
