<p>
<s>
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
je	být	k5eAaImIp3nS	být
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
hudební	hudební	k2eAgNnSc4d1	hudební
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
a	a	k8xC	a
zásilkový	zásilkový	k2eAgMnSc1d1	zásilkový
hudební	hudební	k2eAgMnSc1d1	hudební
distributor	distributor	k1gMnSc1	distributor
s	s	k7c7	s
dceřinými	dceřin	k2eAgFnPc7d1	dceřina
společnostmi	společnost	k1gFnPc7	společnost
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
toto	tento	k3xDgNnSc1	tento
hudební	hudební	k2eAgNnSc1d1	hudební
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
Markusem	Markus	k1gMnSc7	Markus
Staigerem	Staiger	k1gMnSc7	Staiger
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
vydávalo	vydávat	k5eAaPmAgNnS	vydávat
především	především	k9	především
hardcore	hardcor	k1gInSc5	hardcor
punkové	punkový	k2eAgInPc1d1	punkový
nahrávky	nahrávka	k1gFnPc4	nahrávka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
melodický	melodický	k2eAgInSc4d1	melodický
death	death	k1gInSc4	death
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
grindcore	grindcor	k1gMnSc5	grindcor
<g/>
,	,	kIx,	,
power	powrat	k5eAaPmRp2nS	powrat
metal	metal	k1gInSc1	metal
nebo	nebo	k8xC	nebo
black	black	k1gInSc1	black
metal	metat	k5eAaImAgInS	metat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
tribute	tribut	k1gInSc5	tribut
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
