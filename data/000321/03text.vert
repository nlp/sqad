<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
a	a	k8xC	a
současně	současně	k6eAd1	současně
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
mírně	mírně	k6eAd1	mírně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Čech	Čechy	k1gFnPc2	Čechy
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k7c2	uvnitř
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
je	být	k5eAaImIp3nS	být
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
kraj	kraj	k1gInSc1	kraj
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
státních	státní	k2eAgFnPc2d1	státní
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
dalších	další	k2eAgFnPc2d1	další
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
ústřední	ústřední	k2eAgInPc1d1	ústřední
státní	státní	k2eAgInPc1d1	státní
orgány	orgán	k1gInPc1	orgán
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
vrchních	vrchní	k2eAgInPc2d1	vrchní
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
je	být	k5eAaImIp3nS	být
Praha	Praha	k1gFnSc1	Praha
sídlem	sídlo	k1gNnSc7	sídlo
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgInPc2d1	další
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
ústředních	ústřední	k2eAgInPc6d1	ústřední
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
<g/>
;	;	kIx,	;
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
též	též	k9	též
ústředí	ústředí	k1gNnSc1	ústředí
většiny	většina	k1gFnSc2	většina
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnPc2	hnutí
a	a	k8xC	a
iniciativ	iniciativa	k1gFnPc2	iniciativa
a	a	k8xC	a
centrály	centrála	k1gFnSc2	centrála
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgNnPc2d1	náboženské
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
sdružení	sdružení	k1gNnPc2	sdružení
s	s	k7c7	s
celorepublikovou	celorepublikový	k2eAgFnSc7d1	celorepubliková
působností	působnost	k1gFnSc7	působnost
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
se	se	k3xPyFc4	se
Praha	Praha	k1gFnSc1	Praha
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
jedenáct	jedenáct	k4xCc4	jedenáct
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Coby	Coby	k?	Coby
historická	historický	k2eAgFnSc1d1	historická
metropole	metropole	k1gFnSc1	metropole
Čech	Čechy	k1gFnPc2	Čechy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
českých	český	k2eAgMnPc2d1	český
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
římsko-německých	římskoěmecký	k2eAgMnPc2d1	římsko-německý
císařů	císař	k1gMnPc2	císař
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
území	území	k1gNnSc6	území
496	[number]	k4	496
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
skoro	skoro	k6eAd1	skoro
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
4	[number]	k4	4
983	[number]	k4	983
km2	km2	k4	km2
žijí	žít	k5eAaImIp3nP	žít
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
Praha	Praha	k1gFnSc1	Praha
také	také	k9	také
vysoce	vysoce	k6eAd1	vysoce
ekonomicky	ekonomicky	k6eAd1	ekonomicky
vyspělým	vyspělý	k2eAgInSc7d1	vyspělý
a	a	k8xC	a
bohatým	bohatý	k2eAgInSc7d1	bohatý
regionem	region	k1gInSc7	region
s	s	k7c7	s
výjimečně	výjimečně	k6eAd1	výjimečně
vysokou	vysoký	k2eAgFnSc7d1	vysoká
životní	životní	k2eAgFnSc7d1	životní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tímto	tento	k3xDgInSc7	tento
vyniká	vynikat	k5eAaImIp3nS	vynikat
nejen	nejen	k6eAd1	nejen
nad	nad	k7c4	nad
české	český	k2eAgNnSc4d1	české
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nad	nad	k7c4	nad
evropské	evropský	k2eAgInPc4d1	evropský
standardy	standard	k1gInPc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
Eurostatu	Eurostat	k1gInSc2	Eurostat
je	být	k5eAaImIp3nS	být
devátým	devátý	k4xOgInSc7	devátý
nejbohatším	bohatý	k2eAgInSc7d3	nejbohatší
regionem	region	k1gInSc7	region
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
171	[number]	k4	171
%	%	kIx~	%
průměru	průměr	k1gInSc2	průměr
celé	celý	k2eAgFnSc2d1	celá
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
ČR	ČR	kA	ČR
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
80	[number]	k4	80
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
sídlí	sídlet	k5eAaImIp3nS	sídlet
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
univerzit	univerzita	k1gFnPc2	univerzita
(	(	kIx(	(
<g/>
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgNnPc2d3	nejkrásnější
měst	město	k1gNnPc2	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
s	s	k7c7	s
jedinečným	jedinečný	k2eAgNnSc7d1	jedinečné
panoramatem	panorama	k1gNnSc7	panorama
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
největšího	veliký	k2eAgInSc2d3	veliký
hradního	hradní	k2eAgInSc2d1	hradní
komplexu	komplex	k1gInSc2	komplex
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
památky	památka	k1gFnPc1	památka
přilákají	přilákat	k5eAaPmIp3nP	přilákat
ročně	ročně	k6eAd1	ročně
miliony	milion	k4xCgInPc1	milion
turistů	turist	k1gMnPc2	turist
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
Prahu	Praha	k1gFnSc4	Praha
6547700	[number]	k4	6547700
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
páté	pátý	k4xOgNnSc4	pátý
nejnavštěvovanější	navštěvovaný	k2eAgNnSc4d3	nejnavštěvovanější
město	město	k1gNnSc4	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
po	po	k7c6	po
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Istanbulu	Istanbul	k1gInSc6	Istanbul
a	a	k8xC	a
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
města	město	k1gNnSc2	město
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
Královské	královský	k2eAgNnSc4d1	královské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
jméno	jméno	k1gNnSc4	jméno
obvykle	obvykle	k6eAd1	obvykle
zní	znět	k5eAaImIp3nS	znět
Praga	Praga	k1gFnSc1	Praga
(	(	kIx(	(
<g/>
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
románských	románský	k2eAgInPc2d1	románský
a	a	k8xC	a
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prag	Prag	k1gMnSc1	Prag
(	(	kIx(	(
<g/>
němčina	němčina	k1gFnSc1	němčina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Prague	Prague	k1gFnSc1	Prague
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
dřívější	dřívější	k2eAgFnSc1d1	dřívější
výslovnost	výslovnost	k1gFnSc1	výslovnost
s	s	k7c7	s
g	g	kA	g
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
rámeček	rámeček	k1gInSc4	rámeček
"	"	kIx"	"
<g/>
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
jméno	jméno	k1gNnSc1	jméno
Praha	Praha	k1gFnSc1	Praha
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
nejvíce	nejvíce	k6eAd1	nejvíce
diskuzí	diskuze	k1gFnPc2	diskuze
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
nebudou	být	k5eNaImBp3nP	být
nikdy	nikdy	k6eAd1	nikdy
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
hypotézy	hypotéza	k1gFnPc1	hypotéza
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
zdůvodňovány	zdůvodňovat	k5eAaImNgInP	zdůvodňovat
jazykovědnými	jazykovědný	k2eAgFnPc7d1	jazykovědná
konstrukcemi	konstrukce	k1gFnPc7	konstrukce
nebo	nebo	k8xC	nebo
archeologickými	archeologický	k2eAgInPc7d1	archeologický
nálezy	nález	k1gInPc7	nález
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
práh	práh	k1gInSc1	práh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
nejuznávanější	uznávaný	k2eAgFnSc2d3	nejuznávanější
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
Praha	Praha	k1gFnSc1	Praha
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
říčním	říční	k2eAgInSc6d1	říční
prahu	práh	k1gInSc6	práh
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
brodu	brod	k1gInSc2	brod
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
někde	někde	k6eAd1	někde
pod	pod	k7c7	pod
dnešním	dnešní	k2eAgInSc7d1	dnešní
Karlovým	Karlův	k2eAgInSc7d1	Karlův
mostem	most	k1gInSc7	most
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
brod	brod	k1gInSc4	brod
přecházeli	přecházet	k5eAaImAgMnP	přecházet
lidé	člověk	k1gMnPc1	člověk
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
právě	právě	k9	právě
nad	nad	k7c7	nad
tímto	tento	k3xDgInSc7	tento
brodem	brod	k1gInSc7	brod
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
prahem	práh	k1gInSc7	práh
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nazváno	nazvat	k5eAaBmNgNnS	nazvat
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgMnPc1d1	dnešní
urbanisté	urbanista	k1gMnPc1	urbanista
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejvíce	hodně	k6eAd3	hodně
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
Prahy	Praha	k1gFnSc2	Praha
byly	být	k5eAaImAgInP	být
právě	právě	k6eAd1	právě
říční	říční	k2eAgInPc1d1	říční
brody	brod	k1gInPc1	brod
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
říčních	říční	k2eAgInPc2d1	říční
břehů	břeh	k1gInPc2	břeh
či	či	k8xC	či
činností	činnost	k1gFnPc2	činnost
prováděných	prováděný	k2eAgFnPc2d1	prováděná
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgNnPc2d1	jiné
vysvětlení	vysvětlení	k1gNnPc2	vysvětlení
bývá	bývat	k5eAaImIp3nS	bývat
pojmenování	pojmenování	k1gNnSc1	pojmenování
spojováno	spojovat	k5eAaImNgNnS	spojovat
také	také	k6eAd1	také
jednak	jednak	k8xC	jednak
s	s	k7c7	s
pražením	pražení	k1gNnSc7	pražení
(	(	kIx(	(
<g/>
opracováním	opracování	k1gNnSc7	opracování
ohněm	oheň	k1gInSc7	oheň
<g/>
)	)	kIx)	)
ať	ať	k8xS	ať
už	už	k6eAd1	už
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
kovů	kov	k1gInPc2	kov
nebo	nebo	k8xC	nebo
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
s	s	k7c7	s
vyprahlým	vyprahlý	k2eAgNnSc7d1	vyprahlé
návrším	návrší	k1gNnSc7	návrší
<g/>
,	,	kIx,	,
ostrohem	ostroh	k1gInSc7	ostroh
nad	nad	k7c7	nad
brodem	brod	k1gInSc7	brod
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
prazje	prazj	k1gFnPc4	prazj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
sluncem	slunce	k1gNnSc7	slunce
vyprahlé	vyprahlý	k2eAgNnSc1d1	vyprahlé
místo	místo	k1gNnSc4	místo
či	či	k8xC	či
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
takovou	takový	k3xDgFnSc4	takový
půdu	půda	k1gFnSc4	půda
samotnou	samotný	k2eAgFnSc4d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Prahy	Praha	k1gFnSc2	Praha
název	název	k1gInSc1	název
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kněžna	kněžna	k1gFnSc1	kněžna
Libuše	Libuše	k1gFnSc1	Libuše
nechala	nechat	k5eAaPmAgFnS	nechat
založit	založit	k5eAaPmF	založit
město	město	k1gNnSc4	město
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
osadník	osadník	k1gMnSc1	osadník
uprostřed	uprostřed	k7c2	uprostřed
lesa	les	k1gInSc2	les
tesal	tesat	k5eAaImAgInS	tesat
práh	práh	k1gInSc1	práh
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
srubu	srub	k1gInSc3	srub
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastek	přívlastek	k1gInSc1	přívlastek
Praga	Prag	k1gMnSc2	Prag
Caput	Caput	k1gInSc1	Caput
Regni	Regeň	k1gFnSc3	Regeň
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
hlava	hlava	k1gFnSc1	hlava
království	království	k1gNnSc4	království
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1518	[number]	k4	1518
byl	být	k5eAaImAgInS	být
vyznačen	vyznačit	k5eAaPmNgInS	vyznačit
na	na	k7c6	na
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
radnici	radnice	k1gFnSc6	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
znaku	znak	k1gInSc2	znak
Prahy	Praha	k1gFnSc2	Praha
přívlastek	přívlastek	k1gInSc1	přívlastek
Praha	Praha	k1gFnSc1	Praha
matka	matka	k1gFnSc1	matka
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Praga	Praga	k1gFnSc1	Praga
mater	mater	k1gFnSc2	mater
urbium	urbium	k1gNnSc1	urbium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
součástí	součást	k1gFnSc7	součást
znaku	znak	k1gInSc2	znak
přívlastek	přívlastek	k1gInSc1	přívlastek
Praga	Praga	k1gFnSc1	Praga
Caput	Caput	k1gMnSc1	Caput
Rei	Rea	k1gFnSc3	Rea
publicae	publicaat	k5eAaPmIp3nS	publicaat
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
hlava	hlava	k1gFnSc1	hlava
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastek	přívlastek	k1gInSc1	přívlastek
Praha	Praha	k1gFnSc1	Praha
stověžatá	stověžatý	k2eAgFnSc1d1	stověžatá
zřejmě	zřejmě	k6eAd1	zřejmě
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
historik	historik	k1gMnSc1	historik
Josef	Josef	k1gMnSc1	Josef
Hormayer	Hormayer	k1gMnSc1	Hormayer
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
tyto	tento	k3xDgFnPc4	tento
věže	věž	k1gFnPc4	věž
spočítal	spočítat	k5eAaPmAgMnS	spočítat
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Bernard	Bernard	k1gMnSc1	Bernard
Bolzano	Bolzana	k1gFnSc5	Bolzana
a	a	k8xC	a
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
103	[number]	k4	103
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vodáren	vodárna	k1gFnPc2	vodárna
a	a	k8xC	a
soukromých	soukromý	k2eAgInPc2d1	soukromý
domů	dům	k1gInPc2	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Prahy	Praha	k1gFnSc2	Praha
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
předhistorické	předhistorický	k2eAgFnSc6d1	předhistorická
době	doba	k1gFnSc6	doba
řada	řada	k1gFnSc1	řada
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
kmenů	kmen	k1gInPc2	kmen
–	–	k?	–
poslední	poslední	k2eAgInPc4d1	poslední
nálezy	nález	k1gInPc4	nález
u	u	k7c2	u
Křeslic	Křeslice	k1gFnPc2	Křeslice
datují	datovat	k5eAaImIp3nP	datovat
zdejší	zdejší	k2eAgNnSc4d1	zdejší
osídlení	osídlení	k1gNnSc4	osídlení
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
sedmi	sedm	k4xCc7	sedm
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kulturu	kultura	k1gFnSc4	kultura
s	s	k7c7	s
lineární	lineární	k2eAgFnSc7d1	lineární
–	–	k?	–
dříve	dříve	k6eAd2	dříve
volutovou	volutový	k2eAgFnSc7d1	volutová
–	–	k?	–
keramikou	keramika	k1gFnSc7	keramika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
200	[number]	k4	200
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
Závist	závist	k1gFnSc4	závist
založeno	založit	k5eAaPmNgNnS	založit
sídliště	sídliště	k1gNnSc1	sídliště
Keltů	Kelt	k1gMnPc2	Kelt
(	(	kIx(	(
<g/>
Bojové	bojový	k2eAgNnSc4d1	bojové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nyní	nyní	k6eAd1	nyní
stojí	stát	k5eAaImIp3nS	stát
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Ptolemaiova	Ptolemaiův	k2eAgFnSc1d1	Ptolemaiova
mapa	mapa	k1gFnSc1	mapa
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
germánské	germánský	k2eAgNnSc1d1	germánské
město	město	k1gNnSc1	město
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Casurgis	Casurgis	k1gInSc1	Casurgis
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
osídlovat	osídlovat	k5eAaImF	osídlovat
pražskou	pražský	k2eAgFnSc4d1	Pražská
kotlinu	kotlina	k1gFnSc4	kotlina
Slované	Slovan	k1gMnPc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
českou	český	k2eAgFnSc7d1	Česká
kněžnou	kněžna	k1gFnSc7	kněžna
a	a	k8xC	a
věštkyní	věštkyně	k1gFnSc7	věštkyně
Libuší	Libuše	k1gFnPc2	Libuše
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
Přemyslem	Přemysl	k1gMnSc7	Přemysl
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Libuše	Libuše	k1gFnSc1	Libuše
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c4	na
skalnatý	skalnatý	k2eAgInSc4d1	skalnatý
útes	útes	k1gInSc4	útes
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
prorokovala	prorokovat	k5eAaImAgFnS	prorokovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Město	město	k1gNnSc1	město
vidím	vidět	k5eAaImIp1nS	vidět
veliké	veliký	k2eAgNnSc1d1	veliké
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
sláva	sláva	k1gFnSc1	sláva
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
dotýkat	dotýkat	k5eAaImF	dotýkat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
nařídila	nařídit	k5eAaPmAgFnS	nařídit
vystavět	vystavět	k5eAaPmF	vystavět
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
město	město	k1gNnSc4	město
nazvala	nazvat	k5eAaBmAgFnS	nazvat
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgNnSc7d1	důležité
kultovním	kultovní	k2eAgNnSc7d1	kultovní
místem	místo	k1gNnSc7	místo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ostroh	ostroh	k1gInSc4	ostroh
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nechal	nechat	k5eAaPmAgMnS	nechat
kníže	kníže	k1gMnSc1	kníže
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
postavit	postavit	k5eAaPmF	postavit
koncem	koncem	k7c2	koncem
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
druhý	druhý	k4xOgInSc1	druhý
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
kostelík	kostelík	k1gInSc1	kostelík
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
zasvěcený	zasvěcený	k2eAgMnSc1d1	zasvěcený
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Bořivojova	Bořivojův	k2eAgMnSc2d1	Bořivojův
syna	syn	k1gMnSc2	syn
Spytihněva	Spytihněvo	k1gNnSc2	Spytihněvo
I.	I.	kA	I.
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
plošina	plošina	k1gFnSc1	plošina
ostrohu	ostroh	k1gInSc2	ostroh
obehnána	obehnat	k5eAaPmNgFnS	obehnat
obranným	obranný	k2eAgInSc7d1	obranný
valem	val	k1gInSc7	val
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zde	zde	k6eAd1	zde
knížecí	knížecí	k2eAgInSc1d1	knížecí
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
základy	základ	k1gInPc1	základ
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
kníže	kníže	k1gMnSc1	kníže
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
družinou	družina	k1gFnSc7	družina
z	z	k7c2	z
Levého	levý	k2eAgInSc2d1	levý
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
centrem	centr	k1gInSc7	centr
rodícího	rodící	k2eAgMnSc2d1	rodící
se	se	k3xPyFc4	se
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
hradišť	hradiště	k1gNnPc2	hradiště
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
začali	začít	k5eAaPmAgMnP	začít
přemyslovští	přemyslovský	k2eAgMnPc1d1	přemyslovský
vládci	vládce	k1gMnPc1	vládce
spravovat	spravovat	k5eAaImF	spravovat
okolní	okolní	k2eAgNnSc4d1	okolní
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
postupně	postupně	k6eAd1	postupně
podřídili	podřídit	k5eAaPmAgMnP	podřídit
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
přiměli	přimět	k5eAaPmAgMnP	přimět
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
odvádění	odvádění	k1gNnSc3	odvádění
dávek	dávka	k1gFnPc2	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
knížecího	knížecí	k2eAgInSc2d1	knížecí
hradu	hrad	k1gInSc2	hrad
přivedla	přivést	k5eAaPmAgFnS	přivést
do	do	k7c2	do
jeho	jeho	k3xOp3gNnPc2	jeho
podhradí	podhradí	k1gNnPc2	podhradí
řemeslníky	řemeslník	k1gMnPc4	řemeslník
a	a	k8xC	a
obchodníky	obchodník	k1gMnPc4	obchodník
–	–	k?	–
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
rodit	rodit	k5eAaImF	rodit
středověké	středověký	k2eAgNnSc4d1	středověké
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
kvetoucím	kvetoucí	k2eAgNnSc7d1	kvetoucí
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
vznosně	vznosně	k6eAd1	vznosně
vypínal	vypínat	k5eAaImAgInS	vypínat
knížecí	knížecí	k2eAgInSc1d1	knížecí
Hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tam	tam	k6eAd1	tam
žijí	žít	k5eAaImIp3nP	žít
Židé	Žid	k1gMnPc1	Žid
mající	mající	k2eAgMnPc1d1	mající
plno	plno	k6eAd1	plno
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
národů	národ	k1gInPc2	národ
nejbohatší	bohatý	k2eAgMnPc1d3	nejbohatší
kupci	kupec	k1gMnPc1	kupec
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
jsou	být	k5eAaImIp3nP	být
nejzámožnější	zámožný	k2eAgMnPc1d3	nejzámožnější
peněžníci	peněžník	k1gMnPc1	peněžník
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
stojí	stát	k5eAaImIp3nS	stát
tržiště	tržiště	k1gNnSc1	tržiště
a	a	k8xC	a
něm	on	k3xPp3gNnSc6	on
plno	plno	k6eAd1	plno
kořisti	kořist	k1gFnSc2	kořist
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgInS	napsat
kdosi	kdosi	k3yInSc1	kdosi
neznámý	známý	k2eNgMnSc1d1	neznámý
o	o	k7c6	o
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Kupec	kupec	k1gMnSc1	kupec
Ibráhím	Ibráhí	k1gMnSc7	Ibráhí
ibn	ibn	k?	ibn
Jákúb	Jákúb	k1gMnSc1	Jákúb
napsal	napsat	k5eAaBmAgMnS	napsat
již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
o	o	k7c6	o
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
město	město	k1gNnSc1	město
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
znakem	znak	k1gInSc7	znak
spíše	spíše	k9	spíše
bohatých	bohatý	k2eAgNnPc2d1	bohaté
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
Starší	starší	k1gMnSc1	starší
–	–	k?	–
respektive	respektive	k9	respektive
latinsky	latinsky	k6eAd1	latinsky
Maior	Maiora	k1gFnPc2	Maiora
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
také	také	k9	také
Větší	veliký	k2eAgInPc4d2	veliký
–	–	k?	–
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgFnSc6d1	Pražská
získala	získat	k5eAaPmAgFnS	získat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Přemysl	Přemysl	k1gMnSc1	Přemysl
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
udělil	udělit	k5eAaPmAgMnS	udělit
roku	rok	k1gInSc2	rok
1257	[number]	k4	1257
magdeburská	magdeburský	k2eAgFnSc1d1	Magdeburská
městská	městský	k2eAgFnSc1d1	městská
práva	právo	k1gNnPc4	právo
pražskému	pražský	k2eAgNnSc3d1	Pražské
podhradí	podhradí	k1gNnSc3	podhradí
<g/>
,	,	kIx,	,
osídlenému	osídlený	k2eAgMnSc3d1	osídlený
již	již	k6eAd1	již
minimálně	minimálně	k6eAd1	minimálně
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Menší	malý	k2eAgNnSc1d2	menší
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgNnSc1d1	Pražské
–	–	k?	–
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Příliv	příliv	k1gInSc1	příliv
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
kupců	kupec	k1gMnPc2	kupec
do	do	k7c2	do
obou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
sílil	sílit	k5eAaImAgInS	sílit
<g/>
,	,	kIx,	,
zvláště	zvláště	k9	zvláště
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
panování	panování	k1gNnPc2	panování
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
stala	stát	k5eAaPmAgFnS	stát
císařskou	císařský	k2eAgFnSc7d1	císařská
residencí	residence	k1gFnSc7	residence
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
učinit	učinit	k5eAaPmF	učinit
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
vymezený	vymezený	k2eAgInSc1d1	vymezený
hradbami	hradba	k1gFnPc7	hradba
pražských	pražský	k2eAgNnPc2d1	Pražské
měst	město	k1gNnPc2	město
však	však	k8xC	však
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
početné	početný	k2eAgMnPc4d1	početný
příchozí	příchozí	k1gMnPc4	příchozí
brzy	brzy	k6eAd1	brzy
příliš	příliš	k6eAd1	příliš
těsný	těsný	k2eAgInSc1d1	těsný
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
panovník	panovník	k1gMnSc1	panovník
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
k	k	k7c3	k
velkorysému	velkorysý	k2eAgInSc3d1	velkorysý
podniku	podnik	k1gInSc3	podnik
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1348	[number]	k4	1348
založil	založit	k5eAaPmAgMnS	založit
Nové	Nové	k2eAgNnSc4d1	Nové
Město	město	k1gNnSc4	město
pražské	pražský	k2eAgInPc4d1	pražský
a	a	k8xC	a
vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
území	území	k1gNnSc4	území
rozprostírající	rozprostírající	k2eAgFnSc2d1	rozprostírající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Vyšehradem	Vyšehrad	k1gInSc7	Vyšehrad
<g/>
,	,	kIx,	,
Poříčím	Poříčí	k1gNnSc7	Poříčí
a	a	k8xC	a
Starým	starý	k2eAgNnSc7d1	staré
Městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
zaplňováno	zaplňovat	k5eAaImNgNnS	zaplňovat
měšťanskými	měšťanský	k2eAgInPc7d1	měšťanský
domy	dům	k1gInPc7	dům
<g/>
,	,	kIx,	,
prostornými	prostorný	k2eAgNnPc7d1	prostorné
tržišti	tržiště	k1gNnPc7	tržiště
<g/>
,	,	kIx,	,
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
,	,	kIx,	,
nádhernými	nádherný	k2eAgFnPc7d1	nádherná
stavbami	stavba	k1gFnPc7	stavba
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
nádherou	nádhera	k1gFnSc7	nádhera
mohla	moct	k5eAaImAgFnS	moct
Praha	Praha	k1gFnSc1	Praha
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
soupeřit	soupeřit	k5eAaImF	soupeřit
s	s	k7c7	s
nejslavnějšími	slavný	k2eAgNnPc7d3	nejslavnější
městy	město	k1gNnPc7	město
Římem	Řím	k1gInSc7	Řím
<g/>
,	,	kIx,	,
Florencií	Florencie	k1gFnSc7	Florencie
<g/>
,	,	kIx,	,
Paříží	Paříž	k1gFnSc7	Paříž
<g/>
,	,	kIx,	,
Kolínem	Kolín	k1gInSc7	Kolín
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
o	o	k7c6	o
sídelním	sídelní	k2eAgNnSc6d1	sídelní
městě	město	k1gNnSc6	město
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Karlově	Karlův	k2eAgFnSc6d1	Karlova
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
jím	jíst	k5eAaImIp1nS	jíst
založených	založený	k2eAgInPc2d1	založený
kostelů	kostel	k1gInPc2	kostel
jen	jen	k9	jen
rozestavěna	rozestavěn	k2eAgFnSc1d1	rozestavěna
<g/>
.	.	kIx.	.
</s>
<s>
Bohatým	bohatý	k2eAgNnPc3d1	bohaté
italským	italský	k2eAgNnPc3d1	italské
městům	město	k1gNnPc3	město
jako	jako	k8xC	jako
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnPc1	Florencie
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
se	se	k3xPyFc4	se
nemohla	moct	k5eNaImAgFnS	moct
rovnat	rovnat	k5eAaImF	rovnat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tehdy	tehdy	k6eAd1	tehdy
Praha	Praha	k1gFnSc1	Praha
sice	sice	k8xC	sice
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnPc4d3	veliký
města	město	k1gNnPc4	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
rozlohou	rozloha	k1gFnSc7	rozloha
třetím	třetí	k4xOgMnSc6	třetí
největším	veliký	k2eAgInSc7d3	veliký
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnSc6d1	římská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Prahy	Praha	k1gFnSc2	Praha
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
ovšem	ovšem	k9	ovšem
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
v	v	k7c6	v
Římu	Řím	k1gInSc3	Řím
<g/>
,	,	kIx,	,
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
Janovu	Janův	k2eAgFnSc4d1	Janova
<g/>
,	,	kIx,	,
Florencii	Florencie	k1gFnSc3	Florencie
<g/>
,	,	kIx,	,
Milánu	Milán	k1gInSc3	Milán
<g/>
,	,	kIx,	,
Cařihradu	Cařihrad	k1gInSc3	Cařihrad
<g/>
,	,	kIx,	,
Paříži	Paříž	k1gFnSc6	Paříž
či	či	k8xC	či
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
pokusu	pokus	k1gInSc3	pokus
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
Starého	Starého	k2eAgNnSc2d1	Starého
a	a	k8xC	a
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1518	[number]	k4	1518
na	na	k7c4	na
popud	popud	k1gInSc4	popud
staroměstských	staroměstský	k2eAgMnPc2d1	staroměstský
měšťanů	měšťan	k1gMnPc2	měšťan
vedených	vedený	k2eAgFnPc2d1	vedená
Janem	Jan	k1gMnSc7	Jan
Paškem	Pašek	k1gInSc7	Pašek
z	z	k7c2	z
Vratu	vrat	k1gInSc2	vrat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1523	[number]	k4	1523
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
sjednocenou	sjednocený	k2eAgFnSc4d1	sjednocená
městskou	městský	k2eAgFnSc4d1	městská
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
spojení	spojení	k1gNnSc1	spojení
pražských	pražský	k2eAgNnPc2d1	Pražské
měst	město	k1gNnPc2	město
legalizoval	legalizovat	k5eAaBmAgInS	legalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
Praha	Praha	k1gFnSc1	Praha
však	však	k9	však
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1528	[number]	k4	1528
znovu	znovu	k6eAd1	znovu
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Sjednotit	sjednotit	k5eAaPmF	sjednotit
pražská	pražský	k2eAgNnPc4d1	Pražské
města	město	k1gNnPc4	město
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
správního	správní	k2eAgInSc2d1	správní
celku	celek	k1gInSc2	celek
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgNnSc1d1	královské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
pak	pak	k6eAd1	pak
tvořilo	tvořit	k5eAaImAgNnS	tvořit
Nové	Nové	k2eAgNnSc1d1	Nové
a	a	k8xC	a
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgNnSc1d1	Pražské
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
a	a	k8xC	a
Hradčany	Hradčany	k1gInPc1	Hradčany
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
Josefov	Josefov	k1gInSc1	Josefov
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
a	a	k8xC	a
Holešovice	Holešovice	k1gFnPc1	Holešovice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
již	již	k6eAd1	již
industrializovaným	industrializovaný	k2eAgInSc7d1	industrializovaný
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozvíjejícím	rozvíjející	k2eAgNnSc7d1	rozvíjející
městem	město	k1gNnSc7	město
se	s	k7c7	s
železnicí	železnice	k1gFnSc7	železnice
a	a	k8xC	a
továrnami	továrna	k1gFnPc7	továrna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
bylo	být	k5eAaImAgNnS	být
největší	veliký	k2eAgNnSc1d3	veliký
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
pražská	pražský	k2eAgFnSc1d1	Pražská
asanace	asanace	k1gFnSc1	asanace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Josefov	Josefov	k1gInSc4	Josefov
a	a	k8xC	a
Podskalí	Podskalí	k1gNnSc2	Podskalí
<g/>
;	;	kIx,	;
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
elektrické	elektrický	k2eAgFnSc2d1	elektrická
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
k	k	k7c3	k
městu	město	k1gNnSc3	město
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
připojila	připojit	k5eAaPmAgFnS	připojit
Libeň	Libeň	k1gFnSc1	Libeň
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Velká	velký	k2eAgFnSc1d1	velká
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
připojilo	připojit	k5eAaPmAgNnS	připojit
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
symbolů	symbol	k1gInPc2	symbol
rakouského	rakouský	k2eAgNnSc2d1	rakouské
mocnářství	mocnářství	k1gNnSc2	mocnářství
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
nebo	nebo	k8xC	nebo
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
pomník	pomník	k1gInSc1	pomník
maršála	maršál	k1gMnSc2	maršál
Václava	Václav	k1gMnSc2	Václav
Radeckého	Radecký	k1gMnSc2	Radecký
(	(	kIx(	(
<g/>
socha	socha	k1gFnSc1	socha
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Lapidáriu	lapidárium	k1gNnSc6	lapidárium
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
na	na	k7c6	na
Výstavišti	výstaviště	k1gNnSc6	výstaviště
a	a	k8xC	a
pomník	pomník	k1gInSc1	pomník
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
–	–	k?	–
později	pozdě	k6eAd2	pozdě
rovněž	rovněž	k9	rovněž
odstraněným	odstraněný	k2eAgInSc7d1	odstraněný
–	–	k?	–
pomníkem	pomník	k1gInSc7	pomník
Ernesta	Ernest	k1gMnSc4	Ernest
Denise	Denisa	k1gFnSc6	Denisa
<g/>
)	)	kIx)	)
na	na	k7c6	na
Malostranském	malostranský	k2eAgNnSc6d1	Malostranské
náměstí	náměstí	k1gNnSc6	náměstí
nebo	nebo	k8xC	nebo
socha	socha	k1gFnSc1	socha
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
Habsburka	Habsburk	k1gMnSc2	Habsburk
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
na	na	k7c6	na
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
nábřeží	nábřeží	k1gNnSc6	nábřeží
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
nově	nově	k6eAd1	nově
ustanoveného	ustanovený	k2eAgNnSc2d1	ustanovené
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
i	i	k9	i
nadále	nadále	k6eAd1	nadále
modernizována	modernizovat	k5eAaBmNgFnS	modernizovat
a	a	k8xC	a
rozšiřována	rozšiřovat	k5eAaImNgFnS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Velká	velký	k2eAgFnSc1d1	velká
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
byla	být	k5eAaImAgFnS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
všechna	všechen	k3xTgNnPc4	všechen
předměstí	předměstí	k1gNnSc4	předměstí
včetně	včetně	k7c2	včetně
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
samostatných	samostatný	k2eAgNnPc2d1	samostatné
měst	město	k1gNnPc2	město
jako	jako	k8xC	jako
Královské	královský	k2eAgInPc1d1	královský
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
,	,	kIx,	,
Nusle	Nusle	k1gFnPc1	Nusle
nebo	nebo	k8xC	nebo
Košíře	Košíře	k1gInPc1	Košíře
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v	v	k7c4	v
niž	jenž	k3xRgFnSc4	jenž
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
žilo	žít	k5eAaImAgNnS	žít
české	český	k2eAgNnSc1d1	české
<g/>
,	,	kIx,	,
německé	německý	k2eAgNnSc1d1	německé
a	a	k8xC	a
židovské	židovský	k2eAgNnSc1d1	Židovské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pozoruhodným	pozoruhodný	k2eAgNnSc7d1	pozoruhodné
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Historizující	historizující	k2eAgInSc1d1	historizující
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
např.	např.	kA	např.
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
pseudogotické	pseudogotický	k2eAgInPc1d1	pseudogotický
chrámy	chrám	k1gInPc1	chrám
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
a	a	k8xC	a
sv.	sv.	kA	sv.
Ludmily	Ludmila	k1gFnSc2	Ludmila
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
či	či	k8xC	či
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
vyšehradská	vyšehradský	k2eAgFnSc1d1	Vyšehradská
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
,	,	kIx,	,
rázně	rázně	k6eAd1	rázně
odvrhla	odvrhnout	k5eAaPmAgFnS	odvrhnout
secese	secese	k1gFnSc1	secese
přelomu	přelom	k1gInSc2	přelom
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Obecní	obecní	k2eAgInSc1d1	obecní
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
Hotel	hotel	k1gInSc1	hotel
Central	Central	k1gFnSc2	Central
v	v	k7c6	v
Hybernské	hybernský	k2eAgFnSc6d1	Hybernská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
a	a	k8xC	a
Průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
<g/>
,	,	kIx,	,
Husův	Husův	k2eAgInSc1d1	Husův
pomník	pomník	k1gInSc1	pomník
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Desetileté	desetiletý	k2eAgNnSc4d1	desetileté
secesní	secesní	k2eAgNnSc4d1	secesní
období	období	k1gNnSc4	období
přerušila	přerušit	k5eAaPmAgFnS	přerušit
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
projevovat	projevovat	k5eAaImF	projevovat
funkcionalismus	funkcionalismus	k1gInSc4	funkcionalismus
<g/>
;	;	kIx,	;
byla	být	k5eAaImAgFnS	být
také	také	k9	také
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
gotická	gotický	k2eAgFnSc1d1	gotická
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1929	[number]	k4	1929
(	(	kIx(	(
<g/>
za	za	k7c2	za
prezidenta	prezident	k1gMnSc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc4	Masaryk
a	a	k8xC	a
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
Františka	František	k1gMnSc4	František
Kordače	Kordač	k1gMnSc4	Kordač
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
nad	nad	k7c7	nad
klenbou	klenba	k1gFnSc7	klenba
triumfálního	triumfální	k2eAgInSc2d1	triumfální
oblouku	oblouk	k1gInSc2	oblouk
chóru	chór	k1gInSc2	chór
dómu	dóm	k1gInSc2	dóm
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
dostavba	dostavba	k1gFnSc1	dostavba
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřít	k5eAaPmNgFnS	otevřít
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
zavraždění	zavraždění	k1gNnSc2	zavraždění
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Světově	světově	k6eAd1	světově
unikátní	unikátní	k2eAgFnSc1d1	unikátní
je	být	k5eAaImIp3nS	být
architektura	architektura	k1gFnSc1	architektura
kubistická	kubistický	k2eAgFnSc1d1	kubistická
–	–	k?	–
jedině	jedině	k6eAd1	jedině
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
přešel	přejít	k5eAaPmAgInS	přejít
kubismus	kubismus	k1gInSc1	kubismus
z	z	k7c2	z
malířských	malířský	k2eAgNnPc2d1	malířské
pláten	plátno	k1gNnPc2	plátno
do	do	k7c2	do
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Janák	Janák	k1gMnSc1	Janák
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Gočár	Gočár	k1gMnSc1	Gočár
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Chochol	Chochol	k1gMnSc1	Chochol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ambiciózním	ambiciózní	k2eAgInSc7d1	ambiciózní
plánem	plán	k1gInSc7	plán
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc1	založení
kubistického	kubistický	k2eAgNnSc2d1	kubistické
sídliště	sídliště	k1gNnSc2	sídliště
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
ochromen	ochromit	k5eAaPmNgInS	ochromit
<g/>
;	;	kIx,	;
po	po	k7c6	po
incidentu	incident	k1gInSc6	incident
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
obětí	oběť	k1gFnSc7	oběť
byl	být	k5eAaImAgMnS	být
student	student	k1gMnSc1	student
Jan	Jan	k1gMnSc1	Jan
Opletal	opletat	k5eAaImAgMnS	opletat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zavřeny	zavřít	k5eAaPmNgFnP	zavřít
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
předáci	předák	k1gMnPc1	předák
studentů	student	k1gMnPc2	student
popraveni	popraven	k2eAgMnPc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
deportováni	deportovat	k5eAaBmNgMnP	deportovat
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
příslušníci	příslušník	k1gMnPc1	příslušník
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
méněcenných	méněcenný	k2eAgFnPc2d1	méněcenná
ras	rasa	k1gFnPc2	rasa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zejména	zejména	k9	zejména
židovské	židovský	k2eAgFnSc2d1	židovská
a	a	k8xC	a
cikánské	cikánský	k2eAgFnSc2d1	cikánská
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
označení	označení	k1gNnSc2	označení
<g/>
)	)	kIx)	)
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgFnPc1d1	běžná
byly	být	k5eAaImAgFnP	být
popravy	poprava	k1gFnPc1	poprava
a	a	k8xC	a
věznění	věznění	k1gNnPc1	věznění
odpůrců	odpůrce	k1gMnPc2	odpůrce
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Nechvalně	chvalně	k6eNd1	chvalně
proslulou	proslulý	k2eAgFnSc7d1	proslulá
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
stala	stát	k5eAaPmAgFnS	stát
například	například	k6eAd1	například
úřadovna	úřadovna	k1gFnSc1	úřadovna
gestapa	gestapo	k1gNnSc2	gestapo
v	v	k7c6	v
Petschkově	Petschkův	k2eAgInSc6d1	Petschkův
paláci	palác	k1gInSc6	palác
nebo	nebo	k8xC	nebo
střelnice	střelnice	k1gFnSc2	střelnice
v	v	k7c6	v
Kobylisích	Kobylisy	k1gInPc6	Kobylisy
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
dále	daleko	k6eAd2	daleko
zesílil	zesílit	k5eAaPmAgInS	zesílit
represe	represe	k1gFnPc4	represe
po	po	k7c6	po
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
zastupujícího	zastupující	k2eAgMnSc4d1	zastupující
říšského	říšský	k2eAgMnSc4d1	říšský
protektora	protektor	k1gMnSc4	protektor
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
byly	být	k5eAaImAgInP	být
vyhlazeny	vyhladit	k5eAaPmNgInP	vyhladit
vesnice	vesnice	k1gFnSc2	vesnice
Lidice	Lidice	k1gInPc1	Lidice
a	a	k8xC	a
Ležáky	Ležáky	k1gInPc1	Ležáky
<g/>
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
Pražské	pražský	k2eAgNnSc1d1	Pražské
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
kolem	kolem	k6eAd1	kolem
7	[number]	k4	7
400	[number]	k4	400
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
civilistů	civilista	k1gMnPc2	civilista
a	a	k8xC	a
na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
nacistické	nacistický	k2eAgFnSc2d1	nacistická
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dohod	dohoda	k1gFnPc2	dohoda
mezi	mezi	k7c7	mezi
spojenci	spojenec	k1gMnPc7	spojenec
z	z	k7c2	z
protihitlerovské	protihitlerovský	k2eAgFnSc2d1	protihitlerovská
koalice	koalice	k1gFnSc2	koalice
ovšem	ovšem	k9	ovšem
musela	muset	k5eAaImAgFnS	muset
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
zůstat	zůstat	k5eAaPmF	zůstat
stát	stát	k1gInSc4	stát
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Pražskému	pražský	k2eAgNnSc3d1	Pražské
povstání	povstání	k1gNnSc3	povstání
významně	významně	k6eAd1	významně
pomohli	pomoct	k5eAaPmAgMnP	pomoct
Vlasovci	vlasovec	k1gMnPc1	vlasovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vydáni	vydat	k5eAaPmNgMnP	vydat
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
byli	být	k5eAaImAgMnP	být
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
pražští	pražský	k2eAgMnPc1d1	pražský
Němci	Němec	k1gMnPc1	Němec
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
vysídleni	vysídlen	k2eAgMnPc1d1	vysídlen
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgFnPc4d1	válečná
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
zástavbě	zástavba	k1gFnSc6	zástavba
byly	být	k5eAaImAgInP	být
minimální	minimální	k2eAgFnSc4d1	minimální
<g/>
;	;	kIx,	;
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
likvidaci	likvidace	k1gFnSc4	likvidace
města	město	k1gNnSc2	město
už	už	k6eAd1	už
nemohl	moct	k5eNaImAgMnS	moct
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
oslabený	oslabený	k2eAgInSc4d1	oslabený
a	a	k8xC	a
ustupující	ustupující	k2eAgInSc4d1	ustupující
nacistický	nacistický	k2eAgInSc4d1	nacistický
režim	režim	k1gInSc4	režim
provést	provést	k5eAaPmF	provést
<g/>
,	,	kIx,	,
k	k	k7c3	k
vážnějšímu	vážní	k2eAgNnSc3d2	vážnější
poškození	poškození	k1gNnSc3	poškození
tedy	tedy	k9	tedy
došlo	dojít	k5eAaPmAgNnS	dojít
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
trojnásobného	trojnásobný	k2eAgNnSc2d1	trojnásobné
bombardování	bombardování	k1gNnSc2	bombardování
americkými	americký	k2eAgNnPc7d1	americké
letadly	letadlo	k1gNnPc7	letadlo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
:	:	kIx,	:
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
zejména	zejména	k6eAd1	zejména
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
Nuslích	Nusle	k1gFnPc6	Nusle
<g/>
,	,	kIx,	,
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
na	na	k7c4	na
strategické	strategický	k2eAgInPc4d1	strategický
cíle	cíl	k1gInPc4	cíl
ve	v	k7c6	v
Vysočanech	Vysočany	k1gInPc6	Vysočany
<g/>
,	,	kIx,	,
Libni	Libeň	k1gFnSc6	Libeň
a	a	k8xC	a
Kbelích	Kbely	k1gInPc6	Kbely
<g/>
.	.	kIx.	.
</s>
<s>
Výraznými	výrazný	k2eAgFnPc7d1	výrazná
stavbami	stavba	k1gFnPc7	stavba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
jako	jako	k8xS	jako
náprava	náprava	k1gFnSc1	náprava
válečných	válečný	k2eAgFnPc2d1	válečná
škod	škoda	k1gFnPc2	škoda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
věže	věž	k1gFnPc1	věž
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Emauzích	Emauzy	k1gInPc6	Emauzy
nebo	nebo	k8xC	nebo
novější	nový	k2eAgInSc4d2	novější
Tančící	tančící	k2eAgInSc4d1	tančící
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgNnSc4	první
sídliště	sídliště	k1gNnSc4	sídliště
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
napřed	napřed	k6eAd1	napřed
v	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc2	první
dvouletky	dvouletka	k1gFnSc2	dvouletka
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
se	se	k3xPyFc4	se
chopila	chopit	k5eAaPmAgFnS	chopit
moci	moct	k5eAaImF	moct
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
sídlišť	sídliště	k1gNnPc2	sídliště
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
urychlila	urychlit	k5eAaPmAgFnS	urychlit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
zástavba	zástavba	k1gFnSc1	zástavba
byla	být	k5eAaImAgFnS	být
někde	někde	k6eAd1	někde
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
panelovými	panelový	k2eAgInPc7d1	panelový
domy	dům	k1gInPc7	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
a	a	k8xC	a
1974	[number]	k4	1974
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
připojeno	připojit	k5eAaPmNgNnS	připojit
dalších	další	k2eAgMnPc2d1	další
celkem	celkem	k6eAd1	celkem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
vtrhla	vtrhnout	k5eAaPmAgNnP	vtrhnout
vojska	vojsko	k1gNnPc1	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahájila	zahájit	k5eAaPmAgFnS	zahájit
okupaci	okupace	k1gFnSc4	okupace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zmodernizovaly	zmodernizovat	k5eAaPmAgFnP	zmodernizovat
důležité	důležitý	k2eAgFnPc1d1	důležitá
dopravní	dopravní	k2eAgFnPc1d1	dopravní
stavby	stavba	k1gFnPc1	stavba
jako	jako	k8xC	jako
letiště	letiště	k1gNnSc1	letiště
a	a	k8xC	a
hlavní	hlavní	k2eAgNnPc1d1	hlavní
nádraží	nádraží	k1gNnPc1	nádraží
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
budovat	budovat	k5eAaImF	budovat
metro	metro	k1gNnSc4	metro
a	a	k8xC	a
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
ZÁKOS	ZÁKOS	kA	ZÁKOS
–	–	k?	–
systém	systém	k1gInSc1	systém
kapacitních	kapacitní	k2eAgFnPc2d1	kapacitní
městských	městský	k2eAgFnPc2d1	městská
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
oddělila	oddělit	k5eAaPmAgFnS	oddělit
Národní	národní	k2eAgNnSc4d1	národní
muzeum	muzeum	k1gNnSc4	muzeum
od	od	k7c2	od
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
potlačování	potlačování	k1gNnSc3	potlačování
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
pasivnímu	pasivní	k2eAgInSc3d1	pasivní
přístupu	přístup	k1gInSc3	přístup
veřejnosti	veřejnost	k1gFnSc2	veřejnost
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
armádami	armáda	k1gFnPc7	armáda
států	stát	k1gInPc2	stát
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
upálil	upálit	k5eAaPmAgMnS	upálit
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc4	palach
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnSc4	obyvatel
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
zelených	zelený	k2eAgFnPc6d1	zelená
loukách	louka	k1gFnPc6	louka
okolo	okolo	k7c2	okolo
Prahy	Praha	k1gFnSc2	Praha
budována	budován	k2eAgNnPc4d1	budováno
panelová	panelový	k2eAgNnPc4d1	panelové
sídliště	sídliště	k1gNnPc4	sídliště
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnPc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
Jižní	jižní	k2eAgNnSc1d1	jižní
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupující	postupující	k2eAgInSc1d1	postupující
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
úpadek	úpadek	k1gInSc1	úpadek
země	zem	k1gFnSc2	zem
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
i	i	k9	i
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
zanedbaném	zanedbaný	k2eAgInSc6d1	zanedbaný
vzhledu	vzhled	k1gInSc6	vzhled
města	město	k1gNnSc2	město
i	i	k9	i
ve	v	k7c6	v
zhoršování	zhoršování	k1gNnSc6	zhoršování
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ukončila	ukončit	k5eAaPmAgFnS	ukončit
mocenský	mocenský	k2eAgInSc4d1	mocenský
monopol	monopol	k1gInSc4	monopol
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Zavedením	zavedení	k1gNnSc7	zavedení
standardních	standardní	k2eAgInPc2d1	standardní
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
západoevropskými	západoevropský	k2eAgFnPc7d1	západoevropská
zeměmi	zem	k1gFnPc7	zem
se	se	k3xPyFc4	se
Praha	Praha	k1gFnSc1	Praha
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
otevřela	otevřít	k5eAaPmAgFnS	otevřít
turistice	turistika	k1gFnSc3	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
a	a	k8xC	a
podnikání	podnikání	k1gNnSc2	podnikání
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
projevil	projevit	k5eAaPmAgInS	projevit
ve	v	k7c4	v
zkvalitnění	zkvalitnění	k1gNnSc4	zkvalitnění
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
opravě	oprava	k1gFnSc6	oprava
chátrajících	chátrající	k2eAgFnPc2d1	chátrající
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
samém	samý	k3xTgNnSc6	samý
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1	individuální
automobilová	automobilový	k2eAgFnSc1d1	automobilová
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
jako	jako	k8xC	jako
v	v	k7c6	v
mnohých	mnohý	k2eAgNnPc6d1	mnohé
městech	město	k1gNnPc6	město
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
zhoršení	zhoršení	k1gNnSc3	zhoršení
dopravní	dopravní	k2eAgFnSc2d1	dopravní
situace	situace	k1gFnSc2	situace
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
cestujících	cestující	k1gMnPc2	cestující
MHD	MHD	kA	MHD
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
z	z	k7c2	z
1,104	[number]	k4	1,104
miliard	miliarda	k4xCgFnPc2	miliarda
na	na	k7c4	na
1,186	[number]	k4	1,186
miliard	miliarda	k4xCgFnPc2	miliarda
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
461	[number]	k4	461
miliónů	milión	k4xCgInPc2	milión
cestovalo	cestovat	k5eAaImAgNnS	cestovat
metrem	metro	k1gNnSc7	metro
<g/>
.	.	kIx.	.
</s>
<s>
Nejvytíženější	vytížený	k2eAgFnSc7d3	nejvytíženější
stanicí	stanice	k1gFnSc7	stanice
metra	metro	k1gNnSc2	metro
je	být	k5eAaImIp3nS	být
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
<g/>
,	,	kIx,	,
nejvytíženější	vytížený	k2eAgFnSc1d3	nejvytíženější
tramvajovou	tramvajový	k2eAgFnSc7d1	tramvajová
linkou	linka	k1gFnSc7	linka
linka	linka	k1gFnSc1	linka
číslo	číslo	k1gNnSc1	číslo
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
Prahu	Praha	k1gFnSc4	Praha
těžce	těžce	k6eAd1	těžce
poničila	poničit	k5eAaPmAgFnS	poničit
povodeň	povodeň	k1gFnSc1	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Povodeň	povodeň	k1gFnSc1	povodeň
poškodila	poškodit	k5eAaPmAgFnS	poškodit
např.	např.	kA	např.
pražskou	pražský	k2eAgFnSc7d1	Pražská
zoo	zoo	k1gFnSc7	zoo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
poté	poté	k6eAd1	poté
rychle	rychle	k6eAd1	rychle
zotavila	zotavit	k5eAaPmAgFnS	zotavit
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
18	[number]	k4	18
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byly	být	k5eAaImAgFnP	být
uzavřené	uzavřený	k2eAgInPc1d1	uzavřený
až	až	k9	až
na	na	k7c4	na
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
leží	ležet	k5eAaImIp3nS	ležet
mírně	mírně	k6eAd1	mírně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
přítoků	přítok	k1gInPc2	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
erozní	erozní	k2eAgFnSc1d1	erozní
činnost	činnost	k1gFnSc1	činnost
vymodelovala	vymodelovat	k5eAaPmAgFnS	vymodelovat
členitý	členitý	k2eAgInSc4d1	členitý
reliéf	reliéf	k1gInSc4	reliéf
<g/>
,	,	kIx,	,
nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
Vltavy	Vltava	k1gFnSc2	Vltava
u	u	k7c2	u
Suchdola	Suchdola	k1gFnSc1	Suchdola
(	(	kIx(	(
<g/>
177	[number]	k4	177
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
pak	pak	k6eAd1	pak
nevýrazný	výrazný	k2eNgInSc4d1	nevýrazný
vrch	vrch	k1gInSc4	vrch
Teleček	Telečko	k1gNnPc2	Telečko
mezi	mezi	k7c7	mezi
Sobínem	Sobín	k1gMnSc7	Sobín
a	a	k8xC	a
Chrášťany	Chrášťan	k1gMnPc7	Chrášťan
(	(	kIx(	(
<g/>
399	[number]	k4	399
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
vrchol	vrchol	k1gInSc1	vrchol
Petřín	Petřín	k1gInSc1	Petřín
(	(	kIx(	(
<g/>
327	[number]	k4	327
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
s	s	k7c7	s
Petřínskou	petřínský	k2eAgFnSc7d1	Petřínská
rozhlednou	rozhledna	k1gFnSc7	rozhledna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
strmě	strmě	k6eAd1	strmě
se	se	k3xPyFc4	se
zdvihající	zdvihající	k2eAgMnSc1d1	zdvihající
od	od	k7c2	od
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
členění	členění	k1gNnSc2	členění
náleží	náležet	k5eAaImIp3nS	náležet
většina	většina	k1gFnSc1	většina
rozlohy	rozloha	k1gFnSc2	rozloha
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
k	k	k7c3	k
celku	celek	k1gInSc3	celek
Pražská	pražský	k2eAgFnSc1d1	Pražská
plošina	plošina	k1gFnSc1	plošina
a	a	k8xC	a
jen	jen	k9	jen
menší	malý	k2eAgInSc1d2	menší
díl	díl	k1gInSc1	díl
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
Středolabské	středolabský	k2eAgFnSc2d1	Středolabská
tabule	tabule	k1gFnSc2	tabule
(	(	kIx(	(
<g/>
Březiněves	Březiněves	k1gInSc1	Březiněves
<g/>
,	,	kIx,	,
Letňany	Letňan	k1gMnPc4	Letňan
<g/>
,	,	kIx,	,
Čakovice	Čakovice	k1gFnSc1	Čakovice
<g/>
,	,	kIx,	,
Miškovice	Miškovice	k1gFnSc1	Miškovice
<g/>
,	,	kIx,	,
Vinoř	Vinoř	k1gFnSc1	Vinoř
<g/>
,	,	kIx,	,
Prosek	proséct	k5eAaPmDgInS	proséct
<g/>
,	,	kIx,	,
Kbely	Kbely	k1gInPc1	Kbely
<g/>
,	,	kIx,	,
Satalice	Satalice	k1gFnPc1	Satalice
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnPc1d1	horní
Počernice	Počernice	k1gFnPc1	Počernice
a	a	k8xC	a
Klánovice	Klánovice	k1gFnPc1	Klánovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
samý	samý	k3xTgInSc4	samý
jih	jih	k1gInSc4	jih
města	město	k1gNnSc2	město
pronikají	pronikat	k5eAaImIp3nP	pronikat
svými	svůj	k3xOyFgMnPc7	svůj
výběžky	výběžek	k1gInPc1	výběžek
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
celky	celek	k1gInPc4	celek
–	–	k?	–
niva	niva	k1gFnSc1	niva
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
Berounky	Berounka	k1gFnSc2	Berounka
(	(	kIx(	(
<g/>
Lipence	Lipence	k1gFnSc1	Lipence
<g/>
,	,	kIx,	,
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
<g/>
,	,	kIx,	,
Lahovice	Lahovice	k1gFnSc1	Lahovice
<g/>
,	,	kIx,	,
Radotín	Radotín	k1gInSc1	Radotín
<g/>
)	)	kIx)	)
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
Hořovické	hořovický	k2eAgFnSc3d1	Hořovická
pahorkatině	pahorkatina	k1gFnSc3	pahorkatina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Brdská	brdský	k2eAgFnSc1d1	Brdská
vrchovina	vrchovina	k1gFnSc1	vrchovina
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
svým	svůj	k3xOyFgInSc7	svůj
nejzazším	zadní	k2eAgInSc7d3	nejzazší
koncem	konec	k1gInSc7	konec
mezi	mezi	k7c7	mezi
Baněmi	baně	k1gFnPc7	baně
a	a	k8xC	a
Točnou	točna	k1gFnSc7	točna
až	až	k9	až
na	na	k7c4	na
pravý	pravý	k2eAgInSc4d1	pravý
vltavský	vltavský	k2eAgInSc4d1	vltavský
břeh	břeh	k1gInSc4	břeh
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
vrchů	vrch	k1gInPc2	vrch
Čihadlo	čihadlo	k1gNnSc1	čihadlo
<g/>
,	,	kIx,	,
Šance	šance	k1gFnSc1	šance
a	a	k8xC	a
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
,	,	kIx,	,
teplejší	teplý	k2eAgFnSc1d2	teplejší
než	než	k8xS	než
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
°	°	k?	°
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
–	–	k?	–
např.	např.	kA	např.
v	v	k7c6	v
kanadském	kanadský	k2eAgInSc6d1	kanadský
Winnipegu	Winnipeg	k1gInSc6	Winnipeg
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
průměrná	průměrný	k2eAgFnSc1d1	průměrná
denní	denní	k2eAgFnSc1d1	denní
teplota	teplota	k1gFnSc1	teplota
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc1d1	noční
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
také	také	k9	také
občasný	občasný	k2eAgInSc1d1	občasný
silný	silný	k2eAgInSc1d1	silný
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
14	[number]	k4	14
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větry	vítr	k1gInPc1	vítr
ale	ale	k8xC	ale
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
vanou	vanout	k5eAaImIp3nP	vanout
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
roky	rok	k1gInPc4	rok
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Praha-Ruzyně	Praha-Ruzyně	k1gFnSc2	Praha-Ruzyně
byly	být	k5eAaImAgInP	být
526,6	[number]	k4	526,6
mm	mm	kA	mm
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejvíce	hodně	k6eAd3	hodně
napršelo	napršet	k5eAaPmAgNnS	napršet
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
(	(	kIx(	(
<g/>
78	[number]	k4	78
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
(	(	kIx(	(
<g/>
23	[number]	k4	23
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roky	rok	k1gInPc4	rok
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
průměrně	průměrně	k6eAd1	průměrně
ročně	ročně	k6eAd1	ročně
160	[number]	k4	160
dnů	den	k1gInPc2	den
deštivých	deštivý	k2eAgInPc2d1	deštivý
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
zasněžených	zasněžený	k2eAgInPc2d1	zasněžený
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
průměrná	průměrný	k2eAgFnSc1d1	průměrná
výška	výška	k1gFnSc1	výška
sněhu	sníh	k1gInSc2	sníh
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ročně	ročně	k6eAd1	ročně
přes	přes	k7c4	přes
1	[number]	k4	1
600	[number]	k4	600
slunečných	slunečný	k2eAgFnPc2d1	Slunečná
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
slunečných	slunečný	k2eAgFnPc2d1	Slunečná
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
(	(	kIx(	(
<g/>
230	[number]	k4	230
<g/>
,	,	kIx,	,
za	za	k7c4	za
den	den	k1gInSc4	den
8,5	[number]	k4	8,5
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
(	(	kIx(	(
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
za	za	k7c4	za
den	den	k1gInSc4	den
1,5	[number]	k4	1,5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
8,5	[number]	k4	8,5
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
nejchladnějším	chladný	k2eAgInSc6d3	nejchladnější
měsíci	měsíc	k1gInSc6	měsíc
lednu	leden	k1gInSc6	leden
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
denní	denní	k2eAgFnSc1d1	denní
teplota	teplota	k1gFnSc1	teplota
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc1d1	noční
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
nejteplejším	teplý	k2eAgInSc6d3	nejteplejší
měsíci	měsíc	k1gInSc6	měsíc
červenci	červenec	k1gInSc6	červenec
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
denní	denní	k2eAgFnSc1d1	denní
teplota	teplota	k1gFnSc1	teplota
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc1d1	noční
13	[number]	k4	13
°	°	k?	°
<g/>
C.	C.	kA	C.
Ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
100	[number]	k4	100
mrazivých	mrazivý	k2eAgInPc2d1	mrazivý
dnů	den	k1gInPc2	den
a	a	k8xC	a
30	[number]	k4	30
ledových	ledový	k2eAgInPc2d1	ledový
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
celoročně	celoročně	k6eAd1	celoročně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
65	[number]	k4	65
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
a	a	k8xC	a
letní	letní	k2eAgFnSc2d1	letní
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
radnice	radnice	k1gFnSc1	radnice
reaguje	reagovat	k5eAaBmIp3nS	reagovat
Strategii	strategie	k1gFnSc4	strategie
klimatické	klimatický	k2eAgFnSc2d1	klimatická
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
přijmuta	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
větší	veliký	k2eAgFnSc4d2	veliký
podporu	podpora	k1gFnSc4	podpora
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgNnSc1d2	lepší
hospodaření	hospodaření	k1gNnSc1	hospodaření
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
vytváření	vytváření	k1gNnSc4	vytváření
nových	nový	k2eAgFnPc2d1	nová
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
či	či	k8xC	či
zadržování	zadržování	k1gNnSc2	zadržování
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
průměrné	průměrný	k2eAgFnSc2d1	průměrná
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
srážek	srážka	k1gFnPc2	srážka
podle	podle	k7c2	podle
měsíců	měsíc	k1gInPc2	měsíc
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiná	jiný	k2eAgFnSc1d1	jiná
velká	velká	k1gFnSc1	velká
města	město	k1gNnSc2	město
sužována	sužovat	k5eAaImNgFnS	sužovat
zhoršenou	zhoršený	k2eAgFnSc7d1	zhoršená
kvalitou	kvalita	k1gFnSc7	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tvoří	tvořit	k5eAaImIp3nP	tvořit
smog	smog	k1gInSc4	smog
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
podíl	podíl	k1gInSc1	podíl
na	na	k7c4	na
znečištění	znečištění	k1gNnSc4	znečištění
má	mít	k5eAaImIp3nS	mít
prašný	prašný	k2eAgInSc1d1	prašný
spad	spad	k1gInSc1	spad
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
lehký	lehký	k2eAgInSc1d1	lehký
polétavý	polétavý	k2eAgInSc1d1	polétavý
prach	prach	k1gInSc1	prach
s	s	k7c7	s
částicemi	částice	k1gFnPc7	částice
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
10	[number]	k4	10
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
také	také	k9	také
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
koncentrace	koncentrace	k1gFnSc1	koncentrace
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
také	také	k9	také
přízemního	přízemní	k2eAgInSc2d1	přízemní
ozónu	ozón	k1gInSc2	ozón
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
koncentrace	koncentrace	k1gFnSc1	koncentrace
škodlivin	škodlivina	k1gFnPc2	škodlivina
byla	být	k5eAaImAgFnS	být
naměřena	naměřit	k5eAaBmNgFnS	naměřit
ve	v	k7c6	v
čtvrtích	čtvrt	k1gFnPc6	čtvrt
u	u	k7c2	u
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
především	především	k9	především
ve	v	k7c6	v
stopě	stopa	k1gFnSc6	stopa
hlavních	hlavní	k2eAgInPc2d1	hlavní
silničních	silniční	k2eAgInPc2d1	silniční
tahů	tah	k1gInPc2	tah
(	(	kIx(	(
<g/>
Pražský	pražský	k2eAgInSc4d1	pražský
a	a	k8xC	a
Městský	městský	k2eAgInSc4d1	městský
okruh	okruh	k1gInSc4	okruh
+	+	kIx~	+
jejich	jejich	k3xOp3gFnPc1	jejich
radiály	radiála	k1gFnPc1	radiála
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
,	,	kIx,	,
Pankráci	Pankrác	k1gFnSc6	Pankrác
<g/>
,	,	kIx,	,
Roztylech	Roztyl	k1gInPc6	Roztyl
<g/>
,	,	kIx,	,
Uhříněvsi	Uhříněves	k1gFnPc1	Uhříněves
<g/>
,	,	kIx,	,
Vysočanech	Vysočany	k1gInPc6	Vysočany
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
škodlivin	škodlivina	k1gFnPc2	škodlivina
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
kontrolována	kontrolován	k2eAgFnSc1d1	kontrolována
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
například	například	k6eAd1	například
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
tržnice	tržnice	k1gFnSc2	tržnice
v	v	k7c6	v
Libuši	Libuše	k1gFnSc6	Libuše
nebyli	být	k5eNaImAgMnP	být
občané	občan	k1gMnPc1	občan
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
zvýšeném	zvýšený	k2eAgInSc6d1	zvýšený
obsahu	obsah	k1gInSc6	obsah
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
informováni	informovat	k5eAaBmNgMnP	informovat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
ČR	ČR	kA	ČR
výrazně	výrazně	k6eAd1	výrazně
bohatším	bohatý	k2eAgInSc7d2	bohatší
regionem	region	k1gInSc7	region
a	a	k8xC	a
svojí	svůj	k3xOyFgFnSc7	svůj
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
evropský	evropský	k2eAgInSc4d1	evropský
průměr	průměr	k1gInSc4	průměr
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
Eurostatu	Eurostat	k1gInSc2	Eurostat
umístila	umístit	k5eAaPmAgFnS	umístit
jako	jako	k9	jako
9	[number]	k4	9
<g/>
.	.	kIx.	.
nejbohatší	bohatý	k2eAgInSc1d3	nejbohatší
region	region	k1gInSc1	region
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
nejbohatší	bohatý	k2eAgInSc1d3	nejbohatší
region	region	k1gInSc1	region
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
jenom	jenom	k9	jenom
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
patří	patřit	k5eAaImIp3nS	patřit
tradičně	tradičně	k6eAd1	tradičně
k	k	k7c3	k
nejdůležitějším	důležitý	k2eAgInPc3d3	nejdůležitější
hospodářským	hospodářský	k2eAgInPc3d1	hospodářský
centrům	centr	k1gInPc3	centr
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
význačného	význačný	k2eAgInSc2d1	význačný
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
nejvýznačnějšího	význačný	k2eAgNnSc2d3	nejvýznačnější
odvětví	odvětví	k1gNnSc2	odvětví
<g/>
,	,	kIx,	,
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
závodů	závod	k1gInPc2	závod
zpracovatelského	zpracovatelský	k2eAgInSc2d1	zpracovatelský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
kraji	kraj	k1gInSc6	kraj
obnášel	obnášet	k5eAaImAgInS	obnášet
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
kolem	kolem	k7c2	kolem
620	[number]	k4	620
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
25,7	[number]	k4	25,7
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
HDP	HDP	kA	HDP
v	v	k7c6	v
tržních	tržní	k2eAgFnPc6d1	tržní
cenách	cena	k1gFnPc6	cena
<g/>
;	;	kIx,	;
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
to	ten	k3xDgNnSc1	ten
činí	činit	k5eAaImIp3nS	činit
226	[number]	k4	226
%	%	kIx~	%
celorepublikového	celorepublikový	k2eAgInSc2d1	celorepublikový
průměru	průměr	k1gInSc2	průměr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgInSc4d2	nižší
podíl	podíl	k1gInSc4	podíl
obecně	obecně	k6eAd1	obecně
ekonomicky	ekonomicky	k6eAd1	ekonomicky
neaktivních	aktivní	k2eNgMnPc2d1	neaktivní
jedinců	jedinec	k1gMnPc2	jedinec
i	i	k8xC	i
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
55	[number]	k4	55
%	%	kIx~	%
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
%	%	kIx~	%
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
běžné	běžný	k2eAgFnPc4d1	běžná
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnPc4d2	vyšší
mzdy	mzda	k1gFnPc4	mzda
<g/>
,	,	kIx,	,
než	než	k8xS	než
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Medián	medián	k1gInSc1	medián
hrubých	hrubý	k2eAgFnPc2d1	hrubá
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
mezd	mzda	k1gFnPc2	mzda
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
28	[number]	k4	28
386	[number]	k4	386
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
ČR	ČR	kA	ČR
jen	jen	k6eAd1	jen
22	[number]	k4	22
229	[number]	k4	229
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
znatelné	znatelný	k2eAgNnSc1d1	znatelné
je	být	k5eAaImIp3nS	být
také	také	k9	také
zaměření	zaměření	k1gNnSc4	zaměření
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
činnosti	činnost	k1gFnSc2	činnost
terciérního	terciérní	k2eAgInSc2d1	terciérní
sektoru	sektor	k1gInSc2	sektor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
Pražanů	Pražan	k1gMnPc2	Pražan
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
terciérní	terciérní	k2eAgInSc1d1	terciérní
sektor	sektor	k1gInSc1	sektor
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
pouze	pouze	k6eAd1	pouze
58	[number]	k4	58
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
obrácená	obrácený	k2eAgFnSc1d1	obrácená
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgMnSc6d1	průmyslový
(	(	kIx(	(
<g/>
sekundárním	sekundární	k2eAgInSc6d1	sekundární
<g/>
)	)	kIx)	)
a	a	k8xC	a
zemědělsko-těžebním	zemědělskoěžební	k2eAgMnSc6d1	zemědělsko-těžební
(	(	kIx(	(
<g/>
primárním	primární	k2eAgInSc6d1	primární
<g/>
)	)	kIx)	)
sektoru	sektor	k1gInSc6	sektor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zaměstnávají	zaměstnávat	k5eAaImIp3nP	zaměstnávat
20	[number]	k4	20
%	%	kIx~	%
respektive	respektive	k9	respektive
0	[number]	k4	0
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
ČR	ČR	kA	ČR
39	[number]	k4	39
%	%	kIx~	%
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Specifický	specifický	k2eAgMnSc1d1	specifický
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
je	být	k5eAaImIp3nS	být
také	také	k9	také
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
podíl	podíl	k1gInSc1	podíl
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nebydlí	bydlet	k5eNaImIp3nP	bydlet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
cestují	cestovat	k5eAaImIp3nP	cestovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
také	také	k9	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
asi	asi	k9	asi
81	[number]	k4	81
%	%	kIx~	%
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
19	[number]	k4	19
%	%	kIx~	%
jedinců	jedinec	k1gMnPc2	jedinec
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
za	za	k7c7	za
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
aktivitami	aktivita	k1gFnPc7	aktivita
cestuje	cestovat	k5eAaImIp3nS	cestovat
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
13	[number]	k4	13
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
pracujících	pracující	k1gMnPc2	pracující
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obráceně	obráceně	k6eAd1	obráceně
ale	ale	k8xC	ale
96	[number]	k4	96
%	%	kIx~	%
Pražanů	Pražan	k1gMnPc2	Pražan
má	mít	k5eAaImIp3nS	mít
svoje	svůj	k3xOyFgFnPc4	svůj
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
aktivity	aktivita	k1gFnPc4	aktivita
soustředěné	soustředěný	k2eAgFnPc4d1	soustředěná
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
%	%	kIx~	%
Pražanů	Pražan	k1gMnPc2	Pražan
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
cestuje	cestovat	k5eAaImIp3nS	cestovat
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zpracovatelský	zpracovatelský	k2eAgInSc1d1	zpracovatelský
průmysl	průmysl	k1gInSc1	průmysl
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
s	s	k7c7	s
7,6	[number]	k4	7,6
procenty	procent	k1gInPc7	procent
celkové	celkový	k2eAgFnSc2d1	celková
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
páté	pátá	k1gFnSc2	pátá
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
regionální	regionální	k2eAgFnSc6d1	regionální
struktuře	struktura	k1gFnSc6	struktura
všech	všecek	k3xTgMnPc2	všecek
čtrnácti	čtrnáct	k4xCc2	čtrnáct
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
závody	závod	k1gInPc1	závod
se	se	k3xPyFc4	se
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
zejména	zejména	k9	zejména
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
registrováno	registrovat	k5eAaBmNgNnS	registrovat
733	[number]	k4	733
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
závodů	závod	k1gInPc2	závod
(	(	kIx(	(
<g/>
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvaceti	dvacet	k4xCc7	dvacet
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
)	)	kIx)	)
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
111	[number]	k4	111
tisíci	tisíc	k4xCgInSc3	tisíc
pracujících	pracující	k1gFnPc2	pracující
<g/>
.	.	kIx.	.
</s>
<s>
Objemem	objem	k1gInSc7	objem
své	svůj	k3xOyFgFnSc2	svůj
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jednoznačně	jednoznačně	k6eAd1	jednoznačně
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
dvě	dva	k4xCgNnPc1	dva
odvětví	odvětví	k1gNnPc1	odvětví
<g/>
:	:	kIx,	:
produkce	produkce	k1gFnSc1	produkce
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
produkce	produkce	k1gFnSc2	produkce
elektrických	elektrický	k2eAgInPc2d1	elektrický
a	a	k8xC	a
optických	optický	k2eAgInPc2d1	optický
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
obě	dva	k4xCgNnPc1	dva
odvětví	odvětví	k1gNnPc2	odvětví
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
33	[number]	k4	33
miliardami	miliarda	k4xCgFnPc7	miliarda
Kč	Kč	kA	Kč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
nebo	nebo	k8xC	nebo
podílem	podíl	k1gInSc7	podíl
12	[number]	k4	12
%	%	kIx~	%
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
odvětví	odvětví	k1gNnSc2	odvětví
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
tržby	tržba	k1gFnPc1	tržba
za	za	k7c4	za
vlastní	vlastní	k2eAgInPc4d1	vlastní
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
cenách	cena	k1gFnPc6	cena
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
i	i	k9	i
diagramy	diagram	k1gInPc1	diagram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Význačnou	význačný	k2eAgFnSc4d1	význačná
roli	role	k1gFnSc4	role
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
především	především	k9	především
výroba	výroba	k1gFnSc1	výroba
rozhlasových	rozhlasový	k2eAgMnPc2d1	rozhlasový
a	a	k8xC	a
televizních	televizní	k2eAgMnPc2d1	televizní
přijímačů	přijímač	k1gMnPc2	přijímač
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
následována	následovat	k5eAaImNgFnS	následovat
polygrafickým	polygrafický	k2eAgInSc7d1	polygrafický
průmyslem	průmysl	k1gInSc7	průmysl
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
24,5	[number]	k4	24,5
miliardami	miliarda	k4xCgFnPc7	miliarda
Kč	Kč	kA	Kč
tržeb	tržba	k1gFnPc2	tržba
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
tohoto	tento	k3xDgNnSc2	tento
odvětví	odvětví	k1gNnSc2	odvětví
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
odvětví	odvětví	k1gNnSc1	odvětví
silně	silně	k6eAd1	silně
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
(	(	kIx(	(
<g/>
44	[number]	k4	44
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
produkce	produkce	k1gFnSc2	produkce
<g/>
)	)	kIx)	)
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
zde	zde	k6eAd1	zde
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
další	další	k2eAgInPc1d1	další
význačné	význačný	k2eAgInPc1d1	význačný
závody	závod	k1gInPc1	závod
tradičních	tradiční	k2eAgInPc2d1	tradiční
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
sektorů	sektor	k1gInPc2	sektor
a	a	k8xC	a
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
významu	význam	k1gInSc3	význam
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
střediska	středisko	k1gNnSc2	středisko
<g/>
:	:	kIx,	:
kovodělný	kovodělný	k2eAgInSc1d1	kovodělný
průmysl	průmysl	k1gInSc1	průmysl
výroba	výroba	k1gFnSc1	výroba
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnSc2	zařízení
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
farmaceutický	farmaceutický	k2eAgInSc1d1	farmaceutický
průmysl	průmysl	k1gInSc1	průmysl
průmysl	průmysl	k1gInSc1	průmysl
výrobků	výrobek	k1gInPc2	výrobek
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgFnSc2d1	stavební
hmoty	hmota	k1gFnSc2	hmota
výroba	výroba	k1gFnSc1	výroba
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
Další	další	k2eAgNnPc4d1	další
odvětví	odvětví	k1gNnPc4	odvětví
hrají	hrát	k5eAaImIp3nP	hrát
spíše	spíše	k9	spíše
méně	málo	k6eAd2	málo
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
měla	mít	k5eAaImAgFnS	mít
ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
celkem	celkem	k6eAd1	celkem
1	[number]	k4	1
280	[number]	k4	280
508	[number]	k4	508
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
evidováno	evidován	k2eAgNnSc1d1	evidováno
k	k	k7c3	k
trvalému	trvalý	k2eAgInSc3d1	trvalý
pobytu	pobyt	k1gInSc3	pobyt
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
301	[number]	k4	301
489	[number]	k4	489
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
613738	[number]	k4	613738
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
655058	[number]	k4	655058
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
činil	činit	k5eAaImAgInS	činit
41,2	[number]	k4	41,2
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
mužské	mužský	k2eAgFnSc2d1	mužská
části	část	k1gFnSc2	část
pak	pak	k6eAd1	pak
39,7	[number]	k4	39,7
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
ženské	ženský	k2eAgFnSc2d1	ženská
části	část	k1gFnSc2	část
42,7	[number]	k4	42,7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
dojíždělo	dojíždět	k5eAaImAgNnS	dojíždět
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
studiem	studio	k1gNnSc7	studio
199	[number]	k4	199
360	[number]	k4	360
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
128	[number]	k4	128
675	[number]	k4	675
(	(	kIx(	(
<g/>
65	[number]	k4	65
%	%	kIx~	%
<g/>
)	)	kIx)	)
ze	z	k7c2	z
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
vyjíždělo	vyjíždět	k5eAaImAgNnS	vyjíždět
23	[number]	k4	23
332	[number]	k4	332
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
16	[number]	k4	16
989	[number]	k4	989
(	(	kIx(	(
<g/>
73	[number]	k4	73
%	%	kIx~	%
<g/>
)	)	kIx)	)
do	do	k7c2	do
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
romských	romský	k2eAgFnPc2d1	romská
komunit	komunita	k1gFnPc2	komunita
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc1	podíl
Romů	Rom	k1gMnPc2	Rom
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
procento	procento	k1gNnSc1	procento
uvedené	uvedený	k2eAgFnSc2d1	uvedená
romské	romský	k2eAgFnSc2d1	romská
národnosti	národnost	k1gFnSc2	národnost
poloviční	poloviční	k2eAgMnSc1d1	poloviční
proti	proti	k7c3	proti
celorepublikovému	celorepublikový	k2eAgInSc3d1	celorepublikový
průměru	průměr	k1gInSc3	průměr
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnSc1d2	nižší
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
pro	pro	k7c4	pro
MPSV	MPSV	kA	MPSV
a	a	k8xC	a
ESF	ESF	kA	ESF
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
šest	šest	k4xCc1	šest
sociálně	sociálně	k6eAd1	sociálně
vyloučených	vyloučený	k2eAgFnPc2d1	vyloučená
romských	romský	k2eAgFnPc2d1	romská
lokalit	lokalita	k1gFnPc2	lokalita
s	s	k7c7	s
odhadovaným	odhadovaný	k2eAgInSc7d1	odhadovaný
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
přes	přes	k7c4	přes
9	[number]	k4	9
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtvrtích	čtvrt	k1gFnPc6	čtvrt
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
,	,	kIx,	,
Libeň	Libeň	k1gFnSc1	Libeň
<g/>
,	,	kIx,	,
Karlín	Karlín	k1gInSc1	Karlín
<g/>
,	,	kIx,	,
Černý	černý	k2eAgInSc1d1	černý
Most	most	k1gInSc1	most
a	a	k8xC	a
Harfa	harfa	k1gFnSc1	harfa
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
sčítání	sčítání	k1gNnSc2	sčítání
pražského	pražský	k2eAgInSc2d1	pražský
magistrátu	magistrát	k1gInSc2	magistrát
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pohybovalo	pohybovat	k5eAaImAgNnS	pohybovat
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
000	[number]	k4	000
bezdomovců	bezdomovec	k1gMnPc2	bezdomovec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
míru	míra	k1gFnSc4	míra
počtu	počet	k1gInSc2	počet
přistěhovalých	přistěhovalý	k2eAgFnPc2d1	přistěhovalá
i	i	k8xC	i
počtu	počet	k1gInSc3	počet
vystěhovalých	vystěhovalý	k2eAgFnPc2d1	vystěhovalý
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
přistěhovalými	přistěhovalý	k2eAgMnPc7d1	přistěhovalý
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Pražané	Pražan	k1gMnPc1	Pražan
se	se	k3xPyFc4	se
vystěhovávají	vystěhovávat	k5eAaImIp3nP	vystěhovávat
nejvíce	hodně	k6eAd3	hodně
do	do	k7c2	do
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
okresů	okres	k1gInPc2	okres
Praha-východ	Prahaýchod	k1gInSc4	Praha-východ
a	a	k8xC	a
Praha-západ	Prahaápad	k1gInSc4	Praha-západ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzniká	vznikat	k5eAaImIp3nS	vznikat
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejrychleji	rychle	k6eAd3	rychle
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
žilo	žít	k5eAaImAgNnS	žít
velice	velice	k6eAd1	velice
heterogenní	heterogenní	k2eAgNnSc4d1	heterogenní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
–	–	k?	–
vedle	vedle	k7c2	vedle
Čechů	Čech	k1gMnPc2	Čech
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
;	;	kIx,	;
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
také	také	k9	také
Italové	Ital	k1gMnPc1	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Architektonické	architektonický	k2eAgFnPc1d1	architektonická
památky	památka	k1gFnPc1	památka
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
tvoří	tvořit	k5eAaImIp3nP	tvořit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Malé	Malé	k2eAgFnSc2d1	Malé
Strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
židovského	židovský	k2eAgMnSc2d1	židovský
několik	několik	k4yIc4	několik
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
po	po	k7c6	po
asanaci	asanace	k1gFnSc6	asanace
Josefova	Josefov	k1gInSc2	Josefov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
synagogy	synagoga	k1gFnPc4	synagoga
mimo	mimo	k7c4	mimo
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
Jubilejní	jubilejní	k2eAgFnSc1d1	jubilejní
synagoga	synagoga	k1gFnSc1	synagoga
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
synagoga	synagoga	k1gFnSc1	synagoga
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
a	a	k8xC	a
na	na	k7c6	na
Palmovce	Palmovka	k1gFnSc6	Palmovka
aj.	aj.	kA	aj.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
byla	být	k5eAaImAgFnS	být
obcovacím	obcovací	k2eAgInSc7d1	obcovací
jazykem	jazyk	k1gInSc7	jazyk
86,2	[number]	k4	86,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Prahy	Praha	k1gFnSc2	Praha
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
13,7	[number]	k4	13,7
%	%	kIx~	%
pak	pak	k6eAd1	pak
němčina	němčina	k1gFnSc1	němčina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
k	k	k7c3	k
česko-slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
národnosti	národnost	k1gFnSc3	národnost
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
94,2	[number]	k4	94,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
4,6	[number]	k4	4,6
%	%	kIx~	%
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
0,9	[number]	k4	0,9
%	%	kIx~	%
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
opět	opět	k6eAd1	opět
stoupá	stoupat	k5eAaImIp3nS	stoupat
počet	počet	k1gInSc1	počet
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
61	[number]	k4	61
477	[number]	k4	477
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
162	[number]	k4	162
715	[number]	k4	715
<g/>
,	,	kIx,	,
když	když	k8xS	když
tvořili	tvořit	k5eAaImAgMnP	tvořit
13	[number]	k4	13
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
připadá	připadat	k5eAaPmIp3nS	připadat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
cizinců	cizinec	k1gMnPc2	cizinec
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
žilo	žít	k5eAaImAgNnS	žít
okolo	okolo	k7c2	okolo
170	[number]	k4	170
tisíc	tisíc	k4xCgInPc2	tisíc
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
45	[number]	k4	45
333	[number]	k4	333
občanů	občan	k1gMnPc2	občan
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
26	[number]	k4	26
281	[number]	k4	281
občanů	občan	k1gMnPc2	občan
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
21	[number]	k4	21
098	[number]	k4	098
občanů	občan	k1gMnPc2	občan
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
11	[number]	k4	11
277	[number]	k4	277
občanů	občan	k1gMnPc2	občan
Vietnamu	Vietnam	k1gInSc2	Vietnam
a	a	k8xC	a
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgFnSc7d1	kulturní
metropolí	metropol	k1gFnSc7	metropol
celé	celý	k2eAgFnSc2d1	celá
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgNnSc1d1	Evropské
město	město	k1gNnSc1	město
kultury	kultura	k1gFnSc2	kultura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nP	působit
zde	zde	k6eAd1	zde
desítky	desítka	k1gFnPc1	desítka
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
galerií	galerie	k1gFnPc2	galerie
<g/>
,	,	kIx,	,
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
kin	kino	k1gNnPc2	kino
a	a	k8xC	a
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
kulturních	kulturní	k2eAgFnPc2d1	kulturní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
spravuje	spravovat	k5eAaImIp3nS	spravovat
největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stálé	stálý	k2eAgFnSc6d1	stálá
expozici	expozice	k1gFnSc6	expozice
Veletržního	veletržní	k2eAgInSc2d1	veletržní
paláce	palác	k1gInSc2	palác
jsou	být	k5eAaImIp3nP	být
díla	dílo	k1gNnPc1	dílo
světových	světový	k2eAgMnPc2d1	světový
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xS	jako
např.	např.	kA	např.
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
Monet	moneta	k1gFnPc2	moneta
nebo	nebo	k8xC	nebo
Van	vana	k1gFnPc2	vana
Gogh	Gogha	k1gFnPc2	Gogha
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
magistrát	magistrát	k1gInSc1	magistrát
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
na	na	k7c4	na
kulturu	kultura	k1gFnSc4	kultura
ročně	ročně	k6eAd1	ročně
stamilionové	stamilionový	k2eAgFnSc2d1	stamilionová
částky	částka	k1gFnSc2	částka
<g/>
;	;	kIx,	;
způsob	způsob	k1gInSc1	způsob
přerozdělování	přerozdělování	k1gNnSc2	přerozdělování
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
však	však	k9	však
v	v	k7c6	v
letech	let	k1gInPc6	let
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
ohrozil	ohrozit	k5eAaPmAgInS	ohrozit
především	především	k9	především
některá	některý	k3yIgNnPc1	některý
menší	malý	k2eAgNnPc1d2	menší
divadla	divadlo	k1gNnPc1	divadlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
předmětem	předmět	k1gInSc7	předmět
ostrých	ostrý	k2eAgInPc2d1	ostrý
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
projekt	projekt	k1gInSc1	projekt
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
město	město	k1gNnSc1	město
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
podpořit	podpořit	k5eAaPmF	podpořit
vnímání	vnímání	k1gNnSc4	vnímání
Prahy	Praha	k1gFnSc2	Praha
jako	jako	k8xC	jako
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
četbu	četba	k1gFnSc4	četba
a	a	k8xC	a
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
cílů	cíl	k1gInPc2	cíl
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
získání	získání	k1gNnSc1	získání
titulu	titul	k1gInSc2	titul
UNESCO	UNESCO	kA	UNESCO
kreativní	kreativní	k2eAgNnSc1d1	kreativní
město	město	k1gNnSc1	město
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
např.	např.	kA	např.
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
rozmístěno	rozmístit	k5eAaPmNgNnS	rozmístit
několik	několik	k4yIc1	několik
reproduktorů	reproduktor	k1gInPc2	reproduktor
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
ptačích	ptačí	k2eAgFnPc2d1	ptačí
budek	budka	k1gFnPc2	budka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
byly	být	k5eAaImAgFnP	být
pouštěny	pouštět	k5eAaImNgInP	pouštět
audioknihy	audioknih	k1gInPc1	audioknih
českých	český	k2eAgMnPc2d1	český
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
zámků	zámek	k1gInPc2	zámek
a	a	k8xC	a
tvrzí	tvrz	k1gFnPc2	tvrz
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc4	seznam
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Seznam	seznam	k1gInSc4	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
díky	dík	k1gInPc1	dík
je	být	k5eAaImIp3nS	být
své	svůj	k3xOyFgFnSc6	svůj
bohaté	bohatý	k2eAgFnSc6d1	bohatá
historii	historie	k1gFnSc6	historie
významným	významný	k2eAgInSc7d1	významný
světovým	světový	k2eAgInSc7d1	světový
kulturním	kulturní	k2eAgInSc7d1	kulturní
centrem	centr	k1gInSc7	centr
a	a	k8xC	a
po	po	k7c6	po
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
Madridu	Madrid	k1gInSc6	Madrid
a	a	k8xC	a
Berlínu	Berlín	k1gInSc3	Berlín
je	být	k5eAaImIp3nS	být
šestým	šestý	k4xOgNnSc7	šestý
nejnavštěvovanějším	navštěvovaný	k2eAgNnSc7d3	nejnavštěvovanější
městem	město	k1gNnSc7	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Neobyčejné	obyčejný	k2eNgNnSc1d1	neobyčejné
množství	množství	k1gNnSc1	množství
a	a	k8xC	a
koncentrace	koncentrace	k1gFnSc1	koncentrace
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
architektonických	architektonický	k2eAgFnPc2d1	architektonická
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zapříčiněno	zapříčiněn	k2eAgNnSc4d1	zapříčiněno
relativně	relativně	k6eAd1	relativně
minimálním	minimální	k2eAgNnSc7d1	minimální
poškozením	poškození	k1gNnSc7	poškození
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
ostatních	ostatní	k2eAgNnPc2d1	ostatní
evropských	evropský	k2eAgNnPc2d1	Evropské
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
památková	památkový	k2eAgFnSc1d1	památková
rezervace	rezervace	k1gFnSc1	rezervace
(	(	kIx(	(
<g/>
PPR	PPR	kA	PPR
<g/>
)	)	kIx)	)
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
městské	městský	k2eAgFnSc3d1	městská
části	část	k1gFnSc3	část
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
koncentrací	koncentrace	k1gFnSc7	koncentrace
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
8,66	[number]	k4	8,66
km2	km2	k4	km2
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
rezervace	rezervace	k1gFnPc4	rezervace
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Prahy	Praha	k1gFnSc2	Praha
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
37	[number]	k4	37
národních	národní	k2eAgFnPc2d1	národní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
28	[number]	k4	28
na	na	k7c6	na
území	území	k1gNnSc6	území
PPR	PPR	kA	PPR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
seznam	seznam	k1gInSc1	seznam
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
objektů	objekt	k1gInPc2	objekt
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Seznam	seznam	k1gInSc4	seznam
pražských	pražský	k2eAgFnPc2d1	Pražská
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Kurzivou	kurziva	k1gFnSc7	kurziva
zvýrazněné	zvýrazněný	k2eAgInPc1d1	zvýrazněný
objekty	objekt	k1gInPc1	objekt
jsou	být	k5eAaImIp3nP	být
národními	národní	k2eAgFnPc7d1	národní
kulturními	kulturní	k2eAgFnPc7d1	kulturní
památkami	památka	k1gFnPc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Letohrádek	letohrádek	k1gInSc1	letohrádek
Hvězda	Hvězda	k1gMnSc1	Hvězda
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášter	klášter	k1gInSc1	klášter
Trojský	trojský	k2eAgInSc4d1	trojský
zámek	zámek	k1gInSc4	zámek
Veletržní	veletržní	k2eAgInSc1d1	veletržní
palác	palác	k1gInSc1	palác
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
koncentrací	koncentrace	k1gFnSc7	koncentrace
malých	malý	k2eAgFnPc2d1	malá
i	i	k8xC	i
velkých	velký	k2eAgFnPc2d1	velká
divadelních	divadelní	k2eAgFnPc2d1	divadelní
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvýznamnějším	významný	k2eAgNnSc6d3	nejvýznamnější
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
výzdoba	výzdoba	k1gFnSc1	výzdoba
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
českého	český	k2eAgNnSc2d1	české
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
stálé	stálý	k2eAgInPc4d1	stálý
soubory	soubor	k1gInPc4	soubor
činohry	činohra	k1gFnSc2	činohra
<g/>
,	,	kIx,	,
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
<g/>
;	;	kIx,	;
ty	ten	k3xDgInPc1	ten
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
střídavě	střídavě	k6eAd1	střídavě
také	také	k9	také
v	v	k7c6	v
klasicistním	klasicistní	k2eAgNnSc6d1	klasicistní
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
premiéry	premiéra	k1gFnPc1	premiéra
Mozartových	Mozartových	k2eAgFnPc2d1	Mozartových
oper	opera	k1gFnPc2	opera
Don	dona	k1gFnPc2	dona
Giovanni	Giovann	k1gMnPc1	Giovann
a	a	k8xC	a
La	la	k1gNnSc1	la
clemenza	clemenz	k1gMnSc2	clemenz
di	di	k?	di
Tito	tento	k3xDgMnPc1	tento
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
činoherními	činoherní	k2eAgFnPc7d1	činoherní
scénami	scéna	k1gFnPc7	scéna
dále	daleko	k6eAd2	daleko
vyniká	vynikat	k5eAaImIp3nS	vynikat
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Prestižní	prestižní	k2eAgFnSc7d1	prestižní
hudební	hudební	k2eAgFnSc7d1	hudební
scénou	scéna	k1gFnSc7	scéna
je	být	k5eAaImIp3nS	být
pražská	pražský	k2eAgFnSc1d1	Pražská
Státní	státní	k2eAgFnSc1d1	státní
opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
pražská	pražský	k2eAgFnSc1d1	Pražská
německá	německý	k2eAgFnSc1d1	německá
scéna	scéna	k1gFnSc1	scéna
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
síň	síň	k1gFnSc1	síň
Rudolfina	Rudolfinum	k1gNnSc2	Rudolfinum
<g/>
,	,	kIx,	,
sídla	sídlo	k1gNnSc2	sídlo
České	český	k2eAgFnSc2d1	Česká
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
.	.	kIx.	.
</s>
<s>
Světově	světově	k6eAd1	světově
známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
také	také	k9	také
divadlo	divadlo	k1gNnSc1	divadlo
Laterna	laterna	k1gFnSc1	laterna
magika	magika	k1gFnSc1	magika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
menší	malý	k2eAgFnPc4d2	menší
scény	scéna	k1gFnPc4	scéna
patří	patřit	k5eAaImIp3nS	patřit
mj.	mj.	kA	mj.
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Dlouhé	Dlouhá	k1gFnSc6	Dlouhá
<g/>
,	,	kIx,	,
Dejvické	dejvický	k2eAgNnSc4d1	Dejvické
divadlo	divadlo	k1gNnSc4	divadlo
či	či	k8xC	či
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
legendární	legendární	k2eAgInSc4d1	legendární
Semafor	Semafor	k1gInSc4	Semafor
a	a	k8xC	a
divadlo	divadlo	k1gNnSc4	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
má	mít	k5eAaImIp3nS	mít
Praha	Praha	k1gFnSc1	Praha
množství	množství	k1gNnSc1	množství
muzikálových	muzikálový	k2eAgNnPc2d1	muzikálové
a	a	k8xC	a
zábavných	zábavný	k2eAgNnPc2d1	zábavné
divadel	divadlo	k1gNnPc2	divadlo
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
Hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
Karlín	Karlín	k1gInSc1	Karlín
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
komerčních	komerční	k2eAgFnPc2d1	komerční
scén	scéna	k1gFnPc2	scéna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
např.	např.	kA	např.
na	na	k7c4	na
černé	černý	k2eAgNnSc4d1	černé
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
z	z	k7c2	z
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
sídlí	sídlet	k5eAaImIp3nP	sídlet
symfonická	symfonický	k2eAgNnPc1d1	symfonické
hudební	hudební	k2eAgNnPc1d1	hudební
tělesa	těleso	k1gNnPc1	těleso
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
,	,	kIx,	,
Symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
FOK	FOK	kA	FOK
<g/>
,	,	kIx,	,
či	či	k8xC	či
Symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
(	(	kIx(	(
<g/>
SOČR	SOČR	kA	SOČR
<g/>
)	)	kIx)	)
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jsou	být	k5eAaImIp3nP	být
stovky	stovka	k1gFnPc1	stovka
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
barů	bar	k1gInPc2	bar
a	a	k8xC	a
především	především	k9	především
hospod	hospod	k?	hospod
s	s	k7c7	s
dobrým	dobrý	k2eAgNnSc7d1	dobré
českým	český	k2eAgNnSc7d1	české
pivem	pivo	k1gNnSc7	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Žižkov	Žižkov	k1gInSc1	Žižkov
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
i	i	k9	i
Nusle	Nusle	k1gFnPc1	Nusle
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgFnPc1d1	plná
dobrých	dobrý	k2eAgInPc6d1	dobrý
hospod	hospod	k?	hospod
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mezi	mezi	k7c7	mezi
nejznámější	známý	k2eAgMnSc1d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
U	u	k7c2	u
Pinkasů	Pinkas	k1gMnPc2	Pinkas
<g/>
,	,	kIx,	,
U	u	k7c2	u
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
Tygra	tygr	k1gMnSc2	tygr
<g/>
,	,	kIx,	,
U	u	k7c2	u
černého	černé	k1gNnSc2	černé
vola	vůl	k1gMnSc2	vůl
<g/>
,	,	kIx,	,
Klášterní	klášterní	k2eAgInSc1d1	klášterní
pivovar	pivovar	k1gInSc1	pivovar
Strahov	Strahov	k1gInSc1	Strahov
<g/>
,	,	kIx,	,
U	u	k7c2	u
Sadu	sad	k1gInSc2	sad
<g/>
,	,	kIx,	,
U	u	k7c2	u
Medvídků	medvídek	k1gMnPc2	medvídek
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1	slavná
je	být	k5eAaImIp3nS	být
také	také	k9	také
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
vaří	vařit	k5eAaImIp3nS	vařit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
pivo	pivo	k1gNnSc4	pivo
–	–	k?	–
U	u	k7c2	u
Fleků	flek	k1gInPc2	flek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
více	hodně	k6eAd2	hodně
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
restaurací	restaurace	k1gFnPc2	restaurace
vedených	vedený	k2eAgFnPc2d1	vedená
zkušenými	zkušený	k2eAgMnPc7d1	zkušený
šéfkuchaři	šéfkuchař	k1gMnPc7	šéfkuchař
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
gastronomie	gastronomie	k1gFnSc2	gastronomie
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
pražské	pražský	k2eAgFnPc1d1	Pražská
restaurace	restaurace	k1gFnPc1	restaurace
se	se	k3xPyFc4	se
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
mohou	moct	k5eAaImIp3nP	moct
pyšnit	pyšnit	k5eAaImF	pyšnit
hvězdičkou	hvězdička	k1gFnSc7	hvězdička
Michelin	Michelina	k1gFnPc2	Michelina
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pořádán	pořádán	k2eAgInSc1d1	pořádán
festival	festival	k1gInSc1	festival
jídla	jídlo	k1gNnSc2	jídlo
–	–	k?	–
Prague	Prague	k1gInSc1	Prague
Food	Food	k1gMnSc1	Food
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
festival	festival	k1gInSc1	festival
piva	pivo	k1gNnSc2	pivo
–	–	k?	–
Český	český	k2eAgInSc1d1	český
pivní	pivní	k2eAgInSc1d1	pivní
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
či	či	k8xC	či
festival	festival	k1gInSc1	festival
zmrzlin	zmrzlina	k1gFnPc2	zmrzlina
–	–	k?	–
Prague	Pragu	k1gInPc1	Pragu
Ice	Ice	k1gFnSc1	Ice
Cream	Cream	k1gInSc1	Cream
Festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
také	také	k9	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
nebo	nebo	k8xC	nebo
sídlilo	sídlit	k5eAaImAgNnS	sídlit
několik	několik	k4yIc1	několik
pivovarů	pivovar	k1gInPc2	pivovar
<g/>
:	:	kIx,	:
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášterní	klášterní	k2eAgInSc1d1	klášterní
pivovar	pivovar	k1gInSc1	pivovar
První	první	k4xOgMnSc1	první
novoměstský	novoměstský	k2eAgInSc1d1	novoměstský
restaurační	restaurační	k2eAgInSc1d1	restaurační
pivovar	pivovar	k1gInSc1	pivovar
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
Pivovar	pivovar	k1gInSc1	pivovar
U	u	k7c2	u
Fleků	flek	k1gInPc2	flek
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
Klášterní	klášterní	k2eAgInSc1d1	klášterní
pivovar	pivovar	k1gInSc1	pivovar
Strahov	Strahov	k1gInSc1	Strahov
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
Pivovar	pivovar	k1gInSc1	pivovar
Pražský	pražský	k2eAgInSc4d1	pražský
most	most	k1gInSc4	most
u	u	k7c2	u
Valšů	Valš	k1gInPc2	Valš
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
Pivovarský	pivovarský	k2eAgInSc1d1	pivovarský
Hotel	hotel	k1gInSc1	hotel
U	u	k7c2	u
Medvídků	medvídek	k1gMnPc2	medvídek
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
Pivovarský	pivovarský	k2eAgInSc1d1	pivovarský
dům	dům	k1gInSc1	dům
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2	[number]	k4	2
Pivovar	pivovar	k1gInSc1	pivovar
Braník	Braník	k1gInSc4	Braník
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
Jihoměstský	Jihoměstský	k2eAgInSc1d1	Jihoměstský
pivovar	pivovar	k1gInSc1	pivovar
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
Sousedský	sousedský	k2eAgInSc1d1	sousedský
pivovar	pivovar	k1gInSc1	pivovar
U	u	k7c2	u
Bansethů	Banseth	k1gInPc2	Banseth
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
Pivovary	pivovar	k1gInPc1	pivovar
Staropramen	staropramen	k1gInSc1	staropramen
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
Měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
7	[number]	k4	7
Výukový	výukový	k2eAgMnSc1d1	výukový
a	a	k8xC	a
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
pivovar	pivovar	k1gInSc1	pivovar
–	–	k?	–
Suchdolský	Suchdolský	k2eAgMnSc1d1	Suchdolský
Jeník	Jeník	k1gMnSc1	Jeník
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
Pivovar	pivovar	k1gInSc1	pivovar
U	u	k7c2	u
Bulovky	Bulovka	k1gFnSc2	Bulovka
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
8	[number]	k4	8
Fotografové	fotograf	k1gMnPc1	fotograf
zachycovali	zachycovat	k5eAaImAgMnP	zachycovat
pohledy	pohled	k1gInPc4	pohled
na	na	k7c4	na
architektonicky	architektonicky	k6eAd1	architektonicky
proslavené	proslavený	k2eAgFnPc4d1	proslavená
lokality	lokalita	k1gFnPc4	lokalita
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc4d1	Staré
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
výjevy	výjev	k1gInPc1	výjev
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
působili	působit	k5eAaImAgMnP	působit
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xS	jako
například	například	k6eAd1	například
Rudolf	Rudolf	k1gMnSc1	Rudolf
Bruner-Dvořák	Bruner-Dvořák	k1gMnSc1	Bruner-Dvořák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Srp	srp	k1gInSc1	srp
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Petrák	Petrák	k1gMnSc1	Petrák
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Fiedler	Fiedler	k1gMnSc1	Fiedler
nebo	nebo	k8xC	nebo
František	František	k1gMnSc1	František
Krátký	Krátký	k1gMnSc1	Krátký
<g/>
.	.	kIx.	.
</s>
<s>
Fotografem	fotograf	k1gMnSc7	fotograf
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
věhlasem	věhlas	k1gInSc7	věhlas
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Fridrich	Fridrich	k1gMnSc1	Fridrich
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
září	září	k1gNnSc6	září
1856	[number]	k4	1856
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
první	první	k4xOgFnSc4	první
autorskou	autorský	k2eAgFnSc4d1	autorská
fotografickou	fotografický	k2eAgFnSc4d1	fotografická
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
české	český	k2eAgFnSc2d1	Česká
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
největším	veliký	k2eAgMnSc7d3	veliký
vydavatelem	vydavatel	k1gMnSc7	vydavatel
fotografických	fotografický	k2eAgFnPc2d1	fotografická
pohlednic	pohlednice	k1gFnPc2	pohlednice
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc4d1	vysoká
dokumentární	dokumentární	k2eAgFnSc4d1	dokumentární
hodnotu	hodnota	k1gFnSc4	hodnota
mají	mít	k5eAaImIp3nP	mít
jeho	jeho	k3xOp3gInPc1	jeho
pohledy	pohled	k1gInPc1	pohled
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
pražskou	pražský	k2eAgFnSc4d1	Pražská
zástavbu	zástavba	k1gFnSc4	zástavba
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
v	v	k7c6	v
nejednom	nejeden	k4xCyIgInSc6	nejeden
případě	případ	k1gInSc6	případ
asanovanou	asanovaný	k2eAgFnSc7d1	asanovaná
<g/>
,	,	kIx,	,
či	či	k8xC	či
fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
společenských	společenský	k2eAgFnPc2d1	společenská
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
nejstarším	starý	k2eAgInPc3d3	nejstarší
pražským	pražský	k2eAgInPc3d1	pražský
snímkům	snímek	k1gInPc3	snímek
patří	patřit	k5eAaImIp3nS	patřit
záběry	záběr	k1gInPc4	záběr
z	z	k7c2	z
Hospodářsko-průmyslové	hospodářskorůmyslový	k2eAgFnSc2d1	hospodářsko-průmyslová
výstavy	výstava	k1gFnSc2	výstava
na	na	k7c6	na
Rohanském	Rohanský	k2eAgInSc6d1	Rohanský
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
z	z	k7c2	z
posledního	poslední	k2eAgNnSc2d1	poslední
období	období	k1gNnSc2	období
zase	zase	k9	zase
cyklus	cyklus	k1gInSc4	cyklus
fotografií	fotografia	k1gFnSc7	fotografia
rozestavěného	rozestavěný	k2eAgInSc2d1	rozestavěný
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
požárem	požár	k1gInSc7	požár
zničeného	zničený	k2eAgInSc2d1	zničený
a	a	k8xC	a
opět	opět	k6eAd1	opět
vystavěného	vystavěný	k2eAgNnSc2d1	vystavěné
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1880	[number]	k4	1880
až	až	k6eAd1	až
1882	[number]	k4	1882
nebo	nebo	k8xC	nebo
právě	právě	k6eAd1	právě
dokončená	dokončený	k2eAgFnSc1d1	dokončená
stavba	stavba	k1gFnSc1	stavba
Rudolfina	Rudolfinum	k1gNnSc2	Rudolfinum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
vedl	vést	k5eAaImAgMnS	vést
František	František	k1gMnSc1	František
Fridrich	Fridrich	k1gMnSc1	Fridrich
kromě	kromě	k7c2	kromě
ateliéru	ateliér	k1gInSc2	ateliér
v	v	k7c6	v
Michalské	Michalský	k2eAgFnSc6d1	Michalská
ulici	ulice	k1gFnSc6	ulice
ještě	ještě	k9	ještě
vlastní	vlastní	k2eAgInSc4d1	vlastní
fotografický	fotografický	k2eAgInSc4d1	fotografický
obchod	obchod	k1gInSc4	obchod
na	na	k7c6	na
nároží	nároží	k1gNnSc6	nároží
Příkopů	příkop	k1gInPc2	příkop
a	a	k8xC	a
Havířské	havířský	k2eAgFnSc2d1	Havířská
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
svůj	svůj	k3xOyFgInSc4	svůj
fotoateliér	fotoateliér	k1gInSc4	fotoateliér
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
malíři	malíř	k1gMnSc3	malíř
a	a	k8xC	a
vyučenému	vyučený	k2eAgMnSc3d1	vyučený
daguerrotypistovi	daguerrotypista	k1gMnSc3	daguerrotypista
Janu	Jan	k1gMnSc3	Jan
Malochovi	Maloch	k1gMnSc3	Maloch
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
mimochodem	mimochodem	k9	mimochodem
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
zřídil	zřídit	k5eAaPmAgInS	zřídit
nový	nový	k2eAgInSc4d1	nový
ateliér	ateliér	k1gInSc4	ateliér
na	na	k7c6	na
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Ferdinandově	Ferdinandův	k2eAgFnSc6d1	Ferdinandova
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc3d1	dnešní
Národní	národní	k2eAgFnSc3d1	národní
třídě	třída	k1gFnSc3	třída
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
kavárna	kavárna	k1gFnSc1	kavárna
Louvre	Louvre	k1gInSc1	Louvre
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Piktorialista	Piktorialista	k1gMnSc1	Piktorialista
Vladimír	Vladimír	k1gMnSc1	Vladimír
Jindřich	Jindřich	k1gMnSc1	Jindřich
Bufka	Bufka	k1gMnSc1	Bufka
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
J.	J.	kA	J.
Langhanse	Langhans	k1gMnSc2	Langhans
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
večer	večer	k6eAd1	večer
<g/>
,	,	kIx,	,
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
nebo	nebo	k8xC	nebo
v	v	k7c6	v
protisvětle	protisvětlo	k1gNnSc6	protisvětlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
znalost	znalost	k1gFnSc1	znalost
fotografických	fotografický	k2eAgNnPc2d1	fotografické
technik	technikum	k1gNnPc2	technikum
mu	on	k3xPp3gMnSc3	on
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
provádět	provádět	k5eAaImF	provádět
obtížné	obtížný	k2eAgInPc4d1	obtížný
záběry	záběr	k1gInPc4	záběr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
patří	patřit	k5eAaImIp3nS	patřit
tyto	tento	k3xDgFnPc4	tento
práce	práce	k1gFnPc4	práce
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
stylizovaných	stylizovaný	k2eAgFnPc6d1	stylizovaná
děl	dělo	k1gNnPc2	dělo
náročnými	náročný	k2eAgFnPc7d1	náročná
technikami	technika	k1gFnPc7	technika
gumotisku	gumotisk	k1gInSc2	gumotisk
a	a	k8xC	a
barevného	barevný	k2eAgInSc2d1	barevný
gumotisku	gumotisk	k1gInSc2	gumotisk
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
inovativní	inovativní	k2eAgInPc1d1	inovativní
<g/>
.	.	kIx.	.
</s>
<s>
Inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
se	se	k3xPyFc4	se
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
slohů	sloh	k1gInPc2	sloh
a	a	k8xC	a
stylů	styl	k1gInPc2	styl
<g/>
:	:	kIx,	:
impresionismu	impresionismus	k1gInSc2	impresionismus
a	a	k8xC	a
postimpresionismu	postimpresionismus	k1gInSc2	postimpresionismus
<g/>
,	,	kIx,	,
secese	secese	k1gFnSc2	secese
<g/>
,	,	kIx,	,
symbolismu	symbolismus	k1gInSc2	symbolismus
a	a	k8xC	a
dekadence	dekadence	k1gFnSc2	dekadence
<g/>
,	,	kIx,	,
kubismu	kubismus	k1gInSc2	kubismus
<g/>
,	,	kIx,	,
futurismu	futurismus	k1gInSc2	futurismus
nebo	nebo	k8xC	nebo
art	art	k?	art
deco	deco	k1gMnSc1	deco
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kříženecký	kříženecký	k2eAgMnSc1d1	kříženecký
se	se	k3xPyFc4	se
po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
architektury	architektura	k1gFnSc2	architektura
stal	stát	k5eAaPmAgMnS	stát
revidentem	revident	k1gMnSc7	revident
stavebního	stavební	k2eAgInSc2d1	stavební
úřadu	úřad	k1gInSc2	úřad
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
zálibě	záliba	k1gFnSc3	záliba
ve	v	k7c6	v
fotografování	fotografování	k1gNnSc6	fotografování
byl	být	k5eAaImAgInS	být
městským	městský	k2eAgInSc7d1	městský
archívem	archív	k1gInSc7	archív
požádán	požádat	k5eAaPmNgMnS	požádat
o	o	k7c4	o
fotodokumentaci	fotodokumentace	k1gFnSc4	fotodokumentace
starých	starý	k2eAgFnPc2d1	stará
a	a	k8xC	a
historicky	historicky	k6eAd1	historicky
cenných	cenný	k2eAgInPc2d1	cenný
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
památek	památka	k1gFnPc2	památka
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
soupis	soupis	k1gInSc4	soupis
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tak	tak	k6eAd1	tak
pořídil	pořídit	k5eAaPmAgMnS	pořídit
v	v	k7c6	v
letech	let	k1gInPc6	let
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
asi	asi	k9	asi
čtyři	čtyři	k4xCgInPc4	čtyři
tisíce	tisíc	k4xCgInPc4	tisíc
fotografií	fotografia	k1gFnPc2	fotografia
staré	starý	k2eAgFnSc2d1	stará
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
Klubu	klub	k1gInSc2	klub
fotografů	fotograf	k1gMnPc2	fotograf
amatérů	amatér	k1gMnPc2	amatér
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
fotografické	fotografický	k2eAgFnSc6d1	fotografická
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
vynikly	vyniknout	k5eAaPmAgFnP	vyniknout
jeho	jeho	k3xOp3gFnPc1	jeho
snímky	snímka	k1gFnPc1	snímka
soch	socha	k1gFnPc2	socha
z	z	k7c2	z
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
a	a	k8xC	a
fota	foto	k1gNnSc2	foto
staveb	stavba	k1gFnPc2	stavba
z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
pražským	pražský	k2eAgMnSc7d1	pražský
fotografem	fotograf	k1gMnSc7	fotograf
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Sudek	sudka	k1gFnPc2	sudka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Prahu	Praha	k1gFnSc4	Praha
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
sestavil	sestavit	k5eAaPmAgInS	sestavit
malé	malý	k2eAgNnSc4d1	malé
album	album	k1gNnSc4	album
156	[number]	k4	156
originálních	originální	k2eAgInPc2d1	originální
záběrů	záběr	k1gInPc2	záběr
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c6	na
formátu	formát	k1gInSc6	formát
3,5	[number]	k4	3,5
<g/>
×	×	k?	×
<g/>
5	[number]	k4	5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Invalidovně	invalidovna	k1gFnSc6	invalidovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
cyklus	cyklus	k1gInSc1	cyklus
Z	z	k7c2	z
Invalidovny	invalidovna	k1gFnSc2	invalidovna
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
žánrové	žánrový	k2eAgNnSc4d1	žánrové
malířství	malířství	k1gNnSc4	malířství
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
impresionismus	impresionismus	k1gInSc1	impresionismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
dostavbu	dostavba	k1gFnSc4	dostavba
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
k	k	k7c3	k
10	[number]	k4	10
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
15	[number]	k4	15
originálních	originální	k2eAgFnPc2d1	originální
fotografií	fotografia	k1gFnPc2	fotografia
s	s	k7c7	s
názvem	název	k1gInSc7	název
Svatý	svatý	k2eAgMnSc1d1	svatý
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
fotografický	fotografický	k2eAgInSc4d1	fotografický
ateliér	ateliér	k1gInSc4	ateliér
na	na	k7c6	na
Újezdu	Újezd	k1gInSc6	Újezd
čp.	čp.	k?	čp.
30	[number]	k4	30
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
přízemí	přízemí	k1gNnSc2	přízemí
domu	dům	k1gInSc2	dům
na	na	k7c6	na
Úvoze	úvoz	k1gInSc6	úvoz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
používal	používat	k5eAaImAgInS	používat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
nachází	nacházet	k5eAaImIp3nS	nacházet
Galerie	galerie	k1gFnSc1	galerie
Josefa	Josef	k1gMnSc2	Josef
Sudka	sudka	k1gFnSc1	sudka
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
Karel	Karel	k1gMnSc1	Karel
Plicka	Plicko	k1gNnSc2	Plicko
Prahu	práh	k1gInSc2	práh
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
obměnách	obměna	k1gFnPc6	obměna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
Státním	státní	k2eAgInSc6d1	státní
fotoměřickém	fotoměřický	k2eAgInSc6d1	fotoměřický
ústavu	ústav	k1gInSc6	ústav
s	s	k7c7	s
náplní	náplň	k1gFnSc7	náplň
fotografické	fotografický	k2eAgFnSc2d1	fotografická
dokumentace	dokumentace	k1gFnSc2	dokumentace
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Praha	Praha	k1gFnSc1	Praha
ve	v	k7c6	v
fotografiích	fotografia	k1gFnPc6	fotografia
K.	K.	kA	K.
P.	P.	kA	P.
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadčasovém	nadčasový	k2eAgInSc6d1	nadčasový
monumentalizujícím	monumentalizující	k2eAgInSc6d1	monumentalizující
pohledu	pohled	k1gInSc6	pohled
harmonii	harmonie	k1gFnSc4	harmonie
a	a	k8xC	a
krásu	krása	k1gFnSc4	krása
v	v	k7c6	v
architektonických	architektonický	k2eAgInPc6d1	architektonický
klenotech	klenot	k1gInPc6	klenot
minulosti	minulost	k1gFnSc2	minulost
<g/>
;	;	kIx,	;
publikace	publikace	k1gFnPc4	publikace
zároveň	zároveň	k6eAd1	zároveň
výrazně	výrazně	k6eAd1	výrazně
posilovala	posilovat	k5eAaImAgFnS	posilovat
národní	národní	k2eAgNnSc4d1	národní
povědomí	povědomí	k1gNnSc4	povědomí
v	v	k7c6	v
době	doba	k1gFnSc6	doba
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
několika	několik	k4yIc2	několik
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
Karol	Karol	k1gInSc1	Karol
Benický	Benický	k2eAgInSc1d1	Benický
<g/>
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Tůma	Tůma	k1gMnSc1	Tůma
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
téma	téma	k1gNnSc4	téma
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
Malou	malý	k2eAgFnSc4d1	malá
Stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
Hradčany	Hradčany	k1gInPc4	Hradčany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Suburbium	Suburbium	k1gNnSc1	Suburbium
Pragense	Pragense	k1gFnSc2	Pragense
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
oceněnou	oceněný	k2eAgFnSc4d1	oceněná
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
fotografická	fotografický	k2eAgFnSc1d1	fotografická
publikace	publikace	k1gFnSc1	publikace
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
Prager	Prager	k1gInSc1	Prager
Motive	motiv	k1gInSc5	motiv
<g/>
"	"	kIx"	"
získala	získat	k5eAaPmAgFnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
fotografická	fotografický	k2eAgFnSc1d1	fotografická
publikace	publikace	k1gFnSc1	publikace
s	s	k7c7	s
textem	text	k1gInSc7	text
<g/>
"	"	kIx"	"
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Sitenský	Sitenský	k2eAgMnSc1d1	Sitenský
Jan	Jan	k1gMnSc1	Jan
Reich	Reich	k?	Reich
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgMnPc2d1	poslední
klasiků	klasik	k1gMnPc2	klasik
české	český	k2eAgFnSc2d1	Česká
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
černobíle	černobíle	k6eAd1	černobíle
na	na	k7c4	na
velkoformátový	velkoformátový	k2eAgInSc4d1	velkoformátový
deskový	deskový	k2eAgInSc4d1	deskový
přístroj	přístroj	k1gInSc4	přístroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
skupiny	skupina	k1gFnSc2	skupina
Český	český	k2eAgMnSc1d1	český
dřevák	dřevák	k1gMnSc1	dřevák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
cyklem	cyklus	k1gInSc7	cyklus
je	být	k5eAaImIp3nS	být
Mizející	mizející	k2eAgFnSc1d1	mizející
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
snímal	snímat	k5eAaImAgInS	snímat
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
,	,	kIx,	,
ohrady	ohrada	k1gFnSc2	ohrada
<g/>
,	,	kIx,	,
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
nábřeží	nábřeží	k1gNnSc2	nábřeží
apod.	apod.	kA	apod.
Na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
si	se	k3xPyFc3	se
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
produkcí	produkce	k1gFnSc7	produkce
barevných	barevný	k2eAgFnPc2d1	barevná
pohlednic	pohlednice	k1gFnPc2	pohlednice
v	v	k7c6	v
družstvu	družstvo	k1gNnSc6	družstvo
Fotografia	fotografia	k1gFnSc1	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Pepa	Pepa	k1gFnSc1	Pepa
Středa	středa	k1gFnSc1	středa
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
panoramatické	panoramatický	k2eAgInPc4d1	panoramatický
pohledy	pohled	k1gInPc4	pohled
na	na	k7c4	na
město	město	k1gNnSc4	město
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pohribný	Pohribný	k2eAgMnSc1d1	Pohribný
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
fotografické	fotografický	k2eAgFnSc6d1	fotografická
tvorbě	tvorba	k1gFnSc6	tvorba
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k8xC	jako
tvůrčí	tvůrčí	k2eAgInPc1d1	tvůrčí
postupy	postup	k1gInPc1	postup
vícenásobnou	vícenásobný	k2eAgFnSc4d1	vícenásobná
expozici	expozice	k1gFnSc4	expozice
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
pohybovou	pohybový	k2eAgFnSc7d1	pohybová
neostrostí	neostrost	k1gFnSc7	neostrost
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
nejen	nejen	k6eAd1	nejen
principy	princip	k1gInPc4	princip
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
výrazných	výrazný	k2eAgFnPc2d1	výrazná
barevných	barevný	k2eAgFnPc2d1	barevná
stop	stopa	k1gFnPc2	stopa
či	či	k8xC	či
monochromatických	monochromatický	k2eAgNnPc2d1	monochromatické
řešení	řešení	k1gNnPc2	řešení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využívá	využívat	k5eAaPmIp3nS	využívat
také	také	k9	také
světelné	světelný	k2eAgFnSc2d1	světelná
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
"	"	kIx"	"
<g/>
zviditelňuje	zviditelňovat	k5eAaImIp3nS	zviditelňovat
<g/>
"	"	kIx"	"
energie	energie	k1gFnSc1	energie
zvolených	zvolený	k2eAgFnPc2d1	zvolená
přírodních	přírodní	k2eAgFnPc2d1	přírodní
lokalit	lokalita	k1gFnPc2	lokalita
či	či	k8xC	či
posvátných	posvátný	k2eAgInPc2d1	posvátný
kamenů	kámen	k1gInPc2	kámen
vztyčených	vztyčený	k2eAgInPc2d1	vztyčený
lidskou	lidský	k2eAgFnSc7d1	lidská
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Koudelka	Koudelka	k1gMnSc1	Koudelka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
dokumentoval	dokumentovat	k5eAaBmAgInS	dokumentovat
invazi	invaze	k1gFnSc4	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nejdřív	dříve	k6eAd3	dříve
publikoval	publikovat	k5eAaBmAgInS	publikovat
anonymně	anonymně	k6eAd1	anonymně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
symboly	symbol	k1gInPc1	symbol
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
za	za	k7c4	za
ně	on	k3xPp3gNnSc4	on
dostal	dostat	k5eAaPmAgInS	dostat
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
Roberta	Robert	k1gMnSc2	Robert
Capy	capa	k1gFnSc2	capa
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Všetečka	Všetečka	k1gMnSc1	Všetečka
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgInPc7	svůj
snímky	snímek	k1gInPc7	snímek
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
dokumentárními	dokumentární	k2eAgInPc7d1	dokumentární
snímky	snímek	k1gInPc7	snímek
a	a	k8xC	a
ilustracemi	ilustrace	k1gFnPc7	ilustrace
inspirovanými	inspirovaný	k2eAgFnPc7d1	inspirovaná
podle	podle	k7c2	podle
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Nezvalův	Nezvalův	k2eAgMnSc1d1	Nezvalův
Pražský	pražský	k2eAgMnSc1d1	pražský
chodec	chodec	k1gMnSc1	chodec
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Dostál	Dostál	k1gMnSc1	Dostál
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
věnuje	věnovat	k5eAaPmIp3nS	věnovat
především	především	k9	především
lidem	člověk	k1gMnPc3	člověk
z	z	k7c2	z
pražských	pražský	k2eAgFnPc2d1	Pražská
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
jemný	jemný	k2eAgInSc1d1	jemný
humor	humor	k1gInSc1	humor
těžící	těžící	k2eAgInSc1d1	těžící
z	z	k7c2	z
často	často	k6eAd1	často
absurdních	absurdní	k2eAgNnPc2d1	absurdní
setkání	setkání	k1gNnPc2	setkání
více	hodně	k6eAd2	hodně
nesourodých	sourodý	k2eNgInPc2d1	nesourodý
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
obrazu	obraz	k1gInSc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgFnSc7d1	prvotní
inspirací	inspirace	k1gFnSc7	inspirace
ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
rodná	rodný	k2eAgFnSc1d1	rodná
čtvrť	čtvrť	k1gFnSc1	čtvrť
Vršovice	Vršovice	k1gFnPc1	Vršovice
<g/>
.	.	kIx.	.
</s>
<s>
Zachycoval	zachycovat	k5eAaImAgMnS	zachycovat
lidi	člověk	k1gMnPc4	člověk
ve	v	k7c6	v
zdánlivě	zdánlivě	k6eAd1	zdánlivě
obyčejných	obyčejný	k2eAgFnPc6d1	obyčejná
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
měly	mít	k5eAaImAgInP	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
výtvarný	výtvarný	k2eAgInSc4d1	výtvarný
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
věnoval	věnovat	k5eAaPmAgMnS	věnovat
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
<g/>
:	:	kIx,	:
Trvalé	trvalý	k2eAgNnSc1d1	trvalé
bydliště	bydliště	k1gNnSc1	bydliště
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
ocenění	ocenění	k1gNnSc1	ocenění
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
kniha	kniha	k1gFnSc1	kniha
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Praha	Praha	k1gFnSc1	Praha
ve	v	k7c6	v
společné	společný	k2eAgFnSc6d1	společná
péči	péče	k1gFnSc6	péče
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
konají	konat	k5eAaImIp3nP	konat
fotografické	fotografický	k2eAgInPc4d1	fotografický
festivaly	festival	k1gInPc4	festival
a	a	k8xC	a
výstavy	výstava	k1gFnPc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
akcí	akce	k1gFnPc2	akce
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
Prague	Pragu	k1gInPc1	Pragu
Photo	Photo	k1gNnSc1	Photo
<g/>
,	,	kIx,	,
Prague	Prague	k1gNnSc1	Prague
Biennale	Biennale	k1gFnPc2	Biennale
Photo	Photo	k1gNnSc1	Photo
<g/>
,	,	kIx,	,
Digiforum	Digiforum	k1gNnSc1	Digiforum
nebo	nebo	k8xC	nebo
Festival	festival	k1gInSc4	festival
Fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Výstavy	výstava	k1gFnPc1	výstava
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
fotografických	fotografický	k2eAgFnPc2d1	fotografická
galerií	galerie	k1gFnPc2	galerie
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Galerie	galerie	k1gFnSc1	galerie
Leica	Leica	k1gFnSc1	Leica
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Langhans	Langhans	k1gInSc1	Langhans
nebo	nebo	k8xC	nebo
Galerie	galerie	k1gFnSc1	galerie
Josefa	Josef	k1gMnSc2	Josef
Sudka	sudka	k1gFnSc1	sudka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
vybrané	vybraný	k2eAgFnPc4d1	vybraná
výstavy	výstava	k1gFnPc4	výstava
věnuje	věnovat	k5eAaPmIp3nS	věnovat
fotografii	fotografia	k1gFnSc4	fotografia
Centrum	centrum	k1gNnSc1	centrum
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
DOX	DOX	kA	DOX
nebo	nebo	k8xC	nebo
Galerie	galerie	k1gFnSc2	galerie
Rudolfinum	Rudolfinum	k1gNnSc1	Rudolfinum
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Magistrát	magistrát	k1gInSc1	magistrát
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
jako	jako	k9	jako
taková	takový	k3xDgFnSc1	takový
je	být	k5eAaImIp3nS	být
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
jejích	její	k3xOp3gInPc2	její
ústředních	ústřední	k2eAgInPc2d1	ústřední
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
de	de	k?	de
facto	facto	k1gNnSc4	facto
opět	opět	k6eAd1	opět
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
specifické	specifický	k2eAgNnSc1d1	specifické
postavení	postavení	k1gNnSc1	postavení
obce	obec	k1gFnSc2	obec
i	i	k8xC	i
kraje	kraj	k1gInSc2	kraj
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Netýká	týkat	k5eNaImIp3nS	týkat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
(	(	kIx(	(
<g/>
č.	č.	k?	č.
128	[number]	k4	128
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
krajích	kraj	k1gInPc6	kraj
(	(	kIx(	(
<g/>
č.	č.	k?	č.
129	[number]	k4	129
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
č.	č.	k?	č.
131	[number]	k4	131
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
statutu	statut	k1gInSc6	statut
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c4	mezi
statutární	statutární	k2eAgNnPc4d1	statutární
města	město	k1gNnPc4	město
ji	on	k3xPp3gFnSc4	on
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
vydává	vydávat	k5eAaImIp3nS	vydávat
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
právní	právní	k2eAgInPc1d1	právní
předpisy	předpis	k1gInPc1	předpis
<g/>
,	,	kIx,	,
vyhlášky	vyhláška	k1gFnPc1	vyhláška
i	i	k8xC	i
nařízení	nařízení	k1gNnSc1	nařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
publikuje	publikovat	k5eAaBmIp3nS	publikovat
ve	v	k7c6	v
Sbírce	sbírka	k1gFnSc6	sbírka
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zároveň	zároveň	k6eAd1	zároveň
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
správní	správní	k2eAgFnSc1d1	správní
instituce	instituce	k1gFnSc1	instituce
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Podrobněji	podrobně	k6eAd2	podrobně
o	o	k7c6	o
částech	část	k1gFnPc6	část
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
členění	členění	k1gNnSc6	členění
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
do	do	k7c2	do
současností	současnost	k1gFnPc2	současnost
čtěte	číst	k5eAaImRp2nP	číst
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Části	část	k1gFnSc2	část
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
o	o	k7c6	o
zásadních	zásadní	k2eAgInPc6d1	zásadní
dějinných	dějinný	k2eAgInPc6d1	dějinný
přelomech	přelom	k1gInPc6	přelom
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Královské	královský	k2eAgInPc1d1	královský
hlavní	hlavní	k2eAgInPc1d1	hlavní
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
byly	být	k5eAaImAgInP	být
správní	správní	k2eAgInPc1d1	správní
obvody	obvod	k1gInPc1	obvod
až	až	k9	až
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
výjimku	výjimka	k1gFnSc4	výjimka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
tvořeny	tvořit	k5eAaImNgFnP	tvořit
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
celými	celý	k2eAgInPc7d1	celý
katastrálními	katastrální	k2eAgInPc7d1	katastrální
celky	celek	k1gInPc7	celek
<g/>
,	,	kIx,	,
bývalými	bývalý	k2eAgFnPc7d1	bývalá
obcemi	obec	k1gFnPc7	obec
či	či	k8xC	či
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zásadní	zásadní	k2eAgFnSc3d1	zásadní
změně	změna	k1gFnSc3	změna
správního	správní	k2eAgNnSc2d1	správní
členění	členění	k1gNnSc2	členění
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hranice	hranice	k1gFnSc2	hranice
mnoha	mnoho	k4c2	mnoho
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
správních	správní	k2eAgInPc2d1	správní
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
jsou	být	k5eAaImIp3nP	být
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
katastrálních	katastrální	k2eAgFnPc6d1	katastrální
území	území	k1gNnSc6	území
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
správních	správní	k2eAgFnPc2d1	správní
a	a	k8xC	a
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
<g/>
,	,	kIx,	,
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nadále	nadále	k6eAd1	nadále
rozhodující	rozhodující	k2eAgInPc1d1	rozhodující
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
evidenci	evidence	k1gFnSc4	evidence
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
nemovitostí	nemovitost	k1gFnPc2	nemovitost
a	a	k8xC	a
označování	označování	k1gNnSc2	označování
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
112	[number]	k4	112
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
různé	různý	k2eAgFnSc2d1	různá
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
charakteru	charakter	k1gInSc2	charakter
i	i	k8xC	i
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Josefov	Josefov	k1gInSc1	Josefov
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Hradčany	Hradčany	k1gInPc1	Hradčany
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
čtvrti	čtvrt	k1gFnPc1	čtvrt
poblíž	poblíž	k7c2	poblíž
centra	centrum	k1gNnSc2	centrum
<g/>
:	:	kIx,	:
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
,	,	kIx,	,
Nusle	Nusle	k1gFnPc1	Nusle
<g/>
,	,	kIx,	,
Vršovice	Vršovice	k1gFnPc1	Vršovice
<g/>
,	,	kIx,	,
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
,	,	kIx,	,
Karlín	Karlín	k1gInSc1	Karlín
<g/>
,	,	kIx,	,
Libeň	Libeň	k1gFnSc1	Libeň
<g/>
,	,	kIx,	,
Vysočany	Vysočany	k1gInPc1	Vysočany
<g/>
,	,	kIx,	,
Holešovice	Holešovice	k1gFnPc1	Holešovice
<g/>
,	,	kIx,	,
Bubeneč	Bubeneč	k1gInSc1	Bubeneč
<g/>
,	,	kIx,	,
Dejvice	Dejvice	k1gFnPc1	Dejvice
<g/>
,	,	kIx,	,
Braník	Braník	k1gInSc1	Braník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
deset	deset	k4xCc4	deset
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
okresů	okres	k1gInPc2	okres
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
o	o	k7c4	o
nově	nově	k6eAd1	nově
přidružené	přidružený	k2eAgFnPc4d1	přidružená
obce	obec	k1gFnPc4	obec
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
územním	územní	k2eAgNnSc6d1	územní
členění	členění	k1gNnSc6	členění
státu	stát	k1gInSc2	stát
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
i	i	k9	i
správními	správní	k2eAgInPc7d1	správní
a	a	k8xC	a
samosprávnými	samosprávný	k2eAgInPc7d1	samosprávný
obvody	obvod	k1gInPc7	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
většiny	většina	k1gFnSc2	většina
území	území	k1gNnSc2	území
obvodů	obvod	k1gInPc2	obvod
podléhali	podléhat	k5eAaImAgMnP	podléhat
přímo	přímo	k6eAd1	přímo
obvodnímu	obvodní	k2eAgInSc3d1	obvodní
národnímu	národní	k2eAgInSc3d1	národní
výboru	výbor	k1gInSc3	výbor
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
obvodnímu	obvodní	k2eAgInSc3d1	obvodní
úřadu	úřad	k1gInSc3	úřad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
později	pozdě	k6eAd2	pozdě
připojených	připojený	k2eAgFnPc6d1	připojená
obcích	obec	k1gFnPc6	obec
však	však	k9	však
zůstaly	zůstat	k5eAaPmAgInP	zůstat
jako	jako	k8xC	jako
mezičlánek	mezičlánek	k1gInSc1	mezičlánek
místní	místní	k2eAgMnSc1d1	místní
národní	národní	k2eAgInPc4d1	národní
výbory	výbor	k1gInPc4	výbor
(	(	kIx(	(
<g/>
místní	místní	k2eAgInPc1d1	místní
úřady	úřad	k1gInPc1	úřad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zároveň	zároveň	k6eAd1	zároveň
spadaly	spadat	k5eAaImAgInP	spadat
i	i	k9	i
pod	pod	k7c4	pod
působnost	působnost	k1gFnSc4	působnost
příslušného	příslušný	k2eAgInSc2d1	příslušný
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
nejsou	být	k5eNaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
městské	městský	k2eAgInPc1d1	městský
obvody	obvod	k1gInPc1	obvod
samosprávnými	samosprávný	k2eAgInPc7d1	samosprávný
celky	celek	k1gInPc7	celek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gInSc7	jejich
základem	základ	k1gInSc7	základ
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
totožného	totožný	k2eAgInSc2d1	totožný
názvu	název	k1gInSc2	název
(	(	kIx(	(
<g/>
u	u	k7c2	u
prvních	první	k4xOgNnPc2	první
tří	tři	k4xCgNnPc2	tři
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
části	část	k1gFnPc1	část
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
přímo	přímo	k6eAd1	přímo
totožné	totožný	k2eAgInPc1d1	totožný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
územně-správními	územněprávní	k2eAgInPc7d1	územně-správní
obvody	obvod	k1gInPc7	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
organizovány	organizovat	k5eAaBmNgInP	organizovat
jen	jen	k9	jen
soudy	soud	k1gInPc1	soud
<g/>
,	,	kIx,	,
pošty	pošta	k1gFnPc1	pošta
nebo	nebo	k8xC	nebo
různé	různý	k2eAgFnPc1d1	různá
správní	správní	k2eAgFnPc1d1	správní
firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
deseti	deset	k4xCc2	deset
obvodů	obvod	k1gInPc2	obvod
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
36	[number]	k4	36
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nyní	nyní	k6eAd1	nyní
tvoří	tvořit	k5eAaImIp3nP	tvořit
území	území	k1gNnSc4	území
obvodu	obvod	k1gInSc2	obvod
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
má	mít	k5eAaImIp3nS	mít
57	[number]	k4	57
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spravovány	spravovat	k5eAaImNgFnP	spravovat
voleným	volený	k2eAgNnSc7d1	volené
zastupitelstvem	zastupitelstvo	k1gNnSc7	zastupitelstvo
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
radou	rada	k1gMnSc7	rada
<g/>
,	,	kIx,	,
starostou	starosta	k1gMnSc7	starosta
a	a	k8xC	a
úřadem	úřad	k1gInSc7	úřad
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Úřady	úřada	k1gMnPc7	úřada
některých	některý	k3yIgFnPc2	některý
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
měly	mít	k5eAaImAgFnP	mít
již	již	k9	již
od	od	k7c2	od
ustavení	ustavení	k1gNnSc2	ustavení
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
svěřeny	svěřit	k5eAaPmNgFnP	svěřit
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
působností	působnost	k1gFnPc2	působnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
i	i	k9	i
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
úroveň	úroveň	k1gFnSc1	úroveň
působností	působnost	k1gFnPc2	působnost
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Praze	Praha	k1gFnSc6	Praha
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
22	[number]	k4	22
správních	správní	k2eAgInPc2d1	správní
obvodů	obvod	k1gInPc2	obvod
–	–	k?	–
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
nebo	nebo	k8xC	nebo
textu	text	k1gInSc6	text
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
správní	správní	k2eAgInSc1d1	správní
obvod	obvod	k1gInSc1	obvod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgInSc7	ten
nejčastěji	často	k6eAd3	často
míněn	míněn	k2eAgInSc1d1	míněn
správní	správní	k2eAgInSc1d1	správní
obvod	obvod	k1gInSc1	obvod
této	tento	k3xDgFnSc2	tento
působnosti	působnost	k1gFnSc2	působnost
a	a	k8xC	a
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
je	být	k5eAaImIp3nS	být
názvem	název	k1gInSc7	název
těchto	tento	k3xDgInPc6	tento
22	[number]	k4	22
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
slovo	slovo	k1gNnSc4	slovo
Praha	Praha	k1gFnSc1	Praha
s	s	k7c7	s
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
číslovkou	číslovka	k1gFnSc7	číslovka
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgNnSc1d1	stejné
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
správní	správní	k2eAgInSc4d1	správní
obvod	obvod	k1gInSc4	obvod
jejich	jejich	k3xOp3gFnSc2	jejich
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
působnosti	působnost	k1gFnSc2	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
22	[number]	k4	22
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
mají	mít	k5eAaImIp3nP	mít
tuto	tento	k3xDgFnSc4	tento
působnost	působnost	k1gFnSc4	působnost
jen	jen	k9	jen
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k9	i
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
22	[number]	k4	22
správních	správní	k2eAgInPc2d1	správní
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
všech	všecek	k3xTgFnPc2	všecek
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Prahy	Praha	k1gFnSc2	Praha
1	[number]	k4	1
až	až	k9	až
Prahy	Praha	k1gFnSc2	Praha
3	[number]	k4	3
<g/>
)	)	kIx)	)
však	však	k9	však
není	být	k5eNaImIp3nS	být
totožné	totožný	k2eAgNnSc1d1	totožné
s	s	k7c7	s
územními	územní	k2eAgInPc7d1	územní
obvody	obvod	k1gInPc7	obvod
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
územním	územní	k2eAgNnSc6d1	územní
členění	členění	k1gNnSc6	členění
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
(	(	kIx(	(
<g/>
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
i	i	k8xC	i
správními	správní	k2eAgInPc7d1	správní
obvody	obvod	k1gInPc7	obvod
a	a	k8xC	a
měly	mít	k5eAaImAgInP	mít
i	i	k9	i
vlastní	vlastní	k2eAgInPc1d1	vlastní
sbory	sbor	k1gInPc1	sbor
(	(	kIx(	(
<g/>
ONV	ONV	kA	ONV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
i	i	k8xC	i
poslanci	poslanec	k1gMnPc1	poslanec
opakovaně	opakovaně	k6eAd1	opakovaně
přicházejí	přicházet	k5eAaImIp3nP	přicházet
s	s	k7c7	s
návrhy	návrh	k1gInPc7	návrh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
staré	starý	k2eAgNnSc1d1	staré
členění	členění	k1gNnSc1	členění
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
901	[number]	k4	901
základních	základní	k2eAgFnPc2d1	základní
sídelních	sídelní	k2eAgFnPc2d1	sídelní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
vrstva	vrstva	k1gFnSc1	vrstva
pražských	pražský	k2eAgInPc2d1	pražský
pomístních	pomístní	k2eAgInPc2d1	pomístní
názvů	název	k1gInPc2	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vžily	vžít	k5eAaPmAgFnP	vžít
zejména	zejména	k9	zejména
názvy	název	k1gInPc4	název
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
rynků	rynek	k1gInPc2	rynek
a	a	k8xC	a
trhů	trh	k1gInPc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
místa	místo	k1gNnPc1	místo
měla	mít	k5eAaImAgNnP	mít
vžitý	vžitý	k2eAgInSc4d1	vžitý
název	název	k1gInSc4	název
latinský	latinský	k2eAgInSc4d1	latinský
<g/>
,	,	kIx,	,
německý	německý	k2eAgInSc4d1	německý
i	i	k8xC	i
český	český	k2eAgInSc4d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Vžívala	vžívat	k5eAaImAgFnS	vžívat
se	se	k3xPyFc4	se
též	též	k9	též
označení	označení	k1gNnSc1	označení
ulic	ulice	k1gFnPc2	ulice
podle	podle	k7c2	podle
zasvěcení	zasvěcení	k1gNnSc2	zasvěcení
kostela	kostel	k1gInSc2	kostel
nebo	nebo	k8xC	nebo
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
významných	významný	k2eAgMnPc2d1	významný
majitelů	majitel	k1gMnPc2	majitel
domů	dům	k1gInPc2	dům
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
nebo	nebo	k8xC	nebo
domovních	domovní	k2eAgNnPc2d1	domovní
znamení	znamení	k1gNnPc2	znamení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
vzhledu	vzhled	k1gInSc2	vzhled
nebo	nebo	k8xC	nebo
charakteru	charakter	k1gInSc2	charakter
ulice	ulice	k1gFnSc2	ulice
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
rozlišovány	rozlišovat	k5eAaImNgInP	rozlišovat
zejména	zejména	k9	zejména
podle	podle	k7c2	podle
domovních	domovní	k2eAgNnPc2d1	domovní
znamení	znamení	k1gNnPc2	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
zavedena	zaveden	k2eAgNnPc1d1	zavedeno
popisná	popisný	k2eAgNnPc1d1	popisné
čísla	číslo	k1gNnPc1	číslo
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sloučení	sloučení	k1gNnSc2	sloučení
čtyř	čtyři	k4xCgNnPc2	čtyři
pražských	pražský	k2eAgNnPc2d1	Pražské
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
1784	[number]	k4	1784
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
počínaje	počínaje	k7c7	počínaje
27	[number]	k4	27
<g/>
.	.	kIx.	.
říjnem	říjen	k1gInSc7	říjen
1787	[number]	k4	1787
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zavedeny	zaveden	k2eAgInPc4d1	zaveden
oficiální	oficiální	k2eAgInPc4d1	oficiální
názvy	název	k1gInPc4	název
ulic	ulice	k1gFnPc2	ulice
jako	jako	k8xS	jako
nutný	nutný	k2eAgInSc4d1	nutný
doplněk	doplněk	k1gInSc4	doplněk
k	k	k7c3	k
domovním	domovní	k2eAgNnPc3d1	domovní
číslům	číslo	k1gNnPc3	číslo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
orientační	orientační	k2eAgNnPc1d1	orientační
funkci	funkce	k1gFnSc6	funkce
plnila	plnit	k5eAaImAgFnS	plnit
hůře	zle	k6eAd2	zle
než	než	k8xS	než
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
domovní	domovní	k2eAgNnSc4d1	domovní
znamení	znamení	k1gNnSc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgNnSc7d1	úřední
zavedením	zavedení	k1gNnSc7	zavedení
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
názvy	název	k1gInPc1	název
ulic	ulice	k1gFnPc2	ulice
důsledně	důsledně	k6eAd1	důsledně
poněmčeny	poněmčen	k2eAgFnPc4d1	poněmčena
a	a	k8xC	a
úřední	úřední	k2eAgNnSc1d1	úřední
zavádění	zavádění	k1gNnSc1	zavádění
a	a	k8xC	a
schvalování	schvalování	k1gNnSc1	schvalování
postupně	postupně	k6eAd1	postupně
nahrazovalo	nahrazovat	k5eAaImAgNnS	nahrazovat
spontánní	spontánní	k2eAgInSc4d1	spontánní
vývoj	vývoj	k1gInSc4	vývoj
názvů	název	k1gInPc2	název
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
císařské	císařský	k2eAgNnSc1d1	císařské
nařízení	nařízení	k1gNnSc1	nařízení
o	o	k7c4	o
pojmenovávání	pojmenovávání	k1gNnSc4	pojmenovávání
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
zavádění	zavádění	k1gNnSc6	zavádění
domovních	domovní	k2eAgNnPc2d1	domovní
čísel	číslo	k1gNnPc2	číslo
podle	podle	k7c2	podle
ulic	ulice	k1gFnPc2	ulice
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
orientační	orientační	k2eAgNnPc4d1	orientační
čísla	číslo	k1gNnPc4	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1868	[number]	k4	1868
se	se	k3xPyFc4	se
sbor	sbor	k1gInSc1	sbor
pražských	pražský	k2eAgMnPc2d1	pražský
obecních	obecní	k2eAgMnPc2d1	obecní
starších	starší	k1gMnPc2	starší
usnesl	usnést	k5eAaPmAgMnS	usnést
označit	označit	k5eAaPmF	označit
pražské	pražský	k2eAgFnSc2d1	Pražská
ulice	ulice	k1gFnSc2	ulice
jednotnými	jednotný	k2eAgFnPc7d1	jednotná
německo-českými	německo-český	k2eAgFnPc7d1	německo-česká
plechovými	plechový	k2eAgFnPc7d1	plechová
tabulemi	tabule	k1gFnPc7	tabule
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
stanovil	stanovit	k5eAaPmAgInS	stanovit
zásadu	zásada	k1gFnSc4	zásada
unikátnosti	unikátnost	k1gFnSc2	unikátnost
názvu	název	k1gInSc2	název
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
usnesení	usnesení	k1gNnSc2	usnesení
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
názvů	název	k1gInPc2	název
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
její	její	k3xOp3gFnSc2	její
práce	práce	k1gFnSc2	práce
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
Autentický	autentický	k2eAgInSc1d1	autentický
ukazatel	ukazatel	k1gInSc1	ukazatel
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnSc2	náměstí
i	i	k8xC	i
čísel	číslo	k1gNnPc2	číslo
domovních	domovní	k2eAgMnPc2d1	domovní
královského	královský	k2eAgNnSc2d1	královské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
z	z	k7c2	z
nařízení	nařízení	k1gNnSc2	nařízení
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
sestavil	sestavit	k5eAaPmAgMnS	sestavit
ředitel	ředitel	k1gMnSc1	ředitel
městského	městský	k2eAgInSc2d1	městský
archivu	archiv	k1gInSc2	archiv
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nově	nově	k6eAd1	nově
připojených	připojený	k2eAgFnPc6d1	připojená
Holešovicích	Holešovice	k1gFnPc6	Holešovice
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgInP	umístit
jen	jen	k9	jen
české	český	k2eAgInPc1d1	český
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
jednojazyčné	jednojazyčný	k2eAgFnSc2d1	jednojazyčná
tabulky	tabulka	k1gFnSc2	tabulka
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
národních	národní	k2eAgFnPc6d1	národní
barvách	barva	k1gFnPc6	barva
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
soudních	soudní	k2eAgInPc6d1	soudní
sporech	spor	k1gInPc6	spor
bylo	být	k5eAaImAgNnS	být
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1894	[number]	k4	1894
vyhláškou	vyhláška	k1gFnSc7	vyhláška
rozhodnuto	rozhodnut	k2eAgNnSc4d1	rozhodnuto
o	o	k7c4	o
nahrazení	nahrazení	k1gNnSc4	nahrazení
dvojjazyčných	dvojjazyčný	k2eAgFnPc2d1	dvojjazyčná
tabulí	tabule	k1gFnPc2	tabule
českými	český	k2eAgInPc7d1	český
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1898	[number]	k4	1898
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
definitivně	definitivně	k6eAd1	definitivně
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
smaltované	smaltovaný	k2eAgFnPc1d1	smaltovaná
červenobílé	červenobílý	k2eAgFnPc1d1	červenobílá
tabule	tabule	k1gFnPc1	tabule
s	s	k7c7	s
názvy	název	k1gInPc7	název
ulic	ulice	k1gFnPc2	ulice
jednotného	jednotný	k2eAgInSc2d1	jednotný
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
popisná	popisný	k2eAgNnPc1d1	popisné
čísla	číslo	k1gNnPc1	číslo
domů	dům	k1gInPc2	dům
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
provedena	provést	k5eAaPmNgFnS	provést
bílým	bílý	k2eAgNnSc7d1	bílé
písmem	písmo	k1gNnSc7	písmo
na	na	k7c6	na
červených	červený	k2eAgFnPc6d1	červená
tabulkách	tabulka	k1gFnPc6	tabulka
a	a	k8xC	a
orientační	orientační	k2eAgNnPc1d1	orientační
čísla	číslo	k1gNnPc1	číslo
bílým	bílý	k2eAgNnSc7d1	bílé
písmem	písmo	k1gNnSc7	písmo
na	na	k7c6	na
modrých	modrý	k2eAgFnPc6d1	modrá
tabulkách	tabulka	k1gFnPc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
městských	městský	k2eAgFnPc6d1	městská
částech	část	k1gFnPc6	část
je	být	k5eAaImIp3nS	být
orientační	orientační	k2eAgInSc1d1	orientační
systém	systém	k1gInSc1	systém
doplněn	doplnit	k5eAaPmNgInS	doplnit
též	též	k9	též
názvy	název	k1gInPc1	název
ulic	ulice	k1gFnPc2	ulice
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
směrového	směrový	k2eAgNnSc2d1	směrové
dopravního	dopravní	k2eAgNnSc2d1	dopravní
značení	značení	k1gNnSc2	značení
nebo	nebo	k8xC	nebo
speciálními	speciální	k2eAgInPc7d1	speciální
městskými	městský	k2eAgInPc7d1	městský
rozcestníky	rozcestník	k1gInPc7	rozcestník
či	či	k8xC	či
směrovkami	směrovka	k1gFnPc7	směrovka
<g/>
.	.	kIx.	.
</s>
<s>
Evidenční	evidenční	k2eAgFnPc1d1	evidenční
části	část	k1gFnPc1	část
Prahy	Praha	k1gFnSc2	Praha
se	s	k7c7	s
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
řadami	řada	k1gFnPc7	řada
čísel	číslo	k1gNnPc2	číslo
popisných	popisný	k2eAgFnPc2d1	popisná
jsou	být	k5eAaImIp3nP	být
totožné	totožný	k2eAgInPc1d1	totožný
s	s	k7c7	s
katastrálními	katastrální	k2eAgNnPc7d1	katastrální
územími	území	k1gNnPc7	území
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnPc1	ulice
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
evidenčních	evidenční	k2eAgFnPc6d1	evidenční
částech	část	k1gFnPc6	část
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
částí	část	k1gFnPc2	část
Prahy	Praha	k1gFnSc2	Praha
jsou	být	k5eAaImIp3nP	být
domům	dům	k1gInPc3	dům
přidělována	přidělován	k2eAgNnPc1d1	přidělováno
i	i	k8xC	i
čísla	číslo	k1gNnPc1	číslo
orientační	orientační	k2eAgNnPc1d1	orientační
<g/>
;	;	kIx,	;
nejsou	být	k5eNaImIp3nP	být
přidělena	přidělen	k2eAgNnPc1d1	přiděleno
například	například	k6eAd1	například
v	v	k7c6	v
částech	část	k1gFnPc6	část
Běchovice	Běchovice	k1gFnPc1	Běchovice
<g/>
,	,	kIx,	,
Benice	Benice	k1gFnPc1	Benice
<g/>
,	,	kIx,	,
Šeberov	Šeberov	k1gInSc1	Šeberov
<g/>
,	,	kIx,	,
Petrovice	Petrovice	k1gFnSc1	Petrovice
nebo	nebo	k8xC	nebo
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
<g/>
.	.	kIx.	.
</s>
<s>
Šesticifernými	šesticiferný	k2eAgNnPc7d1	šesticiferné
čísly	číslo	k1gNnPc7	číslo
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
sloupy	sloup	k1gInPc1	sloup
veřejného	veřejný	k2eAgNnSc2d1	veřejné
osvětlení	osvětlení	k1gNnSc2	osvětlení
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
společnosti	společnost	k1gFnSc2	společnost
Eltodo	Eltoda	k1gFnSc5	Eltoda
Citelum	Citelum	k1gNnSc4	Citelum
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
identifikaci	identifikace	k1gFnSc4	identifikace
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
lokalizaci	lokalizace	k1gFnSc3	lokalizace
i	i	k9	i
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
záchranný	záchranný	k2eAgInSc1d1	záchranný
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
orientačním	orientační	k2eAgInSc7d1	orientační
prvkem	prvek	k1gInSc7	prvek
jsou	být	k5eAaImIp3nP	být
názvy	název	k1gInPc4	název
zastávek	zastávka	k1gFnPc2	zastávka
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Soudy	soud	k1gInPc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Praha	Praha	k1gFnSc1	Praha
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
soudní	soudní	k2eAgFnSc2d1	soudní
instituce	instituce	k1gFnSc2	instituce
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
vrchních	vrchní	k2eAgInPc2d1	vrchní
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
s	s	k7c7	s
působností	působnost	k1gFnSc7	působnost
vůči	vůči	k7c3	vůči
téměř	téměř	k6eAd1	téměř
celým	celý	k2eAgFnPc3d1	celá
Čechám	Čechy	k1gFnPc3	Čechy
<g/>
,	,	kIx,	,
působnost	působnost	k1gFnSc4	působnost
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
vůči	vůči	k7c3	vůči
území	území	k1gNnSc3	území
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
městský	městský	k2eAgInSc1d1	městský
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
jde	jít	k5eAaImIp3nS	jít
zde	zde	k6eAd1	zde
ale	ale	k9	ale
také	také	k9	také
středočeský	středočeský	k2eAgInSc1d1	středočeský
krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Působnost	působnost	k1gFnSc1	působnost
okresních	okresní	k2eAgInPc2d1	okresní
soudů	soud	k1gInPc2	soud
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
deset	deset	k4xCc1	deset
soudů	soud	k1gInPc2	soud
obvodních	obvodní	k2eAgInPc2d1	obvodní
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pro	pro	k7c4	pro
pražské	pražský	k2eAgNnSc4d1	Pražské
okolí	okolí	k1gNnSc4	okolí
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
klasické	klasický	k2eAgInPc1d1	klasický
soudy	soud	k1gInPc1	soud
okresní	okresní	k2eAgInPc1d1	okresní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc1	jeden
vícemandátový	vícemandátový	k2eAgInSc1d1	vícemandátový
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
jednomandátových	jednomandátový	k2eAgInPc2d1	jednomandátový
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
volební	volební	k2eAgInPc1d1	volební
obvody	obvod	k1gInPc1	obvod
částečně	částečně	k6eAd1	částečně
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
městským	městský	k2eAgInPc3d1	městský
deseti	deset	k4xCc7	deset
obvodům	obvod	k1gInPc3	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
významnou	významný	k2eAgFnSc7d1	významná
křižovatkou	křižovatka	k1gFnSc7	křižovatka
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
dopravní	dopravní	k2eAgFnSc4d1	dopravní
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
železniční	železniční	k2eAgInSc1d1	železniční
uzel	uzel	k1gInSc1	uzel
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
dálkové	dálkový	k2eAgFnSc2d1	dálková
i	i	k8xC	i
příměstské	příměstský	k2eAgFnSc2d1	příměstská
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
pražská	pražský	k2eAgNnPc1d1	Pražské
nákladová	nákladový	k2eAgNnPc1d1	nákladové
nádraží	nádraží	k1gNnPc1	nádraží
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
útlumu	útlum	k1gInSc6	útlum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
Uhříněvsi	Uhříněves	k1gFnSc6	Uhříněves
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
největší	veliký	k2eAgNnSc1d3	veliký
kontejnerové	kontejnerový	k2eAgNnSc1d1	kontejnerové
překladiště	překladiště	k1gNnSc1	překladiště
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
(	(	kIx(	(
<g/>
do	do	k7c2	do
r.	r.	kA	r.
2012	[number]	k4	2012
Praha-Ruzyně	Praha-Ruzyně	k1gFnPc1	Praha-Ruzyně
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
pražským	pražský	k2eAgNnSc7d1	Pražské
letištěm	letiště	k1gNnSc7	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
zprvu	zprvu	k6eAd1	zprvu
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
drožkami	drožka	k1gFnPc7	drožka
a	a	k8xC	a
omnibusy	omnibus	k1gInPc7	omnibus
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
vznikala	vznikat	k5eAaImAgFnS	vznikat
síť	síť	k1gFnSc1	síť
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
elektrická	elektrický	k2eAgFnSc1d1	elektrická
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
soukromé	soukromý	k2eAgFnSc2d1	soukromá
městské	městský	k2eAgFnSc2d1	městská
dráhy	dráha	k1gFnSc2	dráha
vykoupilo	vykoupit	k5eAaPmAgNnS	vykoupit
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
v	v	k7c6	v
letech	let	k1gInPc6	let
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
začlenily	začlenit	k5eAaPmAgFnP	začlenit
do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
i	i	k8xC	i
autobusy	autobus	k1gInPc4	autobus
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
nabyly	nabýt	k5eAaPmAgFnP	nabýt
důrazu	důraz	k1gInSc2	důraz
snahy	snaha	k1gFnSc2	snaha
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
městskou	městský	k2eAgFnSc4d1	městská
a	a	k8xC	a
příměstskou	příměstský	k2eAgFnSc4d1	příměstská
dopravu	doprava	k1gFnSc4	doprava
více	hodně	k6eAd2	hodně
i	i	k9	i
železnici	železnice	k1gFnSc4	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Doplňkovou	doplňkový	k2eAgFnSc4d1	doplňková
roli	role	k1gFnSc4	role
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
lanovka	lanovka	k1gFnSc1	lanovka
<g/>
;	;	kIx,	;
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgInP	být
obnoveny	obnoven	k2eAgInPc1d1	obnoven
tři	tři	k4xCgInPc1	tři
malé	malý	k2eAgInPc1d1	malý
osobní	osobní	k2eAgInPc1d1	osobní
přívozy	přívoz	k1gInPc1	přívoz
a	a	k8xC	a
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
přívozy	přívoz	k1gInPc1	přívoz
a	a	k8xC	a
lanovka	lanovka	k1gFnSc1	lanovka
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
do	do	k7c2	do
systému	systém	k1gInSc2	systém
Pražské	pražský	k2eAgFnSc2d1	Pražská
integrované	integrovaný	k2eAgFnSc2d1	integrovaná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Předplatné	předplatný	k2eAgNnSc1d1	předplatné
jízdné	jízdné	k1gNnSc1	jízdné
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
formou	forma	k1gFnSc7	forma
nákupu	nákup	k1gInSc2	nákup
tištěných	tištěný	k2eAgInPc2d1	tištěný
kupónů	kupón	k1gInPc2	kupón
na	na	k7c4	na
pevné	pevný	k2eAgNnSc4d1	pevné
časové	časový	k2eAgNnSc4d1	časové
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
měsíční	měsíční	k2eAgFnSc1d1	měsíční
<g/>
,	,	kIx,	,
čtvrtletní	čtvrtletní	k2eAgFnSc1d1	čtvrtletní
<g/>
,	,	kIx,	,
roční	roční	k2eAgFnSc1d1	roční
<g/>
)	)	kIx)	)
a	a	k8xC	a
systémem	systém	k1gInSc7	systém
čipových	čipový	k2eAgFnPc2d1	čipová
karet	kareta	k1gFnPc2	kareta
Lítačka	lítačka	k1gFnSc1	lítačka
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnPc1	ulice
a	a	k8xC	a
silnice	silnice	k1gFnPc1	silnice
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
vývoje	vývoj	k1gInSc2	vývoj
města	město	k1gNnSc2	město
postupně	postupně	k6eAd1	postupně
upravovány	upravovat	k5eAaImNgFnP	upravovat
a	a	k8xC	a
nové	nový	k2eAgFnPc1d1	nová
přistavovány	přistavován	k2eAgFnPc1d1	přistavována
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
dálkovými	dálkový	k2eAgInPc7d1	dálkový
silničními	silniční	k2eAgInPc7d1	silniční
tahy	tah	k1gInPc7	tah
využívanými	využívaný	k2eAgInPc7d1	využívaný
také	také	k6eAd1	také
k	k	k7c3	k
příměstské	příměstský	k2eAgFnSc3d1	příměstská
dopravě	doprava	k1gFnSc3	doprava
jsou	být	k5eAaImIp3nP	být
dálnice	dálnice	k1gFnPc1	dálnice
D1	D1	k1gMnPc2	D1
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
D5	D5	k1gFnSc1	D5
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
D8	D8	k1gFnSc1	D8
(	(	kIx(	(
<g/>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
a	a	k8xC	a
D11	D11	k1gFnSc1	D11
(	(	kIx(	(
<g/>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
budován	budovat	k5eAaImNgInS	budovat
Pražský	pražský	k2eAgInSc1d1	pražský
okruh	okruh	k1gInSc1	okruh
a	a	k8xC	a
Městský	městský	k2eAgInSc1d1	městský
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
kontroverzní	kontroverzní	k2eAgInSc1d1	kontroverzní
tunelový	tunelový	k2eAgInSc1d1	tunelový
komplex	komplex	k1gInSc1	komplex
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
.	.	kIx.	.
</s>
<s>
Dostavba	dostavba	k1gFnSc1	dostavba
obou	dva	k4xCgInPc2	dva
okruhů	okruh	k1gInPc2	okruh
nabírá	nabírat	k5eAaImIp3nS	nabírat
zpoždění	zpoždění	k1gNnSc1	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Automobilová	automobilový	k2eAgFnSc1d1	automobilová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
a	a	k8xC	a
v	v	k7c6	v
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
čtvrtích	čtvrt	k1gFnPc6	čtvrt
je	být	k5eAaImIp3nS	být
regulována	regulovat	k5eAaImNgFnS	regulovat
zejména	zejména	k9	zejména
systémem	systém	k1gInSc7	systém
zón	zóna	k1gFnPc2	zóna
placeného	placený	k2eAgNnSc2d1	placené
stání	stání	k1gNnSc2	stání
<g/>
,	,	kIx,	,
především	především	k9	především
u	u	k7c2	u
okrajových	okrajový	k2eAgFnPc2d1	okrajová
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
jsou	být	k5eAaImIp3nP	být
zřizována	zřizován	k2eAgNnPc1d1	zřizováno
odstavná	odstavný	k2eAgNnPc1d1	odstavné
parkoviště	parkoviště	k1gNnPc1	parkoviště
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R.	R.	kA	R.
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
koncepce	koncepce	k1gFnSc1	koncepce
ucelené	ucelený	k2eAgFnSc2d1	ucelená
pražské	pražský	k2eAgFnSc2d1	Pražská
sítě	síť	k1gFnSc2	síť
cyklistických	cyklistický	k2eAgFnPc2d1	cyklistická
tras	trasa	k1gFnPc2	trasa
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
některých	některý	k3yIgFnPc2	některý
ulic	ulice	k1gFnPc2	ulice
či	či	k8xC	či
samostatně	samostatně	k6eAd1	samostatně
mimo	mimo	k6eAd1	mimo
ulice	ulice	k1gFnPc1	ulice
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgFnP	vybudovat
stezky	stezka	k1gFnPc1	stezka
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
pohyb	pohyb	k1gInSc4	pohyb
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
po	po	k7c6	po
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
bylo	být	k5eAaImAgNnS	být
163	[number]	k4	163
km	km	kA	km
chráněných	chráněný	k2eAgFnPc2d1	chráněná
tras	trasa	k1gFnPc2	trasa
vedených	vedený	k2eAgFnPc2d1	vedená
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
automobilové	automobilový	k2eAgFnSc2d1	automobilová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
41	[number]	k4	41
km	km	kA	km
cyklopruhů	cyklopruh	k1gInPc2	cyklopruh
a	a	k8xC	a
20,3	[number]	k4	20,3
km	km	kA	km
jednosměrných	jednosměrný	k2eAgFnPc2d1	jednosměrná
ulic	ulice	k1gFnPc2	ulice
obousměrných	obousměrný	k2eAgFnPc2d1	obousměrná
pro	pro	k7c4	pro
kola	kolo	k1gNnPc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnSc1d1	osobní
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
převážně	převážně	k6eAd1	převážně
rekreační	rekreační	k2eAgInSc4d1	rekreační
a	a	k8xC	a
turistický	turistický	k2eAgInSc4d1	turistický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Radotínský	radotínský	k2eAgInSc1d1	radotínský
přístav	přístav	k1gInSc1	přístav
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nákladní	nákladní	k2eAgFnSc4d1	nákladní
dopravu	doprava	k1gFnSc4	doprava
po	po	k7c6	po
Vltavě	Vltava	k1gFnSc6	Vltava
na	na	k7c4	na
labskou	labský	k2eAgFnSc4d1	Labská
vodní	vodní	k2eAgFnSc4d1	vodní
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
sypkých	sypký	k2eAgFnPc2d1	sypká
stavebních	stavební	k2eAgFnPc2d1	stavební
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
rozměrných	rozměrný	k2eAgInPc2d1	rozměrný
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
pár	pár	k4xCyI	pár
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vlaků	vlak	k1gInPc2	vlak
EuroCity	EuroCita	k1gFnSc2	EuroCita
společností	společnost	k1gFnPc2	společnost
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
Polskie	Polskie	k1gFnSc2	Polskie
Koleje	kolej	k1gFnSc2	kolej
Państwowe	Państwow	k1gFnSc2	Państwow
(	(	kIx(	(
<g/>
k	k	k7c3	k
r.	r.	kA	r.
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
Bohumín	Bohumín	k1gInSc1	Bohumín
–	–	k?	–
Katowice	Katowic	k1gMnSc2	Katowic
–	–	k?	–
Warszawa	Warszawus	k1gMnSc2	Warszawus
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Karlova	Karlův	k2eAgFnSc1d1	Karlova
univerzita	univerzita	k1gFnSc1	univerzita
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
České	český	k2eAgNnSc1d1	české
vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
chemicko-technologická	chemickoechnologický	k2eAgFnSc1d1	chemicko-technologická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
VŠCHT	VŠCHT	kA	VŠCHT
<g/>
)	)	kIx)	)
založena	založen	k2eAgFnSc1d1	založena
1920	[number]	k4	1920
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
VŠE	všechen	k3xTgNnSc1	všechen
<g/>
)	)	kIx)	)
založena	založen	k2eAgFnSc1d1	založena
1953	[number]	k4	1953
<g />
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
ČZU	ČZU	kA	ČZU
<g/>
)	)	kIx)	)
založena	založen	k2eAgFnSc1d1	založena
1906	[number]	k4	1906
<g/>
/	/	kIx~	/
<g/>
1952	[number]	k4	1952
Akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
AVU	AVU	kA	AVU
<g/>
)	)	kIx)	)
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1	uměleckoprůmyslová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
VŠUP	VŠUP	kA	VŠUP
<g/>
)	)	kIx)	)
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
Akademie	akademie	k1gFnSc2	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
AMU	AMU	kA	AMU
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
Policejní	policejní	k2eAgFnSc2d1	policejní
akademie	akademie	k1gFnSc2	akademie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
University	universita	k1gFnSc2	universita
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
in	in	k?	in
Prague	Prague	k1gInSc1	Prague
(	(	kIx(	(
<g/>
UNYP	UNYP	kA	UNYP
<g/>
)	)	kIx)	)
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
International	International	k1gMnPc1	International
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Prague	Prague	k1gNnSc4	Prague
(	(	kIx(	(
<g/>
ISP	ISP	kA	ISP
<g/>
)	)	kIx)	)
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
Anglo-American	Anglo-American	k1gInSc4	Anglo-American
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
AAU	AAU	kA	AAU
<g/>
)	)	kIx)	)
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
University	universita	k1gFnSc2	universita
of	of	k?	of
Northern	Northern	k1gMnSc1	Northern
Virginia	Virginium	k1gNnSc2	Virginium
in	in	k?	in
<g />
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
(	(	kIx(	(
<g/>
UNVA	UNVA	kA	UNVA
<g/>
)	)	kIx)	)
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
Architectural	Architectural	k1gFnPc2	Architectural
Institute	institut	k1gInSc5	institut
in	in	k?	in
Prague	Pragu	k1gMnSc4	Pragu
(	(	kIx(	(
<g/>
ARCHIP	ARCHIP	kA	ARCHIP
<g/>
)	)	kIx)	)
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
finanční	finanční	k2eAgFnSc2d1	finanční
a	a	k8xC	a
správní	správní	k2eAgFnSc2d1	správní
(	(	kIx(	(
<g/>
VSFS	VSFS	kA	VSFS
<g/>
)	)	kIx)	)
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
univerzita	univerzita	k1gFnSc1	univerzita
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
MUP	MUP	kA	MUP
<g/>
)	)	kIx)	)
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
Prague	Pragu	k1gFnSc2	Pragu
College	College	k1gNnPc2	College
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g />
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc4	York
University	universita	k1gFnSc2	universita
Instituto	Institut	k2eAgNnSc1d1	Instituto
Camõ	Camõ	k1gFnPc4	Camõ
Goethe-Institut	Goethe-Institut	k1gInSc4	Goethe-Institut
Česko	Česko	k1gNnSc1	Česko
Instituto	Institut	k2eAgNnSc1d1	Instituto
Cervantes	Cervantes	k1gInSc4	Cervantes
British	British	k1gInSc4	British
Council	Councila	k1gFnPc2	Councila
Alliance	Allianec	k1gInSc2	Allianec
française	française	k1gFnSc1	française
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
významných	významný	k2eAgInPc2d1	významný
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
týmů	tým	k1gInPc2	tým
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
FK	FK	kA	FK
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Bohemians	Bohemians	k1gInSc1	Bohemians
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
FK	FK	kA	FK
Viktoria	Viktoria	k1gFnSc1	Viktoria
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
,	,	kIx,	,
FK	FK	kA	FK
Loko	Loko	k1gNnSc4	Loko
Vltavín	vltavín	k1gInSc1	vltavín
a	a	k8xC	a
FK	FK	kA	FK
Bohemians	Bohemians	k1gInSc1	Bohemians
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dále	daleko	k6eAd2	daleko
hokejových	hokejový	k2eAgInPc2d1	hokejový
týmů	tým	k1gInPc2	tým
HC	HC	kA	HC
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
HC	HC	kA	HC
Lev	lev	k1gInSc1	lev
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Strahovský	strahovský	k2eAgInSc4d1	strahovský
stadion	stadion	k1gInSc4	stadion
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
stadion	stadion	k1gInSc4	stadion
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
využíván	využívat	k5eAaImNgInS	využívat
jen	jen	k9	jen
jako	jako	k8xC	jako
výcvikové	výcvikový	k2eAgNnSc1d1	výcvikové
centrum	centrum	k1gNnSc1	centrum
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
česká	český	k2eAgFnSc1d1	Česká
multifunkční	multifunkční	k2eAgFnSc1d1	multifunkční
hala	hala	k1gFnSc1	hala
je	být	k5eAaImIp3nS	být
O2	O2	k1gFnSc7	O2
arena	areno	k1gNnSc2	areno
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
významný	významný	k2eAgInSc1d1	významný
Stadion	stadion	k1gInSc1	stadion
Letná	Letná	k1gFnSc1	Letná
je	být	k5eAaImIp3nS	být
domovským	domovský	k2eAgInSc7d1	domovský
stadionem	stadion	k1gInSc7	stadion
AC	AC	kA	AC
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc4d1	moderní
Eden	Eden	k1gInSc4	Eden
Aréna	aréna	k1gFnSc1	aréna
ve	v	k7c6	v
Vršovicích	Vršovice	k1gFnPc6	Vršovice
sídlem	sídlo	k1gNnSc7	sídlo
SK	Sk	kA	Sk
Slavie	slavie	k1gFnSc2	slavie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
tenisový	tenisový	k2eAgInSc1d1	tenisový
areál	areál	k1gInSc1	areál
Štvanice	Štvanice	k1gFnSc1	Štvanice
patří	patřit	k5eAaImIp3nS	patřit
Českému	český	k2eAgInSc3d1	český
tenisovému	tenisový	k2eAgInSc3d1	tenisový
svazu	svaz	k1gInSc3	svaz
<g/>
,	,	kIx,	,
kurty	kurt	k1gInPc4	kurt
pak	pak	k8xC	pak
nejstaršímu	starý	k2eAgInSc3d3	nejstarší
českému	český	k2eAgInSc3d1	český
tenisovému	tenisový	k2eAgInSc3d1	tenisový
klubu	klub	k1gInSc3	klub
I.	I.	kA	I.
ČLTK	ČLTK	kA	ČLTK
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
golfové	golfový	k2eAgNnSc4d1	golfové
tréninkové	tréninkový	k2eAgNnSc4d1	tréninkové
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c6	v
ČR	ČR	kA	ČR
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
Hodkovičkách	Hodkovička	k1gFnPc6	Hodkovička
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc4d1	další
také	také	k9	také
ve	v	k7c6	v
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
,	,	kIx,	,
Hostivaři	Hostivař	k1gFnSc6	Hostivař
<g/>
,	,	kIx,	,
Motole	Motol	k1gInSc6	Motol
a	a	k8xC	a
nově	nově	k6eAd1	nově
také	také	k9	také
na	na	k7c6	na
Černém	černý	k2eAgInSc6d1	černý
Mostě	most	k1gInSc6	most
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každoročním	každoroční	k2eAgFnPc3d1	každoroční
sportovním	sportovní	k2eAgFnPc3d1	sportovní
událostem	událost	k1gFnPc3	událost
také	také	k9	také
patří	patřit	k5eAaImIp3nP	patřit
Pražský	pražský	k2eAgInSc4d1	pražský
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
maraton	maraton	k1gInSc4	maraton
(	(	kIx(	(
<g/>
PIM	PIM	kA	PIM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Memoriál	memoriál	k1gInSc4	memoriál
Josefa	Josef	k1gMnSc2	Josef
Odložila	odložit	k5eAaPmAgNnP	odložit
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
atletický	atletický	k2eAgInSc4d1	atletický
mítink	mítink	k1gInSc4	mítink
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mystic	Mystice	k1gFnPc2	Mystice
Sk	Sk	kA	Sk
<g/>
8	[number]	k4	8
cup	cup	k1gInSc4	cup
(	(	kIx(	(
<g/>
světový	světový	k2eAgInSc4d1	světový
skateboardový	skateboardový	k2eAgInSc4d1	skateboardový
pohár	pohár	k1gInSc4	pohár
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Frisbeer	Frisbeer	k1gMnSc1	Frisbeer
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
frisbee	frisbee	k1gInSc1	frisbee
turnaj	turnaj	k1gInSc1	turnaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zabruslit	zabruslit	k5eAaPmF	zabruslit
na	na	k7c6	na
zimních	zimní	k2eAgInPc6d1	zimní
stadionech	stadion	k1gInPc6	stadion
v	v	k7c6	v
Ice	Ice	k1gFnSc6	Ice
Areně	Areeň	k1gFnSc2	Areeň
v	v	k7c6	v
Letňanech	Letňan	k1gMnPc6	Letňan
<g/>
,	,	kIx,	,
Nikolajce	Nikolajka	k1gFnSc6	Nikolajka
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
,	,	kIx,	,
stadionu	stadion	k1gInSc6	stadion
Kobra	kobra	k1gFnSc1	kobra
v	v	k7c6	v
Braníku	Braník	k1gInSc6	Braník
<g/>
,	,	kIx,	,
ZS	ZS	kA	ZS
Hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgFnSc6d1	Malé
sportovní	sportovní	k2eAgFnSc6d1	sportovní
hale	hala	k1gFnSc6	hala
Incheba	Incheba	k1gFnSc1	Incheba
na	na	k7c6	na
Výstavišti	výstaviště	k1gNnSc6	výstaviště
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
a	a	k8xC	a
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
venkovních	venkovní	k2eAgNnPc6d1	venkovní
kluzištích	kluziště	k1gNnPc6	kluziště
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
bazény	bazén	k1gInPc4	bazén
a	a	k8xC	a
akvaparky	akvapark	k1gInPc4	akvapark
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Plavecký	plavecký	k2eAgInSc4d1	plavecký
stadion	stadion	k1gInSc4	stadion
Podolí	Podolí	k1gNnSc2	Podolí
<g/>
,	,	kIx,	,
Plavecký	plavecký	k2eAgInSc4d1	plavecký
stadion	stadion	k1gInSc4	stadion
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Plavecký	plavecký	k2eAgInSc1d1	plavecký
areál	areál	k1gInSc1	areál
Šutka	šutka	k1gFnSc1	šutka
<g/>
,	,	kIx,	,
Aquapark	aquapark	k1gInSc1	aquapark
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
Aquacentrum	Aquacentrum	k1gNnSc1	Aquacentrum
Letňany	Letňan	k1gMnPc4	Letňan
a	a	k8xC	a
PSA	pes	k1gMnSc4	pes
Hloubětín	Hloubětín	k1gInSc4	Hloubětín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
přisolovaný	přisolovaný	k2eAgInSc1d1	přisolovaný
mořskou	mořský	k2eAgFnSc7d1	mořská
solí	sůl	k1gFnSc7	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
(	(	kIx(	(
<g/>
asi	asi	k9	asi
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
největší	veliký	k2eAgInSc1d3	veliký
český	český	k2eAgInSc1d1	český
aquapark	aquapark	k1gInSc1	aquapark
Aquapalace	Aquapalace	k1gFnSc1	Aquapalace
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Vltavy	Vltava	k1gFnSc2	Vltava
v	v	k7c6	v
Podolí	Podolí	k1gNnSc6	Podolí
je	být	k5eAaImIp3nS	být
také	také	k9	také
písečná	písečný	k2eAgFnSc1d1	písečná
pláž	pláž	k1gFnSc1	pláž
Žluté	žlutý	k2eAgFnPc1d1	žlutá
lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
emigranti	emigrant	k1gMnPc1	emigrant
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
několik	několik	k4yIc1	několik
nově	nově	k6eAd1	nově
založených	založený	k2eAgNnPc2d1	založené
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
New	New	k1gFnPc2	New
Prague	Prague	k1gNnSc2	Prague
<g/>
,	,	kIx,	,
Minnesota	Minnesota	k1gFnSc1	Minnesota
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
Prague	Pragu	k1gFnSc2	Pragu
<g/>
,	,	kIx,	,
Oklahoma	Oklahomum	k1gNnSc2	Oklahomum
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnPc1d1	americká
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
Prague	Pragu	k1gFnSc2	Pragu
<g/>
,	,	kIx,	,
Nebraska	Nebrask	k1gInSc2	Nebrask
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
například	například	k6eAd1	například
správce	správce	k1gMnSc1	správce
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
internetové	internetový	k2eAgFnSc2d1	internetová
domény	doména	k1gFnSc2	doména
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
sdružení	sdružení	k1gNnSc2	sdružení
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
<g/>
NIC	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
také	také	k9	také
sídlo	sídlo	k1gNnSc4	sídlo
vyhledávače	vyhledávač	k1gMnSc2	vyhledávač
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
českou	český	k2eAgFnSc4d1	Česká
pobočku	pobočka	k1gFnSc4	pobočka
Google	Googl	k1gMnSc4	Googl
<g/>
,	,	kIx,	,
vývojáře	vývojář	k1gMnSc4	vývojář
Skype	Skyp	k1gInSc5	Skyp
nebo	nebo	k8xC	nebo
aukčního	aukční	k2eAgInSc2d1	aukční
serveru	server	k1gInSc2	server
eBay	eBaa	k1gFnSc2	eBaa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
mediálních	mediální	k2eAgInPc2d1	mediální
domů	dům	k1gInPc2	dům
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
vydavatelství	vydavatelství	k1gNnSc3	vydavatelství
MAFRA	MAFRA	kA	MAFRA
<g/>
,	,	kIx,	,
Economia	Economia	k1gFnSc1	Economia
<g/>
,	,	kIx,	,
Televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
Televize	televize	k1gFnSc1	televize
Prima	prima	k1gFnSc1	prima
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
média	médium	k1gNnPc1	médium
<g/>
.	.	kIx.	.
</s>
<s>
Technologicky	technologicky	k6eAd1	technologicky
je	být	k5eAaImIp3nS	být
Praha	Praha	k1gFnSc1	Praha
místem	místem	k6eAd1	místem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
fyzicky	fyzicky	k6eAd1	fyzicky
běží	běžet	k5eAaImIp3nS	běžet
řada	řada	k1gFnSc1	řada
serverů	server	k1gInPc2	server
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
velkých	velký	k2eAgFnPc2d1	velká
datacenter	datacentra	k1gFnPc2	datacentra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
jmenována	jmenován	k2eAgFnSc1d1	jmenována
čestnými	čestný	k2eAgMnPc7d1	čestný
občany	občan	k1gMnPc7	občan
Prahy	Praha	k1gFnSc2	Praha
řada	řada	k1gFnSc1	řada
osobností	osobnost	k1gFnPc2	osobnost
<g/>
;	;	kIx,	;
posledním	poslední	k2eAgMnSc7d1	poslední
adeptem	adept	k1gMnSc7	adept
je	být	k5eAaImIp3nS	být
režisér	režisér	k1gMnSc1	režisér
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
.	.	kIx.	.
</s>
