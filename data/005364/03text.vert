<s>
Transkripce	transkripce	k1gFnSc1	transkripce
v	v	k7c6	v
lingvistice	lingvistika	k1gFnSc6	lingvistika
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
:	:	kIx,	:
zápis	zápis	k1gInSc1	zápis
řeči	řeč	k1gFnSc2	řeč
pomocí	pomocí	k7c2	pomocí
vhodných	vhodný	k2eAgInPc2d1	vhodný
zvolených	zvolený	k2eAgInPc2d1	zvolený
znaků	znak	k1gInPc2	znak
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
východiskem	východisko	k1gNnSc7	východisko
je	být	k5eAaImIp3nS	být
zvuková	zvukový	k2eAgFnSc1d1	zvuková
podoba	podoba	k1gFnSc1	podoba
jazyka	jazyk	k1gInSc2	jazyk
přepis	přepis	k1gInSc1	přepis
textu	text	k1gInSc2	text
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
písma	písmo	k1gNnSc2	písmo
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
např.	např.	kA	např.
pro	pro	k7c4	pro
přepis	přepis	k1gInSc4	přepis
výslovnosti	výslovnost	k1gFnSc2	výslovnost
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
jmen	jméno	k1gNnPc2	jméno
ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
<g/>
,	,	kIx,	,
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
názvů	název	k1gInPc2	název
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
a	a	k8xC	a
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
či	či	k8xC	či
pro	pro	k7c4	pro
přepis	přepis	k1gInSc4	přepis
výslovnosti	výslovnost	k1gFnSc2	výslovnost
v	v	k7c6	v
lingvistických	lingvistický	k2eAgInPc6d1	lingvistický
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
výslovnosti	výslovnost	k1gFnPc4	výslovnost
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
konkrétní	konkrétní	k2eAgNnSc1d1	konkrétní
schéma	schéma	k1gNnSc1	schéma
transkripce	transkripce	k1gFnSc2	transkripce
odráží	odrážet	k5eAaImIp3nS	odrážet
nejen	nejen	k6eAd1	nejen
zdrojové	zdrojový	k2eAgNnSc4d1	zdrojové
a	a	k8xC	a
cílové	cílový	k2eAgNnSc4d1	cílové
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zdrojový	zdrojový	k2eAgInSc4d1	zdrojový
a	a	k8xC	a
cílový	cílový	k2eAgInSc4d1	cílový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
výslovnost	výslovnost	k1gFnSc4	výslovnost
také	také	k9	také
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
transkripci	transkripce	k1gFnSc4	transkripce
od	od	k7c2	od
transliterace	transliterace	k1gFnSc2	transliterace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zachovat	zachovat	k5eAaPmF	zachovat
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
kompletní	kompletní	k2eAgFnSc4d1	kompletní
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
původním	původní	k2eAgInSc6d1	původní
pravopisu	pravopis	k1gInSc6	pravopis
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
písmu	písmo	k1gNnSc6	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
zvukové	zvukový	k2eAgFnSc2d1	zvuková
stránky	stránka	k1gFnSc2	stránka
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
ve	v	k7c6	v
fonetice	fonetika	k1gFnSc6	fonetika
a	a	k8xC	a
fonologii	fonologie	k1gFnSc6	fonologie
používají	používat	k5eAaImIp3nP	používat
následující	následující	k2eAgInPc1d1	následující
typy	typ	k1gInPc1	typ
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
:	:	kIx,	:
Fonetická	fonetický	k2eAgFnSc1d1	fonetická
transkripce	transkripce	k1gFnSc1	transkripce
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
co	co	k3yQnSc4	co
nejpřesnější	přesný	k2eAgInSc1d3	nejpřesnější
záznam	záznam	k1gInSc1	záznam
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
,	,	kIx,	,
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
hranatých	hranatý	k2eAgFnPc2d1	hranatá
závorek	závorka	k1gFnPc2	závorka
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
kɲ	kɲ	k?	kɲ
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
kňíška	kňíška	k1gFnSc1	kňíška
<g/>
]	]	kIx)	]
Fonetická	fonetický	k2eAgFnSc1d1	fonetická
transkripce	transkripce	k1gFnSc1	transkripce
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
například	například	k6eAd1	například
v	v	k7c6	v
dvojjazyčných	dvojjazyčný	k2eAgInPc6d1	dvojjazyčný
slovnících	slovník	k1gInPc6	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Fonologická	fonologický	k2eAgFnSc1d1	fonologická
transkripce	transkripce	k1gFnSc1	transkripce
je	být	k5eAaImIp3nS	být
přepis	přepis	k1gInSc4	přepis
textu	text	k1gInSc2	text
pomocí	pomocí	k7c2	pomocí
fonémů	foném	k1gInPc2	foném
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
mezi	mezi	k7c4	mezi
lomítka	lomítko	k1gNnPc4	lomítko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
jednotlivého	jednotlivý	k2eAgInSc2d1	jednotlivý
fonému	foném	k1gInSc2	foném
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
znak	znak	k1gInSc1	znak
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
alofonu	alofon	k1gInSc2	alofon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
způsob	způsob	k1gInSc4	způsob
výslovnosti	výslovnost	k1gFnSc2	výslovnost
<g/>
:	:	kIx,	:
/	/	kIx~	/
<g/>
kɲ	kɲ	k?	kɲ
<g/>
/	/	kIx~	/
nebo	nebo	k8xC	nebo
/	/	kIx~	/
<g/>
kňížka	kňížka	k1gFnSc1	kňížka
<g/>
/	/	kIx~	/
</s>
<s>
Morfonologická	morfonologický	k2eAgFnSc1d1	morfonologická
transkripce	transkripce	k1gFnSc1	transkripce
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
stavbu	stavba	k1gFnSc4	stavba
slov	slovo	k1gNnPc2	slovo
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
morfonémů	morfoném	k1gInPc2	morfoném
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
mezi	mezi	k7c4	mezi
složené	složený	k2eAgFnPc4d1	složená
závorky	závorka	k1gFnPc4	závorka
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
způsob	způsob	k1gInSc4	způsob
morfonologické	morfonologický	k2eAgFnSc2d1	morfonologická
transkripce	transkripce	k1gFnSc2	transkripce
kmenového	kmenový	k2eAgInSc2d1	kmenový
morfému	morfém	k1gInSc2	morfém
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
střídání	střídání	k1gNnSc3	střídání
fonémů	foném	k1gInPc2	foném
v	v	k7c6	v
tvarech	tvar	k1gInPc6	tvar
knih-a	knihs	k1gMnSc2	knih-us
–	–	k?	–
kniz-e	kniz-	k1gMnSc2	kniz-
–	–	k?	–
kniž-ní	kniží	k1gMnSc1	kniž-ní
–	–	k?	–
kníž-ka	kníža	k1gMnSc1	kníž-ka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
příkladu	příklad	k1gInSc6	příklad
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
strídání	strídání	k1gNnSc3	strídání
hlásek	hláska	k1gFnPc2	hláska
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dvou	dva	k4xCgInPc2	dva
morfonémů	morfoném	k1gInPc2	morfoném
<g/>
:	:	kIx,	:
kɲ	kɲ	k?	kɲ
<g/>
{	{	kIx(	{
<g/>
ɪ	ɪ	k?	ɪ
<g/>
,	,	kIx,	,
iː	iː	k?	iː
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
ɦ	ɦ	k?	ɦ
<g/>
,	,	kIx,	,
z	z	k0	z
<g/>
,	,	kIx,	,
ʒ	ʒ	k?	ʒ
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
nebo	nebo	k8xC	nebo
kň-	kň-	k?	kň-
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
,	,	kIx,	,
í	í	k0	í
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
z	z	k0	z
<g/>
,	,	kIx,	,
ž	ž	k?	ž
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
třeba	třeba	k6eAd1	třeba
odlišit	odlišit	k5eAaPmF	odlišit
grafický	grafický	k2eAgInSc4d1	grafický
zápis	zápis	k1gInSc4	zápis
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
podle	podle	k7c2	podle
příslušných	příslušný	k2eAgNnPc2d1	příslušné
pravidel	pravidlo	k1gNnPc2	pravidlo
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
umístit	umístit	k5eAaPmF	umístit
text	text	k1gInSc4	text
mezi	mezi	k7c4	mezi
ostré	ostrý	k2eAgFnPc4d1	ostrá
závorky	závorka	k1gFnPc4	závorka
<g/>
:	:	kIx,	:
<knížka>
Pro	pro	k7c4	pro
přepis	přepis	k1gInSc4	přepis
zvukové	zvukový	k2eAgFnSc2d1	zvuková
stránky	stránka	k1gFnSc2	stránka
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
<g />
.	.	kIx.	.
</s>
<s>
znaky	znak	k1gInPc1	znak
fonetické	fonetický	k2eAgFnSc2d1	fonetická
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
fonetické	fonetický	k2eAgFnSc2d1	fonetická
abecedy	abeceda	k1gFnSc2	abeceda
(	(	kIx(	(
<g/>
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
americké	americký	k2eAgInPc1d1	americký
(	(	kIx(	(
<g/>
APN	APN	kA	APN
<g/>
,	,	kIx,	,
APA	APA	kA	APA
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
–	–	k?	–
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
–	–	k?	–
znaky	znak	k1gInPc4	znak
národní	národní	k2eAgFnSc2d1	národní
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
daný	daný	k2eAgInSc1d1	daný
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgInPc1d1	doplněný
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
o	o	k7c4	o
znaky	znak	k1gInPc4	znak
jiné	jiný	k2eAgInPc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
z	z	k7c2	z
(	(	kIx(	(
<g/>
ruské	ruský	k2eAgFnSc2d1	ruská
<g/>
)	)	kIx)	)
cyrilice	cyrilice	k1gFnSc2	cyrilice
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
<g/>
:	:	kIx,	:
Ч	Ч	k?	Ч
lze	lze	k6eAd1	lze
přepsat	přepsat	k5eAaPmF	přepsat
jako	jako	k8xS	jako
Čajkovskij	Čajkovskij	k1gFnSc4	Čajkovskij
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chaikovsky	Chaikovsky	k1gFnSc1	Chaikovsky
(	(	kIx(	(
<g/>
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tschaikowskij	Tschaikowskij	k1gFnSc1	Tschaikowskij
(	(	kIx(	(
<g/>
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tchaikovski	Tchaikovske	k1gFnSc4	Tchaikovske
(	(	kIx(	(
<g/>
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Běžná	běžný	k2eAgFnSc1d1	běžná
transkripce	transkripce	k1gFnSc1	transkripce
jména	jméno	k1gNnSc2	jméno
ruského	ruský	k2eAgNnSc2d1	ruské
města	město	k1gNnSc2	město
Е	Е	k?	Е
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
je	být	k5eAaImIp3nS	být
Jekatěrinburg	Jekatěrinburg	k1gInSc1	Jekatěrinburg
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
transliterace	transliterace	k1gFnSc1	transliterace
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
podle	podle	k7c2	podle
doporučení	doporučení	k1gNnSc2	doporučení
OSN	OSN	kA	OSN
je	být	k5eAaImIp3nS	být
Ekaterinburg	Ekaterinburg	k1gInSc1	Ekaterinburg
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
obvykle	obvykle	k6eAd1	obvykle
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
řecká	řecký	k2eAgNnPc4d1	řecké
písmena	písmeno	k1gNnPc4	písmeno
ι	ι	k?	ι
<g/>
,	,	kIx,	,
η	η	k?	η
a	a	k8xC	a
υ	υ	k?	υ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
všechna	všechen	k3xTgNnPc1	všechen
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
transliterace	transliterace	k1gFnSc1	transliterace
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
tato	tento	k3xDgNnPc4	tento
písmena	písmeno	k1gNnPc4	písmeno
rozlišit	rozlišit	k5eAaPmF	rozlišit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jako	jako	k8xC	jako
i-î-u	i-îs	k1gInSc2	i-î-us
nebo	nebo	k8xC	nebo
i-î-y	i-î	k1gFnSc2	i-î-a
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
původní	původní	k2eAgInSc4d1	původní
řecký	řecký	k2eAgInSc4d1	řecký
pravopis	pravopis	k1gInSc4	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Ču-mu-lang-ma	Čuuanga	k1gFnSc1	Ču-mu-lang-ma
feng	fenga	k1gFnPc2	fenga
nebo	nebo	k8xC	nebo
Zhū	Zhū	k1gFnPc1	Zhū
fē	fē	k?	fē
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgInPc4d1	možný
přepisy	přepis	k1gInPc4	přepis
čínského	čínský	k2eAgNnSc2d1	čínské
珠	珠	k?	珠
(	(	kIx(	(
<g/>
místní	místní	k2eAgInSc1d1	místní
název	název	k1gInSc1	název
pro	pro	k7c4	pro
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc4	Everest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chuang-che	Chuanghe	k1gNnSc1	Chuang-che
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
přepis	přepis	k1gInSc4	přepis
názvu	název	k1gInSc2	název
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
řeka	řeka	k1gFnSc1	řeka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
anglické	anglický	k2eAgFnSc2d1	anglická
výslovnosti	výslovnost	k1gFnSc2	výslovnost
(	(	kIx(	(
<g/>
pra	přít	k5eAaImSgMnS	přít
<g/>
:	:	kIx,	:
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
přepíše	přepsat	k5eAaPmIp3nS	přepsat
do	do	k7c2	do
čínštiny	čínština	k1gFnSc2	čínština
foneticky	foneticky	k6eAd1	foneticky
jako	jako	k8xC	jako
布	布	k?	布
(	(	kIx(	(
<g/>
a	a	k8xC	a
po	po	k7c6	po
zpětné	zpětný	k2eAgFnSc6d1	zpětná
transkripci	transkripce	k1gFnSc6	transkripce
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
dostaneme	dostat	k5eAaPmIp1nP	dostat
Bù	Bù	k1gFnPc1	Bù
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
Pu-la-ke	Puae	k1gFnSc1	Pu-la-ke
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dilí	Dilí	k1gMnPc1	Dilí
nebo	nebo	k8xC	nebo
Dillī	Dillī	k1gMnPc1	Dillī
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgFnPc4d1	možná
transkripce	transkripce	k1gFnPc4	transkripce
indického	indický	k2eAgMnSc2d1	indický
द	द	k?	द
<g/>
ि	ि	k?	ि
<g/>
ल	ल	k?	ल
<g/>
्	्	k?	्
<g/>
ल	ल	k?	ल
<g/>
ी	ी	k?	ी
(	(	kIx(	(
<g/>
název	název	k1gInSc4	název
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Indie	Indie	k1gFnSc2	Indie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
transkripce	transkripce	k1gFnSc1	transkripce
i	i	k9	i
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
abecedami	abeceda	k1gFnPc7	abeceda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obě	dva	k4xCgFnPc1	dva
využívají	využívat	k5eAaPmIp3nP	využívat
stejné	stejný	k2eAgNnSc4d1	stejné
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bývají	bývat	k5eAaImIp3nP	bývat
technické	technický	k2eAgFnPc4d1	technická
obtíže	obtíž	k1gFnPc4	obtíž
se	s	k7c7	s
zadáváním	zadávání	k1gNnSc7	zadávání
<g/>
,	,	kIx,	,
zobrazováním	zobrazování	k1gNnSc7	zobrazování
a	a	k8xC	a
tiskem	tisk	k1gInSc7	tisk
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
cílovém	cílový	k2eAgInSc6d1	cílový
jazyce	jazyk	k1gInSc6	jazyk
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
též	též	k9	též
velký	velký	k2eAgInSc4d1	velký
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
výslovností	výslovnost	k1gFnSc7	výslovnost
určité	určitý	k2eAgFnSc2d1	určitá
skupiny	skupina	k1gFnSc2	skupina
znaků	znak	k1gInPc2	znak
ve	v	k7c6	v
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
a	a	k8xC	a
v	v	k7c6	v
cílovém	cílový	k2eAgInSc6d1	cílový
jazyku	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prvně	prvně	k?	prvně
jmenované	jmenovaný	k2eAgFnSc2d1	jmenovaná
kategorie	kategorie	k1gFnSc2	kategorie
patří	patřit	k5eAaImIp3nS	patřit
odstraňování	odstraňování	k1gNnSc4	odstraňování
či	či	k8xC	či
nahrazování	nahrazování	k1gNnSc4	nahrazování
cizokrajných	cizokrajný	k2eAgNnPc2d1	cizokrajné
diakritických	diakritický	k2eAgNnPc2d1	diakritické
znamének	znaménko	k1gNnPc2	znaménko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
è	è	k?	è
→	→	k?	→
e	e	k0	e
<g/>
,	,	kIx,	,
ñ	ñ	k?	ñ
→	→	k?	→
ň	ň	k?	ň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nahrazování	nahrazování	k1gNnSc1	nahrazování
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
specifické	specifický	k2eAgInPc1d1	specifický
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
nepodobají	podobat	k5eNaImIp3nP	podobat
se	se	k3xPyFc4	se
ostatním	ostatní	k2eAgNnPc3d1	ostatní
písmenům	písmeno	k1gNnPc3	písmeno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
islandské	islandský	k2eAgInPc1d1	islandský
ð	ð	k?	ð
a	a	k8xC	a
þ	þ	k?	þ
lze	lze	k6eAd1	lze
nahradit	nahradit	k5eAaPmF	nahradit
dh	dh	k?	dh
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
th	th	k?	th
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
kategorie	kategorie	k1gFnSc2	kategorie
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
příležitostné	příležitostný	k2eAgNnSc1d1	příležitostné
přepisování	přepisování	k1gNnSc1	přepisování
albánského	albánský	k2eAgMnSc2d1	albánský
Hoxha	Hoxh	k1gMnSc2	Hoxh
(	(	kIx(	(
<g/>
jméno	jméno	k1gNnSc1	jméno
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
albánského	albánský	k2eAgMnSc2d1	albánský
diktátora	diktátor	k1gMnSc2	diktátor
<g/>
)	)	kIx)	)
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jako	jako	k8xC	jako
Hodža	Hodža	k1gMnSc1	Hodža
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
abecedy	abeceda	k1gFnPc1	abeceda
založené	založený	k2eAgFnPc1d1	založená
na	na	k7c6	na
cyrilici	cyrilice	k1gFnSc6	cyrilice
používá	používat	k5eAaImIp3nS	používat
řada	řada	k1gFnSc1	řada
navzájem	navzájem	k6eAd1	navzájem
velmi	velmi	k6eAd1	velmi
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
transkripčních	transkripční	k2eAgNnPc2d1	transkripční
schémat	schéma	k1gNnPc2	schéma
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
výslovnosti	výslovnost	k1gFnSc2	výslovnost
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
transkripčních	transkripční	k2eAgNnPc2d1	transkripční
schémat	schéma	k1gNnPc2	schéma
z	z	k7c2	z
cyrilice	cyrilice	k1gFnSc2	cyrilice
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
v	v	k7c6	v
přepisu	přepis	k1gInSc6	přepis
následujících	následující	k2eAgInPc2d1	následující
znaků	znak	k1gInPc2	znak
<g/>
:	:	kIx,	:
Transkripce	transkripce	k1gFnSc1	transkripce
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
běžně	běžně	k6eAd1	běžně
dostupné	dostupný	k2eAgInPc1d1	dostupný
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
abecedách	abeceda	k1gFnPc6	abeceda
postavených	postavený	k2eAgMnPc2d1	postavený
na	na	k7c6	na
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
cílovým	cílový	k2eAgInSc7d1	cílový
jazykem	jazyk	k1gInSc7	jazyk
<g/>
:	:	kIx,	:
Uvedená	uvedený	k2eAgNnPc1d1	uvedené
pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
transkripci	transkripce	k1gFnSc4	transkripce
jmen	jméno	k1gNnPc2	jméno
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
nejednotné	jednotný	k2eNgNnSc1d1	nejednotné
přepisování	přepisování	k1gNnSc1	přepisování
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
názvů	název	k1gInPc2	název
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistické	lingvistický	k2eAgFnPc1d1	lingvistická
publikace	publikace	k1gFnPc1	publikace
obvykle	obvykle	k6eAd1	obvykle
nepoužívají	používat	k5eNaImIp3nP	používat
transkripci	transkripce	k1gFnSc4	transkripce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
transliteraci	transliterace	k1gFnSc4	transliterace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgInPc6d1	uvedený
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
českému	český	k2eAgInSc3d1	český
přepisu	přepis	k1gInSc3	přepis
(	(	kIx(	(
<g/>
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
však	však	k9	však
např.	např.	kA	např.
přepisem	přepis	k1gInSc7	přepis
х	х	k?	х
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
jedním	jeden	k4xCgInSc7	jeden
znakem	znak	k1gInSc7	znak
buď	buď	k8xC	buď
jako	jako	k9	jako
h	h	k?	h
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
českému	český	k2eAgInSc3d1	český
přepisu	přepis	k1gInSc3	přepis
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
doporučení	doporučení	k1gNnSc4	doporučení
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
vydané	vydaný	k2eAgFnPc4d1	vydaná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
návrhu	návrh	k1gInSc2	návrh
někdejší	někdejší	k2eAgFnSc2d1	někdejší
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
sjednotit	sjednotit	k5eAaPmF	sjednotit
přepis	přepis	k1gInSc4	přepis
ruských	ruský	k2eAgNnPc2d1	ruské
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
obyčejných	obyčejný	k2eAgFnPc2d1	obyčejná
<g/>
)	)	kIx)	)
žijících	žijící	k2eAgFnPc2d1	žijící
osob	osoba	k1gFnPc2	osoba
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
řídí	řídit	k5eAaImIp3nS	řídit
přepisem	přepis	k1gInSc7	přepis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
mají	mít	k5eAaImIp3nP	mít
uveden	uvést	k5eAaPmNgMnS	uvést
v	v	k7c6	v
cestovním	cestovní	k2eAgInSc6d1	cestovní
pase	pas	k1gInSc6	pas
<g/>
.	.	kIx.	.
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
úředníci	úředník	k1gMnPc1	úředník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pasy	pas	k1gInPc4	pas
vydávají	vydávat	k5eAaImIp3nP	vydávat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
při	při	k7c6	při
transkripci	transkripce	k1gFnSc6	transkripce
neřídí	řídit	k5eNaImIp3nS	řídit
jednotnými	jednotný	k2eAgNnPc7d1	jednotné
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
lze	lze	k6eAd1	lze
však	však	k9	však
pozorovat	pozorovat	k5eAaImF	pozorovat
vliv	vliv	k1gInSc4	vliv
anglického	anglický	k2eAgInSc2d1	anglický
a	a	k8xC	a
francouzského	francouzský	k2eAgInSc2d1	francouzský
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
transkripce	transkripce	k1gFnSc1	transkripce
naopak	naopak	k6eAd1	naopak
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
následující	následující	k2eAgFnSc7d1	následující
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
kapitolou	kapitola	k1gFnSc7	kapitola
je	být	k5eAaImIp3nS	být
srbština	srbština	k1gFnSc1	srbština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použití	použití	k1gNnSc4	použití
cyrilice	cyrilice	k1gFnSc2	cyrilice
i	i	k8xC	i
latinky	latinka	k1gFnSc2	latinka
a	a	k8xC	a
definuje	definovat	k5eAaBmIp3nS	definovat
vzájemně	vzájemně	k6eAd1	vzájemně
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
zobrazení	zobrazení	k1gNnSc1	zobrazení
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
abecedami	abeceda	k1gFnPc7	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
abeceda	abeceda	k1gFnSc1	abeceda
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
svázána	svázat	k5eAaPmNgFnS	svázat
s	s	k7c7	s
jediným	jediný	k2eAgInSc7d1	jediný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
řečtinou	řečtina	k1gFnSc7	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
historii	historie	k1gFnSc3	historie
tohoto	tento	k3xDgMnSc2	tento
jazyka	jazyk	k1gMnSc2	jazyk
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
liší	lišit	k5eAaImIp3nS	lišit
zvyklosti	zvyklost	k1gFnPc4	zvyklost
pro	pro	k7c4	pro
transkripci	transkripce	k1gFnSc4	transkripce
starověkých	starověký	k2eAgFnPc2d1	starověká
písemných	písemný	k2eAgFnPc2d1	písemná
památek	památka	k1gFnPc2	památka
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
jmen	jméno	k1gNnPc2	jméno
hrdinů	hrdina	k1gMnPc2	hrdina
řeckých	řecký	k2eAgFnPc2d1	řecká
bájí	báj	k1gFnPc2	báj
<g/>
)	)	kIx)	)
od	od	k7c2	od
transkripce	transkripce	k1gFnSc2	transkripce
novořečtiny	novořečtina	k1gFnSc2	novořečtina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
současných	současný	k2eAgInPc2d1	současný
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
názvů	název	k1gInPc2	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řečtina	řečtina	k1gFnSc1	řečtina
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
složitý	složitý	k2eAgInSc1d1	složitý
pravopis	pravopis	k1gInSc1	pravopis
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
transkripce	transkripce	k1gFnSc1	transkripce
(	(	kIx(	(
<g/>
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nutně	nutně	k6eAd1	nutně
bude	být	k5eAaImBp3nS	být
lišit	lišit	k5eAaImF	lišit
od	od	k7c2	od
transliterace	transliterace	k1gFnSc2	transliterace
(	(	kIx(	(
<g/>
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
pravopis	pravopis	k1gInSc4	pravopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
transkripčních	transkripční	k2eAgNnPc2d1	transkripční
schémat	schéma	k1gNnPc2	schéma
z	z	k7c2	z
(	(	kIx(	(
<g/>
novo	nova	k1gFnSc5	nova
<g/>
)	)	kIx)	)
<g/>
řečtiny	řečtina	k1gFnSc2	řečtina
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
v	v	k7c6	v
přepisu	přepis	k1gInSc6	přepis
následujících	následující	k2eAgInPc2d1	následující
znaků	znak	k1gInPc2	znak
<g/>
:	:	kIx,	:
Výjimkou	výjimka	k1gFnSc7	výjimka
ovšem	ovšem	k9	ovšem
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
např.	např.	kA	např.
μ	μ	k?	μ
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
b	b	k?	b
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
mb	mb	k?	mb
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
ν	ν	k?	ν
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
d	d	k?	d
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
uprostřed	uprostřed	k7c2	uprostřed
nd	nd	k?	nd
<g/>
,	,	kIx,	,
γ	γ	k?	γ
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
γ	γ	k?	γ
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
g	g	kA	g
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
γ	γ	k?	γ
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
nkje	nkje	k1gInSc1	nkje
<g/>
,	,	kIx,	,
γ	γ	k?	γ
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
ng	ng	k?	ng
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
γ	γ	k?	γ
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
nje	nje	k?	nje
atd.	atd.	kA	atd.
Díky	dík	k1gInPc4	dík
změkčené	změkčený	k2eAgFnSc2d1	změkčená
výslovnosti	výslovnost	k1gFnSc2	výslovnost
γ	γ	k?	γ
před	před	k7c7	před
některými	některý	k3yIgFnPc7	některý
samohláskami	samohláska	k1gFnPc7	samohláska
tak	tak	k6eAd1	tak
narazíme	narazit	k5eAaPmIp1nP	narazit
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
přepis	přepis	k1gInSc4	přepis
Agios	Agios	k1gMnSc1	Agios
Nikolaos	Nikolaos	k1gMnSc1	Nikolaos
(	(	kIx(	(
<g/>
Α	Α	k?	Α
Ν	Ν	k?	Ν
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
Ajios	Ajios	k1gMnSc1	Ajios
Nikolaos	Nikolaos	k1gMnSc1	Nikolaos
<g/>
,	,	kIx,	,
Ayios	Ayios	k1gMnSc1	Ayios
Nikolaos	Nikolaos	k1gMnSc1	Nikolaos
apod.	apod.	kA	apod.
Skupina	skupina	k1gFnSc1	skupina
τ	τ	k?	τ
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
dz	dz	k?	dz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
projevuje	projevovat	k5eAaImIp3nS	projevovat
při	při	k7c6	při
transkripci	transkripce	k1gFnSc6	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
beta	beta	k1gNnSc1	beta
(	(	kIx(	(
<g/>
β	β	k?	β
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
jako	jako	k9	jako
b	b	k?	b
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
současné	současný	k2eAgFnSc3d1	současná
výslovnosti	výslovnost	k1gFnSc3	výslovnost
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
v.	v.	k?	v.
Písmeno	písmeno	k1gNnSc1	písmeno
χ	χ	k?	χ
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
jako	jako	k9	jako
h	h	k?	h
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
kontextu	kontext	k1gInSc6	kontext
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
díky	díky	k7c3	díky
výslovnosti	výslovnost	k1gFnSc3	výslovnost
<g/>
)	)	kIx)	)
běžnější	běžný	k2eAgInSc1d2	běžnější
přepis	přepis	k1gInSc1	přepis
ch	ch	k0	ch
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
českému	český	k2eAgInSc3d1	český
i	i	k9	i
lze	lze	k6eAd1	lze
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
zapsat	zapsat	k5eAaPmF	zapsat
pěti	pět	k4xCc7	pět
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
ι	ι	k?	ι
<g/>
,	,	kIx,	,
η	η	k?	η
<g/>
,	,	kIx,	,
υ	υ	k?	υ
<g/>
,	,	kIx,	,
ε	ε	k?	ε
<g/>
,	,	kIx,	,
ο	ο	k?	ο
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
schémata	schéma	k1gNnPc1	schéma
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
skupiny	skupina	k1gFnPc4	skupina
jako	jako	k9	jako
i	i	k8xC	i
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
původní	původní	k2eAgInSc4d1	původní
pravopis	pravopis	k1gInSc4	pravopis
u	u	k7c2	u
ei	ei	k?	ei
a	a	k8xC	a
oi	oi	k?	oi
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
υ	υ	k?	υ
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
y	y	k?	y
nebo	nebo	k8xC	nebo
u	u	k7c2	u
<g/>
)	)	kIx)	)
a	a	k8xC	a
úplná	úplný	k2eAgFnSc1d1	úplná
transliterace	transliterace	k1gFnSc1	transliterace
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
(	(	kIx(	(
<g/>
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
)	)	kIx)	)
i	i	k9	i
η	η	k?	η
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
α	α	k?	α
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
(	(	kIx(	(
<g/>
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
též	též	k9	též
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
e.	e.	k?	e.
Skupiny	skupina	k1gFnSc2	skupina
α	α	k?	α
a	a	k8xC	a
ε	ε	k?	ε
se	se	k3xPyFc4	se
čtou	číst	k5eAaImIp3nP	číst
af	af	k?	af
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ef	ef	k?	ef
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
používaných	používaný	k2eAgFnPc2d1	používaná
transliterací	transliterace	k1gFnPc2	transliterace
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
av	av	k?	av
<g/>
,	,	kIx,	,
ev	ev	k?	ev
nebo	nebo	k8xC	nebo
au	au	k0	au
<g/>
,	,	kIx,	,
eu	eu	k?	eu
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
ο	ο	k?	ο
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
u	u	k7c2	u
<g/>
,	,	kIx,	,
alternativní	alternativní	k2eAgInSc4d1	alternativní
přepis	přepis	k1gInSc4	přepis
je	být	k5eAaImIp3nS	být
ou	ou	k0	ou
<g/>
.	.	kIx.	.
</s>
<s>
Diakritická	diakritický	k2eAgNnPc1d1	diakritické
znaménka	znaménko	k1gNnPc1	znaménko
<g/>
,	,	kIx,	,
kterými	který	k3yQgNnPc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
volitelně	volitelně	k6eAd1	volitelně
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
přízvuk	přízvuk	k1gInSc1	přízvuk
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
nepřepisují	přepisovat	k5eNaImIp3nP	přepisovat
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
transkripci	transkripce	k1gFnSc4	transkripce
a	a	k8xC	a
ne	ne	k9	ne
transliteraci	transliterace	k1gFnSc4	transliterace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Arabské	arabský	k2eAgNnSc1d1	arabské
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vedle	vedle	k7c2	vedle
arabštiny	arabština	k1gFnSc2	arabština
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
několika	několik	k4yIc2	několik
indoevropských	indoevropský	k2eAgFnPc2d1	indoevropská
(	(	kIx(	(
<g/>
např.	např.	kA	např.
perština	perština	k1gFnSc1	perština
<g/>
,	,	kIx,	,
paštština	paštština	k1gFnSc1	paštština
<g/>
,	,	kIx,	,
urdština	urdština	k1gFnSc1	urdština
<g/>
)	)	kIx)	)
a	a	k8xC	a
turkických	turkický	k2eAgInPc2d1	turkický
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ujgurština	ujgurština	k1gFnSc1	ujgurština
<g/>
)	)	kIx)	)
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
částečný	částečný	k2eAgInSc4d1	částečný
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
standard	standard	k1gInSc4	standard
pro	pro	k7c4	pro
transkripci	transkripce	k1gFnSc4	transkripce
arabštiny	arabština	k1gFnSc2	arabština
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
u	u	k7c2	u
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
názvů	název	k1gInPc2	název
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
anglický	anglický	k2eAgInSc4d1	anglický
a	a	k8xC	a
francouzský	francouzský	k2eAgInSc4d1	francouzský
přepis	přepis	k1gInSc4	přepis
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
koloniální	koloniální	k2eAgFnSc3d1	koloniální
historii	historie	k1gFnSc3	historie
<g/>
)	)	kIx)	)
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
arabsky	arabsky	k6eAd1	arabsky
mluvící	mluvící	k2eAgFnSc2d1	mluvící
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
arabštině	arabština	k1gFnSc6	arabština
najdeme	najít	k5eAaPmIp1nP	najít
řadu	řada	k1gFnSc4	řada
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
indoevropské	indoevropský	k2eAgInPc1d1	indoevropský
jazyky	jazyk	k1gInPc1	jazyk
neznají	znát	k5eNaImIp3nP	znát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
transkripci	transkripce	k1gFnSc6	transkripce
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
najít	najít	k5eAaPmF	najít
jejich	jejich	k3xOp3gInSc4	jejich
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
<g/>
;	;	kIx,	;
není	být	k5eNaImIp3nS	být
pak	pak	k6eAd1	pak
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupině	skupina	k1gFnSc3	skupina
znaků	znak	k1gInPc2	znak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jediný	jediný	k2eAgInSc1d1	jediný
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
se	se	k3xPyFc4	se
některý	některý	k3yIgInSc1	některý
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
znak	znak	k1gInSc1	znak
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
úplně	úplně	k6eAd1	úplně
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Bezproblémové	bezproblémový	k2eAgInPc1d1	bezproblémový
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgInPc1d1	následující
znaky	znak	k1gInPc1	znak
<g/>
:	:	kIx,	:
Další	další	k2eAgInPc1d1	další
znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Evropan	Evropan	k1gMnSc1	Evropan
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
odliší	odlišit	k5eAaPmIp3nS	odlišit
od	od	k7c2	od
výslovnosti	výslovnost	k1gFnSc2	výslovnost
některých	některý	k3yIgFnPc2	některý
výše	vysoce	k6eAd2	vysoce
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
<g/>
,	,	kIx,	,
a	a	k8xC	a
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
obvykle	obvykle	k6eAd1	obvykle
stejným	stejný	k2eAgNnSc7d1	stejné
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
transkripce	transkripce	k1gFnSc2	transkripce
arabštiny	arabština	k1gFnSc2	arabština
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
zpětně	zpětně	k6eAd1	zpětně
získat	získat	k5eAaPmF	získat
původní	původní	k2eAgInSc4d1	původní
pravopis	pravopis	k1gInSc4	pravopis
<g/>
:	:	kIx,	:
Obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
skupinu	skupina	k1gFnSc4	skupina
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
přepis	přepis	k1gInSc1	přepis
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
cílovém	cílový	k2eAgInSc6d1	cílový
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
ج	ج	k?	ج
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
dž	dž	k?	dž
<g/>
,	,	kIx,	,
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
francouzštiny	francouzština	k1gFnSc2	francouzština
j	j	k?	j
<g/>
,	,	kIx,	,
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
z	z	k7c2	z
egyptské	egyptský	k2eAgFnSc2d1	egyptská
arabštiny	arabština	k1gFnSc2	arabština
kvůli	kvůli	k7c3	kvůli
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g />
.	.	kIx.	.
</s>
<s>
výslovnosti	výslovnost	k1gFnSc3	výslovnost
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
خ	خ	k?	خ
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
ch	ch	k0	ch
<g/>
,	,	kIx,	,
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
francouzštiny	francouzština	k1gFnSc2	francouzština
kh	kh	k0	kh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ش	ش	k?	ش
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
š	š	k?	š
<g/>
,	,	kIx,	,
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
sh	sh	k?	sh
<g/>
,	,	kIx,	,
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
ch	ch	k0	ch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
غ	غ	k?	غ
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
g	g	kA	g
nebo	nebo	k8xC	nebo
gh	gh	k?	gh
<g/>
,	,	kIx,	,
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
gh	gh	k?	gh
<g/>
,	,	kIx,	,
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
rh	rh	k?	rh
<g/>
)	)	kIx)	)
a	a	k8xC	a
ق	ق	k?	ق
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
k	k	k7c3	k
<g/>
,	,	kIx,	,
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
francouzštiny	francouzština	k1gFnSc2	francouzština
q	q	k?	q
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Arabština	arabština	k1gFnSc1	arabština
obvykle	obvykle	k6eAd1	obvykle
nezapisuje	zapisovat	k5eNaImIp3nS	zapisovat
krátké	krátký	k2eAgFnPc4d1	krátká
samohlásky	samohláska	k1gFnPc4	samohláska
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
má	mít	k5eAaImIp3nS	mít
prostředky	prostředek	k1gInPc4	prostředek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
transkripce	transkripce	k1gFnSc1	transkripce
má	mít	k5eAaImIp3nS	mít
aproximovat	aproximovat	k5eAaBmF	aproximovat
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
přepisu	přepis	k1gInSc6	přepis
by	by	kYmCp3nS	by
samohlásky	samohláska	k1gFnPc1	samohláska
chybět	chybět	k5eAaImF	chybět
neměly	mít	k5eNaImAgFnP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
některých	některý	k3yIgNnPc2	některý
jiných	jiný	k2eAgNnPc2d1	jiné
písem	písmo	k1gNnPc2	písmo
proto	proto	k8xC	proto
arabštinu	arabština	k1gFnSc4	arabština
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
přepsat	přepsat	k5eAaPmF	přepsat
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
neumí	umět	k5eNaImIp3nS	umět
arabsky	arabsky	k6eAd1	arabsky
<g/>
,	,	kIx,	,
jen	jen	k9	jen
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
převodní	převodní	k2eAgFnSc2d1	převodní
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
arabština	arabština	k1gFnSc1	arabština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc4	tři
samohlásky	samohláska	k1gFnPc4	samohláska
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
i	i	k8xC	i
a	a	k8xC	a
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
transkripcích	transkripce	k1gFnPc6	transkripce
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k8xC	i
e	e	k0	e
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
má	mít	k5eAaImIp3nS	mít
výslovnost	výslovnost	k1gFnSc1	výslovnost
některých	některý	k3yIgInPc2	některý
arabských	arabský	k2eAgInPc2d1	arabský
dialektů	dialekt	k1gInPc2	dialekt
blíže	blíž	k1gFnSc2	blíž
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
samohlásky	samohláska	k1gFnPc1	samohláska
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
pomocí	pomocí	k7c2	pomocí
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
kontextu	kontext	k1gInSc6	kontext
představují	představovat	k5eAaImIp3nP	představovat
souhlásky	souhláska	k1gFnPc1	souhláska
či	či	k8xC	či
polosamohlásky	polosamohláska	k1gFnPc1	polosamohláska
<g/>
:	:	kIx,	:
ا	ا	k?	ا
(	(	kIx(	(
<g/>
samohláska	samohláska	k1gFnSc1	samohláska
ā	ā	k?	ā
<g/>
,	,	kIx,	,
o	o	k7c6	o
souhlásce	souhláska	k1gFnSc6	souhláska
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ى	ى	k?	ى
(	(	kIx(	(
<g/>
samohláska	samohláska	k1gFnSc1	samohláska
ī	ī	k?	ī
<g/>
,	,	kIx,	,
souhláska	souhláska	k1gFnSc1	souhláska
j	j	k?	j
(	(	kIx(	(
<g/>
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
y	y	k?	y
(	(	kIx(	(
<g/>
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
))	))	k?	))
a	a	k8xC	a
و	و	k?	و
(	(	kIx(	(
<g/>
samohláska	samohláska	k1gFnSc1	samohláska
ū	ū	k?	ū
<g/>
,	,	kIx,	,
souhláska	souhláska	k1gFnSc1	souhláska
w	w	k?	w
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
álif	álif	k1gInSc1	álif
(	(	kIx(	(
<g/>
ا	ا	k?	ا
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
souhláska	souhláska	k1gFnSc1	souhláska
představuje	představovat	k5eAaImIp3nS	představovat
ráz	ráz	k1gInSc4	ráz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
předchází	předcházet	k5eAaImIp3nS	předcházet
samohlásce	samohláska	k1gFnSc3	samohláska
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
latinky	latinka	k1gFnSc2	latinka
"	"	kIx"	"
<g/>
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
nepřepisuje	přepisovat	k5eNaImIp3nS	přepisovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
jeho	jeho	k3xOp3gInSc7	jeho
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
byla	být	k5eAaImAgFnS	být
samohláska	samohláska	k1gFnSc1	samohláska
a	a	k8xC	a
<g/>
,	,	kIx,	,
i	i	k8xC	i
nebo	nebo	k8xC	nebo
u.	u.	k?	u.
Také	také	k9	také
souhláska	souhláska	k1gFnSc1	souhláska
ع	ع	k?	ع
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
Evropanem	Evropan	k1gMnSc7	Evropan
těžko	těžko	k6eAd1	těžko
zvládnutelný	zvládnutelný	k2eAgInSc4d1	zvládnutelný
hrdelní	hrdelní	k2eAgInSc4d1	hrdelní
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
úplně	úplně	k6eAd1	úplně
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
různými	různý	k2eAgInPc7d1	různý
apostrofy	apostrof	k1gInPc7	apostrof
<g/>
,	,	kIx,	,
v	v	k7c6	v
odborněji	odborně	k6eAd2	odborně
laděných	laděný	k2eAgInPc6d1	laděný
kontextech	kontext	k1gInPc6	kontext
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
malé	malý	k2eAgNnSc1d1	malé
c	c	k0	c
jako	jako	k8xC	jako
horní	horní	k2eAgInSc4d1	horní
index	index	k1gInSc4	index
<g/>
:	:	kIx,	:
al-cIrā	alIrā	k?	al-cIrā
<g/>
.	.	kIx.	.
</s>
<s>
Apostrofem	apostrof	k1gInSc7	apostrof
se	se	k3xPyFc4	se
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
také	také	k9	také
znak	znak	k1gInSc1	znak
hamza	hamz	k1gMnSc2	hamz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
nad	nad	k7c7	nad
a	a	k8xC	a
pod	pod	k7c7	pod
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
:	:	kIx,	:
ء	ء	k?	ء
,	,	kIx,	,
<g/>
ؤ	ؤ	k?	ؤ
,	,	kIx,	,
<g/>
إ	إ	k?	إ
,	,	kIx,	,
<g/>
ئ	ئ	k?	ئ
,	,	kIx,	,
<g/>
أ	أ	k?	أ
V	v	k7c6	v
transkripci	transkripce	k1gFnSc6	transkripce
arabštiny	arabština	k1gFnSc2	arabština
lze	lze	k6eAd1	lze
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
další	další	k2eAgInPc4d1	další
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgMnPc3	který
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g />
.	.	kIx.	.
</s>
<s>
znalost	znalost	k1gFnSc1	znalost
jazyka	jazyk	k1gInSc2	jazyk
<g/>
:	:	kIx,	:
např.	např.	kA	např.
zdvojená	zdvojený	k2eAgNnPc4d1	zdvojené
písmena	písmeno	k1gNnPc4	písmeno
(	(	kIx(	(
<g/>
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
arabském	arabský	k2eAgInSc6d1	arabský
textu	text	k1gInSc6	text
zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
nejsou	být	k5eNaImIp3nP	být
<g/>
)	)	kIx)	)
či	či	k8xC	či
upravený	upravený	k2eAgInSc1d1	upravený
člen	člen	k1gInSc1	člen
před	před	k7c7	před
konkrétními	konkrétní	k2eAgFnPc7d1	konkrétní
souhláskami	souhláska	k1gFnPc7	souhláska
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
letoviska	letovisko	k1gNnSc2	letovisko
ش	ش	k?	ش
ا	ا	k?	ا
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
a	a	k8xC	a
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
Šarm	šarm	k1gInSc4	šarm
aš-Šajch	aš-Šajcha	k1gFnPc2	aš-Šajcha
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
arabském	arabský	k2eAgInSc6d1	arabský
pravopisu	pravopis	k1gInSc6	pravopis
není	být	k5eNaImIp3nS	být
aš	aš	k?	aš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
al	ala	k1gFnPc2	ala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nearabské	arabský	k2eNgInPc1d1	nearabský
jazyky	jazyk	k1gInPc1	jazyk
používající	používající	k2eAgFnSc4d1	používající
arabské	arabský	k2eAgNnSc4d1	arabské
písmo	písmo	k1gNnSc4	písmo
většinou	většina	k1gFnSc7	většina
nemají	mít	k5eNaImIp3nP	mít
řadu	řada	k1gFnSc4	řada
specificky	specificky	k6eAd1	specificky
arabských	arabský	k2eAgFnPc2d1	arabská
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
příslušná	příslušný	k2eAgNnPc1d1	příslušné
písmena	písmeno	k1gNnPc1	písmeno
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
např.	např.	kA	např.
v	v	k7c6	v
běžných	běžný	k2eAgNnPc6d1	běžné
muslimských	muslimský	k2eAgNnPc6d1	muslimské
jménech	jméno	k1gNnPc6	jméno
(	(	kIx(	(
<g/>
Muhammad	Muhammad	k1gInSc1	Muhammad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
lépe	dobře	k6eAd2	dobře
vyznačeny	vyznačit	k5eAaPmNgFnP	vyznačit
krátké	krátký	k2eAgFnPc1d1	krátká
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
arabština	arabština	k1gFnSc1	arabština
nemá	mít	k5eNaImIp3nS	mít
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
nimž	jenž	k3xRgFnPc3	jenž
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc4d1	nutné
původní	původní	k2eAgNnSc4d1	původní
arabské	arabský	k2eAgNnSc4d1	arabské
písmo	písmo	k1gNnSc4	písmo
rozšířit	rozšířit	k5eAaPmF	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnPc1d3	nejběžnější
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
پ	پ	k?	پ
(	(	kIx(	(
<g/>
p	p	k?	p
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
چ	چ	k?	چ
(	(	kIx(	(
<g/>
č	č	k0	č
<g/>
)	)	kIx)	)
a	a	k8xC	a
گ	گ	k?	گ
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
hebrejština	hebrejština	k1gFnSc1	hebrejština
obvykle	obvykle	k6eAd1	obvykle
nezaznamenává	zaznamenávat	k5eNaImIp3nS	zaznamenávat
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
spisovné	spisovný	k2eAgFnSc2d1	spisovná
arabštiny	arabština	k1gFnSc2	arabština
se	se	k3xPyFc4	se
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
se	s	k7c7	s
samohláskami	samohláska	k1gFnPc7	samohláska
e	e	k0	e
a	a	k8xC	a
o.	o.	k?	o.
V	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
navíc	navíc	k6eAd1	navíc
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
znaky	znak	k1gInPc4	znak
existuje	existovat	k5eAaImIp3nS	existovat
dvojí	dvojí	k4xRgFnSc1	dvojí
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
,	,	kIx,	,
ražená	ražený	k2eAgFnSc1d1	ražená
a	a	k8xC	a
třená	třený	k2eAgFnSc1d1	třená
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
přepise	přepis	k1gInSc6	přepis
třeba	třeba	k6eAd1	třeba
zohlednit	zohlednit	k5eAaPmF	zohlednit
<g/>
.	.	kIx.	.
</s>
<s>
Upravené	upravený	k2eAgNnSc1d1	upravené
hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
jidiš	jidiš	k1gNnSc2	jidiš
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
formy	forma	k1gFnPc1	forma
aramejštiny	aramejština	k1gFnSc2	aramejština
a	a	k8xC	a
mandejštiny	mandejština	k1gFnSc2	mandejština
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
א	א	k?	א
a	a	k8xC	a
ע	ע	k?	ע
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
jako	jako	k9	jako
ráz	ráz	k1gInSc4	ráz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
obvykle	obvykle	k6eAd1	obvykle
nepřepisují	přepisovat	k5eNaImIp3nP	přepisovat
vůbec	vůbec	k9	vůbec
(	(	kIx(	(
<g/>
přepsané	přepsaný	k2eAgNnSc1d1	přepsané
slovo	slovo	k1gNnSc1	slovo
pak	pak	k6eAd1	pak
začíná	začínat	k5eAaImIp3nS	začínat
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
vynechávají	vynechávat	k5eAaImIp3nP	vynechávat
nebo	nebo	k8xC	nebo
zvýrazňují	zvýrazňovat	k5eAaImIp3nP	zvýrazňovat
apostrofy	apostrofa	k1gFnPc4	apostrofa
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fonologie	fonologie	k1gFnSc2	fonologie
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgNnSc1d1	čínské
písmo	písmo	k1gNnSc1	písmo
využívá	využívat	k5eAaPmIp3nS	využívat
repertoár	repertoár	k1gInSc4	repertoár
desetitisíců	desetitisíce	k1gInPc2	desetitisíce
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
jistě	jistě	k9	jistě
část	část	k1gFnSc1	část
informace	informace	k1gFnSc2	informace
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
více	hodně	k6eAd2	hodně
než	než	k8xS	než
kde	kde	k6eAd1	kde
jinde	jinde	k6eAd1	jinde
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tentýž	týž	k3xTgInSc1	týž
znak	znak	k1gInSc1	znak
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
v	v	k7c6	v
(	(	kIx(	(
<g/>
mandarínské	mandarínský	k2eAgFnSc6d1	mandarínská
<g/>
)	)	kIx)	)
čínštině	čínština	k1gFnSc6	čínština
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
v	v	k7c6	v
kantonštině	kantonština	k1gFnSc6	kantonština
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
ve	v	k7c6	v
tchajwanštině	tchajwanština	k1gFnSc6	tchajwanština
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přepis	přepis	k1gInSc4	přepis
ze	z	k7c2	z
samotné	samotný	k2eAgFnSc2d1	samotná
čínštiny	čínština	k1gFnSc2	čínština
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
fonetický	fonetický	k2eAgInSc1d1	fonetický
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
znak	znak	k1gInSc1	znak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
právě	právě	k9	právě
jedné	jeden	k4xCgFnSc3	jeden
slabice	slabika	k1gFnSc3	slabika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
počáteční	počáteční	k2eAgFnSc2d1	počáteční
souhlásky	souhláska	k1gFnSc2	souhláska
(	(	kIx(	(
<g/>
iniciály	iniciála	k1gFnSc2	iniciála
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
samohláskami	samohláska	k1gFnPc7	samohláska
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
ještě	ještě	k9	ještě
koncovým	koncový	k2eAgMnPc3d1	koncový
[	[	kIx(	[
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ng	ng	k?	ng
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
r	r	kA	r
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Slabika	slabika	k1gFnSc1	slabika
dále	daleko	k6eAd2	daleko
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
tónů	tón	k1gInPc2	tón
nebo	nebo	k8xC	nebo
být	být	k5eAaImF	být
tónově	tónově	k6eAd1	tónově
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
systémů	systém	k1gInPc2	systém
přepisu	přepis	k1gInSc2	přepis
čínštiny	čínština	k1gFnSc2	čínština
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
většinou	většinou	k6eAd1	většinou
respektují	respektovat	k5eAaImIp3nP	respektovat
repertoár	repertoár	k1gInSc4	repertoár
iniciál	iniciála	k1gFnPc2	iniciála
<g/>
,	,	kIx,	,
koncovek	koncovka	k1gFnPc2	koncovka
a	a	k8xC	a
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
správně	správně	k6eAd1	správně
přepsaný	přepsaný	k2eAgInSc4d1	přepsaný
text	text	k1gInSc4	text
lze	lze	k6eAd1	lze
převádět	převádět	k5eAaImF	převádět
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
systému	systém	k1gInSc2	systém
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc4	systém
pinyin	pinyin	k2eAgInSc4d1	pinyin
(	(	kIx(	(
<g/>
též	též	k9	též
hanyu	hanyu	k6eAd1	hanyu
pinyin	pinyin	k2eAgMnSc1d1	pinyin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
vládami	vláda	k1gFnPc7	vláda
ČLR	ČLR	kA	ČLR
a	a	k8xC	a
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
tongyong	tongyong	k1gMnSc1	tongyong
pinyin	pinyin	k1gMnSc1	pinyin
doporučený	doporučený	k2eAgMnSc1d1	doporučený
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
,	,	kIx,	,
Wade-Gilesova	Wade-Gilesův	k2eAgFnSc1d1	Wade-Gilesův
transkripce	transkripce	k1gFnSc1	transkripce
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Švarného	švarný	k2eAgMnSc2d1	švarný
(	(	kIx(	(
<g/>
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
uzákoněn	uzákoněn	k2eAgMnSc1d1	uzákoněn
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
oficiální	oficiální	k2eAgInSc1d1	oficiální
přepis	přepis	k1gInSc1	přepis
také	také	k9	také
hanyu	hanyu	k5eAaPmIp1nS	hanyu
pinyin	pinyin	k1gInSc1	pinyin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tóny	Tóny	k1gFnSc1	Tóny
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
jen	jen	k9	jen
v	v	k7c6	v
lingvisticky	lingvisticky	k6eAd1	lingvisticky
orientovaných	orientovaný	k2eAgFnPc6d1	orientovaná
publikacích	publikace	k1gFnPc6	publikace
a	a	k8xC	a
běžná	běžný	k2eAgNnPc1d1	běžné
média	médium	k1gNnPc1	médium
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
jmen	jméno	k1gNnPc2	jméno
čínských	čínský	k2eAgFnPc2d1	čínská
osobností	osobnost	k1gFnPc2	osobnost
<g/>
)	)	kIx)	)
je	on	k3xPp3gNnSc4	on
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odborné	odborný	k2eAgFnPc4d1	odborná
publikace	publikace	k1gFnPc4	publikace
(	(	kIx(	(
<g/>
i	i	k9	i
české	český	k2eAgFnPc1d1	Česká
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
,	,	kIx,	,
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
tóny	tón	k1gInPc7	tón
vyznačenými	vyznačený	k2eAgInPc7d1	vyznačený
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
transkripčních	transkripční	k2eAgInPc6d1	transkripční
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
otázky	otázka	k1gFnSc2	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zapisovat	zapisovat	k5eAaImF	zapisovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
iniciály	iniciála	k1gFnPc4	iniciála
a	a	k8xC	a
koncovky	koncovka	k1gFnPc4	koncovka
a	a	k8xC	a
zda	zda	k8xS	zda
a	a	k8xC	a
jak	jak	k6eAd1	jak
vyznačit	vyznačit	k5eAaPmF	vyznačit
tóny	tón	k1gInPc4	tón
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
transkripce	transkripce	k1gFnSc1	transkripce
čínštiny	čínština	k1gFnSc2	čínština
stanovit	stanovit	k5eAaPmF	stanovit
i	i	k9	i
další	další	k2eAgNnPc4d1	další
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
dělení	dělení	k1gNnSc4	dělení
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
čínština	čínština	k1gFnSc1	čínština
mezery	mezera	k1gFnSc2	mezera
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
nedělá	dělat	k5eNaImIp3nS	dělat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
značení	značení	k1gNnSc1	značení
hranic	hranice	k1gFnPc2	hranice
slabik	slabika	k1gFnPc2	slabika
uvnitř	uvnitř	k7c2	uvnitř
slov	slovo	k1gNnPc2	slovo
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
chybnému	chybný	k2eAgNnSc3d1	chybné
čtení	čtení	k1gNnSc3	čtení
<g/>
,	,	kIx,	,
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
velkých	velký	k2eAgNnPc2d1	velké
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
čínštiny	čínština	k1gFnSc2	čínština
foneticky	foneticky	k6eAd1	foneticky
přepsána	přepsán	k2eAgFnSc1d1	přepsána
z	z	k7c2	z
latinky	latinka	k1gFnSc2	latinka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
z	z	k7c2	z
čínštiny	čínština	k1gFnSc2	čínština
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
pravopisu	pravopis	k1gInSc2	pravopis
a	a	k8xC	a
k	k	k7c3	k
čínské	čínský	k2eAgFnSc3d1	čínská
výslovnosti	výslovnost	k1gFnSc3	výslovnost
se	se	k3xPyFc4	se
nepřihlíží	přihlížet	k5eNaImIp3nS	přihlížet
<g/>
:	:	kIx,	:
např.	např.	kA	např.
Washington	Washington	k1gInSc1	Washington
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nepřepisuje	přepisovat	k5eNaImIp3nS	přepisovat
do	do	k7c2	do
pinyinu	pinyin	k1gInSc2	pinyin
jako	jako	k8xC	jako
Huáshè	Huáshè	k1gFnSc2	Huáshè
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
normálně	normálně	k6eAd1	normálně
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
čínským	čínský	k2eAgInPc3d1	čínský
znakům	znak	k1gInPc3	znak
<g/>
,	,	kIx,	,
kterými	který	k3yRgNnPc7	který
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přepis	přepis	k1gInSc4	přepis
z	z	k7c2	z
kantonštiny	kantonština	k1gFnSc2	kantonština
je	být	k5eAaImIp3nS	být
de-facto	deacto	k1gNnSc1	de-facto
standardem	standard	k1gInSc7	standard
systém	systém	k1gInSc4	systém
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
na	na	k7c6	na
Yaleské	Yaleský	k2eAgFnSc6d1	Yaleská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
japonštinu	japonština	k1gFnSc4	japonština
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
systém	systém	k1gInSc1	systém
rómadži	rómadž	k1gFnSc3	rómadž
(	(	kIx(	(
<g/>
skupinový	skupinový	k2eAgInSc1d1	skupinový
název	název	k1gInSc1	název
<g/>
)	)	kIx)	)
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
samotnými	samotný	k2eAgMnPc7d1	samotný
Japonci	Japonec	k1gMnPc7	Japonec
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
pravidla	pravidlo	k1gNnSc2	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
i	i	k9	i
zde	zde	k6eAd1	zde
odchylky	odchylka	k1gFnPc4	odchylka
pro	pro	k7c4	pro
citovaná	citovaný	k2eAgNnPc4d1	citované
slova	slovo	k1gNnPc4	slovo
uvnitř	uvnitř	k6eAd1	uvnitř
českého	český	k2eAgInSc2d1	český
textu	text	k1gInSc2	text
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
ustálený	ustálený	k2eAgInSc4d1	ustálený
pravopis	pravopis	k1gInSc4	pravopis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
používat	používat	k5eAaImF	používat
přepis	přepis	k1gInSc4	přepis
podle	podle	k7c2	podle
systému	systém	k1gInSc2	systém
čekošiki	čekošiki	k6eAd1	čekošiki
rómadži	rómadzat	k5eAaPmIp1nS	rómadzat
a	a	k8xC	a
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
přepisu	přepis	k1gInSc3	přepis
<g/>
,	,	kIx,	,
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgNnSc4d1	mluvící
hebonšiki	hebonšiki	k1gNnSc4	hebonšiki
rómadži	rómadž	k1gFnSc3	rómadž
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgInPc3d1	jiný
přepisům	přepis	k1gInPc3	přepis
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
určeným	určený	k2eAgNnPc3d1	určené
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přepis	přepis	k1gInSc4	přepis
nejběžnějších	běžný	k2eAgInPc2d3	nejběžnější
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
některými	některý	k3yIgInPc7	některý
jmenovanými	jmenovaný	k2eAgInPc7d1	jmenovaný
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Karlík	Karlík	k1gMnSc1	Karlík
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Nekula	Nekula	k1gMnSc1	Nekula
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Pleskalová	Pleskalový	k2eAgFnSc1d1	Pleskalový
J.	J.	kA	J.
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedický	encyklopedický	k2eAgInSc1d1	encyklopedický
slovník	slovník	k1gInSc1	slovník
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
484	[number]	k4	484
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
Transkripce	transkripce	k1gFnSc2	transkripce
a	a	k8xC	a
transliterace	transliterace	k1gFnSc2	transliterace
azbuky	azbuka	k1gFnSc2	azbuka
podle	podle	k7c2	podle
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
9	[number]	k4	9
ONLINE	ONLINE	kA	ONLINE
<g/>
.	.	kIx.	.
</s>
<s>
Transliterace	transliterace	k1gFnSc1	transliterace
arabského	arabský	k2eAgNnSc2d1	arabské
písma	písmo	k1gNnSc2	písmo
ONLINE	ONLINE	kA	ONLINE
<g/>
.	.	kIx.	.
</s>
