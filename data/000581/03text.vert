<s>
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
posledního	poslední	k2eAgInSc2d1	poslední
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
svazků	svazek	k1gInPc2	svazek
románu	román	k1gInSc2	román
Johna	John	k1gMnSc2	John
Ronalda	Ronald	k1gMnSc2	Ronald
Reuela	Reuel	k1gMnSc2	Reuel
Tolkiena	Tolkien	k1gMnSc2	Tolkien
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
páté	pátý	k4xOgFnSc2	pátý
knihy	kniha	k1gFnSc2	kniha
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
Gandalf	Gandalf	k1gInSc1	Gandalf
s	s	k7c7	s
Pipinem	Pipin	k1gInSc7	Pipin
do	do	k7c2	do
Minas	Minasa	k1gFnPc2	Minasa
Tirithu	Tirith	k1gInSc2	Tirith
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
.	.	kIx.	.
</s>
<s>
Gondorská	Gondorský	k2eAgFnSc1d1	Gondorská
říše	říše	k1gFnSc1	říše
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tíživé	tíživý	k2eAgFnSc6d1	tíživá
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zhoršující	zhoršující	k2eAgFnSc4d1	zhoršující
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Sauron	Sauron	k1gInSc1	Sauron
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
nezískal	získat	k5eNaPmAgMnS	získat
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
hodlá	hodlat	k5eAaImIp3nS	hodlat
válkou	válka	k1gFnSc7	válka
přemoci	přemoct	k5eAaPmF	přemoct
ostatní	ostatní	k2eAgMnPc4d1	ostatní
obyvatele	obyvatel	k1gMnPc4	obyvatel
Středozemě	Středozem	k1gFnSc2	Středozem
a	a	k8xC	a
vybudovat	vybudovat	k5eAaPmF	vybudovat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
neomezené	omezený	k2eNgNnSc1d1	neomezené
panství	panství	k1gNnSc1	panství
a	a	k8xC	a
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
hodlá	hodlat	k5eAaImIp3nS	hodlat
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
Gondor	Gondor	k1gInSc4	Gondor
jakožto	jakožto	k8xS	jakožto
na	na	k7c4	na
nejmocnější	mocný	k2eAgFnSc4d3	nejmocnější
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
odpůrců	odpůrce	k1gMnPc2	odpůrce
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
kvůli	kvůli	k7c3	kvůli
dávné	dávný	k2eAgFnSc3d1	dávná
zášti	zášť	k1gFnSc3	zášť
(	(	kIx(	(
<g/>
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
v	v	k7c6	v
Silmarillionu	Silmarillion	k1gInSc6	Silmarillion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gondor	Gondor	k1gInSc1	Gondor
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
zoufale	zoufale	k6eAd1	zoufale
spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Aragorn	Aragorn	k1gInSc1	Aragorn
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Gandalf	Gandalf	k1gInSc1	Gandalf
s	s	k7c7	s
Pipinem	Pipin	k1gInSc7	Pipin
cestují	cestovat	k5eAaImIp3nP	cestovat
do	do	k7c2	do
Minas	Minasa	k1gFnPc2	Minasa
Tirithu	Tirith	k1gInSc2	Tirith
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaImIp3nS	vydávat
do	do	k7c2	do
rohanského	rohanský	k2eAgInSc2d1	rohanský
horského	horský	k2eAgInSc2d1	horský
průsmyku	průsmyk	k1gInSc2	průsmyk
na	na	k7c4	na
Stezky	stezka	k1gFnPc4	stezka
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dlí	dlít	k5eAaImIp3nS	dlít
stínové	stínový	k2eAgNnSc1d1	stínové
vojsko	vojsko	k1gNnSc1	vojsko
duchů	duch	k1gMnPc2	duch
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
dávné	dávný	k2eAgFnSc6d1	dávná
válce	válka	k1gFnSc6	válka
porušili	porušit	k5eAaPmAgMnP	porušit
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gInSc1	Aragorn
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
pomohli	pomoct	k5eAaPmAgMnP	pomoct
a	a	k8xC	a
tak	tak	k6eAd1	tak
odčinili	odčinit	k5eAaPmAgMnP	odčinit
dávnou	dávný	k2eAgFnSc4d1	dávná
vinu	vina	k1gFnSc4	vina
<g/>
.	.	kIx.	.
</s>
<s>
Stínoví	stínový	k2eAgMnPc1d1	stínový
bojovníci	bojovník	k1gMnPc1	bojovník
porazí	porazit	k5eAaPmIp3nP	porazit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Gondoru	Gondor	k1gInSc2	Gondor
Sauronovy	Sauronův	k2eAgMnPc4d1	Sauronův
spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
korzáry	korzár	k1gMnPc4	korzár
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vázali	vázat	k5eAaImAgMnP	vázat
gondorské	gondorský	k2eAgFnPc4d1	gondorská
síly	síla	k1gFnPc4	síla
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
jejich	jejich	k3xOp3gFnSc2	jejich
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
oslabovali	oslabovat	k5eAaImAgMnP	oslabovat
tak	tak	k9	tak
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Mordorem	Mordor	k1gInSc7	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Gondoru	Gondor	k1gInSc2	Gondor
chystají	chystat	k5eAaImIp3nP	chystat
i	i	k9	i
Rohirové	Rohir	k1gMnPc1	Rohir
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
však	však	k9	však
pomoc	pomoc	k1gFnSc1	pomoc
stihne	stihnout	k5eAaPmIp3nS	stihnout
dorazit	dorazit	k5eAaPmF	dorazit
<g/>
,	,	kIx,	,
Sauronovo	Sauronův	k2eAgNnSc1d1	Sauronovo
vojsko	vojsko	k1gNnSc1	vojsko
opustí	opustit	k5eAaPmIp3nS	opustit
Mordor	Mordor	k1gInSc4	Mordor
a	a	k8xC	a
oblehne	oblehnout	k5eAaPmIp3nS	oblehnout
Minas	Minas	k1gMnSc1	Minas
Tirith	Tirith	k1gMnSc1	Tirith
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
je	být	k5eAaImIp3nS	být
kapitán	kapitán	k1gMnSc1	kapitán
Prstenových	prstenový	k2eAgMnPc2d1	prstenový
přízraků	přízrak	k1gInPc2	přízrak
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
nazgû	nazgû	k?	nazgû
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
však	však	k9	však
zanedlouho	zanedlouho	k6eAd1	zanedlouho
zabije	zabít	k5eAaPmIp3nS	zabít
rohirská	rohirský	k2eAgFnSc1d1	rohirský
dívka	dívka	k1gFnSc1	dívka
Éowyn	Éowyn	k1gMnSc1	Éowyn
(	(	kIx(	(
<g/>
neteř	neteř	k1gFnSc1	neteř
Théodena	Théodena	k1gFnSc1	Théodena
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
dorazí	dorazit	k5eAaPmIp3nP	dorazit
i	i	k9	i
pomoc	pomoc	k1gFnSc4	pomoc
od	od	k7c2	od
Rohirů	Rohir	k1gInPc2	Rohir
a	a	k8xC	a
gondorských	gondorský	k2eAgNnPc2d1	gondorské
vojsk	vojsko	k1gNnPc2	vojsko
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
Sauronův	Sauronův	k2eAgInSc1d1	Sauronův
útok	útok	k1gInSc1	útok
je	být	k5eAaImIp3nS	být
odražen	odražen	k2eAgInSc1d1	odražen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sauron	Sauron	k1gInSc1	Sauron
tím	ten	k3xDgNnSc7	ten
oslaben	oslabit	k5eAaPmNgInS	oslabit
nebyl	být	k5eNaImAgInS	být
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Gondor	Gondor	k1gMnSc1	Gondor
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Gandalf	Gandalf	k1gMnSc1	Gandalf
se	se	k3xPyFc4	se
od	od	k7c2	od
Faramira	Faramir	k1gInSc2	Faramir
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Frodem	Frod	k1gMnSc7	Frod
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
odlákat	odlákat	k5eAaPmF	odlákat
Sauronovu	Sauronův	k2eAgFnSc4d1	Sauronova
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Frodova	Frodův	k2eAgFnSc1d1	Frodova
pouť	pouť	k1gFnSc1	pouť
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
naděje	naděje	k1gFnSc1	naděje
svobodných	svobodný	k2eAgMnPc2d1	svobodný
národů	národ	k1gInPc2	národ
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Gondorští	Gondorský	k2eAgMnPc1d1	Gondorský
proto	proto	k8xC	proto
shromáždí	shromáždit	k5eAaPmIp3nP	shromáždit
vojsko	vojsko	k1gNnSc4	vojsko
největších	veliký	k2eAgMnPc2d3	veliký
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
vydají	vydat	k5eAaPmIp3nP	vydat
se	se	k3xPyFc4	se
k	k	k7c3	k
Černé	Černá	k1gFnSc3	Černá
bráně	brána	k1gFnSc3	brána
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
,	,	kIx,	,
na	na	k7c4	na
opačnou	opačný	k2eAgFnSc4d1	opačná
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
než	než	k8xS	než
kudy	kudy	k6eAd1	kudy
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
k	k	k7c3	k
Orodruině	Orodruina	k1gFnSc6	Orodruina
Frodo	Frodo	k1gNnSc1	Frodo
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gInSc1	Sauron
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vojsku	vojsko	k1gNnSc6	vojsko
je	být	k5eAaImIp3nS	být
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
že	že	k8xS	že
výprava	výprava	k1gFnSc1	výprava
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
ho	on	k3xPp3gNnSc4	on
svrhnout	svrhnout	k5eAaPmF	svrhnout
a	a	k8xC	a
nositele	nositel	k1gMnPc4	nositel
prstenu	prsten	k1gInSc2	prsten
nastolit	nastolit	k5eAaPmF	nastolit
novým	nový	k2eAgMnSc7d1	nový
pánem	pán	k1gMnSc7	pán
<g/>
;	;	kIx,	;
vrhne	vrhnout	k5eAaPmIp3nS	vrhnout
proto	proto	k8xC	proto
proti	proti	k7c3	proti
Gondorským	Gondorský	k2eAgFnPc3d1	Gondorská
největší	veliký	k2eAgInSc1d3	veliký
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
,	,	kIx,	,
a	a	k8xC	a
přes	přes	k7c4	přes
úporný	úporný	k2eAgInSc4d1	úporný
odpor	odpor	k1gInSc4	odpor
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
tohoto	tento	k3xDgNnSc2	tento
vojska	vojsko	k1gNnSc2	vojsko
začne	začít	k5eAaPmIp3nS	začít
klonit	klonit	k5eAaImF	klonit
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Šestá	šestý	k4xOgFnSc1	šestý
kniha	kniha	k1gFnSc1	kniha
začíná	začínat	k5eAaImIp3nS	začínat
Samovou	Samův	k2eAgFnSc7d1	Samova
snahou	snaha	k1gFnSc7	snaha
osvobodit	osvobodit	k5eAaPmF	osvobodit
Froda	Frodo	k1gNnPc4	Frodo
ze	z	k7c2	z
skřetího	skřetí	k2eAgNnSc2d1	skřetí
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
podaří	podařit	k5eAaPmIp3nS	podařit
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
hobiti	hobit	k1gMnPc1	hobit
se	se	k3xPyFc4	se
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
za	za	k7c4	za
skřety	skřet	k1gMnPc4	skřet
vydávají	vydávat	k5eAaImIp3nP	vydávat
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
Sauronovy	Sauronův	k2eAgFnSc2d1	Sauronova
říše	říš	k1gFnSc2	říš
Mordoru	Mordor	k1gInSc2	Mordor
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hoře	hora	k1gFnSc3	hora
Orodruině	Orodruina	k1gFnSc3	Orodruina
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
jsou	být	k5eAaImIp3nP	být
málem	málem	k6eAd1	málem
odhaleni	odhalen	k2eAgMnPc1d1	odhalen
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
oddíl	oddíl	k1gInSc4	oddíl
skřetů	skřet	k1gMnPc2	skřet
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
velitel	velitel	k1gMnSc1	velitel
je	on	k3xPp3gNnPc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
uprchlé	uprchlý	k2eAgMnPc4d1	uprchlý
skřety	skřet	k1gMnPc4	skřet
<g/>
.	.	kIx.	.
</s>
<s>
Donutí	donutit	k5eAaPmIp3nS	donutit
je	on	k3xPp3gInPc4	on
proto	proto	k8xC	proto
zařadit	zařadit	k5eAaPmF	zařadit
se	se	k3xPyFc4	se
do	do	k7c2	do
oddílu	oddíl	k1gInSc2	oddíl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
severní	severní	k2eAgFnSc3d1	severní
hlavní	hlavní	k2eAgFnSc3d1	hlavní
bráně	brána	k1gFnSc3	brána
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
má	mít	k5eAaImIp3nS	mít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
blížící	blížící	k2eAgNnSc4d1	blížící
se	se	k3xPyFc4	se
Gondorské	Gondorský	k2eAgNnSc1d1	Gondorské
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
brány	brána	k1gFnSc2	brána
se	se	k3xPyFc4	se
v	v	k7c6	v
nastalém	nastalý	k2eAgInSc6d1	nastalý
zmatku	zmatek	k1gInSc6	zmatek
nicméně	nicméně	k8xC	nicméně
oba	dva	k4xCgMnPc1	dva
hobiti	hobit	k1gMnPc1	hobit
vytratí	vytratit	k5eAaPmIp3nP	vytratit
a	a	k8xC	a
vydají	vydat	k5eAaPmIp3nP	vydat
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Froda	Froda	k1gFnSc1	Froda
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
unavuje	unavovat	k5eAaImIp3nS	unavovat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
únavu	únava	k1gFnSc4	únava
umocňuje	umocňovat	k5eAaImIp3nS	umocňovat
i	i	k9	i
staré	starý	k2eAgNnSc4d1	staré
zranění	zranění	k1gNnSc4	zranění
na	na	k7c6	na
Větrově	Větrov	k1gInSc6	Větrov
a	a	k8xC	a
břemeno	břemeno	k1gNnSc1	břemeno
samotného	samotný	k2eAgInSc2d1	samotný
prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
moc	moc	k6eAd1	moc
se	se	k3xPyFc4	se
s	s	k7c7	s
přibližováním	přibližování	k1gNnSc7	přibližování
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dovršení	dovršení	k1gNnSc3	dovršení
všeho	všecek	k3xTgNnSc2	všecek
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
stále	stále	k6eAd1	stále
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
Glum	Glum	k1gInSc4	Glum
a	a	k8xC	a
touží	toužit	k5eAaImIp3nP	toužit
se	se	k3xPyFc4	se
prstenu	prsten	k1gInSc3	prsten
zmocnit	zmocnit	k5eAaPmF	zmocnit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nakonec	nakonec	k6eAd1	nakonec
Frodo	Frodo	k1gNnSc1	Frodo
dorazí	dorazit	k5eAaPmIp3nS	dorazit
k	k	k7c3	k
hoře	hora	k1gFnSc3	hora
a	a	k8xC	a
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
Puklin	puklina	k1gFnPc2	puklina
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
moci	moct	k5eAaImF	moct
prstenu	prsten	k1gInSc2	prsten
a	a	k8xC	a
místo	místo	k7c2	místo
zničení	zničení	k1gNnSc2	zničení
jej	on	k3xPp3gMnSc4	on
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
okamžitě	okamžitě	k6eAd1	okamžitě
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
Sauron	Sauron	k1gInSc4	Sauron
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
Barad-dû	Baradû	k1gFnSc6	Barad-dû
a	a	k8xC	a
pošle	poslat	k5eAaPmIp3nS	poslat
k	k	k7c3	k
hoře	hora	k1gFnSc3	hora
Prstenové	prstenový	k2eAgInPc4d1	prstenový
přízraky	přízrak	k1gInPc4	přízrak
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
právě	právě	k6eAd1	právě
bojují	bojovat	k5eAaImIp3nP	bojovat
u	u	k7c2	u
Černé	Černé	k2eAgFnSc2d1	Černé
brány	brána	k1gFnSc2	brána
Mordoru	Mordor	k1gInSc2	Mordor
s	s	k7c7	s
gondorským	gondorský	k2eAgNnSc7d1	gondorské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
Středozemě	Středozem	k1gFnSc2	Středozem
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
zpečetěn	zpečetěn	k2eAgMnSc1d1	zpečetěn
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Frodo	Frodo	k1gNnSc1	Frodo
by	by	kYmCp3nS	by
ani	ani	k9	ani
s	s	k7c7	s
prstenem	prsten	k1gInSc7	prsten
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
nazgû	nazgû	k?	nazgû
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
<g/>
,	,	kIx,	,
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
však	však	k9	však
Glum	Glum	k1gInSc1	Glum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šílené	šílený	k2eAgFnSc6d1	šílená
snaze	snaha	k1gFnSc6	snaha
zmocnit	zmocnit	k5eAaPmF	zmocnit
se	se	k3xPyFc4	se
prstenu	prsten	k1gInSc3	prsten
Froda	Frodo	k1gNnSc2	Frodo
přepadne	přepadnout	k5eAaPmIp3nS	přepadnout
a	a	k8xC	a
odkousne	odkousnout	k5eAaPmIp3nS	odkousnout
mu	on	k3xPp3gNnSc3	on
prst	prst	k1gInSc4	prst
i	i	k8xC	i
s	s	k7c7	s
prstenem	prsten	k1gInSc7	prsten
<g/>
,	,	kIx,	,
v	v	k7c6	v
radosti	radost	k1gFnSc6	radost
nad	nad	k7c7	nad
znovunabytím	znovunabytí	k1gNnSc7	znovunabytí
prstenu	prsten	k1gInSc2	prsten
však	však	k9	však
ztratí	ztratit	k5eAaPmIp3nS	ztratit
rovnováhu	rovnováha	k1gFnSc4	rovnováha
a	a	k8xC	a
zřítí	zřítit	k5eAaPmIp3nS	zřítit
se	se	k3xPyFc4	se
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
i	i	k9	i
s	s	k7c7	s
prstenem	prsten	k1gInSc7	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Frodovo	Frodův	k2eAgNnSc4d1	Frodovo
poslání	poslání	k1gNnSc4	poslání
tak	tak	k9	tak
nakonec	nakonec	k6eAd1	nakonec
přece	přece	k9	přece
dojde	dojít	k5eAaPmIp3nS	dojít
naplnění	naplnění	k1gNnSc4	naplnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prsten	prsten	k1gInSc1	prsten
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Sauronova	Sauronův	k2eAgFnSc1d1	Sauronova
věž	věž	k1gFnSc1	věž
zřítí	zřítit	k5eAaPmIp3nS	zřítit
<g/>
,	,	kIx,	,
Sauronova	Sauronův	k2eAgNnPc1d1	Sauronovo
vojska	vojsko	k1gNnPc1	vojsko
jsou	být	k5eAaImIp3nP	být
ochromena	ochromen	k2eAgNnPc1d1	ochromeno
a	a	k8xC	a
Sauron	Sauron	k1gMnSc1	Sauron
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
bezmocného	bezmocný	k2eAgMnSc4d1	bezmocný
ducha	duch	k1gMnSc4	duch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zmizí	zmizet	k5eAaPmIp3nS	zmizet
do	do	k7c2	do
dáli	dál	k1gFnSc2	dál
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgFnPc1d1	závěrečná
kapitoly	kapitola	k1gFnPc1	kapitola
líčí	líčit	k5eAaImIp3nP	líčit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
panství	panství	k1gNnSc1	panství
Gondoru	Gondor	k1gInSc2	Gondor
nad	nad	k7c7	nad
celou	celý	k2eAgFnSc7d1	celá
západní	západní	k2eAgFnSc7d1	západní
Středozemí	Středozem	k1gFnSc7	Středozem
a	a	k8xC	a
jak	jak	k6eAd1	jak
opět	opět	k6eAd1	opět
nastoupí	nastoupit	k5eAaPmIp3nS	nastoupit
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
třetího	třetí	k4xOgInSc2	třetí
dílu	díl	k1gInSc2	díl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
Aragorn	Aragorn	k1gInSc1	Aragorn
(	(	kIx(	(
<g/>
Elessar	Elessar	k1gInSc1	Elessar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
čtyři	čtyři	k4xCgMnPc1	čtyři
hobiti	hobit	k1gMnPc1	hobit
vracejí	vracet	k5eAaImIp3nP	vracet
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
jej	on	k3xPp3gMnSc4	on
opouštěli	opouštět	k5eAaImAgMnP	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
se	se	k3xPyFc4	se
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vyhnání	vyhnání	k1gNnSc6	vyhnání
z	z	k7c2	z
Orthanku	Orthanka	k1gFnSc4	Orthanka
usadil	usadit	k5eAaPmAgMnS	usadit
Saruman	Saruman	k1gMnSc1	Saruman
s	s	k7c7	s
Červivcem	Červivec	k1gMnSc7	Červivec
a	a	k8xC	a
hobity	hobit	k1gMnPc7	hobit
si	se	k3xPyFc3	se
zotročil	zotročit	k5eAaPmAgMnS	zotročit
<g/>
.	.	kIx.	.
</s>
<s>
Frodovi	Frodův	k2eAgMnPc1d1	Frodův
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
Kraj	kraj	k1gInSc4	kraj
osvobodit	osvobodit	k5eAaPmF	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Gríma	Gríma	k1gFnSc1	Gríma
Červivec	Červivec	k1gMnSc1	Červivec
již	již	k6eAd1	již
nechtěl	chtít	k5eNaImAgMnS	chtít
Sarumanovi	Saruman	k1gMnSc3	Saruman
sloužit	sloužit	k5eAaImF	sloužit
a	a	k8xC	a
zabil	zabít	k5eAaPmAgMnS	zabít
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgMnS	zabít
hobity	hobit	k1gMnPc7	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
končí	končit	k5eAaImIp3nS	končit
konstatováním	konstatování	k1gNnSc7	konstatování
<g/>
,	,	kIx,	,
že	že	k8xS	že
Třetí	třetí	k4xOgInSc1	třetí
věk	věk	k1gInSc1	věk
Středozemě	Středozem	k1gFnSc2	Středozem
skončil	skončit	k5eAaPmAgInS	skončit
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
panování	panování	k1gNnSc4	panování
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
knihy	kniha	k1gFnSc2	kniha
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
obsáhlé	obsáhlý	k2eAgInPc1d1	obsáhlý
dodatky	dodatek	k1gInPc1	dodatek
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
vydáních	vydání	k1gNnPc6	vydání
i	i	k8xC	i
rejstřík	rejstřík	k1gInSc1	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
A	a	k9	a
představuje	představovat	k5eAaImIp3nS	představovat
zhuštěné	zhuštěný	k2eAgNnSc1d1	zhuštěné
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
Númenoru	Númenor	k1gInSc2	Númenor
<g/>
,	,	kIx,	,
Arnoru	Arnor	k1gMnSc3	Arnor
<g/>
,	,	kIx,	,
Gondoru	Gondor	k1gMnSc3	Gondor
<g/>
,	,	kIx,	,
Rohanu	Rohan	k1gMnSc3	Rohan
a	a	k8xC	a
Durinova	Durinův	k2eAgInSc2d1	Durinův
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něj	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
včleněna	včleněn	k2eAgFnSc1d1	včleněna
také	také	k9	také
část	část	k1gFnSc1	část
příběhu	příběh	k1gInSc2	příběh
Aragorna	Aragorno	k1gNnSc2	Aragorno
a	a	k8xC	a
Arwen	Arwna	k1gFnPc2	Arwna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
vypsání	vypsání	k1gNnSc4	vypsání
rodu	rod	k1gInSc2	rod
trpaslíků	trpaslík	k1gMnPc2	trpaslík
z	z	k7c2	z
Ereboru	Erebor	k1gInSc2	Erebor
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
B	B	kA	B
představuje	představovat	k5eAaImIp3nS	představovat
chronologický	chronologický	k2eAgInSc4d1	chronologický
záznam	záznam	k1gInSc4	záznam
událostí	událost	k1gFnPc2	událost
druhého	druhý	k4xOgInSc2	druhý
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
události	událost	k1gFnSc3	událost
je	být	k5eAaImIp3nS	být
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
datum	datum	k1gNnSc1	datum
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
někde	někde	k6eAd1	někde
jen	jen	k9	jen
přibližné	přibližný	k2eAgFnSc3d1	přibližná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc4	část
Velké	velký	k2eAgInPc4d1	velký
roky	rok	k1gInPc4	rok
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
podrobnějšímu	podrobný	k2eAgNnSc3d2	podrobnější
zápisu	zápis	k1gInSc6	zápis
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
let	léto	k1gNnPc2	léto
3018	[number]	k4	3018
až	až	k6eAd1	až
3021	[number]	k4	3021
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
hlavní	hlavní	k2eAgInSc1d1	hlavní
děj	děj	k1gInSc1	děj
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
obdobný	obdobný	k2eAgInSc1d1	obdobný
chronologický	chronologický	k2eAgInSc1d1	chronologický
záznam	záznam	k1gInSc1	záznam
Pozdější	pozdní	k2eAgFnSc2d2	pozdější
události	událost	k1gFnSc2	událost
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
členů	člen	k1gInPc2	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
C	C	kA	C
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
rodokmeny	rodokmen	k1gInPc4	rodokmen
Pytlíků	pytlík	k1gInPc2	pytlík
z	z	k7c2	z
Hobitína	Hobitín	k1gInSc2	Hobitín
<g/>
,	,	kIx,	,
Bralů	Bral	k1gInPc2	Bral
z	z	k7c2	z
Velkých	velký	k2eAgInPc2d1	velký
Pelouchů	pelouch	k1gInPc2	pelouch
<g/>
,	,	kIx,	,
Brandorádů	Brandorád	k1gInPc2	Brandorád
z	z	k7c2	z
Rádovska	Rádovsko	k1gNnSc2	Rádovsko
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Starodávný	starodávný	k2eAgInSc1d1	starodávný
rodokmen	rodokmen	k1gInSc1	rodokmen
pana	pan	k1gMnSc2	pan
Samvěda	Samvěd	k1gMnSc2	Samvěd
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
D	D	kA	D
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
kalendářích	kalendář	k1gInPc6	kalendář
hobitů	hobit	k1gMnPc2	hobit
<g/>
,	,	kIx,	,
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
E	E	kA	E
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
výslovnosti	výslovnost	k1gFnSc6	výslovnost
a	a	k8xC	a
písmech	písmo	k1gNnPc6	písmo
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
době	doba	k1gFnSc6	doba
děje	dít	k5eAaImIp3nS	dít
Pána	pán	k1gMnSc4	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
jmen	jméno	k1gNnPc2	jméno
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
angličtiny	angličtina	k1gFnSc2	angličtina
i	i	k8xC	i
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
F	F	kA	F
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
jazycích	jazyk	k1gInPc6	jazyk
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
lidí	člověk	k1gMnPc2	člověk
hobitů	hobit	k1gMnPc2	hobit
a	a	k8xC	a
ostatních	ostatní	k2eAgNnPc2d1	ostatní
plemen	plemeno	k1gNnPc2	plemeno
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
O	o	k7c6	o
překladu	překlad	k1gInSc6	překlad
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
k	k	k7c3	k
fiktivnímu	fiktivní	k2eAgInSc3d1	fiktivní
překladu	překlad	k1gInSc3	překlad
z	z	k7c2	z
Červené	Červené	k2eAgFnSc2d1	Červené
knihy	kniha	k1gFnSc2	kniha
psané	psaný	k2eAgFnSc2d1	psaná
v	v	k7c6	v
západštině	západština	k1gFnSc6	západština
<g/>
.	.	kIx.	.
</s>
