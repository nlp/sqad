<s>
Blud	blud	k1gInSc4	blud
je	být	k5eAaImIp3nS	být
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
a	a	k8xC	a
nevývratné	vývratný	k2eNgNnSc1d1	nevývratné
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
nesprávném	správný	k2eNgNnSc6d1	nesprávné
odvození	odvození	k1gNnSc6	odvození
závěru	závěr	k1gInSc2	závěr
ze	z	k7c2	z
zevní	zevní	k2eAgFnSc2d1	zevní
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Blud	blud	k1gInSc1	blud
musí	muset	k5eAaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
tyto	tento	k3xDgFnPc4	tento
charakteristiky	charakteristika	k1gFnPc4	charakteristika
<g/>
:	:	kIx,	:
Nepravdivost	Nepravdivost	k1gFnSc1	Nepravdivost
Nevývratnost	nevývratnost	k1gFnSc1	nevývratnost
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
daného	daný	k2eAgNnSc2d1	dané
jedince	jedinko	k6eAd1	jedinko
nelze	lze	k6eNd1	lze
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
žádnými	žádný	k3yNgInPc7	žádný
logickými	logický	k2eAgInPc7d1	logický
argumenty	argument	k1gInPc7	argument
Vliv	vliv	k1gInSc1	vliv
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
–	–	k?	–
daný	daný	k2eAgMnSc1d1	daný
jedinec	jedinec	k1gMnSc1	jedinec
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
blud	blud	k1gInSc1	blud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
pozornosti	pozornost	k1gFnSc2	pozornost
daného	daný	k2eAgMnSc2d1	daný
jedince	jedinec	k1gMnSc2	jedinec
Chorobný	chorobný	k2eAgInSc1d1	chorobný
vznik	vznik	k1gInSc1	vznik
–	–	k?	–
blud	blud	k1gInSc1	blud
je	být	k5eAaImIp3nS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
u	u	k7c2	u
jedince	jedinec	k1gMnSc2	jedinec
2	[number]	k4	2
<g/>
.	.	kIx.	.
signální	signální	k2eAgFnSc7d1	signální
soustavou	soustava	k1gFnSc7	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
představa	představa	k1gFnSc1	představa
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
halucinacemi	halucinace	k1gFnPc7	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
mozek	mozek	k1gInSc4	mozek
nemocného	nemocný	k1gMnSc2	nemocný
vjemy	vjem	k1gInPc1	vjem
z	z	k7c2	z
vně	vně	k6eAd1	vně
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
mluvení	mluvení	k1gNnSc1	mluvení
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
ozvučení	ozvučení	k1gNnSc4	ozvučení
vlastních	vlastní	k2eAgFnPc2d1	vlastní
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
reaguje	reagovat	k5eAaBmIp3nS	reagovat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
hlavě	hlava	k1gFnSc6	hlava
na	na	k7c4	na
danou	daný	k2eAgFnSc4d1	daná
situaci	situace	k1gFnSc4	situace
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
signální	signální	k2eAgFnSc6d1	signální
soustavě	soustava	k1gFnSc6	soustava
tvorbu	tvorba	k1gFnSc4	tvorba
schémat	schéma	k1gNnPc2	schéma
<g/>
,	,	kIx,	,
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
napojeny	napojit	k5eAaPmNgFnP	napojit
na	na	k7c4	na
mozek	mozek	k1gInSc4	mozek
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Halucinace	halucinace	k1gFnPc1	halucinace
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
mluvení	mluvení	k1gNnSc1	mluvení
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
představy	představa	k1gFnPc4	představa
<g/>
,	,	kIx,	,
komunikační	komunikační	k2eAgNnPc4d1	komunikační
schémata	schéma	k1gNnPc4	schéma
<g/>
,	,	kIx,	,
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
,	,	kIx,	,
roky	rok	k1gInPc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
halucinacemi	halucinace	k1gFnPc7	halucinace
trýzněn	trýznit	k5eAaImNgInS	trýznit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
štvaným	štvaný	k2eAgNnSc7d1	štvané
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
signální	signální	k2eAgFnSc1d1	signální
soustava	soustava	k1gFnSc1	soustava
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
obrazy	obraz	k1gInPc4	obraz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
<g/>
/	/	kIx~	/
<g/>
co	co	k8xS	co
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
to	ten	k3xDgNnSc4	ten
člověk	člověk	k1gMnSc1	člověk
bere	brát	k5eAaImIp3nS	brát
jako	jako	k9	jako
normální	normální	k2eAgFnSc1d1	normální
věc	věc	k1gFnSc1	věc
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
sám	sám	k3xTgMnSc1	sám
nepřišel	přijít	k5eNaPmAgMnS	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
blud	blud	k1gInSc1	blud
<g/>
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
svou	svůj	k3xOyFgFnSc4	svůj
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
,	,	kIx,	,
komplexní	komplexní	k2eAgFnSc4d1	komplexní
<g/>
,	,	kIx,	,
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
slyší	slyšet	k5eAaImIp3nS	slyšet
<g/>
,	,	kIx,	,
vnímá	vnímat	k5eAaImIp3nS	vnímat
<g/>
,	,	kIx,	,
cítí	cítit	k5eAaImIp3nS	cítit
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
všech	všecek	k3xTgInPc2	všecek
těchto	tento	k3xDgInPc6	tento
smysly	smysl	k1gInPc4	smysl
vnímaných	vnímaný	k2eAgInPc2d1	vnímaný
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Blud	blud	k1gInSc1	blud
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
individuální	individuální	k2eAgNnSc1d1	individuální
či	či	k8xC	či
kolektivní	kolektivní	k2eAgNnSc1d1	kolektivní
(	(	kIx(	(
<g/>
pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
sdílí	sdílet	k5eAaImIp3nS	sdílet
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
folie	folie	k1gFnSc1	folie
à	à	k?	à
deux	deux	k1gInSc1	deux
(	(	kIx(	(
<g/>
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
šílenství	šílenství	k1gNnSc1	šílenství
ve	v	k7c6	v
dvou	dva	k4xCgFnPc2	dva
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stejná	stejný	k2eAgFnSc1d1	stejná
duševní	duševní	k2eAgFnSc1d1	duševní
choroba	choroba	k1gFnSc1	choroba
projeví	projevit	k5eAaPmIp3nS	projevit
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
okamžiku	okamžik	k1gInSc6	okamžik
u	u	k7c2	u
dvou	dva	k4xCgFnPc2	dva
navzájem	navzájem	k6eAd1	navzájem
si	se	k3xPyFc3	se
blízkých	blízký	k2eAgFnPc2d1	blízká
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Megalomanické	megalomanický	k2eAgNnSc1d1	megalomanické
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
zvláštním	zvláštní	k2eAgInSc6d1	zvláštní
významu	význam	k1gInSc6	význam
vlastní	vlastní	k2eAgFnSc2d1	vlastní
osobnosti	osobnost	k1gFnSc2	osobnost
Extrapotenční	Extrapotenční	k2eAgFnSc2d1	Extrapotenční
–	–	k?	–
přesvědčení	přesvědčení	k1gNnPc4	přesvědčení
o	o	k7c6	o
nadpřirozených	nadpřirozený	k2eAgFnPc6d1	nadpřirozená
schopnostech	schopnost	k1gFnPc6	schopnost
či	či	k8xC	či
nadání	nadání	k1gNnSc1	nadání
Originární	Originární	k2eAgNnSc1d1	Originární
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
vznešeném	vznešený	k2eAgInSc6d1	vznešený
původu	původ	k1gInSc6	původ
Inventorní	Inventorní	k2eAgNnSc1d1	Inventorní
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
daného	daný	k2eAgMnSc2d1	daný
jedince	jedinec	k1gMnSc2	jedinec
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vynálezcem	vynálezce	k1gMnSc7	vynálezce
významného	významný	k2eAgInSc2d1	významný
objevu	objev	k1gInSc2	objev
Reformátorské	reformátorský	k2eAgFnSc2d1	reformátorská
–	–	k?	–
přesvědčení	přesvědčení	k1gNnPc4	přesvědčení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
daný	daný	k2eAgMnSc1d1	daný
jedinec	jedinec	k1gMnSc1	jedinec
provede	provést	k5eAaPmIp3nS	provést
významné	významný	k2eAgFnPc4d1	významná
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
společnosti	společnost	k1gFnSc3	společnost
Mesiášské	mesiášský	k2eAgFnSc3d1	mesiášská
(	(	kIx(	(
<g/>
religiózní	religiózní	k2eAgFnSc3d1	religiózní
<g/>
)	)	kIx)	)
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
daného	daný	k2eAgMnSc2d1	daný
jedince	jedinec	k1gMnSc2	jedinec
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
spasitelem	spasitel	k1gMnSc7	spasitel
Erotomanické	Erotomanický	k2eAgFnSc2d1	Erotomanický
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
o	o	k7c6	o
neodolatelnosti	neodolatelnost	k1gFnSc6	neodolatelnost
pro	pro	k7c4	pro
druhé	druhý	k4xOgNnSc4	druhý
pohlaví	pohlaví	k1gNnSc4	pohlaví
Eternity	eternit	k1gInPc1	eternit
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
je	být	k5eAaImIp3nS	být
nesmrtelný	smrtelný	k2eNgMnSc1d1	nesmrtelný
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezemře	zemřít	k5eNaPmIp3nS	zemřít
Insuficientní	Insuficientní	k2eAgNnPc4d1	Insuficientní
–	–	k?	–
přesvědčení	přesvědčení	k1gNnPc4	přesvědčení
o	o	k7c6	o
vlastní	vlastní	k2eAgFnSc6d1	vlastní
neschopnosti	neschopnost	k1gFnSc6	neschopnost
Autoakuzační	Autoakuzační	k2eAgNnSc1d1	Autoakuzační
–	–	k?	–
sebeobviňování	sebeobviňování	k1gNnSc1	sebeobviňování
za	za	k7c4	za
různá	různý	k2eAgNnPc4d1	různé
<g />
.	.	kIx.	.
</s>
<s>
neštěstí	štěstit	k5eNaImIp3nP	štěstit
atp.	atp.	kA	atp.
Obavné	obavný	k2eAgInPc4d1	obavný
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přihodí	přihodit	k5eAaPmIp3nS	přihodit
katastrofa	katastrofa	k1gFnSc1	katastrofa
Ruinační	Ruinační	k2eAgFnSc1d1	Ruinační
–	–	k?	–
přesvědčení	přesvědčení	k1gNnPc4	přesvědčení
o	o	k7c6	o
totálním	totální	k2eAgNnSc6d1	totální
zchudnutí	zchudnutí	k1gNnSc6	zchudnutí
Negační	negační	k2eAgFnSc2d1	negační
–	–	k?	–
popírá	popírat	k5eAaImIp3nS	popírat
existenci	existence	k1gFnSc4	existence
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
vlastní	vlastní	k2eAgNnSc4d1	vlastní
či	či	k8xC	či
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgMnSc4d1	jiný
–	–	k?	–
např.	např.	kA	např.
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
Enormity	enormita	k1gFnPc1	enormita
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
svojí	svůj	k3xOyFgFnSc7	svůj
existencí	existence	k1gFnSc7	existence
či	či	k8xC	či
činností	činnost	k1gFnSc7	činnost
přivodí	přivodit	k5eAaPmIp3nP	přivodit
katastrofu	katastrofa	k1gFnSc4	katastrofa
Eternity	eternit	k1gInPc1	eternit
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
žít	žít	k5eAaImF	žít
navždy	navždy	k6eAd1	navždy
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
trpěl	trpět	k5eAaImAgMnS	trpět
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
hříchy	hřích	k1gInPc4	hřích
Hypochondrické	hypochondrický	k2eAgInPc4d1	hypochondrický
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
daného	daný	k2eAgMnSc2d1	daný
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nP	trpět
nějakou	nějaký	k3yIgFnSc7	nějaký
chorobou	choroba	k1gFnSc7	choroba
–	–	k?	–
většinou	většina	k1gFnSc7	většina
nevyléčitelnou	vyléčitelný	k2eNgFnSc7d1	nevyléčitelná
Dysmorfofobické	Dysmorfofobická	k1gFnPc4	Dysmorfofobická
–	–	k?	–
Přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
znetvořená	znetvořený	k2eAgFnSc1d1	znetvořená
nebo	nebo	k8xC	nebo
zohyzděná	zohyzděný	k2eAgFnSc1d1	zohyzděná
Paranoidní	paranoidní	k2eAgFnSc1d1	paranoidní
–	–	k?	–
připisuje	připisovat	k5eAaImIp3nS	připisovat
věcem	věc	k1gFnPc3	věc
a	a	k8xC	a
situacím	situace	k1gFnPc3	situace
kolem	kolo	k1gNnSc7	kolo
sebe	sebe	k3xPyFc4	sebe
význam	význam	k1gInSc1	význam
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
vlastní	vlastní	k2eAgFnSc3d1	vlastní
osobě	osoba	k1gFnSc3	osoba
Perzekuční	perzekuční	k2eAgNnPc4d1	perzekuční
–	–	k?	–
přesvědčení	přesvědčení	k1gNnPc4	přesvědčení
o	o	k7c6	o
pronásledování	pronásledování	k1gNnSc6	pronásledování
a	a	k8xC	a
ohrožení	ohrožení	k1gNnSc6	ohrožení
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
osoby	osoba	k1gFnPc4	osoba
Kverulační	Kverulační	k2eAgFnPc4d1	Kverulační
–	–	k?	–
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
perzekuci	perzekuce	k1gFnSc6	perzekuce
si	se	k3xPyFc3	se
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
na	na	k7c4	na
policii	policie	k1gFnSc4	policie
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
instituce	instituce	k1gFnPc4	instituce
<g/>
,	,	kIx,	,
podává	podávat	k5eAaImIp3nS	podávat
žaloby	žaloba	k1gFnSc2	žaloba
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
se	se	k3xPyFc4	se
až	až	k9	až
k	k	k7c3	k
nejvyšším	vysoký	k2eAgNnPc3d3	nejvyšší
místům	místo	k1gNnPc3	místo
Emulační	emulační	k2eAgFnSc3d1	emulační
(	(	kIx(	(
<g/>
žárlivecké	žárlivecký	k2eAgFnSc3d1	žárlivecký
<g/>
)	)	kIx)	)
–	–	k?	–
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
nevěře	nevěra	k1gFnSc6	nevěra
partnera	partner	k1gMnSc2	partner
Transformační	transformační	k2eAgFnSc2d1	transformační
–	–	k?	–
chorobný	chorobný	k2eAgInSc4d1	chorobný
pocit	pocit	k1gInSc4	pocit
změny	změna	k1gFnSc2	změna
osobnosti	osobnost	k1gFnSc2	osobnost
Metamorfózy	metamorfóza	k1gFnSc2	metamorfóza
–	–	k?	–
pocit	pocit	k1gInSc1	pocit
změny	změna	k1gFnSc2	změna
v	v	k7c4	v
jinou	jiný	k2eAgFnSc4d1	jiná
bytost	bytost	k1gFnSc4	bytost
–	–	k?	–
např.	např.	kA	např.
zvíře	zvíře	k1gNnSc1	zvíře
Kosmické	kosmický	k2eAgNnSc1d1	kosmické
–	–	k?	–
obsahem	obsah	k1gInSc7	obsah
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
civilizace	civilizace	k1gFnSc2	civilizace
(	(	kIx(	(
<g/>
ovlivňování	ovlivňování	k1gNnSc1	ovlivňování
<g/>
,	,	kIx,	,
pozorování	pozorování	k1gNnSc1	pozorování
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Logický	logický	k2eAgInSc1d1	logický
klam	klam	k1gInSc1	klam
Deprese	deprese	k1gFnSc1	deprese
Paranoia	paranoia	k1gFnSc1	paranoia
</s>
