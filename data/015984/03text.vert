<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Dalby	Dalba	k1gFnSc2
Söderskog	Söderskoga	k1gFnPc2
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuNárodní	infoboxuNárodní	k2eAgInPc1d1
park	park	k1gInSc1
Dalby	Dalba	k1gFnSc2
SöderskogIUCN	SöderskogIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Vyhlášení	vyhlášení	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1918	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
36,56	36,56	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
55	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Dalby	Dalba	k1gFnSc2
Söderskog	Söderskoga	k1gFnPc2
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc1
</s>
<s>
www.sverigesnationalparker.se/Dalbysoderskog/	www.sverigesnationalparker.se/Dalbysoderskog/	k?
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Národní	národní	k2eAgFnSc2d1
Dalby	Dalba	k1gFnSc2
Söderskog	Söderskoga	k1gFnPc2
(	(	kIx(
<g/>
švédsky	švédsky	k6eAd1
Dalby	Dalba	k1gFnPc1
Söderskogs	Söderskogsa	k1gFnPc2
nationalpark	nationalpark	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
<g/>
,	,	kIx,
rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
okresu	okres	k1gInSc2
Lund	Lunda	k1gFnPc2
v	v	k7c6
regionu	region	k1gInSc6
Skå	Skå	k1gMnSc5
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
30	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Lund	Lunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
druhý	druhý	k4xOgInSc1
nejjižněji	jižně	k6eAd3
položený	položený	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
s	s	k7c7
cílem	cíl	k1gInSc7
ochrany	ochrana	k1gFnSc2
listnatého	listnatý	k2eAgInSc2d1
lesa	les	k1gInSc2
v	v	k7c6
rovinách	rovina	k1gFnPc6
Skå	Skå	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
parku	park	k1gInSc2
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
36	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vápencovém	vápencový	k2eAgNnSc6d1
a	a	k8xC
křídovém	křídový	k2eAgNnSc6d1
podloží	podloží	k1gNnSc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
např.	např.	kA
tyto	tento	k3xDgInPc4
druhy	druh	k1gInPc4
<g/>
:	:	kIx,
jasan	jasan	k1gInSc4
(	(	kIx(
<g/>
Fraxinus	Fraxinus	k1gMnSc1
excelsior	excelsior	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jilm	jilm	k1gInSc1
(	(	kIx(
<g/>
Ulmus	Ulmus	k1gInSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dub	dub	k1gInSc1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
nebo	nebo	k8xC
ve	v	k7c6
Skandinávii	Skandinávie	k1gFnSc6
dosti	dosti	k6eAd1
řídký	řídký	k2eAgInSc4d1
buk	buk	k1gInSc4
(	(	kIx(
<g/>
Fagus	Fagus	k1gMnSc1
sylvatica	sylvatica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
i	i	k9
s	s	k7c7
olší	olše	k1gFnSc7
lepkavou	lepkavý	k2eAgFnSc7d1
(	(	kIx(
<g/>
Alnus	Alnus	k1gInSc1
glutinosa	glutinosa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jírovcem	jírovec	k1gInSc7
maďalem	maďal	k1gInSc7
(	(	kIx(
<g/>
Aesculus	Aesculus	k1gInSc1
hippocastanum	hippocastanum	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jabloní	jabloň	k1gFnSc7
(	(	kIx(
<g/>
Malus	Malus	k1gInSc1
sylvestris	sylvestris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lípou	lípa	k1gFnSc7
(	(	kIx(
<g/>
Tilia	Tilia	k1gFnSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
javorem	javor	k1gInSc7
(	(	kIx(
<g/>
Acer	Acer	k1gInSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
vrbou	vrba	k1gFnSc7
(	(	kIx(
<g/>
Salix	Salix	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejkrásnější	krásný	k2eAgInSc1d3
pohled	pohled	k1gInSc1
skýtá	skýtat	k5eAaImIp3nS
park	park	k1gInSc4
na	na	k7c6
jaře	jaro	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
kvetou	kvést	k5eAaImIp3nP
hajní	hajní	k2eAgFnPc1d1
byliny	bylina	k1gFnPc1
<g/>
:	:	kIx,
sasanka	sasanka	k1gFnSc1
<g/>
,	,	kIx,
blatouch	blatouch	k1gInSc1
<g/>
,	,	kIx,
křivatec	křivatec	k1gInSc1
<g/>
,	,	kIx,
dymnivka	dymnivka	k1gFnSc1
<g/>
,	,	kIx,
orsej	orsej	k1gInSc1
jarní	jarní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Dalby	Dalba	k1gFnSc2
Söderskog	Söderskoga	k1gFnPc2
je	být	k5eAaImIp3nS
snadno	snadno	k6eAd1
dostupný	dostupný	k2eAgInSc1d1
pro	pro	k7c4
turisty	turist	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
je	být	k5eAaImIp3nS
vyznačeno	vyznačit	k5eAaPmNgNnS
několik	několik	k4yIc1
turistických	turistický	k2eAgFnPc2d1
stezek	stezka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zhruba	zhruba	k6eAd1
polovina	polovina	k1gFnSc1
rozlohy	rozloha	k1gFnSc2
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
obehnána	obehnat	k5eAaPmNgFnS
valem	val	k1gInSc7
o	o	k7c6
výšce	výška	k1gFnSc6
1	#num#	k4
m	m	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vypovídá	vypovídat	k5eAaImIp3nS,k5eAaPmIp3nS
o	o	k7c4
existenci	existence	k1gFnSc4
pravěkého	pravěký	k2eAgNnSc2d1
hradiště	hradiště	k1gNnSc2
na	na	k7c6
tomto	tento	k3xDgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Protected	Protected	k1gMnSc1
Areas	Areas	k1gMnSc1
(	(	kIx(
<g/>
National	National	k1gFnSc1
Parks	Parksa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
Dalby	Dalba	k1gFnSc2
Söderskog	Söderskog	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
parku	park	k1gInSc6
na	na	k7c6
webu	web	k1gInSc6
Švédské	švédský	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Galerie	galerie	k1gFnPc1
Národní	národní	k2eAgFnPc1d1
park	park	k1gInSc4
Dalby	Dalba	k1gFnSc2
Söderskog	Söderskog	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
</s>
<s>
Abisko	Abisko	k6eAd1
•	•	k?
Ängsö	Ängsö	k1gFnSc1
•	•	k?
Å	Å	k1gFnPc2
•	•	k?
Björnlandet	Björnlandet	k1gMnSc1
•	•	k?
Blå	Blå	k1gMnSc1
Jungfrun	Jungfrun	k1gMnSc1
•	•	k?
Dalby	Dalba	k1gFnSc2
Söderskog	Söderskog	k1gMnSc1
•	•	k?
Djurö	Djurö	k1gFnSc1
•	•	k?
Färnebofjärden	Färnebofjärdna	k1gFnPc2
•	•	k?
Fulufjället	Fulufjället	k1gInSc1
•	•	k?
Garphyttan	Garphyttan	k1gInSc1
•	•	k?
Gotska	Gotsk	k1gInSc2
Sandön	Sandön	k1gMnSc1
•	•	k?
Hamra	Hamra	k1gMnSc1
•	•	k?
souostroví	souostroví	k1gNnSc2
Haparanda	Haparanda	k1gFnSc1
•	•	k?
Kosterhavet	Kosterhavet	k1gInSc1
•	•	k?
Muddus	Muddus	k1gInSc1
•	•	k?
Norra	Norra	k1gFnSc1
Kvill	Kvillum	k1gNnPc2
•	•	k?
Padjelanta	Padjelant	k1gMnSc2
•	•	k?
Pieljekaise	Pieljekaise	k1gFnSc2
•	•	k?
Sarek	Sarek	k1gInSc1
•	•	k?
Skuleskogen	Skuleskogen	k1gInSc1
•	•	k?
Söderå	Söderå	k1gInSc1
•	•	k?
Sonfjället	Sonfjället	k1gInSc1
•	•	k?
Stenshuvud	Stenshuvud	k1gInSc1
•	•	k?
Stora	Stor	k1gInSc2
Sjöfallet	Sjöfallet	k1gInSc1
•	•	k?
Store	Stor	k1gMnSc5
Mosse	Moss	k1gMnSc5
•	•	k?
Tiveden	Tiveden	k2eAgInSc1d1
•	•	k?
Töfsingdalen	Töfsingdalen	k2eAgInSc1d1
•	•	k?
Tresticklan	Tresticklan	k1gInSc1
•	•	k?
Tyresta	Tyrest	k1gMnSc2
•	•	k?
Vadvetjå	Vadvetjå	k1gMnSc2
</s>
<s>
↑	↑	k?
Protected	Protected	k1gMnSc1
Areas	Areas	k1gMnSc1
(	(	kIx(
<g/>
National	National	k1gFnSc1
Parks	Parksa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
