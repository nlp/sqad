<s>
Národní	národní	k2eAgFnSc2d1
Dalby	Dalba	k1gFnSc2
Söderskog	Söderskoga	k1gFnPc2
(	(	kIx(
<g/>
švédsky	švédsky	k6eAd1
Dalby	Dalba	k1gFnPc1
Söderskogs	Söderskogsa	k1gFnPc2
nationalpark	nationalpark	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
<g/>
,	,	kIx,
rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
okresu	okres	k1gInSc2
Lund	Lunda	k1gFnPc2
v	v	k7c6
regionu	region	k1gInSc6
Skå	Skå	k1gMnSc5
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
30	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Lund	Lunda	k1gFnPc2
<g/>
.	.	kIx.
</s>