<s>
Chrám	chrám	k1gInSc1	chrám
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
bazilika	bazilika	k1gFnSc1	bazilika
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
církvích	církev	k1gFnPc6	církev
kostel	kostel	k1gInSc4	kostel
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
–	–	k?	–
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
Ν	Ν	k?	Ν
τ	τ	k?	τ
Α	Α	k?	Α
<g/>
,	,	kIx,	,
Naos	Naos	k1gInSc1	Naos
tis	tis	k1gInSc1	tis
Anastaseos	Anastaseos	k1gInSc4	Anastaseos
<g/>
;	;	kIx,	;
arménsky	arménsky	k6eAd1	arménsky
<g/>
:	:	kIx,	:
Surp	Surp	k1gMnSc1	Surp
Haruthjun	Haruthjun	k1gMnSc1	Haruthjun
<g/>
;	;	kIx,	;
gruzínsky	gruzínsky	k6eAd1	gruzínsky
<g/>
:	:	kIx,	:
Agdgomis	Agdgomis	k1gFnSc1	Agdgomis
Tadzari	Tadzar	k1gFnSc2	Tadzar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
kostel	kostel	k1gInSc1	kostel
uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
starého	starý	k2eAgNnSc2d1	staré
města	město	k1gNnSc2	město
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
