<p>
<s>
Chrám	chrám	k1gInSc1	chrám
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
bazilika	bazilika	k1gFnSc1	bazilika
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
církvích	církev	k1gFnPc6	církev
kostel	kostel	k1gInSc4	kostel
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
–	–	k?	–
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
Ν	Ν	k?	Ν
τ	τ	k?	τ
Α	Α	k?	Α
<g/>
,	,	kIx,	,
Naos	Naos	k1gInSc1	Naos
tis	tis	k1gInSc1	tis
Anastaseos	Anastaseos	k1gInSc4	Anastaseos
<g/>
;	;	kIx,	;
arménsky	arménsky	k6eAd1	arménsky
<g/>
:	:	kIx,	:
Surp	Surp	k1gMnSc1	Surp
Haruthjun	Haruthjun	k1gMnSc1	Haruthjun
<g/>
;	;	kIx,	;
gruzínsky	gruzínsky	k6eAd1	gruzínsky
<g/>
:	:	kIx,	:
Agdgomis	Agdgomis	k1gFnSc1	Agdgomis
Tadzari	Tadzar	k1gFnSc2	Tadzar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
kostel	kostel	k1gInSc1	kostel
uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
starého	starý	k2eAgNnSc2d1	staré
města	město	k1gNnSc2	město
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
většina	většina	k1gFnSc1	většina
křesťanů	křesťan	k1gMnPc2	křesťan
uctívá	uctívat	k5eAaImIp3nS	uctívat
jako	jako	k9	jako
Golgotu	Golgota	k1gFnSc4	Golgota
neboli	neboli	k8xC	neboli
horu	hora	k1gFnSc4	hora
Kalvárii	Kalvárie	k1gFnSc4	Kalvárie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
ukřižován	ukřižován	k2eAgMnSc1d1	ukřižován
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgNnSc7d1	důležité
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
již	již	k6eAd1	již
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
představuje	představovat	k5eAaImIp3nS	představovat
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
hlavní	hlavní	k2eAgInSc1d1	hlavní
cíl	cíl	k1gInSc1	cíl
poutníků	poutník	k1gMnPc2	poutník
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
ukřižování	ukřižování	k1gNnSc2	ukřižování
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
místo	místo	k1gNnSc4	místo
poprav	poprava	k1gFnPc2	poprava
<g/>
)	)	kIx)	)
nacházelo	nacházet	k5eAaImAgNnS	nacházet
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
města	město	k1gNnSc2	město
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
jeruzalémské	jeruzalémský	k2eAgFnPc1d1	Jeruzalémská
hradby	hradba	k1gFnPc1	hradba
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
Herodem	Herod	k1gMnSc7	Herod
Agrippou	Agrippa	k1gMnSc7	Agrippa
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
,	,	kIx,	,
obehnaly	obehnat	k5eAaPmAgInP	obehnat
i	i	k9	i
prostor	prostora	k1gFnPc2	prostora
nynějšího	nynější	k2eAgInSc2d1	nynější
chrámu	chrám	k1gInSc2	chrám
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
135	[number]	k4	135
Římané	Říman	k1gMnPc1	Říman
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
postavili	postavit	k5eAaPmAgMnP	postavit
chrámový	chrámový	k2eAgInSc4d1	chrámový
komplex	komplex	k1gInSc4	komplex
bohyně	bohyně	k1gFnSc2	bohyně
Afrodíté	Afrodítý	k2eAgFnSc2d1	Afrodítý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biskup	biskup	k1gMnSc1	biskup
Eusebios	Eusebios	k1gMnSc1	Eusebios
z	z	k7c2	z
Kaisareie	Kaisareie	k1gFnSc2	Kaisareie
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
327	[number]	k4	327
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
ukřižování	ukřižování	k1gNnSc2	ukřižování
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
v	v	k7c6	v
Aelii	Aelie	k1gFnSc6	Aelie
(	(	kIx(	(
<g/>
Aelia	Aelia	k1gFnSc1	Aelia
Capitolina	Capitolina	k1gFnSc1	Capitolina
byl	být	k5eAaImAgInS	být
římský	římský	k2eAgInSc1d1	římský
název	název	k1gInSc1	název
pro	pro	k7c4	pro
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
po	po	k7c6	po
roce	rok	k1gInSc6	rok
135	[number]	k4	135
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
legendy	legenda	k1gFnSc2	legenda
byl	být	k5eAaImAgInS	být
Ježíšův	Ježíšův	k2eAgInSc1d1	Ježíšův
hrob	hrob	k1gInSc1	hrob
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
kříž	kříž	k1gInSc4	kříž
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
325	[number]	k4	325
sv.	sv.	kA	sv.
Helenou	Helena	k1gFnSc7	Helena
(	(	kIx(	(
<g/>
matkou	matka	k1gFnSc7	matka
Constantina	Constantin	k1gMnSc2	Constantin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Constantinus	Constantinus	k1gMnSc1	Constantinus
I.	I.	kA	I.
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
326	[number]	k4	326
až	až	k9	až
335	[number]	k4	335
postavit	postavit	k5eAaPmF	postavit
kostel	kostel	k1gInSc4	kostel
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
Ježíšova	Ježíšův	k2eAgInSc2d1	Ježíšův
hrobu	hrob	k1gInSc2	hrob
poblíž	poblíž	k7c2	poblíž
Golgoty	Golgota	k1gFnSc2	Golgota
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc1	část
rotundy	rotunda	k1gFnSc2	rotunda
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
Anastasis	Anastasis	k1gFnSc2	Anastasis
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc1	zmrtvýchvstání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
byla	být	k5eAaImAgFnS	být
bazilika	bazilika	k1gFnSc1	bazilika
poničena	poničit	k5eAaPmNgFnS	poničit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
při	při	k7c6	při
nájezdu	nájezd	k1gInSc6	nájezd
Peršanů	Peršan	k1gMnPc2	Peršan
roku	rok	k1gInSc2	rok
614	[number]	k4	614
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1009	[number]	k4	1009
přikázal	přikázat	k5eAaPmAgMnS	přikázat
kalifa	kalif	k1gMnSc2	kalif
Chakim	Chakim	k1gMnSc1	Chakim
kostel	kostel	k1gInSc1	kostel
do	do	k7c2	do
základů	základ	k1gInPc2	základ
zbourat	zbourat	k5eAaPmF	zbourat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1099	[number]	k4	1099
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
dobyli	dobýt	k5eAaPmAgMnP	dobýt
křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
probíhala	probíhat	k5eAaImAgFnS	probíhat
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
baziliky	bazilika	k1gFnSc2	bazilika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
roku	rok	k1gInSc2	rok
1141	[number]	k4	1141
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
baziliku	bazilika	k1gFnSc4	bazilika
spravovali	spravovat	k5eAaImAgMnP	spravovat
augustiniáni	augustinián	k1gMnPc1	augustinián
a	a	k8xC	a
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
ochraně	ochrana	k1gFnSc3	ochrana
sloužil	sloužit	k5eAaImAgInS	sloužit
Řád	řád	k1gInSc1	řád
Božího	boží	k2eAgInSc2d1	boží
Hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1187	[number]	k4	1187
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
dobyt	dobyt	k2eAgInSc1d1	dobyt
sultánem	sultán	k1gMnSc7	sultán
Saladinem	Saladin	k1gMnSc7	Saladin
<g/>
,	,	kIx,	,
získalo	získat	k5eAaPmAgNnS	získat
správu	správa	k1gFnSc4	správa
nad	nad	k7c7	nad
bazilikou	bazilika	k1gFnSc7	bazilika
společně	společně	k6eAd1	společně
několik	několik	k4yIc4	několik
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Složitá	složitý	k2eAgFnSc1d1	složitá
situace	situace	k1gFnSc1	situace
panovala	panovat	k5eAaImAgFnS	panovat
i	i	k9	i
za	za	k7c2	za
tureckého	turecký	k2eAgNnSc2d1	turecké
panství	panství	k1gNnSc2	panství
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1517	[number]	k4	1517
<g/>
.	.	kIx.	.
</s>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
sporům	spor	k1gInPc3	spor
uvnitř	uvnitř	k7c2	uvnitř
baziliky	bazilika	k1gFnSc2	bazilika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
tureckým	turecký	k2eAgMnSc7d1	turecký
sultánem	sultán	k1gMnSc7	sultán
vyhlášena	vyhlášen	k2eAgNnPc4d1	vyhlášeno
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Status	status	k1gInSc1	status
quo	quo	k?	quo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1757	[number]	k4	1757
a	a	k8xC	a
opět	opět	k6eAd1	opět
potvrzená	potvrzená	k1gFnSc1	potvrzená
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
církví	církev	k1gFnPc2	církev
uvnitř	uvnitř	k7c2	uvnitř
baziliky	bazilika	k1gFnSc2	bazilika
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
i	i	k9	i
za	za	k7c2	za
Britského	britský	k2eAgInSc2d1	britský
mandátu	mandát	k1gInSc2	mandát
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
,	,	kIx,	,
jordánské	jordánský	k2eAgFnSc2d1	Jordánská
i	i	k8xC	i
izraelské	izraelský	k2eAgFnSc2d1	izraelská
správy	správa	k1gFnSc2	správa
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
schody	schod	k1gInPc1	schod
vpravo	vpravo	k6eAd1	vpravo
vedou	vést	k5eAaImIp3nP	vést
nahoru	nahoru	k6eAd1	nahoru
do	do	k7c2	do
bohatě	bohatě	k6eAd1	bohatě
vyzdobené	vyzdobený	k2eAgFnSc2d1	vyzdobená
kaple	kaple	k1gFnSc2	kaple
Ukřižování	ukřižování	k1gNnSc2	ukřižování
nad	nad	k7c7	nad
skalou	skala	k1gFnSc7	skala
Kalvarie	Kalvarie	k1gFnSc2	Kalvarie
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
ochozu	ochoz	k1gInSc2	ochoz
středověké	středověký	k2eAgFnPc1d1	středověká
stavby	stavba	k1gFnPc1	stavba
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
různých	různý	k2eAgFnPc2d1	různá
kaplí	kaple	k1gFnPc2	kaple
a	a	k8xC	a
výklenků	výklenek	k1gInPc2	výklenek
<g/>
.	.	kIx.	.
</s>
<s>
Vlevo	vlevo	k6eAd1	vlevo
(	(	kIx(	(
<g/>
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rotunda	rotunda	k1gFnSc1	rotunda
(	(	kIx(	(
<g/>
Anastasis	Anastasis	k1gInSc1	Anastasis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kruhová	kruhový	k2eAgFnSc1d1	kruhová
stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
sloupovým	sloupový	k2eAgInSc7d1	sloupový
ochozem	ochoz	k1gInSc7	ochoz
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
patrech	patro	k1gNnPc6	patro
zaklenutá	zaklenutý	k2eAgFnSc1d1	zaklenutá
kopulí	kopule	k1gFnSc7	kopule
s	s	k7c7	s
lucernou	lucerna	k1gFnSc7	lucerna
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
zachovaná	zachovaný	k2eAgFnSc1d1	zachovaná
část	část	k1gFnSc1	část
Konstantinova	Konstantinův	k2eAgInSc2d1	Konstantinův
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
rotundy	rotunda	k1gFnSc2	rotunda
stojí	stát	k5eAaImIp3nS	stát
malá	malý	k2eAgFnSc1d1	malá
svatyně	svatyně	k1gFnSc1	svatyně
<g/>
,	,	kIx,	,
edikula	edikula	k1gFnSc1	edikula
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Ježíšův	Ježíšův	k2eAgInSc1d1	Ježíšův
hrob	hrob	k1gInSc1	hrob
s	s	k7c7	s
předsíňkou	předsíňka	k1gFnSc7	předsíňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
navazuje	navazovat	k5eAaImIp3nS	navazovat
katholikon	katholikon	k1gNnSc1	katholikon
<g/>
,	,	kIx,	,
středověká	středověký	k2eAgFnSc1d1	středověká
loď	loď	k1gFnSc1	loď
kostela	kostel	k1gInSc2	kostel
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
ortodoxním	ortodoxní	k2eAgInSc7d1	ortodoxní
ikonostasem	ikonostas	k1gInSc7	ikonostas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
kamenná	kamenný	k2eAgFnSc1d1	kamenná
nádoba	nádoba	k1gFnSc1	nádoba
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
omfalos	omfalos	k1gInSc1	omfalos
(	(	kIx(	(
<g/>
pupek	pupek	k1gInSc1	pupek
<g/>
)	)	kIx)	)
a	a	k8xC	a
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
střed	střed	k1gInSc4	střed
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
se	se	k3xPyFc4	se
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
do	do	k7c2	do
arménského	arménský	k2eAgInSc2d1	arménský
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Heleny	Helena	k1gFnSc2	Helena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
Ježíšův	Ježíšův	k2eAgInSc1d1	Ježíšův
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
a	a	k8xC	a
užívání	užívání	k1gNnSc1	užívání
celého	celý	k2eAgInSc2d1	celý
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
společné	společný	k2eAgNnSc4d1	společné
–	–	k?	–
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
<g/>
,	,	kIx,	,
katolické	katolický	k2eAgFnSc2d1	katolická
a	a	k8xC	a
arménské	arménský	k2eAgFnSc2d1	arménská
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
různé	různý	k2eAgFnPc1d1	různá
části	část	k1gFnPc1	část
mají	mít	k5eAaImIp3nP	mít
přesně	přesně	k6eAd1	přesně
určeného	určený	k2eAgMnSc4d1	určený
vlastníka	vlastník	k1gMnSc4	vlastník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
Stautu	Staut	k1gInSc2	Staut
quo	quo	k?	quo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
pouze	pouze	k6eAd1	pouze
kaple	kaple	k1gFnSc1	kaple
Ježíšova	Ježíšův	k2eAgInSc2d1	Ježíšův
hrobu	hrob	k1gInSc2	hrob
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rotundou	rotunda	k1gFnSc7	rotunda
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gInSc2	on
(	(	kIx(	(
<g/>
anastasis	anastasis	k1gFnPc3	anastasis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
společná	společný	k2eAgFnSc1d1	společná
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
společenství	společenství	k1gNnPc1	společenství
mají	mít	k5eAaImIp3nP	mít
vyhrazené	vyhrazený	k2eAgFnPc4d1	vyhrazená
okolní	okolní	k2eAgFnPc4d1	okolní
prostory	prostora	k1gFnPc4	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
loď	loď	k1gFnSc1	loď
kostela	kostel	k1gInSc2	kostel
z	z	k7c2	z
křižácké	křižácký	k2eAgFnSc2d1	křižácká
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nS	patřit
řeckým	řecký	k2eAgNnSc7d1	řecké
pravoslavným	pravoslavný	k2eAgNnSc7d1	pravoslavné
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
oltář	oltář	k1gInSc1	oltář
svaté	svatý	k2eAgFnSc2d1	svatá
Marie	Maria	k1gFnSc2	Maria
Magdaleny	Magdalena	k1gFnSc2	Magdalena
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
kaple	kaple	k1gFnSc1	kaple
Zvěstování	zvěstování	k1gNnSc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
patří	patřit	k5eAaImIp3nS	patřit
katolíkům	katolík	k1gMnPc3	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Kalvárie	Kalvárie	k1gFnSc1	Kalvárie
je	být	k5eAaImIp3nS	být
rozdělená	rozdělená	k1gFnSc1	rozdělená
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
téměř	téměř	k6eAd1	téměř
stejné	stejný	k2eAgInPc4d1	stejný
díly	díl	k1gInPc4	díl
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgInSc4d1	katolický
a	a	k8xC	a
pravoslavný	pravoslavný	k2eAgInSc4d1	pravoslavný
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
krypt	krypta	k1gFnPc2	krypta
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
arménská	arménský	k2eAgFnSc1d1	arménská
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
zasvěcená	zasvěcený	k2eAgNnPc1d1	zasvěcené
Nalezení	nalezení	k1gNnPc1	nalezení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
katolická	katolický	k2eAgFnSc1d1	katolická
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
prostorech	prostor	k1gInPc6	prostor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
jedné	jeden	k4xCgFnSc2	jeden
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
jiná	jiný	k2eAgFnSc1d1	jiná
církev	církev	k1gFnSc1	církev
právo	práv	k2eAgNnSc1d1	právo
sloužit	sloužit	k5eAaImF	sloužit
svou	svůj	k3xOyFgFnSc4	svůj
liturgii	liturgie	k1gFnSc4	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
předpisy	předpis	k1gInPc1	předpis
platí	platit	k5eAaImIp3nP	platit
na	na	k7c4	na
velké	velký	k2eAgInPc4d1	velký
svátky	svátek	k1gInPc4	svátek
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
půst	půst	k1gInSc4	půst
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
a	a	k8xC	a
Vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nehybný	hybný	k2eNgInSc1d1	nehybný
žebřík	žebřík	k1gInSc1	žebřík
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
</s>
</p>
<p>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
čtvrť	čtvrť	k1gFnSc1	čtvrť
(	(	kIx(	(
<g/>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
sakrální	sakrální	k2eAgFnSc1d1	sakrální
architektura	architektura	k1gFnSc1	architektura
</s>
</p>
<p>
<s>
Statu	status	k1gInSc3	status
quo	quo	k?	quo
(	(	kIx(	(
<g/>
Svatá	svatý	k2eAgFnSc1d1	svatá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chrám	chrám	k1gInSc1	chrám
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
přehlídka	přehlídka	k1gFnSc1	přehlídka
Baziliky	bazilika	k1gFnSc2	bazilika
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mapy	mapa	k1gFnPc1	mapa
a	a	k8xC	a
galerie	galerie	k1gFnPc1	galerie
baziliky	bazilika	k1gFnSc2	bazilika
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fotografie	fotografia	k1gFnPc1	fotografia
baziliky	bazilika	k1gFnSc2	bazilika
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Článek	článek	k1gInSc1	článek
a	a	k8xC	a
fotografie	fotografie	k1gFnSc1	fotografie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Podrobný	podrobný	k2eAgInSc1d1	podrobný
popis	popis	k1gInSc1	popis
na	na	k7c4	na
OrthodoxWiki	OrthodoxWike	k1gFnSc4	OrthodoxWike
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
poutního	poutní	k2eAgNnSc2d1	poutní
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
františkánské	františkánský	k2eAgFnSc2d1	Františkánská
kustodie	kustodie	k1gFnSc2	kustodie
</s>
</p>
