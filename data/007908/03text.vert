<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
je	být	k5eAaImIp3nS	být
prastaré	prastarý	k2eAgNnSc4d1	prastaré
slovanské	slovanský	k2eAgNnSc4d1	slovanské
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k9	jako
silný	silný	k2eAgInSc4d1	silný
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
v	v	k7c6	v
plucích	pluk	k1gInPc6	pluk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vojsku	vojsko	k1gNnSc6	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Svante	Svant	k1gMnSc5	Svant
<g/>
,	,	kIx,	,
Svantepolk	Svantepolk	k1gInSc1	Svantepolk
–	–	k?	–
švédsky	švédsky	k6eAd1	švédsky
Svjatopolk	Svjatopolk	k1gInSc1	Svjatopolk
–	–	k?	–
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
,	,	kIx,	,
bělorusky	bělorusky	k6eAd1	bělorusky
Świetopelk	Świetopelk	k1gInSc1	Świetopelk
–	–	k?	–
polsky	polsky	k6eAd1	polsky
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
–	–	k?	–
<g/>
1,8	[number]	k4	1,8
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
I.	I.	kA	I.
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
velkomoravský	velkomoravský	k2eAgMnSc1d1	velkomoravský
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
nitranský	nitranský	k2eAgMnSc1d1	nitranský
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
český	český	k2eAgMnSc1d1	český
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Doseděl	dosedět	k5eAaPmAgMnS	dosedět
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Havelka	Havelka	k1gMnSc1	Havelka
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Innemann	Innemann	k1gMnSc1	Innemann
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
režisér	režisér	k1gMnSc1	režisér
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Káš	Káš	k1gMnSc1	Káš
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
humorista	humorista	k1gMnSc1	humorista
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Karásek	Karásek	k1gMnSc1	Karásek
<g/>
,	,	kIx,	,
farář	farář	k1gMnSc1	farář
<g/>
,	,	kIx,	,
písničkář	písničkář	k1gMnSc1	písničkář
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Skládal	skládat	k5eAaImAgMnS	skládat
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Skopal	Skopal	k1gMnSc1	Skopal
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Svjatopolk	Svjatopolk	k1gMnSc1	Svjatopolk
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
<g/>
,	,	kIx,	,
kijevský	kijevský	k2eAgMnSc1d1	kijevský
kníže	kníže	k1gMnSc1	kníže
Svante	Svant	k1gInSc5	Svant
August	August	k1gMnSc1	August
Arrhenius	Arrhenius	k1gInSc1	Arrhenius
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
chemik	chemik	k1gMnSc1	chemik
Svante	Svant	k1gInSc5	Svant
Bosson	Bosson	k1gMnSc1	Bosson
<g/>
,	,	kIx,	,
strýc	strýc	k1gMnSc1	strýc
vladaře	vladař	k1gMnSc2	vladař
Svanteho	Svante	k1gMnSc2	Svante
(	(	kIx(	(
<g/>
1460	[number]	k4	1460
<g/>
-	-	kIx~	-
<g/>
1512	[number]	k4	1512
<g/>
)	)	kIx)	)
Svante	Svant	k1gMnSc5	Svant
Sture	Stur	k1gMnSc5	Stur
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
Stegeholmu	Stegeholm	k1gInSc2	Stegeholm
(	(	kIx(	(
<g/>
1517	[number]	k4	1517
<g/>
-	-	kIx~	-
<g/>
1567	[number]	k4	1567
<g/>
)	)	kIx)	)
Svante	Svant	k1gInSc5	Svant
Stenbock	Stenbock	k1gMnSc1	Stenbock
(	(	kIx(	(
<g/>
1578	[number]	k4	1578
<g/>
-	-	kIx~	-
<g/>
1632	[number]	k4	1632
<g/>
)	)	kIx)	)
Svante	Svant	k1gMnSc5	Svant
Bielke	Bielkus	k1gMnSc5	Bielkus
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vysoký	vysoký	k2eAgMnSc1d1	vysoký
kancléř	kancléř	k1gMnSc1	kancléř
Švédska	Švédsko	k1gNnSc2	Švédsko
Svante	Svant	k1gInSc5	Svant
Larsson	Larsson	k1gInSc1	Larsson
Sparre	Sparr	k1gInSc5	Sparr
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
Upplandu	Uppland	k1gInSc2	Uppland
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1649-1652	[number]	k4	1649-1652
Svante	Svant	k1gInSc5	Svant
Banér	Banér	k1gMnSc1	Banér
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
-	-	kIx~	-
<g/>
1628	[number]	k4	1628
<g/>
)	)	kIx)	)
Svante	Svant	k1gInSc5	Svant
Svantesson	Svantesson	k1gMnSc1	Svantesson
Banér	Banér	k1gInSc1	Banér
(	(	kIx(	(
<g/>
1624	[number]	k4	1624
<g/>
-	-	kIx~	-
<g/>
1674	[number]	k4	1674
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
Upplandu	Uppland	k1gInSc2	Uppland
1652-1654	[number]	k4	1652-1654
Svantepolk	Svantepolk	k1gMnSc1	Svantepolk
Knutsson	Knutsson	k1gMnSc1	Knutsson
(	(	kIx(	(
<g/>
1225	[number]	k4	1225
a	a	k8xC	a
1235	[number]	k4	1235
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc1	soudce
Svantepolk	Svantepolk	k1gMnSc1	Svantepolk
of	of	k?	of
Viby	Viba	k1gFnSc2	Viba
(	(	kIx(	(
<g/>
1310	[number]	k4	1310
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skandinávský	skandinávský	k2eAgMnSc1d1	skandinávský
velmož	velmož	k1gMnSc1	velmož
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Pulec	pulec	k1gMnSc1	pulec
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kuřátko	kuřátko	k1gNnSc1	kuřátko
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Rozpaky	rozpak	k1gInPc1	rozpak
kuchaře	kuchař	k1gMnSc2	kuchař
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
(	(	kIx(	(
<g/>
Svätopluk	Svätopluk	k1gMnSc1	Svätopluk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
Eugena	Eugena	k1gFnSc1	Eugena
Suchoně	Suchoň	k1gFnSc2	Suchoň
Svätopluk	Svätopluka	k1gFnPc2	Svätopluka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Udatný	udatný	k2eAgMnSc1d1	udatný
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
Alexandra	Alexandr	k1gMnSc2	Alexandr
Moyzese	Moyzese	k1gFnSc2	Moyzese
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
v	v	k7c6	v
sekci	sekce	k1gFnSc6	sekce
jména	jméno	k1gNnSc2	jméno
na	na	k7c4	na
www.libri.cz	www.libri.cz	k1gInSc4	www.libri.cz
</s>
