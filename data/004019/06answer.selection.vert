<s>
Živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
Metazoa	Metazoa	k1gMnSc1	Metazoa
<g/>
,	,	kIx,	,
Animalia	Animalia	k1gFnSc1	Animalia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
říše	říše	k1gFnSc1	říše
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
heterotrofních	heterotrofní	k2eAgInPc2d1	heterotrofní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
na	na	k7c6	na
buněčné	buněčný	k2eAgFnSc6d1	buněčná
úrovni	úroveň	k1gFnSc6	úroveň
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
