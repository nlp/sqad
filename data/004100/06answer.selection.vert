<s>
Leopold	Leopold	k1gMnSc1	Leopold
Sviták	Sviták	k1gMnSc1	Sviták
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1856	[number]	k4	1856
Frenštát	Frenštát	k1gInSc1	Frenštát
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
Hukvaldy	Hukvaldy	k1gInPc4	Hukvaldy
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
automobilový	automobilový	k2eAgMnSc1d1	automobilový
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1897	[number]	k4	1897
až	až	k9	až
1898	[number]	k4	1898
sestavil	sestavit	k5eAaPmAgInS	sestavit
první	první	k4xOgInSc4	první
rakousko-uherský	rakouskoherský	k2eAgInSc4d1	rakousko-uherský
osobní	osobní	k2eAgInSc4d1	osobní
automobil	automobil	k1gInSc4	automobil
NW	NW	kA	NW
Präsident	Präsident	k1gMnSc1	Präsident
<g/>
.	.	kIx.	.
</s>
