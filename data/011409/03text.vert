<p>
<s>
Pánský	pánský	k2eAgInSc1d1	pánský
oblek	oblek	k1gInSc1	oblek
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
saka	sako	k1gNnSc2	sako
a	a	k8xC	a
kalhot	kalhoty	k1gFnPc2	kalhoty
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Třídílný	třídílný	k2eAgInSc4d1	třídílný
oblek	oblek	k1gInSc4	oblek
navíc	navíc	k6eAd1	navíc
z	z	k7c2	z
vesty	vesta	k1gFnSc2	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Společenský	společenský	k2eAgInSc1d1	společenský
oblek	oblek	k1gInSc1	oblek
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
jemné	jemný	k2eAgFnSc2d1	jemná
česané	česaný	k2eAgFnSc2d1	česaná
vlny	vlna	k1gFnSc2	vlna
<g/>
;	;	kIx,	;
také	také	k9	také
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
,	,	kIx,	,
mohér	mohér	k1gInSc1	mohér
či	či	k8xC	či
kašmír	kašmír	k1gInSc1	kašmír
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
vlněné	vlněný	k2eAgFnSc2d1	vlněná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
vlněný	vlněný	k2eAgInSc1d1	vlněný
materiál	materiál	k1gInSc1	materiál
bývá	bývat	k5eAaImIp3nS	bývat
označen	označit	k5eAaPmNgInS	označit
slovem	slovo	k1gNnSc7	slovo
Super	super	k1gInSc2	super
a	a	k8xC	a
číslem	číslo	k1gNnSc7	číslo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Super	super	k6eAd1	super
120	[number]	k4	120
<g/>
'	'	kIx"	'
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
vyšší	vysoký	k2eAgMnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
jemnější	jemný	k2eAgInPc1d2	jemnější
a	a	k8xC	a
kvalitnější	kvalitní	k2eAgFnSc1d2	kvalitnější
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Levné	levný	k2eAgInPc1d1	levný
obleky	oblek	k1gInPc1	oblek
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
umělých	umělý	k2eAgFnPc2d1	umělá
vláken	vlákna	k1gFnPc2	vlákna
nebo	nebo	k8xC	nebo
směsi	směs	k1gFnSc2	směs
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
umělého	umělý	k2eAgNnSc2d1	umělé
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
polyesteru	polyester	k1gInSc2	polyester
<g/>
.	.	kIx.	.
</s>
<s>
Vycházkové	vycházkový	k2eAgInPc1d1	vycházkový
obleky	oblek	k1gInPc1	oblek
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
chladných	chladný	k2eAgInPc6d1	chladný
měsících	měsíc	k1gInPc6	měsíc
z	z	k7c2	z
hrubší	hrubý	k2eAgFnSc2d2	hrubší
vlny	vlna	k1gFnSc2	vlna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
flanel	flanel	k1gInSc1	flanel
nebo	nebo	k8xC	nebo
tvíd	tvíd	k1gInSc1	tvíd
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
z	z	k7c2	z
bavlněného	bavlněný	k2eAgInSc2d1	bavlněný
manšestru	manšestr	k1gInSc2	manšestr
<g/>
,	,	kIx,	,
v	v	k7c6	v
teplých	teplý	k2eAgInPc6d1	teplý
měsících	měsíc	k1gInPc6	měsíc
pak	pak	k6eAd1	pak
z	z	k7c2	z
lehčí	lehký	k2eAgFnSc2d2	lehčí
bavlny	bavlna	k1gFnSc2	bavlna
nebo	nebo	k8xC	nebo
lnu	len	k1gInSc2	len
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
obleku	oblek	k1gInSc3	oblek
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
košile	košile	k1gFnSc2	košile
s	s	k7c7	s
vyztuženým	vyztužený	k2eAgInSc7d1	vyztužený
ohrnutým	ohrnutý	k2eAgInSc7d1	ohrnutý
límcem	límec	k1gInSc7	límec
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
rukávem	rukáv	k1gInSc7	rukáv
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
manžeta	manžeta	k1gFnSc1	manžeta
rukávu	rukáv	k1gInSc2	rukáv
košile	košile	k1gFnSc2	košile
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
o	o	k7c4	o
kousek	kousek	k1gInSc4	kousek
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1-2	[number]	k4	1-2
cm	cm	kA	cm
<g/>
)	)	kIx)	)
přesahovat	přesahovat	k5eAaImF	přesahovat
rukáv	rukáv	k1gInSc4	rukáv
saka	sako	k1gNnSc2	sako
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
<g/>
-li	i	k?	-li
muž	muž	k1gMnSc1	muž
košile	košile	k1gFnSc2	košile
s	s	k7c7	s
manžetovými	manžetový	k2eAgInPc7d1	manžetový
knoflíčky	knoflíček	k1gInPc7	knoflíček
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
délka	délka	k1gFnSc1	délka
rukávu	rukáv	k1gInSc2	rukáv
saka	sako	k1gNnSc2	sako
umožnit	umožnit	k5eAaPmF	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
knoflíčky	knoflíček	k1gInPc1	knoflíček
byly	být	k5eAaImAgInP	být
viditelné	viditelný	k2eAgInPc1d1	viditelný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
obuv	obuv	k1gFnSc4	obuv
se	se	k3xPyFc4	se
k	k	k7c3	k
obleku	oblek	k1gInSc3	oblek
nosí	nosit	k5eAaImIp3nS	nosit
kožené	kožený	k2eAgFnSc2d1	kožená
šněrovací	šněrovací	k2eAgFnSc2d1	šněrovací
boty	bota	k1gFnSc2	bota
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
oblek	oblek	k1gInSc4	oblek
vesta	vesta	k1gFnSc1	vesta
<g/>
,	,	kIx,	,
kravata	kravata	k1gFnSc1	kravata
nebo	nebo	k8xC	nebo
motýlek	motýlek	k1gInSc1	motýlek
<g/>
,	,	kIx,	,
kabát	kabát	k1gInSc1	kabát
a	a	k8xC	a
klobouk	klobouk	k1gInSc1	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Pověstný	pověstný	k2eAgInSc1d1	pověstný
je	být	k5eAaImIp3nS	být
také	také	k9	také
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
který	který	k3yRgInSc1	který
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
koncert	koncert	k1gInSc4	koncert
či	či	k8xC	či
jiné	jiný	k2eAgNnSc4d1	jiné
setkaní	setkaný	k2eAgMnPc1d1	setkaný
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
si	se	k3xPyFc3	se
bral	brát	k5eAaImAgMnS	brát
jiné	jiný	k2eAgNnSc4d1	jiné
sako	sako	k1gNnSc4	sako
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
toto	tento	k3xDgNnSc4	tento
pravidlo	pravidlo	k1gNnSc4	pravidlo
neporušil	porušit	k5eNaPmAgMnS	porušit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
obleků	oblek	k1gInPc2	oblek
==	==	k?	==
</s>
</p>
<p>
<s>
Společenský	společenský	k2eAgInSc1d1	společenský
oblek	oblek	k1gInSc1	oblek
<g/>
,	,	kIx,	,
také	také	k9	také
nazýván	nazývat	k5eAaImNgInS	nazývat
formální	formální	k2eAgInSc1d1	formální
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgFnPc4d1	různá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
šedou	šedý	k2eAgFnSc4d1	šedá
a	a	k8xC	a
tmavě	tmavě	k6eAd1	tmavě
modrou	modrý	k2eAgFnSc7d1	modrá
pro	pro	k7c4	pro
nošení	nošení	k1gNnSc4	nošení
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
hnědou	hnědý	k2eAgFnSc4d1	hnědá
nebo	nebo	k8xC	nebo
olivovou	olivový	k2eAgFnSc4d1	olivová
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc1d1	populární
i	i	k8xC	i
černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
vyhrazená	vyhrazený	k2eAgFnSc1d1	vyhrazená
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
smokingy	smoking	k1gInPc4	smoking
a	a	k8xC	a
fraky	frak	k1gInPc4	frak
<g/>
.	.	kIx.	.
</s>
<s>
Tmavý	tmavý	k2eAgInSc1d1	tmavý
oblek	oblek	k1gInSc1	oblek
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nS	hodit
na	na	k7c4	na
večerní	večerní	k2eAgNnSc4d1	večerní
nošení	nošení	k1gNnSc4	nošení
a	a	k8xC	a
ve	v	k7c6	v
dne	den	k1gInSc2	den
pro	pro	k7c4	pro
formálnější	formální	k2eAgFnPc4d2	formálnější
nebo	nebo	k8xC	nebo
slavnostnější	slavnostní	k2eAgFnPc4d2	slavnostnější
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
večeře	večeře	k1gFnPc4	večeře
<g/>
,	,	kIx,	,
promoce	promoce	k1gFnPc4	promoce
nebo	nebo	k8xC	nebo
plesy	ples	k1gInPc4	ples
<g/>
.	.	kIx.	.
</s>
<s>
Světlý	světlý	k2eAgInSc1d1	světlý
oblek	oblek	k1gInSc1	oblek
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
nošení	nošení	k1gNnSc4	nošení
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
nebo	nebo	k8xC	nebo
na	na	k7c4	na
obchodní	obchodní	k2eAgFnPc4d1	obchodní
schůzky	schůzka	k1gFnPc4	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
Společenský	společenský	k2eAgInSc1d1	společenský
oblek	oblek	k1gInSc1	oblek
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
nosí	nosit	k5eAaImIp3nP	nosit
s	s	k7c7	s
kravatou	kravata	k1gFnSc7	kravata
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
s	s	k7c7	s
motýlkem	motýlek	k1gInSc7	motýlek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvýšit	zvýšit	k5eAaPmF	zvýšit
slavnostnost	slavnostnost	k1gFnSc4	slavnostnost
obleku	oblek	k1gInSc2	oblek
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pomocí	pomocí	k7c2	pomocí
tradičních	tradiční	k2eAgInPc2d1	tradiční
doplňků	doplněk	k1gInPc2	doplněk
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vesta	vesta	k1gFnSc1	vesta
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
materiálu	materiál	k1gInSc2	materiál
jako	jako	k8xC	jako
sako	sako	k1gNnSc4	sako
a	a	k8xC	a
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
z	z	k7c2	z
odlišného	odlišný	k2eAgInSc2d1	odlišný
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kapesníček	kapesníček	k1gInSc4	kapesníček
do	do	k7c2	do
kapsičky	kapsička	k1gFnSc2	kapsička
u	u	k7c2	u
saka	sako	k1gNnSc2	sako
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
společenského	společenský	k2eAgInSc2d1	společenský
obleku	oblek	k1gInSc2	oblek
v	v	k7c6	v
zemitých	zemitý	k2eAgInPc6d1	zemitý
tónech	tón	k1gInPc6	tón
volíme	volit	k5eAaImIp1nP	volit
hnědé	hnědý	k2eAgFnPc4d1	hnědá
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
šedému	šedý	k2eAgMnSc3d1	šedý
a	a	k8xC	a
modrému	modrý	k2eAgInSc3d1	modrý
obleku	oblek	k1gInSc3	oblek
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nosit	nosit	k5eAaImF	nosit
jak	jak	k6eAd1	jak
černé	černý	k2eAgFnPc4d1	černá
(	(	kIx(	(
<g/>
anglický	anglický	k2eAgInSc4d1	anglický
styl	styl	k1gInSc4	styl
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
hnědé	hnědý	k2eAgFnPc1d1	hnědá
boty	bota	k1gFnPc1	bota
(	(	kIx(	(
<g/>
italský	italský	k2eAgInSc1d1	italský
styl	styl	k1gInSc1	styl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boty	bota	k1gFnPc1	bota
by	by	kYmCp3nP	by
ovšem	ovšem	k9	ovšem
neměly	mít	k5eNaImAgFnP	mít
být	být	k5eAaImF	být
světlejší	světlý	k2eAgFnPc1d2	světlejší
než	než	k8xS	než
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
ponožek	ponožka	k1gFnPc2	ponožka
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
barvě	barva	k1gFnSc3	barva
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
večerní	večerní	k2eAgFnSc6d1	večerní
příležitosti	příležitost	k1gFnSc6	příležitost
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nS	hodit
výhradně	výhradně	k6eAd1	výhradně
tmavý	tmavý	k2eAgInSc4d1	tmavý
oblek	oblek	k1gInSc4	oblek
a	a	k8xC	a
černé	černý	k2eAgFnPc4d1	černá
polobotky	polobotka	k1gFnPc4	polobotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vycházkový	vycházkový	k2eAgInSc1d1	vycházkový
oblek	oblek	k1gInSc1	oblek
<g/>
,	,	kIx,	,
také	také	k9	také
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
neformální	formální	k2eNgFnSc4d1	neformální
<g/>
,	,	kIx,	,
odpolední	odpolední	k2eAgFnSc4d1	odpolední
či	či	k8xC	či
denní	denní	k2eAgFnSc4d1	denní
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
s	s	k7c7	s
kravatou	kravata	k1gFnSc7	kravata
či	či	k8xC	či
motýlkem	motýlek	k1gInSc7	motýlek
nebo	nebo	k8xC	nebo
s	s	k7c7	s
rozepnutou	rozepnutý	k2eAgFnSc7d1	rozepnutá
košilí	košile	k1gFnSc7	košile
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uvázán	uvázán	k2eAgInSc4d1	uvázán
šátek	šátek	k1gInSc4	šátek
či	či	k8xC	či
tzv.	tzv.	kA	tzv.
kravatová	kravatový	k2eAgFnSc1d1	kravatová
šála	šála	k1gFnSc1	šála
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nošen	nosit	k5eAaImNgInS	nosit
na	na	k7c4	na
neformální	formální	k2eNgFnPc4d1	neformální
obchodní	obchodní	k2eAgFnPc4d1	obchodní
schůzky	schůzka	k1gFnPc4	schůzka
<g/>
,	,	kIx,	,
vycházky	vycházka	k1gFnPc4	vycházka
<g/>
,	,	kIx,	,
večírky	večírek	k1gInPc4	večírek
zahradní	zahradní	k2eAgFnSc4d1	zahradní
party	party	k1gFnSc4	party
apod.	apod.	kA	apod.
Alternativou	alternativa	k1gFnSc7	alternativa
k	k	k7c3	k
vycházkovému	vycházkový	k2eAgInSc3d1	vycházkový
obleku	oblek	k1gInSc3	oblek
je	být	k5eAaImIp3nS	být
blejzr	blejzr	k1gInSc1	blejzr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
sako	sako	k1gNnSc4	sako
v	v	k7c6	v
námořnické	námořnický	k2eAgFnSc6d1	námořnická
modré	modrý	k2eAgFnSc6d1	modrá
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
kovovými	kovový	k2eAgInPc7d1	kovový
knoflíky	knoflík	k1gInPc7	knoflík
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
nošené	nošený	k2eAgInPc1d1	nošený
s	s	k7c7	s
šedými	šedý	k2eAgFnPc7d1	šedá
kalhotami	kalhoty	k1gFnPc7	kalhoty
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
zapínání	zapínání	k1gNnSc2	zapínání
se	se	k3xPyFc4	se
obleky	oblek	k1gInPc1	oblek
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
jednořadové	jednořadový	k2eAgFnSc6d1	jednořadová
a	a	k8xC	a
dvouřadové	dvouřadový	k2eAgFnSc6d1	dvouřadová
<g/>
.	.	kIx.	.
</s>
<s>
Dvouřadová	dvouřadový	k2eAgNnPc1d1	dvouřadové
saka	sako	k1gNnPc1	sako
se	se	k3xPyFc4	se
během	během	k7c2	během
nošení	nošení	k1gNnSc2	nošení
nerozepínají	rozepínat	k5eNaImIp3nP	rozepínat
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
častá	častý	k2eAgNnPc4d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Jednořadová	jednořadový	k2eAgNnPc1d1	jednořadové
saka	sako	k1gNnPc1	sako
se	se	k3xPyFc4	se
rozepínají	rozepínat	k5eAaImIp3nP	rozepínat
v	v	k7c6	v
sedě	sedě	k6eAd1	sedě
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
uvolněné	uvolněný	k2eAgFnSc6d1	uvolněná
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Saka	sako	k1gNnSc2	sako
jednořadových	jednořadový	k2eAgInPc2d1	jednořadový
obleků	oblek	k1gInPc2	oblek
mají	mít	k5eAaImIp3nP	mít
tradičně	tradičně	k6eAd1	tradičně
tři	tři	k4xCgInPc4	tři
knoflíky	knoflík	k1gInPc4	knoflík
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
moderní	moderní	k2eAgInPc1d1	moderní
dva	dva	k4xCgInPc1	dva
knoflíky	knoflík	k1gInPc1	knoflík
(	(	kIx(	(
<g/>
střih	střih	k1gInSc1	střih
přejatý	přejatý	k2eAgInSc1d1	přejatý
od	od	k7c2	od
sportovních	sportovní	k2eAgNnPc2d1	sportovní
sak	sako	k1gNnPc2	sako
<g/>
)	)	kIx)	)
a	a	k8xC	a
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
i	i	k9	i
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
čtyřmi	čtyři	k4xCgInPc7	čtyři
knoflíky	knoflík	k1gInPc7	knoflík
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgInSc1d1	spodní
knoflík	knoflík	k1gInSc1	knoflík
saka	sako	k1gNnSc2	sako
se	se	k3xPyFc4	se
nezapíná	zapínat	k5eNaImIp3nS	zapínat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
saka	sako	k1gNnSc2	sako
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
knoflíky	knoflík	k1gMnPc7	knoflík
se	se	k3xPyFc4	se
zapínají	zapínat	k5eAaImIp3nP	zapínat
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
knoflíky	knoflík	k1gInPc4	knoflík
od	od	k7c2	od
shora	shora	k6eAd1	shora
u	u	k7c2	u
klasických	klasický	k2eAgFnPc2d1	klasická
sak	sako	k1gNnPc2	sako
<g/>
,	,	kIx,	,
u	u	k7c2	u
sak	sako	k1gNnPc2	sako
se	s	k7c7	s
zvlněnými	zvlněný	k2eAgFnPc7d1	zvlněná
klopami	klopa	k1gFnPc7	klopa
(	(	kIx(	(
<g/>
tzv	tzv	kA	tzv
3	[number]	k4	3
<g/>
-roll-	oll-	k?	-roll-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Počet	počet	k1gInSc1	počet
knoflíků	knoflík	k1gMnPc2	knoflík
na	na	k7c6	na
rukávu	rukáv	k1gInSc6	rukáv
saka	sako	k1gNnSc2	sako
obleku	oblek	k1gInSc2	oblek
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
čtyři	čtyři	k4xCgFnPc1	čtyři
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
tři	tři	k4xCgMnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
luxusnějších	luxusní	k2eAgFnPc2d2	luxusnější
nebo	nebo	k8xC	nebo
ručně	ručně	k6eAd1	ručně
šitých	šitý	k2eAgInPc2d1	šitý
obleků	oblek	k1gInPc2	oblek
bývají	bývat	k5eAaImIp3nP	bývat
i	i	k9	i
tyto	tento	k3xDgInPc1	tento
knoflíky	knoflík	k1gInPc1	knoflík
rozepínací	rozepínací	k2eAgInPc1d1	rozepínací
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formální	formální	k2eAgNnSc1d1	formální
pánské	pánský	k2eAgNnSc1d1	pánské
oblečení	oblečení	k1gNnSc1	oblečení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Smoking	smoking	k1gInSc4	smoking
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
smoking	smoking	k1gInSc1	smoking
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
Tuxedo	Tuxedo	k1gNnSc1	Tuxedo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
Dinner	Dinner	k1gMnSc1	Dinner
Jacket	Jacket	k1gMnSc1	Jacket
<g/>
.	.	kIx.	.
</s>
<s>
Smoking	smoking	k1gInSc1	smoking
je	být	k5eAaImIp3nS	být
formální	formální	k2eAgInSc4d1	formální
večerní	večerní	k2eAgInSc4d1	večerní
oblek	oblek	k1gInSc4	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
černý	černý	k2eAgMnSc1d1	černý
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
půlnoční	půlnoční	k2eAgFnSc2d1	půlnoční
modři	modř	k1gFnSc2	modř
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
lesklé	lesklý	k2eAgNnSc1d1	lesklé
(	(	kIx(	(
<g/>
hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
<g/>
)	)	kIx)	)
klopy	klopa	k1gFnSc2	klopa
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
buď	buď	k8xC	buď
schodovité	schodovitý	k2eAgFnPc4d1	schodovitá
<g/>
,	,	kIx,	,
špičaté	špičatý	k2eAgFnPc4d1	špičatá
nebo	nebo	k8xC	nebo
šálové	šálový	k2eAgFnPc4d1	Šálová
(	(	kIx(	(
<g/>
západní	západní	k2eAgInSc4d1	západní
střih	střih	k1gInSc4	střih
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
klop	klopa	k1gFnPc2	klopa
(	(	kIx(	(
<g/>
východní	východní	k2eAgInSc1d1	východní
střih	střih	k1gInSc1	střih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kalhoty	kalhoty	k1gFnPc1	kalhoty
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
jeden	jeden	k4xCgInSc4	jeden
lesklý	lesklý	k2eAgInSc4d1	lesklý
lampas	lampas	k1gInSc4	lampas
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
bílá	bílý	k2eAgFnSc1d1	bílá
košile	košile	k1gFnSc1	košile
s	s	k7c7	s
dvojitými	dvojitý	k2eAgFnPc7d1	dvojitá
manžetami	manžeta	k1gFnPc7	manžeta
na	na	k7c4	na
manžetové	manžetový	k2eAgInPc4d1	manžetový
knoflíčky	knoflíček	k1gInPc4	knoflíček
a	a	k8xC	a
s	s	k7c7	s
běžným	běžný	k2eAgMnSc7d1	běžný
nebo	nebo	k8xC	nebo
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
frakovým	frakový	k2eAgInSc7d1	frakový
límečkem	límeček	k1gInSc7	límeček
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
smokingu	smoking	k1gInSc3	smoking
patří	patřit	k5eAaImIp3nS	patřit
výhradně	výhradně	k6eAd1	výhradně
černý	černý	k2eAgInSc1d1	černý
motýlek	motýlek	k1gInSc1	motýlek
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
angl.	angl.	k?	angl.
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
black	black	k1gInSc1	black
tie	tie	k?	tie
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
společenské	společenský	k2eAgFnPc4d1	společenská
události	událost	k1gFnPc4	událost
vyžadující	vyžadující	k2eAgFnSc1d1	vyžadující
smoking	smoking	k1gInSc1	smoking
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
černá	černý	k2eAgFnSc1d1	černá
hluboko	hluboko	k6eAd1	hluboko
vykrojená	vykrojený	k2eAgFnSc1d1	vykrojená
smokingová	smokingový	k2eAgFnSc1d1	smokingová
vesta	vesta	k1gFnSc1	vesta
nebo	nebo	k8xC	nebo
hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
smokingový	smokingový	k2eAgInSc1d1	smokingový
pás	pás	k1gInSc1	pás
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
jinou	jiný	k2eAgFnSc4d1	jiná
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vínový	vínový	k2eAgMnSc1d1	vínový
<g/>
,	,	kIx,	,
lahvově	lahvově	k6eAd1	lahvově
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smoking	smoking	k1gInSc1	smoking
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
s	s	k7c7	s
černými	černý	k2eAgFnPc7d1	černá
lakovanými	lakovaný	k2eAgFnPc7d1	lakovaná
nebo	nebo	k8xC	nebo
dobře	dobře	k6eAd1	dobře
vyleštěnými	vyleštěný	k2eAgFnPc7d1	vyleštěná
polobotkami	polobotka	k1gFnPc7	polobotka
s	s	k7c7	s
uzavřeným	uzavřený	k2eAgNnSc7d1	uzavřené
šněrováním	šněrování	k1gNnSc7	šněrování
a	a	k8xC	a
černými	černý	k2eAgFnPc7d1	černá
hedvábnými	hedvábný	k2eAgFnPc7d1	hedvábná
ponožkami	ponožka	k1gFnPc7	ponožka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
na	na	k7c4	na
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
večerní	večerní	k2eAgFnPc4d1	večerní
společenské	společenský	k2eAgFnPc4d1	společenská
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
večeře	večeře	k1gFnPc4	večeře
<g/>
,	,	kIx,	,
galavečery	galavečer	k1gInPc4	galavečer
<g/>
,	,	kIx,	,
předávání	předávání	k1gNnSc1	předávání
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
plesy	ples	k1gInPc4	ples
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c4	o
stupeň	stupeň	k1gInSc4	stupeň
slavnostnější	slavnostní	k2eAgInSc4d2	slavnostnější
a	a	k8xC	a
formálnější	formální	k2eAgInSc4d2	formálnější
než	než	k8xS	než
společenský	společenský	k2eAgInSc4d1	společenský
oblek	oblek	k1gInSc4	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
oblékat	oblékat	k5eAaImF	oblékat
před	před	k7c7	před
šestou	šestý	k4xOgFnSc7	šestý
hodinou	hodina	k1gFnSc7	hodina
večerní	večerní	k2eAgFnSc7d1	večerní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
letní	letní	k2eAgNnPc4d1	letní
období	období	k1gNnPc4	období
a	a	k8xC	a
teplé	teplý	k2eAgInPc4d1	teplý
kraje	kraj	k1gInPc4	kraj
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
záměna	záměna	k1gFnSc1	záměna
klasického	klasický	k2eAgNnSc2d1	klasické
černého	černé	k1gNnSc2	černé
smokingového	smokingový	k2eAgNnSc2d1	smokingový
saka	sako	k1gNnSc2	sako
za	za	k7c4	za
bílé	bílý	k2eAgNnSc4d1	bílé
nebo	nebo	k8xC	nebo
krémové	krémový	k2eAgNnSc4d1	krémové
smokingové	smokingový	k2eAgNnSc4d1	smokingový
sako	sako	k1gNnSc4	sako
<g/>
.	.	kIx.	.
</s>
<s>
Vhodným	vhodný	k2eAgInSc7d1	vhodný
kloboukem	klobouk	k1gInSc7	klobouk
ke	k	k7c3	k
smokingu	smoking	k1gInSc3	smoking
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc4d1	černý
homburg	homburg	k1gInSc4	homburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
svátečnější	sváteční	k2eAgFnPc1d2	svátečnější
či	či	k8xC	či
oficielnější	oficielní	k2eAgFnPc1d2	oficielní
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
frak	frak	k1gInSc1	frak
nebo	nebo	k8xC	nebo
žaket	žaket	k1gInSc1	žaket
<g/>
.	.	kIx.	.
</s>
<s>
Smoking	smoking	k1gInSc1	smoking
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgMnSc1d1	vhodný
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
při	při	k7c6	při
premierách	premiera	k1gFnPc6	premiera
<g/>
,	,	kIx,	,
derniérách	derniéra	k1gFnPc6	derniéra
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
návštěvu	návštěva	k1gFnSc4	návštěva
divadla	divadlo	k1gNnSc2	divadlo
muži	muž	k1gMnPc1	muž
volí	volit	k5eAaImIp3nS	volit
oblek	oblek	k1gInSc1	oblek
tmavších	tmavý	k2eAgFnPc2d2	tmavší
barev	barva	k1gFnPc2	barva
s	s	k7c7	s
motýlkem	motýlek	k1gInSc7	motýlek
nebo	nebo	k8xC	nebo
vázankou	vázanka	k1gFnSc7	vázanka
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
použitím	použití	k1gNnSc7	použití
smokingu	smoking	k1gInSc2	smoking
či	či	k8xC	či
fraku	frak	k1gInSc2	frak
splývají	splývat	k5eAaImIp3nP	splývat
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
přesně	přesně	k6eAd1	přesně
stanovené	stanovený	k2eAgInPc1d1	stanovený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Detaily	detail	k1gInPc1	detail
a	a	k8xC	a
doplňky	doplněk	k1gInPc1	doplněk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
klasický	klasický	k2eAgInSc1d1	klasický
smoking	smoking	k1gInSc1	smoking
není	být	k5eNaImIp3nS	být
dle	dle	k7c2	dle
pravidel	pravidlo	k1gNnPc2	pravidlo
černý	černý	k2eAgMnSc1d1	černý
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
"	"	kIx"	"
<g/>
půlnoční	půlnoční	k2eAgFnSc2d1	půlnoční
modři	modř	k1gFnSc2	modř
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
kalhoty	kalhoty	k1gFnPc1	kalhoty
bez	bez	k7c2	bez
záložky	záložka	k1gFnSc2	záložka
na	na	k7c6	na
krajích	kraj	k1gInPc6	kraj
se	s	k7c7	s
saténovým	saténový	k2eAgInSc7d1	saténový
proužkem	proužek	k1gInSc7	proužek
</s>
</p>
<p>
<s>
k	k	k7c3	k
"	"	kIx"	"
<g/>
black	black	k1gMnSc1	black
tie	tie	k?	tie
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
košile	košile	k1gFnSc2	košile
s	s	k7c7	s
límcem	límec	k1gInSc7	límec
kent	kenta	k1gFnPc2	kenta
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
s	s	k7c7	s
frakovým	frakový	k2eAgInSc7d1	frakový
límcem	límec	k1gInSc7	límec
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
optimální	optimální	k2eAgFnSc1d1	optimální
je	být	k5eAaImIp3nS	být
košile	košile	k1gFnSc1	košile
se	s	k7c7	s
skrytou	skrytý	k2eAgFnSc7d1	skrytá
légou	léga	k1gFnSc7	léga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
stříbrné	stříbrný	k2eAgInPc1d1	stříbrný
manžetové	manžetový	k2eAgInPc1d1	manžetový
knoflíky	knoflík	k1gInPc1	knoflík
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
černé	černý	k2eAgFnPc1d1	černá
lakované	lakovaný	k2eAgFnPc1d1	lakovaná
kožené	kožený	k2eAgFnPc1d1	kožená
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
vzor	vzor	k1gInSc1	vzor
oxford	oxford	k1gInSc1	oxford
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Frak	frak	k1gInSc4	frak
===	===	k?	===
</s>
</p>
<p>
<s>
Frak	frak	k1gInSc1	frak
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
formální	formální	k2eAgInSc4d1	formální
večerní	večerní	k2eAgInSc4d1	večerní
oblek	oblek	k1gInSc4	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Frak	frak	k1gInSc1	frak
má	mít	k5eAaImIp3nS	mít
kabát	kabát	k1gInSc4	kabát
se	s	k7c7	s
šosy	šos	k1gInPc7	šos
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nezapíná	zapínat	k5eNaImIp3nS	zapínat
<g/>
,	,	kIx,	,
a	a	k8xC	a
lesklé	lesklý	k2eAgFnPc1d1	lesklá
špičaté	špičatý	k2eAgFnPc1d1	špičatá
klopy	klopa	k1gFnPc1	klopa
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
něj	on	k3xPp3gInSc4	on
bílá	bílý	k2eAgFnSc1d1	bílá
košile	košile	k1gFnSc1	košile
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
ztuženými	ztužený	k2eAgFnPc7d1	ztužená
manžetami	manžeta	k1gFnPc7	manžeta
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nosí	nosit	k5eAaImIp3nS	nosit
převážně	převážně	k6eAd1	převážně
smokingové	smokingový	k2eAgFnPc4d1	smokingová
košile	košile	k1gFnPc4	košile
s	s	k7c7	s
dvojitými	dvojitý	k2eAgFnPc7d1	dvojitá
manžetami	manžeta	k1gFnPc7	manžeta
<g/>
)	)	kIx)	)
na	na	k7c4	na
manžetové	manžetový	k2eAgInPc4d1	manžetový
knoflíčky	knoflíček	k1gInPc4	knoflíček
s	s	k7c7	s
předním	přední	k2eAgInSc7d1	přední
štítem	štít	k1gInSc7	štít
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
z	z	k7c2	z
piké	piké	k1gNnSc2	piké
<g/>
.	.	kIx.	.
</s>
<s>
Košile	košile	k1gFnSc1	košile
je	být	k5eAaImIp3nS	být
zapínána	zapínán	k2eAgFnSc1d1	zapínána
na	na	k7c4	na
cvoky	cvok	k1gMnPc4	cvok
(	(	kIx(	(
<g/>
studs	studs	k6eAd1	studs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
barvách	barva	k1gFnPc6	barva
a	a	k8xC	a
provedeních	provedení	k1gNnPc6	provedení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
černé	černý	k2eAgFnPc1d1	černá
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
srdce	srdce	k1gNnSc2	srdce
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
košile	košile	k1gFnSc1	košile
má	mít	k5eAaImIp3nS	mít
frakový	frakový	k2eAgInSc4d1	frakový
límec	límec	k1gInSc4	límec
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
stojáček	stojáček	k1gInSc1	stojáček
s	s	k7c7	s
ohnutými	ohnutý	k2eAgInPc7d1	ohnutý
cípy	cíp	k1gInPc7	cíp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tento	tento	k3xDgInSc1	tento
límec	límec	k1gInSc1	límec
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odnímací	odnímací	k2eAgMnSc1d1	odnímací
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
či	či	k8xC	či
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
neoddělitelnou	oddělitelný	k2eNgFnSc7d1	neoddělitelná
součástí	součást	k1gFnSc7	součást
košile	košile	k1gFnSc2	košile
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
bílá	bílý	k2eAgFnSc1d1	bílá
vesta	vesta	k1gFnSc1	vesta
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
piké	piké	k1gNnSc2	piké
<g/>
,	,	kIx,	,
hluboce	hluboko	k6eAd1	hluboko
vykrojená	vykrojený	k2eAgFnSc1d1	vykrojená
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
V	V	kA	V
či	či	k8xC	či
U	U	kA	U
<g/>
,	,	kIx,	,
jednořadá	jednořadý	k2eAgFnSc1d1	jednořadá
nebo	nebo	k8xC	nebo
dvouřadá	dvouřadý	k2eAgFnSc1d1	dvouřadá
s	s	k7c7	s
klopami	klopa	k1gFnPc7	klopa
šálovými	šálový	k2eAgFnPc7d1	Šálová
či	či	k8xC	či
tradičními	tradiční	k2eAgFnPc7d1	tradiční
<g/>
.	.	kIx.	.
</s>
<s>
Vesta	vesta	k1gFnSc1	vesta
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
bez	bez	k7c2	bez
zadního	zadní	k2eAgInSc2d1	zadní
dílu	díl	k1gInSc2	díl
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
backless	backless	k1gInSc1	backless
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
se	s	k7c7	s
šněrováním	šněrování	k1gNnSc7	šněrování
<g/>
.	.	kIx.	.
</s>
<s>
Motýlek	motýlek	k1gInSc1	motýlek
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
white	white	k5eAaPmIp2nP	white
tie	tie	k?	tie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
z	z	k7c2	z
piké	piké	k1gNnSc2	piké
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
vázaný	vázaný	k2eAgMnSc1d1	vázaný
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
převážně	převážně	k6eAd1	převážně
předvázaný	předvázaný	k2eAgInSc1d1	předvázaný
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
faux	faux	k1gInSc4	faux
pas	pas	k1gInSc4	pas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgFnPc1d1	Černé
kalhoty	kalhoty	k1gFnPc1	kalhoty
mají	mít	k5eAaImIp3nP	mít
lesklý	lesklý	k2eAgInSc4d1	lesklý
lampas	lampas	k1gInSc4	lampas
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
lampasy	lampas	k1gInPc4	lampas
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Frakové	frakový	k2eAgFnPc1d1	fraková
kalhoty	kalhoty	k1gFnPc1	kalhoty
mívají	mívat	k5eAaImIp3nP	mívat
vysoko	vysoko	k6eAd1	vysoko
střižený	střižený	k2eAgInSc4d1	střižený
pas	pas	k1gInSc4	pas
a	a	k8xC	a
upevňují	upevňovat	k5eAaImIp3nP	upevňovat
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
pomocí	pomocí	k7c2	pomocí
knoflíků	knoflík	k1gMnPc2	knoflík
šle	šle	k1gFnSc2	šle
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
měly	mít	k5eAaImAgFnP	mít
kalhoty	kalhoty	k1gFnPc1	kalhoty
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
rozparek	rozparek	k1gInSc4	rozparek
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
rybího	rybí	k2eAgInSc2d1	rybí
ocasu	ocas	k1gInSc2	ocas
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
fishtail	fishtail	k1gInSc1	fishtail
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Frak	frak	k1gInSc1	frak
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgNnSc1d1	různé
množství	množství	k1gNnSc1	množství
knoflíků	knoflík	k1gInPc2	knoflík
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
2	[number]	k4	2
i	i	k8xC	i
žádný	žádný	k1gMnSc1	žádný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Frak	frak	k1gInSc1	frak
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
frakového	frakový	k2eAgInSc2d1	frakový
kabátce	kabátec	k1gInSc2	kabátec
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
frock	frock	k1gMnSc1	frock
coat	coat	k1gMnSc1	coat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
a	a	k8xC	a
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
frak	frak	k1gInSc1	frak
nošen	nošen	k2eAgInSc1d1	nošen
(	(	kIx(	(
<g/>
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
vestou	vesta	k1gFnSc7	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Vesta	vesta	k1gFnSc1	vesta
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
přesahovat	přesahovat	k5eAaImF	přesahovat
pod	pod	k7c4	pod
střih	střih	k1gInSc4	střih
frakového	frakový	k2eAgInSc2d1	frakový
kabátu	kabát	k1gInSc2	kabát
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc4	tento
ovšem	ovšem	k9	ovšem
vždy	vždy	k6eAd1	vždy
neplatilo	platit	k5eNaImAgNnS	platit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Frak	frak	k1gInSc1	frak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejslavnostnější	slavnostní	k2eAgNnSc1d3	nejslavnostnější
a	a	k8xC	a
nejformálnější	formální	k2eAgNnSc1d3	formální
večerní	večerní	k2eAgNnSc1d1	večerní
pánské	pánský	k2eAgNnSc1d1	pánské
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
nahrazen	nahradit	k5eAaPmNgInS	nahradit
smokingem	smoking	k1gInSc7	smoking
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
na	na	k7c4	na
ty	ten	k3xDgFnPc4	ten
nejformálnější	formální	k2eAgFnPc4d3	nejformálnější
večerní	večerní	k2eAgFnPc4d1	večerní
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
ples	ples	k1gInSc1	ples
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
nebo	nebo	k8xC	nebo
státnické	státnický	k2eAgFnPc4d1	státnická
večeře	večeře	k1gFnPc4	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fraku	frak	k1gInSc6	frak
obvykle	obvykle	k6eAd1	obvykle
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
také	také	k9	také
tanečníci	tanečník	k1gMnPc1	tanečník
klasických	klasický	k2eAgInPc2d1	klasický
tanců	tanec	k1gInPc2	tanec
či	či	k8xC	či
hudebníci	hudebník	k1gMnPc1	hudebník
a	a	k8xC	a
dirigenti	dirigent	k1gMnPc1	dirigent
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
fraku	frak	k1gInSc3	frak
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nosit	nosit	k5eAaImF	nosit
bílé	bílý	k2eAgFnPc4d1	bílá
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
z	z	k7c2	z
jemné	jemný	k2eAgFnSc2d1	jemná
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
spíše	spíše	k9	spíše
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
frak	frak	k1gInSc4	frak
nosí	nosit	k5eAaImIp3nS	nosit
vždy	vždy	k6eAd1	vždy
černý	černý	k2eAgInSc1d1	černý
svrchní	svrchní	k2eAgInSc1d1	svrchní
kabát	kabát	k1gInSc1	kabát
nebo	nebo	k8xC	nebo
tradiční	tradiční	k2eAgInSc1d1	tradiční
operní	operní	k2eAgInSc1d1	operní
plášť	plášť	k1gInSc1	plášť
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
pokrývkou	pokrývka	k1gFnSc7	pokrývka
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
cylindr	cylindr	k1gInSc1	cylindr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
specifické	specifický	k2eAgInPc4d1	specifický
účely	účel	k1gInPc4	účel
v	v	k7c6	v
diplomacii	diplomacie	k1gFnSc6	diplomacie
je	být	k5eAaImIp3nS	být
frak	frak	k1gInSc1	frak
výjimečně	výjimečně	k6eAd1	výjimečně
nošen	nosit	k5eAaImNgInS	nosit
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
vestou	vesta	k1gFnSc7	vesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Žaket	žaket	k1gInSc4	žaket
===	===	k?	===
</s>
</p>
<p>
<s>
Žaket	žaket	k1gInSc1	žaket
je	být	k5eAaImIp3nS	být
formální	formální	k2eAgInSc1d1	formální
denní	denní	k2eAgInSc1d1	denní
oblek	oblek	k1gInSc1	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
velké	velká	k1gFnPc4	velká
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
šlechtické	šlechtický	k2eAgFnPc4d1	šlechtická
svatby	svatba	k1gFnPc4	svatba
nebo	nebo	k8xC	nebo
na	na	k7c4	na
dostihy	dostih	k1gInPc4	dostih
Royal	Royal	k1gMnSc1	Royal
Ascot	Ascot	k1gMnSc1	Ascot
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
denní	denní	k2eAgFnSc1d1	denní
obdoba	obdoba	k1gFnSc1	obdoba
fraku	frak	k1gInSc2	frak
<g/>
.	.	kIx.	.
</s>
<s>
Sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
černého	černý	k2eAgInSc2d1	černý
nebo	nebo	k8xC	nebo
antracitového	antracitový	k2eAgInSc2d1	antracitový
kabátu	kabát	k1gInSc2	kabát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
vepředu	vepředu	k6eAd1	vepředu
šikmo	šikmo	k6eAd1	šikmo
ustřižené	ustřižený	k2eAgInPc1d1	ustřižený
šosy	šos	k1gInPc1	šos
<g/>
,	,	kIx,	,
zapínání	zapínání	k1gNnSc1	zapínání
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
knoflík	knoflík	k1gInSc4	knoflík
a	a	k8xC	a
špičaté	špičatý	k2eAgFnSc2d1	špičatá
klopy	klopa	k1gFnSc2	klopa
<g/>
,	,	kIx,	,
šedých	šedý	k2eAgFnPc2d1	šedá
formálních	formální	k2eAgFnPc2d1	formální
kalhot	kalhoty	k1gFnPc2	kalhoty
s	s	k7c7	s
proužky	proužek	k1gInPc7	proužek
<g/>
,	,	kIx,	,
kostkami	kostka	k1gFnPc7	kostka
nebo	nebo	k8xC	nebo
vzorem	vzor	k1gInSc7	vzor
tzv.	tzv.	kA	tzv.
psího	psí	k2eAgInSc2d1	psí
zubu	zub	k1gInSc2	zub
a	a	k8xC	a
světle	světlo	k1gNnSc6	světlo
šedé	šedý	k2eAgFnSc2d1	šedá
nebo	nebo	k8xC	nebo
béžové	béžový	k2eAgFnSc2d1	béžová
vesty	vesta	k1gFnSc2	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Košile	košile	k1gFnSc1	košile
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
nebo	nebo	k8xC	nebo
v	v	k7c6	v
bledé	bledý	k2eAgFnSc6d1	bledá
barvě	barva	k1gFnSc6	barva
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
límečkem	límeček	k1gInSc7	límeček
<g/>
,	,	kIx,	,
manžety	manžeta	k1gFnPc1	manžeta
košile	košile	k1gFnSc2	košile
jsou	být	k5eAaImIp3nP	být
dvojité	dvojitý	k2eAgInPc1d1	dvojitý
na	na	k7c4	na
manžetové	manžetový	k2eAgInPc4d1	manžetový
knoflíčky	knoflíček	k1gInPc4	knoflíček
<g/>
,	,	kIx,	,
límeček	límeček	k1gInSc1	límeček
bývá	bývat	k5eAaImIp3nS	bývat
tradičně	tradičně	k6eAd1	tradičně
odepínací	odepínací	k2eAgNnSc1d1	odepínací
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
žaketu	žaket	k1gInSc3	žaket
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
buď	buď	k8xC	buď
klasická	klasický	k2eAgFnSc1d1	klasická
kravata	kravata	k1gFnSc1	kravata
(	(	kIx(	(
<g/>
tradičně	tradičně	k6eAd1	tradičně
šedá	šedý	k2eAgFnSc1d1	šedá
nebo	nebo	k8xC	nebo
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
i	i	k9	i
barevná	barevný	k2eAgFnSc1d1	barevná
<g/>
)	)	kIx)	)
s	s	k7c7	s
ohrnutým	ohrnutý	k2eAgInSc7d1	ohrnutý
límcem	límec	k1gInSc7	límec
košile	košile	k1gFnSc2	košile
nebo	nebo	k8xC	nebo
formální	formální	k2eAgInSc4d1	formální
askot	askot	k1gInSc4	askot
(	(	kIx(	(
<g/>
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
uvázaný	uvázaný	k2eAgInSc4d1	uvázaný
šátek	šátek	k1gInSc4	šátek
<g/>
)	)	kIx)	)
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
frakovým	frakový	k2eAgInSc7d1	frakový
límečkem	límeček	k1gInSc7	límeček
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
kravaty	kravata	k1gFnSc2	kravata
nebo	nebo	k8xC	nebo
askotu	askot	k1gInSc2	askot
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
i	i	k8xC	i
vzorovaný	vzorovaný	k2eAgInSc1d1	vzorovaný
motýlek	motýlek	k1gInSc1	motýlek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
žaketu	žaket	k1gInSc3	žaket
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
obouvat	obouvat	k5eAaImF	obouvat
buď	buď	k8xC	buď
černé	černý	k2eAgFnPc1d1	černá
polobotky	polobotka	k1gFnPc1	polobotka
s	s	k7c7	s
uzavřeným	uzavřený	k2eAgNnSc7d1	uzavřené
šněrováním	šněrování	k1gNnSc7	šněrování
nebo	nebo	k8xC	nebo
obdobně	obdobně	k6eAd1	obdobně
střižené	střižený	k2eAgFnPc4d1	střižená
kotníkové	kotníkový	k2eAgFnPc4d1	kotníková
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
žaketu	žaket	k1gInSc3	žaket
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
černý	černý	k2eAgInSc1d1	černý
nebo	nebo	k8xC	nebo
šedý	šedý	k2eAgInSc1d1	šedý
cylindr	cylindr	k1gInSc1	cylindr
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
také	také	k9	také
hůlka	hůlka	k1gFnSc1	hůlka
a	a	k8xC	a
šedé	šedý	k2eAgFnPc1d1	šedá
semišové	semišový	k2eAgFnPc1d1	semišová
nebo	nebo	k8xC	nebo
bledě	bledě	k6eAd1	bledě
žluté	žlutý	k2eAgFnPc4d1	žlutá
jelenicové	jelenicový	k2eAgFnPc4d1	jelenicová
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgFnSc7d1	alternativní
variantou	varianta	k1gFnSc7	varianta
<g/>
,	,	kIx,	,
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
je	být	k5eAaImIp3nS	být
žaketový	žaketový	k2eAgInSc4d1	žaketový
oblek	oblek	k1gInSc4	oblek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
morning	morning	k1gInSc1	morning
suit	suita	k1gFnPc2	suita
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
nějž	jenž	k3xRgNnSc2	jenž
má	mít	k5eAaImIp3nS	mít
kabát	kabát	k1gInSc1	kabát
<g/>
,	,	kIx,	,
kalhoty	kalhoty	k1gFnPc1	kalhoty
a	a	k8xC	a
vesta	vesta	k1gFnSc1	vesta
stejnou	stejná	k1gFnSc4	stejná
středně	středně	k6eAd1	středně
šedou	šedý	k2eAgFnSc4d1	šedá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
formální	formální	k2eAgInSc1d1	formální
než	než	k8xS	než
žaket	žaket	k1gInSc1	žaket
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
alternativou	alternativa	k1gFnSc7	alternativa
je	být	k5eAaImIp3nS	být
nošení	nošení	k1gNnSc1	nošení
krátkého	krátký	k2eAgNnSc2d1	krátké
černého	černý	k2eAgNnSc2d1	černé
saka	sako	k1gNnSc2	sako
namísto	namísto	k7c2	namísto
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
žaketového	žaketový	k2eAgInSc2d1	žaketový
kabátu	kabát	k1gInSc2	kabát
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
oblečení	oblečení	k1gNnSc3	oblečení
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
říká	říkat	k5eAaImIp3nS	říkat
Stresemann	Stresemann	k1gInSc1	Stresemann
podle	podle	k7c2	podle
kancléře	kancléř	k1gMnSc2	kancléř
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
nosil	nosit	k5eAaImAgMnS	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Vhodným	vhodný	k2eAgInSc7d1	vhodný
kloboukem	klobouk	k1gInSc7	klobouk
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
buřinka	buřinka	k1gFnSc1	buřinka
nebo	nebo	k8xC	nebo
homburg	homburg	k1gInSc1	homburg
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
méně	málo	k6eAd2	málo
formální	formální	k2eAgFnSc1d1	formální
kombinace	kombinace	k1gFnSc1	kombinace
byla	být	k5eAaImAgFnS	být
běžná	běžný	k2eAgFnSc1d1	běžná
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
půle	půle	k1gFnSc2	půle
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k9	však
prakticky	prakticky	k6eAd1	prakticky
vymizela	vymizet	k5eAaPmAgFnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Stresemann	Stresemann	k1gInSc1	Stresemann
nosí	nosit	k5eAaImIp3nS	nosit
např.	např.	kA	např.
známá	známý	k2eAgFnSc1d1	známá
postava	postava	k1gFnSc1	postava
pořadů	pořad	k1gInPc2	pořad
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
pan	pan	k1gMnSc1	pan
Tau	tau	k1gNnPc2	tau
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
oblek	oblek	k1gInSc1	oblek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
oblek	oblek	k1gInSc1	oblek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
http://twogentlemen.cz/286/tmavy-univerzalni-oblek-vychazkovy-oblek-a-zaket/	[url]	k4	http://twogentlemen.cz/286/tmavy-univerzalni-oblek-vychazkovy-oblek-a-zaket/
</s>
</p>
<p>
<s>
http://twogentlemen.cz/244/smoking-a-frak/	[url]	k4	http://twogentlemen.cz/244/smoking-a-frak/
</s>
</p>
