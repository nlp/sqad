<s>
Roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnPc7	Galile
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
objevil	objevit	k5eAaPmAgMnS	objevit
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
obíhající	obíhající	k2eAgFnSc4d1	obíhající
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gInSc1	Jupiter
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Koperníkovu	Koperníkův	k2eAgFnSc4d1	Koperníkova
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
Slunci	slunce	k1gNnSc6	slunce
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
a	a	k8xC	a
planetách	planeta	k1gFnPc6	planeta
kroužících	kroužící	k2eAgFnPc6d1	kroužící
kolem	kolem	k6eAd1	kolem
<g/>
.	.	kIx.	.
</s>
