<s>
Jak	jak	k6eAd1	jak
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
hledal	hledat	k5eAaImAgInS	hledat
příznivé	příznivý	k2eAgNnSc4d1	příznivé
podnebí	podnebí	k1gNnSc4	podnebí
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
choroby	choroba	k1gFnPc4	choroba
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
;	;	kIx,	;
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
Sils-Maria	Sils-Marium	k1gNnPc4	Sils-Marium
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
Rapallo	Rapallo	k1gNnSc1	Rapallo
<g/>
,	,	kIx,	,
Turín	Turín	k1gInSc1	Turín
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
<g/>
.	.	kIx.	.
</s>
