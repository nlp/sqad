<s>
Desatero	desatero	k1gNnSc1
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c7
„	„	k?
<g/>
katechetický	katechetický	k2eAgInSc4d1
souhrn	souhrn	k1gInSc4
morálky	morálka	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
recitováno	recitovat	k5eAaImNgNnS
v	v	k7c6
jeruzalémském	jeruzalémský	k2eAgInSc6d1
chrámě	chrám	k1gInSc6
<g/>
,	,	kIx,
zbožní	zbožnit	k5eAaPmIp3nP
Židé	Žid	k1gMnPc1
je	on	k3xPp3gNnSc4
recitují	recitovat	k5eAaImIp3nP
denně	denně	k6eAd1
při	při	k7c6
ranní	ranní	k2eAgFnSc6d1
modlitbě	modlitba	k1gFnSc6
<g/>
.	.	kIx.
</s>