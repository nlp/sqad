<s>
Desatero	desatero	k1gNnSc1
</s>
<s>
Rukopis	rukopis	k1gInSc1
s	s	k7c7
Desaterem	desatero	k1gNnSc7
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Nashův	Nashův	k2eAgInSc1d1
papyrus	papyrus	k1gInSc1
</s>
<s>
Desatero	desatero	k1gNnSc1
<g/>
,	,	kIx,
Desatero	desatero	k1gNnSc1
přikázání	přikázání	k1gNnSc1
<g/>
,	,	kIx,
Desatero	desatero	k1gNnSc1
Božích	boží	k2eAgNnPc2d1
přikázání	přikázání	k1gNnPc2
či	či	k8xC
dekalog	dekalog	k1gInSc4
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
δ	δ	k?
deka	deka	k1gNnSc2
deset	deset	k4xCc4
+	+	kIx~
λ	λ	k?
logos	logos	k1gInSc1
slovo	slovo	k1gNnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
hebrejsky	hebrejsky	k6eAd1
ע	ע	k?
ה	ה	k?
Deset	deset	k4xCc4
slov	slovo	k1gNnPc2
<g/>
)	)	kIx)
považují	považovat	k5eAaImIp3nP
židé	žid	k1gMnPc1
a	a	k8xC
křesťané	křesťan	k1gMnPc1
za	za	k7c4
příkazy	příkaz	k1gInPc4
(	(	kIx(
<g/>
přikázání	přikázání	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
Bůh	bůh	k1gMnSc1
dal	dát	k5eAaPmAgMnS
lidem	člověk	k1gMnPc3
jako	jako	k8xS,k8xC
směrnici	směrnice	k1gFnSc3
pro	pro	k7c4
způsob	způsob	k1gInSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Starého	Starého	k2eAgInSc2d1
zákona	zákon	k1gInSc2
předal	předat	k5eAaPmAgMnS
Bůh	bůh	k1gMnSc1
dvě	dva	k4xCgFnPc4
desky	deska	k1gFnPc1
s	s	k7c7
přikázáními	přikázání	k1gNnPc7
na	na	k7c6
hoře	hora	k1gFnSc6
Sinaj	Sinaj	k1gInSc4
do	do	k7c2
rukou	ruka	k1gFnPc2
Mojžíšových	Mojžíšová	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přikázání	přikázání	k1gNnSc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Bibli	bible	k1gFnSc6
vyjmenována	vyjmenovat	k5eAaPmNgFnS
dvakrát	dvakrát	k6eAd1
<g/>
;	;	kIx,
poprvé	poprvé	k6eAd1
v	v	k7c6
knize	kniha	k1gFnSc6
Exodus	Exodus	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
podruhé	podruhé	k6eAd1
v	v	k7c6
knize	kniha	k1gFnSc6
Deuteronomium	Deuteronomium	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Novém	nový	k2eAgInSc6d1
zákoně	zákon	k1gInSc6
není	být	k5eNaImIp3nS
Desatero	desatero	k1gNnSc1
nikde	nikde	k6eAd1
výslovně	výslovně	k6eAd1
zmíněno	zmínit	k5eAaPmNgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
najdeme	najít	k5eAaPmIp1nP
v	v	k7c6
něm	on	k3xPp3gNnSc6
neúplné	úplný	k2eNgNnSc1d1
výčty	výčet	k1gInPc7
přikázání	přikázání	k1gNnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
jejich	jejich	k3xOp3gFnSc1
parafráze	parafráze	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
Desatera	desatero	k1gNnSc2
</s>
<s>
Pergamen	pergamen	k1gInSc1
(	(	kIx(
<g/>
612	#num#	k4
<g/>
x	x	k?
<g/>
502	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
z	z	k7c2
r.	r.	kA
1768	#num#	k4
od	od	k7c2
Jekuti	Jekuť	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ela	ela	k1gFnSc1
Sofera	Sofera	k1gFnSc1
zpodobňující	zpodobňující	k2eAgNnSc4d1
Desatero	desatero	k1gNnSc4
z	z	k7c2
r.	r.	kA
1675	#num#	k4
v	v	k7c6
amsterdamské	amsterdamský	k2eAgFnSc6d1
synagoze	synagoga	k1gFnSc6
Esnoga	Esnoga	k1gFnSc1
</s>
<s>
Pro	pro	k7c4
etiku	etika	k1gFnSc4
v	v	k7c6
židovství	židovství	k1gNnSc6
a	a	k8xC
křesťanství	křesťanství	k1gNnSc6
má	mít	k5eAaImIp3nS
Desatero	desatero	k1gNnSc1
přikázání	přikázání	k1gNnSc2
zásadní	zásadní	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
křesťanství	křesťanství	k1gNnSc6
se	se	k3xPyFc4
význam	význam	k1gInSc1
Desatera	desatero	k1gNnSc2
pro	pro	k7c4
spásu	spása	k1gFnSc4
člověka	člověk	k1gMnSc2
podle	podle	k7c2
Ježíše	Ježíš	k1gMnSc2
naplnil	naplnit	k5eAaPmAgInS
–	–	k?
výslednou	výsledný	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
přebralo	přebrat	k5eAaPmAgNnS
dvojí	dvojí	k4xRgNnSc1
přikázání	přikázání	k1gNnSc1
lásky	láska	k1gFnSc2
k	k	k7c3
Bohu	bůh	k1gMnSc3
a	a	k8xC
k	k	k7c3
bližnímu	bližní	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tomto	tento	k3xDgInSc6
dvojím	dvojí	k4xRgInSc6
příkazu	příkaz	k1gInSc6
však	však	k9
vidí	vidět	k5eAaImIp3nS
naplnění	naplnění	k1gNnSc4
Mojžíšova	Mojžíšův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
(	(	kIx(
<g/>
Tóry	tóra	k1gFnSc2
<g/>
)	)	kIx)
i	i	k9
židovství	židovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Desatero	desatero	k1gNnSc1
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c7
„	„	k?
<g/>
katechetický	katechetický	k2eAgInSc4d1
souhrn	souhrn	k1gInSc4
morálky	morálka	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
recitováno	recitovat	k5eAaImNgNnS
v	v	k7c6
jeruzalémském	jeruzalémský	k2eAgInSc6d1
chrámě	chrám	k1gInSc6
<g/>
,	,	kIx,
zbožní	zbožnit	k5eAaPmIp3nP
Židé	Žid	k1gMnPc1
je	on	k3xPp3gNnSc4
recitují	recitovat	k5eAaImIp3nP
denně	denně	k6eAd1
při	při	k7c6
ranní	ranní	k2eAgFnSc6d1
modlitbě	modlitba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
pro	pro	k7c4
Židy	Žid	k1gMnPc4
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
platnost	platnost	k1gFnSc1
omezuje	omezovat	k5eAaImIp3nS
na	na	k7c4
národ	národ	k1gInSc4
vyvedený	vyvedený	k2eAgInSc4d1
z	z	k7c2
egyptského	egyptský	k2eAgNnSc2d1
otroctví	otroctví	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
křesťanské	křesťanský	k2eAgFnSc6d1
katechezi	katecheze	k1gFnSc6
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
připisována	připisován	k2eAgFnSc1d1
platnost	platnost	k1gFnSc1
univerzální	univerzální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Desatero	desatero	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
evropské	evropský	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
i	i	k9
mimo	mimo	k7c4
okruh	okruh	k1gInSc4
náboženství	náboženství	k1gNnSc2
považováno	považován	k2eAgNnSc4d1
často	často	k6eAd1
za	za	k7c4
„	„	k?
<g/>
etické	etický	k2eAgNnSc4d1
minimum	minimum	k1gNnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
neboť	neboť	k8xC
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
resp.	resp.	kA
5	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
až	až	k9
10	#num#	k4
<g/>
.	.	kIx.
přikázání	přikázání	k1gNnSc1
Desatera	desatero	k1gNnSc2
vymezuje	vymezovat	k5eAaImIp3nS
základní	základní	k2eAgInPc4d1
mezilidské	mezilidský	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
2	#num#	k4
<g/>
.	.	kIx.
deska	deska	k1gFnSc1
Zákona	zákon	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znalost	znalost	k1gFnSc1
znění	znění	k1gNnSc2
Desatera	desatero	k1gNnSc2
však	však	k8xC
obecná	obecná	k1gFnSc1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Členění	členění	k1gNnSc1
a	a	k8xC
číslování	číslování	k1gNnSc1
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1
přikázání	přikázání	k1gNnPc4
Desatera	desatero	k1gNnSc2
se	se	k3xPyFc4
podle	podle	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
tradic	tradice	k1gFnPc2
číslují	číslovat	k5eAaImIp3nP
odlišně	odlišně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovská	židovský	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
(	(	kIx(
<g/>
Ž	Ž	kA
<g/>
)	)	kIx)
chápe	chápat	k5eAaImIp3nS
sebepředstavení	sebepředstavení	k1gNnSc4
Boha	bůh	k1gMnSc2
jako	jako	k8xS,k8xC
samostatné	samostatný	k2eAgNnSc4d1
první	první	k4xOgNnSc4
přikázání	přikázání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číslování	číslování	k1gNnSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
pravoslavných	pravoslavný	k2eAgFnPc2d1
a	a	k8xC
reformovaných	reformovaný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
ponechává	ponechávat	k5eAaImIp3nS
první	první	k4xOgNnPc1
dvě	dva	k4xCgNnPc1
přikázání	přikázání	k1gNnPc1
(	(	kIx(
<g/>
tzn.	tzn.	kA
příkaz	příkaz	k1gInSc4
monoteismu	monoteismus	k1gInSc2
a	a	k8xC
zákaz	zákaz	k1gInSc1
zobrazování	zobrazování	k1gNnSc2
Boha	bůh	k1gMnSc2
<g/>
)	)	kIx)
rozdělené	rozdělený	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číslování	číslování	k1gNnSc1
(	(	kIx(
<g/>
B	B	kA
<g/>
)	)	kIx)
běžné	běžný	k2eAgNnSc1d1
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
a	a	k8xC
lutherských	lutherský	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
tyto	tento	k3xDgFnPc4
dvě	dva	k4xCgFnPc4
sloučilo	sloučit	k5eAaPmAgNnS
a	a	k8xC
rozděluje	rozdělovat	k5eAaImIp3nS
naopak	naopak	k6eAd1
poslední	poslední	k2eAgNnSc4d1
přikázání	přikázání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
číslování	číslování	k1gNnSc1
zachovává	zachovávat	k5eAaImIp3nS
sled	sled	k1gInSc4
knihy	kniha	k1gFnSc2
Exodus	Exodus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Oba	dva	k4xCgInPc1
výčty	výčet	k1gInPc1
v	v	k7c6
Exodu	Exodus	k1gInSc6
a	a	k8xC
Deuteronomiu	Deuteronomium	k1gNnSc6
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
především	především	k6eAd1
základem	základ	k1gInSc7
příkazu	příkaz	k1gInSc2
sobotního	sobotní	k2eAgInSc2d1
odpočinku	odpočinek	k1gInSc2
–	–	k?
Exodus	Exodus	k1gInSc1
jej	on	k3xPp3gMnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
připomínku	připomínka	k1gFnSc4
sedmý	sedmý	k4xOgInSc4
den	den	k1gInSc4
stvoření	stvoření	k1gNnSc2
<g/>
,	,	kIx,
Deuteronomium	Deuteronomium	k1gNnSc1
za	za	k7c4
připomínku	připomínka	k1gFnSc4
vyjití	vyjití	k1gNnSc2
Izraelitů	izraelita	k1gMnPc2
z	z	k7c2
egyptského	egyptský	k2eAgNnSc2d1
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Letniční	letniční	k2eAgNnPc1d1
hnutí	hnutí	k1gNnPc1
<g/>
,	,	kIx,
evangelikální	evangelikální	k2eAgNnPc1d1
hnutí	hnutí	k1gNnPc1
a	a	k8xC
svobodné	svobodný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
obvykle	obvykle	k6eAd1
přejímají	přejímat	k5eAaImIp3nP
znění	znění	k1gNnSc4
Desatera	desatero	k1gNnSc2
doslovně	doslovně	k6eAd1
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
ostatní	ostatní	k2eAgFnPc1d1
křesťanské	křesťanský	k2eAgFnPc1d1
církve	církev	k1gFnPc1
obvykle	obvykle	k6eAd1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
kratšího	krátký	k2eAgInSc2d2
textu	text	k1gInSc2
navazujícího	navazující	k2eAgInSc2d1
zejména	zejména	k9
na	na	k7c4
sv.	sv.	kA
Augustina	Augustin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
si	se	k3xPyFc3
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
spise	spis	k1gInSc6
Questiones	Questiones	k1gMnSc1
in	in	k?
Exodum	Exodum	k1gInSc1
kladl	klást	k5eAaImAgInS
nad	nad	k7c7
Desaterem	desatero	k1gNnSc7
pět	pět	k4xCc4
otázek	otázka	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeho	jeho	k3xOp3gFnPc1
odpovědi	odpověď	k1gFnPc1
zásadně	zásadně	k6eAd1
ovlivnily	ovlivnit	k5eAaPmAgFnP
výklad	výklad	k1gInSc4
textu	text	k1gInSc2
v	v	k7c6
západním	západní	k2eAgNnSc6d1
křesťanství	křesťanství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Augustin	Augustin	k1gMnSc1
prosazoval	prosazovat	k5eAaImAgMnS
ze	z	k7c2
symbolických	symbolický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
dělení	dělení	k1gNnSc2
3	#num#	k4
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přikázání	přikázání	k1gNnSc1
„	„	k?
<g/>
nezabiješ	zabít	k5eNaPmIp2nS
<g/>
“	“	k?
vykládal	vykládat	k5eAaImAgMnS
úzce	úzko	k6eAd1
jako	jako	k9
zákaz	zákaz	k1gInSc1
nespravedlivého	spravedlivý	k2eNgNnSc2d1
zabití	zabití	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
přikázání	přikázání	k1gNnSc1
„	„	k?
<g/>
nezcizoložíš	zcizoložit	k5eNaPmIp2nS
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
nepromluvíš	promluvit	k5eNaPmIp2nS
křivé	křivý	k2eAgNnSc4d1
svědectví	svědectví	k1gNnSc4
<g/>
“	“	k?
chápal	chápat	k5eAaImAgMnS
široce	široko	k6eAd1
jako	jako	k8xS,k8xC
zákaz	zákaz	k1gInSc4
jakéhokoliv	jakýkoliv	k3yIgNnSc2
smilstva	smilstvo	k1gNnSc2
<g/>
,	,	kIx,
respektive	respektive	k9
lži	lež	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákaz	zákaz	k1gInSc1
bažení	bažení	k1gNnSc2
pak	pak	k6eAd1
vysvětloval	vysvětlovat	k5eAaImAgMnS
jako	jako	k9
vnitřní	vnitřní	k2eAgInSc4d1
hřích	hřích	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Pozitivní	pozitivní	k2eAgFnSc1d1
formulace	formulace	k1gFnSc1
prvního	první	k4xOgNnSc2
přikázání	přikázání	k1gNnSc2
„	„	k?
<g/>
V	v	k7c4
jednoho	jeden	k4xCgMnSc4
Boha	bůh	k1gMnSc4
věřiti	věřit	k5eAaImF
budeš	být	k5eAaImBp2nS
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
až	až	k9
z	z	k7c2
konce	konec	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
ji	on	k3xPp3gFnSc4
použil	použít	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Kanisius	Kanisius	k1gMnSc1
v	v	k7c6
německé	německý	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
svého	svůj	k3xOyFgInSc2
Malého	Malého	k2eAgInSc2d1
katechismu	katechismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
češtiny	čeština	k1gFnSc2
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1778	#num#	k4
při	při	k7c6
překladu	překlad	k1gInSc6
vídeňského	vídeňský	k2eAgInSc2d1
Velkého	velký	k2eAgInSc2d1
katechismu	katechismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
variantu	varianta	k1gFnSc4
proto	proto	k8xC
používají	používat	k5eAaImIp3nP
jen	jen	k9
německy	německy	k6eAd1
a	a	k8xC
česky	česky	k6eAd1
mluvící	mluvící	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ž	Ž	kA
</s>
<s>
A	a	k9
</s>
<s>
B	B	kA
</s>
<s>
Text	text	k1gInSc1
v	v	k7c6
Exodu	Exodus	k1gInSc6
</s>
<s>
Text	text	k1gInSc1
v	v	k7c6
Deuteronomiu	Deuteronomium	k1gNnSc6
</s>
<s>
Katechetická	katechetický	k2eAgFnSc1d1
formule	formule	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Já	já	k3xPp1nSc1
jsem	být	k5eAaImIp1nS
Hospodin	Hospodin	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k3xOp2gMnSc1
Bůh	bůh	k1gMnSc1
<g/>
;	;	kIx,
já	já	k3xPp1nSc1
jsem	být	k5eAaImIp1nS
tě	ty	k3xPp2nSc4
vyvedl	vyvést	k5eAaPmAgInS
z	z	k7c2
egyptské	egyptský	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
domu	dům	k1gInSc2
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c4
jednoho	jeden	k4xCgMnSc4
Boha	bůh	k1gMnSc4
věřiti	věřit	k5eAaImF
budeš	být	k5eAaImBp2nS
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nebudeš	být	k5eNaImBp2nS
mít	mít	k5eAaImF
jiného	jiný	k2eAgMnSc4d1
boha	bůh	k1gMnSc4
mimo	mimo	k7c4
mne	já	k3xPp1nSc4
<g/>
.	.	kIx.
</s>
<s>
Nebudeš	být	k5eNaImBp2nS
mít	mít	k5eAaImF
jiné	jiný	k2eAgMnPc4d1
bohy	bůh	k1gMnPc4
mimo	mimo	k7c4
mne	já	k3xPp1nSc4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nezobrazíš	zobrazit	k5eNaPmIp2nS
si	se	k3xPyFc3
Boha	bůh	k1gMnSc4
zpodobením	zpodobení	k1gNnPc3
ničeho	nic	k3yNnSc2
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
nahoře	nahoře	k6eAd1
na	na	k7c6
nebi	nebe	k1gNnSc6
<g/>
,	,	kIx,
dole	dole	k6eAd1
na	na	k7c6
zemi	zem	k1gFnSc6
nebo	nebo	k8xC
ve	v	k7c6
vodách	voda	k1gFnPc6
pod	pod	k7c7
zemí	zem	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebudeš	být	k5eNaImBp2nS
se	se	k3xPyFc4
ničemu	ničema	k1gFnSc4
takovému	takový	k3xDgMnSc3
klanět	klanět	k5eAaImF
ani	ani	k8xC
tomu	ten	k3xDgInSc3
sloužit	sloužit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
jsem	být	k5eAaImIp1nS
Hospodin	Hospodin	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k3xOp2gInSc4
Bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
Bůh	bůh	k1gMnSc1
žárlivě	žárlivě	k6eAd1
milující	milující	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stíhám	stíhat	k5eAaImIp1nS
vinu	vina	k1gFnSc4
otců	otec	k1gMnPc2
na	na	k7c6
synech	syn	k1gMnPc6
do	do	k7c2
třetího	třetí	k4xOgMnSc2
i	i	k8xC
čtvrtého	čtvrtý	k4xOgNnSc2
pokolení	pokolení	k1gNnPc2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mě	já	k3xPp1nSc4
nenávidí	nenávidět	k5eAaImIp3nP,k5eNaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
prokazuji	prokazovat	k5eAaImIp1nS
milosrdenství	milosrdenství	k1gNnSc1
tisícům	tisíc	k4xCgInPc3
pokolení	pokolení	k1gNnPc2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
mě	já	k3xPp1nSc4
milují	milovat	k5eAaImIp3nP
a	a	k8xC
má	mít	k5eAaImIp3nS
přikázání	přikázání	k1gNnSc1
zachovávají	zachovávat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nezneužiješ	zneužít	k5eNaPmIp2nS
jména	jméno	k1gNnPc4
Hospodina	Hospodin	k1gMnSc2
<g/>
,	,	kIx,
svého	svůj	k3xOyFgMnSc4
Boha	bůh	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodin	Hospodin	k1gMnSc1
nenechá	nechat	k5eNaPmIp3nS
bez	bez	k7c2
trestu	trest	k1gInSc2
toho	ten	k3xDgInSc2
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
jeho	jeho	k3xOp3gMnPc3
jméno	jméno	k1gNnSc4
zneužíval	zneužívat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Nevezmeš	vzít	k5eNaPmIp2nS
jména	jméno	k1gNnSc2
Božího	boží	k2eAgNnSc2d1
nadarmo	nadarmo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pamatuj	pamatovat	k5eAaImRp2nS
na	na	k7c4
den	den	k1gInSc4
odpočinku	odpočinek	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ti	ty	k3xPp2nSc3
byl	být	k5eAaImAgMnS
svatý	svatý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc1
dní	den	k1gInPc2
budeš	být	k5eAaImBp2nS
pracovat	pracovat	k5eAaImF
a	a	k8xC
dělat	dělat	k5eAaImF
všechnu	všechen	k3xTgFnSc4
svou	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
sedmý	sedmý	k4xOgInSc1
den	den	k1gInSc1
je	být	k5eAaImIp3nS
dnem	den	k1gInSc7
odpočinku	odpočinek	k1gInSc2
<g/>
,	,	kIx,
zasvěceným	zasvěcený	k2eAgMnPc3d1
Hospodinu	Hospodin	k1gMnSc3
<g/>
,	,	kIx,
tvému	tvůj	k1gMnSc3
Bohu	bůh	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebudeš	být	k5eNaImBp2nS
dělat	dělat	k5eAaImF
žádnou	žádný	k3yNgFnSc4
práci	práce	k1gFnSc4
–	–	k?
ty	ty	k3xPp2nSc1
<g/>
,	,	kIx,
tvůj	tvůj	k1gMnSc1
syn	syn	k1gMnSc1
ani	ani	k8xC
tvá	tvůj	k3xOp2gFnSc1
dcera	dcera	k1gFnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k1gMnSc1
otrok	otrok	k1gMnSc1
ani	ani	k8xC
tvá	tvůj	k3xOp2gFnSc1
děvečka	děvečka	k1gFnSc1
<g/>
,	,	kIx,
tvé	tvůj	k3xOp2gNnSc1
dobytče	dobytče	k1gNnSc1
ani	ani	k8xC
přistěhovalec	přistěhovalec	k1gMnSc1
ve	v	k7c6
tvých	tvůj	k3xOp2gFnPc6
branách	brána	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
šesti	šest	k4xCc6
dnech	den	k1gInPc6
totiž	totiž	k9
Hospodin	Hospodin	k1gMnSc1
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
nebe	nebe	k1gNnSc4
i	i	k8xC
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
moře	moře	k1gNnSc4
a	a	k8xC
všechno	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
nich	on	k3xPp3gMnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
sedmého	sedmý	k4xOgInSc2
dne	den	k1gInSc2
odpočinul	odpočinout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
Hospodin	Hospodin	k1gMnSc1
požehnal	požehnat	k5eAaPmAgMnS
sobotní	sobotní	k2eAgInSc4d1
den	den	k1gInSc4
a	a	k8xC
posvětil	posvětit	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Dbej	dbát	k5eAaImRp2nS
na	na	k7c4
den	den	k1gInSc4
odpočinku	odpočinek	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ti	ty	k3xPp2nSc3
byl	být	k5eAaImAgInS
svatý	svatý	k2eAgInSc1d1
<g/>
,	,	kIx,
jak	jak	k6eAd1
ti	ty	k3xPp2nSc3
přikázal	přikázat	k5eAaPmAgMnS
Hospodin	Hospodin	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k1gMnSc1
Bůh	bůh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc1
dní	den	k1gInPc2
budeš	být	k5eAaImBp2nS
pracovat	pracovat	k5eAaImF
<g/>
...	...	k?
ani	ani	k8xC
tvůj	tvůj	k3xOp2gMnSc1
host	host	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
žije	žít	k5eAaImIp3nS
v	v	k7c6
tvých	tvůj	k3xOp2gFnPc6
branách	brána	k1gFnPc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odpočinul	odpočinout	k5eAaPmAgMnS
tvůj	tvůj	k3xOp2gMnSc1
otrok	otrok	k1gMnSc1
a	a	k8xC
tvá	tvůj	k3xOp2gFnSc1
otrokyně	otrokyně	k1gFnSc1
tak	tak	k9
jako	jako	k9
ty	ten	k3xDgFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pamatuj	pamatovat	k5eAaImRp2nS
<g/>
,	,	kIx,
že	že	k8xS
jsi	být	k5eAaImIp2nS
byl	být	k5eAaImAgMnS
otrokem	otrok	k1gMnSc7
v	v	k7c6
egyptské	egyptský	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
a	a	k8xC
že	že	k8xS
tě	ty	k3xPp2nSc4
Hospodin	Hospodin	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k3xOp2gMnSc1
Bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
odtud	odtud	k6eAd1
vyvedl	vyvést	k5eAaPmAgMnS
pevnou	pevný	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
a	a	k8xC
vztaženou	vztažený	k2eAgFnSc7d1
paží	paže	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k6eAd1
ti	ty	k3xPp2nSc3
přikázal	přikázat	k5eAaPmAgMnS
Hospodin	Hospodin	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k3xOp2gMnSc1
Bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
dodržovat	dodržovat	k5eAaImF
den	den	k1gInSc4
odpočinku	odpočinek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pomni	pomnout	k5eAaPmRp2nS
<g/>
,	,	kIx,
abys	aby	kYmCp2nS
den	den	k1gInSc1
sváteční	sváteční	k2eAgFnSc2d1
světil	světit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Cti	čest	k1gFnSc3
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
a	a	k8xC
svou	svůj	k3xOyFgFnSc4
matku	matka	k1gFnSc4
<g/>
,	,	kIx,
abys	aby	kYmCp2nS
byl	být	k5eAaImAgInS
dlouho	dlouho	k6eAd1
živ	živ	k2eAgMnSc1d1
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
ti	ten	k3xDgMnPc1
dává	dávat	k5eAaImIp3nS
Hospodin	Hospodin	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k1gMnSc1
Bůh	bůh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Cti	čest	k1gFnSc3
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
i	i	k9
matku	matka	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k6eAd1
ti	ty	k3xPp2nSc3
přikázal	přikázat	k5eAaPmAgMnS
Hospodin	Hospodin	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k3xOp2gMnSc1
Bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
abys	aby	kYmCp2nS
dlouho	dlouho	k6eAd1
byl	být	k5eAaImAgInS
živ	živ	k2eAgMnSc1d1
a	a	k8xC
dobře	dobře	k6eAd1
se	se	k3xPyFc4
ti	ty	k3xPp2nSc3
vedlo	vést	k5eAaImAgNnS
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
ti	ten	k3xDgMnPc1
dává	dávat	k5eAaImIp3nS
Hospodin	Hospodin	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k1gMnSc1
Bůh	bůh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Cti	čest	k1gFnSc3
otce	otka	k1gFnSc3
svého	své	k1gNnSc2
i	i	k8xC
matku	matka	k1gFnSc4
svou	svůj	k3xOyFgFnSc4
<g/>
,	,	kIx,
abys	aby	kYmCp2nS
dlouho	dlouho	k6eAd1
živ	živ	k2eAgInSc1d1
byl	být	k5eAaImAgInS
a	a	k8xC
dobře	dobře	k6eAd1
ti	ty	k3xPp2nSc3
bylo	být	k5eAaImAgNnS
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nezabiješ	zabít	k5eNaPmIp2nS
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nesesmilníš	Nesesmilnět	k5eAaImIp2nS,k5eAaPmIp2nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
dosl.	dosl.	k?
Nezcizoložíš	zcizoložit	k5eNaPmIp2nS
<g/>
)	)	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nepokradeš	pokrást	k5eNaPmIp2nS
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nevydáš	vydat	k5eNaPmIp2nS
proti	proti	k7c3
svému	svůj	k3xOyFgMnSc3
bližnímu	bližní	k1gMnSc3
křivé	křivý	k2eAgNnSc1d1
svědectví	svědectví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Nepromluvíš	promluvit	k5eNaPmIp2nS
křivého	křivý	k2eAgNnSc2d1
svědectví	svědectví	k1gNnSc2
proti	proti	k7c3
bližnímu	bližní	k1gMnSc3
svému	svůj	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nebudeš	být	k5eNaImBp2nS
dychtit	dychtit	k5eAaImF
po	po	k7c6
domě	dům	k1gInSc6
svého	svůj	k3xOyFgMnSc2
bližního	bližní	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Nebudeš	být	k5eNaImBp2nS
dychtit	dychtit	k5eAaImF
po	po	k7c6
ženě	žena	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
bližního	bližní	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebudeš	být	k5eNaImBp2nS
toužit	toužit	k5eAaImF
po	po	k7c6
domě	dům	k1gInSc6
svého	svůj	k3xOyFgMnSc2
bližního	bližní	k1gMnSc2
ani	ani	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
poli	pole	k1gNnSc6
ani	ani	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gMnSc6
otroku	otrok	k1gMnSc6
ani	ani	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
otrokyni	otrokyně	k1gFnSc6
ani	ani	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gMnSc6
býku	býk	k1gMnSc6
ani	ani	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gMnSc6
oslu	osel	k1gMnSc6
<g/>
,	,	kIx,
vůbec	vůbec	k9
po	po	k7c6
ničem	nic	k3yNnSc6
<g/>
,	,	kIx,
co	co	k9
patří	patřit	k5eAaImIp3nS
tvému	tvůj	k3xOp2gMnSc3
bližnímu	bližní	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Nepožádáš	požádat	k5eNaPmIp2nS
manželky	manželka	k1gFnPc4
bližního	bližní	k1gMnSc2
svého	svůj	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nebudeš	být	k5eNaImBp2nS
dychtit	dychtit	k5eAaImF
po	po	k7c6
ženě	žena	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
bližního	bližní	k1gMnSc2
ani	ani	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gMnSc6
otroku	otrok	k1gMnSc6
nebo	nebo	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
otrokyni	otrokyně	k1gFnSc6
ani	ani	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gMnSc6
býku	býk	k1gMnSc6
ani	ani	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gMnSc6
oslu	osel	k1gMnSc6
<g/>
,	,	kIx,
vůbec	vůbec	k9
po	po	k7c6
ničem	nic	k3yNnSc6
<g/>
,	,	kIx,
co	co	k9
patří	patřit	k5eAaImIp3nS
tvému	tvůj	k3xOp2gMnSc3
bližnímu	bližní	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Aniž	aniž	k8xS,k8xC
požádáš	požádat	k5eAaPmIp2nS
statku	statek	k1gInSc2
jeho	jeho	k3xOp3gInSc1
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Biblické	biblický	k2eAgFnPc4d1
varianty	varianta	k1gFnPc4
podle	podle	k7c2
českého	český	k2eAgInSc2d1
ekumenického	ekumenický	k2eAgInSc2d1
překladu	překlad	k1gInSc2
<g/>
,	,	kIx,
katechetické	katechetický	k2eAgFnSc2d1
formule	formule	k1gFnSc2
podle	podle	k7c2
katolického	katolický	k2eAgInSc2d1
jednotného	jednotný	k2eAgInSc2d1
Kancionálu	kancionál	k1gInSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Podle	podle	k7c2
zmínky	zmínka	k1gFnSc2
o	o	k7c6
„	„	k?
<g/>
dvou	dva	k4xCgFnPc6
deskách	deska	k1gFnPc6
Zákona	zákon	k1gInSc2
<g/>
“	“	k?
se	se	k3xPyFc4
vžilo	vžít	k5eAaPmAgNnS
dělit	dělit	k5eAaImF
v	v	k7c6
katechezi	katecheze	k1gFnSc6
Desatero	desatero	k1gNnSc4
na	na	k7c4
první	první	k4xOgFnSc4
a	a	k8xC
druhou	druhý	k4xOgFnSc4
desku	deska	k1gFnSc4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
=	=	kIx~
vztah	vztah	k1gInSc4
k	k	k7c3
Bohu	bůh	k1gMnSc3
<g/>
;	;	kIx,
2	#num#	k4
<g/>
.	.	kIx.
=	=	kIx~
vztah	vztah	k1gInSc4
k	k	k7c3
lidem	člověk	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťanská	křesťanský	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
ovšem	ovšem	k9
začíná	začínat	k5eAaImIp3nS
2	#num#	k4
<g/>
.	.	kIx.
desku	deska	k1gFnSc4
příkazem	příkaz	k1gInSc7
úcty	úcta	k1gFnSc2
k	k	k7c3
rodičům	rodič	k1gMnPc3
<g/>
,	,	kIx,
Židé	Žid	k1gMnPc1
jej	on	k3xPp3gMnSc4
řadí	řadit	k5eAaImIp3nP
ještě	ještě	k6eAd1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
neboť	neboť	k8xC
vztah	vztah	k1gInSc1
rodičů	rodič	k1gMnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
náboženskou	náboženský	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
-	-	kIx~
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
třetím	třetí	k4xOgMnSc7
partnerem	partner	k1gMnSc7
při	při	k7c6
plození	plození	k1gNnSc6
dítěte	dítě	k1gNnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
Desatera	desatero	k1gNnSc2
někdy	někdy	k6eAd1
považuje	považovat	k5eAaImIp3nS
přikázání	přikázání	k1gNnSc4
o	o	k7c6
víře	víra	k1gFnSc6
v	v	k7c4
jediného	jediný	k2eAgMnSc4d1
Boha	bůh	k1gMnSc4
za	za	k7c4
součást	součást	k1gFnSc4
1	#num#	k4
<g/>
.	.	kIx.
židovského	židovský	k2eAgNnSc2d1
přikázání	přikázání	k1gNnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Já	já	k3xPp1nSc1
jsem	být	k5eAaImIp1nS
Hospodin	Hospodin	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k3xOp2gMnSc1
Bůh	bůh	k1gMnSc1
…	…	k?
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
přikázání	přikázání	k1gNnSc1
o	o	k7c6
nezobrazování	nezobrazování	k1gNnSc6
Boha	bůh	k1gMnSc2
za	za	k7c4
samostatný	samostatný	k2eAgInSc4d1
článek	článek	k1gInSc4
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
v	v	k7c6
evangelické	evangelický	k2eAgFnSc6d1
tradici	tradice	k1gFnSc6
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Filon	Filon	k1gInSc1
Alexandrijský	alexandrijský	k2eAgInSc1d1
<g/>
,	,	kIx,
H.	H.	kA
Gese	Gese	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
Desatera	desatero	k1gNnSc2
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1
výklad	výklad	k1gInSc1
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Zákon	zákon	k1gInSc1
v	v	k7c6
dnešním	dnešní	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
Desatera	desatero	k1gNnSc2
<g/>
,	,	kIx,
přijal	přijmout	k5eAaPmAgMnS
Mojžíš	Mojžíš	k1gMnSc1
na	na	k7c6
Sinaji	Sinaj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nP
jak	jak	k6eAd1
ortodoxní	ortodoxní	k2eAgMnPc1d1
Židé	Žid	k1gMnPc1
<g/>
,	,	kIx,
Talmud	talmud	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
tak	tak	k6eAd1
i	i	k9
fundamentalističtí	fundamentalistický	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Rembrandt	Rembrandt	k1gMnSc1
van	vana	k1gFnPc2
Rijn	Rijn	k1gMnSc1
<g/>
,	,	kIx,
Mojžíš	Mojžíš	k1gMnSc1
s	s	k7c7
deskami	deska	k1gFnPc7
Zákona	zákon	k1gInSc2
</s>
<s>
Biblická	biblický	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
rozlišuje	rozlišovat	k5eAaImIp3nS
v	v	k7c6
Bibli	bible	k1gFnSc6
různé	různý	k2eAgFnSc2d1
časové	časový	k2eAgFnSc2d1
a	a	k8xC
tradiční	tradiční	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
připouští	připouštět	k5eAaImIp3nS
postupné	postupný	k2eAgNnSc1d1
a	a	k8xC
mozaikovité	mozaikovitý	k2eAgNnSc1d1
vznikání	vznikání	k1gNnSc1
Bible	bible	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Desatera	desatero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
něj	on	k3xPp3gInSc2
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
velmi	velmi	k6eAd1
stručná	stručný	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
u	u	k7c2
všech	všecek	k3xTgNnPc2
přikázání	přikázání	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
u	u	k7c2
„	„	k?
<g/>
Nezabiješ	zabít	k5eNaPmIp2nS
<g/>
,	,	kIx,
nesesmilníš	sesmilnět	k5eNaImIp2nS,k5eNaPmIp2nS
<g/>
,	,	kIx,
nepokradeš	pokrást	k5eNaPmIp2nS
<g/>
“	“	k?
(	(	kIx(
<g/>
G.	G.	kA
v.	v.	k?
Rad	rad	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdějších	pozdní	k2eAgInPc6d2
komentářích	komentář	k1gInPc6
a	a	k8xC
výkladech	výklad	k1gInPc6
se	se	k3xPyFc4
obě	dva	k4xCgFnPc1
verze	verze	k1gFnPc1
liší	lišit	k5eAaImIp3nP
<g/>
,	,	kIx,
bývají	bývat	k5eAaImIp3nP
tudíž	tudíž	k8xC
považovány	považován	k2eAgInPc1d1
za	za	k7c4
pozdější	pozdní	k2eAgInPc4d2
dodatky	dodatek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
L.	L.	kA
Hoßfeld	Hoßfeld	k1gInSc4
a	a	k8xC
W.	W.	kA
Johnstone	Johnston	k1gInSc5
jsou	být	k5eAaImIp3nP
toho	ten	k3xDgInSc2
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
celé	celý	k2eAgNnSc1d1
Desatero	desatero	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
babylonském	babylonský	k2eAgNnSc6d1
zajetí	zajetí	k1gNnSc6
či	či	k8xC
po	po	k7c6
něm	on	k3xPp3gInSc6
(	(	kIx(
<g/>
asi	asi	k9
6	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
<g/>
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobný	pravděpodobný	k2eAgInSc4d1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vliv	vliv	k1gInSc4
Egyptské	egyptský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
jejíž	jejíž	k3xOyRp3gFnSc6
125	#num#	k4
<g/>
.	.	kIx.
kapitole	kapitola	k1gFnSc6
<g/>
,	,	kIx,
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
soudu	soud	k1gInSc3
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
můžeme	moct	k5eAaImIp1nP
najít	najít	k5eAaPmF
v	v	k7c6
mnoha	mnoho	k4c6
jiných	jiný	k2eAgFnPc6d1
bodech	bod	k1gInPc6
několik	několik	k4yIc4
podobných	podobný	k2eAgFnPc6d1
těm	ten	k3xDgInPc3
z	z	k7c2
Desatera	desatero	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Egyptská	egyptský	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
nepochybně	pochybně	k6eNd1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
starší	starý	k2eAgFnSc1d2
než	než	k8xS
samotná	samotný	k2eAgFnSc1d1
Bible	bible	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
H.	H.	kA
Greßmanna	Greßmanna	k1gFnSc1
a	a	k8xC
E.	E.	kA
Jenniho	Jenni	k1gMnSc2
pochází	pocházet	k5eAaImIp3nS
jádro	jádro	k1gNnSc4
už	už	k9
z	z	k7c2
Mojžíšovy	Mojžíšův	k2eAgFnSc2d1
doby	doba	k1gFnSc2
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
L.	L.	kA
Perlitta	Perlitta	k1gFnSc1
z	z	k7c2
konce	konec	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
J.	J.	kA
Wellhausen	Wellhausen	k1gInSc1
jej	on	k3xPp3gMnSc4
datuje	datovat	k5eAaImIp3nS
8	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
atd.	atd.	kA
Vodítkem	vodítko	k1gNnSc7
je	být	k5eAaImIp3nS
především	především	k9
předpoklad	předpoklad	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
osloveným	oslovený	k2eAgMnSc7d1
je	být	k5eAaImIp3nS
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
svobodný	svobodný	k2eAgMnSc1d1
rolník	rolník	k1gMnSc1
<g/>
,	,	kIx,
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
starých	starý	k2eAgMnPc2d1
rodičů	rodič	k1gMnPc2
<g/>
,	,	kIx,
majitel	majitel	k1gMnSc1
statku	statek	k1gInSc2
a	a	k8xC
otroků	otrok	k1gMnPc2
-	-	kIx~
sociální	sociální	k2eAgInSc4d1
status	status	k1gInSc4
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
(	(	kIx(
<g/>
kočovní	kočovní	k2eAgFnSc1d1
<g/>
)	)	kIx)
Židé	Žid	k1gMnPc1
vypracovali	vypracovat	k5eAaPmAgMnP
až	až	k9
v	v	k7c6
pozdějších	pozdní	k2eAgFnPc6d2
dobách	doba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důrazech	důraz	k1gInPc6
na	na	k7c4
sobotu	sobota	k1gFnSc4
je	být	k5eAaImIp3nS
zase	zase	k9
spatřován	spatřován	k2eAgInSc4d1
vliv	vliv	k1gInSc4
náboženského	náboženský	k2eAgInSc2d1
života	život	k1gInSc2
v	v	k7c6
době	doba	k1gFnSc6
chrámů	chrám	k1gInPc2
a	a	k8xC
po	po	k7c6
babylonském	babylonský	k2eAgNnSc6d1
zajetí	zajetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důrazu	důraz	k1gInSc6
na	na	k7c4
jedinost	jedinost	k1gFnSc4
Boží	božit	k5eAaImIp3nS
a	a	k8xC
v	v	k7c6
zákazu	zákaz	k1gInSc6
zobrazování	zobrazování	k1gNnSc2
Boha	bůh	k1gMnSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
spatřována	spatřován	k2eAgFnSc1d1
polemika	polemika	k1gFnSc1
proti	proti	k7c3
babylonskému	babylonský	k2eAgNnSc3d1
pohanství	pohanství	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
pro	pro	k7c4
exulanty	exulant	k1gMnPc4
svůdné	svůdný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vedle	vedle	k7c2
etického	etický	k2eAgNnSc2d1
desatera	desatero	k1gNnSc2
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
dodržováno	dodržovat	k5eAaImNgNnS
celkem	celkem	k6eAd1
odlišné	odlišný	k2eAgNnSc1d1
rituální	rituální	k2eAgNnSc1d1
desatero	desatero	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Srovnatelná	srovnatelný	k2eAgNnPc1d1
etická	etický	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
</s>
<s>
Ve	v	k7c6
všech	všecek	k3xTgNnPc6
abrahámovských	abrahámovský	k2eAgNnPc6d1
náboženstvích	náboženství	k1gNnPc6
mají	mít	k5eAaImIp3nP
zásadní	zásadní	k2eAgInSc4d1
význam	význam	k1gInSc4
první	první	k4xOgFnSc2
dvě	dva	k4xCgFnPc4
resp.	resp.	kA
tři	tři	k4xCgFnPc4
přikázání	přikázání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladou	klást	k5eAaImIp3nP
totiž	totiž	k9
jako	jako	k9
ústřední	ústřední	k2eAgInSc1d1
bod	bod	k1gInSc1
víry	víra	k1gFnSc2
jednoho	jeden	k4xCgMnSc2
Boha	bůh	k1gMnSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
Abrahám	Abrahám	k1gMnSc1
uzavřel	uzavřít	k5eAaPmAgInS
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
neabrahámovská	abrahámovský	k2eNgNnPc1d1
náboženství	náboženství	k1gNnPc1
nemají	mít	k5eNaImIp3nP
proto	proto	k8xC
tato	tento	k3xDgNnPc1
dvě	dva	k4xCgFnPc4
(	(	kIx(
<g/>
resp.	resp.	kA
tři	tři	k4xCgFnPc4
<g/>
)	)	kIx)
přikázání	přikázání	k1gNnSc4
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
i	i	k9
s	s	k7c7
příkazem	příkaz	k1gInSc7
svátečního	sváteční	k2eAgInSc2d1
odpočinku	odpočinek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k1gNnSc1
přikázání	přikázání	k1gNnPc2
však	však	k9
kladou	klást	k5eAaImIp3nP
základ	základ	k1gInSc1
mezilidských	mezilidský	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
lze	lze	k6eAd1
je	být	k5eAaImIp3nS
proto	proto	k8xC
nalézt	nalézt	k5eAaPmF,k5eAaBmF
i	i	k9
v	v	k7c6
ostatních	ostatní	k2eAgNnPc6d1
náboženstvích	náboženství	k1gNnPc6
a	a	k8xC
kulturách	kultura	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Islám	islám	k1gInSc1
</s>
<s>
Korán	korán	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
súře	súra	k1gFnSc6
nazvané	nazvaný	k2eAgFnSc6d1
Noční	noční	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
předání	předání	k1gNnSc1
deseti	deset	k4xCc2
přikázání	přikázání	k1gNnPc2
Mojžíšovi	Mojžíš	k1gMnSc3
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
A	a	k8xC
darovali	darovat	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
Mojžíšovi	Mojžíš	k1gMnSc3
Písmo	písmo	k1gNnSc4
a	a	k8xC
učinili	učinit	k5eAaPmAgMnP,k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
je	on	k3xPp3gNnSc4
pro	pro	k7c4
dítka	dítko	k1gNnPc4
Izraele	Izrael	k1gInSc2
vedením	vedení	k1gNnSc7
řkouce	říct	k5eAaPmSgFnP
<g/>
:	:	kIx,
„	„	k?
<g/>
Nevezmete	vzít	k5eNaPmIp2nP
sobě	se	k3xPyFc3
vedle	vedle	k7c2
Mne	já	k3xPp1nSc4
nikoho	nikdo	k3yNnSc2
jiného	jiný	k2eAgMnSc2d1
za	za	k7c4
ochránce	ochránce	k1gMnSc4
<g/>
,	,	kIx,
ó	ó	k0
potomci	potomek	k1gMnPc1
těch	ten	k3xDgInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
nesli	nést	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
v	v	k7c6
arše	archa	k1gFnSc6
s	s	k7c7
Noem	Noe	k1gNnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
služebníkem	služebník	k1gMnSc7
byl	být	k5eAaImAgInS
Naším	náš	k3xOp1gNnSc7
vděčným	vděčný	k2eAgMnSc7d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Súra	súra	k1gFnSc1
17	#num#	k4
<g/>
,	,	kIx,
ája	ája	k?
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Desatero	desatero	k1gNnSc1
však	však	k9
v	v	k7c6
Koránu	korán	k1gInSc6
samotném	samotný	k2eAgInSc6d1
nehraje	hrát	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
obsah	obsah	k1gInSc1
lze	lze	k6eAd1
však	však	k9
přitom	přitom	k6eAd1
v	v	k7c6
textu	text	k1gInSc6
Koránu	korán	k1gInSc2
nalézt	nalézt	k5eAaPmF,k5eAaBmF
<g/>
;	;	kIx,
samotná	samotný	k2eAgFnSc1d1
17	#num#	k4
<g/>
.	.	kIx.
súra	súra	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
několik	několik	k4yIc1
z	z	k7c2
příkazů	příkaz	k1gInPc2
obsažených	obsažený	k2eAgInPc2d1
v	v	k7c6
Desateru	desatero	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkaz	příkaz	k1gInSc1
víry	víra	k1gFnSc2
v	v	k7c6
jednoho	jeden	k4xCgMnSc2
Boha	bůh	k1gMnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c4
vícero	vícero	k1gNnSc4
místech	místo	k1gNnPc6
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
Koránu	korán	k1gInSc6
se	se	k3xPyFc4
však	však	k9
nenachází	nacházet	k5eNaImIp3nS
explicitně	explicitně	k6eAd1
zákaz	zákaz	k1gInSc1
zobrazování	zobrazování	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
vyvozuje	vyvozovat	k5eAaImIp3nS
z	z	k7c2
často	často	k6eAd1
opakovaného	opakovaný	k2eAgInSc2d1
zákazu	zákaz	k1gInSc2
uctívání	uctívání	k1gNnSc2
obrazů	obraz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jména	jméno	k1gNnPc4
Božího	boží	k2eAgNnSc2d1
se	se	k3xPyFc4
dle	dle	k7c2
Koránu	korán	k1gInSc2
nesmí	smět	k5eNaImIp3nS
zneužívat	zneužívat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Pátek	pátek	k1gInSc1
není	být	k5eNaImIp3nS
dnem	den	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
nemá	mít	k5eNaImIp3nS
pracovat	pracovat	k5eAaImF
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
dnem	den	k1gInSc7
modlitby	modlitba	k1gFnSc2
(	(	kIx(
<g/>
62,9	62,9	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
děti	dítě	k1gFnPc1
mají	mít	k5eAaImIp3nP
ctít	ctít	k5eAaImF
své	svůj	k3xOyFgMnPc4
rodiče	rodič	k1gMnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
ti	ten	k3xDgMnPc1
jsou	být	k5eAaImIp3nP
ovšem	ovšem	k9
také	také	k9
zodpovědní	zodpovědný	k2eAgMnPc1d1
za	za	k7c4
své	svůj	k3xOyFgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupen	k2eAgInPc1d1
i	i	k8xC
ostatní	ostatní	k2eAgInPc1d1
příkazy	příkaz	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ex	ex	k6eAd1
34	#num#	k4
<g/>
,	,	kIx,
28	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Ex	ex	k6eAd1
20	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Dt	Dt	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
21	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Mk	Mk	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
1	#num#	k4
<g/>
Tm	Tm	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
OPATRNÝ	opatrný	k2eAgInSc1d1
<g/>
,	,	kIx,
Dominik	Dominik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pastorální	pastorální	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
biblických	biblický	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Mt	Mt	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
37	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Ex	ex	k6eAd1
20	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dt	Dt	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
OPATRNÝ	opatrný	k2eAgInSc1d1
<g/>
,	,	kIx,
Dominik	Dominik	k1gMnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
V	v	k7c4
jednoho	jeden	k4xCgMnSc4
Boha	bůh	k1gMnSc4
věřiti	věřit	k5eAaImF
budeš	být	k5eAaImBp2nS
<g/>
“	“	k?
Anonymní	anonymní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
Kanisiových	Kanisiový	k2eAgInPc2d1
katechismů	katechismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnSc2
theologica	theologicum	k1gNnSc2
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
76	#num#	k4
<g/>
-	-	kIx~
<g/>
78	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8570	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
OPATRNÝ	opatrný	k2eAgInSc1d1
<g/>
,	,	kIx,
Dominik	Dominik	k1gMnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
V	v	k7c4
jednoho	jeden	k4xCgMnSc4
Boha	bůh	k1gMnSc4
věřiti	věřit	k5eAaImF
budeš	být	k5eAaImBp2nS
<g/>
“	“	k?
Anonymní	anonymní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
Kanisiových	Kanisiový	k2eAgInPc2d1
katechismů	katechismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnSc2
theologica	theologicum	k1gNnSc2
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
81	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8570	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Pesachim	Pesachim	k1gInSc1
6	#num#	k4
<g/>
b	b	k?
<g/>
;	;	kIx,
Sanhedrin	sanhedrin	k1gInSc1
90	#num#	k4
<g/>
a	a	k8xC
<g/>
↑	↑	k?
O	o	k7c4
125	#num#	k4
<g/>
.	.	kIx.
kapitole	kapitola	k1gFnSc6
<g/>
.	.	kIx.
egypt-egypt	egypt-egypt	k1gInSc1
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
http://www.jitrnizeme.cz/view.php?cisloclanku=2003122501	http://www.jitrnizeme.cz/view.php?cisloclanku=2003122501	k4
<g/>
↑	↑	k?
Ex	ex	k6eAd1
34	#num#	k4
<g/>
,	,	kIx,
14	#num#	k4
<g/>
–	–	k?
<g/>
26	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
http://www.osel.cz/8315-zeptejte-se-sveho-kneze-7-desatero-bozich-prikazani.html	http://www.osel.cz/8315-zeptejte-se-sveho-kneze-7-desatero-bozich-prikazani.html	k1gInSc1
-	-	kIx~
Zeptejte	zeptat	k5eAaPmRp2nP
se	se	k3xPyFc4
svého	svůj	k3xOyFgMnSc2
kněze	kněz	k1gMnSc2
7	#num#	k4
<g/>
:	:	kIx,
Desatero	desatero	k1gNnSc1
Božích	boží	k2eAgNnPc2d1
přikázání	přikázání	k1gNnPc2
<g/>
↑	↑	k?
Súra	súra	k1gFnSc1
3,89	3,89	k4
<g/>
;	;	kIx,
7,138	7,138	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
17,22	17,22	k4
<g/>
;	;	kIx,
39,1	39,1	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
;	;	kIx,
112,2	112,2	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Súra	súra	k1gFnSc1
11,18	11,18	k4
<g/>
;	;	kIx,
39,32	39,32	k4
<g/>
;	;	kIx,
39,60	39,60	k4
<g/>
↑	↑	k?
Súra	súra	k1gFnSc1
6,151	6,151	k4
<g/>
,	,	kIx,
17,23	17,23	k4
<g/>
,	,	kIx,
29,8	29,8	k4
<g/>
,31	,31	k4
<g/>
,14	,14	k4
<g/>
,	,	kIx,
46,15	46,15	k4
<g/>
↑	↑	k?
Súra	súra	k1gFnSc1
6,15	6,15	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BARTON	BARTON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etika	etika	k1gFnSc1
a	a	k8xC
Starý	starý	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihlava	Jihlava	k1gFnSc1
<g/>
:	:	kIx,
Mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86498	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HELLER	HELLER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Desatero	desatero	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc4
a	a	k8xC
výklad	výklad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Zákon	zákon	k1gInSc1
a	a	k8xC
proroci	prorok	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kalich	kalich	k1gInSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LOCHMAN	Lochman	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Milíč	Milíč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Desatero	desatero	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrovky	směrovka	k1gFnPc4
ke	k	k7c3
svobodě	svoboda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kalich	kalich	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7017	#num#	k4
<g/>
-	-	kIx~
<g/>
828	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MAYER	Mayer	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Netradiční	tradiční	k2eNgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
Desatero	desatero	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maskil	Maskil	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červen	červen	k1gInSc1
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
PÖHLMANN	PÖHLMANN	kA
<g/>
,	,	kIx,
Horst	Horst	k1gMnSc1
Georg	Georg	k1gInSc1
<g/>
;	;	kIx,
STERN	sternum	k1gNnPc2
<g/>
,	,	kIx,
Marc	Marc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Desatero	desatero	k1gNnSc1
v	v	k7c6
životě	život	k1gInSc6
Židů	Žid	k1gMnPc2
a	a	k8xC
křesťanů	křesťan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dialog	dialog	k1gInSc1
ortodoxního	ortodoxní	k2eAgMnSc2d1
rabína	rabín	k1gMnSc2
a	a	k8xC
křesťanského	křesťanský	k2eAgMnSc2d1
teologa	teolog	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
783	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RÖMER	RÖMER	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skrytý	skrytý	k2eAgMnSc1d1
Bůh	bůh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sex	sex	k1gInSc1
<g/>
,	,	kIx,
krutost	krutost	k1gFnSc1
a	a	k8xC
násilí	násilí	k1gNnSc1
ve	v	k7c6
Starém	starý	k2eAgInSc6d1
zákoně	zákon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihlava	Jihlava	k1gFnSc1
<g/>
:	:	kIx,
Mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86498	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Patero	Patera	k1gMnSc5
církevních	církevní	k2eAgFnPc2d1
přikázání	přikázání	k1gNnPc2
</s>
<s>
Hřích	hřích	k1gInSc1
</s>
<s>
Morálka	morálka	k1gFnSc1
</s>
<s>
Kázání	kázání	k1gNnSc1
na	na	k7c6
hoře	hora	k1gFnSc6
</s>
<s>
Šíla	šíl	k1gMnSc4
</s>
<s>
Sedm	sedm	k4xCc1
noachidských	noachidský	k2eAgNnPc2d1
přikázání	přikázání	k1gNnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Desatero	desatero	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Desatero	desatero	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Dekalog	dekalog	k1gInSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Dílo	dílo	k1gNnSc1
Bible	bible	k1gFnSc2
kralická	kralický	k2eAgFnSc1d1
<g/>
/	/	kIx~
<g/>
Exodus	Exodus	k1gInSc1
<g/>
#	#	kIx~
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Mýty	mýtus	k1gInPc1
a	a	k8xC
skutečnost	skutečnost	k1gFnSc1
–	–	k?
Desatero	desatero	k1gNnSc1
</s>
<s>
Biblické	biblický	k2eAgFnPc1d1
archeologické	archeologický	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
</s>
<s>
Deset	deset	k4xCc1
přikázání	přikázání	k1gNnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4070267-4	4070267-4	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
66154074376911740523	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bible	bible	k1gFnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
