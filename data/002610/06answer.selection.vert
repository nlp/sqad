<s>
Klement	Klement	k1gMnSc1
Gottwald	Gottwald	k1gMnSc1
(	(	kIx(
<g/>
23	[number]	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1896	[number]	k4
Heroltice	Heroltice	k1gFnSc1
-	-	kIx~
14	[number]	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1953	[number]	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
československý	československý	k2eAgMnSc1d1
komunistický	komunistický	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
od	od	k7c2
roku	rok	k1gInSc2
1929	[number]	k4
zastával	zastávat	k5eAaImAgMnS
úřad	úřad	k1gInSc4
poslance	poslanec	k1gMnSc2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
<g/>
,	,	kIx,
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vykonával	vykonávat	k5eAaImAgMnS
nejdříve	dříve	k6eAd3
funkci	funkce	k1gFnSc4
premiéra	premiéra	k1gFnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
první	první	k4xOgFnSc6
a	a	k8xC
druhé	druhý	k4xOgFnSc3
vládě	vláda	k1gFnSc3
<g/>
.	.	kIx.
</s>