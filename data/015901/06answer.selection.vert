<s>
Keř	keř	k1gInSc1
je	být	k5eAaImIp3nS
jen	jen	k9
50	#num#	k4
cm	cm	kA
vysoký	vysoký	k2eAgMnSc1d1
<g/>
,	,	kIx,
liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
tavolníkovců	tavolníkovec	k1gMnPc2
nízkým	nízký	k2eAgInSc7d1
vzrůstem	vzrůst	k1gInSc7
<g/>
,	,	kIx,
výhony	výhon	k1gInPc1
chlupaté	chlupatý	k2eAgInPc1d1
<g/>
,	,	kIx,
červenošedé	červenošedý	k2eAgNnSc1d1
<g/>
,	,	kIx,
kůra	kůra	k1gFnSc1
později	pozdě	k6eAd2
odlupující	odlupující	k2eAgFnSc1d1
se	se	k3xPyFc4
<g/>
,	,	kIx,
listy	list	k1gInPc1
lichozpeřené	lichozpeřený	k2eAgInPc1d1
<g/>
,	,	kIx,
dlouhé	dlouhý	k2eAgInPc1d1
max	max	kA
<g/>
.	.	kIx.
18	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
květy	květ	k1gInPc7
v	v	k7c6
řidších	řídký	k2eAgInPc6d2
chocholících	chocholík	k1gInPc6
<g/>
,	,	kIx,
až	až	k9
13	#num#	k4
cm	cm	kA
velkých	velká	k1gFnPc2
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnPc1d1
<g/>
,	,	kIx,
kvete	kvést	k5eAaImIp3nS
v	v	k7c6
červenci	červenec	k1gInSc6
<g/>
.	.	kIx.
</s>