<s>
Tavolníkovec	Tavolníkovec	k1gMnSc1
velkokvětý	velkokvětý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Sorbaria	Sorbarium	k1gNnSc2
grandiflora	grandiflora	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bíle	bíle	k6eAd1
kvetoucí	kvetoucí	k2eAgInSc1d1
keř	keř	k1gInSc1
z	z	k7c2
rodu	rod	k1gInSc2
tavolníkovec	tavolníkovec	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
náleží	náležet	k5eAaImIp3nS
do	do	k7c2
čeledi	čeleď	k1gFnSc2
růžovité	růžovitý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>