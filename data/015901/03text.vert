<s>
Tavolníkovec	Tavolníkovec	k1gInSc1
velkokvětý	velkokvětý	k2eAgInSc1d1
</s>
<s>
Tavolníkovec	Tavolníkovec	k1gInSc1
velkokvětý	velkokvětý	k2eAgInSc4d1
Tavolníkovec	Tavolníkovec	k1gInSc4
velkokvětý	velkokvětý	k2eAgInSc4d1
(	(	kIx(
<g/>
Sorbaria	Sorbarium	k1gNnSc2
grandiflora	grandiflora	k1gFnSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
růžotvaré	růžotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Rosales	Rosales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
růžovité	růžovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Rosaceae	Rosacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
tavolníkovec	tavolníkovec	k1gInSc1
(	(	kIx(
<g/>
Sorbaria	Sorbarium	k1gNnSc2
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Sorbaria	Sorbarium	k1gNnSc2
grandiflora	grandiflora	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Sweet	Sweet	k1gMnSc1
<g/>
)	)	kIx)
Maxim	Maxim	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1879	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tavolníkovec	Tavolníkovec	k1gMnSc1
velkokvětý	velkokvětý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Sorbaria	Sorbarium	k1gNnSc2
grandiflora	grandiflora	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bíle	bíle	k6eAd1
kvetoucí	kvetoucí	k2eAgInSc1d1
keř	keř	k1gInSc1
z	z	k7c2
rodu	rod	k1gInSc2
tavolníkovec	tavolníkovec	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
náleží	náležet	k5eAaImIp3nS
do	do	k7c2
čeledi	čeleď	k1gFnSc2
růžovité	růžovitý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Keř	keř	k1gInSc1
je	být	k5eAaImIp3nS
jen	jen	k9
50	#num#	k4
cm	cm	kA
vysoký	vysoký	k2eAgMnSc1d1
<g/>
,	,	kIx,
liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
tavolníkovců	tavolníkovec	k1gMnPc2
nízkým	nízký	k2eAgInSc7d1
vzrůstem	vzrůst	k1gInSc7
<g/>
,	,	kIx,
výhony	výhon	k1gInPc1
chlupaté	chlupatý	k2eAgInPc1d1
<g/>
,	,	kIx,
červenošedé	červenošedý	k2eAgNnSc1d1
<g/>
,	,	kIx,
kůra	kůra	k1gFnSc1
později	pozdě	k6eAd2
odlupující	odlupující	k2eAgFnSc1d1
se	se	k3xPyFc4
<g/>
,	,	kIx,
listy	list	k1gInPc1
lichozpeřené	lichozpeřený	k2eAgInPc1d1
<g/>
,	,	kIx,
dlouhé	dlouhý	k2eAgInPc1d1
max	max	kA
<g/>
.	.	kIx.
18	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
květy	květ	k1gInPc7
v	v	k7c6
řidších	řídký	k2eAgInPc6d2
chocholících	chocholík	k1gInPc6
<g/>
,	,	kIx,
až	až	k9
13	#num#	k4
cm	cm	kA
velkých	velká	k1gFnPc2
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnPc1d1
<g/>
,	,	kIx,
kvete	kvést	k5eAaImIp3nS
v	v	k7c6
červenci	červenec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Taxon	taxon	k1gInSc1
Sorbaria	Sorbarium	k1gNnSc2
grandiflora	grandiflora	k1gFnSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
