<s>
Jens	Jens	k6eAd1
Ludwig	Ludwig	k1gMnSc1
</s>
<s>
Jens	Jens	k6eAd1
Ludwig	Ludwig	k1gMnSc1
Jens	Jensa	k1gFnPc2
Ludwig	Ludwig	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Jens	Jens	k6eAd1
Ludwig	Ludwig	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
43	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Fulda	Fulda	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Žánry	žánr	k1gInPc7
</s>
<s>
Heavy	Heava	k1gFnPc1
metal	metal	k1gInSc1
<g/>
,	,	kIx,
Power	Power	k1gInSc1
metal	metat	k5eAaImAgInS
Povolání	povolání	k1gNnSc4
</s>
<s>
muzikant	muzikant	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
kytara	kytara	k1gFnSc1
Aktivní	aktivní	k2eAgFnSc1d1
roky	rok	k1gInPc4
</s>
<s>
1992	#num#	k4
–	–	k?
současnost	současnost	k1gFnSc1
Příbuzná	příbuzná	k1gFnSc1
témata	téma	k1gNnPc4
</s>
<s>
EdguyAvantasia	EdguyAvantasia	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jens	Jens	k1gInSc1
Ludwig	Ludwig	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1977	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
německý	německý	k2eAgMnSc1d1
kytarista	kytarista	k1gMnSc1
a	a	k8xC
spoluzakladatel	spoluzakladatel	k1gMnSc1
německé	německý	k2eAgFnSc2d1
power	power	k1gInSc4
metalové	metalový	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
Edguy	Edguy	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapelu	kapela	k1gFnSc4
založil	založit	k5eAaPmAgMnS
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
spolužáky	spolužák	k1gMnPc7
Tobiasem	Tobias	k1gMnSc7
Sammetem	Sammet	k1gMnSc7
<g/>
,	,	kIx,
Dirkem	Direk	k1gMnSc7
Sauerem	Sauer	k1gMnSc7
a	a	k8xC
Dominikem	Dominik	k1gMnSc7
Storchem	Storch	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
pouhých	pouhý	k2eAgNnPc2d1
čtrnáct	čtrnáct	k4xCc1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
kapele	kapela	k1gFnSc6
hraje	hrát	k5eAaImIp3nS
na	na	k7c4
sólovou	sólový	k2eAgFnSc4d1
kytaru	kytara	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílel	podílet	k5eAaImAgMnS
se	se	k3xPyFc4
také	také	k9
na	na	k7c6
prvních	první	k4xOgNnPc6
dvou	dva	k4xCgNnPc6
albech	album	k1gNnPc6
Sammetovy	Sammetův	k2eAgFnSc2d1
opery	opera	k1gFnSc2
Avantasia	Avantasia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Edguy	Edgua	k1gFnPc1
</s>
<s>
Evil	Evil	k1gMnSc1
Minded	Minded	k1gMnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Children	Childrno	k1gNnPc2
of	of	k?
Steel	Steel	k1gMnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Savage	Savage	k1gInSc1
Poetry	Poetr	k1gInPc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kingdom	Kingdom	k1gInSc1
of	of	k?
Madness	Madness	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vain	Vain	k1gMnSc1
Glory	Glora	k1gFnSc2
Opera	opera	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Theater	Theater	k1gInSc1
of	of	k?
Salvation	Salvation	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Savage	Savage	k1gFnSc1
Poetry	Poetr	k1gInPc4
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mandrake	Mandrake	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Burning	Burning	k1gInSc1
Down	Down	k1gInSc1
the	the	k?
Opera	opera	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
King	King	k1gMnSc1
of	of	k?
Fools	Fools	k1gInSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hellfire	Hellfir	k1gMnSc5
Club	club	k1gInSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hall	Hall	k1gMnSc1
of	of	k?
Flames	Flames	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Superheroes	Superheroes	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rocket	Rocket	k1gInSc1
Ride	Rid	k1gFnSc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tinnitus	Tinnitus	k1gMnSc1
Sanctus	Sanctus	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Singles	Singles	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fucking	Fucking	k1gInSc1
with	with	k1gInSc1
F	F	kA
<g/>
***	***	k?
<g/>
:	:	kIx,
Live	Live	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Age	Age	k?
of	of	k?
the	the	k?
Joker	Joker	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Space	Space	k1gFnSc1
Police	police	k1gFnSc2
<g/>
:	:	kIx,
Defenders	Defenders	k1gInSc1
of	of	k?
the	the	k?
Crown	Crown	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Avantasia	Avantasia	k1gFnSc1
</s>
<s>
The	The	k?
Metal	metal	k1gInSc1
Opera	opera	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Metal	metal	k1gInSc1
Opera	opera	k1gFnSc1
Part	part	k1gInSc1
II	II	kA
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Edguy	Edgu	k2eAgFnPc4d1
Biography	Biographa	k1gFnPc4
<g/>
,	,	kIx,
metalstorm	metalstorm	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
Edguy	Edgua	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Edguy	Edgua	k1gMnSc2
Tobias	Tobias	k1gMnSc1
Sammet	Sammet	k1gMnSc1
•	•	k?
Jens	Jens	k1gInSc1
Ludwig	Ludwig	k1gMnSc1
•	•	k?
Dirk	Dirk	k1gMnSc1
Sauer	Sauer	k1gMnSc1
•	•	k?
Tobias	Tobias	k1gMnSc1
Exxel	Exxel	k1gMnSc1
•	•	k?
Felix	Felix	k1gMnSc1
BohnkeDominik	BohnkeDominik	k1gMnSc1
Storch	Storch	k1gInSc4
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Kingdom	Kingdom	k1gInSc1
of	of	k?
Madness	Madness	k1gInSc1
•	•	k?
Vain	Vain	k1gInSc4
Glory	Glora	k1gFnSc2
Opera	opera	k1gFnSc1
•	•	k?
Theater	Theater	k1gInSc1
of	of	k?
Salvation	Salvation	k1gInSc1
•	•	k?
The	The	k1gMnPc4
Savage	Savag	k1gFnSc2
Poetry	Poetr	k1gInPc4
•	•	k?
Mandrake	Mandrak	k1gFnSc2
•	•	k?
Hellfire	Hellfir	k1gInSc5
Club	club	k1gInSc4
•	•	k?
Rocket	Rocket	k1gInSc1
Ride	Rid	k1gFnSc2
•	•	k?
Tinnitus	Tinnitus	k1gMnSc1
Sanctus	Sanctus	k1gMnSc1
•	•	k?
Age	Age	k1gMnSc1
of	of	k?
the	the	k?
Joker	Joker	k1gMnSc1
•	•	k?
Space	Space	k1gMnSc1
Police	police	k1gFnSc2
<g/>
:	:	kIx,
Defenders	Defenders	k1gInSc1
of	of	k?
the	the	k?
Crown	Crown	k1gInSc1
•	•	k?
Monuments	Monuments	k1gInSc4
Koncertní	koncertní	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Burning	Burning	k1gInSc1
Down	Down	k1gInSc1
the	the	k?
Opera	opera	k1gFnSc1
•	•	k?
Fucking	Fucking	k1gInSc1
With	With	k1gMnSc1
F	F	kA
<g/>
***	***	k?
<g/>
:	:	kIx,
Live	Live	k1gInSc1
Kompilace	kompilace	k1gFnSc2
</s>
<s>
Hall	Hall	k1gMnSc1
of	of	k?
Flames	Flames	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Singles	Singles	k1gMnSc1
•	•	k?
Monuments	Monuments	k1gInSc1
EP	EP	kA
</s>
<s>
King	King	k1gMnSc1
of	of	k?
Fools	Fools	k1gInSc1
•	•	k?
Superheroes	Superheroes	k1gInSc1
DVD	DVD	kA
</s>
<s>
Superheroes	Superheroes	k1gInSc1
•	•	k?
Fucking	Fucking	k1gInSc1
With	With	k1gMnSc1
F	F	kA
<g/>
***	***	k?
<g/>
:	:	kIx,
Live	Live	k1gInSc1
•	•	k?
Monuments	Monuments	k1gInSc1
Dema	Dema	k1gFnSc1
</s>
<s>
Evil	Evil	k1gMnSc1
Minded	Minded	k1gMnSc1
•	•	k?
Children	Childrno	k1gNnPc2
of	of	k?
Steel	Steel	k1gMnSc1
•	•	k?
Savage	Savag	k1gFnSc2
Poetry	Poetr	k1gInPc1
Příbuzná	příbuzný	k2eAgFnSc1d1
témata	téma	k1gNnPc4
</s>
<s>
Diskografie	diskografie	k1gFnSc1
Edguy	Edgua	k1gMnSc2
•	•	k?
Avantasia	Avantasius	k1gMnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
203380	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
29146522343432390915	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
