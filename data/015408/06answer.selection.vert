<s desamb="1">
Kapelu	kapela	k1gFnSc4
založil	založit	k5eAaPmAgMnS
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
spolužáky	spolužák	k1gMnPc7
Tobiasem	Tobias	k1gMnSc7
Sammetem	Sammet	k1gMnSc7
<g/>
,	,	kIx,
Dirkem	Direk	k1gMnSc7
Sauerem	Sauer	k1gMnSc7
a	a	k8xC
Dominikem	Dominik	k1gMnSc7
Storchem	Storch	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
pouhých	pouhý	k2eAgNnPc2d1
čtrnáct	čtrnáct	k4xCc1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
kapele	kapela	k1gFnSc6
hraje	hrát	k5eAaImIp3nS
na	na	k7c4
sólovou	sólový	k2eAgFnSc4d1
kytaru	kytara	k1gFnSc4
<g/>
.	.	kIx.
</s>