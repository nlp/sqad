<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
vzpomínán	vzpomínán	k2eAgInSc1d1	vzpomínán
především	především	k9	především
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
přínos	přínos	k1gInSc4	přínos
vědě	věda	k1gFnSc3	věda
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
svých	svůj	k3xOyFgInPc2	svůj
textů	text	k1gInPc2	text
věnoval	věnovat	k5eAaPmAgMnS	věnovat
výkladům	výklad	k1gInPc3	výklad
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
