<s>
Solid-state	Solidtat	k1gMnSc5	Solid-stat
drive	drive	k1gInSc1	drive
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SSD	SSD	kA	SSD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informačních	informační	k2eAgFnPc6d1	informační
technologiích	technologie	k1gFnPc6	technologie
typ	typ	k1gInSc4	typ
datového	datový	k2eAgNnSc2d1	datové
média	médium	k1gNnSc2	médium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
magnetických	magnetický	k2eAgInPc2d1	magnetický
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
pohyblivé	pohyblivý	k2eAgFnSc3d1	pohyblivá
mechanické	mechanický	k2eAgFnSc3d1	mechanická
části	část	k1gFnSc3	část
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc4d2	nižší
spotřebu	spotřeba	k1gFnSc4	spotřeba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
SSD	SSD	kA	SSD
emuluje	emulovat	k5eAaImIp3nS	emulovat
(	(	kIx(	(
<g/>
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
<g/>
)	)	kIx)	)
rozhraní	rozhraní	k1gNnSc4	rozhraní
používané	používaný	k2eAgNnSc4d1	používané
pro	pro	k7c4	pro
pevné	pevný	k2eAgInPc4d1	pevný
disky	disk	k1gInPc4	disk
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
SATA	SATA	kA	SATA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
mohl	moct	k5eAaImAgInS	moct
snadno	snadno	k6eAd1	snadno
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
použita	použit	k2eAgFnSc1d1	použita
nevolatilní	volatilní	k2eNgFnSc1d1	volatilní
flash	flash	k1gMnSc1	flash
paměť	paměť	k1gFnSc1	paměť
<g/>
.	.	kIx.	.
</s>
<s>
SSD	SSD	kA	SSD
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
nevolatilní	volatilní	k2eNgFnSc4d1	volatilní
paměť	paměť	k1gFnSc4	paměť
typu	typ	k1gInSc2	typ
SRAM	SRAM	kA	SRAM
nebo	nebo	k8xC	nebo
DRAM	DRAM	kA	DRAM
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazývána	nazýván	k2eAgFnSc1d1	nazývána
RAM-drive	RAMriev	k1gFnPc1	RAM-driev
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
SSD	SSD	kA	SSD
jednotka	jednotka	k1gFnSc1	jednotka
byla	být	k5eAaImAgFnS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trvalo	trvat	k5eAaImAgNnS	trvat
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
takovou	takový	k3xDgFnSc4	takový
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
masivní	masivní	k2eAgNnSc1d1	masivní
rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvopočátku	prvopočátek	k1gInSc6	prvopočátek
byla	být	k5eAaImAgFnS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zaplatit	zaplatit	k5eAaPmF	zaplatit
takto	takto	k6eAd1	takto
vysokou	vysoký	k2eAgFnSc4d1	vysoká
cenu	cena	k1gFnSc4	cena
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
nejmodernějších	moderní	k2eAgFnPc2d3	nejmodernější
technologií	technologie	k1gFnPc2	technologie
pouze	pouze	k6eAd1	pouze
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zákazníkem	zákazník	k1gMnSc7	zákazník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
vyrobit	vyrobit	k5eAaPmF	vyrobit
jednotku	jednotka	k1gFnSc4	jednotka
fungující	fungující	k2eAgFnSc1d1	fungující
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kritické	kritický	k2eAgFnSc2d1	kritická
situace	situace	k1gFnSc2	situace
jednoduše	jednoduše	k6eAd1	jednoduše
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
data	datum	k1gNnPc4	datum
na	na	k7c6	na
datovém	datový	k2eAgInSc6d1	datový
nosiči	nosič	k1gInSc6	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
vlastnosti	vlastnost	k1gFnPc4	vlastnost
elektronická	elektronický	k2eAgFnSc1d1	elektronická
jednotka	jednotka	k1gFnSc1	jednotka
splňovala	splňovat	k5eAaImAgFnS	splňovat
<g/>
.	.	kIx.	.
</s>
<s>
SSD	SSD	kA	SSD
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
postupně	postupně	k6eAd1	postupně
nahradí	nahradit	k5eAaPmIp3nS	nahradit
pevné	pevný	k2eAgInPc4d1	pevný
disky	disk	k1gInPc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
stejné	stejný	k2eAgNnSc4d1	stejné
rozhraní	rozhraní	k1gNnSc4	rozhraní
SATA	SATA	kA	SATA
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnPc4d2	vyšší
přenosové	přenosový	k2eAgFnPc4d1	přenosová
rychlosti	rychlost	k1gFnPc4	rychlost
PCI	PCI	kA	PCI
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
ATA	ATA	kA	ATA
v	v	k7c6	v
rozhraní	rozhraní	k1gNnSc6	rozhraní
PCMCIA	PCMCIA	kA	PCMCIA
<g/>
,	,	kIx,	,
ExpressCard	ExpressCard	k1gInSc4	ExpressCard
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
stejný	stejný	k2eAgInSc4d1	stejný
konektor	konektor	k1gInSc4	konektor
i	i	k8xC	i
typ	typ	k1gInSc4	typ
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
SSD	SSD	kA	SSD
nemají	mít	k5eNaImIp3nP	mít
mechanické	mechanický	k2eAgFnPc1d1	mechanická
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
nižší	nízký	k2eAgFnSc4d2	nižší
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgInSc4d2	nižší
čas	čas	k1gInSc4	čas
na	na	k7c6	na
alokaci	alokace	k1gFnSc6	alokace
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
u	u	k7c2	u
klasických	klasický	k2eAgInPc2d1	klasický
disků	disk	k1gInPc2	disk
spotřebovaný	spotřebovaný	k2eAgInSc4d1	spotřebovaný
na	na	k7c6	na
přesunutí	přesunutí	k1gNnSc6	přesunutí
čtecích	čtecí	k2eAgFnPc2d1	čtecí
<g/>
/	/	kIx~	/
<g/>
zápisových	zápisový	k2eAgFnPc2d1	zápisová
hlaviček	hlavička	k1gFnPc2	hlavička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vyšších	vysoký	k2eAgFnPc2d2	vyšší
přenosových	přenosový	k2eAgFnPc2d1	přenosová
rychlostí	rychlost	k1gFnPc2	rychlost
a	a	k8xC	a
nevydávají	vydávat	k5eNaImIp3nP	vydávat
hluk	hluk	k1gInSc4	hluk
atd.	atd.	kA	atd.
Taktéž	Taktéž	k?	Taktéž
<g />
.	.	kIx.	.
</s>
<s>
jsou	být	k5eAaImIp3nP	být
znatelně	znatelně	k6eAd1	znatelně
lehčí	lehký	k2eAgFnPc1d2	lehčí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
spotřebou	spotřeba	k1gFnSc7	spotřeba
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
2	[number]	k4	2
Watty	watt	k1gInPc4	watt
při	při	k7c6	při
plném	plný	k2eAgInSc6d1	plný
provozu	provoz	k1gInSc6	provoz
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
ve	v	k7c4	v
standby	standb	k1gInPc4	standb
režimu	režim	k1gInSc2	režim
<g/>
)	)	kIx)	)
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
do	do	k7c2	do
notebooků	notebook	k1gInPc2	notebook
<g/>
,	,	kIx,	,
netbooků	netbook	k1gInPc2	netbook
<g/>
,	,	kIx,	,
PDA	PDA	kA	PDA
a	a	k8xC	a
podobných	podobný	k2eAgNnPc2d1	podobné
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spotřeba	spotřeba	k1gFnSc1	spotřeba
hraje	hrát	k5eAaImIp3nS	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
náchylné	náchylný	k2eAgInPc4d1	náchylný
na	na	k7c4	na
nárazy	náraz	k1gInPc4	náraz
a	a	k8xC	a
otřesy	otřes	k1gInPc4	otřes
jako	jako	k8xS	jako
mechanické	mechanický	k2eAgInPc4d1	mechanický
disky	disk	k1gInPc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
času	čas	k1gInSc6	čas
potřebném	potřebný	k2eAgInSc6d1	potřebný
pro	pro	k7c4	pro
vybavení	vybavení	k1gNnSc4	vybavení
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
mikrosekundy	mikrosekunda	k1gFnSc2	mikrosekunda
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
milisekundami	milisekunda	k1gFnPc7	milisekunda
u	u	k7c2	u
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
<g/>
)	)	kIx)	)
a	a	k8xC	a
rychlostmi	rychlost	k1gFnPc7	rychlost
čtení	čtení	k1gNnSc2	čtení
(	(	kIx(	(
<g/>
OCZ	OCZ	kA	OCZ
Z-DRIVE	Z-DRIVE	k1gMnSc1	Z-DRIVE
e	e	k0	e
<g/>
84	[number]	k4	84
–	–	k?	–
800	[number]	k4	800
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
;	;	kIx,	;
Fusion-io	Fusiono	k1gNnSc1	Fusion-io
–	–	k?	–
1,5	[number]	k4	1,5
GB	GB	kA	GB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
proti	proti	k7c3	proti
<g />
.	.	kIx.	.
</s>
<s>
pevným	pevný	k2eAgInPc3d1	pevný
diskům	disk	k1gInPc3	disk
výrazné	výrazný	k2eAgFnSc2d1	výrazná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
SSD	SSD	kA	SSD
jednotky	jednotka	k1gFnPc1	jednotka
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
pro	pro	k7c4	pro
specifické	specifický	k2eAgNnSc4d1	specifické
zvýšení	zvýšení	k1gNnSc4	zvýšení
výkonu	výkon	k1gInSc2	výkon
počítačového	počítačový	k2eAgInSc2d1	počítačový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
pořizovací	pořizovací	k2eAgFnSc1d1	pořizovací
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
<g/>
×	×	k?	×
větší	veliký	k2eAgNnPc4d2	veliký
než	než	k8xS	než
u	u	k7c2	u
běžného	běžný	k2eAgInSc2d1	běžný
HDD	HDD	kA	HDD
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
<g/>
Kč	Kč	kA	Kč
<g/>
/	/	kIx~	/
<g/>
GB	GB	kA	GB
<g/>
,	,	kIx,	,
ceny	cena	k1gFnPc1	cena
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mění	měnit	k5eAaImIp3nS	měnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
SSD	SSD	kA	SSD
jednotky	jednotka	k1gFnPc1	jednotka
však	však	k9	však
trpí	trpět	k5eAaImIp3nP	trpět
i	i	k8xC	i
mnoha	mnoho	k4c7	mnoho
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
dány	dán	k2eAgInPc1d1	dán
jejich	jejich	k3xOp3gFnSc7	jejich
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Flash	Flash	k1gInSc4	Flash
paměti	paměť	k1gFnSc2	paměť
mají	mít	k5eAaImIp3nP	mít
omezenou	omezený	k2eAgFnSc4d1	omezená
životnost	životnost	k1gFnSc4	životnost
maximálním	maximální	k2eAgInSc7d1	maximální
počtem	počet	k1gInSc7	počet
zápisů	zápis	k1gInPc2	zápis
do	do	k7c2	do
stejného	stejný	k2eAgNnSc2d1	stejné
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
klasických	klasický	k2eAgInPc2d1	klasický
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
(	(	kIx(	(
<g/>
udáváno	udávat	k5eAaImNgNnS	udávat
kolem	kolem	k6eAd1	kolem
100	[number]	k4	100
000	[number]	k4	000
zápisů	zápis	k1gInPc2	zápis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
použitých	použitý	k2eAgInPc2d1	použitý
čipů	čip	k1gInPc2	čip
(	(	kIx(	(
<g/>
SLC	SLC	kA	SLC
<g/>
,	,	kIx,	,
MLC	MLC	kA	MLC
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
vyšší	vysoký	k2eAgFnSc2d2	vyšší
ceny	cena	k1gFnSc2	cena
i	i	k8xC	i
životnosti	životnost	k1gFnSc2	životnost
nebo	nebo	k8xC	nebo
nižší	nízký	k2eAgFnSc2d2	nižší
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
kratší	krátký	k2eAgFnSc2d2	kratší
životnosti	životnost	k1gFnSc2	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
je	být	k5eAaImIp3nS	být
životnost	životnost	k1gFnSc1	životnost
SSD	SSD	kA	SSD
jednotek	jednotka	k1gFnPc2	jednotka
naopak	naopak	k6eAd1	naopak
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
přepisů	přepis	k1gInPc2	přepis
jedné	jeden	k4xCgFnSc2	jeden
buňky	buňka	k1gFnSc2	buňka
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
SSD	SSD	kA	SSD
zápis	zápis	k1gInSc4	zápis
automaticky	automaticky	k6eAd1	automaticky
postupně	postupně	k6eAd1	postupně
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
k	k	k7c3	k
SSD	SSD	kA	SSD
obvykle	obvykle	k6eAd1	obvykle
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
jako	jako	k9	jako
k	k	k7c3	k
normálním	normální	k2eAgInPc3d1	normální
pevným	pevný	k2eAgInPc3d1	pevný
diskům	disk	k1gInPc3	disk
a	a	k8xC	a
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
degradaci	degradace	k1gFnSc3	degradace
jejich	jejich	k3xOp3gInSc2	jejich
výkonu	výkon	k1gInSc2	výkon
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
jistý	jistý	k2eAgInSc4d1	jistý
nárůst	nárůst	k1gInSc4	nárůst
výkonu	výkon	k1gInSc2	výkon
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
implementace	implementace	k1gFnSc1	implementace
příkazu	příkaz	k1gInSc2	příkaz
TRIM	TRIM	kA	TRIM
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
optimalizace	optimalizace	k1gFnPc1	optimalizace
zabudované	zabudovaný	k2eAgFnPc1d1	zabudovaná
v	v	k7c6	v
ovladačích	ovladač	k1gInPc6	ovladač
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
počítají	počítat	k5eAaImIp3nP	počítat
se	s	k7c7	s
sekvenčním	sekvenční	k2eAgInSc7d1	sekvenční
zápisem	zápis	k1gInSc7	zápis
na	na	k7c4	na
pevné	pevný	k2eAgInPc4d1	pevný
disky	disk	k1gInPc4	disk
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nP	působit
u	u	k7c2	u
SSD	SSD	kA	SSD
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
výkon	výkon	k1gInSc4	výkon
negativně	negativně	k6eAd1	negativně
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
výkonu	výkon	k1gInSc2	výkon
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zejména	zejména	k9	zejména
při	při	k7c6	při
operacích	operace	k1gFnPc6	operace
zápisu	zápis	k1gInSc2	zápis
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kvůli	kvůli	k7c3	kvůli
jednomu	jeden	k4xCgInSc3	jeden
zápisu	zápis	k1gInSc3	zápis
musí	muset	k5eAaImIp3nS	muset
proběhnout	proběhnout	k5eAaPmF	proběhnout
několik	několik	k4yIc4	několik
čtení	čtení	k1gNnPc2	čtení
a	a	k8xC	a
následně	následně	k6eAd1	následně
ještě	ještě	k9	ještě
mazání	mazání	k1gNnSc4	mazání
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Linux	Linux	kA	Linux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
pamětí	paměť	k1gFnSc7	paměť
mají	mít	k5eAaImIp3nP	mít
speciální	speciální	k2eAgInPc1d1	speciální
souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
(	(	kIx(	(
<g/>
Log-structured	Logtructured	k1gInSc1	Log-structured
file	file	k1gNnSc1	file
systems	systems	k1gInSc1	systems
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
tyto	tento	k3xDgInPc4	tento
nedostatky	nedostatek	k1gInPc4	nedostatek
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
hardwarové	hardwarový	k2eAgNnSc1d1	hardwarové
emulační	emulační	k2eAgNnSc1d1	emulační
rozhraní	rozhraní	k1gNnSc1	rozhraní
vypínat	vypínat	k5eAaImF	vypínat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
vznik	vznik	k1gInSc1	vznik
objektově	objektově	k6eAd1	objektově
orientovaných	orientovaný	k2eAgInPc2d1	orientovaný
souborových	souborový	k2eAgInPc2d1	souborový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
odstraní	odstranit	k5eAaPmIp3nP	odstranit
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
emulační	emulační	k2eAgFnSc7d1	emulační
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
oproti	oproti	k7c3	oproti
klasickým	klasický	k2eAgInPc3d1	klasický
pevným	pevný	k2eAgInPc3d1	pevný
diskům	disk	k1gInPc3	disk
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
zatím	zatím	k6eAd1	zatím
znatelně	znatelně	k6eAd1	znatelně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
GB	GB	kA	GB
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
nižší	nízký	k2eAgFnPc4d2	nižší
kapacity	kapacita	k1gFnPc4	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
TRIM	TRIM	kA	TRIM
<g/>
.	.	kIx.	.
</s>
<s>
Příkaz	příkaz	k1gInSc1	příkaz
TRIM	TRIM	kA	TRIM
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
informoval	informovat	k5eAaBmAgInS	informovat
SSD	SSD	kA	SSD
jednotku	jednotka	k1gFnSc4	jednotka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
datové	datový	k2eAgInPc1d1	datový
bloky	blok	k1gInPc1	blok
jsou	být	k5eAaImIp3nP	být
volné	volný	k2eAgInPc1d1	volný
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
dále	daleko	k6eAd2	daleko
využívány	využívat	k5eAaPmNgInP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
nejtypičtěji	typicky	k6eAd3	typicky
datové	datový	k2eAgInPc1d1	datový
bloky	blok	k1gInPc1	blok
smazaného	smazaný	k2eAgInSc2d1	smazaný
souboru	soubor	k1gInSc2	soubor
(	(	kIx(	(
<g/>
data	datum	k1gNnPc1	datum
nejsou	být	k5eNaImIp3nP	být
nulována	nulován	k2eAgNnPc1d1	nulován
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
přepsání	přepsání	k1gNnSc4	přepsání
jinými	jiný	k2eAgNnPc7d1	jiné
daty	datum	k1gNnPc7	datum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkaz	příkaz	k1gInSc1	příkaz
TRIM	TRIM	kA	TRIM
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
SSD	SSD	kA	SSD
jednotka	jednotka	k1gFnSc1	jednotka
mohla	moct	k5eAaImAgFnS	moct
správně	správně	k6eAd1	správně
alokovat	alokovat	k5eAaImF	alokovat
rozložení	rozložení	k1gNnSc4	rozložení
zápisů	zápis	k1gInPc2	zápis
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
tak	tak	k9	tak
k	k	k7c3	k
rovnoměrnému	rovnoměrný	k2eAgNnSc3d1	rovnoměrné
opotřebení	opotřebení	k1gNnSc3	opotřebení
datových	datový	k2eAgFnPc2d1	datová
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
SSD	SSD	kA	SSD
jednotce	jednotka	k1gFnSc6	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
SLC	SLC	kA	SLC
–	–	k?	–
dražší	drahý	k2eAgFnSc1d2	dražší
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
buňka	buňka	k1gFnSc1	buňka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
1	[number]	k4	1
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
zápisů	zápis	k1gInPc2	zápis
a	a	k8xC	a
rychlosti	rychlost	k1gFnPc1	rychlost
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
MLC	MLC	kA	MLC
eMLC	eMLC	k?	eMLC
–	–	k?	–
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
MLC	MLC	kA	MLC
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
zápisů	zápis	k1gInPc2	zápis
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
MLC	MLC	kA	MLC
–	–	k?	–
levnější	levný	k2eAgFnSc1d2	levnější
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
buňka	buňka	k1gFnSc1	buňka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2	[number]	k4	2
stavy	stav	k1gInPc7	stav
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
zápisů	zápis	k1gInPc2	zápis
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
rychlosti	rychlost	k1gFnPc1	rychlost
jsou	být	k5eAaImIp3nP	být
nižší	nízký	k2eAgFnPc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
SLC	SLC	kA	SLC
TLC	TLC	kA	TLC
–	–	k?	–
nejlevnější	levný	k2eAgFnSc1d3	nejlevnější
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
buňka	buňka	k1gFnSc1	buňka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
3	[number]	k4	3
stavy	stav	k1gInPc7	stav
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
zápisů	zápis	k1gInPc2	zápis
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc2	rychlost
jsou	být	k5eAaImIp3nP	být
nižší	nízký	k2eAgInPc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
MLC	MLC	kA	MLC
Flash	Flash	k1gInSc1	Flash
Translation	Translation	k1gInSc1	Translation
Layer	Layer	k1gInSc4	Layer
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
solid-state	solidtat	k1gInSc5	solid-stat
drive	drive	k1gInSc4	drive
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
SSD	SSD	kA	SSD
disky	disk	k1gInPc4	disk
–	–	k?	–
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vavřina	Vavřina	k1gMnSc1	Vavřina
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
magazin	magazin	k1gInSc4	magazin
<g/>
.	.	kIx.	.
<g/>
stahuj	stahovat	k5eAaImRp2nS	stahovat
<g/>
.	.	kIx.	.
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
ten	ten	k3xDgInSc4	ten
správný	správný	k2eAgInSc4d1	správný
čas	čas	k1gInSc4	čas
nahradit	nahradit	k5eAaPmF	nahradit
pevné	pevný	k2eAgInPc4d1	pevný
disky	disk	k1gInPc4	disk
SSD	SSD	kA	SSD
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Bohuněk	Bohuňka	k1gFnPc2	Bohuňka
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
DIIT	DIIT	kA	DIIT
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
