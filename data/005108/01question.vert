<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zvykne	zvyknout	k5eAaPmIp3nS	zvyknout
označovat	označovat	k5eAaImF	označovat
SSD	SSD	kA	SSD
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
používá	používat	k5eAaImIp3nS	používat
volatilní	volatilní	k2eAgFnSc3d1	volatilní
paměti	paměť	k1gFnSc3	paměť
typu	typ	k1gInSc2	typ
SRAM	SRAM	kA	SRAM
nebo	nebo	k8xC	nebo
DRAM	DRAM	kA	DRAM
<g/>
?	?	kIx.	?
</s>
