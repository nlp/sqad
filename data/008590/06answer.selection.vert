<s>
Česká	český	k2eAgFnSc1d1	Česká
ústava	ústava	k1gFnSc1	ústava
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
preambulí	preambule	k1gFnSc7	preambule
a	a	k8xC	a
8	[number]	k4	8
hlavami	hlava	k1gFnPc7	hlava
<g/>
,	,	kIx,	,
zahrnujícími	zahrnující	k2eAgInPc7d1	zahrnující
základní	základní	k2eAgFnSc4d1	základní
ustanovení	ustanovení	k1gNnSc3	ustanovení
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
soudní	soudní	k2eAgInSc1d1	soudní
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
Českou	český	k2eAgFnSc4d1	Česká
národní	národní	k2eAgFnSc4d1	národní
banku	banka	k1gFnSc4	banka
<g/>
,	,	kIx,	,
územní	územní	k2eAgFnSc4d1	územní
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
přechodná	přechodný	k2eAgFnSc1d1	přechodná
a	a	k8xC	a
závěrečná	závěrečný	k2eAgNnPc1d1	závěrečné
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
.	.	kIx.	.
</s>
