<p>
<s>
Melvin	Melvina	k1gFnPc2	Melvina
Jerome	Jerom	k1gInSc5	Jerom
"	"	kIx"	"
<g/>
Mel	mlít	k5eAaImRp2nS	mlít
<g/>
"	"	kIx"	"
Blanc	Blanc	k1gMnSc1	Blanc
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1908	[number]	k4	1908
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
komik	komik	k1gMnSc1	komik
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
rádiový	rádiový	k2eAgMnSc1d1	rádiový
hlasatel	hlasatel	k1gMnSc1	hlasatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
začátků	začátek	k1gInPc2	začátek
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Warner	Warnero	k1gNnPc2	Warnero
Brothers	Brothersa	k1gFnPc2	Brothersa
je	být	k5eAaImIp3nS	být
Blanc	Blanc	k1gMnSc1	Blanc
považován	považován	k2eAgMnSc1d1	považován
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
dabér	dabér	k1gMnSc1	dabér
a	a	k8xC	a
hlasatel	hlasatel	k1gMnSc1	hlasatel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hlasy	hlas	k1gInPc1	hlas
králíka	králík	k1gMnSc2	králík
Bugse	Bugs	k1gMnSc2	Bugs
<g/>
,	,	kIx,	,
kačera	kačer	k1gMnSc2	kačer
Daffyho	Daffy	k1gMnSc2	Daffy
<g/>
,	,	kIx,	,
vepříka	vepřík	k1gMnSc2	vepřík
Porkyho	Porky	k1gMnSc2	Porky
<g/>
,	,	kIx,	,
žlutého	žlutý	k2eAgMnSc2d1	žlutý
kanárka	kanárek	k1gMnSc2	kanárek
Tweetyho	Tweety	k1gMnSc2	Tweety
<g/>
,	,	kIx,	,
kocoura	kocour	k1gMnSc2	kocour
Sylvestera	Sylvester	k1gMnSc2	Sylvester
<g/>
,	,	kIx,	,
loupežníka	loupežník	k1gMnSc2	loupežník
Yosemite	Yosemit	k1gInSc5	Yosemit
Sama	Sam	k1gMnSc2	Sam
<g/>
,	,	kIx,	,
kohouta	kohout	k1gMnSc2	kohout
Leghorna	Leghorna	k1gFnSc1	Leghorna
<g/>
,	,	kIx,	,
marťana	marťan	k1gMnSc4	marťan
Marvina	Marvin	k2eAgMnSc4d1	Marvin
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Pepé	Pepé	k1gNnSc7	Pepé
Le	Le	k1gMnSc2	Le
Pewa	Pewus	k1gMnSc2	Pewus
<g/>
,	,	kIx,	,
Rychlíka	Rychlík	k1gMnSc2	Rychlík
Gonzalese	Gonzalese	k1gFnSc2	Gonzalese
<g/>
,	,	kIx,	,
Wile	Wil	k1gFnSc2	Wil
E.	E.	kA	E.
Coyote	Coyot	k1gInSc5	Coyot
a	a	k8xC	a
Road	Road	k1gInSc4	Road
Runnera	Runner	k1gMnSc2	Runner
<g/>
,	,	kIx,	,
Tasmánského	tasmánský	k2eAgMnSc2d1	tasmánský
Ďábla	ďábel	k1gMnSc2	ďábel
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
Looney	Loonea	k1gFnSc2	Loonea
Tunes	Tunes	k1gInSc1	Tunes
a	a	k8xC	a
Merrie	Merrie	k1gFnSc1	Merrie
Melodies	Melodiesa	k1gFnPc2	Melodiesa
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
zlatých	zlatý	k2eAgFnPc2d1	zlatá
let	léto	k1gNnPc2	léto
animované	animovaný	k2eAgFnSc2d1	animovaná
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
práci	práce	k1gFnSc3	práce
asi	asi	k9	asi
70	[number]	k4	70
animátorů	animátor	k1gMnPc2	animátor
a	a	k8xC	a
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
oceňovány	oceňován	k2eAgInPc1d1	oceňován
za	za	k7c4	za
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
rádiu	rádio	k1gNnSc6	rádio
CBS	CBS	kA	CBS
pracoval	pracovat	k5eAaImAgMnS	pracovat
znovu	znovu	k6eAd1	znovu
jako	jako	k8xC	jako
dabér	dabér	k1gInSc1	dabér
pro	pro	k7c4	pro
studio	studio	k1gNnSc4	studio
Hanna-Barbera	Hanna-Barbero	k1gNnSc2	Hanna-Barbero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
především	především	k9	především
daboval	dabovat	k5eAaBmAgInS	dabovat
postavy	postava	k1gFnSc2	postava
Barneyho	Barney	k1gMnSc2	Barney
Rubbla	Rubbla	k1gMnSc2	Rubbla
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
The	The	k1gMnSc1	The
Flintstones	Flintstones	k1gMnSc1	Flintstones
a	a	k8xC	a
pana	pan	k1gMnSc2	pan
Spacelyho	Spacely	k1gMnSc2	Spacely
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
The	The	k1gFnSc2	The
Jetsons	Jetsonsa	k1gFnPc2	Jetsonsa
<g/>
.	.	kIx.	.
</s>
<s>
Blanc	Blanc	k1gMnSc1	Blanc
byl	být	k5eAaImAgMnS	být
také	také	k9	také
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
součástí	součást	k1gFnSc7	součást
programu	program	k1gInSc2	program
Jacka	Jacek	k1gMnSc2	Jacek
Bennyho	Benny	k1gMnSc2	Benny
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
i	i	k9	i
v	v	k7c6	v
rádiu	rádio	k1gNnSc3	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Daboval	dabovat	k5eAaBmAgMnS	dabovat
také	také	k9	také
původní	původní	k2eAgFnSc4d1	původní
postavu	postava	k1gFnSc4	postava
Datla	datel	k1gMnSc2	datel
Woodyho	Woody	k1gMnSc2	Woody
pro	pro	k7c4	pro
Universal	Universal	k1gFnSc4	Universal
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
získal	získat	k5eAaPmAgMnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Muž	muž	k1gMnSc1	muž
tisíce	tisíc	k4xCgInPc1	tisíc
hlasů	hlas	k1gInPc2	hlas
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
Blanc	Blanc	k1gMnSc1	Blanc
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
lidí	člověk	k1gMnPc2	člověk
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
