<s>
Pryskyřník	pryskyřník	k1gInSc1
karpatský	karpatský	k2eAgInSc4d1
</s>
<s>
Pryskyřník	pryskyřník	k1gInSc1
karpatský	karpatský	k2eAgInSc4d1
Pryskyřník	pryskyřník	k1gInSc4
karpatský	karpatský	k2eAgInSc4d1
(	(	kIx(
<g/>
Ranunculus	Ranunculus	k1gInSc1
carpaticus	carpaticus	k1gInSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
pryskyřníkotvaré	pryskyřníkotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Ranunculales	Ranunculales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
pryskyřníkovité	pryskyřníkovitá	k1gFnPc1
(	(	kIx(
<g/>
Ranunculaceae	Ranunculaceae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
pryskyřník	pryskyřník	k1gInSc1
(	(	kIx(
<g/>
Ranunculus	Ranunculus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Ranunculus	Ranunculus	k1gInSc1
carpaticusHerbich	carpaticusHerbicha	k1gFnPc2
Synonyma	synonymum	k1gNnSc2
</s>
<s>
Ranunculus	Ranunculus	k1gMnSc1
dentatus	dentatus	k1gMnSc1
(	(	kIx(
<g/>
Baumg	Baumg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Freyn	Freyn	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pryskyřník	pryskyřník	k1gInSc1
karpatský	karpatský	k2eAgInSc1d1
(	(	kIx(
<g/>
Ranunculus	Ranunculus	k1gInSc1
carpaticus	carpaticus	k2eAgInSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
rostliny	rostlina	k1gFnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
pryskyřníkovité	pryskyřníkovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Ranunculaceae	Ranunculaceae	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
vytrvalou	vytrvalý	k2eAgFnSc4d1
rostlinu	rostlina	k1gFnSc4
dorůstající	dorůstající	k2eAgFnSc4d1
nejčastěji	často	k6eAd3
výšky	výška	k1gFnPc1
15	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
cm	cm	kA
s	s	k7c7
podzemním	podzemní	k2eAgInSc7d1
oddenkem	oddenek	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Lodyha	lodyha	k1gFnSc1
je	být	k5eAaImIp3nS
přímá	přímý	k2eAgFnSc1d1
<g/>
,	,	kIx,
víceméně	víceméně	k9
nevětvená	větvený	k2eNgFnSc1d1
<g/>
,	,	kIx,
přitiskle	přitiskle	k6eAd1
chlupatá	chlupatý	k2eAgFnSc1d1
až	až	k8xS
olysalá	olysalý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
střídavé	střídavý	k2eAgFnPc1d1
<g/>
,	,	kIx,
přízemní	přízemní	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
dlouze	dlouho	k6eAd1
řapíkaté	řapíkatý	k2eAgInPc1d1
<g/>
,	,	kIx,
lodyžní	lodyžní	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
víceméně	víceméně	k9
přisedlé	přisedlý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čepele	čepel	k1gInSc2
přízemních	přízemní	k2eAgMnPc2d1
listů	list	k1gInPc2
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
troj	trojit	k5eAaImRp2nS
až	až	k9
pětidílné	pětidílný	k2eAgNnSc4d1
<g/>
,	,	kIx,
úkrojky	úkrojek	k1gInPc4
jsou	být	k5eAaImIp3nP
vpředu	vpředu	k6eAd1
hrubě	hrubě	k6eAd1
zubaté	zubatý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Lodyžní	lodyžní	k2eAgInPc1d1
listy	list	k1gInPc1
jsou	být	k5eAaImIp3nP
troj	trojit	k5eAaImRp2nS
až	až	k6eAd1
pětiklané	pětiklaný	k2eAgInPc1d1
s	s	k7c7
čárkovitými	čárkovitý	k2eAgInPc7d1
úkrojky	úkrojek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
žluté	žlutý	k2eAgFnPc1d1
<g/>
,	,	kIx,
asi	asi	k9
30	#num#	k4
mm	mm	kA
v	v	k7c6
průměru	průměr	k1gInSc6
<g/>
,	,	kIx,
květní	květní	k2eAgNnSc1d1
lůžko	lůžko	k1gNnSc1
je	být	k5eAaImIp3nS
chlupaté	chlupatý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Kališních	kališní	k2eAgInPc2d1
lístků	lístek	k1gInPc2
je	být	k5eAaImIp3nS
5	#num#	k4
<g/>
,	,	kIx,
vně	vně	k6eAd1
chlupaté	chlupatý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korunní	korunní	k2eAgInPc1d1
lístky	lístek	k1gInPc1
jsou	být	k5eAaImIp3nP
žluté	žlutý	k2eAgFnPc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jich	on	k3xPp3gFnPc2
5	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
nejsou	být	k5eNaImIp3nP
vykrojené	vykrojený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvete	kvést	k5eAaImIp3nS
v	v	k7c6
červnu	červen	k1gInSc6
až	až	k6eAd1
v	v	k7c6
červenci	červenec	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
nažka	nažka	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
zakončená	zakončený	k2eAgFnSc1d1
srpovitým	srpovitý	k2eAgInSc7d1
zobánkem	zobánek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nažky	nažka	k1gFnPc1
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgFnP
do	do	k7c2
souplodí	souplodí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
chromozomů	chromozom	k1gInPc2
je	být	k5eAaImIp3nS
2	#num#	k4
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Pryskyřník	pryskyřník	k1gInSc1
karpatský	karpatský	k2eAgInSc1d1
je	být	k5eAaImIp3nS
endemit	endemit	k1gInSc1
východních	východní	k2eAgInPc2d1
a	a	k8xC
jižních	jižní	k2eAgInPc2d1
Karpat	Karpaty	k1gInPc2
<g/>
,	,	kIx,
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
hlavně	hlavně	k9
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
a	a	k8xC
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
<g/>
,	,	kIx,
okrajově	okrajově	k6eAd1
na	na	k7c6
východním	východní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
neroste	růst	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
jen	jen	k9
ojedinělá	ojedinělý	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
na	na	k7c6
východě	východ	k1gInSc6
v	v	k7c6
Bukovských	Bukovských	k2eAgInPc6d1
vrších	vrch	k1gInPc6
u	u	k7c2
obce	obec	k1gFnSc2
Zboj	zboj	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
DOSTÁL	Dostál	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
Květena	květena	k1gFnSc1
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Flora	Flora	k1gMnSc1
Europaea	Europaea	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
