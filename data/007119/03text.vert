<s>
Sítnice	sítnice	k1gFnSc1	sítnice
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
retina	retina	k1gFnSc1	retina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
tenká	tenký	k2eAgFnSc1d1	tenká
vrstva	vrstva	k1gFnSc1	vrstva
oka	oko	k1gNnSc2	oko
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
snímání	snímání	k1gNnSc4	snímání
a	a	k8xC	a
předzpracování	předzpracování	k1gNnSc4	předzpracování
světelných	světelný	k2eAgInPc2d1	světelný
signálů	signál	k1gInPc2	signál
přicházejících	přicházející	k2eAgInPc2d1	přicházející
na	na	k7c4	na
sítnici	sítnice	k1gFnSc4	sítnice
skrze	skrze	k?	skrze
čočku	čočka	k1gFnSc4	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Anatomicky	anatomicky	k6eAd1	anatomicky
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
deset	deset	k4xCc4	deset
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
:	:	kIx,	:
vrstva	vrstva	k1gFnSc1	vrstva
pigmentových	pigmentový	k2eAgFnPc2d1	pigmentová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
vrstva	vrstva	k1gFnSc1	vrstva
čivých	čivý	k2eAgInPc2d1	čivý
výběžků	výběžek	k1gInPc2	výběžek
<g/>
,	,	kIx,	,
zevní	zevní	k2eAgFnSc1d1	zevní
ohraničující	ohraničující	k2eAgFnSc1d1	ohraničující
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
jádrová	jádrový	k2eAgFnSc1d1	jádrová
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
tyčinky	tyčinka	k1gFnPc1	tyčinka
a	a	k8xC	a
čípky	čípek	k1gInPc1	čípek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zevní	zevní	k2eAgFnSc1d1	zevní
plexiformní	plexiformní	k2eAgFnSc1d1	plexiformní
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
jádrová	jádrový	k2eAgFnSc1d1	jádrová
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
bipolární	bipolární	k2eAgMnPc1d1	bipolární
<g/>
,	,	kIx,	,
horizontální	horizontální	k2eAgMnPc1d1	horizontální
a	a	k8xC	a
amakrinní	amakrinný	k2eAgMnPc1d1	amakrinný
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
plexiformní	plexiformní	k2eAgFnSc1d1	plexiformní
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
vrstva	vrstva	k1gFnSc1	vrstva
gangliových	gangliový	k2eAgFnPc2d1	gangliová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
ohraničující	ohraničující	k2eAgFnSc1d1	ohraničující
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
vrstva	vrstva	k1gFnSc1	vrstva
axonů	axon	k1gInPc2	axon
gangliových	gangliový	k2eAgFnPc2d1	gangliová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
čivé	čivý	k2eAgInPc1d1	čivý
výběžky	výběžek	k1gInPc1	výběžek
jsou	být	k5eAaImIp3nP	být
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Pigmentové	pigmentový	k2eAgFnPc1d1	pigmentová
buňky	buňka	k1gFnPc1	buňka
–	–	k?	–
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
tyčinkami	tyčinka	k1gFnPc7	tyčinka
a	a	k8xC	a
čípky	čípek	k1gInPc7	čípek
a	a	k8xC	a
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
jeho	jeho	k3xOp3gInSc3	jeho
zpětnému	zpětný	k2eAgInSc3d1	zpětný
odrazu	odraz	k1gInSc3	odraz
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
kočkovitých	kočkovití	k1gMnPc2	kočkovití
šelem	šelma	k1gFnPc2	šelma
<g/>
)	)	kIx)	)
čímž	což	k3yRnSc7	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
ostrost	ostrost	k1gFnSc4	ostrost
vidění	vidění	k1gNnSc3	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinky	tyčinka	k1gFnPc1	tyčinka
a	a	k8xC	a
čípky	čípek	k1gInPc1	čípek
–	–	k?	–
modifikované	modifikovaný	k2eAgInPc1d1	modifikovaný
neurony	neuron	k1gInPc1	neuron
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
dopad	dopad	k1gInSc4	dopad
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinky	tyčinka	k1gFnPc1	tyčinka
reagují	reagovat	k5eAaBmIp3nP	reagovat
i	i	k9	i
na	na	k7c4	na
slabé	slabý	k2eAgNnSc4d1	slabé
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neregistrují	registrovat	k5eNaBmIp3nP	registrovat
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
neposkytují	poskytovat	k5eNaImIp3nP	poskytovat
dostatečně	dostatečně	k6eAd1	dostatečně
ostrý	ostrý	k2eAgInSc4d1	ostrý
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Čípků	Čípek	k1gMnPc2	Čípek
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
základní	základní	k2eAgFnSc4d1	základní
barvu	barva	k1gFnSc4	barva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
ostrý	ostrý	k2eAgInSc4d1	ostrý
a	a	k8xC	a
barevný	barevný	k2eAgInSc4d1	barevný
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
osvětlení	osvětlení	k1gNnSc4	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Bipolární	bipolární	k2eAgFnPc1d1	bipolární
buňky	buňka	k1gFnPc1	buňka
–	–	k?	–
přepojení	přepojení	k1gNnSc1	přepojení
vzruchu	vzruch	k1gInSc2	vzruch
z	z	k7c2	z
čivých	čivý	k2eAgFnPc2d1	čivý
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Horizontální	horizontální	k2eAgFnPc1d1	horizontální
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
amakrinní	amakrinný	k2eAgMnPc1d1	amakrinný
buňky	buňka	k1gFnSc2	buňka
–	–	k?	–
Asociační	asociační	k2eAgFnPc1d1	asociační
buňky	buňka	k1gFnPc1	buňka
propojující	propojující	k2eAgFnPc1d1	propojující
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
bipolární	bipolární	k2eAgFnSc4d1	bipolární
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
gangliové	gangliový	k2eAgFnPc1d1	gangliová
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Podílejí	podílet	k5eAaImIp3nP	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
předzpracování	předzpracování	k1gNnSc4	předzpracování
obrazu	obraz	k1gInSc2	obraz
(	(	kIx(	(
<g/>
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
původu	původ	k1gInSc2	původ
sítnice	sítnice	k1gFnSc2	sítnice
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gangliové	gangliový	k2eAgFnPc1d1	gangliová
buňky	buňka	k1gFnPc1	buňka
–	–	k?	–
buňky	buňka	k1gFnSc2	buňka
sbírající	sbírající	k2eAgFnPc1d1	sbírající
informace	informace	k1gFnPc1	informace
ze	z	k7c2	z
sítnice	sítnice	k1gFnSc2	sítnice
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
cca	cca	kA	cca
10	[number]	k4	10
<g/>
x	x	k?	x
méně	málo	k6eAd2	málo
než	než	k8xS	než
čivých	čivý	k2eAgFnPc2d1	čivý
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
a	a	k8xC	a
přeposílající	přeposílající	k2eAgFnPc4d1	přeposílající
informace	informace	k1gFnPc4	informace
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
jejich	jejich	k3xOp3gInPc2	jejich
axonů	axon	k1gInPc2	axon
tvoří	tvořit	k5eAaImIp3nS	tvořit
zrakový	zrakový	k2eAgInSc1d1	zrakový
nerv	nerv	k1gInSc1	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sítnici	sítnice	k1gFnSc6	sítnice
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
zřetelné	zřetelný	k2eAgInPc4d1	zřetelný
dva	dva	k4xCgInPc4	dva
útvary	útvar	k1gInPc4	útvar
–	–	k?	–
slepá	slepý	k2eAgFnSc1d1	slepá
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
optický	optický	k2eAgInSc4d1	optický
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
zrakový	zrakový	k2eAgInSc1d1	zrakový
nerv	nerv	k1gInSc1	nerv
a	a	k8xC	a
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
a.	a.	k?	a.
centralis	centralis	k1gFnSc1	centralis
retinae	retinaat	k5eAaPmIp3nS	retinaat
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
i	i	k9	i
větví	větvit	k5eAaImIp3nS	větvit
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
čtyři	čtyři	k4xCgFnPc4	čtyři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
větve	větev	k1gFnPc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Optický	optický	k2eAgInSc1d1	optický
disk	disk	k1gInSc1	disk
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
bod	bod	k1gInSc1	bod
při	při	k7c6	při
vyšetření	vyšetření	k1gNnSc6	vyšetření
oftalmoskopem	oftalmoskop	k1gInSc7	oftalmoskop
–	–	k?	–
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
propadlý	propadlý	k2eAgMnSc1d1	propadlý
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
je	být	k5eAaImIp3nS	být
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
nitrooční	nitrooční	k2eAgInSc1d1	nitrooční
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
zelený	zelený	k2eAgInSc1d1	zelený
zákal	zákal	k1gInSc1	zákal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
vystouplý	vystouplý	k2eAgInSc1d1	vystouplý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
tlak	tlak	k1gInSc1	tlak
nitrolebeční	nitrolebeční	k2eAgInSc1d1	nitrolebeční
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
mnoha	mnoho	k4c7	mnoho
patologickými	patologický	k2eAgInPc7d1	patologický
procesy	proces	k1gInPc7	proces
v	v	k7c6	v
lebce	lebka	k1gFnSc6	lebka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
život	život	k1gInSc4	život
ohrožující	ohrožující	k2eAgInSc4d1	ohrožující
(	(	kIx(	(
<g/>
nádor	nádor	k1gInSc4	nádor
<g/>
,	,	kIx,	,
hydrocephalus	hydrocephalus	k1gInSc4	hydrocephalus
<g/>
,	,	kIx,	,
epidurální	epidurální	k2eAgNnSc4d1	epidurální
krvácení	krvácení	k1gNnSc4	krvácení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
útvarem	útvar	k1gInSc7	útvar
je	být	k5eAaImIp3nS	být
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
centrální	centrální	k2eAgFnSc1d1	centrální
jamka	jamka	k1gFnSc1	jamka
obsahující	obsahující	k2eAgFnSc4d1	obsahující
žlutou	žlutý	k2eAgFnSc4d1	žlutá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
skvrna	skvrna	k1gFnSc1	skvrna
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
maximální	maximální	k2eAgFnSc2d1	maximální
ostrosti	ostrost	k1gFnSc2	ostrost
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
čípky	čípek	k1gInPc7	čípek
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
vrstvy	vrstva	k1gFnPc1	vrstva
sítnice	sítnice	k1gFnSc2	sítnice
jsou	být	k5eAaImIp3nP	být
odsunuty	odsunout	k5eAaPmNgInP	odsunout
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
ona	onen	k3xDgFnSc1	onen
jamka	jamka	k1gFnSc1	jamka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paprsky	paprsek	k1gInPc1	paprsek
jdoucí	jdoucí	k2eAgInPc1d1	jdoucí
z	z	k7c2	z
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
se	se	k3xPyFc4	se
oko	oko	k1gNnSc1	oko
soustředí	soustředit	k5eAaPmIp3nS	soustředit
jsou	být	k5eAaImIp3nP	být
zaostřovány	zaostřovat	k5eAaImNgInP	zaostřovat
právě	právě	k9	právě
sem	sem	k6eAd1	sem
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
světelných	světelný	k2eAgInPc2d1	světelný
paprsků	paprsek	k1gInPc2	paprsek
pronikajících	pronikající	k2eAgInPc2d1	pronikající
do	do	k7c2	do
oční	oční	k2eAgFnSc2d1	oční
koule	koule	k1gFnSc2	koule
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
tyčinkách	tyčinka	k1gFnPc6	tyčinka
a	a	k8xC	a
čípcích	čípek	k1gInPc6	čípek
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
chemické	chemický	k2eAgFnSc2d1	chemická
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
vzniku	vznik	k1gInSc3	vznik
nervových	nervový	k2eAgInPc2d1	nervový
impulsů	impuls	k1gInPc2	impuls
přenášených	přenášený	k2eAgInPc2d1	přenášený
do	do	k7c2	do
centrálního	centrální	k2eAgInSc2d1	centrální
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
obsažená	obsažený	k2eAgFnSc1d1	obsažená
ve	v	k7c6	v
fotoreceptorech	fotoreceptor	k1gInPc6	fotoreceptor
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozkladu	rozklad	k1gInSc3	rozklad
působením	působení	k1gNnSc7	působení
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rodopsin	rodopsin	k1gInSc1	rodopsin
<g/>
.	.	kIx.	.
</s>
<s>
Sítnice	sítnice	k1gFnSc1	sítnice
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
výchlipka	výchlipka	k1gFnSc1	výchlipka
z	z	k7c2	z
embryonálního	embryonální	k2eAgInSc2d1	embryonální
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
prosencephala	prosencephal	k1gMnSc2	prosencephal
<g/>
)	)	kIx)	)
což	což	k3yQnSc1	což
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
jednak	jednak	k8xC	jednak
její	její	k3xOp3gFnSc4	její
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
jednak	jednak	k8xC	jednak
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
zpracování	zpracování	k1gNnSc3	zpracování
zrakového	zrakový	k2eAgInSc2d1	zrakový
vjemu	vjem	k1gInSc2	vjem
dochází	docházet	k5eAaImIp3nS	docházet
už	už	k6eAd1	už
v	v	k7c6	v
sítnici	sítnice	k1gFnSc6	sítnice
samotné	samotný	k2eAgFnSc6d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
to	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zrakový	zrakový	k2eAgInSc1d1	zrakový
nerv	nerv	k1gInSc1	nerv
na	na	k7c4	na
sobě	se	k3xPyFc3	se
má	mít	k5eAaImIp3nS	mít
meningy	meninga	k1gFnPc4	meninga
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
zdráhají	zdráhat	k5eAaImIp3nP	zdráhat
nazývat	nazývat	k5eAaImF	nazývat
zrakovým	zrakový	k2eAgInSc7d1	zrakový
nervem	nerv	k1gInSc7	nerv
a	a	k8xC	a
preferují	preferovat	k5eAaImIp3nP	preferovat
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
zrakový	zrakový	k2eAgInSc4d1	zrakový
svazek	svazek	k1gInSc4	svazek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sítnice	sítnice	k1gFnSc2	sítnice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
