<s>
Tři	tři	k4xCgInPc1	tři
oříšky	oříšek	k1gInPc1	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Drei	Dree	k1gFnSc4	Dree
Haselnüsse	Haselnüsse	k1gFnSc1	Haselnüsse
für	für	k?	für
Aschenbrödel	Aschenbrödlo	k1gNnPc2	Aschenbrödlo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česko-německá	českoěmecký	k2eAgFnSc1d1	česko-německá
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
režiséra	režisér	k1gMnSc2	režisér
Václava	Václav	k1gMnSc2	Václav
Vorlíčka	Vorlíček	k1gMnSc2	Vorlíček
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
napsal	napsat	k5eAaBmAgInS	napsat
podle	podle	k7c2	podle
předlohy	předloha	k1gFnSc2	předloha
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
František	František	k1gMnSc1	František
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
<g/>
,	,	kIx,	,
v	v	k7c6	v
titulcích	titulek	k1gInPc6	titulek
je	být	k5eAaImIp3nS	být
však	však	k9	však
uvedena	uvést	k5eAaPmNgFnS	uvést
Bohumila	Bohumila	k1gFnSc1	Bohumila
Zelenková	Zelenková	k1gFnSc1	Zelenková
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
zaštítila	zaštítit	k5eAaPmAgFnS	zaštítit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Pavlíčkovi	Pavlíčkův	k2eAgMnPc1d1	Pavlíčkův
komunisté	komunista	k1gMnPc1	komunista
zakázali	zakázat	k5eAaPmAgMnP	zakázat
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc4d1	hlavní
píseň	píseň	k1gFnSc4	píseň
Kdepak	kdepak	k9	kdepak
ty	ten	k3xDgInPc4	ten
ptáčku	ptáček	k1gInSc2	ptáček
hnízdo	hnízdo	k1gNnSc1	hnízdo
máš	mít	k5eAaImIp2nS	mít
zpívá	zpívat	k5eAaImIp3nS	zpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gInSc1	Gott
a	a	k8xC	a
text	text	k1gInSc4	text
napsal	napsat	k5eAaPmAgMnS	napsat
Jiří	Jiří	k1gMnSc1	Jiří
Štaidl	Štaidl	k1gMnSc1	Štaidl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
Libuše	Libuše	k1gFnSc1	Libuše
Šafránková	Šafránková	k1gFnSc1	Šafránková
jako	jako	k8xC	jako
Popelka	Popelka	k1gMnSc1	Popelka
a	a	k8xC	a
jako	jako	k8xS	jako
princ	princ	k1gMnSc1	princ
Pavel	Pavel	k1gMnSc1	Pavel
Trávníček	Trávníček	k1gMnSc1	Trávníček
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
daboval	dabovat	k5eAaBmAgMnS	dabovat
Petr	Petr	k1gMnSc1	Petr
Svojtka	Svojtka	k1gMnSc1	Svojtka
<g/>
.	.	kIx.	.
</s>
<s>
Natáčelo	natáčet	k5eAaImAgNnS	natáčet
se	se	k3xPyFc4	se
na	na	k7c6	na
Klatovsku	Klatovsko	k1gNnSc6	Klatovsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
lesích	les	k1gInPc6	les
kolem	kolem	k7c2	kolem
Javorné	Javorný	k2eAgFnSc2d1	Javorná
u	u	k7c2	u
Čachrova	Čachrov	k1gInSc2	Čachrov
<g/>
,	,	kIx,	,
u	u	k7c2	u
vodního	vodní	k2eAgInSc2d1	vodní
hradu	hrad	k1gInSc2	hrad
Švihov	Švihov	k1gInSc1	Švihov
<g/>
,	,	kIx,	,
v	v	k7c6	v
barrandovském	barrandovský	k2eAgNnSc6d1	Barrandovské
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Studiu	studio	k1gNnSc6	studio
Babelsberg	Babelsberg	k1gInSc4	Babelsberg
a	a	k8xC	a
u	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
Moritzburg	Moritzburg	k1gInSc1	Moritzburg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
klasického	klasický	k2eAgNnSc2d1	klasické
schématu	schéma	k1gNnSc2	schéma
pohádky	pohádka	k1gFnSc2	pohádka
o	o	k7c6	o
Popelce	Popelka	k1gFnSc6	Popelka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
doma	doma	k6eAd1	doma
musí	muset	k5eAaImIp3nS	muset
snášet	snášet	k5eAaImF	snášet
ústrky	ústrk	k1gInPc4	ústrk
macechy	macecha	k1gFnSc2	macecha
a	a	k8xC	a
nevlastní	vlastní	k2eNgFnSc2d1	nevlastní
sestry	sestra	k1gFnSc2	sestra
<g/>
,	,	kIx,	,
v	v	k7c6	v
kouzelných	kouzelný	k2eAgInPc6d1	kouzelný
oříšcích	oříšek	k1gInPc6	oříšek
dostane	dostat	k5eAaPmIp3nS	dostat
plesové	plesový	k2eAgInPc4d1	plesový
šaty	šat	k1gInPc4	šat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
na	na	k7c6	na
plese	ples	k1gInSc6	ples
okouzlí	okouzlit	k5eAaPmIp3nS	okouzlit
prince	princ	k1gMnPc4	princ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
ztratí	ztratit	k5eAaPmIp3nS	ztratit
střevíček	střevíček	k1gInSc1	střevíček
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
princ	princ	k1gMnSc1	princ
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
všem	všecek	k3xTgFnPc3	všecek
dívkám	dívka	k1gFnPc3	dívka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
Popelce	Popelka	k1gFnSc3	Popelka
padne	padnout	k5eAaImIp3nS	padnout
jako	jako	k9	jako
ulitý	ulitý	k2eAgMnSc1d1	ulitý
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Popelku	Popelka	k1gFnSc4	Popelka
ze	z	k7c2	z
statku	statek	k1gInSc2	statek
odvede	odvést	k5eAaPmIp3nS	odvést
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgNnPc3d1	ostatní
filmovým	filmový	k2eAgNnPc3d1	filmové
zpracováním	zpracování	k1gNnPc3	zpracování
Popelky	Popelka	k1gMnSc2	Popelka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
verzi	verze	k1gFnSc6	verze
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
obohaceni	obohatit	k5eAaPmNgMnP	obohatit
o	o	k7c4	o
nové	nový	k2eAgFnPc4d1	nová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
Popelka	Popelka	k1gMnSc1	Popelka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Libuše	Libuše	k1gFnSc2	Libuše
Šafránkové	Šafránková	k1gFnSc2	Šafránková
živější	živý	k2eAgFnSc2d2	živější
-	-	kIx~	-
ovládá	ovládat	k5eAaImIp3nS	ovládat
jízdu	jízda	k1gFnSc4	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
střílí	střílet	k5eAaImIp3nS	střílet
z	z	k7c2	z
kuše	kuše	k1gFnSc2	kuše
jako	jako	k8xS	jako
obratný	obratný	k2eAgMnSc1d1	obratný
lovec	lovec	k1gMnSc1	lovec
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
Pavel	Pavel	k1gMnSc1	Pavel
Trávníček	Trávníček	k1gMnSc1	Trávníček
představuje	představovat	k5eAaImIp3nS	představovat
svéhlavého	svéhlavý	k2eAgMnSc4d1	svéhlavý
mladíka	mladík	k1gMnSc4	mladík
<g/>
,	,	kIx,	,
oddávajícího	oddávající	k2eAgMnSc2d1	oddávající
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
zábavě	zábava	k1gFnSc3	zábava
s	s	k7c7	s
kamarády	kamarád	k1gMnPc7	kamarád
nežli	nežli	k8xS	nežli
pilování	pilování	k1gNnSc3	pilování
dvorní	dvorní	k2eAgFnSc2d1	dvorní
etikety	etiketa	k1gFnSc2	etiketa
a	a	k8xC	a
přípravám	příprava	k1gFnPc3	příprava
na	na	k7c4	na
ples	ples	k1gInSc4	ples
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
má	mít	k5eAaImIp3nS	mít
najít	najít	k5eAaPmF	najít
nevěstu	nevěsta	k1gFnSc4	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Moritzburg	Moritzburg	k1gInSc1	Moritzburg
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
výstava	výstava	k1gFnSc1	výstava
o	o	k7c6	o
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Popelce	popelec	k1gInPc1	popelec
v	v	k7c6	v
útlém	útlý	k2eAgNnSc6d1	útlé
dětství	dětství	k1gNnSc6	dětství
zemřela	zemřít	k5eAaPmAgFnS	zemřít
maminka	maminka	k1gFnSc1	maminka
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgMnSc1d1	bohatý
statkář	statkář	k1gMnSc1	statkář
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
znovu	znovu	k6eAd1	znovu
oženit	oženit	k5eAaPmF	oženit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
děvče	děvče	k1gNnSc1	děvče
nevyrůstalo	vyrůstat	k5eNaImAgNnS	vyrůstat
samo	sám	k3xTgNnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Macecha	macecha	k1gFnSc1	macecha
si	se	k3xPyFc3	se
přivedla	přivést	k5eAaPmAgFnS	přivést
dceru	dcera	k1gFnSc4	dcera
Doru	Dora	k1gFnSc4	Dora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
podobně	podobně	k6eAd1	podobně
stará	starý	k2eAgFnSc1d1	stará
jako	jako	k8xS	jako
Popelka	Popelka	k1gFnSc1	Popelka
<g/>
.	.	kIx.	.
</s>
<s>
Tatínek	tatínek	k1gMnSc1	tatínek
měl	mít	k5eAaImAgMnS	mít
Popelku	Popelka	k1gFnSc4	Popelka
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
trávil	trávit	k5eAaImAgMnS	trávit
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spoustu	spoustu	k6eAd1	spoustu
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
ji	on	k3xPp3gFnSc4	on
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
střílet	střílet	k5eAaImF	střílet
z	z	k7c2	z
kuše	kuše	k1gFnSc2	kuše
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
dovednosti	dovednost	k1gFnPc1	dovednost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
spíš	spíš	k9	spíš
hodily	hodit	k5eAaPmAgFnP	hodit
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
statkářově	statkářův	k2eAgFnSc6d1	statkářova
smrti	smrt	k1gFnSc6	smrt
majetek	majetek	k1gInSc4	majetek
zdědila	zdědit	k5eAaPmAgFnS	zdědit
jeho	jeho	k3xOp3gFnSc1	jeho
druhá	druhý	k4xOgFnSc1	druhý
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
tyranizují	tyranizovat	k5eAaImIp3nP	tyranizovat
čeledíny	čeledín	k1gMnPc4	čeledín
<g/>
,	,	kIx,	,
Popelce	popelec	k1gInPc1	popelec
se	se	k3xPyFc4	se
vysmívají	vysmívat	k5eAaImIp3nP	vysmívat
a	a	k8xC	a
vymýšlejí	vymýšlet	k5eAaImIp3nP	vymýšlet
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
ty	ty	k3xPp2nSc1	ty
nejpodřadnější	podřadný	k2eAgFnSc1d3	nejpodřadnější
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
vše	všechen	k3xTgNnSc1	všechen
s	s	k7c7	s
pokorou	pokora	k1gFnSc7	pokora
snáší	snášet	k5eAaImIp3nS	snášet
a	a	k8xC	a
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
vše	všechen	k3xTgNnSc1	všechen
obrátí	obrátit	k5eAaPmIp3nS	obrátit
v	v	k7c4	v
dobré	dobrý	k2eAgNnSc4d1	dobré
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
čas	čas	k1gInSc1	čas
tráví	trávit	k5eAaImIp3nS	trávit
s	s	k7c7	s
oblíbenými	oblíbený	k2eAgNnPc7d1	oblíbené
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
její	její	k3xOp3gFnSc4	její
lásku	láska	k1gFnSc4	láska
oplácejí	oplácet	k5eAaImIp3nP	oplácet
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
třemi	tři	k4xCgNnPc7	tři
lety	léto	k1gNnPc7	léto
dostala	dostat	k5eAaPmAgFnS	dostat
od	od	k7c2	od
tatínka	tatínek	k1gMnSc2	tatínek
bílého	bílý	k1gMnSc2	bílý
koně	kůň	k1gMnSc2	kůň
Juráška	Jurášek	k1gMnSc2	Jurášek
<g/>
,	,	kIx,	,
na	na	k7c6	na
statku	statek	k1gInSc6	statek
jí	on	k3xPp3gFnSc3	on
dělá	dělat	k5eAaImIp3nS	dělat
společnost	společnost	k1gFnSc1	společnost
pes	pes	k1gMnSc1	pes
Tajtrlík	Tajtrlík	k?	Tajtrlík
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
poklady	poklad	k1gInPc1	poklad
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
si	se	k3xPyFc3	se
schovává	schovávat	k5eAaImIp3nS	schovávat
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
špejchárku	špejchárku	k?	špejchárku
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
střeží	střežit	k5eAaImIp3nS	střežit
sova	sova	k1gFnSc1	sova
Rozárka	Rozárka	k1gFnSc1	Rozárka
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc2	jeden
zimy	zima	k1gFnSc2	zima
panuje	panovat	k5eAaImIp3nS	panovat
na	na	k7c6	na
statku	statek	k1gInSc6	statek
čilý	čilý	k2eAgInSc1d1	čilý
ruch	ruch	k1gInSc1	ruch
<g/>
,	,	kIx,	,
očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
návštěva	návštěva	k1gFnSc1	návštěva
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
míří	mířit	k5eAaImIp3nS	mířit
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
nedaleký	daleký	k2eNgInSc4d1	nedaleký
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Nervozní	Nervozní	k2eAgFnSc1d1	Nervozní
statkářka	statkářka	k1gFnSc1	statkářka
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
stav	stav	k1gInSc4	stav
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
čeledíny	čeledín	k1gMnPc4	čeledín
<g/>
,	,	kIx,	,
z	z	k7c2	z
Popelky	Popelka	k1gMnSc2	Popelka
však	však	k9	však
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
klid	klid	k1gInSc1	klid
<g/>
.	.	kIx.	.
</s>
<s>
Pomocník	pomocník	k1gMnSc1	pomocník
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
rozbije	rozbít	k5eAaPmIp3nS	rozbít
mísu	mísa	k1gFnSc4	mísa
<g/>
,	,	kIx,	,
statkářka	statkářka	k1gFnSc1	statkářka
jej	on	k3xPp3gMnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
potrestat	potrestat	k5eAaPmF	potrestat
karabáčem	karabáč	k1gInSc7	karabáč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Popelka	Popelka	k1gMnSc1	Popelka
vezme	vzít	k5eAaPmIp3nS	vzít
vinu	vina	k1gFnSc4	vina
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Macecha	macecha	k1gFnSc1	macecha
jí	jíst	k5eAaImIp3nS	jíst
za	za	k7c4	za
trest	trest	k1gInSc4	trest
přikáže	přikázat	k5eAaPmIp3nS	přikázat
přebrat	přebrat	k5eAaPmF	přebrat
hrách	hrách	k1gInSc4	hrách
s	s	k7c7	s
popelem	popel	k1gInSc7	popel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přebíráním	přebírání	k1gNnSc7	přebírání
luštěnin	luštěnina	k1gFnPc2	luštěnina
dívce	dívka	k1gFnSc6	dívka
pomohou	pomoct	k5eAaPmIp3nP	pomoct
holubi	holub	k1gMnPc1	holub
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
pustila	pustit	k5eAaPmAgFnS	pustit
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příjezdem	příjezd	k1gInSc7	příjezd
královské	královský	k2eAgFnSc2d1	královská
družiny	družina	k1gFnSc2	družina
si	se	k3xPyFc3	se
Popelka	Popelka	k1gMnSc1	Popelka
přes	přes	k7c4	přes
zákaz	zákaz	k1gInSc4	zákaz
macechy	macecha	k1gFnSc2	macecha
vyjede	vyjet	k5eAaPmIp3nS	vyjet
na	na	k7c4	na
Juráškovi	Jurášek	k1gMnSc3	Jurášek
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
do	do	k7c2	do
lesa	les	k1gInSc2	les
do	do	k7c2	do
starého	starý	k2eAgInSc2d1	starý
špejchárku	špejchárku	k?	špejchárku
za	za	k7c7	za
sovou	sova	k1gFnSc7	sova
Rozárkou	Rozárka	k1gFnSc7	Rozárka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
statek	statek	k1gInSc4	statek
dorazí	dorazit	k5eAaPmIp3nS	dorazit
královská	královský	k2eAgFnSc1d1	královská
družina	družina	k1gFnSc1	družina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
princ	princ	k1gMnSc1	princ
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Statkářka	statkářka	k1gFnSc1	statkářka
je	být	k5eAaImIp3nS	být
zklamaná	zklamaný	k2eAgFnSc1d1	zklamaná
z	z	k7c2	z
princovy	princův	k2eAgFnSc2d1	princova
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
a	a	k8xC	a
vyprosí	vyprosit	k5eAaPmIp3nS	vyprosit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
král	král	k1gMnSc1	král
pozval	pozvat	k5eAaPmAgMnS	pozvat
na	na	k7c4	na
velkolepý	velkolepý	k2eAgInSc4d1	velkolepý
ples	ples	k1gInSc4	ples
do	do	k7c2	do
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
panovník	panovník	k1gMnSc1	panovník
plánuje	plánovat	k5eAaImIp3nS	plánovat
sezvat	sezvat	k5eAaPmF	sezvat
nezadané	zadaný	k2eNgFnPc4d1	nezadaná
dívky	dívka	k1gFnPc4	dívka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
chce	chtít	k5eAaImIp3nS	chtít
syna	syn	k1gMnSc4	syn
oženit	oženit	k5eAaPmF	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princa	k1gFnPc2	princa
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
pobočníky	pobočník	k1gMnPc7	pobočník
utekl	utéct	k5eAaPmAgMnS	utéct
na	na	k7c4	na
lov	lov	k1gInSc4	lov
do	do	k7c2	do
zasněženého	zasněžený	k2eAgInSc2d1	zasněžený
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gFnSc1	Popelka
mladíky	mladík	k1gMnPc4	mladík
spatří	spatřit	k5eAaPmIp3nS	spatřit
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
sledovat	sledovat	k5eAaImF	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
míří	mířit	k5eAaImIp3nS	mířit
kuší	kuše	k1gFnSc7	kuše
na	na	k7c4	na
laň	laň	k1gFnSc4	laň
<g/>
,	,	kIx,	,
Popelka	Popelka	k1gFnSc1	Popelka
mu	on	k3xPp3gMnSc3	on
do	do	k7c2	do
obličeje	obličej	k1gInSc2	obličej
hodí	hodit	k5eAaPmIp3nS	hodit
sněhovou	sněhový	k2eAgFnSc4d1	sněhová
kouli	koule	k1gFnSc4	koule
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
střelu	střela	k1gFnSc4	střela
překazila	překazit	k5eAaPmAgFnS	překazit
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
s	s	k7c7	s
družinou	družina	k1gFnSc7	družina
začnou	začít	k5eAaPmIp3nP	začít
dívku	dívka	k1gFnSc4	dívka
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jim	on	k3xPp3gMnPc3	on
utíká	utíkat	k5eAaImIp3nS	utíkat
mezi	mezi	k7c7	mezi
závějemi	závěj	k1gFnPc7	závěj
<g/>
,	,	kIx,	,
schovává	schovávat	k5eAaImIp3nS	schovávat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
skalním	skalní	k2eAgInSc7d1	skalní
převisem	převis	k1gInSc7	převis
a	a	k8xC	a
mezi	mezi	k7c7	mezi
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
<s>
Urození	urozený	k2eAgMnPc1d1	urozený
mladíci	mladík	k1gMnPc1	mladík
využijí	využít	k5eAaPmIp3nP	využít
početní	početní	k2eAgFnPc4d1	početní
převahy	převaha	k1gFnPc4	převaha
<g/>
,	,	kIx,	,
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
se	se	k3xPyFc4	se
a	a	k8xC	a
Popelku	Popelka	k1gMnSc4	Popelka
obklíčí	obklíčit	k5eAaPmIp3nS	obklíčit
<g/>
.	.	kIx.	.
</s>
<s>
Utahují	utahovat	k5eAaImIp3nP	utahovat
si	se	k3xPyFc3	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
holka	holka	k1gFnSc1	holka
<g/>
,	,	kIx,	,
neopeřené	opeřený	k2eNgNnSc1d1	neopeřené
kuře	kuře	k1gNnSc1	kuře
<g/>
,	,	kIx,	,
cácorka	cácorka	k1gFnSc1	cácorka
a	a	k8xC	a
vyhrožují	vyhrožovat	k5eAaImIp3nP	vyhrožovat
jí	on	k3xPp3gFnSc3	on
trestem	trest	k1gInSc7	trest
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
jim	on	k3xPp3gMnPc3	on
Popelka	Popelka	k1gFnSc1	Popelka
drze	drze	k6eAd1	drze
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Dívce	dívka	k1gFnSc3	dívka
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
utéct	utéct	k5eAaPmF	utéct
<g/>
,	,	kIx,	,
nasedne	nasednout	k5eAaPmIp3nS	nasednout
na	na	k7c4	na
princova	princův	k2eAgMnSc4d1	princův
nezkrotného	zkrotný	k2eNgMnSc4d1	nezkrotný
grošáka	grošák	k1gMnSc4	grošák
a	a	k8xC	a
ujede	ujet	k5eAaPmIp3nS	ujet
jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Přesedlá	přesedlat	k5eAaPmIp3nS	přesedlat
na	na	k7c4	na
Juráška	Jurášek	k1gMnSc4	Jurášek
a	a	k8xC	a
grošáka	grošák	k1gMnSc4	grošák
pošle	poslat	k5eAaPmIp3nS	poslat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Urozené	urozený	k2eAgMnPc4d1	urozený
mladíky	mladík	k1gMnPc4	mladík
zatím	zatím	k6eAd1	zatím
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
preceptor	preceptor	k1gMnSc1	preceptor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	on	k3xPp3gNnSc4	on
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
králova	králův	k2eAgInSc2d1	králův
příkazu	příkaz	k1gInSc2	příkaz
dovést	dovést	k5eAaPmF	dovést
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
syna	syn	k1gMnSc2	syn
kárá	kárat	k5eAaImIp3nS	kárat
a	a	k8xC	a
vyhrožuje	vyhrožovat	k5eAaImIp3nS	vyhrožovat
mu	on	k3xPp3gMnSc3	on
svatbou	svatba	k1gFnSc7	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Statkářka	statkářka	k1gFnSc1	statkářka
pošle	poslat	k5eAaPmIp3nS	poslat
podkoního	podkoní	k1gMnSc4	podkoní
Vincka	Vincek	k1gMnSc4	Vincek
do	do	k7c2	do
města	město	k1gNnSc2	město
s	s	k7c7	s
povozem	povoz	k1gInSc7	povoz
nakoupit	nakoupit	k5eAaPmF	nakoupit
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
cestou	cesta	k1gFnSc7	cesta
uvidí	uvidět	k5eAaPmIp3nS	uvidět
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
macechy	macecha	k1gFnSc2	macecha
v	v	k7c6	v
potoce	potok	k1gInSc6	potok
pere	prát	k5eAaImIp3nS	prát
prádlo	prádlo	k1gNnSc1	prádlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
co	co	k8xS	co
jí	on	k3xPp3gFnSc3	on
má	mít	k5eAaImIp3nS	mít
dovézt	dovézt	k5eAaPmF	dovézt
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
skromně	skromně	k6eAd1	skromně
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc7	on
přivezl	přivézt	k5eAaPmAgMnS	přivézt
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
mu	on	k3xPp3gMnSc3	on
cestou	cesta	k1gFnSc7	cesta
cvrkne	cvrknout	k5eAaPmIp3nS	cvrknout
do	do	k7c2	do
nosu	nos	k1gInSc2	nos
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
zámek	zámek	k1gInSc1	zámek
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c4	na
ples	ples	k1gInSc4	ples
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
raději	rád	k6eAd2	rád
uteče	utéct	k5eAaPmIp3nS	utéct
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uslyší	uslyšet	k5eAaPmIp3nP	uslyšet
cinkající	cinkající	k2eAgFnPc4d1	cinkající
rolničky	rolnička	k1gFnPc4	rolnička
z	z	k7c2	z
projíždějícího	projíždějící	k2eAgInSc2d1	projíždějící
povozu	povoz	k1gInSc2	povoz
dřímajícího	dřímající	k2eAgMnSc4d1	dřímající
Vincka	Vincek	k1gMnSc4	Vincek
<g/>
.	.	kIx.	.
</s>
<s>
Sestřelí	sestřelit	k5eAaPmIp3nS	sestřelit
opuštěné	opuštěný	k2eAgNnSc1d1	opuštěné
ptačí	ptačí	k2eAgNnSc1d1	ptačí
hnízdo	hnízdo	k1gNnSc1	hnízdo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spadlo	spadnout	k5eAaPmAgNnS	spadnout
podkonímu	podkoní	k1gMnSc3	podkoní
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
probudí	probudit	k5eAaPmIp3nS	probudit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
najde	najít	k5eAaPmIp3nS	najít
tři	tři	k4xCgInPc4	tři
lískové	lískový	k2eAgInPc4d1	lískový
oříšky	oříšek	k1gInPc4	oříšek
a	a	k8xC	a
vzpomene	vzpomenout	k5eAaPmIp3nS	vzpomenout
si	se	k3xPyFc3	se
na	na	k7c4	na
slib	slib	k1gInSc4	slib
Popelce	popelec	k1gInSc2	popelec
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
z	z	k7c2	z
dárku	dárek	k1gInSc2	dárek
raduje	radovat	k5eAaImIp3nS	radovat
a	a	k8xC	a
uschová	uschovat	k5eAaPmIp3nS	uschovat
jej	on	k3xPp3gNnSc4	on
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgInPc4	svůj
poklady	poklad	k1gInPc4	poklad
k	k	k7c3	k
sově	sova	k1gFnSc3	sova
Rozárce	Rozárka	k1gFnSc3	Rozárka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
svěří	svěřit	k5eAaPmIp3nS	svěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ráda	rád	k2eAgFnSc1d1	ráda
podívala	podívat	k5eAaImAgFnS	podívat
za	za	k7c7	za
princem	princ	k1gMnSc7	princ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
schází	scházet	k5eAaImIp3nS	scházet
jí	on	k3xPp3gFnSc3	on
vhodné	vhodný	k2eAgNnSc1d1	vhodné
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
oříšků	oříšek	k1gMnPc2	oříšek
upadne	upadnout	k5eAaPmIp3nS	upadnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
nakřápne	nakřápnout	k5eAaPmIp3nS	nakřápnout
se	se	k3xPyFc4	se
a	a	k8xC	a
po	po	k7c6	po
rozlousknutí	rozlousknutí	k1gNnSc6	rozlousknutí
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
lovecký	lovecký	k2eAgInSc1d1	lovecký
komplet	komplet	k1gInSc1	komplet
s	s	k7c7	s
kamizolou	kamizola	k1gFnSc7	kamizola
a	a	k8xC	a
klobouk	klobouk	k1gInSc1	klobouk
s	s	k7c7	s
pérem	péro	k1gNnSc7	péro
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
se	se	k3xPyFc4	se
převlékne	převléknout	k5eAaPmIp3nS	převléknout
a	a	k8xC	a
vyrazí	vyrazit	k5eAaPmIp3nS	vyrazit
na	na	k7c4	na
projížďku	projížďka	k1gFnSc4	projížďka
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
královský	královský	k2eAgInSc4d1	královský
hon	hon	k1gInSc4	hon
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
skolí	skolit	k5eAaPmIp3nS	skolit
lišku	liška	k1gFnSc4	liška
a	a	k8xC	a
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
titulu	titul	k1gInSc2	titul
krále	král	k1gMnSc2	král
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
krásného	krásný	k2eAgInSc2d1	krásný
prstenu	prsten	k1gInSc2	prsten
z	z	k7c2	z
královské	královský	k2eAgFnSc2d1	královská
pokladnice	pokladnice	k1gFnSc2	pokladnice
musí	muset	k5eAaImIp3nS	muset
zastřelit	zastřelit	k5eAaPmF	zastřelit
ještě	ještě	k6eAd1	ještě
dravého	dravý	k2eAgMnSc4d1	dravý
ptáka	pták	k1gMnSc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
takový	takový	k3xDgMnSc1	takový
krouží	kroužit	k5eAaImIp3nS	kroužit
nad	nad	k7c7	nad
lesem	les	k1gInSc7	les
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
rány	rána	k1gFnPc4	rána
vystřelí	vystřelit	k5eAaPmIp3nP	vystřelit
princovi	princův	k2eAgMnPc1d1	princův
souputníci	souputník	k1gMnPc1	souputník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
netrefí	trefit	k5eNaPmIp3nS	trefit
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
šance	šance	k1gFnSc2	šance
vzdá	vzdát	k5eAaPmIp3nS	vzdát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pták	pták	k1gMnSc1	pták
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vysoko	vysoko	k6eAd1	vysoko
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
však	však	k9	však
dravce	dravec	k1gMnSc4	dravec
sestřelí	sestřelit	k5eAaPmIp3nS	sestřelit
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
padne	padnout	k5eAaImIp3nS	padnout
k	k	k7c3	k
mladíkovým	mladíkův	k2eAgFnPc3d1	mladíkova
nohám	noha	k1gFnPc3	noha
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
Popelku	Popelka	k1gFnSc4	Popelka
nepozná	poznat	k5eNaPmIp3nS	poznat
a	a	k8xC	a
navlékne	navléknout	k5eAaPmIp3nS	navléknout
jí	on	k3xPp3gFnSc7	on
vyhraný	vyhraný	k2eAgInSc1d1	vyhraný
prsten	prsten	k1gInSc1	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
své	svůj	k3xOyFgNnSc4	svůj
střelecké	střelecký	k2eAgNnSc4d1	střelecké
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
sestřelí	sestřelit	k5eAaPmIp3nS	sestřelit
šišku	šiška	k1gFnSc4	šiška
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
stromu	strom	k1gInSc2	strom
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
dokáže	dokázat	k5eAaPmIp3nS	dokázat
malá	malý	k2eAgFnSc1d1	malá
holka	holka	k1gFnSc1	holka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
uteče	utéct	k5eAaPmIp3nS	utéct
a	a	k8xC	a
převlékne	převléknout	k5eAaPmIp3nS	převléknout
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
domnělého	domnělý	k2eAgMnSc2d1	domnělý
myslivce	myslivec	k1gMnSc2	myslivec
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
narazí	narazit	k5eAaPmIp3nS	narazit
jen	jen	k9	jen
na	na	k7c4	na
umouněnou	umouněný	k2eAgFnSc4d1	umouněná
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
macechu	macecha	k1gFnSc4	macecha
s	s	k7c7	s
nevlastní	vlastní	k2eNgFnSc7d1	nevlastní
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
chystají	chystat	k5eAaImIp3nP	chystat
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
ples	ples	k1gInSc4	ples
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
prosí	prosit	k5eAaImIp3nS	prosit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
jet	jet	k5eAaImF	jet
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
podívat	podívat	k5eAaImF	podívat
se	se	k3xPyFc4	se
do	do	k7c2	do
zámku	zámek	k1gInSc2	zámek
alespoň	alespoň	k9	alespoň
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
vysmějí	vysmát	k5eAaPmIp3nP	vysmát
a	a	k8xC	a
statkářka	statkářka	k1gFnSc1	statkářka
jí	on	k3xPp3gFnSc3	on
opět	opět	k6eAd1	opět
zaúkoluje	zaúkolovat	k5eAaPmIp3nS	zaúkolovat
tříděním	třídění	k1gNnSc7	třídění
čočky	čočka	k1gFnSc2	čočka
a	a	k8xC	a
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nesmyslným	smyslný	k2eNgInSc7d1	nesmyslný
trestem	trest	k1gInSc7	trest
pomohou	pomoct	k5eAaPmIp3nP	pomoct
holoubci	holoubek	k1gMnPc1	holoubek
a	a	k8xC	a
Popelka	Popelka	k1gMnSc1	Popelka
si	se	k3xPyFc3	se
jde	jít	k5eAaImIp3nS	jít
postěžovat	postěžovat	k5eAaPmF	postěžovat
Rozárce	Rozárka	k1gFnSc3	Rozárka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	jíst	k5eAaImIp3nS	jíst
maminka	maminka	k1gFnSc1	maminka
slibovala	slibovat	k5eAaImAgFnS	slibovat
šaty	šat	k1gInPc4	šat
růžové	růžový	k2eAgInPc4d1	růžový
jako	jako	k9	jako
červánky	červánek	k1gInPc4	červánek
se	s	k7c7	s
závojem	závoj	k1gInSc7	závoj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Rozárčinu	Rozárčin	k2eAgFnSc4d1	Rozárčina
radu	rada	k1gFnSc4	rada
dívka	dívka	k1gFnSc1	dívka
hodí	hodit	k5eAaImIp3nS	hodit
druhým	druhý	k4xOgInSc7	druhý
oříškem	oříšek	k1gInSc7	oříšek
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
vyklouznou	vyklouznout	k5eAaPmIp3nP	vyklouznout
krásné	krásný	k2eAgInPc4d1	krásný
plesové	plesový	k2eAgInPc4d1	plesový
šaty	šat	k1gInPc4	šat
<g/>
.	.	kIx.	.
</s>
<s>
Dole	dole	k6eAd1	dole
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
čeká	čekat	k5eAaImIp3nS	čekat
již	již	k6eAd1	již
osedlaný	osedlaný	k2eAgMnSc1d1	osedlaný
Jurášek	Jurášek	k1gMnSc1	Jurášek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
plesu	ples	k1gInSc2	ples
preceptor	preceptor	k1gMnSc1	preceptor
královské	královský	k2eAgFnSc3d1	královská
rodině	rodina	k1gFnSc3	rodina
přestavuje	přestavovat	k5eAaImIp3nS	přestavovat
vdavekchtivé	vdavekchtivý	k2eAgFnPc4d1	vdavekchtivá
dívky	dívka	k1gFnPc4	dívka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
matky	matka	k1gFnPc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Princovi	princ	k1gMnSc3	princ
se	se	k3xPyFc4	se
do	do	k7c2	do
ženění	ženění	k1gNnSc2	ženění
ani	ani	k8xC	ani
tance	tanec	k1gInSc2	tanec
příliš	příliš	k6eAd1	příliš
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otcovu	otcův	k2eAgFnSc4d1	otcova
výzvu	výzva	k1gFnSc4	výzva
si	se	k3xPyFc3	se
poslepu	poslepu	k6eAd1	poslepu
vybere	vybrat	k5eAaPmIp3nS	vybrat
tanečnici	tanečnice	k1gFnSc4	tanečnice
-	-	kIx~	-
Popelčinu	Popelčin	k2eAgFnSc4d1	Popelčina
nevlastní	vlastní	k2eNgFnSc4d1	nevlastní
sestru	sestra	k1gFnSc4	sestra
Doru	Dora	k1gFnSc4	Dora
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
však	však	k9	však
předběhne	předběhnout	k5eAaPmIp3nS	předběhnout
rázná	rázný	k2eAgFnSc1d1	rázná
dcera	dcera	k1gFnSc1	dcera
paní	paní	k1gFnSc1	paní
z	z	k7c2	z
Outlova	Outlův	k2eAgNnSc2d1	Outlův
<g/>
,	,	kIx,	,
Droběna	Droběna	k1gFnSc1	Droběna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
s	s	k7c7	s
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
tančí	tančit	k5eAaImIp3nS	tančit
velmi	velmi	k6eAd1	velmi
svérázně	svérázně	k6eAd1	svérázně
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gFnSc1	Popelka
přijela	přijet	k5eAaPmAgFnS	přijet
k	k	k7c3	k
zámku	zámek	k1gInSc3	zámek
na	na	k7c6	na
Juráškovi	Jurášek	k1gMnSc6	Jurášek
a	a	k8xC	a
oknem	okno	k1gNnSc7	okno
zahlédne	zahlédnout	k5eAaPmIp3nS	zahlédnout
prince	princ	k1gMnSc2	princ
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zrovna	zrovna	k6eAd1	zrovna
tančí	tančit	k5eAaImIp3nS	tančit
s	s	k7c7	s
Dorou	Dora	k1gFnSc7	Dora
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
dívku	dívka	k1gFnSc4	dívka
zamrzí	zamrzet	k5eAaPmIp3nS	zamrzet
a	a	k8xC	a
váhá	váhat	k5eAaImIp3nS	váhat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
ples	ples	k1gInSc4	ples
vůbec	vůbec	k9	vůbec
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dívku	dívka	k1gFnSc4	dívka
svého	svůj	k3xOyFgNnSc2	svůj
srdce	srdce	k1gNnSc2	srdce
mezi	mezi	k7c7	mezi
účastnicemi	účastnice	k1gFnPc7	účastnice
ne	ne	k9	ne
a	a	k8xC	a
ne	ne	k9	ne
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
<s>
Pohádá	pohádat	k5eAaPmIp3nS	pohádat
se	se	k3xPyFc4	se
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
odejít	odejít	k5eAaPmF	odejít
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc4	ten
bude	být	k5eAaImBp3nS	být
raději	rád	k6eAd2	rád
kácet	kácet	k5eAaImF	kácet
stromy	strom	k1gInPc4	strom
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zrovna	zrovna	k6eAd1	zrovna
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
princ	princ	k1gMnSc1	princ
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
do	do	k7c2	do
sálu	sál	k1gInSc2	sál
vkročí	vkročit	k5eAaPmIp3nS	vkročit
Popelka	Popelka	k1gFnSc1	Popelka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
závojem	závoj	k1gInSc7	závoj
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
poznáním	poznání	k1gNnSc7	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Vyčiní	vyčinit	k5eAaPmIp3nP	vyčinit
princi	princa	k1gFnPc1	princa
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
dosud	dosud	k6eAd1	dosud
nepřivítal	přivítat	k5eNaPmAgMnS	přivítat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechce	chtít	k5eNaImIp3nS	chtít
mu	on	k3xPp3gMnSc3	on
stát	stát	k1gInSc4	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
když	když	k8xS	když
právě	právě	k6eAd1	právě
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
se	se	k3xPyFc4	se
zahanbeně	zahanbeně	k6eAd1	zahanbeně
omlouvá	omlouvat	k5eAaImIp3nS	omlouvat
<g/>
,	,	kIx,	,
Popelka	Popelka	k1gFnSc1	Popelka
jej	on	k3xPp3gInSc4	on
okouzlila	okouzlit	k5eAaPmAgFnS	okouzlit
<g/>
.	.	kIx.	.
</s>
<s>
Vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
tanci	tanec	k1gInSc3	tanec
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
odhalit	odhalit	k5eAaPmF	odhalit
identitu	identita	k1gFnSc4	identita
tajemné	tajemný	k2eAgFnSc2d1	tajemná
krásky	kráska	k1gFnSc2	kráska
a	a	k8xC	a
oznámí	oznámit	k5eAaPmIp3nP	oznámit
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
troufale	troufale	k6eAd1	troufale
odvětí	odvětit	k5eAaPmIp3nS	odvětit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nezeptal	zeptat	k5eNaPmAgMnS	zeptat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
ho	on	k3xPp3gNnSc4	on
vůbec	vůbec	k9	vůbec
chtěla	chtít	k5eAaImAgFnS	chtít
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
mu	on	k3xPp3gMnSc3	on
hádanku	hádanka	k1gFnSc4	hádanka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
poprvé	poprvé	k6eAd1	poprvé
<g/>
:	:	kIx,	:
Tváře	tvář	k1gFnPc1	tvář
umouněné	umouněný	k2eAgFnPc1d1	umouněná
od	od	k7c2	od
popela	popel	k1gInSc2	popel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kominík	kominík	k1gMnSc1	kominík
to	ten	k3xDgNnSc4	ten
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
<g/>
:	:	kIx,	:
klobouk	klobouk	k1gInSc1	klobouk
s	s	k7c7	s
peřím	peří	k1gNnSc7	peří
<g/>
,	,	kIx,	,
luk	luk	k1gInSc1	luk
a	a	k8xC	a
kamizola	kamizola	k1gFnSc1	kamizola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
myslivec	myslivec	k1gMnSc1	myslivec
to	ten	k3xDgNnSc4	ten
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
potřetí	potřetí	k4xO	potřetí
<g/>
:	:	kIx,	:
šaty	šat	k1gInPc1	šat
s	s	k7c7	s
vlečkou	vlečka	k1gFnSc7	vlečka
<g/>
,	,	kIx,	,
stříbrem	stříbro	k1gNnSc7	stříbro
vyšívané	vyšívaný	k2eAgFnSc2d1	vyšívaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
princezna	princezna	k1gFnSc1	princezna
to	ten	k3xDgNnSc4	ten
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
jasný	jasný	k2eAgInSc1d1	jasný
pane	pan	k1gMnSc5	pan
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Princ	princ	k1gMnSc1	princ
si	se	k3xPyFc3	se
neví	vědět	k5eNaImIp3nS	vědět
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
Popelka	Popelka	k1gMnSc1	Popelka
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
proto	proto	k8xC	proto
rozloučí	rozloučit	k5eAaPmIp3nP	rozloučit
a	a	k8xC	a
prchne	prchnout	k5eAaPmIp3nS	prchnout
z	z	k7c2	z
tanečního	taneční	k2eAgInSc2d1	taneční
sálu	sál	k1gInSc2	sál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
schodech	schod	k1gInPc6	schod
ztratí	ztratit	k5eAaPmIp3nS	ztratit
střevíček	střevíček	k1gInSc1	střevíček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
najde	najít	k5eAaPmIp3nS	najít
princ	princ	k1gMnSc1	princ
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dívku	dívka	k1gFnSc4	dívka
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
pobočníky	pobočník	k1gMnPc7	pobočník
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
až	až	k9	až
ke	k	k7c3	k
statku	statek	k1gInSc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Princova	princův	k2eAgFnSc1d1	princova
družina	družina	k1gFnSc1	družina
buší	bušit	k5eAaImIp3nS	bušit
na	na	k7c4	na
vrata	vrata	k1gNnPc4	vrata
a	a	k8xC	a
když	když	k8xS	když
nikdo	nikdo	k3yNnSc1	nikdo
neotvírá	otvírat	k5eNaImIp3nS	otvírat
<g/>
,	,	kIx,	,
vlomí	vlomit	k5eAaPmIp3nS	vlomit
se	se	k3xPyFc4	se
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Služebnictvu	služebnictvo	k1gNnSc3	služebnictvo
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
hledá	hledat	k5eAaImIp3nS	hledat
princeznu	princezna	k1gFnSc4	princezna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
před	před	k7c7	před
chvílí	chvíle	k1gFnSc7	chvíle
přijela	přijet	k5eAaPmAgFnS	přijet
<g/>
.	.	kIx.	.
</s>
<s>
Poddaní	poddaný	k1gMnPc1	poddaný
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vysmějí	vysmát	k5eAaPmIp3nP	vysmát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
žádná	žádný	k3yNgFnSc1	žádný
princezna	princezna	k1gFnSc1	princezna
na	na	k7c6	na
statku	statek	k1gInSc6	statek
přece	přece	k9	přece
nežije	žít	k5eNaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
svém	své	k1gNnSc6	své
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
vidět	vidět	k5eAaImF	vidět
všechny	všechen	k3xTgFnPc4	všechen
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
dívky	dívka	k1gFnPc4	dívka
a	a	k8xC	a
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
jim	on	k3xPp3gMnPc3	on
nalezený	nalezený	k2eAgInSc4d1	nalezený
střevíc	střevíc	k1gInSc4	střevíc
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
střevíc	střevíc	k1gInSc1	střevíc
padne	padnout	k5eAaImIp3nS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Střevíček	střevíček	k1gInSc1	střevíček
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
všem	všecek	k3xTgMnPc3	všecek
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Vincka	Vincek	k1gMnSc4	Vincek
napadne	napadnout	k5eAaPmIp3nS	napadnout
přivést	přivést	k5eAaPmF	přivést
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
osazenstvo	osazenstvo	k1gNnSc1	osazenstvo
statku	statek	k1gInSc2	statek
dívku	dívka	k1gFnSc4	dívka
hledá	hledat	k5eAaImIp3nS	hledat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
najdou	najít	k5eAaPmIp3nP	najít
jen	jen	k9	jen
slavnostně	slavnostně	k6eAd1	slavnostně
osedlaného	osedlaný	k2eAgMnSc4d1	osedlaný
Juráška	Jurášek	k1gMnSc4	Jurášek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
statek	statek	k1gInSc4	statek
přijede	přijet	k5eAaPmIp3nS	přijet
statkářka	statkářka	k1gFnSc1	statkářka
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
<g/>
,	,	kIx,	,
vypátrá	vypátrat	k5eAaPmIp3nS	vypátrat
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
,	,	kIx,	,
spoutá	spoutat	k5eAaPmIp3nS	spoutat
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
sebere	sebrat	k5eAaPmIp3nS	sebrat
její	její	k3xOp3gInPc4	její
šaty	šat	k1gInPc4	šat
<g/>
,	,	kIx,	,
do	do	k7c2	do
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
obleče	obléct	k5eAaPmIp3nS	obléct
Dora	Dora	k1gFnSc1	Dora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
nasednou	nasednout	k5eAaPmIp3nP	nasednout
do	do	k7c2	do
saní	saně	k1gFnPc2	saně
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
chce	chtít	k5eAaImIp3nS	chtít
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
dívce	dívka	k1gFnSc3	dívka
v	v	k7c6	v
růžových	růžový	k2eAgInPc6d1	růžový
šatech	šat	k1gInPc6	šat
střevíc	střevíc	k1gInSc4	střevíc
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
statkářka	statkářka	k1gFnSc1	statkářka
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
,	,	kIx,	,
vytrhne	vytrhnout	k5eAaPmIp3nS	vytrhnout
mu	on	k3xPp3gMnSc3	on
střevíček	střevíček	k1gInSc1	střevíček
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
<g/>
.	.	kIx.	.
</s>
<s>
Urozený	urozený	k2eAgMnSc1d1	urozený
mladík	mladík	k1gMnSc1	mladík
je	on	k3xPp3gNnSc4	on
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
,	,	kIx,	,
sáně	sáně	k1gFnPc1	sáně
spadnou	spadnout	k5eAaPmIp3nP	spadnout
do	do	k7c2	do
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
oběma	dva	k4xCgFnPc3	dva
ženám	žena	k1gFnPc3	žena
snaží	snažit	k5eAaImIp3nS	snažit
pomoci	pomoct	k5eAaPmF	pomoct
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Popelčiných	Popelčin	k2eAgInPc6d1	Popelčin
šatech	šat	k1gInPc6	šat
však	však	k9	však
pozná	poznat	k5eAaPmIp3nS	poznat
Doru	Dora	k1gFnSc4	Dora
a	a	k8xC	a
uhání	uhánět	k5eAaImIp3nS	uhánět
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
statek	statek	k1gInSc4	statek
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gFnSc1	Popelka
se	se	k3xPyFc4	se
sama	sám	k3xTgMnSc4	sám
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
a	a	k8xC	a
odejde	odejít	k5eAaPmIp3nS	odejít
k	k	k7c3	k
Rozárce	Rozárka	k1gFnSc3	Rozárka
<g/>
,	,	kIx,	,
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
hodí	hodit	k5eAaImIp3nS	hodit
poslední	poslední	k2eAgInSc1d1	poslední
oříšek	oříšek	k1gInSc1	oříšek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
vyloupnou	vyloupnout	k5eAaPmIp3nP	vyloupnout
svatební	svatební	k2eAgInPc1d1	svatební
šaty	šat	k1gInPc1	šat
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
se	se	k3xPyFc4	se
za	za	k7c2	za
svítání	svítání	k1gNnSc2	svítání
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
usedlosti	usedlost	k1gFnSc3	usedlost
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
i	i	k9	i
Popelka	Popelka	k1gFnSc1	Popelka
na	na	k7c6	na
Juráškovi	Jurášek	k1gMnSc6	Jurášek
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
děvčeti	děvče	k1gNnSc3	děvče
nasadí	nasadit	k5eAaPmIp3nS	nasadit
střevíc	střevíc	k1gInSc1	střevíc
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jí	on	k3xPp3gFnSc3	on
padne	padnout	k5eAaPmIp3nS	padnout
jako	jako	k9	jako
ulitý	ulitý	k2eAgMnSc1d1	ulitý
<g/>
.	.	kIx.	.
</s>
<s>
Chasa	chasa	k1gFnSc1	chasa
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
rozluštit	rozluštit	k5eAaPmF	rozluštit
Popelčinu	Popelčin	k2eAgFnSc4d1	Popelčina
hádanku	hádanka	k1gFnSc4	hádanka
<g/>
.	.	kIx.	.
</s>
<s>
Zamilovaná	zamilovaný	k2eAgFnSc1d1	zamilovaná
dvojice	dvojice	k1gFnSc1	dvojice
opustí	opustit	k5eAaPmIp3nS	opustit
zdi	zeď	k1gFnSc3	zeď
statku	statek	k1gInSc2	statek
a	a	k8xC	a
ujíždí	ujíždět	k5eAaImIp3nS	ujíždět
zasněženými	zasněžený	k2eAgFnPc7d1	zasněžená
pláněmi	pláň	k1gFnPc7	pláň
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc4	scénář
napsal	napsat	k5eAaPmAgMnS	napsat
František	František	k1gMnSc1	František
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
autorka	autorka	k1gFnSc1	autorka
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
Bohumila	Bohumila	k1gFnSc1	Bohumila
Zelenková	Zelenková	k1gFnSc1	Zelenková
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kryla	krýt	k5eAaImAgFnS	krýt
pravého	pravý	k2eAgMnSc4d1	pravý
scenáristu	scenárista	k1gMnSc4	scenárista
Františka	František	k1gMnSc4	František
Pavlíčka	Pavlíček	k1gMnSc4	Pavlíček
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
komunisté	komunista	k1gMnPc1	komunista
nedovolili	dovolit	k5eNaPmAgMnP	dovolit
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
námětu	námět	k1gInSc2	námět
pohádky	pohádka	k1gFnSc2	pohádka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
O	o	k7c6	o
Popelce	Popelka	k1gFnSc6	Popelka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
scenárista	scenárista	k1gMnSc1	scenárista
se	se	k3xPyFc4	se
původní	původní	k2eAgFnSc2d1	původní
předlohy	předloha	k1gFnSc2	předloha
příliš	příliš	k6eAd1	příliš
nedržel	držet	k5eNaImAgMnS	držet
<g/>
.	.	kIx.	.
</s>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
české	český	k2eAgFnSc2d1	Česká
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
hororově	hororově	k6eAd1	hororově
laděná	laděný	k2eAgFnSc1d1	laděná
a	a	k8xC	a
Popelka	Popelka	k1gFnSc1	Popelka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ztvárněna	ztvárnit	k5eAaPmNgFnS	ztvárnit
tradičně	tradičně	k6eAd1	tradičně
-	-	kIx~	-
jako	jako	k8xS	jako
trpící	trpící	k2eAgFnSc1d1	trpící
<g/>
,	,	kIx,	,
unylá	unylý	k2eAgFnSc1d1	unylá
a	a	k8xC	a
špinavá	špinavý	k2eAgFnSc1d1	špinavá
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
Třech	tři	k4xCgInPc6	tři
oříšcích	oříšek	k1gInPc6	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
stala	stát	k5eAaPmAgFnS	stát
energická	energický	k2eAgFnSc1d1	energická
a	a	k8xC	a
vzpurná	vzpurný	k2eAgFnSc1d1	vzpurná
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prince	princ	k1gMnPc4	princ
dokonce	dokonce	k9	dokonce
provokuje	provokovat	k5eAaImIp3nS	provokovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
nabídku	nabídka	k1gFnSc4	nabídka
se	s	k7c7	s
scénářem	scénář	k1gInSc7	scénář
dostal	dostat	k5eAaPmAgMnS	dostat
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
patřičná	patřičný	k2eAgNnPc4d1	patřičné
povolení	povolení	k1gNnPc4	povolení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ústřední	ústřední	k2eAgMnSc1d1	ústřední
dramaturg	dramaturg	k1gMnSc1	dramaturg
Filmového	filmový	k2eAgNnSc2d1	filmové
studia	studio	k1gNnSc2	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
Ludvík	Ludvík	k1gMnSc1	Ludvík
Toman	Toman	k1gMnSc1	Toman
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
a	a	k8xC	a
scénář	scénář	k1gInSc4	scénář
zadal	zadat	k5eAaPmAgMnS	zadat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1972	[number]	k4	1972
Václavu	Václav	k1gMnSc3	Václav
Vorlíčkovi	Vorlíček	k1gMnSc3	Vorlíček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
méně	málo	k6eAd2	málo
problematického	problematický	k2eAgMnSc4d1	problematický
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
režiséra	režisér	k1gMnSc2	režisér
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
nechtěl	chtít	k5eNaImAgMnS	chtít
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c6	na
koprodukci	koprodukce	k1gFnSc6	koprodukce
s	s	k7c7	s
DEFA	DEFA	kA	DEFA
<g/>
.	.	kIx.	.
</s>
<s>
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
pohádku	pohádka	k1gFnSc4	pohádka
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
bohatou	bohatý	k2eAgFnSc4d1	bohatá
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
film	film	k1gInSc4	film
zasadit	zasadit	k5eAaPmF	zasadit
do	do	k7c2	do
období	období	k1gNnSc2	období
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
DEFA	DEFA	kA	DEFA
kývl	kývnout	k5eAaPmAgMnS	kývnout
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
více	hodně	k6eAd2	hodně
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
oříšky	oříšek	k1gInPc1	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
původně	původně	k6eAd1	původně
natáčet	natáčet	k5eAaImF	natáčet
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrával	odehrávat	k5eAaImAgInS	odehrávat
mezi	mezi	k7c7	mezi
sytě	sytě	k6eAd1	sytě
zelenými	zelený	k2eAgInPc7d1	zelený
stromy	strom	k1gInPc7	strom
a	a	k8xC	a
zurčícími	zurčící	k2eAgInPc7d1	zurčící
potůčky	potůček	k1gInPc7	potůček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1972	[number]	k4	1972
však	však	k9	však
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
přijeli	přijet	k5eAaPmAgMnP	přijet
zástupci	zástupce	k1gMnPc1	zástupce
DEFA	DEFA	kA	DEFA
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
prosinec	prosinec	k1gInSc4	prosinec
a	a	k8xC	a
leden	leden	k1gInSc4	leden
volných	volný	k2eAgInPc2d1	volný
800	[number]	k4	800
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
na	na	k7c4	na
nabídku	nabídka	k1gFnSc4	nabídka
kývl	kývnout	k5eAaPmAgMnS	kývnout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
variantu	varianta	k1gFnSc4	varianta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
exteriéry	exteriér	k1gInPc7	exteriér
natočily	natočit	k5eAaBmAgFnP	natočit
až	až	k9	až
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mohlo	moct	k5eAaImAgNnS	moct
něco	něco	k3yInSc1	něco
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
pohádku	pohádka	k1gFnSc4	pohádka
zasadit	zasadit	k5eAaPmF	zasadit
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
představoval	představovat	k5eAaImAgInS	představovat
obraz	obraz	k1gInSc1	obraz
vlámského	vlámský	k2eAgMnSc2d1	vlámský
malíře	malíř	k1gMnSc2	malíř
Pietera	Pieter	k1gMnSc2	Pieter
Brueghela	Brueghela	k1gFnSc1	Brueghela
<g/>
,	,	kIx,	,
Zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
Zimě	zima	k1gFnSc3	zima
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
scénář	scénář	k1gInSc4	scénář
i	i	k8xC	i
vtípky	vtípek	k1gInPc4	vtípek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dokončeného	dokončený	k2eAgInSc2d1	dokončený
scénáře	scénář	k1gInSc2	scénář
byla	být	k5eAaImAgFnS	být
také	také	k9	také
přidána	přidán	k2eAgFnSc1d1	přidána
postava	postava	k1gFnSc1	postava
preceptora	preceptor	k1gMnSc2	preceptor
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zimní	zimní	k2eAgNnSc1d1	zimní
prostředí	prostředí	k1gNnSc1	prostředí
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
Karla	Karel	k1gMnSc2	Karel
Svobody	Svoboda	k1gMnSc2	Svoboda
se	s	k7c7	s
zvonečky	zvoneček	k1gInPc7	zvoneček
dodaly	dodat	k5eAaPmAgFnP	dodat
filmu	film	k1gInSc2	film
vánoční	vánoční	k2eAgFnSc4d1	vánoční
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obsazení	obsazení	k1gNnSc6	obsazení
obou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
probíhaly	probíhat	k5eAaImAgFnP	probíhat
po	po	k7c6	po
Československu	Československo	k1gNnSc6	Československo
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
konkurzy	konkurz	k1gInPc4	konkurz
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
Popelku	Popelka	k1gFnSc4	Popelka
hrát	hrát	k5eAaImF	hrát
blondýnka	blondýnka	k1gFnSc1	blondýnka
Jana	Jana	k1gFnSc1	Jana
Preissová	Preissová	k1gFnSc1	Preissová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
roli	role	k1gFnSc4	role
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
těhotenství	těhotenství	k1gNnSc3	těhotenství
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
někdo	někdo	k3yInSc1	někdo
ze	z	k7c2	z
štábu	štáb	k1gInSc2	štáb
vzpomněl	vzpomnít	k5eAaPmAgMnS	vzpomnít
na	na	k7c6	na
Libuši	Libuše	k1gFnSc6	Libuše
Šafránkovou	Šafránková	k1gFnSc4	Šafránková
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Babička	babička	k1gFnSc1	babička
hraje	hrát	k5eAaImIp3nS	hrát
Barunku	Barunka	k1gFnSc4	Barunka
<g/>
.	.	kIx.	.
</s>
<s>
Šafránková	Šafránková	k1gFnSc1	Šafránková
na	na	k7c4	na
roli	role	k1gFnSc4	role
ihned	ihned	k6eAd1	ihned
kývla	kývnout	k5eAaPmAgFnS	kývnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
uměla	umět	k5eAaImAgFnS	umět
dobře	dobře	k6eAd1	dobře
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nepotřebovala	potřebovat	k5eNaImAgFnS	potřebovat
jezdecký	jezdecký	k2eAgInSc4d1	jezdecký
výcvik	výcvik	k1gInSc4	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Popelka	Popelka	k1gMnSc1	Popelka
skáče	skákat	k5eAaImIp3nS	skákat
v	v	k7c6	v
lese	les	k1gInSc6	les
přes	přes	k7c4	přes
pokácený	pokácený	k2eAgInSc4d1	pokácený
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
záskok	záskok	k1gInSc4	záskok
<g/>
.	.	kIx.	.
</s>
<s>
Herec	herec	k1gMnSc1	herec
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
prince	princ	k1gMnSc2	princ
byl	být	k5eAaImAgMnS	být
vybírán	vybírat	k5eAaImNgMnS	vybírat
na	na	k7c6	na
kamerových	kamerový	k2eAgFnPc6d1	kamerová
zkouškách	zkouška	k1gFnPc6	zkouška
<g/>
,	,	kIx,	,
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c6	o
Janu	Jan	k1gMnSc6	Jan
Hrušínském	Hrušínský	k2eAgMnSc6d1	Hrušínský
či	či	k8xC	či
Jaroslavu	Jaroslav	k1gMnSc3	Jaroslav
Drbohlavovi	Drbohlava	k1gMnSc3	Drbohlava
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
hrál	hrát	k5eAaImAgMnS	hrát
Vítka	Vítek	k1gMnSc4	Vítek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
režisér	režisér	k1gMnSc1	režisér
vybral	vybrat	k5eAaPmAgMnS	vybrat
čerstvého	čerstvý	k2eAgMnSc4d1	čerstvý
absolventa	absolvent	k1gMnSc4	absolvent
pražské	pražský	k2eAgFnSc2d1	Pražská
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
Pavla	Pavel	k1gMnSc4	Pavel
Trávníčka	Trávníček	k1gMnSc4	Trávníček
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
kamerových	kamerový	k2eAgFnPc2d1	kamerová
zkoušek	zkouška	k1gFnPc2	zkouška
účastnil	účastnit	k5eAaImAgInS	účastnit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
jej	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
znal	znát	k5eAaImAgMnS	znát
z	z	k7c2	z
absolventského	absolventský	k2eAgInSc2d1	absolventský
filmu	film	k1gInSc2	film
na	na	k7c6	na
FAMU	FAMU	kA	FAMU
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Trávníček	Trávníček	k1gMnSc1	Trávníček
hrál	hrát	k5eAaImAgMnS	hrát
vojáka	voják	k1gMnSc4	voják
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
službě	služba	k1gFnSc6	služba
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Soukupa	Soukup	k1gMnSc2	Soukup
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
namluvil	namluvit	k5eAaPmAgMnS	namluvit
Petr	Petr	k1gMnSc1	Petr
Svojtka	Svojtka	k1gMnSc1	Svojtka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
režiséra	režisér	k1gMnSc2	režisér
Vorlíčka	Vorlíček	k1gMnSc2	Vorlíček
měl	mít	k5eAaImAgInS	mít
Trávníček	trávníček	k1gInSc4	trávníček
silný	silný	k2eAgInSc1d1	silný
moravský	moravský	k2eAgInSc1d1	moravský
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
to	ten	k3xDgNnSc4	ten
však	však	k9	však
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
.	.	kIx.	.
</s>
<s>
Představitelka	představitelka	k1gFnSc1	představitelka
Dory	Dora	k1gFnSc2	Dora
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
Hlaváčová	Hlaváčová	k1gFnSc1	Hlaváčová
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
natáčení	natáčení	k1gNnSc2	natáčení
těhotná	těhotný	k2eAgFnSc1d1	těhotná
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
šaty	šat	k1gInPc1	šat
byly	být	k5eAaImAgInP	být
proto	proto	k6eAd1	proto
volnější	volný	k2eAgFnSc4d2	volnější
a	a	k8xC	a
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
macechou	macecha	k1gFnSc7	macecha
padá	padat	k5eAaImIp3nS	padat
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
točili	točit	k5eAaImAgMnP	točit
kaskadéři	kaskadér	k1gMnPc1	kaskadér
<g/>
.	.	kIx.	.
</s>
<s>
Hlaváčová	Hlaváčová	k1gFnSc1	Hlaváčová
si	se	k3xPyFc3	se
stejnou	stejný	k2eAgFnSc4d1	stejná
roli	role	k1gFnSc4	role
Popelčiny	Popelčin	k2eAgFnSc2d1	Popelčina
sestry	sestra	k1gFnSc2	sestra
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Popelka	Popelka	k1gMnSc1	Popelka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
se	se	k3xPyFc4	se
do	do	k7c2	do
filmu	film	k1gInSc2	film
snažil	snažit	k5eAaImAgMnS	snažit
dostat	dostat	k5eAaPmF	dostat
trochu	trocha	k1gFnSc4	trocha
laskavého	laskavý	k2eAgInSc2d1	laskavý
a	a	k8xC	a
přátelského	přátelský	k2eAgInSc2d1	přátelský
humoru	humor	k1gInSc2	humor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chtěl	chtít	k5eAaImAgMnS	chtít
aby	aby	kYmCp3nS	aby
zůstal	zůstat	k5eAaPmAgInS	zůstat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
koření	kořenit	k5eAaImIp3nS	kořenit
a	a	k8xC	a
ne	ne	k9	ne
základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
úlohy	úloha	k1gFnPc4	úloha
vybíral	vybírat	k5eAaImAgMnS	vybírat
komické	komický	k2eAgInPc4d1	komický
typy	typ	k1gInPc4	typ
herců	herc	k1gInPc2	herc
<g/>
.	.	kIx.	.
</s>
<s>
Podkoního	podkoní	k1gMnSc4	podkoní
Vincka	Vincek	k1gMnSc4	Vincek
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Menšík	Menšík	k1gMnSc1	Menšík
<g/>
,	,	kIx,	,
preceptora	preceptor	k1gMnSc2	preceptor
Jan	Jan	k1gMnSc1	Jan
Libíček	Libíček	k1gMnSc1	Libíček
a	a	k8xC	a
vdavekchtivou	vdavekchtivý	k2eAgFnSc4d1	vdavekchtivá
urozenou	urozený	k2eAgFnSc4d1	urozená
slečnu	slečna	k1gFnSc4	slečna
Droběnu	Droběn	k2eAgFnSc4d1	Droběn
z	z	k7c2	z
Outlova	Outlův	k2eAgFnSc1d1	Outlův
Helena	Helena	k1gFnSc1	Helena
Růžičková	Růžičková	k1gFnSc1	Růžičková
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Kleinröschen	Kleinröschen	k1gInSc1	Kleinröschen
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
růžička	růžička	k1gFnSc1	růžička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejího	její	k3xOp3gInSc2	její
komediálního	komediální	k2eAgInSc2d1	komediální
talentu	talent	k1gInSc2	talent
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
využil	využít	k5eAaPmAgMnS	využít
ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Droběna	Droběna	k1gFnSc1	Droběna
nadzvedne	nadzvednout	k5eAaPmIp3nS	nadzvednout
prince	princ	k1gMnPc4	princ
nad	nad	k7c4	nad
parket	parket	k1gInSc4	parket
a	a	k8xC	a
tančí	tančit	k5eAaImIp3nS	tančit
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jak	jak	k8xC	jak
s	s	k7c7	s
pytlem	pytel	k1gInSc7	pytel
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Trávníček	trávníček	k1gInSc1	trávníček
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
úmyslu	úmysl	k1gInSc6	úmysl
nevěděl	vědět	k5eNaImAgMnS	vědět
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gInSc1	jeho
zoufalý	zoufalý	k2eAgInSc1d1	zoufalý
výraz	výraz	k1gInSc1	výraz
není	být	k5eNaImIp3nS	být
hraný	hraný	k2eAgInSc1d1	hraný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
koprodukční	koprodukční	k2eAgFnSc2d1	koprodukční
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
DEFA	DEFA	kA	DEFA
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
filmu	film	k1gInSc2	film
obsazeni	obsazen	k2eAgMnPc1d1	obsazen
i	i	k8xC	i
němečtí	německý	k2eAgMnPc1d1	německý
herci	herec	k1gMnPc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
krále	král	k1gMnSc2	král
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Rolf	Rolf	k1gMnSc1	Rolf
Hoppe	Hopp	k1gInSc5	Hopp
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
často	často	k6eAd1	často
obsazovaný	obsazovaný	k2eAgMnSc1d1	obsazovaný
herec	herec	k1gMnSc1	herec
zejména	zejména	k9	zejména
do	do	k7c2	do
negativních	negativní	k2eAgFnPc2d1	negativní
rolí	role	k1gFnPc2	role
Vorlíčka	Vorlíček	k1gMnSc2	Vorlíček
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
svýma	svůj	k3xOyFgNnPc7	svůj
modrýma	modrý	k2eAgNnPc7d1	modré
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Karin	Karina	k1gFnPc2	Karina
Leschovou	Leschová	k1gFnSc4	Leschová
si	se	k3xPyFc3	se
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
DEFA	DEFA	kA	DEFA
vybral	vybrat	k5eAaPmAgMnS	vybrat
sám	sám	k3xTgMnSc1	sám
Hoppe	Hopp	k1gMnSc5	Hopp
<g/>
.	.	kIx.	.
</s>
<s>
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vybral	vybrat	k5eAaPmAgMnS	vybrat
podle	podle	k7c2	podle
fotky	fotka	k1gFnSc2	fotka
v	v	k7c6	v
castingovém	castingový	k2eAgInSc6d1	castingový
archivu	archiv	k1gInSc6	archiv
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nic	nic	k3yNnSc4	nic
většího	veliký	k2eAgMnSc4d2	veliký
nehrála	hrát	k5eNaImAgFnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Dabéry	Dabér	k1gInPc1	Dabér
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
i	i	k9	i
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
vybírali	vybírat	k5eAaImAgMnP	vybírat
především	především	k9	především
pomocní	pomocní	k2eAgMnPc1d1	pomocní
režiséři	režisér	k1gMnPc1	režisér
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Komparz	komparz	k1gInSc1	komparz
na	na	k7c6	na
plese	ples	k1gInSc6	ples
čítal	čítat	k5eAaImAgInS	čítat
přibližně	přibližně	k6eAd1	přibližně
sto	sto	k4xCgNnSc1	sto
tanečníků	tanečník	k1gMnPc2	tanečník
<g/>
.	.	kIx.	.
</s>
<s>
Tvořili	tvořit	k5eAaImAgMnP	tvořit
jej	on	k3xPp3gMnSc4	on
převážně	převážně	k6eAd1	převážně
tanečníci	tanečník	k1gMnPc1	tanečník
z	z	k7c2	z
východoněmeckého	východoněmecký	k2eAgInSc2d1	východoněmecký
televizního	televizní	k2eAgInSc2d1	televizní
baletu	balet	k1gInSc2	balet
DDR-Fernsehballett	DDR-Fernsehballetta	k1gFnPc2	DDR-Fernsehballetta
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
i	i	k8xC	i
princ	princ	k1gMnSc1	princ
měli	mít	k5eAaImAgMnP	mít
více	hodně	k6eAd2	hodně
koní	koní	k2eAgMnPc1d1	koní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nemohli	moct	k5eNaImAgMnP	moct
převážet	převážet	k5eAaImF	převážet
přes	přes	k7c4	přes
hranice	hranice	k1gFnPc4	hranice
kvůli	kvůli	k7c3	kvůli
slintavce	slintavka	k1gFnSc3	slintavka
a	a	k8xC	a
kulhavce	kulhavka	k1gFnSc3	kulhavka
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Jurášci	Jurášek	k1gMnPc1	Jurášek
(	(	kIx(	(
<g/>
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
Nikolaus	Nikolaus	k1gInSc1	Nikolaus
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
tři	tři	k4xCgMnPc1	tři
-	-	kIx~	-
dva	dva	k4xCgMnPc1	dva
čeští	český	k2eAgMnPc1d1	český
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
německý	německý	k2eAgMnSc1d1	německý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Bonek	Bonek	k1gMnSc1	Bonek
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
odlišit	odlišit	k5eAaPmF	odlišit
podle	podle	k7c2	podle
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
kůň	kůň	k1gMnSc1	kůň
ve	v	k7c6	v
stáji	stáj	k1gFnSc6	stáj
a	a	k8xC	a
u	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
má	mít	k5eAaImIp3nS	mít
zimní	zimní	k2eAgFnSc4d1	zimní
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
<s>
Sestřelený	sestřelený	k2eAgMnSc1d1	sestřelený
dravec	dravec	k1gMnSc1	dravec
byl	být	k5eAaImAgMnS	být
skutečný	skutečný	k2eAgMnSc1d1	skutečný
<g/>
,	,	kIx,	,
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
jej	on	k3xPp3gInSc4	on
měl	mít	k5eAaImAgMnS	mít
zmrazeného	zmrazený	k2eAgMnSc4d1	zmrazený
šumavský	šumavský	k2eAgMnSc5d1	šumavský
hajný	hajný	k1gMnSc5	hajný
<g/>
.	.	kIx.	.
</s>
<s>
Pták	pták	k1gMnSc1	pták
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
natáčením	natáčení	k1gNnSc7	natáčení
rozmrazen	rozmrazit	k5eAaPmNgMnS	rozmrazit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
vláčný	vláčný	k2eAgInSc1d1	vláčný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
filmování	filmování	k1gNnSc6	filmování
jej	on	k3xPp3gMnSc2	on
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
shazoval	shazovat	k5eAaImAgMnS	shazovat
sám	sám	k3xTgMnSc1	sám
režisér	režisér	k1gMnSc1	režisér
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
<g/>
.	.	kIx.	.
</s>
<s>
Sova	sova	k1gFnSc1	sova
byla	být	k5eAaImAgFnS	být
zapůjčena	zapůjčit	k5eAaPmNgFnS	zapůjčit
z	z	k7c2	z
berlínského	berlínský	k2eAgNnSc2d1	berlínské
zoo	zoo	k1gNnSc2	zoo
<g/>
,	,	kIx,	,
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
musel	muset	k5eAaImAgMnS	muset
šťouchat	šťouchat	k5eAaImF	šťouchat
špejlí	špejle	k1gFnSc7	špejle
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mrkala	mrkat	k5eAaImAgFnS	mrkat
<g/>
.	.	kIx.	.
</s>
<s>
Kostýmy	kostým	k1gInPc4	kostým
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
dílna	dílna	k1gFnSc1	dílna
Theodora	Theodor	k1gMnSc4	Theodor
Pištěka	Pištěk	k1gMnSc4	Pištěk
<g/>
,	,	kIx,	,
za	za	k7c4	za
německou	německý	k2eAgFnSc4d1	německá
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
o	o	k7c4	o
kostýmní	kostýmní	k2eAgFnSc4d1	kostýmní
výpravu	výprava	k1gFnSc4	výprava
staral	starat	k5eAaImAgMnS	starat
Günter	Günter	k1gMnSc1	Günter
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
se	se	k3xPyFc4	se
80	[number]	k4	80
originálních	originální	k2eAgInPc2d1	originální
kostýmů	kostým	k1gInPc2	kostým
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
materiálů	materiál	k1gInPc2	materiál
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
krimplenu	krimplen	k1gInSc2	krimplen
<g/>
.	.	kIx.	.
</s>
<s>
Kostýmy	kostým	k1gInPc1	kostým
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
na	na	k7c4	na
letní	letní	k2eAgNnSc4d1	letní
natáčení	natáčení	k1gNnSc4	natáčení
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
herci	herec	k1gMnPc1	herec
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
mrzli	mrznout	k5eAaImAgMnP	mrznout
<g/>
.	.	kIx.	.
</s>
<s>
Popelčin	Popelčin	k2eAgInSc1d1	Popelčin
střevíček	střevíček	k1gInSc1	střevíček
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
velikostech	velikost	k1gFnPc6	velikost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c6	na
kameře	kamera	k1gFnSc6	kamera
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Nahrál	nahrát	k5eAaPmAgMnS	nahrát
ji	on	k3xPp3gFnSc4	on
Filmový	filmový	k2eAgInSc1d1	filmový
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
dirigenta	dirigent	k1gMnSc2	dirigent
Štěpána	Štěpán	k1gMnSc2	Štěpán
Koníčka	Koníček	k1gMnSc2	Koníček
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
píseň	píseň	k1gFnSc1	píseň
Kdepak	kdepak	k6eAd1	kdepak
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
ptáčku	ptáček	k1gInSc6	ptáček
<g/>
,	,	kIx,	,
hnízdo	hnízdo	k1gNnSc1	hnízdo
máš	mít	k5eAaImIp2nS	mít
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Karla	Karel	k1gMnSc4	Karel
Gotta	Gott	k1gMnSc4	Gott
nahrál	nahrát	k5eAaBmAgMnS	nahrát
ji	on	k3xPp3gFnSc4	on
orchestr	orchestr	k1gInSc1	orchestr
Ladislava	Ladislav	k1gMnSc2	Ladislav
Štaidla	Štaidlo	k1gNnSc2	Štaidlo
<g/>
,	,	kIx,	,
slova	slovo	k1gNnSc2	slovo
napsal	napsat	k5eAaBmAgMnS	napsat
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
verzi	verze	k1gFnSc6	verze
zní	znět	k5eAaImIp3nS	znět
pouze	pouze	k6eAd1	pouze
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
verze	verze	k1gFnSc1	verze
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
tehdy	tehdy	k6eAd1	tehdy
zpíval	zpívat	k5eAaImAgMnS	zpívat
u	u	k7c2	u
konkurence	konkurence	k1gFnSc2	konkurence
píseň	píseň	k1gFnSc1	píseň
Včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
na	na	k7c4	na
drahý	drahý	k2eAgInSc4d1	drahý
barevný	barevný	k2eAgInSc4d1	barevný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
záběrů	záběr	k1gInPc2	záběr
točila	točit	k5eAaImAgFnS	točit
na	na	k7c4	na
první	první	k4xOgInSc4	první
pokus	pokus	k1gInSc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1972	[number]	k4	1972
v	v	k7c6	v
ateliérech	ateliér	k1gInPc6	ateliér
Babelsberg	Babelsberg	k1gInSc1	Babelsberg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgFnPc4d1	postavena
kulisy	kulisa	k1gFnPc4	kulisa
pro	pro	k7c4	pro
scény	scéna	k1gFnPc4	scéna
na	na	k7c6	na
tanečním	taneční	k2eAgInSc6d1	taneční
sále	sál	k1gInSc6	sál
a	a	k8xC	a
interiér	interiér	k1gInSc1	interiér
špejcharu	špejchar	k1gInSc2	špejchar
s	s	k7c7	s
Rozárkou	Rozárka	k1gFnSc7	Rozárka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1973	[number]	k4	1973
se	se	k3xPyFc4	se
natáčely	natáčet	k5eAaImAgInP	natáčet
exteriéry	exteriér	k1gInPc1	exteriér
okolo	okolo	k7c2	okolo
zámku	zámek	k1gInSc2	zámek
Moritzburg	Moritzburg	k1gInSc1	Moritzburg
<g/>
.	.	kIx.	.
</s>
<s>
Plesové	plesový	k2eAgFnPc1d1	plesová
scény	scéna	k1gFnPc1	scéna
se	se	k3xPyFc4	se
uvnitř	uvnitř	k6eAd1	uvnitř
nenatáčely	natáčet	k5eNaImAgFnP	natáčet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tamější	tamější	k2eAgInSc1d1	tamější
taneční	taneční	k2eAgInSc1d1	taneční
sál	sál	k1gInSc1	sál
je	být	k5eAaImIp3nS	být
obložen	obložit	k5eAaPmNgInS	obložit
černými	černý	k2eAgFnPc7d1	černá
koženými	kožený	k2eAgFnPc7d1	kožená
tapetami	tapeta	k1gFnPc7	tapeta
zdobenými	zdobený	k2eAgFnPc7d1	zdobená
zlatem	zlato	k1gNnSc7	zlato
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Vorlíčkovi	Vorlíček	k1gMnSc3	Vorlíček
spíše	spíše	k9	spíše
připomínalo	připomínat	k5eAaImAgNnS	připomínat
hrobku	hrobka	k1gFnSc4	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
provázel	provázet	k5eAaImAgInS	provázet
nedostatek	nedostatek	k1gInSc4	nedostatek
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
sníh	sníh	k1gInSc1	sníh
umělý	umělý	k2eAgInSc1d1	umělý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
rybí	rybí	k2eAgFnPc4d1	rybí
konzervárny	konzervárna	k1gFnPc4	konzervárna
z	z	k7c2	z
rozemletých	rozemletý	k2eAgFnPc2d1	rozemletá
rybích	rybí	k2eAgFnPc2d1	rybí
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
zapáchal	zapáchat	k5eAaImAgInS	zapáchat
a	a	k8xC	a
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
rozložil	rozložit	k5eAaPmAgMnS	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
Sypal	sypat	k5eAaImAgInS	sypat
se	se	k3xPyFc4	se
však	však	k9	však
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
snímala	snímat	k5eAaImAgFnS	snímat
kamera	kamera	k1gFnSc1	kamera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
natáčela	natáčet	k5eAaImAgFnS	natáčet
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
exteriéry	exteriér	k1gInPc1	exteriér
<g/>
,	,	kIx,	,
statek	statek	k1gInSc1	statek
<g/>
,	,	kIx,	,
hluboké	hluboký	k2eAgInPc1d1	hluboký
lesy	les	k1gInPc1	les
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Scény	scéna	k1gFnPc4	scéna
u	u	k7c2	u
špejchárku	špejchárku	k?	špejchárku
se	se	k3xPyFc4	se
točily	točit	k5eAaImAgInP	točit
u	u	k7c2	u
Klatov	Klatovy	k1gInPc2	Klatovy
<g/>
,	,	kIx,	,
cesty	cesta	k1gFnPc1	cesta
do	do	k7c2	do
špejcharu	špejchar	k1gInSc2	špejchar
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
hory	hora	k1gFnSc2	hora
Běleč	Běleč	k1gInSc4	Běleč
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
Onoho	onen	k3xDgInSc2	onen
Světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
lovecké	lovecký	k2eAgFnPc1d1	lovecká
scény	scéna	k1gFnPc1	scéna
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mezihoří	mezihoří	k1gNnSc6	mezihoří
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
pád	pád	k1gInSc1	pád
saní	saní	k2eAgInSc1d1	saní
do	do	k7c2	do
Panského	panský	k2eAgInSc2d1	panský
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
hrad	hrad	k1gInSc1	hrad
Švihov	Švihov	k1gInSc1	Švihov
posloužil	posloužit	k5eAaPmAgInS	posloužit
jako	jako	k9	jako
kulisa	kulisa	k1gFnSc1	kulisa
statku	statek	k1gInSc2	statek
<g/>
,	,	kIx,	,
vybral	vybrat	k5eAaPmAgInS	vybrat
jej	on	k3xPp3gMnSc4	on
filmový	filmový	k2eAgMnSc1d1	filmový
architekt	architekt	k1gMnSc1	architekt
Oldřich	Oldřich	k1gMnSc1	Oldřich
Bosák	Bosák	k1gMnSc1	Bosák
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
hrad	hrad	k1gInSc1	hrad
se	se	k3xPyFc4	se
však	však	k9	však
ve	v	k7c6	v
filmu	film	k1gInSc6	film
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Dostavovaly	dostavovat	k5eAaImAgFnP	dostavovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
stodoly	stodola	k1gFnPc1	stodola
<g/>
,	,	kIx,	,
obytná	obytný	k2eAgFnSc1d1	obytná
část	část	k1gFnSc1	část
a	a	k8xC	a
sádrová	sádrový	k2eAgFnSc1d1	sádrová
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Natáčelo	natáčet	k5eAaImAgNnS	natáčet
se	se	k3xPyFc4	se
na	na	k7c6	na
Červené	Červené	k2eAgFnSc6d1	Červené
baště	bašta	k1gFnSc6	bašta
<g/>
,	,	kIx,	,
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
byl	být	k5eAaImAgInS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
holubník	holubník	k1gInSc1	holubník
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
si	se	k3xPyFc3	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
zahrálo	zahrát	k5eAaPmAgNnS	zahrát
jako	jako	k8xS	jako
komparz	komparz	k1gInSc4	komparz
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
natáčení	natáčení	k1gNnSc2	natáčení
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
na	na	k7c6	na
Švihově	švihově	k6eAd1	švihově
se	se	k3xPyFc4	se
stříkaly	stříkat	k5eAaImAgInP	stříkat
střechy	střecha	k1gFnSc2	střecha
vápnem	vápno	k1gNnSc7	vápno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
umělý	umělý	k2eAgInSc1d1	umělý
sníh	sníh	k1gInSc1	sníh
sloužil	sloužit	k5eAaImAgInS	sloužit
rozemletý	rozemletý	k2eAgInSc4d1	rozemletý
polystyren	polystyren	k1gInSc4	polystyren
<g/>
.	.	kIx.	.
</s>
<s>
Štáb	štáb	k1gInSc1	štáb
čekal	čekat	k5eAaImAgInS	čekat
<g/>
,	,	kIx,	,
až	až	k9	až
napadne	napadnout	k5eAaPmIp3nS	napadnout
sníh	sníh	k1gInSc1	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jej	on	k3xPp3gMnSc4	on
napadlo	napadnout	k5eAaPmAgNnS	napadnout
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
původně	původně	k6eAd1	původně
vybraných	vybraný	k2eAgInPc2d1	vybraný
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
před	před	k7c7	před
princem	princ	k1gMnSc7	princ
drží	držet	k5eAaImIp3nS	držet
bičík	bičík	k1gInSc4	bičík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
záběru	záběr	k1gInSc6	záběr
neměla	mít	k5eNaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
honu	hon	k1gInSc6	hon
princ	princ	k1gMnSc1	princ
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
lišku	liška	k1gFnSc4	liška
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
následně	následně	k6eAd1	následně
vytahuje	vytahovat	k5eAaImIp3nS	vytahovat
šíp	šíp	k1gInSc1	šíp
z	z	k7c2	z
břicha	břicho	k1gNnSc2	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
sluha	sluha	k1gMnSc1	sluha
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
záběru	záběr	k1gInSc6	záběr
zvedne	zvednout	k5eAaPmIp3nS	zvednout
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jinou	jiný	k2eAgFnSc4d1	jiná
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přípravách	příprava	k1gFnPc6	příprava
na	na	k7c4	na
ples	ples	k1gInSc4	ples
král	král	k1gMnSc1	král
vykoukne	vykouknout	k5eAaPmIp3nS	vykouknout
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
<g/>
,	,	kIx,	,
uvidí	uvidět	k5eAaPmIp3nS	uvidět
princova	princův	k2eAgMnSc4d1	princův
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
připomíná	připomínat	k5eAaImIp3nS	připomínat
Popelčina	Popelčin	k2eAgFnSc1d1	Popelčina
Juráška	Juráška	k1gFnSc1	Juráška
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
princ	princ	k1gMnSc1	princ
jezdí	jezdit	k5eAaImIp3nS	jezdit
na	na	k7c6	na
grošákovi	grošák	k1gMnSc6	grošák
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Vincek	Vincek	k1gMnSc1	Vincek
řeže	řezat	k5eAaImIp3nS	řezat
v	v	k7c6	v
sadě	sada	k1gFnSc6	sada
dříví	dříví	k1gNnSc2	dříví
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
vidět	vidět	k5eAaImF	vidět
sloup	sloup	k1gInSc1	sloup
s	s	k7c7	s
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
Popelka	Popelka	k1gMnSc1	Popelka
na	na	k7c6	na
noci	noc	k1gFnSc6	noc
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
k	k	k7c3	k
zámku	zámek	k1gInSc3	zámek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odekrává	odekrávat	k5eAaImIp3nS	odekrávat
ples	ples	k1gInSc1	ples
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
nahlíží	nahlížet	k5eAaImIp3nS	nahlížet
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
fází	fáze	k1gFnSc7	fáze
dechem	dech	k1gInSc7	dech
rozehřívá	rozehřívat	k5eAaImIp3nS	rozehřívat
námrazu	námraza	k1gFnSc4	námraza
na	na	k7c6	na
okně	okno	k1gNnSc6	okno
a	a	k8xC	a
smazává	smazávat	k5eAaImIp3nS	smazávat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lépe	dobře	k6eAd2	dobře
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
namráza	namráza	k1gFnSc1	namráza
se	se	k3xPyFc4	se
při	při	k7c6	při
mrazech	mráz	k1gInPc6	mráz
na	na	k7c6	na
oknech	okno	k1gNnPc6	okno
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
zevnitř	zevnitř	k6eAd1	zevnitř
místnosti	místnost	k1gFnPc4	místnost
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
zvenku	zvenku	k6eAd1	zvenku
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
premiéra	premiéra	k1gFnSc1	premiéra
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1973	[number]	k4	1973
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
kině	kino	k1gNnSc6	kino
Blaník	Blaník	k1gInSc1	Blaník
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
se	se	k3xPyFc4	se
však	však	k9	však
objevila	objevit	k5eAaPmAgFnS	objevit
už	už	k6eAd1	už
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
přehlídce	přehlídka	k1gFnSc6	přehlídka
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
v	v	k7c6	v
Ostrově	ostrov	k1gInSc6	ostrov
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
sklidil	sklidit	k5eAaPmAgInS	sklidit
film	film	k1gInSc1	film
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
pražských	pražský	k2eAgNnPc6d1	Pražské
kinech	kino	k1gNnPc6	kino
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgMnS	hrát
až	až	k9	až
do	do	k7c2	do
června	červen	k1gInSc2	červen
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
ve	v	k7c6	v
Východním	východní	k2eAgNnSc6d1	východní
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
pak	pak	k6eAd1	pak
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
vysílala	vysílat	k5eAaImAgFnS	vysílat
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
ji	on	k3xPp3gFnSc4	on
německé	německý	k2eAgFnPc1d1	německá
televizní	televizní	k2eAgFnPc1d1	televizní
stanice	stanice	k1gFnPc1	stanice
vysílaly	vysílat	k5eAaImAgFnP	vysílat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
osmnáctkrát	osmnáctkrát	k6eAd1	osmnáctkrát
<g/>
.	.	kIx.	.
</s>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
je	být	k5eAaImIp3nS	být
však	však	k9	však
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
se	se	k3xPyFc4	se
Tři	tři	k4xCgInPc1	tři
oříšky	oříšek	k1gInPc1	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
vysílají	vysílat	k5eAaImIp3nP	vysílat
dopoledne	dopoledne	k6eAd1	dopoledne
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
arabských	arabský	k2eAgInPc6d1	arabský
emirátech	emirát	k1gInPc6	emirát
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohádka	pohádka	k1gFnSc1	pohádka
vysílá	vysílat	k5eAaImIp3nS	vysílat
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
<g/>
,	,	kIx,	,
v	v	k7c6	v
pětimilionové	pětimilionový	k2eAgFnSc6d1	pětimilionová
zemi	zem	k1gFnSc6	zem
ji	on	k3xPp3gFnSc4	on
sleduje	sledovat	k5eAaImIp3nS	sledovat
každý	každý	k3xTgMnSc1	každý
pátý	pátý	k4xOgMnSc1	pátý
obyvatel	obyvatel	k1gMnSc1	obyvatel
<g/>
,	,	kIx,	,
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1	veřejnoprávní
norská	norský	k2eAgFnSc1d1	norská
televize	televize	k1gFnSc1	televize
NRK	NRK	kA	NRK
Popelku	Popelka	k1gFnSc4	Popelka
neodvysílala	odvysílat	k5eNaPmAgFnS	odvysílat
<g/>
,	,	kIx,	,
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
pak	pak	k6eAd1	pak
volali	volat	k5eAaImAgMnP	volat
rozhořčení	rozhořčený	k2eAgMnPc1d1	rozhořčený
diváci	divák	k1gMnPc1	divák
a	a	k8xC	a
před	před	k7c7	před
sídlem	sídlo	k1gNnSc7	sídlo
stanice	stanice	k1gFnSc2	stanice
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
i	i	k9	i
demonstrující	demonstrující	k2eAgMnPc1d1	demonstrující
<g/>
.	.	kIx.	.
</s>
<s>
Vysílá	vysílat	k5eAaImIp3nS	vysílat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
dabingem	dabing	k1gInSc7	dabing
herce	herec	k1gMnSc2	herec
Knuta	knuta	k1gFnSc1	knuta
Risana	Risana	k1gFnSc1	Risana
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
DVD	DVD	kA	DVD
s	s	k7c7	s
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
za	za	k7c4	za
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
21	[number]	k4	21
tisíc	tisíc	k4xCgInPc2	tisíc
exemplářů	exemplář	k1gInPc2	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
setkání	setkání	k1gNnSc2	setkání
na	na	k7c6	na
Moritzburgu	Moritzburg	k1gInSc6	Moritzburg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
také	také	k9	také
natáčel	natáčet	k5eAaImAgInS	natáčet
hodinový	hodinový	k2eAgInSc1d1	hodinový
dokument	dokument	k1gInSc1	dokument
televize	televize	k1gFnSc2	televize
MDR	MDR	kA	MDR
<g/>
,	,	kIx,	,
odvysílán	odvysílán	k2eAgMnSc1d1	odvysílán
byl	být	k5eAaImAgMnS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
režisér	režisér	k1gMnSc1	režisér
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
<g/>
,	,	kIx,	,
Trávníček	Trávníček	k1gMnSc1	Trávníček
i	i	k8xC	i
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Westdeutscher	Westdeutschra	k1gFnPc2	Westdeutschra
Rundfunk	Rundfunk	k1gMnSc1	Rundfunk
Köln	Köln	k1gMnSc1	Köln
natočil	natočit	k5eAaBmAgMnS	natočit
o	o	k7c6	o
filmu	film	k1gInSc6	film
třicetiminutový	třicetiminutový	k2eAgInSc4d1	třicetiminutový
dokument	dokument	k1gInSc4	dokument
Auf	Auf	k1gMnPc2	Auf
den	den	k1gInSc1	den
Spuren	Spurna	k1gFnPc2	Spurna
von	von	k1gInSc1	von
Aschenbrödel	Aschenbrödlo	k1gNnPc2	Aschenbrödlo
(	(	kIx(	(
<g/>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
Popelky	Popelka	k1gMnSc2	Popelka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
původních	původní	k2eAgFnPc2d1	původní
kopií	kopie	k1gFnPc2	kopie
filmu	film	k1gInSc2	film
zrestaurována	zrestaurován	k2eAgFnSc1d1	zrestaurována
<g/>
,	,	kIx,	,
pohádka	pohádka	k1gFnSc1	pohádka
byla	být	k5eAaImAgFnS	být
digitálně	digitálně	k6eAd1	digitálně
vyčištěna	vyčištěn	k2eAgFnSc1d1	vyčištěna
<g/>
,	,	kIx,	,
převedena	převeden	k2eAgFnSc1d1	převedena
do	do	k7c2	do
vysokého	vysoký	k2eAgNnSc2d1	vysoké
rozlišení	rozlišení	k1gNnSc2	rozlišení
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
barevných	barevný	k2eAgInPc2d1	barevný
tónů	tón	k1gInPc2	tón
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
.	.	kIx.	.
</s>
<s>
Restaurovaná	restaurovaný	k2eAgFnSc1d1	restaurovaná
verze	verze	k1gFnSc1	verze
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c4	na
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
na	na	k7c6	na
obrazovkách	obrazovka	k1gFnPc6	obrazovka
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
oříšky	oříšek	k1gInPc1	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tradiční	tradiční	k2eAgNnSc4d1	tradiční
schéma	schéma	k1gNnSc4	schéma
střetu	střet	k1gInSc2	střet
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
romantické	romantický	k2eAgFnPc4d1	romantická
příběhu	příběh	k1gInSc3	příběh
na	na	k7c6	na
konci	konec	k1gInSc3	konec
nezvítězí	zvítězit	k5eNaPmIp3nS	zvítězit
jen	jen	k9	jen
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kurátorky	kurátorka	k1gFnSc2	kurátorka
výstavy	výstava	k1gFnSc2	výstava
na	na	k7c6	na
Moritzburgu	Moritzburg	k1gInSc6	Moritzburg
Margitty	Margitta	k1gMnSc2	Margitta
Hensel	Hensel	k1gMnSc1	Hensel
popularita	popularita	k1gFnSc1	popularita
filmu	film	k1gInSc2	film
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
touhou	touha	k1gFnSc7	touha
diváků	divák	k1gMnPc2	divák
po	po	k7c6	po
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
,	,	kIx,	,
spravedlnosti	spravedlnost	k1gFnSc3	spravedlnost
<g/>
,	,	kIx,	,
štěstí	štěstí	k1gNnSc3	štěstí
a	a	k8xC	a
blahobytu	blahobyt	k1gInSc3	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
nepřechází	přecházet	k5eNaImIp3nS	přecházet
do	do	k7c2	do
směšnosti	směšnost	k1gFnSc2	směšnost
a	a	k8xC	a
neubírá	ubírat	k5eNaImIp3nS	ubírat
dramatičnosti	dramatičnost	k1gFnPc4	dramatičnost
<g/>
,	,	kIx,	,
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
však	však	k9	však
velké	velký	k2eAgInPc1d1	velký
filmařské	filmařský	k2eAgInPc1d1	filmařský
triky	trik	k1gInPc1	trik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
analýzy	analýza	k1gFnSc2	analýza
v	v	k7c6	v
Die	Die	k1gFnSc6	Die
Welt	Welta	k1gFnPc2	Welta
by	by	kYmCp3nS	by
bez	bez	k7c2	bez
sněhu	sníh	k1gInSc2	sníh
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
nestal	stát	k5eNaPmAgInS	stát
kultem	kult	k1gInSc7	kult
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vánočnímu	vánoční	k2eAgInSc3d1	vánoční
programu	program	k1gInSc3	program
jako	jako	k8xS	jako
adventní	adventní	k2eAgInSc4d1	adventní
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
svíčky	svíčka	k1gFnPc4	svíčka
<g/>
,	,	kIx,	,
stromeček	stromeček	k1gInSc4	stromeček
a	a	k8xC	a
cukroví	cukroví	k1gNnSc4	cukroví
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
recenze	recenze	k1gFnSc2	recenze
v	v	k7c6	v
Die	Die	k1gFnSc6	Die
Welt	Welta	k1gFnPc2	Welta
"	"	kIx"	"
<g/>
Přesně	přesně	k6eAd1	přesně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
chtějí	chtít	k5eAaImIp3nP	chtít
být	být	k5eAaImF	být
viděny	viděn	k2eAgFnPc1d1	viděna
moderní	moderní	k2eAgFnPc1d1	moderní
postfeministické	postfeministický	k2eAgFnPc1d1	postfeministický
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Česká	český	k2eAgFnSc1d1	Česká
Popelka	Popelka	k1gFnSc1	Popelka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
německé	německý	k2eAgFnSc2d1	německá
pořád	pořád	k6eAd1	pořád
dobrou	dobrý	k2eAgFnSc4d1	dobrá
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc4	chuť
do	do	k7c2	do
života	život	k1gInSc2	život
a	a	k8xC	a
nenaříká	naříkat	k5eNaBmIp3nS	naříkat
na	na	k7c4	na
osud	osud	k1gInSc4	osud
<g/>
,	,	kIx,	,
vtip	vtip	k1gInSc4	vtip
a	a	k8xC	a
divokost	divokost	k1gFnSc4	divokost
jí	on	k3xPp3gFnSc2	on
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
scenárista	scenárista	k1gMnSc1	scenárista
František	František	k1gMnSc1	František
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Popelka	Popelka	k1gFnSc1	Popelka
je	být	k5eAaImIp3nS	být
emancipovaná	emancipovaný	k2eAgFnSc1d1	emancipovaná
<g/>
,	,	kIx,	,
obléká	oblékat	k5eAaImIp3nS	oblékat
se	se	k3xPyFc4	se
jako	jako	k9	jako
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
rajtuje	rajtovat	k5eAaImIp3nS	rajtovat
<g/>
,	,	kIx,	,
loví	lovit	k5eAaImIp3nP	lovit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
drzá	drzý	k2eAgFnSc1d1	drzá
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
docela	docela	k6eAd1	docela
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
duch	duch	k1gMnSc1	duch
odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
reagovat	reagovat	k5eAaBmF	reagovat
ironicky	ironicky	k6eAd1	ironicky
a	a	k8xC	a
s	s	k7c7	s
humorem	humor	k1gInSc7	humor
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
všechna	všechen	k3xTgNnPc4	všechen
pokoření	pokoření	k1gNnSc4	pokoření
si	se	k3xPyFc3	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
užívat	užívat	k5eAaImF	užívat
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
ji	on	k3xPp3gFnSc4	on
macecha	macecha	k1gFnSc1	macecha
uráží	urážet	k5eAaImIp3nS	urážet
<g/>
,	,	kIx,	,
nasedne	nasednout	k5eAaPmIp3nS	nasednout
na	na	k7c4	na
koně	kůň	k1gMnSc4	kůň
a	a	k8xC	a
prohání	prohánět	k5eAaImIp3nS	prohánět
se	se	k3xPyFc4	se
lesem	les	k1gInSc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
má	mít	k5eAaImIp3nS	mít
štěstí	štěstí	k1gNnSc4	štěstí
sama	sám	k3xTgFnSc1	sám
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
důvěřuje	důvěřovat	k5eAaImIp3nS	důvěřovat
osudu	osud	k1gInSc3	osud
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
její	její	k3xOp3gFnSc2	její
příbuzné	příbuzná	k1gFnSc2	příbuzná
objednávají	objednávat	k5eAaImIp3nP	objednávat
u	u	k7c2	u
Vincka	Vincek	k1gMnSc2	Vincek
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jim	on	k3xPp3gMnPc3	on
má	mít	k5eAaImIp3nS	mít
koupit	koupit	k5eAaPmF	koupit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zeptá	zeptat	k5eAaPmIp3nS	zeptat
<g/>
,	,	kIx,	,
co	co	k8xS	co
jí	on	k3xPp3gFnSc3	on
má	mít	k5eAaImIp3nS	mít
koupit	koupit	k5eAaPmF	koupit
<g/>
,	,	kIx,	,
nerozpláče	rozplakat	k5eNaPmIp3nS	rozplakat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ještě	ještě	k6eAd1	ještě
dělat	dělat	k5eAaImF	dělat
vtipy	vtip	k1gInPc4	vtip
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Popelka	Popelka	k1gFnSc1	Popelka
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cool	cool	k1gMnSc1	cool
princezna	princezna	k1gFnSc1	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
kočáru	kočár	k1gInSc2	kočár
z	z	k7c2	z
dýně	dýně	k1gFnSc2	dýně
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
vlastním	vlastní	k2eAgMnSc6d1	vlastní
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
zaplétání	zaplétání	k1gNnSc2	zaplétání
copů	cop	k1gInPc2	cop
raději	rád	k6eAd2	rád
střílí	střílet	k5eAaImIp3nS	střílet
z	z	k7c2	z
kuše	kuše	k1gFnSc2	kuše
<g/>
,	,	kIx,	,
donutí	donutit	k5eAaPmIp3nS	donutit
nemotorného	motorný	k2eNgMnSc2d1	nemotorný
prince	princ	k1gMnSc2	princ
se	se	k3xPyFc4	se
snažit	snažit	k5eAaImF	snažit
a	a	k8xC	a
pochybovačně	pochybovačně	k6eAd1	pochybovačně
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
tázat	tázat	k5eAaImF	tázat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
jej	on	k3xPp3gInSc4	on
chtěla	chtít	k5eAaImAgFnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Popelka	Popelka	k1gFnSc1	Popelka
nečeká	čekat	k5eNaImIp3nS	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
ji	on	k3xPp3gFnSc4	on
princ	princ	k1gMnSc1	princ
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
půvabu	půvab	k1gInSc2	půvab
oplývá	oplývat	k5eAaImIp3nS	oplývat
i	i	k9	i
sebevědomím	sebevědomí	k1gNnSc7	sebevědomí
<g/>
,	,	kIx,	,
duchaplností	duchaplnost	k1gFnSc7	duchaplnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
Popelce	Popelka	k1gFnSc3	Popelka
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nehodil	hodit	k5eNaPmAgMnS	hodit
princ	princ	k1gMnSc1	princ
v	v	k7c6	v
uniformě	uniforma	k1gFnSc6	uniforma
z	z	k7c2	z
pohádek	pohádka	k1gFnPc2	pohádka
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gMnPc2	Grimm
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
princ	princ	k1gMnSc1	princ
je	být	k5eAaImIp3nS	být
svéhlavý	svéhlavý	k2eAgMnSc1d1	svéhlavý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nemotorný	nemotorný	k2eAgMnSc1d1	nemotorný
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
před	před	k7c7	před
těsným	těsný	k2eAgInSc7d1	těsný
dvorským	dvorský	k2eAgInSc7d1	dvorský
životem	život	k1gInSc7	život
utíká	utíkat	k5eAaImIp3nS	utíkat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Popelka	Popelka	k1gMnSc1	Popelka
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
macechy	macecha	k1gFnSc2	macecha
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
premiérou	premiéra	k1gFnSc7	premiéra
se	se	k3xPyFc4	se
pohádka	pohádka	k1gFnSc1	pohádka
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
přehlídce	přehlídka	k1gFnSc6	přehlídka
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
v	v	k7c6	v
Ostrově	ostrov	k1gInSc6	ostrov
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
Křišťálovou	křišťálový	k2eAgFnSc4d1	Křišťálová
vázu	váza	k1gFnSc4	váza
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
představitelé	představitel	k1gMnPc1	představitel
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
získali	získat	k5eAaPmAgMnP	získat
za	za	k7c4	za
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
herecké	herecký	k2eAgInPc4d1	herecký
výkony	výkon	k1gInPc4	výkon
Zlatého	zlatý	k1gInSc2	zlatý
Ledňáčka	ledňáček	k1gMnSc4	ledňáček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
účastnil	účastnit	k5eAaImAgInS	účastnit
dětského	dětský	k2eAgInSc2d1	dětský
filmového	filmový	k2eAgInSc2d1	filmový
festivalu	festival	k1gInSc2	festival
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
pohádka	pohádka	k1gFnSc1	pohádka
zvolena	zvolit	k5eAaPmNgFnS	zvolit
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
pohádkou	pohádka	k1gFnSc7	pohádka
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
<g/>
,	,	kIx,	,
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zde	zde	k6eAd1	zde
říká	říkat	k5eAaImIp3nS	říkat
3	[number]	k4	3
<g/>
HfA	HfA	k1gFnPc2	HfA
(	(	kIx(	(
<g/>
Drei	Dree	k1gFnSc4	Dree
Haselnüsse	Haselnüsse	k1gFnSc1	Haselnüsse
für	für	k?	für
Aschenbrödel	Aschenbrödlo	k1gNnPc2	Aschenbrödlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
tu	tu	k6eAd1	tu
fanklub	fanklub	k1gInSc4	fanklub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Bilstein	Bilsteina	k1gFnPc2	Bilsteina
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
tematické	tematický	k2eAgInPc1d1	tematický
plesy	ples	k1gInPc1	ples
s	s	k7c7	s
kostýmy	kostým	k1gInPc7	kostým
podle	podle	k7c2	podle
filmu	film	k1gInSc2	film
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
stejná	stejný	k2eAgFnSc1d1	stejná
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
Babylon	Babylon	k1gInSc1	Babylon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
film	film	k1gInSc4	film
svou	svůj	k3xOyFgFnSc4	svůj
východoněmeckou	východoněmecký	k2eAgFnSc4d1	východoněmecká
premiéru	premiéra	k1gFnSc4	premiéra
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Moritzburg	Moritzburg	k1gInSc1	Moritzburg
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
nachází	nacházet	k5eAaImIp3nS	nacházet
velká	velký	k2eAgFnSc1d1	velká
expozice	expozice	k1gFnSc1	expozice
rekvizit	rekvizita	k1gFnPc2	rekvizita
<g/>
,	,	kIx,	,
kostýmů	kostým	k1gInPc2	kostým
<g/>
,	,	kIx,	,
původních	původní	k2eAgInPc2d1	původní
scénářů	scénář	k1gInPc2	scénář
z	z	k7c2	z
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
každoročně	každoročně	k6eAd1	každoročně
ji	on	k3xPp3gFnSc4	on
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přes	přes	k7c4	přes
150	[number]	k4	150
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgFnP	konat
oslavy	oslava	k1gFnPc1	oslava
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
premiéry	premiéra	k1gFnSc2	premiéra
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
neznámý	známý	k2eNgMnSc1d1	neznámý
pachatel	pachatel	k1gMnSc1	pachatel
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
kopii	kopie	k1gFnSc4	kopie
plesové	plesový	k2eAgFnSc2d1	plesová
róby	róba	k1gFnSc2	róba
Popelky	Popelka	k1gFnSc2	Popelka
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
poštou	pošta	k1gFnSc7	pošta
zaslat	zaslat	k5eAaPmF	zaslat
zpět	zpět	k6eAd1	zpět
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
vodním	vodní	k2eAgInSc6d1	vodní
hradě	hrad	k1gInSc6	hrad
Švihov	Švihov	k1gInSc4	Švihov
výstava	výstava	k1gFnSc1	výstava
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
Popelky	Popelka	k1gMnSc2	Popelka
ke	k	k7c3	k
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
pohádky	pohádka	k1gFnSc2	pohádka
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
různé	různý	k2eAgInPc1d1	různý
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
nové	nový	k2eAgNnSc4d1	nové
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
pokračování	pokračování	k1gNnSc6	pokračování
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
opět	opět	k6eAd1	opět
hráli	hrát	k5eAaImAgMnP	hrát
původní	původní	k2eAgMnPc1d1	původní
herci	herec	k1gMnPc1	herec
(	(	kIx(	(
<g/>
Šafránková	Šafránková	k1gFnSc1	Šafránková
<g/>
,	,	kIx,	,
Trávníček	Trávníček	k1gMnSc1	Trávníček
<g/>
,	,	kIx,	,
Jandák	Jandák	k1gMnSc1	Jandák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
i	i	k9	i
režisér	režisér	k1gMnSc1	režisér
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nenašel	najít	k5eNaPmAgMnS	najít
se	se	k3xPyFc4	se
žádný	žádný	k3yNgInSc4	žádný
vhodný	vhodný	k2eAgInSc4d1	vhodný
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
televizní	televizní	k2eAgFnSc1d1	televizní
společnost	společnost	k1gFnSc1	společnost
Bavaria	Bavarium	k1gNnSc2	Bavarium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
chtěla	chtít	k5eAaImAgFnS	chtít
po	po	k7c6	po
Vorlíčkovi	Vorlíček	k1gMnSc6	Vorlíček
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
záběr	záběr	k1gInSc1	záběr
po	po	k7c6	po
záběru	záběr	k1gInSc6	záběr
pohádku	pohádka	k1gFnSc4	pohádka
přetočil	přetočit	k5eAaPmAgInS	přetočit
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
herci	herec	k1gMnPc7	herec
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
pouze	pouze	k6eAd1	pouze
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
nezrealizoval	zrealizovat	k5eNaPmAgInS	zrealizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
muzikál	muzikál	k1gInSc1	muzikál
na	na	k7c6	na
ledě	led	k1gInSc6	led
Popelka	Popelka	k1gMnSc1	Popelka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
scénář	scénář	k1gInSc4	scénář
napsal	napsat	k5eAaBmAgMnS	napsat
Jindřich	Jindřich	k1gMnSc1	Jindřich
Šimek	Šimek	k1gMnSc1	Šimek
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Petr	Petr	k1gMnSc1	Petr
Malásek	Malásek	k1gMnSc1	Malásek
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
objevila	objevit	k5eAaPmAgFnS	objevit
zpráva	zpráva	k1gFnSc1	zpráva
že	že	k8xS	že
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
točit	točit	k5eAaImF	točit
remake	remake	k6eAd1	remake
s	s	k7c7	s
norskými	norský	k2eAgMnPc7d1	norský
herci	herec	k1gMnPc7	herec
<g/>
.	.	kIx.	.
</s>
<s>
Václavu	Václav	k1gMnSc3	Václav
Vorlíčkovi	Vorlíček	k1gMnSc3	Vorlíček
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
režii	režie	k1gFnSc4	režie
ten	ten	k3xDgMnSc1	ten
jí	on	k3xPp3gFnSc3	on
však	však	k9	však
odmítnul	odmítnout	k5eAaPmAgInS	odmítnout
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zařekl	zařeknout	k5eAaPmAgMnS	zařeknout
že	že	k8xS	že
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
točit	točit	k5eAaImF	točit
zimní	zimní	k2eAgInSc4d1	zimní
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
