<s>
Kuba	Kuba	k1gFnSc1	Kuba
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
se	s	k7c7	s
socialistickým	socialistický	k2eAgInSc7d1	socialistický
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
přes	přes	k7c4	přes
moře	moře	k1gNnSc4	moře
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
a	a	k8xC	a
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
,	,	kIx,	,
ostrovními	ostrovní	k2eAgInPc7d1	ostrovní
státy	stát	k1gInPc7	stát
karibské	karibský	k2eAgFnSc2d1	karibská
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
Bahamy	Bahamy	k1gFnPc1	Bahamy
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc1	Haiti
a	a	k8xC	a
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
již	již	k9	již
vzdálenějšími	vzdálený	k2eAgInPc7d2	vzdálenější
středoamerickými	středoamerický	k2eAgInPc7d1	středoamerický
či	či	k8xC	či
karibskými	karibský	k2eAgInPc7d1	karibský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
Florida	Florida	k1gFnSc1	Florida
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Kuby	Kuba	k1gFnSc2	Kuba
vzdálena	vzdáleno	k1gNnSc2	vzdáleno
150	[number]	k4	150
km	km	kA	km
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
Kuby	Kuba	k1gFnSc2	Kuba
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
mexický	mexický	k2eAgInSc1d1	mexický
Yucatán	Yucatán	k2eAgInSc1d1	Yucatán
a	a	k8xC	a
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
od	od	k7c2	od
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mohutný	mohutný	k2eAgInSc1d1	mohutný
Mexický	mexický	k2eAgInSc1d1	mexický
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
ostrovní	ostrovní	k2eAgInPc1d1	ostrovní
státy	stát	k1gInPc1	stát
leží	ležet	k5eAaImIp3nP	ležet
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
až	až	k8xS	až
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
Kuby	Kuba	k1gFnSc2	Kuba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vojenská	vojenský	k2eAgFnSc1d1	vojenská
základna	základna	k1gFnSc1	základna
"	"	kIx"	"
<g/>
Zátoka	zátoka	k1gFnSc1	zátoka
Guantánamo	Guantánama	k1gFnSc5	Guantánama
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Kuba	Kuba	k1gFnSc1	Kuba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
v	v	k7c6	v
časově	časově	k6eAd1	časově
neomezené	omezený	k2eNgFnSc6d1	neomezená
smlouvě	smlouva	k1gFnSc6	smlouva
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Příjezd	příjezd	k1gInSc1	příjezd
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
chráněný	chráněný	k2eAgInSc1d1	chráněný
protitankovými	protitankový	k2eAgInPc7d1	protitankový
zátarasy	zátaras	k1gInPc7	zátaras
a	a	k8xC	a
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
jedinou	jediný	k2eAgFnSc4d1	jediná
vnitrozemní	vnitrozemní	k2eAgFnSc4d1	vnitrozemní
hranici	hranice	k1gFnSc4	hranice
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
souostroví	souostroví	k1gNnSc2	souostroví
Velké	velký	k2eAgFnPc1d1	velká
Antily	Antily	k1gFnPc1	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
dle	dle	k7c2	dle
velikosti	velikost	k1gFnSc2	velikost
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1200	[number]	k4	1200
km	km	kA	km
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc1d1	široký
až	až	k9	až
190	[number]	k4	190
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
jeho	on	k3xPp3gInSc2	on
povrchu	povrch	k1gInSc2	povrch
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
nížiny	nížina	k1gFnPc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
nachazí	nachazit	k5eAaPmIp3nS	nachazit
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
lokalitě	lokalita	k1gFnSc6	lokalita
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
krasové	krasový	k2eAgInPc1d1	krasový
útvary	útvar	k1gInPc1	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
táhne	táhnout	k5eAaImIp3nS	táhnout
pohoří	pohoří	k1gNnSc1	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Maestra	maestro	k1gMnSc2	maestro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
pohoří	pohoří	k1gNnSc6	pohoří
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
Pico	Pico	k6eAd1	Pico
Turquino	Turquino	k1gNnSc1	Turquino
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
1974	[number]	k4	1974
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Bažinaté	bažinatý	k2eAgFnSc2d1	bažinatá
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Zapata	Zapata	k1gFnSc1	Zapata
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nacházejí	nacházet	k5eAaImIp3nP	nacházet
korálové	korálový	k2eAgInPc4d1	korálový
útesy	útes	k1gInPc4	útes
a	a	k8xC	a
atoly	atol	k1gInPc4	atol
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
Kuba	Kuba	k1gFnSc1	Kuba
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
hlavního	hlavní	k2eAgInSc2d1	hlavní
ostrova	ostrov	k1gInSc2	ostrov
tvořen	tvořit	k5eAaImNgInS	tvořit
přibližně	přibližně	k6eAd1	přibližně
čtyřmi	čtyři	k4xCgInPc7	čtyři
tisíci	tisíc	k4xCgInPc7	tisíc
malých	malý	k2eAgInPc2d1	malý
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
kubánský	kubánský	k2eAgInSc1d1	kubánský
ostrov	ostrov	k1gInSc1	ostrov
Isla	Isl	k1gInSc2	Isl
de	de	k?	de
la	la	k1gNnPc2	la
Juventud	Juventud	k1gInSc1	Juventud
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc1	ostrov
Mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
před	před	k7c7	před
revolucí	revoluce	k1gFnSc7	revoluce
zvaném	zvaný	k2eAgInSc6d1	zvaný
Ostrov	ostrov	k1gInSc1	ostrov
borovic	borovice	k1gFnPc2	borovice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
hlavního	hlavní	k2eAgInSc2d1	hlavní
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
<g/>
Ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
přírodní	přírodní	k2eAgInSc4d1	přírodní
zdroj	zdroj	k1gInSc4	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Isla	Isla	k1gMnSc1	Isla
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
bez	bez	k7c2	bez
zdroje	zdroj	k1gInSc2	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
Caya	Cay	k1gInSc2	Cay
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
souostroví	souostroví	k1gNnSc4	souostroví
okolo	okolo	k7c2	okolo
hlavního	hlavní	k2eAgInSc2d1	hlavní
ostrova	ostrov	k1gInSc2	ostrov
patří	patřit	k5eAaImIp3nS	patřit
Jardines	Jardines	k1gInSc1	Jardines
de	de	k?	de
la	la	k1gNnSc2	la
Reina	Rein	k2eAgNnSc2d1	Reino
a	a	k8xC	a
Jardines	Jardines	k1gInSc1	Jardines
del	del	k?	del
Rey	Rea	k1gFnSc2	Rea
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
<g/>
.	.	kIx.	.
</s>
<s>
Celoročně	celoročně	k6eAd1	celoročně
jej	on	k3xPp3gNnSc4	on
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
pasáty	pasát	k1gInPc1	pasát
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
celoroční	celoroční	k2eAgFnSc1d1	celoroční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
zpravidla	zpravidla	k6eAd1	zpravidla
neklesá	klesat	k5eNaImIp3nS	klesat
pod	pod	k7c7	pod
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Nejvydatněji	vydatně	k6eAd3	vydatně
prší	pršet	k5eAaImIp3nS	pršet
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
,	,	kIx,	,
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
ročně	ročně	k6eAd1	ročně
spadne	spadnout	k5eAaPmIp3nS	spadnout
600	[number]	k4	600
<g/>
-	-	kIx~	-
<g/>
1200	[number]	k4	1200
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
bývají	bývat	k5eAaImIp3nP	bývat
tyto	tento	k3xDgFnPc1	tento
hodnoty	hodnota	k1gFnPc1	hodnota
až	až	k9	až
dvojnásobné	dvojnásobný	k2eAgFnPc1d1	dvojnásobná
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
hurikány	hurikán	k1gInPc1	hurikán
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
přírodní	přírodní	k2eAgFnPc4d1	přírodní
krásy	krása	k1gFnPc4	krása
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
další	další	k2eAgInPc1d1	další
ostrovy	ostrov	k1gInPc1	ostrov
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
nazývána	nazýván	k2eAgFnSc1d1	nazývána
perlou	perla	k1gFnSc7	perla
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
izolaci	izolace	k1gFnSc3	izolace
od	od	k7c2	od
pevniny	pevnina	k1gFnSc2	pevnina
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
žije	žít	k5eAaImIp3nS	žít
či	či	k8xC	či
roste	růst	k5eAaImIp3nS	růst
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
endemických	endemický	k2eAgMnPc2d1	endemický
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
existovalo	existovat	k5eAaImAgNnS	existovat
14	[number]	k4	14
kubánských	kubánský	k2eAgInPc2d1	kubánský
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pokrývaly	pokrývat	k5eAaImAgInP	pokrývat
téměř	téměř	k6eAd1	téměř
5	[number]	k4	5
<g/>
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
části	část	k1gFnPc1	část
území	území	k1gNnSc2	území
ostrova	ostrov	k1gInSc2	ostrov
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
savany	savana	k1gFnPc1	savana
<g/>
.	.	kIx.	.
</s>
<s>
Nepříliš	příliš	k6eNd1	příliš
husté	hustý	k2eAgInPc1d1	hustý
lesy	les	k1gInPc1	les
tvoří	tvořit	k5eAaImIp3nP	tvořit
stromy	strom	k1gInPc7	strom
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
bobovitých	bobovitý	k2eAgMnPc2d1	bobovitý
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
akáty	akát	k1gInPc4	akát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kubánské	kubánský	k2eAgFnSc2d1	kubánská
borovice	borovice	k1gFnSc2	borovice
a	a	k8xC	a
palmy	palma	k1gFnSc2	palma
-	-	kIx~	-
především	především	k9	především
palma	palma	k1gFnSc1	palma
královská	královský	k2eAgFnSc1d1	královská
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
tropické	tropický	k2eAgInPc1d1	tropický
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
rostou	růst	k5eAaImIp3nP	růst
porosty	porost	k1gInPc7	porost
mangrove	mangrove	k1gNnSc2	mangrove
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
rostou	růst	k5eAaImIp3nP	růst
i	i	k9	i
vzácné	vzácný	k2eAgInPc1d1	vzácný
karibské	karibský	k2eAgInPc1d1	karibský
cykasy	cykas	k1gInPc1	cykas
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
sbírku	sbírka	k1gFnSc4	sbírka
v	v	k7c6	v
ČR	ČR	kA	ČR
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c6	v
Botanické	botanický	k2eAgFnSc6d1	botanická
zahradě	zahrada	k1gFnSc6	zahrada
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
obdělávána	obděláván	k2eAgFnSc1d1	obdělávána
a	a	k8xC	a
řízeně	řízeně	k6eAd1	řízeně
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívána	využívat	k5eAaImNgFnS	využívat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
orné	orný	k2eAgFnSc2d1	orná
půdy	půda	k1gFnSc2	půda
je	být	k5eAaImIp3nS	být
zarostlá	zarostlý	k2eAgFnSc1d1	zarostlá
plevelem	plevel	k1gInSc7	plevel
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
marabu	marabu	k1gMnPc1	marabu
(	(	kIx(	(
<g/>
Dichrostachys	Dichrostachys	k1gInSc1	Dichrostachys
cinerea	cinere	k1gInSc2	cinere
<g/>
,	,	kIx,	,
Marabou-thorn	Marabouhorn	k1gMnSc1	Marabou-thorn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Národním	národní	k2eAgInSc7d1	národní
stromem	strom	k1gInSc7	strom
Kuby	Kuba	k1gFnSc2	Kuba
je	být	k5eAaImIp3nS	být
obří	obří	k2eAgFnSc1d1	obří
palma	palma	k1gFnSc1	palma
královská	královský	k2eAgFnSc1d1	královská
(	(	kIx(	(
<g/>
Palmera	Palmera	k1gFnSc1	Palmera
real	real	k1gInSc1	real
<g/>
,	,	kIx,	,
Roystonea	Roystone	k2eAgFnSc1d1	Roystone
regia	regia	k1gFnSc1	regia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národním	národní	k2eAgInSc7d1	národní
květem	květ	k1gInSc7	květ
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
vonící	vonící	k2eAgFnSc1d1	vonící
mariposa	mariposa	k1gFnSc1	mariposa
(	(	kIx(	(
<g/>
Hedychium	Hedychium	k1gNnSc1	Hedychium
coronarium	coronarium	k1gNnSc1	coronarium
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
motýlek	motýlek	k1gMnSc1	motýlek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
zázvorovitých	zázvorovitý	k2eAgMnPc2d1	zázvorovitý
<g/>
.	.	kIx.	.
</s>
<s>
Národním	národní	k2eAgMnSc7d1	národní
ptákem	pták	k1gMnSc7	pták
s	s	k7c7	s
barvami	barva	k1gFnPc7	barva
kubánské	kubánský	k2eAgFnSc2d1	kubánská
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
barevný	barevný	k2eAgInSc1d1	barevný
tocororo	tocorora	k1gFnSc5	tocorora
(	(	kIx(	(
<g/>
Priotelus	Priotelus	k1gMnSc1	Priotelus
temnurus	temnurus	k1gMnSc1	temnurus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgNnSc1d1	životní
prostředí	prostředí	k1gNnSc1	prostředí
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
narušeno	narušit	k5eAaPmNgNnS	narušit
aktivitami	aktivita	k1gFnPc7	aktivita
státu	stát	k1gInSc2	stát
-	-	kIx~	-
jak	jak	k8xS	jak
klasickým	klasický	k2eAgInSc7d1	klasický
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
stavebními	stavební	k2eAgFnPc7d1	stavební
aktivitami	aktivita	k1gFnPc7	aktivita
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
kontrola	kontrola	k1gFnSc1	kontrola
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
aktivisty	aktivista	k1gMnSc2	aktivista
je	být	k5eAaImIp3nS	být
státem	stát	k1gInSc7	stát
blokována	blokován	k2eAgFnSc1d1	blokována
<g/>
,	,	kIx,	,
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
publicita	publicita	k1gFnSc1	publicita
zakazována	zakazovat	k5eAaImNgFnS	zakazovat
a	a	k8xC	a
kritici	kritik	k1gMnPc1	kritik
znečišťování	znečišťování	k1gNnSc2	znečišťování
jsou	být	k5eAaImIp3nP	být
vězněni	vězněn	k2eAgMnPc1d1	vězněn
<g/>
.	.	kIx.	.
</s>
<s>
Recyklace	recyklace	k1gFnSc1	recyklace
surovin	surovina	k1gFnPc2	surovina
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
bezolovnatý	bezolovnatý	k2eAgInSc4d1	bezolovnatý
benzin	benzin	k1gInSc4	benzin
<g/>
,	,	kIx,	,
při	při	k7c6	při
technických	technický	k2eAgFnPc6d1	technická
kontrolách	kontrola	k1gFnPc6	kontrola
automobilů	automobil	k1gInPc2	automobil
se	se	k3xPyFc4	se
neměří	měřit	k5eNaImIp3nS	měřit
exhalace	exhalace	k1gFnSc1	exhalace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Provincie	provincie	k1gFnSc2	provincie
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
15	[number]	k4	15
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
provincias	provincias	k1gInSc1	provincias
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostrov	ostrov	k1gInSc4	ostrov
Mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
okrsek	okrsek	k1gInSc1	okrsek
<g/>
,	,	kIx,	,
Municipio	Municipio	k1gMnSc1	Municipio
Especial	Especial	k1gMnSc1	Especial
-	-	kIx~	-
Isla	Isla	k1gMnSc1	Isla
de	de	k?	de
la	la	k1gNnSc2	la
Juventud	Juventuda	k1gFnPc2	Juventuda
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
16	[number]	k4	16
celků	celek	k1gInPc2	celek
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
169	[number]	k4	169
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
municipios	municipios	k1gInSc1	municipios
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1492	[number]	k4	1492
Kubu	kubo	k1gNnSc6	kubo
obývaly	obývat	k5eAaImAgInP	obývat
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
Sibonejů	Sibonej	k1gMnPc2	Sibonej
<g/>
,	,	kIx,	,
Tainů	Tain	k1gMnPc2	Tain
<g/>
,	,	kIx,	,
Karibů	Karib	k1gMnPc2	Karib
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
během	během	k7c2	během
španělské	španělský	k2eAgFnSc2d1	španělská
kolonizace	kolonizace	k1gFnSc2	kolonizace
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
na	na	k7c4	na
infekční	infekční	k2eAgFnPc4d1	infekční
nemoci	nemoc	k1gFnPc4	nemoc
zavlečené	zavlečený	k2eAgFnPc4d1	zavlečená
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
jako	jako	k9	jako
neštovice	neštovice	k1gFnPc1	neštovice
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
smísili	smísit	k5eAaPmAgMnP	smísit
se	s	k7c7	s
španělskými	španělský	k2eAgMnPc7d1	španělský
dobyvateli	dobyvatel	k1gMnPc7	dobyvatel
a	a	k8xC	a
původní	původní	k2eAgFnSc2d1	původní
populace	populace	k1gFnSc2	populace
již	již	k6eAd1	již
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
Kubu	Kuba	k1gFnSc4	Kuba
objevil	objevit	k5eAaPmAgMnS	objevit
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
také	také	k9	také
prozkoumal	prozkoumat	k5eAaPmAgMnS	prozkoumat
pobřeží	pobřeží	k1gNnSc2	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Juana	Juan	k1gMnSc4	Juan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1511	[number]	k4	1511
se	se	k3xPyFc4	se
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Baracoa	Baracoum	k1gNnSc2	Baracoum
usadila	usadit	k5eAaPmAgFnS	usadit
první	první	k4xOgFnSc1	první
španělská	španělský	k2eAgFnSc1d1	španělská
kolonie	kolonie	k1gFnSc1	kolonie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Diega	Dieg	k1gMnSc2	Dieg
Velázqueze	Velázqueze	k1gFnSc2	Velázqueze
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
zůstala	zůstat	k5eAaPmAgFnS	zůstat
španělskou	španělský	k2eAgFnSc7d1	španělská
kolonií	kolonie	k1gFnSc7	kolonie
po	po	k7c4	po
následujících	následující	k2eAgNnPc2d1	následující
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
žili	žít	k5eAaImAgMnP	žít
černošští	černošský	k2eAgMnPc1d1	černošský
obyvatelé	obyvatel	k1gMnPc1	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
však	však	k9	však
k	k	k7c3	k
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
plné	plný	k2eAgFnSc3d1	plná
nezávislosti	nezávislost	k1gFnSc3	nezávislost
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
dočkala	dočkat	k5eAaPmAgFnS	dočkat
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
Španělsko-americká	španělskomerický	k2eAgFnSc1d1	španělsko-americká
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
Španělsko	Španělsko	k1gNnSc1	Španělsko
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Kubou	Kuba	k1gFnSc7	Kuba
<g/>
,	,	kIx,	,
Portorikem	Portorico	k1gNnSc7	Portorico
a	a	k8xC	a
Španělskou	španělský	k2eAgFnSc7d1	španělská
východní	východní	k2eAgFnSc7d1	východní
Indií	Indie	k1gFnSc7	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	Krátké	k2eAgNnSc4d1	Krátké
období	období	k1gNnSc4	období
byla	být	k5eAaImAgFnS	být
Kuba	Kuba	k1gFnSc1	Kuba
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Kuba	Kuba	k1gFnSc1	Kuba
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
si	se	k3xPyFc3	se
jako	jako	k8xS	jako
podmínku	podmínka	k1gFnSc4	podmínka
stažení	stažení	k1gNnSc2	stažení
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
udržely	udržet	k5eAaPmAgFnP	udržet
v	v	k7c6	v
pronájmu	pronájem	k1gInSc6	pronájem
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
základnu	základna	k1gFnSc4	základna
Guantánamo	Guantánama	k1gFnSc5	Guantánama
a	a	k8xC	a
zátoku	zátok	k1gInSc2	zátok
Bahía	Bahía	k1gMnSc1	Bahía
Honda	Honda	k1gMnSc1	Honda
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc3	ten
se	se	k3xPyFc4	se
však	však	k9	však
později	pozdě	k6eAd2	pozdě
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
za	za	k7c4	za
rozšíření	rozšíření	k1gNnSc4	rozšíření
základny	základna	k1gFnSc2	základna
v	v	k7c6	v
Guantánamu	Guantánam	k1gInSc6	Guantánam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dostal	dostat	k5eAaPmAgMnS	dostat
seržant	seržant	k1gMnSc1	seržant
Fulgencio	Fulgencio	k1gMnSc1	Fulgencio
Batista	Batista	k1gMnSc1	Batista
y	y	k?	y
Zaldívar	Zaldívar	k1gInSc1	Zaldívar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zavedl	zavést	k5eAaPmAgInS	zavést
nejdříve	dříve	k6eAd3	dříve
relativně	relativně	k6eAd1	relativně
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Kubu	Kuba	k1gFnSc4	Kuba
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
a	a	k8xC	a
poté	poté	k6eAd1	poté
opět	opět	k6eAd1	opět
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
vlády	vláda	k1gFnSc2	vláda
Batista	Batista	k1gMnSc1	Batista
prosadil	prosadit	k5eAaPmAgMnS	prosadit
řadu	řada	k1gFnSc4	řada
sociálně	sociálně	k6eAd1	sociálně
laděných	laděný	k2eAgInPc2d1	laděný
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
znám	znát	k5eAaImIp1nS	znát
dobrou	dobrá	k1gFnSc4	dobrá
spoluprací	spolupráce	k1gFnPc2	spolupráce
s	s	k7c7	s
odbory	odbor	k1gInPc7	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
válku	válka	k1gFnSc4	válka
Japonsku	Japonsko	k1gNnSc3	Japonsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
válečných	válečný	k2eAgFnPc2d1	válečná
operací	operace	k1gFnPc2	operace
se	se	k3xPyFc4	se
však	však	k9	však
Kuba	Kuba	k1gFnSc1	Kuba
přímo	přímo	k6eAd1	přímo
neúčastnila	účastnit	k5eNaImAgFnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
svobodné	svobodný	k2eAgFnSc2d1	svobodná
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc2	který
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
nastávající	nastávající	k1gMnSc1	nastávající
prezident	prezident	k1gMnSc1	prezident
Ramón	Ramón	k1gMnSc1	Ramón
Grau	Graa	k1gMnSc4	Graa
<g/>
.	.	kIx.	.
</s>
<s>
Batista	Batista	k1gMnSc1	Batista
následujících	následující	k2eAgNnPc2d1	následující
8	[number]	k4	8
let	léto	k1gNnPc2	léto
žil	žíla	k1gFnPc2	žíla
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgFnSc2d1	domácí
politiky	politika	k1gFnSc2	politika
se	se	k3xPyFc4	se
však	však	k9	však
účastnil	účastnit	k5eAaImAgMnS	účastnit
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
zvolen	zvolit	k5eAaPmNgMnS	zvolit
senátorem	senátor	k1gMnSc7	senátor
"	"	kIx"	"
<g/>
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
kandidující	kandidující	k2eAgMnSc1d1	kandidující
Batista	Batista	k1gMnSc1	Batista
skončí	skončit	k5eAaPmIp3nS	skončit
na	na	k7c6	na
slabém	slabý	k2eAgMnSc6d1	slabý
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Batista	Batista	k1gMnSc1	Batista
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1952	[number]	k4	1952
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vojenským	vojenský	k2eAgInSc7d1	vojenský
pučem	puč	k1gInSc7	puč
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
následující	následující	k2eAgFnSc1d1	následující
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
již	již	k6eAd1	již
projevovala	projevovat	k5eAaImAgFnS	projevovat
korupcí	korupce	k1gFnSc7	korupce
<g/>
,	,	kIx,	,
policejní	policejní	k2eAgFnSc7d1	policejní
brutalitou	brutalita	k1gFnSc7	brutalita
<g/>
,	,	kIx,	,
překaženými	překažený	k2eAgFnPc7d1	Překažená
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
a	a	k8xC	a
zhoršující	zhoršující	k2eAgInSc4d1	zhoršující
se	se	k3xPyFc4	se
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
situací	situace	k1gFnSc7	situace
<g/>
.	.	kIx.	.
</s>
<s>
Havana	Havana	k1gFnSc1	Havana
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
známým	známý	k2eAgInSc7d1	známý
centrem	centr	k1gInSc7	centr
gamblerů	gambler	k1gMnPc2	gambler
<g/>
,	,	kIx,	,
prostituce	prostituce	k1gFnSc2	prostituce
a	a	k8xC	a
americké	americký	k2eAgFnSc2d1	americká
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
atmosféře	atmosféra	k1gFnSc6	atmosféra
vznikaly	vznikat	k5eAaImAgFnP	vznikat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nejrůznější	různý	k2eAgNnPc1d3	nejrůznější
hnutí	hnutí	k1gNnPc1	hnutí
směřující	směřující	k2eAgNnPc1d1	směřující
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
požadující	požadující	k2eAgFnSc2d1	požadující
svobodné	svobodný	k2eAgFnSc2d1	svobodná
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
kolem	kolem	k7c2	kolem
jistého	jistý	k2eAgMnSc2d1	jistý
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
statkáře	statkář	k1gMnSc2	statkář
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Kuby	Kuba	k1gMnSc2	Kuba
a	a	k8xC	a
člena	člen	k1gMnSc2	člen
Ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1953	[number]	k4	1953
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
známý	známý	k2eAgInSc4d1	známý
útok	útok	k1gInSc4	útok
na	na	k7c4	na
kasárna	kasárna	k1gNnPc4	kasárna
Moncada	Moncada	k1gFnSc1	Moncada
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Raúlem	Raúlo	k1gNnSc7	Raúlo
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
členy	člen	k1gInPc7	člen
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
na	na	k7c6	na
Ostrově	ostrov	k1gInSc6	ostrov
borovic	borovice	k1gFnPc2	borovice
(	(	kIx(	(
<g/>
Isla	Isla	k1gFnSc1	Isla
de	de	k?	de
los	los	k1gInSc1	los
Pinos	Pinos	k1gInSc1	Pinos
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
ostrov	ostrov	k1gInSc1	ostrov
mládeže	mládež	k1gFnSc2	mládež
Isla	Islum	k1gNnSc2	Islum
de	de	k?	de
la	la	k1gNnSc2	la
Juventud	Juventuda	k1gFnPc2	Juventuda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
<g/>
,	,	kIx,	,
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
marně	marně	k6eAd1	marně
snažil	snažit	k5eAaImAgMnS	snažit
prosadit	prosadit	k5eAaPmF	prosadit
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
zmedializoval	zmedializovat	k5eAaImAgMnS	zmedializovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
Castro	Castro	k1gNnSc1	Castro
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
své	svůj	k3xOyFgNnSc4	svůj
Hnutí	hnutí	k1gNnSc4	hnutí
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
základ	základ	k1gInSc1	základ
budoucí	budoucí	k2eAgFnSc2d1	budoucí
nové	nový	k2eAgFnSc2d1	nová
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hlasování	hlasování	k1gNnSc6	hlasování
v	v	k7c6	v
kubánském	kubánský	k2eAgInSc6d1	kubánský
parlamentu	parlament	k1gInSc6	parlament
o	o	k7c6	o
amnestii	amnestie	k1gFnSc6	amnestie
byli	být	k5eAaImAgMnP	být
bratři	bratr	k1gMnPc1	bratr
Castrovi	Castrův	k2eAgMnPc1d1	Castrův
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
propuštěni	propustit	k5eAaPmNgMnP	propustit
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
se	se	k3xPyFc4	se
Castro	Castro	k1gNnSc4	Castro
obával	obávat	k5eAaImAgMnS	obávat
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
proto	proto	k8xC	proto
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sestavil	sestavit	k5eAaPmAgMnS	sestavit
a	a	k8xC	a
vycvičil	vycvičit	k5eAaPmAgMnS	vycvičit
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jednoho	jeden	k4xCgMnSc4	jeden
dne	den	k1gInSc2	den
měla	mít	k5eAaImAgFnS	mít
vylodit	vylodit	k5eAaPmF	vylodit
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
Batista	Batista	k1gMnSc1	Batista
široce	široko	k6eAd1	široko
nenáviděným	nenáviděný	k2eAgMnSc7d1	nenáviděný
diktátorem	diktátor	k1gMnSc7	diktátor
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Kuba	Kuba	k1gFnSc1	Kuba
v	v	k7c6	v
období	období	k1gNnSc6	období
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejbohatších	bohatý	k2eAgFnPc2d3	nejbohatší
zemí	zem	k1gFnPc2	zem
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
gramotnosti	gramotnost	k1gFnSc2	gramotnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
pádem	pád	k1gInSc7	pád
necelých	celý	k2eNgNnPc6d1	necelé
80	[number]	k4	80
%	%	kIx~	%
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1956	[number]	k4	1956
připlul	připlout	k5eAaPmAgMnS	připlout
do	do	k7c2	do
země	zem	k1gFnSc2	zem
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
na	na	k7c6	na
známé	známý	k2eAgFnSc6d1	známá
jachtě	jachta	k1gFnSc6	jachta
Granma	Granma	k1gFnSc1	Granma
(	(	kIx(	(
<g/>
babička	babička	k1gFnSc1	babička
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vyvolat	vyvolat	k5eAaPmF	vyvolat
revoluci	revoluce	k1gFnSc4	revoluce
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
Fulgencia	Fulgencium	k1gNnSc2	Fulgencium
Batisty	batist	k1gInPc1	batist
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylodění	vylodění	k1gNnSc6	vylodění
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
Oriente	Orient	k1gInSc5	Orient
<g/>
)	)	kIx)	)
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
postupně	postupně	k6eAd1	postupně
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
tzv.	tzv.	kA	tzv.
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1959	[number]	k4	1959
vítězstvím	vítězství	k1gNnSc7	vítězství
revolucionářů	revolucionář	k1gMnPc2	revolucionář
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Havany	Havana	k1gFnSc2	Havana
triumfálně	triumfálně	k6eAd1	triumfálně
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
s	s	k7c7	s
příslibem	příslib	k1gInSc7	příslib
brzkých	brzký	k2eAgFnPc2d1	brzká
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nekonaly	konat	k5eNaImAgFnP	konat
<g/>
.	.	kIx.	.
</s>
<s>
Castrovi	Castrův	k2eAgMnPc1d1	Castrův
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
Ernesto	Ernesta	k1gMnSc5	Ernesta
Guevara	Guevar	k1gMnSc4	Guevar
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Raúl	Raúl	k1gMnSc1	Raúl
Castro	Castro	k1gNnSc4	Castro
byli	být	k5eAaImAgMnP	být
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
přesvědčenými	přesvědčený	k2eAgMnPc7d1	přesvědčený
marxisty	marxista	k1gMnPc7	marxista
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Raúl	Raúl	k1gMnSc1	Raúl
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
levého	levý	k2eAgNnSc2d1	levé
křídla	křídlo	k1gNnSc2	křídlo
místní	místní	k2eAgFnSc2d1	místní
strany	strana	k1gFnSc2	strana
PSP	PSP	kA	PSP
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc2d1	původní
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
tak	tak	k6eAd1	tak
začaly	začít	k5eAaPmAgInP	začít
první	první	k4xOgInPc1	první
protesty	protest	k1gInPc1	protest
proti	proti	k7c3	proti
vlivu	vliv	k1gInSc3	vliv
komunismu	komunismus	k1gInSc2	komunismus
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
Batista	Batista	k1gMnSc1	Batista
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1958	[number]	k4	1958
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
nastal	nastat	k5eAaPmAgInS	nastat
rychlý	rychlý	k2eAgInSc1d1	rychlý
exodus	exodus	k1gInSc1	exodus
obyvatel	obyvatel	k1gMnPc2	obyvatel
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
zatýkání	zatýkání	k1gNnSc2	zatýkání
a	a	k8xC	a
masové	masový	k2eAgFnSc2d1	masová
popravy	poprava	k1gFnSc2	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
prokomunistický	prokomunistický	k2eAgInSc4d1	prokomunistický
vývoj	vývoj	k1gInSc4	vývoj
revoluce	revoluce	k1gFnSc2	revoluce
zahájily	zahájit	k5eAaPmAgFnP	zahájit
tisícovky	tisícovka	k1gFnPc1	tisícovka
Kubánců	Kubánec	k1gMnPc2	Kubánec
proticastrovské	proticastrovský	k2eAgNnSc1d1	proticastrovský
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Sierra	Sierra	k1gFnSc1	Sierra
Escambray	Escambraa	k1gFnPc1	Escambraa
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgNnSc4	který
Castro	Castro	k1gNnSc4	Castro
reagoval	reagovat	k5eAaBmAgInS	reagovat
masovým	masový	k2eAgNnSc7d1	masové
vystěhováváním	vystěhovávání	k1gNnSc7	vystěhovávání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Pinar	Pinar	k1gInSc1	Pinar
del	del	k?	del
Río	Río	k1gFnSc2	Río
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zablokoval	zablokovat	k5eAaPmAgMnS	zablokovat
jejich	jejich	k3xOp3gFnSc4	jejich
podporu	podpora	k1gFnSc4	podpora
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100.000	[number]	k4	100.000
obyvatel	obyvatel	k1gMnPc2	obyvatel
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
a	a	k8xC	a
zavřeno	zavřít	k5eAaPmNgNnS	zavřít
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
z	z	k7c2	z
Batistovy	Batistovy	k?	Batistovy
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
kapacitně	kapacitně	k6eAd1	kapacitně
nedostačovala	dostačovat	k5eNaImAgFnS	dostačovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jako	jako	k9	jako
věznice	věznice	k1gFnPc1	věznice
musely	muset	k5eAaImAgFnP	muset
tehdy	tehdy	k6eAd1	tehdy
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
některé	některý	k3yIgInPc4	některý
stadiony	stadion	k1gInPc4	stadion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
revolučních	revoluční	k2eAgFnPc2d1	revoluční
jednotek	jednotka	k1gFnPc2	jednotka
stáli	stát	k5eAaImAgMnP	stát
tzv.	tzv.	kA	tzv.
vousáči	vousáč	k1gMnPc1	vousáč
-	-	kIx~	-
"	"	kIx"	"
<g/>
barbudos	barbudos	k1gInSc1	barbudos
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zapřisáhli	zapřisáhnout	k5eAaPmAgMnP	zapřisáhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neoholí	oholit	k5eNaPmIp3nS	oholit
až	až	k9	až
do	do	k7c2	do
vítězství	vítězství	k1gNnSc2	vítězství
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
osobnosti	osobnost	k1gFnPc1	osobnost
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
staly	stát	k5eAaPmAgFnP	stát
vedoucími	vedoucí	k2eAgMnPc7d1	vedoucí
představiteli	představitel	k1gMnPc7	představitel
porevoluční	porevoluční	k2eAgFnSc2d1	porevoluční
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgFnPc4	tento
hlavní	hlavní	k2eAgFnPc4d1	hlavní
vojenské	vojenský	k2eAgFnPc4d1	vojenská
vůdce	vůdce	k1gMnSc2	vůdce
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
comandante	comandant	k1gMnSc5	comandant
<g/>
)	)	kIx)	)
patřili	patřit	k5eAaImAgMnP	patřit
mj.	mj.	kA	mj.
Frank	frank	k1gInSc1	frank
País	País	k1gInSc1	País
<g/>
,	,	kIx,	,
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc1	Castro
<g/>
,	,	kIx,	,
Huber	Huber	k1gMnSc1	Huber
Matos	Matos	k1gMnSc1	Matos
<g/>
,	,	kIx,	,
Rolando	Rolanda	k1gFnSc5	Rolanda
Cubela	Cubela	k1gFnSc1	Cubela
<g/>
,	,	kIx,	,
Camilo	Camila	k1gFnSc5	Camila
Cienfuegos	Cienfuegosa	k1gFnPc2	Cienfuegosa
a	a	k8xC	a
Ernesto	Ernesta	k1gMnSc5	Ernesta
Guevara	Guevar	k1gMnSc2	Guevar
zvaný	zvaný	k2eAgInSc1d1	zvaný
Che	che	k0	che
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
osudy	osud	k1gInPc1	osud
byly	být	k5eAaImAgInP	být
rozmanité	rozmanitý	k2eAgInPc4d1	rozmanitý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
až	až	k9	až
na	na	k7c4	na
bratry	bratr	k1gMnPc4	bratr
Castry	Castr	k1gMnPc4	Castr
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
tragické	tragický	k2eAgNnSc1d1	tragické
<g/>
.	.	kIx.	.
</s>
<s>
Frank	frank	k1gInSc1	frank
País	País	k1gInSc1	País
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
Batistovou	batistový	k2eAgFnSc7d1	Batistová
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Huber	Huber	k1gMnSc1	Huber
Matos	Matos	k1gMnSc1	Matos
podal	podat	k5eAaPmAgMnS	podat
demisi	demise	k1gFnSc4	demise
již	již	k6eAd1	již
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Demisi	demise	k1gFnSc4	demise
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
nejdříve	dříve	k6eAd3	dříve
osobně	osobně	k6eAd1	osobně
F.	F.	kA	F.
Castrovi	Castrův	k2eAgMnPc1d1	Castrův
nesouhlasem	nesouhlas	k1gInSc7	nesouhlas
s	s	k7c7	s
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
směřováním	směřování	k1gNnSc7	směřování
země	zem	k1gFnSc2	zem
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ji	on	k3xPp3gFnSc4	on
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
i	i	k9	i
ve	v	k7c6	v
zdvořilém	zdvořilý	k2eAgInSc6d1	zdvořilý
dopise	dopis	k1gInSc6	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
proto	proto	k6eAd1	proto
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
20	[number]	k4	20
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
odsloužil	odsloužit	k5eAaPmAgMnS	odsloužit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
vypršení	vypršení	k1gNnSc6	vypršení
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zatčením	zatčení	k1gNnSc7	zatčení
"	"	kIx"	"
<g/>
zrádce	zrádce	k1gMnSc1	zrádce
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgInS	pověřit
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
Camilo	Camila	k1gFnSc5	Camila
Cienfuegos	Cienfuegos	k1gMnSc1	Cienfuegos
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sám	sám	k3xTgMnSc1	sám
projevil	projevit	k5eAaPmAgMnS	projevit
jisté	jistý	k2eAgNnSc4d1	jisté
překvapení	překvapení	k1gNnSc4	překvapení
nad	nad	k7c7	nad
tímto	tento	k3xDgMnSc7	tento
postupem	postupem	k7c2	postupem
bratrů	bratr	k1gMnPc2	bratr
Castrových	Castrův	k2eAgMnPc2d1	Castrův
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
poté	poté	k6eAd1	poté
jeho	jeho	k3xOp3gMnSc3	jeho
letadlo	letadlo	k1gNnSc1	letadlo
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
z	z	k7c2	z
radarů	radar	k1gInPc2	radar
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
přeletů	přelet	k1gInPc2	přelet
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Che	che	k0	che
Guevarův	Guevarův	k2eAgInSc1d1	Guevarův
odjezd	odjezd	k1gInSc4	odjezd
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
problematický	problematický	k2eAgInSc1d1	problematický
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
stále	stále	k6eAd1	stále
dominantnějšímu	dominantní	k2eAgMnSc3d2	dominantnější
Fidelu	Fidel	k1gMnSc3	Fidel
Castrovi	Castr	k1gMnSc3	Castr
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
středem	střed	k1gInSc7	střed
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Guevara	Guevara	k1gFnSc1	Guevara
byl	být	k5eAaImAgInS	být
popraven	popraven	k2eAgMnSc1d1	popraven
v	v	k7c6	v
Bolívii	Bolívie	k1gFnSc6	Bolívie
při	při	k7c6	při
dalším	další	k2eAgMnSc6d1	další
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
světovou	světový	k2eAgFnSc4d1	světová
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Rolando	Rolando	k6eAd1	Rolando
Cubela	Cubela	k1gFnSc1	Cubela
byl	být	k5eAaImAgInS	být
zatčen	zatčen	k2eAgMnSc1d1	zatčen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
po	po	k7c4	po
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
plánu	plán	k1gInSc2	plán
zavraždit	zavraždit	k5eAaPmF	zavraždit
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Cubela	Cubela	k1gFnSc1	Cubela
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Kuby	Kuba	k1gFnSc2	Kuba
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
revoluce	revoluce	k1gFnSc2	revoluce
nebyla	být	k5eNaImAgFnS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
revolučního	revoluční	k2eAgNnSc2d1	revoluční
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
nesla	nést	k5eAaImAgFnS	nést
nové	nový	k2eAgNnSc4d1	nové
jméno	jméno	k1gNnSc4	jméno
Lidové	lidový	k2eAgFnSc2d1	lidová
socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
PSP	PSP	kA	PSP
<g/>
)	)	kIx)	)
a	a	k8xC	a
angažovala	angažovat	k5eAaBmAgFnS	angažovat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
demokratickým	demokratický	k2eAgFnPc3d1	demokratická
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
hnutí	hnutí	k1gNnSc6	hnutí
a	a	k8xC	a
následných	následný	k2eAgInPc6d1	následný
bojích	boj	k1gInPc6	boj
angažovali	angažovat	k5eAaBmAgMnP	angažovat
(	(	kIx(	(
<g/>
především	především	k9	především
Raúl	Raúl	k1gInSc1	Raúl
Castro	Castro	k1gNnSc1	Castro
a	a	k8xC	a
Blas	Blas	k1gInSc1	Blas
Roca	Rocum	k1gNnSc2	Rocum
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
disidenta	disident	k1gMnSc2	disident
Vladimira	Vladimir	k1gMnSc2	Vladimir
Rocy	Roca	k1gMnSc2	Roca
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
KSK	KSK	kA	KSK
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
její	její	k3xOp3gFnSc7	její
přímou	přímý	k2eAgFnSc7d1	přímá
následovnicí	následovnice	k1gFnSc7	následovnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
se	se	k3xPyFc4	se
především	především	k9	především
z	z	k7c2	z
Castrova	Castrův	k2eAgNnSc2d1	Castrovo
Hnutí	hnutí	k1gNnSc2	hnutí
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
pohltilo	pohltit	k5eAaPmAgNnS	pohltit
další	další	k2eAgFnPc4d1	další
revoluční	revoluční	k2eAgFnPc4d1	revoluční
organizace	organizace	k1gFnPc4	organizace
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
ORI	ORI	kA	ORI
-	-	kIx~	-
Sloučené	sloučený	k2eAgFnSc2d1	sloučená
revoluční	revoluční	k2eAgFnSc2d1	revoluční
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ORI	ORI	kA	ORI
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
i	i	k9	i
původní	původní	k2eAgFnSc1d1	původní
historická	historický	k2eAgFnSc1d1	historická
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
PSP	PSP	kA	PSP
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
strana	strana	k1gFnSc1	strana
ORI	ORI	kA	ORI
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
transformovala	transformovat	k5eAaBmAgFnS	transformovat
v	v	k7c6	v
PURSC	PURSC	kA	PURSC
(	(	kIx(	(
<g/>
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
strana	strana	k1gFnSc1	strana
kubánské	kubánský	k2eAgFnSc2d1	kubánská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Kuby	Kuba	k1gFnSc2	Kuba
KSK	KSK	kA	KSK
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
šestičlenný	šestičlenný	k2eAgInSc1d1	šestičlenný
sekretariát	sekretariát	k1gInSc1	sekretariát
(	(	kIx(	(
<g/>
F.	F.	kA	F.
Castro	Castro	k1gNnSc1	Castro
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Castro	Castro	k1gNnSc1	Castro
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Dorticos	Dorticos	k1gInSc1	Dorticos
<g/>
,	,	kIx,	,
C.	C.	kA	C.
<g/>
R.	R.	kA	R.
Rodríguez	Rodrígueza	k1gFnPc2	Rodrígueza
<g/>
,	,	kIx,	,
Blas	Blasa	k1gFnPc2	Blasa
Roca	Roc	k1gInSc2	Roc
a	a	k8xC	a
Faure	Faur	k1gInSc5	Faur
Chomón	Chomón	k1gInSc1	Chomón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
byly	být	k5eAaImAgInP	být
zakázány	zakázat	k5eAaPmNgInP	zakázat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
mohl	moct	k5eAaImAgMnS	moct
světu	svět	k1gInSc3	svět
veřejně	veřejně	k6eAd1	veřejně
potvrdit	potvrdit	k5eAaPmF	potvrdit
to	ten	k3xDgNnSc4	ten
co	co	k9	co
dosud	dosud	k6eAd1	dosud
neprohlašoval	prohlašovat	k5eNaImAgMnS	prohlašovat
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
revoluce	revoluce	k1gFnSc1	revoluce
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
byla	být	k5eAaImAgFnS	být
revolucí	revoluce	k1gFnSc7	revoluce
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
toto	tento	k3xDgNnSc4	tento
pronesl	pronést	k5eAaPmAgMnS	pronést
při	při	k7c6	při
uctění	uctění	k1gNnSc6	uctění
památky	památka	k1gFnSc2	památka
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
bombardování	bombardování	k1gNnSc2	bombardování
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
Manuela	Manuel	k1gMnSc2	Manuel
Urrutii	Urrutie	k1gFnSc4	Urrutie
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Osvaldo	Osvalda	k1gFnSc5	Osvalda
Dorticós	Dorticós	k1gInSc1	Dorticós
Torrado	Torrada	k1gFnSc5	Torrada
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
vedl	vést	k5eAaImAgMnS	vést
první	první	k4xOgFnSc4	první
porevoluční	porevoluční	k2eAgFnSc4d1	porevoluční
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Invaze	invaze	k1gFnSc2	invaze
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neměl	mít	k5eNaImAgMnS	mít
a	a	k8xC	a
priori	priori	k6eAd1	priori
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
rozkol	rozkol	k1gInSc1	rozkol
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vymanit	vymanit	k5eAaPmF	vymanit
se	se	k3xPyFc4	se
z	z	k7c2	z
jednostranné	jednostranný	k2eAgFnSc2d1	jednostranná
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
USA	USA	kA	USA
(	(	kIx(	(
<g/>
které	který	k3yQgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
např.	např.	kA	např.
kontrolovaly	kontrolovat	k5eAaImAgFnP	kontrolovat
40	[number]	k4	40
<g/>
%	%	kIx~	%
produkce	produkce	k1gFnSc2	produkce
cukru	cukr	k1gInSc2	cukr
<g/>
)	)	kIx)	)
a	a	k8xC	a
těžbu	těžba	k1gFnSc4	těžba
veškerého	veškerý	k3xTgNnSc2	veškerý
nerostného	nerostný	k2eAgNnSc2d1	nerostné
bohatství	bohatství	k1gNnSc2	bohatství
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
politiky	politika	k1gFnSc2	politika
byl	být	k5eAaImAgInS	být
pravý	pravý	k2eAgInSc1d1	pravý
opak	opak	k1gInSc1	opak
-	-	kIx~	-
totální	totální	k2eAgFnSc1d1	totální
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
dolech	dol	k1gInPc6	dol
a	a	k8xC	a
RVHP	RVHP	kA	RVHP
<g/>
.	.	kIx.	.
</s>
<s>
Narůstající	narůstající	k2eAgFnSc1d1	narůstající
podpora	podpora	k1gFnSc1	podpora
Kuby	Kuba	k1gFnSc2	Kuba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
socialistických	socialistický	k2eAgFnPc2d1	socialistická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
především	především	k9	především
však	však	k9	však
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
USA	USA	kA	USA
velmi	velmi	k6eAd1	velmi
negativně	negativně	k6eAd1	negativně
vnímána	vnímat	k5eAaImNgFnS	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
Podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
dohoda	dohoda	k1gFnSc1	dohoda
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
znamenala	znamenat	k5eAaImAgFnS	znamenat
příliv	příliv	k1gInSc4	příliv
subvencovaného	subvencovaný	k2eAgNnSc2d1	subvencované
zboží	zboží	k1gNnSc2	zboží
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
včetně	včetně	k7c2	včetně
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
americké	americký	k2eAgFnPc1d1	americká
rafinérie	rafinérie	k1gFnPc1	rafinérie
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrocenou	vyhrocený	k2eAgFnSc4d1	vyhrocená
situaci	situace	k1gFnSc4	situace
Castro	Castra	k1gFnSc5	Castra
řešil	řešit	k5eAaImAgMnS	řešit
jejich	jejich	k3xOp3gNnPc3	jejich
znárodněním	znárodnění	k1gNnPc3	znárodnění
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
i	i	k9	i
na	na	k7c4	na
banky	banka	k1gFnPc4	banka
<g/>
,	,	kIx,	,
podniky	podnik	k1gInPc4	podnik
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
sektory	sektor	k1gInPc4	sektor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
znárodnění	znárodnění	k1gNnSc6	znárodnění
majetku	majetek	k1gInSc2	majetek
amerických	americký	k2eAgFnPc2d1	americká
firem	firma	k1gFnPc2	firma
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1960	[number]	k4	1960
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
USA	USA	kA	USA
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
embargo	embargo	k1gNnSc1	embargo
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
amerických	americký	k2eAgInPc2d1	americký
výrobků	výrobek	k1gInPc2	výrobek
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dále	daleko	k6eAd2	daleko
posílily	posílit	k5eAaPmAgFnP	posílit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
ránu	rána	k1gFnSc4	rána
vztahům	vztah	k1gInPc3	vztah
mezi	mezi	k7c7	mezi
Kubou	Kuba	k1gFnSc7	Kuba
a	a	k8xC	a
USA	USA	kA	USA
dalo	dát	k5eAaPmAgNnS	dát
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
vylodění	vylodění	k1gNnSc1	vylodění
exilových	exilový	k2eAgMnPc2d1	exilový
Kubánců	Kubánec	k1gMnPc2	Kubánec
původně	původně	k6eAd1	původně
podporovaný	podporovaný	k2eAgMnSc1d1	podporovaný
CIA	CIA	kA	CIA
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	sviní	k2eAgFnSc6d1	sviní
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Eisenhowera	Eisenhower	k1gMnSc2	Eisenhower
také	také	k9	také
z	z	k7c2	z
valnou	valný	k2eAgFnSc7d1	valná
měrou	míra	k1gFnSc7wR	míra
vyzbrojila	vyzbrojit	k5eAaPmAgFnS	vyzbrojit
<g/>
.	.	kIx.	.
</s>
<s>
Přípravy	příprava	k1gFnPc1	příprava
na	na	k7c6	na
invazi	invaze	k1gFnSc6	invaze
však	však	k9	však
narušily	narušit	k5eAaPmAgInP	narušit
volby	volba	k1gFnSc2	volba
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
však	však	k9	však
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
původně	původně	k6eAd1	původně
přislíbenou	přislíbený	k2eAgFnSc4d1	přislíbená
rozsáhlejší	rozsáhlý	k2eAgFnSc4d2	rozsáhlejší
pomoc	pomoc	k1gFnSc4	pomoc
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Započatá	započatý	k2eAgFnSc1d1	započatá
invaze	invaze	k1gFnSc1	invaze
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
proto	proto	k8xC	proto
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poblíž	poblíž	k7c2	poblíž
ostrova	ostrov	k1gInSc2	ostrov
čekala	čekat	k5eAaImAgFnS	čekat
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
a	a	k8xC	a
výsadkové	výsadkový	k2eAgFnPc1d1	výsadková
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
Kennedy	Kenned	k1gMnPc7	Kenned
neudělil	udělit	k5eNaPmAgMnS	udělit
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
konala	konat	k5eAaImAgFnS	konat
provizorně	provizorně	k6eAd1	provizorně
bez	bez	k7c2	bez
přislíbené	přislíbený	k2eAgFnSc2d1	přislíbená
letecké	letecký	k2eAgFnSc2d1	letecká
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Kubánská	kubánský	k2eAgNnPc1d1	kubánské
vojska	vojsko	k1gNnPc1	vojsko
při	při	k7c6	při
střetu	střet	k1gInSc6	střet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
naopak	naopak	k6eAd1	naopak
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
masivně	masivně	k6eAd1	masivně
využívala	využívat	k5eAaPmAgFnS	využívat
sovětské	sovětský	k2eAgInPc4d1	sovětský
a	a	k8xC	a
československé	československý	k2eAgFnPc4d1	Československá
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
invazi	invaze	k1gFnSc4	invaze
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
překonala	překonat	k5eAaPmAgFnS	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
svržení	svržení	k1gNnSc4	svržení
režimu	režim	k1gInSc2	režim
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
tedy	tedy	k9	tedy
skončil	skončit	k5eAaPmAgMnS	skončit
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
zbraně	zbraň	k1gFnPc1	zbraň
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
najdete	najít	k5eAaPmIp2nP	najít
i	i	k9	i
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
Girón	Girón	k1gInSc4	Girón
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
USA	USA	kA	USA
rozmístily	rozmístit	k5eAaPmAgFnP	rozmístit
rakety	raketa	k1gFnPc1	raketa
středního	střední	k2eAgInSc2d1	střední
doletu	dolet	k1gInSc2	dolet
Jupiter	Jupiter	k1gMnSc1	Jupiter
vyzbrojené	vyzbrojený	k2eAgInPc1d1	vyzbrojený
jadernými	jaderný	k2eAgFnPc7d1	jaderná
hlavicemi	hlavice	k1gFnPc7	hlavice
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
požádal	požádat	k5eAaPmAgMnS	požádat
SSSR	SSSR	kA	SSSR
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
americkou	americký	k2eAgFnSc7d1	americká
agresí	agrese	k1gFnSc7	agrese
<g/>
,	,	kIx,	,
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
sovětské	sovětský	k2eAgNnSc1d1	sovětské
vedení	vedení	k1gNnSc1	vedení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Nikitou	Nikita	k1gMnSc7	Nikita
Chruščovem	Chruščov	k1gInSc7	Chruščov
tajně	tajně	k6eAd1	tajně
rozmístit	rozmístit	k5eAaPmF	rozmístit
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
rakety	raketa	k1gFnSc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
leteckých	letecký	k2eAgInPc6d1	letecký
snímcích	snímek	k1gInPc6	snímek
pořízených	pořízený	k2eAgInPc6d1	pořízený
americkou	americký	k2eAgFnSc7d1	americká
CIA	CIA	kA	CIA
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kubánci	Kubánec	k1gMnPc1	Kubánec
staví	stavit	k5eAaPmIp3nP	stavit
odpalovací	odpalovací	k2eAgFnPc4d1	odpalovací
rampy	rampa	k1gFnPc4	rampa
pro	pro	k7c4	pro
rakety	raketa	k1gFnPc4	raketa
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
dozvěděl	dozvědět	k5eAaPmAgInS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
již	již	k6eAd1	již
míří	mířit	k5eAaImIp3nS	mířit
sovětský	sovětský	k2eAgInSc4d1	sovětský
konvoj	konvoj	k1gInSc4	konvoj
s	s	k7c7	s
raketami	raketa	k1gFnPc7	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvážení	zvážení	k1gNnSc6	zvážení
možných	možný	k2eAgFnPc2d1	možná
reakcí	reakce	k1gFnPc2	reakce
–	–	k?	–
mj.	mj.	kA	mj.
invaze	invaze	k1gFnSc2	invaze
na	na	k7c6	na
Kubu	kubo	k1gNnSc6	kubo
nebo	nebo	k8xC	nebo
bombardování	bombardování	k1gNnSc6	bombardování
odpalovacích	odpalovací	k2eAgFnPc2d1	odpalovací
ramp	rampa	k1gFnPc2	rampa
–	–	k?	–
se	se	k3xPyFc4	se
USA	USA	kA	USA
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
námořní	námořní	k2eAgFnSc4d1	námořní
blokádu	blokáda	k1gFnSc4	blokáda
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
změnily	změnit	k5eAaPmAgInP	změnit
sovětské	sovětský	k2eAgInPc1d1	sovětský
konvoje	konvoj	k1gInPc1	konvoj
směr	směr	k1gInSc1	směr
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
Nikita	Nikit	k1gMnSc2	Nikit
S.	S.	kA	S.
Chruščov	Chruščov	k1gInSc1	Chruščov
oficiálně	oficiálně	k6eAd1	oficiálně
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
SSSR	SSSR	kA	SSSR
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
své	svůj	k3xOyFgFnPc4	svůj
rakety	raketa	k1gFnPc4	raketa
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
USA	USA	kA	USA
zavážou	zavázat	k5eAaPmIp3nP	zavázat
neprovést	provést	k5eNaPmF	provést
invazi	invaze	k1gFnSc4	invaze
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
a	a	k8xC	a
stáhnou	stáhnout	k5eAaPmIp3nP	stáhnout
rakety	raketa	k1gFnPc1	raketa
středního	střední	k2eAgInSc2d1	střední
doletu	dolet	k1gInSc2	dolet
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
dohodou	dohoda	k1gFnSc7	dohoda
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
hrozící	hrozící	k2eAgFnSc1d1	hrozící
jaderná	jaderný	k2eAgFnSc1d1	jaderná
krize	krize	k1gFnSc1	krize
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
USA	USA	kA	USA
se	se	k3xPyFc4	se
zavázaly	zavázat	k5eAaPmAgInP	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepodniknou	podniknout	k5eNaPmIp3nP	podniknout
ani	ani	k8xC	ani
nepodpoří	podpořit	k5eNaPmIp3nP	podpořit
žádné	žádný	k3yNgFnPc4	žádný
vojenské	vojenský	k2eAgFnPc4d1	vojenská
akce	akce	k1gFnPc4	akce
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
diktátorského	diktátorský	k2eAgInSc2d1	diktátorský
režimu	režim	k1gInSc2	režim
rodiny	rodina	k1gFnSc2	rodina
Castrů	Castr	k1gMnPc2	Castr
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Kuba	Kuba	k1gFnSc1	Kuba
pokusila	pokusit	k5eAaPmAgFnS	pokusit
naplnit	naplnit	k5eAaPmF	naplnit
Che	che	k0	che
Guevarovo	Guevarův	k2eAgNnSc1d1	Guevarovo
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
i	i	k9	i
více	hodně	k6eAd2	hodně
Vietnamů	Vietnam	k1gInPc2	Vietnam
<g/>
"	"	kIx"	"
a	a	k8xC	a
vyvolat	vyvolat	k5eAaPmF	vyvolat
či	či	k8xC	či
podpořit	podpořit	k5eAaPmF	podpořit
revoluce	revoluce	k1gFnPc4	revoluce
v	v	k7c6	v
rozvojovém	rozvojový	k2eAgInSc6d1	rozvojový
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
byly	být	k5eAaImAgFnP	být
expediční	expediční	k2eAgFnPc1d1	expediční
síly	síla	k1gFnPc1	síla
vyslány	vyslat	k5eAaPmNgFnP	vyslat
do	do	k7c2	do
válkou	válka	k1gFnSc7	válka
rozvráceného	rozvrácený	k2eAgNnSc2d1	rozvrácené
Konga	Kongo	k1gNnSc2	Kongo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
později	pozdě	k6eAd2	pozdě
popravený	popravený	k2eAgMnSc1d1	popravený
generál	generál	k1gMnSc1	generál
Ochoa	Ochoa	k1gMnSc1	Ochoa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
angažoval	angažovat	k5eAaBmAgInS	angažovat
i	i	k9	i
při	při	k7c6	při
tajných	tajný	k2eAgFnPc6d1	tajná
a	a	k8xC	a
prohraných	prohraný	k2eAgFnPc6d1	prohraná
misích	mise	k1gFnPc6	mise
do	do	k7c2	do
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
popravě	poprava	k1gFnSc6	poprava
Che	che	k0	che
Guevary	Guevar	k1gInPc4	Guevar
v	v	k7c6	v
Bolívii	Bolívie	k1gFnSc6	Bolívie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
pokusily	pokusit	k5eAaPmAgInP	pokusit
zlepšit	zlepšit	k5eAaPmF	zlepšit
vztahy	vztah	k1gInPc1	vztah
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
amerického	americký	k2eAgMnSc2d1	americký
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Henryho	Henry	k1gMnSc2	Henry
Kissingera	Kissinger	k1gMnSc2	Kissinger
však	však	k9	však
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
výrazné	výrazný	k2eAgFnSc2d1	výrazná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
angažovanosti	angažovanost	k1gFnSc2	angažovanost
Castrovy	Castrův	k2eAgFnSc2d1	Castrova
vlády	vláda	k1gFnSc2	vláda
během	během	k7c2	během
Angolské	angolský	k2eAgFnSc2d1	angolská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc4	účast
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kubánských	kubánský	k2eAgMnPc2d1	kubánský
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
konfliktu	konflikt	k1gInSc6	konflikt
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
totiž	totiž	k9	totiž
Američané	Američan	k1gMnPc1	Američan
nemohli	moct	k5eNaImAgMnP	moct
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
byly	být	k5eAaImAgFnP	být
kubánské	kubánský	k2eAgFnPc1d1	kubánská
expediční	expediční	k2eAgFnPc1d1	expediční
síly	síla	k1gFnPc1	síla
generála	generál	k1gMnSc2	generál
Ochoy	Ochoa	k1gFnSc2	Ochoa
vyslány	vyslat	k5eAaPmNgFnP	vyslat
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
sovětského	sovětský	k2eAgMnSc2d1	sovětský
generála	generál	k1gMnSc2	generál
Petrova	Petrov	k1gInSc2	Petrov
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
Somálským	somálský	k2eAgMnPc3d1	somálský
rebelům	rebel	k1gMnPc3	rebel
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
špatná	špatný	k2eAgFnSc1d1	špatná
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
zájem	zájem	k1gInSc4	zájem
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
o	o	k7c6	o
"	"	kIx"	"
<g/>
hlasování	hlasování	k1gNnSc6	hlasování
nohama	noha	k1gFnPc7	noha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1980	[number]	k4	1980
masově	masově	k6eAd1	masově
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
politický	politický	k2eAgInSc4d1	politický
azyl	azyl	k1gInSc4	azyl
na	na	k7c6	na
peruánském	peruánský	k2eAgNnSc6d1	peruánské
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
postupně	postupně	k6eAd1	postupně
gradovala	gradovat	k5eAaImAgFnS	gradovat
až	až	k9	až
počet	počet	k1gInSc1	počet
žádostí	žádost	k1gFnPc2	žádost
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
10.000	[number]	k4	10.000
Kubánců	Kubánec	k1gMnPc2	Kubánec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
obývali	obývat	k5eAaImAgMnP	obývat
veškeré	veškerý	k3xTgFnPc4	veškerý
volné	volný	k2eAgFnPc4d1	volná
prostory	prostora	k1gFnPc4	prostora
ambasády	ambasáda	k1gFnPc4	ambasáda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
nepodobné	podobný	k2eNgFnPc1d1	nepodobná
exodu	exodus	k1gInSc6	exodus
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
nucen	nutit	k5eAaImNgMnS	nutit
umožnit	umožnit	k5eAaPmF	umožnit
volný	volný	k2eAgInSc4d1	volný
odjezd	odjezd	k1gInSc4	odjezd
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Exodus	Exodus	k1gInSc1	Exodus
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1980	[number]	k4	1980
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
přístavu	přístav	k1gInSc2	přístav
Mariel	Mariela	k1gFnPc2	Mariela
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
125.000	[number]	k4	125.000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
událostí	událost	k1gFnSc7	událost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
politiky	politika	k1gFnSc2	politika
režimu	režim	k1gInSc2	režim
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Arnaldem	Arnald	k1gMnSc7	Arnald
Ochoou	Ochoa	k1gMnSc7	Ochoa
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
vysokými	vysoký	k2eAgMnPc7d1	vysoký
funkcionáři	funkcionář	k1gMnPc7	funkcionář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
obviněni	obvinit	k5eAaPmNgMnP	obvinit
z	z	k7c2	z
pašování	pašování	k1gNnSc2	pašování
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
Ochoa	Ochoa	k1gMnSc1	Ochoa
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
dalšími	další	k2eAgMnPc7d1	další
obviněnými	obviněný	k1gMnPc7	obviněný
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
soudu	soud	k1gInSc2	soud
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Ochoa	Ochoa	k1gMnSc1	Ochoa
byl	být	k5eAaImAgMnS	být
přitom	přitom	k6eAd1	přitom
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
členem	člen	k1gInSc7	člen
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
světových	světový	k2eAgMnPc2d1	světový
historiků	historik	k1gMnPc2	historik
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
zcela	zcela	k6eAd1	zcela
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
a	a	k8xC	a
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zajišťování	zajišťování	k1gNnSc2	zajišťování
financí	finance	k1gFnPc2	finance
pro	pro	k7c4	pro
státní	státní	k2eAgInSc4d1	státní
i	i	k8xC	i
vlastní	vlastní	k2eAgInSc4d1	vlastní
rozpočet	rozpočet	k1gInSc4	rozpočet
zapleten	zaplést	k5eAaPmNgMnS	zaplést
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
kubánský	kubánský	k2eAgMnSc1d1	kubánský
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
tohoto	tento	k3xDgMnSc2	tento
vysokého	vysoký	k2eAgMnSc2d1	vysoký
důstojníka	důstojník	k1gMnSc2	důstojník
měla	mít	k5eAaImAgFnS	mít
možná	možná	k6eAd1	možná
i	i	k9	i
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
ambice	ambice	k1gFnSc1	ambice
populárního	populární	k2eAgMnSc2d1	populární
vojenského	vojenský	k2eAgMnSc2d1	vojenský
vůdce	vůdce	k1gMnSc2	vůdce
a	a	k8xC	a
zastrašit	zastrašit	k5eAaPmF	zastrašit
opozici	opozice	k1gFnSc4	opozice
ve	v	k7c6	v
velení	velení	k1gNnSc6	velení
kubánské	kubánský	k2eAgFnSc2d1	kubánská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
ukončení	ukončení	k1gNnSc6	ukončení
sponzorování	sponzorování	k1gNnSc2	sponzorování
země	zem	k1gFnSc2	zem
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
RVHP	RVHP	kA	RVHP
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
propadla	propadnout	k5eAaPmAgFnS	propadnout
do	do	k7c2	do
hluboké	hluboký	k2eAgFnSc2d1	hluboká
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
nutnost	nutnost	k1gFnSc4	nutnost
striktních	striktní	k2eAgNnPc2d1	striktní
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
omezení	omezení	k1gNnPc2	omezení
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgInPc2d1	zvaný
"	"	kIx"	"
<g/>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
období	období	k1gNnSc4	období
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Período	Períoda	k1gFnSc5	Períoda
especial	especial	k1gInSc1	especial
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
ekonomika	ekonomika	k1gFnSc1	ekonomika
se	se	k3xPyFc4	se
po	po	k7c6	po
prudkém	prudký	k2eAgInSc6d1	prudký
propadu	propad	k1gInSc6	propad
byla	být	k5eAaImAgFnS	být
nucena	nucen	k2eAgFnSc1d1	nucena
přeorientovat	přeorientovat	k5eAaPmF	přeorientovat
na	na	k7c4	na
spolupráci	spolupráce	k1gFnSc4	spolupráce
(	(	kIx(	(
<g/>
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
<g/>
)	)	kIx)	)
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
chávezovské	chávezovský	k2eAgFnSc2d1	chávezovský
Venezuely	Venezuela	k1gFnSc2	Venezuela
a	a	k8xC	a
komunistické	komunistický	k2eAgFnSc2d1	komunistická
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
vážné	vážný	k2eAgFnPc4d1	vážná
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c4	o
zdraví	zdraví	k1gNnSc4	zdraví
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castrum	k1gNnSc2	Castrum
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
posléze	posléze	k6eAd1	posléze
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
pozváním	pozvání	k1gNnSc7	pozvání
španělského	španělský	k2eAgInSc2d1	španělský
lékařského	lékařský	k2eAgInSc2d1	lékařský
týmu	tým	k1gInSc2	tým
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
konstatovat	konstatovat	k5eAaBmF	konstatovat
chybnou	chybný	k2eAgFnSc4d1	chybná
léčbu	léčba	k1gFnSc4	léčba
po	po	k7c6	po
odoperování	odoperování	k1gNnSc6	odoperování
části	část	k1gFnSc2	část
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
kubánskými	kubánský	k2eAgFnPc7d1	kubánská
lékaři	lékař	k1gMnPc1	lékař
a	a	k8xC	a
vměšování	vměšování	k1gNnSc1	vměšování
pacienta	pacient	k1gMnSc2	pacient
do	do	k7c2	do
lékařského	lékařský	k2eAgNnSc2d1	lékařské
rozhodování	rozhodování	k1gNnSc2	rozhodování
<g/>
,	,	kIx,	,
Castra	Castrum	k1gNnSc2	Castrum
však	však	k9	však
po	po	k7c6	po
značném	značný	k2eAgNnSc6d1	značné
úsilí	úsilí	k1gNnSc6	úsilí
zachránili	zachránit	k5eAaPmAgMnP	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
oslabený	oslabený	k2eAgMnSc1d1	oslabený
a	a	k8xC	a
nemocný	mocný	k2eNgMnSc1d1	nemocný
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc1	Castro
nucen	nutit	k5eAaImNgMnS	nutit
abdikovat	abdikovat	k5eAaBmF	abdikovat
a	a	k8xC	a
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
pravomoci	pravomoc	k1gFnPc4	pravomoc
převzal	převzít	k5eAaPmAgMnS	převzít
muž	muž	k1gMnSc1	muž
číslo	číslo	k1gNnSc4	číslo
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
Fidelův	Fidelův	k2eAgMnSc1d1	Fidelův
bratr	bratr	k1gMnSc1	bratr
Raúl	Raúla	k1gFnPc2	Raúla
Castro	Castro	k1gNnSc4	Castro
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc1	Castro
si	se	k3xPyFc3	se
však	však	k9	však
ponechal	ponechat	k5eAaPmAgMnS	ponechat
funkci	funkce	k1gFnSc4	funkce
prvního	první	k4xOgMnSc2	první
tajemníka	tajemník	k1gMnSc2	tajemník
ÚV	ÚV	kA	ÚV
KSK	KSK	kA	KSK
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
publikovala	publikovat	k5eAaBmAgFnS	publikovat
kubánská	kubánský	k2eAgFnSc1d1	kubánská
vláda	vláda	k1gFnSc1	vláda
velký	velký	k2eAgInSc4d1	velký
návrh	návrh	k1gInSc4	návrh
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
"	"	kIx"	"
<g/>
úprav	úprava	k1gFnPc2	úprava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
i	i	k8xC	i
propouštění	propouštění	k1gNnSc1	propouštění
500.000	[number]	k4	500.000
státních	státní	k2eAgMnPc2d1	státní
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
do	do	k7c2	do
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
umožnit	umožnit	k5eAaPmF	umožnit
jejich	jejich	k3xOp3gInSc4	jejich
přechod	přechod	k1gInSc4	přechod
do	do	k7c2	do
soukromé	soukromý	k2eAgFnSc2d1	soukromá
sféry	sféra	k1gFnSc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
reforem	reforma	k1gFnPc2	reforma
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
schválen	schválit	k5eAaPmNgInS	schválit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
pětiletým	pětiletý	k2eAgInSc7d1	pětiletý
sjezdem	sjezd	k1gInSc7	sjezd
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
11	[number]	k4	11
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
propouštěním	propouštění	k1gNnSc7	propouštění
dalšího	další	k2eAgInSc2d1	další
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
sjezdu	sjezd	k1gInSc6	sjezd
se	se	k3xPyFc4	se
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
vzdal	vzdát	k5eAaPmAgMnS	vzdát
svých	svůj	k3xOyFgFnPc2	svůj
funkcí	funkce	k1gFnPc2	funkce
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
prvního	první	k4xOgMnSc2	první
tajemníka	tajemník	k1gMnSc2	tajemník
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Raúl	Raúl	k1gMnSc1	Raúl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tak	tak	k6eAd1	tak
opět	opět	k6eAd1	opět
drží	držet	k5eAaImIp3nS	držet
všechny	všechen	k3xTgFnPc4	všechen
hlavní	hlavní	k2eAgFnPc4d1	hlavní
funkce	funkce	k1gFnPc4	funkce
-	-	kIx~	-
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
premiéra	premiér	k1gMnSc2	premiér
i	i	k8xC	i
předsedy	předseda	k1gMnSc2	předseda
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
kubánské	kubánský	k2eAgFnSc2d1	kubánská
ústavy	ústava	k1gFnSc2	ústava
koncentrována	koncentrovat	k5eAaBmNgFnS	koncentrovat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
Raúla	Raúlo	k1gNnSc2	Raúlo
Castra	Castrum	k1gNnSc2	Castrum
jako	jako	k8xC	jako
prvního	první	k4xOgMnSc2	první
tajemníka	tajemník	k1gMnSc2	tajemník
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
historický	historický	k2eAgMnSc1d1	historický
vůdce	vůdce	k1gMnSc1	vůdce
revoluce	revoluce	k1gFnSc2	revoluce
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zakázány	zakázán	k2eAgInPc1d1	zakázán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
paralelních	paralelní	k2eAgFnPc2d1	paralelní
mocenských	mocenský	k2eAgFnPc2d1	mocenská
struktur	struktura	k1gFnPc2	struktura
<g/>
:	:	kIx,	:
Politbyro	politbyro	k1gNnSc1	politbyro
ÚV	ÚV	kA	ÚV
KSK	KSK	kA	KSK
-	-	kIx~	-
vedené	vedený	k2eAgFnSc2d1	vedená
jeho	jeho	k3xOp3gInSc3	jeho
prvním	první	k4xOgMnSc6	první
tajemníkem	tajemník	k1gMnSc7	tajemník
Raúlem	Raúl	k1gMnSc7	Raúl
Castrem	Castr	k1gMnSc7	Castr
<g/>
.	.	kIx.	.
</s>
<s>
KSK	KSK	kA	KSK
má	mít	k5eAaImIp3nS	mít
dle	dle	k7c2	dle
ústavy	ústava	k1gFnSc2	ústava
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
roli	role	k1gFnSc4	role
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
-	-	kIx~	-
vedená	vedený	k2eAgFnSc1d1	vedená
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
prezidentem	prezident	k1gMnSc7	prezident
Raúlem	Raúl	k1gMnSc7	Raúl
Castrem	Castr	k1gMnSc7	Castr
Rada	Rada	k1gMnSc1	Rada
ministrů	ministr	k1gMnPc2	ministr
(	(	kIx(	(
<g/>
vláda	vláda	k1gFnSc1	vláda
<g/>
)	)	kIx)	)
-	-	kIx~	-
vedená	vedený	k2eAgFnSc1d1	vedená
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
Raúlem	Raúl	k1gMnSc7	Raúl
Castrem	Castr	k1gMnSc7	Castr
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
organizace	organizace	k1gFnPc4	organizace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
patří	patřit	k5eAaImIp3nS	patřit
kromě	kromě	k7c2	kromě
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
i	i	k9	i
uliční	uliční	k2eAgInPc1d1	uliční
tak	tak	k8xC	tak
zvané	zvaný	k2eAgInPc1d1	zvaný
Výbory	výbor	k1gInPc1	výbor
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
CDR	CDR	kA	CDR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
původně	původně	k6eAd1	původně
fungovaly	fungovat	k5eAaImAgFnP	fungovat
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
udávání	udávání	k1gNnSc1	udávání
všeho	všecek	k3xTgInSc2	všecek
protirevolučního	protirevoluční	k2eAgInSc2d1	protirevoluční
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dámy	dáma	k1gFnSc2	dáma
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
politická	politický	k2eAgFnSc1d1	politická
opozice	opozice	k1gFnSc1	opozice
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
a	a	k8xC	a
persekuována	persekuován	k2eAgFnSc1d1	persekuován
<g/>
,	,	kIx,	,
osobnosti	osobnost	k1gFnPc1	osobnost
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
názorem	názor	k1gInSc7	názor
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgMnSc1d1	oficiální
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
s	s	k7c7	s
pronásledováním	pronásledování	k1gNnSc7	pronásledování
<g/>
,	,	kIx,	,
vězněním	věznění	k1gNnSc7	věznění
i	i	k8xC	i
častým	častý	k2eAgNnSc7d1	časté
fyzickým	fyzický	k2eAgNnSc7d1	fyzické
napadáním	napadání	k1gNnSc7	napadání
organizovaným	organizovaný	k2eAgNnSc7d1	organizované
složkami	složka	k1gFnPc7	složka
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
opozice	opozice	k1gFnSc2	opozice
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zcela	zcela	k6eAd1	zcela
ilegální	ilegální	k2eAgMnSc1d1	ilegální
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
představitelé	představitel	k1gMnPc1	představitel
jsou	být	k5eAaImIp3nP	být
vězněni	vězněn	k2eAgMnPc1d1	vězněn
v	v	k7c6	v
přeplněných	přeplněný	k2eAgFnPc6d1	přeplněná
věznicích	věznice	k1gFnPc6	věznice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mezi	mezi	k7c7	mezi
nejznámější	známý	k2eAgFnSc1d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
havanská	havanský	k2eAgFnSc1d1	Havanská
Villa	Villa	k1gFnSc1	Villa
Marista	Marist	k1gMnSc2	Marist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
však	však	k9	však
režim	režim	k1gInSc1	režim
změnil	změnit	k5eAaPmAgInS	změnit
taktiku	taktika	k1gFnSc4	taktika
a	a	k8xC	a
namísto	namísto	k7c2	namísto
dlouholetého	dlouholetý	k2eAgNnSc2d1	dlouholeté
věznění	věznění	k1gNnSc2	věznění
probíhají	probíhat	k5eAaImIp3nP	probíhat
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
stovky	stovka	k1gFnSc2	stovka
až	až	k8xS	až
tisíce	tisíc	k4xCgInPc4	tisíc
krátkodobých	krátkodobý	k2eAgNnPc2d1	krátkodobé
zadržení	zadržení	k1gNnPc2	zadržení
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zastrašit	zastrašit	k5eAaPmF	zastrašit
a	a	k8xC	a
odradit	odradit	k5eAaPmF	odradit
opoziční	opoziční	k2eAgInPc4d1	opoziční
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
na	na	k7c6	na
podpoře	podpora	k1gFnSc6	podpora
kubánské	kubánský	k2eAgFnSc2d1	kubánská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
opozice	opozice	k1gFnSc2	opozice
podílí	podílet	k5eAaImIp3nS	podílet
humanitární	humanitární	k2eAgFnSc2d1	humanitární
organizace	organizace	k1gFnSc2	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
společnosti	společnost	k1gFnSc2	společnost
distribuují	distribuovat	k5eAaBmIp3nP	distribuovat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
techniku	technika	k1gFnSc4	technika
a	a	k8xC	a
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
nemocným	nemocný	k1gMnPc3	nemocný
<g/>
,	,	kIx,	,
rodinám	rodina	k1gFnPc3	rodina
uvězněných	uvězněný	k2eAgMnPc2d1	uvězněný
a	a	k8xC	a
odpůrcům	odpůrce	k1gMnPc3	odpůrce
totalitního	totalitní	k2eAgInSc2d1	totalitní
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
organizují	organizovat	k5eAaBmIp3nP	organizovat
kampaně	kampaň	k1gFnPc4	kampaň
podporující	podporující	k2eAgFnSc4d1	podporující
kubánskou	kubánský	k2eAgFnSc4d1	kubánská
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
opozici	opozice	k1gFnSc4	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
osoby	osoba	k1gFnPc4	osoba
demokratické	demokratický	k2eAgFnSc2d1	demokratická
opozice	opozice	k1gFnSc2	opozice
náleží	náležet	k5eAaImIp3nS	náležet
sociálnědemokratický	sociálnědemokratický	k2eAgMnSc1d1	sociálnědemokratický
politik	politik	k1gMnSc1	politik
Elizardo	Elizardo	k1gNnSc1	Elizardo
Sánchez	Sánchez	k1gMnSc1	Sánchez
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vede	vést	k5eAaImIp3nS	vést
kubánský	kubánský	k2eAgInSc4d1	kubánský
seznam	seznam	k1gInSc4	seznam
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
vězně	vězeň	k1gMnPc4	vězeň
svědomí	svědomí	k1gNnPc2	svědomí
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Óscar	Óscar	k1gMnSc1	Óscar
Elías	Elías	k1gMnSc1	Elías
Biscet	Biscet	k1gMnSc1	Biscet
<g/>
,	,	kIx,	,
propuštěný	propuštěný	k2eAgMnSc1d1	propuštěný
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
a	a	k8xC	a
řadil	řadit	k5eAaImAgMnS	řadit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
i	i	k9	i
Orlando	Orlanda	k1gFnSc5	Orlanda
Zapata	Zapat	k1gMnSc4	Zapat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hladovky	hladovka	k1gFnSc2	hladovka
zemřel	zemřít	k5eAaPmAgInS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
disidentem	disident	k1gMnSc7	disident
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
proti	proti	k7c3	proti
jednání	jednání	k1gNnSc3	jednání
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
protestuje	protestovat	k5eAaBmIp3nS	protestovat
sérií	série	k1gFnSc7	série
hladovek	hladovka	k1gFnPc2	hladovka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Guillermo	Guillerma	k1gFnSc5	Guillerma
Fariñ	Fariñ	k1gFnPc2	Fariñ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2012	[number]	k4	2012
zahynul	zahynout	k5eAaPmAgInS	zahynout
za	za	k7c2	za
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
objasněných	objasněný	k2eAgFnPc2d1	objasněná
okolností	okolnost	k1gFnPc2	okolnost
křesťanskodemokratický	křesťanskodemokratický	k2eAgMnSc1d1	křesťanskodemokratický
politik	politik	k1gMnSc1	politik
Oswaldo	Oswaldo	k1gNnSc1	Oswaldo
Payá	Payá	k1gFnSc1	Payá
Sardiñ	Sardiñ	k1gFnSc1	Sardiñ
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Projektu	projekt	k1gInSc2	projekt
Varela	Varel	k1gMnSc2	Varel
<g/>
,	,	kIx,	,
obdoby	obdoba	k1gFnSc2	obdoba
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
zatčen	zatčen	k2eAgMnSc1d1	zatčen
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
novinář	novinář	k1gMnSc1	novinář
Calixto	Calixto	k1gNnSc1	Calixto
Martínez	Martínez	k1gMnSc1	Martínez
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
za	za	k7c4	za
neúctu	neúcta	k1gFnSc4	neúcta
k	k	k7c3	k
minulé	minulý	k2eAgFnSc3d1	minulá
i	i	k8xC	i
současné	současný	k2eAgFnSc3d1	současná
hlavě	hlava	k1gFnSc3	hlava
státu	stát	k1gInSc2	stát
hrozí	hrozit	k5eAaImIp3nS	hrozit
až	až	k9	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
typické	typický	k2eAgInPc4d1	typický
způsoby	způsob	k1gInPc4	způsob
násilí	násilí	k1gNnSc2	násilí
proti	proti	k7c3	proti
nenásilné	násilný	k2eNgFnSc3d1	nenásilná
opozici	opozice	k1gFnSc3	opozice
patří	patřit	k5eAaImIp3nS	patřit
tzv.	tzv.	kA	tzv.
Akty	akt	k1gInPc1	akt
zastrašení	zastrašení	k1gNnSc1	zastrašení
(	(	kIx(	(
<g/>
Actos	Actos	k1gMnSc1	Actos
de	de	k?	de
repudio	repudio	k1gMnSc1	repudio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
dav	dav	k1gInSc1	dav
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
kubánské	kubánský	k2eAgFnSc2d1	kubánská
StB	StB	k1gFnSc2	StB
shlukne	shluknout	k5eAaPmIp3nS	shluknout
před	před	k7c7	před
bydlištěm	bydliště	k1gNnSc7	bydliště
disidenta	disident	k1gMnSc2	disident
<g/>
,	,	kIx,	,
útočí	útočit	k5eAaImIp3nS	útočit
a	a	k8xC	a
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
jeho	jeho	k3xOp3gInSc4	jeho
dům	dům	k1gInSc4	dům
a	a	k8xC	a
případně	případně	k6eAd1	případně
disidenta	disident	k1gMnSc4	disident
i	i	k8xC	i
fyzicky	fyzicky	k6eAd1	fyzicky
napadá	napadat	k5eAaImIp3nS	napadat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zlepšování	zlepšování	k1gNnSc3	zlepšování
vztahů	vztah	k1gInPc2	vztah
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
krátké	krátký	k2eAgNnSc1d1	krátké
setkání	setkání	k1gNnSc1	setkání
a	a	k8xC	a
podání	podání	k1gNnSc1	podání
ruky	ruka	k1gFnSc2	ruka
Raúla	Raúlo	k1gNnSc2	Raúlo
Castra	Castrum	k1gNnSc2	Castrum
a	a	k8xC	a
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
,	,	kIx,	,
na	na	k7c6	na
celoamerickém	celoamerický	k2eAgInSc6d1	celoamerický
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Panamě	Panama	k1gFnSc6	Panama
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Kuby	Kuba	k1gFnSc2	Kuba
dostaly	dostat	k5eAaPmAgFnP	dostat
na	na	k7c4	na
kandidátku	kandidátka	k1gFnSc4	kandidátka
obecních	obecní	k2eAgFnPc2d1	obecní
voleb	volba	k1gFnPc2	volba
opoziční	opoziční	k2eAgMnPc1d1	opoziční
disidenti	disident	k1gMnPc1	disident
<g/>
,	,	kIx,	,
Hildebrando	Hildebranda	k1gFnSc5	Hildebranda
Chaviano	Chaviana	k1gFnSc5	Chaviana
a	a	k8xC	a
Yuniel	Yuniel	k1gMnSc1	Yuniel
López	López	k1gMnSc1	López
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zastupitelské	zastupitelský	k2eAgFnPc4d1	zastupitelská
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Havaně	Havana	k1gFnSc6	Havana
<g/>
.	.	kIx.	.
</s>
<s>
Všepronikající	Všepronikající	k2eAgFnSc1d1	Všepronikající
státní	státní	k2eAgFnSc1d1	státní
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
,	,	kIx,	,
požadavek	požadavek	k1gInSc1	požadavek
servilního	servilní	k2eAgNnSc2d1	servilní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
cenzura	cenzura	k1gFnSc1	cenzura
či	či	k8xC	či
zákaz	zákaz	k1gInSc1	zákaz
cest	cesta	k1gFnPc2	cesta
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
pro	pro	k7c4	pro
neservilní	servilní	k2eNgMnPc4d1	servilní
autory	autor	k1gMnPc4	autor
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
veškerou	veškerý	k3xTgFnSc4	veškerý
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
tvorbu	tvorba	k1gFnSc4	tvorba
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
kubánských	kubánský	k2eAgMnPc2d1	kubánský
umělců	umělec	k1gMnPc2	umělec
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
proslavila	proslavit	k5eAaPmAgFnS	proslavit
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
celá	celý	k2eAgFnSc1d1	celá
generace	generace	k1gFnSc1	generace
slavných	slavný	k2eAgMnPc2d1	slavný
kubánských	kubánský	k2eAgMnPc2d1	kubánský
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
co	co	k9	co
zůstali	zůstat	k5eAaPmAgMnP	zůstat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
měli	mít	k5eAaImAgMnP	mít
často	často	k6eAd1	často
vážné	vážný	k2eAgInPc4d1	vážný
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
diktaturou	diktatura	k1gFnSc7	diktatura
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
,	,	kIx,	,
když	když	k8xS	když
důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
zatčení	zatčení	k1gNnSc3	zatčení
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
pouhá	pouhý	k2eAgFnSc1d1	pouhá
kritika	kritika	k1gFnSc1	kritika
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
jen	jen	k9	jen
o	o	k7c4	o
ojedinělé	ojedinělý	k2eAgInPc4d1	ojedinělý
případy	případ	k1gInPc4	případ
-	-	kIx~	-
některá	některý	k3yIgFnSc1	některý
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
slavná	slavný	k2eAgNnPc4d1	slavné
umělecká	umělecký	k2eAgNnPc4d1	umělecké
odvětví	odvětví	k1gNnPc4	odvětví
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
zcela	zcela	k6eAd1	zcela
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
)	)	kIx)	)
zlikvidována	zlikvidován	k2eAgFnSc1d1	zlikvidována
<g/>
.	.	kIx.	.
</s>
<s>
Zmizela	zmizet	k5eAaPmAgFnS	zmizet
tak	tak	k9	tak
například	například	k6eAd1	například
dokumentární	dokumentární	k2eAgFnSc1d1	dokumentární
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
otevřeným	otevřený	k2eAgInSc7d1	otevřený
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
realitu	realita	k1gFnSc4	realita
nemohla	moct	k5eNaImAgFnS	moct
nové	nový	k2eAgInPc4d1	nový
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
ještě	ještě	k9	ještě
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
revoluce	revoluce	k1gFnSc1	revoluce
dala	dát	k5eAaPmAgFnS	dát
země	zem	k1gFnPc4	zem
světu	svět	k1gInSc3	svět
slavná	slavný	k2eAgNnPc4d1	slavné
jména	jméno	k1gNnPc4	jméno
jako	jako	k8xS	jako
Korda	Korda	k1gMnSc1	Korda
či	či	k8xC	či
Corrales	Corrales	k1gMnSc1	Corrales
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
významnější	významný	k2eAgMnPc1d2	významnější
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Kuby	Kuba	k1gFnSc2	Kuba
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gFnSc1	jejich
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
často	často	k6eAd1	často
týkaly	týkat	k5eAaImAgInP	týkat
zrušení	zrušení	k1gNnSc4	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
Gertrudis	Gertrudis	k1gInSc1	Gertrudis
Gomez	Gomez	k1gMnSc1	Gomez
de	de	k?	de
Avellaneda	Avellaneda	k1gMnSc1	Avellaneda
a	a	k8xC	a
Cirilo	Cirila	k1gFnSc5	Cirila
Villaverde	Villaverd	k1gMnSc5	Villaverd
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
blížící	blížící	k2eAgFnSc7d1	blížící
se	se	k3xPyFc4	se
nezávislostí	nezávislost	k1gFnSc7	nezávislost
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
tématům	téma	k1gNnPc3	téma
svobody	svoboda	k1gFnSc2	svoboda
začali	začít	k5eAaPmAgMnP	začít
věnovat	věnovat	k5eAaPmF	věnovat
osobnosti	osobnost	k1gFnPc4	osobnost
jako	jako	k8xC	jako
José	Josý	k2eAgNnSc4d1	José
Martí	Martí	k1gNnSc4	Martí
či	či	k8xC	či
Nicolás	Nicolás	k1gInSc4	Nicolás
Guillén	Guillén	k1gInSc1	Guillén
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známá	známý	k2eAgNnPc4d1	známé
jména	jméno	k1gNnPc4	jméno
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaných	uznávaný	k2eAgMnPc2d1	uznávaný
autorů	autor	k1gMnPc2	autor
tvořících	tvořící	k2eAgInPc2d1	tvořící
i	i	k8xC	i
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
patří	patřit	k5eAaImIp3nS	patřit
Dulce	Dulce	k1gFnPc4	Dulce
María	Marí	k2eAgFnSc1d1	María
Loynaz	Loynaz	k1gInSc1	Loynaz
<g/>
,	,	kIx,	,
José	Josý	k2eAgNnSc1d1	José
Lezama	Lezama	k1gNnSc1	Lezama
Lima	Lima	k1gFnSc1	Lima
<g/>
,	,	kIx,	,
Alejo	Alejo	k6eAd1	Alejo
Carpentier	Carpentier	k1gInSc1	Carpentier
<g/>
,	,	kIx,	,
Reinaldo	Reinaldo	k1gNnSc1	Reinaldo
Arenas	Arenasa	k1gFnPc2	Arenasa
či	či	k8xC	či
Guillermo	Guillerma	k1gFnSc5	Guillerma
Cabrera	Cabrer	k1gMnSc2	Cabrer
Infante	infant	k1gMnSc5	infant
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
kubánské	kubánský	k2eAgMnPc4d1	kubánský
spisovatele	spisovatel	k1gMnPc4	spisovatel
současnosti	současnost	k1gFnSc2	současnost
patří	patřit	k5eAaImIp3nS	patřit
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
emigrovat	emigrovat	k5eAaBmF	emigrovat
<g/>
:	:	kIx,	:
Daína	Daína	k1gFnSc1	Daína
Chaviano	Chaviana	k1gFnSc5	Chaviana
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zoé	Zoé	k1gMnSc1	Zoé
Valdés	Valdés	k1gInSc1	Valdés
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eliseo	Eliseo	k6eAd1	Eliseo
Alberto	Alberta	k1gFnSc5	Alberta
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pedro	Pedro	k1gNnSc1	Pedro
Juan	Juan	k1gMnSc1	Juan
Gutiérrez	Gutiérrez	k1gMnSc1	Gutiérrez
(	(	kIx(	(
<g/>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
Padura	Padura	k1gFnSc1	Padura
(	(	kIx(	(
<g/>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
a	a	k8xC	a
Abilio	Abilio	k1gMnSc1	Abilio
Estévez	Estévez	k1gMnSc1	Estévez
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slavnou	slavný	k2eAgFnSc7d1	slavná
mladou	mladý	k2eAgFnSc7d1	mladá
autorkou	autorka	k1gFnSc7	autorka
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
stále	stále	k6eAd1	stále
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
žijící	žijící	k2eAgFnSc1d1	žijící
blogerka	blogerka	k1gFnSc1	blogerka
Yoani	Yoaň	k1gMnSc3	Yoaň
Sánchez	Sáncheza	k1gFnPc2	Sáncheza
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
na	na	k7c4	na
blogu	bloga	k1gFnSc4	bloga
Generacion	Generacion	k1gInSc1	Generacion
Y	Y	kA	Y
<g/>
,	,	kIx,	,
udržovaným	udržovaný	k2eAgNnSc7d1	udržované
v	v	k7c4	v
zahraničí	zahraničí	k1gNnSc4	zahraničí
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
stránka	stránka	k1gFnSc1	stránka
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
zablokována	zablokovat	k5eAaPmNgFnS	zablokovat
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Time	Time	k1gInSc1	Time
ji	on	k3xPp3gFnSc4	on
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
100	[number]	k4	100
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
osobností	osobnost	k1gFnPc2	osobnost
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
získala	získat	k5eAaPmAgFnS	získat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
literární	literární	k2eAgFnSc4d1	literární
cenu	cena	k1gFnSc4	cena
Ortega	Ortega	k1gFnSc1	Ortega
y	y	k?	y
Gasset	Gasseta	k1gFnPc2	Gasseta
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
i	i	k9	i
další	další	k2eAgFnPc4d1	další
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
převzetí	převzetí	k1gNnSc6	převzetí
ovšem	ovšem	k9	ovšem
neobdržela	obdržet	k5eNaPmAgFnS	obdržet
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
vycestování	vycestování	k1gNnSc3	vycestování
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
kubánské	kubánský	k2eAgMnPc4d1	kubánský
fotografy	fotograf	k1gMnPc4	fotograf
patří	patřit	k5eAaImIp3nS	patřit
dokumentární	dokumentární	k2eAgMnPc1d1	dokumentární
fotografové	fotograf	k1gMnPc1	fotograf
z	z	k7c2	z
období	období	k1gNnSc2	období
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Raúl	Raúl	k1gMnSc1	Raúl
Corrales	Corrales	k1gMnSc1	Corrales
<g/>
,	,	kIx,	,
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
však	však	k8xC	však
Alberto	Alberta	k1gFnSc5	Alberta
Korda	Korda	k1gMnSc1	Korda
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
především	především	k9	především
slavnou	slavný	k2eAgFnSc7d1	slavná
fotografií	fotografia	k1gFnSc7	fotografia
Ernesta	Ernest	k1gMnSc2	Ernest
"	"	kIx"	"
<g/>
Che	che	k0	che
<g/>
"	"	kIx"	"
Guevary	Guevar	k1gInPc4	Guevar
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
fotografiím	fotografia	k1gFnPc3	fotografia
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dokumentární	dokumentární	k2eAgFnSc1d1	dokumentární
fotografie	fotografie	k1gFnSc1	fotografie
již	již	k6eAd1	již
na	na	k7c6	na
současné	současný	k2eAgFnSc6d1	současná
Kubě	Kuba	k1gFnSc6	Kuba
nehraje	hrát	k5eNaImIp3nS	hrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
i	i	k9	i
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
citlivost	citlivost	k1gFnSc4	citlivost
dokumentování	dokumentování	k1gNnSc2	dokumentování
chmurné	chmurný	k2eAgFnSc2d1	chmurná
ostrovní	ostrovní	k2eAgFnSc2d1	ostrovní
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
stylů	styl	k1gInPc2	styl
vycházejících	vycházející	k2eAgInPc2d1	vycházející
z	z	k7c2	z
evropské	evropský	k2eAgFnSc2d1	Evropská
a	a	k8xC	a
africké	africký	k2eAgFnSc2d1	africká
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
tradiční	tradiční	k2eAgInPc4d1	tradiční
styly	styl	k1gInPc4	styl
kubánské	kubánský	k2eAgFnSc2d1	kubánská
hudby	hudba	k1gFnSc2	hudba
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
trova	trova	k1gFnSc1	trova
<g/>
,	,	kIx,	,
habanera	habanera	k1gFnSc1	habanera
<g/>
,	,	kIx,	,
danzón	danzón	k1gInSc1	danzón
<g/>
,	,	kIx,	,
son	son	k1gInSc1	son
<g/>
,	,	kIx,	,
kubánský	kubánský	k2eAgInSc1d1	kubánský
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
rumba	rumba	k1gFnSc1	rumba
<g/>
,	,	kIx,	,
timba	timba	k1gFnSc1	timba
(	(	kIx(	(
<g/>
kubánská	kubánský	k2eAgFnSc1d1	kubánská
salsa	salsa	k1gFnSc1	salsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
vyťukávaný	vyťukávaný	k2eAgInSc1d1	vyťukávaný
rytmus	rytmus	k1gInSc1	rytmus
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
clave	clavat	k5eAaPmIp3nS	clavat
(	(	kIx(	(
<g/>
klíč	klíč	k1gInSc4	klíč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
kubánské	kubánský	k2eAgMnPc4d1	kubánský
zpěváky	zpěvák	k1gMnPc4	zpěvák
a	a	k8xC	a
hudebníky	hudebník	k1gMnPc4	hudebník
patří	patřit	k5eAaImIp3nP	patřit
Kubánci	Kubánec	k1gMnPc1	Kubánec
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
90	[number]	k4	90
miliony	milion	k4xCgInPc7	milion
prodanými	prodaný	k2eAgInPc7d1	prodaný
alby	alba	k1gFnPc4	alba
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
latinskoamerickou	latinskoamerický	k2eAgFnSc7d1	latinskoamerická
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
například	například	k6eAd1	například
Gloria	Gloria	k1gFnSc1	Gloria
Estefan	Estefana	k1gFnPc2	Estefana
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
slavná	slavný	k2eAgNnPc4d1	slavné
jména	jméno	k1gNnPc4	jméno
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
salsy	salsa	k1gFnSc2	salsa
Celia	Celia	k1gFnSc1	Celia
Cruz	Cruz	k1gMnSc1	Cruz
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
salsy	salsa	k1gFnSc2	salsa
a	a	k8xC	a
merengue	merengue	k1gNnSc4	merengue
Willy	Willa	k1gFnSc2	Willa
Chirino	Chirin	k2eAgNnSc4d1	Chirin
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
kubánské	kubánský	k2eAgMnPc4d1	kubánský
oficiální	oficiální	k2eAgMnPc4d1	oficiální
zpěváky	zpěvák	k1gMnPc4	zpěvák
starší	starší	k1gMnPc4	starší
generace	generace	k1gFnSc2	generace
nadále	nadále	k6eAd1	nadále
žijící	žijící	k2eAgMnSc1d1	žijící
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
patří	patřit	k5eAaImIp3nS	patřit
Pablo	Pablo	k1gNnSc1	Pablo
Milanés	Milanésa	k1gFnPc2	Milanésa
<g/>
,	,	kIx,	,
Silvio	Silvio	k1gMnSc1	Silvio
Rodríguez	Rodríguez	k1gMnSc1	Rodríguez
<g/>
,	,	kIx,	,
Omara	Omara	k1gMnSc1	Omara
Portuondo	Portuondo	k6eAd1	Portuondo
či	či	k8xC	či
známá	známý	k2eAgFnSc1d1	známá
kapela	kapela	k1gFnSc1	kapela
Buena	Buen	k1gInSc2	Buen
Vista	vista	k2eAgInSc1d1	vista
Social	Social	k1gInSc1	Social
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgInPc4	který
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
i	i	k9	i
film	film	k1gInSc1	film
Wima	Wim	k2eAgFnSc1d1	Wima
Wenderse	Wenderse	k1gFnSc1	Wenderse
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
diktaturou	diktatura	k1gFnSc7	diktatura
a	a	k8xC	a
související	související	k2eAgFnSc7d1	související
cenzurou	cenzura	k1gFnSc7	cenzura
mívají	mívat	k5eAaImIp3nP	mívat
i	i	k9	i
etablované	etablovaný	k2eAgFnPc1d1	etablovaná
a	a	k8xC	a
státem	stát	k1gInSc7	stát
podporované	podporovaný	k2eAgFnSc2d1	podporovaná
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
popová	popový	k2eAgFnSc1d1	popová
skupina	skupina	k1gFnSc1	skupina
Moneda	Moned	k1gMnSc2	Moned
Dura	Durus	k1gMnSc2	Durus
tak	tak	k9	tak
například	například	k6eAd1	například
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
svého	svůj	k3xOyFgNnSc2	svůj
alba	album	k1gNnSc2	album
Alma	alma	k1gFnSc1	alma
sin	sin	kA	sin
Bolsillos	Bolsillosa	k1gFnPc2	Bolsillosa
byla	být	k5eAaImAgNnP	být
svědkem	svědek	k1gMnSc7	svědek
zákazu	zákaz	k1gInSc3	zákaz
vysílání	vysílání	k1gNnSc1	vysílání
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Mala	Mal	k2eAgFnSc1d1	Mala
leche	leche	k1gFnSc1	leche
<g/>
"	"	kIx"	"
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
všech	všecek	k3xTgInPc2	všecek
<g/>
)	)	kIx)	)
médiích	médium	k1gNnPc6	médium
jakožto	jakožto	k8xS	jakožto
nepřípustné	přípustný	k2eNgFnSc2d1	nepřípustná
kritiky	kritika	k1gFnSc2	kritika
situace	situace	k1gFnSc2	situace
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
diktátora	diktátor	k1gMnSc4	diktátor
Castra	Castr	k1gMnSc4	Castr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
rockovým	rockový	k2eAgFnPc3d1	rocková
kapelám	kapela	k1gFnPc3	kapela
současnosti	současnost	k1gFnSc2	současnost
patří	patřit	k5eAaImIp3nS	patřit
punkrocková	punkrockový	k2eAgFnSc1d1	punkrocková
skupina	skupina	k1gFnSc1	skupina
Porno	porno	k1gNnSc4	porno
para	para	k1gInSc2	para
Ricardo	Ricardo	k1gNnSc4	Ricardo
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
hlavní	hlavní	k2eAgMnSc1d1	hlavní
představitel	představitel	k1gMnSc1	představitel
Gorki	Gorki	k1gFnPc1	Gorki
Águila	Águil	k1gMnSc2	Águil
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
vězněn	vězněn	k2eAgInSc1d1	vězněn
a	a	k8xC	a
nedávno	nedávno	k6eAd1	nedávno
opět	opět	k6eAd1	opět
zatčen	zatknout	k5eAaPmNgMnS	zatknout
za	za	k7c4	za
"	"	kIx"	"
<g/>
příliš	příliš	k6eAd1	příliš
hlasitou	hlasitý	k2eAgFnSc4d1	hlasitá
hudbu	hudba	k1gFnSc4	hudba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
posledním	poslední	k2eAgInSc6d1	poslední
soudu	soud	k1gInSc6	soud
byl	být	k5eAaImAgInS	být
souzen	soudit	k5eAaImNgInS	soudit
podle	podle	k7c2	podle
nového	nový	k2eAgInSc2d1	nový
politického	politický	k2eAgInSc2d1	politický
zákona	zákon	k1gInSc2	zákon
za	za	k7c4	za
"	"	kIx"	"
<g/>
nebezpečnost	nebezpečnost	k1gFnSc4	nebezpečnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
silném	silný	k2eAgInSc6d1	silný
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
tlaku	tlak	k1gInSc6	tlak
byl	být	k5eAaImAgMnS	být
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
a	a	k8xC	a
potrestán	potrestat	k5eAaPmNgMnS	potrestat
pouze	pouze	k6eAd1	pouze
pokutou	pokuta	k1gFnSc7	pokuta
za	za	k7c4	za
veřejné	veřejný	k2eAgNnSc4d1	veřejné
pohoršení	pohoršení	k1gNnSc4	pohoršení
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejznámějším	známý	k2eAgInSc7d3	nejznámější
kubánským	kubánský	k2eAgInSc7d1	kubánský
filmem	film	k1gInSc7	film
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Jahody	jahoda	k1gFnPc1	jahoda
a	a	k8xC	a
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
"	"	kIx"	"
režiséra	režisér	k1gMnSc2	režisér
Tomáse	Tomáse	k1gFnSc1	Tomáse
Gutiérreze	Gutiérreze	k1gFnSc1	Gutiérreze
Aley	Alea	k1gFnSc2	Alea
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
pozastaveno	pozastavit	k5eAaPmNgNnS	pozastavit
členství	členství	k1gNnSc1	členství
v	v	k7c6	v
OAS	oasa	k1gFnPc2	oasa
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
členem	člen	k1gMnSc7	člen
RVHP	RVHP	kA	RVHP
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
až	až	k9	až
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Současnými	současný	k2eAgMnPc7d1	současný
hlavními	hlavní	k2eAgMnPc7d1	hlavní
kubánskými	kubánský	k2eAgMnPc7d1	kubánský
spojenci	spojenec	k1gMnPc7	spojenec
jsou	být	k5eAaImIp3nP	být
latinskoamerické	latinskoamerický	k2eAgInPc1d1	latinskoamerický
státy	stát	k1gInPc1	stát
s	s	k7c7	s
levicovými	levicový	k2eAgFnPc7d1	levicová
vládami	vláda	k1gFnPc7	vláda
<g/>
:	:	kIx,	:
především	především	k6eAd1	především
Venezuela	Venezuela	k1gFnSc1	Venezuela
a	a	k8xC	a
Bolívie	Bolívie	k1gFnPc1	Bolívie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
Bolívarovský	Bolívarovský	k2eAgInSc1d1	Bolívarovský
svaz	svaz	k1gInSc1	svaz
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
naší	náš	k3xOp1gFnSc2	náš
Ameriky	Amerika	k1gFnSc2	Amerika
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
zakládající	zakládající	k2eAgFnSc4d1	zakládající
smlouvu	smlouva	k1gFnSc4	smlouva
podpsali	podpsat	k5eAaImAgMnP	podpsat
Hugo	Hugo	k1gMnSc1	Hugo
Chávez	Chávez	k1gMnSc1	Chávez
a	a	k8xC	a
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc1	Castro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
partenerem	partener	k1gMnSc7	partener
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Holandsko	Holandsko	k1gNnSc1	Holandsko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česko-kubánské	českoubánský	k2eAgInPc4d1	česko-kubánský
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
navázání	navázání	k1gNnSc2	navázání
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
styků	styk	k1gInPc2	styk
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
23	[number]	k4	23
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1920	[number]	k4	1920
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
Československo	Československo	k1gNnSc1	Československo
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
ČSR	ČSR	kA	ČSR
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
stálý	stálý	k2eAgMnSc1d1	stálý
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
Kubu	Kuba	k1gFnSc4	Kuba
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
až	až	k9	až
po	po	k7c4	po
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
Fidelem	Fidel	k1gMnSc7	Fidel
Castrem	Castr	k1gMnSc7	Castr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
tak	tak	k6eAd1	tak
podporovalo	podporovat	k5eAaImAgNnS	podporovat
Castrovu	Castrův	k2eAgFnSc4d1	Castrova
Kubu	Kuba	k1gFnSc4	Kuba
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
dodávkami	dodávka	k1gFnPc7	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
půjčkami	půjčka	k1gFnPc7	půjčka
<g/>
,	,	kIx,	,
odpouštěním	odpouštění	k1gNnSc7	odpouštění
plateb	platba	k1gFnPc2	platba
atd.	atd.	kA	atd.
Československo	Československo	k1gNnSc1	Československo
současně	současně	k6eAd1	současně
zastupovalo	zastupovat	k5eAaImAgNnS	zastupovat
Kubu	Kuba	k1gFnSc4	Kuba
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
Havana	havana	k1gNnSc1	havana
neměla	mít	k5eNaImAgFnS	mít
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
ČSR	ČSR	kA	ČSR
zkomplikoval	zkomplikovat	k5eAaPmAgInS	zkomplikovat
až	až	k9	až
rok	rok	k1gInSc4	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
poté	poté	k6eAd1	poté
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
z	z	k7c2	z
23.8	[number]	k4	23.8
<g/>
.1968	.1968	k4	.1968
rozporuplně	rozporuplně	k6eAd1	rozporuplně
a	a	k8xC	a
emotivně	emotivně	k6eAd1	emotivně
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
na	na	k7c4	na
okupaci	okupace	k1gFnSc4	okupace
Československa	Československo	k1gNnSc2	Československo
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
současně	současně	k6eAd1	současně
silně	silně	k6eAd1	silně
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
kvalitu	kvalita	k1gFnSc4	kvalita
československých	československý	k2eAgFnPc2d1	Československá
dodávek	dodávka	k1gFnPc2	dodávka
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
vztahy	vztah	k1gInPc1	vztah
začaly	začít	k5eAaPmAgInP	začít
vracet	vracet	k5eAaImF	vracet
do	do	k7c2	do
starých	starý	k2eAgFnPc2d1	stará
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1969	[number]	k4	1969
již	již	k6eAd1	již
Castro	Castro	k1gNnSc4	Castro
československé	československý	k2eAgFnSc2d1	Československá
dodávky	dodávka	k1gFnSc2	dodávka
opětovně	opětovně	k6eAd1	opětovně
pochválil	pochválit	k5eAaPmAgMnS	pochválit
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
patřilo	patřit	k5eAaImAgNnS	patřit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
socialismu	socialismus	k1gInSc2	socialismus
k	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
dodavatelům	dodavatel	k1gMnPc3	dodavatel
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
současně	současně	k6eAd1	současně
odebíralo	odebírat	k5eAaImAgNnS	odebírat
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
nikl	nikl	k1gInSc4	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
výměna	výměna	k1gFnSc1	výměna
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
příliš	příliš	k6eAd1	příliš
veliká	veliký	k2eAgFnSc1d1	veliká
<g/>
,	,	kIx,	,
v	v	k7c6	v
porevoluční	porevoluční	k2eAgFnSc6d1	porevoluční
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
českým	český	k2eAgInPc3d1	český
podnikům	podnik	k1gInPc3	podnik
mezi	mezi	k7c4	mezi
větší	veliký	k2eAgInPc4d2	veliký
obchody	obchod	k1gInPc4	obchod
podařilo	podařit	k5eAaPmAgNnS	podařit
vyjednat	vyjednat	k5eAaPmF	vyjednat
stavbu	stavba	k1gFnSc4	stavba
elektrárny	elektrárna	k1gFnSc2	elektrárna
Felton	Felton	k1gInSc1	Felton
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
smlouva	smlouva	k1gFnSc1	smlouva
podepsána	podepsán	k2eAgFnSc1d1	podepsána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
2	[number]	k4	2
<g/>
×	×	k?	×
250	[number]	k4	250
MW	MW	kA	MW
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
kubánskou	kubánský	k2eAgFnSc7d1	kubánská
elektrárnou	elektrárna	k1gFnSc7	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
dodavatelem	dodavatel	k1gMnSc7	dodavatel
je	být	k5eAaImIp3nS	být
Škodaexport	škodaexport	k1gInSc1	škodaexport
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
byla	být	k5eAaImAgFnS	být
plně	plně	k6eAd1	plně
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
dodávky	dodávka	k1gFnPc1	dodávka
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
hutních	hutní	k2eAgInPc2d1	hutní
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
dílů	díl	k1gInPc2	díl
na	na	k7c4	na
montáž	montáž	k1gFnSc4	montáž
motocyklů	motocykl	k1gInPc2	motocykl
Jawa	jawa	k1gFnSc1	jawa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
naopak	naopak	k6eAd1	naopak
putují	putovat	k5eAaImIp3nP	putovat
kubánské	kubánský	k2eAgInPc1d1	kubánský
doutníky	doutník	k1gInPc1	doutník
a	a	k8xC	a
rum	rum	k1gInSc1	rum
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
někdejších	někdejší	k2eAgFnPc2d1	někdejší
předrevolučních	předrevoluční	k2eAgFnPc2d1	předrevoluční
palíren	palírna	k1gFnPc2	palírna
firmy	firma	k1gFnSc2	firma
Bacardi	Bacard	k1gMnPc1	Bacard
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
rekreují	rekreovat	k5eAaImIp3nP	rekreovat
tisíce	tisíc	k4xCgInPc1	tisíc
českých	český	k2eAgMnPc2d1	český
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
kubánské	kubánský	k2eAgFnSc6d1	kubánská
státní	státní	k2eAgFnSc6d1	státní
licenci	licence	k1gFnSc6	licence
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
pražská	pražský	k2eAgFnSc1d1	Pražská
restaurace	restaurace	k1gFnSc1	restaurace
"	"	kIx"	"
<g/>
Bodeguita	Bodeguita	k1gMnSc1	Bodeguita
del	del	k?	del
Medio	Medio	k1gMnSc1	Medio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
řetězce	řetězec	k1gInSc2	řetězec
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
podle	podle	k7c2	podle
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
Hemingwayovy	Hemingwayův	k2eAgFnSc2d1	Hemingwayova
restaurace	restaurace	k1gFnSc2	restaurace
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
státem	stát	k1gInSc7	stát
vysílané	vysílaný	k2eAgFnSc2d1	vysílaná
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
je	být	k5eAaImIp3nS	být
etnicky	etnicky	k6eAd1	etnicky
velmi	velmi	k6eAd1	velmi
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
<g/>
,	,	kIx,	,
obývají	obývat	k5eAaImIp3nP	obývat
ji	on	k3xPp3gFnSc4	on
španělsky	španělsky	k6eAd1	španělsky
mluvící	mluvící	k2eAgMnPc1d1	mluvící
Kubánci	Kubánec	k1gMnPc1	Kubánec
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
především	především	k9	především
z	z	k7c2	z
obyvatel	obyvatel	k1gMnPc2	obyvatel
historického	historický	k2eAgNnSc2d1	historické
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
černošské	černošský	k2eAgMnPc4d1	černošský
otroky	otrok	k1gMnPc4	otrok
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
rasově	rasově	k6eAd1	rasově
tvoří	tvořit	k5eAaImIp3nP	tvořit
především	především	k9	především
běloši	běloch	k1gMnPc1	běloch
(	(	kIx(	(
<g/>
64,1	[number]	k4	64,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mulati	mulat	k1gMnPc1	mulat
<g/>
/	/	kIx~	/
<g/>
mesticové	mestic	k1gMnPc1	mestic
(	(	kIx(	(
<g/>
26,6	[number]	k4	26,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
černoši	černoch	k1gMnPc1	černoch
(	(	kIx(	(
<g/>
9,3	[number]	k4	9,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žije	žít	k5eAaImIp3nS	žít
značná	značný	k2eAgFnSc1d1	značná
-	-	kIx~	-
až	až	k9	až
1	[number]	k4	1
%	%	kIx~	%
-	-	kIx~	-
čínská	čínský	k2eAgFnSc1d1	čínská
menšina	menšina	k1gFnSc1	menšina
obývající	obývající	k2eAgFnSc1d1	obývající
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
vlastní	vlastní	k2eAgFnSc4d1	vlastní
čínskou	čínský	k2eAgFnSc4d1	čínská
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
španělské	španělský	k2eAgFnSc2d1	španělská
kolonizace	kolonizace	k1gFnSc2	kolonizace
vymřely	vymřít	k5eAaPmAgFnP	vymřít
na	na	k7c4	na
nemoci	nemoc	k1gFnPc4	nemoc
zanesené	zanesený	k2eAgFnPc4d1	zanesená
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
původní	původní	k2eAgFnSc2d1	původní
populace	populace	k1gFnSc2	populace
již	již	k6eAd1	již
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
někteří	některý	k3yIgMnPc1	některý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
indiánské	indiánský	k2eAgFnSc3d1	indiánská
krvi	krev	k1gFnSc3	krev
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
katolickou	katolický	k2eAgFnSc7d1	katolická
zemí	zem	k1gFnSc7	zem
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
vlivem	vliv	k1gInSc7	vliv
afrokubánské	afrokubánský	k2eAgFnSc2d1	afrokubánská
Santérie	Santérie	k1gFnSc2	Santérie
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
kultů	kult	k1gInPc2	kult
západoafrického	západoafrický	k2eAgNnSc2d1	západoafrické
etnika	etnikum	k1gNnSc2	etnikum
Yoruba	Yoruba	k1gFnSc1	Yoruba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
Kuba	kuba	k1gNnPc2	kuba
silně	silně	k6eAd1	silně
omezila	omezit	k5eAaPmAgFnS	omezit
náboženský	náboženský	k2eAgInSc4d1	náboženský
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
papírově	papírově	k6eAd1	papírově
nadále	nadále	k6eAd1	nadále
uznávala	uznávat	k5eAaImAgFnS	uznávat
svobodu	svoboda	k1gFnSc4	svoboda
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
této	tento	k3xDgFnSc2	tento
"	"	kIx"	"
<g/>
svobody	svoboda	k1gFnSc2	svoboda
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
odchod	odchod	k1gInSc1	odchod
80	[number]	k4	80
%	%	kIx~	%
katolických	katolický	k2eAgFnPc2d1	katolická
a	a	k8xC	a
protestantských	protestantský	k2eAgFnPc2d1	protestantská
kněžích	kněz	k1gMnPc6	kněz
do	do	k7c2	do
USA	USA	kA	USA
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
<s>
Věřícím	věřící	k1gMnPc3	věřící
byl	být	k5eAaImAgInS	být
zakazován	zakazován	k2eAgInSc4d1	zakazován
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
Kuba	Kuba	k1gFnSc1	Kuba
oficiálně	oficiálně	k6eAd1	oficiálně
stala	stát	k5eAaPmAgFnS	stát
ateistickým	ateistický	k2eAgInSc7d1	ateistický
národem	národ	k1gInSc7	národ
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zakázány	zakázán	k2eAgFnPc1d1	zakázána
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Tenze	tenze	k1gFnSc1	tenze
mezi	mezi	k7c7	mezi
věřícími	věřící	k1gFnPc7	věřící
a	a	k8xC	a
státem	stát	k1gInSc7	stát
mírně	mírně	k6eAd1	mírně
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kardinál	kardinál	k1gMnSc1	kardinál
Ortega	Orteg	k1gMnSc2	Orteg
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
hrály	hrát	k5eAaImAgInP	hrát
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
jistou	jistý	k2eAgFnSc4d1	jistá
roli	role	k1gFnSc4	role
při	při	k7c6	při
propouštění	propouštění	k1gNnSc6	propouštění
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
náboženství	náboženství	k1gNnSc6	náboženství
obyvatel	obyvatel	k1gMnPc2	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
převládá	převládat	k5eAaImIp3nS	převládat
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
kardinálem	kardinál	k1gMnSc7	kardinál
Ortegou	Ortega	k1gFnSc7	Ortega
(	(	kIx(	(
<g/>
Jaime	Jaim	k1gInSc5	Jaim
Lucas	Lucasa	k1gFnPc2	Lucasa
Cardinal	Cardinal	k1gMnSc4	Cardinal
Ortega	Orteg	k1gMnSc4	Orteg
y	y	k?	y
Alamino	Alamin	k2eAgNnSc1d1	Alamino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
hlásí	hlásit	k5eAaImIp3nS	hlásit
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
věřících	věřící	k1gMnPc2	věřící
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
původně	původně	k6eAd1	původně
africká	africký	k2eAgFnSc1d1	africká
Santérie	Santérie	k1gFnSc1	Santérie
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gFnSc3	jejichž
praxi	praxe	k1gFnSc3	praxe
se	se	k3xPyFc4	se
různou	různý	k2eAgFnSc7d1	různá
měrou	míra	k1gFnSc7wR	míra
obrací	obracet	k5eAaImIp3nS	obracet
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
katolicismus	katolicismus	k1gInSc1	katolicismus
a	a	k8xC	a
santérie	santérie	k1gFnSc1	santérie
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
částečně	částečně	k6eAd1	částečně
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
i	i	k9	i
funkční	funkční	k2eAgFnSc1d1	funkční
židovská	židovský	k2eAgFnSc1d1	židovská
obec	obec	k1gFnSc1	obec
a	a	k8xC	a
v	v	k7c6	v
čínském	čínský	k2eAgNnSc6d1	čínské
městě	město	k1gNnSc6	město
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
dominují	dominovat	k5eAaImIp3nP	dominovat
buddhisté	buddhista	k1gMnPc1	buddhista
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
slaběji	slabo	k6eAd2	slabo
zastoupené	zastoupený	k2eAgNnSc1d1	zastoupené
patří	patřit	k5eAaImIp3nS	patřit
ruské	ruský	k2eAgNnSc1d1	ruské
pravoslaví	pravoslaví	k1gNnSc1	pravoslaví
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgMnPc4	který
otevřel	otevřít	k5eAaPmAgInS	otevřít
pravoslavný	pravoslavný	k2eAgInSc1d1	pravoslavný
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
sám	sám	k3xTgInSc1	sám
Raúl	Raúl	k1gInSc1	Raúl
Castro	Castro	k1gNnSc1	Castro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
žily	žít	k5eAaImAgInP	žít
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
Rusů	Rus	k1gMnPc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Katolickou	katolický	k2eAgFnSc7d1	katolická
patronkou	patronka	k1gFnSc7	patronka
Kuby	Kuba	k1gFnSc2	Kuba
je	být	k5eAaImIp3nS	být
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
Milosrdná	milosrdný	k2eAgFnSc1d1	milosrdná
z	z	k7c2	z
Cobre	Cobr	k1gInSc5	Cobr
(	(	kIx(	(
<g/>
Vírgen	Vírgen	k1gInSc1	Vírgen
de	de	k?	de
la	la	k1gNnPc2	la
Caridad	Caridad	k1gInSc1	Caridad
del	del	k?	del
Cobre	Cobr	k1gInSc5	Cobr
<g/>
)	)	kIx)	)
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Maestra	maestro	k1gMnSc2	maestro
<g/>
.	.	kIx.	.
</s>
<s>
Panna	Panna	k1gFnSc1	Panna
Marie	Marie	k1gFnSc1	Marie
z	z	k7c2	z
Cobre	Cobr	k1gInSc5	Cobr
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
afrokubánských	afrokubánský	k2eAgInPc6d1	afrokubánský
kultech	kult	k1gInPc6	kult
synkretizována	synkretizovat	k5eAaImNgFnS	synkretizovat
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
božskou	božský	k2eAgFnSc4d1	božská
osobu	osoba	k1gFnSc4	osoba
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
Očún	Očúna	k1gFnPc2	Očúna
(	(	kIx(	(
<g/>
šp	šp	k?	šp
<g/>
:	:	kIx,	:
Ochún	Ochún	k1gMnSc1	Ochún
<g/>
,	,	kIx,	,
angl	angl	k1gMnSc1	angl
<g/>
:	:	kIx,	:
Oshun	Oshun	k1gMnSc1	Oshun
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
El	Ela	k1gFnPc2	Ela
Cobre	Cobr	k1gInSc5	Cobr
je	on	k3xPp3gMnPc4	on
také	také	k9	také
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
kubánským	kubánský	k2eAgNnSc7d1	kubánské
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
napovídá	napovídat	k5eAaBmIp3nS	napovídat
i	i	k9	i
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
lokalitu	lokalita	k1gFnSc4	lokalita
poblíž	poblíž	k6eAd1	poblíž
historických	historický	k2eAgInPc2d1	historický
měděných	měděný	k2eAgInPc2d1	měděný
dolů	dol	k1gInPc2	dol
založených	založený	k2eAgInPc2d1	založený
kdysi	kdysi	k6eAd1	kdysi
Španěly	Španěl	k1gMnPc7	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Havana	havana	k1gNnSc1	havana
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Habana	Habana	k1gFnSc1	Habana
<g/>
)	)	kIx)	)
-	-	kIx~	-
2,2	[number]	k4	2,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
)	)	kIx)	)
Velkoryse	velkoryse	k6eAd1	velkoryse
řešená	řešený	k2eAgFnSc1d1	řešená
metropole	metropole	k1gFnSc1	metropole
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Floridského	floridský	k2eAgInSc2d1	floridský
průlivu	průliv	k1gInSc2	průliv
<g/>
,	,	kIx,	,
s	s	k7c7	s
cenným	cenný	k2eAgNnSc7d1	cenné
historickým	historický	k2eAgNnSc7d1	historické
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
památka	památka	k1gFnSc1	památka
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
vystěhování	vystěhování	k1gNnSc2	vystěhování
většiny	většina	k1gFnSc2	většina
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
hůře	zle	k6eAd2	zle
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
(	(	kIx(	(
<g/>
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
kubánských	kubánský	k2eAgNnPc2d1	kubánské
měst	město	k1gNnPc2	město
i	i	k8xC	i
vesnic	vesnice	k1gFnPc2	vesnice
kromě	kromě	k7c2	kromě
některých	některý	k3yIgFnPc2	některý
výjimek	výjimka	k1gFnPc2	výjimka
v	v	k7c6	v
historických	historický	k2eAgNnPc6d1	historické
centrech	centrum	k1gNnPc6	centrum
zaměřených	zaměřený	k2eAgNnPc6d1	zaměřené
na	na	k7c4	na
turisty	turist	k1gMnPc4	turist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
existují	existovat	k5eAaImIp3nP	existovat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stavěny	stavěn	k2eAgFnPc4d1	stavěna
zděné	zděný	k2eAgFnPc4d1	zděná
a	a	k8xC	a
železobetonové	železobetonový	k2eAgFnPc4d1	železobetonová
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc1d1	železniční
i	i	k8xC	i
silniční	silniční	k2eAgFnSc1d1	silniční
křižovatka	křižovatka	k1gFnSc1	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Cuba	Cub	k1gInSc2	Cub
-	-	kIx~	-
450	[number]	k4	450
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Původní	původní	k2eAgFnSc7d1	původní
hlavní	hlavní	k2eAgFnSc7d1	hlavní
město	město	k1gNnSc4	město
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Camagüey	Camagüea	k1gFnPc1	Camagüea
-	-	kIx~	-
295	[number]	k4	295
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Centrum	centrum	k1gNnSc1	centrum
dobytkářství	dobytkářství	k1gNnSc1	dobytkářství
<g/>
,	,	kIx,	,
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
spleť	spleť	k1gFnSc1	spleť
uliček	ulička	k1gFnPc2	ulička
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Cienfuegos	Cienfuegosa	k1gFnPc2	Cienfuegosa
Perla	perla	k1gFnSc1	perla
Karibiku	Karibika	k1gFnSc4	Karibika
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Holguín	Holguín	k1gInSc1	Holguín
-	-	kIx~	-
245	[number]	k4	245
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Rušné	rušný	k2eAgNnSc1d1	rušné
město	město	k1gNnSc1	město
bez	bez	k7c2	bez
zásadních	zásadní	k2eAgFnPc2d1	zásadní
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Guantánamo	Guantánamo	k6eAd1	Guantánamo
-	-	kIx~	-
210	[number]	k4	210
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
americká	americký	k2eAgFnSc1d1	americká
vojenská	vojenský	k2eAgFnSc1d1	vojenská
základna	základna	k1gFnSc1	základna
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Guantánamo	Guantánama	k1gFnSc5	Guantánama
<g/>
.	.	kIx.	.
</s>
<s>
Slumy	slum	k1gInPc1	slum
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
zvané	zvaný	k2eAgNnSc1d1	zvané
"	"	kIx"	"
<g/>
Llega	Llega	k1gFnSc1	Llega
y	y	k?	y
Pon	Pon	k1gFnSc1	Pon
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Přijeď	přijet	k5eAaPmRp2nS	přijet
a	a	k8xC	a
usaď	usadit	k5eAaPmRp2nS	usadit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
ukryté	ukrytý	k2eAgFnPc1d1	ukrytá
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
již	již	k9	již
20	[number]	k4	20
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
slum	slum	k1gInSc1	slum
Casablanca	Casablanca	k1gFnSc1	Casablanca
v	v	k7c6	v
havanském	havanský	k2eAgInSc6d1	havanský
okrsku	okrsek	k1gInSc6	okrsek
Regla	Reglo	k1gNnSc2	Reglo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
novější	nový	k2eAgInSc1d2	novější
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
El	Ela	k1gFnPc2	Ela
Bachiplán	Bachiplán	k2eAgInSc1d1	Bachiplán
<g/>
,	,	kIx,	,
Blumer	Blumer	k1gInSc1	Blumer
Caliente	Calient	k1gInSc5	Calient
a	a	k8xC	a
San	San	k1gMnSc1	San
Miguel	Miguel	k1gMnSc1	Miguel
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
vesnice	vesnice	k1gFnSc2	vesnice
bídy	bída	k1gFnSc2	bída
<g/>
"	"	kIx"	"
podobné	podobný	k2eAgNnSc1d1	podobné
slumům	slum	k1gInPc3	slum
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Slumy	slum	k1gInPc1	slum
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
je	být	k5eAaImIp3nS	být
prováděna	prováděn	k2eAgFnSc1d1	prováděna
častá	častý	k2eAgFnSc1d1	častá
kontrola	kontrola	k1gFnSc1	kontrola
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
povolení	povolení	k1gNnSc2	povolení
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
jsou	být	k5eAaImIp3nP	být
odesíláni	odesílat	k5eAaImNgMnP	odesílat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
chudých	chudý	k2eAgFnPc2d1	chudá
provincií	provincie	k1gFnPc2	provincie
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
šancí	šance	k1gFnSc7	šance
najít	najít	k5eAaPmF	najít
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
sužováno	sužovat	k5eAaImNgNnS	sužovat
vleklou	vleklý	k2eAgFnSc7d1	vleklá
krizí	krize	k1gFnSc7	krize
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
centralizovaným	centralizovaný	k2eAgNnSc7d1	centralizované
řízením	řízení	k1gNnSc7	řízení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
i	i	k8xC	i
firem	firma	k1gFnPc2	firma
zabavených	zabavený	k2eAgFnPc2d1	zabavená
jejich	jejich	k3xOp3gMnPc3	jejich
původním	původní	k2eAgMnPc3d1	původní
vlastníkům	vlastník	k1gMnPc3	vlastník
a	a	k8xC	a
také	také	k9	také
nadměrnou	nadměrný	k2eAgFnSc7d1	nadměrná
byrokratizací	byrokratizace	k1gFnSc7	byrokratizace
jak	jak	k8xC	jak
rozhodování	rozhodování	k1gNnSc1	rozhodování
tak	tak	k9	tak
i	i	k9	i
celého	celý	k2eAgInSc2d1	celý
plánovacího	plánovací	k2eAgInSc2d1	plánovací
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
pravidelně	pravidelně	k6eAd1	pravidelně
propadá	propadat	k5eAaPmIp3nS	propadat
do	do	k7c2	do
potíží	potíž	k1gFnPc2	potíž
podle	podle	k7c2	podle
aktuálně	aktuálně	k6eAd1	aktuálně
aplikovaných	aplikovaný	k2eAgFnPc2d1	aplikovaná
metod	metoda	k1gFnPc2	metoda
stranického	stranický	k2eAgNnSc2d1	stranické
vedení	vedení	k1gNnSc2	vedení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
země	zem	k1gFnSc2	zem
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
se	se	k3xPyFc4	se
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
RVHP	RVHP	kA	RVHP
a	a	k8xC	a
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Kubu	Kuba	k1gFnSc4	Kuba
sponzorovaly	sponzorovat	k5eAaImAgInP	sponzorovat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
tzv.	tzv.	kA	tzv.
Zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
Período	Períoda	k1gMnSc5	Períoda
especial	especial	k1gInSc1	especial
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
druhou	druhý	k4xOgFnSc7	druhý
nejchudší	chudý	k2eAgFnSc7d3	nejchudší
zemí	zem	k1gFnSc7	zem
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
před	před	k7c7	před
posledním	poslední	k2eAgNnSc7d1	poslední
Haiti	Haiti	k1gNnSc7	Haiti
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
plat	plat	k1gInSc1	plat
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
dle	dle	k7c2	dle
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
statistik	statistika	k1gFnPc2	statistika
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
přibližně	přibližně	k6eAd1	přibližně
€	€	k?	€
<g/>
12,7	[number]	k4	12,7
(	(	kIx(	(
<g/>
přesně	přesně	k6eAd1	přesně
17.92	[number]	k4	17.92
CUC	CUC	kA	CUC
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
448	[number]	k4	448
CUP	cup	k1gInSc4	cup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInSc1d1	minimální
měsíční	měsíční	k2eAgInSc1d1	měsíční
plat	plat	k1gInSc1	plat
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
stanoven	stanovit	k5eAaPmNgMnS	stanovit
na	na	k7c6	na
€	€	k?	€
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
však	však	k9	však
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
přičíst	přičíst	k5eAaPmF	přičíst
úsporu	úspora	k1gFnSc4	úspora
za	za	k7c4	za
neplacení	neplacení	k1gNnSc4	neplacení
nájmu	nájem	k1gInSc2	nájem
z	z	k7c2	z
bydlení	bydlení	k1gNnSc2	bydlení
(	(	kIx(	(
<g/>
€	€	k?	€
<g/>
150	[number]	k4	150
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnPc1d1	minimální
ceny	cena	k1gFnPc1	cena
energií	energie	k1gFnPc2	energie
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnPc1d1	minimální
ceny	cena	k1gFnPc1	cena
základních	základní	k2eAgFnPc2d1	základní
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
(	(	kIx(	(
<g/>
v	v	k7c6	v
€	€	k?	€
0,10	[number]	k4	0,10
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
V	v	k7c6	v
indexu	index	k1gInSc6	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
Kuba	Kuba	k1gFnSc1	Kuba
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
poměrně	poměrně	k6eAd1	poměrně
vysoko	vysoko	k6eAd1	vysoko
-	-	kIx~	-
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
nad	nad	k7c7	nad
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pod	pod	k7c7	pod
Uruguayí	Uruguay	k1gFnSc7	Uruguay
<g/>
,	,	kIx,	,
Argentinou	Argentina	k1gFnSc7	Argentina
a	a	k8xC	a
Chile	Chile	k1gNnSc7	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc1	zpracování
a	a	k8xC	a
export	export	k1gInSc1	export
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
je	být	k5eAaImIp3nS	být
těžena	těžen	k2eAgFnSc1d1	těžena
nafta	nafta	k1gFnSc1	nafta
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
turistů	turist	k1gMnPc2	turist
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
finanční	finanční	k2eAgInPc1d1	finanční
převody	převod	k1gInPc1	převod
volných	volný	k2eAgFnPc2d1	volná
měn	měna	k1gFnPc2	měna
od	od	k7c2	od
Kubánců	Kubánec	k1gMnPc2	Kubánec
uprchlých	uprchlý	k2eAgMnPc2d1	uprchlý
a	a	k8xC	a
pracujících	pracující	k2eAgMnPc2d1	pracující
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgFnSc1d1	dřívější
hlavní	hlavní	k2eAgFnSc1d1	hlavní
opora	opora	k1gFnSc1	opora
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
cukru	cukr	k1gInSc2	cukr
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
masové	masový	k2eAgFnPc4d1	masová
mobilizace	mobilizace	k1gFnPc4	mobilizace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
sklizní	sklizeň	k1gFnPc2	sklizeň
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
způsobeném	způsobený	k2eAgInSc6d1	způsobený
špatnou	špatná	k1gFnSc4	špatná
organizací	organizace	k1gFnSc7	organizace
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
ovládaném	ovládaný	k2eAgNnSc6d1	ovládané
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
důstojníky	důstojník	k1gMnPc7	důstojník
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
donedávna	donedávna	k6eAd1	donedávna
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
"	"	kIx"	"
<g/>
cukřenka	cukřenka	k1gFnSc1	cukřenka
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
občas	občas	k6eAd1	občas
cukr	cukr	k1gInSc4	cukr
i	i	k9	i
dovážet	dovážet	k5eAaImF	dovážet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgInPc4d1	známý
produkty	produkt	k1gInPc4	produkt
patří	patřit	k5eAaImIp3nS	patřit
zpracování	zpracování	k1gNnSc1	zpracování
tabáku	tabák	k1gInSc2	tabák
a	a	k8xC	a
známých	známý	k2eAgInPc2d1	známý
havanských	havanský	k2eAgInPc2d1	havanský
doutníků	doutník	k1gInPc2	doutník
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
historických	historický	k2eAgFnPc2d1	historická
a	a	k8xC	a
původně	původně	k6eAd1	původně
soukromých	soukromý	k2eAgFnPc2d1	soukromá
značek	značka	k1gFnPc2	značka
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
i	i	k8xC	i
značka	značka	k1gFnSc1	značka
Cohiba	Cohiba	k1gFnSc1	Cohiba
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
původně	původně	k6eAd1	původně
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
Fidela	Fidel	k1gMnSc4	Fidel
Castra	Castrum	k1gNnSc2	Castrum
a	a	k8xC	a
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
i	i	k9	i
export	export	k1gInSc4	export
rumu	rum	k1gInSc2	rum
z	z	k7c2	z
továren	továrna	k1gFnPc2	továrna
zabavených	zabavený	k2eAgFnPc2d1	zabavená
především	především	k6eAd1	především
firmě	firma	k1gFnSc3	firma
Bacardi	Bacard	k1gMnPc1	Bacard
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Mramor	mramor	k1gInSc1	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Návštěva	návštěva	k1gFnSc1	návštěva
kubánského	kubánský	k2eAgInSc2d1	kubánský
hřbitova	hřbitov	k1gInSc2	hřbitov
je	být	k5eAaImIp3nS	být
nezapomenutelným	zapomenutelný	k2eNgInSc7d1	nezapomenutelný
zážitkem	zážitek	k1gInSc7	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Hřbitovy	hřbitov	k1gInPc1	hřbitov
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
malá	malý	k2eAgNnPc4d1	malé
mramorová	mramorový	k2eAgNnPc4d1	mramorové
městečka	městečko	k1gNnPc4	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kvality	kvalita	k1gFnSc2	kvalita
použitého	použitý	k2eAgInSc2d1	použitý
mramoru	mramor	k1gInSc2	mramor
lze	lze	k6eAd1	lze
odhadnout	odhadnout	k5eAaPmF	odhadnout
movitost	movitost	k1gFnSc4	movitost
zemřelého	zemřelý	k1gMnSc2	zemřelý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1960	[number]	k4	1960
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
USA	USA	kA	USA
embargo	embargo	k1gNnSc1	embargo
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
amerických	americký	k2eAgInPc2d1	americký
výrobků	výrobek	k1gInPc2	výrobek
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dále	daleko	k6eAd2	daleko
posílily	posílit	k5eAaPmAgFnP	posílit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1962	[number]	k4	1962
embargem	embargo	k1gNnSc7	embargo
na	na	k7c4	na
veškerý	veškerý	k3xTgInSc4	veškerý
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
a	a	k8xC	a
1996	[number]	k4	1996
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
obchodní	obchodní	k2eAgNnSc4d1	obchodní
embargo	embargo	k1gNnSc4	embargo
ještě	ještě	k6eAd1	ještě
zpřísnily	zpřísnit	k5eAaPmAgFnP	zpřísnit
<g/>
.	.	kIx.	.
</s>
<s>
Helms-Burtonovův	Helms-Burtonovův	k2eAgInSc1d1	Helms-Burtonovův
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
omezuje	omezovat	k5eAaImIp3nS	omezovat
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
z	z	k7c2	z
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
zemi	zem	k1gFnSc3	zem
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
embargo	embargo	k1gNnSc1	embargo
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
postupně	postupně	k6eAd1	postupně
omezováno	omezován	k2eAgNnSc1d1	omezováno
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
první	první	k4xOgMnSc1	první
největší	veliký	k2eAgMnSc1d3	veliký
vývozce	vývozce	k1gMnSc1	vývozce
potravin	potravina	k1gFnPc2	potravina
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
a	a	k8xC	a
současně	současně	k6eAd1	současně
6	[number]	k4	6
<g/>
.	.	kIx.	.
největší	veliký	k2eAgMnSc1d3	veliký
celkový	celkový	k2eAgMnSc1d1	celkový
vývozce	vývozce	k1gMnSc1	vývozce
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
Kuba	Kuba	k1gFnSc1	Kuba
dováží	dovážet	k5eAaImIp3nS	dovážet
i	i	k8xC	i
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
produkty	produkt	k1gInPc4	produkt
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Embargo	embargo	k1gNnSc1	embargo
je	být	k5eAaImIp3nS	být
však	však	k9	však
Kubou	Kuba	k1gFnSc7	Kuba
silně	silně	k6eAd1	silně
využíváno	využívat	k5eAaPmNgNnS	využívat
propagandisticky	propagandisticky	k6eAd1	propagandisticky
a	a	k8xC	a
především	především	k6eAd1	především
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castra	k1gFnSc5	Castra
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
připisoval	připisovat	k5eAaImAgMnS	připisovat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
při	při	k7c6	při
problémech	problém	k1gInPc6	problém
kubánské	kubánský	k2eAgFnSc2d1	kubánská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
Raúla	Raúl	k1gMnSc2	Raúl
Castra	Castr	k1gMnSc2	Castr
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
mírnější	mírný	k2eAgFnSc1d2	mírnější
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
oficiální	oficiální	k2eAgFnPc4d1	oficiální
měny	měna	k1gFnPc4	měna
<g/>
:	:	kIx,	:
kubánské	kubánský	k2eAgMnPc4d1	kubánský
"	"	kIx"	"
<g/>
národní	národní	k2eAgFnSc7d1	národní
<g/>
"	"	kIx"	"
peso	peso	k1gNnSc1	peso
(	(	kIx(	(
<g/>
peso	peso	k1gNnSc1	peso
cubano	cubana	k1gFnSc5	cubana
nacional	nacionat	k5eAaImAgInS	nacionat
<g/>
,	,	kIx,	,
ISO	ISO	kA	ISO
4217	[number]	k4	4217
kód	kód	k1gInSc1	kód
CUP	cup	k1gInSc4	cup
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
měna	měna	k1gFnSc1	měna
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
dostává	dostávat	k5eAaImIp3nS	dostávat
výplatu	výplata	k1gFnSc4	výplata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
se	se	k3xPyFc4	se
za	za	k7c4	za
CUP	cup	k1gInSc4	cup
dostane	dostat	k5eAaPmIp3nS	dostat
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgNnSc1d1	omezené
zboží	zboží	k1gNnSc1	zboží
-	-	kIx~	-
přídělové	přídělový	k2eAgFnPc1d1	přídělová
potraviny	potravina	k1gFnPc1	potravina
a	a	k8xC	a
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
také	také	k9	také
technické	technický	k2eAgInPc4d1	technický
výrobky	výrobek	k1gInPc4	výrobek
kubánské	kubánský	k2eAgFnSc2d1	kubánská
produkce	produkce	k1gFnSc2	produkce
nebo	nebo	k8xC	nebo
asijské	asijský	k2eAgFnSc2d1	asijská
provenience	provenience	k1gFnSc2	provenience
(	(	kIx(	(
<g/>
nejobyčejnější	obyčejný	k2eAgInPc1d3	nejobyčejnější
oděvy	oděv	k1gInPc1	oděv
a	a	k8xC	a
obuv	obuv	k1gFnSc1	obuv
<g/>
,	,	kIx,	,
rákosový	rákosový	k2eAgInSc1d1	rákosový
nábytek	nábytek	k1gInSc1	nábytek
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
CUP	cup	k1gInSc4	cup
je	být	k5eAaImIp3nS	být
také	také	k9	také
zelenina	zelenina	k1gFnSc1	zelenina
a	a	k8xC	a
ovoce	ovoce	k1gNnSc1	ovoce
na	na	k7c6	na
tržišti	tržiště	k1gNnSc6	tržiště
<g/>
.	.	kIx.	.
kubánské	kubánský	k2eAgNnSc1d1	kubánské
konvertibilní	konvertibilní	k2eAgNnSc1d1	konvertibilní
peso	peso	k1gNnSc1	peso
(	(	kIx(	(
<g/>
peso	peso	k1gNnSc1	peso
cubano	cubana	k1gFnSc5	cubana
convertible	convertible	k6eAd1	convertible
<g/>
,	,	kIx,	,
ISO	ISO	kA	ISO
4217	[number]	k4	4217
kód	kód	k1gInSc4	kód
CUC	CUC	kA	CUC
postavené	postavený	k2eAgNnSc1d1	postavené
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
dolaru	dolar	k1gInSc2	dolar
v	v	k7c6	v
kurzu	kurz	k1gInSc6	kurz
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1,08	[number]	k4	1,08
USD	USD	kA	USD
<g/>
)	)	kIx)	)
-	-	kIx~	-
obdoba	obdoba	k1gFnSc1	obdoba
"	"	kIx"	"
<g/>
tuzexových	tuzexový	k2eAgInPc2d1	tuzexový
bonů	bon	k1gInPc2	bon
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
podstatně	podstatně	k6eAd1	podstatně
širší	široký	k2eAgNnSc1d2	širší
využití	využití	k1gNnSc1	využití
než	než	k8xS	než
někdejší	někdejší	k2eAgInPc1d1	někdejší
bony	bon	k1gInPc1	bon
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Směnitelnost	směnitelnost	k1gFnSc1	směnitelnost
CUC	CUC	kA	CUC
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
obchodů	obchod	k1gInPc2	obchod
využitelných	využitelný	k2eAgInPc2d1	využitelný
turisty	turist	k1gMnPc7	turist
prodává	prodávat	k5eAaImIp3nS	prodávat
zboží	zboží	k1gNnSc4	zboží
za	za	k7c4	za
CUC	CUC	kA	CUC
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
otevírá	otevírat	k5eAaImIp3nS	otevírat
dveře	dveře	k1gFnPc4	dveře
ke	k	k7c3	k
kvalitnějšímu	kvalitní	k2eAgMnSc3d2	kvalitnější
byť	byť	k8xS	byť
předraženému	předražený	k2eAgInSc3d1	předražený
<g/>
,	,	kIx,	,
zboží	zboží	k1gNnSc4	zboží
z	z	k7c2	z
dovozu	dovoz	k1gInSc2	dovoz
<g/>
.	.	kIx.	.
</s>
<s>
Řadový	řadový	k2eAgMnSc1d1	řadový
Kubánec	Kubánec	k1gMnSc1	Kubánec
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
oficiální	oficiální	k2eAgFnSc4d1	oficiální
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
CUC	CUC	kA	CUC
vydělat	vydělat	k5eAaPmF	vydělat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
je	on	k3xPp3gMnPc4	on
však	však	k9	však
směnit	směnit	k5eAaPmF	směnit
ve	v	k7c6	v
směnárně	směnárna	k1gFnSc6	směnárna
za	za	k7c4	za
CUP	cup	k1gInSc4	cup
či	či	k8xC	či
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
valuty	valuta	k1gFnPc4	valuta
<g/>
,	,	kIx,	,
kurz	kurz	k1gInSc4	kurz
25	[number]	k4	25
CUP	cup	k1gInSc1	cup
:	:	kIx,	:
1	[number]	k4	1
CUC	CUC	kA	CUC
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
zdrojem	zdroj	k1gInSc7	zdroj
CUC	CUC	kA	CUC
pro	pro	k7c4	pro
Kubánce	Kubánec	k1gMnSc4	Kubánec
jsou	být	k5eAaImIp3nP	být
dary	dar	k1gInPc4	dar
od	od	k7c2	od
jejich	jejich	k3xOp3gMnPc2	jejich
příbuzných	příbuzný	k1gMnPc2	příbuzný
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
odhadů	odhad	k1gInPc2	odhad
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ročně	ročně	k6eAd1	ročně
až	až	k9	až
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
měn	měna	k1gFnPc2	měna
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
i	i	k9	i
euro	euro	k1gNnSc4	euro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
v	v	k7c6	v
izolovaných	izolovaný	k2eAgInPc6d1	izolovaný
turistických	turistický	k2eAgInPc6d1	turistický
centrech	centr	k1gInPc6	centr
jako	jako	k8xS	jako
Varadero	Varadero	k1gNnSc1	Varadero
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
Cayo	Cayo	k6eAd1	Cayo
Largo	largo	k6eAd1	largo
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Eurem	euro	k1gNnSc7	euro
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
možné	možný	k2eAgNnSc1d1	možné
platit	platit	k5eAaImF	platit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tím	ten	k3xDgInSc7	ten
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
pokladny	pokladna	k1gFnSc2	pokladna
zákazník	zákazník	k1gMnSc1	zákazník
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
eurem	euro	k1gNnSc7	euro
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
přepočet	přepočet	k1gInSc4	přepočet
na	na	k7c6	na
CUC	CUC	kA	CUC
a	a	k8xC	a
nazpět	nazpět	k6eAd1	nazpět
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
v	v	k7c6	v
CUC	CUC	kA	CUC
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
zavedení	zavedení	k1gNnSc2	zavedení
eura	euro	k1gNnSc2	euro
bylo	být	k5eAaImAgNnS	být
zkomplikovat	zkomplikovat	k5eAaPmF	zkomplikovat
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
"	"	kIx"	"
<g/>
imperialistickém	imperialistický	k2eAgInSc6d1	imperialistický
<g/>
"	"	kIx"	"
dolaru	dolar	k1gInSc6	dolar
(	(	kIx(	(
<g/>
současně	současně	k6eAd1	současně
totiž	totiž	k9	totiž
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
daň	daň	k1gFnSc1	daň
10	[number]	k4	10
<g/>
%	%	kIx~	%
na	na	k7c4	na
výměnu	výměna	k1gFnSc4	výměna
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
a	a	k8xC	a
zlepšit	zlepšit	k5eAaPmF	zlepšit
příjem	příjem	k1gInSc4	příjem
deviz	deviza	k1gFnPc2	deviza
do	do	k7c2	do
chudé	chudý	k2eAgFnSc2d1	chudá
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
(	(	kIx(	(
<g/>
turisté	turist	k1gMnPc1	turist
dostávají	dostávat	k5eAaImIp3nP	dostávat
zpět	zpět	k6eAd1	zpět
bankovky	bankovka	k1gFnPc4	bankovka
CUC	CUC	kA	CUC
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
bezcenné	bezcenný	k2eAgFnPc1d1	bezcenná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
se	se	k3xPyFc4	se
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
používal	používat	k5eAaImAgMnS	používat
k	k	k7c3	k
platbám	platba	k1gFnPc3	platba
v	v	k7c6	v
převážné	převážný	k2eAgFnSc6d1	převážná
míře	míra	k1gFnSc6	míra
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
(	(	kIx(	(
<g/>
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
však	však	k9	však
byl	být	k5eAaImAgInS	být
stažen	stáhnout	k5eAaPmNgInS	stáhnout
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
CUC	CUC	kA	CUC
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nyní	nyní	k6eAd1	nyní
někdo	někdo	k3yInSc1	někdo
chce	chtít	k5eAaImIp3nS	chtít
v	v	k7c6	v
bance	banka	k1gFnSc6	banka
vyměnit	vyměnit	k5eAaPmF	vyměnit
bankovky	bankovka	k1gFnPc4	bankovka
USD	USD	kA	USD
za	za	k7c4	za
CUC	CUC	kA	CUC
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
účtována	účtován	k2eAgFnSc1d1	účtována
10	[number]	k4	10
<g/>
%	%	kIx~	%
přirážka	přirážka	k1gFnSc1	přirážka
<g/>
,	,	kIx,	,
interpretovaná	interpretovaný	k2eAgFnSc1d1	interpretovaná
jako	jako	k8xC	jako
riziko	riziko	k1gNnSc1	riziko
Kuby	Kuba	k1gFnSc2	Kuba
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
embargované	embargovaný	k2eAgFnSc2d1	embargovaná
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bezhotovostním	bezhotovostní	k2eAgInSc6d1	bezhotovostní
styku	styk	k1gInSc6	styk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
bankovní	bankovní	k2eAgFnSc2d1	bankovní
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tento	tento	k3xDgInSc1	tento
poplatek	poplatek	k1gInSc1	poplatek
účtován	účtován	k2eAgInSc1d1	účtován
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
bankovní	bankovní	k2eAgInSc4d1	bankovní
styk	styk	k1gInSc4	styk
se	se	k3xPyFc4	se
Kuba	Kuba	k1gFnSc1	Kuba
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nepoužívat	používat	k5eNaImF	používat
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
záměr	záměr	k1gInSc1	záměr
zavést	zavést	k5eAaPmF	zavést
euro	euro	k1gNnSc4	euro
jako	jako	k8xS	jako
povinné	povinný	k2eAgNnSc4d1	povinné
platidlo	platidlo	k1gNnSc4	platidlo
při	při	k7c6	při
obchodních	obchodní	k2eAgFnPc6d1	obchodní
transakcích	transakce	k1gFnPc6	transakce
se	s	k7c7	s
zeměmi	zem	k1gFnPc7	zem
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
cesty	cesta	k1gFnPc4	cesta
z	z	k7c2	z
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
EU	EU	kA	EU
včetně	včetně	k7c2	včetně
ČR	ČR	kA	ČR
platí	platit	k5eAaImIp3nS	platit
vízová	vízový	k2eAgFnSc1d1	vízová
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
jinak	jinak	k6eAd1	jinak
platí	platit	k5eAaImIp3nS	platit
dramatický	dramatický	k2eAgInSc4d1	dramatický
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
extrémně	extrémně	k6eAd1	extrémně
omezenými	omezený	k2eAgFnPc7d1	omezená
právy	právo	k1gNnPc7	právo
Kubánců	Kubánec	k1gMnPc2	Kubánec
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
širokými	široký	k2eAgFnPc7d1	široká
možnostmi	možnost	k1gFnPc7	možnost
cizinců	cizinec	k1gMnPc2	cizinec
cestujících	cestující	k1gFnPc2	cestující
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
najdete	najít	k5eAaPmIp2nP	najít
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
"	"	kIx"	"
<g/>
hotelů	hotel	k1gInPc2	hotel
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
autobusů	autobus	k1gInPc2	autobus
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vynucovaného	vynucovaný	k2eAgNnSc2d1	vynucované
dělení	dělení	k1gNnSc2	dělení
plateb	platba	k1gFnPc2	platba
a	a	k8xC	a
rozdílných	rozdílný	k2eAgFnPc6d1	rozdílná
cenách	cena	k1gFnPc6	cena
v	v	k7c6	v
"	"	kIx"	"
<g/>
národních	národní	k2eAgInPc6d1	národní
pesech	peso	k1gNnPc6	peso
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
konvertibilních	konvertibilní	k2eAgNnPc6d1	konvertibilní
pesech	peso	k1gNnPc6	peso
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
valutách	valuta	k1gFnPc6	valuta
<g/>
)	)	kIx)	)
od	od	k7c2	od
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
lepší	dobrý	k2eAgFnSc6d2	lepší
přístupnosti	přístupnost	k1gFnSc6	přístupnost
některého	některý	k3yIgNnSc2	některý
zboží	zboží	k1gNnSc2	zboží
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Komplikace	komplikace	k1gFnPc1	komplikace
nastávají	nastávat	k5eAaImIp3nP	nastávat
i	i	k9	i
při	při	k7c6	při
získávání	získávání	k1gNnSc6	získávání
automobilu	automobil	k1gInSc2	automobil
či	či	k8xC	či
jen	jen	k9	jen
volném	volný	k2eAgInSc6d1	volný
nákupu	nákup	k1gInSc6	nákup
benzínu	benzín	k1gInSc2	benzín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
relativně	relativně	k6eAd1	relativně
dostupný	dostupný	k2eAgMnSc1d1	dostupný
v	v	k7c6	v
omezených	omezený	k2eAgNnPc6d1	omezené
valutových	valutový	k2eAgNnPc6d1	Valutové
(	(	kIx(	(
<g/>
CUC	CUC	kA	CUC
<g/>
)	)	kIx)	)
benzinových	benzinový	k2eAgFnPc6d1	benzinová
pumpách	pumpa	k1gFnPc6	pumpa
<g/>
.	.	kIx.	.
</s>
<s>
Vládou	vláda	k1gFnSc7	vláda
organizované	organizovaný	k2eAgNnSc1d1	organizované
zhoršené	zhoršený	k2eAgNnSc1d1	zhoršené
postavení	postavení	k1gNnSc1	postavení
místních	místní	k2eAgMnPc2d1	místní
občanů	občan	k1gMnPc2	občan
oproti	oproti	k7c3	oproti
cizincům	cizinec	k1gMnPc3	cizinec
ostatně	ostatně	k6eAd1	ostatně
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Nákup	nákup	k1gInSc1	nákup
automobilu	automobil	k1gInSc2	automobil
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
je	být	k5eAaImIp3nS	být
podmíněn	podmíněn	k2eAgInSc1d1	podmíněn
státním	státní	k2eAgNnSc7d1	státní
povolením	povolení	k1gNnSc7	povolení
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
podání	podání	k1gNnSc2	podání
důkazu	důkaz	k1gInSc2	důkaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zájemce	zájemce	k1gMnSc1	zájemce
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c4	na
automobil	automobil	k1gInSc4	automobil
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Automobily	automobil	k1gInPc1	automobil
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
různou	různý	k2eAgFnSc7d1	různá
barvou	barva	k1gFnSc7	barva
poznávacích	poznávací	k2eAgFnPc2d1	poznávací
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pro	pro	k7c4	pro
policii	policie	k1gFnSc4	policie
jasně	jasně	k6eAd1	jasně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
majitele	majitel	k1gMnSc4	majitel
(	(	kIx(	(
<g/>
státní	státní	k2eAgInSc1d1	státní
podnik	podnik	k1gInSc1	podnik
<g/>
,	,	kIx,	,
soukromník	soukromník	k1gMnSc1	soukromník
<g/>
,	,	kIx,	,
turista	turista	k1gMnSc1	turista
či	či	k8xC	či
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ulicích	ulice	k1gFnPc6	ulice
jezdí	jezdit	k5eAaImIp3nP	jezdit
americké	americký	k2eAgInPc1d1	americký
vozy	vůz	k1gInPc1	vůz
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
v	v	k7c4	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
držící	držící	k2eAgMnSc1d1	držící
pohromadě	pohromadě	k6eAd1	pohromadě
často	často	k6eAd1	často
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
sovětských	sovětský	k2eAgFnPc2d1	sovětská
aj.	aj.	kA	aj.
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
sovětské	sovětský	k2eAgFnSc2d1	sovětská
lady	lady	k1gFnSc2	lady
a	a	k8xC	a
nová	nový	k2eAgNnPc4d1	nové
vozidla	vozidlo	k1gNnPc4	vozidlo
státních	státní	k2eAgFnPc2d1	státní
firem	firma	k1gFnPc2	firma
Peugeot	peugeot	k1gInSc1	peugeot
<g/>
,	,	kIx,	,
Volkswagen	volkswagen	k1gInSc1	volkswagen
<g/>
,	,	kIx,	,
Mercedes	mercedes	k1gInSc1	mercedes
<g/>
,	,	kIx,	,
Toyota	toyota	k1gFnSc1	toyota
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
a	a	k8xC	a
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
značky	značka	k1gFnPc1	značka
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
pro	pro	k7c4	pro
soukromníky	soukromník	k1gMnPc4	soukromník
prakticky	prakticky	k6eAd1	prakticky
nedostupné	dostupný	k2eNgNnSc1d1	nedostupné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
křižovatkách	křižovatka	k1gFnPc6	křižovatka
i	i	k8xC	i
ve	v	k7c6	v
městech	město	k1gNnPc6	město
obvykle	obvykle	k6eAd1	obvykle
stojí	stát	k5eAaImIp3nP	stát
"	"	kIx"	"
<g/>
vytěžovači	vytěžovač	k1gMnPc1	vytěžovač
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
zefektivňovači	zefektivňovač	k1gMnPc1	zefektivňovač
dopravy	doprava	k1gFnSc2	doprava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
státní	státní	k2eAgInPc1d1	státní
vozy	vůz	k1gInPc1	vůz
nejsou	být	k5eNaImIp3nP	být
zneužívány	zneužívat	k5eAaImNgInP	zneužívat
k	k	k7c3	k
soukromým	soukromý	k2eAgInPc3d1	soukromý
účelům	účel	k1gInPc3	účel
a	a	k8xC	a
do	do	k7c2	do
poloprázdných	poloprázdný	k2eAgNnPc2d1	poloprázdné
aut	auto	k1gNnPc2	auto
rozmísťují	rozmísťovat	k5eAaImIp3nP	rozmísťovat
čekající	čekající	k2eAgMnPc4d1	čekající
"	"	kIx"	"
<g/>
stopaře	stopař	k1gMnPc4	stopař
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
se	se	k3xPyFc4	se
rozmohla	rozmoct	k5eAaPmAgFnS	rozmoct
po	po	k7c6	po
kolapsu	kolaps	k1gInSc6	kolaps
státní	státní	k2eAgFnSc2d1	státní
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
především	především	k9	především
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
socialistického	socialistický	k2eAgInSc2d1	socialistický
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
však	však	k9	však
oprávnění	oprávněný	k2eAgMnPc1d1	oprávněný
vnucovat	vnucovat	k5eAaImF	vnucovat
"	"	kIx"	"
<g/>
stopaře	stopař	k1gMnSc4	stopař
<g/>
"	"	kIx"	"
do	do	k7c2	do
pronajatých	pronajatý	k2eAgNnPc2d1	pronajaté
vozidel	vozidlo	k1gNnPc2	vozidlo
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
a	a	k8xC	a
hlavní	hlavní	k2eAgFnPc1d1	hlavní
silnice	silnice	k1gFnPc1	silnice
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
použitelném	použitelný	k2eAgInSc6d1	použitelný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
pohyb	pohyb	k1gInSc1	pohyb
dobytka	dobytek	k1gInSc2	dobytek
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
častým	častý	k2eAgInSc7d1	častý
jevem	jev	k1gInSc7	jev
a	a	k8xC	a
řidič	řidič	k1gMnSc1	řidič
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
střehu	střeh	k1gInSc6	střeh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
cest	cesta	k1gFnPc2	cesta
cizinců	cizinec	k1gMnPc2	cizinec
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
platil	platit	k5eAaImAgInS	platit
donedávna	donedávna	k6eAd1	donedávna
pro	pro	k7c4	pro
Kubánce	Kubánec	k1gMnPc4	Kubánec
striktní	striktní	k2eAgInSc1d1	striktní
zákaz	zákaz	k1gInSc1	zákaz
vycestovat	vycestovat	k5eAaPmF	vycestovat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
bez	bez	k7c2	bez
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
povolení	povolení	k1gNnSc2	povolení
(	(	kIx(	(
<g/>
Carta	Cart	k1gMnSc4	Cart
Blanca	Blanc	k1gMnSc4	Blanc
<g/>
,	,	kIx,	,
obdoba	obdoba	k1gFnSc1	obdoba
výjezdní	výjezdní	k2eAgFnSc2d1	výjezdní
doložky	doložka	k1gFnSc2	doložka
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
není	být	k5eNaImIp3nS	být
vydáváno	vydávat	k5eAaImNgNnS	vydávat
automaticky	automaticky	k6eAd1	automaticky
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
stojí	stát	k5eAaImIp3nS	stát
mnohanásobek	mnohanásobek	k1gInSc1	mnohanásobek
měsíčního	měsíční	k2eAgInSc2d1	měsíční
platu	plat	k1gInSc2	plat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
(	(	kIx(	(
<g/>
150	[number]	k4	150
CUC	CUC	kA	CUC
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
fenomén	fenomén	k1gInSc1	fenomén
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
balseros	balserosa	k1gFnPc2	balserosa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
uprchlíků	uprchlík	k1gMnPc2	uprchlík
na	na	k7c6	na
rybářských	rybářský	k2eAgFnPc6d1	rybářská
loďkách	loďka	k1gFnPc6	loďka
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zrušení	zrušení	k1gNnSc1	zrušení
zákazu	zákaz	k1gInSc2	zákaz
bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
zvažováno	zvažovat	k5eAaImNgNnS	zvažovat
již	již	k6eAd1	již
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Raúla	Raúl	k1gMnSc2	Raúl
Castra	Castr	k1gMnSc2	Castr
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
příznivě	příznivě	k6eAd1	příznivě
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
i	i	k8xC	i
Mariela	Mariela	k1gFnSc1	Mariela
Castro	Castro	k1gNnSc1	Castro
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Raúla	Raúla	k1gFnSc1	Raúla
Castra	Castra	k1gFnSc1	Castra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
kubánská	kubánský	k2eAgFnSc1d1	kubánská
média	médium	k1gNnPc4	médium
informovala	informovat	k5eAaBmAgFnS	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povinnost	povinnost	k1gFnSc1	povinnost
Kubánců	Kubánec	k1gMnPc2	Kubánec
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
výjezdu	výjezd	k1gInSc3	výjezd
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
bude	být	k5eAaImBp3nS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Opatření	opatření	k1gNnSc1	opatření
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
14	[number]	k4	14
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
trvalým	trvalý	k2eAgNnSc7d1	trvalé
bydlištěm	bydliště	k1gNnSc7	bydliště
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
mohou	moct	k5eAaImIp3nP	moct
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
24	[number]	k4	24
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
muset	muset	k5eAaImF	muset
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
se	se	k3xPyFc4	se
výjezdní	výjezdní	k2eAgFnPc1d1	výjezdní
doložky	doložka	k1gFnPc1	doložka
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nichž	jenž	k3xRgMnPc2	jenž
Kubánci	Kubánec	k1gMnPc1	Kubánec
nemohli	moct	k5eNaImAgMnP	moct
vycestovat	vycestovat	k5eAaPmF	vycestovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
vydávaly	vydávat	k5eAaImAgFnP	vydávat
jenom	jenom	k9	jenom
na	na	k7c4	na
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
nicméně	nicméně	k8xC	nicméně
dále	daleko	k6eAd2	daleko
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
potřebu	potřeba	k1gFnSc4	potřeba
chránit	chránit	k5eAaImF	chránit
"	"	kIx"	"
<g/>
lidský	lidský	k2eAgInSc4d1	lidský
kapitál	kapitál	k1gInSc4	kapitál
<g/>
"	"	kIx"	"
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vysoce	vysoce	k6eAd1	vysoce
kvalifikovaní	kvalifikovaný	k2eAgMnPc1d1	kvalifikovaný
profesionálové	profesionál	k1gMnPc1	profesionál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
i	i	k9	i
nadále	nadále	k6eAd1	nadále
překonat	překonat	k5eAaPmF	překonat
více	hodně	k6eAd2	hodně
překážek	překážka	k1gFnPc2	překážka
<g/>
,	,	kIx,	,
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
-li	i	k?	-li
vycestovat	vycestovat	k5eAaPmF	vycestovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
potíže	potíž	k1gFnPc4	potíž
také	také	k9	také
zřejmě	zřejmě	k6eAd1	zřejmě
narazí	narazit	k5eAaPmIp3nP	narazit
kritici	kritik	k1gMnPc1	kritik
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
prodlužování	prodlužování	k1gNnSc3	prodlužování
pasů	pas	k1gInPc2	pas
jim	on	k3xPp3gMnPc3	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odepřeno	odepřít	k5eAaPmNgNnS	odepřít
z	z	k7c2	z
"	"	kIx"	"
<g/>
důvodů	důvod	k1gInPc2	důvod
veřejného	veřejný	k2eAgInSc2d1	veřejný
zájmu	zájem	k1gInSc2	zájem
definovaných	definovaný	k2eAgInPc2d1	definovaný
orgány	orgán	k1gInPc7	orgán
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zastavení	zastavení	k1gNnSc1	zastavení
půjček	půjčka	k1gFnPc2	půjčka
od	od	k7c2	od
zemí	zem	k1gFnPc2	zem
socialistického	socialistický	k2eAgInSc2d1	socialistický
bloku	blok	k1gInSc2	blok
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
prudký	prudký	k2eAgInSc1d1	prudký
propad	propad	k1gInSc1	propad
vlastní	vlastní	k2eAgFnSc2d1	vlastní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
donutil	donutit	k5eAaPmAgMnS	donutit
Kubu	Kuba	k1gFnSc4	Kuba
otevřít	otevřít	k5eAaPmF	otevřít
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
turistům	turist	k1gMnPc3	turist
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgNnSc7d3	nejznámější
letoviskem	letovisko	k1gNnSc7	letovisko
je	být	k5eAaImIp3nS	být
Varadero	Varadero	k1gNnSc1	Varadero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
UNESCO	UNESCO	kA	UNESCO
nejnavštěvovanější	navštěvovaný	k2eAgNnSc1d3	nejnavštěvovanější
město	město	k1gNnSc1	město
Havana	Havana	k1gFnSc1	Havana
<g/>
,	,	kIx,	,
blízký	blízký	k2eAgInSc1d1	blízký
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
také	také	k9	také
nedaleká	daleký	k2eNgFnSc1d1	nedaleká
oblast	oblast	k1gFnSc1	oblast
Viñ	Viñ	k1gFnSc2	Viñ
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Pinar	Pinar	k1gMnSc1	Pinar
del	del	k?	del
Río	Río	k1gMnSc1	Río
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
turistických	turistický	k2eAgFnPc2d1	turistická
oblastí	oblast	k1gFnPc2	oblast
tvoří	tvořit	k5eAaImIp3nP	tvořit
zóny	zóna	k1gFnPc1	zóna
zakázané	zakázaný	k2eAgFnPc1d1	zakázaná
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
nezaměstnaní	zaměstnaný	k2eNgMnPc1d1	nezaměstnaný
či	či	k8xC	či
nežijící	žijící	k2eNgMnPc1d1	nežijící
Kubánci	Kubánec	k1gMnPc1	Kubánec
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
měli	mít	k5eAaImAgMnP	mít
donedávna	donedávna	k6eAd1	donedávna
zakázán	zakázán	k2eAgInSc4d1	zakázán
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
vládou	vláda	k1gFnSc7	vláda
Raúla	Raúl	k1gMnSc2	Raúl
Castra	Castr	k1gMnSc2	Castr
uvolněn	uvolněn	k2eAgInSc1d1	uvolněn
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
nadále	nadále	k6eAd1	nadále
určeny	určit	k5eAaPmNgInP	určit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
a	a	k8xC	a
vrcholné	vrcholný	k2eAgMnPc4d1	vrcholný
politiky	politik	k1gMnPc4	politik
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
případná	případný	k2eAgFnSc1d1	případná
byť	byť	k8xS	byť
povolená	povolený	k2eAgFnSc1d1	povolená
návštěva	návštěva	k1gFnSc1	návštěva
Kubánce	Kubánec	k1gMnSc2	Kubánec
může	moct	k5eAaImIp3nS	moct
mezi	mezi	k7c7	mezi
místními	místní	k2eAgFnPc7d1	místní
bezpečnostními	bezpečnostní	k2eAgFnPc7d1	bezpečnostní
složkami	složka	k1gFnPc7	složka
vzbudit	vzbudit	k5eAaPmF	vzbudit
nežádoucí	žádoucí	k2eNgInSc1d1	nežádoucí
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
rekreačních	rekreační	k2eAgNnPc2d1	rekreační
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
politiky	politik	k1gMnPc4	politik
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
Kubáncům	Kubánec	k1gMnPc3	Kubánec
i	i	k8xC	i
turistům	turist	k1gMnPc3	turist
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
historických	historický	k2eAgNnPc2d1	historické
center	centrum	k1gNnPc2	centrum
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
opravena	opravit	k5eAaPmNgFnS	opravit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ostrově	ostrov	k1gInSc6	ostrov
lze	lze	k6eAd1	lze
cestovat	cestovat	k5eAaImF	cestovat
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
oblastí	oblast	k1gFnPc2	oblast
autem	auto	k1gNnSc7	auto
<g/>
,	,	kIx,	,
na	na	k7c6	na
motorce	motorka	k1gFnSc6	motorka
i	i	k8xC	i
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
možnosti	možnost	k1gFnSc6	možnost
přespání	přespání	k1gNnPc2	přespání
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgInPc1d1	omezený
na	na	k7c4	na
oficiální	oficiální	k2eAgInPc4d1	oficiální
hotely	hotel	k1gInPc4	hotel
a	a	k8xC	a
soukromá	soukromý	k2eAgNnPc4d1	soukromé
ubytovací	ubytovací	k2eAgNnPc4d1	ubytovací
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Organizovaní	organizovaný	k2eAgMnPc1d1	organizovaný
turisté	turist	k1gMnPc1	turist
jsou	být	k5eAaImIp3nP	být
přepravováni	přepravovat	k5eAaImNgMnP	přepravovat
převážně	převážně	k6eAd1	převážně
klimatizovanými	klimatizovaný	k2eAgInPc7d1	klimatizovaný
autobusy	autobus	k1gInPc7	autobus
Yutong	Yutonga	k1gFnPc2	Yutonga
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Kubánce	Kubánec	k1gMnSc4	Kubánec
nedostupným	dostupný	k2eNgNnSc7d1	nedostupné
<g/>
.	.	kIx.	.
</s>
<s>
Orientace	orientace	k1gFnSc1	orientace
v	v	k7c6	v
hromadných	hromadný	k2eAgInPc6d1	hromadný
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
dodávky	dodávka	k1gFnPc4	dodávka
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
celé	celý	k2eAgNnSc4d1	celé
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgMnPc1d1	individuální
turisté	turist	k1gMnPc1	turist
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
pronajmout	pronajmout	k5eAaPmF	pronajmout
osobní	osobní	k2eAgInPc4d1	osobní
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
<g/>
,	,	kIx,	,
Toyota	toyota	k1gFnSc1	toyota
Yaris	Yaris	k1gFnSc1	Yaris
<g/>
,	,	kIx,	,
Peugeot	peugeot	k1gInSc1	peugeot
206	[number]	k4	206
případně	případně	k6eAd1	případně
další	další	k2eAgFnPc4d1	další
značky	značka	k1gFnPc4	značka
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
pronájmu	pronájem	k1gInSc2	pronájem
automobilů	automobil	k1gInPc2	automobil
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgInPc1d2	vyšší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jako	jako	k8xC	jako
taxi	taxi	k1gNnSc4	taxi
lze	lze	k6eAd1	lze
zajistit	zajistit	k5eAaPmF	zajistit
běžné	běžný	k2eAgInPc1d1	běžný
automobily	automobil	k1gInPc1	automobil
i	i	k8xC	i
koňské	koňský	k2eAgFnPc1d1	koňská
drožky	drožka	k1gFnPc1	drožka
a	a	k8xC	a
malé	malý	k2eAgFnPc1d1	malá
motorové	motorový	k2eAgFnPc1d1	motorová
tříkolky	tříkolka	k1gFnPc1	tříkolka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Kokotaxi	Kokotaxe	k1gFnSc4	Kokotaxe
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
benzínu	benzín	k1gInSc2	benzín
a	a	k8xC	a
nafty	nafta	k1gFnSc2	nafta
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dražší	drahý	k2eAgFnSc1d2	dražší
než	než	k8xS	než
cena	cena	k1gFnSc1	cena
světová	světový	k2eAgFnSc1d1	světová
(	(	kIx(	(
<g/>
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
1,5	[number]	k4	1,5
USD	USD	kA	USD
(	(	kIx(	(
<g/>
37	[number]	k4	37
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
litr	litr	k1gInSc1	litr
olovnatého	olovnatý	k2eAgInSc2d1	olovnatý
benzínu	benzín	k1gInSc2	benzín
94	[number]	k4	94
oktanu	oktan	k1gInSc2	oktan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bezolovnatý	bezolovnatý	k2eAgInSc1d1	bezolovnatý
benzín	benzín	k1gInSc1	benzín
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ubytovávání	ubytovávání	k1gNnSc1	ubytovávání
je	být	k5eAaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
přísnou	přísný	k2eAgFnSc7d1	přísná
kontrolou	kontrola	k1gFnSc7	kontrola
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
přísné	přísný	k2eAgInPc4d1	přísný
tresty	trest	k1gInPc4	trest
za	za	k7c4	za
soukromé	soukromý	k2eAgNnSc4d1	soukromé
ubytování	ubytování	k1gNnSc4	ubytování
cizince	cizinec	k1gMnSc2	cizinec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
nemá	mít	k5eNaImIp3nS	mít
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
licenci	licence	k1gFnSc4	licence
<g/>
,	,	kIx,	,
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
15-30	[number]	k4	15-30
CUC	CUC	kA	CUC
za	za	k7c4	za
pokoj	pokoj	k1gInSc4	pokoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ubytování	ubytování	k1gNnSc4	ubytování
cizího	cizí	k2eAgMnSc2d1	cizí
hosta	host	k1gMnSc2	host
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
hostitele	hostitel	k1gMnSc4	hostitel
relativně	relativně	k6eAd1	relativně
riskantním	riskantní	k2eAgInPc3d1	riskantní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
ovoce	ovoce	k1gNnSc1	ovoce
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
velmi	velmi	k6eAd1	velmi
levné	levný	k2eAgFnSc2d1	levná
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
rybích	rybí	k2eAgInPc2d1	rybí
výrobků	výrobek	k1gInPc2	výrobek
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
dostupnost	dostupnost	k1gFnSc1	dostupnost
problematičtější	problematický	k2eAgFnSc1d2	problematičtější
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
ubytování	ubytování	k1gNnSc2	ubytování
Kubánců	Kubánec	k1gMnPc2	Kubánec
v	v	k7c6	v
hotelích	hotel	k1gInPc6	hotel
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
neplatí	platit	k5eNaImIp3nS	platit
(	(	kIx(	(
<g/>
Varadero	Varadero	k1gNnSc1	Varadero
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
povoleno	povolit	k5eAaPmNgNnS	povolit
kubánskou	kubánský	k2eAgFnSc7d1	kubánská
ústavou	ústava	k1gFnSc7	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
bylo	být	k5eAaImAgNnS	být
novou	nový	k2eAgFnSc7d1	nová
vládou	vláda	k1gFnSc7	vláda
Raúla	Raúl	k1gMnSc2	Raúl
Castra	Castr	k1gMnSc2	Castr
zrušeno	zrušen	k2eAgNnSc1d1	zrušeno
<g/>
,	,	kIx,	,
ubytování	ubytování	k1gNnSc4	ubytování
je	být	k5eAaImIp3nS	být
však	však	k9	však
de	de	k?	de
facto	facto	k1gNnSc1	facto
znemožněno	znemožněn	k2eAgNnSc1d1	znemožněno
výškou	výška	k1gFnSc7	výška
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
Kubánci	Kubánec	k1gMnPc1	Kubánec
převážně	převážně	k6eAd1	převážně
využívají	využívat	k5eAaImIp3nP	využívat
odborářských	odborářský	k2eAgInPc2d1	odborářský
rekreačních	rekreační	k2eAgInPc2d1	rekreační
objektů	objekt	k1gInPc2	objekt
na	na	k7c6	na
stejných	stejný	k2eAgFnPc6d1	stejná
státních	státní	k2eAgFnPc6d1	státní
plážích	pláž	k1gFnPc6	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Kubánský	kubánský	k2eAgMnSc1d1	kubánský
turista	turista	k1gMnSc1	turista
s	s	k7c7	s
vyššími	vysoký	k2eAgInPc7d2	vyšší
příjmy	příjem	k1gInPc7	příjem
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
ubytovat	ubytovat	k5eAaPmF	ubytovat
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
hotelu	hotel	k1gInSc6	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
ubytoven	ubytovna	k1gFnPc2	ubytovna
různého	různý	k2eAgInSc2d1	různý
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
pionýrské	pionýrský	k2eAgFnSc2d1	Pionýrská
<g/>
,	,	kIx,	,
školní	školní	k2eAgFnSc2d1	školní
<g/>
,	,	kIx,	,
<g/>
odborářské	odborářský	k2eAgFnSc2d1	odborářská
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
přeměněny	přeměnit	k5eAaPmNgFnP	přeměnit
na	na	k7c4	na
standardní	standardní	k2eAgNnSc4d1	standardní
ubytování	ubytování	k1gNnSc4	ubytování
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc4d1	další
slouží	sloužit	k5eAaImIp3nS	sloužit
nadále	nadále	k6eAd1	nadále
Kubáncům	Kubánec	k1gMnPc3	Kubánec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
podporuje	podporovat	k5eAaImIp3nS	podporovat
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
turistiky	turistika	k1gFnSc2	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
veselí	veselý	k2eAgMnPc1d1	veselý
a	a	k8xC	a
přívětiví	přívětivý	k2eAgMnPc1d1	přívětivý
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
běžných	běžný	k2eAgInPc2d1	běžný
mezilidských	mezilidský	k2eAgInPc2d1	mezilidský
vztahů	vztah	k1gInPc2	vztah
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
ostrova	ostrov	k1gInSc2	ostrov
samozřejmě	samozřejmě	k6eAd1	samozřejmě
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
propagandy	propaganda	k1gFnSc2	propaganda
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
Kubánců	Kubánec	k1gMnPc2	Kubánec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
se	s	k7c7	s
společenským	společenský	k2eAgInSc7d1	společenský
řádem	řád	k1gInSc7	řád
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
dramatický	dramatický	k2eAgInSc4d1	dramatický
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
příjmech	příjem	k1gInPc6	příjem
mezi	mezi	k7c7	mezi
Kubánci	Kubánec	k1gMnPc7	Kubánec
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
€	€	k?	€
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
kompenzováno	kompenzovat	k5eAaBmNgNnS	kompenzovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kubánci	Kubánec	k1gMnPc1	Kubánec
nemusí	muset	k5eNaImIp3nP	muset
platit	platit	k5eAaImF	platit
nájem	nájem	k1gInSc4	nájem
za	za	k7c4	za
byt	byt	k1gInSc4	byt
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
věcí	věc	k1gFnPc2	věc
domácí	domácí	k2eAgFnSc2d1	domácí
potřeby	potřeba	k1gFnSc2	potřeba
dostávají	dostávat	k5eAaImIp3nP	dostávat
zdarma	zdarma	k6eAd1	zdarma
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc2	energie
jsou	být	k5eAaImIp3nP	být
neskonale	skonale	k6eNd1	skonale
levné	levný	k2eAgFnPc1d1	levná
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nikomu	nikdo	k3yNnSc3	nikdo
nehrozí	hrozit	k5eNaImIp3nS	hrozit
hlad	hlad	k1gInSc1	hlad
a	a	k8xC	a
bída	bída	k1gFnSc1	bída
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nikdo	nikdo	k3yNnSc1	nikdo
nemusí	muset	k5eNaImIp3nS	muset
žebrat	žebrat	k5eAaImF	žebrat
a	a	k8xC	a
nikde	nikde	k6eAd1	nikde
nevidíte	vidět	k5eNaImIp2nP	vidět
bezdomovce	bezdomovec	k1gMnSc4	bezdomovec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Lidé	člověk	k1gMnPc1	člověk
bydlí	bydlet	k5eAaImIp3nP	bydlet
zdarma	zdarma	k6eAd1	zdarma
ve	v	k7c6	v
státních	státní	k2eAgInPc6d1	státní
domech	dům	k1gInPc6	dům
(	(	kIx(	(
<g/>
zabavených	zabavený	k2eAgFnPc2d1	zabavená
původním	původní	k2eAgMnPc3d1	původní
majitelům	majitel	k1gMnPc3	majitel
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
v	v	k7c6	v
domech	dům	k1gInPc6	dům
vybudovaných	vybudovaný	k2eAgInPc6d1	vybudovaný
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc4d1	základní
potraviny	potravina	k1gFnPc4	potravina
pro	pro	k7c4	pro
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
příděl	příděl	k1gInSc4	příděl
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
lístků	lístek	k1gInPc2	lístek
a	a	k8xC	a
v	v	k7c6	v
omezeném	omezený	k2eAgNnSc6d1	omezené
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dostávají	dostávat	k5eAaImIp3nP	dostávat
od	od	k7c2	od
státu	stát	k1gInSc2	stát
za	za	k7c4	za
nízké	nízký	k2eAgFnPc4d1	nízká
částky	částka	k1gFnPc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
lze	lze	k6eAd1	lze
nakoupit	nakoupit	k5eAaPmF	nakoupit
na	na	k7c6	na
tržištích	tržiště	k1gNnPc6	tržiště
a	a	k8xC	a
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
převládají	převládat	k5eAaImIp3nP	převládat
zděné	zděný	k2eAgInPc4d1	zděný
nebo	nebo	k8xC	nebo
panelové	panelový	k2eAgInPc4d1	panelový
domy	dům	k1gInPc4	dům
a	a	k8xC	a
domky	domek	k1gInPc4	domek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vesnicích	vesnice	k1gFnPc6	vesnice
je	být	k5eAaImIp3nS	být
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
domů	dům	k1gInPc2	dům
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
okolo	okolo	k7c2	okolo
20	[number]	k4	20
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
angličtiny	angličtina	k1gFnSc2	angličtina
v	v	k7c6	v
turisticky	turisticky	k6eAd1	turisticky
atraktivních	atraktivní	k2eAgFnPc6d1	atraktivní
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
přijatelná	přijatelný	k2eAgFnSc1d1	přijatelná
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
ně	on	k3xPp3gFnPc4	on
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mluvit	mluvit	k5eAaImF	mluvit
španělsky	španělsky	k6eAd1	španělsky
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
Kubánci	Kubánec	k1gMnPc1	Kubánec
nad	nad	k7c4	nad
15	[number]	k4	15
let	léto	k1gNnPc2	léto
běžně	běžně	k6eAd1	běžně
ovládají	ovládat	k5eAaImIp3nP	ovládat
angličtinu	angličtina	k1gFnSc4	angličtina
a	a	k8xC	a
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
také	také	k9	také
německy	německy	k6eAd1	německy
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
turistických	turistický	k2eAgFnPc6d1	turistická
destinacích	destinace	k1gFnPc6	destinace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
Amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
drobné	drobný	k2eAgFnPc4d1	drobná
krádeže	krádež	k1gFnPc4	krádež
za	za	k7c2	za
bílého	bílý	k2eAgInSc2d1	bílý
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Nápravná	nápravný	k2eAgNnPc1d1	nápravné
zařízení	zařízení	k1gNnPc1	zařízení
převážně	převážně	k6eAd1	převážně
jsou	být	k5eAaImIp3nP	být
prázdná	prázdný	k2eAgNnPc1d1	prázdné
<g/>
,	,	kIx,	,
jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
jsou	být	k5eAaImIp3nP	být
umístěni	umístěn	k2eAgMnPc1d1	umístěn
disidenti	disident	k1gMnPc1	disident
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
protistátní	protistátní	k2eAgInPc1d1	protistátní
živly	živel	k1gInPc1	živel
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
věznice	věznice	k1gFnSc2	věznice
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Guantánamo	Guantánama	k1gFnSc5	Guantánama
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgFnPc1d1	plná
nepřátel	nepřítel	k1gMnPc2	nepřítel
USA	USA	kA	USA
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
teroristů	terorista	k1gMnPc2	terorista
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
létá	létat	k5eAaImIp3nS	létat
přes	přes	k7c4	přes
100	[number]	k4	100
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
charterového	charterový	k2eAgInSc2d1	charterový
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
založena	založen	k2eAgFnSc1d1	založena
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
letecká	letecký	k2eAgFnSc1d1	letecká
asociace	asociace	k1gFnSc1	asociace
IATA	IATA	kA	IATA
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
pravidelná	pravidelný	k2eAgNnPc1d1	pravidelné
spojení	spojení	k1gNnPc1	spojení
<g/>
:	:	kIx,	:
Air	Air	k1gFnSc1	Air
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
Iberia	Iberium	k1gNnSc2	Iberium
<g/>
,	,	kIx,	,
Cubana	Cubana	k1gFnSc1	Cubana
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Air	Air	k1gMnSc1	Air
<g/>
,	,	kIx,	,
Mexicana	Mexicana	k1gFnSc1	Mexicana
<g/>
,	,	kIx,	,
Condor	Condor	k1gInSc1	Condor
<g/>
,	,	kIx,	,
Air	Air	k1gMnSc5	Air
Europe	Europ	k1gMnSc5	Europ
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
<g/>
:	:	kIx,	:
Havana	havana	k1gNnSc1	havana
<g/>
,	,	kIx,	,
Varadero	Varadero	k1gNnSc1	Varadero
<g/>
,	,	kIx,	,
Holguín	Holguín	k1gInSc1	Holguín
a	a	k8xC	a
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Cuba	Cub	k1gInSc2	Cub
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
autobusy	autobus	k1gInPc1	autobus
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
sloužily	sloužit	k5eAaImAgInP	sloužit
donedávna	donedávna	k6eAd1	donedávna
především	především	k9	především
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
velbloudi	velbloud	k1gMnPc1	velbloud
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
camellos	camellos	k1gInSc1	camellos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
hrbaté	hrbatý	k2eAgInPc4d1	hrbatý
plechové	plechový	k2eAgInPc4d1	plechový
návěsy	návěs	k1gInPc4	návěs
k	k	k7c3	k
nákladním	nákladní	k2eAgInPc3d1	nákladní
automobilům	automobil	k1gInPc3	automobil
TIR	TIR	kA	TIR
přizpůsobené	přizpůsobený	k2eAgInPc1d1	přizpůsobený
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Camellos	Camellos	k1gInSc1	Camellos
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
zachraňovaly	zachraňovat	k5eAaImAgFnP	zachraňovat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
levnými	levný	k2eAgInPc7d1	levný
čínskými	čínský	k2eAgInPc7d1	čínský
autobusy	autobus	k1gInPc7	autobus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provinciích	provincie	k1gFnPc6	provincie
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
stále	stále	k6eAd1	stále
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
lidový	lidový	k2eAgInSc4d1	lidový
název	název	k1gInSc4	název
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc7d1	poslední
zemí	zem	k1gFnSc7	zem
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Karibiku	Karibik	k1gInSc6	Karibik
s	s	k7c7	s
existujícím	existující	k2eAgNnSc7d1	existující
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
železničním	železniční	k2eAgNnSc7d1	železniční
spojením	spojení	k1gNnSc7	spojení
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc1d1	další
linka	linka	k1gFnSc1	linka
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
u	u	k7c2	u
Barranca	Barranc	k1gInSc2	Barranc
del	del	k?	del
Cobre	Cobr	k1gMnSc5	Cobr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
určena	určit	k5eAaPmNgFnS	určit
primárně	primárně	k6eAd1	primárně
turistům	turist	k1gMnPc3	turist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
ani	ani	k8xC	ani
vlaky	vlak	k1gInPc1	vlak
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
trpí	trpět	k5eAaImIp3nS	trpět
značnou	značný	k2eAgFnSc7d1	značná
poruchovostí	poruchovost	k1gFnSc7	poruchovost
a	a	k8xC	a
velkými	velký	k2eAgNnPc7d1	velké
zpožděními	zpoždění	k1gNnPc7	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
základní	základní	k2eAgFnSc1d1	základní
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
péče	péče	k1gFnSc1	péče
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
místní	místní	k2eAgMnPc4d1	místní
občany	občan	k1gMnPc4	občan
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nedostupný	dostupný	k2eNgInSc4d1	nedostupný
materiál	materiál	k1gInSc4	materiál
či	či	k8xC	či
léky	lék	k1gInPc4	lék
musí	muset	k5eAaImIp3nP	muset
pacienti	pacient	k1gMnPc1	pacient
shánět	shánět	k5eAaImF	shánět
sami	sám	k3xTgMnPc1	sám
a	a	k8xC	a
úplatky	úplatek	k1gInPc7	úplatek
patří	patřit	k5eAaImIp3nP	patřit
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
k	k	k7c3	k
běžné	běžný	k2eAgFnSc3d1	běžná
praxi	praxe	k1gFnSc3	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
výši	výše	k1gFnSc6	výše
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
péče	péče	k1gFnSc1	péče
je	být	k5eAaImIp3nS	být
celoplošně	celoplošně	k6eAd1	celoplošně
zabezpečena	zabezpečit	k5eAaPmNgFnS	zabezpečit
<g/>
,	,	kIx,	,
úroveň	úroveň	k1gFnSc1	úroveň
však	však	k9	však
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
standardům	standard	k1gInPc3	standard
známým	známý	k2eAgInSc7d1	známý
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnické	zdravotnický	k2eAgFnPc1d1	zdravotnická
služby	služba	k1gFnPc1	služba
jsou	být	k5eAaImIp3nP	být
charakterizovány	charakterizován	k2eAgFnPc1d1	charakterizována
"	"	kIx"	"
<g/>
úplatky	úplatek	k1gInPc7	úplatek
<g/>
,	,	kIx,	,
používáním	používání	k1gNnSc7	používání
primitivních	primitivní	k2eAgMnPc2d1	primitivní
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
pochybnými	pochybný	k2eAgFnPc7d1	pochybná
praktikami	praktika	k1gFnPc7	praktika
<g/>
,	,	kIx,	,
špatnou	špatný	k2eAgFnSc7d1	špatná
výživou	výživa	k1gFnSc7	výživa
<g/>
,	,	kIx,	,
nedostatkem	nedostatek	k1gInSc7	nedostatek
léků	lék	k1gInPc2	lék
a	a	k8xC	a
špínou	špína	k1gFnSc7	špína
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
lékařů	lékař	k1gMnPc2	lékař
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
části	část	k1gFnSc2	část
zapříčiněn	zapříčiněn	k2eAgMnSc1d1	zapříčiněn
velmi	velmi	k6eAd1	velmi
velkou	velký	k2eAgFnSc7d1	velká
externí	externí	k2eAgFnSc7d1	externí
praxí	praxe	k1gFnSc7	praxe
kubánských	kubánský	k2eAgMnPc2d1	kubánský
lékařů	lékař	k1gMnPc2	lékař
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyučují	vyučovat	k5eAaImIp3nP	vyučovat
na	na	k7c6	na
tamních	tamní	k2eAgFnPc6d1	tamní
univerzitách	univerzita	k1gFnPc6	univerzita
a	a	k8xC	a
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgFnPc1d1	značná
investice	investice	k1gFnPc1	investice
do	do	k7c2	do
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
lékařů	lékař	k1gMnPc2	lékař
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nebyly	být	k5eNaImAgInP	být
vyrovnávány	vyrovnáván	k2eAgInPc1d1	vyrovnáván
obdobnými	obdobný	k2eAgFnPc7d1	obdobná
investicemi	investice	k1gFnPc7	investice
do	do	k7c2	do
nemocnic	nemocnice	k1gFnPc2	nemocnice
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
lékařů	lékař	k1gMnPc2	lékař
známá	známý	k2eAgFnSc1d1	známá
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
RVHP	RVHP	kA	RVHP
již	již	k6eAd1	již
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
s	s	k7c7	s
ukončením	ukončení	k1gNnSc7	ukončení
podpory	podpora	k1gFnSc2	podpora
Kuby	Kuba	k1gFnSc2	Kuba
zeměmi	zem	k1gFnPc7	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
musí	muset	k5eAaImIp3nP	muset
dokupovat	dokupovat	k5eAaImF	dokupovat
i	i	k9	i
řadu	řada	k1gFnSc4	řada
lékařského	lékařský	k2eAgInSc2d1	lékařský
materiálu	materiál	k1gInSc2	materiál
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vybavení	vybavení	k1gNnSc1	vybavení
nemocnic	nemocnice	k1gFnPc2	nemocnice
pro	pro	k7c4	pro
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
svou	svůj	k3xOyFgFnSc7	svůj
zanedbaností	zanedbanost	k1gFnSc7	zanedbanost
<g/>
.	.	kIx.	.
atp.	atp.	kA	atp.
Hygienické	hygienický	k2eAgFnPc4d1	hygienická
potřeby	potřeba	k1gFnPc4	potřeba
(	(	kIx(	(
<g/>
zubní	zubní	k2eAgFnPc1d1	zubní
pasty	pasta	k1gFnPc1	pasta
<g/>
,	,	kIx,	,
mýdla	mýdlo	k1gNnPc1	mýdlo
<g/>
,	,	kIx,	,
vložky	vložka	k1gFnPc1	vložka
<g/>
,	,	kIx,	,
tampóny	tampón	k1gInPc1	tampón
<g/>
)	)	kIx)	)
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
běžné	běžný	k2eAgMnPc4d1	běžný
Kubánce	Kubánec	k1gMnPc4	Kubánec
zcela	zcela	k6eAd1	zcela
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
horší	zlý	k2eAgFnSc7d2	horší
verzí	verze	k1gFnSc7	verze
někdejších	někdejší	k2eAgInPc2d1	někdejší
československých	československý	k2eAgInPc2d1	československý
tzv.	tzv.	kA	tzv.
front	front	k1gInSc1	front
na	na	k7c4	na
toaletní	toaletní	k2eAgInSc4d1	toaletní
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
dětských	dětský	k2eAgFnPc2d1	dětská
úmrtností	úmrtnost	k1gFnPc2	úmrtnost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
dáno	dát	k5eAaPmNgNnS	dát
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc7d1	aktivní
potratovou	potratový	k2eAgFnSc7d1	potratová
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jakkoli	jakkoli	k8xS	jakkoli
potenciálně	potenciálně	k6eAd1	potenciálně
problematický	problematický	k2eAgInSc1d1	problematický
plod	plod	k1gInSc1	plod
podléhá	podléhat	k5eAaImIp3nS	podléhat
interrupci	interrupce	k1gFnSc4	interrupce
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnickému	zdravotnický	k2eAgInSc3d1	zdravotnický
systému	systém	k1gInSc3	systém
prý	prý	k9	prý
často	často	k6eAd1	často
nedůvěřují	důvěřovat	k5eNaImIp3nP	důvěřovat
i	i	k9	i
vlastní	vlastní	k2eAgFnPc1d1	vlastní
politické	politický	k2eAgFnPc1d1	politická
špičky	špička	k1gFnPc1	špička
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nechávají	nechávat	k5eAaImIp3nP	nechávat
operovat	operovat	k5eAaImF	operovat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
i	i	k8xC	i
život	život	k1gInSc4	život
samotného	samotný	k2eAgMnSc2d1	samotný
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
byt	byt	k1gInSc1	byt
léčen	léčen	k2eAgInSc1d1	léčen
španělskými	španělský	k2eAgMnPc7d1	španělský
lékaři	lékař	k1gMnPc7	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
upadli	upadnout	k5eAaPmAgMnP	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
nedostává	dostávat	k5eNaImIp3nS	dostávat
ani	ani	k8xC	ani
elementární	elementární	k2eAgFnSc2d1	elementární
lékařské	lékařský	k2eAgFnSc2d1	lékařská
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
I	i	k9	i
proto	proto	k8xC	proto
patří	patřit	k5eAaImIp3nS	patřit
základní	základní	k2eAgInPc4d1	základní
léky	lék	k1gInPc4	lék
mezi	mezi	k7c4	mezi
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
některé	některý	k3yIgFnPc4	některý
nadace	nadace	k1gFnPc1	nadace
systematicky	systematicky	k6eAd1	systematicky
distribuují	distribuovat	k5eAaBmIp3nP	distribuovat
disidentům	disident	k1gMnPc3	disident
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
lékařů	lékař	k1gMnPc2	lékař
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
nedostatek	nedostatek	k1gInSc4	nedostatek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
režim	režim	k1gInSc1	režim
je	on	k3xPp3gNnSc4	on
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
politickým	politický	k2eAgInPc3d1	politický
i	i	k8xC	i
finančním	finanční	k2eAgInPc3d1	finanční
cílům	cíl	k1gInPc3	cíl
a	a	k8xC	a
"	"	kIx"	"
<g/>
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
vybraných	vybraný	k2eAgFnPc2d1	vybraná
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
či	či	k8xC	či
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
léčit	léčit	k5eAaImF	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
více	hodně	k6eAd2	hodně
než	než	k8xS	než
37	[number]	k4	37
000	[number]	k4	000
kubánských	kubánský	k2eAgMnPc2d1	kubánský
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
je	on	k3xPp3gFnPc4	on
práce	práce	k1gFnPc4	práce
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
vítanou	vítaný	k2eAgFnSc7d1	vítaná
možností	možnost	k1gFnSc7	možnost
výdělku	výdělek	k1gInSc2	výdělek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vystudovali	vystudovat	k5eAaPmAgMnP	vystudovat
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
a	a	k8xC	a
v	v	k7c6	v
Santigo	Santigo	k6eAd1	Santigo
de	de	k?	de
Cuba	Cuba	k1gMnSc1	Cuba
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lékaři	lékař	k1gMnPc1	lékař
však	však	k9	však
chybí	chybit	k5eAaPmIp3nP	chybit
v	v	k7c6	v
kubánských	kubánský	k2eAgFnPc6d1	kubánská
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
existují	existovat	k5eAaImIp3nP	existovat
ústavy	ústav	k1gInPc1	ústav
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
odešla	odejít	k5eAaPmAgFnS	odejít
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
lékařů	lékař	k1gMnPc2	lékař
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Vývoz	vývoz	k1gInSc1	vývoz
také	také	k9	také
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
konflikty	konflikt	k1gInPc4	konflikt
s	s	k7c7	s
lékařskými	lékařský	k2eAgFnPc7d1	lékařská
organizacemi	organizace	k1gFnPc7	organizace
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odmítají	odmítat	k5eAaImIp3nP	odmítat
řídit	řídit	k5eAaImF	řídit
místně	místně	k6eAd1	místně
platnými	platný	k2eAgFnPc7d1	platná
normami	norma	k1gFnPc7	norma
<g/>
.	.	kIx.	.
</s>
<s>
Skutečností	skutečnost	k1gFnSc7	skutečnost
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
útěky	útěk	k1gInPc1	útěk
těchto	tento	k3xDgMnPc2	tento
vyslaných	vyslaný	k2eAgMnPc2d1	vyslaný
lékařů	lékař	k1gMnPc2	lékař
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
třetích	třetí	k4xOgFnPc2	třetí
zemí	zem	k1gFnPc2	zem
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
především	především	k9	především
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
odešlo	odejít	k5eAaPmAgNnS	odejít
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1574	[number]	k4	1574
kubánských	kubánský	k2eAgMnPc2d1	kubánský
lékařů	lékař	k1gMnPc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
rodiny	rodina	k1gFnPc1	rodina
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
za	za	k7c4	za
emigraci	emigrace	k1gFnSc4	emigrace
trestány	trestán	k2eAgFnPc1d1	trestána
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
propouštění	propouštění	k1gNnSc2	propouštění
rodinných	rodinný	k2eAgMnPc2d1	rodinný
příslušníků	příslušník	k1gMnPc2	příslušník
ze	z	k7c2	z
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rodiny	rodina	k1gFnPc4	rodina
donutili	donutit	k5eAaPmAgMnP	donutit
zaplatit	zaplatit	k5eAaPmF	zaplatit
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
vzdělání	vzdělání	k1gNnSc4	vzdělání
svých	svůj	k3xOyFgMnPc2	svůj
příslušníků	příslušník	k1gMnPc2	příslušník
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
jsou	být	k5eAaImIp3nP	být
prý	prý	k9	prý
zabavovány	zabavován	k2eAgFnPc4d1	zabavována
nemovitosti	nemovitost	k1gFnPc4	nemovitost
emigrantů	emigrant	k1gMnPc2	emigrant
atp.	atp.	kA	atp.
Dle	dle	k7c2	dle
deníků	deník	k1gInPc2	deník
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
lékaři	lékař	k1gMnPc1	lékař
snažící	snažící	k2eAgMnPc1d1	snažící
o	o	k7c4	o
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
aktivity	aktivita	k1gFnPc4	aktivita
vězněni	vězněn	k2eAgMnPc1d1	vězněn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
existují	existovat	k5eAaImIp3nP	existovat
výběrové	výběrový	k2eAgFnPc1d1	výběrová
nemocnice	nemocnice	k1gFnPc1	nemocnice
pro	pro	k7c4	pro
politiky	politik	k1gMnPc4	politik
a	a	k8xC	a
cizince	cizinec	k1gMnSc4	cizinec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
klinika	klinika	k1gFnSc1	klinika
Cira	Cira	k1gFnSc1	Cira
García	García	k1gFnSc1	García
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgInPc2	který
má	mít	k5eAaImIp3nS	mít
běžné	běžný	k2eAgNnSc4d1	běžné
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
zakázán	zakázán	k2eAgInSc1d1	zakázán
přístup	přístup	k1gInSc1	přístup
a	a	k8xC	a
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
tzv.	tzv.	kA	tzv.
zdravotním	zdravotní	k2eAgMnSc6d1	zdravotní
turistům	turist	k1gMnPc3	turist
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
se	se	k3xPyFc4	se
tuto	tento	k3xDgFnSc4	tento
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
turistiku	turistika	k1gFnSc4	turistika
pro	pro	k7c4	pro
cizí	cizí	k2eAgMnPc4d1	cizí
občany	občan	k1gMnPc4	občan
snaží	snažit	k5eAaImIp3nP	snažit
vehementně	vehementně	k6eAd1	vehementně
podporovat	podporovat	k5eAaImF	podporovat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
domovině	domovina	k1gFnSc6	domovina
nemají	mít	k5eNaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navíc	navíc	k6eAd1	navíc
panuje	panovat	k5eAaImIp3nS	panovat
široký	široký	k2eAgInSc4d1	široký
nedostatek	nedostatek	k1gInSc4	nedostatek
i	i	k8xC	i
základních	základní	k2eAgInPc2d1	základní
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
valutové	valutový	k2eAgFnPc1d1	valutová
lékárny	lékárna	k1gFnPc1	lékárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
za	za	k7c4	za
ceny	cena	k1gFnPc4	cena
pro	pro	k7c4	pro
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
nepřístupné	přístupný	k2eNgInPc1d1	nepřístupný
prodávají	prodávat	k5eAaImIp3nP	prodávat
léky	lék	k1gInPc1	lék
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
na	na	k7c6	na
běžném	běžný	k2eAgInSc6d1	běžný
trhu	trh	k1gInSc6	trh
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
tohoto	tento	k3xDgNnSc2	tento
dělení	dělení	k1gNnSc2	dělení
na	na	k7c4	na
"	"	kIx"	"
<g/>
bohaté	bohatý	k2eAgMnPc4d1	bohatý
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
chudé	chudý	k2eAgMnPc4d1	chudý
<g/>
"	"	kIx"	"
pacienty	pacient	k1gMnPc7	pacient
jsou	být	k5eAaImIp3nP	být
zatýkáni	zatýkán	k2eAgMnPc1d1	zatýkán
a	a	k8xC	a
vězněni	vězněn	k2eAgMnPc1d1	vězněn
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
jsou	být	k5eAaImIp3nP	být
uvězňováni	uvězňován	k2eAgMnPc1d1	uvězňován
či	či	k8xC	či
souzeni	souzen	k2eAgMnPc1d1	souzen
i	i	k8xC	i
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
mapující	mapující	k2eAgNnSc1d1	mapující
znečištění	znečištění	k1gNnSc1	znečištění
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
atp.	atp.	kA	atp.
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
Kuba	Kuba	k1gFnSc1	Kuba
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
celosvětově	celosvětově	k6eAd1	celosvětově
organizací	organizace	k1gFnSc7	organizace
OSN	OSN	kA	OSN
za	za	k7c4	za
nejčistší	čistý	k2eAgFnSc4d3	nejčistší
zemi	zem	k1gFnSc4	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
jsou	být	k5eAaImIp3nP	být
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
návštěvníci	návštěvník	k1gMnPc1	návštěvník
povinni	povinen	k2eAgMnPc1d1	povinen
uzavřít	uzavřít	k5eAaPmF	uzavřít
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
země	zem	k1gFnSc2	zem
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pobytu	pobyt	k1gInSc2	pobyt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
rychlá	rychlý	k2eAgFnSc1d1	rychlá
a	a	k8xC	a
odborná	odborný	k2eAgFnSc1d1	odborná
péče	péče	k1gFnSc1	péče
<g/>
.	.	kIx.	.
</s>
<s>
Kubánské	kubánský	k2eAgFnPc1d1	kubánská
lékárny	lékárna	k1gFnPc1	lékárna
na	na	k7c6	na
malých	malý	k2eAgNnPc6d1	malé
městech	město	k1gNnPc6	město
mnohdy	mnohdy	k6eAd1	mnohdy
připomínají	připomínat	k5eAaImIp3nP	připomínat
kiosky	kiosek	k1gInPc1	kiosek
s	s	k7c7	s
novinami	novina	k1gFnPc7	novina
a	a	k8xC	a
cigaretami	cigareta	k1gFnPc7	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
několika	několik	k4yIc6	několik
desetiletích	desetiletí	k1gNnPc6	desetiletí
objevila	objevit	k5eAaPmAgFnS	objevit
cholera	cholera	k1gFnSc1	cholera
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
případy	případ	k1gInPc1	případ
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
ve	v	k7c6	v
východokubánské	východokubánský	k2eAgFnSc6d1	východokubánský
provincii	provincie	k1gFnSc6	provincie
Granma	Granmum	k1gNnSc2	Granmum
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
režim	režim	k1gInSc1	režim
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
však	však	k9	však
nákazu	nákaza	k1gFnSc4	nákaza
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
vymýcenou	vymýcený	k2eAgFnSc4d1	vymýcená
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
zprávy	zpráva	k1gFnPc1	zpráva
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Calixta	Calixta	k1gFnSc1	Calixta
Martíneze	Martíneze	k1gFnSc1	Martíneze
<g/>
,	,	kIx,	,
prokazovaly	prokazovat	k5eAaImAgFnP	prokazovat
opak	opak	k1gInSc4	opak
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
více	hodně	k6eAd2	hodně
případů	případ	k1gInPc2	případ
také	také	k9	také
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Úřady	úřad	k1gInPc1	úřad
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
51	[number]	k4	51
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
až	až	k9	až
o	o	k7c6	o
stovkách	stovka	k1gFnPc6	stovka
nakažených	nakažený	k2eAgMnPc2d1	nakažený
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
jednom	jeden	k4xCgNnSc6	jeden
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
většině	většina	k1gFnSc6	většina
zcela	zcela	k6eAd1	zcela
gramotné	gramotný	k2eAgFnSc6d1	gramotná
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
umí	umět	k5eAaImIp3nS	umět
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
psát	psát	k5eAaImF	psát
a	a	k8xC	a
počítat	počítat	k5eAaImF	počítat
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
zemí	zem	k1gFnPc2	zem
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Alfabetizace	Alfabetizace	k1gFnSc1	Alfabetizace
však	však	k9	však
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
již	již	k6eAd1	již
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
masové	masový	k2eAgFnSc2d1	masová
školící	školící	k2eAgFnSc2d1	školící
kampaně	kampaň	k1gFnSc2	kampaň
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
a	a	k8xC	a
za	za	k7c4	za
Batisty	batist	k1gInPc4	batist
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
téměř	téměř	k6eAd1	téměř
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vzdělání	vzdělání	k1gNnSc2	vzdělání
je	být	k5eAaImIp3nS	být
však	však	k9	však
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc1	škola
trpí	trpět	k5eAaImIp3nP	trpět
horší	zlý	k2eAgFnPc1d2	horší
vybaveností	vybavenost	k1gFnSc7	vybavenost
a	a	k8xC	a
výuka	výuka	k1gFnSc1	výuka
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
ovládána	ovládat	k5eAaImNgFnS	ovládat
propagandou	propaganda	k1gFnSc7	propaganda
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Studentům	student	k1gMnPc3	student
je	být	k5eAaImIp3nS	být
znemožněn	znemožněn	k2eAgInSc1d1	znemožněn
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
odborné	odborný	k2eAgFnSc3d1	odborná
literatuře	literatura	k1gFnSc3	literatura
i	i	k8xC	i
beletrii	beletrie	k1gFnSc3	beletrie
i	i	k8xC	i
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
světovým	světový	k2eAgInPc3d1	světový
názorům	názor	k1gInPc3	názor
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
do	do	k7c2	do
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
ideologicky	ideologicky	k6eAd1	ideologicky
organizovaných	organizovaný	k2eAgFnPc2d1	organizovaná
mládežnických	mládežnický	k2eAgFnPc2d1	mládežnická
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
další	další	k2eAgNnSc1d1	další
uplatnění	uplatnění	k1gNnSc1	uplatnění
je	být	k5eAaImIp3nS	být
politicky	politicky	k6eAd1	politicky
podmiňováno	podmiňován	k2eAgNnSc1d1	podmiňováno
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
je	být	k5eAaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
výuka	výuka	k1gFnSc1	výuka
marx-leninismu	marxeninismus	k1gInSc2	marx-leninismus
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Internet	Internet	k1gInSc1	Internet
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
je	být	k5eAaImIp3nS	být
cenzurován	cenzurován	k2eAgMnSc1d1	cenzurován
<g/>
,	,	kIx,	,
vybrané	vybraný	k2eAgFnPc1d1	vybraná
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
především	především	k9	především
s	s	k7c7	s
kubánskými	kubánský	k2eAgNnPc7d1	kubánské
tématy	téma	k1gNnPc7	téma
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
nelze	lze	k6eNd1	lze
otevřít	otevřít	k5eAaPmF	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
E-maily	eail	k1gInPc1	e-mail
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
kontrolovány	kontrolován	k2eAgMnPc4d1	kontrolován
<g/>
.	.	kIx.	.
</s>
<s>
Běžnému	běžný	k2eAgNnSc3d1	běžné
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
není	být	k5eNaImIp3nS	být
internet	internet	k1gInSc1	internet
poskytován	poskytovat	k5eAaImNgInS	poskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
připojení	připojení	k1gNnSc2	připojení
při	při	k7c6	při
256	[number]	k4	256
kb	kb	kA	kb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejhorším	zlý	k2eAgNnPc3d3	nejhorší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
Internetu	Internet	k1gInSc3	Internet
je	být	k5eAaImIp3nS	být
organizován	organizovat	k5eAaBmNgInS	organizovat
a	a	k8xC	a
striktně	striktně	k6eAd1	striktně
kontrolován	kontrolovat	k5eAaImNgInS	kontrolovat
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
ho	on	k3xPp3gNnSc4	on
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
především	především	k9	především
prominenti	prominent	k1gMnPc1	prominent
režimu	režim	k1gInSc2	režim
<g/>
:	:	kIx,	:
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
pracovníci	pracovník	k1gMnPc1	pracovník
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
rodiny	rodina	k1gFnSc2	rodina
či	či	k8xC	či
státem	stát	k1gInSc7	stát
protežovaní	protežovaný	k2eAgMnPc1d1	protežovaný
umělci	umělec	k1gMnPc1	umělec
či	či	k8xC	či
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
již	již	k9	již
navykli	navyknout	k5eAaPmAgMnP	navyknout
využívat	využívat	k5eAaPmF	využívat
Facebook	Facebook	k1gInSc4	Facebook
i	i	k8xC	i
jiné	jiný	k2eAgFnPc4d1	jiná
sociální	sociální	k2eAgFnPc4d1	sociální
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc4	Internet
mívají	mívat	k5eAaImIp3nP	mívat
omezeně	omezeně	k6eAd1	omezeně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
státem	stát	k1gInSc7	stát
zřizovaných	zřizovaný	k2eAgFnPc2d1	zřizovaná
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
bývá	bývat	k5eAaImIp3nS	bývat
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
hlavně	hlavně	k9	hlavně
e-mail	eail	k1gInSc1	e-mail
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
přístup	přístup	k1gInSc1	přístup
na	na	k7c4	na
internet	internet	k1gInSc4	internet
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nekvalitní	kvalitní	k2eNgInSc4d1	nekvalitní
<g/>
,	,	kIx,	,
omezovaný	omezovaný	k2eAgInSc4d1	omezovaný
a	a	k8xC	a
proto	proto	k8xC	proto
málo	málo	k6eAd1	málo
používaný	používaný	k2eAgInSc1d1	používaný
<g/>
.	.	kIx.	.
</s>
<s>
Objemy	objem	k1gInPc1	objem
dat	datum	k1gNnPc2	datum
potřebné	potřebný	k2eAgInPc1d1	potřebný
k	k	k7c3	k
využívání	využívání	k1gNnSc3	využívání
např.	např.	kA	např.
Facebooku	Facebook	k1gInSc2	Facebook
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
sítí	síť	k1gFnPc2	síť
zatím	zatím	k6eAd1	zatím
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
jejich	jejich	k3xOp3gNnSc4	jejich
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
tak	tak	k6eAd1	tak
nekvalitním	kvalitní	k2eNgNnSc7d1	nekvalitní
připojením	připojení	k1gNnSc7	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
i	i	k9	i
ilegální	ilegální	k2eAgNnSc1d1	ilegální
používání	používání	k1gNnSc1	používání
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezi	mezi	k7c4	mezi
disidenty	disident	k1gMnPc4	disident
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
skupina	skupina	k1gFnSc1	skupina
blogerů	bloger	k1gInPc2	bloger
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slavná	slavný	k2eAgFnSc1d1	slavná
Yoani	Yoaen	k2eAgMnPc1d1	Yoaen
Sánchez	Sáncheza	k1gFnPc2	Sáncheza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pomocí	pomocí	k7c2	pomocí
internetu	internet	k1gInSc2	internet
obcházejí	obcházet	k5eAaImIp3nP	obcházet
informační	informační	k2eAgInSc4d1	informační
monopol	monopol	k1gInSc4	monopol
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
blogy	blog	k1gInPc1	blog
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
blokovány	blokován	k2eAgFnPc1d1	blokována
státem	stát	k1gInSc7	stát
a	a	k8xC	a
nejdou	jít	k5eNaImIp3nP	jít
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
otevřít	otevřít	k5eAaPmF	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Vystavování	vystavování	k1gNnSc1	vystavování
článků	článek	k1gInPc2	článek
na	na	k7c4	na
internet	internet	k1gInSc4	internet
probíhá	probíhat	k5eAaImIp3nS	probíhat
často	často	k6eAd1	často
přes	přes	k7c4	přes
zahraničí	zahraničí	k1gNnSc4	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Donedávna	donedávna	k6eAd1	donedávna
platil	platit	k5eAaImAgInS	platit
i	i	k9	i
zákaz	zákaz	k1gInSc1	zákaz
nákupu	nákup	k1gInSc2	nákup
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
mobilů	mobil	k1gInPc2	mobil
soukromými	soukromý	k2eAgFnPc7d1	soukromá
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
však	však	k9	však
novou	nový	k2eAgFnSc7d1	nová
vládou	vláda	k1gFnSc7	vláda
Raúla	Raúl	k1gMnSc2	Raúl
Castra	Castrum	k1gNnSc2	Castrum
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
běžného	běžný	k2eAgInSc2d1	běžný
počítače	počítač	k1gInSc2	počítač
však	však	k9	však
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
ročního	roční	k2eAgInSc2d1	roční
platu	plat	k1gInSc2	plat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
dramatickými	dramatický	k2eAgFnPc7d1	dramatická
rozšířením	rozšíření	k1gNnSc7	rozšíření
počítačů	počítač	k1gInPc2	počítač
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
i	i	k8xC	i
mezi	mezi	k7c7	mezi
běžnými	běžný	k2eAgMnPc7d1	běžný
obyvateli	obyvatel	k1gMnPc7	obyvatel
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
bývá	bývat	k5eAaImIp3nS	bývat
internet	internet	k1gInSc1	internet
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
hotelech	hotel	k1gInPc6	hotel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
nutno	nutno	k6eAd1	nutno
si	se	k3xPyFc3	se
zakoupit	zakoupit	k5eAaPmF	zakoupit
internetovou	internetový	k2eAgFnSc4d1	internetová
kartu	karta	k1gFnSc4	karta
(	(	kIx(	(
<g/>
cca	cca	kA	cca
10	[number]	k4	10
<g/>
CUC	CUC	kA	CUC
<g/>
/	/	kIx~	/
<g/>
hodinu	hodina	k1gFnSc4	hodina
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
1	[number]	k4	1
CUC	CUC	kA	CUC
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgFnSc1d1	mobilní
síť	síť	k1gFnSc1	síť
GSM	GSM	kA	GSM
(	(	kIx(	(
<g/>
Cubacel	Cubacel	k1gMnSc1	Cubacel
<g/>
/	/	kIx~	/
<g/>
368	[number]	k4	368
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
dostupná	dostupný	k2eAgFnSc1d1	dostupná
mj.	mj.	kA	mj.
v	v	k7c6	v
krajích	kraj	k1gInPc6	kraj
La	la	k1gNnSc2	la
Habana	Haban	k1gMnSc4	Haban
<g/>
,	,	kIx,	,
Mayabeque	Mayabequ	k1gMnSc4	Mayabequ
<g/>
,	,	kIx,	,
Matanzas	Matanzas	k1gMnSc1	Matanzas
<g/>
,	,	kIx,	,
Cienfuegos	Cienfuegos	k1gMnSc1	Cienfuegos
<g/>
,	,	kIx,	,
Sancti	Sanct	k1gMnPc1	Sanct
Spíritus	Spíritus	k1gMnSc1	Spíritus
a	a	k8xC	a
Villa	Villa	k1gMnSc1	Villa
Clara	Clara	k1gFnSc1	Clara
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
hovorů	hovor	k1gInPc2	hovor
pro	pro	k7c4	pro
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nadále	nadále	k6eAd1	nadále
vysoké	vysoký	k2eAgNnSc1d1	vysoké
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
ostrova	ostrov	k1gInSc2	ostrov
stojí	stát	k5eAaImIp3nS	stát
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
jedna	jeden	k4xCgFnSc1	jeden
SMS	SMS	kA	SMS
do	do	k7c2	do
ČR	ČR	kA	ČR
cca	cca	kA	cca
15	[number]	k4	15
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
minuta	minuta	k1gFnSc1	minuta
odchozího	odchozí	k2eAgInSc2d1	odchozí
hovoru	hovor	k1gInSc2	hovor
pak	pak	k6eAd1	pak
cca	cca	kA	cca
60	[number]	k4	60
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
roamingová	roamingový	k2eAgFnSc1d1	roamingová
zóna	zóna	k1gFnSc1	zóna
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
Rukojmí	rukojmí	k1gNnSc1	rukojmí
v	v	k7c4	v
Bella	Bell	k1gMnSc4	Bell
Vista	vista	k2eAgInSc2d1	vista
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
volně	volně	k6eAd1	volně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
stejně	stejně	k6eAd1	stejně
pojmenovaný	pojmenovaný	k2eAgInSc4d1	pojmenovaný
díl	díl	k1gInSc4	díl
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
30	[number]	k4	30
případů	případ	k1gInPc2	případ
majora	major	k1gMnSc2	major
Zemana	Zeman	k1gMnSc2	Zeman
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Los	los	k1gInSc1	los
Jazmines	Jazmines	k1gInSc1	Jazmines
nad	nad	k7c7	nad
oblastí	oblast	k1gFnSc7	oblast
Viñ	Viñ	k1gFnSc2	Viñ
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Pinar	Pinar	k1gMnSc1	Pinar
del	del	k?	del
Río	Río	k1gMnSc1	Río
<g/>
.	.	kIx.	.
</s>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
téměř	téměř	k6eAd1	téměř
20	[number]	k4	20
let	léto	k1gNnPc2	léto
a	a	k8xC	a
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
téma	téma	k1gNnSc1	téma
jeho	on	k3xPp3gInSc2	on
nobelovského	nobelovský	k2eAgInSc2d1	nobelovský
románu	román	k1gInSc2	román
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
Finca	Fincus	k1gMnSc2	Fincus
Vigía	Vigíus	k1gMnSc2	Vigíus
nedaleko	nedaleko	k7c2	nedaleko
Havany	Havana	k1gFnSc2	Havana
<g/>
.	.	kIx.	.
</s>
<s>
Kubánci	Kubánec	k1gMnPc1	Kubánec
nesmí	smět	k5eNaImIp3nP	smět
soukromě	soukromě	k6eAd1	soukromě
prodávat	prodávat	k5eAaImF	prodávat
státem	stát	k1gInSc7	stát
kontrolované	kontrolovaný	k2eAgInPc4d1	kontrolovaný
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Rybáři	rybář	k1gMnPc1	rybář
nesmí	smět	k5eNaImIp3nP	smět
soukromě	soukromě	k6eAd1	soukromě
prodávat	prodávat	k5eAaImF	prodávat
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
ulovené	ulovený	k2eAgFnPc1d1	ulovená
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
mořské	mořský	k2eAgInPc1d1	mořský
plody	plod	k1gInPc1	plod
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
zákaz	zákaz	k1gInSc1	zákaz
platí	platit	k5eAaImIp3nS	platit
také	také	k9	také
pro	pro	k7c4	pro
hovězí	hovězí	k1gNnSc4	hovězí
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
krávy	kráva	k1gFnSc2	kráva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
pro	pro	k7c4	pro
vepřové	vepřové	k1gNnSc4	vepřové
či	či	k8xC	či
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
montují	montovat	k5eAaImIp3nP	montovat
české	český	k2eAgInPc1d1	český
motocykly	motocykl	k1gInPc1	motocykl
Jawa	jawa	k1gFnSc1	jawa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
zemřel	zemřít	k5eAaPmAgMnS	zemřít
český	český	k2eAgMnSc1d1	český
podnikatel	podnikatel	k1gMnSc1	podnikatel
Jindřich	Jindřich	k1gMnSc1	Jindřich
Waldes	Waldes	k1gMnSc1	Waldes
<g/>
,	,	kIx,	,
prchající	prchající	k2eAgFnSc1d1	prchající
z	z	k7c2	z
protektorátu	protektorát	k1gInSc2	protektorát
před	před	k7c7	před
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
považuje	považovat	k5eAaImIp3nS	považovat
Kuba	Kuba	k1gMnSc1	Kuba
americkou	americký	k2eAgFnSc4d1	americká
přítomnost	přítomnost	k1gFnSc4	přítomnost
na	na	k7c6	na
vojenské	vojenský	k2eAgFnSc6d1	vojenská
základně	základna	k1gFnSc6	základna
Guantánamo	Guantánama	k1gFnSc5	Guantánama
za	za	k7c4	za
ilegální	ilegální	k2eAgFnSc4d1	ilegální
okupaci	okupace	k1gFnSc4	okupace
kubánského	kubánský	k2eAgNnSc2d1	kubánské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
zbudovaly	zbudovat	k5eAaPmAgInP	zbudovat
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
věznici	věznice	k1gFnSc6	věznice
Guantánamo	Guantánama	k1gFnSc5	Guantánama
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
militantní	militantní	k2eAgMnPc4d1	militantní
bojovníky	bojovník	k1gMnPc4	bojovník
<g/>
"	"	kIx"	"
zadržené	zadržený	k2eAgFnPc4d1	zadržená
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
a	a	k8xC	a
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zakázány	zakázán	k2eAgFnPc1d1	zakázána
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
obnoveny	obnoven	k2eAgFnPc1d1	obnovena
byly	být	k5eAaImAgFnP	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Kubánský	kubánský	k2eAgInSc1d1	kubánský
mramor	mramor	k1gInSc1	mramor
byl	být	k5eAaImAgInS	být
dovážen	dovážit	k5eAaPmNgInS	dovážit
do	do	k7c2	do
ČSSR	ČSSR	kA	ČSSR
<g/>
:	:	kIx,	:
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
budovách	budova	k1gFnPc6	budova
ze	z	k7c2	z
70	[number]	k4	70
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
nádražích	nádraží	k1gNnPc6	nádraží
<g/>
,	,	kIx,	,
centrálách	centrála	k1gFnPc6	centrála
KSČ	KSČ	kA	KSČ
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
najdete	najít	k5eAaPmIp2nP	najít
často	často	k6eAd1	často
obložení	obložení	k1gNnPc1	obložení
z	z	k7c2	z
růžového	růžový	k2eAgInSc2d1	růžový
či	či	k8xC	či
žlutavého	žlutavý	k2eAgInSc2d1	žlutavý
kubánského	kubánský	k2eAgInSc2d1	kubánský
mramoru	mramor	k1gInSc2	mramor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
kubánský	kubánský	k2eAgMnSc1d1	kubánský
prezident	prezident	k1gMnSc1	prezident
označen	označit	k5eAaPmNgInS	označit
americkým	americký	k2eAgInSc7d1	americký
časopisem	časopis	k1gInSc7	časopis
Forbes	forbes	k1gInSc1	forbes
jako	jako	k8xS	jako
7	[number]	k4	7
<g/>
.	.	kIx.	.
nejbohatší	bohatý	k2eAgFnSc1d3	nejbohatší
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc1	Castro
toto	tento	k3xDgNnSc4	tento
veřejně	veřejně	k6eAd1	veřejně
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
televizním	televizní	k2eAgInSc6d1	televizní
přenosu	přenos	k1gInSc6	přenos
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
deklaroval	deklarovat	k5eAaBmAgMnS	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mu	on	k3xPp3gMnSc3	on
někdo	někdo	k3yInSc1	někdo
toto	tento	k3xDgNnSc4	tento
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
zavázal	zavázat	k5eAaPmAgMnS	zavázat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
vzdá	vzdát	k5eAaPmIp3nS	vzdát
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
státnických	státnický	k2eAgFnPc2d1	státnická
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgInSc4d1	pamětní
pomník	pomník	k1gInSc4	pomník
vyhlazení	vyhlazení	k1gNnSc2	vyhlazení
českých	český	k2eAgInPc2d1	český
Lidic	Lidice	k1gInPc2	Lidice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Kubánci	Kubánec	k1gMnPc1	Kubánec
instalovali	instalovat	k5eAaBmAgMnP	instalovat
k	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
výročí	výročí	k1gNnSc3	výročí
tohoto	tento	k3xDgInSc2	tento
barbarského	barbarský	k2eAgInSc2d1	barbarský
aktu	akt	k1gInSc2	akt
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Havana	havana	k1gNnSc2	havana
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgMnPc1d1	státní
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
(	(	kIx(	(
<g/>
cca	cca	kA	cca
7	[number]	k4	7
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
neplatí	platit	k5eNaImIp3nP	platit
daně	daň	k1gFnPc1	daň
ze	z	k7c2	z
mzdy	mzda	k1gFnSc2	mzda
<g/>
.	.	kIx.	.
</s>
<s>
KAŠPAR	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Karibské	karibský	k2eAgFnSc2d1	karibská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
557	[number]	k4	557
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
OPATRNÝ	opatrný	k2eAgMnSc1d1	opatrný
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
89	[number]	k4	89
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kuba	kuba	k1gNnSc2	kuba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Kuba	Kuba	k1gFnSc1	Kuba
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Galerie	galerie	k1gFnSc2	galerie
Kuba	Kuba	k1gFnSc1	Kuba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Félix	Félix	k1gInSc4	Félix
Andrés	Andrés	k1gInSc1	Andrés
León	León	k1gNnSc1	León
Carballo	Carballo	k1gNnSc1	Carballo
<g/>
:	:	kIx,	:
Současná	současný	k2eAgFnSc1d1	současná
Kuba	Kuba	k1gFnSc1	Kuba
-	-	kIx~	-
přednáška	přednáška	k1gFnSc1	přednáška
kubánského	kubánský	k2eAgMnSc2d1	kubánský
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Lidská	lidský	k2eAgNnPc1d1	lidské
práva	právo	k1gNnPc1	právo
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
článek	článek	k1gInSc1	článek
"	"	kIx"	"
<g/>
Pravá	pravý	k2eAgFnSc1d1	pravá
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
"	"	kIx"	"
na	na	k7c4	na
idnes	idnes	k1gInSc4	idnes
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
cz	cz	k?	cz
reportáž	reportáž	k1gFnSc1	reportáž
a	a	k8xC	a
fotografie	fotografie	k1gFnSc1	fotografie
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
"	"	kIx"	"
<g/>
50	[number]	k4	50
let	léto	k1gNnPc2	léto
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
"	"	kIx"	"
na	na	k7c6	na
webu	web	k1gInSc6	web
jansochor	jansochora	k1gFnPc2	jansochora
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Kuba	Kuba	k1gFnSc1	Kuba
na	na	k7c6	na
webu	web	k1gInSc6	web
clovekvtisni	clovekvtisnout	k5eAaPmRp2nS	clovekvtisnout
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Videozáznam	videozáznam	k1gInSc1	videozáznam
z	z	k7c2	z
besedy	beseda	k1gFnSc2	beseda
s	s	k7c7	s
někdejším	někdejší	k2eAgMnSc7d1	někdejší
československým	československý	k2eAgMnSc7d1	československý
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Svobodou	Svoboda	k1gMnSc7	Svoboda
Ladislav	Ladislav	k1gMnSc1	Ladislav
Holdoš	Holdoš	k1gMnSc1	Holdoš
<g/>
,	,	kIx,	,
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
ČSSR	ČSSR	kA	ČSSR
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
v	v	k7c6	v
kritických	kritický	k2eAgNnPc6d1	kritické
letech	léto	k1gNnPc6	léto
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
Kuba	Kuba	k1gFnSc1	Kuba
pro	pro	k7c4	pro
cestovatele	cestovatel	k1gMnPc4	cestovatel
s	s	k7c7	s
baťohem	baťohem	k?	baťohem
Cuba	Cuba	k1gFnSc1	Cuba
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cuba	Cuba	k1gFnSc1	Cuba
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Cuba	Cubus	k1gMnSc2	Cubus
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k1gMnSc3	Burea
of	of	k?	of
Western	Western	kA	Western
Hemispehere	Hemispeher	k1gInSc5	Hemispeher
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Cuba	Cuba	k1gFnSc1	Cuba
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Cuba	Cuba	k1gFnSc1	Cuba
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Cuba	Cubum	k1gNnSc2	Cubum
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Kuba	Kuba	k1gFnSc1	Kuba
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
KNIGHT	KNIGHT	kA	KNIGHT
<g/>
,	,	kIx,	,
Franklin	Franklin	k2eAgMnSc1d1	Franklin
W	W	kA	W
<g/>
;	;	kIx,	;
LEVINSON	LEVINSON	kA	LEVINSON
<g/>
,	,	kIx,	,
Sandra	Sandra	k1gFnSc1	Sandra
H.	H.	kA	H.
Cuba	Cuba	k1gFnSc1	Cuba
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
