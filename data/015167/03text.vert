<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
britská	britský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Viktoriekrálovna	Viktoriekrálovna	k1gFnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
<g/>
,	,	kIx,
císařovna	císařovna	k1gFnSc1
Indie	Indie	k1gFnSc1
Královna	královna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
foto	foto	k1gNnSc1
<g/>
:	:	kIx,
Alexander	Alexandra	k1gFnPc2
Bassano	Bassana	k1gFnSc5
<g/>
)	)	kIx)
<g/>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1837	#num#	k4
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
Manžel	manžel	k1gMnSc1
</s>
<s>
Albert	Albert	k1gMnSc1
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
Korunovace	korunovace	k1gFnPc4
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1838	#num#	k4
Narození	narození	k1gNnPc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1819	#num#	k4
Kensingtonský	Kensingtonský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
81	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Osborne	Osborn	k1gInSc5
House	house	k1gNnSc1
<g/>
,	,	kIx,
Wight	Wight	k2eAgMnSc1d1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgInSc1d1
Následník	následník	k1gMnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potomci	potomek	k1gMnPc5
</s>
<s>
ViktorieEduard	ViktorieEduard	k1gMnSc1
AliceAlfrédHelenaLuisaArturLeopoldBeatrice	AliceAlfrédHelenaLuisaArturLeopoldBeatrika	k1gFnSc3
Dynastie	dynastie	k1gFnSc2
</s>
<s>
Hannoverská	hannoverský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
Otec	otec	k1gMnSc1
</s>
<s>
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
a	a	k8xC
Strathearnu	Strathearn	k1gInSc2
Matka	matka	k1gFnSc1
</s>
<s>
princezna	princezna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
Podpis	podpis	k1gInSc4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
jiné	jiný	k2eAgInPc1d1
významy	význam	k1gInPc1
slovního	slovní	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
královna	královna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
článek	článek	k1gInSc4
Královna	královna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1819	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
<g/>
,	,	kIx,
ostrov	ostrov	k1gInSc1
Wight	Wight	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
narozená	narozený	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Alexandrina	Alexandrina	k1gFnSc1
Victoria	Victorium	k1gNnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
královna	královna	k1gFnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1837	#num#	k4
a	a	k8xC
první	první	k4xOgFnSc1
císařovna	císařovna	k1gFnSc1
Indie	Indie	k1gFnSc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1876	#num#	k4
<g/>
,	,	kIx,
obojí	obojí	k4xRgMnPc1
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
její	její	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
trvala	trvat	k5eAaImAgFnS
63	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
7	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
druhé	druhý	k4xOgNnSc4
nejdelší	dlouhý	k2eAgNnSc4d3
období	období	k1gNnSc4
vlády	vláda	k1gFnSc2
britského	britský	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
doba	doba	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
viktoriánské	viktoriánský	k2eAgNnSc1d1
období	období	k1gNnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
charakteristická	charakteristický	k2eAgNnPc4d1
bouřlivým	bouřlivý	k2eAgInSc7d1
průmyslovým	průmyslový	k2eAgInSc7d1
<g/>
,	,	kIx,
politickým	politický	k2eAgInSc7d1
<g/>
,	,	kIx,
vědeckým	vědecký	k2eAgInSc7d1
a	a	k8xC
vojenským	vojenský	k2eAgInSc7d1
rozvojem	rozvoj	k1gInSc7
britských	britský	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současná	současný	k2eAgFnSc1d1
britská	britský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
jediná	jediný	k2eAgFnSc1d1
ji	on	k3xPp3gFnSc4
v	v	k7c6
délce	délka	k1gFnSc6
vlády	vláda	k1gFnSc2
předstihla	předstihnout	k5eAaPmAgFnS
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gMnPc1
<g/>
,	,	kIx,
již	již	k9
zesnulý	zesnulý	k1gMnSc1
<g/>
,	,	kIx,
manžel	manžel	k1gMnSc1
Princ	princ	k1gMnSc1
Philip	Philip	k1gMnSc1
jsou	být	k5eAaImIp3nP
Viktoriinými	Viktoriin	k2eAgNnPc7d1
prapravnoučaty	prapravnouče	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
nastoupila	nastoupit	k5eAaPmAgFnS
na	na	k7c4
trůn	trůn	k1gInSc4
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
Británie	Británie	k1gFnSc1
již	již	k6eAd1
zavedenou	zavedený	k2eAgFnSc7d1
konstituční	konstituční	k2eAgFnSc7d1
monarchií	monarchie	k1gFnSc7
a	a	k8xC
panovník	panovník	k1gMnSc1
uplatňoval	uplatňovat	k5eAaImAgMnS
svůj	svůj	k3xOyFgInSc4
vliv	vliv	k1gInSc4
jen	jen	k9
nepřímo	přímo	k6eNd1
přes	přes	k7c4
doporučení	doporučení	k1gNnSc4
předsedy	předseda	k1gMnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
královna	královna	k1gFnSc1
byla	být	k5eAaImAgFnS
tedy	tedy	k9
zejména	zejména	k9
důležitým	důležitý	k2eAgInSc7d1
jednotícím	jednotící	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
za	za	k7c2
její	její	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
svého	svůj	k3xOyFgInSc2
mocenského	mocenský	k2eAgInSc2d1
vrcholu	vrchol	k1gInSc2
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
vedoucí	vedoucí	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
<g/>
,	,	kIx,
vojenskou	vojenský	k2eAgFnSc7d1
a	a	k8xC
hospodářskou	hospodářský	k2eAgFnSc7d1
silou	síla	k1gFnSc7
tehdejšího	tehdejší	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
pocházela	pocházet	k5eAaImAgFnS
z	z	k7c2
německého	německý	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodila	narodit	k5eAaPmAgNnP
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
jediná	jediný	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
čtvrtého	čtvrtý	k4xOgInSc2
syna	syn	k1gMnSc2
krále	král	k1gMnSc4
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
prince	princ	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
Augusta	August	k1gMnSc2
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
z	z	k7c2
Kentu	Kent	k1gInSc2
a	a	k8xC
Strathearnu	Strathearn	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
princezny	princezna	k1gFnPc4
Viktorie	Viktoria	k1gFnSc2
Sasko-Kobursko-Saalfeldské	Sasko-Kobursko-Saalfeldský	k2eAgFnSc2d1
<g/>
;	;	kIx,
jako	jako	k8xC,k8xS
taková	takový	k3xDgFnSc1
byla	být	k5eAaImAgFnS
vnučkou	vnučka	k1gFnSc7
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
neteří	teřet	k5eNaImIp3nS
svého	svůj	k3xOyFgMnSc2
předchůdce	předchůdce	k1gMnSc2
krále	král	k1gMnSc4
Viléma	Vilém	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
svých	svůj	k3xOyFgNnPc2
devět	devět	k4xCc4
dětí	dítě	k1gFnPc2
a	a	k8xC
42	#num#	k4
vnoučat	vnouče	k1gNnPc2
dohodla	dohodnout	k5eAaPmAgFnS
manželství	manželství	k1gNnSc4
v	v	k7c6
panovnických	panovnický	k2eAgFnPc6d1
rodinách	rodina	k1gFnPc6
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
nazývána	nazývat	k5eAaImNgFnS
evropskou	evropský	k2eAgFnSc7d1
babičkou	babička	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzájemná	vzájemný	k2eAgFnSc1d1
spřízněnost	spřízněnost	k1gFnSc1
nicméně	nicméně	k8xC
nezabránila	zabránit	k5eNaPmAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
později	pozdě	k6eAd2
někteří	některý	k3yIgMnPc1
její	její	k3xOp3gMnPc1
potomci	potomek	k1gMnPc1
proti	proti	k7c3
sobě	se	k3xPyFc3
postavili	postavit	k5eAaPmAgMnP
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
Viktoriiných	Viktoriin	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
(	(	kIx(
<g/>
a	a	k8xC
patrně	patrně	k6eAd1
tedy	tedy	k9
nevědomky	nevědomky	k6eAd1
i	i	k9
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
navíc	navíc	k6eAd1
přenašeči	přenašeč	k1gInSc3
hemofilie	hemofilie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
do	do	k7c2
různých	různý	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
panovnických	panovnický	k2eAgFnPc2d1
dynastií	dynastie	k1gFnPc2
(	(	kIx(
<g/>
nejznámější	známý	k2eAgInSc1d3
je	být	k5eAaImIp3nS
případ	případ	k1gInSc1
ruského	ruský	k2eAgMnSc2d1
careviče	carevič	k1gMnSc2
Alexeje	Alexej	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
byla	být	k5eAaImAgFnS
posledním	poslední	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
hannoverské	hannoverský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
na	na	k7c6
britském	britský	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
–	–	k?
její	její	k3xOp3gInSc4
syn	syn	k1gMnSc1
a	a	k8xC
nástupce	nástupce	k1gMnSc1
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
patřil	patřit	k5eAaImAgMnS
po	po	k7c6
otci	otec	k1gMnSc6
již	již	k6eAd1
do	do	k7c2
dynastie	dynastie	k1gFnSc2
sasko-kobursko-gothajské	sasko-kobursko-gothajský	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vládne	vládnout	k5eAaImIp3nS
Británii	Británie	k1gFnSc3
de	de	k?
facto	facto	k1gNnSc4
dodnes	dodnes	k6eAd1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1917	#num#	k4
však	však	k9
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Windsorská	windsorský	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
nárok	nárok	k1gInSc1
na	na	k7c4
trůn	trůn	k1gInSc4
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	s	k7c7
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1819	#num#	k4
v	v	k7c6
Kensingtonském	Kensingtonský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
jejího	její	k3xOp3gNnSc2
narození	narození	k1gNnSc2
vládl	vládnout	k5eAaImAgInS
ještě	ještě	k6eAd1
(	(	kIx(
<g/>
nominálně	nominálně	k6eAd1
<g/>
)	)	kIx)
její	její	k3xOp3gMnSc1
děd	děd	k1gMnSc1
král	král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gMnPc1
tři	tři	k4xCgMnPc1
starší	starý	k2eAgMnPc1d2
synové	syn	k1gMnPc1
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
Frederik	Frederik	k1gMnSc1
a	a	k8xC
Vilém	Vilém	k1gMnSc1
neměli	mít	k5eNaImAgMnP
žádné	žádný	k3yNgMnPc4
žijící	žijící	k2eAgMnPc4d1
legitimní	legitimní	k2eAgMnPc4d1
potomky	potomek	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktorie	Viktorie	k1gFnSc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jediné	jediný	k2eAgNnSc4d1
legitimní	legitimní	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
králova	králův	k2eAgInSc2d1
čtvrtého	čtvrtý	k4xOgInSc2
syna	syn	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
z	z	k7c2
Kentu	Kent	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
dědičkou	dědička	k1gFnSc7
koruny	koruna	k1gFnSc2
(	(	kIx(
<g/>
korunní	korunní	k2eAgFnSc7d1
princeznou	princezna	k1gFnSc7
<g/>
)	)	kIx)
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
strýce	strýc	k1gMnSc2
krále	král	k1gMnSc4
Jiřího	Jiří	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1830	#num#	k4
a	a	k8xC
nástupu	nástup	k1gInSc2
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
bratra	bratr	k1gMnSc2
Viléma	Vilém	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Pochybnosti	pochybnost	k1gFnPc4
o	o	k7c6
Viktoriině	Viktoriin	k2eAgFnSc6d1
legitimitě	legitimita	k1gFnSc6
</s>
<s>
Záhy	záhy	k6eAd1
se	se	k3xPyFc4
vynořily	vynořit	k5eAaPmAgInP
názory	názor	k1gInPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
biologickým	biologický	k2eAgMnSc7d1
otcem	otec	k1gMnSc7
malé	malý	k2eAgFnSc2d1
Viktorie	Viktoria	k1gFnSc2
není	být	k5eNaImIp3nS
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
John	John	k1gMnSc1
Conroy	Conroa	k1gFnSc2
<g/>
,	,	kIx,
tajemník	tajemník	k1gMnSc1
a	a	k8xC
finanční	finanční	k2eAgMnSc1d1
správce	správce	k1gMnSc1
Viktoriiny	Viktoriin	k2eAgFnSc2d1
matky	matka	k1gFnSc2
<g/>
;	;	kIx,
spekulace	spekulace	k1gFnPc1
se	se	k3xPyFc4
objevovaly	objevovat	k5eAaImAgFnP
během	během	k7c2
celé	celý	k2eAgFnSc2d1
doby	doba	k1gFnSc2
Viktoriina	Viktoriin	k2eAgNnSc2d1
panování	panování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkoumání	zkoumání	k1gNnSc1
problému	problém	k1gInSc2
jak	jak	k8xS,k8xC
z	z	k7c2
historického	historický	k2eAgNnSc2d1
<g/>
,	,	kIx,
tak	tak	k9
přírodovědného	přírodovědný	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
nepřineslo	přinést	k5eNaPmAgNnS
ani	ani	k8xC
v	v	k7c6
současnosti	současnost	k1gFnSc6
uspokojivé	uspokojivý	k2eAgNnSc4d1
potvrzení	potvrzení	k1gNnSc4
a	a	k8xC
jakékoli	jakýkoli	k3yIgInPc4
závěry	závěr	k1gInPc4
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
v	v	k7c6
rovině	rovina	k1gFnSc6
spekulací	spekulace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Její	její	k3xOp3gFnSc7
vychovatelkou	vychovatelka	k1gFnSc7
byla	být	k5eAaImAgFnS
baronka	baronka	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Lehzenová	Lehzenová	k1gFnSc1
z	z	k7c2
Hannoveru	Hannover	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
Viktorii	Viktoria	k1gFnSc4
vychovávala	vychovávat	k5eAaImAgFnS
v	v	k7c6
německém	německý	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
až	až	k6eAd1
do	do	k7c2
tří	tři	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
princezna	princezna	k1gFnSc1
učila	učit	k5eAaImAgFnS,k5eAaPmAgFnS
francouzsky	francouzsky	k6eAd1
a	a	k8xC
anglicky	anglicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
budoucím	budoucí	k2eAgMnSc7d1
manželem	manžel	k1gMnSc7
<g/>
,	,	kIx,
bratrancem	bratranec	k1gMnSc7
Albertem	Albert	k1gMnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
seznámila	seznámit	k5eAaPmAgFnS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
rodišti	rodiště	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1836	#num#	k4
<g/>
;	;	kIx,
tehdy	tehdy	k6eAd1
jí	on	k3xPp3gFnSc3
bylo	být	k5eAaImAgNnS
17	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Nástup	nástup	k1gInSc1
na	na	k7c4
trůn	trůn	k1gInSc4
</s>
<s>
Portrét	portrét	k1gInSc1
mladé	mladý	k2eAgFnSc2d1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
</s>
<s>
Král	Král	k1gMnSc1
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1837	#num#	k4
na	na	k7c4
zástavu	zástava	k1gFnSc4
srdce	srdce	k1gNnSc1
a	a	k8xC
princezna	princezna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
z	z	k7c2
Kentu	Kent	k1gInSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
britskou	britský	k2eAgFnSc7d1
královnou	královna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korunována	korunován	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1838	#num#	k4
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	s	k7c7
prvním	první	k4xOgMnSc7
panovníkem	panovník	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
si	se	k3xPyFc3
za	za	k7c4
své	svůj	k3xOyFgNnSc4
sídlo	sídlo	k1gNnSc4
zvolil	zvolit	k5eAaPmAgInS
nově	nově	k6eAd1
dobudovaný	dobudovaný	k2eAgInSc1d1
Buckinghamský	buckinghamský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
práva	právo	k1gNnSc2
uplatňovaného	uplatňovaný	k2eAgNnSc2d1
pro	pro	k7c4
nástupnictví	nástupnictví	k1gNnSc4
na	na	k7c4
hannoverský	hannoverský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
nemohla	moct	k5eNaImAgFnS
být	být	k5eAaImF
nástupkyní	nástupkyně	k1gFnSc7
na	na	k7c6
trůnu	trůn	k1gInSc6
žena	žena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hannoverským	hannoverský	k2eAgMnSc7d1
králem	král	k1gMnSc7
se	se	k3xPyFc4
tak	tak	k9
stal	stát	k5eAaPmAgMnS
její	její	k3xOp3gMnSc1
strýc	strýc	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
August	August	k1gMnSc1
I.	I.	kA
Hannoverský	hannoverský	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
nebyla	být	k5eNaImAgFnS
Viktorie	Viktorie	k1gFnSc1
provdána	provdat	k5eAaPmNgFnS
a	a	k8xC
neměla	mít	k5eNaImAgFnS
žádné	žádný	k3yNgMnPc4
potomky	potomek	k1gMnPc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
její	její	k3xOp3gMnSc1
strýc	strýc	k1gMnSc1
i	i	k9
nástupcem	nástupce	k1gMnSc7
britského	britský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1840	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
královně	královna	k1gFnSc3
Viktorii	Viktoria	k1gFnSc3
a	a	k8xC
princi	princ	k1gMnSc3
Albertovi	Albert	k1gMnSc3
narodilo	narodit	k5eAaPmAgNnS
první	první	k4xOgNnSc1
dítě	dítě	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
jejího	její	k3xOp3gInSc2
nástupu	nástup	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
vládu	vláda	k1gFnSc4
kontrolovala	kontrolovat	k5eAaImAgFnS
strana	strana	k1gFnSc1
whigů	whig	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
u	u	k7c2
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
krátkou	krátký	k2eAgFnSc7d1
přestávkou	přestávka	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1830	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
lord	lord	k1gMnSc1
Melbourne	Melbourne	k1gNnSc2
měl	mít	k5eAaImAgMnS
na	na	k7c4
politicky	politicky	k6eAd1
nezkušenou	zkušený	k2eNgFnSc4d1
Viktorii	Viktoria	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
jeho	jeho	k3xOp3gFnPc6
radách	rada	k1gFnPc6
<g/>
,	,	kIx,
velký	velký	k2eAgInSc1d1
vliv	vliv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Melbourne	Melbourne	k1gNnSc6
se	se	k3xPyFc4
ale	ale	k9
neudržel	udržet	k5eNaPmAgMnS
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
nepopulárním	populární	k2eNgMnSc7d1
a	a	k8xC
musel	muset	k5eAaImAgInS
řešit	řešit	k5eAaImF
problémy	problém	k1gInPc4
v	v	k7c6
britských	britský	k2eAgFnPc6d1
koloniích	kolonie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
velmi	velmi	k6eAd1
populární	populární	k2eAgNnPc1d1
hnutí	hnutí	k1gNnPc1
chartistů	chartista	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
požadovali	požadovat	k5eAaImAgMnP
všeobecné	všeobecný	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
demokratické	demokratický	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1839	#num#	k4
lord	lord	k1gMnSc1
Melbourne	Melbourne	k1gNnPc2
odstoupil	odstoupit	k5eAaPmAgMnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
radikálové	radikál	k1gMnPc1
a	a	k8xC
toryové	tory	k1gMnPc1
zablokovali	zablokovat	k5eAaPmAgMnP
přijetí	přijetí	k1gNnSc4
důležitého	důležitý	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
hlavním	hlavní	k2eAgMnSc7d1
rádcem	rádce	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
její	její	k3xOp3gMnSc1
strýc	strýc	k1gMnSc1
(	(	kIx(
<g/>
bratr	bratr	k1gMnSc1
její	její	k3xOp3gFnSc2
matky	matka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
belgický	belgický	k2eAgMnSc1d1
král	král	k1gMnSc1
Leopold	Leopold	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Královna	královna	k1gFnSc1
poté	poté	k6eAd1
pověřila	pověřit	k5eAaPmAgFnS
sestavením	sestavení	k1gNnSc7
vlády	vláda	k1gFnSc2
torye	tory	k1gMnPc4
Roberta	Robert	k1gMnSc4
Peela	Peel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
obával	obávat	k5eAaImAgMnS
neúspěchu	neúspěch	k1gInSc2
při	při	k7c6
zajištění	zajištění	k1gNnSc6
většiny	většina	k1gFnSc2
v	v	k7c6
parlamentu	parlament	k1gInSc6
<g/>
,	,	kIx,
požadoval	požadovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byli	být	k5eAaImAgMnP
členové	člen	k1gMnPc1
královské	královský	k2eAgFnSc2d1
domácnosti	domácnost	k1gFnSc2
obměněni	obměnit	k5eAaPmNgMnP
podle	podle	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
návrhu	návrh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
dvorních	dvorní	k2eAgFnPc2d1
dam	dáma	k1gFnPc2
byla	být	k5eAaImAgFnS
manželkami	manželka	k1gFnPc7
whigů	whig	k1gMnPc2
a	a	k8xC
Peel	Peela	k1gFnPc2
je	být	k5eAaImIp3nS
chtěl	chtít	k5eAaImAgMnS
vyměnit	vyměnit	k5eAaPmF
za	za	k7c4
manželky	manželka	k1gFnPc4
toryů	tory	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktorie	Viktorie	k1gFnSc1
jejich	jejich	k3xOp3gFnSc4
obměnu	obměna	k1gFnSc4
odmítla	odmítnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
považovala	považovat	k5eAaImAgFnS
spíše	spíše	k9
za	za	k7c4
své	svůj	k3xOyFgFnPc4
přítelkyně	přítelkyně	k1gFnPc4
než	než	k8xS
za	za	k7c4
součást	součást	k1gFnSc4
ceremonie	ceremonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
krize	krize	k1gFnSc1
(	(	kIx(
<g/>
Bedchamber	Bedchamber	k1gInSc1
Crisis	Crisis	k1gFnSc2
<g/>
)	)	kIx)
vyústila	vyústit	k5eAaPmAgFnS
v	v	k7c4
Peelovo	Peelův	k2eAgNnSc4d1
prohlášení	prohlášení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c2
těchto	tento	k3xDgNnPc2
omezení	omezení	k1gNnPc2
ze	z	k7c2
strany	strana	k1gFnSc2
královny	královna	k1gFnSc2
nemůže	moct	k5eNaImIp3nS
sestavit	sestavit	k5eAaPmF
funkční	funkční	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
následované	následovaný	k2eAgMnPc4d1
jeho	jeho	k3xOp3gNnSc1
rezignací	rezignace	k1gFnSc7
a	a	k8xC
Lambovým	Lambův	k2eAgNnSc7d1
(	(	kIx(
<g/>
lord	lord	k1gMnSc1
Melbourne	Melbourne	k1gNnSc2
<g/>
)	)	kIx)
návratem	návrat	k1gInSc7
do	do	k7c2
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sňatek	sňatek	k1gInSc1
a	a	k8xC
pokusy	pokus	k1gInPc1
o	o	k7c4
atentát	atentát	k1gInSc4
</s>
<s>
Svatba	svatba	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
s	s	k7c7
princem	princ	k1gMnSc7
Albertem	Albert	k1gMnSc7
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
v	v	k7c6
královské	královský	k2eAgFnSc6d1
kapli	kaple	k1gFnSc6
St.	st.	kA
James	James	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Palace	Palace	k1gFnPc1
za	za	k7c2
svého	svůj	k3xOyFgMnSc2
bratrance	bratranec	k1gMnSc2
prince	princ	k1gMnSc2
Alberta	Albert	k1gMnSc2
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1840	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejen	nejen	k6eAd1
jejím	její	k3xOp3gMnSc7
manželem	manžel	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
jejím	její	k3xOp3gMnSc7
důležitým	důležitý	k2eAgMnSc7d1
politickým	politický	k2eAgMnSc7d1
rádcem	rádce	k1gMnSc7
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
této	tento	k3xDgFnSc6
roli	role	k1gFnSc6
nahradil	nahradit	k5eAaPmAgMnS
lorda	lord	k1gMnSc4
Melbourna	Melbourno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
Viktorie	Viktorie	k1gFnSc1
poprvé	poprvé	k6eAd1
těhotná	těhotný	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokusil	pokusit	k5eAaPmAgMnS
se	se	k3xPyFc4
osmnáctiletý	osmnáctiletý	k2eAgMnSc1d1
Eduard	Eduard	k1gMnSc1
Oxford	Oxford	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jela	jet	k5eAaImAgFnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
mužem	muž	k1gMnSc7
v	v	k7c6
kočáru	kočár	k1gInSc6
<g/>
,	,	kIx,
spáchat	spáchat	k5eAaPmF
na	na	k7c4
ni	on	k3xPp3gFnSc4
atentát	atentát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
vystřelil	vystřelit	k5eAaPmAgInS
dvakrát	dvakrát	k6eAd1
z	z	k7c2
pistole	pistol	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
pokaždé	pokaždé	k6eAd1
minul	minout	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
za	za	k7c4
zradu	zrada	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
nepříčetnosti	nepříčetnost	k1gFnSc2
byl	být	k5eAaImAgMnS
obvinění	obvinění	k1gNnSc4
zbaven	zbaven	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
dítě	dítě	k1gNnSc1
královského	královský	k2eAgInSc2d1
páru	pár	k1gInSc2
<g/>
,	,	kIx,
princezna	princezna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
narodilo	narodit	k5eAaPmAgNnS
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1840	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
o	o	k7c4
atentát	atentát	k1gInSc4
na	na	k7c6
Viktorii	Viktoria	k1gFnSc6
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgFnP
v	v	k7c6
období	období	k1gNnSc6
květen	květen	k1gInSc4
až	až	k8xS
červen	červen	k1gInSc4
1842	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Francis	Francis	k1gFnSc4
vystřelil	vystřelit	k5eAaPmAgMnS
na	na	k7c4
Viktorii	Viktoria	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
projížděla	projíždět	k5eAaImAgFnS
St.	st.	kA
James	James	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Parkem	park	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
ihned	ihned	k6eAd1
zneškodněn	zneškodnit	k5eAaPmNgMnS
policistou	policista	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
tento	tento	k3xDgInSc1
trest	trest	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
na	na	k7c4
doživotní	doživotní	k2eAgInSc4d1
žalář	žalář	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
William	William	k1gInSc4
Bean	Bean	k1gInSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgInS
na	na	k7c4
Viktorii	Viktoria	k1gFnSc4
vystřelit	vystřelit	k5eAaPmF
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jeho	jeho	k3xOp3gFnSc1
pistole	pistole	k1gFnSc1
byla	být	k5eAaImAgFnS
nabita	nabít	k5eAaPmNgFnS,k5eAaBmNgFnS
papírovými	papírový	k2eAgFnPc7d1
kuličkami	kulička	k1gFnPc7
a	a	k8xC
tabákem	tabák	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tento	tento	k3xDgInSc1
jeho	jeho	k3xOp3gInSc1
čin	čin	k1gInSc1
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
potrestán	potrestat	k5eAaPmNgInS
trestem	trest	k1gInSc7
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princ	princ	k1gMnSc1
Albert	Albert	k1gMnSc1
podnítil	podnítit	k5eAaPmAgMnS
parlament	parlament	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přijal	přijmout	k5eAaPmAgInS
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
by	by	kYmCp3nS
použití	použití	k1gNnSc1
zbraně	zbraň	k1gFnPc1
v	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
panovníka	panovník	k1gMnSc2
za	za	k7c7
účelem	účel	k1gInSc7
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
polekání	polekání	k1gNnSc2
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
potrestáno	potrestat	k5eAaPmNgNnS
maximálně	maximálně	k6eAd1
sedmi	sedm	k4xCc7
lety	léto	k1gNnPc7
vězení	vězení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bean	Bean	k1gMnSc1
tak	tak	k9
byl	být	k5eAaImAgMnS
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
k	k	k7c3
18	#num#	k4
měsícům	měsíc	k1gInPc3
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Počáteční	počáteční	k2eAgNnSc1d1
období	období	k1gNnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Královská	královský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
</s>
<s>
Robert	Robert	k1gMnSc1
Peel	Peel	k1gMnSc1
jako	jako	k8xC,k8xS
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
musel	muset	k5eAaImAgMnS
brzy	brzy	k6eAd1
čelit	čelit	k5eAaImF
krizi	krize	k1gFnSc4
při	při	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c4
zrušení	zrušení	k1gNnSc4
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
toryů	tory	k1gMnPc2
<g/>
,	,	kIx,
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
označovaných	označovaný	k2eAgInPc2d1
za	za	k7c4
konzervativce	konzervativec	k1gMnPc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
proti	proti	k7c3
jejich	jejich	k3xOp3gNnSc3
zrušení	zrušení	k1gNnSc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
část	část	k1gFnSc1
Peelových	Peelův	k2eAgMnPc2d1
spojenců	spojenec	k1gMnPc2
mezi	mezi	k7c7
toryi	tory	k1gMnPc7
a	a	k8xC
whigové	whig	k1gMnPc1
zrušení	zrušení	k1gNnSc2
podporovali	podporovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peel	Peela	k1gFnPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc2
1846	#num#	k4
zrušení	zrušení	k1gNnSc1
zákonů	zákon	k1gInPc2
přijato	přijmout	k5eAaPmNgNnS
těsnou	těsný	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
<g/>
,	,	kIx,
odstoupil	odstoupit	k5eAaPmAgMnS
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
John	John	k1gMnSc1
Russell	Russell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russellova	Russellův	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
byla	být	k5eAaImAgFnS
složena	složit	k5eAaPmNgFnS
whigy	whig	k1gMnPc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
netěšila	těšit	k5eNaImAgFnS
královnině	královnin	k2eAgFnSc3d1
přízni	přízeň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Největším	veliký	k2eAgInSc7d3
problémem	problém	k1gInSc7
byl	být	k5eAaImAgMnS
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
lord	lord	k1gMnSc1
Henry	Henry	k1gMnSc1
Temple	templ	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
často	často	k6eAd1
jednal	jednat	k5eAaImAgMnS
bez	bez	k7c2
konzultace	konzultace	k1gFnSc2
s	s	k7c7
vládou	vláda	k1gFnSc7
<g/>
,	,	kIx,
premiérem	premiér	k1gMnSc7
nebo	nebo	k8xC
královnou	královna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktorie	Viktorie	k1gFnSc1
proti	proti	k7c3
jeho	jeho	k3xOp3gNnSc3
jednání	jednání	k1gNnSc3
protestovala	protestovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1851	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
Temple	templ	k1gInSc5
odvolán	odvolat	k5eAaPmNgMnS
<g/>
,	,	kIx,
bezvýsledně	bezvýsledně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dalším	další	k2eAgInPc3d1
pokusům	pokus	k1gInPc3
o	o	k7c4
atentát	atentát	k1gInSc4
na	na	k7c6
Viktorii	Viktoria	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1849	#num#	k4
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
pokusil	pokusit	k5eAaPmAgMnS
vylekat	vylekat	k5eAaPmF
výstřelem	výstřel	k1gInSc7
z	z	k7c2
pistole	pistol	k1gFnSc2
nabité	nabitý	k2eAgFnSc2d1
pudrem	pudr	k1gInSc7
nezaměstnaný	zaměstnaný	k2eNgMnSc1d1
Ir	Ir	k1gMnSc1
William	William	k1gInSc4
Hamilton	Hamilton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1850	#num#	k4
byla	být	k5eAaImAgFnS
Viktorie	Viktorie	k1gFnSc1
zraněna	zranit	k5eAaPmNgFnS
po	po	k7c6
útoku	útok	k1gInSc6
zřejmě	zřejmě	k6eAd1
šíleného	šílený	k2eAgMnSc2d1
bývalého	bývalý	k2eAgMnSc2d1
důstojníka	důstojník	k1gMnSc2
Roberta	Robert	k1gMnSc2
Pateho	Pate	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
Socha	socha	k1gFnSc1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
v	v	k7c6
Belfastu	Belfast	k1gInSc6
v	v	k7c6
Severním	severní	k2eAgNnSc6d1
Irsku	Irsko	k1gNnSc6
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1
královna	královna	k1gFnSc1
si	se	k3xPyFc3
Irsko	Irsko	k1gNnSc1
oblíbila	oblíbit	k5eAaPmAgFnS
a	a	k8xC
vybrala	vybrat	k5eAaPmAgFnS
si	se	k3xPyFc3
Killarney	Killarney	k1gInPc4
pro	pro	k7c4
odpočinkový	odpočinkový	k2eAgInSc4d1
pobyt	pobyt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pocit	pocit	k1gInSc1
byl	být	k5eAaImAgInS
podpořen	podpořit	k5eAaPmNgInS
jejím	její	k3xOp3gNnSc7
prvotním	prvotní	k2eAgNnSc7d1
vřelým	vřelý	k2eAgNnSc7d1
přijetím	přijetí	k1gNnSc7
ze	z	k7c2
strany	strana	k1gFnSc2
Irů	Ir	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1845	#num#	k4
bylo	být	k5eAaImAgNnS
Irsko	Irsko	k1gNnSc1
postiženo	postižen	k2eAgNnSc1d1
neúrodou	neúroda	k1gFnSc7
brambor	brambora	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
v	v	k7c6
průběhu	průběh	k1gInSc6
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
vyžádala	vyžádat	k5eAaPmAgFnS
život	život	k1gInSc4
více	hodně	k6eAd2
než	než	k8xS
jednoho	jeden	k4xCgInSc2
miliónu	milión	k4xCgInSc2
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
vyústila	vyústit	k5eAaPmAgFnS
v	v	k7c6
emigraci	emigrace	k1gFnSc6
dalšího	další	k2eAgInSc2d1
miliónu	milión	k4xCgInSc2
Irů	Ir	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
tuto	tento	k3xDgFnSc4
pohromu	pohroma	k1gFnSc4
nazývanou	nazývaný	k2eAgFnSc4d1
velký	velký	k2eAgInSc4d1
irský	irský	k2eAgInSc4d1
hladomor	hladomor	k1gInSc4
věnovala	věnovat	k5eAaImAgFnS,k5eAaPmAgFnS
královna	královna	k1gFnSc1
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
prostředků	prostředek	k1gInPc2
2000	#num#	k4
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
turecký	turecký	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
nabídl	nabídnout	k5eAaPmAgMnS
pomoc	pomoc	k1gFnSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
10	#num#	k4
000	#num#	k4
liber	libra	k1gFnPc2
a	a	k8xC
Viktorie	Viktorie	k1gFnSc1
ho	on	k3xPp3gMnSc4
požádala	požádat	k5eAaPmAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tuto	tento	k3xDgFnSc4
částku	částka	k1gFnSc4
snížil	snížit	k5eAaPmAgInS
na	na	k7c4
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sultán	sultána	k1gFnPc2
nakonec	nakonec	k6eAd1
poslal	poslat	k5eAaPmAgMnS
veřejně	veřejně	k6eAd1
1000	#num#	k4
a	a	k8xC
tajně	tajně	k6eAd1
vypravil	vypravit	k5eAaPmAgMnS
tři	tři	k4xCgFnPc4
lodě	loď	k1gFnPc4
s	s	k7c7
potravinami	potravina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angličané	Angličan	k1gMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
tyto	tento	k3xDgFnPc4
lodě	loď	k1gFnPc4
zadržet	zadržet	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
neuspěli	uspět	k5eNaPmAgMnP
a	a	k8xC
pomoc	pomoc	k1gFnSc4
dorazila	dorazit	k5eAaPmAgFnS
do	do	k7c2
irského	irský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
Drogheda	Drogheda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
politika	politika	k1gFnSc1
anglického	anglický	k2eAgMnSc2d1
premiéra	premiér	k1gMnSc2
Johna	John	k1gMnSc2
Russella	Russell	k1gMnSc2
byla	být	k5eAaImAgFnS
obviňována	obviňovat	k5eAaImNgFnS
z	z	k7c2
podceňování	podceňování	k1gNnSc2
rozsahu	rozsah	k1gInSc2
katastrofy	katastrofa	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
přispělo	přispět	k5eAaPmAgNnS
k	k	k7c3
poklesu	pokles	k1gInSc3
královniny	královnin	k2eAgFnSc2d1
popularity	popularita	k1gFnSc2
v	v	k7c6
Irsku	Irsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktoriina	Viktoriin	k2eAgFnSc1d1
první	první	k4xOgFnSc1
návštěva	návštěva	k1gFnSc1
Irska	Irsko	k1gNnSc2
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
roku	rok	k1gInSc2
1849	#num#	k4
a	a	k8xC
měla	mít	k5eAaImAgFnS
za	za	k7c4
úkol	úkol	k1gInSc4
ukázat	ukázat	k5eAaPmF
zájem	zájem	k1gInSc4
britské	britský	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
o	o	k7c4
problémy	problém	k1gInPc4
Irů	Ir	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zájem	zájem	k1gInSc4
monarchie	monarchie	k1gFnSc2
o	o	k7c4
Irsko	Irsko	k1gNnSc4
poklesl	poklesnout	k5eAaPmAgInS
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
královna	královna	k1gFnSc1
odmítla	odmítnout	k5eAaPmAgFnS
navštívit	navštívit	k5eAaPmF
Irsko	Irsko	k1gNnSc4
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
dublinská	dublinský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
odmítla	odmítnout	k5eAaPmAgFnS
blahopřát	blahopřát	k5eAaImF
jejímu	její	k3xOp3gMnSc3
synovi	syn	k1gMnSc3
Eduardovi	Eduard	k1gMnSc3
ke	k	k7c3
sňatku	sňatek	k1gInSc2
a	a	k8xC
narození	narození	k1gNnSc2
nejstaršího	starý	k2eAgMnSc4d3
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
odmítla	odmítnout	k5eAaPmAgFnS
nátlak	nátlak	k1gInSc4
premiérů	premiér	k1gMnPc2
<g/>
,	,	kIx,
správců	správce	k1gMnPc2
i	i	k8xC
členů	člen	k1gMnPc2
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zřídila	zřídit	k5eAaPmAgFnS
v	v	k7c6
Irsku	Irsko	k1gNnSc6
jedno	jeden	k4xCgNnSc1
ze	z	k7c2
svých	svůj	k3xOyFgNnPc2
sídel	sídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
William	William	k1gInSc1
Brodrick	Brodrick	k1gInSc4
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
předchozích	předchozí	k2eAgMnPc2d1
předsedů	předseda	k1gMnPc2
unionistické	unionistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
roku	rok	k1gInSc2
1930	#num#	k4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
příčin	příčina	k1gFnPc2
neúspěchu	neúspěch	k1gInSc2
britské	britský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
v	v	k7c6
Irsku	Irsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktorie	Viktorie	k1gFnSc1
naposledy	naposledy	k6eAd1
navštívila	navštívit	k5eAaPmAgFnS
Irsko	Irsko	k1gNnSc4
roku	rok	k1gInSc2
1900	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
apelovala	apelovat	k5eAaImAgFnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
Irové	Ir	k1gMnPc1
zapojili	zapojit	k5eAaPmAgMnP
do	do	k7c2
britské	britský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
druhé	druhý	k4xOgFnSc2
búrské	búrský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacionalistický	nacionalistický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
proti	proti	k7c3
její	její	k3xOp3gFnSc3
návštěvě	návštěva	k1gFnSc3
vedl	vést	k5eAaImAgMnS
Arthur	Arthur	k1gMnSc1
Griffith	Griffith	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
o	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
využil	využít	k5eAaPmAgInS
získané	získaný	k2eAgInPc4d1
kontakty	kontakt	k1gInPc4
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
politického	politický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
Sinn	Sinn	k1gMnSc1
Féin	Féin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
Irské	irský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
Dublinu	Dublin	k1gInSc6
odstraněna	odstraněn	k2eAgFnSc1d1
socha	socha	k1gFnSc1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
Socha	socha	k1gFnSc1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
před	před	k7c7
budovou	budova	k1gFnSc7
parlamentu	parlament	k1gInSc2
Britské	britský	k2eAgFnSc2d1
Kolumbie	Kolumbie	k1gFnSc2
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Victoria	Victorium	k1gNnSc2
</s>
<s>
Socha	socha	k1gFnSc1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
v	v	k7c6
Ballaratu	Ballarat	k1gInSc6
v	v	k7c6
australském	australský	k2eAgInSc6d1
státě	stát	k1gInSc6
Victoria	Victorium	k1gNnSc2
</s>
<s>
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc1d3
koloniální	koloniální	k2eAgFnSc1d1
říše	říše	k1gFnSc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
lidstva	lidstvo	k1gNnSc2
<g/>
,	,	kIx,
ovládalo	ovládat	k5eAaImAgNnS
ke	k	k7c3
konci	konec	k1gInSc3
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
téměř	téměř	k6eAd1
čtvrtinu	čtvrtina	k1gFnSc4
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
a	a	k8xC
žila	žít	k5eAaImAgFnS
v	v	k7c6
něm	on	k3xPp3gInSc6
čtvrtina	čtvrtina	k1gFnSc1
tehdejší	tehdejší	k2eAgFnSc2d1
světové	světový	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
průmyslové	průmyslový	k2eAgFnSc3d1
revoluci	revoluce	k1gFnSc3
<g/>
,	,	kIx,
koloniální	koloniální	k2eAgFnSc3d1
říši	říš	k1gFnSc3
a	a	k8xC
silnému	silný	k2eAgNnSc3d1
námořnictvu	námořnictvo	k1gNnSc3
Royal	Royal	k1gMnSc1
Navy	Navy	k?
se	se	k3xPyFc4
z	z	k7c2
Británie	Británie	k1gFnSc2
stala	stát	k5eAaPmAgFnS
světová	světový	k2eAgFnSc1d1
velmoc	velmoc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
Viktorii	Viktoria	k1gFnSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
podporovala	podporovat	k5eAaImAgFnS
imperiální	imperiální	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
premiéra	premiér	k1gMnSc2
Benjamina	Benjamin	k1gMnSc2
Disraeliho	Disraeli	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vedla	vést	k5eAaImAgFnS
ke	k	k7c3
druhé	druhý	k4xOgFnSc3
anglo-afghánské	anglo-afghánský	k2eAgFnSc3d1
válce	válka	k1gFnSc3
nebo	nebo	k8xC
k	k	k7c3
britsko-zulské	britsko-zulský	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1879	#num#	k4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Isandlwany	Isandlwana	k1gFnSc2
zničen	zničen	k2eAgInSc1d1
silný	silný	k2eAgInSc1d1
britský	britský	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
rusko-turecké	rusko-turecký	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1877	#num#	k4
<g/>
–	–	k?
<g/>
78	#num#	k4
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
neúspěšně	úspěšně	k6eNd1
přimět	přimět	k5eAaPmF
Disraeliho	Disraeli	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vystoupil	vystoupit	k5eAaPmAgMnS
proti	proti	k7c3
Rusku	Rusko	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Viktorie	Viktorie	k1gFnSc1
měla	mít	k5eAaImAgFnS
špatný	špatný	k2eAgInSc4d1
vztah	vztah	k1gInSc4
s	s	k7c7
premiérem	premiér	k1gMnSc7
Gladstonem	Gladston	k1gInSc7
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
vinila	vinit	k5eAaImAgFnS
ze	z	k7c2
smrti	smrt	k1gFnSc2
generála	generál	k1gMnSc2
Gordona	Gordon	k1gMnSc2
při	při	k7c6
pádu	pád	k1gInSc6
Chartúmu	Chartúm	k1gInSc2
roku	rok	k1gInSc2
1885	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepodpořila	podpořit	k5eNaPmAgFnS
Gladstonův	Gladstonův	k2eAgInSc4d1
zákon	zákon	k1gInSc4
o	o	k7c6
irské	irský	k2eAgFnSc6d1
samosprávě	samospráva	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1886	#num#	k4
v	v	k7c6
britském	britský	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
zamítnut	zamítnout	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c2
vlády	vláda	k1gFnSc2
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
vedla	vést	k5eAaImAgFnS
Britská	britský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
dvě	dva	k4xCgFnPc4
opiové	opiový	k2eAgFnPc4d1
války	válka	k1gFnPc4
proti	proti	k7c3
Číně	Čína	k1gFnSc3
<g/>
,	,	kIx,
při	při	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
byl	být	k5eAaImAgInS
obsazen	obsazen	k2eAgInSc1d1
Hongkong	Hongkong	k1gInSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
podniknuta	podniknut	k2eAgFnSc1d1
invaze	invaze	k1gFnSc1
do	do	k7c2
Afghánistánu	Afghánistán	k1gInSc2
během	během	k7c2
první	první	k4xOgFnSc2
anglo-afghánské	anglo-afghánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ovšem	ovšem	k9
skončila	skončit	k5eAaPmAgFnS
masakrem	masakr	k1gInSc7
Elphinstonovy	Elphinstonův	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
roku	rok	k1gInSc2
1842	#num#	k4
<g/>
,	,	kIx,
vedena	veden	k2eAgFnSc1d1
vítězná	vítězný	k2eAgFnSc1d1
krymská	krymský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
proti	proti	k7c3
Rusku	Rusko	k1gNnSc3
<g/>
,	,	kIx,
obsazen	obsazen	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
po	po	k7c6
podepsání	podepsání	k1gNnSc6
smlouvy	smlouva	k1gFnSc2
z	z	k7c2
Waitangi	Waitang	k1gFnSc2
a	a	k8xC
vedeny	veden	k2eAgFnPc1d1
války	válka	k1gFnPc1
proti	proti	k7c3
Maorům	Maor	k1gMnPc3
<g/>
,	,	kIx,
po	po	k7c6
sedmé	sedmý	k4xOgFnSc6
anglo-ašantské	anglo-ašantský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byla	být	k5eAaImAgFnS
anektována	anektován	k2eAgFnSc1d1
Ašantská	Ašantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
okupován	okupován	k2eAgInSc1d1
Egypt	Egypt	k1gInSc1
a	a	k8xC
po	po	k7c6
potlačení	potlačení	k1gNnSc6
Mahdího	Mahdí	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
obsazen	obsadit	k5eAaPmNgInS
Sudán	Sudán	k1gInSc1
<g/>
,	,	kIx,
britská	britský	k2eAgFnSc1d1
invaze	invaze	k1gFnSc1
do	do	k7c2
Transvaalu	Transvaal	k1gInSc2
byla	být	k5eAaImAgFnS
odražena	odrazit	k5eAaPmNgFnS
v	v	k7c6
první	první	k4xOgFnSc6
búrské	búrský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
po	po	k7c6
třetí	třetí	k4xOgFnSc6
anglo-barmské	anglo-barmský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byla	být	k5eAaImAgFnS
okupována	okupován	k2eAgFnSc1d1
Barma	Barma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1867	#num#	k4
obdržela	obdržet	k5eAaPmAgFnS
Kanada	Kanada	k1gFnSc1
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc1
britská	britský	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
status	status	k1gInSc4
dominia	dominion	k1gNnSc2
a	a	k8xC
právo	právo	k1gNnSc4
na	na	k7c4
samosprávu	samospráva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1868	#num#	k4
přestali	přestat	k5eAaPmAgMnP
být	být	k5eAaImF
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
převáženi	převážen	k2eAgMnPc1d1
trestanci	trestanec	k1gMnPc1
z	z	k7c2
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Berlínské	berlínský	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1884	#num#	k4
si	se	k3xPyFc3
Britská	britský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgInPc7d1
evropskými	evropský	k2eAgInPc7d1
státy	stát	k1gInPc7
rozdělila	rozdělit	k5eAaPmAgFnS
Afriku	Afrika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1
nositelem	nositel	k1gMnSc7
titulu	titul	k1gInSc2
císař	císař	k1gMnSc1
Indie	Indie	k1gFnSc2
byl	být	k5eAaImAgMnS
Bahádur	Bahádur	k1gMnSc1
Šáh	šáh	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Mughalů	Mughal	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
velkém	velký	k2eAgNnSc6d1
indickém	indický	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1857	#num#	k4
rozpuštěna	rozpuštěn	k2eAgFnSc1d1
Britská	britský	k2eAgFnSc1d1
Východoindická	východoindický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
spravovala	spravovat	k5eAaImAgFnS
většinu	většina	k1gFnSc4
Indického	indický	k2eAgInSc2d1
subkontinentu	subkontinent	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
Britské	britský	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
korunní	korunní	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
místokrálem	místokrál	k1gMnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
titul	titul	k1gInSc1
císařovny	císařovna	k1gFnSc2
Indie	Indie	k1gFnSc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1876	#num#	k4
používán	používán	k2eAgInSc4d1
Viktorií	Viktoria	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
titul	titul	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
devatenáct	devatenáct	k4xCc4
let	léto	k1gNnPc2
po	po	k7c6
formálním	formální	k2eAgNnSc6d1
začlenění	začlenění	k1gNnSc6
území	území	k1gNnSc2
ovládaných	ovládaný	k2eAgFnPc2d1
Británií	Británie	k1gFnPc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
do	do	k7c2
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
titul	titul	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
pro	pro	k7c4
Viktorii	Viktoria	k1gFnSc4
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
Disraeli	Disrael	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
proti	proti	k7c3
búrským	búrský	k2eAgFnPc3d1
republikám	republika	k1gFnPc3
Transvaal	Transvaal	k1gInSc1
a	a	k8xC
Orange	Orange	k1gFnSc1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
natolik	natolik	k6eAd1
nepopulární	populární	k2eNgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Viktorie	Viktorie	k1gFnSc1
roku	rok	k1gInSc2
1900	#num#	k4
vynechala	vynechat	k5eAaPmAgFnS
svojí	svojit	k5eAaImIp3nS
každoroční	každoroční	k2eAgFnSc1d1
návštěvu	návštěva	k1gFnSc4
Francie	Francie	k1gFnSc2
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgNnSc2
poprvé	poprvé	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1861	#num#	k4
navštívila	navštívit	k5eAaPmAgFnS
Irsko	Irsko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Viktoriino	Viktoriin	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
dnes	dnes	k6eAd1
nese	nést	k5eAaImIp3nS
mnoho	mnoho	k4c1
míst	místo	k1gNnPc2
bývalého	bývalý	k2eAgNnSc2d1
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
australské	australský	k2eAgInPc4d1
státy	stát	k1gInPc4
(	(	kIx(
<g/>
Victoria	Victorium	k1gNnSc2
a	a	k8xC
Queensland	Queenslanda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Britské	britský	k2eAgFnSc2d1
Kolumbie	Kolumbie	k1gFnSc2
(	(	kIx(
<g/>
Victoria	Victorium	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
Saskatchewanu	Saskatchewana	k1gFnSc4
(	(	kIx(
<g/>
Regina	Regina	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
,	,	kIx,
centrum	centrum	k1gNnSc1
Hongkongu	Hongkong	k1gInSc2
(	(	kIx(
<g/>
Victoria	Victorium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Seychel	Seychely	k1gFnPc2
(	(	kIx(
<g/>
Victoria	Victorium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
ostrova	ostrov	k1gInSc2
Gozo	Gozo	k6eAd1
na	na	k7c6
Maltě	Malta	k1gFnSc6
(	(	kIx(
<g/>
Victoria	Victorium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
největší	veliký	k2eAgNnSc1d3
africké	africký	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
Viktoriino	Viktoriin	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
horní	horní	k2eAgInSc1d1
tok	tok	k1gInSc1
Nilu	Nil	k1gInSc2
Viktoriin	Viktoriin	k2eAgInSc1d1
Nil	Nil	k1gInSc1
nebo	nebo	k8xC
Viktoriiny	Viktoriin	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
princi	princa	k1gFnSc6
Albertovi	Albert	k1gMnSc3
bylo	být	k5eAaImAgNnS
pojmenováno	pojmenován	k2eAgNnSc1d1
Albertovo	Albertův	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
prince	princ	k1gMnSc2
Alberta	Albert	k1gMnSc2
</s>
<s>
Fotografie	fotografia	k1gFnPc1
Jiřího	Jiří	k1gMnSc2
V.	V.	kA
<g/>
,	,	kIx,
Viktorie	Viktoria	k1gFnSc2
<g/>
,	,	kIx,
Eduarda	Eduard	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
mladého	mladý	k2eAgMnSc2d1
Eduarda	Eduard	k1gMnSc2
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
Princ	princ	k1gMnSc1
Albert	Albert	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1861	#num#	k4
na	na	k7c4
tyfus	tyfus	k1gInSc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
špatných	špatný	k2eAgFnPc2d1
hygienických	hygienický	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktorii	Viktoria	k1gFnSc3
<g/>
,	,	kIx,
poznamenanou	poznamenaný	k2eAgFnSc7d1
smrtí	smrt	k1gFnSc7
její	její	k3xOp3gFnSc2
matky	matka	k1gFnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
smrt	smrt	k1gFnSc1
silně	silně	k6eAd1
zasáhla	zasáhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
celý	celý	k2eAgInSc4d1
zbytek	zbytek	k1gInSc4
života	život	k1gInSc2
držela	držet	k5eAaImAgFnS
za	za	k7c4
manžela	manžel	k1gMnSc4
smutek	smutek	k1gInSc4
<g/>
,	,	kIx,
nosila	nosit	k5eAaImAgFnS
černé	černý	k2eAgFnPc4d1
šaty	šata	k1gFnPc4
<g/>
,	,	kIx,
vyhýbala	vyhýbat	k5eAaImAgFnS
se	se	k3xPyFc4
veřejnému	veřejný	k2eAgInSc3d1
životu	život	k1gInSc3
a	a	k8xC
v	v	k7c6
následujícím	následující	k2eAgNnSc6d1
období	období	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
Londýně	Londýn	k1gInSc6
objevovala	objevovat	k5eAaImAgFnS
jen	jen	k9
málokdy	málokdy	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Její	její	k3xOp3gNnSc1
stažení	stažení	k1gNnSc1
se	se	k3xPyFc4
z	z	k7c2
veřejnosti	veřejnost	k1gFnSc2
snížilo	snížit	k5eAaPmAgNnS
popularitu	popularita	k1gFnSc4
monarchie	monarchie	k1gFnSc2
a	a	k8xC
přispělo	přispět	k5eAaPmAgNnS
k	k	k7c3
posílení	posílení	k1gNnSc3
republikánského	republikánský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
Viktorie	Viktorie	k1gFnSc1
plnila	plnit	k5eAaImAgFnS
své	svůj	k3xOyFgFnPc4
oficiální	oficiální	k2eAgFnPc4d1
vládní	vládní	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
<g/>
,	,	kIx,
žila	žít	k5eAaImAgFnS
v	v	k7c6
odloučení	odloučení	k1gNnSc6
na	na	k7c6
svých	svůj	k3xOyFgNnPc6
sídlech	sídlo	k1gNnPc6
na	na	k7c6
Balmoralském	Balmoralský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
ostrově	ostrov	k1gInSc6
Wightu	Wight	k1gInSc2
nebo	nebo	k8xC
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Pozdní	pozdní	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Roku	rok	k1gInSc2
1887	#num#	k4
oslavilo	oslavit	k5eAaPmAgNnS
britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
padesáté	padesátý	k4xOgNnSc4
výročí	výročí	k1gNnSc4
jejího	její	k3xOp3gInSc2
nástupu	nástup	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
té	ten	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
pořádal	pořádat	k5eAaImAgInS
královský	královský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
recepci	recepce	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
pozval	pozvat	k5eAaPmAgMnS
50	#num#	k4
evropských	evropský	k2eAgMnPc2d1
králů	král	k1gMnPc2
a	a	k8xC
princů	princ	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Irští	irský	k2eAgMnPc1d1
anarchisté	anarchista	k1gMnPc1
chtěli	chtít	k5eAaImAgMnP
této	tento	k3xDgFnSc2
akce	akce	k1gFnSc2
využít	využít	k5eAaPmF
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nechali	nechat	k5eAaPmAgMnP
vybouchnout	vybouchnout	k5eAaPmF
Westminsterské	Westminsterský	k2eAgNnSc4d1
opatství	opatství	k1gNnSc4
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
tam	tam	k6eAd1
měla	mít	k5eAaImAgFnS
konat	konat	k5eAaImF
bohoslužba	bohoslužba	k1gFnSc1
za	za	k7c2
účasti	účast	k1gFnSc2
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
plán	plán	k1gInSc1
byl	být	k5eAaImAgInS
ale	ale	k9
odhalen	odhalen	k2eAgMnSc1d1
před	před	k7c7
jeho	jeho	k3xOp3gNnSc7
uskutečněním	uskutečnění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
šedesáté	šedesátý	k4xOgNnSc4
výročí	výročí	k1gNnSc4
nástupu	nástup	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
byli	být	k5eAaImAgMnP
pozváni	pozván	k2eAgMnPc1d1
předsedové	předseda	k1gMnPc1
vlád	vláda	k1gFnPc2
všech	všecek	k3xTgFnPc2
samosprávných	samosprávný	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostní	slavnostní	k2eAgInSc1d1
průvod	průvod	k1gInSc1
zahrnoval	zahrnovat	k5eAaImAgInS
vojenské	vojenský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
ze	z	k7c2
všech	všecek	k3xTgFnPc2
britských	britský	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
a	a	k8xC
kolonií	kolonie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkovné	děkovný	k2eAgFnPc4d1
bohoslužby	bohoslužba	k1gFnPc4
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
před	před	k7c7
katedrálou	katedrála	k1gFnSc7
svatého	svatý	k2eAgMnSc2d1
Pavla	Pavel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Královnin	královnin	k2eAgInSc1d1
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zhoršoval	zhoršovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vánoční	vánoční	k2eAgInPc1d1
svátky	svátek	k1gInPc1
roku	rok	k1gInSc2
1900	#num#	k4
trávila	trávit	k5eAaImAgFnS
v	v	k7c6
jako	jako	k9
obvykle	obvykle	k6eAd1
v	v	k7c6
Osborne	Osborn	k1gInSc5
House	house	k1gNnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Wight	Wight	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
také	také	k9
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
pohřbena	pohřben	k2eAgFnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
ve	v	k7c6
Frogmore	Frogmor	k1gInSc5
Mauzoleu	mauzoleum	k1gNnSc6
vedle	vedle	k7c2
svého	svůj	k3xOyFgMnSc2
manžela	manžel	k1gMnSc2
prince	princ	k1gMnSc2
Alberta	Albert	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktorie	Viktorie	k1gFnSc1
byla	být	k5eAaImAgFnS
posledním	poslední	k2eAgMnSc7d1
příslušníkem	příslušník	k1gMnSc7
hannoverské	hannoverský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
na	na	k7c6
britském	britský	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInSc4
syn	syn	k1gMnSc1
a	a	k8xC
následník	následník	k1gMnSc1
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
pocházel	pocházet	k5eAaImAgMnS
ze	z	k7c2
sasko-kobursko-gotské	sasko-kobursko-gotský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1917	#num#	k4
<g/>
,	,	kIx,
za	za	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
změnil	změnit	k5eAaPmAgMnS
její	její	k3xOp3gMnSc1
vnuk	vnuk	k1gMnSc1
Jiří	Jiří	k1gMnSc1
V.	V.	kA
pod	pod	k7c7
silným	silný	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
protiněmecky	protiněmecky	k6eAd1
naladěné	naladěný	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
jméno	jméno	k1gNnSc1
rodu	rod	k1gInSc2
na	na	k7c6
Windsorská	windsorský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Oslavy	oslava	k1gFnPc4
diamantového	diamantový	k2eAgNnSc2d1
jubilea	jubileum	k1gNnSc2
nástupu	nástup	k1gInSc2
Viktorie	Viktoria	k1gFnSc2
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
roku	rok	k1gInSc2
1897	#num#	k4
</s>
<s>
Délka	délka	k1gFnSc1
její	její	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
zhruba	zhruba	k6eAd1
63	#num#	k4
let	léto	k1gNnPc2
7	#num#	k4
měsíců	měsíc	k1gInPc2
byla	být	k5eAaImAgFnS
opravdu	opravdu	k6eAd1
velmi	velmi	k6eAd1
výjimečná	výjimečný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
okolnost	okolnost	k1gFnSc1
obzvlášť	obzvlášť	k6eAd1
vynikne	vyniknout	k5eAaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
si	se	k3xPyFc3
její	její	k3xOp3gFnSc4
životní	životní	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
srovnáme	srovnat	k5eAaPmIp1nP
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
významnými	významný	k2eAgFnPc7d1
panovnicemi	panovnice	k1gFnPc7
oné	onen	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
jejími	její	k3xOp3gFnPc7
současnicemi	současnice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
s	s	k7c7
předposlední	předposlední	k2eAgFnSc7d1
rakouskou	rakouský	k2eAgFnSc7d1
císařovnou	císařovna	k1gFnSc7
a	a	k8xC
českou	český	k2eAgFnSc7d1
královnou	královna	k1gFnSc7
Alžbětou	Alžběta	k1gFnSc7
Bavorskou	bavorský	k2eAgFnSc4d1
<g/>
,	,	kIx,
zvanou	zvaný	k2eAgFnSc4d1
Sissi	Sisse	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktorie	Viktorie	k1gFnSc1
nastoupila	nastoupit	k5eAaPmAgFnS
na	na	k7c4
britský	britský	k2eAgInSc4d1
královský	královský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
v	v	k7c6
červnu	červen	k1gInSc6
1837	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
Sissi	Sisse	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
právě	právě	k9
na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
více	hodně	k6eAd2
než	než	k8xS
6	#num#	k4
měsíců	měsíc	k1gInPc2
po	po	k7c6
nástupu	nástup	k1gInSc6
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alžběta	Alžběta	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
tragicky	tragicky	k6eAd1
zahynula	zahynout	k5eAaPmAgFnS
v	v	k7c6
září	září	k1gNnSc6
roku	rok	k1gInSc2
1898	#num#	k4
<g/>
,	,	kIx,
Viktoriina	Viktoriin	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
po	po	k7c6
této	tento	k3xDgFnSc6
smutné	smutný	k2eAgFnSc6d1
události	událost	k1gFnSc6
trvala	trvat	k5eAaImAgFnS
ještě	ještě	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Setkání	setkání	k1gNnSc1
s	s	k7c7
ruským	ruský	k2eAgMnSc7d1
carem	car	k1gMnSc7
Mikulášem	Mikuláš	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
vnučkou	vnučka	k1gFnSc7
carevnou	carevna	k1gFnSc7
Alexandrou	Alexandra	k1gFnSc7
Fjodorovnou	Fjodorovna	k1gFnSc7
na	na	k7c6
zámku	zámek	k1gInSc6
Balmoral	Balmoral	k1gFnSc2
roku	rok	k1gInSc2
1896	#num#	k4
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
99	#num#	k4
dní	den	k1gInPc2
německá	německý	k2eAgFnSc1d1
císařovna	císařovna	k1gFnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
posledního	poslední	k2eAgMnSc2d1
německého	německý	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Viléma	Vilém	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Edward	Edward	k1gMnSc1
(	(	kIx(
<g/>
1841	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
král	král	k1gMnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1901	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
Jiřího	Jiří	k1gMnSc2
V.	V.	kA
<g/>
,	,	kIx,
krále	král	k1gMnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1910	#num#	k4
<g/>
-	-	kIx~
<g/>
1936	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
Alice	Alice	k1gFnSc1
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hesenská	hesenský	k2eAgFnSc1d1
velkovévodkyně	velkovévodkyně	k1gFnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
ruské	ruský	k2eAgFnSc2d1
carevny	carevna	k1gFnSc2
Alexandry	Alexandra	k1gFnSc2
Fjodorovny	Fjodorovna	k1gFnSc2
<g/>
,	,	kIx,
manželky	manželka	k1gFnPc1
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Alfréd	Alfréd	k1gMnSc1
(	(	kIx(
<g/>
1844	#num#	k4
<g/>
–	–	k?
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Edinburghu	Edinburgh	k1gInSc2
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
rumunské	rumunský	k2eAgFnSc2d1
královny	královna	k1gFnSc2
Marie	Maria	k1gFnSc2
a	a	k8xC
Viktorie	Viktoria	k1gFnSc2
Melity	Melita	k1gFnSc2
<g/>
,	,	kIx,
prvním	první	k4xOgInSc7
sňatkem	sňatek	k1gInSc7
hesenské	hesenský	k2eAgFnSc2d1
velkovévodkyně	velkovévodkyně	k1gFnSc2
a	a	k8xC
druhým	druhý	k4xOgNnSc7
ruské	ruský	k2eAgFnSc2d1
velkokněžny	velkokněžna	k1gFnSc2
<g/>
,	,	kIx,
admirál	admirál	k1gMnSc1
královského	královský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
a	a	k8xC
velitel	velitel	k1gMnSc1
středomořské	středomořský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
Helena	Helena	k1gFnSc1
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
–	–	k?
<g/>
1923	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
Červeného	Červeného	k2eAgInSc2d1
kříže	kříž	k1gInSc2
<g/>
;	;	kIx,
</s>
<s>
Luisa	Luisa	k1gFnSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévodkyně	vévodkyně	k1gFnSc1
z	z	k7c2
Argyllu	Argyll	k1gInSc2
<g/>
,	,	kIx,
jejím	její	k3xOp3gMnSc7
manželem	manžel	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
John	John	k1gMnSc1
Campbell	Campbell	k1gMnSc1
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Argyllu	Argyll	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1878	#num#	k4
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
Generálním	generální	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
Kanady	Kanada	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
Artur	Artur	k1gMnSc1
(	(	kIx(
<g/>
1850	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Connaught	Connaughta	k1gFnPc2
a	a	k8xC
Strathearn	Strathearna	k1gFnPc2
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
armády	armáda	k1gFnSc2
v	v	k7c6
Irsku	Irsko	k1gNnSc6
a	a	k8xC
generální	generální	k2eAgMnSc1d1
inspektor	inspektor	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
člen	člen	k1gMnSc1
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
Generálním	generální	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
Kanady	Kanada	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
Leopold	Leopold	k1gMnSc1
(	(	kIx(
<g/>
1853	#num#	k4
<g/>
–	–	k?
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Albany	Albana	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
prvním	první	k4xOgMnSc7
dítětem	dítě	k1gNnSc7
porozeným	porozený	k2eAgMnSc7d1
bezbolestně	bezbolestně	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
při	při	k7c6
jeho	jeho	k3xOp3gInSc6
porodu	porod	k1gInSc6
použil	použít	k5eAaPmAgMnS
lékař	lékař	k1gMnSc1
John	John	k1gMnSc1
Snow	Snow	k1gMnSc1
chloroform	chloroform	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c4
toto	tento	k3xDgNnSc4
byl	být	k5eAaImAgInS
povýšen	povýšit	k5eAaPmNgMnS
do	do	k7c2
rytířského	rytířský	k2eAgInSc2d1
stavu	stav	k1gInSc2
</s>
<s>
Beatrix	Beatrix	k1gInSc1
(	(	kIx(
<g/>
1857	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc7
kmotrou	kmotra	k1gFnSc7
byla	být	k5eAaImAgFnS
císařovna	císařovna	k1gFnSc1
Evženie	Evženie	k1gFnSc1
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
císařovna	císařovna	k1gFnSc1
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tituly	titul	k1gInPc4
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc4
Viktorie	Viktoria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Frederik	Frederik	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
z	z	k7c2
Ansbachu	Ansbach	k1gInSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Sasko-Gothajská	sasko-gothajský	k2eAgNnPc4d1
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Augusta	August	k1gMnSc2
Anhaltsko-Zerbstská	Anhaltsko-Zerbstský	k2eAgFnSc5d1
</s>
<s>
Eduard	Eduard	k1gMnSc1
August	August	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meklenbursko-Střelický	Meklenbursko-Střelický	k2eAgInSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Meklenbursko-Střelický	Meklenbursko-Střelický	k2eAgMnSc1d1
</s>
<s>
Christiana	Christian	k1gMnSc4
Emilie	Emilie	k1gFnSc2
Schwarzbursko-Sondershausenská	Schwarzbursko-Sondershausenský	k2eAgFnSc1d1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Sasko-Hildburghausenský	Sasko-Hildburghausenský	k2eAgMnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Sasko-Hildburghausenská	Sasko-Hildburghausenský	k2eAgFnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Albertina	Albertin	k2eAgFnSc1d1
z	z	k7c2
Erbachu	Erbach	k1gInSc2
</s>
<s>
'	'	kIx"
<g/>
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
<g/>
'	'	kIx"
</s>
<s>
František	František	k1gMnSc1
Josiáš	Josiáš	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Žofie	Žofie	k1gFnSc2
Schwarzbursko-Rudolstadtská	Schwarzbursko-Rudolstadtský	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brunšvicko-Wolfenbüttelský	Brunšvicko-Wolfenbüttelský	k2eAgInSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Antonie	Antonie	k1gFnSc2
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Antonie	Antonie	k1gFnSc1
Amálie	Amálie	k1gFnSc2
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
XXIX	XXIX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuss	Reuss	k1gInSc1
Ebersdorf	Ebersdorf	k1gInSc4
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
XXIV	XXIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuss-Ebersdorf	Reuss-Ebersdorf	k1gInSc1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Teodora	Teodora	k1gFnSc1
z	z	k7c2
Castell-Remlingenu	Castell-Remlingen	k1gInSc2
</s>
<s>
Augusta	Augusta	k1gMnSc1
Reuss	Reussa	k1gFnPc2
Ebersdorf	Ebersdorf	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
August	August	k1gMnSc1
z	z	k7c2
Erbach-Schönbergu	Erbach-Schönberg	k1gInSc2
</s>
<s>
Karolina	Karolinum	k1gNnPc1
Ernestina	Ernestin	k2eAgNnPc1d1
Erbašsko-Schönberská	Erbašsko-Schönberský	k2eAgNnPc1d1
</s>
<s>
Ferdinanda	Ferdinand	k1gMnSc2
Henrietta	Henriett	k1gMnSc2
ze	z	k7c2
Stolberg-Gedernu	Stolberg-Gederna	k1gFnSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Victoria	Victorium	k1gNnSc2
of	of	k?
the	the	k?
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
McKusick	McKusick	k1gMnSc1
<g/>
,	,	kIx,
Victor	Victor	k1gMnSc1
A.	A.	kA
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
"	"	kIx"
<g/>
The	The	k1gFnSc1
Royal	Royal	k1gInSc1
Hemophilia	Hemophilia	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Scientific	Scientific	k1gMnSc1
American	American	k1gMnSc1
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
213	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
91	#num#	k4
<g/>
;	;	kIx,
Jones	Jones	k1gMnSc1
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
The	The	k1gFnSc1
Language	language	k1gFnSc2
of	of	k?
the	the	k?
Genes	Genes	k1gMnSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
:	:	kIx,
HarperCollins	HarperCollins	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
255020	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
69	#num#	k4
<g/>
;	;	kIx,
Jones	Jones	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Steve	Steve	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
In	In	k1gFnSc1
The	The	k1gMnSc1
Blood	Blood	k1gInSc1
<g/>
:	:	kIx,
God	God	k1gMnSc1
<g/>
,	,	kIx,
Genes	Genes	k1gMnSc1
and	and	k?
Destiny	Destina	k1gFnSc2
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
:	:	kIx,
HarperCollins	HarperCollins	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
255511	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
270	#num#	k4
<g/>
;	;	kIx,
Rushton	Rushton	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Alan	alan	k1gInSc1
R.	R.	kA
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Royal	Royal	k1gMnSc1
Maladies	Maladies	k1gMnSc1
<g/>
:	:	kIx,
Inherited	Inherited	k1gMnSc1
Diseases	Diseases	k1gMnSc1
in	in	k?
the	the	k?
Royal	Royal	k1gMnSc1
Houses	Houses	k1gMnSc1
of	of	k?
Europe	Europ	k1gMnSc5
<g/>
,	,	kIx,
Victoria	Victorium	k1gNnPc1
<g/>
,	,	kIx,
British	British	k1gInSc1
Columbia	Columbia	k1gFnSc1
<g/>
:	:	kIx,
Trafford	Trafford	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4251	#num#	k4
<g/>
-	-	kIx~
<g/>
6810	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
31	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
<g/>
↑	↑	k?
Longford	Longford	k1gMnSc1
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
Victoria	Victorium	k1gNnSc2
R.	R.	kA
<g/>
I.	I.	kA
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
:	:	kIx,
Weidenfeld	Weidenfeld	k1gMnSc1
&	&	k?
Nicolson	Nicolson	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
412	#num#	k4
<g/>
–	–	k?
<g/>
426	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
297	#num#	k4
<g/>
-	-	kIx~
<g/>
17001	#num#	k4
<g/>
-	-	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
5	#num#	k4
<g/>
↑	↑	k?
Hibbert	Hibbert	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
374	#num#	k4
<g/>
;	;	kIx,
Longford	Longford	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
491	#num#	k4
<g/>
;	;	kIx,
Marshall	Marshall	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
196	#num#	k4
<g/>
;	;	kIx,
St	St	kA
Aubyn	Aubyn	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
460	#num#	k4
<g/>
–	–	k?
<g/>
461	#num#	k4
<g/>
↑	↑	k?
Hibbert	Hibbert	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
437	#num#	k4
<g/>
–	–	k?
<g/>
438	#num#	k4
<g/>
;	;	kIx,
Longford	Longford	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
554	#num#	k4
<g/>
–	–	k?
<g/>
555	#num#	k4
<g/>
;	;	kIx,
St	St	kA
Aubyn	Aubyn	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
555	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Viktorie	Viktorie	k1gFnSc1
Britská	britský	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.thepeerage.com/p10065.htm#i100648	http://www.thepeerage.com/p10065.htm#i100648	k4
</s>
<s>
http://fotocollectie.huisdoorn.nl/lijst?personen=Victoria%2c+Queen	http://fotocollectie.huisdoorn.nl/lijst?personen=Victoria%2c+Queen	k1gInSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Viktorie	Viktorie	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britští	britský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
1707	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Stuartovna	Stuartovna	k1gFnSc1
(	(	kIx(
<g/>
1702	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1714	#num#	k4
<g/>
–	–	k?
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1727	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1760	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1952	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
Rovněž	rovněž	k9
vládce	vládce	k1gMnSc2
Irska	Irsko	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000701900	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118626876	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2144	#num#	k4
3562	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79017983	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500022428	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
95738652	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79017983	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Novověk	novověk	k1gInSc1
</s>
