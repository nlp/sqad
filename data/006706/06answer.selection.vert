<s>
Díky	díky	k7c3	díky
Huygensovu	Huygensův	k2eAgInSc3d1	Huygensův
principu	princip	k1gInSc3	princip
můžeme	moct	k5eAaImIp1nP	moct
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
vlnoplochu	vlnoplocha	k1gFnSc4	vlnoplocha
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
známá	známý	k2eAgFnSc1d1	známá
její	její	k3xOp3gFnSc1	její
poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
předcházejícím	předcházející	k2eAgInSc6d1	předcházející
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
