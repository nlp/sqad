<s>
Jmeniny	jmeniny	k1gFnPc1	jmeniny
nebo	nebo	k8xC	nebo
také	také	k9	také
(	(	kIx(	(
<g/>
jmenný	jmenný	k2eAgInSc4d1	jmenný
<g/>
)	)	kIx)	)
svátek	svátek	k1gInSc4	svátek
je	být	k5eAaImIp3nS	být
zvyk	zvyk	k1gInSc1	zvyk
oslavovat	oslavovat	k5eAaImF	oslavovat
den	den	k1gInSc4	den
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
svatému	svatý	k2eAgNnSc3d1	svaté
nebo	nebo	k8xC	nebo
blahoslavenému	blahoslavený	k2eAgNnSc3d1	blahoslavené
z	z	k7c2	z
církevního	církevní	k2eAgInSc2d1	církevní
kalendáře	kalendář	k1gInSc2	kalendář
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
je	být	k5eAaImIp3nS	být
nějakou	nějaký	k3yIgFnSc7	nějaký
autoritou	autorita	k1gFnSc7	autorita
připsáno	připsán	k2eAgNnSc1d1	připsáno
oslavencovo	oslavencův	k2eAgNnSc1d1	oslavencovo
rodné	rodný	k2eAgNnSc1d1	rodné
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
katolických	katolický	k2eAgFnPc6d1	katolická
a	a	k8xC	a
pravoslavných	pravoslavný	k2eAgFnPc6d1	pravoslavná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
větší	veliký	k2eAgInSc4d2	veliký
význam	význam	k1gInSc4	význam
než	než	k8xS	než
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
katolické	katolický	k2eAgFnSc2d1	katolická
praxe	praxe	k1gFnSc2	praxe
uctívání	uctívání	k1gNnSc2	uctívání
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgMnSc1	každý
světec	světec	k1gMnSc1	světec
(	(	kIx(	(
<g/>
blahoslavený	blahoslavený	k2eAgMnSc1d1	blahoslavený
<g/>
,	,	kIx,	,
ctihodný	ctihodný	k2eAgMnSc1d1	ctihodný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
připomínán	připomínat	k5eAaImNgInS	připomínat
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
každý	každý	k3xTgMnSc1	každý
věřící	věřící	k1gMnSc1	věřící
dostává	dostávat	k5eAaImIp3nS	dostávat
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
rodné	rodný	k2eAgNnSc4d1	rodné
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
"	"	kIx"	"
<g/>
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejen	nejen	k6eAd1	nejen
označuje	označovat	k5eAaImIp3nS	označovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
jmenovat	jmenovat	k5eAaBmF	jmenovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
i	i	k9	i
volbou	volba	k1gFnSc7	volba
jeho	jeho	k3xOp3gMnSc2	jeho
osobního	osobní	k2eAgMnSc2d1	osobní
patrona	patron	k1gMnSc2	patron
<g/>
,	,	kIx,	,
průvodce	průvodce	k1gMnSc2	průvodce
a	a	k8xC	a
životního	životní	k2eAgInSc2d1	životní
vzoru	vzor	k1gInSc2	vzor
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
když	když	k8xS	když
církev	církev	k1gFnSc1	církev
slaví	slavit	k5eAaImIp3nS	slavit
památku	památka	k1gFnSc4	památka
daného	daný	k2eAgInSc2d1	daný
svatého	svatý	k2eAgInSc2d1	svatý
<g/>
,	,	kIx,	,
slaví	slavit	k5eAaImIp3nP	slavit
jej	on	k3xPp3gMnSc4	on
i	i	k9	i
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
nesou	nést	k5eAaImIp3nP	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
den	den	k1gInSc4	den
jejich	jejich	k3xOp3gMnSc2	jejich
patrona	patron	k1gMnSc2	patron
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
zvykem	zvyk	k1gInSc7	zvyk
dávat	dávat	k5eAaImF	dávat
při	pře	k1gFnSc4	pře
křtu	křest	k1gInSc2	křest
i	i	k8xC	i
jmen	jméno	k1gNnPc2	jméno
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
katolických	katolický	k2eAgFnPc6d1	katolická
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
přibírá	přibírat	k5eAaImIp3nS	přibírat
ještě	ještě	k6eAd1	ještě
další	další	k2eAgNnSc4d1	další
jméno	jméno	k1gNnSc4	jméno
i	i	k9	i
při	při	k7c6	při
biřmování	biřmování	k1gNnSc6	biřmování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
naopak	naopak	k6eAd1	naopak
tento	tento	k3xDgInSc4	tento
zvyk	zvyk	k1gInSc4	zvyk
neznají	znát	k5eNaImIp3nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
nositelům	nositel	k1gMnPc3	nositel
jména	jméno	k1gNnPc4	jméno
popřát	popřát	k5eAaPmF	popřát
a	a	k8xC	a
případně	případně	k6eAd1	případně
je	být	k5eAaImIp3nS	být
i	i	k9	i
obdarovat	obdarovat	k5eAaPmF	obdarovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
množství	množství	k1gNnSc3	množství
světců	světec	k1gMnPc2	světec
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
rodných	rodný	k2eAgNnPc2d1	rodné
jmen	jméno	k1gNnPc2	jméno
příslušných	příslušný	k2eAgNnPc2d1	příslušné
k	k	k7c3	k
více	hodně	k6eAd2	hodně
dnům	den	k1gInPc3	den
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
dnům	den	k1gInPc3	den
připadá	připadat	k5eAaImIp3nS	připadat
řada	řada	k1gFnSc1	řada
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
publikuje	publikovat	k5eAaBmIp3nS	publikovat
oficiální	oficiální	k2eAgInSc1d1	oficiální
seznam	seznam	k1gInSc1	seznam
jmenin	jmeniny	k1gFnPc2	jmeniny
Královská	královský	k2eAgFnSc1d1	královská
švédská	švédský	k2eAgFnSc1d1	švédská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
občanský	občanský	k2eAgInSc1d1	občanský
kalendář	kalendář	k1gInSc1	kalendář
původně	původně	k6eAd1	původně
sice	sice	k8xC	sice
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
církevního	církevní	k2eAgInSc2d1	církevní
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
doznal	doznat	k5eAaPmAgMnS	doznat
mnoha	mnoho	k4c2	mnoho
změn	změna	k1gFnPc2	změna
podle	podle	k7c2	podle
vůle	vůle	k1gFnSc2	vůle
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
:	:	kIx,	:
některá	některý	k3yIgNnPc1	některý
jména	jméno	k1gNnPc1	jméno
byla	být	k5eAaImAgNnP	být
vyškrtnuta	vyškrtnout	k5eAaPmNgNnP	vyškrtnout
<g/>
,	,	kIx,	,
jiná	jiná	k1gFnSc1	jiná
dodána	dodán	k2eAgFnSc1d1	dodána
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
přesunuta	přesunut	k2eAgFnSc1d1	přesunuta
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
duplicity	duplicita	k1gFnPc1	duplicita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
původně	původně	k6eAd1	původně
několikrát	několikrát	k6eAd1	několikrát
uvedeno	uvést	k5eAaPmNgNnS	uvést
jméno	jméno	k1gNnSc4	jméno
Karel	Karla	k1gFnPc2	Karla
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
různých	různý	k2eAgMnPc2d1	různý
světců	světec	k1gMnPc2	světec
toho	ten	k3xDgNnSc2	ten
jména	jméno	k1gNnSc2	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
kalendáři	kalendář	k1gInSc6	kalendář
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
dne	den	k1gInSc2	den
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
většinou	většinou	k6eAd1	většinou
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
zpravidla	zpravidla	k6eAd1	zpravidla
jména	jméno	k1gNnPc4	jméno
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
