<s>
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
telegrafii	telegrafie	k1gFnSc6	telegrafie
<g/>
.	.	kIx.	.
</s>
<s>
Kóduje	kódovat	k5eAaBmIp3nS	kódovat
znaky	znak	k1gInPc4	znak
latinské	latinský	k2eAgFnSc2d1	Latinská
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
číslice	číslice	k1gFnPc4	číslice
a	a	k8xC	a
speciální	speciální	k2eAgInPc4d1	speciální
znaky	znak	k1gInPc4	znak
do	do	k7c2	do
kombinací	kombinace	k1gFnPc2	kombinace
krátkých	krátký	k2eAgInPc2d1	krátký
a	a	k8xC	a
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
signálů	signál	k1gInPc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přenášet	přenášet	k5eAaImF	přenášet
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
jednodušším	jednoduchý	k2eAgInSc7d2	jednodušší
způsobem	způsob	k1gInSc7	způsob
než	než	k8xS	než
všechny	všechen	k3xTgInPc4	všechen
znaky	znak	k1gInPc4	znak
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
morseovky	morseovka	k1gFnSc2	morseovka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
zvukový	zvukový	k2eAgInSc4d1	zvukový
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc4d1	elektrický
signál	signál	k1gInSc4	signál
(	(	kIx(	(
<g/>
telegraf	telegraf	k1gInSc1	telegraf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
optický	optický	k2eAgInSc4d1	optický
signál	signál	k1gInSc4	signál
(	(	kIx(	(
<g/>
signalizace	signalizace	k1gFnSc1	signalizace
vlajkami	vlajka	k1gFnPc7	vlajka
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc4	záznam
na	na	k7c4	na
papír	papír	k1gInSc4	papír
pomocí	pomocí	k7c2	pomocí
teček	tečka	k1gFnPc2	tečka
a	a	k8xC	a
čárek	čárka	k1gFnPc2	čárka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgNnSc1d3	nejběžnější
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
amatérské	amatérský	k2eAgFnSc6d1	amatérská
radiotelegrafii	radiotelegrafie	k1gFnSc6	radiotelegrafie
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
komunikace	komunikace	k1gFnSc2	komunikace
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
60	[number]	k4	60
do	do	k7c2	do
250	[number]	k4	250
znaků	znak	k1gInPc2	znak
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Kódování	kódování	k1gNnSc1	kódování
abecedy	abeceda	k1gFnSc2	abeceda
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
americký	americký	k2eAgMnSc1d1	americký
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
Samuel	Samuel	k1gMnSc1	Samuel
Morse	Morse	k1gMnSc1	Morse
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
také	také	k9	také
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
první	první	k4xOgNnSc1	první
telegrafické	telegrafický	k2eAgNnSc1d1	telegrafické
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
Washingtonem	Washington	k1gInSc7	Washington
a	a	k8xC	a
Baltimorem	Baltimore	k1gInSc7	Baltimore
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
abeceda	abeceda	k1gFnSc1	abeceda
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
zdokonalena	zdokonalit	k5eAaPmNgFnS	zdokonalit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
kódování	kódování	k1gNnPc2	kódování
byly	být	k5eAaImAgInP	být
znaky	znak	k1gInPc1	znak
voleny	volit	k5eAaImNgInP	volit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nejfrekventovanějším	frekventovaný	k2eAgNnPc3d3	nejfrekventovanější
písmenům	písmeno	k1gNnPc3	písmeno
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
nejkratší	krátký	k2eAgFnPc1d3	nejkratší
sekvence	sekvence	k1gFnPc1	sekvence
teček	tečka	k1gFnPc2	tečka
a	a	k8xC	a
čárek	čárka	k1gFnPc2	čárka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
nejfrekventovanější	frekventovaný	k2eAgNnSc1d3	nejfrekventovanější
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
E	E	kA	E
<g/>
"	"	kIx"	"
znak	znak	k1gInSc1	znak
"	"	kIx"	"
<g/>
•	•	k?	•
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tečka	tečka	k1gFnSc1	tečka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jeden	jeden	k4xCgInSc4	jeden
jediný	jediný	k2eAgInSc4d1	jediný
krátký	krátký	k2eAgInSc4d1	krátký
signál	signál	k1gInSc4	signál
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Na	na	k7c6	na
podobné	podobný	k2eAgFnSc6d1	podobná
optimalizaci	optimalizace	k1gFnSc6	optimalizace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejčastěji	často	k6eAd3	často
používaným	používaný	k2eAgInPc3d1	používaný
znakům	znak	k1gInPc3	znak
je	být	k5eAaImIp3nS	být
přiřazen	přiřazen	k2eAgInSc4d1	přiřazen
nejkratší	krátký	k2eAgInSc4d3	nejkratší
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
také	také	k6eAd1	také
Huffmanovo	Huffmanův	k2eAgNnSc1d1	Huffmanovo
kódování	kódování	k1gNnSc1	kódování
užívané	užívaný	k2eAgNnSc1d1	užívané
při	při	k7c6	při
bezeztrátové	bezeztrátový	k2eAgFnSc6d1	bezeztrátová
komprimaci	komprimace	k1gFnSc6	komprimace
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Morseovka	morseovka	k1gFnSc1	morseovka
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
k	k	k7c3	k
vysílání	vysílání	k1gNnSc3	vysílání
a	a	k8xC	a
příjmu	příjem	k1gInSc3	příjem
zpráv	zpráva	k1gFnPc2	zpráva
telegrafem	telegraf	k1gInSc7	telegraf
(	(	kIx(	(
<g/>
elektrický	elektrický	k2eAgInSc4d1	elektrický
signál	signál	k1gInSc4	signál
po	po	k7c6	po
drátech	drát	k1gInPc6	drát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
radiotelegrafem	radiotelegraf	k1gInSc7	radiotelegraf
(	(	kIx(	(
<g/>
přenos	přenos	k1gInSc1	přenos
radiovými	radiový	k2eAgFnPc7d1	radiová
vlnami	vlna	k1gFnPc7	vlna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přenášená	přenášený	k2eAgFnSc1d1	přenášená
zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
telegram	telegram	k1gInSc1	telegram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
způsoby	způsob	k1gInPc1	způsob
komunikace	komunikace	k1gFnSc2	komunikace
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
telefonií	telefonie	k1gFnSc7	telefonie
<g/>
,	,	kIx,	,
dálnopisem	dálnopis	k1gInSc7	dálnopis
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
také	také	k9	také
internetovými	internetový	k2eAgFnPc7d1	internetová
službami	služba	k1gFnPc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
Morseovka	morseovka	k1gFnSc1	morseovka
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
používána	používat	k5eAaImNgFnS	používat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
nenáročnost	nenáročnost	k1gFnSc4	nenáročnost
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
signalizace	signalizace	k1gFnSc2	signalizace
v	v	k7c6	v
nouzových	nouzový	k2eAgInPc6d1	nouzový
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
při	při	k7c6	při
výcviku	výcvik	k1gInSc6	výcvik
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
námořníků	námořník	k1gMnPc2	námořník
<g/>
,	,	kIx,	,
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
ji	on	k3xPp3gFnSc4	on
používaly	používat	k5eAaImAgFnP	používat
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc4	prostředek
tísňového	tísňový	k2eAgNnSc2d1	tísňové
volání	volání	k1gNnSc2	volání
všechny	všechen	k3xTgFnPc4	všechen
lodě	loď	k1gFnPc1	loď
o	o	k7c6	o
výtlaku	výtlak	k1gInSc6	výtlak
větším	veliký	k2eAgInSc6d2	veliký
než	než	k8xS	než
300	[number]	k4	300
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
globálním	globální	k2eAgInSc7d1	globální
námořním	námořní	k2eAgInSc7d1	námořní
a	a	k8xC	a
tísňovým	tísňový	k2eAgInSc7d1	tísňový
systémem	systém	k1gInSc7	systém
GMDSS	GMDSS	kA	GMDSS
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nouzové	nouzový	k2eAgNnSc4d1	nouzové
volání	volání	k1gNnSc4	volání
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
známý	známý	k2eAgInSc1d1	známý
signál	signál	k1gInSc1	signál
SOS	sos	k1gInSc1	sos
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
se	se	k3xPyFc4	se
vysílá	vysílat	k5eAaImIp3nS	vysílat
bez	bez	k7c2	bez
mezer	mezera	k1gFnPc2	mezera
mezi	mezi	k7c7	mezi
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
značky	značka	k1gFnSc2	značka
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
výraznosti	výraznost	k1gFnSc3	výraznost
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
tradované	tradovaný	k2eAgNnSc1d1	tradované
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
znamenat	znamenat	k5eAaImF	znamenat
"	"	kIx"	"
<g/>
save	save	k6eAd1	save
our	our	k?	our
souls	souls	k1gInSc1	souls
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
spaste	spasit	k5eAaPmRp2nP	spasit
naše	náš	k3xOp1gNnSc4	náš
duše	duše	k1gFnSc1	duše
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
"	"	kIx"	"
<g/>
Spaste	spasit	k5eAaPmRp2nP	spasit
Od	od	k7c2	od
Smerti	Smert	k1gMnPc1	Smert
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
chybné	chybný	k2eAgNnSc1d1	chybné
<g/>
,	,	kIx,	,
sousloví	sousloví	k1gNnSc1	sousloví
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
jen	jen	k6eAd1	jen
jako	jako	k8xC	jako
mnemotechnická	mnemotechnický	k2eAgFnSc1d1	mnemotechnická
pomůcka	pomůcka	k1gFnSc1	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
SOS	sos	k1gInSc4	sos
znělo	znět	k5eAaImAgNnS	znět
i	i	k9	i
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
potápějícího	potápějící	k2eAgInSc2d1	potápějící
se	se	k3xPyFc4	se
Titanicu	Titanicus	k1gInSc2	Titanicus
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
pouze	pouze	k6eAd1	pouze
jedenkrát	jedenkrát	k6eAd1	jedenkrát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
ostatních	ostatní	k2eAgNnPc6d1	ostatní
voláních	volání	k1gNnPc6	volání
použil	použít	k5eAaPmAgInS	použít
Titanic	Titanic	k1gInSc1	Titanic
starší	starý	k2eAgFnSc4d2	starší
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
rozšířenější	rozšířený	k2eAgInSc1d2	rozšířenější
nouzový	nouzový	k2eAgInSc1d1	nouzový
signál	signál	k1gInSc1	signál
CQD	CQD	kA	CQD
(	(	kIx(	(
<g/>
come	comat	k5eAaPmIp3nS	comat
quick	quick	k1gMnSc1	quick
<g/>
,	,	kIx,	,
danger	danger	k1gMnSc1	danger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nouzové	nouzový	k2eAgNnSc4d1	nouzové
volání	volání	k1gNnSc4	volání
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
signál	signál	k1gInSc1	signál
QRRR	QRRR	kA	QRRR
<g/>
.	.	kIx.	.
</s>
<s>
Morseovka	morseovka	k1gFnSc1	morseovka
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používá	používat	k5eAaImIp3nS	používat
mj.	mj.	kA	mj.
pro	pro	k7c4	pro
dorozumívání	dorozumívání	k1gNnSc4	dorozumívání
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
tělesně	tělesně	k6eAd1	tělesně
postiženými	postižený	k1gMnPc7	postižený
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
pohybu	pohyb	k1gInSc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
Morseovky	morseovka	k1gFnSc2	morseovka
v	v	k7c6	v
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
dobrém	dobrý	k2eAgNnSc6d1	dobré
ovládání	ovládání	k1gNnSc6	ovládání
je	být	k5eAaImIp3nS	být
kódování	kódování	k1gNnSc1	kódování
textových	textový	k2eAgFnPc2d1	textová
zpráv	zpráva	k1gFnPc2	zpráva
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
pomocí	pomocí	k7c2	pomocí
"	"	kIx"	"
<g/>
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
"	"	kIx"	"
na	na	k7c6	na
mobilu	mobil	k1gInSc6	mobil
<g/>
.	.	kIx.	.
</s>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
jen	jen	k9	jen
2	[number]	k4	2
tlačítka	tlačítko	k1gNnSc2	tlačítko
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
1	[number]	k4	1
tlačítko	tlačítko	k1gNnSc4	tlačítko
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
stiskem	stisk	k1gInSc7	stisk
<g/>
.	.	kIx.	.
</s>
<s>
Příjemci	příjemce	k1gMnSc3	příjemce
je	být	k5eAaImIp3nS	být
zpráva	zpráva	k1gFnSc1	zpráva
ovšem	ovšem	k9	ovšem
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
příchozí	příchozí	k1gFnSc1	příchozí
textové	textový	k2eAgFnSc2d1	textová
zprávy	zpráva	k1gFnSc2	zpráva
převedeny	převést	k5eAaPmNgInP	převést
do	do	k7c2	do
Morseovky	morseovka	k1gFnSc2	morseovka
a	a	k8xC	a
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
pomocí	pomocí	k7c2	pomocí
vibrací	vibrace	k1gFnPc2	vibrace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
"	"	kIx"	"
<g/>
hands-free	handsree	k1gNnSc1	hands-free
čtení	čtení	k1gNnSc2	čtení
<g/>
"	"	kIx"	"
textových	textový	k2eAgFnPc2d1	textová
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Morseovka	morseovka	k1gFnSc1	morseovka
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
šifrovacích	šifrovací	k2eAgFnPc6d1	šifrovací
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
známá	známý	k2eAgFnSc1d1	známá
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
pěkně	pěkně	k6eAd1	pěkně
kódovat	kódovat	k5eAaBmF	kódovat
mnoha	mnoho	k4c7	mnoho
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
(	(	kIx(	(
<g/>
stačí	stačit	k5eAaBmIp3nP	stačit
rozlišit	rozlišit	k5eAaPmF	rozlišit
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
objektů	objekt	k1gInPc2	objekt
<g/>
:	:	kIx,	:
tečka	tečka	k1gFnSc1	tečka
<g/>
,	,	kIx,	,
čárka	čárka	k1gFnSc1	čárka
<g/>
,	,	kIx,	,
oddělovač	oddělovač	k1gInSc1	oddělovač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
Morseovky	morseovka	k1gFnSc2	morseovka
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
jako	jako	k9	jako
mnemotechnická	mnemotechnický	k2eAgFnSc1d1	mnemotechnická
pomůcka	pomůcka	k1gFnSc1	pomůcka
používají	používat	k5eAaImIp3nP	používat
pomocná	pomocný	k2eAgNnPc4d1	pomocné
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
počáteční	počáteční	k2eAgNnSc4d1	počáteční
písmeno	písmeno	k1gNnSc4	písmeno
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
slabik	slabika	k1gFnPc2	slabika
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
si	se	k3xPyFc3	se
kód	kód	k1gInSc4	kód
písmene	písmeno	k1gNnSc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
písmen	písmeno	k1gNnPc2	písmeno
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pomocných	pomocný	k2eAgNnPc2d1	pomocné
slov	slovo	k1gNnPc2	slovo
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
F	F	kA	F
–	–	k?	–
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
/	/	kIx~	/
<g/>
Fifipírko	Fifipírka	k1gFnSc5	Fifipírka
<g/>
,	,	kIx,	,
I	i	k9	i
–	–	k?	–
ibis	ibis	k1gMnSc1	ibis
<g/>
/	/	kIx~	/
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
,	,	kIx,	,
S	s	k7c7	s
-	-	kIx~	-
Sekera	Sekera	k1gMnSc1	Sekera
<g/>
/	/	kIx~	/
<g/>
Sobota	sobota	k1gFnSc1	sobota
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
lidové	lidový	k2eAgFnSc3d1	lidová
tvořivosti	tvořivost	k1gFnSc3	tvořivost
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
skupiny	skupina	k1gFnPc1	skupina
pomocných	pomocný	k2eAgNnPc2d1	pomocné
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
alkoholická	alkoholický	k2eAgFnSc1d1	alkoholická
morseovka	morseovka	k1gFnSc1	morseovka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
A	a	k9	a
=	=	kIx~	=
absťák	absťák	k1gMnSc1	absťák
<g/>
,	,	kIx,	,
B	B	kA	B
=	=	kIx~	=
blít	blít	k5eAaImF	blít
až	až	k9	až
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
C	C	kA	C
=	=	kIx~	=
cíl	cíl	k1gInSc1	cíl
je	být	k5eAaImIp3nS	být
výčep	výčep	k1gInSc4	výčep
<g/>
,	,	kIx,	,
...	...	k?	...
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
až	až	k9	až
kontraproduktivní	kontraproduktivní	k2eAgNnSc1d1	kontraproduktivní
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rychlostí	rychlost	k1gFnPc2	rychlost
příjmu	příjem	k1gInSc2	příjem
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
nebo	nebo	k8xC	nebo
radioamatéry	radioamatér	k1gMnPc4	radioamatér
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
takových	takový	k3xDgFnPc6	takový
rychlostech	rychlost	k1gFnPc6	rychlost
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
si	se	k3xPyFc3	se
automaticky	automaticky	k6eAd1	automaticky
pamatovat	pamatovat	k5eAaImF	pamatovat
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc1	jaký
sekvenci	sekvence	k1gFnSc4	sekvence
tónů	tón	k1gInPc2	tón
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jaké	jaký	k3yIgNnSc4	jaký
písmenko	písmenko	k1gNnSc4	písmenko
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
pomocných	pomocný	k2eAgNnPc2d1	pomocné
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
spíše	spíše	k9	spíše
matoucí	matoucí	k2eAgMnSc1d1	matoucí
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
snáze	snadno	k6eAd2	snadno
naučí	naučit	k5eAaPmIp3nS	naučit
přijímat	přijímat	k5eAaImF	přijímat
Morseovy	Morseův	k2eAgFnSc2d1	Morseova
značky	značka	k1gFnSc2	značka
sluchem	sluch	k1gInSc7	sluch
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
pomocná	pomocný	k2eAgFnSc1d1	pomocná
slova	slovo	k1gNnSc2	slovo
nezná	znát	k5eNaImIp3nS	znát
<g/>
,	,	kIx,	,
než	než	k8xS	než
ten	ten	k3xDgMnSc1	ten
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
naučil	naučit	k5eAaPmAgMnS	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
pomůckou	pomůcka	k1gFnSc7	pomůcka
je	být	k5eAaImIp3nS	být
barevná	barevný	k2eAgFnSc1d1	barevná
tabulka	tabulka	k1gFnSc1	tabulka
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Používáním	používání	k1gNnSc7	používání
přejde	přejít	k5eAaPmIp3nS	přejít
pořadí	pořadí	k1gNnSc4	pořadí
znaků	znak	k1gInPc2	znak
lépe	dobře	k6eAd2	dobře
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
nezatěžuje	zatěžovat	k5eNaImIp3nS	zatěžovat
neustálým	neustálý	k2eAgNnSc7d1	neustálé
opakováním	opakování	k1gNnSc7	opakování
pomocných	pomocný	k2eAgNnPc2d1	pomocné
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Zkrácené	zkrácený	k2eAgInPc4d1	zkrácený
kódy	kód	k1gInPc4	kód
číslic	číslice	k1gFnPc2	číslice
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
užít	užít	k5eAaPmF	užít
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysíláme	vysílat	k5eAaImIp1nP	vysílat
číslice	číslice	k1gFnPc4	číslice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
co	co	k8xS	co
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rychlost	rychlost	k1gFnSc1	rychlost
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
při	při	k7c6	při
radioamatérských	radioamatérský	k2eAgFnPc6d1	radioamatérská
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Zkrácené	zkrácený	k2eAgInPc1d1	zkrácený
kódy	kód	k1gInPc1	kód
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
neužívají	užívat	k5eNaImIp3nP	užívat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
hrozí	hrozit	k5eAaImIp3nS	hrozit
jejich	jejich	k3xOp3gFnSc1	jejich
nechtěná	chtěný	k2eNgFnSc1d1	nechtěná
záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
číslici	číslice	k1gFnSc4	číslice
uvnitř	uvnitř	k7c2	uvnitř
volacího	volací	k2eAgInSc2d1	volací
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
znaky	znak	k1gInPc1	znak
patřící	patřící	k2eAgInPc1d1	patřící
do	do	k7c2	do
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
morseovy	morseův	k2eAgFnSc2d1	morseova
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
zavináče	zavináč	k1gInSc2	zavináč
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přidán	přidat	k5eAaPmNgInS	přidat
do	do	k7c2	do
Morseovy	Morseův	k2eAgFnSc2d1	Morseova
abecedy	abeceda	k1gFnSc2	abeceda
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2003	[number]	k4	2003
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
ITU	ITU	kA	ITU
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgInSc7	první
přidaným	přidaný	k2eAgInSc7d1	přidaný
znakem	znak	k1gInSc7	znak
po	po	k7c6	po
několika	několik	k4yIc6	několik
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInSc1d1	předchozí
znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
přidán	přidat	k5eAaPmNgInS	přidat
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc4	znak
pro	pro	k7c4	pro
zavináč	zavináč	k1gInSc4	zavináč
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
kódů	kód	k1gInPc2	kód
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
A	a	k8xC	a
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
at	at	k?	at
commercial	commercial	k1gInSc1	commercial
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
kód	kód	k1gInSc1	kód
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
"	"	kIx"	"
<g/>
commat	commat	k5eAaPmF	commat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
commercial	commercial	k1gMnSc1	commercial
at	at	k?	at
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
formální	formální	k2eAgInSc1d1	formální
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
pro	pro	k7c4	pro
zavináč	zavináč	k1gInSc4	zavináč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
love	lov	k1gInSc5	lov
you	you	k?	you
SOS	sos	k1gInSc4	sos
Její	její	k3xOp3gNnSc1	její
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
a	a	k8xC	a
vhodné	vhodný	k2eAgNnSc1d1	vhodné
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
učí	učit	k5eAaImIp3nP	učit
přijímat	přijímat	k5eAaImF	přijímat
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
postupujeme	postupovat	k5eAaImIp1nP	postupovat
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
políčko	políčko	k1gNnSc1	políčko
představuje	představovat	k5eAaImIp3nS	představovat
část	část	k1gFnSc4	část
jednoho	jeden	k4xCgInSc2	jeden
vyslaného	vyslaný	k2eAgInSc2d1	vyslaný
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
(	(	kIx(	(
<g/>
tečka	tečka	k1gFnSc1	tečka
nebo	nebo	k8xC	nebo
čárka	čárka	k1gFnSc1	čárka
<g/>
)	)	kIx)	)
a	a	k8xC	a
sloupce	sloupec	k1gInPc1	sloupec
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
tečky	tečka	k1gFnSc2	tečka
či	či	k8xC	či
čárky	čárka	k1gFnSc2	čárka
ve	v	k7c6	v
vysílaném	vysílaný	k2eAgInSc6d1	vysílaný
znaku	znak	k1gInSc6	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
představují	představovat	k5eAaImIp3nP	představovat
modrá	modrý	k2eAgFnSc1d1	modrá
pole	pole	k1gFnSc1	pole
čárku	čárka	k1gFnSc4	čárka
a	a	k8xC	a
žlutá	žlutat	k5eAaImIp3nS	žlutat
tečku	tečka	k1gFnSc4	tečka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Váš	váš	k3xOp2gMnSc1	váš
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
kaskádové	kaskádový	k2eAgInPc4d1	kaskádový
styly	styl	k1gInPc4	styl
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
nevidíte	vidět	k5eNaImIp2nP	vidět
barevná	barevný	k2eAgNnPc1d1	barevné
políčka	políčko	k1gNnPc1	políčko
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vězte	vědět	k5eAaImRp2nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
modré	modré	k1gNnSc1	modré
(	(	kIx(	(
<g/>
čárka	čárka	k1gFnSc1	čárka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
políčko	políčko	k1gNnSc4	políčko
horní	horní	k2eAgFnSc2d1	horní
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
,	,	kIx,	,
X	X	kA	X
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
žluté	žlutý	k2eAgNnSc1d1	žluté
(	(	kIx(	(
<g/>
tečka	tečka	k1gFnSc1	tečka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
políčko	políčko	k1gNnSc4	políčko
spodní	spodní	k2eAgNnSc4d1	spodní
(	(	kIx(	(
<g/>
E	e	k0	e
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
Z	Z	kA	Z
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tabulku	tabulka	k1gFnSc4	tabulka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dále	daleko	k6eAd2	daleko
rozšířit	rozšířit	k5eAaPmF	rozšířit
o	o	k7c4	o
speciální	speciální	k2eAgInPc4d1	speciální
či	či	k8xC	či
národní	národní	k2eAgInPc4d1	národní
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
číslice	číslice	k1gFnSc1	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
podstatně	podstatně	k6eAd1	podstatně
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
ale	ale	k9	ale
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
její	její	k3xOp3gInSc4	její
formát	formát	k1gInSc4	formát
<g/>
.	.	kIx.	.
</s>
<s>
Přijato	přijat	k2eAgNnSc1d1	přijato
<g/>
:	:	kIx,	:
-	-	kIx~	-
<g/>
..	..	k?	..
<g/>
-	-	kIx~	-
Rozdělíme	rozdělit	k5eAaPmIp1nP	rozdělit
přijatý	přijatý	k2eAgInSc4d1	přijatý
znak	znak	k1gInSc4	znak
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Postupujeme	postupovat	k5eAaImIp1nP	postupovat
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Vybereme	vybrat	k5eAaPmIp1nP	vybrat
políčko	políčko	k1gNnSc4	políčko
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
sloupci	sloupec	k1gInSc6	sloupec
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
čárka	čárka	k1gFnSc1	čárka
<g/>
)	)	kIx)	)
-	-	kIx~	-
T	T	kA	T
Dále	daleko	k6eAd2	daleko
již	již	k6eAd1	již
postupujeme	postupovat	k5eAaImIp1nP	postupovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
řádku	řádek	k1gInSc6	řádek
s	s	k7c7	s
vybraným	vybraný	k2eAgNnSc7d1	vybrané
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Vybereme	vybrat	k5eAaPmIp1nP	vybrat
políčko	políčko	k1gNnSc4	políčko
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
sloupci	sloupec	k1gInSc6	sloupec
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
tečka	tečka	k1gFnSc1	tečka
<g/>
)	)	kIx)	)
-	-	kIx~	-
N	N	kA	N
Vybereme	vybrat	k5eAaPmIp1nP	vybrat
políčko	políčko	k1gNnSc4	políčko
v	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
sloupci	sloupec	k1gInSc6	sloupec
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
třetí	třetí	k4xOgFnSc2	třetí
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
tečka	tečka	k1gFnSc1	tečka
<g/>
)	)	kIx)	)
-	-	kIx~	-
D	D	kA	D
Vybereme	vybrat	k5eAaPmIp1nP	vybrat
políčko	políčko	k1gNnSc4	políčko
v	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
sloupci	sloupec	k1gInSc6	sloupec
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
čárka	čárka	k1gFnSc1	čárka
<g/>
)	)	kIx)	)
-	-	kIx~	-
X	X	kA	X
Zpracovali	zpracovat	k5eAaPmAgMnP	zpracovat
jsme	být	k5eAaImIp1nP	být
všechny	všechen	k3xTgFnPc4	všechen
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
X	X	kA	X
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přijatému	přijatý	k2eAgInSc3d1	přijatý
znaku	znak	k1gInSc3	znak
-	-	kIx~	-
<g/>
..	..	k?	..
<g/>
-	-	kIx~	-
Při	při	k7c6	při
vysílání	vysílání	k1gNnSc6	vysílání
Morseovy	Morseův	k2eAgFnSc2d1	Morseova
abecedy	abeceda	k1gFnSc2	abeceda
v	v	k7c6	v
akustické	akustický	k2eAgFnSc6d1	akustická
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
následující	následující	k2eAgNnPc1d1	následující
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
:	:	kIx,	:
základní	základní	k2eAgFnSc7d1	základní
časovou	časový	k2eAgFnSc7d1	časová
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
tečky	tečka	k1gFnSc2	tečka
čárka	čárka	k1gFnSc1	čárka
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
jako	jako	k8xC	jako
tři	tři	k4xCgFnPc4	tři
tečky	tečka	k1gFnPc4	tečka
zvuková	zvukový	k2eAgFnSc1d1	zvuková
pauza	pauza	k1gFnSc1	pauza
uvnitř	uvnitř	k7c2	uvnitř
značky	značka	k1gFnSc2	značka
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
tečka	tečka	k1gFnSc1	tečka
zvuková	zvukový	k2eAgFnSc1d1	zvuková
pauza	pauza	k1gFnSc1	pauza
mezi	mezi	k7c7	mezi
značkami	značka	k1gFnPc7	značka
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
čárka	čárka	k1gFnSc1	čárka
Tyto	tento	k3xDgInPc4	tento
poměry	poměr	k1gInPc4	poměr
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
sluchem	sluch	k1gInSc7	sluch
zcela	zcela	k6eAd1	zcela
spolehlivě	spolehlivě	k6eAd1	spolehlivě
rozlišit	rozlišit	k5eAaPmF	rozlišit
tečku	tečka	k1gFnSc4	tečka
od	od	k7c2	od
čárky	čárka	k1gFnSc2	čárka
i	i	k8xC	i
druh	druh	k1gInSc1	druh
akustické	akustický	k2eAgFnSc2d1	akustická
pauzy	pauza	k1gFnSc2	pauza
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgInSc4d1	další
<g/>
,	,	kIx,	,
poměr	poměr	k1gInSc1	poměr
celkové	celkový	k2eAgFnSc2d1	celková
doby	doba	k1gFnSc2	doba
trvání	trvání	k1gNnSc2	trvání
vysílání	vysílání	k1gNnSc1	vysílání
tečky	tečka	k1gFnSc2	tečka
nebo	nebo	k8xC	nebo
čárky	čárka	k1gFnSc2	čárka
včetně	včetně	k7c2	včetně
pauzy	pauza	k1gFnSc2	pauza
uvnitř	uvnitř	k7c2	uvnitř
značky	značka	k1gFnSc2	značka
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
udržet	udržet	k5eAaPmF	udržet
konstantní	konstantní	k2eAgFnSc1d1	konstantní
rychlost	rychlost	k1gFnSc1	rychlost
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
vysílání	vysílání	k1gNnSc2	vysílání
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
obdobné	obdobný	k2eAgNnSc1d1	obdobné
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kdybychom	kdyby	kYmCp1nP	kdyby
na	na	k7c4	na
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
tečku	tečka	k1gFnSc4	tečka
hráli	hrát	k5eAaImAgMnP	hrát
jako	jako	k8xC	jako
osminovou	osminový	k2eAgFnSc4d1	osminová
notu	nota	k1gFnSc4	nota
a	a	k8xC	a
čárku	čárka	k1gFnSc4	čárka
jako	jako	k8xC	jako
čtvrťovou	čtvrťový	k2eAgFnSc4d1	čtvrťová
notu	nota	k1gFnSc4	nota
<g/>
,	,	kIx,	,
pochopitelně	pochopitelně	k6eAd1	pochopitelně
pokud	pokud	k8xS	pokud
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrajeme	hrát	k5eAaImIp1nP	hrát
značně	značně	k6eAd1	značně
staccato	staccato	k6eAd1	staccato
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
odvysílání	odvysílání	k1gNnSc2	odvysílání
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
PARIS	Paris	k1gMnSc1	Paris
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
5	[number]	k4	5
znaků	znak	k1gInPc2	znak
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
mezery	mezera	k1gFnSc2	mezera
za	za	k7c7	za
slovem	slovo	k1gNnSc7	slovo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
48	[number]	k4	48
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
délek	délka	k1gFnPc2	délka
teček	tečka	k1gFnPc2	tečka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
znak	znak	k1gInSc4	znak
tedy	tedy	k8xC	tedy
průměrně	průměrně	k6eAd1	průměrně
připadá	připadat	k5eAaImIp3nS	připadat
9,6	[number]	k4	9,6
časových	časový	k2eAgFnPc2d1	časová
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
při	při	k7c6	při
definici	definice	k1gFnSc6	definice
tempa	tempo	k1gNnSc2	tempo
vysílání	vysílání	k1gNnSc2	vysílání
ve	v	k7c6	v
znacích	znak	k1gInPc6	znak
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tedy	tedy	k9	tedy
například	například	k6eAd1	například
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
tempu	tempo	k1gNnSc6	tempo
vysílání	vysílání	k1gNnPc2	vysílání
80	[number]	k4	80
znaků	znak	k1gInPc2	znak
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgFnSc1d1	základní
časová	časový	k2eAgFnSc1d1	časová
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
délka	délka	k1gFnSc1	délka
tečky	tečka	k1gFnSc2	tečka
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
*	*	kIx~	*
<g/>
9.6	[number]	k4	9.6
<g/>
)	)	kIx)	)
=	=	kIx~	=
0.078125	[number]	k4	0.078125
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tempu	tempo	k1gNnSc6	tempo
vysílání	vysílání	k1gNnPc2	vysílání
50	[number]	k4	50
znaků	znak	k1gInPc2	znak
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
PARIS	Paris	k1gMnSc1	Paris
<g/>
"	"	kIx"	"
včetně	včetně	k7c2	včetně
mezer	mezera	k1gFnPc2	mezera
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
odvysílalo	odvysílat	k5eAaPmAgNnS	odvysílat
přesně	přesně	k6eAd1	přesně
desetkrát	desetkrát	k6eAd1	desetkrát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
předávání	předávání	k1gNnSc4	předávání
zpráv	zpráva	k1gFnPc2	zpráva
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
za	za	k7c4	za
přiměřené	přiměřený	k2eAgFnPc4d1	přiměřená
viditelnosti	viditelnost	k1gFnPc4	viditelnost
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
používat	používat	k5eAaImF	používat
morseovu	morseův	k2eAgFnSc4d1	morseova
signální	signální	k2eAgFnSc4d1	signální
lampu	lampa	k1gFnSc4	lampa
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
přenos	přenos	k1gInSc4	přenos
elektrických	elektrický	k2eAgInPc2d1	elektrický
impulsů	impuls	k1gInPc2	impuls
po	po	k7c6	po
drátech	drát	k1gInPc6	drát
nebo	nebo	k8xC	nebo
rádiově	rádiově	k6eAd1	rádiově
ale	ale	k8xC	ale
o	o	k7c4	o
přenos	přenos	k1gInSc4	přenos
pomocí	pomocí	k7c2	pomocí
světelného	světelný	k2eAgInSc2d1	světelný
paprsku	paprsek	k1gInSc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
a	a	k8xC	a
u	u	k7c2	u
vojenských	vojenský	k2eAgFnPc2d1	vojenská
námořních	námořní	k2eAgFnPc2d1	námořní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysílání	vysílání	k1gNnSc1	vysílání
neporuší	porušit	k5eNaPmIp3nP	porušit
rádiový	rádiový	k2eAgInSc4d1	rádiový
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
snadné	snadný	k2eAgNnSc1d1	snadné
prozrazení	prozrazení	k1gNnSc1	prozrazení
vysílajícího	vysílající	k2eAgInSc2d1	vysílající
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
nutná	nutný	k2eAgFnSc1d1	nutná
přímá	přímý	k2eAgFnSc1d1	přímá
dohlednost	dohlednost	k1gFnSc1	dohlednost
mezi	mezi	k7c7	mezi
vysílající	vysílající	k2eAgFnSc7d1	vysílající
a	a	k8xC	a
přijímací	přijímací	k2eAgFnSc7d1	přijímací
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
světelný	světelný	k2eAgInSc1d1	světelný
zdroj	zdroj	k1gInSc1	zdroj
(	(	kIx(	(
<g/>
žárovka	žárovka	k1gFnSc1	žárovka
<g/>
,	,	kIx,	,
výbojka	výbojka	k1gFnSc1	výbojka
<g/>
)	)	kIx)	)
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
rychlé	rychlý	k2eAgNnSc4d1	rychlé
střídání	střídání	k1gNnSc4	střídání
stavu	stav	k1gInSc2	stav
svítí	svítit	k5eAaImIp3nS	svítit
<g/>
/	/	kIx~	/
<g/>
nesvítí	svítit	k5eNaImIp3nS	svítit
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
předsazená	předsazený	k2eAgFnSc1d1	předsazená
clona	clona	k1gFnSc1	clona
tvořená	tvořený	k2eAgFnSc1d1	tvořená
několika	několik	k4yIc7	několik
žaluziemi	žaluzie	k1gFnPc7	žaluzie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
otevírají	otevírat	k5eAaImIp3nP	otevírat
<g/>
/	/	kIx~	/
<g/>
zavírají	zavírat	k5eAaImIp3nP	zavírat
vysílající	vysílající	k2eAgFnSc7d1	vysílající
obsluhou	obsluha	k1gFnSc7	obsluha
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
-	-	kIx~	-
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
morseovce	morseovka	k1gFnSc6	morseovka
<g/>
,	,	kIx,	,
Samuelu	Samuel	k1gMnSc6	Samuel
Morseovi	Morseus	k1gMnSc6	Morseus
<g/>
,	,	kIx,	,
telegrafii	telegrafie	k1gFnSc6	telegrafie
+	+	kIx~	+
online	onlinout	k5eAaPmIp3nS	onlinout
překladač	překladač	k1gInSc1	překladač
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
a	a	k8xC	a
převodní	převodní	k2eAgFnSc1d1	převodní
tabulka	tabulka	k1gFnSc1	tabulka
-	-	kIx~	-
web	web	k1gInSc1	web
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
morseově	morseův	k2eAgFnSc3d1	morseova
abecedě	abeceda	k1gFnSc3	abeceda
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
historii	historie	k1gFnSc4	historie
Překladač	překladač	k1gMnSc1	překladač
morseovy	morseův	k2eAgFnSc2d1	morseova
abecedy	abeceda	k1gFnSc2	abeceda
-	-	kIx~	-
Překladač	překladač	k1gInSc1	překladač
morseovy	morseův	k2eAgFnSc2d1	morseova
abecedy	abeceda	k1gFnSc2	abeceda
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
volby	volba	k1gFnSc2	volba
znaků	znak	k1gInPc2	znak
pro	pro	k7c4	pro
čárku	čárka	k1gFnSc4	čárka
<g/>
/	/	kIx~	/
<g/>
tečku	tečka	k1gFnSc4	tečka
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
šifrování	šifrování	k1gNnSc1	šifrování
<g/>
/	/	kIx~	/
<g/>
dešifrování	dešifrování	k1gNnSc1	dešifrování
(	(	kIx(	(
<g/>
binární	binární	k2eAgInSc4d1	binární
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
velká	velký	k2eAgNnPc1d1	velké
a	a	k8xC	a
malá	malý	k2eAgNnPc1d1	malé
písmena	písmeno	k1gNnPc1	písmeno
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
</s>
