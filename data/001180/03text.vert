<s>
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1858	[number]	k4	1858
New	New	k1gMnSc2	New
York	York	k1gInSc4	York
City	city	k1gNnSc2	city
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
Oystar	Oystar	k1gMnSc1	Oystar
Bay	Bay	k1gMnSc1	Bay
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
T.	T.	kA	T.
<g/>
R.	R.	kA	R.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
proslul	proslout	k5eAaPmAgMnS	proslout
svojí	svůj	k3xOyFgFnSc7	svůj
energickou	energický	k2eAgFnSc7d1	energická
povahou	povaha	k1gFnSc7	povaha
a	a	k8xC	a
šíří	šíř	k1gFnSc7	šíř
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
vůdcem	vůdce	k1gMnSc7	vůdce
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
26	[number]	k4	26
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
zastával	zastávat	k5eAaImAgMnS	zastávat
řadu	řada	k1gFnSc4	řada
funkcí	funkce	k1gFnPc2	funkce
na	na	k7c6	na
obecní	obecní	k2eAgFnSc6d1	obecní
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc6d1	státní
a	a	k8xC	a
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
jako	jako	k9	jako
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
lovec	lovec	k1gMnSc1	lovec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
největší	veliký	k2eAgFnPc1d3	veliký
proslulosti	proslulost	k1gFnPc1	proslulost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
jako	jako	k9	jako
politikovi	politik	k1gMnSc3	politik
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
42	[number]	k4	42
letech	léto	k1gNnPc6	léto
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgMnS	být
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
prezident	prezident	k1gMnSc1	prezident
William	William	k1gInSc4	William
McKinley	McKinlea	k1gFnSc2	McKinlea
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvář	tvář	k1gFnSc1	tvář
byla	být	k5eAaImAgFnS	být
vytesána	vytesat	k5eAaPmNgFnS	vytesat
do	do	k7c2	do
památníku	památník	k1gInSc2	památník
Mount	Mounta	k1gFnPc2	Mounta
Rushmore	Rushmor	k1gInSc5	Rushmor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
americkým	americký	k2eAgMnSc7d1	americký
generálem	generál	k1gMnSc7	generál
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
Armády	armáda	k1gFnSc2	armáda
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
brigádního	brigádní	k2eAgMnSc2d1	brigádní
generála	generál	k1gMnSc2	generál
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
zástupce	zástupce	k1gMnSc2	zástupce
velitele	velitel	k1gMnSc2	velitel
divize	divize	k1gFnSc2	divize
osobně	osobně	k6eAd1	osobně
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
1	[number]	k4	1
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
spojeneckého	spojenecký	k2eAgNnSc2d1	spojenecké
vylodění	vylodění	k1gNnSc2	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
většina	většina	k1gFnSc1	většina
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
zavedla	zavést	k5eAaPmAgFnS	zavést
ve	v	k7c6	v
veřejnoprávních	veřejnoprávní	k2eAgFnPc6d1	veřejnoprávní
institucích	instituce	k1gFnPc6	instituce
osmihodinový	osmihodinový	k2eAgInSc4d1	osmihodinový
pracovní	pracovní	k2eAgInSc4d1	pracovní
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
důležité	důležitý	k2eAgFnPc1d1	důležitá
byly	být	k5eAaImAgFnP	být
kompenzační	kompenzační	k2eAgInPc4d1	kompenzační
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ukládaly	ukládat	k5eAaImAgInP	ukládat
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
zákonnou	zákonný	k2eAgFnSc4d1	zákonná
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
během	během	k7c2	během
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
zákony	zákon	k1gInPc1	zákon
o	o	k7c6	o
příjmu	příjem	k1gInSc6	příjem
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
,	,	kIx,	,
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
a	a	k8xC	a
majetku	majetek	k1gInSc2	majetek
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
zisků	zisk	k1gInPc2	zisk
obchodních	obchodní	k2eAgFnPc2d1	obchodní
společností	společnost	k1gFnPc2	společnost
usilovaly	usilovat	k5eAaImAgFnP	usilovat
převést	převést	k5eAaPmF	převést
finanční	finanční	k2eAgFnPc1d1	finanční
břemeno	břemeno	k1gNnSc4	břemeno
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
největší	veliký	k2eAgInPc4d3	veliký
předpoklady	předpoklad	k1gInPc4	předpoklad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
unesli	unést	k5eAaPmAgMnP	unést
<g/>
.	.	kIx.	.
</s>
<s>
Mnohým	mnohý	k2eAgMnPc3d1	mnohý
politikům	politik	k1gMnPc3	politik
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jemu	on	k3xPp3gInSc3	on
samotnému	samotný	k2eAgInSc3d1	samotný
a	a	k8xC	a
vůdcům	vůdce	k1gMnPc3	vůdce
progresivistů	progresivista	k1gMnPc2	progresivista
<g/>
,	,	kIx,	,
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
wisconsinský	wisconsinský	k2eAgMnSc1d1	wisconsinský
senátor	senátor	k1gMnSc1	senátor
Robert	Robert	k1gMnSc1	Robert
LaFolette	LaFolett	k1gInSc5	LaFolett
-	-	kIx~	-
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinu	většina	k1gFnSc4	většina
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
znepokojují	znepokojovat	k5eAaImIp3nP	znepokojovat
reformátory	reformátor	k1gMnPc4	reformátor
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
řešit	řešit	k5eAaImF	řešit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Vášnivě	vášnivě	k6eAd1	vášnivě
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
reformu	reforma	k1gFnSc4	reforma
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
nabídnout	nabídnout	k5eAaPmF	nabídnout
lidu	lido	k1gNnSc3	lido
tzv.	tzv.	kA	tzv.
Square	square	k1gInSc4	square
Deal	Deala	k1gFnPc2	Deala
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Poctivé	poctivý	k2eAgNnSc1d1	poctivé
zacházení	zacházení	k1gNnSc1	zacházení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
volebního	volební	k2eAgInSc2d1	volební
programu	program	k1gInSc2	program
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
zpřísněnou	zpřísněný	k2eAgFnSc4d1	zpřísněná
kontrolu	kontrola	k1gFnSc4	kontrola
vlády	vláda	k1gFnSc2	vláda
při	při	k7c6	při
uplatňování	uplatňování	k1gNnSc6	uplatňování
antimonopolních	antimonopolní	k2eAgInPc2d1	antimonopolní
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2	pozdější
rozšíření	rozšíření	k1gNnSc1	rozšíření
vládního	vládní	k2eAgInSc2d1	vládní
dohledu	dohled	k1gInSc2	dohled
na	na	k7c6	na
železnice	železnice	k1gFnSc1	železnice
urychlilo	urychlit	k5eAaPmAgNnS	urychlit
schválení	schválení	k1gNnSc1	schválení
důležitých	důležitý	k2eAgInPc2d1	důležitý
regulačních	regulační	k2eAgInPc2d1	regulační
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
návrhů	návrh	k1gInPc2	návrh
udělal	udělat	k5eAaPmAgInS	udělat
z	z	k7c2	z
uveřejněných	uveřejněný	k2eAgFnPc2d1	uveřejněná
sazeb	sazba	k1gFnPc2	sazba
zákonný	zákonný	k2eAgInSc4d1	zákonný
standard	standard	k1gInSc4	standard
a	a	k8xC	a
opravňoval	opravňovat	k5eAaImAgMnS	opravňovat
i	i	k9	i
dopravce	dopravce	k1gMnSc1	dopravce
na	na	k7c4	na
slevy	sleva	k1gFnPc4	sleva
přepravného	přepravné	k1gNnSc2	přepravné
od	od	k7c2	od
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
osobnost	osobnost	k1gFnSc1	osobnost
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
trustům	trust	k1gInPc3	trust
si	se	k3xPyFc3	se
získaly	získat	k5eAaPmAgFnP	získat
srdce	srdce	k1gNnSc4	srdce
prostých	prostý	k2eAgMnPc2d1	prostý
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc4	jeho
progresivní	progresivní	k2eAgNnPc4d1	progresivní
opatření	opatření	k1gNnPc4	opatření
schvalovali	schvalovat	k5eAaImAgMnP	schvalovat
členové	člen	k1gMnPc1	člen
různých	různý	k2eAgFnPc2d1	různá
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
straně	strana	k1gFnSc6	strana
byl	být	k5eAaImAgMnS	být
ovšem	ovšem	k9	ovšem
nenáviděn	návidět	k5eNaImNgMnS	návidět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
prosperita	prosperita	k1gFnSc1	prosperita
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
přispívala	přispívat	k5eAaImAgFnS	přispívat
ke	k	k7c3	k
spokojenosti	spokojenost	k1gFnSc3	spokojenost
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vítězství	vítězství	k1gNnSc1	vítězství
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
bylo	být	k5eAaImAgNnS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
Povzbuzen	povzbuzen	k2eAgInSc1d1	povzbuzen
skvělým	skvělý	k2eAgNnSc7d1	skvělé
volebním	volební	k2eAgNnSc7d1	volební
vítězstvím	vítězství	k1gNnSc7	vítězství
se	se	k3xPyFc4	se
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
odhodláním	odhodlání	k1gNnSc7	odhodlání
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
prosazování	prosazování	k1gNnSc2	prosazování
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
výroční	výroční	k2eAgFnSc6d1	výroční
zprávě	zpráva	k1gFnSc6	zpráva
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
znovuzvolení	znovuzvolení	k1gNnSc6	znovuzvolení
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
k	k	k7c3	k
ještě	ještě	k6eAd1	ještě
přísnější	přísný	k2eAgFnSc3d2	přísnější
regulaci	regulace	k1gFnSc3	regulace
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1906	[number]	k4	1906
Kongres	kongres	k1gInSc1	kongres
schválil	schválit	k5eAaPmAgInS	schválit
Hepburnův	Hepburnův	k2eAgInSc1d1	Hepburnův
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dával	dávat	k5eAaImAgInS	dávat
Mezivládní	mezivládní	k2eAgFnSc4d1	mezivládní
obchodní	obchodní	k2eAgFnSc4d1	obchodní
komisi	komise	k1gFnSc4	komise
plnou	plný	k2eAgFnSc4d1	plná
moc	moc	k6eAd1	moc
regulovat	regulovat	k5eAaImF	regulovat
sazby	sazba	k1gFnPc4	sazba
<g/>
,	,	kIx,	,
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
její	její	k3xOp3gFnSc4	její
pravomoc	pravomoc	k1gFnSc4	pravomoc
a	a	k8xC	a
donutil	donutit	k5eAaPmAgMnS	donutit
železnice	železnice	k1gFnPc4	železnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
společných	společný	k2eAgInPc2d1	společný
konfliktních	konfliktní	k2eAgInPc2d1	konfliktní
zájmů	zájem	k1gInPc2	zájem
v	v	k7c6	v
parníkové	parníkový	k2eAgFnSc6d1	parníková
dopravě	doprava	k1gFnSc6	doprava
a	a	k8xC	a
uhelných	uhelný	k2eAgFnPc6d1	uhelná
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
opatření	opatření	k1gNnSc4	opatření
Kongresu	kongres	k1gInSc2	kongres
posunula	posunout	k5eAaPmAgFnS	posunout
princip	princip	k1gInSc4	princip
federální	federální	k2eAgFnSc2d1	federální
kontroly	kontrola	k1gFnSc2	kontrola
ještě	ještě	k9	ještě
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
nezávadných	závadný	k2eNgFnPc6d1	nezávadná
potravinách	potravina	k1gFnPc6	potravina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
zakazoval	zakazovat	k5eAaImAgInS	zakazovat
použití	použití	k1gNnSc1	použití
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
"	"	kIx"	"
<g/>
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
nebo	nebo	k8xC	nebo
konzervačních	konzervační	k2eAgFnPc2d1	konzervační
látek	látka	k1gFnPc2	látka
<g/>
"	"	kIx"	"
v	v	k7c6	v
připravovaných	připravovaný	k2eAgInPc6d1	připravovaný
léčivých	léčivý	k2eAgInPc6d1	léčivý
přípravcích	přípravek	k1gInPc6	přípravek
a	a	k8xC	a
potravinách	potravina	k1gFnPc6	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
brzy	brzy	k6eAd1	brzy
posílil	posílit	k5eAaPmAgInS	posílit
další	další	k2eAgInSc4d1	další
zákon	zákon	k1gInSc4	zákon
nařizující	nařizující	k2eAgInSc4d1	nařizující
federální	federální	k2eAgFnSc4d1	federální
kontrolu	kontrola	k1gFnSc4	kontrola
všech	všecek	k3xTgInPc2	všecek
koncernů	koncern	k1gInPc2	koncern
prodávajících	prodávající	k2eAgInPc2d1	prodávající
maso	maso	k1gNnSc4	maso
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
mezistátní	mezistátní	k2eAgInPc4d1	mezistátní
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
zřídil	zřídit	k5eAaPmAgInS	zřídit
nové	nový	k2eAgNnSc4d1	nové
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
šéf	šéf	k1gMnSc1	šéf
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
prezidentova	prezidentův	k2eAgInSc2d1	prezidentův
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
úřadů	úřad	k1gInPc2	úřad
nového	nový	k2eAgNnSc2d1	nové
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
,	,	kIx,	,
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
záležitosti	záležitost	k1gFnPc4	záležitost
velkých	velký	k2eAgNnPc2d1	velké
obchodních	obchodní	k2eAgNnPc2d1	obchodní
sdružení	sdružení	k1gNnPc2	sdružení
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Americká	americký	k2eAgFnSc1d1	americká
cukrovarnická	cukrovarnický	k2eAgFnSc1d1	cukrovarnická
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
American	American	k1gInSc1	American
Sugar	Sugar	k1gInSc1	Sugar
Refining	Refining	k1gInSc4	Refining
Company	Compana	k1gFnSc2	Compana
<g/>
)	)	kIx)	)
poškodila	poškodit	k5eAaPmAgFnS	poškodit
vládu	vláda	k1gFnSc4	vláda
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
sumu	suma	k1gFnSc4	suma
z	z	k7c2	z
dovozních	dovozní	k2eAgInPc2d1	dovozní
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
následným	následný	k2eAgNnPc3d1	následné
zákonným	zákonný	k2eAgNnPc3d1	zákonné
opatřením	opatření	k1gNnPc3	opatření
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
vedení	vedení	k1gNnSc2	vedení
firmy	firma	k1gFnSc2	firma
byli	být	k5eAaImAgMnP	být
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
<g/>
.	.	kIx.	.
</s>
<s>
Standard	standard	k1gInSc1	standard
Oil	Oil	k1gFnSc2	Oil
Company	Compana	k1gFnSc2	Compana
v	v	k7c6	v
Indianě	Indiana	k1gFnSc6	Indiana
obvinili	obvinit	k5eAaPmAgMnP	obvinit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tajně	tajně	k6eAd1	tajně
přijímala	přijímat	k5eAaImAgFnS	přijímat
slevy	sleva	k1gFnPc4	sleva
na	na	k7c4	na
přepravu	přeprava	k1gFnSc4	přeprava
po	po	k7c6	po
chicagské	chicagský	k2eAgFnSc6d1	Chicagská
a	a	k8xC	a
altonské	altonský	k2eAgFnSc6d1	altonský
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Pokuta	pokuta	k1gFnSc1	pokuta
pro	pro	k7c4	pro
Rockefellorovu	Rockefellorův	k2eAgFnSc4d1	Rockefellorův
firmu	firma	k1gFnSc4	firma
SOC	soc	kA	soc
činila	činit	k5eAaImAgFnS	činit
29	[number]	k4	29
000	[number]	k4	000
000	[number]	k4	000
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
veřejně	veřejně	k6eAd1	veřejně
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
uhradí	uhradit	k5eAaPmIp3nP	uhradit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
firma	firma	k1gFnSc1	firma
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
propustit	propustit	k5eAaPmF	propustit
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
svých	svůj	k3xOyFgMnPc2	svůj
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgNnSc2	tento
se	se	k3xPyFc4	se
Rooseveltova	Rooseveltův	k2eAgFnSc1d1	Rooseveltova
administrativa	administrativa	k1gFnSc1	administrativa
zalekla	zaleknout	k5eAaPmAgFnS	zaleknout
a	a	k8xC	a
pokutu	pokuta	k1gFnSc4	pokuta
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
byla	být	k5eAaImAgFnS	být
Standard	standard	k1gInSc4	standard
Oil	Oil	k1gFnSc2	Oil
Company	Compana	k1gFnSc2	Compana
donucena	donucen	k2eAgFnSc1d1	donucena
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
a	a	k8xC	a
reorganizaci	reorganizace	k1gFnSc3	reorganizace
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
firmy	firma	k1gFnPc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zabránily	zabránit	k5eAaPmAgFnP	zabránit
dalšímu	další	k2eAgNnSc3d1	další
drancování	drancování	k1gNnSc3	drancování
nerostného	nerostný	k2eAgNnSc2d1	nerostné
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
kultivaci	kultivace	k1gFnSc3	kultivace
velkých	velký	k2eAgFnPc2d1	velká
ploch	plocha	k1gFnPc2	plocha
zanedbané	zanedbaný	k2eAgFnSc2d1	zanedbaná
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
patřily	patřit	k5eAaImAgFnP	patřit
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
důležitým	důležitý	k2eAgInPc3d1	důležitý
úspěchům	úspěch	k1gInPc3	úspěch
Rooseveltovy	Rooseveltův	k2eAgFnSc2d1	Rooseveltova
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
přijmout	přijmout	k5eAaPmF	přijmout
ucelený	ucelený	k2eAgInSc4d1	ucelený
a	a	k8xC	a
jednotný	jednotný	k2eAgInSc4d1	jednotný
program	program	k1gInSc4	program
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
kultivace	kultivace	k1gFnSc2	kultivace
a	a	k8xC	a
zavlažování	zavlažování	k1gNnSc2	zavlažování
půdy	půda	k1gFnSc2	půda
již	již	k9	již
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
výroční	výroční	k2eAgFnSc6d1	výroční
zprávě	zpráva	k1gFnSc6	zpráva
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
vyčlenili	vyčlenit	k5eAaPmAgMnP	vyčlenit
na	na	k7c4	na
rezervace	rezervace	k1gFnPc4	rezervace
a	a	k8xC	a
přírodní	přírodní	k2eAgInPc4d1	přírodní
parky	park	k1gInPc4	park
18,8	[number]	k4	18,8
milionů	milion	k4xCgInPc2	milion
hektarů	hektar	k1gInPc2	hektar
lesní	lesní	k2eAgFnSc2d1	lesní
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
tuto	tento	k3xDgFnSc4	tento
plochu	plocha	k1gFnSc4	plocha
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
59,2	[number]	k4	59,2
milionů	milion	k4xCgInPc2	milion
hektarů	hektar	k1gInPc2	hektar
a	a	k8xC	a
systematicky	systematicky	k6eAd1	systematicky
dbal	dbát	k5eAaImAgMnS	dbát
na	na	k7c4	na
prevenci	prevence	k1gFnSc4	prevence
lesních	lesní	k2eAgInPc2d1	lesní
požárů	požár	k1gInPc2	požár
a	a	k8xC	a
zalesňování	zalesňování	k1gNnSc2	zalesňování
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
nově	nově	k6eAd1	nově
objevený	objevený	k2eAgInSc1d1	objevený
druh	druh	k1gInSc1	druh
brouka	brouk	k1gMnSc2	brouk
Stenomorpha	Stenomorph	k1gMnSc2	Stenomorph
roosevelti	roosevelť	k1gFnSc2	roosevelť
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
schválil	schválit	k5eAaPmAgInS	schválit
vydání	vydání	k1gNnSc4	vydání
životopisu	životopis	k1gInSc2	životopis
apačského	apačský	k2eAgMnSc2d1	apačský
náčelníka	náčelník	k1gMnSc2	náčelník
Geronima	Geronim	k1gMnSc2	Geronim
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
po	po	k7c4	po
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
vězněn	vězněn	k2eAgInSc1d1	vězněn
coby	coby	k?	coby
válečný	válečný	k2eAgMnSc1d1	válečný
zločinec	zločinec	k1gMnSc1	zločinec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Prezident	prezident	k1gMnSc1	prezident
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
odborech	odbor	k1gInPc6	odbor
"	"	kIx"	"
<g/>
Kdybych	kdyby	kYmCp1nS	kdyby
byl	být	k5eAaImAgMnS	být
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
továrny	továrna	k1gFnSc2	továrna
<g/>
,	,	kIx,	,
dělníkem	dělník	k1gMnSc7	dělník
na	na	k7c4	na
železnici	železnice	k1gFnSc4	železnice
nebo	nebo	k8xC	nebo
jakýmkoli	jakýkoli	k3yIgMnSc7	jakýkoli
námezdním	námezdní	k2eAgMnSc7d1	námezdní
dělníkem	dělník	k1gMnSc7	dělník
<g/>
,	,	kIx,	,
nepochybně	pochybně	k6eNd1	pochybně
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
odborů	odbor	k1gInPc2	odbor
své	svůj	k3xOyFgFnSc2	svůj
profese	profes	k1gFnSc2	profes
<g/>
.	.	kIx.	.
</s>
<s>
Kdybych	kdyby	kYmCp1nS	kdyby
byl	být	k5eAaImAgInS	být
proti	proti	k7c3	proti
politice	politika	k1gFnSc3	politika
mých	můj	k3xOp1gInPc2	můj
odborů	odbor	k1gInPc2	odbor
<g/>
,	,	kIx,	,
připojil	připojit	k5eAaPmAgInS	připojit
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k9	už
pro	pro	k7c4	pro
nic	nic	k3yNnSc4	nic
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
,	,	kIx,	,
tak	tak	k9	tak
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
pomohl	pomoct	k5eAaPmAgMnS	pomoct
napravit	napravit	k5eAaPmF	napravit
to	ten	k3xDgNnSc4	ten
špatné	špatný	k2eAgNnSc4d1	špatné
<g/>
.	.	kIx.	.
</s>
<s>
Kdybych	kdyby	kYmCp1nS	kdyby
měl	mít	k5eAaImAgMnS	mít
výhrady	výhrada	k1gFnSc2	výhrada
vůči	vůči	k7c3	vůči
nečestnému	čestný	k2eNgMnSc3d1	nečestný
předákovi	předák	k1gMnSc3	předák
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
bych	by	kYmCp1nS	by
do	do	k7c2	do
odborů	odbor	k1gInPc2	odbor
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
ho	on	k3xPp3gMnSc4	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Stručně	stručně	k6eAd1	stručně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
věřím	věřit	k5eAaImIp1nS	věřit
v	v	k7c4	v
odbory	odbor	k1gInPc4	odbor
a	a	k8xC	a
myslím	myslet	k5eAaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
odborů	odbor	k1gInPc2	odbor
užitek	užitek	k1gInSc1	užitek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
morálně	morálně	k6eAd1	morálně
zavázáni	zavázat	k5eAaPmNgMnP	zavázat
pomáhat	pomáhat	k5eAaImF	pomáhat
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
sil	síla	k1gFnPc2	síla
ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
zájmu	zájem	k1gInSc6	zájem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
odborový	odborový	k2eAgInSc1d1	odborový
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Odbory	odbor	k1gInPc1	odbor
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	on	k3xPp3gInPc4	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
nepatří	patřit	k5eNaImIp3nP	patřit
členům	člen	k1gMnPc3	člen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
svěřenství	svěřenství	k1gNnSc6	svěřenství
něco	něco	k3yInSc1	něco
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
let	léto	k1gNnPc2	léto
před	před	k7c7	před
Winstonem	Winston	k1gInSc7	Winston
Churchillem	Churchill	k1gInSc7	Churchill
pronesl	pronést	k5eAaPmAgMnS	pronést
výrok	výrok	k1gInSc4	výrok
"	"	kIx"	"
<g/>
I	i	k9	i
have	havat	k5eAaPmIp3nS	havat
nothing	nothing	k1gInSc4	nothing
to	ten	k3xDgNnSc4	ten
offer	offer	k1gMnSc1	offer
but	but	k?	but
blood	blood	k1gInSc1	blood
<g/>
,	,	kIx,	,
toil	toil	k1gInSc1	toil
<g/>
,	,	kIx,	,
tears	tears	k1gInSc1	tears
and	and	k?	and
sweat	sweat	k1gInSc1	sweat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nemohu	moct	k5eNaImIp1nS	moct
vám	vy	k3xPp2nPc3	vy
nabídnout	nabídnout	k5eAaPmF	nabídnout
nic	nic	k3yNnSc4	nic
než	než	k8xS	než
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
dřinu	dřina	k1gFnSc4	dřina
<g/>
,	,	kIx,	,
slzy	slza	k1gFnPc4	slza
a	a	k8xC	a
pot	pot	k1gInSc4	pot
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1897	[number]	k4	1897
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
čerstvě	čerstvě	k6eAd1	čerstvě
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
náměstek	náměstek	k1gMnSc1	náměstek
ministra	ministr	k1gMnSc2	ministr
vojenského	vojenský	k2eAgNnSc2d1	vojenské
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Mluvil	mluvit	k5eAaImAgMnS	mluvit
k	k	k7c3	k
námořním	námořní	k2eAgMnPc3d1	námořní
kadetům	kadet	k1gMnPc3	kadet
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jim	on	k3xPp3gMnPc3	on
vlastně	vlastně	k9	vlastně
realisticky	realisticky	k6eAd1	realisticky
popisoval	popisovat	k5eAaImAgMnS	popisovat
běžné	běžný	k2eAgFnPc4d1	běžná
dny	dna	k1gFnPc4	dna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
čekají	čekat	k5eAaImIp3nP	čekat
<g/>
.	.	kIx.	.
</s>
<s>
ROOSEVELT	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
Theodore	Theodor	k1gMnSc5	Theodor
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc4d1	silný
život	život	k1gInSc4	život
:	:	kIx,	:
úvahy	úvaha	k1gFnPc4	úvaha
a	a	k8xC	a
výzvy	výzva	k1gFnPc4	výzva
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Prusík	Prusík	k1gMnSc1	Prusík
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Tiskem	tisk	k1gInSc7	tisk
a	a	k8xC	a
nákladem	náklad	k1gInSc7	náklad
F.	F.	kA	F.
Šimáčka	Šimáček	k1gMnSc2	Šimáček
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
159	[number]	k4	159
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
