<s>
Karamazovi	Karamaz	k1gMnSc3	Karamaz
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Petra	Petr	k1gMnSc2	Petr
Zelenky	Zelenka	k1gMnSc2	Zelenka
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
česko-polské	českoolský	k2eAgFnSc6d1	česko-polská
koprodukci	koprodukce	k1gFnSc6	koprodukce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
filmu	film	k1gInSc2	film
tvoří	tvořit	k5eAaImIp3nS	tvořit
představení	představení	k1gNnSc4	představení
Dejvického	dejvický	k2eAgNnSc2d1	Dejvické
divadla	divadlo	k1gNnSc2	divadlo
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
(	(	kIx(	(
<g/>
román	román	k1gInSc4	román
F.	F.	kA	F.
M.	M.	kA	M.
Dostojevského	Dostojevský	k2eAgInSc2d1	Dostojevský
v	v	k7c6	v
dramatizaci	dramatizace	k1gFnSc6	dramatizace
Evalda	Evald	k1gMnSc2	Evald
Schorma	Schorm	k1gMnSc2	Schorm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
zasazeno	zasadit	k5eAaPmNgNnS	zasadit
do	do	k7c2	do
netradičního	tradiční	k2eNgNnSc2d1	netradiční
prostředí	prostředí	k1gNnSc2	prostředí
polských	polský	k2eAgFnPc2d1	polská
oceláren	ocelárna	k1gFnPc2	ocelárna
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
souběžně	souběžně	k6eAd1	souběžně
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
další	další	k2eAgFnSc1d1	další
dějová	dějový	k2eAgFnSc1d1	dějová
linie	linie	k1gFnSc1	linie
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
herců	herc	k1gInPc2	herc
a	a	k8xC	a
přihlížejících	přihlížející	k2eAgMnPc2d1	přihlížející
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>

