<s>
Fuzzy	Fuzz	k1gInPc1
množina	množina	k1gFnSc1
</s>
<s>
Fuzzy	Fuzza	k1gFnPc4
množina	množina	k1gFnSc1
není	být	k5eNaImIp3nS
ostře	ostro	k6eAd1
ohraničena	ohraničen	k2eAgFnSc1d1
</s>
<s>
Příslušnost	příslušnost	k1gFnSc1
prvku	prvek	k1gInSc2
x	x	k?
k	k	k7c3
množině	množina	k1gFnSc3
je	být	k5eAaImIp3nS
označena	označit	k5eAaPmNgFnS
μ	μ	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šedá	Šedá	k1gFnSc1
=	=	kIx~
klasická	klasický	k2eAgFnSc1d1
množina	množina	k1gFnSc1
<g/>
,	,	kIx,
černá	černý	k2eAgFnSc1d1
-	-	kIx~
fuzzy	fuzz	k1gInPc1
množina	množina	k1gFnSc1
</s>
<s>
Fuzzy	Fuzz	k1gInPc1
množina	množina	k1gFnSc1
[	[	kIx(
<g/>
fazi	fazi	k6eAd1
<g/>
]	]	kIx)
nebo	nebo	k8xC
neostrá	ostrý	k2eNgFnSc1d1
množina	množina	k1gFnSc1
je	být	k5eAaImIp3nS
množina	množina	k1gFnSc1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
příslušnost	příslušnost	k1gFnSc1
k	k	k7c3
této	tento	k3xDgFnSc3
množině	množina	k1gFnSc3
je	být	k5eAaImIp3nS
odstupňovaná	odstupňovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klasické	klasický	k2eAgFnSc6d1
booleovské	booleovský	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
množin	množina	k1gFnPc2
se	se	k3xPyFc4
pracuje	pracovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
s	s	k7c7
dvouhodnotovými	dvouhodnotový	k2eAgInPc7d1
vstupy	vstup	k1gInPc7
–	–	k?
příslušnost	příslušnost	k1gFnSc4
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
jen	jen	k6eAd1
dvě	dva	k4xCgFnPc4
hodnoty	hodnota	k1gFnPc4
<g/>
,	,	kIx,
0	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasické	klasický	k2eAgFnPc4d1
množiny	množina	k1gFnPc4
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
speciálním	speciální	k2eAgInSc7d1
případem	případ	k1gInSc7
fuzzy	fuzza	k1gFnSc2
množin	množina	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
může	moct	k5eAaImIp3nS
příslušnost	příslušnost	k1gFnSc4
nabývat	nabývat	k5eAaImF
libovolnou	libovolný	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
z	z	k7c2
reálného	reálný	k2eAgInSc2d1
intervalu	interval	k1gInSc2
[	[	kIx(
<g/>
0,1	0,1	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
teorii	teorie	k1gFnSc6
fuzzy	fuzza	k1gFnPc1
množin	množina	k1gFnPc2
lze	lze	k6eAd1
analogicky	analogicky	k6eAd1
zavést	zavést	k5eAaPmF
operace	operace	k1gFnPc4
(	(	kIx(
<g/>
doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
průnik	průnik	k1gInSc1
<g/>
,	,	kIx,
sjednocení	sjednocení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
z	z	k7c2
klasické	klasický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zavedení	zavedení	k1gNnSc1
</s>
<s>
Fuzzy	Fuzz	k1gInPc1
množiny	množina	k1gFnSc2
poprvé	poprvé	k6eAd1
představil	představit	k5eAaPmAgMnS
Lotfi	Lotf	k1gMnSc3
A.	A.	kA
Zadeh	Zadeh	k1gInSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c4
Berkley	Berkley	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
jako	jako	k8xC,k8xS
rozšíření	rozšíření	k1gNnSc4
(	(	kIx(
<g/>
"	"	kIx"
<g/>
zevšeobecnění	zevšeobecnění	k1gNnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
klasických	klasický	k2eAgFnPc2d1
množin	množina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Fuzzy	Fuzz	k1gInPc1
množiny	množina	k1gFnSc2
lze	lze	k6eAd1
použít	použít	k5eAaPmF
k	k	k7c3
popisu	popis	k1gInSc3
pravděpodobných	pravděpodobný	k2eAgInPc2d1
<g/>
,	,	kIx,
nejistých	jistý	k2eNgInPc2d1
nebo	nebo	k8xC
neurčitých	určitý	k2eNgInPc2d1
jevů	jev	k1gInPc2
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgInPc7
se	se	k3xPyFc4
pracuje	pracovat	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
biologii	biologie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
lingvistice	lingvistika	k1gFnSc6
nebo	nebo	k8xC
ve	v	k7c6
společenských	společenský	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Fuzzy	Fuzz	k1gInPc1
logika	logika	k1gFnSc1
</s>
<s>
Amplituda	amplituda	k1gFnSc1
pravděpodobnosti	pravděpodobnost	k1gFnSc2
</s>
<s>
Predikátová	predikátový	k2eAgFnSc1d1
logika	logika	k1gFnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Hájek	Hájek	k1gMnSc1
(	(	kIx(
<g/>
matematik	matematik	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
George	Georg	k1gMnSc2
Klir	Klir	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Teorie	teorie	k1gFnSc2
fuzzy	fuzza	k1gFnSc2
množin	množina	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
Axiomy	axiom	k1gInPc1
</s>
<s>
axiom	axiom	k1gInSc1
výběru	výběr	k1gInSc2
•	•	k?
axiom	axiom	k1gInSc4
spočetného	spočetný	k2eAgInSc2d1
výběru	výběr	k1gInSc2
•	•	k?
axiom	axiom	k1gInSc4
závislého	závislý	k2eAgInSc2d1
výběru	výběr	k1gInSc2
•	•	k?
axiom	axiom	k1gInSc1
extenzionality	extenzionalita	k1gFnSc2
•	•	k?
axiom	axiom	k1gInSc1
nekonečna	nekonečno	k1gNnSc2
•	•	k?
axiom	axiom	k1gInSc1
dvojice	dvojice	k1gFnSc1
•	•	k?
axiom	axiom	k1gInSc4
potenční	potenční	k2eAgFnSc2d1
množiny	množina	k1gFnSc2
•	•	k?
Axiom	axiom	k1gInSc1
regulárnosti	regulárnost	k1gFnSc2
•	•	k?
axiom	axiom	k1gInSc1
sumy	suma	k1gFnSc2
•	•	k?
schéma	schéma	k1gNnSc1
nahrazení	nahrazení	k1gNnSc2
•	•	k?
schéma	schéma	k1gNnSc1
axiomů	axiom	k1gInPc2
vydělení	vydělení	k1gNnSc2
•	•	k?
hypotéza	hypotéza	k1gFnSc1
kontinua	kontinuum	k1gNnSc2
•	•	k?
Martinův	Martinův	k2eAgInSc4d1
axiom	axiom	k1gInSc4
•	•	k?
velké	velký	k2eAgMnPc4d1
kardinály	kardinál	k1gMnPc4
Množinové	množinový	k2eAgFnSc2d1
operace	operace	k1gFnSc2
</s>
<s>
doplněk	doplněk	k1gInSc1
•	•	k?
de	de	k?
Morganovy	morganův	k2eAgInPc4d1
zákony	zákon	k1gInPc4
•	•	k?
kartézský	kartézský	k2eAgInSc1d1
součin	součin	k1gInSc1
•	•	k?
potenční	potenční	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
průnik	průnik	k1gInSc1
•	•	k?
rozdíl	rozdíl	k1gInSc1
množin	množina	k1gFnPc2
•	•	k?
sjednocení	sjednocení	k1gNnSc6
•	•	k?
symetrická	symetrický	k2eAgFnSc1d1
diference	diference	k1gFnSc1
Koncepty	koncept	k1gInPc1
</s>
<s>
bijekce	bijekce	k1gFnSc1
•	•	k?
kardinální	kardinální	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
•	•	k?
konstruovatelná	konstruovatelný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
mohutnost	mohutnost	k1gFnSc1
•	•	k?
ordinální	ordinální	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
•	•	k?
prvek	prvek	k1gInSc1
množiny	množina	k1gFnSc2
•	•	k?
rodina	rodina	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
transfinitní	transfinitní	k2eAgFnSc2d1
indukce	indukce	k1gFnSc2
•	•	k?
třída	třída	k1gFnSc1
•	•	k?
Vennův	Vennův	k2eAgInSc1d1
diagram	diagram	k1gInSc1
Množiny	množina	k1gFnSc2
</s>
<s>
Cantorova	Cantorův	k2eAgFnSc1d1
diagonální	diagonální	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
•	•	k?
fuzzy	fuzza	k1gFnSc2
množina	množina	k1gFnSc1
•	•	k?
konečná	konečný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
nekonečná	konečný	k2eNgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
nespočetná	spočetný	k2eNgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
podmnožina	podmnožina	k1gFnSc1
•	•	k?
prázdná	prázdný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
rekurzivní	rekurzivní	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
spočetná	spočetný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
tranzitivní	tranzitivní	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
univerzální	univerzální	k2eAgFnSc1d1
množina	množina	k1gFnSc1
Teorie	teorie	k1gFnSc1
</s>
<s>
axiomatická	axiomatický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
Cantorova	Cantorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
•	•	k?
forsing	forsing	k1gInSc1
•	•	k?
Kelleyova	Kelleyov	k1gInSc2
<g/>
–	–	k?
<g/>
Morseova	Morseův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
naivní	naivní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
•	•	k?
New	New	k1gFnSc2
Foundations	Foundationsa	k1gFnPc2
•	•	k?
paradoxy	paradox	k1gInPc1
naivní	naivní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
•	•	k?
Principia	principium	k1gNnSc2
Mathematica	Mathematicus	k1gMnSc2
•	•	k?
Russellův	Russellův	k2eAgInSc4d1
paradox	paradox	k1gInSc4
•	•	k?
Suslinova	Suslinův	k2eAgFnSc1d1
hypotéza	hypotéza	k1gFnSc1
•	•	k?
Von	von	k1gInSc1
Neumannova	Neumannův	k2eAgInSc2d1
<g/>
–	–	k?
<g/>
Bernaysova	Bernaysův	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
Gödelova	Gödelův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
Zermelova	Zermelův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
Zermelova	Zermelův	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
Fraenkelova	Fraenkelův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
alternativní	alternativní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
Lidé	člověk	k1gMnPc1
</s>
<s>
Abraham	Abraham	k1gMnSc1
Fraenkel	Fraenkel	k1gMnSc1
•	•	k?
Bertrand	Bertrand	k1gInSc1
Russell	Russell	k1gMnSc1
•	•	k?
Ernst	Ernst	k1gMnSc1
Zermelo	Zermela	k1gFnSc5
•	•	k?
Georg	Georg	k1gMnSc1
Cantor	Cantor	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
von	von	k1gInSc1
Neumann	Neumann	k1gMnSc1
•	•	k?
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
•	•	k?
Lotfi	Lotf	k1gFnSc2
Zadeh	Zadeh	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Bernays	Bernaysa	k1gFnPc2
•	•	k?
Paul	Paul	k1gMnSc1
Cohen	Cohen	k2eAgMnSc1d1
•	•	k?
Petr	Petr	k1gMnSc1
Vopěnka	Vopěnka	k1gFnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Dedekind	Dedekind	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Jech	Jech	k1gMnSc1
•	•	k?
Willard	Willard	k1gMnSc1
Quine	Quin	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
7150	#num#	k4
</s>
