<s>
Émile	Émile	k6eAd1
Mayade	Mayad	k1gInSc5
</s>
<s>
Émile	Émile	k6eAd1
Mayade	Mayad	k1gInSc5
Narození	narození	k1gNnPc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1853	#num#	k4
<g/>
Clermont-Ferrand	Clermont-Ferrando	k1gNnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1898	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
45	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Chevanceaux	Chevanceaux	k1gInSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
silniční	silniční	k2eAgFnSc1d1
dopravní	dopravní	k2eAgFnSc1d1
nehoda	nehoda	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
automobilový	automobilový	k2eAgMnSc1d1
závodník	závodník	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Émile	Émile	k6eAd1
Mayade	Mayad	k1gInSc5
za	za	k7c7
volantem	volant	k1gInSc7
vozu	vůz	k1gInSc2
Panhard	Panharda	k1gFnPc2
&	&	k?
Levasssor	Levasssora	k1gFnPc2
během	během	k7c2
závodu	závod	k1gInSc2
Paříž-Rouen	Paříž-Rouna	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
</s>
<s>
Émile	Émile	k6eAd1
Louis	Louis	k1gMnSc1
Mayade	Mayad	k1gInSc5
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1853	#num#	k4
<g/>
,	,	kIx,
Clermont-Ferrand	Clermont-Ferrand	k1gInSc1
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1898	#num#	k4
<g/>
,	,	kIx,
Chevanceux	Chevanceux	k1gInSc1
<g/>
,	,	kIx,
departement	departement	k1gInSc1
Charente-Maritime	Charente-Maritim	k1gInSc5
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
průkopník	průkopník	k1gMnSc1
automobilismu	automobilismus	k1gInSc2
a	a	k8xC
automobilový	automobilový	k2eAgMnSc1d1
závodník	závodník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Mayade	Mayást	k5eAaPmIp3nS
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
prvních	první	k4xOgMnPc2
zaměstnanců	zaměstnanec	k1gMnPc2
René	René	k1gFnSc2
Panharda	Panhard	k1gMnSc2
a	a	k8xC
Émile	Émil	k1gMnSc2
Levassora	Levassor	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
vyráběli	vyrábět	k5eAaImAgMnP
automobily	automobil	k1gInPc1
Panhard	Panhard	k1gInSc1
&	&	k?
Levassor	Levassor	k1gInSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Panhard	Panhard	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
vedoucím	vedoucí	k1gMnSc7
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
vozem	vůz	k1gInSc7
Panhard	Panharda	k1gFnPc2
také	také	k9
startoval	startovat	k5eAaBmAgInS
při	při	k7c6
vůbec	vůbec	k9
prvním	první	k4xOgInSc6
závodu	závod	k1gInSc2
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Francie	Francie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
na	na	k7c4
127	#num#	k4
kilometrů	kilometr	k1gInPc2
dlouhé	dlouhý	k2eAgFnSc2d1
trati	trať	k1gFnSc2
Paříž-Rouen	Paříž-Rouna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dojel	dojet	k5eAaPmAgMnS
sedmý	sedmý	k4xOgMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
závod	závod	k1gInSc1
Paříž	Paříž	k1gFnSc1
<g/>
–	–	k?
<g/>
Bordeaux	Bordeaux	k1gNnSc2
<g/>
–	–	k?
<g/>
Paříž	Paříž	k1gFnSc1
s	s	k7c7
délkou	délka	k1gFnSc7
1178	#num#	k4
km	km	kA
dokončil	dokončit	k5eAaPmAgInS
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
jako	jako	k8xC,k8xS
šestý	šestý	k4xOgInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
je	být	k5eAaImIp3nS
vítězství	vítězství	k1gNnSc1
v	v	k7c6
závodě	závod	k1gInSc6
Paříž	Paříž	k1gFnSc1
<g/>
–	–	k?
<g/>
Marseille	Marseille	k1gFnSc1
<g/>
–	–	k?
<g/>
Paříž	Paříž	k1gFnSc1
sezóny	sezóna	k1gFnSc2
1896	#num#	k4
o	o	k7c6
délce	délka	k1gFnSc6
1710	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mayade	Mayad	k1gInSc5
vyhrál	vyhrát	k5eAaPmAgMnS
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
25	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
v	v	k7c6
celkovém	celkový	k2eAgInSc6d1
čase	čas	k1gInSc6
67	#num#	k4
<g/>
:	:	kIx,
<g/>
42.58	42.58	k4
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
zvítězil	zvítězit	k5eAaPmAgMnS
i	i	k8xC
v	v	k7c6
posledních	poslední	k2eAgFnPc6d1
třech	tři	k4xCgFnPc6
denních	denní	k2eAgFnPc6d1
etapách	etapa	k1gFnPc6
z	z	k7c2
celkových	celkový	k2eAgInPc2d1
deseti	deset	k4xCc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
závodě	závod	k1gInSc6
se	se	k3xPyFc4
vážně	vážně	k6eAd1
zranil	zranit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
týmový	týmový	k2eAgMnSc1d1
kolega	kolega	k1gMnSc1
Émile	Émile	k1gNnSc2
Levassor	Levassor	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
následující	následující	k2eAgMnSc1d1
jaro	jaro	k6eAd1
na	na	k7c4
následky	následek	k1gInPc4
zranění	zranění	k1gNnSc2
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Mayade	Mayást	k5eAaPmIp3nS
se	se	k3xPyFc4
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1896	#num#	k4
účastnil	účastnit	k5eAaImAgMnS
historické	historický	k2eAgFnPc4d1
jízdy	jízda	k1gFnPc4
The	The	k1gMnPc2
Emancipation	Emancipation	k1gInSc4
Act	Act	k1gFnPc2
na	na	k7c4
86	#num#	k4
km	km	kA
dlouhé	dlouhý	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
z	z	k7c2
Londýna	Londýn	k1gInSc2
do	do	k7c2
Brightonu	Brighton	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
dojel	dojet	k5eAaPmAgMnS
čtvrtý	čtvrtý	k4xOgMnSc1
za	za	k7c7
dvěma	dva	k4xCgInPc7
motorovými	motorový	k2eAgInPc7d1
tricykly	tricykl	k1gInPc7
Léona	Léon	k1gMnSc2
Bollée	Bollé	k1gMnSc2
a	a	k8xC
dalším	další	k2eAgInSc7d1
vozem	vůz	k1gInSc7
Panhard	Panhard	k1gMnSc1
&	&	k?
Levassor	Levassor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozidlo	vozidlo	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
Mayade	Mayad	k1gInSc5
startoval	startovat	k5eAaBmAgInS
<g/>
,	,	kIx,
poté	poté	k6eAd1
zůstalo	zůstat	k5eAaPmAgNnS
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
jej	on	k3xPp3gMnSc4
za	za	k7c4
1200	#num#	k4
liber	libra	k1gFnPc2
koupil	koupit	k5eAaPmAgMnS
Charles	Charles	k1gMnSc1
Rolls	Rollsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůz	vůz	k1gInSc1
existuje	existovat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
1898	#num#	k4
zahynul	zahynout	k5eAaPmAgInS
Émile	Émile	k1gInSc1
Mayade	Mayad	k1gInSc5
při	při	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
prvních	první	k4xOgFnPc2
smrtelných	smrtelný	k2eAgFnPc2d1
dopravních	dopravní	k2eAgFnPc2d1
nehod	nehoda	k1gFnPc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
mimo	mimo	k7c4
závodní	závodní	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Nehodu	nehoda	k1gFnSc4
způsobil	způsobit	k5eAaPmAgMnS
splašený	splašený	k2eAgMnSc1d1
kůn	kůn	k?
táhnoucí	táhnoucí	k2eAgInSc1d1
vozík	vozík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.driverdb.com/drivers/emile-mayade/career/	http://www.driverdb.com/drivers/emile-mayade/career/	k?
Émile	Émile	k1gInSc1
Mayade	Mayad	k1gInSc5
<g/>
↑	↑	k?
teamdan	teamdan	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
1894	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
and	and	k?
Paris	Paris	k1gMnSc1
Races	Races	k1gMnSc1
<g/>
↑	↑	k?
gilbert	gilbert	k1gInSc1
<g/>
.	.	kIx.
<g/>
mayade	mayást	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
pagesperso-orange	pagesperso-orange	k1gNnSc1
<g/>
:	:	kIx,
Emile	Emil	k1gMnSc5
MAYADE	MAYADE	kA
<g/>
,	,	kIx,
coureur	coureur	k1gMnSc1
automobile	automobil	k1gInSc6
<g/>
↑	↑	k?
La	la	k1gNnSc2
France	Franc	k1gMnSc2
Automobile	automobil	k1gInSc6
du	du	k?
1	#num#	k4
<g/>
°	°	k?
octobre	octobr	k1gInSc5
1898	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
page	pag	k1gInSc2
336	#num#	k4
–	–	k?
Obituary	Obituar	k1gInPc1
notes	notes	k1gInSc1
(	(	kIx(
<g/>
Illustrations	Illustrations	k1gInSc1
in	in	k?
Le	Le	k1gMnSc5
Monde	Mond	k1gMnSc5
on	on	k3xPp3gMnSc1
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
January	Januara	k1gFnSc2
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
gilbert	gilbert	k1gInSc1
<g/>
.	.	kIx.
<g/>
mayade	mayást	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
pagesperso-orange	pagesperso-orangat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Anthony	Anthon	k1gInPc1
Bird	Birda	k1gFnPc2
<g/>
:	:	kIx,
De	De	k?
Dion	Dion	k1gInSc1
Bouton	Bouton	k1gInSc1
–	–	k?
First	First	k1gInSc1
automobile	automobil	k1gInSc6
Giant	Gianta	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Ballantine	Ballantin	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Illustrated	Illustrated	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Car	car	k1gMnSc1
marque	marquat	k5eAaPmIp3nS
book	book	k6eAd1
No	no	k9
6	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ballantine	Ballantin	k1gInSc5
Books	Books	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
1971	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
345	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2322	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Richard	Richard	k1gMnSc1
J.	J.	kA
Evans	Evans	k1gInSc1
<g/>
:	:	kIx,
Steam	Steam	k1gInSc1
Cars	Cars	k1gInSc1
(	(	kIx(
<g/>
Shire	Shir	k1gInSc5
Album	album	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shire	Shir	k1gInSc5
Publications	Publicationsa	k1gFnPc2
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
85263	#num#	k4
<g/>
-	-	kIx~
<g/>
774	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Anthony	Anthona	k1gFnPc1
Bird	Birda	k1gFnPc2
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
Douglas-Scott	Douglas-Scott	k1gMnSc1
Montagu	Montag	k1gInSc2
of	of	k?
Beaulieu	Beaulieus	k1gInSc2
<g/>
:	:	kIx,
Steam	Steam	k1gInSc1
Cars	Carsa	k1gFnPc2
<g/>
,	,	kIx,
1770	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Littlehampton	Littlehampton	k1gInSc1
Book	Book	k1gMnSc1
Services	Services	k1gMnSc1
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
304	#num#	k4
<g/>
-	-	kIx~
<g/>
93707	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Floyd	Floyd	k1gMnSc1
Clymer	Clymer	k1gMnSc1
<g/>
,	,	kIx,
Harry	Harra	k1gMnSc2
W.	W.	kA
Gahagan	Gahagan	k1gMnSc1
<g/>
:	:	kIx,
Floyd	Floyd	k1gMnSc1
Clymer	Clymer	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Steam	Steam	k1gInSc1
Car	car	k1gMnSc1
Scrapbook	Scrapbook	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literary	Literara	k1gFnPc1
Licensing	Licensing	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
258	#num#	k4
<g/>
-	-	kIx~
<g/>
42699	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Émile	Émile	k1gFnSc2
Mayade	Mayad	k1gInSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Profil	profil	k1gInSc1
na	na	k7c4
www.historicracing.com	www.historicracing.com	k1gInSc4
</s>
<s>
teamdan	teamdan	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
1894	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
and	and	k?
Paris	Paris	k1gMnSc1
Races	Races	k1gMnSc1
</s>
<s>
teamdan	teamdan	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
1895	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
and	and	k?
Paris	Paris	k1gMnSc1
Races	Races	k1gMnSc1
</s>
<s>
teamdan	teamdan	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
1896	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
and	and	k?
Paris	Paris	k1gMnSc1
Races	Races	k1gMnSc1
</s>
<s>
gilbert	gilbert	k1gInSc1
<g/>
.	.	kIx.
<g/>
mayade	mayást	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
pagesperso-orange	pagesperso-orange	k1gNnSc1
<g/>
:	:	kIx,
Emile	Emil	k1gMnSc5
MAYADE	MAYADE	kA
<g/>
,	,	kIx,
coureur	coureur	k1gMnSc1
automobile	automobil	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
|	|	kIx~
Francie	Francie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
