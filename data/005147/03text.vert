<s>
(	(	kIx(	(
<g/>
136472	[number]	k4	136472
<g/>
)	)	kIx)	)
Makemake	Makemak	k1gInSc2	Makemak
/	/	kIx~	/
<g/>
maki	maki	k6eAd1	maki
<g/>
:	:	kIx,	:
<g/>
maki	maki	k6eAd1	maki
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
obíhající	obíhající	k2eAgFnSc1d1	obíhající
za	za	k7c7	za
drahou	draha	k1gFnSc7	draha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
původní	původní	k2eAgNnSc1d1	původní
označení	označení	k1gNnSc1	označení
bylo	být	k5eAaImAgNnS	být
2005	[number]	k4	2005
FY	fy	kA	fy
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Makemake	Makemake	k6eAd1	Makemake
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k8xC	jako
plutoid	plutoid	k1gInSc1	plutoid
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Makemake	Makemake	k6eAd1	Makemake
obíhá	obíhat	k5eAaImIp3nS	obíhat
Slunce	slunce	k1gNnSc1	slunce
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
38	[number]	k4	38
<g/>
–	–	k?	–
<g/>
53	[number]	k4	53
AU	au	k0	au
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k4c4	málo
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
1300	[number]	k4	1300
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
km	km	kA	km
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
trpasličí	trpasličí	k2eAgFnSc7d1	trpasličí
planetou	planeta	k1gFnSc7	planeta
po	po	k7c6	po
Plutu	Pluto	k1gNnSc6	Pluto
a	a	k8xC	a
Eris	Eris	k1gFnSc1	Eris
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
těleso	těleso	k1gNnSc1	těleso
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
po	po	k7c6	po
Plutu	plut	k1gInSc6	plut
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejjasnější	jasný	k2eAgNnSc4d3	nejjasnější
transneptunické	transneptunický	k2eAgNnSc4d1	transneptunické
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
výkonných	výkonný	k2eAgInPc2d1	výkonný
amatérských	amatérský	k2eAgInPc2d1	amatérský
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
současná	současný	k2eAgFnSc1d1	současná
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
jasnost	jasnost	k1gFnSc1	jasnost
je	být	k5eAaImIp3nS	být
16,7	[number]	k4	16,7
mag	mag	k?	mag
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
této	tento	k3xDgFnSc2	tento
jasnosti	jasnost	k1gFnSc2	jasnost
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc1	její
vysoké	vysoký	k2eAgNnSc1d1	vysoké
albedo	albedo	k1gNnSc1	albedo
(	(	kIx(	(
<g/>
odrazivost	odrazivost	k1gFnSc1	odrazivost
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vypočítáno	vypočítat	k5eAaPmNgNnS	vypočítat
na	na	k7c4	na
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
vysvětlující	vysvětlující	k2eAgFnPc1d1	vysvětlující
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
tělesa	těleso	k1gNnSc2	těleso
mohla	moct	k5eAaImAgFnS	moct
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zmrznout	zmrznout	k5eAaPmF	zmrznout
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
i	i	k9	i
u	u	k7c2	u
Pluta	Pluto	k1gNnSc2	Pluto
–	–	k?	–
v	v	k7c6	v
perihéliu	perihélium	k1gNnSc6	perihélium
(	(	kIx(	(
<g/>
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
nejblíže	blízce	k6eAd3	blízce
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
atmosféra	atmosféra	k1gFnSc1	atmosféra
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
v	v	k7c6	v
plynném	plynný	k2eAgInSc6d1	plynný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
aféliu	afélium	k1gNnSc6	afélium
plyn	plyn	k1gInSc4	plyn
vymrzne	vymrznout	k5eAaPmIp3nS	vymrznout
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
hladké	hladký	k2eAgFnPc4d1	hladká
lesklé	lesklý	k2eAgFnPc4d1	lesklá
plochy	plocha	k1gFnPc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
Hubbelův	Hubbelův	k2eAgInSc1d1	Hubbelův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
teleskop	teleskop	k1gInSc1	teleskop
objevil	objevit	k5eAaPmAgInS	objevit
první	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
této	tento	k3xDgFnSc2	tento
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
pracovně	pracovně	k6eAd1	pracovně
nazvaný	nazvaný	k2eAgMnSc1d1	nazvaný
jako	jako	k9	jako
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
136472	[number]	k4	136472
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
