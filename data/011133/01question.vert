<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
resp.	resp.	kA	resp.
elektronická	elektronický	k2eAgFnSc1d1	elektronická
součást	součást	k1gFnSc1	součást
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
nebo	nebo	k8xC	nebo
měření	měření	k1gNnSc3	měření
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
střídavých	střídavý	k2eAgMnPc2d1	střídavý
<g/>
?	?	kIx.	?
</s>
