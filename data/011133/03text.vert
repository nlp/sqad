<p>
<s>
Transduktor	transduktor	k1gInSc1	transduktor
jinak	jinak	k6eAd1	jinak
též	též	k9	též
magnetický	magnetický	k2eAgInSc4d1	magnetický
zesilovač	zesilovač	k1gInSc4	zesilovač
je	být	k5eAaImIp3nS	být
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
resp.	resp.	kA	resp.
elektronická	elektronický	k2eAgFnSc1d1	elektronická
součást	součást	k1gFnSc1	součást
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
nebo	nebo	k8xC	nebo
měření	měření	k1gNnSc3	měření
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
střídavých	střídavý	k2eAgMnPc2d1	střídavý
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
malého	malý	k2eAgInSc2d1	malý
<g/>
,	,	kIx,	,
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
řídícím	řídící	k2eAgInSc6d1	řídící
obvodu	obvod	k1gInSc6	obvod
se	se	k3xPyFc4	se
reguluje	regulovat	k5eAaImIp3nS	regulovat
velký	velký	k2eAgInSc1d1	velký
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
v	v	k7c6	v
zátěžovém	zátěžový	k2eAgInSc6d1	zátěžový
obvodu	obvod	k1gInSc6	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
transduktoru	transduktor	k1gInSc2	transduktor
je	být	k5eAaImIp3nS	být
přesytka	přesytka	k1gFnSc1	přesytka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
představuje	představovat	k5eAaImIp3nS	představovat
v	v	k7c6	v
principu	princip	k1gInSc6	princip
tlumivku	tlumivka	k1gFnSc4	tlumivka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
schematu	schema	k1gNnSc6	schema
vpravo	vpravo	k6eAd1	vpravo
označená	označený	k2eAgNnPc4d1	označené
T	T	kA	T
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
indukčnost	indukčnost	k1gFnSc1	indukčnost
je	být	k5eAaImIp3nS	být
měněna	měnit	k5eAaImNgFnS	měnit
změnou	změna	k1gFnSc7	změna
předmagnetizace	předmagnetizace	k1gFnSc2	předmagnetizace
železného	železný	k2eAgNnSc2d1	železné
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
transduktor	transduktor	k1gInSc1	transduktor
opatřen	opatřit	k5eAaPmNgInS	opatřit
druhým	druhý	k4xOgNnSc7	druhý
vinutím	vinutí	k1gNnSc7	vinutí
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
protéká	protékat	k5eAaImIp3nS	protékat
řídící	řídící	k2eAgInSc1d1	řídící
proud	proud	k1gInSc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
Železné	železný	k2eAgNnSc1d1	železné
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
feritové	feritový	k2eAgNnSc1d1	feritové
<g/>
)	)	kIx)	)
jádro	jádro	k1gNnSc1	jádro
se	s	k7c7	s
průchodem	průchod	k1gInSc7	průchod
řídícího	řídící	k2eAgInSc2d1	řídící
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
schematu	schema	k1gNnSc6	schema
baterie	baterie	k1gFnSc2	baterie
B	B	kA	B
a	a	k8xC	a
Potenciometr	potenciometr	k1gInSc1	potenciometr
R	R	kA	R
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
nasycení	nasycení	k1gNnSc2	nasycení
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
odpor	odpor	k1gInSc1	odpor
(	(	kIx(	(
<g/>
induktance	induktance	k1gFnSc1	induktance
<g/>
)	)	kIx)	)
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
uvedeném	uvedený	k2eAgInSc6d1	uvedený
příkladu	příklad	k1gInSc6	příklad
zapojení	zapojení	k1gNnSc2	zapojení
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
reguluje	regulovat	k5eAaImIp3nS	regulovat
svítivost	svítivost	k1gFnSc4	svítivost
lampy	lampa	k1gFnSc2	lampa
L	L	kA	L
napájené	napájený	k2eAgMnPc4d1	napájený
střídavým	střídavý	k2eAgInSc7d1	střídavý
proudem	proud	k1gInSc7	proud
z	z	k7c2	z
generátoru	generátor	k1gInSc2	generátor
G.	G.	kA	G.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
regulační	regulační	k2eAgNnSc4d1	regulační
vinutí	vinutí	k1gNnSc4	vinutí
mnoho	mnoho	k6eAd1	mnoho
závitů	závit	k1gInPc2	závit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pro	pro	k7c4	pro
regulaci	regulace	k1gFnSc4	regulace
stačil	stačit	k5eAaBmAgInS	stačit
malý	malý	k2eAgInSc1d1	malý
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
indukuje	indukovat	k5eAaBmIp3nS	indukovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc4d1	velké
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
regulační	regulační	k2eAgInSc1d1	regulační
obvod	obvod	k1gInSc1	obvod
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tomuto	tento	k3xDgMnSc3	tento
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
<g/>
,	,	kIx,	,
použijí	použít	k5eAaPmIp3nP	použít
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
přesytky	přesytka	k1gFnPc1	přesytka
spojené	spojený	k2eAgFnPc1d1	spojená
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
transduktor	transduktor	k1gInSc4	transduktor
<g/>
.	.	kIx.	.
</s>
<s>
Pracovní	pracovní	k2eAgNnPc1d1	pracovní
vinutí	vinutí	k1gNnPc1	vinutí
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
přesytkách	přesytka	k1gFnPc6	přesytka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
propojeno	propojit	k5eAaPmNgNnS	propojit
do	do	k7c2	do
série	série	k1gFnSc2	série
(	(	kIx(	(
<g/>
sériový	sériový	k2eAgInSc1d1	sériový
transduktor	transduktor	k1gInSc1	transduktor
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
paralelně	paralelně	k6eAd1	paralelně
(	(	kIx(	(
<g/>
paralelní	paralelní	k2eAgInSc1d1	paralelní
transduktor	transduktor	k1gInSc1	transduktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
transduktor	transduktor	k1gInSc1	transduktor
se	s	k7c7	s
společným	společný	k2eAgNnSc7d1	společné
stejnosměrným	stejnosměrný	k2eAgNnSc7d1	stejnosměrné
vinutím	vinutí	k1gNnSc7	vinutí
pro	pro	k7c4	pro
obě	dva	k4xCgNnPc4	dva
jádra	jádro	k1gNnPc4	jádro
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
transformační	transformační	k2eAgInSc1d1	transformační
účinek	účinek	k1gInSc1	účinek
ruší	rušit	k5eAaImIp3nS	rušit
magneticky	magneticky	k6eAd1	magneticky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgInP	být
transduktory	transduktor	k1gInPc1	transduktor
široce	široko	k6eAd1	široko
používány	používat	k5eAaImNgInP	používat
pro	pro	k7c4	pro
regulaci	regulace	k1gFnSc4	regulace
otáček	otáčka	k1gFnPc2	otáčka
těžkých	těžký	k2eAgInPc2d1	těžký
elektromotorů	elektromotor	k1gInPc2	elektromotor
a	a	k8xC	a
regulaci	regulace	k1gFnSc4	regulace
osvětlení	osvětlení	k1gNnSc2	osvětlení
velkých	velký	k2eAgInPc2d1	velký
sálů	sál	k1gInPc2	sál
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
vytlačovány	vytlačovat	k5eAaImNgInP	vytlačovat
značně	značně	k6eAd1	značně
lehčími	lehký	k2eAgInPc7d2	lehčí
a	a	k8xC	a
menšími	malý	k2eAgInPc7d2	menší
tyristorovými	tyristorový	k2eAgInPc7d1	tyristorový
regulátory	regulátor	k1gInPc7	regulátor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ovšem	ovšem	k9	ovšem
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
širokého	široký	k2eAgNnSc2d1	široké
spektra	spektrum	k1gNnSc2	spektrum
rušících	rušící	k2eAgFnPc2d1	rušící
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Transduktorová	transduktorový	k2eAgFnSc1d1	transduktorový
regulace	regulace	k1gFnSc1	regulace
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
moderními	moderní	k2eAgInPc7d1	moderní
regulátory	regulátor	k1gInPc7	regulátor
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
v	v	k7c6	v
pomalejší	pomalý	k2eAgFnSc6d2	pomalejší
odezvě	odezva	k1gFnSc6	odezva
–	–	k?	–
řádově	řádově	k6eAd1	řádově
desetinásobek	desetinásobek	k1gInSc4	desetinásobek
periody	perioda	k1gFnSc2	perioda
regulovaného	regulovaný	k2eAgInSc2d1	regulovaný
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Transduktory	transduktor	k1gInPc1	transduktor
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
,	,	kIx,	,
přetížitelnost	přetížitelnost	k1gFnSc4	přetížitelnost
a	a	k8xC	a
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
transduktory	transduktor	k1gInPc1	transduktor
používány	používat	k5eAaImNgInP	používat
jako	jako	k8xS	jako
převodníky	převodník	k1gInPc1	převodník
resp.	resp.	kA	resp.
proudová	proudový	k2eAgNnPc4d1	proudové
čidla	čidlo	k1gNnPc4	čidlo
měřících	měřící	k2eAgInPc2d1	měřící
obvodů	obvod	k1gInPc2	obvod
ve	v	k7c6	v
výkonové	výkonový	k2eAgFnSc6d1	výkonová
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
kompenzace	kompenzace	k1gFnSc2	kompenzace
–	–	k?	–
na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
prochází	procházet	k5eAaImIp3nS	procházet
vodič	vodič	k1gInSc4	vodič
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
navinuto	navinut	k2eAgNnSc1d1	navinuto
další	další	k2eAgNnSc1d1	další
vinutí	vinutí	k1gNnSc1	vinutí
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
prochází	procházet	k5eAaImIp3nP	procházet
takový	takový	k3xDgInSc4	takový
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obě	dva	k4xCgNnPc1	dva
pole	pole	k1gNnPc1	pole
vzájemně	vzájemně	k6eAd1	vzájemně
vyruší	vyrušit	k5eAaPmIp3nP	vyrušit
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
tohoto	tento	k3xDgInSc2	tento
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
úměrná	úměrný	k2eAgFnSc1d1	úměrná
měřenému	měřený	k2eAgInSc3d1	měřený
proudu	proud	k1gInSc3	proud
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
měřící	měřící	k2eAgFnSc1d1	měřící
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
i	i	k9	i
pro	pro	k7c4	pro
vysokofrekvenční	vysokofrekvenční	k2eAgInPc4d1	vysokofrekvenční
proudy	proud	k1gInPc4	proud
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kromě	kromě	k7c2	kromě
regulované	regulovaný	k2eAgFnSc2d1	regulovaná
kompenzace	kompenzace	k1gFnSc2	kompenzace
zde	zde	k6eAd1	zde
také	také	k9	také
působí	působit	k5eAaImIp3nS	působit
magnetická	magnetický	k2eAgFnSc1d1	magnetická
vazba	vazba	k1gFnSc1	vazba
na	na	k7c4	na
užitečný	užitečný	k2eAgInSc4d1	užitečný
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
rychlých	rychlý	k2eAgFnPc2d1	rychlá
změn	změna	k1gFnPc2	změna
se	se	k3xPyFc4	se
zařízení	zařízení	k1gNnSc1	zařízení
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
transformátor	transformátor	k1gInSc4	transformátor
a	a	k8xC	a
pro	pro	k7c4	pro
nízké	nízký	k2eAgFnPc4d1	nízká
frekvence	frekvence	k1gFnPc4	frekvence
a	a	k8xC	a
stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
regulované	regulovaný	k2eAgFnSc2d1	regulovaná
kompenzace	kompenzace	k1gFnSc2	kompenzace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Transduktor_	Transduktor_	k1gFnSc2	Transduktor_
<g/>
(	(	kIx(	(
<g/>
Elektrotechnik	elektrotechnik	k1gMnSc1	elektrotechnik
<g/>
)	)	kIx)	)
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
