<s>
Transduktor	transduktor	k1gInSc1	transduktor
jinak	jinak	k6eAd1	jinak
též	též	k9	též
magnetický	magnetický	k2eAgInSc4d1	magnetický
zesilovač	zesilovač	k1gInSc4	zesilovač
je	být	k5eAaImIp3nS	být
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
resp.	resp.	kA	resp.
elektronická	elektronický	k2eAgFnSc1d1	elektronická
součást	součást	k1gFnSc1	součást
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
nebo	nebo	k8xC	nebo
měření	měření	k1gNnSc3	měření
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
střídavých	střídavý	k2eAgMnPc2d1	střídavý
<g/>
.	.	kIx.	.
</s>
