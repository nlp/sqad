<p>
<s>
Vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
účinky	účinek	k1gInPc1	účinek
znamenají	znamenat	k5eAaImIp3nP	znamenat
v	v	k7c6	v
lékařství	lékařství	k1gNnSc2	lékařství
efekty	efekt	k1gInPc4	efekt
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
léčebné	léčebný	k2eAgInPc4d1	léčebný
<g/>
,	,	kIx,	,
či	či	k8xC	či
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
zamýšlené	zamýšlený	k2eAgInPc4d1	zamýšlený
hlavní	hlavní	k2eAgInPc4d1	hlavní
účinky	účinek	k1gInPc4	účinek
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
též	též	k9	též
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
prospěšné	prospěšný	k2eAgNnSc4d1	prospěšné
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nezamýšlené	zamýšlený	k2eNgInPc1d1	nezamýšlený
následky	následek	k1gInPc1	následek
použití	použití	k1gNnSc2	použití
léků	lék	k1gInPc2	lék
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
léky	lék	k1gInPc1	lék
nebo	nebo	k8xC	nebo
léčebné	léčebný	k2eAgFnPc1d1	léčebná
procedury	procedura	k1gFnPc1	procedura
používají	používat	k5eAaImIp3nP	používat
právě	právě	k9	právě
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgInPc4	svůj
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
přestávají	přestávat	k5eAaImIp3nP	přestávat
tyto	tento	k3xDgInPc1	tento
účinky	účinek	k1gInPc1	účinek
být	být	k5eAaImF	být
vedlejšími	vedlejší	k2eAgInPc7d1	vedlejší
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	s	k7c7	s
záměrnými	záměrná	k1gFnPc7	záměrná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
i	i	k8xC	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používalo	používat	k5eAaImAgNnS	používat
<g/>
/	/	kIx~	/
<g/>
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
technika	technika	k1gFnSc1	technika
při	při	k7c6	při
lékařských	lékařský	k2eAgNnPc6d1	lékařské
vyšetřeních	vyšetření	k1gNnPc6	vyšetření
<g/>
,	,	kIx,	,
díky	dík	k1gInPc7	dík
objevu	objev	k1gInSc2	objev
jeho	jeho	k3xOp3gFnPc2	jeho
onkolytických	onkolytický	k2eAgFnPc2d1	onkolytický
schopností	schopnost	k1gFnPc2	schopnost
(	(	kIx(	(
<g/>
destrukce	destrukce	k1gFnSc1	destrukce
nádorových	nádorový	k2eAgFnPc2d1	nádorová
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
využívat	využívat	k5eAaPmF	využívat
i	i	k9	i
v	v	k7c6	v
radioterapii	radioterapie	k1gFnSc6	radioterapie
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
(	(	kIx(	(
<g/>
ablaci	ablace	k1gFnSc6	ablace
<g/>
)	)	kIx)	)
zhoubných	zhoubný	k2eAgInPc2d1	zhoubný
nádorů	nádor	k1gInPc2	nádor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Frekvence	frekvence	k1gFnSc1	frekvence
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
účinků	účinek	k1gInPc2	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
pacienta	pacient	k1gMnSc2	pacient
dostaví	dostavit	k5eAaPmIp3nP	dostavit
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
jako	jako	k9	jako
:	:	kIx,	:
</s>
</p>
<p>
<s>
Častá	častý	k2eAgFnSc1d1	častá
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
až	až	k9	až
1	[number]	k4	1
z	z	k7c2	z
10	[number]	k4	10
pacientů	pacient	k1gMnPc2	pacient
</s>
</p>
<p>
<s>
Méně	málo	k6eAd2	málo
častá	častý	k2eAgFnSc1d1	častá
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
až	až	k9	až
1	[number]	k4	1
ze	z	k7c2	z
100	[number]	k4	100
pacientů	pacient	k1gMnPc2	pacient
</s>
</p>
<p>
<s>
Vzácná	vzácný	k2eAgFnSc1d1	vzácná
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
až	až	k9	až
1	[number]	k4	1
z	z	k7c2	z
1000	[number]	k4	1000
pacientů	pacient	k1gMnPc2	pacient
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
vzácná	vzácný	k2eAgFnSc1d1	vzácná
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
až	až	k9	až
1	[number]	k4	1
z	z	k7c2	z
10	[number]	k4	10
000	[number]	k4	000
pacientů	pacient	k1gMnPc2	pacient
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
účinků	účinek	k1gInPc2	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Třapatka	Třapatka	k1gFnSc1	Třapatka
(	(	kIx(	(
<g/>
Echinacea	Echinace	k2eAgFnSc1d1	Echinacea
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
někde	někde	k6eAd1	někde
užívala	užívat	k5eAaImAgFnS	užívat
v	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
přes	přes	k7c4	přes
20	[number]	k4	20
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
astmatických	astmatický	k2eAgInPc2d1	astmatický
záchvatů	záchvat	k1gInPc2	záchvat
<g/>
,	,	kIx,	,
potratů	potrat	k1gInPc2	potrat
<g/>
,	,	kIx,	,
kopřivky	kopřivka	k1gFnSc2	kopřivka
<g/>
,	,	kIx,	,
otoků	otok	k1gInPc2	otok
<g/>
,	,	kIx,	,
bolestí	bolest	k1gFnPc2	bolest
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
zažívacích	zažívací	k2eAgFnPc2d1	zažívací
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Side	Sid	k1gFnSc2	Sid
effect	effecta	k1gFnPc2	effecta
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgFnPc4d1	související
stránky	stránka	k1gFnPc4	stránka
==	==	k?	==
</s>
</p>
<p>
<s>
Farmakogenetika	Farmakogenetika	k1gFnSc1	Farmakogenetika
</s>
</p>
