<s>
Například	například	k6eAd1	například
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
i	i	k8xC	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používalo	používat	k5eAaImAgNnS	používat
<g/>
/	/	kIx~	/
<g/>
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
technika	technika	k1gFnSc1	technika
při	při	k7c6	při
lékařských	lékařský	k2eAgNnPc6d1	lékařské
vyšetřeních	vyšetření	k1gNnPc6	vyšetření
<g/>
,	,	kIx,	,
díky	dík	k1gInPc7	dík
objevu	objev	k1gInSc2	objev
jeho	jeho	k3xOp3gFnPc2	jeho
onkolytických	onkolytický	k2eAgFnPc2d1	onkolytický
schopností	schopnost	k1gFnPc2	schopnost
(	(	kIx(	(
<g/>
destrukce	destrukce	k1gFnSc1	destrukce
nádorových	nádorový	k2eAgFnPc2d1	nádorová
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
využívat	využívat	k5eAaImF	využívat
i	i	k9	i
v	v	k7c6	v
radioterapii	radioterapie	k1gFnSc6	radioterapie
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
(	(	kIx(	(
<g/>
ablaci	ablace	k1gFnSc6	ablace
<g/>
)	)	kIx)	)
zhoubných	zhoubný	k2eAgInPc2d1	zhoubný
nádorů	nádor	k1gInPc2	nádor
<g/>
.	.	kIx.	.
</s>
