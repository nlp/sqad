<p>
<s>
Volyňský	volyňský	k2eAgInSc1d1	volyňský
masakr	masakr	k1gInSc1	masakr
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
masové	masový	k2eAgFnPc4d1	masová
vraždy	vražda	k1gFnPc4	vražda
(	(	kIx(	(
<g/>
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
a	a	k8xC	a
genocidu	genocida	k1gFnSc4	genocida
<g/>
)	)	kIx)	)
páchané	páchaný	k2eAgFnPc4d1	páchaná
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
do	do	k7c2	do
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
okupovaného	okupovaný	k2eAgNnSc2d1	okupované
území	území	k1gNnSc2	území
předválečného	předválečný	k2eAgNnSc2d1	předválečné
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zločiny	zločin	k1gInPc1	zločin
byly	být	k5eAaImAgInP	být
naplánovány	naplánovat	k5eAaBmNgInP	naplánovat
a	a	k8xC	a
spáchány	spáchat	k5eAaPmNgInP	spáchat
ukrajinskými	ukrajinský	k2eAgFnPc7d1	ukrajinská
nacionalisty	nacionalista	k1gMnPc7	nacionalista
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
podporou	podpora	k1gFnSc7	podpora
místního	místní	k2eAgNnSc2d1	místní
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
především	především	k9	především
vůči	vůči	k7c3	vůči
tamější	tamější	k2eAgFnSc6d1	tamější
polské	polský	k2eAgFnSc6d1	polská
menšině	menšina	k1gFnSc6	menšina
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
1942	[number]	k4	1942
roce	rok	k1gInSc6	rok
Němci	Němec	k1gMnPc7	Němec
evidovali	evidovat	k5eAaImAgMnP	evidovat
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
306	[number]	k4	306
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
(	(	kIx(	(
<g/>
14,6	[number]	k4	14,6
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
žilo	žít	k5eAaImAgNnS	žít
343	[number]	k4	343
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
16,5	[number]	k4	16,5
%	%	kIx~	%
z	z	k7c2	z
2	[number]	k4	2
086	[number]	k4	086
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnSc1	obyvatel
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
15,1	[number]	k4	15,1
%	%	kIx~	%
(	(	kIx(	(
<g/>
273,5	[number]	k4	273,5
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
27,5	[number]	k4	27,5
%	%	kIx~	%
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
69,5	[number]	k4	69,5
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
zjišťovalo	zjišťovat	k5eAaImAgNnS	zjišťovat
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
stavem	stav	k1gInSc7	stav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
a	a	k8xC	a
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
bojovými	bojový	k2eAgFnPc7d1	bojová
operacemi	operace	k1gFnPc7	operace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
represemi	represe	k1gFnPc7	represe
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
sovětských	sovětský	k2eAgInPc2d1	sovětský
orgánů	orgán	k1gInPc2	orgán
v	v	k7c6	v
době	doba	k1gFnSc6	doba
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
Richarda	Richard	k1gMnSc2	Richard
Torzeckého	Torzecký	k2eAgInSc2d1	Torzecký
představovali	představovat	k5eAaImAgMnP	představovat
Poláci	Polák	k1gMnPc1	Polák
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
ve	v	k7c6	v
venkovských	venkovský	k2eAgFnPc6d1	venkovská
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
15,1	[number]	k4	15,1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
vnímají	vnímat	k5eAaImIp3nP	vnímat
tuto	tento	k3xDgFnSc4	tento
genocidu	genocida	k1gFnSc4	genocida
jako	jako	k8xS	jako
konflikt	konflikt	k1gInSc4	konflikt
dvou	dva	k4xCgInPc2	dva
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
docházelo	docházet	k5eAaImAgNnS	docházet
i	i	k9	i
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
ukrajinské	ukrajinský	k2eAgNnSc4d1	ukrajinské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
genocidy	genocida	k1gFnSc2	genocida
prováděné	prováděný	k2eAgInPc1d1	prováděný
členy	člen	k1gInPc1	člen
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
civilisty	civilista	k1gMnPc4	civilista
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
národnosti	národnost	k1gFnSc2	národnost
bylo	být	k5eAaImAgNnS	být
vyčistit	vyčistit	k5eAaPmF	vyčistit
území	území	k1gNnSc4	území
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
územním	územní	k2eAgNnSc6d1	územní
nárokům	nárok	k1gInPc3	nárok
jiných	jiný	k2eAgMnPc2d1	jiný
států	stát	k1gInPc2	stát
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oběťmi	oběť	k1gFnPc7	oběť
vražedných	vražedný	k2eAgFnPc2d1	vražedná
akcí	akce	k1gFnPc2	akce
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
Ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
povstaleckou	povstalecký	k2eAgFnSc7d1	povstalecká
armádou	armáda	k1gFnSc7	armáda
(	(	kIx(	(
<g/>
UPA	UPA	kA	UPA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
hlavně	hlavně	k9	hlavně
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
menším	malý	k2eAgNnSc6d2	menší
měřítku	měřítko	k1gNnSc6	měřítko
pak	pak	k6eAd1	pak
i	i	k9	i
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
(	(	kIx(	(
<g/>
opozice	opozice	k1gFnSc1	opozice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Židé	Žid	k1gMnPc1	Žid
(	(	kIx(	(
<g/>
zbytky	zbytek	k1gInPc1	zbytek
židovské	židovský	k2eAgFnSc2d1	židovská
populace	populace	k1gFnSc2	populace
vyvražděné	vyvražděný	k2eAgFnPc1d1	vyvražděná
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
kolaborující	kolaborující	k2eAgInPc1d1	kolaborující
Ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
pomocnou	pomocný	k2eAgFnSc7d1	pomocná
policií	policie	k1gFnSc7	policie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arméni	Armén	k1gMnPc1	Armén
<g/>
,	,	kIx,	,
Volyňští	volyňský	k2eAgMnPc1d1	volyňský
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
žijících	žijící	k2eAgFnPc2d1	žijící
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
polské	polský	k2eAgFnSc2d1	polská
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
organizace	organizace	k1gFnSc2	organizace
AK	AK	kA	AK
zakázalo	zakázat	k5eAaPmAgNnS	zakázat
svým	svůj	k3xOyFgInSc7	svůj
jednotkám	jednotka	k1gFnPc3	jednotka
odvetné	odvetný	k2eAgFnSc2d1	odvetná
akce	akce	k1gFnSc2	akce
na	na	k7c6	na
ukrajinských	ukrajinský	k2eAgMnPc6d1	ukrajinský
civilistech	civilista	k1gMnPc6	civilista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
sloužili	sloužit	k5eAaImAgMnP	sloužit
rodinní	rodinní	k2eAgMnPc1d1	rodinní
příslušníci	příslušník	k1gMnPc1	příslušník
povražděných	povražděný	k2eAgFnPc2d1	povražděná
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
omezenějším	omezený	k2eAgNnSc6d2	omezenější
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
vypálily	vypálit	k5eAaPmAgFnP	vypálit
několik	několik	k4yIc4	několik
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
přesný	přesný	k2eAgInSc1d1	přesný
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
až	až	k9	až
60	[number]	k4	60
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
2	[number]	k4	2
000	[number]	k4	000
až	až	k9	až
3	[number]	k4	3
000	[number]	k4	000
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
.	.	kIx.	.
<g/>
Obdobný	obdobný	k2eAgInSc4d1	obdobný
masakr	masakr	k1gInSc4	masakr
UPA	UPA	kA	UPA
spáchala	spáchat	k5eAaPmAgFnS	spáchat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
také	také	k6eAd1	také
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Haliči	Halič	k1gFnSc6	Halič
a	a	k8xC	a
na	na	k7c6	na
Podolí	Podolí	k1gNnSc6	Podolí
(	(	kIx(	(
<g/>
25	[number]	k4	25
000	[number]	k4	000
až	až	k9	až
70	[number]	k4	70
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Lubelsku	Lubelsko	k1gNnSc6	Lubelsko
a	a	k8xC	a
v	v	k7c6	v
Polesí	Polesí	k1gNnSc6	Polesí
(	(	kIx(	(
<g/>
10	[number]	k4	10
000	[number]	k4	000
až	až	k9	až
20	[number]	k4	20
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
)	)	kIx)	)
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Polský	polský	k2eAgMnSc1d1	polský
prezident	prezident	k1gMnSc1	prezident
Aleksander	Aleksander	k1gMnSc1	Aleksander
Kwaśniewski	Kwaśniewske	k1gFnSc4	Kwaśniewske
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
prezident	prezident	k1gMnSc1	prezident
Petro	Petra	k1gFnSc5	Petra
Porošenko	Porošenka	k1gFnSc5	Porošenka
požádal	požádat	k5eAaPmAgMnS	požádat
Poláky	Polák	k1gMnPc4	Polák
za	za	k7c4	za
odpuštění	odpuštění	k1gNnSc4	odpuštění
za	za	k7c4	za
tragédii	tragédie	k1gFnSc4	tragédie
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
,	,	kIx,	,
při	při	k7c6	při
projevu	projev	k1gInSc6	projev
před	před	k7c7	před
oběma	dva	k4xCgFnPc7	dva
komorami	komora	k1gFnPc7	komora
polského	polský	k2eAgInSc2d1	polský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgMnS	být
také	také	k9	také
přítomen	přítomen	k2eAgMnSc1d1	přítomen
prezident	prezident	k1gMnSc1	prezident
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
v	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
prezident	prezident	k1gMnSc1	prezident
Porošenko	Porošenka	k1gFnSc5	Porošenka
označil	označit	k5eAaPmAgMnS	označit
bojovníky	bojovník	k1gMnPc4	bojovník
UPA	UPA	kA	UPA
za	za	k7c4	za
"	"	kIx"	"
<g/>
příklad	příklad	k1gInSc4	příklad
hrdinství	hrdinství	k1gNnSc2	hrdinství
a	a	k8xC	a
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
Parlament	parlament	k1gInSc1	parlament
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
schválil	schválit	k5eAaPmAgInS	schválit
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
udělil	udělit	k5eAaPmAgMnS	udělit
příslušníkům	příslušník	k1gMnPc3	příslušník
OUN	OUN	kA	OUN
a	a	k8xC	a
UPA	UPA	kA	UPA
status	status	k1gInSc1	status
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Organizace	organizace	k1gFnSc2	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
(	(	kIx(	(
<g/>
OUN	OUN	kA	OUN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
UPA	UPA	kA	UPA
systematické	systematický	k2eAgFnSc2d1	systematická
vyvražďování	vyvražďování	k1gNnSc6	vyvražďování
organizovala	organizovat	k5eAaBmAgFnS	organizovat
a	a	k8xC	a
prováděla	provádět	k5eAaImAgFnS	provádět
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
masakrech	masakr	k1gInPc6	masakr
podíl	podíl	k1gInSc4	podíl
osoby	osoba	k1gFnSc2	osoba
vedené	vedený	k2eAgFnSc2d1	vedená
údajně	údajně	k6eAd1	údajně
zahraničními	zahraniční	k2eAgInPc7d1	zahraniční
zájmy	zájem	k1gInPc7	zájem
–	–	k?	–
německými	německý	k2eAgFnPc7d1	německá
a	a	k8xC	a
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vypuknutí	vypuknutí	k1gNnSc4	vypuknutí
událostí	událost	k1gFnPc2	událost
proti	proti	k7c3	proti
Polákům	Polák	k1gMnPc3	Polák
měla	mít	k5eAaImAgFnS	mít
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
obhajoby	obhajoba	k1gFnSc2	obhajoba
mít	mít	k5eAaImF	mít
vinu	vina	k1gFnSc4	vina
národnostní	národnostní	k2eAgFnSc1d1	národnostní
politika	politika	k1gFnSc1	politika
druhé	druhý	k4xOgFnSc2	druhý
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
utlačování	utlačování	k1gNnSc1	utlačování
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Poláků	Polák	k1gMnPc2	Polák
v	v	k7c6	v
Chelmně	Chelmně	k1gFnSc6	Chelmně
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
polských	polský	k2eAgFnPc2d1	polská
a	a	k8xC	a
německých	německý	k2eAgFnPc2d1	německá
okupačních	okupační	k2eAgFnPc2d1	okupační
sil	síla	k1gFnPc2	síla
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
partyzány	partyzána	k1gFnPc1	partyzána
a	a	k8xC	a
masakry	masakr	k1gInPc1	masakr
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
prováděné	prováděný	k2eAgInPc1d1	prováděný
Poláky	polák	k1gInPc1	polák
sloužícími	sloužící	k1gMnPc7	sloužící
v	v	k7c6	v
kolaborantských	kolaborantský	k2eAgFnPc6d1	kolaborantská
německých	německý	k2eAgFnPc6d1	německá
policejních	policejní	k2eAgFnPc6d1	policejní
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
pozadí	pozadí	k1gNnSc1	pozadí
událostí	událost	k1gFnPc2	událost
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
pakt	pakt	k1gInSc1	pakt
potvrzující	potvrzující	k2eAgFnSc4d1	potvrzující
spolupráci	spolupráce	k1gFnSc4	spolupráce
mezi	mezi	k7c7	mezi
Třetí	třetí	k4xOgFnSc7	třetí
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
německé	německý	k2eAgFnSc2d1	německá
ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
síly	síla	k1gFnSc2	síla
obsadily	obsadit	k5eAaPmAgFnP	obsadit
polské	polský	k2eAgNnSc4d1	polské
území	území	k1gNnSc4	území
a	a	k8xC	a
Volyň	Volyň	k1gFnSc1	Volyň
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
vojsky	vojsky	k6eAd1	vojsky
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
společné	společný	k2eAgFnSc6d1	společná
agresi	agrese	k1gFnSc6	agrese
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
komunistického	komunistický	k2eAgInSc2d1	komunistický
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
proti	proti	k7c3	proti
Polsku	Polsko	k1gNnSc3	Polsko
v	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
tak	tak	k8xS	tak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úplné	úplný	k2eAgFnSc3d1	úplná
okupaci	okupace	k1gFnSc3	okupace
území	území	k1gNnSc2	území
Druhé	druhý	k4xOgFnSc2	druhý
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
wehrmachtem	wehrmacht	k1gInSc7	wehrmacht
a	a	k8xC	a
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
odůvodnění	odůvodnění	k1gNnSc1	odůvodnění
anexe	anexe	k1gFnSc2	anexe
okupovaných	okupovaný	k2eAgNnPc2d1	okupované
území	území	k1gNnPc2	území
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
založila	založit	k5eAaPmAgFnS	založit
sovětská	sovětský	k2eAgFnSc1d1	sovětská
moc	moc	k1gFnSc1	moc
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1939	[number]	k4	1939
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
strachu	strach	k1gInSc2	strach
po	po	k7c6	po
fiktivních	fiktivní	k2eAgFnPc6d1	fiktivní
volbách	volba	k1gFnPc6	volba
místní	místní	k2eAgNnSc1d1	místní
lidové	lidový	k2eAgNnSc1d1	lidové
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Západoukrajinské	Západoukrajinský	k2eAgNnSc1d1	Západoukrajinský
lidové	lidový	k2eAgNnSc1d1	lidové
shromáždění	shromáždění	k1gNnSc1	shromáždění
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1939	[number]	k4	1939
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahrnulo	zahrnout	k5eAaPmAgNnS	zahrnout
Volyň	Volyň	k1gFnSc4	Volyň
do	do	k7c2	do
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
prezídium	prezídium	k1gNnSc1	prezídium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
SSSR	SSSR	kA	SSSR
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
německém	německý	k2eAgInSc6d1	německý
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
do	do	k7c2	do
území	území	k1gNnSc2	území
nazývaného	nazývaný	k2eAgNnSc2d1	nazývané
Reichskommissariat	Reichskommissariat	k1gInSc4	Reichskommissariat
Ukraine	Ukrain	k1gInSc5	Ukrain
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
expanzivní	expanzivní	k2eAgFnSc7d1	expanzivní
politikou	politika	k1gFnSc7	politika
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
vůči	vůči	k7c3	vůči
celému	celý	k2eAgInSc3d1	celý
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Drang	Drang	k1gInSc1	Drang
nach	nach	k1gInSc1	nach
Osten	osten	k1gInSc4	osten
<g/>
)	)	kIx)	)
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
materiální	materiální	k2eAgInSc4d1	materiální
zdroj	zdroj	k1gInSc4	zdroj
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
povolit	povolit	k5eAaPmF	povolit
existenci	existence	k1gFnSc4	existence
jiné	jiný	k2eAgFnSc2d1	jiná
vlády	vláda	k1gFnSc2	vláda
než	než	k8xS	než
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
vykořisťování	vykořisťování	k1gNnSc1	vykořisťování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
vysokých	vysoký	k2eAgInPc6d1	vysoký
odvodech	odvod	k1gInPc6	odvod
(	(	kIx(	(
<g/>
daních	daň	k1gFnPc6	daň
<g/>
)	)	kIx)	)
uložených	uložený	k2eAgFnPc2d1	uložená
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c4	po
zahájení	zahájení	k1gNnSc4	zahájení
likvidace	likvidace	k1gFnSc2	likvidace
ghett	ghetto	k1gNnPc2	ghetto
a	a	k8xC	a
masového	masový	k2eAgNnSc2d1	masové
vyvražďování	vyvražďování	k1gNnSc2	vyvražďování
Židů	Žid	k1gMnPc2	Žid
(	(	kIx(	(
<g/>
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnSc1d1	místní
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
policie	policie	k1gFnSc1	policie
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
oddíly	oddíl	k1gInPc7	oddíl
německé	německý	k2eAgFnSc2d1	německá
SS	SS	kA	SS
(	(	kIx(	(
<g/>
Schutzstaffel	Schutzstaffel	k1gMnSc1	Schutzstaffel
<g/>
)	)	kIx)	)
při	při	k7c6	při
policejních	policejní	k2eAgFnPc6d1	policejní
raziích	razie	k1gFnPc6	razie
v	v	k7c6	v
ghettech	ghetto	k1gNnPc6	ghetto
<g/>
,	,	kIx,	,
eskortovala	eskortovat	k5eAaBmAgFnS	eskortovat
Židy	Žid	k1gMnPc4	Žid
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
exekuce	exekuce	k1gFnSc2	exekuce
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zúčastňovala	zúčastňovat	k5eAaImAgFnS	zúčastňovat
poprav	poprava	k1gFnPc2	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
holocaustu	holocaust	k1gInSc6	holocaust
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
aparátem	aparát	k1gInSc7	aparát
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
asi	asi	k9	asi
150	[number]	k4	150
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
a	a	k8xC	a
455	[number]	k4	455
000	[number]	k4	000
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Haliči	Halič	k1gFnSc6	Halič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Genocida	genocida	k1gFnSc1	genocida
prováděná	prováděný	k2eAgFnSc1d1	prováděná
německými	německý	k2eAgMnPc7d1	německý
okupanty	okupant	k1gMnPc7	okupant
na	na	k7c6	na
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
židovské	židovský	k2eAgFnSc2d1	židovská
národnosti	národnost	k1gFnSc2	národnost
–	–	k?	–
masové	masový	k2eAgFnSc2d1	masová
deportace	deportace	k1gFnSc2	deportace
<g/>
,	,	kIx,	,
zatýkání	zatýkání	k1gNnSc6	zatýkání
a	a	k8xC	a
masové	masový	k2eAgNnSc1d1	masové
vraždění	vraždění	k1gNnSc1	vraždění
obyvatel	obyvatel	k1gMnPc2	obyvatel
východních	východní	k2eAgNnPc2d1	východní
pohraničí	pohraničí	k1gNnPc2	pohraničí
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
také	také	k9	také
uskutečňována	uskutečňován	k2eAgNnPc1d1	uskutečňováno
během	během	k7c2	během
sovětského	sovětský	k2eAgNnSc2d1	sovětské
období	období	k1gNnSc2	období
1939	[number]	k4	1939
–	–	k?	–
1941	[number]	k4	1941
jednotkami	jednotka	k1gFnPc7	jednotka
NKVD	NKVD	kA	NKVD
<g/>
.	.	kIx.	.
</s>
<s>
Nemorálnost	nemorálnost	k1gFnSc1	nemorálnost
a	a	k8xC	a
pronásledování	pronásledování	k1gNnSc1	pronásledování
státními	státní	k2eAgFnPc7d1	státní
institucemi	instituce	k1gFnPc7	instituce
namířená	namířený	k2eAgFnSc1d1	namířená
proti	proti	k7c3	proti
celým	celý	k2eAgFnPc3d1	celá
etnickým	etnický	k2eAgFnPc3d1	etnická
či	či	k8xC	či
sociálním	sociální	k2eAgFnPc3d1	sociální
skupinám	skupina	k1gFnPc3	skupina
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
společenského	společenský	k2eAgNnSc2d1	společenské
klimatu	klima	k1gNnSc2	klima
lhostejnosti	lhostejnost	k1gFnSc2	lhostejnost
k	k	k7c3	k
násilí	násilí	k1gNnSc3	násilí
<g/>
,	,	kIx,	,
zločinu	zločin	k1gInSc2	zločin
a	a	k8xC	a
masovému	masový	k2eAgInSc3d1	masový
zločinu	zločin	k1gInSc3	zločin
–	–	k?	–
genocidě	genocida	k1gFnSc3	genocida
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlazení	vyhlazení	k1gNnSc1	vyhlazení
Židů	Žid	k1gMnPc2	Žid
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
pro	pro	k7c4	pro
ukrajinské	ukrajinský	k2eAgMnPc4d1	ukrajinský
nacionalisty	nacionalista	k1gMnPc4	nacionalista
příkladem	příklad	k1gInSc7	příklad
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
lze	lze	k6eAd1	lze
odstranit	odstranit	k5eAaPmF	odstranit
Poláky	Polák	k1gMnPc4	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zločinů	zločin	k1gMnPc2	zločin
sovětských	sovětský	k2eAgFnPc2d1	sovětská
a	a	k8xC	a
německých	německý	k2eAgFnPc2d1	německá
deportací	deportace	k1gFnPc2	deportace
se	se	k3xPyFc4	se
populace	populace	k1gFnPc1	populace
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
snížila	snížit	k5eAaPmAgFnS	snížit
z	z	k7c2	z
odhadovaných	odhadovaný	k2eAgInPc2d1	odhadovaný
2,3	[number]	k4	2,3
milionu	milion	k4xCgInSc2	milion
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1939	[number]	k4	1939
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Židů	Žid	k1gMnPc2	Žid
relativně	relativně	k6eAd1	relativně
největší	veliký	k2eAgFnSc2d3	veliký
ztráty	ztráta	k1gFnSc2	ztráta
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
polská	polský	k2eAgFnSc1d1	polská
menšina	menšina	k1gFnSc1	menšina
–	–	k?	–
asi	asi	k9	asi
45	[number]	k4	45
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mnoha	mnoho	k4c2	mnoho
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
vedení	vedení	k1gNnSc2	vedení
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
Němci	Němec	k1gMnPc1	Němec
odhadli	odhadnout	k5eAaPmAgMnP	odhadnout
počet	počet	k1gInSc4	počet
Poláků	Polák	k1gMnPc2	Polák
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
na	na	k7c4	na
306.000	[number]	k4	306.000
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
14,6	[number]	k4	14,6
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
nemilosrdný	milosrdný	k2eNgInSc1d1	nemilosrdný
postup	postup	k1gInSc1	postup
německých	německý	k2eAgInPc2d1	německý
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
vysoké	vysoký	k2eAgFnPc4d1	vysoká
daně	daň	k1gFnPc4	daň
<g/>
,	,	kIx,	,
popravy	poprava	k1gFnPc4	poprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
samovolnému	samovolný	k2eAgInSc3d1	samovolný
vývoji	vývoj	k1gInSc3	vývoj
partyzánského	partyzánský	k2eAgNnSc2d1	partyzánské
hnutí	hnutí	k1gNnSc2	hnutí
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc4	žádný
politickou	politický	k2eAgFnSc4d1	politická
organizaci	organizace	k1gFnSc4	organizace
a	a	k8xC	a
sovětských	sovětský	k2eAgMnPc2d1	sovětský
partyzánů	partyzán	k1gMnPc2	partyzán
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tři	tři	k4xCgFnPc4	tři
ukrajinské	ukrajinský	k2eAgFnPc4d1	ukrajinská
stranické	stranický	k2eAgFnPc4d1	stranická
formace	formace	k1gFnPc4	formace
<g/>
:	:	kIx,	:
největší	veliký	k2eAgFnPc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
Síč	Síč	k1gFnSc1	Síč
Polesí	Polesí	k1gNnSc2	Polesí
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
První	první	k4xOgFnPc4	první
UPA	UPA	kA	UPA
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
byli	být	k5eAaImAgMnP	být
nacionalističtí	nacionalistický	k2eAgMnPc1d1	nacionalistický
povstalci	povstalec	k1gMnPc1	povstalec
OUN-M	OUN-M	k1gFnSc2	OUN-M
(	(	kIx(	(
<g/>
OUN-Melnyk	OUN-Melnyk	k1gInSc1	OUN-Melnyk
<g/>
)	)	kIx)	)
a	a	k8xC	a
malé	malý	k2eAgFnPc1d1	malá
organizace	organizace	k1gFnPc1	organizace
nacionalistů	nacionalista	k1gMnPc2	nacionalista
později	pozdě	k6eAd2	pozdě
banderovské	banderovský	k2eAgFnSc2d1	banderovská
OUN-SD	OUN-SD	k1gFnSc2	OUN-SD
(	(	kIx(	(
<g/>
OUN-R	OUN-R	k1gMnSc1	OUN-R
a	a	k8xC	a
OUN-B	OUN-B	k1gMnSc1	OUN-B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
organizace	organizace	k1gFnSc1	organizace
OUN-SD	OUN-SD	k1gFnSc1	OUN-SD
byla	být	k5eAaImAgFnS	být
nejdynamičtější	dynamický	k2eAgFnSc7d3	nejdynamičtější
částí	část	k1gFnSc7	část
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
sjednotit	sjednotit	k5eAaPmF	sjednotit
všechny	všechen	k3xTgFnPc4	všechen
ukrajinské	ukrajinský	k2eAgFnPc4d1	ukrajinská
stranické	stranický	k2eAgFnPc4d1	stranická
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
složky	složka	k1gFnPc4	složka
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
násilím	násilí	k1gNnSc7	násilí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
okamžitým	okamžitý	k2eAgFnPc3d1	okamžitá
partyzánským	partyzánský	k2eAgFnPc3d1	Partyzánská
akcím	akce	k1gFnPc3	akce
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
hromadná	hromadný	k2eAgFnSc1d1	hromadná
dezerce	dezerce	k1gFnSc1	dezerce
policistů	policista	k1gMnPc2	policista
ukrajinských	ukrajinský	k2eAgInPc2d1	ukrajinský
pomocných	pomocný	k2eAgInPc2d1	pomocný
policejních	policejní	k2eAgInPc2d1	policejní
sborů	sbor	k1gInPc2	sbor
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
dezertérů	dezertér	k1gMnPc2	dezertér
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
k	k	k7c3	k
vojskům	vojsko	k1gNnPc3	vojsko
OUN-B	OUN-B	k1gFnSc2	OUN-B
(	(	kIx(	(
<g/>
OUN-Bandera	OUN-Bandera	k1gFnSc1	OUN-Bandera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
OUN-M	OUN-M	k1gMnSc1	OUN-M
(	(	kIx(	(
<g/>
OUN-Melnyk	OUN-Melnyk	k1gMnSc1	OUN-Melnyk
<g/>
)	)	kIx)	)
a	a	k8xC	a
UPA	UPA	kA	UPA
<g/>
.	.	kIx.	.
</s>
<s>
Dezertéři	dezertér	k1gMnPc1	dezertér
od	od	k7c2	od
policie	policie	k1gFnSc2	policie
pak	pak	k6eAd1	pak
páchali	páchat	k5eAaImAgMnP	páchat
zločiny	zločin	k1gInPc4	zločin
proti	proti	k7c3	proti
vybraným	vybraný	k2eAgMnPc3d1	vybraný
Polákům	Polák	k1gMnPc3	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
nesla	nést	k5eAaImAgFnS	nést
znaky	znak	k1gInPc4	znak
účasti	účast	k1gFnSc3	účast
OUN-B	OUN-B	k1gFnSc2	OUN-B
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ozbrojeného	ozbrojený	k2eAgInSc2d1	ozbrojený
boje	boj	k1gInSc2	boj
a	a	k8xC	a
plánech	plán	k1gInPc6	plán
na	na	k7c6	na
odstranění	odstranění	k1gNnSc6	odstranění
polské	polský	k2eAgFnSc2d1	polská
populace	populace	k1gFnSc2	populace
na	na	k7c6	na
území	území	k1gNnSc6	území
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c4	o
vyhlazení	vyhlazení	k1gNnSc4	vyhlazení
Poláků	Polák	k1gMnPc2	Polák
==	==	k?	==
</s>
</p>
<p>
<s>
Plány	plán	k1gInPc1	plán
OUN	OUN	kA	OUN
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
a	a	k8xC	a
národnostní	národnostní	k2eAgFnSc1d1	národnostní
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
měl	mít	k5eAaImAgInS	mít
záměr	záměr	k1gInSc1	záměr
provést	provést	k5eAaPmF	provést
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Stecko	Stecko	k1gNnSc4	Stecko
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
odstranění	odstranění	k1gNnSc4	odstranění
Poláků	Polák	k1gMnPc2	Polák
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
a	a	k8xC	a
východní	východní	k2eAgFnSc3d1	východní
Haliči	Halič	k1gFnSc3	Halič
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
záměrem	záměr	k1gInSc7	záměr
použít	použít	k5eAaPmF	použít
projevy	projev	k1gInPc4	projev
vyvolané	vyvolaný	k2eAgInPc4d1	vyvolaný
rolníky	rolník	k1gMnPc7	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
polské	polský	k2eAgFnSc3d1	polská
inteligenci	inteligence	k1gFnSc3	inteligence
plánoval	plánovat	k5eAaImAgMnS	plánovat
uplatnit	uplatnit	k5eAaPmF	uplatnit
stejnou	stejný	k2eAgFnSc4d1	stejná
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
prováděna	provádět	k5eAaImNgFnS	provádět
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
akce	akce	k1gFnSc2	akce
Intelligenzaktion	Intelligenzaktion	k1gInSc1	Intelligenzaktion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
tuto	tento	k3xDgFnSc4	tento
politiku	politika	k1gFnSc4	politika
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
a	a	k8xC	a
odstranit	odstranit	k5eAaPmF	odstranit
polskou	polský	k2eAgFnSc4d1	polská
populaci	populace	k1gFnSc4	populace
ze	z	k7c2	z
sporných	sporný	k2eAgNnPc2d1	sporné
území	území	k1gNnPc2	území
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
činí	činit	k5eAaImIp3nS	činit
nárok	nárok	k1gInSc4	nárok
OUN-B	OUN-B	k1gFnSc2	OUN-B
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
případné	případný	k2eAgInPc4d1	případný
politické	politický	k2eAgInPc4d1	politický
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
etnicky	etnicky	k6eAd1	etnicky
čistá	čistý	k2eAgFnSc1d1	čistá
<g/>
.	.	kIx.	.
</s>
<s>
Dopadem	dopad	k1gInSc7	dopad
tohoto	tento	k3xDgNnSc2	tento
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
vyhlazování	vyhlazování	k1gNnSc1	vyhlazování
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
UPA	UPA	kA	UPA
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
situaci	situace	k1gFnSc4	situace
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
ve	v	k7c6	v
dnech	den	k1gInPc6	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Tereberze	Tereberze	k1gFnSc2	Tereberze
nebo	nebo	k8xC	nebo
Walujky	Walujka	k1gFnSc2	Walujka
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
obce	obec	k1gFnSc2	obec
Oleska	Olesko	k1gNnSc2	Olesko
ve	v	k7c6	v
Lvovské	lvovský	k2eAgFnSc6d1	Lvovská
oblasti	oblast	k1gFnSc6	oblast
III	III	kA	III
<g/>
.	.	kIx.	.
konference	konference	k1gFnSc1	konference
OUN-	OUN-	k1gFnSc2	OUN-
<g/>
B.	B.	kA	B.
Podle	podle	k7c2	podle
Czeslawa	Czeslaw	k1gInSc2	Czeslaw
Partacze	Partacze	k1gFnSc1	Partacze
a	a	k8xC	a
Wladyslawa	Wladyslaw	k2eAgFnSc1d1	Wladyslawa
Filara	Filara	k1gFnSc1	Filara
vedení	vedení	k1gNnSc2	vedení
OUN-B	OUN-B	k1gFnSc1	OUN-B
přijalo	přijmout	k5eAaPmAgNnS	přijmout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c4	o
odstranění	odstranění	k1gNnSc4	odstranění
všech	všecek	k3xTgMnPc2	všecek
Poláků	Polák	k1gMnPc2	Polák
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
považovaných	považovaný	k2eAgFnPc2d1	považovaná
jimi	on	k3xPp3gMnPc7	on
za	za	k7c4	za
ukrajinské	ukrajinský	k2eAgFnPc4d1	ukrajinská
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
oddělení	oddělení	k1gNnSc1	oddělení
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
polského	polský	k2eAgInSc2d1	polský
Institutu	institut	k1gInSc2	institut
národní	národní	k2eAgFnSc2d1	národní
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
silné	silný	k2eAgFnSc2d1	silná
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c4	o
zahájení	zahájení	k1gNnSc4	zahájení
bojové	bojový	k2eAgFnSc2d1	bojová
činnosti	činnost	k1gFnSc2	činnost
odkládajíc	odkládat	k5eAaImSgFnS	odkládat
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
nastání	nastání	k1gNnSc4	nastání
"	"	kIx"	"
<g/>
správného	správný	k2eAgInSc2d1	správný
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
"	"	kIx"	"
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c6	o
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
akce	akce	k1gFnSc2	akce
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
porušila	porušit	k5eAaPmAgFnS	porušit
ustanovení	ustanovení	k1gNnSc4	ustanovení
třetí	třetí	k4xOgFnSc2	třetí
konference	konference	k1gFnSc2	konference
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
samostatně	samostatně	k6eAd1	samostatně
lokální	lokální	k2eAgNnSc1d1	lokální
velení	velení	k1gNnSc1	velení
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
podobný	podobný	k2eAgInSc4d1	podobný
postoj	postoj	k1gInSc4	postoj
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
Grzegorz	Grzegorz	k1gMnSc1	Grzegorz
Hryciuk	Hryciuk	k1gMnSc1	Hryciuk
<g/>
,	,	kIx,	,
a	a	k8xC	a
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
nejpravděpodobnějšího	pravděpodobný	k2eAgInSc2d3	Nejpravděpodobnější
hypotézy	hypotéza	k1gFnPc1	hypotéza
o	o	k7c6	o
genocidě	genocida	k1gFnSc6	genocida
Poláků	polák	k1gInPc2	polák
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
tři	tři	k4xCgMnPc1	tři
představitelé	představitel	k1gMnPc1	představitel
volyňské	volyňský	k2eAgFnSc2d1	Volyňská
OUN-B	OUN-B	k1gFnSc2	OUN-B
<g/>
:	:	kIx,	:
Dmytro	Dmytro	k1gNnSc1	Dmytro
Kljačkivskyj	Kljačkivskyj	k1gFnSc1	Kljačkivskyj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vedl	vést	k5eAaImAgInS	vést
volyňskou	volyňský	k2eAgFnSc4d1	Volyňská
OUN-B	OUN-B	k1gFnSc4	OUN-B
<g/>
,	,	kIx,	,
Vasil	Vasil	k1gInSc4	Vasil
Ivachiv	Ivachiva	k1gFnPc2	Ivachiva
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
úředník	úředník	k1gMnSc1	úředník
OUN-B	OUN-B	k1gMnSc1	OUN-B
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
Lytvynčuk	Lytvynčuk	k1gMnSc1	Lytvynčuk
<g/>
,	,	kIx,	,
velící	velící	k2eAgInPc4d1	velící
jednotkám	jednotka	k1gFnPc3	jednotka
UPA	UPA	kA	UPA
na	na	k7c4	na
severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
Volyni	Volyně	k1gFnSc4	Volyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Lytvynčuk	Lytvynčuk	k1gMnSc1	Lytvynčuk
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
S.	S.	kA	S.
Janiszewského	Janiszewského	k2eAgInSc7d1	Janiszewského
iniciátorem	iniciátor	k1gInSc7	iniciátor
a	a	k8xC	a
nejaktivnějším	aktivní	k2eAgInSc7d3	nejaktivnější
organizátorem	organizátor	k1gInSc7	organizátor
vraždění	vraždění	k1gNnSc2	vraždění
Poláků	polák	k1gInPc2	polák
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Vasila	Vasil	k1gMnSc2	Vasil
Ivachiva	Ivachiv	k1gMnSc2	Ivachiv
moc	moc	k6eAd1	moc
zcela	zcela	k6eAd1	zcela
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Kljačkivského	Kljačkivský	k2eAgInSc2d1	Kljačkivský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
sám	sám	k3xTgMnSc1	sám
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
zahájení	zahájení	k1gNnSc6	zahájení
etnických	etnický	k2eAgFnPc2d1	etnická
čistek	čistka	k1gFnPc2	čistka
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Organizace	organizace	k1gFnSc1	organizace
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
průběh	průběh	k1gInSc4	průběh
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
územní	územní	k2eAgFnSc4d1	územní
působnost	působnost	k1gFnSc4	působnost
<g/>
,	,	kIx,	,
cíle	cíl	k1gInPc4	cíl
a	a	k8xC	a
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
této	tento	k3xDgFnSc3	tento
akci	akce	k1gFnSc3	akce
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
došla	dojít	k5eAaPmAgFnS	dojít
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
IPN	IPN	kA	IPN
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
probíhala	probíhat	k5eAaImAgFnS	probíhat
genocida	genocida	k1gFnSc1	genocida
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zločiny	zločin	k1gInPc1	zločin
byly	být	k5eAaImAgInP	být
dílem	dílem	k6eAd1	dílem
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
posílené	posílený	k2eAgFnSc2d1	posílená
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
dubnu	duben	k1gInSc6	duben
1943	[number]	k4	1943
přeběhlíky	přeběhlík	k1gMnPc4	přeběhlík
z	z	k7c2	z
oddílů	oddíl	k1gInPc2	oddíl
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
rolníky	rolník	k1gMnPc7	rolník
názývanými	názývaný	k2eAgFnPc7d1	názývaný
czerń	czerń	k?	czerń
(	(	kIx(	(
<g/>
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
kozáky	kozák	k1gInPc4	kozák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Samoobronni	Samoobronn	k1gMnPc1	Samoobronn
Kuszczowi	Kuszczow	k1gFnSc2	Kuszczow
Widdily	Widdil	k1gMnPc7	Widdil
a	a	k8xC	a
Slużba	Slużba	k1gMnSc1	Slużba
Bezpieczeństwa	Bezpieczeństwa	k1gMnSc1	Bezpieczeństwa
OUN-	OUN-	k1gMnSc1	OUN-
<g/>
B.	B.	kA	B.
</s>
</p>
<p>
<s>
==	==	k?	==
Iniciátoři	iniciátor	k1gMnPc1	iniciátor
zločinů	zločin	k1gInPc2	zločin
==	==	k?	==
</s>
</p>
<p>
<s>
Odpovědným	odpovědný	k2eAgMnSc7d1	odpovědný
za	za	k7c2	za
vydání	vydání	k1gNnSc2	vydání
příkazu	příkaz	k1gInSc2	příkaz
k	k	k7c3	k
etnické	etnický	k2eAgFnSc3d1	etnická
čistce	čistka	k1gFnSc3	čistka
UPA	UPA	kA	UPA
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
Dmytro	Dmytro	k1gNnSc4	Dmytro
Kljačkivskyj	Kljačkivskyj	k1gInSc4	Kljačkivskyj
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
"	"	kIx"	"
<g/>
Klym	Klym	k1gInSc4	Klym
Savur	Savura	k1gFnPc2	Savura
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgMnSc1d1	okresní
velitel	velitel	k1gMnSc1	velitel
UPA-sever	UPAever	k1gMnSc1	UPA-sever
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vasilem	Vasil	k1gMnSc7	Vasil
Ivachovem	Ivachovo	k1gNnSc7	Ivachovo
a	a	k8xC	a
Ivanem	Ivan	k1gMnSc7	Ivan
Lytvynčukem	Lytvynčuk	k1gMnSc7	Lytvynčuk
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
IPN	IPN	kA	IPN
označuje	označovat	k5eAaImIp3nS	označovat
vůdce	vůdce	k1gMnPc4	vůdce
OUN-B	OUN-B	k1gMnSc4	OUN-B
a	a	k8xC	a
UPA	UPA	kA	UPA
za	za	k7c4	za
přímo	přímo	k6eAd1	přímo
zodpovědné	zodpovědný	k2eAgMnPc4d1	zodpovědný
za	za	k7c2	za
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
o	o	k7c4	o
zločinné	zločinný	k2eAgNnSc4d1	zločinné
a	a	k8xC	a
cílené	cílený	k2eAgNnSc4d1	cílené
vraždění	vraždění	k1gNnSc4	vraždění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dmytro	Dmytro	k6eAd1	Dmytro
Kljačkivskyj	Kljačkivskyj	k1gFnSc1	Kljačkivskyj
<g/>
,	,	kIx,	,
ps	ps	k0	ps
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Klym	Klym	k1gMnSc1	Klym
Savur	Savur	k1gMnSc1	Savur
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
UPA-sever	UPAever	k1gMnSc1	UPA-sever
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
března-května	březnavětn	k1gInSc2	března-květn
1943	[number]	k4	1943
vydal	vydat	k5eAaPmAgInS	vydat
individuální	individuální	k2eAgNnSc4d1	individuální
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c6	o
zahájení	zahájení	k1gNnSc6	zahájení
vražd	vražda	k1gFnPc2	vražda
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
tajnou	tajný	k2eAgFnSc4d1	tajná
směrnici	směrnice	k1gFnSc4	směrnice
UPA	UPA	kA	UPA
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
pro	pro	k7c4	pro
průběh	průběh	k1gInSc4	průběh
likvidace	likvidace	k1gFnSc2	likvidace
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
mužské	mužský	k2eAgFnSc2d1	mužská
populace	populace	k1gFnSc2	populace
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
16	[number]	k4	16
do	do	k7c2	do
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
i	i	k9	i
velitel	velitel	k1gMnSc1	velitel
okresu	okres	k1gInSc2	okres
"	"	kIx"	"
<g/>
Turiw	Turiw	k1gMnSc1	Turiw
<g/>
"	"	kIx"	"
Jurij	Jurij	k1gMnSc1	Jurij
Stelmaszczuk	Stelmaszczuk	k1gMnSc1	Stelmaszczuk
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
"	"	kIx"	"
<g/>
Rudyj	Rudyj	k1gInSc1	Rudyj
<g/>
"	"	kIx"	"
v	v	k7c4	v
poválečné	poválečný	k2eAgFnPc4d1	poválečná
výpovědi	výpověď	k1gFnPc4	výpověď
podané	podaný	k2eAgFnPc4d1	podaná
na	na	k7c6	na
NKVD	NKVD	kA	NKVD
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zatčení	zatčení	k1gNnSc6	zatčení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Stelmaszczuk	Stelmaszczuk	k1gMnSc1	Stelmaszczuk
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
apelovat	apelovat	k5eAaImF	apelovat
na	na	k7c4	na
Mykolu	Mykola	k1gFnSc4	Mykola
Lebedě	Lebeď	k1gMnSc2	Lebeď
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
ze	z	k7c2	z
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
z	z	k7c2	z
orgánů	orgán	k1gInPc2	orgán
OUN-	OUN-	k1gFnSc2	OUN-
<g/>
B.	B.	kA	B.
Je	být	k5eAaImIp3nS	být
však	však	k9	však
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stelmaszczuk	Stelmaszczuk	k1gInSc4	Stelmaszczuk
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
rozkaz	rozkaz	k1gInSc1	rozkaz
vyplnil	vyplnit	k5eAaPmAgInS	vyplnit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
směrnice	směrnice	k1gFnSc1	směrnice
Prowodu	Prowod	k1gInSc2	Prowod
OUN	OUN	kA	OUN
mohla	moct	k5eAaImAgFnS	moct
uložit	uložit	k5eAaPmF	uložit
úplnou	úplný	k2eAgFnSc4d1	úplná
likvidaci	likvidace	k1gFnSc4	likvidace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
nebo	nebo	k8xC	nebo
obsahovat	obsahovat	k5eAaImF	obsahovat
ustanovení	ustanovení	k1gNnSc4	ustanovení
<g/>
,	,	kIx,	,
mírnější	mírný	k2eAgFnPc1d2	mírnější
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
před	před	k7c7	před
vraždami	vražda	k1gFnPc7	vražda
vyzývat	vyzývat	k5eAaImF	vyzývat
k	k	k7c3	k
odjezdu	odjezd	k1gInSc3	odjezd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
byl	být	k5eAaImAgInS	být
svévolně	svévolně	k6eAd1	svévolně
změněn	změnit	k5eAaPmNgInS	změnit
"	"	kIx"	"
<g/>
Klymem	Klym	k1gInSc7	Klym
Savurou	Savura	k1gFnSc7	Savura
<g/>
"	"	kIx"	"
na	na	k7c4	na
radikálnější	radikální	k2eAgFnSc4d2	radikálnější
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
i	i	k9	i
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
Kljačkivskyj	Kljačkivskyj	k1gInSc1	Kljačkivskyj
setkal	setkat	k5eAaPmAgInS	setkat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
aktivistů	aktivista	k1gMnPc2	aktivista
OUN	OUN	kA	OUN
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
zvolený	zvolený	k2eAgInSc1d1	zvolený
způsob	způsob	k1gInSc1	způsob
jednání	jednání	k1gNnSc2	jednání
vůči	vůči	k7c3	vůči
Polákům	Polák	k1gMnPc3	Polák
byl	být	k5eAaImAgInS	být
nicméně	nicméně	k8xC	nicméně
schválen	schválit	k5eAaPmNgInS	schválit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1943	[number]	k4	1943
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
kongresu	kongres	k1gInSc6	kongres
OUN	OUN	kA	OUN
jemuž	jenž	k3xRgMnSc3	jenž
předsedal	předsedat	k5eAaImAgMnS	předsedat
Roman	Roman	k1gMnSc1	Roman
Josypovyč	Josypovyč	k1gMnSc1	Josypovyč
Šuchevyč	Šuchevyč	k1gMnSc1	Šuchevyč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Lytvynčuk	Lytvynčuk	k1gMnSc1	Lytvynčuk
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
"	"	kIx"	"
<g/>
Dubowyj	Dubowyj	k1gMnSc1	Dubowyj
<g/>
"	"	kIx"	"
UPA	UPA	kA	UPA
velitel	velitel	k1gMnSc1	velitel
první	první	k4xOgFnSc2	první
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
vojenského	vojenský	k2eAgInSc2d1	vojenský
okruhu	okruh	k1gInSc2	okruh
"	"	kIx"	"
<g/>
Zahrawa	Zahrawa	k1gFnSc1	Zahrawa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
organizátorem	organizátor	k1gMnSc7	organizátor
a	a	k8xC	a
iniciátorem	iniciátor	k1gMnSc7	iniciátor
ohledně	ohledně	k7c2	ohledně
akcí	akce	k1gFnPc2	akce
vůči	vůči	k7c3	vůči
polskému	polský	k2eAgNnSc3d1	polské
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
chlubil	chlubit	k5eAaImAgInS	chlubit
svými	svůj	k3xOyFgInPc7	svůj
úspěchy	úspěch	k1gInPc7	úspěch
při	při	k7c6	při
likvidaci	likvidace	k1gFnSc6	likvidace
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Petro	Petra	k1gFnSc5	Petra
Olijnyk	Olijnyk	k1gInSc1	Olijnyk
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
Enej	Enej	k1gInSc1	Enej
<g/>
"	"	kIx"	"
velitel	velitel	k1gMnSc1	velitel
vojenského	vojenský	k2eAgInSc2d1	vojenský
okruhu	okruh	k1gInSc2	okruh
"	"	kIx"	"
<g/>
Bohun	Bohuna	k1gFnPc2	Bohuna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
Stelmaščuk	Stelmaščuk	k1gInSc1	Stelmaščuk
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
Rudyj	Rudyj	k1gInSc1	Rudyj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
UPA	UPA	kA	UPA
"	"	kIx"	"
<g/>
Ozero	Ozero	k1gNnSc1	Ozero
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
UPA	UPA	kA	UPA
vojenský	vojenský	k2eAgInSc4d1	vojenský
okruh	okruh	k1gInSc4	okruh
"	"	kIx"	"
<g/>
Turiw	Turiw	k1gFnSc4	Turiw
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vasil	Vasil	k1gMnSc1	Vasil
Ivachiv	Ivachiva	k1gFnPc2	Ivachiva
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
Soma	soma	k1gNnSc1	soma
<g/>
"	"	kIx"	"
vojenský	vojenský	k2eAgMnSc1d1	vojenský
referent	referent	k1gMnSc1	referent
OUN-B	OUN-B	k1gMnSc1	OUN-B
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgMnS	přijmout
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
s	s	k7c7	s
Kljačkivským	Kljačkivský	k2eAgMnSc7d1	Kljačkivský
a	a	k8xC	a
Lytvynčukem	Lytvynčuk	k1gMnSc7	Lytvynčuk
o	o	k7c6	o
zahájení	zahájení	k1gNnSc6	zahájení
protipolských	protipolský	k2eAgFnPc2d1	protipolská
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
zločinů	zločin	k1gInPc2	zločin
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Typy	typ	k1gInPc1	typ
útoků	útok	k1gInPc2	útok
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgInP	popsat
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
útoků	útok	k1gInPc2	útok
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
útoky	útok	k1gInPc1	útok
na	na	k7c4	na
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
a	a	k8xC	a
malé	malý	k2eAgFnPc4d1	malá
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
cestují	cestovat	k5eAaImIp3nP	cestovat
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
nebo	nebo	k8xC	nebo
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
útoky	útok	k1gInPc1	útok
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
skupiny	skupina	k1gFnPc4	skupina
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgFnPc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
ukrajinských	ukrajinský	k2eAgFnPc6d1	ukrajinská
vesnicích	vesnice	k1gFnPc6	vesnice
nebo	nebo	k8xC	nebo
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
velká	velký	k2eAgNnPc4d1	velké
centra	centrum	k1gNnPc4	centrum
polského	polský	k2eAgNnSc2d1	polské
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
větší	veliký	k2eAgNnSc4d2	veliký
soustředění	soustředění	k1gNnSc4	soustředění
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc1	první
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc1	typ
útoků	útok	k1gInPc2	útok
byly	být	k5eAaImAgInP	být
dílem	dílo	k1gNnSc7	dílo
organizace	organizace	k1gFnSc2	organizace
Slużby	Slużba	k1gFnSc2	Slużba
Bezpeky	Bezpek	k1gInPc1	Bezpek
OUN-B	OUN-B	k1gFnPc2	OUN-B
nebo	nebo	k8xC	nebo
poboček	pobočka	k1gFnPc2	pobočka
UPA	UPA	kA	UPA
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
třetí	třetí	k4xOgInSc4	třetí
typ	typ	k1gInSc4	typ
útoků	útok	k1gInPc2	útok
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mobilizovat	mobilizovat	k5eAaBmF	mobilizovat
ukrajinské	ukrajinský	k2eAgMnPc4d1	ukrajinský
civilisty	civilista	k1gMnPc4	civilista
vyzbrojené	vyzbrojený	k2eAgInPc1d1	vyzbrojený
chladnými	chladný	k2eAgFnPc7d1	chladná
zbraněmi	zbraň	k1gFnPc7	zbraň
nebo	nebo	k8xC	nebo
zemědělským	zemědělský	k2eAgNnSc7d1	zemědělské
nářadím	nářadí	k1gNnSc7	nářadí
a	a	k8xC	a
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc1	počátek
masakru	masakr	k1gInSc2	masakr
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
narůstaly	narůstat	k5eAaImAgFnP	narůstat
vraždy	vražda	k1gFnPc1	vražda
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
polských	polský	k2eAgFnPc2d1	polská
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Oběti	oběť	k1gFnPc1	oběť
byly	být	k5eAaImAgFnP	být
hlavně	hlavně	k9	hlavně
Poláci	Polák	k1gMnPc1	Polák
zaměstnáni	zaměstnán	k2eAgMnPc1d1	zaměstnán
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
lesní	lesní	k2eAgFnSc6d1	lesní
správě	správa	k1gFnSc6	správa
(	(	kIx(	(
<g/>
nadlesní	nadlesní	k1gFnSc6	nadlesní
[	[	kIx(	[
<g/>
36	[number]	k4	36
<g/>
]	]	kIx)	]
,	,	kIx,	,
správce	správce	k1gMnSc1	správce
statku	statek	k1gInSc2	statek
<g/>
[	[	kIx(	[
<g/>
37	[number]	k4	37
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
agronomové	agronom	k1gMnPc1	agronom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
venkovské	venkovský	k2eAgNnSc1d1	venkovské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
východních	východní	k2eAgInPc6d1	východní
okresech	okres	k1gInPc6	okres
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc4	první
masovou	masový	k2eAgFnSc4d1	masová
vraždu	vražda	k1gFnSc4	vražda
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
polský	polský	k2eAgInSc1d1	polský
Institut	institut	k1gInSc1	institut
národní	národní	k2eAgFnSc2d1	národní
paměti	paměť	k1gFnSc2	paměť
uznává	uznávat	k5eAaImIp3nS	uznávat
krveprolití	krveprolití	k1gNnSc1	krveprolití
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
kolonii	kolonie	k1gFnSc6	kolonie
Parośla	Parośl	k1gMnSc2	Parośl
Pierwsza	Pierwsz	k1gMnSc2	Pierwsz
(	(	kIx(	(
<g/>
gm	gm	k?	gm
<g/>
.	.	kIx.	.
</s>
<s>
Antonówka	Antonówka	k1gFnSc1	Antonówka
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Sarny	Sarna	k1gFnSc2	Sarna
<g/>
)	)	kIx)	)
kdy	kdy	k6eAd1	kdy
oddíly	oddíl	k1gInPc1	oddíl
UPA	UPA	kA	UPA
Hryhorije	Hryhorije	k1gMnSc2	Hryhorije
Perehijniaka	Perehijniak	k1gMnSc2	Perehijniak
"	"	kIx"	"
<g/>
Dowbeszki-Korobki	Dowbeszki-Korobki	k1gNnSc7	Dowbeszki-Korobki
<g/>
"	"	kIx"	"
zavraždily	zavraždit	k5eAaPmAgInP	zavraždit
173	[number]	k4	173
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
na	na	k7c4	na
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
jednotky	jednotka	k1gFnSc2	jednotka
UPA	UPA	kA	UPA
podřízené	podřízená	k1gFnSc2	podřízená
Ivanu	Ivan	k1gMnSc3	Ivan
Lytvynčukovi	Lytvynčuk	k1gMnSc3	Lytvynčuk
-	-	kIx~	-
"	"	kIx"	"
<g/>
Dubowemu	Dubowem	k1gInSc2	Dubowem
<g/>
"	"	kIx"	"
zabily	zabít	k5eAaPmAgInP	zabít
nejméně	málo	k6eAd3	málo
179	[number]	k4	179
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Lipnikách	Lipnika	k1gFnPc6	Lipnika
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1943	[number]	k4	1943
jednotka	jednotka	k1gFnSc1	jednotka
UPA	UPA	kA	UPA
pod	pod	k7c7	pod
osobním	osobní	k2eAgNnSc7d1	osobní
velením	velení	k1gNnSc7	velení
"	"	kIx"	"
<g/>
Dubowego	Dubowego	k6eAd1	Dubowego
<g/>
"	"	kIx"	"
zabila	zabít	k5eAaPmAgFnS	zabít
asi	asi	k9	asi
600	[number]	k4	600
lidí	člověk	k1gMnPc2	člověk
v	v	k7c4	v
Janowej	Janowej	k1gInSc4	Janowej
Dolinie	Dolinie	k1gFnSc2	Dolinie
(	(	kIx(	(
<g/>
gm	gm	k?	gm
<g/>
.	.	kIx.	.
</s>
<s>
Berezne	Bereznout	k5eAaPmIp3nS	Bereznout
<g/>
,	,	kIx,	,
kostopilský	kostopilský	k2eAgInSc1d1	kostopilský
okres	okres	k1gInSc1	okres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
intenzita	intenzita	k1gFnSc1	intenzita
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
UPA	UPA	kA	UPA
podřízené	podřízená	k1gFnSc2	podřízená
I.	I.	kA	I.
Lytvynčukovi	Lytvynčuk	k1gMnSc3	Lytvynčuk
a	a	k8xC	a
Petrovi	Petr	k1gMnSc3	Petr
Olijnykovi	Olijnyek	k1gMnSc3	Olijnyek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
Sarny	Sarna	k1gFnSc2	Sarna
<g/>
,	,	kIx,	,
Kostopil	Kostopil	k1gMnSc1	Kostopil
a	a	k8xC	a
Kremenec	Kremenec	k1gMnSc1	Kremenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Násilnosti	násilnost	k1gFnPc1	násilnost
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
během	během	k7c2	během
května	květen	k1gInSc2	květen
<g/>
:	:	kIx,	:
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Sarny	Sarna	k1gFnSc2	Sarna
byly	být	k5eAaImAgFnP	být
vypáleny	vypálen	k2eAgFnPc1d1	vypálena
vesnice	vesnice	k1gFnPc1	vesnice
<g/>
:	:	kIx,	:
Ugly	Ugly	k1gInPc1	Ugly
<g/>
,	,	kIx,	,
Konstantynówka	Konstantynówko	k1gNnPc1	Konstantynówko
<g/>
,	,	kIx,	,
Osty	Osty	k1gInPc1	Osty
<g/>
,	,	kIx,	,
Ubereż	Ubereż	k1gFnPc1	Ubereż
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Niemodlin	Niemodlina	k1gFnPc2	Niemodlina
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
u	u	k7c2	u
Kostopilu	Kostopil	k1gInSc2	Kostopil
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
170	[number]	k4	170
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
byly	být	k5eAaImAgInP	být
spáleny	spálen	k2eAgInPc1d1	spálen
všechna	všechen	k3xTgNnPc4	všechen
sídla	sídlo	k1gNnPc4	sídlo
a	a	k8xC	a
statky	statek	k1gInPc4	statek
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Vladimir	Vladimir	k1gMnSc1	Vladimir
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byla	být	k5eAaImAgFnS	být
oddíly	oddíl	k1gInPc4	oddíl
UPA	UPA	kA	UPA
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
600	[number]	k4	600
osob	osoba	k1gFnPc2	osoba
vypálena	vypálen	k2eAgFnSc1d1	vypálena
obec	obec	k1gFnSc1	obec
Staryki	Staryk	k1gFnSc2	Staryk
a	a	k8xC	a
vyvražděno	vyvraždit	k5eAaPmNgNnS	vyvraždit
všechno	všechen	k3xTgNnSc1	všechen
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
horochovském	horochovský	k2eAgInSc6d1	horochovský
23	[number]	k4	23
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
polské	polský	k2eAgFnPc4d1	polská
obce	obec	k1gFnPc4	obec
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
dubenském	dubenský	k2eAgInSc6d1	dubenský
-	-	kIx~	-
15	[number]	k4	15
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Vladimir	Vladimir	k1gMnSc1	Vladimir
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlna	vlna	k1gFnSc1	vlna
útoků	útok	k1gInPc2	útok
se	se	k3xPyFc4	se
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Volyně	Volyně	k1gFnSc2	Volyně
systematicky	systematicky	k6eAd1	systematicky
posouvala	posouvat	k5eAaImAgFnS	posouvat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlazování	vyhlazování	k1gNnSc1	vyhlazování
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
1943	[number]	k4	1943
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
dubenského	dubenský	k2eAgInSc2d1	dubenský
okresu	okres	k1gInSc2	okres
<g/>
,	,	kIx,	,
luckého	lucký	k2eAgInSc2d1	lucký
okresu	okres	k1gInSc2	okres
i	i	k8xC	i
zdolbunivského	zdolbunivský	k2eAgInSc2d1	zdolbunivský
okresu	okres	k1gInSc2	okres
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1943	[number]	k4	1943
již	již	k9	již
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
všechny	všechen	k3xTgFnPc4	všechen
oblasti	oblast	k1gFnPc4	oblast
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
vražd	vražda	k1gFnPc2	vražda
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Vraždy	vražda	k1gFnPc1	vražda
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
často	často	k6eAd1	často
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
využili	využít	k5eAaPmAgMnP	využít
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Poláci	Polák	k1gMnPc1	Polák
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
na	na	k7c4	na
mši	mše	k1gFnSc4	mše
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
kostely	kostel	k1gInPc1	kostel
obklíčeny	obklíčen	k2eAgInPc1d1	obklíčen
a	a	k8xC	a
věřící	věřící	k2eAgInPc1d1	věřící
často	často	k6eAd1	často
mučeni	mučit	k5eAaImNgMnP	mučit
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrtit	k5eAaImIp3nP	smrtit
krutým	krutý	k2eAgInSc7d1	krutý
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
např.	např.	kA	např.
řezání	řezání	k1gNnSc1	řezání
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
pilou	pila	k1gFnSc7	pila
na	na	k7c4	na
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
vytržení	vytržení	k1gNnSc4	vytržení
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
upálení	upálení	k1gNnSc1	upálení
zaživa	zaživa	k6eAd1	zaživa
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
(	(	kIx(	(
<g/>
s	s	k7c7	s
nesprávnými	správný	k2eNgFnPc7d1	nesprávná
informacemi	informace	k1gFnPc7	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
anti-polská	antiolský	k2eAgFnSc1d1	anti-polský
kampaň	kampaň	k1gFnSc1	kampaň
je	být	k5eAaImIp3nS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
polská	polský	k2eAgFnSc1d1	polská
Zemská	zemský	k2eAgFnSc1d1	zemská
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
Armia	Armia	k1gFnSc1	Armia
Krajowa	Krajowa	k1gFnSc1	Krajowa
<g/>
)	)	kIx)	)
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
protiukrajinské	protiukrajinský	k2eAgFnPc4d1	protiukrajinský
akce	akce	k1gFnPc4	akce
včetně	včetně	k7c2	včetně
likvidace	likvidace	k1gFnSc2	likvidace
aktivistů	aktivista	k1gMnPc2	aktivista
OUN-	OUN-	k1gFnSc2	OUN-
<g/>
B.	B.	kA	B.
Cílem	cíl	k1gInSc7	cíl
tohoto	tento	k3xDgNnSc2	tento
úsilí	úsilí	k1gNnSc2	úsilí
bylo	být	k5eAaImAgNnS	být
zmařit	zmařit	k5eAaPmF	zmařit
plány	plán	k1gInPc4	plán
OUN-	OUN-	k1gFnSc2	OUN-
<g/>
B.	B.	kA	B.
Překvapením	překvapení	k1gNnSc7	překvapení
pro	pro	k7c4	pro
AK	AK	kA	AK
bylo	být	k5eAaImAgNnS	být
dřívější	dřívější	k2eAgNnSc1d1	dřívější
zahájení	zahájení	k1gNnSc1	zahájení
akce	akce	k1gFnSc2	akce
Ukrajinci	Ukrajinec	k1gMnSc3	Ukrajinec
<g/>
.	.	kIx.	.
<g/>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
polské	polský	k2eAgFnPc1d1	polská
podzemní	podzemní	k2eAgFnPc1d1	podzemní
síly	síla	k1gFnPc1	síla
pokusily	pokusit	k5eAaPmAgFnP	pokusit
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
s	s	k7c7	s
UPA	UPA	kA	UPA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
vraždění	vraždění	k1gNnSc1	vraždění
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInPc4d1	počáteční
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7	s
místním	místní	k2eAgMnSc7d1	místní
velitelem	velitel	k1gMnSc7	velitel
SB	sb	kA	sb
UPA	UPA	kA	UPA
Szabaturou	Szabatura	k1gFnSc7	Szabatura
byly	být	k5eAaImAgInP	být
vedeny	vést	k5eAaImNgInP	vést
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
obce	obec	k1gFnSc2	obec
Świnarzyna	Świnarzyn	k1gInSc2	Świnarzyn
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
schůzku	schůzka	k1gFnSc4	schůzka
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1943	[number]	k4	1943
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
regionální	regionální	k2eAgFnSc2d1	regionální
delegace	delegace	k1gFnSc2	delegace
jako	jako	k8xC	jako
zplnomocněnec	zplnomocněnec	k1gMnSc1	zplnomocněnec
advokát	advokát	k1gMnSc1	advokát
Zygmunt	Zygmunt	k1gMnSc1	Zygmunt
Ruml	Ruml	k1gMnSc1	Ruml
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
"	"	kIx"	"
<g/>
Krzysztof	Krzysztof	k1gMnSc1	Krzysztof
Poreba	Poreba	k1gMnSc1	Poreba
<g/>
"	"	kIx"	"
a	a	k8xC	a
zástupce	zástupce	k1gMnSc1	zástupce
okresu	okres	k1gInSc2	okres
armády	armáda	k1gFnSc2	armáda
Armii	Armie	k1gFnSc6	Armie
Krajowej	Krajowej	k1gInSc1	Krajowej
Volyň	Volyně	k1gFnPc2	Volyně
Krzysztof	Krzysztof	k1gMnSc1	Krzysztof
Markiewicz	Markiewicz	k1gMnSc1	Markiewicz
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
"	"	kIx"	"
<g/>
Ďábel	ďábel	k1gMnSc1	ďábel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
řidič	řidič	k1gMnSc1	řidič
Witold	Witold	k1gMnSc1	Witold
Dobrowolski	Dobrowolski	k1gNnPc2	Dobrowolski
<g/>
.	.	kIx.	.
</s>
<s>
Markiewicz	Markiewicz	k1gMnSc1	Markiewicz
znal	znát	k5eAaImAgMnS	znát
Szabatureho	Szabature	k1gMnSc4	Szabature
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
jako	jako	k9	jako
gesto	gesto	k1gNnSc4	gesto
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
Poláci	Polák	k1gMnPc1	Polák
na	na	k7c4	na
schůzku	schůzka	k1gFnSc4	schůzka
nepřišli	přijít	k5eNaPmAgMnP	přijít
s	s	k7c7	s
ozbrojeným	ozbrojený	k2eAgInSc7d1	ozbrojený
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
jednání	jednání	k1gNnSc2	jednání
(	(	kIx(	(
<g/>
obec	obec	k1gFnSc1	obec
Kustycze	Kustycze	k1gFnSc2	Kustycze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
zatčeni	zatčen	k2eAgMnPc1d1	zatčen
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
a	a	k8xC	a
zavražděni	zavražděn	k2eAgMnPc1d1	zavražděn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
neděle	neděle	k1gFnSc1	neděle
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c2	za
úsvitu	úsvit	k1gInSc2	úsvit
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
ráno	ráno	k6eAd1	ráno
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
vojáci	voják	k1gMnPc1	voják
UPA	UPA	kA	UPA
koordinovaně	koordinovaně	k6eAd1	koordinovaně
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
civilisty	civilista	k1gMnPc4	civilista
v	v	k7c6	v
99	[number]	k4	99
polských	polský	k2eAgInPc6d1	polský
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
regionech	region	k1gInPc6	region
horochivském	horochivský	k2eAgNnSc6d1	horochivský
a	a	k8xC	a
wlodzimierskim	wlodzimierskim	k6eAd1	wlodzimierskim
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
Smrt	smrt	k1gFnSc1	smrt
Lechům	Lech	k1gMnPc3	Lech
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zabránit	zabránit	k5eAaPmF	zabránit
obyvatelům	obyvatel	k1gMnPc3	obyvatel
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
řeži	řež	k1gFnSc3	řež
a	a	k8xC	a
ničení	ničení	k1gNnSc3	ničení
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
byli	být	k5eAaImAgMnP	být
zabíjeni	zabíjet	k5eAaImNgMnP	zabíjet
střelbou	střelba	k1gFnSc7	střelba
<g/>
,	,	kIx,	,
kosami	kosa	k1gFnPc7	kosa
<g/>
,	,	kIx,	,
vidlemi	vidle	k1gFnPc7	vidle
<g/>
,	,	kIx,	,
sekerami	sekera	k1gFnPc7	sekera
<g/>
,	,	kIx,	,
pilami	pila	k1gFnPc7	pila
<g/>
,	,	kIx,	,
noži	nůž	k1gInPc7	nůž
<g/>
,	,	kIx,	,
kladivy	kladivo	k1gNnPc7	kladivo
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyvraždění	vyvraždění	k1gNnSc6	vyvraždění
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byly	být	k5eAaImAgFnP	být
vsi	ves	k1gFnPc1	ves
vypáleny	vypálen	k2eAgFnPc1d1	vypálena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
znovuosídlení	znovuosídlení	k1gNnSc1	znovuosídlení
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
byla	být	k5eAaImAgFnS	být
dobře	dobře	k6eAd1	dobře
připravena	připravit	k5eAaPmNgFnS	připravit
a	a	k8xC	a
naplánována	naplánovat	k5eAaBmNgFnS	naplánovat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
wlodzimierskim	wlodzimierskim	k6eAd1	wlodzimierskim
předcházela	předcházet	k5eAaImAgFnS	předcházet
koncentrace	koncentrace	k1gFnSc1	koncentrace
jednotek	jednotka	k1gFnPc2	jednotka
UPA	UPA	kA	UPA
v	v	k7c6	v
lese	les	k1gInSc6	les
západně	západně	k6eAd1	západně
od	od	k7c2	od
Poryck	Porycka	k1gFnPc2	Porycka
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Marysin	Marysina	k1gFnPc2	Marysina
Dolinka	dolinka	k1gFnSc1	dolinka
<g/>
,	,	kIx,	,
Lachy	Lach	k1gMnPc4	Lach
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Żdżary	Żdżara	k1gFnSc2	Żdżara
<g/>
,	,	kIx,	,
Litowież	Litowież	k1gFnSc5	Litowież
<g/>
,	,	kIx,	,
Grzybowice	Grzybowice	k1gFnSc5	Grzybowice
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
akcí	akce	k1gFnPc2	akce
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
setkání	setkání	k1gNnSc1	setkání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
místní	místní	k2eAgMnPc1d1	místní
lidé	člověk	k1gMnPc1	člověk
upozorněni	upozornit	k5eAaPmNgMnP	upozornit
na	na	k7c4	na
nutnost	nutnost	k1gFnSc4	nutnost
vyvraždit	vyvraždit	k5eAaPmF	vyvraždit
všechny	všechen	k3xTgInPc4	všechen
Poláky	polák	k1gInPc4	polák
<g/>
.	.	kIx.	.
</s>
<s>
Vraždění	vraždění	k1gNnSc1	vraždění
začalo	začít	k5eAaPmAgNnS	začít
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c4	v
3	[number]	k4	3
hodiny	hodina	k1gFnSc2	hodina
ráno	ráno	k6eAd1	ráno
11	[number]	k4	11
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1943	[number]	k4	1943
od	od	k7c2	od
polské	polský	k2eAgFnSc2d1	polská
obce	obec	k1gFnSc2	obec
Gurów	Gurów	k1gFnSc2	Gurów
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
Gurów	Gurów	k1gMnPc4	Gurów
Wielki	Wielk	k1gFnSc2	Wielk
<g/>
,	,	kIx,	,
Gurów	Gurów	k1gFnSc2	Gurów
Maly	Maly	k?	Maly
<g/>
,	,	kIx,	,
Wygrankę	Wygrankę	k1gFnPc1	Wygrankę
<g/>
,	,	kIx,	,
Zdżary	Zdżar	k1gInPc1	Zdżar
<g/>
,	,	kIx,	,
Zabloćce	Zabloćce	k1gFnPc1	Zabloćce
–	–	k?	–
Sądową	Sądową	k1gFnSc2	Sądową
<g/>
,	,	kIx,	,
Nowiny	Nowina	k1gFnSc2	Nowina
<g/>
,	,	kIx,	,
Zagaję	Zagaję	k1gMnSc1	Zagaję
<g/>
,	,	kIx,	,
Poryck	Poryck	k1gMnSc1	Poryck
<g/>
,	,	kIx,	,
Oleń	Oleń	k1gMnSc1	Oleń
<g/>
,	,	kIx,	,
Orzeszyn	Orzeszyn	k1gMnSc1	Orzeszyn
<g/>
,	,	kIx,	,
Romanówkę	Romanówkę	k1gMnSc1	Romanówkę
<g/>
,	,	kIx,	,
Lachów	Lachów	k1gMnSc1	Lachów
<g/>
,	,	kIx,	,
Swojczów	Swojczów	k1gMnSc1	Swojczów
<g/>
,	,	kIx,	,
Gucin	Gucin	k1gMnSc1	Gucin
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
Gurów	Gurów	k1gFnSc2	Gurów
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
480	[number]	k4	480
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
přežilo	přežít	k5eAaPmAgNnS	přežít
jen	jen	k9	jen
70	[number]	k4	70
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
Orzeszyn	Orzeszyna	k1gFnPc2	Orzeszyna
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
340	[number]	k4	340
obyvatel	obyvatel	k1gMnPc2	obyvatel
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
270	[number]	k4	270
Poláků	Polák	k1gMnPc2	Polák
<g/>
;	;	kIx,	;
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Sądowa	Sądowum	k1gNnSc2	Sądowum
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
ze	z	k7c2	z
600	[number]	k4	600
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
350	[number]	k4	350
Poláků	Polák	k1gMnPc2	Polák
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Zagaje	Zagaj	k1gInSc2	Zagaj
přežil	přežít	k5eAaPmAgInS	přežít
asi	asi	k9	asi
jen	jen	k9	jen
tucet	tucet	k1gInSc4	tucet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
dne	den	k1gInSc2	den
ráno	ráno	k6eAd1	ráno
skupina	skupina	k1gFnSc1	skupina
20	[number]	k4	20
útočníků	útočník	k1gMnPc2	útočník
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
během	během	k7c2	během
mše	mše	k1gFnSc2	mše
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Poryck	Porycka	k1gFnPc2	Porycka
a	a	k8xC	a
během	během	k7c2	během
třiceti	třicet	k4xCc2	třicet
minut	minuta	k1gFnPc2	minuta
zabila	zabít	k5eAaPmAgFnS	zabít
asi	asi	k9	asi
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byly	být	k5eAaImAgFnP	být
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Bandité	bandita	k1gMnPc1	bandita
pak	pak	k6eAd1	pak
zabili	zabít	k5eAaPmAgMnP	zabít
všechny	všechen	k3xTgMnPc4	všechen
(	(	kIx(	(
<g/>
asi	asi	k9	asi
200	[number]	k4	200
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
Poláky	polák	k1gInPc1	polák
žijící	žijící	k2eAgInPc1d1	žijící
v	v	k7c6	v
Porycku	Porycko	k1gNnSc6	Porycko
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1	podobný
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
kostely	kostel	k1gInPc4	kostel
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vraždění	vraždění	k1gNnSc1	vraždění
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
krutostí	krutost	k1gFnSc7	krutost
<g/>
.	.	kIx.	.
</s>
<s>
Polské	polský	k2eAgFnSc2d1	polská
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
osady	osada	k1gFnSc2	osada
byly	být	k5eAaImAgFnP	být
vypleněny	vypleněn	k2eAgFnPc1d1	vypleněna
a	a	k8xC	a
vypáleny	vypálen	k2eAgFnPc1d1	vypálena
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
masakry	masakr	k1gInPc1	masakr
vesnic	vesnice	k1gFnPc2	vesnice
udály	udát	k5eAaPmAgInP	udát
<g/>
,	,	kIx,	,
na	na	k7c6	na
vozech	vůz	k1gInPc6	vůz
vjeli	vjet	k5eAaPmAgMnP	vjet
do	do	k7c2	do
vesnic	vesnice	k1gFnPc2	vesnice
rolníci	rolník	k1gMnPc1	rolník
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
kradli	krást	k5eAaImAgMnP	krást
majetek	majetek	k1gInSc4	majetek
zbývající	zbývající	k2eAgInSc4d1	zbývající
po	po	k7c6	po
zavražděných	zavražděný	k2eAgMnPc6d1	zavražděný
Polácích	Polák	k1gMnPc6	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
akce	akce	k1gFnSc1	akce
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1943	[number]	k4	1943
se	se	k3xPyFc4	se
cílem	cíl	k1gInSc7	cíl
útoků	útok	k1gInPc2	útok
stalo	stát	k5eAaPmAgNnS	stát
nejméně	málo	k6eAd3	málo
530	[number]	k4	530
polských	polský	k2eAgFnPc2d1	polská
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
vesniček	vesnička	k1gFnPc2	vesnička
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
17	[number]	k4	17
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
vyvrcholení	vyvrcholení	k1gNnSc4	vyvrcholení
etnických	etnický	k2eAgFnPc2d1	etnická
čistek	čistka	k1gFnPc2	čistka
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Srpen	srpen	k1gInSc4	srpen
1943	[number]	k4	1943
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
anti-polskou	antiolský	k2eAgFnSc4d1	anti-polský
kampaň	kampaň	k1gFnSc4	kampaň
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
genocida	genocida	k1gFnSc1	genocida
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
měsíce	měsíc	k1gInSc2	měsíc
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
tichý	tichý	k2eAgInSc1d1	tichý
<g/>
,	,	kIx,	,
klid	klid	k1gInSc1	klid
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
dal	dát	k5eAaPmAgInS	dát
Polákům	Polák	k1gMnPc3	Polák
čas	čas	k1gInSc4	čas
na	na	k7c4	na
dokončení	dokončení	k1gNnSc4	dokončení
sklizně	sklizeň	k1gFnSc2	sklizeň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
ukrást	ukrást	k5eAaPmF	ukrást
již	již	k6eAd1	již
sklizené	sklizený	k2eAgFnPc4d1	sklizená
plodiny	plodina	k1gFnPc4	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
k	k	k7c3	k
vraždění	vraždění	k1gNnSc3	vraždění
docházelo	docházet	k5eAaImAgNnS	docházet
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
v	v	k7c6	v
Leonówce	Leonówka	k1gFnSc6	Leonówka
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
asi	asi	k9	asi
150	[number]	k4	150
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
masivní	masivní	k2eAgFnSc3d1	masivní
akci	akce	k1gFnSc3	akce
připomínající	připomínající	k2eAgInPc4d1	připomínající
útoky	útok	k1gInPc4	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
28	[number]	k4	28
<g/>
.	.	kIx.	.
až	až	k9	až
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
napadeno	napadnout	k5eAaPmNgNnS	napadnout
85	[number]	k4	85
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
kowelskim	kowelskim	k6eAd1	kowelskim
a	a	k8xC	a
wlodzimierskim	wlodzimierskim	k6eAd1	wlodzimierskim
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
masakry	masakr	k1gInPc1	masakr
nedotčeném	dotčený	k2eNgInSc6d1	nedotčený
okrese	okres	k1gInSc6	okres
lubomelskim	lubomelskim	k1gInSc1	lubomelskim
<g/>
.	.	kIx.	.
</s>
<s>
UPA	UPA	kA	UPA
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vraždila	vraždit	k5eAaImAgFnS	vraždit
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
Ostrówki	Ostrówk	k1gFnSc2	Ostrówk
a	a	k8xC	a
Wola	Wol	k2eAgFnSc1d1	Wola
Ostrowiecka	Ostrowiecka	k1gFnSc1	Ostrowiecka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
lubomelskim	lubomelskim	k1gInSc1	lubomelskim
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
Poláci	Polák	k1gMnPc1	Polák
byli	být	k5eAaImAgMnP	být
zabiti	zabít	k5eAaPmNgMnP	zabít
<g/>
,	,	kIx,	,
vypáleny	vypálit	k5eAaPmNgFnP	vypálit
všechny	všechen	k3xTgFnPc1	všechen
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
ukraden	ukraden	k2eAgInSc1d1	ukraden
majetek	majetek	k1gInSc1	majetek
a	a	k8xC	a
hospodářská	hospodářský	k2eAgNnPc1d1	hospodářské
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Wola	Wolus	k1gMnSc2	Wolus
Ostrowiecka	Ostrowiecko	k1gNnSc2	Ostrowiecko
zabito	zabít	k5eAaPmNgNnS	zabít
529	[number]	k4	529
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
220	[number]	k4	220
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
věku	věk	k1gInSc2	věk
do	do	k7c2	do
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Ostrówki	Ostrówk	k1gFnSc2	Ostrówk
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
438	[number]	k4	438
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
246	[number]	k4	246
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
věku	věk	k1gInSc2	věk
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1943	[number]	k4	1943
provedeno	provést	k5eAaPmNgNnS	provést
301	[number]	k4	301
nájezdů	nájezd	k1gInPc2	nájezd
na	na	k7c4	na
polské	polský	k2eAgFnPc4d1	polská
vesnice	vesnice	k1gFnPc4	vesnice
a	a	k8xC	a
zavražděno	zavražděn	k2eAgNnSc1d1	zavražděno
nejméně	málo	k6eAd3	málo
8280	[number]	k4	8280
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
UPA	UPA	kA	UPA
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
mezi	mezi	k7c4	mezi
ukrajinské	ukrajinský	k2eAgMnPc4d1	ukrajinský
rolníky	rolník	k1gMnPc4	rolník
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
majetku	majetek	k1gInSc2	majetek
po	po	k7c6	po
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
vypálených	vypálený	k2eAgFnPc2d1	vypálená
polských	polský	k2eAgFnPc2d1	polská
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
půdu	půda	k1gFnSc4	půda
znárodněnou	znárodněný	k2eAgFnSc7d1	znárodněná
sovětskými	sovětský	k2eAgInPc7d1	sovětský
orgány	orgán	k1gInPc7	orgán
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přelom	přelomit	k5eAaPmRp2nS	přelomit
let	let	k1gInSc4	let
1943	[number]	k4	1943
<g/>
/	/	kIx~	/
<g/>
44	[number]	k4	44
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
relativního	relativní	k2eAgInSc2d1	relativní
klidu	klid	k1gInSc2	klid
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc6	listopad
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vánočních	vánoční	k2eAgInPc2d1	vánoční
svátků	svátek	k1gInPc2	svátek
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
kolem	kolem	k7c2	kolem
Volyně	Volyně	k1gFnSc2	Volyně
začala	začít	k5eAaPmAgFnS	začít
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
protipolských	protipolský	k2eAgFnPc2d1	protipolská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
akcí	akce	k1gFnPc2	akce
vedených	vedený	k2eAgFnPc2d1	vedená
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
nacionalisty	nacionalista	k1gMnPc7	nacionalista
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
UPA	UPA	kA	UPA
podporované	podporovaný	k2eAgFnPc1d1	podporovaná
místními	místní	k2eAgMnPc7d1	místní
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
na	na	k7c4	na
centra	centrum	k1gNnPc4	centrum
polských	polský	k2eAgMnPc2d1	polský
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
bráněné	bráněný	k2eAgFnSc2d1	bráněná
osady	osada	k1gFnSc2	osada
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
rówieńském	rówieńský	k2eAgNnSc6d1	rówieńský
<g/>
,	,	kIx,	,
luckém	lucký	k2eAgNnSc6d1	lucké
<g/>
,	,	kIx,	,
kowelském	kowelský	k2eAgNnSc6d1	kowelský
a	a	k8xC	a
wlodzimierském	wlodzimierský	k2eAgNnSc6d1	wlodzimierský
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
krvavé	krvavý	k2eAgInPc4d1	krvavý
svátky	svátek	k1gInPc4	svátek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
další	další	k2eAgFnSc3d1	další
vlně	vlna	k1gFnSc3	vlna
útoků	útok	k1gInPc2	útok
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Využívajíce	využívat	k5eAaImSgFnP	využívat
stažení	stažení	k1gNnSc6	stažení
německých	německý	k2eAgFnPc2d1	německá
posádek	posádka	k1gFnPc2	posádka
před	před	k7c7	před
postupující	postupující	k2eAgFnSc7d1	postupující
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
oddíly	oddíl	k1gInPc1	oddíl
OUN	OUN	kA	OUN
a	a	k8xC	a
UPA	UPA	kA	UPA
na	na	k7c4	na
polské	polský	k2eAgMnPc4d1	polský
civilisty	civilista	k1gMnPc4	civilista
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
chyběla	chybět	k5eAaImAgFnS	chybět
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
obranná	obranný	k2eAgFnSc1d1	obranná
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Kuśkowcami	Kuśkowca	k1gFnPc7	Kuśkowca
Wielkimi	Wielki	k1gFnPc7	Wielki
a	a	k8xC	a
Śniegorówkou	Śniegorówka	k1gFnSc7	Śniegorówka
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
129	[number]	k4	129
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
Lanowiec	Lanowiec	k1gMnSc1	Lanowiec
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
v	v	k7c6	v
několika	několik	k4yIc6	několik
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
Vladimiru	Vladimir	k1gInSc2	Vladimir
Volyňského	volyňský	k2eAgInSc2d1	volyňský
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
140	[number]	k4	140
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
měsíci	měsíc	k1gInSc6	měsíc
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Wiśniowiec	Wiśniowiec	k1gMnSc1	Wiśniowiec
milice	milice	k1gFnSc2	milice
SB	sb	kA	sb
OUN	OUN	kA	OUN
zavraždila	zavraždit	k5eAaPmAgFnS	zavraždit
asi	asi	k9	asi
300	[number]	k4	300
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
T.	T.	kA	T.
Snydera	Snyder	k1gMnSc2	Snyder
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
nacionalistickými	nacionalistický	k2eAgInPc7d1	nacionalistický
oddíly	oddíl	k1gInPc7	oddíl
UPA	UPA	kA	UPA
zabito	zabít	k5eAaPmNgNnS	zabít
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
tisíc	tisíc	k4xCgInSc4	tisíc
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
oddíly	oddíl	k1gInPc1	oddíl
vojsk	vojsko	k1gNnPc2	vojsko
UPA	UPA	kA	UPA
přemístily	přemístit	k5eAaPmAgFnP	přemístit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
těžiště	těžiště	k1gNnSc4	těžiště
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
Lvovské	lvovský	k2eAgFnSc2d1	Lvovská
a	a	k8xC	a
Podolské	podolský	k2eAgFnSc2d1	Podolská
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
byly	být	k5eAaImAgInP	být
početněji	početně	k6eAd2	početně
obývány	obýván	k2eAgInPc4d1	obýván
Poláky	polák	k1gInPc4	polák
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Bug	Bug	k1gFnSc2	Bug
eskalovala	eskalovat	k5eAaImAgFnS	eskalovat
válka	válka	k1gFnSc1	válka
polsko-ukrajinských	polskokrajinský	k2eAgInPc2d1	polsko-ukrajinský
partyzánských	partyzánský	k2eAgInPc2d1	partyzánský
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizace	organizace	k1gFnSc2	organizace
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
v	v	k7c6	v
polských	polský	k2eAgFnPc6d1	polská
komunitách	komunita	k1gFnPc6	komunita
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
rizika	riziko	k1gNnSc2	riziko
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
polsko-ukrajinské	polskokrajinský	k2eAgFnPc4d1	polsko-ukrajinská
vesnice	vesnice	k1gFnPc4	vesnice
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1942	[number]	k4	1942
<g/>
/	/	kIx~	/
<g/>
1943	[number]	k4	1943
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
spontánně	spontánně	k6eAd1	spontánně
tvořit	tvořit	k5eAaImF	tvořit
vlastní	vlastní	k2eAgFnSc2d1	vlastní
obranné	obranný	k2eAgFnSc2d1	obranná
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
málo	málo	k6eAd1	málo
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
vyzbrojené	vyzbrojený	k2eAgFnPc1d1	vyzbrojená
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
především	především	k9	především
na	na	k7c4	na
hlídkování	hlídkování	k1gNnSc4	hlídkování
a	a	k8xC	a
systém	systém	k1gInSc4	systém
varování	varování	k1gNnSc4	varování
před	před	k7c7	před
útoky	útok	k1gInPc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
založeno	založit	k5eAaPmNgNnS	založit
něco	něco	k6eAd1	něco
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
100	[number]	k4	100
bodů	bod	k1gInPc2	bod
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
polských	polský	k2eAgMnPc2d1	polský
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
iniciativy	iniciativa	k1gFnSc2	iniciativa
civilních	civilní	k2eAgInPc2d1	civilní
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
před	před	k7c4	před
útoky	útok	k1gInPc4	útok
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
partyzánů	partyzán	k1gMnPc2	partyzán
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
v	v	k7c6	v
počátečních	počáteční	k2eAgFnPc6d1	počáteční
fázích	fáze	k1gFnPc6	fáze
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vlastními	vlastní	k2eAgFnPc7d1	vlastní
silami	síla	k1gFnPc7	síla
zhotovené	zhotovený	k2eAgNnSc1d1	zhotovené
uskupení	uskupení	k1gNnSc1	uskupení
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
:	:	kIx,	:
Jeziory	Jezior	k1gInPc1	Jezior
<g/>
,	,	kIx,	,
Szachy	Szach	k1gInPc1	Szach
<g/>
,	,	kIx,	,
Serniki	Serniki	k1gNnPc1	Serniki
<g/>
,	,	kIx,	,
Swarycewicze	Swarycewicze	k1gFnPc1	Swarycewicze
<g/>
,	,	kIx,	,
Borowe	Borow	k1gInPc1	Borow
<g/>
,	,	kIx,	,
Dubrowica	Dubrowicum	k1gNnPc1	Dubrowicum
<g/>
,	,	kIx,	,
Wolczyce	Wolczyec	k1gInPc1	Wolczyec
<g/>
,	,	kIx,	,
Huta	Hut	k2eAgFnSc1d1	Huta
Stepańska	Stepańska	k1gFnSc1	Stepańska
<g/>
,	,	kIx,	,
Dobryń	Dobryń	k1gFnSc1	Dobryń
<g/>
,	,	kIx,	,
Nowosiólki	Nowosiólki	k1gNnSc1	Nowosiólki
<g/>
,	,	kIx,	,
Ostrowsk	Ostrowsk	k1gInSc1	Ostrowsk
<g/>
,	,	kIx,	,
Wieluń	Wieluń	k1gFnSc1	Wieluń
<g/>
,	,	kIx,	,
Biala	Bialo	k1gNnPc1	Bialo
<g/>
,	,	kIx,	,
Komary	Komar	k1gInPc1	Komar
<g/>
,	,	kIx,	,
Huta	Hut	k2eAgNnPc1d1	Hut
Sopaczewska	Sopaczewsko	k1gNnPc1	Sopaczewsko
<g/>
,	,	kIx,	,
Haly	hala	k1gFnPc1	hala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kostopolském	kostopolský	k2eAgInSc6d1	kostopolský
okrese	okres	k1gInSc6	okres
byla	být	k5eAaImAgFnS	být
organizována	organizován	k2eAgFnSc1d1	organizována
sebeobranná	sebeobranný	k2eAgFnSc1d1	sebeobranná
uskupení	uskupení	k1gNnSc1	uskupení
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
:	:	kIx,	:
Gluszków	Gluszków	k1gFnSc1	Gluszków
<g/>
,	,	kIx,	,
Moczulanka	Moczulanka	k1gFnSc1	Moczulanka
<g/>
,	,	kIx,	,
Nowiny	Nowina	k1gMnSc2	Nowina
<g/>
,	,	kIx,	,
Stara	Star	k1gMnSc2	Star
Huta	Hutum	k1gNnSc2	Hutum
<g/>
,	,	kIx,	,
Bronislawówka	Bronislawówko	k1gNnSc2	Bronislawówko
<g/>
,	,	kIx,	,
Rudnia	Rudnium	k1gNnSc2	Rudnium
<g/>
,	,	kIx,	,
Stryj	Stryj	k1gFnSc1	Stryj
<g/>
,	,	kIx,	,
Mokre	Mokr	k1gMnSc5	Mokr
<g/>
,	,	kIx,	,
Myszków	Myszków	k1gFnSc5	Myszków
<g/>
,	,	kIx,	,
Zawolocz	Zawolocz	k1gMnSc1	Zawolocz
<g/>
,	,	kIx,	,
Marulczyn	Marulczyn	k1gMnSc1	Marulczyn
<g/>
,	,	kIx,	,
Woronusze	Woronusze	k1gFnSc1	Woronusze
<g/>
,	,	kIx,	,
Jakubówka	Jakubówka	k1gFnSc1	Jakubówka
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
velká	velký	k2eAgFnSc1d1	velká
základna	základna	k1gFnSc1	základna
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
regionu	region	k1gInSc6	region
Stara	Star	k1gMnSc2	Star
Huta	Hutus	k1gMnSc2	Hutus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
14	[number]	k4	14
vesnic	vesnice	k1gFnPc2	vesnice
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
společný	společný	k2eAgInSc4d1	společný
systém	systém	k1gInSc4	systém
obrany	obrana	k1gFnSc2	obrana
řízený	řízený	k2eAgInSc4d1	řízený
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Stará	starý	k2eAgFnSc1d1	stará
Huta	Huta	k1gFnSc1	Huta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
velká	velký	k2eAgFnSc1d1	velká
základna	základna	k1gFnSc1	základna
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
Przebrażu	Przebrażus	k1gInSc6	Przebrażus
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
opevněný	opevněný	k2eAgInSc4d1	opevněný
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
byla	být	k5eAaImAgFnS	být
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
jako	jako	k8xS	jako
sebeobranná	sebeobranný	k2eAgFnSc1d1	sebeobranná
a	a	k8xC	a
skládala	skládat	k5eAaImAgFnS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
4	[number]	k4	4
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
<g/>
,	,	kIx,	,
výzbroji	výzbroj	k1gFnSc3	výzbroj
a	a	k8xC	a
výcviku	výcvik	k1gInSc3	výcvik
jejích	její	k3xOp3gInPc2	její
členů	člen	k1gInPc2	člen
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
podporu	podpora	k1gFnSc4	podpora
Inspektorat	Inspektorat	k1gInSc1	Inspektorat
Luck	Luck	k1gMnSc1	Luck
AK.	AK.	k1gMnSc1	AK.
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1943	[number]	k4	1943
v	v	k7c6	v
Przebrażu	Przebrażus	k1gInSc6	Przebrażus
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
jednotka	jednotka	k1gFnSc1	jednotka
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
sedmi	sedm	k4xCc2	sedm
oddílů	oddíl	k1gInPc2	oddíl
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
průzkumného	průzkumný	k2eAgInSc2d1	průzkumný
jízdního	jízdní	k2eAgInSc2d1	jízdní
oddílu	oddíl	k1gInSc2	oddíl
(	(	kIx(	(
<g/>
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
obranné	obranný	k2eAgFnPc1d1	obranná
jednotky	jednotka	k1gFnPc1	jednotka
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
asi	asi	k9	asi
1000	[number]	k4	1000
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Kovelu	Kovel	k1gInSc2	Kovel
byly	být	k5eAaImAgFnP	být
organizovány	organizovat	k5eAaBmNgInP	organizovat
sebeobranná	sebeobranný	k2eAgNnPc4d1	sebeobranné
uskupení	uskupení	k1gNnSc4	uskupení
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
:	:	kIx,	:
Zasmyki	Zasmyki	k1gNnSc6	Zasmyki
<g/>
,	,	kIx,	,
Janówka	Janówek	k1gMnSc4	Janówek
<g/>
,	,	kIx,	,
Radomle	Radoml	k1gMnSc4	Radoml
<g/>
,	,	kIx,	,
Lityń	Lityń	k1gMnSc4	Lityń
<g/>
,	,	kIx,	,
Ossa	Ossus	k1gMnSc4	Ossus
<g/>
,	,	kIx,	,
Wierzbiczno	Wierzbiczna	k1gFnSc5	Wierzbiczna
<g/>
,	,	kIx,	,
Suszybaba	Suszybaba	k1gMnSc1	Suszybaba
<g/>
,	,	kIx,	,
Lublatyn	Lublatyn	k1gMnSc1	Lublatyn
<g/>
,	,	kIx,	,
Zielona	Zielona	k1gFnSc1	Zielona
<g/>
,	,	kIx,	,
Różyn	Różyn	k1gMnSc1	Różyn
<g/>
,	,	kIx,	,
Stara	Stara	k1gMnSc1	Stara
i	i	k8xC	i
Nowa	Nowa	k1gMnSc1	Nowa
Dąbrowa	Dąbrowa	k1gMnSc1	Dąbrowa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Vladimiru	Vladimir	k1gInSc2	Vladimir
Volyňského	volyňský	k2eAgInSc2d1	volyňský
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
sebeobranné	sebeobranný	k2eAgFnPc1d1	sebeobranná
jednotky	jednotka	k1gFnPc1	jednotka
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
:	:	kIx,	:
Spaszczyzna	Spaszczyzna	k1gFnSc1	Spaszczyzna
<g/>
,	,	kIx,	,
Wodzinów	Wodzinów	k1gFnSc1	Wodzinów
<g/>
,	,	kIx,	,
Wodzinek	Wodzinek	k1gInSc1	Wodzinek
<g/>
,	,	kIx,	,
Bielin	Bielin	k1gInSc1	Bielin
<g/>
,	,	kIx,	,
Sieliski	Sieliski	k1gNnSc1	Sieliski
<g/>
,	,	kIx,	,
Aleksandrówka	Aleksandrówka	k1gFnSc1	Aleksandrówka
<g/>
,	,	kIx,	,
Marianówka	Marianówka	k1gFnSc1	Marianówka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jednotky	jednotka	k1gFnPc1	jednotka
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Przebraże	Przebraże	k1gFnSc6	Przebraże
<g/>
,	,	kIx,	,
Huta	Hut	k2eAgFnSc1d1	Huta
Stepańska	Stepańska	k1gFnSc1	Stepańska
<g/>
,	,	kIx,	,
Pańska	Pańska	k1gFnSc1	Pańska
Dolina	dolina	k1gFnSc1	dolina
<g/>
,	,	kIx,	,
Stara	Stara	k1gFnSc1	Stara
Huta	Huta	k1gFnSc1	Huta
<g/>
,	,	kIx,	,
Zasmyki	Zasmyki	k1gNnSc1	Zasmyki
<g/>
,	,	kIx,	,
Bielin	Bielin	k1gInSc1	Bielin
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgNnPc1d2	menší
střediska	středisko	k1gNnPc1	středisko
nevydržela	vydržet	k5eNaPmAgNnP	vydržet
útoky	útok	k1gInPc4	útok
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
vesničanů	vesničan	k1gMnPc2	vesničan
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
měst	město	k1gNnPc2	město
či	či	k8xC	či
větších	veliký	k2eAgNnPc2d2	veliký
středisek	středisko	k1gNnPc2	středisko
se	s	k7c7	s
silnějšími	silný	k2eAgFnPc7d2	silnější
jednotkami	jednotka	k1gFnPc7	jednotka
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
taktika	taktika	k1gFnSc1	taktika
však	však	k9	však
nebránila	bránit	k5eNaImAgFnS	bránit
dalšímu	další	k2eAgNnSc3d1	další
vraždění	vraždění	k1gNnSc3	vraždění
ani	ani	k8xC	ani
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vládní	vládní	k2eAgFnSc1d1	vládní
delegace	delegace	k1gFnSc1	delegace
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
vydala	vydat	k5eAaPmAgFnS	vydat
instrukce	instrukce	k1gFnSc1	instrukce
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
základen	základna	k1gFnPc2	základna
a	a	k8xC	a
oddílů	oddíl	k1gInPc2	oddíl
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
sporá	sporý	k2eAgFnSc1d1	sporá
iniciativa	iniciativa	k1gFnSc1	iniciativa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
genocidy	genocida	k1gFnSc2	genocida
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1943	[number]	k4	1943
pouze	pouze	k6eAd1	pouze
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vytvořených	vytvořený	k2eAgNnPc2d1	vytvořené
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
středisek	středisko	k1gNnPc2	středisko
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
na	na	k7c6	na
území	území	k1gNnSc6	území
Volyně	Volyně	k1gFnSc2	Volyně
přežilo	přežít	k5eAaPmAgNnS	přežít
útoky	útok	k1gInPc7	útok
jen	jen	k9	jen
asi	asi	k9	asi
tucet	tucet	k1gInSc4	tucet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
větších	veliký	k2eAgFnPc2d2	veliký
základen	základna	k1gFnPc2	základna
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
(	(	kIx(	(
<g/>
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
sousedních	sousední	k2eAgFnPc2d1	sousední
vesnic	vesnice	k1gFnPc2	vesnice
obklopených	obklopený	k2eAgFnPc2d1	obklopená
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
terénních	terénní	k2eAgNnPc2d1	terénní
polských	polský	k2eAgNnPc2d1	polské
opevnění	opevnění	k1gNnPc2	opevnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podporovaných	podporovaný	k2eAgFnPc2d1	podporovaná
partyzánskými	partyzánský	k2eAgFnPc7d1	Partyzánská
jednotkami	jednotka	k1gFnPc7	jednotka
bylo	být	k5eAaImAgNnS	být
celkem	celek	k1gInSc7	celek
16	[number]	k4	16
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jednotky	jednotka	k1gFnPc1	jednotka
UPA	UPA	kA	UPA
dobyly	dobýt	k5eAaPmAgFnP	dobýt
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1944	[number]	k4	1944
partyzánské	partyzánský	k2eAgFnPc4d1	Partyzánská
jednotky	jednotka	k1gFnPc4	jednotka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
27	[number]	k4	27
<g/>
.	.	kIx.	.
volyňskou	volyňský	k2eAgFnSc4d1	Volyňská
pěší	pěší	k2eAgFnSc4d1	pěší
divizi	divize	k1gFnSc4	divize
<g/>
.	.	kIx.	.
</s>
<s>
Vznikem	vznik	k1gInSc7	vznik
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
významně	významně	k6eAd1	významně
omezeny	omezit	k5eAaPmNgFnP	omezit
polské	polský	k2eAgFnPc1d1	polská
ztráty	ztráta	k1gFnPc1	ztráta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
genocidě	genocida	k1gFnSc3	genocida
Poláků	Polák	k1gMnPc2	Polák
zabránit	zabránit	k5eAaPmF	zabránit
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Polská	polský	k2eAgFnSc1d1	polská
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
útoky	útok	k1gInPc4	útok
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přežili	přežít	k5eAaPmAgMnP	přežít
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
vojskům	vojsko	k1gNnPc3	vojsko
německé	německý	k2eAgFnSc2d1	německá
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1200	[number]	k4	1200
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
k	k	k7c3	k
jakýmkoliv	jakýkoliv	k3yIgFnPc3	jakýkoliv
sovětským	sovětský	k2eAgFnPc3d1	sovětská
partyzánským	partyzánský	k2eAgFnPc3d1	Partyzánská
jednotkám	jednotka	k1gFnPc3	jednotka
–	–	k?	–
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
nových	nový	k2eAgFnPc2d1	nová
jednotek	jednotka	k1gFnPc2	jednotka
–	–	k?	–
při	při	k7c6	při
pacifikaci	pacifikace	k1gFnSc6	pacifikace
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
propagačních	propagační	k2eAgInPc6d1	propagační
textech	text	k1gInPc6	text
OUN	OUN	kA	OUN
a	a	k8xC	a
UPA	UPA	kA	UPA
objevují	objevovat	k5eAaImIp3nP	objevovat
obvinění	obvinění	k1gNnPc4	obvinění
Poláků	Polák	k1gMnPc2	Polák
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
i	i	k8xC	i
Třetí	třetí	k4xOgFnSc7	třetí
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
překročila	překročit	k5eAaPmAgFnS	překročit
předválečné	předválečný	k2eAgFnPc4d1	předválečná
polské	polský	k2eAgFnPc4d1	polská
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
armády	armáda	k1gFnSc2	armáda
byla	být	k5eAaImAgFnS	být
nařízena	nařízen	k2eAgFnSc1d1	nařízena
mobilizace	mobilizace	k1gFnSc1	mobilizace
partyzánských	partyzánský	k2eAgInPc2d1	partyzánský
oddílů	oddíl	k1gInPc2	oddíl
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
zahájit	zahájit	k5eAaPmF	zahájit
akci	akce	k1gFnSc4	akce
Bouře	bouř	k1gFnSc2	bouř
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
27	[number]	k4	27
<g/>
.	.	kIx.	.
volyňská	volyňský	k2eAgFnSc1d1	Volyňská
pěší	pěší	k2eAgFnSc1d1	pěší
divize	divize	k1gFnSc1	divize
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
Wolyńską	Wolyńską	k?	Wolyńską
Dywizją	Dywizją	k1gFnSc1	Dywizją
Piechoty	Piechota	k1gFnSc2	Piechota
AK	AK	kA	AK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
akcí	akce	k1gFnPc2	akce
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
divize	divize	k1gFnSc2	divize
provedla	provést	k5eAaPmAgFnS	provést
16	[number]	k4	16
velkých	velký	k2eAgFnPc2d1	velká
vojenských	vojenský	k2eAgFnPc2d1	vojenská
akcí	akce	k1gFnPc2	akce
proti	proti	k7c3	proti
oddílům	oddíl	k1gInPc3	oddíl
UPA	UPA	kA	UPA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
směřovány	směřován	k2eAgInPc1d1	směřován
proti	proti	k7c3	proti
ohrožení	ohrožení	k1gNnSc3	ohrožení
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Polské	polský	k2eAgFnSc3d1	polská
oběti	oběť	k1gFnSc3	oběť
a	a	k8xC	a
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
===	===	k?	===
</s>
</p>
<p>
<s>
Zjištění	zjištění	k1gNnSc1	zjištění
přesného	přesný	k2eAgInSc2d1	přesný
počtu	počet	k1gInSc2	počet
Poláků	Polák	k1gMnPc2	Polák
zabitých	zabitý	k2eAgMnPc2d1	zabitý
během	během	k7c2	během
volynského	volynský	k2eAgInSc2d1	volynský
masakru	masakr	k1gInSc2	masakr
stále	stále	k6eAd1	stále
čelí	čelit	k5eAaImIp3nS	čelit
obtížím	obtíž	k1gFnPc3	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
vesnice	vesnice	k1gFnPc1	vesnice
byly	být	k5eAaImAgFnP	být
zcela	zcela	k6eAd1	zcela
srovnány	srovnat	k5eAaPmNgFnP	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
jejich	jejich	k3xOp3gMnPc1	jejich
obyvatelé	obyvatel	k1gMnPc1	obyvatel
zabiti	zabít	k5eAaPmNgMnP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Vraždy	vražda	k1gFnPc1	vražda
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
postižených	postižený	k2eAgFnPc2d1	postižená
partyzánskou	partyzánský	k2eAgFnSc7d1	Partyzánská
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
chaosem	chaos	k1gInSc7	chaos
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nebyly	být	k5eNaImAgInP	být
zločiny	zločin	k1gInPc1	zločin
ani	ani	k8xC	ani
zdokumentovány	zdokumentován	k2eAgFnPc1d1	zdokumentována
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
svědků	svědek	k1gMnPc2	svědek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přežili	přežít	k5eAaPmAgMnP	přežít
masakr	masakr	k1gInSc4	masakr
<g/>
,	,	kIx,	,
uteklo	utéct	k5eAaPmAgNnS	utéct
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Volyně	Volyně	k1gFnSc2	Volyně
nebo	nebo	k8xC	nebo
byli	být	k5eAaImAgMnP	být
deportováni	deportovat	k5eAaBmNgMnP	deportovat
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byli	být	k5eAaImAgMnP	být
roztroušeni	roztrousit	k5eAaPmNgMnP	roztrousit
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Polské	polský	k2eAgInPc1d1	polský
orgány	orgán	k1gInPc1	orgán
neprováděly	provádět	k5eNaImAgInP	provádět
šetření	šetření	k1gNnSc4	šetření
zločinů	zločin	k1gMnPc2	zločin
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ztráty	ztráta	k1gFnSc2	ztráta
pohraničí	pohraničí	k1gNnSc2	pohraničí
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvním	první	k4xOgInPc3	první
určitějším	určitý	k2eAgInPc3d2	určitější
údajům	údaj	k1gInPc3	údaj
se	se	k3xPyFc4	se
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
soukromým	soukromý	k2eAgInSc7d1	soukromý
výzkumem	výzkum	k1gInSc7	výzkum
několika	několik	k4yIc2	několik
polských	polský	k2eAgMnPc2d1	polský
badatelů	badatel	k1gMnPc2	badatel
provedeném	provedený	k2eAgInSc6d1	provedený
v	v	k7c4	v
pozdních	pozdní	k2eAgNnPc2d1	pozdní
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
těchto	tento	k3xDgFnPc2	tento
snah	snaha	k1gFnPc2	snaha
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
Józefem	Józef	k1gInSc7	Józef
Turowskim	Turowskim	k1gInSc1	Turowskim
a	a	k8xC	a
Wladyslawem	Wladyslawem	k1gInSc1	Wladyslawem
Siemaszkem	Siemaszek	k1gInSc7	Siemaszek
v	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
Zbrodnie	Zbrodnie	k1gFnSc2	Zbrodnie
nacjonalistów	nacjonalistów	k?	nacjonalistów
ukraińskich	ukraińskich	k1gInSc1	ukraińskich
dokonane	dokonanout	k5eAaPmIp3nS	dokonanout
na	na	k7c6	na
ludności	ludnośec	k1gInSc6	ludnośec
polskiej	polskiej	k1gInSc1	polskiej
na	na	k7c6	na
Wolyniu	Wolynium	k1gNnSc6	Wolynium
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zločiny	zločin	k1gInPc1	zločin
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
vykonané	vykonaný	k2eAgInPc1d1	vykonaný
na	na	k7c6	na
polském	polský	k2eAgNnSc6d1	polské
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
Volyně	Volyně	k1gFnSc2	Volyně
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ludobójstwo	Ludobójstwo	k6eAd1	Ludobójstwo
dokonane	dokonanout	k5eAaPmIp3nS	dokonanout
przez	przez	k1gMnSc1	przez
nacjonalistów	nacjonalistów	k?	nacjonalistów
ukraińskich	ukraińskich	k1gMnSc1	ukraińskich
na	na	k7c6	na
ludności	ludnośce	k1gFnSc6	ludnośce
polskiej	polskiej	k1gInSc4	polskiej
Wolynia	Wolynium	k1gNnSc2	Wolynium
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Genocida	genocida	k1gFnSc1	genocida
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
proti	proti	k7c3	proti
polskému	polský	k2eAgNnSc3d1	polské
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
od	od	k7c2	od
autorů	autor	k1gMnPc2	autor
Wladyslawa	Wladyslawus	k1gMnSc2	Wladyslawus
a	a	k8xC	a
Ewy	Ewy	k1gMnPc2	Ewy
Siemaszkových	Siemaszkový	k2eAgMnPc2d1	Siemaszkový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
studie	studie	k1gFnSc2	studie
Siemaszkových	Siemaszkový	k2eAgMnPc2d1	Siemaszkový
bylo	být	k5eAaImAgNnS	být
zdokumentováno	zdokumentovat	k5eAaPmNgNnS	zdokumentovat
36	[number]	k4	36
543-36	[number]	k4	543-36
750	[number]	k4	750
polských	polský	k2eAgFnPc2d1	polská
obětí	oběť	k1gFnPc2	oběť
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
identifikace	identifikace	k1gFnSc1	identifikace
zavražděných	zavražděný	k2eAgMnPc2d1	zavražděný
obyvatel	obyvatel	k1gMnPc2	obyvatel
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
13	[number]	k4	13
500	[number]	k4	500
až	až	k9	až
23	[number]	k4	23
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
okolnosti	okolnost	k1gFnPc1	okolnost
smrti	smrt	k1gFnSc2	smrt
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	on	k3xPp3gMnPc4	on
autory	autor	k1gMnPc4	autor
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
odhadován	odhadován	k2eAgMnSc1d1	odhadován
na	na	k7c4	na
50-60	[number]	k4	50-60
000	[number]	k4	000
nacionalisty	nacionalista	k1gMnPc7	nacionalista
zavražděných	zavražděný	k2eAgMnPc2d1	zavražděný
civilistů	civilista	k1gMnPc2	civilista
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podobnému	podobný	k2eAgInSc3d1	podobný
odhadu	odhad	k1gInSc3	odhad
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
Grzegorz	Grzegorz	k1gInSc4	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Kęsik	Kęsik	k1gMnSc1	Kęsik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
historici	historik	k1gMnPc1	historik
a	a	k8xC	a
badatelé	badatel	k1gMnPc1	badatel
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
polské	polský	k2eAgFnPc4d1	polská
oběti	oběť	k1gFnPc4	oběť
masakru	masakr	k1gInSc2	masakr
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Ryszard	Ryszard	k1gInSc1	Ryszard
Torzecki	Torzeck	k1gFnSc2	Torzeck
<g/>
:	:	kIx,	:
30	[number]	k4	30
000	[number]	k4	000
až	až	k9	až
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
obětí	oběť	k1gFnPc2	oběť
Aleksander	Aleksander	k1gMnSc1	Aleksander
Korman	Korman	k1gMnSc1	Korman
<g/>
:	:	kIx,	:
68	[number]	k4	68
700	[number]	k4	700
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
Tadeusz	Tadeusz	k1gInSc1	Tadeusz
A.	A.	kA	A.
Olszański	Olszańsk	k1gFnSc2	Olszańsk
<g/>
:	:	kIx,	:
60	[number]	k4	60
000	[number]	k4	000
až	až	k9	až
80	[number]	k4	80
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Józef	Józef	k1gInSc1	Józef
Turowski	Turowsk	k1gFnSc2	Turowsk
<g/>
:	:	kIx,	:
60	[number]	k4	60
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
Wincenty	Wincenta	k1gFnSc2	Wincenta
Romanowski	Romanowsk	k1gFnSc2	Romanowsk
<g/>
:	:	kIx,	:
70	[number]	k4	70
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
Pawel	Pawel	k1gMnSc1	Pawel
Wieczorkiewicz	Wieczorkiewicz	k1gMnSc1	Wieczorkiewicz
<g/>
:	:	kIx,	:
40	[number]	k4	40
000	[number]	k4	000
až	až	k9	až
70	[number]	k4	70
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
Ewa	Ewa	k1gFnPc2	Ewa
Siemaszko	Siemaszka	k1gFnSc5	Siemaszka
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
odhady	odhad	k1gInPc1	odhad
polských	polský	k2eAgFnPc2d1	polská
obětí	oběť	k1gFnPc2	oběť
masakru	masakr	k1gInSc2	masakr
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
<g/>
,	,	kIx,	,
zahrnujíce	zahrnovat	k5eAaImSgInP	zahrnovat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
oběti	oběť	k1gFnPc4	oběť
v	v	k7c6	v
období	období	k1gNnSc6	období
<g />
.	.	kIx.	.
</s>
<s>
před	před	k7c7	před
genocidou	genocida	k1gFnSc7	genocida
na	na	k7c4	na
60	[number]	k4	60
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
nejpravděpodobnější	pravděpodobný	k2eAgMnPc1d3	nejpravděpodobnější
polští	polský	k2eAgMnPc1d1	polský
historici	historik	k1gMnPc1	historik
v	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
považují	považovat	k5eAaImIp3nP	považovat
odhad	odhad	k1gInSc4	odhad
počtu	počet	k1gInSc2	počet
zavražděných	zavražděný	k2eAgMnPc2d1	zavražděný
Poláků	Polák	k1gMnPc2	Polák
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
50-60	[number]	k4	50-60
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
a	a	k8xC	a
17	[number]	k4	17
000	[number]	k4	000
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
Poláků	Polák	k1gMnPc2	Polák
hledala	hledat	k5eAaImAgFnS	hledat
útočiště	útočiště	k1gNnSc4	útočiště
ve	v	k7c6	v
městech	město	k1gNnPc6	město
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
cca	cca	kA	cca
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgFnSc1d1	jiná
část	část	k1gFnSc1	část
odešla	odejít	k5eAaPmAgFnS	odejít
k	k	k7c3	k
sovětským	sovětský	k2eAgMnPc3d1	sovětský
partyzánům	partyzán	k1gMnPc3	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1	celková
ztráty	ztráta	k1gFnPc1	ztráta
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Volyni	Volyně	k1gFnSc4	Volyně
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
zraněných	zraněný	k1gMnPc2	zraněný
<g/>
,	,	kIx,	,
deportovaných	deportovaný	k2eAgFnPc2d1	deportovaná
(	(	kIx(	(
<g/>
a	a	k8xC	a
zavražděných	zavražděný	k2eAgFnPc2d1	zavražděná
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byli	být	k5eAaImAgMnP	být
zatčeni	zatčen	k2eAgMnPc1d1	zatčen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
NKVD	NKVD	kA	NKVD
v	v	k7c6	v
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
poslala	poslat	k5eAaPmAgFnS	poslat
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
do	do	k7c2	do
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
(	(	kIx(	(
<g/>
cca	cca	kA	cca
45	[number]	k4	45
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zavražděných	zavražděný	k2eAgInPc2d1	zavražděný
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
NKVD	NKVD	kA	NKVD
v	v	k7c6	v
Lucku	Lucko	k1gNnSc6	Lucko
<g/>
,	,	kIx,	,
Rivne	Rivn	k1gInSc5	Rivn
a	a	k8xC	a
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1941	[number]	k4	1941
(	(	kIx(	(
<g/>
po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Třetí	třetí	k4xOgFnSc7	třetí
říší	říš	k1gFnSc7	říš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
deportovaných	deportovaný	k2eAgFnPc2d1	deportovaná
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
25	[number]	k4	25
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zavražděných	zavražděný	k2eAgFnPc2d1	zavražděná
při	při	k7c6	při
pacifikaci	pacifikace	k1gFnSc6	pacifikace
německou	německý	k2eAgFnSc7d1	německá
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
uprchlíků	uprchlík	k1gMnPc2	uprchlík
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
asi	asi	k9	asi
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Polské	polský	k2eAgFnSc2d1	polská
ilegální	ilegální	k2eAgFnSc2d1	ilegální
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
okresní	okresní	k2eAgMnSc1d1	okresní
delegát	delegát	k1gMnSc1	delegát
vedení	vedení	k1gNnSc2	vedení
<g/>
)	)	kIx)	)
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
ze	z	k7c2	z
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1943	[number]	k4	1943
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
po	po	k7c6	po
největší	veliký	k2eAgFnSc6d3	veliký
vlně	vlna	k1gFnSc6	vlna
vraždění	vraždění	k1gNnSc4	vraždění
<g/>
)	)	kIx)	)
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
ztráty	ztráta	k1gFnPc4	ztráta
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c4	na
170	[number]	k4	170
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
11	[number]	k4	11
městech	město	k1gNnPc6	město
a	a	k8xC	a
25	[number]	k4	25
okresních	okresní	k2eAgFnPc6d1	okresní
bráněných	bráněný	k2eAgFnPc6d1	bráněná
základnách	základna	k1gFnPc6	základna
<g/>
.	.	kIx.	.
<g/>
Část	část	k1gFnSc1	část
údajů	údaj	k1gInPc2	údaj
získaných	získaný	k2eAgInPc2d1	získaný
od	od	k7c2	od
Siemaszka	Siemaszka	k1gFnSc1	Siemaszka
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nadhodnocenou	nadhodnocený	k2eAgFnSc4d1	nadhodnocená
ukrajinským	ukrajinský	k2eAgMnSc7d1	ukrajinský
místopiscem	místopisec	k1gMnSc7	místopisec
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Carukou	Caruký	k2eAgFnSc7d1	Caruký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	on	k3xPp3gInSc2	on
vlastního	vlastní	k2eAgInSc2d1	vlastní
výzkumu	výzkum	k1gInSc2	výzkum
ve	v	k7c6	v
vladimirském	vladimirský	k2eAgInSc6d1	vladimirský
okrese	okres	k1gInSc6	okres
byl	být	k5eAaImAgMnS	být
autory	autor	k1gMnPc4	autor
nadhodnocen	nadhodnocen	k2eAgInSc4d1	nadhodnocen
počet	počet	k1gInSc4	počet
obětí	oběť	k1gFnPc2	oběť
o	o	k7c4	o
1916	[number]	k4	1916
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
lidu	lid	k1gInSc2	lid
snížen	snížit	k5eAaPmNgInS	snížit
o	o	k7c4	o
1184	[number]	k4	1184
<g/>
.	.	kIx.	.
</s>
<s>
Carukovy	Carukův	k2eAgFnPc1d1	Carukův
tvrzení	tvrzení	k1gNnPc4	tvrzení
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
polskými	polský	k2eAgNnPc7d1	polské
svědectvími	svědectví	k1gNnPc7	svědectví
<g/>
.	.	kIx.	.
</s>
<s>
Gregory	Gregor	k1gMnPc4	Gregor
Hryciuk	Hryciuko	k1gNnPc2	Hryciuko
Carukovi	Carukův	k2eAgMnPc1d1	Carukův
vytýká	vytýkat	k5eAaImIp3nS	vytýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
svému	svůj	k3xOyFgInSc3	svůj
spisovatelskému	spisovatelský	k2eAgInSc3d1	spisovatelský
talentu	talent	k1gInSc3	talent
<g/>
.	.	kIx.	.
</s>
<s>
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
oběti	oběť	k1gFnPc4	oběť
připisované	připisovaný	k2eAgFnPc4d1	připisovaná
Carukou	Caruka	k1gFnSc7	Caruka
Polákům	Polák	k1gMnPc3	Polák
<g/>
,	,	kIx,	,
německým	německý	k2eAgMnPc3d1	německý
zločinům	zločin	k1gMnPc3	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Caruka	Caruk	k1gMnSc2	Caruk
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
věrohodně	věrohodně	k6eAd1	věrohodně
ověřeny	ověřen	k2eAgInPc1d1	ověřen
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
okresech	okres	k1gInPc6	okres
lvovském	lvovský	k2eAgNnSc6d1	lvovské
<g/>
,	,	kIx,	,
tarnopolském	tarnopolský	k2eAgNnSc6d1	tarnopolský
a	a	k8xC	a
stanislawowském	stanislawowské	k1gNnSc6	stanislawowské
při	při	k7c6	při
podobných	podobný	k2eAgInPc6d1	podobný
útocích	útok	k1gInPc6	útok
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
od	od	k7c2	od
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
do	do	k7c2	do
asi	asi	k9	asi
70	[number]	k4	70
tisíc	tisíc	k4xCgInPc2	tisíc
polských	polský	k2eAgMnPc2d1	polský
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okresech	okres	k1gInPc6	okres
poleskim	poleskim	k6eAd1	poleskim
i	i	k9	i
lubelskim	lubelskim	k6eAd1	lubelskim
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
10-20000	[number]	k4	10-20000
civilistů	civilista	k1gMnPc2	civilista
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
–	–	k?	–
celkem	celkem	k6eAd1	celkem
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
padl	padnout	k5eAaPmAgInS	padnout
obětí	oběť	k1gFnSc7	oběť
různých	různý	k2eAgFnPc2d1	různá
etnických	etnický	k2eAgFnPc2d1	etnická
čistek	čistka	k1gFnPc2	čistka
od	od	k7c2	od
80	[number]	k4	80
do	do	k7c2	do
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ryszard	Ryszard	k1gInSc1	Ryszard
Torzecki	Torzeck	k1gFnSc2	Torzeck
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
84	[number]	k4	84
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
100	[number]	k4	100
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
85	[number]	k4	85
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
120	[number]	k4	120
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Czeslaw	Czeslaw	k1gMnSc1	Czeslaw
Partacz	Partacz	k1gMnSc1	Partacz
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
o	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
než	než	k8xS	než
130.000	[number]	k4	130.000
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ewa	Ewa	k1gFnSc1	Ewa
Siemaszko	Siemaszka	k1gFnSc5	Siemaszka
<g/>
)	)	kIx)	)
osob	osoba	k1gFnPc2	osoba
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zabiti	zabít	k5eAaPmNgMnP	zabít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
polských	polský	k2eAgFnPc2d1	polská
aktivit	aktivita	k1gFnPc2	aktivita
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
oblasti	oblast	k1gFnSc6	oblast
Grzegorz	Grzegorza	k1gFnPc2	Grzegorza
Motyka	motyka	k1gFnSc1	motyka
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
10-15	[number]	k4	10-15
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Ryszard	Ryszard	k1gInSc1	Ryszard
Szawlowski	Szawlowsk	k1gFnSc2	Szawlowsk
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Genocidum	Genocidum	k1gNnSc1	Genocidum
atrox	atrox	k1gInSc1	atrox
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hrozná	hrozný	k2eAgFnSc1d1	hrozná
<g/>
,	,	kIx,	,
krutá	krutý	k2eAgFnSc1d1	krutá
genocida	genocida	k1gFnSc1	genocida
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
publikovaném	publikovaný	k2eAgInSc6d1	publikovaný
jako	jako	k8xC	jako
Trzy	Trza	k1gMnSc2	Trza
ludobójstwa	ludobójstwus	k1gMnSc2	ludobójstwus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Tři	tři	k4xCgFnPc1	tři
genocidy	genocida	k1gFnPc1	genocida
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
tezi	teze	k1gFnSc4	teze
o	o	k7c6	o
rovnocennosti	rovnocennost	k1gFnSc6	rovnocennost
zločinů	zločin	k1gInPc2	zločin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
Třetí	třetí	k4xOgNnSc1	třetí
říší	říš	k1gFnPc2	říš
<g/>
,	,	kIx,	,
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
nacionalisty	nacionalista	k1gMnPc7	nacionalista
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
zločinům	zločin	k1gMnPc3	zločin
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
vyšší	vysoký	k2eAgFnSc4d2	vyšší
klasifikaci	klasifikace	k1gFnSc4	klasifikace
než	než	k8xS	než
u	u	k7c2	u
německých	německý	k2eAgInPc2d1	německý
a	a	k8xC	a
sovětských	sovětský	k2eAgInPc2d1	sovětský
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
jeho	jeho	k3xOp3gFnSc2	jeho
studie	studie	k1gFnSc2	studie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
zločinů	zločin	k1gInPc2	zločin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
ukrajinskými	ukrajinský	k2eAgInPc7d1	ukrajinský
nacionalisty	nacionalista	k1gMnPc4	nacionalista
proti	proti	k7c3	proti
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
a	a	k8xC	a
Podolí	Podolí	k1gNnSc6	Podolí
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
provádí	provádět	k5eAaImIp3nS	provádět
místní	místní	k2eAgNnSc4d1	místní
oddělení	oddělení	k1gNnSc4	oddělení
Instytutu	Instytut	k2eAgFnSc4d1	Instytut
Pamięci	Pamięce	k1gFnSc4	Pamięce
Narodowej	Narodowej	k1gInSc1	Narodowej
<g/>
.	.	kIx.	.
</s>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
Komise	komise	k1gFnSc2	komise
ŚZPNP	ŚZPNP	kA	ŚZPNP
v	v	k7c6	v
Rzeszówie	Rzeszówie	k1gFnSc1	Rzeszówie
-	-	kIx~	-
vede	vést	k5eAaImIp3nS	vést
"	"	kIx"	"
<g/>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
ukrajinskými	ukrajinský	k2eAgInPc7d1	ukrajinský
nacionalisty	nacionalista	k1gMnPc4	nacionalista
proti	proti	k7c3	proti
polské	polský	k2eAgFnSc3d1	polská
populaci	populace	k1gFnSc3	populace
žijící	žijící	k2eAgFnSc3d1	žijící
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Přemyšl	Přemyšl	k1gFnSc1	Přemyšl
v	v	k7c6	v
letech	let	k1gInPc6	let
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oběti	oběť	k1gFnPc1	oběť
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
než	než	k8xS	než
polskou	polský	k2eAgFnSc7d1	polská
národností	národnost	k1gFnSc7	národnost
===	===	k?	===
</s>
</p>
<p>
<s>
Ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
vraždili	vraždit	k5eAaImAgMnP	vraždit
také	také	k9	také
Židy	Žid	k1gMnPc4	Žid
ale	ale	k8xC	ale
i	i	k9	i
Ukrajince	Ukrajinec	k1gMnPc4	Ukrajinec
kteří	který	k3yQgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
příznivci	příznivec	k1gMnPc1	příznivec
UPA	UPA	kA	UPA
a	a	k8xC	a
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
Polákům	Polák	k1gMnPc3	Polák
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vraždili	vraždit	k5eAaImAgMnP	vraždit
také	také	k9	také
kolaboranty	kolaborant	k1gMnPc4	kolaborant
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
obětí	oběť	k1gFnPc2	oběť
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
mezi	mezi	k7c7	mezi
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
národnostně	národnostně	k6eAd1	národnostně
smíšená	smíšený	k2eAgNnPc4d1	smíšené
manželství	manželství	k1gNnPc4	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
propočtů	propočet	k1gInPc2	propočet
Siemaszka	Siemaszka	k1gFnSc1	Siemaszka
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
rukama	ruka	k1gFnPc7	ruka
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
zavražděno	zavražděn	k2eAgNnSc1d1	zavražděno
<g/>
:	:	kIx,	:
846	[number]	k4	846
<g/>
–	–	k?	–
<g/>
847	[number]	k4	847
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
,	,	kIx,	,
1210	[number]	k4	1210
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
342	[number]	k4	342
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
135	[number]	k4	135
<g/>
–	–	k?	–
<g/>
136	[number]	k4	136
Rusů	Rus	k1gMnPc2	Rus
a	a	k8xC	a
70	[number]	k4	70
osob	osoba	k1gFnPc2	osoba
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
data	datum	k1gNnPc1	datum
zdaleka	zdaleka	k6eAd1	zdaleka
nejsou	být	k5eNaImIp3nP	být
úplná	úplný	k2eAgFnSc1d1	úplná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
obyvatel	obyvatel	k1gMnSc1	obyvatel
polské	polský	k2eAgFnSc2d1	polská
národnosti	národnost	k1gFnSc2	národnost
podle	podle	k7c2	podle
Grzegorza	Grzegorz	k1gMnSc2	Grzegorz
Motyky	motyka	k1gFnSc2	motyka
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
asi	asi	k9	asi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
000	[number]	k4	000
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
.	.	kIx.	.
</s>
<s>
Gregory	Gregor	k1gMnPc4	Gregor
Hryciuk	Hryciuko	k1gNnPc2	Hryciuko
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
na	na	k7c4	na
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
2,2	[number]	k4	2,2
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
zavražděných	zavražděný	k2eAgMnPc2d1	zavražděný
obyvatel	obyvatel	k1gMnPc2	obyvatel
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
Wolodymyr	Wolodymyr	k1gMnSc1	Wolodymyr
Serhijczuk	Serhijczuk	k1gMnSc1	Serhijczuk
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
rovině	rovina	k1gFnSc6	rovina
"	"	kIx"	"
<g/>
několika	několik	k4yIc7	několik
tisících	tisící	k4xOgMnPc6	tisící
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Obětí	oběť	k1gFnSc7	oběť
etnických	etnický	k2eAgFnPc2d1	etnická
čistek	čistka	k1gFnPc2	čistka
byla	být	k5eAaImAgFnS	být
i	i	k9	i
malá	malý	k2eAgFnSc1d1	malá
komunita	komunita	k1gFnSc1	komunita
Arménů	Armén	k1gMnPc2	Armén
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
Haliči	Halič	k1gFnSc6	Halič
<g/>
,	,	kIx,	,
zavražděných	zavražděný	k2eAgMnPc2d1	zavražděný
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
polskými	polský	k2eAgMnPc7d1	polský
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
arménský	arménský	k2eAgMnSc1d1	arménský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Joseph	Joseph	k1gMnSc1	Joseph
Teodorowicz	Teodorowicz	k1gMnSc1	Teodorowicz
výrazně	výrazně	k6eAd1	výrazně
podporoval	podporovat	k5eAaImAgMnS	podporovat
připojení	připojení	k1gNnSc4	připojení
Lvova	Lvov	k1gInSc2	Lvov
k	k	k7c3	k
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1944	[number]	k4	1944
v	v	k7c6	v
Kutach	Kuta	k1gFnPc6	Kuta
nad	nad	k7c7	nad
Czeremoszem	Czeremosz	k1gInSc7	Czeremosz
v	v	k7c6	v
Pokutí	pokutit	k5eAaPmIp3nP	pokutit
(	(	kIx(	(
<g/>
známém	známý	k2eAgInSc6d1	známý
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
osídlení	osídlení	k1gNnSc1	osídlení
polskými	polský	k2eAgMnPc7d1	polský
Armény	Armén	k1gMnPc7	Armén
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
asi	asi	k9	asi
500	[number]	k4	500
polských	polský	k2eAgMnPc2d1	polský
Arménů	Armén	k1gMnPc2	Armén
a	a	k8xC	a
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Český	český	k2eAgInSc1d1	český
Malín	Malín	k1gInSc1	Malín
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Volyňského	volyňský	k2eAgInSc2d1	volyňský
masakru	masakr	k1gInSc2	masakr
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Volyně	Volyně	k1gFnSc2	Volyně
vládla	vládnout	k5eAaImAgFnS	vládnout
anarchie	anarchie	k1gFnSc1	anarchie
a	a	k8xC	a
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
s	s	k7c7	s
obyvateli	obyvatel	k1gMnPc7	obyvatel
české	český	k2eAgFnSc2d1	Česká
národnosti	národnost	k1gFnSc2	národnost
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
vzájemným	vzájemný	k2eAgFnPc3d1	vzájemná
ozbrojeným	ozbrojený	k2eAgFnPc3d1	ozbrojená
střetům	střet	k1gInPc3	střet
partyzánských	partyzánský	k2eAgMnPc2d1	partyzánský
oddílů	oddíl	k1gInPc2	oddíl
i	i	k8xC	i
odvetným	odvetný	k2eAgFnPc3d1	odvetná
akcím	akce	k1gFnPc3	akce
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
Civilní	civilní	k2eAgNnSc1d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
okrádáno	okrádat	k5eAaImNgNnS	okrádat
a	a	k8xC	a
beztrestně	beztrestně	k6eAd1	beztrestně
vražděno	vraždit	k5eAaImNgNnS	vraždit
armádou	armáda	k1gFnSc7	armáda
i	i	k8xC	i
partyzány	partyzán	k1gMnPc7	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Čechů	Čech	k1gMnPc2	Čech
byla	být	k5eAaImAgFnS	být
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
partyzány	partyzán	k1gMnPc7	partyzán
upálena	upálen	k2eAgFnSc1d1	upálena
zaživa	zaživa	k6eAd1	zaživa
ve	v	k7c6	v
stodolách	stodola	k1gFnPc6	stodola
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
žen	žena	k1gFnPc2	žena
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
starých	starý	k2eAgFnPc2d1	stará
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
obyvateli	obyvatel	k1gMnPc7	obyvatel
české	český	k2eAgFnSc2d1	Česká
národnosti	národnost	k1gFnSc2	národnost
Český	český	k2eAgInSc1d1	český
Malín	Malín	k1gInSc1	Malín
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
srovnána	srovnat	k5eAaPmNgFnS	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
nacisty	nacista	k1gMnSc2	nacista
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
dostupných	dostupný	k2eAgInPc2d1	dostupný
zdrojů	zdroj	k1gInPc2	zdroj
mimo	mimo	k6eAd1	mimo
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
okupačních	okupační	k2eAgFnPc2d1	okupační
jednotek	jednotka	k1gFnPc2	jednotka
SS	SS	kA	SS
i	i	k9	i
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
polského	polský	k2eAgMnSc2d1	polský
a	a	k8xC	a
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
oddílu	oddíl	k1gInSc2	oddíl
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
z	z	k7c2	z
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
obce	obec	k1gFnSc2	obec
Nerotůvka	Nerotůvka	k1gFnSc1	Nerotůvka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
popisováno	popisován	k2eAgNnSc1d1	popisováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
součástí	součást	k1gFnSc7	součást
jednotky	jednotka	k1gFnSc2	jednotka
byli	být	k5eAaImAgMnP	být
i	i	k9	i
rusky	rusky	k6eAd1	rusky
mluvící	mluvící	k2eAgMnPc1d1	mluvící
vojáci	voják	k1gMnPc1	voják
(	(	kIx(	(
<g/>
uváděno	uvádět	k5eAaImNgNnS	uvádět
že	že	k8xS	že
bělogvardějci	bělogvardějec	k1gMnPc1	bělogvardějec
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
členové	člen	k1gMnPc1	člen
Ruské	ruský	k2eAgFnSc2d1	ruská
osvobozenecké	osvobozenecký	k2eAgFnSc2d1	osvobozenecká
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rusky	rusky	k6eAd1	rusky
mluvící	mluvící	k2eAgMnPc1d1	mluvící
vojáci	voják	k1gMnPc1	voják
německého	německý	k2eAgInSc2d1	německý
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
,	,	kIx,	,
Vlasovci	vlasovec	k1gMnPc5	vlasovec
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
později	pozdě	k6eAd2	pozdě
vysláni	vyslat	k5eAaPmNgMnP	vyslat
na	na	k7c4	na
trestní	trestní	k2eAgFnSc4d1	trestní
výpravu	výprava	k1gFnSc4	výprava
k	k	k7c3	k
vykonání	vykonání	k1gNnSc3	vykonání
hromadných	hromadný	k2eAgFnPc2d1	hromadná
poprav	poprava	k1gFnPc2	poprava
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
české	český	k2eAgFnSc2d1	Česká
vesnice	vesnice	k1gFnSc2	vesnice
Český	český	k2eAgInSc1d1	český
Volkov	Volkov	k1gInSc1	Volkov
<g/>
.	.	kIx.	.
<g/>
Jsou	být	k5eAaImIp3nP	být
zveřejňovány	zveřejňovat	k5eAaImNgFnP	zveřejňovat
i	i	k9	i
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nešlo	jít	k5eNaImAgNnS	jít
u	u	k7c2	u
masakru	masakr	k1gInSc2	masakr
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Malíně	Malín	k1gInSc6	Malín
o	o	k7c6	o
provokaci	provokace	k1gFnSc6	provokace
UPA	UPA	kA	UPA
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jednotka	jednotka	k1gFnSc1	jednotka
banderovců	banderovec	k1gMnPc2	banderovec
Petra	Petr	k1gMnSc2	Petr
Netoviče	Netovič	k1gMnSc2	Netovič
<g/>
,	,	kIx,	,
vydávající	vydávající	k2eAgFnSc7d1	vydávající
se	se	k3xPyFc4	se
za	za	k7c4	za
sovětské	sovětský	k2eAgFnPc4d1	sovětská
partyzány	partyzána	k1gFnPc4	partyzána
<g/>
,	,	kIx,	,
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
polské	polský	k2eAgFnSc2d1	polská
vesnice	vesnice	k1gFnSc2	vesnice
Parosle	Parosle	k1gFnSc2	Parosle
a	a	k8xC	a
vyvraždila	vyvraždit	k5eAaPmAgFnS	vyvraždit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sběr	sběr	k1gInSc1	sběr
dokladů	doklad	k1gInPc2	doklad
historické	historický	k2eAgFnSc2d1	historická
kriminality	kriminalita	k1gFnSc2	kriminalita
===	===	k?	===
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
státních	státní	k2eAgFnPc2d1	státní
institucí	instituce	k1gFnPc2	instituce
zřízených	zřízený	k2eAgFnPc2d1	zřízená
zákonem	zákon	k1gInSc7	zákon
k	k	k7c3	k
prošetření	prošetření	k1gNnSc3	prošetření
<g/>
,	,	kIx,	,
dokumentaci	dokumentace	k1gFnSc3	dokumentace
a	a	k8xC	a
stíhání	stíhání	k1gNnSc3	stíhání
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
polskému	polský	k2eAgInSc3d1	polský
národu	národ	k1gInSc3	národ
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Ústav	ústav	k1gInSc1	ústav
národní	národní	k2eAgFnSc2d1	národní
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
dokumentace	dokumentace	k1gFnSc1	dokumentace
zločinů	zločin	k1gInPc2	zločin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
ukrajinskými	ukrajinský	k2eAgInPc7d1	ukrajinský
nacionalisty	nacionalista	k1gMnPc4	nacionalista
sociálními	sociální	k2eAgFnPc7d1	sociální
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
organizace	organizace	k1gFnSc2	organizace
Światowy	Światowa	k1gFnSc2	Światowa
Związek	Związek	k1gInSc1	Związek
Żolnierzy	Żolnierza	k1gFnSc2	Żolnierza
Armii	Armie	k1gFnSc3	Armie
Krajowej	Krajowej	k1gInSc1	Krajowej
(	(	kIx(	(
<g/>
Světový	světový	k2eAgInSc1d1	světový
svaz	svaz	k1gInSc1	svaz
vojáků	voják	k1gMnPc2	voják
zemské	zemský	k2eAgFnSc2d1	zemská
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
organizací	organizace	k1gFnSc7	organizace
Stowarzyszenie	Stowarzyszenie	k1gFnSc2	Stowarzyszenie
Upamiętnienia	Upamiętnienium	k1gNnSc2	Upamiętnienium
Ofiar	Ofiar	k1gMnSc1	Ofiar
Zbrodni	Zbrodni	k?	Zbrodni
Ukraińskich	Ukraińskich	k1gMnSc1	Ukraińskich
Nacjonalistów	Nacjonalistów	k1gMnSc1	Nacjonalistów
(	(	kIx(	(
<g/>
Asociace	asociace	k1gFnSc1	asociace
pro	pro	k7c4	pro
památku	památka	k1gFnSc4	památka
obětí	oběť	k1gFnPc2	oběť
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
ve	v	k7c6	v
Wroclawi	Wroclaw	k1gFnSc6	Wroclaw
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
čtvrtletně	čtvrtletně	k6eAd1	čtvrtletně
vydává	vydávat	k5eAaPmIp3nS	vydávat
publikaci	publikace	k1gFnSc4	publikace
Na	na	k7c4	na
Rubieży	Rubieża	k1gFnPc4	Rubieża
<g/>
,	,	kIx,	,
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
ilegálně	ilegálně	k6eAd1	ilegálně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charta	charta	k1gFnSc1	charta
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
na	na	k7c4	na
organizaci	organizace	k1gFnSc4	organizace
série	série	k1gFnSc2	série
seminářů	seminář	k1gInPc2	seminář
polských	polský	k2eAgMnPc2d1	polský
a	a	k8xC	a
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
historiků	historik	k1gMnPc2	historik
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
vztahy	vztah	k1gInPc4	vztah
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
desetidílná	desetidílný	k2eAgFnSc1d1	desetidílná
série	série	k1gFnSc1	série
"	"	kIx"	"
<g/>
Polska-Ukraina	Polska-Ukraina	k1gFnSc1	Polska-Ukraina
<g/>
.	.	kIx.	.
</s>
<s>
Trudne	Trudnout	k5eAaPmIp3nS	Trudnout
pytania	pytanium	k1gNnPc4	pytanium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Polsko-Ukrajina	Polsko-Ukrajina	k1gMnSc1	Polsko-Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Obtížné	obtížný	k2eAgFnPc1d1	obtížná
otázky	otázka	k1gFnPc1	otázka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
studie	studie	k1gFnPc1	studie
připravené	připravený	k2eAgFnPc1d1	připravená
během	během	k7c2	během
seminářů	seminář	k1gInPc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vydání	vydání	k1gNnSc6	vydání
série	série	k1gFnSc2	série
spolupracovaly	spolupracovat	k5eAaImAgFnP	spolupracovat
i	i	k9	i
Światowy	Światowa	k1gFnPc1	Światowa
Związek	Związek	k1gInSc1	Związek
Żolnierzy	Żolnierza	k1gFnSc2	Żolnierza
Armii	Armie	k1gFnSc4	Armie
Krajowej	Krajowej	k1gInSc4	Krajowej
a	a	k8xC	a
středisko	středisko	k1gNnSc4	středisko
Karta	karta	k1gFnSc1	karta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Materiální	materiální	k2eAgFnPc4d1	materiální
ztráty	ztráta	k1gFnPc4	ztráta
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnPc2	oběť
proti-polských	protiolský	k2eAgFnPc2d1	proti-polský
akcí	akce	k1gFnPc2	akce
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
na	na	k7c6	na
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
vedly	vést	k5eAaImAgInP	vést
masakry	masakr	k1gInPc1	masakr
i	i	k9	i
k	k	k7c3	k
obrovským	obrovský	k2eAgFnPc3d1	obrovská
materiálním	materiální	k2eAgFnPc3d1	materiální
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Volyně	Volyně	k1gFnSc2	Volyně
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
oblasti	oblast	k1gFnSc2	oblast
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Haliči	Halič	k1gFnSc6	Halič
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
1150	[number]	k4	1150
polských	polský	k2eAgNnPc2d1	polské
venkovských	venkovský	k2eAgNnPc2d1	venkovské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
bylo	být	k5eAaImAgNnS	být
31	[number]	k4	31
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
hospodářství	hospodářství	k1gNnSc2	hospodářství
(	(	kIx(	(
<g/>
obydlených	obydlený	k2eAgInPc2d1	obydlený
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
statků	statek	k1gInPc2	statek
<g/>
,	,	kIx,	,
farem	farma	k1gFnPc2	farma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
vojáky	voják	k1gMnPc7	voják
zničeno	zničit	k5eAaPmNgNnS	zničit
1048	[number]	k4	1048
osad	osada	k1gFnPc2	osada
a	a	k8xC	a
26	[number]	k4	26
167	[number]	k4	167
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
statků	statek	k1gInPc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
volyňských	volyňský	k2eAgInPc2d1	volyňský
252	[number]	k4	252
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
kaplí	kaple	k1gFnPc2	kaple
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
103	[number]	k4	103
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
<g/>
Už	už	k6eAd1	už
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1943	[number]	k4	1943
velitel	velitel	k1gMnSc1	velitel
vojenského	vojenský	k2eAgInSc2d1	vojenský
okruhu	okruh	k1gInSc2	okruh
UPA	UPA	kA	UPA
Turiw	Turiw	k1gMnSc1	Turiw
<g/>
,	,	kIx,	,
přikázal	přikázat	k5eAaPmAgMnS	přikázat
svým	svůj	k3xOyFgMnPc3	svůj
vojákům	voják	k1gMnPc3	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
blokády	blokáda	k1gFnPc4	blokáda
měst	město	k1gNnPc2	město
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyl	být	k5eNaImAgInS	být
chleba	chléb	k1gInSc2	chléb
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
odstraněny	odstranit	k5eAaPmNgInP	odstranit
stopy	stop	k1gInPc1	stop
polské	polský	k2eAgFnSc2d1	polská
kultury	kultura	k1gFnSc2	kultura
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
rozebírání	rozebírání	k1gNnSc2	rozebírání
nebo	nebo	k8xC	nebo
vypalování	vypalování	k1gNnSc2	vypalování
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
odstranění	odstranění	k1gNnSc1	odstranění
hmotných	hmotný	k2eAgInPc2d1	hmotný
projevů	projev	k1gInPc2	projev
Poláků	polák	k1gInPc2	polák
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
rozkazem	rozkaz	k1gInSc7	rozkaz
OUN	OUN	kA	OUN
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Masakr	masakr	k1gInSc4	masakr
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
a	a	k8xC	a
speciální	speciální	k2eAgFnPc4d1	speciální
služby	služba	k1gFnPc4	služba
SSSR	SSSR	kA	SSSR
==	==	k?	==
</s>
</p>
<p>
<s>
Obecné	obecný	k2eAgInPc1d1	obecný
poznatky	poznatek	k1gInPc1	poznatek
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
konferencí	konference	k1gFnPc2	konference
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
polských	polský	k2eAgMnPc2d1	polský
a	a	k8xC	a
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
historiků	historik	k1gMnPc2	historik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
sovětská	sovětský	k2eAgFnSc1d1	sovětská
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
(	(	kIx(	(
<g/>
NKVD	NKVD	kA	NKVD
a	a	k8xC	a
GRU	GRU	kA	GRU
<g/>
)	)	kIx)	)
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
zažehnutí	zažehnutí	k1gNnSc3	zažehnutí
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc3	rozvoj
polsko-ukrajinského	polskokrajinský	k2eAgNnSc2d1	polsko-ukrajinský
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
míra	míra	k1gFnSc1	míra
zapojení	zapojení	k1gNnSc2	zapojení
sovětských	sovětský	k2eAgFnPc2d1	sovětská
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
hypotéz	hypotéza	k1gFnPc2	hypotéza
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
ale	ale	k9	ale
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
bez	bez	k7c2	bez
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
dokumentaci	dokumentace	k1gFnSc3	dokumentace
těchto	tento	k3xDgFnPc2	tento
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
utajených	utajený	k2eAgInPc6d1	utajený
archivech	archiv	k1gInPc6	archiv
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
<g/>
Úsilí	úsilí	k1gNnSc1	úsilí
na	na	k7c4	na
posílení	posílení	k1gNnSc4	posílení
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
polsko-ukrajinských	polskokrajinský	k2eAgFnPc2d1	polsko-ukrajinská
rozmíšek	rozmíška	k1gFnPc2	rozmíška
Sověty	Sověty	k1gInPc7	Sověty
bylo	být	k5eAaImAgNnS	být
napadeno	napaden	k2eAgNnSc1d1	napadeno
Grzegorzem	Grzegorz	k1gMnSc7	Grzegorz
Hryciukem	Hryciuk	k1gMnSc7	Hryciuk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nevidí	vidět	k5eNaImIp3nS	vidět
možnost	možnost	k1gFnSc4	možnost
sovětské	sovětský	k2eAgFnSc2d1	sovětská
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
provokace	provokace	k1gFnSc2	provokace
při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
o	o	k7c4	o
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
podle	podle	k7c2	podle
nedávných	dávný	k2eNgNnPc2d1	nedávné
prohlášení	prohlášení	k1gNnPc2	prohlášení
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Grzegorza	Grzegorza	k1gFnSc1	Grzegorza
Motyki	Motyki	k1gNnSc2	Motyki
sovětské	sovětský	k2eAgFnSc2d1	sovětská
provokace	provokace	k1gFnSc2	provokace
při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
o	o	k7c4	o
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
jasně	jasně	k6eAd1	jasně
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
zdokumentovaný	zdokumentovaný	k2eAgInSc4d1	zdokumentovaný
případ	případ	k1gInSc4	případ
útoku	útok	k1gInSc2	útok
NKVD	NKVD	kA	NKVD
na	na	k7c4	na
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
polské	polský	k2eAgFnPc4d1	polská
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
napodoboval	napodobovat	k5eAaImAgInS	napodobovat
masakry	masakr	k1gInPc4	masakr
prováděné	prováděný	k2eAgFnSc2d1	prováděná
UPA	UPA	kA	UPA
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
Petr	Petr	k1gMnSc1	Petr
Hare	Hare	k1gFnSc4	Hare
<g/>
,	,	kIx,	,
z	z	k7c2	z
oddělení	oddělení	k1gNnSc2	oddělení
Ścigania	Ściganium	k1gNnSc2	Ściganium
Zbrodni	Zbrodni	k?	Zbrodni
Przeciwko	Przeciwko	k1gNnSc1	Przeciwko
Narodowi	Narodowi	k1gNnPc2	Narodowi
Polskiemu	Polskiem	k1gInSc2	Polskiem
(	(	kIx(	(
<g/>
Stíhání	stíhání	k1gNnSc1	stíhání
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
polskému	polský	k2eAgInSc3d1	polský
národu	národ	k1gInSc3	národ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozkaz	rozkaz	k1gInSc1	rozkaz
k	k	k7c3	k
vyhlazení	vyhlazení	k1gNnSc3	vyhlazení
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
OUN-UPA	OUN-UPA	k1gFnSc2	OUN-UPA
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
určujícími	určující	k2eAgInPc7d1	určující
faktory	faktor	k1gInPc7	faktor
následné	následný	k2eAgFnSc2d1	následná
genocidy	genocida	k1gFnSc2	genocida
bylo	být	k5eAaImAgNnS	být
množství	množství	k1gNnSc1	množství
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
rolníků	rolník	k1gMnPc2	rolník
zagitovaných	zagitovaný	k2eAgMnPc2d1	zagitovaný
pro	pro	k7c4	pro
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
masakrech	masakr	k1gInPc6	masakr
a	a	k8xC	a
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
Timothy	Timotha	k1gFnSc2	Timotha
Snyder	Snyder	k1gInSc1	Snyder
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
historická	historický	k2eAgFnSc1d1	historická
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k9	ale
nebyly	být	k5eNaImAgInP	být
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
vyhlazení	vyhlazení	k1gNnSc4	vyhlazení
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nacionalistické	nacionalistický	k2eAgFnSc6d1	nacionalistická
ideologii	ideologie	k1gFnSc6	ideologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
mezi	mezi	k7c4	mezi
Bělorusy	Bělorus	k1gMnPc4	Bělorus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
reflexe	reflexe	k1gFnSc1	reflexe
událostí	událost	k1gFnPc2	událost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Připomínání	připomínání	k1gNnSc3	připomínání
masakrů	masakr	k1gInPc2	masakr
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
prostředí	prostředí	k1gNnSc6	prostředí
===	===	k?	===
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
při	při	k7c6	při
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
masakru	masakr	k1gInSc2	masakr
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
pozdější	pozdní	k2eAgMnSc1d2	pozdější
premiér	premiér	k1gMnSc1	premiér
Jaroslaw	Jaroslaw	k1gMnSc1	Jaroslaw
Kaczyński	Kaczyńske	k1gFnSc4	Kaczyńske
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
před	před	k7c7	před
60	[number]	k4	60
lety	léto	k1gNnPc7	léto
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
východní	východní	k2eAgFnSc2d1	východní
Haliče	Halič	k1gFnSc2	Halič
–	–	k?	–
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
genocida	genocida	k1gFnSc1	genocida
<g/>
!	!	kIx.	!
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
genocida	genocida	k1gFnSc1	genocida
v	v	k7c6	v
nejčistší	čistý	k2eAgFnSc6d3	nejčistší
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
!	!	kIx.	!
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
genocida	genocida	k1gFnSc1	genocida
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
to	ten	k3xDgNnSc4	ten
nechce	chtít	k5eNaImIp3nS	chtít
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
to	ten	k3xDgNnSc4	ten
prostě	prostě	k9	prostě
neříká	říkat	k5eNaImIp3nS	říkat
-	-	kIx~	-
kapituluje	kapitulovat	k5eAaBmIp3nS	kapitulovat
před	před	k7c7	před
zločiny	zločin	k1gInPc7	zločin
<g/>
,	,	kIx,	,
zajistil	zajistit	k5eAaPmAgMnS	zajistit
triumf	triumf	k1gInSc4	triumf
zločinců	zločinec	k1gMnPc2	zločinec
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
některém	některý	k3yIgNnSc6	některý
způsobem	způsob	k1gInSc7	způsob
relativizována	relativizován	k2eAgFnSc1d1	relativizována
<g/>
,	,	kIx,	,
snižována	snižován	k2eAgFnSc1d1	snižována
váha	váha	k1gFnSc1	váha
<g/>
,	,	kIx,	,
není-	není-	k?	není-
li	li	k8xS	li
definován	definovat	k5eAaBmNgInS	definovat
přiměřeně	přiměřeně	k6eAd1	přiměřeně
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
genocida	genocida	k1gFnSc1	genocida
<g/>
,	,	kIx,	,
masové	masový	k2eAgNnSc1d1	masové
zabíjení	zabíjení	k1gNnSc1	zabíjení
Poláků	Polák	k1gMnPc2	Polák
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
vytvářen	vytvářit	k5eAaPmNgInS	vytvářit
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
opakování	opakování	k1gNnSc4	opakování
takových	takový	k3xDgFnPc2	takový
událostí	událost	k1gFnPc2	událost
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
při	při	k7c6	při
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
masakru	masakr	k1gInSc2	masakr
v	v	k7c6	v
Porycku	Poryck	k1gInSc6	Poryck
<g/>
,	,	kIx,	,
prezidenti	prezident	k1gMnPc1	prezident
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
Aleksander	Aleksander	k1gInSc1	Aleksander
Kwaśniewski	Kwaśniewsk	k1gFnSc2	Kwaśniewsk
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
Leonid	Leonid	k1gInSc1	Leonid
Kučma	kučma	k1gFnSc1	kučma
<g/>
)	)	kIx)	)
odhalili	odhalit	k5eAaPmAgMnP	odhalit
památník	památník	k1gInSc4	památník
obětem	oběť	k1gFnPc3	oběť
v	v	k7c6	v
Porycku	Porycko	k1gNnSc6	Porycko
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Aleksander	Aleksander	k1gMnSc1	Aleksander
Kwasniewski	Kwasniewske	k1gFnSc4	Kwasniewske
tehdy	tehdy	k6eAd1	tehdy
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Musí	muset	k5eAaImIp3nS	muset
zde	zde	k6eAd1	zde
být	být	k5eAaImF	být
vyjádřen	vyjádřen	k2eAgInSc4d1	vyjádřen
morální	morální	k2eAgInSc4d1	morální
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
ideologii	ideologie	k1gFnSc3	ideologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
'	'	kIx"	'
<g/>
protipolské	protipolský	k2eAgFnSc3d1	protipolská
akci	akce	k1gFnSc3	akce
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
iniciovala	iniciovat	k5eAaBmAgFnS	iniciovat
část	část	k1gFnSc1	část
Organizace	organizace	k1gFnSc2	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
a	a	k8xC	a
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
mohou	moct	k5eAaImIp3nP	moct
bolet	bolet	k5eAaImF	bolet
hodně	hodně	k6eAd1	hodně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgInSc4	žádný
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
hrdá	hrdat	k5eAaImIp3nS	hrdat
jako	jako	k9	jako
svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
suverenita	suverenita	k1gFnSc1	suverenita
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
odůvodnit	odůvodnit	k5eAaPmF	odůvodnit
genocidu	genocida	k1gFnSc4	genocida
<g/>
,	,	kIx,	,
masakry	masakr	k1gInPc1	masakr
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc4	násilí
a	a	k8xC	a
znásilnění	znásilnění	k1gNnSc4	znásilnění
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgInSc4d1	způsobující
sousedům	soused	k1gMnPc3	soused
kruté	krutý	k2eAgNnSc4d1	kruté
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
odhalen	odhalit	k5eAaPmNgMnS	odhalit
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Rakowice	Rakowice	k1gFnSc2	Rakowice
památník	památník	k1gInSc1	památník
obětem	oběť	k1gFnPc3	oběť
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
genocidy	genocida	k1gFnSc2	genocida
proforem	profor	k1gMnSc7	profor
Akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
Jana	Jan	k1gMnSc2	Jan
Matejky	Matejka	k1gFnSc2	Matejka
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
Czeslawem	Czeslaw	k1gMnSc7	Czeslaw
Dźwigajem	Dźwigaj	k1gMnSc7	Dźwigaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
–	–	k?	–
návrh	návrh	k1gInSc1	návrh
usnesení	usnesení	k1gNnSc2	usnesení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
masakr	masakr	k1gInSc1	masakr
Poláků	Polák	k1gMnPc2	Polák
Ukrajinci	Ukrajinec	k1gMnSc3	Ukrajinec
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
v	v	k7c6	v
letech	let	k1gInPc6	let
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
genocidu	genocida	k1gFnSc4	genocida
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
vypracován	vypracovat	k5eAaPmNgInS	vypracovat
čtyřmi	čtyři	k4xCgMnPc7	čtyři
poslanci	poslanec	k1gMnPc7	poslanec
zvolenými	zvolený	k2eAgMnPc7d1	zvolený
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Ligy	liga	k1gFnSc2	liga
polských	polský	k2eAgFnPc2d1	polská
rodin	rodina	k1gFnPc2	rodina
[	[	kIx(	[
<g/>
111	[number]	k4	111
<g/>
]	]	kIx)	]
a	a	k8xC	a
předložen	předložit	k5eAaPmNgInS	předložit
všem	všecek	k3xTgFnPc3	všecek
delegacím	delegace	k1gFnPc3	delegace
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
usnesení	usnesení	k1gNnSc2	usnesení
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
odhlasován	odhlasovat	k5eAaPmNgInS	odhlasovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
konference	konference	k1gFnSc2	konference
<g/>
:	:	kIx,	:
Polska-Ukraina	Polska-Ukraina	k1gFnSc1	Polska-Ukraina
przyjaźń	przyjaźń	k?	przyjaźń
i	i	k9	i
partnerstwo	partnerstwo	k1gMnSc1	partnerstwo
(	(	kIx(	(
<g/>
OUN-UPA	OUN-UPA	k1gMnSc1	OUN-UPA
hańba	hańba	k1gMnSc1	hańba
i	i	k8xC	i
potępienie	potępienie	k1gFnSc1	potępienie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Polsko-Ukrajina	Polsko-Ukrajin	k2eAgNnSc2d1	Polsko-Ukrajin
přátelství	přátelství	k1gNnSc2	přátelství
a	a	k8xC	a
partnerství	partnerství	k1gNnSc2	partnerství
(	(	kIx(	(
<g/>
OUN-UPA	OUN-UPA	k1gFnSc1	OUN-UPA
hanba	hanba	k1gFnSc1	hanba
a	a	k8xC	a
odsouzení	odsouzení	k1gNnSc1	odsouzení
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
,	,	kIx,	,
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
Porczyński	Porczyńsk	k1gFnSc2	Porczyńsk
<g/>
,	,	kIx,	,
konference	konference	k1gFnSc1	konference
k	k	k7c3	k
65	[number]	k4	65
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vyhlazení	vyhlazení	k1gNnSc2	vyhlazení
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
pohraničí	pohraničí	k1gNnSc6	pohraničí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
Dolnoslezského	dolnoslezský	k2eAgNnSc2d1	Dolnoslezské
vojvodství	vojvodství	k1gNnSc2	vojvodství
si	se	k3xPyFc3	se
připomnělo	připomnět	k5eAaPmAgNnS	připomnět
65	[number]	k4	65
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
genocidy	genocida	k1gFnSc2	genocida
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
-	-	kIx~	-
Kresy	Kres	k1gInPc1	Kres
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgNnSc1d1	jihovýchodní
pohraničí	pohraničí	k1gNnSc1	pohraničí
polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
usnesením	usnesení	k1gNnSc7	usnesení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
odloženo	odložen	k2eAgNnSc1d1	odloženo
vybudování	vybudování	k1gNnSc1	vybudování
a	a	k8xC	a
odhalení	odhalení	k1gNnSc1	odhalení
pomníku	pomník	k1gInSc2	pomník
genocidy	genocida	k1gFnSc2	genocida
provedené	provedený	k2eAgFnSc2d1	provedená
OUN-UPA	OUN-UPA	k1gFnSc2	OUN-UPA
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
zřízení	zřízení	k1gNnSc1	zřízení
pomníku	pomník	k1gInSc2	pomník
na	na	k7c4	na
Plac	plac	k1gInSc4	plac
Grzybowski	Grzybowsk	k1gFnSc2	Grzybowsk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
formě	forma	k1gFnSc3	forma
(	(	kIx(	(
<g/>
pětimetrový	pětimetrový	k2eAgInSc1d1	pětimetrový
strom	strom	k1gInSc1	strom
větvemi	větev	k1gFnPc7	větev
napodobující	napodobující	k2eAgNnPc4d1	napodobující
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
mrtvolami	mrtvola	k1gFnPc7	mrtvola
dětí	dítě	k1gFnPc2	dítě
přibitými	přibitý	k2eAgFnPc7d1	přibitá
ke	k	k7c3	k
kmeni	kmen	k1gInSc3	kmen
<g/>
)	)	kIx)	)
protestovala	protestovat	k5eAaBmAgFnS	protestovat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Agnieszka	Agnieszka	k1gFnSc1	Agnieszka
Hollandová	Hollandový	k2eAgFnSc1d1	Hollandová
a	a	k8xC	a
Bronisław	Bronisław	k1gFnSc1	Bronisław
Geremek	Geremka	k1gFnPc2	Geremka
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc4	pomník
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
činů	čin	k1gInPc2	čin
UPA	UPA	kA	UPA
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Tarnopol	Tarnopol	k1gInSc1	Tarnopol
"	"	kIx"	"
<g/>
věnce	věnec	k1gInPc1	věnec
<g/>
"	"	kIx"	"
ze	z	k7c2	z
zabitých	zabitý	k2eAgFnPc2d1	zabitá
dětí	dítě	k1gFnPc2	dítě
omotány	omotat	k5eAaPmNgInP	omotat
kolem	kolem	k7c2	kolem
stromů	strom	k1gInPc2	strom
u	u	k7c2	u
cesty	cesta	k1gFnSc2	cesta
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Cesta	cesta	k1gFnSc1	cesta
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
k	k	k7c3	k
nezávislosti	nezávislost	k1gFnSc3	nezávislost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgInSc1d1	polský
Sejm	Sejm	k1gInSc1	Sejm
přijal	přijmout	k5eAaPmAgInS	přijmout
aklamací	aklamace	k1gFnSc7	aklamace
usnesení	usnesení	k1gNnSc4	usnesení
O	o	k7c6	o
tragickém	tragický	k2eAgInSc6d1	tragický
osudu	osud	k1gInSc6	osud
Poláků	Polák	k1gMnPc2	Polák
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
pohraničí	pohraničí	k1gNnSc6	pohraničí
k	k	k7c3	k
66	[number]	k4	66
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
zahájení	zahájení	k1gNnSc2	zahájení
hromadných	hromadný	k2eAgFnPc2d1	hromadná
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
etnických	etnický	k2eAgFnPc2d1	etnická
čistek	čistka	k1gFnPc2	čistka
a	a	k8xC	a
genocidy	genocida	k1gFnSc2	genocida
Organizací	organizace	k1gFnPc2	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
a	a	k8xC	a
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
Opolského	opolský	k2eAgNnSc2d1	Opolské
vojvodství	vojvodství	k1gNnSc2	vojvodství
uctilo	uctít	k5eAaPmAgNnS	uctít
památku	památka	k1gFnSc4	památka
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
občanů	občan	k1gMnPc2	občan
druhé	druhý	k4xOgFnSc2	druhý
polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
zavražděných	zavražděný	k2eAgFnPc2d1	zavražděná
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vzdali	vzdát	k5eAaPmAgMnP	vzdát
hold	hold	k1gInSc4	hold
příslušníkům	příslušník	k1gMnPc3	příslušník
jednotek	jednotka	k1gFnPc2	jednotka
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
rolnickým	rolnický	k2eAgInPc3d1	rolnický
praporům	prapor	k1gInPc3	prapor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vedli	vést	k5eAaImAgMnP	vést
hrdinný	hrdinný	k2eAgInSc4d1	hrdinný
boj	boj	k1gInSc4	boj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
polských	polský	k2eAgMnPc2d1	polský
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
Podkarpatského	podkarpatský	k2eAgNnSc2d1	Podkarpatské
vojvodství	vojvodství	k1gNnSc2	vojvodství
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
hold	hold	k1gInSc1	hold
polským	polský	k2eAgMnPc3d1	polský
občanům	občan	k1gMnPc3	občan
a	a	k8xC	a
občanům	občan	k1gMnPc3	občan
druhé	druhý	k4xOgFnSc6	druhý
republiky	republika	k1gFnSc2	republika
zavražděným	zavražděný	k2eAgInPc3d1	zavražděný
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
a	a	k8xC	a
odsoudilo	odsoudit	k5eAaPmAgNnS	odsoudit
zločiny	zločin	k1gMnPc4	zločin
OUN-UPA	OUN-UPA	k1gFnPc2	OUN-UPA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Malopolské	malopolský	k2eAgNnSc1d1	Malopolské
regionální	regionální	k2eAgNnSc1d1	regionální
shromáždění	shromáždění	k1gNnSc1	shromáždění
ve	v	k7c6	v
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
usnesení	usnesení	k1gNnSc6	usnesení
uctilo	uctít	k5eAaPmAgNnS	uctít
památku	památka	k1gFnSc4	památka
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
občanů	občan	k1gMnPc2	občan
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
zavražděných	zavražděný	k2eAgFnPc2d1	zavražděná
OUN	OUN	kA	OUN
a	a	k8xC	a
UPA	UPA	kA	UPA
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
rada	rada	k1gFnSc1	rada
rovněž	rovněž	k9	rovněž
poděkovala	poděkovat	k5eAaPmAgFnS	poděkovat
Ukrajincům	Ukrajinec	k1gMnPc3	Ukrajinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pomohli	pomoct	k5eAaPmAgMnP	pomoct
polským	polský	k2eAgMnPc3d1	polský
sousedům	soused	k1gMnPc3	soused
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vzdala	vzdát	k5eAaPmAgFnS	vzdát
hold	hold	k1gInSc4	hold
ozbrojeným	ozbrojený	k2eAgFnPc3d1	ozbrojená
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
působily	působit	k5eAaImAgFnP	působit
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
Lubušského	Lubušský	k2eAgNnSc2d1	Lubušský
vojvodství	vojvodství	k1gNnSc2	vojvodství
uctilo	uctít	k5eAaPmAgNnS	uctít
památku	památka	k1gFnSc4	památka
obětí	oběť	k1gFnPc2	oběť
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
členy	člen	k1gMnPc4	člen
Organizace	organizace	k1gFnSc2	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
a	a	k8xC	a
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
a	a	k8xC	a
apelovalo	apelovat	k5eAaImAgNnS	apelovat
na	na	k7c4	na
polské	polský	k2eAgInPc4d1	polský
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přijalo	přijmout	k5eAaPmAgNnS	přijmout
usnesení	usnesení	k1gNnSc1	usnesení
o	o	k7c6	o
ustanovení	ustanovení	k1gNnSc6	ustanovení
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
dnem	dnem	k7c2	dnem
památky	památka	k1gFnSc2	památka
obětí	oběť	k1gFnPc2	oběť
genocidy	genocida	k1gFnSc2	genocida
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pohraničí	pohraničí	k1gNnSc6	pohraničí
druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
polský	polský	k2eAgInSc1d1	polský
biskup	biskup	k1gInSc1	biskup
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
lvovský	lvovský	k2eAgMnSc1d1	lvovský
Mieczyslaw	Mieczyslaw	k1gMnSc1	Mieczyslaw
Mokrzycki	Mokrzycki	k1gNnSc2	Mokrzycki
na	na	k7c4	na
361	[number]	k4	361
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
Konference	konference	k1gFnSc2	konference
polského	polský	k2eAgInSc2d1	polský
episkopátu	episkopát	k1gInSc2	episkopát
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
informoval	informovat	k5eAaBmAgInS	informovat
ostatní	ostatní	k2eAgInPc4d1	ostatní
polské	polský	k2eAgInPc4d1	polský
biskupy	biskup	k1gInPc4	biskup
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvažovaný	uvažovaný	k2eAgInSc1d1	uvažovaný
společný	společný	k2eAgInSc1d1	společný
Pastýřský	pastýřský	k2eAgInSc1d1	pastýřský
list	list	k1gInSc1	list
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
řeckokatolické	řeckokatolický	k2eAgFnSc2d1	řeckokatolická
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
masového	masový	k2eAgNnSc2d1	masové
vraždění	vraždění	k1gNnSc2	vraždění
polských	polský	k2eAgMnPc2d1	polský
civilistů	civilista	k1gMnPc2	civilista
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Haliči	Halič	k1gFnSc6	Halič
nebude	být	k5eNaImBp3nS	být
vytvořen	vytvořen	k2eAgMnSc1d1	vytvořen
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nepřijatelných	přijatelný	k2eNgInPc2d1	nepřijatelný
požadavků	požadavek	k1gInPc2	požadavek
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
seznam	seznam	k1gInSc1	seznam
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
tvrzení	tvrzení	k1gNnPc4	tvrzení
o	o	k7c4	o
zbavení	zbavení	k1gNnSc4	zbavení
práv	právo	k1gNnPc2	právo
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
"	"	kIx"	"
<g/>
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
vlastní	vlastní	k2eAgFnSc6d1	vlastní
půdě	půda	k1gFnSc6	půda
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
bratrovražedné	bratrovražedný	k2eAgFnSc6d1	bratrovražedná
válce	válka	k1gFnSc6	válka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Mokrzyckého	Mokrzycký	k1gMnSc2	Mokrzycký
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
vytvoření	vytvoření	k1gNnSc3	vytvoření
dojmu	dojem	k1gInSc2	dojem
falešné	falešný	k2eAgFnSc3d1	falešná
spoluzodpovědnosti	spoluzodpovědnost	k1gFnSc3	spoluzodpovědnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Polští	polský	k2eAgMnPc1d1	polský
biskupové	biskup	k1gMnPc1	biskup
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
sdělili	sdělit	k5eAaPmAgMnP	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
biskupové	biskup	k1gMnPc1	biskup
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
příčinách	příčina	k1gFnPc6	příčina
masakru	masakr	k1gInSc2	masakr
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
vliv	vliv	k1gInSc4	vliv
činnosti	činnost	k1gFnSc2	činnost
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
populární	populární	k2eAgFnSc1d1	populární
nacionalistická	nacionalistický	k2eAgFnSc1d1	nacionalistická
linie	linie	k1gFnSc1	linie
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c6	na
relativizaci	relativizace	k1gFnSc6	relativizace
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
popírání	popírání	k1gNnSc3	popírání
existence	existence	k1gFnSc2	existence
genocidy	genocida	k1gFnSc2	genocida
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
(	(	kIx(	(
<g/>
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
ozn	ozn	k?	ozn
<g/>
.	.	kIx.	.
za	za	k7c4	za
pouhé	pouhý	k2eAgNnSc4d1	pouhé
"	"	kIx"	"
<g/>
vysídlení	vysídlení	k1gNnSc4	vysídlení
<g/>
"	"	kIx"	"
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
a	a	k8xC	a
negování	negování	k1gNnSc1	negování
vlivu	vliv	k1gInSc2	vliv
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
integrálního	integrální	k2eAgInSc2d1	integrální
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
řeckokatolických	řeckokatolický	k2eAgMnPc2d1	řeckokatolický
biskupů	biskup	k1gMnPc2	biskup
neuváděl	uvádět	k5eNaImAgMnS	uvádět
pachatele	pachatel	k1gMnSc4	pachatel
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhlazení	vyhlazení	k1gNnSc1	vyhlazení
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
způsobily	způsobit	k5eAaPmAgInP	způsobit
"	"	kIx"	"
<g/>
neosobní	osobní	k2eNgInPc1d1	neosobní
síly	síl	k1gInPc1	síl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
rozruch	rozruch	k1gInSc4	rozruch
mezi	mezi	k7c7	mezi
polskými	polský	k2eAgMnPc7d1	polský
biskupy	biskup	k1gMnPc7	biskup
také	také	k9	také
vyvolávaly	vyvolávat	k5eAaImAgFnP	vyvolávat
navržené	navržený	k2eAgFnPc1d1	navržená
formulace	formulace	k1gFnPc1	formulace
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
určené	určený	k2eAgFnSc6d1	určená
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
<g/>
,	,	kIx,	,
že	že	k8xS	že
Poláci	Polák	k1gMnPc1	Polák
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
"	"	kIx"	"
<g/>
odpouštět	odpouštět	k5eAaImF	odpouštět
a	a	k8xC	a
prosit	prosit	k5eAaImF	prosit
o	o	k7c6	o
odpuštění	odpuštění	k1gNnSc6	odpuštění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
návrhy	návrh	k1gInPc4	návrh
usnesení	usnesení	k1gNnSc2	usnesení
předložené	předložený	k2eAgInPc1d1	předložený
kluby	klub	k1gInPc1	klub
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
Sejmu	Sejm	k1gInSc6	Sejm
k	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
masakru	masakr	k1gInSc2	masakr
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
,	,	kIx,	,
Lvovskou	lvovský	k2eAgFnSc7d1	Lvovská
radou	rada	k1gFnSc7	rada
a	a	k8xC	a
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
aktivisty	aktivista	k1gMnPc7	aktivista
různých	různý	k2eAgFnPc2d1	různá
společenských	společenský	k2eAgFnPc2d1	společenská
organizací	organizace	k1gFnPc2	organizace
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
setkání	setkání	k1gNnSc1	setkání
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
setkání	setkání	k1gNnSc2	setkání
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
promluvil	promluvit	k5eAaPmAgMnS	promluvit
člen	člen	k1gMnSc1	člen
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
rady	rada	k1gFnSc2	rada
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
Anatolij	Anatolij	k1gFnSc2	Anatolij
Vitiv	Vitiva	k1gFnPc2	Vitiva
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
jsou	být	k5eAaImIp3nP	být
oběťmi	oběť	k1gFnPc7	oběť
polské	polský	k2eAgFnSc2d1	polská
okupace	okupace	k1gFnSc2	okupace
a	a	k8xC	a
nebudou	být	k5eNaImBp3nP	být
Poláky	Polák	k1gMnPc4	Polák
žádat	žádat	k5eAaImF	žádat
o	o	k7c6	o
odpuštění	odpuštění	k1gNnSc6	odpuštění
<g/>
.	.	kIx.	.
</s>
<s>
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Sereda	Sereda	k1gMnSc1	Sereda
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
postoj	postoj	k1gInSc4	postoj
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
používání	používání	k1gNnSc2	používání
termínu	termín	k1gInSc2	termín
"	"	kIx"	"
<g/>
genocida	genocida	k1gFnSc1	genocida
<g/>
"	"	kIx"	"
polskou	polský	k2eAgFnSc7d1	polská
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
událostmi	událost	k1gFnPc7	událost
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Lvovské	lvovský	k2eAgFnSc2d1	Lvovská
Regionální	regionální	k2eAgFnSc2d1	regionální
rady	rada	k1gFnSc2	rada
Oleh	Oleh	k1gMnSc1	Oleh
Paňkevyč	Paňkevyč	k1gMnSc1	Paňkevyč
<g/>
,	,	kIx,	,
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
"	"	kIx"	"
<g/>
předložení	předložení	k1gNnSc3	předložení
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
svědčícím	svědčící	k2eAgFnPc3d1	svědčící
že	že	k8xS	že
kterýkoliv	kterýkoliv	k3yIgInSc1	kterýkoliv
evropský	evropský	k2eAgInSc1d1	evropský
národ	národ	k1gInSc1	národ
je	být	k5eAaImIp3nS	být
civilizovaný	civilizovaný	k2eAgInSc1d1	civilizovaný
<g/>
"	"	kIx"	"
a	a	k8xC	a
vybíral	vybírat	k5eAaImAgMnS	vybírat
peníze	peníz	k1gInPc4	peníz
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
"	"	kIx"	"
<g/>
odvety	odveta	k1gFnPc4	odveta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
shromážděnými	shromážděný	k2eAgMnPc7d1	shromážděný
zaznívaly	zaznívat	k5eAaImAgFnP	zaznívat
hlasy	hlas	k1gInPc1	hlas
tvrdící	tvrdící	k2eAgInPc1d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
více	hodně	k6eAd2	hodně
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
rukama	ruka	k1gFnPc7	ruka
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
lvovské	lvovský	k2eAgInPc1d1	lvovský
úřady	úřad	k1gInPc1	úřad
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
připravit	připravit	k5eAaPmF	připravit
ultimátum	ultimátum	k1gNnSc1	ultimátum
polské	polský	k2eAgNnSc1d1	polské
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
Polský	polský	k2eAgInSc1d1	polský
Sejm	Sejm	k1gInSc1	Sejm
přijal	přijmout	k5eAaPmAgInS	přijmout
usnesení	usnesení	k1gNnSc4	usnesení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
uznává	uznávat	k5eAaImIp3nS	uznávat
zločiny	zločin	k1gInPc4	zločin
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
za	za	k7c4	za
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
znaky	znak	k1gInPc4	znak
genocidy	genocida	k1gFnSc2	genocida
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dříve	dříve	k6eAd2	dříve
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
novelu	novela	k1gFnSc4	novela
strany	strana	k1gFnSc2	strana
Právo	právo	k1gNnSc4	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nazývala	nazývat	k5eAaImAgFnS	nazývat
tyto	tento	k3xDgInPc4	tento
zločiny	zločin	k1gInPc4	zločin
termínem	termín	k1gInSc7	termín
genocida	genocida	k1gFnSc1	genocida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
v	v	k7c6	v
Lucku	Lucko	k1gNnSc6	Lucko
k	k	k7c3	k
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
oslavy	oslava	k1gFnPc1	oslava
konaly	konat	k5eAaImAgFnP	konat
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
obětí	oběť	k1gFnPc2	oběť
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
prezidenta	prezident	k1gMnSc2	prezident
Polska	Polsko	k1gNnSc2	Polsko
Bronislawa	Bronislawus	k1gMnSc2	Bronislawus
Komorowského	Komorowský	k1gMnSc2	Komorowský
<g/>
,	,	kIx,	,
náměstka	náměstek	k1gMnSc2	náměstek
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
Kostyantyn	Kostyantyna	k1gFnPc2	Kostyantyna
Hryszczenki	Hryszczenk	k1gFnSc2	Hryszczenk
a	a	k8xC	a
lvovského	lvovský	k2eAgMnSc2d1	lvovský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Mieczyslawa	Mieczyslawus	k1gMnSc2	Mieczyslawus
Mokrzyckého	Mokrzycký	k1gMnSc2	Mokrzycký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
během	během	k7c2	během
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
věřícími	věřící	k1gFnPc7	věřící
při	při	k7c6	při
modlitbě	modlitba	k1gFnSc6	modlitba
Anděl	Anděla	k1gFnPc2	Anděla
Páně	páně	k2eAgFnPc1d1	páně
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
František	František	k1gMnSc1	František
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
ke	k	k7c3	k
společné	společný	k2eAgFnSc3d1	společná
modlitbě	modlitba	k1gFnSc3	modlitba
s	s	k7c7	s
duchovními	duchovní	k1gMnPc7	duchovní
a	a	k8xC	a
věřícími	věřící	k1gMnPc7	věřící
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
v	v	k7c6	v
Lucku	Lucko	k1gNnSc6	Lucko
<g/>
,	,	kIx,	,
ke	k	k7c3	k
mši	mše	k1gFnSc3	mše
za	za	k7c4	za
duše	duše	k1gFnPc4	duše
obětí	oběť	k1gFnPc2	oběť
masakru	masakr	k1gInSc3	masakr
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
si	se	k3xPyFc3	se
připomínáme	připomínat	k5eAaImIp1nP	připomínat
<g/>
...	...	k?	...
...	...	k?	...
Tyto	tento	k3xDgInPc1	tento
akty	akt	k1gInPc1	akt
<g/>
,	,	kIx,	,
podmíněné	podmíněný	k2eAgNnSc1d1	podmíněné
nacionalistickou	nacionalistický	k2eAgFnSc7d1	nacionalistická
ideologií	ideologie	k1gFnSc7	ideologie
v	v	k7c6	v
tragické	tragický	k2eAgFnSc6d1	tragická
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
desítky	desítka	k1gFnSc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
zraněné	zraněný	k2eAgNnSc4d1	zraněné
bratrství	bratrství	k1gNnSc4	bratrství
dvou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
polského	polský	k2eAgMnSc2d1	polský
a	a	k8xC	a
ukrajinského	ukrajinský	k2eAgMnSc2d1	ukrajinský
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Popírání	popírání	k1gNnPc4	popírání
zodpovědnosti	zodpovědnost	k1gFnSc2	zodpovědnost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1943	[number]	k4	1943
vedení	vedení	k1gNnSc4	vedení
OUN-B	OUN-B	k1gFnSc2	OUN-B
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
masakrem	masakr	k1gInSc7	masakr
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
,	,	kIx,	,
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
ani	ani	k8xC	ani
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Organizace	organizace	k1gFnSc1	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
<g/>
"	"	kIx"	"
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
hromadnými	hromadný	k2eAgInPc7d1	hromadný
zločiny	zločin	k1gInPc7	zločin
nic	nic	k3yNnSc4	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
"	"	kIx"	"
<g/>
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
na	na	k7c6	na
ukrajinském	ukrajinský	k2eAgNnSc6d1	ukrajinské
území	území	k1gNnSc6	území
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
"	"	kIx"	"
<g/>
jatkách	jatka	k1gFnPc6	jatka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
osoby	osoba	k1gFnPc1	osoba
se	s	k7c7	s
zahraničními	zahraniční	k2eAgInPc7d1	zahraniční
zájmy	zájem	k1gInPc7	zájem
-	-	kIx~	-
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
sovětské	sovětský	k2eAgFnSc2d1	sovětská
<g/>
.	.	kIx.	.
</s>
<s>
Vinu	vinout	k5eAaImIp1nS	vinout
za	za	k7c4	za
vypuknutí	vypuknutí	k1gNnSc4	vypuknutí
událostí	událost	k1gFnPc2	událost
proti	proti	k7c3	proti
Polákům	Polák	k1gMnPc3	Polák
svalují	svalovat	k5eAaImIp3nP	svalovat
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
příčiny	příčina	k1gFnPc4	příčina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
napjaté	napjatý	k2eAgInPc1d1	napjatý
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Ukrajinci	Ukrajinec	k1gMnPc7	Ukrajinec
a	a	k8xC	a
Poláky	Polák	k1gMnPc7	Polák
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
národní	národní	k2eAgFnSc2d1	národní
politiky	politika	k1gFnSc2	politika
druhé	druhý	k4xOgFnSc2	druhý
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
utlačování	utlačování	k1gNnSc1	utlačování
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
"	"	kIx"	"
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Poláků	Polák	k1gMnPc2	Polák
v	v	k7c6	v
Chelmně	Chelmně	k1gFnSc6	Chelmně
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
podpora	podpora	k1gFnSc1	podpora
polských	polský	k2eAgFnPc2d1	polská
a	a	k8xC	a
německých	německý	k2eAgFnPc2d1	německá
okupačních	okupační	k2eAgFnPc2d1	okupační
sil	síla	k1gFnPc2	síla
sovětskými	sovětský	k2eAgMnPc7d1	sovětský
partyzány	partyzán	k1gMnPc7	partyzán
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
systematické	systematický	k2eAgInPc1d1	systematický
masakry	masakr	k1gInPc1	masakr
<g/>
"	"	kIx"	"
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k6eAd1	jak
píše	psát	k5eAaImIp3nS	psát
Krzysztof	Krzysztof	k1gInSc4	Krzysztof
Lada	Lada	k1gFnSc1	Lada
<g/>
,	,	kIx,	,
předložené	předložený	k2eAgFnPc1d1	předložená
interpretace	interpretace	k1gFnPc1	interpretace
OUN-B	OUN-B	k1gMnPc2	OUN-B
jsou	být	k5eAaImIp3nP	být
výmluvy	výmluva	k1gFnPc4	výmluva
ohledně	ohledně	k7c2	ohledně
Volyňského	volyňský	k2eAgInSc2d1	volyňský
masakru	masakr	k1gInSc2	masakr
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
uvedeny	uveden	k2eAgFnPc1d1	uvedena
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
publikacích	publikace	k1gFnPc6	publikace
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
bylo	být	k5eAaImAgNnS	být
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
poměrem	poměr	k1gInSc7	poměr
národnosti	národnost	k1gFnSc2	národnost
vůči	vůči	k7c3	vůči
Polákům	Polák	k1gMnPc3	Polák
<g/>
,	,	kIx,	,
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
politikou	politika	k1gFnSc7	politika
polské	polský	k2eAgFnSc2d1	polská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
k	k	k7c3	k
podobné	podobný	k2eAgFnSc3d1	podobná
situaci	situace	k1gFnSc3	situace
nedošlo	dojít	k5eNaPmAgNnS	dojít
(	(	kIx(	(
<g/>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brutalita	brutalita	k1gFnSc1	brutalita
a	a	k8xC	a
rozsah	rozsah	k1gInSc1	rozsah
masakrů	masakr	k1gInPc2	masakr
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
v	v	k7c6	v
období	období	k1gNnSc6	období
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
prováděná	prováděný	k2eAgFnSc1d1	prováděná
OUN	OUN	kA	OUN
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jednotek	jednotka	k1gFnPc2	jednotka
UPA	UPA	kA	UPA
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
dějinách	dějiny	k1gFnPc6	dějiny
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
obdoby	obdoba	k1gFnSc2	obdoba
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Očistit	očistit	k5eAaPmF	očistit
pověst	pověst	k1gFnSc4	pověst
ukrajinských	ukrajinský	k2eAgFnPc2d1	ukrajinská
nacionalistických	nacionalistický	k2eAgFnPc2d1	nacionalistická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
stály	stát	k5eAaImAgFnP	stát
za	za	k7c7	za
Volyňským	volyňský	k2eAgInSc7d1	volyňský
masakrem	masakr	k1gInSc7	masakr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
prezidenti	prezident	k1gMnPc1	prezident
Viktor	Viktor	k1gMnSc1	Viktor
Juščenko	Juščenka	k1gFnSc5	Juščenka
i	i	k9	i
Petro	Petra	k1gFnSc5	Petra
Porošenko	Porošenka	k1gFnSc5	Porošenka
<g/>
.	.	kIx.	.
<g/>
Polský	polský	k2eAgMnSc1d1	polský
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
místopředseda	místopředseda	k1gMnSc1	místopředseda
vládní	vládní	k2eAgFnSc2d1	vládní
strany	strana	k1gFnSc2	strana
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
(	(	kIx(	(
<g/>
PiS	Pisa	k1gFnPc2	Pisa
<g/>
)	)	kIx)	)
Antoni	Anton	k1gMnPc1	Anton
Macierewicz	Macierewicza	k1gFnPc2	Macierewicza
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
Volyňský	volyňský	k2eAgInSc4d1	volyňský
masakr	masakr	k1gInSc4	masakr
nese	nést	k5eAaImIp3nS	nést
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvrzení	tvrzení	k1gNnSc3	tvrzení
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
ruská	ruský	k2eAgFnSc1d1	ruská
vláda	vláda	k1gFnSc1	vláda
i	i	k8xC	i
Federace	federace	k1gFnSc1	federace
židovských	židovský	k2eAgFnPc2d1	židovská
obcí	obec	k1gFnPc2	obec
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanovisko	stanovisko	k1gNnSc1	stanovisko
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
historiků	historik	k1gMnPc2	historik
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
působení	působení	k1gNnSc2	působení
sovětských	sovětský	k2eAgMnPc2d1	sovětský
historiků	historik	k1gMnPc2	historik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
v	v	k7c6	v
Ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
provádět	provádět	k5eAaImF	provádět
objektivní	objektivní	k2eAgFnSc4d1	objektivní
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
činnost	činnost	k1gFnSc4	činnost
ohledně	ohledně	k7c2	ohledně
akcí	akce	k1gFnPc2	akce
UPA	UPA	kA	UPA
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
její	její	k3xOp3gFnSc2	její
účasti	účast	k1gFnSc2	účast
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Třetí	třetí	k4xOgFnSc3	třetí
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
činnost	činnost	k1gFnSc4	činnost
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Ihora	Ihor	k1gMnSc2	Ihor
Iljušina	Iljušin	k2eAgMnSc2d1	Iljušin
<g/>
,	,	kIx,	,
historiografa	historiograf	k1gMnSc2	historiograf
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zděděn	zdědit	k5eAaPmNgInS	zdědit
oboustranně	oboustranně	k6eAd1	oboustranně
zkreslený	zkreslený	k2eAgInSc4d1	zkreslený
obraz	obraz	k1gInSc4	obraz
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
konalo	konat	k5eAaImAgNnS	konat
zasedání	zasedání	k1gNnSc1	zasedání
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kulatý	kulatý	k2eAgInSc1d1	kulatý
stůl	stůl	k1gInSc1	stůl
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
svedlo	svést	k5eAaPmAgNnS	svést
dohromady	dohromady	k6eAd1	dohromady
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
ukrajinské	ukrajinský	k2eAgMnPc4d1	ukrajinský
historiky	historik	k1gMnPc4	historik
a	a	k8xC	a
sociální	sociální	k2eAgMnPc4d1	sociální
aktivisty	aktivista	k1gMnPc4	aktivista
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednání	jednání	k1gNnSc2	jednání
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
schůzi	schůze	k1gFnSc6	schůze
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavními	hlavní	k2eAgMnPc7d1	hlavní
viníky	viník	k1gMnPc7	viník
ukrajinsko-polské	ukrajinskoolský	k2eAgFnSc2d1	ukrajinsko-polský
války	válka	k1gFnSc2	válka
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
předválečná	předválečný	k2eAgFnSc1d1	předválečná
polská	polský	k2eAgFnSc1d1	polská
totalitní	totalitní	k2eAgFnSc1d1	totalitní
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
používala	používat	k5eAaImAgFnS	používat
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
proti	proti	k7c3	proti
Ukrajincům	Ukrajinec	k1gMnPc3	Ukrajinec
a	a	k8xC	a
na	na	k7c6	na
okupovaných	okupovaný	k2eAgNnPc6d1	okupované
územích	území	k1gNnPc6	území
prováděla	provádět	k5eAaImAgFnS	provádět
kolonizační	kolonizační	k2eAgFnSc4d1	kolonizační
politiku	politika	k1gFnSc4	politika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
"	"	kIx"	"
<g/>
negativní	negativní	k2eAgFnSc1d1	negativní
role	role	k1gFnSc1	role
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sehrála	sehrát	k5eAaPmAgFnS	sehrát
polská	polský	k2eAgFnSc1d1	polská
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
podmínku	podmínka	k1gFnSc4	podmínka
pro	pro	k7c4	pro
jednání	jednání	k1gNnSc4	jednání
polské	polský	k2eAgFnSc2d1	polská
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
stranou	strana	k1gFnSc7	strana
předložila	předložit	k5eAaPmAgFnS	předložit
nepřijatelný	přijatelný	k2eNgInSc4d1	nepřijatelný
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
uznání	uznání	k1gNnSc4	uznání
polských	polský	k2eAgFnPc2d1	polská
hranic	hranice	k1gFnPc2	hranice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
předválečných	předválečný	k2eAgMnPc2d1	předválečný
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
výkladem	výklad	k1gInSc7	výklad
skutkových	skutkový	k2eAgFnPc2d1	skutková
okolností	okolnost	k1gFnPc2	okolnost
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
polský	polský	k2eAgMnSc1d1	polský
profesor	profesor	k1gMnSc1	profesor
Wladyslaw	Wladyslaw	k1gMnSc1	Wladyslaw
Filar	Filar	k1gMnSc1	Filar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
popsal	popsat	k5eAaPmAgMnS	popsat
hledisko	hledisko	k1gNnSc4	hledisko
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
vědců	vědec	k1gMnPc2	vědec
jako	jako	k8xS	jako
podstatně	podstatně	k6eAd1	podstatně
zavádějící	zavádějící	k2eAgFnSc1d1	zavádějící
<g/>
,	,	kIx,	,
odchylující	odchylující	k2eAgFnSc1d1	odchylující
se	se	k3xPyFc4	se
od	od	k7c2	od
historické	historický	k2eAgFnSc2d1	historická
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
označování	označování	k1gNnSc1	označování
událostí	událost	k1gFnPc2	událost
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
za	za	k7c4	za
"	"	kIx"	"
<g/>
ukrajinsko-polskou	ukrajinskoolský	k2eAgFnSc4d1	ukrajinsko-polský
válku	válka	k1gFnSc4	válka
<g/>
"	"	kIx"	"
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
zjevně	zjevně	k6eAd1	zjevně
nesprávné	správný	k2eNgNnSc1d1	nesprávné
<g/>
,	,	kIx,	,
když	když	k8xS	když
napsal	napsat	k5eAaBmAgMnS	napsat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
polsko-ukrajinský	polskokrajinský	k2eAgInSc1d1	polsko-ukrajinský
boj	boj	k1gInSc1	boj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vraždění	vraždění	k1gNnSc1	vraždění
bezbranných	bezbranný	k2eAgMnPc2d1	bezbranný
a	a	k8xC	a
nevinných	vinný	k2eNgMnPc2d1	nevinný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
vyhlazování	vyhlazování	k1gNnSc1	vyhlazování
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Wladyslaw	Wladyslaw	k1gFnSc1	Wladyslaw
Filar	Filar	k1gInSc1	Filar
také	také	k9	také
obvinil	obvinit	k5eAaPmAgInS	obvinit
z	z	k7c2	z
"	"	kIx"	"
<g/>
vybělení	vybělení	k1gNnPc2	vybělení
<g/>
"	"	kIx"	"
faktů	fakt	k1gInPc2	fakt
konkrétní	konkrétní	k2eAgMnPc4d1	konkrétní
ukrajinské	ukrajinský	k2eAgMnPc4d1	ukrajinský
historiky	historik	k1gMnPc4	historik
<g/>
,	,	kIx,	,
Lwa	Lwa	k1gMnPc4	Lwa
Szankowského	Szankowský	k1gMnSc4	Szankowský
i	i	k8xC	i
Petro	Petra	k1gFnSc5	Petra
Poticzneho	Poticzne	k1gMnSc2	Poticzne
<g/>
[	[	kIx(	[
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Filara	Filar	k1gMnSc2	Filar
také	také	k9	také
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
podobný	podobný	k2eAgInSc1d1	podobný
přístup	přístup	k1gInSc1	přístup
i	i	k8xC	i
Jaroslaw	Jaroslaw	k1gFnSc1	Jaroslaw
Polenskyj	Polenskyj	k1gFnSc1	Polenskyj
<g/>
.	.	kIx.	.
<g/>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
před	před	k7c7	před
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročím	výročí	k1gNnSc7	výročí
masakru	masakr	k1gInSc2	masakr
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
otevřený	otevřený	k2eAgInSc1d1	otevřený
dopis	dopis	k1gInSc1	dopis
podepsaný	podepsaný	k2eAgInSc1d1	podepsaný
skupinou	skupina	k1gFnSc7	skupina
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c6	o
odpuštění	odpuštění	k1gNnSc6	odpuštění
Poláky	Polák	k1gMnPc4	Polák
"	"	kIx"	"
<g/>
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
životy	život	k1gInPc1	život
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
ukrajinskými	ukrajinský	k2eAgFnPc7d1	ukrajinská
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Lytvyn	Lytvyn	k1gMnSc1	Lytvyn
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
Tysjača	Tysjačum	k1gNnSc2	Tysjačum
rokiv	rokiva	k1gFnPc2	rokiva
susidstva	susidstvo	k1gNnSc2	susidstvo
i	i	k9	i
vzaemodiji	vzaemodít	k5eAaPmIp1nS	vzaemodít
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Tisíc	tisíc	k4xCgInSc1	tisíc
let	let	k1gInSc1	let
sousedství	sousedství	k1gNnSc2	sousedství
a	a	k8xC	a
vzájemnosti	vzájemnost	k1gFnSc2	vzájemnost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
politik	politik	k1gMnSc1	politik
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
masakry	masakr	k1gInPc1	masakr
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
jsou	být	k5eAaImIp3nP	být
akcemi	akce	k1gFnPc7	akce
nesoucími	nesoucí	k2eAgFnPc7d1	nesoucí
"	"	kIx"	"
<g/>
znaky	znak	k1gInPc1	znak
etnických	etnický	k2eAgFnPc2d1	etnická
čistek	čistka	k1gFnPc2	čistka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
I.	I.	kA	I.
Iljušina	Iljušin	k2eAgFnSc1d1	Iljušina
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
historiografii	historiografie	k1gFnSc6	historiografie
nadále	nadále	k6eAd1	nadále
objevují	objevovat	k5eAaImIp3nP	objevovat
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
polsko-ukrajinské	polskokrajinský	k2eAgInPc4d1	polsko-ukrajinský
vztahy	vztah	k1gInPc4	vztah
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jednostranně	jednostranně	k6eAd1	jednostranně
protipolsky	protipolsky	k6eAd1	protipolsky
<g/>
.	.	kIx.	.
</s>
<s>
Iljušin	iljušin	k1gInSc1	iljušin
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Volodymyra	Volodymyra	k1gFnSc1	Volodymyra
Serhijčuka	Serhijčuk	k1gMnSc2	Serhijčuk
a	a	k8xC	a
Andrije	Andrije	k1gMnSc2	Andrije
Bolanovského	Bolanovský	k2eAgMnSc2d1	Bolanovský
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
také	také	k9	také
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
historiografii	historiografie	k1gFnSc6	historiografie
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
cenné	cenný	k2eAgFnPc1d1	cenná
práce	práce	k1gFnPc1	práce
s	s	k7c7	s
vyváženější	vyvážený	k2eAgFnSc7d2	vyváženější
a	a	k8xC	a
hlubší	hluboký	k2eAgFnSc7d2	hlubší
interpretací	interpretace	k1gFnSc7	interpretace
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
poznamenané	poznamenaný	k2eAgNnSc1d1	poznamenané
vlastenectvím	vlastenectví	k1gNnSc7	vlastenectví
a	a	k8xC	a
cituje	citovat	k5eAaBmIp3nS	citovat
publikace	publikace	k1gFnSc1	publikace
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Isajevyče	Isajevyč	k1gFnSc2	Isajevyč
<g/>
,	,	kIx,	,
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Hrycaka	Hrycak	k1gMnSc2	Hrycak
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Steblije	Steblije	k1gFnSc4	Steblije
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Smolije	Smolije	k1gFnSc1	Smolije
i	i	k8xC	i
Bohdana	Bohdana	k1gFnSc1	Bohdana
Hudije	Hudije	k1gFnSc1	Hudije
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hrycak	Hrycak	k1gMnSc1	Hrycak
volyňský	volyňský	k2eAgInSc4d1	volyňský
masakr	masakr	k1gInSc4	masakr
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
křest	křest	k1gInSc1	křest
ohněm	oheň	k1gInSc7	oheň
<g/>
"	"	kIx"	"
UPA	UPA	kA	UPA
<g/>
,	,	kIx,	,
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
válečnými	válečný	k2eAgInPc7d1	válečný
zločiny	zločin	k1gInPc7	zločin
a	a	k8xC	a
jako	jako	k9	jako
absurdní	absurdní	k2eAgFnSc4d1	absurdní
akci	akce	k1gFnSc4	akce
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
politicko-vojenského	politickoojenský	k2eAgInSc2d1	politicko-vojenský
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Grzegorze	Grzegorze	k1gFnSc2	Grzegorze
Motyky	motyka	k1gFnSc2	motyka
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
publikace	publikace	k1gFnSc2	publikace
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
UPA	UPA	kA	UPA
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
proti	proti	k7c3	proti
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
marginalizují	marginalizovat	k5eAaPmIp3nP	marginalizovat
problém	problém	k1gInSc4	problém
jednání	jednání	k1gNnSc2	jednání
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vraždění	vraždění	k1gNnSc4	vraždění
polských	polský	k2eAgMnPc2d1	polský
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc1	filmografie
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
volyňského	volyňský	k2eAgInSc2d1	volyňský
masakru	masakr	k1gInSc2	masakr
==	==	k?	==
</s>
</p>
<p>
<s>
Wolyń	Wolyń	k?	Wolyń
–	–	k?	–
Zapis	Zapis	k1gInSc1	Zapis
zbrodni	zbrodni	k?	zbrodni
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poeta	poeta	k1gMnSc1	poeta
nieznany	nieznana	k1gFnSc2	nieznana
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bliscy	Bliscy	k1gInPc1	Bliscy
a	a	k8xC	a
dalecy	dalecy	k1gInPc1	dalecy
</s>
</p>
<p>
<s>
Armia	Armia	k1gFnSc1	Armia
Krajowa	Krajow	k1gInSc2	Krajow
na	na	k7c4	na
Kresach	Kresach	k1gInSc4	Kresach
(	(	kIx(	(
<g/>
Wolyń	Wolyń	k1gFnSc4	Wolyń
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kryptonim	Kryptonim	k1gInSc1	Kryptonim
Pożoga	Pożog	k1gMnSc2	Pożog
</s>
</p>
<p>
<s>
Zapomniane	Zapomnianout	k5eAaPmIp3nS	Zapomnianout
zbrodnie	zbrodnie	k1gFnSc1	zbrodnie
na	na	k7c6	na
Wolyniu	Wolynium	k1gNnSc6	Wolynium
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Było	Było	k1gNnSc1	Było
sobie	sobie	k1gFnSc2	sobie
miasteczko	miasteczka	k1gFnSc5	miasteczka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Tadeusz	Tadeusz	k1gMnSc1	Tadeusz
Arciuch	Arciuch	k1gInSc1	Arciuch
i	i	k8xC	i
Maciej	Maciej	k1gInSc1	Maciej
Wojciechowski	Wojciechowsk	k1gFnSc2	Wojciechowsk
</s>
</p>
<p>
<s>
Ukraiński	Ukraiński	k1gNnSc1	Ukraiński
Rapsod	Rapsoda	k1gFnPc2	Rapsoda
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reż	reż	k?	reż
<g/>
.	.	kIx.	.
</s>
<s>
Dariusz	Dariusz	k1gMnSc1	Dariusz
Marek	Marek	k1gMnSc1	Marek
Srzednicki	Srzednicke	k1gFnSc4	Srzednicke
</s>
</p>
<p>
<s>
Garnizon	garnizon	k1gInSc1	garnizon
100	[number]	k4	100
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jaroslaw	Jaroslaw	k1gFnSc2	Jaroslaw
Banaszek	Banaszka	k1gFnPc2	Banaszka
</s>
</p>
<p>
<s>
Śmierć	Śmierć	k?	Śmierć
po	po	k7c6	po
raz	razit	k5eAaImRp2nS	razit
setny	setna	k1gFnSc2	setna
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ewa	Ewa	k1gFnSc1	Ewa
Szakalicka	Szakalicka	k1gFnSc1	Szakalicka
</s>
</p>
<p>
<s>
Czas	Czas	k6eAd1	Czas
Pamięci	Pamięce	k1gMnPc1	Pamięce
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ewa	Ewa	k1gFnSc1	Ewa
Szakalicka	Szakalicka	k1gFnSc1	Szakalicka
</s>
</p>
<p>
<s>
Sahryń	Sahryń	k?	Sahryń
-	-	kIx~	-
pamięć	pamięć	k?	pamięć
zapisana	zapisana	k1gFnSc1	zapisana
w	w	k?	w
kamieniu	kamienium	k1gNnSc6	kamienium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ewa	Ewa	k1gFnSc1	Ewa
Szakalicka	Szakalicka	k1gFnSc1	Szakalicka
</s>
</p>
<p>
<s>
Poła	Poła	k6eAd1	Poła
malowane	malowanout	k5eAaPmIp3nS	malowanout
krwią	krwią	k?	krwią
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Łukasz	Łukasza	k1gFnPc2	Łukasza
Szelecki	Szeleck	k1gFnSc2	Szeleck
</s>
</p>
<p>
<s>
Zatruta	Zatrut	k2eAgFnSc1d1	Zatrut
krew	krew	k?	krew
bratnia	bratnium	k1gNnPc1	bratnium
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Krzysztof	Krzysztof	k1gInSc1	Krzysztof
Wojciechowski	Wojciechowsk	k1gFnSc2	Wojciechowsk
</s>
</p>
<p>
<s>
Volyň	Volyň	k1gFnSc1	Volyň
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Wołyń	Wołyń	k1gFnSc2	Wołyń
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Wojciech	Wojciech	k1gInSc1	Wojciech
Smarzowski	Smarzowske	k1gFnSc4	Smarzowske
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rzeź	Rzeź	k1gMnSc2	Rzeź
wołyńska	wołyńsek	k1gMnSc2	wołyńsek
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
<g/>
:	:	kIx,	:
Volyň	Volyň	k1gFnSc1	Volyň
1943	[number]	k4	1943
<g/>
:	:	kIx,	:
Genocidní	genocidní	k2eAgFnSc1d1	genocidní
čistka	čistka	k1gFnSc1	čistka
-	-	kIx~	-
fakta	faktum	k1gNnPc1	faktum
<g/>
,	,	kIx,	,
analogie	analogie	k1gFnPc1	analogie
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-2857-0	[number]	k4	978-80-200-2857-0
</s>
</p>
<p>
<s>
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
<g/>
:	:	kIx,	:
Polacy	Polacy	k1gInPc1	Polacy
i	i	k8xC	i
Ukraińcy	Ukraińcy	k1gInPc1	Ukraińcy
w	w	k?	w
drugiej	drugiej	k1gInSc4	drugiej
wojnie	wojnie	k1gFnSc2	wojnie
światowej	światowej	k1gInSc1	światowej
–	–	k?	–
Prezentace	prezentace	k1gFnSc2	prezentace
na	na	k7c4	na
X	X	kA	X
zasedání	zasedání	k1gNnPc4	zasedání
polsko-ukrajinské	polskokrajinský	k2eAgFnSc2d1	polsko-ukrajinská
výboru	výbor	k1gInSc2	výbor
expertů	expert	k1gMnPc2	expert
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
obsahu	obsah	k1gInSc2	obsah
učebnic	učebnice	k1gFnPc2	učebnice
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
zeměpisu	zeměpis	k1gInSc2	zeměpis
<g/>
,	,	kIx,	,
Lvov	Lvov	k1gInSc1	Lvov
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
-	-	kIx~	-
stav	stav	k1gInSc4	stav
historického	historický	k2eAgNnSc2d1	historické
bádání	bádání	k1gNnSc2	bádání
o	o	k7c6	o
problematice	problematika	k1gFnSc6	problematika
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
dokumentů	dokument	k1gInPc2	dokument
<g/>
:	:	kIx,	:
Polacy	Polac	k2eAgFnPc4d1	Polac
i	i	k8xC	i
Ukraińcy	Ukraińc	k2eAgFnPc4d1	Ukraińc
pomiędzy	pomiędza	k1gFnPc4	pomiędza
dwoma	dwomum	k1gNnSc2	dwomum
systemami	systema	k1gFnPc7	systema
totalitarnymi	totalitarny	k1gFnPc7	totalitarny
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
t.	t.	k?	t.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
oprac	oprac	k1gFnSc1	oprac
<g/>
.	.	kIx.	.
nauk	nauka	k1gFnPc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
<g/>
,	,	kIx,	,
Jurij	Jurij	k1gMnSc1	Jurij
Szapowal	Szapowal	k1gMnSc1	Szapowal
Instytut	Instytut	k1gInSc4	Instytut
Pamięci	Pamięce	k1gFnSc3	Pamięce
Narodowej	Narodowej	k1gInSc1	Narodowej
–	–	k?	–
Komisja	Komisja	k1gMnSc1	Komisja
Ścigania	Ściganium	k1gNnSc2	Ściganium
Zbrodni	Zbrodni	k?	Zbrodni
przeciwko	przeciwko	k6eAd1	przeciwko
Narodowi	Narodowi	k1gNnSc1	Narodowi
Polskiemu	Polskiem	k1gInSc2	Polskiem
[	[	kIx(	[
<g/>
i	i	k9	i
in	in	k?	in
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Warszawa	Warszawa	k1gFnSc1	Warszawa
/	/	kIx~	/
<g/>
Kijów	Kijów	k1gFnSc1	Kijów
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Warszawa	Warszawum	k1gNnSc2	Warszawum
<g/>
:	:	kIx,	:
Instytut	Instytut	k1gInSc4	Instytut
Pamięci	Pamięce	k1gFnSc3	Pamięce
Narodowej	Narodowej	k1gInSc1	Narodowej
–	–	k?	–
Komisja	Komisja	k1gMnSc1	Komisja
Ścigania	Ściganium	k1gNnSc2	Ściganium
Zbrodni	Zbrodni	k?	Zbrodni
przeciwko	przeciwko	k6eAd1	przeciwko
Narodowi	Narodowi	k1gNnSc1	Narodowi
Polskiemu	Polskiem	k1gInSc2	Polskiem
:	:	kIx,	:
Archiwum	Archiwum	k1gInSc1	Archiwum
Ministerstwa	Ministerstw	k2eAgFnSc1d1	Ministerstw
Spraw	Spraw	k1gFnSc1	Spraw
Wewnętrznych	Wewnętrznycha	k1gFnPc2	Wewnętrznycha
i	i	k9	i
Administracji	Administracj	k1gMnSc3	Administracj
RP	RP	kA	RP
;	;	kIx,	;
Kijów	Kijów	k1gMnSc1	Kijów
:	:	kIx,	:
Państwowe	Państwowe	k1gInSc1	Państwowe
Archiwum	Archiwum	k1gNnSc1	Archiwum
Slużby	Slużba	k1gMnSc2	Slużba
Bezpieczeństwa	Bezpieczeństwus	k1gMnSc2	Bezpieczeństwus
Ukrainy	Ukraina	k1gMnSc2	Ukraina
:	:	kIx,	:
Instytut	Instytut	k1gMnSc1	Instytut
Badań	Badań	k1gMnSc1	Badań
Politycznych	Politycznych	k1gMnSc1	Politycznych
i	i	k8xC	i
Narodowościowych	Narodowościowych	k1gMnSc1	Narodowościowych
Akademii	akademie	k1gFnSc4	akademie
Nauk	nauka	k1gFnPc2	nauka
Ukrainy	Ukraina	k1gFnSc2	Ukraina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Stran	stran	k7c2	stran
1511	[number]	k4	1511
(	(	kIx(	(
<g/>
875	[number]	k4	875
cz	cz	k?	cz
<g/>
.1	.1	k4	.1
<g/>
,	,	kIx,	,
877	[number]	k4	877
<g/>
–	–	k?	–
<g/>
1511	[number]	k4	1511
cz	cz	k?	cz
<g/>
.2	.2	k4	.2
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
89078	[number]	k4	89078
<g/>
-	-	kIx~	-
<g/>
77	[number]	k4	77
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Archiwum	Archiwum	k1gNnSc1	Archiwum
Adama	Adam	k1gMnSc2	Adam
Bienia	Bienium	k1gNnSc2	Bienium
<g/>
:	:	kIx,	:
akta	akta	k1gNnPc1	akta
narodowościowe	narodowościowe	k1gFnPc2	narodowościowe
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
oprac	oprac	k1gInSc1	oprac
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
wstęp	wstęp	k1gInSc4	wstęp
i	i	k8xC	i
przypisy	przypis	k1gInPc4	przypis
Jan	Jan	k1gMnSc1	Jan
Brzeski	Brzesk	k1gFnSc2	Brzesk
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
Roliński	Rolińsk	k1gFnSc2	Rolińsk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kraków	Kraków	k?	Kraków
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
"	"	kIx"	"
<g/>
Księgarnia	Księgarnium	k1gNnSc2	Księgarnium
Akademicka	Akademicko	k1gNnSc2	Akademicko
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
nákladem	náklad	k1gInSc7	náklad
Biblioteki	Biblioteki	k1gNnSc2	Biblioteki
Jagiellońskiej	Jagiellońskiej	k1gInSc1	Jagiellońskiej
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7188	[number]	k4	7188
<g/>
-	-	kIx~	-
<g/>
185	[number]	k4	185
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Antypolska	Antypolska	k1gFnSc1	Antypolska
akcja	akcja	k1gFnSc1	akcja
OUN-UPA	OUN-UPA	k1gFnSc1	OUN-UPA
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
:	:	kIx,	:
fakty	faktum	k1gNnPc7	faktum
i	i	k9	i
interpretacje	interpretacj	k1gFnPc1	interpretacj
(	(	kIx(	(
<g/>
red	red	k?	red
<g/>
.	.	kIx.	.
nauk	nauka	k1gFnPc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
i	i	k8xC	i
Dariusz	Dariusz	k1gInSc1	Dariusz
Libionka	Libionka	k1gFnSc1	Libionka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Instytut	Instytut	k1gInSc4	Instytut
Pamięci	Pamięce	k1gFnSc4	Pamięce
Narodowej	Narodowej	k1gInSc1	Narodowej
<g/>
.	.	kIx.	.
</s>
<s>
Komisja	Komisja	k1gMnSc1	Komisja
Ścigania	Ściganium	k1gNnSc2	Ściganium
Zbrodni	Zbrodni	k?	Zbrodni
przeciwko	przeciwko	k6eAd1	przeciwko
Narodowi	Narodowi	k1gNnSc1	Narodowi
Polskiemu	Polskiem	k1gInSc2	Polskiem
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Sborník	sborník	k1gInSc1	sborník
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
zasedání	zasedání	k1gNnSc2	zasedání
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
úřadem	úřad	k1gInSc7	úřad
veřejného	veřejný	k2eAgNnSc2d1	veřejné
školství	školství	k1gNnSc2	školství
-	-	kIx~	-
KŚZpNP	KŚZpNP	k1gFnPc2	KŚZpNP
<g/>
.	.	kIx.	.
</s>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
v	v	k7c4	v
Lublinie	Lublinie	k1gFnPc4	Lublinie
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
2002	[number]	k4	2002
Wyd	Wyd	k1gFnPc2	Wyd
<g/>
.	.	kIx.	.
</s>
<s>
IPN	IPN	kA	IPN
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
83-89078-09-0	[number]	k4	83-89078-09-0
</s>
</p>
<p>
<s>
Henryk	Henryk	k6eAd1	Henryk
Cybulski	Cybulski	k1gNnSc1	Cybulski
<g/>
:	:	kIx,	:
Czerwone	Czerwon	k1gMnSc5	Czerwon
noce	nocus	k1gMnSc5	nocus
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
5	[number]	k4	5
zm	zm	k?	zm
<g/>
.	.	kIx.	.
</s>
<s>
Bellona	Bellona	k1gFnSc1	Bellona
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
ss	ss	k?	ss
<g/>
.	.	kIx.	.
375	[number]	k4	375
<g/>
.	.	kIx.	.
</s>
<s>
Literárně	literárně	k6eAd1	literárně
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Henryk	Henryk	k1gMnSc1	Henryk
Pająk	Pająk	k1gMnSc1	Pająk
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7834	[number]	k4	7834
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Wladyslaw	Wladyslaw	k?	Wladyslaw
Filar	Filar	k1gInSc1	Filar
<g/>
:	:	kIx,	:
Przebraże	Przebraże	k1gInSc1	Przebraże
–	–	k?	–
bastion	bastion	k1gInSc1	bastion
polskiej	polskiej	k1gInSc1	polskiej
samoobrony	samoobron	k1gInPc7	samoobron
na	na	k7c6	na
Wolyniu	Wolynium	k1gNnSc6	Wolynium
<g/>
.	.	kIx.	.
</s>
<s>
Bitwy	Bitwa	k1gFnPc1	Bitwa
i	i	k8xC	i
akcje	akcj	k1gFnPc1	akcj
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
</s>
<s>
Rytm	Rytm	k1gInSc1	Rytm
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ss	ss	k?	ss
<g/>
.	.	kIx.	.
128	[number]	k4	128
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
<g/>
:	:	kIx,	:
Biblioteka	Biblioteka	k1gFnSc1	Biblioteka
Armii	Armie	k1gFnSc4	Armie
Krajowej	Krajowej	k1gInSc1	Krajowej
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7399	[number]	k4	7399
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Wladyslaw	Wladyslaw	k?	Wladyslaw
Filar	Filar	k1gInSc1	Filar
<g/>
:	:	kIx,	:
Wydarzenia	Wydarzenium	k1gNnSc2	Wydarzenium
wolyńskie	wolyńskie	k1gFnSc2	wolyńskie
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1	vydavatel
Adam	Adam	k1gMnSc1	Adam
Marszalek	Marszalek	k1gInSc4	Marszalek
<g/>
.	.	kIx.	.
</s>
<s>
Toruń	Toruń	k?	Toruń
2008	[number]	k4	2008
ISBN	ISBN	kA	ISBN
978-83-7441-884-3	[number]	k4	978-83-7441-884-3
</s>
</p>
<p>
<s>
Aleksander	Aleksander	k1gMnSc1	Aleksander
Gogun	Gogun	k1gMnSc1	Gogun
<g/>
:	:	kIx,	:
Stalinowska	Stalinowska	k1gFnSc1	Stalinowska
wojna	wojna	k1gFnSc1	wojna
partyzancka	partyzancka	k1gFnSc1	partyzancka
na	na	k7c4	na
Ukrainie	Ukrainie	k1gFnPc4	Ukrainie
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
cz	cz	k?	cz
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
:	:	kIx,	:
<g/>
Pamięć	Pamięć	k1gMnSc1	Pamięć
i	i	k8xC	i
Sprawiedliwość	Sprawiedliwość	k1gFnSc1	Sprawiedliwość
nr	nr	k?	nr
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Warszawa	Warszaw	k1gInSc2	Warszaw
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
293	[number]	k4	293
<g/>
-	-	kIx~	-
<g/>
314	[number]	k4	314
<g/>
,	,	kIx,	,
cz	cz	k?	cz
<g/>
.2	.2	k4	.2
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
:	:	kIx,	:
Pamięć	Pamięć	k1gMnSc1	Pamięć
i	i	k8xC	i
Sprawiedliwość	Sprawiedliwość	k1gFnSc1	Sprawiedliwość
nr	nr	k?	nr
1	[number]	k4	1
<g/>
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
Warszawa	Warszaw	k1gInSc2	Warszaw
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
s.	s.	k?	s.
119	[number]	k4	119
<g/>
-	-	kIx~	-
<g/>
144	[number]	k4	144
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grzegorz	Grzegorz	k1gMnSc1	Grzegorz
Hryciuk	Hryciuk	k1gMnSc1	Hryciuk
<g/>
:	:	kIx,	:
Przemiany	Przemian	k1gInPc1	Przemian
narodowościowe	narodowościowe	k1gNnPc2	narodowościowe
i	i	k8xC	i
ludnościowe	ludnościowe	k1gNnPc2	ludnościowe
w	w	k?	w
Galicji	Galicj	k1gInSc6	Galicj
Wschodniej	Wschodniej	k1gInSc1	Wschodniej
i	i	k8xC	i
na	na	k7c6	na
Wolyniu	Wolynium	k1gNnSc6	Wolynium
w	w	k?	w
latach	latach	k1gInSc1	latach
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Toruń	Toruń	k1gFnSc1	Toruń
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
Adam	Adam	k1gMnSc1	Adam
Marszalek	Marszalek	k1gInSc4	Marszalek
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7441	[number]	k4	7441
<g/>
-	-	kIx~	-
<g/>
121	[number]	k4	121
<g/>
-X	-X	k?	-X
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Hryciuk	Hryciuka	k1gFnPc2	Hryciuka
<g/>
:	:	kIx,	:
Straty	stratus	k1gInPc7	stratus
ludności	ludnośce	k1gFnSc4	ludnośce
na	na	k7c6	na
Wolyniu	Wolynium	k1gNnSc6	Wolynium
w	w	k?	w
latach	latach	k1gInSc1	latach
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
w	w	k?	w
<g/>
:	:	kIx,	:
Polska-Ukraina	Polska-Ukraina	k1gMnSc1	Polska-Ukraina
<g/>
:	:	kIx,	:
<g/>
trudne	trudnout	k5eAaPmIp3nS	trudnout
pytania	pytanium	k1gNnSc2	pytanium
t	t	k?	t
<g/>
.5	.5	k4	.5
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
Światowy	Światow	k2eAgInPc1d1	Światow
Związek	Związek	k1gInSc1	Związek
Żolnierzy	Żolnierza	k1gFnSc2	Żolnierza
Armii	Armie	k1gFnSc3	Armie
Krajowej	Krajowej	k1gInSc1	Krajowej
<g/>
,	,	kIx,	,
Związek	Związek	k1gInSc1	Związek
Ukraińców	Ukraińców	k1gFnSc2	Ukraińców
w	w	k?	w
Polsce	Polska	k1gFnSc6	Polska
<g/>
.	.	kIx.	.
</s>
<s>
Materiály	materiál	k1gInPc4	materiál
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
semináře	seminář	k1gInSc2	seminář
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
"	"	kIx"	"
<g/>
Stosunki	Stosunki	k1gNnSc1	Stosunki
polsko-ukraińskie	polskokraińskie	k1gFnSc2	polsko-ukraińskie
w	w	k?	w
latach	latach	k1gInSc1	latach
II	II	kA	II
wojny	wojn	k1gInPc1	wojn
światowej	światowej	k1gInSc1	światowej
<g/>
"	"	kIx"	"
Łuck	Łuck	k1gInSc1	Łuck
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
kwietnia	kwietnium	k1gNnSc2	kwietnium
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
908944	[number]	k4	908944
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Ihor	Ihor	k1gMnSc1	Ihor
Iljuszyn	Iljuszyn	k1gMnSc1	Iljuszyn
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
UPA	UPA	kA	UPA
i	i	k8xC	i
AK.	AK.	k1gFnPc3	AK.
Konflikt	konflikt	k1gInSc1	konflikt
w	w	k?	w
Zachodniej	Zachodniej	k1gInSc1	Zachodniej
Ukrainie	Ukrainie	k1gFnSc1	Ukrainie
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-83-928483-0-1	[number]	k4	978-83-928483-0-1
</s>
</p>
<p>
<s>
Aleksander	Aleksander	k1gMnSc1	Aleksander
Korman	Korman	k1gMnSc1	Korman
<g/>
:	:	kIx,	:
Ludobójstwo	Ludobójstwo	k1gMnSc1	Ludobójstwo
UPA	UPA	kA	UPA
na	na	k7c6	na
ludności	ludnośce	k1gFnSc6	ludnośce
polskiej	polskiej	k1gInSc4	polskiej
<g/>
.	.	kIx.	.
</s>
<s>
Dokumentacja	Dokumentacj	k2eAgFnSc1d1	Dokumentacj
fotograficzna	fotograficzna	k1gFnSc1	fotograficzna
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
</s>
<s>
Nortom	Nortom	k1gInSc1	Nortom
<g/>
,	,	kIx,	,
Wroclaw	Wroclaw	k1gFnSc1	Wroclaw
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ss	ss	k?	ss
<g/>
.	.	kIx.	.
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83-85829-84-9	[number]	k4	83-85829-84-9
(	(	kIx(	(
<g/>
fragment	fragment	k1gInSc4	fragment
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
verzi	verze	k1gFnSc6	verze
s	s	k7c7	s
ruskými	ruský	k2eAgInPc7d1	ruský
popisy	popis	k1gInPc7	popis
–	–	k?	–
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Grzegorz	Grzegorz	k1gInSc1	Grzegorz
Motyka	motyka	k1gFnSc1	motyka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ukraińska	Ukraińska	k1gFnSc1	Ukraińska
partyzantka	partyzantka	k1gFnSc1	partyzantka
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Wyd	Wyd	k1gFnSc1	Wyd
<g/>
.	.	kIx.	.
</s>
<s>
Instytut	Instytut	k2eAgMnSc1d1	Instytut
Studiów	Studiów	k1gMnSc1	Studiów
Politycznych	Politycznych	k1gMnSc1	Politycznych
PAN	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
Vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
"	"	kIx"	"
<g/>
Rytm	Rytm	k1gInSc1	Rytm
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
83-88490-58-3	[number]	k4	83-88490-58-3
(	(	kIx(	(
<g/>
ISP	ISP	kA	ISP
PAN	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
83-7399-163-8	[number]	k4	83-7399-163-8
(	(	kIx(	(
<g/>
Rytm	Rytm	k1gInSc1	Rytm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
88490	[number]	k4	88490
<g/>
-	-	kIx~	-
<g/>
58	[number]	k4	58
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Tadeusz	Tadeusz	k1gInSc1	Tadeusz
Andrzej	Andrzej	k1gFnSc2	Andrzej
Olszański	Olszańsk	k1gFnSc2	Olszańsk
<g/>
:	:	kIx,	:
Konflikt	konflikt	k1gInSc1	konflikt
polsko-ukraiński	polskokraiński	k1gNnSc1	polsko-ukraiński
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
v	v	k7c6	v
měsíčně	měsíčně	k6eAd1	měsíčně
vydávaném	vydávaný	k2eAgInSc6d1	vydávaný
časopisu	časopis	k1gInSc6	časopis
Więź	Więź	k1gFnSc1	Więź
Nr	Nr	k1gFnSc1	Nr
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
(	(	kIx(	(
<g/>
397	[number]	k4	397
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
<g/>
–	–	k?	–
<g/>
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
1991	[number]	k4	1991
s.	s.	k?	s.
214	[number]	k4	214
<g/>
-	-	kIx~	-
<g/>
232	[number]	k4	232
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wladyslaw	Wladyslaw	k?	Wladyslaw
Siemaszko	Siemaszka	k1gFnSc5	Siemaszka
<g/>
,	,	kIx,	,
Ewa	Ewa	k1gFnSc5	Ewa
Siemaszko	Siemaszka	k1gFnSc5	Siemaszka
<g/>
:	:	kIx,	:
Ludobójstwo	Ludobójstwo	k6eAd1	Ludobójstwo
dokonane	dokonanout	k5eAaPmIp3nS	dokonanout
przez	przez	k1gMnSc1	przez
nacjonalistów	nacjonalistów	k?	nacjonalistów
ukraińskich	ukraińskich	k1gMnSc1	ukraińskich
na	na	k7c6	na
ludności	ludnośce	k1gFnSc6	ludnośce
polskiej	polskiej	k1gInSc4	polskiej
Wolynia	Wolynium	k1gNnSc2	Wolynium
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Wydawnictwo	Wydawnictwo	k6eAd1	Wydawnictwo
"	"	kIx"	"
<g/>
von	von	k1gInSc4	von
Borowiecky	Borowiecko	k1gNnPc7	Borowiecko
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
87689	[number]	k4	87689
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Wydane	Wydanout	k5eAaPmIp3nS	Wydanout
przy	prz	k2eAgFnPc4d1	prz
pomocy	pomoca	k1gFnPc4	pomoca
finansowej	finansowej	k1gInSc4	finansowej
Kancelarii	Kancelarie	k1gFnSc4	Kancelarie
Prezydenta	Prezydent	k1gMnSc2	Prezydent
Rzeczypospolitej	Rzeczypospolitej	k1gMnSc2	Rzeczypospolitej
Polskiej	Polskiej	k1gInSc4	Polskiej
<g/>
,	,	kIx,	,
stron	stron	k1gInSc4	stron
1433	[number]	k4	1433
<g/>
,	,	kIx,	,
ilustrowana	ilustrowana	k1gFnSc1	ilustrowana
<g/>
,	,	kIx,	,
źródla	źródnout	k5eAaPmAgFnS	źródnout
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Timothy	Timoth	k1gInPc1	Timoth
Snyder	Snydra	k1gFnPc2	Snydra
<g/>
:	:	kIx,	:
Tajna	Tajna	k?	Tajna
wojna	wojna	k1gFnSc1	wojna
<g/>
.	.	kIx.	.
</s>
<s>
Henryk	Henryk	k1gInSc1	Henryk
Józewski	Józewsk	k1gFnSc2	Józewsk
i	i	k8xC	i
polsko-sowiecka	polskoowiecko	k1gNnSc2	polsko-sowiecko
rozgrywka	rozgrywek	k1gInSc2	rozgrywek
o	o	k7c6	o
Ukrainę	Ukrainę	k1gFnSc6	Ukrainę
<g/>
.	.	kIx.	.
</s>
<s>
Kraków	Kraków	k?	Kraków
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
</s>
<s>
Instytut	Instytut	k1gMnSc1	Instytut
Wydawniczy	Wydawnicza	k1gFnSc2	Wydawnicza
Znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
240	[number]	k4	240
<g/>
-	-	kIx~	-
<g/>
1033	[number]	k4	1033
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Ryszard	Ryszard	k1gInSc1	Ryszard
Torzecki	Torzeck	k1gFnSc2	Torzeck
<g/>
:	:	kIx,	:
Polacy	Polaca	k1gFnSc2	Polaca
i	i	k8xC	i
Ukraińcy	Ukraińca	k1gFnSc2	Ukraińca
<g/>
.	.	kIx.	.
</s>
<s>
Sprawa	Spraw	k2eAgFnSc1d1	Spraw
ukraińska	ukraińska	k1gFnSc1	ukraińska
w	w	k?	w
czasie	czasie	k1gFnSc2	czasie
II	II	kA	II
wojny	wojna	k1gFnSc2	wojna
światowej	światowej	k1gInSc4	światowej
na	na	k7c4	na
terenie	terenie	k1gFnPc4	terenie
II	II	kA	II
Rzeczypospolitej	Rzeczypospolitej	k1gFnSc1	Rzeczypospolitej
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
1993	[number]	k4	1993
Vyd	Vyd	k1gFnPc2	Vyd
<g/>
.	.	kIx.	.
</s>
<s>
Wydawnictwo	Wydawnictwo	k1gMnSc1	Wydawnictwo
Naukowe	Naukow	k1gFnSc2	Naukow
PWN	PWN	kA	PWN
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11126	[number]	k4	11126
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Р	Р	k?	Р
<g/>
,	,	kIx,	,
Я	Я	k?	Я
<g/>
.	.	kIx.	.
К	К	k?	К
к	к	k?	к
<g/>
.	.	kIx.	.
П	П	k?	П
в	в	k?	в
1919	[number]	k4	1919
т	т	k?	т
1921	[number]	k4	1921
р	р	k?	р
<g/>
.	.	kIx.	.
Д	Д	k?	Д
<g/>
:	:	kIx,	:
В	В	k?	В
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7707	[number]	k4	7707
<g/>
-	-	kIx~	-
<g/>
4786	[number]	k4	4786
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Etnická	etnický	k2eAgFnSc1d1	etnická
čistka	čistka	k1gFnSc1	čistka
ve	v	k7c6	v
východním	východní	k2eAgMnSc6d1	východní
Haliči	halič	k1gMnSc6	halič
</s>
</p>
<p>
<s>
Falšování	falšování	k1gNnSc1	falšování
historie	historie	k1gFnSc2	historie
genocidy	genocida	k1gFnSc2	genocida
OUN	OUN	kA	OUN
a	a	k8xC	a
UPA	UPA	kA	UPA
</s>
</p>
<p>
<s>
Genocida	genocida	k1gFnSc1	genocida
provedená	provedený	k2eAgFnSc1d1	provedená
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
nacionalisty	nacionalista	k1gMnPc7	nacionalista
proti	proti	k7c3	proti
polskému	polský	k2eAgNnSc3d1	polské
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
1939-1945	[number]	k4	1939-1945
</s>
</p>
<p>
<s>
Obrana	obrana	k1gFnSc1	obrana
vesnice	vesnice	k1gFnSc2	vesnice
Přebraz	Přebraz	k1gInSc1	Přebraz
(	(	kIx(	(
<g/>
Obrona	Obrona	k1gFnSc1	Obrona
Przebraże	Przebraże	k1gFnSc1	Przebraże
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Volyňský	volyňský	k2eAgInSc4d1	volyňský
masakr	masakr	k1gInSc4	masakr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zbrodnia	Zbrodnium	k1gNnPc1	Zbrodnium
Wołyńska	Wołyńsko	k1gNnSc2	Wołyńsko
<g/>
.	.	kIx.	.
</s>
<s>
Prawda	Prawda	k1gFnSc1	Prawda
i	i	k8xC	i
Pamięć	Pamięć	k1gFnSc1	Pamięć
(	(	kIx(	(
<g/>
Masakr	masakr	k1gInSc1	masakr
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
)	)	kIx)	)
Instytut	Instytut	k1gMnSc1	Instytut
Pamięci	Pamięce	k1gMnSc3	Pamięce
Narodowej	Narodowej	k1gInSc4	Narodowej
</s>
</p>
<p>
<s>
Wołyń	Wołyń	k?	Wołyń
naszych	naszych	k1gInSc1	naszych
przodków	przodków	k?	przodków
-	-	kIx~	-
www.nawolyniu.pl	www.nawolyniu.pnout	k5eAaPmAgInS	www.nawolyniu.pnout
(	(	kIx(	(
<g/>
staré	starý	k2eAgFnPc4d1	stará
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přežili	přežít	k5eAaPmAgMnP	přežít
masakr	masakr	k1gInSc4	masakr
...	...	k?	...
</s>
</p>
<p>
<s>
Konference	konference	k1gFnSc1	konference
Klubu	klub	k1gInSc2	klub
żołnierzy	żołnierza	k1gFnSc2	żołnierza
27	[number]	k4	27
Dywizji	Dywizj	k1gFnSc3	Dywizj
AK	AK	kA	AK
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
v	v	k7c6	v
materiálech	materiál	k1gInPc6	materiál
IPN	IPN	kA	IPN
(	(	kIx(	(
<g/>
prof.	prof.	kA	prof.
Wladyslaw	Wladyslaw	k1gMnSc1	Wladyslaw
Filar	Filar	k1gMnSc1	Filar
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Klimecki	Klimeck	k1gFnSc2	Klimeck
<g/>
,	,	kIx,	,
Zbigniew	Zbigniew	k1gFnSc2	Zbigniew
Palski	Palsk	k1gFnSc2	Palsk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ośrodek	Ośrodek	k1gInSc1	Ośrodek
Karta	karta	k1gFnSc1	karta
<g/>
.	.	kIx.	.
</s>
<s>
Databáze	databáze	k1gFnPc1	databáze
obětí	oběť	k1gFnPc2	oběť
polsko-ukrajinského	polskokrajinský	k2eAgInSc2d1	polsko-ukrajinský
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tadeusz	Tadeusz	k1gInSc1	Tadeusz
Andrzej	Andrzej	k1gMnPc2	Andrzej
Olszański	Olszańsk	k1gFnSc3	Olszańsk
Polsko-ukrajinský	polskokrajinský	k2eAgInSc4d1	polsko-ukrajinský
konflikt	konflikt	k1gInSc4	konflikt
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
(	(	kIx(	(
<g/>
internetová	internetový	k2eAgFnSc1d1	internetová
verze	verze	k1gFnSc1	verze
článku	článek	k1gInSc6	článek
publikovaném	publikovaný	k2eAgInSc6d1	publikovaný
v	v	k7c4	v
"	"	kIx"	"
<g/>
Zeszyty	Zeszyt	k1gInPc4	Zeszyt
Historyczne	Historyczne	k1gFnPc2	Historyczne
<g/>
"	"	kIx"	"
č	č	k0	č
90	[number]	k4	90
<g/>
.	.	kIx.	.
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgInSc1d1	literární
institut	institut	k1gInSc1	institut
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Jan	Jan	k1gMnSc1	Jan
Lukaszów	Lukaszów	k1gMnSc1	Lukaszów
s	s	k7c7	s
přidanou	přidaný	k2eAgFnSc7d1	přidaná
glosou	glosa	k1gFnSc7	glosa
autora	autor	k1gMnSc2	autor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Por	Por	k?	Por
Tadeusz	Tadeusz	k1gInSc1	Tadeusz
Andrzej	Andrzej	k1gFnPc2	Andrzej
Olszański	Olszańsk	k1gFnSc2	Olszańsk
Polsko-ukrajinský	polskokrajinský	k2eAgInSc4d1	polsko-ukrajinský
konflikt	konflikt	k1gInSc4	konflikt
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
Więź	Więź	k1gFnSc6	Więź
číslo	číslo	k1gNnSc1	číslo
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
(	(	kIx(	(
<g/>
397	[number]	k4	397
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
listopad-prosinec	listopadrosinec	k1gInSc1	listopad-prosinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Warszawa	Warszawa	k1gFnSc1	Warszawa
1991	[number]	k4	1991
s.	s.	k?	s.
<g/>
214	[number]	k4	214
<g/>
-	-	kIx~	-
<g/>
232	[number]	k4	232
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgInPc1d1	vybraný
materiály	materiál	k1gInPc1	materiál
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Władysława	Władysławus	k1gMnSc2	Władysławus
i	i	k8xC	i
Ewy	Ewy	k1gMnSc2	Ewy
Siemaszkó	Siemaszkó	k1gMnSc2	Siemaszkó
"	"	kIx"	"
<g/>
Ludobójstwo	Ludobójstwo	k6eAd1	Ludobójstwo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Genocida	genocida	k1gFnSc1	genocida
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgInPc1d1	vybraný
materiály	materiál	k1gInPc1	materiál
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
</s>
</p>
<p>
<s>
Plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
společný	společný	k2eAgInSc1d1	společný
list	list	k1gInSc1	list
řecko-	řecko-	k?	řecko-
a	a	k8xC	a
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
masakru	masakr	k1gInSc3	masakr
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pořad	pořad	k1gInSc4	pořad
o	o	k7c4	o
krveprolití	krveprolití	k1gNnSc4	krveprolití
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maciej	Maciej	k1gInSc1	Maciej
Ruczaj	Ruczaj	k1gInSc1	Ruczaj
<g/>
:	:	kIx,	:
Mikroapokalypsa	Mikroapokalypsa	k1gFnSc1	Mikroapokalypsa
jménem	jméno	k1gNnSc7	jméno
volyňská	volyňský	k2eAgNnPc5d1	Volyňské
řež	řezat	k5eAaImRp2nS	řezat
</s>
</p>
<p>
<s>
Komorowski	Komorowski	k6eAd1	Komorowski
připomněl	připomnět	k5eAaPmAgMnS	připomnět
masakr	masakr	k1gInSc4	masakr
na	na	k7c4	na
Volyni	Volyně	k1gFnSc4	Volyně
</s>
</p>
<p>
<s>
Bratrovražda	bratrovražda	k1gFnSc1	bratrovražda
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
strašná	strašný	k2eAgFnSc1d1	strašná
<g/>
.	.	kIx.	.
</s>
<s>
Komorowski	Komorowski	k6eAd1	Komorowski
připomněl	připomnět	k5eAaPmAgMnS	připomnět
volyňský	volyňský	k2eAgInSc4d1	volyňský
masakr	masakr	k1gInSc4	masakr
</s>
</p>
<p>
<s>
Šokující	šokující	k2eAgInPc1d1	šokující
záběry	záběr	k1gInPc1	záběr
<g/>
!	!	kIx.	!
</s>
<s>
Proč	proč	k6eAd1	proč
chtějí	chtít	k5eAaImIp3nP	chtít
volyňští	volyňský	k2eAgMnPc1d1	volyňský
Češi	Čech	k1gMnPc1	Čech
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
zpět	zpět	k6eAd1	zpět
domů	dům	k1gInPc2	dům
<g/>
?	?	kIx.	?
</s>
<s>
Podívejte	podívat	k5eAaPmRp2nP	podívat
se	se	k3xPyFc4	se
na	na	k7c4	na
fotky	fotka	k1gFnPc4	fotka
a	a	k8xC	a
pochopíte	pochopit	k5eAaPmIp2nP	pochopit
</s>
</p>
<p>
<s>
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
parlament	parlament	k1gInSc1	parlament
schválil	schválit	k5eAaPmAgInS	schválit
uznání	uznání	k1gNnSc4	uznání
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
organizace	organizace	k1gFnSc2	organizace
UPA	UPA	kA	UPA
</s>
</p>
