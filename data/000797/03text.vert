<s>
Ariel	Ariel	k1gInSc1	Ariel
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gMnSc1	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
Williamem	William	k1gInSc7	William
Lassellem	Lassell	k1gMnSc7	Lassell
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgInSc1d2	podrobnější
průzkum	průzkum	k1gInSc1	průzkum
a	a	k8xC	a
snímky	snímek	k1gInPc1	snímek
měsíce	měsíc	k1gInSc2	měsíc
pořídila	pořídit	k5eAaPmAgFnS	pořídit
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proletěla	proletět	k5eAaPmAgFnS	proletět
127	[number]	k4	127
000	[number]	k4	000
km	km	kA	km
od	od	k7c2	od
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
největších	veliký	k2eAgInPc2d3	veliký
Uranových	Uranův	k2eAgInPc2d1	Uranův
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
měsíci	měsíc	k1gInSc3	měsíc
Oberon	Oberon	k1gInSc1	Oberon
<g/>
,	,	kIx,	,
Umbriel	Umbriel	k1gInSc1	Umbriel
a	a	k8xC	a
Titania	Titanium	k1gNnSc2	Titanium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
191	[number]	k4	191
020	[number]	k4	020
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
155,4	[number]	k4	155,4
km	km	kA	km
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
cca	cca	kA	cca
1,35	[number]	k4	1,35
<g/>
×	×	k?	×
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}}	}}	k?	}}
kg	kg	kA	kg
<g/>
,	,	kIx,	,
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
2,520	[number]	k4	2,520
<g/>
4	[number]	k4	4
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
Uranovy	Uranův	k2eAgInPc4d1	Uranův
měsíce	měsíc	k1gInPc4	měsíc
nese	nést	k5eAaImIp3nS	nést
Ariel	Ariel	k1gInSc1	Ariel
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
díla	dílo	k1gNnSc2	dílo
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
podle	podle	k7c2	podle
postavy	postava	k1gFnSc2	postava
ducha	duch	k1gMnSc2	duch
z	z	k7c2	z
dramatu	drama	k1gNnSc2	drama
Bouře	bouř	k1gFnSc2	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Ariel	Ariel	k1gInSc1	Ariel
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
50	[number]	k4	50
%	%	kIx~	%
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
z	z	k7c2	z
30	[number]	k4	30
%	%	kIx~	%
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
20	[number]	k4	20
%	%	kIx~	%
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
metanový	metanový	k2eAgInSc4d1	metanový
led	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
dominují	dominovat	k5eAaImIp3nP	dominovat
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vypadají	vypadat	k5eAaPmIp3nP	vypadat
jako	jako	k9	jako
čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
námraza	námraza	k1gFnSc1	námraza
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
nejsvětlejší	světlý	k2eAgInSc4d3	nejsvětlejší
povrch	povrch	k1gInSc4	povrch
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
zblízka	zblízka	k6eAd1	zblízka
fotografovaných	fotografovaný	k2eAgInPc2d1	fotografovaný
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gMnSc1	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
krátery	kráter	k1gInPc1	kráter
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
větším	veliký	k2eAgInSc7d2	veliký
než	než	k8xS	než
50	[number]	k4	50
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
bouřlivá	bouřlivý	k2eAgFnSc1d1	bouřlivá
geologická	geologický	k2eAgFnSc1d1	geologická
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zanechala	zanechat	k5eAaPmAgFnS	zanechat
stopy	stopa	k1gFnPc4	stopa
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zlomových	zlomový	k2eAgInPc2d1	zlomový
kaňonů	kaňon	k1gInPc2	kaňon
<g/>
,	,	kIx,	,
zaplavených	zaplavený	k2eAgInPc2d1	zaplavený
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kaňony	kaňon	k1gInPc1	kaňon
přetínají	přetínat	k5eAaImIp3nP	přetínat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
hladký	hladký	k2eAgInSc4d1	hladký
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyplněný	vyplněný	k2eAgInSc1d1	vyplněný
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
