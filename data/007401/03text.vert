<s>
Vetřelci	vetřelec	k1gMnPc1	vetřelec
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Aliens	Aliens	k1gInSc1	Aliens
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
sci-fi	scii	k1gFnPc7	sci-fi
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
jako	jako	k8xC	jako
pokračování	pokračování	k1gNnSc1	pokračování
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
filmu	film	k1gInSc2	film
Vetřelec	vetřelec	k1gMnSc1	vetřelec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
Ellen	Ellen	k1gInSc1	Ellen
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
LV-	LV-	k1gFnSc2	LV-
<g/>
426	[number]	k4	426
<g/>
,	,	kIx,	,
okupovanou	okupovaný	k2eAgFnSc4d1	okupovaná
vetřelci	vetřelec	k1gMnPc7	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
rozpočet	rozpočet	k1gInSc1	rozpočet
pro	pro	k7c4	pro
natočení	natočení	k1gNnSc4	natočení
filmu	film	k1gInSc2	film
činil	činit	k5eAaImAgMnS	činit
cca	cca	kA	cca
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
snímek	snímek	k1gInSc1	snímek
natáčen	natáčet	k5eAaImNgInS	natáčet
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
odstavené	odstavený	k2eAgFnSc6d1	odstavená
elektrárně	elektrárna	k1gFnSc6	elektrárna
a	a	k8xC	a
ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
společnosti	společnost	k1gFnSc2	společnost
Pinewood	Pinewood	k1gInSc1	Pinewood
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Vetřelci	vetřelec	k1gMnPc1	vetřelec
byli	být	k5eAaImAgMnP	být
komerčně	komerčně	k6eAd1	komerčně
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jenom	jenom	k9	jenom
v	v	k7c6	v
USA	USA	kA	USA
film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
$	$	kIx~	$
<g/>
86	[number]	k4	86
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zisk	zisk	k1gInSc1	zisk
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
na	na	k7c6	na
$	$	kIx~	$
<g/>
131	[number]	k4	131
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
7	[number]	k4	7
Oscarů	Oscar	k1gMnPc2	Oscar
včetně	včetně	k7c2	včetně
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
ženského	ženský	k2eAgInSc2d1	ženský
hereckého	herecký	k2eAgInSc2d1	herecký
výkonu	výkon	k1gInSc2	výkon
pro	pro	k7c4	pro
Sigourney	Sigourne	k1gMnPc4	Sigourne
Weaverovou	Weaverová	k1gFnSc4	Weaverová
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
nakonec	nakonec	k6eAd1	nakonec
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
zvukových	zvukový	k2eAgInPc2d1	zvukový
efektů	efekt	k1gInPc2	efekt
a	a	k8xC	a
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Ellen	Ellen	k1gInSc1	Ellen
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
(	(	kIx(	(
<g/>
Sigourney	Sigourney	k1gInPc1	Sigourney
Weaver	Weavra	k1gFnPc2	Weavra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
přeživší	přeživší	k2eAgFnSc1d1	přeživší
z	z	k7c2	z
posádky	posádka	k1gFnSc2	posádka
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Nostromo	Nostroma	k1gFnSc5	Nostroma
byla	být	k5eAaImAgNnP	být
nalezena	nalézt	k5eAaBmNgNnP	nalézt
v	v	k7c6	v
hibernačním	hibernační	k2eAgInSc6d1	hibernační
spánku	spánek	k1gInSc6	spánek
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
únikovém	únikový	k2eAgInSc6d1	únikový
modulu	modul	k1gInSc6	modul
po	po	k7c6	po
57	[number]	k4	57
letech	léto	k1gNnPc6	léto
bloudění	bloudění	k1gNnSc2	bloudění
vesmírem	vesmír	k1gInSc7	vesmír
a	a	k8xC	a
se	s	k7c7	s
smutkem	smutek	k1gInSc7	smutek
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
Amanda	Amanda	k1gFnSc1	Amanda
zatím	zatím	k6eAd1	zatím
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
<g/>
,	,	kIx,	,
zestárla	zestárnout	k5eAaPmAgFnS	zestárnout
a	a	k8xC	a
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
společnosti	společnost	k1gFnSc2	společnost
Weyland-Yutani	Weyland-Yutan	k1gMnPc1	Weyland-Yutan
je	on	k3xPp3gFnPc4	on
velmi	velmi	k6eAd1	velmi
skeptická	skeptický	k2eAgFnSc1d1	skeptická
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
výkladu	výklad	k1gInSc3	výklad
o	o	k7c6	o
důvodech	důvod	k1gInPc6	důvod
zničení	zničení	k1gNnSc1	zničení
kosmické	kosmický	k2eAgFnSc2d1	kosmická
nákladní	nákladní	k2eAgFnSc2d1	nákladní
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
vetřelce	vetřelec	k1gMnSc2	vetřelec
oficiálně	oficiálně	k6eAd1	oficiálně
nevěří	věřit	k5eNaImIp3nS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
LV-426	LV-426	k1gFnSc2	LV-426
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
terraformována	terraformován	k2eAgFnSc1d1	terraformován
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
tam	tam	k6eAd1	tam
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
rodin	rodina	k1gFnPc2	rodina
kolonistů	kolonista	k1gMnPc2	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
tak	tak	k9	tak
přijde	přijít	k5eAaPmIp3nS	přijít
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
licenci	licence	k1gFnSc4	licence
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
docích	dok	k1gInPc6	dok
jako	jako	k8xC	jako
řidička	řidička	k1gFnSc1	řidička
nakladačů	nakladač	k1gMnPc2	nakladač
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
navštívena	navštíven	k2eAgFnSc1d1	navštívena
zástupcem	zástupce	k1gMnSc7	zástupce
společnosti	společnost	k1gFnPc1	společnost
Weyland-Yutani	Weyland-Yutaň	k1gFnSc3	Weyland-Yutaň
<g/>
,	,	kIx,	,
Carterem	Carter	k1gMnSc7	Carter
Burkem	Burek	k1gMnSc7	Burek
a	a	k8xC	a
důstojníkem	důstojník	k1gMnSc7	důstojník
USCM	USCM	kA	USCM
(	(	kIx(	(
<g/>
US	US	kA	US
Colonial	Colonial	k1gMnSc1	Colonial
Marines	Marines	k1gMnSc1	Marines
<g/>
)	)	kIx)	)
Gormanem	Gorman	k1gMnSc7	Gorman
<g/>
.	.	kIx.	.
</s>
<s>
Oznámí	oznámit	k5eAaPmIp3nP	oznámit
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
ztratila	ztratit	k5eAaPmAgFnS	ztratit
s	s	k7c7	s
kolonií	kolonie	k1gFnSc7	kolonie
na	na	k7c4	na
LV-426	LV-426	k1gFnSc4	LV-426
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
kontakt	kontakt	k1gInSc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Ripleyovou	Ripleyovat	k5eAaPmIp3nP	Ripleyovat
to	ten	k3xDgNnSc1	ten
moc	moc	k6eAd1	moc
nepřekvapí	překvapit	k5eNaPmIp3nS	překvapit
a	a	k8xC	a
spíše	spíše	k9	spíše
je	být	k5eAaImIp3nS	být
udivená	udivený	k2eAgFnSc1d1	udivená
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
chtějí	chtít	k5eAaImIp3nP	chtít
letět	letět	k5eAaImF	letět
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
vzít	vzít	k5eAaPmF	vzít
jako	jako	k9	jako
poradce	poradce	k1gMnSc1	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
nejprve	nejprve	k6eAd1	nejprve
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
probouzí	probouzet	k5eAaImIp3nP	probouzet
s	s	k7c7	s
nočními	noční	k2eAgFnPc7d1	noční
můrami	můra	k1gFnPc7	můra
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
překonat	překonat	k5eAaPmF	překonat
svůj	svůj	k3xOyFgInSc4	svůj
strach	strach	k1gInSc4	strach
a	a	k8xC	a
letět	letět	k5eAaImF	letět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
LV-426	LV-426	k1gMnPc2	LV-426
míří	mířit	k5eAaImIp3nS	mířit
loď	loď	k1gFnSc4	loď
Sulaco	Sulaco	k6eAd1	Sulaco
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
dosažitelná	dosažitelný	k2eAgFnSc1d1	dosažitelná
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Ripleyové	Ripleyová	k1gFnSc2	Ripleyová
<g/>
,	,	kIx,	,
Gormana	Gorman	k1gMnSc2	Gorman
a	a	k8xC	a
Burka	burka	k1gFnSc1	burka
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
ještě	ještě	k9	ještě
seržant	seržant	k1gMnSc1	seržant
Apone	Apon	k1gInSc5	Apon
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
Matthews	Matthews	k1gInSc1	Matthews
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
desátník	desátník	k1gMnSc1	desátník
Hicks	Hicks	k1gInSc1	Hicks
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Biehn	Biehn	k1gMnSc1	Biehn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojíni	vojín	k1gMnPc1	vojín
Vasquezová	Vasquezový	k2eAgFnSc1d1	Vasquezový
(	(	kIx(	(
<g/>
Jenette	Jenett	k1gInSc5	Jenett
Goldstein	Goldstein	k2eAgMnSc1d1	Goldstein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hudson	Hudson	k1gMnSc1	Hudson
(	(	kIx(	(
<g/>
Bill	Bill	k1gMnSc1	Bill
Paxton	Paxton	k1gInSc1	Paxton
<g/>
)	)	kIx)	)
a	a	k8xC	a
android	android	k1gInSc1	android
Bishop	Bishop	k1gInSc1	Bishop
(	(	kIx(	(
<g/>
Lance	lance	k1gNnSc1	lance
Henriksen	Henriksna	k1gFnPc2	Henriksna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Bishopovi	Bishop	k1gMnSc3	Bishop
chová	chovat	k5eAaImIp3nS	chovat
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
<g/>
,	,	kIx,	,
po	po	k7c6	po
zkušenostech	zkušenost	k1gFnPc6	zkušenost
s	s	k7c7	s
androidem	android	k1gInSc7	android
Ashem	Ashem	k1gInSc1	Ashem
na	na	k7c6	na
Nostromu	Nostrom	k1gInSc6	Nostrom
<g/>
,	,	kIx,	,
značnou	značný	k2eAgFnSc4d1	značná
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
doráží	dorážet	k5eAaImIp3nS	dorážet
do	do	k7c2	do
kolonie	kolonie	k1gFnSc2	kolonie
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
opuštěná	opuštěný	k2eAgFnSc1d1	opuštěná
a	a	k8xC	a
značně	značně	k6eAd1	značně
poškozená	poškozený	k2eAgFnSc1d1	poškozená
<g/>
.	.	kIx.	.
</s>
<s>
Nikde	nikde	k6eAd1	nikde
nejsou	být	k5eNaImIp3nP	být
známky	známka	k1gFnPc4	známka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
žádná	žádný	k3yNgNnPc4	žádný
těla	tělo	k1gNnPc4	tělo
mrtvých	mrtvý	k1gMnPc2	mrtvý
nebo	nebo	k8xC	nebo
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
známky	známka	k1gFnPc4	známka
boje	boj	k1gInSc2	boj
a	a	k8xC	a
vyleptané	vyleptaný	k2eAgFnSc2d1	vyleptaná
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
opevní	opevnit	k5eAaPmIp3nS	opevnit
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
řídících	řídící	k2eAgInPc2d1	řídící
komplexů	komplex	k1gInPc2	komplex
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
dvojici	dvojice	k1gFnSc4	dvojice
živých	živý	k2eAgInPc2d1	živý
Facehuggerů	Facehugger	k1gInPc2	Facehugger
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
z	z	k7c2	z
hostitelů	hostitel	k1gMnPc2	hostitel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zákrok	zákrok	k1gInSc4	zákrok
nepřežili	přežít	k5eNaPmAgMnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Týmu	tým	k1gInSc2	tým
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
objevit	objevit	k5eAaPmF	objevit
traumatizovanou	traumatizovaný	k2eAgFnSc4d1	traumatizovaná
mladou	mladý	k2eAgFnSc4d1	mladá
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
přezdívanou	přezdívaný	k2eAgFnSc7d1	přezdívaná
Newt	Newt	k1gInSc4	Newt
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
z	z	k7c2	z
kolonie	kolonie	k1gFnSc2	kolonie
jsou	být	k5eAaImIp3nP	být
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prozkoumávání	prozkoumávání	k1gNnSc6	prozkoumávání
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
kolonistů	kolonista	k1gMnPc2	kolonista
je	být	k5eAaImIp3nS	být
pohromadě	pohromadě	k6eAd1	pohromadě
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
komplexu	komplex	k1gInSc2	komplex
-	-	kIx~	-
v	v	k7c6	v
generátoru	generátor	k1gInSc6	generátor
atmosféry	atmosféra	k1gFnSc2	atmosféra
terraformačního	terraformační	k2eAgNnSc2d1	terraformační
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
skupina	skupina	k1gFnSc1	skupina
kosmické	kosmický	k2eAgFnSc2d1	kosmická
pěchoty	pěchota	k1gFnSc2	pěchota
(	(	kIx(	(
<g/>
USCM	USCM	kA	USCM
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgMnPc4	první
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
obaleni	obalit	k5eAaPmNgMnP	obalit
v	v	k7c6	v
kokonech	kokon	k1gInPc6	kokon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
moment	moment	k1gInSc4	moment
začínají	začínat	k5eAaImIp3nP	začínat
útočit	útočit	k5eAaImF	útočit
vetřelci	vetřelec	k1gMnPc1	vetřelec
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
zahyne	zahynout	k5eAaPmIp3nS	zahynout
polovina	polovina	k1gFnSc1	polovina
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zuřivém	zuřivý	k2eAgInSc6d1	zuřivý
boji	boj	k1gInSc6	boj
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
chladicího	chladicí	k2eAgNnSc2d1	chladicí
zařízení	zařízení	k1gNnSc2	zařízení
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
generátoru	generátor	k1gInSc2	generátor
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
filmu	film	k1gInSc6	film
důležitá	důležitý	k2eAgFnSc1d1	důležitá
zápletka	zápletka	k1gFnSc1	zápletka
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
Burkeho	Burke	k1gMnSc4	Burke
je	být	k5eAaImIp3nS	být
rozhodnuta	rozhodnut	k2eAgFnSc1d1	rozhodnuta
opustit	opustit	k5eAaPmF	opustit
neprodleně	prodleně	k6eNd1	prodleně
LV-426	LV-426	k1gFnSc4	LV-426
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
základnu	základna	k1gFnSc4	základna
z	z	k7c2	z
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
přivoláni	přivolán	k2eAgMnPc1d1	přivolán
zbylí	zbylý	k2eAgMnPc1d1	zbylý
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
s	s	k7c7	s
výsadkovým	výsadkový	k2eAgInSc7d1	výsadkový
modulem	modul	k1gInSc7	modul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přistál	přistát	k5eAaImAgInS	přistát
mimo	mimo	k7c4	mimo
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
infiltrován	infiltrován	k2eAgMnSc1d1	infiltrován
vetřelcem	vetřelec	k1gMnSc7	vetřelec
a	a	k8xC	a
tak	tak	k6eAd1	tak
při	při	k7c6	při
příletu	přílet	k1gInSc6	přílet
havaruje	havarovat	k5eAaPmIp3nS	havarovat
<g/>
.	.	kIx.	.
</s>
<s>
Případnou	případný	k2eAgFnSc4d1	případná
pomoc	pomoc	k1gFnSc4	pomoc
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
očekávat	očekávat	k5eAaImF	očekávat
až	až	k9	až
za	za	k7c4	za
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
nebo	nebo	k8xC	nebo
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
kromě	kromě	k7c2	kromě
vetřelců	vetřelec	k1gMnPc2	vetřelec
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
problém	problém	k1gInSc1	problém
se	s	k7c7	s
stabilitou	stabilita	k1gFnSc7	stabilita
terraformačního	terraformační	k2eAgNnSc2d1	terraformační
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
musí	muset	k5eAaImIp3nP	muset
explodovat	explodovat	k5eAaBmF	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
komunikační	komunikační	k2eAgNnSc1d1	komunikační
zařízení	zařízení	k1gNnSc1	zařízení
poničeno	poničen	k2eAgNnSc1d1	poničeno
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
Bishop	Bishop	k1gInSc1	Bishop
k	k	k7c3	k
centrálnímu	centrální	k2eAgInSc3d1	centrální
řídícímu	řídící	k2eAgInSc3d1	řídící
panelu	panel	k1gInSc3	panel
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
může	moct	k5eAaImIp3nS	moct
navést	navést	k5eAaPmF	navést
druhý	druhý	k4xOgInSc4	druhý
výsadkový	výsadkový	k2eAgInSc4d1	výsadkový
modul	modul	k1gInSc4	modul
z	z	k7c2	z
lodi	loď	k1gFnSc2	loď
Sulaco	Sulaco	k6eAd1	Sulaco
z	z	k7c2	z
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
zbytek	zbytek	k1gInSc1	zbytek
skupiny	skupina	k1gFnSc2	skupina
opevňuje	opevňovat	k5eAaImIp3nS	opevňovat
v	v	k7c6	v
části	část	k1gFnSc6	část
komplexu	komplex	k1gInSc2	komplex
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
neustálému	neustálý	k2eAgInSc3d1	neustálý
náporu	nápor	k1gInSc3	nápor
desítek	desítka	k1gFnPc2	desítka
vetřelců	vetřelec	k1gMnPc2	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Impresivní	impresivní	k2eAgInSc1d1	impresivní
je	být	k5eAaImIp3nS	být
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
počítadla	počítadlo	k1gNnPc4	počítadlo
střeliva	střelivo	k1gNnSc2	střelivo
automatických	automatický	k2eAgMnPc2d1	automatický
strážců	strážce	k1gMnPc2	strážce
-	-	kIx~	-
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
verzích	verze	k1gFnPc6	verze
vystřižen	vystřižen	k2eAgInSc1d1	vystřižen
<g/>
.	.	kIx.	.
</s>
<s>
Burke	Burke	k6eAd1	Burke
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
infikovat	infikovat	k5eAaBmF	infikovat
Ripleyovou	Ripleyová	k1gFnSc4	Ripleyová
a	a	k8xC	a
Newt	Newt	k2eAgInSc1d1	Newt
vetřelci	vetřelec	k1gMnPc7	vetřelec
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
jim	on	k3xPp3gMnPc3	on
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spí	spát	k5eAaImIp3nP	spát
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
facehuggery	facehugger	k1gInPc4	facehugger
<g/>
.	.	kIx.	.
</s>
<s>
Vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Burke	Burke	k1gInSc1	Burke
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgNnSc4d1	podobné
tajné	tajný	k2eAgNnSc4d1	tajné
poslání	poslání	k1gNnSc4	poslání
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc4	jaký
měl	mít	k5eAaImAgMnS	mít
Ash	Ash	k1gMnSc1	Ash
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
dílu	díl	k1gInSc6	díl
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nejen	nejen	k6eAd1	nejen
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
o	o	k7c6	o
vetřelci	vetřelec	k1gMnSc6	vetřelec
nejspíš	nejspíš	k9	nejspíš
věděla	vědět	k5eAaImAgFnS	vědět
a	a	k8xC	a
čekala	čekat	k5eAaImAgFnS	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jimi	on	k3xPp3gInPc7	on
budou	být	k5eAaImBp3nP	být
kolonisté	kolonista	k1gMnPc1	kolonista
napadeni	napaden	k2eAgMnPc1d1	napaden
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Burke	Burke	k1gInSc1	Burke
je	být	k5eAaImIp3nS	být
však	však	k9	však
zabit	zabít	k5eAaPmNgMnS	zabít
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vetřelců	vetřelec	k1gMnPc2	vetřelec
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečná	bezpečný	k2eNgNnPc1d1	nebezpečné
monstra	monstrum	k1gNnPc1	monstrum
však	však	k9	však
nachází	nacházet	k5eAaImIp3nP	nacházet
cestu	cesta	k1gFnSc4	cesta
mimo	mimo	k7c4	mimo
opevněné	opevněný	k2eAgFnPc4d1	opevněná
chodby	chodba	k1gFnPc4	chodba
i	i	k8xC	i
zavařené	zavařený	k2eAgFnPc4d1	zavařená
dveře	dveře	k1gFnPc4	dveře
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
přeživších	přeživší	k2eAgFnPc2d1	přeživší
musí	muset	k5eAaImIp3nP	muset
z	z	k7c2	z
komplexu	komplex	k1gInSc2	komplex
utéci	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
zahyne	zahynout	k5eAaPmIp3nS	zahynout
Vasquezová	Vasquezová	k1gFnSc1	Vasquezová
a	a	k8xC	a
Gorman	Gorman	k1gMnSc1	Gorman
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
s	s	k7c7	s
Hicksem	Hicks	k1gInSc7	Hicks
dostávají	dostávat	k5eAaImIp3nP	dostávat
za	za	k7c7	za
Bishopem	Bishop	k1gInSc7	Bishop
k	k	k7c3	k
modulu	modul	k1gInSc3	modul
<g/>
,	,	kIx,	,
Newt	Newt	k1gInSc4	Newt
unesou	unést	k5eAaPmIp3nP	unést
vetřelci	vetřelec	k1gMnPc1	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Newt	Newt	k1gInSc4	Newt
ihned	ihned	k6eAd1	ihned
nezabijí	zabít	k5eNaPmIp3nP	zabít
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
jí	on	k3xPp3gFnSc3	on
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
zárodek	zárodek	k1gInSc4	zárodek
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
vysadit	vysadit	k5eAaPmF	vysadit
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
kolonie	kolonie	k1gFnSc2	kolonie
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
hledat	hledat	k5eAaImF	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Vybavena	vybaven	k2eAgFnSc1d1	vybavena
plamenometem	plamenomet	k1gInSc7	plamenomet
<g/>
,	,	kIx,	,
pulzní	pulzní	k2eAgFnSc7d1	pulzní
puškou	puška	k1gFnSc7	puška
M41A	M41A	k1gFnSc2	M41A
nachází	nacházet	k5eAaImIp3nS	nacházet
Newt	Newt	k1gInSc4	Newt
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
plné	plný	k2eAgFnSc6d1	plná
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
s	s	k7c7	s
Newt	Newta	k1gFnPc2	Newta
zahlédne	zahlédnout	k5eAaPmIp3nS	zahlédnout
královnu	královna	k1gFnSc4	královna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
klade	klást	k5eAaImIp3nS	klást
vejce	vejce	k1gNnSc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
vajec	vejce	k1gNnPc2	vejce
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
rozzuřená	rozzuřený	k2eAgFnSc1d1	rozzuřená
královna	královna	k1gFnSc1	královna
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
stihne	stihnout	k5eAaPmIp3nS	stihnout
naskočit	naskočit	k5eAaPmF	naskočit
do	do	k7c2	do
modulu	modul	k1gInSc2	modul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemohl	moct	k5eNaImAgInS	moct
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c6	na
nestabilní	stabilní	k2eNgFnSc6d1	nestabilní
plošině	plošina	k1gFnSc6	plošina
kolabujícího	kolabující	k2eAgInSc2d1	kolabující
komplexu	komplex	k1gInSc2	komplex
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vzdálil	vzdálit	k5eAaPmAgInS	vzdálit
–	–	k?	–
Ellen	Ellen	k1gInSc1	Ellen
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
diváky	divák	k1gMnPc7	divák
chvíli	chvíle	k1gFnSc4	chvíle
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
zradil	zradit	k5eAaPmAgInS	zradit
další	další	k2eAgInSc1d1	další
android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vystoupení	vystoupení	k1gNnSc6	vystoupení
na	na	k7c6	na
Sulacu	Sulacum	k1gNnSc6	Sulacum
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
po	po	k7c6	po
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
najednou	najednou	k6eAd1	najednou
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
zachytit	zachytit	k5eAaPmF	zachytit
za	za	k7c4	za
modul	modul	k1gInSc4	modul
<g/>
,	,	kIx,	,
když	když	k8xS	když
odlétal	odlétat	k5eAaPmAgMnS	odlétat
z	z	k7c2	z
LV-	LV-	k1gFnSc2	LV-
<g/>
426	[number]	k4	426
<g/>
.	.	kIx.	.
</s>
<s>
Roztrhne	roztrhnout	k5eAaPmIp3nS	roztrhnout
Bishopa	Bishopa	k1gFnSc1	Bishopa
<g/>
,	,	kIx,	,
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
unikne	uniknout	k5eAaPmIp3nS	uniknout
do	do	k7c2	do
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
nákladové	nákladový	k2eAgFnSc2d1	nákladová
místnosti	místnost	k1gFnSc2	místnost
a	a	k8xC	a
Newt	Newt	k1gInSc1	Newt
se	se	k3xPyFc4	se
schová	schovat	k5eAaPmIp3nS	schovat
pod	pod	k7c4	pod
mříže	mříž	k1gFnPc4	mříž
<g/>
.	.	kIx.	.
</s>
<s>
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
nakládací	nakládací	k2eAgInSc4d1	nakládací
hydraulický	hydraulický	k2eAgInSc4d1	hydraulický
exoskeleton	exoskeleton	k1gInSc4	exoskeleton
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
postavit	postavit	k5eAaPmF	postavit
obrovské	obrovský	k2eAgFnSc3d1	obrovská
královně	královna	k1gFnSc3	královna
<g/>
.	.	kIx.	.
</s>
<s>
Souboj	souboj	k1gInSc1	souboj
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
kultovní	kultovní	k2eAgFnSc7d1	kultovní
scénou	scéna	k1gFnSc7	scéna
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Královnu	královna	k1gFnSc4	královna
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nakonec	nakonec	k6eAd1	nakonec
podaří	podařit	k5eAaPmIp3nS	podařit
shodit	shodit	k5eAaPmF	shodit
do	do	k7c2	do
přetlakové	přetlakový	k2eAgFnSc2d1	přetlaková
komory	komora	k1gFnSc2	komora
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
dramatické	dramatický	k2eAgFnSc6d1	dramatická
scéně	scéna	k1gFnSc6	scéna
vyfouknout	vyfouknout	k5eAaPmF	vyfouknout
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
<g/>
,	,	kIx,	,
Newt	Newt	k1gInSc1	Newt
<g/>
,	,	kIx,	,
Hicks	Hicks	k1gInSc1	Hicks
i	i	k8xC	i
Bishop	Bishop	k1gInSc1	Bishop
ukládají	ukládat	k5eAaImIp3nP	ukládat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
hibernačního	hibernační	k2eAgInSc2d1	hibernační
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kompletaci	kompletace	k1gFnSc6	kompletace
předprodukce	předprodukce	k1gFnSc2	předprodukce
filmu	film	k1gInSc2	film
Terminátor	terminátor	k1gInSc1	terminátor
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
možnost	možnost	k1gFnSc4	možnost
tvorby	tvorba	k1gFnSc2	tvorba
dalšího	další	k2eAgInSc2d1	další
dílu	díl	k1gInSc2	díl
Vetřelce	vetřelec	k1gMnSc2	vetřelec
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
s	s	k7c7	s
produkčním	produkční	k2eAgMnSc7d1	produkční
Davidem	David	k1gMnSc7	David
Gilerem	Giler	k1gMnSc7	Giler
<g/>
.	.	kIx.	.
</s>
<s>
Cameron	Cameron	k1gInSc1	Cameron
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
fanoušek	fanoušek	k1gMnSc1	fanoušek
filmu	film	k1gInSc2	film
Vetřelec	vetřelec	k1gMnSc1	vetřelec
chtěl	chtít	k5eAaImAgMnS	chtít
dát	dát	k5eAaPmF	dát
dohromady	dohromady	k6eAd1	dohromady
pokračování	pokračování	k1gNnPc4	pokračování
a	a	k8xC	a
tak	tak	k6eAd1	tak
po	po	k7c6	po
4	[number]	k4	4
dnech	den	k1gInPc6	den
přeložil	přeložit	k5eAaPmAgInS	přeložit
společnosti	společnost	k1gFnSc2	společnost
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
čtyřicetistránkový	čtyřicetistránkový	k2eAgInSc1d1	čtyřicetistránkový
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
náčrt	náčrt	k1gInSc1	náčrt
filmu	film	k1gInSc2	film
Alien	Alina	k1gFnPc2	Alina
II	II	kA	II
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
schválení	schválení	k1gNnSc3	schválení
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
prý	prý	k9	prý
nevyprodukoval	vyprodukovat	k5eNaPmAgInS	vyprodukovat
takový	takový	k3xDgInSc4	takový
zisk	zisk	k1gInSc4	zisk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
natáčelo	natáčet	k5eAaImAgNnS	natáčet
pokračování	pokračování	k1gNnSc1	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
9	[number]	k4	9
<g/>
měsíčnímu	měsíční	k2eAgInSc3d1	měsíční
zpoždění	zpoždění	k1gNnSc4	zpoždění
Terminátora	terminátor	k1gMnSc2	terminátor
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
Schwarzeneggerovo	Schwarzeneggerův	k2eAgNnSc1d1	Schwarzeneggerovo
natáčení	natáčení	k1gNnSc1	natáčení
filmu	film	k1gInSc2	film
Ničitel	ničitel	k1gMnSc1	ničitel
Conan	Conan	k1gMnSc1	Conan
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
Cameron	Cameron	k1gMnSc1	Cameron
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
na	na	k7c4	na
vypracování	vypracování	k1gNnSc4	vypracování
svého	svůj	k3xOyFgInSc2	svůj
scénáře	scénář	k1gInSc2	scénář
filmu	film	k1gInSc2	film
Aliens	Aliensa	k1gFnPc2	Aliensa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
Terminátora	terminátor	k1gMnSc4	terminátor
dal	dát	k5eAaPmAgInS	dát
Cameron	Cameron	k1gInSc1	Cameron
dohromady	dohromady	k6eAd1	dohromady
cca	cca	kA	cca
devadesát	devadesát	k4xCc1	devadesát
stránek	stránka	k1gFnPc2	stránka
Vetřelců	vetřelec	k1gMnPc2	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
filmové	filmový	k2eAgFnSc2d1	filmová
společnosti	společnost	k1gFnSc2	společnost
byli	být	k5eAaImAgMnP	být
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
a	a	k8xC	a
slíbili	slíbit	k5eAaPmAgMnP	slíbit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
Terminátor	terminátor	k1gInSc1	terminátor
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
režírovat	režírovat	k5eAaImF	režírovat
Vetřelce	vetřelec	k1gMnSc4	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Terminátor	terminátor	k1gInSc1	terminátor
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
byl	být	k5eAaImAgInS	být
a	a	k8xC	a
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgInS	dostat
Cameron	Cameron	k1gInSc1	Cameron
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Gale	Gale	k1gMnSc1	Gale
Anne	Anne	k1gFnPc2	Anne
Hurd	hurda	k1gFnPc2	hurda
povolení	povolení	k1gNnSc2	povolení
natočit	natočit	k5eAaBmF	natočit
pokračování	pokračování	k1gNnSc4	pokračování
filmu	film	k1gInSc2	film
Vetřelec	vetřelec	k1gMnSc1	vetřelec
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Camerona	Cameron	k1gMnSc4	Cameron
lákalo	lákat	k5eAaImAgNnS	lákat
vytvořit	vytvořit	k5eAaPmF	vytvořit
trochu	trochu	k6eAd1	trochu
jiný	jiný	k2eAgInSc4d1	jiný
svět	svět	k1gInSc4	svět
vetřelce	vetřelec	k1gMnSc2	vetřelec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nedržel	držet	k5eNaImAgInS	držet
přesných	přesný	k2eAgInPc2d1	přesný
konceptů	koncept	k1gInPc2	koncept
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
více	hodně	k6eAd2	hodně
o	o	k7c6	o
boji	boj	k1gInSc6	boj
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Více	hodně	k6eAd2	hodně
teroru	teror	k1gInSc2	teror
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
hororu	horor	k1gInSc2	horor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sigourney	Sigourney	k1gInPc1	Sigourney
Weaver	Weavra	k1gFnPc2	Weavra
o	o	k7c6	o
celém	celý	k2eAgInSc6d1	celý
projektu	projekt	k1gInSc6	projekt
zpočátku	zpočátku	k6eAd1	zpočátku
pochybovala	pochybovat	k5eAaImAgFnS	pochybovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
Cameronem	Cameron	k1gInSc7	Cameron
jí	on	k3xPp3gFnSc2	on
přesvědčilo	přesvědčit	k5eAaPmAgNnS	přesvědčit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc4	fox
ovšem	ovšem	k9	ovšem
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
smlouvu	smlouva	k1gFnSc4	smlouva
kvůli	kvůli	k7c3	kvůli
financím	finance	k1gFnPc3	finance
a	a	k8xC	a
obrátila	obrátit	k5eAaPmAgFnS	obrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
Camerona	Cameron	k1gMnSc4	Cameron
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
nemůže	moct	k5eNaImIp3nS	moct
sepsat	sepsat	k5eAaPmF	sepsat
scénář	scénář	k1gInSc1	scénář
bez	bez	k7c2	bez
postavy	postava	k1gFnSc2	postava
Ripleyové	Ripleyová	k1gFnSc2	Ripleyová
<g/>
.	.	kIx.	.
</s>
<s>
Cameron	Cameron	k1gMnSc1	Cameron
to	ten	k3xDgNnSc4	ten
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
vytrvalosti	vytrvalost	k1gFnSc3	vytrvalost
nakonec	nakonec	k9	nakonec
společnost	společnost	k1gFnSc4	společnost
smlouvu	smlouva	k1gFnSc4	smlouva
se	se	k3xPyFc4	se
Sigourney	Sigournea	k1gFnSc2	Sigournea
Weaver	Weavra	k1gFnPc2	Weavra
podepsala	podepsat	k5eAaPmAgFnS	podepsat
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
odměnu	odměna	k1gFnSc4	odměna
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
30	[number]	k4	30
<g/>
x	x	k?	x
více	hodně	k6eAd2	hodně
oproti	oproti	k7c3	oproti
prvnímu	první	k4xOgInSc3	první
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
edice	edice	k1gFnSc1	edice
filmu	film	k1gInSc2	film
Vetřelci	vetřelec	k1gMnPc7	vetřelec
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
na	na	k7c6	na
laserdiscu	laserdiscum	k1gNnSc6	laserdiscum
a	a	k8xC	a
VHS	VHS	kA	VHS
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
verzi	verze	k1gFnSc4	verze
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
celých	celá	k1gFnPc2	celá
sedmnáct	sedmnáct	k4xCc1	sedmnáct
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vystřižených	vystřižený	k2eAgFnPc2d1	vystřižená
<g/>
,	,	kIx,	,
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vystřižených	vystřižený	k2eAgFnPc6d1	vystřižená
scénách	scéna	k1gFnPc6	scéna
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
moment	moment	k1gInSc4	moment
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
zemřela	zemřít	k5eAaPmAgFnS	zemřít
věkem	věk	k1gInSc7	věk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
když	když	k8xS	když
rodiče	rodič	k1gMnPc1	rodič
Newt	Newtum	k1gNnPc2	Newtum
objevují	objevovat	k5eAaImIp3nP	objevovat
ztroskotanou	ztroskotaný	k2eAgFnSc4d1	ztroskotaná
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Vetřelec	vetřelec	k1gMnSc1	vetřelec
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Newt	Newt	k1gMnSc1	Newt
je	být	k5eAaImIp3nS	být
napaden	napadnout	k5eAaPmNgMnS	napadnout
Facehuggerem	Facehugger	k1gInSc7	Facehugger
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
vystřižené	vystřižený	k2eAgFnSc2d1	vystřižená
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mariňáci	mariňáci	k?	mariňáci
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
opevnění	opevnění	k1gNnSc3	opevnění
se	se	k3xPyFc4	se
v	v	k7c6	v
části	část	k1gFnSc6	část
komplexu	komplex	k1gInSc2	komplex
automatické	automatický	k2eAgInPc1d1	automatický
kulomety	kulomet	k1gInPc1	kulomet
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
likvidují	likvidovat	k5eAaBmIp3nP	likvidovat
útočící	útočící	k2eAgMnPc4d1	útočící
vetřelce	vetřelec	k1gMnPc4	vetřelec
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
.	.	kIx.	.
</s>
<s>
Scény	scéna	k1gFnPc1	scéna
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
vystřiženy	vystřižen	k2eAgFnPc1d1	vystřižena
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc4	fox
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
společnosti	společnost	k1gFnSc2	společnost
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
"	"	kIx"	"
<g/>
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
ničeho	nic	k3yNnSc2	nic
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vetřelci	vetřelec	k1gMnPc1	vetřelec
byli	být	k5eAaImAgMnP	být
nominováni	nominovat	k5eAaBmNgMnP	nominovat
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
cen	cena	k1gFnPc2	cena
Akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jiných	jiný	k1gMnPc2	jiný
<g/>
:	:	kIx,	:
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
adaptovaný	adaptovaný	k2eAgInSc4d1	adaptovaný
scénář	scénář	k1gInSc4	scénář
Film	film	k1gInSc1	film
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
2	[number]	k4	2
ceny	cena	k1gFnPc1	cena
(	(	kIx(	(
<g/>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
zvukových	zvukový	k2eAgInPc2d1	zvukový
efektů	efekt	k1gInPc2	efekt
a	a	k8xC	a
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sigourney	Sigournea	k1gFnSc2	Sigournea
Weaver	Weavra	k1gFnPc2	Weavra
získala	získat	k5eAaPmAgFnS	získat
její	její	k3xOp3gFnSc4	její
první	první	k4xOgFnSc4	první
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
také	také	k9	také
4	[number]	k4	4
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
BAFTA	BAFTA	kA	BAFTA
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
vítězem	vítěz	k1gMnSc7	vítěz
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
vizuálních	vizuální	k2eAgInPc2d1	vizuální
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
cen	cena	k1gFnPc2	cena
Saturnu	Saturn	k1gInSc2	Saturn
Nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
sci-fi	scii	k1gFnSc2	sci-fi
film	film	k1gInSc1	film
Nejlepší	dobrý	k2eAgInSc1d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
(	(	kIx(	(
<g/>
Sigourney	Sigourney	k1gInPc4	Sigourney
Weaver	Weaver	k1gInSc1	Weaver
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
role	role	k1gFnSc2	role
(	(	kIx(	(
<g/>
Bill	Bill	k1gMnSc1	Bill
Paxton	Paxton	k1gInSc1	Paxton
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
role	role	k1gFnSc2	role
(	(	kIx(	(
<g/>
Jenette	Jenett	k1gInSc5	Jenett
Goldstein	Goldstein	k2eAgInSc4d1	Goldstein
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dětský	dětský	k2eAgInSc4d1	dětský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
(	(	kIx(	(
<g/>
Carrie	Carrie	k1gFnSc1	Carrie
Henn	Henn	k1gMnSc1	Henn
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInSc1d3	nejlepší
scénář	scénář	k1gInSc1	scénář
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
)	)	kIx)	)
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
efekty	efekt	k1gInPc1	efekt
(	(	kIx(	(
<g/>
Stan	stan	k1gInSc1	stan
Winston	Winston	k1gInSc1	Winston
a	a	k8xC	a
L.A.	L.A.	k1gFnSc1	L.A.
Effects	Effects	k1gInSc1	Effects
Group	Group	k1gInSc1	Group
<g/>
)	)	kIx)	)
Proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
pokračování	pokračování	k1gNnSc1	pokračování
filmu	film	k1gInSc2	film
Vetřelec	vetřelec	k1gMnSc1	vetřelec
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
nejmenovalo	jmenovat	k5eNaImAgNnS	jmenovat
Vetřelec	vetřelec	k1gMnSc1	vetřelec
2	[number]	k4	2
<g/>
?	?	kIx.	?
</s>
<s>
Protože	protože	k8xS	protože
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
filmu	film	k1gInSc6	film
Ridleyho	Ridley	k1gMnSc2	Ridley
Scotta	Scott	k1gInSc2	Scott
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
italský	italský	k2eAgInSc1d1	italský
snímek	snímek	k1gInSc1	snímek
Alien	Alien	k2eAgInSc1d1	Alien
2	[number]	k4	2
<g/>
:	:	kIx,	:
Sulla	Sulla	k1gMnSc1	Sulla
Terra	Terra	k1gMnSc1	Terra
(	(	kIx(	(
<g/>
On	on	k3xPp3gMnSc1	on
Earth	Earth	k1gMnSc1	Earth
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
nechtěl	chtít	k5eNaImAgMnS	chtít
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
nic	nic	k3yNnSc4	nic
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
</s>
<s>
Královnu	královna	k1gFnSc4	královna
vetřelců	vetřelec	k1gMnPc2	vetřelec
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
sám	sám	k3xTgInSc1	sám
Cameron	Cameron	k1gInSc1	Cameron
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgMnSc1d1	vysoký
a	a	k8xC	a
muselo	muset	k5eAaImAgNnS	muset
jej	on	k3xPp3gMnSc4	on
obsluhovat	obsluhovat	k5eAaImF	obsluhovat
16	[number]	k4	16
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
sci-fi	scii	k1gFnPc2	sci-fi
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
filmu	film	k1gInSc2	film
Vetřelci	vetřelec	k1gMnPc7	vetřelec
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
komiks	komiks	k1gInSc1	komiks
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
děj	děj	k1gInSc4	děj
filmu	film	k1gInSc2	film
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
příběhem	příběh	k1gInSc7	příběh
Ripleyové	Ripleyová	k1gFnSc2	Ripleyová
<g/>
,	,	kIx,	,
Bishopa	Bishopa	k1gFnSc1	Bishopa
<g/>
,	,	kIx,	,
Newt	Newt	k1gInSc1	Newt
a	a	k8xC	a
Hickse	Hickse	k1gFnSc1	Hickse
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
natočen	natočen	k2eAgInSc1d1	natočen
film	film	k1gInSc1	film
Vetřelec	vetřelec	k1gMnSc1	vetřelec
3	[number]	k4	3
<g/>
,	,	kIx,	,
autoři	autor	k1gMnPc1	autor
komiksu	komiks	k1gInSc2	komiks
změnili	změnit	k5eAaPmAgMnP	změnit
postavám	postava	k1gFnPc3	postava
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
neodporoval	odporovat	k5eNaImAgMnS	odporovat
s	s	k7c7	s
oficiální	oficiální	k2eAgFnSc7d1	oficiální
historií	historie	k1gFnSc7	historie
<g/>
.	.	kIx.	.
</s>
<s>
Kulisy	kulisa	k1gFnPc1	kulisa
v	v	k7c4	v
Pinewood	Pinewood	k1gInSc4	Pinewood
Studios	Studios	k?	Studios
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
také	také	k9	také
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Batman	Batman	k1gMnSc1	Batman
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
natáčel	natáčet	k5eAaImAgMnS	natáčet
o	o	k7c4	o
3	[number]	k4	3
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
scén	scéna	k1gFnPc2	scéna
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
Axis	Axisa	k1gFnPc2	Axisa
Chemicals	Chemicalsa	k1gFnPc2	Chemicalsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Joker	Joker	k1gInSc1	Joker
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
nádrže	nádrž	k1gFnSc2	nádrž
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
pulzní	pulzní	k2eAgFnSc1d1	pulzní
puška	puška	k1gFnSc1	puška
Armat	Armat	k1gInSc4	Armat
M41A	M41A	k1gFnSc2	M41A
z	z	k7c2	z
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k8xC	jako
vzduchem	vzduch	k1gInSc7	vzduch
chlazená	chlazený	k2eAgFnSc1d1	chlazená
10	[number]	k4	10
mm	mm	kA	mm
pulzní	pulzní	k2eAgFnSc1d1	pulzní
útočná	útočný	k2eAgFnSc1d1	útočná
puška	puška	k1gFnSc1	puška
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
zbraň	zbraň	k1gFnSc1	zbraň
vojáků	voják	k1gMnPc2	voják
sborů	sbor	k1gInPc2	sbor
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Army	Arma	k1gFnSc2	Arma
a	a	k8xC	a
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Colonial	Colonial	k1gMnSc1	Colonial
Marine	Marin	k1gInSc5	Marin
Corps	corps	k1gInSc4	corps
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
film	film	k1gInSc1	film
Vetřelci	vetřelec	k1gMnPc7	vetřelec
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
zbraní	zbraň	k1gFnPc2	zbraň
sci-fi	scii	k1gNnPc2	sci-fi
filmů	film	k1gInPc2	film
a	a	k8xC	a
ikonou	ikona	k1gFnSc7	ikona
futuristických	futuristický	k2eAgFnPc2d1	futuristická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Vetřelců	vetřelec	k1gMnPc2	vetřelec
se	se	k3xPyFc4	se
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
Vetřelec3	Vetřelec3	k1gFnSc2	Vetřelec3
a	a	k8xC	a
také	také	k9	také
i	i	k9	i
ve	v	k7c6	v
filmech	film	k1gInPc6	film
zcela	zcela	k6eAd1	zcela
jiných	jiný	k2eAgFnPc2d1	jiná
sérií	série	k1gFnPc2	série
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Terminátor	terminátor	k1gInSc1	terminátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jen	jen	k9	jen
okrajově	okrajově	k6eAd1	okrajově
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
seriálech	seriál	k1gInPc6	seriál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Městečko	městečko	k1gNnSc1	městečko
South	Southa	k1gFnPc2	Southa
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Zbraň	zbraň	k1gFnSc4	zbraň
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
sám	sám	k3xTgMnSc1	sám
režisér	režisér	k1gMnSc1	režisér
Vetřelců	vetřelec	k1gMnPc2	vetřelec
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
;	;	kIx,	;
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
se	se	k3xPyFc4	se
různými	různý	k2eAgFnPc7d1	různá
zbraněmi	zbraň	k1gFnPc7	zbraň
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
například	například	k6eAd1	například
Thompson	Thompson	k1gInSc1	Thompson
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Aliens	Aliens	k1gInSc1	Aliens
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vetřelci	vetřelec	k1gMnPc1	vetřelec
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Vetřelci	vetřelec	k1gMnPc1	vetřelec
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Vetřelci	vetřelec	k1gMnPc1	vetřelec
na	na	k7c4	na
Fox	fox	k1gInSc4	fox
Axe	Axe	k1gFnSc2	Axe
</s>
