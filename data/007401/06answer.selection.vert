<s>
Vetřelci	vetřelec	k1gMnPc1	vetřelec
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Aliens	Aliens	k1gInSc1	Aliens
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
sci-fi	scii	k1gFnPc7	sci-fi
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
jako	jako	k8xC	jako
pokračování	pokračování	k1gNnSc1	pokračování
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
filmu	film	k1gInSc2	film
Vetřelec	vetřelec	k1gMnSc1	vetřelec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
Ellen	Ellen	k1gInSc1	Ellen
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
LV-	LV-	k1gFnSc2	LV-
<g/>
426	[number]	k4	426
<g/>
,	,	kIx,	,
okupovanou	okupovaný	k2eAgFnSc4d1	okupovaná
vetřelci	vetřelec	k1gMnPc7	vetřelec
<g/>
.	.	kIx.	.
</s>
