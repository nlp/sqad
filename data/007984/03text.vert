<s>
Hrtan	hrtan	k1gInSc1	hrtan
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
larynx	larynx	k1gInSc1	larynx
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chrupavkami	chrupavka	k1gFnPc7	chrupavka
vyztužená	vyztužený	k2eAgFnSc1d1	vyztužená
trubice	trubice	k1gFnSc1	trubice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prochází	procházet	k5eAaImIp3nS	procházet
vzduch	vzduch	k1gInSc1	vzduch
do	do	k7c2	do
průdušnice	průdušnice	k1gFnSc2	průdušnice
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Hrtan	hrtan	k1gInSc1	hrtan
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
oddílem	oddíl	k1gInSc7	oddíl
dolních	dolní	k2eAgFnPc2d1	dolní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
chrupavek	chrupavka	k1gFnPc2	chrupavka
párových	párový	k2eAgMnPc2d1	párový
a	a	k8xC	a
nepárových	párový	k2eNgMnPc2d1	nepárový
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
párové	párový	k2eAgFnPc4d1	párová
chrupavky	chrupavka	k1gFnPc4	chrupavka
patří	patřit	k5eAaImIp3nS	patřit
pár	pár	k4xCyI	pár
hlasivkových	hlasivkový	k2eAgFnPc2d1	hlasivková
chrupavek	chrupavka	k1gFnPc2	chrupavka
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nepárové	párový	k2eNgNnSc4d1	nepárové
patří	patřit	k5eAaImIp3nS	patřit
štítná	štítný	k2eAgFnSc1d1	štítná
chrupavka	chrupavka	k1gFnSc1	chrupavka
<g/>
,	,	kIx,	,
prstencová	prstencový	k2eAgFnSc1d1	prstencová
chrupavka	chrupavka	k1gFnSc1	chrupavka
a	a	k8xC	a
příklopka	příklopka	k1gFnSc1	příklopka
hrtanová	hrtanový	k2eAgFnSc1d1	hrtanová
(	(	kIx(	(
<g/>
epiglottis	epiglottis	k1gFnSc1	epiglottis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
několik	několik	k4yIc4	několik
drobných	drobný	k2eAgFnPc2d1	drobná
(	(	kIx(	(
<g/>
sesamských	sesamský	k2eAgFnPc2d1	sesamský
<g/>
)	)	kIx)	)
párových	párový	k2eAgFnPc2d1	párová
chrupavek	chrupavka	k1gFnPc2	chrupavka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
součástí	součást	k1gFnSc7	součást
vazů	vaz	k1gInPc2	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Chrupavky	chrupavka	k1gFnPc1	chrupavka
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
spojeny	spojit	k5eAaPmNgInP	spojit
pomocí	pomocí	k7c2	pomocí
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
vazů	vaz	k1gInPc2	vaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tvořily	tvořit	k5eAaImAgFnP	tvořit
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Hrtan	hrtan	k1gInSc1	hrtan
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
hltan	hltan	k1gInSc4	hltan
<g/>
,	,	kIx,	,
společný	společný	k2eAgInSc1d1	společný
úsek	úsek	k1gInSc1	úsek
trávicí	trávicí	k2eAgFnSc2d1	trávicí
a	a	k8xC	a
dýchací	dýchací	k2eAgFnSc2d1	dýchací
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
průdušnici	průdušnice	k1gFnSc6	průdušnice
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
hrtan	hrtan	k1gInSc1	hrtan
je	být	k5eAaImIp3nS	být
pohyblivě	pohyblivě	k6eAd1	pohyblivě
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
pod	pod	k7c7	pod
čelistí	čelist	k1gFnSc7	čelist
pomocí	pomocí	k7c2	pomocí
jazylky	jazylka	k1gFnSc2	jazylka
<g/>
,	,	kIx,	,
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
vazů	vaz	k1gInPc2	vaz
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
zpevněná	zpevněný	k2eAgFnSc1d1	zpevněná
chrupavkami	chrupavka	k1gFnPc7	chrupavka
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
,	,	kIx,	,
štítná	štítný	k2eAgFnSc1d1	štítná
chrupavka	chrupavka	k1gFnSc1	chrupavka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
podklad	podklad	k1gInSc4	podklad
ohryzku	ohryzek	k1gInSc2	ohryzek
<g/>
,	,	kIx,	,
Adamova	Adamův	k2eAgNnSc2d1	Adamovo
jablka	jablko	k1gNnSc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
hrtanu	hrtan	k1gInSc2	hrtan
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
hrtanovou	hrtanový	k2eAgFnSc7d1	hrtanová
příklopkou	příklopka	k1gFnSc7	příklopka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
polykání	polykání	k1gNnSc6	polykání
reflexně	reflexně	k6eAd1	reflexně
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
proniknutí	proniknutí	k1gNnSc1	proniknutí
sousta	sousto	k1gNnSc2	sousto
do	do	k7c2	do
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
chrupavkami	chrupavka	k1gFnPc7	chrupavka
napjaté	napjatý	k2eAgFnSc2d1	napjatá
dva	dva	k4xCgInPc4	dva
hlasové	hlasový	k2eAgInPc4d1	hlasový
vazy	vaz	k1gInPc4	vaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlasivky	hlasivka	k1gFnPc4	hlasivka
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozkmitáváním	rozkmitávání	k1gNnSc7	rozkmitávání
vazů	vaz	k1gInPc2	vaz
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
tónu	tón	k1gInSc2	tón
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
napětí	napětí	k1gNnSc4	napětí
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
je	on	k3xPp3gNnSc4	on
ovládají	ovládat	k5eAaImIp3nP	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Svaly	sval	k1gInPc1	sval
hrtanu	hrtan	k1gInSc2	hrtan
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
příčně	příčně	k6eAd1	příčně
pruhovanou	pruhovaný	k2eAgFnSc7d1	pruhovaná
svalovinou	svalovina	k1gFnSc7	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
Ovládají	ovládat	k5eAaImIp3nP	ovládat
pohyb	pohyb	k1gInSc4	pohyb
chrupavek	chrupavka	k1gFnPc2	chrupavka
<g/>
,	,	kIx,	,
určují	určovat	k5eAaImIp3nP	určovat
napětí	napětí	k1gNnSc4	napětí
hlasových	hlasový	k2eAgInPc2d1	hlasový
vazů	vaz	k1gInPc2	vaz
a	a	k8xC	a
šířku	šířka	k1gFnSc4	šířka
štěrbiny	štěrbina	k1gFnSc2	štěrbina
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Svaly	sval	k1gInPc4	sval
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgInPc4d1	přední
svaly	sval	k1gInPc4	sval
Musculus	Musculus	k1gInSc1	Musculus
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
pouze	pouze	k6eAd1	pouze
m.	m.	k?	m.
<g/>
)	)	kIx)	)
cricothyroideus	cricothyroideus	k1gMnSc1	cricothyroideus
-	-	kIx~	-
rozepjatý	rozepjatý	k2eAgMnSc1d1	rozepjatý
od	od	k7c2	od
přední	přední	k2eAgFnSc2d1	přední
strany	strana	k1gFnSc2	strana
prstencové	prstencový	k2eAgFnSc2d1	prstencová
chrupavky	chrupavka	k1gFnSc2	chrupavka
k	k	k7c3	k
dolnímu	dolní	k2eAgInSc3d1	dolní
okraji	okraj	k1gInSc3	okraj
chrupavky	chrupavka	k1gFnSc2	chrupavka
štítné	štítný	k2eAgFnSc2d1	štítná
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
vertikální	vertikální	k2eAgFnSc4d1	vertikální
a	a	k8xC	a
skloněnou	skloněný	k2eAgFnSc4d1	skloněná
<g/>
.	.	kIx.	.
</s>
<s>
Sklápí	sklápět	k5eAaImIp3nS	sklápět
štítnou	štítný	k2eAgFnSc4d1	štítná
chrupavku	chrupavka	k1gFnSc4	chrupavka
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
napíná	napínat	k5eAaImIp3nS	napínat
hlasové	hlasový	k2eAgInPc4d1	hlasový
vazy	vaz	k1gInPc4	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Postranní	postranní	k2eAgInPc4d1	postranní
svaly	sval	k1gInPc4	sval
M.	M.	kA	M.
cricoarytenoideus	cricoarytenoideus	k1gInSc4	cricoarytenoideus
lateralis	lateralis	k1gFnSc2	lateralis
-	-	kIx~	-
od	od	k7c2	od
horního	horní	k2eAgInSc2d1	horní
okraje	okraj	k1gInSc2	okraj
prstencové	prstencový	k2eAgFnSc2d1	prstencová
chrupavky	chrupavka	k1gFnSc2	chrupavka
na	na	k7c4	na
výběžek	výběžek	k1gInSc4	výběžek
chrupavky	chrupavka	k1gFnSc2	chrupavka
hlasivkové	hlasivkový	k2eAgFnSc2d1	hlasivková
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nS	provádět
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
rotaci	rotace	k1gFnSc4	rotace
hlasivkové	hlasivkový	k2eAgFnSc2d1	hlasivková
chrupavky	chrupavka	k1gFnSc2	chrupavka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
sbližuje	sbližovat	k5eAaImIp3nS	sbližovat
hlasivkové	hlasivkový	k2eAgInPc4d1	hlasivkový
vazy	vaz	k1gInPc4	vaz
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
thyroarytenoideus	thyroarytenoideus	k1gMnSc1	thyroarytenoideus
-	-	kIx~	-
rozepjatý	rozepjatý	k2eAgMnSc1d1	rozepjatý
kolem	kolem	k7c2	kolem
hlasových	hlasový	k2eAgInPc2d1	hlasový
vazů	vaz	k1gInPc2	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
štítné	štítný	k2eAgFnSc2d1	štítná
chrupavky	chrupavka	k1gFnSc2	chrupavka
na	na	k7c4	na
chrupavku	chrupavka	k1gFnSc4	chrupavka
hlasivkovou	hlasivkový	k2eAgFnSc4d1	hlasivková
<g/>
.	.	kIx.	.
</s>
<s>
Spolupůsobí	spolupůsobit	k5eAaImIp3nS	spolupůsobit
při	při	k7c6	při
sevření	sevření	k1gNnSc6	sevření
hlasových	hlasový	k2eAgInPc2d1	hlasový
vazů	vaz	k1gInPc2	vaz
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
thyroepiglotticus	thyroepiglotticus	k1gMnSc1	thyroepiglotticus
-	-	kIx~	-
od	od	k7c2	od
štítné	štítný	k2eAgFnSc2d1	štítná
chrupavky	chrupavka	k1gFnSc2	chrupavka
dozadu	dozadu	k6eAd1	dozadu
vzhůru	vzhůru	k6eAd1	vzhůru
k	k	k7c3	k
okraji	okraj	k1gInSc3	okraj
epiglottis	epiglottis	k1gFnSc2	epiglottis
<g/>
.	.	kIx.	.
</s>
<s>
Táhne	táhnout	k5eAaImIp3nS	táhnout
za	za	k7c4	za
okraj	okraj	k1gInSc4	okraj
epiglottis	epiglottis	k1gFnSc2	epiglottis
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgInPc4d1	zadní
svaly	sval	k1gInPc4	sval
M.	M.	kA	M.
cricoarytenoideus	cricoarytenoideus	k1gInSc4	cricoarytenoideus
posterior	posteriora	k1gFnPc2	posteriora
-	-	kIx~	-
od	od	k7c2	od
zadní	zadní	k2eAgFnSc2d1	zadní
strany	strana	k1gFnSc2	strana
prstencové	prstencový	k2eAgFnSc2d1	prstencová
chrupavky	chrupavka	k1gFnSc2	chrupavka
na	na	k7c4	na
výběžek	výběžek	k1gInSc4	výběžek
chrupavky	chrupavka	k1gFnSc2	chrupavka
hlasivkové	hlasivkový	k2eAgFnSc2d1	hlasivková
<g/>
.	.	kIx.	.
</s>
<s>
Uklání	uklánět	k5eAaImIp3nS	uklánět
hlasivkovou	hlasivkový	k2eAgFnSc4d1	hlasivková
chrupavku	chrupavka	k1gFnSc4	chrupavka
a	a	k8xC	a
rotuje	rotovat	k5eAaImIp3nS	rotovat
ji	on	k3xPp3gFnSc4	on
zevně	zevně	k6eAd1	zevně
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
rozevírá	rozevírat	k5eAaImIp3nS	rozevírat
štěrbinu	štěrbina	k1gFnSc4	štěrbina
mezi	mezi	k7c7	mezi
hlasovými	hlasový	k2eAgInPc7d1	hlasový
vazy	vaz	k1gInPc7	vaz
a	a	k8xC	a
současně	současně	k6eAd1	současně
vazy	vaz	k1gInPc4	vaz
napíná	napínat	k5eAaImIp3nS	napínat
<g/>
.	.	kIx.	.
</s>
<s>
Obrna	obrna	k1gFnSc1	obrna
tohoto	tento	k3xDgInSc2	tento
svalu	sval	k1gInSc2	sval
znamená	znamenat	k5eAaImIp3nS	znamenat
značné	značný	k2eAgNnSc1d1	značné
poškození	poškození	k1gNnSc1	poškození
fonace	fonace	k1gFnSc2	fonace
i	i	k8xC	i
volného	volný	k2eAgInSc2d1	volný
průchodu	průchod	k1gInSc2	průchod
vzduchu	vzduch	k1gInSc2	vzduch
při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
arytenoideus	arytenoideus	k1gMnSc1	arytenoideus
-	-	kIx~	-
spojuje	spojovat	k5eAaImIp3nS	spojovat
hlasivkové	hlasivkový	k2eAgFnPc4d1	hlasivková
chrupavky	chrupavka	k1gFnPc4	chrupavka
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
-	-	kIx~	-
příčnou	příčný	k2eAgFnSc4d1	příčná
a	a	k8xC	a
šikmou	šikmý	k2eAgFnSc4d1	šikmá
<g/>
.	.	kIx.	.
</s>
<s>
Funkcí	funkce	k1gFnSc7	funkce
tohoto	tento	k3xDgInSc2	tento
svalu	sval	k1gInSc2	sval
je	být	k5eAaImIp3nS	být
zužování	zužování	k1gNnSc4	zužování
štěrbiny	štěrbina	k1gFnSc2	štěrbina
mezi	mezi	k7c7	mezi
hlasovými	hlasový	k2eAgInPc7d1	hlasový
vazy	vaz	k1gInPc7	vaz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
svalu	sval	k1gInSc3	sval
náleží	náležet	k5eAaImIp3nS	náležet
ještě	ještě	k9	ještě
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
epiglottis	epiglottis	k1gFnSc4	epiglottis
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sklání	sklánět	k5eAaImIp3nP	sklánět
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
tak	tak	k9	tak
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
znatelné	znatelný	k2eAgInPc1d1	znatelný
především	především	k9	především
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
a	a	k8xC	a
tvaru	tvar	k1gInSc6	tvar
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Hlasivka	hlasivka	k1gFnSc1	hlasivka
(	(	kIx(	(
<g/>
glottis	glottis	k1gFnSc1	glottis
<g/>
)	)	kIx)	)
u	u	k7c2	u
muže	muž	k1gMnSc2	muž
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
28	[number]	k4	28
mm	mm	kA	mm
<g/>
,	,	kIx,	,
u	u	k7c2	u
ženy	žena	k1gFnSc2	žena
kolem	kolem	k7c2	kolem
20	[number]	k4	20
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
úhel	úhel	k1gInSc1	úhel
sevřený	sevřený	k2eAgInSc1d1	sevřený
vnitřními	vnitřní	k2eAgFnPc7d1	vnitřní
stěnami	stěna	k1gFnPc7	stěna
štítné	štítný	k2eAgFnSc2d1	štítná
chrupavky	chrupavka	k1gFnSc2	chrupavka
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
muže	muž	k1gMnSc2	muž
kolem	kolem	k7c2	kolem
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
u	u	k7c2	u
ženy	žena	k1gFnSc2	žena
120	[number]	k4	120
<g/>
°	°	k?	°
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Hrtan	hrtan	k1gInSc1	hrtan
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
muže	muž	k1gMnSc2	muž
spíše	spíše	k9	spíše
vyšší	vysoký	k2eAgInPc1d2	vyšší
a	a	k8xC	a
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
krku	krk	k1gInSc6	krk
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ohryzek	ohryzek	k1gInSc1	ohryzek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ženy	žena	k1gFnSc2	žena
takto	takto	k6eAd1	takto
obvykle	obvykle	k6eAd1	obvykle
neprominuje	prominovat	k5eNaImIp3nS	prominovat
<g/>
.	.	kIx.	.
</s>
<s>
Dětský	dětský	k2eAgInSc1d1	dětský
hrtan	hrtan	k1gInSc1	hrtan
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
délka	délka	k1gFnSc1	délka
hlasových	hlasový	k2eAgInPc2d1	hlasový
vazů	vaz	k1gInPc2	vaz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
chlapců	chlapec	k1gMnPc2	chlapec
v	v	k7c6	v
období	období	k1gNnSc6	období
puberty	puberta	k1gFnSc2	puberta
hrtan	hrtan	k1gInSc4	hrtan
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
testosteronu	testosteron	k1gInSc2	testosteron
prudce	prudko	k6eAd1	prudko
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužení	prodloužení	k1gNnSc1	prodloužení
délky	délka	k1gFnSc2	délka
hlasových	hlasový	k2eAgInPc2d1	hlasový
vazů	vaz	k1gInPc2	vaz
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
hlasu	hlas	k1gInSc2	hlas
z	z	k7c2	z
dětského	dětský	k2eAgMnSc2d1	dětský
na	na	k7c4	na
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
mutace	mutace	k1gFnSc1	mutace
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
hlasu	hlas	k1gInSc2	hlas
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
pomocí	pomocí	k7c2	pomocí
hlasivky	hlasivka	k1gFnSc2	hlasivka
(	(	kIx(	(
<g/>
glottis	glottis	k1gFnSc1	glottis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
okraje	okraj	k1gInPc1	okraj
<g/>
,	,	kIx,	,
podložené	podložený	k2eAgInPc1d1	podložený
hlasovými	hlasový	k2eAgInPc7d1	hlasový
vazy	vaz	k1gInPc7	vaz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
hrtanových	hrtanový	k2eAgInPc2d1	hrtanový
svalů	sval	k1gInPc2	sval
napínají	napínat	k5eAaImIp3nP	napínat
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
sbližují	sbližovat	k5eAaImIp3nP	sbližovat
nebo	nebo	k8xC	nebo
oddalují	oddalovat	k5eAaImIp3nP	oddalovat
<g/>
.	.	kIx.	.
</s>
<s>
Napjaté	napjatý	k2eAgInPc1d1	napjatý
hlasové	hlasový	k2eAgInPc1d1	hlasový
vazy	vaz	k1gInPc1	vaz
se	se	k3xPyFc4	se
při	při	k7c6	při
úzké	úzký	k2eAgFnSc6d1	úzká
štěrbině	štěrbina	k1gFnSc6	štěrbina
rozechvívají	rozechvívat	k5eAaImIp3nP	rozechvívat
proudem	proud	k1gInSc7	proud
vzduchu	vzduch	k1gInSc2	vzduch
výdechu	výdech	k1gInSc2	výdech
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
(	(	kIx(	(
<g/>
kmitočet	kmitočet	k1gInSc1	kmitočet
<g/>
)	)	kIx)	)
tónu	tón	k1gInSc2	tón
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
<g/>
,	,	kIx,	,
napětí	napětí	k1gNnSc6	napětí
a	a	k8xC	a
tloušťce	tloušťka	k1gFnSc3	tloušťka
vazů	vaz	k1gInPc2	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
intenzita	intenzita	k1gFnSc1	intenzita
(	(	kIx(	(
<g/>
amplituda	amplituda	k1gFnSc1	amplituda
kmitů	kmit	k1gInPc2	kmit
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
vznikající	vznikající	k2eAgInSc1d1	vznikající
v	v	k7c6	v
hlasivkách	hlasivka	k1gFnPc6	hlasivka
je	být	k5eAaImIp3nS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
barvy	barva	k1gFnSc2	barva
lidského	lidský	k2eAgInSc2d1	lidský
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
slabého	slabý	k2eAgInSc2d1	slabý
zvuku	zvuk	k1gInSc2	zvuk
se	se	k3xPyFc4	se
do	do	k7c2	do
nám	my	k3xPp1nPc3	my
známé	známý	k1gMnPc4	známý
formy	forma	k1gFnSc2	forma
hlasu	hlas	k1gInSc2	hlas
mění	měnit	k5eAaImIp3nS	měnit
až	až	k9	až
rezonancí	rezonance	k1gFnSc7	rezonance
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
a	a	k8xC	a
ve	v	k7c6	v
vedlejších	vedlejší	k2eAgFnPc6d1	vedlejší
nosních	nosní	k2eAgFnPc6d1	nosní
dutinách	dutina	k1gFnPc6	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Dýchání	dýchání	k1gNnSc1	dýchání
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
otvíráním	otvírání	k1gNnSc7	otvírání
hlasivkové	hlasivkový	k2eAgFnSc2d1	hlasivková
štěrbiny	štěrbina	k1gFnSc2	štěrbina
(	(	kIx(	(
<g/>
rima	rim	k2eAgFnSc1d1	rim
glottidis	glottidis	k1gFnSc1	glottidis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
fonaci	fonace	k1gFnSc6	fonace
sevřená	sevřený	k2eAgFnSc1d1	sevřená
na	na	k7c6	na
obou	dva	k4xCgNnPc6	dva
svých	svůj	k3xOyFgInPc6	svůj
koncích	konec	k1gInPc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
klidném	klidný	k2eAgInSc6d1	klidný
dýchání	dýchání	k1gNnSc2	dýchání
a	a	k8xC	a
šepotu	šepot	k1gInSc2	šepot
je	být	k5eAaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
v	v	k7c6	v
"	"	kIx"	"
<g/>
přední	přední	k2eAgFnSc6d1	přední
<g/>
"	"	kIx"	"
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
pars	pars	k1gInSc1	pars
intermembranacea	intermembranace	k1gInSc2	intermembranace
<g/>
)	)	kIx)	)
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
části	část	k1gFnSc6	část
"	"	kIx"	"
<g/>
zadní	zadní	k2eAgFnPc1d1	zadní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pars	pars	k1gInSc1	pars
intercartilaginea	intercartilagine	k1gInSc2	intercartilagine
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
středně	středně	k6eAd1	středně
intenzivním	intenzivní	k2eAgNnSc6d1	intenzivní
dýchání	dýchání	k1gNnSc6	dýchání
se	se	k3xPyFc4	se
štěrbina	štěrbina	k1gFnSc1	štěrbina
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
otvírá	otvírat	k5eAaImIp3nS	otvírat
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
svých	svůj	k3xOyFgFnPc6	svůj
částech	část	k1gFnPc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
usilovném	usilovný	k2eAgNnSc6d1	usilovné
dýchání	dýchání	k1gNnSc6	dýchání
je	být	k5eAaImIp3nS	být
štěrbina	štěrbina	k1gFnSc1	štěrbina
otevřena	otevřít	k5eAaPmNgFnS	otevřít
doširoka	doširoka	k6eAd1	doširoka
<g/>
.	.	kIx.	.
</s>
<s>
Kašel	kašel	k1gInSc1	kašel
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
hlen	hlen	k1gInSc4	hlen
nahromaděný	nahromaděný	k2eAgInSc4d1	nahromaděný
v	v	k7c6	v
dolních	dolní	k2eAgFnPc6d1	dolní
cestách	cesta	k1gFnPc6	cesta
dýchacích	dýchací	k2eAgFnPc6d1	dýchací
nebo	nebo	k8xC	nebo
vniknuvší	vniknuvší	k2eAgNnSc4d1	vniknuvší
cizí	cizí	k2eAgNnSc4d1	cizí
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
uzávěru	uzávěr	k1gInSc6	uzávěr
hlasivkové	hlasivkový	k2eAgFnSc2d1	hlasivková
štěrbiny	štěrbina	k1gFnSc2	štěrbina
s	s	k7c7	s
následným	následný	k2eAgInSc7d1	následný
prudkým	prudký	k2eAgInSc7d1	prudký
nárazovým	nárazový	k2eAgInSc7d1	nárazový
výdechem	výdech	k1gInSc7	výdech
<g/>
.	.	kIx.	.
</s>
<s>
Štěrbina	štěrbina	k1gFnSc1	štěrbina
se	se	k3xPyFc4	se
automaticky	automaticky	k6eAd1	automaticky
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vniknutí	vniknutí	k1gNnSc2	vniknutí
chemikálie	chemikálie	k1gFnSc2	chemikálie
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
cizího	cizí	k2eAgNnSc2d1	cizí
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
polykání	polykání	k1gNnSc6	polykání
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
hrtan	hrtan	k1gInSc4	hrtan
sklopením	sklopení	k1gNnSc7	sklopení
epiglottis	epiglottis	k1gFnPc2	epiglottis
nad	nad	k7c4	nad
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
přiblížení	přiblížení	k1gNnSc1	přiblížení
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
zdvižením	zdvižení	k1gNnSc7	zdvižení
jazylky	jazylka	k1gFnSc2	jazylka
(	(	kIx(	(
<g/>
zdvíhá	zdvíhat	k5eAaImIp3nS	zdvíhat
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
)	)	kIx)	)
a	a	k8xC	a
sestupem	sestup	k1gInSc7	sestup
kořene	kořen	k1gInSc2	kořen
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
epiglottis	epiglottis	k1gFnSc4	epiglottis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
