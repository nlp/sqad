<s>
Waldhausen	Waldhausen	k1gInSc1
(	(	kIx(
<g/>
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Waldhausen	Waldhausen	k2eAgInSc1d1
Kostel	kostel	k1gInSc1
ve	v	k7c4
Waldhausenu	Waldhausen	k2eAgFnSc4d1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
682	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc4
spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
okres	okres	k1gInSc4
</s>
<s>
Zwettl	Zwettnout	k5eAaPmAgMnS
</s>
<s>
Poloha	poloha	k1gFnSc1
městyse	městys	k1gInSc2
v	v	k7c6
okrese	okres	k1gInSc6
Zwettl	Zwettl	k1gFnSc2
</s>
<s>
Waldhausen	Waldhausen	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
39,74	39,74	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
1	#num#	k4
252	#num#	k4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
31,5	31,5	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
městys	městys	k1gInSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.waldhausen.riskommunal.net	www.waldhausen.riskommunal.net	k1gInSc1
E-mail	e-mail	k1gInSc1
</s>
<s>
office@waldviertler-kernland.at	office@waldviertler-kernland.at	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
02877	#num#	k4
PSČ	PSČ	kA
</s>
<s>
3531	#num#	k4
<g/>
,	,	kIx,
3914	#num#	k4
Označení	označení	k1gNnSc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
ZT	ZT	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Waldhausen	Waldhausen	k1gInSc1
je	být	k5eAaImIp3nS
městys	městys	k1gInSc4
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
ve	v	k7c6
spolkové	spolkový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
v	v	k7c6
okrese	okres	k1gInSc6
Zwettl	Zwettl	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gInSc6
1252	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Waldhausen	Waldhausen	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
spolkové	spolkový	k2eAgFnSc2d1
země	zem	k1gFnSc2
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
v	v	k7c6
regionu	region	k1gInSc6
Waldviertel	Waldviertel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
11	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Zwettlu	Zwettl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
městysu	městys	k1gInSc2
činí	činit	k5eAaImIp3nS
39,74	39,74	k4
km²	km²	k?
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
42,9	42,9	k4
%	%	kIx~
je	být	k5eAaImIp3nS
zalesněných	zalesněný	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Členění	členění	k1gNnSc1
</s>
<s>
Území	území	k1gNnSc1
městyse	městys	k1gInSc2
Waldhausen	Waldhausna	k1gFnPc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
dvanácti	dvanáct	k4xCc2
částí	část	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
závorce	závorka	k1gFnSc6
uveden	uveden	k2eAgInSc1d1
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Brand	Brand	k1gInSc1
(	(	kIx(
<g/>
192	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gutenbrunn	Gutenbrunn	k1gNnSc1
(	(	kIx(
<g/>
51	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hirschenschlag	Hirschenschlag	k1gInSc1
(	(	kIx(
<g/>
53	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Königsbach	Königsbach	k1gInSc1
(	(	kIx(
<g/>
98	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Loschberg	Loschberg	k1gInSc1
(	(	kIx(
<g/>
69	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Niedernondorf	Niedernondorf	k1gInSc1
(	(	kIx(
<g/>
147	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Niederwaltenreith	Niederwaltenreith	k1gInSc1
(	(	kIx(
<g/>
41	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Obernondorf	Obernondorf	k1gInSc1
(	(	kIx(
<g/>
122	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rappoltschlag	Rappoltschlag	k1gInSc1
(	(	kIx(
<g/>
106	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Waldhausen	Waldhausen	k1gInSc1
(	(	kIx(
<g/>
222	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Werschenschlag	Werschenschlag	k1gInSc1
(	(	kIx(
<g/>
82	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wiesenreith	Wiesenreith	k1gInSc1
(	(	kIx(
<g/>
69	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ve	v	k7c6
středověku	středověk	k1gInSc6
této	tento	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
dominoval	dominovat	k5eAaImAgInS
hrad	hrad	k1gInSc1
Lozburg	Lozburg	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
ruiny	ruina	k1gFnPc4
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
spatřit	spatřit	k5eAaPmF
z	z	k7c2
hory	hora	k1gFnSc2
Loschberg	Loschberg	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
lese	les	k1gInSc6
u	u	k7c2
části	část	k1gFnSc2
Brand	Brand	k1gInSc4
nalezena	nalezen	k2eAgFnSc1d1
odcizená	odcizený	k2eAgFnSc1d1
Celliniho	Celliniha	k1gFnSc5
slánka	slánka	k1gFnSc1
z	z	k7c2
uměleckohistorického	uměleckohistorický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
městysi	městys	k1gInSc6
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
Konrád	Konrád	k1gMnSc1
Waldhauser	Waldhauser	k1gMnSc1
<g/>
,	,	kIx,
významný	významný	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
kazatel	kazatel	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
reformátor	reformátor	k1gMnSc1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Zámek	zámek	k1gInSc1
v	v	k7c6
Niedernondorfu	Niedernondorf	k1gInSc6
</s>
<s>
Kostel	kostel	k1gInSc1
v	v	k7c6
Brandu	Brand	k1gInSc6
</s>
<s>
Kaple	kaple	k1gFnSc1
v	v	k7c6
Niederwaltenreithu	Niederwaltenreith	k1gInSc6
</s>
<s>
Kaple	kaple	k1gFnSc1
v	v	k7c6
Loschbergu	Loschberg	k1gInSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Waldhausen	Waldhausna	k1gFnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Waldhausen	Waldhausna	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Zwettl	Zwettl	k1gFnSc2
</s>
<s>
Allentsteig	Allentsteig	k1gInSc1
•	•	k?
Altmelon	Altmelon	k1gInSc1
•	•	k?
Arbesbach	Arbesbach	k1gMnSc1
•	•	k?
Bad	Bad	k1gMnSc1
Traunstein	Traunstein	k1gMnSc1
•	•	k?
Bärnkopf	Bärnkopf	k1gMnSc1
•	•	k?
Echsenbach	Echsenbach	k1gMnSc1
•	•	k?
Göpfritz	Göpfritz	k1gMnSc1
an	an	k?
der	drát	k5eAaImRp2nS
Wild	Wild	k1gInSc1
•	•	k?
Grafenschlag	Grafenschlag	k1gInSc1
•	•	k?
Groß	Groß	k1gFnSc2
Gerungs	Gerungsa	k1gFnPc2
•	•	k?
Großgöttfritz	Großgöttfritz	k1gMnSc1
•	•	k?
Gutenbrunn	Gutenbrunn	k1gMnSc1
•	•	k?
Kirchschlag	Kirchschlag	k1gMnSc1
•	•	k?
Kottes-Purk	Kottes-Purk	k1gInSc1
•	•	k?
Langschlag	Langschlag	k1gMnSc1
•	•	k?
Martinsberg	Martinsberg	k1gMnSc1
•	•	k?
Ottenschlag	Ottenschlag	k1gMnSc1
•	•	k?
Pölla	Pölla	k1gMnSc1
•	•	k?
Rappottenstein	Rappottenstein	k1gMnSc1
•	•	k?
Sallingberg	Sallingberg	k1gMnSc1
•	•	k?
Schönbach	Schönbach	k1gInSc1
•	•	k?
Schwarzenau	Schwarzenaus	k1gInSc2
•	•	k?
Schweiggers	Schweiggers	k1gInSc1
•	•	k?
Waldhausen	Waldhausen	k1gInSc1
•	•	k?
Zwettl	Zwettl	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
