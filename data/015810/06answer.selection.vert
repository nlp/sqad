<s>
Ve	v	k7c6
středověku	středověk	k1gInSc6
této	tento	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
dominoval	dominovat	k5eAaImAgInS
hrad	hrad	k1gInSc1
Lozburg	Lozburg	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
ruiny	ruina	k1gFnPc4
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
spatřit	spatřit	k5eAaPmF
z	z	k7c2
hory	hora	k1gFnSc2
Loschberg	Loschberg	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
lese	les	k1gInSc6
u	u	k7c2
části	část	k1gFnSc2
Brand	Brand	k1gInSc4
nalezena	nalezen	k2eAgFnSc1d1
odcizená	odcizený	k2eAgFnSc1d1
Celliniho	Celliniha	k1gFnSc5
slánka	slánka	k1gFnSc1
z	z	k7c2
uměleckohistorického	uměleckohistorický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>