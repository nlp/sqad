<s>
Minoranta	Minorant	k1gMnSc2	Minorant
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
dolní	dolní	k2eAgFnSc1d1	dolní
mez	mez	k1gFnSc1	mez
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
závora	závora	k1gFnSc1	závora
nebo	nebo	k8xC	nebo
dolní	dolní	k2eAgInSc1d1	dolní
odhad	odhad	k1gInSc1	odhad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
matematický	matematický	k2eAgInSc1d1	matematický
pojem	pojem	k1gInSc1	pojem
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
teorie	teorie	k1gFnSc2	teorie
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Minoranta	Minorant	k1gMnSc2	Minorant
se	se	k3xPyFc4	se
definuje	definovat	k5eAaBmIp3nS	definovat
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
A	a	k9	a
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
relací	relace	k1gFnSc7	relace
R	R	kA	R
a	a	k8xC	a
B	B	kA	B
je	být	k5eAaImIp3nS	být
podmnožina	podmnožina	k1gFnSc1	podmnožina
A	a	k9	a
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
prvek	prvek	k1gInSc1	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
∈	∈	k?	∈
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
A	A	kA	A
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
minorantou	minoranta	k1gFnSc7	minoranta
B	B	kA	B
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
když	když	k8xS	když
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
b	b	k?	b
∈	∈	k?	∈
B	B	kA	B
)	)	kIx)	)
(	(	kIx(	(
a	a	k8xC	a
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
⊆	⊆	k?	⊆
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
A	A	kA	A
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
zdola	zdola	k6eAd1	zdola
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
existuje	existovat	k5eAaImIp3nS	existovat
alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
minoranta	minorant	k1gMnSc2	minorant
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
⊆	⊆	k?	⊆
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
A	A	kA	A
<g/>
}	}	kIx)	}
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
definice	definice	k1gFnSc2	definice
nejmenší	malý	k2eAgInSc4d3	nejmenší
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
prvek	prvek	k1gInSc1	prvek
minorantou	minoranta	k1gFnSc7	minoranta
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
pojmu	pojem	k1gInSc2	pojem
minoranty	minorant	k1gMnPc7	minorant
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
definuje	definovat	k5eAaBmIp3nS	definovat
pojem	pojem	k1gInSc1	pojem
infimum	infimum	k1gInSc4	infimum
množiny	množina	k1gFnSc2	množina
jako	jako	k8xC	jako
největší	veliký	k2eAgInSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
množiny	množina	k1gFnSc2	množina
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
třídy	třída	k1gFnSc2	třída
<g/>
)	)	kIx)	)
všech	všecek	k3xTgFnPc2	všecek
minorant	minorant	k1gMnSc1	minorant
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
A	a	k9	a
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgNnPc2	všecek
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
A	A	kA	A
=	=	kIx~	=
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
B	B	kA	B
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgNnPc2	všecek
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
x	x	k?	x
takových	takový	k3xDgInPc2	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
<g/>
2	[number]	k4	2
<	<	kIx(	<
3	[number]	k4	3
a	a	k8xC	a
nechť	nechť	k9	nechť
R	R	kA	R
je	být	k5eAaImIp3nS	být
relace	relace	k1gFnSc1	relace
obvyklého	obvyklý	k2eAgNnSc2d1	obvyklé
ostrého	ostrý	k2eAgNnSc2d1	ostré
uspořádání	uspořádání	k1gNnSc2	uspořádání
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
R	R	kA	R
=	=	kIx~	=
<	<	kIx(	<
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
minorantou	minoranta	k1gFnSc7	minoranta
B	B	kA	B
při	při	k7c6	při
uspořádání	uspořádání	k1gNnSc6	uspořádání
R	R	kA	R
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
číslo	číslo	k1gNnSc1	číslo
-10	-10	k4	-10
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
minorantou	minoranta	k1gFnSc7	minoranta
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
infimem	infim	k1gInSc7	infim
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Majoranta	Majorant	k1gMnSc4	Majorant
Supremum	Supremum	k1gInSc1	Supremum
Infimum	Infimum	k1gNnSc5	Infimum
</s>
