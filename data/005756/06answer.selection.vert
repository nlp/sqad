<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
kraji	kraj	k1gInSc6	kraj
obnášel	obnášet	k5eAaImAgInS	obnášet
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
kolem	kolem	k7c2	kolem
620	[number]	k4	620
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
25,7	[number]	k4	25,7
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
HDP	HDP	kA	HDP
v	v	k7c6	v
tržních	tržní	k2eAgFnPc6d1	tržní
cenách	cena	k1gFnPc6	cena
<g/>
;	;	kIx,	;
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
to	ten	k3xDgNnSc1	ten
činí	činit	k5eAaImIp3nS	činit
226	[number]	k4	226
%	%	kIx~	%
celorepublikového	celorepublikový	k2eAgInSc2d1	celorepublikový
průměru	průměr	k1gInSc2	průměr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
