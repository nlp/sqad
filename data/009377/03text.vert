<p>
<s>
Lučík	lučík	k1gInSc1	lučík
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
trigger	trigger	k1gMnSc1	trigger
guard	guard	k1gMnSc1	guard
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ručních	ruční	k2eAgFnPc2d1	ruční
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
obvykle	obvykle	k6eAd1	obvykle
obloukovitá	obloukovitý	k2eAgFnSc1d1	obloukovitá
součástka	součástka	k1gFnSc1	součástka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chání	chání	k2eAgFnSc1d1	chání
spoušť	spoušť	k1gFnSc1	spoušť
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
i	i	k8xC	i
před	před	k7c7	před
nechtěným	chtěný	k2eNgInSc7d1	nechtěný
výstřelem	výstřel	k1gInSc7	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
užívat	užívat	k5eAaImF	užívat
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Lučík	lučík	k1gInSc1	lučík
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
kovový	kovový	k2eAgInSc1d1	kovový
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
z	z	k7c2	z
plastické	plastický	k2eAgFnSc2d1	plastická
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
upraven	upravit	k5eAaPmNgInS	upravit
pro	pro	k7c4	pro
střelbu	střelba	k1gFnSc4	střelba
jednou	jednou	k6eAd1	jednou
rukou	ruka	k1gFnPc2	ruka
nebo	nebo	k8xC	nebo
oběma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
střelbu	střelba	k1gFnSc4	střelba
v	v	k7c6	v
rukavicích	rukavice	k1gFnPc6	rukavice
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Lučík	lučík	k1gInSc1	lučík
–	–	k?	–
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
a	a	k8xC	a
diskuse	diskuse	k1gFnSc1	diskuse
</s>
</p>
