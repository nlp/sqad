<s>
Lučík	lučík	k1gInSc1	lučík
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
trigger	trigger	k1gMnSc1	trigger
guard	guard	k1gMnSc1	guard
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ručních	ruční	k2eAgFnPc2d1	ruční
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
obvykle	obvykle	k6eAd1	obvykle
obloukovitá	obloukovitý	k2eAgFnSc1d1	obloukovitá
součástka	součástka	k1gFnSc1	součástka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chání	chání	k2eAgFnSc1d1	chání
spoušť	spoušť	k1gFnSc1	spoušť
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
i	i	k8xC	i
před	před	k7c7	před
nechtěným	chtěný	k2eNgInSc7d1	nechtěný
výstřelem	výstřel	k1gInSc7	výstřel
<g/>
.	.	kIx.	.
</s>
