<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
obloukovitá	obloukovitý	k2eAgFnSc1d1	obloukovitá
součástka	součástka	k1gFnSc1	součástka
u	u	k7c2	u
ručních	ruční	k2eAgFnPc2d1	ruční
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
spoušť	spoušť	k1gFnSc4	spoušť
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
i	i	k8xC	i
před	před	k7c7	před
nechtěným	chtěný	k2eNgInSc7d1	nechtěný
výstřelem	výstřel	k1gInSc7	výstřel
<g/>
?	?	kIx.	?
</s>
