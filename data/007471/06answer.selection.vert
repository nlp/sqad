<s>
Káně	káně	k1gFnSc1	káně
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Buteo	Buteo	k1gMnSc1	Buteo
buteo	buteo	k1gMnSc1	buteo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
pod	pod	k7c7	pod
starším	starý	k2eAgInSc7d2	starší
názvem	název	k1gInSc7	název
káně	káně	k1gFnSc1	káně
myšilov	myšilov	k1gMnSc1	myšilov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
dravce	dravec	k1gMnSc2	dravec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jestřábovitých	jestřábovitý	k2eAgFnPc2d1	jestřábovitý
(	(	kIx(	(
<g/>
Accipitridae	Accipitridae	k1gFnPc2	Accipitridae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
