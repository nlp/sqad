<s>
Káně	káně	k1gFnSc1	káně
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Buteo	Buteo	k1gMnSc1	Buteo
buteo	buteo	k1gMnSc1	buteo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
pod	pod	k7c7	pod
starším	starý	k2eAgInSc7d2	starší
názvem	název	k1gInSc7	název
káně	káně	k1gFnSc1	káně
myšilov	myšilov	k1gMnSc1	myšilov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
dravce	dravec	k1gMnSc2	dravec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jestřábovitých	jestřábovitý	k2eAgFnPc2d1	jestřábovitý
(	(	kIx(	(
<g/>
Accipitridae	Accipitridae	k1gFnPc2	Accipitridae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
57	[number]	k4	57
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
113	[number]	k4	113
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
775	[number]	k4	775
<g/>
–	–	k?	–
<g/>
975	[number]	k4	975
g.	g.	k?	g.
Má	mít	k5eAaImIp3nS	mít
kompaktní	kompaktní	k2eAgFnSc4d1	kompaktní
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
široká	široký	k2eAgNnPc4d1	široké
křídla	křídlo	k1gNnPc4	křídlo
s	s	k7c7	s
černými	černý	k2eAgInPc7d1	černý
konci	konec	k1gInPc7	konec
letek	letka	k1gFnPc2	letka
<g/>
,	,	kIx,	,
v	v	k7c6	v
letu	let	k1gInSc6	let
široce	široko	k6eAd1	široko
rozevřený	rozevřený	k2eAgInSc4d1	rozevřený
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
,	,	kIx,	,
zakulacený	zakulacený	k2eAgInSc4d1	zakulacený
ocas	ocas	k1gInSc4	ocas
s	s	k7c7	s
tmavým	tmavý	k2eAgNnSc7d1	tmavé
pruhováním	pruhování	k1gNnSc7	pruhování
a	a	k8xC	a
s	s	k7c7	s
tmavou	tmavý	k2eAgFnSc7d1	tmavá
páskou	páska	k1gFnSc7	páska
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc4d1	žlutá
neopeřené	opeřený	k2eNgFnPc4d1	neopeřená
končetiny	končetina	k1gFnPc4	končetina
a	a	k8xC	a
žlutý	žlutý	k2eAgInSc4d1	žlutý
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
černě	černě	k6eAd1	černě
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
zobák	zobák	k1gInSc4	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
káně	káně	k1gFnPc4	káně
lesní	lesní	k2eAgFnPc4d1	lesní
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejproměnlivějších	proměnlivý	k2eAgNnPc2d3	proměnlivý
zbarvení	zbarvení	k1gNnPc2	zbarvení
opeření	opeření	k1gNnSc2	opeření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
od	od	k7c2	od
čistě	čistě	k6eAd1	čistě
bílé	bílý	k2eAgFnSc2d1	bílá
až	až	k9	až
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
černou	černý	k2eAgFnSc4d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
přitom	přitom	k6eAd1	přitom
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svrchu	svrchu	k6eAd1	svrchu
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
zbarven	zbarven	k2eAgMnSc1d1	zbarven
jednolitě	jednolitě	k6eAd1	jednolitě
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
spodinu	spodina	k1gFnSc4	spodina
těla	tělo	k1gNnSc2	tělo
mívá	mívat	k5eAaImIp3nS	mívat
světlejší	světlý	k2eAgInSc1d2	světlejší
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
pruhovanou	pruhovaný	k2eAgFnSc4d1	pruhovaná
<g/>
.	.	kIx.	.
</s>
<s>
Pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
hrozí	hrozit	k5eAaImIp3nS	hrozit
záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc7d1	podobná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzácnější	vzácný	k2eAgFnSc4d2	vzácnější
kání	kání	k2eAgFnSc4d1	kání
rousnou	rousný	k2eAgFnSc4d1	rousná
(	(	kIx(	(
<g/>
Buteo	Buteo	k1gMnSc1	Buteo
lagopus	lagopus	k1gMnSc1	lagopus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
světlý	světlý	k2eAgInSc4d1	světlý
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
izolovanou	izolovaný	k2eAgFnSc4d1	izolovaná
tmavou	tmavý	k2eAgFnSc4d1	tmavá
skvrnu	skvrna	k1gFnSc4	skvrna
v	v	k7c6	v
ohbí	ohbí	k1gNnSc6	ohbí
spodiny	spodina	k1gFnSc2	spodina
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
včelojedem	včelojed	k1gMnSc7	včelojed
lesním	lesní	k2eAgFnPc3d1	lesní
(	(	kIx(	(
<g/>
Pernis	Pernis	k1gFnSc1	Pernis
apivorus	apivorus	k1gMnSc1	apivorus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
káně	káně	k1gFnSc2	káně
lesní	lesní	k2eAgFnSc2d1	lesní
žluté	žlutý	k2eAgFnSc2d1	žlutá
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
užší	úzký	k2eAgNnPc4d2	užší
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ozývá	ozývat	k5eAaImIp3nS	ozývat
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
mňoukavým	mňoukavý	k2eAgInSc7d1	mňoukavý
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
slyšitelným	slyšitelný	k2eAgMnPc3d1	slyšitelný
"	"	kIx"	"
<g/>
vijé	vijé	k0	vijé
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
často	často	k6eAd1	často
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
sojka	sojka	k1gFnSc1	sojka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Garrulus	Garrulus	k1gMnSc1	Garrulus
glandarius	glandarius	k1gMnSc1	glandarius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
již	již	k9	již
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
žebrají	žebrat	k5eAaImIp3nP	žebrat
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
protáhlým	protáhlý	k2eAgFnPc3d1	protáhlá
"	"	kIx"	"
<g/>
kiiij	kiiít	k5eAaPmRp2nS	kiiít
kiiij	kiiít	k5eAaPmRp2nS	kiiít
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
stává	stávat	k5eAaImIp3nS	stávat
hlubší	hluboký	k2eAgFnSc1d2	hlubší
a	a	k8xC	a
hlasitější	hlasitý	k2eAgFnSc1d2	hlasitější
(	(	kIx(	(
nahrávka	nahrávka	k1gFnSc1	nahrávka
s	s	k7c7	s
varovným	varovný	k2eAgInSc7d1	varovný
hlasem	hlas	k1gInSc7	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
východně	východně	k6eAd1	východně
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
širokého	široký	k2eAgInSc2d1	široký
pruhu	pruh	k1gInSc2	pruh
přes	přes	k7c4	přes
střední	střední	k2eAgFnSc4d1	střední
Asii	Asie	k1gFnSc4	Asie
až	až	k9	až
po	po	k7c4	po
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
populace	populace	k1gFnPc1	populace
především	především	k9	především
stálé	stálý	k2eAgFnPc1d1	stálá
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
těch	ten	k3xDgFnPc2	ten
severských	severský	k2eAgFnPc2d1	severská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asijští	asijský	k2eAgMnPc1d1	asijský
ptáci	pták	k1gMnPc1	pták
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
většinově	většinově	k6eAd1	většinově
migrují	migrovat	k5eAaImIp3nP	migrovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
kontinentu	kontinent	k1gInSc2	kontinent
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
mapka	mapka	k1gFnSc1	mapka
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
káně	káně	k1gFnSc2	káně
lesní	lesní	k2eAgFnSc2d1	lesní
nejhojnějším	hojný	k2eAgMnSc7d3	nejhojnější
dravcem	dravec	k1gMnSc7	dravec
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
jeden	jeden	k4xCgInSc1	jeden
milion	milion	k4xCgInSc1	milion
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
početně	početně	k6eAd1	početně
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
zde	zde	k6eAd1	zde
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
9500	[number]	k4	9500
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
po	po	k7c4	po
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
1300	[number]	k4	1300
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
a	a	k8xC	a
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populačním	populační	k2eAgInSc6d1	populační
trendu	trend	k1gInSc6	trend
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulých	minulý	k2eAgNnPc6d1	Minulé
letech	léto	k1gNnPc6	léto
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
mírný	mírný	k2eAgInSc1d1	mírný
vzestup	vzestup	k1gInSc1	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
zalétává	zalétávat	k5eAaImIp3nS	zalétávat
na	na	k7c4	na
otevřená	otevřený	k2eAgNnPc4d1	otevřené
prostranství	prostranství	k1gNnPc4	prostranství
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
pole	pole	k1gNnPc4	pole
<g/>
,	,	kIx,	,
louky	louka	k1gFnPc4	louka
nebo	nebo	k8xC	nebo
pastviny	pastvina	k1gFnPc4	pastvina
<g/>
;	;	kIx,	;
často	často	k6eAd1	často
vysedává	vysedávat	k5eAaImIp3nS	vysedávat
také	také	k9	také
u	u	k7c2	u
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hnízdního	hnízdní	k2eAgNnSc2d1	hnízdní
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
káně	káně	k1gNnSc4	káně
teritoriální	teritoriální	k2eAgFnSc2d1	teritoriální
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
nadbytkem	nadbytek	k1gInSc7	nadbytek
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
však	však	k9	však
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
menší	malý	k2eAgFnPc4d2	menší
volné	volný	k2eAgFnPc4d1	volná
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
lze	lze	k6eAd1	lze
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
i	i	k9	i
během	během	k7c2	během
tahu	tah	k1gInSc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
díky	díky	k7c3	díky
svým	svůj	k3xOyFgNnPc3	svůj
zásnubním	zásnubní	k2eAgNnPc3d1	zásnubní
letům	léto	k1gNnPc3	léto
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgFnPc6	který
kruhově	kruhově	k6eAd1	kruhově
plachtí	plachtit	k5eAaImIp3nP	plachtit
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
padá	padat	k5eAaImIp3nS	padat
a	a	k8xC	a
opět	opět	k6eAd1	opět
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
potravy	potrava	k1gFnSc2	potrava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
silně	silně	k6eAd1	silně
variabilní	variabilní	k2eAgFnSc4d1	variabilní
podle	podle	k7c2	podle
místních	místní	k2eAgFnPc2d1	místní
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
také	také	k9	také
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
proměnlivému	proměnlivý	k2eAgInSc3d1	proměnlivý
životnímu	životní	k2eAgInSc3d1	životní
prostoru	prostor	k1gInSc3	prostor
káně	káně	k1gFnSc2	káně
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
káně	káně	k1gFnSc2	káně
lesní	lesní	k2eAgFnSc2d1	lesní
tvoří	tvořit	k5eAaImIp3nP	tvořit
převážně	převážně	k6eAd1	převážně
malí	malý	k2eAgMnPc1d1	malý
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
především	především	k9	především
hraboš	hraboš	k1gMnSc1	hraboš
polní	polní	k2eAgMnSc1d1	polní
(	(	kIx(	(
<g/>
Microtus	Microtus	k1gInSc1	Microtus
arvalis	arvalis	k1gFnSc2	arvalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Požírá	požírat	k5eAaImIp3nS	požírat
také	také	k9	také
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
mladé	mladý	k2eAgMnPc4d1	mladý
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
plazy	plaz	k1gMnPc4	plaz
(	(	kIx(	(
<g/>
ještěrky	ještěrka	k1gFnPc4	ještěrka
<g/>
,	,	kIx,	,
hady	had	k1gMnPc4	had
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červy	červ	k1gMnPc7	červ
<g/>
,	,	kIx,	,
obojživelníky	obojživelník	k1gMnPc7	obojživelník
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
žáby	žába	k1gFnSc2	žába
<g/>
)	)	kIx)	)
a	a	k8xC	a
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
mrtvé	mrtvý	k1gMnPc4	mrtvý
nebo	nebo	k8xC	nebo
umírající	umírající	k2eAgMnPc4d1	umírající
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
i	i	k9	i
auty	auto	k1gNnPc7	auto
sražená	sražený	k2eAgNnPc4d1	sražené
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
sama	sám	k3xTgMnSc4	sám
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
kolize	kolize	k1gFnSc2	kolize
s	s	k7c7	s
vozidly	vozidlo	k1gNnPc7	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
Castellu	Castell	k1gInSc2	Castell
v	v	k7c6	v
bavorském	bavorský	k2eAgInSc6d1	bavorský
obvodu	obvod	k1gInSc6	obvod
Dolní	dolní	k2eAgInPc1d1	dolní
Franky	Franky	k1gInPc1	Franky
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
až	až	k9	až
1960	[number]	k4	1960
doloženy	doložit	k5eAaPmNgInP	doložit
384	[number]	k4	384
druhy	druh	k1gInPc1	druh
ukořistěných	ukořistěný	k2eAgNnPc2d1	ukořistěné
zvířat	zvíře	k1gNnPc2	zvíře
na	na	k7c6	na
hnízdech	hnízdo	k1gNnPc6	hnízdo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
myši	myš	k1gFnSc2	myš
<g/>
)	)	kIx)	)
tvořili	tvořit	k5eAaImAgMnP	tvořit
70	[number]	k4	70
%	%	kIx~	%
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
12	[number]	k4	12
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
tvořili	tvořit	k5eAaImAgMnP	tvořit
plazi	plaz	k1gMnPc1	plaz
15	[number]	k4	15
%	%	kIx~	%
a	a	k8xC	a
obojživelníci	obojživelník	k1gMnPc1	obojživelník
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Berlína	Berlín	k1gInSc2	Berlín
provedena	proveden	k2eAgFnSc1d1	provedena
studie	studie	k1gFnSc1	studie
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
složením	složení	k1gNnSc7	složení
potravy	potrava	k1gFnSc2	potrava
u	u	k7c2	u
ještě	ještě	k6eAd1	ještě
neopeřených	opeřený	k2eNgNnPc2d1	neopeřené
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hnízdech	hnízdo	k1gNnPc6	hnízdo
bylo	být	k5eAaImAgNnS	být
jako	jako	k9	jako
kořist	kořist	k1gFnSc4	kořist
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
celkem	celkem	k6eAd1	celkem
257	[number]	k4	257
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
37	[number]	k4	37
%	%	kIx~	%
představovali	představovat	k5eAaImAgMnP	představovat
malí	malý	k2eAgMnPc1d1	malý
savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
především	především	k9	především
hraboši	hraboš	k1gMnPc1	hraboš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
59	[number]	k4	59
%	%	kIx~	%
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
1	[number]	k4	1
%	%	kIx~	%
obojživelníci	obojživelník	k1gMnPc1	obojživelník
a	a	k8xC	a
zbývající	zbývající	k2eAgInPc1d1	zbývající
2	[number]	k4	2
%	%	kIx~	%
pak	pak	k6eAd1	pak
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívá	dospívat	k5eAaImIp3nS	dospívat
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
nebo	nebo	k8xC	nebo
třetím	třetí	k4xOgInSc6	třetí
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
monogamní	monogamní	k2eAgInPc1d1	monogamní
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
spolu	spolu	k6eAd1	spolu
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Rozměrné	rozměrný	k2eAgNnSc1d1	rozměrné
hnízdo	hnízdo	k1gNnSc1	hnízdo
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
buduje	budovat	k5eAaImIp3nS	budovat
zpravidla	zpravidla	k6eAd1	zpravidla
vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
začínají	začínat	k5eAaImIp3nP	začínat
první	první	k4xOgNnPc1	první
vejce	vejce	k1gNnPc1	vejce
klást	klást	k5eAaImF	klást
již	již	k9	již
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
však	však	k9	však
až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
snůšce	snůška	k1gFnSc6	snůška
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgFnPc1	dva
až	až	k8xS	až
tři	tři	k4xCgFnPc1	tři
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgFnPc1	čtyři
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
,	,	kIx,	,
červeno-hnědě	červenonědě	k6eAd1	červeno-hnědě
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
56	[number]	k4	56
×	×	k?	×
45	[number]	k4	45
mm	mm	kA	mm
velká	velký	k2eAgFnSc1d1	velká
vejce	vejce	k1gNnPc4	vejce
vážící	vážící	k2eAgInPc4d1	vážící
asi	asi	k9	asi
60	[number]	k4	60
g.	g.	k?	g.
Jsou	být	k5eAaImIp3nP	být
kladena	kladen	k2eAgNnPc1d1	kladeno
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
dvou	dva	k4xCgInPc2	dva
až	až	k9	až
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
33	[number]	k4	33
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
<g/>
denní	denní	k2eAgFnSc4d1	denní
inkubaci	inkubace	k1gFnSc4	inkubace
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
hnízdo	hnízdo	k1gNnSc4	hnízdo
opouštějí	opouštět	k5eAaImIp3nP	opouštět
po	po	k7c6	po
42	[number]	k4	42
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
a	a	k8xC	a
ještě	ještě	k9	ještě
dalších	další	k2eAgInPc2d1	další
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
jsou	být	k5eAaImIp3nP	být
rodiči	rodič	k1gMnPc7	rodič
krmena	krmen	k2eAgFnSc1d1	krmena
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
hnízdní	hnízdní	k2eAgNnSc4d1	hnízdní
teritorium	teritorium	k1gNnSc4	teritorium
opouštějí	opouštět	k5eAaImIp3nP	opouštět
a	a	k8xC	a
usazují	usazovat	k5eAaImIp3nP	usazovat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
jeho	jeho	k3xOp3gFnPc2	jeho
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
však	však	k9	však
zaznamenány	zaznamenán	k2eAgInPc1d1	zaznamenán
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mládě	mládě	k1gNnSc1	mládě
uchýlilo	uchýlit	k5eAaPmAgNnS	uchýlit
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
celých	celý	k2eAgInPc2d1	celý
200	[number]	k4	200
km	km	kA	km
od	od	k7c2	od
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
mladých	mladý	k2eAgMnPc2d1	mladý
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
činí	činit	k5eAaImIp3nS	činit
51	[number]	k4	51
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
32	[number]	k4	32
%	%	kIx~	%
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
roce	rok	k1gInSc6	rok
29	[number]	k4	29
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dožívá	dožívat	k5eAaImIp3nS	dožívat
asi	asi	k9	asi
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
káně	káně	k1gFnSc1	káně
kroužkovaná	kroužkovaný	k2eAgFnSc1d1	kroužkovaná
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byla	být	k5eAaImAgFnS	být
sražena	srazit	k5eAaPmNgFnS	srazit
autem	aut	k1gInSc7	aut
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
17	[number]	k4	17
let	léto	k1gNnPc2	léto
a	a	k8xC	a
1	[number]	k4	1
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgMnSc1d3	nejstarší
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
žijící	žijící	k2eAgMnPc1d1	žijící
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
dožili	dožít	k5eAaPmAgMnP	dožít
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ji	on	k3xPp3gFnSc4	on
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
především	především	k9	především
pronásledování	pronásledování	k1gNnSc1	pronásledování
<g/>
,	,	kIx,	,
kolize	kolize	k1gFnPc1	kolize
s	s	k7c7	s
vozidly	vozidlo	k1gNnPc7	vozidlo
<g/>
,	,	kIx,	,
zranění	zranění	k1gNnSc1	zranění
na	na	k7c6	na
drátech	drát	k1gInPc6	drát
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
rušení	rušení	k1gNnSc3	rušení
na	na	k7c6	na
hnízdištích	hnízdiště	k1gNnPc6	hnízdiště
a	a	k8xC	a
kácení	kácení	k1gNnSc6	kácení
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mnohdy	mnohdy	k6eAd1	mnohdy
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Káně	káně	k1gFnSc1	káně
lesní	lesní	k2eAgFnSc1d1	lesní
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
28	[number]	k4	28
zástupců	zástupce	k1gMnPc2	zástupce
rodu	rod	k1gInSc2	rod
Buteo	Buteo	k1gMnSc1	Buteo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Eurasii	Eurasie	k1gFnSc6	Eurasie
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
10	[number]	k4	10
druhy	druh	k1gInPc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gInPc7	její
nejblíže	blízce	k6eAd3	blízce
příbuznými	příbuzný	k1gMnPc7	příbuzný
jsou	být	k5eAaImIp3nP	být
káně	káně	k1gFnSc1	káně
bělochvostá	bělochvostý	k2eAgFnSc1d1	bělochvostý
(	(	kIx(	(
<g/>
B.	B.	kA	B.
rufinus	rufinus	k1gInSc1	rufinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k.	k.	k?	k.
stepní	stepní	k2eAgInPc1d1	stepní
(	(	kIx(	(
<g/>
B.	B.	kA	B.
hemilasius	hemilasius	k1gInSc1	hemilasius
<g/>
)	)	kIx)	)
a	a	k8xC	a
k.	k.	k?	k.
horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
B.	B.	kA	B.
oreophilus	oreophilus	k1gInSc1	oreophilus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yQgNnPc7	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
superdruh	superdruh	k1gInSc1	superdruh
<g/>
.	.	kIx.	.
</s>
<s>
Rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
11	[number]	k4	11
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
:	:	kIx,	:
B.	B.	kA	B.
b.	b.	k?	b.
buteo	buteo	k1gMnSc1	buteo
–	–	k?	–
Evropa	Evropa	k1gFnSc1	Evropa
východně	východně	k6eAd1	východně
po	po	k7c4	po
Finsko	Finsko	k1gNnSc4	Finsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
izolovaně	izolovaně	k6eAd1	izolovaně
žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
<g/>
;	;	kIx,	;
zimuje	zimovat	k5eAaImIp3nS	zimovat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
svého	svůj	k3xOyFgInSc2	svůj
areálu	areál	k1gInSc2	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
arrigonii	arrigonie	k1gFnSc6	arrigonie
–	–	k?	–
Korsika	Korsika	k1gFnSc1	Korsika
a	a	k8xC	a
Sardinie	Sardinie	k1gFnSc1	Sardinie
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
rothschildi	rothschildit	k5eAaPmRp2nS	rothschildit
–	–	k?	–
Azory	Azory	k1gFnPc4	Azory
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
insularum	insularum	k1gInSc1	insularum
–	–	k?	–
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
bannermani	bannerman	k1gMnPc1	bannerman
–	–	k?	–
souostroví	souostroví	k1gNnSc2	souostroví
Kapverdy	Kapverda	k1gFnSc2	Kapverda
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
vulpinus	vulpinus	k1gInSc1	vulpinus
–	–	k?	–
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
severní	severní	k2eAgFnSc2d1	severní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
východně	východně	k6eAd1	východně
po	po	k7c4	po
řeku	řeka	k1gFnSc4	řeka
Jenisej	Jenisej	k1gInSc4	Jenisej
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
po	po	k7c4	po
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
;	;	kIx,	;
zimuje	zimovat	k5eAaImIp3nS	zimovat
především	především	k9	především
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
menetriesi	menetriese	k1gFnSc6	menetriese
–	–	k?	–
Krym	Krym	k1gInSc1	Krym
a	a	k8xC	a
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
po	po	k7c4	po
východní	východní	k2eAgNnSc4d1	východní
Turecko	Turecko	k1gNnSc4	Turecko
a	a	k8xC	a
severní	severní	k2eAgInSc4d1	severní
Írán	Írán	k1gInSc4	Írán
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
japonicus	japonicus	k1gInSc1	japonicus
–	–	k?	–
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
a	a	k8xC	a
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
východně	východně	k6eAd1	východně
přes	přes	k7c4	přes
Amur	Amur	k1gInSc4	Amur
až	až	k9	až
po	po	k7c4	po
Sachalin	Sachalin	k1gInSc4	Sachalin
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
po	po	k7c4	po
Tibet	Tibet	k1gInSc4	Tibet
<g/>
;	;	kIx,	;
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
refectus	refectus	k1gInSc1	refectus
–	–	k?	–
západní	západní	k2eAgFnSc1d1	západní
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
toyoshimai	toyoshimai	k6eAd1	toyoshimai
–	–	k?	–
ostrovy	ostrov	k1gInPc1	ostrov
Izu	Izu	k1gFnSc7	Izu
<g/>
,	,	kIx,	,
Boninské	Boninský	k2eAgInPc4d1	Boninský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
b.	b.	k?	b.
oshiroi	oshiro	k1gFnSc2	oshiro
–	–	k?	–
Borodinské	Borodinský	k2eAgInPc4d1	Borodinský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
