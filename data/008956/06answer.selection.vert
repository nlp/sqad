<s>
Jednání	jednání	k1gNnSc1	jednání
selhala	selhat	k5eAaPmAgFnS	selhat
kvůli	kvůli	k7c3	kvůli
sovětské	sovětský	k2eAgFnSc3d1	sovětská
neústupnosti	neústupnost	k1gFnSc3	neústupnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
žádal	žádat	k5eAaImAgInS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
průchod	průchod	k1gInSc4	průchod
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
přes	přes	k7c4	přes
svá	svůj	k3xOyFgNnPc4	svůj
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
