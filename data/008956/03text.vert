<p>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
byla	být	k5eAaImAgFnS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
operace	operace	k1gFnSc1	operace
zahájená	zahájený	k2eAgFnSc1d1	zahájená
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
bez	bez	k7c2	bez
formálního	formální	k2eAgNnSc2d1	formální
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Šestnáct	šestnáct	k4xCc1	šestnáct
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgInS	provést
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
útok	útok	k1gInSc1	útok
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
skončila	skončit	k5eAaPmAgFnS	skončit
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
rozdělením	rozdělení	k1gNnSc7	rozdělení
území	území	k1gNnSc4	území
Polska	Polsko	k1gNnSc2	Polsko
mezi	mezi	k7c4	mezi
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
zánikem	zánik	k1gInSc7	zánik
Druhé	druhý	k4xOgFnSc2	druhý
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
zahájil	zahájit	k5eAaPmAgInS	zahájit
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
o	o	k7c4	o
spojenectví	spojenectví	k1gNnSc4	spojenectví
proti	proti	k7c3	proti
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
selhala	selhat	k5eAaPmAgFnS	selhat
kvůli	kvůli	k7c3	kvůli
sovětské	sovětský	k2eAgFnSc3d1	sovětská
neústupnosti	neústupnost	k1gFnSc3	neústupnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
žádal	žádat	k5eAaImAgInS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
průchod	průchod	k1gInSc4	průchod
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
přes	přes	k7c4	přes
svá	svůj	k3xOyFgNnPc4	svůj
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
výsledek	výsledek	k1gInSc1	výsledek
zmíněných	zmíněný	k2eAgNnPc2d1	zmíněné
jednání	jednání	k1gNnSc2	jednání
přivedl	přivést	k5eAaPmAgInS	přivést
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
tajný	tajný	k2eAgInSc4d1	tajný
protokol	protokol	k1gInSc4	protokol
rozdělující	rozdělující	k2eAgFnSc2d1	rozdělující
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
po	po	k7c6	po
podpisu	podpis	k1gInSc6	podpis
smlouvy	smlouva	k1gFnSc2	smlouva
německá	německý	k2eAgNnPc1d1	německé
vojska	vojsko	k1gNnPc1	vojsko
vtrhla	vtrhnout	k5eAaPmAgNnP	vtrhnout
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
západu	západ	k1gInSc2	západ
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
dopracovaných	dopracovaný	k2eAgInPc2d1	dopracovaný
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
Plan	plan	k1gInSc1	plan
Zachód	Zachód	k1gInSc1	Zachód
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgNnP	mít
polská	polský	k2eAgNnPc1d1	polské
vojska	vojsko	k1gNnPc1	vojsko
ustoupit	ustoupit	k5eAaPmF	ustoupit
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
obrana	obrana	k1gFnSc1	obrana
Rumunského	rumunský	k2eAgNnSc2d1	rumunské
předmostí	předmostí	k1gNnSc2	předmostí
<g/>
,	,	kIx,	,
a	a	k8xC	a
očekávat	očekávat	k5eAaImF	očekávat
britskou	britský	k2eAgFnSc4d1	britská
a	a	k8xC	a
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
provedla	provést	k5eAaPmAgFnS	provést
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Kresy	Kresa	k1gFnSc2	Kresa
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
ujednáními	ujednání	k1gNnPc7	ujednání
tajného	tajný	k2eAgInSc2d1	tajný
protokolu	protokol	k1gInSc2	protokol
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
činí	činit	k5eAaImIp3nS	činit
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
a	a	k8xC	a
Bělorusů	Bělorus	k1gMnPc2	Bělorus
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
částech	část	k1gFnPc6	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
polský	polský	k2eAgInSc1d1	polský
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
zaručit	zaručit	k5eAaPmF	zaručit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
vlastních	vlastní	k2eAgMnPc2d1	vlastní
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
konfrontována	konfrontován	k2eAgFnSc1d1	konfrontována
s	s	k7c7	s
otevřením	otevření	k1gNnSc7	otevření
druhé	druhý	k4xOgFnSc2	druhý
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
vyhodnotila	vyhodnotit	k5eAaPmAgFnS	vyhodnotit
obranu	obrana	k1gFnSc4	obrana
Rumunského	rumunský	k2eAgNnSc2d1	rumunské
předmostí	předmostí	k1gNnSc2	předmostí
jako	jako	k9	jako
neudržitelnou	udržitelný	k2eNgFnSc4d1	neudržitelná
a	a	k8xC	a
nařídila	nařídit	k5eAaPmAgFnS	nařídit
urychlenou	urychlený	k2eAgFnSc4d1	urychlená
evakuaci	evakuace	k1gFnSc4	evakuace
všech	všecek	k3xTgFnPc2	všecek
jednotek	jednotka	k1gFnPc2	jednotka
do	do	k7c2	do
neutrálního	neutrální	k2eAgNnSc2d1	neutrální
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
převahou	převaha	k1gFnSc7	převaha
překonala	překonat	k5eAaPmAgFnS	překonat
polský	polský	k2eAgInSc4d1	polský
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svých	svůj	k3xOyFgInPc2	svůj
operačních	operační	k2eAgInPc2d1	operační
cílů	cíl	k1gInPc2	cíl
a	a	k8xC	a
vzala	vzít	k5eAaPmAgFnS	vzít
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
přibližně	přibližně	k6eAd1	přibližně
230	[number]	k4	230
000	[number]	k4	000
polských	polský	k2eAgMnPc2d1	polský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
území	území	k1gNnSc2	území
obsazená	obsazený	k2eAgFnSc1d1	obsazená
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
anektoval	anektovat	k5eAaBmAgInS	anektovat
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1939	[number]	k4	1939
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
13,5	[number]	k4	13,5
milionu	milion	k4xCgInSc2	milion
dřívějších	dřívější	k2eAgMnPc2d1	dřívější
polských	polský	k2eAgMnPc2d1	polský
občanů	občan	k1gMnPc2	občan
žijících	žijící	k2eAgInPc2d1	žijící
na	na	k7c6	na
anektovaném	anektovaný	k2eAgNnSc6d1	anektované
území	území	k1gNnSc6	území
stalo	stát	k5eAaPmAgNnS	stát
občany	občan	k1gMnPc4	občan
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Neprodleně	prodleně	k6eNd1	prodleně
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
kampaň	kampaň	k1gFnSc1	kampaň
sovětizace	sovětizace	k1gFnSc1	sovětizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
zmanipulované	zmanipulovaný	k2eAgFnPc4d1	zmanipulovaná
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
výsledek	výsledek	k1gInSc1	výsledek
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
využila	využít	k5eAaPmAgFnS	využít
k	k	k7c3	k
ospravedlnění	ospravedlnění	k1gNnSc3	ospravedlnění
anexe	anexe	k1gFnSc2	anexe
východních	východní	k2eAgNnPc2d1	východní
polských	polský	k2eAgNnPc2d1	polské
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Hromadné	hromadný	k2eAgFnPc4d1	hromadná
popravy	poprava	k1gFnPc4	poprava
a	a	k8xC	a
tisíce	tisíc	k4xCgInPc1	tisíc
uvězněných	uvězněný	k2eAgMnPc2d1	uvězněný
posloužily	posloužit	k5eAaPmAgInP	posloužit
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Stovky	stovka	k1gFnPc1	stovka
tisíc	tisíc	k4xCgInSc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
byly	být	k5eAaImAgInP	být
deportovány	deportovat	k5eAaBmNgInP	deportovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
až	až	k9	až
1941	[number]	k4	1941
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
do	do	k7c2	do
okrajových	okrajový	k2eAgFnPc2d1	okrajová
částí	část	k1gFnPc2	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
největších	veliký	k2eAgFnPc6d3	veliký
vlnách	vlna	k1gFnPc6	vlna
<g/>
.	.	kIx.	.
<g/>
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
síly	síla	k1gFnPc1	síla
okupovaly	okupovat	k5eAaBmAgFnP	okupovat
východní	východní	k2eAgNnSc4d1	východní
Polsko	Polsko	k1gNnSc4	Polsko
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
vytlačeny	vytlačit	k5eAaPmNgInP	vytlačit
německými	německý	k2eAgFnPc7d1	německá
jednotkami	jednotka	k1gFnPc7	jednotka
postupujícími	postupující	k2eAgFnPc7d1	postupující
na	na	k7c4	na
východ	východ	k1gInSc1	východ
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Operace	operace	k1gFnSc2	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
zůstala	zůstat	k5eAaPmAgFnS	zůstat
okupována	okupovat	k5eAaBmNgFnS	okupovat
nacisty	nacista	k1gMnPc7	nacista
až	až	k6eAd1	až
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Dohody	dohoda	k1gFnPc1	dohoda
učiněné	učiněný	k2eAgFnPc1d1	učiněná
v	v	k7c6	v
Jaltě	Jalta	k1gFnSc6	Jalta
dovolily	dovolit	k5eAaPmAgFnP	dovolit
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
připojit	připojit	k5eAaPmF	připojit
téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgNnPc1	všechen
polská	polský	k2eAgNnPc1d1	polské
území	území	k1gNnPc1	území
obsazená	obsazený	k2eAgNnPc1d1	obsazené
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
náhradu	náhrada	k1gFnSc4	náhrada
dostala	dostat	k5eAaPmAgFnS	dostat
Polská	polský	k2eAgFnSc1d1	polská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
přibližně	přibližně	k6eAd1	přibližně
polovinu	polovina	k1gFnSc4	polovina
území	území	k1gNnSc2	území
Východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
a	a	k8xC	a
také	také	k9	také
území	území	k1gNnSc4	území
východně	východně	k6eAd1	východně
od	od	k7c2	od
linie	linie	k1gFnSc2	linie
Odra-Nisa	Odra-Nis	k1gMnSc2	Odra-Nis
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
většinu	většina	k1gFnSc4	většina
anektovaných	anektovaný	k2eAgFnPc2d1	anektovaná
území	území	k1gNnSc4	území
do	do	k7c2	do
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
a	a	k8xC	a
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
sovětské	sovětský	k2eAgFnSc2d1	sovětská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
na	na	k7c6	na
průběhu	průběh	k1gInSc6	průběh
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
uznávala	uznávat	k5eAaImAgFnS	uznávat
status	status	k1gInSc4	status
quo	quo	k?	quo
jako	jako	k8xC	jako
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
oblasti	oblast	k1gFnSc2	oblast
okolo	okolo	k7c2	okolo
Białystoku	Białystok	k1gInSc2	Białystok
a	a	k8xC	a
malých	malý	k2eAgFnPc2d1	malá
částí	část	k1gFnPc2	část
Haliče	Halič	k1gFnSc2	Halič
východně	východně	k6eAd1	východně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
San	San	k1gFnSc2	San
okolo	okolo	k7c2	okolo
Przemyślu	Przemyśl	k1gInSc2	Przemyśl
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
navráceny	navrátit	k5eAaPmNgFnP	navrátit
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předcházející	předcházející	k2eAgFnSc3d1	předcházející
události	událost	k1gFnSc3	událost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historické	historický	k2eAgNnSc4d1	historické
pozadí	pozadí	k1gNnSc4	pozadí
===	===	k?	===
</s>
</p>
<p>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
učinil	učinit	k5eAaImAgInS	učinit
jen	jen	k9	jen
málo	málo	k1gNnSc4	málo
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
územních	územní	k2eAgFnPc2d1	územní
aspirací	aspirace	k1gFnPc2	aspirace
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Józef	Józef	k1gInSc1	Józef
Piłsudski	Piłsudsk	k1gFnSc2	Piłsudsk
požadoval	požadovat	k5eAaImAgInS	požadovat
posun	posun	k1gInSc1	posun
polských	polský	k2eAgFnPc2d1	polská
hranic	hranice	k1gFnPc2	hranice
co	co	k9	co
nejdále	daleko	k6eAd3	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Polskem	Polsko	k1gNnSc7	Polsko
vedenou	vedený	k2eAgFnSc4d1	vedená
federaci	federace	k1gFnSc4	federace
jako	jako	k8xC	jako
protiváhu	protiváha	k1gFnSc4	protiváha
imperiálním	imperiální	k2eAgFnPc3d1	imperiální
ambicím	ambice	k1gFnPc3	ambice
Ruska	Rusko	k1gNnSc2	Rusko
či	či	k8xC	či
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
bolševici	bolševik	k1gMnPc1	bolševik
začali	začít	k5eAaPmAgMnP	začít
získávat	získávat	k5eAaImF	získávat
převahu	převaha	k1gFnSc4	převaha
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
zahájili	zahájit	k5eAaPmAgMnP	zahájit
postup	postup	k1gInSc4	postup
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sporná	sporný	k2eAgNnPc4d1	sporné
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Pohraniční	pohraniční	k2eAgInPc1d1	pohraniční
střety	střet	k1gInPc1	střet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
polsko-sovětskou	polskoovětský	k2eAgFnSc4d1	polsko-sovětská
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
polském	polský	k2eAgNnSc6d1	polské
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Varšavy	Varšava	k1gFnSc2	Varšava
Sověti	Sovět	k1gMnPc1	Sovět
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
mírová	mírový	k2eAgNnPc4d1	Mírové
jednání	jednání	k1gNnSc4	jednání
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
příměřím	příměří	k1gNnSc7	příměří
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnPc1	strana
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1921	[number]	k4	1921
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
rozdělila	rozdělit	k5eAaPmAgNnP	rozdělit
sporná	sporný	k2eAgNnPc1d1	sporné
území	území	k1gNnPc1	území
mezi	mezi	k7c4	mezi
Polsko	Polsko	k1gNnSc4	Polsko
a	a	k8xC	a
Sovětské	sovětský	k2eAgNnSc4d1	sovětské
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
polské	polský	k2eAgInPc4d1	polský
mírové	mírový	k2eAgInPc4d1	mírový
delegaci	delegace	k1gFnSc4	delegace
značné	značný	k2eAgInPc4d1	značný
územní	územní	k2eAgInPc4d1	územní
ústupky	ústupek	k1gInPc4	ústupek
a	a	k8xC	a
meziválečná	meziválečný	k2eAgFnSc1d1	meziválečná
sovětsko-polská	sovětskoolský	k2eAgFnSc1d1	sovětsko-polská
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
přibližovala	přibližovat	k5eAaImAgFnS	přibližovat
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Polsko-litevskou	polskoitevský	k2eAgFnSc7d1	polsko-litevská
unií	unie	k1gFnSc7	unie
před	před	k7c7	před
prvním	první	k4xOgNnSc7	první
dělením	dělení	k1gNnSc7	dělení
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1772	[number]	k4	1772
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
bylo	být	k5eAaImAgNnS	být
opuštění	opuštění	k1gNnSc1	opuštění
teze	teze	k1gFnSc2	teze
o	o	k7c4	o
vítězství	vítězství	k1gNnSc4	vítězství
bolševické	bolševický	k2eAgFnSc2d1	bolševická
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
byl	být	k5eAaImAgInS	být
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
vůdci	vůdce	k1gMnPc1	vůdce
na	na	k7c4	na
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
pozapomenut	pozapomenut	k2eAgInSc1d1	pozapomenut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
smlouvy	smlouva	k1gFnSc2	smlouva
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
čelit	čelit	k5eAaImF	čelit
hrozící	hrozící	k2eAgFnSc3d1	hrozící
německé	německý	k2eAgFnSc3d1	německá
agresi	agrese	k1gFnSc3	agrese
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
s	s	k7c7	s
výměnou	výměna	k1gFnSc7	výměna
návrhů	návrh	k1gInPc2	návrh
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
možné	možný	k2eAgFnSc2d1	možná
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc2d1	vojenská
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
se	se	k3xPyFc4	se
rozhovorů	rozhovor	k1gInPc2	rozhovor
neúčastnilo	účastnit	k5eNaImAgNnS	účastnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
obávalo	obávat	k5eAaImAgNnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakékoli	jakýkoli	k3yIgNnSc1	jakýkoli
sblížení	sblížení	k1gNnSc1	sblížení
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
by	by	kYmCp3nS	by
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
německou	německý	k2eAgFnSc4d1	německá
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Trojstranné	trojstranný	k2eAgInPc1d1	trojstranný
rozhovory	rozhovor	k1gInPc1	rozhovor
se	se	k3xPyFc4	se
zaměřily	zaměřit	k5eAaPmAgInP	zaměřit
na	na	k7c4	na
případné	případný	k2eAgFnPc4d1	případná
záruky	záruka	k1gFnPc4	záruka
poskytnuté	poskytnutý	k2eAgFnPc4d1	poskytnutá
středoevropským	středoevropský	k2eAgFnPc3d1	středoevropská
a	a	k8xC	a
východoevropským	východoevropský	k2eAgFnPc3d1	východoevropská
zemím	zem	k1gFnPc3	zem
v	v	k7c6	v
případě	případ	k1gInSc6	případ
německé	německý	k2eAgFnSc2d1	německá
agrese	agrese	k1gFnSc2	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
nedůvěřovali	důvěřovat	k5eNaImAgMnP	důvěřovat
britským	britský	k2eAgMnPc3d1	britský
ani	ani	k8xC	ani
francouzským	francouzský	k2eAgFnPc3d1	francouzská
nabídkám	nabídka	k1gFnPc3	nabídka
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
proti	proti	k7c3	proti
nacionalistům	nacionalista	k1gMnPc3	nacionalista
v	v	k7c6	v
době	doba	k1gFnSc6	doba
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
uchránit	uchránit	k5eAaPmF	uchránit
Československo	Československo	k1gNnSc4	Československo
před	před	k7c7	před
expanzí	expanze	k1gFnSc7	expanze
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
též	též	k9	též
podezřívali	podezřívat	k5eAaImAgMnP	podezřívat
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Británii	Británie	k1gFnSc4	Británie
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
držely	držet	k5eAaImAgFnP	držet
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
případném	případný	k2eAgInSc6d1	případný
německo-sovětském	německoovětský	k2eAgInSc6d1	německo-sovětský
konfliktu	konflikt	k1gInSc6	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sověti	Sovět	k1gMnPc1	Sovět
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
pevné	pevný	k2eAgNnSc4d1	pevné
vojenské	vojenský	k2eAgNnSc4d1	vojenské
spojenectví	spojenectví	k1gNnSc4	spojenectví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
případě	případ	k1gInSc6	případ
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
táhnoucí	táhnoucí	k2eAgFnSc1d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
Finska	Finsko	k1gNnSc2	Finsko
po	po	k7c4	po
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
nárazníková	nárazníkový	k2eAgFnSc1d1	nárazníková
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
přijít	přijít	k5eAaPmF	přijít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
či	či	k8xC	či
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
navrhované	navrhovaný	k2eAgFnSc6d1	navrhovaná
sféře	sféra	k1gFnSc6	sféra
sovětského	sovětský	k2eAgInSc2d1	sovětský
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
rovněž	rovněž	k9	rovněž
požadoval	požadovat	k5eAaImAgInS	požadovat
právo	právo	k1gNnSc4	právo
intervenovat	intervenovat	k5eAaImF	intervenovat
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
ohrožena	ohrožen	k2eAgFnSc1d1	ohrožena
jejich	jejich	k3xOp3gFnSc4	jejich
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovory	rozhovor	k1gInPc1	rozhovor
o	o	k7c6	o
vojenské	vojenský	k2eAgFnSc6d1	vojenská
spolupráci	spolupráce	k1gFnSc6	spolupráce
zahájené	zahájený	k2eAgFnSc2d1	zahájená
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
uvázly	uváznout	k5eAaPmAgFnP	uváznout
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Sověti	Sovět	k1gMnPc1	Sovět
požadovali	požadovat	k5eAaImAgMnP	požadovat
průchod	průchod	k1gInSc4	průchod
jednotek	jednotka	k1gFnPc2	jednotka
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
případě	případ	k1gInSc6	případ
německého	německý	k2eAgInSc2d1	německý
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
snažili	snažit	k5eAaImAgMnP	snažit
přimět	přimět	k5eAaPmF	přimět
polskou	polský	k2eAgFnSc4d1	polská
vládu	vláda	k1gFnSc4	vláda
k	k	k7c3	k
souhlasu	souhlas	k1gInSc3	souhlas
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnPc1	jednání
nepokračovala	pokračovat	k5eNaImAgNnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nP	kdyby
umožnili	umožnit	k5eAaPmAgMnP	umožnit
vstup	vstup	k1gInSc4	vstup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
na	na	k7c4	na
polská	polský	k2eAgNnPc4d1	polské
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
sovětské	sovětský	k2eAgFnPc4d1	sovětská
jednotky	jednotka	k1gFnPc4	jednotka
by	by	kYmCp3nS	by
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
neodešly	odejít	k5eNaPmAgFnP	odejít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
vládní	vládní	k2eAgNnPc1d1	vládní
místa	místo	k1gNnPc1	místo
souhlas	souhlas	k1gInSc4	souhlas
odepřela	odepřít	k5eAaPmAgFnS	odepřít
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
nebrat	brat	k5eNaPmF	brat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
polská	polský	k2eAgFnSc1d1	polská
stanoviska	stanovisko	k1gNnPc4	stanovisko
a	a	k8xC	a
dokončit	dokončit	k5eAaPmF	dokončit
trojstranná	trojstranný	k2eAgNnPc1d1	trojstranné
jednání	jednání	k1gNnPc1	jednání
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
polské	polský	k2eAgFnPc4d1	polská
námitky	námitka	k1gFnPc4	námitka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
Britové	Brit	k1gMnPc1	Brit
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc1	takový
krok	krok	k1gInSc1	krok
by	by	kYmCp3nS	by
přiměl	přimět	k5eAaPmAgInS	přimět
Polsko	Polsko	k1gNnSc4	Polsko
posílit	posílit	k5eAaPmF	posílit
německo-polské	německoolský	k2eAgInPc4d1	německo-polský
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
<g/>
Mezitím	mezitím	k6eAd1	mezitím
němečtí	německý	k2eAgMnPc1d1	německý
představitelé	představitel	k1gMnPc1	představitel
tajně	tajně	k6eAd1	tajně
naznačovali	naznačovat	k5eAaImAgMnP	naznačovat
sovětským	sovětský	k2eAgMnPc3d1	sovětský
diplomatům	diplomat	k1gMnPc3	diplomat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Německo	Německo	k1gNnSc1	Německo
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
lepší	dobrý	k2eAgFnPc4d2	lepší
podmínky	podmínka	k1gFnPc4	podmínka
dohody	dohoda	k1gFnSc2	dohoda
než	než	k8xS	než
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
zahájili	zahájit	k5eAaPmAgMnP	zahájit
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
jednání	jednání	k1gNnSc2	jednání
o	o	k7c6	o
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
trojstranná	trojstranný	k2eAgNnPc1d1	trojstranné
jednání	jednání	k1gNnPc1	jednání
zůstávala	zůstávat	k5eAaImAgNnP	zůstávat
neuzavřena	uzavřít	k5eNaPmNgNnP	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
byla	být	k5eAaImAgFnS	být
dojednána	dojednán	k2eAgFnSc1d1	dojednána
většina	většina	k1gFnSc1	většina
podmínek	podmínka	k1gFnPc2	podmínka
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
sovětsko-německé	sovětskoěmecký	k2eAgFnSc2d1	sovětsko-německý
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
projednána	projednán	k2eAgFnSc1d1	projednána
možná	možný	k2eAgFnSc1d1	možná
politická	politický	k2eAgFnSc1d1	politická
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnPc1	strana
dospěly	dochvít	k5eAaPmAgFnP	dochvít
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
ohledně	ohledně	k7c2	ohledně
obchodní	obchodní	k2eAgFnSc2d1	obchodní
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
plánovala	plánovat	k5eAaImAgFnS	plánovat
výměnu	výměna	k1gFnSc4	výměna
surovin	surovina	k1gFnPc2	surovina
za	za	k7c4	za
německé	německý	k2eAgFnPc4d1	německá
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnPc4d1	vojenská
technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
stroje	stroj	k1gInPc4	stroj
pro	pro	k7c4	pro
civilní	civilní	k2eAgFnSc4d1	civilní
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
Sověti	Sovět	k1gMnPc1	Sovět
zastavili	zastavit	k5eAaPmAgMnP	zastavit
trojstranné	trojstranný	k2eAgInPc4d1	trojstranný
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
podepsaly	podepsat	k5eAaPmAgInP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
vojenské	vojenský	k2eAgFnSc6d1	vojenská
a	a	k8xC	a
politické	politický	k2eAgFnSc6d1	politická
spolupráci	spolupráce	k1gFnSc6	spolupráce
–	–	k?	–
pakt	pakt	k1gInSc1	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
obchodní	obchodní	k2eAgFnSc4d1	obchodní
dohodu	dohoda	k1gFnSc4	dohoda
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
tajný	tajný	k2eAgInSc4d1	tajný
protokol	protokol	k1gInSc4	protokol
rozdělující	rozdělující	k2eAgInPc1d1	rozdělující
státy	stát	k1gInPc1	stát
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
vlivových	vlivový	k2eAgFnPc2d1	vlivová
sfér	sféra	k1gFnPc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
sféra	sféra	k1gFnSc1	sféra
původně	původně	k6eAd1	původně
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc4	Estonsko
a	a	k8xC	a
Finsko	Finsko	k1gNnSc4	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnSc2	oblast
východně	východně	k6eAd1	východně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Pisy	Pisa	k1gFnSc2	Pisa
<g/>
,	,	kIx,	,
Narew	Narew	k1gFnSc2	Narew
<g/>
,	,	kIx,	,
Visly	Visla	k1gFnSc2	Visla
a	a	k8xC	a
Sanu	Sanus	k1gInSc2	Sanus
připadly	připadnout	k5eAaPmAgInP	připadnout
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
zvětšili	zvětšit	k5eAaPmAgMnP	zvětšit
obranný	obranný	k2eAgInSc4d1	obranný
prostor	prostor	k1gInSc4	prostor
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
možnost	možnost	k1gFnSc4	možnost
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
území	území	k1gNnPc4	území
odstoupená	odstoupený	k2eAgNnPc4d1	odstoupené
Rižským	rižský	k2eAgInSc7d1	rižský
mírem	mír	k1gInSc7	mír
a	a	k8xC	a
sjednotit	sjednotit	k5eAaPmF	sjednotit
Bělorusy	Bělorus	k1gMnPc4	Bělorus
a	a	k8xC	a
Ukrajince	Ukrajinec	k1gMnPc4	Ukrajinec
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
<g/>
Den	den	k1gInSc1	den
po	po	k7c6	po
podpisu	podpis	k1gInSc6	podpis
paktu	pakt	k1gInSc2	pakt
francouzští	francouzštit	k5eAaImIp3nP	francouzštit
a	a	k8xC	a
britští	britský	k2eAgMnPc1d1	britský
vojenští	vojenský	k2eAgMnPc1d1	vojenský
vyjednavači	vyjednavač	k1gMnPc1	vyjednavač
naléhavě	naléhavě	k6eAd1	naléhavě
požadovali	požadovat	k5eAaImAgMnP	požadovat
setkání	setkání	k1gNnSc4	setkání
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
sovětským	sovětský	k2eAgMnSc7d1	sovětský
partnerem	partner	k1gMnSc7	partner
Vorošilovem	Vorošilov	k1gInSc7	Vorošilov
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
jim	on	k3xPp3gMnPc3	on
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vzkázal	vzkázat	k5eAaPmAgMnS	vzkázat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
změněné	změněný	k2eAgFnSc2d1	změněná
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
nemůže	moct	k5eNaImIp3nS	moct
z	z	k7c2	z
pokračujících	pokračující	k2eAgInPc2d1	pokračující
rozhovorů	rozhovor	k1gInPc2	rozhovor
vzejít	vzejít	k5eAaPmF	vzejít
nic	nic	k3yNnSc1	nic
užitečného	užitečný	k2eAgNnSc2d1	užitečné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
Polsko-britská	polskoritský	k2eAgFnSc1d1	polsko-britská
obranná	obranný	k2eAgFnSc1d1	obranná
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
smlouvou	smlouva	k1gFnSc7	smlouva
Británie	Británie	k1gFnSc2	Británie
zaručila	zaručit	k5eAaPmAgFnS	zaručit
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
zavázala	zavázat	k5eAaPmAgFnS	zavázat
se	se	k3xPyFc4	se
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Německá	německý	k2eAgFnSc1d1	německá
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
před	před	k7c7	před
sovětským	sovětský	k2eAgInSc7d1	sovětský
útokem	útok	k1gInSc7	útok
===	===	k?	===
</s>
</p>
<p>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
odradit	odradit	k5eAaPmF	odradit
Brity	Brit	k1gMnPc4	Brit
a	a	k8xC	a
Francouze	Francouz	k1gMnSc4	Francouz
od	od	k7c2	od
zasahování	zasahování	k1gNnSc2	zasahování
do	do	k7c2	do
nadcházejícího	nadcházející	k2eAgInSc2d1	nadcházející
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
poskytnout	poskytnout	k5eAaPmF	poskytnout
síly	síla	k1gFnPc4	síla
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
Británii	Británie	k1gFnSc4	Británie
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
německý	německý	k2eAgMnSc1d1	německý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Joachim	Joachim	k1gInSc1	Joachim
von	von	k1gInSc1	von
Ribbentrop	Ribbentrop	k1gInSc1	Ribbentrop
předal	předat	k5eAaPmAgInS	předat
britskému	britský	k2eAgMnSc3d1	britský
velvyslanci	velvyslanec	k1gMnSc3	velvyslanec
Neville	Neville	k1gFnSc2	Neville
Hendersonovi	Henderson	k1gMnSc3	Henderson
seznam	seznam	k1gInSc4	seznam
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
údajně	údajně	k6eAd1	údajně
zajistily	zajistit	k5eAaPmAgFnP	zajistit
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
návrhů	návrh	k1gInPc2	návrh
mělo	mít	k5eAaImAgNnS	mít
Polsko	Polsko	k1gNnSc1	Polsko
předat	předat	k5eAaPmF	předat
Německu	Německo	k1gNnSc3	Německo
Danzig	Danziga	k1gFnPc2	Danziga
(	(	kIx(	(
<g/>
Gdańsk	Gdańsk	k1gInSc1	Gdańsk
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
Koridoru	koridor	k1gInSc6	koridor
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
do	do	k7c2	do
roka	rok	k1gInSc2	rok
měl	mít	k5eAaImAgInS	mít
konat	konat	k5eAaImF	konat
plebiscit	plebiscit	k1gInSc1	plebiscit
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
by	by	kYmCp3nS	by
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
obyvatelé	obyvatel	k1gMnPc1	obyvatel
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
ne	ne	k9	ne
novější	nový	k2eAgFnSc6d2	novější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
polský	polský	k2eAgMnSc1d1	polský
vyslanec	vyslanec	k1gMnSc1	vyslanec
Józef	Józef	k1gMnSc1	Józef
Lipski	Lipske	k1gFnSc4	Lipske
dostavil	dostavit	k5eAaPmAgMnS	dostavit
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
k	k	k7c3	k
Ribbentropovi	Ribbentrop	k1gMnSc3	Ribbentrop
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
zmocněn	zmocnit	k5eAaPmNgMnS	zmocnit
nic	nic	k3yNnSc1	nic
podobného	podobný	k2eAgNnSc2d1	podobné
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
,	,	kIx,	,
Ribbentrop	Ribbentrop	k1gInSc4	Ribbentrop
jej	on	k3xPp3gInSc4	on
propustil	propustit	k5eAaPmAgInS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Polsko	Polsko	k1gNnSc1	Polsko
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
německé	německý	k2eAgFnPc4d1	německá
nabídky	nabídka	k1gFnPc4	nabídka
a	a	k8xC	a
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německé	německý	k2eAgFnPc1d1	německá
jednotky	jednotka	k1gFnPc1	jednotka
v	v	k7c6	v
polských	polský	k2eAgFnPc6d1	polská
uniformách	uniforma	k1gFnPc6	uniforma
zinscenovaly	zinscenovat	k5eAaPmAgFnP	zinscenovat
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
blízko	blízko	k7c2	blízko
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
města	město	k1gNnSc2	město
Gleiwitz	Gleiwitz	k1gInSc4	Gleiwitz
přepadení	přepadení	k1gNnSc2	přepadení
vysílačky	vysílačka	k1gFnSc2	vysílačka
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
ráno	ráno	k6eAd1	ráno
Hitler	Hitler	k1gMnSc1	Hitler
nařídil	nařídit	k5eAaPmAgMnS	nařídit
zahájení	zahájení	k1gNnSc3	zahájení
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
(	(	kIx(	(
<g/>
začal	začít	k5eAaPmAgInS	začít
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
ve	v	k7c6	v
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vlády	vláda	k1gFnPc1	vláda
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
Německu	Německo	k1gNnSc6	Německo
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
poskytnout	poskytnout	k5eAaPmF	poskytnout
Polsku	Polska	k1gFnSc4	Polska
významnou	významný	k2eAgFnSc4d1	významná
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Poláci	Polák	k1gMnPc1	Polák
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
v	v	k7c6	v
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
bitvách	bitva	k1gFnPc6	bitva
několika	několik	k4yIc2	několik
dílčích	dílčí	k2eAgInPc2d1	dílčí
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
technická	technický	k2eAgFnSc1d1	technická
<g/>
,	,	kIx,	,
taktická	taktický	k2eAgFnSc1d1	taktická
a	a	k8xC	a
početní	početní	k2eAgFnSc1d1	početní
převaha	převaha	k1gFnSc1	převaha
donutila	donutit	k5eAaPmAgFnS	donutit
polské	polský	k2eAgFnPc4d1	polská
armády	armáda	k1gFnPc4	armáda
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Varšavu	Varšava	k1gFnSc4	Varšava
a	a	k8xC	a
Lvov	Lvov	k1gInSc4	Lvov
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
vydal	vydat	k5eAaPmAgMnS	vydat
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
maršál	maršál	k1gMnSc1	maršál
Edward	Edward	k1gMnSc1	Edward
Rydz-Śmigły	Rydz-Śmigła	k1gFnSc2	Rydz-Śmigła
rozkaz	rozkaz	k1gInSc1	rozkaz
ke	k	k7c3	k
všeobecnému	všeobecný	k2eAgInSc3d1	všeobecný
ústupu	ústup	k1gInSc3	ústup
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
rumunskému	rumunský	k2eAgNnSc3d1	rumunské
předmostí	předmostí	k1gNnSc3	předmostí
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
invaze	invaze	k1gFnSc2	invaze
začali	začít	k5eAaPmAgMnP	začít
vrcholní	vrcholný	k2eAgMnPc1d1	vrcholný
němečtí	německý	k2eAgMnPc1d1	německý
představitelé	představitel	k1gMnPc1	představitel
vyzývat	vyzývat	k5eAaImF	vyzývat
Sověty	Sověty	k1gInPc4	Sověty
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
splnili	splnit	k5eAaPmAgMnP	splnit
svoje	svůj	k3xOyFgInPc4	svůj
závazky	závazek	k1gInPc4	závazek
a	a	k8xC	a
napadli	napadnout	k5eAaPmAgMnP	napadnout
Polsko	Polsko	k1gNnSc4	Polsko
od	od	k7c2	od
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
Friedrich	Friedrich	k1gMnSc1	Friedrich
Werner	Werner	k1gMnSc1	Werner
von	von	k1gInSc4	von
der	drát	k5eAaImRp2nS	drát
Schulenburg	Schulenburg	k1gMnSc1	Schulenburg
a	a	k8xC	a
sovětský	sovětský	k2eAgMnSc1d1	sovětský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Molotov	Molotovo	k1gNnPc2	Molotovo
si	se	k3xPyFc3	se
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
řadu	řada	k1gFnSc4	řada
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
telegramů	telegram	k1gInPc2	telegram
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
však	však	k9	však
Sověti	Sovět	k1gMnPc1	Sovět
s	s	k7c7	s
invazí	invaze	k1gFnSc7	invaze
dávali	dávat	k5eAaImAgMnP	dávat
načas	načas	k6eAd1	načas
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnávaly	zaměstnávat	k5eAaImAgInP	zaměstnávat
je	on	k3xPp3gFnPc4	on
pohraniční	pohraniční	k2eAgInPc1d1	pohraniční
konflikty	konflikt	k1gInPc1	konflikt
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
a	a	k8xC	a
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
čas	čas	k1gInSc4	čas
k	k	k7c3	k
mobilizaci	mobilizace	k1gFnSc3	mobilizace
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Vyčkáváním	vyčkávání	k1gNnSc7	vyčkávání
s	s	k7c7	s
invazí	invaze	k1gFnSc7	invaze
na	na	k7c4	na
úplný	úplný	k2eAgInSc4d1	úplný
rozpad	rozpad	k1gInSc4	rozpad
Polska	Polsko	k1gNnSc2	Polsko
sledovali	sledovat	k5eAaImAgMnP	sledovat
určitou	určitý	k2eAgFnSc4d1	určitá
diplomatickou	diplomatický	k2eAgFnSc4d1	diplomatická
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Nevyhlášenou	vyhlášený	k2eNgFnSc4d1	nevyhlášená
válku	válka	k1gFnSc4	válka
mezi	mezi	k7c7	mezi
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Japonskem	Japonsko	k1gNnSc7	Japonsko
ukončil	ukončit	k5eAaPmAgInS	ukončit
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
podpis	podpis	k1gInSc4	podpis
smlouvy	smlouva	k1gFnSc2	smlouva
Molotov	Molotov	k1gInSc1	Molotov
<g/>
–	–	k?	–
<g/>
Tódžó	Tódžó	k1gMnPc3	Tódžó
s	s	k7c7	s
příměřím	příměří	k1gNnSc7	příměří
účinným	účinný	k2eAgNnSc7d1	účinné
od	od	k7c2	od
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
<g/>
Molotov	Molotov	k1gInSc1	Molotov
předal	předat	k5eAaPmAgInS	předat
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
polskému	polský	k2eAgMnSc3d1	polský
velvyslanci	velvyslanec	k1gMnSc3	velvyslanec
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
Grzybowskému	Grzybowské	k1gNnSc3	Grzybowské
následující	následující	k2eAgFnSc4d1	následující
deklaraci	deklarace	k1gFnSc4	deklarace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Lidový	lidový	k2eAgMnSc1d1	lidový
komisař	komisař	k1gMnSc1	komisař
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
V.	V.	kA	V.
Molotov	Molotov	k1gInSc1	Molotov
</s>
</p>
<p>
<s>
V	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
projevu	projev	k1gInSc6	projev
Molotov	Molotov	k1gInSc1	Molotov
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
všechny	všechen	k3xTgFnPc4	všechen
smlouvy	smlouva	k1gFnPc4	smlouva
mezi	mezi	k7c7	mezi
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
neplatnými	platný	k2eNgFnPc7d1	neplatná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
opustila	opustit	k5eAaPmAgFnS	opustit
svůj	svůj	k3xOyFgInSc4	svůj
lid	lid	k1gInSc4	lid
a	a	k8xC	a
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
překročila	překročit	k5eAaPmAgFnS	překročit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
polskou	polský	k2eAgFnSc4d1	polská
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
invaze	invaze	k1gFnSc1	invaze
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Polsko	Polsko	k1gNnSc1	Polsko
v	v	k7c4	v
den	den	k1gInSc4	den
sovětské	sovětský	k2eAgFnSc2d1	sovětská
invaze	invaze	k1gFnSc2	invaze
===	===	k?	===
</s>
</p>
<p>
<s>
Ráno	ráno	k6eAd1	ráno
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
působily	působit	k5eAaImAgInP	působit
polské	polský	k2eAgInPc1d1	polský
správní	správní	k2eAgInPc1d1	správní
úřady	úřad	k1gInPc1	úřad
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
šesti	šest	k4xCc2	šest
východních	východní	k2eAgNnPc2d1	východní
vojvodství	vojvodství	k1gNnPc2	vojvodství
a	a	k8xC	a
také	také	k9	také
fungovaly	fungovat	k5eAaImAgInP	fungovat
v	v	k7c6	v
částech	část	k1gFnPc6	část
dalších	další	k2eAgNnPc2d1	další
pěti	pět	k4xCc2	pět
vojvodství	vojvodství	k1gNnPc2	vojvodství
<g/>
;	;	kIx,	;
školy	škola	k1gFnPc1	škola
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Polsku	Polsko	k1gNnSc6	Polsko
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
září	zářit	k5eAaImIp3nP	zářit
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
polské	polský	k2eAgFnSc2d1	polská
armády	armáda	k1gFnSc2	armáda
působily	působit	k5eAaImAgFnP	působit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
oblastech	oblast	k1gFnPc6	oblast
-	-	kIx~	-
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
(	(	kIx(	(
<g/>
Tomaszów	Tomaszów	k1gMnSc5	Tomaszów
Lubelski	Lubelsk	k1gMnSc5	Lubelsk
<g/>
,	,	kIx,	,
Zamość	Zamość	k1gMnSc5	Zamość
<g/>
,	,	kIx,	,
Lwów	Lwów	k1gMnSc5	Lwów
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
(	(	kIx(	(
<g/>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Modlin	Modlin	k1gInSc1	Modlin
a	a	k8xC	a
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Bzury	Bzura	k1gFnSc2	Bzura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úporná	úporný	k2eAgFnSc1d1	úporná
polská	polský	k2eAgFnSc1d1	polská
obrana	obrana	k1gFnSc1	obrana
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
přinutila	přinutit	k5eAaPmAgFnS	přinutit
Němce	Němec	k1gMnPc4	Němec
zastavit	zastavit	k5eAaPmF	zastavit
postup	postup	k1gInSc1	postup
a	a	k8xC	a
fronta	fronta	k1gFnSc1	fronta
se	se	k3xPyFc4	se
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
východně	východně	k6eAd1	východně
od	od	k7c2	od
linie	linie	k1gFnSc2	linie
Augustów	Augustów	k1gFnPc2	Augustów
–	–	k?	–
Grodno	Grodno	k1gNnSc4	Grodno
–	–	k?	–
Białystok	Białystok	k1gInSc1	Białystok
–	–	k?	–
Kobryń	Kobryń	k1gFnSc2	Kobryń
–	–	k?	–
Kowel	Kowel	k1gMnSc1	Kowel
–	–	k?	–
Żółkiew	Żółkiew	k1gMnSc1	Żółkiew
–	–	k?	–
Lwów	Lwów	k1gMnSc1	Lwów
–	–	k?	–
Żydaczów	Żydaczów	k1gMnSc1	Żydaczów
–	–	k?	–
Stryj	Stryj	k1gInSc1	Stryj
–	–	k?	–
Turka	turek	k1gInSc2	turek
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
fungovalo	fungovat	k5eAaImAgNnS	fungovat
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
třetině	třetina	k1gFnSc6	třetina
státu	stát	k1gInSc2	stát
a	a	k8xC	a
stále	stále	k6eAd1	stále
fungovalo	fungovat	k5eAaImAgNnS	fungovat
osobní	osobní	k2eAgNnSc1d1	osobní
i	i	k8xC	i
nákladní	nákladní	k2eAgNnSc1d1	nákladní
dopravní	dopravní	k2eAgNnSc1d1	dopravní
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
pěti	pět	k4xCc7	pět
sousedními	sousední	k2eAgInPc7d1	sousední
státy	stát	k1gInPc7	stát
(	(	kIx(	(
<g/>
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pińsku	Pińsek	k1gInSc6	Pińsek
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
výroba	výroba	k1gFnSc1	výroba
několika	několik	k4yIc2	několik
letadel	letadlo	k1gNnPc2	letadlo
PZL	PZL	kA	PZL
<g/>
.37	.37	k4	.37
Łoś	Łoś	k1gFnSc2	Łoś
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
PZL	PZL	kA	PZL
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
z	z	k7c2	z
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
námořní	námořní	k2eAgFnSc1d1	námořní
loď	loď	k1gFnSc1	loď
dopravující	dopravující	k2eAgFnSc1d1	dopravující
tanky	tank	k1gInPc7	tank
Renault	renault	k1gInSc4	renault
R35	R35	k1gFnSc1	R35
připlula	připlout	k5eAaPmAgFnS	připlout
do	do	k7c2	do
rumunské	rumunský	k2eAgFnSc2d1	rumunská
Constanț	Constanț	k1gFnSc2	Constanț
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
dělostřeleckým	dělostřelecký	k2eAgNnSc7d1	dělostřelecké
vybavením	vybavení	k1gNnSc7	vybavení
právě	právě	k6eAd1	právě
opustila	opustit	k5eAaPmAgFnS	opustit
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
sedmnáct	sedmnáct	k4xCc4	sedmnáct
francouzských	francouzský	k2eAgFnPc2d1	francouzská
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
vojenským	vojenský	k2eAgInSc7d1	vojenský
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
směřovaly	směřovat	k5eAaImAgFnP	směřovat
do	do	k7c2	do
Constanț	Constanț	k1gFnSc2	Constanț
a	a	k8xC	a
Galaț	Galaț	k1gFnSc2	Galaț
<g/>
,	,	kIx,	,
vezlo	vézt	k5eAaImAgNnS	vézt
padesát	padesát	k4xCc1	padesát
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
dvacet	dvacet	k4xCc4	dvacet
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
munice	munice	k1gFnSc2	munice
a	a	k8xC	a
TNT	TNT	kA	TNT
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
držení	držení	k1gNnSc6	držení
stále	stále	k6eAd1	stále
byla	být	k5eAaImAgFnS	být
velká	velká	k1gFnSc1	velká
města	město	k1gNnSc2	město
–	–	k?	–
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Lvov	Lvov	k1gInSc1	Lvov
<g/>
,	,	kIx,	,
Vilno	Vilno	k1gNnSc1	Vilno
<g/>
,	,	kIx,	,
Grodno	Grodno	k1gNnSc1	Grodno
<g/>
,	,	kIx,	,
Łuck	Łuck	k1gInSc1	Łuck
<g/>
,	,	kIx,	,
Tarnopol	Tarnopol	k1gInSc1	Tarnopol
a	a	k8xC	a
Lublin	Lublin	k1gInSc1	Lublin
(	(	kIx(	(
<g/>
dobyt	dobyt	k2eAgInSc1d1	dobyt
Němci	Němec	k1gMnSc3	Němec
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leszek	Leszek	k1gInSc1	Leszek
Moczulski	Moczulsk	k1gFnSc2	Moczulsk
udává	udávat	k5eAaImIp3nS	udávat
počet	počet	k1gInSc1	počet
750	[number]	k4	750
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
sloužících	sloužící	k2eAgMnPc2d1	sloužící
v	v	k7c6	v
Polské	polský	k2eAgFnSc6d1	polská
armádě	armáda	k1gFnSc6	armáda
(	(	kIx(	(
<g/>
polští	polský	k2eAgMnPc1d1	polský
historici	historik	k1gMnPc1	historik
Czesław	Czesław	k1gMnSc1	Czesław
Grzelak	Grzelak	k1gMnSc1	Grzelak
a	a	k8xC	a
Henryk	Henryk	k1gMnSc1	Henryk
Stańczyk	Stańczyk	k1gMnSc1	Stańczyk
udávají	udávat	k5eAaImIp3nP	udávat
sílu	síla	k1gFnSc4	síla
650	[number]	k4	650
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
dvě	dva	k4xCgFnPc4	dva
motorizované	motorizovaný	k2eAgFnPc4d1	motorizovaná
brigády	brigáda	k1gFnPc4	brigáda
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
–	–	k?	–
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
obrněná	obrněný	k2eAgFnSc1d1	obrněná
motorizovaná	motorizovaný	k2eAgFnSc1d1	motorizovaná
brigáda	brigáda	k1gFnSc1	brigáda
–	–	k?	–
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
dosud	dosud	k6eAd1	dosud
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
<g/>
)	)	kIx)	)
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
přesouvat	přesouvat	k5eAaImF	přesouvat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
Armádě	armáda	k1gFnSc3	armáda
Krakov	Krakov	k1gInSc1	Krakov
a	a	k8xC	a
dvacet	dvacet	k4xCc1	dvacet
šest	šest	k4xCc1	šest
pěších	pěší	k1gMnPc2	pěší
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
polská	polský	k2eAgFnSc1d1	polská
armáda	armáda	k1gFnSc1	armáda
poničena	poničit	k5eAaPmNgFnS	poničit
týdny	týden	k1gInPc7	týden
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
stále	stále	k6eAd1	stále
impozantní	impozantní	k2eAgFnSc7d1	impozantní
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
historik	historik	k1gMnSc1	historik
Leszek	Leszek	k1gInSc4	Leszek
Moczulski	Moczulsk	k1gFnSc2	Moczulsk
popsal	popsat	k5eAaPmAgMnS	popsat
polskou	polský	k2eAgFnSc4d1	polská
armádu	armáda	k1gFnSc4	armáda
k	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
jako	jako	k8xC	jako
dost	dost	k6eAd1	dost
silnou	silný	k2eAgFnSc7d1	silná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vzdorovala	vzdorovat	k5eAaImAgFnS	vzdorovat
wehrmachtu	wehrmacht	k1gInSc3	wehrmacht
a	a	k8xC	a
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
evropských	evropský	k2eAgFnPc2d1	Evropská
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Baranowicze	Baranowicze	k1gFnSc2	Baranowicze
–	–	k?	–
Łuniniec	Łuniniec	k1gMnSc1	Łuniniec
–	–	k?	–
Równe	Równ	k1gInSc5	Równ
probíhal	probíhat	k5eAaImAgInS	probíhat
ve	v	k7c6	v
dne	den	k1gInSc2	den
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
transport	transporta	k1gFnPc2	transporta
sil	síla	k1gFnPc2	síla
ze	z	k7c2	z
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
koutu	kout	k1gInSc2	kout
země	zem	k1gFnSc2	zem
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Rumunskému	rumunský	k2eAgNnSc3d1	rumunské
předmostí	předmostí	k1gNnSc3	předmostí
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
35	[number]	k4	35
<g/>
.	.	kIx.	.
záložní	záložní	k2eAgFnSc2d1	záložní
pěší	pěší	k2eAgFnSc2d1	pěší
divize	divize	k1gFnSc2	divize
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
plukovníka	plukovník	k1gMnSc2	plukovník
Jarosława	Jarosławus	k1gMnSc2	Jarosławus
Szafrana	Szafran	k1gMnSc2	Szafran
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
skupiny	skupina	k1gFnSc2	skupina
Grodno	Grodno	k1gNnSc1	Grodno
plukovníka	plukovník	k1gMnSc2	plukovník
Bohdana	Bohdana	k1gFnSc1	Bohdana
Hulewicze	Hulewicze	k1gFnSc1	Hulewicze
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
bitva	bitva	k1gFnSc1	bitva
záříjové	záříjový	k2eAgFnSc2d1	záříjová
kampaně	kampaň	k1gFnSc2	kampaň
-	-	kIx~	-
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Tomaszów	Tomaszów	k1gFnSc6	Tomaszów
Lubelski	Lubelske	k1gFnSc4	Lubelske
–	–	k?	–
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c4	v
den	den	k1gInSc4	den
sovětské	sovětský	k2eAgFnSc2d1	sovětská
invaze	invaze	k1gFnSc2	invaze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Leszka	Leszka	k1gFnSc1	Leszka
Moczulského	Moczulský	k2eAgInSc2d1	Moczulský
přibližně	přibližně	k6eAd1	přibližně
250,000	[number]	k4	250,000
polských	polský	k2eAgMnPc2d1	polský
vojáků	voják	k1gMnPc2	voják
bojovalo	bojovat	k5eAaImAgNnS	bojovat
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
350,000	[number]	k4	350,000
se	se	k3xPyFc4	se
chystalo	chystat	k5eAaImAgNnS	chystat
bránit	bránit	k5eAaImF	bránit
Rumunské	rumunský	k2eAgNnSc1d1	rumunské
předmostí	předmostí	k1gNnSc1	předmostí
<g/>
,	,	kIx,	,
35,000	[number]	k4	35,000
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Polesí	Polesí	k1gNnSc6	Polesí
a	a	k8xC	a
10,000	[number]	k4	10,000
bojovalo	bojovat	k5eAaImAgNnS	bojovat
na	na	k7c6	na
baltském	baltský	k2eAgNnSc6d1	Baltské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Hel	Hela	k1gFnPc2	Hela
a	a	k8xC	a
ve	v	k7c6	v
Gdyni	Gdyně	k1gFnSc6	Gdyně
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
probíhajícím	probíhající	k2eAgInPc3d1	probíhající
bojům	boj	k1gInPc3	boj
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
,	,	kIx,	,
pevnosti	pevnost	k1gFnSc3	pevnost
Modlin	Modlina	k1gFnPc2	Modlina
<g/>
,	,	kIx,	,
na	na	k7c6	na
Bzuře	Bzura	k1gFnSc6	Bzura
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Zamośće	Zamośće	k1gNnSc2	Zamośće
<g/>
,	,	kIx,	,
Lwówa	Lwówum	k1gNnSc2	Lwówum
a	a	k8xC	a
Tomaszówa	Tomaszów	k1gInSc2	Tomaszów
Lubelského	Lubelský	k2eAgInSc2d1	Lubelský
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
německých	německý	k2eAgFnPc2d1	německá
divizí	divize	k1gFnPc2	divize
odveleno	odvelet	k5eAaPmNgNnS	odvelet
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
polských	polský	k2eAgMnPc2d1	polský
úřadů	úřad	k1gInPc2	úřad
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
oblast	oblast	k1gFnSc1	oblast
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
140,000	[number]	k4	140,000
km	km	kA	km
<g/>
2	[number]	k4	2
široká	široký	k2eAgFnSc1d1	široká
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
950	[number]	k4	950
kilometrů	kilometr	k1gInPc2	kilometr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
–	–	k?	–
od	od	k7c2	od
Daugavy	Daugava	k1gFnSc2	Daugava
až	až	k9	až
ke	k	k7c3	k
Karpatům	Karpaty	k1gInPc3	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Polské	polský	k2eAgNnSc1d1	polské
Radio	radio	k1gNnSc1	radio
Baranowicze	Baranowicze	k1gFnSc2	Baranowicze
a	a	k8xC	a
Radio	radio	k1gNnSc4	radio
Wilno	Wilno	k6eAd1	Wilno
přestaly	přestat	k5eAaPmAgFnP	přestat
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
po	po	k7c6	po
náletu	nálet	k1gInSc6	nálet
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
vysílat	vysílat	k5eAaImF	vysílat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Radio	radio	k1gNnSc1	radio
Lwów	Lwów	k1gFnSc2	Lwów
a	a	k8xC	a
Radio	radio	k1gNnSc1	radio
Warszawa	Warszaw	k1gInSc2	Warszaw
II	II	kA	II
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
stály	stát	k5eAaImAgFnP	stát
vysílaly	vysílat	k5eAaImAgFnP	vysílat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Síly	síla	k1gFnPc4	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
do	do	k7c2	do
východních	východní	k2eAgFnPc2d1	východní
oblastí	oblast	k1gFnPc2	oblast
Polska	Polsko	k1gNnSc2	Polsko
se	s	k7c7	s
sedmi	sedm	k4xCc2	sedm
polními	polní	k2eAgFnPc7d1	polní
armádami	armáda	k1gFnPc7	armáda
v	v	k7c6	v
síle	síla	k1gFnSc6	síla
mezi	mezi	k7c7	mezi
450.000	[number]	k4	450.000
a	a	k8xC	a
1.000.000	[number]	k4	1.000.000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
rozdělenými	rozdělený	k2eAgInPc7d1	rozdělený
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
frontů	front	k1gInPc2	front
<g/>
.	.	kIx.	.
</s>
<s>
Běloruskému	běloruský	k2eAgInSc3d1	běloruský
frontu	front	k1gInSc3	front
velel	velet	k5eAaImAgMnS	velet
Michail	Michail	k1gMnSc1	Michail
Kovaljov	Kovaljov	k1gInSc4	Kovaljov
<g/>
,	,	kIx,	,
Ukrajinskému	ukrajinský	k2eAgInSc3d1	ukrajinský
frontu	front	k1gInSc3	front
velel	velet	k5eAaImAgMnS	velet
Semjon	Semjon	k1gMnSc1	Semjon
Timošenko	Timošenka	k1gFnSc5	Timošenka
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgInSc1d1	polský
obranný	obranný	k2eAgInSc1d1	obranný
Plán	plán	k1gInSc1	plán
Zachód	Zachóda	k1gFnPc2	Zachóda
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
zůstane	zůstat	k5eAaPmIp3nS	zůstat
během	během	k7c2	během
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
neutrální	neutrální	k2eAgFnSc2d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
polské	polský	k2eAgNnSc1d1	polské
velení	velení	k1gNnSc1	velení
rozmístilo	rozmístit	k5eAaPmAgNnS	rozmístit
většinu	většina	k1gFnSc4	většina
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
čelila	čelit	k5eAaImAgFnS	čelit
německé	německý	k2eAgFnSc3d1	německá
agresi	agrese	k1gFnSc3	agrese
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
východní	východní	k2eAgFnSc2d1	východní
hranice	hranice	k1gFnSc2	hranice
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
méně	málo	k6eAd2	málo
než	než	k8xS	než
20	[number]	k4	20
oslabených	oslabený	k2eAgFnPc2d1	oslabená
brigád	brigáda	k1gFnPc2	brigáda
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
čítaly	čítat	k5eAaImAgInP	čítat
okolo	okolo	k7c2	okolo
20	[number]	k4	20
tisíc	tisíc	k4xCgInSc4	tisíc
příslušníků	příslušník	k1gMnPc2	příslušník
Sboru	sbor	k1gInSc2	sbor
ochrany	ochrana	k1gFnSc2	ochrana
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
polské	polský	k2eAgNnSc1d1	polské
velení	velení	k1gNnSc1	velení
právě	právě	k9	právě
ustupovalo	ustupovat	k5eAaImAgNnS	ustupovat
bojem	boj	k1gInSc7	boj
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Rumunskému	rumunský	k2eAgNnSc3d1	rumunské
předmostí	předmostí	k1gNnSc3	předmostí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vojska	vojsko	k1gNnPc1	vojsko
měla	mít	k5eAaImAgNnP	mít
přeskupit	přeskupit	k5eAaPmF	přeskupit
a	a	k8xC	a
očekávat	očekávat	k5eAaImF	očekávat
britskou	britský	k2eAgFnSc4d1	britská
a	a	k8xC	a
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průběh	průběh	k1gInSc1	průběh
tažení	tažení	k1gNnSc1	tažení
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
sovětské	sovětský	k2eAgFnSc2d1	sovětská
invaze	invaze	k1gFnSc2	invaze
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
Rydz-Śmigły	Rydz-Śmigła	k1gFnPc4	Rydz-Śmigła
vydání	vydání	k1gNnSc2	vydání
rozkazu	rozkaz	k1gInSc2	rozkaz
pohraničním	pohraniční	k2eAgNnPc3d1	pohraniční
vojskům	vojsko	k1gNnPc3	vojsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Felicjan	Felicjan	k1gInSc1	Felicjan
Sławoj	Sławoj	k1gInSc1	Sławoj
Składkowski	Składkowsk	k1gFnSc2	Składkowsk
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Ignacy	Ignaca	k1gFnSc2	Ignaca
Mościcki	Mościck	k1gFnSc2	Mościck
jej	on	k3xPp3gMnSc4	on
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
myšlenky	myšlenka	k1gFnSc2	myšlenka
odradili	odradit	k5eAaPmAgMnP	odradit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Rydz-Śmigły	Rydz-Śmigł	k1gMnPc4	Rydz-Śmigł
stažení	stažení	k1gNnSc2	stažení
polských	polský	k2eAgNnPc2d1	polské
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
boje	boj	k1gInPc1	boj
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
sebeobraně	sebeobrana	k1gFnSc6	sebeobrana
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
německá	německý	k2eAgFnSc1d1	německá
invaze	invaze	k1gFnSc1	invaze
těžce	těžce	k6eAd1	těžce
poškodila	poškodit	k5eAaPmAgFnS	poškodit
polské	polský	k2eAgInPc4d1	polský
systémy	systém	k1gInPc4	systém
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
působilo	působit	k5eAaImAgNnS	působit
jednotkám	jednotka	k1gFnPc3	jednotka
potíže	potíž	k1gFnSc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zmatků	zmatek	k1gInPc2	zmatek
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
střetům	střet	k1gInPc3	střet
polských	polský	k2eAgFnPc2d1	polská
a	a	k8xC	a
sovětských	sovětský	k2eAgFnPc2d1	sovětská
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Orlik-Rückemann	Orlik-Rückemann	k1gMnSc1	Orlik-Rückemann
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
velel	velet	k5eAaImAgMnS	velet
Sboru	sbor	k1gInSc2	sbor
ochrany	ochrana	k1gFnSc2	ochrana
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
nedostal	dostat	k5eNaPmAgMnS	dostat
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
ustanovení	ustanovení	k1gNnSc2	ustanovení
žádné	žádný	k3yNgInPc1	žádný
pokyny	pokyn	k1gInPc1	pokyn
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
síly	síla	k1gFnPc1	síla
napadaly	napadat	k5eAaBmAgFnP	napadat
sovětské	sovětský	k2eAgFnPc1d1	sovětská
jednotky	jednotka	k1gFnPc1	jednotka
až	až	k9	až
do	do	k7c2	do
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
Sboru	sbor	k1gInSc2	sbor
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
nekapitulovala	kapitulovat	k5eNaBmAgFnS	kapitulovat
a	a	k8xC	a
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
nařídila	nařídit	k5eAaPmAgFnS	nařídit
všem	všecek	k3xTgNnPc3	všecek
jednotkám	jednotka	k1gFnPc3	jednotka
opuštění	opuštění	k1gNnSc4	opuštění
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
reorganizaci	reorganizace	k1gFnSc4	reorganizace
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
sovětské	sovětský	k2eAgFnSc2d1	sovětská
invaze	invaze	k1gFnSc2	invaze
překročila	překročit	k5eAaPmAgFnS	překročit
polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
hranice	hranice	k1gFnSc2	hranice
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Polské	polský	k2eAgFnPc1d1	polská
síly	síla	k1gFnPc1	síla
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rumunského	rumunský	k2eAgNnSc2d1	rumunské
předmostí	předmostí	k1gNnSc2	předmostí
<g/>
,	,	kIx,	,
když	když	k8xS	když
čelily	čelit	k5eAaImAgFnP	čelit
německým	německý	k2eAgMnPc3d1	německý
útokům	útok	k1gInPc3	útok
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
křídle	křídlo	k1gNnSc6	křídlo
a	a	k8xC	a
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
střetávaly	střetávat	k5eAaImAgInP	střetávat
se	s	k7c7	s
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
jednotkami	jednotka	k1gFnPc7	jednotka
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vojska	vojsko	k1gNnSc2	vojsko
plnila	plnit	k5eAaImAgFnS	plnit
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
evakuaci	evakuace	k1gFnSc3	evakuace
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
porazili	porazit	k5eAaPmAgMnP	porazit
polskou	polský	k2eAgFnSc4d1	polská
Armádu	armáda	k1gFnSc4	armáda
Krakov	Krakov	k1gInSc4	Krakov
a	a	k8xC	a
Armádu	armáda	k1gFnSc4	armáda
Lublin	Lublin	k1gInSc4	Lublin
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Tomaszówa	Tomaszów	k1gInSc2	Tomaszów
Lubelského	Lubelský	k2eAgInSc2d1	Lubelský
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
partnery	partner	k1gMnPc7	partner
postupujícími	postupující	k2eAgInPc7d1	postupující
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
v	v	k7c6	v
poli	pole	k1gNnSc3	pole
několikrát	několikrát	k6eAd1	několikrát
spolupracovaly	spolupracovat	k5eAaImAgFnP	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
předal	předat	k5eAaPmAgInS	předat
Brestskou	brestský	k2eAgFnSc4d1	Brestská
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
obsadil	obsadit	k5eAaPmAgMnS	obsadit
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Brest	Brest	k1gInSc4	Brest
sovětské	sovětský	k2eAgFnSc2d1	sovětská
29	[number]	k4	29
<g/>
.	.	kIx.	.
tankové	tankový	k2eAgFnSc3d1	tanková
brigádě	brigáda	k1gFnSc3	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
generál	generál	k1gMnSc1	generál
Heinz	Heinz	k1gMnSc1	Heinz
Guderian	Guderian	k1gMnSc1	Guderian
a	a	k8xC	a
sovětský	sovětský	k2eAgMnSc1d1	sovětský
velitel	velitel	k1gMnSc1	velitel
Semjon	Semjona	k1gFnPc2	Semjona
Krivošejn	Krivošejno	k1gNnPc2	Krivošejno
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
v	v	k7c6	v
Brest-Litevsku	Brest-Litevsko	k1gNnSc6	Brest-Litevsko
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
společnou	společný	k2eAgFnSc4d1	společná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
přehlídku	přehlídka	k1gFnSc4	přehlídka
<g/>
.	.	kIx.	.
</s>
<s>
Lwów	Lwów	k?	Lwów
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Lviv	Lviv	k1gInSc1	Lviv
<g/>
)	)	kIx)	)
padl	padnout	k5eAaImAgInS	padnout
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
<g/>
co	co	k3yInSc4	co
Němci	Němec	k1gMnPc1	Němec
přenechali	přenechat	k5eAaPmAgMnP	přenechat
vedení	vedení	k1gNnSc4	vedení
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
Sovětům	Sovět	k1gMnPc3	Sovět
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
jednotky	jednotka	k1gFnSc2	jednotka
dobyly	dobýt	k5eAaPmAgFnP	dobýt
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
Wilno	Wilno	k6eAd1	Wilno
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
)	)	kIx)	)
po	po	k7c6	po
dvoudenní	dvoudenní	k2eAgFnSc6d1	dvoudenní
bitvě	bitva	k1gFnSc6	bitva
a	a	k8xC	a
obsadily	obsadit	k5eAaPmAgFnP	obsadit
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
Grodno	Grodno	k1gNnSc4	Grodno
po	po	k7c6	po
čtyřdenních	čtyřdenní	k2eAgInPc6d1	čtyřdenní
bojích	boj	k1gInPc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
předem	předem	k6eAd1	předem
dohodnuté	dohodnutý	k2eAgFnSc2d1	dohodnutá
hraniční	hraniční	k2eAgFnSc2d1	hraniční
linie	linie	k1gFnSc2	linie
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
Narew	Narew	k1gMnSc1	Narew
<g/>
,	,	kIx,	,
Západní	západní	k2eAgMnSc1d1	západní
Bug	Bug	k1gMnSc1	Bug
<g/>
,	,	kIx,	,
Visla	Visla	k1gFnSc1	Visla
a	a	k8xC	a
San	San	k1gFnSc1	San
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
taktické	taktický	k2eAgNnSc4d1	taktické
vítězství	vítězství	k1gNnSc4	vítězství
Poláků	polák	k1gInPc2	polák
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Szack	Szack	k1gInSc4	Szack
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc4	výsledek
střetnutí	střetnutí	k1gNnSc2	střetnutí
nenechával	nechávat	k5eNaImAgMnS	nechávat
nikoho	nikdo	k3yNnSc4	nikdo
na	na	k7c6	na
pochybách	pochyba	k1gFnPc6	pochyba
<g/>
.	.	kIx.	.
</s>
<s>
Dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
,	,	kIx,	,
domobrana	domobrana	k1gFnSc1	domobrana
a	a	k8xC	a
po	po	k7c6	po
ústupu	ústup	k1gInSc6	ústup
reorganizované	reorganizovaný	k2eAgFnSc2d1	reorganizovaná
jednotky	jednotka	k1gFnSc2	jednotka
bránily	bránit	k5eAaImAgFnP	bránit
Varšavu	Varšava	k1gFnSc4	Varšava
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
modlinská	modlinský	k2eAgFnSc1d1	modlinský
pevnost	pevnost	k1gFnSc1	pevnost
severně	severně	k6eAd1	severně
od	od	k7c2	od
Varšavy	Varšava	k1gFnSc2	Varšava
padla	padnout	k5eAaImAgFnS	padnout
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
po	po	k7c6	po
úporné	úporný	k2eAgFnSc6d1	úporná
šestidenní	šestidenní	k2eAgFnSc6d1	šestidenní
obraně	obrana	k1gFnSc6	obrana
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
střetnutí	střetnutí	k1gNnPc2	střetnutí
<g/>
,	,	kIx,	,
zatlačily	zatlačit	k5eAaPmAgFnP	zatlačit
sovětské	sovětský	k2eAgFnPc4d1	sovětská
jednotky	jednotka	k1gFnPc4	jednotka
Poláky	Polák	k1gMnPc4	Polák
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
Wytyczno	Wytyczna	k1gFnSc5	Wytyczna
do	do	k7c2	do
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
izolovaných	izolovaný	k2eAgFnPc2d1	izolovaná
polských	polský	k2eAgFnPc2d1	polská
jednotek	jednotka	k1gFnPc2	jednotka
udrželo	udržet	k5eAaPmAgNnS	udržet
pozice	pozice	k1gFnPc4	pozice
dlouho	dlouho	k6eAd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byly	být	k5eAaImAgFnP	být
obklíčeny	obklíčen	k2eAgFnPc1d1	obklíčena
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Sarnská	Sarnský	k2eAgFnSc1d1	Sarnský
opevněná	opevněný	k2eAgFnSc1d1	opevněná
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
Volyni	Volyně	k1gFnSc6	Volyně
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
jednotkou	jednotka	k1gFnSc7	jednotka
polské	polský	k2eAgFnSc2d1	polská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
byla	být	k5eAaImAgFnS	být
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
skupina	skupina	k1gFnSc1	skupina
Polesí	Polesí	k1gNnSc2	Polesí
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
velel	velet	k5eAaImAgMnS	velet
generál	generál	k1gMnSc1	generál
Franciszek	Franciszka	k1gFnPc2	Franciszka
Kleeberg	Kleeberg	k1gMnSc1	Kleeberg
<g/>
.	.	kIx.	.
</s>
<s>
Kleeberg	Kleeberg	k1gInSc1	Kleeberg
kapituloval	kapitulovat	k5eAaBmAgInS	kapitulovat
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
po	po	k7c6	po
čtyřdenní	čtyřdenní	k2eAgFnSc6d1	čtyřdenní
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Kock	Kock	k1gInSc4	Kock
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
záříjová	záříjový	k2eAgFnSc1d1	záříjová
kampaň	kampaň	k1gFnSc1	kampaň
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Molotov	Molotov	k1gInSc4	Molotov
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
přednesené	přednesený	k2eAgFnSc6d1	přednesená
Nejvyššímu	vysoký	k2eAgMnSc3d3	nejvyšší
sovětu	sovět	k1gInSc6	sovět
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Krátký	krátký	k2eAgInSc1d1	krátký
úder	úder	k1gInSc1	úder
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
následované	následovaný	k2eAgNnSc1d1	následované
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
stačil	stačit	k5eAaBmAgInS	stačit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
nic	nic	k6eAd1	nic
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
odporné	odporný	k2eAgFnSc2d1	odporná
kreatury	kreatura	k1gFnSc2	kreatura
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
byla	být	k5eAaImAgFnS	být
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Domácí	domácí	k2eAgFnSc1d1	domácí
reakce	reakce	k1gFnSc1	reakce
===	===	k?	===
</s>
</p>
<p>
<s>
Situaci	situace	k1gFnSc4	situace
zkomplikovala	zkomplikovat	k5eAaPmAgFnS	zkomplikovat
reakce	reakce	k1gFnSc1	reakce
neetnických	etnický	k2eNgMnPc2d1	etnický
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
<g/>
,	,	kIx,	,
Bělorusů	Bělorus	k1gMnPc2	Bělorus
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
vítalo	vítat	k5eAaImAgNnS	vítat
sovětská	sovětský	k2eAgNnPc4d1	sovětské
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
předměstí	předměstí	k1gNnSc6	předměstí
Brestu	Brest	k1gInSc2	Brest
se	se	k3xPyFc4	se
seběhli	seběhnout	k5eAaPmAgMnP	seběhnout
místní	místní	k2eAgMnPc1d1	místní
komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uvítali	uvítat	k5eAaPmAgMnP	uvítat
sovětské	sovětský	k2eAgMnPc4d1	sovětský
vojáky	voják	k1gMnPc4	voják
chlebem	chléb	k1gInSc7	chléb
a	a	k8xC	a
solí	sůl	k1gFnSc7	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
byl	být	k5eAaImAgMnS	být
vztyčen	vztyčen	k2eAgInSc4d1	vztyčen
triumfální	triumfální	k2eAgInSc4d1	triumfální
oblouk	oblouk	k1gInSc4	oblouk
tvořený	tvořený	k2eAgInSc4d1	tvořený
dvěma	dva	k4xCgFnPc7	dva
tyčemi	tyč	k1gFnPc7	tyč
ozdobenými	ozdobený	k2eAgFnPc7d1	ozdobená
smrkovými	smrkový	k2eAgFnPc7d1	smrková
větvemi	větev	k1gFnPc7	větev
a	a	k8xC	a
květinami	květina	k1gFnPc7	květina
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
pruh	pruh	k1gInSc1	pruh
rudé	rudý	k2eAgFnSc2d1	rudá
látky	látka	k1gFnSc2	látka
napnutý	napnutý	k2eAgInSc4d1	napnutý
mezi	mezi	k7c7	mezi
tyčemi	tyč	k1gFnPc7	tyč
nesl	nést	k5eAaImAgInS	nést
slogan	slogan	k1gInSc1	slogan
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
velebící	velebící	k2eAgInSc4d1	velebící
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
a	a	k8xC	a
vítající	vítající	k2eAgFnSc4d1	vítající
Rudou	rudý	k2eAgFnSc4d1	rudá
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Reakci	reakce	k1gFnSc4	reakce
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
zmínil	zmínit	k5eAaPmAgMnS	zmínit
Lev	Lev	k1gMnSc1	Lev
Zacharovič	Zacharovič	k1gMnSc1	Zacharovič
Mechlis	Mechlis	k1gInSc4	Mechlis
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
řekl	říct	k5eAaPmAgMnS	říct
Stalinovi	Stalin	k1gMnSc3	Stalin
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
Západní	západní	k2eAgFnSc6d1	západní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
vítali	vítat	k5eAaImAgMnP	vítat
Sověty	Sověty	k1gInPc4	Sověty
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
opravdové	opravdový	k2eAgMnPc4d1	opravdový
osvoboditele	osvoboditel	k1gMnPc4	osvoboditel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
se	se	k3xPyFc4	se
vzbouřila	vzbouřit	k5eAaPmAgNnP	vzbouřit
a	a	k8xC	a
komunističtí	komunistický	k2eAgMnPc1d1	komunistický
partyzáni	partyzán	k1gMnPc1	partyzán
organizovali	organizovat	k5eAaBmAgMnP	organizovat
protipolská	protipolský	k2eAgNnPc4d1	protipolský
místní	místní	k2eAgNnPc4d1	místní
povstání	povstání	k1gNnPc4	povstání
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ve	v	k7c6	v
Skidelu	Skidel	k1gInSc6	Skidel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reakce	reakce	k1gFnPc1	reakce
spojenců	spojenec	k1gMnPc2	spojenec
===	===	k?	===
</s>
</p>
<p>
<s>
Reakce	reakce	k1gFnSc1	reakce
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Británie	Británie	k1gFnSc2	Británie
na	na	k7c4	na
invazi	invaze	k1gFnSc4	invaze
a	a	k8xC	a
anexi	anexe	k1gFnSc4	anexe
východního	východní	k2eAgNnSc2d1	východní
Polska	Polsko	k1gNnSc2	Polsko
byla	být	k5eAaImAgFnS	být
zdrženlivá	zdrženlivý	k2eAgFnSc1d1	zdrženlivá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
si	se	k3xPyFc3	se
nepřála	přát	k5eNaImAgFnS	přát
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
konfrontaci	konfrontace	k1gFnSc4	konfrontace
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
Polsko-britské	polskoritský	k2eAgFnSc2d1	polsko-britská
obranné	obranný	k2eAgFnSc2d1	obranná
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
Britové	Brit	k1gMnPc1	Brit
slíbili	slíbit	k5eAaPmAgMnP	slíbit
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
evropská	evropský	k2eAgFnSc1d1	Evropská
mocnost	mocnost	k1gFnSc1	mocnost
napadne	napadnout	k5eAaPmIp3nS	napadnout
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Tajný	tajný	k2eAgInSc1d1	tajný
protokol	protokol	k1gInSc1	protokol
upřesňoval	upřesňovat	k5eAaImAgInS	upřesňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
evropskou	evropský	k2eAgFnSc7d1	Evropská
mocností	mocnost	k1gFnSc7	mocnost
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
polský	polský	k2eAgMnSc1d1	polský
vyslanec	vyslanec	k1gMnSc1	vyslanec
Edward	Edward	k1gMnSc1	Edward
Raczyński	Raczyńske	k1gFnSc4	Raczyńske
připomněl	připomnět	k5eAaPmAgMnS	připomnět
ministru	ministr	k1gMnSc3	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
lordu	lord	k1gMnSc3	lord
Halifaxovi	Halifaxa	k1gMnSc3	Halifaxa
existenci	existence	k1gFnSc4	existence
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
neomalené	omalený	k2eNgFnPc1d1	neomalená
odpovědi	odpověď	k1gFnPc1	odpověď
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
věcí	věc	k1gFnSc7	věc
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
vyhlásí	vyhlásit	k5eAaPmIp3nP	vyhlásit
válku	válka	k1gFnSc4	válka
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Neville	Neville	k1gNnSc2	Neville
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
veřejný	veřejný	k2eAgInSc4d1	veřejný
závazek	závazek	k1gInSc4	závazek
obnovení	obnovení	k1gNnSc2	obnovení
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
vydal	vydat	k5eAaPmAgMnS	vydat
jen	jen	k9	jen
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
odsudek	odsudek	k1gInSc4	odsudek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postoj	postoj	k1gInSc1	postoj
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
britský	britský	k2eAgInSc1d1	britský
pokus	pokus	k1gInSc1	pokus
udržet	udržet	k5eAaPmF	udržet
rovnováhu	rovnováha	k1gFnSc4	rovnováha
<g/>
:	:	kIx,	:
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
zájmy	zájem	k1gInPc1	zájem
Británie	Británie	k1gFnSc2	Británie
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
válečnému	válečný	k2eAgNnSc3d1	válečné
úsilí	úsilí	k1gNnSc3	úsilí
<g/>
,	,	kIx,	,
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
budoucího	budoucí	k2eAgNnSc2d1	budoucí
anglo-sovětského	angloovětský	k2eAgNnSc2d1	anglo-sovětský
spojenectví	spojenectví	k1gNnSc2	spojenectví
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
bylo	být	k5eAaImAgNnS	být
rozpolceno	rozpolcen	k2eAgNnSc1d1	rozpolceno
<g/>
;	;	kIx,	;
veřejnost	veřejnost	k1gFnSc1	veřejnost
zčásti	zčásti	k6eAd1	zčásti
invazi	invaze	k1gFnSc4	invaze
odsuzovala	odsuzovat	k5eAaImAgFnS	odsuzovat
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
shledával	shledávat	k5eAaImAgMnS	shledávat
sovětské	sovětský	k2eAgInPc4d1	sovětský
nároky	nárok	k1gInPc4	nárok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
oprávněnými	oprávněný	k2eAgMnPc7d1	oprávněný
<g/>
.	.	kIx.	.
<g/>
Ačkoli	ačkoli	k8xS	ačkoli
Francie	Francie	k1gFnSc1	Francie
slíbila	slíbit	k5eAaPmAgFnS	slíbit
Polsku	Polska	k1gFnSc4	Polska
pomoc	pomoc	k1gFnSc4	pomoc
včetně	včetně	k7c2	včetně
letecké	letecký	k2eAgFnSc2d1	letecká
podpory	podpora	k1gFnSc2	podpora
<g/>
,	,	kIx,	,
sliby	slib	k1gInPc1	slib
nebyly	být	k5eNaImAgInP	být
splněny	splnit	k5eAaPmNgInP	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Francouzsko-polská	francouzskoolský	k2eAgFnSc1d1	francouzsko-polský
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
neměla	mít	k5eNaImAgFnS	mít
příliš	příliš	k6eAd1	příliš
velkou	velký	k2eAgFnSc4d1	velká
podporu	podpora	k1gFnSc4	podpora
francouzského	francouzský	k2eAgNnSc2d1	francouzské
armádního	armádní	k2eAgNnSc2d1	armádní
velení	velení	k1gNnSc2	velení
a	a	k8xC	a
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
se	se	k3xPyFc4	se
během	během	k7c2	během
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
francouzského	francouzský	k2eAgInSc2d1	francouzský
náhledu	náhled	k1gInSc2	náhled
<g/>
,	,	kIx,	,
německo-sovětské	německoovětský	k2eAgNnSc1d1	německo-sovětský
spojenectví	spojenectví	k1gNnSc1	spojenectví
je	být	k5eAaImIp3nS	být
křehké	křehký	k2eAgNnSc1d1	křehké
a	a	k8xC	a
jasné	jasný	k2eAgNnSc1d1	jasné
odsouzení	odsouzení	k1gNnSc1	odsouzení
nebo	nebo	k8xC	nebo
protiopatření	protiopatření	k1gNnSc1	protiopatření
nejsou	být	k5eNaImIp3nP	být
zájmem	zájem	k1gInSc7	zájem
ani	ani	k8xC	ani
Polska	Polsko	k1gNnSc2	Polsko
ani	ani	k8xC	ani
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
sověti	sovět	k5eAaPmF	sovět
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Británie	Británie	k1gFnSc2	Británie
usoudily	usoudit	k5eAaPmAgFnP	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
okamžitě	okamžitě	k6eAd1	okamžitě
nemohou	moct	k5eNaImIp3nP	moct
pro	pro	k7c4	pro
Polsko	Polsko	k1gNnSc4	Polsko
nic	nic	k6eAd1	nic
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
plány	plán	k1gInPc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
podnikla	podniknout	k5eAaPmAgFnS	podniknout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	zářit	k5eAaImIp3nS	zářit
váhavou	váhavý	k2eAgFnSc4d1	váhavá
ofenzivu	ofenziva	k1gFnSc4	ofenziva
v	v	k7c6	v
Sársku	Sársko	k1gNnSc6	Sársko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Polska	Polsko	k1gNnSc2	Polsko
se	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
za	za	k7c4	za
Maginotovu	Maginotův	k2eAgFnSc4d1	Maginotova
linii	linie	k1gFnSc4	linie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
projevu	projev	k1gInSc6	projev
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gMnSc1	Churchill
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Důsledky	důsledek	k1gInPc1	důsledek
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1939	[number]	k4	1939
Molotov	Molotov	k1gInSc1	Molotov
zpravil	zpravit	k5eAaPmAgInS	zpravit
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
sovět	sovět	k1gInSc1	sovět
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
737	[number]	k4	737
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
1	[number]	k4	1
862	[number]	k4	862
raněných	raněný	k1gMnPc2	raněný
během	během	k7c2	během
tažení	tažení	k1gNnSc2	tažení
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
polští	polský	k2eAgMnPc1d1	polský
specialisté	specialista	k1gMnPc1	specialista
udávají	udávat	k5eAaImIp3nP	udávat
až	až	k9	až
3	[number]	k4	3
000	[number]	k4	000
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
8	[number]	k4	8
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
000	[number]	k4	000
raněných	raněný	k1gMnPc2	raněný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
3	[number]	k4	3
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
230	[number]	k4	230
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
450	[number]	k4	450
000	[number]	k4	000
padlo	padnout	k5eAaImAgNnS	padnout
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
mnohdy	mnohdy	k6eAd1	mnohdy
nedodrželi	dodržet	k5eNaPmAgMnP	dodržet
sjednané	sjednaný	k2eAgFnPc4d1	sjednaná
podmínky	podmínka	k1gFnPc4	podmínka
složení	složení	k1gNnSc2	složení
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
slíbili	slíbit	k5eAaPmAgMnP	slíbit
Polákům	Polák	k1gMnPc3	Polák
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
po	po	k7c6	po
složení	složení	k1gNnSc6	složení
zbraní	zbraň	k1gFnPc2	zbraň
je	být	k5eAaImIp3nS	být
uvěznili	uvěznit	k5eAaPmAgMnP	uvěznit
<g/>
.	.	kIx.	.
<g/>
Okamžikem	okamžik	k1gInSc7	okamžik
invaze	invaze	k1gFnSc2	invaze
přestal	přestat	k5eAaPmAgInS	přestat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
uznávat	uznávat	k5eAaImF	uznávat
polský	polský	k2eAgInSc4d1	polský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
nevyhlásila	vyhlásit	k5eNaPmAgFnS	vyhlásit
formálně	formálně	k6eAd1	formálně
válku	válka	k1gFnSc4	válka
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
mělo	mít	k5eAaImAgNnS	mít
vážné	vážný	k2eAgInPc4d1	vážný
důsledky	důsledek	k1gInPc4	důsledek
a	a	k8xC	a
Smigly-Rydz	Smigly-Rydz	k1gInSc1	Smigly-Rydz
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
kritizován	kritizován	k2eAgInSc1d1	kritizován
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
zabili	zabít	k5eAaPmAgMnP	zabít
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
polských	polský	k2eAgMnPc2d1	polský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
,	,	kIx,	,
některé	některý	k3yIgMnPc4	některý
ještě	ještě	k9	ještě
během	během	k7c2	během
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
zabili	zabít	k5eAaPmAgMnP	zabít
42	[number]	k4	42
pacientů	pacient	k1gMnPc2	pacient
a	a	k8xC	a
personálu	personál	k1gInSc2	personál
vojenské	vojenský	k2eAgFnSc2d1	vojenská
nemocnice	nemocnice	k1gFnSc2	nemocnice
ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Grabowiec	Grabowiec	k1gMnSc1	Grabowiec
nedaleko	nedaleko	k7c2	nedaleko
Zamośće	Zamość	k1gFnSc2	Zamość
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
také	také	k9	také
povraždili	povraždit	k5eAaPmAgMnP	povraždit
všechny	všechen	k3xTgMnPc4	všechen
důstojníky	důstojník	k1gMnPc4	důstojník
zajaté	zajatý	k2eAgMnPc4d1	zajatý
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Szacku	Szack	k1gInSc2	Szack
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
polských	polský	k2eAgMnPc2d1	polský
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
civilistů	civilista	k1gMnPc2	civilista
v	v	k7c6	v
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
během	během	k7c2	během
Katyňského	Katyňský	k2eAgInSc2d1	Katyňský
masakru	masakr	k1gInSc2	masakr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
vězeních	vězení	k1gNnPc6	vězení
NKVD	NKVD	kA	NKVD
byli	být	k5eAaImAgMnP	být
vězni	vězeň	k1gMnPc1	vězeň
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
mučení	mučený	k2eAgMnPc1d1	mučený
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
<g/>
Poláci	Polák	k1gMnPc1	Polák
a	a	k8xC	a
sověti	sovět	k5eAaImF	sovět
obnovili	obnovit	k5eAaPmAgMnP	obnovit
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
dohody	dohoda	k1gFnSc2	dohoda
Sikorski	Sikorski	k1gNnSc3	Sikorski
<g/>
–	–	k?	–
<g/>
Majskij	Majskij	k1gFnSc2	Majskij
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sověti	sovět	k5eAaPmF	sovět
je	on	k3xPp3gNnPc4	on
znovu	znovu	k6eAd1	znovu
přerušili	přerušit	k5eAaPmAgMnP	přerušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Poláci	Polák	k1gMnPc1	Polák
požadovali	požadovat	k5eAaImAgMnP	požadovat
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
nedávno	nedávno	k6eAd1	nedávno
objevených	objevený	k2eAgFnPc2d1	objevená
pohřebních	pohřební	k2eAgFnPc2d1	pohřební
jam	jáma	k1gFnPc2	jáma
v	v	k7c6	v
Katyni	Katyně	k1gFnSc6	Katyně
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
poté	poté	k6eAd1	poté
tlačili	tlačit	k5eAaImAgMnP	tlačit
západní	západní	k2eAgMnPc4d1	západní
spojence	spojenec	k1gMnPc4	spojenec
k	k	k7c3	k
uznání	uznání	k1gNnSc3	uznání
prosovětské	prosovětský	k2eAgFnSc2d1	prosovětská
loutkové	loutkový	k2eAgFnSc2d1	loutková
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Wandou	Wanda	k1gFnSc7	Wanda
Wasilewskou	Wasilewský	k2eAgFnSc7d1	Wasilewský
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
podepsal	podepsat	k5eAaPmAgInS	podepsat
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
přátelství	přátelství	k1gNnSc6	přátelství
<g/>
,	,	kIx,	,
spolupráci	spolupráce	k1gFnSc6	spolupráce
a	a	k8xC	a
hranicích	hranice	k1gFnPc6	hranice
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
tajných	tajný	k2eAgInPc2d1	tajný
dodatků	dodatek	k1gInPc2	dodatek
paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
do	do	k7c2	do
sféry	sféra	k1gFnSc2	sféra
sovětského	sovětský	k2eAgInSc2d1	sovětský
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
hranice	hranice	k1gFnSc2	hranice
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
byla	být	k5eAaImAgFnS	být
posunuta	posunout	k5eAaPmNgFnS	posunout
východně	východně	k6eAd1	východně
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
popisovanou	popisovaný	k2eAgFnSc4d1	popisovaná
jako	jako	k8xC	jako
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
dělení	dělení	k1gNnSc4	dělení
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgNnPc4	všechen
území	území	k1gNnPc4	území
východně	východně	k6eAd1	východně
od	od	k7c2	od
řek	řeka	k1gFnPc2	řeka
Pisa	Pisa	k1gFnSc1	Pisa
<g/>
,	,	kIx,	,
Narew	Narew	k1gMnPc1	Narew
<g/>
,	,	kIx,	,
Západní	západní	k2eAgMnPc1d1	západní
Bug	Bug	k1gMnPc1	Bug
a	a	k8xC	a
San	San	k1gMnPc1	San
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
okolo	okolo	k7c2	okolo
200	[number]	k4	200
000	[number]	k4	000
km2	km2	k4	km2
obývaných	obývaný	k2eAgInPc2d1	obývaný
13,5	[number]	k4	13,5
miliony	milion	k4xCgInPc1	milion
polských	polský	k2eAgMnPc2d1	polský
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
tvořená	tvořený	k2eAgFnSc1d1	tvořená
touto	tento	k3xDgFnSc7	tento
smlouvou	smlouva	k1gFnSc7	smlouva
zhruba	zhruba	k6eAd1	zhruba
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
Curzonově	Curzonův	k2eAgFnSc6d1	Curzonova
linii	linie	k1gFnSc6	linie
navržené	navržený	k2eAgMnPc4d1	navržený
Brity	Brit	k1gMnPc4	Brit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
momentu	moment	k1gInSc2	moment
Stalin	Stalin	k1gMnSc1	Stalin
úspěšně	úspěšně	k6eAd1	úspěšně
využil	využít	k5eAaPmAgMnS	využít
při	při	k7c6	při
jednáních	jednání	k1gNnPc6	jednání
se	s	k7c7	s
západními	západní	k2eAgMnPc7d1	západní
spojenci	spojenec	k1gMnPc7	spojenec
na	na	k7c6	na
Teheránské	teheránský	k2eAgFnSc6d1	Teheránská
a	a	k8xC	a
Jaltské	jaltský	k2eAgFnSc6d1	Jaltská
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
původně	původně	k6eAd1	původně
zasela	zasít	k5eAaPmAgFnS	zasít
zmatek	zmatek	k1gInSc4	zmatek
mezi	mezi	k7c7	mezi
místními	místní	k2eAgFnPc7d1	místní
tvrzením	tvrzení	k1gNnPc3	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
přichází	přicházet	k5eAaImIp3nS	přicházet
zachránit	zachránit	k5eAaPmF	zachránit
Polsko	Polsko	k1gNnSc4	Polsko
před	před	k7c7	před
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
postup	postup	k1gInSc1	postup
překvapil	překvapit	k5eAaPmAgInS	překvapit
místní	místní	k2eAgNnSc4d1	místní
společenství	společenství	k1gNnSc4	společenství
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc4	jejich
vůdce	vůdce	k1gMnPc4	vůdce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
postrádali	postrádat	k5eAaImAgMnP	postrádat
pokyny	pokyn	k1gInPc4	pokyn
jak	jak	k8xC	jak
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
invazi	invaze	k1gFnSc4	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
zpočátku	zpočátku	k6eAd1	zpočátku
upřednostňovali	upřednostňovat	k5eAaImAgMnP	upřednostňovat
sovětský	sovětský	k2eAgInSc4d1	sovětský
režim	režim	k1gInSc4	režim
před	před	k7c7	před
německým	německý	k2eAgMnSc7d1	německý
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
sověti	sovět	k5eAaPmF	sovět
brzo	brzo	k6eAd1	brzo
začali	začít	k5eAaPmAgMnP	začít
zavádět	zavádět	k5eAaImF	zavádět
svou	svůj	k3xOyFgFnSc4	svůj
ideologii	ideologie	k1gFnSc4	ideologie
do	do	k7c2	do
místního	místní	k2eAgInSc2d1	místní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
začaly	začít	k5eAaPmAgFnP	začít
konfiskace	konfiskace	k1gFnPc1	konfiskace
<g/>
,	,	kIx,	,
znárodňování	znárodňování	k1gNnSc1	znárodňování
a	a	k8xC	a
přerozdělování	přerozdělování	k1gNnSc1	přerozdělování
soukromého	soukromý	k2eAgNnSc2d1	soukromé
a	a	k8xC	a
polského	polský	k2eAgNnSc2d1	polské
státního	státní	k2eAgNnSc2d1	státní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
následovaly	následovat	k5eAaImAgFnP	následovat
anexi	anexe	k1gFnSc4	anexe
<g/>
,	,	kIx,	,
sověti	sovět	k5eAaImF	sovět
uvěznili	uvěznit	k5eAaPmAgMnP	uvěznit
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
polských	polský	k2eAgMnPc2d1	polský
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nemožnosti	nemožnost	k1gFnSc3	nemožnost
přístupu	přístup	k1gInSc2	přístup
do	do	k7c2	do
sovětských	sovětský	k2eAgInPc2d1	sovětský
archivů	archiv	k1gInPc2	archiv
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
počty	počet	k1gInPc4	počet
polských	polský	k2eAgMnPc2d1	polský
občanů	občan	k1gMnPc2	občan
deportovaných	deportovaný	k2eAgMnPc2d1	deportovaný
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
východního	východní	k2eAgNnSc2d1	východní
Polska	Polsko	k1gNnSc2	Polsko
pouze	pouze	k6eAd1	pouze
odhadovaly	odhadovat	k5eAaImAgFnP	odhadovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
odhadovaly	odhadovat	k5eAaImAgInP	odhadovat
počty	počet	k1gInPc1	počet
zabitých	zabitý	k1gMnPc2	zabitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
pracích	prak	k1gInPc6	prak
byla	být	k5eAaImAgNnP	být
udávána	udáván	k2eAgNnPc1d1	udáváno
různá	různý	k2eAgNnPc1d1	různé
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
350	[number]	k4	350
000	[number]	k4	000
a	a	k8xC	a
1	[number]	k4	1
500	[number]	k4	500
000	[number]	k4	000
deportovaných	deportovaný	k2eAgMnPc2d1	deportovaný
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
mezi	mezi	k7c4	mezi
250	[number]	k4	250
000	[number]	k4	000
a	a	k8xC	a
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
zahrnovala	zahrnovat	k5eAaImAgNnP	zahrnovat
především	především	k6eAd1	především
civilisty	civilista	k1gMnPc7	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
sovětských	sovětský	k2eAgInPc2d1	sovětský
tajných	tajný	k2eAgInPc2d1	tajný
archivů	archiv	k1gInPc2	archiv
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
odhadů	odhad	k1gInPc2	odhad
bližší	blízký	k2eAgFnSc3d2	bližší
pravdě	pravda	k1gFnSc3	pravda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
sedmdesátého	sedmdesátý	k4xOgNnSc2	sedmdesátý
výročí	výročí	k1gNnSc2	výročí
sovětské	sovětský	k2eAgFnSc2d1	sovětská
invaze	invaze	k1gFnSc2	invaze
<g/>
,	,	kIx,	,
polský	polský	k2eAgInSc1d1	polský
Institut	institut	k1gInSc1	institut
národní	národní	k2eAgFnSc2d1	národní
paměti	paměť	k1gFnSc2	paměť
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
vědečtí	vědecký	k2eAgMnPc1d1	vědecký
pracovníci	pracovník	k1gMnPc1	pracovník
snížili	snížit	k5eAaPmAgMnP	snížit
odhady	odhad	k1gInPc4	odhad
deportovaných	deportovaný	k2eAgFnPc2d1	deportovaná
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
na	na	k7c4	na
320	[number]	k4	320
000	[number]	k4	000
a	a	k8xC	a
odhadli	odhadnout	k5eAaPmAgMnP	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
150	[number]	k4	150
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
pod	pod	k7c7	pod
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
nadvládou	nadvláda	k1gFnSc7	nadvláda
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
a	a	k8xC	a
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
===	===	k?	===
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
13,5	[number]	k4	13,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
žijících	žijící	k2eAgMnPc2d1	žijící
čerstvě	čerstvě	k6eAd1	čerstvě
anektovaných	anektovaný	k2eAgNnPc6d1	anektované
územích	území	k1gNnPc6	území
tvořili	tvořit	k5eAaImAgMnP	tvořit
Poláci	Polák	k1gMnPc1	Polák
největší	veliký	k2eAgFnSc4d3	veliký
etnickou	etnický	k2eAgFnSc4d1	etnická
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Bělorusové	Bělorus	k1gMnPc1	Bělorus
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
společně	společně	k6eAd1	společně
tvořili	tvořit	k5eAaImAgMnP	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<g/>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
posílilo	posílit	k5eAaPmAgNnS	posílit
zdání	zdání	k1gNnSc1	zdání
právoplatnosti	právoplatnost	k1gFnSc2	právoplatnost
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
připojených	připojený	k2eAgNnPc6d1	připojené
územích	území	k1gNnPc6	území
uspořádány	uspořádán	k2eAgInPc1d1	uspořádán
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
volby	volba	k1gFnSc2	volba
do	do	k7c2	do
zastupitelských	zastupitelský	k2eAgInPc2d1	zastupitelský
orgánů	orgán	k1gInPc2	orgán
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
(	(	kIx(	(
<g/>
sovětů	sovět	k1gInPc2	sovět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bělorusové	Bělorus	k1gMnPc1	Bělorus
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
pod	pod	k7c7	pod
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
tlakem	tlak	k1gInSc7	tlak
polonizace	polonizace	k1gFnSc2	polonizace
a	a	k8xC	a
polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
rovněž	rovněž	k9	rovněž
rozhodně	rozhodně	k6eAd1	rozhodně
potlačovala	potlačovat	k5eAaImAgNnP	potlačovat
jejich	jejich	k3xOp3gNnPc1	jejich
separatistická	separatistický	k2eAgNnPc1d1	separatistické
hnutí	hnutí	k1gNnPc1	hnutí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pociťovali	pociťovat	k5eAaImAgMnP	pociťovat
jen	jen	k9	jen
malou	malý	k2eAgFnSc4d1	malá
loajalitu	loajalita	k1gFnSc4	loajalita
k	k	k7c3	k
Polskému	polský	k2eAgInSc3d1	polský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
Bělorusové	Bělorus	k1gMnPc1	Bělorus
a	a	k8xC	a
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
byli	být	k5eAaImAgMnP	být
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
režimem	režim	k1gInSc7	režim
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobil	způsobit	k5eAaPmAgMnS	způsobit
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
hladomoru	hladomor	k1gInSc2	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
<g/>
,	,	kIx,	,
chudí	chudý	k2eAgMnPc1d1	chudý
vítali	vítat	k5eAaImAgMnP	vítat
Sověty	Sověty	k1gInPc4	Sověty
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
střední	střední	k2eAgFnPc1d1	střední
vrstvy	vrstva	k1gFnPc1	vrstva
stály	stát	k5eAaImAgFnP	stát
spíše	spíše	k9	spíše
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
vlastního	vlastní	k2eAgNnSc2d1	vlastní
znovusjednocení	znovusjednocení	k1gNnSc2	znovusjednocení
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
rychle	rychle	k6eAd1	rychle
zavedli	zavést	k5eAaPmAgMnP	zavést
politiku	politika	k1gFnSc4	politika
sovětizace	sovětizace	k1gFnSc2	sovětizace
včetně	včetně	k7c2	včetně
násilné	násilný	k2eAgFnSc2d1	násilná
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
západní	západní	k2eAgFnSc6d1	západní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
Sověti	Sovět	k1gMnPc1	Sovět
rychle	rychle	k6eAd1	rychle
zavedli	zavést	k5eAaPmAgMnP	zavést
politiku	politika	k1gFnSc4	politika
sovětizace	sovětizace	k1gFnSc2	sovětizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
rámci	rámec	k1gInSc6	rámec
bezohledně	bezohledně	k6eAd1	bezohledně
zrušili	zrušit	k5eAaPmAgMnP	zrušit
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
spolky	spolek	k1gInPc4	spolek
a	a	k8xC	a
popravili	popravit	k5eAaPmAgMnP	popravit
či	či	k8xC	či
uvěznili	uvěznit	k5eAaPmAgMnP	uvěznit
jejich	jejich	k3xOp3gMnPc4	jejich
vůdce	vůdce	k1gMnPc4	vůdce
jako	jako	k8xS	jako
nepřátele	nepřítel	k1gMnPc4	nepřítel
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
potlačili	potlačit	k5eAaPmAgMnP	potlačit
protipolskou	protipolský	k2eAgFnSc4d1	protipolská
Organizaci	organizace	k1gFnSc4	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
aktivně	aktivně	k6eAd1	aktivně
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
proti	proti	k7c3	proti
polskému	polský	k2eAgInSc3d1	polský
režimu	režim	k1gInSc3	režim
a	a	k8xC	a
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
jednotného	jednotný	k2eAgInSc2d1	jednotný
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
událostmi	událost	k1gFnPc7	událost
ze	z	k7c2	z
září	září	k1gNnSc2	září
1939	[number]	k4	1939
historie	historie	k1gFnSc2	historie
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
neskončila	skončit	k5eNaPmAgFnS	skončit
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
samostatnosti	samostatnost	k1gFnPc1	samostatnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cenzura	cenzura	k1gFnSc1	cenzura
===	===	k?	===
</s>
</p>
<p>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
cenzura	cenzura	k1gFnSc1	cenzura
později	pozdě	k6eAd2	pozdě
potlačila	potlačit	k5eAaPmAgFnS	potlačit
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
podrobnostech	podrobnost	k1gFnPc6	podrobnost
invaze	invaze	k1gFnSc2	invaze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
a	a	k8xC	a
jejích	její	k3xOp3gInPc6	její
důsledcích	důsledek	k1gInPc6	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Politbyro	politbyro	k1gNnSc1	politbyro
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
operaci	operace	k1gFnSc4	operace
nazývalo	nazývat	k5eAaImAgNnS	nazývat
osvobozovací	osvobozovací	k2eAgFnSc7d1	osvobozovací
kampaní	kampaň	k1gFnSc7	kampaň
a	a	k8xC	a
ani	ani	k8xC	ani
později	pozdě	k6eAd2	pozdě
sověti	sovět	k5eAaImF	sovět
neopustili	opustit	k5eNaPmAgMnP	opustit
tuto	tento	k3xDgFnSc4	tento
linii	linie	k1gFnSc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
publikování	publikování	k1gNnSc4	publikování
kopií	kopie	k1gFnPc2	kopie
tajných	tajný	k2eAgInPc2d1	tajný
dodatků	dodatek	k1gInPc2	dodatek
ke	k	k7c3	k
smlouvě	smlouva	k1gFnSc3	smlouva
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
<g/>
,	,	kIx,	,
po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
sovětská	sovětský	k2eAgNnPc1d1	sovětské
oficiální	oficiální	k2eAgNnPc1d1	oficiální
místa	místo	k1gNnPc1	místo
popírala	popírat	k5eAaImAgNnP	popírat
jejich	jejich	k3xOp3gFnSc4	jejich
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
tajného	tajný	k2eAgInSc2d1	tajný
protokolu	protokol	k1gInSc2	protokol
byla	být	k5eAaImAgFnS	být
popírána	popírán	k2eAgFnSc1d1	popírána
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Cenzura	cenzura	k1gFnSc1	cenzura
působila	působit	k5eAaImAgFnS	působit
i	i	k9	i
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nic	nic	k3yNnSc1	nic
nenarušilo	narušit	k5eNaPmAgNnS	narušit
obraz	obraz	k1gInSc4	obraz
polsko-sovětského	polskoovětský	k2eAgNnSc2d1	polsko-sovětský
přátelství	přátelství	k1gNnSc2	přátelství
prosazovaný	prosazovaný	k2eAgInSc1d1	prosazovaný
oběma	dva	k4xCgInPc7	dva
komunistickými	komunistický	k2eAgInPc7d1	komunistický
režimy	režim	k1gInPc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
politika	politika	k1gFnSc1	politika
připouštěla	připouštět	k5eAaImAgFnS	připouštět
jen	jen	k9	jen
takový	takový	k3xDgInSc4	takový
výklad	výklad	k1gInSc4	výklad
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vykreslil	vykreslit	k5eAaPmAgMnS	vykreslit
sovětské	sovětský	k2eAgNnSc4d1	sovětské
tažení	tažení	k1gNnSc4	tažení
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
jako	jako	k8xS	jako
znovusjednocení	znovusjednocení	k1gNnSc4	znovusjednocení
Bělorusů	Bělorus	k1gMnPc2	Bělorus
a	a	k8xC	a
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
a	a	k8xC	a
osvobození	osvobození	k1gNnSc4	osvobození
Poláků	Polák	k1gMnPc2	Polák
ze	z	k7c2	z
jha	jho	k1gNnSc2	jho
oligarchického	oligarchický	k2eAgInSc2d1	oligarchický
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Úřady	úřad	k1gInPc1	úřad
tvrdě	tvrdě	k6eAd1	tvrdě
potlačily	potlačit	k5eAaPmAgFnP	potlačit
každý	každý	k3xTgInSc4	každý
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
zkoumání	zkoumání	k1gNnSc4	zkoumání
či	či	k8xC	či
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Invazí	invaze	k1gFnPc2	invaze
se	se	k3xPyFc4	se
zabývalo	zabývat	k5eAaImAgNnS	zabývat
několik	několik	k4yIc1	několik
samizdatových	samizdatový	k2eAgFnPc2d1	samizdatová
publikací	publikace	k1gFnPc2	publikace
a	a	k8xC	a
například	například	k6eAd1	například
i	i	k9	i
protestsong	protestsong	k1gInSc4	protestsong
Ballada	Ballada	k1gFnSc1	Ballada
wrześniowa	wrześniowus	k1gMnSc2	wrześniowus
Jacka	Jacek	k1gInSc2	Jacek
Kaczmarského	Kaczmarský	k2eAgInSc2d1	Kaczmarský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památný	památný	k2eAgInSc4d1	památný
den	den	k1gInSc4	den
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
připomínku	připomínka	k1gFnSc4	připomínka
uzavření	uzavření	k1gNnSc2	uzavření
paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
Evropským	evropský	k2eAgNnSc7d1	Evropské
dnem	dno	k1gNnSc7	dno
památky	památka	k1gFnSc2	památka
obětí	oběť	k1gFnPc2	oběť
stalinismu	stalinismus	k1gInSc2	stalinismus
a	a	k8xC	a
nacismu	nacismus	k1gInSc2	nacismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Soviet	Soviet	k1gInSc1	Soviet
invasion	invasion	k1gInSc1	invasion
of	of	k?	of
Poland	Polanda	k1gFnPc2	Polanda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DAVIES	DAVIES	kA	DAVIES
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
:	:	kIx,	:
dějiny	dějiny	k1gFnPc4	dějiny
jednoho	jeden	k4xCgInSc2	jeden
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
1365	[number]	k4	1365
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7260	[number]	k4	7260
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DAVIES	DAVIES	kA	DAVIES
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
orel	orel	k1gMnSc1	orel
<g/>
,	,	kIx,	,
rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
:	:	kIx,	:
polsko-sovětská	polskoovětský	k2eAgFnSc1d1	polsko-sovětská
válka	válka	k1gFnSc1	válka
1919-20	[number]	k4	1919-20
a	a	k8xC	a
"	"	kIx"	"
<g/>
zázrak	zázrak	k1gInSc1	zázrak
nad	nad	k7c7	nad
Vislou	Visla	k1gFnSc7	Visla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
311	[number]	k4	311
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7341	[number]	k4	7341
<g/>
-	-	kIx~	-
<g/>
939	[number]	k4	939
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAGNUSKI	MAGNUSKI	kA	MAGNUSKI
<g/>
,	,	kIx,	,
Janusz	Janusz	k1gMnSc1	Janusz
<g/>
;	;	kIx,	;
KOLOMIJEC	KOLOMIJEC	kA	KOLOMIJEC
<g/>
,	,	kIx,	,
Maksym	Maksym	k1gInSc1	Maksym
<g/>
.	.	kIx.	.
</s>
<s>
Rudý	rudý	k2eAgInSc1d1	rudý
blitzkrieg	blitzkrieg	k1gInSc1	blitzkrieg
<g/>
:	:	kIx,	:
Září	září	k1gNnSc1	září
1939	[number]	k4	1939
–	–	k?	–
Sovětská	sovětský	k2eAgNnPc4d1	sovětské
tanková	tankový	k2eAgNnPc4d1	tankové
vojska	vojsko	k1gNnPc4	vojsko
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Bonus	bonus	k1gInSc1	bonus
A	A	kA	A
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85914	[number]	k4	85914
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
SANFORD	SANFORD	kA	SANFORD
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
<g/>
.	.	kIx.	.
</s>
<s>
Katyn	Katyn	k1gInSc1	Katyn
and	and	k?	and
the	the	k?	the
Soviet	Soviet	k1gInSc1	Soviet
massacre	massacr	k1gInSc5	massacr
of	of	k?	of
1940	[number]	k4	1940
:	:	kIx,	:
truth	truth	k1gInSc1	truth
<g/>
,	,	kIx,	,
justice	justice	k1gFnSc1	justice
<g/>
,	,	kIx,	,
and	and	k?	and
memory	memora	k1gFnSc2	memora
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
250	[number]	k4	250
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
33873	[number]	k4	33873
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VEBER	VEBER	kA	VEBER
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Pakt	pakt	k1gInSc1	pakt
<g/>
.	.	kIx.	.
</s>
<s>
Sovětsko-nacistická	sovětskoacistický	k2eAgNnPc1d1	sovětsko-nacistický
jednání	jednání	k1gNnPc1	jednání
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
druhé	druhý	k4xOgFnPc4	druhý
světové	světový	k2eAgFnPc4d1	světová
války	válka	k1gFnPc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Paměť	paměť	k1gFnSc1	paměť
a	a	k8xC	a
dějiny	dějiny	k1gFnPc1	dějiny
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
8241	[number]	k4	8241
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WILLIAMSON	WILLIAMSON	kA	WILLIAMSON
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
G.	G.	kA	G.
Zrazené	zrazený	k2eAgNnSc1d1	zrazené
Polsko	Polsko	k1gNnSc1	Polsko
:	:	kIx,	:
nacistická	nacistický	k2eAgFnSc1d1	nacistická
a	a	k8xC	a
sovětská	sovětský	k2eAgFnSc1d1	sovětská
invaze	invaze	k1gFnSc1	invaze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
267	[number]	k4	267
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
740	[number]	k4	740
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Příčiny	příčina	k1gFnSc2	příčina
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
===	===	k?	===
Bibliografie	bibliografie	k1gFnSc2	bibliografie
===	===	k?	===
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
58046	[number]	k4	58046
<g/>
-	-	kIx~	-
<g/>
137	[number]	k4	137
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CARLEY	CARLEY	kA	CARLEY
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Jabara	Jabara	k1gFnSc1	Jabara
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
End	End	k?	End
of	of	k?	of
the	the	k?	the
'	'	kIx"	'
<g/>
Low	Low	k1gMnSc1	Low
<g/>
,	,	kIx,	,
Dishonest	Dishonest	k1gMnSc1	Dishonest
Decade	Decad	k1gInSc5	Decad
<g/>
'	'	kIx"	'
<g/>
:	:	kIx,	:
Failure	Failur	k1gMnSc5	Failur
of	of	k?	of
the	the	k?	the
Anglo	Anglo	k1gNnSc4	Anglo
<g/>
–	–	k?	–
<g/>
Franco	Franco	k1gMnSc1	Franco
<g/>
–	–	k?	–
<g/>
Soviet	Soviet	k1gMnSc1	Soviet
Alliance	Alliance	k1gFnSc2	Alliance
in	in	k?	in
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Europe-Asia	Europe-Asia	k1gFnSc1	Europe-Asia
Studies	Studiesa	k1gFnPc2	Studiesa
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
303	[number]	k4	303
<g/>
–	–	k?	–
<g/>
341	[number]	k4	341
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.108	[number]	k4	10.108
<g/>
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9668139308412091	[number]	k4	9668139308412091
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
10980	[number]	k4	10980
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7126	[number]	k4	7126
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
694	[number]	k4	694
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
820171	[number]	k4	820171
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DAVIES	DAVIES	kA	DAVIES
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
God	God	k?	God
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Playground	Playgrounda	k1gFnPc2	Playgrounda
<g/>
.	.	kIx.	.
revised	revised	k1gInSc1	revised
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
12819	[number]	k4	12819
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DEAN	DEAN	kA	DEAN
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Collaboration	Collaboration	k1gInSc1	Collaboration
in	in	k?	in
the	the	k?	the
Holocaust	holocaust	k1gInSc1	holocaust
<g/>
:	:	kIx,	:
Crimes	Crimes	k1gInSc1	Crimes
of	of	k?	of
the	the	k?	the
Local	Local	k1gMnSc1	Local
Police	police	k1gFnSc2	police
in	in	k?	in
Belorussia	Belorussia	k1gFnSc1	Belorussia
and	and	k?	and
Ukraine	Ukrain	k1gMnSc5	Ukrain
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
44	[number]	k4	44
<g/>
.	.	kIx.	.
</s>
<s>
Basingstoke	Basingstokat	k5eAaPmIp3nS	Basingstokat
<g/>
:	:	kIx,	:
Palgrave	Palgrav	k1gInSc5	Palgrav
Macmillan	Macmillany	k1gInPc2	Macmillany
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4039	[number]	k4	4039
<g/>
-	-	kIx~	-
<g/>
6371	[number]	k4	6371
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DEGRAS	DEGRAS	kA	DEGRAS
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
Tabrisky	Tabrisko	k1gNnPc7	Tabrisko
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Soviet	Soviet	k5eAaPmF	Soviet
Documents	Documents	k1gInSc4	Documents
on	on	k3xPp3gMnSc1	on
Foreign	Foreign	k1gMnSc1	Foreign
Policy	Polica	k1gFnSc2	Polica
<g/>
.	.	kIx.	.
</s>
<s>
Volume	volum	k1gInSc5	volum
I	i	k9	i
<g/>
:	:	kIx,	:
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DUNNIGAN	DUNNIGAN	kA	DUNNIGAN
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
F.	F.	kA	F.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gMnSc1	World
War	War	k1gMnSc2	War
II	II	kA	II
Bookshelf	Bookshelf	k1gInSc1	Bookshelf
<g/>
:	:	kIx,	:
Fifty	Fifta	k1gFnSc2	Fifta
Must-Read	Must-Read	k1gInSc1	Must-Read
Books	Books	k1gInSc1	Books
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Citadel	citadela	k1gFnPc2	citadela
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8065	[number]	k4	8065
<g/>
-	-	kIx~	-
<g/>
2609	[number]	k4	2609
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FERRO	Ferro	k1gNnSc1	Ferro
<g/>
,	,	kIx,	,
Marc	Marc	k1gInSc1	Marc
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Use	usus	k1gInSc5	usus
and	and	k?	and
Abuse	abusus	k1gInSc5	abusus
of	of	k?	of
History	Histor	k1gMnPc7	Histor
<g/>
:	:	kIx,	:
Or	Or	k1gMnPc7	Or
How	How	k1gMnSc7	How
the	the	k?	the
Past	past	k1gFnSc1	past
Is	Is	k1gMnSc1	Is
Taught	Taught	k1gMnSc1	Taught
to	ten	k3xDgNnSc4	ten
Children	Childrna	k1gFnPc2	Childrna
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
28592	[number]	k4	28592
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FRASER	FRASER	kA	FRASER
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Grant	grant	k1gInSc1	grant
<g/>
;	;	kIx,	;
DUNN	DUNN	kA	DUNN
<g/>
,	,	kIx,	,
Seamus	Seamus	k1gInSc1	Seamus
<g/>
;	;	kIx,	;
VON	von	k1gInSc1	von
HABSBURG	HABSBURG	kA	HABSBURG
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Europe	Europ	k1gMnSc5	Europ
and	and	k?	and
Ethnicity	Ethnicita	k1gFnPc1	Ethnicita
<g/>
:	:	kIx,	:
the	the	k?	the
First	First	k1gMnSc1	First
World	World	k1gMnSc1	World
War	War	k1gMnSc1	War
and	and	k?	and
contemporary	contemporara	k1gFnSc2	contemporara
ethnic	ethnice	k1gFnPc2	ethnice
conflict	conflict	k1gMnSc1	conflict
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
11995	[number]	k4	11995
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GOLDSTEIN	GOLDSTEIN	kA	GOLDSTEIN
<g/>
.	.	kIx.	.
</s>
<s>
Missing	Missing	k1gInSc1	Missing
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GELVEN	GELVEN	kA	GELVEN
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
War	War	k?	War
and	and	k?	and
Existence	existence	k1gFnSc1	existence
<g/>
:	:	kIx,	:
A	a	k9	a
Philosophical	Philosophical	k1gFnSc4	Philosophical
Inquiry	Inquira	k1gFnSc2	Inquira
<g/>
.	.	kIx.	.
</s>
<s>
Pennsylvania	Pennsylvanium	k1gNnPc1	Pennsylvanium
<g/>
:	:	kIx,	:
Penn	Penn	k1gInSc1	Penn
State	status	k1gInSc5	status
University	universita	k1gFnPc4	universita
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
271	[number]	k4	271
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1054	[number]	k4	1054
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GOLDMAN	GOLDMAN	kA	GOLDMAN
<g/>
,	,	kIx,	,
Stuart	Stuart	k1gInSc1	Stuart
D.	D.	kA	D.
Nomonhan	Nomonhan	k1gInSc1	Nomonhan
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
;	;	kIx,	;
The	The	k1gFnSc1	The
Red	Red	k1gMnSc2	Red
Army	Arma	k1gMnSc2	Arma
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Victory	Victor	k1gMnPc7	Victor
That	That	k1gMnSc1	That
Shaped	Shaped	k1gMnSc1	Shaped
World	World	k1gMnSc1	World
War	War	k1gMnSc1	War
II	II	kA	II
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Naval	navalit	k5eAaPmRp2nS	navalit
Institute	institut	k1gInSc5	institut
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
61251	[number]	k4	61251
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GRONOWICZ	GRONOWICZ	kA	GRONOWICZ
<g/>
,	,	kIx,	,
Antoni	Anton	k1gMnPc1	Anton
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Polish	Polish	k1gMnSc1	Polish
Profiles	Profiles	k1gMnSc1	Profiles
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Land	Land	k1gMnSc1	Land
<g/>
,	,	kIx,	,
the	the	k?	the
People	People	k1gMnSc5	People
<g/>
,	,	kIx,	,
and	and	k?	and
Their	Their	k1gInSc1	Their
History	Histor	k1gInPc1	Histor
<g/>
.	.	kIx.	.
</s>
<s>
Westport	Westport	k1gInSc1	Westport
<g/>
,	,	kIx,	,
CT	CT	kA	CT
<g/>
:	:	kIx,	:
L.	L.	kA	L.
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
88208	[number]	k4	88208
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GROSS	Gross	k1gMnSc1	Gross
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Tomasz	Tomasz	k1gMnSc1	Tomasz
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Revolution	Revolution	k1gInSc1	Revolution
from	from	k1gInSc1	from
Abroad	Abroad	k1gInSc1	Abroad
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Soviet	Soviet	k1gMnSc1	Soviet
Conquest	Conquest	k1gMnSc1	Conquest
of	of	k?	of
Poland	Poland	k1gInSc1	Poland
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Western	Western	kA	Western
Ukraine	Ukrain	k1gInSc5	Ukrain
and	and	k?	and
Western	western	k1gInSc4	western
Belorussia	Belorussius	k1gMnSc2	Belorussius
<g/>
.	.	kIx.	.
</s>
<s>
Princeton	Princeton	k1gInSc1	Princeton
<g/>
,	,	kIx,	,
NJ	NJ	kA	NJ
<g/>
:	:	kIx,	:
Princeton	Princeton	k1gInSc1	Princeton
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
691	[number]	k4	691
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9603	[number]	k4	9603
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HEHN	HEHN	kA	HEHN
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
N.	N.	kA	N.
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
low	low	k?	low
dishonest	dishonest	k1gFnSc1	dishonest
decade	decást	k5eAaPmIp3nS	decást
<g/>
:	:	kIx,	:
the	the	k?	the
great	great	k2eAgInSc4d1	great
powers	powers	k1gInSc4	powers
<g/>
,	,	kIx,	,
Eastern	Eastern	k1gInSc4	Eastern
Europe	Europ	k1gMnSc5	Europ
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
economic	economic	k1gMnSc1	economic
origins	origins	k6eAd1	origins
of	of	k?	of
World	World	k1gMnSc1	World
War	War	k1gFnSc3	War
II	II	kA	II
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Continuum	Continuum	k1gNnSc1	Continuum
International	International	k1gFnSc2	International
Publishing	Publishing	k1gInSc1	Publishing
Group	Group	k1gInSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8264	[number]	k4	8264
<g/>
-	-	kIx~	-
<g/>
1761	[number]	k4	1761
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HENDERSON	HENDERSON	kA	HENDERSON
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Documents	Documents	k6eAd1	Documents
concerning	concerning	k1gInSc1	concerning
German-Polish	German-Polisha	k1gFnPc2	German-Polisha
relations	relationsa	k1gFnPc2	relationsa
and	and	k?	and
the	the	k?	the
outbreak	outbreak	k6eAd1	outbreak
of	of	k?	of
hostilities	hostilities	k1gInSc4	hostilities
between	between	k2eAgInSc4d1	between
Great	Great	k2eAgInSc4d1	Great
Britain	Britain	k1gInSc4	Britain
and	and	k?	and
Germany	German	k1gInPc4	German
on	on	k3xPp3gMnSc1	on
September	Septembra	k1gFnPc2	Septembra
3	[number]	k4	3
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Great	Great	k2eAgInSc1d1	Great
Britain	Britain	k2eAgInSc1d1	Britain
Foreign	Foreign	k1gInSc1	Foreign
Office	Office	kA	Office
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HIDEN	HIDEN	kA	HIDEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
;	;	kIx,	;
LANE	LANE	kA	LANE
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Baltic	Baltice	k1gFnPc2	Baltice
and	and	k?	and
the	the	k?	the
Outbreak	Outbreak	k1gInSc1	Outbreak
of	of	k?	of
the	the	k?	the
Second	Second	k1gMnSc1	Second
World	World	k1gMnSc1	World
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
53120	[number]	k4	53120
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOUSE	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
<g/>
;	;	kIx,	;
SEYMOUR	SEYMOUR	kA	SEYMOUR
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
What	What	k1gMnSc1	What
Really	Realla	k1gFnSc2	Realla
Happened	Happened	k1gMnSc1	Happened
at	at	k?	at
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Scribner	Scribner	k1gInSc1	Scribner
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JACKSON	JACKSON	kA	JACKSON
<g/>
,	,	kIx,	,
Julian	Julian	k1gMnSc1	Julian
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Fall	Fall	k1gInSc1	Fall
of	of	k?	of
France	Franc	k1gMnSc2	Franc
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Nazi	Naz	k1gFnSc2	Naz
Invasion	Invasion	k1gInSc1	Invasion
of	of	k?	of
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
280300	[number]	k4	280300
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KENÉZ	KENÉZ	kA	KENÉZ
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Soviet	Soviet	k1gInSc1	Soviet
Union	union	k1gInSc1	union
from	from	k1gInSc1	from
the	the	k?	the
Beginning	Beginning	k1gInSc1	Beginning
to	ten	k3xDgNnSc1	ten
the	the	k?	the
End	End	k1gMnPc7	End
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
nd	nd	k?	nd
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
86437	[number]	k4	86437
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KITCHEN	KITCHEN	kA	KITCHEN
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
World	World	k1gMnSc1	World
in	in	k?	in
Flames	Flames	k1gMnSc1	Flames
<g/>
:	:	kIx,	:
A	a	k9	a
Short	Short	k1gInSc1	Short
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Second	Second	k1gMnSc1	Second
World	World	k1gMnSc1	World
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Longman	Longman	k1gMnSc1	Longman
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
582	[number]	k4	582
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3408	[number]	k4	3408
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KUBIK	kubika	k1gFnPc2	kubika
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Power	Power	k1gInSc1	Power
of	of	k?	of
Symbols	Symbols	k1gInSc1	Symbols
Against	Against	k1gInSc1	Against
the	the	k?	the
Symbols	Symbols	k1gInSc1	Symbols
of	of	k?	of
Power	Power	k1gInSc1	Power
<g/>
:	:	kIx,	:
the	the	k?	the
Rise	Rise	k1gInSc1	Rise
of	of	k?	of
Solidarity	solidarita	k1gFnSc2	solidarita
and	and	k?	and
the	the	k?	the
Fall	Fall	k1gInSc1	Fall
of	of	k?	of
State	status	k1gInSc5	status
<g/>
.	.	kIx.	.
</s>
<s>
Pennsylvania	Pennsylvanium	k1gNnPc1	Pennsylvanium
<g/>
:	:	kIx,	:
Penn	Penn	k1gInSc1	Penn
State	status	k1gInSc5	status
University	universita	k1gFnPc4	universita
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
271	[number]	k4	271
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1084	[number]	k4	1084
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KUSHNER	KUSHNER	kA	KUSHNER
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
<g/>
;	;	kIx,	;
KNOX	KNOX	kA	KNOX
<g/>
,	,	kIx,	,
KATHARINE	KATHARINE	kA	KATHARINE
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Refugees	Refugees	k1gInSc1	Refugees
in	in	k?	in
an	an	k?	an
Age	Age	k1gFnSc2	Age
of	of	k?	of
Genocide	Genocid	k1gMnSc5	Genocid
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7146	[number]	k4	7146
<g/>
-	-	kIx~	-
<g/>
4783	[number]	k4	4783
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KUTRZEBA	KUTRZEBA	kA	KUTRZEBA
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Cambridge	Cambridge	k1gFnSc1	Cambridge
history	histor	k1gInPc1	histor
of	of	k?	of
Poland	Poland	k1gInSc1	Poland
|	|	kIx~	|
<g/>
volume	volum	k1gInSc5	volum
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnPc1	redakce
Reddaway	Reddawa	k2eAgFnPc1d1	Reddawa
William	William	k1gInSc4	William
Fiddian	Fiddiana	k1gFnPc2	Fiddiana
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
The	The	k1gMnPc2	The
Struggle	Struggle	k1gNnSc2	Struggle
for	forum	k1gNnPc2	forum
the	the	k?	the
Frontiers	Frontiersa	k1gFnPc2	Frontiersa
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
s.	s.	k?	s.
512	[number]	k4	512
<g/>
–	–	k?	–
<g/>
543	[number]	k4	543
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LEVIN	LEVIN	kA	LEVIN
<g/>
,	,	kIx,	,
Dov	Dov	k1gFnSc1	Dov
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
lesser	lesser	k1gInSc1	lesser
of	of	k?	of
two	two	k?	two
evils	evils	k1gInSc1	evils
<g/>
:	:	kIx,	:
Eastern	Eastern	k1gMnSc1	Eastern
European	European	k1gMnSc1	European
Jewry	Jewra	k1gFnSc2	Jewra
under	under	k1gMnSc1	under
Soviet	Soviet	k1gMnSc1	Soviet
rule	rula	k1gFnSc6	rula
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Jewish	Jewish	k1gInSc1	Jewish
Publication	Publication	k1gInSc1	Publication
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8276	[number]	k4	8276
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
518	[number]	k4	518
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MANVELL	MANVELL	kA	MANVELL
<g/>
,	,	kIx,	,
Roger	Roger	k1gMnSc1	Roger
<g/>
;	;	kIx,	;
FRAENKEL	FRAENKEL	kA	FRAENKEL
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Sinister	Sinister	k1gMnSc1	Sinister
Life	Lif	k1gFnSc2	Lif
of	of	k?	of
the	the	k?	the
Head	Head	k1gInSc1	Head
of	of	k?	of
the	the	k?	the
SS	SS	kA	SS
and	and	k?	and
Gestapo	gestapo	k1gNnSc1	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Greenhill	Greenhill	k1gMnSc1	Greenhill
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
60239	[number]	k4	60239
<g/>
-	-	kIx~	-
<g/>
178	[number]	k4	178
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MENDELSOHN	MENDELSOHN	kA	MENDELSOHN
<g/>
,	,	kIx,	,
Ezra	Ezra	k1gFnSc1	Ezra
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Jews	Jews	k1gInSc1	Jews
and	and	k?	and
the	the	k?	the
Sporting	Sporting	k1gInSc1	Sporting
Life	Life	k1gFnSc1	Life
<g/>
:	:	kIx,	:
Studies	Studies	k1gInSc1	Studies
in	in	k?	in
Contemporary	Contemporara	k1gFnSc2	Contemporara
Jewry	Jewra	k1gFnSc2	Jewra
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
538291	[number]	k4	538291
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MINER	MINER	kA	MINER
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgMnSc1d1	Steven
Merritt	Merritt	k1gMnSc1	Merritt
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Holy	hola	k1gFnPc4	hola
War	War	k1gFnSc1	War
<g/>
:	:	kIx,	:
Religion	religion	k1gInSc1	religion
<g/>
,	,	kIx,	,
Nationalism	Nationalism	k1gInSc1	Nationalism
<g/>
,	,	kIx,	,
and	and	k?	and
Alliance	Alliance	k1gFnPc1	Alliance
Politics	Politicsa	k1gFnPc2	Politicsa
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
North	North	k1gInSc1	North
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
:	:	kIx,	:
UNC	UNC	kA	UNC
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8078	[number]	k4	8078
<g/>
-	-	kIx~	-
<g/>
2736	[number]	k4	2736
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MONTEFIORE	MONTEFIORE	kA	MONTEFIORE
<g/>
,	,	kIx,	,
Simon	Simon	k1gMnSc1	Simon
Sebag	Sebag	k1gMnSc1	Sebag
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Court	Courta	k1gFnPc2	Courta
of	of	k?	of
the	the	k?	the
Red	Red	k1gMnSc1	Red
Tsar	Tsar	k1gMnSc1	Tsar
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Vintage	Vintage	k1gNnSc1	Vintage
Books	Booksa	k1gFnPc2	Booksa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4000	[number]	k4	4000
<g/>
-	-	kIx~	-
<g/>
7678	[number]	k4	7678
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MOWAT	MOWAT	kA	MOWAT
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Loch	loch	k1gInSc1	loch
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Britain	Britain	k2eAgInSc1d1	Britain
between	between	k1gInSc1	between
the	the	k?	the
wars	wars	k1gInSc1	wars
<g/>
:	:	kIx,	:
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
416	[number]	k4	416
<g/>
-	-	kIx~	-
<g/>
29510	[number]	k4	29510
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MOYNIHAN	MOYNIHAN	kA	MOYNIHAN
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Patrick	Patrick	k1gMnSc1	Patrick
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Law	Law	k1gFnSc6	Law
of	of	k?	of
Nations	Nationsa	k1gFnPc2	Nationsa
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
,	,	kIx,	,
MA	MA	kA	MA
<g/>
:	:	kIx,	:
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
674	[number]	k4	674
<g/>
-	-	kIx~	-
<g/>
63575	[number]	k4	63575
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NEILSON	NEILSON	kA	NEILSON
<g/>
,	,	kIx,	,
Keith	Keith	k1gMnSc1	Keith
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Britain	Britain	k1gMnSc1	Britain
<g/>
,	,	kIx,	,
Soviet	Soviet	k1gMnSc1	Soviet
Russia	Russium	k1gNnSc2	Russium
and	and	k?	and
the	the	k?	the
Collapse	Collapse	k1gFnSc2	Collapse
of	of	k?	of
the	the	k?	the
Versailles	Versailles	k1gFnSc3	Versailles
Order	Order	k1gMnSc1	Order
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
85713	[number]	k4	85713
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOWAK	NOWAK	kA	NOWAK
<g/>
,	,	kIx,	,
Andrzej	Andrzej	k1gMnSc1	Andrzej
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Russo-Polish	Russo-Polish	k1gInSc1	Russo-Polish
Historical	Historical	k1gMnSc1	Historical
Confrontation	Confrontation	k1gInSc1	Confrontation
<g/>
.	.	kIx.	.
</s>
<s>
Sarmatian	Sarmatian	k1gMnSc1	Sarmatian
Review	Review	k1gMnSc1	Review
<g/>
.	.	kIx.	.
</s>
<s>
January	Januara	k1gFnPc1	Januara
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
16	[number]	k4	16
July	Jula	k1gFnSc2	Jula
2007	[number]	k4	2007
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ORLIK-RÜCKEMANN	ORLIK-RÜCKEMANN	k?	ORLIK-RÜCKEMANN
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Kampania	Kampanium	k1gNnPc1	Kampanium
wrześniowa	wrześniow	k2eAgNnPc1d1	wrześniow
na	na	k7c6	na
Polesiu	Polesium	k1gNnSc6	Polesium
i	i	k8xC	i
Wołyniu	Wołynium	k1gNnSc6	Wołynium
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
IX	IX	kA	IX
<g/>
.1939	.1939	k4	.1939
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
X	X	kA	X
<g/>
.1939	.1939	k4	.1939
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Jerzewski	Jerzewsk	k1gFnSc2	Jerzewsk
Leopold	Leopolda	k1gFnPc2	Leopolda
<g/>
.	.	kIx.	.
</s>
<s>
Warsaw	Warsaw	k?	Warsaw
<g/>
:	:	kIx,	:
Głos	Głos	k1gInSc1	Głos
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Polish	Polish	k1gInSc1	Polish
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PIOTROWSKI	PIOTROWSKI	kA	PIOTROWSKI
<g/>
,	,	kIx,	,
Tadeusz	Tadeusz	k1gMnSc1	Tadeusz
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Poland	Poland	k1gInSc1	Poland
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Holocaust	holocaust	k1gInSc1	holocaust
<g/>
:	:	kIx,	:
Ethnic	Ethnice	k1gFnPc2	Ethnice
Strife	Strif	k1gInSc5	Strif
<g/>
:	:	kIx,	:
Collaboration	Collaboration	k1gInSc1	Collaboration
with	witha	k1gFnPc2	witha
Occupying	Occupying	k1gInSc1	Occupying
Forces	Forces	k1gMnSc1	Forces
and	and	k?	and
Genocide	Genocid	k1gInSc5	Genocid
in	in	k?	in
the	the	k?	the
Second	Second	k1gInSc1	Second
Republic	Republice	k1gFnPc2	Republice
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Jefferson	Jefferson	k1gMnSc1	Jefferson
<g/>
,	,	kIx,	,
NC	NC	kA	NC
<g/>
:	:	kIx,	:
McFarland	McFarland	k1gInSc1	McFarland
&	&	k?	&
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7864	[number]	k4	7864
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
371	[number]	k4	371
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
OSMAŃCZYK	OSMAŃCZYK	kA	OSMAŃCZYK
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopedia	Encyclopedium	k1gNnPc1	Encyclopedium
of	of	k?	of
the	the	k?	the
United	United	k1gInSc1	United
Nations	Nations	k1gInSc1	Nations
and	and	k?	and
international	internationat	k5eAaImAgInS	internationat
agreements	agreements	k1gInSc1	agreements
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Mango	mango	k1gNnSc1	mango
Anthony	Anthona	k1gFnSc2	Anthona
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
rd	rd	k?	rd
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
93921	[number]	k4	93921
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
POLONSKY	POLONSKY	kA	POLONSKY
<g/>
,	,	kIx,	,
Antony	anton	k1gInPc1	anton
<g/>
;	;	kIx,	;
MICHLIC	MICHLIC	kA	MICHLIC
<g/>
,	,	kIx,	,
Joanna	Joanna	k1gFnSc1	Joanna
B.	B.	kA	B.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
neighbors	neighbors	k1gInSc1	neighbors
respond	respond	k1gInSc1	respond
<g/>
:	:	kIx,	:
the	the	k?	the
controversy	controversa	k1gFnSc2	controversa
over	over	k1gMnSc1	over
the	the	k?	the
Jedwabne	Jedwabne	k1gMnSc5	Jedwabne
Massacre	Massacr	k1gMnSc5	Massacr
in	in	k?	in
Poland	Poland	k1gInSc4	Poland
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Princeton	Princeton	k1gInSc1	Princeton
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
691	[number]	k4	691
<g/>
-	-	kIx~	-
<g/>
11306	[number]	k4	11306
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PRAZMOWSKA	PRAZMOWSKA	kA	PRAZMOWSKA
<g/>
,	,	kIx,	,
Anita	Anita	k1gFnSc1	Anita
J.	J.	kA	J.
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Britain	Britain	k2eAgInSc1d1	Britain
and	and	k?	and
Poland	Poland	k1gInSc1	Poland
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
:	:	kIx,	:
The	The	k1gFnPc1	The
Betrayed	Betrayed	k1gInSc4	Betrayed
Ally	Alla	k1gFnSc2	Alla
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
48385	[number]	k4	48385
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RIEBER	RIEBER	kA	RIEBER
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Forced	Forced	k1gInSc1	Forced
Migration	Migration	k1gInSc1	Migration
in	in	k?	in
Central	Central	k1gFnSc2	Central
and	and	k?	and
Eastern	Eastern	k1gMnSc1	Eastern
Europe	Europ	k1gInSc5	Europ
<g/>
:	:	kIx,	:
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7146	[number]	k4	7146
<g/>
-	-	kIx~	-
<g/>
5132	[number]	k4	5132
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ROBERTS	ROBERTS	kA	ROBERTS
<g/>
,	,	kIx,	,
Geoffrey	Geoffrea	k1gFnSc2	Geoffrea
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Soviet	Soviet	k1gInSc1	Soviet
Decision	Decision	k1gInSc1	Decision
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Pact	Pact	k2eAgMnSc1d1	Pact
with	with	k1gMnSc1	with
Nazi	Naz	k1gFnSc2	Naz
Germany	German	k1gInPc4	German
<g/>
.	.	kIx.	.
</s>
<s>
Soviet	Soviet	k1gMnSc1	Soviet
Studies	Studies	k1gMnSc1	Studies
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
57	[number]	k4	57
<g/>
–	–	k?	–
<g/>
78	[number]	k4	78
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.108	[number]	k4	10.108
<g/>
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9668139208411994	[number]	k4	9668139208411994
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ROSHWALD	ROSHWALD	kA	ROSHWALD
<g/>
,	,	kIx,	,
Aviel	Aviel	k1gMnSc1	Aviel
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Ethnic	Ethnice	k1gFnPc2	Ethnice
Nationalism	Nationalisma	k1gFnPc2	Nationalisma
and	and	k?	and
the	the	k?	the
Fall	Fall	k1gInSc1	Fall
of	of	k?	of
Empires	Empires	k1gInSc4	Empires
<g/>
:	:	kIx,	:
Central	Central	k1gMnSc5	Central
Europe	Europ	k1gMnSc5	Europ
<g/>
,	,	kIx,	,
the	the	k?	the
Middle	Middle	k1gMnSc1	Middle
East	East	k1gMnSc1	East
and	and	k?	and
Russia	Russia	k1gFnSc1	Russia
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
17893	[number]	k4	17893
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RUMMEL	RUMMEL	kA	RUMMEL
<g/>
,	,	kIx,	,
Rudolph	Rudolph	k1gMnSc1	Rudolph
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Lethal	Lethal	k1gMnSc1	Lethal
Politics	Politics	k1gInSc1	Politics
<g/>
:	:	kIx,	:
Soviet	Soviet	k1gInSc1	Soviet
Genocide	Genocid	k1gInSc5	Genocid
and	and	k?	and
Mass	Mass	k1gInSc4	Mass
Murder	Murdra	k1gFnPc2	Murdra
Since	Sinec	k1gInSc2	Sinec
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Jersey	Jersea	k1gFnPc1	Jersea
<g/>
:	:	kIx,	:
Transaction	Transaction	k1gInSc1	Transaction
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
56000	[number]	k4	56000
<g/>
-	-	kIx~	-
<g/>
887	[number]	k4	887
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RYZIŃSKI	RYZIŃSKI	kA	RYZIŃSKI
<g/>
,	,	kIx,	,
Kazimierz	Kazimierz	k1gMnSc1	Kazimierz
<g/>
;	;	kIx,	;
DALECKI	DALECKI	kA	DALECKI
<g/>
,	,	kIx,	,
Ryszard	Ryszard	k1gMnSc1	Ryszard
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Obrona	Obrona	k1gFnSc1	Obrona
Lwowa	Lwow	k1gInSc2	Lwow
w	w	k?	w
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Warszawa	Warszawa	k1gFnSc1	Warszawa
<g/>
:	:	kIx,	:
Instytut	Instytut	k1gInSc1	Instytut
Lwowski	Lwowsk	k1gFnSc2	Lwowsk
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3356	[number]	k4	3356
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Polish	Polish	k1gInSc1	Polish
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SANFORD	SANFORD	kA	SANFORD
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Katyn	Katyn	k1gInSc1	Katyn
and	and	k?	and
the	the	k?	the
Soviet	Soviet	k1gInSc1	Soviet
Massacre	Massacr	k1gInSc5	Massacr
Of	Of	k1gMnSc7	Of
1940	[number]	k4	1940
<g/>
:	:	kIx,	:
Truth	Truth	k1gInSc1	Truth
<g/>
,	,	kIx,	,
Justice	justice	k1gFnSc1	justice
And	Anda	k1gFnPc2	Anda
Memory	Memora	k1gFnSc2	Memora
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
33873	[number]	k4	33873
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SHAW	SHAW	kA	SHAW
<g/>
,	,	kIx,	,
Louise	Louis	k1gMnSc2	Louis
Grace	Grace	k1gMnSc2	Grace
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
British	British	k1gMnSc1	British
Political	Political	k1gMnSc1	Political
Elite	Elit	k1gInSc5	Elit
and	and	k?	and
the	the	k?	the
Soviet	Soviet	k1gInSc1	Soviet
Union	union	k1gInSc1	union
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7146	[number]	k4	7146
<g/>
-	-	kIx~	-
<g/>
5398	[number]	k4	5398
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SHIRER	SHIRER	kA	SHIRER
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Rise	Rise	k1gInSc1	Rise
and	and	k?	and
Fall	Fall	k1gInSc1	Fall
of	of	k?	of
the	the	k?	the
Third	Third	k1gInSc1	Third
Reich	Reich	k?	Reich
<g/>
:	:	kIx,	:
A	a	k8xC	a
History	Histor	k1gInPc1	Histor
of	of	k?	of
Nazi	Naz	k1gFnSc2	Naz
Germany	German	k1gInPc1	German
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Simon	Simon	k1gMnSc1	Simon
and	and	k?	and
Schuster	Schuster	k1gMnSc1	Schuster
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
671	[number]	k4	671
<g/>
-	-	kIx~	-
<g/>
72868	[number]	k4	72868
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SNYDER	SNYDER	kA	SNYDER
<g/>
,	,	kIx,	,
Timothy	Timotha	k1gFnSc2	Timotha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Confini	Confin	k1gMnPc1	Confin
<g/>
:	:	kIx,	:
Costruzioni	Costruzion	k1gMnPc1	Costruzion
<g/>
,	,	kIx,	,
Attraversamenti	Attraversament	k1gMnPc1	Attraversament
<g/>
,	,	kIx,	,
Rappresentazionicura	Rappresentazionicur	k1gMnSc4	Rappresentazionicur
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Salvatici	Salvatice	k1gFnSc4	Salvatice
Silvia	Silvia	k1gFnSc1	Silvia
<g/>
.	.	kIx.	.
</s>
<s>
Soveria	Soverium	k1gNnPc1	Soverium
Mannelli	Mannell	k1gMnPc1	Mannell
(	(	kIx(	(
<g/>
Catanzaro	Catanzara	k1gFnSc5	Catanzara
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Rubbettino	Rubbettin	k2eAgNnSc1d1	Rubbettin
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
498	[number]	k4	498
<g/>
-	-	kIx~	-
<g/>
1276	[number]	k4	1276
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Covert	Covert	k1gInSc1	Covert
Polish	Polish	k1gMnSc1	Polish
Missions	Missionsa	k1gFnPc2	Missionsa
Across	Acrossa	k1gFnPc2	Acrossa
the	the	k?	the
Soviet	Soviet	k1gMnSc1	Soviet
Ukrainian	Ukrainian	k1gMnSc1	Ukrainian
Border	Border	k1gMnSc1	Border
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
STACHURA	STACHURA	kA	STACHURA
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
D.	D.	kA	D.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Poland	Poland	k1gInSc1	Poland
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Interpretive	Interpretiv	k1gInSc5	Interpretiv
and	and	k?	and
Documentary	Documentara	k1gFnSc2	Documentara
History	Histor	k1gInPc4	Histor
of	of	k?	of
the	the	k?	the
Second	Second	k1gInSc1	Second
Republic	Republice	k1gFnPc2	Republice
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
34357	[number]	k4	34357
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
STANLEY	STANLEY	kA	STANLEY
<g/>
.	.	kIx.	.
</s>
<s>
Missing	Missing	k1gInSc1	Missing
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TAYLOR	TAYLOR	kA	TAYLOR
<g/>
,	,	kIx,	,
A.	A.	kA	A.
J.	J.	kA	J.
P.	P.	kA	P.
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Second	Second	k1gMnSc1	Second
World	World	k1gMnSc1	World
War	War	k1gMnSc1	War
<g/>
:	:	kIx,	:
An	An	k1gMnSc1	An
Illustrated	Illustrated	k1gInSc4	Illustrated
History	Histor	k1gInPc1	Histor
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Putnam	Putnam	k1gInSc1	Putnam
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
399	[number]	k4	399
<g/>
-	-	kIx~	-
<g/>
11412	[number]	k4	11412
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TOPOLEWSKI	TOPOLEWSKI	kA	TOPOLEWSKI
<g/>
,	,	kIx,	,
Stanisław	Stanisław	k1gFnSc1	Stanisław
<g/>
;	;	kIx,	;
POLAK	POLAK	kA	POLAK
<g/>
,	,	kIx,	,
Andrzej	Andrzej	k1gMnSc1	Andrzej
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
60	[number]	k4	60
<g/>
.	.	kIx.	.
rocznica	rocznic	k1gInSc2	rocznic
zakończenia	zakończenium	k1gNnSc2	zakończenium
II	II	kA	II
wojny	wojn	k1gInPc1	wojn
światowej	światowej	k1gInSc1	światowej
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Dom	Dom	k?	Dom
wydawniczy	wydawnicza	k1gFnSc2	wydawnicza
Wojska	Wojsek	k1gMnSc4	Wojsek
Polskiego	Polskiego	k1gMnSc1	Polskiego
(	(	kIx(	(
<g/>
Publishing	Publishing	k1gInSc1	Publishing
House	house	k1gNnSc1	house
of	of	k?	of
the	the	k?	the
Polish	Polish	k1gInSc4	Polish
Army	Arma	k1gFnSc2	Arma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Edukacja	Edukacj	k2eAgFnSc1d1	Edukacj
Humanistyczna	Humanistyczna	k1gFnSc1	Humanistyczna
w	w	k?	w
Wojsku	Wojsek	k1gInSc2	Wojsek
(	(	kIx(	(
<g/>
Humanist	Humanist	k1gInSc1	Humanist
Education	Education	k1gInSc1	Education
in	in	k?	in
the	the	k?	the
Army	Arma	k1gFnSc2	Arma
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
z	z	k7c2	z
originálu	originál	k1gInSc6	originál
dne	den	k1gInSc2	den
29	[number]	k4	29
September	September	k1gInSc1	September
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Polish	Polish	k1gInSc1	Polish
<g/>
)	)	kIx)	)
Archivováno	archivovat	k5eAaBmNgNnS	archivovat
29	[number]	k4	29
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
na	na	k7c4	na
Wayback	Wayback	k1gInSc4	Wayback
Machine	Machin	k1gInSc5	Machin
</s>
</p>
<p>
<s>
TRELA-MAZUR	TRELA-MAZUR	k?	TRELA-MAZUR
<g/>
,	,	kIx,	,
Elżbieta	Elżbieta	k1gFnSc1	Elżbieta
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Sowietyzacja	Sowietyzacja	k1gMnSc1	Sowietyzacja
oświaty	oświata	k1gFnSc2	oświata
w	w	k?	w
Małopolsce	Małopolska	k1gFnSc3	Małopolska
Wschodniej	Wschodniej	k1gInSc4	Wschodniej
pod	pod	k7c4	pod
radziecką	radziecką	k?	radziecką
okupacją	okupacją	k?	okupacją
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
(	(	kIx(	(
<g/>
Sovietization	Sovietization	k1gInSc1	Sovietization
of	of	k?	of
Education	Education	k1gInSc1	Education
in	in	k?	in
Eastern	Eastern	k1gInSc1	Eastern
Lesser	Lesser	k1gMnSc1	Lesser
Poland	Polanda	k1gFnPc2	Polanda
During	During	k1gInSc4	During
the	the	k?	the
Soviet	Soviet	k1gInSc1	Soviet
Occupation	Occupation	k1gInSc1	Occupation
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Bonusiak	Bonusiak	k1gMnSc1	Bonusiak
Włodzimierz	Włodzimierz	k1gMnSc1	Włodzimierz
<g/>
.	.	kIx.	.
</s>
<s>
Kielce	Kielec	k1gInPc1	Kielec
<g/>
:	:	kIx,	:
Wyższa	Wyższa	k1gFnSc1	Wyższa
Szkoła	Szkoła	k1gFnSc1	Szkoła
Pedagogiczna	Pedagogiczna	k1gFnSc1	Pedagogiczna
im	im	k?	im
<g/>
.	.	kIx.	.
</s>
<s>
Jana	Jan	k1gMnSc4	Jan
Kochanowskiego	Kochanowskiego	k1gMnSc1	Kochanowskiego
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7133	[number]	k4	7133
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Polish	Polish	k1gInSc1	Polish
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TUCKER	TUCKER	kA	TUCKER
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
C.	C.	kA	C.
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
in	in	k?	in
Power	Power	k1gMnSc1	Power
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Revolution	Revolution	k1gInSc4	Revolution
from	from	k1gInSc1	from
Above	Aboev	k1gFnSc2	Aboev
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Norton	Norton	k1gInSc1	Norton
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
393	[number]	k4	393
<g/>
-	-	kIx~	-
<g/>
30869	[number]	k4	30869
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WATSON	WATSON	kA	WATSON
<g/>
,	,	kIx,	,
Derek	Derek	k1gMnSc1	Derek
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Molotov	Molotov	k1gInSc1	Molotov
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Apprenticeship	Apprenticeship	k1gInSc1	Apprenticeship
in	in	k?	in
Foreign	Foreign	k1gInSc1	Foreign
Policy	Polica	k1gMnSc2	Polica
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Triple	tripl	k1gInSc5	tripl
Alliance	Alliance	k1gFnPc1	Alliance
Negotiations	Negotiations	k1gInSc1	Negotiations
in	in	k?	in
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Europe-Asia	Europe-Asia	k1gFnSc1	Europe-Asia
Studies	Studiesa	k1gFnPc2	Studiesa
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
695	[number]	k4	695
<g/>
–	–	k?	–
<g/>
722	[number]	k4	722
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.108	[number]	k4	10.108
<g/>
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
713663077	[number]	k4	713663077
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WEINBERG	WEINBERG	kA	WEINBERG
<g/>
,	,	kIx,	,
Gerhard	Gerhard	k1gMnSc1	Gerhard
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
World	World	k1gMnSc1	World
at	at	k?	at
Arms	Arms	k1gInSc1	Arms
<g/>
:	:	kIx,	:
A	a	k8xC	a
Global	globat	k5eAaImAgMnS	globat
History	Histor	k1gMnPc4	Histor
of	of	k?	of
World	Worldo	k1gNnPc2	Worldo
War	War	k1gMnPc2	War
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
44317	[number]	k4	44317
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WILSON	WILSON	kA	WILSON
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Ukrainian	Ukrainian	k1gMnSc1	Ukrainian
Nationalism	Nationalism	k1gMnSc1	Nationalism
in	in	k?	in
the	the	k?	the
1990	[number]	k4	1990
<g/>
s	s	k7c7	s
<g/>
:	:	kIx,	:
A	a	k9	a
Minority	minorita	k1gFnPc1	minorita
Faith	Faitha	k1gFnPc2	Faitha
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
57457	[number]	k4	57457
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WETTIG	WETTIG	kA	WETTIG
<g/>
,	,	kIx,	,
Gerhard	Gerhard	k1gMnSc1	Gerhard
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
and	and	k?	and
the	the	k?	the
Cold	Cold	k1gMnSc1	Cold
War	War	k1gMnSc1	War
in	in	k?	in
Europe	Europ	k1gInSc5	Europ
<g/>
:	:	kIx,	:
the	the	k?	the
emergence	emergence	k1gFnSc2	emergence
and	and	k?	and
development	development	k1gMnSc1	development
of	of	k?	of
East	East	k1gMnSc1	East
<g/>
–	–	k?	–
<g/>
West	West	k1gMnSc1	West
conflict	conflict	k1gMnSc1	conflict
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Lanham	Lanham	k1gInSc1	Lanham
<g/>
:	:	kIx,	:
Rowman	Rowman	k1gMnSc1	Rowman
&	&	k?	&
Littlefield	Littlefield	k1gMnSc1	Littlefield
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7425	[number]	k4	7425
<g/>
-	-	kIx~	-
<g/>
5542	[number]	k4	5542
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ZALOGA	ZALOGA	kA	ZALOGA
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgMnSc1d1	Steven
J.	J.	kA	J.
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Poland	Poland	k1gInSc1	Poland
1939	[number]	k4	1939
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Birth	Birth	k1gMnSc1	Birth
of	of	k?	of
Blitzkrieg	Blitzkrieg	k1gMnSc1	Blitzkrieg
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Osprey	Osprea	k1gFnPc1	Osprea
Publishing	Publishing	k1gInSc4	Publishing
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
84176	[number]	k4	84176
<g/>
-	-	kIx~	-
<g/>
408	[number]	k4	408
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
KŘÍŽ	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Pinská	Pinský	k2eAgFnSc1d1	Pinský
vojenská	vojenský	k2eAgFnSc1d1	vojenská
flotila	flotila	k1gFnSc1	flotila
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2012-02-16	[number]	k4	2012-02-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
