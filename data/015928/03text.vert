<s>
Vnitřní	vnitřní	k2eAgNnSc1d1
Město	město	k1gNnSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1
Město	město	k1gNnSc1
Náměstí	náměstí	k1gNnSc6
RepublikyLokalita	RepublikyLokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
část	část	k1gFnSc1
města	město	k1gNnSc2
Obec	obec	k1gFnSc1
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
Okres	okres	k1gInSc1
</s>
<s>
Plzeň-město	Plzeň-město	k6eAd1
Kraj	kraj	k1gInSc1
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
45	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
772	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
PSČ	PSČ	kA
</s>
<s>
301	#num#	k4
00301	#num#	k4
37	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
193	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
406392	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
část	část	k1gFnSc4
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Plzeň	Plzeň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
historická	historický	k2eAgFnSc1d1
část	část	k1gFnSc1
města	město	k1gNnSc2
v	v	k7c6
rozsahu	rozsah	k1gInSc6
vymezeném	vymezený	k2eAgInSc6d1
někdejšími	někdejší	k2eAgFnPc7d1
městskými	městský	k2eAgFnPc7d1
hradbami	hradba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Plzeň	Plzeň	k1gFnSc1
a	a	k8xC
městského	městský	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Plzeň	Plzeň	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
evidováno	evidovat	k5eAaImNgNnS
399	#num#	k4
adres	adresa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
zde	zde	k6eAd1
trvale	trvale	k6eAd1
žilo	žít	k5eAaImAgNnS
1	#num#	k4
863	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adresy	adresa	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-10-10	2009-10-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
ČR	ČR	kA
1869	#num#	k4
-	-	kIx~
2005	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-03-03	2007-03-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
302	#num#	k4
<g/>
,	,	kIx,
303	#num#	k4
<g/>
,	,	kIx,
záznam	záznam	k1gInSc1
3	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Plzni-Vnitřním	Plzni-Vnitřní	k2eAgNnSc6d1
Městě	město	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
katastru	katastr	k1gInSc2
Plzeň	Plzeň	k1gFnSc1
na	na	k7c6
webu	web	k1gInSc6
ČÚZK	ČÚZK	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Bolevec	Bolevec	k1gInSc1
•	•	k?
</s>
<s>
Severní	severní	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
•	•	k?
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
2	#num#	k4
<g/>
-Slovany	-Slovan	k1gMnPc7
<g/>
:	:	kIx,
</s>
<s>
Božkov	Božkov	k1gInSc1
•	•	k?
</s>
<s>
Černice	černice	k1gFnSc1
•	•	k?
</s>
<s>
Doudlevce	Doudlevce	k1gMnSc1
•	•	k?
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
•	•	k?
</s>
<s>
Koterov	Koterov	k1gInSc1
•	•	k?
</s>
<s>
Lobzy	Lobza	k1gFnPc1
•	•	k?
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
•	•	k?
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
3	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Doudlevce	Doudlevce	k1gMnSc1
•	•	k?
</s>
<s>
Jižní	jižní	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
•	•	k?
</s>
<s>
Litice	Litice	k1gFnSc1
•	•	k?
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Hospoda	Hospoda	k?
•	•	k?
</s>
<s>
Radobyčice	Radobyčice	k1gFnSc1
•	•	k?
</s>
<s>
Skvrňany	Skvrňan	k1gMnPc4
•	•	k?
</s>
<s>
Valcha	valcha	k1gFnSc1
•	•	k?
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1
Město	město	k1gNnSc1
•	•	k?
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
•	•	k?
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
4	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Bukovec	Bukovec	k1gInSc1
•	•	k?
</s>
<s>
Červený	červený	k2eAgInSc1d1
Hrádek	hrádek	k1gInSc1
•	•	k?
</s>
<s>
Doubravka	Doubravka	k1gFnSc1
•	•	k?
</s>
<s>
Lobzy	Lobza	k1gFnPc1
•	•	k?
</s>
<s>
Újezd	Újezd	k1gInSc1
•	•	k?
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
•	•	k?
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
5	#num#	k4
<g/>
-Křimice	-Křimika	k1gFnSc6
•	•	k?
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
6	#num#	k4
<g/>
-Litice	-Litice	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Litice	Litice	k1gFnSc1
•	•	k?
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
7	#num#	k4
<g/>
-Radčice	-Radčice	k1gFnSc1
•	•	k?
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
8	#num#	k4
<g/>
-Černice	-Černice	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Černice	černice	k1gFnSc1
•	•	k?
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
9	#num#	k4
<g/>
-Malesice	-Malesice	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Dolní	dolní	k2eAgInSc1d1
Vlkýš	Vlkýš	k1gInSc1
•	•	k?
</s>
<s>
Malesice	Malesice	k1gFnSc1
•	•	k?
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
10	#num#	k4
<g/>
-Lhota	-Lhota	k1gFnSc1
</s>
