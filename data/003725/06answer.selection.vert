<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
zvaný	zvaný	k2eAgInSc1d1	zvaný
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
zasedá	zasedat	k5eAaImIp3nS	zasedat
150	[number]	k4	150
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
