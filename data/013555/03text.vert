<s>
Balaton	Balaton	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Balaton	Balaton	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Balaton	Balaton	k1gInSc1
Balaton	Balaton	k1gInSc1
z	z	k7c2
poloostrova	poloostrov	k1gInSc2
TihanyPoloha	TihanyPoloh	k1gMnSc2
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
Župy	župa	k1gFnSc2
</s>
<s>
Somogy	Somoga	k1gFnPc1
<g/>
,	,	kIx,
Veszprém	Veszprý	k2eAgNnSc6d1
<g/>
,	,	kIx,
Zala	Zalum	k1gNnPc1
</s>
<s>
Balaton	Balaton	k1gInSc1
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
11	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozměry	rozměra	k1gFnSc2
Rozloha	rozloha	k1gFnSc1
</s>
<s>
596	#num#	k4
km²	km²	k?
Délka	délka	k1gFnSc1
</s>
<s>
78	#num#	k4
km	km	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
12	#num#	k4
km	km	kA
Max	max	kA
<g/>
.	.	kIx.
hloubka	hloubka	k1gFnSc1
</s>
<s>
11	#num#	k4
m	m	kA
Ostatní	ostatní	k2eAgInSc1d1
Typ	typ	k1gInSc1
</s>
<s>
tektonicko-deflační	tektonicko-deflační	k2eAgInSc1d1
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
105	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Pobřeží	pobřeží	k1gNnSc2
</s>
<s>
197	#num#	k4
km	km	kA
Přítok	přítok	k1gInSc4
vody	voda	k1gFnSc2
</s>
<s>
Zala	Zala	k6eAd1
Odtok	odtok	k1gInSc1
vody	voda	k1gFnSc2
</s>
<s>
vodní	vodní	k2eAgInSc1d1
kanál	kanál	k1gInSc1
Sió	Sió	k1gFnSc2
Sídla	sídlo	k1gNnSc2
</s>
<s>
Keszthely	Keszthela	k1gFnPc1
<g/>
,	,	kIx,
Siófok	Siófok	k1gInSc1
<g/>
,	,	kIx,
Balatonfüred	Balatonfüred	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Balaton	Balaton	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
též	též	k9
Blatenské	blatenský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jezero	jezero	k1gNnSc1
v	v	k7c6
župách	župa	k1gFnPc6
Somogy	Somoga	k1gFnSc2
<g/>
,	,	kIx,
Veszprém	Veszprý	k2eAgMnSc6d1
a	a	k8xC
Zala	Zala	k1gFnSc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
v	v	k7c6
Dunajské	dunajský	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
největší	veliký	k2eAgNnSc1d3
jezero	jezero	k1gNnSc1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
právě	právě	k6eAd1
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
velikost	velikost	k1gFnSc4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c6
maďarštině	maďarština	k1gFnSc6
říká	říkat	k5eAaImIp3nS
také	také	k9
Magyar	Magyar	k1gMnSc1
tenger	tenger	k1gMnSc1
–	–	k?
Maďarské	maďarský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
podle	podle	k7c2
typu	typ	k1gInSc2
mezi	mezi	k7c4
takzvaná	takzvaný	k2eAgNnPc4d1
stepní	stepní	k2eAgNnPc4d1
jezera	jezero	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Eurasii	Eurasie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
jezera	jezero	k1gNnSc2
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
slovanského	slovanský	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
blato	blato	k1gNnSc4
–	–	k?
bažina	bažina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
vzniku	vznik	k1gInSc2
</s>
<s>
Základ	základ	k1gInSc1
geologického	geologický	k2eAgNnSc2d1
podloží	podloží	k1gNnSc2
Balatonu	Balaton	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
mezozoické	mezozoický	k2eAgInPc1d1
sedimenty	sediment	k1gInPc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
samotné	samotný	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
není	být	k5eNaImIp3nS
starší	starý	k2eAgNnSc4d2
15	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
větrnou	větrný	k2eAgFnSc7d1
erozí	eroze	k1gFnSc7
a	a	k8xC
tektonickými	tektonický	k2eAgInPc7d1
posuvy	posuv	k1gInPc7
tvořit	tvořit	k5eAaImF
malá	malý	k2eAgNnPc4d1
jezírka	jezírko	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
propojovala	propojovat	k5eAaImAgFnS
a	a	k8xC
přibližně	přibližně	k6eAd1
před	před	k7c7
10	#num#	k4
000	#num#	k4
lety	léto	k1gNnPc7
mělo	mít	k5eAaImAgNnS
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
o	o	k7c4
třetinu	třetina	k1gFnSc4
větší	veliký	k2eAgFnSc4d2
rozlohu	rozloha	k1gFnSc4
než	než	k8xS
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
intenzivně	intenzivně	k6eAd1
zanášen	zanášet	k5eAaImNgInS
materiálem	materiál	k1gInSc7
neseným	nesený	k2eAgInSc7d1
větrem	vítr	k1gInSc7
a	a	k8xC
vodními	vodní	k2eAgInPc7d1
přítoky	přítok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
dnes	dnes	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
sedimenty	sediment	k1gInPc1
na	na	k7c6
dně	dno	k1gNnSc6
6	#num#	k4
–	–	k?
10	#num#	k4
metrů	metr	k1gInPc2
mocnou	mocný	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
pokud	pokud	k8xS
by	by	kYmCp3nS
nebyl	být	k5eNaImAgInS
každoročně	každoročně	k6eAd1
bagrován	bagrovat	k5eAaImNgInS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
za	za	k7c4
několik	několik	k4yIc4
málo	málo	k4c4
tisíc	tisíc	k4xCgInSc4
let	léto	k1gNnPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
zanesl	zanést	k5eAaPmAgMnS
úplně	úplně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
kotlině	kotlina	k1gFnSc6
tektonického	tektonický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
podél	podél	k7c2
jihovýchodního	jihovýchodní	k2eAgNnSc2d1
úpatí	úpatí	k1gNnSc2
pohoří	pohořet	k5eAaPmIp3nS
Bakoňský	bakoňský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
596	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
délku	délka	k1gFnSc4
78	#num#	k4
km	km	kA
a	a	k8xC
maximální	maximální	k2eAgFnSc4d1
šířku	šířka	k1gFnSc4
12	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
105	#num#	k4
m.	m.	k?
Průměrná	průměrný	k2eAgFnSc1d1
hloubka	hloubka	k1gFnSc1
je	být	k5eAaImIp3nS
3	#num#	k4
m	m	kA
a	a	k8xC
maxima	maximum	k1gNnSc2
dosahuje	dosahovat	k5eAaImIp3nS
11	#num#	k4
m.	m.	k?
Jezero	jezero	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
větrnou	větrný	k2eAgFnSc7d1
erozí	eroze	k1gFnSc7
a	a	k8xC
následnou	následný	k2eAgFnSc7d1
deflací	deflace	k1gFnSc7
(	(	kIx(
<g/>
větrným	větrný	k2eAgInSc7d1
odnosem	odnos	k1gInSc7
materiálu	materiál	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Panorama	panorama	k1gNnSc1
přes	přes	k7c4
Balaton	Balaton	k1gInSc4
a	a	k8xC
Keszthely	Keszthel	k1gInPc4
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Povodí	povodí	k1gNnSc1
Balatonu	Balaton	k1gInSc2
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
5180	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
množství	množství	k1gNnSc1
vody	voda	k1gFnSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
1,8	1,8	k4
x	x	k?
109	#num#	k4
m	m	kA
<g/>
3	#num#	k4
a	a	k8xC
obmění	obměnit	k5eAaPmIp3nS
se	se	k3xPyFc4
přibližně	přibližně	k6eAd1
za	za	k7c4
2,2	2,2	k4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výška	výška	k1gFnSc1
hladiny	hladina	k1gFnSc2
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1976	#num#	k4
při	při	k7c6
stavbě	stavba	k1gFnSc6
siófokského	siófokský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
upravena	upravit	k5eAaPmNgFnS
a	a	k8xC
ustálena	ustálit	k5eAaPmNgFnS
na	na	k7c4
přibližně	přibližně	k6eAd1
104	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Balaton	Balaton	k1gInSc1
je	být	k5eAaImIp3nS
známý	známý	k2eAgMnSc1d1
svou	svůj	k3xOyFgFnSc7
klidnou	klidný	k2eAgFnSc7d1
hladinou	hladina	k1gFnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
při	při	k7c6
bouřkách	bouřka	k1gFnPc6
či	či	k8xC
dlouhotrvajícím	dlouhotrvající	k2eAgInSc6d1
rychlém	rychlý	k2eAgInSc6d1
větru	vítr	k1gInSc6
se	se	k3xPyFc4
vlny	vlna	k1gFnPc1
objeví	objevit	k5eAaPmIp3nP
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
a	a	k8xC
do	do	k7c2
ohromných	ohromný	k2eAgFnPc2d1
velikostí	velikost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgMnSc1d3
jsou	být	k5eAaImIp3nP
<g/>
,	,	kIx,
pokud	pokud	k8xS
vítr	vítr	k1gInSc1
fouká	foukat	k5eAaImIp3nS
z	z	k7c2
jihozápadního	jihozápadní	k2eAgInSc2d1
směru	směr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
dosud	dosud	k6eAd1
naměřené	naměřený	k2eAgFnPc4d1
měly	mít	k5eAaImAgFnP
výšku	výška	k1gFnSc4
1,82	1,82	k4
m	m	kA
u	u	k7c2
břehu	břeh	k1gInSc2
a	a	k8xC
1,95	1,95	k4
m	m	kA
uprostřed	uprostřed	k7c2
jezera	jezero	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
délka	délka	k1gFnSc1
jedné	jeden	k4xCgFnSc2
se	se	k3xPyFc4
pohybovala	pohybovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
mezi	mezi	k7c7
2	#num#	k4
–	–	k?
12	#num#	k4
m.	m.	k?
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
dvou	dva	k4xCgFnPc2
hodin	hodina	k1gFnPc2
po	po	k7c4
uklidnění	uklidnění	k1gNnSc4
větru	vítr	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
hladina	hladina	k1gFnSc1
navrátila	navrátit	k5eAaPmAgFnS
do	do	k7c2
původního	původní	k2eAgInSc2d1
klidu	klid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slapové	slapový	k2eAgInPc1d1
jevy	jev	k1gInPc1
jsou	být	k5eAaImIp3nP
patrné	patrný	k2eAgInPc1d1
pouze	pouze	k6eAd1
v	v	k7c6
západovýchodním	západovýchodní	k2eAgInSc6d1
směru	směr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc2d3
naměřené	naměřený	k2eAgFnSc2d1
se	se	k3xPyFc4
vyskytly	vyskytnout	k5eAaPmAgFnP
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
roku	rok	k1gInSc2
1962	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c6
západě	západ	k1gInSc6
klesla	klesnout	k5eAaPmAgFnS
hladina	hladina	k1gFnSc1
o	o	k7c4
45	#num#	k4
cm	cm	kA
a	a	k8xC
na	na	k7c6
východě	východ	k1gInSc6
stoupla	stoupnout	k5eAaPmAgFnS
o	o	k7c4
celých	celý	k2eAgInPc2d1
52	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proudění	proudění	k1gNnPc4
probíhá	probíhat	k5eAaImIp3nS
u	u	k7c2
hladiny	hladina	k1gFnSc2
jezera	jezero	k1gNnSc2
od	od	k7c2
severního	severní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
k	k	k7c3
jižnímu	jižní	k2eAgInSc3d1
a	a	k8xC
u	u	k7c2
dna	dno	k1gNnSc2
pak	pak	k6eAd1
ve	v	k7c6
směru	směr	k1gInSc6
opačném	opačný	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místě	místo	k1gNnSc6
zúženiny	zúženina	k1gFnSc2
u	u	k7c2
Tihanyského	Tihanyský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
dosahuje	dosahovat	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
těchto	tento	k3xDgInPc2
proudů	proud	k1gInPc2
běžně	běžně	k6eAd1
i	i	k8xC
2	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
</s>
<s>
BalatonPo	BalatonPo	k1gNnSc1
Balatonu	Balaton	k1gInSc2
ústí	ústit	k5eAaImIp3nS
mnoho	mnoho	k4c1
krátkých	krátký	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
přítokem	přítok	k1gInSc7
je	být	k5eAaImIp3nS
Zala	Zala	k1gFnSc1
přitékající	přitékající	k2eAgFnSc1d1
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
z	z	k7c2
Kis-Balatonu	Kis-Balaton	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtok	odtok	k1gInSc1
je	být	k5eAaImIp3nS
zajištěn	zajistit	k5eAaPmNgInS
přes	přes	k7c4
umělý	umělý	k2eAgInSc4d1
kanál	kanál	k1gInSc4
Sió	Sió	k1gFnPc2
do	do	k7c2
řeky	řeka	k1gFnSc2
téhož	týž	k3xTgNnSc2
jména	jméno	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
spojená	spojený	k2eAgFnSc1d1
s	s	k7c7
Dunajem	Dunaj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Mikroklima	mikroklima	k1gNnSc1
Balatonu	Balaton	k1gInSc2
</s>
<s>
Mikroklima	mikroklima	k1gNnSc1
balatonského	balatonský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
se	se	k3xPyFc4
díky	díky	k7c3
severojižnímu	severojižní	k2eAgNnSc3d1
natočení	natočení	k1gNnSc3
<g/>
,	,	kIx,
převládajícím	převládající	k2eAgMnPc3d1
jihozápadním	jihozápadní	k2eAgMnPc3d1
větrům	vítr	k1gInPc3
<g/>
,	,	kIx,
kontinentálnímu	kontinentální	k2eAgNnSc3d1
okolí	okolí	k1gNnSc3
a	a	k8xC
vodní	vodní	k2eAgFnSc3d1
hladině	hladina	k1gFnSc3
odrážející	odrážející	k2eAgNnSc1d1
sluneční	sluneční	k2eAgNnSc1d1
záření	záření	k1gNnSc1
podobá	podobat	k5eAaImIp3nS
mediterránnímu	mediterránní	k2eAgNnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roční	roční	k2eAgFnSc1d1
množství	množství	k1gNnSc4
slunných	slunný	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
dvou	dva	k4xCgInPc2
tisíc	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nejteplejšími	teplý	k2eAgInPc7d3
měsíci	měsíc	k1gInPc7
jsou	být	k5eAaImIp3nP
červen	červen	k1gInSc4
<g/>
,	,	kIx,
červenec	červenec	k1gInSc4
a	a	k8xC
srpen	srpen	k1gInSc4
<g/>
,	,	kIx,
nejslunnějším	slunný	k2eAgInSc7d3
měsícem	měsíc	k1gInSc7
je	být	k5eAaImIp3nS
červen	červen	k1gInSc1
a	a	k8xC
nejméně	málo	k6eAd3
srážek	srážka	k1gFnPc2
dopadá	dopadat	k5eAaImIp3nS
v	v	k7c6
srpnu	srpen	k1gInSc6
a	a	k8xC
září	září	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
je	být	k5eAaImIp3nS
díky	díky	k7c3
vodní	vodní	k2eAgFnSc3d1
mase	masa	k1gFnSc3
stahována	stahován	k2eAgFnSc1d1
teplotní	teplotní	k2eAgFnSc1d1
amplituda	amplituda	k1gFnSc1
jak	jak	k8xS,k8xC
denní	denní	k2eAgNnSc1d1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
i	i	k9
roční	roční	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
léta	léto	k1gNnSc2
se	se	k3xPyFc4
však	však	k9
teplota	teplota	k1gFnSc1
vody	voda	k1gFnSc2
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
28	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
měří	měřit	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
hloubce	hloubka	k1gFnSc6
1	#num#	k4
m	m	kA
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Siófok	Siófok	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodu	výhod	k1gInSc2
severního	severní	k2eAgNnSc2d1
hornatého	hornatý	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
na	na	k7c4
pěstování	pěstování	k1gNnSc4
vinné	vinný	k2eAgFnSc2d1
révy	réva	k1gFnSc2
objevili	objevit	k5eAaPmAgMnP
už	už	k6eAd1
Římané	Říman	k1gMnPc1
a	a	k8xC
tato	tento	k3xDgFnSc1
tradice	tradice	k1gFnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
zachovala	zachovat	k5eAaPmAgFnS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocházejí	pocházet	k5eAaImIp3nP
odsud	odsud	k6eAd1
ta	ten	k3xDgFnSc1
nejsladší	sladký	k2eAgFnSc1d3
a	a	k8xC
nejkvalitnější	kvalitní	k2eAgFnSc1d3
maďarská	maďarský	k2eAgFnSc1d1
vína	vína	k1gFnSc1
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
bílé	bílý	k2eAgFnPc1d1
odrůdy	odrůda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
těch	ten	k3xDgFnPc2
nejlahodnějších	lahodný	k2eAgFnPc2d3
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
Badacsonyi	Badacsony	k1gFnSc3
Szürkebarát	Szürkebarát	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
endemitním	endemitní	k2eAgMnSc7d1
druhem	druh	k1gMnSc7
<g/>
“	“	k?
a	a	k8xC
nesmí	smět	k5eNaImIp3nS
se	se	k3xPyFc4
pěstovat	pěstovat	k5eAaImF
nikde	nikde	k6eAd1
jinde	jinde	k6eAd1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
resp.	resp.	kA
pouze	pouze	k6eAd1
pro	pro	k7c4
osobní	osobní	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
na	na	k7c6
svazích	svah	k1gInPc6
stolové	stolový	k2eAgFnSc2d1
vyhaslé	vyhaslý	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
Badacsony	Badacsona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
zim	zima	k1gFnPc2
Balaton	Balaton	k1gInSc4
zamrzá	zamrzat	k5eAaImIp3nS
<g/>
,	,	kIx,
vrstva	vrstva	k1gFnSc1
ledu	led	k1gInSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
20	#num#	k4
–	–	k?
30	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větry	vítr	k1gInPc4
nad	nad	k7c7
Balatonem	Balaton	k1gInSc7
povětšinou	povětšinou	k6eAd1
vanou	vanout	k5eAaImIp3nP
z	z	k7c2
jihu	jih	k1gInSc2
<g/>
,	,	kIx,
západu	západ	k1gInSc2
<g/>
,	,	kIx,
severozápadu	severozápad	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
severu	sever	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jihozápadu	jihozápad	k1gInSc2
přicházejí	přicházet	k5eAaImIp3nP
pouze	pouze	k6eAd1
ne	ne	k9
příliš	příliš	k6eAd1
časté	častý	k2eAgFnPc1d1
bouřky	bouřka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsilnější	silný	k2eAgInSc1d3
vítr	vítr	k1gInSc1
byl	být	k5eAaImAgInS
zaznamenán	zaznamenat	k5eAaPmNgInS
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1961	#num#	k4
a	a	k8xC
to	ten	k3xDgNnSc4
129,6	129,6	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
pod	pod	k7c7
pohořím	pohoří	k1gNnSc7
Bakony	Bakona	k1gFnSc2
je	být	k5eAaImIp3nS
prudké	prudký	k2eAgFnSc3d1
a	a	k8xC
vysoké	vysoký	k2eAgFnSc3d1
<g/>
,	,	kIx,
místy	místy	k6eAd1
členité	členitý	k2eAgInPc1d1
(	(	kIx(
<g/>
poloostrov	poloostrov	k1gInSc1
Tihany	Tihana	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
je	on	k3xPp3gInPc4
porostlé	porostlý	k2eAgInPc4d1
lesy	les	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severním	severní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
vinice	vinice	k1gFnPc1
(	(	kIx(
<g/>
Badacsony	Badacsona	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgInSc4d1
pobřeží	pobřeží	k1gNnSc1
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
nízké	nízký	k2eAgNnSc1d1
a	a	k8xC
často	často	k6eAd1
bažinaté	bažinatý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
pobřeží	pobřeží	k1gNnSc2
je	být	k5eAaImIp3nS
asi	asi	k9
200	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Satelitní	satelitní	k2eAgInSc1d1
snímek	snímek	k1gInSc1
Balatonu	Balaton	k1gInSc2
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
jezera	jezero	k1gNnSc2
je	být	k5eAaImIp3nS
nejdůležitější	důležitý	k2eAgFnSc7d3
lázeňskou	lázeňský	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
a	a	k8xC
po	po	k7c6
Budapešti	Budapešť	k1gFnSc6
druhým	druhý	k4xOgInSc7
nejoblíbenějším	oblíbený	k2eAgInSc7d3
cílem	cíl	k1gInSc7
turistů	turist	k1gMnPc2
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
minerální	minerální	k2eAgInPc1d1
a	a	k8xC
termální	termální	k2eAgInPc1d1
prameny	pramen	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jezeře	jezero	k1gNnSc6
je	být	k5eAaImIp3nS
rozvinuto	rozvinut	k2eAgNnSc1d1
rybářství	rybářství	k1gNnSc1
a	a	k8xC
místní	místní	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podél	podél	k7c2
břehů	břeh	k1gInPc2
vedou	vést	k5eAaImIp3nP
silnice	silnice	k1gFnPc1
a	a	k8xC
železnice	železnice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Koupání	koupání	k1gNnSc1
</s>
<s>
Jelikož	jelikož	k8xS
má	mít	k5eAaImIp3nS
Balaton	Balaton	k1gInSc4
poměrně	poměrně	k6eAd1
mělké	mělký	k2eAgNnSc4d1
dno	dno	k1gNnSc4
a	a	k8xC
voda	voda	k1gFnSc1
není	být	k5eNaImIp3nS
hluboká	hluboký	k2eAgFnSc1d1
<g/>
,	,	kIx,
prohřívá	prohřívat	k5eAaImIp3nS
se	se	k3xPyFc4
rovnoměrně	rovnoměrně	k6eAd1
až	až	k9
do	do	k7c2
dna	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letní	letní	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
vody	voda	k1gFnSc2
je	být	k5eAaImIp3nS
20	#num#	k4
–	–	k?
27	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Zvláště	zvláště	k6eAd1
jižní	jižní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
jezera	jezero	k1gNnSc2
je	být	k5eAaImIp3nS
výhodná	výhodný	k2eAgFnSc1d1
pro	pro	k7c4
koupání	koupání	k1gNnSc4
rodiny	rodina	k1gFnSc2
s	s	k7c7
dětmi	dítě	k1gFnPc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
až	až	k9
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
300	#num#	k4
–	–	k?
500	#num#	k4
metrů	metr	k1gInPc2
od	od	k7c2
břehu	břeh	k1gInSc2
dosahuje	dosahovat	k5eAaImIp3nS
hloubka	hloubka	k1gFnSc1
vody	voda	k1gFnSc2
2	#num#	k4
m.	m.	k?
Na	na	k7c6
severním	severní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
hranice	hranice	k1gFnSc1
místy	místy	k6eAd1
již	již	k6eAd1
na	na	k7c4
10	#num#	k4
–	–	k?
20	#num#	k4
metrech	metr	k1gInPc6
od	od	k7c2
břehu	břeh	k1gInSc6
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
celé	celý	k2eAgFnSc2d1
zarostlé	zarostlý	k2eAgFnSc2d1
rákosem	rákos	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInPc4d1
sporty	sport	k1gInPc4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
jezera	jezero	k1gNnSc2
je	být	k5eAaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
jachting	jachting	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
klub	klub	k1gInSc1
jachtařů	jachtař	k1gMnPc2
tu	tu	k6eAd1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
v	v	k7c6
Balatonfüredu	Balatonfüred	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Téměř	téměř	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
částech	část	k1gFnPc6
jezera	jezero	k1gNnSc2
je	být	k5eAaImIp3nS
povolen	povolit	k5eAaPmNgInS
sportovní	sportovní	k2eAgInSc1d1
rybolov	rybolov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
lovené	lovený	k2eAgFnPc4d1
ryby	ryba	k1gFnPc4
jsou	být	k5eAaImIp3nP
candáti	candát	k1gMnPc1
<g/>
,	,	kIx,
sumci	sumec	k1gMnPc1
<g/>
,	,	kIx,
štiky	štika	k1gFnPc1
<g/>
,	,	kIx,
boleni	bolen	k2eAgMnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
kapři	kapr	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Plavba	plavba	k1gFnSc1
po	po	k7c6
jezeře	jezero	k1gNnSc6
</s>
<s>
Je	být	k5eAaImIp3nS
prokázáno	prokázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
již	jenž	k3xRgMnPc1
Římané	Říman	k1gMnPc1
využívali	využívat	k5eAaImAgMnP,k5eAaPmAgMnP
jezero	jezero	k1gNnSc4
k	k	k7c3
vodní	vodní	k2eAgFnSc3d1
dopravě	doprava	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
parník	parník	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c4
jezero	jezero	k1gNnSc4
spuštěn	spustit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1846	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
byla	být	k5eAaImAgFnS
lodní	lodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
obnovena	obnovit	k5eAaPmNgFnS
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rekreačních	rekreační	k2eAgNnPc6d1
místech	místo	k1gNnPc6
jako	jako	k8xC,k8xS
Siófok	Siófok	k1gInSc1
byla	být	k5eAaImAgNnP
vybudována	vybudován	k2eAgNnPc1d1
přístaviště	přístaviště	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
</s>
<s>
Balaton	Balaton	k1gInSc1
</s>
<s>
Severní	severní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
od	od	k7c2
východu	východ	k1gInSc2
na	na	k7c4
západ	západ	k1gInSc4
<g/>
:	:	kIx,
Balatonkenese	Balatonkenese	k1gFnSc1
–	–	k?
Balatonfűzfő	Balatonfűzfő	k1gFnPc2
–	–	k?
Balatonalmádi	Balatonalmád	k1gMnPc1
–	–	k?
Alsóörs	Alsóörsa	k1gFnPc2
–	–	k?
Paloznak	Paloznak	k1gMnSc1
–	–	k?
Csopak	Csopak	k1gMnSc1
–	–	k?
Balatonfüred	Balatonfüred	k1gMnSc1
–	–	k?
Tihany	Tihana	k1gFnSc2
–	–	k?
Aszófő	Aszófő	k1gMnSc1
–	–	k?
Örvényes	Örvényes	k1gMnSc1
–	–	k?
Balatonudvari	Balatonudvar	k1gFnSc2
–	–	k?
Fövenyes	Fövenyes	k1gMnSc1
–	–	k?
Balatonakali	Balatonakali	k1gMnSc1
–	–	k?
Zánka	Zánka	k1gMnSc1
–	–	k?
Balatonszepezd	Balatonszepezd	k1gMnSc1
–	–	k?
Révfülöp	Révfülöp	k1gMnSc1
–	–	k?
Pálköve	Pálköev	k1gFnSc2
–	–	k?
Balatonrendes	Balatonrendes	k1gMnSc1
–	–	k?
Ábrahámhegy	Ábrahámhega	k1gFnSc2
–	–	k?
Badacsonytomaj	Badacsonytomaj	k1gMnSc1
–	–	k?
Badacsonytördemic	Badacsonytördemic	k1gMnSc1
–	–	k?
Szigliget	Szigliget	k1gMnSc1
–	–	k?
Balatonederics	Balatonederics	k1gInSc1
–	–	k?
Balatongyörök	Balatongyörök	k1gInSc1
–	–	k?
Vonyarcvashegy	Vonyarcvashega	k1gFnSc2
–	–	k?
Gyenesdiás	Gyenesdiás	k1gInSc1
–	–	k?
Keszthely	Keszthela	k1gFnSc2
</s>
<s>
Jižní	jižní	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
od	od	k7c2
východu	východ	k1gInSc2
na	na	k7c4
západ	západ	k1gInSc4
<g/>
:	:	kIx,
Balatonakarattya	Balatonakaratty	k2eAgFnSc1d1
–	–	k?
Balatonaliga	Balatonaliga	k1gFnSc1
–	–	k?
Balatonvilágos	Balatonvilágos	k1gMnSc1
–	–	k?
Sóstó	Sóstó	k1gMnSc1
–	–	k?
Szabadifürdo	Szabadifürdo	k1gNnSc4
–	–	k?
Siófok	Siófok	k1gInSc1
–	–	k?
Széplak	Széplak	k1gInSc1
–	–	k?
Zamárdi	Zamárd	k1gMnPc1
–	–	k?
Szántód	Szántód	k1gMnSc1
–	–	k?
Balatonföldvár	Balatonföldvár	k1gMnSc1
–	–	k?
Balatonszárszó	Balatonszárszó	k1gMnSc1
–	–	k?
Balatonszemes	Balatonszemes	k1gMnSc1
–	–	k?
Balatonlelle	Balatonlelle	k1gFnSc2
–	–	k?
Balatonboglár	Balatonboglár	k1gMnSc1
–	–	k?
Fonyód	Fonyód	k1gMnSc1
–	–	k?
Bélatelep	Bélatelep	k1gMnSc1
–	–	k?
Balatonfenyves	Balatonfenyves	k1gMnSc1
–	–	k?
Balatonmáriafürdő	Balatonmáriafürdő	k1gMnSc1
–	–	k?
Balatonkeresztúr	Balatonkeresztúr	k1gMnSc1
–	–	k?
Balatonberény	Balatonberéna	k1gFnSc2
–	–	k?
Fenékpuszta	Fenékpuszta	k1gMnSc1
(	(	kIx(
<g/>
Balatonszentgyörgy	Balatonszentgyörg	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Flóra	Flóra	k1gFnSc1
a	a	k8xC
fauna	fauna	k1gFnSc1
</s>
<s>
Jezero	jezero	k1gNnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
okolí	okolí	k1gNnSc4
hostí	hostit	k5eAaImIp3nP
mnoho	mnoho	k4c4
vzácných	vzácný	k2eAgInPc2d1
i	i	k8xC
chráněných	chráněný	k2eAgInPc2d1
převážně	převážně	k6eAd1
teplomilných	teplomilný	k2eAgInPc2d1
rostlinných	rostlinný	k2eAgInPc2d1
a	a	k8xC
živočišných	živočišný	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Flóra	Flóra	k1gFnSc1
</s>
<s>
Na	na	k7c6
jižních	jižní	k2eAgFnPc6d1
slunci	slunce	k1gNnSc3
vystavených	vystavený	k2eAgInPc6d1
svazích	svah	k1gInPc6
severního	severní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
rostou	růst	k5eAaImIp3nP
mandloně	mandloň	k1gFnPc1
<g/>
,	,	kIx,
roubované	roubovaný	k2eAgInPc1d1
fíkovníky	fíkovník	k1gInPc1
a	a	k8xC
pěstují	pěstovat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
také	také	k9
granátovníky	granátovník	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
strmých	strmý	k2eAgInPc6d1
svazích	svah	k1gInPc6
tabulové	tabulový	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Badacsony	Badacsona	k1gFnSc2
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
vinná	vinný	k2eAgFnSc1d1
réva	réva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
V	v	k7c6
jezeře	jezero	k1gNnSc6
žije	žít	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
padesát	padesát	k4xCc4
druhů	druh	k1gInPc2
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
králem	král	k1gMnSc7
zdejších	zdejší	k2eAgFnPc2d1
vod	voda	k1gFnPc2
je	být	k5eAaImIp3nS
nazýván	nazýván	k2eAgMnSc1d1
candát	candát	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
něj	on	k3xPp3gInSc2
převažují	převažovat	k5eAaImIp3nP
úhoři	úhoř	k1gMnPc1
<g/>
,	,	kIx,
amuři	amur	k1gMnPc1
<g/>
,	,	kIx,
cejni	cejn	k1gMnPc1
<g/>
,	,	kIx,
cejnci	cejnek	k1gMnPc1
<g/>
,	,	kIx,
štiky	štika	k1gFnPc1
<g/>
,	,	kIx,
karasi	karas	k1gMnPc1
<g/>
,	,	kIx,
kapři	kapr	k1gMnPc1
<g/>
,	,	kIx,
líni	lín	k1gMnPc1
a	a	k8xC
sumci	sumec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
250	#num#	k4
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
je	být	k5eAaImIp3nS
27	#num#	k4
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
např.	např.	kA
kalous	kalous	k1gMnSc1
ušatý	ušatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
čáp	čáp	k1gMnSc1
černý	černý	k1gMnSc1
<g/>
,	,	kIx,
datel	datel	k1gMnSc1
černý	černý	k1gMnSc1
<g/>
,	,	kIx,
kormorán	kormorán	k1gMnSc1
velký	velký	k2eAgMnSc1d1
a	a	k8xC
různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
volavek	volavka	k1gFnPc2
a	a	k8xC
kolpíků	kolpík	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Balaton	Balaton	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Balaton	Balaton	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Blatenské	blatenský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Balaton-tourism	Balaton-tourism	k1gInSc1
<g/>
.	.	kIx.
<g/>
hu	hu	k0
</s>
<s>
Balaton	Balaton	k1gInSc1
<g/>
.	.	kIx.
<g/>
hu	hu	k0
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Б	Б	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
160051	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4046313-8	4046313-8	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
46145602553601362381	#num#	k4
</s>
