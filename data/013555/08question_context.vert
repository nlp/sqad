<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
největší	veliký	k2eAgNnSc1d3
jezero	jezero	k1gNnSc1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
právě	právě	k6eAd1
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
velikost	velikost	k1gFnSc4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c6
maďarštině	maďarština	k1gFnSc6
říká	říkat	k5eAaImIp3nS
také	také	k9
Magyar	Magyar	k1gMnSc1
tenger	tenger	k1gMnSc1
–	–	k?
Maďarské	maďarský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
.	.	kIx.
</s>