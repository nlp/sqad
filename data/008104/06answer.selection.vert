<s>
Martina	Martina	k1gFnSc1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1987	[number]	k4	1987
Nové	Nová	k1gFnSc2	Nová
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
specializující	specializující	k2eAgFnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
tratě	trať	k1gFnPc4	trať
<g/>
,	,	kIx,	,
trojnásobná	trojnásobný	k2eAgFnSc1d1	trojnásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
a	a	k8xC	a
několikanásobná	několikanásobný	k2eAgFnSc1d1	několikanásobná
mistryně	mistryně	k1gFnSc1	mistryně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
držitelka	držitelka	k1gFnSc1	držitelka
několika	několik	k4yIc2	několik
juniorských	juniorský	k2eAgMnPc2d1	juniorský
i	i	k8xC	i
seniorských	seniorský	k2eAgMnPc2d1	seniorský
světových	světový	k2eAgMnPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
