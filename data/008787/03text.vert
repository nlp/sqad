<p>
<s>
Kvadrant	kvadrant	k1gInSc1	kvadrant
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
quadrans	quadrans	k1gInSc1	quadrans
=	=	kIx~	=
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
<g/>
;	;	kIx,	;
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
významu	význam	k1gInSc6	význam
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvadrant	kvadrant	k1gInSc1	kvadrant
může	moct	k5eAaImIp3nS	moct
býtː	býtː	k?	býtː
</s>
</p>
<p>
<s>
Kvadrant	kvadrant	k1gInSc1	kvadrant
(	(	kIx(	(
<g/>
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
)	)	kIx)	)
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
částí	část	k1gFnPc2	část
roviny	rovina	k1gFnSc2	rovina
rozdělené	rozdělený	k2eAgFnSc2d1	rozdělená
dvěma	dva	k4xCgFnPc7	dva
navzájem	navzájem	k6eAd1	navzájem
kolmými	kolmý	k2eAgFnPc7d1	kolmá
přímkami	přímka	k1gFnPc7	přímka
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
částí	část	k1gFnPc2	část
prostoru	prostor	k1gInSc2	prostor
rozděleného	rozdělený	k2eAgInSc2d1	rozdělený
dvěma	dva	k4xCgFnPc7	dva
navzájem	navzájem	k6eAd1	navzájem
kolmými	kolmý	k2eAgFnPc7d1	kolmá
rovinami	rovina	k1gFnPc7	rovina
<g/>
,	,	kIx,	,
též	též	k9	též
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
kruhu	kruh	k1gInSc2	kruh
nebo	nebo	k8xC	nebo
obvodu	obvod	k1gInSc2	obvod
kružnice	kružnice	k1gFnSc2	kružnice
</s>
</p>
<p>
<s>
Kvadrant	kvadrant	k1gInSc1	kvadrant
(	(	kIx(	(
<g/>
geografie	geografie	k1gFnSc1	geografie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
zemský	zemský	k2eAgInSc1d1	zemský
kvadrant	kvadrant	k1gInSc1	kvadrant
-	-	kIx~	-
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
zemského	zemský	k2eAgInSc2d1	zemský
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
část	část	k1gFnSc1	část
mezi	mezi	k7c7	mezi
pólem	pól	k1gInSc7	pól
a	a	k8xC	a
rovníkem	rovník	k1gInSc7	rovník
</s>
</p>
<p>
<s>
Kvadrant	kvadrant	k1gInSc1	kvadrant
(	(	kIx(	(
<g/>
přístroj	přístroj	k1gInSc1	přístroj
<g/>
)	)	kIx)	)
-	-	kIx~	-
přístroje	přístroj	k1gInPc1	přístroj
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c4	v
astronomii	astronomie	k1gFnSc4	astronomie
<g/>
,	,	kIx,	,
námořní	námořní	k2eAgFnSc4d1	námořní
navigaci	navigace	k1gFnSc4	navigace
a	a	k8xC	a
vojenství	vojenství	k1gNnSc4	vojenství
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
zenitových	zenitový	k2eAgFnPc2d1	zenitová
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
hvězd	hvězda	k1gFnPc2	hvězda
nebo	nebo	k8xC	nebo
měření	měření	k1gNnSc2	měření
úhlů	úhel	k1gInPc2	úhel
</s>
</p>
<p>
<s>
Kvadrant	kvadrant	k1gInSc1	kvadrant
(	(	kIx(	(
<g/>
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
)	)	kIx)	)
-	-	kIx~	-
již	již	k6eAd1	již
zrušené	zrušený	k2eAgNnSc1d1	zrušené
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc1d1	zvané
též	též	k9	též
zední	zednět	k5eAaPmIp3nS	zednět
kvadrant	kvadrant	k1gInSc1	kvadrant
</s>
<s>
Vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
osa	osa	k1gFnSc1	osa
se	se	k3xPyFc4	se
v	v	k7c6	v
kartézských	kartézský	k2eAgFnPc6d1	kartézská
souřadnicích	souřadnice	k1gFnPc6	souřadnice
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
osa	osa	k1gFnSc1	osa
x	x	k?	x
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
svislá	svislý	k2eAgFnSc1d1	svislá
osa	osa	k1gFnSc1	osa
jako	jako	k8xC	jako
osa	osa	k1gFnSc1	osa
y	y	k?	y
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
</p>
