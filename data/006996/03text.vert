<s>
Spolek	spolek	k1gInSc1	spolek
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
Mánes	Mánes	k1gMnSc1	Mánes
(	(	kIx(	(
<g/>
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
diskutovat	diskutovat	k5eAaImF	diskutovat
a	a	k8xC	a
přednášet	přednášet	k5eAaImF	přednášet
o	o	k7c4	o
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
českém	český	k2eAgInSc6d1	český
tak	tak	k6eAd1	tak
evropském	evropský	k2eAgInSc6d1	evropský
<g/>
,	,	kIx,	,
vydávat	vydávat	k5eAaPmF	vydávat
časopisy	časopis	k1gInPc1	časopis
Volné	volný	k2eAgInPc1d1	volný
směry	směr	k1gInPc1	směr
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Styl	styl	k1gInSc1	styl
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgFnSc2d1	umělecká
publikace	publikace	k1gFnSc2	publikace
a	a	k8xC	a
pořádat	pořádat	k5eAaImF	pořádat
výstavy	výstava	k1gFnPc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
byl	být	k5eAaImAgInS	být
spolek	spolek	k1gInSc1	spolek
mladých	mladý	k2eAgMnPc2d1	mladý
českých	český	k2eAgMnPc2d1	český
výtvarníků	výtvarník	k1gMnPc2	výtvarník
studujících	studující	k2eAgMnPc2d1	studující
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
Škréta	Škrét	k1gInSc2	Škrét
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
malíře	malíř	k1gMnPc4	malíř
byly	být	k5eAaImAgFnP	být
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
poměry	poměr	k1gInPc4	poměr
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Akademii	akademie	k1gFnSc6	akademie
příliš	příliš	k6eAd1	příliš
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
proto	proto	k8xC	proto
odcházeli	odcházet	k5eAaImAgMnP	odcházet
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
Mnichovského	mnichovský	k2eAgInSc2d1	mnichovský
spolku	spolek	k1gInSc2	spolek
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Hudeček	Hudeček	k1gMnSc1	Hudeček
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Marold	Marold	k1gMnSc1	Marold
<g/>
,	,	kIx,	,
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Slavíček	Slavíček	k1gMnSc1	Slavíček
<g/>
,	,	kIx,	,
Joža	Joža	k1gMnSc1	Joža
Uprka	Uprka	k1gMnSc1	Uprka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
změně	změna	k1gFnSc6	změna
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
Akademii	akademie	k1gFnSc6	akademie
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
zakladateli	zakladatel	k1gMnPc7	zakladatel
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
.	.	kIx.	.
</s>
<s>
Spolek	spolek	k1gInSc1	spolek
se	se	k3xPyFc4	se
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
po	po	k7c6	po
malíři	malíř	k1gMnSc6	malíř
Josefu	Josef	k1gMnSc6	Josef
Mánesovi	Mánes	k1gMnSc6	Mánes
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
redaktorem	redaktor	k1gMnSc7	redaktor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Luděk	Luděk	k1gMnSc1	Luděk
Marold	Marold	k1gMnSc1	Marold
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
výstavou	výstava	k1gFnSc7	výstava
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
spolek	spolek	k1gInSc1	spolek
pořádal	pořádat	k5eAaImAgInS	pořádat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
souborná	souborný	k2eAgFnSc1d1	souborná
výstava	výstava	k1gFnSc1	výstava
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
Alše	Aleš	k1gMnSc2	Aleš
v	v	k7c6	v
Topičově	topičův	k2eAgInSc6d1	topičův
salonu	salon	k1gInSc6	salon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
členská	členský	k2eAgFnSc1d1	členská
výstava	výstava	k1gFnSc1	výstava
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
uvedená	uvedený	k2eAgFnSc1d1	uvedená
plakáty	plakát	k1gInPc4	plakát
Arnošta	Arnošt	k1gMnSc4	Arnošt
Hofbauera	Hofbauer	k1gMnSc4	Hofbauer
<g/>
.	.	kIx.	.
</s>
<s>
Otevření	otevření	k1gNnSc1	otevření
kulturního	kulturní	k2eAgInSc2d1	kulturní
obzoru	obzor	k1gInSc2	obzor
<g/>
,	,	kIx,	,
cesty	cesta	k1gFnSc2	cesta
umělců	umělec	k1gMnPc2	umělec
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
a	a	k8xC	a
výstavy	výstava	k1gFnPc1	výstava
přinesly	přinést	k5eAaPmAgFnP	přinést
podněty	podnět	k1gInPc4	podnět
českému	český	k2eAgNnSc3d1	české
umění	umění	k1gNnSc3	umění
–	–	k?	–
jako	jako	k8xS	jako
například	například	k6eAd1	například
secesi	secese	k1gFnSc3	secese
<g/>
,	,	kIx,	,
impresionismus	impresionismus	k1gInSc1	impresionismus
<g/>
,	,	kIx,	,
kubismus	kubismus	k1gInSc1	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
pořádal	pořádat	k5eAaImAgMnS	pořádat
Mánes	Mánes	k1gMnSc1	Mánes
výstavu	výstav	k1gInSc2	výstav
soch	socha	k1gFnPc2	socha
Augusta	Augusta	k1gMnSc1	Augusta
Rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
výstavu	výstava	k1gFnSc4	výstava
ruského	ruský	k2eAgNnSc2d1	ruské
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
výstavu	výstava	k1gFnSc4	výstava
maleb	malba	k1gFnPc2	malba
Edvarda	Edvard	k1gMnSc2	Edvard
Muncha	Munch	k1gMnSc2	Munch
<g/>
,	,	kIx,	,
francouzských	francouzský	k2eAgMnPc2d1	francouzský
impresionistů	impresionista	k1gMnPc2	impresionista
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
výstav	výstava	k1gFnPc2	výstava
německého	německý	k2eAgMnSc2d1	německý
<g/>
,	,	kIx,	,
polského	polský	k2eAgMnSc2d1	polský
<g/>
,	,	kIx,	,
chorvatského	chorvatský	k2eAgInSc2d1	chorvatský
<g/>
,	,	kIx,	,
dánského	dánský	k2eAgMnSc2d1	dánský
<g/>
,	,	kIx,	,
anglického	anglický	k2eAgNnSc2d1	anglické
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Významní	významný	k2eAgMnPc1d1	významný
umělci	umělec	k1gMnPc1	umělec
a	a	k8xC	a
architekti	architekt	k1gMnPc1	architekt
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
zahraničními	zahraniční	k2eAgInPc7d1	zahraniční
členy	člen	k1gInPc7	člen
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
Muncha	Muncha	k1gFnSc1	Muncha
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Henri	Henre	k1gFnSc4	Henre
Matisse	Matisse	k1gFnSc1	Matisse
<g/>
,	,	kIx,	,
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
Marc	Marc	k1gFnSc1	Marc
Chagall	Chagall	k1gMnSc1	Chagall
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Lloyd	Lloyd	k1gMnSc1	Lloyd
Wright	Wright	k1gMnSc1	Wright
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
Gropius	Gropius	k1gMnSc1	Gropius
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
vydával	vydávat	k5eAaPmAgMnS	vydávat
edici	edice	k1gFnSc3	edice
Zlatoroh	Zlatoroh	k1gMnSc1	Zlatoroh
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
ilustrovaných	ilustrovaný	k2eAgFnPc2d1	ilustrovaná
monografií	monografie	k1gFnPc2	monografie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
Max	Max	k1gMnSc1	Max
Švabinský	Švabinský	k2eAgMnSc1d1	Švabinský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
byl	být	k5eAaImAgMnS	být
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
rozpuštěn	rozpustit	k5eAaPmNgInS	rozpustit
<g/>
,	,	kIx,	,
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
funkcionáři	funkcionář	k1gMnPc1	funkcionář
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
dosadili	dosadit	k5eAaPmAgMnP	dosadit
Český	český	k2eAgInSc4d1	český
fond	fond	k1gInSc4	fond
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
(	(	kIx(	(
<g/>
ČFVU	ČFVU	kA	ČFVU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
sice	sice	k8xC	sice
už	už	k6eAd1	už
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
určoval	určovat	k5eAaImAgInS	určovat
dění	dění	k1gNnSc4	dění
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
satelitech	satelit	k1gInPc6	satelit
<g/>
,	,	kIx,	,
probíhala	probíhat	k5eAaImAgFnS	probíhat
krátká	krátký	k2eAgFnSc1d1	krátká
doba	doba	k1gFnSc1	doba
"	"	kIx"	"
<g/>
tání	tání	k1gNnSc1	tání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
do	do	k7c2	do
socialistického	socialistický	k2eAgNnSc2d1	socialistické
Československa	Československo	k1gNnSc2	Československo
dorazila	dorazit	k5eAaPmAgFnS	dorazit
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
až	až	k9	až
počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
činnost	činnost	k1gFnSc1	činnost
spolku	spolek	k1gInSc2	spolek
S.	S.	kA	S.
<g/>
V.U.	V.U.	k1gMnSc1	V.U.
Mánes	Mánes	k1gMnSc1	Mánes
obnovena	obnovit	k5eAaPmNgNnP	obnovit
22	[number]	k4	22
tehdy	tehdy	k6eAd1	tehdy
žijícími	žijící	k2eAgMnPc7d1	žijící
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
architekt	architekt	k1gMnSc1	architekt
Jiří	Jiří	k1gMnSc1	Jiří
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
autora	autor	k1gMnSc2	autor
projektu	projekt	k1gInSc2	projekt
budovy	budova	k1gFnSc2	budova
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Otakara	Otakar	k1gMnSc2	Otakar
Novotného	Novotný	k1gMnSc2	Novotný
<g/>
;	;	kIx,	;
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
arch	arch	k1gInSc4	arch
<g/>
.	.	kIx.	.
</s>
<s>
Jiřího	Jiří	k1gMnSc4	Jiří
Novotného	Novotný	k1gMnSc4	Novotný
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předsedů	předseda	k1gMnPc2	předseda
spolku	spolek	k1gInSc2	spolek
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Tomáš	Tomáš	k1gMnSc1	Tomáš
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
dědicem	dědic	k1gMnSc7	dědic
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
spolku	spolek	k1gInSc2	spolek
Ivan	Ivan	k1gMnSc1	Ivan
Exner	Exner	k1gMnSc1	Exner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dlouhotrvajících	dlouhotrvající	k2eAgInPc2d1	dlouhotrvající
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neukončených	ukončený	k2eNgInPc2d1	neukončený
<g/>
,	,	kIx,	,
majetkových	majetkový	k2eAgInPc2d1	majetkový
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c7	mezi
obnoveným	obnovený	k2eAgNnSc7d1	obnovené
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
a	a	k8xC	a
Nadací	nadace	k1gFnPc2	nadace
Český	český	k2eAgInSc1d1	český
fond	fond	k1gInSc1	fond
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Nadací	nadace	k1gFnSc7	nadace
českého	český	k2eAgNnSc2d1	české
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
pořádá	pořádat	k5eAaImIp3nS	pořádat
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
výstavy	výstava	k1gFnSc2	výstava
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
kulturní	kulturní	k2eAgFnSc2d1	kulturní
akce	akce	k1gFnSc2	akce
v	v	k7c6	v
Galerii	galerie	k1gFnSc6	galerie
Diamant	diamant	k1gInSc1	diamant
<g/>
,	,	kIx,	,
Lazarská	Lazarský	k2eAgFnSc1d1	Lazarská
č.	č.	k?	č.
82	[number]	k4	82
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
členy	člen	k1gMnPc4	člen
a	a	k8xC	a
přátele	přítel	k1gMnPc4	přítel
vydává	vydávat	k5eAaImIp3nS	vydávat
spolek	spolek	k1gInSc1	spolek
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
Listy	lista	k1gFnSc2	lista
S.	S.	kA	S.
<g/>
V.U.	V.U.	k1gMnSc1	V.U.
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Budova	budova	k1gFnSc1	budova
Spolku	spolek	k1gInSc2	spolek
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
.	.	kIx.	.
</s>
<s>
Šítkovský	Šítkovský	k2eAgInSc1d1	Šítkovský
mlýn	mlýn	k1gInSc1	mlýn
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
nábřeží	nábřeží	k1gNnSc6	nábřeží
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
doložen	doložit	k5eAaPmNgInS	doložit
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1495	[number]	k4	1495
o	o	k7c4	o
vodárnu	vodárna	k1gFnSc4	vodárna
a	a	k8xC	a
Šítkovskou	Šítkovský	k2eAgFnSc4d1	Šítkovská
vodárenskou	vodárenský	k2eAgFnSc4d1	vodárenská
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
přestavby	přestavba	k1gFnPc1	přestavba
respektovaly	respektovat	k5eAaImAgFnP	respektovat
vodárenskou	vodárenský	k2eAgFnSc4d1	vodárenská
věž	věž	k1gFnSc4	věž
s	s	k7c7	s
barokní	barokní	k2eAgFnSc7d1	barokní
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Vodárna	vodárna	k1gFnSc1	vodárna
zásobovala	zásobovat	k5eAaImAgFnS	zásobovat
čtyři	čtyři	k4xCgFnPc4	čtyři
městské	městský	k2eAgFnPc4d1	městská
kašny	kašna	k1gFnPc4	kašna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
vystavěl	vystavět	k5eAaPmAgInS	vystavět
spolek	spolek	k1gInSc1	spolek
Mánes	Mánes	k1gMnSc1	Mánes
s	s	k7c7	s
finanční	finanční	k2eAgFnSc7d1	finanční
podporou	podpora	k1gFnSc7	podpora
prezidenta	prezident	k1gMnSc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
funkcionalistický	funkcionalistický	k2eAgInSc4d1	funkcionalistický
komplex	komplex	k1gInSc4	komplex
restaurace	restaurace	k1gFnSc2	restaurace
<g/>
,	,	kIx,	,
klubovny	klubovna	k1gFnSc2	klubovna
a	a	k8xC	a
výstavní	výstavní	k2eAgFnSc2d1	výstavní
síně	síň	k1gFnSc2	síň
<g/>
,	,	kIx,	,
navržený	navržený	k2eAgInSc4d1	navržený
Otakarem	Otakar	k1gMnSc7	Otakar
Novotným	Novotný	k1gMnSc7	Novotný
<g/>
.	.	kIx.	.
viz	vidět	k5eAaImRp2nS	vidět
"	"	kIx"	"
<g/>
Řádní	řádný	k2eAgMnPc1d1	řádný
členové	člen	k1gMnPc1	člen
Spolku	spolek	k1gInSc2	spolek
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
Mánes	Mánes	k1gMnSc1	Mánes
od	od	k7c2	od
generace	generace	k1gFnSc2	generace
zakladatelů	zakladatel	k1gMnPc2	zakladatel
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
"	"	kIx"	"
na	na	k7c6	na
serveru	server	k1gInSc6	server
svumanes	svumanesa	k1gFnPc2	svumanesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
