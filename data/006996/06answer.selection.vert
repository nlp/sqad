<s>
Spolek	spolek	k1gInSc1	spolek
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
Mánes	Mánes	k1gMnSc1	Mánes
(	(	kIx(	(
<g/>
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
diskutovat	diskutovat	k5eAaImF	diskutovat
a	a	k8xC	a
přednášet	přednášet	k5eAaImF	přednášet
o	o	k7c4	o
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
českém	český	k2eAgInSc6d1	český
tak	tak	k6eAd1	tak
evropském	evropský	k2eAgInSc6d1	evropský
<g/>
,	,	kIx,	,
vydávat	vydávat	k5eAaImF	vydávat
časopisy	časopis	k1gInPc1	časopis
Volné	volný	k2eAgInPc1d1	volný
směry	směr	k1gInPc1	směr
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Styl	styl	k1gInSc1	styl
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgFnSc2d1	umělecká
publikace	publikace	k1gFnSc2	publikace
a	a	k8xC	a
pořádat	pořádat	k5eAaImF	pořádat
výstavy	výstava	k1gFnPc4	výstava
<g/>
.	.	kIx.	.
</s>
