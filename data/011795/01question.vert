<s>
Jaký	jaký	k3yQgInSc1	jaký
typ	typ	k1gInSc1	typ
štafety	štafeta	k1gFnSc2	štafeta
byl	být	k5eAaImAgInS	být
součást	součást	k1gFnSc4	součást
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
západosibiřském	západosibiřský	k2eAgInSc6d1	západosibiřský
Chanty-Mansijsku	Chanty-Mansijsek	k1gInSc6	Chanty-Mansijsek
<g/>
?	?	kIx.	?
</s>
