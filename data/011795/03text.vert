<p>
<s>
Smíšená	smíšený	k2eAgFnSc1d1	smíšená
štafeta	štafeta	k1gFnSc1	štafeta
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
západosibiřském	západosibiřský	k2eAgInSc6d1	západosibiřský
Chanty-Mansijsku	Chanty-Mansijsek	k1gInSc6	Chanty-Mansijsek
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
smíšených	smíšený	k2eAgFnPc2d1	smíšená
štafet	štafeta	k1gFnPc2	štafeta
byl	být	k5eAaImAgInS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
soutěží	soutěž	k1gFnSc7	soutěž
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nebyl	být	k5eNaImAgMnS	být
zahrnut	zahrnout	k5eAaPmNgMnS	zahrnout
do	do	k7c2	do
programu	program	k1gInSc2	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
závodu	závod	k1gInSc2	závod
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c4	v
13.15	[number]	k4	13.15
hodin	hodina	k1gFnPc2	hodina
SEČ	seč	k6eAd1	seč
<g/>
.	.	kIx.	.
</s>
<s>
Závodu	závod	k1gInSc3	závod
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
72	[number]	k4	72
závodníků	závodník	k1gMnPc2	závodník
z	z	k7c2	z
18	[number]	k4	18
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
této	tento	k3xDgFnSc2	tento
disciplíny	disciplína	k1gFnSc2	disciplína
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
mužstvo	mužstvo	k1gNnSc1	mužstvo
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
minutu	minuta	k1gFnSc4	minuta
před	před	k7c7	před
druhým	druhý	k4xOgNnSc7	druhý
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Bronz	bronz	k1gInSc4	bronz
brali	brát	k5eAaImAgMnP	brát
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Biathlon	Biathlon	k1gInSc4	Biathlon
World	World	k1gInSc1	World
Championships	Championships	k1gInSc1	Championships
2010	[number]	k4	2010
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgInPc1d1	kompletní
výsledky	výsledek	k1gInPc1	výsledek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
biatlonové	biatlonový	k2eAgFnSc2d1	biatlonová
asociace	asociace	k1gFnSc2	asociace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
