<s>
Cesare	Cesar	k1gMnSc5	Cesar
Borgia	Borgium	k1gNnPc1	Borgium
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1475	[number]	k4	1475
Řím	Řím	k1gInSc1	Řím
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1507	[number]	k4	1507
Viana	Viano	k1gNnSc2	Viano
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
válečník	válečník	k1gMnSc1	válečník
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
původně	původně	k6eAd1	původně
španělského	španělský	k2eAgInSc2d1	španělský
rodu	rod	k1gInSc2	rod
Borgia	Borgius	k1gMnSc2	Borgius
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
Rodrigo	Rodrigo	k1gMnSc1	Rodrigo
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
Vanozza	Vanozza	k1gFnSc1	Vanozza
de	de	k?	de
<g/>
'	'	kIx"	'
Catannei	Catanne	k1gFnSc2	Catanne
<g/>
.	.	kIx.	.
</s>
