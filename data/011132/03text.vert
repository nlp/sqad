<p>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
Borgia	Borgium	k1gNnPc1	Borgium
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1475	[number]	k4	1475
Řím	Řím	k1gInSc1	Řím
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1507	[number]	k4	1507
Viana	Viano	k1gNnSc2	Viano
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
válečník	válečník	k1gMnSc1	válečník
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
původně	původně	k6eAd1	původně
španělského	španělský	k2eAgInSc2d1	španělský
rodu	rod	k1gInSc2	rod
Borgia	Borgius	k1gMnSc2	Borgius
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
Rodrigo	Rodrigo	k1gMnSc1	Rodrigo
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
Vanozza	Vanozza	k1gFnSc1	Vanozza
de	de	k?	de
<g/>
'	'	kIx"	'
Catannei	Catanne	k1gFnSc2	Catanne
<g/>
.	.	kIx.	.
</s>
<s>
Cesarova	Cesarův	k2eAgFnSc1d1	Cesarova
životní	životní	k2eAgFnSc1d1	životní
snaha	snaha	k1gFnSc1	snaha
směřovala	směřovat	k5eAaImAgFnS	směřovat
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
silného	silný	k2eAgInSc2d1	silný
státního	státní	k2eAgInSc2d1	státní
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dočasně	dočasně	k6eAd1	dočasně
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
střední	střední	k2eAgFnSc6d1	střední
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
se	se	k3xPyFc4	se
nebál	bát	k5eNaImAgInS	bát
využívat	využívat	k5eAaImF	využívat
jak	jak	k6eAd1	jak
morálně	morálně	k6eAd1	morálně
pochybných	pochybný	k2eAgFnPc2d1	pochybná
aktivit	aktivita	k1gFnPc2	aktivita
a	a	k8xC	a
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
moci	moct	k5eAaImF	moct
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gNnSc4	on
dokonce	dokonce	k9	dokonce
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
biskupem	biskup	k1gInSc7	biskup
a	a	k8xC	a
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
a	a	k8xC	a
vyhnán	vyhnán	k2eAgInSc1d1	vyhnán
papežem	papež	k1gMnSc7	papež
Juliem	Julius	k1gMnSc7	Julius
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
švagrovi	švagr	k1gMnSc3	švagr
<g/>
,	,	kIx,	,
navarrskému	navarrský	k2eAgMnSc3d1	navarrský
králi	král	k1gMnSc3	král
Janovi	Jan	k1gMnSc3	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
Borgia	Borgius	k1gMnSc2	Borgius
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1498	[number]	k4	1498
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
César	César	k1gMnSc1	César
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
původně	původně	k6eAd1	původně
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
církevní	církevní	k2eAgFnSc4d1	církevní
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
opětného	opětný	k2eAgNnSc2d1	opětné
získání	získání	k1gNnSc2	získání
papežského	papežský	k2eAgInSc2d1	papežský
stolce	stolec	k1gInSc2	stolec
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
Borgiů	Borgi	k1gInPc2	Borgi
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Juan	Juan	k1gMnSc1	Juan
(	(	kIx(	(
<g/>
Giovanni	Giovaneň	k1gFnSc6	Giovaneň
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaPmF	stát
vojákem	voják	k1gMnSc7	voják
a	a	k8xC	a
politikem	politik	k1gMnSc7	politik
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
Cesare	Cesar	k1gMnSc5	Cesar
církevní	církevní	k2eAgMnSc1d1	církevní
dráhy	dráha	k1gFnPc4	dráha
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
<g/>
,	,	kIx,	,
směřovala	směřovat	k5eAaImAgFnS	směřovat
jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
silného	silný	k2eAgInSc2d1	silný
státního	státní	k2eAgInSc2d1	státní
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
střední	střední	k2eAgFnSc6d1	střední
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
se	se	k3xPyFc4	se
nebál	bát	k5eNaImAgInS	bát
využívat	využívat	k5eAaPmF	využívat
jak	jak	k6eAd1	jak
morálně	morálně	k6eAd1	morálně
pochybných	pochybný	k2eAgFnPc2d1	pochybná
aktivit	aktivita	k1gFnPc2	aktivita
a	a	k8xC	a
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
moci	moct	k5eAaImF	moct
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gNnSc4	on
dokonce	dokonce	k9	dokonce
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
biskupem	biskup	k1gInSc7	biskup
a	a	k8xC	a
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
a	a	k8xC	a
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
papežem	papež	k1gMnSc7	papež
Juliem	Julius	k1gMnSc7	Julius
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc1	vzdělání
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
Cesare	Cesar	k1gMnSc5	Cesar
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
katalánský	katalánský	k2eAgInSc4d1	katalánský
či	či	k8xC	či
španělský	španělský	k2eAgInSc4d1	španělský
původ	původ	k1gInSc4	původ
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
výchovu	výchova	k1gFnSc4	výchova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
byla	být	k5eAaImAgFnS	být
hovorovou	hovorový	k2eAgFnSc7d1	hovorová
řečí	řeč	k1gFnSc7	řeč
kromě	kromě	k7c2	kromě
italštiny	italština	k1gFnSc2	italština
též	též	k6eAd1	též
španělština	španělština	k1gFnSc1	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
jeho	jeho	k3xOp3gMnPc2	jeho
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
bratrů	bratr	k1gMnPc2	bratr
byli	být	k5eAaImAgMnP	být
povoláni	povolán	k2eAgMnPc1d1	povolán
přední	přední	k2eAgMnPc1d1	přední
španělští	španělský	k2eAgMnPc1d1	španělský
učenci	učenec	k1gMnPc1	učenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gNnSc4	on
vzdělávali	vzdělávat	k5eAaImAgMnP	vzdělávat
v	v	k7c6	v
aritmetice	aritmetika	k1gFnSc6	aritmetika
<g/>
,	,	kIx,	,
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
,	,	kIx,	,
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
kreslení	kreslení	k1gNnSc6	kreslení
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
též	též	k9	též
francouzštinu	francouzština	k1gFnSc4	francouzština
a	a	k8xC	a
klasickou	klasický	k2eAgFnSc4d1	klasická
řečtinu	řečtina	k1gFnSc4	řečtina
a	a	k8xC	a
latinu	latina	k1gFnSc4	latina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
získal	získat	k5eAaPmAgMnS	získat
Cesare	Cesar	k1gMnSc5	Cesar
také	také	k9	také
značné	značný	k2eAgFnPc4d1	značná
fyzické	fyzický	k2eAgFnPc4d1	fyzická
schopnosti	schopnost	k1gFnPc4	schopnost
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
jako	jako	k8xS	jako
šermíř	šermíř	k1gMnSc1	šermíř
a	a	k8xC	a
jezdec	jezdec	k1gMnSc1	jezdec
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
14	[number]	k4	14
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgMnS	začít
Cesare	Cesar	k1gMnSc5	Cesar
studovat	studovat	k5eAaImF	studovat
právní	právní	k2eAgFnSc2d1	právní
vědy	věda	k1gFnSc2	věda
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Perugii	Perugie	k1gFnSc6	Perugie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1491	[number]	k4	1491
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Pisa	Pisa	k1gFnSc1	Pisa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
posléze	posléze	k6eAd1	posléze
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
doktora	doktor	k1gMnSc2	doktor
církevního	církevní	k2eAgMnSc2d1	církevní
a	a	k8xC	a
světského	světský	k2eAgNnSc2d1	světské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Církevní	církevní	k2eAgMnSc1d1	církevní
hodnostář	hodnostář	k1gMnSc1	hodnostář
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
devíti	devět	k4xCc2	devět
let	léto	k1gNnPc2	léto
zastával	zastávat	k5eAaImAgMnS	zastávat
Cesare	Cesar	k1gMnSc5	Cesar
různé	různý	k2eAgInPc4d1	různý
církevní	církevní	k2eAgInPc4d1	církevní
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
vynášely	vynášet	k5eAaImAgFnP	vynášet
velké	velký	k2eAgInPc4d1	velký
příjmy	příjem	k1gInPc4	příjem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1491	[number]	k4	1491
jej	on	k3xPp3gMnSc4	on
otec	otec	k1gMnSc1	otec
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
biskupem	biskup	k1gMnSc7	biskup
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Pamploně	Pamplona	k1gFnSc6	Pamplona
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Valencie	Valencie	k1gFnSc2	Valencie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1494	[number]	k4	1494
byl	být	k5eAaImAgMnS	být
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
opatem	opat	k1gMnSc7	opat
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
Saint-Michel	Saint-Michel	k1gMnSc1	Saint-Michel
de	de	k?	de
Cuxa	Cuxa	k1gMnSc1	Cuxa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
otce	otec	k1gMnSc2	otec
papežem	papež	k1gMnSc7	papež
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
kardinála	kardinál	k1gMnSc4	kardinál
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
kardinál	kardinál	k1gMnSc1	kardinál
bydlel	bydlet	k5eAaImAgMnS	bydlet
Cesare	Cesar	k1gMnSc5	Cesar
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc2	Vatikán
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
rádcem	rádce	k1gMnSc7	rádce
a	a	k8xC	a
důvěrníkem	důvěrník	k1gMnSc7	důvěrník
svého	svůj	k3xOyFgMnSc2	svůj
papežského	papežský	k2eAgMnSc2d1	papežský
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1494	[number]	k4	1494
podnikl	podniknout	k5eAaPmAgMnS	podniknout
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
velké	velký	k2eAgNnSc1d1	velké
vojenské	vojenský	k2eAgNnSc1d1	vojenské
tažení	tažení	k1gNnSc1	tažení
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zmocnit	zmocnit	k5eAaPmF	zmocnit
se	se	k3xPyFc4	se
Království	království	k1gNnPc2	království
neapolského	neapolský	k2eAgInSc2d1	neapolský
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc4	co
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
vtáhl	vtáhnout	k5eAaPmAgMnS	vtáhnout
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
rádcové	rádce	k1gMnPc1	rádce
nuceni	nutit	k5eAaImNgMnP	nutit
uchýlit	uchýlit	k5eAaPmF	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
Andělského	andělský	k2eAgInSc2d1	andělský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
krále	král	k1gMnSc2	král
svolil	svolit	k5eAaPmAgMnS	svolit
papež	papež	k1gMnSc1	papež
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Cesare	Cesar	k1gMnSc5	Cesar
doprovodil	doprovodit	k5eAaPmAgInS	doprovodit
francouzské	francouzský	k2eAgNnSc4d1	francouzské
vojsko	vojsko	k1gNnSc4	vojsko
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
jako	jako	k8xS	jako
papežský	papežský	k2eAgInSc4d1	papežský
legát	legát	k1gInSc4	legát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
však	však	k9	však
Cesarovi	Cesar	k1gMnSc3	Cesar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
výborným	výborný	k2eAgMnSc7d1	výborný
jezdcem	jezdec	k1gMnSc7	jezdec
<g/>
,	,	kIx,	,
zdařil	zdařit	k5eAaPmAgInS	zdařit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
při	při	k7c6	při
zastávce	zastávka	k1gFnSc6	zastávka
u	u	k7c2	u
Velletri	Velletr	k1gFnSc2	Velletr
v	v	k7c4	v
převlečení	převlečení	k1gNnSc4	převlečení
za	za	k7c4	za
koňského	koňský	k2eAgMnSc4d1	koňský
pacholka	pacholek	k1gMnSc4	pacholek
útěk	útěk	k1gInSc4	útěk
a	a	k8xC	a
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
pod	pod	k7c4	pod
ochranu	ochrana	k1gFnSc4	ochrana
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Voják	voják	k1gMnSc1	voják
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
===	===	k?	===
</s>
</p>
<p>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
kariéra	kariéra	k1gFnSc1	kariéra
však	však	k9	však
Cesara	Cesara	k1gFnSc1	Cesara
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
nepřitahovala	přitahovat	k5eNaImAgFnS	přitahovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1497	[number]	k4	1497
byl	být	k5eAaImAgMnS	být
Cesarův	Cesarův	k2eAgMnSc1d1	Cesarův
bratr	bratr	k1gMnSc1	bratr
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
(	(	kIx(	(
<g/>
Juan	Juan	k1gMnSc1	Juan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
papežových	papežův	k2eAgFnPc2d1	papežova
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
záhadně	záhadně	k6eAd1	záhadně
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezření	k1gNnSc4	podezření
ze	z	k7c2	z
zosnování	zosnování	k1gNnSc2	zosnování
tohoto	tento	k3xDgInSc2	tento
činu	čin	k1gInSc2	čin
padlo	padnout	k5eAaPmAgNnS	padnout
také	také	k9	také
na	na	k7c4	na
Cesara	Cesar	k1gMnSc4	Cesar
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
smrti	smrt	k1gFnSc2	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
se	se	k3xPyFc4	se
Cesare	Cesar	k1gMnSc5	Cesar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1498	[number]	k4	1498
se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
otce	otec	k1gMnSc2	otec
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
kardinálského	kardinálský	k2eAgInSc2d1	kardinálský
klobouku	klobouk	k1gInSc2	klobouk
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
a	a	k8xC	a
politickou	politický	k2eAgFnSc4d1	politická
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
odjakživa	odjakživa	k6eAd1	odjakživa
toužil	toužit	k5eAaImAgMnS	toužit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1498	[number]	k4	1498
se	se	k3xPyFc4	se
Cesare	Cesar	k1gMnSc5	Cesar
stal	stát	k5eAaPmAgInS	stát
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Valentinois	Valentinois	k1gFnSc2	Valentinois
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
staré	starý	k2eAgNnSc1d1	staré
francouzské	francouzský	k2eAgNnSc1d1	francouzské
vévodství	vévodství	k1gNnSc1	vévodství
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Valence	valence	k1gFnSc2	valence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1499	[number]	k4	1499
se	se	k3xPyFc4	se
Cesare	Cesar	k1gMnSc5	Cesar
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Charlottou	Charlotta	k1gFnSc7	Charlotta
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Albret	Albreta	k1gFnPc2	Albreta
<g/>
,	,	kIx,	,
sestrou	sestra	k1gFnSc7	sestra
krále	král	k1gMnSc2	král
navarrského	navarrský	k2eAgMnSc2d1	navarrský
Jana	Jan	k1gMnSc2	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
však	však	k9	však
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
porodila	porodit	k5eAaPmAgFnS	porodit
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nespatřil	spatřit	k5eNaPmAgMnS	spatřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1499	[number]	k4	1499
se	se	k3xPyFc4	se
Cesare	Cesar	k1gMnSc5	Cesar
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
vojenské	vojenský	k2eAgNnSc4d1	vojenské
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
městu	město	k1gNnSc3	město
Forli	Forl	k1gMnPc1	Forl
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
čelit	čelit	k5eAaImF	čelit
neobyčejnému	obyčejný	k2eNgMnSc3d1	neobyčejný
a	a	k8xC	a
obávanému	obávaný	k2eAgMnSc3d1	obávaný
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
byla	být	k5eAaImAgFnS	být
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Caterina	Caterina	k1gFnSc1	Caterina
Sforza	Sforza	k1gFnSc1	Sforza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vládla	vládnout	k5eAaImAgFnS	vládnout
tomuto	tento	k3xDgInSc3	tento
území	území	k1gNnSc4	území
namísto	namísto	k7c2	namísto
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Forli	Forl	k1gMnPc1	Forl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dlouho	dlouho	k6eAd1	dlouho
trpěli	trpět	k5eAaImAgMnP	trpět
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
této	tento	k3xDgFnSc2	tento
mocné	mocný	k2eAgFnSc2d1	mocná
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
otevřít	otevřít	k5eAaPmF	otevřít
Cesarovi	Cesar	k1gMnSc6	Cesar
brány	brána	k1gFnSc2	brána
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechá	nechat	k5eAaPmIp3nS	nechat
své	svůj	k3xOyFgFnPc4	svůj
francouzské	francouzský	k2eAgFnPc4d1	francouzská
jednotky	jednotka	k1gFnPc4	jednotka
před	před	k7c7	před
hradbami	hradba	k1gFnPc7	hradba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
to	ten	k3xDgNnSc4	ten
slíbil	slíbit	k5eAaPmAgMnS	slíbit
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
tam	tam	k6eAd1	tam
však	však	k9	však
vtáhli	vtáhnout	k5eAaPmAgMnP	vtáhnout
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
francouzští	francouzský	k2eAgMnPc1d1	francouzský
žoldnéři	žoldnér	k1gMnPc1	žoldnér
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
Forli	Forl	k1gMnPc1	Forl
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
se	se	k3xPyFc4	se
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
vojenském	vojenský	k2eAgNnSc6d1	vojenské
tažení	tažení	k1gNnSc6	tažení
vrátil	vrátit	k5eAaPmAgInS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
v	v	k7c6	v
triumfálním	triumfální	k2eAgInSc6d1	triumfální
průvodu	průvod	k1gInSc6	průvod
hodném	hodný	k2eAgInSc6d1	hodný
starověkého	starověký	k2eAgMnSc2d1	starověký
římského	římský	k2eAgMnSc2d1	římský
generála	generál	k1gMnSc2	generál
<g/>
.	.	kIx.	.
</s>
<s>
Borgiové	Borgius	k1gMnPc1	Borgius
obnovili	obnovit	k5eAaPmAgMnP	obnovit
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
opět	opět	k6eAd1	opět
napadnout	napadnout	k5eAaPmF	napadnout
Neapol	Neapol	k1gFnSc4	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
do	do	k7c2	do
pole	pole	k1gNnSc2	pole
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1500	[number]	k4	1500
<g/>
.	.	kIx.	.
</s>
<s>
Dobýval	dobývat	k5eAaImAgMnS	dobývat
jedno	jeden	k4xCgNnSc4	jeden
italské	italský	k2eAgNnSc4d1	italské
město	město	k1gNnSc4	město
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
města	město	k1gNnPc1	město
se	se	k3xPyFc4	se
možná	možná	k9	možná
vzdávala	vzdávat	k5eAaImAgFnS	vzdávat
ochotně	ochotně	k6eAd1	ochotně
sama	sám	k3xTgFnSc1	sám
a	a	k8xC	a
zbavovala	zbavovat	k5eAaImAgFnS	zbavovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
svých	svůj	k3xOyFgMnPc2	svůj
tyranských	tyranský	k2eAgMnPc2d1	tyranský
vladařů	vladař	k1gMnPc2	vladař
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Cesara	Cesar	k1gMnSc4	Cesar
Borgii	Borgie	k1gFnSc6	Borgie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
jako	jako	k9	jako
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
a	a	k8xC	a
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
vládce	vládce	k1gMnSc1	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Vybíral	Vybíral	k1gMnSc1	Vybíral
standardní	standardní	k2eAgFnSc2d1	standardní
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
udržoval	udržovat	k5eAaImAgMnS	udržovat
veřejný	veřejný	k2eAgInSc4d1	veřejný
pořádek	pořádek	k1gInSc4	pořádek
<g/>
,	,	kIx,	,
potlačoval	potlačovat	k5eAaImAgMnS	potlačovat
bandity	bandita	k1gMnPc4	bandita
a	a	k8xC	a
pečoval	pečovat	k5eAaImAgMnS	pečovat
o	o	k7c4	o
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Cesare	Cesar	k1gMnSc5	Cesar
coby	coby	k?	coby
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Romagni	Romagň	k1gMnSc3	Romagň
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
papežské	papežský	k2eAgFnSc2d1	Papežská
armády	armáda	k1gFnSc2	armáda
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
králi	král	k1gMnSc3	král
a	a	k8xC	a
táhl	táhnout	k5eAaImAgInS	táhnout
proti	proti	k7c3	proti
Neapoli	Neapol	k1gFnSc3	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
obsadil	obsadit	k5eAaPmAgInS	obsadit
Capuu	Capua	k1gFnSc4	Capua
a	a	k8xC	a
ve	v	k7c6	v
městě	město	k1gNnSc6	město
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
události	událost	k1gFnSc3	událost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
při	při	k7c6	při
válčení	válčení	k1gNnSc6	válčení
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
běžná	běžný	k2eAgFnSc1d1	běžná
–	–	k?	–
bylo	být	k5eAaImAgNnS	být
tam	tam	k6eAd1	tam
zmasakrováno	zmasakrovat	k5eAaPmNgNnS	zmasakrovat
6000	[number]	k4	6000
obyvatel	obyvatel	k1gMnPc2	obyvatel
včetně	včetně	k7c2	včetně
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Pádem	Pád	k1gInSc7	Pád
Neapole	Neapol	k1gFnSc2	Neapol
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
VI	VI	kA	VI
<g/>
.	.	kIx.	.
dočasně	dočasně	k6eAd1	dočasně
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
území	území	k1gNnSc4	území
svých	svůj	k3xOyFgMnPc2	svůj
nepřátel	nepřítel	k1gMnPc2	nepřítel
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
tehdy	tehdy	k6eAd1	tehdy
počínal	počínat	k5eAaImAgMnS	počínat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
"	"	kIx"	"
<g/>
překonal	překonat	k5eAaPmAgInS	překonat
bestialitu	bestialita	k1gFnSc4	bestialita
a	a	k8xC	a
divokost	divokost	k1gFnSc4	divokost
Nerona	Nero	k1gMnSc2	Nero
i	i	k8xC	i
Caliguly	Caligula	k1gFnSc2	Caligula
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1503	[number]	k4	1503
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Cesare	Cesar	k1gMnSc5	Cesar
Borgia	Borgius	k1gMnSc2	Borgius
těžce	těžce	k6eAd1	těžce
onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
byli	být	k5eAaImAgMnP	být
otráveni	otrávit	k5eAaPmNgMnP	otrávit
jedem	jed	k1gInSc7	jed
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
nakazili	nakazit	k5eAaPmAgMnP	nakazit
malárií	malárie	k1gFnSc7	malárie
<g/>
.	.	kIx.	.
</s>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
sice	sice	k8xC	sice
nemoc	nemoc	k1gFnSc4	nemoc
překonal	překonat	k5eAaPmAgMnS	překonat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otcova	otcův	k2eAgFnSc1d1	otcova
smrt	smrt	k1gFnSc1	smrt
pohřbila	pohřbít	k5eAaPmAgFnS	pohřbít
jeho	jeho	k3xOp3gFnSc1	jeho
naděje	naděje	k1gFnSc1	naděje
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
pokročilý	pokročilý	k2eAgInSc4d1	pokročilý
věk	věk	k1gInSc4	věk
energický	energický	k2eAgInSc1d1	energický
a	a	k8xC	a
bojovný	bojovný	k2eAgMnSc1d1	bojovný
papež	papež	k1gMnSc1	papež
Julius	Julius	k1gMnSc1	Julius
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
uchopil	uchopit	k5eAaPmAgInS	uchopit
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Dobyl	dobýt	k5eAaPmAgMnS	dobýt
několik	několik	k4yIc4	několik
měst	město	k1gNnPc2	město
dosud	dosud	k6eAd1	dosud
ovládaných	ovládaný	k2eAgNnPc2d1	ovládané
Cesarem	Cesar	k1gInSc7	Cesar
Borgiou	Borgia	k1gFnSc7	Borgia
<g/>
.	.	kIx.	.
</s>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
budoucnost	budoucnost	k1gFnSc1	budoucnost
zdála	zdát	k5eAaImAgFnS	zdát
příznivá	příznivý	k2eAgFnSc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
trvala	trvat	k5eAaImAgFnS	trvat
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
r.	r.	kA	r.
1504	[number]	k4	1504
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
regent	regent	k1gMnSc1	regent
Neapolského	neapolský	k2eAgNnSc2d1	Neapolské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
na	na	k7c4	na
popud	popud	k1gInSc4	popud
nového	nový	k2eAgMnSc2d1	nový
papeže	papež	k1gMnSc2	papež
zatknout	zatknout	k5eAaPmF	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
mučen	mučit	k5eAaImNgInS	mučit
a	a	k8xC	a
následně	následně	k6eAd1	následně
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c6	v
pevnosti	pevnost	k1gFnSc6	pevnost
La	la	k1gNnSc2	la
Mota	moto	k1gNnSc2	moto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
Cesare	Cesar	k1gMnSc5	Cesar
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
vězení	vězení	k1gNnSc2	vězení
odvážným	odvážný	k2eAgInSc7d1	odvážný
způsobem	způsob	k1gInSc7	způsob
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
spustil	spustit	k5eAaPmAgInS	spustit
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
na	na	k7c6	na
hedvábné	hedvábný	k2eAgFnSc6d1	hedvábná
šňůře	šňůra	k1gFnSc6	šňůra
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgInS	dostat
se	se	k3xPyFc4	se
nepoznán	poznán	k2eNgInSc1d1	nepoznán
až	až	k8xS	až
do	do	k7c2	do
Pamplony	Pamplon	k1gInPc4	Pamplon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
švagrem	švagr	k1gMnSc7	švagr
Jeanem	Jean	k1gMnSc7	Jean
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Albret	Albret	k1gMnSc1	Albret
<g/>
,	,	kIx,	,
vládnoucím	vládnoucí	k2eAgMnPc3d1	vládnoucí
jako	jako	k8xC	jako
navarrský	navarrský	k2eAgMnSc1d1	navarrský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
už	už	k9	už
jen	jen	k9	jen
dobrodruhem	dobrodruh	k1gMnSc7	dobrodruh
bez	bez	k7c2	bez
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Bojoval	bojovat	k5eAaImAgMnS	bojovat
ještě	ještě	k9	ještě
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
žoldnéřských	žoldnéřský	k2eAgFnPc2d1	žoldnéřská
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
Condottiere	Condottier	k1gInSc5	Condottier
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1507	[number]	k4	1507
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
31	[number]	k4	31
let	léto	k1gNnPc2	léto
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
pevnosti	pevnost	k1gFnSc2	pevnost
Viana	Vian	k1gInSc2	Vian
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrtí	smrtit	k5eAaImIp3nS	smrtit
Cesara	Cesara	k1gFnSc1	Cesara
sice	sice	k8xC	sice
rod	rod	k1gInSc1	rod
Borgiů	Borgi	k1gInPc2	Borgi
nezanikl	zaniknout	k5eNaPmAgInS	zaniknout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Cesara	Cesara	k1gFnSc1	Cesara
přežili	přežít	k5eAaPmAgMnP	přežít
jeho	jeho	k3xOp3gFnPc6	jeho
sestra	sestra	k1gFnSc1	sestra
Lucrezia	Lucrezia	k1gFnSc1	Lucrezia
a	a	k8xC	a
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
bratr	bratr	k1gMnSc1	bratr
Goffredo	Goffredo	k1gNnSc1	Goffredo
(	(	kIx(	(
<g/>
Jofré	Jofrý	k2eAgFnPc1d1	Jofrý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mocensky	mocensky	k6eAd1	mocensky
se	se	k3xPyFc4	se
Borgiové	Borgius	k1gMnPc1	Borgius
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nevzchopili	vzchopit	k5eNaPmAgMnP	vzchopit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrazy	odraz	k1gInPc1	odraz
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
filmu	film	k1gInSc6	film
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
své	svůj	k3xOyFgFnSc2	svůj
role	role	k1gFnSc2	role
jako	jako	k8xC	jako
postava	postava	k1gFnSc1	postava
významná	významný	k2eAgFnSc1d1	významná
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
Machiavelliho	Machiavelli	k1gMnSc2	Machiavelli
hlavního	hlavní	k2eAgNnSc2d1	hlavní
díla	dílo	k1gNnSc2	dílo
Vladař	vladař	k1gMnSc1	vladař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Cesare	Cesar	k1gMnSc5	Cesar
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Rodrigo	Rodrigo	k1gMnSc1	Rodrigo
Borgia	Borgia	k1gFnSc1	Borgia
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Lucrezia	Lucrezius	k1gMnSc2	Lucrezius
Borgia	Borgius	k1gMnSc2	Borgius
<g/>
,	,	kIx,	,
postavou	postava	k1gFnSc7	postava
mnoha	mnoho	k4c2	mnoho
historických	historický	k2eAgInPc2d1	historický
románů	román	k1gInPc2	román
a	a	k8xC	a
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesare	Cesar	k1gMnSc5	Cesar
Borgia	Borgius	k1gMnSc2	Borgius
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Assassin	Assassina	k1gFnPc2	Assassina
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Creed	Creed	k1gInSc1	Creed
<g/>
:	:	kIx,	:
Brotherhood	Brotherhood	k1gInSc1	Brotherhood
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
Cesare	Cesar	k1gMnSc5	Cesar
zabit	zabít	k5eAaPmNgMnS	zabít
hlavním	hlavní	k2eAgMnSc7d1	hlavní
protagonistou	protagonista	k1gMnSc7	protagonista
Eziem	Ezi	k1gMnSc7	Ezi
Auditore	auditor	k1gMnSc5	auditor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
jej	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
The	The	k1gFnSc1	The
Borgias	Borgias	k1gInSc1	Borgias
(	(	kIx(	(
<g/>
Borgiové	Borgius	k1gMnPc1	Borgius
<g/>
:	:	kIx,	:
moc	moc	k6eAd1	moc
a	a	k8xC	a
vášeň	vášeň	k1gFnSc1	vášeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cesare	Cesar	k1gMnSc5	Cesar
Borgia	Borgium	k1gNnPc1	Borgium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Cesare	Cesar	k1gMnSc5	Cesar
Borgia	Borgius	k1gMnSc4	Borgius
</s>
</p>
