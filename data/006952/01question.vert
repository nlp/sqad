<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
vodopád	vodopád	k1gInSc1	vodopád
<g/>
,	,	kIx,	,
<g/>
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Semily	Semily	k1gInPc1	Semily
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
rekreačního	rekreační	k2eAgNnSc2d1	rekreační
střediska	středisko	k1gNnSc2	středisko
Harrachov	Harrachov	k1gInSc1	Harrachov
<g/>
?	?	kIx.	?
</s>
