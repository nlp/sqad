<s>
Mumlavský	Mumlavský	k2eAgInSc1d1	Mumlavský
vodopád	vodopád	k1gInSc1	vodopád
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Mumlava	Mumlava	k1gFnSc1	Mumlava
Waterfall	Waterfall	k1gMnSc1	Waterfall
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
vodopád	vodopád	k1gInSc4	vodopád
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
okrese	okres	k1gInSc6	okres
Semily	Semily	k1gInPc1	Semily
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
Harrachova	Harrachov	k1gInSc2	Harrachov
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přírodní	přírodní	k2eAgFnSc4d1	přírodní
zajímavost	zajímavost	k1gFnSc4	zajímavost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ochranném	ochranný	k2eAgNnSc6d1	ochranné
pásmu	pásmo	k1gNnSc6	pásmo
Krkonošského	krkonošský	k2eAgInSc2d1	krkonošský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
německého	německý	k2eAgNnSc2d1	německé
slova	slovo	k1gNnSc2	slovo
murmeln	murmeln	k1gInSc4	murmeln
což	což	k9	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
mumlat	mumlat	k5eAaImF	mumlat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hydrologického	hydrologický	k2eAgNnSc2d1	hydrologické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejmohutnějších	mohutný	k2eAgInPc2d3	nejmohutnější
vodopádů	vodopád	k1gInPc2	vodopád
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgInPc2d3	nejkrásnější
a	a	k8xC	a
turisty	turist	k1gMnPc4	turist
nejvyhledávanějších	vyhledávaný	k2eAgInPc2d3	nejvyhledávanější
vodopádů	vodopád	k1gInPc2	vodopád
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
vodopádu	vodopád	k1gInSc2	vodopád
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
tzv.	tzv.	kA	tzv.
obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
a	a	k8xC	a
kotle	kotel	k1gInPc1	kotel
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
také	také	k9	také
jako	jako	k8xS	jako
čertova	čertův	k2eAgNnPc1d1	Čertovo
oka	oko	k1gNnPc1	oko
<g/>
,	,	kIx,	,
kterých	který	k3yQgNnPc2	který
se	se	k3xPyFc4	se
na	na	k7c6	na
toku	tok	k1gInSc6	tok
nachází	nacházet	k5eAaImIp3nS	nacházet
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
až	až	k9	až
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
hloubky	hloubka	k1gFnPc1	hloubka
3	[number]	k4	3
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
vodopádu	vodopád	k1gInSc2	vodopád
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
typickou	typický	k2eAgFnSc4d1	typická
lesní	lesní	k2eAgFnSc4d1	lesní
flóru	flóra	k1gFnSc4	flóra
a	a	k8xC	a
faunu	fauna	k1gFnSc4	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
vodopádu	vodopád	k1gInSc3	vodopád
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgMnSc1d1	možný
po	po	k7c6	po
asfaltové	asfaltový	k2eAgFnSc6d1	asfaltová
cestě	cesta	k1gFnSc6	cesta
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
Harrachova	Harrachov	k1gInSc2	Harrachov
po	po	k7c6	po
modré	modrý	k2eAgFnSc6d1	modrá
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Mumlavského	Mumlavský	k2eAgInSc2d1	Mumlavský
vodopádu	vodopád	k1gInSc2	vodopád
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
něm.	něm.	k?	něm.
slova	slovo	k1gNnSc2	slovo
murmeln	murmelna	k1gFnPc2	murmelna
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
mumlat	mumlat	k5eAaImF	mumlat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dokonale	dokonale	k6eAd1	dokonale
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vodopád	vodopád	k1gInSc1	vodopád
vydává	vydávat	k5eAaPmIp3nS	vydávat
<g/>
.	.	kIx.	.
</s>
<s>
Mumlavský	Mumlavský	k2eAgInSc1d1	Mumlavský
vodopád	vodopád	k1gInSc1	vodopád
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
v	v	k7c6	v
Harrachovské	harrachovský	k2eAgFnSc6d1	Harrachovská
kotlině	kotlina	k1gFnSc6	kotlina
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Mumlavě	mumlavě	k6eAd1	mumlavě
<g/>
.	.	kIx.	.
</s>
<s>
Vodopád	vodopád	k1gInSc1	vodopád
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
760	[number]	k4	760
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
8,9	[number]	k4	8,9
m	m	kA	m
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
a	a	k8xC	a
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stěna	stěna	k1gFnSc1	stěna
mírně	mírně	k6eAd1	mírně
svažuje	svažovat	k5eAaImIp3nS	svažovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejvýše	nejvýše	k6eAd1	nejvýše
položené	položený	k2eAgFnSc2d1	položená
tůně	tůně	k1gFnSc2	tůně
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vysoký	vysoký	k2eAgInSc1d1	vysoký
9,9	[number]	k4	9,9
m.	m.	k?	m.
Šířka	šířka	k1gFnSc1	šířka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
10	[number]	k4	10
m	m	kA	m
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
při	při	k7c6	při
nízkém	nízký	k2eAgInSc6d1	nízký
průtoku	průtok	k1gInSc6	průtok
se	se	k3xPyFc4	se
tok	tok	k1gInSc1	tok
může	moct	k5eAaImIp3nS	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
menších	malý	k2eAgNnPc2d2	menší
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
činí	činit	k5eAaImIp3nS	činit
800	[number]	k4	800
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
19	[number]	k4	19
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Mohutnost	mohutnost	k1gFnSc1	mohutnost
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
na	na	k7c4	na
7120	[number]	k4	7120
m	m	kA	m
<g/>
*	*	kIx~	*
<g/>
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Mumlavském	Mumlavský	k2eAgInSc6d1	Mumlavský
vodopádu	vodopád	k1gInSc6	vodopád
začínají	začínat	k5eAaImIp3nP	začínat
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
turistickými	turistický	k2eAgInPc7d1	turistický
výlety	výlet	k1gInPc7	výlet
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
věrohodné	věrohodný	k2eAgInPc1d1	věrohodný
záznamy	záznam	k1gInPc1	záznam
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
J.	J.	kA	J.
K.	K.	kA	K.
E.	E.	kA	E.
Hosera	Hosera	k1gFnSc1	Hosera
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
říčku	říčka	k1gFnSc4	říčka
nazývá	nazývat	k5eAaImIp3nS	nazývat
Muml	Muml	k1gInSc1	Muml
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
zmínky	zmínka	k1gFnPc1	zmínka
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
Řivnáčův	řivnáčův	k2eAgMnSc1d1	řivnáčův
Průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c6	po
království	království	k1gNnSc6	království
Českém	český	k2eAgNnSc6d1	české
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
nejstarším	starý	k2eAgMnSc7d3	nejstarší
souhrnným	souhrnný	k2eAgMnSc7d1	souhrnný
průvodcem	průvodce	k1gMnSc7	průvodce
po	po	k7c6	po
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Mumlavský	Mumlavský	k2eAgInSc1d1	Mumlavský
vodopád	vodopád	k1gInSc1	vodopád
zakreslen	zakreslit	k5eAaPmNgInS	zakreslit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
turistických	turistický	k2eAgFnPc6d1	turistická
mapách	mapa	k1gFnPc6	mapa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
vodopádů	vodopád	k1gInPc2	vodopád
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
staršími	starý	k2eAgInPc7d2	starší
obrazy	obraz	k1gInPc7	obraz
či	či	k8xC	či
rytinami	rytina	k1gFnPc7	rytina
se	se	k3xPyFc4	se
z	z	k7c2	z
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
ale	ale	k8xC	ale
nesetkáváme	setkávat	k5eNaImIp1nP	setkávat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejdokonalejší	dokonalý	k2eAgInSc4d3	nejdokonalejší
typ	typ	k1gInSc4	typ
horizontálního	horizontální	k2eAgInSc2d1	horizontální
celistvého	celistvý	k2eAgInSc2d1	celistvý
vodopádu	vodopád	k1gInSc2	vodopád
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
drsnější	drsný	k2eAgFnSc3d2	drsnější
než	než	k8xS	než
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
o	o	k7c4	o
horské	horský	k2eAgNnSc4d1	horské
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
tomu	ten	k3xDgMnSc3	ten
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
i	i	k9	i
teploty	teplota	k1gFnPc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
přilehlými	přilehlý	k2eAgFnPc7d1	přilehlá
horami	hora	k1gFnPc7	hora
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
obecně	obecně	k6eAd1	obecně
chladněji	chladně	k6eAd2	chladně
<g/>
.	.	kIx.	.
</s>
<s>
Geologický	geologický	k2eAgInSc1d1	geologický
podklad	podklad	k1gInSc1	podklad
Mumlavského	Mumlavský	k2eAgInSc2d1	Mumlavský
vodopádu	vodopád	k1gInSc2	vodopád
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnSc2d1	přilehlá
krajiny	krajina	k1gFnSc2	krajina
tvoří	tvořit	k5eAaImIp3nS	tvořit
středně	středně	k6eAd1	středně
zrnitá	zrnitý	k2eAgFnSc1d1	zrnitá
biotická	biotický	k2eAgFnSc1d1	biotická
žula	žula	k1gFnSc1	žula
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
strukturně-tektonickou	strukturněektonický	k2eAgFnSc4d1	strukturně-tektonický
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
kompaktnější	kompaktní	k2eAgFnSc4d2	kompaktnější
horninovou	horninový	k2eAgFnSc4d1	horninová
partii	partie	k1gFnSc4	partie
<g/>
,	,	kIx,	,
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
podélných	podélný	k2eAgInPc2d1	podélný
a	a	k8xC	a
příčných	příčný	k2eAgFnPc2d1	příčná
puklin	puklina	k1gFnPc2	puklina
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
i	i	k9	i
šikmo	šikmo	k6eAd1	šikmo
příčné	příčný	k2eAgFnPc1d1	příčná
či	či	k8xC	či
příčně	příčně	k6eAd1	příčně
svislé	svislý	k2eAgFnSc2d1	svislá
pukliny	puklina	k1gFnSc2	puklina
<g/>
.	.	kIx.	.
</s>
<s>
Vodopád	vodopád	k1gInSc1	vodopád
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
erozí	eroze	k1gFnSc7	eroze
a	a	k8xC	a
lokální	lokální	k2eAgFnSc7d1	lokální
erozí	eroze	k1gFnSc7	eroze
Harrachovské	harrachovský	k2eAgFnSc2d1	Harrachovská
kotliny	kotlina	k1gFnSc2	kotlina
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tektonického	tektonický	k2eAgInSc2d1	tektonický
poklesu	pokles	k1gInSc2	pokles
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
lokální	lokální	k2eAgFnSc1d1	lokální
eroze	eroze	k1gFnSc1	eroze
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
mladších	mladý	k2eAgFnPc6d2	mladší
třetihorách	třetihory	k1gFnPc6	třetihory
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
nerovnost	nerovnost	k1gFnSc1	nerovnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
prohlubovala	prohlubovat	k5eAaImAgFnS	prohlubovat
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
nynější	nynější	k2eAgFnSc3d1	nynější
Harrachovské	harrachovský	k2eAgFnSc3d1	Harrachovská
kotlině	kotlina	k1gFnSc3	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
neopakoval	opakovat	k5eNaImAgInS	opakovat
znovu	znovu	k6eAd1	znovu
i	i	k9	i
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mumlavský	Mumlavský	k2eAgInSc1d1	Mumlavský
vodopád	vodopád	k1gInSc1	vodopád
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
horská	horský	k2eAgFnSc1d1	horská
řeka	řeka	k1gFnSc1	řeka
Mumlava	Mumlava	k1gFnSc1	Mumlava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
Krkonošemi	Krkonoše	k1gFnPc7	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
Malé	Malé	k2eAgFnSc2d1	Malé
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Mumlavy	Mumlavy	k?	Mumlavy
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1030	[number]	k4	1030
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
u	u	k7c2	u
turistického	turistický	k2eAgInSc2d1	turistický
rozcestníku	rozcestník	k1gInSc2	rozcestník
Krakonošova	Krakonošův	k2eAgFnSc1d1	Krakonošova
snídaně	snídaně	k1gFnSc1	snídaně
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
.	.	kIx.	.
<g/>
Cestou	cesta	k1gFnSc7	cesta
překonává	překonávat	k5eAaImIp3nS	překonávat
až	až	k9	až
12	[number]	k4	12
m	m	kA	m
vysoké	vysoký	k2eAgNnSc1d1	vysoké
převýšení	převýšení	k1gNnSc1	převýšení
tvořené	tvořený	k2eAgNnSc1d1	tvořené
skalními	skalní	k2eAgInPc7d1	skalní
bloky	blok	k1gInPc7	blok
v	v	k7c6	v
žulovém	žulový	k2eAgNnSc6d1	žulové
řečišti	řečiště	k1gNnSc6	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
Mumlava	Mumlava	k1gFnSc1	Mumlava
protéká	protékat	k5eAaImIp3nS	protékat
Mumlavským	Mumlavský	k2eAgInSc7d1	Mumlavský
dolem	dol	k1gInSc7	dol
a	a	k8xC	a
po	po	k7c6	po
12,2	[number]	k4	12,2
km	km	kA	km
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Jizery	Jizera	k1gFnSc2	Jizera
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k9	tak
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Krkonošemi	Krkonoše	k1gFnPc7	Krkonoše
a	a	k8xC	a
Jizerskými	jizerský	k2eAgFnPc7d1	Jizerská
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Mumlavský	Mumlavský	k2eAgInSc1d1	Mumlavský
vodopád	vodopád	k1gInSc1	vodopád
má	mít	k5eAaImIp3nS	mít
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
stálý	stálý	k2eAgInSc1d1	stálý
tok	tok	k1gInSc1	tok
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
tak	tak	k9	tak
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvodnatějších	vodnatý	k2eAgMnPc2d3	vodnatý
vodopádu	vodopád	k1gInSc3	vodopád
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Nejmohutnější	mohutný	k2eAgInSc1d3	nejmohutnější
je	být	k5eAaImIp3nS	být
vodopád	vodopád	k1gInSc1	vodopád
při	při	k7c6	při
jarním	jarní	k2eAgNnSc6d1	jarní
tání	tání	k1gNnSc6	tání
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
proud	proud	k1gInSc1	proud
vody	voda	k1gFnPc4	voda
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
unikátní	unikátní	k2eAgNnSc1d1	unikátní
přírodní	přírodní	k2eAgNnSc1d1	přírodní
dílo	dílo	k1gNnSc1	dílo
tzv.	tzv.	kA	tzv.
ledopád	ledopád	k1gInSc1	ledopád
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
a	a	k8xC	a
kotle	kotel	k1gInPc1	kotel
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
také	také	k9	také
jako	jako	k8xC	jako
čertova	čertův	k2eAgNnPc1d1	Čertovo
oka	oko	k1gNnPc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
prohlubně	prohlubeň	k1gFnPc1	prohlubeň
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
působením	působení	k1gNnSc7	působení
silného	silný	k2eAgInSc2d1	silný
proudu	proud	k1gInSc2	proud
řeky	řeka	k1gFnSc2	řeka
na	na	k7c4	na
žulové	žulový	k2eAgInPc4d1	žulový
bloky	blok	k1gInPc4	blok
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
padající	padající	k2eAgFnSc1d1	padající
voda	voda	k1gFnSc1	voda
společně	společně	k6eAd1	společně
s	s	k7c7	s
kamínky	kamínek	k1gInPc7	kamínek
vyhloubila	vyhloubit	k5eAaPmAgFnS	vyhloubit
díru	díra	k1gFnSc4	díra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
nadále	nadále	k6eAd1	nadále
cirkulovala	cirkulovat	k5eAaImAgFnS	cirkulovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
útvary	útvar	k1gInPc1	útvar
zde	zde	k6eAd1	zde
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
až	až	k9	až
3	[number]	k4	3
m	m	kA	m
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
vodopádem	vodopád	k1gInSc7	vodopád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
řečišti	řečiště	k1gNnSc6	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
mohou	moct	k5eAaImIp3nP	moct
tyto	tento	k3xDgInPc4	tento
přirozeně	přirozeně	k6eAd1	přirozeně
vytvořené	vytvořený	k2eAgFnSc3d1	vytvořená
tůně	tůna	k1gFnSc3	tůna
lákat	lákat	k5eAaImF	lákat
turisty	turist	k1gMnPc4	turist
ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
<g/>
,	,	kIx,	,
bohužel	bohužel	k6eAd1	bohužel
tůně	tůně	k1gFnPc1	tůně
jsou	být	k5eAaImIp3nP	být
hluboké	hluboký	k2eAgFnPc1d1	hluboká
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
hladké	hladký	k2eAgFnPc1d1	hladká
a	a	k8xC	a
často	často	k6eAd1	často
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověku	člověk	k1gMnSc3	člověk
se	se	k3xPyFc4	se
nepodaří	podařit	k5eNaPmIp3nS	podařit
vylézt	vylézt	k5eAaPmF	vylézt
bez	bez	k7c2	bez
cizí	cizí	k2eAgFnSc2d1	cizí
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
i	i	k9	i
utopit	utopit	k5eAaPmF	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vodopádu	vodopád	k1gInSc2	vodopád
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
nepůvodní	původní	k2eNgFnPc1d1	nepůvodní
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
jej	on	k3xPp3gInSc4	on
především	především	k6eAd1	především
smrkovo-bukový	smrkovoukový	k2eAgInSc4d1	smrkovo-bukový
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
k	k	k7c3	k
přiblížit	přiblížit	k5eAaPmF	přiblížit
se	se	k3xPyFc4	se
původnímu	původní	k2eAgNnSc3d1	původní
složení	složení	k1gNnSc3	složení
lesa	les	k1gInSc2	les
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
škůdcům	škůdce	k1gMnPc3	škůdce
a	a	k8xC	a
negativním	negativní	k2eAgInPc3d1	negativní
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hojně	hojně	k6eAd1	hojně
vysazují	vysazovat	k5eAaImIp3nP	vysazovat
především	především	k9	především
listnaté	listnatý	k2eAgInPc1d1	listnatý
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
odolnější	odolný	k2eAgMnSc1d2	odolnější
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
buky	buk	k1gInPc1	buk
<g/>
,	,	kIx,	,
jasany	jasan	k1gInPc1	jasan
<g/>
,	,	kIx,	,
javory	javor	k1gInPc1	javor
nebo	nebo	k8xC	nebo
jilmy	jilm	k1gInPc1	jilm
<g/>
.	.	kIx.	.
</s>
<s>
Zástupce	zástupce	k1gMnPc4	zástupce
jehličnanů	jehličnan	k1gInPc2	jehličnan
jsou	být	k5eAaImIp3nP	být
jedle	jedle	k1gFnPc1	jedle
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zde	zde	k6eAd1	zde
také	také	k9	také
bývaly	bývat	k5eAaImAgFnP	bývat
původní	původní	k2eAgFnPc1d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
lišejníků	lišejník	k1gInPc2	lišejník
<g/>
,	,	kIx,	,
mechorostů	mechorost	k1gInPc2	mechorost
a	a	k8xC	a
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
dutohlávka	dutohlávka	k1gFnSc1	dutohlávka
sobí	sobí	k2eAgFnSc1d1	sobí
(	(	kIx(	(
<g/>
Cladonia	Cladonium	k1gNnSc2	Cladonium
rangiferina	rangiferino	k1gNnSc2	rangiferino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pukléřka	pukléřka	k1gFnSc1	pukléřka
islandská	islandský	k2eAgFnSc1d1	islandská
(	(	kIx(	(
<g/>
Cetraria	Cetrarium	k1gNnSc2	Cetrarium
islandica	islandic	k1gInSc2	islandic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ploník	ploník	k1gInSc1	ploník
ztenčený	ztenčený	k2eAgInSc1d1	ztenčený
(	(	kIx(	(
<g/>
Polytrichum	Polytrichum	k1gInSc1	Polytrichum
formosum	formosum	k1gInSc1	formosum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bělomech	bělomech	k1gInSc1	bělomech
sivý	sivý	k2eAgInSc1d1	sivý
(	(	kIx(	(
<g/>
Leucobryum	Leucobryum	k1gInSc1	Leucobryum
glaucum	glaucum	k1gInSc1	glaucum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
troudnatec	troudnatec	k1gMnSc1	troudnatec
pásovaný	pásovaný	k2eAgMnSc1d1	pásovaný
(	(	kIx(	(
<g/>
Fomitopsis	Fomitopsis	k1gInSc1	Fomitopsis
pinicola	pinicola	k1gFnSc1	pinicola
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Faunu	fauna	k1gFnSc4	fauna
tvoří	tvořit	k5eAaImIp3nS	tvořit
zejména	zejména	k9	zejména
lesní	lesní	k2eAgFnSc1d1	lesní
zvěř	zvěř	k1gFnSc1	zvěř
a	a	k8xC	a
ptactvo	ptactvo	k1gNnSc1	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovitě	jmenovitě	k6eAd1	jmenovitě
např.	např.	kA	např.
<g/>
:	:	kIx,	:
jezevec	jezevec	k1gMnSc1	jezevec
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Meles	Meles	k1gMnSc1	Meles
meles	meles	k1gMnSc1	meles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psík	psík	k1gMnSc1	psík
mývalovitý	mývalovitý	k2eAgMnSc1d1	mývalovitý
(	(	kIx(	(
<g/>
Nyctereutes	Nyctereutes	k1gMnSc1	Nyctereutes
procyonoides	procyonoides	k1gMnSc1	procyonoides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
liška	liška	k1gFnSc1	liška
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
vulpes	vulpes	k1gMnSc1	vulpes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kuna	kuna	k1gFnSc1	kuna
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Martes	Martes	k1gMnSc1	Martes
martes	martes	k1gMnSc1	martes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgFnSc2d1	divoká
(	(	kIx(	(
<g/>
Sus	Sus	k1gFnSc2	Sus
scrofa	scrof	k1gMnSc4	scrof
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelen	jelen	k1gMnSc1	jelen
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Cervus	Cervus	k1gMnSc1	Cervus
elaphus	elaphus	k1gMnSc1	elaphus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srnec	srnec	k1gMnSc1	srnec
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Capreolus	Capreolus	k1gMnSc1	Capreolus
capreolus	capreolus	k1gMnSc1	capreolus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zajíc	zajíc	k1gMnSc1	zajíc
polní	polní	k2eAgMnSc1d1	polní
(	(	kIx(	(
<g/>
Lepus	Lepus	k1gMnSc1	Lepus
europaeus	europaeus	k1gMnSc1	europaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
králík	králík	k1gMnSc1	králík
divoký	divoký	k2eAgMnSc1d1	divoký
(	(	kIx(	(
<g/>
Oryctolagus	Oryctolagus	k1gMnSc1	Oryctolagus
cuniculus	cuniculus	k1gMnSc1	cuniculus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veverka	veverka	k1gFnSc1	veverka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Sciurus	Sciurus	k1gInSc1	Sciurus
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Zástupci	zástupce	k1gMnPc1	zástupce
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
<g/>
:	:	kIx,	:
sojka	sojka	k1gFnSc1	sojka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Garrulus	Garrulus	k1gMnSc1	Garrulus
glandarius	glandarius	k1gMnSc1	glandarius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
straka	straka	k1gFnSc1	straka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Pica	Pic	k2eAgFnSc1d1	Pica
pica	pica	k1gFnSc1	pica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
datel	datel	k1gMnSc1	datel
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Dryocopus	Dryocopus	k1gMnSc1	Dryocopus
martius	martius	k1gMnSc1	martius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sluka	sluka	k1gFnSc1	sluka
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Scolopax	Scolopax	k1gInSc1	Scolopax
rusticola	rusticola	k1gFnSc1	rusticola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tetřev	tetřev	k1gMnSc1	tetřev
<g />
.	.	kIx.	.
</s>
<s>
hlušec	hlušec	k1gMnSc1	hlušec
(	(	kIx(	(
<g/>
Tetrao	Tetrao	k1gMnSc1	Tetrao
urogallus	urogallus	k1gMnSc1	urogallus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tetřívek	tetřívek	k1gMnSc1	tetřívek
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Tetrao	Tetrao	k6eAd1	Tetrao
tetrix	tetrix	k1gInSc1	tetrix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
peregrinus	peregrinus	k1gMnSc1	peregrinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jestřáb	jestřáb	k1gMnSc1	jestřáb
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Accipiter	Accipiter	k1gInSc1	Accipiter
gentilis	gentilis	k1gFnSc2	gentilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Bubo	Bubo	k1gMnSc1	Bubo
bubo	bubo	k1gMnSc1	bubo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
puštík	puštík	k1gMnSc1	puštík
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Strix	Strix	k1gInSc1	Strix
aluco	aluco	k1gNnSc1	aluco
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Mumlavský	Mumlavský	k2eAgInSc1d1	Mumlavský
vodopád	vodopád	k1gInSc1	vodopád
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvyhledávanějších	vyhledávaný	k2eAgInPc2d3	nejvyhledávanější
turistických	turistický	k2eAgInPc2d1	turistický
cílů	cíl	k1gInPc2	cíl
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
Krkonošského	krkonošský	k2eAgInSc2d1	krkonošský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Labským	labský	k2eAgInSc7d1	labský
a	a	k8xC	a
Pančavským	Pančavský	k2eAgInSc7d1	Pančavský
vodopádem	vodopád	k1gInSc7	vodopád
nejnavštěvovanější	navštěvovaný	k2eAgInSc1d3	nejnavštěvovanější
vodopád	vodopád	k1gInSc1	vodopád
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
přístupný	přístupný	k2eAgInSc1d1	přístupný
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
ale	ale	k8xC	ale
trasa	trasa	k1gFnSc1	trasa
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
i	i	k9	i
pro	pro	k7c4	pro
vozíčkáře	vozíčkář	k1gMnPc4	vozíčkář
či	či	k8xC	či
maminky	maminka	k1gFnPc4	maminka
s	s	k7c7	s
kočárky	kočárek	k1gInPc7	kočárek
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
zde	zde	k6eAd1	zde
široká	široký	k2eAgFnSc1d1	široká
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
zákaz	zákaz	k1gInSc4	zákaz
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
budete	být	k5eAaImBp2nP	být
pokračovat	pokračovat	k5eAaImF	pokračovat
dále	daleko	k6eAd2	daleko
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
Mumlavy	Mumlavy	k?	Mumlavy
<g/>
,	,	kIx,	,
příjemnou	příjemný	k2eAgFnSc7d1	příjemná
nenáročnou	náročný	k2eNgFnSc7d1	nenáročná
procházkou	procházka	k1gFnSc7	procházka
lze	lze	k6eAd1	lze
dorazit	dorazit	k5eAaPmF	dorazit
i	i	k9	i
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
vodopádům	vodopád	k1gInPc3	vodopád
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
už	už	k9	už
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menším	menšit	k5eAaImIp1nS	menšit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
postavena	postaven	k2eAgFnSc1d1	postavena
Mumlavská	Mumlavský	k2eAgFnSc1d1	Mumlavská
bouda	bouda	k1gFnSc1	bouda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zde	zde	k6eAd1	zde
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
hrabě	hrabě	k1gMnSc1	hrabě
Jan	Jan	k1gMnSc1	Jan
Harrach	Harrach	k1gMnSc1	Harrach
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
kvůli	kvůli	k7c3	kvůli
vodopádu	vodopád	k1gInSc3	vodopád
<g/>
.	.	kIx.	.
<g/>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
hájenku	hájenka	k1gFnSc4	hájenka
nyní	nyní	k6eAd1	nyní
objekt	objekt	k1gInSc1	objekt
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
výletní	výletní	k2eAgFnSc1d1	výletní
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Mumlavských	Mumlavský	k2eAgFnPc2d1	Mumlavská
vodopádům	vodopád	k1gInPc3	vodopád
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
vydat	vydat	k5eAaPmF	vydat
hned	hned	k6eAd1	hned
několika	několik	k4yIc7	několik
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejkratší	krátký	k2eAgMnSc1d3	nejkratší
začíná	začínat	k5eAaImIp3nS	začínat
od	od	k7c2	od
Harrachovského	harrachovský	k2eAgNnSc2d1	harrachovské
autobusového	autobusový	k2eAgNnSc2d1	autobusové
nádraží	nádraží	k1gNnSc2	nádraží
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
Harrachovské	harrachovský	k2eAgFnSc6d1	Harrachovská
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
značena	značit	k5eAaImNgFnS	značit
modrou	modrý	k2eAgFnSc7d1	modrá
turistickou	turistický	k2eAgFnSc7d1	turistická
značkou	značka	k1gFnSc7	značka
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
7,7	[number]	k4	7,7
km	km	kA	km
s	s	k7c7	s
převýšením	převýšení	k1gNnSc7	převýšení
pouhých	pouhý	k2eAgInPc2d1	pouhý
163	[number]	k4	163
m.	m.	k?	m.
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
vyjít	vyjít	k5eAaPmF	vyjít
z	z	k7c2	z
Rýžoviště	rýžoviště	k1gNnSc2	rýžoviště
nebo	nebo	k8xC	nebo
od	od	k7c2	od
Hornického	hornický	k2eAgNnSc2d1	Hornické
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
také	také	k9	také
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
návštěvu	návštěva	k1gFnSc4	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
vydat	vydat	k5eAaPmF	vydat
se	se	k3xPyFc4	se
od	od	k7c2	od
harrachovské	harrachovský	k2eAgFnSc2d1	Harrachovská
sklárny	sklárna	k1gFnSc2	sklárna
po	po	k7c6	po
červené	červený	k2eAgFnSc6d1	červená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
turistickými	turistický	k2eAgMnPc7d1	turistický
lákadly	lákadlo	k1gNnPc7	lákadlo
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Mumlavských	Mumlavský	k2eAgInPc2d1	Mumlavský
vodopádů	vodopád	k1gInPc2	vodopád
je	být	k5eAaImIp3nS	být
Hornické	hornický	k2eAgNnSc1d1	Hornické
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
vodopádům	vodopád	k1gInPc3	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Harrachova	Harrachov	k1gInSc2	Harrachov
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
navštívit	navštívit	k5eAaPmF	navštívit
sklářské	sklářský	k2eAgNnSc4d1	sklářské
muzeum	muzeum	k1gNnSc4	muzeum
nebo	nebo	k8xC	nebo
chatu	chata	k1gFnSc4	chata
Šindelku	šindelka	k1gFnSc4	šindelka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
lesnicko-myslivecká	lesnickoyslivecký	k2eAgFnSc1d1	lesnicko-myslivecký
expozice	expozice	k1gFnSc1	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Čertova	čertův	k2eAgFnSc1d1	Čertova
hora	hora	k1gFnSc1	hora
s	s	k7c7	s
vyhlídkou	vyhlídka	k1gFnSc7	vyhlídka
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
úchvatný	úchvatný	k2eAgInSc4d1	úchvatný
rozhled	rozhled	k1gInSc4	rozhled
do	do	k7c2	do
okolní	okolní	k2eAgFnSc2d1	okolní
krajiny	krajina	k1gFnSc2	krajina
také	také	k9	také
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
návštěvu	návštěva	k1gFnSc4	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
dál	daleko	k6eAd2	daleko
můžete	moct	k5eAaImIp2nP	moct
navštívit	navštívit	k5eAaPmF	navštívit
Labský	labský	k2eAgInSc4d1	labský
důl	důl	k1gInSc4	důl
<g/>
,	,	kIx,	,
Labskou	labský	k2eAgFnSc4d1	Labská
louku	louka	k1gFnSc4	louka
<g/>
,	,	kIx,	,
pramen	pramen	k1gInSc4	pramen
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc4	Labe
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
vodopády	vodopád	k1gInPc4	vodopád
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
s	s	k7c7	s
v	v	k7c6	v
KRNAPu	KRNAPum	k1gNnSc6	KRNAPum
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mumlavský	Mumlavský	k2eAgInSc4d1	Mumlavský
vodopád	vodopád	k1gInSc4	vodopád
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
