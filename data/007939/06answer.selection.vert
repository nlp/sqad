<s>
Organizace	organizace	k1gFnSc1	organizace
má	mít	k5eAaImIp3nS	mít
195	[number]	k4	195
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInPc4d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
autonomní	autonomní	k2eAgNnSc4d1	autonomní
území	území	k1gNnSc4	území
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
přijetí	přijetí	k1gNnSc2	přijetí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
.	.	kIx.	.
</s>
