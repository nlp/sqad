<p>
<s>
Móric	Móric	k1gMnSc1	Móric
Pálfi	Pálf	k1gFnSc2	Pálf
(	(	kIx(	(
<g/>
také	také	k9	také
Pálffy	Pálff	k1gMnPc4	Pálff
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
-	-	kIx~	-
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
a	a	k8xC	a
místodržitel	místodržitel	k1gMnSc1	místodržitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
povolil	povolit	k5eAaPmAgInS	povolit
založení	založení	k1gNnSc4	založení
Matice	matice	k1gFnSc2	matice
slovenské	slovenský	k2eAgFnSc2d1	slovenská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příslušník	příslušník	k1gMnSc1	příslušník
červenokamenské	červenokamenský	k2eAgFnSc2d1	červenokamenský
linie	linie	k1gFnSc2	linie
starší	starý	k2eAgFnPc1d2	starší
větve	větev	k1gFnPc1	větev
Pálfiovců	Pálfiovec	k1gMnPc2	Pálfiovec
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
postava	postava	k1gFnSc1	postava
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
stoupenec	stoupenec	k1gMnSc1	stoupenec
Habsburků	Habsburk	k1gInPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Móric	Mórice	k1gFnPc2	Mórice
Pálfi	Pálfi	k1gNnPc2	Pálfi
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Pálffyové	Pálffyové	k2eAgFnSc1d1	Pálffyové
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Portrét	portrét	k1gInSc1	portrét
na	na	k7c4	na
EUROPEANA	EUROPEANA	kA	EUROPEANA
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Červený	červený	k2eAgInSc1d1	červený
Kameň	kamenit	k5eAaImRp2nS	kamenit
</s>
</p>
