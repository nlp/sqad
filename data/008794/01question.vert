<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
hrabě	hrabě	k1gMnSc1	hrabě
a	a	k8xC	a
místodržitel	místodržitel	k1gMnSc1	místodržitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
povolil	povolit	k5eAaPmAgInS	povolit
založení	založení	k1gNnSc4	založení
Matice	matice	k1gFnSc2	matice
slovenské	slovenský	k2eAgFnSc2d1	slovenská
<g/>
?	?	kIx.	?
</s>
