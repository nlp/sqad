<s>
Vychodil	vychodit	k5eAaPmAgInS,k5eAaImAgInS
reálnou	reálný	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Jičíně	Jičín	k1gInSc6
<g/>
,	,	kIx,
načež	načež	k6eAd1
odešel	odejít	k5eAaPmAgMnS
na	na	k7c4
studia	studio	k1gNnPc4
architektury	architektura	k1gFnSc2
při	při	k7c6
ČVUT	ČVUT	kA
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
navštěvoval	navštěvovat	k5eAaImAgInS
ateliér	ateliér	k1gInSc1
kreslení	kreslení	k1gNnSc2
vedený	vedený	k2eAgInSc1d1
malířem	malíř	k1gMnSc7
Oldřichem	Oldřich	k1gMnSc7
Blažíčkem	Blažíček	k1gMnSc7
a	a	k8xC
Adolfem	Adolf	k1gMnSc7
Liebscherem	Liebscher	k1gMnSc7
mladším	mladý	k2eAgMnSc7d2
<g/>
.	.	kIx.
</s>