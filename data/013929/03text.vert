<s>
František	František	k1gMnSc1
Max	Max	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Max	Max	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1895	#num#	k4
LibáňRakousko-Uhersko	LibáňRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
73	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c4
PrazeAkademie	PrazeAkademie	k1gFnPc4
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
ilustrátor	ilustrátor	k1gMnSc1
a	a	k8xC
architekt	architekt	k1gMnSc1
Podpis	podpis	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Max	Max	k1gMnSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1895	#num#	k4
Libáň	Libáň	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1969	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
vedut	veduta	k1gFnPc2
a	a	k8xC
krajin	krajina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Vychodil	vychodit	k5eAaPmAgInS,k5eAaImAgInS
reálnou	reálný	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Jičíně	Jičín	k1gInSc6
<g/>
,	,	kIx,
načež	načež	k6eAd1
odešel	odejít	k5eAaPmAgMnS
na	na	k7c4
studia	studio	k1gNnPc4
architektury	architektura	k1gFnSc2
při	při	k7c6
ČVUT	ČVUT	kA
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
navštěvoval	navštěvovat	k5eAaImAgInS
ateliér	ateliér	k1gInSc1
kreslení	kreslení	k1gNnSc2
vedený	vedený	k2eAgInSc1d1
malířem	malíř	k1gMnSc7
Oldřichem	Oldřich	k1gMnSc7
Blažíčkem	Blažíček	k1gMnSc7
a	a	k8xC
Adolfem	Adolf	k1gMnSc7
Liebscherem	Liebscher	k1gMnSc7
mladším	mladý	k2eAgMnSc7d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souběžně	souběžně	k6eAd1
začal	začít	k5eAaPmAgInS
studovat	studovat	k5eAaImF
na	na	k7c4
Akademii	akademie	k1gFnSc4
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Jana	Jan	k1gMnSc2
Preislera	Preisler	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhy	záhy	k6eAd1
si	se	k3xPyFc3
vyvinul	vyvinout	k5eAaPmAgInS
svébytný	svébytný	k2eAgInSc4d1
<g/>
,	,	kIx,
lehce	lehko	k6eAd1
rozpoznatelný	rozpoznatelný	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
života	život	k1gInSc2
již	již	k6eAd1
příliš	příliš	k6eAd1
nezměnil	změnit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracoval	pracovat	k5eAaImAgInS
s	s	k7c7
pastózní	pastózní	k2eAgFnSc7d1
malbou	malba	k1gFnSc7
a	a	k8xC
výraznou	výrazný	k2eAgFnSc7d1
barevností	barevnost	k1gFnSc7
olejových	olejový	k2eAgFnPc2d1
barev	barva	k1gFnPc2
<g/>
;	;	kIx,
široké	široký	k2eAgInPc4d1
tahy	tah	k1gInPc4
štětcem	štětec	k1gInSc7
či	či	k8xC
špachtlí	špachtle	k1gFnSc7
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
typické	typický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
do	do	k7c2
malby	malba	k1gFnSc2
ryl	rýt	k5eAaImAgMnS
i	i	k9
obráceným	obrácený	k2eAgInSc7d1
koncem	konec	k1gInSc7
štětce	štětec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
tvorbu	tvorba	k1gFnSc4
je	být	k5eAaImIp3nS
charakteristický	charakteristický	k2eAgInSc1d1
rychlý	rychlý	k2eAgInSc1d1
<g/>
,	,	kIx,
expresivní	expresivní	k2eAgInSc1d1
styl	styl	k1gInSc1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
podtrhuje	podtrhovat	k5eAaImIp3nS
i	i	k9
náležitá	náležitý	k2eAgFnSc1d1
barevnost	barevnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ač	ač	k8xS
žil	žít	k5eAaImAgMnS
a	a	k8xC
tvořil	tvořit	k5eAaImAgInS
převážně	převážně	k6eAd1
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
,	,	kIx,
často	často	k6eAd1
se	se	k3xPyFc4
vracel	vracet	k5eAaImAgMnS
do	do	k7c2
rodného	rodný	k2eAgNnSc2d1
Jičínska	Jičínsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
zpracování	zpracování	k1gNnSc4
tamních	tamní	k2eAgInPc2d1
námětů	námět	k1gInPc2
<g/>
:	:	kIx,
Libáňský	Libáňský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
cukrovar	cukrovar	k1gInSc1
<g/>
,	,	kIx,
hrad	hrad	k1gInSc1
a	a	k8xC
zámek	zámek	k1gInSc1
Staré	Staré	k2eAgInPc1d1
Hrady	hrad	k1gInPc1
se	se	k3xPyFc4
opakují	opakovat	k5eAaImIp3nP
napříč	napříč	k7c7
celou	celý	k2eAgFnSc7d1
jeho	jeho	k3xOp3gFnSc7
tvorbou	tvorba	k1gFnSc7
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
pražské	pražský	k2eAgFnSc2d1
veduty	veduta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohledy	pohled	k1gInPc7
do	do	k7c2
ulic	ulice	k1gFnPc2
Kampy	Kampa	k1gFnSc2
<g/>
,	,	kIx,
Řásnovky	Řásnovka	k1gFnSc2
<g/>
,	,	kIx,
Malé	Malé	k2eAgFnSc2d1
Strany	strana	k1gFnSc2
či	či	k8xC
Hradčan	Hradčany	k1gInPc2
odráží	odrážet	k5eAaImIp3nS
i	i	k9
jeho	jeho	k3xOp3gInSc1
setrvalý	setrvalý	k2eAgInSc1d1
zájem	zájem	k1gInSc1
o	o	k7c4
architekturu	architektura	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
nicméně	nicméně	k8xC
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
již	již	k9
profesně	profesně	k6eAd1
nevěnoval	věnovat	k5eNaImAgMnS
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
se	se	k3xPyFc4
upnul	upnout	k5eAaPmAgInS
na	na	k7c4
malířskou	malířský	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gInSc3
přechodu	přechod	k1gInSc3
od	od	k7c2
architektury	architektura	k1gFnSc2
k	k	k7c3
malířství	malířství	k1gNnSc3
přispěla	přispět	k5eAaPmAgFnS
také	také	k9
ekonomická	ekonomický	k2eAgFnSc1d1
krize	krize	k1gFnSc1
a	a	k8xC
smrt	smrt	k1gFnSc1
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
zaměstnavatele	zaměstnavatel	k1gMnSc2
Otakara	Otakar	k1gMnSc2
Nekvasila	kvasit	k5eNaImAgFnS
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
na	na	k7c6
počátku	počátek	k1gInSc6
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
odešel	odejít	k5eAaPmAgInS
ze	z	k7c2
stavební	stavební	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Nekvasil	kvasit	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc4
výstavu	výstava	k1gFnSc4
uspořádal	uspořádat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
30	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
často	často	k6eAd1
vystavoval	vystavovat	k5eAaImAgInS
zejména	zejména	k9
v	v	k7c6
pražských	pražský	k2eAgFnPc6d1
galeriích	galerie	k1gFnPc6
<g/>
,	,	kIx,
mj.	mj.	kA
v	v	k7c6
Mazačově	mazačův	k2eAgFnSc6d1
galerii	galerie	k1gFnSc6
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
v	v	k7c6
galerii	galerie	k1gFnSc6
Rubešově	Rubešův	k2eAgFnSc6d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
fungovala	fungovat	k5eAaImAgFnS
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
Národní	národní	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
č.	č.	k?
37	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
roce	rok	k1gInSc6
1938	#num#	k4
mj.	mj.	kA
kritika	kritika	k1gFnSc1
ocenila	ocenit	k5eAaPmAgFnS
"	"	kIx"
<g/>
hybný	hybný	k2eAgInSc4d1
a	a	k8xC
citlivý	citlivý	k2eAgInSc4d1
rukopis	rukopis	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
rozehrává	rozehrávat	k5eAaImIp3nS
v	v	k7c4
malebné	malebný	k2eAgFnPc4d1
imprese	imprese	k1gFnPc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
František	František	k1gMnSc1
Max	Max	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
poměrně	poměrně	k6eAd1
stranou	stranou	k6eAd1
hlavních	hlavní	k2eAgInPc2d1
uměleckých	umělecký	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
svérázným	svérázný	k2eAgInSc7d1
malířem-samotářem	malířem-samotář	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
pevně	pevně	k6eAd1
nepřipojil	připojit	k5eNaPmAgInS
k	k	k7c3
žádné	žádný	k3yNgFnSc3
umělecké	umělecký	k2eAgFnSc3d1
skupině	skupina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
byl	být	k5eAaImAgInS
už	už	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
prodávaným	prodávaný	k2eAgInSc7d1
a	a	k8xC
oceňovaným	oceňovaný	k2eAgMnSc7d1
malířem	malíř	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
vydalo	vydat	k5eAaPmAgNnS
Regionální	regionální	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
a	a	k8xC
galerie	galerie	k1gFnSc1
Jičín	Jičín	k1gInSc4
první	první	k4xOgFnSc4
obsáhlejší	obsáhlý	k2eAgFnSc4d2
publikaci	publikace	k1gFnSc4
věnovanou	věnovaný	k2eAgFnSc4d1
přehledu	přehled	k1gInSc6
jeho	jeho	k3xOp3gFnSc2
tvorby	tvorba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Právě	právě	k9
toto	tento	k3xDgNnSc1
muzeum	muzeum	k1gNnSc1
opatruje	opatrovat	k5eAaImIp3nS
patnáctku	patnáctka	k1gFnSc4
Maxových	Maxových	k2eAgInPc2d1
olejů	olej	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
bývají	bývat	k5eAaImIp3nP
průběžně	průběžně	k6eAd1
vystavovány	vystavován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
byla	být	k5eAaImAgFnS
Františku	František	k1gMnSc3
Maxovi	Max	k1gMnSc3
uspořádána	uspořádat	k5eAaPmNgFnS
od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
prostorách	prostora	k1gFnPc6
Zámecké	zámecký	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
v	v	k7c6
Jičíně	Jičín	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SOA	SOA	kA
Zámrsk	Zámrsk	k1gInSc1
<g/>
,	,	kIx,
Matrika	matrika	k1gFnSc1
narozených	narozený	k2eAgFnPc2d1
1876-1896	1876-1896	k4
v	v	k7c6
Libáni	Libán	k1gMnPc1
,	,	kIx,
sign	signum	k1gNnPc2
<g/>
.37	.37	k4
<g/>
-	-	kIx~
<g/>
340	#num#	k4
<g/>
,	,	kIx,
ukn	ukn	k?
5220	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.353	.353	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HAKENOVÁ	Hakenová	k1gFnSc1
<g/>
,	,	kIx,
Jarmila	Jarmila	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobnost	osobnost	k1gFnSc1
František	František	k1gMnSc1
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
Hurá	hurá	k0
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červen	červen	k1gInSc1
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NOMINANDUM	NOMINANDUM	kA
<g/>
,	,	kIx,
Norbert	Norbert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amplion	amplion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polední	polední	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1944	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
18	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
60	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NOMINANDUM	NOMINANDUM	kA
<g/>
,	,	kIx,
Norbert	Norbert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
v	v	k7c6
rozhlase	rozhlas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práce	práce	k1gFnSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1949	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
73	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NOMINANDUM	NOMINANDUM	kA
<g/>
,	,	kIx,
Norbert	Norbert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulturní	kulturní	k2eAgFnSc1d1
rubrika	rubrika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práce	práce	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1949	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
50	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
K	K	kA
<g/>
,	,	kIx,
r.	r.	kA
Z	z	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
venkova	venkov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polední	polední	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1938	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
91	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NOMINANDUM	NOMINANDUM	kA
<g/>
,	,	kIx,
Norbert	Norbert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Synek	Synek	k1gMnSc1
Českého	český	k2eAgInSc2d1
ráje	ráj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
zamiloval	zamilovat	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venkov	venkov	k1gInSc1
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
36	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
139	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
TOBOLKOVÁ	TOBOLKOVÁ	kA
<g/>
,	,	kIx,
Radka	Radka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalog	katalog	k1gInSc1
uměleckých	umělecký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
i	i	k8xC
vzpomínky	vzpomínka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jičínské	jičínský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
představilo	představit	k5eAaPmAgNnS
nové	nový	k2eAgFnPc4d1
publikace	publikace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jičínský	jičínský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
František	František	k1gMnSc1
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
www.muzeumhry.cz	www.muzeumhry.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
abART	abART	k?
<g/>
:	:	kIx,
František	František	k1gMnSc1
Max	Max	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1080918	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5715	#num#	k4
1222	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83823396	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
