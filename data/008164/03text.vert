<p>
<s>
Cremona	Cremona	k1gFnSc1	Cremona
(	(	kIx(	(
<g/>
cremonsky	cremonsky	k6eAd1	cremonsky
Cremuna	Cremuna	k1gFnSc1	Cremuna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
italské	italský	k2eAgNnSc4d1	italské
historické	historický	k2eAgNnSc4d1	historické
město	město	k1gNnSc4	město
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Lombardie	Lombardie	k1gFnSc2	Lombardie
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Pád	Pád	k1gInSc1	Pád
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
71	[number]	k4	71
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
proslula	proslout	k5eAaPmAgFnS	proslout
především	především	k9	především
výrobou	výroba	k1gFnSc7	výroba
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejím	její	k3xOp3gMnPc3	její
rodákům	rodák	k1gMnPc3	rodák
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Claudio	Claudio	k6eAd1	Claudio
Monteverdi	Monteverd	k1gMnPc1	Monteverd
(	(	kIx(	(
<g/>
1567	[number]	k4	1567
<g/>
–	–	k?	–
<g/>
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
a	a	k8xC	a
věhlasný	věhlasný	k2eAgMnSc1d1	věhlasný
houslař	houslař	k1gMnSc1	houslař
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivare	k1gFnSc4	Stradivare
(	(	kIx(	(
<g/>
1644	[number]	k4	1644
<g/>
–	–	k?	–
<g/>
1737	[number]	k4	1737
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úsloví	úsloví	k1gNnSc2	úsloví
==	==	k?	==
</s>
</p>
<p>
<s>
Cremona	Cremona	k1gFnSc1	Cremona
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
latinském	latinský	k2eAgNnSc6d1	latinské
úsloví	úsloví	k1gNnSc6	úsloví
Unus	Unusa	k1gFnPc2	Unusa
Petrus	Petrus	k1gMnSc1	Petrus
in	in	k?	in
Roma	Rom	k1gMnSc4	Rom
<g/>
,	,	kIx,	,
unus	unus	k6eAd1	unus
portus	portus	k1gInSc1	portus
in	in	k?	in
Ancona	Ancona	k1gFnSc1	Ancona
<g/>
,	,	kIx,	,
una	una	k?	una
turris	turris	k1gInSc1	turris
in	in	k?	in
Cremona	Cremon	k1gMnSc2	Cremon
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Jeden	jeden	k4xCgMnSc1	jeden
Petr	Petr	k1gMnSc1	Petr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Ankoně	Ankona	k1gFnSc6	Ankona
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
věž	věž	k1gFnSc1	věž
v	v	k7c6	v
Cremoně	Cremona	k1gFnSc6	Cremona
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c2	za
doby	doba	k1gFnSc2	doba
největšího	veliký	k2eAgInSc2d3	veliký
věhlasu	věhlas	k1gInSc2	věhlas
rakovnického	rakovnický	k2eAgNnSc2d1	rakovnické
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
doplněno	doplnit	k5eAaPmNgNnS	doplnit
o	o	k7c6	o
"	"	kIx"	"
<g/>
...	...	k?	...
una	una	k?	una
ceres	ceres	k1gInSc1	ceres
Raconae	Raconae	k1gFnSc1	Raconae
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
česky	česky	k6eAd1	česky
Jeden	jeden	k4xCgMnSc1	jeden
Petr	Petr	k1gMnSc1	Petr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Anconě	Ancona	k1gFnSc6	Ancona
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
věž	věž	k1gFnSc1	věž
v	v	k7c6	v
Cremoně	Cremon	k1gInSc6	Cremon
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
pivo	pivo	k1gNnSc1	pivo
v	v	k7c6	v
Rakovníku	Rakovník	k1gInSc6	Rakovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Cremona	Cremona	k1gFnSc1	Cremona
existovala	existovat	k5eAaImAgFnS	existovat
už	už	k6eAd1	už
jako	jako	k9	jako
keltská	keltský	k2eAgFnSc1d1	keltská
(	(	kIx(	(
<g/>
galská	galský	k2eAgFnSc1d1	galská
<g/>
)	)	kIx)	)
osada	osada	k1gFnSc1	osada
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
218	[number]	k4	218
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jako	jako	k8xS	jako
římská	římský	k2eAgFnSc1d1	římská
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
opakovaně	opakovaně	k6eAd1	opakovaně
devastována	devastovat	k5eAaBmNgFnS	devastovat
barbarskými	barbarský	k2eAgInPc7d1	barbarský
nájezdy	nájezd	k1gInPc7	nájezd
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k8xC	pak
válkami	válka	k1gFnPc7	válka
mezi	mezi	k7c7	mezi
císařskými	císařský	k2eAgNnPc7d1	císařské
a	a	k8xC	a
papežskými	papežský	k2eAgNnPc7d1	papežské
vojsky	vojsko	k1gNnPc7	vojsko
(	(	kIx(	(
<g/>
načas	načas	k6eAd1	načas
byla	být	k5eAaImAgFnS	být
i	i	k9	i
hlavním	hlavní	k2eAgInSc7d1	hlavní
opěrným	opěrný	k2eAgInSc7d1	opěrný
bodem	bod	k1gInSc7	bod
Friedricha	Friedrich	k1gMnSc2	Friedrich
I.	I.	kA	I.
Barbarossy	Barbarossa	k1gMnPc7	Barbarossa
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gMnSc4	jeho
vnuka	vnuk	k1gMnSc4	vnuk
Friedricha	Friedrich	k1gMnSc4	Friedrich
II	II	kA	II
<g/>
.	.	kIx.	.
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1334	[number]	k4	1334
však	však	k9	však
připadla	připadnout	k5eAaPmAgFnS	připadnout
Milánu	Milán	k1gInSc3	Milán
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
sdílela	sdílet	k5eAaImAgFnS	sdílet
společné	společný	k2eAgInPc4d1	společný
osudy	osud	k1gInPc4	osud
až	až	k9	až
do	do	k7c2	do
sjednocení	sjednocení	k1gNnSc2	sjednocení
Itálie	Itálie	k1gFnSc2	Itálie
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
Palazzo	Palazza	k1gFnSc5	Palazza
Comunale	Comunale	k1gMnPc3	Comunale
<g/>
)	)	kIx)	)
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1206-1246	[number]	k4	1206-1246
(	(	kIx(	(
<g/>
při	při	k7c6	při
renesanční	renesanční	k2eAgFnSc6d1	renesanční
přestavbě	přestavba	k1gFnSc6	přestavba
dostal	dostat	k5eAaPmAgMnS	dostat
nynější	nynější	k2eAgFnSc4d1	nynější
terakotovou	terakotový	k2eAgFnSc4d1	Terakotová
podobu	podoba	k1gFnSc4	podoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
uloženy	uložit	k5eAaPmNgFnP	uložit
čtyři	čtyři	k4xCgFnPc1	čtyři
vzácné	vzácný	k2eAgFnPc1d1	vzácná
housle	housle	k1gFnPc1	housle
–	–	k?	–
od	od	k7c2	od
B.	B.	kA	B.
Guarneriho	Guarneri	k1gMnSc2	Guarneri
del	del	k?	del
Gesù	Gesù	k1gMnSc2	Gesù
<g/>
,	,	kIx,	,
od	od	k7c2	od
Andrey	Andrea	k1gFnSc2	Andrea
Amatiho	Amati	k1gMnSc2	Amati
<g/>
,	,	kIx,	,
Niccola	Niccola	k1gFnSc1	Niccola
Amatiho	Amati	k1gMnSc2	Amati
a	a	k8xC	a
Antonia	Antonio	k1gMnSc2	Antonio
Stradivariho	Stradivari	k1gMnSc2	Stradivari
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
katedrály	katedrála	k1gFnSc2	katedrála
jsou	být	k5eAaImIp3nP	být
arkády	arkáda	k1gFnPc1	arkáda
Loggie	loggie	k1gFnPc1	loggie
dei	dei	k?	dei
Militi	Milit	k1gMnPc1	Milit
(	(	kIx(	(
<g/>
lodžie	lodžie	k1gFnSc1	lodžie
vojáků	voják	k1gMnPc2	voják
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
scházela	scházet	k5eAaImAgFnS	scházet
městská	městský	k2eAgFnSc1d1	městská
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
válečný	válečný	k2eAgInSc1d1	válečný
pomník	pomník	k1gInSc1	pomník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stradivariho	Stradivarize	k6eAd1	Stradivarize
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Palazzo	Palazza	k1gFnSc5	Palazza
Affaitati	Affaitati	k1gMnSc6	Affaitati
(	(	kIx(	(
<g/>
1561	[number]	k4	1561
<g/>
)	)	kIx)	)
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Via	via	k7c4	via
Ugolani	Ugolan	k1gMnPc1	Ugolan
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sbírku	sbírka	k1gFnSc4	sbírka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
700	[number]	k4	700
houslí	housle	k1gFnPc2	housle
<g/>
,	,	kIx,	,
viol	viola	k1gFnPc2	viola
<g/>
,	,	kIx,	,
violoncell	violoncello	k1gNnPc2	violoncello
a	a	k8xC	a
kytar	kytara	k1gFnPc2	kytara
slavných	slavný	k2eAgMnPc2d1	slavný
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
akustických	akustický	k2eAgInPc2d1	akustický
diagramů	diagram	k1gInPc2	diagram
a	a	k8xC	a
vyřezávaných	vyřezávaný	k2eAgInPc2d1	vyřezávaný
ornamentů	ornament	k1gInPc2	ornament
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
budově	budova	k1gFnSc6	budova
sídlí	sídlet	k5eAaImIp3nS	sídlet
též	též	k9	též
Pinakotéka	pinakotéka	k1gFnSc1	pinakotéka
s	s	k7c7	s
díly	díl	k1gInPc7	díl
domácích	domácí	k2eAgMnPc2d1	domácí
i	i	k8xC	i
evropských	evropský	k2eAgMnPc2d1	evropský
mistrů	mistr	k1gMnPc2	mistr
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kostely	kostel	k1gInPc4	kostel
===	===	k?	===
</s>
</p>
<p>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Duomo	Duoma	k1gFnSc5	Duoma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
lombardsko-románském	lombardskoománský	k2eAgInSc6d1	lombardsko-románský
stylu	styl	k1gInSc6	styl
počátkem	počátkem	k7c2	počátkem
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
dokončena	dokončit	k5eAaPmNgFnS	dokončit
renesančně	renesančně	k6eAd1	renesančně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
i	i	k9	i
mramorová	mramorový	k2eAgFnSc1d1	mramorová
fasáda	fasáda	k1gFnSc1	fasáda
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
růžicí	růžice	k1gFnSc7	růžice
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
jemných	jemný	k2eAgInPc2d1	jemný
detailů	detail	k1gInPc2	detail
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
malého	malý	k1gMnSc2	malý
portika	portika	k1gFnSc1	portika
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
dómu	dóm	k1gInSc2	dóm
je	být	k5eAaImIp3nS	být
vyzdoben	vyzdoben	k2eAgInSc1d1	vyzdoben
freskami	freska	k1gFnPc7	freska
z	z	k7c2	z
raného	raný	k2eAgNnSc2d1	rané
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vlámskými	vlámský	k2eAgFnPc7d1	vlámská
tapisériemi	tapisérie	k1gFnPc7	tapisérie
a	a	k8xC	a
malbami	malba	k1gFnPc7	malba
v	v	k7c6	v
bočních	boční	k2eAgFnPc6d1	boční
kaplích	kaple	k1gFnPc6	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Cenný	cenný	k2eAgInSc1d1	cenný
jeportál	jeportál	k1gInSc1	jeportál
s	s	k7c7	s
gotickou	gotický	k2eAgFnSc7d1	gotická
předsíní	předsíň	k1gFnSc7	předsíň
na	na	k7c6	na
sloupech	sloup	k1gInPc6	sloup
<g/>
,	,	kIx,	,
podpíraných	podpíraný	k2eAgFnPc2d1	podpíraná
románskými	románský	k2eAgInPc7d1	románský
lvy	lev	k1gInPc7	lev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavicích	hlavice	k1gFnPc6	hlavice
sloupů	sloup	k1gInPc2	sloup
jsou	být	k5eAaImIp3nP	být
výjevy	výjev	k1gInPc1	výjev
z	z	k7c2	z
rytířské	rytířský	k2eAgFnSc2d1	rytířská
poezie	poezie	k1gFnSc2	poezie
(	(	kIx(	(
<g/>
Roland	Roland	k1gInSc1	Roland
dující	dující	k2eAgInSc1d1	dující
na	na	k7c4	na
roh	roh	k1gInSc4	roh
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Vnitřek	vnitřek	k1gInSc1	vnitřek
chrámu	chrám	k1gInSc2	chrám
je	být	k5eAaImIp3nS	být
vyzdoben	vyzdobit	k5eAaPmNgInS	vyzdobit
renesančními	renesanční	k2eAgFnPc7d1	renesanční
freskami	freska	k1gFnPc7	freska
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1505	[number]	k4	1505
-	-	kIx~	-
1573	[number]	k4	1573
<g/>
,	,	kIx,	,
výjevy	výjev	k1gInPc1	výjev
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Boccaccio	Boccaccio	k6eAd1	Boccaccio
Boccaccino	Boccaccino	k1gNnSc4	Boccaccino
<g/>
,	,	kIx,	,
Romanino	Romanin	k2eAgNnSc1d1	Romanin
da	da	k?	da
Brescia	Brescia	k1gFnSc1	Brescia
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
malíři	malíř	k1gMnPc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Cenné	cenný	k2eAgInPc1d1	cenný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
chórové	chórový	k2eAgFnPc1d1	chórová
lavice	lavice	k1gFnPc1	lavice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
a	a	k8xC	a
bruselské	bruselský	k2eAgFnSc2d1	bruselská
tapisérie	tapisérie	k1gFnSc2	tapisérie
se	s	k7c7	s
Samsonovými	Samsonův	k2eAgInPc7d1	Samsonův
příběhy	příběh	k1gInPc7	příběh
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
dómu	dóm	k1gInSc2	dóm
stojí	stát	k5eAaImIp3nS	stát
mohutné	mohutný	k2eAgNnSc1d1	mohutné
osmiboké	osmiboký	k2eAgNnSc1d1	osmiboké
románské	románský	k2eAgNnSc1d1	románské
baptisterium	baptisterium	k1gNnSc1	baptisterium
(	(	kIx(	(
<g/>
křestní	křestní	k2eAgFnSc1d1	křestní
kaple	kaple	k1gFnSc1	kaple
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
roku	rok	k1gInSc2	rok
1167	[number]	k4	1167
a	a	k8xC	a
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
upravené	upravený	k2eAgFnSc6d1	upravená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvonice	zvonice	k1gFnSc5	zvonice
Torrazzo	Torrazza	k1gFnSc5	Torrazza
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
112,7	[number]	k4	112,7
m	m	kA	m
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1230	[number]	k4	1230
<g/>
–	–	k?	–
<g/>
1309	[number]	k4	1309
<g/>
,	,	kIx,	,
s	s	k7c7	s
katedrálou	katedrála	k1gFnSc7	katedrála
spojená	spojený	k2eAgFnSc1d1	spojená
loggií	loggie	k1gFnSc7	loggie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
patře	patro	k1gNnSc6	patro
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
orloj	orloj	k1gInSc1	orloj
na	na	k7c6	na
světě	svět	k1gInSc6	svět
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
strojem	stroj	k1gInSc7	stroj
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1583	[number]	k4	1583
<g/>
–	–	k?	–
<g/>
1588	[number]	k4	1588
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vrcholku	vrcholek	k1gInSc2	vrcholek
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc4d1	široký
rozhled	rozhled	k1gInSc4	rozhled
na	na	k7c4	na
město	město	k1gNnSc4	město
i	i	k8xC	i
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
San	San	k?	San
Agostino	Agostin	k2eAgNnSc1d1	Agostino
je	být	k5eAaImIp3nS	být
gotický	gotický	k2eAgInSc4d1	gotický
augustiniánský	augustiniánský	k2eAgInSc4d1	augustiniánský
kostel	kostel	k1gInSc4	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1339	[number]	k4	1339
s	s	k7c7	s
cennou	cenný	k2eAgFnSc7d1	cenná
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
San	San	k?	San
Pietro	Pietro	k1gNnSc1	Pietro
del	del	k?	del
Po	po	k7c4	po
je	být	k5eAaImIp3nS	být
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
dobovou	dobový	k2eAgFnSc7d1	dobová
štukovou	štukový	k2eAgFnSc7d1	štuková
a	a	k8xC	a
freskovou	freskový	k2eAgFnSc7d1	fresková
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
San	San	k?	San
Sigismondo	Sigismondo	k6eAd1	Sigismondo
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgInSc1d1	renesanční
kostel	kostel	k1gInSc1	kostel
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
Cremony	Cremona	k1gFnSc2	Cremona
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1441	[number]	k4	1441
konala	konat	k5eAaImAgFnS	konat
svatba	svatba	k1gFnSc1	svatba
Francesca	Francesc	k1gInSc2	Francesc
Sforzy	Sforza	k1gFnSc2	Sforza
a	a	k8xC	a
Biancy	Bianca	k1gFnSc2	Bianca
Viscontiové	Viscontiový	k2eAgFnSc2d1	Viscontiový
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
jej	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
pak	pak	k6eAd1	pak
Francesco	Francesco	k1gMnSc1	Francesco
Sforza	Sforz	k1gMnSc2	Sforz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1463	[number]	k4	1463
upravit	upravit	k5eAaPmF	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
je	být	k5eAaImIp3nS	být
vyzdoben	vyzdoben	k2eAgInSc1d1	vyzdoben
malbami	malba	k1gFnPc7	malba
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
oltářními	oltářní	k2eAgInPc7d1	oltářní
obrazy	obraz	k1gInPc7	obraz
a	a	k8xC	a
freskami	freska	k1gFnPc7	freska
členů	člen	k1gInPc2	člen
cremonské	cremonský	k2eAgFnSc2d1	cremonský
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
rodina	rodina	k1gFnSc1	rodina
Campiů	Campi	k1gInPc2	Campi
<g/>
,	,	kIx,	,
Gatti	Gatti	k1gNnSc1	Gatti
a	a	k8xC	a
Boccaccino	Boccaccino	k1gNnSc1	Boccaccino
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
housle	housle	k1gFnPc4	housle
==	==	k?	==
</s>
</p>
<p>
<s>
Housle	housle	k1gFnPc1	housle
spatřily	spatřit	k5eAaPmAgFnP	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Cremoně	Cremona	k1gFnSc6	Cremona
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
nástroj	nástroj	k1gInSc4	nástroj
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
houslař	houslař	k1gMnSc1	houslař
Andrea	Andrea	k1gFnSc1	Andrea
Amati	Amať	k1gFnSc2	Amať
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
lepší	dobrý	k2eAgInSc4d2	lepší
zvuk	zvuk	k1gInSc4	zvuk
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
středověkým	středověký	k2eAgFnPc3d1	středověká
skřipkám	skřipka	k1gFnPc3	skřipka
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
záhy	záhy	k6eAd1	záhy
získal	získat	k5eAaPmAgMnS	získat
oblibu	obliba	k1gFnSc4	obliba
u	u	k7c2	u
královských	královský	k2eAgInPc2d1	královský
dvorů	dvůr	k1gInPc2	dvůr
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc4	ten
až	až	k6eAd1	až
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
vnuka	vnuk	k1gMnSc2	vnuk
Andrey	Andrea	k1gFnSc2	Andrea
Amatiho	Amati	k1gMnSc2	Amati
<g/>
,	,	kIx,	,
Niccola	Niccola	k1gFnSc1	Niccola
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dovedl	dovést	k5eAaPmAgInS	dovést
toto	tento	k3xDgNnSc4	tento
řemeslo	řemeslo	k1gNnSc4	řemeslo
do	do	k7c2	do
dokonalosti	dokonalost	k1gFnSc2	dokonalost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
dílny	dílna	k1gFnSc2	dílna
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1100	[number]	k4	1100
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
přes	přes	k7c4	přes
400	[number]	k4	400
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
moderní	moderní	k2eAgFnSc1d1	moderní
technologie	technologie	k1gFnSc1	technologie
zatím	zatím	k6eAd1	zatím
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
housle	housle	k1gFnPc4	housle
s	s	k7c7	s
lepším	dobrý	k2eAgInSc7d2	lepší
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Obdivovatele	obdivovatel	k1gMnSc4	obdivovatel
tohoto	tento	k3xDgMnSc4	tento
mistra	mistr	k1gMnSc4	mistr
by	by	kYmCp3nS	by
neměli	mít	k5eNaImAgMnP	mít
vynechat	vynechat	k5eAaPmF	vynechat
Museo	Museo	k6eAd1	Museo
Stradivariano	Stradivariana	k1gFnSc5	Stradivariana
<g/>
,	,	kIx,	,
Palazzo	Palazza	k1gFnSc5	Palazza
del	del	k?	del
Comune	Comun	k1gInSc5	Comun
ani	ani	k8xC	ani
houslařův	houslařův	k2eAgInSc1d1	houslařův
náhrobek	náhrobek	k1gInSc1	náhrobek
na	na	k7c4	na
Piazza	Piazz	k1gMnSc4	Piazz
Roma	Rom	k1gMnSc4	Rom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průmysl	průmysl	k1gInSc1	průmysl
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
proslulo	proslout	k5eAaPmAgNnS	proslout
především	především	k9	především
výrobou	výroba	k1gFnSc7	výroba
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
však	však	k9	však
také	také	k9	také
četné	četný	k2eAgInPc4d1	četný
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
závody	závod	k1gInPc4	závod
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
strojírenské	strojírenský	k2eAgNnSc1d1	strojírenské
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
strojů	stroj	k1gInPc2	stroj
<g/>
)	)	kIx)	)
a	a	k8xC	a
potravinářské	potravinářský	k2eAgFnPc1d1	potravinářská
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
u	u	k7c2	u
Contemaggiore	Contemaggior	k1gMnSc5	Contemaggior
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ložiska	ložisko	k1gNnPc1	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
jako	jako	k9	jako
kulturní	kulturní	k2eAgNnSc4d1	kulturní
středisko	středisko	k1gNnSc4	středisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Slavní	slavný	k2eAgMnPc1d1	slavný
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Gerard	Gerard	k1gInSc1	Gerard
z	z	k7c2	z
Cremony	Cremona	k1gFnSc2	Cremona
(	(	kIx(	(
<g/>
1114	[number]	k4	1114
-	-	kIx~	-
1187	[number]	k4	1187
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
překladem	překlad	k1gInSc7	překlad
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
"	"	kIx"	"
<g/>
znovuobjevil	znovuobjevit	k5eAaPmAgMnS	znovuobjevit
<g/>
"	"	kIx"	"
Ptolemaiovu	Ptolemaiův	k2eAgFnSc4d1	Ptolemaiova
astronomii	astronomie	k1gFnSc4	astronomie
</s>
</p>
<p>
<s>
Claudio	Claudio	k6eAd1	Claudio
Monteverdi	Monteverd	k1gMnPc1	Monteverd
(	(	kIx(	(
<g/>
1567	[number]	k4	1567
-	-	kIx~	-
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
otců	otec	k1gMnPc2	otec
italské	italský	k2eAgFnSc2d1	italská
opery	opera	k1gFnSc2	opera
</s>
</p>
<p>
<s>
Niccolò	Niccolò	k?	Niccolò
Amati	Amati	k1gNnSc1	Amati
(	(	kIx(	(
<g/>
1596	[number]	k4	1596
-	-	kIx~	-
1684	[number]	k4	1684
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
houslař	houslař	k1gMnSc1	houslař
</s>
</p>
<p>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
(	(	kIx(	(
<g/>
1644	[number]	k4	1644
-	-	kIx~	-
1737	[number]	k4	1737
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
houslař	houslař	k1gMnSc1	houslař
</s>
</p>
<p>
<s>
Ugo	Ugo	k?	Ugo
Tognazzi	Tognazh	k1gMnPc1	Tognazh
(	(	kIx(	(
<g/>
*	*	kIx~	*
1922	[number]	k4	1922
-	-	kIx~	-
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Alaquà	Alaquà	k?	Alaquà
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
Krasnojarsk	Krasnojarsk	k1gInSc1	Krasnojarsk
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
K.	K.	kA	K.
Baedeker	Baedeker	k1gInSc1	Baedeker
<g/>
,	,	kIx,	,
Northern	Northerna	k1gFnPc2	Northerna
Italy	Ital	k1gMnPc4	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Leipzig	Leipzig	k1gInSc1	Leipzig
<g/>
:	:	kIx,	:
Baedeker	Baedeker	k1gInSc1	Baedeker
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
244	[number]	k4	244
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
–	–	k?	–
Severní	severní	k2eAgInSc1d1	severní
Jadran	Jadran	k1gInSc1	Jadran
<g/>
,	,	kIx,	,
Pádská	pádský	k2eAgFnSc1d1	Pádská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
Kartografie	kartografie	k1gFnSc1	kartografie
Praha	Praha	k1gFnSc1	Praha
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Ikar	Ikar	k1gInSc1	Ikar
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Cremona	Cremona	k1gFnSc1	Cremona
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
5	[number]	k4	5
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
709	[number]	k4	709
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Cremona	Cremona	k1gFnSc1	Cremona
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Cremony	Cremona	k1gFnSc2	Cremona
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cremona	Cremon	k1gMnSc2	Cremon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Cremona	Cremona	k1gFnSc1	Cremona
v	v	k7c6	v
Baedekerově	Baedekerův	k2eAgMnSc6d1	Baedekerův
průvodci	průvodce	k1gMnSc6	průvodce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
</s>
</p>
<p>
<s>
Cremona	Cremona	k1gFnSc1	Cremona
v	v	k7c6	v
Baedekerově	Baedekerův	k2eAgMnSc6d1	Baedekerův
průvodci	průvodce	k1gMnSc6	průvodce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
</s>
</p>
