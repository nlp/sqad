<s>
Komunitní	komunitní	k2eAgNnSc1d1
kompostování	kompostování	k1gNnSc1
je	být	k5eAaImIp3nS
kompostování	kompostování	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
lidí	člověk	k1gMnPc2
(	(	kIx(
<g/>
komunita	komunita	k1gFnSc1
<g/>
,	,	kIx,
domovní	domovní	k2eAgInSc1d1
blok	blok	k1gInSc1
<g/>
,	,	kIx,
zahrádkářská	zahrádkářský	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
)	)	kIx)
s	s	k7c7
cílem	cíl	k1gInSc7
využít	využít	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
bioodpad	bioodpad	k1gInSc4
a	a	k8xC
získaný	získaný	k2eAgInSc4d1
kompost	kompost	k1gInSc4
využít	využít	k5eAaPmF
co	co	k9
nejblíže	blízce	k6eAd3
místa	místo	k1gNnSc2
vzniku	vznik	k1gInSc2
<g/>
.	.	kIx.
</s>