<s>
Komunitní	komunitní	k2eAgNnSc1d1
kompostování	kompostování	k1gNnSc1
</s>
<s>
Komunitní	komunitní	k2eAgNnSc1d1
kompostování	kompostování	k1gNnSc1
občanů	občan	k1gMnPc2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompostoviště	Kompostoviště	k1gNnPc4
je	být	k5eAaImIp3nS
umístěné	umístěný	k2eAgNnSc1d1
v	v	k7c6
malém	malý	k2eAgInSc6d1
parčíku	parčík	k1gInSc6
na	na	k7c6
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
zúčastnění	zúčastněný	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
žijí	žít	k5eAaImIp3nP
v	v	k7c6
okolních	okolní	k2eAgFnPc6d1
několikapatrových	několikapatrový	k2eAgFnPc6d1
budovách	budova	k1gFnPc6
</s>
<s>
Komunitní	komunitní	k2eAgNnSc1d1
kompostování	kompostování	k1gNnSc1
je	být	k5eAaImIp3nS
kompostování	kompostování	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
lidí	člověk	k1gMnPc2
(	(	kIx(
<g/>
komunita	komunita	k1gFnSc1
<g/>
,	,	kIx,
domovní	domovní	k2eAgInSc1d1
blok	blok	k1gInSc1
<g/>
,	,	kIx,
zahrádkářská	zahrádkářský	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
)	)	kIx)
s	s	k7c7
cílem	cíl	k1gInSc7
využít	využít	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
bioodpad	bioodpad	k1gInSc4
a	a	k8xC
získaný	získaný	k2eAgInSc4d1
kompost	kompost	k1gInSc4
využít	využít	k5eAaPmF
co	co	k9
nejblíže	blízce	k6eAd3
místa	místo	k1gNnSc2
vzniku	vznik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Způsob	způsob	k1gInSc1
komunitního	komunitní	k2eAgNnSc2d1
kompostování	kompostování	k1gNnSc2
je	být	k5eAaImIp3nS
upraven	upravit	k5eAaPmNgInS
novelou	novela	k1gFnSc7
zákona	zákon	k1gInSc2
o	o	k7c6
odpadech	odpad	k1gInPc6
(	(	kIx(
<g/>
Zákon	zákon	k1gInSc1
314	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
je	být	k5eAaImIp3nS
novelizován	novelizován	k2eAgInSc1d1
zákon	zákon	k1gInSc1
č.	č.	k?
185	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
odpadech	odpad	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Komunitní	komunitní	k2eAgNnSc1d1
kompostování	kompostování	k1gNnSc1
může	moct	k5eAaImIp3nS
provozovat	provozovat	k5eAaImF
několik	několik	k4yIc4
domácností	domácnost	k1gFnPc2
žijících	žijící	k2eAgFnPc2d1
v	v	k7c6
sousedství	sousedství	k1gNnSc6
(	(	kIx(
<g/>
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kolektiv	kolektiv	k1gInSc1
zaměstnanců	zaměstnanec	k1gMnPc2
podniku	podnik	k1gInSc2
(	(	kIx(
<g/>
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
škola	škola	k1gFnSc1
apod.	apod.	kA
Úspěšně	úspěšně	k6eAd1
se	se	k3xPyFc4
komunitní	komunitní	k2eAgNnSc1d1
kompostování	kompostování	k1gNnSc1
rozvíjí	rozvíjet	k5eAaImIp3nS
např.	např.	kA
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
díky	díky	k7c3
aktivitám	aktivita	k1gFnPc3
Komunitní	komunitní	k2eAgFnSc2d1
kompostovací	kompostovací	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
nebo	nebo	k8xC
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Komunitní	komunitní	k2eAgInPc1d1
kompostéry	kompostér	k1gInPc1
jsou	být	k5eAaImIp3nP
vhodným	vhodný	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
pro	pro	k7c4
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
kompostování	kompostování	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
na	na	k7c6
veřejném	veřejný	k2eAgNnSc6d1
prostranství	prostranství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uzavřený	uzavřený	k2eAgInSc1d1
kompostér	kompostér	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
zabezpečen	zabezpečit	k5eAaPmNgInS
proti	proti	k7c3
potkanům	potkan	k1gMnPc3
a	a	k8xC
jiným	jiný	k2eAgMnPc3d1
hlodavcům	hlodavec	k1gMnPc3
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
proniknout	proniknout	k5eAaPmF
k	k	k7c3
odpadu	odpad	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompostér	kompostér	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
umístěn	umístit	k5eAaPmNgInS
na	na	k7c6
zastíněném	zastíněný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
blízko	blízko	k7c2
obytných	obytný	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
však	však	k9
ve	v	k7c6
stejných	stejný	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
nádoby	nádoba	k1gFnPc1
s	s	k7c7
odpadem	odpad	k1gInSc7
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
případným	případný	k2eAgInPc3d1
pachům	pach	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Výhody	výhoda	k1gFnPc1
a	a	k8xC
nevýhody	nevýhoda	k1gFnPc1
</s>
<s>
Výhodou	výhoda	k1gFnSc7
pro	pro	k7c4
členy	člen	k1gMnPc4
komunity	komunita	k1gFnSc2
využívající	využívající	k2eAgNnSc4d1
společné	společný	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
je	být	k5eAaImIp3nS
snížení	snížení	k1gNnSc4
nákladů	náklad	k1gInPc2
na	na	k7c4
pořízení	pořízení	k1gNnSc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc1
společné	společný	k2eAgNnSc1d1
vlastnění	vlastnění	k1gNnSc1
a	a	k8xC
rozdělení	rozdělení	k1gNnSc1
nákladů	náklad	k1gInPc2
na	na	k7c4
údržbu	údržba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastníci	účastník	k1gMnPc1
projektu	projekt	k1gInSc2
získávají	získávat	k5eAaImIp3nP
zdarma	zdarma	k6eAd1
kompost	kompost	k1gInSc4
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
potřebu	potřeba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
obec	obec	k1gFnSc4
se	se	k3xPyFc4
snižují	snižovat	k5eAaImIp3nP
náklady	náklad	k1gInPc1
na	na	k7c4
svoz	svoz	k1gInSc4
a	a	k8xC
likvidaci	likvidace	k1gFnSc4
části	část	k1gFnSc2
odpadu	odpad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
je	on	k3xPp3gNnPc4
komunitní	komunitní	k2eAgNnPc4d1
kompostování	kompostování	k1gNnPc4
pro	pro	k7c4
skládkování	skládkování	k1gNnSc4
odpadu	odpad	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
organické	organický	k2eAgFnPc1d1
složky	složka	k1gFnPc1
komunitního	komunitní	k2eAgInSc2d1
odpadu	odpad	k1gInSc2
mohou	moct	k5eAaImIp3nP
změnami	změna	k1gFnPc7
objemu	objem	k1gInSc2
během	během	k7c2
skládkování	skládkování	k1gNnSc2
narušovat	narušovat	k5eAaImF
těsnost	těsnost	k1gFnSc4
skládky	skládka	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
způsobit	způsobit	k5eAaPmF
únik	únik	k1gInSc4
toxických	toxický	k2eAgInPc2d1
výluhů	výluh	k1gInPc2
do	do	k7c2
spodní	spodní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omezení	omezení	k1gNnSc1
bioodpadů	bioodpad	k1gInPc2
tak	tak	k9
nejen	nejen	k6eAd1
sníží	snížit	k5eAaPmIp3nP
náklady	náklad	k1gInPc1
za	za	k7c4
svoz	svoz	k1gInSc4
a	a	k8xC
likvidaci	likvidace	k1gFnSc4
odpadu	odpad	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
učiní	učinit	k5eAaImIp3nS,k5eAaPmIp3nS
nakládání	nakládání	k1gNnSc4
s	s	k7c7
odpady	odpad	k1gInPc7
bezpečnějším	bezpečný	k2eAgMnPc3d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komunitní	komunitní	k2eAgNnSc1d1
kompostování	kompostování	k1gNnSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Obce	obec	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
úmyslu	úmysl	k1gInSc6
provozovat	provozovat	k5eAaImF
systém	systém	k1gInSc4
komunitního	komunitní	k2eAgNnSc2d1
kompostování	kompostování	k1gNnSc2
jsou	být	k5eAaImIp3nP
povinny	povinen	k2eAgFnPc1d1
vydat	vydat	k5eAaPmF
obecně	obecně	k6eAd1
závaznou	závazný	k2eAgFnSc4d1
vyhlášku	vyhláška	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tou	ten	k3xDgFnSc7
je	být	k5eAaImIp3nS
stanoven	stanoven	k2eAgInSc1d1
systém	systém	k1gInSc1
komunitního	komunitní	k2eAgNnSc2d1
kompostování	kompostování	k1gNnSc2
a	a	k8xC
způsob	způsob	k1gInSc1
využití	využití	k1gNnSc2
zeleného	zelený	k2eAgInSc2d1
kompostu	kompost	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
doposud	doposud	k6eAd1
není	být	k5eNaImIp3nS
zaveden	zaveden	k2eAgInSc1d1
systém	systém	k1gInSc1
odděleného	oddělený	k2eAgInSc2d1
sběru	sběr	k1gInSc2
bioodpadu	bioodpást	k5eAaPmIp1nS
v	v	k7c6
obcích	obec	k1gFnPc6
si	se	k3xPyFc3
komunitní	komunitní	k2eAgNnSc4d1
kompostování	kompostování	k1gNnSc4
mohou	moct	k5eAaImIp3nP
zřídit	zřídit	k5eAaPmF
sami	sám	k3xTgMnPc1
občané	občan	k1gMnPc1
svépomocí	svépomoc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
tak	tak	k6eAd1
mohou	moct	k5eAaImIp3nP
postupovat	postupovat	k5eAaImF
firmy	firma	k1gFnSc2
a	a	k8xC
organizace	organizace	k1gFnSc2
pro	pro	k7c4
kompostování	kompostování	k1gNnSc4
odpadu	odpad	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
sami	sám	k3xTgMnPc1
produkují	produkovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Zařízení	zařízení	k1gNnSc1
pro	pro	k7c4
kompostování	kompostování	k1gNnSc4
</s>
<s>
Jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgInP
automatické	automatický	k2eAgInPc1d1
kompostéry	kompostér	k1gInPc1
<g/>
,	,	kIx,
dřevěné	dřevěný	k2eAgInPc1d1
kompostéry	kompostér	k1gInPc1
<g/>
,	,	kIx,
plastové	plastový	k2eAgInPc1d1
kompostéry	kompostér	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zařízení	zařízení	k1gNnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
podobu	podoba	k1gFnSc4
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
stojících	stojící	k2eAgInPc2d1
boxů	box	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
perforované	perforovaný	k2eAgNnSc4d1
(	(	kIx(
<g/>
děrované	děrovaný	k2eAgNnSc4d1
<g/>
)	)	kIx)
dno	dno	k1gNnSc4
pro	pro	k7c4
odvod	odvod	k1gInSc4
zbytkové	zbytkový	k2eAgFnSc2d1
vody	voda	k1gFnSc2
a	a	k8xC
lepší	dobrý	k2eAgNnSc1d2
odvětrání	odvětrání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
doporučeno	doporučit	k5eAaPmNgNnS
aby	aby	k9
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
nádoba	nádoba	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
pro	pro	k7c4
kompostování	kompostování	k1gNnSc4
měla	mít	k5eAaImAgFnS
šířku	šířka	k1gFnSc4
nejvýše	nejvýše	k6eAd1,k6eAd3
2	#num#	k4
<g/>
m	m	kA
a	a	k8xC
výšku	výška	k1gFnSc4
1,5	1,5	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
plnění	plnění	k1gNnSc6
kompostu	kompost	k1gInSc2
výhradně	výhradně	k6eAd1
odpady	odpad	k1gInPc1
z	z	k7c2
domácnosti	domácnost	k1gFnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
kompostér	kompostér	k1gInSc4
o	o	k7c6
objemu	objem	k1gInSc6
cca	cca	kA
0,8	0,8	k4
m	m	kA
<g/>
3	#num#	k4
být	být	k5eAaImF
využíván	využíván	k2eAgInSc4d1
přibližně	přibližně	k6eAd1
20-30	20-30	k4
domácnostmi	domácnost	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Dohlížet	dohlížet	k5eAaImF
na	na	k7c4
kompostování	kompostování	k1gNnSc4
a	a	k8xC
organizovat	organizovat	k5eAaBmF
jej	on	k3xPp3gInSc4
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
odpovědná	odpovědný	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
sledovat	sledovat	k5eAaImF
a	a	k8xC
udržovat	udržovat	k5eAaImF
optimální	optimální	k2eAgFnSc4d1
skladbu	skladba	k1gFnSc4
kompostu	kompost	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
vlhkost	vlhkost	k1gFnSc4
či	či	k8xC
dostatečné	dostatečný	k2eAgNnSc4d1
provzdušnění	provzdušnění	k1gNnSc4
kompostu	kompost	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Komunitní	komunitní	k2eAgNnSc1d1
kompostoviště	kompostoviště	k1gNnSc1
několika	několik	k4yIc2
domácností	domácnost	k1gFnPc2
ve	v	k7c6
švýcarském	švýcarský	k2eAgNnSc6d1
Lucernu	lucerna	k1gFnSc4
</s>
<s>
Komunitní	komunitní	k2eAgNnSc1d1
kompostoviště	kompostoviště	k1gNnSc1
pracovníků	pracovník	k1gMnPc2
administrativní	administrativní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
ve	v	k7c6
Štýrském	štýrský	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
KOLEKTIV	kolektivum	k1gNnPc2
AUTORŮ	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
podporovat	podporovat	k5eAaImF
komunitní	komunitní	k2eAgNnSc4d1
a	a	k8xC
domovní	domovní	k2eAgNnSc4d1
kompostování	kompostování	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Hnutí	hnutí	k1gNnSc1
Duha	duha	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Komunitní	komunitní	k2eAgNnSc4d1
kompostování	kompostování	k1gNnSc4
v	v	k7c6
obcích	obec	k1gFnPc6
podle	podle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c6
odpadech	odpad	k1gInPc6
<g/>
1	#num#	k4
2	#num#	k4
Kompostuj	kompostovat	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Komunitní	komunitní	k2eAgNnSc1d1
kompostování	kompostování	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Ekolist	Ekolist	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Komunitní	komunitní	k2eAgNnSc1d1
kompostování	kompostování	k1gNnSc1
-	-	kIx~
zkušenosti	zkušenost	k1gFnPc1
z	z	k7c2
praxe	praxe	k1gFnSc2
a	a	k8xC
tipy	tip	k1gInPc1
pro	pro	k7c4
začátečníky	začátečník	k1gMnPc4
</s>
