<s>
S	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
souvisí	souviset	k5eAaImIp3nS	souviset
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
též	též	k9	též
hmotové	hmotový	k2eAgNnSc1d1	hmotové
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
všech	všecek	k3xTgInPc2	všecek
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
)	)	kIx)	)
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
