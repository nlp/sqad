<p>
<s>
Three	Thre	k1gInPc1	Thre
6	[number]	k4	6
Mafia	Mafium	k1gNnSc2	Mafium
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Triple	tripl	k1gInSc5	tripl
6	[number]	k4	6
Mafia	Mafium	k1gNnPc1	Mafium
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
Da	Da	k1gMnSc4	Da
Mafia	Mafius	k1gMnSc4	Mafius
6	[number]	k4	6
<g/>
ix	ix	k?	ix
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
hip-hopová	hipopový	k2eAgFnSc1d1	hip-hopová
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Memphisu	Memphis	k1gInSc2	Memphis
v	v	k7c6	v
Tennessee	Tennessee	k1gFnSc6	Tennessee
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
trio	trio	k1gNnSc4	trio
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Tvořili	tvořit	k5eAaImAgMnP	tvořit
jej	on	k3xPp3gMnSc4	on
DJ	DJ	kA	DJ
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
Lord	lord	k1gMnSc1	lord
Infamous	Infamous	k1gInSc1	Infamous
a	a	k8xC	a
Juicy	Juicy	k1gInPc1	Juicy
J.	J.	kA	J.
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
trio	trio	k1gNnSc1	trio
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
členy	člen	k1gInPc4	člen
(	(	kIx(	(
<g/>
Crunchy	Crunch	k1gInPc4	Crunch
Black	Blacka	k1gFnPc2	Blacka
<g/>
,	,	kIx,	,
Gangsta	Gangsta	k1gFnSc1	Gangsta
Boo	boa	k1gFnSc5	boa
a	a	k8xC	a
Koopsta	Koopsta	k1gMnSc1	Koopsta
Knicca	Knicca	k1gMnSc1	Knicca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
hudebních	hudební	k2eAgFnPc2d1	hudební
společností	společnost	k1gFnPc2	společnost
Prophet	Prophet	k1gMnSc1	Prophet
Entertainment	Entertainment	k1gMnSc1	Entertainment
a	a	k8xC	a
Hypnotize	Hypnotize	k1gFnSc1	Hypnotize
Minds	Mindsa	k1gFnPc2	Mindsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
skupina	skupina	k1gFnSc1	skupina
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
It	It	k1gFnSc4	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hard	Hard	k1gInSc1	Hard
Out	Out	k1gFnSc2	Out
Here	Her	k1gInSc2	Her
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Pimp	Pimp	k1gMnSc1	Pimp
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hustle	Hustle	k1gFnSc2	Hustle
&	&	k?	&
Flow	Flow	k1gMnSc1	Flow
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc1	album
Most	most	k1gInSc1	most
Known	Known	k1gMnSc1	Known
Unknown	Unknown	k1gMnSc1	Unknown
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
píseň	píseň	k1gFnSc1	píseň
Stay	Staa	k1gMnSc2	Staa
Fly	Fly	k1gMnSc2	Fly
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
Buck	Buck	k1gMnSc1	Buck
a	a	k8xC	a
8	[number]	k4	8
<g/>
ball	ball	k1gMnSc1	ball
&	&	k?	&
MJG	MJG	kA	MJG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stručná	stručný	k2eAgFnSc1d1	stručná
biografie	biografie	k1gFnSc1	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
v	v	k7c6	v
Memphisu	Memphis	k1gInSc6	Memphis
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
založilo	založit	k5eAaPmAgNnS	založit
trio	trio	k1gNnSc1	trio
DJ	DJ	kA	DJ
Paul	Paul	k1gMnSc1	Paul
(	(	kIx(	(
<g/>
Paul	Paul	k1gMnSc1	Paul
Beauregard	Beauregard	k1gMnSc1	Beauregard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Juicy	Juica	k1gFnPc4	Juica
J	J	kA	J
(	(	kIx(	(
<g/>
Jordan	Jordan	k1gMnSc1	Jordan
Houston	Houston	k1gInSc1	Houston
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lord	lord	k1gMnSc1	lord
Infamous	Infamous	k1gMnSc1	Infamous
(	(	kIx(	(
<g/>
Ricky	Rick	k1gInPc1	Rick
Dunigan	Dunigana	k1gFnPc2	Dunigana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
skupiny	skupina	k1gFnSc2	skupina
byl	být	k5eAaImAgInS	být
Backyard	Backyard	k1gInSc1	Backyard
Posse	Posse	k1gFnSc2	Posse
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
změněn	změněn	k2eAgMnSc1d1	změněn
na	na	k7c4	na
Triple	tripl	k1gInSc5	tripl
6	[number]	k4	6
Mafia	Mafius	k1gMnSc4	Mafius
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
na	na	k7c4	na
žánr	žánr	k1gInSc4	žánr
horrorcore	horrorcor	k1gInSc5	horrorcor
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
několik	několik	k4yIc4	několik
EP	EP	kA	EP
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
labelu	label	k1gInSc6	label
Prophet	Prophet	k1gMnSc1	Prophet
Ent	Ent	k1gMnSc1	Ent
<g/>
..	..	k?	..
Později	pozdě	k6eAd2	pozdě
připojili	připojit	k5eAaPmAgMnP	připojit
Koopsta	Koopsta	k1gMnSc1	Koopsta
Knicca	Knicca	k1gMnSc1	Knicca
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gangsta	Gangsta	k1gMnSc1	Gangsta
Boo	boa	k1gFnSc5	boa
(	(	kIx(	(
<g/>
Lola	Lolum	k1gNnPc1	Lolum
Mitchell	Mitchella	k1gFnPc2	Mitchella
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Crunchy	Cruncha	k1gFnPc1	Cruncha
Black	Black	k1gInSc1	Black
(	(	kIx(	(
<g/>
Darnell	Darnell	k1gInSc1	Darnell
Carlton	Carlton	k1gInSc1	Carlton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
členství	členství	k1gNnSc1	členství
se	se	k3xPyFc4	se
měnilo	měnit	k5eAaImAgNnS	měnit
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
jej	on	k3xPp3gInSc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
základní	základní	k2eAgNnSc1d1	základní
duo	duo	k1gNnSc1	duo
DJ	DJ	kA	DJ
Paul	Paula	k1gFnPc2	Paula
a	a	k8xC	a
Juicy	Juica	k1gFnSc2	Juica
J	J	kA	J
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
posledními	poslední	k2eAgInPc7d1	poslední
členy	člen	k1gInPc7	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
duo	duo	k1gNnSc1	duo
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
Juicy	Juicy	k1gInPc4	Juicy
J	J	kA	J
na	na	k7c4	na
solovou	solový	k2eAgFnSc4d1	solová
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
dřívější	dřívější	k2eAgMnPc1d1	dřívější
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
DJ	DJ	kA	DJ
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
Koopsta	Koopsta	k1gMnSc1	Koopsta
Knicca	Knicca	k1gMnSc1	Knicca
<g/>
,	,	kIx,	,
Crunchy	Cruncha	k1gFnPc1	Cruncha
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
Gangsta	Gangsta	k1gMnSc1	Gangsta
Boo	boa	k1gFnSc5	boa
a	a	k8xC	a
Lord	lord	k1gMnSc1	lord
Infamous	Infamous	k1gMnSc1	Infamous
založili	založit	k5eAaPmAgMnP	založit
skupinu	skupina	k1gFnSc4	skupina
Da	Da	k1gFnSc4	Da
Mafia	Mafia	k1gFnSc1	Mafia
6	[number]	k4	6
<g/>
ix	ix	k?	ix
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Infamous	Infamous	k1gMnSc1	Infamous
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
novou	nový	k2eAgFnSc4d1	nová
skupinu	skupina	k1gFnSc4	skupina
znovuopustila	znovuopustit	k5eAaImAgFnS	znovuopustit
i	i	k9	i
Gangsta	Gangsta	k1gFnSc1	Gangsta
Boo	boa	k1gFnSc5	boa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
s	s	k7c7	s
názvem	název	k1gInSc7	název
6	[number]	k4	6
<g/>
ix	ix	k?	ix
Commandments	Commandmentsa	k1gFnPc2	Commandmentsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
poté	poté	k6eAd1	poté
vydali	vydat	k5eAaPmAgMnP	vydat
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
album	album	k1gNnSc4	album
Watch	Watch	k1gMnSc1	Watch
What	What	k1gMnSc1	What
U	u	k7c2	u
Wish	Wisha	k1gFnPc2	Wisha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
Underground	underground	k1gInSc1	underground
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
Underground	underground	k1gInSc1	underground
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
Club	club	k1gInSc1	club
Memphis	Memphis	k1gFnSc2	Memphis
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
-	-	kIx~	-
Underground	underground	k1gInSc1	underground
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
:	:	kIx,	:
Kings	Kings	k1gInSc1	Kings
od	od	k7c2	od
Memphis	Memphis	k1gFnSc2	Memphis
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Most	most	k1gInSc1	most
Known	Known	k1gNnSc1	Known
Hits	Hitsa	k1gFnPc2	Hitsa
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
Smoked	Smoked	k1gMnSc1	Smoked
Out	Out	k1gMnSc1	Out
Music	Music	k1gMnSc1	Music
Greatest	Greatest	k1gMnSc1	Greatest
Hits	Hits	k1gInSc4	Hits
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
Prophet	Prophet	k1gInSc1	Prophet
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hits	k1gInSc4	Hits
</s>
</p>
<p>
<s>
===	===	k?	===
Nezávislé	závislý	k2eNgNnSc4d1	nezávislé
album	album	k1gNnSc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
-	-	kIx~	-
Smoked	Smoked	k1gMnSc1	Smoked
Out	Out	k1gMnSc1	Out
<g/>
,	,	kIx,	,
Loced	Loced	k1gMnSc1	Loced
Out	Out	k1gMnSc1	Out
</s>
</p>
<p>
<s>
===	===	k?	===
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
-	-	kIx~	-
Live	Live	k1gFnSc1	Live
by	by	kYmCp3nS	by
Yo	Yo	k1gFnSc4	Yo
Rep	Rep	k1gFnSc2	Rep
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	alba	k1gFnSc1	alba
v	v	k7c6	v
jiných	jiná	k1gFnPc6	jiná
uskupení	uskupení	k1gNnSc2	uskupení
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Prophet	Prophet	k1gInSc1	Prophet
Posse	Posse	k1gFnSc2	Posse
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Three	Thre	k1gFnSc2	Thre
6	[number]	k4	6
Mafia	Mafium	k1gNnSc2	Mafium
<g/>
,	,	kIx,	,
The	The	k1gFnSc2	The
Kaze	kaz	k1gInSc5	kaz
<g/>
,	,	kIx,	,
Droopy	Droop	k1gMnPc4	Droop
Drew	Drew	k1gMnSc1	Drew
Dogg	Dogg	k1gMnSc1	Dogg
<g/>
,	,	kIx,	,
M-Child	M-Child	k1gMnSc1	M-Child
<g/>
,	,	kIx,	,
Da	Da	k1gMnSc1	Da
Rockafellaz	Rockafellaz	k1gInSc1	Rockafellaz
<g/>
,	,	kIx,	,
Nigga	Nigga	k1gFnSc1	Nigga
Creep	Creep	k1gMnSc1	Creep
a	a	k8xC	a
Indo	Indo	k1gMnSc1	Indo
G	G	kA	G
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
-	-	kIx~	-
Body	bod	k1gInPc4	bod
Parts	Parts	k1gInSc1	Parts
</s>
</p>
<p>
<s>
====	====	k?	====
Tear	Tear	k1gInSc1	Tear
da	da	k?	da
Club	club	k1gInSc1	club
Up	Up	k1gMnSc1	Up
Thugs	Thugs	k1gInSc1	Thugs
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Juicy	Juica	k1gFnSc2	Juica
J	J	kA	J
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Paul	Paul	k1gMnSc1	Paul
a	a	k8xC	a
Lord	lord	k1gMnSc1	lord
Infamous	Infamous	k1gMnSc1	Infamous
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
CrazyNDaLazDayz	CrazyNDaLazDayz	k1gInSc1	CrazyNDaLazDayz
</s>
</p>
<p>
<s>
====	====	k?	====
Hypnotize	Hypnotize	k1gFnPc1	Hypnotize
Camp	camp	k1gInSc4	camp
Posse	Posse	k1gFnSc2	Posse
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Three	Thre	k1gFnSc2	Thre
6	[number]	k4	6
Mafia	Mafium	k1gNnSc2	Mafium
<g/>
,	,	kIx,	,
La	la	k1gNnSc2	la
Chat	chata	k1gFnPc2	chata
<g/>
,	,	kIx,	,
The	The	k1gFnPc2	The
Kaze	kaz	k1gInSc5	kaz
<g/>
,	,	kIx,	,
Frayser	Frayser	k1gMnSc1	Frayser
Boy	boy	k1gMnSc1	boy
a	a	k8xC	a
Lil	lít	k5eAaImAgMnS	lít
Wyte	Wyte	k1gFnSc4	Wyte
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
-	-	kIx~	-
Three	Thre	k1gInSc2	Thre
6	[number]	k4	6
Mafia	Mafium	k1gNnSc2	Mafium
Presents	Presents	k1gInSc1	Presents
<g/>
:	:	kIx,	:
Hypnotize	Hypnotize	k1gFnPc1	Hypnotize
Camp	camp	k1gInSc4	camp
Posse	Posse	k1gFnSc2	Posse
</s>
</p>
<p>
<s>
====	====	k?	====
Da	Da	k1gFnPc2	Da
Headbussaz	Headbussaz	k1gInSc1	Headbussaz
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Juicy	Juica	k1gFnSc2	Juica
J	J	kA	J
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Paul	Paul	k1gMnSc1	Paul
a	a	k8xC	a
Fiend	Fiend	k1gMnSc1	Fiend
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
-	-	kIx~	-
Dat	datum	k1gNnPc2	datum
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
How	How	k1gFnSc7	How
It	It	k1gMnPc2	It
Happen	Happen	k2eAgMnSc1d1	Happen
To	to	k9	to
<g/>
'	'	kIx"	'
<g/>
M	M	kA	M
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
===	===	k?	===
Úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
singly	singl	k1gInPc1	singl
===	===	k?	===
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Stay	Staa	k1gFnSc2	Staa
Fly	Fly	k1gFnSc2	Fly
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
Buck	Buck	k1gMnSc1	Buck
a	a	k8xC	a
8	[number]	k4	8
<g/>
Ball	Ball	k1gMnSc1	Ball
&	&	k?	&
MJG	MJG	kA	MJG
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
Poppin	Poppin	k1gInSc1	Poppin
<g/>
'	'	kIx"	'
My	my	k3xPp1nPc1	my
Collar	Collara	k1gFnPc2	Collara
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Project	Project	k2eAgInSc1d1	Project
Pat	pat	k1gInSc1	pat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
Doe	Doe	k1gMnSc1	Doe
Boy	boy	k1gMnSc1	boy
Fresh	Fresh	k1gMnSc1	Fresh
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Chamillionaire	Chamillionair	k1gMnSc5	Chamillionair
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Lolli	Lolle	k1gFnSc4	Lolle
Lolli	Lolle	k1gFnSc4	Lolle
(	(	kIx(	(
<g/>
Pop	pop	k1gInSc4	pop
That	That	k2eAgInSc4d1	That
Body	bod	k1gInPc4	bod
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Project	Project	k2eAgInSc1d1	Project
Pat	pat	k1gInSc1	pat
<g/>
,	,	kIx,	,
Young	Young	k1gInSc1	Young
D	D	kA	D
a	a	k8xC	a
Superpower	Superpower	k1gMnSc1	Superpower
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Shake	Shak	k1gInSc2	Shak
My	my	k3xPp1nPc1	my
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Kalenna	Kalenna	k1gFnSc1	Kalenna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Feel	Feel	k1gMnSc1	Feel
It	It	k1gMnSc1	It
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Tiesto	Tiesto	k6eAd1	Tiesto
<g/>
,	,	kIx,	,
Sean	Sean	k1gInSc1	Sean
Kingston	Kingston	k1gInSc1	Kingston
a	a	k8xC	a
Flo	Flo	k1gFnSc1	Flo
Rida	Rida	k1gFnSc1	Rida
<g/>
)	)	kIx)	)
</s>
</p>
