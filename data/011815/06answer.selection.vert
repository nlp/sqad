<s>
Krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
kapalná	kapalný	k2eAgFnSc1d1	kapalná
červená	červený	k2eAgFnSc1d1	červená
tkáň	tkáň	k1gFnSc1	tkáň
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
transport	transport	k1gInSc1	transport
kyslíku	kyslík	k1gInSc2	kyslík
nutný	nutný	k2eAgInSc1d1	nutný
pro	pro	k7c4	pro
život	život	k1gInSc4	život
–	–	k?	–
okysličování	okysličování	k1gNnSc2	okysličování
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
</s>
