<s>
Gustav	Gustav	k1gMnSc1	Gustav
Ludwig	Ludwig	k1gMnSc1	Ludwig
Hertz	hertz	k1gInSc1	hertz
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
Hamburg	Hamburg	k1gInSc1	Hamburg
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Východní	východní	k2eAgInSc1d1	východní
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
za	za	k7c4	za
objev	objev	k1gInSc4	objev
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
srážka	srážka	k1gFnSc1	srážka
elektronu	elektron	k1gInSc2	elektron
s	s	k7c7	s
atomem	atom	k1gInSc7	atom
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Franckem	Francek	k1gMnSc7	Francek
<g/>
.	.	kIx.	.
</s>
