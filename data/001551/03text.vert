<s>
Gustav	Gustav	k1gMnSc1	Gustav
Ludwig	Ludwig	k1gMnSc1	Ludwig
Hertz	hertz	k1gInSc1	hertz
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
Hamburg	Hamburg	k1gInSc1	Hamburg
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Východní	východní	k2eAgInSc1d1	východní
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
za	za	k7c4	za
objev	objev	k1gInSc4	objev
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
srážka	srážka	k1gFnSc1	srážka
elektronu	elektron	k1gInSc2	elektron
s	s	k7c7	s
atomem	atom	k1gInSc7	atom
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Franckem	Francek	k1gMnSc7	Francek
<g/>
.	.	kIx.	.
</s>
<s>
Franck-Hertzův	Franck-Hertzův	k2eAgInSc1d1	Franck-Hertzův
experiment	experiment	k1gInSc1	experiment
byl	být	k5eAaImAgInS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
pokusů	pokus	k1gInPc2	pokus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dal	dát	k5eAaPmAgInS	dát
podklad	podklad	k1gInSc4	podklad
pro	pro	k7c4	pro
Bohrův	Bohrův	k2eAgInSc4d1	Bohrův
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
předchůdce	předchůdce	k1gMnSc1	předchůdce
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
fyzikální	fyzikální	k2eAgFnSc6d1	fyzikální
laboratoři	laboratoř	k1gFnSc6	laboratoř
firmy	firma	k1gFnSc2	firma
Philips	Philips	kA	Philips
v	v	k7c4	v
Eidhovenu	Eidhoven	k2eAgFnSc4d1	Eidhoven
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
profesorem	profesor	k1gMnSc7	profesor
a	a	k8xC	a
ředitelem	ředitel	k1gMnSc7	ředitel
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
institutu	institut	k1gInSc2	institut
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Halle	Halla	k1gFnSc6	Halla
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgNnPc4	tři
léta	léto	k1gNnPc4	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
jako	jako	k8xC	jako
ředitel	ředitel	k1gMnSc1	ředitel
Fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
institutu	institut	k1gInSc2	institut
Technologické	technologický	k2eAgFnSc2d1	technologická
univerzity	univerzita	k1gFnSc2	univerzita
Charlottenburg	Charlottenburg	k1gMnSc1	Charlottenburg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
funkce	funkce	k1gFnPc4	funkce
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
výzkumných	výzkumný	k2eAgFnPc6d1	výzkumná
laboratořích	laboratoř	k1gFnPc6	laboratoř
firmy	firma	k1gFnSc2	firma
Siemens	siemens	k1gInSc1	siemens
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
pracoval	pracovat	k5eAaImAgMnS	pracovat
Hertz	hertz	k1gInSc4	hertz
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
laboratoří	laboratoř	k1gFnPc2	laboratoř
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
svého	svůj	k3xOyFgInSc2	svůj
činného	činný	k2eAgInSc2d1	činný
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgInS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
a	a	k8xC	a
ředitelem	ředitel	k1gMnSc7	ředitel
Fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
institutu	institut	k1gInSc2	institut
Univerzity	univerzita	k1gFnSc2	univerzita
Karla	Karel	k1gMnSc2	Karel
Marxe	Marx	k1gMnSc2	Marx
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
než	než	k8xS	než
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
