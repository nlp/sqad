<p>
<s>
Postel	postel	k1gFnSc1	postel
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
určený	určený	k2eAgInSc1d1	určený
především	především	k9	především
ke	k	k7c3	k
spaní	spaní	k1gNnSc3	spaní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pravěk	pravěk	k1gInSc4	pravěk
===	===	k?	===
</s>
</p>
<p>
<s>
Postel	postel	k1gInSc1	postel
provází	provázet	k5eAaImIp3nS	provázet
člověka	člověk	k1gMnSc4	člověk
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
vystlaná	vystlaný	k2eAgFnSc1d1	vystlaná
suchou	suchý	k2eAgFnSc7d1	suchá
trávou	tráva	k1gFnSc7	tráva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
izolovat	izolovat	k5eAaBmF	izolovat
chlad	chlad	k1gInSc4	chlad
od	od	k7c2	od
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
suchá	suchý	k2eAgFnSc1d1	suchá
tráva	tráva	k1gFnSc1	tráva
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
trvalejší	trvalý	k2eAgFnSc7d2	trvalejší
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc7d2	lepší
izolací	izolace	k1gFnSc7	izolace
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
převrat	převrat	k1gInSc1	převrat
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
výrobě	výroba	k1gFnSc6	výroba
nastal	nastat	k5eAaPmAgInS	nastat
patrně	patrně	k6eAd1	patrně
až	až	k9	až
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
prvních	první	k4xOgFnPc2	první
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
,	,	kIx,	,
měst	město	k1gNnPc2	město
a	a	k8xC	a
říší	říš	k1gFnPc2	říš
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgFnPc7	jaký
byla	být	k5eAaImAgFnS	být
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
a	a	k8xC	a
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
říční	říční	k2eAgFnSc1d1	říční
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
.	.	kIx.	.
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
již	již	k6eAd1	již
znali	znát	k5eAaImAgMnP	znát
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
řemeslníci	řemeslník	k1gMnPc1	řemeslník
většinu	většina	k1gFnSc4	většina
dodnes	dodnes	k6eAd1	dodnes
používaných	používaný	k2eAgInPc2d1	používaný
spojovacích	spojovací	k2eAgInPc2d1	spojovací
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgInPc7	jaký
byly	být	k5eAaImAgFnP	být
svlaky	svlak	k1gInPc4	svlak
<g/>
,	,	kIx,	,
čepy	čep	k1gInPc4	čep
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
vrchol	vrchol	k1gInSc4	vrchol
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
lůžka	lůžko	k1gNnPc4	lůžko
za	za	k7c2	za
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
velké	velký	k2eAgNnSc1d1	velké
rozšíření	rozšíření	k1gNnSc1	rozšíření
železa	železo	k1gNnSc2	železo
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vytvářet	vytvářet	k5eAaImF	vytvářet
dokonalejší	dokonalý	k2eAgInPc4d2	dokonalejší
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Antropometrie	antropometrie	k1gFnSc1	antropometrie
<g/>
,	,	kIx,	,
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
používaná	používaný	k2eAgFnSc1d1	používaná
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
pohodlí	pohodlí	k1gNnSc2	pohodlí
uživatelů	uživatel	k1gMnPc2	uživatel
postelí	postel	k1gFnPc2	postel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
masivní	masivní	k2eAgFnSc1d1	masivní
postele	postlat	k5eAaPmIp3nS	postlat
</s>
</p>
<p>
<s>
postele	postel	k1gInPc1	postel
z	z	k7c2	z
lamina	lamino	k1gNnSc2	lamino
(	(	kIx(	(
<g/>
dřevotřískové	dřevotřískový	k2eAgInPc4d1	dřevotřískový
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kované	kovaný	k2eAgFnSc2d1	kovaná
postele	postel	k1gFnSc2	postel
</s>
</p>
<p>
<s>
kovové	kovový	k2eAgInPc1d1	kovový
postele	postel	k1gInPc1	postel
</s>
</p>
<p>
<s>
celočalouněné	celočalouněný	k2eAgFnPc1d1	celočalouněná
postele	postel	k1gFnPc1	postel
</s>
</p>
<p>
<s>
==	==	k?	==
Současná	současný	k2eAgFnSc1d1	současná
konstrukce	konstrukce	k1gFnSc1	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Postele	postel	k1gFnPc1	postel
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
nebo	nebo	k8xC	nebo
z	z	k7c2	z
ocele	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
postele	postel	k1gFnSc2	postel
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
</s>
</p>
<p>
<s>
dvou	dva	k4xCgNnPc2	dva
svislých	svislý	k2eAgNnPc2d1	svislé
čel	čelo	k1gNnPc2	čelo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
v	v	k7c6	v
hlavách	hlava	k1gFnPc6	hlava
a	a	k8xC	a
v	v	k7c6	v
nohách	noha	k1gFnPc6	noha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
také	také	k6eAd1	také
zvaných	zvaný	k2eAgInPc2d1	zvaný
pelest	pelest	k1gFnSc4	pelest
<g/>
,	,	kIx,	,
svislé	svislý	k2eAgFnPc1d1	svislá
stojací	stojací	k2eAgFnPc1d1	stojací
desky	deska	k1gFnPc1	deska
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
nohami	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
dvou	dva	k4xCgFnPc6	dva
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
podélných	podélný	k2eAgFnPc2d1	podélná
svislých	svislý	k2eAgFnPc2d1	svislá
bočnic	bočnice	k1gFnPc2	bočnice
<g/>
,	,	kIx,	,
nesených	nesený	k2eAgFnPc2d1	nesená
zpravidla	zpravidla	k6eAd1	zpravidla
čely	čelo	k1gNnPc7	čelo
</s>
</p>
<p>
<s>
roštu	rošt	k1gInSc2	rošt
uloženého	uložený	k2eAgInSc2d1	uložený
na	na	k7c6	na
bočnicích	bočnice	k1gFnPc6	bočnice
a	a	k8xC	a
čelech	čelo	k1gNnPc6	čelo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
samonosného	samonosný	k2eAgInSc2d1	samonosný
rámu	rám	k1gInSc2	rám
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tvořit	tvořit	k5eAaImF	tvořit
postel	postel	k1gFnSc4	postel
bez	bez	k7c2	bez
bočnic	bočnice	k1gFnPc2	bočnice
<g/>
.	.	kIx.	.
</s>
<s>
Rošt	rošt	k1gInSc1	rošt
umožňující	umožňující	k2eAgInSc1d1	umožňující
proudění	proudění	k1gNnSc4	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odvádí	odvádět	k5eAaImIp3nS	odvádět
vlhkost	vlhkost	k1gFnSc1	vlhkost
z	z	k7c2	z
matrace	matrace	k1gFnSc2	matrace
i	i	k9	i
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
tvořen	tvořit	k5eAaImNgInS	tvořit
</s>
</p>
<p>
<s>
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
laťkami	laťka	k1gFnPc7	laťka
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
prodyšnou	prodyšný	k2eAgFnSc7d1	prodyšná
deskou	deska	k1gFnSc7	deska
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
dříve	dříve	k6eAd2	dříve
také	také	k9	také
drátovou	drátový	k2eAgFnSc7d1	drátová
pružinovou	pružinový	k2eAgFnSc7d1	pružinová
drátěnkou	drátěnka	k1gFnSc7	drátěnka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
matrace	matrace	k1gFnSc1	matrace
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
pérové	pérový	k2eAgInPc4d1	pérový
<g/>
,	,	kIx,	,
pěnové	pěnový	k2eAgInPc4d1	pěnový
nebo	nebo	k8xC	nebo
nafukovací	nafukovací	k2eAgInPc4d1	nafukovací
gumové	gumový	k2eAgInPc4d1	gumový
<g/>
,	,	kIx,	,
položené	položený	k2eAgInPc4d1	položený
na	na	k7c6	na
roštu	rošt	k1gInSc6	rošt
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mívá	mívat	k5eAaImIp3nS	mívat
výšku	výška	k1gFnSc4	výška
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
tloušťku	tloušťka	k1gFnSc4	tloušťka
<g/>
)	)	kIx)	)
od	od	k7c2	od
10	[number]	k4	10
cm	cm	kA	cm
až	až	k6eAd1	až
po	po	k7c4	po
asi	asi	k9	asi
40	[number]	k4	40
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
stejnému	stejný	k2eAgInSc3d1	stejný
účelu	účel	k1gInSc3	účel
sloužil	sloužit	k5eAaImAgInS	sloužit
slamník	slamník	k1gInSc1	slamník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
jako	jako	k8xC	jako
doplnění	doplnění	k1gNnSc1	doplnění
postele	postel	k1gFnSc2	postel
bývá	bývat	k5eAaImIp3nS	bývat
přikrývka	přikrývka	k1gFnSc1	přikrývka
(	(	kIx(	(
<g/>
deka	deka	k1gFnSc1	deka
<g/>
,	,	kIx,	,
peřina	peřina	k1gFnSc1	peřina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polštář	polštář	k1gInSc1	polštář
pod	pod	k7c4	pod
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
ložní	ložní	k2eAgNnSc4d1	ložní
prádlo	prádlo	k1gNnSc4	prádlo
-	-	kIx~	-
prostěradlo	prostěradlo	k1gNnSc4	prostěradlo
<g/>
,	,	kIx,	,
povlaky	povlak	k1gInPc7	povlak
na	na	k7c4	na
deku	deka	k1gFnSc4	deka
a	a	k8xC	a
polštář	polštář	k1gInSc4	polštář
a	a	k8xC	a
případně	případně	k6eAd1	případně
chránič	chránič	k1gInSc4	chránič
matrace	matrace	k1gFnSc2	matrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
místo	místo	k1gNnSc1	místo
pod	pod	k7c7	pod
matrací	matrace	k1gFnSc7	matrace
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
využíváno	využívat	k5eAaPmNgNnS	využívat
jako	jako	k8xS	jako
úložný	úložný	k2eAgInSc1d1	úložný
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k9	ale
brání	bránit	k5eAaImIp3nS	bránit
odvětrávání	odvětrávání	k1gNnSc4	odvětrávání
a	a	k8xC	a
nepovažuje	považovat	k5eNaImIp3nS	považovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
za	za	k7c4	za
zdravotně	zdravotně	k6eAd1	zdravotně
zcela	zcela	k6eAd1	zcela
vhodné	vhodný	k2eAgInPc1d1	vhodný
<g/>
.	.	kIx.	.
<g/>
RozměryPostele	RozměryPostel	k1gInPc1	RozměryPostel
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
sériově	sériově	k6eAd1	sériově
v	v	k7c6	v
typizovaných	typizovaný	k2eAgInPc6d1	typizovaný
rozměrech	rozměr	k1gInPc6	rozměr
(	(	kIx(	(
<g/>
rozměr	rozměr	k1gInSc1	rozměr
ložné	ložný	k2eAgFnSc2d1	ložná
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nejčastější	častý	k2eAgFnSc7d3	nejčastější
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc1	velikost
90	[number]	k4	90
×	×	k?	×
200	[number]	k4	200
cm	cm	kA	cm
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
další	další	k2eAgFnPc1d1	další
běžné	běžný	k2eAgFnPc1d1	běžná
šířky	šířka	k1gFnPc1	šířka
jsou	být	k5eAaImIp3nP	být
80	[number]	k4	80
<g/>
,	,	kIx,	,
140	[number]	k4	140
<g/>
,	,	kIx,	,
160	[number]	k4	160
<g/>
,	,	kIx,	,
180	[number]	k4	180
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
velikosti	velikost	k1gFnPc1	velikost
jsou	být	k5eAaImIp3nP	být
nabízeny	nabízet	k5eAaImNgFnP	nabízet
jenom	jenom	k9	jenom
jako	jako	k8xC	jako
atypické	atypický	k2eAgNnSc1d1	atypické
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dětské	dětský	k2eAgInPc1d1	dětský
postele	postel	k1gInPc1	postel
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
např.	např.	kA	např.
80	[number]	k4	80
×	×	k?	×
140	[number]	k4	140
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patrové	patrový	k2eAgInPc1d1	patrový
postele	postel	k1gInPc1	postel
používané	používaný	k2eAgInPc1d1	používaný
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
např.	např.	kA	např.
90	[number]	k4	90
<g/>
x	x	k?	x
<g/>
200	[number]	k4	200
cm	cm	kA	cm
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
patry	patro	k1gNnPc7	patro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
ještě	ještě	k6eAd1	ještě
menší	malý	k2eAgFnSc1d2	menší
je	být	k5eAaImIp3nS	být
dětská	dětský	k2eAgFnSc1d1	dětská
postýlka	postýlka	k1gFnSc1	postýlka
<g/>
.	.	kIx.	.
<g/>
Horní	horní	k2eAgFnSc1d1	horní
plocha	plocha	k1gFnSc1	plocha
matrace	matrace	k1gFnSc2	matrace
bývá	bývat	k5eAaImIp3nS	bývat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
45	[number]	k4	45
cm	cm	kA	cm
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
vstávání	vstávání	k1gNnSc4	vstávání
zejména	zejména	k9	zejména
zesláblých	zesláblý	k2eAgFnPc2d1	zesláblá
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
vyšší	vysoký	k2eAgFnSc1d2	vyšší
postel	postel	k1gFnSc1	postel
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k9	také
americká	americký	k2eAgFnSc1d1	americká
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
druhem	druh	k1gInSc7	druh
jsou	být	k5eAaImIp3nP	být
zdravotnické	zdravotnický	k2eAgFnPc4d1	zdravotnická
postele	postel	k1gFnPc4	postel
určené	určený	k2eAgFnPc4d1	určená
do	do	k7c2	do
nemocnic	nemocnice	k1gFnPc2	nemocnice
a	a	k8xC	a
případně	případně	k6eAd1	případně
do	do	k7c2	do
domovů	domov	k1gInPc2	domov
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
jednak	jednak	k8xC	jednak
větší	veliký	k2eAgFnSc4d2	veliký
výšku	výška	k1gFnSc4	výška
ložné	ložný	k2eAgFnSc2d1	ložná
plochy	plocha	k1gFnSc2	plocha
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
bohatší	bohatý	k2eAgFnSc1d2	bohatší
možnost	možnost	k1gFnSc1	možnost
polohování	polohování	k1gNnSc2	polohování
matrace	matrace	k1gFnSc2	matrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
WINTER	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
domácnosti	domácnost	k1gFnSc2	domácnost
staročeské	staročeský	k2eAgFnSc2d1	staročeská
:	:	kIx,	:
Ze	z	k7c2	z
života	život	k1gInSc2	život
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
O	o	k7c6	o
staročeské	staročeský	k2eAgFnSc6d1	staročeská
posteli	postel	k1gFnSc6	postel
<g/>
,	,	kIx,	,
s.	s.	k?	s.
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
vestavěné	vestavěný	k2eAgNnSc1d1	vestavěné
patro	patro	k1gNnSc1	patro
</s>
</p>
<p>
<s>
nábytek	nábytek	k1gInSc1	nábytek
</s>
</p>
<p>
<s>
ložnice	ložnice	k1gFnSc1	ložnice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
postel	postel	k1gFnSc1	postel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
postel	postel	k1gInSc1	postel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Druhy	druh	k1gInPc1	druh
postelí	postel	k1gFnPc2	postel
</s>
</p>
