<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mlhovina	mlhovina	k1gFnSc1	mlhovina
složená	složený	k2eAgFnSc1d1	složená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
emituje	emitovat	k5eAaBmIp3nS	emitovat
světlo	světlo	k1gNnSc4	světlo
různé	různý	k2eAgFnSc2d1	různá
barvy	barva	k1gFnSc2	barva
<g/>
?	?	kIx.	?
</s>
