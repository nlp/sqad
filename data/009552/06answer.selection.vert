<s>
Emisní	emisní	k2eAgFnSc1d1	emisní
mlhovina	mlhovina	k1gFnSc1	mlhovina
je	být	k5eAaImIp3nS	být
mlhovina	mlhovina	k1gFnSc1	mlhovina
složená	složený	k2eAgFnSc1d1	složená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
(	(	kIx(	(
<g/>
plazmatu	plazma	k1gNnSc2	plazma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
emituje	emitovat	k5eAaBmIp3nS	emitovat
(	(	kIx(	(
<g/>
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
<g/>
)	)	kIx)	)
světlo	světlo	k1gNnSc1	světlo
různé	různý	k2eAgFnSc2d1	různá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
