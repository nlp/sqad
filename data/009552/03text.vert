<p>
<s>
Emisní	emisní	k2eAgFnSc1d1	emisní
mlhovina	mlhovina	k1gFnSc1	mlhovina
je	být	k5eAaImIp3nS	být
mlhovina	mlhovina	k1gFnSc1	mlhovina
složená	složený	k2eAgFnSc1d1	složená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
(	(	kIx(	(
<g/>
plazmatu	plazma	k1gNnSc2	plazma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
emituje	emitovat	k5eAaBmIp3nS	emitovat
(	(	kIx(	(
<g/>
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
<g/>
)	)	kIx)	)
světlo	světlo	k1gNnSc1	světlo
různé	různý	k2eAgFnSc2d1	různá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
ionizace	ionizace	k1gFnSc2	ionizace
bývá	bývat	k5eAaImIp3nS	bývat
vysoce	vysoce	k6eAd1	vysoce
energetické	energetický	k2eAgNnSc1d1	energetické
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
blízké	blízký	k2eAgFnSc2d1	blízká
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
typ	typ	k1gInSc4	typ
difúzní	difúzní	k2eAgFnSc2d1	difúzní
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
zdrojem	zdroj	k1gInSc7	zdroj
ionizace	ionizace	k1gFnSc2	ionizace
je	být	k5eAaImIp3nS	být
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
velmi	velmi	k6eAd1	velmi
žhavé	žhavý	k2eAgFnSc2d1	žhavá
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
okolní	okolní	k2eAgInSc4d1	okolní
plyn	plyn	k1gInSc4	plyn
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jej	on	k3xPp3gMnSc4	on
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
srážejí	srážet	k5eAaImIp3nP	srážet
s	s	k7c7	s
atomy	atom	k1gInPc7	atom
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčetnějším	četný	k2eAgInSc7d3	nejčetnější
typem	typ	k1gInSc7	typ
emisní	emisní	k2eAgFnSc2d1	emisní
mlhoviny	mlhovina	k1gFnSc2	mlhovina
je	být	k5eAaImIp3nS	být
HII	HII	kA	HII
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
planetární	planetární	k2eAgFnPc1d1	planetární
mlhoviny	mlhovina	k1gFnPc1	mlhovina
nebo	nebo	k8xC	nebo
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
supernovy	supernova	k1gFnSc2	supernova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
emisní	emisní	k2eAgFnPc1d1	emisní
mlhoviny	mlhovina	k1gFnPc1	mlhovina
</s>
</p>
<p>
<s>
Tarantule	tarantule	k1gFnSc1	tarantule
(	(	kIx(	(
<g/>
NGC	NGC	kA	NGC
2070	[number]	k4	2070
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mlhovina	mlhovina	k1gFnSc1	mlhovina
v	v	k7c6	v
Orionu	orion	k1gInSc6	orion
(	(	kIx(	(
<g/>
M	M	kA	M
42	[number]	k4	42
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krabí	krabí	k2eAgFnSc1d1	krabí
mlhovina	mlhovina	k1gFnSc1	mlhovina
(	(	kIx(	(
<g/>
M	M	kA	M
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Orlí	orlí	k2eAgFnSc1d1	orlí
mlhovina	mlhovina	k1gFnSc1	mlhovina
(	(	kIx(	(
<g/>
M	M	kA	M
16	[number]	k4	16
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mlhovina	mlhovina	k1gFnSc1	mlhovina
Laguna	laguna	k1gFnSc1	laguna
(	(	kIx(	(
<g/>
M	M	kA	M
8	[number]	k4	8
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mlhovina	mlhovina	k1gFnSc1	mlhovina
Trifid	Trifida	k1gFnPc2	Trifida
(	(	kIx(	(
<g/>
M	M	kA	M
20	[number]	k4	20
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
emisní	emisní	k2eAgFnSc1d1	emisní
mlhovina	mlhovina	k1gFnSc1	mlhovina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
