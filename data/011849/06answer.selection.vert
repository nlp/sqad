<s>
Lázně	lázeň	k1gFnPc1	lázeň
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
financováním	financování	k1gNnSc7	financování
a	a	k8xC	a
stavebním	stavební	k2eAgFnPc3d1	stavební
komplikacím	komplikace	k1gFnPc3	komplikace
se	se	k3xPyFc4	se
ale	ale	k9	ale
zahájení	zahájení	k1gNnSc1	zahájení
jejich	jejich	k3xOp3gInSc2	jejich
provozu	provoz	k1gInSc2	provoz
postupně	postupně	k6eAd1	postupně
odkládalo	odkládat	k5eAaImAgNnS	odkládat
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
na	na	k7c4	na
květen	květen	k1gInSc4	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
