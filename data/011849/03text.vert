<p>
<s>
Termální	termální	k2eAgFnPc1d1	termální
lázně	lázeň	k1gFnPc1	lázeň
Chrastava	Chrastava	k1gFnSc1	Chrastava
jsou	být	k5eAaImIp3nP	být
veřejné	veřejný	k2eAgFnPc4d1	veřejná
lázně	lázeň	k1gFnPc4	lázeň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
Chrastava	Chrastava	k1gFnSc1	Chrastava
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
<g/>
.	.	kIx.	.
</s>
<s>
Lázně	lázeň	k1gFnPc1	lázeň
využívají	využívat	k5eAaImIp3nP	využívat
minerální	minerální	k2eAgFnSc4d1	minerální
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
vrtu	vrt	k1gInSc2	vrt
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
ohřívána	ohřívat	k5eAaImNgFnS	ohřívat
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
32	[number]	k4	32
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
36	[number]	k4	36
°	°	k?	°
<g/>
C.	C.	kA	C.
Areál	areál	k1gInSc1	areál
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
projektem	projekt	k1gInSc7	projekt
výstavby	výstavba	k1gFnSc2	výstavba
chrastavských	chrastavský	k2eAgFnPc2d1	Chrastavská
termálních	termální	k2eAgFnPc2d1	termální
lázní	lázeň	k1gFnPc2	lázeň
přišla	přijít	k5eAaPmAgFnS	přijít
společnost	společnost	k1gFnSc1	společnost
Dům	dům	k1gInSc1	dům
pohody	pohoda	k1gFnSc2	pohoda
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Lázně	lázeň	k1gFnPc1	lázeň
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
financováním	financování	k1gNnSc7	financování
a	a	k8xC	a
stavebním	stavební	k2eAgFnPc3d1	stavební
komplikacím	komplikace	k1gFnPc3	komplikace
se	se	k3xPyFc4	se
ale	ale	k9	ale
zahájení	zahájení	k1gNnSc1	zahájení
jejich	jejich	k3xOp3gInSc2	jejich
provozu	provoz	k1gInSc2	provoz
postupně	postupně	k6eAd1	postupně
odkládalo	odkládat	k5eAaImAgNnS	odkládat
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
na	na	k7c4	na
květen	květen	k1gInSc4	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
lázní	lázeň	k1gFnPc2	lázeň
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
chrastavského	chrastavský	k2eAgMnSc2d1	chrastavský
starosty	starosta	k1gMnSc2	starosta
Michaela	Michael	k1gMnSc2	Michael
Canova	Canovo	k1gNnSc2	Canovo
a	a	k8xC	a
chrastavských	chrastavský	k2eAgMnPc2d1	chrastavský
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
<g/>
Výstavba	výstavba	k1gFnSc1	výstavba
první	první	k4xOgFnSc2	první
etapy	etapa	k1gFnSc2	etapa
stála	stát	k5eAaImAgFnS	stát
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
šedesát	šedesát	k4xCc1	šedesát
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
pokryla	pokrýt	k5eAaPmAgFnS	pokrýt
evropská	evropský	k2eAgFnSc1d1	Evropská
dotace	dotace	k1gFnSc1	dotace
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
etapa	etapa	k1gFnSc1	etapa
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
dětský	dětský	k2eAgInSc4d1	dětský
bazén	bazén	k1gInSc4	bazén
nebo	nebo	k8xC	nebo
sportovní	sportovní	k2eAgFnSc4d1	sportovní
halu	hala	k1gFnSc4	hala
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k5eAaPmF	stát
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
dalších	další	k2eAgInPc2d1	další
120	[number]	k4	120
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybavení	vybavení	k1gNnSc1	vybavení
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
areál	areál	k1gInSc1	areál
lázní	lázeň	k1gFnPc2	lázeň
disponuje	disponovat	k5eAaBmIp3nS	disponovat
jedním	jeden	k4xCgInSc7	jeden
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
venkovními	venkovní	k2eAgInPc7d1	venkovní
bazény	bazén	k1gInPc7	bazén
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
bazén	bazén	k1gInSc1	bazén
je	být	k5eAaImIp3nS	být
naplněn	naplnit	k5eAaPmNgInS	naplnit
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
vrtu	vrt	k1gInSc2	vrt
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
32-35	[number]	k4	32-35
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k9	ještě
vířivka	vířivka	k1gFnSc1	vířivka
<g/>
,	,	kIx,	,
sauna	sauna	k1gFnSc1	sauna
<g/>
,	,	kIx,	,
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
turecké	turecký	k2eAgFnSc2d1	turecká
masáže	masáž	k1gFnSc2	masáž
hammam	hammam	k6eAd1	hammam
a	a	k8xC	a
relaxační	relaxační	k2eAgFnPc1d1	relaxační
multifunkční	multifunkční	k2eAgFnPc1d1	multifunkční
vany	vana	k1gFnPc1	vana
se	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
využitím	využití	k1gNnSc7	využití
<g/>
.	.	kIx.	.
</s>
<s>
Občerstvení	občerstvení	k1gNnSc1	občerstvení
je	být	k5eAaImIp3nS	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
v	v	k7c6	v
suchém	suchý	k2eAgInSc6d1	suchý
i	i	k8xC	i
vodním	vodní	k2eAgInSc6d1	vodní
baru	bar	k1gInSc6	bar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
areálu	areál	k1gInSc2	areál
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
dva	dva	k4xCgInPc4	dva
hotely	hotel	k1gInPc4	hotel
a	a	k8xC	a
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
vybudování	vybudování	k1gNnSc1	vybudování
jízdárny	jízdárna	k1gFnSc2	jízdárna
<g/>
,	,	kIx,	,
kempu	kemp	k1gInSc2	kemp
nebo	nebo	k8xC	nebo
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
rybaření	rybařený	k2eAgMnPc5d1	rybařený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Termální	termální	k2eAgFnSc2d1	termální
lázně	lázeň	k1gFnSc2	lázeň
Chrastava	Chrastava	k1gFnSc1	Chrastava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
Kudy	kudy	k6eAd1	kudy
z	z	k7c2	z
nudy	nuda	k1gFnSc2	nuda
</s>
</p>
