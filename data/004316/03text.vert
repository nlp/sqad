<s>
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
velkoplošná	velkoplošný	k2eAgNnPc1d1	velkoplošné
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
čtyři	čtyři	k4xCgInPc1	čtyři
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
status	status	k1gInSc1	status
upravuje	upravovat	k5eAaImIp3nS	upravovat
zákon	zákon	k1gInSc4	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
vyhlášen	vyhlášen	k2eAgMnSc1d1	vyhlášen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Šumava	Šumava	k1gFnSc1	Šumava
(	(	kIx(	(
<g/>
69	[number]	k4	69
030	[number]	k4	030
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
zkratka	zkratka	k1gFnSc1	zkratka
NP	NP	kA	NP
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
v	v	k7c4	v
§	§	k?	§
15	[number]	k4	15
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1956	[number]	k4	1956
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
definoval	definovat	k5eAaBmAgInS	definovat
pojem	pojem	k1gInSc4	pojem
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc4	jeho
náplň	náplň	k1gFnSc4	náplň
na	na	k7c6	na
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Rozsahy	rozsah	k1gInPc1	rozsah
vyhlášených	vyhlášený	k2eAgNnPc2d1	vyhlášené
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
se	se	k3xPyFc4	se
dodatečně	dodatečně	k6eAd1	dodatečně
měnily	měnit	k5eAaImAgFnP	měnit
vládními	vládní	k2eAgNnPc7d1	vládní
nařízeními	nařízení	k1gNnPc7	nařízení
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
nového	nový	k2eAgInSc2d1	nový
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
