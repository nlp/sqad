<s>
Glenn	Glenn	k1gMnSc1	Glenn
Theodore	Theodor	k1gMnSc5	Theodor
Seaborg	Seaborg	k1gMnSc1	Seaborg
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
americký	americký	k2eAgMnSc1d1	americký
jaderný	jaderný	k2eAgMnSc1d1	jaderný
chemik	chemik	k1gMnSc1	chemik
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
objevy	objev	k1gInPc4	objev
transuranů	transuran	k1gInPc2	transuran
získal	získat	k5eAaPmAgMnS	získat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Edwinem	Edwin	k1gMnSc7	Edwin
Mattisonem	Mattison	k1gMnSc7	Mattison
McMillanem	McMillan	k1gMnSc7	McMillan
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
objevitelem	objevitel	k1gMnSc7	objevitel
všech	všecek	k3xTgMnPc2	všecek
transuranů	transuran	k1gInPc2	transuran
až	až	k9	až
do	do	k7c2	do
čísla	číslo	k1gNnSc2	číslo
102	[number]	k4	102
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
výchovy	výchova	k1gFnSc2	výchova
radil	radit	k5eAaImAgMnS	radit
devíti	devět	k4xCc3	devět
americkým	americký	k2eAgMnPc3d1	americký
prezidentům	prezident	k1gMnPc3	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
106	[number]	k4	106
-	-	kIx~	-
seaborgium	seaborgium	k1gNnSc1	seaborgium
<g/>
.	.	kIx.	.
</s>
