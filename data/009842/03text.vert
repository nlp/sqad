<p>
<s>
Hřbitov	hřbitov	k1gInSc1	hřbitov
svaté	svatý	k2eAgFnSc2d1	svatá
Otýlie	Otýlie	k1gFnSc2	Otýlie
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
českobudějovickým	českobudějovický	k2eAgInSc7d1	českobudějovický
hřbitovem	hřbitov	k1gInSc7	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
u	u	k7c2	u
Pražské	pražský	k2eAgFnSc2d1	Pražská
třídy	třída	k1gFnSc2	třída
v	v	k7c6	v
Nemanicích	Nemanice	k1gFnPc6	Nemanice
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
hřbitova	hřbitov	k1gInSc2	hřbitov
je	být	k5eAaImIp3nS	být
krematorium	krematorium	k1gNnSc4	krematorium
s	s	k7c7	s
obřadní	obřadní	k2eAgFnSc7d1	obřadní
síní	síň	k1gFnSc7	síň
a	a	k8xC	a
kaple	kaple	k1gFnSc1	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Otýlie	Otýlie	k1gFnSc2	Otýlie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
hřbitov	hřbitov	k1gInSc1	hřbitov
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Hřbitov	hřbitov	k1gInSc1	hřbitov
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
výstavbu	výstavba	k1gFnSc4	výstavba
řídil	řídit	k5eAaImAgMnS	řídit
Jakob	Jakob	k1gMnSc1	Jakob
Stabernak	Stabernak	k1gMnSc1	Stabernak
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
jej	on	k3xPp3gNnSc4	on
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1889	[number]	k4	1889
biskup	biskup	k1gMnSc1	biskup
Martin	Martin	k1gMnSc1	Martin
Josef	Josef	k1gMnSc1	Josef
Říha	Říha	k1gMnSc1	Říha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
pohřeb	pohřeb	k1gInSc4	pohřeb
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
nechal	nechat	k5eAaPmAgMnS	nechat
starosta	starosta	k1gMnSc1	starosta
Josef	Josef	k1gMnSc1	Josef
Kneissl	Kneissl	k1gInSc4	Kneissl
upravit	upravit	k5eAaPmF	upravit
obřadní	obřadní	k2eAgFnSc1d1	obřadní
síň	síň	k1gFnSc1	síň
na	na	k7c4	na
kapli	kaple	k1gFnSc4	kaple
sv.	sv.	kA	sv.
Otýlie	Otýlie	k1gFnSc1	Otýlie
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
své	svůj	k3xOyFgFnSc2	svůj
zesnulé	zesnulý	k2eAgFnSc2d1	zesnulá
manželky	manželka	k1gFnSc2	manželka
Otýlie	Otýlie	k1gFnSc2	Otýlie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hřbitov	hřbitov	k1gInSc1	hřbitov
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
rozšiřován	rozšiřovat	k5eAaImNgInS	rozšiřovat
o	o	k7c6	o
další	další	k2eAgFnSc6d1	další
části	část	k1gFnSc6	část
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
krematorium	krematorium	k1gNnSc1	krematorium
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavěn	k2eAgNnSc1d1	vystavěno
současné	současný	k2eAgNnSc1d1	současné
krematorium	krematorium	k1gNnSc1	krematorium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
rozptylové	rozptylový	k2eAgFnSc2d1	rozptylová
loučky	loučka	k1gFnSc2	loučka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
hřbitov	hřbitov	k1gInSc1	hřbitov
rozlohu	rozloha	k1gFnSc4	rozloha
11,5	[number]	k4	11,5
ha	ha	kA	ha
a	a	k8xC	a
asi	asi	k9	asi
15	[number]	k4	15
500	[number]	k4	500
hrobů	hrob	k1gInPc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
pohřbené	pohřbený	k2eAgFnPc4d1	pohřbená
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
patří	patřit	k5eAaImIp3nS	patřit
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Jeremiáš	Jeremiáš	k1gMnSc1	Jeremiáš
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Otto	Otto	k1gMnSc1	Otto
Matoušek	Matoušek	k1gMnSc1	Matoušek
<g/>
,	,	kIx,	,
politici	politik	k1gMnPc1	politik
August	August	k1gMnSc1	August
Zátka	zátka	k1gFnSc1	zátka
<g/>
,	,	kIx,	,
Wendelin	Wendelin	k2eAgMnSc1d1	Wendelin
Rziha	Rziha	k1gMnSc1	Rziha
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Taschek	Taschek	k1gMnSc1	Taschek
a	a	k8xC	a
Albín	Albín	k1gMnSc1	Albín
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
nebo	nebo	k8xC	nebo
továrník	továrník	k1gMnSc1	továrník
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Johann	Johann	k1gMnSc1	Johann
Stegmann	Stegmann	k1gMnSc1	Stegmann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Části	část	k1gFnPc1	část
hřbitova	hřbitov	k1gInSc2	hřbitov
včetně	včetně	k7c2	včetně
krematoria	krematorium	k1gNnSc2	krematorium
a	a	k8xC	a
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Otýlie	Otýlie	k1gFnPc1	Otýlie
jsou	být	k5eAaImIp3nP	být
chráněny	chránit	k5eAaImNgFnP	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Církevní	církevní	k2eAgNnPc1d1	církevní
pohřebiště	pohřebiště	k1gNnPc1	pohřebiště
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
hřbitova	hřbitov	k1gInSc2	hřbitov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
(	(	kIx(	(
<g/>
v	v	k7c6	v
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
)	)	kIx)	)
hrobka	hrobka	k1gFnSc1	hrobka
českobudějovických	českobudějovický	k2eAgMnPc2d1	českobudějovický
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
biskupové	biskup	k1gMnPc1	biskup
Martin	Martin	k1gMnSc1	Martin
Josef	Josef	k1gMnSc1	Josef
Říha	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Antonín	Antonín	k1gMnSc1	Antonín
Hůlka	Hůlka	k1gMnSc1	Hůlka
<g/>
,	,	kIx,	,
Šimon	Šimon	k1gMnSc1	Šimon
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hlouch	Hlouch	k1gMnSc1	Hlouch
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Paďour	paďour	k1gMnSc1	paďour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
biskupské	biskupský	k2eAgFnSc2d1	biskupská
hrobky	hrobka	k1gFnSc2	hrobka
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
oddělení	oddělení	k1gNnSc1	oddělení
kněžských	kněžský	k2eAgInPc2d1	kněžský
hrobů	hrob	k1gInPc2	hrob
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
jednak	jednak	k8xC	jednak
kanovníci	kanovník	k1gMnPc1	kanovník
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
další	další	k2eAgMnPc1d1	další
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
českobudějovické	českobudějovický	k2eAgFnSc6d1	českobudějovická
diecézi	diecéze	k1gFnSc6	diecéze
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Tomáš	Tomáš	k1gMnSc1	Tomáš
Koupal	koupat	k5eAaImAgMnS	koupat
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
dva	dva	k4xCgMnPc1	dva
vyšebrodští	vyšebrodský	k2eAgMnPc1d1	vyšebrodský
cisterciáci	cisterciák	k1gMnPc1	cisterciák
(	(	kIx(	(
<g/>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Nýdl	Nýdl	k1gMnSc1	Nýdl
a	a	k8xC	a
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Ivo	Ivo	k1gMnSc1	Ivo
Kvapil	Kvapil	k1gMnSc1	Kvapil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
hrobka	hrobka	k1gFnSc1	hrobka
kongregace	kongregace	k1gFnSc2	kongregace
redemptoristů	redemptorista	k1gMnPc2	redemptorista
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
laičtí	laický	k2eAgMnPc1d1	laický
bratři	bratr	k1gMnPc1	bratr
této	tento	k3xDgFnSc2	tento
kongregace	kongregace	k1gFnSc2	kongregace
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
českobudějovické	českobudějovický	k2eAgFnSc6d1	českobudějovická
redemptoristické	redemptoristický	k2eAgFnSc6d1	redemptoristický
koleji	kolej	k1gFnSc6	kolej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
hrobce	hrobka	k1gFnSc6	hrobka
je	být	k5eAaImIp3nS	být
také	také	k9	také
pohřben	pohřben	k2eAgInSc1d1	pohřben
11	[number]	k4	11
<g/>
.	.	kIx.	.
českobudějovický	českobudějovický	k2eAgMnSc1d1	českobudějovický
biskup	biskup	k1gMnSc1	biskup
Antonín	Antonín	k1gMnSc1	Antonín
Liška	Liška	k1gMnSc1	Liška
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
této	tento	k3xDgFnSc2	tento
kongregace	kongregace	k1gFnSc2	kongregace
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kněžských	kněžský	k2eAgInPc2d1	kněžský
hrobů	hrob	k1gInPc2	hrob
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
hřbitova	hřbitov	k1gInSc2	hřbitov
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
oddíly	oddíl	k1gInPc1	oddíl
s	s	k7c7	s
hroby	hrob	k1gInPc7	hrob
členek	členka	k1gFnPc2	členka
ženských	ženský	k2eAgFnPc2d1	ženská
kongregací	kongregace	k1gFnPc2	kongregace
Školských	školský	k2eAgFnPc2d1	školská
sester	sestra	k1gFnPc2	sestra
de	de	k?	de
Notre	Notr	k1gMnSc5	Notr
Dame	Damus	k1gMnSc5	Damus
a	a	k8xC	a
Sester	sestra	k1gFnPc2	sestra
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Svátosti	svátost	k1gFnSc2	svátost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hřbitov	hřbitov	k1gInSc1	hřbitov
svaté	svatý	k2eAgFnSc2d1	svatá
Otýlie	Otýlie	k1gFnSc2	Otýlie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
hřbitova	hřbitov	k1gInSc2	hřbitov
</s>
</p>
