<s>
Akustika	akustika	k1gFnSc1	akustika
je	být	k5eAaImIp3nS	být
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
zvukového	zvukový	k2eAgNnSc2d1	zvukové
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
šířením	šíření	k1gNnSc7	šíření
<g/>
,	,	kIx,	,
vnímáním	vnímání	k1gNnSc7	vnímání
zvuku	zvuk	k1gInSc2	zvuk
sluchem	sluch	k1gInSc7	sluch
a	a	k8xC	a
přenosu	přenos	k1gInSc2	přenos
prostorem	prostor	k1gInSc7	prostor
až	až	k8xS	až
po	po	k7c6	po
vnímání	vnímání	k1gNnSc6	vnímání
lidskými	lidský	k2eAgFnPc7d1	lidská
smysly	smysl	k1gInPc1	smysl
<g/>
.	.	kIx.	.
</s>
