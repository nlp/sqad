<s>
Fyziologickými	fyziologický	k2eAgInPc7d1	fyziologický
projevy	projev	k1gInPc7	projev
hněvu	hněv	k1gInSc2	hněv
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc1	některý
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
tělesné	tělesný	k2eAgFnSc6d1	tělesná
činnosti	činnost	k1gFnSc6	činnost
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgInSc4d1	směřující
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
fyzické	fyzický	k2eAgFnSc2d1	fyzická
síly	síla	k1gFnSc2	síla
organismu	organismus	k1gInSc2	organismus
–	–	k?	–
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
srdeční	srdeční	k2eAgInSc1d1	srdeční
tep	tep	k1gInSc1	tep
<g/>
,	,	kIx,	,
stoupá	stoupat	k5eAaImIp3nS	stoupat
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
zčervená	zčervenat	k5eAaPmIp3nS	zčervenat
<g/>
.	.	kIx.	.
</s>
