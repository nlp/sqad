<s>
Trianon	Trianon	k1gInSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Velký	velký	k2eAgInSc1d1
Trianon	Trianon	k1gInSc1
<g/>
,	,	kIx,
Malý	malý	k2eAgInSc1d1
Trianon	Trianon	k1gInSc1
nebo	nebo	k8xC
Trianonská	trianonský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Trianon	Trianon	k1gInSc1
Účel	účel	k1gInSc1
stavby	stavba	k1gFnSc2
</s>
<s>
kanceláře	kancelář	k1gFnPc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
Novotny	Novotna	k1gFnPc1
Mähner	Mähnra	k1gFnPc2
Assoziierte	Assoziiert	k1gInSc5
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
Technické	technický	k2eAgInPc1d1
parametry	parametr	k1gInPc1
Výška	výška	k1gFnSc1
střechy	střecha	k1gFnPc4
</s>
<s>
186	#num#	k4
m	m	kA
Počet	počet	k1gInSc1
podlaží	podlaží	k1gNnSc2
</s>
<s>
45	#num#	k4
Podlahová	podlahový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
</s>
<s>
85	#num#	k4
000	#num#	k4
m	m	kA
<g/>
2	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
8	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Trianon	Trianon	k1gInSc1
je	být	k5eAaImIp3nS
mrakodrap	mrakodrap	k1gInSc1
v	v	k7c6
německém	německý	k2eAgNnSc6d1
městě	město	k1gNnSc6
Frankfurt	Frankfurt	k1gInSc1
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postaven	postaven	k2eAgInSc1d1
byl	být	k5eAaImAgInS
podle	podle	k7c2
návrhu	návrh	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
vypracovala	vypracovat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Novotny	Novotna	k1gFnSc2
Mähner	Mähnra	k1gFnPc2
Assoziierte	Assoziiert	k1gInSc5
s	s	k7c7
pomocí	pomoc	k1gFnSc7
jiných	jiný	k2eAgNnPc2d1
architektonických	architektonický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
45	#num#	k4
podlaží	podlaží	k1gNnSc2
a	a	k8xC
výšku	výška	k1gFnSc4
186	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Výstavba	výstavba	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1989	#num#	k4
-	-	kIx~
1993	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Budova	budova	k1gFnSc1
disponuje	disponovat	k5eAaBmIp3nS
přibližně	přibližně	k6eAd1
85	#num#	k4
000	#num#	k4
m	m	kA
<g/>
2	#num#	k4
převážně	převážně	k6eAd1
kancelářských	kancelářský	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Info	Info	k6eAd1
o	o	k7c6
budově	budova	k1gFnSc6
na	na	k7c6
emporis	emporis	k1gFnSc6
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
budov	budova	k1gFnPc2
v	v	k7c6
Německu	Německo	k1gNnSc6
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
budov	budova	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
