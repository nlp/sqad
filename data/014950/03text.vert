<s>
William	William	k1gInSc1
Morgan	morgan	k1gMnSc1
(	(	kIx(
<g/>
překladatel	překladatel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
William	William	k6eAd1
Morgan	morgan	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
1545	#num#	k4
<g/>
Tŷ	Tŷ	k1gMnSc1
Mawr	Mawr	k1gMnSc1
Wybrnant	Wybrnant	k1gMnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1604	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
58	#num#	k4
<g/>
–	–	k?
<g/>
59	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
St	St	kA
Asaph	Asapha	k1gFnPc2
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
St	St	kA
John	John	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
College	College	k1gFnSc7
Povolání	povolání	k1gNnSc2
</s>
<s>
jazykovědec	jazykovědec	k1gMnSc1
<g/>
,	,	kIx,
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
a	a	k8xC
překladatel	překladatel	k1gMnSc1
Bible	bible	k1gFnSc2
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
anglikánství	anglikánství	k1gNnSc1
Funkce	funkce	k1gFnSc2
</s>
<s>
Bishop	Bishop	k1gInSc1
of	of	k?
Llandaff	Llandaff	k1gInSc1
(	(	kIx(
<g/>
1595	#num#	k4
<g/>
–	–	k?
<g/>
1601	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
William	William	k1gInSc1
Morgan	morgan	k1gInSc1
(	(	kIx(
<g/>
1545	#num#	k4
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1604	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
velšský	velšský	k2eAgMnSc1d1
překladatel	překladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
domě	dům	k1gInSc6
Tŷ	Tŷ	k1gMnSc1
Mawr	Mawr	k1gMnSc1
Wybrnant	Wybrnant	k1gMnSc1
v	v	k7c6
severovelšské	severovelšský	k2eAgFnSc6d1
obci	obec	k1gFnSc6
Bro	Bro	k1gFnSc2
Machno	Machno	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
uváděný	uváděný	k2eAgInSc4d1
rok	rok	k1gInSc4
narození	narození	k1gNnSc2
je	být	k5eAaImIp3nS
1545	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
na	na	k7c4
cambridgeské	cambridgeský	k2eAgNnSc4d1
St	St	kA
John	John	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
College	College	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
překladatelem	překladatel	k1gMnSc7
celé	celá	k1gFnSc2
bible	bible	k1gFnSc2
do	do	k7c2
velštiny	velština	k1gFnSc2
(	(	kIx(
<g/>
překládal	překládat	k5eAaImAgMnS
z	z	k7c2
řečtiny	řečtina	k1gFnSc2
a	a	k8xC
hebrejštiny	hebrejština	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1595	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
llandaffským	llandaffský	k2eAgMnSc7d1
biskupem	biskup	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1601	#num#	k4
byl	být	k5eAaImAgInS
biskupem	biskup	k1gMnSc7
v	v	k7c6
St	St	kA
Asaph	Asaph	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
roku	rok	k1gInSc2
1604	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
WILLIAMS	WILLIAMS	kA
<g/>
,	,	kIx,
Glanmor	Glanmor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morgan	morgan	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velšská	velšský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
William	William	k1gInSc1
Morgan	morgan	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
123927706	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6324	#num#	k4
1996	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2006026716	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
64926842	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2006026716	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bible	bible	k1gFnSc1
</s>
