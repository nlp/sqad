<s>
Berlín	Berlín	k1gInSc1	Berlín
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
Braniborsko	Braniborsko	k1gNnSc4	Braniborsko
(	(	kIx(	(
<g/>
Brandenburg	Brandenburg	k1gInSc1	Brandenburg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
