<s>
O	o	k7c6	o
krtkovi	krtek	k1gMnSc6	krtek
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
série	série	k1gFnSc1	série
krátkých	krátký	k2eAgInPc2d1	krátký
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
výtvarníka	výtvarník	k1gMnSc2	výtvarník
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Milera	Miler	k1gMnSc2	Miler
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikala	vznikat	k5eAaImAgFnS	vznikat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1957	[number]	k4	1957
až	až	k9	až
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
kreslený	kreslený	k2eAgMnSc1d1	kreslený
krtek	krtek	k1gMnSc1	krtek
potýkající	potýkající	k2eAgMnSc1d1	potýkající
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
se	s	k7c7	s
světem	svět	k1gInSc7	svět
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zažívající	zažívající	k2eAgFnPc4d1	zažívající
různé	různý	k2eAgFnPc4d1	různá
příhody	příhoda	k1gFnPc4	příhoda
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
zvířecími	zvířecí	k2eAgMnPc7d1	zvířecí
kamarády	kamarád	k1gMnPc7	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
autora	autor	k1gMnSc2	autor
umožnila	umožnit	k5eAaPmAgFnS	umožnit
jeho	jeho	k3xOp3gFnSc1	jeho
vnučka	vnučka	k1gFnSc1	vnučka
vznik	vznik	k1gInSc4	vznik
nových	nový	k2eAgMnPc2d1	nový
dílů	díl	k1gInPc2	díl
s	s	k7c7	s
krtkem	krtek	k1gMnSc7	krtek
<g/>
,	,	kIx,	,
koprodukční	koprodukční	k2eAgInSc1d1	koprodukční
čínsko-český	čínsko-český	k2eAgInSc1d1	čínsko-český
seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
Panda	panda	k1gFnSc1	panda
<g/>
.	.	kIx.	.
</s>
<s>
Postavičku	postavička	k1gFnSc4	postavička
krtka	krtek	k1gMnSc2	krtek
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Krátký	krátký	k2eAgInSc1d1	krátký
film	film	k1gInSc1	film
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
obvykle	obvykle	k6eAd1	obvykle
uváděn	uváděn	k2eAgInSc4d1	uváděn
jako	jako	k8xC	jako
ucelený	ucelený	k2eAgInSc4d1	ucelený
televizním	televizní	k2eAgNnSc6d1	televizní
seriál	seriál	k1gInSc1	seriál
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Večerníček	večerníček	k1gInSc1	večerníček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
uveden	uveden	k2eAgInSc1d1	uveden
roku	rok	k1gInSc3	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
první	první	k4xOgInPc1	první
filmy	film	k1gInPc1	film
vznikaly	vznikat	k5eAaImAgInP	vznikat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>

