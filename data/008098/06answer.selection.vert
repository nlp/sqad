<s>
Animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
obvykle	obvykle	k6eAd1	obvykle
uváděn	uváděn	k2eAgInSc4d1	uváděn
jako	jako	k8xC	jako
ucelený	ucelený	k2eAgInSc4d1	ucelený
televizním	televizní	k2eAgNnSc6d1	televizní
seriál	seriál	k1gInSc1	seriál
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Večerníček	večerníček	k1gInSc1	večerníček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
uveden	uveden	k2eAgInSc1d1	uveden
roku	rok	k1gInSc3	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
první	první	k4xOgInPc1	první
filmy	film	k1gInPc1	film
vznikaly	vznikat	k5eAaImAgInP	vznikat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
