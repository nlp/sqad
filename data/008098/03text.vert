<s>
O	o	k7c6	o
krtkovi	krtek	k1gMnSc6	krtek
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
série	série	k1gFnSc1	série
krátkých	krátký	k2eAgInPc2d1	krátký
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
výtvarníka	výtvarník	k1gMnSc2	výtvarník
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Milera	Miler	k1gMnSc2	Miler
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikala	vznikat	k5eAaImAgFnS	vznikat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1957	[number]	k4	1957
až	až	k9	až
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
kreslený	kreslený	k2eAgMnSc1d1	kreslený
krtek	krtek	k1gMnSc1	krtek
potýkající	potýkající	k2eAgMnSc1d1	potýkající
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
se	s	k7c7	s
světem	svět	k1gInSc7	svět
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zažívající	zažívající	k2eAgFnPc4d1	zažívající
různé	různý	k2eAgFnPc4d1	různá
příhody	příhoda	k1gFnPc4	příhoda
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
zvířecími	zvířecí	k2eAgMnPc7d1	zvířecí
kamarády	kamarád	k1gMnPc7	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
autora	autor	k1gMnSc2	autor
umožnila	umožnit	k5eAaPmAgFnS	umožnit
jeho	jeho	k3xOp3gFnSc1	jeho
vnučka	vnučka	k1gFnSc1	vnučka
vznik	vznik	k1gInSc4	vznik
nových	nový	k2eAgMnPc2d1	nový
dílů	díl	k1gInPc2	díl
s	s	k7c7	s
krtkem	krtek	k1gMnSc7	krtek
<g/>
,	,	kIx,	,
koprodukční	koprodukční	k2eAgInSc1d1	koprodukční
čínsko-český	čínsko-český	k2eAgInSc1d1	čínsko-český
seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
Panda	panda	k1gFnSc1	panda
<g/>
.	.	kIx.	.
</s>
<s>
Postavičku	postavička	k1gFnSc4	postavička
krtka	krtek	k1gMnSc2	krtek
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Krátký	krátký	k2eAgInSc1d1	krátký
film	film	k1gInSc1	film
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
obvykle	obvykle	k6eAd1	obvykle
uváděn	uváděn	k2eAgInSc4d1	uváděn
jako	jako	k8xC	jako
ucelený	ucelený	k2eAgInSc4d1	ucelený
televizním	televizní	k2eAgNnSc6d1	televizní
seriál	seriál	k1gInSc1	seriál
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Večerníček	večerníček	k1gInSc1	večerníček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
uveden	uveden	k2eAgInSc1d1	uveden
roku	rok	k1gInSc3	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
první	první	k4xOgInPc1	první
filmy	film	k1gInPc1	film
vznikaly	vznikat	k5eAaImAgInP	vznikat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc1	název
Jak	jak	k8xC	jak
krtek	krtek	k1gMnSc1	krtek
ke	k	k7c3	k
kalhotkám	kalhotky	k1gFnPc3	kalhotky
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
i	i	k9	i
další	další	k2eAgFnPc1d1	další
postavičky	postavička	k1gFnPc1	postavička
jako	jako	k8xC	jako
žába	žába	k1gFnSc1	žába
<g/>
,	,	kIx,	,
čáp	čáp	k1gMnSc1	čáp
<g/>
,	,	kIx,	,
rákosník	rákosník	k1gMnSc1	rákosník
<g/>
,	,	kIx,	,
rak	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
pavouci	pavouk	k1gMnPc1	pavouk
<g/>
,	,	kIx,	,
ježek	ježek	k1gMnSc1	ježek
a	a	k8xC	a
především	především	k6eAd1	především
rostlina	rostlina	k1gFnSc1	rostlina
len	len	k1gInSc1	len
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
díly	díl	k1gInPc1	díl
přibývaly	přibývat	k5eAaImAgInP	přibývat
velmi	velmi	k6eAd1	velmi
pozvolna	pozvolna	k6eAd1	pozvolna
<g/>
,	,	kIx,	,
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
autíčko	autíčko	k1gNnSc4	autíčko
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
raketa	raketa	k1gFnSc1	raketa
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
49	[number]	k4	49
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
díly	díl	k1gInPc1	díl
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
od	od	k7c2	od
cca	cca	kA	cca
5	[number]	k4	5
min	mina	k1gFnPc2	mina
až	až	k9	až
po	po	k7c4	po
29	[number]	k4	29
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
Krtek	krtek	k1gMnSc1	krtek
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
;	;	kIx,	;
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
4	[number]	k4	4
díly	díl	k1gInPc4	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
úspěchu	úspěch	k1gInSc6	úspěch
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
a	a	k8xC	a
večerníčků	večerníček	k1gInPc2	večerníček
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
dětských	dětský	k2eAgFnPc2d1	dětská
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
kompaktních	kompaktní	k2eAgInPc2d1	kompaktní
disků	disk	k1gInPc2	disk
a	a	k8xC	a
videokazet	videokazeta	k1gFnPc2	videokazeta
s	s	k7c7	s
námětem	námět	k1gInSc7	námět
Krtka	krtek	k1gMnSc2	krtek
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
údajně	údajně	k6eAd1	údajně
postavičku	postavička	k1gFnSc4	postavička
krtka	krtek	k1gMnSc4	krtek
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zakopl	zakopnout	k5eAaPmAgMnS	zakopnout
o	o	k7c4	o
krtinu	krtina	k1gFnSc4	krtina
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
krtka	krtek	k1gMnSc4	krtek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
zvířátek	zvířátko	k1gNnPc2	zvířátko
<g/>
,	,	kIx,	,
nepoužil	použít	k5eNaPmAgMnS	použít
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
filmech	film	k1gInPc6	film
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnPc4	Disnea
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
postava	postava	k1gFnSc1	postava
krtka	krtek	k1gMnSc2	krtek
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
originální	originální	k2eAgInSc4d1	originální
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
krtka	krtek	k1gMnSc2	krtek
téměř	téměř	k6eAd1	téměř
nemluví	mluvit	k5eNaImIp3nS	mluvit
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
jen	jen	k9	jen
nemnoho	nemnoho	k6eAd1	nemnoho
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
citoslovce	citoslovce	k1gNnSc1	citoslovce
(	(	kIx(	(
<g/>
hele	hele	k0	hele
<g/>
,	,	kIx,	,
jé	jé	k0	jé
<g/>
,	,	kIx,	,
ach	ach	k0	ach
jo	jo	k9	jo
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pro	pro	k7c4	pro
první	první	k4xOgInPc4	první
díly	díl	k1gInPc4	díl
nadabovaly	nadabovat	k5eAaPmAgFnP	nadabovat
Millerovy	Millerův	k2eAgFnPc4d1	Millerova
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
film	film	k1gInSc1	film
Jak	jak	k8xC	jak
krtek	krtek	k1gMnSc1	krtek
ke	k	k7c3	k
kalhotkám	kalhotky	k1gFnPc3	kalhotky
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
mluvený	mluvený	k2eAgMnSc1d1	mluvený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
vnučka	vnučka	k1gFnSc1	vnučka
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Milera	Miler	k1gMnSc2	Miler
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
čínskou	čínský	k2eAgFnSc7d1	čínská
televizí	televize	k1gFnSc7	televize
CCTV	CCTV	kA	CCTV
o	o	k7c6	o
natáčení	natáčení	k1gNnSc6	natáčení
nových	nový	k2eAgInPc2d1	nový
příběhů	příběh	k1gInPc2	příběh
Krtečka	krteček	k1gMnSc2	krteček
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Vadima	Vadimum	k1gNnSc2	Vadimum
Petrova	Petrov	k1gInSc2	Petrov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
k	k	k7c3	k
původní	původní	k2eAgFnSc2d1	původní
příběhům	příběh	k1gInPc3	příběh
skládal	skládat	k5eAaImAgMnS	skládat
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
nepřál	přát	k5eNaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
dále	daleko	k6eAd2	daleko
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
díly	díl	k1gInPc1	díl
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
už	už	k6eAd1	už
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
3D	[number]	k4	3D
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
postava	postava	k1gFnSc1	postava
krtka	krtek	k1gMnSc2	krtek
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgFnPc3d1	předchozí
zvyklostem	zvyklost	k1gFnPc3	zvyklost
také	také	k9	také
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jeho	jeho	k3xOp3gInSc4	jeho
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
uvedla	uvést	k5eAaPmAgFnS	uvést
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc4	Barrandov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Empresa	Empresa	k1gFnSc1	Empresa
Media	medium	k1gNnSc2	medium
s	s	k7c7	s
minoritním	minoritní	k2eAgMnSc7d1	minoritní
čínským	čínský	k2eAgMnSc7d1	čínský
vlastníkem	vlastník	k1gMnSc7	vlastník
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
samostatně	samostatně	k6eAd1	samostatně
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
státní	státní	k2eAgFnSc2d1	státní
návštěvy	návštěva	k1gFnSc2	návštěva
čínského	čínský	k2eAgMnSc2d1	čínský
prezidenta	prezident	k1gMnSc2	prezident
Si	se	k3xPyFc3	se
Ťin-pchinga	Ťinchinga	k1gFnSc1	Ťin-pchinga
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
kritička	kritička	k1gFnSc1	kritička
Mirka	Mirka	k1gFnSc1	Mirka
Spáčilová	Spáčilová	k1gFnSc1	Spáčilová
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
odvysílání	odvysílání	k1gNnSc6	odvysílání
usoudila	usoudit	k5eAaPmAgFnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
krtek	krtek	k1gMnSc1	krtek
"	"	kIx"	"
<g/>
chová	chovat	k5eAaImIp3nS	chovat
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
ctitele	ctitel	k1gMnPc4	ctitel
původní	původní	k2eAgFnSc2d1	původní
postavičky	postavička	k1gFnSc2	postavička
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Milera	Miler	k1gMnSc4	Miler
vyděsit	vyděsit	k5eAaPmF	vyděsit
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
okatý	okatý	k2eAgInSc4d1	okatý
politický	politický	k2eAgInSc4d1	politický
kontext	kontext	k1gInSc4	kontext
celého	celý	k2eAgInSc2d1	celý
projektu	projekt	k1gInSc2	projekt
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
i	i	k9	i
z	z	k7c2	z
jediné	jediný	k2eAgFnSc2d1	jediná
epizody	epizoda	k1gFnSc2	epizoda
čiší	čišet	k5eAaImIp3nS	čišet
<g/>
,	,	kIx,	,
že	že	k8xS	že
krtek	krtek	k1gMnSc1	krtek
prodal	prodat	k5eAaPmAgMnS	prodat
svůj	svůj	k3xOyFgInSc4	svůj
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
lokajský	lokajský	k2eAgMnSc1d1	lokajský
<g/>
,	,	kIx,	,
upovídaný	upovídaný	k2eAgMnSc1d1	upovídaný
<g/>
,	,	kIx,	,
nyvý	nyvý	k2eAgMnSc1d1	nyvý
<g/>
,	,	kIx,	,
výtvarně	výtvarně	k6eAd1	výtvarně
konfekční	konfekční	k2eAgNnSc1d1	konfekční
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nový	nový	k2eAgInSc1d1	nový
26	[number]	k4	26
<g/>
dílný	dílný	k2eAgInSc1d1	dílný
seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
Panda	panda	k1gFnSc1	panda
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
na	na	k7c6	na
DVD	DVD	kA	DVD
a	a	k8xC	a
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
jej	on	k3xPp3gInSc4	on
stanice	stanice	k1gFnSc1	stanice
Barrandov	Barrandov	k1gInSc1	Barrandov
Plus	plus	k1gInSc1	plus
<g/>
.	.	kIx.	.
</s>
<s>
Plyšová	plyšový	k2eAgFnSc1d1	plyšová
figurka	figurka	k1gFnSc1	figurka
Krtečka	krteček	k1gMnSc2	krteček
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
poslední	poslední	k2eAgFnSc2d1	poslední
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
mise	mise	k1gFnSc2	mise
raketoplánu	raketoplán	k1gInSc2	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
propaguje	propagovat	k5eAaImIp3nS	propagovat
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
výzkum	výzkum	k1gInSc4	výzkum
všech	všecek	k3xTgFnPc2	všecek
dětí	dítě	k1gFnPc2	dítě
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
STS-134	STS-134	k1gFnSc1	STS-134
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
zahájena	zahájen	k2eAgFnSc1d1	zahájena
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
byl	být	k5eAaImAgInS	být
odložen	odložit	k5eAaPmNgInS	odložit
a	a	k8xC	a
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
se	s	k7c7	s
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Krteček	krteček	k1gMnSc1	krteček
letěl	letět	k5eAaImAgMnS	letět
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
astronauta	astronaut	k1gMnSc2	astronaut
A.	A.	kA	A.
J.	J.	kA	J.
Feustela	Feustela	k1gFnSc1	Feustela
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
manželka	manželka	k1gFnSc1	manželka
má	mít	k5eAaImIp3nS	mít
české	český	k2eAgMnPc4d1	český
předky	předek	k1gMnPc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
v	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
pak	pak	k6eAd1	pak
raketoplán	raketoplán	k1gInSc1	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
společně	společně	k6eAd1	společně
s	s	k7c7	s
Krtkem	krtek	k1gMnSc7	krtek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
oblíbenosti	oblíbenost	k1gFnSc6	oblíbenost
postavy	postava	k1gFnSc2	postava
Krtečka	krteček	k1gMnSc2	krteček
není	být	k5eNaImIp3nS	být
pochyb	pochyba	k1gFnPc2	pochyba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
svého	svůj	k3xOyFgMnSc4	svůj
maskota	maskot	k1gMnSc4	maskot
pro	pro	k7c4	pro
projekt	projekt	k1gInSc4	projekt
Naše	náš	k3xOp1gFnSc1	náš
cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
vybrali	vybrat	k5eAaPmAgMnP	vybrat
cestovatelé	cestovatel	k1gMnPc1	cestovatel
Pavel	Pavel	k1gMnSc1	Pavel
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
a	a	k8xC	a
Martina	Martina	k1gFnSc1	Martina
Libřická	Libřická	k1gFnSc1	Libřická
<g/>
.	.	kIx.	.
</s>
<s>
Krteček	krteček	k1gMnSc1	krteček
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
novými	nový	k2eAgMnPc7d1	nový
přáteli	přítel	k1gMnPc7	přítel
cestovat	cestovat	k5eAaImF	cestovat
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
poznával	poznávat	k5eAaImAgInS	poznávat
celkem	celkem	k6eAd1	celkem
527	[number]	k4	527
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yIgInPc2	který
procestoval	procestovat	k5eAaPmAgMnS	procestovat
22	[number]	k4	22
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
stovky	stovka	k1gFnPc4	stovka
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
poznal	poznat	k5eAaPmAgInS	poznat
mnoho	mnoho	k4c4	mnoho
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Krtečka	krteček	k1gMnSc2	krteček
si	se	k3xPyFc3	se
cestovatelé	cestovatel	k1gMnPc1	cestovatel
vybrali	vybrat	k5eAaPmAgMnP	vybrat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
představující	představující	k2eAgInSc1d1	představující
domov	domov	k1gInSc1	domov
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
chtěli	chtít	k5eAaImAgMnP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Krteček	krteček	k1gMnSc1	krteček
i	i	k9	i
nadále	nadále	k6eAd1	nadále
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
<g/>
.	.	kIx.	.
</s>
<s>
Krteček	krteček	k1gMnSc1	krteček
cestoval	cestovat	k5eAaImAgMnS	cestovat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kamarádem	kamarád	k1gMnSc7	kamarád
Myšákem	myšák	k1gMnSc7	myšák
<g/>
,	,	kIx,	,
známým	známý	k2eAgMnSc7d1	známý
jako	jako	k8xC	jako
Gosig	Gosig	k1gMnSc1	Gosig
Mus	Musa	k1gFnPc2	Musa
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
O	o	k7c6	o
krtkovi	krtek	k1gMnSc6	krtek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1957	[number]	k4	1957
až	až	k9	až
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
celkem	celkem	k6eAd1	celkem
49	[number]	k4	49
krátkých	krátký	k2eAgInPc2d1	krátký
snímků	snímek	k1gInPc2	snímek
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
od	od	k7c2	od
5	[number]	k4	5
až	až	k9	až
po	po	k7c4	po
28	[number]	k4	28
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jiný	jiný	k2eAgInSc4d1	jiný
počet	počet	k1gInSc4	počet
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
uvádí	uvádět	k5eAaImIp3nS	uvádět
51	[number]	k4	51
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgMnPc4	dva
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
chybně	chybně	k6eAd1	chybně
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gInSc6	Databas
napočítali	napočítat	k5eAaPmAgMnP	napočítat
63	[number]	k4	63
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
chybějících	chybějící	k2eAgInPc2d1	chybějící
14	[number]	k4	14
dílů	díl	k1gInPc2	díl
má	mít	k5eAaImIp3nS	mít
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
krátké	krátká	k1gFnPc1	krátká
skeče	skeč	k1gInSc2	skeč
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
minutové	minutový	k2eAgFnPc1d1	minutová
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
údajně	údajně	k6eAd1	údajně
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
na	na	k7c4	na
DVD	DVD	kA	DVD
jako	jako	k8xS	jako
bonus	bonus	k1gInSc1	bonus
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
seznam	seznam	k1gInSc1	seznam
<g/>
:	:	kIx,	:
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
televize	televize	k1gFnSc1	televize
(	(	kIx(	(
<g/>
neplést	plést	k5eNaImF	plést
s	s	k7c7	s
dílem	díl	k1gInSc7	díl
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
televizor	televizor	k1gInSc1	televizor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
žabka	žabka	k1gFnSc1	žabka
(	(	kIx(	(
<g/>
shoda	shoda	k1gFnSc1	shoda
jmen	jméno	k1gNnPc2	jméno
s	s	k7c7	s
dílem	díl	k1gInSc7	díl
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
neplést	plést	k5eNaImF	plést
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
Krtek	krtek	k1gMnSc1	krtek
malířem	malíř	k1gMnSc7	malíř
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
balónek	balónek	k1gInSc1	balónek
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
dovolená	dovolená	k1gFnSc1	dovolená
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
domek	domek	k1gInSc1	domek
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
třešně	třešně	k1gFnSc1	třešně
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
šnek	šnek	k1gMnSc1	šnek
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
šperky	šperk	k1gInPc7	šperk
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
myška	myška	k1gFnSc1	myška
(	(	kIx(	(
<g/>
shoda	shoda	k1gFnSc1	shoda
jmen	jméno	k1gNnPc2	jméno
s	s	k7c7	s
dílem	díl	k1gInSc7	díl
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
také	také	k9	také
někdy	někdy	k6eAd1	někdy
uváděným	uváděný	k2eAgFnPc3d1	uváděná
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
potopa	potopa	k1gFnSc1	potopa
<g/>
)	)	kIx)	)
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
krabice	krabice	k1gFnSc1	krabice
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
klobouk	klobouk	k1gInSc1	klobouk
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
kouzelník	kouzelník	k1gMnSc1	kouzelník
Krtek	krtek	k1gMnSc1	krtek
a	a	k8xC	a
kačenka	kačenka	k1gFnSc1	kačenka
</s>
