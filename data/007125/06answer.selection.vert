<s>
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
vařením	vaření	k1gNnSc7
různých	různý	k2eAgFnPc2d1
přísad	přísada	k1gFnPc2
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1
předávají	předávat	k5eAaImIp3nP
chuť	chuť	k1gFnSc4
<g/>
,	,	kIx,
barvu	barva	k1gFnSc4
i	i	k8xC
vůni	vůně	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
předem	předem	k6eAd1
připraveném	připravený	k2eAgInSc6d1
vývaru	vývar	k1gInSc6
<g/>
.	.	kIx.
</s>