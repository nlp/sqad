<s>
Polévka	polévka	k1gFnSc1	polévka
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
polívka	polívka	k1gFnSc1	polívka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tekuté	tekutý	k2eAgNnSc1d1	tekuté
až	až	k9	až
mírně	mírně	k6eAd1	mírně
husté	hustý	k2eAgInPc1d1	hustý
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
teplé	teplý	k2eAgNnSc4d1	teplé
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
vařením	vaření	k1gNnSc7	vaření
různých	různý	k2eAgFnPc2d1	různá
přísad	přísada	k1gFnPc2	přísada
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
předávají	předávat	k5eAaImIp3nP	předávat
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
barvu	barva	k1gFnSc4	barva
i	i	k8xC	i
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
předem	předem	k6eAd1	předem
připraveném	připravený	k2eAgInSc6d1	připravený
vývaru	vývar	k1gInSc6	vývar
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
suroviny	surovina	k1gFnPc1	surovina
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnSc2	luštěnina
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Polévky	polévka	k1gFnPc1	polévka
bývají	bývat	k5eAaImIp3nP	bývat
buď	buď	k8xC	buď
čiré	čirý	k2eAgFnPc4d1	čirá
<g/>
,	,	kIx,	,
mléčné	mléčný	k2eAgFnPc4d1	mléčná
<g/>
,	,	kIx,	,
zahuštěné	zahuštěný	k2eAgFnPc4d1	zahuštěná
např.	např.	kA	např.
moukou	mouka	k1gFnSc7	mouka
<g/>
,	,	kIx,	,
rozmixovanou	rozmixovaný	k2eAgFnSc7d1	rozmixovaná
zeleninou	zelenina	k1gFnSc7	zelenina
(	(	kIx(	(
<g/>
krém	krém	k1gInSc1	krém
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
nebo	nebo	k8xC	nebo
také	také	k9	také
s	s	k7c7	s
pevnými	pevný	k2eAgInPc7d1	pevný
–	–	k?	–
zpravidla	zpravidla	k6eAd1	zpravidla
vařenými	vařený	k2eAgFnPc7d1	vařená
–	–	k?	–
potravinami	potravina	k1gFnPc7	potravina
<g/>
,	,	kIx,	,
zavářkami	zavářka	k1gFnPc7	zavářka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
rýží	rýže	k1gFnSc7	rýže
<g/>
,	,	kIx,	,
těstovinami	těstovina	k1gFnPc7	těstovina
apod.	apod.	kA	apod.
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
polévka	polévka	k1gFnSc1	polévka
většinou	většinou	k6eAd1	většinou
solí	solit	k5eAaImIp3nS	solit
<g/>
,	,	kIx,	,
dochucuje	dochucovat	k5eAaImIp3nS	dochucovat
kořením	koření	k1gNnSc7	koření
a	a	k8xC	a
omašťuje	omašťovat	k5eAaImIp3nS	omašťovat
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
máslem	máslo	k1gNnSc7	máslo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zjemňuje	zjemňovat	k5eAaImIp3nS	zjemňovat
smetanou	smetana	k1gFnSc7	smetana
<g/>
.	.	kIx.	.
</s>
<s>
Polévka	polévka	k1gFnSc1	polévka
zpravidla	zpravidla	k6eAd1	zpravidla
předchází	předcházet	k5eAaImIp3nS	předcházet
hlavní	hlavní	k2eAgInSc4d1	hlavní
chod	chod	k1gInSc4	chod
(	(	kIx(	(
<g/>
u	u	k7c2	u
oběda	oběd	k1gInSc2	oběd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
bohatším	bohatý	k2eAgNnSc6d2	bohatší
menu	menu	k1gNnSc6	menu
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
po	po	k7c6	po
studeném	studený	k2eAgInSc6d1	studený
předkrmu	předkrm	k1gInSc6	předkrm
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
případně	případně	k6eAd1	případně
ještě	ještě	k6eAd1	ještě
teplým	teplý	k2eAgInSc7d1	teplý
předkrmem	předkrm	k1gInSc7	předkrm
(	(	kIx(	(
<g/>
snackem	snack	k1gInSc7	snack
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
podávat	podávat	k5eAaImF	podávat
jako	jako	k9	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
jídlo	jídlo	k1gNnSc4	jídlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
k	k	k7c3	k
večeři	večeře	k1gFnSc3	večeře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
pečivem	pečivo	k1gNnSc7	pečivo
resp.	resp.	kA	resp.
chlebem	chléb	k1gInSc7	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Polévka	polévka	k1gFnSc1	polévka
je	být	k5eAaImIp3nS	být
připravována	připravován	k2eAgFnSc1d1	připravována
buď	buď	k8xC	buď
tradičním	tradiční	k2eAgInSc7d1	tradiční
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
vyvařeny	vyvařit	k5eAaPmNgFnP	vyvařit
originální	originální	k2eAgFnPc1d1	originální
suroviny	surovina	k1gFnPc1	surovina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
z	z	k7c2	z
instantní	instantní	k2eAgFnSc2d1	instantní
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
polévka	polévka	k1gFnSc1	polévka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
vyvařeno	vyvařit	k5eAaPmNgNnS	vyvařit
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
ryba	ryba	k1gFnSc1	ryba
či	či	k8xC	či
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
též	též	k9	též
nazývá	nazývat	k5eAaImIp3nS	nazývat
vývar	vývar	k1gInSc1	vývar
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tradičního	tradiční	k2eAgNnSc2d1	tradiční
vaření	vaření	k1gNnSc2	vaření
polévky	polévka	k1gFnSc2	polévka
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
příprava	příprava	k1gFnSc1	příprava
<g/>
,	,	kIx,	,
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
suroviny	surovina	k1gFnPc1	surovina
bývají	bývat	k5eAaImIp3nP	bývat
čerstvé	čerstvý	k2eAgFnPc1d1	čerstvá
a	a	k8xC	a
polévka	polévka	k1gFnSc1	polévka
většinou	většinou	k6eAd1	většinou
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
přídatné	přídatný	k2eAgFnPc4d1	přídatná
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nedochucuje	dochucovat	k5eNaImIp3nS	dochucovat
masoxem	masox	k1gInSc7	masox
či	či	k8xC	či
jinými	jiný	k2eAgInPc7d1	jiný
dochucovacími	dochucovací	k2eAgInPc7d1	dochucovací
a	a	k8xC	a
solícími	solící	k2eAgInPc7d1	solící
přípravky	přípravek	k1gInPc7	přípravek
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Instantní	instantní	k2eAgFnSc1d1	instantní
polévka	polévka	k1gFnSc1	polévka
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
připravuje	připravovat	k5eAaImIp3nS	připravovat
5	[number]	k4	5
až	až	k9	až
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
prostým	prostý	k2eAgNnSc7d1	prosté
vsypáním	vsypání	k1gNnSc7	vsypání
do	do	k7c2	do
studené	studený	k2eAgFnSc2d1	studená
či	či	k8xC	či
vroucí	vroucí	k2eAgFnSc2d1	vroucí
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
připadně	připadně	k6eAd1	připadně
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
ještě	ještě	k9	ještě
nějaká	nějaký	k3yIgFnSc1	nějaký
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
ingredience	ingredience	k1gFnSc1	ingredience
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zhotovována	zhotovovat	k5eAaImNgFnS	zhotovovat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pytlíková	pytlíkový	k2eAgFnSc1d1	Pytlíková
polévka	polévka	k1gFnSc1	polévka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
v	v	k7c6	v
restauracích	restaurace	k1gFnPc6	restaurace
a	a	k8xC	a
stravovacích	stravovací	k2eAgFnPc6d1	stravovací
provozovnách	provozovna	k1gFnPc6	provozovna
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	být	k5eAaImIp3nS	být
dodávána	dodávat	k5eAaImNgFnS	dodávat
v	v	k7c4	v
gastro	gastro	k1gNnSc4	gastro
balení	balení	k1gNnSc2	balení
<g/>
.	.	kIx.	.
</s>
<s>
Yaka	Yaka	k1gMnSc1	Yaka
mein	mein	k1gMnSc1	mein
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
hovězí	hovězí	k2eAgFnSc2d1	hovězí
nudlové	nudlový	k2eAgFnSc2d1	nudlová
polévky	polévka	k1gFnSc2	polévka
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
čínských	čínský	k2eAgFnPc6d1	čínská
restauracích	restaurace	k1gFnPc6	restaurace
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc4	Orleans
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
dušeného	dušený	k2eAgNnSc2d1	dušené
hovězího	hovězí	k1gNnSc2	hovězí
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
špagetových	špagetový	k2eAgFnPc2d1	špagetová
nudlí	nudle	k1gFnPc2	nudle
<g/>
,	,	kIx,	,
vejce	vejce	k1gNnSc1	vejce
uvařené	uvařený	k2eAgNnSc1d1	uvařené
natvrdo	natvrdo	k6eAd1	natvrdo
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
cibule	cibule	k1gFnPc1	cibule
a	a	k8xC	a
cajunského	cajunský	k2eAgNnSc2d1	cajunský
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
chilli	chilli	k1gNnSc2	chilli
nebo	nebo	k8xC	nebo
koření	koření	k1gNnSc2	koření
Old	Olda	k1gFnPc2	Olda
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Čirá	čirý	k2eAgFnSc1d1	čirá
polévka	polévka	k1gFnSc1	polévka
je	být	k5eAaImIp3nS	být
vývar	vývar	k1gInSc4	vývar
z	z	k7c2	z
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
jako	jako	k8xS	jako
základ	základ	k1gInSc4	základ
polévky	polévka	k1gFnSc2	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
masa	maso	k1gNnSc2	maso
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
i	i	k9	i
sama	sám	k3xTgFnSc1	sám
polévka	polévka	k1gFnSc1	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
maso	maso	k1gNnSc1	maso
hovězí	hovězí	k2eAgNnSc1d1	hovězí
nebo	nebo	k8xC	nebo
drůbeží	drůbeží	k2eAgNnSc1d1	drůbeží
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
zástupcem	zástupce	k1gMnSc7	zástupce
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
je	být	k5eAaImIp3nS	být
hovězí	hovězí	k2eAgFnSc1d1	hovězí
polévka	polévka	k1gFnSc1	polévka
(	(	kIx(	(
<g/>
vývar	vývar	k1gInSc1	vývar
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
bujón	bujón	k1gInSc1	bujón
<g/>
,	,	kIx,	,
slepičí	slepičí	k2eAgFnSc1d1	slepičí
nebo	nebo	k8xC	nebo
kuřecí	kuřecí	k2eAgFnSc1d1	kuřecí
polévka	polévka	k1gFnSc1	polévka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vývaru	vývar	k1gInSc3	vývar
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
do	do	k7c2	do
polévky	polévka	k1gFnSc2	polévka
přidávají	přidávat	k5eAaImIp3nP	přidávat
přísady	přísada	k1gFnPc1	přísada
<g/>
:	:	kIx,	:
nudlová	nudlový	k2eAgFnSc1d1	nudlová
polévka	polévka	k1gFnSc1	polévka
<g/>
,	,	kIx,	,
polévka	polévka	k1gFnSc1	polévka
s	s	k7c7	s
játrovými	játrový	k2eAgInPc7d1	játrový
knedlíčky	knedlíček	k1gInPc7	knedlíček
<g/>
,	,	kIx,	,
fritátová	fritátový	k2eAgFnSc1d1	fritátový
polévka	polévka	k1gFnSc1	polévka
<g/>
,	,	kIx,	,
polévka	polévka	k1gFnSc1	polévka
s	s	k7c7	s
uzeným	uzený	k2eAgNnSc7d1	uzené
masem	maso	k1gNnSc7	maso
<g/>
,	,	kIx,	,
cibulová	cibulový	k2eAgFnSc1d1	cibulová
polévka	polévka	k1gFnSc1	polévka
aj.	aj.	kA	aj.
Zahuštěné	zahuštěný	k2eAgFnSc2d1	zahuštěná
polévky	polévka	k1gFnSc2	polévka
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnPc4	takový
polévky	polévka	k1gFnPc4	polévka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgFnPc2d1	různá
přísad	přísada	k1gFnPc2	přísada
zahušťují	zahušťovat	k5eAaImIp3nP	zahušťovat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
různé	různý	k2eAgFnPc1d1	různá
krémové	krémový	k2eAgFnPc1d1	krémová
polévky	polévka	k1gFnPc1	polévka
nebo	nebo	k8xC	nebo
polévky	polévka	k1gFnPc1	polévka
zahuštěné	zahuštěný	k2eAgFnPc1d1	zahuštěná
jíškou	jíška	k1gFnSc7	jíška
a	a	k8xC	a
také	také	k9	také
polévky	polévka	k1gFnPc4	polévka
zahuštěné	zahuštěný	k2eAgInPc1d1	zahuštěný
jinými	jiný	k2eAgFnPc7d1	jiná
přísadami	přísada	k1gFnPc7	přísada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vaří	vařit	k5eAaImIp3nP	vařit
společně	společně	k6eAd1	společně
s	s	k7c7	s
masem	maso	k1gNnSc7	maso
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
hrách	hrách	k1gInSc1	hrách
<g/>
,	,	kIx,	,
čočka	čočka	k1gFnSc1	čočka
<g/>
,	,	kIx,	,
fazole	fazole	k1gFnSc1	fazole
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
,	,	kIx,	,
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
smetana	smetana	k1gFnSc1	smetana
nebo	nebo	k8xC	nebo
syrové	syrový	k2eAgNnSc1d1	syrové
vejce	vejce	k1gNnSc1	vejce
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
vaječný	vaječný	k2eAgInSc1d1	vaječný
žloutek	žloutek	k1gInSc1	žloutek
<g/>
.	.	kIx.	.
bramboračka	bramboračka	k1gFnSc1	bramboračka
(	(	kIx(	(
<g/>
bramborová	bramborový	k2eAgFnSc1d1	bramborová
polévka	polévka	k1gFnSc1	polévka
<g/>
)	)	kIx)	)
kulajda	kulajda	k1gFnSc1	kulajda
dršťková	dršťková	k1gFnSc1	dršťková
polévka	polévka	k1gFnSc1	polévka
slepičí	slepičit	k5eAaImIp3nS	slepičit
polévka	polévka	k1gFnSc1	polévka
hovězí	hovězí	k2eAgFnSc1d1	hovězí
vývar	vývar	k1gInSc4	vývar
drůbeží	drůbeží	k2eAgInSc1d1	drůbeží
vývar	vývar	k1gInSc1	vývar
hrášková	hráškový	k2eAgFnSc1d1	hrášková
polévka	polévka	k1gFnSc1	polévka
gulášovka	gulášovka	k1gFnSc1	gulášovka
(	(	kIx(	(
<g/>
gulášová	gulášový	k2eAgFnSc1d1	gulášová
polévka	polévka	k1gFnSc1	polévka
<g/>
)	)	kIx)	)
cibulačka	cibulačka	k1gFnSc1	cibulačka
(	(	kIx(	(
<g/>
cibulová	cibulový	k2eAgFnSc1d1	cibulová
polévka	polévka	k1gFnSc1	polévka
<g/>
)	)	kIx)	)
zelňačka	zelňačka	k1gFnSc1	zelňačka
(	(	kIx(	(
<g/>
zelná	zelný	k2eAgFnSc1d1	zelná
polévka	polévka	k1gFnSc1	polévka
<g/>
)	)	kIx)	)
čočková	čočkový	k2eAgFnSc1d1	čočková
polévka	polévka	k1gFnSc1	polévka
fazolová	fazolový	k2eAgFnSc1d1	fazolová
polévka	polévka	k1gFnSc1	polévka
polévka	polévka	k1gFnSc1	polévka
s	s	k7c7	s
játrovými	játrový	k2eAgInPc7d1	játrový
knedlíčky	knedlíček	k1gInPc7	knedlíček
kyselo	kyselo	k6eAd1	kyselo
</s>
