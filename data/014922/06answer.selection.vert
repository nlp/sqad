<s>
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
tvořena	tvořit	k5eAaImNgFnS
čtyřmi	čtyři	k4xCgFnPc7
linkami	linka	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
plně	plně	k6eAd1
zapojeny	zapojit	k5eAaPmNgFnP
do	do	k7c2
Integrovaného	integrovaný	k2eAgInSc2d1
dopravního	dopravní	k2eAgInSc2d1
systému	systém	k1gInSc2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
IDS	IDS	kA
JMK	JMK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>