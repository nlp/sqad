<s>
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
</s>
<s>
Autobus	autobus	k1gInSc1
Karosa	karosa	k1gFnSc1
C	C	kA
954	#num#	k4
na	na	k7c6
městské	městský	k2eAgFnSc6d1
lince	linka	k1gFnSc6
743	#num#	k4
</s>
<s>
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
tvořena	tvořit	k5eAaImNgFnS
čtyřmi	čtyři	k4xCgFnPc7
linkami	linka	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
plně	plně	k6eAd1
zapojeny	zapojit	k5eAaPmNgFnP
do	do	k7c2
Integrovaného	integrovaný	k2eAgInSc2d1
dopravního	dopravní	k2eAgInSc2d1
systému	systém	k1gInSc2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
IDS	IDS	kA
JMK	JMK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopravcem	dopravce	k1gMnSc7
na	na	k7c6
těchto	tento	k3xDgFnPc6
linkách	linka	k1gFnPc6
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
VYDOS	VYDOS	kA
BUS	bus	k1gInSc1
a.	a.	k?
s.	s.	k?
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
MHD	MHD	kA
Vyškov	Vyškov	k1gInSc1
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
k	k	k7c3
roku	rok	k1gInSc3
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
autobusovém	autobusový	k2eAgInSc6d1
jízdním	jízdní	k2eAgInSc6d1
řádu	řád	k1gInSc6
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
jsou	být	k5eAaImIp3nP
uvedeny	uveden	k2eAgInPc1d1
dvě	dva	k4xCgFnPc1
linky	linka	k1gFnPc1
městské	městský	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc4
vedoucí	vedoucí	k1gFnPc4
v	v	k7c6
polookružních	polookružní	k2eAgFnPc6d1
trasách	trasa	k1gFnPc6
a	a	k8xC
začínající	začínající	k2eAgMnPc4d1
i	i	k9
končící	končící	k2eAgNnSc1d1
v	v	k7c6
Nosálovicích	Nosálovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linka	linka	k1gFnSc1
č.	č.	k?
1	#num#	k4
jezdila	jezdit	k5eAaImAgFnS
v	v	k7c6
trase	trasa	k1gFnSc6
Nosálovice	Nosálovice	k1gFnSc2
–	–	k?
Autobusové	autobusový	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
–	–	k?
Dukelská	dukelský	k2eAgFnSc1d1
–	–	k?
Tržiště	tržiště	k1gNnSc1
–	–	k?
Křečkovice	Křečkovice	k1gFnSc2
–	–	k?
Brňany	Brňan	k1gMnPc4
–	–	k?
Dvořákova	Dvořákův	k2eAgMnSc4d1
–	–	k?
Železniční	železniční	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
–	–	k?
Autobusové	autobusový	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
–	–	k?
Nosálovice	Nosálovice	k1gFnSc1
<g/>
,	,	kIx,
linka	linka	k1gFnSc1
č.	č.	k?
2	#num#	k4
potom	potom	k8xC
v	v	k7c6
trase	trasa	k1gFnSc6
Nosálovice	Nosálovice	k1gFnSc2
–	–	k?
Autobusové	autobusový	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
–	–	k?
Palánek	Palánek	k1gInSc1
–	–	k?
Dvořákova	Dvořákův	k2eAgFnSc1d1
–	–	k?
Dukelská	dukelský	k2eAgFnSc1d1
–	–	k?
Tržiště	tržiště	k1gNnSc2
–	–	k?
Hybešova	Hybešův	k2eAgFnSc1d1
–	–	k?
Dukelská	dukelský	k2eAgFnSc1d1
–	–	k?
Železniční	železniční	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
–	–	k?
Autobusové	autobusový	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
–	–	k?
Nosálovice	Nosálovice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
linky	linka	k1gFnPc1
provozoval	provozovat	k5eAaImAgMnS
národní	národní	k2eAgInSc4d1
podnik	podnik	k1gInSc4
ČSAD	ČSAD	kA
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
dopravní	dopravní	k2eAgInSc1d1
závod	závod	k1gInSc1
Vyškov	Vyškov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
do	do	k7c2
současnosti	současnost	k1gFnSc2
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2002	#num#	k4
(	(	kIx(
<g/>
rok	rok	k1gInSc4
před	před	k7c7
zahájením	zahájení	k1gNnSc7
provozu	provoz	k1gInSc2
IDS	IDS	kA
JMK	JMK	kA
<g/>
)	)	kIx)
existovaly	existovat	k5eAaImAgInP
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
dvě	dva	k4xCgFnPc1
autobusové	autobusový	k2eAgFnPc1d1
linky	linka	k1gFnPc1
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byly	být	k5eAaImAgFnP
označení	označení	k1gNnSc4
čísly	číslo	k1gNnPc7
1	#num#	k4
a	a	k8xC
2	#num#	k4
a	a	k8xC
de	de	k?
facto	facto	k1gNnSc1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
linku	linka	k1gFnSc4
jedinou	jediný	k2eAgFnSc4d1
<g/>
,	,	kIx,
protínající	protínající	k2eAgInPc1d1
město	město	k1gNnSc4
ve	v	k7c6
směru	směr	k1gInSc6
jihozápad	jihozápad	k1gInSc1
–	–	k?
severovýchod	severovýchod	k1gInSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
každý	každý	k3xTgInSc1
směr	směr	k1gInSc1
byl	být	k5eAaImAgInS
označen	označit	k5eAaPmNgMnS
svým	svůj	k3xOyFgNnSc7
číslem	číslo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
rozdílného	rozdílný	k2eAgNnSc2d1
značení	značení	k1gNnSc2
byl	být	k5eAaImAgInS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
třech	tři	k4xCgFnPc6
oblastech	oblast	k1gFnPc6
vedly	vést	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
linky	linka	k1gFnPc1
jinými	jiný	k2eAgFnPc7d1
ulicemi	ulice	k1gFnPc7
<g/>
,	,	kIx,
či	či	k8xC
naopak	naopak	k6eAd1
stejnou	stejný	k2eAgFnSc7d1
ulicí	ulice	k1gFnSc7
stejným	stejný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trasování	trasování	k1gNnSc1
bylo	být	k5eAaImAgNnS
následující	následující	k2eAgMnSc1d1
<g/>
:	:	kIx,
Poliklinika	poliklinika	k1gFnSc1
–	–	k?
Nouzka	Nouzka	k1gFnSc1
–	–	k?
Autobusové	autobusový	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
–	–	k?
Železniční	železniční	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
–	–	k?
centrum	centrum	k1gNnSc1
města	město	k1gNnSc2
–	–	k?
Křečkovice	Křečkovice	k1gFnSc1
–	–	k?
Tržiště	tržiště	k1gNnSc1
–	–	k?
Tovární	tovární	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
s	s	k7c7
těmito	tento	k3xDgFnPc7
městskými	městský	k2eAgFnPc7d1
linkami	linka	k1gFnPc7
existovaly	existovat	k5eAaImAgFnP
i	i	k9
další	další	k2eAgFnPc1d1
tři	tři	k4xCgFnPc1
linky	linka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jezdily	jezdit	k5eAaImAgFnP
mezi	mezi	k7c7
městem	město	k1gNnSc7
a	a	k8xC
areálem	areál	k1gInSc7
vojenských	vojenský	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
nacházejících	nacházející	k2eAgFnPc2d1
se	se	k3xPyFc4
v	v	k7c6
městských	městský	k2eAgFnPc6d1
částech	část	k1gFnPc6
Dědice	dědic	k1gMnSc2
a	a	k8xC
Hamiltony	Hamilton	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
ale	ale	k9
formálně	formálně	k6eAd1
součástí	součást	k1gFnPc2
vyškovské	vyškovský	k2eAgFnSc2d1
MHD	MHD	kA
nebyly	být	k5eNaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všech	všecek	k3xTgMnPc2
pět	pět	k4xCc4
těchto	tento	k3xDgFnPc2
linek	linka	k1gFnPc2
provozovala	provozovat	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
ČSAD	ČSAD	kA
Vyškov	Vyškov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2004	#num#	k4
byla	být	k5eAaImAgFnS
zprovozněna	zprovoznit	k5eAaPmNgFnS
třetí	třetí	k4xOgFnSc1
městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
(	(	kIx(
<g/>
č.	č.	k?
3	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
trase	trasa	k1gFnSc6
Autobusové	autobusový	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
–	–	k?
sídliště	sídliště	k1gNnSc1
Osvobození	osvobození	k1gNnSc2
–	–	k?
Poliklinika	poliklinika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
zanikla	zaniknout	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
ČSAD	ČSAD	kA
Vyškov	Vyškov	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
rozdělila	rozdělit	k5eAaPmAgFnS
do	do	k7c2
několika	několik	k4yIc2
samostatných	samostatný	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Provozovatelem	provozovatel	k1gMnSc7
vyškovské	vyškovský	k2eAgFnSc2d1
MHD	MHD	kA
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
nově	nově	k6eAd1
vzniklých	vzniklý	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
VYDOS	VYDOS	kA
BUS	bus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vyškovský	vyškovský	k2eAgInSc1d1
autobus	autobus	k1gInSc1
Irisbus	Irisbus	k1gInSc4
Crossway	Crosswaa	k1gFnSc2
</s>
<s>
Již	již	k6eAd1
od	od	k7c2
vzniku	vznik	k1gInSc2
IDS	IDS	kA
JMK	JMK	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
jeho	jeho	k3xOp3gFnPc1
součástí	součást	k1gFnSc7
dvě	dva	k4xCgFnPc1
regionální	regionální	k2eAgFnPc1d1
autobusové	autobusový	k2eAgFnPc1d1
linky	linka	k1gFnPc1
vedoucí	vedoucí	k2eAgFnPc1d1
z	z	k7c2
Vyškova	Vyškov	k1gInSc2
do	do	k7c2
Blanska	Blansko	k1gNnSc2
a	a	k8xC
Adamova	Adamov	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Vyškovská	vyškovský	k2eAgFnSc1d1
městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
byla	být	k5eAaImAgFnS
do	do	k7c2
IDS	IDS	kA
JMK	JMK	kA
zaintegrována	zaintegrovat	k5eAaPmNgNnP,k5eAaBmNgNnP,k5eAaImNgNnP
v	v	k7c6
rámci	rámec	k1gInSc6
etapy	etapa	k1gFnSc2
3A	3A	k4
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2005	#num#	k4
(	(	kIx(
<g/>
celý	celý	k2eAgInSc1d1
Vyškov	Vyškov	k1gInSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
zóně	zóna	k1gFnSc6
740	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc4
páteřní	páteřní	k2eAgFnPc4d1
linky	linka	k1gFnPc4
č.	č.	k?
1	#num#	k4
a	a	k8xC
2	#num#	k4
byly	být	k5eAaImAgInP
přečíslovány	přečíslován	k2eAgInPc1d1
na	na	k7c4
č.	č.	k?
741	#num#	k4
a	a	k8xC
742	#num#	k4
<g/>
,	,	kIx,
linka	linka	k1gFnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
na	na	k7c6
jaře	jaro	k1gNnSc6
2004	#num#	k4
obdržela	obdržet	k5eAaPmAgFnS
č.	č.	k?
743	#num#	k4
(	(	kIx(
<g/>
zároveň	zároveň	k6eAd1
byla	být	k5eAaImAgFnS
upravena	upravit	k5eAaPmNgFnS
její	její	k3xOp3gFnSc1
trasa	trasa	k1gFnSc1
závlekem	závlek	k1gInSc7
od	od	k7c2
železniční	železniční	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
do	do	k7c2
zastávky	zastávka	k1gFnSc2
Palánek	Palánek	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
byla	být	k5eAaImAgFnS
do	do	k7c2
MHD	MHD	kA
zařazena	zařazen	k2eAgFnSc1d1
linka	linka	k1gFnSc1
Tovární	tovární	k2eAgFnSc2d1
–	–	k?
střed	střed	k1gInSc1
města	město	k1gNnSc2
–	–	k?
Dědice	dědic	k1gMnSc2
<g/>
,	,	kIx,
VVŠ	VVŠ	kA
(	(	kIx(
<g/>
k	k	k7c3
vojenským	vojenský	k2eAgFnPc3d1
vysokým	vysoký	k2eAgFnPc3d1
školám	škola	k1gFnPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
sloučením	sloučení	k1gNnSc7
dosavadních	dosavadní	k2eAgFnPc2d1
linek	linka	k1gFnPc2
vedoucích	vedoucí	k1gMnPc2
tam	tam	k6eAd1
a	a	k8xC
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
obdržela	obdržet	k5eAaPmAgFnS
č.	č.	k?
744	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2006	#num#	k4
byly	být	k5eAaImAgFnP
<g />
.	.	kIx.
</s>
<s hack="1">
některé	některý	k3yIgInPc4
spoje	spoj	k1gInPc4
linek	linka	k1gFnPc2
741	#num#	k4
<g/>
,	,	kIx,
742	#num#	k4
a	a	k8xC
744	#num#	k4
prodlouženy	prodloužit	k5eAaPmNgFnP
ze	z	k7c2
stávající	stávající	k2eAgFnSc2d1
konečné	konečný	k2eAgFnSc2d1
Tovární	tovární	k2eAgInSc4d1
(	(	kIx(
<g/>
v	v	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
<g/>
)	)	kIx)
do	do	k7c2
nové	nový	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
Pustiměřská	Pustiměřský	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2007	#num#	k4
byla	být	k5eAaImAgFnS
linka	linka	k1gFnSc1
č.	č.	k?
743	#num#	k4
prodloužena	prodloužit	k5eAaPmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
Palánku	Palánek	k1gInSc2
vytvoří	vytvořit	k5eAaPmIp3nS
velkou	velký	k2eAgFnSc4d1
smyčku	smyčka	k1gFnSc4
přes	přes	k7c4
Křečkovice	Křečkovice	k1gFnPc4
a	a	k8xC
ulice	ulice	k1gFnPc4
Tržiště	tržiště	k1gNnSc2
a	a	k8xC
Tyršovu	Tyršův	k2eAgFnSc4d1
a	a	k8xC
přes	přes	k7c4
Palánek	Palánek	k1gInSc4
pokračuje	pokračovat	k5eAaImIp3nS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
dosavadní	dosavadní	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
k	k	k7c3
Poliklinice	poliklinika	k1gFnSc3
či	či	k8xC
na	na	k7c4
autobusové	autobusový	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
linek	linka	k1gFnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
jezdily	jezdit	k5eAaImAgFnP
autobusové	autobusový	k2eAgFnPc1d1
linky	linka	k1gFnPc1
vyškovské	vyškovský	k2eAgFnSc2d1
MHD	MHD	kA
po	po	k7c6
těchto	tento	k3xDgFnPc6
trasách	trasa	k1gFnPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
741	#num#	k4
Poliklinika	poliklinika	k1gFnSc1
–	–	k?
Autobusové	autobusový	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
–	–	k?
Palánek	Palánek	k1gInSc1
–	–	k?
Tovární	tovární	k2eAgInSc1d1
</s>
<s>
742	#num#	k4
Tovární	tovární	k2eAgInSc1d1
–	–	k?
Agrodům	Agrod	k1gInPc3
–	–	k?
Autobusové	autobusový	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
–	–	k?
Poliklinika	poliklinika	k1gFnSc1
</s>
<s>
743	#num#	k4
Autobusové	autobusový	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
–	–	k?
Sídliště	sídliště	k1gNnSc1
Osvobození	osvobození	k1gNnSc2
–	–	k?
Poliklinika	poliklinika	k1gFnSc1
</s>
<s>
744	#num#	k4
VTÚPV	VTÚPV	kA
–	–	k?
VVŠ	VVŠ	kA
–	–	k?
Na	na	k7c6
vyhlídce	vyhlídka	k1gFnSc6
–	–	k?
Tovární	tovární	k2eAgFnSc1d1
</s>
<s>
Tarif	tarif	k1gInSc1
</s>
<s>
Tarif	tarif	k1gInSc1
ČSAD	ČSAD	kA
Vyškov	Vyškov	k1gInSc1
na	na	k7c6
linkách	linka	k1gFnPc6
MHD	MHD	kA
byl	být	k5eAaImAgInS
stejný	stejný	k2eAgInSc1d1
jako	jako	k9
na	na	k7c6
ostatních	ostatní	k2eAgFnPc6d1
autobusových	autobusový	k2eAgFnPc6d1
linkách	linka	k1gFnPc6
<g/>
,	,	kIx,
provozovaných	provozovaný	k2eAgFnPc6d1
touto	tento	k3xDgFnSc7
firmou	firma	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pásmový	pásmový	k2eAgMnSc1d1
<g/>
,	,	kIx,
odstupňovaný	odstupňovaný	k2eAgMnSc1d1
po	po	k7c6
dvou	dva	k4xCgInPc6
až	až	k6eAd1
třech	tři	k4xCgInPc6
kilometrech	kilometr	k1gInPc6
(	(	kIx(
<g/>
do	do	k7c2
20	#num#	k4
km	km	kA
jízdy	jízda	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
pěti	pět	k4xCc6
kilometrech	kilometr	k1gInPc6
(	(	kIx(
<g/>
nad	nad	k7c7
20	#num#	k4
km	km	kA
jízdy	jízda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Nastupovalo	nastupovat	k5eAaImAgNnS
se	s	k7c7
předními	přední	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
<g/>
,	,	kIx,
cestující	cestující	k1gMnPc1
platili	platit	k5eAaImAgMnP
řidiči	řidič	k1gMnPc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
jim	on	k3xPp3gMnPc3
vydal	vydat	k5eAaPmAgMnS
jízdenku	jízdenka	k1gFnSc4
<g/>
,	,	kIx,
hotově	hotově	k6eAd1
nebo	nebo	k8xC
čipovou	čipový	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
(	(	kIx(
<g/>
sleva	sleva	k1gFnSc1
oproti	oproti	k7c3
placení	placení	k1gNnSc3
hotovostí	hotovost	k1gFnPc2
o	o	k7c4
2	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
existovalo	existovat	k5eAaImAgNnS
poloviční	poloviční	k2eAgNnSc1d1
jízdné	jízdné	k1gNnSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
žákovské	žákovský	k2eAgNnSc4d1
jízdné	jízdné	k1gNnSc4
a	a	k8xC
jízdné	jízdné	k1gNnSc4
pro	pro	k7c4
osoby	osoba	k1gFnPc4
s	s	k7c7
průkazem	průkaz	k1gInSc7
ZTP	ZTP	kA
nebo	nebo	k8xC
ZTP	ZTP	kA
<g/>
/	/	kIx~
<g/>
P.	P.	kA
</s>
<s>
Po	po	k7c6
zaintegrování	zaintegrování	k1gNnSc6
městské	městský	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
do	do	k7c2
IDS	IDS	kA
JMK	JMK	kA
byl	být	k5eAaImAgInS
převzat	převzat	k2eAgInSc1d1
tarif	tarif	k1gInSc1
integrovaného	integrovaný	k2eAgInSc2d1
dopravního	dopravní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jízdné	jízdné	k1gNnSc1
se	se	k3xPyFc4
zjednodušilo	zjednodušit	k5eAaPmAgNnS
<g/>
,	,	kIx,
sleva	sleva	k1gFnSc1
pro	pro	k7c4
čipové	čipový	k2eAgFnPc4d1
karty	karta	k1gFnPc4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byly	být	k5eAaImAgInP
do	do	k7c2
autobusů	autobus	k1gInPc2
nainstalovány	nainstalován	k2eAgInPc4d1
označovače	označovač	k1gInPc4
pro	pro	k7c4
jízdenky	jízdenka	k1gFnPc4
zakoupené	zakoupený	k2eAgFnPc4d1
v	v	k7c6
předprodeji	předprodej	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
stála	stát	k5eAaImAgFnS
základní	základní	k2eAgFnSc1d1
nepřestupní	přestupní	k2eNgFnSc1d1
úseková	úsekový	k2eAgFnSc1d1
jízdenka	jízdenka	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
740	#num#	k4
(	(	kIx(
<g/>
město	město	k1gNnSc1
Vyškov	Vyškov	k1gInSc1
<g/>
)	)	kIx)
10	#num#	k4
Kč	Kč	kA
<g/>
,	,	kIx,
přestupní	přestupní	k2eAgFnSc1d1
na	na	k7c4
45	#num#	k4
minut	minuta	k1gFnPc2
platila	platit	k5eAaImAgFnS
ve	v	k7c6
dvou	dva	k4xCgFnPc6
zónách	zóna	k1gFnPc6
a	a	k8xC
stála	stát	k5eAaImAgFnS
20	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s>
Vozový	vozový	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
Vozový	vozový	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
společnost	společnost	k1gFnSc1
VYDOS	VYDOS	kA
BUS	bus	k1gInSc1
používá	používat	k5eAaImIp3nS
na	na	k7c6
linkách	linka	k1gFnPc6
MHD	MHD	kA
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nP
především	především	k9
linkové	linkový	k2eAgInPc1d1
autobusy	autobus	k1gInPc1
Karosa	karosa	k1gFnSc1
C	C	kA
954	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
vozy	vůz	k1gInPc1
Karosa	karosa	k1gFnSc1
B	B	kA
952	#num#	k4
(	(	kIx(
<g/>
městské	městský	k2eAgNnSc1d1
provedení	provedení	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Irisbus	Irisbus	k1gInSc4
Crossway	Crosswaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jízdní	jízdní	k2eAgInSc1d1
řád	řád	k1gInSc1
ČSAD	ČSAD	kA
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
ČSAD	ČSAD	kA
Brno	Brno	k1gNnSc1
↑	↑	k?
DUŠEK	Dušek	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
159	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
263	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WOHLGEMUTHOVÁ	WOHLGEMUTHOVÁ	kA
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nové	nový	k2eAgFnSc6d1
městské	městský	k2eAgFnSc6d1
lince	linka	k1gFnSc6
mohou	moct	k5eAaImIp3nP
cestovat	cestovat	k5eAaImF
i	i	k9
tělesně	tělesně	k6eAd1
postižení	postižený	k2eAgMnPc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2004-05-06	2004-05-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Výpis	výpis	k1gInSc1
z	z	k7c2
Obchodního	obchodní	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jízdní	jízdní	k2eAgInSc4d1
řád	řád	k1gInSc4
IDS	IDS	kA
JMK	JMK	kA
<g/>
,	,	kIx,
celosíťový	celosíťový	k2eAgMnSc1d1
2004	#num#	k4
<g/>
↑	↑	k?
Jízdní	jízdní	k2eAgInSc4d1
řád	řád	k1gInSc4
IDS	IDS	kA
JMK	JMK	kA
<g/>
,	,	kIx,
celosíťový	celosíťový	k2eAgMnSc1d1
2006	#num#	k4
<g/>
↑	↑	k?
Jízdní	jízdní	k2eAgInSc4d1
řád	řád	k1gInSc4
IDS	IDS	kA
JMK	JMK	kA
<g/>
,	,	kIx,
celosíťový	celosíťový	k2eAgMnSc1d1
2007	#num#	k4
<g/>
↑	↑	k?
Jízdní	jízdní	k2eAgInSc4d1
řád	řád	k1gInSc4
IDS	IDS	kA
JMK	JMK	kA
<g/>
,	,	kIx,
celosíťový	celosíťový	k2eAgMnSc1d1
2008	#num#	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.idsjmk.cz	www.idsjmk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ceník	ceník	k1gInSc1
jízdného	jízdné	k1gNnSc2
pro	pro	k7c4
vnitrostátní	vnitrostátní	k2eAgFnSc4d1
pravidelnou	pravidelný	k2eAgFnSc4d1
autobusovou	autobusový	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
dopravce	dopravce	k1gMnSc1
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
autobusů	autobus	k1gInPc2
VYDOS	VYDOS	kA
BUS	bus	k1gInSc1
(	(	kIx(
<g/>
nejen	nejen	k6eAd1
vyškovská	vyškovský	k2eAgFnSc1d1
MHD	MHD	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Metro	metro	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
♦	♦	k?
Plánované	plánovaný	k2eAgNnSc1d1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
Regionální	regionální	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Praha	Praha	k1gFnSc1
•	•	k?
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
♦	♦	k?
Zrušené	zrušený	k2eAgNnSc4d1
<g/>
:	:	kIx,
Ostravsko	Ostravsko	k1gNnSc4
Tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Brno	Brno	k1gNnSc4
•	•	k?
Liberec	Liberec	k1gInSc1
•	•	k?
Most	most	k1gInSc1
a	a	k8xC
Litvínov	Litvínov	k1gInSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Ostrava	Ostrava	k1gFnSc1
•	•	k?
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Praha	Praha	k1gFnSc1
Zrušené	zrušený	k2eAgFnPc1d1
<g/>
:	:	kIx,
Bohumín	Bohumín	k1gInSc1
•	•	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Český	český	k2eAgInSc1d1
Těšín	Těšín	k1gInSc1
•	•	k?
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
•	•	k?
Jihlava	Jihlava	k1gFnSc1
•	•	k?
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
•	•	k?
Opava	Opava	k1gFnSc1
•	•	k?
Teplice	teplice	k1gFnSc1
•	•	k?
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
♦	♦	k?
Nerealizované	realizovaný	k2eNgInPc1d1
<g/>
:	:	kIx,
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Trolejbusová	trolejbusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Brno	Brno	k1gNnSc1
•	•	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
•	•	k?
Chomutov	Chomutov	k1gInSc1
a	a	k8xC
Jirkov	Jirkov	k1gInSc1
•	•	k?
Jihlava	Jihlava	k1gFnSc1
•	•	k?
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
•	•	k?
Opava	Opava	k1gFnSc1
•	•	k?
Ostrava	Ostrava	k1gFnSc1
•	•	k?
Pardubice	Pardubice	k1gInPc1
•	•	k?
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Praha	Praha	k1gFnSc1
•	•	k?
Teplice	teplice	k1gFnSc1
•	•	k?
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Zlín	Zlín	k1gInSc1
a	a	k8xC
Otrokovice	Otrokovice	k1gFnPc1
Zrušené	zrušený	k2eAgFnPc1d1
<g/>
:	:	kIx,
České	český	k2eAgFnPc1d1
Velenice	Velenice	k1gFnPc1
•	•	k?
Děčín	Děčín	k1gInSc1
•	•	k?
Most	most	k1gInSc1
a	a	k8xC
Litvínov	Litvínov	k1gInSc1
•	•	k?
zkušební	zkušební	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Ostrov	ostrov	k1gInSc1
–	–	k?
Jáchymov	Jáchymov	k1gInSc1
♦	♦	k?
Nerealizované	realizovaný	k2eNgInPc1d1
<g/>
:	:	kIx,
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
•	•	k?
Kladno	Kladno	k1gNnSc4
•	•	k?
Třebíč	Třebíč	k1gFnSc1
Lanové	lanový	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Petřín	Petřín	k1gInSc1
•	•	k?
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
Imperial	Imperial	k1gInSc1
•	•	k?
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Větruše	Větruše	k1gFnSc2
♦	♦	k?
Zrušené	zrušený	k2eAgNnSc1d1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Letná	Letná	k1gFnSc1
•	•	k?
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
Slovenská	slovenský	k2eAgFnSc1d1
–	–	k?
Imperial	Imperial	k1gInSc1
♦	♦	k?
Souhrnně	souhrnně	k6eAd1
<g/>
:	:	kIx,
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
♦	♦	k?
Plánované	plánovaný	k2eAgInPc1d1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc4
<g/>
,	,	kIx,
Pisárky	Pisárka	k1gFnPc4
–	–	k?
Bohunice	Bohunice	k1gFnPc4
Autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
•	•	k?
Benešov	Benešov	k1gInSc1
•	•	k?
Beroun	Beroun	k1gInSc1
a	a	k8xC
Králův	Králův	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
•	•	k?
Čáslav	Čáslav	k1gFnSc1
•	•	k?
Kladno	Kladno	k1gNnSc4
•	•	k?
Kolín	Kolín	k1gInSc1
•	•	k?
Kralupy	Kralupy	k1gInPc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
•	•	k?
Mělník	Mělník	k1gInSc1
•	•	k?
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
Mníšek	Mníšek	k1gInSc1
pod	pod	k7c4
Brdy	Brdy	k1gInPc4
•	•	k?
Nymburk	Nymburk	k1gInSc1
•	•	k?
Příbram	Příbram	k1gFnSc1
•	•	k?
Říčany	Říčany	k1gInPc1
•	•	k?
Slaný	Slaný	k1gInSc1
•	•	k?
Vlašim	Vlašim	k1gFnSc4
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
•	•	k?
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
•	•	k?
Písek	Písek	k1gInSc1
•	•	k?
Strakonice	Strakonice	k1gFnPc1
•	•	k?
Tábor	Tábor	k1gInSc1
•	•	k?
Vimperk	Vimperk	k1gInSc1
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Domažlice	Domažlice	k1gFnPc4
•	•	k?
Klatovy	Klatovy	k1gInPc1
•	•	k?
Rokycany	Rokycany	k1gInPc1
•	•	k?
Stříbro	stříbro	k1gNnSc4
•	•	k?
Tachov	Tachov	k1gInSc1
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
•	•	k?
Aš	Aš	k1gInSc1
•	•	k?
Cheb	Cheb	k1gInSc1
•	•	k?
Jáchymov	Jáchymov	k1gInSc1
•	•	k?
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
•	•	k?
Ostrov	ostrov	k1gInSc1
•	•	k?
Sokolov	Sokolov	k1gInSc1
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Teplice	Teplice	k1gFnPc4
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Liberec	Liberec	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
•	•	k?
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Pardubice	Pardubice	k1gInPc1
•	•	k?
Chrudim	Chrudim	k1gFnSc1
•	•	k?
Litomyšl	Litomyšl	k1gFnSc1
•	•	k?
Moravská	moravský	k2eAgFnSc1d1
Třebová	Třebová	k1gFnSc1
•	•	k?
Přelouč	Přelouč	k1gFnSc1
•	•	k?
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
•	•	k?
Žamberk	Žamberk	k1gInSc4
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
</s>
<s>
Jihlava	Jihlava	k1gFnSc1
•	•	k?
Bystřice	Bystřice	k1gFnSc1
nad	nad	k7c7
Pernštejnem	Pernštejn	k1gInSc7
•	•	k?
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
Pelhřimov	Pelhřimov	k1gInSc1
•	•	k?
Třebíč	Třebíč	k1gFnSc1
•	•	k?
Velké	velký	k2eAgNnSc4d1
Meziříčí	Meziříčí	k1gNnSc4
•	•	k?
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
•	•	k?
Bruntál	Bruntál	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
Těšín	Těšín	k1gInSc1
•	•	k?
Frýdek-Místek	Frýdek-Místek	k1gInSc1
•	•	k?
Havířov	Havířov	k1gInSc1
•	•	k?
Karviná	Karviná	k1gFnSc1
•	•	k?
Krnov	Krnov	k1gInSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
•	•	k?
Opava	Opava	k1gFnSc1
•	•	k?
Orlová	orlový	k2eAgFnSc1d1
•	•	k?
Studénka	studénka	k1gFnSc1
•	•	k?
Třinec	Třinec	k1gInSc4
Olomoucký	olomoucký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Hranice	hranice	k1gFnSc1
•	•	k?
Prostějov	Prostějov	k1gInSc1
•	•	k?
Přerov	Přerov	k1gInSc1
•	•	k?
Šumperk	Šumperk	k1gInSc1
•	•	k?
Zábřeh	Zábřeh	k1gInSc1
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Brno	Brno	k1gNnSc4
•	•	k?
Adamov	Adamov	k1gInSc1
•	•	k?
Blansko	Blansko	k1gNnSc1
•	•	k?
Břeclav	Břeclav	k1gFnSc1
•	•	k?
Hodonín	Hodonín	k1gInSc1
•	•	k?
Kyjov	Kyjov	k1gInSc1
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Znojmo	Znojmo	k1gNnSc1
Zlínský	zlínský	k2eAgMnSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Zlín	Zlín	k1gInSc1
a	a	k8xC
Otrokovice	Otrokovice	k1gFnPc1
•	•	k?
Kroměříž	Kroměříž	k1gFnSc1
•	•	k?
Valašské	valašský	k2eAgFnPc4d1
Meziřící	Meziřící	k2eAgFnPc4d1
•	•	k?
Vsetín	Vsetín	k1gInSc1
•	•	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
přívozy	přívoz	k1gInPc1
PID	PID	kA
•	•	k?
Brněnská	brněnský	k2eAgFnSc1d1
přehrada	přehrada	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
|	|	kIx~
Vyškov	Vyškov	k1gInSc1
</s>
