<s>
Navzdory	navzdory	k7c3	navzdory
bezpečnostní	bezpečnostní	k2eAgFnSc3d1	bezpečnostní
krizi	krize	k1gFnSc3	krize
nedovolil	dovolit	k5eNaPmAgInS	dovolit
irácký	irácký	k2eAgInSc1d1	irácký
parlament	parlament	k1gInSc1	parlament
premiéru	premiér	k1gMnSc3	premiér
Malikímu	Malikí	k1gMnSc3	Malikí
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
stav	stav	k1gInSc4	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
sunnitských	sunnitský	k2eAgMnPc2d1	sunnitský
a	a	k8xC	a
kurdských	kurdský	k2eAgMnPc2d1	kurdský
poslanců	poslanec	k1gMnPc2	poslanec
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
nezúčastnilo	zúčastnit	k5eNaPmAgNnS	zúčastnit
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
schůze	schůze	k1gFnPc4	schůze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odmítali	odmítat	k5eAaImAgMnP	odmítat
rozšíření	rozšíření	k1gNnSc4	rozšíření
premiérových	premiérový	k2eAgFnPc2d1	premiérová
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
