<p>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
ukončený	ukončený	k2eAgInSc4d1	ukončený
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
také	také	k6eAd1	také
nazýván	nazývat	k5eAaImNgInS	nazývat
Iráckou	irácký	k2eAgFnSc7d1	irácká
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stažení	stažení	k1gNnSc6	stažení
amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
konflikt	konflikt	k1gInSc1	konflikt
různých	různý	k2eAgFnPc2d1	různá
náboženských	náboženský	k2eAgFnPc2d1	náboženská
a	a	k8xC	a
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
s	s	k7c7	s
centrální	centrální	k2eAgFnSc7d1	centrální
vládou	vláda	k1gFnSc7	vláda
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
sektářské	sektářský	k2eAgNnSc4d1	sektářské
násilí	násilí	k1gNnSc4	násilí
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
jevem	jev	k1gInSc7	jev
byl	být	k5eAaImAgMnS	být
vznik	vznik	k1gInSc4	vznik
tzv.	tzv.	kA	tzv.
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
IS	IS	kA	IS
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
krutovlády	krutovláda	k1gFnPc1	krutovláda
na	na	k7c6	na
částech	část	k1gFnPc6	část
území	území	k1gNnSc2	území
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
počátečních	počáteční	k2eAgInPc6d1	počáteční
úspěších	úspěch	k1gInPc6	úspěch
vojensky	vojensky	k6eAd1	vojensky
přemožen	přemožen	k2eAgMnSc1d1	přemožen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
sunnitské	sunnitský	k2eAgFnPc1d1	sunnitská
menšiny	menšina	k1gFnPc1	menšina
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itské	itský	k2eAgFnSc2d1	itská
většiny	většina	k1gFnSc2	většina
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přímým	přímý	k2eAgNnSc7d1	přímé
pokračováním	pokračování	k1gNnSc7	pokračování
odboje	odboj	k1gInSc2	odboj
proti	proti	k7c3	proti
americké	americký	k2eAgFnSc3d1	americká
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stažení	stažení	k1gNnSc6	stažení
amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
se	se	k3xPyFc4	se
úroveň	úroveň	k1gFnSc1	úroveň
násilností	násilnost	k1gFnPc2	násilnost
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
,	,	kIx,	,
když	když	k8xS	když
sunnitské	sunnitský	k2eAgFnPc1d1	sunnitská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
skupiny	skupina	k1gFnPc1	skupina
přistoupily	přistoupit	k5eAaPmAgFnP	přistoupit
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
proti	proti	k7c3	proti
většinové	většinový	k2eAgFnSc3d1	většinová
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itské	itský	k2eAgFnSc6d1	itská
populaci	populace	k1gFnSc6	populace
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
podlomit	podlomit	k5eAaPmF	podlomit
důvěru	důvěra	k1gFnSc4	důvěra
ve	v	k7c4	v
schopnost	schopnost	k1gFnSc4	schopnost
Ší	Ší	k1gFnSc2	Ší
<g/>
'	'	kIx"	'
<g/>
ity	ity	k?	ity
dominované	dominovaný	k2eAgFnSc2d1	dominovaná
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
ochránit	ochránit	k5eAaPmF	ochránit
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
bez	bez	k7c2	bez
přímé	přímý	k2eAgFnSc2d1	přímá
americké	americký	k2eAgFnSc2d1	americká
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Irácké	irácký	k2eAgFnPc1d1	irácká
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
skupiny	skupina	k1gFnPc1	skupina
byly	být	k5eAaImAgFnP	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přitahovány	přitahován	k2eAgInPc1d1	přitahován
probíhající	probíhající	k2eAgInPc1d1	probíhající
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
názorově	názorově	k6eAd1	názorově
velmi	velmi	k6eAd1	velmi
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
sunnitské	sunnitský	k2eAgFnPc1d1	sunnitská
skupiny	skupina	k1gFnPc1	skupina
bojovaly	bojovat	k5eAaImAgFnP	bojovat
proti	proti	k7c3	proti
syrské	syrský	k2eAgFnSc3d1	Syrská
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itské	itský	k2eAgFnSc2d1	itská
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
ke	k	k7c3	k
snaze	snaha	k1gFnSc3	snaha
syrskou	syrský	k2eAgFnSc4d1	Syrská
vládu	vláda	k1gFnSc4	vláda
podpořit	podpořit	k5eAaPmF	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
povstání	povstání	k1gNnSc2	povstání
dramaticky	dramaticky	k6eAd1	dramaticky
eskalovalo	eskalovat	k5eAaImAgNnS	eskalovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
skupina	skupina	k1gFnSc1	skupina
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Levantě	Levanta	k1gFnSc3	Levanta
dobyla	dobýt	k5eAaPmAgFnS	dobýt
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
irácké	irácký	k2eAgNnSc4d1	irácké
město	město	k1gNnSc4	město
Mosul	Mosul	k1gInSc1	Mosul
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
území	území	k1gNnPc4	území
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
propojen	propojit	k5eAaPmNgMnS	propojit
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
aspekt	aspekt	k1gInSc4	aspekt
a	a	k8xC	a
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
genocidu	genocida	k1gFnSc4	genocida
iráckých	irácký	k2eAgMnPc2d1	irácký
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
Jezídů	Jezíd	k1gMnPc2	Jezíd
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
omezenému	omezený	k2eAgNnSc3d1	omezené
zapojení	zapojení	k1gNnSc3	zapojení
amerických	americký	k2eAgFnPc2d1	americká
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
iráckého	irácký	k2eAgInSc2d1	irácký
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Irácká	irácký	k2eAgFnSc1d1	irácká
krize	krize	k1gFnSc1	krize
2014	[number]	k4	2014
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Iráku	Irák	k1gInSc6	Irák
střety	střet	k1gInPc4	střet
mezi	mezi	k7c7	mezi
klanovými	klanový	k2eAgFnPc7d1	klanová
milicemi	milice	k1gFnPc7	milice
<g/>
,	,	kIx,	,
iráckými	irácký	k2eAgFnPc7d1	irácká
bezpečnostními	bezpečnostní	k2eAgFnPc7d1	bezpečnostní
jednotkami	jednotka	k1gFnPc7	jednotka
a	a	k8xC	a
Islámským	islámský	k2eAgInSc7d1	islámský
státem	stát	k1gInSc7	stát
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Levantě	Levanta	k1gFnSc6	Levanta
(	(	kIx(	(
<g/>
ISIL	ISIL	kA	ISIL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
obsadily	obsadit	k5eAaPmAgFnP	obsadit
jednotky	jednotka	k1gFnPc1	jednotka
ISIL	ISIL	kA	ISIL
města	město	k1gNnPc1	město
Fallúdžu	Fallúdžu	k1gMnPc1	Fallúdžu
a	a	k8xC	a
Ramádí	Ramádí	k1gNnSc1	Ramádí
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
většinu	většina	k1gFnSc4	většina
governorátu	governorát	k1gInSc2	governorát
Anbár	Anbár	k1gInSc1	Anbár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
zahájila	zahájit	k5eAaPmAgFnS	zahájit
irácká	irácký	k2eAgFnSc1d1	irácká
armáda	armáda	k1gFnSc1	armáda
ofenzivu	ofenziva	k1gFnSc4	ofenziva
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
obnovit	obnovit	k5eAaPmF	obnovit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
ztraceným	ztracený	k2eAgNnSc7d1	ztracené
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
ofenzivy	ofenziva	k1gFnSc2	ofenziva
přednesl	přednést	k5eAaPmAgMnS	přednést
irácký	irácký	k2eAgMnSc1d1	irácký
premiér	premiér	k1gMnSc1	premiér
kontroverzní	kontroverzní	k2eAgInSc4d1	kontroverzní
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
nadcházející	nadcházející	k2eAgFnSc4d1	nadcházející
ofenzivu	ofenziva	k1gFnSc4	ofenziva
jako	jako	k8xC	jako
pokračování	pokračování	k1gNnSc4	pokračování
starobylé	starobylý	k2eAgFnSc2d1	starobylá
sektářské	sektářský	k2eAgFnSc2d1	sektářská
války	válka	k1gFnSc2	válka
mezi	mezi	k7c4	mezi
"	"	kIx"	"
<g/>
následovníky	následovník	k1gMnPc4	následovník
Husajna	Husajn	k1gMnSc4	Husajn
a	a	k8xC	a
následovníky	následovník	k1gMnPc4	následovník
Jazída	Jazíd	k1gMnSc4	Jazíd
I.	I.	kA	I.
<g/>
"	"	kIx"	"
Odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Karbaly	Karbal	k1gMnPc4	Karbal
z	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
anbárské	anbárský	k2eAgMnPc4d1	anbárský
sunnity	sunnita	k1gMnPc4	sunnita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
před	před	k7c7	před
tím	ten	k3xDgMnSc7	ten
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
iráckou	irácký	k2eAgFnSc7d1	irácká
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
ISIS	Isis	k1gFnSc1	Isis
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
Sýrii	Sýrie	k1gFnSc6	Sýrie
upevnila	upevnit	k5eAaPmAgFnS	upevnit
její	její	k3xOp3gFnSc1	její
pozice	pozice	k1gFnSc1	pozice
v	v	k7c6	v
Anbáru	Anbár	k1gInSc6	Anbár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2014	[number]	k4	2014
zahájil	zahájit	k5eAaPmAgInS	zahájit
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Levantě	Levanta	k1gFnSc6	Levanta
ofenzivu	ofenziva	k1gFnSc4	ofenziva
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
obsadil	obsadit	k5eAaPmAgInS	obsadit
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Samarru	Samarr	k1gInSc2	Samarr
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
irácké	irácký	k2eAgNnSc4d1	irácké
město	město	k1gNnSc4	město
Mosul	Mosul	k1gInSc4	Mosul
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gNnSc4	on
následoval	následovat	k5eAaImAgInS	následovat
Tikrít	Tikrít	k1gFnSc4	Tikrít
<g/>
,	,	kIx,	,
rodiště	rodiště	k1gNnSc4	rodiště
Saddáma	Saddám	k1gMnSc2	Saddám
Husajna	Husajn	k1gMnSc2	Husajn
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
irácká	irácký	k2eAgFnSc1d1	irácká
armáda	armáda	k1gFnSc1	armáda
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
obsadily	obsadit	k5eAaPmAgFnP	obsadit
kurdské	kurdský	k2eAgFnPc1d1	kurdská
jednotky	jednotka	k1gFnPc1	jednotka
Kirkúk	Kirkúka	k1gFnPc2	Kirkúka
s	s	k7c7	s
přilehlými	přilehlý	k2eAgFnPc7d1	přilehlá
rafineriemi	rafinerie	k1gFnPc7	rafinerie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
sporného	sporný	k2eAgNnSc2d1	sporné
území	území	k1gNnSc2	území
narovnaného	narovnaný	k2eAgInSc2d1	narovnaný
iráckou	irácký	k2eAgFnSc4d1	irácká
vládou	vláda	k1gFnSc7	vláda
i	i	k8xC	i
kurdskou	kurdský	k2eAgFnSc7d1	kurdská
administrativou	administrativa	k1gFnSc7	administrativa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
ztratil	ztratit	k5eAaPmAgInS	ztratit
Irák	Irák	k1gInSc1	Irák
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
hranicemi	hranice	k1gFnPc7	hranice
s	s	k7c7	s
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
a	a	k8xC	a
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Irácký	irácký	k2eAgMnSc1d1	irácký
premiér	premiér	k1gMnSc1	premiér
Núrí	Núrí	k1gMnSc1	Núrí
Málikí	Málikí	k1gMnSc1	Málikí
naléhal	naléhat	k5eAaImAgMnS	naléhat
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Mosulu	Mosul	k1gInSc2	Mosul
na	na	k7c4	na
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
stavu	stav	k1gInSc2	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
bezpečnostní	bezpečnostní	k2eAgFnSc3d1	bezpečnostní
krizi	krize	k1gFnSc3	krize
nedovolil	dovolit	k5eNaPmAgInS	dovolit
irácký	irácký	k2eAgInSc1d1	irácký
parlament	parlament	k1gInSc1	parlament
premiéru	premiér	k1gMnSc3	premiér
Malikímu	Malikí	k1gMnSc3	Malikí
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
stav	stav	k1gInSc4	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
sunnitských	sunnitský	k2eAgMnPc2d1	sunnitský
a	a	k8xC	a
kurdských	kurdský	k2eAgMnPc2d1	kurdský
poslanců	poslanec	k1gMnPc2	poslanec
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
nezúčastnilo	zúčastnit	k5eNaPmAgNnS	zúčastnit
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
schůze	schůze	k1gFnPc4	schůze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odmítali	odmítat	k5eAaImAgMnP	odmítat
rozšíření	rozšíření	k1gNnSc4	rozšíření
premiérových	premiérový	k2eAgFnPc2d1	premiérová
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
změnil	změnit	k5eAaPmAgInS	změnit
ISIL	ISIL	kA	ISIL
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Islámský	islámský	k2eAgInSc4d1	islámský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
"	"	kIx"	"
<g/>
chalífát	chalífát	k1gInSc4	chalífát
<g/>
"	"	kIx"	"
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
jak	jak	k8xS	jak
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
Irák	Irák	k1gInSc1	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
velitel	velitel	k1gMnSc1	velitel
Abú	abú	k1gMnSc1	abú
Bakr	Bakr	k1gMnSc1	Bakr
al-Bagdádí	al-Bagdádit	k5eAaImIp3nS	al-Bagdádit
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
"	"	kIx"	"
<g/>
chalífou	chalífa	k1gMnSc7	chalífa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vůdcem	vůdce	k1gMnSc7	vůdce
všech	všecek	k3xTgMnPc2	všecek
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ofenziva	ofenziva	k1gFnSc1	ofenziva
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
proti	proti	k7c3	proti
kurdským	kurdský	k2eAgNnPc3d1	kurdské
územím	území	k1gNnPc3	území
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
chalífát	chalífát	k1gInSc4	chalífát
–	–	k?	–
systém	systém	k1gInSc1	systém
vlády	vláda	k1gFnSc2	vláda
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
fundamentalistickém	fundamentalistický	k2eAgInSc6d1	fundamentalistický
výkladu	výklad	k1gInSc6	výklad
islámského	islámský	k2eAgNnSc2d1	islámské
náboženského	náboženský	k2eAgNnSc2d1	náboženské
práva	právo	k1gNnSc2	právo
–	–	k?	–
dopouštěl	dopouštět	k5eAaImAgInS	dopouštět
se	se	k3xPyFc4	se
únosů	únos	k1gInPc2	únos
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
masových	masový	k2eAgFnPc2d1	masová
poprav	poprava	k1gFnPc2	poprava
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itů	itů	k?	itů
a	a	k8xC	a
nemuslimů	nemuslim	k1gMnPc2	nemuslim
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
akce	akce	k1gFnPc1	akce
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
masovému	masový	k2eAgInSc3d1	masový
exodu	exodus	k1gInSc3	exodus
místních	místní	k2eAgMnPc2d1	místní
Jezídů	Jezíd	k1gMnPc2	Jezíd
a	a	k8xC	a
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ofenzivy	ofenziva	k1gFnSc2	ofenziva
Islámský	islámský	k2eAgInSc4d1	islámský
stát	stát	k1gInSc4	stát
obsadil	obsadit	k5eAaPmAgMnS	obsadit
jezídské	jezídský	k2eAgNnSc1d1	jezídský
město	město	k1gNnSc1	město
Sindžár	Sindžár	k1gInSc1	Sindžár
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
tisíců	tisíc	k4xCgInPc2	tisíc
Jezídů	Jezíd	k1gMnPc2	Jezíd
hledajících	hledající	k2eAgMnPc2d1	hledající
útočiště	útočiště	k1gNnPc4	útočiště
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Sindžár	Sindžár	k1gInSc4	Sindžár
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
ohrožování	ohrožování	k1gNnSc2	ohrožování
nedostatkem	nedostatek	k1gInSc7	nedostatek
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
nezbytností	nezbytnost	k1gFnPc2	nezbytnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
popraveno	popraven	k2eAgNnSc4d1	popraveno
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
Jezídů	Jezíd	k1gInPc2	Jezíd
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Sindžár	Sindžár	k1gInSc1	Sindžár
hrozila	hrozit	k5eAaImAgFnS	hrozit
množstvím	množství	k1gNnSc7	množství
úmrtí	úmrť	k1gFnPc2	úmrť
na	na	k7c4	na
dehydrataci	dehydratace	k1gFnSc4	dehydratace
a	a	k8xC	a
hladovění	hladovění	k1gNnSc4	hladovění
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostřelováním	ostřelování	k1gNnSc7	ostřelování
Irbílu	Irbíl	k1gInSc2	Irbíl
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
intervenci	intervence	k1gFnSc3	intervence
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
provedlo	provést	k5eAaPmAgNnS	provést
americké	americký	k2eAgNnSc1d1	americké
letectvo	letectvo	k1gNnSc1	letectvo
první	první	k4xOgInPc1	první
nálety	nálet	k1gInPc1	nálet
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
irácký	irácký	k2eAgMnSc1d1	irácký
prezident	prezident	k1gMnSc1	prezident
Fuád	Fuád	k1gMnSc1	Fuád
Masúm	Masúm	k1gMnSc1	Masúm
nového	nový	k2eAgMnSc2d1	nový
premiéra	premiér	k1gMnSc2	premiér
Hajdara	Hajdar	k1gMnSc2	Hajdar
Abádího	Abádí	k1gMnSc2	Abádí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
většiny	většina	k1gFnSc2	většina
frakcí	frakce	k1gFnPc2	frakce
iráckého	irácký	k2eAgInSc2d1	irácký
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Núrí	Núrí	k2eAgFnSc1d1	Núrí
Málikí	Málikí	k1gFnSc1	Málikí
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInSc4d1	počáteční
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dnes	dnes	k6eAd1	dnes
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
de	de	k?	de
facto	facta	k1gFnSc5	facta
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gMnPc2	jeho
bojovníků	bojovník	k1gMnPc2	bojovník
se	se	k3xPyFc4	se
transformovala	transformovat	k5eAaBmAgFnS	transformovat
do	do	k7c2	do
milice	milice	k1gFnSc2	milice
Riyat	Riyat	k2eAgMnSc1d1	Riyat
al-Bayda	al-Bayda	k1gMnSc1	al-Bayda
(	(	kIx(	(
<g/>
anglcky	anglcky	k6eAd1	anglcky
<g/>
:	:	kIx,	:
White	Whit	k1gInSc5	Whit
Flags	Flagsa	k1gFnPc2	Flagsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
kurdští	kurdský	k2eAgMnPc1d1	kurdský
mafiáni	mafián	k1gMnPc1	mafián
<g/>
.	.	kIx.	.
</s>
<s>
Velí	velet	k5eAaImIp3nS	velet
jim	on	k3xPp3gMnPc3	on
muž	muž	k1gMnSc1	muž
jménem	jméno	k1gNnSc7	jméno
Al	ala	k1gFnPc2	ala
Mansouri	Mansouri	k1gNnSc2	Mansouri
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
od	od	k7c2	od
1300	[number]	k4	1300
do	do	k7c2	do
2000	[number]	k4	2000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Disponují	disponovat	k5eAaBmIp3nP	disponovat
obrněnou	obrněný	k2eAgFnSc7d1	obrněná
technikou	technika	k1gFnSc7	technika
ale	ale	k8xC	ale
i	i	k9	i
dělostřelectvem	dělostřelectvo	k1gNnSc7	dělostřelectvo
<g/>
.	.	kIx.	.
</s>
<s>
Pohybovat	pohybovat	k5eAaImF	pohybovat
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
ovládnout	ovládnout	k5eAaPmF	ovládnout
ropná	ropný	k2eAgNnPc4d1	ropné
pole	pole	k1gNnPc4	pole
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
osvobození	osvobození	k1gNnSc6	osvobození
zbytku	zbytek	k1gInSc2	zbytek
IS	IS	kA	IS
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
Hamrin	Hamrin	k1gInSc1	Hamrin
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Hawija	Hawij	k1gInSc2	Hawij
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
mít	mít	k5eAaImF	mít
dokonce	dokonce	k9	dokonce
výcvikový	výcvikový	k2eAgInSc4d1	výcvikový
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Iraqi	Iraq	k1gFnSc2	Iraq
insurgency	insurgenca	k1gFnSc2	insurgenca
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
present	present	k1gMnSc1	present
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
Northern	Northern	k1gMnSc1	Northern
Iraq	Iraq	k1gFnSc2	Iraq
offensive	offensiev	k1gFnSc2	offensiev
(	(	kIx(	(
<g/>
June	jun	k1gMnSc5	jun
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Northern	Northern	k1gNnSc4	Northern
Iraq	Iraq	k1gFnSc2	Iraq
offensive	offensiev	k1gFnSc2	offensiev
(	(	kIx(	(
<g/>
August	August	k1gMnSc1	August
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
</s>
</p>
<p>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
</s>
</p>
<p>
<s>
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
</s>
</p>
<p>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
</s>
</p>
