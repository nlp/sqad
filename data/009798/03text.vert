<p>
<s>
Únorová	únorový	k2eAgFnSc1d1	únorová
revoluce	revoluce	k1gFnSc1	revoluce
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
revolucí	revoluce	k1gFnPc2	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
v	v	k7c6	v
Ruském	ruský	k2eAgNnSc6d1	ruské
impériu	impérium	k1gNnSc6	impérium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
fázi	fáze	k1gFnSc6	fáze
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
předcházela	předcházet	k5eAaImAgFnS	předcházet
vypuknutí	vypuknutí	k1gNnSc4	vypuknutí
Říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
válečnými	válečný	k2eAgInPc7d1	válečný
poměry	poměr	k1gInPc7	poměr
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
generální	generální	k2eAgFnSc1d1	generální
stávka	stávka	k1gFnSc1	stávka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
Únorové	únorový	k2eAgFnSc2d1	únorová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
důsledku	důsledek	k1gInSc6	důsledek
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
imperátor	imperátor	k1gMnSc1	imperátor
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
moc	moc	k6eAd1	moc
získala	získat	k5eAaPmAgFnS	získat
Prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
revoluční	revoluční	k2eAgInPc4d1	revoluční
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sověty	sovět	k1gInPc1	sovět
(	(	kIx(	(
<g/>
rady	rada	k1gFnPc1	rada
<g/>
)	)	kIx)	)
dělnických	dělnický	k2eAgFnPc2d1	Dělnická
<g/>
,	,	kIx,	,
rolnických	rolnický	k2eAgFnPc2d1	rolnická
a	a	k8xC	a
vojenských	vojenský	k2eAgMnPc2d1	vojenský
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
moc	moc	k1gFnSc4	moc
měl	mít	k5eAaImAgMnS	mít
Petrohradský	petrohradský	k2eAgInSc4d1	petrohradský
sovět	sovět	k1gInSc4	sovět
dělnických	dělnický	k2eAgMnPc2d1	dělnický
a	a	k8xC	a
vojenských	vojenský	k2eAgMnPc2d1	vojenský
delegátů	delegát	k1gMnPc2	delegát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vládě	vláda	k1gFnSc3	vláda
nedůvěřoval	důvěřovat	k5eNaImAgMnS	důvěřovat
a	a	k8xC	a
přisvojoval	přisvojovat	k5eAaImAgMnS	přisvojovat
si	se	k3xPyFc3	se
střídavě	střídavě	k6eAd1	střídavě
moc	moc	k6eAd1	moc
i	i	k9	i
práva	právo	k1gNnSc2	právo
vlády	vláda	k1gFnSc2	vláda
i	i	k8xC	i
dumy	duma	k1gFnSc2	duma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
však	však	k9	však
bolševici	bolševik	k1gMnPc1	bolševik
zahájili	zahájit	k5eAaPmAgMnP	zahájit
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
Říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
říjnová	říjnový	k2eAgFnSc1d1	říjnová
socialistická	socialistický	k2eAgFnSc1d1	socialistická
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
)	)	kIx)	)
a	a	k8xC	a
uzurpovali	uzurpovat	k5eAaBmAgMnP	uzurpovat
si	se	k3xPyFc3	se
absolutní	absolutní	k2eAgFnSc4d1	absolutní
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
revoluce	revoluce	k1gFnSc2	revoluce
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
Rusko	Rusko	k1gNnSc4	Rusko
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
i	i	k9	i
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pohromu	pohroma	k1gFnSc4	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
Zaostalý	zaostalý	k2eAgInSc1d1	zaostalý
carský	carský	k2eAgInSc1d1	carský
režim	režim	k1gInSc1	režim
nedokázal	dokázat	k5eNaPmAgInS	dokázat
účinně	účinně	k6eAd1	účinně
mobilizovat	mobilizovat	k5eAaBmF	mobilizovat
všechny	všechen	k3xTgFnPc4	všechen
síly	síla	k1gFnPc4	síla
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
před	před	k7c7	před
vnějším	vnější	k2eAgMnSc7d1	vnější
nepřítelem	nepřítel	k1gMnSc7	nepřítel
a	a	k8xC	a
po	po	k7c6	po
vzedmutí	vzedmutí	k1gNnSc6	vzedmutí
lidových	lidový	k2eAgFnPc2d1	lidová
bouří	bouř	k1gFnPc2	bouř
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
oponenti	oponent	k1gMnPc1	oponent
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
dostávali	dostávat	k5eAaImAgMnP	dostávat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
od	od	k7c2	od
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
totiž	totiž	k9	totiž
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
padne	padnout	k5eAaImIp3nS	padnout
carský	carský	k2eAgInSc4d1	carský
režim	režim	k1gInSc4	režim
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
nebudou	být	k5eNaImBp3nP	být
muset	muset	k5eAaImF	muset
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
nadále	nadále	k6eAd1	nadále
válčit	válčit	k5eAaImF	válčit
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvolněné	uvolněný	k2eAgMnPc4d1	uvolněný
vojáky	voják	k1gMnPc4	voják
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
přesunout	přesunout	k5eAaPmF	přesunout
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
fronty	fronta	k1gFnSc2	fronta
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nedostatku	nedostatek	k1gInSc2	nedostatek
potravin	potravina	k1gFnPc2	potravina
stávkovat	stávkovat	k5eAaImF	stávkovat
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
petrohradských	petrohradský	k2eAgMnPc2d1	petrohradský
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
však	však	k9	však
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
stávka	stávka	k1gFnSc1	stávka
automaticky	automaticky	k6eAd1	automaticky
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
sabotáž	sabotáž	k1gFnSc4	sabotáž
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
vyřešit	vyřešit	k5eAaPmF	vyřešit
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
jednáním	jednání	k1gNnSc7	jednání
vyvolali	vyvolat	k5eAaPmAgMnP	vyvolat
naopak	naopak	k6eAd1	naopak
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInPc1d2	veliký
protesty	protest	k1gInPc1	protest
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
nespokojené	spokojený	k2eNgFnSc2d1	nespokojená
petrohradské	petrohradský	k2eAgFnSc2d1	Petrohradská
ženy	žena	k1gFnSc2	žena
stojící	stojící	k2eAgFnSc2d1	stojící
v	v	k7c6	v
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
řadách	řada	k1gFnPc6	řada
na	na	k7c4	na
chléb	chléb	k1gInSc4	chléb
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
protiválečnou	protiválečný	k2eAgFnSc4d1	protiválečná
demonstraci	demonstrace	k1gFnSc4	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
protesty	protest	k1gInPc1	protest
přerostly	přerůst	k5eAaPmAgInP	přerůst
ve	v	k7c4	v
střety	střet	k1gInPc4	střet
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
kozáky	kozák	k1gMnPc7	kozák
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k1gInSc1	večer
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
vydal	vydat	k5eAaPmAgMnS	vydat
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
rozkaz	rozkaz	k1gInSc1	rozkaz
veliteli	velitel	k1gMnPc7	velitel
petrohradského	petrohradský	k2eAgInSc2d1	petrohradský
vojenského	vojenský	k2eAgInSc2d1	vojenský
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
demonstrace	demonstrace	k1gFnSc1	demonstrace
okamžitě	okamžitě	k6eAd1	okamžitě
ukončil	ukončit	k5eAaPmAgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
nasadit	nasadit	k5eAaPmF	nasadit
proti	proti	k7c3	proti
demonstrantům	demonstrant	k1gMnPc3	demonstrant
vojsko	vojsko	k1gNnSc4	vojsko
nebyly	být	k5eNaImAgFnP	být
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vojáci	voják	k1gMnPc1	voják
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
střílet	střílet	k5eAaImF	střílet
do	do	k7c2	do
neozbrojených	ozbrojený	k2eNgMnPc2d1	neozbrojený
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
<g/>
,	,	kIx,	,
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
své	svůj	k3xOyFgMnPc4	svůj
velitele	velitel	k1gMnPc4	velitel
a	a	k8xC	a
přidali	přidat	k5eAaPmAgMnP	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
nespokojencům	nespokojenec	k1gMnPc3	nespokojenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgInPc2d1	další
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
začala	začít	k5eAaPmAgFnS	začít
generální	generální	k2eAgFnSc1d1	generální
stávka	stávka	k1gFnSc1	stávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
někteří	některý	k3yIgMnPc1	některý
důstojníci	důstojník	k1gMnPc1	důstojník
<g/>
,	,	kIx,	,
carovi	car	k1gMnSc3	car
oddané	oddaný	k2eAgFnSc2d1	oddaná
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
policie	policie	k1gFnSc2	policie
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
při	při	k7c6	při
nepokojích	nepokoj	k1gInPc6	nepokoj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
150	[number]	k4	150
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
už	už	k6eAd1	už
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
přerostly	přerůst	k5eAaPmAgInP	přerůst
nepokoje	nepokoj	k1gInPc1	nepokoj
v	v	k7c4	v
otevřený	otevřený	k2eAgInSc4d1	otevřený
boj	boj	k1gInSc4	boj
mezi	mezi	k7c7	mezi
revolučními	revoluční	k2eAgFnPc7d1	revoluční
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
carským	carský	k2eAgNnSc7d1	carské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnPc1d1	revoluční
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
i	i	k9	i
gardový	gardový	k2eAgInSc4d1	gardový
Pavlovský	pavlovský	k2eAgInSc4d1	pavlovský
pluk	pluk	k1gInSc4	pluk
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
obsadily	obsadit	k5eAaPmAgFnP	obsadit
všechny	všechen	k3xTgFnPc1	všechen
důležité	důležitý	k2eAgFnPc1d1	důležitá
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Petropavlovskou	petropavlovský	k2eAgFnSc4d1	Petropavlovská
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
budovy	budova	k1gFnPc4	budova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
dělníci	dělník	k1gMnPc1	dělník
a	a	k8xC	a
vojáci	voják	k1gMnPc1	voják
orgány	orgán	k1gInPc7	orgán
samosprávy	samospráva	k1gFnSc2	samospráva
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
sověty	sovět	k1gInPc1	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
menševik	menševik	k1gMnSc1	menševik
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Čcheidze	Čcheidze	k1gFnSc1	Čcheidze
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
zástupce	zástupce	k1gMnSc1	zástupce
eser	eser	k1gMnSc1	eser
Alexandr	Alexandr	k1gMnSc1	Alexandr
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
<g/>
.	.	kIx.	.
</s>
<s>
Sovět	Sovět	k1gMnSc1	Sovět
přebral	přebrat	k5eAaPmAgMnS	přebrat
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
dodržování	dodržování	k1gNnSc4	dodržování
pořádku	pořádek	k1gInSc2	pořádek
a	a	k8xC	a
zásobování	zásobování	k1gNnSc2	zásobování
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dělníci	dělník	k1gMnPc1	dělník
a	a	k8xC	a
vojáci	voják	k1gMnPc1	voják
pak	pak	k6eAd1	pak
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
městech	město	k1gNnPc6	město
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
tvořili	tvořit	k5eAaImAgMnP	tvořit
další	další	k2eAgInPc4d1	další
sověty	sovět	k1gInPc4	sovět
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
státní	státní	k2eAgFnSc1d1	státní
duma	duma	k1gFnSc1	duma
dočasný	dočasný	k2eAgInSc1d1	dočasný
kabinet	kabinet	k1gInSc1	kabinet
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Michaila	Michail	k1gMnSc2	Michail
Rodzjanka	Rodzjanka	k1gFnSc1	Rodzjanka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
imperátor	imperátor	k1gMnSc1	imperátor
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
opustil	opustit	k5eAaPmAgMnS	opustit
svůj	svůj	k3xOyFgInSc4	svůj
štáb	štáb	k1gInSc4	štáb
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
revolučních	revoluční	k2eAgNnPc2d1	revoluční
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
proto	proto	k8xC	proto
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Pskova	Pskov	k1gInSc2	Pskov
<g/>
,	,	kIx,	,
na	na	k7c4	na
štáb	štáb	k1gInSc4	štáb
severní	severní	k2eAgFnSc2d1	severní
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
s	s	k7c7	s
velitelem	velitel	k1gMnSc7	velitel
fronty	fronta	k1gFnSc2	fronta
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
revoluce	revoluce	k1gFnSc2	revoluce
nemá	mít	k5eNaImIp3nS	mít
dostatek	dostatek	k1gInSc4	dostatek
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
trůnu	trůn	k1gInSc2	trůn
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Michaila	Michail	k1gMnSc2	Michail
Alexandroviče	Alexandrovič	k1gMnSc2	Alexandrovič
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
však	však	k9	však
lid	lid	k1gInSc4	lid
neuspokojila	uspokojit	k5eNaPmAgFnS	uspokojit
a	a	k8xC	a
Michail	Michail	k1gMnSc1	Michail
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
trůnu	trůn	k1gInSc2	trůn
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
budoucnost	budoucnost	k1gFnSc1	budoucnost
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
musí	muset	k5eAaImIp3nS	muset
vyřešit	vyřešit	k5eAaPmF	vyřešit
zákonodárné	zákonodárný	k2eAgNnSc1d1	zákonodárné
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1917	[number]	k4	1917
skončila	skončit	k5eAaPmAgFnS	skončit
stopadesátiletá	stopadesátiletý	k2eAgFnSc1d1	stopadesátiletá
vláda	vláda	k1gFnSc1	vláda
rodu	rod	k1gInSc2	rod
Holstein	Holstein	k2eAgInSc1d1	Holstein
<g/>
-	-	kIx~	-
<g/>
Gottorp	Gottorp	k1gInSc1	Gottorp
<g/>
-	-	kIx~	-
<g/>
Romanov	Romanov	k1gInSc1	Romanov
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc1d1	státní
duma	duma	k1gFnSc1	duma
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Petrohradským	petrohradský	k2eAgInSc7d1	petrohradský
sovětem	sovět	k1gInSc7	sovět
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
byl	být	k5eAaImAgMnS	být
kníže	kníže	k1gMnSc1	kníže
Georgij	Georgij	k1gFnSc2	Georgij
Lvov	Lvov	k1gInSc1	Lvov
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dočasná	dočasný	k2eAgFnSc1d1	dočasná
vláda	vláda	k1gFnSc1	vláda
musela	muset	k5eAaImAgFnS	muset
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
kroky	krok	k1gInPc4	krok
prodiskutovat	prodiskutovat	k5eAaPmF	prodiskutovat
s	s	k7c7	s
Petrohradským	petrohradský	k2eAgInSc7d1	petrohradský
sovětem	sovět	k1gInSc7	sovět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
dvojvládí	dvojvládí	k1gNnPc2	dvojvládí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bolševici	bolševik	k1gMnPc5	bolševik
===	===	k?	===
</s>
</p>
<p>
<s>
Význačný	význačný	k2eAgMnSc1d1	význačný
představitel	představitel	k1gMnSc1	představitel
bolševické	bolševický	k2eAgFnSc2d1	bolševická
strany	strana	k1gFnSc2	strana
Vladimir	Vladimir	k1gMnSc1	Vladimir
Iljič	Iljič	k1gMnSc1	Iljič
Lenin	Lenin	k1gMnSc1	Lenin
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
únorové	únorový	k2eAgFnSc2d1	únorová
revoluce	revoluce	k1gFnSc2	revoluce
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
bolševiky	bolševik	k1gMnPc7	bolševik
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Svržení	svržení	k1gNnSc1	svržení
cara	car	k1gMnSc2	car
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1917	[number]	k4	1917
exilové	exilový	k2eAgInPc4d1	exilový
revolucionáře	revolucionář	k1gMnPc4	revolucionář
šokovalo	šokovat	k5eAaBmAgNnS	šokovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Lenina	Lenin	k1gMnSc2	Lenin
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
moci	moct	k5eAaImF	moct
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
ujala	ujmout	k5eAaPmAgFnS	ujmout
buržoazie	buržoazie	k1gFnSc1	buržoazie
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
revolucionáři	revolucionář	k1gMnPc1	revolucionář
zkusí	zkusit	k5eAaPmIp3nP	zkusit
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
a	a	k8xC	a
udělat	udělat	k5eAaPmF	udělat
revoluci	revoluce	k1gFnSc4	revoluce
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Únorová	únorový	k2eAgFnSc1d1	únorová
revoluce	revoluce	k1gFnSc1	revoluce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
procházka	procházka	k1gFnSc1	procházka
carským	carský	k2eAgNnSc7d1	carské
Ruskem	Rusko	k1gNnSc7	Rusko
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
