<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
neboli	neboli	k8xC	neboli
sňatek	sňatek	k1gInSc1	sňatek
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
vznik	vznik	k1gInSc1	vznik
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
ženy	žena	k1gFnSc2	žena
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
též	též	k9	též
vdavky	vdavka	k1gFnPc4	vdavka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
ženitba	ženitba	k1gFnSc1	ženitba
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
svatba	svatba	k1gFnSc1	svatba
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
praslov	praslovo	k1gNnPc2	praslovo
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
svętъ	svętъ	k?	svętъ
=	=	kIx~	=
"	"	kIx"	"
<g/>
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
<g/>
"	"	kIx"	"
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
kořene	kořen	k1gInSc2	kořen
indoevrop	indoevrop	k1gInSc1	indoevrop
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
su	su	k?	su
<g/>
̯	̯	k?	̯
<g/>
o-	o-	k?	o-
=	=	kIx~	=
"	"	kIx"	"
<g/>
svůj	svůj	k3xOyFgMnSc1	svůj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
sňatek	sňatek	k1gInSc1	sňatek
staročesky	staročesky	k6eAd1	staročesky
znamenal	znamenat	k5eAaImAgInS	znamenat
též	též	k9	též
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
spojení	spojení	k1gNnSc4	spojení
či	či	k8xC	či
počátek	počátek	k1gInSc4	počátek
<g/>
,	,	kIx,	,
z	z	k7c2	z
praslov	praslovo	k1gNnPc2	praslovo
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
sъ	sъ	k?	sъ
<g/>
(	(	kIx(	(
<g/>
j	j	k?	j
<g/>
)	)	kIx)	)
<g/>
ęt-ъ	ęt-ъ	k?	ęt-ъ
<g/>
.	.	kIx.	.
<g/>
Svatba	svatba	k1gFnSc1	svatba
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
společenský	společenský	k2eAgInSc1d1	společenský
obřad	obřad	k1gInSc1	obřad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
dvě	dva	k4xCgFnPc4	dva
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
osoby	osoba	k1gFnPc4	osoba
opačného	opačný	k2eAgNnSc2d1	opačné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
liberalizaci	liberalizace	k1gFnSc3	liberalizace
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
možné	možný	k2eAgInPc1d1	možný
uzavřít	uzavřít	k5eAaPmF	uzavřít
sňatek	sňatek	k1gInSc4	sňatek
i	i	k9	i
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
obřad	obřad	k1gInSc1	obřad
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
světových	světový	k2eAgFnPc2d1	světová
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sociálních	sociální	k2eAgFnPc6d1	sociální
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgFnPc6d1	náboženská
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
odlišnostech	odlišnost	k1gFnPc6	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
můžeme	moct	k5eAaImIp1nP	moct
sledovat	sledovat	k5eAaImF	sledovat
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
úrovních	úroveň	k1gFnPc6	úroveň
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
obřadu	obřad	k1gInSc2	obřad
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
důsledků	důsledek	k1gInPc2	důsledek
se	se	k3xPyFc4	se
měnil	měnit	k5eAaImAgInS	měnit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
na	na	k7c6	na
základě	základ	k1gInSc6	základ
obecných	obecný	k2eAgInPc2d1	obecný
společenských	společenský	k2eAgInPc2d1	společenský
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgInPc7d1	důležitý
faktory	faktor	k1gInPc7	faktor
pro	pro	k7c4	pro
historický	historický	k2eAgInSc4d1	historický
vývoj	vývoj	k1gInSc4	vývoj
svateb	svatba	k1gFnPc2	svatba
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
(	(	kIx(	(
<g/>
kult	kult	k1gInSc1	kult
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc4	vliv
církve	církev	k1gFnSc2	církev
<g/>
)	)	kIx)	)
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
především	především	k9	především
subjektivnější	subjektivní	k2eAgInPc1d2	subjektivnější
důvody	důvod	k1gInPc1	důvod
(	(	kIx(	(
<g/>
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc1	peníz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svatby	svatba	k1gFnPc4	svatba
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nahlížet	nahlížet	k5eAaImF	nahlížet
jak	jak	k6eAd1	jak
ze	z	k7c2	z
sociologického	sociologický	k2eAgNnSc2d1	sociologické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
např.	např.	kA	např.
I	i	k9	i
z	z	k7c2	z
teologického	teologický	k2eAgNnSc2d1	teologické
<g/>
,	,	kIx,	,
psychologického	psychologický	k2eAgNnSc2d1	psychologické
nebo	nebo	k8xC	nebo
politického	politický	k2eAgNnSc2d1	politické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
sňatkem	sňatek	k1gInSc7	sňatek
většinou	většinou	k6eAd1	většinou
předchází	předcházet	k5eAaImIp3nS	předcházet
řada	řada	k1gFnSc1	řada
příprav	příprava	k1gFnPc2	příprava
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
závazné	závazný	k2eAgInPc1d1	závazný
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
spíše	spíše	k9	spíše
tradiční	tradiční	k2eAgInPc1d1	tradiční
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zásnuby	zásnub	k1gInPc1	zásnub
<g/>
,	,	kIx,	,
zasnoubení	zasnoubení	k1gNnPc1	zasnoubení
<g/>
,	,	kIx,	,
zaslíbení	zaslíbení	k1gNnSc1	zaslíbení
čili	čili	k8xC	čili
dohoda	dohoda	k1gFnSc1	dohoda
zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c4	o
sňatek	sňatek	k1gInSc4	sňatek
o	o	k7c6	o
záměru	záměr	k1gInSc6	záměr
uzavření	uzavření	k1gNnSc2	uzavření
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
o	o	k7c6	o
svatbě	svatba	k1gFnSc6	svatba
</s>
</p>
<p>
<s>
ženichova	ženichův	k2eAgFnSc1d1	ženichova
žádost	žádost	k1gFnSc1	žádost
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
rodičů	rodič	k1gMnPc2	rodič
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
o	o	k7c4	o
souhlas	souhlas	k1gInSc4	souhlas
nevěstiných	nevěstin	k2eAgMnPc2d1	nevěstin
rodičů	rodič	k1gMnPc2	rodič
se	s	k7c7	s
svatbou	svatba	k1gFnSc7	svatba
</s>
</p>
<p>
<s>
objednání	objednání	k1gNnSc1	objednání
obřadu	obřad	k1gInSc2	obřad
<g/>
;	;	kIx,	;
u	u	k7c2	u
církevního	církevní	k2eAgInSc2d1	církevní
sňatku	sňatek	k1gInSc2	sňatek
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
církve	církev	k1gFnSc2	církev
případně	případně	k6eAd1	případně
i	i	k9	i
další	další	k2eAgInPc4d1	další
úkony	úkon	k1gInPc4	úkon
</s>
</p>
<p>
<s>
uzavření	uzavření	k1gNnSc1	uzavření
předmanželské	předmanželský	k2eAgFnSc2d1	předmanželská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
majetku	majetek	k1gInSc2	majetek
snoubenců	snoubenec	k1gMnPc2	snoubenec
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
majetku	majetek	k1gInSc2	majetek
získaného	získaný	k2eAgInSc2d1	získaný
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
a	a	k8xC	a
případného	případný	k2eAgNnSc2d1	případné
majekového	majekový	k2eAgNnSc2d1	majekový
vypořádání	vypořádání	k1gNnSc2	vypořádání
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rozvodu	rozvod	k1gInSc2	rozvod
manželství	manželství	k1gNnSc2	manželství
</s>
</p>
<p>
<s>
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
shody	shoda	k1gFnSc2	shoda
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
snoubenců	snoubenec	k1gMnPc2	snoubenec
přestoupí	přestoupit	k5eAaPmIp3nS	přestoupit
do	do	k7c2	do
církve	církev	k1gFnSc2	církev
snoubence	snoubenec	k1gMnSc2	snoubenec
<g/>
/	/	kIx~	/
<g/>
snoubenky	snoubenka	k1gFnPc1	snoubenka
</s>
</p>
<p>
<s>
zasílání	zasílání	k1gNnSc1	zasílání
svatební	svatební	k2eAgFnSc2d1	svatební
oznámení	oznámení	k1gNnSc4	oznámení
příbuzným	příbuzný	k2eAgMnSc7d1	příbuzný
a	a	k8xC	a
blízkým	blízký	k2eAgFnPc3d1	blízká
osobám	osoba	k1gFnPc3	osoba
</s>
</p>
<p>
<s>
opatření	opatření	k1gNnSc1	opatření
snubních	snubní	k2eAgInPc2d1	snubní
prstenů	prsten	k1gInPc2	prsten
a	a	k8xC	a
svatební	svatební	k2eAgFnSc2d1	svatební
kytice	kytice	k1gFnSc2	kytice
–	–	k?	–
objednání	objednání	k1gNnSc1	objednání
a	a	k8xC	a
koupě	koupě	k1gFnSc1	koupě
<g/>
,	,	kIx,	,
zvykově	zvykově	k6eAd1	zvykově
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
věc	věc	k1gFnSc1	věc
ženichovy	ženichův	k2eAgFnSc2d1	ženichova
péče	péče	k1gFnSc2	péče
</s>
</p>
<p>
<s>
dojednání	dojednání	k1gNnSc1	dojednání
svatebních	svatební	k2eAgMnPc2d1	svatební
svědků	svědek	k1gMnPc2	svědek
<g/>
,	,	kIx,	,
družiček	družička	k1gFnPc2	družička
a	a	k8xC	a
družbůVětšina	družbůVětšina	k1gFnSc1	družbůVětšina
párů	pár	k1gInPc2	pár
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
přípravami	příprava	k1gFnPc7	příprava
svatby	svatba	k1gFnSc2	svatba
přibližně	přibližně	k6eAd1	přibližně
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
uskutečněním	uskutečnění	k1gNnSc7	uskutečnění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
každého	každý	k3xTgInSc2	každý
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
míst	místo	k1gNnPc2	místo
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
koná	konat	k5eAaImIp3nS	konat
bezpočet	bezpočet	k1gInSc1	bezpočet
svatebních	svatební	k2eAgInPc2d1	svatební
veletrhů	veletrh	k1gInPc2	veletrh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
páry	pár	k1gInPc1	pár
načerpat	načerpat	k5eAaPmF	načerpat
inspiraci	inspirace	k1gFnSc4	inspirace
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
svatební	svatební	k2eAgInSc4d1	svatební
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Plánování	plánování	k1gNnSc1	plánování
a	a	k8xC	a
organizace	organizace	k1gFnSc1	organizace
svatby	svatba	k1gFnSc2	svatba
je	být	k5eAaImIp3nS	být
časově	časově	k6eAd1	časově
i	i	k9	i
organizačně	organizačně	k6eAd1	organizačně
značně	značně	k6eAd1	značně
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
najímány	najímán	k2eAgFnPc1d1	najímána
agentury	agentura	k1gFnPc1	agentura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
celkovou	celkový	k2eAgFnSc7d1	celková
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
od	od	k7c2	od
naplánování	naplánování	k1gNnSc2	naplánování
po	po	k7c4	po
závěr	závěr	k1gInSc4	závěr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Termíny	termín	k1gInPc1	termín
svateb	svatba	k1gFnPc2	svatba
konaných	konaný	k2eAgFnPc2d1	konaná
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
Valná	valný	k2eAgFnSc1d1	valná
většina	většina	k1gFnSc1	většina
svateb	svatba	k1gFnPc2	svatba
konaných	konaný	k2eAgFnPc2d1	konaná
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
-	-	kIx~	-
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgMnPc1d1	budoucí
novomanželé	novomanžel	k1gMnPc1	novomanžel
volí	volit	k5eAaImIp3nP	volit
letní	letní	k2eAgInPc4d1	letní
měsíce	měsíc	k1gInPc4	měsíc
zejména	zejména	k9	zejména
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
hezkého	hezký	k2eAgNnSc2d1	hezké
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
faktu	fakt	k1gInSc3	fakt
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
i	i	k9	i
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
popularita	popularita	k1gFnSc1	popularita
svateb	svatba	k1gFnPc2	svatba
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
popularity	popularita	k1gFnSc2	popularita
letních	letní	k2eAgInPc2d1	letní
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
zařizování	zařizování	k1gNnSc1	zařizování
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
svatebních	svatební	k2eAgFnPc2d1	svatební
záležitostí	záležitost	k1gFnPc2	záležitost
právě	právě	k9	právě
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
obtižnější	obtižný	k2eAgFnSc1d2	obtižný
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
řešit	řešit	k5eAaImF	řešit
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
náskokem	náskok	k1gInSc7	náskok
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ceny	cena	k1gFnPc1	cena
služeb	služba	k1gFnPc2	služba
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vyšší	vysoký	k2eAgInPc1d2	vyšší
než	než	k8xS	než
"	"	kIx"	"
<g/>
mimo	mimo	k7c4	mimo
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
svateb	svatba	k1gFnPc2	svatba
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c4	v
sobotní	sobotní	k2eAgInSc4d1	sobotní
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
páteční	páteční	k2eAgInSc4d1	páteční
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Extrémně	extrémně	k6eAd1	extrémně
exponované	exponovaný	k2eAgInPc1d1	exponovaný
jsou	být	k5eAaImIp3nP	být
potom	potom	k6eAd1	potom
termíny	termín	k1gInPc1	termín
se	s	k7c7	s
shodou	shoda	k1gFnSc7	shoda
číslic	číslice	k1gFnPc2	číslice
jako	jako	k8xC	jako
8.8	[number]	k4	8.8
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
16.6	[number]	k4	16.6
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
9.9	[number]	k4	9.9
<g/>
.	.	kIx.	.
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Svatba	svatba	k1gFnSc1	svatba
==	==	k?	==
</s>
</p>
<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
samotná	samotný	k2eAgFnSc1d1	samotná
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
části	část	k1gFnPc1	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sňatek	sňatek	k1gInSc4	sňatek
<g/>
,	,	kIx,	,
oddavky	oddavky	k1gFnPc4	oddavky
<g/>
,	,	kIx,	,
svatební	svatební	k2eAgInSc4d1	svatební
obřad	obřad	k1gInSc4	obřad
<g/>
,	,	kIx,	,
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
slibem	slib	k1gInSc7	slib
věrnosti	věrnost	k1gFnSc2	věrnost
a	a	k8xC	a
oddanosti	oddanost	k1gFnSc2	oddanost
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
předepsaný	předepsaný	k2eAgInSc1d1	předepsaný
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
autorizované	autorizovaný	k2eAgFnSc2d1	autorizovaná
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
pověřené	pověřený	k2eAgFnSc2d1	pověřená
státem	stát	k1gInSc7	stát
nebo	nebo	k8xC	nebo
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jinou	jiný	k2eAgFnSc7d1	jiná
institucí	instituce	k1gFnSc7	instituce
<g/>
,	,	kIx,	,
státem	stát	k1gInSc7	stát
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
oprávněnou	oprávněný	k2eAgFnSc7d1	oprávněná
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
oprávnění	oprávnění	k1gNnSc1	oprávnění
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
omezenou	omezený	k2eAgFnSc4d1	omezená
či	či	k8xC	či
podmíněnou	podmíněný	k2eAgFnSc4d1	podmíněná
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
církevní	církevní	k2eAgInPc4d1	církevní
sňatky	sňatek	k1gInPc4	sňatek
nebyly	být	k5eNaImAgFnP	být
státem	stát	k1gInSc7	stát
uznávány	uznávat	k5eAaImNgFnP	uznávat
jako	jako	k9	jako
plnohodnotné	plnohodnotný	k2eAgFnPc1d1	plnohodnotná
a	a	k8xC	a
pro	pro	k7c4	pro
právní	právní	k2eAgFnSc4d1	právní
závaznost	závaznost	k1gFnSc4	závaznost
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
provedeny	provést	k5eAaPmNgFnP	provést
i	i	k9	i
na	na	k7c6	na
úřadě	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
víceméně	víceméně	k9	víceméně
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
oslava	oslava	k1gFnSc1	oslava
<g/>
,	,	kIx,	,
svatební	svatební	k2eAgFnSc1d1	svatební
hostina	hostina	k1gFnSc1	hostina
<g/>
,	,	kIx,	,
veselka	veselka	k1gFnSc1	veselka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
zajištění	zajištění	k1gNnSc1	zajištění
bývalo	bývat	k5eAaImAgNnS	bývat
věcí	věc	k1gFnSc7	věc
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jejích	její	k3xOp3gMnPc2	její
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
či	či	k8xC	či
zvyk	zvyk	k1gInSc1	zvyk
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
je	být	k5eAaImIp3nS	být
svatba	svatba	k1gFnSc1	svatba
zařizována	zařizovat	k5eAaImNgFnS	zařizovat
svatebním	svatební	k2eAgInSc7d1	svatební
párem	pár	k1gInSc7	pár
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
či	či	k8xC	či
dle	dle	k7c2	dle
dispozic	dispozice	k1gFnPc2	dispozice
a	a	k8xC	a
domluvy	domluva	k1gFnSc2	domluva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oslavě	oslava	k1gFnSc6	oslava
se	se	k3xPyFc4	se
také	také	k9	také
provádějí	provádět	k5eAaImIp3nP	provádět
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
zvyky	zvyk	k1gInPc4	zvyk
dle	dle	k7c2	dle
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tuzemsku	tuzemsko	k1gNnSc6	tuzemsko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
například	například	k6eAd1	například
o	o	k7c4	o
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
krmení	krmení	k1gNnSc4	krmení
se	se	k3xPyFc4	se
novomanželů	novomanžel	k1gMnPc2	novomanžel
svatební	svatební	k2eAgFnSc7d1	svatební
polévkou	polévka	k1gFnSc7	polévka
(	(	kIx(	(
<g/>
vývar	vývar	k1gInSc1	vývar
s	s	k7c7	s
knedlíčky	knedlíček	k1gInPc7	knedlíček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
společné	společný	k2eAgNnSc1d1	společné
zametání	zametání	k1gNnSc1	zametání
rozbitého	rozbitý	k2eAgInSc2d1	rozbitý
talíře	talíř	k1gInSc2	talíř
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
na	na	k7c4	na
hostinu	hostina	k1gFnSc4	hostina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
==	==	k?	==
</s>
</p>
<p>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
svatebním	svatební	k2eAgInSc6d1	svatební
dni	den	k1gInSc6	den
následuje	následovat	k5eAaImIp3nS	následovat
svatební	svatební	k2eAgFnSc4d1	svatební
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
hrála	hrát	k5eAaImAgFnS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
zejména	zejména	k9	zejména
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
nejenže	nejenže	k6eAd1	nejenže
nastávají	nastávat	k5eAaImIp3nP	nastávat
novomanželům	novomanžel	k1gMnPc3	novomanžel
líbánky	líbánky	k1gInPc1	líbánky
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
radostné	radostný	k2eAgNnSc4d1	radostné
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
řeší	řešit	k5eAaImIp3nS	řešit
společné	společný	k2eAgNnSc4d1	společné
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
dosud	dosud	k6eAd1	dosud
bydleli	bydlet	k5eAaImAgMnP	bydlet
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
také	také	k9	také
jedou	jet	k5eAaImIp3nP	jet
na	na	k7c4	na
svatební	svatební	k2eAgFnSc4d1	svatební
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
během	během	k7c2	během
let	léto	k1gNnPc2	léto
manželství	manželství	k1gNnSc2	manželství
mohou	moct	k5eAaImIp3nP	moct
každoročně	každoročně	k6eAd1	každoročně
slavit	slavit	k5eAaImF	slavit
výročí	výročí	k1gNnSc4	výročí
svatby	svatba	k1gFnSc2	svatba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
kovové	kovový	k2eAgInPc1d1	kovový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
<g/>
,	,	kIx,	,
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
a	a	k8xC	a
zlatou	zlatý	k2eAgFnSc4d1	zlatá
svatbu	svatba	k1gFnSc4	svatba
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
i	i	k9	i
diamantovou	diamantový	k2eAgFnSc4d1	Diamantová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zánik	zánik	k1gInSc1	zánik
manželství	manželství	k1gNnSc1	manželství
===	===	k?	===
</s>
</p>
<p>
<s>
A	a	k9	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
rozvodem	rozvod	k1gInSc7	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
manželství	manželství	k1gNnSc1	manželství
končí	končit	k5eAaImIp3nS	končit
sice	sice	k8xC	sice
úmrtím	úmrtí	k1gNnSc7	úmrtí
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
společenský	společenský	k2eAgInSc1d1	společenský
status	status	k1gInSc1	status
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
<g/>
,	,	kIx,	,
respektuje	respektovat	k5eAaImIp3nS	respektovat
jakési	jakýsi	k3yIgNnSc1	jakýsi
jeho	jeho	k3xOp3gNnSc4	jeho
pokračování	pokračování	k1gNnSc4	pokračování
ve	v	k7c6	v
vdovství	vdovství	k1gNnSc6	vdovství
zbylé	zbylý	k2eAgFnSc2d1	zbylá
osobě	osoba	k1gFnSc3	osoba
vdovy	vdova	k1gFnSc2	vdova
či	či	k8xC	či
vdovce	vdovec	k1gMnSc2	vdovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Svatebčané	svatebčan	k1gMnPc5	svatebčan
==	==	k?	==
</s>
</p>
<p>
<s>
Snoubenci	snoubenec	k1gMnPc1	snoubenec
-	-	kIx~	-
ženich	ženich	k1gMnSc1	ženich
a	a	k8xC	a
nevěstaSvobodná	věstaSvobodný	k2eNgFnSc1d1	věstaSvobodný
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
slečna	slečna	k1gFnSc1	slečna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
již	již	k6eAd1	již
rozvedená	rozvedený	k2eAgFnSc1d1	rozvedená
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
paní	paní	k1gFnSc1	paní
<g/>
)	)	kIx)	)
vstupující	vstupující	k2eAgInPc1d1	vstupující
do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
(	(	kIx(	(
<g/>
snoubenka	snoubenka	k1gFnSc1	snoubenka
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
též	též	k9	též
"	"	kIx"	"
<g/>
nastávající	nastávající	k2eAgInSc4d1	nastávající
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
,	,	kIx,	,
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
novomanželka	novomanželka	k1gFnSc1	novomanželka
<g/>
,	,	kIx,	,
vdaná	vdaný	k2eAgFnSc1d1	vdaná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
manželova	manželův	k2eAgFnSc1d1	manželova
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
a	a	k8xC	a
oslovením	oslovení	k1gNnSc7	oslovení
(	(	kIx(	(
<g/>
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
)	)	kIx)	)
paní	paní	k1gFnSc1	paní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muž	muž	k1gMnSc1	muž
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
a	a	k8xC	a
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
snoubenec	snoubenec	k1gMnSc1	snoubenec
<g/>
,	,	kIx,	,
ženich	ženich	k1gMnSc1	ženich
<g/>
,	,	kIx,	,
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
ženatý	ženatý	k2eAgMnSc1d1	ženatý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
ženáč	ženáč	k1gMnSc1	ženáč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
manželčin	manželčin	k2eAgMnSc1d1	manželčin
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
i	i	k9	i
novomanžel	novomanžel	k1gMnSc1	novomanžel
<g/>
.	.	kIx.	.
<g/>
Oba	dva	k4xCgMnPc1	dva
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
snoubenci	snoubenec	k1gMnPc1	snoubenec
a	a	k8xC	a
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
manželé	manžel	k1gMnPc1	manžel
a	a	k8xC	a
novomanželé	novomanžel	k1gMnPc1	novomanžel
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jsou	být	k5eAaImIp3nP	být
svoji	svůj	k3xOyFgMnPc1	svůj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Právní	právní	k2eAgFnPc4d1	právní
náležitosti	náležitost	k1gFnPc4	náležitost
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
do	do	k7c2	do
matriky	matrika	k1gFnSc2	matrika
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
podpis	podpis	k1gInSc4	podpis
na	na	k7c6	na
zápisu	zápis	k1gInSc6	zápis
novomanželů	novomanžel	k1gMnPc2	novomanžel
a	a	k8xC	a
oddávajícího	oddávající	k2eAgNnSc2d1	oddávající
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
svatebních	svatební	k2eAgMnPc2d1	svatební
svědků	svědek	k1gMnPc2	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
čestnou	čestný	k2eAgFnSc4d1	čestná
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
určí	určit	k5eAaPmIp3nS	určit
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
snoubenci	snoubenec	k1gMnPc1	snoubenec
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jeden	jeden	k4xCgMnSc1	jeden
od	od	k7c2	od
nevěsty	nevěsta	k1gFnSc2	nevěsta
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
od	od	k7c2	od
ženicha	ženich	k1gMnSc2	ženich
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
hostů	host	k1gMnPc2	host
se	se	k3xPyFc4	se
svatby	svatba	k1gFnPc1	svatba
mohou	moct	k5eAaImIp3nP	moct
účastnit	účastnit	k5eAaImF	účastnit
osoby	osoba	k1gFnPc4	osoba
se	s	k7c7	s
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
pověřením	pověření	k1gNnSc7	pověření
od	od	k7c2	od
snoubenců	snoubenec	k1gMnPc2	snoubenec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
jednak	jednak	k8xC	jednak
družičky	družička	k1gFnPc1	družička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
nevěstě	nevěsta	k1gFnSc3	nevěsta
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
družbové	družba	k1gMnPc1	družba
čili	čili	k8xC	čili
pomocníci	pomocník	k1gMnPc1	pomocník
ženicha	ženich	k1gMnSc2	ženich
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
mají	mít	k5eAaImIp3nP	mít
bránit	bránit	k5eAaImF	bránit
nevěstu	nevěsta	k1gFnSc4	nevěsta
před	před	k7c7	před
únosci	únosce	k1gMnPc7	únosce
<g/>
.	.	kIx.	.
</s>
<s>
Oddávající	oddávající	k2eAgMnPc1d1	oddávající
jsou	být	k5eAaImIp3nP	být
pověření	pověřený	k2eAgMnPc1d1	pověřený
úředníci	úředník	k1gMnPc1	úředník
nebo	nebo	k8xC	nebo
církevní	církevní	k2eAgMnPc1d1	církevní
hodnostáři	hodnostář	k1gMnPc1	hodnostář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
svatbu	svatba	k1gFnSc4	svatba
vedou	vést	k5eAaImIp3nP	vést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Možnost	možnost	k1gFnSc1	možnost
uzavřít	uzavřít	k5eAaPmF	uzavřít
sňatek	sňatek	k1gInSc4	sňatek
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zletilí	zletilý	k2eAgMnPc1d1	zletilý
a	a	k8xC	a
svobodní	svobodný	k2eAgMnPc1d1	svobodný
čeští	český	k2eAgMnPc1d1	český
občané	občan	k1gMnPc1	občan
i	i	k8xC	i
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nebyla	být	k5eNaImAgFnS	být
soudem	soud	k1gInSc7	soud
omezena	omezit	k5eAaPmNgFnS	omezit
jejich	jejich	k3xOp3gFnSc1	jejich
práva	práv	k2eAgFnSc1d1	práva
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
manželství	manželství	k1gNnSc2	manželství
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
potřebné	potřebný	k2eAgInPc4d1	potřebný
doklady	doklad	k1gInPc4	doklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
doklad	doklad	k1gInSc1	doklad
totožnosti	totožnost	k1gFnSc2	totožnost
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
snoubenců	snoubenec	k1gMnPc2	snoubenec
</s>
</p>
<p>
<s>
občanské	občanský	k2eAgInPc4d1	občanský
průkazy	průkaz	k1gInPc4	průkaz
<g/>
,	,	kIx,	,
u	u	k7c2	u
cizinců	cizinec	k1gMnPc2	cizinec
cestovní	cestovní	k2eAgFnSc2d1	cestovní
pasy	pas	k1gInPc1	pas
</s>
</p>
<p>
<s>
rodné	rodný	k2eAgInPc1d1	rodný
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
u	u	k7c2	u
cizinců	cizinec	k1gMnPc2	cizinec
rovnocenný	rovnocenný	k2eAgInSc4d1	rovnocenný
doklad	doklad	k1gInSc4	doklad
o	o	k7c6	o
narození	narození	k1gNnSc6	narození
–	–	k?	–
zejména	zejména	k9	zejména
o	o	k7c6	o
místu	místo	k1gNnSc3	místo
a	a	k8xC	a
datu	datum	k1gNnSc3	datum
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
jménu	jméno	k1gNnSc3	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc4	příjmení
osoby	osoba	k1gFnSc2	osoba
–	–	k?	–
a	a	k8xC	a
stejné	stejný	k2eAgInPc4d1	stejný
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
jeho	jeho	k3xOp3gMnPc6	jeho
rodičích	rodič	k1gMnPc6	rodič
</s>
</p>
<p>
<s>
oprávnění	oprávnění	k1gNnSc4	oprávnění
uzavřít	uzavřít	k5eAaPmF	uzavřít
sňatek	sňatek	k1gInSc4	sňatek
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zabránění	zabránění	k1gNnSc4	zabránění
bigamie	bigamie	k1gFnSc2	bigamie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
výpis	výpis	k1gInSc1	výpis
z	z	k7c2	z
evidence	evidence	k1gFnSc2	evidence
obyvatel	obyvatel	k1gMnPc2	obyvatel
o	o	k7c6	o
osobním	osobní	k2eAgInSc6d1	osobní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
občanský	občanský	k2eAgInSc4d1	občanský
průkaz	průkaz	k1gInSc4	průkaz
<g/>
;	;	kIx,	;
u	u	k7c2	u
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
:	:	kIx,	:
potvrzení	potvrzení	k1gNnSc1	potvrzení
o	o	k7c6	o
osobním	osobní	k2eAgInSc6d1	osobní
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
pobytu	pobyt	k1gInSc6	pobyt
</s>
</p>
<p>
<s>
u	u	k7c2	u
rozvedených	rozvedený	k1gMnPc2	rozvedený
pravomocný	pravomocný	k2eAgInSc4d1	pravomocný
rozsudek	rozsudek	k1gInSc4	rozsudek
o	o	k7c6	o
rozvodu	rozvod	k1gInSc6	rozvod
předchozího	předchozí	k2eAgNnSc2d1	předchozí
manželství	manželství	k1gNnSc2	manželství
nebo	nebo	k8xC	nebo
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
partnerství	partnerství	k1gNnSc2	partnerství
</s>
</p>
<p>
<s>
u	u	k7c2	u
ovdovělých	ovdovělý	k2eAgInPc2d1	ovdovělý
úmrtní	úmrtní	k2eAgInSc4d1	úmrtní
list	list	k1gInSc4	list
manžela	manžel	k1gMnSc2	manžel
nebo	nebo	k8xC	nebo
partnera	partner	k1gMnSc2	partner
</s>
</p>
<p>
<s>
u	u	k7c2	u
cizinců	cizinec	k1gMnPc2	cizinec
doklad	doklad	k1gInSc1	doklad
o	o	k7c6	o
právní	právní	k2eAgFnSc6d1	právní
způsobilosti	způsobilost	k1gFnSc6	způsobilost
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
manželství	manželství	k1gNnSc2	manželství
nebo	nebo	k8xC	nebo
povolení	povolení	k1gNnSc2	povolení
domovského	domovský	k2eAgInSc2d1	domovský
státu	stát	k1gInSc2	stát
uzavřít	uzavřít	k5eAaPmF	uzavřít
manželství	manželství	k1gNnSc4	manželství
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
o	o	k7c6	o
dětech	dítě	k1gFnPc6	dítě
vstupujících	vstupující	k2eAgFnPc6d1	vstupující
sňatkem	sňatek	k1gInSc7	sňatek
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
jako	jako	k8xC	jako
vlastní	vlastní	k2eAgFnSc2d1	vlastní
</s>
</p>
<p>
<s>
rodné	rodný	k2eAgInPc1d1	rodný
listy	list	k1gInPc1	list
společných	společný	k2eAgFnPc2d1	společná
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc1	jejichž
otec	otec	k1gMnSc1	otec
dětí	dítě	k1gFnPc2	dítě
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
sňatku	sňatek	k1gInSc2	sňatek
==	==	k?	==
</s>
</p>
<p>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
uzavření	uzavření	k1gNnSc2	uzavření
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
církevní	církevní	k2eAgInSc4d1	církevní
sňatek	sňatek	k1gInSc4	sňatek
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
vedený	vedený	k2eAgInSc1d1	vedený
oddávajícím	oddávající	k2eAgNnSc7d1	oddávající
duchovním	duchovní	k2eAgNnSc7d1	duchovní
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
občanský	občanský	k2eAgInSc1d1	občanský
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
civilní	civilní	k2eAgInSc4d1	civilní
sňatek	sňatek	k1gInSc4	sňatek
<g/>
,	,	kIx,	,
vedený	vedený	k2eAgInSc4d1	vedený
obecním	obecní	k2eAgInSc7d1	obecní
nebo	nebo	k8xC	nebo
státním	státní	k2eAgMnSc7d1	státní
úředníkem	úředník	k1gMnSc7	úředník
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jinou	jiný	k2eAgFnSc7d1	jiná
oprávněnou	oprávněný	k2eAgFnSc7d1	oprávněná
osobou	osoba	k1gFnSc7	osoba
<g/>
.	.	kIx.	.
<g/>
Jejich	jejich	k3xOp3gFnPc1	jejich
platnosti	platnost	k1gFnPc1	platnost
nebývají	bývat	k5eNaImIp3nP	bývat
vždy	vždy	k6eAd1	vždy
zcela	zcela	k6eAd1	zcela
rovnocenné	rovnocenný	k2eAgFnSc2d1	rovnocenná
–	–	k?	–
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
státních	státní	k2eAgInPc2d1	státní
úřadů	úřad	k1gInPc2	úřad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
církevní	církevní	k2eAgInSc1d1	církevní
sňatek	sňatek	k1gInSc1	sňatek
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
neúplný	úplný	k2eNgInSc4d1	neúplný
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
některých	některý	k3yIgFnPc2	některý
církví	církev	k1gFnPc2	církev
občanský	občanský	k2eAgInSc4d1	občanský
sňatek	sňatek	k1gInSc4	sňatek
není	být	k5eNaImIp3nS	být
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
také	také	k9	také
podle	podle	k7c2	podle
církví	církev	k1gFnPc2	církev
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
něčím	něčí	k3xOyIgNnSc7	něčí
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
<g/>
,	,	kIx,	,
např.	např.	kA	např.
exotická	exotický	k2eAgFnSc1d1	exotická
nebo	nebo	k8xC	nebo
hromadná	hromadný	k2eAgFnSc1d1	hromadná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Svazky	svazek	k1gInPc7	svazek
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
==	==	k?	==
</s>
</p>
<p>
<s>
Napříč	napříč	k7c7	napříč
společností	společnost	k1gFnSc7	společnost
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
sňatky	sňatek	k1gInPc7	sňatek
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Pohledy	pohled	k1gInPc1	pohled
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
problematiku	problematika	k1gFnSc4	problematika
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
napříč	napříč	k7c7	napříč
kulturami	kultura	k1gFnPc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
ve	v	k7c6	v
vyspělejších	vyspělý	k2eAgFnPc6d2	vyspělejší
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
společenský	společenský	k2eAgInSc1d1	společenský
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
svazky	svazek	k1gInPc4	svazek
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
tyto	tento	k3xDgInPc1	tento
svazky	svazek	k1gInPc1	svazek
již	již	k6eAd1	již
legalizované	legalizovaný	k2eAgInPc1d1	legalizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
v	v	k7c6	v
méně	málo	k6eAd2	málo
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
svazky	svazek	k1gInPc4	svazek
nahlíženo	nahlížen	k2eAgNnSc1d1	nahlíženo
s	s	k7c7	s
opovržením	opovržení	k1gNnSc7	opovržení
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
jasně	jasně	k6eAd1	jasně
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
Více	hodně	k6eAd2	hodně
v	v	k7c4	v
Registrované	registrovaný	k2eAgNnSc4d1	registrované
partnerství	partnerství	k1gNnSc4	partnerství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Svatby	svatba	k1gFnPc1	svatba
podle	podle	k7c2	podle
náboženství	náboženství	k1gNnSc2	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
KřesťanstvíŘímskokatolická	KřesťanstvíŘímskokatolický	k2eAgFnSc1d1	KřesťanstvíŘímskokatolický
církev	církev	k1gFnSc1	církev
-	-	kIx~	-
svatební	svatební	k2eAgInSc1d1	svatební
průvod	průvod	k1gInSc1	průvod
vchází	vcházet	k5eAaImIp3nS	vcházet
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
poslední	poslední	k2eAgInPc1d1	poslední
přicházejí	přicházet	k5eAaImIp3nP	přicházet
ženich	ženich	k1gMnSc1	ženich
s	s	k7c7	s
nevěstou	nevěsta	k1gFnSc7	nevěsta
k	k	k7c3	k
oltáři	oltář	k1gInSc3	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Kněz	kněz	k1gMnSc1	kněz
odříkává	odříkávat	k5eAaImIp3nS	odříkávat
pasáže	pasáž	k1gFnPc4	pasáž
z	z	k7c2	z
Písma	písmo	k1gNnSc2	písmo
svatého	svatý	k1gMnSc2	svatý
a	a	k8xC	a
předčítá	předčítat	k5eAaImIp3nS	předčítat
svatební	svatební	k2eAgInSc4d1	svatební
slib	slib	k1gInSc4	slib
<g/>
.	.	kIx.	.
</s>
<s>
Snoubenci	snoubenec	k1gMnPc1	snoubenec
si	se	k3xPyFc3	se
vymění	vyměnit	k5eAaPmIp3nP	vyměnit
prsteny	prsten	k1gInPc4	prsten
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
modlitba	modlitba	k1gFnSc1	modlitba
<g/>
,	,	kIx,	,
zpěvy	zpěv	k1gInPc1	zpěv
a	a	k8xC	a
blahopřání	blahopřání	k1gNnSc1	blahopřání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
-	-	kIx~	-
</s>
</p>
<p>
<s>
Evangelická	evangelický	k2eAgFnSc1d1	evangelická
církev	církev	k1gFnSc1	církev
-	-	kIx~	-
farář	farář	k1gMnSc1	farář
čeká	čekat	k5eAaImIp3nS	čekat
před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
(	(	kIx(	(
<g/>
portálem	portál	k1gInSc7	portál
<g/>
)	)	kIx)	)
na	na	k7c4	na
ženicha	ženich	k1gMnSc4	ženich
s	s	k7c7	s
nevěstou	nevěsta	k1gFnSc7	nevěsta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
mohl	moct	k5eAaImAgInS	moct
zavést	zavést	k5eAaPmF	zavést
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svatebčany	svatebčan	k1gMnPc4	svatebčan
ke	k	k7c3	k
svatebnímu	svatební	k2eAgInSc3d1	svatební
oltáři	oltář	k1gInSc3	oltář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
průvodu	průvod	k1gInSc2	průvod
stojí	stát	k5eAaImIp3nS	stát
kněz	kněz	k1gMnSc1	kněz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Unitářství	unitářství	k1gNnSc1	unitářství
-	-	kIx~	-
svatební	svatební	k2eAgInSc4d1	svatební
obřad	obřad	k1gInSc4	obřad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgFnSc1d1	variabilní
a	a	k8xC	a
mít	mít	k5eAaImF	mít
různou	různý	k2eAgFnSc4d1	různá
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
lišit	lišit	k5eAaImF	lišit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
může	moct	k5eAaImIp3nS	moct
podoba	podoba	k1gFnSc1	podoba
svatebního	svatební	k2eAgInSc2d1	svatební
slibu	slib	k1gInSc2	slib
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
unitářského	unitářský	k2eAgNnSc2d1	unitářské
duchovního	duchovní	k2eAgNnSc2d1	duchovní
<g/>
.	.	kIx.	.
<g/>
JudaismusIslám	JudaismusIsla	k1gFnPc3	JudaismusIsla
</s>
</p>
<p>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
</s>
</p>
<p>
<s>
==	==	k?	==
Svatební	svatební	k2eAgInPc4d1	svatební
zvyky	zvyk	k1gInPc4	zvyk
==	==	k?	==
</s>
</p>
<p>
<s>
Svatební	svatební	k2eAgInPc1d1	svatební
zvyky	zvyk	k1gInPc1	zvyk
jsou	být	k5eAaImIp3nP	být
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
o	o	k7c6	o
svatebním	svatební	k2eAgInSc6d1	svatební
dni	den	k1gInSc6	den
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
se	se	k3xPyFc4	se
svatbou	svatba	k1gFnSc7	svatba
jinak	jinak	k6eAd1	jinak
spjaté	spjatý	k2eAgNnSc1d1	spjaté
<g/>
.	.	kIx.	.
jiné	jiný	k2eAgNnSc4d1	jiné
nejen	nejen	k6eAd1	nejen
zemi	zem	k1gFnSc4	zem
od	od	k7c2	od
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kraj	kraj	k1gInSc1	kraj
od	od	k7c2	od
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
<g/>
Nejběžnější	běžný	k2eAgInPc4d3	nejběžnější
svatební	svatební	k2eAgInPc4d1	svatební
zvyky	zvyk	k1gInPc4	zvyk
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
</s>
</p>
<p>
<s>
Zdobení	zdobení	k1gNnSc1	zdobení
svatebních	svatební	k2eAgNnPc2d1	svatební
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
automobilů	automobil	k1gInPc2	automobil
stuhami	stuha	k1gFnPc7	stuha
a	a	k8xC	a
květinami	květina	k1gFnPc7	květina
a	a	k8xC	a
zdobení	zdobení	k1gNnSc1	zdobení
svatebního	svatební	k2eAgInSc2d1	svatební
sálu	sál	k1gInSc2	sál
-	-	kIx~	-
dekorace	dekorace	k1gFnSc1	dekorace
stolů	stol	k1gInPc2	stol
<g/>
,	,	kIx,	,
židlí	židle	k1gFnPc2	židle
</s>
</p>
<p>
<s>
Svatební	svatební	k2eAgInPc1d1	svatební
koláčky	koláček	k1gInPc1	koláček
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
zvou	zvát	k5eAaImIp3nP	zvát
svatební	svatební	k2eAgMnPc1d1	svatební
hosté	host	k1gMnPc1	host
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
malé	malý	k2eAgInPc4d1	malý
koláčky	koláček	k1gInPc4	koláček
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
do	do	k7c2	do
asi	asi	k9	asi
5	[number]	k4	5
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
koláčové	koláčový	k2eAgFnSc2d1	koláčová
jednohubky	jednohubka	k1gFnSc2	jednohubka
<g/>
,	,	kIx,	,
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
náplněmi	náplň	k1gFnPc7	náplň
<g/>
.	.	kIx.	.
</s>
<s>
Svatební	svatební	k2eAgInPc4d1	svatební
koláčky	koláček	k1gInPc4	koláček
nesmí	smět	k5eNaImIp3nS	smět
péct	péct	k5eAaImF	péct
sama	sám	k3xTgFnSc1	sám
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sní	snít	k5eAaImIp3nS	snít
koláček	koláček	k1gInSc4	koláček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
upečen	upéct	k5eAaPmNgInS	upéct
v	v	k7c6	v
rohu	roh	k1gInSc6	roh
plechu	plech	k1gInSc2	plech
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
roka	rok	k1gInSc2	rok
a	a	k8xC	a
do	do	k7c2	do
dne	den	k1gInSc2	den
vdá	vdát	k5eAaPmIp3nS	vdát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblečení	oblečení	k1gNnSc1	oblečení
nevěsty	nevěsta	k1gFnSc2	nevěsta
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
něco	něco	k3yInSc4	něco
nového	nový	k2eAgMnSc2d1	nový
<g/>
,	,	kIx,	,
starého	starý	k2eAgInSc2d1	starý
<g/>
,	,	kIx,	,
půjčeného	půjčený	k2eAgInSc2d1	půjčený
a	a	k8xC	a
modrého	modré	k1gNnSc2	modré
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Voničky	vonička	k1gFnPc4	vonička
nebo	nebo	k8xC	nebo
vývazek	vývazek	k1gInSc4	vývazek
dostane	dostat	k5eAaPmIp3nS	dostat
každý	každý	k3xTgMnSc1	každý
svatební	svatební	k2eAgMnSc1d1	svatební
host	host	k1gMnSc1	host
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
klopy	klopa	k1gFnSc2	klopa
nebo	nebo	k8xC	nebo
na	na	k7c4	na
šaty	šat	k1gInPc4	šat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Falešná	falešný	k2eAgFnSc1d1	falešná
nevěsta	nevěsta	k1gFnSc1	nevěsta
–	–	k?	–
někdo	někdo	k3yInSc1	někdo
se	se	k3xPyFc4	se
převlékne	převléknout	k5eAaPmIp3nS	převléknout
za	za	k7c4	za
nevěstu	nevěsta	k1gFnSc4	nevěsta
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
přebrat	přebrat	k5eAaPmF	přebrat
pravé	pravý	k2eAgFnSc3d1	pravá
nevěstě	nevěsta	k1gFnSc3	nevěsta
ženicha	ženich	k1gMnSc2	ženich
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
po	po	k7c6	po
obřadu	obřad	k1gInSc6	obřad
</s>
</p>
<p>
<s>
Házení	házení	k1gNnSc1	házení
kyticí	kytice	k1gFnSc7	kytice
–	–	k?	–
nevěsta	nevěsta	k1gFnSc1	nevěsta
si	se	k3xPyFc3	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
obřadu	obřad	k1gInSc2	obřad
či	či	k8xC	či
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
tanci	tanec	k1gInSc6	tanec
a	a	k8xC	a
fotografování	fotografování	k1gNnSc6	fotografování
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
zády	záda	k1gNnPc7	záda
k	k	k7c3	k
neprovdaným	provdaný	k2eNgFnPc3d1	neprovdaná
dívkám	dívka	k1gFnPc3	dívka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
svatby	svatba	k1gFnSc2	svatba
účastní	účastnit	k5eAaImIp3nS	účastnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
hodí	hodit	k5eAaPmIp3nS	hodit
kytici	kytice	k1gFnSc4	kytice
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kytici	kytice	k1gFnSc4	kytice
chytí	chytit	k5eAaPmIp3nS	chytit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
roka	rok	k1gInSc2	rok
jistě	jistě	k9	jistě
bude	být	k5eAaImBp3nS	být
vdávat	vdávat	k5eAaImF	vdávat
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
převzatá	převzatý	k2eAgFnSc1d1	převzatá
z	z	k7c2	z
anglosaských	anglosaský	k2eAgFnPc2d1	anglosaská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zasypávání	zasypávání	k1gNnSc1	zasypávání
rýží	rýže	k1gFnPc2	rýže
–	–	k?	–
při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
obřadní	obřadní	k2eAgFnSc2d1	obřadní
síně	síň	k1gFnSc2	síň
zasypávají	zasypávat	k5eAaImIp3nP	zasypávat
svatebčané	svatebčan	k1gMnPc1	svatebčan
novomanželský	novomanželský	k2eAgInSc4d1	novomanželský
pár	pár	k4xCyI	pár
rýží	rýže	k1gFnSc7	rýže
–	–	k?	–
přivolávají	přivolávat	k5eAaImIp3nP	přivolávat
tak	tak	k9	tak
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
plodnost	plodnost	k1gFnSc4	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
konfetami	konfeta	k1gFnPc7	konfeta
a	a	k8xC	a
flitry	flitr	k1gInPc7	flitr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chomout	chomout	k1gInSc1	chomout
dostane	dostat	k5eAaPmIp3nS	dostat
novomanžel	novomanžel	k1gMnSc1	novomanžel
na	na	k7c4	na
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
symbol	symbol	k1gInSc4	symbol
svázání	svázání	k1gNnSc2	svázání
manželstvím	manželství	k1gNnSc7	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Chomout	chomout	k1gInSc1	chomout
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
doplněn	doplněn	k2eAgMnSc1d1	doplněn
i	i	k9	i
železnou	železný	k2eAgFnSc7d1	železná
koulí	koule	k1gFnSc7	koule
přiveněnou	přiveněný	k2eAgFnSc7d1	přiveněný
nad	nad	k7c4	nad
kotník	kotník	k1gInSc4	kotník
ženicha	ženich	k1gMnSc2	ženich
řetězem	řetěz	k1gInSc7	řetěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
–	–	k?	–
zpravidla	zpravidla	k6eAd1	zpravidla
svatební	svatební	k2eAgFnSc6d1	svatební
hostině	hostina	k1gFnSc6	hostina
</s>
</p>
<p>
<s>
Střepy	střep	k1gInPc1	střep
–	–	k?	–
při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
na	na	k7c4	na
svatební	svatební	k2eAgFnSc4d1	svatební
hostinu	hostina	k1gFnSc4	hostina
rozbije	rozbít	k5eAaPmIp3nS	rozbít
obsluhující	obsluhující	k2eAgInSc1d1	obsluhující
personál	personál	k1gInSc1	personál
talíř	talíř	k1gInSc1	talíř
<g/>
,	,	kIx,	,
nevěsta	nevěsta	k1gFnSc1	nevěsta
s	s	k7c7	s
ženichem	ženich	k1gMnSc7	ženich
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nS	muset
střepy	střep	k1gInPc4	střep
zamést	zamést	k5eAaPmF	zamést
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
k	k	k7c3	k
úkolu	úkol	k1gInSc3	úkol
postaví	postavit	k5eAaPmIp3nS	postavit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jim	on	k3xPp3gFnPc3	on
bude	být	k5eAaImBp3nS	být
klapat	klapat	k5eAaImF	klapat
společná	společný	k2eAgFnSc1d1	společná
domácnost	domácnost	k1gFnSc1	domácnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polévka	polévka	k1gFnSc1	polévka
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
talíře	talíř	k1gInSc2	talíř
novomanželé	novomanžel	k1gMnPc1	novomanžel
jsou	být	k5eAaImIp3nP	být
svázáni	svázat	k5eAaPmNgMnP	svázat
bílým	bílý	k2eAgInSc7d1	bílý
ubrusem	ubrus	k1gInSc7	ubrus
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
se	se	k3xPyFc4	se
krmí	krmit	k5eAaImIp3nS	krmit
jednou	jeden	k4xCgFnSc7	jeden
lžící	lžíce	k1gFnSc7	lžíce
<g/>
.	.	kIx.	.
</s>
<s>
Svatební	svatební	k2eAgFnSc7d1	svatební
polevkou	polevka	k1gFnSc7	polevka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
většinou	většina	k1gFnSc7	většina
vývar	vývar	k1gInSc4	vývar
s	s	k7c7	s
knedlíčky	knedlíček	k1gInPc7	knedlíček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krájení	krájení	k1gNnSc1	krájení
svatebního	svatební	k2eAgInSc2d1	svatební
dortu	dort	k1gInSc2	dort
–	–	k?	–
novomanželé	novomanžel	k1gMnPc1	novomanžel
opět	opět	k6eAd1	opět
dostávají	dostávat	k5eAaImIp3nP	dostávat
jediný	jediný	k2eAgInSc4d1	jediný
nůž	nůž	k1gInSc4	nůž
a	a	k8xC	a
společně	společně	k6eAd1	společně
nakrojí	nakrojit	k5eAaPmIp3nS	nakrojit
dort	dort	k1gInSc1	dort
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
ze	z	k7c2	z
svatebčanů	svatebčan	k1gMnPc2	svatebčan
nesmí	smět	k5eNaImIp3nS	smět
dort	dort	k1gInSc4	dort
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
manželský	manželský	k2eAgInSc1d1	manželský
tanec	tanec	k1gInSc1	tanec
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
sólo	sólo	k2eAgInSc1d1	sólo
tanec	tanec	k1gInSc1	tanec
<g/>
,	,	kIx,	,
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
pouze	pouze	k6eAd1	pouze
novomanželům	novomanžel	k1gMnPc3	novomanžel
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
svatebčané	svatebčan	k1gMnPc1	svatebčan
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
kolem	kolem	k7c2	kolem
tančících	tančící	k2eAgMnPc2d1	tančící
novomanželů	novomanžel	k1gMnPc2	novomanžel
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Únos	únos	k1gInSc1	únos
nevěsty	nevěsta	k1gFnSc2	nevěsta
–	–	k?	–
Tradice	tradice	k1gFnSc1	tradice
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
zejména	zejména	k9	zejména
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
přátel	přítel	k1gMnPc2	přítel
obou	dva	k4xCgInPc2	dva
novomanželů	novomanžel	k1gMnPc2	novomanžel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
oslavy	oslava	k1gFnSc2	oslava
unesou	unést	k5eAaPmIp3nP	unést
novomanželku	novomanželka	k1gFnSc4	novomanželka
do	do	k7c2	do
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
hospůdek	hospůdka	k1gFnPc2	hospůdka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rozpustile	rozpustile	k6eAd1	rozpustile
a	a	k8xC	a
nekontrolovatelně	kontrolovatelně	k6eNd1	kontrolovatelně
zapíjejí	zapíjet	k5eAaImIp3nP	zapíjet
štěstí	štěstí	k1gNnSc4	štěstí
novomanželů	novomanžel	k1gMnPc2	novomanžel
<g/>
.	.	kIx.	.
</s>
<s>
Novomanžel	novomanžel	k1gMnSc1	novomanžel
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
najít	najít	k5eAaPmF	najít
co	co	k3yQnSc4	co
nejdříve	dříve	k6eAd3	dříve
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
celou	celý	k2eAgFnSc4d1	celá
útratu	útrata	k1gFnSc4	útrata
těchto	tento	k3xDgMnPc2	tento
přátel	přítel	k1gMnPc2	přítel
platí	platit	k5eAaImIp3nS	platit
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
zvyku	zvyk	k1gInSc2	zvyk
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInPc4	jeho
často	často	k6eAd1	často
neblahé	blahý	k2eNgInPc4d1	neblahý
následky	následek	k1gInPc4	následek
na	na	k7c4	na
průběh	průběh	k1gInSc4	průběh
svatební	svatební	k2eAgFnSc2d1	svatební
hostiny	hostina	k1gFnSc2	hostina
upouští	upouštět	k5eAaImIp3nS	upouštět
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
bývá	bývat	k5eAaImIp3nS	bývat
novomanželským	novomanželský	k2eAgInSc7d1	novomanželský
párem	pár	k1gInSc7	pár
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přinesení	přinesení	k1gNnSc1	přinesení
novomanželky	novomanželka	k1gFnSc2	novomanželka
–	–	k?	–
Ženich	ženich	k1gMnSc1	ženich
má	mít	k5eAaImIp3nS	mít
přenést	přenést	k5eAaPmF	přenést
přes	přes	k7c4	přes
práh	práh	k1gInSc4	práh
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
náručí	náručí	k1gNnSc6	náručí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatební	svatební	k2eAgFnSc1d1	svatební
cesta	cesta	k1gFnSc1	cesta
–	–	k?	–
Posvatební	posvatební	k2eAgFnSc1d1	posvatební
rekreační	rekreační	k2eAgFnSc1d1	rekreační
cesta	cesta	k1gFnSc1	cesta
novomanželů	novomanžel	k1gMnPc2	novomanžel
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
usnadnit	usnadnit	k5eAaPmF	usnadnit
přechod	přechod	k1gInSc4	přechod
ke	k	k7c3	k
každodennímu	každodenní	k2eAgNnSc3d1	každodenní
soužití	soužití	k1gNnSc3	soužití
novomanželů	novomanžel	k1gMnPc2	novomanžel
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
tradiční	tradiční	k2eAgInPc4d1	tradiční
zvyky	zvyk	k1gInPc4	zvyk
patří	patřit	k5eAaImIp3nS	patřit
některé	některý	k3yIgFnPc4	některý
části	část	k1gFnPc4	část
oděvu	oděv	k1gInSc2	oděv
či	či	k8xC	či
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
nevěsta	nevěsta	k1gFnSc1	nevěsta
na	na	k7c6	na
sobě	se	k3xPyFc3	se
v	v	k7c4	v
den	den	k1gInSc4	den
svatby	svatba	k1gFnSc2	svatba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nová	nový	k2eAgFnSc1d1	nová
věc	věc	k1gFnSc1	věc
symbolizující	symbolizující	k2eAgFnSc2d1	symbolizující
nový	nový	k2eAgInSc4d1	nový
začátek	začátek	k1gInSc4	začátek
</s>
</p>
<p>
<s>
stará	starý	k2eAgFnSc1d1	stará
věc	věc	k1gFnSc1	věc
značící	značící	k2eAgNnSc4d1	značící
zachování	zachování	k1gNnSc4	zachování
rodinné	rodinný	k2eAgFnSc2d1	rodinná
tradice	tradice	k1gFnSc2	tradice
</s>
</p>
<p>
<s>
zapůjčená	zapůjčený	k2eAgFnSc1d1	zapůjčená
věc	věc	k1gFnSc1	věc
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
toho	ten	k3xDgMnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
budeme	být	k5eAaImBp1nP	být
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
rady	rada	k1gFnPc4	rada
druhých	druhý	k4xOgFnPc2	druhý
</s>
</p>
<p>
<s>
modrá	modrý	k2eAgFnSc1d1	modrá
jakožto	jakožto	k8xS	jakožto
barva	barva	k1gFnSc1	barva
věrnosti	věrnost	k1gFnSc2	věrnost
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
tradice	tradice	k1gFnPc1	tradice
ovšem	ovšem	k9	ovšem
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
modré	modrý	k2eAgFnSc3d1	modrá
barvě	barva	k1gFnSc3	barva
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
"	"	kIx"	"
<g/>
modřiny	modřina	k1gFnPc1	modřina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
Stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
je	být	k5eAaImIp3nS	být
též	též	k9	též
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
svatebními	svatební	k2eAgInPc7d1	svatební
zvyky	zvyk	k1gInPc7	zvyk
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
z	z	k7c2	z
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
novodobými	novodobý	k2eAgFnPc7d1	novodobá
svatebními	svatební	k2eAgFnPc7d1	svatební
tradicemi	tradice	k1gFnPc7	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
KOPECKÝ	Kopecký	k1gMnSc1	Kopecký
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Slovácká	slovácký	k2eAgFnSc1d1	Slovácká
svatba	svatba	k1gFnSc1	svatba
na	na	k7c6	na
Podluží	Podluží	k1gNnSc6	Podluží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
LAHEY	LAHEY	kA	LAHEY
<g/>
,	,	kIx,	,
Kathleen	Kathleen	k1gInSc1	Kathleen
A.	A.	kA	A.
a	a	k8xC	a
Kevin	Kevin	k1gMnSc1	Kevin
ALDERSON	ALDERSON	kA	ALDERSON
<g/>
.	.	kIx.	.
</s>
<s>
Same-sex	Sameex	k1gInSc1	Same-sex
marriage	marriag	k1gInSc2	marriag
<g/>
:	:	kIx,	:
the	the	k?	the
personal	personat	k5eAaPmAgMnS	personat
and	and	k?	and
the	the	k?	the
political	politicat	k5eAaPmAgMnS	politicat
<g/>
.	.	kIx.	.
</s>
<s>
Kathleen	Kathleen	k1gInSc1	Kathleen
A.	A.	kA	A.
Lahey	Lahea	k1gMnSc2	Lahea
and	and	k?	and
Kevin	Kevin	k1gMnSc1	Kevin
Alderson	Alderson	k1gMnSc1	Alderson
<g/>
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Andrea	Andrea	k1gFnSc1	Andrea
Sluková	Sluková	k1gFnSc1	Sluková
<g/>
:	:	kIx,	:
Svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
průvodce	průvodce	k1gMnSc2	průvodce
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
i	i	k8xC	i
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Grada	Grada	k1gFnSc1	Grada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-247-9064-5	[number]	k4	80-247-9064-5
</s>
</p>
<p>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Ivens	Ivensa	k1gFnPc2	Ivensa
<g/>
:	:	kIx,	:
Svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
nikdo	nikdo	k3yNnSc1	nikdo
nezapomene	zapomnět	k5eNaImIp3nS	zapomnět
<g/>
,	,	kIx,	,
rady	rada	k1gFnPc1	rada
pro	pro	k7c4	pro
nevěsty	nevěsta	k1gFnPc4	nevěsta
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Pragma	Pragmum	k1gNnSc2	Pragmum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7349-116-1	[number]	k4	978-80-7349-116-1
</s>
</p>
<p>
<s>
Patricia	Patricius	k1gMnSc4	Patricius
Janečková	Janečková	k1gFnSc1	Janečková
<g/>
:	:	kIx,	:
Svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Grada	Grada	k1gFnSc1	Grada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-247-2583-3	[number]	k4	978-80-247-2583-3
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
</s>
</p>
<p>
<s>
Figarova	Figarův	k2eAgFnSc1d1	Figarova
svatba	svatba	k1gFnSc1	svatba
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
filmů	film	k1gInPc2	film
se	se	k3xPyFc4	se
svatební	svatební	k2eAgFnSc7d1	svatební
tematikou	tematika	k1gFnSc7	tematika
</s>
</p>
<p>
<s>
Výročí	výročí	k1gNnSc1	výročí
svatby	svatba	k1gFnSc2	svatba
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
svatba	svatba	k1gFnSc1	svatba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
svatba	svatba	k1gFnSc1	svatba
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Svatba	svatba	k1gFnSc1	svatba
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Svatba	svatba	k1gFnSc1	svatba
</s>
</p>
<p>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
v	v	k7c6	v
Sociologické	sociologický	k2eAgFnSc6d1	sociologická
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Sociologického	sociologický	k2eAgInSc2d1	sociologický
ústavu	ústav	k1gInSc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Uzavření	uzavření	k1gNnSc1	uzavření
manželství	manželství	k1gNnSc2	manželství
na	na	k7c6	na
webu	web	k1gInSc6	web
MV	MV	kA	MV
ČR	ČR	kA	ČR
vč.	vč.	k?	vč.
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
dokladech	doklad	k1gInPc6	doklad
potřebných	potřebný	k2eAgFnPc2d1	potřebná
k	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
</s>
</p>
