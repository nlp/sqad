<p>
<s>
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
alegorické	alegorický	k2eAgNnSc1d1	alegorické
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
společná	společný	k2eAgNnPc4d1	společné
díla	dílo	k1gNnPc4	dílo
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
komedii	komedie	k1gFnSc6	komedie
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
autoři	autor	k1gMnPc1	autor
uplatnili	uplatnit	k5eAaPmAgMnP	uplatnit
kritický	kritický	k2eAgInSc4d1	kritický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
soudobou	soudobý	k2eAgFnSc4d1	soudobá
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
všechny	všechen	k3xTgInPc4	všechen
vztahy	vztah	k1gInPc4	vztah
určují	určovat	k5eAaImIp3nP	určovat
sobecké	sobecký	k2eAgInPc4d1	sobecký
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Předmluva	předmluva	k1gFnSc1	předmluva
===	===	k?	===
</s>
</p>
<p>
<s>
Opilý	opilý	k2eAgMnSc1d1	opilý
tulák	tulák	k1gMnSc1	tulák
si	se	k3xPyFc3	se
v	v	k7c6	v
samomluvě	samomluva	k1gFnSc6	samomluva
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
lidé	člověk	k1gMnPc1	člověk
oslovují	oslovovat	k5eAaImIp3nP	oslovovat
"	"	kIx"	"
<g/>
člověče	člověk	k1gMnSc5	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sami	sám	k3xTgMnPc1	sám
by	by	kYmCp3nS	by
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
podobné	podobný	k2eAgNnSc1d1	podobné
oslovení	oslovení	k1gNnSc1	oslovení
nesnesli	snést	k5eNaPmAgMnP	snést
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
necítí	cítit	k5eNaImIp3nS	cítit
uražen	urazit	k5eAaPmNgMnS	urazit
oslovením	oslovení	k1gNnSc7	oslovení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
lidé	člověk	k1gMnPc1	člověk
nechtějí	chtít	k5eNaImIp3nP	chtít
být	být	k5eAaImF	být
takto	takto	k6eAd1	takto
oslovováni	oslovován	k2eAgMnPc1d1	oslovován
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
je	on	k3xPp3gNnSc4	on
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
pedant	pedant	k1gMnSc1	pedant
se	s	k7c7	s
síťkou	síťka	k1gFnSc7	síťka
na	na	k7c4	na
motýly	motýl	k1gMnPc4	motýl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vidí	vidět	k5eAaImIp3nS	vidět
řád	řád	k1gInSc4	řád
přírody	příroda	k1gFnSc2	příroda
ve	v	k7c6	v
věčném	věčný	k2eAgInSc6d1	věčný
zápase	zápas	k1gInSc6	zápas
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Tulák	tulák	k1gMnSc1	tulák
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
tuto	tento	k3xDgFnSc4	tento
myšlenku	myšlenka	k1gFnSc4	myšlenka
ve	v	k7c6	v
verši	verš	k1gInSc6	verš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgNnPc4	první
dějství	dějství	k1gNnPc4	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
dějství	dějství	k1gNnSc6	dějství
se	se	k3xPyFc4	se
tulák	tulák	k1gMnSc1	tulák
objevuje	objevovat	k5eAaImIp3nS	objevovat
mezi	mezi	k7c7	mezi
pěti	pět	k4xCc7	pět
motýly	motýl	k1gMnPc7	motýl
(	(	kIx(	(
<g/>
Iris	iris	k1gFnSc4	iris
<g/>
,	,	kIx,	,
Clythia	Clythia	k1gFnSc1	Clythia
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
a	a	k8xC	a
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
střídavě	střídavě	k6eAd1	střídavě
koketují	koketovat	k5eAaImIp3nP	koketovat
s	s	k7c7	s
oběma	dva	k4xCgMnPc7	dva
motýly	motýl	k1gMnPc7	motýl
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
odlétají	odlétat	k5eAaPmIp3nP	odlétat
s	s	k7c7	s
motýlem	motýl	k1gMnSc7	motýl
té	ten	k3xDgFnSc6	ten
druhé	druhý	k4xOgFnSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
se	se	k3xPyFc4	se
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejího	její	k3xOp3gMnSc4	její
milého	milý	k1gMnSc4	milý
sežral	sežrat	k5eAaPmAgMnS	sežrat
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
snášet	snášet	k5eAaImF	snášet
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
odlétá	odlétat	k5eAaPmIp3nS	odlétat
hledat	hledat	k5eAaImF	hledat
jiného	jiný	k2eAgMnSc4d1	jiný
motýla	motýl	k1gMnSc4	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Motýl	motýl	k1gMnSc1	motýl
básník	básník	k1gMnSc1	básník
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
osamocen	osamocen	k2eAgMnSc1d1	osamocen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
básně	báseň	k1gFnPc1	báseň
žádnou	žádný	k3yNgFnSc4	žádný
samičku	samička	k1gFnSc4	samička
nalákat	nalákat	k5eAaPmF	nalákat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhé	druhý	k4xOgNnSc1	druhý
dějství	dějství	k1gNnSc1	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
dějství	dějství	k1gNnSc6	dějství
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
pár	pár	k4xCyI	pár
kořistníků	kořistník	k1gMnPc2	kořistník
valící	valící	k2eAgFnSc1d1	valící
svoji	svůj	k3xOyFgFnSc4	svůj
kuličku	kulička	k1gFnSc4	kulička
trusu	trus	k1gInSc2	trus
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
hledají	hledat	k5eAaImIp3nP	hledat
úkryt	úkryt	k1gInSc4	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
drahocenným	drahocenný	k2eAgInSc7d1	drahocenný
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
smyslem	smysl	k1gInSc7	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
úkrytu	úkryt	k1gInSc2	úkryt
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
jiný	jiný	k2eAgMnSc1d1	jiný
kořistník	kořistník	k1gMnSc1	kořistník
kuličku	kulička	k1gFnSc4	kulička
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
chamtivý	chamtivý	k2eAgInSc1d1	chamtivý
je	být	k5eAaImIp3nS	být
i	i	k9	i
cvrček	cvrček	k1gMnSc1	cvrček
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
raduje	radovat	k5eAaImIp3nS	radovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jiného	jiný	k2eAgMnSc4d1	jiný
cvrčka	cvrček	k1gMnSc4	cvrček
sežral	sežrat	k5eAaPmAgMnS	sežrat
pták	pták	k1gMnSc1	pták
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
čeká	čekat	k5eAaImIp3nS	čekat
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
nastěhovat	nastěhovat	k5eAaPmF	nastěhovat
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
bytu	byt	k1gInSc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
ale	ale	k9	ale
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
stanou	stanout	k5eAaPmIp3nP	stanout
oběťmi	oběť	k1gFnPc7	oběť
lumka	lumek	k1gMnSc2	lumek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
zásoby	zásoba	k1gFnPc4	zásoba
potravy	potrava	k1gFnSc2	potrava
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
larvičku	larvička	k1gFnSc4	larvička
<g/>
.	.	kIx.	.
</s>
<s>
Larvičku	larvička	k1gFnSc4	larvička
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
se	s	k7c7	s
zásobami	zásoba	k1gFnPc7	zásoba
vytvořenými	vytvořený	k2eAgFnPc7d1	vytvořená
jejím	její	k3xOp3gMnSc7	její
otcem	otec	k1gMnSc7	otec
sežere	sežrat	k5eAaPmIp3nS	sežrat
parazit	parazit	k1gMnSc1	parazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednání	jednání	k1gNnPc2	jednání
tulák	tulák	k1gMnSc1	tulák
rozmlouvá	rozmlouvat	k5eAaImIp3nS	rozmlouvat
s	s	k7c7	s
kuklou	kukla	k1gFnSc7	kukla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neustále	neustále	k6eAd1	neustále
vykřikuje	vykřikovat	k5eAaImIp3nS	vykřikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
zrodí	zrodit	k5eAaPmIp3nS	zrodit
něco	něco	k3yInSc1	něco
ohromného	ohromný	k2eAgNnSc2d1	ohromné
a	a	k8xC	a
velkolepého	velkolepý	k2eAgNnSc2d1	velkolepé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgNnSc1	třetí
dějství	dějství	k1gNnSc1	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
dějství	dějství	k1gNnSc1	dějství
je	být	k5eAaImIp3nS	být
věnováno	věnovat	k5eAaImNgNnS	věnovat
mravencům	mravenec	k1gMnPc3	mravenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
rychlém	rychlý	k2eAgNnSc6d1	rychlé
tempu	tempo	k1gNnSc6	tempo
a	a	k8xC	a
neohlížejí	ohlížet	k5eNaImIp3nP	ohlížet
se	se	k3xPyFc4	se
na	na	k7c4	na
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
podléhají	podléhat	k5eAaImIp3nP	podléhat
tempu	tempo	k1gNnSc3	tempo
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
umírají	umírat	k5eAaImIp3nP	umírat
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
společenství	společenství	k1gNnSc1	společenství
vede	vést	k5eAaImIp3nS	vést
slepý	slepý	k2eAgMnSc1d1	slepý
mravenec	mravenec	k1gMnSc1	mravenec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
rychlost	rychlost	k1gFnSc4	rychlost
práce	práce	k1gFnSc2	práce
počítáním	počítání	k1gNnSc7	počítání
do	do	k7c2	do
čtyř	čtyři	k4xCgMnPc2	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
mravenci	mravenec	k1gMnPc1	mravenec
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
velkém	velký	k2eAgNnSc6d1	velké
území	území	k1gNnSc6	území
a	a	k8xC	a
nadvládou	nadvláda	k1gFnSc7	nadvláda
nad	nad	k7c7	nad
ostatními	ostatní	k2eAgInPc7d1	ostatní
mravenčími	mravenčí	k2eAgInPc7d1	mravenčí
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
bitvu	bitva	k1gFnSc4	bitva
se	s	k7c7	s
žlutými	žlutý	k2eAgMnPc7d1	žlutý
mravenci	mravenec	k1gMnPc7	mravenec
–	–	k?	–
jediným	jediný	k2eAgInSc7d1	jediný
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
sami	sám	k3xTgMnPc1	sám
ještě	ještě	k6eAd1	ještě
nepodmanili	podmanit	k5eNaPmAgMnP	podmanit
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gNnSc4	on
nezničili	zničit	k5eNaPmAgMnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nakonec	nakonec	k6eAd1	nakonec
válku	válka	k1gFnSc4	válka
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
žlutých	žlutý	k2eAgMnPc2d1	žlutý
vítězných	vítězný	k2eAgMnPc2d1	vítězný
mravenců	mravenec	k1gMnPc2	mravenec
se	se	k3xPyFc4	se
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
za	za	k7c7	za
"	"	kIx"	"
<g/>
vladaře	vladař	k1gMnSc2	vladař
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
modlí	modlit	k5eAaImIp3nP	modlit
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
národní	národní	k2eAgFnSc6d1	národní
cti	čest	k1gFnSc6	čest
<g/>
,	,	kIx,	,
o	o	k7c6	o
obchodních	obchodní	k2eAgInPc6d1	obchodní
zájmech	zájem	k1gInPc6	zájem
a	a	k8xC	a
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
ho	on	k3xPp3gInSc4	on
tulák	tulák	k1gMnSc1	tulák
s	s	k7c7	s
nenávistnými	návistný	k2eNgNnPc7d1	nenávistné
slovy	slovo	k1gNnPc7	slovo
zašlápne	zašlápnout	k5eAaPmIp3nS	zašlápnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Epilog	epilog	k1gInSc1	epilog
===	===	k?	===
</s>
</p>
<p>
<s>
Tulák	tulák	k1gMnSc1	tulák
se	se	k3xPyFc4	se
probouzí	probouzet	k5eAaImIp3nS	probouzet
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
.	.	kIx.	.
</s>
<s>
Najednou	najednou	k6eAd1	najednou
se	se	k3xPyFc4	se
rozsvítí	rozsvítit	k5eAaPmIp3nS	rozsvítit
a	a	k8xC	a
tulák	tulák	k1gMnSc1	tulák
vidí	vidět	k5eAaImIp3nS	vidět
rej	rej	k1gFnSc4	rej
jepic	jepice	k1gFnPc2	jepice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
radují	radovat	k5eAaImIp3nP	radovat
ze	z	k7c2	z
života	život	k1gInSc2	život
a	a	k8xC	a
při	při	k7c6	při
tanci	tanec	k1gInSc6	tanec
hynou	hynout	k5eAaImIp3nP	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Kukla	Kukla	k1gMnSc1	Kukla
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
dějství	dějství	k1gNnSc2	dějství
se	se	k3xPyFc4	se
také	také	k9	také
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c6	v
jepici	jepice	k1gFnSc6	jepice
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
zahyne	zahynout	k5eAaPmIp3nS	zahynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
umírá	umírat	k5eAaImIp3nS	umírat
i	i	k9	i
tulák	tulák	k1gMnSc1	tulák
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
přihlížejí	přihlížet	k5eAaImIp3nP	přihlížet
dva	dva	k4xCgMnPc1	dva
neteční	netečný	k2eAgMnPc1d1	netečný
slimáci	slimák	k1gMnPc1	slimák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
lesa	les	k1gInSc2	les
přichází	přicházet	k5eAaImIp3nS	přicházet
dřevorubec	dřevorubec	k1gMnSc1	dřevorubec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Drama	drama	k1gNnSc1	drama
nabízí	nabízet	k5eAaImIp3nS	nabízet
i	i	k9	i
alternativní	alternativní	k2eAgInSc4d1	alternativní
konec	konec	k1gInSc4	konec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
tulák	tulák	k1gMnSc1	tulák
nezemře	zemřít	k5eNaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
slimáků	slimák	k1gMnPc2	slimák
se	se	k3xPyFc4	se
probouzí	probouzet	k5eAaImIp3nS	probouzet
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vše	všechen	k3xTgNnSc1	všechen
jen	jen	k9	jen
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
dřevorubci	dřevorubec	k1gMnPc1	dřevorubec
<g/>
,	,	kIx,	,
nabídnou	nabídnout	k5eAaPmIp3nP	nabídnout
mu	on	k3xPp3gMnSc3	on
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
tulák	tulák	k1gMnSc1	tulák
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
začleňuje	začleňovat	k5eAaImIp3nS	začleňovat
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Překlady	překlad	k1gInPc4	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
:	:	kIx,	:
Paul	Paul	k1gMnSc1	Paul
Selver	Selver	k1gMnSc1	Selver
</s>
</p>
<p>
<s>
slovenština	slovenština	k1gFnSc1	slovenština
<g/>
:	:	kIx,	:
Milo	milo	k6eAd1	milo
Urban	Urban	k1gMnSc1	Urban
</s>
</p>
<p>
<s>
==	==	k?	==
Adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
Opera	opera	k1gFnSc1	opera
Zo	Zo	k1gFnSc2	Zo
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
Ján	Ján	k1gMnSc1	Ján
Cikker	Cikker	k1gMnSc1	Cikker
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
libreto	libreto	k1gNnSc4	libreto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
Opera	opera	k1gFnSc1	opera
Hyönteiselämää	Hyönteiselämää	k1gMnSc1	Hyönteiselämää
(	(	kIx(	(
<g/>
Kalevi	Kaleev	k1gFnSc6	Kaleev
Aho	Aho	k1gFnPc2	Aho
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
libreto	libreto	k1gNnSc4	libreto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Měnivá	měnivý	k2eAgFnSc1d1	měnivá
tvář	tvář	k1gFnSc1	tvář
divadla	divadlo	k1gNnSc2	divadlo
aneb	aneb	k?	aneb
Dvě	dva	k4xCgNnPc1	dva
století	století	k1gNnPc2	století
s	s	k7c7	s
pražskými	pražský	k2eAgMnPc7d1	pražský
herci	herec	k1gMnPc7	herec
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
143	[number]	k4	143
<g/>
,	,	kIx,	,
146	[number]	k4	146
<g/>
,	,	kIx,	,
148	[number]	k4	148
<g/>
,	,	kIx,	,
168	[number]	k4	168
<g/>
,	,	kIx,	,
228	[number]	k4	228
<g/>
,	,	kIx,	,
235	[number]	k4	235
<g/>
,	,	kIx,	,
260	[number]	k4	260
<g/>
,	,	kIx,	,
261	[number]	k4	261
<g/>
,	,	kIx,	,
269	[number]	k4	269
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
<g/>
/	/	kIx~	/
<g/>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
34	[number]	k4	34
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
71	[number]	k4	71
<g/>
,	,	kIx,	,
73	[number]	k4	73
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
124	[number]	k4	124
<g/>
,	,	kIx,	,
127	[number]	k4	127
<g/>
,	,	kIx,	,
150	[number]	k4	150
<g/>
,	,	kIx,	,
154	[number]	k4	154
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
157	[number]	k4	157
<g/>
,	,	kIx,	,
167	[number]	k4	167
<g/>
,	,	kIx,	,
169	[number]	k4	169
<g/>
,	,	kIx,	,
171	[number]	k4	171
<g/>
,	,	kIx,	,
181	[number]	k4	181
<g/>
,	,	kIx,	,
431	[number]	k4	431
<g/>
,	,	kIx,	,
605	[number]	k4	605
<g/>
,	,	kIx,	,
610	[number]	k4	610
<g/>
,	,	kIx,	,
641	[number]	k4	641
<g/>
,	,	kIx,	,
648	[number]	k4	648
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
www.capek.misto.cz	www.capek.misto.cz	k1gMnSc1	www.capek.misto.cz
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
</s>
</p>
