<s desamb="1">
Intersekcionální	Intersekcionální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
tvrzením	tvrzení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
diskriminováni	diskriminován	k2eAgMnPc1d1
na	na	k7c6
základě	základ	k1gInSc6
několika	několik	k4yIc2
sociálních	sociální	k2eAgMnPc2d1
a	a	k8xC
kulturních	kulturní	k2eAgMnPc2d1
faktorů	faktor	k1gInPc2
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>