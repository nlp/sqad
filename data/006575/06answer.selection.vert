<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
sudý	sudý	k2eAgInSc4d1	sudý
počet	počet	k1gInSc4	počet
uhlíkových	uhlíkový	k2eAgInPc2d1	uhlíkový
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
biosyntéza	biosyntéza	k1gFnSc1	biosyntéza
probíhá	probíhat	k5eAaImIp3nS	probíhat
adicí	adice	k1gFnSc7	adice
acetátu	acetát	k1gInSc2	acetát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
uhlíky	uhlík	k1gInPc4	uhlík
<g/>
.	.	kIx.	.
</s>
