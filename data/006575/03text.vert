<s>
Jako	jako	k8xC	jako
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
se	se	k3xPyFc4	se
v	v	k7c6	v
biochemii	biochemie	k1gFnSc6	biochemie
označují	označovat	k5eAaImIp3nP	označovat
vyšší	vysoký	k2eAgFnPc1d2	vyšší
monokarboxylové	monokarboxylový	k2eAgFnPc1d1	monokarboxylový
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
lze	lze	k6eAd1	lze
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kritérií	kritérion	k1gNnPc2	kritérion
–	–	k?	–
například	například	k6eAd1	například
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
řetězce	řetězec	k1gInSc2	řetězec
nebo	nebo	k8xC	nebo
nasycení	nasycení	k1gNnSc2	nasycení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
je	být	k5eAaImIp3nS	být
izoloval	izolovat	k5eAaBmAgMnS	izolovat
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
M.	M.	kA	M.
E.	E.	kA	E.
Chevreul	Chevreula	k1gFnPc2	Chevreula
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
některými	některý	k3yIgInPc7	některý
alkoholy	alkohol	k1gInPc7	alkohol
tvoří	tvořit	k5eAaImIp3nS	tvořit
estery	ester	k1gInPc1	ester
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
tuky	tuk	k1gInPc1	tuk
(	(	kIx(	(
<g/>
estery	ester	k1gInPc1	ester
s	s	k7c7	s
glycerolem	glycerol	k1gInSc7	glycerol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Esterifikací	esterifikace	k1gFnSc7	esterifikace
s	s	k7c7	s
cetylalkoholem	cetylalkohol	k1gInSc7	cetylalkohol
<g/>
,	,	kIx,	,
cerylalkoholem	cerylalkohol	k1gInSc7	cerylalkohol
a	a	k8xC	a
myristylalkoholem	myristylalkohol	k1gInSc7	myristylalkohol
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
také	také	k9	také
dalšími	další	k2eAgInPc7d1	další
mastnými	mastný	k2eAgInPc7d1	mastný
alkoholy	alkohol	k1gInPc7	alkohol
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
vosky	vosk	k1gInPc4	vosk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
sudý	sudý	k2eAgInSc4d1	sudý
počet	počet	k1gInSc4	počet
uhlíkových	uhlíkový	k2eAgInPc2d1	uhlíkový
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
biosyntéza	biosyntéza	k1gFnSc1	biosyntéza
probíhá	probíhat	k5eAaImIp3nS	probíhat
adicí	adice	k1gFnSc7	adice
acetátu	acetát	k1gInSc2	acetát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
uhlíky	uhlík	k1gInPc4	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
hydrolýzou	hydrolýza	k1gFnSc7	hydrolýza
esterových	esterový	k2eAgFnPc2d1	esterová
vazeb	vazba	k1gFnPc2	vazba
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
mnoha	mnoho	k4c2	mnoho
biologicky	biologicky	k6eAd1	biologicky
důležitých	důležitý	k2eAgFnPc2d1	důležitá
látek	látka	k1gFnPc2	látka
<g/>
:	:	kIx,	:
V	v	k7c6	v
organismech	organismus	k1gInPc6	organismus
mají	mít	k5eAaImIp3nP	mít
strukturní	strukturní	k2eAgFnSc4d1	strukturní
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
buněčné	buněčný	k2eAgFnSc6d1	buněčná
membráně	membrána	k1gFnSc6	membrána
(	(	kIx(	(
<g/>
fosfolipidy	fosfolipid	k1gInPc4	fosfolipid
membrán	membrána	k1gFnPc2	membrána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Glykolipidy	Glykolipid	k1gInPc1	Glykolipid
mají	mít	k5eAaImIp3nP	mít
důležitou	důležitý	k2eAgFnSc4d1	důležitá
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
nervové	nervový	k2eAgFnSc6d1	nervová
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Lipidy	lipid	k1gInPc1	lipid
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
zásobní	zásobní	k2eAgFnSc4d1	zásobní
funkci	funkce	k1gFnSc4	funkce
<g/>
:	:	kIx,	:
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
živin	živina	k1gFnPc2	živina
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nejvydatnější	vydatný	k2eAgInPc1d3	nejvydatnější
zdroje	zdroj	k1gInPc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Eikosanoidy	Eikosanoid	k1gInPc1	Eikosanoid
<g/>
,	,	kIx,	,
deriváty	derivát	k1gInPc1	derivát
eikosa-polyenových	eikosaolyenová	k1gFnPc2	eikosa-polyenová
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
látky	látka	k1gFnPc1	látka
jako	jako	k9	jako
prostaglandiny	prostaglandin	k2eAgFnPc1d1	prostaglandin
<g/>
,	,	kIx,	,
leukotrieny	leukotriena	k1gFnPc1	leukotriena
nebo	nebo	k8xC	nebo
thromboxany	thromboxana	k1gFnPc1	thromboxana
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vnitrobuněčné	vnitrobuněčný	k2eAgFnPc1d1	vnitrobuněčná
signalizační	signalizační	k2eAgFnPc1d1	signalizační
molekuly	molekula	k1gFnPc1	molekula
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
autokrinní	autokrinný	k2eAgMnPc1d1	autokrinný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
svalový	svalový	k2eAgInSc4d1	svalový
stah	stah	k1gInSc4	stah
<g/>
,	,	kIx,	,
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc4	bolest
či	či	k8xC	či
například	například	k6eAd1	například
zánět	zánět	k1gInSc4	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Syntéza	syntéza	k1gFnSc1	syntéza
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Syntéza	syntéza	k1gFnSc1	syntéza
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
prodlužováním	prodlužování	k1gNnPc3	prodlužování
acetylkoenzymu	acetylkoenzymum	k1gNnSc3	acetylkoenzymum
A.	A.	kA	A.
Proces	proces	k1gInSc4	proces
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
podobný	podobný	k2eAgInSc1d1	podobný
beta-oxidaci	betaxidace	k1gFnSc3	beta-oxidace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
enzymatický	enzymatický	k2eAgInSc1d1	enzymatický
aparát	aparát	k1gInSc1	aparát
<g/>
,	,	kIx,	,
buněčná	buněčný	k2eAgFnSc1d1	buněčná
lokalizace	lokalizace	k1gFnSc1	lokalizace
i	i	k8xC	i
detaily	detail	k1gInPc1	detail
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
reakcí	reakce	k1gFnPc2	reakce
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
řetězcem	řetězec	k1gInSc7	řetězec
(	(	kIx(	(
<g/>
SCFA	SCFA	kA	SCFA
<g/>
)	)	kIx)	)
–	–	k?	–
méně	málo	k6eAd2	málo
než	než	k8xS	než
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
se	s	k7c7	s
středně	středně	k6eAd1	středně
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
řetězcem	řetězec	k1gInSc7	řetězec
(	(	kIx(	(
<g/>
MCFA	MCFA	kA	MCFA
<g/>
)	)	kIx)	)
–	–	k?	–
6	[number]	k4	6
až	až	k9	až
12	[number]	k4	12
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
řetězcem	řetězec	k1gInSc7	řetězec
(	(	kIx(	(
<g/>
LCFA	LCFA	kA	LCFA
<g/>
)	)	kIx)	)
–	–	k?	–
14	[number]	k4	14
až	až	k9	až
20	[number]	k4	20
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
řetězcem	řetězec	k1gInSc7	řetězec
(	(	kIx(	(
<g/>
VLCFA	VLCFA	kA	VLCFA
<g/>
)	)	kIx)	)
–	–	k?	–
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
atomů	atom	k1gInPc2	atom
uhlíku	uhlík	k1gInSc2	uhlík
Nasycené	nasycený	k2eAgFnSc2d1	nasycená
mastné	mastný	k2eAgFnSc2d1	mastná
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
SAturated	SAturated	k1gInSc1	SAturated
Fatty	Fatta	k1gFnSc2	Fatta
Acids	Acidsa	k1gFnPc2	Acidsa
<g/>
)	)	kIx)	)
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
v	v	k7c6	v
řetězci	řetězec	k1gInSc6	řetězec
žádnou	žádný	k3yNgFnSc4	žádný
dvojnou	dvojný	k2eAgFnSc4d1	dvojná
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
přímé	přímý	k2eAgInPc4d1	přímý
řetězce	řetězec	k1gInPc4	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
živočišných	živočišný	k2eAgInPc6d1	živočišný
tucích	tuk	k1gInPc6	tuk
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
nasycených	nasycený	k2eAgFnPc2d1	nasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
jako	jako	k8xS	jako
energetická	energetický	k2eAgFnSc1d1	energetická
rezerva	rezerva	k1gFnSc1	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
palmovém	palmový	k2eAgInSc6d1	palmový
oleji	olej	k1gInSc6	olej
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
nasycených	nasycený	k2eAgFnPc2d1	nasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
kyselina	kyselina	k1gFnSc1	kyselina
kaprylová	kaprylový	k2eAgFnSc1d1	kaprylová
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
kaprinová	kaprinová	k1gFnSc1	kaprinová
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
8	[number]	k4	8
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
laurová	laurový	k2eAgFnSc1d1	Laurová
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
10	[number]	k4	10
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
myristová	myristová	k1gFnSc1	myristová
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
12	[number]	k4	12
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
palmitová	palmitový	k2eAgFnSc1d1	palmitová
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
14	[number]	k4	14
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
stearová	stearový	k2eAgFnSc1d1	stearová
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
16	[number]	k4	16
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
arachidová	arachidový	k2eAgFnSc1d1	arachidový
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
18	[number]	k4	18
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
lignocerová	lignocerová	k1gFnSc1	lignocerová
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
22	[number]	k4	22
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
Nasycené	nasycený	k2eAgFnPc1d1	nasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
jater	játra	k1gNnPc2	játra
na	na	k7c6	na
periferii	periferie	k1gFnSc6	periferie
distribuovány	distribuovat	k5eAaBmNgFnP	distribuovat
navázány	navázán	k2eAgFnPc1d1	navázána
v	v	k7c6	v
lipoproteinech	lipoproteino	k1gNnPc6	lipoproteino
VLDL	VLDL	kA	VLDL
(	(	kIx(	(
<g/>
very	ver	k2eAgInPc1d1	ver
low	low	k?	low
density	densit	k1gInPc1	densit
lipoprotein	lipoprotein	k1gInSc1	lipoprotein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
malá	malý	k2eAgFnSc1d1	malá
částice	částice	k1gFnSc1	částice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
nasycené	nasycený	k2eAgNnSc1d1	nasycené
mastné	mastné	k1gNnSc1	mastné
kyseliny	kyselina	k1gFnSc2	kyselina
prokazatelně	prokazatelně	k6eAd1	prokazatelně
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
pokládat	pokládat	k5eAaImF	pokládat
je	on	k3xPp3gInPc4	on
za	za	k7c4	za
atherogenní	atherogenní	k2eAgNnSc4d1	atherogenní
<g/>
.	.	kIx.	.
</s>
<s>
Mononenasycené	Mononenasycený	k2eAgFnPc1d1	Mononenasycený
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
(	(	kIx(	(
<g/>
Mono	mono	k2eAgInSc1d1	mono
Unsaturated	Unsaturated	k1gInSc1	Unsaturated
Fatty	Fatta	k1gFnSc2	Fatta
Acids	Acidsa	k1gFnPc2	Acidsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monoenové	monoenový	k2eAgFnPc1d1	monoenový
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
řetězci	řetězec	k1gInSc6	řetězec
jednu	jeden	k4xCgFnSc4	jeden
dvojnou	dvojný	k2eAgFnSc4d1	dvojná
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
mononenasycených	mononenasycený	k2eAgFnPc2d1	mononenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
kyselina	kyselina	k1gFnSc1	kyselina
palmitoolejová	palmitoolejový	k2eAgFnSc1d1	palmitoolejový
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
CH	Ch	kA	Ch
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
7	[number]	k4	7
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
olejová	olejový	k2eAgFnSc1d1	olejová
–	–	k?	–
cis	cis	k1gNnPc2	cis
izomer	izomer	k1gInSc1	izomer
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
<g />
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
7	[number]	k4	7
<g/>
CH	Ch	kA	Ch
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
7	[number]	k4	7
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
elaidová	elaidový	k2eAgFnSc1d1	elaidový
–	–	k?	–
trans	trans	k1gInSc1	trans
izomer	izomer	k1gInSc1	izomer
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
<g/>
CH	Ch	kA	Ch
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
7	[number]	k4	7
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
eruková	eruková	k1gFnSc1	eruková
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
7	[number]	k4	7
<g/>
CH	Ch	kA	Ch
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
11	[number]	k4	11
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
nervonová	nervonová	k1gFnSc1	nervonová
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
7	[number]	k4	7
<g/>
CH	Ch	kA	Ch
<g/>
=	=	kIx~	=
<g/>
CH	Ch	kA	Ch
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
13	[number]	k4	13
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
Polynenasycené	Polynenasycený	k2eAgFnPc1d1	Polynenasycený
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
(	(	kIx(	(
<g/>
PolyUnsaturated	PolyUnsaturated	k1gInSc1	PolyUnsaturated
Fatty	Fatta	k1gFnSc2	Fatta
Acids	Acidsa	k1gFnPc2	Acidsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polyenové	polyenový	k2eAgFnPc1d1	polyenový
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
řetězci	řetězec	k1gInSc6	řetězec
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednu	jeden	k4xCgFnSc4	jeden
dvojnou	dvojný	k2eAgFnSc4d1	dvojná
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
i	i	k9	i
tzv.	tzv.	kA	tzv.
esenciální	esenciální	k2eAgNnSc1d1	esenciální
mastné	mastné	k1gNnSc1	mastné
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
přijímat	přijímat	k5eAaImF	přijímat
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Esenciální	esenciální	k2eAgFnPc1d1	esenciální
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
jsou	být	k5eAaImIp3nP	být
nutným	nutný	k2eAgInSc7d1	nutný
substrátem	substrát	k1gInSc7	substrát
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
prostaglandinů	prostaglandin	k1gInPc2	prostaglandin
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
biologicky	biologicky	k6eAd1	biologicky
aktivních	aktivní	k2eAgFnPc2d1	aktivní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
polynenasycených	polynenasycený	k2eAgFnPc2d1	polynenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
kyselina	kyselina	k1gFnSc1	kyselina
linolová	linolový	k2eAgFnSc1d1	linolová
–	–	k?	–
esenciální	esenciální	k2eAgFnSc1d1	esenciální
kyselina	kyselina	k1gFnSc1	kyselina
γ	γ	k?	γ
kyselina	kyselina	k1gFnSc1	kyselina
α	α	k?	α
–	–	k?	–
esenciální	esenciální	k2eAgFnSc1d1	esenciální
kyselina	kyselina	k1gFnSc1	kyselina
arachidonová	arachidonová	k1gFnSc1	arachidonová
–	–	k?	–
esenciální	esenciální	k2eAgFnPc4d1	esenciální
pro	pro	k7c4	pro
kočkovité	kočkovitý	k2eAgFnPc4d1	kočkovitá
šelmy	šelma	k1gFnPc4	šelma
kyselina	kyselina	k1gFnSc1	kyselina
klupadonová	klupadonová	k1gFnSc1	klupadonová
kyselina	kyselina	k1gFnSc1	kyselina
eikosapentaenová	eikosapentaenová	k1gFnSc1	eikosapentaenová
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
EPA	EPA	kA	EPA
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
dokosahexaenová	dokosahexaenová	k1gFnSc1	dokosahexaenová
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
DHA	DHA	kA	DHA
<g/>
)	)	kIx)	)
Některé	některý	k3yIgFnPc1	některý
polyenové	polyenová	k1gFnPc1	polyenová
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
snižují	snižovat	k5eAaImIp3nP	snižovat
množství	množství	k1gNnSc4	množství
LDL	LDL	kA	LDL
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
tak	tak	k6eAd1	tak
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
snižovat	snižovat	k5eAaImF	snižovat
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
(	(	kIx(	(
<g/>
hlavě	hlava	k1gFnSc6	hlava
omega-	omega-	k?	omega-
<g/>
3	[number]	k4	3
nenasycené	nasycený	k2eNgFnPc1d1	nenasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tuky	tuk	k1gInPc1	tuk
<g/>
#	#	kIx~	#
<g/>
Transmastné	Transmastný	k2eAgFnPc1d1	Transmastný
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
přírodních	přírodní	k2eAgFnPc2d1	přírodní
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
konfiguraci	konfigurace	k1gFnSc6	konfigurace
cis	cis	k1gNnSc1	cis
<g/>
.	.	kIx.	.
</s>
<s>
Trans	trans	k1gInSc1	trans
izomery	izomer	k1gInPc1	izomer
(	(	kIx(	(
<g/>
transmastné	transmastný	k2eAgFnPc1d1	transmastný
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
TFA	TFA	kA	TFA
nebo	nebo	k8xC	nebo
TRANS	trans	k1gInSc1	trans
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
ztužených	ztužený	k2eAgInPc6d1	ztužený
tucích	tuk	k1gInPc6	tuk
a	a	k8xC	a
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
v	v	k7c6	v
tuku	tuk	k1gInSc6	tuk
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
.	.	kIx.	.
</s>
<s>
Cis-mononenasycené	Cisononenasycený	k2eAgFnSc2d1	Cis-mononenasycený
kyseliny	kyselina	k1gFnSc2	kyselina
zrychlují	zrychlovat	k5eAaImIp3nP	zrychlovat
odbouráváni	odbouráván	k2eAgMnPc1d1	odbouráván
lipoproteinů	lipoprotein	k1gMnPc2	lipoprotein
LDL	LDL	kA	LDL
(	(	kIx(	(
<g/>
low	low	k?	low
density	densit	k1gInPc1	densit
lipoprotein	lipoprotein	k1gInSc1	lipoprotein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snižují	snižovat	k5eAaImIp3nP	snižovat
tak	tak	k9	tak
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
transmastné	transmastný	k2eAgFnPc1d1	transmastný
kyseliny	kyselina	k1gFnPc1	kyselina
prokazatelně	prokazatelně	k6eAd1	prokazatelně
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
a	a	k8xC	a
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
tak	tak	k9	tak
riziko	riziko	k1gNnSc4	riziko
aterosklerózy	ateroskleróza	k1gFnSc2	ateroskleróza
<g/>
.	.	kIx.	.
</s>
<s>
Nasycené	nasycený	k2eAgFnPc1d1	nasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získaly	získat	k5eAaPmAgFnP	získat
neprávem	neprávo	k1gNnSc7	neprávo
status	status	k1gInSc4	status
škodlivých	škodlivý	k2eAgMnPc2d1	škodlivý
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jejich	jejich	k3xOp3gFnSc1	jejich
častá	častý	k2eAgFnSc1d1	častá
nebo	nebo	k8xC	nebo
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
konzumace	konzumace	k1gFnSc1	konzumace
není	být	k5eNaImIp3nS	být
dietology	dietolog	k1gMnPc4	dietolog
doporučována	doporučován	k2eAgFnSc1d1	doporučována
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhy	druh	k1gInPc1	druh
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
tukové	tukový	k2eAgFnSc6d1	tuková
tkáni	tkáň	k1gFnSc6	tkáň
časteji	častej	k1gInSc6	častej
ukládají	ukládat	k5eAaImIp3nP	ukládat
nasycené	nasycený	k2eAgFnPc1d1	nasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mastná	mastný	k2eAgFnSc1d1	mastná
kyselina	kyselina	k1gFnSc1	kyselina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
mastná	mastný	k2eAgFnSc1d1	mastná
kyselina	kyselina	k1gFnSc1	kyselina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Film	film	k1gInSc1	film
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
o	o	k7c6	o
mastných	mastný	k2eAgFnPc6d1	mastná
kyselinách	kyselina	k1gFnPc6	kyselina
</s>
