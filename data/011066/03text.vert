<p>
<s>
Rčení	rčení	k1gNnSc1	rčení
je	být	k5eAaImIp3nS	být
útvar	útvar	k1gInSc4	útvar
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
krátká	krátký	k2eAgFnSc1d1	krátká
průpovídka	průpovídka	k1gFnSc1	průpovídka
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
určitou	určitý	k2eAgFnSc4d1	určitá
životní	životní	k2eAgFnSc4d1	životní
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Nechá	nechat	k5eAaPmIp3nS	nechat
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
krejcar	krejcar	k1gInSc4	krejcar
koleno	koleno	k1gNnSc4	koleno
vrtat	vrtat	k5eAaImF	vrtat
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
má	mít	k5eAaImIp3nS	mít
obě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
levé	levá	k1gFnSc2	levá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Rčení	rčení	k1gNnSc1	rčení
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
úslovím	úsloví	k1gNnSc7	úsloví
a	a	k8xC	a
pořekadlem	pořekadlo	k1gNnSc7	pořekadlo
<g/>
.	.	kIx.	.
</s>
<s>
Rčení	rčení	k1gNnSc1	rčení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
pořekadla	pořekadlo	k1gNnSc2	pořekadlo
především	především	k6eAd1	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rčení	rčení	k1gNnSc1	rčení
lze	lze	k6eAd1	lze
volně	volně	k6eAd1	volně
časovat	časovat	k5eAaBmF	časovat
(	(	kIx(	(
<g/>
házet	házet	k5eAaImF	házet
perly	perl	k1gInPc4	perl
sviním	svinit	k5eAaImIp1nS	svinit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pořekadlo	pořekadlo	k1gNnSc1	pořekadlo
jen	jen	k9	jen
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
ne	ne	k9	ne
(	(	kIx(	(
<g/>
chyba	chyba	k1gFnSc1	chyba
lávky	lávka	k1gFnSc2	lávka
<g/>
;	;	kIx,	;
já	já	k3xPp1nSc1	já
nic	nic	k6eAd1	nic
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
muzikant	muzikant	k1gMnSc1	muzikant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úsloví	úsloví	k1gNnSc1	úsloví
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
přechodový	přechodový	k2eAgInSc1d1	přechodový
typ	typ	k1gInSc1	typ
mezi	mezi	k7c7	mezi
rčením	rčení	k1gNnSc7	rčení
a	a	k8xC	a
pořekadlem	pořekadlo	k1gNnSc7	pořekadlo
<g/>
.	.	kIx.	.
<g/>
Specifickým	specifický	k2eAgInSc7d1	specifický
typem	typ	k1gInSc7	typ
rčení	rčení	k1gNnSc2	rčení
je	být	k5eAaImIp3nS	být
přirovnání	přirovnání	k1gNnSc4	přirovnání
<g/>
.	.	kIx.	.
</s>
<s>
Přirovnání	přirovnání	k1gNnSc1	přirovnání
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
vlastnost	vlastnost	k1gFnSc4	vlastnost
subjektu	subjekt	k1gInSc2	subjekt
připodobněním	připodobnění	k1gNnSc7	připodobnění
k	k	k7c3	k
typické	typický	k2eAgFnSc3d1	typická
vlastnosti	vlastnost	k1gFnSc3	vlastnost
nějaké	nějaký	k3yIgFnSc2	nějaký
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
předmětu	předmět	k1gInSc2	předmět
apod.	apod.	kA	apod.
Nejčastěji	často	k6eAd3	často
bývá	bývat	k5eAaImIp3nS	bývat
tvořeno	tvořit	k5eAaImNgNnS	tvořit
pomocí	pomocí	k7c2	pomocí
částice	částice	k1gFnSc2	částice
"	"	kIx"	"
<g/>
jako	jako	k9	jako
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
jiných	jiný	k2eAgFnPc2d1	jiná
forem	forma	k1gFnPc2	forma
přirovnání	přirovnání	k1gNnSc2	přirovnání
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
za	za	k7c7	za
<g/>
"	"	kIx"	"
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
sílu	síla	k1gFnSc4	síla
za	za	k7c4	za
koně	kůň	k1gMnSc4	kůň
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
komparativ	komparativ	k1gInSc1	komparativ
+	+	kIx~	+
než	než	k8xS	než
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
hůře	zle	k6eAd2	zle
než	než	k8xS	než
pes	pes	k1gMnSc1	pes
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
bezpředložkově	bezpředložkově	k6eAd1	bezpředložkově
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
hůře	zle	k6eAd2	zle
psa	pes	k1gMnSc4	pes
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
nad	nad	k7c7	nad
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
přes	přes	k7c4	přes
<g/>
"	"	kIx"	"
–	–	k?	–
je	být	k5eAaImIp3nS	být
chytrý	chytrý	k2eAgInSc1d1	chytrý
nad	nad	k7c4	nad
lišku	liška	k1gFnSc4	liška
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
přímá	přímý	k2eAgFnSc1d1	přímá
metaforická	metaforický	k2eAgFnSc1d1	metaforická
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
baba	baba	k1gFnSc1	baba
<g/>
,	,	kIx,	,
tele	tele	k1gNnSc1	tele
<g/>
,	,	kIx,	,
křečekPodobným	křečekPodobný	k2eAgInSc7d1	křečekPodobný
útvarem	útvar	k1gInSc7	útvar
je	být	k5eAaImIp3nS	být
také	také	k9	také
přísloví	přísloví	k1gNnSc1	přísloví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
rčení	rčení	k1gNnSc2	rčení
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
ohledech	ohled	k1gInPc6	ohled
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
přísloví	přísloví	k1gNnSc1	přísloví
je	být	k5eAaImIp3nS	být
vyjádřením	vyjádření	k1gNnSc7	vyjádření
nějaké	nějaký	k3yIgFnSc2	nějaký
životní	životní	k2eAgFnSc2d1	životní
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
;	;	kIx,	;
rčení	rčení	k1gNnSc1	rčení
je	být	k5eAaImIp3nS	být
produktem	produkt	k1gInSc7	produkt
lidové	lidový	k2eAgFnSc2d1	lidová
fantazie	fantazie	k1gFnSc2	fantazie
</s>
</p>
<p>
<s>
smyslem	smysl	k1gInSc7	smysl
přísloví	přísloví	k1gNnSc2	přísloví
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
morální	morální	k2eAgNnSc4d1	morální
ponaučení	ponaučení	k1gNnSc4	ponaučení
<g/>
,	,	kIx,	,
výstraha	výstraha	k1gFnSc1	výstraha
či	či	k8xC	či
pokárání	pokárání	k1gNnSc1	pokárání
<g/>
;	;	kIx,	;
smyslem	smysl	k1gInSc7	smysl
rčení	rčení	k1gNnSc2	rčení
je	být	k5eAaImIp3nS	být
pobavení	pobavení	k1gNnSc4	pobavení
<g/>
,	,	kIx,	,
oživení	oživení	k1gNnSc4	oživení
jazykového	jazykový	k2eAgInSc2d1	jazykový
projevu	projev	k1gInSc2	projev
</s>
</p>
<p>
<s>
přísloví	přísloví	k1gNnSc1	přísloví
tvoří	tvořit	k5eAaImIp3nS	tvořit
celé	celý	k2eAgFnPc4d1	celá
formalizované	formalizovaný	k2eAgFnPc4d1	formalizovaná
věty	věta	k1gFnPc4	věta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
nelze	lze	k6eNd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
<g/>
,	,	kIx,	,
přeformulovat	přeformulovat	k5eAaPmF	přeformulovat
<g/>
,	,	kIx,	,
skloňovat	skloňovat	k5eAaImF	skloňovat
či	či	k8xC	či
časovat	časovat	k5eAaBmF	časovat
<g/>
;	;	kIx,	;
rčení	rčení	k1gNnPc1	rčení
a	a	k8xC	a
pořekadla	pořekadlo	k1gNnPc1	pořekadlo
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
částmi	část	k1gFnPc7	část
vět	věta	k1gFnPc2	věta
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
časovat	časovat	k5eAaImF	časovat
</s>
</p>
<p>
<s>
==	==	k?	==
Sbírka	sbírka	k1gFnSc1	sbírka
českých	český	k2eAgNnPc2d1	české
rčení	rčení	k1gNnPc2	rčení
==	==	k?	==
</s>
</p>
<p>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejobsáhlejší	obsáhlý	k2eAgFnSc7d3	nejobsáhlejší
sbírkou	sbírka	k1gFnSc7	sbírka
českých	český	k2eAgNnPc2d1	české
lidových	lidový	k2eAgNnPc2d1	lidové
rčení	rčení	k1gNnPc2	rčení
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Zaorálka	Zaorálka	k1gFnSc1	Zaorálka
Lidová	lidový	k2eAgFnSc1d1	lidová
rčení	rčení	k1gNnSc4	rčení
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pochopitelně	pochopitelně	k6eAd1	pochopitelně
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgNnPc4d1	vzniklé
rčení	rčení	k1gNnPc4	rčení
z	z	k7c2	z
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ZAORÁLEK	ZAORÁLEK	kA	ZAORÁLEK
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgNnPc1d1	lidové
rčení	rčení	k1gNnPc1	rčení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Aforismus	aforismus	k1gInSc1	aforismus
</s>
</p>
<p>
<s>
Anekdota	anekdota	k1gFnSc1	anekdota
</s>
</p>
<p>
<s>
Folklór	folklór	k1gInSc1	folklór
</s>
</p>
<p>
<s>
Frazeologizmus	frazeologizmus	k1gInSc1	frazeologizmus
</s>
</p>
<p>
<s>
Idiom	idiom	k1gInSc1	idiom
</s>
</p>
<p>
<s>
Přísloví	přísloví	k1gNnSc1	přísloví
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Česká	český	k2eAgFnSc1d1	Česká
rčení	rčení	k1gNnSc4	rčení
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
