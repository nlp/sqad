<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Ming	Minga	k1gFnPc2	Minga
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Ming	Ming	k1gMnSc1	Ming
čchao	čchao	k1gMnSc1	čchao
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Míng	Mínga	k1gFnPc2	Mínga
Cháo	Cháo	k6eAd1	Cháo
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
明	明	k?	明
<g/>
;	;	kIx,	;
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Zářivá	zářivý	k2eAgFnSc1d1	zářivá
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vládla	vládnout	k5eAaImAgFnS	vládnout
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
Ming	Minga	k1gFnPc2	Minga
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Číně	Čína	k1gFnSc3	Čína
v	v	k7c6	v
letech	let	k1gInPc6	let
1368	[number]	k4	1368
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
mingský	mingský	k2eAgInSc4d1	mingský
stát	stát	k1gInSc4	stát
udrželi	udržet	k5eAaPmAgMnP	udržet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1661	[number]	k4	1661
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
na	na	k7c4	na
stále	stále	k6eAd1	stále
se	s	k7c7	s
zmenšujícím	zmenšující	k2eAgMnSc7d1	zmenšující
se	se	k3xPyFc4	se
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
