<p>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Ming	Minga	k1gFnPc2	Minga
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Ming	Ming	k1gMnSc1	Ming
čchao	čchao	k1gMnSc1	čchao
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Míng	Mínga	k1gFnPc2	Mínga
Cháo	Cháo	k6eAd1	Cháo
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc1	znak
明	明	k?	明
<g/>
;	;	kIx,	;
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Zářivá	zářivý	k2eAgFnSc1d1	zářivá
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vládla	vládnout	k5eAaImAgFnS	vládnout
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
Ming	Minga	k1gFnPc2	Minga
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Číně	Čína	k1gFnSc3	Čína
v	v	k7c6	v
letech	let	k1gInPc6	let
1368	[number]	k4	1368
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
mingský	mingský	k2eAgInSc4d1	mingský
stát	stát	k1gInSc4	stát
udrželi	udržet	k5eAaPmAgMnP	udržet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1661	[number]	k4	1661
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
na	na	k7c4	na
stále	stále	k6eAd1	stále
se	s	k7c7	s
zmenšujícím	zmenšující	k2eAgMnSc7d1	zmenšující
se	se	k3xPyFc4	se
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc7d1	poslední
čínskou	čínský	k2eAgFnSc7d1	čínská
národní	národní	k2eAgFnSc7d1	národní
dynastií	dynastie	k1gFnSc7	dynastie
<g/>
,	,	kIx,	,
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgFnS	být
Čína	Čína	k1gFnSc1	Čína
dobyta	dobýt	k5eAaPmNgFnS	dobýt
mandžuskou	mandžuský	k2eAgFnSc7d1	mandžuská
říší	říš	k1gFnSc7	říš
Čching	Čching	k1gInSc1	Čching
(	(	kIx(	(
<g/>
1635	[number]	k4	1635
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
čínský	čínský	k2eAgInSc1d1	čínský
stát	stát	k1gInSc1	stát
došel	dojít	k5eAaPmAgInS	dojít
své	svůj	k3xOyFgFnPc4	svůj
obnovy	obnova	k1gFnPc4	obnova
až	až	k9	až
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Čchingů	Čching	k1gInPc2	Čching
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
když	když	k8xS	když
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
byl	být	k5eAaImAgMnS	být
Ču	Ču	k1gMnSc1	Ču
Jüan-čang	Jüan-čang	k1gMnSc1	Jüan-čang
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
mongolské	mongolský	k2eAgFnSc3d1	mongolská
dynastii	dynastie	k1gFnSc3	dynastie
Jüan	jüan	k1gInSc4	jüan
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
potomci	potomek	k1gMnPc1	potomek
pozvedli	pozvednout	k5eAaPmAgMnP	pozvednout
Čínu	Čína	k1gFnSc4	Čína
k	k	k7c3	k
dlouhodobé	dlouhodobý	k2eAgFnSc3d1	dlouhodobá
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
prosperitě	prosperita	k1gFnSc3	prosperita
a	a	k8xC	a
politické	politický	k2eAgFnSc3d1	politická
stabilitě	stabilita	k1gFnSc3	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
díky	díky	k7c3	díky
polygamii	polygamie	k1gFnSc3	polygamie
běžné	běžný	k2eAgFnPc4d1	běžná
u	u	k7c2	u
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
čínské	čínský	k2eAgFnSc2d1	čínská
společnosti	společnost	k1gFnSc2	společnost
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
mužských	mužský	k2eAgInPc2d1	mužský
členů	člen	k1gInPc2	člen
rodu	rod	k1gInSc2	rod
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
však	však	k9	však
byli	být	k5eAaImAgMnP	být
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
císařů	císař	k1gMnPc2	císař
a	a	k8xC	a
následníků	následník	k1gMnPc2	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
stability	stabilita	k1gFnSc2	stabilita
vlády	vláda	k1gFnSc2	vláda
vyloučeni	vyloučit	k5eAaPmNgMnP	vyloučit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
potíže	potíž	k1gFnSc2	potíž
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
plynoucí	plynoucí	k2eAgNnPc4d1	plynoucí
rolnická	rolnický	k2eAgNnPc4d1	rolnické
povstání	povstání	k1gNnPc4	povstání
přinesly	přinést	k5eAaPmAgFnP	přinést
oslabení	oslabení	k1gNnSc4	oslabení
mingské	mingská	k1gFnSc2	mingská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
využité	využitý	k2eAgFnSc2d1	využitá
roku	rok	k1gInSc2	rok
1644	[number]	k4	1644
Mandžuy	Mandžu	k1gMnPc4	Mandžu
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
dynastie	dynastie	k1gFnSc2	dynastie
a	a	k8xC	a
převzetí	převzetí	k1gNnSc2	převzetí
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
císařem	císař	k1gMnSc7	císař
dynastie	dynastie	k1gFnSc2	dynastie
byl	být	k5eAaImAgMnS	být
Ču	Ču	k1gMnSc1	Ču
Jüan-čang	Jüan-čang	k1gMnSc1	Jüan-čang
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1328	[number]	k4	1328
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
chudého	chudý	k2eAgMnSc2d1	chudý
čínského	čínský	k2eAgMnSc2d1	čínský
rolníka	rolník	k1gMnSc2	rolník
žijícího	žijící	k2eAgMnSc2d1	žijící
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Čung	Čung	k1gMnSc1	Čung
<g/>
-li	i	k?	-li
(	(	kIx(	(
<g/>
鍾	鍾	k?	鍾
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Feng-jang	Fengang	k1gInSc1	Feng-jang
<g/>
)	)	kIx)	)
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
An-chuej	Anhuej	k1gInSc4	An-chuej
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Číně	Čína	k1gFnSc6	Čína
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
roku	rok	k1gInSc2	rok
1344	[number]	k4	1344
při	při	k7c6	při
epidemii	epidemie	k1gFnSc6	epidemie
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
střídavě	střídavě	k6eAd1	střídavě
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
buddhistickém	buddhistický	k2eAgInSc6d1	buddhistický
klášteře	klášter	k1gInSc6	klášter
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
žebrotou	žebrota	k1gFnSc7	žebrota
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
bojovníkům	bojovník	k1gMnPc3	bojovník
povstání	povstání	k1gNnSc2	povstání
rudých	rudý	k2eAgInPc2d1	rudý
turbanů	turban	k1gInPc2	turban
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
čelné	čelný	k2eAgNnSc4d1	čelné
postavení	postavení	k1gNnSc4	postavení
mezi	mezi	k7c7	mezi
povstalci	povstalec	k1gMnPc7	povstalec
<g/>
,	,	kIx,	,
porazil	porazit	k5eAaPmAgMnS	porazit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
povstalecké	povstalecký	k2eAgMnPc4d1	povstalecký
vůdce	vůdce	k1gMnPc4	vůdce
i	i	k8xC	i
mongolská	mongolský	k2eAgNnPc1d1	mongolské
vojska	vojsko	k1gNnPc1	vojsko
říše	říš	k1gFnSc2	říš
Jüan	jüan	k1gInSc4	jüan
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1368	[number]	k4	1368
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
císařem	císař	k1gMnSc7	císař
čínské	čínský	k2eAgFnSc2d1	čínská
říše	říš	k1gFnSc2	říš
Ming	Minga	k1gFnPc2	Minga
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1381	[number]	k4	1381
dobyl	dobýt	k5eAaPmAgInS	dobýt
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Říši	říše	k1gFnSc4	říše
a	a	k8xC	a
dynastii	dynastie	k1gFnSc4	dynastie
Ming	Minga	k1gFnPc2	Minga
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Ču	Ču	k1gMnSc1	Ču
Jüan-čang	Jüan-čang	k1gMnSc1	Jüan-čang
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
císař	císař	k1gMnSc1	císař
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
své	svůj	k3xOyFgFnSc2	svůj
éry	éra	k1gFnSc2	éra
vlády	vláda	k1gFnSc2	vláda
–	–	k?	–
Chung-wu	Chungus	k1gInSc2	Chung-wus
<g/>
)	)	kIx)	)
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Nankingu	Nanking	k1gInSc2	Nanking
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1368	[number]	k4	1368
<g/>
.	.	kIx.	.
</s>
<s>
Ču	Ču	k?	Ču
byl	být	k5eAaImAgMnS	být
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1361	[number]	k4	1361
vůdcem	vůdce	k1gMnSc7	vůdce
Rudých	rudý	k2eAgInPc2d1	rudý
turbanů	turban	k1gInPc2	turban
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
říše	říš	k1gFnSc2	říš
Sung	Sung	k1gMnSc1	Sung
Chan	Chan	k1gMnSc1	Chan
Lin-erem	Linr	k1gMnSc7	Lin-er
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
vévodou	vévoda	k1gMnSc7	vévoda
z	z	k7c2	z
Wu	Wu	k1gFnSc1	Wu
(	(	kIx(	(
<g/>
吳	吳	k?	吳
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Wu	Wu	k1gMnSc1	Wu
byl	být	k5eAaImAgMnS	být
název	název	k1gInSc4	název
starověkého	starověký	k2eAgInSc2d1	starověký
státu	stát	k1gInSc2	stát
Wu	Wu	k1gFnSc2	Wu
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
regionu	region	k1gInSc2	region
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
Jang-c	Jang	k1gFnSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1364	[number]	k4	1364
se	se	k3xPyFc4	se
Ču	Ču	k1gMnSc1	Ču
Jüan-čang	Jüan-čang	k1gMnSc1	Jüan-čang
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
krále	král	k1gMnSc4	král
Wu	Wu	k1gMnSc4	Wu
(	(	kIx(	(
<g/>
吳	吳	k?	吳
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
nejmenoval	jmenovat	k5eNaImAgMnS	jmenovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
<g/>
,	,	kIx,	,
císařem	císař	k1gMnSc7	císař
Wu	Wu	k1gMnSc7	Wu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
stát	stát	k1gInSc4	stát
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
dynastii	dynastie	k1gFnSc4	dynastie
vybral	vybrat	k5eAaPmAgInS	vybrat
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Ming	Ming	k1gInSc1	Ming
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
明	明	k?	明
<g/>
)	)	kIx)	)
–	–	k?	–
Jasná	jasný	k2eAgFnSc1d1	jasná
či	či	k8xC	či
Zářivá	zářivý	k2eAgFnSc1d1	zářivá
<g/>
,	,	kIx,	,
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Ta	ten	k3xDgFnSc1	ten
Ming	Ming	k1gInSc4	Ming
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
大	大	k?	大
<g/>
)	)	kIx)	)
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
záře	záře	k1gFnSc1	záře
<g/>
.	.	kIx.	.
<g/>
Název	název	k1gInSc1	název
Ming	Minga	k1gFnPc2	Minga
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Číně	Čína	k1gFnSc6	Čína
několikerý	několikerý	k4xRyIgInSc1	několikerý
politický	politický	k2eAgInSc1d1	politický
kontext	kontext	k1gInSc1	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Jas	jas	k1gInSc1	jas
a	a	k8xC	a
záře	zář	k1gFnPc1	zář
evokovaly	evokovat	k5eAaBmAgFnP	evokovat
oheň	oheň	k1gInSc4	oheň
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
rudou	rudý	k2eAgFnSc4d1	rudá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc4	prvek
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
opozici	opozice	k1gFnSc4	opozice
vůči	vůči	k7c3	vůči
dynastii	dynastie	k1gFnSc3	dynastie
Jüan	jüan	k1gInSc4	jüan
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
totiž	totiž	k9	totiž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
systému	systém	k1gInSc6	systém
pěti	pět	k4xCc2	pět
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
souvstažností	souvstažnost	k1gFnPc2	souvstažnost
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobnosti	podobnost	k1gFnSc3	podobnost
slov	slovo	k1gNnPc2	slovo
jüan	jüan	k1gInSc1	jüan
a	a	k8xC	a
süan	süan	k1gInSc1	süan
(	(	kIx(	(
<g/>
temný	temný	k2eAgInSc1d1	temný
<g/>
)	)	kIx)	)
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
severem	sever	k1gInSc7	sever
<g/>
.	.	kIx.	.
</s>
<s>
Geografické	geografický	k2eAgNnSc1d1	geografické
položení	položení	k1gNnSc1	položení
Mongolů	mongol	k1gInPc2	mongol
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Číňanů	Číňan	k1gMnPc2	Číňan
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
asociaci	asociace	k1gFnSc4	asociace
posilovalo	posilovat	k5eAaImAgNnS	posilovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Chung-wu	Chungus	k1gInSc2	Chung-wus
částečně	částečně	k6eAd1	částečně
převzal	převzít	k5eAaPmAgInS	převzít
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
Velký	velký	k2eAgMnSc1d1	velký
král	král	k1gMnSc1	král
světla	světlo	k1gNnSc2	světlo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ta	ten	k3xDgFnSc1	ten
Ming	Minga	k1gFnPc2	Minga
wang	wang	k1gInSc1	wang
<g/>
,	,	kIx,	,
大	大	k?	大
<g/>
)	)	kIx)	)
Chan	Chan	k1gMnSc1	Chan
Šan-tchunga	Šanchung	k1gMnSc2	Šan-tchung
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc2	vůdce
sekty	sekta	k1gFnSc2	sekta
Bílého	bílý	k2eAgInSc2d1	bílý
lotosu	lotos	k1gInSc2	lotos
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
Ming	Ming	k1gInSc1	Ming
odkazoval	odkazovat	k5eAaImAgInS	odkazovat
na	na	k7c4	na
Ming-ťiao	Ming-ťiao	k1gNnSc4	Ming-ťiao
–	–	k?	–
"	"	kIx"	"
<g/>
náboženství	náboženství	k1gNnSc1	náboženství
světla	světlo	k1gNnSc2	světlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
manicheismus	manicheismus	k1gInSc1	manicheismus
<g/>
,	,	kIx,	,
přítomný	přítomný	k2eAgInSc1d1	přítomný
v	v	k7c6	v
ideologii	ideologie	k1gFnSc6	ideologie
Bílého	bílý	k2eAgInSc2d1	bílý
lotosu	lotos	k1gInSc2	lotos
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
zorganizovaného	zorganizovaný	k2eAgNnSc2d1	zorganizované
povstání	povstání	k1gNnSc2	povstání
Rudých	rudý	k2eAgInPc2d1	rudý
turbanů	turban	k1gInPc2	turban
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
se	se	k3xPyFc4	se
tak	tak	k9	tak
za	za	k7c4	za
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
víry	víra	k1gFnSc2	víra
povstalců	povstalec	k1gMnPc2	povstalec
v	v	k7c4	v
příchod	příchod	k1gInSc4	příchod
Ming-wanga	Mingang	k1gMnSc2	Ming-wang
<g/>
,	,	kIx,	,
Krále	Král	k1gMnSc2	Král
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
ospravedlnil	ospravedlnit	k5eAaPmAgMnS	ospravedlnit
odstranění	odstranění	k1gNnSc4	odstranění
Chan	Chan	k1gInSc1	Chan
Lin-era	Linro	k1gNnSc2	Lin-ero
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
třetí	třetí	k4xOgNnSc4	třetí
se	se	k3xPyFc4	se
ve	v	k7c6	v
výběru	výběr	k1gInSc6	výběr
jména	jméno	k1gNnSc2	jméno
dynastie	dynastie	k1gFnSc2	dynastie
projevilo	projevit	k5eAaPmAgNnS	projevit
císařovo	císařův	k2eAgNnSc1d1	císařovo
pragmatické	pragmatický	k2eAgNnSc1d1	pragmatické
přebírání	přebírání	k1gNnSc1	přebírání
mongolských	mongolský	k2eAgFnPc2d1	mongolská
vládních	vládní	k2eAgFnPc2d1	vládní
praktik	praktika	k1gFnPc2	praktika
–	–	k?	–
použití	použití	k1gNnSc4	použití
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
pojmu	pojem	k1gInSc2	pojem
jako	jako	k8xC	jako
jména	jméno	k1gNnSc2	jméno
dynastie	dynastie	k1gFnSc2	dynastie
byla	být	k5eAaImAgFnS	být
nečínská	čínský	k2eNgFnSc1d1	nečínská
tradice	tradice	k1gFnSc1	tradice
založená	založený	k2eAgFnSc1d1	založená
Džurčeny	Džurčen	k2eAgFnPc4d1	Džurčen
(	(	kIx(	(
<g/>
Dynastie	dynastie	k1gFnPc4	dynastie
Ťin	Ťin	k1gMnSc3	Ťin
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
přebraná	přebraný	k2eAgFnSc1d1	přebraná
Mongoly	Mongol	k1gMnPc7	Mongol
(	(	kIx(	(
<g/>
Dynastie	dynastie	k1gFnSc1	dynastie
Jüan	jüan	k1gInSc1	jüan
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Počátek	počátek	k1gInSc1	počátek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čína	Čína	k1gFnSc1	Čína
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
dynastie	dynastie	k1gFnSc2	dynastie
==	==	k?	==
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
nové	nový	k2eAgFnSc2d1	nová
dynastie	dynastie	k1gFnSc2	dynastie
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
napraveny	napraven	k2eAgFnPc4d1	napravena
škody	škoda	k1gFnPc4	škoda
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
v	v	k7c6	v
desetiletích	desetiletí	k1gNnPc6	desetiletí
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
vlády	vláda	k1gFnSc2	vláda
předchozí	předchozí	k2eAgFnSc2d1	předchozí
dynastie	dynastie	k1gFnSc2	dynastie
Jüan	jüan	k1gInSc1	jüan
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
Ming	Minga	k1gFnPc2	Minga
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prosperujícím	prosperující	k2eAgInSc7d1	prosperující
a	a	k8xC	a
bohatým	bohatý	k2eAgInSc7d1	bohatý
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
výrazný	výrazný	k2eAgInSc4d1	výrazný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc4d1	kulturní
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
a	a	k8xC	a
pobřežních	pobřežní	k2eAgInPc6d1	pobřežní
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
zavlažovací	zavlažovací	k2eAgInPc1d1	zavlažovací
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
pěstována	pěstován	k2eAgFnSc1d1	pěstována
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
produkce	produkce	k1gFnSc1	produkce
hedvábných	hedvábný	k2eAgFnPc2d1	hedvábná
a	a	k8xC	a
bavlněných	bavlněný	k2eAgFnPc2d1	bavlněná
tkanin	tkanina	k1gFnPc2	tkanina
<g/>
,	,	kIx,	,
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
.	.	kIx.	.
<g/>
Začátkem	začátkem	k7c2	začátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
mingská	mingskat	k5eAaPmIp3nS	mingskat
Čína	Čína	k1gFnSc1	Čína
na	na	k7c4	na
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
podrobila	podrobit	k5eAaPmAgFnS	podrobit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Mandžusko	Mandžusko	k1gNnSc4	Mandžusko
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Vietnam	Vietnam	k1gInSc1	Vietnam
a	a	k8xC	a
oživila	oživit	k5eAaPmAgFnS	oživit
obchodní	obchodní	k2eAgInPc4d1	obchodní
a	a	k8xC	a
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
,	,	kIx,	,
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc7d1	jihovýchodní
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Umožnila	umožnit	k5eAaPmAgFnS	umožnit
to	ten	k3xDgNnSc1	ten
velká	velký	k2eAgFnSc1d1	velká
flotila	flotila	k1gFnSc1	flotila
dalekomořských	dalekomořský	k2eAgFnPc2d1	dalekomořský
obchodních	obchodní	k2eAgFnPc2d1	obchodní
džunek	džunka	k1gFnPc2	džunka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
staly	stát	k5eAaPmAgFnP	stát
žádanou	žádaný	k2eAgFnSc7d1	žádaná
kořistí	kořist	k1gFnSc7	kořist
čínských	čínský	k2eAgMnPc2d1	čínský
a	a	k8xC	a
japonských	japonský	k2eAgMnPc2d1	japonský
pirátů	pirát	k1gMnPc2	pirát
<g/>
,	,	kIx,	,
napadajících	napadající	k2eAgFnPc2d1	napadající
rovněž	rovněž	k9	rovněž
pobřežní	pobřežní	k2eAgNnPc1d1	pobřežní
města	město	k1gNnPc1	město
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
vážným	vážný	k2eAgInSc7d1	vážný
ohrožením	ohrožení	k1gNnSc7	ohrožení
byly	být	k5eAaImAgFnP	být
neustávající	ustávající	k2eNgInPc4d1	neustávající
útoky	útok	k1gInPc4	útok
Mongolů	Mongol	k1gMnPc2	Mongol
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
před	před	k7c7	před
nájezdy	nájezd	k1gInPc7	nájezd
kočovníků	kočovník	k1gMnPc2	kočovník
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
Velká	velký	k2eAgFnSc1d1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
zeď	zeď	k1gFnSc1	zeď
a	a	k8xC	a
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
přes	přes	k7c4	přes
pět	pět	k4xCc4	pět
tisíc	tisíc	k4xCgInSc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
příkopů	příkop	k1gInPc2	příkop
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1556	[number]	k4	1556
byly	být	k5eAaImAgFnP	být
severní	severní	k2eAgFnPc1d1	severní
provincie	provincie	k1gFnPc1	provincie
Číny	Čína	k1gFnSc2	Čína
zasaženy	zasáhnout	k5eAaPmNgFnP	zasáhnout
silným	silný	k2eAgNnSc7d1	silné
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
kolem	kolem	k6eAd1	kolem
830	[number]	k4	830
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
Čína	Čína	k1gFnSc1	Čína
potýkala	potýkat	k5eAaImAgFnS	potýkat
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
hospodářskými	hospodářský	k2eAgInPc7d1	hospodářský
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
důsledkem	důsledek	k1gInSc7	důsledek
byla	být	k5eAaImAgFnS	být
početná	početný	k2eAgFnSc1d1	početná
rolnická	rolnický	k2eAgFnSc1d1	rolnická
povstání	povstání	k1gNnSc1	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Oslabení	oslabení	k1gNnSc1	oslabení
čínského	čínský	k2eAgInSc2d1	čínský
státu	stát	k1gInSc2	stát
využili	využít	k5eAaPmAgMnP	využít
Mandžuové	Mandžu	k1gMnPc1	Mandžu
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
roku	rok	k1gInSc2	rok
1644	[number]	k4	1644
obsadili	obsadit	k5eAaPmAgMnP	obsadit
sever	sever	k1gInSc4	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
severní	severní	k2eAgFnSc2d1	severní
Číny	Čína	k1gFnSc2	Čína
mandžuskou	mandžuský	k2eAgFnSc7d1	mandžuská
dynastií	dynastie	k1gFnSc7	dynastie
Čching	Čching	k1gInSc1	Čching
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
mingská	mingský	k2eAgFnSc1d1	mingský
vláda	vláda	k1gFnSc1	vláda
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1661	[number]	k4	1661
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zde	zde	k6eAd1	zde
žijících	žijící	k2eAgMnPc2d1	žijící
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
byli	být	k5eAaImAgMnP	být
úředníky	úředník	k1gMnPc7	úředník
spravujícími	spravující	k2eAgMnPc7d1	spravující
jižní	jižní	k2eAgFnSc7d1	jižní
provincie	provincie	k1gFnPc4	provincie
vybráni	vybrán	k2eAgMnPc1d1	vybrán
noví	nový	k2eAgMnPc1d1	nový
císaři	císař	k1gMnPc1	císař
<g/>
,	,	kIx,	,
pozdějšími	pozdní	k2eAgMnPc7d2	pozdější
historiky	historik	k1gMnPc7	historik
označeni	označit	k5eAaPmNgMnP	označit
za	za	k7c4	za
dynastii	dynastie	k1gFnSc4	dynastie
Jižní	jižní	k2eAgInSc1d1	jižní
Ming	Ming	k1gInSc1	Ming
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
mingský	mingský	k2eAgMnSc1d1	mingský
císař	císař	k1gMnSc1	císař
Ču	Ču	k1gMnPc2	Ču
Jou-lang	Jouang	k1gMnSc1	Jou-lang
roku	rok	k1gInSc2	rok
1661	[number]	k4	1661
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Barmy	Barma	k1gFnSc2	Barma
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
zde	zde	k6eAd1	zde
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Císaři	císař	k1gMnPc5	císař
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vláda	vláda	k1gFnSc1	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
Mingští	Mingský	k2eAgMnPc1d1	Mingský
císaři	císař	k1gMnPc1	císař
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
země	zem	k1gFnSc2	zem
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
hlavou	hlava	k1gFnSc7	hlava
vojenské	vojenský	k2eAgFnSc2d1	vojenská
i	i	k8xC	i
civilní	civilní	k2eAgFnSc2d1	civilní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
stál	stát	k5eAaImAgMnS	stát
císař	císař	k1gMnSc1	císař
nade	nad	k7c7	nad
všemi	všecek	k3xTgMnPc7	všecek
úředníky	úředník	k1gMnPc7	úředník
i	i	k8xC	i
generály	generál	k1gMnPc7	generál
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
řídila	řídit	k5eAaImAgFnS	řídit
jeho	jeho	k3xOp3gInPc4	jeho
výnosy	výnos	k1gInPc4	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Chung-wu	Chungu	k6eAd1	Chung-wu
skutečně	skutečně	k6eAd1	skutečně
moc	moc	k6eAd1	moc
pevně	pevně	k6eAd1	pevně
držel	držet	k5eAaImAgMnS	držet
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
masívních	masívní	k2eAgFnPc2d1	masívní
čistek	čistka	k1gFnPc2	čistka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
roku	rok	k1gInSc3	rok
1380	[number]	k4	1380
přinesly	přinést	k5eAaPmAgFnP	přinést
i	i	k9	i
zrušení	zrušení	k1gNnSc4	zrušení
ústředního	ústřední	k2eAgInSc2d1	ústřední
sekretariátu	sekretariát	k1gInSc2	sekretariát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
vedoucí	vedoucí	k1gMnSc1	vedoucí
byl	být	k5eAaImAgMnS	být
faktickým	faktický	k2eAgMnSc7d1	faktický
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
císař	císař	k1gMnSc1	císař
soustředil	soustředit	k5eAaPmAgMnS	soustředit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
rozhodování	rozhodování	k1gNnSc2	rozhodování
o	o	k7c6	o
všech	všecek	k3xTgInPc6	všecek
problémech	problém	k1gInPc6	problém
přesahujících	přesahující	k2eAgFnPc6d1	přesahující
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
jednoho	jeden	k4xCgNnSc2	jeden
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
třetí	třetí	k4xOgMnSc1	třetí
císař	císař	k1gMnSc1	císař
Jung-le	Junge	k1gFnSc2	Jung-le
osobně	osobně	k6eAd1	osobně
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
i	i	k8xC	i
malých	malý	k2eAgFnPc6d1	malá
záležitostech	záležitost	k1gFnPc6	záležitost
a	a	k8xC	a
rozhněval	rozhněvat	k5eAaPmAgMnS	rozhněvat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
nedostalo	dostat	k5eNaPmAgNnS	dostat
několik	několik	k4yIc4	několik
hlášení	hlášení	k1gNnPc2	hlášení
o	o	k7c6	o
nepodstatných	podstatný	k2eNgFnPc6d1	nepodstatná
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgMnPc1d1	další
panovníci	panovník	k1gMnPc1	panovník
už	už	k6eAd1	už
postrádali	postrádat	k5eAaImAgMnP	postrádat
rozhodnost	rozhodnost	k1gFnSc4	rozhodnost
prvních	první	k4xOgMnPc2	první
vládců	vládce	k1gMnPc2	vládce
dynastie	dynastie	k1gFnSc2	dynastie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
moc	moc	k1gFnSc1	moc
podléhala	podléhat	k5eAaImAgFnS	podléhat
řadě	řada	k1gFnSc3	řada
tradičních	tradiční	k2eAgNnPc2d1	tradiční
omezení	omezení	k1gNnPc2	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
císařů	císař	k1gMnPc2	císař
se	se	k3xPyFc4	se
neočekávalo	očekávat	k5eNaImAgNnS	očekávat
iniciativní	iniciativní	k2eAgNnSc1d1	iniciativní
rozhodování	rozhodování	k1gNnSc1	rozhodování
o	o	k7c4	o
směřování	směřování	k1gNnSc4	směřování
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Memoranda	memorandum	k1gNnPc1	memorandum
a	a	k8xC	a
požadavky	požadavek	k1gInPc1	požadavek
jim	on	k3xPp3gMnPc3	on
byly	být	k5eAaImAgInP	být
předkládány	předkládat	k5eAaImNgInP	předkládat
už	už	k6eAd1	už
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
řešení	řešení	k1gNnSc1	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vládce	vládce	k1gMnSc2	vládce
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
potvrzení	potvrzení	k1gNnSc1	potvrzení
předložených	předložený	k2eAgInPc2d1	předložený
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dosáhnutí	dosáhnutý	k2eAgMnPc1d1	dosáhnutý
souhlasu	souhlas	k1gInSc2	souhlas
předkladatelů	předkladatel	k1gMnPc2	předkladatel
s	s	k7c7	s
jím	on	k3xPp3gNnSc7	on
navrženým	navržený	k2eAgNnSc7d1	navržené
alternativním	alternativní	k2eAgNnSc7d1	alternativní
řešením	řešení	k1gNnSc7	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
císaři	císař	k1gMnPc1	císař
jmenovali	jmenovat	k5eAaBmAgMnP	jmenovat
úředníky	úředník	k1gMnPc4	úředník
a	a	k8xC	a
vojáky	voják	k1gMnPc4	voják
podle	podle	k7c2	podle
návrhů	návrh	k1gInPc2	návrh
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vojenství	vojenství	k1gNnSc4	vojenství
<g/>
;	;	kIx,	;
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vysokých	vysoká	k1gFnPc2	vysoká
hodnostářů	hodnostář	k1gMnPc2	hodnostář
dostal	dostat	k5eAaPmAgMnS	dostat
císař	císař	k1gMnSc1	císař
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
až	až	k9	až
třemi	tři	k4xCgInPc7	tři
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgFnPc1d2	významnější
otázky	otázka	k1gFnPc1	otázka
byly	být	k5eAaImAgFnP	být
diskutovány	diskutovat	k5eAaImNgFnP	diskutovat
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
audiencích	audience	k1gFnPc6	audience
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
neformálních	formální	k2eNgFnPc6d1	neformální
poradách	porada	k1gFnPc6	porada
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
relativně	relativně	k6eAd1	relativně
široké	široký	k2eAgFnSc2d1	široká
shody	shoda	k1gFnSc2	shoda
dvorských	dvorský	k2eAgMnPc2d1	dvorský
hodnostářů	hodnostář	k1gMnPc2	hodnostář
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
žádné	žádný	k3yNgFnSc6	žádný
zásadní	zásadní	k2eAgFnSc6d1	zásadní
otázce	otázka	k1gFnSc6	otázka
<g/>
.	.	kIx.	.
<g/>
Jelikož	jelikož	k8xS	jelikož
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
ústředního	ústřední	k2eAgInSc2d1	ústřední
sekretariátu	sekretariát	k1gInSc2	sekretariát
neexistovala	existovat	k5eNaImAgFnS	existovat
funkce	funkce	k1gFnSc1	funkce
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
předsedovi	předseda	k1gMnSc3	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
Chung-wu	Chungus	k1gInSc2	Chung-wus
její	její	k3xOp3gNnSc4	její
obnovení	obnovení	k1gNnSc4	obnovení
výslovně	výslovně	k6eAd1	výslovně
zakázal	zakázat	k5eAaPmAgInS	zakázat
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgInS	stát
nad	nad	k7c7	nad
ministry	ministr	k1gMnPc7	ministr
<g/>
,	,	kIx,	,
kontrolními	kontrolní	k2eAgMnPc7d1	kontrolní
a	a	k8xC	a
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
úřady	úřada	k1gMnPc7	úřada
pouze	pouze	k6eAd1	pouze
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Hladký	hladký	k2eAgInSc1d1	hladký
chod	chod	k1gInSc1	chod
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
aktivní	aktivní	k2eAgFnSc6d1	aktivní
účasti	účast	k1gFnSc6	účast
<g/>
.	.	kIx.	.
</s>
<s>
Úlohy	úloha	k1gFnPc1	úloha
koordinátora	koordinátor	k1gMnSc2	koordinátor
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
administrativního	administrativní	k2eAgInSc2d1	administrativní
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slaďuje	slaďovat	k5eAaImIp3nS	slaďovat
resortní	resortní	k2eAgInPc4d1	resortní
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
panovníkovi	panovníkův	k2eAgMnPc1d1	panovníkův
tajemníci	tajemník	k1gMnPc1	tajemník
<g/>
,	,	kIx,	,
velkých	velký	k2eAgInPc2d1	velký
sekretářů	sekretář	k1gInPc2	sekretář
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vysoce	vysoce	k6eAd1	vysoce
postavení	postavení	k1gNnSc4	postavení
eunuchové	eunuch	k1gMnPc1	eunuch
<g/>
,	,	kIx,	,
obklopující	obklopující	k2eAgMnSc1d1	obklopující
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
každodenně	každodenně	k6eAd1	každodenně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
konzultující	konzultující	k2eAgFnSc1d1	konzultující
projednávané	projednávaný	k2eAgFnPc4d1	projednávaná
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Faktická	faktický	k2eAgFnSc1d1	faktická
moc	moc	k1gFnSc1	moc
císařových	císařův	k2eAgMnPc2d1	císařův
sekretářů	sekretář	k1gMnPc2	sekretář
anebo	anebo	k8xC	anebo
eunuchů	eunuch	k1gMnPc2	eunuch
<g/>
,	,	kIx,	,
však	však	k9	však
byla	být	k5eAaImAgFnS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
potvrzení	potvrzení	k1gNnSc4	potvrzení
jejich	jejich	k3xOp3gNnSc2	jejich
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
panovníkem	panovník	k1gMnSc7	panovník
<g/>
;	;	kIx,	;
sami	sám	k3xTgMnPc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
neměli	mít	k5eNaImAgMnP	mít
ani	ani	k8xC	ani
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
vydávat	vydávat	k5eAaPmF	vydávat
příkazy	příkaz	k1gInPc4	příkaz
ministrům	ministr	k1gMnPc3	ministr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mingští	Mingský	k2eAgMnPc1d1	Mingský
císaři	císař	k1gMnPc1	císař
sídlili	sídlit	k5eAaImAgMnP	sídlit
zprvu	zprvu	k6eAd1	zprvu
v	v	k7c6	v
Zakázaném	zakázaný	k2eAgNnSc6d1	zakázané
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
komplexu	komplex	k1gInSc6	komplex
paláců	palác	k1gInPc2	palác
a	a	k8xC	a
budov	budova	k1gFnPc2	budova
vybudovaném	vybudovaný	k2eAgInSc6d1	vybudovaný
v	v	k7c6	v
Nankingu	Nanking	k1gInSc6	Nanking
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Jung-le	Junge	k1gNnSc2	Jung-le
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
přenesení	přenesení	k1gNnSc6	přenesení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
do	do	k7c2	do
Pekingu	Peking	k1gInSc2	Peking
<g/>
,	,	kIx,	,
zdejší	zdejší	k2eAgNnSc1d1	zdejší
Zakázané	zakázaný	k2eAgNnSc1d1	zakázané
město	město	k1gNnSc1	město
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
72	[number]	k4	72
ha	ha	kA	ha
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
panovník	panovník	k1gMnSc1	panovník
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
dvorem	dvůr	k1gInSc7	dvůr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Ču	Ču	k?	Ču
Piao	Piao	k1gMnSc1	Piao
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
zakladatele	zakladatel	k1gMnSc2	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
Chung-wua	Chunguus	k1gMnSc2	Chung-wuus
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Chung-wu	Chunga	k1gFnSc4	Chung-wa
za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
nástupce	nástupce	k1gMnSc1	nástupce
určil	určit	k5eAaPmAgMnS	určit
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
žijícího	žijící	k2eAgMnSc4d1	žijící
Ču	Ču	k1gMnSc4	Ču
Piaova	Piaův	k2eAgMnSc4d1	Piaův
syna	syn	k1gMnSc4	syn
Ču	Ču	k1gMnSc4	Ču
Jün-wena	Jünen	k1gMnSc4	Jün-wen
(	(	kIx(	(
<g/>
císaře	císař	k1gMnSc4	císař
Ťien-wena	Ťienen	k1gMnSc4	Ťien-wen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
mladým	mladý	k2eAgMnSc7d1	mladý
císařem	císař	k1gMnSc7	císař
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
roku	rok	k1gInSc2	rok
1398	[number]	k4	1398
a	a	k8xC	a
neprodleně	prodleně	k6eNd1	prodleně
začala	začít	k5eAaPmAgFnS	začít
rázně	rázně	k6eAd1	rázně
zasahovat	zasahovat	k5eAaImF	zasahovat
vůči	vůči	k7c3	vůči
císařovým	císařův	k2eAgMnPc3d1	císařův
strýcům	strýc	k1gMnPc3	strýc
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1399	[number]	k4	1399
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
povstání	povstání	k1gNnSc1	povstání
nejsilnějšího	silný	k2eAgNnSc2d3	nejsilnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
–	–	k?	–
Ču	Ču	k1gFnPc2	Ču
Tiho	Tiho	k6eAd1	Tiho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nazval	nazvat	k5eAaBmAgMnS	nazvat
kampaň	kampaň	k1gFnSc4	kampaň
ťing-nan	ťingan	k1gMnSc1	ťing-nan
<g/>
,	,	kIx,	,
kampaň	kampaň	k1gFnSc1	kampaň
za	za	k7c4	za
odstranění	odstranění	k1gNnSc4	odstranění
nepořádků	nepořádek	k1gInPc2	nepořádek
<g/>
,	,	kIx,	,
Ču	Ču	k1gMnSc1	Ču
Ti	ty	k3xPp2nSc3	ty
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1402	[number]	k4	1402
jeho	jeho	k3xOp3gNnSc2	jeho
vojska	vojsko	k1gNnSc2	vojsko
dobyla	dobýt	k5eAaPmAgFnS	dobýt
hlavní	hlavní	k2eAgFnSc1d1	hlavní
město	město	k1gNnSc4	město
Nanking	Nanking	k1gInSc1	Nanking
<g/>
.	.	kIx.	.
</s>
<s>
Ťien-wen	Ťienen	k1gInSc1	Ťien-wen
zahynul	zahynout	k5eAaPmAgInS	zahynout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
císařského	císařský	k2eAgInSc2d1	císařský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
císař	císař	k1gMnSc1	císař
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
všemožně	všemožně	k6eAd1	všemožně
zahladit	zahladit	k5eAaPmF	zahladit
památku	památka	k1gFnSc4	památka
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
popřel	popřít	k5eAaPmAgMnS	popřít
jeho	jeho	k3xOp3gFnSc4	jeho
legitimitu	legitimita	k1gFnSc4	legitimita
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
zpětně	zpětně	k6eAd1	zpětně
zrušil	zrušit	k5eAaPmAgInS	zrušit
éru	éra	k1gFnSc4	éra
Ťien-wen	Ťienen	k1gInSc1	Ťien-wen
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgMnPc1d1	další
císaři	císař	k1gMnPc1	císař
přebírali	přebírat	k5eAaImAgMnP	přebírat
trůn	trůn	k1gInSc4	trůn
podle	podle	k7c2	podle
zásady	zásada	k1gFnSc2	zásada
primogenitury	primogenitura	k1gFnSc2	primogenitura
–	–	k?	–
novým	nový	k2eAgMnSc7d1	nový
panovníkem	panovník	k1gMnSc7	panovník
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
nejstarší	starý	k2eAgMnSc1d3	nejstarší
(	(	kIx(	(
<g/>
žijící	žijící	k2eAgMnSc1d1	žijící
<g/>
)	)	kIx)	)
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
mužský	mužský	k2eAgMnSc1d1	mužský
příbuzný	příbuzný	k1gMnSc1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
roku	rok	k1gInSc2	rok
1449	[number]	k4	1449
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
císař	císař	k1gMnSc1	císař
Jing-cung	Jingung	k1gMnSc1	Jing-cung
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
mongolského	mongolský	k2eAgNnSc2d1	mongolské
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kratším	krátký	k2eAgNnSc6d2	kratší
váhání	váhání	k1gNnSc6	váhání
se	se	k3xPyFc4	se
dvůr	dvůr	k1gInSc1	dvůr
shodl	shodnout	k5eAaPmAgInS	shodnout
na	na	k7c6	na
povýšení	povýšení	k1gNnSc6	povýšení
císařova	císařův	k2eAgMnSc2d1	císařův
bratra	bratr	k1gMnSc2	bratr
Ťing-tchaje	Ťingchaje	k1gMnSc2	Ťing-tchaje
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
ovšem	ovšem	k9	ovšem
Mongolové	Mongol	k1gMnPc1	Mongol
zajatého	zajatý	k2eAgNnSc2d1	zajaté
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k9	již
bývalého	bývalý	k2eAgMnSc2d1	bývalý
<g/>
,	,	kIx,	,
císaře	císař	k1gMnSc2	císař
propustili	propustit	k5eAaPmAgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
strávil	strávit	k5eAaPmAgMnS	strávit
Jing-cung	Jingung	k1gInSc4	Jing-cung
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1457	[number]	k4	1457
Ťing-tchaj	Ťingchaj	k1gInSc1	Ťing-tchaj
neonemocněl	onemocnět	k5eNaPmAgInS	onemocnět
a	a	k8xC	a
protože	protože	k8xS	protože
neměl	mít	k5eNaImAgInS	mít
dědice	dědic	k1gMnPc4	dědic
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Jing-cung	Jingung	k1gInSc1	Jing-cung
schopen	schopen	k2eAgInSc1d1	schopen
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
palácový	palácový	k2eAgInSc1d1	palácový
převrat	převrat	k1gInSc1	převrat
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
opět	opět	k6eAd1	opět
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
Ťing-tchaj	Ťingchaj	k1gInSc1	Ťing-tchaj
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
.	.	kIx.	.
<g/>
Vážný	vážný	k2eAgInSc1d1	vážný
konflikt	konflikt	k1gInSc1	konflikt
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1521	[number]	k4	1521
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
císař	císař	k1gMnSc1	císař
Čeng-te	Čeng	k1gInSc5	Čeng-t
bez	bez	k7c2	bez
přímého	přímý	k2eAgMnSc2d1	přímý
dědice	dědic	k1gMnSc2	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
vládcem	vládce	k1gMnSc7	vládce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
příbuzný	příbuzný	k1gMnSc1	příbuzný
–	–	k?	–
bratranec	bratranec	k1gMnSc1	bratranec
Ču	Ču	k1gMnSc1	Ču
Chou-cchung	Chouchung	k1gMnSc1	Chou-cchung
(	(	kIx(	(
<g/>
císař	císař	k1gMnSc1	císař
Ťia-ťing	Ťia-ťing	k1gInSc1	Ťia-ťing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ministři	ministr	k1gMnPc1	ministr
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
Ťia-ťing	Ťia-ťing	k1gInSc1	Ťia-ťing
přijat	přijmout	k5eAaPmNgInS	přijmout
za	za	k7c2	za
syna	syn	k1gMnSc2	syn
zemřelého	zemřelý	k1gMnSc2	zemřelý
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
zachována	zachovat	k5eAaPmNgFnS	zachovat
nepřerušená	přerušený	k2eNgFnSc1d1	nepřerušená
posloupnost	posloupnost	k1gFnSc1	posloupnost
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
císařského	císařský	k2eAgInSc2d1	císařský
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
otec	otec	k1gMnSc1	otec
<g/>
–	–	k?	–
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Ťia-ťing	Ťia-ťing	k1gInSc1	Ťia-ťing
ministerské	ministerský	k2eAgInPc1d1	ministerský
požadavky	požadavek	k1gInPc1	požadavek
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
posmrtné	posmrtný	k2eAgNnSc4d1	posmrtné
jmenování	jmenování	k1gNnSc4	jmenování
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
stav	stav	k1gInSc1	stav
jeho	jeho	k3xOp3gMnSc4	jeho
otce	otec	k1gMnSc4	otec
uveden	uveden	k2eAgInSc1d1	uveden
do	do	k7c2	do
souladu	soulad	k1gInSc2	soulad
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
vlastním	vlastní	k2eAgNnSc7d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
spor	spor	k1gInSc1	spor
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
obdržel	obdržet	k5eAaPmAgInS	obdržet
název	název	k1gInSc4	název
velký	velký	k2eAgInSc1d1	velký
spor	spor	k1gInSc1	spor
o	o	k7c4	o
obřady	obřad	k1gInPc4	obřad
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
táhl	táhnout	k5eAaImAgInS	táhnout
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
a	a	k8xC	a
panovník	panovník	k1gMnSc1	panovník
ho	on	k3xPp3gNnSc4	on
nakonec	nakonec	k6eAd1	nakonec
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
silou	síla	k1gFnSc7	síla
–	–	k?	–
popravami	poprava	k1gFnPc7	poprava
a	a	k8xC	a
vypovězením	vypovězení	k1gNnSc7	vypovězení
protestujících	protestující	k2eAgMnPc2d1	protestující
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
vážně	vážně	k6eAd1	vážně
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
počátek	počátek	k1gInSc4	počátek
Ťia-ťingovy	Ťia-ťingův	k2eAgFnSc2d1	Ťia-ťingův
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
Dlouhotrvající	dlouhotrvající	k2eAgInSc1d1	dlouhotrvající
nástupnický	nástupnický	k2eAgInSc1d1	nástupnický
spor	spor	k1gInSc1	spor
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Wan-liho	Wani	k1gMnSc2	Wan-li
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
totiž	totiž	k9	totiž
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jmenovat	jmenovat	k5eAaImF	jmenovat
korunním	korunní	k2eAgMnSc7d1	korunní
princem	princ	k1gMnSc7	princ
svého	svůj	k3xOyFgMnSc2	svůj
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Ču	Ču	k1gMnSc2	Ču
Čchang-lua	Čchanguus	k1gMnSc2	Čchang-luus
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnSc1d2	pozdější
císař	císař	k1gMnSc1	císař
Tchaj-čchang	Tchaj-čchang	k1gMnSc1	Tchaj-čchang
<g/>
)	)	kIx)	)
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
a	a	k8xC	a
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
nástupnickým	nástupnický	k2eAgInSc7d1	nástupnický
řádem	řád	k1gInSc7	řád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
svého	svůj	k3xOyFgMnSc4	svůj
třetího	třetí	k4xOgMnSc4	třetí
syna	syn	k1gMnSc4	syn
Ču	Ču	k1gMnSc4	Ču
Čchang-süna	Čchangün	k1gMnSc4	Čchang-sün
<g/>
,	,	kIx,	,
potomka	potomek	k1gMnSc4	potomek
jeho	jeho	k3xOp3gFnSc7	jeho
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
konkubíny	konkubína	k1gFnPc4	konkubína
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
spor	spor	k1gInSc1	spor
trval	trvat	k5eAaImAgInS	trvat
přes	přes	k7c4	přes
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
konečně	konečně	k6eAd1	konečně
panovník	panovník	k1gMnSc1	panovník
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
tlaku	tlak	k1gInSc3	tlak
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
následníka	následník	k1gMnSc4	následník
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
odcizení	odcizení	k1gNnSc1	odcizení
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
neústupností	neústupnost	k1gFnSc7	neústupnost
obou	dva	k4xCgInPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
značných	značný	k2eAgInPc2d1	značný
rozměrů	rozměr	k1gInPc2	rozměr
a	a	k8xC	a
poškozovalo	poškozovat	k5eAaImAgNnS	poškozovat
správu	správa	k1gFnSc4	správa
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
panovník	panovník	k1gMnSc1	panovník
přestal	přestat	k5eAaPmAgMnS	přestat
stýkat	stýkat	k5eAaImF	stýkat
s	s	k7c7	s
ministry	ministr	k1gMnPc7	ministr
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
jmenovat	jmenovat	k5eAaBmF	jmenovat
nové	nový	k2eAgMnPc4d1	nový
úředníky	úředník	k1gMnPc4	úředník
na	na	k7c4	na
uvolněná	uvolněný	k2eAgNnPc4d1	uvolněné
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
rodu	rod	k1gInSc2	rod
==	==	k?	==
</s>
</p>
<p>
<s>
Chung-wu	Chunga	k1gFnSc4	Chung-wa
svého	svůj	k3xOyFgMnSc2	svůj
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Ču	Ču	k1gMnSc1	Ču
Piaa	Piaa	k1gMnSc1	Piaa
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
(	(	kIx(	(
<g/>
太	太	k?	太
<g/>
,	,	kIx,	,
tchaj-c	tchaj	k1gFnSc1	tchaj-c
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatním	ostatní	k2eAgMnPc3d1	ostatní
synům	syn	k1gMnPc3	syn
udělil	udělit	k5eAaPmAgMnS	udělit
tituly	titul	k1gInPc4	titul
knížat	kníže	k1gMnPc2wR	kníže
(	(	kIx(	(
<g/>
親	親	k?	親
<g/>
,	,	kIx,	,
čchin	čchin	k2eAgInSc1d1	čchin
wang	wang	k1gInSc1	wang
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
kníže	kníže	k1gMnSc1	kníže
[	[	kIx(	[
<g/>
císařské	císařský	k2eAgFnSc2d1	císařská
<g/>
]	]	kIx)	]
krve	krev	k1gFnSc2	krev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
též	též	k9	též
jednoduše	jednoduše	k6eAd1	jednoduše
<g />
.	.	kIx.	.
</s>
<s>
王	王	k?	王
<g/>
,	,	kIx,	,
wang	wang	k1gInSc1	wang
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
dosažení	dosažení	k1gNnSc3	dosažení
zhruba	zhruba	k6eAd1	zhruba
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
je	být	k5eAaImIp3nS	být
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
provincií	provincie	k1gFnPc2	provincie
s	s	k7c7	s
početným	početný	k2eAgInSc7d1	početný
doprovodem	doprovod	k1gInSc7	doprovod
a	a	k8xC	a
širokými	široký	k2eAgInPc7d1	široký
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
vojenskými	vojenský	k2eAgInPc7d1	vojenský
<g/>
,	,	kIx,	,
pravomocemi	pravomoc	k1gFnPc7	pravomoc
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
přes	přes	k7c4	přes
své	svůj	k3xOyFgInPc4	svůj
tituly	titul	k1gInPc4	titul
nevládli	vládnout	k5eNaImAgMnP	vládnout
v	v	k7c6	v
regionech	region	k1gInPc6	region
jako	jako	k8xS	jako
údělná	údělný	k2eAgNnPc1d1	údělné
knížata	kníže	k1gNnPc1	kníže
<g/>
,	,	kIx,	,
úředníci	úředník	k1gMnPc1	úředník
místní	místní	k2eAgFnSc2d1	místní
správy	správa	k1gFnSc2	správa
nadále	nadále	k6eAd1	nadále
podléhali	podléhat	k5eAaImAgMnP	podléhat
centru	centrum	k1gNnSc3	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
císaře	císař	k1gMnSc4	císař
představovali	představovat	k5eAaImAgMnP	představovat
oporu	opora	k1gFnSc4	opora
jeho	jeho	k3xOp3gFnSc2	jeho
osobní	osobní	k2eAgFnSc2d1	osobní
moci	moc	k1gFnSc2	moc
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
na	na	k7c6	na
běžné	běžný	k2eAgFnSc6d1	běžná
civilní	civilní	k2eAgFnSc6d1	civilní
i	i	k8xC	i
vojenské	vojenský	k2eAgFnSc6d1	vojenská
hierarchii	hierarchie	k1gFnSc6	hierarchie
<g/>
;	;	kIx,	;
po	po	k7c6	po
císařově	císařův	k2eAgFnSc6d1	císařova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
pokusila	pokusit	k5eAaPmAgFnS	pokusit
omezit	omezit	k5eAaPmF	omezit
jejich	jejich	k3xOp3gInSc4	jejich
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyprovokovalo	vyprovokovat	k5eAaPmAgNnS	vyprovokovat
povstání	povstání	k1gNnSc2	povstání
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
–	–	k?	–
Ču	Ču	k1gMnSc1	Ču
Tiho	Tiho	k1gMnSc1	Tiho
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Ču	Ču	k1gMnSc1	Ču
Ti	ten	k3xDgMnPc1	ten
jako	jako	k8xS	jako
císař	císař	k1gMnSc1	císař
Jung-le	Junge	k1gFnSc2	Jung-le
moc	moc	k6eAd1	moc
knížat	kníže	k1gMnPc2wR	kníže
v	v	k7c6	v
regionech	region	k1gInPc6	region
omezil	omezit	k5eAaPmAgInS	omezit
a	a	k8xC	a
po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
vzpouře	vzpoura	k1gFnSc6	vzpoura
knížete	kníže	k1gNnSc2wR	kníže
Ču	Ču	k1gFnPc2	Ču
Kao-süa	Kaoü	k1gInSc2	Kao-sü
roku	rok	k1gInSc2	rok
1426	[number]	k4	1426
ztratili	ztratit	k5eAaPmAgMnP	ztratit
politický	politický	k2eAgInSc4d1	politický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedené	uvedený	k2eAgFnSc2d1	uvedená
skupiny	skupina	k1gFnSc2	skupina
byli	být	k5eAaImAgMnP	být
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
rodu	rod	k1gInSc2	rod
ze	z	k7c2	z
správy	správa	k1gFnSc2	správa
země	zem	k1gFnSc2	zem
vyloučeni	vyloučen	k2eAgMnPc1d1	vyloučen
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Chung-wuových	Chunguův	k2eAgMnPc2d1	Chung-wuův
nástupců	nástupce	k1gMnPc2	nástupce
byl	být	k5eAaImAgInS	být
nadále	nadále	k6eAd1	nadále
už	už	k6eAd1	už
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
císařští	císařský	k2eAgMnPc1d1	císařský
synové	syn	k1gMnPc1	syn
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stávali	stávat	k5eAaImAgMnP	stávat
knížaty	kníže	k1gMnPc7wR	kníže
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
posíláni	posílat	k5eAaImNgMnP	posílat
do	do	k7c2	do
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Dědic	dědic	k1gMnSc1	dědic
knížete	kníže	k1gNnSc2wR	kníže
přebíral	přebírat	k5eAaImAgMnS	přebírat
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
synové	syn	k1gMnPc1	syn
byli	být	k5eAaImAgMnP	být
knížaty	kníže	k1gMnPc7wR	kníže
komandérie	komandérie	k1gFnSc2	komandérie
(	(	kIx(	(
<g/>
郡	郡	k?	郡
<g/>
,	,	kIx,	,
ťün-wang	ťünang	k1gMnSc1	ťün-wang
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
generacích	generace	k1gFnPc6	generace
dostávali	dostávat	k5eAaImAgMnP	dostávat
mužští	mužský	k2eAgMnPc1d1	mužský
členové	člen	k1gMnPc1	člen
rodu	rod	k1gInSc2	rod
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
snižující	snižující	k2eAgInPc1d1	snižující
tituly	titul	k1gInPc1	titul
v	v	k7c6	v
celkem	celkem	k6eAd1	celkem
šesti	šest	k4xCc6	šest
úrovních	úroveň	k1gFnPc6	úroveň
(	(	kIx(	(
<g/>
鎭	鎭	k?	鎭
<g/>
,	,	kIx,	,
čen-kuo	čenuo	k1gMnSc1	čen-kuo
ťiang-ťün	ťiang-ťün	k1gMnSc1	ťiang-ťün
<g/>
;	;	kIx,	;
輔	輔	k?	輔
<g/>
,	,	kIx,	,
fu-kuo	fuuo	k1gMnSc1	fu-kuo
ťiang-ťün	ťiang-ťün	k1gMnSc1	ťiang-ťün
<g/>
;	;	kIx,	;
奉	奉	k?	奉
<g/>
,	,	kIx,	,
feng-kuo	fenguo	k1gMnSc1	feng-kuo
ťiang-ťün	ťiang-ťün	k1gMnSc1	ťiang-ťün
<g/>
;	;	kIx,	;
鎭	鎭	k?	鎭
<g/>
,	,	kIx,	,
čen-kuo	čenuo	k1gMnSc1	čen-kuo
čung-wej	čungej	k1gInSc1	čung-wej
<g/>
;	;	kIx,	;
輔	輔	k?	輔
<g/>
,	,	kIx,	,
fu-kuo	fuuo	k6eAd1	fu-kuo
čung-wej	čungej	k1gInSc1	čung-wej
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
奉	奉	k?	奉
<g/>
,	,	kIx,	,
feng-kuo	fenguo	k1gMnSc1	feng-kuo
čung-wej	čungej	k1gInSc1	čung-wej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
muž	muž	k1gMnSc1	muž
dynastie	dynastie	k1gFnSc2	dynastie
měl	mít	k5eAaImAgMnS	mít
minimálně	minimálně	k6eAd1	minimálně
poslední	poslední	k2eAgInSc4d1	poslední
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
smrti	smrt	k1gFnSc2	smrt
Chung-wua	Chungu	k1gInSc2	Chung-wu
(	(	kIx(	(
<g/>
1398	[number]	k4	1398
<g/>
)	)	kIx)	)
žilo	žít	k5eAaImAgNnS	žít
58	[number]	k4	58
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
127	[number]	k4	127
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
419	[number]	k4	419
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
konci	konec	k1gInSc6	konec
už	už	k9	už
přes	přes	k7c4	přes
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
měli	mít	k5eAaImAgMnP	mít
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
zaopatření	zaopatření	k1gNnSc4	zaopatření
ze	z	k7c2	z
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgFnSc4d1	soudní
imunitu	imunita	k1gFnSc4	imunita
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
privilegií	privilegium	k1gNnPc2	privilegium
závislých	závislý	k2eAgNnPc2d1	závislé
na	na	k7c6	na
jejich	jejich	k3xOp3gInPc6	jejich
titulech	titul	k1gInPc6	titul
a	a	k8xC	a
postavení	postavení	k1gNnSc6	postavení
<g/>
;	;	kIx,	;
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
hromadit	hromadit	k5eAaImF	hromadit
i	i	k9	i
pozemkový	pozemkový	k2eAgInSc4d1	pozemkový
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Pozdně	pozdně	k6eAd1	pozdně
mingští	mingský	k2eAgMnPc1d1	mingský
autoři	autor	k1gMnPc1	autor
odhadovali	odhadovat	k5eAaImAgMnP	odhadovat
počet	počet	k1gInSc4	počet
mužských	mužský	k2eAgMnPc2d1	mužský
potomků	potomek	k1gMnPc2	potomek
Chung-wua	Chungu	k1gInSc2	Chung-wu
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Tituly	titul	k1gInPc1	titul
a	a	k8xC	a
penzemi	penze	k1gFnPc7	penze
byly	být	k5eAaImAgFnP	být
obdařeny	obdařit	k5eAaPmNgFnP	obdařit
i	i	k9	i
dcery	dcera	k1gFnPc1	dcera
císařského	císařský	k2eAgInSc2d1	císařský
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
manželé	manžel	k1gMnPc1	manžel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
růstem	růst	k1gInSc7	růst
počtu	počet	k1gInSc2	počet
členů	člen	k1gMnPc2	člen
dynastie	dynastie	k1gFnSc2	dynastie
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
hmotné	hmotný	k2eAgNnSc4d1	hmotné
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
značně	značně	k6eAd1	značně
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zatížily	zatížit	k5eAaPmAgInP	zatížit
především	především	k6eAd1	především
severočínské	severočínský	k2eAgInPc1d1	severočínský
regiony	region	k1gInPc1	region
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlila	sídlit	k5eAaImAgFnS	sídlit
většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
roku	rok	k1gInSc2	rok
1562	[number]	k4	1562
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Šan-si	Šane	k1gFnSc4	Šan-se
vynaloženo	vynaložen	k2eAgNnSc1d1	vynaloženo
z	z	k7c2	z
výnosu	výnos	k1gInSc2	výnos
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
daně	daň	k1gFnSc2	daň
více	hodně	k6eAd2	hodně
prostředků	prostředek	k1gInPc2	prostředek
než	než	k8xS	než
na	na	k7c4	na
výdaje	výdaj	k1gInPc4	výdaj
provinčního	provinční	k2eAgNnSc2d1	provinční
<g/>
,	,	kIx,	,
prefekturních	prefekturní	k2eAgInPc2d1	prefekturní
a	a	k8xC	a
okresních	okresní	k2eAgInPc2d1	okresní
úřadů	úřad	k1gInPc2	úřad
dohromady	dohromady	k6eAd1	dohromady
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
severočínských	severočínský	k2eAgInPc6d1	severočínský
okresech	okres	k1gInPc6	okres
šla	jít	k5eAaImAgFnS	jít
členům	člen	k1gInPc3	člen
dynastie	dynastie	k1gFnSc2	dynastie
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
veškerých	veškerý	k3xTgInPc2	veškerý
daňových	daňový	k2eAgInPc2d1	daňový
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodokmen	rodokmen	k1gInSc4	rodokmen
==	==	k?	==
</s>
</p>
<p>
<s>
Rodokmen	rodokmen	k1gInSc1	rodokmen
dynastie	dynastie	k1gFnSc2	dynastie
je	být	k5eAaImIp3nS	být
zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
především	především	k6eAd1	především
císaře	císař	k1gMnPc4	císař
a	a	k8xC	a
následníky	následník	k1gMnPc4	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
panovníků	panovník	k1gMnPc2	panovník
Jižní	jižní	k2eAgInSc4d1	jižní
Ming	Ming	k1gInSc4	Ming
<g/>
.	.	kIx.	.
<g/>
Uvedeno	uveden	k2eAgNnSc1d1	uvedeno
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pořadí	pořadí	k1gNnSc1	pořadí
narození	narození	k1gNnSc1	narození
(	(	kIx(	(
<g/>
kolikátý	kolikátý	k4xOyIgMnSc1	kolikátý
syn	syn	k1gMnSc1	syn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
příjmení	příjmení	k1gNnSc1	příjmení
(	(	kIx(	(
<g/>
Ču	Ču	k1gMnSc5	Ču
<g/>
,	,	kIx,	,
朱	朱	k?	朱
<g/>
)	)	kIx)	)
a	a	k8xC	a
osobní	osobní	k2eAgNnSc1d1	osobní
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
data	datum	k1gNnSc2	datum
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
datum	datum	k1gNnSc1	datum
jmenování	jmenování	k1gNnSc6	jmenování
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
</s>
</p>
<p>
<s>
u	u	k7c2	u
knížat	kníže	k1gMnPc2wR	kníže
též	též	k9	též
knížecí	knížecí	k2eAgInSc1d1	knížecí
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
chrámové	chrámový	k2eAgNnSc4d1	chrámové
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
<g/>
-li	i	k?	-li
uděleno	udělit	k5eAaPmNgNnS	udělit
<g/>
,	,	kIx,	,
<g/>
u	u	k7c2	u
císařů	císař	k1gMnPc2	císař
ještě	ještě	k6eAd1	ještě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jméno	jméno	k1gNnSc1	jméno
éry	éra	k1gFnSc2	éra
(	(	kIx(	(
<g/>
tučně	tučně	k6eAd1	tučně
<g/>
)	)	kIx)	)
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
tučně	tučně	k6eAd1	tučně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pod	pod	k7c7	pod
obdélníkem	obdélník	k1gInSc7	obdélník
počet	počet	k1gInSc1	počet
synůCísařové	synůCísařová	k1gFnSc2	synůCísařová
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Minga	k1gFnPc2	Minga
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
červených	červený	k2eAgInPc6d1	červený
rámečcích	rámeček	k1gInPc6	rámeček
<g/>
,	,	kIx,	,
císařové	císařová	k1gFnSc2	císařová
Jižní	jižní	k2eAgInSc1d1	jižní
Ming	Ming	k1gInSc1	Ming
v	v	k7c6	v
modrých	modré	k1gNnPc6	modré
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Chinese	Chinese	k1gFnSc2	Chinese
emperors	emperorsa	k1gFnPc2	emperorsa
family	famít	k5eAaPmAgFnP	famít
tree	tre	k1gInSc2	tre
(	(	kIx(	(
<g/>
late	lat	k1gInSc5	lat
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BROOK	BROOK	kA	BROOK
<g/>
,	,	kIx,	,
Timothy	Timoth	k1gInPc7	Timoth
<g/>
.	.	kIx.	.
</s>
<s>
Čtvero	čtvero	k4xRgNnSc4	čtvero
ročních	roční	k2eAgFnPc2d1	roční
období	období	k1gNnPc2	období
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Ming	k1gInSc1	Ming
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
v	v	k7c6	v
období	období	k1gNnSc6	období
1368	[number]	k4	1368
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Vladimír	Vladimír	k1gMnSc1	Vladimír
Liščák	Liščák	k1gMnSc1	Liščák
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
368	[number]	k4	368
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
583	[number]	k4	583
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DREYER	DREYER	kA	DREYER
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
L.	L.	kA	L.
Early	earl	k1gMnPc4	earl
Ming	Ming	k1gInSc1	Ming
China	China	k1gFnSc1	China
<g/>
:	:	kIx,	:
a	a	k8xC	a
political	politicat	k5eAaPmAgMnS	politicat
history	histor	k1gInPc4	histor
<g/>
,	,	kIx,	,
1355	[number]	k4	1355
<g/>
-	-	kIx~	-
<g/>
1435	[number]	k4	1435
<g/>
.	.	kIx.	.
</s>
<s>
Stanford	Stanford	k1gInSc1	Stanford
<g/>
:	:	kIx,	:
Stanford	Stanford	k1gInSc1	Stanford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
315	[number]	k4	315
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8047	[number]	k4	8047
<g/>
-	-	kIx~	-
<g/>
1105	[number]	k4	1105
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FAIRBANK	FAIRBANK	kA	FAIRBANK
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Marin	Marina	k1gFnPc2	Marina
Hála	Hála	k1gMnSc1	Hála
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Hollanová	Hollanová	k1gFnSc1	Hollanová
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Lomová	lomový	k2eAgFnSc1d1	Lomová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
249	[number]	k4	249
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
Asian	Asian	k1gMnSc1	Asian
Studies	Studies	k1gMnSc1	Studies
<g/>
.	.	kIx.	.
</s>
<s>
Ming	Ming	k1gInSc1	Ming
Biographical	Biographical	k1gFnSc2	Biographical
History	Histor	k1gInPc1	Histor
Project	Projecta	k1gFnPc2	Projecta
Committee	Committee	k1gFnPc2	Committee
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
FANG	FANG	kA	FANG
<g/>
,	,	kIx,	,
Chaoying	Chaoying	k1gInSc1	Chaoying
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dictionary	Dictionar	k1gInPc1	Dictionar
of	of	k?	of
Ming	Ming	k1gInSc4	Ming
Biography	Biographa	k1gMnSc2	Biographa
<g/>
,	,	kIx,	,
1368	[number]	k4	1368
<g/>
-	-	kIx~	-
<g/>
1644	[number]	k4	1644
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
xxi	xxi	k?	xxi
+	+	kIx~	+
1751	[number]	k4	1751
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0231038011	[number]	k4	0231038011
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
023103833X	[number]	k4	023103833X
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HUCKER	HUCKER	kA	HUCKER
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
O.	O.	kA	O.
A	a	k8xC	a
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
Official	Official	k1gMnSc1	Official
Titles	Titles	k1gMnSc1	Titles
in	in	k?	in
Imperial	Imperial	k1gInSc1	Imperial
China	China	k1gFnSc1	China
<g/>
.	.	kIx.	.
</s>
<s>
Stanford	Stanford	k1gInSc1	Stanford
<g/>
:	:	kIx,	:
Stanford	Stanford	k1gInSc1	Stanford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8047	[number]	k4	8047
<g/>
-	-	kIx~	-
<g/>
1193	[number]	k4	1193
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
'	'	kIx"	'
<g/>
IEN	IEN	kA	IEN
<g/>
,	,	kIx,	,
Mu	on	k3xPp3gNnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Traditional	Traditionat	k5eAaPmAgInS	Traditionat
government	government	k1gInSc1	government
in	in	k?	in
imperial	imperial	k1gInSc1	imperial
China	China	k1gFnSc1	China
<g/>
:	:	kIx,	:
a	a	k8xC	a
critical	criticat	k5eAaPmAgMnS	criticat
analysis	analysis	k1gFnSc4	analysis
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Chün-tu	Chün	k1gInSc2	Chün-t
Hsüeh	Hsüeha	k1gFnPc2	Hsüeha
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
O.	O.	kA	O.
Totten	Totten	k2eAgInSc1d1	Totten
<g/>
.	.	kIx.	.
</s>
<s>
Hongkong	Hongkong	k1gInSc1	Hongkong
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Chinese	Chinese	k1gFnSc2	Chinese
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
962	[number]	k4	962
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MOTE	MOTE	kA	MOTE
<g/>
,	,	kIx,	,
Frederick	Frederick	k1gMnSc1	Frederick
W.	W.	kA	W.
Imperial	Imperial	k1gInSc1	Imperial
China	China	k1gFnSc1	China
900	[number]	k4	900
<g/>
-	-	kIx~	-
<g/>
1800	[number]	k4	1800
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
:	:	kIx,	:
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
1136	[number]	k4	1136
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
674	[number]	k4	674
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MOTE	MOTE	kA	MOTE
<g/>
,	,	kIx,	,
Frederick	Frederick	k1gMnSc1	Frederick
W.	W.	kA	W.
<g/>
;	;	kIx,	;
TWITCHETT	TWITCHETT	kA	TWITCHETT
<g/>
,	,	kIx,	,
Denis	Denisa	k1gFnPc2	Denisa
C.	C.	kA	C.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Cambridge	Cambridge	k1gFnSc2	Cambridge
History	Histor	k1gInPc4	Histor
of	of	k?	of
China	China	k1gFnSc1	China
<g/>
.	.	kIx.	.
</s>
<s>
Volume	volum	k1gInSc5	volum
7	[number]	k4	7
<g/>
:	:	kIx,	:
The	The	k1gFnSc6	The
Ming	Minga	k1gFnPc2	Minga
Dynasty	dynasta	k1gMnSc2	dynasta
<g/>
,	,	kIx,	,
1368	[number]	k4	1368
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
1008	[number]	k4	1008
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
521243327	[number]	k4	521243327
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TWITCHETT	TWITCHETT	kA	TWITCHETT
<g/>
,	,	kIx,	,
Denis	Denisa	k1gFnPc2	Denisa
C.	C.	kA	C.
<g/>
;	;	kIx,	;
MOTE	MOTE	kA	MOTE
<g/>
,	,	kIx,	,
Frederick	Frederick	k1gMnSc1	Frederick
W.	W.	kA	W.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Cambridge	Cambridge	k1gFnSc2	Cambridge
History	Histor	k1gInPc4	Histor
of	of	k?	of
China	China	k1gFnSc1	China
<g/>
.	.	kIx.	.
</s>
<s>
Volume	volum	k1gInSc5	volum
8	[number]	k4	8
<g/>
:	:	kIx,	:
The	The	k1gFnSc6	The
Ming	Minga	k1gFnPc2	Minga
Dynasty	dynasta	k1gMnSc2	dynasta
<g/>
,	,	kIx,	,
1368	[number]	k4	1368
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
1231	[number]	k4	1231
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
521243335	[number]	k4	521243335
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dynastie	dynastie	k1gFnSc2	dynastie
Ming	Ming	k1gInSc1	Ming
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
THEOBALD	THEOBALD	kA	THEOBALD
<g/>
,	,	kIx,	,
Ulrich	Ulrich	k1gMnSc1	Ulrich
<g/>
.	.	kIx.	.
</s>
<s>
Chinaknowledge	Chinaknowledgat	k5eAaPmIp3nS	Chinaknowledgat
-	-	kIx~	-
a	a	k8xC	a
universal	universat	k5eAaPmAgMnS	universat
guide	guide	k6eAd1	guide
for	forum	k1gNnPc2	forum
China	China	k1gFnSc1	China
studies	studies	k1gMnSc1	studies
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Chinese	Chinese	k1gFnSc2	Chinese
History	Histor	k1gInPc1	Histor
-	-	kIx~	-
Ming	Ming	k1gInSc1	Ming
Dynasty	dynasta	k1gMnSc2	dynasta
明	明	k?	明
(	(	kIx(	(
<g/>
1368	[number]	k4	1368
<g/>
-	-	kIx~	-
<g/>
1644	[number]	k4	1644
<g/>
)	)	kIx)	)
emperors	emperors	k6eAd1	emperors
and	and	k?	and
rulers	rulers	k1gInSc1	rulers
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
