<s>
Katrán	katrán	k1gInSc1	katrán
přímořský	přímořský	k2eAgInSc1d1	přímořský
(	(	kIx(	(
<g/>
Crambe	Cramb	k1gInSc5	Cramb
maritima	maritima	k1gNnSc4	maritima
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
halofilní	halofilní	k2eAgFnSc1d1	halofilní
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
druh	druh	k1gMnSc1	druh
rodu	rod	k1gInSc2	rod
katrán	katrán	k1gInSc1	katrán
z	z	k7c2	z
čeledě	čeleď	k1gFnSc2	čeleď
brukvovitých	brukvovitý	k2eAgMnPc2d1	brukvovitý
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
na	na	k7c6	na
mořských	mořský	k2eAgNnPc6d1	mořské
pobřežích	pobřeží	k1gNnPc6	pobřeží
kolem	kolem	k7c2	kolem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
u	u	k7c2	u
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
u	u	k7c2	u
Atlantiku	Atlantik	k1gInSc2	Atlantik
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
u	u	k7c2	u
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
neroste	růst	k5eNaImIp3nS	růst
však	však	k9	však
u	u	k7c2	u
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
oblázkových	oblázkový	k2eAgFnPc6d1	oblázková
nebo	nebo	k8xC	nebo
písčitých	písčitý	k2eAgFnPc6d1	písčitá
plážích	pláž	k1gFnPc6	pláž
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
která	který	k3yRgNnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
občas	občas	k6eAd1	občas
zaplavována	zaplavovat	k5eAaImNgFnS	zaplavovat
vodou	voda	k1gFnSc7	voda
přinášející	přinášející	k2eAgFnSc7d1	přinášející
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
živiny	živina	k1gFnSc2	živina
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
humusu	humus	k1gInSc2	humus
se	se	k3xPyFc4	se
zbytků	zbytek	k1gInPc2	zbytek
odumřelých	odumřelý	k2eAgFnPc2d1	odumřelá
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
schopnost	schopnost	k1gFnSc4	schopnost
přežít	přežít	k5eAaPmF	přežít
v	v	k7c6	v
silně	silně	k6eAd1	silně
zasolených	zasolený	k2eAgInPc6d1	zasolený
a	a	k8xC	a
v	v	k7c6	v
písčitých	písčitý	k2eAgInPc6d1	písčitý
nebo	nebo	k8xC	nebo
kamenitých	kamenitý	k2eAgInPc6d1	kamenitý
substrátech	substrát	k1gInPc6	substrát
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
rostlinném	rostlinný	k2eAgNnSc6d1	rostlinné
společenstvu	společenstvo	k1gNnSc6	společenstvo
mnoho	mnoho	k4c4	mnoho
konkurentů	konkurent	k1gMnPc2	konkurent
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
industrializaci	industrializace	k1gFnSc3	industrializace
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
,	,	kIx,	,
znečišťování	znečišťování	k1gNnSc1	znečišťování
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
rekreačních	rekreační	k2eAgFnPc2d1	rekreační
oblastí	oblast	k1gFnPc2	oblast
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
svých	svůj	k3xOyFgNnPc2	svůj
tradičních	tradiční	k2eAgNnPc2d1	tradiční
stanovišť	stanoviště	k1gNnPc2	stanoviště
vytratil	vytratit	k5eAaPmAgInS	vytratit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
stavy	stav	k1gInPc1	stav
klesají	klesat	k5eAaImIp3nP	klesat
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
jen	jen	k9	jen
na	na	k7c6	na
odlehlých	odlehlý	k2eAgNnPc6d1	odlehlé
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
rostlinou	rostlina	k1gFnSc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
bývá	bývat	k5eAaImIp3nS	bývat
tento	tento	k3xDgInSc4	tento
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
vyrůstající	vyrůstající	k2eAgInSc1d1	vyrůstající
nepůvodní	původní	k2eNgInSc1d1	nepůvodní
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
neofyt	neofyt	k1gInSc1	neofyt
<g/>
,	,	kIx,	,
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
druh	druh	k1gInSc4	druh
příležitostně	příležitostně	k6eAd1	příležitostně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
nebo	nebo	k8xC	nebo
za	za	k7c4	za
druh	druh	k1gInSc4	druh
naturalizovaný	naturalizovaný	k2eAgMnSc1d1	naturalizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Katrán	katrán	k1gInSc1	katrán
přímořský	přímořský	k2eAgInSc1d1	přímořský
je	být	k5eAaImIp3nS	být
vzpřímená	vzpřímený	k2eAgFnSc1d1	vzpřímená
<g/>
,	,	kIx,	,
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
lysá	lysý	k2eAgFnSc1d1	Lysá
rostlina	rostlina	k1gFnSc1	rostlina
nasivělé	nasivělý	k2eAgFnSc2d1	nasivělý
barvy	barva	k1gFnSc2	barva
vyrůstající	vyrůstající	k2eAgFnSc1d1	vyrůstající
s	s	k7c7	s
vícehlavého	vícehlavý	k2eAgInSc2d1	vícehlavý
kořene	kořen	k1gInSc2	kořen
řepovitého	řepovitý	k2eAgInSc2d1	řepovitý
tvaru	tvar	k1gInSc2	tvar
s	s	k7c7	s
tlustými	tlustý	k2eAgInPc7d1	tlustý
oddenky	oddenek	k1gInPc7	oddenek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
tuhé	tuhý	k2eAgFnPc1d1	tuhá
<g/>
,	,	kIx,	,
hranaté	hranatý	k2eAgFnPc1d1	hranatá
rozvětvené	rozvětvený	k2eAgFnPc1d1	rozvětvená
lodyhy	lodyha	k1gFnPc1	lodyha
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
30	[number]	k4	30
až	až	k9	až
60	[number]	k4	60
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Namodrale	namodrale	k6eAd1	namodrale
ojíněné	ojíněný	k2eAgInPc1d1	ojíněný
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
masité	masitý	k2eAgInPc1d1	masitý
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
dužnaté	dužnatý	k2eAgFnPc1d1	dužnatá
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgInPc1d1	spodní
listy	list	k1gInPc1	list
v	v	k7c6	v
růžici	růžice	k1gFnSc6	růžice
jsou	být	k5eAaImIp3nP	být
podlouhle	podlouhle	k6eAd1	podlouhle
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
nebo	nebo	k8xC	nebo
okrouhlé	okrouhlý	k2eAgFnPc1d1	okrouhlá
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
peřenolaločné	peřenolaločný	k2eAgFnPc1d1	peřenolaločný
<g/>
,	,	kIx,	,
rozdílně	rozdílně	k6eAd1	rozdílně
zubaté	zubatý	k2eAgFnPc1d1	zubatá
a	a	k8xC	a
zvlněné	zvlněný	k2eAgFnPc1d1	zvlněná
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zúžené	zúžený	k2eAgInPc1d1	zúžený
v	v	k7c4	v
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
řapík	řapík	k1gInSc4	řapík
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
10	[number]	k4	10
až	až	k9	až
40	[number]	k4	40
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
8	[number]	k4	8
až	až	k9	až
30	[number]	k4	30
cm	cm	kA	cm
široké	široký	k2eAgNnSc1d1	široké
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tupě	tupě	k6eAd1	tupě
zakončené	zakončený	k2eAgInPc4d1	zakončený
<g/>
.	.	kIx.	.
</s>
<s>
Lodyžní	lodyžní	k2eAgInPc1d1	lodyžní
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
oválné	oválný	k2eAgFnPc4d1	oválná
nebo	nebo	k8xC	nebo
kosočtverečné	kosočtverečný	k2eAgFnPc4d1	kosočtverečná
s	s	k7c7	s
klínovitou	klínovitý	k2eAgFnSc7d1	klínovitá
bázi	báze	k1gFnSc4	báze
<g/>
,	,	kIx,	,
6	[number]	k4	6
až	až	k9	až
8	[number]	k4	8
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
4	[number]	k4	4
až	až	k9	až
6	[number]	k4	6
cm	cm	kA	cm
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
začínají	začínat	k5eAaImIp3nP	začínat
kvést	kvést	k5eAaImF	kvést
a	a	k8xC	a
okolo	okolo	k7c2	okolo
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
mívá	mívat	k5eAaImIp3nS	mívat
rostlina	rostlina	k1gFnSc1	rostlina
i	i	k9	i
přes	přes	k7c4	přes
metr	metr	k1gInSc4	metr
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
na	na	k7c4	na
ní	on	k3xPp3gFnSc3	on
veliké	veliký	k2eAgNnSc1d1	veliké
množství	množství	k1gNnSc1	množství
malých	malý	k2eAgFnPc2d1	malá
<g/>
,	,	kIx,	,
sladce	sladko	k6eAd1	sladko
vonících	vonící	k2eAgInPc2d1	vonící
čtyřčetných	čtyřčetný	k2eAgInPc2d1	čtyřčetný
bílých	bílý	k2eAgInPc2d1	bílý
květů	květ	k1gInPc2	květ
sestavených	sestavený	k2eAgInPc2d1	sestavený
do	do	k7c2	do
mnohokvětých	mnohokvětý	k2eAgNnPc2d1	mnohokvěté
hroznovitých	hroznovitý	k2eAgNnPc2d1	hroznovité
květenství	květenství	k1gNnPc2	květenství
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
plátků	plátek	k1gInPc2	plátek
velkých	velký	k2eAgInPc2d1	velký
až	až	k6eAd1	až
4	[number]	k4	4
×	×	k?	×
3,5	[number]	k4	3,5
mm	mm	kA	mm
a	a	k8xC	a
koruna	koruna	k1gFnSc1	koruna
z	z	k7c2	z
plátků	plátek	k1gInPc2	plátek
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
asi	asi	k9	asi
10	[number]	k4	10
×	×	k?	×
6	[number]	k4	6
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
s	s	k7c7	s
prašníky	prašník	k1gInPc7	prašník
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
květu	květ	k1gInSc6	květ
šest	šest	k4xCc4	šest
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
kratší	krátký	k2eAgFnPc4d2	kratší
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
delší	dlouhý	k2eAgFnPc4d2	delší
<g/>
;	;	kIx,	;
dvoudílný	dvoudílný	k2eAgInSc4d1	dvoudílný
semeník	semeník	k1gInSc4	semeník
nese	nést	k5eAaImIp3nS	nést
pestík	pestík	k1gInSc1	pestík
s	s	k7c7	s
bliznou	blizna	k1gFnSc7	blizna
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
vykvétá	vykvétat	k5eAaImIp3nS	vykvétat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
opylovači	opylovač	k1gMnPc1	opylovač
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
květech	květ	k1gInPc6	květ
nektar	nektar	k1gInSc4	nektar
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
opyleny	opylit	k5eAaPmNgInP	opylit
cizím	cizí	k2eAgInSc7d1	cizí
i	i	k9	i
vlastním	vlastní	k2eAgInSc7d1	vlastní
pylem	pyl	k1gInSc7	pyl
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
stopkách	stopka	k1gFnPc6	stopka
vztyčené	vztyčený	k2eAgFnPc1d1	vztyčená
<g/>
,	,	kIx,	,
dvoudílné	dvoudílný	k2eAgFnPc1d1	dvoudílná
<g/>
,	,	kIx,	,
oválné	oválný	k2eAgFnPc1d1	oválná
nepukavé	pukavý	k2eNgFnPc1d1	nepukavá
šešulky	šešulka	k1gFnPc1	šešulka
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
drobnější	drobný	k2eAgFnSc1d2	drobnější
část	část	k1gFnSc1	část
jen	jen	k9	jen
asi	asi	k9	asi
3	[number]	k4	3
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
je	být	k5eAaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
až	až	k9	až
8	[number]	k4	8
mm	mm	kA	mm
velká	velká	k1gFnSc1	velká
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jedno	jeden	k4xCgNnSc1	jeden
semeno	semeno	k1gNnSc1	semeno
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
cca	cca	kA	cca
5	[number]	k4	5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
semeny	semeno	k1gNnPc7	semeno
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
v	v	k7c6	v
šešulkách	šešulka	k1gFnPc6	šešulka
dlouho	dlouho	k6eAd1	dlouho
plout	plout	k5eAaImF	plout
po	po	k7c6	po
vodní	vodní	k2eAgFnSc6d1	vodní
hladině	hladina	k1gFnSc6	hladina
nebo	nebo	k8xC	nebo
kousky	kousek	k1gInPc4	kousek
oddenků	oddenek	k1gInPc2	oddenek
odtrženými	odtržený	k2eAgInPc7d1	odtržený
vodními	vodní	k2eAgInPc7d1	vodní
proudy	proud	k1gInPc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
slanomilná	slanomilný	k2eAgFnSc1d1	slanomilná
rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
i	i	k9	i
lahůdkovou	lahůdkový	k2eAgFnSc7d1	lahůdková
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
pojmenování	pojmenování	k1gNnSc6	pojmenování
"	"	kIx"	"
<g/>
mořská	mořský	k2eAgFnSc1d1	mořská
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
mořské	mořský	k2eAgNnSc1d1	mořské
zelí	zelí	k1gNnSc1	zelí
<g/>
"	"	kIx"	"
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
anglicky	anglicky	k6eAd1	anglicky
sea	sea	k?	sea
kale	kale	k6eAd1	kale
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Meerkohl	Meerkohl	k1gFnSc1	Meerkohl
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
merikaali	merikaat	k5eAaBmAgMnP	merikaat
<g/>
,	,	kIx,	,
estonsky	estonsky	k6eAd1	estonsky
merekapsas	merekapsas	k1gMnSc1	merekapsas
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
časně	časně	k6eAd1	časně
z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
hlavně	hlavně	k9	hlavně
řapíky	řapík	k1gInPc4	řapík
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
rozvinutou	rozvinutý	k2eAgFnSc7d1	rozvinutá
listovou	listový	k2eAgFnSc7d1	listová
čepelí	čepel	k1gFnSc7	čepel
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
upravují	upravovat	k5eAaImIp3nP	upravovat
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
chřest	chřest	k1gInSc4	chřest
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
jsou	být	k5eAaImIp3nP	být
přirovnávány	přirovnáván	k2eAgFnPc4d1	přirovnávána
i	i	k8xC	i
chuťově	chuťově	k6eAd1	chuťově
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zeleninových	zeleninový	k2eAgInPc2d1	zeleninový
salátů	salát	k1gInPc2	salát
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
celé	celý	k2eAgInPc4d1	celý
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
několikanásobně	několikanásobně	k6eAd1	několikanásobně
více	hodně	k6eAd2	hodně
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
než	než	k8xS	než
citrusy	citrus	k1gInPc1	citrus
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
začíná	začínat	k5eAaImIp3nS	začínat
rostlina	rostlina	k1gFnSc1	rostlina
kvést	kvést	k5eAaImF	kvést
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
hořkou	hořký	k2eAgFnSc4d1	hořká
příchuť	příchuť	k1gFnSc4	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Úbytek	úbytek	k1gInSc1	úbytek
katránu	katrán	k1gInSc2	katrán
přímořského	přímořský	k2eAgInSc2d1	přímořský
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
areálů	areál	k1gInPc2	areál
okolo	okolo	k7c2	okolo
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
jihu	jih	k1gInSc3	jih
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
severu	sever	k1gInSc2	sever
Francie	Francie	k1gFnSc2	Francie
je	být	k5eAaImIp3nS	být
také	také	k9	také
spojován	spojovat	k5eAaImNgMnS	spojovat
i	i	k8xC	i
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
tradičním	tradiční	k2eAgInSc7d1	tradiční
sběrem	sběr	k1gInSc7	sběr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
zcela	zcela	k6eAd1	zcela
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
uměle	uměle	k6eAd1	uměle
z	z	k7c2	z
podzimního	podzimní	k2eAgInSc2d1	podzimní
výsevu	výsev	k1gInSc2	výsev
semen	semeno	k1gNnPc2	semeno
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jarní	jarní	k2eAgFnSc2d1	jarní
výsadby	výsadba	k1gFnSc2	výsadba
řízků	řízek	k1gInPc2	řízek
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
oddenků	oddenek	k1gInPc2	oddenek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zdárný	zdárný	k2eAgInSc4d1	zdárný
vývoj	vývoj	k1gInSc4	vývoj
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
kompostem	kompost	k1gInSc7	kompost
dostatečně	dostatečně	k6eAd1	dostatečně
vyhnojenou	vyhnojený	k2eAgFnSc4d1	vyhnojená
písčitou	písčitý	k2eAgFnSc4d1	písčitá
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
rostliny	rostlina	k1gFnPc1	rostlina
lépe	dobře	k6eAd2	dobře
přezimovaly	přezimovat	k5eAaBmAgFnP	přezimovat
při	při	k7c6	při
silných	silný	k2eAgInPc6d1	silný
vnitrozemských	vnitrozemský	k2eAgInPc6d1	vnitrozemský
mrazech	mráz	k1gInPc6	mráz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
zakrýt	zakrýt	k5eAaPmF	zakrýt
chvojím	chvojí	k1gNnSc7	chvojí
<g/>
,	,	kIx,	,
slámou	sláma	k1gFnSc7	sláma
nebo	nebo	k8xC	nebo
listím	listí	k1gNnSc7	listí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
včasnou	včasný	k2eAgFnSc4d1	včasná
jarní	jarní	k2eAgFnSc4d1	jarní
produkci	produkce	k1gFnSc4	produkce
lze	lze	k6eAd1	lze
rostliny	rostlina	k1gFnSc2	rostlina
i	i	k8xC	i
rychlit	rychlit	k5eAaImF	rychlit
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Katrán	katrán	k1gInSc1	katrán
přímořský	přímořský	k2eAgMnSc1d1	přímořský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Recepty	recept	k1gInPc1	recept
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
katránu	katrán	k1gInSc2	katrán
přímořského	přímořský	k2eAgInSc2d1	přímořský
</s>
