<s>
Katrán	katrán	k1gInSc1	katrán
přímořský	přímořský	k2eAgInSc1d1	přímořský
(	(	kIx(	(
<g/>
Crambe	Cramb	k1gInSc5	Cramb
maritima	maritima	k1gNnSc4	maritima
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
halofilní	halofilní	k2eAgFnSc1d1	halofilní
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
druh	druh	k1gMnSc1	druh
rodu	rod	k1gInSc2	rod
katrán	katrán	k1gInSc1	katrán
z	z	k7c2	z
čeledě	čeleď	k1gFnSc2	čeleď
brukvovitých	brukvovitý	k2eAgMnPc2d1	brukvovitý
<g/>
.	.	kIx.	.
</s>
