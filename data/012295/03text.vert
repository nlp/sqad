<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Kmeť	Kmeť	k1gFnSc1	Kmeť
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
Separ	Separa	k1gFnPc2	Separa
a	a	k8xC	a
nebo	nebo	k8xC	nebo
Monsignor	Monsignor	k1gMnSc1	Monsignor
Separ	Separ	k1gMnSc1	Separ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
raper	raper	k1gMnSc1	raper
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
bratislavské	bratislavský	k2eAgFnSc2d1	Bratislavská
hip-hopové	hipopový	k2eAgFnSc2d1	hip-hopová
skupiny	skupina	k1gFnSc2	skupina
DMS	DMS	kA	DMS
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
nezávislého	závislý	k2eNgInSc2d1	nezávislý
labelu	label	k1gInSc2	label
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc4	Rokkaz
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
víceméně	víceméně	k9	víceméně
aktivně	aktivně	k6eAd1	aktivně
věnoval	věnovat	k5eAaPmAgMnS	věnovat
malování	malování	k1gNnSc4	malování
grafitů	grafit	k1gInPc2	grafit
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
s	s	k7c7	s
rapem	rape	k1gNnSc7	rape
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
DMS	DMS	kA	DMS
vydal	vydat	k5eAaPmAgInS	vydat
jedno	jeden	k4xCgNnSc1	jeden
EP	EP	kA	EP
(	(	kIx(	(
<g/>
Teraz	Teraz	k1gInSc1	Teraz
už	už	k6eAd1	už
naozaj	naozaj	k1gInSc1	naozaj
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgNnPc1	tři
alba	album	k1gNnPc1	album
(	(	kIx(	(
<g/>
Čo	Čo	k1gFnSc2	Čo
sa	sa	k?	sa
stalo	stát	k5eAaPmAgNnS	stát
<g/>
?!	?!	k?	?!
,	,	kIx,	,
MMXV	MMXV	kA	MMXV
a	a	k8xC	a
Prepáčte	Prepáčte	k1gFnSc2	Prepáčte
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
sólový	sólový	k2eAgMnSc1d1	sólový
interpret	interpret	k1gMnSc1	interpret
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Buldozér	Buldozér	k1gInSc4	Buldozér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2014	[number]	k4	2014
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jeho	jeho	k3xOp3gNnSc1	jeho
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Pirát	pirát	k1gMnSc1	pirát
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vydal	vydat	k5eAaPmAgInS	vydat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
PANCIER	PANCIER	kA	PANCIER
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvulgárnějších	vulgární	k2eAgMnPc2d3	nejvulgárnější
a	a	k8xC	a
nejpřímočařejších	přímočarý	k2eAgMnPc2d3	nejpřímočařejší
raperů	raper	k1gMnPc2	raper
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
hip-hopové	hipopový	k2eAgFnSc6d1	hip-hopová
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
je	být	k5eAaImIp3nS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Tina	Tina	k1gFnSc1	Tina
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Csillaghová	Csillaghová	k1gFnSc1	Csillaghová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jméno	jméno	k1gNnSc1	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
jménu	jméno	k1gNnSc3	jméno
Separ	Separa	k1gFnPc2	Separa
se	se	k3xPyFc4	se
Michal	Michal	k1gMnSc1	Michal
dostal	dostat	k5eAaPmAgMnS	dostat
přes	přes	k7c4	přes
graffiti	graffiti	k1gNnSc4	graffiti
<g/>
,	,	kIx,	,
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
jako	jako	k9	jako
Sort	sorta	k1gFnPc2	sorta
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Žiliny	Žilina	k1gFnSc2	Žilina
existovala	existovat	k5eAaImAgFnS	existovat
skupina	skupina	k1gFnSc1	skupina
"	"	kIx"	"
<g/>
Sorta	sorta	k1gFnSc1	sorta
<g/>
"	"	kIx"	"
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
přátel	přítel	k1gMnPc2	přítel
začal	začít	k5eAaPmAgMnS	začít
měnit	měnit	k5eAaImF	měnit
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podmínka	podmínka	k1gFnSc1	podmínka
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
počáteční	počáteční	k2eAgFnSc1d1	počáteční
nebo	nebo	k8xC	nebo
koncové	koncový	k2eAgFnPc1d1	koncová
písmena	písmeno	k1gNnPc4	písmeno
byla	být	k5eAaImAgFnS	být
R	R	kA	R
<g/>
,	,	kIx,	,
K	K	kA	K
a	a	k8xC	a
nebo	nebo	k8xC	nebo
S.	S.	kA	S.
Postupně	postupně	k6eAd1	postupně
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
dají	dát	k5eAaPmIp3nP	dát
písmeno	písmeno	k1gNnSc4	písmeno
S	s	k7c7	s
a	a	k8xC	a
na	na	k7c4	na
konec	konec	k1gInSc4	konec
dali	dát	k5eAaPmAgMnP	dát
nejprve	nejprve	k6eAd1	nejprve
K	k	k7c3	k
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
později	pozdě	k6eAd2	pozdě
změnili	změnit	k5eAaPmAgMnP	změnit
na	na	k7c6	na
R	R	kA	R
a	a	k8xC	a
doprostřed	doprostřed	k6eAd1	doprostřed
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
písmeno	písmeno	k1gNnSc4	písmeno
P	P	kA	P
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dost	dost	k6eAd1	dost
úzké	úzký	k2eAgNnSc1d1	úzké
a	a	k8xC	a
vysoké	vysoký	k2eAgNnSc1d1	vysoké
<g/>
.	.	kIx.	.
</s>
<s>
Dosazením	dosazení	k1gNnSc7	dosazení
samohlásek	samohláska	k1gFnPc2	samohláska
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Separ	Separ	k1gInSc4	Separ
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
přezdívka	přezdívka	k1gFnSc1	přezdívka
mu	on	k3xPp3gMnSc3	on
seděla	sedět	k5eAaImAgFnS	sedět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
často	často	k6eAd1	často
separoval	separovat	k5eAaBmAgInS	separovat
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
a	a	k8xC	a
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
nezapadal	zapadat	k5eNaPmAgInS	zapadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Separ	Separ	k1gInSc1	Separ
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
aktivní	aktivní	k2eAgFnSc6d1	aktivní
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
Damem	Dam	k1gInSc7	Dam
doma	doma	k6eAd1	doma
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
napsané	napsaný	k2eAgInPc4d1	napsaný
texty	text	k1gInPc4	text
na	na	k7c4	na
kazety	kazeta	k1gFnPc4	kazeta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
jako	jako	k9	jako
skupina	skupina	k1gFnSc1	skupina
2	[number]	k4	2
<g/>
kusy	kus	k1gInPc1	kus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
bratislavskou	bratislavský	k2eAgFnSc4d1	Bratislavská
městskou	městský	k2eAgFnSc4d1	městská
část	část	k1gFnSc4	část
Dúbravka	Dúbravka	k1gFnSc1	Dúbravka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
skupina	skupina	k1gFnSc1	skupina
DWS	DWS	kA	DWS
(	(	kIx(	(
<g/>
Dame	Dame	k1gFnSc1	Dame
<g/>
,	,	kIx,	,
Wrana	Wrana	k1gFnSc1	Wrana
<g/>
,	,	kIx,	,
Separ	Separ	k1gInSc1	Separ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
seskupení	seskupení	k1gNnSc1	seskupení
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
přes	přes	k7c4	přes
50	[number]	k4	50
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
užívalo	užívat	k5eAaImAgNnS	užívat
si	se	k3xPyFc3	se
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
menších	malý	k2eAgFnPc6d2	menší
neshodách	neshoda	k1gFnPc6	neshoda
se	se	k3xPyFc4	se
ale	ale	k9	ale
skupina	skupina	k1gFnSc1	skupina
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozpad	rozpad	k1gInSc1	rozpad
způsobil	způsobit	k5eAaPmAgInS	způsobit
roční	roční	k2eAgFnSc4d1	roční
pauzu	pauza	k1gFnSc4	pauza
raperů	raper	k1gInPc2	raper
a	a	k8xC	a
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
přišli	přijít	k5eAaPmAgMnP	přijít
až	až	k9	až
s	s	k7c7	s
prvním	první	k4xOgMnSc7	první
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
albem	album	k1gNnSc7	album
Teraz	Teraz	k1gInSc1	Teraz
už	už	k9	už
naozaj	naozaj	k1gInSc1	naozaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nepodléhal	podléhat	k5eNaImAgInS	podléhat
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
ba	ba	k9	ba
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
EP	EP	kA	EP
má	mít	k5eAaImIp3nS	mít
podíl	podíl	k1gInSc4	podíl
i	i	k9	i
producent	producent	k1gMnSc1	producent
Smart	Smarta	k1gFnPc2	Smarta
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
nazvala	nazvat	k5eAaBmAgFnS	nazvat
DMS	DMS	kA	DMS
(	(	kIx(	(
<g/>
Dame	Dame	k1gFnSc1	Dame
<g/>
,	,	kIx,	,
Metys	Metys	k1gInSc1	Metys
<g/>
,	,	kIx,	,
Separ	Separ	k1gInSc1	Separ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešel	odejít	k5eAaPmAgInS	odejít
DJ	DJ	kA	DJ
Metys	Metys	k1gInSc4	Metys
a	a	k8xC	a
přidal	přidat	k5eAaPmAgInS	přidat
se	se	k3xPyFc4	se
Smart	Smart	k1gInSc1	Smart
(	(	kIx(	(
<g/>
Dame	Dame	k1gFnSc1	Dame
<g/>
,	,	kIx,	,
Monsignor	Monsignor	k1gMnSc1	Monsignor
Separ	Separ	k1gMnSc1	Separ
<g/>
,	,	kIx,	,
Smart	Smart	k1gInSc1	Smart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
postupně	postupně	k6eAd1	postupně
vzniká	vznikat	k5eAaImIp3nS	vznikat
hudební	hudební	k2eAgNnSc4d1	hudební
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
skupina	skupina	k1gFnSc1	skupina
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc4	Rokkaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
skupina	skupina	k1gFnSc1	skupina
DMS	DMS	kA	DMS
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Separem	Separ	k1gInSc7	Separ
jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
skupiny	skupina	k1gFnSc2	skupina
DMS	DMS	kA	DMS
patřili	patřit	k5eAaImAgMnP	patřit
do	do	k7c2	do
Grama	Gramum	k1gNnSc2	Gramum
i	i	k8xC	i
skupina	skupina	k1gFnSc1	skupina
Mater	Matra	k1gFnPc2	Matra
<g/>
,	,	kIx,	,
rapeři	raper	k1gMnPc1	raper
Decko	Decko	k1gNnSc4	Decko
a	a	k8xC	a
Danosť	Danosť	k1gMnPc1	Danosť
<g/>
,	,	kIx,	,
producenti	producent	k1gMnPc1	producent
a	a	k8xC	a
DJové	DJová	k1gFnPc1	DJová
Dekan	dekan	k1gInSc1	dekan
<g/>
,	,	kIx,	,
Deryck	Deryck	k1gMnSc1	Deryck
<g/>
,	,	kIx,	,
Analytik	analytik	k1gMnSc1	analytik
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Lkama	Lkama	k1gNnSc1	Lkama
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Metys	Metys	k1gInSc1	Metys
a	a	k8xC	a
DJ	DJ	kA	DJ
Miko	Miko	k6eAd1	Miko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
tato	tento	k3xDgFnSc1	tento
sestava	sestava	k1gFnSc1	sestava
vydává	vydávat	k5eAaImIp3nS	vydávat
první	první	k4xOgInSc4	první
sampler	sampler	k1gInSc4	sampler
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
s	s	k7c7	s
názvem	název	k1gInSc7	název
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
<g/>
Manifest	manifest	k1gInSc1	manifest
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
16	[number]	k4	16
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Separ	Separ	k1gInSc1	Separ
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
šesti	šest	k4xCc6	šest
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
už	už	k6eAd1	už
skupina	skupina	k1gFnSc1	skupina
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
GR	GR	kA	GR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Debutové	debutový	k2eAgNnSc4d1	debutové
sólo	sólo	k2eAgNnSc4d1	sólo
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
DMS	DMS	kA	DMS
"	"	kIx"	"
<g/>
Čo	Čo	k1gFnPc3	Čo
sa	sa	k?	sa
stalo	stát	k5eAaPmAgNnS	stát
<g/>
?!	?!	k?	?!
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
vydávají	vydávat	k5eAaPmIp3nP	vydávat
DMS	DMS	kA	DMS
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Čo	Čo	k1gFnSc2	Čo
sa	sa	k?	sa
stalo	stát	k5eAaPmAgNnS	stát
<g/>
?!	?!	k?	?!
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
má	mít	k5eAaImIp3nS	mít
16	[number]	k4	16
zvukových	zvukový	k2eAgFnPc2d1	zvuková
stop	stopa	k1gFnPc2	stopa
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
Separ	Separ	k1gInSc1	Separ
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
nahrané	nahraný	k2eAgNnSc1d1	nahrané
ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
BMT	BMT	kA	BMT
a	a	k8xC	a
Anapol	Anapol	k1gInSc1	Anapol
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Separa	Separ	k1gMnSc2	Separ
<g/>
,	,	kIx,	,
G-Boda	G-Bod	k1gMnSc2	G-Bod
a	a	k8xC	a
Danosťa	Danosťus	k1gMnSc2	Danosťus
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
hosté	host	k1gMnPc1	host
jako	jako	k8xS	jako
Čistychov	Čistychov	k1gInSc1	Čistychov
<g/>
,	,	kIx,	,
DNA	DNA	kA	DNA
nebo	nebo	k8xC	nebo
Momo	Momo	k6eAd1	Momo
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
kluci	kluk	k1gMnPc1	kluk
z	z	k7c2	z
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc4	Rokkaz
<g/>
.	.	kIx.	.
</s>
<s>
Nejzajímavější	zajímavý	k2eAgFnSc1d3	nejzajímavější
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Termanologym	Termanologym	k1gInSc1	Termanologym
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
Bostonu	Boston	k1gInSc2	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
feat	feat	k1gInSc1	feat
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
kempů	kemp	k1gInPc2	kemp
dali	dát	k5eAaPmAgMnP	dát
do	do	k7c2	do
řeči	řeč	k1gFnSc2	řeč
právě	právě	k6eAd1	právě
zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
Termanology	Termanolog	k1gMnPc4	Termanolog
s	s	k7c7	s
Tonom	Tonom	k1gInSc4	Tonom
S.	S.	kA	S.
a	a	k8xC	a
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
se	se	k3xPyFc4	se
na	na	k7c6	na
nějakém	nějaký	k3yIgInSc6	nějaký
společném	společný	k2eAgInSc6d1	společný
projektu	projekt	k1gInSc6	projekt
Grama	Grama	k1gFnSc1	Grama
Rokkaz	Rokkaz	k1gInSc1	Rokkaz
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
americkým	americký	k2eAgInSc7d1	americký
raperem	raper	k1gInSc7	raper
<g/>
.	.	kIx.	.
</s>
<s>
Separ	Separ	k1gInSc1	Separ
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
mail	mail	k1gInSc4	mail
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
nahrávka	nahrávka	k1gFnSc1	nahrávka
Žijem	Žijem	k?	Žijem
svoj	svojit	k5eAaImRp2nS	svojit
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
natočený	natočený	k2eAgInSc1d1	natočený
i	i	k8xC	i
klip	klip	k1gInSc1	klip
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
i	i	k9	i
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
postaraly	postarat	k5eAaPmAgFnP	postarat
Mona	Mona	k1gFnSc1	Mona
a	a	k8xC	a
Beri	Beri	k1gNnSc1	Beri
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
debutové	debutový	k2eAgNnSc1d1	debutové
sólo	sólo	k1gNnSc1	sólo
album	album	k1gNnSc1	album
===	===	k?	===
</s>
</p>
<p>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
sólo	sólo	k2eAgNnSc1d1	sólo
album	album	k1gNnSc1	album
Buldozér	Buldozér	k1gInSc4	Buldozér
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
pod	pod	k7c4	pod
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc1	Rokkaz
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
jako	jako	k8xC	jako
CD	CD	kA	CD
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
i	i	k9	i
proto	proto	k8xC	proto
nazval	nazvat	k5eAaBmAgInS	nazvat
Buldozér	Buldozér	k1gInSc1	Buldozér
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
připomíná	připomínat	k5eAaImIp3nS	připomínat
něco	něco	k3yInSc1	něco
co	co	k3yInSc4	co
převálcuje	převálcovat	k5eAaPmIp3nS	převálcovat
ostatní	ostatní	k2eAgMnSc1d1	ostatní
a	a	k8xC	a
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
Buldozér	Buldozér	k1gInSc1	Buldozér
v	v	k7c6	v
hube	hube	k1gFnSc6	hube
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
rapuje	rapovat	k5eAaImIp3nS	rapovat
dost	dost	k6eAd1	dost
vulgárně	vulgárně	k6eAd1	vulgárně
a	a	k8xC	a
ostře	ostro	k6eAd1	ostro
-	-	kIx~	-
před	před	k7c4	před
ústa	ústa	k1gNnPc4	ústa
si	se	k3xPyFc3	se
servítku	servítek	k1gInSc2	servítek
nedává	dávat	k5eNaImIp3nS	dávat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
sólo	sólo	k2eAgNnSc2d1	sólo
alba	album	k1gNnSc2	album
přispěl	přispět	k5eAaPmAgInS	přispět
nejen	nejen	k6eAd1	nejen
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc1	Rokkaz
jako	jako	k8xS	jako
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hodně	hodně	k6eAd1	hodně
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
mají	mít	k5eAaImIp3nP	mít
málo	málo	k1gNnSc4	málo
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
tak	tak	k9	tak
asi	asi	k9	asi
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
roku	rok	k1gInSc2	rok
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
a	a	k8xC	a
s	s	k7c7	s
psaním	psaní	k1gNnSc7	psaní
textů	text	k1gInPc2	text
na	na	k7c4	na
album	album	k1gNnSc4	album
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
18	[number]	k4	18
skladebs	skladebsa	k1gFnPc2	skladebsa
více	hodně	k6eAd2	hodně
hosty	host	k1gMnPc7	host
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yQgInPc7	který
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
členové	člen	k1gMnPc1	člen
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc1	Rokkaz
<g/>
,	,	kIx,	,
Čistychov	Čistychov	k1gInSc1	Čistychov
<g/>
,	,	kIx,	,
Momo	Moma	k1gFnSc5	Moma
<g/>
,	,	kIx,	,
Strapo	Strapa	k1gFnSc5	Strapa
<g/>
,	,	kIx,	,
Rest	rest	k6eAd1	rest
(	(	kIx(	(
<g/>
raper	raper	k1gInSc1	raper
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mišo	Mišo	k1gMnSc1	Mišo
Biely	Biela	k1gFnSc2	Biela
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Botló	Botló	k1gMnSc1	Botló
anebo	anebo	k8xC	anebo
Layla	Layla	k1gMnSc1	Layla
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
hudební	hudební	k2eAgInPc4d1	hudební
podklady	podklad	k1gInPc4	podklad
se	se	k3xPyFc4	se
postarala	postarat	k5eAaPmAgFnS	postarat
stejně	stejně	k6eAd1	stejně
pestrá	pestrý	k2eAgFnSc1d1	pestrá
sestava	sestava	k1gFnSc1	sestava
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiná	k1gFnPc7	jiná
i	i	k9	i
Smat	Smat	k1gMnSc1	Smat
<g/>
,	,	kIx,	,
Deryck	Deryck	k1gMnSc1	Deryck
<g/>
,	,	kIx,	,
Lkhama	Lkhama	k1gFnSc1	Lkhama
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Fatte	Fatt	k1gInSc5	Fatt
<g/>
,	,	kIx,	,
Emeres	Emeresa	k1gFnPc2	Emeresa
anebo	anebo	k8xC	anebo
G-Bod	G-Boda	k1gFnPc2	G-Boda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
převažují	převažovat	k5eAaImIp3nP	převažovat
hlavně	hlavně	k9	hlavně
sociální	sociální	k2eAgNnPc1d1	sociální
témata	téma	k1gNnPc1	téma
o	o	k7c6	o
světě	svět	k1gInSc6	svět
okolo	okolo	k7c2	okolo
Separa	Separ	k1gMnSc2	Separ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odtrhnutí	odtrhnutí	k1gNnPc4	odtrhnutí
od	od	k7c2	od
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc1	Rokkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Separ	Separ	k1gMnSc1	Separ
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
své	své	k1gNnSc4	své
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
měnit	měnit	k5eAaImF	měnit
postup	postup	k1gInSc4	postup
styl	styl	k1gInSc1	styl
rapování	rapování	k1gNnSc1	rapování
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
rapu	rapa	k1gFnSc4	rapa
(	(	kIx(	(
<g/>
90	[number]	k4	90
bpm	bpm	k?	bpm
<g/>
)	)	kIx)	)
do	do	k7c2	do
trapu	trap	k1gInSc2	trap
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
začali	začít	k5eAaPmAgMnP	začít
podporovat	podporovat	k5eAaImF	podporovat
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
kolegové	kolega	k1gMnPc1	kolega
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
DMS	DMS	kA	DMS
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
z	z	k7c2	z
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc1	Rokkaz
chtěli	chtít	k5eAaImAgMnP	chtít
zůstal	zůstat	k5eAaPmAgInS	zůstat
při	při	k7c6	při
boombapu	boombap	k1gInSc6	boombap
a	a	k8xC	a
nebo	nebo	k8xC	nebo
klasické	klasický	k2eAgFnSc2d1	klasická
90	[number]	k4	90
<g/>
bmp	bmp	k?	bmp
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
v	v	k7c6	v
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc1	Rokkaz
setrvávali	setrvávat	k5eAaImAgMnP	setrvávat
2	[number]	k4	2
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Kluci	kluk	k1gMnPc1	kluk
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozejdou	rozejít	k5eAaPmIp3nP	rozejít
v	v	k7c6	v
dobrém	dobré	k1gNnSc6	dobré
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
teda	teda	k?	teda
skupina	skupina	k1gFnSc1	skupina
DMS	DMS	kA	DMS
odloučí	odloučit	k5eAaPmIp3nS	odloučit
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc4	Rokkaz
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc4	ten
Separ	Separ	k1gMnSc1	Separ
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
oficiálním	oficiální	k2eAgInSc6d1	oficiální
facebookovém	facebookový	k2eAgInSc6d1	facebookový
profilu	profil	k1gInSc6	profil
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Separ	Separ	k1gInSc1	Separ
s	s	k7c7	s
Damem	Damum	k1gNnSc7	Damum
vydali	vydat	k5eAaPmAgMnP	vydat
trapovou	trapový	k2eAgFnSc4d1	trapový
skladbu	skladba	k1gFnSc4	skladba
produkovanou	produkovaný	k2eAgFnSc4d1	produkovaná
Smartem	Smart	k1gInSc7	Smart
Vitaj	Vitaj	k1gInSc4	Vitaj
vo	vo	k?	vo
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yRgFnSc6	který
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
od	od	k7c2	od
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc1	Rokkaz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
Separově	Separův	k2eAgNnSc6d1	Separův
albu	album	k1gNnSc6	album
Pirát	pirát	k1gMnSc1	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
se	se	k3xPyFc4	se
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc4	Rokkaz
přestalo	přestat	k5eAaPmAgNnS	přestat
nazývat	nazývat	k5eAaImF	nazývat
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
figurovalo	figurovat	k5eAaImAgNnS	figurovat
jako	jako	k8xS	jako
hudební	hudební	k2eAgNnSc4d1	hudební
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zůstali	zůstat	k5eAaPmAgMnP	zůstat
po	po	k7c6	po
oddělení	oddělení	k1gNnSc6	oddělení
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
DMS	DMS	kA	DMS
si	se	k3xPyFc3	se
založili	založit	k5eAaPmAgMnP	založit
rapové	rap	k1gMnPc1	rap
uskupení	uskupení	k1gNnSc2	uskupení
GR	GR	kA	GR
Team	team	k1gInSc1	team
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
patří	patřit	k5eAaImIp3nS	patřit
Decko	Decko	k1gNnSc1	Decko
<g/>
,	,	kIx,	,
Tono	Tono	k1gNnSc1	Tono
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Danosť	Danosť	k1gMnSc1	Danosť
a	a	k8xC	a
Rebel	rebel	k1gMnSc1	rebel
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Separ	Separ	k1gMnSc1	Separ
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
i	i	k8xC	i
GR	GR	kA	GR
Team	team	k1gInSc4	team
pracovalo	pracovat	k5eAaImAgNnS	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
vlastním	vlastní	k2eAgNnSc6d1	vlastní
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
1	[number]	k4	1
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
Separ	Separ	k1gMnSc1	Separ
hostuje	hostovat	k5eAaImIp3nS	hostovat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
věci	věc	k1gFnSc6	věc
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
udělali	udělat	k5eAaPmAgMnP	udělat
ještě	ještě	k9	ještě
před	před	k7c7	před
rozpadem	rozpad	k1gInSc7	rozpad
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
skladbě	skladba	k1gFnSc6	skladba
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
celá	celý	k2eAgFnSc1d1	celá
bývalá	bývalý	k2eAgFnSc1d1	bývalá
sestava	sestava	k1gFnSc1	sestava
Gramo	Grama	k1gFnSc5	Grama
Rokkaz	Rokkaz	k1gInSc1	Rokkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pod	pod	k7c7	pod
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
DMS	DMS	kA	DMS
Records	Recordsa	k1gFnPc2	Recordsa
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
odehrál	odehrát	k5eAaPmAgInS	odehrát
Separ	Separ	k1gInSc4	Separ
více	hodně	k6eAd2	hodně
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
nahrál	nahrát	k5eAaBmAgInS	nahrát
nějaké	nějaký	k3yIgInPc4	nějaký
videoklipy	videoklip	k1gInPc4	videoklip
a	a	k8xC	a
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
mnoho	mnoho	k4c4	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
hlavně	hlavně	k6eAd1	hlavně
díky	díky	k7c3	díky
hostování	hostování	k1gNnSc3	hostování
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
skladbách	skladba	k1gFnPc6	skladba
od	od	k7c2	od
Rytmuse	Rytmuse	k1gFnSc2	Rytmuse
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
skladba	skladba	k1gFnSc1	skladba
Škola	škola	k1gFnSc1	škola
rapu	rapa	k1gFnSc4	rapa
je	být	k5eAaImIp3nS	být
odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
Rakbyho	Rakby	k1gMnSc4	Rakby
diss	diss	k6eAd1	diss
na	na	k7c6	na
Rytmuse	Rytmus	k1gInSc6	Rytmus
Čau	čau	k0	čau
Paťo	Paťo	k6eAd1	Paťo
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Rytmusem	Rytmus	k1gInSc7	Rytmus
V	v	k7c4	v
mojom	mojom	k1gInSc4	mojom
svetě	sveť	k1gFnSc2	sveť
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zahrnutá	zahrnutý	k2eAgFnSc1d1	zahrnutá
v	v	k7c6	v
albu	album	k1gNnSc6	album
Navždy	navždy	k6eAd1	navždy
od	od	k7c2	od
Kontrafaktu	Kontrafakt	k1gInSc2	Kontrafakt
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Rytmuse	Rytmuse	k1gFnSc2	Rytmuse
<g/>
,	,	kIx,	,
Ega	ego	k1gNnSc2	ego
a	a	k8xC	a
Separa	Separa	k1gFnSc1	Separa
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
skladbě	skladba	k1gFnSc6	skladba
i	i	k8xC	i
český	český	k2eAgMnSc1d1	český
zástupce	zástupce	k1gMnSc1	zástupce
Ektor	Ektor	k1gMnSc1	Ektor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
fakta	faktum	k1gNnPc4	faktum
zpopularizovali	zpopularizovat	k5eAaPmAgMnP	zpopularizovat
Separa	Separ	k1gMnSc4	Separ
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
přičinil	přičinit	k5eAaPmAgInS	přičinit
vydáním	vydání	k1gNnSc7	vydání
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhého	druhý	k4xOgNnSc2	druhý
sólového	sólový	k2eAgNnSc2d1	sólové
alba	album	k1gNnSc2	album
Pirát	pirát	k1gMnSc1	pirát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Album	album	k1gNnSc1	album
Pirát	pirát	k1gMnSc1	pirát
====	====	k?	====
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
sólo	sólo	k2eAgNnSc1d1	sólo
album	album	k1gNnSc1	album
Separa	Separ	k1gMnSc2	Separ
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
pod	pod	k7c7	pod
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
DMS	DMS	kA	DMS
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
taktéž	taktéž	k?	taktéž
18	[number]	k4	18
skladeb	skladba	k1gFnPc2	skladba
-	-	kIx~	-
tak	tak	k9	tak
jako	jako	k9	jako
na	na	k7c6	na
předešlém	předešlý	k2eAgNnSc6d1	předešlé
albu	album	k1gNnSc6	album
Buldozér	Buldozér	k1gMnSc1	Buldozér
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
hosty	host	k1gMnPc7	host
patří	patřit	k5eAaImIp3nS	patřit
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
i	i	k8xC	i
Strapo	Strapa	k1gFnSc5	Strapa
<g/>
,	,	kIx,	,
Momo	Momo	k1gMnSc1	Momo
<g/>
,	,	kIx,	,
Čistychov	Čistychov	k1gInSc1	Čistychov
či	či	k8xC	či
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
skupiny	skupina	k1gFnSc2	skupina
PSH	PSH	kA	PSH
a	a	k8xC	a
slohem	sloh	k1gInSc7	sloh
dokonce	dokonce	k9	dokonce
přispěl	přispět	k5eAaPmAgMnS	přispět
i	i	k9	i
Rakaa	Rakaa	k1gMnSc1	Rakaa
Iriscience	Iriscienec	k1gInSc2	Iriscienec
ze	z	k7c2	z
známé	známý	k2eAgFnSc2d1	známá
skupiny	skupina	k1gFnSc2	skupina
americké	americký	k2eAgFnSc2d1	americká
hip-hopové	hipopový	k2eAgFnSc2d1	hip-hopová
formace	formace	k1gFnSc2	formace
Dilated	Dilated	k1gMnSc1	Dilated
Peoples	Peoples	k1gMnSc1	Peoples
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
stránce	stránka	k1gFnSc6	stránka
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
objevili	objevit	k5eAaPmAgMnP	objevit
známé	známá	k1gFnPc4	známá
jména	jméno	k1gNnSc2	jméno
jako	jako	k8xS	jako
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
<g/>
,	,	kIx,	,
Grimaso	grimasa	k1gFnSc5	grimasa
<g/>
,	,	kIx,	,
Smart	Smart	k1gInSc1	Smart
<g/>
,	,	kIx,	,
Lkama	Lkama	k1gFnSc1	Lkama
či	či	k8xC	či
Eremes	Eremes	k1gInSc1	Eremes
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
přšedcházející	přšedcházející	k2eAgNnSc4d1	přšedcházející
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
nachází	nacházet	k5eAaImIp3nS	nacházet
skladby	skladba	k1gFnSc2	skladba
Hejtklub	Hejtklub	k1gInSc4	Hejtklub
3	[number]	k4	3
a	a	k8xC	a
Hejtklub	Hejtklub	k1gInSc1	Hejtklub
4	[number]	k4	4
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
pokračováním	pokračování	k1gNnSc7	pokračování
skladeb	skladba	k1gFnPc2	skladba
Hejtklub	Hejtklub	k1gInSc4	Hejtklub
rýchlo	rýchnout	k5eAaPmAgNnS	rýchnout
a	a	k8xC	a
Hejtklub	Hejtklub	k1gMnSc1	Hejtklub
pomaly	pomaly	k?	pomaly
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
písniček	písnička	k1gFnPc2	písnička
nese	nést	k5eAaImIp3nS	nést
svou	svůj	k3xOyFgFnSc4	svůj
určitou	určitý	k2eAgFnSc4d1	určitá
tématiku	tématika	k1gFnSc4	tématika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
i	i	k9	i
skladby	skladba	k1gFnPc1	skladba
jako	jako	k8xS	jako
Intro	Intro	k1gNnSc1	Intro
a	a	k8xC	a
Outro	Outro	k1gNnSc1	Outro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
téma	téma	k1gNnSc1	téma
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
i	i	k9	i
g-funkově	gunkově	k6eAd1	g-funkově
stylizovanou	stylizovaný	k2eAgFnSc4d1	stylizovaná
věc	věc	k1gFnSc4	věc
Jemiedno	Jemiedno	k1gNnSc1	Jemiedno
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Botlem	Botl	k1gMnSc7	Botl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
Psí	psí	k2eAgInSc4d1	psí
život	život	k1gInSc4	život
si	se	k3xPyFc3	se
Separ	Separ	k1gMnSc1	Separ
přidal	přidat	k5eAaPmAgMnS	přidat
pár	pár	k4xCyI	pár
autobiografických	autobiografický	k2eAgMnPc2d1	autobiografický
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
jeho	jeho	k3xOp3gNnSc4	jeho
sídliště	sídliště	k1gNnSc4	sídliště
a	a	k8xC	a
chlapce	chlapec	k1gMnSc4	chlapec
ze	z	k7c2	z
sídliště	sídliště	k1gNnSc2	sídliště
tu	tu	k6eAd1	tu
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
ke	k	k7c3	k
psovi	pes	k1gMnSc3	pes
<g/>
.	.	kIx.	.
</s>
<s>
Tina	Tina	k1gFnSc1	Tina
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
Separovi	Separ	k1gMnSc3	Separ
se	s	k7c7	s
skladbou	skladba	k1gFnSc7	skladba
Bublina	bublina	k1gFnSc1	bublina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c4	o
obtěžování	obtěžování	k1gNnSc4	obtěžování
scény	scéna	k1gFnSc2	scéna
novináři	novinář	k1gMnSc3	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Cover	Cover	k1gInSc1	Cover
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Separovu	Separův	k2eAgFnSc4d1	Separův
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
od	od	k7c2	od
nosu	nos	k1gInSc2	nos
stává	stávat	k5eAaImIp3nS	stávat
kostrou	kostra	k1gFnSc7	kostra
<g/>
.	.	kIx.	.
</s>
<s>
Lesklý	lesklý	k2eAgInSc1d1	lesklý
potisk	potisk	k1gInSc1	potisk
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dojem	dojem	k1gInSc4	dojem
zlaté	zlatý	k2eAgFnSc2d1	zlatá
kostry	kostra	k1gFnSc2	kostra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
albu	album	k1gNnSc3	album
pirát	pirát	k1gMnSc1	pirát
si	se	k3xPyFc3	se
Separ	Separ	k1gMnSc1	Separ
udělal	udělat	k5eAaPmAgMnS	udělat
tour	tour	k1gInSc4	tour
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
navštívil	navštívit	k5eAaPmAgInS	navštívit
28	[number]	k4	28
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
klubů	klub	k1gInPc2	klub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc4	Tour
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
Pirát	pirát	k1gMnSc1	pirát
Tour	Tour	k1gMnSc1	Tour
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
v	v	k7c6	v
Piešťanském	piešťanský	k2eAgInSc6d1	piešťanský
klubu	klub	k1gInSc6	klub
Coolturak	Coolturak	k1gInSc1	Coolturak
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
zastávka	zastávka	k1gFnSc1	zastávka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
na	na	k7c6	na
Zlatých	zlatá	k1gFnPc6	zlatá
Pieskoch	Pieskoch	k1gInSc1	Pieskoch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Skupinové	skupinový	k2eAgFnPc1d1	skupinová
DMS	DMS	kA	DMS
album	album	k1gNnSc4	album
MMXV	MMXV	kA	MMXV
====	====	k?	====
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
avizované	avizovaný	k2eAgNnSc1d1	avizované
asi	asi	k9	asi
rok	rok	k1gInSc4	rok
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
20	[number]	k4	20
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgNnSc4	první
Intro	Intro	k1gNnSc4	Intro
skladbu	skladba	k1gFnSc4	skladba
je	být	k5eAaImIp3nS	být
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
i	i	k8xC	i
exotický	exotický	k2eAgInSc1d1	exotický
videoklip	videoklip	k1gInSc1	videoklip
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
například	například	k6eAd1	například
Sajfa	Sajf	k1gMnSc4	Sajf
nebo	nebo	k8xC	nebo
Majster	Majster	k1gInSc4	Majster
N.	N.	kA	N.
O	o	k7c6	o
produkci	produkce	k1gFnSc6	produkce
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
Michal	Michal	k1gMnSc1	Michal
Nemtuda	Nemtuda	k1gMnSc1	Nemtuda
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
zdobí	zdobit	k5eAaImIp3nS	zdobit
hosté	host	k1gMnPc1	host
jako	jako	k8xC	jako
Pil	pít	k5eAaImAgMnS	pít
C	C	kA	C
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
<g/>
,	,	kIx,	,
Tina	Tina	k1gFnSc1	Tina
<g/>
,	,	kIx,	,
Minimo	Minima	k1gFnSc5	Minima
<g/>
,	,	kIx,	,
Lúza	Lúza	k1gMnSc1	Lúza
a	a	k8xC	a
Strapo	Strapa	k1gFnSc5	Strapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Separ	Separa	k1gFnPc2	Separa
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
