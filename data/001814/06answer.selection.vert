<s>
Byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
za	za	k7c4	za
filmy	film	k1gInPc4	film
Sloní	slonit	k5eAaImIp3nS	slonit
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Modrý	modrý	k2eAgInSc1d1	modrý
samet	samet	k1gInSc1	samet
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mulholland	Mulholland	k1gInSc1	Mulholland
Drive	drive	k1gInSc1	drive
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
