<s>
David	David	k1gMnSc1	David
Keith	Keith	k1gMnSc1	Keith
Lynch	Lynch	k1gMnSc1	Lynch
[	[	kIx(	[
<g/>
Linč	Linč	k1gMnSc1	Linč
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
Missoula	Missoula	k1gFnSc1	Missoula
<g/>
,	,	kIx,	,
Montana	Montana	k1gFnSc1	Montana
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
performer	performer	k1gMnSc1	performer
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
významného	významný	k2eAgMnSc4d1	významný
představitele	představitel	k1gMnSc4	představitel
postmoderní	postmoderní	k2eAgFnSc2d1	postmoderní
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
za	za	k7c4	za
filmy	film	k1gInPc4	film
Sloní	slonit	k5eAaImIp3nS	slonit
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Modrý	modrý	k2eAgInSc1d1	modrý
samet	samet	k1gInSc1	samet
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mulholland	Mulholland	k1gInSc1	Mulholland
Drive	drive	k1gInSc1	drive
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slonímu	sloní	k2eAgMnSc3d1	sloní
muž	muž	k1gMnSc1	muž
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
nominován	nominovat	k5eAaBmNgMnS	nominovat
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
a	a	k8xC	a
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Středoškolské	středoškolský	k2eAgNnSc1d1	středoškolské
vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
studoval	studovat	k5eAaImAgMnS	studovat
malířství	malířství	k1gNnSc4	malířství
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
zahájil	zahájit	k5eAaPmAgMnS	zahájit
studium	studium	k1gNnSc4	studium
na	na	k7c4	na
Pennsylvania	Pennsylvanium	k1gNnPc4	Pennsylvanium
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
the	the	k?	the
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc4	Arts
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
ukončil	ukončit	k5eAaPmAgMnS	ukončit
ho	on	k3xPp3gMnSc4	on
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
záběrem	záběr	k1gInSc7	záběr
jeho	jeho	k3xOp3gFnSc2	jeho
umělecké	umělecký	k2eAgFnSc2d1	umělecká
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
expresionismem	expresionismus	k1gInSc7	expresionismus
a	a	k8xC	a
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gMnSc4	jeho
specifický	specifický	k2eAgInSc1d1	specifický
styl	styl	k1gInSc1	styl
režie	režie	k1gFnSc2	režie
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
iracionální	iracionální	k2eAgFnSc1d1	iracionální
stavba	stavba	k1gFnSc1	stavba
děje	děj	k1gInSc2	děj
a	a	k8xC	a
surrealistická	surrealistický	k2eAgNnPc4d1	surrealistické
sdělení	sdělení	k1gNnPc4	sdělení
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc1	jeho
projekty	projekt	k1gInPc1	projekt
vesměs	vesměs	k6eAd1	vesměs
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
kontroverzní	kontroverzní	k2eAgNnSc4d1	kontroverzní
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
stravitelné	stravitelný	k2eAgNnSc1d1	stravitelné
<g/>
.	.	kIx.	.
</s>
<s>
Lynch	Lynch	k1gMnSc1	Lynch
si	se	k3xPyFc3	se
často	často	k6eAd1	často
vybírá	vybírat	k5eAaImIp3nS	vybírat
jako	jako	k9	jako
místo	místo	k1gNnSc1	místo
děje	děj	k1gInSc2	děj
buď	buď	k8xC	buď
malá	malý	k2eAgNnPc1d1	malé
americká	americký	k2eAgNnPc1d1	americké
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
tak	tak	k9	tak
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
Modrém	modrý	k2eAgInSc6d1	modrý
sametu	samet	k1gInSc6	samet
nebo	nebo	k8xC	nebo
seriálu	seriál	k1gInSc6	seriál
Městečko	městečko	k1gNnSc1	městečko
Twin	Twin	k1gInSc1	Twin
Peaks	Peaks	k1gInSc1	Peaks
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
metropole	metropol	k1gFnSc2	metropol
(	(	kIx(	(
<g/>
Lost	Lost	k1gInSc1	Lost
Highway	Highwaa	k1gFnSc2	Highwaa
<g/>
,	,	kIx,	,
Mulholland	Mulholland	k1gInSc1	Mulholland
Drive	drive	k1gInSc1	drive
nebo	nebo	k8xC	nebo
Inland	Inland	k1gInSc1	Inland
Empire	empir	k1gInSc5	empir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
neslaví	slavit	k5eNaImIp3nP	slavit
vždy	vždy	k6eAd1	vždy
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
fanoušků	fanoušek	k1gMnPc2	fanoušek
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nP	těšit
až	až	k9	až
kultovní	kultovní	k2eAgFnPc1d1	kultovní
obliby	obliba	k1gFnPc1	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Svérázný	svérázný	k2eAgMnSc1d1	svérázný
autor	autor	k1gMnSc1	autor
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
využívá	využívat	k5eAaImIp3nS	využívat
transcendentální	transcendentální	k2eAgFnSc4d1	transcendentální
meditaci	meditace	k1gFnSc4	meditace
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
nadaci	nadace	k1gFnSc4	nadace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zavést	zavést	k5eAaPmF	zavést
tuto	tento	k3xDgFnSc4	tento
meditační	meditační	k2eAgFnSc4d1	meditační
techniku	technika	k1gFnSc4	technika
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
natočil	natočit	k5eAaBmAgMnS	natočit
reklamní	reklamní	k2eAgFnSc4d1	reklamní
kampaň	kampaň	k1gFnSc4	kampaň
na	na	k7c4	na
oděvní	oděvní	k2eAgFnSc4d1	oděvní
značku	značka	k1gFnSc4	značka
HM	HM	k?	HM
s	s	k7c7	s
Lanu	lano	k1gNnSc6	lano
Del	Del	k1gMnPc4	Del
Rey	Rea	k1gFnSc2	Rea
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
dcerou	dcera	k1gFnSc7	dcera
je	být	k5eAaImIp3nS	být
režisérka	režisérka	k1gFnSc1	režisérka
Jennifer	Jennifra	k1gFnPc2	Jennifra
Lynchová	Lynchová	k1gFnSc1	Lynchová
<g/>
.	.	kIx.	.
</s>
<s>
Six	Six	k?	Six
Figures	Figures	k1gInSc1	Figures
Getting	Getting	k1gInSc1	Getting
Sick	Sick	k1gInSc4	Sick
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
-	-	kIx~	-
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
Alphabet	Alphabet	k1gInSc1	Alphabet
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
-	-	kIx~	-
krátkometrážní	krátkometrážní	k2eAgFnSc1d1	krátkometrážní
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Grandmother	Grandmothra	k1gFnPc2	Grandmothra
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
krátkometrážní	krátkometrážní	k2eAgFnSc1d1	krátkometrážní
Mazací	mazací	k2eAgFnSc1d1	mazací
hlava	hlava	k1gFnSc1	hlava
(	(	kIx(	(
<g/>
Eraserhead	Eraserhead	k1gInSc1	Eraserhead
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
-	-	kIx~	-
experimentální	experimentální	k2eAgFnSc1d1	experimentální
filmová	filmový	k2eAgFnSc1d1	filmová
prvotina	prvotina	k1gFnSc1	prvotina
Sloní	sloň	k1gFnPc2	sloň
<g />
.	.	kIx.	.
</s>
<s>
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Elephant	Elephant	k1gMnSc1	Elephant
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
-	-	kIx~	-
největší	veliký	k2eAgInSc1d3	veliký
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
Duna	duna	k1gFnSc1	duna
(	(	kIx(	(
<g/>
Dune	Dune	k1gNnSc1	Dune
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
-	-	kIx~	-
filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
slavného	slavný	k2eAgInSc2d1	slavný
románu	román	k1gInSc2	román
F.	F.	kA	F.
Herberta	Herbert	k1gMnSc2	Herbert
Modrý	modrý	k2eAgInSc1d1	modrý
samet	samet	k1gInSc1	samet
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Blue	Blu	k1gFnSc2	Blu
Velvet	Velveta	k1gFnPc2	Velveta
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Zběsilost	zběsilost	k1gFnSc1	zběsilost
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
(	(	kIx(	(
<g/>
Wild	Wild	k1gMnSc1	Wild
at	at	k?	at
<g />
.	.	kIx.	.
</s>
<s>
Heart	Heart	k1gInSc1	Heart
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
-	-	kIx~	-
oceněno	oceněn	k2eAgNnSc1d1	oceněno
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
palmou	palma	k1gFnSc7	palma
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
Městečko	městečko	k1gNnSc4	městečko
Twin	Twina	k1gFnPc2	Twina
Peaks	Peaksa	k1gFnPc2	Peaksa
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
-	-	kIx~	-
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
Twin	Twin	k1gNnSc1	Twin
Peaks	Peaks	k1gInSc1	Peaks
(	(	kIx(	(
<g/>
Twin	Twin	k1gInSc1	Twin
Peaks	Peaks	k1gInSc1	Peaks
<g/>
:	:	kIx,	:
Fire	Fire	k1gFnSc1	Fire
Walk	Walk	k1gMnSc1	Walk
with	with	k1gMnSc1	with
Me	Me	k1gMnSc1	Me
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
-	-	kIx~	-
filmový	filmový	k2eAgInSc1d1	filmový
prequel	prequel	k1gInSc1	prequel
k	k	k7c3	k
seriálu	seriál	k1gInSc3	seriál
Lost	Losta	k1gFnPc2	Losta
Highway	Highwaa	k1gFnSc2	Highwaa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Příběh	příběh	k1gInSc1	příběh
Alvina	Alvin	k1gMnSc2	Alvin
Straighta	Straight	k1gMnSc2	Straight
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Straight	Straight	k1gMnSc1	Straight
Story	story	k1gFnPc4	story
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Mulholland	Mulhollanda	k1gFnPc2	Mulhollanda
Drive	drive	k1gInSc1	drive
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Darkened	Darkened	k1gMnSc1	Darkened
Room	Room	k1gMnSc1	Room
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
-	-	kIx~	-
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
Rabbits	Rabbits	k1gInSc1	Rabbits
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
-	-	kIx~	-
minisérie	minisérie	k1gFnSc1	minisérie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g />
.	.	kIx.	.
</s>
<s>
Inland	Inland	k1gInSc1	Inland
Empire	empir	k1gInSc5	empir
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Postmoderna	postmoderna	k1gFnSc1	postmoderna
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
David	David	k1gMnSc1	David
Lynch	Lyncha	k1gFnPc2	Lyncha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Galerie	galerie	k1gFnSc2	galerie
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
Stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
Davidu	David	k1gMnSc6	David
Lynchovi	Lynch	k1gMnSc6	Lynch
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
