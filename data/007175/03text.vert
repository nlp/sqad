<s>
Monzun	monzun	k1gInSc1	monzun
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
mění	měnit	k5eAaImIp3nS	měnit
směr	směr	k1gInSc1	směr
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
přináší	přinášet	k5eAaImIp3nS	přinášet
vlhký	vlhký	k2eAgInSc1d1	vlhký
vzduch	vzduch	k1gInSc1	vzduch
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
nad	nad	k7c4	nad
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
suchý	suchý	k2eAgInSc4d1	suchý
vzduch	vzduch	k1gInSc4	vzduch
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
nad	nad	k7c4	nad
oceán	oceán	k1gInSc4	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Monzuny	monzun	k1gInPc1	monzun
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikde	nikde	k6eAd1	nikde
nehrají	hrát	k5eNaImIp3nP	hrát
tak	tak	k6eAd1	tak
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
na	na	k7c6	na
Indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
září	září	k1gNnSc2	září
sem	sem	k6eAd1	sem
letní	letní	k2eAgInSc1d1	letní
monzun	monzun	k1gInSc1	monzun
přináší	přinášet	k5eAaImIp3nS	přinášet
vydatné	vydatný	k2eAgFnPc4d1	vydatná
dešťové	dešťový	k2eAgFnPc4d1	dešťová
srážky	srážka	k1gFnPc4	srážka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
zdrojem	zdroj	k1gInSc7	zdroj
závlah	závlaha	k1gFnPc2	závlaha
<g/>
.	.	kIx.	.
</s>
<s>
Monzuny	monzun	k1gInPc1	monzun
do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
oblastí	oblast	k1gFnPc2	oblast
přinášejí	přinášet	k5eAaImIp3nP	přinášet
až	až	k9	až
extrémní	extrémní	k2eAgNnSc4d1	extrémní
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
Indie	Indie	k1gFnSc2	Indie
nebo	nebo	k8xC	nebo
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
může	moct	k5eAaImIp3nS	moct
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
10	[number]	k4	10
m	m	kA	m
(	(	kIx(	(
<g/>
10000	[number]	k4	10000
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rychlejšího	rychlý	k2eAgNnSc2d2	rychlejší
zahřívání	zahřívání	k1gNnSc2	zahřívání
pevniny	pevnina	k1gFnSc2	pevnina
oproti	oproti	k7c3	oproti
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
pevninská	pevninský	k2eAgFnSc1d1	pevninská
masa	masa	k1gFnSc1	masa
Asie	Asie	k1gFnSc2	Asie
až	až	k9	až
o	o	k7c4	o
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
teplejší	teplý	k2eAgInSc1d2	teplejší
než	než	k8xS	než
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Ohřátý	ohřátý	k2eAgInSc1d1	ohřátý
vzduch	vzduch	k1gInSc1	vzduch
nad	nad	k7c7	nad
pevninou	pevnina	k1gFnSc7	pevnina
stoupá	stoupat	k5eAaImIp3nS	stoupat
a	a	k8xC	a
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tak	tak	k9	tak
oblast	oblast	k1gFnSc1	oblast
tlakové	tlakový	k2eAgFnSc2d1	tlaková
níže	níže	k1gFnSc2	níže
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
monzun	monzun	k1gInSc4	monzun
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
tlaku	tlak	k1gInSc2	tlak
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
proudit	proudit	k5eAaImF	proudit
nad	nad	k7c4	nad
pevninu	pevnina	k1gFnSc4	pevnina
chladný	chladný	k2eAgInSc4d1	chladný
těžší	těžký	k2eAgInSc4d2	těžší
vzduch	vzduch	k1gInSc4	vzduch
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Výpary	výpar	k1gInPc1	výpar
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
jeho	jeho	k3xOp3gFnSc4	jeho
vlhkost	vlhkost	k1gFnSc4	vlhkost
<g/>
,	,	kIx,	,
po	po	k7c4	po
dosažení	dosažení	k1gNnSc4	dosažení
pevniny	pevnina	k1gFnSc2	pevnina
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
kondenzují	kondenzovat	k5eAaImIp3nP	kondenzovat
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
pršet	pršet	k5eAaImF	pršet
<g/>
.	.	kIx.	.
</s>
<s>
Monzunová	monzunový	k2eAgFnSc1d1	monzunová
masa	masa	k1gFnSc1	masa
vzduchu	vzduch	k1gInSc2	vzduch
odsouvá	odsouvat	k5eAaImIp3nS	odsouvat
teplejší	teplý	k2eAgInSc1d2	teplejší
vzduch	vzduch	k1gInSc1	vzduch
nad	nad	k7c7	nad
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
obráceně	obráceně	k6eAd1	obráceně
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
monzun	monzun	k1gInSc1	monzun
bez	bez	k7c2	bez
vlhkosti	vlhkost	k1gFnSc2	vlhkost
se	se	k3xPyFc4	se
žene	hnát	k5eAaImIp3nS	hnát
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
kontinentu	kontinent	k1gInSc2	kontinent
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Indii	Indie	k1gFnSc4	Indie
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
května	květen	k1gInSc2	květen
tu	tu	k6eAd1	tu
panuje	panovat	k5eAaImIp3nS	panovat
stálé	stálý	k2eAgNnSc1d1	stálé
suché	suchý	k2eAgNnSc1d1	suché
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgInSc1d1	letní
monzun	monzun	k1gInSc1	monzun
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
nerovnoměrném	rovnoměrný	k2eNgNnSc6d1	nerovnoměrné
zahřívání	zahřívání	k1gNnSc6	zahřívání
pevniny	pevnina	k1gFnSc2	pevnina
a	a	k8xC	a
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
teplo	teplo	k6eAd1	teplo
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
stoupá	stoupat	k5eAaImIp3nS	stoupat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
pevnina	pevnina	k1gFnSc1	pevnina
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
tlaková	tlakový	k2eAgFnSc1d1	tlaková
níže	níže	k1gFnSc1	níže
<g/>
.	.	kIx.	.
</s>
<s>
Vlhký	vlhký	k2eAgInSc1d1	vlhký
vzduch	vzduch	k1gInSc1	vzduch
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
přitahován	přitahován	k2eAgInSc1d1	přitahován
<g/>
,	,	kIx,	,
zaráží	zarážet	k5eAaImIp3nS	zarážet
se	se	k3xPyFc4	se
o	o	k7c4	o
vysoká	vysoký	k2eAgNnPc4d1	vysoké
pohoří	pohoří	k1gNnPc4	pohoří
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
stoupá	stoupat	k5eAaImIp3nS	stoupat
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
a	a	k8xC	a
principiálně	principiálně	k6eAd1	principiálně
vznikají	vznikat	k5eAaImIp3nP	vznikat
bouřlivé	bouřlivý	k2eAgFnPc1d1	bouřlivá
srážky	srážka	k1gFnPc1	srážka
nad	nad	k7c7	nad
těmito	tento	k3xDgFnPc7	tento
oblastmi	oblast	k1gFnPc7	oblast
(	(	kIx(	(
<g/>
Čérápuňdží	Čérápuňdž	k1gFnSc7	Čérápuňdž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgInSc1d1	zimní
monzun	monzun	k1gInSc1	monzun
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
nerovnoměrném	rovnoměrný	k2eNgNnSc6d1	nerovnoměrné
ochlazování	ochlazování	k1gNnSc6	ochlazování
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
suchý	suchý	k2eAgInSc1d1	suchý
vzduch	vzduch	k1gInSc1	vzduch
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzniká	vznikat	k5eAaImIp3nS	vznikat
tlaková	tlakový	k2eAgFnSc1d1	tlaková
výše	výše	k1gFnSc1	výše
(	(	kIx(	(
<g/>
nízký	nízký	k2eAgInSc1d1	nízký
tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
vysoký	vysoký	k2eAgInSc4d1	vysoký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
moří	moře	k1gNnPc2	moře
se	se	k3xPyFc4	se
nasává	nasávat	k5eAaImIp3nS	nasávat
vlhkost	vlhkost	k1gFnSc1	vlhkost
a	a	k8xC	a
prouděním	proudění	k1gNnSc7	proudění
nakonec	nakonec	k6eAd1	nakonec
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
sněhovým	sněhový	k2eAgFnPc3d1	sněhová
srážkám	srážka	k1gFnPc3	srážka
–	–	k?	–
nejvíce	hodně	k6eAd3	hodně
na	na	k7c6	na
japonských	japonský	k2eAgInPc6d1	japonský
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
Hokkaidó	Hokkaidó	k1gMnSc1	Hokkaidó
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
měly	mít	k5eAaImAgFnP	mít
monzuny	monzun	k1gInPc4	monzun
tendenci	tendence	k1gFnSc4	tendence
selhat	selhat	k5eAaPmF	selhat
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
posunu	posun	k1gInSc6	posun
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
Afriky	Afrika	k1gFnSc2	Afrika
do	do	k7c2	do
Sahelu	Sahel	k1gInSc2	Sahel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
krystalizovat	krystalizovat	k5eAaImF	krystalizovat
podoba	podoba	k1gFnSc1	podoba
teorie	teorie	k1gFnSc1	teorie
globálního	globální	k2eAgNnSc2d1	globální
stmívání	stmívání	k1gNnSc2	stmívání
(	(	kIx(	(
<g/>
pokles	pokles	k1gInSc1	pokles
intenzity	intenzita	k1gFnSc2	intenzita
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
znečištěné	znečištěný	k2eAgFnSc2d1	znečištěná
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nalezeno	nalezen	k2eAgNnSc1d1	Nalezeno
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
globální	globální	k2eAgNnSc4d1	globální
stmívání	stmívání	k1gNnSc4	stmívání
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
příčinou	příčina	k1gFnSc7	příčina
selhání	selhání	k1gNnSc2	selhání
afrických	africký	k2eAgInPc2d1	africký
monzunových	monzunový	k2eAgInPc2d1	monzunový
dešťů	dešť	k1gInPc2	dešť
-	-	kIx~	-
sluneční	sluneční	k2eAgFnSc1d1	sluneční
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
snížená	snížený	k2eAgFnSc1d1	snížená
o	o	k7c4	o
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přes	přes	k7c4	přes
znečištěnou	znečištěný	k2eAgFnSc4d1	znečištěná
atmosféru	atmosféra	k1gFnSc4	atmosféra
nedopadne	dopadnout	k5eNaPmIp3nS	dopadnout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nestačí	stačit	k5eNaBmIp3nS	stačit
k	k	k7c3	k
dostatečnému	dostatečný	k2eAgInSc3d1	dostatečný
ohřevu	ohřev	k1gInSc3	ohřev
souše	souš	k1gFnSc2	souš
i	i	k8xC	i
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
drift	drift	k1gInSc4	drift
srážkových	srážkový	k2eAgInPc2d1	srážkový
mraků	mrak	k1gInPc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
monzun	monzun	k1gInSc1	monzun
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
monzun	monzun	k1gInSc1	monzun
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
