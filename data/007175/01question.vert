<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
vzdušný	vzdušný	k2eAgInSc1d1	vzdušný
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
mění	měnit	k5eAaImIp3nS	měnit
směr	směr	k1gInSc1	směr
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
<g/>
?	?	kIx.	?
</s>
