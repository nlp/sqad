<p>
<s>
Osvračínský	Osvračínský	k2eAgInSc1d1	Osvračínský
jinan	jinan	k1gInSc1	jinan
je	být	k5eAaImIp3nS	být
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Osvračín	Osvračína	k1gFnPc2	Osvračína
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Staňkova	Staňkův	k2eAgNnSc2d1	Staňkův
<g/>
.	.	kIx.	.
</s>
<s>
Stoletý	stoletý	k2eAgInSc1d1	stoletý
samčí	samčí	k2eAgInSc1d1	samčí
jinan	jinan	k1gInSc1	jinan
dvoulaločný	dvoulaločný	k2eAgInSc1d1	dvoulaločný
(	(	kIx(	(
<g/>
Ginkgo	Ginkgo	k1gNnSc1	Ginkgo
biloba	biloba	k1gFnSc1	biloba
<g/>
)	)	kIx)	)
roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
dvoře	dvůr	k1gInSc6	dvůr
zemědělského	zemědělský	k2eAgNnSc2d1	zemědělské
družstva	družstvo	k1gNnSc2	družstvo
u	u	k7c2	u
novogoticky	novogoticky	k6eAd1	novogoticky
upraveného	upravený	k2eAgInSc2d1	upravený
zámečku	zámeček	k1gInSc2	zámeček
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
byl	být	k5eAaImAgInS	být
vysazen	vysazen	k2eAgInSc1d1	vysazen
jako	jako	k8xC	jako
třináctiletý	třináctiletý	k2eAgInSc1d1	třináctiletý
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
jeho	jeho	k3xOp3gInSc2	jeho
kmene	kmen	k1gInSc2	kmen
měří	měřit	k5eAaImIp3nS	měřit
225	[number]	k4	225
cm	cm	kA	cm
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
a	a	k8xC	a
hustá	hustý	k2eAgFnSc1d1	hustá
koruna	koruna	k1gFnSc1	koruna
stromu	strom	k1gInSc2	strom
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
16,5	[number]	k4	16,5
m	m	kA	m
(	(	kIx(	(
<g/>
měření	měření	k1gNnSc4	měření
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chráněn	chráněn	k2eAgMnSc1d1	chráněn
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
dendrologickou	dendrologický	k2eAgFnSc4d1	Dendrologická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
