<p>
<s>
Zápasy	zápas	k1gInPc1	zápas
a	a	k8xC	a
turnaje	turnaj	k1gInPc1	turnaj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mistryně	mistryně	k1gFnSc2	mistryně
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
–	–	k?	–
přehled	přehled	k1gInSc1	přehled
šachových	šachový	k2eAgFnPc2d1	šachová
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
o	o	k7c4	o
titul	titul	k1gInSc4	titul
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
šachistky	šachistka	k1gFnSc2	šachistka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
ženské	ženská	k1gFnPc1	ženská
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
jsou	být	k5eAaImIp3nP	být
mistryně	mistryně	k1gFnPc1	mistryně
světa	svět	k1gInSc2	svět
titulovány	titulovat	k5eAaImNgFnP	titulovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
FIDE	FIDE	kA	FIDE
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
šampionátu	šampionát	k1gInSc2	šampionát
mužů	muž	k1gMnPc2	muž
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
organizovala	organizovat	k5eAaBmAgFnS	organizovat
tyto	tento	k3xDgInPc4	tento
turnaje	turnaj	k1gInPc4	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
==	==	k?	==
</s>
</p>
<p>
<s>
Turnaje	turnaj	k1gInPc1	turnaj
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
až	až	k9	až
1939	[number]	k4	1939
byly	být	k5eAaImAgFnP	být
organizovány	organizovat	k5eAaBmNgFnP	organizovat
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
kongresy	kongres	k1gInPc7	kongres
FIDE	FIDE	kA	FIDE
a	a	k8xC	a
šachovými	šachový	k2eAgFnPc7d1	šachová
olympiádami	olympiáda	k1gFnPc7	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
7	[number]	k4	7
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
všechny	všechen	k3xTgFnPc4	všechen
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Věra	Věra	k1gFnSc1	Věra
Menčíková	Menčíková	k1gFnSc1	Menčíková
z	z	k7c2	z
celkovou	celkový	k2eAgFnSc7d1	celková
bilancí	bilance	k1gFnSc7	bilance
78	[number]	k4	78
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
4	[number]	k4	4
remíz	remíza	k1gFnPc2	remíza
a	a	k8xC	a
1	[number]	k4	1
prohry	prohra	k1gFnPc4	prohra
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
účastnic	účastnice	k1gFnPc2	účastnice
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
švýcarským	švýcarský	k2eAgInSc7d1	švýcarský
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
turnajích	turnaj	k1gInPc6	turnaj
hrály	hrát	k5eAaImAgFnP	hrát
účastnice	účastnice	k1gFnPc1	účastnice
systémem	systém	k1gInSc7	systém
každá	každý	k3xTgFnSc1	každý
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
a	a	k8xC	a
1933	[number]	k4	1933
se	se	k3xPyFc4	se
hráčky	hráčka	k1gFnPc1	hráčka
střetly	střetnout	k5eAaPmAgFnP	střetnout
každá	každý	k3xTgFnSc1	každý
s	s	k7c7	s
každou	každý	k3xTgFnSc4	každý
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
odvetách	odveta	k1gFnPc6	odveta
měly	mít	k5eAaImAgFnP	mít
opačné	opačný	k2eAgFnPc1d1	opačná
barvy	barva	k1gFnPc1	barva
figur	figura	k1gFnPc2	figura
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1930	[number]	k4	1930
a	a	k8xC	a
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
FIDE	FIDE	kA	FIDE
omezila	omezit	k5eAaPmAgFnS	omezit
počet	počet	k1gInSc4	počet
startujících	startující	k2eAgMnPc2d1	startující
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
země	zem	k1gFnSc2	zem
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
šachistky	šachistka	k1gFnPc1	šachistka
se	se	k3xPyFc4	se
těchto	tento	k3xDgInPc2	tento
šampionátů	šampionát	k1gInPc2	šampionát
nezúčastňovaly	zúčastňovat	k5eNaImAgFnP	zúčastňovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nebyl	být	k5eNaImAgInS	být
členem	člen	k1gMnSc7	člen
FIDE	FIDE	kA	FIDE
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
dvakrát	dvakrát	k6eAd1	dvakrát
zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mistryně	mistryně	k1gFnSc2	mistryně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
byla	být	k5eAaImAgFnS	být
vyzývatelkou	vyzývatelka	k1gFnSc7	vyzývatelka
mistryně	mistryně	k1gFnSc2	mistryně
světa	svět	k1gInSc2	svět
Věry	Věra	k1gFnSc2	Věra
Menčíkové	Menčíkové	k2eAgFnSc1d1	Menčíkové
německá	německý	k2eAgFnSc1d1	německá
šachistka	šachistka	k1gFnSc1	šachistka
Sonja	Sonja	k1gFnSc1	Sonja
Grafová	Grafová	k1gFnSc1	Grafová
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc4	dva
zápasy	zápas	k1gInPc4	zápas
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Menčíková	Menčíková	k1gFnSc1	Menčíková
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
prvního	první	k4xOgInSc2	první
zápasu	zápas	k1gInSc2	zápas
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zdokumentováno	zdokumentován	k2eAgNnSc1d1	zdokumentováno
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
oficiální	oficiální	k2eAgInSc4d1	oficiální
zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mistryně	mistryně	k1gFnSc2	mistryně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhý	druhý	k4xOgInSc1	druhý
zápas	zápas	k1gInSc1	zápas
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
oficiální	oficiální	k2eAgInSc1d1	oficiální
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
FIDE	FIDE	kA	FIDE
a	a	k8xC	a
předcházel	předcházet	k5eAaImAgInS	předcházet
mu	on	k3xPp3gMnSc3	on
turnaj	turnaj	k1gInSc4	turnaj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vyzývatelka	vyzývatelka	k1gFnSc1	vyzývatelka
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1945	[number]	k4	1945
-	-	kIx~	-
1988	[number]	k4	1988
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Věry	Věra	k1gFnSc2	Věra
Menčíkové	Menčíkový	k2eAgFnSc2d1	Menčíková
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
FIDE	FIDE	kA	FIDE
turnaj	turnaj	k1gInSc4	turnaj
o	o	k7c4	o
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
trůn	trůn	k1gInSc4	trůn
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1949	[number]	k4	1949
a	a	k8xC	a
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
a	a	k8xC	a
Ljudmila	Ljudmila	k1gFnSc1	Ljudmila
Ruděnková	Ruděnková	k1gFnSc1	Ruděnková
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
pak	pak	k6eAd1	pak
systém	systém	k1gInSc1	systém
ženských	ženský	k2eAgInPc2d1	ženský
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
turnajů	turnaj	k1gInPc2	turnaj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mistryně	mistryně	k1gFnSc2	mistryně
světa	svět	k1gInSc2	svět
probíhal	probíhat	k5eAaImAgInS	probíhat
podle	podle	k7c2	podle
přesného	přesný	k2eAgNnSc2d1	přesné
schématu	schéma	k1gNnSc2	schéma
FIDE	FIDE	kA	FIDE
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
byly	být	k5eAaImAgFnP	být
zavedeny	zavést	k5eAaPmNgInP	zavést
pásmové	pásmový	k2eAgInPc1d1	pásmový
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
mezipásmové	mezipásmový	k2eAgInPc4d1	mezipásmový
turnaje	turnaj	k1gInPc4	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
titul	titul	k1gInSc4	titul
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
to	ten	k3xDgNnSc1	ten
dvakrát	dvakrát	k6eAd1	dvakrát
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
13	[number]	k4	13
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
turnaj	turnaj	k1gInSc1	turnaj
tří	tři	k4xCgFnPc2	tři
hráček	hráčka	k1gFnPc2	hráčka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Olga	Olga	k1gFnSc1	Olga
Rubcovová	Rubcovová	k1gFnSc1	Rubcovová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
na	na	k7c4	na
14	[number]	k4	14
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
hrály	hrát	k5eAaImAgFnP	hrát
na	na	k7c4	na
16	[number]	k4	16
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
remíza	remíza	k1gFnSc1	remíza
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
stačila	stačit	k5eAaBmAgFnS	stačit
mistryni	mistryně	k1gFnSc4	mistryně
světa	svět	k1gInSc2	svět
na	na	k7c4	na
uhájení	uhájení	k1gNnSc4	uhájení
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
skutečně	skutečně	k6eAd1	skutečně
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
na	na	k7c4	na
odvetu	odveta	k1gFnSc4	odveta
poražená	poražený	k2eAgFnSc1d1	poražená
šampiónka	šampiónka	k1gFnSc1	šampiónka
neměla	mít	k5eNaImAgFnS	mít
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
poskytnuto	poskytnout	k5eAaPmNgNnS	poskytnout
Jelizavetě	Jelizaveta	k1gFnSc3	Jelizaveta
Bykovové	Bykovové	k2eAgMnPc1d1	Bykovové
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
Sovětské	sovětský	k2eAgFnSc6d1	sovětská
svazu	svaz	k1gInSc2	svaz
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
zápasu	zápas	k1gInSc2	zápas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Účastnice	účastnice	k1gFnSc2	účastnice
zápasů	zápas	k1gInPc2	zápas
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
sovětské	sovětský	k2eAgFnPc1d1	sovětská
šachistky	šachistka	k1gFnPc1	šachistka
<g/>
.	.	kIx.	.
</s>
<s>
Desetkrát	desetkrát	k6eAd1	desetkrát
byl	být	k5eAaImAgInS	být
titul	titul	k1gInSc1	titul
obhájen	obhájit	k5eAaPmNgInS	obhájit
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
vyzyvatelka	vyzyvatelka	k1gFnSc1	vyzyvatelka
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
všech	všecek	k3xTgFnPc2	všecek
16	[number]	k4	16
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
zápasech	zápas	k1gInPc6	zápas
stačilo	stačit	k5eAaBmAgNnS	stačit
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
partií	partie	k1gFnPc2	partie
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejméně	málo	k6eAd3	málo
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jich	on	k3xPp3gInPc2	on
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1991	[number]	k4	1991
-	-	kIx~	-
1999	[number]	k4	1999
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozpadem	rozpad	k1gInSc7	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jeho	jeho	k3xOp3gFnPc4	jeho
hranice	hranice	k1gFnPc4	hranice
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
šampionát	šampionát	k1gInSc1	šampionát
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
i	i	k9	i
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
v	v	k7c6	v
Manile	Manila	k1gFnSc6	Manila
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Číňanka	Číňanka	k1gFnSc1	Číňanka
Sie	Sie	k1gFnSc1	Sie
Ťün	Ťün	k1gFnSc1	Ťün
a	a	k8xC	a
titul	titul	k1gInSc1	titul
s	s	k7c7	s
přestávkou	přestávka	k1gFnSc7	přestávka
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1996	[number]	k4	1996
a	a	k8xC	a
1999	[number]	k4	1999
držela	držet	k5eAaImAgFnS	držet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
se	se	k3xPyFc4	se
o	o	k7c4	o
titul	titul	k1gInSc4	titul
bojovalo	bojovat	k5eAaImAgNnS	bojovat
novým	nový	k2eAgInSc7d1	nový
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
ji	on	k3xPp3gFnSc4	on
o	o	k7c4	o
titul	titul	k1gInSc4	titul
připravila	připravit	k5eAaPmAgFnS	připravit
Maďarka	Maďarka	k1gFnSc1	Maďarka
Zsuzsa	Zsuzsa	k1gFnSc1	Zsuzsa
Polgárová	Polgárový	k2eAgFnSc1d1	Polgárová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svůj	svůj	k3xOyFgInSc4	svůj
titul	titul	k1gInSc4	titul
neobhajovala	obhajovat	k5eNaImAgFnS	obhajovat
a	a	k8xC	a
tak	tak	k9	tak
o	o	k7c4	o
něj	on	k3xPp3gNnSc4	on
přišla	přijít	k5eAaPmAgFnS	přijít
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
Robert	Robert	k1gMnSc1	Robert
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Dillí	Dillí	k1gNnSc6	Dillí
konal	konat	k5eAaImAgInS	konat
šampionát	šampionát	k1gInSc1	šampionát
poprvé	poprvé	k6eAd1	poprvé
vyřazovacím	vyřazovací	k2eAgInSc7d1	vyřazovací
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
mistryně	mistryně	k1gFnSc2	mistryně
světa	svět	k1gInSc2	svět
Sie	Sie	k1gFnSc1	Sie
Ťün	Ťün	k1gFnSc1	Ťün
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
titul	titul	k1gInSc4	titul
obhájila	obhájit	k5eAaPmAgFnS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
však	však	k9	však
v	v	k7c6	v
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
nestartovala	startovat	k5eNaBmAgFnS	startovat
a	a	k8xC	a
o	o	k7c4	o
titul	titul	k1gInSc4	titul
ji	on	k3xPp3gFnSc4	on
připravila	připravit	k5eAaPmAgFnS	připravit
její	její	k3xOp3gNnPc4	její
krajanka	krajanka	k1gFnSc1	krajanka
Ču	Ču	k1gFnSc2	Ču
Čchen	Čchna	k1gFnPc2	Čchna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
šampionát	šampionát	k1gInSc1	šampionát
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
v	v	k7c6	v
Elistě	Elist	k1gInSc6	Elist
opět	opět	k6eAd1	opět
bez	bez	k7c2	bez
účasti	účast	k1gFnSc2	účast
obhájkyně	obhájkyně	k1gFnSc2	obhájkyně
a	a	k8xC	a
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Bulharka	Bulharka	k1gFnSc1	Bulharka
Antoaneta	Antoaneta	k1gFnSc1	Antoaneta
Stojanovová	Stojanovová	k1gFnSc1	Stojanovová
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
další	další	k2eAgNnSc1d1	další
mistrovsví	mistrovsví	k1gNnSc1	mistrovsví
konaly	konat	k5eAaImAgFnP	konat
opět	opět	k6eAd1	opět
vyřazovacím	vyřazovací	k2eAgInSc7d1	vyřazovací
způsobem	způsob	k1gInSc7	způsob
v	v	k7c6	v
sudých	sudý	k2eAgNnPc6d1	sudé
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
titul	titul	k1gInSc1	titul
putoval	putovat	k5eAaImAgInS	putovat
mezi	mezi	k7c7	mezi
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
novém	nový	k2eAgInSc6d1	nový
systému	systém	k1gInSc6	systém
tak	tak	k6eAd1	tak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
žádná	žádný	k3yNgFnSc1	žádný
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
svůj	svůj	k3xOyFgInSc4	svůj
titul	titul	k1gInSc4	titul
neobhájila	obhájit	k5eNaPmAgFnS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
až	až	k9	až
Chou	Choa	k1gFnSc4	Choa
I-fan	Iany	k1gInPc2	I-fany
<g/>
,	,	kIx,	,
když	když	k8xS	když
FIDE	FIDE	kA	FIDE
obohatila	obohatit	k5eAaPmAgFnS	obohatit
systém	systém	k1gInSc4	systém
mistrovství	mistrovství	k1gNnSc2	mistrovství
v	v	k7c6	v
lichých	lichý	k2eAgInPc6d1	lichý
letech	let	k1gInPc6	let
o	o	k7c4	o
zápasy	zápas	k1gInPc4	zápas
mezi	mezi	k7c7	mezi
obhájkyní	obhájkyně	k1gFnSc7	obhájkyně
a	a	k8xC	a
vyzyvatelkou	vyzyvatelka	k1gFnSc7	vyzyvatelka
(	(	kIx(	(
<g/>
vítězkou	vítězka	k1gFnSc7	vítězka
cyklu	cyklus	k1gInSc2	cyklus
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chou	Chou	k5eAaPmIp1nS	Chou
I-fan	Ian	k1gInSc4	I-fan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
porazila	porazit	k5eAaPmAgFnS	porazit
Koneruovou	Koneruová	k1gFnSc4	Koneruová
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
ale	ale	k8xC	ale
znovu	znovu	k6eAd1	znovu
ve	v	k7c6	v
vyřazovacím	vyřazovací	k2eAgInSc6d1	vyřazovací
turnaji	turnaj	k1gInSc6	turnaj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
přišla	přijít	k5eAaPmAgFnS	přijít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měla	mít	k5eAaImAgFnS	mít
právo	právo	k1gNnSc4	právo
utkat	utkat	k5eAaPmF	utkat
se	se	k3xPyFc4	se
s	s	k7c7	s
vítězkou	vítězka	k1gFnSc7	vítězka
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
Annou	Anna	k1gFnSc7	Anna
Ušeninovou	Ušeninový	k2eAgFnSc7d1	Ušeninový
v	v	k7c6	v
odvetném	odvetný	k2eAgInSc6d1	odvetný
zápase	zápas	k1gInSc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
v	v	k7c4	v
Tchaj-čou	Tchaj-čá	k1gFnSc4	Tchaj-čá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
5,5	[number]	k4	5,5
<g/>
:	:	kIx,	:
<g/>
1,5	[number]	k4	1,5
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
o	o	k7c4	o
titul	titul	k1gInSc4	titul
opět	opět	k6eAd1	opět
přišla	přijít	k5eAaPmAgFnS	přijít
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
nezúčastnila	zúčastnit	k5eNaPmAgFnS	zúčastnit
turnaje	turnaj	k1gInPc4	turnaj
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
turnajů	turnaj	k1gInPc2	turnaj
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
М	М	k?	М
и	и	k?	и
т	т	k?	т
з	з	k?	з
з	з	k?	з
ч	ч	k?	ч
м	м	k?	м
п	п	k?	п
ш	ш	k?	ш
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Šachmatnyj	Šachmatnyj	k1gFnSc1	Šachmatnyj
slovar	slovar	k1gInSc1	slovar
<g/>
,	,	kIx,	,
Fizkultura	Fizkultura	k1gFnSc1	Fizkultura
i	i	k8xC	i
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ш	Ш	k?	Ш
с	с	k?	с
<g/>
,	,	kIx,	,
Ф	Ф	k?	Ф
и	и	k?	и
С	С	k?	С
<g/>
,	,	kIx,	,
М	М	k?	М
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Šachmaty	šachmat	k1gInPc1	šachmat
<g/>
:	:	kIx,	:
Enciklopedičeskij	Enciklopedičeskij	k1gFnSc1	Enciklopedičeskij
slovar	slovar	k1gInSc1	slovar
<g/>
,	,	kIx,	,
Sovetskaja	Sovetskaj	k2eAgFnSc1d1	Sovetskaj
enciklopedija	enciklopedija	k1gFnSc1	enciklopedija
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ш	Ш	k?	Ш
<g/>
:	:	kIx,	:
Э	Э	k?	Э
с	с	k?	с
<g/>
,	,	kIx,	,
С	С	k?	С
э	э	k?	э
<g/>
,	,	kIx,	,
М	М	k?	М
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Chalupa	Chalupa	k1gMnSc1	Chalupa
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
šachu	šach	k1gInSc2	šach
<g/>
,	,	kIx,	,
LIKA	LIKA	kA	LIKA
KLUB	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
mistryň	mistryně	k1gFnPc2	mistryně
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
</s>
</p>
<p>
<s>
Mistři	mistr	k1gMnPc1	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
</s>
</p>
<p>
<s>
Zápasy	zápas	k1gInPc1	zápas
a	a	k8xC	a
turnaje	turnaj	k1gInPc1	turnaj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Světové	světový	k2eAgInPc4d1	světový
šachové	šachový	k2eAgInPc4d1	šachový
šampionáty	šampionát	k1gInPc4	šampionát
žen	žena	k1gFnPc2	žena
</s>
</p>
