<s>
Měděrytectví	Měděrytectví	k1gNnSc2	Měděrytectví
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
u	u	k7c2	u
Dietricha	Dietrich	k1gMnSc2	Dietrich
Meyera	Meyer	k1gMnSc2	Meyer
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
a	a	k8xC	a
pak	pak	k6eAd1	pak
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
a	a	k8xC	a
ve	v	k7c6	v
svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
