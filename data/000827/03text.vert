<s>
Matthäus	Matthäus	k1gMnSc1	Matthäus
Merian	Merian	k1gMnSc1	Merian
starší	starší	k1gMnSc1	starší
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1593	[number]	k4	1593
<g/>
,	,	kIx,	,
Basilej	Basilej	k1gFnSc1	Basilej
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1650	[number]	k4	1650
<g/>
,	,	kIx,	,
Bad	Bad	k1gFnSc1	Bad
Schwalbach	Schwalbach	k1gMnSc1	Schwalbach
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
rytec	rytec	k1gMnSc1	rytec
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
působící	působící	k2eAgMnSc1d1	působící
především	především	k6eAd1	především
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Měděrytectví	Měděrytectví	k1gNnSc2	Měděrytectví
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
u	u	k7c2	u
Dietricha	Dietrich	k1gMnSc2	Dietrich
Meyera	Meyer	k1gMnSc2	Meyer
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
a	a	k8xC	a
pak	pak	k6eAd1	pak
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
a	a	k8xC	a
ve	v	k7c6	v
svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Usadil	usadit	k5eAaPmAgMnS	usadit
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
tchánem	tchán	k1gMnSc7	tchán
Theodorem	Theodor	k1gMnSc7	Theodor
de	de	k?	de
Bry	Bry	k1gMnSc1	Bry
vedl	vést	k5eAaImAgMnS	vést
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
mědiryteckou	mědirytecký	k2eAgFnSc4d1	mědirytecký
dílnu	dílna	k1gFnSc4	dílna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1631-1632	[number]	k4	1631-1632
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
dílně	dílna	k1gFnSc6	dílna
pracoval	pracovat	k5eAaImAgMnS	pracovat
český	český	k2eAgMnSc1d1	český
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
kreslíř	kreslíř	k1gMnSc1	kreslíř
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
rytin	rytina	k1gFnPc2	rytina
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
uvést	uvést	k5eAaPmF	uvést
zejména	zejména	k9	zejména
sbírku	sbírka	k1gFnSc4	sbírka
grafik	grafika	k1gFnPc2	grafika
evropských	evropský	k2eAgNnPc2d1	Evropské
měst	město	k1gNnPc2	město
Topographia	Topographia	k1gFnSc1	Topographia
Germaniae	Germaniae	k1gFnSc1	Germaniae
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
od	od	k7c2	od
Martina	Martin	k1gMnSc2	Martin
Zeillera	Zeiller	k1gMnSc2	Zeiller
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
od	od	k7c2	od
r.	r.	kA	r.
1640	[number]	k4	1640
<g/>
;	;	kIx,	;
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
až	až	k9	až
po	po	k7c6	po
Merianově	Merianův	k2eAgFnSc6d1	Merianův
smrti	smrt	k1gFnSc6	smrt
r.	r.	kA	r.
1688	[number]	k4	1688
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
30	[number]	k4	30
svazků	svazek	k1gInPc2	svazek
a	a	k8xC	a
přes	přes	k7c4	přes
2000	[number]	k4	2000
rytin	rytina	k1gFnPc2	rytina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
měst	město	k1gNnPc2	město
i	i	k8xC	i
krajin	krajina	k1gFnPc2	krajina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
Merian	Merian	k1gInSc1	Merian
provedl	provést	k5eAaPmAgInS	provést
<g/>
,	,	kIx,	,
vynikají	vynikat	k5eAaImIp3nP	vynikat
výtečnou	výtečný	k2eAgFnSc7d1	výtečná
perspektivou	perspektiva	k1gFnSc7	perspektiva
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
ceněny	cenit	k5eAaImNgFnP	cenit
jsou	být	k5eAaImIp3nP	být
ilustrace	ilustrace	k1gFnPc1	ilustrace
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
obohatil	obohatit	k5eAaPmAgMnS	obohatit
četné	četný	k2eAgFnSc2d1	četná
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Theatrum	Theatrum	k1gNnSc1	Theatrum
Europaeum	Europaeum	k1gNnSc1	Europaeum
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc1d1	velké
dílo	dílo	k1gNnSc1	dílo
ze	z	k7c2	z
současných	současný	k2eAgFnPc2d1	současná
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
bibli	bible	k1gFnSc6	bible
<g/>
,	,	kIx,	,
Gottfriedovu	Gottfriedův	k2eAgMnSc3d1	Gottfriedův
Chroniku	chronik	k1gMnSc3	chronik
a	a	k8xC	a
jiné	jiný	k2eAgFnSc3d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Matthäus	Matthäus	k1gMnSc1	Matthäus
Merian	Merian	k1gMnSc1	Merian
mladší	mladý	k2eAgMnSc1d2	mladší
(	(	kIx(	(
<g/>
1621	[number]	k4	1621
<g/>
,	,	kIx,	,
Basileji	Basilej	k1gFnSc6	Basilej
-	-	kIx~	-
1687	[number]	k4	1687
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
malířem	malíř	k1gMnSc7	malíř
podobizen	podobizna	k1gFnPc2	podobizna
dle	dle	k7c2	dle
vzoru	vzor	k1gInSc2	vzor
Anthonise	Anthonise	k1gFnSc2	Anthonise
van	vana	k1gFnPc2	vana
Dycka	Dycka	k1gMnSc1	Dycka
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Kaspar	Kaspar	k1gMnSc1	Kaspar
Merian	Merian	k1gMnSc1	Merian
byl	být	k5eAaImAgMnS	být
mědirytcem	mědirytec	k1gMnSc7	mědirytec
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Maria	Maria	k1gFnSc1	Maria
Sibylla	Sibylla	k1gFnSc1	Sibylla
Merianová	Merianová	k1gFnSc1	Merianová
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Graffová	Graffový	k2eAgFnSc1d1	Graffová
(	(	kIx(	(
<g/>
1647	[number]	k4	1647
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
-	-	kIx~	-
1717	[number]	k4	1717
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
malířkou	malířka	k1gFnSc7	malířka
a	a	k8xC	a
zabývala	zabývat	k5eAaImAgFnS	zabývat
se	se	k3xPyFc4	se
také	také	k9	také
mědirytectvím	mědirytectví	k1gNnSc7	mědirytectví
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
proslulou	proslulý	k2eAgFnSc4d1	proslulá
se	s	k7c7	s
stala	stát	k5eAaPmAgFnS	stát
svými	svůj	k3xOyFgFnPc7	svůj
miniaturami	miniatura	k1gFnPc7	miniatura
a	a	k8xC	a
akvarely	akvarel	k1gInPc7	akvarel
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1699	[number]	k4	1699
podnikla	podniknout	k5eAaPmAgFnS	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Surinamu	Surinam	k1gInSc2	Surinam
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
pak	pak	k6eAd1	pak
Metamorphosis	Metamorphosis	k1gInSc4	Metamorphosis
insectorum	insectorum	k1gInSc1	insectorum
surinamensium	surinamensium	k1gNnSc1	surinamensium
(	(	kIx(	(
<g/>
Amsterd	Amsterd	k1gInSc1	Amsterd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1705	[number]	k4	1705
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
studií	studie	k1gFnPc2	studie
o	o	k7c6	o
proměnách	proměna	k1gFnPc6	proměna
surinamského	surinamský	k2eAgInSc2d1	surinamský
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
napsala	napsat	k5eAaPmAgFnS	napsat
také	také	k6eAd1	také
Erucarum	Erucarum	k1gInSc4	Erucarum
ortus	ortus	k1gInSc4	ortus
<g/>
,	,	kIx,	,
alimentum	alimentum	k1gNnSc4	alimentum
et	et	k?	et
paradoxa	paradoxon	k1gNnSc2	paradoxon
metamorphosis	metamorphosis	k1gFnSc1	metamorphosis
(	(	kIx(	(
<g/>
Norimb	Norimb	k1gInSc1	Norimb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1679	[number]	k4	1679
a	a	k8xC	a
1683	[number]	k4	1683
<g/>
;	;	kIx,	;
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
práce	práce	k1gFnPc1	práce
nalézají	nalézat	k5eAaImIp3nP	nalézat
se	se	k3xPyFc4	se
v	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
,	,	kIx,	,
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Matthäus	Matthäus	k1gInSc1	Matthäus
Merian	Merian	k1gInSc4	Merian
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
Matthäus	Matthäus	k1gMnSc1	Matthäus
Merian	Merian	k1gMnSc1	Merian
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Matthäus	Matthäus	k1gMnSc1	Matthäus
Merian	Merian	k1gMnSc1	Merian
</s>
