<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
volbou	volba	k1gFnSc7	volba
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vlastně	vlastně	k9	vlastně
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
zemi	zem	k1gFnSc6	zem
regulérní	regulérní	k2eAgFnSc1d1	regulérní
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
Habsburk	Habsburk	k1gMnSc1	Habsburk
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
