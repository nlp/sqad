<s>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Harry	Harr	k1gInPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
and	and	k?	and
the	the	k?	the
Half-Blood	Half-Blood	k1gInSc1	Half-Blood
Prince	princ	k1gMnSc2	princ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
šestá	šestý	k4xOgFnSc1	šestý
ze	z	k7c2	z
série	série	k1gFnSc2	série
knih	kniha	k1gFnPc2	kniha
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgNnSc1d1	Rowlingové
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
<s>
Originál	originál	k1gInSc1	originál
vyšel	vyjít	k5eAaPmAgInS	vyjít
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
(	(	kIx(	(
<g/>
britského	britský	k2eAgInSc2d1	britský
letního	letní	k2eAgInSc2d1	letní
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
a	a	k8xC	a
během	během	k7c2	během
prvních	první	k4xOgNnPc2	první
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
jen	jen	k9	jen
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
6,9	[number]	k4	6,9
milionu	milion	k4xCgInSc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejrychleji	rychle	k6eAd3	rychle
prodávanou	prodávaný	k2eAgFnSc7d1	prodávaná
knihou	kniha	k1gFnSc7	kniha
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
ohlášeným	ohlášený	k2eAgInSc7d1	ohlášený
termínem	termín	k1gInSc7	termín
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
170	[number]	k4	170
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc1	Voldemort
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
neklidu	neklid	k1gInSc2	neklid
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
čarodějů	čaroděj	k1gMnPc2	čaroděj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
mudlovské	mudlovský	k2eAgFnSc2d1	mudlovská
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Rufus	Rufus	k1gInSc1	Rufus
Brousek	brousek	k1gInSc1	brousek
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Kornelia	Kornelius	k1gMnSc4	Kornelius
Popletala	Popletala	k1gMnSc4	Popletala
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ministra	ministr	k1gMnSc2	ministr
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
úspěšně	úspěšně	k6eAd1	úspěšně
zvládá	zvládat	k5eAaImIp3nS	zvládat
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
Pánovi	pán	k1gMnSc3	pán
Zla	zlo	k1gNnPc4	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
protivník	protivník	k1gMnSc1	protivník
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
jiné	jiný	k2eAgInPc4d1	jiný
plány	plán	k1gInPc4	plán
a	a	k8xC	a
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
Draco	Draco	k6eAd1	Draco
Malfoyovi	Malfoya	k1gMnSc3	Malfoya
tajnou	tajný	k2eAgFnSc4d1	tajná
misi	mise	k1gFnSc4	mise
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Narcissa	Narcissa	k1gFnSc1	Narcissa
<g/>
,	,	kIx,	,
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
o	o	k7c4	o
Draca	Dracum	k1gNnPc4	Dracum
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
Snapea	Snapea	k1gFnSc1	Snapea
odpřisáhnout	odpřisáhnout	k5eAaPmF	odpřisáhnout
jeho	jeho	k3xOp3gFnSc4	jeho
ochranu	ochrana	k1gFnSc4	ochrana
uzavřením	uzavření	k1gNnSc7	uzavření
neporušitelného	porušitelný	k2eNgInSc2d1	neporušitelný
slibu	slib	k1gInSc2	slib
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
když	když	k8xS	když
poruší	porušit	k5eAaPmIp3nS	porušit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
se	se	k3xPyFc4	se
Severus	Severus	k1gMnSc1	Severus
Snape	Snap	k1gInSc5	Snap
stává	stávat	k5eAaImIp3nS	stávat
novým	nový	k2eAgMnSc7d1	nový
učitelem	učitel	k1gMnSc7	učitel
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
a	a	k8xC	a
Horacio	Horacio	k6eAd1	Horacio
Křiklan	Křiklan	k1gInSc1	Křiklan
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
výuku	výuka	k1gFnSc4	výuka
lektvarů	lektvar	k1gInPc2	lektvar
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
získává	získávat	k5eAaImIp3nS	získávat
bývalou	bývalý	k2eAgFnSc4d1	bývalá
učebnici	učebnice	k1gFnSc4	učebnice
lektvarů	lektvar	k1gInPc2	lektvar
"	"	kIx"	"
<g/>
Prince	princa	k1gFnSc6	princa
dvojí	dvojit	k5eAaImIp3nS	dvojit
krve	krev	k1gFnSc2	krev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
připsaným	připsaný	k2eAgInPc3d1	připsaný
komentářům	komentář	k1gInPc3	komentář
prince	princ	k1gMnSc2	princ
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
daří	dařit	k5eAaImIp3nS	dařit
být	být	k5eAaImF	být
v	v	k7c6	v
lektvarech	lektvar	k1gInPc6	lektvar
první	první	k4xOgInSc4	první
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
zápasů	zápas	k1gInPc2	zápas
Famfrpálu	Famfrpál	k1gInSc2	Famfrpál
Harry	Harra	k1gMnSc2	Harra
začíná	začínat	k5eAaImIp3nS	začínat
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
Ginny	Ginn	k1gMnPc7	Ginn
Weasleyovou	Weasleyová	k1gFnSc4	Weasleyová
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
Ron	Ron	k1gMnSc1	Ron
zatím	zatím	k6eAd1	zatím
chodí	chodit	k5eAaImIp3nS	chodit
s	s	k7c7	s
Levandulí	levandule	k1gFnSc7	levandule
Brownovou	Brownový	k2eAgFnSc7d1	Brownová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ho	on	k3xPp3gNnSc4	on
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
Hermiona	Hermiona	k1gFnSc1	Hermiona
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
sblíží	sblížit	k5eAaPmIp3nS	sblížit
po	po	k7c6	po
bolestivém	bolestivý	k2eAgInSc6d1	bolestivý
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
Levandulí	levandule	k1gFnSc7	levandule
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
také	také	k9	také
začíná	začínat	k5eAaImIp3nS	začínat
docházet	docházet	k5eAaImF	docházet
na	na	k7c4	na
soukromé	soukromý	k2eAgFnPc4d1	soukromá
hodiny	hodina	k1gFnPc4	hodina
k	k	k7c3	k
řediteli	ředitel	k1gMnSc3	ředitel
Brumbálovi	brumbál	k1gMnSc3	brumbál
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
nemálo	málo	k6eNd1	málo
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
minulosti	minulost	k1gFnSc6	minulost
lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
společně	společně	k6eAd1	společně
s	s	k7c7	s
Harrym	Harrym	k1gInSc4	Harrym
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voldemort	Voldemort	k1gInSc1	Voldemort
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
sedm	sedm	k4xCc4	sedm
viteálů	viteál	k1gInPc2	viteál
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgFnPc3	který
může	moct	k5eAaImIp3nS	moct
uchovat	uchovat	k5eAaPmF	uchovat
svoji	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
uvnitř	uvnitř	k7c2	uvnitř
různých	různý	k2eAgInPc2d1	různý
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
někdo	někdo	k3yInSc1	někdo
mohl	moct	k5eAaImAgMnS	moct
Voldemorta	Voldemort	k1gMnSc4	Voldemort
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
musí	muset	k5eAaImIp3nP	muset
zničit	zničit	k5eAaPmF	zničit
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
viteály	viteála	k1gFnPc4	viteála
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
dva	dva	k4xCgInPc1	dva
<g/>
:	:	kIx,	:
deník	deník	k1gInSc1	deník
Toma	Tom	k1gMnSc2	Tom
Raddlea	Raddleus	k1gMnSc2	Raddleus
a	a	k8xC	a
prsten	prsten	k1gInSc4	prsten
Marvola	Marvola	k1gFnSc1	Marvola
(	(	kIx(	(
<g/>
Rojvola	Rojvola	k1gFnSc1	Rojvola
<g/>
)	)	kIx)	)
Gaunta	Gaunta	k1gFnSc1	Gaunta
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
hledat	hledat	k5eAaImF	hledat
třetí	třetí	k4xOgInSc1	třetí
viteál	viteál	k1gInSc1	viteál
<g/>
,	,	kIx,	,
Zmijozelův	Zmijozelův	k2eAgInSc1d1	Zmijozelův
medailon	medailon	k1gInSc1	medailon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ukrytý	ukrytý	k2eAgInSc1d1	ukrytý
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
u	u	k7c2	u
útesu	útes	k1gInSc2	útes
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
se	s	k7c7	s
hledáním	hledání	k1gNnSc7	hledání
velmi	velmi	k6eAd1	velmi
vyčerpá	vyčerpat	k5eAaPmIp3nS	vyčerpat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
se	se	k3xPyFc4	se
dozvídají	dozvídat	k5eAaImIp3nP	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
-	-	kIx~	-
Draco	Draco	k6eAd1	Draco
Malfoy	Malfoy	k1gInPc4	Malfoy
Smtijedům	Smtijed	k1gMnPc3	Smtijed
umožnil	umožnit	k5eAaPmAgInS	umožnit
vniknutí	vniknutí	k1gNnSc4	vniknutí
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
pomocí	pomocí	k7c2	pomocí
rozplývavé	rozplývavý	k2eAgFnSc2d1	rozplývavá
skříně	skříň	k1gFnSc2	skříň
<g/>
.	.	kIx.	.
</s>
<s>
Draco	Draco	k1gNnSc1	Draco
Malfoy	Malfoa	k1gFnSc2	Malfoa
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
Brumbála	brumbál	k1gMnSc2	brumbál
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dostatek	dostatek	k1gInSc1	dostatek
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Snape	Snap	k1gInSc5	Snap
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Brumbála	brumbál	k1gMnSc4	brumbál
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
Harryho	Harry	k1gMnSc2	Harry
zavraždí	zavraždit	k5eAaPmIp3nP	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pronásledování	pronásledování	k1gNnSc6	pronásledování
Snapea	Snapeus	k1gMnSc2	Snapeus
Harry	Harra	k1gMnSc2	Harra
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
Snape	Snap	k1gInSc5	Snap
je	on	k3xPp3gMnPc4	on
princem	princ	k1gMnSc7	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
medailon	medailon	k1gInSc1	medailon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
získali	získat	k5eAaPmAgMnP	získat
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
Voldemortův	Voldemortův	k2eAgInSc4d1	Voldemortův
viteál	viteál	k1gInSc4	viteál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouhý	pouhý	k2eAgInSc4d1	pouhý
padělek	padělek	k1gInSc4	padělek
položený	položený	k2eAgInSc4d1	položený
tajemným	tajemný	k2eAgInSc7d1	tajemný
R.	R.	kA	R.
A.	A.	kA	A.
B.	B.	kA	B.
Identita	identita	k1gFnSc1	identita
této	tento	k3xDgFnSc2	tento
osoby	osoba	k1gFnSc2	osoba
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zatím	zatím	k6eAd1	zatím
nezodpovězenou	zodpovězený	k2eNgFnSc7d1	nezodpovězená
otázkou	otázka	k1gFnSc7	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Brumbálově	brumbálův	k2eAgInSc6d1	brumbálův
pohřbu	pohřeb	k1gInSc6	pohřeb
se	se	k3xPyFc4	se
Harry	Harr	k1gInPc7	Harr
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
nevrátit	vrátit	k5eNaPmF	vrátit
se	se	k3xPyFc4	se
už	už	k6eAd1	už
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
jít	jít	k5eAaImF	jít
hledat	hledat	k5eAaImF	hledat
zbylé	zbylý	k2eAgInPc4d1	zbylý
viteály	viteál	k1gInPc4	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Rozchází	rozcházet	k5eAaImIp3nS	rozcházet
se	se	k3xPyFc4	se
s	s	k7c7	s
Ginny	Ginn	k1gMnPc7	Ginn
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
nechce	chtít	k5eNaImIp3nS	chtít
vystavovat	vystavovat	k5eAaImF	vystavovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
nabízí	nabízet	k5eAaImIp3nP	nabízet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
doprovodí	doprovodit	k5eAaPmIp3nS	doprovodit
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
shrnutím	shrnutí	k1gNnSc7	shrnutí
děje	děj	k1gInSc2	děj
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
rozhovoru	rozhovor	k1gInSc2	rozhovor
mezi	mezi	k7c7	mezi
ministrem	ministr	k1gMnSc7	ministr
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
britským	britský	k2eAgMnSc7d1	britský
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgMnSc1d1	předchozí
ministr	ministr	k1gMnSc1	ministr
kouzel	kouzlo	k1gNnPc2	kouzlo
Kornelius	Kornelius	k1gMnSc1	Kornelius
Popletal	Popletal	k1gMnSc1	Popletal
musel	muset	k5eAaImAgMnS	muset
rezignovat	rezignovat	k5eAaBmF	rezignovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gInSc4	on
Rufus	Rufus	k1gInSc4	Rufus
Brousek	brousek	k1gInSc1	brousek
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
si	se	k3xPyFc3	se
vybavuje	vybavovat	k5eAaImIp3nS	vybavovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
hned	hned	k6eAd1	hned
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Popletal	Popletal	k1gMnSc1	Popletal
a	a	k8xC	a
informoval	informovat	k5eAaBmAgMnS	informovat
ho	on	k3xPp3gMnSc4	on
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
kouzelnického	kouzelnický	k2eAgInSc2d1	kouzelnický
světa	svět	k1gInSc2	svět
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
ho	on	k3xPp3gMnSc4	on
řídí	řídit	k5eAaImIp3nS	řídit
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
vybavuje	vybavovat	k5eAaImIp3nS	vybavovat
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
Popletalových	Popletalův	k2eAgFnPc2d1	Popletalova
návštěv	návštěva	k1gFnPc2	návštěva
v	v	k7c6	v
předešlých	předešlý	k2eAgNnPc6d1	předešlé
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
premiérovy	premiérův	k2eAgFnSc2d1	premiérova
kanceláře	kancelář	k1gFnSc2	kancelář
pomocí	pomocí	k7c2	pomocí
letaxové	letaxový	k2eAgFnSc2d1	letaxová
sítě	síť	k1gFnSc2	síť
dorazí	dorazit	k5eAaPmIp3nP	dorazit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Popletalem	Popletal	k1gMnSc7	Popletal
i	i	k8xC	i
Brousek	brousek	k1gInSc4	brousek
a	a	k8xC	a
premiér	premiér	k1gMnSc1	premiér
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
neštěstí	neštěstí	k1gNnSc4	neštěstí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
přihodila	přihodit	k5eAaPmAgFnS	přihodit
v	v	k7c6	v
mudlovském	mudlovský	k2eAgInSc6d1	mudlovský
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc1	Voldemort
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
je	být	k5eAaImIp3nS	být
otrávený	otrávený	k2eAgMnSc1d1	otrávený
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
kouzelnický	kouzelnický	k2eAgInSc1d1	kouzelnický
svět	svět	k1gInSc1	svět
prolíná	prolínat	k5eAaImIp3nS	prolínat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
mudlovským	mudlovský	k2eAgNnSc7d1	mudlovské
<g/>
.	.	kIx.	.
</s>
<s>
Belatrix	Belatrix	k1gInSc1	Belatrix
Lestrangeová	Lestrangeová	k1gFnSc1	Lestrangeová
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Narcissa	Narcissa	k1gFnSc1	Narcissa
Malfoyová	Malfoyová	k1gFnSc1	Malfoyová
navštíví	navštívit	k5eAaPmIp3nS	navštívit
dům	dům	k1gInSc4	dům
na	na	k7c6	na
Tkalcovské	tkalcovský	k2eAgFnSc6d1	tkalcovská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
profesor	profesor	k1gMnSc1	profesor
Snape	Snap	k1gInSc5	Snap
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Narcissy	Narcissa	k1gFnSc2	Narcissa
<g/>
,	,	kIx,	,
Draco	Draco	k1gMnSc1	Draco
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
úkol	úkol	k1gInSc4	úkol
od	od	k7c2	od
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
nad	nad	k7c4	nad
jeho	jeho	k3xOp3gFnPc4	jeho
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
Snape	Snap	k1gInSc5	Snap
pomohl	pomoct	k5eAaPmAgMnS	pomoct
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
něj	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
Draco	Draco	k6eAd1	Draco
nezvládne	zvládnout	k5eNaPmIp3nS	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
Belatrix	Belatrix	k1gInSc1	Belatrix
ale	ale	k8xC	ale
Snapeovi	Snapeův	k2eAgMnPc1d1	Snapeův
nevěří	věřit	k5eNaImIp3nP	věřit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Voldemort	Voldemort	k1gInSc4	Voldemort
byl	být	k5eAaImAgMnS	být
poprvé	poprvé	k6eAd1	poprvé
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
pro	pro	k7c4	pro
Brumbála	brumbál	k1gMnSc4	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Snapeovi	Snapeus	k1gMnSc3	Snapeus
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
vše	všechen	k3xTgNnSc1	všechen
Belatrix	Belatrix	k1gInSc1	Belatrix
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ona	onen	k3xDgFnSc1	onen
nezabrání	zabránit	k5eNaPmIp3nS	zabránit
sestře	sestra	k1gFnSc3	sestra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
Snapeovi	Snapeův	k2eAgMnPc1d1	Snapeův
svěřila	svěřit	k5eAaPmAgFnS	svěřit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Snape	Snap	k1gInSc5	Snap
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
neporušitelný	porušitelný	k2eNgInSc4d1	neporušitelný
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázal	dokázat	k5eAaPmAgMnS	dokázat
svou	svůj	k3xOyFgFnSc4	svůj
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
ochotu	ochota	k1gFnSc4	ochota
udělat	udělat	k5eAaPmF	udělat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
o	o	k7c4	o
co	co	k3yQnSc4	co
ho	on	k3xPp3gMnSc4	on
Narcissa	Narcissa	k1gFnSc1	Narcissa
požádala	požádat	k5eAaPmAgFnS	požádat
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
u	u	k7c2	u
Dursleyových	Dursleyová	k1gFnPc2	Dursleyová
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
že	že	k8xS	že
Grimmauldovo	Grimmauldův	k2eAgNnSc1d1	Grimmauldovo
náměstí	náměstí	k1gNnSc1	náměstí
12	[number]	k4	12
<g/>
,	,	kIx,	,
Krátura	Krátura	k1gFnSc1	Krátura
<g/>
,	,	kIx,	,
Klofan	klofan	k1gInSc1	klofan
a	a	k8xC	a
všechen	všechen	k3xTgInSc4	všechen
další	další	k2eAgInSc4d1	další
Siriusův	Siriusův	k2eAgInSc4d1	Siriusův
majetek	majetek	k1gInSc4	majetek
teď	teď	k6eAd1	teď
patří	patřit	k5eAaImIp3nS	patřit
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
pošle	poslat	k5eAaPmIp3nS	poslat
Kráturu	Krátura	k1gFnSc4	Krátura
pracovat	pracovat	k5eAaImF	pracovat
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
a	a	k8xC	a
Klofana	Klofana	k1gFnSc1	Klofana
dá	dát	k5eAaPmIp3nS	dát
do	do	k7c2	do
opatrování	opatrování	k1gNnSc2	opatrování
Hagridovi	Hagrid	k1gMnSc3	Hagrid
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
připomene	připomenout	k5eAaPmIp3nS	připomenout
Brumbál	brumbál	k1gMnSc1	brumbál
Dursleyovým	Dursleyová	k1gFnPc3	Dursleyová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Harryho	Harry	k1gMnSc4	Harry
čekali	čekat	k5eAaImAgMnP	čekat
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
návštěvu	návštěva	k1gFnSc4	návštěva
před	před	k7c7	před
jeho	jeho	k3xOp3gFnPc7	jeho
sedmnáctými	sedmnáctý	k4xOgFnPc7	sedmnáctý
narozeninami	narozeniny	k1gFnPc7	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
a	a	k8xC	a
Harry	Harra	k1gMnSc2	Harra
opustí	opustit	k5eAaPmIp3nS	opustit
dům	dům	k1gInSc1	dům
strýce	strýc	k1gMnSc2	strýc
a	a	k8xC	a
tety	teta	k1gFnSc2	teta
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
zraněnou	zraněný	k2eAgFnSc4d1	zraněná
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobily	způsobit	k5eAaPmAgInP	způsobit
jeho	jeho	k3xOp3gFnPc4	jeho
"	"	kIx"	"
<g/>
pomalejší	pomalý	k2eAgFnPc4d2	pomalejší
reakce	reakce	k1gFnPc4	reakce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harrymu	Harrym	k1gInSc2	Harrym
neřekne	říct	k5eNaPmIp3nS	říct
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
se	se	k3xPyFc4	se
Harry	Harra	k1gFnSc2	Harra
vydá	vydat	k5eAaPmIp3nS	vydat
místo	místo	k1gNnSc4	místo
do	do	k7c2	do
Doupěte	doupě	k1gNnSc2	doupě
k	k	k7c3	k
vesnici	vesnice	k1gFnSc3	vesnice
Blábolivý	blábolivý	k2eAgInSc1d1	blábolivý
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrymum	k1gNnSc3	Harrymum
<g/>
,	,	kIx,	,
že	že	k8xS	že
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
nového	nový	k2eAgMnSc4d1	nový
učitele	učitel	k1gMnSc4	učitel
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
učitele	učitel	k1gMnSc2	učitel
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
)	)	kIx)	)
a	a	k8xC	a
jdou	jít	k5eAaImIp3nP	jít
přemluvit	přemluvit	k5eAaPmF	přemluvit
Horacia	Horacius	k1gMnSc2	Horacius
Křiklana	Křiklan	k1gMnSc2	Křiklan
(	(	kIx(	(
<g/>
Brumbálova	brumbálův	k2eAgMnSc4d1	brumbálův
starého	starý	k2eAgMnSc4d1	starý
kolegu	kolega	k1gMnSc4	kolega
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc4	ten
místo	místo	k1gNnSc4	místo
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Křiklan	Křiklan	k1gMnSc1	Křiklan
je	být	k5eAaImIp3nS	být
obtloustlý	obtloustlý	k2eAgMnSc1d1	obtloustlý
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
Zmijozelské	Zmijozelský	k2eAgFnSc2d1	Zmijozelská
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Nechce	chtít	k5eNaImIp3nS	chtít
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
místo	místo	k1gNnSc4	místo
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ho	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
nejspíš	nejspíš	k9	nejspíš
díky	díky	k7c3	díky
Harrymu	Harrym	k1gInSc3	Harrym
<g/>
)	)	kIx)	)
podaří	podařit	k5eAaPmIp3nS	podařit
přemluvit	přemluvit	k5eAaPmF	přemluvit
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
pak	pak	k6eAd1	pak
Harryho	Harry	k1gMnSc4	Harry
dopraví	dopravit	k5eAaPmIp3nP	dopravit
do	do	k7c2	do
Doupěte	doupě	k1gNnSc2	doupě
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
dávat	dávat	k5eAaImF	dávat
soukromé	soukromý	k2eAgFnPc4d1	soukromá
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Artur	Artur	k1gMnSc1	Artur
Weasley	Weaslea	k1gFnSc2	Weaslea
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nového	nový	k2eAgNnSc2d1	nové
oddělení	oddělení	k1gNnSc2	oddělení
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
a	a	k8xC	a
konfiskaci	konfiskace	k1gFnSc4	konfiskace
falešných	falešný	k2eAgNnPc2d1	falešné
obranných	obranný	k2eAgNnPc2d1	obranné
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
ochranných	ochranný	k2eAgInPc2d1	ochranný
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hledají	hledat	k5eAaImIp3nP	hledat
a	a	k8xC	a
zabavují	zabavovat	k5eAaImIp3nP	zabavovat
falešné	falešný	k2eAgInPc1d1	falešný
obranné	obranný	k2eAgInPc1d1	obranný
předměty	předmět	k1gInPc1	předmět
jako	jako	k8xC	jako
lektvary	lektvar	k1gInPc1	lektvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lotroskopy	lotroskop	k1gInPc1	lotroskop
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
sledovací	sledovací	k2eAgNnSc4d1	sledovací
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
Smrtijedy	Smrtijed	k1gMnPc4	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Fleur	Fleur	k1gMnSc1	Fleur
Delacourová	Delacourový	k2eAgFnSc1d1	Delacourová
je	on	k3xPp3gFnPc4	on
u	u	k7c2	u
Weasleyových	Weasleyová	k1gFnPc2	Weasleyová
také	také	k6eAd1	také
<g/>
,	,	kIx,	,
chtějí	chtít	k5eAaImIp3nP	chtít
se	se	k3xPyFc4	se
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Weasleym	Weasleym	k1gInSc4	Weasleym
vzít	vzít	k5eAaPmF	vzít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Hermiona	Hermiona	k1gFnSc1	Hermiona
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Doupěti	doupě	k1gNnSc6	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
kapitoly	kapitola	k1gFnSc2	kapitola
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
hanlivé	hanlivý	k2eAgFnSc2d1	hanlivá
přezdívky	přezdívka	k1gFnSc2	přezdívka
(	(	kIx(	(
<g/>
Šišla	šišla	k1gMnSc1	šišla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
Hermiona	Hermiona	k1gFnSc1	Hermiona
a	a	k8xC	a
Ginny	Ginn	k1gInPc1	Ginn
vymyslely	vymyslet	k5eAaPmAgInP	vymyslet
pro	pro	k7c4	pro
Fleur	Fleur	k1gMnSc1	Fleur
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
Ron	Ron	k1gMnSc1	Ron
a	a	k8xC	a
Hermiona	Hermion	k1gMnSc2	Hermion
dostanou	dostat	k5eAaPmIp3nP	dostat
výsledky	výsledek	k1gInPc1	výsledek
zkoušek	zkouška	k1gFnPc2	zkouška
NKÚ	NKÚ	kA	NKÚ
<g/>
.	.	kIx.	.
</s>
<s>
Dozvíme	dozvědět	k5eAaPmIp1nP	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
známka	známka	k1gFnSc1	známka
T	T	kA	T
opravdu	opravdu	k6eAd1	opravdu
znamená	znamenat	k5eAaImIp3nS	znamenat
Troll	troll	k1gMnSc1	troll
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
získal	získat	k5eAaPmAgInS	získat
<g/>
:	:	kIx,	:
Astronomie	astronomie	k1gFnSc1	astronomie
<g/>
:	:	kIx,	:
P	P	kA	P
Péče	péče	k1gFnSc1	péče
o	o	k7c4	o
kouzelné	kouzelný	k2eAgMnPc4d1	kouzelný
tvory	tvor	k1gMnPc4	tvor
<g/>
:	:	kIx,	:
N	N	kA	N
Kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
formule	formule	k1gFnSc2	formule
<g/>
:	:	kIx,	:
N	N	kA	N
Obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
:	:	kIx,	:
V	v	k7c4	v
Jasnovidectví	jasnovidectví	k1gNnSc4	jasnovidectví
<g/>
:	:	kIx,	:
M	M	kA	M
Bylinkářství	bylinkářství	k1gNnSc4	bylinkářství
<g/>
:	:	kIx,	:
N	N	kA	N
Dějiny	dějiny	k1gFnPc4	dějiny
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
:	:	kIx,	:
H	H	kA	H
Lektvary	lektvar	k1gInPc1	lektvar
<g/>
:	:	kIx,	:
N	N	kA	N
Přeměňování	přeměňování	k1gNnSc4	přeměňování
<g/>
:	:	kIx,	:
N	N	kA	N
Harryho	Harry	k1gMnSc2	Harry
výsledky	výsledek	k1gInPc7	výsledek
jsou	být	k5eAaImIp3nP	být
dobré	dobrý	k2eAgInPc1d1	dobrý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	on	k3xPp3gInSc4	on
sen	sen	k1gInSc4	sen
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
bystrozorem	bystrozor	k1gInSc7	bystrozor
se	se	k3xPyFc4	se
nesplní	splnit	k5eNaPmIp3nS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
V	V	kA	V
z	z	k7c2	z
lektvarů	lektvar	k1gInPc2	lektvar
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
Snape	Snap	k1gInSc5	Snap
přijal	přijmout	k5eAaPmAgMnS	přijmout
na	na	k7c4	na
lektvary	lektvar	k1gInPc4	lektvar
pro	pro	k7c4	pro
pokročilé	pokročilý	k2eAgInPc4d1	pokročilý
-	-	kIx~	-
lektvary	lektvar	k1gInPc4	lektvar
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
bystrozor	bystrozor	k1gInSc1	bystrozor
musí	muset	k5eAaImIp3nS	muset
splnit	splnit	k5eAaPmF	splnit
u	u	k7c2	u
OVCE	ovce	k1gFnSc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
dostane	dostat	k5eAaPmIp3nS	dostat
podobné	podobný	k2eAgInPc4d1	podobný
výsledky	výsledek	k1gInPc4	výsledek
(	(	kIx(	(
<g/>
také	také	k9	také
neudělá	udělat	k5eNaPmIp3nS	udělat
dějiny	dějiny	k1gFnPc4	dějiny
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
věštectví	věštectví	k1gNnPc2	věštectví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádné	žádný	k3yNgNnSc4	žádný
V	V	kA	V
<g/>
,	,	kIx,	,
Hermiona	Hermiona	k1gFnSc1	Hermiona
dostane	dostat	k5eAaPmIp3nS	dostat
deset	deset	k4xCc4	deset
V	v	k7c6	v
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
N	N	kA	N
(	(	kIx(	(
<g/>
z	z	k7c2	z
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
mohou	moct	k5eAaImIp3nP	moct
postoupit	postoupit	k5eAaPmF	postoupit
do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
pro	pro	k7c4	pro
OVCE	ovce	k1gFnPc4	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
jsou	být	k5eAaImIp3nP	být
zabíjeni	zabíjen	k2eAgMnPc1d1	zabíjen
a	a	k8xC	a
mizí	mizet	k5eAaImIp3nP	mizet
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Igor	Igor	k1gMnSc1	Igor
Karkarov	Karkarov	k1gInSc4	Karkarov
je	být	k5eAaImIp3nS	být
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgMnS	zabít
Smrtijedy	Smrtijed	k1gMnPc7	Smrtijed
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Ollivander	Ollivander	k1gMnSc1	Ollivander
(	(	kIx(	(
<g/>
výrobce	výrobce	k1gMnSc1	výrobce
hůlek	hůlka	k1gFnPc2	hůlka
<g/>
)	)	kIx)	)
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
famfrpálovým	famfrpálový	k2eAgMnSc7d1	famfrpálový
kapitánem	kapitán	k1gMnSc7	kapitán
<g/>
,	,	kIx,	,
což	což	k9	což
mu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
Hermiony	Hermion	k1gInPc7	Hermion
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
status	status	k1gInSc4	status
prefekta	prefekt	k1gMnSc2	prefekt
(	(	kIx(	(
<g/>
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
jejich	jejich	k3xOp3gFnSc4	jejich
koupelnu	koupelna	k1gFnSc4	koupelna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jestli	jestli	k8xS	jestli
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
jde	jít	k5eAaImIp3nS	jít
nakupovat	nakupovat	k5eAaBmF	nakupovat
do	do	k7c2	do
Příčné	příčný	k2eAgFnSc2d1	příčná
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkají	potkat	k5eAaPmIp3nP	potkat
Hagrida	Hagrid	k1gMnSc2	Hagrid
a	a	k8xC	a
zažijí	zažít	k5eAaPmIp3nP	zažít
nepříjemné	příjemný	k2eNgNnSc4d1	nepříjemné
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
Dracem	Draec	k1gInSc7	Draec
Malfoyem	Malfoy	k1gInSc7	Malfoy
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
Narcissou	Narcissa	k1gFnSc7	Narcissa
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
vidí	vidět	k5eAaImIp3nS	vidět
Draca	Draca	k1gFnSc1	Draca
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
Madam	madam	k1gFnSc2	madam
Malkinové	Malkinová	k1gFnSc2	Malkinová
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
podezření	podezření	k1gNnSc4	podezření
ještě	ještě	k9	ještě
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
<g/>
,	,	kIx,	,
když	když	k8xS	když
Malfoy	Malfoy	k1gInPc1	Malfoy
odstrčí	odstrčit	k5eAaPmIp3nP	odstrčit
paní	paní	k1gFnSc7	paní
Malkinovou	Malkinový	k2eAgFnSc7d1	Malkinová
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
snaží	snažit	k5eAaImIp3nS	snažit
vyhrnout	vyhrnout	k5eAaPmF	vyhrnout
rukáv	rukáv	k1gInSc4	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Draco	Draco	k6eAd1	Draco
schovává	schovávat	k5eAaImIp3nS	schovávat
Znamení	znamení	k1gNnSc4	znamení
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
důkaz	důkaz	k1gInSc1	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Smrtijed	Smrtijed	k1gInSc4	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
pak	pak	k6eAd1	pak
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
Kratochvilných	kratochvilný	k2eAgFnPc2d1	kratochvilná
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
kejklí	kejkle	k1gFnPc2	kejkle
<g/>
,	,	kIx,	,
žertovného	žertovný	k2eAgInSc2d1	žertovný
obchodu	obchod	k1gInSc2	obchod
Freda	Fred	k1gMnSc2	Fred
a	a	k8xC	a
George	Georg	k1gMnSc2	Georg
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jen	jen	k9	jen
vzkvétá	vzkvétat	k5eAaImIp3nS	vzkvétat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
prodeji	prodej	k1gInSc3	prodej
žertovných	žertovný	k2eAgInPc2d1	žertovný
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
prodává	prodávat	k5eAaImIp3nS	prodávat
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
začarované	začarovaný	k2eAgInPc4d1	začarovaný
ochranné	ochranný	k2eAgInPc4d1	ochranný
klobouky	klobouk	k1gInPc4	klobouk
<g/>
,	,	kIx,	,
kabáty	kabát	k1gInPc4	kabát
a	a	k8xC	a
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
.	.	kIx.	.
</s>
<s>
Draco	Draco	k6eAd1	Draco
zajde	zajít	k5eAaPmIp3nS	zajít
do	do	k7c2	do
Obrtlé	Obrtlý	k2eAgFnSc2d1	Obrtlá
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštíví	navštívit	k5eAaPmIp3nS	navštívit
obchod	obchod	k1gInSc1	obchod
Borgin	Borgin	k2eAgInSc1d1	Borgin
&	&	k?	&
Burkes	Burkes	k1gInSc1	Burkes
<g/>
.	.	kIx.	.
</s>
<s>
Straší	strašit	k5eAaImIp3nS	strašit
Borgina	Borgina	k1gFnSc1	Borgina
<g/>
,	,	kIx,	,
když	když	k8xS	když
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
rodinný	rodinný	k2eAgMnSc1d1	rodinný
přítel	přítel	k1gMnSc1	přítel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Fenrir	Fenrir	k1gMnSc1	Fenrir
Šedohřbet	Šedohřbet	k1gMnSc1	Šedohřbet
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
navštíví	navštívit	k5eAaPmIp3nS	navštívit
<g/>
,	,	kIx,	,
když	když	k8xS	když
"	"	kIx"	"
<g/>
tuto	tento	k3xDgFnSc4	tento
věc	věc	k1gFnSc4	věc
neopraví	opravit	k5eNaPmIp3nS	opravit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Malfoy	Malfoa	k1gFnPc4	Malfoa
mu	on	k3xPp3gMnSc3	on
také	také	k6eAd1	také
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
mu	on	k3xPp3gMnSc3	on
zarezervuje	zarezervovat	k5eAaImIp3nS	zarezervovat
na	na	k7c4	na
později	pozdě	k6eAd2	pozdě
určitou	určitý	k2eAgFnSc4d1	určitá
věc	věc	k1gFnSc4	věc
a	a	k8xC	a
ať	ať	k9	ať
to	ten	k3xDgNnSc1	ten
nikomu	nikdo	k3yNnSc3	nikdo
neříká	říkat	k5eNaImIp3nS	říkat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
svěří	svěřit	k5eAaPmIp3nP	svěřit
panu	pan	k1gMnSc3	pan
Weasleymu	Weasleym	k1gInSc3	Weasleym
s	s	k7c7	s
podezřením	podezření	k1gNnSc7	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
Draco	Draco	k1gNnSc1	Draco
Malfoy	Malfoa	k1gFnSc2	Malfoa
je	být	k5eAaImIp3nS	být
Smrtijed	Smrtijed	k1gInSc1	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
jim	on	k3xPp3gMnPc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Draco	Draco	k6eAd1	Draco
řval	řvát	k5eAaImAgMnS	řvát
a	a	k8xC	a
ucukl	ucuknout	k5eAaPmAgMnS	ucuknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
chtěla	chtít	k5eAaImAgFnS	chtít
paní	paní	k1gFnSc4	paní
Malkinová	Malkinová	k1gFnSc1	Malkinová
vyhrnout	vyhrnout	k5eAaPmF	vyhrnout
rukáv	rukáv	k1gInSc4	rukáv
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
také	také	k9	také
Borginovi	Borgin	k1gMnSc3	Borgin
ukázal	ukázat	k5eAaPmAgMnS	ukázat
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ho	on	k3xPp3gMnSc4	on
přimělo	přimět	k5eAaPmAgNnS	přimět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Malfoye	Malfoye	k1gInSc1	Malfoye
bral	brát	k5eAaImAgInS	brát
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
prozatím	prozatím	k6eAd1	prozatím
pouze	pouze	k6eAd1	pouze
podezření	podezření	k1gNnSc4	podezření
<g/>
.	.	kIx.	.
</s>
<s>
Ginny	Ginna	k1gFnPc4	Ginna
Weasleyová	Weasleyová	k1gFnSc1	Weasleyová
má	mít	k5eAaImIp3nS	mít
mazlíčka	mazlíček	k1gMnSc4	mazlíček
-	-	kIx~	-
trpaslenku	trpaslenka	k1gFnSc4	trpaslenka
Arnolda	Arnold	k1gMnSc2	Arnold
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Bradavickém	Bradavický	k2eAgInSc6d1	Bradavický
expresu	expres	k1gInSc6	expres
Harry	Harra	k1gMnSc2	Harra
a	a	k8xC	a
Neville	Nevill	k1gMnSc2	Nevill
pozváni	pozvat	k5eAaPmNgMnP	pozvat
na	na	k7c4	na
svačinu	svačina	k1gFnSc4	svačina
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
Křiklanem	Křiklan	k1gMnSc7	Křiklan
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tam	tam	k6eAd1	tam
dorazí	dorazit	k5eAaPmIp3nP	dorazit
<g/>
,	,	kIx,	,
najdou	najít	k5eAaPmIp3nP	najít
tam	tam	k6eAd1	tam
spoustu	spousta	k1gFnSc4	spousta
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Ginny	Ginna	k1gFnSc2	Ginna
Weasleyové	Weasleyová	k1gFnSc2	Weasleyová
a	a	k8xC	a
Blaise	Blaise	k1gFnSc1	Blaise
Zabiniho	Zabini	k1gMnSc2	Zabini
<g/>
,	,	kIx,	,
zmijozelského	zmijozelský	k2eAgMnSc2d1	zmijozelský
studenta	student	k1gMnSc2	student
z	z	k7c2	z
Harryho	Harry	k1gMnSc2	Harry
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
studenti	student	k1gMnPc1	student
jsou	být	k5eAaImIp3nP	být
spojeni	spojit	k5eAaPmNgMnP	spojit
se	s	k7c7	s
slavnými	slavný	k2eAgMnPc7d1	slavný
čaroději	čaroděj	k1gMnPc7	čaroděj
a	a	k8xC	a
čarodějkami	čarodějka	k1gFnPc7	čarodějka
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Ginny	Ginna	k1gFnSc2	Ginna
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Křiklan	Křiklan	k1gMnSc1	Křiklan
pozval	pozvat	k5eAaPmAgMnS	pozvat
<g/>
,	,	kIx,	,
když	když	k8xS	když
viděl	vidět	k5eAaImAgMnS	vidět
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
jejích	její	k3xOp3gNnPc2	její
zaklínadel	zaklínadlo	k1gNnPc2	zaklínadlo
<g/>
.	.	kIx.	.
</s>
<s>
Křiklan	Křiklan	k1gMnSc1	Křiklan
je	on	k3xPp3gInPc4	on
všechny	všechen	k3xTgInPc4	všechen
vyzpovídá	vyzpovídat	k5eAaPmIp3nS	vyzpovídat
(	(	kIx(	(
<g/>
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
Nevilla	Nevilla	k1gFnSc1	Nevilla
<g/>
,	,	kIx,	,
Giny	gin	k1gInPc1	gin
<g/>
,	,	kIx,	,
Zabiniho	Zabiniha	k1gFnSc5	Zabiniha
<g/>
,	,	kIx,	,
Cormaca	Cormaca	k1gMnSc1	Cormaca
McLaggena	McLaggena	k1gFnSc1	McLaggena
a	a	k8xC	a
Marcuse	Marcuse	k1gFnSc1	Marcuse
Belbyho	Belby	k1gMnSc2	Belby
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozloučí	rozloučit	k5eAaPmIp3nS	rozloučit
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
s	s	k7c7	s
důvěrou	důvěra	k1gFnSc7	důvěra
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
slávě	sláva	k1gFnSc6	sláva
svých	svůj	k3xOyFgMnPc2	svůj
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Harry	Harra	k1gFnPc1	Harra
odchází	odcházet	k5eAaImIp3nP	odcházet
z	z	k7c2	z
kupé	kupé	k1gNnSc2	kupé
<g/>
,	,	kIx,	,
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
,	,	kIx,	,
schován	schován	k2eAgInSc1d1	schován
pod	pod	k7c7	pod
neviditelným	viditelný	k2eNgInSc7d1	neviditelný
pláštěm	plášť	k1gInSc7	plášť
<g/>
,	,	kIx,	,
Zabiniho	Zabini	k1gMnSc4	Zabini
do	do	k7c2	do
kupé	kupé	k1gNnSc2	kupé
k	k	k7c3	k
Malfoyovi	Malfoya	k1gMnSc3	Malfoya
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
Malfoye	Malfoye	k1gInSc4	Malfoye
nepozorovaně	pozorovaně	k6eNd1	pozorovaně
poslouchat	poslouchat	k5eAaImF	poslouchat
<g/>
.	.	kIx.	.
</s>
<s>
Malfoy	Malfoy	k1gInPc1	Malfoy
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
už	už	k6eAd1	už
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
být	být	k5eAaImF	být
nemusí	muset	k5eNaImIp3nS	muset
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
zabývat	zabývat	k5eAaImF	zabývat
"	"	kIx"	"
<g/>
většími	veliký	k2eAgFnPc7d2	veliký
a	a	k8xC	a
lepšími	dobrý	k2eAgFnPc7d2	lepší
věcmi	věc	k1gFnPc7	věc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
zmijozelští	zmijozelský	k2eAgMnPc1d1	zmijozelský
jsou	být	k5eAaImIp3nP	být
překvapeni	překvapen	k2eAgMnPc1d1	překvapen
a	a	k8xC	a
Zabini	Zabin	k2eAgMnPc1d1	Zabin
se	se	k3xPyFc4	se
zeptá	zeptat	k5eAaPmIp3nS	zeptat
<g/>
,	,	kIx,	,
co	co	k9	co
asi	asi	k9	asi
tak	tak	k9	tak
pro	pro	k7c4	pro
Voldemorta	Voldemort	k1gMnSc4	Voldemort
může	moct	k5eAaImIp3nS	moct
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
když	když	k8xS	když
ještě	ještě	k6eAd1	ještě
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Malfoy	Malfoy	k1gInPc4	Malfoy
odsekne	odseknout	k5eAaPmIp3nS	odseknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
Pán	pán	k1gMnSc1	pán
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Vlak	vlak	k1gInSc1	vlak
dorazí	dorazit	k5eAaPmIp3nS	dorazit
do	do	k7c2	do
Prasinek	Prasinka	k1gFnPc2	Prasinka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
<g/>
,	,	kIx,	,
narazí	narazit	k5eAaPmIp3nP	narazit
Malfoy	Malfo	k2eAgFnPc1d1	Malfo
<g/>
.	.	kIx.	.
</s>
<s>
Znehybní	znehybnit	k5eAaPmIp3nS	znehybnit
Harryho	Harry	k1gMnSc4	Harry
a	a	k8xC	a
zlomí	zlomit	k5eAaPmIp3nS	zlomit
mu	on	k3xPp3gMnSc3	on
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
dupne	dupnout	k5eAaPmIp3nS	dupnout
na	na	k7c4	na
obličej	obličej	k1gInSc4	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
něj	on	k3xPp3gMnSc4	on
přehodí	přehodit	k5eAaPmIp3nS	přehodit
neviditelný	viditelný	k2eNgInSc1d1	neviditelný
plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Harry	Harra	k1gFnPc1	Harra
vůbec	vůbec	k9	vůbec
nemusí	muset	k5eNaImIp3nP	muset
letos	letos	k6eAd1	letos
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Harryho	Harryze	k6eAd1	Harryze
zachrání	zachránit	k5eAaPmIp3nS	zachránit
Tonksová	Tonksová	k1gFnSc1	Tonksová
(	(	kIx(	(
<g/>
pozor	pozor	k1gInSc1	pozor
-	-	kIx~	-
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Lenka	lenka	k1gFnPc2	lenka
Láskorádová	Láskorádový	k2eAgFnSc1d1	Láskorádová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyléčí	vyléčit	k5eAaPmIp3nS	vyléčit
mu	on	k3xPp3gMnSc3	on
zlomený	zlomený	k2eAgInSc1d1	zlomený
nos	nos	k1gInSc4	nos
a	a	k8xC	a
vykouzlí	vykouzlit	k5eAaPmIp3nP	vykouzlit
patrona	patron	k1gMnSc4	patron
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
měla	mít	k5eAaImAgNnP	mít
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
Harry	Harr	k1gMnPc4	Harr
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
ji	on	k3xPp3gFnSc4	on
zřejmě	zřejmě	k6eAd1	zřejmě
trápí	trápit	k5eAaImIp3nS	trápit
Siriusova	Siriusův	k2eAgFnSc1d1	Siriusova
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
vypadá	vypadat	k5eAaImIp3nS	vypadat
velmi	velmi	k6eAd1	velmi
bledě	bledě	k6eAd1	bledě
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dorazí	dorazit	k5eAaPmIp3nS	dorazit
ke	k	k7c3	k
vchodu	vchod	k1gInSc3	vchod
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc4	dveře
otevře	otevřít	k5eAaPmIp3nS	otevřít
Snape	Snap	k1gInSc5	Snap
a	a	k8xC	a
Harryho	Harry	k1gMnSc4	Harry
dovede	dovést	k5eAaPmIp3nS	dovést
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
síně	síň	k1gFnSc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
strhne	strhnout	k5eAaPmIp3nS	strhnout
70	[number]	k4	70
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
na	na	k7c4	na
Malfoye	Malfoye	k1gFnSc4	Malfoye
nežaluje	žalovat	k5eNaImIp3nS	žalovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
hrdý	hrdý	k2eAgMnSc1d1	hrdý
a	a	k8xC	a
stydí	stydět	k5eAaImIp3nS	stydět
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slavnosti	slavnost	k1gFnSc6	slavnost
(	(	kIx(	(
<g/>
kterou	který	k3yQgFnSc4	který
Harry	Harro	k1gNnPc7	Harro
zmešká	zmeškat	k5eAaPmIp3nS	zmeškat
o	o	k7c4	o
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
)	)	kIx)	)
oznámí	oznámit	k5eAaPmIp3nS	oznámit
Brumbál	brumbál	k1gMnSc1	brumbál
jmenování	jmenování	k1gNnSc4	jmenování
profesora	profesor	k1gMnSc2	profesor
Horacia	Horacius	k1gMnSc2	Horacius
Křiklana	Křiklan	k1gMnSc2	Křiklan
na	na	k7c4	na
post	post	k1gInSc4	post
učitele	učitel	k1gMnSc4	učitel
lektvarů	lektvar	k1gInPc2	lektvar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snape	Snap	k1gInSc5	Snap
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
ujme	ujmout	k5eAaPmIp3nS	ujmout
vysněné	vysněný	k2eAgFnSc2d1	vysněná
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
.	.	kIx.	.
</s>
<s>
Harryho	Harryze	k6eAd1	Harryze
sen	sen	k1gInSc4	sen
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
bystrozorem	bystrozor	k1gInSc7	bystrozor
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zase	zase	k9	zase
skutečný	skutečný	k2eAgInSc1d1	skutečný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
lektvary	lektvar	k1gInPc1	lektvar
učí	učit	k5eAaImIp3nP	učit
Křiklan	Křiklan	k1gInSc4	Křiklan
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tam	tam	k6eAd1	tam
chodit	chodit	k5eAaImF	chodit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
dostal	dostat	k5eAaPmAgMnS	dostat
N.	N.	kA	N.
Bohužel	bohužel	k9	bohužel
pro	pro	k7c4	pro
Hagrida	Hagrid	k1gMnSc4	Hagrid
už	už	k9	už
Harry	Harr	k1gMnPc4	Harr
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
ani	ani	k8xC	ani
Hermiona	Hermiona	k1gFnSc1	Hermiona
nechodí	chodit	k5eNaImIp3nS	chodit
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
kouzelné	kouzelný	k2eAgMnPc4d1	kouzelný
tvory	tvor	k1gMnPc4	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
ostatní	ostatní	k2eAgInPc4d1	ostatní
předměty	předmět	k1gInPc4	předmět
ale	ale	k8xC	ale
chodí	chodit	k5eAaImIp3nP	chodit
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
<s>
Profesorka	profesorka	k1gFnSc1	profesorka
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
Harrymu	Harrym	k1gInSc2	Harrym
předá	předat	k5eAaPmIp3nS	předat
seznam	seznam	k1gInSc1	seznam
potencionálních	potencionální	k2eAgMnPc2d1	potencionální
hráčů	hráč	k1gMnPc2	hráč
famfrpálu	famfrpál	k1gInSc2	famfrpál
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hodině	hodina	k1gFnSc6	hodina
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
tichá	tichý	k2eAgNnPc1d1	tiché
kouzla	kouzlo	k1gNnPc1	kouzlo
<g/>
,	,	kIx,	,
kouzla	kouzlo	k1gNnPc1	kouzlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
vyvoláváte	vyvolávat	k5eAaImIp2nP	vyvolávat
bez	bez	k7c2	bez
mluvení	mluvení	k1gNnSc2	mluvení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Harry	Harra	k1gFnPc1	Harra
vykouzlí	vykouzlit	k5eAaPmIp3nP	vykouzlit
ochranný	ochranný	k2eAgInSc4d1	ochranný
štít	štít	k1gInSc4	štít
a	a	k8xC	a
Snape	Snap	k1gInSc5	Snap
ho	on	k3xPp3gMnSc4	on
srazí	srazit	k5eAaPmIp3nS	srazit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
Harry	Harro	k1gNnPc7	Harro
drzý	drzý	k2eAgInSc4d1	drzý
<g/>
.	.	kIx.	.
</s>
<s>
Dostane	dostat	k5eAaPmIp3nS	dostat
proto	proto	k8xC	proto
od	od	k7c2	od
Snapea	Snape	k1gInSc2	Snape
školní	školní	k2eAgInSc4d1	školní
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
ho	on	k3xPp3gMnSc4	on
zachrání	zachránit	k5eAaPmIp3nS	zachránit
Brumbál	brumbál	k1gMnSc1	brumbál
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
dá	dát	k5eAaPmIp3nS	dát
první	první	k4xOgFnSc4	první
soukromou	soukromý	k2eAgFnSc4d1	soukromá
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Lektvary	lektvar	k1gInPc1	lektvar
jsou	být	k5eAaImIp3nP	být
teď	teď	k6eAd1	teď
daleko	daleko	k6eAd1	daleko
zábavnější	zábavní	k2eAgNnSc1d2	zábavnější
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Snape	Snap	k1gInSc5	Snap
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
30	[number]	k4	30
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Křiklana	Křiklana	k1gFnSc1	Křiklana
(	(	kIx(	(
<g/>
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ředitele	ředitel	k1gMnSc2	ředitel
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
)	)	kIx)	)
potěší	potěšit	k5eAaPmIp3nS	potěšit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Hermiona	Hermiona	k1gFnSc1	Hermiona
taková	takový	k3xDgFnSc1	takový
dobrá	dobrý	k2eAgFnSc1d1	dobrá
čarodějka	čarodějka	k1gFnSc1	čarodějka
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
rodiče	rodič	k1gMnPc4	rodič
mudly	mudl	k1gInPc7	mudl
<g/>
.	.	kIx.	.
</s>
<s>
Lily	lít	k5eAaImAgFnP	lít
Potterová	Potterový	k2eAgFnSc1d1	Potterová
byla	být	k5eAaImAgFnS	být
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
studentek	studentka	k1gFnPc2	studentka
(	(	kIx(	(
<g/>
dává	dávat	k5eAaImIp3nS	dávat
ale	ale	k8xC	ale
přednost	přednost	k1gFnSc4	přednost
čisté	čistý	k2eAgFnSc3d1	čistá
krvi	krev	k1gFnSc3	krev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křiklan	Křiklan	k1gInSc1	Křiklan
také	také	k9	také
uvaří	uvařit	k5eAaPmIp3nS	uvařit
lektvar	lektvar	k1gInSc1	lektvar
Felix	Felix	k1gMnSc1	Felix
felicis	felicis	k1gInSc1	felicis
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
šťastné	šťastný	k2eAgNnSc4d1	šťastné
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
tekuté	tekutý	k2eAgNnSc4d1	tekuté
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
vezměte	vzít	k5eAaPmRp2nP	vzít
lžičku	lžička	k1gFnSc4	lžička
ke	k	k7c3	k
snídani	snídaně	k1gFnSc3	snídaně
<g/>
,	,	kIx,	,
budete	být	k5eAaImBp2nP	být
mít	mít	k5eAaImF	mít
perfektní	perfektní	k2eAgInSc4d1	perfektní
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
zakázaný	zakázaný	k2eAgInSc1d1	zakázaný
při	při	k7c6	při
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
,	,	kIx,	,
zkouškách	zkouška	k1gFnPc6	zkouška
<g/>
,	,	kIx,	,
volbách	volba	k1gFnPc6	volba
a	a	k8xC	a
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
konzumace	konzumace	k1gFnSc1	konzumace
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
závratě	závrať	k1gFnPc4	závrať
<g/>
,	,	kIx,	,
lehkomyslnost	lehkomyslnost	k1gFnSc4	lehkomyslnost
a	a	k8xC	a
při	při	k7c6	při
předávkování	předávkování	k1gNnSc6	předávkování
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
na	na	k7c6	na
první	první	k4xOgFnSc6	první
hodině	hodina	k1gFnSc6	hodina
uvaří	uvařit	k5eAaPmIp3nS	uvařit
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
doušek	doušek	k1gInSc4	doušek
živoucí	živoucí	k2eAgFnSc2d1	živoucí
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
lahvičku	lahvička	k1gFnSc4	lahvička
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stačí	stačit	k5eAaBmIp3nS	stačit
na	na	k7c4	na
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
uvaří	uvařit	k5eAaPmIp3nP	uvařit
lektvar	lektvar	k1gInSc4	lektvar
za	za	k7c4	za
pomocí	pomoc	k1gFnPc2	pomoc
roztrhané	roztrhaný	k2eAgFnPc4d1	roztrhaná
<g/>
,	,	kIx,	,
vypůjčené	vypůjčený	k2eAgFnPc4d1	vypůjčená
knihy	kniha	k1gFnPc4	kniha
lektvary	lektvar	k1gInPc4	lektvar
pro	pro	k7c4	pro
pokročilé	pokročilý	k2eAgMnPc4d1	pokročilý
<g/>
.	.	kIx.	.
</s>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
ale	ale	k8xC	ale
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
stránek	stránka	k1gFnPc2	stránka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
užitečné	užitečný	k2eAgFnPc4d1	užitečná
rady	rada	k1gFnPc4	rada
a	a	k8xC	a
Harrymu	Harrymum	k1gNnSc6	Harrymum
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
přetrumfnout	přetrumfnout	k5eAaPmF	přetrumfnout
celou	celý	k2eAgFnSc4d1	celá
třídu	třída	k1gFnSc4	třída
včetně	včetně	k7c2	včetně
Hermiony	Hermion	k1gInPc4	Hermion
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obědě	oběd	k1gInSc6	oběd
řekne	říct	k5eAaPmIp3nS	říct
Ginny	Ginna	k1gFnPc4	Ginna
<g/>
,	,	kIx,	,
Ronovi	Ron	k1gMnSc3	Ron
a	a	k8xC	a
Hermioně	Hermiona	k1gFnSc3	Hermiona
<g/>
,	,	kIx,	,
že	že	k8xS	že
postupoval	postupovat	k5eAaImAgInS	postupovat
podle	podle	k7c2	podle
rad	rada	k1gFnPc2	rada
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Ginny	Ginna	k1gFnSc2	Ginna
<g/>
,	,	kIx,	,
když	když	k8xS	když
chodila	chodit	k5eAaImAgFnS	chodit
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
knihu	kniha	k1gFnSc4	kniha
přezkouší	přezkoušet	k5eAaPmIp3nP	přezkoušet
<g/>
,	,	kIx,	,
neobjeví	objevit	k5eNaPmIp3nP	objevit
žádné	žádný	k3yNgNnSc4	žádný
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
shodí	shodit	k5eAaPmIp3nS	shodit
a	a	k8xC	a
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
stránce	stránka	k1gFnSc6	stránka
najde	najít	k5eAaPmIp3nS	najít
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
majetek	majetek	k1gInSc4	majetek
prince	princ	k1gMnSc2	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Hermioně	Hermioň	k1gFnPc4	Hermioň
se	se	k3xPyFc4	se
ta	ten	k3xDgFnSc1	ten
kniha	kniha	k1gFnSc1	kniha
nelíbí	líbit	k5eNaImIp3nS	líbit
a	a	k8xC	a
naznačí	naznačit	k5eAaPmIp3nS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
princ	princ	k1gMnSc1	princ
<g/>
"	"	kIx"	"
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
i	i	k9	i
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
písmo	písmo	k1gNnSc1	písmo
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
vypadá	vypadat	k5eAaImIp3nS	vypadat
spíše	spíše	k9	spíše
jako	jako	k9	jako
ženské	ženský	k2eAgFnSc6d1	ženská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
argumentuje	argumentovat	k5eAaImIp3nS	argumentovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kolik	kolik	k4yRc1	kolik
holek	holka	k1gFnPc2	holka
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
princem	princ	k1gMnSc7	princ
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Ale	ale	k9	ale
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
Lilly	Lilla	k1gFnSc2	Lilla
Potterová	Potterový	k2eAgFnSc1d1	Potterová
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
původním	původní	k2eAgMnSc7d1	původní
vlastníkem	vlastník	k1gMnSc7	vlastník
knihy	kniha	k1gFnSc2	kniha
(	(	kIx(	(
<g/>
Křiklan	Křiklan	k1gMnSc1	Křiklan
sám	sám	k3xTgMnSc1	sám
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lilly	Lill	k1gInPc4	Lill
byla	být	k5eAaImAgFnS	být
mistryní	mistryně	k1gFnPc2	mistryně
v	v	k7c6	v
lektvarech	lektvar	k1gInPc6	lektvar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
odchází	odcházet	k5eAaImIp3nS	odcházet
na	na	k7c4	na
první	první	k4xOgFnSc4	první
soukromou	soukromý	k2eAgFnSc4d1	soukromá
hodinu	hodina	k1gFnSc4	hodina
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
učit	učit	k5eAaImF	učit
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
vypátrat	vypátrat	k5eAaPmF	vypátrat
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc1	jaký
jsou	být	k5eAaImIp3nP	být
Voldemortovy	Voldemortův	k2eAgInPc4d1	Voldemortův
úmysly	úmysl	k1gInPc4	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
lahvičku	lahvička	k1gFnSc4	lahvička
se	s	k7c7	s
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
Boba	Bob	k1gMnSc2	Bob
Odgena	Odgen	k1gMnSc2	Odgen
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
oddělení	oddělení	k1gNnSc6	oddělení
pro	pro	k7c4	pro
uplatňování	uplatňování	k1gNnSc4	uplatňování
kouzelnických	kouzelnický	k2eAgInPc2d1	kouzelnický
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
než	než	k8xS	než
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Vylije	vylít	k5eAaPmIp3nS	vylít
lahvičku	lahvička	k1gFnSc4	lahvička
do	do	k7c2	do
myslánky	myslánka	k1gFnSc2	myslánka
a	a	k8xC	a
s	s	k7c7	s
Harrym	Harrymum	k1gNnPc2	Harrymum
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
ponoří	ponořit	k5eAaPmIp3nS	ponořit
<g/>
.	.	kIx.	.
</s>
<s>
Navštíví	navštívit	k5eAaPmIp3nS	navštívit
vesnici	vesnice	k1gFnSc4	vesnice
Malý	Malý	k1gMnSc1	Malý
Visánek	Visánek	k1gMnSc1	Visánek
(	(	kIx(	(
<g/>
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
<g/>
,	,	kIx,	,
hřbitovem	hřbitov	k1gInSc7	hřbitov
<g/>
,	,	kIx,	,
panským	panský	k2eAgNnSc7d1	panské
sídlem	sídlo	k1gNnSc7	sídlo
a	a	k8xC	a
malým	malý	k2eAgNnSc7d1	malé
křovím	křoví	k1gNnSc7	křoví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dům	dům	k1gInSc4	dům
Gauntových	Gauntův	k2eAgMnPc2d1	Gauntův
<g/>
.	.	kIx.	.
</s>
<s>
Odgen	Odgen	k1gInSc1	Odgen
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
domu	dům	k1gInSc2	dům
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
zatknout	zatknout	k5eAaPmF	zatknout
muže	muž	k1gMnPc4	muž
jménem	jméno	k1gNnSc7	jméno
Morfin	morfin	k1gInSc1	morfin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mluví	mluvit	k5eAaImIp3nS	mluvit
hadím	hadí	k2eAgInSc7d1	hadí
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Rojvol	Rojvol	k1gInSc1	Rojvol
<g/>
,	,	kIx,	,
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
posledními	poslední	k2eAgMnPc7d1	poslední
potomky	potomek	k1gMnPc7	potomek
Salazara	Salazara	k1gFnSc1	Salazara
Zmijozela	Zmijozela	k1gFnSc1	Zmijozela
a	a	k8xC	a
že	že	k8xS	že
nesnáší	snášet	k5eNaImIp3nP	snášet
mudly	mudla	k1gFnPc4	mudla
a	a	k8xC	a
mudlovské	mudlovský	k2eAgNnSc4d1	mudlovské
šmejdy	šmejdy	k?	šmejdy
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc3	jeho
dceři	dcera	k1gFnSc3	dcera
Meropě	Meropa	k1gFnSc3	Meropa
(	(	kIx(	(
<g/>
Voldemortova	Voldemortův	k2eAgFnSc1d1	Voldemortova
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
líbí	líbit	k5eAaImIp3nS	líbit
mudla	mudla	k1gMnSc1	mudla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
Odgen	Odgen	k1gInSc1	Odgen
vrátí	vrátit	k5eAaPmIp3nS	vrátit
s	s	k7c7	s
posilou	posila	k1gFnSc7	posila
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zatkli	zatknout	k5eAaPmAgMnP	zatknout
otce	otec	k1gMnSc4	otec
i	i	k8xC	i
Morfina	Morfin	k1gMnSc4	Morfin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Merope	Merop	k1gInSc5	Merop
volná	volný	k2eAgNnPc4d1	volné
<g/>
.	.	kIx.	.
</s>
<s>
Očaruje	očarovat	k5eAaPmIp3nS	očarovat
mudlu	mudla	k1gFnSc4	mudla
Toma	Tom	k1gMnSc2	Tom
Raddlea	Raddleum	k1gNnSc2	Raddleum
st.	st.	kA	st.
a	a	k8xC	a
vezmou	vzít	k5eAaPmIp3nP	vzít
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
zbaví	zbavit	k5eAaPmIp3nS	zbavit
očarování	očarování	k1gNnSc1	očarování
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
opustí	opustit	k5eAaPmIp3nP	opustit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
hodinu	hodina	k1gFnSc4	hodina
končí	končit	k5eAaImIp3nS	končit
tady	tady	k6eAd1	tady
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnSc1d1	další
mu	on	k3xPp3gMnSc3	on
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
poranil	poranit	k5eAaPmAgMnS	poranit
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
normálně	normálně	k6eAd1	normálně
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
naučit	naučit	k5eAaPmF	naučit
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
nevyslovují	vyslovovat	k5eNaImIp3nP	vyslovovat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
půjde	jít	k5eAaImIp3nS	jít
promluvit	promluvit	k5eAaPmF	promluvit
s	s	k7c7	s
Hagridem	Hagrid	k1gInSc7	Hagrid
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
musí	muset	k5eAaImIp3nS	muset
jít	jít	k5eAaImF	jít
na	na	k7c4	na
zkoušky	zkouška	k1gFnPc4	zkouška
famfrpálu	famfrpál	k1gInSc2	famfrpál
a	a	k8xC	a
vybrat	vybrat	k5eAaPmF	vybrat
nový	nový	k2eAgInSc4d1	nový
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
jen	jen	k9	jen
tak	tak	k6eAd1	tak
zmíní	zmínit	k5eAaPmIp3nS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Harry	Harr	k1gInPc1	Harr
"	"	kIx"	"
<g/>
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
populární	populární	k2eAgMnSc1d1	populární
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
dívkami	dívka	k1gFnPc7	dívka
a	a	k8xC	a
že	že	k8xS	že
"	"	kIx"	"
<g/>
ani	ani	k8xC	ani
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsi	být	k5eAaImIp2nS	být
přes	přes	k7c4	přes
prázdniny	prázdniny	k1gFnPc4	prázdniny
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
skoro	skoro	k6eAd1	skoro
o	o	k7c4	o
třicet	třicet	k4xCc4	třicet
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Ron	Ron	k1gMnSc1	Ron
odvětí	odvětit	k5eAaPmIp3nS	odvětit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
také	také	k9	také
dost	dost	k6eAd1	dost
velký	velký	k2eAgInSc1d1	velký
(	(	kIx(	(
<g/>
Hermiona	Hermiona	k1gFnSc1	Hermiona
totiž	totiž	k9	totiž
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
Harryho	Harry	k1gMnSc4	Harry
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
Ronovi	Ronův	k2eAgMnPc1d1	Ronův
moc	moc	k6eAd1	moc
nelíbí	líbit	k5eNaImIp3nP	líbit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harrymu	Harrym	k1gInSc3	Harrym
je	být	k5eAaImIp3nS	být
nějak	nějak	k6eAd1	nějak
horko	horko	k1gNnSc1	horko
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
počasí	počasí	k1gNnSc1	počasí
je	být	k5eAaImIp3nS	být
bídné	bídný	k2eAgNnSc1d1	bídné
<g/>
.	.	kIx.	.
</s>
<s>
Průvodčí	průvodčí	k1gMnPc1	průvodčí
ze	z	k7c2	z
Záchranného	záchranný	k2eAgInSc2d1	záchranný
autobusu	autobus	k1gInSc2	autobus
<g/>
,	,	kIx,	,
Stan	stan	k1gInSc1	stan
Silnička	silnička	k1gFnSc1	silnička
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
projednával	projednávat	k5eAaImAgInS	projednávat
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
plány	plán	k1gInPc4	plán
Smrtijedů	Smrtijed	k1gMnPc2	Smrtijed
<g/>
,	,	kIx,	,
a	a	k8xC	a
Brumbála	brumbál	k1gMnSc2	brumbál
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
soukromé	soukromý	k2eAgFnSc2d1	soukromá
hodiny	hodina	k1gFnSc2	hodina
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
nikdo	nikdo	k3yNnSc1	nikdo
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Eloise	Eloise	k1gFnSc1	Eloise
Midgenová	Midgenová	k1gFnSc1	Midgenová
odešla	odejít	k5eAaPmAgFnS	odejít
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
rodičů	rodič	k1gMnPc2	rodič
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
báli	bát	k5eAaImAgMnP	bát
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
matku	matka	k1gFnSc4	matka
Hannah	Hannaha	k1gFnPc2	Hannaha
Abbottové	Abbottové	k2eAgFnPc2d1	Abbottové
před	před	k7c7	před
nedávnem	nedávno	k1gNnSc7	nedávno
nalezli	naleznout	k5eAaPmAgMnP	naleznout
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
trojice	trojice	k1gFnSc1	trojice
projde	projít	k5eAaPmIp3nS	projít
kolem	kolem	k7c2	kolem
Parvati	Parvati	k1gFnSc2	Parvati
a	a	k8xC	a
Levandule	levandule	k1gFnSc2	levandule
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Parvati	Parvati	k1gFnSc1	Parvati
šťouchne	šťouchnout	k5eAaPmIp3nS	šťouchnout
do	do	k7c2	do
Levandule	levandule	k1gFnSc2	levandule
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
Rona	Ron	k1gMnSc4	Ron
usměje	usmát	k5eAaPmIp3nS	usmát
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
také	také	k9	také
nejistě	jistě	k6eNd1	jistě
usměje	usmát	k5eAaPmIp3nS	usmát
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
je	být	k5eAaImIp3nS	být
najednou	najednou	k6eAd1	najednou
chladná	chladný	k2eAgFnSc1d1	chladná
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
si	se	k3xPyFc3	se
odstup	odstup	k1gInSc4	odstup
a	a	k8xC	a
ani	ani	k8xC	ani
mu	on	k3xPp3gInSc3	on
nepopřeje	popřát	k5eNaPmIp3nS	popřát
hodně	hodně	k6eAd1	hodně
štěstí	štěstí	k1gNnSc1	štěstí
na	na	k7c6	na
zkouškách	zkouška	k1gFnPc6	zkouška
do	do	k7c2	do
famfrpálového	famfrpálový	k2eAgNnSc2d1	famfrpálové
družstva	družstvo	k1gNnSc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušky	zkouška	k1gFnPc1	zkouška
nedopadnou	dopadnout	k5eNaPmIp3nP	dopadnout
nic	nic	k3yNnSc4	nic
moc	moc	k6eAd1	moc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harry	Harr	k1gInPc4	Harr
si	se	k3xPyFc3	se
vybere	vybrat	k5eAaPmIp3nS	vybrat
Katie	Katie	k1gFnSc1	Katie
Bellovou	Bellová	k1gFnSc4	Bellová
<g/>
,	,	kIx,	,
Ginny	Ginna	k1gFnPc4	Ginna
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
hráčku	hráčka	k1gFnSc4	hráčka
Demelzu	Demelza	k1gFnSc4	Demelza
Robinsovou	Robinsová	k1gFnSc4	Robinsová
jako	jako	k8xC	jako
střelce	střelec	k1gMnSc2	střelec
<g/>
,	,	kIx,	,
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Peakse	Peaks	k1gMnSc2	Peaks
a	a	k8xC	a
Ritchieho	Ritchie	k1gMnSc2	Ritchie
Coota	Coot	k1gMnSc2	Coot
jako	jako	k8xS	jako
odrážeče	odrážeč	k1gMnSc2	odrážeč
a	a	k8xC	a
Rona	Ron	k1gMnSc2	Ron
jako	jako	k8xC	jako
brankáře	brankář	k1gMnSc2	brankář
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poněkud	poněkud	k6eAd1	poněkud
naštve	naštvat	k5eAaBmIp3nS	naštvat
Cormaca	Cormac	k2eAgFnSc1d1	Cormac
McLoggana	McLoggana	k1gFnSc1	McLoggana
<g/>
.	.	kIx.	.
</s>
<s>
Levandule	levandule	k1gFnSc1	levandule
Brownová	Brownová	k1gFnSc1	Brownová
popřeje	popřát	k5eAaPmIp3nS	popřát
Ronovi	Ron	k1gMnSc3	Ron
hodně	hodně	k6eAd1	hodně
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
když	když	k8xS	když
letí	letět	k5eAaImIp3nS	letět
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
to	ten	k3xDgNnSc4	ten
zaslechne	zaslechnout	k5eAaPmIp3nS	zaslechnout
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
Ronovi	Ron	k1gMnSc3	Ron
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
výborný	výborný	k2eAgMnSc1d1	výborný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naštve	naštvat	k5eAaBmIp3nS	naštvat
Levanduli	levandule	k1gFnSc4	levandule
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
je	být	k5eAaImIp3nS	být
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
Hermiona	Hermiona	k1gFnSc1	Hermiona
pochválila	pochválit	k5eAaPmAgFnS	pochválit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
potom	potom	k6eAd1	potom
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
Hagridova	Hagridův	k2eAgInSc2d1	Hagridův
domku	domek	k1gInSc2	domek
<g/>
,	,	kIx,	,
řekne	říct	k5eAaPmIp3nS	říct
Ron	ron	k1gInSc1	ron
<g/>
,	,	kIx,	,
že	že	k8xS	že
McLoggan	McLoggan	k1gInSc1	McLoggan
vypadal	vypadat	k5eAaPmAgInS	vypadat
jako	jako	k9	jako
zmatený	zmatený	k2eAgMnSc1d1	zmatený
<g/>
,	,	kIx,	,
když	když	k8xS	když
chytal	chytat	k5eAaImAgMnS	chytat
branku	branka	k1gFnSc4	branka
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
začervená	začervenat	k5eAaPmIp3nS	začervenat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ron	Ron	k1gMnSc1	Ron
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
nevšimne	všimnout	k5eNaPmIp3nS	všimnout
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Hagridem	Hagrid	k1gInSc7	Hagrid
je	být	k5eAaImIp3nS	být
rozpačitý	rozpačitý	k2eAgMnSc1d1	rozpačitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Aragog	Aragog	k1gInSc1	Aragog
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
trojice	trojice	k1gFnSc1	trojice
mu	on	k3xPp3gMnSc3	on
(	(	kIx(	(
<g/>
nepravdivě	pravdivě	k6eNd1	pravdivě
<g/>
)	)	kIx)	)
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
profesorka	profesorka	k1gFnSc1	profesorka
Červotočková	Červotočková	k1gFnSc1	Červotočková
je	být	k5eAaImIp3nS	být
hrozná	hrozný	k2eAgFnSc1d1	hrozná
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
.	.	kIx.	.
</s>
<s>
Horacio	Horacio	k1gMnSc1	Horacio
Křiklan	Křiklan	k1gMnSc1	Křiklan
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
večeří	večeře	k1gFnSc7	večeře
zastaví	zastavit	k5eAaPmIp3nS	zastavit
a	a	k8xC	a
pozve	pozvat	k5eAaPmIp3nS	pozvat
Harryho	Harry	k1gMnSc4	Harry
a	a	k8xC	a
Hermionu	Hermion	k1gInSc2	Hermion
na	na	k7c4	na
"	"	kIx"	"
<g/>
malinkou	malinký	k2eAgFnSc4d1	malinká
party	party	k1gFnSc4	party
<g/>
,	,	kIx,	,
jen	jen	k9	jen
několik	několik	k4yIc1	několik
vycházejících	vycházející	k2eAgFnPc2d1	vycházející
hvězdiček	hvězdička	k1gFnPc2	hvězdička
<g/>
"	"	kIx"	"
a	a	k8xC	a
Rona	Ron	k1gMnSc2	Ron
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
zcela	zcela	k6eAd1	zcela
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
školní	školní	k2eAgInSc4d1	školní
trest	trest	k1gInSc4	trest
se	s	k7c7	s
Snapem	Snap	k1gMnSc7	Snap
a	a	k8xC	a
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
přijít	přijít	k5eAaPmF	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Křiklan	Křiklan	k1gInSc1	Křiklan
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
přemluvit	přemluvit	k5eAaPmF	přemluvit
Snapea	Snapeus	k1gMnSc4	Snapeus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
pustil	pustit	k5eAaPmAgMnS	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
večer	večer	k6eAd1	večer
<g/>
,	,	kIx,	,
než	než	k8xS	než
jde	jít	k5eAaImIp3nS	jít
Harry	Harra	k1gMnSc2	Harra
ke	k	k7c3	k
Snapeovi	Snapeus	k1gMnSc3	Snapeus
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
Hermiona	Hermiona	k1gFnSc1	Hermiona
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Artur	Artur	k1gMnSc1	Artur
Weasley	Weaslea	k1gFnSc2	Weaslea
prohledal	prohledat	k5eAaPmAgMnS	prohledat
rezidenci	rezidence	k1gFnSc4	rezidence
Malfoyových	Malfoyových	k2eAgMnSc1d1	Malfoyových
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
nenašel	najít	k5eNaPmAgMnS	najít
nic	nic	k3yNnSc4	nic
a	a	k8xC	a
že	že	k8xS	že
Malfoy	Malfo	k1gMnPc4	Malfo
nemohl	moct	k5eNaImAgMnS	moct
tu	tu	k6eAd1	tu
věc	věc	k1gFnSc4	věc
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
nikam	nikam	k6eAd1	nikam
odnést	odnést	k5eAaPmF	odnést
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
všechno	všechen	k3xTgNnSc1	všechen
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
detektory	detektor	k1gInPc7	detektor
temných	temný	k2eAgFnPc2d1	temná
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
Ron	Ron	k1gMnSc1	Ron
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
zírá	zírat	k5eAaImIp3nS	zírat
na	na	k7c4	na
Levanduli	levandule	k1gFnSc4	levandule
Brownovou	Brownový	k2eAgFnSc4d1	Brownová
<g/>
.	.	kIx.	.
</s>
<s>
Demelza	Demelza	k1gFnSc1	Demelza
přinese	přinést	k5eAaPmIp3nS	přinést
Harrymu	Harrym	k1gInSc2	Harrym
vzkaz	vzkaz	k1gInSc4	vzkaz
od	od	k7c2	od
Snapea	Snape	k1gInSc2	Snape
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
očekává	očekávat	k5eAaImIp3nS	očekávat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
kabinetu	kabinet	k1gInSc6	kabinet
a	a	k8xC	a
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nezajímá	zajímat	k5eNaImIp3nS	zajímat
<g/>
,	,	kIx,	,
na	na	k7c4	na
kolik	kolik	k4yQc4	kolik
party	party	k1gFnPc2	party
byl	být	k5eAaImAgInS	být
pozván	pozván	k2eAgInSc1d1	pozván
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
a	a	k8xC	a
čte	číst	k5eAaImIp3nS	číst
si	se	k3xPyFc3	se
knihu	kniha	k1gFnSc4	kniha
prince	princ	k1gMnSc2	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
přijít	přijít	k5eAaPmF	přijít
vhod	vhod	k6eAd1	vhod
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
vyzkouší	vyzkoušet	k5eAaPmIp3nS	vyzkoušet
jedno	jeden	k4xCgNnSc4	jeden
bezeslovné	bezeslovný	k2eAgNnSc4d1	bezeslovné
kouzlo	kouzlo	k1gNnSc4	kouzlo
na	na	k7c4	na
levitování	levitování	k1gNnSc4	levitování
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgFnPc1d1	podobná
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
Snapeově	Snapeův	k2eAgFnSc6d1	Snapeova
vzpomínce	vzpomínka	k1gFnSc6	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
snad	snad	k9	snad
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
princem	princ	k1gMnSc7	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zavrhne	zavrhnout	k5eAaPmIp3nS	zavrhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
čisté	čistý	k2eAgFnSc2d1	čistá
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
naplánuje	naplánovat	k5eAaBmIp3nS	naplánovat
další	další	k2eAgFnSc4d1	další
hodinu	hodina	k1gFnSc4	hodina
na	na	k7c4	na
pondělní	pondělní	k2eAgInSc4d1	pondělní
večer	večer	k1gInSc4	večer
<g/>
,	,	kIx,	,
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
Křiklan	Křiklan	k1gInSc4	Křiklan
další	další	k2eAgInSc4d1	další
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
večírků	večírek	k1gInPc2	večírek
<g/>
.	.	kIx.	.
</s>
<s>
Křiklan	Křiklan	k1gInSc1	Křiklan
tam	tam	k6eAd1	tam
chce	chtít	k5eAaImIp3nS	chtít
mít	mít	k5eAaImF	mít
Harryho	Harry	k1gMnSc4	Harry
společně	společně	k6eAd1	společně
s	s	k7c7	s
Hermionou	Hermiona	k1gFnSc7	Hermiona
a	a	k8xC	a
Ginny	Ginn	k1gMnPc7	Ginn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neustále	neustále	k6eAd1	neustále
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
maří	mařit	k5eAaImIp3nS	mařit
jiné	jiný	k2eAgFnPc4d1	jiná
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
také	také	k9	také
první	první	k4xOgInSc1	první
výlet	výlet	k1gInSc1	výlet
do	do	k7c2	do
Prasinek	Prasinka	k1gFnPc2	Prasinka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
odmítnou	odmítnout	k5eAaPmIp3nP	odmítnout
pozvání	pozvání	k1gNnSc4	pozvání
Křiklana	Křiklan	k1gMnSc4	Křiklan
<g/>
,	,	kIx,	,
potkají	potkat	k5eAaPmIp3nP	potkat
hostinského	hostinský	k1gMnSc4	hostinský
z	z	k7c2	z
Prasečí	prasečí	k2eAgFnSc2d1	prasečí
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
Mundunguse	Mundunguse	k1gFnSc2	Mundunguse
Fletchera	Fletchero	k1gNnSc2	Fletchero
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
bednu	bedna	k1gFnSc4	bedna
plnou	plný	k2eAgFnSc7d1	plná
kradeného	kradený	k2eAgNnSc2d1	kradené
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
poháru	pohár	k1gInSc2	pohár
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
Blacků	Black	k1gInPc2	Black
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
chytí	chytit	k5eAaPmIp3nP	chytit
Mundunguse	Mundunguse	k1gFnPc4	Mundunguse
pod	pod	k7c7	pod
krkem	krk	k1gInSc7	krk
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
vzít	vzít	k5eAaPmF	vzít
zpátky	zpátky	k6eAd1	zpátky
a	a	k8xC	a
nařkne	nařknout	k5eAaPmIp3nS	nařknout
Fletchera	Fletchera	k1gFnSc1	Fletchera
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Siriuse	Siriuse	k1gFnSc2	Siriuse
ten	ten	k3xDgInSc4	ten
dům	dům	k1gInSc4	dům
vykradl	vykrást	k5eAaPmAgMnS	vykrást
<g/>
.	.	kIx.	.
</s>
<s>
Fletcher	Fletchra	k1gFnPc2	Fletchra
Harryho	Harry	k1gMnSc4	Harry
odstrčí	odstrčit	k5eAaPmIp3nP	odstrčit
a	a	k8xC	a
přemístí	přemístit	k5eAaPmIp3nP	přemístit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
Tonksová	Tonksová	k1gFnSc1	Tonksová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nemá	mít	k5eNaImIp3nS	mít
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Trojice	trojice	k1gFnSc1	trojice
je	být	k5eAaImIp3nS	být
chvíli	chvíle	k1gFnSc4	chvíle
U	u	k7c2	u
tří	tři	k4xCgNnPc2	tři
košťat	koště	k1gNnPc2	koště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
se	se	k3xPyFc4	se
jít	jít	k5eAaImF	jít
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
jdou	jít	k5eAaImIp3nP	jít
za	za	k7c4	za
Katie	Katius	k1gMnPc4	Katius
Bellovou	Bellová	k1gFnSc4	Bellová
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
kamarádkou	kamarádka	k1gFnSc7	kamarádka
Leanne	Leann	k1gMnSc5	Leann
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
Katie	Katie	k1gFnSc1	Katie
a	a	k8xC	a
Leanne	Leann	k1gInSc5	Leann
začnou	začít	k5eAaPmIp3nP	začít
hádat	hádat	k5eAaImF	hádat
kvůli	kvůli	k7c3	kvůli
balíčku	balíček	k1gInSc3	balíček
<g/>
.	.	kIx.	.
</s>
<s>
Leanne	Leannout	k5eAaImIp3nS	Leannout
ho	on	k3xPp3gInSc4	on
chce	chtít	k5eAaImIp3nS	chtít
vzít	vzít	k5eAaPmF	vzít
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
roztrhne	roztrhnout	k5eAaPmIp3nS	roztrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Katie	Katie	k1gFnSc1	Katie
se	se	k3xPyFc4	se
s	s	k7c7	s
křikem	křik	k1gInSc7	křik
zvedne	zvednout	k5eAaPmIp3nS	zvednout
do	do	k7c2	do
dvoumetrové	dvoumetrový	k2eAgFnSc2d1	dvoumetrová
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
spadne	spadnout	k5eAaPmIp3nS	spadnout
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
svíjí	svíjet	k5eAaImIp3nS	svíjet
se	se	k3xPyFc4	se
a	a	k8xC	a
křičí	křičet	k5eAaImIp3nS	křičet
<g/>
.	.	kIx.	.
</s>
<s>
Přijde	přijít	k5eAaPmIp3nS	přijít
Hagrid	Hagrid	k1gInSc4	Hagrid
a	a	k8xC	a
odnese	odnést	k5eAaPmIp3nS	odnést
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
dotknout	dotknout	k5eAaPmF	dotknout
balíčku	balíček	k1gInSc3	balíček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harry	Harra	k1gFnPc1	Harra
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
zabrání	zabrání	k1gNnSc6	zabrání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
balíčku	balíček	k1gInSc6	balíček
je	být	k5eAaImIp3nS	být
náhrdelník	náhrdelník	k1gInSc1	náhrdelník
od	od	k7c2	od
Borgina	Borgin	k1gMnSc2	Borgin
&	&	k?	&
Burkese	Burkese	k1gFnSc2	Burkese
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgMnSc4	který
měl	mít	k5eAaImAgMnS	mít
cedulku	cedulka	k1gFnSc4	cedulka
"	"	kIx"	"
<g/>
prokletý	prokletý	k2eAgMnSc1d1	prokletý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Leanne	Leannout	k5eAaImIp3nS	Leannout
jim	on	k3xPp3gMnPc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Katie	Katie	k1gFnSc1	Katie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
chovat	chovat	k5eAaImF	chovat
divně	divně	k6eAd1	divně
<g/>
,	,	kIx,	,
když	když	k8xS	když
přišla	přijít	k5eAaPmAgFnS	přijít
ze	z	k7c2	z
záchoda	záchod	k1gMnSc2	záchod
a	a	k8xC	a
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
držela	držet	k5eAaImAgFnS	držet
balíček	balíček	k1gInSc4	balíček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
měla	mít	k5eAaImAgFnS	mít
doručit	doručit	k5eAaPmF	doručit
<g/>
.	.	kIx.	.
</s>
<s>
Domnívala	domnívat	k5eAaImAgFnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Katie	Katie	k1gFnSc1	Katie
je	být	k5eAaImIp3nS	být
ovládána	ovládat	k5eAaImNgFnS	ovládat
kletbou	kletba	k1gFnSc7	kletba
Imperius	Imperius	k1gInSc1	Imperius
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
chtěla	chtít	k5eAaImAgFnS	chtít
ten	ten	k3xDgInSc4	ten
balíček	balíček	k1gInSc4	balíček
vzít	vzít	k5eAaPmF	vzít
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
roztrhl	roztrhnout	k5eAaPmAgMnS	roztrhnout
a	a	k8xC	a
Katie	Katie	k1gFnSc1	Katie
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
dotkla	dotknout	k5eAaPmAgFnS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Dostaví	dostavit	k5eAaPmIp3nS	dostavit
se	se	k3xPyFc4	se
profesorka	profesorka	k1gFnSc1	profesorka
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
a	a	k8xC	a
odvede	odvést	k5eAaPmIp3nS	odvést
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jí	on	k3xPp3gFnSc2	on
Harry	Harra	k1gFnSc2	Harra
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
podezřívá	podezřívat	k5eAaImIp3nS	podezřívat
Malfoye	Malfoye	k1gFnSc1	Malfoye
<g/>
.	.	kIx.	.
</s>
<s>
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
to	ten	k3xDgNnSc4	ten
popře	popřít	k5eAaPmIp3nS	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
prý	prý	k9	prý
měl	mít	k5eAaImAgInS	mít
Malfoy	Malfoy	k1gInPc7	Malfoy
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
školní	školní	k2eAgFnSc1d1	školní
trest	trest	k1gInSc1	trest
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harry	Harra	k1gFnPc1	Harra
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
komplice	komplic	k1gMnSc4	komplic
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
a	a	k8xC	a
Hermiona	Hermion	k1gMnSc4	Hermion
už	už	k9	už
Harryho	Harry	k1gMnSc4	Harry
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c6	o
Malfoyovi	Malfoya	k1gMnSc6	Malfoya
neberou	brát	k5eNaImIp3nP	brát
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
má	mít	k5eAaImIp3nS	mít
druhou	druhý	k4xOgFnSc4	druhý
hodinu	hodina	k1gFnSc4	hodina
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Posledně	posledně	k6eAd1	posledně
byla	být	k5eAaImAgFnS	být
Meropa	Meropa	k1gFnSc1	Meropa
(	(	kIx(	(
<g/>
Voldemortova	Voldemortův	k2eAgFnSc1d1	Voldemortova
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
těhotná	těhotný	k2eAgFnSc1d1	těhotná
<g/>
.	.	kIx.	.
</s>
<s>
Potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nedokázala	dokázat	k5eNaPmAgNnP	dokázat
použít	použít	k5eAaPmF	použít
kouzla	kouzlo	k1gNnPc1	kouzlo
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uživila	uživit	k5eAaPmAgFnS	uživit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Kataraktusovi	Kataraktus	k1gMnSc3	Kataraktus
Burkesovi	Burkes	k1gMnSc3	Burkes
(	(	kIx(	(
<g/>
jednomu	jeden	k4xCgMnSc3	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
obchodu	obchod	k1gInSc2	obchod
Borgin	Borgin	k1gMnSc1	Borgin
&	&	k?	&
Burkes	Burkes	k1gMnSc1	Burkes
<g/>
)	)	kIx)	)
prodala	prodat	k5eAaPmAgFnS	prodat
zlatý	zlatý	k2eAgInSc4d1	zlatý
medailonek	medailonek	k1gInSc4	medailonek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kdysi	kdysi	k6eAd1	kdysi
patřil	patřit	k5eAaImAgMnS	patřit
Salazaru	Salazar	k1gMnSc3	Salazar
Zmijozelovi	Zmijozel	k1gMnSc3	Zmijozel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
cenný	cenný	k2eAgInSc4d1	cenný
náhrdelník	náhrdelník	k1gInSc4	náhrdelník
jí	jíst	k5eAaImIp3nS	jíst
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
galeonů	galeon	k1gInPc2	galeon
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
Harry	Harr	k1gInPc7	Harr
podívá	podívat	k5eAaPmIp3nS	podívat
do	do	k7c2	do
Brumbálových	brumbálův	k2eAgFnPc2d1	Brumbálova
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
navštíví	navštívit	k5eAaPmIp3nS	navštívit
mudlovský	mudlovský	k2eAgInSc4d1	mudlovský
sirotčinec	sirotčinec	k1gInSc4	sirotčinec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
promluvil	promluvit	k5eAaPmAgMnS	promluvit
s	s	k7c7	s
jedenáctiletým	jedenáctiletý	k2eAgMnSc7d1	jedenáctiletý
Tomem	Tom	k1gMnSc7	Tom
Raddlem	Raddlo	k1gNnSc7	Raddlo
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
sestra	sestra	k1gFnSc1	sestra
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Raddle	Raddle	k1gMnSc1	Raddle
je	být	k5eAaImIp3nS	být
divný	divný	k2eAgMnSc1d1	divný
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Brumbál	brumbál	k1gMnSc1	brumbál
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jde	jít	k5eAaImIp3nS	jít
osobně	osobně	k6eAd1	osobně
promluvit	promluvit	k5eAaPmF	promluvit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jemu	on	k3xPp3gMnSc3	on
teprve	teprve	k6eAd1	teprve
11	[number]	k4	11
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
už	už	k6eAd1	už
manipuluje	manipulovat	k5eAaImIp3nS	manipulovat
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
škrtí	škrtit	k5eAaImIp3nP	škrtit
králíky	králík	k1gMnPc4	králík
<g/>
,	,	kIx,	,
zraňuje	zraňovat	k5eAaImIp3nS	zraňovat
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nP	mluvit
hadím	hadí	k2eAgInSc7d1	hadí
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mladého	mladý	k2eAgMnSc4d1	mladý
Brumbála	brumbál	k1gMnSc4	brumbál
znepokojuje	znepokojovat	k5eAaImIp3nS	znepokojovat
jeho	jeho	k3xOp3gNnSc1	jeho
ctižádost	ctižádost	k1gFnSc1	ctižádost
<g/>
,	,	kIx,	,
krutost	krutost	k1gFnSc1	krutost
<g/>
,	,	kIx,	,
krádeže	krádež	k1gFnPc1	krádež
a	a	k8xC	a
podobné	podobný	k2eAgFnPc1d1	podobná
věci	věc	k1gFnPc1	věc
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nespustí	spustit	k5eNaPmIp3nS	spustit
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
pracovny	pracovna	k1gFnSc2	pracovna
se	se	k3xPyFc4	se
Brumbál	brumbál	k1gMnSc1	brumbál
přizná	přiznat	k5eAaPmIp3nS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdy	tehdy	k6eAd1	tehdy
vůbec	vůbec	k9	vůbec
neměl	mít	k5eNaImAgMnS	mít
tušení	tušení	k1gNnSc4	tušení
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
tohoto	tento	k3xDgMnSc2	tento
malého	malý	k2eAgMnSc2d1	malý
kluka	kluk	k1gMnSc2	kluk
jednou	jednou	k6eAd1	jednou
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
ten	ten	k3xDgMnSc1	ten
nejobávanější	obávaný	k2eAgMnSc1d3	nejobávanější
zlý	zlý	k2eAgMnSc1d1	zlý
čaroděj	čaroděj	k1gMnSc1	čaroděj
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Trojice	trojice	k1gFnSc1	trojice
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
hodině	hodina	k1gFnSc6	hodina
bylinkářství	bylinkářství	k1gNnSc2	bylinkářství
a	a	k8xC	a
probírají	probírat	k5eAaImIp3nP	probírat
právě	právě	k9	právě
rostlinu	rostlina	k1gFnSc4	rostlina
s	s	k7c7	s
názvem	název	k1gInSc7	název
Snargaluff	Snargaluff	k1gMnSc1	Snargaluff
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
hodně	hodně	k6eAd1	hodně
chapadýlek	chapadýlek	k1gInSc4	chapadýlek
a	a	k8xC	a
uvnitř	uvnitř	k7c2	uvnitř
ní	on	k3xPp3gFnSc2	on
rostou	růst	k5eAaImIp3nP	růst
lusky	luska	k1gFnPc1	luska
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
grapefruitu	grapefruit	k1gInSc2	grapefruit
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
řekne	říct	k5eAaPmIp3nS	říct
Ronovi	Ron	k1gMnSc3	Ron
a	a	k8xC	a
Harrymu	Harrym	k1gInSc6	Harrym
<g/>
,	,	kIx,	,
že	že	k8xS	že
večeře	večeře	k1gFnSc1	večeře
Křikova	Křikův	k2eAgInSc2d1	Křikův
klubu	klub	k1gInSc2	klub
jsou	být	k5eAaImIp3nP	být
docela	docela	k6eAd1	docela
zábavné	zábavný	k2eAgFnPc1d1	zábavná
a	a	k8xC	a
že	že	k8xS	že
Harry	Harr	k1gInPc1	Harr
musí	muset	k5eAaImIp3nP	muset
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
vánoční	vánoční	k2eAgFnSc4d1	vánoční
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Křiklan	Křiklan	k1gMnSc1	Křiklan
si	se	k3xPyFc3	se
vybere	vybrat	k5eAaPmIp3nS	vybrat
den	den	k1gInSc4	den
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tam	tam	k6eAd1	tam
mohl	moct	k5eAaImAgInS	moct
Harry	Harra	k1gFnSc2	Harra
přijít	přijít	k5eAaPmF	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
se	se	k3xPyFc4	se
naštve	naštvat	k5eAaPmIp3nS	naštvat
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
Hermioně	Hermioň	k1gFnPc4	Hermioň
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
tam	tam	k6eAd1	tam
klidně	klidně	k6eAd1	klidně
jde	jít	k5eAaImIp3nS	jít
třeba	třeba	k6eAd1	třeba
s	s	k7c7	s
McLaggenem	McLaggen	k1gInSc7	McLaggen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
mohli	moct	k5eAaImAgMnP	moct
udělat	udělat	k5eAaPmF	udělat
"	"	kIx"	"
<g/>
Krále	Král	k1gMnSc4	Král
a	a	k8xC	a
královnu	královna	k1gFnSc4	královna
křiklounů	křikloun	k1gMnPc2	křikloun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
rozhořčeně	rozhořčeně	k6eAd1	rozhořčeně
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
chtěla	chtít	k5eAaImAgFnS	chtít
pozvat	pozvat	k5eAaPmF	pozvat
Rona	Ron	k1gMnSc4	Ron
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc1	takový
protivný	protivný	k2eAgInSc1d1	protivný
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vykašlat	vykašlat	k5eAaPmF	vykašlat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
si	se	k3xPyFc3	se
pomyslí	pomyslet	k5eAaPmIp3nP	pomyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
dají	dát	k5eAaPmIp3nP	dát
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
rozejdou	rozejít	k5eAaPmIp3nP	rozejít
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nepřeklenutelná	překlenutelný	k2eNgFnSc1d1	nepřeklenutelná
propast	propast	k1gFnSc1	propast
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
je	on	k3xPp3gMnPc4	on
nebude	být	k5eNaImBp3nS	být
umět	umět	k5eAaImF	umět
smířit	smířit	k5eAaPmF	smířit
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
si	se	k3xPyFc3	se
budou	být	k5eAaImBp3nP	být
tak	tak	k6eAd1	tak
blízcí	blízký	k2eAgMnPc1d1	blízký
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
sebe	sebe	k3xPyFc4	sebe
Harryho	Harry	k1gMnSc2	Harry
nepustí	pustit	k5eNaPmIp3nP	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Řekne	říct	k5eAaPmIp3nS	říct
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
počká	počkat	k5eAaPmIp3nS	počkat
a	a	k8xC	a
uvidí	uvidět	k5eAaPmIp3nS	uvidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Katie	Katie	k1gFnSc1	Katie
Bellová	Bellová	k1gFnSc1	Bellová
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
pořád	pořád	k6eAd1	pořád
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
chybí	chybit	k5eAaPmIp3nS	chybit
jeden	jeden	k4xCgMnSc1	jeden
střelec	střelec	k1gMnSc1	střelec
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
vybere	vybrat	k5eAaPmIp3nS	vybrat
Deana	Dean	k1gMnSc4	Dean
Thomase	Thomas	k1gMnSc4	Thomas
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dobře	dobře	k6eAd1	dobře
létá	létat	k5eAaImIp3nS	létat
<g/>
.	.	kIx.	.
</s>
<s>
Dean	Dean	k1gMnSc1	Dean
je	být	k5eAaImIp3nS	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
běží	běžet	k5eAaImIp3nS	běžet
tu	ten	k3xDgFnSc4	ten
novinku	novinka	k1gFnSc4	novinka
sdělit	sdělit	k5eAaPmF	sdělit
Ginny	Ginna	k1gFnPc4	Ginna
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
střelcem	střelec	k1gMnSc7	střelec
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
spolu	spolu	k6eAd1	spolu
chodí	chodit	k5eAaImIp3nP	chodit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seamus	Seamus	k1gMnSc1	Seamus
se	se	k3xPyFc4	se
ale	ale	k9	ale
cítí	cítit	k5eAaImIp3nS	cítit
poněkud	poněkud	k6eAd1	poněkud
v	v	k7c6	v
rozpacích	rozpak	k1gInPc6	rozpak
<g/>
.	.	kIx.	.
</s>
<s>
Nebelvírským	Nebelvírský	k2eAgMnPc3d1	Nebelvírský
studentům	student	k1gMnPc3	student
se	se	k3xPyFc4	se
moc	moc	k6eAd1	moc
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Harry	Harra	k1gFnPc4	Harra
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
vybral	vybrat	k5eAaPmAgMnS	vybrat
dalšího	další	k2eAgMnSc4d1	další
spolužáka	spolužák	k1gMnSc4	spolužák
z	z	k7c2	z
ročníku	ročník	k1gInSc2	ročník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harry	Harra	k1gFnPc1	Harra
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Dean	Dean	k1gMnSc1	Dean
šikovný	šikovný	k2eAgMnSc1d1	šikovný
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
předvedl	předvést	k5eAaPmAgMnS	předvést
na	na	k7c6	na
tréninku	trénink	k1gInSc6	trénink
před	před	k7c7	před
zápasem	zápas	k1gInSc7	zápas
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
odrážečům	odrážeč	k1gMnPc3	odrážeč
to	ten	k3xDgNnSc1	ten
jde	jít	k5eAaImIp3nS	jít
skvěle	skvěle	k6eAd1	skvěle
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Ron	Ron	k1gMnSc1	Ron
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Zase	zase	k9	zase
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vrátila	vrátit	k5eAaPmAgFnS	vrátit
nervozita	nervozita	k1gFnSc1	nervozita
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nervozní	rvozní	k2eNgNnSc1d1	nervozní
<g/>
,	,	kIx,	,
že	že	k8xS	že
udeří	udeřit	k5eAaPmIp3nS	udeřit
Demelzu	Demelza	k1gFnSc4	Demelza
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gMnSc2	Harra
Rona	Ron	k1gMnSc2	Ron
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
očividně	očividně	k6eAd1	očividně
cítí	cítit	k5eAaImIp3nS	cítit
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
nebelvírské	belvírský	k2eNgFnSc2d1	nebelvírská
společenské	společenský	k2eAgFnSc2d1	společenská
místnosti	místnost	k1gFnSc2	místnost
nepotkají	potkat	k5eNaPmIp3nP	potkat
Deana	Dean	k1gMnSc4	Dean
a	a	k8xC	a
Ginny	Ginn	k1gMnPc4	Ginn
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
líbají	líbat	k5eAaImIp3nP	líbat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
cítí	cítit	k5eAaImIp3nP	cítit
žárlivost	žárlivost	k1gFnSc4	žárlivost
<g/>
,	,	kIx,	,
když	když	k8xS	když
tam	tam	k6eAd1	tam
tak	tak	k9	tak
Deana	Deana	k1gFnSc1	Deana
s	s	k7c7	s
Ginny	Ginn	k1gMnPc7	Ginn
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Ron	ron	k1gInSc1	ron
se	se	k3xPyFc4	se
zlobí	zlobit	k5eAaImIp3nS	zlobit
<g/>
.	.	kIx.	.
</s>
<s>
Dean	Dean	k1gMnSc1	Dean
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
vytratí	vytratit	k5eAaPmIp3nP	vytratit
a	a	k8xC	a
Ginny	Ginn	k1gInPc1	Ginn
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
začnou	začít	k5eAaPmIp3nP	začít
křičet	křičet	k5eAaImF	křičet
<g/>
.	.	kIx.	.
</s>
<s>
Vytáhnou	vytáhnout	k5eAaPmIp3nP	vytáhnout
hůlky	hůlka	k1gFnPc1	hůlka
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
odstrčí	odstrčit	k5eAaPmIp3nP	odstrčit
Rona	Ron	k1gMnSc4	Ron
ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ten	ten	k3xDgMnSc1	ten
jen	jen	k9	jen
tak	tak	k6eAd1	tak
tak	tak	k6eAd1	tak
mine	minout	k5eAaImIp3nS	minout
sestru	sestra	k1gFnSc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Ginny	Ginn	k1gInPc1	Ginn
urazí	urazit	k5eAaPmIp3nP	urazit
Rona	Ron	k1gMnSc4	Ron
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k9	už
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
kamarádi	kamarád	k1gMnPc1	kamarád
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
někdy	někdy	k6eAd1	někdy
někoho	někdo	k3yInSc4	někdo
políbili	políbit	k5eAaPmAgMnP	políbit
a	a	k8xC	a
že	že	k8xS	že
Ron	Ron	k1gMnSc1	Ron
má	mít	k5eAaImIp3nS	mít
zkušenosti	zkušenost	k1gFnPc4	zkušenost
"	"	kIx"	"
<g/>
dvanáctiletého	dvanáctiletý	k2eAgMnSc2d1	dvanáctiletý
kluka	kluk	k1gMnSc2	kluk
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
polibek	polibek	k1gInSc4	polibek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kdy	kdy	k6eAd1	kdy
Ron	ron	k1gInSc4	ron
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
naší	náš	k3xOp1gFnSc2	náš
tety	teta	k1gFnSc2	teta
Muriel	Muriel	k1gInSc1	Muriel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ginny	Ginna	k1gFnPc4	Ginna
odejde	odejít	k5eAaPmIp3nS	odejít
a	a	k8xC	a
Harry	Harr	k1gInPc4	Harr
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
společenské	společenský	k2eAgFnSc2d1	společenská
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
si	se	k3xPyFc3	se
Harry	Harra	k1gFnPc4	Harra
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
naštvaný	naštvaný	k2eAgMnSc1d1	naštvaný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
spolužák	spolužák	k1gMnSc1	spolužák
líbá	líbat	k5eAaImIp3nS	líbat
sestru	sestra	k1gFnSc4	sestra
jeho	on	k3xPp3gMnSc4	on
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
kamaráda	kamarád	k1gMnSc4	kamarád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc2	jeho
vlastní	vlastní	k2eAgFnSc2d1	vlastní
myšlenky	myšlenka	k1gFnSc2	myšlenka
ho	on	k3xPp3gMnSc4	on
zradí	zradit	k5eAaPmIp3nS	zradit
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
vidí	vidět	k5eAaImIp3nP	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
chodbě	chodba	k1gFnSc6	chodba
místa	místo	k1gNnSc2	místo
Deana	Dean	k1gMnSc4	Dean
<g/>
,	,	kIx,	,
líbá	líbat	k5eAaImIp3nS	líbat
Ginny	Ginn	k1gInPc4	Ginn
a	a	k8xC	a
jak	jak	k6eAd1	jak
přiletí	přiletět	k5eAaPmIp3nS	přiletět
Ron	ron	k1gInSc4	ron
a	a	k8xC	a
křičí	křičet	k5eAaImIp3nS	křičet
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Ginny	Ginn	k1gInPc7	Ginn
sám	sám	k3xTgInSc1	sám
a	a	k8xC	a
žádný	žádný	k3yNgInSc1	žádný
Ron	ron	k1gInSc1	ron
je	on	k3xPp3gMnPc4	on
neruší	rušit	k5eNaImIp3nS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
agresivní	agresivní	k2eAgInPc4d1	agresivní
a	a	k8xC	a
Harry	Harr	k1gInPc1	Harr
se	se	k3xPyFc4	se
ponoří	ponořit	k5eAaPmIp3nP	ponořit
do	do	k7c2	do
nepokojného	pokojný	k2eNgInSc2d1	nepokojný
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
nemyslet	myslet	k5eNaImF	myslet
na	na	k7c4	na
Ginny	Ginna	k1gFnPc4	Ginna
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
jen	jen	k9	jen
bratrsky	bratrsky	k6eAd1	bratrsky
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
Ron	ron	k1gInSc1	ron
nadává	nadávat	k5eAaImIp3nS	nadávat
na	na	k7c4	na
každého	každý	k3xTgMnSc4	každý
kolem	kolem	k7c2	kolem
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
Deana	Dean	k1gMnSc4	Dean
a	a	k8xC	a
Ginny	Ginna	k1gFnPc4	Ginna
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
a	a	k8xC	a
vysmívá	vysmívat	k5eAaImIp3nS	vysmívat
se	se	k3xPyFc4	se
Hermioně	Hermiona	k1gFnSc3	Hermiona
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
dotčená	dotčený	k2eAgFnSc1d1	dotčená
a	a	k8xC	a
zmatená	zmatený	k2eAgFnSc1d1	zmatená
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
jde	jít	k5eAaImIp3nS	jít
pak	pak	k6eAd1	pak
rychle	rychle	k6eAd1	rychle
spát	spát	k5eAaImF	spát
a	a	k8xC	a
Ron	ron	k1gInSc1	ron
jde	jít	k5eAaImIp3nS	jít
rychle	rychle	k6eAd1	rychle
do	do	k7c2	do
postele	postel	k1gFnSc2	postel
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nadával	nadávat	k5eAaImAgMnS	nadávat
na	na	k7c4	na
nějaké	nějaký	k3yIgInPc4	nějaký
prváky	prvák	k1gInPc4	prvák
<g/>
.	.	kIx.	.
</s>
<s>
Ronovi	Ron	k1gMnSc3	Ron
se	se	k3xPyFc4	se
nevede	vést	k5eNaImIp3nS	vést
líp	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
famfrpálovém	famfrpálový	k2eAgInSc6d1	famfrpálový
tréninku	trénink	k1gInSc6	trénink
před	před	k7c7	před
zápasem	zápas	k1gInSc7	zápas
se	s	k7c7	s
Zmijozelem	Zmijozel	k1gInSc7	Zmijozel
pustí	pustit	k5eAaPmIp3nS	pustit
všechny	všechen	k3xTgInPc4	všechen
góly	gól	k1gInPc4	gól
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
tak	tak	k6eAd1	tak
křičí	křičet	k5eAaImIp3nS	křičet
na	na	k7c4	na
Demelzu	Demelza	k1gFnSc4	Demelza
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
rozpláče	rozplakat	k5eAaPmIp3nS	rozplakat
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
téměř	téměř	k6eAd1	téměř
odstoupí	odstoupit	k5eAaPmIp3nS	odstoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harry	Harra	k1gFnPc1	Harra
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
zabrání	zabrání	k1gNnSc6	zabrání
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
celý	celý	k2eAgInSc1d1	celý
večer	večer	k1gInSc1	večer
je	být	k5eAaImIp3nS	být
skleslý	skleslý	k2eAgInSc1d1	skleslý
<g/>
.	.	kIx.	.
</s>
<s>
Nepomůže	pomoct	k5eNaPmIp3nS	pomoct
mu	on	k3xPp3gMnSc3	on
ani	ani	k9	ani
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc2	on
Harry	Harra	k1gMnSc2	Harra
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
a	a	k8xC	a
křičí	křičet	k5eAaImIp3nS	křičet
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
konečně	konečně	k6eAd1	konečně
dostane	dostat	k5eAaPmIp3nS	dostat
nápad	nápad	k1gInSc1	nápad
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Rona	Ron	k1gMnSc4	Ron
pro	pro	k7c4	pro
zápas	zápas	k1gInSc4	zápas
povzbudit	povzbudit	k5eAaPmF	povzbudit
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
Ronovi	Ron	k1gMnSc3	Ron
při	při	k7c6	při
snídani	snídaně	k1gFnSc6	snídaně
sklenici	sklenice	k1gFnSc4	sklenice
dýňového	dýňový	k2eAgInSc2d1	dýňový
džusu	džus	k1gInSc2	džus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přiběhne	přiběhnout	k5eAaPmIp3nS	přiběhnout
rozzlobená	rozzlobený	k2eAgFnSc1d1	rozzlobená
Hermiona	Hermiona	k1gFnSc1	Hermiona
a	a	k8xC	a
radí	radit	k5eAaImIp3nS	radit
Ronovi	Ron	k1gMnSc3	Ron
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc4	ten
nepil	pít	k5eNaImAgMnS	pít
<g/>
.	.	kIx.	.
</s>
<s>
Viděla	vidět	k5eAaImAgFnS	vidět
totiž	totiž	k9	totiž
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
něco	něco	k3yInSc1	něco
do	do	k7c2	do
sklenice	sklenice	k1gFnSc2	sklenice
nalil	nalít	k5eAaPmAgMnS	nalít
<g/>
,	,	kIx,	,
a	a	k8xC	a
podezřívá	podezřívat	k5eAaImIp3nS	podezřívat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
použil	použít	k5eAaPmAgMnS	použít
Felix	Felix	k1gMnSc1	Felix
felicis	felicis	k1gFnSc2	felicis
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
přesto	přesto	k8xC	přesto
džus	džus	k1gInSc1	džus
vypije	vypít	k5eAaPmIp3nS	vypít
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
šoku	šok	k1gInSc6	šok
<g/>
.	.	kIx.	.
</s>
<s>
Felix	Felix	k1gMnSc1	Felix
zřejmě	zřejmě	k6eAd1	zřejmě
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
zmijozelský	zmijozelský	k2eAgMnSc1d1	zmijozelský
střelec	střelec	k1gMnSc1	střelec
je	být	k5eAaImIp3nS	být
zraněný	zraněný	k2eAgMnSc1d1	zraněný
a	a	k8xC	a
Malfoy	Malfoy	k1gInPc1	Malfoy
je	být	k5eAaImIp3nS	být
též	též	k9	též
nemocný	nemocný	k2eAgMnSc1d1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
podezřívá	podezřívat	k5eAaImIp3nS	podezřívat
Malfoye	Malfoye	k1gFnSc1	Malfoye
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
něco	něco	k3yInSc1	něco
za	za	k7c7	za
lubem	lub	k1gInSc7	lub
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
komentátorem	komentátor	k1gMnSc7	komentátor
je	být	k5eAaImIp3nS	být
Zachariáš	Zachariáš	k1gMnSc1	Zachariáš
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
využije	využít	k5eAaPmIp3nS	využít
každou	každý	k3xTgFnSc4	každý
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Harryho	Harry	k1gMnSc4	Harry
tým	tým	k1gInSc1	tým
urazil	urazit	k5eAaPmAgInS	urazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
půl	půl	k1xP	půl
hodině	hodina	k1gFnSc6	hodina
vede	vést	k5eAaImIp3nS	vést
Nebelvír	Nebelvír	k1gInSc1	Nebelvír
70	[number]	k4	70
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
vynikajících	vynikající	k2eAgInPc2d1	vynikající
zásahů	zásah	k1gInPc2	zásah
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Smith	Smith	k1gMnSc1	Smith
napadá	napadat	k5eAaImIp3nS	napadat
odrážeče	odrážeč	k1gInPc4	odrážeč
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
minut	minuta	k1gFnPc2	minuta
už	už	k6eAd1	už
Nebelvír	Nebelvír	k1gInSc1	Nebelvír
vede	vést	k5eAaImIp3nS	vést
100	[number]	k4	100
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
zmijozelský	zmijozelský	k2eAgInSc1d1	zmijozelský
chytač	chytač	k1gInSc1	chytač
uvidí	uvidět	k5eAaPmIp3nS	uvidět
Zlatonku	Zlatonka	k1gFnSc4	Zlatonka
<g/>
.	.	kIx.	.
</s>
<s>
Harrymu	Harrym	k1gInSc2	Harrym
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
podaří	podařit	k5eAaPmIp3nS	podařit
na	na	k7c4	na
chvilku	chvilka	k1gFnSc4	chvilka
vyrušit	vyrušit	k5eAaPmF	vyrušit
a	a	k8xC	a
Zlatonku	Zlatonka	k1gFnSc4	Zlatonka
chytí	chytit	k5eAaPmIp3nS	chytit
a	a	k8xC	a
Nebelvír	Nebelvír	k1gInSc1	Nebelvír
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šatně	šatna	k1gFnSc6	šatna
panuje	panovat	k5eAaImIp3nS	panovat
vítězoslavná	vítězoslavný	k2eAgFnSc1d1	vítězoslavná
atmosféra	atmosféra	k1gFnSc1	atmosféra
a	a	k8xC	a
ve	v	k7c6	v
společenské	společenský	k2eAgFnSc6d1	společenská
místnosti	místnost	k1gFnSc6	místnost
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
večírek	večírek	k1gInSc1	večírek
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
kromě	kromě	k7c2	kromě
Rona	Ron	k1gMnSc2	Ron
a	a	k8xC	a
Harryho	Harry	k1gMnSc2	Harry
odejdou	odejít	k5eAaPmIp3nP	odejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
tam	tam	k6eAd1	tam
vlítne	vlítnout	k5eAaPmIp3nS	vlítnout
Hermiona	Hermiona	k1gFnSc1	Hermiona
a	a	k8xC	a
vynadá	vynadat	k5eAaPmIp3nS	vynadat
Harrymu	Harrym	k1gInSc2	Harrym
a	a	k8xC	a
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
jde	jít	k5eAaImIp3nS	jít
říct	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
se	se	k3xPyFc4	se
na	na	k7c4	na
oba	dva	k4xCgMnPc4	dva
usmívá	usmívat	k5eAaImIp3nS	usmívat
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
Ronovi	Ron	k1gMnSc3	Ron
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
do	do	k7c2	do
džusu	džus	k1gInSc2	džus
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nic	nic	k6eAd1	nic
nenalil	nalít	k5eNaPmAgMnS	nalít
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukáže	ukázat	k5eAaPmIp3nS	ukázat
jim	on	k3xPp3gMnPc3	on
plnou	plný	k2eAgFnSc4d1	plná
lahvičku	lahvička	k1gFnSc4	lahvička
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
,	,	kIx,	,
že	že	k8xS	že
zařve	zařvat	k5eAaPmIp3nS	zařvat
na	na	k7c6	na
Hermionu	Hermion	k1gInSc6	Hermion
a	a	k8xC	a
vyřítí	vyřítit	k5eAaPmIp3nS	vyřítit
se	se	k3xPyFc4	se
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
ho	on	k3xPp3gInSc4	on
se	s	k7c7	s
slzami	slza	k1gFnPc7	slza
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
následuje	následovat	k5eAaImIp3nS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
je	být	k5eAaImIp3nS	být
namíchnutý	namíchnutý	k2eAgMnSc1d1	namíchnutý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
udobřit	udobřit	k5eAaPmF	udobřit
<g/>
,	,	kIx,	,
nevyšel	vyjít	k5eNaPmAgMnS	vyjít
<g/>
,	,	kIx,	,
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
večírku	večírek	k1gInSc6	večírek
mu	on	k3xPp3gMnSc3	on
Ginny	Ginen	k2eAgMnPc4d1	Ginen
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ron	ron	k1gInSc1	ron
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
koutě	kout	k1gInSc6	kout
s	s	k7c7	s
Levandulí	levandule	k1gFnSc7	levandule
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
najde	najít	k5eAaPmIp3nS	najít
Rona	Ron	k1gMnSc2	Ron
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Ginny	Ginna	k1gFnSc2	Ginna
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
Levanduli	levandule	k1gFnSc3	levandule
sníst	sníst	k5eAaPmF	sníst
obličej	obličej	k1gInSc4	obličej
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
spatří	spatřit	k5eAaPmIp3nP	spatřit
Hermionu	Hermion	k1gInSc2	Hermion
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vyřítila	vyřítit	k5eAaPmAgFnS	vyřítit
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Najde	najít	k5eAaPmIp3nS	najít
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
a	a	k8xC	a
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
jí	on	k3xPp3gFnSc3	on
létají	létat	k5eAaImIp3nP	létat
ptáčci	ptáček	k1gMnPc1	ptáček
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
si	se	k3xPyFc3	se
vyčarovala	vyčarovat	k5eAaPmAgFnS	vyčarovat
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
utěšit	utěšit	k5eAaPmF	utěšit
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
viděla	vidět	k5eAaImAgFnS	vidět
Rona	Ron	k1gMnSc4	Ron
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
schopná	schopný	k2eAgNnPc4d1	schopné
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
místnosti	místnost	k1gFnSc2	místnost
vtrhne	vtrhnout	k5eAaPmIp3nS	vtrhnout
s	s	k7c7	s
Levandulí	levandule	k1gFnSc7	levandule
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
vytratí	vytratit	k5eAaPmIp3nS	vytratit
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
řekne	říct	k5eAaPmIp3nS	říct
Ronovi	Ron	k1gMnSc3	Ron
sbohem	sbohem	k0	sbohem
a	a	k8xC	a
poštve	poštvat	k5eAaPmIp3nS	poštvat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
ptáčky	ptáček	k1gMnPc4	ptáček
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gInSc4	on
začnou	začít	k5eAaPmIp3nP	začít
klovat	klovat	k5eAaImF	klovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vzlykotem	vzlykot	k1gInSc7	vzlykot
uteče	utéct	k5eAaPmIp3nS	utéct
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
teď	teď	k6eAd1	teď
každý	každý	k3xTgInSc4	každý
večer	večer	k6eAd1	večer
tráví	trávit	k5eAaImIp3nP	trávit
líbáním	líbání	k1gNnSc7	líbání
Levandule	levandule	k1gFnSc2	levandule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
"	"	kIx"	"
<g/>
považuje	považovat	k5eAaImIp3nS	považovat
každou	každý	k3xTgFnSc4	každý
chvilku	chvilka	k1gFnSc4	chvilka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nelíbá	líbat	k5eNaImIp3nS	líbat
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
<g/>
,	,	kIx,	,
za	za	k7c4	za
ztrátu	ztráta	k1gFnSc4	ztráta
času	čas	k1gInSc2	čas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
je	být	k5eAaImIp3nS	být
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
zatrpklejší	zatrpklý	k2eAgInSc1d2	zatrpklý
a	a	k8xC	a
Ron	ron	k1gInSc1	ron
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
neměla	mít	k5eNaImAgFnS	mít
stěžovat	stěžovat	k5eAaImF	stěžovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
už	už	k6eAd1	už
líbala	líbat	k5eAaImAgFnS	líbat
s	s	k7c7	s
Viktorem	Viktor	k1gMnSc7	Viktor
Krumem	Krum	k1gMnSc7	Krum
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
zatím	zatím	k6eAd1	zatím
úspěšně	úspěšně	k6eAd1	úspěšně
využívá	využívat	k5eAaImIp3nS	využívat
rady	rada	k1gFnPc4	rada
z	z	k7c2	z
princovy	princův	k2eAgFnSc2d1	princova
knihy	kniha	k1gFnSc2	kniha
Lektvary	lektvar	k1gInPc4	lektvar
pro	pro	k7c4	pro
pokročilé	pokročilý	k2eAgMnPc4d1	pokročilý
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
mu	on	k3xPp3gMnSc3	on
radí	radit	k5eAaImIp3nS	radit
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
je	být	k5eAaImIp3nS	být
opatrný	opatrný	k2eAgMnSc1d1	opatrný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harry	Harr	k1gInPc1	Harr
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
snad	snad	k9	snad
ztratil	ztratit	k5eAaPmAgMnS	ztratit
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
nemluvil	mluvit	k5eNaImAgMnS	mluvit
<g/>
)	)	kIx)	)
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
prince	princ	k1gMnSc2	princ
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
daleko	daleko	k6eAd1	daleko
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
od	od	k7c2	od
Křiklana	Křiklan	k1gMnSc2	Křiklan
a	a	k8xC	a
Snapea	Snapeus	k1gMnSc2	Snapeus
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
rozzlobeně	rozzlobeně	k6eAd1	rozzlobeně
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
prince	princ	k1gMnSc2	princ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dívky	dívka	k1gFnPc1	dívka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
blázen	blázen	k1gMnSc1	blázen
a	a	k8xC	a
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
snaží	snažit	k5eAaImIp3nS	snažit
podstrčit	podstrčit	k5eAaPmF	podstrčit
nápoj	nápoj	k1gInSc4	nápoj
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
některou	některý	k3yIgFnSc4	některý
dívku	dívka	k1gFnSc4	dívka
pozval	pozvat	k5eAaPmAgMnS	pozvat
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
vánoční	vánoční	k2eAgFnSc4d1	vánoční
party	party	k1gFnSc4	party
u	u	k7c2	u
Křiklana	Křiklan	k1gMnSc2	Křiklan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
daly	dát	k5eAaPmAgFnP	dát
pokoj	pokoj	k1gInSc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
nemyslet	myslet	k5eNaImF	myslet
na	na	k7c4	na
Ginny	Ginna	k1gFnPc4	Ginna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
pořád	pořád	k6eAd1	pořád
vkrádá	vkrádat	k5eAaImIp3nS	vkrádat
do	do	k7c2	do
snů	sen	k1gInPc2	sen
v	v	k7c6	v
takových	takový	k3xDgFnPc6	takový
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Harry	Harra	k1gFnPc4	Harra
rád	rád	k6eAd1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ron	ron	k1gInSc1	ron
neovládá	ovládat	k5eNaImIp3nS	ovládat
nitrozpyt	nitrozpyt	k1gInSc1	nitrozpyt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zmíní	zmínit	k5eAaPmIp3nS	zmínit
o	o	k7c6	o
Malfoyovi	Malfoya	k1gMnSc6	Malfoya
a	a	k8xC	a
náhrdelníku	náhrdelník	k1gInSc3	náhrdelník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Hermiona	Hermiona	k1gFnSc1	Hermiona
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
detektory	detektor	k1gInPc1	detektor
temných	temný	k2eAgFnPc2d1	temná
věcí	věc	k1gFnPc2	věc
by	by	kYmCp3nP	by
určitě	určitě	k6eAd1	určitě
našly	najít	k5eAaPmAgFnP	najít
prokletí	prokletí	k1gNnSc4	prokletí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
takový	takový	k3xDgInSc4	takový
nápoj	nápoj	k1gInSc4	nápoj
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lahvičce	lahvička	k1gFnSc6	lahvička
od	od	k7c2	od
lektvaru	lektvar	k1gInSc2	lektvar
proti	proti	k7c3	proti
kašli	kašel	k1gInSc3	kašel
(	(	kIx(	(
<g/>
Filch	Filch	k1gMnSc1	Filch
zakázal	zakázat	k5eAaPmAgMnS	zakázat
všechny	všechen	k3xTgFnPc4	všechen
věci	věc	k1gFnPc4	věc
z	z	k7c2	z
Kratochvilných	kratochvilný	k2eAgFnPc2d1	kratochvilná
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
kejklí	kejkle	k1gFnPc2	kejkle
<g/>
)	)	kIx)	)
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Harryho	Harryze	k6eAd1	Harryze
napadne	napadnout	k5eAaPmIp3nS	napadnout
rozzlobená	rozzlobený	k2eAgFnSc1d1	rozzlobená
knihovnice	knihovnice	k1gFnSc1	knihovnice
paní	paní	k1gFnSc1	paní
Pinceová	Pinceová	k1gFnSc1	Pinceová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
poškrábat	poškrábat	k5eAaPmF	poškrábat
<g/>
,	,	kIx,	,
když	když	k8xS	když
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
princova	princův	k2eAgFnSc1d1	princova
kniha	kniha	k1gFnSc1	kniha
popsaná	popsaný	k2eAgFnSc1d1	popsaná
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
s	s	k7c7	s
Hermionou	Hermiona	k1gFnSc7	Hermiona
utečou	utéct	k5eAaPmIp3nP	utéct
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
vysloví	vyslovit	k5eAaPmIp3nP	vyslovit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pinceová	Pinceová	k1gFnSc1	Pinceová
a	a	k8xC	a
Filch	Filch	k1gInSc1	Filch
spolu	spolu	k6eAd1	spolu
něco	něco	k3yInSc4	něco
mají	mít	k5eAaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společenské	společenský	k2eAgFnSc6d1	společenská
místnosti	místnost	k1gFnSc6	místnost
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
Harrymu	Harrym	k1gInSc2	Harrym
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
obdivovatelek	obdivovatelka	k1gFnPc2	obdivovatelka
odvar	odvar	k1gInSc4	odvar
z	z	k7c2	z
chejru	chejr	k1gInSc2	chejr
a	a	k8xC	a
krabici	krabice	k1gFnSc4	krabice
bonbonů	bonbon	k1gInPc2	bonbon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
rychle	rychle	k6eAd1	rychle
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
si	se	k3xPyFc3	se
chtěla	chtít	k5eAaImAgFnS	chtít
přisednout	přisednout	k5eAaPmF	přisednout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
vidí	vidět	k5eAaImIp3nS	vidět
Rona	Ron	k1gMnSc4	Ron
a	a	k8xC	a
Levanduli	levandule	k1gFnSc4	levandule
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
sebe	se	k3xPyFc2	se
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
křesle	křeslo	k1gNnSc6	křeslo
zapleteni	zaplést	k5eAaPmNgMnP	zaplést
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
radši	rád	k6eAd2	rád
spát	spát	k5eAaImF	spát
<g/>
.	.	kIx.	.
</s>
<s>
Příští	příští	k2eAgInSc4d1	příští
den	den	k1gInSc4	den
na	na	k7c6	na
hodině	hodina	k1gFnSc6	hodina
přeměňování	přeměňování	k1gNnSc2	přeměňování
se	se	k3xPyFc4	se
Hermiona	Hermiona	k1gFnSc1	Hermiona
a	a	k8xC	a
Ron	ron	k1gInSc1	ron
pohádají	pohádat	k5eAaPmIp3nP	pohádat
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
s	s	k7c7	s
brekotem	brekot	k1gInSc7	brekot
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
záchodě	záchod	k1gInSc6	záchod
ji	on	k3xPp3gFnSc4	on
utěší	utěšit	k5eAaPmIp3nS	utěšit
Lenka	Lenka	k1gFnSc1	Lenka
Láskorádová	Láskorádový	k2eAgFnSc1d1	Láskorádová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pak	pak	k6eAd1	pak
potká	potkat	k5eAaPmIp3nS	potkat
Harryho	Harry	k1gMnSc4	Harry
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
si	se	k3xPyFc3	se
Hermiona	Hermiona	k1gFnSc1	Hermiona
vezme	vzít	k5eAaPmIp3nS	vzít
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zapomněla	zapomenout	k5eAaPmAgFnS	zapomenout
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
Ron	ron	k1gInSc1	ron
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
legrační	legrační	k2eAgFnPc4d1	legrační
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
jeho	jeho	k3xOp3gInPc1	jeho
výroky	výrok	k1gInPc1	výrok
i	i	k9	i
zraňují	zraňovat	k5eAaImIp3nP	zraňovat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
si	se	k3xPyFc3	se
pomyslí	pomyslet	k5eAaPmIp3nP	pomyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lenka	Lenka	k1gFnSc1	Lenka
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
nadání	nadání	k1gNnSc4	nadání
říkat	říkat	k5eAaImF	říkat
děsivou	děsivý	k2eAgFnSc4d1	děsivá
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
také	také	k9	také
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
Ginny	Ginna	k1gFnSc2	Ginna
postavila	postavit	k5eAaPmAgFnS	postavit
a	a	k8xC	a
zakázala	zakázat	k5eAaPmAgFnS	zakázat
dvou	dva	k4xCgMnPc6	dva
klukům	kluk	k1gMnPc3	kluk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
říkali	říkat	k5eAaImAgMnP	říkat
"	"	kIx"	"
<g/>
Střelenko	Střelenka	k1gFnSc5	Střelenka
<g/>
"	"	kIx"	"
a	a	k8xC	a
Harry	Harra	k1gFnSc2	Harra
požádá	požádat	k5eAaPmIp3nS	požádat
Lenku	Lenka	k1gFnSc4	Lenka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
šla	jít	k5eAaImAgFnS	jít
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
vánoční	vánoční	k2eAgFnSc4d1	vánoční
party	party	k1gFnSc4	party
u	u	k7c2	u
Křiklana	Křiklan	k1gMnSc2	Křiklan
<g/>
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
je	být	k5eAaImIp3nS	být
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
ještě	ještě	k6eAd1	ještě
nikdo	nikdo	k3yNnSc1	nikdo
nikdy	nikdy	k6eAd1	nikdy
na	na	k7c4	na
party	party	k1gFnSc4	party
nepozval	pozvat	k5eNaPmAgMnS	pozvat
<g/>
.	.	kIx.	.
</s>
<s>
Najednou	najednou	k6eAd1	najednou
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
Protiva	protiva	k1gFnSc1	protiva
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
vykřikovat	vykřikovat	k5eAaImF	vykřikovat
"	"	kIx"	"
<g/>
Harry	Harra	k1gFnPc4	Harra
láskuje	láskovat	k5eAaBmIp3nS	láskovat
Střelenku	Střelenka	k1gFnSc4	Střelenka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
už	už	k9	už
každý	každý	k3xTgMnSc1	každý
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Harry	Harra	k1gFnSc2	Harra
pozval	pozvat	k5eAaPmAgMnS	pozvat
Lenku	Lenka	k1gFnSc4	Lenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ronovi	Ron	k1gMnSc3	Ron
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgMnS	moct
pozvat	pozvat	k5eAaPmF	pozvat
kohokoliv	kdokoliv	k3yInSc4	kdokoliv
kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Ginny	Ginna	k1gFnPc1	Ginna
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
vynadá	vynadat	k5eAaPmIp3nS	vynadat
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
sedí	sedit	k5eAaImIp3nS	sedit
sama	sám	k3xTgFnSc1	sám
hraje	hrát	k5eAaImIp3nS	hrát
si	se	k3xPyFc3	se
s	s	k7c7	s
jídlem	jídlo	k1gNnSc7	jídlo
na	na	k7c6	na
talíři	talíř	k1gInSc6	talíř
a	a	k8xC	a
Ron	Ron	k1gMnSc1	Ron
s	s	k7c7	s
Harrym	Harrym	k1gInSc4	Harrym
si	se	k3xPyFc3	se
přisednou	přisednout	k5eAaPmIp3nP	přisednout
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
chvilku	chvilka	k1gFnSc4	chvilka
dorazí	dorazit	k5eAaPmIp3nP	dorazit
i	i	k9	i
ostatní	ostatní	k2eAgFnPc1d1	ostatní
nebelvírské	belvírský	k2eNgFnPc1d1	nebelvírská
holky	holka	k1gFnPc1	holka
a	a	k8xC	a
Levandule	levandule	k1gFnSc1	levandule
padne	padnout	k5eAaImIp3nS	padnout
Ronovi	Ron	k1gMnSc3	Ron
do	do	k7c2	do
náručí	náručí	k1gNnSc2	náručí
<g/>
.	.	kIx.	.
</s>
<s>
Parvati	Parvat	k5eAaPmF	Parvat
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
provinile	provinile	k6eAd1	provinile
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
smála	smát	k5eAaImAgFnS	smát
Hermioně	Hermioň	k1gFnPc4	Hermioň
a	a	k8xC	a
zeptá	zeptat	k5eAaPmIp3nS	zeptat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
na	na	k7c4	na
večírek	večírek	k1gInSc4	večírek
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
Hermiona	Hermiona	k1gFnSc1	Hermiona
ublížila	ublížit	k5eAaPmAgFnS	ublížit
Ronovi	Ron	k1gMnSc3	Ron
<g/>
,	,	kIx,	,
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
s	s	k7c7	s
McLaggenem	McLaggen	k1gInSc7	McLaggen
<g/>
.	.	kIx.	.
</s>
<s>
Parvati	Parvat	k5eAaBmF	Parvat
je	být	k5eAaImIp3nS	být
ohromená	ohromený	k2eAgFnSc1d1	ohromená
a	a	k8xC	a
poznamená	poznamenat	k5eAaPmIp3nS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hermioně	Hermioň	k1gFnPc1	Hermioň
se	se	k3xPyFc4	se
fakt	fakt	k9	fakt
líbí	líbit	k5eAaImIp3nP	líbit
hráči	hráč	k1gMnPc1	hráč
famfrpálu	famfrpála	k1gFnSc4	famfrpála
<g/>
,	,	kIx,	,
nejdřív	dříve	k6eAd3	dříve
Krum	Krum	k1gInSc1	Krum
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
McLaggen	McLaggen	k1gInSc1	McLaggen
<g/>
!	!	kIx.	!
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
zdůrazní	zdůraznit	k5eAaPmIp3nS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
líbí	líbit	k5eAaImIp3nP	líbit
jen	jen	k9	jen
dobří	dobrý	k2eAgMnPc1d1	dobrý
hráči	hráč	k1gMnPc1	hráč
famfrpálu	famfrpál	k1gInSc2	famfrpál
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
až	až	k9	až
holky	holka	k1gFnSc2	holka
můžou	můžou	k?	můžou
klesnout	klesnout	k5eAaPmF	klesnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pomstily	pomstít	k5eAaPmAgFnP	pomstít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jdou	jít	k5eAaImIp3nP	jít
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
Lenka	Lenka	k1gFnSc1	Lenka
na	na	k7c4	na
večírek	večírek	k1gInSc4	večírek
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijde	přijít	k5eAaPmIp3nS	přijít
i	i	k9	i
Rufus	Rufus	k1gMnSc1	Rufus
Brousek	brousek	k1gInSc1	brousek
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
upír	upír	k1gMnSc1	upír
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
jí	on	k3xPp3gFnSc3	on
má	mít	k5eAaImIp3nS	mít
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
večírku	večírek	k1gInSc6	večírek
Křiklan	Křiklana	k1gFnPc2	Křiklana
Harrymu	Harrym	k1gInSc2	Harrym
představí	představit	k5eAaPmIp3nP	představit
experta	expert	k1gMnSc4	expert
na	na	k7c4	na
upíry	upír	k1gMnPc4	upír
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
přítele	přítel	k1gMnSc2	přítel
upíra	upír	k1gMnSc2	upír
Sanguiniho	Sanguini	k1gMnSc2	Sanguini
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
i	i	k9	i
Sudičky	sudička	k1gFnSc2	sudička
a	a	k8xC	a
také	také	k9	také
profesorka	profesorka	k1gFnSc1	profesorka
Trelawneyová	Trelawneyová	k1gFnSc1	Trelawneyová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pořád	pořád	k6eAd1	pořád
pije	pít	k5eAaImIp3nS	pít
<g/>
.	.	kIx.	.
</s>
<s>
Přijde	přijít	k5eAaPmIp3nS	přijít
i	i	k9	i
Snape	Snap	k1gInSc5	Snap
a	a	k8xC	a
Křiklan	Křiklan	k1gMnSc1	Křiklan
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Harry	Harra	k1gFnPc4	Harra
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
lektvary	lektvar	k1gInPc4	lektvar
talent	talent	k1gInSc4	talent
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nemyslí	myslet	k5eNaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	k9	by
Harryho	Harry	k1gMnSc4	Harry
za	za	k7c4	za
těch	ten	k3xDgNnPc2	ten
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
něco	něco	k3yInSc1	něco
naučil	naučit	k5eAaPmAgMnS	naučit
a	a	k8xC	a
Křiklan	Křiklan	k1gMnSc1	Křiklan
vydedukuje	vydedukovat	k5eAaPmIp3nS	vydedukovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
Harry	Harra	k1gFnPc4	Harra
talent	talent	k1gInSc4	talent
od	od	k7c2	od
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
řekne	říct	k5eAaPmIp3nS	říct
Křiklanovi	Křiklan	k1gMnSc3	Křiklan
<g/>
,	,	kIx,	,
že	že	k8xS	že
chodí	chodit	k5eAaImIp3nS	chodit
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
musí	muset	k5eAaImIp3nP	muset
zvládnout	zvládnout	k5eAaPmF	zvládnout
bystrozor	bystrozor	k1gInSc4	bystrozor
<g/>
,	,	kIx,	,
a	a	k8xC	a
Křiklan	Křiklan	k1gMnSc1	Křiklan
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
výtečným	výtečný	k2eAgInSc7d1	výtečný
bystrozorem	bystrozor	k1gInSc7	bystrozor
<g/>
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
se	se	k3xPyFc4	se
zmíní	zmínit	k5eAaPmIp3nS	zmínit
o	o	k7c6	o
nějakém	nějaký	k3yIgInSc6	nějaký
komplotu	komplot	k1gInSc6	komplot
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
a	a	k8xC	a
nemoci	nemoc	k1gFnSc2	nemoc
dásní	dáseň	k1gFnPc2	dáseň
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
baví	bavit	k5eAaImIp3nS	bavit
s	s	k7c7	s
Lenkou	Lenka	k1gFnSc7	Lenka
a	a	k8xC	a
Křiklanem	Křiklan	k1gInSc7	Křiklan
<g/>
,	,	kIx,	,
když	když	k8xS	když
Filch	Filch	k1gInSc1	Filch
přitáhne	přitáhnout	k5eAaPmIp3nS	přitáhnout
za	za	k7c4	za
ucho	ucho	k1gNnSc4	ucho
Malfoye	Malfoy	k1gInSc2	Malfoy
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
vkrást	vkrást	k5eAaPmF	vkrást
na	na	k7c4	na
party	party	k1gFnSc4	party
<g/>
.	.	kIx.	.
</s>
<s>
Křiklan	Křiklan	k1gInSc1	Křiklan
se	se	k3xPyFc4	se
směje	smát	k5eAaImIp3nS	smát
a	a	k8xC	a
vynadá	vynadat	k5eAaPmIp3nS	vynadat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dovolí	dovolit	k5eAaPmIp3nS	dovolit
mu	on	k3xPp3gMnSc3	on
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Malfoy	Malfoa	k1gFnPc4	Malfoa
je	být	k5eAaImIp3nS	být
naštvaný	naštvaný	k2eAgMnSc1d1	naštvaný
<g/>
,	,	kIx,	,
Filch	Filch	k1gMnSc1	Filch
také	také	k9	také
a	a	k8xC	a
Snape	Snap	k1gInSc5	Snap
vypadá	vypadat	k5eAaImIp3nS	vypadat
vystrašeně	vystrašeně	k6eAd1	vystrašeně
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
odejde	odejít	k5eAaPmIp3nS	odejít
s	s	k7c7	s
Malfoyem	Malfoy	k1gInSc7	Malfoy
na	na	k7c4	na
slovíčko	slovíčko	k1gNnSc4	slovíčko
a	a	k8xC	a
Křiklan	Křiklan	k1gInSc1	Křiklan
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
není	být	k5eNaImIp3nS	být
přísný	přísný	k2eAgInSc1d1	přísný
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
řekne	říct	k5eAaPmIp3nS	říct
Lence	Lenka	k1gFnSc3	Lenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
na	na	k7c4	na
záchod	záchod	k1gInSc4	záchod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
špehovat	špehovat	k5eAaImF	špehovat
Snapea	Snapea	k1gFnSc1	Snapea
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
vymámit	vymámit	k5eAaPmF	vymámit
z	z	k7c2	z
Malfoye	Malfoy	k1gFnSc2	Malfoy
informace	informace	k1gFnSc2	informace
o	o	k7c4	o
Katie	Katie	k1gFnPc4	Katie
Bellové	Bellový	k2eAgFnPc4d1	Bellová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Malfoy	Malfoa	k1gFnPc1	Malfoa
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
poznamená	poznamenat	k5eAaPmIp3nS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
teta	teta	k1gFnSc1	teta
Bella	Bella	k1gFnSc1	Bella
musela	muset	k5eAaImAgFnS	muset
učit	učit	k5eAaImF	učit
nitrobanu	nitrobana	k1gFnSc4	nitrobana
<g/>
.	.	kIx.	.
</s>
<s>
Malfoy	Malfo	k1gMnPc4	Malfo
se	se	k3xPyFc4	se
toulal	toulat	k5eAaImAgMnS	toulat
po	po	k7c6	po
hradě	hrad	k1gInSc6	hrad
a	a	k8xC	a
vymýšlel	vymýšlet	k5eAaImAgInS	vymýšlet
plán	plán	k1gInSc1	plán
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
se	se	k3xPyFc4	se
Malfoyovi	Malfoyův	k2eAgMnPc1d1	Malfoyův
snaží	snažit	k5eAaImIp3nP	snažit
pomoci	pomoct	k5eAaPmF	pomoct
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
neporušitelný	porušitelný	k2eNgInSc4d1	neporušitelný
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
ochránil	ochránit	k5eAaPmAgMnS	ochránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Malfoyovi	Malfoya	k1gMnSc3	Malfoya
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Vyběhne	vyběhnout	k5eAaPmIp3nS	vyběhnout
z	z	k7c2	z
místnosti	místnost	k1gFnSc2	místnost
a	a	k8xC	a
málem	málem	k6eAd1	málem
srazí	srazit	k5eAaPmIp3nS	srazit
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
a	a	k8xC	a
Ron	ron	k1gInSc1	ron
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Doupěti	doupě	k1gNnSc6	doupě
a	a	k8xC	a
užívají	užívat	k5eAaImIp3nP	užívat
si	se	k3xPyFc3	se
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
chce	chtít	k5eAaImIp3nS	chtít
říct	říct	k5eAaPmF	říct
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
důvěryhodným	důvěryhodný	k2eAgMnPc3d1	důvěryhodný
lidem	člověk	k1gMnPc3	člověk
o	o	k7c6	o
rozhovoru	rozhovor	k1gInSc6	rozhovor
mezi	mezi	k7c7	mezi
Snapem	Snap	k1gMnSc7	Snap
a	a	k8xC	a
Dracem	Drace	k1gMnSc7	Drace
<g/>
.	.	kIx.	.
</s>
<s>
Šanci	šance	k1gFnSc4	šance
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
řekne	říct	k5eAaPmIp3nS	říct
Remusu	Remus	k1gMnSc3	Remus
Lupinovi	Lupin	k1gMnSc3	Lupin
a	a	k8xC	a
panu	pan	k1gMnSc3	pan
Weasleymu	Weasleym	k1gInSc2	Weasleym
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
řeknou	říct	k5eAaPmIp3nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Brumbál	brumbál	k1gMnSc1	brumbál
Snapeovi	Snapeův	k2eAgMnPc1d1	Snapeův
věří	věřit	k5eAaImIp3nP	věřit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Lupin	lupina	k1gFnPc2	lupina
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
učil	učít	k5eAaPmAgMnS	učít
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
měsíc	měsíc	k1gInSc1	měsíc
bezchybně	bezchybně	k6eAd1	bezchybně
připravoval	připravovat	k5eAaImAgInS	připravovat
lektvar	lektvar	k1gInSc1	lektvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
tu	ten	k3xDgFnSc4	ten
zahořklost	zahořklost	k1gFnSc4	zahořklost
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
Harrym	Harrym	k1gInSc1	Harrym
<g/>
)	)	kIx)	)
nikdo	nikdo	k3yNnSc1	nikdo
nezacelí	zacelet	k5eNaPmIp3nS	zacelet
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
se	se	k3xPyFc4	se
zeptá	zeptat	k5eAaPmIp3nS	zeptat
Lupina	lupina	k1gFnSc1	lupina
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
teď	teď	k6eAd1	teď
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Lupin	lupina	k1gFnPc2	lupina
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ilegalitě	ilegalita	k1gFnSc6	ilegalita
<g/>
;	;	kIx,	;
žije	žít	k5eAaImIp3nS	žít
mezi	mezi	k7c7	mezi
vlkodlaky	vlkodlak	k1gMnPc7	vlkodlak
a	a	k8xC	a
podává	podávat	k5eAaImIp3nS	podávat
informace	informace	k1gFnSc1	informace
Řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chtějí	chtít	k5eAaImIp3nP	chtít
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
vládou	vláda	k1gFnSc7	vláda
získat	získat	k5eAaPmF	získat
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
se	se	k3xPyFc4	se
domluvit	domluvit	k5eAaPmF	domluvit
s	s	k7c7	s
Fenrirem	Fenrir	k1gMnSc7	Fenrir
Šedohřbetem	Šedohřbet	k1gMnSc7	Šedohřbet
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
šéf	šéf	k1gMnSc1	šéf
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejbrutálnější	brutální	k2eAgMnSc1d3	nejbrutálnější
žijící	žijící	k2eAgMnSc1d1	žijící
vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
on	on	k3xPp3gMnSc1	on
kousl	kousnout	k5eAaPmAgMnS	kousnout
Lupina	lupina	k1gFnSc1	lupina
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Fenrir	Fenrir	k1gMnSc1	Fenrir
si	se	k3xPyFc3	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nenáviděly	návidět	k5eNaImAgFnP	návidět
kouzelníky	kouzelník	k1gMnPc4	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
také	také	k9	také
pokousat	pokousat	k5eAaPmF	pokousat
dost	dost	k6eAd1	dost
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vlkodlaci	vlkodlak	k1gMnPc1	vlkodlak
mohli	moct	k5eAaImAgMnP	moct
chopit	chopit	k5eAaPmF	chopit
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Lupin	lupina	k1gFnPc2	lupina
dovypráví	dovyprávět	k5eAaPmIp3nS	dovyprávět
a	a	k8xC	a
vypije	vypít	k5eAaPmIp3nS	vypít
si	se	k3xPyFc3	se
vaječný	vaječný	k2eAgInSc4d1	vaječný
koňak	koňak	k1gInSc4	koňak
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
pak	pak	k6eAd1	pak
zeptá	zeptat	k5eAaPmIp3nS	zeptat
na	na	k7c6	na
prince	princa	k1gFnSc6	princa
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
kouzlo	kouzlo	k1gNnSc4	kouzlo
Levicorpus	Levicorpus	k1gInSc1	Levicorpus
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
kdysi	kdysi	k6eAd1	kdysi
James	James	k1gMnSc1	James
použil	použít	k5eAaPmAgMnS	použít
na	na	k7c4	na
Snapea	Snapeus	k1gMnSc4	Snapeus
<g/>
.	.	kIx.	.
</s>
<s>
Lupin	lupina	k1gFnPc2	lupina
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
páru	pára	k1gFnSc4	pára
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
nebyl	být	k5eNaImAgInS	být
ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
Sirius	Sirius	k1gInSc1	Sirius
<g/>
.	.	kIx.	.
</s>
<s>
Lupin	lupina	k1gFnPc2	lupina
poradí	poradit	k5eAaPmIp3nS	poradit
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
kniha	kniha	k1gFnSc1	kniha
stará	starat	k5eAaImIp3nS	starat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
udělat	udělat	k5eAaPmF	udělat
lepší	dobrý	k2eAgInSc4d2	lepší
obrázek	obrázek	k1gInSc4	obrázek
o	o	k7c6	o
princovi	princ	k1gMnSc6	princ
<g/>
.	.	kIx.	.
</s>
<s>
Dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
dostanou	dostat	k5eAaPmIp3nP	dostat
Ron	ron	k1gInSc4	ron
s	s	k7c7	s
Harrym	Harrym	k1gInSc4	Harrym
vánoční	vánoční	k2eAgInPc4d1	vánoční
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
jako	jako	k9	jako
obvykle	obvykle	k6eAd1	obvykle
svetry	svetr	k1gInPc4	svetr
<g/>
,	,	kIx,	,
ošklivý	ošklivý	k2eAgInSc4d1	ošklivý
náhrdelník	náhrdelník	k1gInSc4	náhrdelník
Ronovi	Ron	k1gMnSc3	Ron
od	od	k7c2	od
Levandule	levandule	k1gFnSc2	levandule
a	a	k8xC	a
krabička	krabička	k1gFnSc1	krabička
červů	červ	k1gMnPc2	červ
od	od	k7c2	od
Krátury	Krátura	k1gFnSc2	Krátura
pro	pro	k7c4	pro
páníčka	páníček	k1gMnSc4	páníček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Boží	boží	k2eAgInSc4d1	boží
hod	hod	k1gInSc4	hod
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
s	s	k7c7	s
Fleur	Fleura	k1gFnPc2	Fleura
a	a	k8xC	a
Lupinem	Lupino	k1gNnSc7	Lupino
<g/>
,	,	kIx,	,
cpe	cpát	k5eAaImIp3nS	cpát
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nepřijde	přijít	k5eNaPmIp3nS	přijít
Percy	Percy	k1gInPc4	Percy
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
Rufusem	Rufus	k1gMnSc7	Rufus
Brouskem	brousek	k1gInSc7	brousek
<g/>
.	.	kIx.	.
</s>
<s>
Rufus	Rufus	k1gInSc1	Rufus
pozdraví	pozdravit	k5eAaPmIp3nS	pozdravit
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
paní	paní	k1gFnSc1	paní
Weasleyové	Weasleyové	k2eAgFnSc1d1	Weasleyové
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
nedaleko	daleko	k6eNd1	daleko
<g/>
,	,	kIx,	,
když	když	k8xS	když
Percy	Perca	k1gFnPc1	Perca
"	"	kIx"	"
<g/>
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Brousek	brousek	k1gInSc1	brousek
chce	chtít	k5eAaImIp3nS	chtít
ale	ale	k9	ale
mluvit	mluvit	k5eAaImF	mluvit
s	s	k7c7	s
Harrym	Harrymum	k1gNnPc2	Harrymum
<g/>
.	.	kIx.	.
</s>
<s>
Jdou	jít	k5eAaImIp3nP	jít
ven	ven	k6eAd1	ven
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
ocení	ocenit	k5eAaPmIp3nP	ocenit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypadá	vypadat	k5eAaImIp3nS	vypadat
drsněji	drsně	k6eAd2	drsně
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
korpulentní	korpulentní	k2eAgMnSc1d1	korpulentní
předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Rufus	Rufus	k1gInSc1	Rufus
chce	chtít	k5eAaImIp3nS	chtít
po	po	k7c4	po
Harrym	Harrym	k1gInSc4	Harrym
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pravidelně	pravidelně	k6eAd1	pravidelně
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vyvolený	vyvolený	k2eAgMnSc1d1	vyvolený
<g/>
"	"	kIx"	"
a	a	k8xC	a
velký	velký	k2eAgMnSc1d1	velký
hrdina	hrdina	k1gMnSc1	hrdina
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
postupy	postup	k1gInPc7	postup
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
to	ten	k3xDgNnSc1	ten
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zavřeli	zavřít	k5eAaPmAgMnP	zavřít
Stana	Stan	k1gMnSc2	Stan
Silničku	silnička	k1gFnSc4	silnička
<g/>
.	.	kIx.	.
</s>
<s>
Rufus	Rufus	k1gInSc1	Rufus
je	být	k5eAaImIp3nS	být
najednou	najednou	k6eAd1	najednou
chladný	chladný	k2eAgInSc1d1	chladný
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
z	z	k7c2	z
Harryho	Harry	k1gMnSc2	Harry
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
Brumbálovi	brumbál	k1gMnSc6	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nic	nic	k6eAd1	nic
neřekne	říct	k5eNaPmIp3nS	říct
a	a	k8xC	a
varuje	varovat	k5eAaImIp3nS	varovat
Rufuse	Rufuse	k1gFnSc1	Rufuse
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
nezahrával	zahrávat	k5eNaImAgMnS	zahrávat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
Ginny	Ginn	k1gInPc1	Ginn
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nP	vracet
po	po	k7c6	po
Vánocích	Vánoce	k1gFnPc6	Vánoce
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společenské	společenský	k2eAgFnSc6d1	společenská
místnosti	místnost	k1gFnSc6	místnost
potkají	potkat	k5eAaPmIp3nP	potkat
Hermionu	Hermion	k1gInSc2	Hermion
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zcela	zcela	k6eAd1	zcela
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
Rona	Ron	k1gMnSc4	Ron
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
řekne	říct	k5eAaPmIp3nS	říct
Hermioně	Hermioň	k1gFnPc4	Hermioň
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
v	v	k7c6	v
Doupěti	doupě	k1gNnSc6	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
Lupinem	Lupin	k1gMnSc7	Lupin
a	a	k8xC	a
panem	pan	k1gMnSc7	pan
Weasleym	Weasleym	k1gInSc4	Weasleym
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
upozorní	upozornit	k5eAaPmIp3nP	upozornit
Harryho	Harry	k1gMnSc4	Harry
na	na	k7c4	na
Fenrira	Fenrir	k1gMnSc4	Fenrir
Šedohřbeta	Šedohřbet	k1gMnSc4	Šedohřbet
<g/>
.	.	kIx.	.
</s>
<s>
Malfoy	Malfoa	k1gFnPc1	Malfoa
jím	on	k3xPp3gMnSc7	on
hrozil	hrozit	k5eAaImAgInS	hrozit
Borginovi	Borgin	k1gMnSc3	Borgin
<g/>
,	,	kIx,	,
tak	tak	k9	tak
konečně	konečně	k6eAd1	konečně
teď	teď	k6eAd1	teď
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
koho	kdo	k3yInSc4	kdo
tím	ten	k3xDgNnSc7	ten
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nástěnce	nástěnka	k1gFnSc6	nástěnka
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
komu	kdo	k3yInSc3	kdo
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
bude	být	k5eAaImBp3nS	být
sedmnáct	sedmnáct	k4xCc4	sedmnáct
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
začít	začít	k5eAaPmF	začít
chodit	chodit	k5eAaImF	chodit
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
přemisťování	přemisťování	k1gNnSc4	přemisťování
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
Rona	Ron	k1gMnSc2	Ron
i	i	k8xC	i
Hermionu	Hermion	k1gInSc2	Hermion
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
a	a	k8xC	a
ptají	ptat	k5eAaImIp3nP	ptat
se	se	k3xPyFc4	se
Harryho	Harryha	k1gMnSc5	Harryha
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc1	jaký
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
ze	z	k7c2	z
šestého	šestý	k4xOgInSc2	šestý
ročníku	ročník	k1gInSc2	ročník
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
to	ten	k3xDgNnSc4	ten
zažil	zažít	k5eAaPmAgMnS	zažít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
je	on	k3xPp3gInPc4	on
odbude	odbýt	k5eAaPmIp3nS	odbýt
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
na	na	k7c4	na
třetí	třetí	k4xOgFnSc4	třetí
hodinu	hodina	k1gFnSc4	hodina
k	k	k7c3	k
Brumbálovi	brumbál	k1gMnSc3	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
Brumbálovi	brumbál	k1gMnSc3	brumbál
řekne	říct	k5eAaPmIp3nS	říct
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
Dracovi	Draec	k1gMnSc6	Draec
<g/>
,	,	kIx,	,
Rufusovi	Rufus	k1gMnSc6	Rufus
Brouskovi	Brousek	k1gMnSc6	Brousek
a	a	k8xC	a
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
událo	udát	k5eAaPmAgNnS	udát
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrymum	k1gNnSc3	Harrymum
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snapeův	Snapeův	k2eAgInSc4d1	Snapeův
rozhovor	rozhovor	k1gInSc4	rozhovor
ho	on	k3xPp3gMnSc4	on
netrápí	trápit	k5eNaImIp3nS	trápit
a	a	k8xC	a
že	že	k8xS	že
i	i	k9	i
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
s	s	k7c7	s
Rufusem	Rufus	k1gInSc7	Rufus
hádal	hádat	k5eAaImAgMnS	hádat
kvůli	kvůli	k7c3	kvůli
Harrymu	Harrym	k1gInSc3	Harrym
<g/>
.	.	kIx.	.
</s>
<s>
Brumbálovi	brumbál	k1gMnSc3	brumbál
se	se	k3xPyFc4	se
zajiskří	zajiskřit	k5eAaPmIp3nS	zajiskřit
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
<g/>
,	,	kIx,	,
když	když	k8xS	když
Harry	Harr	k1gMnPc4	Harr
imituje	imitovat	k5eAaBmIp3nS	imitovat
Brouska	Brouska	k1gFnSc1	Brouska
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Harry	Harr	k1gInPc4	Harr
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
skrz	skrz	k6eAd1	skrz
naskrz	naskrz	k6eAd1	naskrz
Brumbálův	brumbálův	k2eAgMnSc1d1	brumbálův
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
víc	hodně	k6eAd2	hodně
upevní	upevnit	k5eAaPmIp3nS	upevnit
přátelství	přátelství	k1gNnSc4	přátelství
mezi	mezi	k7c4	mezi
Harrym	Harrym	k1gInSc4	Harrym
a	a	k8xC	a
Brumbálem	brumbál	k1gMnSc7	brumbál
(	(	kIx(	(
<g/>
úplný	úplný	k2eAgInSc1d1	úplný
opak	opak	k1gInSc1	opak
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaký	jaký	k3yQgMnSc1	jaký
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gInSc4	jejich
vztah	vztah	k1gInSc4	vztah
v	v	k7c6	v
páté	pátý	k4xOgFnSc6	pátý
knize	kniha	k1gFnSc6	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
vydají	vydat	k5eAaPmIp3nP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
za	za	k7c7	za
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
Raddle	Raddle	k1gMnSc1	Raddle
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
.	.	kIx.	.
</s>
<s>
Učitelé	učitel	k1gMnPc1	učitel
toho	ten	k3xDgNnSc2	ten
od	od	k7c2	od
Raddlea	Raddle	k1gInSc2	Raddle
moc	moc	k6eAd1	moc
nečekají	čekat	k5eNaImIp3nP	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
slušný	slušný	k2eAgMnSc1d1	slušný
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
vypadá	vypadat	k5eAaImIp3nS	vypadat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ctižádostivý	ctižádostivý	k2eAgMnSc1d1	ctižádostivý
sirotek	sirotek	k1gMnSc1	sirotek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
mají	mít	k5eAaImIp3nP	mít
soucit	soucit	k1gInSc4	soucit
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
předchůdci	předchůdce	k1gMnPc1	předchůdce
Smrtijedů	Smrtijed	k1gMnPc2	Smrtijed
už	už	k9	už
tehdy	tehdy	k6eAd1	tehdy
shromažďovali	shromažďovat	k5eAaImAgMnP	shromažďovat
kolem	kolem	k7c2	kolem
Raddlea	Raddle	k1gInSc2	Raddle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
Raddleovi	Raddleus	k1gMnSc3	Raddleus
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
z	z	k7c2	z
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Raddle	Raddle	k6eAd1	Raddle
je	být	k5eAaImIp3nS	být
posedlý	posedlý	k2eAgInSc1d1	posedlý
svým	svůj	k3xOyFgInSc7	svůj
původem	původ	k1gInSc7	původ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
je	být	k5eAaImIp3nS	být
Morfinova	Morfinův	k2eAgFnSc1d1	Morfinův
<g/>
.	.	kIx.	.
</s>
<s>
Raddle	Raddle	k6eAd1	Raddle
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
domu	dům	k1gInSc2	dům
Gauntových	Gauntův	k2eAgMnPc2d1	Gauntův
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
s	s	k7c7	s
Morfinem	morfin	k1gInSc7	morfin
hadím	hadí	k2eAgInSc7d1	hadí
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
náhle	náhle	k6eAd1	náhle
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Morfin	morfin	k1gInSc1	morfin
si	se	k3xPyFc3	se
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
nic	nic	k3yNnSc1	nic
nepamatoval	pamatovat	k5eNaImAgMnS	pamatovat
<g/>
,	,	kIx,	,
až	až	k9	až
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
probudí	probudit	k5eAaPmIp3nS	probudit
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
dodá	dodat	k5eAaPmIp3nS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tu	tu	k6eAd1	tu
samou	samý	k3xTgFnSc4	samý
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgMnS	být
Raddle	Raddle	k1gMnSc1	Raddle
starší	starší	k1gMnSc1	starší
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
nalezeni	naleznout	k5eAaPmNgMnP	naleznout
zavražděni	zavražděn	k2eAgMnPc1d1	zavražděn
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vyslýchá	vyslýchat	k5eAaImIp3nS	vyslýchat
Morfina	Morfina	k1gFnSc1	Morfina
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
přizná	přiznat	k5eAaPmIp3nS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
Brumbál	brumbál	k1gMnSc1	brumbál
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Raddle	Raddle	k1gMnSc1	Raddle
omráčil	omráčit	k5eAaPmAgMnS	omráčit
svého	svůj	k3xOyFgMnSc4	svůj
strýce	strýc	k1gMnSc4	strýc
Morfina	Morfin	k1gMnSc4	Morfin
<g/>
,	,	kIx,	,
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
otce	otec	k1gMnPc4	otec
a	a	k8xC	a
prarodiče	prarodič	k1gMnPc4	prarodič
a	a	k8xC	a
pak	pak	k6eAd1	pak
Morfinovi	Morfinův	k2eAgMnPc1d1	Morfinův
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
vložil	vložit	k5eAaPmAgMnS	vložit
falešné	falešný	k2eAgFnPc4d1	falešná
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
patří	patřit	k5eAaImIp3nS	patřit
Křiklanovi	Křiklan	k1gMnSc3	Křiklan
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
schůzka	schůzka	k1gFnSc1	schůzka
Klubu	klub	k1gInSc2	klub
slimáků	slimák	k1gMnPc2	slimák
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
Toma	Tom	k1gMnSc2	Tom
Raddlea	Raddleus	k1gMnSc2	Raddleus
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc1	dva
místa	místo	k1gNnPc1	místo
ve	v	k7c6	v
vzpomínce	vzpomínka	k1gFnSc6	vzpomínka
jsou	být	k5eAaImIp3nP	být
podivně	podivně	k6eAd1	podivně
zamlžená	zamlžený	k2eAgNnPc1d1	zamlžené
a	a	k8xC	a
je	on	k3xPp3gFnPc4	on
slyšet	slyšet	k5eAaImF	slyšet
pouze	pouze	k6eAd1	pouze
Křiklanův	Křiklanův	k2eAgInSc4d1	Křiklanův
hlas	hlas	k1gInSc4	hlas
(	(	kIx(	(
<g/>
když	když	k8xS	když
se	se	k3xPyFc4	se
Raddle	Raddle	k1gFnSc1	Raddle
ptá	ptat	k5eAaImIp3nS	ptat
Křiklana	Křiklana	k1gFnSc1	Křiklana
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
viteály	viteála	k1gFnPc1	viteála
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
mlze	mlha	k1gFnSc6	mlha
mu	on	k3xPp3gMnSc3	on
Křiklan	Křiklan	k1gInSc1	Křiklan
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
nic	nic	k6eAd1	nic
neví	vědět	k5eNaImIp3nS	vědět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
Křiklanovou	Křiklanový	k2eAgFnSc7d1	Křiklanový
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
někdo	někdo	k3yInSc1	někdo
manipuloval	manipulovat	k5eAaImAgMnS	manipulovat
a	a	k8xC	a
nařídí	nařídit	k5eAaPmIp3nS	nařídit
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tu	ten	k3xDgFnSc4	ten
pravou	pravý	k2eAgFnSc4d1	pravá
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
od	od	k7c2	od
něj	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
úkolem	úkol	k1gInSc7	úkol
svěří	svěřit	k5eAaPmIp3nP	svěřit
Hermioně	Hermioň	k1gFnPc1	Hermioň
a	a	k8xC	a
Ronovi	Ronův	k2eAgMnPc1d1	Ronův
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hodině	hodina	k1gFnSc6	hodina
Lektvarů	lektvar	k1gInPc2	lektvar
jim	on	k3xPp3gMnPc3	on
Křiklan	Křiklan	k1gMnSc1	Křiklan
dá	dát	k5eAaPmIp3nS	dát
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vyrobit	vyrobit	k5eAaPmF	vyrobit
protijed	protijed	k1gInSc4	protijed
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
jedy	jed	k1gInPc4	jed
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
nenajde	najít	k5eNaPmIp3nS	najít
žádnou	žádný	k3yNgFnSc4	žádný
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
poznámku	poznámka	k1gFnSc4	poznámka
a	a	k8xC	a
když	když	k8xS	když
zbývají	zbývat	k5eAaImIp3nP	zbývat
už	už	k6eAd1	už
jen	jen	k9	jen
2	[number]	k4	2
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
najde	najít	k5eAaPmIp3nS	najít
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
stránky	stránka	k1gFnSc2	stránka
napsáno	napsán	k2eAgNnSc1d1	napsáno
"	"	kIx"	"
<g/>
Prostě	prostě	k6eAd1	prostě
jim	on	k3xPp3gMnPc3	on
do	do	k7c2	do
krku	krk	k1gInSc2	krk
strč	strčit	k5eAaPmRp2nS	strčit
bezoár	bezoár	k1gInSc1	bezoár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
si	se	k3xPyFc3	se
vzpomene	vzpomenout	k5eAaPmIp3nS	vzpomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
jim	on	k3xPp3gMnPc3	on
Snape	Snap	k1gInSc5	Snap
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
kamen	kamna	k1gNnPc2	kamna
z	z	k7c2	z
žaludku	žaludek	k1gInSc2	žaludek
kozy	koza	k1gFnSc2	koza
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
bezoár	bezoár	k1gInSc1	bezoár
<g/>
)	)	kIx)	)
vás	vy	k3xPp2nPc4	vy
ochrání	ochránit	k5eAaPmIp3nP	ochránit
před	před	k7c7	před
většinou	většina	k1gFnSc7	většina
jedů	jed	k1gInPc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
doběhne	doběhnout	k5eAaPmIp3nS	doběhnout
do	do	k7c2	do
skříně	skříň	k1gFnSc2	skříň
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Nikomu	nikdo	k3yNnSc3	nikdo
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
se	se	k3xPyFc4	se
nepodaří	podařit	k5eNaPmIp3nS	podařit
protijed	protijed	k1gInSc1	protijed
uvařit	uvařit	k5eAaPmF	uvařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
Křiklan	Křiklan	k1gMnSc1	Křiklan
přijde	přijít	k5eAaPmIp3nS	přijít
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
Harryho	Harry	k1gMnSc2	Harry
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
bezoár	bezoár	k1gInSc1	bezoár
<g/>
.	.	kIx.	.
</s>
<s>
Křiklan	Křiklan	k1gMnSc1	Křiklan
je	být	k5eAaImIp3nS	být
okouzlen	okouzlit	k5eAaPmNgMnS	okouzlit
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
(	(	kIx(	(
<g/>
Hermiona	Hermiona	k1gFnSc1	Hermiona
ale	ale	k8xC	ale
vůbec	vůbec	k9	vůbec
okouzlena	okouzlen	k2eAgFnSc1d1	okouzlena
není	být	k5eNaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
po	po	k7c6	po
hodině	hodina	k1gFnSc6	hodina
zůstane	zůstat	k5eAaPmIp3nS	zůstat
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
zeptá	zeptat	k5eAaPmIp3nS	zeptat
se	se	k3xPyFc4	se
Křiklana	Křiklana	k1gFnSc1	Křiklana
na	na	k7c4	na
viteály	viteála	k1gFnPc4	viteála
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Harry	Harr	k1gMnPc4	Harr
míří	mířit	k5eAaImIp3nS	mířit
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgFnSc1	ten
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dal	dát	k5eAaPmAgMnS	dát
Brumbálovi	brumbál	k1gMnSc3	brumbál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
mají	mít	k5eAaImIp3nP	mít
první	první	k4xOgFnSc4	první
hodinu	hodina	k1gFnSc4	hodina
přemisťování	přemisťování	k1gNnSc2	přemisťování
s	s	k7c7	s
kouzelníkem	kouzelník	k1gMnSc7	kouzelník
z	z	k7c2	z
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
přemístit	přemístit	k5eAaPmF	přemístit
jen	jen	k9	jen
o	o	k7c4	o
pár	pár	k4xCyI	pár
desítek	desítka	k1gFnPc2	desítka
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
několika	několik	k4yIc6	několik
pokusech	pokus	k1gInPc6	pokus
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
nepodaří	podařit	k5eNaPmIp3nS	podařit
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
Susan	Susan	k1gInSc1	Susan
Bonesová	Bonesový	k2eAgFnSc1d1	Bonesová
se	se	k3xPyFc4	se
na	na	k7c4	na
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
pokus	pokus	k1gInSc4	pokus
přemístí	přemístit	k5eAaPmIp3nS	přemístit
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
jedna	jeden	k4xCgFnSc1	jeden
noha	noha	k1gFnSc1	noha
ji	on	k3xPp3gFnSc4	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hodině	hodina	k1gFnSc6	hodina
zaslechne	zaslechnout	k5eAaPmIp3nS	zaslechnout
Harry	Harr	k1gMnPc4	Harr
rozhovor	rozhovor	k1gInSc1	rozhovor
mezi	mezi	k7c7	mezi
Malfoyem	Malfoy	k1gInSc7	Malfoy
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
poskoky	poskok	k1gInPc7	poskok
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
začne	začít	k5eAaPmIp3nS	začít
sledovat	sledovat	k5eAaImF	sledovat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Pobertova	pobertův	k2eAgInSc2d1	pobertův
plánku	plánek	k1gInSc2	plánek
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
má	mít	k5eAaImIp3nS	mít
narozeniny	narozeniny	k1gFnPc4	narozeniny
a	a	k8xC	a
když	když	k8xS	když
rozbaluje	rozbalovat	k5eAaImIp3nS	rozbalovat
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
přivlastní	přivlastnit	k5eAaPmIp3nS	přivlastnit
si	se	k3xPyFc3	se
i	i	k9	i
krabici	krabice	k1gFnSc4	krabice
čokoládových	čokoládový	k2eAgInPc2d1	čokoládový
bonbónů	bonbón	k1gInPc2	bonbón
s	s	k7c7	s
nápojem	nápoj	k1gInSc7	nápoj
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Harrymu	Harrym	k1gInSc2	Harrym
přinesla	přinést	k5eAaPmAgFnS	přinést
Romilda	Romilda	k1gFnSc1	Romilda
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
jich	on	k3xPp3gFnPc2	on
sní	snít	k5eAaImIp3nP	snít
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
být	být	k5eAaImF	být
Romildou	Romilda	k1gMnSc7	Romilda
posedlý	posedlý	k2eAgMnSc1d1	posedlý
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
a	a	k8xC	a
vezme	vzít	k5eAaPmIp3nS	vzít
Rona	Ron	k1gMnSc2	Ron
k	k	k7c3	k
Křiklanovi	Křiklan	k1gMnSc3	Křiklan
<g/>
.	.	kIx.	.
</s>
<s>
Křiklan	Křiklan	k1gInSc1	Křiklan
dá	dát	k5eAaPmIp3nS	dát
Ronovi	Ron	k1gMnSc3	Ron
protilátku	protilátka	k1gFnSc4	protilátka
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
připijí	připít	k5eAaPmIp3nP	připít
na	na	k7c6	na
Ronovi	Ron	k1gMnSc6	Ron
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
,	,	kIx,	,
a	a	k8xC	a
otevře	otevřít	k5eAaPmIp3nS	otevřít
lahev	lahev	k1gFnSc4	lahev
medoviny	medovina	k1gFnSc2	medovina
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
napije	napít	k5eAaBmIp3nS	napít
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
otráven	otrávit	k5eAaPmNgInS	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Křiklan	Křiklan	k1gInSc1	Křiklan
jedná	jednat	k5eAaImIp3nS	jednat
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harry	Harra	k1gFnPc4	Harra
použije	použít	k5eAaPmIp3nS	použít
bezoár	bezoár	k1gInSc4	bezoár
a	a	k8xC	a
strčí	strčit	k5eAaPmIp3nP	strčit
ho	on	k3xPp3gNnSc4	on
Ronovi	Ronův	k2eAgMnPc1d1	Ronův
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Rona	Ron	k1gMnSc4	Ron
odnesou	odnést	k5eAaPmIp3nP	odnést
na	na	k7c4	na
ošetřovnu	ošetřovna	k1gFnSc4	ošetřovna
a	a	k8xC	a
přijedou	přijet	k5eAaPmIp3nP	přijet
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Dozví	dozvědět	k5eAaPmIp3nS	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Ron	Ron	k1gMnSc1	Ron
musí	muset	k5eAaImIp3nS	muset
odpočinout	odpočinout	k5eAaPmF	odpočinout
a	a	k8xC	a
uzdraví	uzdravit	k5eAaPmIp3nP	uzdravit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
famfrpálovém	famfrpálový	k2eAgInSc6d1	famfrpálový
zápase	zápas	k1gInSc6	zápas
Rona	Ron	k1gMnSc2	Ron
nahradí	nahradit	k5eAaPmIp3nP	nahradit
na	na	k7c6	na
postu	post	k1gInSc6	post
brankáře	brankář	k1gMnSc2	brankář
Cormac	Cormac	k1gFnSc1	Cormac
McLaggen	McLaggen	k1gInSc1	McLaggen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ale	ale	k9	ale
snaží	snažit	k5eAaImIp3nS	snažit
řídit	řídit	k5eAaImF	řídit
tým	tým	k1gInSc4	tým
a	a	k8xC	a
půjčí	půjčit	k5eAaPmIp3nS	půjčit
si	se	k3xPyFc3	se
odrážečskou	odrážečský	k2eAgFnSc4d1	odrážečský
pálku	pálka	k1gFnSc4	pálka
<g/>
,	,	kIx,	,
odpálí	odpálit	k5eAaPmIp3nS	odpálit
potlouk	potlouct	k5eAaPmDgInS	potlouct
na	na	k7c4	na
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
to	ten	k3xDgNnSc1	ten
naštípne	naštípnout	k5eAaPmIp3nS	naštípnout
lebeční	lebeční	k2eAgFnSc4d1	lebeční
kost	kost	k1gFnSc4	kost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Harry	Harr	k1gMnPc4	Harr
také	také	k9	také
skončí	skončit	k5eAaPmIp3nS	skončit
na	na	k7c6	na
ošetřovně	ošetřovna	k1gFnSc6	ošetřovna
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
je	být	k5eAaImIp3nS	být
Harry	Harr	k1gInPc4	Harr
na	na	k7c6	na
ošetřovně	ošetřovna	k1gFnSc6	ošetřovna
<g/>
,	,	kIx,	,
napadne	napadnout	k5eAaPmIp3nS	napadnout
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Malfoy	Malfoy	k1gInPc4	Malfoy
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Zavolá	zavolat	k5eAaPmIp3nS	zavolat
Kráturu	Krátura	k1gFnSc4	Krátura
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
Siriusova	Siriusův	k2eAgMnSc2d1	Siriusův
domácího	domácí	k2eAgMnSc2d1	domácí
skřítka	skřítek	k1gMnSc2	skřítek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přijde	přijít	k5eAaPmIp3nS	přijít
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
Dobby	Dobba	k1gFnPc4	Dobba
a	a	k8xC	a
poperou	poprat	k5eAaPmIp3nP	poprat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Krátura	Krátura	k1gFnSc1	Krátura
urážel	urážet	k5eAaPmAgInS	urážet
v	v	k7c6	v
Dobbyho	Dobby	k1gMnSc2	Dobby
přítomnosti	přítomnost	k1gFnSc6	přítomnost
Harryho	Harry	k1gMnSc2	Harry
<g/>
.	.	kIx.	.
</s>
<s>
Harrymu	Harrym	k1gInSc2	Harrym
a	a	k8xC	a
Ronovi	Ron	k1gMnSc3	Ron
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
a	a	k8xC	a
Harry	Harra	k1gFnPc4	Harra
jim	on	k3xPp3gMnPc3	on
řekne	říct	k5eAaPmIp3nS	říct
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
plánu	plán	k1gInSc6	plán
<g/>
.	.	kIx.	.
</s>
<s>
Nařídí	nařídit	k5eAaPmIp3nP	nařídit
Kráturovi	Kráturův	k2eAgMnPc1d1	Kráturův
a	a	k8xC	a
Dobbymu	Dobbym	k1gInSc6	Dobbym
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všude	všude	k6eAd1	všude
sledovali	sledovat	k5eAaImAgMnP	sledovat
Draca	Drac	k2eAgMnSc4d1	Drac
Malfoye	Malfoy	k1gMnSc4	Malfoy
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
přilepili	přilepit	k5eAaPmAgMnP	přilepit
jako	jako	k8xS	jako
dvě	dva	k4xCgFnPc4	dva
náplasti	náplast	k1gFnPc1	náplast
na	na	k7c4	na
kuří	kuří	k2eAgNnPc4d1	kuří
oka	oko	k1gNnPc4	oko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
jim	on	k3xPp3gMnPc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gNnSc3	on
pravidelně	pravidelně	k6eAd1	pravidelně
dávali	dávat	k5eAaImAgMnP	dávat
hlášení	hlášení	k1gNnSc4	hlášení
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Malfoy	Malfo	k2eAgFnPc1d1	Malfo
chodí	chodit	k5eAaImIp3nP	chodit
a	a	k8xC	a
co	co	k3yQnSc4	co
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
má	mít	k5eAaImIp3nS	mít
další	další	k2eAgFnSc4d1	další
hodinu	hodina	k1gFnSc4	hodina
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
dvě	dva	k4xCgFnPc4	dva
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
dvacetiletý	dvacetiletý	k2eAgInSc1d1	dvacetiletý
Voldemort	Voldemort	k1gInSc1	Voldemort
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
nákupčí	nákupčí	k1gFnSc1	nákupčí
pro	pro	k7c4	pro
Borgina	Borgino	k1gNnPc4	Borgino
a	a	k8xC	a
Burkese	Burkese	k1gFnPc4	Burkese
<g/>
.	.	kIx.	.
</s>
<s>
Navštíví	navštívit	k5eAaPmIp3nS	navštívit
dům	dům	k1gInSc4	dům
jedné	jeden	k4xCgFnSc2	jeden
postarší	postarší	k2eAgFnSc2d1	postarší
dámy	dáma	k1gFnSc2	dáma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Hepziba	Hepziba	k1gFnSc1	Hepziba
Smithová	Smithová	k1gFnSc1	Smithová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
příbuznou	příbuzná	k1gFnSc4	příbuzná
Helgy	Helga	k1gFnSc2	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
<g/>
.	.	kIx.	.
</s>
<s>
Hepzibah	Hepzibah	k1gInSc1	Hepzibah
mu	on	k3xPp3gMnSc3	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
své	svůj	k3xOyFgInPc4	svůj
dva	dva	k4xCgInPc4	dva
poklady	poklad	k1gInPc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
šálek	šálek	k1gInSc1	šálek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kdysi	kdysi	k6eAd1	kdysi
patřil	patřit	k5eAaImAgMnS	patřit
Helze	Helze	k1gFnPc4	Helze
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
je	být	k5eAaImIp3nS	být
medailonek	medailonek	k1gInSc1	medailonek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dříve	dříve	k6eAd2	dříve
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
Meropa	Meropa	k1gFnSc1	Meropa
Gauntová	Gauntový	k2eAgFnSc1d1	Gauntová
a	a	k8xC	a
který	který	k3yRgInSc1	který
kdysi	kdysi	k6eAd1	kdysi
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
Salazar	Salazar	k1gMnSc1	Salazar
Zmijozel	Zmijozel	k1gMnSc1	Zmijozel
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
je	být	k5eAaImIp3nS	být
Brumbálova	brumbálův	k2eAgFnSc1d1	Brumbálova
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
cestoval	cestovat	k5eAaImAgMnS	cestovat
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgInS	ucházet
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
učitele	učitel	k1gMnSc2	učitel
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
připomene	připomenout	k5eAaPmIp3nS	připomenout
jeho	jeho	k3xOp3gFnPc4	jeho
opravdové	opravdový	k2eAgFnPc4d1	opravdová
pohnutky	pohnutka	k1gFnPc4	pohnutka
a	a	k8xC	a
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gMnSc4	on
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
přijmout	přijmout	k5eAaPmF	přijmout
Harry	Harr	k1gInPc4	Harr
najde	najít	k5eAaPmIp3nS	najít
v	v	k7c6	v
učebnici	učebnice	k1gFnSc6	učebnice
lektvarů	lektvar	k1gInPc2	lektvar
nové	nový	k2eAgNnSc1d1	nové
kouzlo	kouzlo	k1gNnSc1	kouzlo
Sectumsempra	Sectumsempr	k1gInSc2	Sectumsempr
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
ho	on	k3xPp3gMnSc4	on
zkusit	zkusit	k5eAaPmF	zkusit
<g/>
.	.	kIx.	.
</s>
<s>
Krátura	Krátura	k1gFnSc1	Krátura
a	a	k8xC	a
Dobby	Dobba	k1gFnPc1	Dobba
mu	on	k3xPp3gNnSc3	on
podají	podat	k5eAaPmIp3nP	podat
hlášení	hlášení	k1gNnSc4	hlášení
o	o	k7c6	o
Malfoyovi	Malfoya	k1gMnSc6	Malfoya
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravidelně	pravidelně	k6eAd1	pravidelně
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
Komnatu	komnata	k1gFnSc4	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Harry	Harra	k1gFnPc1	Harra
pokusí	pokusit	k5eAaPmIp3nP	pokusit
do	do	k7c2	do
komnaty	komnata	k1gFnSc2	komnata
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dveře	dveře	k1gFnPc1	dveře
zmizí	zmizet	k5eAaPmIp3nP	zmizet
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
i	i	k9	i
z	z	k7c2	z
mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Hagrid	Hagrid	k1gInSc1	Hagrid
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrym	k1gInSc3	Harrym
<g/>
,	,	kIx,	,
že	že	k8xS	že
pavouk	pavouk	k1gMnSc1	pavouk
Aragog	Aragog	k1gMnSc1	Aragog
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
a	a	k8xC	a
přeje	přát	k5eAaImIp3nS	přát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
Harry	Harra	k1gFnSc2	Harra
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
pohřbu	pohřeb	k1gInSc3	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
vypije	vypít	k5eAaPmIp3nS	vypít
lektvar	lektvar	k1gInSc1	lektvar
štěstí	štěstí	k1gNnSc2	štěstí
Felix	Felix	k1gMnSc1	Felix
felicis	felicis	k1gFnSc2	felicis
a	a	k8xC	a
pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
přemluvit	přemluvit	k5eAaPmF	přemluvit
profesora	profesor	k1gMnSc4	profesor
Křiklana	Křiklan	k1gMnSc4	Křiklan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
prozradil	prozradit	k5eAaPmAgMnS	prozradit
pravou	pravý	k2eAgFnSc4d1	pravá
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
lektvar	lektvar	k1gInSc1	lektvar
ho	on	k3xPp3gMnSc4	on
přiměje	přimět	k5eAaPmIp3nS	přimět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
šel	jít	k5eAaImAgMnS	jít
za	za	k7c7	za
Hagridem	Hagrid	k1gInSc7	Hagrid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
potká	potkat	k5eAaPmIp3nS	potkat
Křiklana	Křiklana	k1gFnSc1	Křiklana
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
k	k	k7c3	k
Harrymu	Harrymum	k1gNnSc3	Harrymum
připojí	připojit	k5eAaPmIp3nP	připojit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chce	chtít	k5eAaImIp3nS	chtít
získat	získat	k5eAaPmF	získat
trochu	trocha	k1gFnSc4	trocha
jedu	jed	k1gInSc2	jed
z	z	k7c2	z
akromantule	akromantule	k1gFnSc2	akromantule
(	(	kIx(	(
<g/>
Aragog	Aragog	k1gInSc1	Aragog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dojemném	dojemný	k2eAgInSc6d1	dojemný
pohřbu	pohřeb	k1gInSc6	pohřeb
se	se	k3xPyFc4	se
Hagrid	Hagrid	k1gInSc1	Hagrid
se	s	k7c7	s
Křiklanem	Křiklan	k1gInSc7	Křiklan
opijí	opít	k5eAaPmIp3nP	opít
a	a	k8xC	a
Harry	Harr	k1gInPc1	Harr
toho	ten	k3xDgNnSc2	ten
využije	využít	k5eAaPmIp3nS	využít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
Křiklan	Křiklan	k1gMnSc1	Křiklan
nebude	být	k5eNaImBp3nS	být
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
pamatovat	pamatovat	k5eAaImF	pamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Zmínkou	zmínka	k1gFnSc7	zmínka
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
Lilly	Lilla	k1gFnSc2	Lilla
Evansové	Evansová	k1gFnSc2	Evansová
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
měl	mít	k5eAaImAgMnS	mít
Křiklan	Křiklan	k1gMnSc1	Křiklan
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
přemluvit	přemluvit	k5eAaPmF	přemluvit
Křiklana	Křiklan	k1gMnSc4	Křiklan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
vyzradil	vyzradit	k5eAaPmAgMnS	vyzradit
svou	svůj	k3xOyFgFnSc4	svůj
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
na	na	k7c4	na
Voldemorta	Voldemort	k1gMnSc4	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Viteály	Viteála	k1gFnPc1	Viteála
jsou	být	k5eAaImIp3nP	být
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
lidskou	lidský	k2eAgFnSc4d1	lidská
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málo	málo	k4c1	málo
lidí	člověk	k1gMnPc2	člověk
zaklelo	zaklít	k5eAaPmAgNnS	zaklít
půlku	půlka	k1gFnSc4	půlka
své	svůj	k3xOyFgFnSc2	svůj
duše	duše	k1gFnSc2	duše
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
nerozdělil	rozdělit	k5eNaPmAgMnS	rozdělit
na	na	k7c4	na
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voldemort	Voldemort	k1gInSc1	Voldemort
v	v	k7c6	v
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
nesmrtelnosti	nesmrtelnost	k1gFnSc6	nesmrtelnost
<g/>
,	,	kIx,	,
udělal	udělat	k5eAaPmAgMnS	udělat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
:	:	kIx,	:
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
na	na	k7c4	na
7	[number]	k4	7
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ho	on	k3xPp3gMnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc1	jeden
viteál	viteál	k1gInSc1	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voldemort	Voldemort	k1gInSc1	Voldemort
si	se	k3xPyFc3	se
jako	jako	k8xS	jako
viteály	viteála	k1gFnSc2	viteála
vybral	vybrat	k5eAaPmAgMnS	vybrat
ojedinělé	ojedinělý	k2eAgInPc4d1	ojedinělý
předměty	předmět	k1gInPc4	předmět
s	s	k7c7	s
dávnou	dávný	k2eAgFnSc7d1	dávná
minulostí	minulost	k1gFnSc7	minulost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nezničil	zničit	k5eNaPmAgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Sedmý	sedmý	k4xOgInSc1	sedmý
kousek	kousek	k1gInSc1	kousek
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zbývá	zbývat	k5eAaImIp3nS	zbývat
ještě	ještě	k6eAd1	ještě
šest	šest	k4xCc1	šest
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
částí	část	k1gFnPc2	část
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
zničeny	zničen	k2eAgInPc1d1	zničen
<g/>
:	:	kIx,	:
deník	deník	k1gInSc1	deník
(	(	kIx(	(
<g/>
v	v	k7c6	v
Tajemné	tajemný	k2eAgFnSc6d1	tajemná
komnatě	komnata	k1gFnSc6	komnata
<g/>
)	)	kIx)	)
a	a	k8xC	a
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kdysi	kdysi	k6eAd1	kdysi
patřil	patřit	k5eAaImAgMnS	patřit
Rojvolu	Rojvola	k1gFnSc4	Rojvola
Gauntovi	Gaunt	k1gMnSc3	Gaunt
<g/>
,	,	kIx,	,
Voldemortovu	Voldemortův	k2eAgMnSc3d1	Voldemortův
dědečkovi	dědeček	k1gMnSc3	dědeček
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
Harrymu	Harrym	k1gInSc2	Harrym
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruku	ruka	k1gFnSc4	ruka
si	se	k3xPyFc3	se
poranil	poranit	k5eAaPmAgMnS	poranit
<g/>
,	,	kIx,	,
když	když	k8xS	když
ničil	ničit	k5eAaImAgMnS	ničit
ten	ten	k3xDgInSc4	ten
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jestli	jestli	k8xS	jestli
ještě	ještě	k6eAd1	ještě
najde	najít	k5eAaPmIp3nS	najít
nějaké	nějaký	k3yIgInPc4	nějaký
viteály	viteál	k1gInPc4	viteál
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
mu	on	k3xPp3gMnSc3	on
Harry	Harr	k1gInPc7	Harr
moci	moc	k1gFnSc2	moc
pomáhat	pomáhat	k5eAaImF	pomáhat
je	on	k3xPp3gMnPc4	on
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
nadále	nadále	k6eAd1	nadále
sleduje	sledovat	k5eAaImIp3nS	sledovat
Draca	Drac	k2eAgFnSc1d1	Draca
a	a	k8xC	a
najde	najít	k5eAaPmIp3nS	najít
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
fňuká	fňukat	k5eAaImIp3nS	fňukat
na	na	k7c6	na
záchodcích	záchodek	k1gInPc6	záchodek
Ufňukané	ufňukaný	k2eAgFnSc2d1	ufňukaná
Uršuly	Uršula	k1gFnSc2	Uršula
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
plánuje	plánovat	k5eAaImIp3nS	plánovat
cokoliv	cokoliv	k3yInSc1	cokoliv
<g/>
,	,	kIx,	,
bojí	bát	k5eAaImIp3nS	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
a	a	k8xC	a
bojí	bát	k5eAaImIp3nS	bát
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
životy	život	k1gInPc4	život
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
(	(	kIx(	(
<g/>
a	a	k8xC	a
svůj	svůj	k3xOyFgMnSc1	svůj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nepodaří	podařit	k5eNaPmIp3nS	podařit
udělat	udělat	k5eAaPmF	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
si	se	k3xPyFc3	se
ale	ale	k9	ale
Harryho	Harry	k1gMnSc4	Harry
všimne	všimnout	k5eAaPmIp3nS	všimnout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
chce	chtít	k5eAaImIp3nS	chtít
Malfoy	Malfoy	k1gInPc4	Malfoy
na	na	k7c4	na
Harryho	Harry	k1gMnSc4	Harry
použít	použít	k5eAaPmF	použít
kouzlo	kouzlo	k1gNnSc4	kouzlo
Cruciatus	Cruciatus	k1gInSc1	Cruciatus
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
Harry	Harra	k1gFnSc2	Harra
kouzlo	kouzlo	k1gNnSc4	kouzlo
Sectusempra	Sectusempr	k1gInSc2	Sectusempr
z	z	k7c2	z
učebnice	učebnice	k1gFnSc2	učebnice
prince	princ	k1gMnSc4	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
.	.	kIx.	.
</s>
<s>
Kouzlo	kouzlo	k1gNnSc1	kouzlo
Draca	Drac	k1gInSc2	Drac
těžce	těžce	k6eAd1	těžce
zraní	zranit	k5eAaPmIp3nS	zranit
<g/>
,	,	kIx,	,
všude	všude	k6eAd1	všude
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stříká	stříkat	k5eAaImIp3nS	stříkat
krev	krev	k1gFnSc1	krev
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
profesor	profesor	k1gMnSc1	profesor
Snape	Snap	k1gInSc5	Snap
a	a	k8xC	a
zachrání	zachránit	k5eAaPmIp3nP	zachránit
Dracovi	Dracův	k2eAgMnPc1d1	Dracův
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Harry	Harra	k1gFnSc2	Harra
použil	použít	k5eAaPmAgInS	použít
černou	černý	k2eAgFnSc4d1	černá
magii	magie	k1gFnSc4	magie
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgMnS	způsobit
vážné	vážný	k2eAgNnSc4d1	vážné
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
od	od	k7c2	od
Snapea	Snape	k1gInSc2	Snape
školní	školní	k2eAgInSc4d1	školní
trest	trest	k1gInSc4	trest
na	na	k7c4	na
zbytek	zbytek	k1gInSc4	zbytek
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zmešká	zmeškat	k5eAaPmIp3nS	zmeškat
finále	finále	k1gNnSc4	finále
ve	v	k7c6	v
famfrpálu	famfrpál	k1gInSc6	famfrpál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
chytače	chytač	k1gInSc2	chytač
nastoupí	nastoupit	k5eAaPmIp3nP	nastoupit
Ginny	Ginna	k1gFnPc1	Ginna
a	a	k8xC	a
Dean	Dean	k1gMnSc1	Dean
Thomas	Thomas	k1gMnSc1	Thomas
nahradí	nahradit	k5eAaPmIp3nS	nahradit
Ginny	Ginna	k1gFnPc4	Ginna
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
střelce	střelec	k1gMnSc2	střelec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Harry	Harra	k1gFnPc1	Harra
vrátí	vrátit	k5eAaPmIp3nP	vrátit
do	do	k7c2	do
nebelvírské	belvírský	k2eNgFnSc2d1	nebelvírská
společenské	společenský	k2eAgFnSc2d1	společenská
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nebelvír	Nebelvír	k1gMnSc1	Nebelvír
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
zápas	zápas	k1gInSc4	zápas
i	i	k9	i
školní	školní	k2eAgInSc4d1	školní
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzrušení	vzrušení	k1gNnSc6	vzrušení
Harry	Harra	k1gFnSc2	Harra
políbí	políbit	k5eAaPmIp3nP	políbit
Ginny	Ginen	k2eAgFnPc1d1	Ginna
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
Ron	ron	k1gInSc1	ron
zdráhavě	zdráhavě	k6eAd1	zdráhavě
požehnává	požehnávat	k5eAaImIp3nS	požehnávat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
a	a	k8xC	a
Ginny	Ginn	k1gInPc1	Ginn
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
šťastní	šťastný	k2eAgMnPc1d1	šťastný
a	a	k8xC	a
smějí	smát	k5eAaImIp3nP	smát
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgInPc1	jaký
drby	drb	k1gInPc1	drb
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
kolují	kolovat	k5eAaImIp3nP	kolovat
po	po	k7c6	po
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ron	Ron	k1gMnSc1	Ron
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
vztahem	vztah	k1gInSc7	vztah
nemá	mít	k5eNaImIp3nS	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
vyřkne	vyřknout	k5eAaPmIp3nS	vyřknout
svou	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
princi	princa	k1gFnSc6	princa
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
zavolá	zavolat	k5eAaPmIp3nS	zavolat
Harryho	Harry	k1gMnSc4	Harry
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
pracovny	pracovna	k1gFnSc2	pracovna
a	a	k8xC	a
Harry	Harra	k1gFnSc2	Harra
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
potká	potkat	k5eAaPmIp3nS	potkat
profesorku	profesorka	k1gFnSc4	profesorka
Trelawneyovou	Trelawneyová	k1gFnSc4	Trelawneyová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
Komnaty	komnata	k1gFnSc2	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Někdo	někdo	k3yInSc1	někdo
je	být	k5eAaImIp3nS	být
očividně	očividně	k6eAd1	očividně
uvnitř	uvnitř	k6eAd1	uvnitř
a	a	k8xC	a
nikoho	nikdo	k3yNnSc4	nikdo
jiného	jiný	k2eAgMnSc4d1	jiný
tam	tam	k6eAd1	tam
nepustí	pustit	k5eNaPmIp3nP	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Trelawneyová	Trelawneyová	k1gFnSc1	Trelawneyová
zaslechne	zaslechnout	k5eAaPmIp3nS	zaslechnout
výskot	výskot	k1gInSc4	výskot
a	a	k8xC	a
Harrymu	Harrymum	k1gNnSc6	Harrymum
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
že	že	k8xS	že
Malfoy	Malfoy	k1gInPc4	Malfoy
uspěl	uspět	k5eAaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Trelawneyová	Trelawneyová	k1gFnSc1	Trelawneyová
také	také	k9	také
Harrymu	Harrym	k1gInSc3	Harrym
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
tehdy	tehdy	k6eAd1	tehdy
vyslechl	vyslechnout	k5eAaPmAgMnS	vyslechnout
její	její	k3xOp3gNnSc4	její
proroctví	proroctví	k1gNnSc4	proroctví
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Snape	Snap	k1gMnSc5	Snap
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
teď	teď	k6eAd1	teď
Harry	Harr	k1gInPc4	Harr
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snape	Snap	k1gInSc5	Snap
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabil	zabít	k5eAaPmAgMnS	zabít
jeho	jeho	k3xOp3gMnPc4	jeho
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
vtrhne	vtrhnout	k5eAaPmIp3nS	vtrhnout
celý	celý	k2eAgInSc1d1	celý
rozčilený	rozčilený	k2eAgInSc1d1	rozčilený
k	k	k7c3	k
Brumbálovi	brumbál	k1gMnSc3	brumbál
a	a	k8xC	a
dožaduje	dožadovat	k5eAaImIp3nS	dožadovat
se	se	k3xPyFc4	se
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
Harryho	Harry	k1gMnSc2	Harry
uklidní	uklidnit	k5eAaPmIp3nS	uklidnit
a	a	k8xC	a
vydají	vydat	k5eAaPmIp3nP	vydat
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
za	za	k7c7	za
jednou	jeden	k4xCgFnSc7	jeden
částí	část	k1gFnSc7	část
Voldemortovy	Voldemortův	k2eAgFnSc2d1	Voldemortova
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
se	se	k3xPyFc4	se
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
přemístí	přemístit	k5eAaPmIp3nS	přemístit
na	na	k7c4	na
útes	útes	k1gInSc4	útes
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
kdysi	kdysi	k6eAd1	kdysi
sirotek	sirotek	k1gMnSc1	sirotek
Tom	Tom	k1gMnSc1	Tom
Raddle	Raddle	k1gMnSc1	Raddle
mučil	mučit	k5eAaImAgMnS	mučit
dva	dva	k4xCgInPc4	dva
malé	malý	k2eAgFnPc4d1	malá
sirotky	sirotka	k1gFnSc2	sirotka
<g/>
.	.	kIx.	.
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1	jeskyně
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
Brumbál	brumbál	k1gMnSc1	brumbál
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
musí	muset	k5eAaImIp3nP	muset
potopit	potopit	k5eAaPmF	potopit
a	a	k8xC	a
doplavat	doplavat	k5eAaPmF	doplavat
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
rozsvítí	rozsvítit	k5eAaPmIp3nS	rozsvítit
své	svůj	k3xOyFgFnSc2	svůj
hůlky	hůlka	k1gFnSc2	hůlka
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
najít	najít	k5eAaPmF	najít
nějaký	nějaký	k3yIgInSc4	nějaký
vchod	vchod	k1gInSc4	vchod
<g/>
.	.	kIx.	.
</s>
<s>
Probírá	probírat	k5eAaImIp3nS	probírat
se	se	k3xPyFc4	se
Voldemortovými	Voldemortův	k2eAgInPc7d1	Voldemortův
úmysly	úmysl	k1gInPc7	úmysl
a	a	k8xC	a
přijde	přijít	k5eAaPmIp3nS	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voldemort	Voldemort	k1gInSc1	Voldemort
chce	chtít	k5eAaImIp3nS	chtít
své	svůj	k3xOyFgMnPc4	svůj
soupeře	soupeř	k1gMnPc4	soupeř
oslabit	oslabit	k5eAaPmF	oslabit
<g/>
.	.	kIx.	.
</s>
<s>
Řízne	říznout	k5eAaPmIp3nS	říznout
se	se	k3xPyFc4	se
a	a	k8xC	a
pokape	pokapat	k5eAaPmIp3nS	pokapat
krví	krvit	k5eAaImIp3nS	krvit
zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
vchod	vchod	k1gInSc1	vchod
a	a	k8xC	a
Harry	Harr	k1gInPc1	Harr
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
se	se	k3xPyFc4	se
ocitnou	ocitnout	k5eAaPmIp3nP	ocitnout
u	u	k7c2	u
velikánského	velikánský	k2eAgNnSc2d1	velikánské
podzemního	podzemní	k2eAgNnSc2d1	podzemní
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
září	zářit	k5eAaImIp3nS	zářit
podivné	podivný	k2eAgNnSc1d1	podivné
zelené	zelený	k2eAgNnSc1d1	zelené
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nP	pokusit
použít	použít	k5eAaPmF	použít
kouzlo	kouzlo	k1gNnSc4	kouzlo
accio	accio	k6eAd1	accio
a	a	k8xC	a
přivolat	přivolat	k5eAaPmF	přivolat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
viteál	viteát	k5eAaPmAgMnS	viteát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodaří	podařit	k5eNaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
na	na	k7c4	na
chvilku	chvilka	k1gFnSc4	chvilka
vynoří	vynořit	k5eAaPmIp3nS	vynořit
něco	něco	k3yInSc1	něco
obrovského	obrovský	k2eAgNnSc2d1	obrovské
<g/>
.	.	kIx.	.
</s>
<s>
Kráčí	kráčet	k5eAaImIp3nS	kráčet
kolem	kolem	k7c2	kolem
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
instinktivně	instinktivně	k6eAd1	instinktivně
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
najít	najít	k5eAaPmF	najít
řetěz	řetěz	k1gInSc4	řetěz
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
přivázaná	přivázaný	k2eAgFnSc1d1	přivázaná
loďka	loďka	k1gFnSc1	loďka
<g/>
.	.	kIx.	.
</s>
<s>
Doplaví	doplavit	k5eAaPmIp3nS	doplavit
se	se	k3xPyFc4	se
do	do	k7c2	do
středu	střed	k1gInSc2	střed
jezera	jezero	k1gNnSc2	jezero
na	na	k7c4	na
malý	malý	k2eAgInSc4d1	malý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
mísa	mísa	k1gFnSc1	mísa
se	s	k7c7	s
zelenou	zelený	k2eAgFnSc7d1	zelená
tekutinou	tekutina	k1gFnSc7	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
viteál	viteál	k1gMnSc1	viteál
je	být	k5eAaImIp3nS	být
ponořený	ponořený	k2eAgMnSc1d1	ponořený
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
dotknou	dotknout	k5eAaPmIp3nP	dotknout
mísy	mísa	k1gFnPc4	mísa
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
té	ten	k3xDgFnSc2	ten
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
,	,	kIx,	,
Brumbál	brumbál	k1gMnSc1	brumbál
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
ji	on	k3xPp3gFnSc4	on
vypít	vypít	k5eAaPmF	vypít
<g/>
.	.	kIx.	.
</s>
<s>
Vykouzlí	vykouzlit	k5eAaPmIp3nS	vykouzlit
si	se	k3xPyFc3	se
pohár	pohár	k1gInSc4	pohár
a	a	k8xC	a
tekutinu	tekutina	k1gFnSc4	tekutina
vypije	vypít	k5eAaPmIp3nS	vypít
<g/>
.	.	kIx.	.
</s>
<s>
Trpí	trpět	k5eAaImIp3nS	trpět
strašnou	strašný	k2eAgFnSc7d1	strašná
bolestí	bolest	k1gFnSc7	bolest
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
prosí	prosit	k5eAaImIp3nS	prosit
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
protože	protože	k8xS	protože
Harry	Harra	k1gFnSc2	Harra
musel	muset	k5eAaImAgInS	muset
Brumbálovi	brumbál	k1gMnSc3	brumbál
slíbit	slíbit	k5eAaPmF	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
donutí	donutit	k5eAaPmIp3nP	donutit
vypít	vypít	k5eAaPmF	vypít
tekutinu	tekutina	k1gFnSc4	tekutina
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
kapičky	kapička	k1gFnSc2	kapička
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnPc4	Harra
vydrží	vydržet	k5eAaPmIp3nS	vydržet
a	a	k8xC	a
Brumbál	brumbál	k1gMnSc1	brumbál
to	ten	k3xDgNnSc4	ten
vypije	vypít	k5eAaPmIp3nS	vypít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Harry	Harra	k1gFnPc1	Harra
pokusí	pokusit	k5eAaPmIp3nP	pokusit
nabrat	nabrat	k5eAaPmF	nabrat
Brumbálovi	brumbál	k1gMnSc3	brumbál
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
se	se	k3xPyFc4	se
vynořovat	vynořovat	k5eAaImF	vynořovat
mrtvá	mrtvý	k2eAgNnPc1d1	mrtvé
těla	tělo	k1gNnPc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
je	on	k3xPp3gNnPc4	on
zastraší	zastrašit	k5eAaPmIp3nS	zastrašit
kouzelným	kouzelný	k2eAgInSc7d1	kouzelný
ohněm	oheň	k1gInSc7	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mísy	mísa	k1gFnSc2	mísa
vyjmou	vyjmout	k5eAaPmIp3nP	vyjmout
medailon	medailon	k1gInSc4	medailon
a	a	k8xC	a
oslabený	oslabený	k2eAgMnSc1d1	oslabený
Brumbál	brumbál	k1gMnSc1	brumbál
s	s	k7c7	s
Harrym	Harrym	k1gInSc4	Harrym
pádlují	pádlovat	k5eAaImIp3nP	pádlovat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
podzemního	podzemní	k2eAgNnSc2d1	podzemní
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečně	bezpečně	k6eAd1	bezpečně
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
z	z	k7c2	z
jeskyně	jeskyně	k1gFnSc2	jeskyně
a	a	k8xC	a
Harry	Harra	k1gFnSc2	Harra
je	být	k5eAaImIp3nS	být
oba	dva	k4xCgInPc1	dva
přemístí	přemístit	k5eAaPmIp3nP	přemístit
do	do	k7c2	do
Prasinek	Prasinka	k1gFnPc2	Prasinka
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
Brumbál	brumbál	k1gMnSc1	brumbál
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Madam	madam	k1gFnSc1	madam
Rosmerta	Rosmerta	k1gFnSc1	Rosmerta
jim	on	k3xPp3gMnPc3	on
půjčí	půjčit	k5eAaPmIp3nS	půjčit
košťata	koště	k1gNnPc4	koště
a	a	k8xC	a
oba	dva	k4xCgMnPc4	dva
letí	letět	k5eAaImIp3nS	letět
k	k	k7c3	k
astronomické	astronomický	k2eAgFnSc3d1	astronomická
věži	věž	k1gFnSc3	věž
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
Znamení	znamení	k1gNnSc3	znamení
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
zavládl	zavládnout	k5eAaPmAgInS	zavládnout
chaos	chaos	k1gInSc1	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
věži	věž	k1gFnSc6	věž
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
Draco	Draco	k6eAd1	Draco
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
znehybní	znehybnit	k5eAaPmIp3nS	znehybnit
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
neviditelným	viditelný	k2eNgInSc7d1	neviditelný
pláštěm	plášť	k1gInSc7	plášť
a	a	k8xC	a
Draco	Draco	k6eAd1	Draco
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
úkol	úkol	k1gInSc1	úkol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dostal	dostat	k5eAaPmAgInS	dostat
od	od	k7c2	od
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zabít	zabít	k5eAaPmF	zabít
Brumbála	brumbál	k1gMnSc4	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomocí	pomocí	k7c2	pomocí
rozplývavé	rozplývavý	k2eAgFnSc2d1	rozplývavá
skříně	skříň	k1gFnSc2	skříň
propašoval	propašovat	k5eAaPmAgMnS	propašovat
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
další	další	k2eAgMnPc4d1	další
Smrtijedy	Smrtijed	k1gMnPc4	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Použil	použít	k5eAaPmAgInS	použít
rozplývavou	rozplývavý	k2eAgFnSc4d1	rozplývavá
skříň	skříň	k1gFnSc4	skříň
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
minulý	minulý	k2eAgInSc4d1	minulý
rok	rok	k1gInSc4	rok
zmizel	zmizet	k5eAaPmAgMnS	zmizet
Montague	Montague	k1gNnSc7	Montague
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
Borgin	Borgina	k1gFnPc2	Borgina
a	a	k8xC	a
Burkes	Burkesa	k1gFnPc2	Burkesa
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
rozmlouvá	rozmlouvat	k5eAaImIp3nS	rozmlouvat
s	s	k7c7	s
Malfoyem	Malfoyum	k1gNnSc7	Malfoyum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
dobra	dobro	k1gNnSc2	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Malfoy	Malfoa	k1gFnPc4	Malfoa
váhá	váhat	k5eAaImIp3nS	váhat
a	a	k8xC	a
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
splnit	splnit	k5eAaPmF	splnit
svůj	svůj	k3xOyFgInSc4	svůj
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
přijdou	přijít	k5eAaPmIp3nP	přijít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
další	další	k2eAgMnPc1d1	další
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
Snape	Snap	k1gInSc5	Snap
a	a	k8xC	a
chvilku	chvilka	k1gFnSc4	chvilka
si	se	k3xPyFc3	se
mapuje	mapovat	k5eAaImIp3nS	mapovat
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
tiše	tiš	k1gFnSc2	tiš
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
na	na	k7c4	na
Brumbála	brumbál	k1gMnSc4	brumbál
chvíli	chvíle	k1gFnSc4	chvíle
kouká	koukat	k5eAaImIp3nS	koukat
<g/>
,	,	kIx,	,
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
má	mít	k5eAaImIp3nS	mít
výraz	výraz	k1gInSc1	výraz
nenávisti	nenávist	k1gFnSc2	nenávist
a	a	k8xC	a
zášti	zášť	k1gFnSc2	zášť
<g/>
.	.	kIx.	.
</s>
<s>
Brumbálova	brumbálův	k2eAgNnPc1d1	Brumbálovo
poslední	poslední	k2eAgNnPc1d1	poslední
slova	slovo	k1gNnPc1	slovo
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
Severusi	Severus	k1gMnPc1	Severus
....	....	k?	....
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
zdvihne	zdvihnout	k5eAaPmIp3nS	zdvihnout
hůlku	hůlka	k1gFnSc4	hůlka
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
kouzla	kouzlo	k1gNnSc2	kouzlo
Avada	Avada	k1gMnSc1	Avada
Kedavra	Kedavra	k1gMnSc1	Kedavra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Brumbálovi	brumbálův	k2eAgMnPc1d1	brumbálův
narazí	narazit	k5eAaPmIp3nP	narazit
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
hrudi	hruď	k1gFnSc2	hruď
a	a	k8xC	a
vyhodího	vyhodí	k1gMnSc2	vyhodí
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Snape	Snap	k1gInSc5	Snap
zabije	zabít	k5eAaPmIp3nS	zabít
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Drakem	drak	k1gInSc7	drak
prchají	prchat	k5eAaImIp3nP	prchat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
je	být	k5eAaImIp3nS	být
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
bradavickými	bradavický	k2eAgMnPc7d1	bradavický
studenty	student	k1gMnPc7	student
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
bojují	bojovat	k5eAaImIp3nP	bojovat
se	s	k7c7	s
Smrtijedy	Smrtijed	k1gMnPc7	Smrtijed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hagrid	Hagrid	k1gInSc1	Hagrid
zatím	zatím	k6eAd1	zatím
Snapea	Snape	k2eAgFnSc1d1	Snapea
a	a	k8xC	a
Malfoye	Malfoye	k1gFnSc1	Malfoye
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
žilách	žíla	k1gFnPc6	žíla
koluje	kolovat	k5eAaImIp3nS	kolovat
obří	obří	k2eAgFnSc4d1	obří
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
odolává	odolávat	k5eAaImIp3nS	odolávat
Snapeovým	Snapeův	k2eAgNnPc3d1	Snapeovo
kouzlům	kouzlo	k1gNnPc3	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
je	on	k3xPp3gInPc4	on
doběhne	doběhnout	k5eAaPmIp3nS	doběhnout
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
Snapea	Snapea	k1gMnSc1	Snapea
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
různá	různý	k2eAgNnPc4d1	různé
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
i	i	k8xC	i
sectusempra	sectusempro	k1gNnPc4	sectusempro
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
našel	najít	k5eAaPmAgMnS	najít
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
prince	princ	k1gMnSc2	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Snapeovi	Snapeův	k2eAgMnPc1d1	Snapeův
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
daří	dařit	k5eAaImIp3nP	dařit
odrážet	odrážet	k5eAaImF	odrážet
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
pokusí	pokusit	k5eAaPmIp3nS	pokusit
o	o	k7c4	o
kouzlo	kouzlo	k1gNnSc4	kouzlo
cruciatus	cruciatus	k1gInSc1	cruciatus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěje	uspět	k5eNaPmIp3nS	uspět
a	a	k8xC	a
Snape	Snap	k1gInSc5	Snap
se	se	k3xPyFc4	se
přizná	přiznat	k5eAaPmIp3nS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgNnPc4	ten
kouzla	kouzlo	k1gNnPc4	kouzlo
sám	sám	k3xTgInSc1	sám
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgMnSc7	ten
princem	princ	k1gMnSc7	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
!	!	kIx.	!
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
zabrání	zabrání	k1gNnSc4	zabrání
Smrtijedům	Smrtijed	k1gMnPc3	Smrtijed
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zabili	zabít	k5eAaPmAgMnP	zabít
Harryho	Harry	k1gMnSc4	Harry
(	(	kIx(	(
<g/>
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
chce	chtít	k5eAaImIp3nS	chtít
zabít	zabít	k5eAaPmF	zabít
Voldemort	Voldemort	k1gInSc4	Voldemort
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
utečou	utéct	k5eAaPmIp3nP	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
a	a	k8xC	a
Hagrid	Hagrid	k1gInSc1	Hagrid
dorazí	dorazit	k5eAaPmIp3nS	dorazit
k	k	k7c3	k
hloučku	hlouček	k1gInSc3	hlouček
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
sklání	sklánět	k5eAaImIp3nS	sklánět
nad	nad	k7c7	nad
Brumbálovým	brumbálův	k2eAgNnSc7d1	Brumbálovo
zkrouceným	zkroucený	k2eAgNnSc7d1	zkroucené
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
si	se	k3xPyFc3	se
z	z	k7c2	z
Brumbálovy	brumbálův	k2eAgFnSc2d1	Brumbálova
kapsy	kapsa	k1gFnSc2	kapsa
vezme	vzít	k5eAaPmIp3nS	vzít
medailon	medailon	k1gInSc4	medailon
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
najde	najít	k5eAaPmIp3nS	najít
záhadný	záhadný	k2eAgInSc4d1	záhadný
lístek	lístek	k1gInSc4	lístek
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
Pána	pán	k1gMnSc4	pán
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
medailonek	medailonek	k1gInSc1	medailonek
není	být	k5eNaImIp3nS	být
pravý	pravý	k2eAgInSc1d1	pravý
viteál	viteál	k1gInSc1	viteál
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgInSc1	ten
pravý	pravý	k2eAgInSc1d1	pravý
byl	být	k5eAaImAgInS	být
ukraden	ukraden	k2eAgInSc1d1	ukraden
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
lístku	lístek	k1gInSc2	lístek
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
pomůže	pomoct	k5eAaPmIp3nS	pomoct
ke	k	k7c3	k
zkáze	zkáza	k1gFnSc3	zkáza
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Podepsal	podepsat	k5eAaPmAgMnS	podepsat
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
R.	R.	kA	R.
A.	A.	kA	A.
B.	B.	kA	B.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaPmNgFnS	nazvat
podle	podle	k7c2	podle
zpěvu	zpěv	k1gInSc2	zpěv
ptáka	pták	k1gMnSc2	pták
fénixe	fénix	k1gMnSc2	fénix
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
na	na	k7c6	na
ošetřovně	ošetřovna	k1gFnSc6	ošetřovna
zaposlouchali	zaposlouchat	k5eAaPmAgMnP	zaposlouchat
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
však	však	k9	však
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Hagrid	Hagrid	k1gInSc1	Hagrid
snaží	snažit	k5eAaImIp3nS	snažit
Harryho	Harry	k1gMnSc4	Harry
odtáhnout	odtáhnout	k5eAaPmF	odtáhnout
od	od	k7c2	od
Brumbálova	brumbálův	k2eAgNnSc2d1	Brumbálovo
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Ginny	Ginn	k1gInPc1	Ginn
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
profesorky	profesorka	k1gFnSc2	profesorka
McGonagallové	McGonagallová	k1gFnSc2	McGonagallová
má	mít	k5eAaImIp3nS	mít
Harryho	Harry	k1gMnSc4	Harry
přivést	přivést	k5eAaPmF	přivést
na	na	k7c4	na
ošetřovnu	ošetřovna	k1gFnSc4	ošetřovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
skoro	skoro	k6eAd1	skoro
všichni	všechen	k3xTgMnPc1	všechen
Weasleyovi	Weasleyův	k2eAgMnPc1d1	Weasleyův
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
Harry	Harra	k1gMnSc2	Harra
Ginny	Ginna	k1gMnSc2	Ginna
ptá	ptat	k5eAaImIp3nS	ptat
kdo	kdo	k3yQnSc1	kdo
další	další	k2eAgMnSc1d1	další
kromě	kromě	k7c2	kromě
Brumbála	brumbál	k1gMnSc4	brumbál
zemřel	zemřít	k5eAaPmAgMnS	zemřít
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Ginny	Ginn	k1gInPc4	Ginn
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kratiknot	kratiknot	k1gInSc1	kratiknot
byl	být	k5eAaImAgInS	být
omráčen	omráčit	k5eAaPmNgInS	omráčit
Snapeem	Snapeus	k1gMnSc7	Snapeus
<g/>
,	,	kIx,	,
Nevile	Nevila	k1gFnSc3	Nevila
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ošetřovně	ošetřovna	k1gFnSc6	ošetřovna
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
Smrtijed	Smrtijed	k1gMnSc1	Smrtijed
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
jiným	jiný	k2eAgMnSc7d1	jiný
Smrtijedem	Smrtijed	k1gMnSc7	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ošetřovně	ošetřovna	k1gFnSc6	ošetřovna
také	také	k9	také
leží	ležet	k5eAaImIp3nS	ležet
pokousaný	pokousaný	k2eAgMnSc1d1	pokousaný
Bill	Bill	k1gMnSc1	Bill
od	od	k7c2	od
vlkodlaka	vlkodlak	k1gMnSc2	vlkodlak
Šedohřbeta	Šedohřbeto	k1gNnSc2	Šedohřbeto
<g/>
,	,	kIx,	,
Lupin	lupina	k1gFnPc2	lupina
ale	ale	k8xC	ale
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
Šedohřbet	Šedohřbet	k1gMnSc1	Šedohřbet
nebyl	být	k5eNaImAgMnS	být
promněněný	promněněný	k2eAgMnSc1d1	promněněný
Bill	Bill	k1gMnSc1	Bill
vlkodlakem	vlkodlak	k1gMnSc7	vlkodlak
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ptají	ptat	k5eAaImIp3nP	ptat
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Brumbál	brumbál	k1gMnSc1	brumbál
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnPc4	Harra
jim	on	k3xPp3gMnPc3	on
vypoví	vypovědět	k5eAaPmIp3nS	vypovědět
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
na	na	k7c6	na
Astronomické	astronomický	k2eAgFnSc6d1	astronomická
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
<g/>
,	,	kIx,	,
Lupin	lupina	k1gFnPc2	lupina
i	i	k8xC	i
pí	pí	k1gNnPc2	pí
<g/>
.	.	kIx.	.
</s>
<s>
Weasleyová	Weasleyovat	k5eAaPmIp3nS	Weasleyovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
chvilku	chvilka	k1gFnSc4	chvilka
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
pak	pak	k6eAd1	pak
Harrymu	Harrymum	k1gNnSc3	Harrymum
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Harry	Harr	k1gInPc1	Harr
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
na	na	k7c6	na
chodbách	chodba	k1gFnPc6	chodba
hlídkovali	hlídkovat	k5eAaImAgMnP	hlídkovat
jak	jak	k8xC	jak
učitelé	učitel	k1gMnPc1	učitel
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
BA	ba	k9	ba
(	(	kIx(	(
<g/>
Brumbálova	brumbálův	k2eAgFnSc1d1	Brumbálova
armáda	armáda	k1gFnSc1	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vzkaz	vzkaz	k1gInSc4	vzkaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Hermiona	Hermiona	k1gFnSc1	Hermiona
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
poslala	poslat	k5eAaPmAgFnS	poslat
po	po	k7c6	po
falešných	falešný	k2eAgInPc6d1	falešný
galeonech	galeon	k1gInPc6	galeon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
jen	jen	k9	jen
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
,	,	kIx,	,
Neville	Neville	k1gFnPc1	Neville
a	a	k8xC	a
Ginny	Ginna	k1gFnPc1	Ginna
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
hlídkovala	hlídkovat	k5eAaImAgFnS	hlídkovat
s	s	k7c7	s
Lenkou	Lenka	k1gFnSc7	Lenka
u	u	k7c2	u
Snapeova	Snapeův	k2eAgInSc2d1	Snapeův
kabinetu	kabinet	k1gInSc2	kabinet
a	a	k8xC	a
Ron	ron	k1gInSc1	ron
<g/>
,	,	kIx,	,
Neville	Neville	k1gNnPc1	Neville
a	a	k8xC	a
Ginny	Ginn	k1gInPc1	Ginn
u	u	k7c2	u
Komnaty	komnata	k1gFnSc2	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nenašli	najít	k5eNaPmAgMnP	najít
Malfoye	Malfoye	k1gFnSc4	Malfoye
na	na	k7c6	na
Pobetrově	Pobetrův	k2eAgInSc6d1	Pobetrův
plánku	plánek	k1gInSc6	plánek
a	a	k8xC	a
usoudili	usoudit	k5eAaPmAgMnP	usoudit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
právě	právě	k9	právě
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
vyšel	vyjít	k5eAaPmAgMnS	vyjít
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
hodil	hodit	k5eAaImAgMnS	hodit
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
Peruánský	peruánský	k2eAgInSc4d1	peruánský
zatmívací	zatmívací	k2eAgInSc4d1	zatmívací
prášek	prášek	k1gInSc4	prášek
a	a	k8xC	a
svítil	svítit	k5eAaImAgMnS	svítit
sobě	se	k3xPyFc3	se
a	a	k8xC	a
Smrtijedům	Smrtijed	k1gMnPc3	Smrtijed
rukou	ruka	k1gFnPc2	ruka
Slávy	Sláv	k1gMnPc4	Sláv
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
smrtijedů	smrtijed	k1gMnPc2	smrtijed
vyčaroval	vyčarovat	k5eAaPmAgMnS	vyčarovat
Znamení	znamení	k1gNnSc4	znamení
zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
zatím	zatím	k6eAd1	zatím
dole	dole	k6eAd1	dole
bojovali	bojovat	k5eAaImAgMnP	bojovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
Astronomické	astronomický	k2eAgFnSc6d1	astronomická
věži	věž	k1gFnSc6	věž
objevil	objevit	k5eAaPmAgMnS	objevit
Brumbál	brumbál	k1gMnSc1	brumbál
Malfoy	Malfoa	k1gFnSc2	Malfoa
ho	on	k3xPp3gNnSc4	on
měl	mít	k5eAaImAgMnS	mít
jít	jít	k5eAaImF	jít
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezvládl	zvládnout	k5eNaPmAgInS	zvládnout
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
nahoru	nahoru	k6eAd1	nahoru
trousili	trousit	k5eAaImAgMnP	trousit
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
a	a	k8xC	a
šel	jít	k5eAaImAgMnS	jít
tam	tam	k6eAd1	tam
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
Snape	Snap	k1gMnSc5	Snap
<g/>
.	.	kIx.	.
</s>
<s>
Vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
Astronomické	astronomický	k2eAgFnSc2d1	astronomická
věže	věž	k1gFnSc2	věž
byl	být	k5eAaImAgMnS	být
ale	ale	k9	ale
začarovaný	začarovaný	k2eAgMnSc1d1	začarovaný
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
bez	bez	k7c2	bez
Znamení	znamení	k1gNnSc2	znamení
zla	zlo	k1gNnSc2	zlo
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nedostal	dostat	k5eNaPmAgMnS	dostat
(	(	kIx(	(
<g/>
Lupina	lupina	k1gFnSc1	lupina
a	a	k8xC	a
Nevila	vít	k5eNaImAgFnS	vít
to	ten	k3xDgNnSc4	ten
kouzlo	kouzlo	k1gNnSc4	kouzlo
odhodilo	odhodit	k5eAaPmAgNnS	odhodit
na	na	k7c4	na
protější	protější	k2eAgFnSc4d1	protější
stěnu	stěna	k1gFnSc4	stěna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fleur	Fleur	k1gMnSc1	Fleur
pak	pak	k6eAd1	pak
Molly	Moll	k1gMnPc4	Moll
ujišťuje	ujišťovat	k5eAaImIp3nS	ujišťovat
že	že	k8xS	že
Billa	Bill	k1gMnSc2	Bill
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
i	i	k9	i
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gNnPc4	jeho
znetvoření	znetvoření	k1gNnPc4	znetvoření
a	a	k8xC	a
Molly	Molla	k1gFnPc4	Molla
jí	jíst	k5eAaImIp3nS	jíst
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
svolá	svolat	k5eAaPmIp3nS	svolat
poradu	porada	k1gFnSc4	porada
učitelů	učitel	k1gMnPc2	učitel
do	do	k7c2	do
ředitelny	ředitelna	k1gFnSc2	ředitelna
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
jestli	jestli	k8xS	jestli
by	by	kYmCp3nP	by
i	i	k9	i
po	po	k7c6	po
Brumbálově	brumbálův	k2eAgFnSc6d1	Brumbálova
smrti	smrt	k1gFnSc6	smrt
měly	mít	k5eAaImAgFnP	mít
Bradavice	bradavice	k1gFnPc1	bradavice
zůstat	zůstat	k5eAaPmF	zůstat
otevřeny	otevřen	k2eAgFnPc4d1	otevřena
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čekají	čekat	k5eAaImIp3nP	čekat
ještě	ještě	k6eAd1	ještě
na	na	k7c4	na
verdikt	verdikt	k1gInSc4	verdikt
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
se	se	k3xPyFc4	se
také	také	k9	také
snaží	snažit	k5eAaImIp3nS	snažit
od	od	k7c2	od
Harryho	Harry	k1gMnSc2	Harry
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
byli	být	k5eAaImAgMnP	být
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
jí	jíst	k5eAaImIp3nS	jíst
cokoli	cokoli	k3yInSc4	cokoli
říct	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kapitoly	kapitola	k1gFnSc2	kapitola
ještě	ještě	k6eAd1	ještě
Hermioně	Hermioň	k1gFnSc2	Hermioň
a	a	k8xC	a
Ronovi	Ron	k1gMnSc3	Ron
dává	dávat	k5eAaImIp3nS	dávat
přečíst	přečíst	k5eAaPmF	přečíst
vzkaz	vzkaz	k1gInSc4	vzkaz
od	od	k7c2	od
R.	R.	kA	R.
<g/>
A.B	A.B	k1gFnSc1	A.B
Hermiona	Hermiona	k1gFnSc1	Hermiona
řekne	říct	k5eAaPmIp3nS	říct
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eileen	Eileen	k1gInSc1	Eileen
Princeová	Princeová	k1gFnSc1	Princeová
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgMnPc4	který
si	se	k3xPyFc3	se
myslela	myslet	k5eAaImAgFnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
princem	princ	k1gMnSc7	princ
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Snapeova	Snapeův	k2eAgFnSc1d1	Snapeova
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Princeová	Princeová	k1gFnSc1	Princeová
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
mudlu	mudla	k1gFnSc4	mudla
Tobiase	Tobiasa	k1gFnSc3	Tobiasa
Snapea	Snapeus	k1gMnSc2	Snapeus
a	a	k8xC	a
pak	pak	k6eAd1	pak
porodila	porodit	k5eAaPmAgFnS	porodit
...	...	k?	...
Harry	Harra	k1gFnPc4	Harra
vykřikne	vykřiknout	k5eAaPmIp3nS	vykřiknout
"	"	kIx"	"
<g/>
vraha	vrah	k1gMnSc4	vrah
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
je	být	k5eAaImIp3nS	být
dvojí	dvojí	k4xRgFnPc4	dvojí
krve	krev	k1gFnPc4	krev
jako	jako	k8xS	jako
Voldemort	Voldemort	k1gInSc4	Voldemort
(	(	kIx(	(
<g/>
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnPc1	jejich
mudlovští	mudlovský	k2eAgMnPc1d1	mudlovský
otcové	otec	k1gMnPc1	otec
zradili	zradit	k5eAaPmAgMnP	zradit
<g/>
)	)	kIx)	)
a	a	k8xC	a
Harry	Harra	k1gFnPc4	Harra
také	také	k9	také
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
vlastně	vlastně	k9	vlastně
Lilly	Lill	k1gMnPc4	Lill
byla	být	k5eAaImAgFnS	být
čarodějka	čarodějka	k1gFnSc1	čarodějka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brumbála	brumbál	k1gMnSc2	brumbál
uloží	uložit	k5eAaPmIp3nS	uložit
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
do	do	k7c2	do
bílé	bílý	k2eAgFnSc2d1	bílá
hrobky	hrobka	k1gFnSc2	hrobka
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
známé	známý	k2eAgFnPc1d1	známá
osobnosti	osobnost	k1gFnPc1	osobnost
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nP	zúčastnit
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jezerní	jezerní	k2eAgMnPc1d1	jezerní
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
kentauři	kentaur	k1gMnPc1	kentaur
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
lesa	les	k1gInSc2	les
vzdají	vzdát	k5eAaPmIp3nP	vzdát
hold	hold	k1gInSc1	hold
střelbou	střelba	k1gFnSc7	střelba
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
luků	luk	k1gInPc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
se	se	k3xPyFc4	se
rozejde	rozejít	k5eAaPmIp3nS	rozejít
s	s	k7c7	s
Ginny	Ginn	k1gInPc7	Ginn
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Ginny	Ginn	k1gMnPc4	Ginn
to	ten	k3xDgNnSc1	ten
akceptuje	akceptovat	k5eAaBmIp3nS	akceptovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k6eAd1	až
bude	být	k5eAaImBp3nS	být
Voldemort	Voldemort	k1gInSc1	Voldemort
mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
<g/>
,	,	kIx,	,
zase	zase	k9	zase
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Brousek	brousek	k1gInSc1	brousek
se	se	k3xPyFc4	se
Harryho	Harry	k1gMnSc4	Harry
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ten	ten	k3xDgInSc4	ten
večer	večer	k1gInSc4	večer
s	s	k7c7	s
Brumbálem	brumbál	k1gMnSc7	brumbál
byli	být	k5eAaImAgMnP	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nemá	mít	k5eNaImIp3nS	mít
co	co	k9	co
říct	říct	k5eAaPmF	říct
a	a	k8xC	a
zopakuje	zopakovat	k5eAaPmIp3nS	zopakovat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
skrz	skrz	k6eAd1	skrz
naskrz	naskrz	k6eAd1	naskrz
Brumbálův	brumbálův	k2eAgMnSc1d1	brumbálův
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
Hermiona	Hermion	k1gMnSc2	Hermion
a	a	k8xC	a
Ron	ron	k1gInSc4	ron
vyptávají	vyptávat	k5eAaImIp3nP	vyptávat
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnPc4	Harra
jim	on	k3xPp3gMnPc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
se	se	k3xPyFc4	se
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
domu	dům	k1gInSc2	dům
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
Zobí	Zobí	k2eAgFnSc2d1	Zobí
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc4	ten
Brumbálovi	brumbál	k1gMnSc3	brumbál
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
zničí	zničit	k5eAaPmIp3nS	zničit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
viteály	viteál	k1gMnPc4	viteál
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
Voldemorta	Voldemort	k1gMnSc4	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
přísahají	přísahat	k5eAaImIp3nP	přísahat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
vždy	vždy	k6eAd1	vždy
budou	být	k5eAaImBp3nP	být
stát	stát	k5eAaPmF	stát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjdou	jít	k5eAaImIp3nP	jít
hledat	hledat	k5eAaImF	hledat
části	část	k1gFnPc4	část
Voldemortovy	Voldemortův	k2eAgFnSc2d1	Voldemortova
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Harry	Harra	k1gFnSc2	Harra
utěšuje	utěšovat	k5eAaImIp3nS	utěšovat
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
než	než	k8xS	než
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
stráví	strávit	k5eAaPmIp3nS	strávit
alespoň	alespoň	k9	alespoň
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc4	jeden
poklidný	poklidný	k2eAgInSc4d1	poklidný
den	den	k1gInSc4	den
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
Fleur	Fleura	k1gFnPc2	Fleura
a	a	k8xC	a
Billa	Bill	k1gMnSc2	Bill
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
