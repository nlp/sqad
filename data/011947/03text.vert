<p>
<s>
Obec	obec	k1gFnSc1	obec
Údrnice	Údrnice	k1gFnSc2	Údrnice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jičín	Jičín	k1gInSc1	Jičín
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
9	[number]	k4	9
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
288	[number]	k4	288
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc3	část
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Údrnice	Údrnice	k1gFnSc1	Údrnice
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Údrnice	Údrnice	k1gFnSc1	Údrnice
a	a	k8xC	a
Údrnická	Údrnický	k2eAgFnSc1d1	Údrnický
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bílsko	Bílsko	k1gNnSc1	Bílsko
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Bílsko	Bílsko	k1gNnSc4	Bílsko
u	u	k7c2	u
Kopidlna	Kopidlno	k1gNnSc2	Kopidlno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Únětice	Únětice	k1gFnSc1	Únětice
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Únětice	Únětice	k1gFnSc2	Únětice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1318	[number]	k4	1318
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
náležela	náležet	k5eAaImAgFnS	náležet
Vokovi	Voka	k1gMnSc3	Voka
z	z	k7c2	z
Rotštejna	Rotštejn	k1gInSc2	Rotštejn
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
manželce	manželka	k1gFnSc3	manželka
Rychce	Rychce	k1gFnSc2	Rychce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vedli	vést	k5eAaImAgMnP	vést
spor	spor	k1gInSc4	spor
s	s	k7c7	s
pány	pan	k1gMnPc7	pan
z	z	k7c2	z
Labouně	Labouna	k1gFnSc3	Labouna
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
protiprávní	protiprávní	k2eAgFnSc4d1	protiprávní
držbu	držba	k1gFnSc4	držba
části	část	k1gFnSc2	část
vsi	ves	k1gFnSc2	ves
<g/>
.	.	kIx.	.
</s>
<s>
Páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Labouně	Labouna	k1gFnSc3	Labouna
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spor	spor	k1gInSc4	spor
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
připomínají	připomínat	k5eAaImIp3nP	připomínat
až	až	k9	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
zdejší	zdejší	k2eAgFnSc4d1	zdejší
tvrz	tvrz	k1gFnSc4	tvrz
s	s	k7c7	s
dvorem	dvůr	k1gInSc7	dvůr
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1435	[number]	k4	1435
také	také	k9	také
hrad	hrad	k1gInSc1	hrad
Brada	brada	k1gFnSc1	brada
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1511	[number]	k4	1511
panství	panství	k1gNnSc2	panství
koupil	koupit	k5eAaPmAgMnS	koupit
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
spojil	spojit	k5eAaPmAgMnS	spojit
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Velišem	Veliš	k1gMnSc7	Veliš
<g/>
,	,	kIx,	,
tvrz	tvrz	k1gFnSc1	tvrz
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
panským	panský	k2eAgNnSc7d1	panské
sídlem	sídlo	k1gNnSc7	sídlo
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Dochoval	dochovat	k5eAaPmAgInS	dochovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
jen	jen	k9	jen
okrouhlý	okrouhlý	k2eAgInSc1d1	okrouhlý
pahorek	pahorek	k1gInSc1	pahorek
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Hrádek	hrádek	k1gInSc1	hrádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Martina	Martin	k1gMnSc2	Martin
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
fotografie	fotografia	k1gFnPc1	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Šimek	Šimek	k1gMnSc1	Šimek
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
<g/>
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
tvrze	tvrz	k1gFnPc1	tvrz
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
díl	díl	k1gInSc4	díl
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnPc1d1	východní
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
Praha	Praha	k1gFnSc1	Praha
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
s.	s.	k?	s.
509	[number]	k4	509
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Údrnice	Údrnice	k1gFnSc2	Údrnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Údrnice	Údrnice	k1gFnSc2	Údrnice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
