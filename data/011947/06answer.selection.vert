<s>
Obec	obec	k1gFnSc1	obec
Údrnice	Údrnice	k1gFnSc2	Údrnice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jičín	Jičín	k1gInSc1	Jičín
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
9	[number]	k4	9
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Jičína	Jičín	k1gInSc2	Jičín
<g/>
.	.	kIx.	.
</s>
