<p>
<s>
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Brandenburger	Brandenburger	k1gMnSc1	Brandenburger
Tor	Tor	k1gMnSc1	Tor
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
pamětihodnostem	pamětihodnost	k1gFnPc3	pamětihodnost
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
významným	významný	k2eAgInPc3d1	významný
symbolům	symbol	k1gInPc3	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
dvou	dva	k4xCgFnPc2	dva
známých	známý	k2eAgFnPc2d1	známá
a	a	k8xC	a
velkých	velký	k2eAgFnPc2d1	velká
tříd	třída	k1gFnPc2	třída
<g/>
:	:	kIx,	:
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
počíná	počínat	k5eAaImIp3nS	počínat
Straße	Straße	k1gInSc1	Straße
des	des	k1gNnSc1	des
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Juni	Juni	k1gNnSc1	Juni
(	(	kIx(	(
<g/>
Ulice	ulice	k1gFnSc1	ulice
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
brány	brána	k1gFnSc2	brána
pak	pak	k6eAd1	pak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
historická	historický	k2eAgFnSc1d1	historická
třída	třída	k1gFnSc1	třída
Unter	Unter	k1gInSc4	Unter
den	den	k1gInSc1	den
Linden	Lindna	k1gFnPc2	Lindna
(	(	kIx(	(
<g/>
Pod	pod	k7c7	pod
lípami	lípa	k1gFnPc7	lípa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
stavební	stavební	k2eAgFnSc7d1	stavební
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
mnoho	mnoho	k4c1	mnoho
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
26	[number]	k4	26
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
65,5	[number]	k4	65,5
metrů	metr	k1gInPc2	metr
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
(	(	kIx(	(
<g/>
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
)	)	kIx)	)
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
pískovce	pískovec	k1gInSc2	pískovec
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
raného	raný	k2eAgInSc2d1	raný
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
silně	silně	k6eAd1	silně
připomíná	připomínat	k5eAaImIp3nS	připomínat
propylaje	propylat	k5eAaImSgInS	propylat
athénské	athénský	k2eAgFnPc1d1	Athénská
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc1	pět
průjezdů	průjezd	k1gInPc2	průjezd
<g/>
,	,	kIx,	,
střední	střední	k2eAgMnPc4d1	střední
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
širší	široký	k2eAgFnSc4d2	širší
nežli	nežli	k8xS	nežli
čtyři	čtyři	k4xCgInPc1	čtyři
postranní	postranní	k2eAgInPc1d1	postranní
<g/>
,	,	kIx,	,
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
malé	malý	k2eAgFnPc4d1	malá
haly	hala	k1gFnPc4	hala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bránu	brána	k1gFnSc4	brána
tvoří	tvořit	k5eAaImIp3nS	tvořit
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
vysoké	vysoký	k2eAgInPc1d1	vysoký
sloupy	sloup	k1gInPc1	sloup
v	v	k7c6	v
jónsko-dórském	jónskoórský	k2eAgInSc6d1	jónsko-dórský
stylu	styl	k1gInSc6	styl
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
1,75	[number]	k4	1,75
metrů	metr	k1gInPc2	metr
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
zdobena	zdoben	k2eAgFnSc1d1	zdobena
ornamenty	ornament	k1gInPc7	ornament
a	a	k8xC	a
reliéfy	reliéf	k1gInPc7	reliéf
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
různé	různý	k2eAgInPc4d1	různý
výjevy	výjev	k1gInPc4	výjev
ze	z	k7c2	z
života	život	k1gInSc2	život
Herkulova	Herkulův	k2eAgInSc2d1	Herkulův
<g/>
,	,	kIx,	,
boha	bůh	k1gMnSc2	bůh
Marta	Marta	k1gFnSc1	Marta
a	a	k8xC	a
bohyně	bohyně	k1gFnSc1	bohyně
Minervy	Minerva	k1gFnSc2	Minerva
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
korunována	korunován	k2eAgFnSc1d1	korunována
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
vysokou	vysoký	k2eAgFnSc7d1	vysoká
sochou	socha	k1gFnSc7	socha
kvadrigy	kvadriga	k1gFnSc2	kvadriga
(	(	kIx(	(
<g/>
čtyřspřeží	čtyřspřeží	k1gNnSc4	čtyřspřeží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnSc4d1	představující
okřídlenou	okřídlený	k2eAgFnSc4d1	okřídlená
bohyni	bohyně	k1gFnSc4	bohyně
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
vjíždí	vjíždět	k5eAaImIp3nP	vjíždět
do	do	k7c2	do
města	město	k1gNnSc2	město
na	na	k7c4	na
čtyřmi	čtyři	k4xCgNnPc7	čtyři
koňmi	kůň	k1gMnPc7	kůň
taženém	tažený	k2eAgInSc6d1	tažený
voze	vůz	k1gInSc6	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
roku	rok	k1gInSc2	rok
1734	[number]	k4	1734
jako	jako	k8xC	jako
celní	celní	k2eAgFnSc1d1	celní
brána	brána	k1gFnSc1	brána
v	v	k7c6	v
opevnění	opevnění	k1gNnSc6	opevnění
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1788	[number]	k4	1788
a	a	k8xC	a
1791	[number]	k4	1791
ji	on	k3xPp3gFnSc4	on
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
nechal	nechat	k5eAaPmAgMnS	nechat
přestavět	přestavět	k5eAaPmF	přestavět
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
stylu	styl	k1gInSc6	styl
raného	raný	k2eAgInSc2d1	raný
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc2	práce
provedl	provést	k5eAaPmAgMnS	provést
architekt	architekt	k1gMnSc1	architekt
Carl	Carl	k1gMnSc1	Carl
Gotthard	Gotthard	k1gMnSc1	Gotthard
Langhans	Langhans	k1gInSc4	Langhans
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
dostala	dostat	k5eAaPmAgFnS	dostat
brána	brána	k1gFnSc1	brána
kvadrigu	kvadriga	k1gFnSc4	kvadriga
<g/>
,	,	kIx,	,
zhotovenou	zhotovený	k2eAgFnSc4d1	zhotovená
Johannem	Johann	k1gInSc7	Johann
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Schadowem	Schadow	k1gMnSc7	Schadow
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
socha	socha	k1gFnSc1	socha
představovala	představovat	k5eAaImAgFnS	představovat
okřídlenou	okřídlený	k2eAgFnSc4d1	okřídlená
bohyni	bohyně	k1gFnSc4	bohyně
míru	mír	k1gInSc2	mír
Eirene	Eiren	k1gInSc5	Eiren
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
byla	být	k5eAaImAgFnS	být
Napoleonem	napoleon	k1gInSc7	napoleon
dopravena	dopraven	k2eAgFnSc1d1	dopravena
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
byla	být	k5eAaImAgFnS	být
převezena	převézt	k5eAaPmNgFnS	převézt
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
restauraci	restaurace	k1gFnSc6	restaurace
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
věnec	věnec	k1gInSc1	věnec
z	z	k7c2	z
dubového	dubový	k2eAgNnSc2d1	dubové
listí	listí	k1gNnSc2	listí
nahrazen	nahradit	k5eAaPmNgInS	nahradit
novým	nový	k2eAgInSc7d1	nový
symbolem	symbol	k1gInSc7	symbol
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
železným	železný	k2eAgInSc7d1	železný
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
Schadowova	Schadowův	k2eAgFnSc1d1	Schadowův
bohyně	bohyně	k1gFnSc1	bohyně
míru	mír	k1gInSc2	mír
Eirene	Eiren	k1gInSc5	Eiren
nově	nově	k6eAd1	nově
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
bohyně	bohyně	k1gFnSc1	bohyně
vítězství	vítězství	k1gNnSc2	vítězství
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
směl	smět	k5eAaImAgMnS	smět
Braniborskou	braniborský	k2eAgFnSc7d1	Braniborská
bránou	brána	k1gFnSc7	brána
projíždět	projíždět	k5eAaImF	projíždět
pouze	pouze	k6eAd1	pouze
německý	německý	k2eAgMnSc1d1	německý
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
byla	být	k5eAaImAgFnS	být
brána	brána	k1gFnSc1	brána
svědkem	svědek	k1gMnSc7	svědek
velkého	velký	k2eAgInSc2d1	velký
průvodu	průvod	k1gInSc2	průvod
nacistické	nacistický	k2eAgFnSc2d1	nacistická
NSDAP	NSDAP	kA	NSDAP
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slavila	slavit	k5eAaImAgFnS	slavit
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
sovětské	sovětský	k2eAgFnPc1d1	sovětská
jednotky	jednotka	k1gFnPc1	jednotka
vyvěsily	vyvěsit	k5eAaPmAgFnP	vyvěsit
rudou	rudý	k2eAgFnSc4d1	rudá
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
brána	brána	k1gFnSc1	brána
silně	silně	k6eAd1	silně
poškozena	poškodit	k5eAaPmNgFnS	poškodit
dělostřelbou	dělostřelba	k1gFnSc7	dělostřelba
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
již	již	k6eAd1	již
ovšem	ovšem	k9	ovšem
existoval	existovat	k5eAaImAgInS	existovat
sádrový	sádrový	k2eAgInSc1d1	sádrový
obtisk	obtisk	k1gInSc1	obtisk
sochy	socha	k1gFnSc2	socha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
poprvé	poprvé	k6eAd1	poprvé
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
(	(	kIx(	(
<g/>
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
NDR	NDR	kA	NDR
a	a	k8xC	a
SRN	SRN	kA	SRN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byl	být	k5eAaImAgInS	být
železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
a	a	k8xC	a
orel	orel	k1gMnSc1	orel
odstraněn	odstraněn	k2eAgMnSc1d1	odstraněn
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
symboly	symbol	k1gInPc4	symbol
pruského	pruský	k2eAgInSc2d1	pruský
militarismu	militarismus	k1gInSc2	militarismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
existence	existence	k1gFnSc2	existence
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
se	se	k3xPyFc4	se
brána	brána	k1gFnSc1	brána
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
hraničním	hraniční	k2eAgInSc6d1	hraniční
pásu	pás	k1gInSc6	pás
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
vešel	vejít	k5eAaPmAgInS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
proslov	proslov	k1gInSc1	proslov
presidenta	president	k1gMnSc2	president
USA	USA	kA	USA
Reagana	Reagan	k1gMnSc2	Reagan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
1987	[number]	k4	1987
pronesl	pronést	k5eAaPmAgMnS	pronést
známou	známý	k2eAgFnSc4d1	známá
výzvu	výzva	k1gFnSc4	výzva
<g/>
,	,	kIx,	,
adresovanou	adresovaný	k2eAgFnSc4d1	adresovaná
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
prvnímu	první	k4xOgNnSc3	první
tajemníkovi	tajemník	k1gMnSc3	tajemník
KSSS	KSSS	kA	KSSS
Gorbačovovi	Gorbačova	k1gMnSc3	Gorbačova
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Gorbachev	Gorbachev	k1gFnSc1	Gorbachev
<g/>
,	,	kIx,	,
open	open	k1gInSc1	open
this	this	k6eAd1	this
gate	gatat	k5eAaPmIp3nS	gatat
<g/>
!	!	kIx.	!
</s>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Gorbachev	Gorbachev	k1gFnSc1	Gorbachev
<g/>
,	,	kIx,	,
tear	tear	k1gMnSc1	tear
down	down	k1gMnSc1	down
this	this	k6eAd1	this
wall	walnout	k5eAaPmAgMnS	walnout
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
Gorbačove	Gorbačov	k1gInSc5	Gorbačov
<g/>
:	:	kIx,	:
otevřte	otevřit	k5eAaImRp2nP	otevřit
tuto	tento	k3xDgFnSc4	tento
bránu	brána	k1gFnSc4	brána
<g/>
!	!	kIx.	!
</s>
<s>
Pane	Pan	k1gMnSc5	Pan
Gorbačove	Gorbačov	k1gInSc5	Gorbačov
<g/>
:	:	kIx,	:
strhněte	strhnout	k5eAaPmRp2nP	strhnout
tuto	tento	k3xDgFnSc4	tento
zeď	zeď	k1gFnSc4	zeď
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Brána	brána	k1gFnSc1	brána
dnes	dnes	k6eAd1	dnes
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
byla	být	k5eAaImAgFnS	být
jak	jak	k6eAd1	jak
socha	socha	k1gFnSc1	socha
kvadrigy	kvadriga	k1gFnSc2	kvadriga
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
brána	brána	k1gFnSc1	brána
samotná	samotný	k2eAgFnSc1d1	samotná
znovu	znovu	k6eAd1	znovu
restaurována	restaurován	k2eAgFnSc1d1	restaurována
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
zdobí	zdobit	k5eAaImIp3nS	zdobit
–	–	k?	–
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
rozdělení	rozdělení	k1gNnSc2	rozdělení
a	a	k8xC	a
sjednocení	sjednocení	k1gNnSc2	sjednocení
města	město	k1gNnSc2	město
i	i	k8xC	i
země	zem	k1gFnSc2	zem
–	–	k?	–
spodní	spodní	k2eAgFnSc4d1	spodní
stranu	strana	k1gFnSc4	strana
německého	německý	k2eAgNnSc2d1	německé
vydání	vydání	k1gNnSc2	vydání
mincí	mince	k1gFnPc2	mince
eura	euro	k1gNnSc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
během	během	k7c2	během
silvestrovské	silvestrovský	k2eAgFnSc2d1	silvestrovská
noci	noc	k1gFnSc2	noc
konají	konat	k5eAaImIp3nP	konat
velká	velký	k2eAgNnPc4d1	velké
oslavná	oslavný	k2eAgNnPc4d1	oslavné
shromáždění	shromáždění	k1gNnPc4	shromáždění
s	s	k7c7	s
ohňostrojem	ohňostroj	k1gInSc7	ohňostroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Braniborská	braniborský	k2eAgFnSc1d1	Braniborská
brána	brána	k1gFnSc1	brána
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
