<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
orgánů	orgán	k1gInPc2	orgán
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Radou	rada	k1gFnSc7	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
přijímá	přijímat	k5eAaImIp3nS	přijímat
její	její	k3xOp3gInPc4	její
legislativní	legislativní	k2eAgInPc4d1	legislativní
akty	akt	k1gInPc4	akt
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
občany	občan	k1gMnPc4	občan
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
parlamentu	parlament	k1gInSc2	parlament
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
každých	každý	k3xTgNnPc2	každý
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
přímé	přímý	k2eAgFnPc1d1	přímá
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
voleni	volit	k5eAaImNgMnP	volit
podle	podle	k7c2	podle
zásad	zásada	k1gFnPc2	zásada
poměrného	poměrný	k2eAgNnSc2d1	poměrné
zastoupení	zastoupení	k1gNnSc2	zastoupení
v	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
volbě	volba	k1gFnSc6	volba
všemi	všecek	k3xTgFnPc7	všecek
občany	občan	k1gMnPc4	občan
EU	EU	kA	EU
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
volební	volební	k2eAgInPc1d1	volební
systémy	systém	k1gInPc1	systém
se	se	k3xPyFc4	se
však	však	k9	však
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
<g/>
,	,	kIx,	,
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
jsou	být	k5eAaImIp3nP	být
volby	volba	k1gFnPc1	volba
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
povinné	povinný	k2eAgNnSc1d1	povinné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
751	[number]	k4	751
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
21	[number]	k4	21
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
poslanců	poslanec	k1gMnPc2	poslanec
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
určuje	určovat	k5eAaImIp3nS	určovat
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
parlamentu	parlament	k1gInSc2	parlament
jednomyslným	jednomyslný	k2eAgNnPc3d1	jednomyslné
rozhodnutím	rozhodnutí	k1gNnPc3	rozhodnutí
Evropská	evropský	k2eAgFnSc1d1	Evropská
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
14	[number]	k4	14
SEU	SEU	kA	SEU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volit	volit	k5eAaImF	volit
mohou	moct	k5eAaImIp3nP	moct
všichni	všechen	k3xTgMnPc1	všechen
občané	občan	k1gMnPc1	občan
Unie	unie	k1gFnSc2	unie
na	na	k7c4	na
území	území	k1gNnSc4	území
daného	daný	k2eAgInSc2d1	daný
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
EP	EP	kA	EP
je	být	k5eAaImIp3nS	být
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
(	(	kIx(	(
<g/>
plenární	plenární	k2eAgNnSc1d1	plenární
zasedání	zasedání	k1gNnSc1	zasedání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
parlament	parlament	k1gInSc1	parlament
pracuje	pracovat	k5eAaImIp3nS	pracovat
také	také	k9	také
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
(	(	kIx(	(
<g/>
výbory	výbor	k1gInPc1	výbor
<g/>
,	,	kIx,	,
schůze	schůze	k1gFnSc1	schůze
politických	politický	k2eAgFnPc2d1	politická
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lucemburku	Lucemburk	k1gInSc2	Lucemburk
(	(	kIx(	(
<g/>
sekretariát	sekretariát	k1gInSc1	sekretariát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc4d1	základní
pravomoce	pravomoc	k1gFnPc4	pravomoc
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
jsou	být	k5eAaImIp3nP	být
legislativní	legislativní	k2eAgFnSc1d1	legislativní
<g/>
,	,	kIx,	,
rozpočtová	rozpočtový	k2eAgFnSc1d1	rozpočtová
a	a	k8xC	a
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
politická	politický	k2eAgFnSc1d1	politická
role	role	k1gFnSc1	role
Parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
EU	EU	kA	EU
postupně	postupně	k6eAd1	postupně
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legislativní	legislativní	k2eAgFnSc4d1	legislativní
pravomoc	pravomoc	k1gFnSc4	pravomoc
==	==	k?	==
</s>
</p>
<p>
<s>
Maastrichtská	maastrichtský	k2eAgFnSc1d1	Maastrichtská
smlouva	smlouva	k1gFnSc1	smlouva
umožnila	umožnit	k5eAaPmAgFnS	umožnit
parlamentu	parlament	k1gInSc3	parlament
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
procedurou	procedura	k1gFnSc7	procedura
spolurozhodování	spolurozhodování	k1gNnSc2	spolurozhodování
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
legislativním	legislativní	k2eAgInSc6d1	legislativní
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgInPc4d1	zastoupen
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
však	však	k9	však
uchovala	uchovat	k5eAaPmAgFnS	uchovat
výrazně	výrazně	k6eAd1	výrazně
silnější	silný	k2eAgFnSc4d2	silnější
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Lisabonské	lisabonský	k2eAgFnSc6d1	Lisabonská
smlouvě	smlouva	k1gFnSc6	smlouva
je	být	k5eAaImIp3nS	být
postavení	postavení	k1gNnSc4	postavení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
legislativním	legislativní	k2eAgInSc6d1	legislativní
procesu	proces	k1gInSc6	proces
rovnocenné	rovnocenný	k2eAgInPc1d1	rovnocenný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
evropské	evropský	k2eAgFnSc2d1	Evropská
legislativy	legislativa	k1gFnSc2	legislativa
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
zavádí	zavádět	k5eAaImIp3nS	zavádět
tzv.	tzv.	kA	tzv.
řádný	řádný	k2eAgInSc1d1	řádný
legislativní	legislativní	k2eAgInSc1d1	legislativní
proces	proces	k1gInSc1	proces
dle	dle	k7c2	dle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
294	[number]	k4	294
Smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
přijímání	přijímání	k1gNnSc4	přijímání
textů	text	k1gInPc2	text
legislativního	legislativní	k2eAgInSc2d1	legislativní
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
charakteru	charakter	k1gInSc2	charakter
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
užívá	užívat	k5eAaImIp3nS	užívat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gInPc4	jeho
národní	národní	k2eAgInPc4d1	národní
protějšky	protějšek	k1gInPc4	protějšek
<g/>
,	,	kIx,	,
systém	systém	k1gInSc4	systém
výborů	výbor	k1gInPc2	výbor
a	a	k8xC	a
zpravodajů	zpravodaj	k1gInPc2	zpravodaj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
texty	text	k1gInPc4	text
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
návrhy	návrh	k1gInPc1	návrh
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
či	či	k8xC	či
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
rezoluce	rezoluce	k1gFnSc2	rezoluce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
(	(	kIx(	(
<g/>
vybraný	vybraný	k2eAgMnSc1d1	vybraný
poslanec	poslanec	k1gMnSc1	poslanec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
příslušného	příslušný	k2eAgInSc2d1	příslušný
výboru	výbor	k1gInSc2	výbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
poslanec	poslanec	k1gMnSc1	poslanec
vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
sekretariátem	sekretariát	k1gInSc7	sekretariát
<g/>
,	,	kIx,	,
pozici	pozice	k1gFnSc4	pozice
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
otázce	otázka	k1gFnSc3	otázka
(	(	kIx(	(
<g/>
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
k	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
Komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodaj	zpravodaj	k1gInSc1	zpravodaj
pak	pak	k6eAd1	pak
přijímá	přijímat	k5eAaImIp3nS	přijímat
a	a	k8xC	a
zapracovává	zapracovávat	k5eAaImIp3nS	zapracovávat
pozměňovací	pozměňovací	k2eAgInPc4d1	pozměňovací
návrhy	návrh	k1gInPc4	návrh
ostatních	ostatní	k2eAgMnPc2d1	ostatní
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnSc1d1	ostatní
frakce	frakce	k1gFnSc1	frakce
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
každá	každý	k3xTgFnSc1	každý
svého	svůj	k3xOyFgInSc2	svůj
"	"	kIx"	"
<g/>
stínového	stínový	k2eAgInSc2d1	stínový
zpravodaje	zpravodaj	k1gInSc2	zpravodaj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
se	s	k7c7	s
zpravodajem	zpravodaj	k1gInSc7	zpravodaj
<g/>
,	,	kIx,	,
formuluje	formulovat	k5eAaImIp3nS	formulovat
mu	on	k3xPp3gMnSc3	on
pozici	pozice	k1gFnSc6	pozice
své	svůj	k3xOyFgFnSc2	svůj
politické	politický	k2eAgFnSc2d1	politická
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
skupinu	skupina	k1gFnSc4	skupina
návod	návod	k1gInSc1	návod
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
o	o	k7c6	o
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
pozměňovacích	pozměňovací	k2eAgInPc6d1	pozměňovací
návrzích	návrh	k1gInPc6	návrh
a	a	k8xC	a
o	o	k7c6	o
textu	text	k1gInSc6	text
samotném	samotný	k2eAgInSc6d1	samotný
mají	mít	k5eAaImIp3nP	mít
jeho	jeho	k3xOp3gNnSc4	jeho
kolegové	kolega	k1gMnPc1	kolega
a	a	k8xC	a
kolegyně	kolegyně	k1gFnPc1	kolegyně
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
text	text	k1gInSc4	text
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
podobě	podoba	k1gFnSc6	podoba
schválen	schválit	k5eAaPmNgInS	schválit
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
na	na	k7c4	na
plénum	plénum	k1gNnSc4	plénum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozpočtová	rozpočtový	k2eAgFnSc1d1	rozpočtová
pravomoc	pravomoc	k1gFnSc1	pravomoc
==	==	k?	==
</s>
</p>
<p>
<s>
Parlament	parlament	k1gInSc1	parlament
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
rozpočtu	rozpočet	k1gInSc6	rozpočet
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
společně	společně	k6eAd1	společně
s	s	k7c7	s
Radou	rada	k1gFnSc7	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
nevstoupí	vstoupit	k5eNaPmIp3nS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ho	on	k3xPp3gMnSc4	on
nepodepíše	podepsat	k5eNaPmIp3nS	podepsat
předseda	předseda	k1gMnSc1	předseda
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
rozpočtových	rozpočtový	k2eAgFnPc6d1	rozpočtová
položkách	položka	k1gFnPc6	položka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
výdaje	výdaj	k1gInPc4	výdaj
může	moct	k5eAaImIp3nS	moct
pouze	pouze	k6eAd1	pouze
doplnit	doplnit	k5eAaPmF	doplnit
<g/>
;	;	kIx,	;
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
položky	položka	k1gFnSc2	položka
má	mít	k5eAaImIp3nS	mít
Rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
může	moct	k5eAaImIp3nS	moct
rozpočet	rozpočet	k1gInSc4	rozpočet
zamítnout	zamítnout	k5eAaPmF	zamítnout
<g/>
;	;	kIx,	;
jednání	jednání	k1gNnPc4	jednání
o	o	k7c6	o
rozpočtu	rozpočet	k1gInSc6	rozpočet
pak	pak	k6eAd1	pak
začíná	začínat	k5eAaImIp3nS	začínat
nanovo	nanovo	k6eAd1	nanovo
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
rozpočet	rozpočet	k1gInSc1	rozpočet
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
tuto	tento	k3xDgFnSc4	tento
silnou	silný	k2eAgFnSc4d1	silná
pravomoc	pravomoc	k1gFnSc4	pravomoc
nepoužil	použít	k5eNaPmAgMnS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
také	také	k9	také
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
rozpočtové	rozpočtový	k2eAgInPc1d1	rozpočtový
výdaje	výdaj	k1gInPc1	výdaj
využity	využít	k5eAaPmNgInP	využít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dozorčí	dozorčí	k2eAgFnSc4d1	dozorčí
pravomoc	pravomoc	k1gFnSc4	pravomoc
==	==	k?	==
</s>
</p>
<p>
<s>
Parlament	parlament	k1gInSc1	parlament
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
aktivity	aktivita	k1gFnPc4	aktivita
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pravomoc	pravomoc	k1gFnSc1	pravomoc
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
aktivity	aktivita	k1gFnPc4	aktivita
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
i	i	k9	i
na	na	k7c4	na
Radu	rada	k1gFnSc4	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
orgány	orgán	k1gInPc1	orgán
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
Společnou	společný	k2eAgFnSc4d1	společná
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
hraje	hrát	k5eAaImIp3nS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
jmenování	jmenování	k1gNnSc6	jmenování
Komise	komise	k1gFnSc2	komise
<g/>
:	:	kIx,	:
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
o	o	k7c6	o
předsedovi	předseda	k1gMnSc6	předseda
Komise	komise	k1gFnSc2	komise
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
slyšení	slyšení	k1gNnSc1	slyšení
o	o	k7c6	o
každém	každý	k3xTgMnSc6	každý
navrženém	navržený	k2eAgMnSc6d1	navržený
komisaři	komisař	k1gMnSc6	komisař
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c4	o
jmenování	jmenování	k1gNnSc4	jmenování
Komise	komise	k1gFnSc2	komise
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
v	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
EP	EP	kA	EP
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
členové	člen	k1gMnPc1	člen
mohou	moct	k5eAaImIp3nP	moct
komisi	komise	k1gFnSc4	komise
interpelovat	interpelovat	k5eAaBmF	interpelovat
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
právo	právo	k1gNnSc4	právo
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
Komisi	komise	k1gFnSc3	komise
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
a	a	k8xC	a
tak	tak	k6eAd1	tak
ji	on	k3xPp3gFnSc4	on
přinutit	přinutit	k5eAaPmF	přinutit
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
pravomoc	pravomoc	k1gFnSc4	pravomoc
ovšem	ovšem	k9	ovšem
Parlament	parlament	k1gInSc4	parlament
zatím	zatím	k6eAd1	zatím
nikdy	nikdy	k6eAd1	nikdy
nevyužil	využít	k5eNaPmAgMnS	využít
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
již	již	k6eAd1	již
schylovalo	schylovat	k5eAaImAgNnS	schylovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Santerova	Santerův	k2eAgFnSc1d1	Santerova
komise	komise	k1gFnSc1	komise
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
hovoří	hovořit	k5eAaImIp3nS	hovořit
i	i	k9	i
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
politická	politický	k2eAgFnSc1d1	politická
orientace	orientace	k1gFnSc1	orientace
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
měla	mít	k5eAaImAgFnS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
většině	většina	k1gFnSc3	většina
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
vzešlé	vzešlý	k2eAgFnPc1d1	vzešlá
z	z	k7c2	z
voleb	volba	k1gFnPc2	volba
(	(	kIx(	(
<g/>
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
obou	dva	k4xCgFnPc2	dva
orgánů	orgán	k1gInPc2	orgán
jsou	být	k5eAaImIp3nP	být
sladěna	sladěn	k2eAgNnPc1d1	sladěno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
Unie	unie	k1gFnSc2	unie
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c4	na
Evropský	evropský	k2eAgInSc4d1	evropský
parlament	parlament	k1gInSc4	parlament
obracet	obracet	k5eAaImF	obracet
s	s	k7c7	s
peticemi	petice	k1gFnPc7	petice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
stížnosti	stížnost	k1gFnPc4	stížnost
proti	proti	k7c3	proti
činnosti	činnost	k1gFnSc3	činnost
orgánů	orgán	k1gInPc2	orgán
EU	EU	kA	EU
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
obrátit	obrátit	k5eAaPmF	obrátit
na	na	k7c4	na
Evropského	evropský	k2eAgMnSc2d1	evropský
veřejného	veřejný	k2eAgMnSc2d1	veřejný
ochránce	ochránce	k1gMnSc2	ochránce
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
podávat	podávat	k5eAaImF	podávat
žaloby	žaloba	k1gFnSc2	žaloba
k	k	k7c3	k
Evropskému	evropský	k2eAgInSc3d1	evropský
soudnímu	soudní	k2eAgInSc3d1	soudní
dvoru	dvůr	k1gInSc3	dvůr
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
meziinstitucionálních	meziinstitucionální	k2eAgInPc2d1	meziinstitucionální
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
role	role	k1gFnSc1	role
==	==	k?	==
</s>
</p>
<p>
<s>
Politická	politický	k2eAgFnSc1d1	politická
role	role	k1gFnSc1	role
Parlamentu	parlament	k1gInSc2	parlament
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
konzultuje	konzultovat	k5eAaImIp3nS	konzultovat
s	s	k7c7	s
Parlamentem	parlament	k1gInSc7	parlament
svá	svůj	k3xOyFgNnPc4	svůj
důležitá	důležitý	k2eAgNnPc4d1	důležité
zahraničněpolitická	zahraničněpolitický	k2eAgNnPc4d1	zahraničněpolitické
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
vznáší	vznášet	k5eAaImIp3nS	vznášet
na	na	k7c4	na
Radu	rada	k1gFnSc4	rada
dotazy	dotaz	k1gInPc4	dotaz
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vydávat	vydávat	k5eAaPmF	vydávat
doporučení	doporučení	k1gNnSc4	doporučení
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
události	událost	k1gFnPc1	událost
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
rozprav	rozprava	k1gFnPc2	rozprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zakončeny	zakončit	k5eAaPmNgFnP	zakončit
přijetím	přijetí	k1gNnSc7	přijetí
usnesení	usnesení	k1gNnSc2	usnesení
požadujících	požadující	k2eAgInPc2d1	požadující
určitý	určitý	k2eAgInSc4d1	určitý
zahraničněpolitický	zahraničněpolitický	k2eAgInSc4d1	zahraničněpolitický
přístup	přístup	k1gInSc4	přístup
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc4	přijetí
každého	každý	k3xTgMnSc2	každý
nového	nový	k2eAgMnSc2d1	nový
člena	člen	k1gMnSc2	člen
EU	EU	kA	EU
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
odsouhlasena	odsouhlasit	k5eAaPmNgFnS	odsouhlasit
Parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
značnou	značný	k2eAgFnSc4d1	značná
důležitost	důležitost	k1gFnSc4	důležitost
ochraně	ochrana	k1gFnSc3	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
jak	jak	k8xC	jak
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vně	vně	k7c2	vně
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
svoji	svůj	k3xOyFgFnSc4	svůj
pravomoc	pravomoc	k1gFnSc4	pravomoc
souhlasu	souhlas	k1gInSc2	souhlas
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
podpory	podpora	k1gFnSc2	podpora
vážnosti	vážnost	k1gFnSc6	vážnost
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
pravomoc	pravomoc	k1gFnSc4	pravomoc
Parlament	parlament	k1gInSc1	parlament
použil	použít	k5eAaPmAgInS	použít
k	k	k7c3	k
zamítnutí	zamítnutí	k1gNnSc3	zamítnutí
několika	několik	k4yIc2	několik
finančních	finanční	k2eAgInPc2d1	finanční
protokolů	protokol	k1gInPc2	protokol
s	s	k7c7	s
jistými	jistý	k2eAgFnPc7d1	jistá
nečlenskými	členský	k2eNgFnPc7d1	nečlenská
zeměmi	zem	k1gFnPc7	zem
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nedodržování	nedodržování	k1gNnSc2	nedodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
tyto	tento	k3xDgFnPc4	tento
země	zem	k1gFnPc4	zem
přinutil	přinutit	k5eAaPmAgMnS	přinutit
propustit	propustit	k5eAaPmF	propustit
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
anebo	anebo	k8xC	anebo
přijmout	přijmout	k5eAaPmF	přijmout
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
zásady	zásada	k1gFnPc4	zásada
ochrany	ochrana	k1gFnSc2	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
Parlament	parlament	k1gInSc1	parlament
založil	založit	k5eAaPmAgInS	založit
Sacharovovu	Sacharovův	k2eAgFnSc4d1	Sacharovova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
uděluje	udělovat	k5eAaImIp3nS	udělovat
jednomu	jeden	k4xCgNnSc3	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
nebo	nebo	k8xC	nebo
skupině	skupina	k1gFnSc3	skupina
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
cena	cena	k1gFnSc1	cena
udělena	udělen	k2eAgFnSc1d1	udělena
Alexandru	Alexandr	k1gMnSc3	Alexandr
Dubčekovi	Dubčeek	k1gMnSc3	Dubčeek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
občanem	občan	k1gMnSc7	občan
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Občanství	občanství	k1gNnSc1	občanství
EU	EU	kA	EU
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
<g/>
,	,	kIx,	,
národní	národní	k2eAgNnSc4d1	národní
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
EU	EU	kA	EU
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
podávat	podávat	k5eAaImF	podávat
petice	petice	k1gFnSc2	petice
k	k	k7c3	k
Evropskému	evropský	k2eAgInSc3d1	evropský
parlamentu	parlament	k1gInSc3	parlament
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
předkládat	předkládat	k5eAaImF	předkládat
své	svůj	k3xOyFgFnPc4	svůj
stížnosti	stížnost	k1gFnPc4	stížnost
Evropskému	evropský	k2eAgMnSc3d1	evropský
veřejnému	veřejný	k2eAgMnSc3d1	veřejný
ochránci	ochránce	k1gMnSc3	ochránce
práv	práv	k2eAgInSc4d1	práv
(	(	kIx(	(
<g/>
ombudsmanovi	ombudsman	k1gMnSc6	ombudsman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
oslovit	oslovit	k5eAaPmF	oslovit
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
orgán	orgán	k1gInSc4	orgán
nebo	nebo	k8xC	nebo
instituci	instituce	k1gFnSc4	instituce
EU	EU	kA	EU
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
Unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
jazyky	jazyk	k1gInPc7	jazyk
všech	všecek	k3xTgInPc2	všecek
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
EU	EU	kA	EU
<g/>
)	)	kIx)	)
a	a	k8xC	a
obdržet	obdržet	k5eAaPmF	obdržet
odpověď	odpověď	k1gFnSc4	odpověď
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Parlamentní	parlamentní	k2eAgNnSc1d1	parlamentní
shromáždění	shromáždění	k1gNnSc1	shromáždění
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
již	již	k9	již
v	v	k7c6	v
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
smlouvě	smlouva	k1gFnSc6	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
ustavující	ustavující	k2eAgFnSc4d1	ustavující
Evropské	evropský	k2eAgNnSc1d1	Evropské
společenství	společenství	k1gNnSc1	společenství
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Evropského	evropský	k2eAgNnSc2d1	Evropské
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
a	a	k8xC	a
Euratomu	Euratom	k1gInSc2	Euratom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
se	s	k7c7	s
pravomoce	pravomoc	k1gFnSc2	pravomoc
tohoto	tento	k3xDgNnSc2	tento
Shromáždění	shromáždění	k1gNnSc2	shromáždění
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
i	i	k9	i
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
působnosti	působnost	k1gFnSc2	působnost
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
společenství	společenství	k1gNnPc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
bylo	být	k5eAaImAgNnS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
zástupci	zástupce	k1gMnPc7	zástupce
národních	národní	k2eAgInPc2d1	národní
parlamentů	parlament	k1gInPc2	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
smluv	smlouva	k1gFnPc2	smlouva
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
byli	být	k5eAaImAgMnP	být
poslanci	poslanec	k1gMnPc1	poslanec
voleni	volit	k5eAaImNgMnP	volit
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
občany	občan	k1gMnPc7	občan
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
přímých	přímý	k2eAgFnPc6d1	přímá
volbách	volba	k1gFnPc6	volba
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
518	[number]	k4	518
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotný	jednotný	k2eAgInSc1d1	jednotný
evropský	evropský	k2eAgInSc1d1	evropský
akt	akt	k1gInSc1	akt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
oficiálně	oficiálně	k6eAd1	oficiálně
změnil	změnit	k5eAaPmAgInS	změnit
název	název	k1gInSc1	název
Shromáždění	shromáždění	k1gNnSc2	shromáždění
na	na	k7c4	na
Evropský	evropský	k2eAgInSc4d1	evropský
parlament	parlament	k1gInSc4	parlament
a	a	k8xC	a
přidělil	přidělit	k5eAaPmAgMnS	přidělit
mu	on	k3xPp3gInSc3	on
nové	nový	k2eAgFnPc4d1	nová
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
fakticky	fakticky	k6eAd1	fakticky
používán	používat	k5eAaImNgInS	používat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přistoupení	přistoupení	k1gNnSc1	přistoupení
nových	nový	k2eAgInPc2d1	nový
členů	člen	k1gInPc2	člen
a	a	k8xC	a
pozdější	pozdní	k2eAgNnSc1d2	pozdější
sjednocení	sjednocení	k1gNnSc1	sjednocení
Německa	Německo	k1gNnSc2	Německo
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
počet	počet	k1gInSc4	počet
členů	člen	k1gMnPc2	člen
na	na	k7c4	na
626	[number]	k4	626
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počet	počet	k1gInSc1	počet
poslanců	poslanec	k1gMnPc2	poslanec
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
navržený	navržený	k2eAgInSc1d1	navržený
počet	počet	k1gInSc1	počet
poslanců	poslanec	k1gMnPc2	poslanec
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
27	[number]	k4	27
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
pro	pro	k7c4	pro
předpokládaných	předpokládaný	k2eAgInPc2d1	předpokládaný
12	[number]	k4	12
nových	nový	k2eAgInPc2d1	nový
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nejdříve	dříve	k6eAd3	dříve
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
25	[number]	k4	25
<g/>
,	,	kIx,	,
Smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
určila	určit	k5eAaPmAgFnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
budou	být	k5eAaImBp3nP	být
neobsazená	obsazený	k2eNgNnPc1d1	neobsazené
poslanecká	poslanecký	k2eAgNnPc1d1	poslanecké
místa	místo	k1gNnPc1	místo
poměrně	poměrně	k6eAd1	poměrně
rozdělena	rozdělen	k2eAgNnPc1d1	rozděleno
mezi	mezi	k7c4	mezi
všechny	všechen	k3xTgMnPc4	všechen
členy	člen	k1gMnPc4	člen
Unie	unie	k1gFnSc1	unie
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
poslanců	poslanec	k1gMnPc2	poslanec
byl	být	k5eAaImAgInS	být
732	[number]	k4	732
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
platí	platit	k5eAaImIp3nS	platit
přechodně	přechodně	k6eAd1	přechodně
pro	pro	k7c4	pro
parlamentní	parlamentní	k2eAgNnSc4d1	parlamentní
období	období	k1gNnSc4	období
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byly	být	k5eAaImAgFnP	být
přijaty	přijmout	k5eAaPmNgFnP	přijmout
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
nové	nový	k2eAgFnPc1d1	nová
členské	členský	k2eAgFnPc1d1	členská
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
tito	tento	k3xDgMnPc1	tento
noví	nový	k2eAgMnPc1d1	nový
členové	člen	k1gMnPc1	člen
volili	volit	k5eAaImAgMnP	volit
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
do	do	k7c2	do
EP	EP	kA	EP
a	a	k8xC	a
počet	počet	k1gInSc1	počet
poslanců	poslanec	k1gMnPc2	poslanec
tak	tak	k9	tak
dočasně	dočasně	k6eAd1	dočasně
překročil	překročit	k5eAaPmAgInS	překročit
maximální	maximální	k2eAgInSc4d1	maximální
počet	počet	k1gInSc4	počet
732	[number]	k4	732
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
určuje	určovat	k5eAaImIp3nS	určovat
maximální	maximální	k2eAgInSc4d1	maximální
počet	počet	k1gInSc4	počet
poslanců	poslanec	k1gMnPc2	poslanec
na	na	k7c4	na
751	[number]	k4	751
celkově	celkově	k6eAd1	celkově
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
parlamentu	parlament	k1gInSc2	parlament
však	však	k9	však
nemůže	moct	k5eNaImIp3nS	moct
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
96	[number]	k4	96
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
členský	členský	k2eAgInSc4d1	členský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
však	však	k9	však
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
rada	rada	k1gFnSc1	rada
proto	proto	k8xC	proto
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
dočasné	dočasný	k2eAgNnSc4d1	dočasné
zvýšení	zvýšení	k1gNnSc4	zvýšení
počtu	počet	k1gInSc2	počet
poslanců	poslanec	k1gMnPc2	poslanec
na	na	k7c4	na
754	[number]	k4	754
(	(	kIx(	(
<g/>
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
jejich	jejich	k3xOp3gInSc2	jejich
výběru	výběr	k1gInSc2	výběr
<g/>
)	)	kIx)	)
do	do	k7c2	do
konce	konec	k1gInSc2	konec
tohoto	tento	k3xDgNnSc2	tento
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zachována	zachován	k2eAgFnSc1d1	zachována
proporcionalita	proporcionalita	k1gFnSc1	proporcionalita
při	při	k7c6	při
nemožnosti	nemožnost	k1gFnSc6	nemožnost
odebrat	odebrat	k5eAaPmF	odebrat
Německu	Německo	k1gNnSc3	Německo
existující	existující	k2eAgInSc4d1	existující
mandáty	mandát	k1gInPc4	mandát
během	během	k7c2	během
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
úprava	úprava	k1gFnSc1	úprava
tak	tak	k6eAd1	tak
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
poslanců	poslanec	k1gMnPc2	poslanec
navýšen	navýšit	k5eAaPmNgInS	navýšit
o	o	k7c4	o
12	[number]	k4	12
reprezentantů	reprezentant	k1gInPc2	reprezentant
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
na	na	k7c4	na
766	[number]	k4	766
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
751	[number]	k4	751
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
poslanců	poslanec	k1gMnPc2	poslanec
749	[number]	k4	749
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oproti	oproti	k7c3	oproti
stavu	stav	k1gInSc3	stav
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
přijdou	přijít	k5eAaPmIp3nP	přijít
o	o	k7c4	o
minimálně	minimálně	k6eAd1	minimálně
jednoho	jeden	k4xCgMnSc4	jeden
poslance	poslanec	k1gMnSc4	poslanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Orgány	orgány	k1gFnPc4	orgány
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
==	==	k?	==
</s>
</p>
<p>
<s>
Práce	práce	k1gFnSc1	práce
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
parlamentů	parlament	k1gInPc2	parlament
organizována	organizovat	k5eAaBmNgFnS	organizovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výborů	výbor	k1gInPc2	výbor
a	a	k8xC	a
pléna	plénum	k1gNnSc2	plénum
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
text	text	k1gInSc1	text
schválený	schválený	k2eAgInSc1d1	schválený
na	na	k7c6	na
plénu	plénum	k1gNnSc6	plénum
je	být	k5eAaImIp3nS	být
platný	platný	k2eAgInSc1d1	platný
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
práce	práce	k1gFnSc1	práce
výborů	výbor	k1gInPc2	výbor
nepostradatelná	postradatelný	k2eNgFnSc1d1	nepostradatelná
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
totiž	totiž	k9	totiž
poslancům	poslanec	k1gMnPc3	poslanec
možnost	možnost	k1gFnSc4	možnost
specializace	specializace	k1gFnSc1	specializace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
o	o	k7c6	o
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
návrzích	návrh	k1gInPc6	návrh
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výborů	výbor	k1gInPc2	výbor
mezi	mezi	k7c7	mezi
poslanci	poslanec	k1gMnPc7	poslanec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
dané	daný	k2eAgFnPc1d1	daná
oblasti	oblast	k1gFnPc1	oblast
blíže	blízce	k6eAd2	blízce
věnují	věnovat	k5eAaPmIp3nP	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
20	[number]	k4	20
stálých	stálý	k2eAgInPc2d1	stálý
výborů	výbor	k1gInPc2	výbor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vesměs	vesměs	k6eAd1	vesměs
tematicky	tematicky	k6eAd1	tematicky
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
generálním	generální	k2eAgNnSc7d1	generální
ředitelstvím	ředitelství	k1gNnSc7	ředitelství
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výborů	výbor	k1gInPc2	výbor
existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
podvýbory	podvýbor	k1gInPc1	podvýbor
a	a	k8xC	a
také	také	k9	také
dočasné	dočasný	k2eAgInPc1d1	dočasný
výbory	výbor	k1gInPc1	výbor
svolávané	svolávaný	k2eAgInPc1d1	svolávaný
k	k	k7c3	k
aktuálním	aktuální	k2eAgNnPc3d1	aktuální
tématům	téma	k1gNnPc3	téma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
výborů	výbor	k1gInPc2	výbor
má	mít	k5eAaImIp3nS	mít
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
ještě	ještě	k6eAd1	ještě
delegace	delegace	k1gFnSc2	delegace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gNnPc2	on
33	[number]	k4	33
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
meziparlamentní	meziparlamentní	k2eAgInSc4d1	meziparlamentní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
poslanci	poslanec	k1gMnPc7	poslanec
parlamentů	parlament	k1gInPc2	parlament
třetích	třetí	k4xOgFnPc2	třetí
zemí	zem	k1gFnPc2	zem
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
a	a	k8xC	a
také	také	k9	také
přidružených	přidružený	k2eAgFnPc2d1	přidružená
či	či	k8xC	či
kandidátských	kandidátský	k2eAgFnPc2d1	kandidátská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
EP	EP	kA	EP
i	i	k9	i
delegace	delegace	k1gFnSc2	delegace
pro	pro	k7c4	pro
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
parlamentních	parlamentní	k2eAgNnPc6d1	parlamentní
shromážděních	shromáždění	k1gNnPc6	shromáždění
EU-AKT	EU-AKT	k1gMnPc2	EU-AKT
(	(	kIx(	(
<g/>
africké	africký	k2eAgInPc1d1	africký
<g/>
,	,	kIx,	,
karibské	karibský	k2eAgInPc1d1	karibský
a	a	k8xC	a
tichomořské	tichomořský	k2eAgInPc1d1	tichomořský
státy	stát	k1gInPc1	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
EU-Středomoří	EU-Středomoří	k1gNnSc1	EU-Středomoří
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
parlamentním	parlamentní	k2eAgNnSc6d1	parlamentní
shromáždění	shromáždění	k1gNnSc6	shromáždění
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
výborů	výbor	k1gInPc2	výbor
a	a	k8xC	a
delegací	delegace	k1gFnPc2	delegace
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
ještě	ještě	k9	ještě
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
jeho	on	k3xPp3gInSc4	on
chod	chod	k1gInSc4	chod
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gFnPc7	on
konference	konference	k1gFnSc1	konference
předsedů	předseda	k1gMnPc2	předseda
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
EP	EP	kA	EP
a	a	k8xC	a
předsedové	předseda	k1gMnPc1	předseda
politických	politický	k2eAgFnPc2d1	politická
skupin	skupina	k1gFnPc2	skupina
<g/>
;	;	kIx,	;
řeší	řešit	k5eAaImIp3nS	řešit
otázky	otázka	k1gFnPc4	otázka
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
chodem	chod	k1gInSc7	chod
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvestoři	kvestor	k1gMnPc1	kvestor
(	(	kIx(	(
<g/>
volení	volený	k2eAgMnPc1d1	volený
poslanci	poslanec	k1gMnPc1	poslanec
pro	pro	k7c4	pro
vyřizování	vyřizování	k1gNnSc4	vyřizování
administrativních	administrativní	k2eAgFnPc2d1	administrativní
a	a	k8xC	a
finančních	finanční	k2eAgFnPc2d1	finanční
otázek	otázka	k1gFnPc2	otázka
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
a	a	k8xC	a
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
EP	EP	kA	EP
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
EP	EP	kA	EP
<g/>
,	,	kIx,	,
místopředsedové	místopředseda	k1gMnPc1	místopředseda
a	a	k8xC	a
kvestoři	kvestor	k1gMnPc1	kvestor
jako	jako	k8xC	jako
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
má	mít	k5eAaImIp3nS	mít
předseda	předseda	k1gMnSc1	předseda
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
EP	EP	kA	EP
navenek	navenek	k6eAd1	navenek
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
EP	EP	kA	EP
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
a	a	k8xC	a
momentálně	momentálně	k6eAd1	momentálně
jím	on	k3xPp3gNnSc7	on
je	být	k5eAaImIp3nS	být
italský	italský	k2eAgMnSc1d1	italský
poslanec	poslanec	k1gMnSc1	poslanec
Antonio	Antonio	k1gMnSc1	Antonio
Tajani	Tajan	k1gMnPc1	Tajan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
svého	svůj	k3xOyFgMnSc2	svůj
německého	německý	k2eAgMnSc2d1	německý
předchůdce	předchůdce	k1gMnSc2	předchůdce
Martina	Martin	k1gMnSc2	Martin
Schulze	Schulz	k1gMnSc2	Schulz
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
Generální	generální	k2eAgInSc1d1	generální
sekretariát	sekretariát	k1gInSc1	sekretariát
tvořený	tvořený	k2eAgInSc1d1	tvořený
5	[number]	k4	5
000	[number]	k4	000
úředníky	úředník	k1gMnPc7	úředník
v	v	k7c6	v
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
a	a	k8xC	a
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
rozdělování	rozdělování	k1gNnSc4	rozdělování
všech	všecek	k3xTgInPc2	všecek
postů	post	k1gInPc2	post
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
orgánů	orgán	k1gInPc2	orgán
EP	EP	kA	EP
mezi	mezi	k7c7	mezi
frakcemi	frakce	k1gFnPc7	frakce
(	(	kIx(	(
<g/>
předsednictví	předsednictví	k1gNnSc2	předsednictví
EP	EP	kA	EP
<g/>
,	,	kIx,	,
předsednictví	předsednictví	k1gNnSc2	předsednictví
výborů	výbor	k1gInPc2	výbor
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
frakcí	frakce	k1gFnPc2	frakce
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
národními	národní	k2eAgFnPc7d1	národní
delegacemi	delegace	k1gFnPc7	delegace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Hondtova	Hondtův	k2eAgFnSc1d1	Hondtova
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dobře	dobře	k6eAd1	dobře
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
váhu	váha	k1gFnSc4	váha
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
subjektů	subjekt	k1gInPc2	subjekt
(	(	kIx(	(
<g/>
stejný	stejný	k2eAgInSc1d1	stejný
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
rozdělování	rozdělování	k1gNnSc4	rozdělování
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
funguje	fungovat	k5eAaImIp3nS	fungovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
institucionálním	institucionální	k2eAgInSc7d1	institucionální
rámcem	rámec	k1gInSc7	rámec
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburk	Lucemburk	k1gInSc1	Lucemburk
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
s.	s.	k?	s.
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MCCORMICK	MCCORMICK	kA	MCCORMICK
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
:	:	kIx,	:
Understanding	Understanding	k1gInSc1	Understanding
the	the	k?	the
European	European	k1gInSc1	European
Union	union	k1gInSc1	union
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Concise	Concise	k1gFnPc1	Concise
Introduction	Introduction	k1gInSc1	Introduction
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
th	th	k?	th
ed	ed	k?	ed
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
s.	s.	k?	s.
87	[number]	k4	87
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
</s>
</p>
<p>
<s>
Politická	politický	k2eAgFnSc1d1	politická
skupina	skupina	k1gFnSc1	skupina
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
</s>
</p>
<p>
<s>
Volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Evropský	evropský	k2eAgInSc4d1	evropský
parlament	parlament	k1gInSc4	parlament
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc2	galerie
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Informační	informační	k2eAgFnSc1d1	informační
kancelář	kancelář	k1gFnSc1	kancelář
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
Odborně	odborně	k6eAd1	odborně
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Vliv	vliv	k1gInSc1	vliv
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
na	na	k7c4	na
rozhodování	rozhodování	k1gNnSc4	rozhodování
v	v	k7c6	v
EU	EU	kA	EU
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Ucelený	ucelený	k2eAgInSc4d1	ucelený
a	a	k8xC	a
aktuální	aktuální	k2eAgInSc4d1	aktuální
přehled	přehled	k1gInSc4	přehled
souvislostí	souvislost	k1gFnPc2	souvislost
ohledně	ohledně	k7c2	ohledně
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
zajímavosti	zajímavost	k1gFnSc2	zajímavost
a	a	k8xC	a
stanoviska	stanovisko	k1gNnSc2	stanovisko
významných	významný	k2eAgMnPc2d1	významný
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
