<p>
<s>
V	v	k7c6	v
Březinách	Březina	k1gFnPc6	Březina
(	(	kIx(	(
<g/>
388	[number]	k4	388
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc4	vrch
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Louny	Louny	k1gInPc1	Louny
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
Smolnické	Smolnický	k2eAgFnSc2d1	Smolnická
stupňoviny	stupňovina	k1gFnSc2	stupňovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Západním	západní	k2eAgNnSc7d1	západní
a	a	k8xC	a
jižním	jižní	k2eAgNnSc7d1	jižní
okolím	okolí	k1gNnSc7	okolí
vrcholu	vrchol	k1gInSc2	vrchol
prochází	procházet	k5eAaImIp3nS	procházet
červená	červený	k2eAgFnSc1d1	červená
turistická	turistický	k2eAgFnSc1d1	turistická
značka	značka	k1gFnSc1	značka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geomorfologické	geomorfologický	k2eAgNnSc4d1	Geomorfologické
zařazení	zařazení	k1gNnSc4	zařazení
==	==	k?	==
</s>
</p>
<p>
<s>
Geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
vrch	vrch	k1gInSc1	vrch
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
celku	celek	k1gInSc2	celek
Dolnooharská	Dolnooharský	k2eAgFnSc1d1	Dolnooharský
tabule	tabule	k1gFnSc1	tabule
<g/>
,	,	kIx,	,
podcelku	podcelka	k1gFnSc4	podcelka
Hazmburská	Hazmburský	k2eAgFnSc1d1	Hazmburská
tabule	tabule	k1gFnSc1	tabule
<g/>
,	,	kIx,	,
okrsku	okrsek	k1gInSc6	okrsek
Smolnická	Smolnický	k2eAgFnSc1d1	Smolnická
stupňovina	stupňovina	k1gFnSc1	stupňovina
a	a	k8xC	a
podokrsku	podokrska	k1gFnSc4	podokrska
Hřivčická	Hřivčická	k1gFnSc1	Hřivčická
stupňovina	stupňovina	k1gFnSc1	stupňovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
