<s>
Mont	Mont	k2eAgInSc1d1
Thabor	Thabor	k1gInSc1
</s>
<s>
Mont	Mont	k2eAgMnSc1d1
Thabor	Thabor	k1gMnSc1
Mont	Mont	k1gMnSc1
Thabor	Thabor	k1gMnSc1
v	v	k7c6
Savojských	savojský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
3178	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
97	#num#	k4
m	m	kA
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Západní	západní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Mont	Mont	k2eAgInSc1d1
Thabor	Thabor	k1gInSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
kvarcit	kvarcit	k1gInSc1
<g/>
,	,	kIx,
verrucano	verrucana	k1gFnSc5
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mont	Mont	k2eAgInSc1d1
Thabor	Thabor	k1gInSc1
(	(	kIx(
<g/>
3178	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejvyšších	vysoký	k2eAgInPc2d3
vrcholů	vrchol	k1gInPc2
v	v	k7c6
cerceském	cerceský	k2eAgInSc6d1
masívu	masív	k1gInSc6
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
massif	massif	k1gMnSc1
des	des	k1gNnSc2
Cerces	Cerces	k1gMnSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgMnSc1d1
též	též	k9
massif	massif	k1gMnSc1
du	du	k?
Thabor	Thabor	k1gMnSc1
nebo	nebo	k8xC
massif	massif	k1gMnSc1
des	des	k1gNnSc1
Cerces-Thabor	Cerces-Thabor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
náleží	náležet	k5eAaImIp3nS
do	do	k7c2
Kottických	Kottický	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
Západních	západní	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hora	Hora	k1gMnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
departementů	departement	k1gInPc2
Savojsko	Savojsko	k1gNnSc1
(	(	kIx(
<g/>
region	region	k1gInSc1
Auvergne-Rhône-Alpes	Auvergne-Rhône-Alpes	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Hautes-Alpes	Hautes-Alpes	k1gInSc1
(	(	kIx(
<g/>
region	region	k1gInSc1
Provence-Alpes-Côte	Provence-Alpes-Côt	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Azur	azur	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
</s>
<s>
Hora	hora	k1gFnSc1
Thabor	Thabora	k1gFnPc2
leží	ležet	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
jen	jen	k9
3,5	3,5	k4
-	-	kIx~
5	#num#	k4
km	km	kA
vzdušnou	vzdušný	k2eAgFnSc7d1
čarou	čára	k1gFnSc7
na	na	k7c4
západ	západ	k1gInSc4
od	od	k7c2
francouzsko-italské	francouzsko-italský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masív	masív	k1gInSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
francouzského	francouzský	k2eAgNnSc2d1
horského	horský	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
Modane	Modan	k1gMnSc5
a	a	k8xC
od	od	k7c2
údolí	údolí	k1gNnSc2
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
prochází	procházet	k5eAaImIp3nS
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
z	z	k7c2
Turína	Turín	k1gInSc2
do	do	k7c2
Chambéry	Chambéra	k1gFnSc2
a	a	k8xC
Grenoblu	Grenoble	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
departementu	departement	k1gInSc6
Savojsko	Savojsko	k1gNnSc1
jsou	být	k5eAaImIp3nP
nejbližšími	blízký	k2eAgFnPc7d3
obcemi	obec	k1gFnPc7
Valmeinier	Valmeinira	k1gFnPc2
a	a	k8xC
Orelle	Orelle	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
v	v	k7c6
departementu	departement	k1gInSc6
Hautes-Alpes	Hautes-Alpes	k1gMnSc1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
Névache	Névache	k1gNnSc4
<g/>
,	,	kIx,
nejvýše	vysoce	k6eAd3,k6eAd1
položená	položený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
(	(	kIx(
<g/>
1596	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
v	v	k7c6
údolí	údolí	k1gNnSc6
Clarée	Claré	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhé	Dlouhé	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
Clarée	Claré	k1gFnSc2
stoupá	stoupat	k5eAaImIp3nS
od	od	k7c2
Névache	Névach	k1gInSc2
severozápadním	severozápadní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c4
sever	sever	k1gInSc4
až	až	k9
k	k	k7c3
průsmyku	průsmyk	k1gInSc3
Col	cola	k1gFnPc2
des	des	k1gNnSc2
Méandes	Méandesa	k1gFnPc2
a	a	k8xC
masívu	masív	k1gInSc2
hory	hora	k1gFnSc2
Thabor	Thabora	k1gFnPc2
vede	vést	k5eAaImIp3nS
z	z	k7c2
Névache	Névach	k1gInSc2
cesta	cesta	k1gFnSc1
údolím	údolí	k1gNnSc7
Étroite	Étroit	k1gInSc5
(	(	kIx(
<g/>
Vallée	Valléus	k1gMnSc5
Étroite	Étroit	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
500	#num#	k4
metrů	metr	k1gInPc2
severně	severně	k6eAd1
od	od	k7c2
hory	hora	k1gFnSc2
Thabor	Thabora	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
výrazný	výrazný	k2eAgInSc1d1
horský	horský	k2eAgInSc1d1
štít	štít	k1gInSc1
Pic	pic	k0
du	du	k?
Thabor	Thabor	k1gInSc1
(	(	kIx(
<g/>
3207	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
29	#num#	k4
metrů	metr	k1gInPc2
vyšší	vysoký	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
samotná	samotný	k2eAgFnSc1d1
hora	hora	k1gFnSc1
Thabor	Thabor	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
hory	hora	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
poutní	poutní	k2eAgInSc1d1
kostelík	kostelík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
místního	místní	k2eAgNnSc2d1
podání	podání	k1gNnSc2
zde	zde	k6eAd1
křesťanský	křesťanský	k2eAgInSc1d1
svatostánek	svatostánek	k1gInSc1
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
konaly	konat	k5eAaImAgFnP
poutě	pouť	k1gFnPc1
<g/>
,	,	kIx,
existoval	existovat	k5eAaImAgMnS
již	již	k6eAd1
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
této	tento	k3xDgFnSc2
tradované	tradovaný	k2eAgFnSc2d1
verze	verze	k1gFnSc2
je	být	k5eAaImIp3nS
název	název	k1gInSc1
hory	hora	k1gFnSc2
spojován	spojovat	k5eAaImNgInS
s	s	k7c7
existencí	existence	k1gFnSc7
posvátné	posvátný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Tábor	Tábor	k1gInSc1
v	v	k7c6
Galileji	Galilea	k1gFnSc6
<g/>
,	,	kIx,
o	o	k7c4
niž	jenž	k3xRgFnSc4
přinesli	přinést	k5eAaPmAgMnP
do	do	k7c2
Evropy	Evropa	k1gFnSc2
informace	informace	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
jejím	její	k3xOp3gNnSc7
jménem	jméno	k1gNnSc7
a	a	k8xC
s	s	k7c7
tradicí	tradice	k1gFnSc7
jejího	její	k3xOp3gNnSc2
uctívání	uctívání	k1gNnSc2
účastníci	účastník	k1gMnPc1
středověkých	středověký	k2eAgFnPc2d1
křižáckých	křižácký	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradice	tradice	k1gFnSc1
poutí	pouť	k1gFnPc2
na	na	k7c4
horu	hora	k1gFnSc4
Thabor	Thabora	k1gFnPc2
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
patřil	patřit	k5eAaImAgInS
vrchol	vrchol	k1gInSc1
hory	hora	k1gFnSc2
Thabor	Thabora	k1gFnPc2
spolu	spolu	k6eAd1
s	s	k7c7
údolím	údolí	k1gNnSc7
Étroite	Étroit	k1gInSc5
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
Valle	Valle	k1gFnSc1
Stretta	stretta	k1gFnSc1
<g/>
)	)	kIx)
Itálii	Itálie	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c6
ratifikaci	ratifikace	k1gFnSc6
Pařížských	pařížský	k2eAgFnPc2d1
mírových	mírový	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
v	v	k7c6
únoru	únor	k1gInSc6
1947	#num#	k4
však	však	k9
byla	být	k5eAaImAgFnS
hranice	hranice	k1gFnSc1
posunuta	posunut	k2eAgFnSc1d1
a	a	k8xC
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
připadlo	připadnout	k5eAaPmAgNnS
Francii	Francie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Poutníci	poutník	k1gMnPc1
u	u	k7c2
kostelíka	kostelík	k1gInSc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
hory	hora	k1gFnSc2
Thabor	Thabora	k1gFnPc2
</s>
<s>
Pic	pic	k0
du	du	k?
Thabor	Thabor	k1gInSc4
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Mont	Mont	k2eAgInSc1d1
Thabor	Thabor	k1gInSc1
</s>
<s>
Ulice	ulice	k1gFnSc1
Mont	Mont	k1gMnSc1
Thabor	Thabor	k1gMnSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
Pohled	pohled	k1gInSc1
k	k	k7c3
Thaboru	Thabor	k1gInSc3
od	od	k7c2
průsmyku	průsmyk	k1gInSc2
Valmeinier	Valmeinira	k1gFnPc2
</s>
<s>
Horská	horský	k2eAgFnSc1d1
chata	chata	k1gFnSc1
(	(	kIx(
<g/>
refuge	refuge	k1gInSc1
<g/>
)	)	kIx)
Mont	Mont	k2eAgInSc1d1
Thabor	Thabor	k1gInSc1
</s>
<s>
Zimní	zimní	k2eAgInSc4d1
výstup	výstup	k1gInSc4
na	na	k7c4
Mont	Mont	k2eAgInSc4d1
Thabor	Thabor	k1gInSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mont	Mont	k1gMnSc1
Thabor	Thabor	k1gMnSc1
(	(	kIx(
<g/>
France	Franc	k1gMnSc2
<g/>
)	)	kIx)
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Mont	Mont	k1gMnSc1
Thabor	Thabor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
výškový	výškový	k2eAgInSc1d1
bod	bod	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
-	-	kIx~
mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Quelques	Quelques	k1gMnSc1
randonnées	randonnées	k1gMnSc1
en	en	k?
Savoie	Savoie	k1gFnSc1
<g/>
:	:	kIx,
Mont	Mont	k2eAgInSc1d1
Thabor	Thabor	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mont	Mont	k2eAgInSc4d1
Thabor	Thabor	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Geologický	geologický	k2eAgInSc1d1
popis	popis	k1gInSc1
hory	hora	k1gFnSc2
Thabor	Thabora	k1gFnPc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Popis	popis	k1gInSc1
výstupové	výstupový	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
na	na	k7c4
Mont	Mont	k2eAgInSc4d1
Thabor	Thabor	k1gInSc4
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
6708148574363024430004	#num#	k4
</s>
