<p>
<s>
Paradox	paradox	k1gInSc1	paradox
sta	sto	k4xCgNnSc2	sto
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
také	také	k9	také
Berryho	Berry	k1gMnSc2	Berry
paradox	paradox	k1gInSc1	paradox
či	či	k8xC	či
paradox	paradox	k1gInSc1	paradox
25	[number]	k4	25
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
s	s	k7c7	s
příslušnými	příslušný	k2eAgFnPc7d1	příslušná
obměnami	obměna	k1gFnPc7	obměna
<g/>
))	))	k?	))
je	být	k5eAaImIp3nS	být
logický	logický	k2eAgInSc4d1	logický
paradox	paradox	k1gInSc4	paradox
založený	založený	k2eAgInSc4d1	založený
na	na	k7c4	na
nerozlišování	nerozlišování	k1gNnSc4	nerozlišování
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
metajazyka	metajazyk	k1gInSc2	metajazyk
neboli	neboli	k8xC	neboli
na	na	k7c6	na
hovoření	hovoření	k1gNnPc2	hovoření
jazykem	jazyk	k1gInSc7	jazyk
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
podobnými	podobný	k2eAgInPc7d1	podobný
paradoxy	paradox	k1gInPc7	paradox
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Russellův	Russellův	k2eAgInSc1d1	Russellův
paradox	paradox	k1gInSc1	paradox
<g/>
,	,	kIx,	,
Richardův	Richardův	k2eAgInSc1d1	Richardův
paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
podnítil	podnítit	k5eAaPmAgInS	podnítit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
matematické	matematický	k2eAgFnSc2d1	matematická
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Paradox	paradox	k1gInSc1	paradox
sta	sto	k4xCgNnSc2	sto
slov	slovo	k1gNnPc2	slovo
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
formulován	formulovat	k5eAaImNgInS	formulovat
Bertrandem	Bertrando	k1gNnSc7	Bertrando
Russellem	Russell	k1gInSc7	Russell
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Russell	Russell	k1gMnSc1	Russell
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
veškeré	veškerý	k3xTgFnPc4	veškerý
zásluhy	zásluha	k1gFnPc4	zásluha
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vzniku	vznik	k1gInSc6	vznik
přisoudil	přisoudit	k5eAaPmAgInS	přisoudit
G.	G.	kA	G.
G.	G.	kA	G.
Berrymu	Berrym	k1gInSc6	Berrym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Znění	znění	k1gNnSc2	znění
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
abeceda	abeceda	k1gFnSc1	abeceda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
konečně	konečně	k6eAd1	konečně
mnoho	mnoho	k4c4	mnoho
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
českých	český	k2eAgInPc2d1	český
(	(	kIx(	(
<g/>
smysluplných	smysluplný	k2eAgInPc2d1	smysluplný
<g/>
)	)	kIx)	)
slov	slovo	k1gNnPc2	slovo
majících	mající	k2eAgMnPc2d1	mající
méně	málo	k6eAd2	málo
než	než	k8xS	než
sto	sto	k4xCgNnSc1	sto
písmen	písmeno	k1gNnPc2	písmeno
je	být	k5eAaImIp3nS	být
také	také	k9	také
pouze	pouze	k6eAd1	pouze
konečně	konečně	k6eAd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
i	i	k8xC	i
všech	všecek	k3xTgFnPc2	všecek
českých	český	k2eAgFnPc2d1	Česká
(	(	kIx(	(
<g/>
smysluplných	smysluplný	k2eAgFnPc2d1	smysluplná
<g/>
)	)	kIx)	)
vět	věta	k1gFnPc2	věta
obsahujících	obsahující	k2eAgFnPc6d1	obsahující
méně	málo	k6eAd2	málo
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každé	každý	k3xTgNnSc1	každý
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
konečně	konečně	k6eAd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
vět	věta	k1gFnPc2	věta
definují	definovat	k5eAaBmIp3nP	definovat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nějaké	nějaký	k3yIgNnSc4	nějaký
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
takovou	takový	k3xDgFnSc7	takový
větou	věta	k1gFnSc7	věta
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Dvacet	dvacet	k4xCc4	dvacet
sedm	sedm	k4xCc1	sedm
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Třetí	třetí	k4xOgFnSc1	třetí
mocnina	mocnina	k1gFnSc1	mocnina
největšího	veliký	k2eAgNnSc2d3	veliký
dvanácticiferného	dvanácticiferný	k2eAgNnSc2d1	dvanácticiferný
prvočísla	prvočíslo	k1gNnSc2	prvočíslo
zvětšená	zvětšený	k2eAgFnSc1d1	zvětšená
o	o	k7c4	o
pět	pět	k4xCc4	pět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
větou	věta	k1gFnSc7	věta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nedefinuje	definovat	k5eNaBmIp3nS	definovat
žádné	žádný	k3yNgNnSc4	žádný
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
filipínská	filipínský	k2eAgFnSc1d1	filipínská
řeka	řeka	k1gFnSc1	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
všech	všecek	k3xTgFnPc2	všecek
vět	věta	k1gFnPc2	věta
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
majících	mající	k2eAgFnPc6d1	mající
méně	málo	k6eAd2	málo
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každé	každý	k3xTgNnSc1	každý
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
písmen	písmeno	k1gNnPc2	písmeno
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
definují	definovat	k5eAaBmIp3nP	definovat
nějaké	nějaký	k3yIgNnSc4	nějaký
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
konečně	konečně	k6eAd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgNnPc2	všecek
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
však	však	k9	však
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nP	muset
existovat	existovat	k5eAaImF	existovat
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
žádnou	žádný	k3yNgFnSc7	žádný
větou	věta	k1gFnSc7	věta
splňující	splňující	k2eAgFnPc1d1	splňující
výše	výše	k1gFnPc1	výše
popsané	popsaný	k2eAgFnPc4d1	popsaná
podmínky	podmínka	k1gFnPc4	podmínka
definovat	definovat	k5eAaBmF	definovat
nelze	lze	k6eNd1	lze
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
existuje	existovat	k5eAaImIp3nS	existovat
nejmenší	malý	k2eAgNnSc1d3	nejmenší
takové	takový	k3xDgNnSc4	takový
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ovšem	ovšem	k9	ovšem
věta	věta	k1gFnSc1	věta
"	"	kIx"	"
<g/>
Nejmenší	malý	k2eAgNnSc1d3	nejmenší
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
definovat	definovat	k5eAaBmF	definovat
pomocí	pomocí	k7c2	pomocí
věty	věta	k1gFnSc2	věta
o	o	k7c4	o
méně	málo	k6eAd2	málo
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
slovech	slovo	k1gNnPc6	slovo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každé	každý	k3xTgNnSc1	každý
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
písmen	písmeno	k1gNnPc2	písmeno
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
větou	věta	k1gFnSc7	věta
o	o	k7c4	o
méně	málo	k6eAd2	málo
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
slovech	slovo	k1gNnPc6	slovo
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
o	o	k7c6	o
24	[number]	k4	24
slovech	slovo	k1gNnPc6	slovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každé	každý	k3xTgNnSc1	každý
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
písmen	písmeno	k1gNnPc2	písmeno
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
toto	tento	k3xDgNnSc4	tento
číslo	číslo	k1gNnSc4	číslo
definuje	definovat	k5eAaBmIp3nS	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
náležející	náležející	k2eAgMnSc1d1	náležející
mezi	mezi	k7c4	mezi
čísla	číslo	k1gNnPc4	číslo
(	(	kIx(	(
<g/>
větou	věta	k1gFnSc7	věta
jistých	jistý	k2eAgFnPc2d1	jistá
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
)	)	kIx)	)
nedefinovatelná	definovatelný	k2eNgFnSc1d1	nedefinovatelná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
(	(	kIx(	(
<g/>
větou	věta	k1gFnSc7	věta
těchto	tento	k3xDgFnPc2	tento
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
)	)	kIx)	)
definováno	definovat	k5eAaBmNgNnS	definovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řešení	řešení	k1gNnSc1	řešení
==	==	k?	==
</s>
</p>
<p>
<s>
Řešení	řešení	k1gNnSc1	řešení
paradoxu	paradox	k1gInSc2	paradox
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
odlišení	odlišení	k1gNnSc6	odlišení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
metajazyka	metajazyk	k1gInSc2	metajazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
běžně	běžně	k6eAd1	běžně
komunikujeme	komunikovat	k5eAaImIp1nP	komunikovat
a	a	k8xC	a
přemýšlíme	přemýšlet	k5eAaImIp1nP	přemýšlet
<g/>
,	,	kIx,	,
od	od	k7c2	od
jazyka	jazyk	k1gInSc2	jazyk
speciálního	speciální	k2eAgInSc2d1	speciální
<g/>
,	,	kIx,	,
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
mluvení	mluvení	k1gNnSc4	mluvení
o	o	k7c6	o
objektech	objekt	k1gInPc6	objekt
nějaké	nějaký	k3yIgFnSc2	nějaký
užší	úzký	k2eAgFnSc2d2	užší
oblasti	oblast	k1gFnSc2	oblast
našeho	náš	k3xOp1gInSc2	náš
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
mluvit	mluvit	k5eAaImF	mluvit
přirozeným	přirozený	k2eAgInSc7d1	přirozený
jazykem	jazyk	k1gInSc7	jazyk
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
speciálním	speciální	k2eAgInSc6d1	speciální
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
mluvit	mluvit	k5eAaImF	mluvit
přirozeným	přirozený	k2eAgInSc7d1	přirozený
jazykem	jazyk	k1gInSc7	jazyk
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
přirozeném	přirozený	k2eAgInSc6d1	přirozený
ani	ani	k8xC	ani
mluvit	mluvit	k5eAaImF	mluvit
speciálním	speciální	k2eAgInSc7d1	speciální
jazykem	jazyk	k1gInSc7	jazyk
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
speciálním	speciální	k2eAgInSc6d1	speciální
či	či	k8xC	či
přirozeném	přirozený	k2eAgInSc6d1	přirozený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
paradoxu	paradox	k1gInSc2	paradox
sta	sto	k4xCgNnPc1	sto
slov	slovo	k1gNnPc2	slovo
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
promíchání	promíchání	k1gNnSc3	promíchání
jazyka	jazyk	k1gInSc2	jazyk
speciálního	speciální	k2eAgInSc2d1	speciální
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
sloužícího	sloužící	k2eAgInSc2d1	sloužící
k	k	k7c3	k
definování	definování	k1gNnSc3	definování
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
a	a	k8xC	a
metajazyka	metajazyk	k1gInSc2	metajazyk
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
definujeme	definovat	k5eAaBmIp1nP	definovat
"	"	kIx"	"
<g/>
Nejmenší	malý	k2eAgNnSc1d3	nejmenší
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
definovat	definovat	k5eAaBmF	definovat
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
tuto	tento	k3xDgFnSc4	tento
"	"	kIx"	"
<g/>
metavětu	metavět	k1gInSc6	metavět
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
větu	věta	k1gFnSc4	věta
v	v	k7c6	v
metajazyce	metajazyk	k1gInSc6	metajazyk
<g/>
)	)	kIx)	)
považujeme	považovat	k5eAaImIp1nP	považovat
zároveň	zároveň	k6eAd1	zároveň
za	za	k7c4	za
větu	věta	k1gFnSc4	věta
speciálního	speciální	k2eAgInSc2d1	speciální
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jediný	jediný	k2eAgMnSc1d1	jediný
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
definovat	definovat	k5eAaBmF	definovat
přirozená	přirozený	k2eAgNnPc4d1	přirozené
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Metavětou	Metavěíst	k5eAaPmIp3nP	Metavěíst
přirozená	přirozený	k2eAgNnPc4d1	přirozené
čísla	číslo	k1gNnPc4	číslo
definovat	definovat	k5eAaBmF	definovat
nelze	lze	k6eNd1	lze
<g/>
,	,	kIx,	,
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
metavětou	metavětý	k2eAgFnSc4d1	metavětý
definujeme	definovat	k5eAaBmIp1nP	definovat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
případě	případ	k1gInSc6	případ
"	"	kIx"	"
<g/>
metapřirozeným	metapřirozený	k2eAgNnSc7d1	metapřirozený
číslem	číslo	k1gNnSc7	číslo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
celého	celý	k2eAgInSc2d1	celý
paradoxu	paradox	k1gInSc2	paradox
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
čísla	číslo	k1gNnPc4	číslo
definovaná	definovaný	k2eAgNnPc4d1	definované
větami	věta	k1gFnPc7	věta
(	(	kIx(	(
<g/>
speciálního	speciální	k2eAgInSc2d1	speciální
jazyka	jazyk	k1gInSc2	jazyk
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Dvacet	dvacet	k4xCc4	dvacet
sedm	sedm	k4xCc1	sedm
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
definované	definovaný	k2eAgNnSc1d1	definované
metavětou	metavěta	k1gFnSc7	metavěta
"	"	kIx"	"
<g/>
Nejmenší	malý	k2eAgNnSc1d3	nejmenší
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
definovat	definovat	k5eAaBmF	definovat
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
metapřirozené	metapřirozený	k2eAgNnSc4d1	metapřirozený
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ve	v	k7c6	v
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
totéž	týž	k3xTgNnSc4	týž
<g/>
"	"	kIx"	"
číslo	číslo	k1gNnSc4	číslo
zároveň	zároveň	k6eAd1	zároveň
lze	lze	k6eAd1	lze
i	i	k9	i
nelze	lze	k6eNd1	lze
definovat	definovat	k5eAaBmF	definovat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
spor	spor	k1gInSc1	spor
–	–	k?	–
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
totiž	totiž	k9	totiž
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
"	"	kIx"	"
<g/>
totéž	týž	k3xTgNnSc4	týž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
je	být	k5eAaImIp3nS	být
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
číslem	číslo	k1gNnSc7	číslo
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
metapřirozeným	metapřirozený	k2eAgFnPc3d1	metapřirozený
<g/>
,	,	kIx,	,
jako	jako	k9	jako
přirozené	přirozený	k2eAgNnSc1d1	přirozené
ho	on	k3xPp3gNnSc4	on
nelze	lze	k6eNd1	lze
definovat	definovat	k5eAaBmF	definovat
v	v	k7c6	v
(	(	kIx(	(
<g/>
speciálním	speciální	k2eAgInSc6d1	speciální
<g/>
)	)	kIx)	)
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
metapřirozené	metapřirozený	k2eAgNnSc4d1	metapřirozený
ho	on	k3xPp3gNnSc4	on
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
v	v	k7c6	v
metajazyce	metajazyk	k1gInSc6	metajazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Richardův	Richardův	k2eAgInSc1d1	Richardův
paradox	paradox	k1gInSc1	paradox
</s>
</p>
<p>
<s>
Russellův	Russellův	k2eAgInSc1d1	Russellův
paradox	paradox	k1gInSc1	paradox
</s>
</p>
<p>
<s>
Paradox	paradox	k1gInSc1	paradox
lháře	lhář	k1gMnSc4	lhář
</s>
</p>
