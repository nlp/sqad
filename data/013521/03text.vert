<s>
Go	Go	k?
(	(	kIx(
<g/>
desková	deskový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Go	Go	k?
turnaj	turnaj	k1gInSc1
v	v	k7c4
GONadřazené	GONadřazený	k2eAgNnSc4d1
odvětví	odvětví	k1gNnSc4
</s>
<s>
Duševní	duševní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
,	,	kIx,
Deskové	deskový	k2eAgFnPc4d1
hry	hra	k1gFnPc4
Mezinárodní	mezinárodní	k2eAgFnSc2d1
federace	federace	k1gFnSc2
Název	název	k1gInSc1
</s>
<s>
Světová	světový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
go	go	k?
(	(	kIx(
<g/>
IGF	IGF	kA
<g/>
)	)	kIx)
Založena	založen	k2eAgFnSc1d1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1982	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.intergofed.org	www.intergofed.org	k1gInSc1
Národní	národní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Název	název	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
go	go	k?
(	(	kIx(
<g/>
ČAGo	ČAGo	k1gMnSc1
<g/>
)	)	kIx)
Založen	založen	k2eAgMnSc1d1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1991	#num#	k4
Web	web	k1gInSc1
</s>
<s>
goweb	gowba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Go	Go	k?
Počet	počet	k1gInSc4
hráčů	hráč	k1gMnPc2
</s>
<s>
2	#num#	k4
Doporučený	doporučený	k2eAgInSc1d1
věk	věk	k1gInSc1
</s>
<s>
5	#num#	k4
až	až	k8xS
120	#num#	k4
Délka	délka	k1gFnSc1
hry	hra	k1gFnSc2
</s>
<s>
15	#num#	k4
až	až	k6eAd1
180	#num#	k4
minut	minuta	k1gFnPc2
Doba	doba	k1gFnSc1
vzniku	vznik	k1gInSc3
</s>
<s>
2000	#num#	k4
př.n.l.	př.n.l.	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Go	Go	k1gNnSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
碁	碁	kA
nebo	nebo	k8xC
častěji	často	k6eAd2
囲	囲	kA
–	–	kIx~
igo	igo	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
desková	deskový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
pro	pro	k7c4
dva	dva	k4xCgMnPc4
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
však	však	k9
nazývá	nazývat	k5eAaImIp3nS
Wej-čchi	Wej-čchi		k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
někdy	někdy	k6eAd1
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Do	do	k7c2
dnešní	dnešní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
hlavně	hlavně	k9
díky	díky	k7c3
jejímu	její	k3xOp3gInSc3
rozvoji	rozvoj	k1gInSc3
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
podporována	podporovat	k5eAaImNgFnS
šógunátem	šógunát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
rozšířená	rozšířený	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
v	v	k7c6
Koreji	Korea	k1gFnSc6
(	(	kIx(
<g/>
pod	pod	k7c7
názvem	název	k1gInSc7
Baduk	Baduk	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
všeobecně	všeobecně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
na	na	k7c6
desce	deska	k1gFnSc6
19	#num#	k4
<g/>
×	×	k?
<g/>
19	#num#	k4
průsečíků	průsečík	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
goban	goban	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
výuku	výuka	k1gFnSc4
a	a	k8xC
rychlé	rychlý	k2eAgFnPc4d1
partie	partie	k1gFnPc4
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
i	i	k9
menší	malý	k2eAgFnPc1d2
desky	deska	k1gFnPc1
–	–	k?
například	například	k6eAd1
13	#num#	k4
<g/>
×	×	k?
<g/>
13	#num#	k4
nebo	nebo	k8xC
9	#num#	k4
<g/>
×	×	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
mít	mít	k5eAaImF
na	na	k7c6
konci	konec	k1gInSc6
hry	hra	k1gFnSc2
větší	veliký	k2eAgNnSc4d2
skóre	skóre	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
zajatými	zajatý	k2eAgInPc7d1
kameny	kámen	k1gInPc7
a	a	k8xC
obklíčenými	obklíčený	k2eAgInPc7d1
průsečíky	průsečík	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Stručná	stručný	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
</s>
<s>
Příklad	příklad	k1gInSc1
sebrání	sebrání	k1gNnSc2
skupiny	skupina	k1gFnSc2
dvou	dva	k4xCgInPc2
kamenů	kámen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílý	bílý	k2eAgInSc1d1
tahem	tah	k1gInSc7
na	na	k7c4
A	A	kA
odebírá	odebírat	k5eAaImIp3nS
černé	černý	k2eAgNnSc1d1
skupině	skupina	k1gFnSc3
poslední	poslední	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
(	(	kIx(
<g/>
skupina	skupina	k1gFnSc1
byla	být	k5eAaImAgFnS
před	před	k7c7
tím	ten	k3xDgNnSc7
v	v	k7c6
atari	atar	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
odebírá	odebírat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
z	z	k7c2
desky	deska	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Pravidla	pravidlo	k1gNnSc2
hry	hra	k1gFnSc2
go	go	k?
<g/>
.	.	kIx.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
čtvercové	čtvercový	k2eAgFnSc6d1
desce	deska	k1gFnSc6
19	#num#	k4
<g/>
×	×	k?
<g/>
19	#num#	k4
průsečíků	průsečík	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
–	–	k?
černý	černý	k2eAgInSc1d1
a	a	k8xC
bílý	bílý	k2eAgInSc1d1
–	–	k?
střídavě	střídavě	k6eAd1
kladou	klást	k5eAaImIp3nP
kameny	kámen	k1gInPc1
na	na	k7c4
volné	volný	k2eAgInPc4d1
průsečíky	průsečík	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Položené	položený	k2eAgInPc1d1
kameny	kámen	k1gInPc1
se	se	k3xPyFc4
po	po	k7c6
desce	deska	k1gFnSc6
dále	daleko	k6eAd2
nijak	nijak	k6eAd1
neposunují	posunovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
černý	černý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Několik	několik	k4yIc4
kamenů	kámen	k1gInPc2
stejné	stejný	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
tzv.	tzv.	kA
skupinu	skupina	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
spolu	spolu	k6eAd1
těsně	těsně	k6eAd1
sousedí	sousedit	k5eAaImIp3nS
(	(	kIx(
<g/>
tj.	tj.	kA
z	z	k7c2
každého	každý	k3xTgNnSc2
lze	lze	k6eAd1
na	na	k7c4
libovolný	libovolný	k2eAgInSc4d1
jiný	jiný	k2eAgInSc4d1
přejít	přejít	k5eAaPmF
kroky	krok	k1gInPc4
svisle	svisle	k6eAd1
a	a	k8xC
vodorovně	vodorovně	k6eAd1
po	po	k7c6
kamenech	kámen	k1gInPc6
vlastní	vlastní	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prázdný	prázdný	k2eAgInSc1d1
průsečík	průsečík	k1gInSc1
sousedící	sousedící	k2eAgInSc1d1
se	s	k7c7
skupinou	skupina	k1gFnSc7
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
svoboda	svoboda	k1gFnSc1
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
hráč	hráč	k1gMnSc1
svým	svůj	k3xOyFgInSc7
tahem	tah	k1gInSc7
odejme	odejmout	k5eAaPmIp3nS
soupeřově	soupeřův	k2eAgFnSc3d1
skupině	skupina	k1gFnSc3
poslední	poslední	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
<g/>
,	,	kIx,
vyjme	vyjmout	k5eAaPmIp3nS
tuto	tento	k3xDgFnSc4
skupinu	skupina	k1gFnSc4
z	z	k7c2
desky	deska	k1gFnSc2
a	a	k8xC
nechá	nechat	k5eAaPmIp3nS
si	se	k3xPyFc3
kameny	kámen	k1gInPc4
jako	jako	k8xS,k8xC
tzv.	tzv.	kA
zajatce	zajatec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
zakázáno	zakázán	k2eAgNnSc1d1
provést	provést	k5eAaPmF
sebevraždu	sebevražda	k1gFnSc4
(	(	kIx(
<g/>
sebrat	sebrat	k5eAaPmF
svým	svůj	k3xOyFgInSc7
tahem	tah	k1gInSc7
své	svůj	k3xOyFgFnSc6
skupině	skupina	k1gFnSc6
poslední	poslední	k2eAgInSc4d1
volný	volný	k2eAgInSc4d1
průsečík	průsečík	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
ovšem	ovšem	k9
hráč	hráč	k1gMnSc1
svým	svůj	k3xOyFgInSc7
tahem	tah	k1gInSc7
zajme	zajmout	k5eAaPmIp3nS
soupeřovy	soupeřův	k2eAgInPc4d1
kameny	kámen	k1gInPc4
<g/>
,	,	kIx,
o	o	k7c4
sebevraždu	sebevražda	k1gFnSc4
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
takto	takto	k6eAd1
jeho	jeho	k3xOp3gFnSc1
skupina	skupina	k1gFnSc1
nezůstane	zůstat	k5eNaPmIp3nS
bez	bez	k7c2
svobod	svoboda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Hráč	hráč	k1gMnSc1
nesmí	smět	k5eNaImIp3nS
svým	svůj	k3xOyFgInSc7
tahem	tah	k1gInSc7
na	na	k7c6
desce	deska	k1gFnSc6
způsobit	způsobit	k5eAaPmF
opakování	opakování	k1gNnSc4
pozice	pozice	k1gFnSc2
těsně	těsně	k6eAd1
před	před	k7c7
soupeřovým	soupeřův	k2eAgInSc7d1
tahem	tah	k1gInSc7
(	(	kIx(
<g/>
pravidlo	pravidlo	k1gNnSc1
ko	ko	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmem	pojem	k1gInSc7
pozice	pozice	k1gFnSc1
je	být	k5eAaImIp3nS
míněna	míněn	k2eAgFnSc1d1
situace	situace	k1gFnSc1
na	na	k7c6
celé	celý	k2eAgFnSc6d1
desce	deska	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
svého	svůj	k3xOyFgInSc2
tahu	tah	k1gInSc2
vzdát	vzdát	k5eAaPmF
slovem	slovem	k6eAd1
pass	pass	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
to	ten	k3xDgNnSc1
udělají	udělat	k5eAaPmIp3nP
oba	dva	k4xCgMnPc1
hráči	hráč	k1gMnPc1
ihned	ihned	k6eAd1
po	po	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
partie	partie	k1gFnPc4
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
se	se	k3xPyFc4
shodnou	shodnout	k5eAaPmIp3nP,k5eAaBmIp3nP
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
skupiny	skupina	k1gFnPc1
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
nepochybně	pochybně	k6eNd1
zajaty	zajat	k2eAgFnPc1d1
<g/>
;	;	kIx,
takové	takový	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
si	se	k3xPyFc3
soupeř	soupeř	k1gMnSc1
vezme	vzít	k5eAaPmIp3nS
jako	jako	k8xS,k8xC
zajatce	zajatec	k1gMnPc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Neshody	neshoda	k1gFnPc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
řídké	řídký	k2eAgInPc1d1
a	a	k8xC
řeší	řešit	k5eAaImIp3nS
se	se	k3xPyFc4
dohráním	dohrání	k1gNnSc7
situace	situace	k1gFnSc2
podle	podle	k7c2
zvláštních	zvláštní	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
každý	každý	k3xTgInSc4
hráčův	hráčův	k2eAgInSc4d1
pass	pass	k1gInSc4
získá	získat	k5eAaPmIp3nS
soupeř	soupeř	k1gMnSc1
bod	bod	k1gInSc4
navíc	navíc	k6eAd1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Oba	dva	k4xCgMnPc1
soupeři	soupeř	k1gMnPc1
pak	pak	k6eAd1
spočítají	spočítat	k5eAaPmIp3nP
své	svůj	k3xOyFgNnSc4
skóre	skóre	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
prázdnými	prázdný	k2eAgInPc7d1
průsečíky	průsečík	k1gInPc7
v	v	k7c6
území	území	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
ohraničili	ohraničit	k5eAaPmAgMnP
<g/>
,	,	kIx,
a	a	k8xC
zajatci	zajatec	k1gMnPc1
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
získali	získat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
s	s	k7c7
větším	veliký	k2eAgInSc7d2
počtem	počet	k1gInSc7
bodů	bod	k1gInPc2
vyhrává	vyhrávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
kompenzaci	kompenzace	k1gFnSc4
výhody	výhoda	k1gFnSc2
prvního	první	k4xOgInSc2
tahu	tah	k1gInSc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
bílému	bílý	k1gMnSc3
přičítá	přičítat	k5eAaImIp3nS
tzv.	tzv.	kA
komi	komi	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
činí	činit	k5eAaImIp3nS
nejčastěji	často	k6eAd3
5,5	5,5	k4
<g/>
,	,	kIx,
6,5	6,5	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
7,5	7,5	k4
bodu	bod	k1gInSc2
(	(	kIx(
<g/>
polovina	polovina	k1gFnSc1
bodu	bod	k1gInSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
nemožnost	nemožnost	k1gFnSc1
remízy	remíza	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výše	vysoce	k6eAd2
uvedená	uvedený	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
nejsou	být	k5eNaImIp3nP
úplně	úplně	k6eAd1
stejná	stejný	k2eAgNnPc4d1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podrobnostech	podrobnost	k1gFnPc6
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
lišit	lišit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
rozdíl	rozdíl	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
počítání	počítání	k1gNnSc6
bodů	bod	k1gInPc2
mezi	mezi	k7c7
japonskými	japonský	k2eAgNnPc7d1
a	a	k8xC
čínskými	čínský	k2eAgNnPc7d1
pravidly	pravidlo	k1gNnPc7
(	(	kIx(
<g/>
česká	český	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
vycházejí	vycházet	k5eAaImIp3nP
z	z	k7c2
japonských	japonský	k2eAgFnPc2d1
s	s	k7c7
mírnou	mírný	k2eAgFnSc7d1
odlišností	odlišnost	k1gFnSc7
v	v	k7c6
řešení	řešení	k1gNnSc6
sporných	sporný	k2eAgFnPc2d1
situací	situace	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drobné	drobný	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
jsou	být	k5eAaImIp3nP
také	také	k9
v	v	k7c6
určení	určení	k1gNnSc6
bodů	bod	k1gInPc2
komi	kom	k1gFnSc2
<g/>
,	,	kIx,
možnosti	možnost	k1gFnSc2
remízy	remíza	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
řešení	řešení	k1gNnSc1
některých	některý	k3yIgFnPc2
speciálních	speciální	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
hry	hra	k1gFnSc2
jsou	být	k5eAaImIp3nP
však	však	k9
tyto	tento	k3xDgInPc1
rozdíly	rozdíl	k1gInPc1
obvykle	obvykle	k6eAd1
nepodstatné	podstatný	k2eNgInPc1d1
a	a	k8xC
projeví	projevit	k5eAaPmIp3nS
se	se	k3xPyFc4
jen	jen	k9
vzácně	vzácně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
<g/>
,	,	kIx,
dané	daný	k2eAgInPc1d1
sportovními	sportovní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
v	v	k7c6
systému	systém	k1gInSc6
turnajů	turnaj	k1gInPc2
a	a	k8xC
kritérií	kritérion	k1gNnPc2
pro	pro	k7c4
získávání	získávání	k1gNnSc4
tříd	třída	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Gejši	gejša	k1gFnPc4
hrající	hrající	k2eAgFnPc4d1
go	go	k?
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
go	go	k?
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc1
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřilo	patřit	k5eAaImAgNnS
mezi	mezi	k7c4
čtyři	čtyři	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
lidské	lidský	k2eAgFnPc4d1
dovednosti	dovednost	k1gFnPc4
spolu	spolu	k6eAd1
s	s	k7c7
poezií	poezie	k1gFnSc7
<g/>
,	,	kIx,
malířstvím	malířství	k1gNnSc7
a	a	k8xC
hudbou	hudba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
učení	učení	k1gNnSc2
taoistů	taoista	k1gMnPc2
je	být	k5eAaImIp3nS
go	go	k?
prostředkem	prostředek	k1gInSc7
k	k	k7c3
poznání	poznání	k1gNnSc3
sama	sám	k3xTgMnSc4
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
<g/>
l.	l.	k?
se	se	k3xPyFc4
hra	hra	k1gFnSc1
přenesla	přenést	k5eAaPmAgFnS
do	do	k7c2
Koreje	Korea	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
zábavou	zábava	k1gFnSc7
pro	pro	k7c4
vyšší	vysoký	k2eAgFnPc4d2
třídy	třída	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
go	go	k?
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zpočátku	zpočátku	k6eAd1
rozvíjelo	rozvíjet	k5eAaImAgNnS
v	v	k7c6
klášterech	klášter	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
rychle	rychle	k6eAd1
se	se	k3xPyFc4
rozšířilo	rozšířit	k5eAaPmAgNnS
i	i	k9
mezi	mezi	k7c7
aristokracií	aristokracie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
go	go	k?
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
nastal	nastat	k5eAaPmAgInS
od	od	k7c2
počátku	počátek	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
go	go	k?
stalo	stát	k5eAaPmAgNnS
šógunem	šógun	k1gInSc7
oficiálně	oficiálně	k6eAd1
podporovanou	podporovaný	k2eAgFnSc7d1
hrou	hra	k1gFnSc7
a	a	k8xC
nejlepší	dobrý	k2eAgMnPc1d3
hráči	hráč	k1gMnPc1
go	go	k?
měli	mít	k5eAaImAgMnP
šógunátem	šógunát	k1gInSc7
zajištěný	zajištěný	k2eAgInSc4d1
příjem	příjem	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
byl	být	k5eAaImAgInS
dán	dát	k5eAaPmNgInS
základ	základ	k1gInSc1
profesionálnímu	profesionální	k2eAgInSc3d1
go	go	k?
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
go	go	k?
vrátilo	vrátit	k5eAaPmAgNnS
na	na	k7c6
výsluní	výsluní	k1gNnSc6
i	i	k8xC
v	v	k7c6
Číně	Čína	k1gFnSc6
a	a	k8xC
Jižní	jižní	k2eAgFnSc3d1
Koreji	Korea	k1gFnSc3
a	a	k8xC
hráči	hráč	k1gMnSc3
těchto	tento	k3xDgInPc2
3	#num#	k4
států	stát	k1gInPc2
jsou	být	k5eAaImIp3nP
dnes	dnes	k6eAd1
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
špičku	špička	k1gFnSc4
světového	světový	k2eAgInSc2d1
go	go	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
pak	pak	k6eAd1
hráči	hráč	k1gMnPc1
Koreje	Korea	k1gFnSc2
sklízejí	sklízet	k5eAaImIp3nP
na	na	k7c6
mezinárodním	mezinárodní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
letech	let	k1gInPc6
hodně	hodně	k6eAd1
vavřínů	vavřín	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byly	být	k5eAaImAgInP
vytvořeny	vytvořen	k2eAgInPc1d1
počítačové	počítačový	k2eAgInPc1d1
programy	program	k1gInPc1
<g/>
,	,	kIx,
schopne	schopnout	k5eAaPmIp3nS
porážet	porážet	k5eAaImF
lidské	lidský	k2eAgMnPc4d1
mistry	mistr	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevyužívají	využívat	k5eNaImIp3nP
jen	jen	k9
řešení	řešení	k1gNnSc4
hrubou	hrubý	k2eAgFnSc7d1
silou	síla	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
roli	role	k1gFnSc4
hraje	hrát	k5eAaImIp3nS
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2015	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
<g/>
)	)	kIx)
vyhrál	vyhrát	k5eAaPmAgMnS
v	v	k7c6
Londýně	Londýn	k1gInSc6
program	program	k1gInSc1
AlphaGo	AlphaGo	k6eAd1
(	(	kIx(
<g/>
výsledek	výsledek	k1gInSc1
ale	ale	k8xC
zveřejnil	zveřejnit	k5eAaPmAgInS
časopis	časopis	k1gInSc1
Nature	Natur	k1gMnSc5
až	až	k9
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
nejen	nejen	k6eAd1
nad	nad	k7c7
konkurenčními	konkurenční	k2eAgMnPc7d1
softwarovými	softwarový	k2eAgMnPc7d1
soupeři	soupeř	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
i	i	k8xC
nad	nad	k7c7
evropským	evropský	k2eAgMnSc7d1
šampionem	šampion	k1gMnSc7
Fan	Fana	k1gFnPc2
Huiem	Huiem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překvapivá	překvapivý	k2eAgFnSc1d1
výhra	výhra	k1gFnSc1
přišla	přijít	k5eAaPmAgFnS
dle	dle	k7c2
odborníku	odborník	k1gMnSc3
o	o	k7c4
minimálně	minimálně	k6eAd1
deset	deset	k4xCc4
let	léto	k1gNnPc2
dříve	dříve	k6eAd2
než	než	k8xS
bylo	být	k5eAaImAgNnS
očekáváno	očekávat	k5eAaImNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Výhra	výhra	k1gFnSc1
nad	nad	k7c7
světovým	světový	k2eAgMnSc7d1
šampionem	šampion	k1gMnSc7
přišla	přijít	k5eAaPmAgFnS
na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Filosofie	filosofie	k1gFnSc1
go	go	k?
</s>
<s>
Z	z	k7c2
filosofického	filosofický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
mentalitu	mentalita	k1gFnSc4
go	go	k?
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
vztah	vztah	k1gInSc1
k	k	k7c3
lidskému	lidský	k2eAgNnSc3d1
bytí	bytí	k1gNnSc3
snad	snad	k9
nejlépe	dobře	k6eAd3
vystihuje	vystihovat	k5eAaImIp3nS
pohled	pohled	k1gInSc1
Japonců	Japonec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
ho	on	k3xPp3gMnSc4
již	již	k6eAd1
od	od	k7c2
pradávna	pradávno	k1gNnSc2
zařadili	zařadit	k5eAaPmAgMnP
mezi	mezi	k7c4
tzv.	tzv.	kA
Tři	tři	k4xCgFnPc4
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
tj.	tj.	kA
3	#num#	k4
velké	velký	k2eAgFnPc4d1
<g/>
,	,	kIx,
základní	základní	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
:	:	kIx,
vrhcáby	vrhcáby	k1gInPc4
<g/>
,	,	kIx,
šógi	šóg	k1gFnPc4
a	a	k8xC
go	go	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
Tři	tři	k4xCgFnPc1
hry	hra	k1gFnPc1
spolu	spolu	k6eAd1
představují	představovat	k5eAaImIp3nP
ucelený	ucelený	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
svět	svět	k1gInSc4
<g/>
,	,	kIx,
představují	představovat	k5eAaImIp3nP
člověka	člověk	k1gMnSc4
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
třech	tři	k4xCgInPc6
základních	základní	k2eAgInPc6d1
životních	životní	k2eAgInPc6d1
postojích	postoj	k1gInPc6
–	–	k?
vztazích	vztah	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgMnSc1
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
vzdáleným	vzdálený	k2eAgMnPc3d1
bohům	bůh	k1gMnPc3
a	a	k8xC
záhadným	záhadný	k2eAgFnPc3d1
silám	síla	k1gFnPc3
vesmíru	vesmír	k1gInSc2
<g/>
:	:	kIx,
vrhcáby	vrhcáby	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
hraje	hrát	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
osud	osud	k1gInSc1
a	a	k8xC
náhoda	náhoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
nich	on	k3xPp3gNnPc6
hrají	hrát	k5eAaImIp3nP
kostky	kostka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
hráč	hráč	k1gMnSc1
neovlivní	ovlivnit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
představuje	představovat	k5eAaImIp3nS
člověka	člověk	k1gMnSc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
si	se	k3xPyFc3
kolem	kolem	k6eAd1
sebe	sebe	k3xPyFc4
vytvořil	vytvořit	k5eAaPmAgInS
<g/>
:	:	kIx,
šógi	šóge	k1gFnSc4
<g/>
,	,	kIx,
odrážející	odrážející	k2eAgInPc4d1
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
kamenech	kámen	k1gInPc6
hierarchickou	hierarchický	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
armády	armáda	k1gFnSc2
a	a	k8xC
krveprolití	krveprolití	k1gNnSc2
s	s	k7c7
konečným	konečný	k2eAgInSc7d1
cílem	cíl	k1gInSc7
zničit	zničit	k5eAaPmF
nepřátelského	přátelský	k2eNgMnSc4d1
krále	král	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
A	A	kA
potřetí	potřetí	k4xO
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
konečně	konečně	k6eAd1
člověk	člověk	k1gMnSc1
sám	sám	k3xTgMnSc1
se	s	k7c7
sebou	se	k3xPyFc7
<g/>
:	:	kIx,
go	go	k?
–	–	k?
hra	hra	k1gFnSc1
orientovaná	orientovaný	k2eAgFnSc1d1
ne	ne	k9
na	na	k7c4
ničení	ničení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
budování	budování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
kameny	kámen	k1gInPc1
mají	mít	k5eAaImIp3nP
stejnou	stejný	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
síla	síla	k1gFnSc1
záleží	záležet	k5eAaImIp3nS
na	na	k7c4
jejich	jejich	k3xOp3gNnSc4
umístění	umístění	k1gNnSc4
v	v	k7c6
konkrétní	konkrétní	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kameny	kámen	k1gInPc7
se	se	k3xPyFc4
spojují	spojovat	k5eAaImIp3nP
do	do	k7c2
skupin	skupina	k1gFnPc2
a	a	k8xC
ty	ten	k3xDgMnPc4
pak	pak	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
organické	organický	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
buď	buď	k8xC
přežijí	přežít	k5eAaPmIp3nP
<g/>
,	,	kIx,
nebo	nebo	k8xC
zahynou	zahynout	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
hry	hra	k1gFnSc2
na	na	k7c6
desce	deska	k1gFnSc6
proběhne	proběhnout	k5eAaPmIp3nS
spousta	spousta	k1gFnSc1
proměn	proměna	k1gFnPc2
–	–	k?
vzestupů	vzestup	k1gInPc2
a	a	k8xC
pádů	pád	k1gInPc2
<g/>
,	,	kIx,
malých	malý	k2eAgFnPc2d1
proher	prohra	k1gFnPc2
a	a	k8xC
dočasných	dočasný	k2eAgNnPc2d1
vítězství	vítězství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
překonat	překonat	k5eAaPmF
běžné	běžný	k2eAgMnPc4d1
lidské	lidský	k2eAgMnPc4d1
nepřátele	nepřítel	k1gMnPc4
jako	jako	k8xS,k8xC
chamtivost	chamtivost	k1gFnSc4
<g/>
,	,	kIx,
hněv	hněv	k1gInSc4
a	a	k8xC
hloupost	hloupost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Turnaje	turnaj	k1gInPc1
a	a	k8xC
třídy	třída	k1gFnPc1
</s>
<s>
Třídy	třída	k1gFnPc1
</s>
<s>
Na	na	k7c6
turnajích	turnaj	k1gInPc6
a	a	k8xC
na	na	k7c6
internetových	internetový	k2eAgInPc6d1
serverech	server	k1gInPc6
je	být	k5eAaImIp3nS
hráči	hráč	k1gMnPc1
podle	podle	k7c2
jeho	jeho	k3xOp3gInPc2
výsledků	výsledek	k1gInPc2
vypočtena	vypočíst	k5eAaPmNgFnS
jeho	jeho	k3xOp3gFnSc1
třída	třída	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
herní	herní	k2eAgFnSc1d1
zdatnost	zdatnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učňovské	učňovský	k2eAgFnPc4d1
třídy	třída	k1gFnPc4
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
kjú	kjú	k?
a	a	k8xC
mistrovské	mistrovský	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
dan	dan	k?
<g/>
.	.	kIx.
</s>
<s>
Nejslabší	slabý	k2eAgMnSc1d3
z	z	k7c2
tříd	třída	k1gFnPc2
kjú	kjú	k?
je	být	k5eAaImIp3nS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
kjú	kjú	k?
(	(	kIx(
<g/>
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
30	#num#	k4
<g/>
.	.	kIx.
kjú	kjú	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsilnější	silný	k2eAgInSc4d3
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
1	#num#	k4
<g/>
.	.	kIx.
kjú	kjú	k?
<g/>
.	.	kIx.
</s>
<s>
Ihned	ihned	k6eAd1
nad	nad	k7c7
prvním	první	k4xOgInSc7
kjú	kjú	k?
je	být	k5eAaImIp3nS
1	#num#	k4
<g/>
.	.	kIx.
amatérský	amatérský	k2eAgInSc1d1
dan	dan	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsilnější	silný	k2eAgInSc4d3
z	z	k7c2
amatérských	amatérský	k2eAgInPc2d1
danů	dan	k1gInPc2
je	být	k5eAaImIp3nS
8	#num#	k4
<g/>
.	.	kIx.
<g/>
amatérský	amatérský	k2eAgInSc1d1
dan	dan	k?
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
odpovídá	odpovídat	k5eAaImIp3nS
síle	síla	k1gFnSc6
několika	několik	k4yIc2
nejsilnějších	silný	k2eAgMnPc2d3
evropských	evropský	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
</s>
<s>
Výjimečně	výjimečně	k6eAd1
silní	silný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
Asii	Asie	k1gFnSc6
mohou	moct	k5eAaImIp3nP
pokusit	pokusit	k5eAaPmF
získat	získat	k5eAaPmF
titul	titul	k1gInSc4
tzv.	tzv.	kA
profesionálního	profesionální	k2eAgMnSc4d1
hráče	hráč	k1gMnSc4
go	go	k?
<g/>
;	;	kIx,
jejich	jejich	k3xOp3gFnSc1
síla	síla	k1gFnSc1
se	se	k3xPyFc4
měří	měřit	k5eAaImIp3nS
od	od	k7c2
prvního	první	k4xOgInSc2
(	(	kIx(
<g/>
nejslabšího	slabý	k2eAgInSc2d3
<g/>
)	)	kIx)
do	do	k7c2
devátého	devátý	k4xOgInSc2
profesionálního	profesionální	k2eAgInSc2d1
danu	danus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
profesionální	profesionální	k2eAgFnSc1d1
liga	liga	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
první	první	k4xOgMnSc1
hráč	hráč	k1gMnSc1
získal	získat	k5eAaPmAgMnS
třídu	třída	k1gFnSc4
1	#num#	k4
pro	pro	k7c4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
Evropa	Evropa	k1gFnSc1
7	#num#	k4
profesionálních	profesionální	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Ilya	Ilya	k1gFnSc1
Shikshin	Shikshin	k1gInSc1
4	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
RU	RU	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Artem	Artem	k1gInSc1
Kachanovskyi	Kachanovskyi	k1gNnSc1
2	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
UA	UA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mateusz	Mateusz	k1gInSc1
Surma	surma	k1gFnSc1
2	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
PL	PL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pavol	Pavol	k1gInSc1
Lisy	Lisa	k1gFnSc2
2	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
SK	Sk	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ali	Ali	k1gFnSc1
Jabarin	Jabarin	k1gInSc1
2	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
IL	IL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Andrii	Andrie	k1gFnSc4
Kravets	Kravetsa	k1gFnPc2
1	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
UA	UA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tanguy	Tangua	k1gFnPc4
Le	Le	k1gFnSc2
Calvé	Calvý	k2eAgFnSc2d1
1	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
FR	fr	k0
<g/>
)	)	kIx)
</s>
<s>
Rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
evropskými	evropský	k2eAgFnPc7d1
a	a	k8xC
světovými	světový	k2eAgFnPc7d1
špičkami	špička	k1gFnPc7
se	se	k3xPyFc4
snižuje	snižovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několika	několik	k4yIc3
Evropanům	Evropan	k1gMnPc3
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
titul	titul	k1gInSc4
profesionála	profesionál	k1gMnSc4
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
:	:	kIx,
Catalin	Catalin	k1gInSc1
Taranu	taran	k1gInSc2
(	(	kIx(
<g/>
RO	RO	kA
<g/>
,	,	kIx,
5	#num#	k4
<g/>
p	p	k?
Nihon	Nihon	k1gMnSc1
Ki-in	Ki-in	k1gMnSc1
<g/>
,	,	kIx,
Japan	japan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alexandr	Alexandr	k1gMnSc1
Dinerchtein	Dinerchtein	k1gMnSc1
(	(	kIx(
<g/>
RU	RU	kA
<g/>
,	,	kIx,
3	#num#	k4
<g/>
p	p	k?
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Svetlana	Svetlana	k1gFnSc1
Shikshina	Shikshina	k1gMnSc1
(	(	kIx(
<g/>
RU	RU	kA
<g/>
,	,	kIx,
3	#num#	k4
<g/>
p	p	k?
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Diana	Diana	k1gFnSc1
Koszegi	Koszeg	k1gFnSc2
(	(	kIx(
<g/>
HU	hu	k0
<g/>
,	,	kIx,
1	#num#	k4
<g/>
p	p	k?
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
Mariya	Mariya	k1gFnSc1
Zakharchenko	Zakharchenka	k1gFnSc5
(	(	kIx(
<g/>
UA	UA	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
p	p	k?
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Antti	Antti	k1gNnSc1
Törmänen	Törmänna	k1gFnPc2
(	(	kIx(
<g/>
FI	fi	k0
<g/>
,	,	kIx,
1	#num#	k4
<g/>
p	p	k?
Nihon	Nihon	k1gMnSc1
Ki-in	Ki-in	k1gMnSc1
<g/>
,	,	kIx,
Japan	japan	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Třídy	třída	k1gFnPc1
nejsou	být	k5eNaImIp3nP
celosvětově	celosvětově	k6eAd1
sjednoceny	sjednocen	k2eAgInPc1d1
<g/>
:	:	kIx,
pokud	pokud	k8xS
nějaký	nějaký	k3yIgMnSc1
hráč	hráč	k1gMnSc1
získal	získat	k5eAaPmAgMnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
třídu	třída	k1gFnSc4
6	#num#	k4
<g/>
.	.	kIx.
kjú	kjú	k?
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
stejně	stejně	k6eAd1
silný	silný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
mít	mít	k5eAaImF
o	o	k7c4
několik	několik	k4yIc4
čísel	číslo	k1gNnPc2
jinou	jiný	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
a	a	k8xC
hráč	hráč	k1gMnSc1
na	na	k7c6
internetových	internetový	k2eAgInPc6d1
serverech	server	k1gInPc6
ještě	ještě	k6eAd1
jinou	jiný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Turnaje	turnaj	k1gInPc1
</s>
<s>
Prázdná	prázdný	k2eAgFnSc1d1
deska	deska	k1gFnSc1
go	go	k?
–	–	k?
tzv.	tzv.	kA
Goban	Goban	k1gMnSc1
</s>
<s>
Většina	většina	k1gFnSc1
turnajů	turnaj	k1gInPc2
v	v	k7c6
go	go	k?
je	být	k5eAaImIp3nS
přístupná	přístupný	k2eAgFnSc1d1
každému	každý	k3xTgMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
nezařazeným	zařazený	k2eNgMnPc3d1
hráčům	hráč	k1gMnPc3
je	být	k5eAaImIp3nS
udělena	udělen	k2eAgFnSc1d1
třída	třída	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
kjú	kjú	k?
a	a	k8xC
ta	ten	k3xDgFnSc1
dále	daleko	k6eAd2
roste	růst	k5eAaImIp3nS
a	a	k8xC
případně	případně	k6eAd1
i	i	k9
klesá	klesat	k5eAaImIp3nS
podle	podle	k7c2
hráčových	hráčův	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
(	(	kIx(
<g/>
ovšem	ovšem	k9
neklesne	klesnout	k5eNaPmIp3nS
pod	pod	k7c7
20	#num#	k4
<g/>
.	.	kIx.
kjú	kjú	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Typický	typický	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
v	v	k7c6
českých	český	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
přes	přes	k7c4
víkend	víkend	k1gInSc4
na	na	k7c4
pět	pět	k4xCc4
nebo	nebo	k8xC
šest	šest	k4xCc4
partií	partie	k1gFnPc2
a	a	k8xC
partie	partie	k1gFnPc1
trvají	trvat	k5eAaImIp3nP
kolem	kolem	k7c2
dvou	dva	k4xCgInPc2
a	a	k8xC
půl	půl	k1xP
hodiny	hodina	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
níže	níže	k1gFnSc2
„	„	k?
<g/>
Časové	časový	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
turnajů	turnaj	k1gInPc2
je	být	k5eAaImIp3nS
organizována	organizovat	k5eAaBmNgFnS
systémem	systém	k1gInSc7
McMahon	McMahon	k1gMnSc1
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
hráči	hráč	k1gMnPc1
zpočátku	zpočátku	k6eAd1
hrají	hrát	k5eAaImIp3nP
s	s	k7c7
protivníky	protivník	k1gMnPc7
stejné	stejná	k1gFnSc2
nebo	nebo	k8xC
podobné	podobný	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
a	a	k8xC
hráči	hráč	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
prohraje	prohrát	k5eAaPmIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
příští	příští	k2eAgFnSc6d1
partii	partie	k1gFnSc6
přiřazen	přiřadit	k5eAaPmNgMnS
slabší	slabý	k2eAgMnSc1d2
protivník	protivník	k1gMnSc1
<g/>
,	,	kIx,
vítězi	vítěz	k1gMnSc6
naopak	naopak	k6eAd1
silnější	silný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
mj.	mj.	kA
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
hráč	hráč	k1gMnSc1
rychle	rychle	k6eAd1
pozná	poznat	k5eAaPmIp3nS
svoji	svůj	k3xOyFgFnSc4
skutečnou	skutečný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Časové	časový	k2eAgInPc1d1
systémy	systém	k1gInPc1
</s>
<s>
Na	na	k7c6
turnajích	turnaj	k1gInPc6
v	v	k7c6
go	go	k?
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
na	na	k7c6
šachových	šachový	k2eAgFnPc6d1
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
na	na	k7c4
hru	hra	k1gFnSc4
omezený	omezený	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
měří	měřit	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
šachovými	šachový	k2eAgFnPc7d1
hodinami	hodina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
tyto	tento	k3xDgInPc4
systémy	systém	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Absolutní	absolutní	k2eAgInSc1d1
čas	čas	k1gInSc1
–	–	k?
každý	každý	k3xTgMnSc1
hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
spotřebovat	spotřebovat	k5eAaPmF
na	na	k7c4
hru	hra	k1gFnSc4
daný	daný	k2eAgInSc4d1
čas	čas	k1gInSc4
a	a	k8xC
pokud	pokud	k8xS
jej	on	k3xPp3gMnSc4
překročí	překročit	k5eAaPmIp3nS
<g/>
,	,	kIx,
prohrál	prohrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Standardní	standardní	k2eAgNnSc1d1
(	(	kIx(
<g/>
japonské	japonský	k2eAgNnSc4d1
<g/>
)	)	kIx)
bjó-jomi	bjó-jo	k1gFnPc7
(	(	kIx(
<g/>
秒	秒	k?
<g/>
)	)	kIx)
–	–	k?
například	například	k6eAd1
60	#num#	k4
minut	minuta	k1gFnPc2
plus	plus	k6eAd1
3	#num#	k4
<g/>
×	×	k?
<g/>
30	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyčerpání	vyčerpání	k1gNnSc6
60	#num#	k4
minut	minuta	k1gFnPc2
má	mít	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
30	#num#	k4
sekund	sekunda	k1gFnPc2
na	na	k7c4
tah	tah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těchto	tento	k3xDgFnPc2
30	#num#	k4
sekund	sekunda	k1gFnPc2
smí	smět	k5eAaImIp3nS
dvakrát	dvakrát	k6eAd1
za	za	k7c4
hru	hra	k1gFnSc4
překročit	překročit	k5eAaPmF
<g/>
,	,	kIx,
při	při	k7c6
třetím	třetí	k4xOgInSc6
překročení	překročení	k1gNnSc6
prohrál	prohrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Pokud	pokud	k8xS
spotřebuje	spotřebovat	k5eAaPmIp3nS
30	#num#	k4
až	až	k9
59	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
jedno	jeden	k4xCgNnSc4
překročení	překročení	k1gNnSc4
<g/>
;	;	kIx,
pokud	pokud	k8xS
60	#num#	k4
až	až	k9
89	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
pokládáno	pokládat	k5eAaImNgNnS
za	za	k7c4
dvojnásobné	dvojnásobný	k2eAgNnSc4d1
překročení	překročení	k1gNnSc4
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Kanadské	kanadský	k2eAgFnPc1d1
bjó-jomi	bjó-jo	k1gFnPc7
–	–	k?
například	například	k6eAd1
60	#num#	k4
minut	minuta	k1gFnPc2
plus	plus	k6eAd1
5	#num#	k4
minut	minuta	k1gFnPc2
na	na	k7c4
15	#num#	k4
tahů	tah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyčerpání	vyčerpání	k1gNnSc6
60	#num#	k4
minut	minuta	k1gFnPc2
musí	muset	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
na	na	k7c4
nejbližších	blízký	k2eAgNnPc2d3
15	#num#	k4
tahů	tah	k1gInPc2
spotřebovat	spotřebovat	k5eAaPmF
nejvýše	vysoce	k6eAd3,k6eAd1
5	#num#	k4
minut	minuta	k1gFnPc2
<g/>
;	;	kIx,
na	na	k7c4
dalších	další	k2eAgFnPc2d1
15	#num#	k4
tahů	tah	k1gInPc2
také	také	k9
nejvýše	vysoce	k6eAd3,k6eAd1
5	#num#	k4
minut	minuta	k1gFnPc2
atd.	atd.	kA
</s>
<s>
Fischerův	Fischerův	k2eAgInSc1d1
čas	čas	k1gInSc1
–	–	k?
například	například	k6eAd1
45	#num#	k4
minut	minuta	k1gFnPc2
plus	plus	k6eAd1
20	#num#	k4
sekund	sekunda	k1gFnPc2
bonus	bonus	k1gInSc4
na	na	k7c4
každý	každý	k3xTgInSc4
zahraný	zahraný	k2eAgInSc4d1
tah	tah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
se	se	k3xPyFc4
nedohrává	dohrávat	k5eNaImIp3nS
ve	v	k7c6
dvou	dva	k4xCgInPc6
fázích	fág	k1gInPc6
jako	jako	k9
u	u	k7c2
japonského	japonský	k2eAgMnSc2d1
nebo	nebo	k8xC
kanadského	kanadský	k2eAgMnSc2d1
bjó-jomi	bjó-jo	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
jako	jako	k9
jedna	jeden	k4xCgFnSc1
fáze	fáze	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
to	ten	k3xDgNnSc1
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
systémech	systém	k1gInPc6
nazýváno	nazýván	k2eAgNnSc4d1
hlavní	hlavní	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
spíše	spíše	k9
časová	časový	k2eAgFnSc1d1
rezerva	rezerva	k1gFnSc1
na	na	k7c4
promýšlení	promýšlení	k1gNnSc4
složitějších	složitý	k2eAgFnPc2d2
situací	situace	k1gFnPc2
na	na	k7c6
desce	deska	k1gFnSc6
nad	nad	k7c4
základní	základní	k2eAgInSc4d1
čas	čas	k1gInSc4
určený	určený	k2eAgInSc4d1
na	na	k7c4
jeden	jeden	k4xCgInSc4
tah	tah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Přikupování	přikupování	k1gNnSc1
času	čas	k1gInSc2
–	–	k?
hráč	hráč	k1gMnSc1
za	za	k7c4
zvýšení	zvýšení	k1gNnSc4
času	čas	k1gInSc2
platí	platit	k5eAaImIp3nP
svými	svůj	k3xOyFgInPc7
body	bod	k1gInPc7
v	v	k7c6
závěrečném	závěrečný	k2eAgNnSc6d1
počítání	počítání	k1gNnSc6
skóre	skóre	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
má	mít	k5eAaImIp3nS
na	na	k7c4
hru	hra	k1gFnSc4
60	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
poté	poté	k6eAd1
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
dvakrát	dvakrát	k6eAd1
přikoupit	přikoupit	k5eAaPmF
pět	pět	k4xCc4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
za	za	k7c4
každé	každý	k3xTgNnSc4
přikoupení	přikoupení	k1gNnSc4
jsou	být	k5eAaImIp3nP
mu	on	k3xPp3gNnSc3
strženy	stržen	k2eAgInPc1d1
tři	tři	k4xCgInPc1
body	bod	k1gInPc1
z	z	k7c2
výsledného	výsledný	k2eAgNnSc2d1
skóre	skóre	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
českých	český	k2eAgInPc6d1
turnajích	turnaj	k1gInPc6
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
používá	používat	k5eAaImIp3nS
japonské	japonský	k2eAgNnSc4d1
nebo	nebo	k8xC
kanadské	kanadský	k2eAgNnSc4d1
bjó-jomi	bjó-jo	k1gFnPc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
základní	základní	k2eAgInSc1d1
čas	čas	k1gInSc1
bývá	bývat	k5eAaImIp3nS
60	#num#	k4
nebo	nebo	k8xC
75	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
parametry	parametr	k1gInPc4
bjó-jomi	bjó-jo	k1gFnPc7
jsou	být	k5eAaImIp3nP
stejné	stejné	k1gNnSc1
jako	jako	k8xS,k8xC
v	v	k7c6
uvedených	uvedený	k2eAgInPc6d1
příkladech	příklad	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořádají	pořádat	k5eAaImIp3nP
se	se	k3xPyFc4
též	též	k9
bleskové	bleskový	k2eAgInPc1d1
turnaje	turnaj	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hráč	hráč	k1gMnSc1
má	mít	k5eAaImIp3nS
absolutní	absolutní	k2eAgInSc4d1
čas	čas	k1gInSc4
například	například	k6eAd1
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
hráč	hráč	k1gMnSc1
hospodaří	hospodařit	k5eAaImIp3nS
s	s	k7c7
časem	čas	k1gInSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
jeho	jeho	k3xOp3gFnSc2
strategie	strategie	k1gFnSc2
a	a	k8xC
dovednosti	dovednost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesionální	profesionální	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
obvykle	obvykle	k6eAd1
spotřebují	spotřebovat	k5eAaPmIp3nP
většinu	většina	k1gFnSc4
času	čas	k1gInSc2
na	na	k7c4
první	první	k4xOgFnSc4
třetinu	třetina	k1gFnSc4
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
ta	ten	k3xDgFnSc1
výrazně	výrazně	k6eAd1
ovlivní	ovlivnit	k5eAaPmIp3nS
celý	celý	k2eAgInSc4d1
zbytek	zbytek	k1gInSc4
partie	partie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
systémem	systém	k1gInSc7
japonského	japonský	k2eAgNnSc2d1
bjó-jomi	bjó-jo	k1gFnPc7
<g/>
,	,	kIx,
hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
zvýšit	zvýšit	k5eAaPmF
celkový	celkový	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
partii	partie	k1gFnSc6
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
přejde	přejít	k5eAaPmIp3nS
do	do	k7c2
bjó-jomi	bjó-jo	k1gFnPc7
relativně	relativně	k6eAd1
brzy	brzy	k6eAd1
v	v	k7c6
partii	partie	k1gFnSc6
<g/>
,	,	kIx,
ovšem	ovšem	k9
potom	potom	k6eAd1
nemůže	moct	k5eNaImIp3nS
nad	nad	k7c7
důležitým	důležitý	k2eAgInSc7d1
tahem	tah	k1gInSc7
přemýšlet	přemýšlet	k5eAaImF
dlouho	dlouho	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
též	též	k9
začíná	začínat	k5eAaImIp3nS
<g/>
,	,	kIx,
čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
dál	daleko	k6eAd2
častěji	často	k6eAd2
používat	používat	k5eAaImF
na	na	k7c6
turnajích	turnaj	k1gInPc6
i	i	k8xC
Fischerův	Fischerův	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jeho	jeho	k3xOp3gNnSc6
použití	použití	k1gNnSc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
hráče	hráč	k1gMnPc4
mnohem	mnohem	k6eAd1
jednoduší	jednoduchý	k2eAgMnPc1d1
udržet	udržet	k5eAaPmF
si	se	k3xPyFc3
přehled	přehled	k1gInSc4
o	o	k7c6
spotřebovaném	spotřebovaný	k2eAgInSc6d1
a	a	k8xC
zbývajícím	zbývající	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
málokdy	málokdy	k6eAd1
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
do	do	k7c2
neřešitelné	řešitelný	k2eNgFnSc2d1
časové	časový	k2eAgFnSc2d1
tísně	tíseň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Handicapové	handicapový	k2eAgFnPc1d1
partie	partie	k1gFnPc1
</s>
<s>
Pokud	pokud	k8xS
si	se	k3xPyFc3
chtějí	chtít	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
rozdílné	rozdílný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
zahrát	zahrát	k5eAaPmF
vyrovnanou	vyrovnaný	k2eAgFnSc4d1
hru	hra	k1gFnSc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
si	se	k3xPyFc3
dohodnout	dohodnout	k5eAaPmF
tzv.	tzv.	kA
handicap	handicap	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
dva	dva	k4xCgInPc4
až	až	k6eAd1
devět	devět	k4xCc4
kamenů	kámen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
si	se	k3xPyFc3
před	před	k7c7
hrou	hra	k1gFnSc7
černý	černý	k2eAgInSc1d1
položí	položit	k5eAaPmIp3nS
na	na	k7c4
desku	deska	k1gFnSc4
(	(	kIx(
<g/>
potom	potom	k8xC
první	první	k4xOgFnSc4
hraje	hrát	k5eAaImIp3nS
bílý	bílý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
většiny	většina	k1gFnSc2
pravidel	pravidlo	k1gNnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
japonských	japonský	k2eAgInPc2d1
<g/>
)	)	kIx)
se	se	k3xPyFc4
handicapové	handicapový	k2eAgInPc1d1
kameny	kámen	k1gInPc1
musí	muset	k5eAaImIp3nP
umístit	umístit	k5eAaPmF
na	na	k7c4
přesně	přesně	k6eAd1
dané	daný	k2eAgInPc4d1
body	bod	k1gInPc4
(	(	kIx(
<g/>
těchto	tento	k3xDgInPc2
9	#num#	k4
bodů	bod	k1gInPc2
je	být	k5eAaImIp3nS
na	na	k7c6
desce	deska	k1gFnSc6
zvýrazněno	zvýrazněn	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiná	jiná	k1gFnSc1
pravidla	pravidlo	k1gNnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
čínská	čínský	k2eAgFnSc1d1
<g/>
)	)	kIx)
povolují	povolovat	k5eAaImIp3nP
černému	černé	k1gNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gNnSc4
umístil	umístit	k5eAaPmAgMnS
podle	podle	k7c2
své	svůj	k3xOyFgFnSc2
volby	volba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1
ve	v	k7c6
třídě	třída	k1gFnSc6
kjú	kjú	k?
nebo	nebo	k8xC
amatérský	amatérský	k2eAgInSc1d1
dan	dan	k?
odpovídá	odpovídat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
jednomu	jeden	k4xCgNnSc3
handicapovému	handicapový	k2eAgInSc3d1
kameni	kámen	k1gInSc3
(	(	kIx(
<g/>
např.	např.	kA
osmé	osmý	k4xOgNnSc4
kjú	kjú	k?
by	by	kYmCp3nS
si	se	k3xPyFc3
mělo	mít	k5eAaImAgNnS
vzít	vzít	k5eAaPmF
pětihandicap	pětihandicap	k1gInSc4
proti	proti	k7c3
třetímu	třetí	k4xOgMnSc3
kjú	kjú	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
šance	šance	k1gFnPc1
byly	být	k5eAaImAgFnP
vyrovnané	vyrovnaný	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
ve	v	k7c6
třídě	třída	k1gFnSc6
profesionálního	profesionální	k2eAgInSc2d1
danu	danus	k1gInSc2
odpovídá	odpovídat	k5eAaImIp3nS
třetině	třetina	k1gFnSc3
nebo	nebo	k8xC
čtvrtině	čtvrtina	k1gFnSc3
kamene	kámen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
rozdílně	rozdílně	k6eAd1
silní	silný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
zahráli	zahrát	k5eAaPmAgMnP
vyrovnanou	vyrovnaný	k2eAgFnSc4d1
hru	hra	k1gFnSc4
<g/>
,	,	kIx,
lze	lze	k6eAd1
místo	místo	k7c2
handicapu	handicap	k1gInSc2
použít	použít	k5eAaPmF
též	též	k9
reverzní	reverzní	k2eAgFnPc4d1
komi	kom	k1gFnPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
komi	komi	k6eAd1
ve	v	k7c4
prospěch	prospěch	k1gInSc4
černého	černé	k1gNnSc2
(	(	kIx(
<g/>
ve	v	k7c4
výši	výše	k1gFnSc4
několika	několik	k4yIc2
desítek	desítka	k1gFnPc2
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
rozdílu	rozdíl	k1gInSc6
v	v	k7c6
síle	síla	k1gFnSc6
hráčů	hráč	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dohraná	dohraný	k2eAgFnSc1d1
hra	hra	k1gFnSc1
na	na	k7c6
desce	deska	k1gFnSc6
9	#num#	k4
<g/>
×	×	k?
<g/>
9	#num#	k4
</s>
<s>
Strategie	strategie	k1gFnSc1
a	a	k8xC
taktika	taktika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Strategie	strategie	k1gFnSc1
a	a	k8xC
taktika	taktika	k1gFnSc1
ve	v	k7c6
hře	hra	k1gFnSc6
go	go	k?
<g/>
.	.	kIx.
</s>
<s>
Desková	deskový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
go	go	k?
má	mít	k5eAaImIp3nS
přes	přes	k7c4
jednoduchost	jednoduchost	k1gFnSc4
pravidel	pravidlo	k1gNnPc2
hlubokou	hluboký	k2eAgFnSc4d1
strategii	strategie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesionální	profesionální	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
věnují	věnovat	k5eAaPmIp3nP,k5eAaImIp3nP
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
jejímu	její	k3xOp3gNnSc3
studiu	studio	k1gNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
dosáhli	dosáhnout	k5eAaPmAgMnP
zvýšení	zvýšení	k1gNnSc2
úrovně	úroveň	k1gFnSc2
své	svůj	k3xOyFgFnSc2
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
musí	muset	k5eAaImIp3nS
volit	volit	k5eAaImF
své	svůj	k3xOyFgInPc4
tahy	tah	k1gInPc4
s	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
protichůdným	protichůdný	k2eAgInPc3d1
zájmům	zájem	k1gInPc3
<g/>
,	,	kIx,
například	například	k6eAd1
kladením	kladení	k1gNnSc7
kamenů	kámen	k1gInPc2
v	v	k7c6
zahájení	zahájení	k1gNnSc6
daleko	daleko	k6eAd1
od	od	k7c2
sebe	sebe	k3xPyFc4
si	se	k3xPyFc3
hráč	hráč	k1gMnSc1
vytyčí	vytyčit	k5eAaPmIp3nS
větší	veliký	k2eAgNnSc4d2
území	území	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
to	ten	k3xDgNnSc4
menší	malý	k2eAgFnSc1d2
má	mít	k5eAaImIp3nS
jistotu	jistota	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
uhájí	uhájit	k5eAaPmIp3nS
proti	proti	k7c3
invazím	invaze	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladení	kladení	k1gNnSc1
kamenů	kámen	k1gInPc2
na	na	k7c6
třetí	třetí	k4xOgFnSc6
linii	linie	k1gFnSc6
lépe	dobře	k6eAd2
zabezpečí	zabezpečit	k5eAaPmIp3nS
území	území	k1gNnSc4
při	při	k7c6
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
dává	dávat	k5eAaImIp3nS
menší	malý	k2eAgFnSc4d2
výhodu	výhoda	k1gFnSc4
(	(	kIx(
<g/>
vliv	vliv	k1gInSc4
<g/>
)	)	kIx)
pro	pro	k7c4
pozdější	pozdní	k2eAgInSc4d2
boj	boj	k1gInSc4
v	v	k7c6
centru	centrum	k1gNnSc6
<g/>
,	,	kIx,
než	než	k8xS
hra	hra	k1gFnSc1
na	na	k7c4
čtvrtou	čtvrtý	k4xOgFnSc4
linii	linie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
smrt	smrt	k1gFnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
je	být	k5eAaImIp3nS
nazvána	nazván	k2eAgFnSc1d1
živá	živý	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
může	moct	k5eAaImIp3nS
zabránit	zabránit	k5eAaPmF
svému	svůj	k3xOyFgNnSc3
zajmutí	zajmutí	k1gNnSc3
<g/>
,	,	kIx,
a	a	k8xC
mrtvá	mrtvý	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
ji	on	k3xPp3gFnSc4
soupeř	soupeř	k1gMnSc1
může	moct	k5eAaImIp3nS
zajmout	zajmout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
životu	život	k1gInSc3
postačují	postačovat	k5eAaImIp3nP
dvě	dva	k4xCgNnPc1
oči	oko	k1gNnPc1
(	(	kIx(
<g/>
navzájem	navzájem	k6eAd1
oddělené	oddělený	k2eAgInPc4d1
prázdné	prázdný	k2eAgInPc4d1
průsečíky	průsečík	k1gInPc4
uvnitř	uvnitř	k7c2
skupiny	skupina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	on	k3xPp3gFnPc4
soupeř	soupeř	k1gMnSc1
nemůže	moct	k5eNaImIp3nS
donutit	donutit	k5eAaPmF
k	k	k7c3
vyplnění	vyplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1
dovedností	dovednost	k1gFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
propočet	propočet	k1gInSc1
<g/>
,	,	kIx,
tj.	tj.	kA
schopnost	schopnost	k1gFnSc4
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
živá	živý	k2eAgFnSc1d1
a	a	k8xC
jakým	jaký	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
tahem	tah	k1gInSc7
ji	on	k3xPp3gFnSc4
zachránit	zachránit	k5eAaPmF
resp.	resp.	kA
zajmout	zajmout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Zkušení	zkušený	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
nevynechají	vynechat	k5eNaPmIp3nP
příležitost	příležitost	k1gFnSc4
provést	provést	k5eAaPmF
tzv.	tzv.	kA
střih	střih	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
tah	tah	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dvěma	dva	k4xCgFnPc3
soupeřovým	soupeřův	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
odepře	odepřít	k5eAaPmIp3nS
možnost	možnost	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
spojit	spojit	k5eAaPmF
v	v	k7c4
jednu	jeden	k4xCgFnSc4
skupinu	skupina	k1gFnSc4
<g/>
:	:	kIx,
Napadnout	napadnout	k5eAaPmF
dvě	dva	k4xCgFnPc4
menší	malý	k2eAgFnPc4d2
skupiny	skupina	k1gFnPc4
je	být	k5eAaImIp3nS
totiž	totiž	k9
snazší	snadný	k2eAgNnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
jednu	jeden	k4xCgFnSc4
velkou	velká	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Slabá	slabý	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
je	být	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
soupeř	soupeř	k1gMnSc1
může	moct	k5eAaImIp3nS
útočit	útočit	k5eAaImF
<g/>
,	,	kIx,
tedy	tedy	k8xC
získat	získat	k5eAaPmF
výhodu	výhoda	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
ohrožuje	ohrožovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
slabá	slabý	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
tahy	tah	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
ji	on	k3xPp3gFnSc4
udržují	udržovat	k5eAaImIp3nP
při	při	k7c6
životě	život	k1gInSc6
<g/>
,	,	kIx,
soupeř	soupeř	k1gMnSc1
svými	svůj	k3xOyFgInPc7
tahy	tah	k1gInPc7
zároveň	zároveň	k6eAd1
skupinu	skupina	k1gFnSc4
ohrožuje	ohrožovat	k5eAaImIp3nS
a	a	k8xC
zároveň	zároveň	k6eAd1
získává	získávat	k5eAaImIp3nS
nějakou	nějaký	k3yIgFnSc4
další	další	k2eAgFnSc4d1
výhodu	výhoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
může	moct	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
žít	žít	k5eAaImF
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
výsledku	výsledek	k1gInSc6
kó	kó	k?
–	–	k?
hráči	hráč	k1gMnPc1
bojují	bojovat	k5eAaImIp3nP
o	o	k7c4
dva	dva	k4xCgInPc4
průsečíky	průsečík	k1gInPc4
ve	v	k7c6
snaze	snaha	k1gFnSc6
vyplnit	vyplnit	k5eAaPmF
oba	dva	k4xCgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyklicky	cyklicky	k6eAd1
se	se	k3xPyFc4
opakuje	opakovat	k5eAaImIp3nS
stav	stav	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hráč	hráč	k1gMnSc1
zajme	zajmout	k5eAaPmIp3nS
kámen	kámen	k1gInSc4
na	na	k7c6
tomto	tento	k3xDgInSc6
průsečíku	průsečík	k1gInSc6
a	a	k8xC
pravidlo	pravidlo	k1gNnSc1
kó	kó	k?
zakazuje	zakazovat	k5eAaImIp3nS
soupeři	soupeř	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
položený	položený	k2eAgInSc1d1
kámen	kámen	k1gInSc1
také	také	k9
ihned	ihned	k6eAd1
zajal	zajmout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
neprohrál	prohrát	k5eNaPmAgInS
boj	boj	k1gInSc1
o	o	k7c4
kó	kó	k?
(	(	kIx(
<g/>
劫	劫	k?
<g/>
,	,	kIx,
v	v	k7c6
terminologii	terminologie	k1gFnSc6
go	go	k?
častěji	často	k6eAd2
コ	コ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
soupeř	soupeř	k1gMnSc1
zahraje	zahrát	k5eAaPmIp3nS
jinde	jinde	k6eAd1
na	na	k7c6
desce	deska	k1gFnSc6
hrozbu	hrozba	k1gFnSc4
na	na	k7c4
kó	kó	k?
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
první	první	k4xOgMnSc1
hráč	hráč	k1gMnSc1
nutně	nutně	k6eAd1
musí	muset	k5eAaImIp3nS
odpovědět	odpovědět	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Animace	animace	k1gFnSc1
partie	partie	k1gFnSc1
go	go	k?
</s>
<s>
Fáze	fáze	k1gFnSc1
partie	partie	k1gFnSc1
</s>
<s>
Zahájení	zahájení	k1gNnSc1
–	–	k?
jde	jít	k5eAaImIp3nS
o	o	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
desce	deska	k1gFnSc6
mnoho	mnoho	k6eAd1
neobsazeného	obsazený	k2eNgNnSc2d1
území	území	k1gNnSc2
a	a	k8xC
hráči	hráč	k1gMnPc1
je	on	k3xPp3gNnSc4
spíše	spíše	k9
zabírají	zabírat	k5eAaImIp3nP
<g/>
,	,	kIx,
než	než	k8xS
aby	aby	kYmCp3nP
bojovali	bojovat	k5eAaImAgMnP
se	s	k7c7
soupeřovými	soupeřův	k2eAgInPc7d1
kameny	kámen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
tahy	tah	k1gInPc4
hlavně	hlavně	k9
při	při	k7c6
krajích	kraj	k1gInPc6
desky	deska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Střední	střední	k2eAgFnSc1d1
hra	hra	k1gFnSc1
nastává	nastávat	k5eAaImIp3nS
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
již	jenž	k3xRgMnPc1
hráči	hráč	k1gMnPc1
nemají	mít	k5eNaImIp3nP
tahy	tah	k1gInPc4
<g/>
,	,	kIx,
kterými	který	k3yQgInPc7,k3yRgInPc7,k3yIgInPc7
by	by	kYmCp3nP
obsadili	obsadit	k5eAaPmAgMnP
volné	volný	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
začínají	začínat	k5eAaImIp3nP
bojovat	bojovat	k5eAaImF
se	s	k7c7
soupeřovými	soupeřův	k2eAgInPc7d1
kameny	kámen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boj	boj	k1gInSc1
se	se	k3xPyFc4
přesouvá	přesouvat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
středu	střed	k1gInSc2
desky	deska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc3
fázi	fáze	k1gFnSc3
dominuje	dominovat	k5eAaImIp3nS
útok	útok	k1gInSc1
a	a	k8xC
obrana	obrana	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
snaha	snaha	k1gFnSc1
zajmout	zajmout	k5eAaPmF
soupeřovy	soupeřův	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
alespoň	alespoň	k9
útokem	útok	k1gInSc7
na	na	k7c4
ně	on	k3xPp3gMnPc4
dosáhnout	dosáhnout	k5eAaPmF
nějaké	nějaký	k3yIgFnPc4
výhody	výhoda	k1gFnPc4
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
výše	vysoce	k6eAd2
Slabé	Slabé	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Koncovka	koncovka	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
boj	boj	k1gInSc1
zklidní	zklidnit	k5eAaPmIp3nS
a	a	k8xC
o	o	k7c6
všech	všecek	k3xTgFnPc6
velkých	velký	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
je	být	k5eAaImIp3nS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
živé	živý	k2eAgFnPc1d1
či	či	k8xC
mrtvé	mrtvý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
poklesne	poklesnout	k5eAaPmIp3nS
zisk	zisk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
získat	získat	k5eAaPmF
jedním	jeden	k4xCgInSc7
tahem	tah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
velké	velký	k2eAgInPc1d1
tahy	tah	k1gInPc1
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
vyčerpány	vyčerpat	k5eAaPmNgFnP
<g/>
,	,	kIx,
hráčům	hráč	k1gMnPc3
zbývají	zbývat	k5eAaImIp3nP
jen	jen	k6eAd1
drobnosti	drobnost	k1gFnPc1
jako	jako	k9
např.	např.	kA
malé	malý	k2eAgNnSc1d1
zmenšení	zmenšení	k1gNnSc1
soupeřova	soupeřův	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k9
tyto	tento	k3xDgInPc1
tahy	tah	k1gInPc1
však	však	k9
nakonec	nakonec	k6eAd1
mohou	moct	k5eAaImIp3nP
ovlivnit	ovlivnit	k5eAaPmF
vítěze	vítěz	k1gMnPc4
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Strategie	strategie	k1gFnSc1
v	v	k7c6
go	go	k?
</s>
<s>
Rohy	roh	k1gInPc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
střed	střed	k1gInSc1
–	–	k?
obklíčit	obklíčit	k5eAaPmF
území	území	k1gNnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
rozích	roh	k1gInPc6
mnohem	mnohem	k6eAd1
snazší	snadný	k2eAgNnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
při	při	k7c6
okraji	okraj	k1gInSc6
a	a	k8xC
zejména	zejména	k9
než	než	k8xS
ve	v	k7c6
středu	střed	k1gInSc6
desky	deska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
bojuje	bojovat	k5eAaImIp3nS
přednostně	přednostně	k6eAd1
o	o	k7c4
rohy	roh	k1gInPc4
a	a	k8xC
strany	strana	k1gFnPc4
<g/>
,	,	kIx,
dokud	dokud	k8xS
tam	tam	k6eAd1
lze	lze	k6eAd1
získat	získat	k5eAaPmF
nějaké	nějaký	k3yIgNnSc4
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Vliv	vliv	k1gInSc1
–	–	k?
je	být	k5eAaImIp3nS
dobré	dobrý	k2eAgNnSc1d1
pokládat	pokládat	k5eAaImF
kameny	kámen	k1gInPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ovlivňovaly	ovlivňovat	k5eAaImAgFnP
co	co	k9
největší	veliký	k2eAgNnSc4d3
území	území	k1gNnSc4
a	a	k8xC
přinesly	přinést	k5eAaPmAgFnP
tím	ten	k3xDgNnSc7
hráči	hráč	k1gMnPc1
výhodu	výhoda	k1gFnSc4
v	v	k7c6
pozdějším	pozdní	k2eAgInSc6d2
boji	boj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
více	hodně	k6eAd2
hráčových	hráčův	k2eAgInPc2d1
(	(	kIx(
<g/>
a	a	k8xC
méně	málo	k6eAd2
soupeřových	soupeřův	k2eAgInPc2d1
<g/>
)	)	kIx)
kamenů	kámen	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
určitého	určitý	k2eAgNnSc2d1
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
snazší	snadný	k2eAgMnSc1d2
je	být	k5eAaImIp3nS
zachránit	zachránit	k5eAaPmF
svoje	svůj	k3xOyFgInPc4
kameny	kámen	k1gInPc4
a	a	k8xC
zajmout	zajmout	k5eAaPmF
soupeřovy	soupeřův	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
často	často	k6eAd1
musí	muset	k5eAaImIp3nS
volit	volit	k5eAaImF
mezi	mezi	k7c7
získáním	získání	k1gNnSc7
území	území	k1gNnSc2
a	a	k8xC
vytvořením	vytvoření	k1gNnSc7
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Jelikož	jelikož	k8xS
skóre	skóre	k1gNnSc1
je	být	k5eAaImIp3nS
definováno	definovat	k5eAaBmNgNnS
pomocí	pomocí	k7c2
území	území	k1gNnSc2
<g/>
,	,	kIx,
vliv	vliv	k1gInSc1
je	být	k5eAaImIp3nS
hráči	hráč	k1gMnPc7
užitečný	užitečný	k2eAgInSc4d1
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
jej	on	k3xPp3gMnSc4
dokáže	dokázat	k5eAaPmIp3nS
během	během	k7c2
hry	hra	k1gFnSc2
využít	využít	k5eAaPmF
k	k	k7c3
získání	získání	k1gNnSc3
území	území	k1gNnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Džóseki	Džóseki	k1gNnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
posloupnost	posloupnost	k1gFnSc4
tahů	tah	k1gInPc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
profesionálové	profesionál	k1gMnPc1
označili	označit	k5eAaPmAgMnP
za	za	k7c4
posloupnost	posloupnost	k1gFnSc4
nejlepších	dobrý	k2eAgInPc2d3
tahů	tah	k1gInPc2
v	v	k7c6
dané	daný	k2eAgFnSc6d1
lokální	lokální	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
(	(	kIx(
<g/>
tedy	tedy	k9
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
ostatní	ostatní	k2eAgFnPc4d1
části	část	k1gFnPc4
desky	deska	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
desítky	desítka	k1gFnPc4
tisíc	tisíc	k4xCgInPc2
džóseki	džóseki	k1gNnPc2
<g/>
;	;	kIx,
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
týkají	týkat	k5eAaImIp3nP
boje	boj	k1gInPc1
o	o	k7c4
roh	roh	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
situace	situace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
každé	každý	k3xTgFnSc6
partii	partie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
džóseki	džóseki	k6eAd1
pomůže	pomoct	k5eAaPmIp3nS
hráči	hráč	k1gMnSc3
k	k	k7c3
hlubšímu	hluboký	k2eAgInSc3d2
vhledu	vhled	k1gInSc3
do	do	k7c2
hry	hra	k1gFnSc2
<g/>
;	;	kIx,
důležité	důležitý	k2eAgNnSc1d1
není	být	k5eNaImIp3nS
se	se	k3xPyFc4
je	on	k3xPp3gInPc4
naučit	naučit	k5eAaPmF
nazpaměť	nazpaměť	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
pochopit	pochopit	k5eAaPmF
účel	účel	k1gInSc4
každého	každý	k3xTgInSc2
tahu	tah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
mnoha	mnoho	k4c6
situacích	situace	k1gFnPc6
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
jeden	jeden	k4xCgInSc4
správný	správný	k2eAgInSc4d1
tah	tah	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
více	hodně	k6eAd2
než	než	k8xS
jedna	jeden	k4xCgFnSc1
pokračující	pokračující	k2eAgFnSc1d1
džóseki	džósek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
pak	pak	k9
musí	muset	k5eAaImIp3nS
zvolit	zvolit	k5eAaPmF
správnou	správný	k2eAgFnSc4d1
z	z	k7c2
nich	on	k3xPp3gFnPc2
podle	podle	k7c2
celkové	celkový	k2eAgFnSc2d1
situace	situace	k1gFnSc2
na	na	k7c6
desce	deska	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Bezpečná	bezpečný	k2eAgFnSc1d1
vs	vs	k?
<g/>
.	.	kIx.
riskantní	riskantní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
–	–	k?
pokud	pokud	k8xS
hráč	hráč	k1gMnSc1
vede	vést	k5eAaImIp3nS
(	(	kIx(
<g/>
tj.	tj.	kA
lze	lze	k6eAd1
očekávat	očekávat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
vyhraje	vyhrát	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
by	by	kYmCp3nS
volit	volit	k5eAaImF
spíše	spíše	k9
tahy	tah	k1gInPc4
s	s	k7c7
předvídatelným	předvídatelný	k2eAgInSc7d1
výsledkem	výsledek	k1gInSc7
než	než	k8xS
zahajovat	zahajovat	k5eAaImF
riskantní	riskantní	k2eAgInPc4d1
boje	boj	k1gInPc4
s	s	k7c7
těžko	těžko	k6eAd1
odhadnutelným	odhadnutelný	k2eAgInSc7d1
koncem	konec	k1gInSc7
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yRgFnPc7,k3yIgFnPc7
může	moct	k5eAaImIp3nS
hodně	hodně	k6eAd1
získat	získat	k5eAaPmF
anebo	anebo	k8xC
hodně	hodně	k6eAd1
ztratit	ztratit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
pokud	pokud	k8xS
hráč	hráč	k1gMnSc1
prohrává	prohrávat	k5eAaImIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
takové	takový	k3xDgInPc4
tahy	tah	k1gInPc4
jeho	jeho	k3xOp3gFnSc7
jedinou	jediný	k2eAgFnSc7d1
nadějí	naděje	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Slovníček	slovníček	k1gInSc1
japonských	japonský	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
z	z	k7c2
go	go	k?
</s>
<s>
Do	do	k7c2
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Ameriky	Amerika	k1gFnSc2
rozšířili	rozšířit	k5eAaPmAgMnP
go	go	k?
především	především	k6eAd1
japonští	japonský	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
pro	pro	k7c4
mnoho	mnoho	k4c4
pojmů	pojem	k1gInPc2
používá	používat	k5eAaImIp3nS
japonský	japonský	k2eAgInSc1d1
výraz	výraz	k1gInSc1
<g/>
,	,	kIx,
především	především	k6eAd1
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
nebylo	být	k5eNaImAgNnS
snadné	snadný	k2eAgNnSc1d1
najít	najít	k5eAaPmF
odpovídající	odpovídající	k2eAgInSc4d1
ekvivalent	ekvivalent	k1gInSc4
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
(	(	kIx(
<g/>
zejména	zejména	k9
angličtině	angličtina	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
adži	adži	k6eAd1
–	–	k?
味	味	k?
–	–	k?
doslova	doslova	k6eAd1
"	"	kIx"
<g/>
chuť	chuť	k1gFnSc1
<g/>
"	"	kIx"
–	–	k?
budoucí	budoucí	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
</s>
<s>
atari	atari	k6eAd1
–	–	k?
当	当	k?
–	–	k?
hrozba	hrozba	k1gFnSc1
zajetím	zajetí	k1gNnPc3
–	–	k?
tah	tah	k1gInSc4
<g/>
,	,	kIx,
odebírající	odebírající	k2eAgFnSc4d1
nějaké	nějaký	k3yIgFnSc3
skupině	skupina	k1gFnSc3
předposlední	předposlední	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
</s>
<s>
dame	dame	k1gFnSc1
–	–	k?
駄	駄	k?
–	–	k?
nezaplněné	zaplněný	k2eNgNnSc1d1
neutrální	neutrální	k2eAgNnSc1d1
území	území	k1gNnSc1
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
pro	pro	k7c4
žádného	žádný	k3yNgMnSc4
z	z	k7c2
hráčů	hráč	k1gMnPc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
okamžiku	okamžik	k1gInSc6
není	být	k5eNaImIp3nS
výhodné	výhodný	k2eAgNnSc1d1
hrát	hrát	k5eAaImF
<g/>
,	,	kIx,
území	území	k1gNnSc6
<g/>
,	,	kIx,
o	o	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
není	být	k5eNaImIp3nS
zájem	zájem	k1gInSc1
</s>
<s>
fuseki	fuseki	k6eAd1
–	–	k?
布	布	k?
–	–	k?
zahájení	zahájení	k1gNnSc2
partie	partie	k1gFnSc1
</s>
<s>
gote	gote	k1gFnSc1
–	–	k?
後	後	k?
–	–	k?
ztráta	ztráta	k1gFnSc1
tempa	tempo	k1gNnSc2
–	–	k?
takový	takový	k3xDgInSc4
závěr	závěr	k1gInSc4
sekvence	sekvence	k1gFnSc1
tahů	tah	k1gInPc2
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
na	na	k7c6
tahu	tah	k1gInSc6
soupeř	soupeř	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
výhodu	výhoda	k1gFnSc4
tahu	tah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opakem	opak	k1gInSc7
je	on	k3xPp3gInPc4
sente	senit	k5eAaBmRp2nP,k5eAaImRp2nP,k5eAaPmRp2nP
<g/>
.	.	kIx.
</s>
<s>
hane	hane	k1gFnSc1
–	–	k?
跳	跳	k?
(	(	kIx(
<g/>
ハ	ハ	k?
<g/>
)	)	kIx)
–	–	k?
ohnutí	ohnutí	k1gNnSc2
–	–	k?
diagonální	diagonální	k2eAgInSc1d1
tah	tah	k1gInSc1
kolem	kolem	k7c2
soupeřova	soupeřův	k2eAgInSc2d1
kamene	kámen	k1gInSc2
</s>
<s>
hoši	hoch	k1gMnPc1
–	–	k?
星	星	k?
–	–	k?
hvězda	hvězda	k1gFnSc1
<g/>
,	,	kIx,
některý	některý	k3yIgInSc1
z	z	k7c2
devíti	devět	k4xCc2
základních	základní	k2eAgInPc2d1
bodů	bod	k1gInPc2
gobanu	goban	k1gInSc2
<g/>
,	,	kIx,
označený	označený	k2eAgInSc1d1
černou	černý	k2eAgFnSc7d1
tečkou	tečka	k1gFnSc7
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
největší	veliký	k2eAgInSc4d3
strategický	strategický	k2eAgInSc4d1
význam	význam	k1gInSc4
</s>
<s>
iši	iši	k?
–	–	k?
石	石	k?
–	–	k?
kámen	kámen	k1gInSc1
</s>
<s>
džóseki	džóseki	k6eAd1
–	–	k?
定	定	k?
–	–	k?
nejlepší	dobrý	k2eAgFnSc1d3
sekvence	sekvence	k1gFnSc1
tahů	tah	k1gInPc2
pro	pro	k7c4
oba	dva	k4xCgMnPc4
hráče	hráč	k1gMnPc4
v	v	k7c6
určité	určitý	k2eAgFnSc6d1
lokální	lokální	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
</s>
<s>
keima	keima	k1gFnSc1
–	–	k?
ケ	ケ	k?
(	(	kIx(
<g/>
v	v	k7c6
terminologii	terminologie	k1gFnSc6
go	go	k?
se	se	k3xPyFc4
píše	psát	k5eAaImIp3nS
v	v	k7c6
katakaně	katakaně	k6eAd1
<g/>
,	,	kIx,
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
šachového	šachový	k2eAgMnSc2d1
桂	桂	k?
<g/>
)	)	kIx)
–	–	k?
malý	malý	k2eAgInSc1d1
koňský	koňský	k2eAgInSc1d1
skok	skok	k1gInSc1
–	–	k?
položení	položení	k1gNnSc3
jednoho	jeden	k4xCgInSc2
kamene	kámen	k1gInSc2
ke	k	k7c3
druhému	druhý	k4xOgInSc3
svému	svůj	k3xOyFgInSc3
kameni	kámen	k1gInSc3
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
vytvoří	vytvořit	k5eAaPmIp3nS
tvar	tvar	k1gInSc4
podobný	podobný	k2eAgInSc4d1
tahu	tah	k1gInSc2
šachového	šachový	k2eAgMnSc2d1
koně	kůň	k1gMnSc2
</s>
<s>
kó	kó	k?
–	–	k?
劫	劫	k?
hráči	hráč	k1gMnPc1
bojují	bojovat	k5eAaImIp3nP
o	o	k7c4
dva	dva	k4xCgInPc4
průsečíky	průsečík	k1gInPc4
ve	v	k7c6
snaze	snaha	k1gFnSc6
vyplnit	vyplnit	k5eAaPmF
oba	dva	k4xCgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyklicky	cyklicky	k6eAd1
se	se	k3xPyFc4
opakuje	opakovat	k5eAaImIp3nS
stav	stav	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hráč	hráč	k1gMnSc1
zajme	zajmout	k5eAaPmIp3nS
kámen	kámen	k1gInSc4
na	na	k7c6
tomto	tento	k3xDgInSc6
průsečíku	průsečík	k1gInSc6
a	a	k8xC
pravidlo	pravidlo	k1gNnSc1
kó	kó	k?
zakazuje	zakazovat	k5eAaImIp3nS
soupeři	soupeř	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
položený	položený	k2eAgInSc1d1
kámen	kámen	k1gInSc1
také	také	k9
ihned	ihned	k6eAd1
zajal	zajmout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
komi	komi	k6eAd1
–	–	k?
コ	コ	k?
<g/>
,	,	kIx,
správněji	správně	k6eAd2
コ	コ	k?
komidaši	komidasat	k5eAaPmIp1nSwK
–	–	k?
bonus	bonus	k1gInSc1
<g/>
,	,	kIx,
kompensace	kompensace	k1gFnSc1
za	za	k7c4
výhodu	výhoda	k1gFnSc4
prvního	první	k4xOgInSc2
tahu	tah	k1gInSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
bývá	bývat	k5eAaImIp3nS
6,5	6,5	k4
bodu	bod	k1gInSc2
i	i	k9
jinak	jinak	k6eAd1
–	–	k?
viz	vidět	k5eAaImRp2nS
nahoře	nahoře	k6eAd1
</s>
<s>
kosumi	kosu	k1gFnPc7
–	–	k?
コ	コ	k?
–	–	k?
diagonální	diagonální	k2eAgInSc1d1
tah	tah	k1gInSc1
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
kameni	kámen	k1gInSc3
</s>
<s>
miai	miai	k6eAd1
–	–	k?
見	見	k?
–	–	k?
stejné	stejný	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
–	–	k?
soupeř	soupeř	k1gMnSc1
může	moct	k5eAaImIp3nS
útočit	útočit	k5eAaImF
tahem	tah	k1gInSc7
na	na	k7c4
jeden	jeden	k4xCgInSc4
ze	z	k7c2
dvou	dva	k4xCgInPc2
bodů	bod	k1gInPc2
a	a	k8xC
druhý	druhý	k4xOgInSc4
vždy	vždy	k6eAd1
poslouží	posloužit	k5eAaPmIp3nS
k	k	k7c3
obraně	obrana	k1gFnSc3
</s>
<s>
mojó	mojó	k?
–	–	k?
模	模	k?
–	–	k?
územní	územní	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
–	–	k?
velmi	velmi	k6eAd1
volně	volně	k6eAd1
naznačené	naznačený	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
ógeima	ógeima	k1gFnSc1
–	–	k?
大	大	k?
–	–	k?
velký	velký	k2eAgInSc1d1
koňský	koňský	k2eAgInSc1d1
skok	skok	k1gInSc1
–	–	k?
podobný	podobný	k2eAgInSc1d1
tahu	tah	k1gInSc6
keima	keima	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
jeden	jeden	k4xCgInSc4
bod	bod	k1gInSc4
dále	daleko	k6eAd2
</s>
<s>
me	me	k?
–	–	k?
眼	眼	k?
–	–	k?
oko	oko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
životu	život	k1gInSc3
postačují	postačovat	k5eAaImIp3nP
dvě	dva	k4xCgNnPc1
oči	oko	k1gNnPc1
(	(	kIx(
<g/>
navzájem	navzájem	k6eAd1
oddělené	oddělený	k2eAgInPc4d1
prázdné	prázdný	k2eAgInPc4d1
průsečíky	průsečík	k1gInPc4
uvnitř	uvnitř	k7c2
skupiny	skupina	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
san-san	san-san	k1gInSc1
–	–	k?
三	三	k?
bod	bod	k1gInSc4
v	v	k7c6
rohu	roh	k1gInSc6
na	na	k7c6
pozici	pozice	k1gFnSc6
3-3	3-3	k4
</s>
<s>
sente	sente	k5eAaPmIp2nP
–	–	k?
先	先	k?
–	–	k?
tempo	tempo	k1gNnSc4
–	–	k?
takový	takový	k3xDgInSc4
závěr	závěr	k1gInSc4
sekvence	sekvence	k1gFnSc1
tahů	tah	k1gInPc2
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
opět	opět	k6eAd1
na	na	k7c6
tahu	tah	k1gInSc6
a	a	k8xC
ve	v	k7c6
výhodě	výhoda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opakem	opak	k1gInSc7
je	on	k3xPp3gNnSc4
gote	gote	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
seki	seki	k6eAd1
–	–	k?
セ	セ	k?
–	–	k?
lokálně	lokálně	k6eAd1
remízová	remízový	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
by	by	kYmCp3nS
hráč	hráč	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
by	by	kYmCp3nS
první	první	k4xOgFnSc7
táhl	táhnout	k5eAaImAgInS
<g/>
,	,	kIx,
prohrál	prohrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
pozice	pozice	k1gFnSc1
se	se	k3xPyFc4
ponechává	ponechávat	k5eAaImIp3nS
po	po	k7c6
konci	konec	k1gInSc6
partie	partie	k1gFnSc2
na	na	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
v	v	k7c6
ní	on	k3xPp3gFnSc6
obsažené	obsažený	k2eAgInPc1d1
body	bod	k1gInPc1
se	se	k3xPyFc4
nepočítají	počítat	k5eNaImIp3nP
žádnému	žádný	k3yNgNnSc3
z	z	k7c2
hráčů	hráč	k1gMnPc2
</s>
<s>
šimari	šimari	k6eAd1
–	–	k?
シ	シ	k?
–	–	k?
uzávěra	uzávěra	k1gFnSc1
rohu	roh	k1gInSc2
–	–	k?
dvojkamenová	dvojkamenový	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
uzavírající	uzavírající	k2eAgNnSc1d1
území	území	k1gNnSc4
v	v	k7c6
rohu	roh	k1gInSc6
</s>
<s>
tenuki	tenuki	k6eAd1
–	–	k?
手	手	k?
lokální	lokální	k2eAgNnSc4d1
ignorování	ignorování	k1gNnSc4
soupeřova	soupeřův	k2eAgInSc2d1
tahu	tah	k1gInSc2
(	(	kIx(
<g/>
a	a	k8xC
táhnutí	táhnutí	k1gNnSc1
v	v	k7c6
jiné	jiný	k2eAgFnSc6d1
části	část	k1gFnSc6
desky	deska	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
tesudži	tesudž	k1gFnSc3
–	–	k?
手	手	k?
technika	technika	k1gFnSc1
<g/>
,	,	kIx,
něco	něco	k3yInSc4
jako	jako	k9
chvat	chvat	k1gInSc4
v	v	k7c6
bojovém	bojový	k2eAgNnSc6d1
umění	umění	k1gNnSc6
<g/>
,	,	kIx,
chytrý	chytrý	k2eAgInSc1d1
tah	tah	k1gInSc1
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
není	být	k5eNaImIp3nS
patrný	patrný	k2eAgInSc1d1
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
výrazy	výraz	k1gInPc1
můžete	moct	k5eAaImIp2nP
nalézt	nalézt	k5eAaPmF,k5eAaBmF
zde	zde	k6eAd1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Go	Go	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://phys.org/news/2016-01-chess-human-ancient-chinese-game.html	http://phys.org/news/2016-01-chess-human-ancient-chinese-game.html	k1gInSc1
-	-	kIx~
Game	game	k1gInSc1
over	overa	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Computer	computer	k1gInSc1
beats	beats	k6eAd1
human	human	k1gMnSc1
champ	champ	k1gMnSc1
in	in	k?
ancient	ancient	k1gMnSc1
Chinese	Chinese	k1gFnSc2
game	game	k1gInSc1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
gogameguru	gogamegur	k1gInSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://technet.idnes.cz/alphago-crr-/veda.aspx?c=A160128_150313_veda_mla	http://technet.idnes.cz/alphago-crr-/veda.aspx?c=A160128_150313_veda_mla	k1gMnSc1
<g/>
↑	↑	k?
http://phys.org/news/2016-03-game-series-champion.html	http://phys.org/news/2016-03-game-series-champion.html	k1gMnSc1
-	-	kIx~
Game	game	k1gInSc1
over	overa	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Computer	computer	k1gInSc1
wins	wins	k6eAd1
series	series	k1gInSc1
against	against	k1gFnSc1
Go	Go	k1gMnSc1
champion	champion	k1gInSc1
(	(	kIx(
<g/>
Update	update	k1gInSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
European	European	k1gInSc1
Pros	prosa	k1gFnPc2
<g/>
.	.	kIx.
www.eurogofed.org	www.eurogofed.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Europeans	Europeans	k1gInSc1
with	with	k1gMnSc1
other	other	k1gMnSc1
pro	pro	k7c4
accreditation	accreditation	k1gInSc4
<g/>
.	.	kIx.
www.eurogofed.org	www.eurogofed.org	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Joseki	Joseki	k1gNnSc1
<g/>
,	,	kIx,
senseis	senseis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
xmp	xmp	k?
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Slovníček	slovníček	k1gInSc1
výrazů	výraz	k1gInPc2
v	v	k7c6
go	go	k?
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Světová	světový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
go	go	k?
(	(	kIx(
<g/>
IGF	IGF	kA
<g/>
)	)	kIx)
</s>
<s>
Rendžu	Rendzat	k5eAaPmIp1nS
</s>
<s>
Židi	žid	k1gMnPc1
</s>
<s>
Piškvorky	piškvorka	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Go	Go	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
go	go	k?
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Intergofed	Intergofed	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
-	-	kIx~
Světová	světový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
go	go	k?
International	International	k1gMnSc1
Go	Go	k1gMnSc1
Federation	Federation	k1gInSc4
</s>
<s>
České	český	k2eAgFnPc1d1
</s>
<s>
Goweb	Gowba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
České	český	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
go	go	k?
Czech	Czech	k1gMnSc1
Go	Go	k1gMnSc1
Association	Association	k1gInSc4
</s>
<s>
eGoban	eGoban	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
magazín	magazín	k1gInSc1
z	z	k7c2
goistické	goistický	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
</s>
<s>
Hra-go	Hra-go	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
informace	informace	k1gFnSc1
o	o	k7c6
hře	hra	k1gFnSc6
go	go	k?
–	–	k?
pravidla	pravidlo	k1gNnSc2
<g/>
,	,	kIx,
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
odkazy	odkaz	k1gInPc4
</s>
<s>
Deskovehry	Deskovehra	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
-	-	kIx~
recenze	recenze	k1gFnPc1
hry	hra	k1gFnSc2
Go	Go	k1gFnSc2
</s>
<s>
Studium	studium	k1gNnSc1
</s>
<s>
Sensei	Sensei	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Library	Librar	k1gInPc7
(	(	kIx(
<g/>
wiki	wikit	k5eAaImRp2nS,k5eAaPmRp2nS
web	web	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
senseis	senseis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
xmp	xmp	k?
<g/>
.	.	kIx.
<g/>
net	net	k?
–	–	k?
Výklad	výklad	k1gInSc1
pojmů	pojem	k1gInPc2
<g/>
,	,	kIx,
strategie	strategie	k1gFnSc2
a	a	k8xC
taktiky	taktika	k1gFnSc2
apod.	apod.	kA
</s>
<s>
GoProblems	GoProblems	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
Úlohy	úloha	k1gFnPc4
života	život	k1gInSc2
a	a	k8xC
smrti	smrt	k1gFnSc2
apod.	apod.	kA
</s>
<s>
Go	Go	k?
Teaching	Teaching	k1gInSc1
Ladder	Ladder	k1gInSc1
<g/>
,	,	kIx,
gtl	gtl	k?
<g/>
.	.	kIx.
<g/>
xmp	xmp	k?
<g/>
.	.	kIx.
<g/>
net	net	k?
–	–	k?
Komentáře	komentář	k1gInSc2
partií	partie	k1gFnPc2
všech	všecek	k3xTgFnPc2
úrovní	úroveň	k1gFnPc2
od	od	k7c2
silných	silný	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
</s>
<s>
Realtime	Realtimat	k5eAaPmIp3nS
servery	server	k1gInPc4
</s>
<s>
KGS	KGS	kA
<g/>
,	,	kIx,
gokgs	gokgs	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
IGS	IGS	kA
<g/>
,	,	kIx,
pandanet-igs	pandanet-igs	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Turn-based	Turn-based	k1gInSc1
servery	server	k1gInPc1
</s>
<s>
Dragon	Dragon	k1gMnSc1
Go	Go	k1gFnSc2
Server	server	k1gInSc1
</s>
<s>
Little	Little	k6eAd1
Golem	Golem	k1gMnSc1
</s>
<s>
Novinky	novinka	k1gFnPc1
</s>
<s>
Gobase	Gobase	k6eAd1
</s>
<s>
Go	Go	k?
<g/>
4	#num#	k4
<g/>
Go	Go	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4071909-1	4071909-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85055468	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85055468	#num#	k4
</s>
