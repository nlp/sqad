<p>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
je	být	k5eAaImIp3nS	být
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
a	a	k8xC	a
osmileté	osmiletý	k2eAgNnSc4d1	osmileté
gymnázium	gymnázium	k1gNnSc4	gymnázium
založené	založený	k2eAgFnSc2d1	založená
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
české	český	k2eAgNnSc4d1	české
gymnázium	gymnázium	k1gNnSc4	gymnázium
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
u	u	k7c2	u
jeho	jeho	k3xOp3gInSc2	jeho
zrodu	zrod	k1gInSc2	zrod
stál	stát	k5eAaImAgMnS	stát
zemský	zemský	k2eAgMnSc1d1	zemský
poslanec	poslanec	k1gMnSc1	poslanec
Jan	Jan	k1gMnSc1	Jan
Kozánek	Kozánek	k1gMnSc1	Kozánek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
existovalo	existovat	k5eAaImAgNnS	existovat
piaristické	piaristický	k2eAgNnSc1d1	Piaristické
gymnázium	gymnázium	k1gNnSc1	gymnázium
založené	založený	k2eAgNnSc1d1	založené
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
Karlem	Karel	k1gMnSc7	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Liechtensteina-Calstelcornu	Liechtensteina-Calstelcorn	k1gInSc2	Liechtensteina-Calstelcorn
a	a	k8xC	a
Arcibiskupské	arcibiskupský	k2eAgNnSc4d1	Arcibiskupské
gymnázium	gymnázium	k1gNnSc4	gymnázium
založené	založený	k2eAgNnSc4d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Bedřichem	Bedřich	k1gMnSc7	Bedřich
z	z	k7c2	z
Fürstenberka	Fürstenberka	k1gFnSc1	Fürstenberka
jako	jako	k8xS	jako
konvikt	konvikt	k1gInSc1	konvikt
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
na	na	k7c6	na
piaristickém	piaristický	k2eAgNnSc6d1	Piaristické
gymnáziu	gymnázium	k1gNnSc6	gymnázium
probíhala	probíhat	k5eAaImAgNnP	probíhat
původně	původně	k6eAd1	původně
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
po	po	k7c6	po
josefinských	josefinský	k2eAgFnPc6d1	josefinská
reformách	reforma	k1gFnPc6	reforma
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
mezi	mezi	k7c7	mezi
studenty	student	k1gMnPc7	student
převažovali	převažovat	k5eAaImAgMnP	převažovat
česky	česky	k6eAd1	česky
hovořící	hovořící	k2eAgMnPc1d1	hovořící
Moravané	Moravan	k1gMnPc1	Moravan
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskupské	arcibiskupský	k2eAgNnSc1d1	Arcibiskupské
gymnázium	gymnázium	k1gNnSc1	gymnázium
bylo	být	k5eAaImAgNnS	být
jazykově	jazykově	k6eAd1	jazykově
českoněmecké	českoněmecký	k2eAgNnSc1d1	českoněmecké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
úplné	úplný	k2eAgNnSc1d1	úplné
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
výuky	výuka	k1gFnSc2	výuka
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
piaristickém	piaristický	k2eAgNnSc6d1	Piaristické
gymnáziu	gymnázium	k1gNnSc6	gymnázium
a	a	k8xC	a
studenti	student	k1gMnPc1	student
skládali	skládat	k5eAaImAgMnP	skládat
maturitu	maturita	k1gFnSc4	maturita
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zřízení	zřízení	k1gNnSc1	zřízení
českých	český	k2eAgFnPc2d1	Česká
tříd	třída	k1gFnPc2	třída
na	na	k7c6	na
piaristickém	piaristický	k2eAgNnSc6d1	Piaristické
gymnáziu	gymnázium	k1gNnSc6	gymnázium
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nereálné	reálný	k2eNgNnSc1d1	nereálné
<g/>
.	.	kIx.	.
</s>
<s>
Neprosadila	prosadit	k5eNaPmAgFnS	prosadit
se	se	k3xPyFc4	se
ani	ani	k9	ani
myšlenka	myšlenka	k1gFnSc1	myšlenka
rozšířit	rozšířit	k5eAaPmF	rozšířit
Arcibiskupské	arcibiskupský	k2eAgNnSc4d1	Arcibiskupské
gymnázium	gymnázium	k1gNnSc4	gymnázium
na	na	k7c4	na
úplnou	úplný	k2eAgFnSc4d1	úplná
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
začali	začít	k5eAaPmAgMnP	začít
místní	místní	k2eAgMnPc1d1	místní
vlastenci	vlastenec	k1gMnPc1	vlastenec
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
nového	nový	k2eAgInSc2d1	nový
<g/>
,	,	kIx,	,
českého	český	k2eAgInSc2d1	český
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
potřeby	potřeba	k1gFnSc2	potřeba
vzdělání	vzdělání	k1gNnSc2	vzdělání
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
jazyce	jazyk	k1gInSc6	jazyk
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
i	i	k9	i
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
profesorů	profesor	k1gMnPc2	profesor
piaristického	piaristický	k2eAgNnSc2d1	Piaristické
gymnázia	gymnázium	k1gNnSc2	gymnázium
nesložila	složit	k5eNaPmAgFnS	složit
zkoušky	zkouška	k1gFnPc4	zkouška
z	z	k7c2	z
učitelské	učitelský	k2eAgFnSc2d1	učitelská
způsobilosti	způsobilost	k1gFnSc2	způsobilost
a	a	k8xC	a
učí	učit	k5eAaImIp3nS	učit
nekvalitně	kvalitně	k6eNd1	kvalitně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
táboru	tábor	k1gInSc6	tábor
lidu	lid	k1gInSc2	lid
konaném	konaný	k2eAgInSc6d1	konaný
29	[number]	k4	29
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1869	[number]	k4	1869
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
rezoluce	rezoluce	k1gFnSc1	rezoluce
za	za	k7c4	za
založení	založení	k1gNnSc4	založení
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
bylo	být	k5eAaImAgNnS	být
postátněno	postátněn	k2eAgNnSc1d1	postátněno
piaristické	piaristický	k2eAgNnSc1d1	Piaristické
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
schválil	schválit	k5eAaPmAgInS	schválit
rozpočtový	rozpočtový	k2eAgInSc1d1	rozpočtový
výbor	výbor	k1gInSc1	výbor
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
rezoluci	rezoluce	k1gFnSc4	rezoluce
vyzývající	vyzývající	k2eAgFnSc4d1	vyzývající
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
počtu	počet	k1gInSc2	počet
českých	český	k2eAgFnPc2d1	Česká
státních	státní	k2eAgFnPc2d1	státní
škol	škola	k1gFnPc2	škola
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lépe	dobře	k6eAd2	dobře
odrážel	odrážet	k5eAaImAgInS	odrážet
národnostní	národnostní	k2eAgInSc1d1	národnostní
poměr	poměr	k1gInSc1	poměr
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
i	i	k9	i
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
ke	k	k7c3	k
zvýšenému	zvýšený	k2eAgInSc3d1	zvýšený
tlaku	tlak	k1gInSc3	tlak
za	za	k7c4	za
prosazení	prosazení	k1gNnSc4	prosazení
výuky	výuka	k1gFnSc2	výuka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
se	se	k3xPyFc4	se
angažovala	angažovat	k5eAaBmAgFnS	angažovat
Moravská	moravský	k2eAgFnSc1d1	Moravská
národní	národní	k2eAgFnSc1d1	národní
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
staročeská	staročeský	k2eAgFnSc1d1	staročeská
<g/>
)	)	kIx)	)
vedená	vedený	k2eAgFnSc1d1	vedená
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kozánkem	Kozánek	k1gMnSc7	Kozánek
<g/>
,	,	kIx,	,
ta	ten	k3xDgNnPc1	ten
však	však	k9	však
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
drtivě	drtivě	k6eAd1	drtivě
prohrála	prohrát	k5eAaPmAgFnS	prohrát
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
městskou	městský	k2eAgFnSc4d1	městská
správu	správa	k1gFnSc4	správa
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
německá	německý	k2eAgFnSc1d1	německá
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenku	myšlenka	k1gFnSc4	myšlenka
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
pak	pak	k6eAd1	pak
nadále	nadále	k6eAd1	nadále
prosazovaly	prosazovat	k5eAaImAgFnP	prosazovat
především	především	k9	především
obce	obec	k1gFnPc1	obec
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Zaznívaly	zaznívat	k5eAaImAgInP	zaznívat
i	i	k9	i
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
argumenty	argument	k1gInPc1	argument
<g/>
.	.	kIx.	.
</s>
<s>
Nebude	být	k5eNaImBp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
česká	český	k2eAgFnSc1d1	Česká
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
odliv	odliv	k1gInSc1	odliv
studentů	student	k1gMnPc2	student
na	na	k7c4	na
slovanská	slovanský	k2eAgNnPc4d1	slovanské
gymnázia	gymnázium	k1gNnPc4	gymnázium
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
město	město	k1gNnSc1	město
přijde	přijít	k5eAaPmIp3nS	přijít
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
studenti	student	k1gMnPc1	student
přinášejí	přinášet	k5eAaImIp3nP	přinášet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vyučování	vyučování	k1gNnSc2	vyučování
zřízení	zřízení	k1gNnSc2	zřízení
paralelních	paralelní	k2eAgFnPc2d1	paralelní
českých	český	k2eAgFnPc2d1	Česká
tříd	třída	k1gFnPc2	třída
na	na	k7c6	na
kroměřížském	kroměřížský	k2eAgNnSc6d1	Kroměřížské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
již	již	k6eAd1	již
od	od	k7c2	od
následujícího	následující	k2eAgInSc2d1	následující
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
obecní	obecní	k2eAgFnSc1d1	obecní
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
purkmistrem	purkmistr	k1gMnSc7	purkmistr
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Beneschem	Benesch	k1gMnSc7	Benesch
však	však	k8xC	však
návrh	návrh	k1gInSc4	návrh
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Pobouření	pobouření	k1gNnSc4	pobouření
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
kroměřížské	kroměřížský	k2eAgFnSc2d1	Kroměřížská
veřejnosti	veřejnost	k1gFnSc2	veřejnost
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
větší	veliký	k2eAgNnSc4d2	veliký
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městě	město	k1gNnSc6	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
existovala	existovat	k5eAaImAgFnS	existovat
i	i	k9	i
německá	německý	k2eAgFnSc1d1	německá
reálka	reálka	k1gFnSc1	reálka
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Justiční	justiční	k2eAgFnSc1d1	justiční
akademie	akademie	k1gFnSc1	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1882-	[number]	k4	1882-
podali	podat	k5eAaPmAgMnP	podat
advokáti	advokát	k1gMnPc1	advokát
Jan	Jan	k1gMnSc1	Jan
Kozánek	Kozánek	k1gMnSc1	Kozánek
a	a	k8xC	a
Vilibald	Vilibald	k1gMnSc1	Vilibald
Mildschuh	Mildschuh	k1gMnSc1	Mildschuh
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kanovníkem	kanovník	k1gMnSc7	kanovník
od	od	k7c2	od
sv.	sv.	kA	sv.
Mořice	Mořic	k1gMnPc4	Mořic
Františkem	František	k1gMnSc7	František
Dvořákem	Dvořák	k1gMnSc7	Dvořák
svým	svůj	k1gMnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
jménem	jméno	k1gNnSc7	jméno
dalších	další	k2eAgInPc2d1	další
patnácti	patnáct	k4xCc2	patnáct
kroměřížských	kroměřížský	k2eAgMnPc2d1	kroměřížský
občanů	občan	k1gMnPc2	občan
k	k	k7c3	k
c.	c.	k?	c.
k.	k.	k?	k.
zemské	zemský	k2eAgFnSc3d1	zemská
školní	školní	k2eAgFnSc3d1	školní
radě	rada	k1gFnSc3	rada
žádost	žádost	k1gFnSc4	žádost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
u	u	k7c2	u
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kultury	kultura	k1gFnSc2	kultura
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
čtyř	čtyři	k4xCgMnPc2	čtyři
tříd	třída	k1gFnPc2	třída
nižšího	nízký	k2eAgNnSc2d2	nižší
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
jedné	jeden	k4xCgFnSc2	jeden
třídy	třída	k1gFnSc2	třída
ke	k	k7c3	k
školnímu	školní	k2eAgInSc3d1	školní
roku	rok	k1gInSc3	rok
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zmíněný	zmíněný	k2eAgInSc4d1	zmíněný
ústav	ústav	k1gInSc4	ústav
se	se	k3xPyFc4	se
zavázali	zavázat	k5eAaPmAgMnP	zavázat
zajistit	zajistit	k5eAaPmF	zajistit
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
přišla	přijít	k5eAaPmAgFnS	přijít
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
málo	málo	k4c1	málo
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
až	až	k9	až
od	od	k7c2	od
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
nižší	nízký	k2eAgNnSc4d2	nižší
gymnázium	gymnázium	k1gNnSc4	gymnázium
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
–	–	k?	–
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Nově	nově	k6eAd1	nově
založené	založený	k2eAgNnSc1d1	založené
"	"	kIx"	"
<g/>
Družstvo	družstvo	k1gNnSc1	družstvo
gymnasiální	gymnasiální	k2eAgNnSc1d1	gymnasiální
<g/>
"	"	kIx"	"
začalo	začít	k5eAaPmAgNnS	začít
bezprostředně	bezprostředně	k6eAd1	bezprostředně
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
vše	všechen	k3xTgNnSc4	všechen
potřebné	potřebné	k1gNnSc4	potřebné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
škola	škola	k1gFnSc1	škola
mohla	moct	k5eAaImAgFnS	moct
od	od	k7c2	od
září	září	k1gNnSc2	září
zahájit	zahájit	k5eAaPmF	zahájit
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Prozatímním	prozatímní	k2eAgMnSc7d1	prozatímní
ředitelem	ředitel	k1gMnSc7	ředitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesor	profesor	k1gMnSc1	profesor
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
František	František	k1gMnSc1	František
Višňák	višňák	k1gMnSc1	višňák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
mateřském	mateřský	k2eAgInSc6d1	mateřský
ústavu	ústav	k1gInSc6	ústav
roční	roční	k2eAgFnSc4d1	roční
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
159	[number]	k4	159
žáků	žák	k1gMnPc2	žák
rozdělených	rozdělený	k2eAgMnPc2d1	rozdělený
do	do	k7c2	do
oddělení	oddělení	k1gNnSc2	oddělení
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
C.	C.	kA	C.
Prostory	prostora	k1gFnSc2	prostora
pro	pro	k7c4	pro
jedno	jeden	k4xCgNnSc4	jeden
oddělení	oddělení	k1gNnSc4	oddělení
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
piaristické	piaristický	k2eAgFnSc6d1	piaristická
koleji	kolej	k1gFnSc6	kolej
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
P.	P.	kA	P.
J.	J.	kA	J.
Vejvanovského	Vejvanovský	k2eAgNnSc2d1	Vejvanovský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgMnPc1d1	zbývající
dvě	dva	k4xCgFnPc4	dva
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Nadsklepí	nadsklepí	k1gNnSc2	nadsklepí
<g/>
.	.	kIx.	.
</s>
<s>
Školní	školní	k2eAgInSc1d1	školní
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
slavnostní	slavnostní	k2eAgFnSc7d1	slavnostní
bohoslužbou	bohoslužba	k1gFnSc7	bohoslužba
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
prvního	první	k4xOgInSc2	první
profesorského	profesorský	k2eAgInSc2d1	profesorský
sboru	sbor	k1gInSc2	sbor
bylo	být	k5eAaImAgNnS	být
následující	následující	k2eAgNnSc1d1	následující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Višňák	višňák	k1gMnSc1	višňák
–	–	k?	–
zatímní	zatímní	k2eAgMnSc1d1	zatímní
ředitel	ředitel	k1gMnSc1	ředitel
–	–	k?	–
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
čeština	čeština	k1gFnSc1	čeština
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Krejčiřík	Krejčiřík	k1gMnSc1	Krejčiřík
–	–	k?	–
farář	farář	k1gMnSc1	farář
na	na	k7c6	na
dočasném	dočasný	k2eAgInSc6d1	dočasný
odpočinku	odpočinek	k1gInSc6	odpočinek
–	–	k?	–
náboženství	náboženství	k1gNnSc2	náboženství
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Klvaňa	Klvaňa	k1gMnSc1	Klvaňa
–	–	k?	–
suplent	suplent	k1gMnSc1	suplent
–	–	k?	–
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
přírodopis	přírodopis	k1gInSc1	přírodopis
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Fialka	fialka	k1gFnSc1	fialka
–	–	k?	–
suplent	suplent	k1gMnSc1	suplent
–	–	k?	–
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Sloupský	Sloupský	k2eAgMnSc1d1	Sloupský
–	–	k?	–
suplent	suplent	k1gMnSc1	suplent
–	–	k?	–
zeměpis	zeměpis	k1gInSc1	zeměpis
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
tělocvik	tělocvik	k1gInSc1	tělocvik
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Klvaňa	Klvaňa	k1gMnSc1	Klvaňa
–	–	k?	–
suplent	suplent	k1gMnSc1	suplent
–	–	k?	–
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
němčinaZpěvu	němčinaZpěv	k1gInSc2	němčinaZpěv
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
externě	externě	k6eAd1	externě
Jan	Jan	k1gMnSc1	Jan
Talich	Talich	k1gMnSc1	Talich
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Václava	Václav	k1gMnSc2	Václav
Talicha	Talich	k1gMnSc2	Talich
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc4	dva
učitele	učitel	k1gMnPc4	učitel
jménem	jméno	k1gNnSc7	jméno
Klvaňa	Klvaňus	k1gMnSc2	Klvaňus
si	se	k3xPyFc3	se
studenti	student	k1gMnPc1	student
rozlišili	rozlišit	k5eAaPmAgMnP	rozlišit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednomu	jeden	k4xCgMnSc3	jeden
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
Klvaňka	Klvaňka	k1gFnSc1	Klvaňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
druhého	druhý	k4xOgInSc2	druhý
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
dohodou	dohoda	k1gFnSc7	dohoda
postátněno	postátnit	k5eAaPmNgNnS	postátnit
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
prostory	prostora	k1gFnPc4	prostora
a	a	k8xC	a
vybavení	vybavení	k1gNnSc4	vybavení
však	však	k9	však
nadále	nadále	k6eAd1	nadále
neslo	nést	k5eAaImAgNnS	nést
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
postátněného	postátněný	k2eAgInSc2d1	postátněný
ústavu	ústav	k1gInSc2	ústav
byl	být	k5eAaImAgInS	být
definitivně	definitivně	k6eAd1	definitivně
jmenován	jmenován	k2eAgMnSc1d1	jmenován
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
ředitel	ředitel	k1gMnSc1	ředitel
František	František	k1gMnSc1	František
Višňák	višňák	k1gMnSc1	višňák
,	,	kIx,	,
škola	škola	k1gFnSc1	škola
získala	získat	k5eAaPmAgFnS	získat
další	další	k2eAgFnPc4d1	další
prostory	prostora	k1gFnPc4	prostora
pronájmem	pronájem	k1gInSc7	pronájem
v	v	k7c6	v
domě	dům	k1gInSc6	dům
paní	paní	k1gFnSc2	paní
Kubesové	Kubesový	k2eAgFnSc2d1	Kubesová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvního	první	k4xOgInSc2	první
listopadu	listopad	k1gInSc2	listopad
1883	[number]	k4	1883
se	se	k3xPyFc4	se
družstvo	družstvo	k1gNnSc1	družstvo
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
na	na	k7c4	na
obecní	obecní	k2eAgFnSc4d1	obecní
radu	rada	k1gFnSc4	rada
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
materiální	materiální	k2eAgNnSc1d1	materiální
zajištění	zajištění	k1gNnSc1	zajištění
školy	škola	k1gFnSc2	škola
převzalo	převzít	k5eAaPmAgNnS	převzít
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
kroměřížských	kroměřížský	k2eAgMnPc2d1	kroměřížský
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
daněmi	daň	k1gFnPc7	daň
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
německých	německý	k2eAgFnPc2d1	německá
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
spravedlivé	spravedlivý	k2eAgNnSc1d1	spravedlivé
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
stejnou	stejný	k2eAgFnSc4d1	stejná
podporu	podpora	k1gFnSc4	podpora
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
škola	škola	k1gFnSc1	škola
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazňovali	zdůrazňovat	k5eAaImAgMnP	zdůrazňovat
i	i	k8xC	i
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
přínos	přínos	k1gInSc1	přínos
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
představují	představovat	k5eAaImIp3nP	představovat
čtyři	čtyři	k4xCgFnPc1	čtyři
stovky	stovka	k1gFnPc1	stovka
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
dětí	dítě	k1gFnPc2	dítě
zámožných	zámožný	k2eAgInPc2d1	zámožný
sedláků	sedlák	k1gInPc2	sedlák
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Obecní	obecní	k2eAgInSc1d1	obecní
výbor	výbor	k1gInSc1	výbor
návrh	návrh	k1gInSc1	návrh
jednohlasně	jednohlasně	k6eAd1	jednohlasně
schválil	schválit	k5eAaPmAgInS	schválit
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1884	[number]	k4	1884
si	se	k3xPyFc3	se
však	však	k9	však
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kozánek	Kozánek	k1gMnSc1	Kozánek
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
zatím	zatím	k6eAd1	zatím
nedalo	dát	k5eNaPmAgNnS	dát
ani	ani	k8xC	ani
krejcar	krejcar	k1gInSc4	krejcar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
postoupili	postoupit	k5eAaPmAgMnP	postoupit
první	první	k4xOgMnPc1	první
studenti	student	k1gMnPc1	student
do	do	k7c2	do
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
ročníku	ročník	k1gInSc2	ročník
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
začít	začít	k5eAaPmF	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
gymnázia	gymnázium	k1gNnSc2	gymnázium
na	na	k7c4	na
úplné	úplný	k2eAgFnPc4d1	úplná
osmileté	osmiletý	k2eAgFnPc4d1	osmiletá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
uspěli	uspět	k5eAaPmAgMnP	uspět
čeští	český	k2eAgMnPc1d1	český
kandidáti	kandidát	k1gMnPc1	kandidát
v	v	k7c6	v
obecních	obecní	k2eAgFnPc6d1	obecní
volbách	volba	k1gFnPc6	volba
a	a	k8xC	a
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
se	se	k3xPyFc4	se
vyrovnaly	vyrovnat	k5eAaBmAgFnP	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Obecní	obecní	k2eAgInSc1d1	obecní
výbor	výbor	k1gInSc1	výbor
tedy	tedy	k9	tedy
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
s	s	k7c7	s
postavením	postavení	k1gNnSc7	postavení
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
čeští	český	k2eAgMnPc1d1	český
zastupitelé	zastupitel	k1gMnPc1	zastupitel
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
podpořili	podpořit	k5eAaPmAgMnP	podpořit
stavbu	stavba	k1gFnSc4	stavba
německé	německý	k2eAgFnSc2d1	německá
dívčí	dívčí	k2eAgFnSc2d1	dívčí
školy	škola	k1gFnSc2	škola
obecné	obecný	k2eAgFnSc2d1	obecná
a	a	k8xC	a
občanské	občanský	k2eAgFnSc2d1	občanská
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
však	však	k9	však
zhatil	zhatit	k5eAaPmAgMnS	zhatit
ministr	ministr	k1gMnSc1	ministr
kultu	kult	k1gInSc2	kult
a	a	k8xC	a
vyučování	vyučování	k1gNnSc2	vyučování
baron	baron	k1gMnSc1	baron
Gautsch	Gautsch	k1gMnSc1	Gautsch
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žádost	žádost	k1gFnSc4	žádost
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Pobouření	pobouření	k1gNnSc4	pobouření
české	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
bylo	být	k5eAaImAgNnS	být
obrovské	obrovský	k2eAgNnSc1d1	obrovské
<g/>
,	,	kIx,	,
úřady	úřad	k1gInPc1	úřad
však	však	k9	však
žádost	žádost	k1gFnSc1	žádost
opakovaně	opakovaně	k6eAd1	opakovaně
zamítaly	zamítat	k5eAaImAgFnP	zamítat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
rozšíření	rozšíření	k1gNnSc6	rozšíření
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
momentálně	momentálně	k6eAd1	momentálně
nedostává	dostávat	k5eNaImIp3nS	dostávat
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
poměr	poměr	k1gInSc4	poměr
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
německých	německý	k2eAgFnPc2d1	německá
škol	škola	k1gFnPc2	škola
není	být	k5eNaImIp3nS	být
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
pouze	pouze	k6eAd1	pouze
množství	množství	k1gNnSc2	množství
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
jeho	jeho	k3xOp3gInPc4	jeho
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
a	a	k8xC	a
sociální	sociální	k2eAgInPc4d1	sociální
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c6	o
nasměrování	nasměrování	k1gNnSc6	nasměrování
studentů	student	k1gMnPc2	student
na	na	k7c4	na
živnostenské	živnostenský	k2eAgFnPc4d1	Živnostenská
školy	škola	k1gFnPc4	škola
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
představami	představa	k1gFnPc7	představa
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Záležitost	záležitost	k1gFnSc1	záležitost
se	se	k3xPyFc4	se
projednávala	projednávat	k5eAaImAgFnS	projednávat
i	i	k9	i
v	v	k7c6	v
poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
Češi	Čech	k1gMnPc1	Čech
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
obecní	obecní	k2eAgFnPc4d1	obecní
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
prvním	první	k4xOgMnPc3	první
českým	český	k2eAgMnPc3d1	český
starostou	starosta	k1gMnSc7	starosta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kulp	Kulp	k1gMnSc1	Kulp
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
tak	tak	k6eAd1	tak
získalo	získat	k5eAaPmAgNnS	získat
jasnou	jasný	k2eAgFnSc4d1	jasná
podporu	podpora	k1gFnSc4	podpora
obecní	obecní	k2eAgFnSc2d1	obecní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
tábor	tábor	k1gInSc1	tábor
lidu	lid	k1gInSc2	lid
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
ministerskému	ministerský	k2eAgNnSc3d1	ministerské
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1888	[number]	k4	1888
podala	podat	k5eAaPmAgFnS	podat
rada	rada	k1gFnSc1	rada
další	další	k2eAgFnSc4d1	další
žádost	žádost	k1gFnSc4	žádost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
císařova	císařův	k2eAgNnSc2d1	císařovo
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
/	/	kIx~	/
<g/>
89	[number]	k4	89
bylo	být	k5eAaImAgNnS	být
kroměřížské	kroměřížský	k2eAgNnSc1d1	Kroměřížské
gymnázium	gymnázium	k1gNnSc1	gymnázium
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
na	na	k7c4	na
úplné	úplný	k2eAgNnSc4d1	úplné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úplné	úplný	k2eAgNnSc1d1	úplné
osmileté	osmiletý	k2eAgNnSc1d1	osmileté
gymnázium	gymnázium	k1gNnSc1	gymnázium
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Ihned	ihned	k6eAd1	ihned
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
přípravy	příprava	k1gFnPc1	příprava
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
parcela	parcela	k1gFnSc1	parcela
za	za	k7c7	za
Kovářskou	kovářský	k2eAgFnSc7d1	Kovářská
branou	brána	k1gFnSc7	brána
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vojenské	vojenský	k2eAgFnSc2d1	vojenská
nemocnice	nemocnice	k1gFnSc2	nemocnice
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
sousedních	sousední	k2eAgInPc2d1	sousední
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bouráním	bourání	k1gNnSc7	bourání
těchto	tento	k3xDgFnPc2	tento
staveb	stavba	k1gFnPc2	stavba
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
stavět	stavět	k5eAaImF	stavět
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
prvního	první	k4xOgInSc2	první
července	červenec	k1gInSc2	červenec
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
brněnských	brněnský	k2eAgMnPc2d1	brněnský
architektů	architekt	k1gMnPc2	architekt
Julia	Julius	k1gMnSc2	Julius
Tebicha	Tebich	k1gMnSc2	Tebich
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Karáska	Karásek	k1gMnSc2	Karásek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
dodavatelem	dodavatel	k1gMnSc7	dodavatel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výhodné	výhodný	k2eAgFnSc2d1	výhodná
nabídky	nabídka	k1gFnSc2	nabídka
kroměřížský	kroměřížský	k2eAgMnSc1d1	kroměřížský
stavitel	stavitel	k1gMnSc1	stavitel
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
obecního	obecní	k2eAgInSc2d1	obecní
výboru	výbor	k1gInSc2	výbor
Ladislav	Ladislav	k1gMnSc1	Ladislav
Mesenský	Mesenský	k2eAgInSc1d1	Mesenský
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
svěcení	svěcení	k1gNnSc1	svěcení
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
21	[number]	k4	21
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
řádná	řádný	k2eAgFnSc1d1	řádná
výuka	výuka	k1gFnSc1	výuka
začala	začít	k5eAaPmAgFnS	začít
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
sloužila	sloužit	k5eAaImAgFnS	sloužit
od	od	k7c2	od
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
i	i	k9	i
nově	nově	k6eAd1	nově
zřízenému	zřízený	k2eAgInSc3d1	zřízený
učitelskému	učitelský	k2eAgInSc3d1	učitelský
ústavu	ústav	k1gInSc3	ústav
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
maturity	maturita	k1gFnPc1	maturita
probíhaly	probíhat	k5eAaImAgFnP	probíhat
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
provozu	provoz	k1gInSc2	provoz
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pohromě	pohroma	k1gFnSc3	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1892	[number]	k4	1892
a	a	k8xC	a
93	[number]	k4	93
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
podlahami	podlaha	k1gFnPc7	podlaha
objevena	objevit	k5eAaPmNgFnS	objevit
plíseň	plíseň	k1gFnSc1	plíseň
<g/>
,	,	kIx,	,
v	v	k7c6	v
místnostech	místnost	k1gFnPc6	místnost
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgInS	šířit
zápach	zápach	k1gInSc1	zápach
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
havarijním	havarijní	k2eAgInSc6d1	havarijní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
si	se	k3xPyFc3	se
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
bytu	byt	k1gInSc2	byt
zatéká	zatékat	k5eAaImIp3nS	zatékat
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
plíseň	plíseň	k1gFnSc1	plíseň
a	a	k8xC	a
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
pokoje	pokoj	k1gInSc2	pokoj
prosakuje	prosakovat	k5eAaImIp3nS	prosakovat
močůvka	močůvka	k1gFnSc1	močůvka
z	z	k7c2	z
chlapeckých	chlapecký	k2eAgInPc2d1	chlapecký
záchodů	záchod	k1gInPc2	záchod
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
měl	mít	k5eAaImAgMnS	mít
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
statice	statika	k1gFnSc6	statika
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
křídle	křídlo	k1gNnSc6	křídlo
učitelského	učitelský	k2eAgInSc2d1	učitelský
ústavu	ústav	k1gInSc2	ústav
začala	začít	k5eAaPmAgFnS	začít
sesedat	sesedat	k5eAaPmF	sesedat
chodba	chodba	k1gFnSc1	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
přehnaný	přehnaný	k2eAgInSc1d1	přehnaný
spěch	spěch	k1gInSc1	spěch
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Stavitel	stavitel	k1gMnSc1	stavitel
Mesenský	Mesenský	k2eAgMnSc1d1	Mesenský
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
problémů	problém	k1gInPc2	problém
vlhkou	vlhký	k2eAgFnSc4d1	vlhká
hlínu	hlína	k1gFnSc4	hlína
z	z	k7c2	z
městské	městský	k2eAgFnSc2d1	městská
cihelny	cihelna	k1gFnSc2	cihelna
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dodala	dodat	k5eAaPmAgFnS	dodat
na	na	k7c4	na
násepy	násep	k1gInPc4	násep
podlah	podlaha	k1gFnPc2	podlaha
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
a	a	k8xC	a
průtrž	průtrž	k1gFnSc4	průtrž
mračen	mračno	k1gNnPc2	mračno
během	během	k7c2	během
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
budova	budova	k1gFnSc1	budova
navlhla	navlhnout	k5eAaPmAgFnS	navlhnout
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavitel	stavitel	k1gMnSc1	stavitel
ručí	ručit	k5eAaImIp3nS	ručit
za	za	k7c4	za
práce	práce	k1gFnPc4	práce
jen	jen	k9	jen
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
od	od	k7c2	od
kolaudace	kolaudace	k1gFnSc2	kolaudace
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
uhradí	uhradit	k5eAaPmIp3nS	uhradit
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
5	[number]	k4	5
000	[number]	k4	000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
když	když	k8xS	když
město	město	k1gNnSc1	město
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
zbylých	zbylý	k2eAgNnPc2d1	zbylé
9	[number]	k4	9
813	[number]	k4	813
zl	zl	k?	zl
<g/>
.	.	kIx.	.
a	a	k8xC	a
dodá	dodat	k5eAaPmIp3nS	dodat
25	[number]	k4	25
000	[number]	k4	000
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
prací	práce	k1gFnPc2	práce
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
února	únor	k1gInSc2	únor
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
státního	státní	k2eAgMnSc2d1	státní
technika	technik	k1gMnSc2	technik
měla	mít	k5eAaImAgFnS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
během	během	k7c2	během
hlavních	hlavní	k2eAgFnPc2d1	hlavní
prázdnin	prázdniny	k1gFnPc2	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
prodražila	prodražit	k5eAaPmAgFnS	prodražit
<g/>
,	,	kIx,	,
škola	škola	k1gFnSc1	škola
však	však	k9	však
získala	získat	k5eAaPmAgFnS	získat
řádnou	řádný	k2eAgFnSc4d1	řádná
konstrukci	konstrukce	k1gFnSc4	konstrukce
a	a	k8xC	a
kvalitní	kvalitní	k2eAgFnPc4d1	kvalitní
podlahy	podlaha	k1gFnPc4	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
zatím	zatím	k6eAd1	zatím
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
Lichtenštejnském	lichtenštejnský	k2eAgInSc6d1	lichtenštejnský
semináři	seminář	k1gInSc6	seminář
v	v	k7c6	v
Jánské	Jánské	k2eAgFnSc6d1	Jánské
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
v	v	k7c6	v
měšťance	měšťanka	k1gFnSc6	měšťanka
v	v	k7c6	v
Oskoli	Oskole	k1gFnSc6	Oskole
<g/>
.	.	kIx.	.
</s>
<s>
Kolaudace	kolaudace	k1gFnSc1	kolaudace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
již	již	k6eAd1	již
za	za	k7c2	za
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
února	únor	k1gInSc2	únor
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
si	se	k3xPyFc3	se
ředitel	ředitel	k1gMnSc1	ředitel
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
opět	opět	k6eAd1	opět
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
7	[number]	k4	7
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1896	[number]	k4	1896
ředitel	ředitel	k1gMnSc1	ředitel
Višňák	višňák	k1gMnSc1	višňák
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
nemoci	nemoc	k1gFnSc6	nemoc
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
krátce	krátce	k6eAd1	krátce
řídil	řídit	k5eAaImAgMnS	řídit
Bedřich	Bedřich	k1gMnSc1	Bedřich
Fialka	fialka	k1gFnSc1	fialka
<g/>
,	,	kIx,	,
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1897	[number]	k4	1897
se	se	k3xPyFc4	se
ředitelem	ředitel	k1gMnSc7	ředitel
stal	stát	k5eAaPmAgMnS	stát
Rudolf	Rudolf	k1gMnSc1	Rudolf
baron	baron	k1gMnSc1	baron
Henniger	Henniger	k1gMnSc1	Henniger
z	z	k7c2	z
Eberku	Eberek	k1gInSc2	Eberek
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
učitel	učitel	k1gMnSc1	učitel
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
okresní	okresní	k2eAgMnSc1d1	okresní
školní	školní	k2eAgMnSc1d1	školní
inspektor	inspektor	k1gMnSc1	inspektor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
založena	založen	k2eAgFnSc1d1	založena
česká	český	k2eAgFnSc1d1	Česká
reálka	reálka	k1gFnSc1	reálka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Střední	střední	k2eAgFnSc1d1	střední
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
Hennigerovým	Hennigerův	k2eAgNnSc7d1	Hennigerův
vedením	vedení	k1gNnSc7	vedení
prožila	prožít	k5eAaPmAgFnS	prožít
škola	škola	k1gFnSc1	škola
klidné	klidný	k2eAgNnSc1d1	klidné
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
elektrické	elektrický	k2eAgNnSc1d1	elektrické
osvětlení	osvětlení	k1gNnSc1	osvětlení
v	v	k7c6	v
učebně	učebna	k1gFnSc6	učebna
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
vylepšeno	vylepšen	k2eAgNnSc4d1	vylepšeno
plynové	plynový	k2eAgNnSc4d1	plynové
osvětlení	osvětlení	k1gNnSc4	osvětlení
zbytku	zbytek	k1gInSc2	zbytek
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
vodovod	vodovod	k1gInSc1	vodovod
s	s	k7c7	s
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
splachovací	splachovací	k2eAgInPc4d1	splachovací
záchody	záchod	k1gInPc4	záchod
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
tělocvičnu	tělocvična	k1gFnSc4	tělocvična
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
využívala	využívat	k5eAaPmAgFnS	využívat
i	i	k9	i
tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
k	k	k7c3	k
provozování	provozování	k1gNnSc3	provozování
venkovních	venkovní	k2eAgFnPc2d1	venkovní
her	hra	k1gFnPc2	hra
sloužilo	sloužit	k5eAaImAgNnS	sloužit
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
hřiště	hřiště	k1gNnSc2	hřiště
na	na	k7c6	na
Rejdišti	rejdiště	k1gNnSc6	rejdiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
mj.	mj.	kA	mj.
i	i	k8xC	i
ředitele	ředitel	k1gMnSc4	ředitel
Hennigera	Henniger	k1gMnSc4	Henniger
vydala	vydat	k5eAaPmAgFnS	vydat
obecní	obecní	k2eAgFnSc1d1	obecní
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
vyhlášku	vyhláška	k1gFnSc4	vyhláška
zakazující	zakazující	k2eAgFnSc1d1	zakazující
studující	studující	k2eAgFnSc3d1	studující
mládeži	mládež	k1gFnSc3	mládež
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
hostincích	hostinec	k1gInPc6	hostinec
bez	bez	k7c2	bez
doprovodu	doprovod	k1gInSc6	doprovod
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
odešel	odejít	k5eAaPmAgMnS	odejít
Henniger	Henniger	k1gInSc4	Henniger
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
a	a	k8xC	a
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Bedřich	Bedřich	k1gMnSc1	Bedřich
Fialka	fialka	k1gFnSc1	fialka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
mnohé	mnohý	k2eAgFnPc1d1	mnohá
školy	škola	k1gFnPc1	škola
zabrány	zabrán	k2eAgFnPc1d1	zabrána
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgInPc4d1	vojenský
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
posloužilo	posloužit	k5eAaPmAgNnS	posloužit
jako	jako	k8xC	jako
kasárna	kasárna	k1gNnPc4	kasárna
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
však	však	k9	však
muselo	muset	k5eAaImAgNnS	muset
sdílet	sdílet	k5eAaImF	sdílet
svou	svůj	k3xOyFgFnSc4	svůj
budovu	budova	k1gFnSc4	budova
s	s	k7c7	s
reálkou	reálka	k1gFnSc7	reálka
přeměněnou	přeměněný	k2eAgFnSc7d1	přeměněná
na	na	k7c4	na
lazaret	lazaret	k1gInSc4	lazaret
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
odvedením	odvedení	k1gNnSc7	odvedení
některých	některý	k3yIgMnPc2	některý
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
starších	starý	k2eAgMnPc2d2	starší
studentů	student	k1gMnPc2	student
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Československa	Československo	k1gNnSc2	Československo
po	po	k7c4	po
nástup	nástup	k1gInSc4	nástup
komunismu	komunismus	k1gInSc2	komunismus
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
začali	začít	k5eAaPmAgMnP	začít
rodiče	rodič	k1gMnPc1	rodič
hromadně	hromadně	k6eAd1	hromadně
odvolávat	odvolávat	k5eAaImF	odvolávat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
manifestaci	manifestace	k1gFnSc6	manifestace
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
rezoluce	rezoluce	k1gFnSc1	rezoluce
za	za	k7c4	za
zrušení	zrušení	k1gNnSc4	zrušení
německého	německý	k2eAgNnSc2d1	německé
gymnázia	gymnázium	k1gNnSc2	gymnázium
a	a	k8xC	a
reálky	reálka	k1gFnSc2	reálka
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
školy	škola	k1gFnPc1	škola
byly	být	k5eAaImAgFnP	být
skutečně	skutečně	k6eAd1	skutečně
zanedlouho	zanedlouho	k6eAd1	zanedlouho
zrušeny	zrušit	k5eAaPmNgInP	zrušit
jako	jako	k8xS	jako
zbytečné	zbytečný	k2eAgInPc1d1	zbytečný
<g/>
,	,	kIx,	,
do	do	k7c2	do
bývalé	bývalý	k2eAgFnSc2d1	bývalá
německé	německý	k2eAgFnSc2d1	německá
reálky	reálka	k1gFnSc2	reálka
se	se	k3xPyFc4	se
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
přesunul	přesunout	k5eAaPmAgInS	přesunout
učitelský	učitelský	k2eAgInSc1d1	učitelský
ústav	ústav	k1gInSc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nadále	nadále	k6eAd1	nadále
však	však	k9	však
gymnázium	gymnázium	k1gNnSc1	gymnázium
sdílelo	sdílet	k5eAaImAgNnS	sdílet
své	svůj	k3xOyFgFnPc4	svůj
prostory	prostora	k1gFnPc4	prostora
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
školami	škola	k1gFnPc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
systému	systém	k1gInSc6	systém
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
přijímat	přijímat	k5eAaImF	přijímat
dívky	dívka	k1gFnPc4	dívka
jako	jako	k8xC	jako
řádné	řádný	k2eAgFnPc4d1	řádná
žákyně	žákyně	k1gFnPc4	žákyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1929	[number]	k4	1929
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
postupná	postupný	k2eAgFnSc1d1	postupná
přeměna	přeměna	k1gFnSc1	přeměna
ústavu	ústav	k1gInSc2	ústav
v	v	k7c4	v
reálné	reálný	k2eAgNnSc4d1	reálné
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
ředitel	ředitel	k1gMnSc1	ředitel
Bedřich	Bedřich	k1gMnSc1	Bedřich
Fialka	fialka	k1gFnSc1	fialka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
zanedlouho	zanedlouho	k6eAd1	zanedlouho
stal	stát	k5eAaPmAgMnS	stát
Jiří	Jiří	k1gMnSc1	Jiří
Malovaný	malovaný	k2eAgMnSc1d1	malovaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
mohutné	mohutný	k2eAgFnPc1d1	mohutná
oslavy	oslava	k1gFnPc1	oslava
padesátého	padesátý	k4xOgNnSc2	padesátý
výročí	výročí	k1gNnSc2	výročí
založení	založení	k1gNnSc2	založení
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1934	[number]	k4	1934
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
ředitele	ředitel	k1gMnSc2	ředitel
Karel	Karel	k1gMnSc1	Karel
Kepert	Kepert	k1gMnSc1	Kepert
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
čím	čí	k3xOyQgInSc7	čí
dál	daleko	k6eAd2	daleko
větším	veliký	k2eAgInSc7d2	veliký
problémem	problém	k1gInSc7	problém
stával	stávat	k5eAaImAgInS	stávat
nedostatek	nedostatek	k1gInSc4	nedostatek
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
celé	celý	k2eAgFnSc2d1	celá
historické	historický	k2eAgFnSc2d1	historická
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
prozatím	prozatím	k6eAd1	prozatím
však	však	k9	však
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
tři	tři	k4xCgFnPc4	tři
učebny	učebna	k1gFnPc4	učebna
vyklizené	vyklizený	k2eAgInPc1d1	vyklizený
Jungmannovou	Jungmannový	k2eAgFnSc7d1	Jungmannová
chlapeckou	chlapecký	k2eAgFnSc7d1	chlapecká
školou	škola	k1gFnSc7	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
opakovala	opakovat	k5eAaImAgFnS	opakovat
situace	situace	k1gFnSc1	situace
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
muselo	muset	k5eAaImAgNnS	muset
poskytovat	poskytovat	k5eAaImF	poskytovat
prostory	prostor	k1gInPc1	prostor
reálce	reálka	k1gFnSc3	reálka
a	a	k8xC	a
Arcibiskupskému	arcibiskupský	k2eAgNnSc3d1	Arcibiskupské
gymnáziu	gymnázium	k1gNnSc3	gymnázium
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
budovy	budova	k1gFnPc4	budova
zabrali	zabrat	k5eAaPmAgMnP	zabrat
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
opustili	opustit	k5eAaPmAgMnP	opustit
studenti	student	k1gMnPc1	student
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1941	[number]	k4	1941
se	se	k3xPyFc4	se
kroměřížská	kroměřížský	k2eAgFnSc1d1	Kroměřížská
reálka	reálka	k1gFnSc1	reálka
postupně	postupně	k6eAd1	postupně
transformuje	transformovat	k5eAaBmIp3nS	transformovat
v	v	k7c4	v
reálné	reálný	k2eAgNnSc4d1	reálné
gymnázium	gymnázium	k1gNnSc4	gymnázium
Na	na	k7c6	na
příkopech	příkop	k1gInPc6	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
ředitele	ředitel	k1gMnSc2	ředitel
gymnázia	gymnázium	k1gNnSc2	gymnázium
se	se	k3xPyFc4	se
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
E.	E.	kA	E.
Čecháček	čecháček	k1gMnSc1	čecháček
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Koupil	koupit	k5eAaPmAgInS	koupit
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
E.	E.	kA	E.
Čecháček	čecháček	k1gMnSc1	čecháček
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
zatímní	zatímní	k2eAgMnSc1d1	zatímní
správce	správce	k1gMnSc1	správce
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
bývalé	bývalý	k2eAgFnSc2d1	bývalá
reálky	reálka	k1gFnSc2	reálka
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jindřich	Jindřich	k1gMnSc1	Jindřich
Březina	Březina	k1gMnSc1	Březina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
zahynul	zahynout	k5eAaPmAgMnS	zahynout
sokolský	sokolský	k2eAgMnSc1d1	sokolský
funkcionář	funkcionář	k1gMnSc1	funkcionář
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
zeměpisu	zeměpis	k1gInSc2	zeměpis
a	a	k8xC	a
tělocviku	tělocvik	k1gInSc2	tělocvik
Václav	Václav	k1gMnSc1	Václav
Sedláček	Sedláček	k1gMnSc1	Sedláček
a	a	k8xC	a
student	student	k1gMnSc1	student
Miroslav	Miroslav	k1gMnSc1	Miroslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
prezidenta	prezident	k1gMnSc2	prezident
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
1943	[number]	k4	1943
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
kroměřížské	kroměřížský	k2eAgFnPc1d1	Kroměřížská
střední	střední	k2eAgFnPc1d1	střední
školy	škola	k1gFnPc1	škola
sloučeny	sloučen	k2eAgFnPc1d1	sloučena
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Josefa	Josef	k1gMnSc2	Josef
Hilgartnera	Hilgartner	k1gMnSc2	Hilgartner
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
zakrátko	zakrátko	k6eAd1	zakrátko
opět	opět	k6eAd1	opět
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Březina	Březina	k1gMnSc1	Březina
<g/>
.	.	kIx.	.
</s>
<s>
Množila	množit	k5eAaImAgNnP	množit
se	se	k3xPyFc4	se
udání	udání	k1gNnPc1	udání
<g/>
,	,	kIx,	,
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přesouvaly	přesouvat	k5eAaImAgFnP	přesouvat
další	další	k2eAgFnPc1d1	další
vzdělávací	vzdělávací	k2eAgFnPc1d1	vzdělávací
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
byli	být	k5eAaImAgMnP	být
odesíláni	odesílat	k5eAaImNgMnP	odesílat
na	na	k7c4	na
nucené	nucený	k2eAgFnPc4d1	nucená
práce	práce	k1gFnPc4	práce
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
je	být	k5eAaImIp3nS	být
vyučování	vyučování	k1gNnSc4	vyučování
redukováno	redukován	k2eAgNnSc4d1	redukováno
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1945	[number]	k4	1945
obsadili	obsadit	k5eAaPmAgMnP	obsadit
budovu	budova	k1gFnSc4	budova
maďarští	maďarský	k2eAgMnPc1d1	maďarský
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
přidala	přidat	k5eAaPmAgFnS	přidat
se	se	k3xPyFc4	se
epidemie	epidemie	k1gFnSc1	epidemie
tyfu	tyf	k1gInSc2	tyf
a	a	k8xC	a
výuka	výuka	k1gFnSc1	výuka
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
znemožněna	znemožnit	k5eAaPmNgFnS	znemožnit
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
přerušena	přerušen	k2eAgFnSc1d1	přerušena
18	[number]	k4	18
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
byla	být	k5eAaImAgFnS	být
osvobozena	osvobodit	k5eAaPmNgFnS	osvobodit
ve	v	k7c6	v
dnech	den	k1gInPc6	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
6	[number]	k4	6
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
vyučování	vyučování	k1gNnSc1	vyučování
bylo	být	k5eAaImAgNnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
jako	jako	k8xS	jako
humanitní	humanitní	k2eAgInSc1d1	humanitní
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
neslo	nést	k5eAaImAgNnS	nést
název	název	k1gInSc4	název
Benešovo	Benešův	k2eAgNnSc4d1	Benešovo
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
opět	opět	k6eAd1	opět
Josef	Josef	k1gMnSc1	Josef
Hilgartner	Hilgartner	k1gMnSc1	Hilgartner
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskupské	arcibiskupský	k2eAgNnSc1d1	Arcibiskupské
gymnázium	gymnázium	k1gNnSc1	gymnázium
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
zrušenou	zrušený	k2eAgFnSc4d1	zrušená
reálku	reálka	k1gFnSc4	reálka
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
nové	nový	k2eAgNnSc1d1	nové
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
reálné	reálný	k2eAgNnSc1d1	reálné
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
však	však	k9	však
neměl	mít	k5eNaImAgInS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
bylo	být	k5eAaImAgNnS	být
gymnázium	gymnázium	k1gNnSc1	gymnázium
redukováno	redukovat	k5eAaBmNgNnS	redukovat
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgFnPc4d1	čtyřletá
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
Benešovo	Benešův	k2eAgNnSc4d1	Benešovo
byl	být	k5eAaImAgMnS	být
opuštěn	opuštěn	k2eAgMnSc1d1	opuštěn
a	a	k8xC	a
ředitelem	ředitel	k1gMnSc7	ředitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Josef	Josef	k1gMnSc1	Josef
Zelingr	Zelingr	k1gMnSc1	Zelingr
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskupské	arcibiskupský	k2eAgNnSc1d1	Arcibiskupské
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
reálné	reálný	k2eAgNnSc1d1	reálné
gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
učitelský	učitelský	k2eAgInSc1d1	učitelský
ústav	ústav	k1gInSc1	ústav
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgInP	zrušit
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc6	první
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
gymnasium	gymnasium	k1gNnSc1	gymnasium
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1	Moravská
orlice	orlice	k1gFnSc1	orlice
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
130	[number]	k4	130
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PÁLKA	Pálka	k1gMnSc1	Pálka
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
počátkům	počátek	k1gInPc3	počátek
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Sborník	sborník	k1gInSc1	sborník
Muzea	muzeum	k1gNnSc2	muzeum
kroměřížska	kroměřížsk	k1gInSc2	kroměřížsk
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
Kroměřížska	Kroměřížsk	k1gInSc2	Kroměřížsk
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85945	[number]	k4	85945
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
37	[number]	k4	37
<g/>
–	–	k?	–
<g/>
71	[number]	k4	71
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1882	[number]	k4	1882
-	-	kIx~	-
1972	[number]	k4	1972
České	český	k2eAgInPc1d1	český
gymnasium	gymnasium	k1gNnSc4	gymnasium
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Josef	Josef	k1gMnSc1	Josef
Šubert	Šubert	k1gMnSc1	Šubert
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Gymnasium	gymnasium	k1gNnSc1	gymnasium
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
100	[number]	k4	100
let	léto	k1gNnPc2	léto
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Lajkep	Lajkep	k1gMnSc1	Lajkep
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Sdružení	sdružení	k1gNnSc1	sdružení
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
školy	škola	k1gFnSc2	škola
při	při	k7c6	při
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
110	[number]	k4	110
let	léto	k1gNnPc2	léto
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Malcherová	Malcherová	k1gFnSc1	Malcherová
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Sdružení	sdružení	k1gNnSc1	sdružení
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
gymnázia	gymnázium	k1gNnSc2	gymnázium
Kroměříž	Kroměříž	k1gFnSc4	Kroměříž
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
120	[number]	k4	120
let	léto	k1gNnPc2	léto
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Malcherová	Malcherová	k1gFnSc1	Malcherová
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Sdružení	sdružení	k1gNnSc1	sdružení
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
gymnázia	gymnázium	k1gNnSc2	gymnázium
Kroměříž	Kroměříž	k1gFnSc4	Kroměříž
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
