<p>
<s>
Joe	Joe	k?	Joe
Kelly	Kella	k1gFnPc1	Kella
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Dublin	Dublin	k1gInSc1	Dublin
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
Neston	Neston	k1gInSc1	Neston
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
irský	irský	k2eAgMnSc1d1	irský
pilot	pilot	k1gMnSc1	pilot
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
návratem	návrat	k1gInSc7	návrat
rodičů	rodič	k1gMnPc2	rodič
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
až	až	k9	až
na	na	k7c6	na
faře	fara	k1gFnSc6	fara
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
z	z	k7c2	z
něho	on	k3xPp3gMnSc4	on
schopný	schopný	k2eAgMnSc1d1	schopný
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
motory	motor	k1gInPc7	motor
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
mladý	mladý	k2eAgMnSc1d1	mladý
Joe	Joe	k1gMnSc1	Joe
inklinoval	inklinovat	k5eAaImAgMnS	inklinovat
k	k	k7c3	k
rychlým	rychlý	k2eAgInPc3d1	rychlý
vozům	vůz	k1gInPc3	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Získal	získat	k5eAaPmAgInS	získat
starší	starý	k2eAgInSc1d2	starší
vůz	vůz	k1gInSc1	vůz
od	od	k7c2	od
Geoffreye	Geoffrey	k1gMnSc2	Geoffrey
Taylora	Taylor	k1gMnSc2	Taylor
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
vůz	vůz	k1gInSc4	vůz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
nikdy	nikdy	k6eAd1	nikdy
skutečného	skutečný	k2eAgInSc2d1	skutečný
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
Kelly	Kella	k1gFnSc2	Kella
konkurovat	konkurovat	k5eAaImF	konkurovat
při	pře	k1gFnSc3	pře
Wakefield	Wakefield	k1gInSc4	Wakefield
Trophy	Tropha	k1gFnSc2	Tropha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nestačil	stačit	k5eNaBmAgInS	stačit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
Hamiltonovo	Hamiltonův	k2eAgNnSc4d1	Hamiltonovo
Maserati	Maserat	k1gMnPc1	Maserat
<g/>
.	.	kIx.	.
</s>
<s>
Slavit	slavit	k5eAaImF	slavit
mohl	moct	k5eAaImAgInS	moct
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
na	na	k7c4	na
Ulster	Ulster	k1gInSc4	Ulster
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
v	v	k7c6	v
Dundrodu	Dundrod	k1gInSc6	Dundrod
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
obstál	obstát	k5eAaPmAgMnS	obstát
tak	tak	k6eAd1	tak
o	o	k7c6	o
proti	proti	k7c3	proti
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgFnSc3d2	silnější
konkurenci	konkurence	k1gFnSc3	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
i	i	k9	i
v	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
a	a	k8xC	a
1951	[number]	k4	1951
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
startu	start	k1gInSc6	start
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
jednou	jednou	k6eAd1	jednou
nedovezl	dovézt	k5eNaPmAgMnS	dovézt
svou	svůj	k3xOyFgFnSc4	svůj
Altu	alt	k1gInSc2	alt
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Změna	změna	k1gFnSc1	změna
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1952	[number]	k4	1952
a	a	k8xC	a
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jezdila	jezdit	k5eAaImAgFnS	jezdit
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
s	s	k7c7	s
vozy	vůz	k1gInPc1	vůz
Formule	formule	k1gFnSc1	formule
2	[number]	k4	2
<g/>
,	,	kIx,	,
dávalo	dávat	k5eAaImAgNnS	dávat
i	i	k9	i
značné	značný	k2eAgFnSc2d1	značná
šance	šance	k1gFnSc2	šance
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
vozům	vůz	k1gInPc3	vůz
Alta	Altum	k1gNnSc2	Altum
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Kelly	Kell	k1gMnPc4	Kell
nenechal	nechat	k5eNaPmAgMnS	nechat
nic	nic	k6eAd1	nic
náhodě	náhoda	k1gFnSc3	náhoda
a	a	k8xC	a
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
Alty	alt	k1gInPc7	alt
namontoval	namontovat	k5eAaPmAgInS	namontovat
výkonnější	výkonný	k2eAgInSc1d2	výkonnější
motor	motor	k1gInSc1	motor
Bristol	Bristol	k1gInSc1	Bristol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
akcích	akce	k1gFnPc6	akce
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
vozem	vůz	k1gInSc7	vůz
Kelly	Kella	k1gFnSc2	Kella
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
jen	jen	k9	jen
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
zaneprázdněn	zaneprázdnit	k5eAaPmNgInS	zaneprázdnit
přípravou	příprava	k1gFnSc7	příprava
vozu	vůz	k1gInSc2	vůz
Jaguar	Jaguar	k1gInSc1	Jaguar
C	C	kA	C
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yIgMnSc7	který
slavil	slavit	k5eAaImAgInS	slavit
úspěchy	úspěch	k1gInPc7	úspěch
až	až	k8xS	až
do	do	k7c2	do
osudného	osudný	k2eAgInSc2d1	osudný
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
havaroval	havarovat	k5eAaPmAgMnS	havarovat
v	v	k7c4	v
Outlon	Outlon	k1gInSc4	Outlon
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
Kelly	Kella	k1gFnSc2	Kella
utrpěl	utrpět	k5eAaPmAgInS	utrpět
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
a	a	k8xC	a
jen	jen	k9	jen
o	o	k7c4	o
vlásek	vlásek	k1gInSc4	vlásek
utekl	utéct	k5eAaPmAgMnS	utéct
amputaci	amputace	k1gFnSc3	amputace
ošklivě	ošklivě	k6eAd1	ošklivě
poraněné	poraněný	k2eAgFnPc4d1	poraněná
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
kariérou	kariéra	k1gFnSc7	kariéra
závodního	závodní	k2eAgMnSc2d1	závodní
pilota	pilot	k1gMnSc2	pilot
se	se	k3xPyFc4	se
Kelly	Kella	k1gFnPc4	Kella
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
rozloučit	rozloučit	k5eAaPmF	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
začíná	začínat	k5eAaImIp3nS	začínat
znovu	znovu	k6eAd1	znovu
s	s	k7c7	s
obchodní	obchodní	k2eAgFnSc7d1	obchodní
činností	činnost	k1gFnSc7	činnost
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
prodejcem	prodejce	k1gMnSc7	prodejce
vozů	vůz	k1gInPc2	vůz
Ferrari	ferrari	k1gNnSc2	ferrari
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
začíná	začínat	k5eAaImIp3nS	začínat
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
obchodní	obchodní	k2eAgFnSc4d1	obchodní
síť	síť	k1gFnSc4	síť
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účinkování	účinkování	k1gNnSc3	účinkování
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
bez	bez	k7c2	bez
bodů	bod	k1gInPc2	bod
–	–	k?	–
Alta	Alta	k1gFnSc1	Alta
GP	GP	kA	GP
</s>
</p>
<p>
<s>
1951	[number]	k4	1951
bez	bez	k7c2	bez
bodů	bod	k1gInPc2	bod
–	–	k?	–
Alta	Alta	k1gFnSc1	Alta
GP	GP	kA	GP
</s>
</p>
<p>
<s>
===	===	k?	===
Statistiky	statistika	k1gFnPc1	statistika
===	===	k?	===
</s>
</p>
<p>
<s>
2	[number]	k4	2
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
</s>
</p>
<p>
<s>
0	[number]	k4	0
vítězství	vítězství	k1gNnSc1	vítězství
</s>
</p>
<p>
<s>
0	[number]	k4	0
pole	pole	k1gNnSc1	pole
positions	positions	k6eAd1	positions
</s>
</p>
<p>
<s>
0	[number]	k4	0
nejrychlejších	rychlý	k2eAgNnPc2d3	nejrychlejší
kol	kolo	k1gNnPc2	kolo
</s>
</p>
<p>
<s>
0	[number]	k4	0
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
0	[number]	k4	0
x	x	k?	x
podium	podium	k1gNnSc1	podium
</s>
</p>
<p>
<s>
===	===	k?	===
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
F1	F1	k1gFnSc2	F1
===	===	k?	===
</s>
</p>
<p>
<s>
1951	[number]	k4	1951
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
1951	[number]	k4	1951
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Joe	Joe	k1gFnSc2	Joe
Kelly	Kella	k1gFnSc2	Kella
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Joe	Joe	k?	Joe
Kelly	Kell	k1gInPc1	Kell
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
F1DB	F1DB	k1gFnSc2	F1DB
</s>
</p>
