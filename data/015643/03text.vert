<s>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
</s>
<s>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
Název	název	k1gInSc1
</s>
<s>
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Los	los	k1gInSc1
Indios	Indiosa	k1gFnPc2
<g/>
,	,	kIx,
El	Ela	k1gFnPc2
Pupas	Pupasa	k1gFnPc2
<g/>
,	,	kIx,
Los	los	k1gInSc4
Colchoneros	Colchonerosa	k1gFnPc2
<g/>
,	,	kIx,
Los	los	k1gInSc1
Rojiblancos	Rojiblancosa	k1gFnPc2
Země	zem	k1gFnSc2
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Madrid	Madrid	k1gInSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1903	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Asociace	asociace	k1gFnSc2
</s>
<s>
RFEF	RFEF	kA
Barvy	barva	k1gFnPc1
</s>
<s>
•	•	k?
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
1903	#num#	k4
<g/>
–	–	k?
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
•	•	k?
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
červená	červený	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
1911	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
•	•	k?
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
a	a	k8xC
červená	červený	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
od	od	k7c2
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_atlmadrid	_atlmadrid	k1gInSc1
<g/>
1920	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_atlmadrid	_atlmadrid	k1gInSc1
<g/>
1920	#num#	k4
<g/>
a	a	k8xC
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_atlmadrid	_atlmadrid	k1gInSc1
<g/>
1920	#num#	k4
<g/>
t	t	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
španělská	španělský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Wanda	Wanda	k1gFnSc1
Metropolitano	Metropolitana	k1gFnSc5
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
40	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
58	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
67	#num#	k4
703	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Vedení	vedení	k1gNnSc1
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Miguel	Miguel	k1gMnSc1
Ángel	Ángel	k1gMnSc1
Gil	Gil	k1gMnSc1
Marín	Marína	k1gFnPc2
(	(	kIx(
<g/>
54,6	54,6	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
Wanda	Wanda	k1gMnSc1
Group	Group	k1gMnSc1
(	(	kIx(
<g/>
20	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
Enrique	Enrique	k1gInSc1
Cerezo	Cereza	k1gFnSc5
(	(	kIx(
<g/>
17,9	17,9	k4
%	%	kIx~
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
</s>
<s>
Enrique	Enrique	k1gFnSc5
Cerezo	Cereza	k1gFnSc5
Trenér	trenér	k1gMnSc1
</s>
<s>
Diego	Diega	k1gMnSc5
Simeone	Simeon	k1gMnSc5
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
10	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Španělska	Španělsko	k1gNnSc2
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
Domácí	domácí	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
10	#num#	k4
<g/>
×	×	k?
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
2	#num#	k4
<g/>
×	×	k?
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gNnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
3	#num#	k4
<g/>
×	×	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
3	#num#	k4
<g/>
×	×	k?
Superpohár	superpohár	k1gInSc4
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
×	×	k?
Interkontinentální	interkontinentální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Atlético	Atlético	k6eAd1
de	de	k?
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
působí	působit	k5eAaImIp3nP
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
<g/>
,	,	kIx,
španělské	španělský	k2eAgFnSc6d1
nejvyšší	vysoký	k2eAgFnSc6d3
fotbalové	fotbalový	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubové	klubový	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
a	a	k8xC
červená	červený	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Založen	založit	k5eAaPmNgInS
byl	být	k5eAaImAgInS
dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1903	#num#	k4
jako	jako	k8xS,k8xC
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc4
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
založili	založit	k5eAaPmAgMnP
tři	tři	k4xCgMnPc4
baskičtí	baskický	k2eAgMnPc1d1
studenti	student	k1gMnPc1
žijící	žijící	k2eAgMnPc1d1
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
Atlético	Atlético	k6eAd1
kompletně	kompletně	k6eAd1
nezávislým	závislý	k2eNgInSc7d1
na	na	k7c6
Athleticu	Athleticus	k1gInSc6
Bilbao	Bilbao	k6eAd1
(	(	kIx(
<g/>
a	a	k8xC
Basků	Bask	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
madridský	madridský	k2eAgInSc1d1
celek	celek	k1gInSc1
hrál	hrát	k5eAaImAgInS
v	v	k7c6
tomto	tento	k3xDgInSc6
partnerství	partnerství	k1gNnSc1
jakousi	jakýsi	k3yIgFnSc4
baskickou	baskický	k2eAgFnSc4d1
enklávu	enkláva	k1gFnSc4
mimo	mimo	k7c4
Bilbao	Bilbao	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1939	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
byl	být	k5eAaImAgInS
klub	klub	k1gInSc1
součástí	součást	k1gFnPc2
frankistických	frankistický	k2eAgFnPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
po	po	k7c6
fúzí	fúze	k1gFnSc7
s	s	k7c7
Aviación	Aviación	k1gMnSc1
Nacional	Nacional	k1gMnSc1
ze	z	k7c2
Zaragozy	Zaragoz	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1941	#num#	k4
pak	pak	k6eAd1
vymizel	vymizet	k5eAaPmAgMnS
z	z	k7c2
názvu	název	k1gInSc2
anglický	anglický	k2eAgInSc1d1
Athletic	Athletice	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
španělským	španělský	k2eAgInSc7d1
názvem	název	k1gInSc7
Atlético	Atlético	k6eAd1
(	(	kIx(
<g/>
čímž	což	k3yQnSc7,k3yRnSc7
bylo	být	k5eAaImAgNnS
také	také	k9
vymazáno	vymazán	k2eAgNnSc1d1
poslední	poslední	k2eAgNnSc1d1
pouto	pouto	k1gNnSc1
na	na	k7c4
baskický	baskický	k2eAgInSc4d1
Athletic	Athletice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
z	z	k7c2
důvodu	důvod	k1gInSc2
pečlivého	pečlivý	k2eAgNnSc2d1
frankistického	frankistický	k2eAgNnSc2d1
potírání	potírání	k1gNnSc2
všech	všecek	k3xTgInPc2
anglicismů	anglicismus	k1gInPc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
také	také	k9
fotbalovou	fotbalový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
poprvé	poprvé	k6eAd1
a	a	k8xC
také	také	k9
naposled	naposled	k6eAd1
nahradil	nahradit	k5eAaPmAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
z	z	k7c2
pozice	pozice	k1gFnSc2
toho	ten	k3xDgInSc2
silnějšího	silný	k2eAgInSc2d2
<g />
.	.	kIx.
</s>
<s hack="1">
klubu	klub	k1gInSc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Opětovná	opětovný	k2eAgFnSc1d1
změna	změna	k1gFnSc1
nastala	nastat	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
z	z	k7c2
Realu	Real	k1gInSc2
hegemon	hegemon	k1gMnSc1
nejen	nejen	k6eAd1
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
evropské	evropský	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
těchto	tento	k3xDgFnPc6
změnách	změna	k1gFnPc6
nebylo	být	k5eNaImAgNnS
Atlético	Atlético	k6eAd1
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
konečném	konečný	k2eAgInSc6d1
součtu	součet	k1gInSc6
následujících	následující	k2eAgNnPc2d1
padesáti	padesát	k4xCc2
let	léto	k1gNnPc2
příliš	příliš	k6eAd1
dominantní	dominantní	k2eAgFnSc1d1
oproti	oproti	k7c3
městskému	městský	k2eAgInSc3d1
rivalovi	rivalův	k2eAgMnPc1d1
a	a	k8xC
nebo	nebo	k8xC
katalánské	katalánský	k2eAgFnSc6d1
Barceloně	Barcelona	k1gFnSc6
(	(	kIx(
<g/>
jediné	jediný	k2eAgInPc1d1
tituly	titul	k1gInPc1
"	"	kIx"
<g/>
Rojiblancos	Rojiblancos	k1gInSc1
<g/>
"	"	kIx"
v	v	k7c6
této	tento	k3xDgFnSc6
éře	éra	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
sezón	sezóna	k1gFnPc2
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
a	a	k8xC
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dokonce	dokonce	k9
na	na	k7c6
počátku	počátek	k1gInSc6
nového	nový	k2eAgNnSc2d1
milénia	milénium	k1gNnSc2
přišel	přijít	k5eAaPmAgInS
teprve	teprve	k6eAd1
druhý	druhý	k4xOgInSc1
sestup	sestup	k1gInSc1
z	z	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
začala	začít	k5eAaPmAgFnS
postupná	postupný	k2eAgFnSc1d1
změna	změna	k1gFnSc1
k	k	k7c3
lepšímu	lepší	k1gNnSc3
a	a	k8xC
po	po	k7c6
nástupu	nástup	k1gInSc6
Diega	Diega	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Simeoneho	Simeone	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
načatí	načatý	k2eAgMnPc1d1
další	další	k2eAgFnPc4d1
zlaté	zlatý	k2eAgFnPc4d1
éry	éra	k1gFnPc4
klubu	klub	k1gInSc2
(	(	kIx(
<g/>
započaté	započatý	k2eAgInPc1d1
titulem	titul	k1gInSc7
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
a	a	k8xC
následovaných	následovaný	k2eAgFnPc2d1
dvěma	dva	k4xCgFnPc7
finálovými	finálový	k2eAgFnPc7d1
účasti	účast	k1gFnSc3
v	v	k7c6
LM	LM	kA
–	–	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
zvítězil	zvítězit	k5eAaPmAgMnS
shodně	shodně	k6eAd1
desetkrát	desetkrát	k6eAd1
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
soutěži	soutěž	k1gFnSc6
i	i	k8xC
domácím	domácí	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
historického	historický	k2eAgInSc2d1
doublu	double	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
Poháru	pohár	k1gInSc6
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
vítězem	vítěz	k1gMnSc7
Interkontinentálního	interkontinentální	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
získal	získat	k5eAaPmAgMnS
trofej	trofej	k1gFnSc4
pro	pro	k7c4
vítěze	vítěz	k1gMnPc4
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
finále	finále	k1gNnSc2
porazil	porazit	k5eAaPmAgInS
anglický	anglický	k2eAgInSc1d1
Fulham	Fulham	k1gInSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejný	stejný	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
zopakoval	zopakovat	k5eAaPmAgMnS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
tentokrát	tentokrát	k6eAd1
pokořil	pokořit	k5eAaPmAgMnS
španělský	španělský	k2eAgMnSc1d1
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc1d1
mezinárodní	mezinárodní	k2eAgInSc1d1
triumf	triumf	k1gInSc1
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
do	do	k7c2
sezóny	sezóna	k1gFnSc2
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Atlético	Atlético	k6eAd1
zvítězilo	zvítězit	k5eAaPmAgNnS
potřetí	potřetí	k4xO
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
stadionu	stadion	k1gInSc6
Wanda	Wando	k1gNnSc2
Metropolitano	Metropolitana	k1gFnSc5
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
67	#num#	k4
703	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc4
de	de	k?
Madrid	Madrid	k1gInSc1
</s>
<s>
Tým	tým	k1gInSc4
původně	původně	k6eAd1
založili	založit	k5eAaPmAgMnP
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1903	#num#	k4
jako	jako	k8xC,k8xS
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc4
de	de	k?
Madrid	Madrid	k1gInSc1
tři	tři	k4xCgMnPc4
baskičtí	baskický	k2eAgMnPc1d1
studenti	student	k1gMnPc1
žijící	žijící	k2eAgMnPc1d1
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viděli	vidět	k5eAaImAgMnP
v	v	k7c6
něm	on	k3xPp3gNnSc6
jakousi	jakýsi	k3yIgFnSc4
větev	větev	k1gFnSc1
týmu	tým	k1gInSc2
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
se	se	k3xPyFc4
připojili	připojit	k5eAaPmAgMnP
k	k	k7c3
odpadlým	odpadlý	k2eAgMnPc3d1
hráčům	hráč	k1gMnPc3
z	z	k7c2
týmu	tým	k1gInSc2
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začali	začít	k5eAaPmAgMnP
hrát	hrát	k5eAaImF
v	v	k7c6
barvách	barva	k1gFnPc6
jako	jako	k8xC,k8xS
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k6eAd1
<g/>
,	,	kIx,
tedy	tedy	k9
v	v	k7c4
modré	modrý	k2eAgFnPc4d1
a	a	k8xC
bílé	bílý	k2eAgFnPc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1911	#num#	k4
hrají	hrát	k5eAaImIp3nP
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
současných	současný	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvod	důvod	k1gInSc4
této	tento	k3xDgFnSc2
změny	změna	k1gFnSc2
není	být	k5eNaImIp3nS
znám	znám	k2eAgInSc1d1
s	s	k7c7
naprostou	naprostý	k2eAgFnSc7d1
jistotou	jistota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
společnost	společnost	k1gFnSc1
Compañ	Compañ	k1gFnSc1
Urbanizadora	Urbanizadora	k1gFnSc1
Metropolitana	Metropolitana	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
budovala	budovat	k5eAaImAgFnS
podzemní	podzemní	k2eAgInSc4d1
komunikační	komunikační	k2eAgInSc4d1
systém	systém	k1gInSc4
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
půdu	půda	k1gFnSc4
poblíž	poblíž	k7c2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
projektu	projekt	k1gInSc2
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
sportovní	sportovní	k2eAgInSc1d1
stadión	stadión	k1gInSc1
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
Estadio	Estadio	k1gMnSc1
Metropolitano	Metropolitana	k1gFnSc5
de	de	k?
Madrid	Madrid	k1gInSc1
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
35	#num#	k4
800	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
jej	on	k3xPp3gMnSc4
měl	mít	k5eAaImAgInS
pronajmutý	pronajmutý	k2eAgInSc1d1
právě	právě	k9
klub	klub	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
ještě	ještě	k6eAd1
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc4
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1966	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
tým	tým	k1gInSc1
přestěhoval	přestěhovat	k5eAaPmAgInS
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
současného	současný	k2eAgNnSc2d1
působiště	působiště	k1gNnSc2
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
nově	nově	k6eAd1
vybudovaného	vybudovaný	k2eAgInSc2d1
stadiónu	stadión	k1gInSc2
Estadio	Estadio	k6eAd1
Vicente	Vicent	k1gInSc5
Calderón	Calderón	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přestěhování	přestěhování	k1gNnSc6
týmu	tým	k1gInSc2
byl	být	k5eAaImAgInS
Estadio	Estadio	k6eAd1
Metropolitano	Metropolitana	k1gFnSc5
de	de	k?
Madrid	Madrid	k1gInSc1
zdemolován	zdemolovat	k5eAaPmNgInS
a	a	k8xC
nahrazen	nahradit	k5eAaPmNgInS
budovami	budova	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
patří	patřit	k5eAaImIp3nP
univerzitě	univerzita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1921	#num#	k4
se	se	k3xPyFc4
Athletic	Athletice	k1gFnPc2
Madrid	Madrid	k1gInSc1
stal	stát	k5eAaPmAgMnS
nezávislým	závislý	k2eNgMnSc7d1
na	na	k7c6
Athleticu	Athleticus	k1gInSc6
Bilbao	Bilbao	k6eAd1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
se	se	k3xPyFc4
usadil	usadit	k5eAaPmAgInS
na	na	k7c6
svém	svůj	k3xOyFgInSc6
prvním	první	k4xOgInSc6
stadiónu	stadión	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
zvítězil	zvítězit	k5eAaPmAgMnS
třikrát	třikrát	k6eAd1
v	v	k7c6
soutěži	soutěž	k1gFnSc6
Campeonato	Campeonat	k2eAgNnSc1d1
del	del	k?
Centro	Centro	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1921	#num#	k4
a	a	k8xC
1926	#num#	k4
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
poháru	pohár	k1gInSc6
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
těchto	tento	k3xDgInPc2
výsledků	výsledek	k1gInPc2
došlo	dojít	k5eAaPmAgNnS
roku	rok	k1gInSc2
1928	#num#	k4
k	k	k7c3
pozvání	pozvání	k1gNnSc3
klubu	klub	k1gInSc2
do	do	k7c2
soutěže	soutěž	k1gFnSc2
Primera	primera	k1gFnSc1
División	División	k1gInSc1
La	la	k1gNnSc6
Ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
první	první	k4xOgFnSc2
sezóny	sezóna	k1gFnSc2
byl	být	k5eAaImAgInS
manažerem	manažer	k1gInSc7
týmu	tým	k1gInSc2
Angličan	Angličan	k1gMnSc1
Fred	Fred	k1gMnSc1
Pentland	Pentland	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
klub	klub	k1gInSc1
sestoupil	sestoupit	k5eAaPmAgInS
do	do	k7c2
nižší	nízký	k2eAgFnSc2d2
soutěže	soutěž	k1gFnSc2
Segunda	Segunda	k1gFnSc1
División	División	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
se	se	k3xPyFc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Pentlanda	Pentlando	k1gNnSc2
opět	opět	k6eAd1
vrátil	vrátit	k5eAaPmAgInS
zpátky	zpátky	k6eAd1
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
tým	tým	k1gInSc1
znovu	znovu	k6eAd1
sestoupil	sestoupit	k5eAaPmAgInS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
jej	on	k3xPp3gMnSc4
v	v	k7c6
polovině	polovina	k1gFnSc6
sezóny	sezóna	k1gFnSc2
převzal	převzít	k5eAaPmAgMnS
Josep	Josep	k1gMnSc1
Samitier	Samitier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témž	týž	k3xTgInSc6
roce	rok	k1gInSc6
však	však	k9
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
začátku	začátek	k1gInSc3
španělské	španělský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
důsledku	důsledek	k1gInSc6
byla	být	k5eAaImAgFnS
klubu	klub	k1gInSc2
„	„	k?
<g/>
udělena	udělen	k2eAgFnSc1d1
milost	milost	k1gFnSc1
<g/>
“	“	k?
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
sestup	sestup	k1gInSc1
společně	společně	k6eAd1
s	s	k7c7
La	la	k1gNnSc7
Ligou	liga	k1gFnSc7
byl	být	k5eAaImAgInS
pozastaven	pozastaven	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Athletic	Athletice	k1gFnPc2
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
La	la	k1gNnSc1
Liga	liga	k1gFnSc1
opět	opět	k6eAd1
rozběhla	rozběhnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
Athletic	Athletice	k1gFnPc2
Madrid	Madrid	k1gInSc1
sloučil	sloučit	k5eAaPmAgInS
s	s	k7c7
týmem	tým	k1gInSc7
Aviación	Aviación	k1gMnSc1
Nacional	Nacional	k1gMnSc1
ze	z	k7c2
Zaragozy	Zaragoz	k1gInPc1
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
vznikl	vzniknout	k5eAaPmAgInS
tým	tým	k1gInSc4
Athletic	Athletice	k1gFnPc2
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aviación	Aviación	k1gInSc1
Nacional	Nacional	k1gFnSc2
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
založen	založen	k2eAgInSc1d1
roku	rok	k1gInSc3
1937	#num#	k4
členy	člen	k1gMnPc7
španělského	španělský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
jim	on	k3xPp3gMnPc3
slíbeno	slíben	k2eAgNnSc1d1
účastnické	účastnický	k2eAgNnSc1d1
místo	místo	k1gNnSc1
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gInSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1939	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
španělská	španělský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
toto	tento	k3xDgNnSc4
zamítla	zamítnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
se	se	k3xPyFc4
tedy	tedy	k9
sloučil	sloučit	k5eAaPmAgInS
s	s	k7c7
Athleticem	Athletic	k1gMnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
během	během	k7c2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
ztratilo	ztratit	k5eAaPmAgNnS
8	#num#	k4
mužů	muž	k1gMnPc2
sestavy	sestava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
celek	celek	k1gInSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
tohoto	tento	k3xDgInSc2
ročníku	ročník	k1gInSc2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
zúčastnil	zúčastnit	k5eAaPmAgMnS
<g/>
,	,	kIx,
avšak	avšak	k8xC
jen	jen	k9
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
půda	půda	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
hrál	hrát	k5eAaImAgInS
tým	tým	k1gInSc1
Real	Real	k1gInSc1
Oviedo	Oviedo	k1gNnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
během	během	k7c2
konfliktu	konflikt	k1gInSc2
poškozena	poškozen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
manažerem	manažer	k1gMnSc7
Ricardem	Ricard	k1gMnSc7
Zamorou	Zamora	k1gFnSc7
se	se	k3xPyFc4
tým	tým	k1gInSc1
následně	následně	k6eAd1
stal	stát	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
mistrem	mistr	k1gMnSc7
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
vítězem	vítěz	k1gMnSc7
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
jej	on	k3xPp3gMnSc4
obhájil	obhájit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
diktátor	diktátor	k1gMnSc1
Francisco	Francisco	k1gMnSc1
Franco	Franco	k1gMnSc1
zakázal	zakázat	k5eAaPmAgMnS
týmům	tým	k1gInPc3
používat	používat	k5eAaImF
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
názvech	název	k1gInPc6
cizojazyčná	cizojazyčný	k2eAgNnPc4d1
slova	slovo	k1gNnPc4
a	a	k8xC
jména	jméno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
se	se	k3xPyFc4
tedy	tedy	k9
přejmenoval	přejmenovat	k5eAaPmAgMnS
na	na	k7c4
Atlético	Atlético	k1gNnSc4
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
se	se	k3xPyFc4
tým	tým	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
jména	jméno	k1gNnSc2
odstranit	odstranit	k5eAaPmF
vojenskou	vojenský	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
a	a	k8xC
přejmenoval	přejmenovat	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
současný	současný	k2eAgInSc4d1
název	název	k1gInSc4
Club	club	k1gInSc4
Atlético	Atlético	k6eAd1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témž	týž	k3xTgInSc6
roce	rok	k1gInSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
zvítězit	zvítězit	k5eAaPmF
na	na	k7c6
stadionu	stadion	k1gInSc6
Metropolitano	Metropolitana	k1gFnSc5
nad	nad	k7c7
Realem	Real	k1gInSc7
Madrid	Madrid	k1gInSc4
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
nejvyšší	vysoký	k2eAgNnSc1d3
vítězství	vítězství	k1gNnSc1
týmu	tým	k1gInSc2
nad	nad	k7c7
svým	svůj	k3xOyFgMnSc7
městským	městský	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
éra	éra	k1gFnSc1
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1950	#num#	k4
a	a	k8xC
1951	#num#	k4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
manažera	manažer	k1gMnSc2
Helenia	Helenium	k1gNnSc2
Herrery	Herrera	k1gFnSc2
a	a	k8xC
s	s	k7c7
pomocí	pomoc	k1gFnSc7
Larbi	Larb	k1gFnSc2
Benbareka	Benbareek	k1gMnSc2
<g/>
,	,	kIx,
marockého	marocký	k2eAgMnSc2d1
fotbalisty	fotbalista	k1gMnSc2
a	a	k8xC
jedné	jeden	k4xCgFnSc2
z	z	k7c2
prvních	první	k4xOgFnPc2
afrických	africký	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
Atlético	Atlético	k1gMnSc1
Madrid	Madrid	k1gInSc4
další	další	k2eAgInPc4d1
dva	dva	k4xCgInPc4
tituly	titul	k1gInPc4
mistra	mistr	k1gMnSc4
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
odchodem	odchod	k1gInSc7
Herrery	Herrera	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
však	však	k9
tým	tým	k1gInSc1
začal	začít	k5eAaPmAgInS
sklouzávat	sklouzávat	k5eAaImF
za	za	k7c4
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
a	a	k8xC
FC	FC	kA
Barcelonu	Barcelona	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c4
zbytek	zbytek	k1gInSc4
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
naň	naň	k?
zbyly	zbýt	k5eAaPmAgInP
bitvy	bitva	k1gFnSc2
o	o	k7c6
třetí	třetí	k4xOgFnSc6
tým	tým	k1gInSc1
země	zem	k1gFnPc4
s	s	k7c7
Atléticem	Atlétic	k1gMnSc7
Bilbao	Bilbao	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
během	během	k7c2
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
Atlético	Atlético	k6eAd1
de	de	k?
Madrid	Madrid	k1gInSc1
bojoval	bojovat	k5eAaImAgInS
s	s	k7c7
Barcelonou	Barcelona	k1gFnSc7
o	o	k7c6
pozici	pozice	k1gFnSc6
druhého	druhý	k4xOgInSc2
nejlepšího	dobrý	k2eAgInSc2d3
týmu	tým	k1gInSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
obsadil	obsadit	k5eAaPmAgMnS
post	post	k1gInSc4
manažera	manažer	k1gMnSc2
čechoslovák	čechoslovák	k?
Ferdinand	Ferdinand	k1gMnSc1
Daučík	Daučík	k1gMnSc1
(	(	kIx(
<g/>
též	též	k9
Fernando	Fernanda	k1gFnSc5
Daucik	Daucikum	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
dovedl	dovést	k5eAaPmAgMnS
tým	tým	k1gInSc4
až	až	k9
na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
výsledkem	výsledek	k1gInSc7
se	se	k3xPyFc4
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
do	do	k7c2
příštího	příští	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
PMEZ	PMEZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veden	veden	k2eAgInSc1d1
brazilskými	brazilský	k2eAgMnPc7d1
útočníky	útočník	k1gMnPc7
Vavou	Vava	k1gMnSc7
a	a	k8xC
Enrique	Enrique	k1gInSc1
Collarem	Collar	k1gInSc7
dosáhl	dosáhnout	k5eAaPmAgInS
klub	klub	k1gInSc1
semifinále	semifinále	k1gNnSc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
porazil	porazit	k5eAaPmAgMnS
týmy	tým	k1gInPc4
Drumcondra	Drumcondr	k1gMnSc4
FC	FC	kA
<g/>
,	,	kIx,
CSKA	CSKA	kA
Sofia	Sofia	k1gFnSc1
a	a	k8xC
FC	FC	kA
Schalke	Schalke	k1gFnSc1
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
semifinále	semifinále	k1gNnSc6
však	však	k9
narazil	narazit	k5eAaPmAgMnS
na	na	k7c4
svého	svůj	k3xOyFgMnSc4
úhlavního	úhlavní	k2eAgMnSc4d1
rivala	rival	k1gMnSc4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgNnSc6
utkání	utkání	k1gNnSc6
zvítězil	zvítězit	k5eAaPmAgInS
Real	Real	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
na	na	k7c6
domácím	domácí	k2eAgNnSc6d1
hřišti	hřiště	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
druhém	druhý	k4xOgNnSc6
však	však	k9
na	na	k7c6
stadionu	stadion	k1gInSc6
Metropolitano	Metropolitana	k1gFnSc5
zvítězil	zvítězit	k5eAaPmAgInS
Atlético	Atlético	k1gNnSc4
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodující	rozhodující	k2eAgInSc4d1
třetí	třetí	k4xOgInSc4
duel	duel	k1gInSc4
se	se	k3xPyFc4
hrál	hrát	k5eAaImAgMnS
na	na	k7c6
neutrální	neutrální	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
v	v	k7c6
Zaragoze	Zaragoz	k1gInSc5
<g/>
,	,	kIx,
vítězství	vítězství	k1gNnSc3
a	a	k8xC
postup	postup	k1gInSc1
nakonec	nakonec	k6eAd1
slavil	slavit	k5eAaImAgInS
konkurenční	konkurenční	k2eAgInSc1d1
Real	Real	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zde	zde	k6eAd1
vyhrál	vyhrát	k5eAaPmAgInS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Atlético	Atlético	k6eAd1
nakonec	nakonec	k6eAd1
Realu	Real	k1gInSc2
připravilo	připravit	k5eAaPmAgNnS
pomstu	pomsta	k1gFnSc4
<g/>
,	,	kIx,
pod	pod	k7c7
vedením	vedení	k1gNnSc7
manažera	manažer	k1gMnSc2
José	Josá	k1gFnSc2
Villalongy	Villalong	k1gInPc4
<g/>
,	,	kIx,
bývalého	bývalý	k2eAgMnSc4d1
kouče	kouč	k1gMnSc4
městského	městský	k2eAgMnSc4d1
rivala	rival	k1gMnSc4
<g/>
,	,	kIx,
porazili	porazit	k5eAaPmAgMnP
bílý	bílý	k2eAgInSc4d1
balet	balet	k1gInSc4
ve	v	k7c6
dvou	dva	k4xCgInPc6
po	po	k7c6
sobě	se	k3xPyFc3
následujících	následující	k2eAgNnPc6d1
finálích	finálí	k1gNnPc6
španělského	španělský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1960	#num#	k4
a	a	k8xC
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1962	#num#	k4
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
Poháru	pohár	k1gInSc6
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
když	když	k8xS
porazil	porazit	k5eAaPmAgMnS
v	v	k7c6
opakovaném	opakovaný	k2eAgNnSc6d1
finále	finále	k1gNnSc6
Fiorentinu	Fiorentin	k1gInSc2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
došel	dojít	k5eAaPmAgInS
opět	opět	k6eAd1
do	do	k7c2
finále	finále	k1gNnSc2
této	tento	k3xDgFnSc2
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
však	však	k9
podlehl	podlehnout	k5eAaPmAgMnS
Tottenhamu	Tottenham	k1gInSc6
Hotspur	Hotspura	k1gFnPc2
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlético	Atlético	k1gMnSc1
táhl	táhnout	k5eAaImAgMnS
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
Enrique	Enrique	k1gFnSc4
Collar	Collar	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
vlivným	vlivný	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
společně	společně	k6eAd1
se	s	k7c7
záložníky	záložník	k1gMnPc7
Miguelem	Miguel	k1gMnSc7
Jonesem	Jones	k1gMnSc7
a	a	k8xC
Adelardem	Adelard	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc4
nejlepší	dobrý	k2eAgNnSc4d3
období	období	k1gNnSc4
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
se	se	k3xPyFc4
však	však	k9
shoduje	shodovat	k5eAaImIp3nS
s	s	k7c7
obdobím	období	k1gNnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1961	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
kraloval	kralovat	k5eAaImAgMnS
La	la	k1gNnSc2
Lize	liga	k1gFnSc3
právě	právě	k6eAd1
jiný	jiný	k2eAgInSc1d1
madridský	madridský	k2eAgInSc1d1
tým	tým	k1gInSc1
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Finále	finále	k1gNnSc1
PMEZ	PMEZ	kA
</s>
<s>
Stadion	stadion	k1gInSc1
Vicente	Vicent	k1gInSc5
Calderóna	Calderón	k1gMnSc2
</s>
<s>
Významní	významný	k2eAgMnPc1d1
a	a	k8xC
důležití	důležitý	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
hrávali	hrávat	k5eAaImAgMnP
za	za	k7c4
Atlético	Atlético	k1gNnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
éře	éra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
zejména	zejména	k9
o	o	k7c4
veterána	veterán	k1gMnSc4
Adelarda	Adelard	k1gMnSc4
a	a	k8xC
pravdielné	pravdielný	k2eAgFnSc3d1
skorující	skorující	k2eAgFnSc3d1
Luise	Luisa	k1gFnSc3
Aragonése	Aragonés	k1gMnSc2
<g/>
,	,	kIx,
Javiera	Javier	k1gMnSc2
Iruretu	Iruret	k1gInSc2
a	a	k8xC
José	Josý	k2eAgNnSc4d1
Eulogio	Eulogio	k1gNnSc4
Gárateho	Gárate	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc1d1
jmenovaný	jmenovaný	k2eAgInSc1d1
získal	získat	k5eAaPmAgInS
třikrát	třikrát	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
v	v	k7c6
letech	let	k1gInPc6
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
Trofeo	Trofeo	k1gNnSc4
Pichichi	Pichichi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
angažoval	angažovat	k5eAaBmAgMnS
také	také	k9
několik	několik	k4yIc4
známých	známý	k2eAgMnPc2d1
argentinských	argentinský	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
–	–	k?
Rubéna	Rubén	k1gMnSc4
Ayalu	Ayala	k1gMnSc4
<g/>
,	,	kIx,
Panadera	Panader	k1gMnSc4
Díaze	Díaze	k1gFnSc2
a	a	k8xC
Ramóna	Ramóna	k1gFnSc1
"	"	kIx"
<g/>
Cacha	Cacha	k1gFnSc1
<g/>
"	"	kIx"
Herediu	Heredium	k1gNnSc6
<g/>
,	,	kIx,
mimo	mimo	k7c4
to	ten	k3xDgNnSc4
také	také	k9
trenéra	trenér	k1gMnSc4
Juana	Juan	k1gMnSc4
Carlose	Carlosa	k1gFnSc3
Lorenza	Lorenza	k?
<g/>
,	,	kIx,
taktéž	taktéž	k?
Argentince	Argentinka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lorenzo	Lorenza	k1gFnSc5
věřil	věřit	k5eAaImAgMnS
v	v	k7c4
disciplínu	disciplína	k1gFnSc4
<g/>
,	,	kIx,
obezřetnost	obezřetnost	k1gFnSc4
a	a	k8xC
narušení	narušení	k1gNnSc4
soupeřovy	soupeřův	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
jistou	jistý	k2eAgFnSc4d1
kontroverznost	kontroverznost	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
metody	metoda	k1gFnSc2
slavily	slavit	k5eAaImAgInP
úspěch	úspěch	k1gInSc4
a	a	k8xC
po	po	k7c6
vítězství	vítězství	k1gNnSc6
v	v	k7c6
La	la	k1gNnSc6
Lize	liga	k1gFnSc6
roku	rok	k1gInSc2
1973	#num#	k4
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
došel	dojít	k5eAaPmAgMnS
do	do	k7c2
finále	finále	k1gNnSc2
Poháru	pohár	k1gInSc2
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
PMEZ	PMEZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
cestě	cesta	k1gFnSc6
do	do	k7c2
něj	on	k3xPp3gMnSc2
vyřadil	vyřadit	k5eAaPmAgInS
Galatasaray	Galatasaraa	k1gFnSc2
Istanbul	Istanbul	k1gInSc1
<g/>
,	,	kIx,
Dinamo	Dinama	k1gFnSc5
Bukurešť	Bukurešť	k1gFnSc1
<g/>
,	,	kIx,
Crvenou	Crvený	k2eAgFnSc4d1
zvezdu	zvezda	k1gFnSc4
Bělehrad	Bělehrad	k1gInSc1
a	a	k8xC
Celtic	Celtice	k1gFnPc2
Glasgow	Glasgow	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
však	však	k9
narazil	narazit	k5eAaPmAgMnS
na	na	k7c4
silný	silný	k2eAgInSc4d1
Bayern	Bayern	k1gInSc4
Mnichov	Mnichov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finálový	finálový	k2eAgInSc1d1
duel	duel	k1gInSc1
skončil	skončit	k5eAaPmAgInS
remízou	remíza	k1gFnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
odvetě	odveta	k1gFnSc6
však	však	k9
již	již	k6eAd1
kraloval	kralovat	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
velkoklub	velkoklub	k1gMnSc1
a	a	k8xC
zvítězil	zvítězit	k5eAaPmAgMnS
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Éra	éra	k1gFnSc1
Luise	Luisa	k1gFnSc3
Aragonése	Aragonése	k1gFnSc1
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
porážce	porážka	k1gFnSc6
ve	v	k7c6
finále	finále	k1gNnSc6
PMEZ	PMEZ	kA
angažovalo	angažovat	k5eAaBmAgNnS
Atlético	Atlético	k6eAd1
jako	jako	k9
trenéra	trenér	k1gMnSc4
svoji	svůj	k3xOyFgFnSc4
bývalou	bývalý	k2eAgFnSc4d1
hvězdu	hvězda	k1gFnSc4
Luise	Luisa	k1gFnSc3
Aragonése	Aragonésa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
vedl	vést	k5eAaImAgInS
tým	tým	k1gInSc4
v	v	k7c6
několika	několik	k4yIc6
obdobích	období	k1gNnPc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1974	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
a	a	k8xC
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
první	první	k4xOgInSc1
úspěch	úspěch	k1gInSc1
přišel	přijít	k5eAaPmAgInS
velmi	velmi	k6eAd1
brzy	brzy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
jako	jako	k9
vítěz	vítěz	k1gMnSc1
PMEZ	PMEZ	kA
odmítl	odmítnout	k5eAaPmAgMnS
účast	účast	k1gFnSc4
na	na	k7c6
Interkontinentálním	interkontinentální	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
jej	on	k3xPp3gMnSc4
jako	jako	k9
druhý	druhý	k4xOgInSc4
tým	tým	k1gInSc4
nahradilo	nahradit	k5eAaPmAgNnS
právě	právě	k9
Atlético	Atlético	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
poháru	pohár	k1gInSc6
porazilo	porazit	k5eAaPmAgNnS
celkově	celkově	k6eAd1
ve	v	k7c6
dvou	dva	k4xCgInPc6
zápasech	zápas	k1gInPc6
(	(	kIx(
<g/>
první	první	k4xOgMnPc1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
argentinský	argentinský	k2eAgInSc1d1
tým	tým	k1gInSc1
CA	ca	kA
Independiente	Independient	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aragonés	Aragonés	k1gInSc4
také	také	k9
tým	tým	k1gInSc4
dovedl	dovést	k5eAaPmAgMnS
k	k	k7c3
vítězství	vítězství	k1gNnSc3
v	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
a	a	k8xC
v	v	k7c6
La	la	k1gNnSc1
Lize	liga	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
druhého	druhý	k4xOgNnSc2
období	období	k1gNnSc6
trénování	trénování	k1gNnSc4
dovedl	dovést	k5eAaPmAgMnS
tým	tým	k1gInSc4
na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
La	la	k1gNnSc6
Lize	liga	k1gFnSc6
a	a	k8xC
vítězství	vítězství	k1gNnSc2
v	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
mu	on	k3xPp3gMnSc3
k	k	k7c3
tomu	ten	k3xDgNnSc3
pomohl	pomoct	k5eAaPmAgMnS
mexický	mexický	k2eAgMnSc1d1
kanonýr	kanonýr	k1gMnSc1
Hugo	Hugo	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
nastřílel	nastřílet	k5eAaPmAgMnS
v	v	k7c6
lize	liga	k1gFnSc6
19	#num#	k4
branek	branka	k1gFnPc2
a	a	k8xC
obdržel	obdržet	k5eAaPmAgMnS
Trofeo	Trofeo	k1gNnSc4
Pichichi	Pichich	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
stejný	stejný	k2eAgInSc1d1
také	také	k6eAd1
skóroval	skórovat	k5eAaBmAgInS
dvakrát	dvakrát	k6eAd1
ve	v	k7c6
finále	finále	k1gNnSc6
španělského	španělský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgMnSc6,k3yIgMnSc6,k3yQgMnSc6
Atlético	Atlético	k6eAd1
porazilo	porazit	k5eAaPmAgNnS
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gNnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sánchez	Sánchez	k1gInSc1
v	v	k7c6
týmu	tým	k1gInSc6
strávil	strávit	k5eAaPmAgMnS
jedinou	jediný	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
odebral	odebrat	k5eAaPmAgMnS
přes	přes	k7c4
město	město	k1gNnSc4
do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
tuto	tento	k3xDgFnSc4
ztrátu	ztráta	k1gFnSc4
dovedl	dovést	k5eAaPmAgInS
Aragonés	Aragonés	k1gInSc1
tým	tým	k1gInSc1
k	k	k7c3
vítězství	vítězství	k1gNnSc3
ve	v	k7c6
španělském	španělský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
dokormidloval	dokormidlovat	k5eAaPmAgInS,k5eAaBmAgInS,k5eAaImAgInS
tým	tým	k1gInSc1
do	do	k7c2
finále	finále	k1gNnSc2
Poháru	pohár	k1gInSc2
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
však	však	k9
podlehl	podlehnout	k5eAaPmAgInS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
Dynamu	dynamo	k1gNnSc3
Kyjev	Kyjev	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Éra	éra	k1gFnSc1
Jesúse	Jesúse	k1gFnSc1
Gila	Gila	k1gFnSc1
</s>
<s>
Jesús	Jesús	k1gInSc1
Gil	Gil	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prezidentem	prezident	k1gMnSc7
týmu	tým	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlético	Atlético	k1gNnSc1
tehdy	tehdy	k6eAd1
již	již	k6eAd1
deset	deset	k4xCc1
let	léto	k1gNnPc2
neslavilo	slavit	k5eNaImAgNnS
titul	titul	k1gInSc4
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
a	a	k8xC
zoufale	zoufale	k6eAd1
se	se	k3xPyFc4
snažilo	snažit	k5eAaImAgNnS
o	o	k7c4
nějaký	nějaký	k3yIgInSc4
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gil	Gil	k1gMnSc1
přivedl	přivést	k5eAaPmAgMnS
několik	několik	k4yIc1
velice	velice	k6eAd1
drahých	drahý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
např.	např.	kA
Paula	Paul	k1gMnSc2
Futreho	Futre	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
titul	titul	k1gInSc4
zůstáuval	zůstáuvat	k5eAaBmAgMnS,k5eAaImAgMnS,k5eAaPmAgMnS
stále	stále	k6eAd1
nedotknutelný	nedotknutelný	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gil	Gil	k1gFnSc1
v	v	k7c6
následujících	následující	k2eAgInPc6d1
letech	let	k1gInPc6
najal	najmout	k5eAaPmAgMnS
a	a	k8xC
propustil	propustit	k5eAaPmAgMnS
hodně	hodně	k6eAd1
manažerů	manažer	k1gMnPc2
<g/>
,	,	kIx,
např.	např.	kA
Césara	César	k1gMnSc2
Luise	Luisa	k1gFnSc6
Menottiho	Menotti	k1gMnSc2
<g/>
,	,	kIx,
Rona	Ron	k1gMnSc2
Atkinsona	Atkinson	k1gMnSc2
<g/>
,	,	kIx,
Javiera	Javier	k1gMnSc2
Clementa	Clement	k1gMnSc2
a	a	k8xC
vracejícího	vracející	k2eAgMnSc2d1
se	se	k3xPyFc4
Luise	Luisa	k1gFnSc3
Aragonése	Aragonése	k1gFnSc2
v	v	k7c6
honbě	honba	k1gFnSc6
za	za	k7c7
úspěchem	úspěch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
manažerem	manažer	k1gInSc7
Radomir	Radomira	k1gFnPc2
Antić	Antić	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
sestavou	sestava	k1gFnSc7
obsahující	obsahující	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
jako	jako	k8xS,k8xC
José	José	k1gNnSc1
Luis	Luisa	k1gFnPc2
Caminero	Caminero	k1gNnSc1
<g/>
,	,	kIx,
Luboslav	Luboslav	k1gMnSc1
Penev	Penev	k1gFnSc1
<g/>
,	,	kIx,
Diego	Diega	k1gMnSc5
Simeone	Simeon	k1gMnSc5
<g/>
,	,	kIx,
Milinko	Milinka	k1gFnSc5
Pantić	Pantić	k1gMnSc1
<g/>
,	,	kIx,
Juan	Juan	k1gMnSc1
Manuel	Manuela	k1gFnPc2
Lopéz	Lopéza	k1gFnPc2
a	a	k8xC
Kiko	Kiko	k1gNnSc1
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
vytoužený	vytoužený	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
přišel	přijít	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlético	Atlético	k1gNnSc1
tehdy	tehdy	k6eAd1
vyhrálo	vyhrát	k5eAaPmAgNnS
jak	jak	k8xC,k8xS
ligu	liga	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
národní	národní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
ani	ani	k8xC
tento	tento	k3xDgInSc1
výsledek	výsledek	k1gInSc1
nijak	nijak	k6eAd1
nezměnil	změnit	k5eNaPmAgInS
Gilovu	Gilův	k2eAgFnSc4d1
taktiku	taktika	k1gFnSc4
<g/>
,	,	kIx,
Antić	Antić	k1gFnSc1
se	se	k3xPyFc4
u	u	k7c2
týmu	tým	k1gInSc2
udržel	udržet	k5eAaPmAgInS
ještě	ještě	k6eAd1
následující	následující	k2eAgFnPc4d1
tři	tři	k4xCgFnPc4
sezóny	sezóna	k1gFnPc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
nahrazen	nahradit	k5eAaPmNgInS
Arrigem	Arrig	k1gInSc7
Sacchim	Sacchimo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
se	se	k3xPyFc4
nakrátko	nakrátko	k6eAd1
Antić	Antić	k1gMnSc1
vrátil	vrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
avšak	avšak	k8xC
již	již	k6eAd1
záhy	záhy	k6eAd1
jej	on	k3xPp3gMnSc4
vystřídal	vystřídat	k5eAaPmAgMnS
Claudio	Claudio	k1gMnSc1
Ranieri	Ranier	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezóna	sezóna	k1gFnSc1
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
však	však	k9
skončila	skončit	k5eAaPmAgFnS
pro	pro	k7c4
tým	tým	k1gInSc4
katastrofou	katastrofa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ranieri	Ranieri	k1gNnSc1
byl	být	k5eAaImAgInS
vyhozen	vyhozen	k2eAgInSc1d1
a	a	k8xC
třetí	třetí	k4xOgInSc1
příchod	příchod	k1gInSc1
Antiće	Antić	k1gInSc2
selhal	selhat	k5eAaPmAgInS
zabránění	zabránění	k1gNnSc4
nevyhnutelného	vyhnutelný	k2eNgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
úspěch	úspěch	k1gInSc4
v	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
tým	tým	k1gInSc1
dokráčel	dokráčet	k5eAaPmAgInS
až	až	k9
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
sestupu	sestup	k1gInSc3
do	do	k7c2
Segunda	Segund	k1gMnSc2
División	División	k1gInSc4
<g/>
,	,	kIx,
druhé	druhý	k4xOgFnSc2
nejvyšší	vysoký	k2eAgFnSc2d3
španělské	španělský	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
Atlético	Atlético	k6eAd1
strávilo	strávit	k5eAaPmAgNnS
dvě	dva	k4xCgFnPc4
sezóny	sezóna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
se	se	k3xPyFc4
neumístilo	umístit	k5eNaPmAgNnS
na	na	k7c6
postupových	postupový	k2eAgFnPc6d1
pozicích	pozice	k1gFnPc6
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
celkovým	celkový	k2eAgNnSc7d1
vítězem	vítěz	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
Finále	finále	k1gNnSc1
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Před	před	k7c7
smrtí	smrt	k1gFnSc7
Jesúse	Jesúse	k1gFnSc1
Gila	Gilum	k1gNnPc4
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
novým	nový	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
klubu	klub	k1gInSc2
Enrique	Enriqu	k1gFnSc2
Cerezo	Cereza	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgNnSc2d1
tisíciletí	tisíciletí	k1gNnSc2
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
tabulce	tabulka	k1gFnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
Atlético	Atlético	k6eAd1
mělo	mít	k5eAaImAgNnS
pohybovat	pohybovat	k5eAaImF
okolo	okolo	k7c2
středu	střed	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
se	se	k3xPyFc4
k	k	k7c3
týmu	tým	k1gInSc3
přidalo	přidat	k5eAaPmAgNnS
několik	několik	k4yIc1
velice	velice	k6eAd1
zdatných	zdatný	k2eAgMnPc2d1
fotbalistů	fotbalista	k1gMnPc2
–	–	k?
Fernando	Fernanda	k1gFnSc5
Torres	Torres	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
talentů	talent	k1gInPc2
nedávné	dávný	k2eNgFnSc2d1
minulosti	minulost	k1gFnSc2
španělského	španělský	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
<g/>
,	,	kIx,
portugalští	portugalský	k2eAgMnPc1d1
reprezentanti	reprezentant	k1gMnPc1
a	a	k8xC
internacionálové	internacionál	k1gMnPc1
Costinha	Costinha	k1gMnSc1
a	a	k8xC
Maniche	Maniche	k1gFnSc1
a	a	k8xC
Argentinec	Argentinec	k1gMnSc1
Sergio	Sergio	k6eAd1
Agüero	Agüero	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
tým	tým	k1gInSc1
vynakládá	vynakládat	k5eAaImIp3nS
nemalé	malý	k2eNgFnPc4d1
finanční	finanční	k2eAgFnPc4d1
částky	částka	k1gFnPc4
na	na	k7c4
platy	plat	k1gInPc4
svých	svůj	k3xOyFgMnPc2
kvalitních	kvalitní	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
dobré	dobrý	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
prosazuje	prosazovat	k5eAaImIp3nS
jen	jen	k9
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
úsilím	úsilí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2007	#num#	k4
klub	klub	k1gInSc1
opustil	opustit	k5eAaPmAgInS
Torres	Torres	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
upsal	upsat	k5eAaPmAgInS
anglickému	anglický	k2eAgMnSc3d1
Liverpoolu	Liverpool	k1gInSc2
<g/>
,	,	kIx,
opačným	opačný	k2eAgInSc7d1
směrem	směr	k1gInSc7
putoval	putovat	k5eAaImAgMnS
jiný	jiný	k2eAgMnSc1d1
Španěl	Španěl	k1gMnSc1
–	–	k?
Luis	Luisa	k1gFnPc2
García	Garcí	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Villarrealu	Villarreal	k1gInSc2
přišel	přijít	k5eAaPmAgMnS
reprezentant	reprezentant	k1gMnSc1
Uruguaye	Uruguay	k1gFnSc2
Diego	Diego	k1gMnSc1
Forlán	Forlán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezónách	sezóna	k1gFnPc6
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
a	a	k8xC
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
klub	klub	k1gInSc1
hrál	hrát	k5eAaImAgInS
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
případě	případ	k1gInSc6
vypadl	vypadnout	k5eAaPmAgMnS
v	v	k7c6
osmifinále	osmifinále	k1gNnSc6
s	s	k7c7
Portem	port	k1gInSc7
a	a	k8xC
druhém	druhý	k4xOgInSc6
se	se	k3xPyFc4
loučil	loučit	k5eAaImAgMnS
se	s	k7c7
soutěží	soutěž	k1gFnSc7
už	už	k9
po	po	k7c6
základních	základní	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
dokázal	dokázat	k5eAaPmAgMnS
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
probojovat	probojovat	k5eAaPmF
až	až	k9
k	k	k7c3
vítězství	vítězství	k1gNnSc3
<g/>
,	,	kIx,
když	když	k8xS
Atlético	Atlético	k6eAd1
postupně	postupně	k6eAd1
vyřadilo	vyřadit	k5eAaPmAgNnS
Galatasaray	Galatasaraa	k1gFnSc2
<g/>
,	,	kIx,
Sporting	Sporting	k1gInSc4
CP	CP	kA
<g/>
,	,	kIx,
Valencii	Valencie	k1gFnSc4
a	a	k8xC
Liverpool	Liverpool	k1gInSc1
<g/>
,	,	kIx,
následně	následně	k6eAd1
si	se	k3xPyFc3
ve	v	k7c6
finálovém	finálový	k2eAgInSc6d1
zápase	zápas	k1gInSc6
poradili	poradit	k5eAaPmAgMnP
s	s	k7c7
anglickým	anglický	k2eAgInSc7d1
klubem	klub	k1gInSc7
z	z	k7c2
Londýna	Londýn	k1gInSc2
Fulhamem	Fulham	k1gInSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
po	po	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
trenéra	trenér	k1gMnSc2
Quique	Quiqu	k1gMnSc2
Sáncheze	Sáncheze	k1gFnSc2
Florese	Florese	k1gFnSc1
táhli	táhnout	k5eAaImAgMnP
hráči	hráč	k1gMnPc1
jako	jako	k8xS,k8xC
Sergio	Sergio	k6eAd1
Agüero	Agüero	k1gNnSc1
<g/>
,	,	kIx,
Diego	Diego	k1gNnSc1
Forlán	Forlán	k1gInSc1
<g/>
,	,	kIx,
Simã	Simã	k1gNnSc1
<g/>
,	,	kIx,
José	José	k1gNnSc1
Manuel	Manuela	k1gFnPc2
Jurado	Jurada	k1gFnSc5
<g/>
,	,	kIx,
José	Josí	k1gMnPc4
Antonio	Antonio	k1gMnSc1
Reyes	Reyes	k1gMnSc1
<g/>
,	,	kIx,
Antonio	Antonio	k1gMnSc1
López	López	k1gMnSc1
<g/>
,	,	kIx,
Álvaro	Álvara	k1gFnSc5
Domínguez	Domínguez	k1gMnSc1
nebo	nebo	k8xC
devatenáctiletý	devatenáctiletý	k2eAgMnSc1d1
brankář	brankář	k1gMnSc1
David	David	k1gMnSc1
de	de	k?
Gea	Gea	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Triumf	triumf	k1gInSc1
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
si	se	k3xPyFc3
Atlético	Atlético	k6eAd1
zopakovalo	zopakovat	k5eAaPmAgNnS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
porazilo	porazit	k5eAaPmAgNnS
ve	v	k7c6
finále	finále	k1gNnSc6
další	další	k2eAgInSc4d1
španělský	španělský	k2eAgInSc4d1
tým	tým	k1gInSc4
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gNnSc1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
základní	základní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
B	B	kA
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
bylo	být	k5eAaImAgNnS
Atlético	Atlético	k1gNnSc1
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
obhájce	obhájce	k1gMnSc1
trofeje	trofej	k1gFnSc2
<g/>
)	)	kIx)
přilosováno	přilosovat	k5eAaBmNgNnS,k5eAaImNgNnS,k5eAaPmNgNnS
k	k	k7c3
týmům	tým	k1gInPc3
Académica	Académic	k1gInSc2
de	de	k?
Coimbra	Coimbra	k1gFnSc1
(	(	kIx(
<g/>
Portugalsko	Portugalsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hapoel	Hapoel	k1gMnSc1
Tel	tel	kA
Aviv	Aviv	k1gMnSc1
(	(	kIx(
<g/>
Izrael	Izrael	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skupině	skupina	k1gFnSc6
obsadilo	obsadit	k5eAaPmAgNnS
Atlético	Atlético	k6eAd1
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
za	za	k7c4
první	první	k4xOgInSc4
Plzní	Plzeň	k1gFnPc2
(	(	kIx(
<g/>
bilance	bilance	k1gFnSc2
4	#num#	k4
výhry	výhra	k1gFnSc2
<g/>
,	,	kIx,
0	#num#	k4
remíz	remíza	k1gFnPc2
a	a	k8xC
2	#num#	k4
prohry	prohra	k1gFnPc4
<g/>
,	,	kIx,
12	#num#	k4
bodů	bod	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
šestnáctifinále	šestnáctifinála	k1gFnSc6
bylo	být	k5eAaImAgNnS
vyřazeno	vyřadit	k5eAaPmNgNnS
ruským	ruský	k2eAgInSc7d1
klubem	klub	k1gInSc7
FC	FC	kA
Rubin	Rubin	k1gInSc1
Kazaň	Kazaň	k1gFnSc1
poměrem	poměr	k1gInSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
z	z	k7c2
dvojzápasu	dvojzápas	k1gInSc2
(	(	kIx(
<g/>
domácí	domácí	k2eAgFnSc1d1
prohra	prohra	k1gFnSc1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
venkovní	venkovní	k2eAgFnSc1d1
výhra	výhra	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2013	#num#	k4
porazilo	porazit	k5eAaPmAgNnS
Atlético	Atlético	k1gNnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
<g/>
,	,	kIx,
skórovali	skórovat	k5eAaBmAgMnP
Diego	Diego	k6eAd1
da	da	k?
Silva	Silva	k1gFnSc1
Costa	Costa	k1gFnSc1
a	a	k8xC
Joã	Joã	k1gNnSc1
Miranda	Mirando	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
jubilejní	jubilejní	k2eAgMnSc1d1
desátý	desátý	k4xOgInSc1
triumf	triumf	k1gInSc1
klubu	klub	k1gInSc2
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
sezoně	sezona	k1gFnSc6
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
vyhrálo	vyhrát	k5eAaPmAgNnS
Atlético	Atlético	k1gNnSc1
titul	titul	k1gInSc1
v	v	k7c6
La	la	k1gNnSc6
Lize	liga	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
udrželo	udržet	k5eAaPmAgNnS
za	za	k7c7
zády	záda	k1gNnPc7
Real	Real	k1gInSc1
i	i	k8xC
Barcelonu	Barcelona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
proti	proti	k7c3
Realu	Real	k1gInSc3
Madrid	Madrid	k1gInSc1
mělo	mít	k5eAaImAgNnS
na	na	k7c4
dosah	dosah	k1gInSc4
celkový	celkový	k2eAgInSc4d1
triumf	triumf	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
prohrálo	prohrát	k5eAaPmAgNnS
s	s	k7c7
městským	městský	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
nastaveném	nastavený	k2eAgInSc6d1
čase	čas	k1gInSc6
vedlo	vést	k5eAaImAgNnS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
ovšem	ovšem	k9
chvíli	chvíle	k1gFnSc4
před	před	k7c7
koncem	konec	k1gInSc7
vyrovnal	vyrovnat	k5eAaPmAgMnS,k5eAaBmAgMnS
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
na	na	k7c4
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
mužstvo	mužstvo	k1gNnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
úplně	úplně	k6eAd1
zhroutilo	zhroutit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
sezony	sezona	k1gFnSc2
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
získalo	získat	k5eAaPmAgNnS
španělský	španělský	k2eAgInSc4d1
Superpohár	superpohár	k1gInSc4
(	(	kIx(
<g/>
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
když	když	k8xS
porazilo	porazit	k5eAaPmAgNnS
Real	Real	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
druhá	druhý	k4xOgFnSc1
trofej	trofej	k1gFnSc1
klubu	klub	k1gInSc2
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
koncem	konec	k1gInSc7
sezóny	sezóna	k1gFnSc2
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
přišlo	přijít	k5eAaPmAgNnS
i	i	k9
rozloučení	rozloučení	k1gNnSc1
se	s	k7c7
stadionem	stadion	k1gInSc7
Vicente	Vicent	k1gInSc5
Calderóna	Calderóna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nové	nový	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
se	se	k3xPyFc4
klub	klub	k1gInSc1
přestěhuje	přestěhovat	k5eAaPmIp3nS
do	do	k7c2
supermoderního	supermoderní	k2eAgInSc2d1
fotbalového	fotbalový	k2eAgInSc2d1
stánku	stánek	k1gInSc2
Wanda	Wanda	k1gFnSc1
Metropolitano	Metropolitana	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
rovněž	rovněž	k9
jako	jako	k9
La	la	k1gNnSc4
Peineta	Peineto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
městský	městský	k2eAgInSc4d1
stadión	stadión	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
přestavba	přestavba	k1gFnSc1
byla	být	k5eAaImAgFnS
zamýšlena	zamýšlet	k5eAaImNgFnS
pro	pro	k7c4
možné	možný	k2eAgNnSc4d1
budoucí	budoucí	k2eAgNnSc4d1
využití	využití	k1gNnSc4
při	při	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
město	město	k1gNnSc1
nikdy	nikdy	k6eAd1
nevyhrálo	vyhrát	k5eNaPmAgNnS
volbu	volba	k1gFnSc4
zaručující	zaručující	k2eAgNnSc1d1
konání	konání	k1gNnSc1
tohoto	tento	k3xDgInSc2
sportovního	sportovní	k2eAgInSc2d1
svátku	svátek	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
mezi	mezi	k7c7
radnicí	radnice	k1gFnSc7
<g/>
,	,	kIx,
klubem	klub	k1gInSc7
a	a	k8xC
titulárním	titulární	k2eAgMnSc7d1
sponzorem	sponzor	k1gMnSc7
pivovarem	pivovar	k1gInSc7
Mahou	Mahá	k1gFnSc4
o	o	k7c4
rekonstrukci	rekonstrukce	k1gFnSc4
stadiónu	stadión	k1gInSc2
na	na	k7c4
nový	nový	k2eAgInSc4d1
moderní	moderní	k2eAgInSc4d1
fotbalový	fotbalový	k2eAgInSc4d1
stánek	stánek	k1gInSc4
pro	pro	k7c4
Atlético	Atlético	k1gNnSc4
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontroverze	kontroverze	k1gFnSc1
ovšem	ovšem	k9
budí	budit	k5eAaImIp3nS
čínský	čínský	k2eAgMnSc1d1
sponzor	sponzor	k1gMnSc1
Wanda	Wanda	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
titulárním	titulární	k2eAgMnSc7d1
sponzorem	sponzor	k1gMnSc7
Metropolitano	Metropolitana	k1gFnSc5
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
rozlítilo	rozlítit	k5eAaPmAgNnS
některé	některý	k3yIgMnPc4
fanoušky	fanoušek	k1gMnPc4
Colchoneros	Colchonerosa	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
považují	považovat	k5eAaImIp3nP
jméno	jméno	k1gNnSc4
čínského	čínský	k2eAgInSc2d1
konglomerátu	konglomerát	k1gInSc2
v	v	k7c6
názvu	název	k1gInSc6
stadionu	stadion	k1gInSc2
za	za	k7c4
výsměch	výsměch	k1gInSc4
historii	historie	k1gFnSc4
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Kapacita	kapacita	k1gFnSc1
činí	činit	k5eAaImIp3nS
68	#num#	k4
000	#num#	k4
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
novou	nový	k2eAgFnSc7d1
sezónou	sezóna	k1gFnSc7
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
přichází	přicházet	k5eAaImIp3nS
i	i	k9
modernizace	modernizace	k1gFnSc1
klubového	klubový	k2eAgNnSc2d1
loga	logo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
spíše	spíše	k9
o	o	k7c4
evoluci	evoluce	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
za	za	k7c4
úkol	úkol	k1gInSc4
přiblížit	přiblížit	k5eAaPmF
logo	logo	k1gNnSc4
nové	nový	k2eAgFnSc6d1
éře	éra	k1gFnSc3
Atlética	Atlétic	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
jednoduchostí	jednoduchost	k1gFnSc7
<g/>
,	,	kIx,
lehkostí	lehkost	k1gFnSc7
<g/>
,	,	kIx,
redukcí	redukce	k1gFnSc7
zdobných	zdobný	k2eAgInPc2d1
prvků	prvek	k1gInPc2
a	a	k8xC
celkově	celkově	k6eAd1
zaoblenějšího	zaoblený	k2eAgInSc2d2
tvaru	tvar	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1903	#num#	k4
–	–	k?
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
(	(	kIx(
<g/>
Sucursal	Sucursal	k1gFnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1921	#num#	k4
–	–	k?
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc4
de	de	k?
Madrid	Madrid	k1gInSc1
</s>
<s>
1939	#num#	k4
–	–	k?
fúze	fúze	k1gFnSc1
s	s	k7c7
Aviación	Aviación	k1gMnSc1
Nacional	Nacional	k1gMnSc7
de	de	k?
Zaragoza	Zaragoza	k1gFnSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
<g/>
>	>	kIx)
Athletic	Athletice	k1gFnPc2
Aviación	Aviación	k1gInSc1
de	de	k?
Madrid	Madrid	k1gInSc1
</s>
<s>
1941	#num#	k4
–	–	k?
Atlético	Atlético	k1gMnSc1
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1947	#num#	k4
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Úspěchy	úspěch	k1gInPc1
A	a	k8xC
<g/>
–	–	k?
<g/>
týmu	tým	k1gInSc2
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1974	#num#	k4
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
(	(	kIx(
<g/>
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
<g/>
;	;	kIx,
10	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Athletic	Athletice	k1gFnPc2
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
<g/>
;	;	kIx,
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
–	–	k?
Atlético	Atlético	k1gMnSc1
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
;	;	kIx,
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
–	–	k?
Atlético	Atlético	k1gMnSc1
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
;	;	kIx,
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
71	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
<g/>
,	,	kIx,
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
;	;	kIx,
10	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
(	(	kIx(
<g/>
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
</s>
<s>
Menší	malý	k2eAgInPc1d2
úspěchy	úspěch	k1gInPc1
</s>
<s>
Copa	Copa	k1gFnSc1
de	de	k?
los	los	k1gInSc1
Campeones	Campeones	k1gMnSc1
de	de	k?
Españ	Españ	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Atlético	Atlético	k1gMnSc1
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1947	#num#	k4
</s>
<s>
Copa	Copa	k6eAd1
Presidente	president	k1gMnSc5
FEF	FEF	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Atlético	Atlético	k1gMnSc1
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
</s>
<s>
Copa	Cop	k2eAgFnSc1d1
Eva	Eva	k1gFnSc1
Duarte	Duart	k1gInSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1951	#num#	k4
</s>
<s>
Campeonato	Campeonato	k6eAd1
Regional	Regional	k1gFnSc7
Centro	Centro	k1gNnSc4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
(	(	kIx(
<g/>
Sucursal	Sucursal	k1gFnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
;	;	kIx,
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
<g/>
,	,	kIx,
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
<g/>
;	;	kIx,
Athletic	Athletice	k1gFnPc2
Aviación	Aviación	k1gInSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
</s>
<s>
–	–	k?
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
(	(	kIx(
<g/>
Sucursal	Sucursal	k1gFnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
1917	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
20	#num#	k4
<g/>
;	;	kIx,
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
</s>
<s>
Copa	Copa	k6eAd1
Federación	Federación	k1gInSc1
Centro	Centro	k1gNnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Athletic	Athletice	k1gFnPc2
Aviación	Aviación	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trofeo	Trofeo	k6eAd1
Teresa	Teresa	k1gFnSc1
Herrera	Herrera	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1956	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
Coupe	coup	k1gInSc5
Mohammed	Mohammed	k1gMnSc1
V	V	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1965	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Colombino	Colombin	k2eAgNnSc1d1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ramón	Ramón	k1gInSc1
de	de	k?
Carranza	Carranz	k1gMnSc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1968	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
</s>
<s>
Trofeo	Trofeo	k1gMnSc1
Villa	Villa	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
</s>
<s>
Iberian	Iberian	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1991	#num#	k4
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Marbella	Marbella	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
</s>
<s>
Trofeo	Trofeo	k1gMnSc1
Naranja	Naranja	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1995	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Alicante	Alicant	k1gMnSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1996	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Zaragoza	Zaragoza	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
2004	#num#	k4
</s>
<s>
Shanghai	Shanghai	k6eAd1
International	International	k1gMnSc1
Football	Football	k1gMnSc1
Tournament	Tournament	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
2006	#num#	k4
</s>
<s>
Memorial	Memorial	k1gMnSc1
Jesús	Jesúsa	k1gFnPc2
Gil	Gil	k1gMnSc1
y	y	k?
Gil	Gil	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
</s>
<s>
Audi	Audi	k1gNnSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
mládeže	mládež	k1gFnSc2
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
ligová	ligový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
/	/	kIx~
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
jako	jako	k8xS,k8xC
turnaj	turnaj	k1gInSc1
Copa	Cop	k1gInSc2
de	de	k?
Campeones	Campeones	k1gMnSc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
ligová	ligový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
kvalifikace	kvalifikace	k1gFnSc2
do	do	k7c2
soutěže	soutěž	k1gFnSc2
Copa	Copa	k1gMnSc1
de	de	k?
Campeones	Campeones	k1gMnSc1
<g/>
;	;	kIx,
9	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
Juvenil	Juvenil	k1gFnSc2
<g/>
;	;	kIx,
5	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Club	club	k1gInSc1
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
1952	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Významní	významný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Adelardo	Adelardo	k1gNnSc1
Rodríguez	Rodrígueza	k1gFnPc2
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Luis	Luisa	k1gFnPc2
Aragonés	Aragonésa	k1gFnPc2
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hugo	Hugo	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fernando	Fernando	k6eAd1
Torres	Torres	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Antoine	Antoinout	k5eAaPmIp3nS
Griezmann	Griezmann	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1929	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
:	:	kIx,
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1930	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
:	:	kIx,
Segunda	Segunda	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1934	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
:	:	kIx,
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
:	:	kIx,
Segunda	Segunda	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	z	k7c2
–	–	k?
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
V	v	k7c6
–	–	k?
výhry	výhra	k1gFnPc1
<g/>
,	,	kIx,
R	R	kA
–	–	k?
remízy	remíza	k1gFnPc4
<g/>
,	,	kIx,
P	P	kA
–	–	k?
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
–	–	k?
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
–	–	k?
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
–	–	k?
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
–	–	k?
body	bod	k1gInPc1
<g/>
,	,	kIx,
zlaté	zlatý	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
stříbrné	stříbrný	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
bronzové	bronzový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
1929	#num#	k4
–	–	k?
<g/>
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1929	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1188284341	#num#	k4
<g/>
+	+	kIx~
<g/>
218	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
11852113250-1812	11852113250-1812	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
</s>
<s>
Segunda	Segunda	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
21811164731	#num#	k4
<g/>
+	+	kIx~
<g/>
1623	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
</s>
<s>
Segunda	Segunda	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
2168263834	#num#	k4
<g/>
+	+	kIx~
<g/>
418	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
</s>
<s>
Segunda	Segunda	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
21810444035	#num#	k4
<g/>
+	+	kIx~
<g/>
524	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
</s>
<s>
Segunda	Segunda	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
21811254528	#num#	k4
<g/>
+	+	kIx~
<g/>
1724	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1228594045-521	1228594045-521	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12263133450-1615	12263133450-1615	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Španělské	španělský	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
se	se	k3xPyFc4
v	v	k7c6
letech	let	k1gInPc6
1936	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
nehrály	hrát	k5eNaImAgInP
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
právě	právě	k9
probíhající	probíhající	k2eAgFnSc3d1
občanské	občanský	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12214174329	#num#	k4
<g/>
+	+	kIx~
<g/>
1429	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12213727036	#num#	k4
<g/>
+	+	kIx~
<g/>
3433	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12614575044	#num#	k4
<g/>
+	+	kIx~
<g/>
633	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
126115105444	#num#	k4
<g/>
+	+	kIx~
<g/>
1027	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12615476649	#num#	k4
<g/>
+	+	kIx~
<g/>
1734	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12613584641	#num#	k4
<g/>
+	+	kIx~
<g/>
531	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
126106105048	#num#	k4
<g/>
+	+	kIx~
<g/>
226	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12613675844	#num#	k4
<g/>
+	+	kIx~
<g/>
1432	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12613767345	#num#	k4
<g/>
+	+	kIx~
<g/>
2833	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12615475432	#num#	k4
<g/>
+	+	kIx~
<g/>
2234	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12615387151	#num#	k4
<g/>
+	+	kIx~
<g/>
2033	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13017678750	#num#	k4
<g/>
+	+	kIx~
<g/>
3740	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13016598057	#num#	k4
<g/>
+	+	kIx~
<g/>
2337	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130134136570-530	130134136570-530	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130117125747	#num#	k4
<g/>
+	+	kIx~
<g/>
1029	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130117125964-529	130117125964-529	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130145117549	#num#	k4
<g/>
+	+	kIx~
<g/>
2633	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130154116554	#num#	k4
<g/>
+	+	kIx~
<g/>
1134	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130161047843	#num#	k4
<g/>
+	+	kIx~
<g/>
3542	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130136115848	#num#	k4
<g/>
+	+	kIx~
<g/>
1032	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130153125940	#num#	k4
<g/>
+	+	kIx~
<g/>
1933	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13017675735	#num#	k4
<g/>
+	+	kIx~
<g/>
2240	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13015695036	#num#	k4
<g/>
+	+	kIx~
<g/>
1436	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13014976136	#num#	k4
<g/>
+	+	kIx~
<g/>
2537	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130109113734	#num#	k4
<g/>
+	+	kIx~
<g/>
329	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13020375827	#num#	k4
<g/>
+	+	kIx~
<g/>
3143	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13018845420	#num#	k4
<g/>
+	+	kIx~
<g/>
3444	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13014795730	#num#	k4
<g/>
+	+	kIx~
<g/>
2735	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13012993832	#num#	k4
<g/>
+	+	kIx~
<g/>
633	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1301010104037	#num#	k4
<g/>
+	+	kIx~
<g/>
330	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13018665322	#num#	k4
<g/>
+	+	kIx~
<g/>
3142	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13017855120	#num#	k4
<g/>
+	+	kIx~
<g/>
3142	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134141194528	#num#	k4
<g/>
+	+	kIx~
<g/>
1739	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13420864929	#num#	k4
<g/>
+	+	kIx~
<g/>
2048	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134168105031	#num#	k4
<g/>
+	+	kIx~
<g/>
1940	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1341113104634	#num#	k4
<g/>
+	+	kIx~
<g/>
1235	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134186106038	#num#	k4
<g/>
+	+	kIx~
<g/>
2242	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13419876233	#num#	k4
<g/>
+	+	kIx~
<g/>
2946	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134164146152	#num#	k4
<g/>
+	+	kIx~
<g/>
936	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134141375537	#num#	k4
<g/>
+	+	kIx~
<g/>
1841	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1341011133844-631	1341011133844-631	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13417895941	#num#	k4
<g/>
+	+	kIx~
<g/>
1842	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134154153837	#num#	k4
<g/>
+	+	kIx~
<g/>
134	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13420685638	#num#	k4
<g/>
+	+	kIx~
<g/>
1846	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13417895347	#num#	k4
<g/>
+	+	kIx~
<g/>
642	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134161175128	#num#	k4
<g/>
+	+	kIx~
<g/>
2343	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13417895338	#num#	k4
<g/>
+	+	kIx~
<g/>
1542	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1441811155854	#num#	k4
<g/>
+	+	kIx~
<g/>
447	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138191096038	#num#	k4
<g/>
+	+	kIx~
<g/>
2248	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138198116945	#num#	k4
<g/>
+	+	kIx~
<g/>
2446	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138201085535	#num#	k4
<g/>
+	+	kIx~
<g/>
2050	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138171385228	#num#	k4
<g/>
+	+	kIx~
<g/>
2447	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13824596735	#num#	k4
<g/>
+	+	kIx~
<g/>
3253	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381611115242	#num#	k4
<g/>
+	+	kIx~
<g/>
1043	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138139165454035	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138139165654	#num#	k4
<g/>
+	+	kIx~
<g/>
235	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
14226977532	#num#	k4
<g/>
+	+	kIx~
<g/>
4387	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1422011117664	#num#	k4
<g/>
+	+	kIx~
<g/>
1271	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381612107956	#num#	k4
<g/>
+	+	kIx~
<g/>
2360	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381210165450	#num#	k4
<g/>
+	+	kIx~
<g/>
446	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138911184864-1638	138911184864-1638	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Segunda	Segunda	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
2422111105939	#num#	k4
<g/>
+	+	kIx~
<g/>
2074	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Segunda	Segunda	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
242231096844	#num#	k4
<g/>
+	+	kIx~
<g/>
2479	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381211155156-547	1381211155156-547	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381510135153-255	1381510135153-255	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381311144034	#num#	k4
<g/>
+	+	kIx~
<g/>
650	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381313124537	#num#	k4
<g/>
+	+	kIx~
<g/>
852	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138179124639	#num#	k4
<g/>
+	+	kIx~
<g/>
760	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138197126647	#num#	k4
<g/>
+	+	kIx~
<g/>
1964	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138207118057	#num#	k4
<g/>
+	+	kIx~
<g/>
2367	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138138175761-447	138138175761-447	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138177146253	#num#	k4
<g/>
+	+	kIx~
<g/>
958	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381511125346	#num#	k4
<g/>
+	+	kIx~
<g/>
756	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13823786531	#num#	k4
<g/>
+	+	kIx~
<g/>
3476	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13828647726	#num#	k4
<g/>
+	+	kIx~
<g/>
5190	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13823966729	#num#	k4
<g/>
+	+	kIx~
<g/>
3878	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13828466318	#num#	k4
<g/>
+	+	kIx~
<g/>
4588	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13823967027	#num#	k4
<g/>
+	+	kIx~
<g/>
4378	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138231055822	#num#	k4
<g/>
+	+	kIx~
<g/>
3679	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138221065529	#num#	k4
<g/>
+	+	kIx~
<g/>
2676	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138181645127	#num#	k4
<g/>
+	+	kIx~
<g/>
2470	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138	#num#	k4
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Kolo	kolo	k1gNnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Doma	doma	k6eAd1
</s>
<s>
Venku	venku	k6eAd1
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
1950	#num#	k4
</s>
<s>
Latinský	latinský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Girondins	Girondinsa	k1gFnPc2
de	de	k?
Bordeaux	Bordeaux	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
Lisabon	Lisabon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k7c2
SS	SS	kA
Lazio	Lazio	k6eAd1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
Lisabon	Lisabon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1951	#num#	k4
</s>
<s>
Latinský	latinský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k6eAd1
Sporting	Sporting	k1gInSc1
CP	CP	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
Milán	Milán	k1gInSc1
nebo	nebo	k8xC
Turín	Turín	k1gInSc1
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
Předkolo	předkolo	k1gNnSc1
Drumcondra	Drumcondr	k1gMnSc2
FC	FC	kA
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
113	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
CDNA	CDNA	kA
Sofia	Sofia	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Schalke	Schalk	k1gFnSc2
0	#num#	k4
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
Předkolo	předkolo	k1gNnSc1
CS	CS	kA
Sedan	sedan	k1gInSc1
Ardennes	Ardennes	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Leicester	Leicestra	k1gFnPc2
City	city	k1gNnSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Werder	Werdra	k1gFnPc2
Brémy	Brémy	k1gFnPc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
SC	SC	kA
Motor	motor	k1gInSc1
Jena	Jena	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
ACF	ACF	kA
Fiorentina	Fiorentin	k1gMnSc2
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
;	;	kIx,
opak	opak	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Hibernians	Hiberniansa	k1gFnPc2
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
PFK	PFK	kA
Botev	Botev	k1gFnSc1
Plovdiv	Plovdiv	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Norimberk	Norimberk	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Juventus	Juventus	k1gInSc4
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Servette	Servett	k1gInSc5
FC	FC	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Shelbourne	Shelbourn	k1gInSc5
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
RFC	RFC	kA
de	de	k?
Liè	Liè	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
Volný	volný	k2eAgInSc1d1
los	los	k1gInSc1
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
NK	NK	kA
Dinamo	Dinama	k1gFnSc5
Zagreb	Zagreb	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Ș	Ș	k1gFnSc1
Cluj	Cluj	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Malmö	Malmö	k1gFnPc2
FF	ff	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Vojvodina	Vojvodina	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Wiener	Wienra	k1gFnPc2
Sport-Club	Sport-Cluba	k1gFnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Göztepe	Göztep	k1gInSc5
AŞ	AŞ	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
KSV	KSV	kA
Waregem	Wareg	k1gInSc7
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Austria	Austrium	k1gNnSc2
Wien	Wien	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Cagliari	Cagliar	k1gFnSc2
Calcio	Calcio	k6eAd1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Legia	Legius	k1gMnSc2
Warszawa	Warszawus	k1gMnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Semifinále	semifinále	k1gNnSc1
AFC	AFC	kA
Ajax	Ajax	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Panionios	Panioniosa	k1gFnPc2
GSS	GSS	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
SC	SC	kA
Bastia	Bastia	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Galatasaray	Galatasaraa	k1gFnSc2
SK	Sk	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Dinamo	Dinama	k1gFnSc5
Bucureşti	Bucureşti	k1gNnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FK	FK	kA
Crvena	Crven	k1gMnSc2
zvezda	zvezd	k1gMnSc2
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
;	;	kIx,
opak	opak	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1974	#num#	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
Finále	finále	k1gNnSc1
CA	ca	kA
Independiente	Independient	k1gInSc5
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Kjø	Kjø	k1gFnPc2
Boldklub	Boldkluba	k1gFnPc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Derby	derby	k1gNnSc1
County	Counta	k1gFnSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Basel	Baslo	k1gNnPc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Eintracht	Eintracht	k2eAgInSc1d1
Frankfurt	Frankfurt	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
SK	Sk	kA
Rapid	rapid	k1gInSc1
Wien	Wien	k1gNnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
HNK	HNK	kA
Hajduk	hajduk	k1gMnSc1
Split	Split	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Levski	Levsk	k1gFnSc2
Spartak	Spartak	k1gInSc1
Sofia	Sofia	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Hamburger	hamburger	k1gInSc1
SV	sv	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Dinamo	Dinama	k1gFnSc5
Bucureşti	Bucureşti	k1gNnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Nantes	Nantes	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Club	club	k1gInSc1
Brugge	Brugge	k1gFnSc1
KV	KV	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Dynamo	dynamo	k1gNnSc1
Dresden	Dresdno	k1gNnPc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Boavista	Boavist	k1gMnSc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Groningen	Groningen	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Sion	Siono	k1gNnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Bangor	Bangora	k1gFnPc2
City	city	k1gNnSc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FK	FK	kA
Crvena	Crven	k1gMnSc2
zvezda	zvezd	k1gMnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Bayer	Bayer	k1gMnSc1
Uerdingen	Uerdingen	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc4
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Werder	Werdra	k1gFnPc2
Brémy	Brémy	k1gFnPc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Vitória	Vitórium	k1gNnSc2
SC	SC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Groningen	Groningen	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
ACF	ACF	kA
Fiorentina	Fiorentina	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Politehnica	Politehnicus	k1gMnSc2
Timiș	Timiș	k1gMnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Fyllingen	Fyllingen	k1gInSc4
Fotball	Fotball	k1gInSc1
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Club	club	k1gInSc1
Brugge	Brugge	k1gFnSc1
KV	KV	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
NK	NK	kA
Maribor	Maribor	k1gInSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Trabzonspor	Trabzonspora	k1gFnPc2
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Olympiacos	Olympiacosa	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Parma	Parma	k1gFnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Heart	Hearta	k1gFnPc2
of	of	k?
Midlothian	Midlothiana	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
OFI	OFI	kA
Kréta	Kréta	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
FC	FC	kA
Steaua	Steaua	k1gFnSc1
Bucureș	Bucureș	k1gFnSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Widzew	Widzew	k?
Łódź	Łódź	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
AFC	AFC	kA
Ajax	Ajax	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Leicester	Leicestra	k1gFnPc2
City	city	k1gNnSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PAOK	PAOK	kA
FC	FC	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
49	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
GNK	GNK	kA
Croatia	Croatia	k1gFnSc1
Zagreb	Zagreb	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Aston	Astona	k1gFnPc2
Villa	Villo	k1gNnSc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Semifinále	semifinále	k1gNnSc1
SS	SS	kA
Lazio	Lazio	k6eAd1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Obilić	Obilić	k1gFnSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PFK	PFK	kA
CSKA	CSKA	kA
Sofia	Sofia	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Real	Real	k1gInSc4
Sociedad	Sociedad	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
AS	as	k1gNnSc2
Roma	Rom	k1gMnSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Parma	Parma	k1gFnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
MKE	MKE	kA
Ankaragücü	Ankaragücü	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Amica	Amica	k1gFnSc1
Wronki	Wronki	k1gNnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
VfL	VfL	k1gFnSc2
Wolfsburg	Wolfsburg	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
RC	RC	kA
Lens	Lens	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
Intertoto	Intertota	k1gFnSc5
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Tescoma	Tescoma	k1gNnSc4
Zlín	Zlín	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Semifinále	semifinále	k1gNnSc1
OFK	OFK	kA
Bělehrad	Bělehrad	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finála	k1gFnSc3
Villarreal	Villarreal	k1gMnSc1
CF	CF	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
Intertoto	Intertota	k1gFnSc5
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
ACF	ACF	kA
Gloria	gloria	k1gNnSc4
Bistriț	Bistriț	k1gInSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
FK	FK	kA
Vojvodina	Vojvodina	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Kayseri	Kayseri	k1gNnSc2
Erciyesspor	Erciyesspor	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
FK	FK	kA
Lokomotiv	lokomotiva	k1gFnPc2
Moskva	Moskva	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Aberdeen	Aberdeen	k1gInSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FC	FC	kA
Kø	Kø	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Panathinaikos	Panathinaikos	k1gInSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
Bolton	Bolton	k1gInSc1
Wanderers	Wanderers	k1gInSc4
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
FC	FC	kA
Schalke	Schalk	k1gFnSc2
0	#num#	k4
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc4d1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Olympique	Olympique	k6eAd1
de	de	k?
Marseille	Marseille	k1gFnPc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Panathinaikos	Panathinaikosa	k1gFnPc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
APOEL	APOEL	kA
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
Galatasaray	Galatasaraa	k1gFnSc2
SK	Sk	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Sporting	Sporting	k1gInSc1
CP	CP	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Valencia	Valencium	k1gNnSc2
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
Fulham	Fulham	k1gInSc1
FC	FC	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
FC	FC	kA
Internazionale	Internazionale	k1gMnSc2
Milano	Milana	k1gFnSc5
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Aris	Aris	k1gInSc1
Soluň	Soluň	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Rosenborg	Rosenborg	k1gInSc1
BK	BK	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Strø	Strø	k1gFnPc2
IF	IF	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Vitória	Vitórium	k1gNnSc2
SC	SC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
I	i	k9
</s>
<s>
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Stade	Stade	k6eAd1
Rennais	Rennais	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Udinese	Udinést	k5eAaPmIp3nS
Calcio	Calcio	k6eAd1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
SS	SS	kA
Lazio	Lazio	k6eAd1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Beşiktaş	Beşiktaş	k1gFnPc2
JK	JK	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Hannover	Hannover	k1gInSc1
962	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Valencia	Valencium	k1gNnSc2
CF	CF	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k6eAd1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Hapoel	Hapoel	k1gMnSc1
Tel	tel	kA
Aviv	Aviv	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Académica	Académic	k2eAgFnSc1d1
de	de	k?
Coimbra	Coimbra	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
FK	FK	kA
Rubin	Rubin	k1gInSc1
Kazaň	Kazaň	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
</s>
<s>
FK	FK	kA
Zenit	zenit	k1gInSc1
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FK	FK	kA
Austria	Austrium	k1gNnSc2
Wien	Wien	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Olympiacos	Olympiacos	k1gInSc1
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Malmö	Malmö	k?
FF	ff	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SL	SL	kA
Benfica	Benfica	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FC	FC	kA
Astana	Astana	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgMnSc1d1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc4d1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FK	FK	kA
Rostov	Rostov	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Leicester	Leicestra	k1gFnPc2
City	city	k1gNnSc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
AS	as	k1gNnSc1
Roma	Rom	k1gMnSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Qarabağ	Qarabağ	k?
FK	FK	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
FC	FC	kA
Kø	Kø	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FK	FK	kA
Lokomotiv	lokomotiva	k1gFnPc2
Moskva	Moskva	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Sporting	Sporting	k1gInSc1
CP	CP	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Arsenal	Arsenal	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Olympique	Olympiqu	k1gFnSc2
de	de	k?
Marseille	Marseille	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
AS	as	k9
Monaco	Monaco	k6eAd1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Club	club	k1gInSc1
Brugge	Brugg	k1gInSc2
KV	KV	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
Lokomotiv	lokomotiva	k1gFnPc2
Moskva	Moskva	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
Prodloužení	prodloužení	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
Soupeř	soupeř	k1gMnSc1
bude	být	k5eAaImBp3nS
upřesněn	upřesnit	k5eAaPmNgMnS
po	po	k7c4
dohrání	dohrání	k1gNnSc4
všech	všecek	k3xTgFnPc2
odvet	odveta	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
</s>
<s>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
„	„	k?
<g/>
C	C	kA
<g/>
“	“	k?
</s>
<s>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
Femenino	Femenin	k2eAgNnSc4d1
</s>
<s>
Atlético	Atlético	k1gNnSc1
de	de	k?
Kolkata	Kolkat	k1gMnSc2
–	–	k?
indický	indický	k2eAgInSc4d1
fotbalový	fotbalový	k2eAgInSc4d1
klub	klub	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
2014	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
v	v	k7c6
částečném	částečný	k2eAgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
madridského	madridský	k2eAgInSc2d1
Atlética	Atlétic	k1gInSc2
</s>
<s>
Atlético	Atlético	k1gMnSc1
San	San	k1gMnSc1
Luis	Luisa	k1gFnPc2
–	–	k?
mexický	mexický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
madridského	madridský	k2eAgInSc2d1
Atlética	Atlétic	k1gInSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
"	"	kIx"
<g/>
Historia	Historium	k1gNnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
atleticodemadrid	atleticodemadrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Atletico	Atletico	k6eAd1
Madrid	Madrid	k1gInSc1
Colors	Colorsa	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
teamcolorcodes	teamcolorcodes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Wanda	Wanda	k1gFnSc1
Metropolitano	Metropolitana	k1gFnSc5
(	(	kIx(
<g/>
Estadio	Estadia	k1gFnSc5
Metropolitano	Metropolitana	k1gFnSc5
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
stadiumdb	stadiumdb	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
Why	Why	k1gFnSc1
everything	everything	k1gInSc1
you	you	k?
know	know	k?
about	about	k1gInSc1
the	the	k?
Madrid	Madrid	k1gInSc1
derby	derby	k1gNnSc1
might	might	k1gInSc1
be	be	k?
wrong	wrong	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fourfourtwo	fourfourtwo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
7.01	7.01	k4
<g/>
.2015	.2015	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
Un	Un	k1gMnSc1
decreto	decreto	k1gNnSc4
españ	españ	k1gMnSc2
los	los	k1gMnSc1
nombres	nombres	k1gMnSc1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
as	as	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
20.12	20.12	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
European	Europeana	k1gFnPc2
Champions	Championsa	k1gFnPc2
Clubs	Clubs	k1gInSc1
<g/>
'	'	kIx"
Cup	cup	k1gInSc1
-	-	kIx~
Final	Final	k1gInSc1
<g/>
,	,	kIx,
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Plzeň	Plzeň	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Horváth	Horváth	k1gInSc1
a	a	k8xC
spol	spol	k1gInSc1
<g/>
.	.	kIx.
zakončili	zakončit	k5eAaPmAgMnP
podzim	podzim	k1gInSc4
báječně	báječně	k6eAd1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-12-06	2012-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Real	Real	k1gInSc1
zostal	zostat	k5eAaImAgInS,k5eAaPmAgInS
bez	bez	k7c2
trofeje	trofej	k1gFnSc2
<g/>
,	,	kIx,
prehral	prehrat	k5eAaPmAgMnS,k5eAaImAgMnS
aj	aj	kA
vo	vo	k?
finále	finále	k1gNnSc2
pohára	pohár	k1gMnSc2
<g/>
,	,	kIx,
SME	SME	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Atletico	Atletico	k1gNnSc1
zdolalo	zdolat	k5eAaPmAgNnS
v	v	k7c6
odvete	odvet	k1gInSc5
Real	Real	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
získalo	získat	k5eAaPmAgNnS
Superpohár	superpohár	k1gInSc4
<g/>
,	,	kIx,
SME	SME	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
23	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Página	Página	k1gMnSc1
oficial	oficial	k1gMnSc1
del	del	k?
Atlético	Atlético	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
-	-	kIx~
Wanda	Wanda	k1gFnSc1
Metropolitano	Metropolitana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Club	club	k1gInSc1
Atlético	Atlético	k6eAd1
de	de	k?
Madrid	Madrid	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Memes	Memes	k1gMnSc1
<g/>
,	,	kIx,
críticas	críticas	k1gMnSc1
y	y	k?
polémica	polémica	k1gMnSc1
<g/>
:	:	kIx,
por	por	k?
qué	qué	k?
el	ela	k1gFnPc2
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
le	le	k?
puso	pusa	k1gFnSc5
"	"	kIx"
<g/>
Wanda	Wanda	k1gFnSc1
<g/>
"	"	kIx"
a	a	k8xC
su	su	k?
nuevo	nuevo	k6eAd1
estadio	estadio	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infobae	Infobae	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Atlético	Atlético	k6eAd1
de	de	k?
Madrid	Madrid	k1gInSc4
<g/>
:	:	kIx,
El	Ela	k1gFnPc2
Atlético	Atlético	k6eAd1
presenta	presenta	k1gFnSc1
su	su	k?
nuevo	nuevo	k6eAd1
escudo	escudo	k1gNnSc1
-	-	kIx~
Marca	Marca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marca	Marca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
"	"	kIx"
<g/>
European	European	k1gInSc1
Cups	Cupsa	k1gFnPc2
Archive	archiv	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Intercontinental	Intercontinental	k1gMnSc1
Club	club	k1gInSc4
Cup	cup	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Champions	Champions	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
"	"	kIx"
<g/>
Spain	Spain	k1gInSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Super	super	k2eAgInSc1d1
Cup	cup	k1gInSc1
Finals	Finalsa	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Champions	Champions	k1gInSc1
of	of	k?
Centro	Centro	k1gNnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Mundo	Mundo	k1gNnSc1
Deportivo	Deportiva	k1gFnSc5
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
4	#num#	k4
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1941	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hemeroteca	hemeroteca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
abc	abc	k?
<g/>
.	.	kIx.
<g/>
es	es	k1gNnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Teresa	Teresa	k1gFnSc1
Herrera	Herrera	k1gFnSc1
(	(	kIx(
<g/>
La	la	k1gNnSc1
Coruñ	Coruñ	k1gFnPc2
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Coupe	coup	k1gInSc5
Mohamed	Mohamed	k1gMnSc1
V	V	kA
(	(	kIx(
<g/>
Casablanca	Casablanca	k1gFnSc1
-	-	kIx~
Morocco	Morocco	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k6eAd1
Colombino	Colombin	k2eAgNnSc1d1
(	(	kIx(
<g/>
Huelva-Spain	Huelva-Spain	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Ramón	Ramón	k1gMnSc1
de	de	k?
Carranza	Carranza	k1gFnSc1
(	(	kIx(
<g/>
Cádiz-Spain	Cádiz-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Villa	Villa	k1gMnSc1
de	de	k?
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
Madrid-Spain	Madrid-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Mundo	Mundo	k1gNnSc1
Deportivo	Deportiva	k1gFnSc5
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
18	#num#	k4
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hemeroteca	hemeroteca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
abc	abc	k?
<g/>
.	.	kIx.
<g/>
es	es	k1gNnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Semana	Seman	k1gMnSc2
del	del	k?
Sol-Ciudad	Sol-Ciudad	k1gInSc1
de	de	k?
Marbella	Marbella	k1gFnSc1
(	(	kIx(
<g/>
Marbella	Marbella	k1gMnSc1
<g/>
,	,	kIx,
Málaga-Spain	Málaga-Spain	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Naranja	Naranja	k1gMnSc1
(	(	kIx(
<g/>
Valencia-Spain	Valencia-Spain	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Alicante-Costa	Alicante-Costa	k1gMnSc1
Blanca-Ciudad	Blanca-Ciudad	k1gInSc1
de	de	k?
Alicante-Ayuntamiento	Alicante-Ayuntamiento	k1gNnSc1
(	(	kIx(
<g/>
Alicante-Spain	Alicante-Spain	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Zaragoza	Zaragoza	k1gFnSc1
"	"	kIx"
Memorial	Memorial	k1gInSc1
Carlos	Carlos	k1gMnSc1
Lapetra	Lapetra	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Zaragoza-Spain	Zaragoza-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Shanghai	Shanghai	k1gNnSc1
-	-	kIx~
International	International	k1gFnSc1
Tournaments	Tournamentsa	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Memorial	Memorial	k1gMnSc1
Jesús	Jesúsa	k1gFnPc2
Gil	Gil	k1gMnSc1
y	y	k?
Gil	Gil	k1gMnSc1
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
2019	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Liverpool	Liverpool	k1gInSc1
1	#num#	k4
Atletico	Atletico	k6eAd1
Madrid	Madrid	k1gInSc1
1	#num#	k4
(	(	kIx(
<g/>
Atletico	Atletico	k1gNnSc1
win	win	k?
5-4	5-4	k4
on	on	k3xPp3gMnSc1
pens	pensum	k1gNnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Diego	Diega	k1gMnSc5
Simeone	Simeon	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
side	side	k1gFnSc7
a	a	k8xC
step	step	k1gFnSc1
too	too	k?
far	fara	k1gFnPc2
for	forum	k1gNnPc2
Jurgen	Jurgen	k1gInSc4
Klopp	Klopp	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Reds	Redsa	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
telegraph	telegraph	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
2.08	2.08	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Liverpool	Liverpool	k1gInSc1
lose	los	k1gInSc6
to	ten	k3xDgNnSc4
Atletico	Atletico	k1gNnSc4
Madrid	Madrid	k1gInSc4
on	on	k3xPp3gMnSc1
penalties	penalties	k1gMnSc1
in	in	k?
Audi	Audi	k1gNnPc2
Cup	cup	k1gInSc1
final	finat	k5eAaImAgInS,k5eAaBmAgInS,k5eAaPmAgInS
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
2.08	2.08	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
U-19	U-19	k1gMnSc1
League	Leagu	k1gFnSc2
Champions	Champions	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
U-19	U-19	k1gFnSc1
Cup	cup	k1gInSc1
History	Histor	k1gInPc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Ligas	Ligas	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bdfutbol	bdfutbol	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
RESULTADOS	RESULTADOS	kA
HISTÓRICOS	HISTÓRICOS	kA
DEL	DEL	kA
FÚTBOL	FÚTBOL	kA
ESPAÑ	ESPAÑ	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arquero-arba	arquero-arba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
futbolme	futbolit	k5eAaImRp1nP,k5eAaPmRp1nP
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
RESULTADOS	RESULTADOS	kA
HISTÓRICOS	HISTÓRICOS	kA
DE	DE	k?
LAS	laso	k1gNnPc2
LIGAS	LIGAS	kA
ESPAÑ	ESPAÑ	k1gFnSc2
DE	DE	k?
FÚTBOL	FÚTBOL	kA
EN	EN	kA
CATEGORÍA	CATEGORÍA	kA
REGIONAL	REGIONAL	kA
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arquero-arba	arquero-arba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
futbolme	futbolit	k5eAaPmRp1nP,k5eAaImRp1nP
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Latin	latina	k1gFnPc2
Cup	cup	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Primera	primera	k1gFnSc1
División	División	k1gInSc1
–	–	k?
španělská	španělský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ocenění	ocenění	k1gNnSc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Deportivo	Deportiva	k1gFnSc5
Alavés	Alavésa	k1gFnPc2
•	•	k?
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gMnSc1
•	•	k?
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
Cádiz	Cádiz	k1gInSc1
CF	CF	kA
•	•	k?
Celta	celta	k1gFnSc1
de	de	k?
Vigo	Vigo	k1gMnSc1
•	•	k?
CA	ca	kA
Osasuna	Osasuna	k1gFnSc1
•	•	k?
SD	SD	kA
Eibar	Eibar	k1gMnSc1
•	•	k?
Elche	Elche	k1gNnPc2
CF	CF	kA
•	•	k?
Getafe	Getaf	k1gInSc5
CF	CF	kA
•	•	k?
Granada	Granada	k1gFnSc1
CF	CF	kA
•	•	k?
SD	SD	kA
Huesca	Huesc	k2eAgMnSc4d1
•	•	k?
Levante	Levant	k1gMnSc5
UD	UD	kA
•	•	k?
Real	Real	k1gInSc1
Valladolid	Valladolid	k1gInSc1
•	•	k?
Betis	Betis	k1gInSc1
Sevilla	Sevilla	k1gFnSc1
•	•	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
•	•	k?
Real	Real	k1gInSc1
Sociedad	Sociedad	k1gInSc1
•	•	k?
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
Valencia	Valencia	k1gFnSc1
CF	CF	kA
•	•	k?
Villarreal	Villarreal	k1gInSc1
CF	CF	kA
Sezóny	sezóna	k1gFnSc2
</s>
<s>
1929	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
•	•	k?
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
•	•	k?
1945	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
•	•	k?
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CE	CE	kA
Europa	Europa	k1gFnSc1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Unión	Unión	k1gInSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arenas	Arenas	k1gInSc1
Club	club	k1gInSc1
de	de	k?
Getxo	Getxo	k1gMnSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Alcoyano	Alcoyana	k1gFnSc5
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atlético	Atlético	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Tetuán	Tetuán	k1gInSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cultural	Cultural	k1gFnSc1
y	y	k?
Deportiva	Deportiva	k1gFnSc1
Leonesa	Leonesa	k1gFnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Condal	Condal	k1gFnSc2
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Jaén	Jaén	k1gInSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pontevedra	Pontevedra	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
CF	CF	kA
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Burgos	Burgos	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AD	ad	k7c4
Almería	Almería	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CE	CE	kA
Sabadell	Sabadell	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Castellón	Castellón	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Burgos	Burgos	k1gMnSc1
CF	CF	kA
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UE	UE	kA
Lleida	Lleida	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Logroñ	Logroñ	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SD	SD	kA
Compostela	Compostela	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CP	CP	kA
Mérida	Mérida	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CF	CF	kA
Extremadura	Extremadura	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UD	UD	kA
Salamanca	Salamanca	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Oviedo	Oviedo	k1gNnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Albacete	Albace	k1gNnSc2
Balompié	Balompiá	k1gFnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cádiz	Cádiz	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gimnà	Gimnà	k1gFnPc2
de	de	k?
Tarragona	Tarragon	k1gMnSc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Murcia	Murcia	k1gFnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Numancia	Numancium	k1gNnSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Recreativo	Recreativa	k1gFnSc5
de	de	k?
Huelva	Huelvo	k1gNnPc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Tenerife	Tenerif	k1gInSc5
(	(	kIx(
<g/>
2009	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Xerez	Xerez	k1gInSc1
CD	CD	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hércules	Hércules	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Racing	Racing	k1gInSc1
de	de	k?
Santander	Santander	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Zaragoza	Zaragoza	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UD	UD	kA
Almería	Almería	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Córdoba	Córdoba	k1gFnSc1
CF	CF	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Elche	Elche	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sporting	Sporting	k1gInSc1
de	de	k?
Gijón	Gijón	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Deportivo	Deportiva	k1gFnSc5
de	de	k?
La	la	k1gNnSc1
Coruñ	Coruñ	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UD	UD	kA
Las	laso	k1gNnPc2
Palmas	Palmas	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Málaga	Málaga	k1gFnSc1
CF	CF	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Girona	Girona	k1gFnSc1
FC	FC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rayo	Rayo	k6eAd1
Vallecano	Vallecana	k1gFnSc5
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SD	SD	kA
Huesca	Huesca	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Trofeje	trofej	k1gInSc2
a	a	k8xC
ocenění	ocenění	k1gNnSc2
</s>
<s>
Premios	Premios	k1gMnSc1
LFP	LFP	kA
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Pichichi	Pichich	k1gFnSc2
•	•	k?
Premio	Premio	k1gMnSc1
Don	Don	k1gMnSc1
Balón	balón	k1gInSc1
•	•	k?
Premios	Premios	k1gInSc1
Santander	Santander	k1gMnSc1
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Zarra	Zarra	k1gMnSc1
•	•	k?
Trofeo	Trofeo	k6eAd1
Ricardo	Ricardo	k1gNnSc1
Zamora	Zamor	k1gMnSc2
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Miguel	Miguel	k1gMnSc1
Muñ	Muñ	k1gMnSc1
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Guruceta	Guruceto	k1gNnSc2
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Guruceta	Gurucet	k1gMnSc2
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc7
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Španělský	španělský	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
•	•	k?
Španělský	španělský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
Fotbal	fotbal	k1gInSc1
ženy	žena	k1gFnSc2
<g/>
:	:	kIx,
Primera	primera	k1gFnSc1
División	División	k1gInSc1
Femenina	Femenina	k1gFnSc1
de	de	k?
Españ	Españ	k2eAgFnSc1d1
•	•	k?
Copa	Copa	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Reina	Rein	k2eAgNnSc2d1
de	de	k?
Fútbol	Fútbol	k1gInSc1
•	•	k?
Španělská	španělský	k2eAgFnSc1d1
ženská	ženský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1972	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1973	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1975	#num#	k4
FK	FK	kA
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
•	•	k?
1976	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1977	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
1978	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1979	#num#	k4
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gInSc4
FC	FC	kA
•	•	k?
1980	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
1982	#num#	k4
Aston	Aston	k1gNnSc4
Villa	Villo	k1gNnSc2
FC	FC	kA
•	•	k?
1983	#num#	k4
Aberdeen	Aberdeen	k1gInSc1
FC	FC	kA
•	•	k?
1984	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
KV	KV	kA
Mechelen	Mechelna	k1gFnPc2
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
1992	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1993	#num#	k4
AC	AC	kA
Parma	Parma	k1gFnSc1
•	•	k?
1994	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1995	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1998	#num#	k4
Chelsea	Chelseus	k1gMnSc2
FC	FC	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1999	#num#	k4
Lazio	Lazio	k6eAd1
Řím	Řím	k1gInSc1
•	•	k?
2000	#num#	k4
Galatasaray	Galatasaraa	k1gMnSc2
SK	Sk	kA
•	•	k?
2001	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2004	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
2005	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2006	#num#	k4
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Zenit	zenit	k1gInSc1
Petrohrad	Petrohrad	k1gInSc4
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2013	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2021	#num#	k4
•	•	k?
2022	#num#	k4
•	•	k?
2023	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
-	-	kIx~
předchůdce	předchůdce	k1gMnSc2
</s>
<s>
1960	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1961	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1962	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1963	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1964	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1965	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1966	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1967	#num#	k4
Racing	Racing	k1gInSc1
Club	club	k1gInSc4
•	•	k?
1968	#num#	k4
Estudiantes	Estudiantes	k1gInSc1
de	de	k?
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
•	•	k?
1969	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1970	#num#	k4
Feyenoord	Feyenoord	k1gInSc1
•	•	k?
1971	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1972	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1973	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
•	•	k?
1974	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
1975	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1976	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
1977	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
1978	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1979	#num#	k4
Club	club	k1gInSc1
Olimpia	Olimpium	k1gNnSc2
•	•	k?
1980	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1981	#num#	k4
Flamengo	flamengo	k1gNnSc4
•	•	k?
1982	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1983	#num#	k4
Grê	Grê	k1gNnSc4
•	•	k?
1984	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1985	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
CA	ca	kA
River	Rivra	k1gFnPc2
Plate	plat	k1gInSc5
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
FK	FK	kA
Crvena	Crven	k2eAgFnSc1d1
zvezda	zvezda	k1gFnSc1
•	•	k?
1992	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1993	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1994	#num#	k4
CA	ca	kA
Vélez	Vélez	k1gInSc1
Sarsfield	Sarsfield	k1gInSc1
•	•	k?
1995	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc1
•	•	k?
1998	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1999	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2000	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2001	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2004	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
Mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
</s>
<s>
2000	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2001-2004	2001-2004	k4
•	•	k?
2005	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
2006	#num#	k4
Sport	sport	k1gInSc1
Club	club	k1gInSc4
Internacional	Internacional	k1gFnSc2
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2013	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
16160634-9	16160634-9	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2303	#num#	k4
4252	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2003117551	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
240666937	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2003117551	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
