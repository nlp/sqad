<s>
Neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
trhu	trh	k1gInSc2
</s>
<s>
Neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
trhu	trh	k1gInSc2
je	být	k5eAaImIp3nS
termín	termín	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
použil	použít	k5eAaPmAgMnS
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
k	k	k7c3
popisu	popis	k1gInSc3
nezamyšlených	zamyšlený	k2eNgInPc2d1
sociálních	sociální	k2eAgInPc2d1
přínosů	přínos	k1gInPc2
vlastních	vlastní	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
jednotlivce	jednotlivec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
fráze	fráze	k1gFnSc1
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
Smithem	Smith	k1gInSc7
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
rozdělení	rozdělení	k1gNnSc4
příjmů	příjem	k1gInPc2
(	(	kIx(
<g/>
1759	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
produkce	produkce	k1gFnSc1
(	(	kIx(
<g/>
1776	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesná	přesný	k2eAgFnSc1d1
fráze	fráze	k1gFnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
Smithových	Smithův	k2eAgInPc6d1
spisech	spis	k1gInPc6
použita	použít	k5eAaPmNgFnS
třikrát	třikrát	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
zachycuje	zachycovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
myšlenku	myšlenka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
snaha	snaha	k1gFnSc1
jednotlivců	jednotlivec	k1gMnPc2
dosáhnout	dosáhnout	k5eAaPmF
jejich	jejich	k3xOp3gInSc2
vlastního	vlastní	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
často	často	k6eAd1
prospěšná	prospěšný	k2eAgFnSc1d1
společnosti	společnost	k1gFnPc1
více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
kdyby	kdyby	kYmCp3nS
jim	on	k3xPp3gFnPc3
šlo	jít	k5eAaImAgNnS
přímo	přímo	k6eAd1
o	o	k7c4
zájmy	zájem	k1gInPc4
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smith	Smith	k1gMnSc1
mohl	moct	k5eAaImAgMnS
přijít	přijít	k5eAaPmF
s	s	k7c7
dvěma	dva	k4xCgInPc7
významy	význam	k1gInPc7
tohoto	tento	k3xDgNnSc2
spojení	spojení	k1gNnSc2
-	-	kIx~
od	od	k7c2
Richarda	Richard	k1gMnSc2
Cantillona	Cantillon	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
modelu	model	k1gInSc6
izolovaného	izolovaný	k2eAgInSc2d1
statku	statek	k1gInSc2
vyvinul	vyvinout	k5eAaPmAgInS
obě	dva	k4xCgNnPc4
ekonomická	ekonomický	k2eAgNnPc4d1
použití	použití	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Smith	Smith	k1gMnSc1
poprvé	poprvé	k6eAd1
představil	představit	k5eAaPmAgMnS
tento	tento	k3xDgInSc4
koncept	koncept	k1gInSc4
v	v	k7c6
díle	dílo	k1gNnSc6
Teorie	teorie	k1gFnSc2
mravních	mravní	k2eAgInPc2d1
citů	cit	k1gInPc2
<g/>
,	,	kIx,
napsané	napsaný	k2eAgInPc1d1
v	v	k7c6
roce	rok	k1gInSc6
1759	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
rozdělováním	rozdělování	k1gNnSc7
příjmů	příjem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
této	tento	k3xDgFnSc6
práci	práce	k1gFnSc6
není	být	k5eNaImIp3nS
téma	téma	k1gNnSc1
trhů	trh	k1gInPc2
probírané	probíraný	k2eAgFnSc2d1
a	a	k8xC
slovo	slovo	k1gNnSc1
kapitalismus	kapitalismus	k1gInSc1
není	být	k5eNaImIp3nS
nikde	nikde	k6eAd1
použito	použít	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
psal	psát	k5eAaImAgInS
knihu	kniha	k1gFnSc4
Bohatství	bohatství	k1gNnSc2
národů	národ	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1776	#num#	k4
<g/>
,	,	kIx,
studoval	studovat	k5eAaImAgMnS
Smith	Smith	k1gMnSc1
již	již	k9
mnoho	mnoho	k4c4
let	let	k1gInSc4
ekonomické	ekonomický	k2eAgInPc4d1
modely	model	k1gInPc4
francouzských	francouzský	k2eAgMnPc2d1
fyziokratů	fyziokrat	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
této	tento	k3xDgFnSc6
práci	práce	k1gFnSc6
je	být	k5eAaImIp3nS
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
přímo	přímo	k6eAd1
spojována	spojovat	k5eAaImNgFnS
s	s	k7c7
výrobou	výroba	k1gFnSc7
a	a	k8xC
zaměstnáním	zaměstnání	k1gNnSc7
kapitálu	kapitál	k1gInSc2
na	na	k7c4
podporu	podpora	k1gFnSc4
domácích	domácí	k2eAgInPc2d1
průmyslů	průmysl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediná	jediný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
spojení	spojení	k1gNnSc2
neviditelné	viditelný	k2eNgFnSc2d1
ruky	ruka	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
knize	kniha	k1gFnSc6
čtvrté	čtvrtá	k1gFnSc6
<g/>
,	,	kIx,
kapitole	kapitola	k1gFnSc6
druhé	druhý	k4xOgFnSc6
<g/>
,	,	kIx,
„	„	k?
<g/>
O	o	k7c6
omezování	omezování	k1gNnSc6
dovozu	dovoz	k1gInSc2
zboží	zboží	k1gNnSc2
ze	z	k7c2
zahraničních	zahraniční	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
lze	lze	k6eAd1
vyrábět	vyrábět	k5eAaImF
doma	doma	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1
obchodu	obchod	k1gInSc2
a	a	k8xC
tržní	tržní	k2eAgFnSc2d1
výměny	výměna	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
automaticky	automaticky	k6eAd1
přenáší	přenášet	k5eAaImIp3nS
vlastní	vlastní	k2eAgInSc4d1
zájem	zájem	k1gInSc4
na	na	k7c4
společensky	společensky	k6eAd1
žádoucí	žádoucí	k2eAgInPc4d1
cíle	cíl	k1gInPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
k	k	k7c3
laissez-fairové	laissez-fairový	k2eAgFnSc3d1
ekonomické	ekonomický	k2eAgFnSc3d1
filozofii	filozofie	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
neoklasické	neoklasický	k2eAgFnSc6d1
ekonomii	ekonomie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgInSc6
smyslu	smysl	k1gInSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
centrální	centrální	k2eAgFnSc1d1
neshoda	neshoda	k1gFnSc1
mezi	mezi	k7c7
ekonomickými	ekonomický	k2eAgFnPc7d1
ideologiemi	ideologie	k1gFnPc7
považována	považován	k2eAgFnSc1d1
za	za	k7c4
nesouhlas	nesouhlas	k1gInSc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
silná	silný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
alternativních	alternativní	k2eAgInPc6d1
modelech	model	k1gInPc6
síly	síla	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
rodily	rodit	k5eAaImAgFnP
během	během	k7c2
Smithova	Smithův	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
rozsáhlý	rozsáhlý	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
<g/>
,	,	kIx,
finance	finance	k1gFnPc1
a	a	k8xC
reklama	reklama	k1gFnSc1
<g/>
,	,	kIx,
snižovaly	snižovat	k5eAaImAgFnP
jeho	jeho	k3xOp3gFnSc4
účinnost	účinnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
</s>
<s>
Teorie	teorie	k1gFnSc1
mravních	mravní	k2eAgInPc2d1
citů	cit	k1gInPc2
</s>
<s>
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
neviditelné	viditelný	k2eNgFnSc6d1
ruce	ruka	k1gFnSc6
trhu	trh	k1gInSc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
ve	v	k7c6
Smithově	Smithův	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
Teorie	teorie	k1gFnSc2
mravních	mravní	k2eAgInPc2d1
citů	cit	k1gInPc2
(	(	kIx(
<g/>
1759	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
části	část	k1gFnSc6
IV	IV	kA
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kde	kde	k6eAd1
Smith	Smith	k1gMnSc1
popisuje	popisovat	k5eAaImIp3nS
sobeckého	sobecký	k2eAgMnSc4d1
hospodáře	hospodář	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
neviditelnou	viditelný	k2eNgFnSc7d1
rukou	ruka	k1gFnSc7
veden	vést	k5eAaImNgMnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
svou	svůj	k3xOyFgFnSc4
sklizeň	sklizeň	k1gFnSc4
rozdělil	rozdělit	k5eAaPmAgMnS
mezi	mezi	k7c4
své	svůj	k3xOyFgMnPc4
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
:	:	kIx,
</s>
<s>
Pyšný	pyšný	k2eAgMnSc1d1
a	a	k8xC
necitlivý	citlivý	k2eNgMnSc1d1
hospodář	hospodář	k1gMnSc1
hledí	hledět	k5eAaImIp3nS
na	na	k7c4
svá	svůj	k3xOyFgNnPc4
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
pole	pole	k1gNnPc4
a	a	k8xC
bez	bez	k7c2
jediné	jediný	k2eAgFnSc2d1
myšlenky	myšlenka	k1gFnSc2
na	na	k7c4
své	svůj	k3xOyFgMnPc4
bratry	bratr	k1gMnPc4
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
sám	sám	k3xTgMnSc1
užívá	užívat	k5eAaImIp3nS
celé	celý	k2eAgFnPc4d1
sklizně	sklizeň	k1gFnPc4
…	…	k?
Jenže	jenže	k8xC
kapacita	kapacita	k1gFnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
žaludku	žaludek	k1gInSc2
nemůže	moct	k5eNaImIp3nS
se	se	k3xPyFc4
rovnat	rovnat	k5eAaImF
velikosti	velikost	k1gFnPc4
jeho	jeho	k3xOp3gFnPc2
tužeb	tužba	k1gFnPc2
…	…	k?
zbytek	zbytek	k1gInSc1
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
rozdělit	rozdělit	k5eAaPmF
mezi	mezi	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
tím	ten	k3xDgInSc7
nejlaskavějším	laskavý	k2eAgInSc7d3
způsobem	způsob	k1gInSc7
připravují	připravovat	k5eAaImIp3nP
to	ten	k3xDgNnSc4
málo	málo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
využije	využít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
připravují	připravovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gInSc1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
ono	onen	k3xDgNnSc4
málo	málo	k6eAd1
bude	být	k5eAaImBp3nS
konzumovat	konzumovat	k5eAaBmF
<g/>
,	,	kIx,
mezi	mezi	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
mu	on	k3xPp3gMnSc3
poskytují	poskytovat	k5eAaImIp3nP
nejrůznější	různý	k2eAgFnPc1d3
cetky	cetka	k1gFnPc1
a	a	k8xC
ozdoby	ozdoba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
zahrnuty	zahrnout	k5eAaPmNgInP
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
velikosti	velikost	k1gFnSc2
<g/>
;	;	kIx,
všichni	všechen	k3xTgMnPc1
tito	tento	k3xDgMnPc1
tedy	tedy	k9
čerpají	čerpat	k5eAaImIp3nP
z	z	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
luxusu	luxus	k1gInSc2
a	a	k8xC
rozmaru	rozmar	k1gInSc2
svůj	svůj	k3xOyFgInSc4
podíl	podíl	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
potřebují	potřebovat	k5eAaImIp3nP
k	k	k7c3
životu	život	k1gInSc3
a	a	k8xC
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
by	by	kYmCp3nS
od	od	k7c2
něj	on	k3xPp3gMnSc2
marně	marně	k6eAd1
očekávali	očekávat	k5eAaImAgMnP
z	z	k7c2
takových	takový	k3xDgFnPc2
pohnutek	pohnutka	k1gFnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
lidskost	lidskost	k1gFnSc1
či	či	k8xC
spravedlnost	spravedlnost	k1gFnSc1
…	…	k?
Bohatí	bohatit	k5eAaImIp3nS
…	…	k?
jsou	být	k5eAaImIp3nP
vedeni	vést	k5eAaImNgMnP
neviditelnou	viditelný	k2eNgFnSc7d1
rukou	ruka	k1gFnSc7
trhu	trh	k1gInSc2
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
svůj	svůj	k3xOyFgInSc4
majetek	majetek	k1gInSc4
rozložili	rozložit	k5eAaPmAgMnP
mezi	mezi	k7c4
potřebné	potřebné	k1gNnSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
tomu	ten	k3xDgMnSc3
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
země	země	k1gFnSc1
byla	být	k5eAaImAgFnS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
stejné	stejný	k2eAgFnPc4d1
části	část	k1gFnPc4
mezi	mezi	k7c4
své	svůj	k3xOyFgMnPc4
obyvatele	obyvatel	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
bez	bez	k7c2
toho	ten	k3xDgInSc2
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
to	ten	k3xDgNnSc4
zamýšleli	zamýšlet	k5eAaImAgMnP
či	či	k8xC
o	o	k7c6
tom	ten	k3xDgNnSc6
věděli	vědět	k5eAaImAgMnP
<g/>
,	,	kIx,
prospívají	prospívat	k5eAaImIp3nP
zájmu	zájem	k1gInSc3
celé	celý	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
…	…	k?
I	i	k8xC
ti	ten	k3xDgMnPc1
nejposlednější	poslední	k2eAgMnPc1d3
si	se	k3xPyFc3
užijí	užít	k5eAaPmIp3nP
svou	svůj	k3xOyFgFnSc4
část	část	k1gFnSc4
ze	z	k7c2
všeho	všecek	k3xTgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
vyprodukováno	vyprodukovat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
smyslu	smysl	k1gInSc6
opravdového	opravdový	k2eAgNnSc2d1
štěstí	štěstí	k1gNnSc2
nejsou	být	k5eNaImIp3nP
o	o	k7c4
nic	nic	k3yNnSc4
podřadnější	podřadný	k2eAgNnSc1d2
než	než	k8xS
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
jeví	jevit	k5eAaImIp3nP
být	být	k5eAaImF
postaveni	postavit	k5eAaPmNgMnP
tak	tak	k9
vysoko	vysoko	k6eAd1
nad	nad	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lehkosti	lehkost	k1gFnSc6
těla	tělo	k1gNnSc2
a	a	k8xC
klidu	klid	k1gInSc2
duše	duše	k1gFnSc2
…	…	k?
žebrák	žebrák	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
vyhřívá	vyhřívat	k5eAaImIp3nS
na	na	k7c6
slunci	slunce	k1gNnSc6
na	na	k7c6
okraji	okraj	k1gInSc6
silnice	silnice	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
bezpečí	bezpečí	k1gNnSc1
<g/>
,	,	kIx,
o	o	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
králové	králová	k1gFnSc6
vedou	vést	k5eAaImIp3nP
války	válka	k1gFnPc1
</s>
<s>
V	v	k7c6
jiné	jiný	k2eAgFnSc6d1
části	část	k1gFnSc6
Teorie	teorie	k1gFnSc2
mravních	mravní	k2eAgInPc2d1
citů	cit	k1gInPc2
<g/>
,	,	kIx,
Smith	Smith	k1gMnSc1
popisuje	popisovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
muži	muž	k1gMnPc1
touží	toužit	k5eAaImIp3nP
být	být	k5eAaImF
respektováni	respektovat	k5eAaImNgMnP
členy	člen	k1gMnPc7
komunity	komunita	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
žijí	žít	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
touhu	touha	k1gFnSc4
cítit	cítit	k5eAaImF
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
úctyhodné	úctyhodný	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
zmínky	zmínka	k1gFnPc1
o	o	k7c6
neviditelné	viditelný	k2eNgFnSc6d1
ruce	ruka	k1gFnSc6
ve	v	k7c6
Smithově	Smithův	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
</s>
<s>
Smithova	Smithův	k2eAgFnSc1d1
návštěva	návštěva	k1gFnSc1
Francie	Francie	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
známost	známost	k1gFnSc1
s	s	k7c7
francouzskými	francouzský	k2eAgNnPc7d1
Économistes	Économistes	k1gInSc1
(	(	kIx(
<g/>
známí	známit	k5eAaImIp3nS
také	také	k9
jako	jako	k8xS,k8xC
fyziokraté	fyziokrat	k1gMnPc1
<g/>
)	)	kIx)
změnila	změnit	k5eAaPmAgFnS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
pohled	pohled	k1gInSc4
z	z	k7c2
mikroekonomické	mikroekonomický	k2eAgFnSc2d1
optimalizace	optimalizace	k1gFnSc2
na	na	k7c4
makroekonomický	makroekonomický	k2eAgInSc4d1
růst	růst	k1gInSc4
jako	jako	k8xS,k8xC
konec	konec	k1gInSc4
Politické	politický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takže	takže	k8xS
hospodářova	hospodářův	k2eAgFnSc1d1
nenasytnost	nenasytnost	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
zmiňuje	zmiňovat	k5eAaImIp3nS
v	v	k7c6
Teorii	teorie	k1gFnSc6
mravních	mravní	k2eAgInPc2d1
citů	cit	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
odsouzena	odsouzet	k5eAaImNgFnS,k5eAaPmNgFnS
v	v	k7c6
Bohatství	bohatství	k1gNnSc6
národů	národ	k1gInPc2
jako	jako	k8xC,k8xS
neproduktivní	produktivní	k2eNgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gInSc7
shoduje	shodovat	k5eAaImIp3nS
i	i	k9
Amasa	Amasa	k1gFnSc1
Walker	Walker	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
1885	#num#	k4
až	až	k9
1892	#num#	k4
<g/>
)	)	kIx)
Americké	americký	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Domácí	domácí	k2eAgMnSc1d1
služebník	služebník	k1gMnSc1
..	..	k?
není	být	k5eNaImIp3nS
zaměstnán	zaměstnat	k5eAaPmNgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zvýšil	zvýšit	k5eAaPmAgInS
výdělek	výdělek	k1gInSc1
svého	svůj	k3xOyFgMnSc4
pána	pán	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příjem	příjem	k1gInSc4
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
pána	pán	k1gMnSc2
nemá	mít	k5eNaImIp3nS
nic	nic	k3yNnSc1
společného	společný	k2eAgNnSc2d1
s	s	k7c7
žádnou	žádný	k3yNgFnSc7
částí	část	k1gFnSc7
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
zaměstnání	zaměstnání	k1gNnSc2
<g/>
;	;	kIx,
naopak	naopak	k6eAd1
<g/>
,	,	kIx,
napřed	napřed	k6eAd1
přichází	přicházet	k5eAaImIp3nS
výdělek	výdělek	k1gInSc1
..	..	k?
a	a	k8xC
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
výši	výše	k1gFnSc6,k1gFnSc6wB
je	být	k5eAaImIp3nS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
zda	zda	k8xS
bude	být	k5eAaImBp3nS
služebník	služebník	k1gMnSc1
zaměstnán	zaměstnat	k5eAaPmNgMnS
či	či	k8xC
nikoli	nikoli	k9
<g/>
.	.	kIx.
</s>
<s>
Smithův	Smithův	k2eAgInSc1d1
teoretický	teoretický	k2eAgInSc1d1
obrat	obrat	k1gInSc1
o	o	k7c4
180	#num#	k4
<g/>
°	°	k?
z	z	k7c2
mikroekonomického	mikroekonomický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
na	na	k7c4
makroekonomický	makroekonomický	k2eAgInSc4d1
se	se	k3xPyFc4
neodráží	odrážet	k5eNaImIp3nS
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
Bohatství	bohatství	k1gNnSc4
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
části	část	k1gFnPc1
knihy	kniha	k1gFnSc2
jsou	být	k5eAaImIp3nP
převzaty	převzít	k5eAaPmNgFnP
ze	z	k7c2
Smithových	Smithův	k2eAgFnPc2d1
přednášek	přednáška	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
proběhly	proběhnout	k5eAaPmAgFnP
ještě	ještě	k6eAd1
před	před	k7c7
jeho	jeho	k3xOp3gFnSc7
návštěvou	návštěva	k1gFnSc7
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
díle	díl	k1gInSc6
třeba	třeba	k6eAd1
rozlišovat	rozlišovat	k5eAaImF
mikroekonomického	mikroekonomický	k2eAgMnSc4d1
Adama	Adam	k1gMnSc4
Smithe	Smith	k1gMnSc4
a	a	k8xC
makroekonomického	makroekonomický	k2eAgMnSc4d1
Adama	Adam	k1gMnSc4
Smithe	Smith	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestli	jestli	k8xS
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
neviditelné	viditelný	k2eNgFnSc6d1
ruce	ruka	k1gFnSc6
trhu	trh	k1gInSc2
v	v	k7c6
polovině	polovina	k1gFnSc6
knihy	kniha	k1gFnSc2
mikroekonomickým	mikroekonomický	k2eAgNnPc3d1
či	či	k8xC
makroekonomickým	makroekonomický	k2eAgNnPc3d1
tvrzením	tvrzení	k1gNnPc3
<g/>
,	,	kIx,
odsuzujícím	odsuzující	k2eAgInSc6d1
monopoly	monopol	k1gInPc7
a	a	k8xC
zásahy	zásah	k1gInPc1
vlády	vláda	k1gFnSc2
pomocí	pomocí	k7c2
tarifů	tarif	k1gInPc2
a	a	k8xC
patentů	patent	k1gInPc2
je	být	k5eAaImIp3nS
diskutabilní	diskutabilní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Bohatství	bohatství	k1gNnSc1
národů	národ	k1gInPc2
</s>
<s>
Celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
Pojednání	pojednání	k1gNnSc2
o	o	k7c6
podstatě	podstata	k1gFnSc6
a	a	k8xC
původu	původ	k1gInSc2
bohatství	bohatství	k1gNnSc2
národů	národ	k1gInPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
skotský	skotský	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
a	a	k8xC
filosof	filosof	k1gMnSc1
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
byla	být	k5eAaImAgNnP
vydáno	vydat	k5eAaPmNgNnS
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1776	#num#	k4
v	v	k7c6
sedmi	sedm	k4xCc6
svazcích	svazek	k1gInPc6
<g/>
,	,	kIx,
během	během	k7c2
skotského	skotský	k2eAgNnSc2d1
osvícenství	osvícenství	k1gNnSc2
a	a	k8xC
skotské	skotský	k2eAgFnSc2d1
zemědělské	zemědělský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
Smithova	Smithův	k2eAgInSc2d1
života	život	k1gInSc2
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
ještě	ještě	k6eAd1
pětkrát	pětkrát	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1776	#num#	k4
<g/>
,	,	kIx,
1778	#num#	k4
<g/>
,	,	kIx,
1784	#num#	k4
<g/>
,	,	kIx,
1786	#num#	k4
a	a	k8xC
1789	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
vůbec	vůbec	k9
první	první	k4xOgInSc4
popis	popis	k1gInSc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
vlastně	vlastně	k9
tvoří	tvořit	k5eAaImIp3nS
bohatství	bohatství	k1gNnSc4
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Smithovi	Smithův	k2eAgMnPc1d1
trvalo	trvat	k5eAaImAgNnS
sedmnáct	sedmnáct	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
než	než	k8xS
sesbíral	sesbírat	k5eAaPmAgInS
podklady	podklad	k1gInPc4
pro	pro	k7c4
tuhle	tenhle	k3xDgFnSc4
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
pojednání	pojednání	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
nabízí	nabízet	k5eAaImIp3nS
a	a	k8xC
popisuje	popisovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
aplikovat	aplikovat	k5eAaBmF
reformovanou	reformovaný	k2eAgFnSc4d1
ekonomickou	ekonomický	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
<g/>
,	,	kIx,
místo	místo	k1gNnSc4
merkantilismu	merkantilismus	k1gInSc2
a	a	k8xC
fyziokratizmus	fyziokratizmus	k1gInSc4
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
přestávaly	přestávat	k5eAaImAgFnP
být	být	k5eAaImF
účelnými	účelný	k2eAgInPc7d1
<g/>
,	,	kIx,
hlavně	hlavně	k6eAd1
kvůli	kvůli	k7c3
průmyslové	průmyslový	k2eAgFnSc3d1
revoluci	revoluce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Položil	položit	k5eAaPmAgMnS
tak	tak	k6eAd1
základní	základní	k2eAgInPc4d1
kameny	kámen	k1gInPc4
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohatství	bohatství	k1gNnSc1
národů	národ	k1gInPc2
znamenalo	znamenat	k5eAaImAgNnS
ve	v	k7c6
světě	svět	k1gInSc6
ekonomie	ekonomie	k1gFnSc2
obrovský	obrovský	k2eAgInSc4d1
posun	posun	k1gInSc4
a	a	k8xC
ovlivnilo	ovlivnit	k5eAaPmAgNnS
mnoho	mnoho	k4c1
osobností	osobnost	k1gFnPc2
od	od	k7c2
Marxe	Marx	k1gMnSc2
po	po	k7c4
Alexandra	Alexandr	k1gMnSc4
Hamiltona	Hamilton	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Bohatství	bohatství	k1gNnSc1
národů	národ	k1gInPc2
popisuje	popisovat	k5eAaImIp3nS
neviditelnou	viditelný	k2eNgFnSc4d1
ruku	ruka	k1gFnSc4
trhu	trh	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vzniká	vznikat	k5eAaImIp3nS
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
všichni	všechen	k3xTgMnPc1
zvýší	zvýšit	k5eAaPmIp3nP
své	svůj	k3xOyFgNnSc4
úsilí	úsilí	k1gNnSc4
a	a	k8xC
produktivitu	produktivita	k1gFnSc4
na	na	k7c4
maximum	maximum	k1gNnSc4
<g/>
,	,	kIx,
trh	trh	k1gInSc4
tak	tak	k9
začne	začít	k5eAaPmIp3nS
prosperovat	prosperovat	k5eAaImF
na	na	k7c6
ohromné	ohromný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
tomu	ten	k3xDgInSc3
musel	muset	k5eAaImAgMnS
někdo	někdo	k3yInSc1
pomáhat	pomáhat	k5eAaImF
nebo	nebo	k8xC
někdo	někdo	k3yInSc1
do	do	k7c2
něj	on	k3xPp3gMnSc2
nějak	nějak	k6eAd1
zasahovat	zasahovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
téma	téma	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yQgMnSc7,k3yRgMnSc7
se	se	k3xPyFc4
Smith	Smith	k1gMnSc1
v	v	k7c6
Bohatství	bohatství	k1gNnSc6
národů	národ	k1gInPc2
zabývá	zabývat	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
regulace	regulace	k1gFnSc1
na	na	k7c6
trhu	trh	k1gInSc6
je	být	k5eAaImIp3nS
kontraproduktivní	kontraproduktivní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
by	by	kYmCp3nS
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
měly	mít	k5eAaImAgFnP
zvýšit	zvýšit	k5eAaPmF
vývoz	vývoz	k1gInSc4
a	a	k8xC
snížit	snížit	k5eAaPmF
import	import	k1gInSc4
za	za	k7c7
účelem	účel	k1gInSc7
maximalizovat	maximalizovat	k5eAaBmF
tak	tak	k9
své	svůj	k3xOyFgNnSc4
bohatství	bohatství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smithův	Smithův	k2eAgInSc1d1
radikální	radikální	k2eAgInSc1d1
pohled	pohled	k1gInSc1
byl	být	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
bohatství	bohatství	k1gNnSc1
národů	národ	k1gInPc2
je	být	k5eAaImIp3nS
proud	proud	k1gInSc1
zboží	zboží	k1gNnSc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
jej	on	k3xPp3gMnSc4
tvoří	tvořit	k5eAaImIp3nS
<g/>
,	,	kIx,
nikoli	nikoli	k9
zlato	zlato	k1gNnSc1
a	a	k8xC
stříbro	stříbro	k1gNnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
jak	jak	k6eAd1
bylo	být	k5eAaImAgNnS
bohatství	bohatství	k1gNnSc1
původně	původně	k6eAd1
vnímáno	vnímat	k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
produktivita	produktivita	k1gFnSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
dělbě	dělba	k1gFnSc6
práce	práce	k1gFnSc2
a	a	k8xC
hromadění	hromadění	k1gNnSc2
kapitálu	kapitál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Produktivita	produktivita	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
znásobit	znásobit	k5eAaPmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
výrobu	výroba	k1gFnSc4
produktu	produkt	k1gInSc2
rozdělíme	rozdělit	k5eAaPmIp1nP
mezi	mezi	k7c4
více	hodně	k6eAd2
lidí	člověk	k1gMnPc2
specializovaných	specializovaný	k2eAgFnPc2d1
přesně	přesně	k6eAd1
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
část	část	k1gFnSc4
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smith	Smith	k1gInSc1
zvýšení	zvýšení	k1gNnSc2
produktivity	produktivita	k1gFnSc2
specializací	specializace	k1gFnPc2
vysvětluje	vysvětlovat	k5eAaImIp3nS
na	na	k7c6
příkladu	příklad	k1gInSc6
s	s	k7c7
továrnou	továrna	k1gFnSc7
na	na	k7c4
jehly	jehla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Továrny	továrna	k1gFnPc1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
pak	pak	k6eAd1
mohou	moct	k5eAaImIp3nP
obchodovat	obchodovat	k5eAaImF
a	a	k8xC
propojit	propojit	k5eAaPmF
tak	tak	k6eAd1
výrobní	výrobní	k2eAgInSc4d1
řetězec	řetězec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlost	rychlost	k1gFnSc1
rozšíření	rozšíření	k1gNnSc2
výhod	výhoda	k1gFnPc2
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
široký	široký	k2eAgInSc1d1
a	a	k8xC
efektivní	efektivní	k2eAgInSc1d1
je	být	k5eAaImIp3nS
trh	trh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
zaměstnavatelé	zaměstnavatel	k1gMnPc1
manipulují	manipulovat	k5eAaImIp3nP
trhem	trh	k1gInSc7
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
vlastním	vlastní	k2eAgInSc6d1
zájmu	zájem	k1gInSc6
a	a	k8xC
často	často	k6eAd1
se	se	k3xPyFc4
obracejí	obracet	k5eAaImIp3nP
na	na	k7c4
stát	stát	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jim	on	k3xPp3gMnPc3
pomohl	pomoct	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomuto	tento	k3xDgInSc3
by	by	kYmCp3nS
stát	stát	k1gInSc1
měl	mít	k5eAaImAgInS
vyhnout	vyhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Smith	Smith	k1gMnSc1
dále	daleko	k6eAd2
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
budování	budování	k1gNnSc1
kapitálu	kapitál	k1gInSc2
je	být	k5eAaImIp3nS
důležitá	důležitý	k2eAgFnSc1d1
podmínka	podmínka	k1gFnSc1
pro	pro	k7c4
ekonomický	ekonomický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
ušetřené	ušetřený	k2eAgInPc4d1
peníze	peníz	k1gInPc4
pak	pak	k6eAd1
výrobce	výrobce	k1gMnSc4
může	moct	k5eAaImIp3nS
koupit	koupit	k5eAaPmF
nové	nový	k2eAgNnSc4d1
vybavení	vybavení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
ušetří	ušetřit	k5eAaPmIp3nS
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takže	takže	k8xS
čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
více	hodně	k6eAd2
investujeme	investovat	k5eAaBmIp1nP
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
efektivnější	efektivní	k2eAgFnSc1d2
je	být	k5eAaImIp3nS
naše	náš	k3xOp1gFnSc1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzrůstem	vzrůst	k1gInSc7
kapitálu	kapitál	k1gInSc2
se	se	k3xPyFc4
prospěch	prospěch	k1gInSc1
začne	začít	k5eAaPmIp3nS
rozrůstat	rozrůstat	k5eAaImF
a	a	k8xC
všichni	všechen	k3xTgMnPc1
postupně	postupně	k6eAd1
začnou	začít	k5eAaPmIp3nP
bohatnout	bohatnout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
kapitál	kapitál	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
ztracen	ztratit	k5eAaPmNgInS
chybami	chyba	k1gFnPc7
<g/>
,	,	kIx,
krádeží	krádež	k1gFnSc7
nebo	nebo	k8xC
rozmařilým	rozmařilý	k2eAgNnSc7d1
rozhazováním	rozhazování	k1gNnSc7
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stát	stát	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
povolit	povolit	k5eAaPmF
lidem	člověk	k1gMnPc3
vytvořit	vytvořit	k5eAaPmF
si	se	k3xPyFc3
vlastní	vlastní	k2eAgInSc4d1
kapitál	kapitál	k1gInSc4
s	s	k7c7
vědomím	vědomí	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
sklidí	sklidit	k5eAaPmIp3nS
jeho	jeho	k3xOp3gInPc4
plody	plod	k1gInPc4
a	a	k8xC
měli	mít	k5eAaImAgMnP
by	by	kYmCp3nP
být	být	k5eAaImF
na	na	k7c6
pozoru	pozor	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gInPc1
vlastní	vlastní	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
a	a	k8xC
utrácení	utrácení	k1gNnSc1
bude	být	k5eAaImBp3nS
pouze	pouze	k6eAd1
zužovat	zužovat	k5eAaImF
státní	státní	k2eAgInSc4d1
kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
jedinci	jedinec	k1gMnPc1
můžou	můžou	k?
prosperovat	prosperovat	k5eAaImF
ze	z	k7c2
specializace	specializace	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
můžou	můžou	k?
i	i	k8xC
národy	národ	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
by	by	kYmCp3nS
mezi	mezi	k7c7
sebou	se	k3xPyFc7
měly	mít	k5eAaImAgFnP
spolupracovat	spolupracovat	k5eAaImF
a	a	k8xC
obchodovat	obchodovat	k5eAaImF
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
umí	umět	k5eAaImIp3nS
nejlíp	dobře	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Smithe	Smith	k1gInSc2
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
také	také	k9
hromadění	hromadění	k1gNnSc1
kapitálu	kapitál	k1gInSc2
a	a	k8xC
investice	investice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
více	hodně	k6eAd2
je	být	k5eAaImIp3nS
totiž	totiž	k9
investováno	investovat	k5eAaBmNgNnS
do	do	k7c2
lepších	dobrý	k2eAgInPc2d2
výrobních	výrobní	k2eAgInPc2d1
procesů	proces	k1gInPc2
a	a	k8xC
postupů	postup	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c4
to	ten	k3xDgNnSc4
větším	většit	k5eAaImIp1nS
bohatstvím	bohatství	k1gNnSc7
bude	být	k5eAaImBp3nS
země	země	k1gFnSc1
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
disponovat	disponovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
prosperují	prosperovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
ty	ten	k3xDgInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
rostoucí	rostoucí	k2eAgInSc4d1
kapitál	kapitál	k1gInSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
jej	on	k3xPp3gNnSc4
udržují	udržovat	k5eAaImIp3nP
a	a	k8xC
ochraňují	ochraňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc1d1
část	část	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
nedostatku	nedostatek	k1gInSc6
lidé	člověk	k1gMnPc1
klidně	klidně	k6eAd1
za	za	k7c4
zboží	zboží	k1gNnSc4
zaplatí	zaplatit	k5eAaPmIp3nS
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
profituje	profitovat	k5eAaBmIp3nS
víc	hodně	k6eAd2
dodavatel	dodavatel	k1gMnSc1
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
výrobci	výrobce	k1gMnPc1
více	hodně	k6eAd2
investují	investovat	k5eAaBmIp3nP
do	do	k7c2
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
přebytek	přebytek	k1gInSc4
<g/>
,	,	kIx,
ceny	cena	k1gFnPc4
a	a	k8xC
zisk	zisk	k1gInSc4
jsou	být	k5eAaImIp3nP
nízké	nízký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průmysl	průmysl	k1gInSc1
se	se	k3xPyFc4
takto	takto	k6eAd1
zaměří	zaměřit	k5eAaPmIp3nS
jen	jen	k9
na	na	k7c4
nejnutnější	nutný	k2eAgFnPc4d3
potřeby	potřeba	k1gFnPc4
národa	národ	k1gInSc2
<g/>
,	,	kIx,
bez	bez	k7c2
potřeby	potřeba	k1gFnSc2
centrálního	centrální	k2eAgInSc2d1
zásahu	zásah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
tohle	tenhle	k3xDgNnSc1
se	se	k3xPyFc4
dělo	dít	k5eAaImAgNnS,k5eAaBmAgNnS
automaticky	automaticky	k6eAd1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
trh	trh	k1gInSc4
volný	volný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dotace	dotace	k1gFnPc1
nebo	nebo	k8xC
monopol	monopol	k1gInSc1
vlády	vláda	k1gFnSc2
na	na	k7c4
upřednostněné	upřednostněný	k2eAgMnPc4d1
výrobce	výrobce	k1gMnPc4
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
vzrůst	vzrůst	k1gInSc4
cen	cena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
strádají	strádat	k5eAaImIp3nP
chudí	chudý	k1gMnPc1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
musí	muset	k5eAaImIp3nP
čelit	čelit	k5eAaImF
vysokým	vysoký	k2eAgFnPc3d1
cenám	cena	k1gFnPc3
za	za	k7c2
věci	věc	k1gFnSc2
nutné	nutný	k2eAgFnSc2d1
k	k	k7c3
životu	život	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
motivem	motiv	k1gInSc7
Bohatství	bohatství	k1gNnSc2
národů	národ	k1gInPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
konkurence	konkurence	k1gFnSc1
a	a	k8xC
volný	volný	k2eAgInSc1d1
trh	trh	k1gInSc1
jsou	být	k5eAaImIp3nP
ohroženy	ohrozit	k5eAaPmNgFnP
ze	z	k7c2
strany	strana	k1gFnSc2
monopolů	monopol	k1gInPc2
<g/>
,	,	kIx,
různých	různý	k2eAgInPc2d1
daňových	daňový	k2eAgInPc2d1
ohodnocení	ohodnocení	k1gNnSc6
(	(	kIx(
<g/>
spotřební	spotřební	k2eAgFnSc4d1
daň	daň	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
různých	různý	k2eAgFnPc2d1
úrovní	úroveň	k1gFnPc2
kontrol	kontrola	k1gFnPc2
(	(	kIx(
<g/>
hygiena	hygiena	k1gFnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
a	a	k8xC
dalších	další	k2eAgNnPc2d1
zvýhodnění	zvýhodnění	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
získat	získat	k5eAaPmF
od	od	k7c2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
těchto	tento	k3xDgInPc2
důvodů	důvod	k1gInPc2
si	se	k3xPyFc3
Smith	Smith	k1gInSc1
myslí	myslet	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vláda	vláda	k1gFnSc1
a	a	k8xC
stát	stát	k1gInSc1
v	v	k7c6
tomhle	tenhle	k3xDgInSc6
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
limitováni	limitován	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnPc2
funkcí	funkce	k1gFnPc2
je	být	k5eAaImIp3nS
totiž	totiž	k9
hlavně	hlavně	k9
obrana	obrana	k1gFnSc1
<g/>
,	,	kIx,
udržovat	udržovat	k5eAaImF
pořádek	pořádek	k1gInSc4
<g/>
,	,	kIx,
budovaní	budovaný	k2eAgMnPc1d1
infrastruktury	infrastruktura	k1gFnSc2
a	a	k8xC
vzdělávání	vzdělávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
by	by	kYmCp3nP
udržovat	udržovat	k5eAaImF
ekonomiku	ekonomika	k1gFnSc4
otevřenou	otevřený	k2eAgFnSc4d1
a	a	k8xC
volnou	volnou	k6eAd1
a	a	k8xC
nenarušovat	narušovat	k5eNaImF
ji	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redukce	redukce	k1gFnSc1
mezinárodního	mezinárodní	k2eAgInSc2d1
trhu	trh	k1gInSc2
dělá	dělat	k5eAaImIp3nS
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
chudší	chudý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Smith	Smith	k1gMnSc1
je	být	k5eAaImIp3nS
sice	sice	k8xC
velmi	velmi	k6eAd1
kritický	kritický	k2eAgMnSc1d1
ke	k	k7c3
státu	stát	k1gInSc3
a	a	k8xC
byrokracii	byrokracie	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
rozhodně	rozhodně	k6eAd1
není	být	k5eNaImIp3nS
zastáncem	zastánce	k1gMnSc7
laissez	laissez	k1gMnSc1
faire	fair	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myslí	myslet	k5eAaImIp3nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
tržní	tržní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
popsal	popsat	k5eAaPmAgMnS
může	moct	k5eAaImIp3nS
fungovat	fungovat	k5eAaImF
a	a	k8xC
prosperovat	prosperovat	k5eAaImF
pouze	pouze	k6eAd1
pokud	pokud	k8xS
se	se	k3xPyFc4
dodržují	dodržovat	k5eAaImIp3nP
pravidla	pravidlo	k1gNnPc1
<g/>
,	,	kIx,
majetek	majetek	k1gInSc1
je	být	k5eAaImIp3nS
zajištěn	zajištěn	k2eAgInSc1d1
a	a	k8xC
smlouvy	smlouva	k1gFnPc1
jsou	být	k5eAaImIp3nP
ctěny	ctěn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákony	zákon	k1gInPc1
a	a	k8xC
právo	právo	k1gNnSc1
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
podle	podle	k7c2
něj	on	k3xPp3gInSc2
životně	životně	k6eAd1
důležité	důležitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
obrana	obrana	k1gFnSc1
našeho	náš	k3xOp1gInSc2
majetku	majetek	k1gInSc2
před	před	k7c7
krádeží	krádež	k1gFnSc7
je	být	k5eAaImIp3nS
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
důležité	důležitý	k2eAgNnSc1d1
i	i	k9
vzdělání	vzdělání	k1gNnSc1
a	a	k8xC
veřejné	veřejný	k2eAgFnPc1d1
práce	práce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kde	kde	k6eAd1
se	se	k3xPyFc4
daň	daň	k1gFnSc1
zvedá	zvedat	k5eAaImIp3nS
pro	pro	k7c4
tyhle	tenhle	k3xDgInPc4
účely	účel	k1gInPc4
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
by	by	kYmCp3nS
být	být	k5eAaImF
v	v	k7c6
poměru	poměr	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
lidé	člověk	k1gMnPc1
zvládali	zvládat	k5eAaImAgMnP
platit	platit	k5eAaImF
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
mít	mít	k5eAaImF
minimální	minimální	k2eAgInPc4d1
vedlejší	vedlejší	k2eAgInPc4d1
efekty	efekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stát	stát	k1gInSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
vyhnout	vyhnout	k5eAaPmF
danění	daněný	k2eAgMnPc1d1
kapitálu	kapitál	k1gInSc2
a	a	k8xC
větším	veliký	k2eAgInPc3d2
dluhům	dluh	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomické	ekonomický	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
neviditelné	viditelný	k2eNgFnSc2d1
ruky	ruka	k1gFnSc2
trhu	trh	k1gInSc2
</s>
<s>
Koncepce	koncepce	k1gFnSc1
neviditelné	viditelný	k2eNgFnSc2d1
ruky	ruka	k1gFnSc2
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
vždy	vždy	k6eAd1
interpretována	interpretovat	k5eAaBmNgFnS
nad	nad	k7c4
rámec	rámec	k1gInSc4
původního	původní	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
a	a	k8xC
užití	užití	k1gNnSc2
Adama	Adam	k1gMnSc2
Smithe	Smith	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojem	pojem	k1gInSc1
nebyl	být	k5eNaImAgInS
příliš	příliš	k6eAd1
populární	populární	k2eAgInSc1d1
zejména	zejména	k9
mezi	mezi	k7c4
ekonomy	ekonom	k1gMnPc4
v	v	k7c6
období	období	k1gNnSc6
před	před	k7c7
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
;	;	kIx,
Alfred	Alfred	k1gMnSc1
Marshall	Marshall	k1gMnSc1
tento	tento	k3xDgInSc4
termín	termín	k1gInSc4
nikdy	nikdy	k6eAd1
nepoužil	použít	k5eNaPmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
učebnici	učebnice	k1gFnSc6
Principles	Principles	k1gMnSc1
of	of	k?
Economics	Economics	k1gInSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
neviditelné	viditelný	k2eNgFnSc6d1
ruce	ruka	k1gFnSc6
nezmiňuje	zmiňovat	k5eNaImIp3nS
William	William	k1gInSc1
Stanley	Stanlea	k1gFnSc2
Jevons	Jevonsa	k1gFnPc2
v	v	k7c6
Teorii	teorie	k1gFnSc6
politické	politický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Ekonomii	ekonomie	k1gFnSc6
Paul	Paul	k1gMnSc1
Samuelson	Samuelson	k1gMnSc1
interpretuje	interpretovat	k5eAaBmIp3nS
Neviditelnou	viditelný	k2eNgFnSc4d1
ruku	ruka	k1gFnSc4
následovně	následovně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
si	se	k3xPyFc3
každý	každý	k3xTgMnSc1
spotřebitel	spotřebitel	k1gMnSc1
může	moct	k5eAaImIp3nS
svobodně	svobodně	k6eAd1
vybírat	vybírat	k5eAaImF
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
si	se	k3xPyFc3
koupí	koupit	k5eAaPmIp3nS
a	a	k8xC
producent	producent	k1gMnSc1
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
svobodně	svobodně	k6eAd1
vybírat	vybírat	k5eAaImF
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
bude	být	k5eAaImBp3nS
vyrábět	vyrábět	k5eAaImF
<g/>
,	,	kIx,
trh	trh	k1gInSc1
se	se	k3xPyFc4
ustálí	ustálit	k5eAaPmIp3nS
na	na	k7c4
distribuci	distribuce	k1gFnSc4
produktů	produkt	k1gInPc2
a	a	k8xC
na	na	k7c6
cenách	cena	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
přínosné	přínosný	k2eAgFnPc1d1
všem	všecek	k3xTgMnPc3
jednotlivcům	jednotlivec	k1gMnPc3
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
společnosti	společnost	k1gFnSc2
jako	jako	k8xS,k8xC
celku	celek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
tohoto	tento	k3xDgInSc2
děje	děj	k1gInSc2
je	být	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
sledování	sledování	k1gNnSc4
našich	náš	k3xOp1gInPc2
vlastních	vlastní	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
vede	vést	k5eAaImIp3nS
k	k	k7c3
chování	chování	k1gNnSc3
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
prospívá	prospívat	k5eAaImIp3nS
společnosti	společnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
cílem	cíl	k1gInSc7
maximalizování	maximalizování	k1gNnSc2
zisku	zisk	k1gInSc2
jsou	být	k5eAaImIp3nP
zaváděny	zaváděn	k2eAgFnPc1d1
efektivní	efektivní	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
snižovány	snižován	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
za	za	k7c7
účelem	účel	k1gInSc7
maximalizování	maximalizování	k1gNnSc1
výnosu	výnos	k1gInSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
potlačení	potlačení	k1gNnSc2
konkurence	konkurence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Investoři	investor	k1gMnPc1
investují	investovat	k5eAaBmIp3nP
do	do	k7c2
nejvýnosnějších	výnosný	k2eAgNnPc2d3
průmyslových	průmyslový	k2eAgNnPc2d1
odvětví	odvětví	k1gNnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
stahují	stahovat	k5eAaImIp3nP
kapitál	kapitál	k1gInSc4
z	z	k7c2
méně	málo	k6eAd2
výkonného	výkonný	k2eAgInSc2d1
a	a	k8xC
výdělečného	výdělečný	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tyto	tento	k3xDgInPc4
děje	děj	k1gInPc4
probíhají	probíhat	k5eAaImIp3nP
dynamicky	dynamicky	k6eAd1
a	a	k8xC
automaticky	automaticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
časů	čas	k1gInPc2
Smithe	Smithe	k1gNnSc2
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
koncept	koncept	k1gInSc1
stále	stále	k6eAd1
hlouběji	hluboko	k6eAd2
začleňován	začleňovat	k5eAaImNgMnS
do	do	k7c2
ekonomické	ekonomický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
León	León	k1gInSc1
Walras	Walras	k1gInSc4
vytvořil	vytvořit	k5eAaPmAgInS
model	model	k1gInSc1
všeobecné	všeobecný	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
rovnováhy	rovnováha	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
vyvozuje	vyvozovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
sledování	sledování	k1gNnSc1
osobních	osobní	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
v	v	k7c6
soutěživém	soutěživý	k2eAgNnSc6d1
tržním	tržní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
vytváří	vytvářit	k5eAaPmIp3nP,k5eAaImIp3nP
podmínky	podmínka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
přináší	přinášet	k5eAaImIp3nP
společnosti	společnost	k1gFnPc4
největší	veliký	k2eAgInSc4d3
užitek	užitek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
interpretuje	interpretovat	k5eAaBmIp3nS
sociální	sociální	k2eAgNnSc1d1
optimum	optimum	k1gNnSc1
Vilfredo	Vilfredo	k1gNnSc4
Pareto	Pareto	k1gNnSc1
za	za	k7c7
pomocí	pomoc	k1gFnSc7
diagramu	diagram	k1gInSc2
edgeworth	edgeworth	k1gInSc1
box	box	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
díle	dílo	k1gNnSc6
Lidské	lidský	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
používá	používat	k5eAaImIp3nS
Ludwig	Ludwig	k1gInSc4
von	von	k1gInSc1
Mises	Mises	k1gInSc1
výraz	výraz	k1gInSc1
“	“	k?
<g/>
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
prozřetelnosti	prozřetelnost	k1gFnSc2
<g/>
”	”	k?
<g/>
,	,	kIx,
odkazuje	odkazovat	k5eAaImIp3nS
tímto	tento	k3xDgMnSc7
na	na	k7c6
období	období	k1gNnSc6
Marxe	Marx	k1gMnSc4
a	a	k8xC
myšlenku	myšlenka	k1gFnSc4
meliorismu	meliorismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mises	Mises	k1gMnSc1
to	ten	k3xDgNnSc4
však	však	k9
nemínil	mínit	k5eNaImAgMnS
jako	jako	k8xC,k8xS
kritiku	kritika	k1gFnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
zastával	zastávat	k5eAaImAgMnS
podobné	podobný	k2eAgInPc4d1
názory	názor	k1gInPc4
jako	jako	k8xS,k8xC
Smith	Smith	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milton	Milton	k1gInSc4
Friedman	Friedman	k1gMnSc1
<g/>
,	,	kIx,
držitel	držitel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
<g/>
,	,	kIx,
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
Smithovu	Smithova	k1gFnSc4
neviditelnou	viditelný	k2eNgFnSc7d1
rukou	ruka	k1gFnSc7
“	“	k?
<g/>
pravděpodobností	pravděpodobnost	k1gFnSc7
kooperace	kooperace	k1gFnSc2
bez	bez	k7c2
použití	použití	k1gNnSc2
nátlaku	nátlak	k1gInSc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
Někteří	některý	k3yIgMnPc1
ekonomové	ekonom	k1gMnPc1
zpochybňují	zpochybňovat	k5eAaImIp3nP
integritu	integrita	k1gFnSc4
užívání	užívání	k1gNnSc2
termínu	termín	k1gInSc2
“	“	k?
<g/>
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
trhu	trh	k1gInSc2
<g/>
”	”	k?
v	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gavin	Gavin	k1gInSc1
Kennedy	Kenneda	k1gMnSc2
<g/>
,	,	kIx,
emeritní	emeritní	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
Univerzity	univerzita	k1gFnSc2
Heriot-Watt	Heriot-Watt	k1gMnSc1
v	v	k7c6
Edinburghu	Edinburgh	k1gInSc6
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gNnSc4
současné	současný	k2eAgNnSc4d1
užívání	užívání	k1gNnSc4
v	v	k7c4
moderní	moderní	k2eAgFnSc4d1
ekonomii	ekonomie	k1gFnSc4
jako	jako	k8xS,k8xC
symbol	symbol	k1gInSc4
volného	volný	k2eAgInSc2d1
kapitalistického	kapitalistický	k2eAgInSc2d1
trhu	trh	k1gInSc2
je	být	k5eAaImIp3nS
neakceptovatelné	akceptovatelný	k2eNgNnSc1d1
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
původním	původní	k2eAgNnSc7d1
neurčitým	určitý	k2eNgNnSc7d1
podáním	podání	k1gNnSc7
Smithe	Smith	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
Kennedyho	Kennedy	k1gMnSc4
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Klein	Klein	k1gMnSc1
považuje	považovat	k5eAaImIp3nS
užívání	užívání	k1gNnSc4
termínu	termín	k1gInSc2
v	v	k7c6
současném	současný	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
za	za	k7c4
soudné	soudný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
i	i	k8xC
kdyby	kdyby	kYmCp3nS
Smith	Smith	k1gMnSc1
nezamýšlel	zamýšlet	k5eNaImAgMnS
užívání	užívání	k1gNnSc3
termínu	termín	k1gInSc2
v	v	k7c6
dnešním	dnešní	k2eAgInSc6d1
významu	význam	k1gInSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
upotřebitelnost	upotřebitelnost	k1gFnSc1
by	by	kYmCp3nS
neměla	mít	k5eNaImAgFnS
být	být	k5eAaImF
označována	označovat	k5eAaImNgFnS
za	za	k7c4
neefektivní	efektivní	k2eNgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
jejich	jejich	k3xOp3gFnSc2
diskuze	diskuze	k1gFnSc2
<g/>
,	,	kIx,
Kennedy	Kenneda	k1gMnSc2
trvá	trvat	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Smithovy	Smithův	k2eAgFnPc1d1
intence	intence	k1gFnPc1
jsou	být	k5eAaImIp3nP
nadmíru	nadmíru	k6eAd1
důležité	důležitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
termín	termín	k1gInSc1
užíván	užívat	k5eAaImNgInS
jako	jako	k8xS,k8xC
symbol	symbol	k1gInSc1
volnosti	volnost	k1gFnSc2
a	a	k8xC
ekonomické	ekonomický	k2eAgFnSc2d1
koordinace	koordinace	k1gFnSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
by	by	kYmCp3nS
být	být	k5eAaImF
konceptem	koncept	k1gInSc7
zcela	zcela	k6eAd1
oddělený	oddělený	k2eAgInSc4d1
od	od	k7c2
Adama	Adam	k1gMnSc2
Smithe	Smith	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Emmy	Emma	k1gFnSc2
Rothschild	Rothschild	k1gMnSc1
byl	být	k5eAaImAgMnS
při	při	k7c6
užití	užití	k1gNnSc6
termínu	termín	k1gInSc2
ironický	ironický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warren	Warrna	k1gFnPc2
Samuels	Samuels	k1gInSc4
vidí	vidět	k5eAaImIp3nS
neviditelnou	viditelný	k2eNgFnSc4d1
ruku	ruka	k1gFnSc4
jako	jako	k8xS,k8xC
prostředek	prostředek	k1gInSc4
propojování	propojování	k1gNnSc2
moderní	moderní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
se	s	k7c7
Smithem	Smith	k1gInSc7
a	a	k8xC
rovněž	rovněž	k9
zajímavým	zajímavý	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
rozvoje	rozvoj	k1gInSc2
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
jako	jako	k8xC,k8xS
metafora	metafora	k1gFnSc1
</s>
<s>
Smith	Smith	k1gMnSc1
používá	používat	k5eAaImIp3nS
metaforu	metafora	k1gFnSc4
v	v	k7c6
kontextu	kontext	k1gInSc6
argumentu	argument	k1gInSc2
proti	proti	k7c3
protekcionismu	protekcionismus	k1gInSc3
a	a	k8xC
řízení	řízení	k1gNnSc4
trhu	trh	k1gInSc2
státem	stát	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
metafora	metafora	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
obecných	obecný	k2eAgInPc2d1
principů	princip	k1gInPc2
Bernarda	Bernard	k1gMnSc2
Mandevilly	Mandevilla	k1gMnSc2
<g/>
,	,	kIx,
Bishopa	Bishop	k1gMnSc2
Butlera	Butler	k1gMnSc2
<g/>
,	,	kIx,
Lorda	lord	k1gMnSc2
Shaftesburyho	Shaftesbury	k1gMnSc2
a	a	k8xC
Francise	Francise	k1gFnSc1
Hutchesona	Hutchesona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
označení	označení	k1gNnSc1
“	“	k?
<g/>
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
<g/>
”	”	k?
vztaženo	vztažen	k2eAgNnSc4d1
na	na	k7c6
jakékoli	jakýkoli	k3yIgFnSc6
jednání	jednání	k1gNnSc4
s	s	k7c7
neplánovanými	plánovaný	k2eNgInPc7d1
<g/>
,	,	kIx,
nezáměrnými	záměrný	k2eNgInPc7d1
důsledky	důsledek	k1gInPc7
-	-	kIx~
zejména	zejména	k9
takovými	takový	k3xDgInPc7
důsledky	důsledek	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
pramení	pramenit	k5eAaImIp3nP
z	z	k7c2
počínání	počínání	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
nebylo	být	k5eNaImAgNnS
organizováno	organizovat	k5eAaBmNgNnS
ústředním	ústřední	k2eAgNnSc7d1
velením	velení	k1gNnSc7
a	a	k8xC
mají	mít	k5eAaImIp3nP
pozorovatelný	pozorovatelný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Bernarda	Bernard	k1gMnSc2
Mandevilly	Mandevilla	k1gMnSc2
osobní	osobní	k2eAgFnPc1d1
neřesti	neřest	k1gFnPc1
prospívají	prospívat	k5eAaImIp3nP
veřejnosti	veřejnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Civilizovaný	civilizovaný	k2eAgInSc1d1
člověk	člověk	k1gMnSc1
však	však	k9
stigmatizoval	stigmatizovat	k5eAaBmAgMnS
své	svůj	k3xOyFgInPc4
zájmy	zájem	k1gInPc4
a	a	k8xC
chtíče	chtíč	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
má	mít	k5eAaImIp3nS
negativní	negativní	k2eAgInSc1d1
důsledek	důsledek	k1gInSc1
na	na	k7c4
obecné	obecný	k2eAgNnSc4d1
blaho	blaho	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Lorda	lord	k1gMnSc2
Shaftesburyho	Shaftesbury	k1gMnSc2
jednání	jednání	k1gNnSc2
za	za	k7c7
účelem	účel	k1gInSc7
vlastního	vlastní	k2eAgInSc2d1
prospěchu	prospěch	k1gInSc2
je	být	k5eAaImIp3nS
společensky	společensky	k6eAd1
prospěšné	prospěšný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něho	on	k3xPp3gInSc2
existuje	existovat	k5eAaImIp3nS
skrytá	skrytý	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
nazývá	nazývat	k5eAaImIp3nS
“	“	k?
<g/>
vůle	vůle	k1gFnSc1
přírody	příroda	k1gFnSc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
síla	síla	k1gFnSc1
udržuje	udržovat	k5eAaImIp3nS
rovnováhu	rovnováha	k1gFnSc4
<g/>
,	,	kIx,
zachovává	zachovávat	k5eAaImIp3nS
kongruenci	kongruence	k1gFnSc4
a	a	k8xC
harmonii	harmonie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
uplatnění	uplatnění	k1gNnSc3
této	tento	k3xDgFnSc2
síly	síla	k1gFnSc2
je	být	k5eAaImIp3nS
zapotřebí	zapotřebí	k6eAd1
snaha	snaha	k1gFnSc1
o	o	k7c4
dosažení	dosažení	k1gNnSc4
vlastních	vlastní	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Navzdory	navzdory	k7c3
obvyklým	obvyklý	k2eAgFnPc3d1
mylným	mylný	k2eAgFnPc3d1
představám	představa	k1gFnPc3
<g/>
,	,	kIx,
Smith	Smith	k1gMnSc1
rozhodně	rozhodně	k6eAd1
netvrdil	tvrdit	k5eNaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
práce	práce	k1gFnPc1
ve	v	k7c4
vlastní	vlastní	k2eAgInSc4d1
prospěch	prospěch	k1gInSc4
nutně	nutně	k6eAd1
přinášela	přinášet	k5eAaImAgFnS
prospěch	prospěch	k1gInSc4
společnosti	společnost	k1gFnSc2
nebo	nebo	k8xC
že	že	k8xS
obecné	obecný	k2eAgNnSc4d1
blaho	blaho	k1gNnSc4
je	být	k5eAaImIp3nS
důsledkem	důsledek	k1gInSc7
sobeckého	sobecký	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
a	a	k8xC
práce	práce	k1gFnSc2
sledující	sledující	k2eAgFnSc2d1
vlastní	vlastní	k2eAgInSc4d1
prospěch	prospěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
podání	podání	k1gNnSc6
znamená	znamenat	k5eAaImIp3nS
pouze	pouze	k6eAd1
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
obvykle	obvykle	k6eAd1
produkují	produkovat	k5eAaImIp3nP
zboží	zboží	k1gNnSc4
<g/>
,	,	kIx,
po	po	k7c6
němž	jenž	k3xRgInSc6
dychtí	dychtit	k5eAaImIp3nP
i	i	k9
jejich	jejich	k3xOp3gMnPc1
sousedé	soused	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tragédie	tragédie	k1gFnSc1
občiny	občina	k1gFnSc2
je	být	k5eAaImIp3nS
příkladem	příklad	k1gInSc7
toho	ten	k3xDgMnSc2
<g/>
,	,	kIx,
že	že	k8xS
sledování	sledování	k1gNnSc1
vlastních	vlastní	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
obvykle	obvykle	k6eAd1
přináší	přinášet	k5eAaImIp3nS
nechtěné	chtěný	k2eNgInPc4d1
důsledky	důsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
chápána	chápat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
pojem	pojem	k1gInSc1
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
Robert	Robert	k1gMnSc1
Nozick	Nozick	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
Anarchie	anarchie	k1gFnSc1
<g/>
,	,	kIx,
stát	stát	k1gInSc1
a	a	k8xC
utopie	utopie	k1gFnSc1
prohlašuje	prohlašovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
totožný	totožný	k2eAgInSc4d1
koncept	koncept	k1gInSc4
existuje	existovat	k5eAaImIp3nS
v	v	k7c6
řadě	řada	k1gFnSc6
jiných	jiný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
akademického	akademický	k2eAgInSc2d1
diskurzu	diskurz	k1gInSc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
pod	pod	k7c7
jinými	jiný	k2eAgInPc7d1
názvy	název	k1gInPc7
-	-	kIx~
např.	např.	kA
darwinistický	darwinistický	k2eAgInSc1d1
přirozený	přirozený	k2eAgInSc1d1
výběr	výběr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Joseph	Joseph	k1gMnSc1
E.	E.	kA
Stiglitz	Stiglitz	k1gMnSc1
</s>
<s>
Držitel	držitel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
<g/>
,	,	kIx,
ekonom	ekonom	k1gMnSc1
Joseph	Joseph	k1gMnSc1
E.	E.	kA
Stiglitz	Stiglitz	k1gMnSc1
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
důvodem	důvod	k1gInSc7
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
občas	občas	k6eAd1
neviditelnou	viditelný	k2eNgFnSc7d1
<g/>
,	,	kIx,
jest	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
tam	tam	k6eAd1
někdy	někdy	k6eAd1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
Stiglitz	Stiglitz	k1gInSc1
své	svůj	k3xOyFgNnSc4
tvrzení	tvrzení	k1gNnSc4
vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
moderní	moderní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
často	často	k6eAd1
citován	citovat	k5eAaBmNgInS
kvůli	kvůli	k7c3
spojení	spojení	k1gNnSc3
neviditelné	viditelný	k2eNgFnSc2d1
ruky	ruka	k1gFnSc2
a	a	k8xC
volných	volný	k2eAgInPc2d1
trhů	trh	k1gInPc2
<g/>
:	:	kIx,
firmy	firma	k1gFnPc1
jsou	být	k5eAaImIp3nP
při	při	k7c6
hledání	hledání	k1gNnSc6
zisku	zisk	k1gInSc2
vedeny	vést	k5eAaImNgFnP
neviditelnou	viditelný	k2eNgFnSc7d1
rukou	ruka	k1gFnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
dělaly	dělat	k5eAaImAgFnP
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
je	být	k5eAaImIp3nS
nejlepší	dobrý	k2eAgNnSc4d3
pro	pro	k7c4
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
svých	svůj	k3xOyFgMnPc2
následovníků	následovník	k1gMnPc2
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
vědom	vědom	k2eAgMnSc1d1
některých	některý	k3yIgNnPc6
omezeních	omezení	k1gNnPc6
volných	volný	k2eAgMnPc2d1
trhů	trh	k1gInPc2
<g/>
,	,	kIx,
výzkum	výzkum	k1gInSc1
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
objasnil	objasnit	k5eAaPmAgMnS
<g/>
,	,	kIx,
proč	proč	k6eAd1
volné	volný	k2eAgInPc4d1
trhy	trh	k1gInPc4
samy	sám	k3xTgInPc1
o	o	k7c6
sobě	sebe	k3xPyFc6
často	často	k6eAd1
nevedou	vést	k5eNaImIp3nP
k	k	k7c3
tomu	ten	k3xDgNnSc3
nejlepšímu	dobrý	k2eAgMnSc3d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
uvedl	uvést	k5eAaPmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
nové	nový	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
Making	Making	k1gInSc4
Globalization	Globalization	k1gInSc4
Work	Worko	k1gNnPc2
<g/>
,	,	kIx,
důvodem	důvod	k1gInSc7
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
často	často	k6eAd1
neviditelnou	viditelný	k2eNgFnSc7d1
<g/>
,	,	kIx,
jest	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
tam	tam	k6eAd1
často	často	k6eAd1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdykoli	kdykoli	k6eAd1
existují	existovat	k5eAaImIp3nP
externality	externalita	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jednání	jednání	k1gNnSc1
jednotlivce	jednotlivec	k1gMnSc2
má	mít	k5eAaImIp3nS
dopady	dopad	k1gInPc7
na	na	k7c4
ostatní	ostatní	k2eAgFnPc4d1
<g/>
,	,	kIx,
za	za	k7c4
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
neplatí	platit	k5eNaImIp3nP
<g/>
,	,	kIx,
nebo	nebo	k8xC
pro	pro	k7c4
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
nejsou	být	k5eNaImIp3nP
kompenzovány	kompenzovat	k5eAaBmNgInP
–	–	k?
trhy	trh	k1gInPc1
nebudou	být	k5eNaImBp3nP
dobře	dobře	k6eAd1
fungovat	fungovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
důležité	důležitý	k2eAgInPc4d1
případy	případ	k1gInPc4
dlouho	dlouho	k6eAd1
vnímají	vnímat	k5eAaImIp3nP
environmentální	environmentální	k2eAgFnPc4d1
externality	externalita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trhy	trh	k1gInPc1
samy	sám	k3xTgInPc1
o	o	k7c6
sobě	sebe	k3xPyFc6
způsobují	způsobovat	k5eAaImIp3nP
nadměrné	nadměrný	k2eAgNnSc4d1
znečištění	znečištění	k1gNnSc4
<g/>
,	,	kIx,
také	také	k9
samy	sám	k3xTgFnPc1
o	o	k7c6
sobě	sebe	k3xPyFc6
produkují	produkovat	k5eAaImIp3nP
málo	málo	k4c4
základních	základní	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Vláda	vláda	k1gFnSc1
byla	být	k5eAaImAgFnS
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
financování	financování	k1gNnSc4
většiny	většina	k1gFnSc2
důležitých	důležitý	k2eAgInPc2d1
vědeckých	vědecký	k2eAgInPc2d1
průlomů	průlom	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
internetu	internet	k1gInSc2
a	a	k8xC
první	první	k4xOgFnSc2
telegrafní	telegrafní	k2eAgFnSc2d1
linky	linka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
mnoha	mnoho	k4c2
pokroků	pokrok	k1gInPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
biotechnologie	biotechnologie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Nedávný	dávný	k2eNgInSc1d1
výzkum	výzkum	k1gInSc1
však	však	k9
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
externality	externalita	k1gFnPc1
jsou	být	k5eAaImIp3nP
všudypřítomné	všudypřítomný	k2eAgFnPc1d1
<g/>
,	,	kIx,
kdykoli	kdykoli	k6eAd1
existují	existovat	k5eAaImIp3nP
nedokonalé	dokonalý	k2eNgFnPc4d1
informace	informace	k1gFnPc4
nebo	nebo	k8xC
nedokonalé	dokonalý	k2eNgInPc4d1
trhy	trh	k1gInPc4
s	s	k7c7
rizikem	riziko	k1gNnSc7
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
hraje	hrát	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
bankovnictví	bankovnictví	k1gNnSc2
a	a	k8xC
regulací	regulace	k1gFnSc7
cenných	cenný	k2eAgInPc2d1
papírů	papír	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
řady	řada	k1gFnPc1
dalších	další	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
:	:	kIx,
některá	některý	k3yIgFnSc1
regulace	regulace	k1gFnSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
trhy	trh	k1gInPc1
fungovaly	fungovat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
je	být	k5eAaImIp3nS
potřebná	potřebný	k2eAgFnSc1d1
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
všichni	všechen	k3xTgMnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
shodli	shodnout	k5eAaBmAgMnP,k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přinejmenším	přinejmenším	k6eAd1
vynucovala	vynucovat	k5eAaImAgFnS
smlouvy	smlouva	k1gFnPc4
a	a	k8xC
majetková	majetkový	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečná	skutečný	k2eAgFnSc1d1
diskuse	diskuse	k1gFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
o	o	k7c4
nalezení	nalezení	k1gNnSc4
té	ten	k3xDgFnSc2
správné	správný	k2eAgFnSc2d1
rovnováhy	rovnováha	k1gFnSc2
mezi	mezi	k7c7
trhem	trh	k1gInSc7
a	a	k8xC
vládou	vláda	k1gFnSc7
(	(	kIx(
<g/>
a	a	k8xC
třetím	třetí	k4xOgNnSc7
odvětvím	odvětví	k1gNnSc7
–	–	k?
neziskovými	ziskový	k2eNgFnPc7d1
vládními	vládní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Oba	dva	k4xCgMnPc1
jsou	být	k5eAaImIp3nP
zapotřebí	zapotřebí	k6eAd1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
navzájem	navzájem	k6eAd1
doplňovat	doplňovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
rovnováha	rovnováha	k1gFnSc1
se	se	k3xPyFc4
čas	čas	k1gInSc4
od	od	k7c2
času	čas	k1gInSc2
a	a	k8xC
místem	místo	k1gNnSc7
od	od	k7c2
místa	místo	k1gNnSc2
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Předcházející	předcházející	k2eAgNnSc1d1
tvrzení	tvrzení	k1gNnSc1
je	být	k5eAaImIp3nS
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
Stiglitzovi	Stiglitz	k1gMnSc6
práci	práce	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
-	-	kIx~
Externity	Externita	k1gFnSc2
in	in	k?
Economies	Economies	k1gMnSc1
with	with	k1gMnSc1
Imperfect	Imperfect	k2eAgInSc4d1
Information	Information	k1gInSc4
and	and	k?
Incomplete	Incomple	k1gNnSc2
Markets	Marketsa	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
popisuje	popisovat	k5eAaImIp3nS
obecnou	obecný	k2eAgFnSc4d1
metodiku	metodika	k1gFnSc4
řešení	řešení	k1gNnSc2
externalit	externalita	k1gFnPc2
a	a	k8xC
výpočty	výpočet	k1gInPc4
optimálních	optimální	k2eAgFnPc2d1
daní	daň	k1gFnPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
obecnou	obecný	k2eAgFnSc7d1
rovnováhou	rovnováha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
práci	práce	k1gFnSc6
zvažuje	zvažovat	k5eAaImIp3nS
model	model	k1gInSc1
s	s	k7c7
domácnostmi	domácnost	k1gFnPc7
<g/>
,	,	kIx,
firmami	firma	k1gFnPc7
a	a	k8xC
vládou	vláda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Domácnosti	domácnost	k1gFnPc1
maximalizují	maximalizovat	k5eAaBmIp3nP
užitečnou	užitečný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
</s>
<s>
u	u	k7c2
</s>
<s>
h	h	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
h	h	k?
</s>
<s>
,	,	kIx,
</s>
<s>
z	z	k7c2
</s>
<s>
h	h	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
u	u	k7c2
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
z	z	k7c2
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
x	x	k?
</s>
<s>
h	h	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
spotřební	spotřební	k2eAgNnSc1d1
vektor	vektor	k1gInSc4
a	a	k8xC
</s>
<s>
z	z	k7c2
</s>
<s>
h	h	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
z	z	k7c2
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}}	}}	k?
</s>
<s>
jsou	být	k5eAaImIp3nP
další	další	k2eAgFnPc1d1
proměnné	proměnná	k1gFnPc1
ovlivňující	ovlivňující	k2eAgFnSc4d1
užitečnost	užitečnost	k1gFnSc4
domácnosti	domácnost	k1gFnSc2
př	př	kA
<g/>
.	.	kIx.
znečištění	znečištění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpočtové	rozpočtový	k2eAgNnSc1d1
omezení	omezení	k1gNnSc1
je	být	k5eAaImIp3nS
dáno	dát	k5eAaPmNgNnS
</s>
<s>
x	x	k?
</s>
<s>
1	#num#	k4
</s>
<s>
h	h	k?
</s>
<s>
+	+	kIx~
</s>
<s>
q	q	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
x	x	k?
</s>
<s>
¯	¯	k?
</s>
<s>
h	h	k?
</s>
<s>
≤	≤	k?
</s>
<s>
I	i	k9
</s>
<s>
h	h	k?
</s>
<s>
+	+	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
a	a	k8xC
</s>
<s>
h	h	k?
</s>
<s>
f	f	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
π	π	k?
</s>
<s>
f	f	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x_	x_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
q	q	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
bar	bar	k1gInSc1
{	{	kIx(
<g/>
x	x	k?
<g/>
}}	}}	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
leq	leq	k?
I	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
a	a	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
hf	hf	k?
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
\	\	kIx~
<g/>
pi	pi	k0
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
q	q	k?
je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
ahf	ahf	k?
částečné	částečný	k2eAgNnSc1d1
držení	držení	k1gNnSc1
domácností	domácnost	k1gFnPc2
h	h	k?
ve	v	k7c6
firmě	firma	k1gFnSc6
f	f	k?
<g/>
,	,	kIx,
π	π	k1gInSc1
je	být	k5eAaImIp3nS
profit	profit	k1gInSc1
firmy	firma	k1gFnSc2
f	f	k?
<g/>
,	,	kIx,
Ih	Ih	k1gFnSc7
paušální	paušální	k2eAgInSc1d1
vládní	vládní	k2eAgInSc1d1
převod	převod	k1gInSc1
do	do	k7c2
domácnosti	domácnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spotřební	spotřební	k2eAgInSc1d1
vektor	vektor	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rozdělen	rozdělit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
</s>
<s>
x	x	k?
</s>
<s>
h	h	k?
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
1	#num#	k4
</s>
<s>
h	h	k?
</s>
<s>
,	,	kIx,
</s>
<s>
x	x	k?
</s>
<s>
¯	¯	k?
</s>
<s>
h	h	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
(	(	kIx(
<g/>
x_	x_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
overline	overlin	k1gInSc5
{	{	kIx(
<g/>
x	x	k?
<g/>
}}	}}	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
Firma	firma	k1gFnSc1
maximalizuje	maximalizovat	k5eAaBmIp3nS
profit	profit	k1gInSc4
</s>
<s>
π	π	k?
</s>
<s>
f	f	k?
</s>
<s>
=	=	kIx~
</s>
<s>
y	y	k?
</s>
<s>
1	#num#	k4
</s>
<s>
f	f	k?
</s>
<s>
+	+	kIx~
</s>
<s>
p	p	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
y	y	k?
</s>
<s>
¯	¯	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
y_	y_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
p	p	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
overline	overlin	k1gInSc5
{	{	kIx(
<g/>
y	y	k?
<g/>
}}	}}	k?
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
yf	yf	k?
je	být	k5eAaImIp3nS
vektor	vektor	k1gInSc4
produkce	produkce	k1gFnSc2
a	a	k8xC
p	p	k?
je	být	k5eAaImIp3nS
vektor	vektor	k1gInSc1
výrobních	výrobní	k2eAgFnPc2d1
cen	cena	k1gFnPc2
<g/>
,	,	kIx,
podléhá	podléhat	k5eAaImIp3nS
</s>
<s>
y	y	k?
</s>
<s>
1	#num#	k4
</s>
<s>
f	f	k?
</s>
<s>
−	−	k?
</s>
<s>
G	G	kA
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
¯	¯	k?
</s>
<s>
f	f	k?
</s>
<s>
,	,	kIx,
</s>
<s>
z	z	k7c2
</s>
<s>
f	f	k?
</s>
<s>
)	)	kIx)
</s>
<s>
≤	≤	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y_	y_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}	}	kIx)
<g/>
-G	-G	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
overline	overlin	k1gInSc5
{	{	kIx(
<g/>
y	y	k?
<g/>
}}	}}	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
z	z	k7c2
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
leq	leq	k?
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
Gf	Gf	k1gFnSc1
je	být	k5eAaImIp3nS
výrobní	výrobní	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
a	a	k8xC
zf	zf	k?
jsou	být	k5eAaImIp3nP
další	další	k2eAgFnPc4d1
proměnné	proměnná	k1gFnPc4
ovlivňující	ovlivňující	k2eAgFnSc4d1
firmu	firma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vektor	vektor	k1gInSc1
produkce	produkce	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rozdělen	rozdělit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
</s>
<s>
y	y	k?
</s>
<s>
f	f	k?
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
1	#num#	k4
</s>
<s>
f	f	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
¯	¯	k?
</s>
<s>
f	f	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
(	(	kIx(
<g/>
y_	y_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
overline	overlin	k1gInSc5
{	{	kIx(
<g/>
y	y	k?
<g/>
}}	}}	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Vláda	vláda	k1gFnSc1
obdrží	obdržet	k5eAaPmIp3nS
čistý	čistý	k2eAgInSc4d1
příjem	příjem	k1gInSc4
</s>
<s>
R	R	kA
</s>
<s>
=	=	kIx~
</s>
<s>
t	t	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
x	x	k?
</s>
<s>
¯	¯	k?
</s>
<s>
−	−	k?
</s>
<s>
∑	∑	k?
</s>
<s>
I	i	k9
</s>
<s>
h	h	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
R	R	kA
<g/>
=	=	kIx~
<g/>
t	t	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
overline	overlin	k1gInSc5
{	{	kIx(
<g/>
x	x	k?
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
I	i	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
h	h	k?
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
t	t	k?
<g/>
=	=	kIx~
<g/>
(	(	kIx(
<g/>
q-p	q-p	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
daň	daň	k1gFnSc4
zboží	zboží	k1gNnSc2
prodávaného	prodávaný	k2eAgInSc2d1
domácnostem	domácnost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
ukázat	ukázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
výsledná	výsledný	k2eAgFnSc1d1
rovnováha	rovnováha	k1gFnSc1
není	být	k5eNaImIp3nS
obecně	obecně	k6eAd1
účinná	účinný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Noam	Noam	k1gMnSc1
Chomsky	Chomsky	k1gMnSc1
</s>
<s>
Noam	Noam	k1gMnSc1
Chomsky	Chomsky	k1gMnSc1
navrhuje	navrhovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Smith	Smith	k1gMnSc1
(	(	kIx(
<g/>
a	a	k8xC
přesněji	přesně	k6eAd2
David	David	k1gMnSc1
Ricardo	Ricardo	k1gNnSc4
<g/>
)	)	kIx)
někdy	někdy	k6eAd1
použili	použít	k5eAaPmAgMnP
toto	tento	k3xDgNnSc4
slovní	slovní	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
s	s	k7c7
odkazem	odkaz	k1gInSc7
na	na	k7c6
„	„	k?
<g/>
home	homat	k5eAaPmIp3nS
bias	bias	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
domácí	domácí	k2eAgFnSc1d1
předpojatost	předpojatost	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pro	pro	k7c4
domácí	domácí	k2eAgFnPc4d1
investice	investice	k1gFnPc4
v	v	k7c6
protikladu	protiklad	k1gInSc6
s	s	k7c7
offshore	offshor	k1gInSc5
outsourcingové	outsourcingový	k2eAgFnPc4d1
produkci	produkce	k1gFnSc4
a	a	k8xC
neoliberalismem	neoliberalismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Zajímavě	zajímavě	k6eAd1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc4
otázky	otázka	k1gFnPc4
předvídali	předvídat	k5eAaImAgMnP
velcí	velký	k2eAgMnPc1d1
zakladatelé	zakladatel	k1gMnPc1
moderní	moderní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
rozpoznal	rozpoznat	k5eAaPmAgMnS
a	a	k8xC
diskutoval	diskutovat	k5eAaImAgMnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
s	s	k7c7
Británií	Británie	k1gFnSc7
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nP
se	se	k3xPyFc4
továrníci	továrník	k1gMnPc1
řídili	řídit	k5eAaImAgMnP
pravidly	pravidlo	k1gNnPc7
zdravé	zdravá	k1gFnSc2
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
nazývána	nazývat	k5eAaImNgFnS
neoliberalismem	neoliberalismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varoval	varovat	k5eAaImAgMnS
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nP
se	se	k3xPyFc4
britští	britský	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
<g/>
,	,	kIx,
obchodníci	obchodník	k1gMnPc1
a	a	k8xC
investoři	investor	k1gMnPc1
obrátili	obrátit	k5eAaPmAgMnP
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohli	moct	k5eAaImAgMnP
by	by	kYmCp3nP
to	ten	k3xDgNnSc4
z	z	k7c2
toho	ten	k3xDgInSc2
těžit	těžit	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
Anglie	Anglie	k1gFnSc1
by	by	kYmCp3nS
tím	ten	k3xDgNnSc7
utrpěla	utrpět	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
měl	mít	k5eAaImAgInS
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
toto	tento	k3xDgNnSc1
nemohlo	moct	k5eNaImAgNnS
nastat	nastat	k5eAaPmF
<g/>
,	,	kIx,
neboť	neboť	k8xC
by	by	kYmCp3nP
se	se	k3xPyFc4
továrníci	továrník	k1gMnPc1
řídili	řídit	k5eAaImAgMnP
domácí	domácí	k2eAgFnSc7d1
předpojatostí	předpojatost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tudíž	tudíž	k8xC
by	by	kYmCp3nS
Anglie	Anglie	k1gFnSc1
byla	být	k5eAaImAgFnS
díky	díky	k7c3
neviditelné	viditelný	k2eNgFnSc3d1
ruce	ruka	k1gFnSc6
ušetřena	ušetřen	k2eAgNnPc1d1
zpustošení	zpustošení	k1gNnPc1
ekonomické	ekonomický	k2eAgFnSc2d1
racionality	racionalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
pasáž	pasáž	k1gFnSc1
se	se	k3xPyFc4
nedá	dát	k5eNaPmIp3nS
v	v	k7c6
díle	díl	k1gInSc6
Bohatství	bohatství	k1gNnSc2
národů	národ	k1gInPc2
přehlédnout	přehlédnout	k5eAaPmF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
totiž	totiž	k9
jediný	jediný	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
spojení	spojení	k1gNnSc2
”	”	k?
<g/>
neviditelné	viditelný	k2eNgFnSc2d1
ruky	ruka	k1gFnSc2
<g/>
“	“	k?
v	v	k7c6
kritice	kritika	k1gFnSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
nazýváme	nazývat	k5eAaImIp1nP
neoliberalismem	neoliberalismus	k1gInSc7
<g/>
.	.	kIx.
<g/>
”	”	k?
</s>
<s>
Stephen	Stephen	k1gInSc1
LeRoy	LeRoa	k1gFnSc2
</s>
<s>
Stephen	Stephen	k1gInSc1
LeRoy	LeRoa	k1gFnSc2
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
na	na	k7c6
Kalifornské	kalifornský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Santa	Sant	k1gMnSc2
Barbaře	Barbara	k1gFnSc6
a	a	k8xC
hostující	hostující	k2eAgMnSc1d1
odborník	odborník	k1gMnSc1
ve	v	k7c6
Federální	federální	k2eAgFnSc6d1
rezervní	rezervní	k2eAgFnSc6d1
bance	banka	k1gFnSc6
v	v	k7c6
San	San	k1gFnSc6
Francisku	Francisek	k1gInSc2
<g/>
,	,	kIx,
přišel	přijít	k5eAaPmAgMnS
s	s	k7c7
kritikou	kritika	k1gFnSc7
neviditelné	viditelný	k2eNgFnSc2d1
ruky	ruka	k1gFnSc2
a	a	k8xC
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
nejdůležitější	důležitý	k2eAgNnSc4d3
tvrzení	tvrzení	k1gNnSc4
v	v	k7c6
ekonomické	ekonomický	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
prve	prve	k6eAd1
stanoveno	stanovit	k5eAaPmNgNnS
Adamem	Adam	k1gMnSc7
Smithem	Smith	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
že	že	k8xS
konkurenční	konkurenční	k2eAgInPc1d1
trhy	trh	k1gInPc1
odvádí	odvádět	k5eAaImIp3nP
dobrou	dobrý	k2eAgFnSc4d1
práci	práce	k1gFnSc4
v	v	k7c6
přidělování	přidělování	k1gNnSc6
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
…	…	k?
<g/>
)	)	kIx)
Finanční	finanční	k2eAgFnSc1d1
krize	krize	k1gFnSc1
podnítila	podnítit	k5eAaPmAgFnS
debatu	debata	k1gFnSc4
o	o	k7c6
správné	správný	k2eAgFnSc6d1
rovnováze	rovnováha	k1gFnSc6
mezi	mezi	k7c7
trhy	trh	k1gInPc7
a	a	k8xC
vládou	vláda	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
přiměla	přimět	k5eAaPmAgFnS
některý	některý	k3yIgInSc4
odborníky	odborník	k1gMnPc4
k	k	k7c3
otázce	otázka	k1gFnSc3
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
Smith	Smith	k1gMnSc1
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgInSc1d1
pro	pro	k7c4
moderní	moderní	k2eAgFnPc4d1
ekonomiky	ekonomika	k1gFnPc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Výpočtový	výpočtový	k2eAgInSc1d1
model	model	k1gInSc1
tohoto	tento	k3xDgInSc2
fenoménu	fenomén	k1gInSc2
</s>
<s>
Tadeusz	Tadeusz	k1gMnSc1
Szuba	Szuba	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
modelu	model	k1gInSc2
pro	pro	k7c4
jev	jev	k1gInSc4
kolektivní	kolektivní	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
<g/>
,	,	kIx,
navrhl	navrhnout	k5eAaPmAgMnS
teorii	teorie	k1gFnSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
metafora	metafora	k1gFnSc1
neviditelné	viditelný	k2eNgFnSc2d1
ruky	ruka	k1gFnSc2
Adama	Adam	k1gMnSc2
Smithe	Smith	k1gMnSc2
je	být	k5eAaImIp3nS
fenoménem	fenomén	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
formalizován	formalizován	k2eAgInSc4d1
<g/>
,	,	kIx,
simulován	simulován	k2eAgInSc4d1
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
používán	používat	k5eAaImNgInS
k	k	k7c3
navrhování	navrhování	k1gNnSc3
zcela	zcela	k6eAd1
nových	nový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
pro	pro	k7c4
analýzu	analýza	k1gFnSc4
a	a	k8xC
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
předvídat	předvídat	k5eAaImF
trhy	trh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Navrhovaná	navrhovaný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
je	být	k5eAaImIp3nS
příznakem	příznak	k1gInSc7
existence	existence	k1gFnSc2
jiné	jiný	k2eAgFnSc2d1
dimenze	dimenze	k1gFnSc2
trhu	trh	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
výpočetní	výpočetní	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trh	trh	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
agenti	agent	k1gMnPc1
si	se	k3xPyFc3
tuto	tento	k3xDgFnSc4
skutečnost	skutečnost	k1gFnSc4
neuvědomují	uvědomovat	k5eNaImIp3nP
<g/>
,	,	kIx,
protože	protože	k8xS
lze	lze	k6eAd1
pozorovat	pozorovat	k5eAaImF
pouze	pouze	k6eAd1
část	část	k1gFnSc1
nebo	nebo	k8xC
výsledek	výsledek	k1gInSc1
tohoto	tento	k3xDgInSc2
příznaku	příznak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
rozměru	rozměr	k1gInSc6
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
povaha	povaha	k1gFnSc1
trhu	trh	k1gInSc2
a	a	k8xC
agentů	agent	k1gMnPc2
kompletní	kompletní	k2eAgInSc1d1
<g/>
,	,	kIx,
programovatelný	programovatelný	k2eAgInSc1d1
počítač	počítač	k1gInSc1
na	na	k7c6
základě	základ	k1gInSc6
vědomostí	vědomost	k1gFnPc2
agentů	agens	k1gInPc2
a	a	k8xC
fyzické	fyzický	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
trhu	trh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
počítač	počítač	k1gInSc1
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
programuje	programovat	k5eAaImIp3nS
a	a	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
existuje	existovat	k5eAaImIp3nS
a	a	k8xC
funguje	fungovat	k5eAaImIp3nS
na	na	k7c6
základě	základ	k1gInSc6
znalostí	znalost	k1gFnPc2
agentů	agens	k1gInPc2
trhu	trh	k1gInSc2
<g/>
,	,	kIx,
výsledky	výsledek	k1gInPc1
výpočtů	výpočet	k1gInPc2
jsou	být	k5eAaImIp3nP
vedeny	vést	k5eAaImNgInP
přes	přes	k7c4
vědomosti	vědomost	k1gFnPc4
těchto	tento	k3xDgInPc2
agentů	agens	k1gInPc2
a	a	k8xC
prezentují	prezentovat	k5eAaBmIp3nP
se	se	k3xPyFc4
jako	jako	k9
chování	chování	k1gNnSc1
trhu	trh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
výpočty	výpočet	k1gInPc4
jsou	být	k5eAaImIp3nP
chaotické	chaotický	k2eAgInPc4d1
<g/>
,	,	kIx,
distribuované	distribuovaný	k2eAgInPc4d1
<g/>
,	,	kIx,
paralelní	paralelní	k2eAgInSc4d1
a	a	k8xC
nekontinuální	kontinuální	k2eNgInSc4d1
s	s	k7c7
prokládáním	prokládání	k1gNnSc7
souvislostí	souvislost	k1gFnPc2
odlišných	odlišný	k2eAgInPc2d1
výpočtů	výpočet	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgInPc1
výpočty	výpočet	k1gInPc1
jsou	být	k5eAaImIp3nP
řízeny	řízen	k2eAgInPc1d1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
abstraktní	abstraktní	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
přiřazenou	přiřazený	k2eAgFnSc7d1
objektům	objekt	k1gInPc3
a	a	k8xC
službám	služba	k1gFnPc3
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
výpočty	výpočet	k1gInPc1
a	a	k8xC
logickými	logický	k2eAgInPc7d1
závěry	závěr	k1gInPc7
</s>
<s>
Pro	pro	k7c4
vznik	vznik	k1gInSc4
této	tento	k3xDgFnSc2
výpočetní	výpočetní	k2eAgFnSc2d1
dimenze	dimenze	k1gFnSc2
je	být	k5eAaImIp3nS
zásadní	zásadní	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
agenti	agent	k1gMnPc1
trhu	trh	k1gInSc2
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
přiřadit	přiřadit	k5eAaPmF
hodnotu	hodnota	k1gFnSc4
objektům	objekt	k1gInPc3
<g/>
,	,	kIx,
službám	služba	k1gFnPc3
a	a	k8xC
činnostem	činnost	k1gFnPc3
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
vědomě	vědomě	k6eAd1
budovat	budovat	k5eAaImF
řetězce	řetězec	k1gInPc4
závěrů	závěr	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
převést	převést	k5eAaPmF
závěry	závěr	k1gInPc4
do	do	k7c2
obchodních	obchodní	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
je	být	k5eAaImIp3nS
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
trhu	trh	k1gInSc2
mnohem	mnohem	k6eAd1
silnější	silný	k2eAgMnSc1d2
a	a	k8xC
univerzálnější	univerzální	k2eAgMnSc1d2
než	než	k8xS
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
a	a	k8xC
současní	současný	k2eAgMnPc1d1
ekonomové	ekonom	k1gMnPc1
vůbec	vůbec	k9
očekávali	očekávat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
trhu	trh	k1gInSc2
provádí	provádět	k5eAaImIp3nS
kontrolu	kontrola	k1gFnSc4
trhu	trh	k1gInSc2
na	na	k7c6
několika	několik	k4yIc6
úrovních	úroveň	k1gFnPc6
<g/>
:	:	kIx,
z	z	k7c2
hlediska	hledisko	k1gNnSc2
výroby	výroba	k1gFnSc2
a	a	k8xC
spotřeby	spotřeba	k1gFnSc2
(	(	kIx(
<g/>
optimalizace	optimalizace	k1gFnSc1
rovnováhy	rovnováha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
z	z	k7c2
hlediska	hledisko	k1gNnSc2
nových	nový	k2eAgInPc2d1
objevů	objev	k1gInPc2
(	(	kIx(
<g/>
technická	technický	k2eAgFnSc1d1
optimalizace	optimalizace	k1gFnSc1
trhu	trh	k1gInSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
z	z	k7c2
hlediska	hledisko	k1gNnSc2
sociálního	sociální	k2eAgNnSc2d1
chování	chování	k1gNnSc2
(	(	kIx(
<g/>
optimalizace	optimalizace	k1gFnSc1
sociálního	sociální	k2eAgInSc2d1
trhu	trh	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
úprava	úprava	k1gFnSc1
a	a	k8xC
objevování	objevování	k1gNnSc1
nových	nový	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
tržního	tržní	k2eAgNnSc2d1
chování	chování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Očekává	očekávat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
objev	objev	k1gInSc1
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
zcela	zcela	k6eAd1
nových	nový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
a	a	k8xC
modelů	model	k1gInPc2
pro	pro	k7c4
analýzu	analýza	k1gFnSc4
trhu	trh	k1gInSc2
a	a	k8xC
předpovědí	předpověď	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
dnešními	dnešní	k2eAgInPc7d1
makroekonomickými	makroekonomický	k2eAgInPc7d1
modely	model	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Citace	citace	k1gFnPc1
</s>
<s>
V	v	k7c6
díle	dílo	k1gNnSc6
Bohatství	bohatství	k1gNnPc2
národů	národ	k1gInPc2
uvádí	uvádět	k5eAaImIp3nS
příklad	příklad	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
ilustruje	ilustrovat	k5eAaBmIp3nS
jednoduchost	jednoduchost	k1gFnSc4
tohoto	tento	k3xDgInSc2
principu	princip	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
laskavost	laskavost	k1gFnSc1
řezníka	řezník	k1gMnSc2
<g/>
,	,	kIx,
sládka	sládek	k1gMnSc2
nebo	nebo	k8xC
pekaře	pekař	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
vděčíme	vděčit	k5eAaImIp1nP
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
oběd	oběd	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jejich	jejich	k3xOp3gInSc1
ohled	ohled	k1gInSc1
na	na	k7c4
jejich	jejich	k3xOp3gInSc4
vlastní	vlastní	k2eAgInSc4d1
zájem	zájem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nespoléháme	spoléhat	k5eNaImIp1nP
se	se	k3xPyFc4
na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
lidskost	lidskost	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
sebelásku	sebeláska	k1gFnSc4
a	a	k8xC
nikdy	nikdy	k6eAd1
jim	on	k3xPp3gMnPc3
nezdůrazňujeme	zdůrazňovat	k5eNaImIp1nP
naše	náš	k3xOp1gFnPc4
potřeby	potřeba	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gFnPc4
výhody	výhoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Citát	citát	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
zmiňuje	zmiňovat	k5eAaImIp3nS
o	o	k7c6
neviditelné	viditelný	k2eNgFnSc6d1
ruce	ruka	k1gFnSc6
trhu	trh	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
jedinec	jedinec	k1gMnSc1
ze	z	k7c2
všech	všecek	k3xTgFnPc2
sil	síla	k1gFnPc2
snaží	snažit	k5eAaImIp3nS
použít	použít	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
kapitál	kapitál	k1gInSc4
na	na	k7c4
podporu	podpora	k1gFnSc4
domácího	domácí	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
výroba	výroba	k1gFnSc1
měla	mít	k5eAaImAgFnS
co	co	k9
největší	veliký	k2eAgFnSc4d3
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
nutně	nutně	k6eAd1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
roční	roční	k2eAgInPc1d1
příjmy	příjem	k1gInPc1
společnosti	společnost	k1gFnSc2
byly	být	k5eAaImAgInP
co	co	k9
nejvyšší	vysoký	k2eAgMnSc1d3
<g/>
.	.	kIx.
</s>
<s>
Ovšemže	ovšemže	k9
většinou	většinou	k6eAd1
není	být	k5eNaImIp3nS
jeho	jeho	k3xOp3gInSc7
úmyslem	úmysl	k1gInSc7
podporovat	podporovat	k5eAaImF
veřejný	veřejný	k2eAgInSc4d1
zájem	zájem	k1gInSc4
a	a	k8xC
ani	ani	k8xC
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
nakolik	nakolik	k6eAd1
jej	on	k3xPp3gMnSc4
podporuje	podporovat	k5eAaImIp3nS
…	…	k?
myslí	mysl	k1gFnPc2
jen	jen	k9
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
zisk	zisk	k1gInSc4
a	a	k8xC
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
v	v	k7c6
mnohém	mnohé	k1gNnSc6
jiném	jiný	k2eAgNnSc6d1
<g/>
,	,	kIx,
jej	on	k3xPp3gMnSc4
vede	vést	k5eAaImIp3nS
neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
podporoval	podporovat	k5eAaImAgInS
cíl	cíl	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
neměl	mít	k5eNaImAgInS
vůbec	vůbec	k9
v	v	k7c6
úmyslu	úmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Invisible	Invisible	k1gMnSc1
Hand	Hand	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
Wealth	Wealth	k1gMnSc1
of	of	k?
Nations	Nations	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
Institute	institut	k1gInSc5
<g/>
:	:	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Alokace	alokace	k1gFnSc1
trhu	trh	k1gInSc2
</s>
<s>
Laissez	Laissez	k1gMnSc1
faire	fair	k1gInSc5
</s>
<s>
Ekonomismus	ekonomismus	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
trhu	trh	k1gInSc2
</s>
