<s>
Neviditelná	viditelný	k2eNgFnSc1d1
ruka	ruka	k1gFnSc1
trhu	trh	k1gInSc2
je	být	k5eAaImIp3nS
termín	termín	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
použil	použít	k5eAaPmAgMnS
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
k	k	k7c3
popisu	popis	k1gInSc3
nezamyšlených	zamyšlený	k2eNgInPc2d1
sociálních	sociální	k2eAgInPc2d1
přínosů	přínos	k1gInPc2
vlastních	vlastní	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
jednotlivce	jednotlivec	k1gMnSc2
<g/>
.	.	kIx.
</s>