<s>
Terezie	Terezie	k1gFnSc1	Terezie
Měchurová	Měchurový	k2eAgFnSc1d1	Měchurová
(	(	kIx(	(
<g/>
1807	[number]	k4	1807
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
ženou	žena	k1gFnSc7	žena
českého	český	k2eAgMnSc2d1	český
historika	historik	k1gMnSc2	historik
a	a	k8xC	a
politika	politik	k1gMnSc2	politik
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
Jana	Jan	k1gMnSc2	Jan
Měchury	Měchury	k?	Měchury
<g/>
,	,	kIx,	,
bohatého	bohatý	k2eAgMnSc2d1	bohatý
advokáta	advokát	k1gMnSc2	advokát
českých	český	k2eAgMnPc2d1	český
šlechticů	šlechtic	k1gMnPc2	šlechtic
a	a	k8xC	a
velkostatkáře	velkostatkář	k1gMnSc2	velkostatkář
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
ženy	žena	k1gFnPc1	žena
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
bratrancem	bratranec	k1gMnSc7	bratranec
Jana	Jan	k1gMnSc2	Jan
Jeníka	Jeník	k1gMnSc2	Jeník
z	z	k7c2	z
Bratřic	Bratřice	k1gFnPc2	Bratřice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Leopold	Leopold	k1gMnSc1	Leopold
byl	být	k5eAaImAgMnS	být
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Palackým	Palacký	k1gMnSc7	Palacký
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterého	který	k3yIgMnSc4	který
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1827	[number]	k4	1827
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
v	v	k7c6	v
kulturním	kulturní	k2eAgInSc6d1	kulturní
salónu	salón	k1gInSc6	salón
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
(	(	kIx(	(
<g/>
po	po	k7c6	po
tchánově	tchánův	k2eAgFnSc6d1	tchánova
smrti	smrt	k1gFnSc6	smrt
vedl	vést	k5eAaImAgMnS	vést
salón	salón	k1gInSc4	salón
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
přinesla	přinést	k5eAaPmAgFnS	přinést
věno	věno	k1gNnSc4	věno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyřešilo	vyřešit	k5eAaPmAgNnS	vyřešit
Palackého	Palackého	k2eAgInPc4d1	Palackého
finanční	finanční	k2eAgInPc4d1	finanční
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
–	–	k?	–
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
protestant	protestant	k1gMnSc1	protestant
–	–	k?	–
musel	muset	k5eAaImAgMnS	muset
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
zavázat	zavázat	k5eAaPmF	zavázat
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
výchově	výchova	k1gFnSc3	výchova
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Terezie	Terezie	k1gFnSc1	Terezie
měla	mít	k5eAaImAgFnS	mít
velmi	velmi	k6eAd1	velmi
křehké	křehký	k2eAgNnSc4d1	křehké
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
vrozenou	vrozený	k2eAgFnSc4d1	vrozená
srdeční	srdeční	k2eAgFnSc4d1	srdeční
nemoc	nemoc	k1gFnSc4	nemoc
a	a	k8xC	a
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
trpěla	trpět	k5eAaImAgFnS	trpět
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
rodinný	rodinný	k2eAgMnSc1d1	rodinný
lékař	lékař	k1gMnSc1	lékař
Měchurů	Měchur	k1gMnPc2	Měchur
Jan	Jan	k1gMnSc1	Jan
Theobald	Theobald	k1gMnSc1	Theobald
Held	Held	k1gMnSc1	Held
řekl	říct	k5eAaPmAgMnS	říct
Palackému	Palacký	k1gMnSc3	Palacký
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
uchází	ucházet	k5eAaImIp3nS	ucházet
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
zcela	zcela	k6eAd1	zcela
zdravá	zdravý	k2eAgFnSc1d1	zdravá
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgInSc1d2	veliký
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
převážně	převážně	k6eAd1	převážně
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
,	,	kIx,	,
poslal	poslat	k5eAaPmAgMnS	poslat
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
na	na	k7c4	na
502	[number]	k4	502
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Terezie	Terezie	k1gFnSc1	Terezie
zemřela	zemřít	k5eAaPmAgFnS	zemřít
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
nemoci	nemoc	k1gFnSc6	nemoc
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
53	[number]	k4	53
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
KOŘALKA	kořalka	k1gMnSc1	kořalka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
–	–	k?	–
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
životopis	životopis	k1gInSc1	životopis
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
661	[number]	k4	661
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
Ecce	Ecce	k1gFnSc1	Ecce
homo	homo	k1gMnSc1	homo
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
125	[number]	k4	125
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
ŘEZNÍČEK	Řezníček	k1gMnSc1	Řezníček
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
Čech	Čech	k1gMnSc1	Čech
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
působení	působení	k1gNnSc1	působení
a	a	k8xC	a
význam	význam	k1gInSc1	význam
Fr.	Fr.	k1gMnSc2	Fr.
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Jos	Jos	k1gFnPc6	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
357	[number]	k4	357
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
l.	l.	k?	l.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
