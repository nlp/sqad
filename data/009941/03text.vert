<p>
<s>
Kdopak	kdopak	k3yQnSc1
to	ten	k3xDgNnSc4
mluví	mluvit	k5eAaImIp3nS
(	(	kIx(
<g/>
v	v	k7c6
americkém	americký	k2eAgInSc6d1	americký
originále	originál	k1gInSc6	originál
<g/>
:	:	kIx,
Look	Look	k1gMnSc1	Look
Who	Who	k1gMnSc1	Who
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Talking	Talking	k1gInSc1	Talking
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1	americká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2
roku	rok	k1gInSc2	rok
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režisérem	režisér	k1gMnSc7	režisér
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS
Amy	Amy	k1gFnSc1	Amy
Heckerling	Heckerling	k1gInSc1	Heckerling
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
ve	v	k7c6
filmu	film	k1gInSc6	film
ztvárnili	ztvárnit	k5eAaPmAgMnP
John	John	k1gMnSc1	John
Travolta	Travolta	k1gMnSc1	Travolta
<g/>
,	,	kIx,
Kirstie	Kirstie	k1gFnSc1	Kirstie
Alley	Allea	k1gFnPc1	Allea
<g/>
,	,	kIx,
George	Georg	k1gFnPc1	Georg
Segal	Segal	k1gInSc1	Segal
<g/>
,	,	kIx,
Olympia	Olympia	k1gFnSc1	Olympia
Dukakis	Dukakis	k1gFnSc1	Dukakis
a	a	k8xC
Twink	Twink	k1gMnSc1	Twink
Caplan	Caplan	k1gMnSc1	Caplan
<g/>
.	.	kIx.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?
Reakce	reakce	k1gFnPc1	reakce
==	==	k?
</s>
</p>
<p>
<s>
aktuální	aktuální	k2eAgInPc1d1	aktuální
k	k	k7c3
2	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2014	#num#	k4
<g/>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS
mezi	mezi	k7c7
diváky	divák	k1gMnPc7	divák
na	na	k7c6
největších	veliký	k2eAgFnPc6d3	veliký
filmových	filmový	k2eAgFnPc6d1	filmová
databázích	databáze	k1gFnPc6	databáze
spíše	spíše	k9
průměrné	průměrný	k2eAgNnSc4d1	průměrné
hodnocení	hodnocení	k1gNnSc4	hodnocení
<g/>
.	.	kIx.
</s>
</p>
<p>
</p>
<p>
<s>
csfd	csfd	k1gInSc1	csfd
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
</p>
<p>
<s>
imdb	imdb	k1gInSc1	imdb
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
</s>
</p>
<p>
<s>
fdb	fdb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?
</s>
</p>
<p>
<s>
Kdopak	kdopak	k3yQnSc1
to	ten	k3xDgNnSc1
mluví	mluvit	k5eAaImIp3nS
v	v	k7c6
Česko-Slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
</s>
</p>
<p>
<s>
Kdopak	kdopak	k3yQnSc1
to	ten	k3xDgNnSc1
mluví	mluvit	k5eAaImIp3nS
v	v	k7c4
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
Kdopak	kdopak	k3yQnSc1
to	ten	k3xDgNnSc1
mluví	mluvit	k5eAaImIp3nS
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
