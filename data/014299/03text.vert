<s>
Markéta	Markéta	k1gFnSc1
Dobiášová	Dobiášová	k1gFnSc1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Dobiášová	Dobiášová	k1gFnSc1
Narození	narozený	k2eAgMnPc5d1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
44	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Třebíč	Třebíč	k1gFnSc1
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
reportérka	reportérka	k1gFnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Novinářská	novinářský	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
tři	tři	k4xCgFnPc1
děti	dítě	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Markéta	Markéta	k1gFnSc1
Dobiášová	Dobiášová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1977	#num#	k4
Třebíč	Třebíč	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
reportérka	reportérka	k1gFnSc1
<g/>
,	,	kIx,
moderátorka	moderátorka	k1gFnSc1
a	a	k8xC
dramaturgyně	dramaturgyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
působila	působit	k5eAaImAgFnS
v	v	k7c6
České	český	k2eAgFnSc6d1
televizi	televize	k1gFnSc6
jako	jako	k8xS,k8xC
investigativní	investigativní	k2eAgFnSc1d1
reportérka	reportérka	k1gFnSc1
pořadu	pořad	k1gInSc2
Reportéři	reportér	k1gMnPc1
ČT	ČT	kA
a	a	k8xC
od	od	k7c2
března	březen	k1gInSc2
do	do	k7c2
června	červen	k1gInSc2
2015	#num#	k4
uváděla	uvádět	k5eAaImAgFnS
pořad	pořad	k1gInSc1
168	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
působí	působit	k5eAaImIp3nS
na	na	k7c6
televizní	televizní	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
CNN	CNN	kA
Prima	prima	k2eAgInSc4d1
News	News	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
na	na	k7c6
starosti	starost	k1gFnSc6
vedení	vedení	k1gNnSc2
investigativního	investigativní	k2eAgInSc2d1
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS
Filozofickou	filozofický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Univerzity	univerzita	k1gFnSc2
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1997	#num#	k4
až	až	k8xS
1999	#num#	k4
se	se	k3xPyFc4
živila	živit	k5eAaImAgNnP
jako	jako	k8xS,k8xC
produkční	produkční	k2eAgMnPc1d1
v	v	k7c6
Divadle	divadlo	k1gNnSc6
Bolka	Bolek	k1gMnSc2
Polívky	Polívka	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
a	a	k8xC
regionální	regionální	k2eAgFnSc1d1
TV	TV	kA
Fatem	fatum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
pak	pak	k6eAd1
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1999	#num#	k4
a	a	k8xC
2001	#num#	k4
pracovala	pracovat	k5eAaImAgFnS
v	v	k7c6
redakci	redakce	k1gFnSc6
zpravodajství	zpravodajství	k1gNnSc2
TV	TV	kA
Nova	novum	k1gNnSc2
a	a	k8xC
v	v	k7c6
České	český	k2eAgFnSc6d1
televizi	televize	k1gFnSc6
(	(	kIx(
<g/>
podílela	podílet	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c6
zpravodajství	zpravodajství	k1gNnSc6
a	a	k8xC
na	na	k7c6
pořadech	pořad	k1gInPc6
Tady	tady	k6eAd1
a	a	k8xC
teď	teď	k6eAd1
a	a	k8xC
Fakta	faktum	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2002	#num#	k4
až	až	k9
2003	#num#	k4
spolupracovala	spolupracovat	k5eAaImAgNnP
s	s	k7c7
Nadací	nadace	k1gFnSc7
Člověk	člověk	k1gMnSc1
v	v	k7c6
tísni	tíseň	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
natáčela	natáčet	k5eAaImAgFnS
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Koreji	Korea	k1gFnSc6
a	a	k8xC
v	v	k7c6
Číně	Čína	k1gFnSc6
dokument	dokument	k1gInSc4
o	o	k7c6
severokorejských	severokorejský	k2eAgMnPc6d1
uprchlících	uprchlík	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
TV	TV	kA
Nova	nova	k1gFnSc1
pak	pak	k6eAd1
spolupracovala	spolupracovat	k5eAaImAgFnS
na	na	k7c6
pořadu	pořad	k1gInSc6
Na	na	k7c4
vlastní	vlastní	k2eAgNnPc4d1
oči	oko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
působí	působit	k5eAaImIp3nS
v	v	k7c6
České	český	k2eAgFnSc6d1
televizi	televize	k1gFnSc6
jako	jako	k8xS,k8xC
reportérka	reportérka	k1gFnSc1
pořadu	pořad	k1gInSc2
Reportéři	reportér	k1gMnPc1
ČT	ČT	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
se	se	k3xPyFc4
rovněž	rovněž	k9
podílela	podílet	k5eAaImAgFnS
na	na	k7c6
tvorbě	tvorba	k1gFnSc6
více	hodně	k6eAd2
než	než	k8xS
10	#num#	k4
dílů	díl	k1gInPc2
pořadu	pořad	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
komnata	komnata	k1gFnSc1
(	(	kIx(
<g/>
připravovala	připravovat	k5eAaImAgFnS
díly	díl	k1gInPc4
o	o	k7c6
těchto	tento	k3xDgFnPc6
osobnostech	osobnost	k1gFnPc6
<g/>
:	:	kIx,
Vendula	Vendula	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
<g/>
,	,	kIx,
Wabi	Wabi	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Daněk	Daněk	k1gMnSc1
<g/>
,	,	kIx,
Naďa	Naďa	k1gFnSc1
Urbánková	Urbánková	k1gFnSc1
<g/>
,	,	kIx,
Dáša	Dáša	k1gFnSc1
Bláhová	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
Simona	Simona	k1gFnSc1
Chytrová	Chytrová	k1gFnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
Kornová	kornový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
Stirská	Stirský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Miroslava	Miroslava	k1gFnSc1
Němcová	Němcová	k1gFnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Žáček	Žáček	k1gMnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Šilerová	Šilerová	k1gFnSc1
<g/>
,	,	kIx,
Barbora	Barbora	k1gFnSc1
Kohoutková	Kohoutková	k1gFnSc1
a	a	k8xC
Ivan	Ivan	k1gMnSc1
Hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
kolegyní	kolegyně	k1gFnSc7
Anetou	Aneta	k1gFnSc7
Snopovou	snopův	k2eAgFnSc7d1
získala	získat	k5eAaPmAgFnS
Novinářskou	novinářský	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
rok	rok	k1gInSc4
2012	#num#	k4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
audiovizuální	audiovizuální	k2eAgFnSc2d1
žurnalistiky	žurnalistika	k1gFnSc2
Nejlepší	dobrý	k2eAgInSc4d3
analyticko-investigativní	analyticko-investigativní	k2eAgInSc4d1
příspěvek	příspěvek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konkrétně	konkrétně	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
reportáž	reportáž	k1gFnSc4
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
televizi	televize	k1gFnSc4
s	s	k7c7
názvem	název	k1gInSc7
Letadla	letadlo	k1gNnSc2
CASA	CASA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
do	do	k7c2
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
také	také	k9
dočasně	dočasně	k6eAd1
moderovala	moderovat	k5eAaBmAgFnS
pořad	pořad	k1gInSc4
168	#num#	k4
hodin	hodina	k1gFnPc2
za	za	k7c4
Noru	Nora	k1gFnSc4
Fridrichovou	Fridrichová	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
odešla	odejít	k5eAaPmAgFnS
na	na	k7c4
mateřskou	mateřský	k2eAgFnSc4d1
dovolenou	dovolená	k1gFnSc4
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
uváděna	uvádět	k5eAaImNgFnS
i	i	k9
jako	jako	k8xS,k8xC
dramaturgyně	dramaturgyně	k1gFnSc1
pořadu	pořad	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
od	od	k7c2
ČT	ČT	kA
přešla	přejít	k5eAaPmAgFnS
k	k	k7c3
tehdy	tehdy	k6eAd1
nově	nově	k6eAd1
spuštěné	spuštěný	k2eAgInPc1d1
CNN	CNN	kA
Prima	prima	k2eAgFnPc2d1
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
opustila	opustit	k5eAaPmAgFnS
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Markéta	Markéta	k1gFnSc1
Dobiášová	Dobiášová	k1gFnSc1
má	mít	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Kauza	kauza	k1gFnSc1
léčby	léčba	k1gFnSc2
ALS	ALS	kA
kmenovými	kmenový	k2eAgFnPc7d1
buňkami	buňka	k1gFnPc7
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
zpracovala	zpracovat	k5eAaPmAgFnS
reportáž	reportáž	k1gFnSc1
pro	pro	k7c4
pořad	pořad	k1gInSc4
Reportéři	reportér	k1gMnPc1
ČT	ČT	kA
o	o	k7c6
klinické	klinický	k2eAgFnSc6d1
studii	studie	k1gFnSc6
týmu	tým	k1gInSc2
profesorky	profesorka	k1gFnSc2
Sykové	Syková	k1gFnSc2
zaměřené	zaměřený	k2eAgFnSc2d1
na	na	k7c4
léčbu	léčba	k1gFnSc4
amyotrofické	amyotrofický	k2eAgFnSc2d1
laterální	laterální	k2eAgFnSc2d1
sklerózy	skleróza	k1gFnSc2
(	(	kIx(
<g/>
ALS	ALS	kA
<g/>
)	)	kIx)
kmenovými	kmenový	k2eAgFnPc7d1
buňkami	buňka	k1gFnPc7
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
několik	několik	k4yIc1
pacientů	pacient	k1gMnPc2
zařazených	zařazený	k2eAgMnPc2d1
do	do	k7c2
studie	studie	k1gFnSc2
vypovědělo	vypovědět	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
museli	muset	k5eAaImAgMnP
za	za	k7c4
podání	podání	k1gNnSc4
kmenových	kmenový	k2eAgFnPc2d1
buněk	buňka	k1gFnPc2
uhradit	uhradit	k5eAaPmF
poplatek	poplatek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
jakékoliv	jakýkoliv	k3yIgFnPc4
platby	platba	k1gFnPc4
za	za	k7c4
účast	účast	k1gFnSc4
v	v	k7c6
klinických	klinický	k2eAgFnPc6d1
studiích	studie	k1gFnPc6
jsou	být	k5eAaImIp3nP
zákonem	zákon	k1gInSc7
zakázané	zakázaný	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
reportáž	reportáž	k1gFnSc4
předseda	předseda	k1gMnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
Jiří	Jiří	k1gMnSc1
Drahoš	Drahoš	k1gMnSc1
odvolal	odvolat	k5eAaPmAgMnS
Sykovou	Syková	k1gFnSc4
z	z	k7c2
postu	post	k1gInSc2
ředitelky	ředitelka	k1gFnSc2
Ústavu	ústav	k1gInSc2
experimentální	experimentální	k2eAgFnSc2d1
medicíny	medicína	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
a	a	k8xC
Syková	Syková	k1gFnSc1
rezignovala	rezignovat	k5eAaBmAgFnS
funkci	funkce	k1gFnSc4
místopředsedkyně	místopředsedkyně	k1gFnSc2
vládní	vládní	k2eAgFnSc2d1
Rady	rada	k1gFnSc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc1
a	a	k8xC
inovace	inovace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nicméně	nicméně	k8xC
Syková	Syková	k1gFnSc1
následně	následně	k6eAd1
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
podá	podat	k5eAaPmIp3nS
žalobu	žaloba	k1gFnSc4
„	„	k?
<g/>
proti	proti	k7c3
institucím	instituce	k1gFnPc3
a	a	k8xC
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
vyvolali	vyvolat	k5eAaPmAgMnP
tuto	tento	k3xDgFnSc4
kauzu	kauza	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Osobnost	osobnost	k1gFnSc1
<g/>
:	:	kIx,
Markéta	Markéta	k1gFnSc1
Dobiášová	Dobiášová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příjmení	příjmení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Markéta	Markéta	k1gFnSc1
Dobiášová	Dobiášová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
superFilm	superFilm	k1gInSc1
<g/>
.	.	kIx.
<g/>
csk	csk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Investigativu	Investigativ	k1gInSc2
CNN	CNN	kA
Prima	prima	k2eAgInSc4d1
News	News	k1gInSc4
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
Dobiášová	Dobiášová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Médiář	Médiář	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Markéta	Markéta	k1gFnSc1
Dobiášová	Dobiášová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Markéta	Markéta	k1gFnSc1
Dobiášová	Dobiášová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSFD	ČSFD	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výsledky	výsledek	k1gInPc1
soutěže	soutěž	k1gFnSc2
NC	NC	kA
2012	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinářská	novinářský	k2eAgFnSc1d1
cena	cena	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Těhotná	těhotný	k2eAgFnSc1d1
Fridrichová	Fridrichová	k1gFnSc1
mizí	mizet	k5eAaImIp3nS
z	z	k7c2
obrazovek	obrazovka	k1gFnPc2
<g/>
:	:	kIx,
Víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
ji	on	k3xPp3gFnSc4
nahradí	nahradit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-03-11	2015-03-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Novým	nový	k2eAgMnSc7d1
šéfem	šéf	k1gMnSc7
české	český	k2eAgFnSc2d1
CNN	CNN	kA
je	být	k5eAaImIp3nS
Pavel	Pavel	k1gMnSc1
Štrunc	Štrunc	k1gMnSc1
<g/>
,	,	kIx,
reportérka	reportérka	k1gFnSc1
Dobiášová	Dobiášová	k1gFnSc1
ohlásila	ohlásit	k5eAaPmAgFnS
odchod	odchod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Info	Info	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
31.8	31.8	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
DOBIÁŠOVÁ	Dobiášová	k1gFnSc1
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchod	obchod	k1gInSc1
s	s	k7c7
nadějí	naděje	k1gFnSc7
II	II	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Syková	Syková	k1gFnSc1
rezignovala	rezignovat	k5eAaBmAgFnS
na	na	k7c4
funkci	funkce	k1gFnSc4
ve	v	k7c6
vládní	vládní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
<g/>
↑	↑	k?
Podám	podat	k5eAaPmIp1nS
žalobu	žaloba	k1gFnSc4
na	na	k7c4
instituce	instituce	k1gFnPc4
a	a	k8xC
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
rozpoutali	rozpoutat	k5eAaPmAgMnP
moji	můj	k3xOp1gFnSc4
kauzu	kauza	k1gFnSc4
<g/>
,	,	kIx,
oznámila	oznámit	k5eAaPmAgFnS
Eva	Eva	k1gFnSc1
Syková	Syková	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2007377378	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5668	#num#	k4
4326	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84691854	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Televize	televize	k1gFnSc1
</s>
