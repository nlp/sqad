<s>
Petra	Petra	k1gFnSc1	Petra
Kvitová	Kvitová	k1gFnSc1	Kvitová
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
Bílovec	Bílovec	k1gInSc1	Bílovec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
profesionálního	profesionální	k2eAgInSc2d1	profesionální
tenisu	tenis	k1gInSc2	tenis
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
devatenáct	devatenáct	k4xCc4	devatenáct
turnajů	turnaj	k1gInPc2	turnaj
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
okruhu	okruh	k1gInSc2	okruh
ITF	ITF	kA	ITF
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
sedmi	sedm	k4xCc6	sedm
singlových	singlový	k2eAgFnPc6d1	singlová
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
grandslamové	grandslamový	k2eAgFnSc6d1	grandslamová
úrovni	úroveň	k1gFnSc6	úroveň
získala	získat	k5eAaPmAgFnS	získat
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
premiérovém	premiérový	k2eAgNnSc6d1	premiérové
finále	finále	k1gNnSc6	finále
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2011	[number]	k4	2011
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Rusku	Ruska	k1gFnSc4	Ruska
Marii	Maria	k1gFnSc4	Maria
Šarapovovou	Šarapovová	k1gFnSc4	Šarapovová
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc7	první
tenistkou	tenistka	k1gFnSc7	tenistka
narozenou	narozený	k2eAgFnSc7d1	narozená
v	v	k7c4	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
turnaj	turnaj	k1gInSc4	turnaj
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc4d1	velká
čtyřky	čtyřka	k1gFnPc4	čtyřka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
finálovém	finálový	k2eAgInSc6d1	finálový
boji	boj	k1gInSc6	boj
na	na	k7c6	na
travnatém	travnatý	k2eAgInSc6d1	travnatý
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2014	[number]	k4	2014
hladce	hladko	k6eAd1	hladko
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Kanaďanku	Kanaďanka	k1gFnSc4	Kanaďanka
Eugenii	Eugenie	k1gFnSc4	Eugenie
Bouchardovou	Bouchardův	k2eAgFnSc7d1	Bouchardův
a	a	k8xC	a
po	po	k7c6	po
Martině	Martin	k2eAgFnSc6d1	Martina
Navrátilové	Navrátilová	k1gFnSc6	Navrátilová
vybojovala	vybojovat	k5eAaPmAgNnP	vybojovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
druhá	druhý	k4xOgFnSc1	druhý
žena	žena	k1gFnSc1	žena
narozená	narozený	k2eAgFnSc1d1	narozená
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednu	jeden	k4xCgFnSc4	jeden
mísu	mísa	k1gFnSc4	mísa
Venus	Venus	k1gMnSc1	Venus
Rosewater	Rosewater	k1gMnSc1	Rosewater
určenou	určený	k2eAgFnSc4d1	určená
pro	pro	k7c4	pro
šampiónky	šampiónka	k1gFnPc4	šampiónka
z	z	k7c2	z
All	All	k1gFnSc2	All
England	England	k1gInSc4	England
Clubu	club	k1gInSc2	club
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
a	a	k8xC	a
Ženská	ženský	k2eAgFnSc1d1	ženská
tenisová	tenisový	k2eAgFnSc1d1	tenisová
asociace	asociace	k1gFnSc1	asociace
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
hráčkou	hráčka	k1gFnSc7	hráčka
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
federace	federace	k1gFnSc1	federace
pak	pak	k6eAd1	pak
mistryní	mistryně	k1gFnSc7	mistryně
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
fedcupovém	fedcupový	k2eAgInSc6d1	fedcupový
týmu	tým	k1gInSc6	tým
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
získala	získat	k5eAaPmAgFnS	získat
vítězný	vítězný	k2eAgInSc4d1	vítězný
pohár	pohár	k1gInSc4	pohár
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
oporou	opora	k1gFnSc7	opora
družstva	družstvo	k1gNnSc2	družstvo
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
všech	všecek	k3xTgFnPc2	všecek
šest	šest	k4xCc1	šest
dvouher	dvouhra	k1gFnPc2	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
jedničky	jednička	k1gFnSc2	jednička
či	či	k8xC	či
dvojky	dvojka	k1gFnSc2	dvojka
týmu	tým	k1gInSc2	tým
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
další	další	k2eAgInSc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
trofeje	trofej	k1gInPc4	trofej
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
a	a	k8xC	a
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Berdychem	Berdych	k1gMnSc7	Berdych
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
XXXI	XXXI	kA	XXXI
<g/>
.	.	kIx.	.
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
de	de	k?	de
Janeiru	Janeir	k1gInSc2	Janeir
získala	získat	k5eAaPmAgFnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
Čuchajským	Čuchajský	k2eAgInSc7d1	Čuchajský
titulem	titul	k1gInSc7	titul
z	z	k7c2	z
WTA	WTA	kA	WTA
Elite	Elit	k1gInSc5	Elit
Trophy	Tropha	k1gFnSc2	Tropha
2016	[number]	k4	2016
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc7	první
tenistkou	tenistka	k1gFnSc7	tenistka
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
oba	dva	k4xCgInPc4	dva
závěrečné	závěrečný	k2eAgInPc4d1	závěrečný
turnaje	turnaj	k1gInPc4	turnaj
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
byla	být	k5eAaImAgFnS	být
nejvýše	nejvýše	k6eAd1	nejvýše
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
na	na	k7c4	na
196	[number]	k4	196
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
ji	on	k3xPp3gFnSc4	on
trenérsky	trenérsky	k6eAd1	trenérsky
povede	vést	k5eAaImIp3nS	vést
bývalý	bývalý	k2eAgMnSc1d1	bývalý
tenista	tenista	k1gMnSc1	tenista
Jiří	Jiří	k1gMnSc1	Jiří
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
.	.	kIx.	.
</s>
<s>
Kondičním	kondiční	k2eAgMnSc7d1	kondiční
koučem	kouč	k1gMnSc7	kouč
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
triatlonista	triatlonista	k1gMnSc1	triatlonista
David	David	k1gMnSc1	David
Vydra	Vydra	k1gMnSc1	Vydra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2010	[number]	k4	2010
ji	on	k3xPp3gFnSc4	on
Ženská	ženský	k2eAgFnSc1d1	ženská
tenisová	tenisový	k2eAgFnSc1d1	tenisová
asociace	asociace	k1gFnSc1	asociace
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nováčkem	nováček	k1gInSc7	nováček
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc4d1	následující
sezónu	sezóna	k1gFnSc4	sezóna
získala	získat	k5eAaPmAgFnS	získat
čtyři	čtyři	k4xCgFnPc4	čtyři
individuální	individuální	k2eAgFnPc4d1	individuální
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
hráčky	hráčka	k1gFnSc2	hráčka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
a	a	k8xC	a
2014	[number]	k4	2014
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ocenění	ocenění	k1gNnSc4	ocenění
pro	pro	k7c4	pro
kolektiv	kolektiv	k1gInSc4	kolektiv
roku	rok	k1gInSc2	rok
navíc	navíc	k6eAd1	navíc
vždy	vždy	k6eAd1	vždy
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
fedcupový	fedcupový	k2eAgInSc1d1	fedcupový
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
šestnácti	šestnáct	k4xCc2	šestnáct
let	léto	k1gNnPc2	léto
startovala	startovat	k5eAaBmAgFnS	startovat
za	za	k7c4	za
TJ	tj	kA	tj
Fulnek	Fulnek	k1gInSc4	Fulnek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
vedl	vést	k5eAaImAgMnS	vést
otec	otec	k1gMnSc1	otec
Jiří	Jiří	k1gMnSc1	Jiří
Kvita	Kvita	k1gMnSc1	Kvita
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gInSc7	její
mateřským	mateřský	k2eAgInSc7d1	mateřský
tenisovým	tenisový	k2eAgInSc7d1	tenisový
klubem	klub	k1gInSc7	klub
TK	TK	kA	TK
Agrofert	Agrofert	k1gInSc1	Agrofert
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
opakovaně	opakovaně	k6eAd1	opakovaně
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
republiky	republika	k1gFnSc2	republika
smíšených	smíšený	k2eAgNnPc2d1	smíšené
družstev	družstvo	k1gNnPc2	družstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
jehož	jehož	k3xOyRp3gNnSc1	jehož
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
doživotní	doživotní	k2eAgFnSc7d1	doživotní
čestnou	čestný	k2eAgFnSc7d1	čestná
členkou	členka	k1gFnSc7	členka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
i	i	k8xC	i
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
na	na	k7c6	na
Pardubické	pardubický	k2eAgFnSc6d1	pardubická
juniorce	juniorka	k1gFnSc6	juniorka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ženském	ženský	k2eAgInSc6d1	ženský
okruhu	okruh	k1gInSc6	okruh
ITF	ITF	kA	ITF
odehrála	odehrát	k5eAaPmAgFnS	odehrát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
a	a	k8xC	a
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgInSc4d1	zlatý
kanár	kanár	k1gInSc4	kanár
pak	pak	k6eAd1	pak
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
talent	talent	k1gInSc1	talent
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2007	[number]	k4	2007
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
turnaje	turnaj	k1gInSc2	turnaj
WTA	WTA	kA	WTA
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
stockholmském	stockholmský	k2eAgInSc6d1	stockholmský
Nordea	Nordeum	k1gNnSc2	Nordeum
Nordic	Nordic	k1gMnSc1	Nordic
Light	Light	k1gMnSc1	Light
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
polské	polský	k2eAgFnSc2d1	polská
kvalifikantce	kvalifikantka	k1gFnSc3	kvalifikantka
Martě	Marta	k1gFnSc3	Marta
Domachowské	Domachowská	k1gFnSc2	Domachowská
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Open	Open	k1gMnSc1	Open
Gaz	Gaz	k1gMnSc1	Gaz
de	de	k?	de
France	Franc	k1gMnSc4	Franc
porazila	porazit	k5eAaPmAgFnS	porazit
třicátou	třicátý	k4xOgFnSc4	třicátý
hráčku	hráčka	k1gFnSc4	hráčka
světa	svět	k1gInSc2	svět
Anabel	Anablo	k1gNnPc2	Anablo
Medinaovou	Medinaová	k1gFnSc7	Medinaová
Garriguesovou	Garriguesový	k2eAgFnSc7d1	Garriguesová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
debutovou	debutový	k2eAgFnSc4d1	debutová
výhru	výhra	k1gFnSc4	výhra
nad	nad	k7c7	nad
tenistkou	tenistka	k1gFnSc7	tenistka
z	z	k7c2	z
první	první	k4xOgFnSc2	první
padesátky	padesátka	k1gFnSc2	padesátka
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Memphisu	Memphis	k1gInSc6	Memphis
narazila	narazit	k5eAaPmAgFnS	narazit
jako	jako	k9	jako
kvalifikantka	kvalifikantka	k1gFnSc1	kvalifikantka
na	na	k7c4	na
bývalou	bývalý	k2eAgFnSc4d1	bývalá
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Venus	Venus	k1gMnSc1	Venus
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zdolala	zdolat	k5eAaPmAgFnS	zdolat
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
tak	tak	k6eAd1	tak
dokázala	dokázat	k5eAaPmAgFnS	dokázat
přehrát	přehrát	k5eAaPmF	přehrát
hráčku	hráčka	k1gFnSc4	hráčka
první	první	k4xOgFnPc4	první
světové	světový	k2eAgFnPc4d1	světová
desítky	desítka	k1gFnPc4	desítka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
143	[number]	k4	143
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
událostech	událost	k1gFnPc6	událost
okruhu	okruh	k1gInSc2	okruh
byla	být	k5eAaImAgFnS	být
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
v	v	k7c6	v
úvodních	úvodní	k2eAgNnPc6d1	úvodní
kolech	kolo	k1gNnPc6	kolo
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c4	na
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
ECM	ECM	kA	ECM
Prague	Prague	k1gFnSc4	Prague
Open	Opena	k1gFnPc2	Opena
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
turnaje	turnaj	k1gInSc2	turnaj
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Premiérovým	premiérový	k2eAgInSc7d1	premiérový
grandslamem	grandslam	k1gInSc7	grandslam
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
dějství	dějství	k1gNnSc6	dějství
postoupila	postoupit	k5eAaPmAgFnS	postoupit
přes	přes	k7c4	přes
Akiko	Akiko	k1gNnSc4	Akiko
Morigamiovou	Morigamiový	k2eAgFnSc4d1	Morigamiová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Samanthu	Samantha	k1gFnSc4	Samantha
Stosurovou	stosurový	k2eAgFnSc4d1	stosurový
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgMnSc6	třetí
pak	pak	k6eAd1	pak
Maďarku	Maďarka	k1gFnSc4	Maďarka
Ágnes	Ágnes	k1gInSc1	Ágnes
Szávayovou	Szávayová	k1gFnSc4	Szávayová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Estonku	Estonka	k1gFnSc4	Estonka
Kaiu	Kaius	k1gInSc2	Kaius
Kanepiovou	Kanepiový	k2eAgFnSc7d1	Kanepiová
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
zbylými	zbylý	k2eAgInPc7d1	zbylý
grandslamy	grandslam	k1gInPc7	grandslam
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
a	a	k8xC	a
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
prohrála	prohrát	k5eAaPmAgFnS	prohrát
zápasy	zápas	k1gInPc7	zápas
úvodního	úvodní	k2eAgNnSc2d1	úvodní
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
dokázala	dokázat	k5eAaPmAgFnS	dokázat
postoupit	postoupit	k5eAaPmF	postoupit
do	do	k7c2	do
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgMnSc6	který
na	na	k7c6	na
budapešťském	budapešťský	k2eAgInSc6d1	budapešťský
turnaji	turnaj	k1gInSc6	turnaj
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Andreje	Andrej	k1gMnSc4	Andrej
Klepačové	Klepačová	k1gFnSc2	Klepačová
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhých	druhý	k4xOgNnPc2	druhý
kol	kolo	k1gNnPc2	kolo
prošla	projít	k5eAaPmAgFnS	projít
na	na	k7c6	na
událostech	událost	k1gFnPc6	událost
East	East	k2eAgInSc1d1	East
West	West	k2eAgInSc1d1	West
Bank	bank	k1gInSc1	bank
Classic	Classice	k1gFnPc2	Classice
a	a	k8xC	a
Rogers	Rogersa	k1gFnPc2	Rogersa
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prohraném	prohraný	k2eAgNnSc6d1	prohrané
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
Anou	Anoum	k1gNnSc3	Anoum
Ivanovićovou	Ivanovićová	k1gFnSc7	Ivanovićová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
kvalifikantky	kvalifikantka	k1gFnSc2	kvalifikantka
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
pronikla	proniknout	k5eAaPmAgFnS	proniknout
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
mezi	mezi	k7c7	mezi
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
padesát	padesát	k4xCc1	padesát
singlistek	singlistka	k1gFnPc2	singlistka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Generali	Generali	k1gMnSc1	Generali
Ladies	Ladies	k1gMnSc1	Ladies
Linz	Linz	k1gMnSc1	Linz
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Francouzku	Francouzka	k1gFnSc4	Francouzka
Marion	Marion	k1gInSc1	Marion
Bartoliovou	Bartoliový	k2eAgFnSc4d1	Bartoliová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ITF	ITF	kA	ITF
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
na	na	k7c6	na
události	událost	k1gFnSc6	událost
v	v	k7c6	v
Monzónu	Monzón	k1gInSc6	Monzón
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazila	porazit	k5eAaPmAgFnS	porazit
Yaninu	Yanina	k1gFnSc4	Yanina
Wickmayerovou	Wickmayerový	k2eAgFnSc4d1	Wickmayerový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baráži	baráž	k1gFnSc6	baráž
o	o	k7c4	o
Světovou	světový	k2eAgFnSc4d1	světová
skupinu	skupina	k1gFnSc4	skupina
Fed	Fed	k1gMnSc1	Fed
Cupu	cup	k1gInSc2	cup
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
výhru	výhra	k1gFnSc4	výhra
nad	nad	k7c7	nad
Izraelkou	Izraelka	k1gFnSc7	Izraelka
Šachar	Šachara	k1gFnPc2	Šachara
Pe	Pe	k1gMnPc1	Pe
<g/>
'	'	kIx"	'
<g/>
erovou	erovat	k5eAaPmIp3nP	erovat
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
sezóny	sezóna	k1gFnSc2	sezóna
2009	[number]	k4	2009
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Brisbane	Brisban	k1gMnSc5	Brisban
International	International	k1gMnPc7	International
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
Srbce	Srbka	k1gFnSc6	Srbka
Aně	Aně	k1gFnSc1	Aně
Ivanovićové	Ivanovićová	k1gFnSc2	Ivanovićová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Hobartu	Hobart	k1gInSc6	Hobart
postupně	postupně	k6eAd1	postupně
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Australanku	Australanka	k1gFnSc4	Australanka
Sally	Salla	k1gFnSc2	Salla
Peersovou	Peersová	k1gFnSc7	Peersová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Ukrajinku	Ukrajinka	k1gFnSc4	Ukrajinka
Aljonu	Aljon	k1gInSc2	Aljon
Bondarenkovou	Bondarenkový	k2eAgFnSc4d1	Bondarenková
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Rusku	Ruska	k1gFnSc4	Ruska
Anastasii	Anastasie	k1gFnSc4	Anastasie
Pavljučenkovovou	Pavljučenkovová	k1gFnSc7	Pavljučenkovová
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Francouzku	Francouzka	k1gFnSc4	Francouzka
Virginii	Virginie	k1gFnSc4	Virginie
Razzanovou	Razzanová	k1gFnSc7	Razzanová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6-2	[number]	k4	6-2
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
finále	finále	k1gNnSc2	finále
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
porazila	porazit	k5eAaPmAgFnS	porazit
Češku	Češka	k1gFnSc4	Češka
Ivetu	Iveta	k1gFnSc4	Iveta
Benešovou	Benešová	k1gFnSc7	Benešová
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
turnaj	turnaj	k1gInSc4	turnaj
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
prohrála	prohrát	k5eAaPmAgFnS	prohrát
již	již	k6eAd1	již
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
třináctou	třináctý	k4xOgFnSc7	třináctý
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Běloruskou	běloruský	k2eAgFnSc7d1	Běloruská
Viktorií	Viktoria	k1gFnSc7	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc7d1	Azarenková
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Open	Open	k1gInSc4	Open
GDF	GDF	kA	GDF
Suez	Suez	k1gInSc1	Suez
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
na	na	k7c4	na
Barclays	Barclays	k1gInSc4	Barclays
Dubai	Dubai	k1gNnSc4	Dubai
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
pokaždé	pokaždé	k6eAd1	pokaždé
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
kvalifikantkami	kvalifikantka	k1gFnPc7	kvalifikantka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
postupně	postupně	k6eAd1	postupně
porazila	porazit	k5eAaPmAgFnS	porazit
Francouzku	Francouzka	k1gFnSc4	Francouzka
Pauline	Paulin	k1gInSc5	Paulin
Parmentierovou	Parmentierová	k1gFnSc7	Parmentierová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
Češku	Češka	k1gFnSc4	Češka
Ivetu	Iveta	k1gFnSc4	Iveta
Benešovou	Benešová	k1gFnSc7	Benešová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
vítězkou	vítězka	k1gFnSc7	vítězka
a	a	k8xC	a
šestou	šestý	k4xOgFnSc7	šestý
hráčkou	hráčka	k1gFnSc7	hráčka
světa	svět	k1gInSc2	svět
Ruskou	Ruska	k1gFnSc7	Ruska
Věrou	Věra	k1gFnSc7	Věra
Zvonarevovou	Zvonarevová	k1gFnSc7	Zvonarevová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Open	Open	k1gNnSc4	Open
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
Ladies	Ladies	k1gMnSc1	Ladies
Open	Open	k1gMnSc1	Open
a	a	k8xC	a
Estoril	Estoril	k1gMnSc1	Estoril
Open	Open	k1gNnSc1	Open
prohrála	prohrát	k5eAaPmAgFnS	prohrát
již	již	k9	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Mutua	Mutua	k1gFnSc1	Mutua
Madrileñ	Madrileñ	k1gMnPc2	Madrileñ
Madrid	Madrid	k1gInSc4	Madrid
Open	Opena	k1gFnPc2	Opena
prohrála	prohrát	k5eAaPmAgFnS	prohrát
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
Italkou	Italka	k1gFnSc7	Italka
Francescou	Francescý	k2eAgFnSc7d1	Francescý
Schiavoneovou	Schiavoneův	k2eAgFnSc7d1	Schiavoneův
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
kotníku	kotník	k1gInSc2	kotník
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
z	z	k7c2	z
French	Frencha	k1gFnPc2	Frencha
Open	Opena	k1gFnPc2	Opena
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c6	na
Rusku	Rusko	k1gNnSc6	Rusko
Marii	Maria	k1gFnSc4	Maria
Kirilenkovou	Kirilenkový	k2eAgFnSc4d1	Kirilenková
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Swedish	Swedish	k1gInSc4	Swedish
Open	Opena	k1gFnPc2	Opena
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
finalistkou	finalistka	k1gFnSc7	finalistka
a	a	k8xC	a
devátou	devátý	k4xOgFnSc7	devátý
hráčkou	hráčka	k1gFnSc7	hráčka
světa	svět	k1gInSc2	svět
Dánkou	Dánka	k1gFnSc7	Dánka
Carolinou	Carolina	k1gFnSc7	Carolina
Wozniackou	Wozniacka	k1gFnSc7	Wozniacka
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ECM	ECM	kA	ECM
Prague	Prague	k1gNnSc1	Prague
Open	Open	k1gNnSc1	Open
a	a	k8xC	a
Banka	banka	k1gFnSc1	banka
Koper	kopra	k1gFnPc2	kopra
Slovenia	Slovenium	k1gNnSc2	Slovenium
Open	Open	k1gMnSc1	Open
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Rusku	Ruska	k1gFnSc4	Ruska
Alisu	Alisa	k1gFnSc4	Alisa
Klejbanovovou	Klejbanovová	k1gFnSc7	Klejbanovová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
Italku	Italka	k1gFnSc4	Italka
Tathianu	Tathiana	k1gFnSc4	Tathiana
Garbinovou	Garbinová	k1gFnSc7	Garbinová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
porazila	porazit	k5eAaPmAgFnS	porazit
po	po	k7c6	po
úspěšně	úspěšně	k6eAd1	úspěšně
odvrácených	odvrácený	k2eAgInPc6d1	odvrácený
mečbolech	mečbol	k1gInPc6	mečbol
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
setu	set	k1gInSc6	set
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Rusku	Ruska	k1gFnSc4	Ruska
Dinaru	Dinara	k1gFnSc4	Dinara
Safinovou	Safinová	k1gFnSc7	Safinová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nepřešla	přejít	k5eNaPmAgFnS	přejít
přes	přes	k7c4	přes
Belgičanku	Belgičanka	k1gFnSc4	Belgičanka
Yaninu	Yanin	k2eAgFnSc4d1	Yanina
Wickmayerovou	Wickmayerová	k1gFnSc4	Wickmayerová
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Linci	Linec	k1gInSc6	Linec
postupně	postupně	k6eAd1	postupně
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Němku	Němka	k1gFnSc4	Němka
Andreu	Andrea	k1gFnSc4	Andrea
Petkovicovou	Petkovicový	k2eAgFnSc4d1	Petkovicová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Češku	Češka	k1gFnSc4	Češka
Ivetu	Iveta	k1gFnSc4	Iveta
Benešovou	Benešová	k1gFnSc7	Benešová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
Španělku	Španělka	k1gFnSc4	Španělka
Carlu	Carla	k1gFnSc4	Carla
Suárezovou	Suárezová	k1gFnSc4	Suárezová
Navarrovou	Navarrová	k1gFnSc7	Navarrová
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Polku	Polka	k1gFnSc4	Polka
Agnieszku	Agnieszka	k1gFnSc4	Agnieszka
Radwańskou	Radwańska	k1gFnSc7	Radwańska
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Belgičanku	Belgičanka	k1gFnSc4	Belgičanka
Yaninu	Yanin	k2eAgFnSc4d1	Yanina
Wickmayerovou	Wickmayerová	k1gFnSc4	Wickmayerová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
začala	začít	k5eAaPmAgFnS	začít
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
na	na	k7c4	na
turnaj	turnaj	k1gInSc4	turnaj
v	v	k7c6	v
Hobartu	Hobart	k1gInSc6	Hobart
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
prohrála	prohrát	k5eAaPmAgFnS	prohrát
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zde	zde	k6eAd1	zde
minulý	minulý	k2eAgInSc4d1	minulý
rok	rok	k1gInSc4	rok
slavila	slavit	k5eAaImAgFnS	slavit
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
grandslamu	grandslam	k1gInSc6	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
2010	[number]	k4	2010
Australian	Australiana	k1gFnPc2	Australiana
Open	Openo	k1gNnPc2	Openo
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
porazila	porazit	k5eAaPmAgFnS	porazit
pozdější	pozdní	k2eAgFnSc1d2	pozdější
vítězka	vítězka	k1gFnSc1	vítězka
Serena	Serena	k1gFnSc1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Memphisu	Memphis	k1gInSc6	Memphis
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
až	až	k9	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
s	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
vítězkou	vítězka	k1gFnSc7	vítězka
Marií	Maria	k1gFnSc7	Maria
Šarapovovou	Šarapovová	k1gFnSc7	Šarapovová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
prohrála	prohrát	k5eAaPmAgFnS	prohrát
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
Flavií	Flavie	k1gFnSc7	Flavie
Pennettaovou	Pennettaová	k1gFnSc7	Pennettaová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
poté	poté	k6eAd1	poté
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
dějství	dějství	k1gNnSc6	dějství
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Kim	Kim	k1gFnSc4	Kim
Clijstersovou	Clijstersová	k1gFnSc4	Clijstersová
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
dohrála	dohrát	k5eAaPmAgFnS	dohrát
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
postupně	postupně	k6eAd1	postupně
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Rumunku	Rumunka	k1gFnSc4	Rumunka
Soranu	Sorana	k1gFnSc4	Sorana
Cîrsteaovou	Cîrsteaová	k1gFnSc7	Cîrsteaová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Číňanku	Číňanka	k1gFnSc4	Číňanka
Čeng	Čeng	k1gInSc4	Čeng
Ťie	Ťie	k1gFnSc2	Ťie
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
čtrnáctou	čtrnáctý	k4xOgFnSc4	čtrnáctý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Bělorusku	Běloruska	k1gFnSc4	Běloruska
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
<g />
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
hráčku	hráčka	k1gFnSc4	hráčka
světa	svět	k1gInSc2	svět
Dánku	Dánka	k1gFnSc4	Dánka
Carolinu	Carolina	k1gFnSc4	Carolina
Wozniackou	Wozniacka	k1gFnSc7	Wozniacka
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Estonku	Estonka	k1gFnSc4	Estonka
Kaiu	Kaius	k1gInSc2	Kaius
Kanepiovou	Kanepiový	k2eAgFnSc4d1	Kanepiová
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
když	když	k8xS	když
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
pět	pět	k4xCc4	pět
mečbolů	mečbol	k1gInPc2	mečbol
<g/>
,	,	kIx,	,
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
postoupila	postoupit	k5eAaPmAgFnS	postoupit
až	až	k9	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
však	však	k9	však
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc4d2	pozdější
vítězku	vítězka	k1gFnSc4	vítězka
Serenu	Seren	k2eAgFnSc4d1	Serena
Williamsovou	Williamsová	k1gFnSc4	Williamsová
a	a	k8xC	a
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
turnaji	turnaj	k1gInSc6	turnaj
postoupila	postoupit	k5eAaPmAgFnS	postoupit
o	o	k7c4	o
33	[number]	k4	33
míst	místo	k1gNnPc2	místo
výše	vysoce	k6eAd2	vysoce
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
29	[number]	k4	29
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
následoval	následovat	k5eAaImAgInS	následovat
výkonnostní	výkonnostní	k2eAgInSc1d1	výkonnostní
pokles	pokles	k1gInSc1	pokles
a	a	k8xC	a
na	na	k7c6	na
pěti	pět	k4xCc6	pět
po	po	k7c4	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgInPc6d1	jdoucí
turnajích	turnaj	k1gInPc6	turnaj
prohrála	prohrát	k5eAaPmAgFnS	prohrát
již	již	k9	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Sérii	série	k1gFnSc4	série
proher	prohra	k1gFnPc2	prohra
přerušila	přerušit	k5eAaPmAgFnS	přerušit
až	až	k9	až
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Češkou	Češka	k1gFnSc7	Češka
Lucií	Lucie	k1gFnSc7	Lucie
Hradeckou	Hradecká	k1gFnSc7	Hradecká
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
až	až	k9	až
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opět	opět	k6eAd1	opět
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
pozdější	pozdní	k2eAgFnSc4d2	pozdější
vítězku	vítězka	k1gFnSc4	vítězka
Kim	Kim	k1gMnPc2	Kim
Clijstersovou	Clijstersový	k2eAgFnSc4d1	Clijstersová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
se	se	k3xPyFc4	se
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Dánce	Dánka	k1gFnSc3	Dánka
Carolině	Carolina	k1gFnSc3	Carolina
Wozniacké	Wozniacký	k2eAgFnSc2d1	Wozniacká
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zakončila	zakončit	k5eAaPmAgFnS	zakončit
na	na	k7c4	na
34	[number]	k4	34
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
Češka	Češka	k1gFnSc1	Češka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
zápasů	zápas	k1gInPc2	zápas
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
turnaji	turnaj	k1gInSc6	turnaj
sezóny	sezóna	k1gFnSc2	sezóna
v	v	k7c6	v
australském	australský	k2eAgInSc6d1	australský
Brisbane	Brisban	k1gMnSc5	Brisban
získala	získat	k5eAaPmAgFnS	získat
Kvitová	Kvitová	k1gFnSc1	Kvitová
svůj	svůj	k3xOyFgInSc4	svůj
druhý	druhý	k4xOgInSc4	druhý
titul	titul	k1gInSc4	titul
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Němku	Němka	k1gFnSc4	Němka
Andreu	Andrea	k1gFnSc4	Andrea
Petkovicovou	Petkovicový	k2eAgFnSc4d1	Petkovicová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
poprvé	poprvé	k6eAd1	poprvé
posunulo	posunout	k5eAaPmAgNnS	posunout
mezi	mezi	k7c4	mezi
třicet	třicet	k4xCc4	třicet
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
tenistek	tenistka	k1gFnPc2	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
porazila	porazit	k5eAaPmAgFnS	porazit
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
šestou	šestý	k4xOgFnSc4	šestý
hráčku	hráčka	k1gFnSc4	hráčka
světa	svět	k1gInSc2	svět
Australanku	Australanka	k1gFnSc4	Australanka
Samanthu	Samanth	k1gInSc2	Samanth
Stosurovou	stosurový	k2eAgFnSc4d1	stosurový
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
probojovala	probojovat	k5eAaPmAgFnS	probojovat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
druhé	druhý	k4xOgFnSc3	druhý
hráčce	hráčka	k1gFnSc3	hráčka
světa	svět	k1gInSc2	svět
Rusce	Ruska	k1gFnSc3	Ruska
Věře	Věra	k1gFnSc6	Věra
Zvonarevové	Zvonarevový	k2eAgNnSc1d1	Zvonarevový
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
mezi	mezi	k7c4	mezi
prvních	první	k4xOgInPc2	první
dvacet	dvacet	k4xCc4	dvacet
tenistek	tenistka	k1gFnPc2	tenistka
světa	svět	k1gInSc2	svět
na	na	k7c4	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
,	,	kIx,	,
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
první	první	k4xOgInSc4	první
turnaj	turnaj	k1gInSc4	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premira	k1gFnPc2	Premira
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Open	Open	k1gMnSc1	Open
GDF	GDF	kA	GDF
Suez	Suez	k1gInSc1	Suez
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Finálovou	finálový	k2eAgFnSc7d1	finálová
soupeřkou	soupeřka	k1gFnSc7	soupeřka
byla	být	k5eAaImAgFnS	být
aktuální	aktuální	k2eAgFnSc1d1	aktuální
šampiónka	šampiónka	k1gFnSc1	šampiónka
z	z	k7c2	z
Australian	Australiana	k1gFnPc2	Australiana
Open	Open	k1gInSc1	Open
Kim	Kim	k1gMnSc1	Kim
Clijstersová	Clijstersová	k1gFnSc1	Clijstersová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
navzdory	navzdory	k7c3	navzdory
tomuto	tento	k3xDgNnSc3	tento
prohranému	prohraný	k2eAgNnSc3d1	prohrané
finále	finále	k1gNnSc3	finále
vrátila	vrátit	k5eAaPmAgFnS	vrátit
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Kvitová	Kvitová	k1gFnSc1	Kvitová
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
posunula	posunout	k5eAaPmAgFnS	posunout
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
maximum	maximum	k1gNnSc4	maximum
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
čtrnáctkou	čtrnáctka	k1gFnSc7	čtrnáctka
hráčkou	hráčka	k1gFnSc7	hráčka
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnostní	výkonnostní	k2eAgInSc1d1	výkonnostní
útlum	útlum	k1gInSc1	útlum
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c6	na
třech	tři	k4xCgNnPc6	tři
dalších	další	k2eAgNnPc6d1	další
turnjích	turnjí	k1gNnPc6	turnjí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
ani	ani	k8xC	ani
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
WTA	WTA	kA	WTA
v	v	k7c4	v
Dubaji	Dubaje	k1gFnSc4	Dubaje
a	a	k8xC	a
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
a	a	k8xC	a
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
ITF	ITF	kA	ITF
v	v	k7c6	v
Nassau	Nassaus	k1gInSc6	Nassaus
na	na	k7c6	na
Bahamách	Bahamy	k1gFnPc6	Bahamy
<g/>
.	.	kIx.	.
</s>
<s>
Výhry	výhra	k1gFnSc2	výhra
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
až	až	k9	až
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
zápas	zápas	k1gInSc1	zápas
však	však	k9	však
opět	opět	k6eAd1	opět
prohrála	prohrát	k5eAaPmAgFnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
titul	titul	k1gInSc1	titul
sezóny	sezóna	k1gFnSc2	sezóna
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
titul	titul	k1gInSc4	titul
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Premier	Premira	k1gFnPc2	Premira
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postupně	postupně	k6eAd1	postupně
přehrála	přehrát	k5eAaPmAgFnS	přehrát
světovou	světový	k2eAgFnSc4d1	světová
trojku	trojka	k1gFnSc4	trojka
Zvonarevovou	Zvonarevová	k1gFnSc4	Zvonarevová
<g/>
,	,	kIx,	,
šestku	šestka	k1gFnSc4	šestka
Li	li	k8xS	li
Na	na	k7c4	na
a	a	k8xC	a
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
pak	pak	k8xC	pak
pětku	pětka	k1gFnSc4	pětka
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc7d1	Azarenková
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
první	první	k4xOgFnSc7	první
Češkou	Češka	k1gFnSc7	Češka
v	v	k7c6	v
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
figurovala	figurovat	k5eAaImAgFnS	figurovat
Nicole	Nicole	k1gFnSc1	Nicole
Vaidišová	Vaidišová	k1gFnSc1	Vaidišová
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
květnový	květnový	k2eAgInSc4d1	květnový
týden	týden	k1gInSc4	týden
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
turnaje	turnaj	k1gInSc2	turnaj
série	série	k1gFnSc2	série
ITF	ITF	kA	ITF
Sparta	Sparta	k1gFnSc1	Sparta
Prague	Prague	k1gFnSc1	Prague
Open	Open	k1gInSc4	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
překvapivě	překvapivě	k6eAd1	překvapivě
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Slovence	Slovenec	k1gMnSc4	Slovenec
Rybárikové	Rybárik	k1gMnPc1	Rybárik
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
Jelena	Jelena	k1gFnSc1	Jelena
Jankovićová	Jankovićová	k1gFnSc1	Jankovićová
neobhájila	obhájit	k5eNaPmAgFnS	obhájit
své	svůj	k3xOyFgInPc4	svůj
body	bod	k1gInPc4	bod
na	na	k7c6	na
římském	římský	k2eAgNnSc6d1	římské
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
mistrovství	mistrovství	k1gNnSc6	mistrovství
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
Kvitová	Kvitová	k1gFnSc1	Kvitová
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
přeskočila	přeskočit	k5eAaPmAgFnS	přeskočit
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
grandslamovém	grandslamový	k2eAgInSc6d1	grandslamový
turnaji	turnaj	k1gInSc6	turnaj
French	Frencha	k1gFnPc2	Frencha
Open	Openo	k1gNnPc2	Openo
prošla	projít	k5eAaPmAgFnS	projít
Kvitová	Kvitový	k2eAgFnSc1d1	Kvitová
až	až	k9	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
utkala	utkat	k5eAaPmAgFnS	utkat
s	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
vítězkou	vítězka	k1gFnSc7	vítězka
Li	li	k8xS	li
Na	na	k7c6	na
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc4	zápas
zahájila	zahájit	k5eAaPmAgFnS	zahájit
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Číňanka	Číňanka	k1gFnSc1	Číňanka
dokázala	dokázat	k5eAaPmAgFnS	dokázat
utkání	utkání	k1gNnSc4	utkání
otočit	otočit	k5eAaPmF	otočit
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
poměrem	poměr	k1gInSc7	poměr
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
týdnu	týden	k1gInSc6	týden
se	se	k3xPyFc4	se
Kvitová	Kvitová	k1gFnSc1	Kvitová
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
červnovém	červnový	k2eAgInSc6d1	červnový
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Eastbourne	Eastbourn	k1gInSc5	Eastbourn
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premier	k1gInSc1	Premier
došla	dojít	k5eAaPmAgFnS	dojít
až	až	k6eAd1	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
podlehla	podlehnout	k5eAaPmAgNnP	podlehnout
Francouzce	Francouzka	k1gFnSc3	Francouzka
Marion	Marion	k1gInSc4	Marion
Bartoliové	Bartoliový	k2eAgMnPc4d1	Bartoliový
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejslavnějším	slavný	k2eAgInSc6d3	nejslavnější
turnaji	turnaj	k1gInSc6	turnaj
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
postoupila	postoupit	k5eAaPmAgFnS	postoupit
až	až	k6eAd1	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c7	nad
Ruskou	Ruska	k1gFnSc7	Ruska
Marií	Maria	k1gFnSc7	Maria
Šarapovovou	Šarapovová	k1gFnSc7	Šarapovová
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
tak	tak	k9	tak
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
<g/>
..	..	k?	..
Touto	tento	k3xDgFnSc7	tento
výhrou	výhra	k1gFnSc7	výhra
se	se	k3xPyFc4	se
také	také	k9	také
automaticky	automaticky	k6eAd1	automaticky
stala	stát	k5eAaPmAgFnS	stát
čestnou	čestný	k2eAgFnSc7d1	čestná
členkou	členka	k1gFnSc7	členka
All	All	k1gFnSc2	All
England	Englando	k1gNnPc2	Englando
Lawn	Lawn	k1gMnSc1	Lawn
Tennis	Tennis	k1gFnSc2	Tennis
and	and	k?	and
Croquet	Croquet	k1gMnSc1	Croquet
Clubu	club	k1gInSc2	club
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letních	letní	k2eAgInPc6d1	letní
turnajích	turnaj	k1gInPc6	turnaj
na	na	k7c6	na
amerických	americký	k2eAgInPc6d1	americký
betonových	betonový	k2eAgInPc6d1	betonový
kurtech	kurt	k1gInPc6	kurt
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
Cincinatti	Cincinatť	k1gFnSc3	Cincinatť
prohrála	prohrát	k5eAaPmAgFnS	prohrát
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
s	s	k7c7	s
Němkou	Němka	k1gFnSc7	Němka
Andreou	Andrea	k1gFnSc7	Andrea
Petkovicovou	Petkovicový	k2eAgFnSc7d1	Petkovicová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
skončila	skončit	k5eAaPmAgFnS	skončit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
aktuální	aktuální	k2eAgFnSc1d1	aktuální
wimbledonská	wimbledonský	k2eAgFnSc1d1	wimbledonská
vítězka	vítězka	k1gFnSc1	vítězka
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
již	již	k6eAd1	již
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prohrou	prohra	k1gFnSc7	prohra
s	s	k7c7	s
Rumunkou	Rumunka	k1gFnSc7	Rumunka
Alexandrou	Alexandra	k1gFnSc7	Alexandra
Dulgheruovou	Dulgheruová	k1gFnSc7	Dulgheruová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
září	září	k1gNnSc2	září
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
tokijského	tokijský	k2eAgInSc2d1	tokijský
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Rusce	Ruska	k1gFnSc3	Ruska
Věře	Věra	k1gFnSc3	Věra
Zvonarevové	Zvonarevový	k2eAgFnSc2d1	Zvonarevová
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výsledek	výsledek	k1gInSc1	výsledek
jí	jíst	k5eAaImIp3nS	jíst
přesto	přesto	k8xC	přesto
zajistil	zajistit	k5eAaPmAgMnS	zajistit
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
China	China	k1gFnSc1	China
Open	Open	k1gInSc1	Open
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
byla	být	k5eAaImAgFnS	být
překvapivě	překvapivě	k6eAd1	překvapivě
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
Švédkou	Švédka	k1gFnSc7	Švédka
Sofií	Sofia	k1gFnSc7	Sofia
Arvidssonovou	Arvidssonová	k1gFnSc7	Arvidssonová
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
týden	týden	k1gInSc1	týden
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
opět	opět	k6eAd1	opět
posunula	posunout	k5eAaPmAgFnS	posunout
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
světového	světový	k2eAgNnSc2d1	světové
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
pátý	pátý	k4xOgInSc4	pátý
titul	titul	k1gInSc4	titul
sezóny	sezóna	k1gFnSc2	sezóna
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
na	na	k7c6	na
lineckém	linecký	k2eAgInSc6d1	linecký
turnaji	turnaj	k1gInSc6	turnaj
po	po	k7c6	po
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Dominikou	Dominika	k1gFnSc7	Dominika
Cibulkovou	Cibulková	k1gFnSc7	Cibulková
<g/>
.	.	kIx.	.
</s>
<s>
Singlovou	singlový	k2eAgFnSc4d1	singlová
sezónu	sezóna	k1gFnSc4	sezóna
zakončila	zakončit	k5eAaPmAgFnS	zakončit
říjnovým	říjnový	k2eAgInSc7d1	říjnový
Turnajem	turnaj	k1gInSc7	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
v	v	k7c6	v
tureckém	turecký	k2eAgInSc6d1	turecký
Istanbulu	Istanbul	k1gInSc6	Istanbul
pro	pro	k7c4	pro
osm	osm	k4xCc4	osm
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
tenistek	tenistka	k1gFnPc2	tenistka
světa	svět	k1gInSc2	svět
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
3	[number]	k4	3
<g/>
.	.	kIx.	.
nasazené	nasazený	k2eAgMnPc4d1	nasazený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
neztratila	ztratit	k5eNaPmAgFnS	ztratit
žádný	žádný	k3yNgInSc4	žádný
zápas	zápas	k1gInSc4	zápas
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
nad	nad	k7c7	nad
Viktorií	Viktoria	k1gFnSc7	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc7d1	Azarenková
z	z	k7c2	z
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
ji	on	k3xPp3gFnSc4	on
vynesl	vynést	k5eAaPmAgInS	vynést
až	až	k8xS	až
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
ztrátou	ztráta	k1gFnSc7	ztráta
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
jedničku	jednička	k1gFnSc4	jednička
Caroline	Carolin	k1gInSc5	Carolin
Wozniackou	Wozniacký	k2eAgFnSc7d1	Wozniacká
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
listopadový	listopadový	k2eAgInSc1d1	listopadový
víkend	víkend	k1gInSc1	víkend
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
týmem	tým	k1gInSc7	tým
Fed	Fed	k1gFnSc2	Fed
Cup	cup	k1gInSc1	cup
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sobotní	sobotní	k2eAgFnSc6d1	sobotní
dvouhře	dvouhra	k1gFnSc6	dvouhra
porazila	porazit	k5eAaPmAgFnS	porazit
Marii	Maria	k1gFnSc4	Maria
Kirilenkovou	Kirilenkový	k2eAgFnSc4d1	Kirilenková
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
pak	pak	k6eAd1	pak
po	po	k7c6	po
napínavém	napínavý	k2eAgNnSc6d1	napínavé
utkání	utkání	k1gNnSc6	utkání
také	také	k6eAd1	také
Světlanu	Světlana	k1gFnSc4	Světlana
Kuzněcovovou	Kuzněcovová	k1gFnSc7	Kuzněcovová
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
setu	set	k1gInSc6	set
již	již	k6eAd1	již
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
však	však	k9	však
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
šest	šest	k4xCc4	šest
her	hra	k1gFnPc2	hra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
zápas	zápas	k1gInSc4	zápas
dovedla	dovést	k5eAaPmAgFnS	dovést
do	do	k7c2	do
vítězného	vítězný	k2eAgInSc2d1	vítězný
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
21	[number]	k4	21
zápasech	zápas	k1gInPc6	zápas
sezóny	sezóna	k1gFnSc2	sezóna
hraných	hraný	k2eAgInPc2d1	hraný
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
šesti	šest	k4xCc2	šest
fedcupových	fedcupový	k2eAgInPc2d1	fedcupový
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Kvitová	Kvitová	k1gFnSc1	Kvitová
vítězná	vítězný	k2eAgFnSc1d1	vítězná
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
utkání	utkání	k1gNnSc6	utkání
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
české	český	k2eAgFnSc2d1	Česká
extraligy	extraliga	k1gFnSc2	extraliga
smíšených	smíšený	k2eAgNnPc2d1	smíšené
družstev	družstvo	k1gNnPc2	družstvo
mezi	mezi	k7c7	mezi
Agrofertem	Agrofert	k1gInSc7	Agrofert
Prostějov	Prostějov	k1gInSc1	Prostějov
a	a	k8xC	a
I.	I.	kA	I.
ČLTK	ČLTK	kA	ČLTK
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Lucií	Lucie	k1gFnSc7	Lucie
Hradecké	Hradecká	k1gFnSc3	Hradecká
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
tříhodinovém	tříhodinový	k2eAgInSc6d1	tříhodinový
boji	boj	k1gInSc6	boj
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
,	,	kIx,	,
65	[number]	k4	65
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
2011	[number]	k4	2011
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
největší	veliký	k2eAgFnSc1d3	veliký
finanční	finanční	k2eAgFnSc1d1	finanční
hotovost	hotovost	k1gFnSc1	hotovost
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
tenistek	tenistka	k1gFnPc2	tenistka
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
5	[number]	k4	5
145	[number]	k4	145
943	[number]	k4	943
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
částka	částka	k1gFnSc1	částka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
sezóně	sezóna	k1gFnSc6	sezóna
nevydělal	vydělat	k5eNaPmAgMnS	vydělat
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
český	český	k2eAgMnSc1d1	český
tenista	tenista	k1gMnSc1	tenista
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
sezóny	sezóna	k1gFnSc2	sezóna
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
jako	jako	k8xC	jako
možná	možný	k2eAgFnSc1d1	možná
příští	příští	k2eAgFnSc1d1	příští
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagovala	reagovat	k5eAaBmAgFnS	reagovat
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosažení	dosažení	k1gNnSc1	dosažení
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
"	"	kIx"	"
<g/>
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
pěkné	pěkný	k2eAgNnSc1d1	pěkné
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc7	její
prioritou	priorita	k1gFnSc7	priorita
je	být	k5eAaImIp3nS	být
zlepšování	zlepšování	k1gNnSc4	zlepšování
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
získávání	získávání	k1gNnSc2	získávání
bodů	bod	k1gInPc2	bod
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
obhajování	obhajování	k1gNnSc2	obhajování
titulu	titul	k1gInSc2	titul
z	z	k7c2	z
Brisbane	Brisban	k1gMnSc5	Brisban
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
startovat	startovat	k5eAaBmF	startovat
v	v	k7c6	v
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
po	po	k7c6	po
boku	bok	k1gInSc6	bok
světové	světový	k2eAgFnSc2d1	světová
sedmičky	sedmička	k1gFnSc2	sedmička
Tomáše	Tomáš	k1gMnSc2	Tomáš
Berdycha	Berdych	k1gMnSc2	Berdych
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
všechny	všechen	k3xTgFnPc4	všechen
dvouhry	dvouhra	k1gFnPc4	dvouhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
postupně	postupně	k6eAd1	postupně
porazila	porazit	k5eAaPmAgFnS	porazit
Cvetanu	Cvetan	k1gInSc6	Cvetan
Pironkovovou	Pironkovová	k1gFnSc4	Pironkovová
<g/>
,	,	kIx,	,
Bethanie	Bethanie	k1gFnSc1	Bethanie
Mattekovou-Sandsovou	Mattekovou-Sandsová	k1gFnSc4	Mattekovou-Sandsová
<g/>
,	,	kIx,	,
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Caroline	Carolin	k1gInSc5	Carolin
Wozniackou	Wozniacka	k1gFnSc7	Wozniacka
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Marion	Marion	k1gInSc1	Marion
Bartoliovou	Bartoliový	k2eAgFnSc4d1	Bartoliová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozice	pozice	k1gFnPc4	pozice
prvního	první	k4xOgInSc2	první
nasazeného	nasazený	k2eAgInSc2d1	nasazený
týmu	tým	k1gInSc2	tým
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
24	[number]	k4	24
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
jediného	jediný	k2eAgInSc2d1	jediný
mezistátního	mezistátní	k2eAgInSc2d1	mezistátní
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pátý	pátý	k4xOgInSc4	pátý
titul	titul	k1gInSc4	titul
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
třetí	třetí	k4xOgInPc4	třetí
v	v	k7c6	v
týmové	týmový	k2eAgFnSc6d1	týmová
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Kvitová	Kvitová	k1gFnSc1	Kvitová
mohla	moct	k5eAaImAgFnS	moct
s	s	k7c7	s
Berdychem	Berdych	k1gInSc7	Berdych
vytvořit	vytvořit	k5eAaPmF	vytvořit
pár	pár	k4xCyI	pár
pro	pro	k7c4	pro
smíšenou	smíšený	k2eAgFnSc4d1	smíšená
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jejím	její	k3xOp3gMnSc7	její
partnerem	partner	k1gMnSc7	partner
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
stane	stanout	k5eAaPmIp3nS	stanout
Radek	Radek	k1gMnSc1	Radek
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
.	.	kIx.	.
</s>
<s>
Mix	mix	k1gInSc1	mix
bude	být	k5eAaImBp3nS	být
do	do	k7c2	do
programu	program	k1gInSc2	program
zařazen	zařadit	k5eAaPmNgMnS	zařadit
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvních	první	k4xOgInPc6	první
dvou	dva	k4xCgInPc6	dva
turnajích	turnaj	k1gInPc6	turnaj
WTA	WTA	kA	WTA
měla	mít	k5eAaImAgFnS	mít
šanci	šance	k1gFnSc4	šance
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
oplatila	oplatit	k5eAaPmAgFnS	oplatit
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
Dulgheruové	Dulgheruové	k2eAgFnSc4d1	Dulgheruové
porážku	porážka	k1gFnSc4	porážka
z	z	k7c2	z
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
a	a	k8xC	a
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
Hantuchovou	Hantuchový	k2eAgFnSc4d1	Hantuchová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prohrála	prohrát	k5eAaPmAgFnS	prohrát
se	s	k7c7	s
čtvrtou	čtvrtá	k1gFnSc7	čtvrtá
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Li	li	k8xS	li
Na	na	k7c6	na
a	a	k8xC	a
obhájkyní	obhájkyně	k1gFnSc7	obhájkyně
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vedla	vést	k5eAaImAgFnS	vést
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
sety	set	k1gInPc4	set
a	a	k8xC	a
3-1	[number]	k4	3-1
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
prohrála	prohrát	k5eAaPmAgFnS	prohrát
poměrem	poměr	k1gInSc7	poměr
1-6	[number]	k4	1-6
7-5	[number]	k4	7-5
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgNnSc2	druhý
semifinále	semifinále	k1gNnSc2	semifinále
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c6	na
melbournském	melbournský	k2eAgInSc6d1	melbournský
grandslamu	grandslam	k1gInSc6	grandslam
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odjela	odjet	k5eAaPmAgFnS	odjet
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
smetla	smetnout	k5eAaPmAgFnS	smetnout
Věru	Věra	k1gFnSc4	Věra
Duševinovou	Duševinová	k1gFnSc4	Duševinová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
udolala	udolat	k5eAaPmAgFnS	udolat
Carlu	Carla	k1gFnSc4	Carla
Suárezovou	Suárezový	k2eAgFnSc4d1	Suárezový
Navarrovou	Navarrová	k1gFnSc4	Navarrová
6-4	[number]	k4	6-4
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
setu	set	k1gInSc6	set
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
jí	jíst	k5eAaImIp3nS	jíst
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
6-0	[number]	k4	6-0
a	a	k8xC	a
1-0	[number]	k4	1-0
vzdala	vzdát	k5eAaPmAgFnS	vzdát
Ruska	Ruska	k1gFnSc1	Ruska
Maria	Maria	k1gFnSc1	Maria
Kirilenková	Kirilenkový	k2eAgFnSc1d1	Kirilenková
<g/>
,	,	kIx,	,
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
bývalou	bývalý	k2eAgFnSc4d1	bývalá
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Anu	Anu	k1gMnSc4	Anu
Ivanovićovou	Ivanovićová	k1gFnSc4	Ivanovićová
a	a	k8xC	a
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Saru	Sara	k1gFnSc4	Sara
Erraniovou	Erraniový	k2eAgFnSc4d1	Erraniová
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Prohrou	prohra	k1gFnSc7	prohra
Dánky	Dánka	k1gFnSc2	Dánka
Wozniacké	Wozniacký	k2eAgFnSc2d1	Wozniacká
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
světový	světový	k2eAgInSc4d1	světový
tenisový	tenisový	k2eAgInSc4d1	tenisový
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
se	se	k3xPyFc4	se
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
vydání	vydání	k1gNnSc6	vydání
dostane	dostat	k5eAaPmIp3nS	dostat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
trojice	trojice	k1gFnSc2	trojice
Kvitová	Kvitová	k1gFnSc1	Kvitová
<g/>
,	,	kIx,	,
<g/>
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
<g/>
,	,	kIx,	,
<g/>
Azarenková	Azarenkový	k2eAgFnSc1d1	Azarenková
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
postoupily	postoupit	k5eAaPmAgFnP	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
Kvitové	Kvitová	k1gFnSc2	Kvitová
postavila	postavit	k5eAaPmAgFnS	postavit
právě	právě	k6eAd1	právě
turnajová	turnajový	k2eAgFnSc1d1	turnajová
i	i	k8xC	i
světová	světový	k2eAgFnSc1d1	světová
čtyřka	čtyřka	k1gFnSc1	čtyřka
Maria	Maria	k1gFnSc1	Maria
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
nad	nad	k7c4	nad
její	její	k3xOp3gFnPc4	její
síly	síla	k1gFnPc4	síla
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vedla	vést	k5eAaImAgFnS	vést
o	o	k7c4	o
brejk	brejk	k1gInSc4	brejk
,	,	kIx,	,
<g/>
a	a	k8xC	a
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Openo	k1gNnPc2	Openo
tak	tak	k6eAd1	tak
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
mírným	mírný	k2eAgInSc7d1	mírný
náskokem	náskok	k1gInSc7	náskok
si	se	k3xPyFc3	se
udržela	udržet	k5eAaPmAgFnS	udržet
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
právě	právě	k6eAd1	právě
před	před	k7c7	před
svojí	svůj	k3xOyFgFnSc7	svůj
přemožitelkou	přemožitelka	k1gFnSc7	přemožitelka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
hraném	hraný	k2eAgNnSc6d1	hrané
v	v	k7c6	v
Porsche-Areně	Porsche-Arena	k1gFnSc6	Porsche-Arena
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
proti	proti	k7c3	proti
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
obě	dva	k4xCgFnPc4	dva
třísetové	třísetový	k2eAgFnPc4d1	třísetová
dvouhry	dvouhra	k1gFnPc4	dvouhra
s	s	k7c7	s
Görgesovou	Görgesový	k2eAgFnSc7d1	Görgesová
i	i	k8xC	i
Lisickou	Lisická	k1gFnSc4	Lisická
a	a	k8xC	a
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
přes	přes	k7c4	přes
Německem	Německo	k1gNnSc7	Německo
do	do	k7c2	do
dubnového	dubnový	k2eAgNnSc2d1	dubnové
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Neobhajováním	neobhajování	k1gNnSc7	neobhajování
titulu	titul	k1gInSc2	titul
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
však	však	k9	však
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
achillovky	achillovka	k1gFnSc2	achillovka
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
5	[number]	k4	5
Qatar	Qatar	k1gMnSc1	Qatar
Total	totat	k5eAaImAgMnS	totat
Open	Open	k1gInSc4	Open
2012	[number]	k4	2012
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
angíně	angína	k1gFnSc3	angína
pak	pak	k6eAd1	pak
i	i	k9	i
z	z	k7c2	z
Dubaje	Dubaj	k1gInSc2	Dubaj
<g/>
.	.	kIx.	.
</s>
<s>
Účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
tak	tak	k9	tak
až	až	k6eAd1	až
velkých	velký	k2eAgInPc2d1	velký
amerických	americký	k2eAgInPc2d1	americký
turnajů	turnaj	k1gInPc2	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premira	k1gFnPc2	Premira
Mandatory	Mandator	k1gMnPc4	Mandator
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc4	Wells
oplatila	oplatit	k5eAaPmAgFnS	oplatit
loňskou	loňský	k2eAgFnSc4d1	loňská
porážku	porážka	k1gFnSc4	porážka
Záhlavové	Záhlavový	k2eAgFnSc2d1	Záhlavová
Strýcové	strýc	k1gMnPc1	strýc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
Američankou	Američanka	k1gFnSc7	Američanka
Christine	Christin	k1gInSc5	Christin
McHaleovou	McHaleův	k2eAgFnSc7d1	McHaleův
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelé	pořadatel	k1gMnPc1	pořadatel
udělili	udělit	k5eAaPmAgMnP	udělit
jí	on	k3xPp3gFnSc3	on
a	a	k8xC	a
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
světové	světový	k2eAgFnSc3d1	světová
jedničce	jednička	k1gFnSc3	jednička
Viktorii	Viktoria	k1gFnSc3	Viktoria
Azarenkové	Azarenkový	k2eAgInPc1d1	Azarenkový
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
do	do	k7c2	do
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
porazily	porazit	k5eAaPmAgFnP	porazit
tým	tým	k1gInSc1	tým
Grandinová	Grandinový	k2eAgFnSc1d1	Grandinová
<g/>
/	/	kIx~	/
<g/>
Uhlířová	Uhlířová	k1gFnSc1	Uhlířová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
viróze	viróza	k1gFnSc3	viróza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skolila	skolit	k5eAaImAgFnS	skolit
nejen	nejen	k6eAd1	nejen
Kvitovou	Kvitová	k1gFnSc4	Kvitová
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
před	před	k7c7	před
zápasem	zápas	k1gInSc7	zápas
s	s	k7c7	s
Erraniovou	Erraniový	k2eAgFnSc7d1	Erraniová
a	a	k8xC	a
Vinciovou	Vinciový	k2eAgFnSc7d1	Vinciová
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nestačila	stačit	k5eNaBmAgFnS	stačit
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
Miami	Miami	k1gNnSc2	Miami
Masters	Masters	k1gInSc4	Masters
na	na	k7c4	na
bývalou	bývalý	k2eAgFnSc4d1	bývalá
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Venus	Venus	k1gMnSc1	Venus
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
,	,	kIx,	,
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
134	[number]	k4	134
<g/>
.	.	kIx.	.
hráčku	hráčka	k1gFnSc4	hráčka
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dostala	dostat	k5eAaPmAgFnS	dostat
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
setu	set	k1gInSc6	set
"	"	kIx"	"
<g/>
kanára	kanár	k1gMnSc2	kanár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
měsíční	měsíční	k2eAgFnSc6d1	měsíční
pauze	pauza	k1gFnSc6	pauza
vyplněné	vyplněný	k2eAgFnSc2d1	vyplněná
kondičním	kondiční	k2eAgNnSc7d1	kondiční
cvičením	cvičení	k1gNnSc7	cvičení
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
ostravské	ostravský	k2eAgFnSc2d1	Ostravská
ČEZ	ČEZ	kA	ČEZ
Arény	aréna	k1gFnSc2	aréna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
udržela	udržet	k5eAaPmAgFnS	udržet
halovou	halový	k2eAgFnSc4d1	halová
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
fedcupovém	fedcupový	k2eAgNnSc6d1	fedcupové
semifinále	semifinále	k1gNnSc6	semifinále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Francescu	Francesca	k1gFnSc4	Francesca
Schiavoneovou	Schiavoneův	k2eAgFnSc7d1	Schiavoneův
a	a	k8xC	a
Saru	Sara	k1gFnSc4	Sara
Erraniovou	Erraniový	k2eAgFnSc4d1	Erraniová
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
přes	přes	k7c4	přes
Itálii	Itálie	k1gFnSc4	Itálie
probojoval	probojovat	k5eAaPmAgMnS	probojovat
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
stuttgartské	stuttgartský	k2eAgFnSc2d1	Stuttgartská
haly	hala	k1gFnSc2	hala
na	na	k7c4	na
první	první	k4xOgInSc4	první
antukový	antukový	k2eAgInSc4d1	antukový
turnaj	turnaj	k1gInSc4	turnaj
sezóny	sezóna	k1gFnSc2	sezóna
a	a	k8xC	a
jediný	jediný	k2eAgInSc4d1	jediný
hraný	hraný	k2eAgInSc4d1	hraný
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
<g/>
.	.	kIx.	.
</s>
<s>
Přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
Francesu	Francesa	k1gFnSc4	Francesa
Schiavoneovou	Schiavoneův	k2eAgFnSc7d1	Schiavoneův
a	a	k8xC	a
domácí	domácí	k2eAgFnSc7d1	domácí
Angelique	Angelique	k1gFnSc7	Angelique
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
pozdější	pozdní	k2eAgFnSc2d2	pozdější
vítězky	vítězka	k1gFnSc2	vítězka
turnaje	turnaj	k1gInSc2	turnaj
Marie	Maria	k1gFnSc2	Maria
Šarapovové	Šarapovová	k1gFnSc2	Šarapovová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
netradiční	tradiční	k2eNgFnSc4d1	netradiční
madridskou	madridský	k2eAgFnSc4d1	Madridská
modrou	modrý	k2eAgFnSc4d1	modrá
antuku	antuka	k1gFnSc4	antuka
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
jako	jako	k9	jako
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
ji	on	k3xPp3gFnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
kvalifikantka	kvalifikantka	k1gFnSc1	kvalifikantka
a	a	k8xC	a
krajanka	krajanka	k1gFnSc1	krajanka
Lucie	Lucie	k1gFnSc1	Lucie
Hradecká	Hradecká	k1gFnSc1	Hradecká
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
došla	dojít	k5eAaPmAgFnS	dojít
až	až	k9	až
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výsledek	výsledek	k1gInSc1	výsledek
znamenal	znamenat	k5eAaImAgInS	znamenat
pokles	pokles	k1gInSc4	pokles
na	na	k7c4	na
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
římském	římský	k2eAgInSc6d1	římský
Internazionali	Internazionali	k1gMnSc3	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italia	k1gFnSc1	Italia
pak	pak	k6eAd1	pak
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
vydřených	vydřený	k2eAgFnPc6d1	vydřená
výhrách	výhra	k1gFnPc6	výhra
mezi	mezi	k7c7	mezi
posledními	poslední	k2eAgMnPc7d1	poslední
osmi	osm	k4xCc7	osm
odešla	odejít	k5eAaPmAgFnS	odejít
poražena	porazit	k5eAaPmNgFnS	porazit
od	od	k7c2	od
Angelique	Angelique	k1gNnPc2	Angelique
Kerberové	Kerber	k1gMnPc1	Kerber
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
sehrála	sehrát	k5eAaPmAgFnS	sehrát
třísetový	třísetový	k2eAgInSc4d1	třísetový
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
limitována	limitovat	k5eAaBmNgFnS	limitovat
natažením	natažení	k1gNnSc7	natažení
břišního	břišní	k2eAgInSc2d1	břišní
svalu	sval	k1gInSc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
grandslamu	grandslam	k1gInSc6	grandslam
French	Frencha	k1gFnPc2	Frencha
Open	Openo	k1gNnPc2	Openo
hrála	hrát	k5eAaImAgFnS	hrát
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
nasazené	nasazený	k2eAgFnSc2d1	nasazená
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
postupně	postupně	k6eAd1	postupně
porazila	porazit	k5eAaPmAgFnS	porazit
Ashleigh	Ashleigh	k1gInSc4	Ashleigh
Bartyovou	Bartyová	k1gFnSc4	Bartyová
<g/>
,	,	kIx,	,
Urszulu	Urszula	k1gFnSc4	Urszula
Radwańskou	Radwańská	k1gFnSc4	Radwańská
<g/>
,	,	kIx,	,
Ninu	Nina	k1gFnSc4	Nina
Bratčikovovou	Bratčikovová	k1gFnSc4	Bratčikovová
<g/>
,	,	kIx,	,
Varvaru	Varvar	k1gInSc2	Varvar
Lepčenkovou	Lepčenkový	k2eAgFnSc7d1	Lepčenková
a	a	k8xC	a
Jaroslavu	Jaroslava	k1gFnSc4	Jaroslava
Švedovovou	Švedovový	k2eAgFnSc4d1	Švedovový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
postup	postup	k1gInSc4	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
nestačila	stačit	k5eNaBmAgFnS	stačit
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
na	na	k7c4	na
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
dvojku	dvojka	k1gFnSc4	dvojka
Marii	Maria	k1gFnSc4	Maria
Šarapovovou	Šarapovová	k1gFnSc4	Šarapovová
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gInSc4	jejich
třetí	třetí	k4xOgInSc4	třetí
souboj	souboj	k1gInSc4	souboj
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
čtyř	čtyři	k4xCgInPc2	čtyři
grandslamů	grandslam	k1gInPc2	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
zahájila	zahájit	k5eAaPmAgFnS	zahájit
vyřazením	vyřazení	k1gNnSc7	vyřazení
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
eastbournského	eastbournský	k2eAgInSc2d1	eastbournský
AEGON	AEGON	kA	AEGON
International	International	k1gMnSc1	International
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
loňská	loňský	k2eAgFnSc1d1	loňská
finalistka	finalistka	k1gFnSc1	finalistka
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
ruské	ruský	k2eAgFnSc2d1	ruská
hráčky	hráčka	k1gFnSc2	hráčka
Jekatěriny	Jekatěrin	k1gInPc1	Jekatěrin
Makarovové	Makarovový	k2eAgInPc1d1	Makarovový
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
jako	jako	k9	jako
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
a	a	k8xC	a
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
prvenství	prvenství	k1gNnSc2	prvenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
třech	tři	k4xCgNnPc6	tři
kolech	kolo	k1gNnPc6	kolo
neztratila	ztratit	k5eNaPmAgFnS	ztratit
jediný	jediný	k2eAgInSc4d1	jediný
set	set	k1gInSc4	set
a	a	k8xC	a
po	po	k7c6	po
výhrách	výhra	k1gFnPc6	výhra
nad	nad	k7c7	nad
Amanmuradovovou	Amanmuradovová	k1gFnSc7	Amanmuradovová
<g/>
,	,	kIx,	,
Baltachovou	Baltachův	k2eAgFnSc7d1	Baltachův
a	a	k8xC	a
Lepčenkovou	Lepčenkový	k2eAgFnSc7d1	Lepčenková
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
osmifinále	osmifinále	k1gNnSc2	osmifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
porazila	porazit	k5eAaPmAgFnS	porazit
Italku	Italka	k1gFnSc4	Italka
Schiavoneovou	Schiavoneův	k2eAgFnSc7d1	Schiavoneův
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7-5	[number]	k4	7-5
a	a	k8xC	a
6-1	[number]	k4	6-1
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
wimbledonské	wimbledonský	k2eAgNnSc4d1	wimbledonské
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
v	v	k7c6	v
řádě	řád	k1gInSc6	řád
<g/>
.	.	kIx.	.
</s>
<s>
Kvitová	Kvitová	k1gFnSc1	Kvitová
ale	ale	k9	ale
nestačila	stačit	k5eNaBmAgFnS	stačit
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
na	na	k7c4	na
dobře	dobře	k6eAd1	dobře
servírující	servírující	k2eAgFnSc4d1	servírující
šestou	šestý	k4xOgFnSc4	šestý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Američanku	Američanka	k1gFnSc4	Američanka
Serenu	Seren	k2eAgFnSc4d1	Serena
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
celý	celý	k2eAgInSc4d1	celý
turnaj	turnaj	k1gInSc4	turnaj
nakonec	nakonec	k6eAd1	nakonec
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mohla	moct	k5eAaImAgFnS	moct
zápas	zápas	k1gInSc4	zápas
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
třetího	třetí	k4xOgInSc2	třetí
setu	set	k1gInSc2	set
<g/>
;	;	kIx,	;
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
druhého	druhý	k4xOgInSc2	druhý
setu	set	k1gInSc2	set
měla	mít	k5eAaImAgFnS	mít
setbol	setbol	k1gInSc4	setbol
<g/>
.	.	kIx.	.
</s>
<s>
Kvitová	Kvitová	k1gFnSc1	Kvitová
tak	tak	k9	tak
ze	z	k7c2	z
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
místa	místo	k1gNnSc2	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
spadla	spadnout	k5eAaPmAgFnS	spadnout
na	na	k7c6	na
šesté	šestý	k4xOgFnSc6	šestý
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c4	před
Němku	Němka	k1gFnSc4	Němka
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
následovala	následovat	k5eAaImAgFnS	následovat
téměř	téměř	k6eAd1	téměř
třítýdenní	třítýdenní	k2eAgFnSc1d1	třítýdenní
pauza	pauza	k1gFnSc1	pauza
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
připravovala	připravovat	k5eAaImAgFnS	připravovat
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
sezony	sezona	k1gFnSc2	sezona
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgInSc1d1	olympijský
turnaj	turnaj	k1gInSc1	turnaj
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
šesté	šestý	k4xOgFnSc2	šestý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
hráčky	hráčka	k1gFnSc2	hráčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
porazila	porazit	k5eAaPmAgFnS	porazit
Ukrajinku	Ukrajinka	k1gFnSc4	Ukrajinka
Katerynu	Kateryn	k1gInSc2	Kateryn
Bondarenkovou	Bondarenkový	k2eAgFnSc4d1	Bondarenková
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
kole	kolo	k1gNnSc6	kolo
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
Číňanku	Číňanka	k1gFnSc4	Číňanka
Pcheng	Pcheng	k1gMnSc1	Pcheng
Šuaj	Šuaj	k1gMnSc1	Šuaj
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
zdolala	zdolat	k5eAaPmAgFnS	zdolat
také	také	k9	také
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zápasem	zápas	k1gInSc7	zápas
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
čekal	čekat	k5eAaImAgMnS	čekat
s	s	k7c7	s
Italkou	Italka	k1gFnSc7	Italka
Flavií	Flavie	k1gFnSc7	Flavie
Pennetaovou	Pennetaová	k1gFnSc7	Pennetaová
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Kvitová	Kvitová	k1gFnSc1	Kvitová
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
na	na	k7c4	na
natažený	natažený	k2eAgInSc4d1	natažený
břišní	břišní	k2eAgInSc4d1	břišní
sval	sval	k1gInSc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gNnSc3	on
zrušila	zrušit	k5eAaPmAgFnS	zrušit
svoji	svůj	k3xOyFgFnSc4	svůj
původně	původně	k6eAd1	původně
nahlášenou	nahlášený	k2eAgFnSc4d1	nahlášená
účast	účast	k1gFnSc4	účast
ve	v	k7c6	v
smíšené	smíšený	k2eAgFnSc6d1	smíšená
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
s	s	k7c7	s
Radkem	Radek	k1gMnSc7	Radek
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Pennettaovou	Pennettaová	k1gFnSc7	Pennettaová
však	však	k9	však
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Rusce	Ruska	k1gFnSc3	Ruska
Marii	Maria	k1gFnSc3	Maria
Kirilenkové	Kirilenkový	k2eAgFnSc2d1	Kirilenková
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
titul	titul	k1gInSc1	titul
sezóny	sezóna	k1gFnSc2	sezóna
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
osmý	osmý	k4xOgMnSc1	osmý
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
amerických	americký	k2eAgInPc6d1	americký
betonech	beton	k1gInPc6	beton
série	série	k1gFnSc2	série
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
když	když	k8xS	když
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
montréalském	montréalský	k2eAgInSc6d1	montréalský
Canada	Canada	k1gFnSc1	Canada
Masters	Masters	k1gInSc1	Masters
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
otočila	otočit	k5eAaPmAgFnS	otočit
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
Xenii	Xenie	k1gFnSc3	Xenie
Pervekovou	Perveková	k1gFnSc4	Perveková
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
soupeřka	soupeřka	k1gFnSc1	soupeřka
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
podávala	podávat	k5eAaImAgFnS	podávat
na	na	k7c4	na
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
Češka	Češka	k1gFnSc1	Češka
druhý	druhý	k4xOgInSc4	druhý
set	set	k1gInSc4	set
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
hře	hra	k1gFnSc6	hra
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
setu	set	k1gInSc6	set
nadělila	nadělit	k5eAaPmAgFnS	nadělit
soupeřce	soupeřka	k1gFnSc3	soupeřka
"	"	kIx"	"
<g/>
kanára	kanár	k1gMnSc2	kanár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
porazila	porazit	k5eAaPmAgFnS	porazit
hráčku	hráčka	k1gFnSc4	hráčka
elitní	elitní	k2eAgFnSc2d1	elitní
desítky	desítka	k1gFnSc2	desítka
<g/>
,	,	kIx,	,
když	když	k8xS	když
zničila	zničit	k5eAaPmAgFnS	zničit
Marion	Marion	k1gInSc4	Marion
Bartoliovou	Bartoliový	k2eAgFnSc4d1	Bartoliová
6-1	[number]	k4	6-1
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
raketě	raketa	k1gFnSc6	raketa
zůstala	zůstat	k5eAaPmAgFnS	zůstat
bývalá	bývalý	k2eAgFnSc1d1	bývalá
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
Dánka	Dánka	k1gFnSc1	Dánka
Caroline	Carolin	k1gInSc5	Carolin
Wozniacká	Wozniacký	k2eAgNnPc4d1	Wozniacký
a	a	k8xC	a
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
přehrála	přehrát	k5eAaPmAgFnS	přehrát
čínskou	čínský	k2eAgFnSc4d1	čínská
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
desítku	desítka	k1gFnSc4	desítka
Li	li	k8xS	li
Na	na	k7c4	na
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
,	,	kIx,	,
když	když	k8xS	když
rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
podání	podání	k1gNnSc1	podání
soupeřce	soupeřka	k1gFnSc3	soupeřka
sebrala	sebrat	k5eAaPmAgFnS	sebrat
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
na	na	k7c4	na
gamy	game	k1gInPc4	game
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sadě	sada	k1gFnSc6	sada
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
představoval	představovat	k5eAaImAgInS	představovat
první	první	k4xOgInSc4	první
finálový	finálový	k2eAgInSc4d1	finálový
zápas	zápas	k1gInSc4	zápas
hraný	hraný	k2eAgInSc4d1	hraný
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
se	se	k3xPyFc4	se
po	po	k7c6	po
výhrách	výhra	k1gFnPc6	výhra
nad	nad	k7c7	nad
Barthelovou	Barthelová	k1gFnSc7	Barthelová
7-5	[number]	k4	7-5
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
setu	set	k1gInSc6	set
<g/>
,	,	kIx,	,
Pchengovou	Pchengový	k2eAgFnSc4d1	Pchengová
a	a	k8xC	a
Pavlujčenkovovou	Pavlujčenkovová	k1gFnSc4	Pavlujčenkovová
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
dostala	dostat	k5eAaPmAgFnS	dostat
také	také	k9	také
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Němkou	Němka	k1gFnSc7	Němka
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
přidala	přidat	k5eAaPmAgFnS	přidat
titul	titul	k1gInSc4	titul
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
New	New	k1gFnSc6	New
Havenu	Haven	k1gInSc2	Haven
<g/>
,	,	kIx,	,
když	když	k8xS	když
postupně	postupně	k6eAd1	postupně
porazila	porazit	k5eAaPmAgFnS	porazit
Gibbsovou	Gibbsová	k1gFnSc4	Gibbsová
<g/>
,	,	kIx,	,
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
hráčku	hráčka	k1gFnSc4	hráčka
světové	světový	k2eAgFnSc2d1	světová
dvacítky	dvacítka	k1gFnSc2	dvacítka
a	a	k8xC	a
svojí	svojit	k5eAaImIp3nS	svojit
krajanku	krajanka	k1gFnSc4	krajanka
Lucii	Lucie	k1gFnSc4	Lucie
Šafářovou	Šafářová	k1gFnSc4	Šafářová
a	a	k8xC	a
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Erraniovou	Erraniový	k2eAgFnSc7d1	Erraniová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
oplatila	oplatit	k5eAaPmAgFnS	oplatit
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
porážku	porážka	k1gFnSc4	porážka
Kirilenkové	Kirilenkový	k2eAgFnSc2d1	Kirilenková
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
7-6	[number]	k4	7-6
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
setu	set	k1gInSc6	set
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
setboly	setbol	k1gInPc4	setbol
a	a	k8xC	a
tiebreak	tiebreak	k6eAd1	tiebreak
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
poměrem	poměr	k1gInSc7	poměr
11-9	[number]	k4	11-9
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
ze	z	k7c2	z
stavu	stav	k1gInSc2	stav
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
výsledkům	výsledek	k1gInPc3	výsledek
poprvé	poprvé	k6eAd1	poprvé
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
letní	letní	k2eAgInSc4d1	letní
US	US	kA	US
Open	Open	k1gInSc4	Open
Series	Seriesa	k1gFnPc2	Seriesa
a	a	k8xC	a
posunula	posunout	k5eAaPmAgFnS	posunout
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
startovala	startovat	k5eAaBmAgFnS	startovat
jako	jako	k9	jako
pátá	pátý	k4xOgFnSc1	pátý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
hráčka	hráčka	k1gFnSc1	hráčka
<g/>
,	,	kIx,	,
porazila	porazit	k5eAaPmAgFnS	porazit
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
Slovinku	Slovinka	k1gFnSc4	Slovinka
Polonu	Polona	k1gFnSc4	Polona
Hercogovou	Hercogový	k2eAgFnSc4d1	Hercogová
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgFnSc7	první
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
až	až	k8xS	až
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
porazila	porazit	k5eAaPmAgFnS	porazit
shodně	shodně	k6eAd1	shodně
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
sety	set	k1gInPc4	set
francouzské	francouzský	k2eAgFnSc2d1	francouzská
tenistky	tenistka	k1gFnSc2	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
Alizé	Alizý	k2eAgNnSc1d1	Alizý
Cornetovou	Cornetový	k2eAgFnSc7d1	Cornetová
a	a	k8xC	a
následně	následně	k6eAd1	následně
Pauline	Paulin	k1gInSc5	Paulin
Parmantierovou	Parmantierův	k2eAgFnSc7d1	Parmantierův
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
Francouzka	Francouzka	k1gFnSc1	Francouzka
<g/>
,	,	kIx,	,
Marion	Marion	k1gInSc1	Marion
Bartoliová	Bartoliový	k2eAgFnSc1d1	Bartoliová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
čekala	čekat	k5eAaImAgFnS	čekat
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
nad	nad	k7c4	nad
její	její	k3xOp3gFnPc4	její
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
utkání	utkání	k1gNnSc2	utkání
odešla	odejít	k5eAaPmAgFnS	odejít
poražena	porazit	k5eAaPmNgFnS	porazit
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
setu	set	k1gInSc6	set
obdržela	obdržet	k5eAaPmAgFnS	obdržet
"	"	kIx"	"
<g/>
kanára	kanár	k1gMnSc2	kanár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dohrání	dohrání	k1gNnSc6	dohrání
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Bartoliové	Bartoliový	k2eAgNnSc4d1	Bartoliové
cítila	cítit	k5eAaImAgFnS	cítit
na	na	k7c4	na
kurtu	kurta	k1gFnSc4	kurta
naprosto	naprosto	k6eAd1	naprosto
bezradně	bezradně	k6eAd1	bezradně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podzimní	podzimní	k2eAgFnSc6d1	podzimní
asijské	asijský	k2eAgFnSc6d1	asijská
části	část	k1gFnSc6	část
WTA	WTA	kA	WTA
Tour	Toura	k1gFnPc2	Toura
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
nasazené	nasazený	k2eAgFnPc4d1	nasazená
do	do	k7c2	do
tokijského	tokijský	k2eAgMnSc2d1	tokijský
Toray	Toraa	k1gMnSc2	Toraa
Pan	Pan	k1gMnSc1	Pan
Pacific	Pacific	k1gMnSc1	Pacific
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Chorvatce	Chorvatka	k1gFnSc3	Chorvatka
Petře	Petra	k1gFnSc3	Petra
Martićové	Martićová	k1gFnSc3	Martićová
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
nasazená	nasazený	k2eAgFnSc1d1	nasazená
byla	být	k5eAaImAgFnS	být
také	také	k9	také
v	v	k7c6	v
pekingském	pekingský	k2eAgInSc6d1	pekingský
China	China	k1gFnSc1	China
Open	Openo	k1gNnPc2	Openo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
porazila	porazit	k5eAaPmAgFnS	porazit
Slovenku	Slovenka	k1gFnSc4	Slovenka
Hantuchovou	Hantuchový	k2eAgFnSc4d1	Hantuchová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
ji	on	k3xPp3gFnSc4	on
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Carla	Carla	k1gFnSc1	Carla
Suárezová	Suárezová	k1gFnSc1	Suárezová
Navarrová	Navarrová	k1gFnSc1	Navarrová
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
se	se	k3xPyFc4	se
kvalifikovala	kvalifikovat	k5eAaBmAgNnP	kvalifikovat
na	na	k7c4	na
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
Turnaj	turnaj	k1gInSc4	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přijížděla	přijíždět	k5eAaImAgFnS	přijíždět
jako	jako	k9	jako
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
světová	světový	k2eAgFnSc1d1	světová
šestka	šestka	k1gFnSc1	šestka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nalosována	nalosovat	k5eAaBmNgFnS	nalosovat
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Šarapovovou	Šarapovová	k1gFnSc7	Šarapovová
<g/>
,	,	kIx,	,
<g/>
Agnieszkou	Agnieszka	k1gFnSc7	Agnieszka
Radwańskou	Radwańska	k1gFnSc7	Radwańska
a	a	k8xC	a
Sarou	Sara	k1gFnSc7	Sara
Erraniovou	Erraniový	k2eAgFnSc7d1	Erraniová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
první	první	k4xOgFnSc6	první
porážce	porážka	k1gFnSc6	porážka
od	od	k7c2	od
Agnieszky	Agnieszka	k1gFnSc2	Agnieszka
Radwańské	Radwańská	k1gFnSc2	Radwańská
však	však	k9	však
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
pro	pro	k7c4	pro
zánět	zánět	k1gInSc4	zánět
nosohltanu	nosohltan	k1gInSc2	nosohltan
<g/>
..	..	k?	..
Odhlášení	odhlášení	k1gNnSc4	odhlášení
komentovala	komentovat	k5eAaBmAgFnS	komentovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
líto	líto	k6eAd1	líto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
s	s	k7c7	s
lékaři	lékař	k1gMnPc7	lékař
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebudu	být	k5eNaImBp1nS	být
dál	daleko	k6eAd2	daleko
hrát	hrát	k5eAaImF	hrát
...	...	k?	...
Jsem	být	k5eAaImIp1nS	být
bojovnice	bojovnice	k1gFnSc1	bojovnice
a	a	k8xC	a
vždycky	vždycky	k6eAd1	vždycky
jsem	být	k5eAaImIp1nS	být
chtěla	chtít	k5eAaImAgFnS	chtít
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opravdu	opravdu	k6eAd1	opravdu
se	se	k3xPyFc4	se
necítím	cítit	k5eNaImIp1nS	cítit
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
i	i	k9	i
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
nadcházející	nadcházející	k2eAgNnSc4d1	nadcházející
finále	finále	k1gNnSc4	finále
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
letím	letět	k5eAaImIp1nS	letět
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Odstoupení	odstoupení	k1gNnSc1	odstoupení
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pokles	pokles	k1gInSc4	pokles
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
pak	pak	k6eAd1	pak
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
areně	areeň	k1gFnSc2	areeň
<g/>
]	]	kIx)	]
exhibici	exhibice	k1gFnSc4	exhibice
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Šarapovovou	Šarapovová	k1gFnSc7	Šarapovová
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
dvou	dva	k4xCgInPc2	dva
setů	set	k1gInPc2	set
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
k	k	k7c3	k
jedinému	jediné	k1gNnSc3	jediné
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
prohrála	prohrát	k5eAaPmAgFnS	prohrát
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
ji	on	k3xPp3gFnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Lucie	Lucie	k1gFnSc1	Lucie
Šafářová	Šafářová	k1gFnSc1	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
finále	finále	k1gNnSc4	finále
světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
proti	proti	k7c3	proti
Jeleně	Jelena	k1gFnSc3	Jelena
Jankovićové	Jankovićová	k1gFnSc2	Jankovićová
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
týmových	týmový	k2eAgFnPc2d1	týmová
jedniček	jednička	k1gFnPc2	jednička
však	však	k9	však
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Ivanovićovou	Ivanovićová	k1gFnSc4	Ivanovićová
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
výběr	výběr	k1gInSc1	výběr
kapitána	kapitán	k1gMnSc2	kapitán
Petra	Petr	k1gMnSc2	Petr
Pály	Pála	k1gMnSc2	Pála
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k9	tak
obhájil	obhájit	k5eAaPmAgMnS	obhájit
vítězství	vítězství	k1gNnSc4	vítězství
pro	pro	k7c4	pro
výhře	výhra	k1gFnSc6	výhra
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zakončila	zakončit	k5eAaPmAgFnS	zakončit
těsně	těsně	k6eAd1	těsně
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
Li	li	k8xS	li
Na	na	k7c4	na
a	a	k8xC	a
15	[number]	k4	15
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
Erraniovou	Erraniový	k2eAgFnSc4d1	Erraniová
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
5510	[number]	k4	5510
bodů	bod	k1gInPc2	bod
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
sezóna	sezóna	k1gFnSc1	sezóna
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úvodní	úvodní	k2eAgFnSc6d1	úvodní
turnajích	turnaj	k1gInPc6	turnaj
v	v	k7c6	v
Brisbane	Brisban	k1gMnSc5	Brisban
a	a	k8xC	a
v	v	k7c4	v
Sydney	Sydney	k1gNnSc4	Sydney
dohrála	dohrát	k5eAaPmAgFnS	dohrát
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Australské	australský	k2eAgNnSc1d1	Australské
trápení	trápení	k1gNnSc1	trápení
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
grandslamu	grandslam	k1gInSc6	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
už	už	k6eAd1	už
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
osmnáctileté	osmnáctiletý	k2eAgFnSc2d1	osmnáctiletá
Britky	Britka	k1gFnSc2	Britka
Laury	Laura	k1gFnSc2	Laura
Robsonové	Robsonová	k1gFnSc2	Robsonová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
dva	dva	k4xCgInPc4	dva
míčky	míček	k1gInPc4	míček
od	od	k7c2	od
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
..	..	k?	..
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Open	Opena	k1gFnPc2	Opena
GDF	GDF	kA	GDF
Suez	Suez	k1gInSc1	Suez
hraném	hraný	k2eAgNnSc6d1	hrané
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
Qatar	Qatara	k1gFnPc2	Qatara
Ladies	Ladies	k1gMnSc1	Ladies
Open	Open	k1gMnSc1	Open
v	v	k7c4	v
Dauhá	Dauhý	k2eAgNnPc4d1	Dauhý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
sebrala	sebrat	k5eAaPmAgFnS	sebrat
set	sto	k4xCgNnPc2	sto
Sereně	Serena	k1gFnSc3	Serena
Williamsové	Williamsové	k2eAgMnPc1d1	Williamsové
<g/>
.	.	kIx.	.
</s>
<s>
Skvěle	skvěle	k6eAd1	skvěle
rozehrané	rozehraný	k2eAgNnSc1d1	rozehrané
utkaní	utkaný	k2eAgMnPc1d1	utkaný
ale	ale	k9	ale
ztratila	ztratit	k5eAaPmAgFnS	ztratit
poměrem	poměr	k1gInSc7	poměr
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sadě	sada	k1gFnSc6	sada
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vedla	vést	k5eAaImAgFnS	vést
již	již	k6eAd1	již
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Američanka	Američanka	k1gFnSc1	Američanka
se	se	k3xPyFc4	se
výhrou	výhra	k1gFnSc7	výhra
stala	stát	k5eAaPmAgFnS	stát
nejstarší	starý	k2eAgFnSc1d3	nejstarší
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Porážku	porážka	k1gFnSc4	porážka
si	se	k3xPyFc3	se
vynahradila	vynahradit	k5eAaPmAgFnS	vynahradit
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
turnaji	turnaj	k1gInSc6	turnaj
Dubai	Duba	k1gFnSc2	Duba
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
arabských	arabský	k2eAgInPc6d1	arabský
emirátech	emirát	k1gInPc6	emirát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postupně	postupně	k6eAd1	postupně
porazila	porazit	k5eAaPmAgFnS	porazit
Hantuchovou	Hantuchův	k2eAgFnSc7d1	Hantuchova
<g/>
,	,	kIx,	,
Ivanovićovou	Ivanovićová	k1gFnSc7	Ivanovićová
<g/>
,	,	kIx,	,
obhájkyni	obhájkyně	k1gFnSc3	obhájkyně
titulu	titul	k1gInSc2	titul
Radwańskou	Radwańský	k2eAgFnSc7d1	Radwańský
<g/>
,	,	kIx,	,
<g/>
Wozniackou	Wozniacký	k2eAgFnSc7d1	Wozniacká
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Erraniovou	Erraniový	k2eAgFnSc4d1	Erraniová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
tak	tak	k9	tak
desátý	desátý	k4xOgInSc4	desátý
titul	titul	k1gInSc4	titul
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
a	a	k8xC	a
první	první	k4xOgInSc4	první
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc4	Wells
Masters	Masters	k1gInSc4	Masters
došla	dojít	k5eAaPmAgFnS	dojít
jako	jako	k9	jako
pátá	pátá	k1gFnSc1	pátá
nasazená	nasazený	k2eAgFnSc1d1	nasazená
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
vylepšila	vylepšit	k5eAaPmAgFnS	vylepšit
místní	místní	k2eAgNnSc4d1	místní
maximum	maximum	k1gNnSc4	maximum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
však	však	k9	však
neudržela	udržet	k5eNaPmAgFnS	udržet
vedení	vedení	k1gNnSc4	vedení
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
4-2	[number]	k4	4-2
proti	proti	k7c3	proti
Rusce	Ruska	k1gFnSc3	Ruska
Marii	Maria	k1gFnSc4	Maria
Kirilenkové	Kirilenkový	k2eAgInPc4d1	Kirilenkový
a	a	k8xC	a
prohrála	prohrát	k5eAaPmAgFnS	prohrát
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Wickmayerovou	Wickmayerová	k1gFnSc7	Wickmayerová
zápas	zápas	k1gInSc4	zápas
prvního	první	k4xOgNnSc2	první
kola	kolo	k1gNnPc4	kolo
proti	proti	k7c3	proti
Čanové	Čanové	k2eAgInPc3d1	Čanové
a	a	k8xC	a
Husárové	Husárový	k2eAgInPc1d1	Husárový
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
turnaji	turnaj	k1gInSc6	turnaj
kategorie	kategorie	k1gFnPc4	kategorie
Premier	Premira	k1gFnPc2	Premira
Mandatory	Mandator	k1gInPc4	Mandator
Sony	Sony	kA	Sony
Open	Open	k1gInSc4	Open
Tennis	Tennis	k1gFnPc2	Tennis
hraném	hraný	k2eAgNnSc6d1	hrané
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
prohrála	prohrát	k5eAaPmAgFnS	prohrát
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
s	s	k7c7	s
Belgičankou	Belgičanka	k1gFnSc7	Belgičanka
Kirsten	Kirsten	k2eAgInSc1d1	Kirsten
Flipkensovou	Flipkensová	k1gFnSc7	Flipkensová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
nadělila	nadělit	k5eAaPmAgFnS	nadělit
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
setu	set	k1gInSc6	set
"	"	kIx"	"
<g/>
kanára	kanár	k1gMnSc2	kanár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
startovala	startovat	k5eAaBmAgFnS	startovat
jako	jako	k9	jako
turnajová	turnajový	k2eAgFnSc1d1	turnajová
jednička	jednička	k1gFnSc1	jednička
na	na	k7c6	na
premiérovém	premiérový	k2eAgInSc6d1	premiérový
ročníku	ročník	k1gInSc6	ročník
polského	polský	k2eAgInSc2d1	polský
BNP	BNP	kA	BNP
Paribas	Paribas	k1gMnSc1	Paribas
Katowice	Katowice	k1gFnSc2	Katowice
Open	Opena	k1gFnPc2	Opena
hraném	hraný	k2eAgNnSc6d1	hrané
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pronikla	proniknout	k5eAaPmAgFnS	proniknout
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
prvního	první	k4xOgNnSc2	první
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podlehla	podlehnout	k5eAaPmAgNnP	podlehnout
druhé	druhý	k4xOgFnSc6	druhý
nasazené	nasazený	k2eAgFnSc6d1	nasazená
Italce	Italka	k1gFnSc6	Italka
Vinciové	Vinciový	k2eAgFnSc6d1	Vinciová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
a	a	k8xC	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
prohrála	prohrát	k5eAaPmAgFnS	prohrát
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
od	od	k7c2	od
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
Eastbourne	Eastbourn	k1gInSc5	Eastbourn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
antukové	antukový	k2eAgFnPc4d1	antuková
události	událost	k1gFnPc4	událost
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
<g/>
,	,	kIx,	,
stuttgartském	stuttgartský	k2eAgNnSc6d1	stuttgartské
Porsche	Porsche	k1gNnSc6	Porsche
Tennis	Tennis	k1gFnSc1	Tennis
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
světová	světový	k2eAgFnSc1d1	světová
pětka	pětka	k1gFnSc1	pětka
Li	li	k8xS	li
Na	na	k7c6	na
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
události	událost	k1gFnSc6	událost
Mutua	Mutu	k1gInSc2	Mutu
Madrid	Madrid	k1gInSc4	Madrid
Open	Openo	k1gNnPc2	Openo
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premiero	k1gNnPc2	Premiero
Mandatory	Mandator	k1gInPc7	Mandator
došla	dojít	k5eAaPmAgFnS	dojít
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
Daniely	Daniela	k1gFnSc2	Daniela
Hantuchové	Hantuchový	k2eAgFnSc2d1	Hantuchová
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
startovala	startovat	k5eAaBmAgFnS	startovat
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
Ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
ale	ale	k8xC	ale
nestačila	stačit	k5eNaBmAgFnS	stačit
Australanku	Australanka	k1gFnSc4	Australanka
Stosurovou	stosurový	k2eAgFnSc4d1	stosurový
po	po	k7c6	po
třísetovém	třísetový	k2eAgInSc6d1	třísetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pařížský	pařížský	k2eAgInSc4d1	pařížský
grandslam	grandslam	k1gInSc4	grandslam
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
přijížděla	přijíždět	k5eAaImAgFnS	přijíždět
v	v	k7c6	v
roli	role	k1gFnSc6	role
sedmé	sedmý	k4xOgFnSc2	sedmý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
a	a	k8xC	a
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
semifinálovou	semifinálový	k2eAgFnSc4d1	semifinálová
účast	účast	k1gFnSc4	účast
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
fázi	fáze	k1gFnSc6	fáze
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
stejně	stejně	k6eAd1	stejně
staré	starý	k2eAgFnSc2d1	stará
Američanky	Američanka	k1gFnSc2	Američanka
Jamie	Jamie	k1gFnSc2	Jamie
Hamptonové	Hamptonový	k2eAgFnSc2d1	Hamptonová
výsledkem	výsledek	k1gInSc7	výsledek
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Travnatou	travnatý	k2eAgFnSc4d1	travnatá
sezónu	sezóna	k1gFnSc4	sezóna
jako	jako	k8xC	jako
už	už	k6eAd1	už
tradičně	tradičně	k6eAd1	tradičně
zahájila	zahájit	k5eAaPmAgFnS	zahájit
na	na	k7c4	na
AEGON	AEGON	kA	AEGON
International	International	k1gFnSc2	International
v	v	k7c6	v
Eastbourne	Eastbourn	k1gInSc5	Eastbourn
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
turnajová	turnajový	k2eAgFnSc1d1	turnajová
čtyřka	čtyřka	k1gFnSc1	čtyřka
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
prohrála	prohrát	k5eAaPmAgFnS	prohrát
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
kamarádkou	kamarádka	k1gFnSc7	kamarádka
a	a	k8xC	a
občasnou	občasný	k2eAgFnSc7d1	občasná
partnerkou	partnerka	k1gFnSc7	partnerka
do	do	k7c2	do
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
Wickmayerovou	Wickmayerová	k1gFnSc4	Wickmayerová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
setu	set	k1gInSc6	set
ujala	ujmout	k5eAaPmAgFnS	ujmout
vedení	vedení	k1gNnSc3	vedení
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
30	[number]	k4	30
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
podání	podání	k1gNnSc6	podání
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
s	s	k7c7	s
Wickmayerovou	Wickmayerová	k1gFnSc7	Wickmayerová
obdržely	obdržet	k5eAaPmAgFnP	obdržet
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
do	do	k7c2	do
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
ale	ale	k8xC	ale
prohrály	prohrát	k5eAaPmAgInP	prohrát
s	s	k7c7	s
nenasazenou	nasazený	k2eNgFnSc7d1	nenasazená
dvojicí	dvojice	k1gFnSc7	dvojice
Su-wej	Suej	k1gInSc1	Su-wej
Sieová	Sieová	k1gFnSc1	Sieová
a	a	k8xC	a
Mirjana	Mirjana	k1gFnSc1	Mirjana
Lučićová	Lučićová	k1gFnSc1	Lučićová
Baroniová	Baroniový	k2eAgFnSc1d1	Baroniový
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nejslavnějšího	slavný	k2eAgInSc2d3	nejslavnější
turnaje	turnaj	k1gInSc2	turnaj
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
jako	jako	k9	jako
osmá	osmý	k4xOgFnSc1	osmý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypadnutí	vypadnutí	k1gNnSc6	vypadnutí
velkých	velký	k2eAgFnPc2d1	velká
favoritek	favoritka	k1gFnPc2	favoritka
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
šance	šance	k1gFnPc1	šance
na	na	k7c4	na
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
po	po	k7c6	po
třísetovém	třísetový	k2eAgInSc6d1	třísetový
průběhu	průběh	k1gInSc6	průběh
s	s	k7c7	s
belgickou	belgický	k2eAgFnSc7d1	belgická
tenistkou	tenistka	k1gFnSc7	tenistka
Kirsten	Kirsten	k2eAgInSc4d1	Kirsten
Flipkensovou	Flipkensová	k1gFnSc7	Flipkensová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
titulu	titul	k1gInSc2	titul
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c6	na
srpnovém	srpnový	k2eAgInSc6d1	srpnový
Canada	Canada	k1gFnSc1	Canada
Masters	Masters	k1gInSc1	Masters
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
nenasazená	nasazený	k2eNgFnSc1d1	nenasazená
Rumunka	Rumunka	k1gFnSc1	Rumunka
Sorana	Sorana	k1gFnSc1	Sorana
Cîrsteaová	Cîrsteaová	k1gFnSc1	Cîrsteaová
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
bodů	bod	k1gInPc2	bod
znamenala	znamenat	k5eAaImAgFnS	znamenat
pád	pád	k1gInSc4	pád
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
třetí	třetí	k4xOgFnSc1	třetí
nasazená	nasazený	k2eAgFnSc1d1	nasazená
se	se	k3xPyFc4	se
druhý	druhý	k4xOgInSc1	druhý
rok	rok	k1gInSc1	rok
za	za	k7c4	za
sebou	se	k3xPyFc7	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
New	New	k1gMnSc2	New
Haven	Havno	k1gNnPc2	Havno
Open	Openo	k1gNnPc2	Openo
at	at	k?	at
Yale	Yal	k1gFnSc2	Yal
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
však	však	k8xC	však
hladce	hladko	k6eAd1	hladko
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
další	další	k2eAgFnSc1d1	další
rumunské	rumunský	k2eAgFnSc3d1	rumunská
hráčce	hráčka	k1gFnSc3	hráčka
Simoně	Simona	k1gFnSc3	Simona
Halepové	Halepová	k1gFnSc3	Halepová
<g/>
.	.	kIx.	.
</s>
<s>
Bodová	bodový	k2eAgFnSc1d1	bodová
ztráta	ztráta	k1gFnSc1	ztráta
po	po	k7c6	po
neobhájení	neobhájení	k1gNnSc6	neobhájení
titulu	titul	k1gInSc2	titul
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pád	pád	k1gInSc4	pád
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
grandslamovém	grandslamový	k2eAgNnSc6d1	grandslamové
US	US	kA	US
Open	Open	k1gInSc4	Open
pak	pak	k6eAd1	pak
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
po	po	k7c6	po
hladkém	hladký	k2eAgInSc6d1	hladký
průběhu	průběh	k1gInSc6	průběh
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
s	s	k7c7	s
81	[number]	k4	81
<g/>
.	.	kIx.	.
tenistkou	tenistka	k1gFnSc7	tenistka
klasifikace	klasifikace	k1gFnSc2	klasifikace
Alison	Alisona	k1gFnPc2	Alisona
Riskeovou	Riskeův	k2eAgFnSc7d1	Riskeův
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
nevytvořila	vytvořit	k5eNaPmAgFnS	vytvořit
žádnou	žádný	k3yNgFnSc4	žádný
breakovou	breakový	k2eAgFnSc4d1	breaková
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turnaje	turnaj	k1gInSc2	turnaj
trpěla	trpět	k5eAaImAgFnS	trpět
vysokými	vysoký	k2eAgFnPc7d1	vysoká
horečkami	horečka	k1gFnPc7	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
grandslamu	grandslam	k1gInSc6	grandslam
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
z	z	k7c2	z
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
hráček	hráčka	k1gFnPc2	hráčka
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
patřilo	patřit	k5eAaImAgNnS	patřit
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Premiérový	premiérový	k2eAgInSc1d1	premiérový
tokijský	tokijský	k2eAgInSc1d1	tokijský
titul	titul	k1gInSc1	titul
z	z	k7c2	z
Toray	Toraa	k1gFnSc2	Toraa
Pan	Pan	k1gMnSc1	Pan
Pacific	Pacific	k1gMnSc1	Pacific
Open	Open	k1gMnSc1	Open
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Premier	Premier	k1gInSc4	Premier
5	[number]	k4	5
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
semifinálové	semifinálový	k2eAgFnSc6d1	semifinálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
bývalou	bývalý	k2eAgFnSc7d1	bývalá
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
Venus	Venus	k1gMnSc1	Venus
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Němkou	Němka	k1gFnSc7	Němka
Angelique	Angelique	k1gFnSc7	Angelique
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
.	.	kIx.	.
</s>
<s>
Bodový	bodový	k2eAgInSc1d1	bodový
zisk	zisk	k1gInSc1	zisk
znamenal	znamenat	k5eAaImAgInS	znamenat
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
navazujícím	navazující	k2eAgInSc6d1	navazující
pekingském	pekingský	k2eAgInSc6d1	pekingský
China	China	k1gFnSc1	China
Open	Opena	k1gFnPc2	Opena
se	se	k3xPyFc4	se
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
čtvrtou	čtvrtá	k1gFnSc7	čtvrtá
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Li	li	k8xS	li
Na	na	k7c6	na
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
nad	nad	k7c4	nad
její	její	k3xOp3gFnPc4	její
síly	síla	k1gFnPc4	síla
Srbka	Srbka	k1gFnSc1	Srbka
Jelena	Jelena	k1gFnSc1	Jelena
Jankovićová	Jankovićová	k1gFnSc1	Jankovićová
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poraněným	poraněný	k2eAgNnPc3d1	poraněné
zádům	záda	k1gNnPc3	záda
a	a	k8xC	a
únavě	únava	k1gFnSc3	únava
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
z	z	k7c2	z
lineckého	linecký	k2eAgInSc2d1	linecký
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
sezóně	sezóna	k1gFnSc6	sezóna
tak	tak	k6eAd1	tak
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
již	již	k6eAd1	již
do	do	k7c2	do
třiceti	třicet	k4xCc2	třicet
šesti	šest	k4xCc2	šest
třísetových	třísetový	k2eAgInPc2d1	třísetový
duelů	duel	k1gInPc2	duel
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
rekord	rekord	k1gInSc4	rekord
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgInPc4d1	patřící
Patty	Patt	k1gInPc4	Patt
Schnyderové	Schnyderový	k2eAgInPc4d1	Schnyderový
ze	z	k7c2	z
sezóny	sezóna	k1gFnSc2	sezóna
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Kvitová	Kvitová	k1gFnSc1	Kvitová
jich	on	k3xPp3gInPc2	on
dovedla	dovést	k5eAaPmAgFnS	dovést
do	do	k7c2	do
vítězného	vítězný	k2eAgInSc2d1	vítězný
konce	konec	k1gInSc2	konec
dvacet	dvacet	k4xCc4	dvacet
čtyři	čtyři	k4xCgNnPc1	čtyři
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
nový	nový	k2eAgInSc4d1	nový
rekordní	rekordní	k2eAgInSc4d1	rekordní
počet	počet	k1gInSc4	počet
třísetových	třísetový	k2eAgFnPc2d1	třísetová
výher	výhra	k1gFnPc2	výhra
na	na	k7c6	na
ženském	ženský	k2eAgInSc6d1	ženský
okruhu	okruh	k1gInSc6	okruh
během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rekord	rekord	k1gInSc1	rekord
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezóny	sezóna	k1gFnSc2	sezóna
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
během	během	k7c2	během
istanbulského	istanbulský	k2eAgInSc2d1	istanbulský
turnaje	turnaj	k1gInSc2	turnaj
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
třísetovou	třísetový	k2eAgFnSc4d1	třísetová
výhru	výhra	k1gFnSc4	výhra
na	na	k7c4	na
25	[number]	k4	25
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
Turnaj	turnaj	k1gInSc4	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
hraný	hraný	k2eAgMnSc1d1	hraný
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
přijížděla	přijíždět	k5eAaImAgFnS	přijíždět
jako	jako	k9	jako
šestá	šestý	k4xOgFnSc1	šestý
hráčka	hráčka	k1gFnSc1	hráčka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
nejprve	nejprve	k6eAd1	nejprve
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Agnieszku	Agnieszka	k1gFnSc4	Agnieszka
Radwańskou	Radwańský	k2eAgFnSc4d1	Radwańský
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Sereně	Serena	k1gFnSc3	Serena
Williamsové	Williamsová	k1gFnSc3	Williamsová
a	a	k8xC	a
třísetová	třísetový	k2eAgFnSc1d1	třísetová
výhra	výhra	k1gFnSc1	výhra
nad	nad	k7c7	nad
Angelique	Angelique	k1gFnSc7	Angelique
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
jí	jíst	k5eAaImIp3nS	jíst
zajistila	zajistit	k5eAaPmAgFnS	zajistit
postup	postup	k1gInSc4	postup
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podlehla	podlehnout	k5eAaPmAgNnP	podlehnout
Li	li	k9	li
Na	na	k7c6	na
po	po	k7c6	po
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zakončila	zakončit	k5eAaPmAgFnS	zakončit
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
jako	jako	k9	jako
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
boku	bok	k1gInSc6	bok
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
její	její	k3xOp3gNnPc4	její
přítel	přítel	k1gMnSc1	přítel
Radek	Radek	k1gMnSc1	Radek
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
dvouhry	dvouhra	k1gFnPc4	dvouhra
nad	nad	k7c7	nad
Cornetovou	Cornetový	k2eAgFnSc7d1	Cornetová
<g/>
,	,	kIx,	,
Medinaovou	Medinaový	k2eAgFnSc7d1	Medinaová
Garriguesovou	Garriguesový	k2eAgFnSc7d1	Garriguesová
a	a	k8xC	a
Stephensovou	Stephensová	k1gFnSc7	Stephensová
<g/>
,	,	kIx,	,
družstvo	družstvo	k1gNnSc1	družstvo
skončilo	skončit	k5eAaPmAgNnS	skončit
na	na	k7c6	na
nepostupovém	postupový	k2eNgMnSc6d1	nepostupový
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
událostí	událost	k1gFnSc7	událost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
lednový	lednový	k2eAgMnSc1d1	lednový
Apia	Apia	k1gMnSc1	Apia
International	International	k1gMnSc2	International
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
turnajová	turnajový	k2eAgFnSc1d1	turnajová
dvojka	dvojka	k1gFnSc1	dvojka
prošla	projít	k5eAaPmAgFnS	projít
přes	přes	k7c4	přes
Lucii	Lucie	k1gFnSc4	Lucie
Šafářovou	Šafářová	k1gFnSc4	Šafářová
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
107	[number]	k4	107
<g/>
.	.	kIx.	.
hráčku	hráčka	k1gFnSc4	hráčka
klasifikace	klasifikace	k1gFnSc2	klasifikace
a	a	k8xC	a
kvalifikantku	kvalifikantka	k1gFnSc4	kvalifikantka
Cvetanu	Cvetan	k1gInSc2	Cvetan
Pironkovovou	Pironkovová	k1gFnSc4	Pironkovová
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
šesté	šestý	k4xOgFnSc6	šestý
nasazené	nasazený	k2eAgFnPc1d1	nasazená
skončil	skončit	k5eAaPmAgInS	skončit
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
překvapivě	překvapivě	k6eAd1	překvapivě
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
s	s	k7c7	s
88	[number]	k4	88
<g/>
.	.	kIx.	.
tenistkou	tenistka	k1gFnSc7	tenistka
žebříčku	žebříček	k1gInSc2	žebříček
Luksiku	Luksika	k1gFnSc4	Luksika
Kumkhumovou	Kumkhumová	k1gFnSc4	Kumkhumová
z	z	k7c2	z
Thajska	Thajsko	k1gNnSc2	Thajsko
poměrem	poměr	k1gInSc7	poměr
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
pařížského	pařížský	k2eAgInSc2d1	pařížský
Open	Open	k1gInSc1	Open
GDF	GDF	kA	GDF
Suez	Suez	k1gInSc4	Suez
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
pro	pro	k7c4	pro
respirační	respirační	k2eAgNnSc4d1	respirační
onemocnění	onemocnění	k1gNnSc4	onemocnění
a	a	k8xC	a
nestihla	stihnout	k5eNaPmAgFnS	stihnout
tak	tak	k6eAd1	tak
ani	ani	k8xC	ani
první	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
proti	proti	k7c3	proti
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
zavítala	zavítat	k5eAaPmAgFnS	zavítat
na	na	k7c4	na
Qatar	Qatar	k1gInSc4	Qatar
Total	totat	k5eAaImAgInS	totat
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
třetí	třetí	k4xOgFnSc1	třetí
nasazená	nasazený	k2eAgFnSc1d1	nasazená
sehrála	sehrát	k5eAaPmAgFnS	sehrát
<g/>
,	,	kIx,	,
po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
<g/>
,	,	kIx,	,
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
bitvu	bitva	k1gFnSc4	bitva
s	s	k7c7	s
Venus	Venus	k1gMnSc1	Venus
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vyšla	vyjít	k5eAaPmAgFnS	vyjít
vítězně	vítězně	k6eAd1	vítězně
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
setu	set	k1gInSc6	set
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c6	na
prahu	práh	k1gInSc6	práh
vyřazení	vyřazení	k1gNnSc2	vyřazení
<g/>
,	,	kIx,	,
když	když	k8xS	když
měla	mít	k5eAaImAgFnS	mít
Američanka	Američanka	k1gFnSc1	Američanka
breakovou	breakový	k2eAgFnSc4d1	breaková
příležitost	příležitost	k1gFnSc4	příležitost
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
gamů	game	k1gInPc2	game
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
pak	pak	k6eAd1	pak
Češka	Češka	k1gFnSc1	Češka
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
mečbol	mečbol	k1gInSc4	mečbol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
střetnutí	střetnutí	k1gNnSc6	střetnutí
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
porazila	porazit	k5eAaPmAgFnS	porazit
Lucii	Lucie	k1gFnSc4	Lucie
Šafářovou	Šafářová	k1gFnSc4	Šafářová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gFnSc1	její
cesta	cesta	k1gFnSc1	cesta
pavoukem	pavouk	k1gMnSc7	pavouk
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
světové	světový	k2eAgFnSc2d1	světová
osmičky	osmička	k1gFnSc2	osmička
Jeleny	Jelena	k1gFnSc2	Jelena
Jankovićové	Jankovićová	k1gFnSc2	Jankovićová
<g/>
,	,	kIx,	,
když	když	k8xS	když
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
čtyři	čtyři	k4xCgFnPc1	čtyři
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
navazujícím	navazující	k2eAgInSc6d1	navazující
Dubai	Duba	k1gInSc6	Duba
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
odešla	odejít	k5eAaPmAgFnS	odejít
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
úvodního	úvodní	k2eAgInSc2d1	úvodní
zápasu	zápas	k1gInSc2	zápas
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
těsně	těsně	k6eAd1	těsně
poražena	porazit	k5eAaPmNgFnS	porazit
od	od	k7c2	od
šestnácté	šestnáctý	k4xOgFnSc2	šestnáctý
ženy	žena	k1gFnSc2	žena
klasifikace	klasifikace	k1gFnSc2	klasifikace
Carly	Carla	k1gMnSc2	Carla
Suárezové	Suárezové	k2eAgInSc2d1	Suárezové
Navarrové	Navarrové	k2eAgInSc7d1	Navarrové
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
březnové	březnový	k2eAgFnSc6d1	březnová
události	událost	k1gFnSc6	událost
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Masters	Masters	k1gInSc1	Masters
jí	on	k3xPp3gFnSc7	on
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Světlanou	světlaný	k2eAgFnSc7d1	světlaný
Kuzněcovovou	Kuzněcovová	k1gFnSc7	Kuzněcovová
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
zastavila	zastavit	k5eAaPmAgFnS	zastavit
úřadující	úřadující	k2eAgFnSc1d1	úřadující
finalistka	finalistka	k1gFnSc1	finalistka
Australian	Australian	k1gInSc1	Australian
Open	Open	k1gInSc1	Open
Dominika	Dominik	k1gMnSc2	Dominik
Cibulková	Cibulková	k1gFnSc1	Cibulková
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
antukový	antukový	k2eAgInSc1d1	antukový
Porsche	Porsche	k1gNnSc1	Porsche
Tennis	Tennis	k1gInSc1	Tennis
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
s	s	k7c7	s
Ruskou	Ruska	k1gFnSc7	Ruska
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
stovky	stovka	k1gFnSc2	stovka
žebříčku	žebříček	k1gInSc2	žebříček
Alisou	Alisý	k2eAgFnSc7d1	Alisý
Klejbanovovou	Klejbanovová	k1gFnSc7	Klejbanovová
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšení	zlepšení	k1gNnSc1	zlepšení
formy	forma	k1gFnSc2	forma
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
Mutua	Mutuus	k1gMnSc4	Mutuus
Madrid	Madrid	k1gInSc4	Madrid
Open	Open	k1gNnSc4	Open
<g/>
,	,	kIx,	,
turnaji	turnaj	k1gInSc6	turnaj
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premiero	k1gNnPc2	Premiero
Mandatory	Mandator	k1gInPc7	Mandator
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nenašla	najít	k5eNaPmAgFnS	najít
recept	recept	k1gInSc4	recept
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
pětku	pětka	k1gFnSc4	pětka
Simonu	Simona	k1gFnSc4	Simona
Halepovou	Halepová	k1gFnSc4	Halepová
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Rome	Rom	k1gMnSc5	Rom
Masters	Mastersa	k1gFnPc2	Mastersa
představoval	představovat	k5eAaImAgInS	představovat
další	další	k2eAgNnSc4d1	další
vyřazení	vyřazení	k1gNnSc4	vyřazení
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
úvodním	úvodní	k2eAgInSc6d1	úvodní
duelu	duel	k1gInSc6	duel
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
s	s	k7c7	s
Číňankou	Číňanka	k1gFnSc7	Číňanka
Čang	Čang	k1gMnSc1	Čang
Šuaj	Šuaj	k1gMnSc1	Šuaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Marinou	Marina	k1gFnSc7	Marina
Erakovicovou	Erakovicový	k2eAgFnSc7d1	Erakovicová
na	na	k7c6	na
French	Fren	k1gFnPc6	Fren
Open	Opena	k1gFnPc2	Opena
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
fázi	fáze	k1gFnSc6	fáze
nad	nad	k7c4	nad
její	její	k3xOp3gFnPc4	její
síly	síla	k1gFnPc4	síla
houževnatá	houževnatý	k2eAgFnSc1d1	houževnatá
27	[number]	k4	27
<g/>
.	.	kIx.	.
nasazená	nasazený	k2eAgFnSc1d1	nasazená
Světlana	Světlana	k1gFnSc1	Světlana
Kuzněcovová	Kuzněcovová	k1gFnSc1	Kuzněcovová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
turnajové	turnajový	k2eAgFnSc2d1	turnajová
šestky	šestka	k1gFnSc2	šestka
svedla	svést	k5eAaPmAgFnS	svést
s	s	k7c7	s
Ruskou	Ruska	k1gFnSc7	Ruska
dramatický	dramatický	k2eAgInSc4d1	dramatický
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
až	až	k8xS	až
koncovka	koncovka	k1gFnSc1	koncovka
třetí	třetí	k4xOgFnSc2	třetí
sady	sada	k1gFnSc2	sada
nepříznivým	příznivý	k2eNgInSc7d1	nepříznivý
poměrem	poměr	k1gInSc7	poměr
gamů	game	k1gInPc2	game
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Travnatou	travnatý	k2eAgFnSc4d1	travnatá
část	část	k1gFnSc4	část
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
na	na	k7c4	na
AEGON	AEGON	kA	AEGON
International	International	k1gFnSc2	International
v	v	k7c6	v
Eastbourne	Eastbourn	k1gInSc5	Eastbourn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
porazila	porazit	k5eAaPmAgFnS	porazit
Lucii	Lucie	k1gFnSc3	Lucie
Šafářovou	Šafářův	k2eAgFnSc7d1	Šafářova
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vyrovnaném	vyrovnaný	k2eAgInSc6d1	vyrovnaný
třísetovém	třísetový	k2eAgInSc6d1	třísetový
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
až	až	k9	až
tiebreak	tiebreak	k1gInSc1	tiebreak
třetího	třetí	k4xOgInSc2	třetí
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
přehrála	přehrát	k5eAaPmAgFnS	přehrát
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
americkou	americký	k2eAgFnSc4d1	americká
tenistku	tenistka	k1gFnSc4	tenistka
Varvaru	Varvar	k1gInSc2	Varvar
Lepčenkovou	Lepčenkový	k2eAgFnSc7d1	Lepčenková
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
proti	proti	k7c3	proti
Britce	Britka	k1gFnSc3	Britka
Heather	Heathra	k1gFnPc2	Heathra
Watsonové	Watson	k1gMnPc1	Watson
již	již	k6eAd1	již
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
nenastoupila	nastoupit	k5eNaPmAgFnS	nastoupit
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
šetřit	šetřit	k5eAaImF	šetřit
síly	síla	k1gFnPc1	síla
na	na	k7c4	na
nadcházející	nadcházející	k2eAgInSc4d1	nadcházející
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
nasazené	nasazený	k2eAgFnSc2d1	nasazená
šestky	šestka	k1gFnSc2	šestka
<g/>
.	.	kIx.	.
</s>
<s>
Poraženým	poražený	k2eAgFnPc3d1	poražená
soupeřkám	soupeřka	k1gFnPc3	soupeřka
v	v	k7c6	v
úvodních	úvodní	k2eAgNnPc6d1	úvodní
dvou	dva	k4xCgNnPc6	dva
kolech	kolo	k1gNnPc6	kolo
Andree	Andrea	k1gFnSc3	Andrea
Hlaváčkové	Hlaváčkové	k2eAgMnSc2d1	Hlaváčkové
i	i	k8xC	i
Němce	Němec	k1gMnSc2	Němec
Moně	Moň	k1gMnSc2	Moň
Barthelové	Barthelová	k1gFnSc2	Barthelová
uštědřila	uštědřit	k5eAaPmAgFnS	uštědřit
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
"	"	kIx"	"
<g/>
kanáru	kanár	k1gMnSc6	kanár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
zdolala	zdolat	k5eAaPmAgFnS	zdolat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
30	[number]	k4	30
<g/>
.	.	kIx.	.
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Venus	Venus	k1gInSc1	Venus
Williamsovou	Williamsová	k1gFnSc4	Williamsová
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
a	a	k8xC	a
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvyrovnanější	vyrovnaný	k2eAgInSc4d3	nejvyrovnanější
duel	duel	k1gInSc4	duel
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
dva	dva	k4xCgInPc4	dva
míče	míč	k1gInPc4	míč
od	od	k7c2	od
vyřazení	vyřazení	k1gNnSc2	vyřazení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
porazila	porazit	k5eAaPmAgFnS	porazit
Číňanku	Číňanka	k1gFnSc4	Číňanka
Šuaj	Šuaj	k1gFnSc4	Šuaj
Pchengovou	Pchengový	k2eAgFnSc4d1	Pchengová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
osmičkou	osmička	k1gFnSc7	osmička
hráček	hráčka	k1gFnPc2	hráčka
figurovaly	figurovat	k5eAaImAgInP	figurovat
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
polovině	polovina	k1gFnSc6	polovina
pavouka	pavouk	k1gMnSc2	pavouk
tři	tři	k4xCgFnPc4	tři
Češky	Češka	k1gFnPc1	Češka
<g/>
.	.	kIx.	.
</s>
<s>
Kvitová	Kvitová	k1gFnSc1	Kvitová
tak	tak	k9	tak
mohla	moct	k5eAaImAgFnS	moct
dvakrát	dvakrát	k6eAd1	dvakrát
narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
krajanku	krajanka	k1gFnSc4	krajanka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
porazila	porazit	k5eAaPmAgFnS	porazit
Barboru	Barbora	k1gFnSc4	Barbora
Záhlavovou-Strýcovou	Záhlavovou-Strýcový	k2eAgFnSc4d1	Záhlavovou-Strýcový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
pak	pak	k6eAd1	pak
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Lucii	Lucie	k1gFnSc4	Lucie
Šafářovou	Šafářová	k1gFnSc4	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnanou	vyrovnaný	k2eAgFnSc4d1	vyrovnaná
úvodní	úvodní	k2eAgFnSc4d1	úvodní
sadu	sada	k1gFnSc4	sada
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
až	až	k8xS	až
tiebreakem	tiebreak	k1gInSc7	tiebreak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poté	poté	k6eAd1	poté
již	již	k6eAd1	již
na	na	k7c6	na
dvorci	dvorec	k1gInSc6	dvorec
dominovala	dominovat	k5eAaImAgFnS	dominovat
<g/>
.	.	kIx.	.
</s>
<s>
Šafářovou	Šafářová	k1gFnSc4	Šafářová
tak	tak	k6eAd1	tak
zdolala	zdolat	k5eAaPmAgFnS	zdolat
popáté	popáté	k4xO	popáté
v	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
sezóně	sezóna	k1gFnSc6	sezóna
a	a	k8xC	a
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
bilanci	bilance	k1gFnSc4	bilance
zápasů	zápas	k1gInPc2	zápas
navýšila	navýšit	k5eAaPmAgFnS	navýšit
na	na	k7c4	na
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
druhém	druhý	k4xOgNnSc6	druhý
grandslamovém	grandslamový	k2eAgNnSc6d1	grandslamové
finále	finále	k1gNnSc6	finále
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
20	[number]	k4	20
<g/>
letou	letý	k2eAgFnSc7d1	letá
turnajovou	turnajový	k2eAgFnSc7d1	turnajová
třináctkou	třináctka	k1gFnSc7	třináctka
Eugenií	Eugenie	k1gFnSc7	Eugenie
Bouchardovou	Bouchardův	k2eAgFnSc7d1	Bouchardův
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednoznačném	jednoznačný	k2eAgInSc6d1	jednoznačný
průběhu	průběh	k1gInSc6	průběh
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
za	za	k7c4	za
55	[number]	k4	55
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
nejkratší	krátký	k2eAgNnSc1d3	nejkratší
wimbledonské	wimbledonský	k2eAgNnSc1d1	wimbledonské
finále	finále	k1gNnSc1	finále
po	po	k7c6	po
31	[number]	k4	31
letech	let	k1gInPc6	let
a	a	k8xC	a
páté	pátá	k1gFnSc6	pátá
nejkratší	krátký	k2eAgFnSc6d3	nejkratší
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
All	All	k1gMnPc2	All
England	Englanda	k1gFnPc2	Englanda
Clubu	club	k1gInSc2	club
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
naposledy	naposledy	k6eAd1	naposledy
figurovala	figurovat	k5eAaImAgNnP	figurovat
během	během	k7c2	během
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnSc4d1	letní
srpnovou	srpnový	k2eAgFnSc4d1	srpnová
US	US	kA	US
Open	Open	k1gInSc4	Open
Series	Seriesa	k1gFnPc2	Seriesa
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
na	na	k7c4	na
Canada	Canada	k1gFnSc1	Canada
Masters	Masters	k1gInSc1	Masters
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Casey	Casey	k1gInPc7	Casey
Dellacquovou	Dellacquová	k1gFnSc4	Dellacquová
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
fázi	fáze	k1gFnSc6	fáze
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
Jekatěriny	Jekatěrin	k1gInPc4	Jekatěrin
Makarovové	Makarovová	k1gFnSc2	Makarovová
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc2d2	pozdější
semifinalistky	semifinalistka	k1gFnSc2	semifinalistka
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
navazujícího	navazující	k2eAgMnSc2d1	navazující
Cincinnati	Cincinnati	k1gMnSc2	Cincinnati
Masters	Mastersa	k1gFnPc2	Mastersa
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
Ukrajinkou	Ukrajinka	k1gFnSc7	Ukrajinka
Elinou	Elina	k1gFnSc7	Elina
Svitolinovou	Svitolinový	k2eAgFnSc7d1	Svitolinový
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
Connecticut	Connecticut	k2eAgInSc1d1	Connecticut
Open	Open	k1gInSc1	Open
přinesl	přinést	k5eAaPmAgInS	přinést
zlepšení	zlepšení	k1gNnSc4	zlepšení
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
turnajové	turnajový	k2eAgFnSc2d1	turnajová
dvojky	dvojka	k1gFnSc2	dvojka
prošla	projít	k5eAaPmAgFnS	projít
pavoukem	pavouk	k1gMnSc7	pavouk
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
oplatila	oplatit	k5eAaPmAgFnS	oplatit
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
porážku	porážka	k1gFnSc4	porážka
Makarovové	Makarovová	k1gFnSc2	Makarovová
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	jíst	k5eAaImIp3nS	jíst
povolila	povolit	k5eAaPmAgFnS	povolit
uhrát	uhrát	k5eAaPmF	uhrát
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc4	tři
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
raketě	raketa	k1gFnSc6	raketa
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Češka	Češka	k1gFnSc1	Češka
Barbora	Barbora	k1gFnSc1	Barbora
Záhlavová-Strýcová	Záhlavová-Strýcová	k1gFnSc1	Záhlavová-Strýcová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
semifinále	semifinále	k1gNnSc4	semifinále
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
se	se	k3xPyFc4	se
Sam	Sam	k1gMnSc1	Sam
Stosurovou	stosurový	k2eAgFnSc7d1	stosurový
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gMnSc1	Tour
střetla	střetnout	k5eAaPmAgFnS	střetnout
se	s	k7c7	s
Slovenkou	Slovenka	k1gFnSc7	Slovenka
Magdalénou	Magdaléna	k1gFnSc7	Magdaléna
Rybárikovou	Rybárikův	k2eAgFnSc7d1	Rybárikův
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
slavila	slavit	k5eAaImAgFnS	slavit
třináctý	třináctý	k4xOgInSc4	třináctý
kariérní	kariérní	k2eAgInSc4d1	kariérní
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
českých	český	k2eAgFnPc2d1	Česká
tenistek	tenistka	k1gFnPc2	tenistka
dotáhla	dotáhnout	k5eAaPmAgFnS	dotáhnout
na	na	k7c4	na
Reginu	Regina	k1gFnSc4	Regina
Maršíkovou	Maršíková	k1gFnSc4	Maršíková
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
trofej	trofej	k1gFnSc4	trofej
z	z	k7c2	z
New	New	k1gFnSc2	New
Havenu	Haven	k1gInSc2	Haven
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odehrála	odehrát	k5eAaPmAgFnS	odehrát
třetí	třetí	k4xOgNnPc4	třetí
finále	finále	k1gNnPc4	finále
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
poslední	poslední	k2eAgInSc4d1	poslední
grandslam	grandslam	k1gInSc4	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
US	US	kA	US
Open	Open	k1gMnSc1	Open
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
třetí	třetí	k4xOgInSc4	třetí
nejvýše	vysoce	k6eAd3	vysoce
postavené	postavený	k2eAgFnPc4d1	postavená
hráčky	hráčka	k1gFnPc4	hráčka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
Francouzku	Francouzka	k1gFnSc4	Francouzka
Kristinu	Kristina	k1gFnSc4	Kristina
Mladenovicovou	Mladenovicový	k2eAgFnSc7d1	Mladenovicová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jí	on	k3xPp3gFnSc3	on
dokázala	dokázat	k5eAaPmAgFnS	dokázat
odebrat	odebrat	k5eAaPmF	odebrat
jediný	jediný	k2eAgInSc4d1	jediný
game	game	k1gInSc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
utkání	utkání	k1gNnSc4	utkání
proti	proti	k7c3	proti
krajance	krajanka	k1gFnSc3	krajanka
Petře	Petra	k1gFnSc3	Petra
Cetkovské	Cetkovský	k2eAgFnSc2d1	Cetkovská
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
překvapivě	překvapivě	k6eAd1	překvapivě
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
srbskou	srbský	k2eAgFnSc4d1	Srbská
kvalifikantku	kvalifikantka	k1gFnSc4	kvalifikantka
Aleksandru	Aleksandra	k1gFnSc4	Aleksandra
Krunićovou	Krunićová	k1gFnSc4	Krunićová
po	po	k7c6	po
setech	set	k1gInPc6	set
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Asijskou	asijský	k2eAgFnSc4d1	asijská
část	část	k1gFnSc4	část
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
premiérovým	premiérový	k2eAgInSc7d1	premiérový
ročníkem	ročník	k1gInSc7	ročník
Wuhan	Wuhan	k1gMnSc1	Wuhan
Open	Open	k1gMnSc1	Open
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premier	k1gInSc1	Premier
5	[number]	k4	5
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roli	role	k1gFnSc6	role
třetí	třetí	k4xOgFnSc2	třetí
nasazené	nasazený	k2eAgFnSc2d1	nasazená
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
čtrnáctý	čtrnáctý	k4xOgInSc4	čtrnáctý
titul	titul	k1gInSc4	titul
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
Karin	Karina	k1gFnPc2	Karina
Knappové	Knappové	k2eAgMnPc2d1	Knappové
<g/>
,	,	kIx,	,
ztratila	ztratit	k5eAaPmAgFnS	ztratit
jediný	jediný	k2eAgInSc4d1	jediný
set	set	k1gInSc4	set
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
šampiónku	šampiónka	k1gFnSc4	šampiónka
ze	z	k7c2	z
Soulu	Soul	k1gInSc2	Soul
a	a	k8xC	a
krajanku	krajanka	k1gFnSc4	krajanka
Karolínu	Karolína	k1gFnSc4	Karolína
Plíškovou	plíškový	k2eAgFnSc4d1	Plíšková
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Francouzku	Francouzka	k1gFnSc4	Francouzka
Caroline	Carolin	k1gInSc5	Carolin
Garciaovou	Garciaová	k1gFnSc7	Garciaová
a	a	k8xC	a
mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtyřkou	čtyřka	k1gFnSc7	čtyřka
oplatila	oplatit	k5eAaPmAgFnS	oplatit
srpnovou	srpnový	k2eAgFnSc4d1	srpnová
porážku	porážka	k1gFnSc4	porážka
Svitolinové	Svitolinový	k2eAgFnPc4d1	Svitolinový
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
ji	on	k3xPp3gFnSc4	on
nezastavila	zastavit	k5eNaPmAgFnS	zastavit
ani	ani	k9	ani
šestá	šestý	k4xOgFnSc1	šestý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
Eugenie	Eugenie	k1gFnSc1	Eugenie
Bouchardová	Bouchardový	k2eAgFnSc1d1	Bouchardový
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
repete	repete	k6eAd1	repete
wimbledonského	wimbledonský	k2eAgNnSc2d1	wimbledonské
finále	finále	k1gNnSc2	finále
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
dvacet	dvacet	k4xCc4	dvacet
minut	minuta	k1gFnPc2	minuta
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gInSc4	její
první	první	k4xOgInSc4	první
vítězný	vítězný	k2eAgInSc4d1	vítězný
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
sezóně	sezóna	k1gFnSc6	sezóna
nad	nad	k7c7	nad
hráčkou	hráčka	k1gFnSc7	hráčka
z	z	k7c2	z
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Povinný	povinný	k2eAgInSc4d1	povinný
China	China	k1gFnSc1	China
Open	Open	k1gInSc1	Open
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
když	když	k8xS	když
wuchanské	wuchanský	k2eAgFnPc1d1	wuchanský
semifinalistky	semifinalistka	k1gFnPc1	semifinalistka
obdržely	obdržet	k5eAaPmAgFnP	obdržet
volný	volný	k2eAgInSc4d1	volný
los	los	k1gInSc4	los
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
porazila	porazit	k5eAaPmAgFnS	porazit
domácí	domácí	k1gFnSc1	domácí
Pcheng	Pcheng	k1gMnSc1	Pcheng
Šuaj	Šuaj	k1gInSc1	Šuaj
<g/>
,	,	kIx,	,
semifinalistkou	semifinalistka	k1gFnSc7	semifinalistka
posledního	poslední	k2eAgInSc2d1	poslední
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
soupeřkou	soupeřka	k1gFnSc7	soupeřka
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
Venus	Venus	k1gInSc4	Venus
Williamsová	Williamsový	k2eAgFnSc1d1	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
třetího	třetí	k4xOgNnSc2	třetí
kola	kolo	k1gNnSc2	kolo
se	se	k3xPyFc4	se
však	však	k9	však
neodehrál	odehrát	k5eNaPmAgMnS	odehrát
pro	pro	k7c4	pro
nemoc	nemoc	k1gFnSc4	nemoc
Američanky	Američanka	k1gFnSc2	Američanka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvládnutém	zvládnutý	k2eAgNnSc6d1	zvládnuté
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
Italkou	Italka	k1gFnSc7	Italka
Robertou	Roberta	k1gFnSc7	Roberta
Vinciovou	Vinciový	k2eAgFnSc7d1	Vinciová
a	a	k8xC	a
následném	následný	k2eAgInSc6d1	následný
zápasu	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Sam	Sam	k1gMnSc1	Sam
Stosurové	stosurový	k2eAgFnSc2d1	stosurový
<g/>
,	,	kIx,	,
ukončila	ukončit	k5eAaPmAgFnS	ukončit
její	její	k3xOp3gNnSc1	její
vítězné	vítězný	k2eAgNnSc1d1	vítězné
tažení	tažení	k1gNnSc1	tažení
čínskými	čínský	k2eAgInPc7d1	čínský
turnaji	turnaj	k1gInPc7	turnaj
Maria	Mario	k1gMnSc2	Mario
Šarapovová	Šarapovový	k2eAgFnSc1d1	Šarapovová
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
tenistka	tenistka	k1gFnSc1	tenistka
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
uhrála	uhrát	k5eAaPmAgFnS	uhrát
set	set	k1gInSc4	set
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
od	od	k7c2	od
smolné	smolný	k2eAgFnSc2d1	smolná
porážky	porážka	k1gFnSc2	porážka
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prohraném	prohraný	k2eAgNnSc6d1	prohrané
finále	finále	k1gNnSc6	finále
klesla	klesnout	k5eAaPmAgFnS	klesnout
ze	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
příčku	příčka	k1gFnSc4	příčka
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
výhra	výhra	k1gFnSc1	výhra
a	a	k8xC	a
zisk	zisk	k1gInSc1	zisk
titulu	titul	k1gInSc2	titul
by	by	kYmCp3nS	by
jí	on	k3xPp3gFnSc7	on
zajistil	zajistit	k5eAaPmAgMnS	zajistit
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
po	po	k7c6	po
wuchanské	wuchanský	k2eAgFnSc6d1	wuchanský
finálové	finálový	k2eAgFnSc6d1	finálová
bitvě	bitva	k1gFnSc6	bitva
proti	proti	k7c3	proti
Bouchardové	Bouchardový	k2eAgFnSc3d1	Bouchardový
počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
na	na	k7c4	na
Turnaj	turnaj	k1gInSc4	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
,	,	kIx,	,
hraný	hraný	k2eAgInSc1d1	hraný
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
její	její	k3xOp3gInPc1	její
tři	tři	k4xCgInPc1	tři
duely	duel	k1gInPc1	duel
v	v	k7c6	v
bíle	bíle	k6eAd1	bíle
skupině	skupina	k1gFnSc3	skupina
skončily	skončit	k5eAaPmAgInP	skončit
poměrem	poměr	k1gInSc7	poměr
setů	set	k1gInPc2	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
odešla	odejít	k5eAaPmAgFnS	odejít
poražena	poražen	k2eAgFnSc1d1	poražena
<g/>
,	,	kIx,	,
když	když	k8xS	když
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
i	i	k8xC	i
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Polku	Polka	k1gFnSc4	Polka
Agnieszku	Agnieszka	k1gFnSc4	Agnieszka
Radwańskou	Radwańská	k1gFnSc4	Radwańská
a	a	k8xC	a
Caroline	Carolin	k1gInSc5	Carolin
Wozniackou	Wozniacký	k2eAgFnSc7d1	Wozniacká
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
prohru	prohra	k1gFnSc4	prohra
vrátila	vrátit	k5eAaPmAgFnS	vrátit
Šarapovové	Šarapovové	k2eAgFnSc1d1	Šarapovové
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ukončila	ukončit	k5eAaPmAgFnS	ukončit
sérii	série	k1gFnSc6	série
pěti	pět	k4xCc2	pět
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
výher	výhra	k1gFnPc2	výhra
Rusky	rusky	k6eAd1	rusky
<g/>
.	.	kIx.	.
</s>
<s>
Obsadila	obsadit	k5eAaPmAgFnS	obsadit
tak	tak	k9	tak
poslední	poslední	k2eAgNnSc4d1	poslední
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jí	on	k3xPp3gFnSc3	on
stačilo	stačit	k5eAaBmAgNnS	stačit
Wozniackou	Wozniacký	k2eAgFnSc4d1	Wozniacká
porazit	porazit	k5eAaPmF	porazit
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
by	by	k9	by
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
pražského	pražský	k2eAgNnSc2d1	Pražské
finále	finále	k1gNnSc2	finále
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
slavila	slavit	k5eAaImAgFnS	slavit
třetí	třetí	k4xOgFnSc4	třetí
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zakončila	zakončit	k5eAaPmAgFnS	zakončit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
místa	místo	k1gNnPc4	místo
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
než	než	k8xS	než
minulý	minulý	k2eAgInSc4d1	minulý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zahájila	zahájit	k5eAaPmAgFnS	zahájit
na	na	k7c6	na
šenčenském	šenčenský	k2eAgInSc6d1	šenčenský
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
roli	role	k1gFnSc6	role
druhé	druhý	k4xOgFnSc6	druhý
nasazené	nasazený	k2eAgFnPc1d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
Katowice	Katowice	k1gFnSc2	Katowice
Open	Open	k1gInSc1	Open
2013	[number]	k4	2013
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
turnaje	turnaj	k1gInPc4	turnaj
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
International	International	k1gFnSc2	International
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
byla	být	k5eAaImAgFnS	být
nad	nad	k7c4	nad
její	její	k3xOp3gFnPc4	její
síly	síla	k1gFnPc4	síla
Švýcarka	Švýcarka	k1gFnSc1	Švýcarka
Timea	Timea	k1gFnSc1	Timea
Bacsinszká	Bacsinszká	k1gFnSc1	Bacsinszká
<g/>
.	.	kIx.	.
</s>
<s>
Počtvrté	počtvrté	k4xO	počtvrté
za	za	k7c4	za
sebou	se	k3xPyFc7	se
zavítala	zavítat	k5eAaPmAgFnS	zavítat
na	na	k7c4	na
Apia	Apius	k1gMnSc4	Apius
International	International	k1gFnSc7	International
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
oplatila	oplatit	k5eAaPmAgFnS	oplatit
porážku	porážka	k1gFnSc4	porážka
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
ročníku	ročník	k1gInSc2	ročník
Cvetaně	Cvetaně	k1gFnSc2	Cvetaně
Pironkovové	Pironkovová	k1gFnSc2	Pironkovová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
krajanku	krajanka	k1gFnSc4	krajanka
Karolínu	Karolína	k1gFnSc4	Karolína
Plíškovou	plíškový	k2eAgFnSc7d1	Plíšková
po	po	k7c6	po
zvládnutých	zvládnutý	k2eAgFnPc6d1	zvládnutá
tiebreacích	tiebreace	k1gFnPc6	tiebreace
obou	dva	k4xCgInPc2	dva
setů	set	k1gInPc2	set
a	a	k8xC	a
připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
patnáctý	patnáctý	k4xOgInSc4	patnáctý
kariérní	kariérní	k2eAgInSc4d1	kariérní
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Sérii	série	k1gFnSc4	série
neporazitelnosti	neporazitelnost	k1gFnSc2	neporazitelnost
proti	proti	k7c3	proti
Češkám	Češka	k1gFnPc3	Češka
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
na	na	k7c4	na
patnáct	patnáct	k4xCc4	patnáct
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
melbournském	melbournský	k2eAgInSc6d1	melbournský
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
postoupila	postoupit	k5eAaPmAgFnS	postoupit
přes	přes	k7c4	přes
nizozemskou	nizozemský	k2eAgFnSc4d1	nizozemská
kvalifikantku	kvalifikantka	k1gFnSc4	kvalifikantka
Richel	Richela	k1gFnPc2	Richela
Hogenkampovou	Hogenkampový	k2eAgFnSc4d1	Hogenkampový
a	a	k8xC	a
Němku	Němka	k1gFnSc4	Němka
Monu	Mon	k2eAgFnSc4d1	Mona
Barthelovou	Barthelová	k1gFnSc4	Barthelová
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nestačila	stačit	k5eNaBmAgFnS	stačit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
na	na	k7c4	na
útočně	útočně	k6eAd1	útočně
hrající	hrající	k2eAgFnSc4d1	hrající
americkou	americký	k2eAgFnSc4d1	americká
teenagerku	teenagerka	k1gFnSc4	teenagerka
Madison	Madison	k1gInSc1	Madison
Keysovou	Keysová	k1gFnSc4	Keysová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
Dubai	Duba	k1gFnSc2	Duba
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
a	a	k8xC	a
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Qatar	Qatar	k1gMnSc1	Qatar
Total	totat	k5eAaImAgMnS	totat
Open	Open	k1gInSc4	Open
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
Carly	Carla	k1gFnSc2	Carla
Suárezové	Suárezový	k2eAgFnSc2d1	Suárezový
Navarrové	Navarrová	k1gFnSc2	Navarrová
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
přerušila	přerušit	k5eAaPmAgFnS	přerušit
na	na	k7c4	na
pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgFnS	cítit
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
totálně	totálně	k6eAd1	totálně
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
a	a	k8xC	a
prožívala	prožívat	k5eAaImAgFnS	prožívat
prázdnotu	prázdnota	k1gFnSc4	prázdnota
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2015	[number]	k4	2015
lékaři	lékař	k1gMnPc1	lékař
z	z	k7c2	z
Centra	centrum	k1gNnSc2	centrum
pohybové	pohybový	k2eAgFnPc4d1	pohybová
medicíny	medicína	k1gFnSc2	medicína
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
jarní	jarní	k2eAgFnSc2d1	jarní
pauzy	pauza	k1gFnSc2	pauza
mononukleózu	mononukleóza	k1gFnSc4	mononukleóza
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
prokázaly	prokázat	k5eAaPmAgInP	prokázat
testy	test	k1gInPc1	test
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
profesionály	profesionál	k1gMnPc7	profesionál
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
přiřadila	přiřadit	k5eAaPmAgFnS	přiřadit
k	k	k7c3	k
McHaleové	McHaleová	k1gFnSc3	McHaleová
<g/>
,	,	kIx,	,
Watsonové	Watsonový	k2eAgFnSc3d1	Watsonová
či	či	k8xC	či
Federerovi	Federer	k1gMnSc3	Federer
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc4	onemocnění
prodělali	prodělat	k5eAaPmAgMnP	prodělat
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
si	se	k3xPyFc3	se
přes	přes	k7c4	přes
pauzu	pauza	k1gFnSc4	pauza
udržela	udržet	k5eAaPmAgFnS	udržet
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dvorce	dvorec	k1gInPc4	dvorec
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
dvěma	dva	k4xCgFnPc7	dva
výhrami	výhra	k1gFnPc7	výhra
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
čímž	což	k3yRnSc7	což
dopomohla	dopomoct	k5eAaPmAgFnS	dopomoct
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
vítězstvím	vítězství	k1gNnSc7	vítězství
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
antukovém	antukový	k2eAgNnSc6d1	antukové
Porsche	Porsche	k1gNnSc6	Porsche
Tennis	Tennis	k1gFnSc1	Tennis
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
porážkou	porážka	k1gFnSc7	porážka
od	od	k7c2	od
Američanky	Američanka	k1gFnSc2	Američanka
Madison	Madisona	k1gFnPc2	Madisona
Brengleové	Brengleová	k1gFnSc2	Brengleová
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
nasazená	nasazený	k2eAgFnSc1d1	nasazená
zdolala	zdolat	k5eAaPmAgFnS	zdolat
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Mutua	Mutu	k1gInSc2	Mutu
Madrid	Madrid	k1gInSc1	Madrid
Open	Open	k1gInSc1	Open
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Serenu	Seren	k2eAgFnSc4d1	Serena
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jí	on	k3xPp3gFnSc3	on
odebrala	odebrat	k5eAaPmAgFnS	odebrat
pouze	pouze	k6eAd1	pouze
pět	pět	k4xCc4	pět
gamů	game	k1gInPc2	game
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
první	první	k4xOgNnSc4	první
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
předchozích	předchozí	k2eAgNnPc6d1	předchozí
pěti	pět	k4xCc6	pět
prohrách	prohra	k1gFnPc6	prohra
dokázala	dokázat	k5eAaPmAgFnS	dokázat
uhrát	uhrát	k5eAaPmF	uhrát
jediný	jediný	k2eAgInSc4d1	jediný
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
za	za	k7c4	za
66	[number]	k4	66
minut	minuta	k1gFnPc2	minuta
Rusku	Rusko	k1gNnSc6	Rusko
Světlanu	Světlana	k1gFnSc4	Světlana
Kuzněcovovou	Kuzněcovový	k2eAgFnSc4d1	Kuzněcovová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
tak	tak	k9	tak
druhou	druhý	k4xOgFnSc4	druhý
madridskou	madridský	k2eAgFnSc4d1	Madridská
trofej	trofej	k1gFnSc4	trofej
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
znamenala	znamenat	k5eAaImAgFnS	znamenat
šestnácté	šestnáctý	k4xOgNnSc4	šestnáctý
turnajové	turnajový	k2eAgNnSc4d1	turnajové
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duelu	duel	k1gInSc6	duel
využila	využít	k5eAaPmAgFnS	využít
čtyři	čtyři	k4xCgFnPc4	čtyři
breakové	breakový	k2eAgFnPc4d1	breaková
příležitosti	příležitost	k1gFnPc4	příležitost
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
nabídek	nabídka	k1gFnPc2	nabídka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Ruska	Ruska	k1gFnSc1	Ruska
si	se	k3xPyFc3	se
nevypracovala	vypracovat	k5eNaPmAgFnS	vypracovat
ani	ani	k8xC	ani
jednu	jeden	k4xCgFnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Rome	Rom	k1gMnSc5	Rom
Masters	Masters	k1gInSc1	Masters
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Suárezovou	Suárezový	k2eAgFnSc4d1	Suárezový
Navarrovou	Navarrová	k1gFnSc4	Navarrová
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
Španělka	Španělka	k1gFnSc1	Španělka
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
poměr	poměr	k1gInSc4	poměr
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
na	na	k7c4	na
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Roland	Roland	k1gInSc4	Roland
Garros	Garrosa	k1gFnPc2	Garrosa
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
třísetovými	třísetový	k2eAgFnPc7d1	třísetová
výhrami	výhra	k1gFnPc7	výhra
nad	nad	k7c7	nad
Marinou	Marina	k1gFnSc7	Marina
Erakovicovou	Erakovicový	k2eAgFnSc7d1	Erakovicová
a	a	k8xC	a
Solerovou	Solerová	k1gFnSc4	Solerová
Espinosovou	Espinosový	k2eAgFnSc4d1	Espinosový
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
nasbírala	nasbírat	k5eAaPmAgFnS	nasbírat
101	[number]	k4	101
nevynucených	vynucený	k2eNgFnPc2d1	nevynucená
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
přešla	přejít	k5eAaPmAgFnS	přejít
hladce	hladko	k6eAd1	hladko
přes	přes	k7c4	přes
30	[number]	k4	30
<g/>
.	.	kIx.	.
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Rumunku	Rumunka	k1gFnSc4	Rumunka
Irinu-Camelii	Irinu-Camelie	k1gFnSc4	Irinu-Camelie
Beguovou	Beguová	k1gFnSc4	Beguová
<g/>
.	.	kIx.	.
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1	osmifinále
však	však	k9	však
přineslo	přinést	k5eAaPmAgNnS	přinést
vyřazení	vyřazení	k1gNnSc1	vyřazení
od	od	k7c2	od
dvacáté	dvacátý	k4xOgFnSc2	dvacátý
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
ženy	žena	k1gFnSc2	žena
žebříčku	žebříček	k1gInSc2	žebříček
Timey	Time	k2eAgFnPc4d1	Timea
Bacsinszké	Bacsinszká	k1gFnPc4	Bacsinszká
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Bodové	bodový	k2eAgFnPc1d1	bodová
ztráty	ztráta	k1gFnPc1	ztráta
Šarapovové	Šarapovový	k2eAgFnPc1d1	Šarapovová
i	i	k8xC	i
Halepové	Halepová	k1gFnPc1	Halepová
ovšem	ovšem	k9	ovšem
znamenaly	znamenat	k5eAaImAgFnP	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Kvitová	Kvitová	k1gFnSc1	Kvitová
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
grandslamu	grandslam	k1gInSc6	grandslam
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
posunula	posunout	k5eAaPmAgFnS	posunout
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
eastbournského	eastbournský	k1gMnSc2	eastbournský
AEGON	AEGON	kA	AEGON
International	International	k1gMnSc2	International
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgNnP	odhlásit
pro	pro	k7c4	pro
virózu	viróza	k1gFnSc4	viróza
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
druhé	druhý	k4xOgFnSc2	druhý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
jako	jako	k8xS	jako
obhájkyně	obhájkyně	k1gFnSc2	obhájkyně
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
Nizozemku	Nizozemka	k1gFnSc4	Nizozemka
Kiki	Kike	k1gFnSc4	Kike
Bertensovou	Bertensová	k1gFnSc4	Bertensová
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
za	za	k7c2	za
35	[number]	k4	35
minut	minuta	k1gFnPc2	minuta
porazila	porazit	k5eAaPmAgFnS	porazit
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vlastním	vlastní	k2eAgNnSc6d1	vlastní
podání	podání	k1gNnSc6	podání
soupeřce	soupeřka	k1gFnSc6	soupeřka
povolila	povolit	k5eAaPmAgFnS	povolit
jediný	jediný	k2eAgInSc4d1	jediný
fiftýn	fiftýn	k1gInSc4	fiftýn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
otevřela	otevřít	k5eAaPmAgFnS	otevřít
poslední	poslední	k2eAgFnSc4d1	poslední
hru	hra	k1gFnSc4	hra
dvojchybou	dvojchyba	k1gFnSc7	dvojchyba
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejkratší	krátký	k2eAgInSc4d3	nejkratší
zápas	zápas	k1gInSc4	zápas
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
kole	kolo	k1gNnSc6	kolo
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
s	s	k7c7	s
57	[number]	k4	57
<g/>
.	.	kIx.	.
ženou	žena	k1gFnSc7	žena
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
Kurumi	Kuru	k1gFnPc7	Kuru
Narovou	Narový	k2eAgFnSc7d1	Narový
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
posléze	posléze	k6eAd1	posléze
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
turnajové	turnajový	k2eAgInPc4d1	turnajový
osmadvacítce	osmadvacítec	k1gInPc4	osmadvacítec
Jeleně	Jelena	k1gFnSc3	Jelena
Jankovićové	Jankovićová	k1gFnSc2	Jankovićová
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohospodařila	prohospodařit	k5eAaPmAgFnS	prohospodařit
vedení	vedení	k1gNnSc4	vedení
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazení	vyřazení	k1gNnSc4	vyřazení
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
fázi	fáze	k1gFnSc6	fáze
představovalo	představovat	k5eAaImAgNnS	představovat
její	její	k3xOp3gInSc4	její
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
wimbledonský	wimbledonský	k2eAgInSc4d1	wimbledonský
výsledek	výsledek	k1gInSc4	výsledek
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Severoamerickou	severoamerický	k2eAgFnSc4d1	severoamerická
tenisovou	tenisový	k2eAgFnSc4d1	tenisová
sezónu	sezóna	k1gFnSc4	sezóna
na	na	k7c6	na
betonech	beton	k1gInPc6	beton
zahájila	zahájit	k5eAaPmAgFnS	zahájit
srpnovým	srpnový	k2eAgFnPc3d1	srpnová
Canada	Canada	k1gFnSc1	Canada
Masters	Mastersa	k1gFnPc2	Mastersa
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
nestačila	stačit	k5eNaBmAgFnS	stačit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
na	na	k7c4	na
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
z	z	k7c2	z
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
na	na	k7c6	na
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
Masters	Mastersa	k1gFnPc2	Mastersa
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
svůj	svůj	k3xOyFgInSc4	svůj
úvodní	úvodní	k2eAgInSc4d1	úvodní
zápas	zápas	k1gInSc4	zápas
a	a	k8xC	a
po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
ji	on	k3xPp3gFnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
39	[number]	k4	39
<g/>
.	.	kIx.	.
žena	žena	k1gFnSc1	žena
žebříčku	žebříček	k1gInSc2	žebříček
Caroline	Carolin	k1gInSc5	Carolin
Garciaová	Garciaová	k1gFnSc1	Garciaová
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
níž	jenž	k3xRgFnSc3	jenž
ztratila	ztratit	k5eAaPmAgFnS	ztratit
šestkrát	šestkrát	k6eAd1	šestkrát
podání	podání	k1gNnSc4	podání
a	a	k8xC	a
zahrála	zahrát	k5eAaPmAgFnS	zahrát
jedenáct	jedenáct	k4xCc4	jedenáct
dvojchyb	dvojchyba	k1gFnPc2	dvojchyba
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
titul	titul	k1gInSc1	titul
za	za	k7c4	za
předchozí	předchozí	k2eAgInPc4d1	předchozí
čtyři	čtyři	k4xCgInPc4	čtyři
ročníky	ročník	k1gInPc4	ročník
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c4	na
Connecticut	Connecticut	k2eAgInSc4d1	Connecticut
Open	Open	k1gInSc4	Open
v	v	k7c6	v
New	New	k1gFnSc6	New
Havenu	Haven	k1gInSc2	Haven
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
startovala	startovat	k5eAaBmAgFnS	startovat
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvládnutém	zvládnutý	k2eAgNnSc6d1	zvládnuté
semifinále	semifinále	k1gNnSc6	semifinále
proti	proti	k7c3	proti
čtyřnásobné	čtyřnásobný	k2eAgFnSc3d1	čtyřnásobná
šampionce	šampionka	k1gFnSc3	šampionka
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
světové	světový	k2eAgFnSc3d1	světová
čtyřce	čtyřka	k1gFnSc3	čtyřka
Caroline	Carolin	k1gInSc5	Carolin
Wozniacké	Wozniacké	k2eAgMnSc6d1	Wozniacké
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
sadě	sada	k1gFnSc6	sada
setbol	setbol	k1gInSc4	setbol
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
šestá	šestý	k4xOgFnSc1	šestý
hráčka	hráčka	k1gFnSc1	hráčka
klasifikace	klasifikace	k1gFnSc1	klasifikace
Lucie	Lucie	k1gFnSc1	Lucie
Šafářová	Šafářová	k1gFnSc1	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
úvodní	úvodní	k2eAgFnSc2d1	úvodní
sady	sada	k1gFnSc2	sada
si	se	k3xPyFc3	se
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
devět	devět	k4xCc4	devět
brejkových	brejkův	k2eAgFnPc2d1	brejkův
příležitostí	příležitost	k1gFnPc2	příležitost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
čtyři	čtyři	k4xCgInPc1	čtyři
proměnila	proměnit	k5eAaPmAgFnS	proměnit
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
neztratila	ztratit	k5eNaPmAgFnS	ztratit
žádné	žádný	k3yNgNnSc4	žádný
podání	podání	k1gNnSc4	podání
a	a	k8xC	a
za	za	k7c4	za
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
bilanci	bilance	k1gFnSc4	bilance
tímto	tento	k3xDgNnSc7	tento
vítězstvím	vítězství	k1gNnSc7	vítězství
upravila	upravit	k5eAaPmAgFnS	upravit
na	na	k7c6	na
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Češkám	Češka	k1gFnPc3	Češka
držela	držet	k5eAaImAgFnS	držet
již	již	k9	již
16	[number]	k4	16
<g/>
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vítězné	vítězný	k2eAgFnSc6d1	vítězná
sérii	série	k1gFnSc6	série
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
úvodních	úvodní	k2eAgNnPc6d1	úvodní
čtyřech	čtyři	k4xCgNnPc6	čtyři
kolech	kolo	k1gNnPc6	kolo
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
raketě	raketa	k1gFnSc6	raketa
skončily	skončit	k5eAaPmAgInP	skončit
německá	německý	k2eAgFnSc1d1	německá
kvalifikantka	kvalifikantka	k1gFnSc1	kvalifikantka
Laura	Laura	k1gFnSc1	Laura
Siegemundová	Siegemundový	k2eAgFnSc1d1	Siegemundový
<g/>
,	,	kIx,	,
Američanka	Američanka	k1gFnSc1	Američanka
Nicole	Nicole	k1gFnSc1	Nicole
Gibbsová	Gibbsová	k1gFnSc1	Gibbsová
<g/>
,	,	kIx,	,
třicátá	třicátý	k4xOgFnSc1	třicátý
druhá	druhý	k4xOgFnSc1	druhý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
Anna	Anna	k1gFnSc1	Anna
Karolína	Karolína	k1gFnSc1	Karolína
Schmiedlová	Schmiedlový	k2eAgFnSc1d1	Schmiedlová
a	a	k8xC	a
britská	britský	k2eAgFnSc1d1	britská
kvalifikantka	kvalifikantka	k1gFnSc1	kvalifikantka
Johanna	Johanna	k1gFnSc1	Johanna
Kontaová	Kontaová	k1gFnSc1	Kontaová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
svého	svůj	k3xOyFgMnSc2	svůj
prvního	první	k4xOgNnSc2	první
newyorského	newyorský	k2eAgNnSc2d1	newyorské
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
neztratila	ztratit	k5eNaPmAgFnS	ztratit
žádný	žádný	k3yNgInSc4	žádný
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
českou	český	k2eAgFnSc7d1	Česká
hráčkou	hráčka	k1gFnSc7	hráčka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
když	když	k8xS	když
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
čtvrtfinálovou	čtvrtfinálový	k2eAgFnSc4d1	čtvrtfinálová
účast	účast	k1gFnSc4	účast
Dáji	Dája	k1gFnSc2	Dája
Bedáňové	Bedáňová	k1gFnSc2	Bedáňová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
však	však	k9	však
byla	být	k5eAaImAgFnS	být
nad	nad	k7c4	nad
její	její	k3xOp3gFnPc4	její
síly	síla	k1gFnPc4	síla
turnajová	turnajový	k2eAgFnSc1d1	turnajová
šestadvacítka	šestadvacítka	k1gFnSc1	šestadvacítka
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc1d2	pozdější
vítězka	vítězka	k1gFnSc1	vítězka
grandslamu	grandslam	k1gInSc2	grandslam
Flavia	Flavia	k1gFnSc1	Flavia
Pennettaová	Pennettaová	k1gFnSc1	Pennettaová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
prohrála	prohrát	k5eAaPmAgFnS	prohrát
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
a	a	k8xC	a
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zisku	zisk	k1gInSc6	zisk
úvodní	úvodní	k2eAgFnSc2d1	úvodní
sady	sada	k1gFnSc2	sada
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
nevyužila	využít	k5eNaPmAgFnS	využít
prolomené	prolomený	k2eAgNnSc4d1	prolomené
podání	podání	k1gNnSc4	podání
soupeřky	soupeřka	k1gFnSc2	soupeřka
a	a	k8xC	a
vedení	vedení	k1gNnSc2	vedení
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
na	na	k7c4	na
gamy	game	k1gInPc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Asijskou	asijský	k2eAgFnSc4d1	asijská
podzimní	podzimní	k2eAgFnSc4d1	podzimní
část	část	k1gFnSc4	část
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
roli	role	k1gFnSc6	role
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
titulu	titul	k1gInSc2	titul
na	na	k7c4	na
Wuhan	Wuhan	k1gInSc4	Wuhan
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
zastavila	zastavit	k5eAaPmAgFnS	zastavit
úřadující	úřadující	k2eAgFnSc1d1	úřadující
finalistka	finalistka	k1gFnSc1	finalistka
US	US	kA	US
Open	Open	k1gInSc1	Open
a	a	k8xC	a
patnáctá	patnáctý	k4xOgFnSc1	patnáctý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
Roberta	Roberta	k1gFnSc1	Roberta
Vinciová	Vinciový	k2eAgFnSc1d1	Vinciová
<g/>
.	.	kIx.	.
</s>
<s>
Pekingský	pekingský	k2eAgInSc1d1	pekingský
China	China	k1gFnSc1	China
Open	Openo	k1gNnPc2	Openo
pak	pak	k6eAd1	pak
opustila	opustit	k5eAaPmAgFnS	opustit
porážkou	porážka	k1gFnSc7	porážka
v	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
třetí	třetí	k4xOgFnSc4	třetí
Italku	Italka	k1gFnSc4	Italka
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
Saru	Sara	k1gFnSc4	Sara
Erraniovou	Erraniový	k2eAgFnSc4d1	Erraniová
po	po	k7c6	po
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Popáte	Popat	k5eAaPmIp2nP	Popat
za	za	k7c7	za
sebou	se	k3xPyFc7	se
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
na	na	k7c4	na
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
Turnaj	turnaj	k1gInSc4	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
hraný	hraný	k2eAgInSc1d1	hraný
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
fázi	fáze	k1gFnSc6	fáze
dvouhry	dvouhra	k1gFnSc2	dvouhra
na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgFnPc1d1	pohybující
Angelique	Angeliqu	k1gFnPc1	Angeliqu
Kerberové	Kerber	k1gMnPc1	Kerber
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Lucii	Lucie	k1gFnSc4	Lucie
Šafářovou	Šafářová	k1gFnSc4	Šafářová
a	a	k8xC	a
udržela	udržet	k5eAaPmAgFnS	udržet
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
osmizápasovou	osmizápasový	k2eAgFnSc4d1	osmizápasový
neporazitelnost	neporazitelnost	k1gFnSc1	neporazitelnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
klání	klání	k1gNnSc6	klání
bílé	bílý	k2eAgFnSc2d1	bílá
skupiny	skupina	k1gFnSc2	skupina
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
duelu	duel	k1gInSc6	duel
<g/>
,	,	kIx,	,
světové	světový	k2eAgFnSc6d1	světová
trojce	trojka	k1gFnSc6	trojka
Garbiñ	Garbiñ	k1gFnSc2	Garbiñ
Muguruzaové	Muguruzaové	k2eAgFnSc2d1	Muguruzaové
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
pronikla	proniknout	k5eAaPmAgFnS	proniknout
až	až	k9	až
díky	díky	k7c3	díky
pomoci	pomoc	k1gFnSc3	pomoc
Šafářové	Šafářová	k1gFnSc2	Šafářová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
dvousetovém	dvousetový	k2eAgNnSc6d1	dvousetové
utkání	utkání	k1gNnSc6	utkání
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtveřicí	čtveřice	k1gFnSc7	čtveřice
hráček	hráčka	k1gFnPc2	hráčka
předvedla	předvést	k5eAaPmAgFnS	předvést
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
výkonů	výkon	k1gInPc2	výkon
sezóny	sezóna	k1gFnSc2	sezóna
a	a	k8xC	a
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
Marii	Maria	k1gFnSc4	Maria
Šarapovovou	Šarapovový	k2eAgFnSc4d1	Šarapovová
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
setbol	setbol	k1gInSc4	setbol
a	a	k8xC	a
dotáhla	dotáhnout	k5eAaPmAgFnS	dotáhnout
ztrátu	ztráta	k1gFnSc4	ztráta
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
na	na	k7c4	na
gamy	game	k1gInPc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
málo	málo	k6eAd1	málo
chybující	chybující	k2eAgFnSc4d1	chybující
Polku	Polka	k1gFnSc4	Polka
Agnieszku	Agnieszka	k1gFnSc4	Agnieszka
Radwańskou	Radwańská	k1gFnSc4	Radwańská
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
nevynucených	vynucený	k2eNgFnPc2d1	nevynucená
chyb	chyba	k1gFnPc2	chyba
vyzněl	vyznět	k5eAaImAgInS	vyznět
výrazně	výrazně	k6eAd1	výrazně
v	v	k7c4	v
její	její	k3xOp3gInSc4	její
neprospěch	neprospěch	k1gInSc4	neprospěch
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
53	[number]	k4	53
<g/>
.	.	kIx.	.
</s>
<s>
Využila	využít	k5eAaPmAgFnS	využít
čtyři	čtyři	k4xCgInPc4	čtyři
z	z	k7c2	z
pěti	pět	k4xCc2	pět
brejkbolů	brejkbol	k1gInPc2	brejkbol
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
soupeřce	soupeřka	k1gFnSc3	soupeřka
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
čtrnáct	čtrnáct	k4xCc4	čtrnáct
možností	možnost	k1gFnPc2	možnost
na	na	k7c4	na
prolomení	prolomení	k1gNnSc4	prolomení
servisu	servis	k1gInSc2	servis
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
sedm	sedm	k4xCc4	sedm
prohrála	prohrát	k5eAaPmAgFnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
překročila	překročit	k5eAaPmAgFnS	překročit
jako	jako	k9	jako
třináctá	třináctý	k4xOgFnSc1	třináctý
tenistka	tenistka	k1gFnSc1	tenistka
historie	historie	k1gFnSc2	historie
hranici	hranice	k1gFnSc4	hranice
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c6	na
odměnách	odměna	k1gFnPc6	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
exhibici	exhibice	k1gFnSc6	exhibice
Tennis	Tennis	k1gFnSc1	Tennis
Champions	Championsa	k1gFnPc2	Championsa
prohrála	prohrát	k5eAaPmAgFnS	prohrát
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Karolínou	Karolína	k1gFnSc7	Karolína
Schmiedlovou	Schmiedlový	k2eAgFnSc7d1	Schmiedlová
v	v	k7c6	v
jednosetovém	jednosetový	k2eAgInSc6d1	jednosetový
duelu	duel	k1gInSc6	duel
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zakončila	zakončit	k5eAaPmAgFnS	zakončit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
páté	pátý	k4xOgFnSc6	pátý
pozici	pozice	k1gFnSc6	pozice
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
starší	starý	k2eAgFnSc1d2	starší
ze	z	k7c2	z
sester	sestra	k1gFnPc2	sestra
Radwańských	Radwańský	k1gMnPc2	Radwańský
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
na	na	k7c6	na
šenčenském	šenčenský	k2eAgInSc6d1	šenčenský
turnaji	turnaj	k1gInSc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
prvního	první	k4xOgNnSc2	první
kola	kolo	k1gNnSc2	kolo
s	s	k7c7	s
Číňankou	Číňanka	k1gFnSc7	Číňanka
Čeng	Čeng	k1gMnSc1	Čeng
Saj-saj	Sajaj	k1gMnSc1	Saj-saj
však	však	k9	však
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
úvodní	úvodní	k2eAgFnSc2d1	úvodní
sady	sada	k1gFnSc2	sada
skrečovala	skrečovat	k5eAaPmAgFnS	skrečovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gInSc4	její
první	první	k4xOgInSc4	první
skrečovaný	skrečovaný	k2eAgInSc4d1	skrečovaný
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
profesionální	profesionální	k2eAgFnSc6d1	profesionální
kariéře	kariéra	k1gFnSc6	kariéra
a	a	k8xC	a
první	první	k4xOgFnSc3	první
prohrané	prohraná	k1gFnSc3	prohraná
úvodní	úvodní	k2eAgNnSc1d1	úvodní
utkání	utkání	k1gNnSc1	utkání
sezóny	sezóna	k1gFnSc2	sezóna
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Češka	Češka	k1gFnSc1	Češka
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
žaludeční	žaludeční	k2eAgInPc1d1	žaludeční
problémy	problém	k1gInPc1	problém
byly	být	k5eAaImAgInP	být
zřejmě	zřejmě	k6eAd1	zřejmě
způsobeny	způsobit	k5eAaPmNgInP	způsobit
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příletu	přílet	k1gInSc6	přílet
na	na	k7c4	na
Apia	Apius	k1gMnSc4	Apius
International	International	k1gFnSc7	International
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
obhajovat	obhajovat	k5eAaImF	obhajovat
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
pro	pro	k7c4	pro
pokračující	pokračující	k2eAgFnPc4d1	pokračující
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
potíže	potíž	k1gFnPc4	potíž
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
dohraný	dohraný	k2eAgInSc4d1	dohraný
zápas	zápas	k1gInSc4	zápas
tak	tak	k6eAd1	tak
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
na	na	k7c6	na
grandslamu	grandslam	k1gInSc6	grandslam
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
oplatila	oplatit	k5eAaPmAgFnS	oplatit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
starou	starý	k2eAgFnSc4d1	stará
porážku	porážka	k1gFnSc4	porážka
thajské	thajský	k2eAgFnSc3d1	thajská
kvalifikantce	kvalifikantka	k1gFnSc3	kvalifikantka
Luksice	Luksika	k1gFnSc3	Luksika
Kumkhumové	Kumkhumová	k1gFnSc3	Kumkhumová
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
však	však	k9	však
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
vítězku	vítězka	k1gFnSc4	vítězka
Hopmanova	Hopmanův	k2eAgInSc2d1	Hopmanův
poháru	pohár	k1gInSc2	pohár
a	a	k8xC	a
nováčka	nováček	k1gMnSc2	nováček
roku	rok	k1gInSc2	rok
Darju	Darja	k1gFnSc4	Darja
Gavrilovovou	Gavrilovová	k1gFnSc4	Gavrilovová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvousetovém	dvousetový	k2eAgNnSc6d1	dvousetové
střetnutí	střetnutí	k1gNnSc6	střetnutí
<g/>
,	,	kIx,	,
trvajícím	trvající	k2eAgMnSc6d1	trvající
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
,	,	kIx,	,
zahrála	zahrát	k5eAaPmAgFnS	zahrát
17	[number]	k4	17
vítězných	vítězný	k2eAgInPc2d1	vítězný
míčů	míč	k1gInPc2	míč
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
35	[number]	k4	35
nevynucených	vynucený	k2eNgFnPc2d1	nevynucená
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
setu	set	k1gInSc6	set
dvakrát	dvakrát	k6eAd1	dvakrát
nevyužila	využít	k5eNaPmAgFnS	využít
výhodu	výhoda	k1gFnSc4	výhoda
prolomeného	prolomený	k2eAgNnSc2d1	prolomené
podání	podání	k1gNnSc2	podání
Australanky	Australanka	k1gFnSc2	Australanka
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vypadnutí	vypadnutí	k1gNnSc6	vypadnutí
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
oznámila	oznámit	k5eAaPmAgFnS	oznámit
konec	konec	k1gInSc4	konec
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Kotyzou	Kotyza	k1gFnSc7	Kotyza
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vedl	vést	k5eAaImAgMnS	vést
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
a	a	k8xC	a
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gNnSc7	jehož
vedením	vedení	k1gNnSc7	vedení
dvakrát	dvakrát	k6eAd1	dvakrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
a	a	k8xC	a
také	také	k9	také
Turnaj	turnaj	k1gInSc1	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
obdržela	obdržet	k5eAaPmAgFnS	obdržet
v	v	k7c6	v
roli	role	k1gFnSc6	role
turnajové	turnajový	k2eAgFnPc4d1	turnajová
čtyřky	čtyřka	k1gFnPc4	čtyřka
na	na	k7c4	na
Dubai	Dubai	k1gNnSc4	Dubai
Tennis	Tennis	k1gFnPc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
volný	volný	k2eAgInSc4d1	volný
los	los	k1gInSc4	los
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
dohrála	dohrát	k5eAaPmAgFnS	dohrát
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
americké	americký	k2eAgFnSc2d1	americká
světové	světový	k2eAgFnSc2d1	světová
šedesátky	šedesátka	k1gFnSc2	šedesátka
Madison	Madisona	k1gFnPc2	Madisona
Brengleové	Brengleová	k1gFnSc2	Brengleová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
přitom	přitom	k6eAd1	přitom
ztratila	ztratit	k5eAaPmAgFnS	ztratit
vedení	vedení	k1gNnSc3	vedení
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
a	a	k8xC	a
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Navazující	navazující	k2eAgInSc1d1	navazující
Qatar	Qatar	k1gInSc1	Qatar
Total	totat	k5eAaImAgInS	totat
Open	Open	k1gInSc1	Open
znamenal	znamenat	k5eAaImAgInS	znamenat
druhý	druhý	k4xOgInSc4	druhý
vítězný	vítězný	k2eAgInSc4d1	vítězný
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Barboru	Barbora	k1gFnSc4	Barbora
Strýcovou	strýcův	k2eAgFnSc7d1	strýcova
<g/>
.	.	kIx.	.
</s>
<s>
Šestá	šestý	k4xOgFnSc1	šestý
porážka	porážka	k1gFnSc1	porážka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
však	však	k9	však
následovala	následovat	k5eAaImAgFnS	následovat
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
fázi	fáze	k1gFnSc6	fáze
z	z	k7c2	z
rakety	raketa	k1gFnSc2	raketa
pozdější	pozdní	k2eAgFnSc2d2	pozdější
lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
finalistky	finalistka	k1gFnSc2	finalistka
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
leté	letý	k2eAgFnPc1d1	letá
Jeļ	Jeļ	k1gFnPc1	Jeļ
Ostapenkové	Ostapenkový	k2eAgFnPc1d1	Ostapenková
<g/>
,	,	kIx,	,
když	když	k8xS	když
opět	opět	k6eAd1	opět
promarnila	promarnit	k5eAaPmAgFnS	promarnit
náskok	náskok	k1gInSc4	náskok
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
její	její	k3xOp3gFnSc1	její
hra	hra	k1gFnSc1	hra
začala	začít	k5eAaPmAgFnS	začít
mít	mít	k5eAaImF	mít
vážné	vážný	k2eAgFnPc4d1	vážná
trhliny	trhlina	k1gFnPc4	trhlina
a	a	k8xC	a
od	od	k7c2	od
daného	daný	k2eAgInSc2d1	daný
stavu	stav	k1gInSc2	stav
uhrála	uhrát	k5eAaPmAgFnS	uhrát
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc4d1	jediný
game	game	k1gInSc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
BNP	BNP	kA	BNP
Paribas	Paribas	k1gInSc4	Paribas
Open	Opena	k1gFnPc2	Opena
dohrála	dohrát	k5eAaPmAgFnS	dohrát
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Agnieszkou	Agnieszka	k1gFnSc7	Agnieszka
Radwańskou	Radwańský	k2eAgFnSc7d1	Radwańský
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nejkvalitnějšího	kvalitní	k2eAgInSc2d3	nejkvalitnější
výsledku	výsledek	k1gInSc2	výsledek
v	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Navazující	navazující	k2eAgNnSc4d1	navazující
Miami	Miami	k1gNnSc4	Miami
Open	Opena	k1gFnPc2	Opena
opustila	opustit	k5eAaPmAgFnS	opustit
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
po	po	k7c6	po
dvousetové	dvousetový	k2eAgFnSc6d1	dvousetový
prohře	prohra	k1gFnSc6	prohra
s	s	k7c7	s
turnajovou	turnajový	k2eAgFnSc7d1	turnajová
třicítkou	třicítka	k1gFnSc7	třicítka
Jekatěrinou	Jekatěrina	k1gFnSc7	Jekatěrina
Makarovovou	Makarovová	k1gFnSc7	Makarovová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
sadě	sada	k1gFnSc6	sada
zhtratila	zhtratit	k5eAaImAgFnS	zhtratit
vedení	vedení	k1gNnSc4	vedení
gamů	game	k1gInPc2	game
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
brejk	brejk	k1gInSc1	brejk
Rusky	rusky	k6eAd1	rusky
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
druhé	druhý	k4xOgFnSc2	druhý
sady	sada	k1gFnSc2	sada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tak	tak	k6eAd1	tak
od	od	k7c2	od
stavu	stav	k1gInSc2	stav
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
stačilo	stačit	k5eAaBmAgNnS	stačit
držet	držet	k5eAaImF	držet
svá	svůj	k3xOyFgNnPc4	svůj
podání	podání	k1gNnPc4	podání
<g/>
.	.	kIx.	.
</s>
<s>
Antuka	antuka	k1gFnSc1	antuka
Porsche	Porsche	k1gNnSc2	Porsche
Tennis	Tennis	k1gFnSc2	Tennis
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
znamenala	znamenat	k5eAaImAgFnS	znamenat
postup	postup	k1gInSc4	postup
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
semifinále	semifinále	k1gNnSc2	semifinále
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
i	i	k9	i
nástup	nástup	k1gInSc1	nástup
nového	nový	k2eAgMnSc2d1	nový
trenéra	trenér	k1gMnSc2	trenér
Františka	František	k1gMnSc2	František
Čermáka	Čermák	k1gMnSc2	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
vrátila	vrátit	k5eAaPmAgFnS	vrátit
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
fedcupovou	fedcupový	k2eAgFnSc4d1	fedcupová
porážku	porážka	k1gFnSc4	porážka
Monice	Monika	k1gFnSc3	Monika
Niculescuové	Niculescuové	k2eAgFnSc3d1	Niculescuové
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
měla	mít	k5eAaImAgFnS	mít
soupeřka	soupeřka	k1gFnSc1	soupeřka
tři	tři	k4xCgInPc4	tři
mečboly	mečbol	k1gInPc4	mečbol
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
sadě	sada	k1gFnSc6	sada
třetí	třetí	k4xOgFnSc1	třetí
fáze	fáze	k1gFnSc1	fáze
uštědřila	uštědřit	k5eAaPmAgFnS	uštědřit
"	"	kIx"	"
<g/>
kanára	kanár	k1gMnSc2	kanár
<g/>
"	"	kIx"	"
světové	světový	k2eAgFnSc6d1	světová
čtyřce	čtyřka	k1gFnSc6	čtyřka
Garbiñ	Garbiñ	k1gFnSc2	Garbiñ
Muguruzaové	Muguruzaová	k1gFnSc2	Muguruzaová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtveřicí	čtveřice	k1gFnSc7	čtveřice
odešla	odejít	k5eAaPmAgFnS	odejít
poražena	porazit	k5eAaPmNgFnS	porazit
z	z	k7c2	z
třísetového	třísetový	k2eAgInSc2d1	třísetový
zápasu	zápas	k1gInSc2	zápas
s	s	k7c7	s
obhájkyní	obhájkyně	k1gFnSc7	obhájkyně
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc7d2	pozdější
vítězkou	vítězka	k1gFnSc7	vítězka
Angelique	Angelique	k1gFnSc7	Angelique
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
květnovém	květnový	k2eAgInSc6d1	květnový
Mutua	Mutuum	k1gNnSc2	Mutuum
Madrid	Madrid	k1gInSc1	Madrid
Open	Openo	k1gNnPc2	Openo
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
Laru	Lara	k1gFnSc4	Lara
Arruabarrenovou	Arruabarrenový	k2eAgFnSc4d1	Arruabarrenový
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc4d1	hrající
na	na	k7c4	na
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
<g/>
,	,	kIx,	,
a	a	k8xC	a
kvalifikantku	kvalifikantka	k1gFnSc4	kvalifikantka
Jelenu	Jelena	k1gFnSc4	Jelena
Vesninovou	Vesninový	k2eAgFnSc4d1	Vesninová
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c6	na
lednovém	lednový	k2eAgInSc6d1	lednový
Australian	Australian	k1gInSc1	Australian
Open	Open	k1gInSc4	Open
<g/>
,	,	kIx,	,
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
Darji	Darja	k1gFnSc2	Darja
Gavrilovové	Gavrilovová	k1gFnSc2	Gavrilovová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
na	na	k7c6	na
římském	římský	k2eAgInSc6d1	římský
Internazionali	Internazionali	k1gMnSc3	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italia	k1gFnSc1	Italia
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
americké	americký	k2eAgInPc4d1	americký
světové	světový	k2eAgInPc4d1	světový
čtyřiadvacítce	čtyřiadvacítec	k1gInPc4	čtyřiadvacítec
Madison	Madison	k1gNnSc4	Madison
Keysové	Keysová	k1gFnSc2	Keysová
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
sadách	sada	k1gFnPc6	sada
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
bodů	bod	k1gInPc2	bod
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
necelých	celý	k2eNgNnPc6d1	necelé
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
-	-	kIx~	-
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
z	z	k7c2	z
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Klesla	klesnout	k5eAaPmAgFnS	klesnout
tak	tak	k9	tak
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
35	[number]	k4	35
bodů	bod	k1gInPc2	bod
před	před	k7c4	před
třináctou	třináctý	k4xOgFnSc4	třináctý
Šafářovou	Šafářová	k1gFnSc4	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
předtím	předtím	k6eAd1	předtím
opustila	opustit	k5eAaPmAgFnS	opustit
první	první	k4xOgFnSc4	první
desítku	desítka	k1gFnSc4	desítka
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pařížském	pařížský	k2eAgNnSc6d1	pařížské
French	French	k1gMnSc1	French
Open	Open	k1gNnSc1	Open
startovala	startovat	k5eAaBmAgFnS	startovat
jako	jako	k9	jako
turnajová	turnajový	k2eAgFnSc1d1	turnajová
desítka	desítka	k1gFnSc1	desítka
<g/>
,	,	kIx,	,
nejníže	nízce	k6eAd3	nízce
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Černohorku	Černohorka	k1gFnSc4	Černohorka
Danku	Danka	k1gFnSc4	Danka
Kovinićovou	Kovinićová	k1gFnSc4	Kovinićová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
šla	jít	k5eAaImAgFnS	jít
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
sadě	sada	k1gFnSc6	sada
podávat	podávat	k5eAaImF	podávat
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Game	game	k1gInSc4	game
ovšem	ovšem	k9	ovšem
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
a	a	k8xC	a
třemi	tři	k4xCgFnPc7	tři
hrami	hra	k1gFnPc7	hra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
si	se	k3xPyFc3	se
zajistila	zajistit	k5eAaPmAgFnS	zajistit
Kvitová	Kvitová	k1gFnSc1	Kvitová
postup	postup	k1gInSc1	postup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
Tchajwanky	Tchajwanka	k1gFnSc2	Tchajwanka
Sie	Sie	k1gFnPc2	Sie
Su-wej	Suej	k1gInSc1	Su-wej
však	však	k8xC	však
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
23	[number]	k4	23
<g/>
leté	letý	k2eAgFnSc2d1	letá
Američanky	Američanka	k1gFnSc2	Američanka
a	a	k8xC	a
108	[number]	k4	108
<g/>
.	.	kIx.	.
hráčky	hráčka	k1gFnSc2	hráčka
žebříčku	žebříček	k1gInSc2	žebříček
Shelby	Shelba	k1gFnSc2	Shelba
Rogersové	Rogersová	k1gFnSc2	Rogersová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tak	tak	k6eAd1	tak
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
největší	veliký	k2eAgFnPc4d3	veliký
výhry	výhra	k1gFnPc4	výhra
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
ztratila	ztratit	k5eAaPmAgFnS	ztratit
druhý	druhý	k4xOgInSc4	druhý
set	set	k1gInSc4	set
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbývajících	zbývající	k2eAgInPc6d1	zbývající
dvou	dva	k4xCgInPc6	dva
nedovolila	dovolit	k5eNaPmAgFnS	dovolit
české	český	k2eAgFnSc2d1	Česká
jedničce	jednička	k1gFnSc3	jednička
uhrát	uhrát	k5eAaPmF	uhrát
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
game	game	k1gInSc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
travnaté	travnatý	k2eAgFnSc2d1	travnatá
části	část	k1gFnSc2	část
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
birminghamským	birminghamský	k2eAgFnPc3d1	birminghamská
AEGON	AEGON	kA	AEGON
Classic	Classice	k1gFnPc2	Classice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
pátá	pátá	k1gFnSc1	pátá
nasazená	nasazený	k2eAgFnSc1d1	nasazená
porazila	porazit	k5eAaPmAgFnS	porazit
potřetí	potřetí	k4xO	potřetí
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
povrchu	povrch	k1gInSc6	povrch
krajanku	krajanka	k1gFnSc4	krajanka
Lucii	Lucie	k1gFnSc4	Lucie
Šafářovou	Šafářová	k1gFnSc4	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
poměr	poměr	k1gInSc1	poměr
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
tak	tak	k6eAd1	tak
navýšila	navýšit	k5eAaPmAgFnS	navýšit
na	na	k7c4	na
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
a	a	k8xC	a
sérii	série	k1gFnSc4	série
neporazitelnosit	neporazitelnosita	k1gFnPc2	neporazitelnosita
proti	proti	k7c3	proti
Češkám	Češka	k1gFnPc3	Češka
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
na	na	k7c4	na
devatenáct	devatenáct	k4xCc4	devatenáct
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2013	[number]	k4	2013
jí	jíst	k5eAaImIp3nS	jíst
patřila	patřit	k5eAaImAgFnS	patřit
ve	v	k7c6	v
statistice	statistika	k1gFnSc6	statistika
vyhraných	vyhraný	k2eAgInPc2d1	vyhraný
gamů	game	k1gInPc2	game
na	na	k7c6	na
servisu	servis	k1gInSc6	servis
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
travnatého	travnatý	k2eAgInSc2d1	travnatý
povrchu	povrch	k1gInSc2	povrch
první	první	k4xOgFnSc1	první
příčka	příčka	k1gFnSc1	příčka
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
hráčkami	hráčka	k1gFnPc7	hráčka
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
prvním	první	k4xOgNnSc6	první
birmingamském	birmingamský	k2eAgNnSc6d1	birmingamský
kole	kolo	k1gNnSc6	kolo
činila	činit	k5eAaImAgFnS	činit
její	její	k3xOp3gFnSc4	její
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
83,9	[number]	k4	83,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
83,1	[number]	k4	83,1
%	%	kIx~	%
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
Maria	Maria	k1gFnSc1	Maria
Šarapovová	Šarapovový	k2eAgFnSc1d1	Šarapovová
pak	pak	k6eAd1	pak
na	na	k7c4	na
82,4	[number]	k4	82,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
však	však	k9	však
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
teenagerky	teenagerka	k1gFnSc2	teenagerka
Jeleny	Jelena	k1gFnSc2	Jelena
Ostapenkové	Ostapenkový	k2eAgFnSc2d1	Ostapenková
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
přerušovaném	přerušovaný	k2eAgInSc6d1	přerušovaný
deštěm	dešť	k1gInSc7	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
plnila	plnit	k5eAaImAgFnS	plnit
roli	role	k1gFnSc4	role
desáté	desátá	k1gFnSc2	desátá
nasazené	nasazený	k2eAgFnSc2d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
konec	konec	k1gInSc4	konec
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
žebříčku	žebříček	k1gInSc2	žebříček
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Rummunkou	Rummunka	k1gFnSc7	Rummunka
Soranou	Soraný	k2eAgFnSc7d1	Soraný
Cîrsteaovou	Cîrsteaová	k1gFnSc7	Cîrsteaová
nestačila	stačit	k5eNaBmAgFnS	stačit
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
na	na	k7c6	na
35	[number]	k4	35
<g/>
.	.	kIx.	.
hráčku	hráčka	k1gFnSc4	hráčka
žebříčku	žebříček	k1gInSc2	žebříček
Jekatěrinu	Jekatěrin	k1gInSc2	Jekatěrin
Makarovovou	Makarovová	k1gFnSc7	Makarovová
po	po	k7c4	po
nezvládnutýh	nezvládnutýh	k1gInSc4	nezvládnutýh
koncovkách	koncovka	k1gFnPc6	koncovka
obou	dva	k4xCgFnPc2	dva
setů	set	k1gInPc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Horšího	zlý	k2eAgInSc2d2	horší
wimbledonského	wimbledonský	k2eAgInSc2d1	wimbledonský
výsledku	výsledek	k1gInSc2	výsledek
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
jen	jen	k9	jen
při	při	k7c6	při
startech	start	k1gInPc6	start
v	v	k7c6	v
letech	let	k1gInPc6	let
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
letní	letní	k2eAgInPc4d1	letní
americké	americký	k2eAgInPc4d1	americký
betony	beton	k1gInPc4	beton
přijela	přijet	k5eAaPmAgFnS	přijet
jako	jako	k9	jako
světová	světový	k2eAgFnSc1d1	světová
třináctka	třináctka	k1gFnSc1	třináctka
<g/>
.	.	kIx.	.
</s>
<s>
Červencový	červencový	k2eAgInSc1d1	červencový
Rogers	Rogers	k1gInSc1	Rogers
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
Montréalu	Montréal	k1gInSc6	Montréal
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
v	v	k7c6	v
roli	role	k1gFnSc6	role
dvanácté	dvanáctý	k4xOgFnSc6	dvanáctý
nasazené	nasazený	k2eAgFnPc1d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
nestačily	stačit	k5eNaBmAgInP	stačit
polská	polský	k2eAgFnSc1d1	polská
kvalifikantka	kvalifikantka	k1gFnSc1	kvalifikantka
Magda	Magda	k1gFnSc1	Magda
Linetteová	Linetteová	k1gFnSc1	Linetteová
ani	ani	k8xC	ani
Němka	Němka	k1gFnSc1	Němka
Andrea	Andrea	k1gFnSc1	Andrea
Petkovicová	Petkovicový	k2eAgFnSc1d1	Petkovicová
<g/>
,	,	kIx,	,
dohrála	dohrát	k5eAaPmAgFnS	dohrát
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
turnajové	turnajový	k2eAgFnSc2d1	turnajová
devítky	devítka	k1gFnSc2	devítka
Světlany	Světlana	k1gFnSc2	Světlana
Kuzněcovové	Kuzněcovový	k2eAgFnSc2d1	Kuzněcovová
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Ruska	Ruska	k1gFnSc1	Ruska
proměnila	proměnit	k5eAaPmAgFnS	proměnit
pět	pět	k4xCc4	pět
brejkbolů	brejkbol	k1gInPc2	brejkbol
z	z	k7c2	z
devíti	devět	k4xCc2	devět
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
česká	český	k2eAgFnSc1d1	Česká
jednička	jednička	k1gFnSc1	jednička
čtyři	čtyři	k4xCgInPc4	čtyři
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
servisu	servis	k1gInSc6	servis
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
32	[number]	k4	32
%	%	kIx~	%
míčů	míč	k1gInPc2	míč
(	(	kIx(	(
<g/>
10	[number]	k4	10
z	z	k7c2	z
31	[number]	k4	31
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srpen	srpen	k1gInSc4	srpen
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
v	v	k7c6	v
roli	role	k1gFnSc6	role
jedenácté	jedenáctý	k4xOgFnSc2	jedenáctý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
na	na	k7c6	na
olympijském	olympijský	k2eAgInSc6d1	olympijský
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
de	de	k?	de
Janeiru	Janeir	k1gInSc2	Janeir
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhrách	výhra	k1gFnPc6	výhra
nad	nad	k7c7	nad
Maďarkou	Maďarka	k1gFnSc7	Maďarka
Tímeou	Tímea	k1gFnSc7	Tímea
Babosovou	Babosový	k2eAgFnSc7d1	Babosový
a	a	k8xC	a
Dánkou	Dánka	k1gFnSc7	Dánka
Caroline	Carolin	k1gInSc5	Carolin
Wozniackou	Wozniacký	k2eAgFnSc7d1	Wozniacká
<g/>
,	,	kIx,	,
vracející	vracející	k2eAgFnSc7d1	vracející
se	se	k3xPyFc4	se
na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
po	po	k7c6	po
zranění	zranění	k1gNnSc6	zranění
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
první	první	k4xOgInSc4	první
set	set	k1gInSc4	set
odebrala	odebrat	k5eAaPmAgFnS	odebrat
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
Jekatěrina	Jekatěrina	k1gFnSc1	Jekatěrina
Makarovová	Makarovová	k1gFnSc1	Makarovová
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgFnPc1d1	zbylá
dvě	dva	k4xCgFnPc1	dva
sady	sada	k1gFnPc1	sada
však	však	k9	však
získala	získat	k5eAaPmAgFnS	získat
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
porážkách	porážka	k1gFnPc6	porážka
Rusku	Rusko	k1gNnSc6	Rusko
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
zdolala	zdolat	k5eAaPmAgFnS	zdolat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvtrtfinále	čtvtrtfinál	k1gInSc6	čtvtrtfinál
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
za	za	k7c4	za
necelou	celý	k2eNgFnSc4d1	necelá
hodinu	hodina	k1gFnSc4	hodina
Ukrajinku	Ukrajinka	k1gFnSc4	Ukrajinka
Elinu	Elina	k1gFnSc4	Elina
Svitolinovou	Svitolinový	k2eAgFnSc7d1	Svitolinový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
skončila	skončit	k5eAaPmAgFnS	skončit
mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtveřicí	čtveřice	k1gFnSc7	čtveřice
hráček	hráčka	k1gFnPc2	hráčka
s	s	k7c7	s
překvapením	překvapení	k1gNnSc7	překvapení
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
letou	letý	k2eAgFnSc7d1	letá
Portoričankou	Portoričanka	k1gFnSc7	Portoričanka
a	a	k8xC	a
34	[number]	k4	34
<g/>
.	.	kIx.	.
ženou	žena	k1gFnSc7	žena
žebříčku	žebříček	k1gInSc2	žebříček
Mónicou	Mónicý	k2eAgFnSc4d1	Mónicý
Puigovou	Puigový	k2eAgFnSc4d1	Puigová
po	po	k7c6	po
třísetovém	třísetový	k2eAgNnSc6d1	třísetové
klání	klání	k1gNnSc6	klání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Američanku	Američanka	k1gFnSc4	Američanka
Madison	Madisona	k1gFnPc2	Madisona
Keysovou	Keysová	k1gFnSc4	Keysová
opět	opět	k6eAd1	opět
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
sadách	sada	k1gFnPc6	sada
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
navazujícího	navazující	k2eAgInSc2d1	navazující
Western	Western	kA	Western
&	&	k?	&
Southern	Southern	k1gInSc1	Southern
Open	Opena	k1gFnPc2	Opena
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
aby	aby	k9	aby
další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
trofej	trofej	k1gFnSc1	trofej
na	na	k7c6	na
newhavenském	whavenský	k2eNgInSc6d1	whavenský
Connecticut	Connecticut	k1gInSc1	Connecticut
Open	Openo	k1gNnPc2	Openo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
Bouchardovou	Bouchardový	k2eAgFnSc4d1	Bouchardový
a	a	k8xC	a
Makarovovou	Makarovová	k1gFnSc4	Makarovová
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
semifinále	semifinále	k1gNnSc2	semifinále
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
hladce	hladko	k6eAd1	hladko
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podlehla	podlehnout	k5eAaPmAgNnP	podlehnout
pozdější	pozdní	k2eAgNnPc1d2	pozdější
vítězce	vítězka	k1gFnSc3	vítězka
Agnieszce	Agnieszka	k1gFnSc3	Agnieszka
Radwańské	Radwańská	k1gFnSc3	Radwańská
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgInSc1d1	letní
vrchol	vrchol	k1gInSc1	vrchol
US	US	kA	US
Open	Open	k1gInSc1	Open
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
roli	role	k1gFnSc6	role
turnajové	turnajový	k2eAgFnSc2d1	turnajová
čtrnáctky	čtrnáctka	k1gFnSc2	čtrnáctka
a	a	k8xC	a
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
grandslamového	grandslamový	k2eAgInSc2d1	grandslamový
výsledku	výsledek	k1gInSc2	výsledek
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
Svitolinovou	Svitolinový	k2eAgFnSc7d1	Svitolinový
a	a	k8xC	a
poté	poté	k6eAd1	poté
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
světové	světový	k2eAgFnSc2d1	světová
dvojky	dvojka	k1gFnSc2	dvojka
Angelique	Angeliqu	k1gInSc2	Angeliqu
Kerberové	Kerber	k1gMnPc1	Kerber
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
,	,	kIx,	,
když	když	k8xS	když
duel	duel	k1gInSc4	duel
zakončila	zakončit	k5eAaPmAgFnS	zakončit
dvojchybou	dvojchyba	k1gFnSc7	dvojchyba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
starým	starý	k2eAgInSc7d1	starý
newyorským	newyorský	k2eAgInSc7d1	newyorský
maximem	maxim	k1gInSc7	maxim
zaostala	zaostat	k5eAaPmAgFnS	zaostat
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
kolo	kolo	k1gNnSc4	kolo
a	a	k8xC	a
bodová	bodový	k2eAgFnSc1d1	bodová
ztráta	ztráta	k1gFnSc1	ztráta
190	[number]	k4	190
bodů	bod	k1gInPc2	bod
ji	on	k3xPp3gFnSc4	on
udržela	udržet	k5eAaPmAgFnS	udržet
mezi	mezi	k7c7	mezi
elitní	elitní	k2eAgFnSc7d1	elitní
dvacítkou	dvacítka	k1gFnSc7	dvacítka
hráček	hráčka	k1gFnPc2	hráčka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
duelu	duel	k1gInSc3	duel
sdělila	sdělit	k5eAaPmAgFnS	sdělit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Měla	mít	k5eAaImAgFnS	mít
jsem	být	k5eAaImIp1nS	být
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hraju	hrát	k5eAaImIp1nS	hrát
strašně	strašně	k6eAd1	strašně
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
řekla	říct	k5eAaPmAgFnS	říct
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Snažila	snažit	k5eAaImAgFnS	snažit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
i	i	k9	i
šance	šance	k1gFnPc1	šance
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bohužel	bohužel	k9	bohužel
je	být	k5eAaImIp3nS	být
nevyužila	využít	k5eNaPmAgFnS	využít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
turnaji	turnaj	k1gInSc6	turnaj
ukončila	ukončit	k5eAaPmAgFnS	ukončit
pětiměsíční	pětiměsíční	k2eAgFnSc4d1	pětiměsíční
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
trenérem	trenér	k1gMnSc7	trenér
Františkem	František	k1gMnSc7	František
Čermákem	Čermák	k1gMnSc7	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Toray	Toray	k1gInPc4	Toray
Pan	Pan	k1gMnSc1	Pan
Pacific	Pacific	k1gMnSc1	Pacific
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
přijala	přijmout	k5eAaPmAgFnS	přijmout
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
sedmé	sedmý	k4xOgFnSc2	sedmý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
nejdříve	dříve	k6eAd3	dříve
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
Američanku	Američanka	k1gFnSc4	Američanka
Madison	Madison	k1gNnSc1	Madison
Brengleovou	Brengleův	k2eAgFnSc7d1	Brengleův
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
poté	poté	k6eAd1	poté
v	v	k7c6	v
repríze	repríza	k1gFnSc6	repríza
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
klání	klání	k1gNnSc2	klání
zastavila	zastavit	k5eAaPmAgFnS	zastavit
Monica	Monica	k1gFnSc1	Monica
Puigová	Puigový	k2eAgFnSc1d1	Puigová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
setu	set	k1gInSc6	set
přitom	přitom	k6eAd1	přitom
nevyužila	využít	k5eNaPmAgFnS	využít
výhodu	výhoda	k1gFnSc4	výhoda
prolomeného	prolomený	k2eAgNnSc2d1	prolomené
podání	podání	k1gNnSc2	podání
a	a	k8xC	a
náskok	náskok	k1gInSc1	náskok
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
jedenácti	jedenáct	k4xCc2	jedenáct
výměn	výměna	k1gFnPc2	výměna
duelu	duel	k1gInSc2	duel
získala	získat	k5eAaPmAgFnS	získat
proti	proti	k7c3	proti
světové	světový	k2eAgFnSc3d1	světová
třiatřicítce	třiatřicítka	k1gFnSc3	třiatřicítka
jen	jen	k6eAd1	jen
jedinou	jediný	k2eAgFnSc7d1	jediná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
hrála	hrát	k5eAaImAgFnS	hrát
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
belgického	belgický	k2eAgMnSc2d1	belgický
kouče	kouč	k1gMnSc2	kouč
Wima	Wimus	k1gMnSc2	Wimus
Fissetta	Fissett	k1gMnSc2	Fissett
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
na	na	k7c4	na
týdenní	týdenní	k2eAgNnSc4d1	týdenní
testovací	testovací	k2eAgNnSc4d1	testovací
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	září	k1gNnSc2	září
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
startovala	startovat	k5eAaBmAgNnP	startovat
na	na	k7c6	na
podniku	podnik	k1gInSc6	podnik
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premier	k1gInSc1	Premier
5	[number]	k4	5
ve	v	k7c6	v
Wu-chanu	Wuhan	k1gMnSc6	Wu-chan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
před	před	k7c7	před
dvěma	dva	k4xCgNnPc7	dva
lety	léto	k1gNnPc7	léto
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
si	se	k3xPyFc3	se
snadno	snadno	k6eAd1	snadno
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Jeļ	Jeļ	k1gFnSc7	Jeļ
Ostapenkovou	Ostapenkový	k2eAgFnSc7d1	Ostapenková
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
porazila	porazit	k5eAaPmAgFnS	porazit
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
tak	tak	k6eAd1	tak
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
bilanci	bilance	k1gFnSc4	bilance
na	na	k7c4	na
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
skórem	skór	k1gInSc7	skór
vyprovodila	vyprovodit	k5eAaPmAgFnS	vyprovodit
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
fázi	fáze	k1gFnSc6	fáze
turnaje	turnaj	k1gInSc2	turnaj
i	i	k9	i
Elinu	Elina	k1gFnSc4	Elina
Svitolinovou	Svitolinový	k2eAgFnSc4d1	Svitolinový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
pak	pak	k6eAd1	pak
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třech	tři	k4xCgInPc2	tři
a	a	k8xC	a
čtvrt	čtvrt	k1xP	čtvrt
hodinách	hodina	k1gFnPc6	hodina
boje	boj	k1gInSc2	boj
udolala	udolat	k5eAaPmAgFnS	udolat
po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Angelique	Angeliqu	k1gInSc2	Angeliqu
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tak	tak	k6eAd1	tak
porazila	porazit	k5eAaPmAgFnS	porazit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
a	a	k8xC	a
počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
porazila	porazit	k5eAaPmAgFnS	porazit
nejvýše	vysoce	k6eAd3	vysoce
postavenou	postavený	k2eAgFnSc4d1	postavená
hráčku	hráčka	k1gFnSc4	hráčka
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
potíží	potíž	k1gFnPc2	potíž
pak	pak	k6eAd1	pak
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
sadách	sada	k1gFnPc6	sada
třináctou	třináctý	k4xOgFnSc4	třináctý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Britku	Britka	k1gFnSc4	Britka
Johannu	Johanen	k2eAgFnSc4d1	Johanna
Kontaovou	Kontaová	k1gFnSc4	Kontaová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
dokázala	dokázat	k5eAaPmAgFnS	dokázat
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
porazit	porazit	k5eAaPmF	porazit
Simonu	Simona	k1gFnSc4	Simona
Halepovou	Halepový	k2eAgFnSc4d1	Halepový
z	z	k7c2	z
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
když	když	k8xS	když
čtvrté	čtvrtý	k4xOgFnPc1	čtvrtý
nasazené	nasazený	k2eAgFnPc1d1	nasazená
povolila	povolit	k5eAaPmAgFnS	povolit
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
jen	jen	k6eAd1	jen
3	[number]	k4	3
gemy	gema	k1gFnPc1	gema
(	(	kIx(	(
<g/>
výsledek	výsledek	k1gInSc1	výsledek
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
poté	poté	k6eAd1	poté
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
desítku	desítka	k1gFnSc4	desítka
Dominiku	Dominik	k1gMnSc3	Dominik
Cibulkovou	Cibulková	k1gFnSc4	Cibulková
po	po	k7c6	po
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ukončila	ukončit	k5eAaPmAgFnS	ukončit
tak	tak	k6eAd1	tak
13	[number]	k4	13
<g/>
měsíčním	měsíční	k2eAgInSc7d1	měsíční
čekání	čekání	k1gNnSc4	čekání
na	na	k7c6	na
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Navazující	navazující	k2eAgInSc1d1	navazující
silně	silně	k6eAd1	silně
obsazený	obsazený	k2eAgInSc1d1	obsazený
China	China	k1gFnSc1	China
Open	Open	k1gNnSc4	Open
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
znamenal	znamenat	k5eAaImAgInS	znamenat
definitivní	definitivní	k2eAgFnSc4d1	definitivní
ztrátu	ztráta	k1gFnSc4	ztráta
naděje	naděje	k1gFnSc2	naděje
startovat	startovat	k5eAaBmF	startovat
na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tak	tak	k6eAd1	tak
chyběla	chybět	k5eAaImAgFnS	chybět
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
duely	duel	k1gInPc4	duel
s	s	k7c7	s
čínskou	čínský	k2eAgFnSc7d1	čínská
kvalifikantkou	kvalifikantka	k1gFnSc7	kvalifikantka
Wang	Wanga	k1gFnPc2	Wanga
Ja-fan	Jaana	k1gFnPc2	Ja-fana
i	i	k8xC	i
druhou	druhý	k4xOgFnSc4	druhý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Garbiñ	Garbiñ	k1gFnSc4	Garbiñ
Muguruzaovou	Muguruzaový	k2eAgFnSc4d1	Muguruzaový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
ve	v	k7c4	v
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
vystavila	vystavit	k5eAaPmAgFnS	vystavit
stopku	stopka	k1gFnSc4	stopka
turnajová	turnajový	k2eAgFnSc1d1	turnajová
osmička	osmička	k1gFnSc1	osmička
Madison	Madisona	k1gFnPc2	Madisona
Keysová	Keysová	k1gFnSc1	Keysová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dramatickém	dramatický	k2eAgInSc6d1	dramatický
duelu	duel	k1gInSc6	duel
trvajícím	trvající	k2eAgInSc6d1	trvající
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
získala	získat	k5eAaPmAgFnS	získat
druhý	druhý	k4xOgInSc4	druhý
set	set	k1gInSc4	set
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
sadě	sada	k1gFnSc6	sada
zkrácenou	zkrácený	k2eAgFnSc4d1	zkrácená
hru	hra	k1gFnSc4	hra
ztratila	ztratit	k5eAaPmAgFnS	ztratit
poměrem	poměr	k1gInSc7	poměr
míčů	míč	k1gInPc2	míč
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Unavená	unavený	k2eAgFnSc1d1	unavená
Češka	Češka	k1gFnSc1	Češka
tak	tak	k6eAd1	tak
ukončila	ukončit	k5eAaPmAgFnS	ukončit
sérii	série	k1gFnSc4	série
osmi	osm	k4xCc2	osm
výher	výhra	k1gFnPc2	výhra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
podzimu	podzim	k1gInSc2	podzim
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jediný	jediný	k2eAgInSc4d1	jediný
turnaj	turnaj	k1gInSc4	turnaj
BGL	BGL	kA	BGL
Luxembourg	Luxembourg	k1gInSc1	Luxembourg
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
nejvýše	nejvýše	k6eAd1	nejvýše
nasazené	nasazený	k2eAgNnSc4d1	nasazené
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
pátého	pátý	k4xOgNnSc2	pátý
karierního	karierní	k2eAgNnSc2d1	karierní
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
pavoukem	pavouk	k1gMnSc7	pavouk
otočila	otočit	k5eAaPmAgFnS	otočit
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
vývoj	vývoj	k1gInSc4	vývoj
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
proti	proti	k7c3	proti
Varvaře	Varvara	k1gFnSc3	Varvara
Lepčenkové	Lepčenkový	k2eAgInPc4d1	Lepčenkový
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
úvodního	úvodní	k2eAgInSc2d1	úvodní
setu	set	k1gInSc2	set
dovolila	dovolit	k5eAaPmAgFnS	dovolit
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
průběhu	průběh	k1gInSc6	průběh
soupeřce	soupeřka	k1gFnSc3	soupeřka
uhrát	uhrát	k5eAaPmF	uhrát
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
gamy	game	k1gInPc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
semifinálovém	semifinálový	k2eAgNnSc6d1	semifinálové
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
americkou	americký	k2eAgFnSc7d1	americká
kvalifikantkou	kvalifikantka	k1gFnSc7	kvalifikantka
Lauren	Laurna	k1gFnPc2	Laurna
Davisovou	Davisový	k2eAgFnSc4d1	Davisová
však	však	k9	však
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
klání	klání	k1gNnSc4	klání
proti	proti	k7c3	proti
Rumunce	Rumunka	k1gFnSc3	Rumunka
Monice	Monika	k1gFnSc3	Monika
Niculescuové	Niculescuová	k1gFnSc3	Niculescuová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
uštědřila	uštědřit	k5eAaPmAgFnS	uštědřit
"	"	kIx"	"
<g/>
kanára	kanár	k1gMnSc2	kanár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výborně	výborně	k6eAd1	výborně
pohybující	pohybující	k2eAgFnSc7d1	pohybující
se	se	k3xPyFc4	se
soupeřkou	soupeřka	k1gFnSc7	soupeřka
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc7d1	hrající
stylem	styl	k1gInSc7	styl
čopovaných	čopovaný	k2eAgInPc2d1	čopovaný
úderů	úder	k1gInPc2	úder
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
poměr	poměr	k1gInSc1	poměr
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
po	po	k7c6	po
finále	finále	k1gNnSc6	finále
činil	činit	k5eAaImAgMnS	činit
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
se	se	k3xPyFc4	se
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
na	na	k7c4	na
listopadovou	listopadový	k2eAgFnSc4d1	listopadová
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
událost	událost	k1gFnSc4	událost
WTA	WTA	kA	WTA
Elite	Elit	k1gInSc5	Elit
Trophy	Troph	k1gMnPc7	Troph
v	v	k7c6	v
Ču-chaji	Čuhaj	k1gInSc6	Ču-chaj
<g/>
,	,	kIx,	,
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
malý	malý	k2eAgInSc1d1	malý
Turnaj	turnaj	k1gInSc1	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pivoňkové	pivoňkový	k2eAgFnSc6d1	pivoňková
skupině	skupina	k1gFnSc6	skupina
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Vinciovou	Vinciový	k2eAgFnSc7d1	Vinciová
i	i	k8xC	i
Strýcovou	strýcův	k2eAgFnSc7d1	strýcova
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Čang	Čang	k1gInSc4	Čang
Šuaj	Šuaj	k1gInSc1	Šuaj
startující	startující	k2eAgInSc1d1	startující
na	na	k7c4	na
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
trofej	trofej	k1gFnSc4	trofej
v	v	k7c6	v
roce	rok	k1gInSc6	rok
vybojovala	vybojovat	k5eAaPmAgNnP	vybojovat
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Svitolinovou	Svitolinový	k2eAgFnSc7d1	Svitolinový
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
sadě	sada	k1gFnSc6	sada
doháněla	dohánět	k5eAaImAgFnS	dohánět
náskok	náskok	k1gInSc1	náskok
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
na	na	k7c4	na
gamy	game	k1gInPc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
však	však	k9	však
získala	získat	k5eAaPmAgFnS	získat
pět	pět	k4xCc4	pět
her	hra	k1gFnPc2	hra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
úvodní	úvodní	k2eAgNnSc4d1	úvodní
dějství	dějství	k1gNnSc4	dějství
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
dokázala	dokázat	k5eAaPmAgFnS	dokázat
soupeřce	soupeřka	k1gFnSc3	soupeřka
třikrát	třikrát	k6eAd1	třikrát
odebrat	odebrat	k5eAaPmF	odebrat
podání	podání	k1gNnSc4	podání
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
přišla	přijít	k5eAaPmAgFnS	přijít
<g/>
,	,	kIx,	,
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Ukrajince	Ukrajinka	k1gFnSc3	Ukrajinka
tak	tak	k6eAd1	tak
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
počtvrté	počtvrté	k4xO	počtvrté
během	během	k7c2	během
necelých	celý	k2eNgInPc2d1	necelý
čtyř	čtyři	k4xCgInPc2	čtyři
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yIgInSc4	který
nepřišla	přijít	k5eNaPmAgFnS	přijít
ani	ani	k8xC	ani
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
první	první	k4xOgFnSc7	první
tenistkou	tenistka	k1gFnSc7	tenistka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
oba	dva	k4xCgInPc4	dva
závěrečné	závěrečný	k2eAgInPc4d1	závěrečný
turnaje	turnaj	k1gInPc4	turnaj
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
když	když	k8xS	když
Turnaj	turnaj	k1gInSc4	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
listopadovém	listopadový	k2eAgInSc6d1	listopadový
týdnu	týden	k1gInSc6	týden
plnila	plnit	k5eAaImAgFnS	plnit
roli	role	k1gFnSc4	role
české	český	k2eAgFnSc2d1	Česká
týmové	týmový	k2eAgFnSc2d1	týmová
dvojky	dvojka	k1gFnSc2	dvojka
ve	v	k7c6	v
štrasburském	štrasburský	k2eAgNnSc6d1	štrasburské
finále	finále	k1gNnSc6	finále
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
prohála	prohát	k5eAaPmAgFnS	prohát
sobotní	sobotní	k2eAgFnSc4d1	sobotní
dvouhru	dvouhra	k1gFnSc4	dvouhra
s	s	k7c7	s
Caroline	Carolin	k1gInSc5	Carolin
Garciaovou	Garciaový	k2eAgFnSc7d1	Garciaový
<g/>
.	.	kIx.	.
</s>
<s>
Češky	Češka	k1gFnPc1	Češka
přesto	přesto	k6eAd1	přesto
zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pátou	pátý	k4xOgFnSc4	pátý
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
úvodní	úvodní	k2eAgFnSc2d1	úvodní
akce	akce	k1gFnSc2	akce
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
Hopmanova	Hopmanův	k2eAgInSc2d1	Hopmanův
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
pro	pro	k7c4	pro
nedoléčenou	doléčený	k2eNgFnSc4d1	nedoléčená
únavovou	únavový	k2eAgFnSc4d1	únavová
zlomeninu	zlomenina	k1gFnSc4	zlomenina
zánártní	zánártní	k2eAgFnSc2d1	zánártní
kosti	kost	k1gFnSc2	kost
pravé	pravý	k2eAgFnSc2d1	pravá
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
týmu	tým	k1gInSc6	tým
ji	on	k3xPp3gFnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Lucie	Lucie	k1gFnSc1	Lucie
Hradecká	Hradecká	k1gFnSc1	Hradecká
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
doplnila	doplnit	k5eAaPmAgFnS	doplnit
nominovaného	nominovaný	k2eAgMnSc4d1	nominovaný
Adama	Adam	k1gMnSc4	Adam
Pavláska	Pavlásek	k1gMnSc4	Pavlásek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
v	v	k7c6	v
prostějovském	prostějovský	k2eAgInSc6d1	prostějovský
bytě	byt	k1gInSc6	byt
stala	stát	k5eAaPmAgFnS	stát
obětí	oběť	k1gFnSc7	oběť
loupežného	loupežný	k2eAgNnSc2d1	loupežné
přepadení	přepadení	k1gNnSc2	přepadení
<g/>
.	.	kIx.	.
</s>
<s>
Útočník	útočník	k1gMnSc1	útočník
jí	jíst	k5eAaImIp3nS	jíst
nožem	nůž	k1gInSc7	nůž
způsobil	způsobit	k5eAaPmAgMnS	způsobit
řezné	řezný	k2eAgFnPc4d1	řezná
rány	rána	k1gFnPc4	rána
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
nutností	nutnost	k1gFnSc7	nutnost
operačního	operační	k2eAgInSc2d1	operační
zákroku	zákrok	k1gInSc2	zákrok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
ve	v	k7c6	v
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
,	,	kIx,	,
a	a	k8xC	a
předpokladem	předpoklad	k1gInSc7	předpoklad
několikaměsíční	několikaměsíční	k2eAgFnSc2d1	několikaměsíční
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
<g/>
.	.	kIx.	.
</s>
<s>
Pooperační	pooperační	k2eAgFnSc1d1	pooperační
prognóza	prognóza	k1gFnSc1	prognóza
těžkého	těžký	k2eAgNnSc2d1	těžké
poranění	poranění	k1gNnSc2	poranění
ruky	ruka	k1gFnSc2	ruka
udávala	udávat	k5eAaImAgFnS	udávat
minimálně	minimálně	k6eAd1	minimálně
půlroční	půlroční	k2eAgInSc4d1	půlroční
výpadek	výpadek	k1gInSc4	výpadek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
odehrála	odehrát	k5eAaPmAgFnS	odehrát
ve	v	k7c6	v
Fed	Fed	k1gFnSc6	Fed
Cupu	cup	k1gInSc2	cup
za	za	k7c4	za
tým	tým	k1gInSc4	tým
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
20	[number]	k4	20
mezistátních	mezistátní	k2eAgInPc2d1	mezistátní
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
jedničky	jednička	k1gFnSc2	jednička
či	či	k8xC	či
dvojky	dvojka	k1gFnSc2	dvojka
dovedla	dovést	k5eAaPmAgFnS	dovést
družstvo	družstvo	k1gNnSc4	družstvo
ke	k	k7c3	k
čtyřem	čtyři	k4xCgInPc3	čtyři
titulům	titul	k1gInPc3	titul
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
a	a	k8xC	a
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
statistice	statistika	k1gFnSc6	statistika
činí	činit	k5eAaImIp3nS	činit
nejúspěšnější	úspěšný	k2eAgFnSc4d3	nejúspěšnější
českou	český	k2eAgFnSc4d1	Česká
reprezentantku	reprezentantka	k1gFnSc4	reprezentantka
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
poprvé	poprvé	k6eAd1	poprvé
vítězkou	vítězka	k1gFnSc7	vítězka
Fed	Fed	k1gFnSc2	Fed
Cupu	cupat	k5eAaImIp1nS	cupat
<g/>
,	,	kIx,	,
když	když	k8xS	když
neprohrála	prohrát	k5eNaPmAgFnS	prohrát
žádný	žádný	k3yNgInSc4	žádný
zápas	zápas	k1gInSc4	zápas
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
tak	tak	k6eAd1	tak
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
zisku	zisk	k1gInSc2	zisk
této	tento	k3xDgFnSc2	tento
týmové	týmový	k2eAgFnSc2d1	týmová
trofeje	trofej	k1gFnSc2	trofej
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
devíti	devět	k4xCc3	devět
bodům	bod	k1gInPc3	bod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
ziskal	ziskat	k5eAaImAgMnS	ziskat
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
utkáních	utkání	k1gNnPc6	utkání
<g/>
,	,	kIx,	,
přispěla	přispět	k5eAaPmAgFnS	přispět
Kvitová	Kvitová	k1gFnSc1	Kvitová
šesti	šest	k4xCc7	šest
výhrami	výhra	k1gFnPc7	výhra
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přesně	přesně	k6eAd1	přesně
dvěma	dva	k4xCgFnPc7	dva
třetinami	třetina	k1gFnPc7	třetina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moskevském	moskevský	k2eAgNnSc6d1	moskevské
finále	finále	k1gNnSc6	finále
nejprve	nejprve	k6eAd1	nejprve
porazila	porazit	k5eAaPmAgFnS	porazit
Kirilenkovou	Kirilenkový	k2eAgFnSc7d1	Kirilenková
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
Kuzněcovovou	Kuzněcovová	k1gFnSc4	Kuzněcovová
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
třetí	třetí	k4xOgInSc4	třetí
bod	bod	k1gInSc4	bod
následně	následně	k6eAd1	následně
získaly	získat	k5eAaPmAgFnP	získat
Češky	Češka	k1gFnPc1	Češka
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
Ve	v	k7c6	v
Fed	Fed	k1gFnSc6	Fed
Cupu	cup	k1gInSc2	cup
2012	[number]	k4	2012
opět	opět	k6eAd1	opět
výrazně	výrazně	k6eAd1	výrazně
pomohla	pomoct	k5eAaPmAgFnS	pomoct
českému	český	k2eAgInSc3d1	český
týmu	tým	k1gInSc3	tým
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
i	i	k9	i
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
obě	dva	k4xCgFnPc1	dva
své	svůj	k3xOyFgFnSc2	svůj
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
finále	finále	k1gNnSc1	finále
hrané	hraný	k2eAgFnSc2d1	hraná
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
skončilo	skončit	k5eAaPmAgNnS	skončit
opět	opět	k6eAd1	opět
vítězstvím	vítězství	k1gNnSc7	vítězství
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
porazil	porazit	k5eAaPmAgInS	porazit
Srbsko	Srbsko	k1gNnSc4	Srbsko
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dvouhře	dvouhra	k1gFnSc6	dvouhra
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Jankovićovou	Jankovićová	k1gFnSc4	Jankovićová
a	a	k8xC	a
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
dvouhře	dvouhra	k1gFnSc6	dvouhra
pak	pak	k6eAd1	pak
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Ivanovićové	Ivanovićové	k2eAgFnSc1d1	Ivanovićové
<g/>
.	.	kIx.	.
</s>
<s>
Dvěma	dva	k4xCgInPc7	dva
body	bod	k1gInPc7	bod
tentokrát	tentokrát	k6eAd1	tentokrát
k	k	k7c3	k
triumfu	triumf	k1gInSc3	triumf
přispěla	přispět	k5eAaPmAgFnS	přispět
Lucie	Lucie	k1gFnSc1	Lucie
Šafářová	Šafářová	k1gFnSc1	Šafářová
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
Ve	v	k7c6	v
Fed	Fed	k1gFnSc6	Fed
Cupu	cup	k1gInSc2	cup
2013	[number]	k4	2013
plnila	plnit	k5eAaImAgFnS	plnit
opět	opět	k6eAd1	opět
roli	role	k1gFnSc4	role
týmové	týmový	k2eAgFnSc2d1	týmová
jedničky	jednička	k1gFnSc2	jednička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únorovém	únorový	k2eAgMnSc6d1	únorový
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Austrálii	Austrálie	k1gFnSc3	Austrálie
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
ostravské	ostravský	k2eAgFnSc6d1	Ostravská
hale	hala	k1gFnSc6	hala
<g/>
,	,	kIx,	,
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
dvě	dva	k4xCgFnPc4	dva
výhry	výhra	k1gFnPc4	výhra
<g/>
;	;	kIx,	;
nad	nad	k7c7	nad
Gajdošovou	Gajdošová	k1gFnSc7	Gajdošová
a	a	k8xC	a
nad	nad	k7c7	nad
světovou	světový	k2eAgFnSc7d1	světová
devítkou	devítka	k1gFnSc7	devítka
Stosurovou	stosurový	k2eAgFnSc7d1	stosurový
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
musela	muset	k5eAaImAgFnS	muset
odvracet	odvracet	k5eAaImF	odvracet
mečbol	mečbol	k1gInSc4	mečbol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
zajížděly	zajíždět	k5eAaImAgFnP	zajíždět
české	český	k2eAgFnPc1d1	Česká
reprezentantky	reprezentantka	k1gFnPc1	reprezentantka
k	k	k7c3	k
dubnovému	dubnový	k2eAgNnSc3d1	dubnové
semifinále	semifinále	k1gNnSc3	semifinále
na	na	k7c4	na
palermskou	palermský	k2eAgFnSc4d1	Palermská
antuku	antuka	k1gFnSc4	antuka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
čekalo	čekat	k5eAaImAgNnS	čekat
družstvo	družstvo	k1gNnSc1	družstvo
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
dvouhře	dvouhra	k1gFnSc6	dvouhra
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
jednoho	jeden	k4xCgInSc2	jeden
týdnu	týden	k1gInSc6	týden
Robertě	Roberta	k1gFnSc3	Roberta
Vinciové	Vinciový	k2eAgInPc4d1	Vinciový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
jedniček	jednička	k1gFnPc2	jednička
porazila	porazit	k5eAaPmAgFnS	porazit
Erraniovou	Erraniový	k2eAgFnSc4d1	Erraniová
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Vydřená	vydřený	k2eAgFnSc1d1	vydřená
výhra	výhra	k1gFnSc1	výhra
sice	sice	k8xC	sice
dala	dát	k5eAaPmAgFnS	dát
Češkám	Češka	k1gFnPc3	Češka
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
zvrácení	zvrácení	k1gNnSc4	zvrácení
nepříznivého	příznivý	k2eNgInSc2d1	nepříznivý
výsledku	výsledek	k1gInSc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
po	po	k7c6	po
pondělní	pondělní	k2eAgFnSc6d1	pondělní
dohrávce	dohrávka	k1gFnSc6	dohrávka
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
Úvodní	úvodní	k2eAgFnSc4d1	úvodní
únorové	únorový	k2eAgNnSc1d1	únorové
kolo	kolo	k1gNnSc1	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
2014	[number]	k4	2014
zmeškala	zmeškat	k5eAaPmAgFnS	zmeškat
pro	pro	k7c4	pro
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnovém	dubnový	k2eAgNnSc6d1	dubnové
semifinále	semifinále	k1gNnSc6	semifinále
pak	pak	k6eAd1	pak
přispěla	přispět	k5eAaPmAgFnS	přispět
dvěma	dva	k4xCgFnPc7	dva
ostravskými	ostravský	k2eAgFnPc7d1	Ostravská
výhrami	výhra	k1gFnPc7	výhra
nad	nad	k7c7	nad
Camilou	Camilý	k2eAgFnSc7d1	Camilý
Giorgiovou	Giorgiový	k2eAgFnSc7d1	Giorgiový
a	a	k8xC	a
Vinciovou	Vinciový	k2eAgFnSc7d1	Vinciová
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
přes	přes	k7c4	přes
Itálii	Itálie	k1gFnSc4	Itálie
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadovém	listopadový	k2eAgNnSc6d1	listopadové
pražském	pražský	k2eAgNnSc6d1	Pražské
finále	finále	k1gNnSc6	finále
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
po	po	k7c6	po
hladké	hladký	k2eAgFnSc6d1	hladká
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Andreou	Andrea	k1gFnSc7	Andrea
Petkovicovou	Petkovicový	k2eAgFnSc7d1	Petkovicová
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tříhodinové	tříhodinový	k2eAgFnSc6d1	tříhodinová
bitvě	bitva	k1gFnSc6	bitva
s	s	k7c7	s
Angelique	Angelique	k1gFnSc7	Angelique
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
,	,	kIx,	,
hráčkou	hráčka	k1gFnSc7	hráčka
první	první	k4xOgFnPc4	první
světové	světový	k2eAgFnPc4d1	světová
desítky	desítka	k1gFnPc4	desítka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
setu	set	k1gInSc6	set
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
ze	z	k7c2	z
stavu	stav	k1gInSc2	stav
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
čtyři	čtyři	k4xCgInPc4	čtyři
setboly	setbol	k1gInPc1	setbol
<g/>
,	,	kIx,	,
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
set	set	k1gInSc4	set
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
sady	sada	k1gFnSc2	sada
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
vedením	vedení	k1gNnSc7	vedení
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
bojovnice	bojovnice	k1gFnSc1	bojovnice
se	se	k3xPyFc4	se
ale	ale	k9	ale
nevzdala	vzdát	k5eNaPmAgFnS	vzdát
a	a	k8xC	a
set	set	k1gInSc4	set
otočila	otočit	k5eAaPmAgFnS	otočit
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
dějství	dějství	k1gNnSc2	dějství
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
Češka	Češka	k1gFnSc1	Češka
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
když	když	k8xS	když
rychle	rychle	k6eAd1	rychle
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
a	a	k8xC	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
však	však	k9	však
následovala	následovat	k5eAaImAgFnS	následovat
šňůra	šňůra	k1gFnSc1	šňůra
jejích	její	k3xOp3gFnPc2	její
pěti	pět	k4xCc2	pět
gamů	game	k1gInPc2	game
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
dobyly	dobýt	k5eAaPmAgFnP	dobýt
Češky	Češka	k1gFnPc1	Češka
třetí	třetí	k4xOgFnPc1	třetí
trofej	trofej	k1gFnSc1	trofej
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
Z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Kanadě	Kanada	k1gFnSc3	Kanada
se	se	k3xPyFc4	se
omluvila	omluvit	k5eAaPmAgFnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dubnového	dubnový	k2eAgNnSc2d1	dubnové
semifinále	semifinále	k1gNnSc2	semifinále
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
po	po	k7c6	po
pětitýdenní	pětitýdenní	k2eAgFnSc6d1	pětitýdenní
pauze	pauza	k1gFnSc6	pauza
způsobené	způsobený	k2eAgFnSc6d1	způsobená
pocitem	pocit	k1gInSc7	pocit
vyhoření	vyhoření	k1gNnSc2	vyhoření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
však	však	k9	však
testy	test	k1gInPc1	test
prokázaly	prokázat	k5eAaPmAgInP	prokázat
prodělanou	prodělaný	k2eAgFnSc4d1	prodělaná
mononukleózu	mononukleóza	k1gFnSc4	mononukleóza
<g/>
.	.	kIx.	.
</s>
<s>
Dvouhry	dvouhra	k1gFnPc1	dvouhra
nad	nad	k7c7	nad
Mladenovicovou	Mladenovicový	k2eAgFnSc7d1	Mladenovicová
i	i	k8xC	i
Garciaovou	Garciaová	k1gFnSc4	Garciaová
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
tak	tak	k9	tak
týmu	tým	k1gInSc3	tým
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
finále	finále	k1gNnSc6	finále
proti	proti	k7c3	proti
Rusku	Rusko	k1gNnSc3	Rusko
na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Pavljučenkovovou	Pavljučenkovová	k1gFnSc7	Pavljučenkovová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
v	v	k7c6	v
nedělní	nedělní	k2eAgFnSc6d1	nedělní
dvouhře	dvouhra	k1gFnSc6	dvouhra
oplatila	oplatit	k5eAaPmAgFnS	oplatit
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
starou	starý	k2eAgFnSc4d1	stará
porážku	porážka	k1gFnSc4	porážka
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
<g/>
.	.	kIx.	.
</s>
<s>
Češky	Češka	k1gFnPc1	Češka
přesto	přesto	k8xC	přesto
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
trofej	trofej	k1gFnSc4	trofej
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
deblu	debl	k1gInSc6	debl
získaly	získat	k5eAaPmAgFnP	získat
rozdílový	rozdílový	k2eAgInSc4d1	rozdílový
bod	bod	k1gInSc4	bod
a	a	k8xC	a
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
hrála	hrát	k5eAaImAgFnS	hrát
jako	jako	k9	jako
jednička	jednička	k1gFnSc1	jednička
družstva	družstvo	k1gNnSc2	družstvo
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
patřilo	patřit	k5eAaImAgNnS	patřit
deváté	devátý	k4xOgNnSc1	devátý
místo	místo	k1gNnSc1	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
poprvé	poprvé	k6eAd1	poprvé
prohrála	prohrát	k5eAaPmAgFnS	prohrát
obě	dva	k4xCgFnPc4	dva
dvouhry	dvouhra	k1gFnPc4	dvouhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Moniku	Monika	k1gFnSc4	Monika
Niculescuovou	Niculescuová	k1gFnSc4	Niculescuová
a	a	k8xC	a
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
pak	pak	k6eAd1	pak
ani	ani	k8xC	ani
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
trojku	trojka	k1gFnSc4	trojka
Simonu	Simona	k1gFnSc4	Simona
Halepovou	Halepová	k1gFnSc4	Halepová
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
tři	tři	k4xCgInPc1	tři
body	bod	k1gInPc1	bod
však	však	k8xC	však
Češky	Češka	k1gFnPc1	Češka
získaly	získat	k5eAaPmAgFnP	získat
a	a	k8xC	a
postoupily	postoupit	k5eAaPmAgFnP	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pokles	pokles	k1gInSc4	pokles
formy	forma	k1gFnSc2	forma
se	se	k3xPyFc4	se
během	během	k7c2	během
Miami	Miami	k1gNnSc2	Miami
Open	Open	k1gMnSc1	Open
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
s	s	k7c7	s
kapitánem	kapitán	k1gMnSc7	kapitán
Pálou	Pála	k1gMnSc7	Pála
<g/>
,	,	kIx,	,
že	že	k8xS	že
vynechá	vynechat	k5eAaPmIp3nS	vynechat
lucernské	lucernský	k2eAgNnSc4d1	lucernský
semifinále	semifinále	k1gNnSc4	semifinále
proti	proti	k7c3	proti
Švýcarsku	Švýcarsko	k1gNnSc3	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
štrasburském	štrasburský	k2eAgNnSc6d1	štrasburské
finále	finále	k1gNnSc6	finále
proti	proti	k7c3	proti
Francií	Francie	k1gFnSc7	Francie
prohrála	prohrát	k5eAaPmAgFnS	prohrát
sobotní	sobotní	k2eAgFnSc4d1	sobotní
dvouhru	dvouhra	k1gFnSc4	dvouhra
s	s	k7c7	s
Caroline	Carolin	k1gInSc5	Carolin
Garciaovou	Garciaová	k1gFnSc4	Garciaová
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
semifinále	semifinále	k1gNnSc2	semifinále
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
neplnila	plnit	k5eNaImAgFnS	plnit
roli	role	k1gFnSc4	role
české	český	k2eAgFnSc2d1	Česká
jedničky	jednička	k1gFnSc2	jednička
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dvojky	dvojka	k1gFnPc1	dvojka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nedělního	nedělní	k2eAgInSc2d1	nedělní
programu	program	k1gInSc2	program
již	již	k6eAd1	již
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
pro	pro	k7c4	pro
diagnostikovaný	diagnostikovaný	k2eAgInSc4d1	diagnostikovaný
edém	edém	k1gInSc4	edém
nártu	nárt	k1gInSc2	nárt
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
na	na	k7c4	na
bolestivost	bolestivost	k1gFnSc4	bolestivost
nohy	noha	k1gFnSc2	noha
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
již	již	k6eAd1	již
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
týdnu	týden	k1gInSc6	týden
v	v	k7c6	v
Ču-chaji	Čuhaj	k1gFnSc6	Ču-chaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
ročníku	ročník	k1gInSc6	ročník
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
žádný	žádný	k3yNgInSc4	žádný
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
naposledy	naposledy	k6eAd1	naposledy
stalo	stát	k5eAaPmAgNnS	stát
při	při	k7c6	při
debutovém	debutový	k2eAgInSc6d1	debutový
startu	start	k1gInSc6	start
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
během	během	k7c2	během
sezóny	sezóna	k1gFnSc2	sezóna
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jen	jen	k6eAd1	jen
jednu	jeden	k4xCgFnSc4	jeden
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Češky	Češka	k1gFnPc1	Češka
přesto	přesto	k6eAd1	přesto
zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Pátou	pátý	k4xOgFnSc7	pátý
trofejí	trofej	k1gFnSc7	trofej
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
českou	český	k2eAgFnSc7d1	Česká
reprezentantkou	reprezentantka	k1gFnSc7	reprezentantka
po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
čtyř	čtyři	k4xCgInPc2	čtyři
titulů	titul	k1gInPc2	titul
Heleny	Helena	k1gFnSc2	Helena
Sukové	Suková	k1gFnSc2	Suková
z	z	k7c2	z
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tenis	tenis	k1gInSc4	tenis
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
dvorcích	dvorec	k1gInPc6	dvorec
klubu	klub	k1gInSc2	klub
TJ	tj	kA	tj
Fulnek	Fulnek	k1gMnSc1	Fulnek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
šestnácti	šestnáct	k4xCc2	šestnáct
let	léto	k1gNnPc2	léto
ji	on	k3xPp3gFnSc4	on
zde	zde	k6eAd1	zde
trénoval	trénovat	k5eAaImAgMnS	trénovat
otec	otec	k1gMnSc1	otec
Jiří	Jiří	k1gMnSc1	Jiří
Kvita	Kvita	k1gMnSc1	Kvita
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
přestoupila	přestoupit	k5eAaPmAgFnS	přestoupit
do	do	k7c2	do
extraligového	extraligový	k2eAgInSc2d1	extraligový
oddílu	oddíl	k1gInSc2	oddíl
TK	TK	kA	TK
Agrofert	Agrofert	k1gInSc1	Agrofert
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
osobním	osobní	k2eAgMnSc7d1	osobní
koučem	kouč	k1gMnSc7	kouč
David	David	k1gMnSc1	David
Kotyza	Kotyz	k1gMnSc2	Kotyz
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vedl	vést	k5eAaImAgMnS	vést
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
oznámila	oznámit	k5eAaPmAgFnS	oznámit
ukončení	ukončení	k1gNnSc4	ukončení
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
do	do	k7c2	do
US	US	kA	US
Open	Open	k1gInSc4	Open
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
trenérem	trenér	k1gMnSc7	trenér
bývalý	bývalý	k2eAgMnSc1d1	bývalý
deblový	deblový	k2eAgMnSc1d1	deblový
specialista	specialista	k1gMnSc1	specialista
František	František	k1gMnSc1	František
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zářijovém	zářijový	k2eAgInSc6d1	zářijový
Toray	Toraa	k1gFnSc2	Toraa
Pan	Pan	k1gMnSc1	Pan
Pacific	Pacific	k1gMnSc1	Pacific
Open	Open	k1gInSc4	Open
2016	[number]	k4	2016
hrála	hrát	k5eAaImAgFnS	hrát
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
belgického	belgický	k2eAgMnSc2d1	belgický
kouče	kouč	k1gMnSc2	kouč
Wima	Wimus	k1gMnSc2	Wimus
Fissetta	Fissett	k1gMnSc2	Fissett
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
na	na	k7c4	na
týdenní	týdenní	k2eAgNnSc4d1	týdenní
testovací	testovací	k2eAgNnSc4d1	testovací
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Fissett	Fissett	k1gInSc1	Fissett
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
připravoval	připravovat	k5eAaImAgMnS	připravovat
světové	světový	k2eAgFnPc4d1	světová
jedničky	jednička	k1gFnPc4	jednička
Kim	Kim	k1gFnSc4	Kim
Clijstersovou	Clijstersová	k1gFnSc4	Clijstersová
a	a	k8xC	a
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovovou	Azarenkovový	k2eAgFnSc4d1	Azarenkovový
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
se	se	k3xPyFc4	se
jejím	její	k3xOp3gMnSc7	její
trenérem	trenér	k1gMnSc7	trenér
stane	stanout	k5eAaPmIp3nS	stanout
bývalý	bývalý	k2eAgMnSc1d1	bývalý
tenista	tenista	k1gMnSc1	tenista
Jiří	Jiří	k1gMnSc1	Jiří
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
dříve	dříve	k6eAd2	dříve
ukončil	ukončit	k5eAaPmAgInS	ukončit
spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
světovou	světový	k2eAgFnSc7d1	světová
šestkou	šestka	k1gFnSc7	šestka
Karolínou	Karolína	k1gFnSc7	Karolína
Plíškovou	plíškový	k2eAgFnSc7d1	Plíšková
<g/>
.	.	kIx.	.
</s>
<s>
Kondičními	kondiční	k2eAgMnPc7d1	kondiční
trenéry	trenér	k1gMnPc7	trenér
postupně	postupně	k6eAd1	postupně
byli	být	k5eAaImAgMnP	být
Kristian	Kristian	k1gMnSc1	Kristian
Bajza	Bajza	k1gMnSc1	Bajza
<g/>
,	,	kIx,	,
Slováci	Slovák	k1gMnPc1	Slovák
Jozef	Jozef	k1gMnSc1	Jozef
Ivanko	Ivanka	k1gFnSc5	Ivanka
a	a	k8xC	a
Marek	Marek	k1gMnSc1	Marek
Všetíček	Všetíček	k1gMnSc1	Všetíček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
připravoval	připravovat	k5eAaImAgMnS	připravovat
od	od	k7c2	od
září	září	k1gNnSc2	září
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
služeb	služba	k1gFnPc2	služba
využila	využít	k5eAaPmAgFnS	využít
díky	díky	k7c3	díky
probíhajícímu	probíhající	k2eAgInSc3d1	probíhající
partnerskému	partnerský	k2eAgInSc3d1	partnerský
vztahu	vztah	k1gInSc3	vztah
se	s	k7c7	s
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
také	také	k9	také
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
skončení	skončení	k1gNnSc2	skončení
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2014	[number]	k4	2014
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
téže	týž	k3xTgFnSc2	týž
sezóny	sezóna	k1gFnSc2	sezóna
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
převzal	převzít	k5eAaPmAgMnS	převzít
Slovák	Slovák	k1gMnSc1	Slovák
Branislav	Branislav	k1gMnSc1	Branislav
Bundzik	Bundzik	k1gMnSc1	Bundzik
<g/>
,	,	kIx,	,
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
prostějovském	prostějovský	k2eAgInSc6d1	prostějovský
tenisovém	tenisový	k2eAgInSc6d1	tenisový
klubu	klub	k1gInSc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
angažovala	angažovat	k5eAaBmAgFnS	angažovat
německého	německý	k2eAgMnSc4d1	německý
fyzioterapeuta	fyzioterapeut	k1gMnSc4	fyzioterapeut
Alexe	Alex	k1gMnSc5	Alex
Stobera	Stobero	k1gNnPc4	Stobero
se	s	k7c7	s
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
z	z	k7c2	z
přípravy	příprava	k1gFnSc2	příprava
Samprase	Samprasa	k1gFnSc3	Samprasa
<g/>
,	,	kIx,	,
Agassiho	Agassi	k1gMnSc4	Agassi
i	i	k8xC	i
Li	li	k9	li
Na	na	k7c6	na
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
madridským	madridský	k2eAgInSc7d1	madridský
turnajem	turnaj	k1gInSc7	turnaj
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
ukončila	ukončit	k5eAaPmAgFnS	ukončit
s	s	k7c7	s
Němcem	Němec	k1gMnSc7	Němec
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
se	se	k3xPyFc4	se
kondičním	kondiční	k2eAgMnSc7d1	kondiční
trenérem	trenér	k1gMnSc7	trenér
stal	stát	k5eAaPmAgMnS	stát
bývalý	bývalý	k2eAgMnSc1d1	bývalý
triatlonista	triatlonista	k1gMnSc1	triatlonista
David	David	k1gMnSc1	David
Vydra	Vydra	k1gMnSc1	Vydra
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
týden	týden	k1gInSc1	týden
předtím	předtím	k6eAd1	předtím
rozešel	rozejít	k5eAaPmAgInS	rozejít
s	s	k7c7	s
Rosolem	rosol	k1gInSc7	rosol
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
vedl	vést	k5eAaImAgMnS	vést
Berdycha	Berdycha	k1gMnSc1	Berdycha
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
trenérského	trenérský	k2eAgInSc2d1	trenérský
týmu	tým	k1gInSc2	tým
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
srpnové	srpnový	k2eAgFnSc2d1	srpnová
US	US	kA	US
Open	Open	k1gMnSc1	Open
Series	Series	k1gMnSc1	Series
2015	[number]	k4	2015
také	také	k6eAd1	také
psycholog	psycholog	k1gMnSc1	psycholog
Michal	Michal	k1gMnSc1	Michal
Šafář	Šafář	k1gMnSc1	Šafář
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
Fulneku	Fulnek	k1gInSc6	Fulnek
nedaleko	nedaleko	k7c2	nedaleko
Bílovce	Bílovec	k1gInSc2	Bílovec
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
učitele	učitel	k1gMnSc2	učitel
Jiřího	Jiří	k1gMnSc2	Jiří
Kvity	kvit	k1gInPc4	kvit
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
místostarosty	místostarosta	k1gMnSc2	místostarosta
Fulneku	Fulnek	k1gInSc2	Fulnek
<g/>
,	,	kIx,	,
a	a	k8xC	a
Pavly	Pavla	k1gFnPc1	Pavla
Kvitové	Kvitový	k2eAgFnPc1d1	Kvitová
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
starší	starý	k2eAgMnPc4d2	starší
bratry	bratr	k1gMnPc4	bratr
Jiřího	Jiří	k1gMnSc4	Jiří
(	(	kIx(	(
<g/>
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
)	)	kIx)	)
a	a	k8xC	a
Libora	Libor	k1gMnSc2	Libor
(	(	kIx(	(
<g/>
učitel	učitel	k1gMnSc1	učitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
hrála	hrát	k5eAaImAgFnS	hrát
nebo	nebo	k8xC	nebo
hraje	hrát	k5eAaImIp3nS	hrát
tenis	tenis	k1gInSc1	tenis
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
tenisový	tenisový	k2eAgMnSc1d1	tenisový
trenér	trenér	k1gMnSc1	trenér
3	[number]	k4	3
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
od	od	k7c2	od
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
trénoval	trénovat	k5eAaImAgMnS	trénovat
dceru	dcera	k1gFnSc4	dcera
Petru	Petr	k1gMnSc3	Petr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
skončil	skončit	k5eAaPmAgInS	skončit
její	její	k3xOp3gInSc4	její
partnerský	partnerský	k2eAgInSc4d1	partnerský
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
tenistou	tenista	k1gMnSc7	tenista
Adamem	Adam	k1gMnSc7	Adam
Pavláskem	Pavlásek	k1gMnSc7	Pavlásek
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
prvním	první	k4xOgMnSc7	první
hráčem	hráč	k1gMnSc7	hráč
celostátního	celostátní	k2eAgInSc2d1	celostátní
žebříčku	žebříček	k1gInSc2	žebříček
v	v	k7c6	v
mladších	mladý	k2eAgFnPc6d2	mladší
i	i	k8xC	i
starších	starý	k2eAgFnPc6d2	starší
žácích	žák	k1gMnPc6	žák
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
sezóně	sezóna	k1gFnSc6	sezóna
jejím	její	k3xOp3gMnSc7	její
partnerem	partner	k1gMnSc7	partner
stal	stát	k5eAaPmAgMnS	stát
další	další	k2eAgMnSc1d1	další
český	český	k2eAgMnSc1d1	český
tenista	tenista	k1gMnSc1	tenista
Radek	Radek	k1gMnSc1	Radek
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
se	se	k3xPyFc4	se
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
tweetem	tweet	k1gMnSc7	tweet
sdělila	sdělit	k5eAaPmAgFnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
navázala	navázat	k5eAaPmAgFnS	navázat
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
hokejistou	hokejista	k1gMnSc7	hokejista
Radkem	Radek	k1gMnSc7	Radek
Meidlem	Meidlo	k1gNnSc7	Meidlo
<g/>
,	,	kIx,	,
hrajícím	hrající	k2eAgMnSc7d1	hrající
za	za	k7c4	za
polský	polský	k2eAgInSc4d1	polský
klub	klub	k1gInSc4	klub
v	v	k7c6	v
Bytomi	Byto	k1gFnPc7	Byto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
týdnu	týden	k1gInSc6	týden
před	před	k7c7	před
listopadovým	listopadový	k2eAgNnSc7d1	listopadové
finále	finále	k1gNnSc7	finále
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
2015	[number]	k4	2015
se	se	k3xPyFc4	se
pár	pár	k4xCyI	pár
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
oznámila	oznámit	k5eAaPmAgFnS	oznámit
zrušení	zrušení	k1gNnSc4	zrušení
zásnub	zásnub	k1gInSc1	zásnub
a	a	k8xC	a
rozchod	rozchod	k1gInSc1	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
daňovou	daňový	k2eAgFnSc7d1	daňová
rezidentkou	rezidentka	k1gFnSc7	rezidentka
Monaka	Monako	k1gNnSc2	Monako
kvůli	kvůli	k7c3	kvůli
nižším	nízký	k2eAgFnPc3d2	nižší
daním	daň	k1gFnPc3	daň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
byla	být	k5eAaImAgFnS	být
kritizována	kritizován	k2eAgFnSc1d1	kritizována
sociálnědemokratickým	sociálnědemokratický	k2eAgMnSc7d1	sociálnědemokratický
poslancem	poslanec	k1gMnSc7	poslanec
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Humlem	Huml	k1gMnSc7	Huml
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
postoj	postoj	k1gInSc1	postoj
však	však	k9	však
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
mediální	mediální	k2eAgFnSc4d1	mediální
odezvu	odezva	k1gFnSc4	odezva
a	a	k8xC	a
nesouhlasné	souhlasný	k2eNgFnPc4d1	nesouhlasná
reakce	reakce	k1gFnPc4	reakce
dalších	další	k2eAgMnPc2d1	další
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Tenistka	tenistka	k1gFnSc1	tenistka
je	být	k5eAaImIp3nS	být
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
Centra	centrum	k1gNnSc2	centrum
sportu	sport	k1gInSc2	sport
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2011	[number]	k4	2011
jí	on	k3xPp3gFnSc3	on
zastupitelé	zastupitel	k1gMnPc1	zastupitel
Fulneka	Fulneko	k1gNnSc2	Fulneko
udělili	udělit	k5eAaPmAgMnP	udělit
čestné	čestný	k2eAgNnSc4d1	čestné
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
druhou	druhý	k4xOgFnSc7	druhý
držitelkou	držitelka	k1gFnSc7	držitelka
ocenění	ocenění	k1gNnSc2	ocenění
po	po	k7c6	po
Janu	Jan	k1gMnSc6	Jan
Amosi	Amos	k1gMnSc6	Amos
Komenském	Komenský	k1gMnSc6	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc4d1	aktivní
tenisovou	tenisový	k2eAgFnSc4d1	tenisová
kariéru	kariéra	k1gFnSc4	kariéra
jí	on	k3xPp3gFnSc2	on
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
astma	astma	k1gNnSc1	astma
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
přepadena	přepaden	k2eAgFnSc1d1	přepadena
a	a	k8xC	a
následně	následně	k6eAd1	následně
poraněna	poraněn	k2eAgFnSc1d1	poraněna
neznámým	známý	k2eNgMnSc7d1	neznámý
pachatelem	pachatel	k1gMnSc7	pachatel
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
snažila	snažit	k5eAaImAgFnS	snažit
bránit	bránit	k5eAaImF	bránit
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
nožem	nůž	k1gInSc7	nůž
způsobil	způsobit	k5eAaPmAgMnS	způsobit
řezné	řezný	k2eAgFnPc4d1	řezná
rány	rána	k1gFnPc4	rána
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
drží	držet	k5eAaImIp3nS	držet
raketu	raketa	k1gFnSc4	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
operaci	operace	k1gFnSc3	operace
ve	v	k7c6	v
specializovaném	specializovaný	k2eAgNnSc6d1	specializované
zařízení	zařízení	k1gNnSc6	zařízení
<g/>
,	,	kIx,	,
Ústavu	ústav	k1gInSc6	ústav
chirurgie	chirurgie	k1gFnSc2	chirurgie
ruky	ruka	k1gFnSc2	ruka
a	a	k8xC	a
plastické	plastický	k2eAgFnSc2d1	plastická
chirurgie	chirurgie	k1gFnSc2	chirurgie
ve	v	k7c6	v
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgInSc4d1	trvající
3.45	[number]	k4	3.45
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
masivním	masivní	k2eAgInSc6d1	masivní
zákroku	zákrok	k1gInSc6	zákrok
těžkého	těžký	k2eAgNnSc2d1	těžké
poranění	poranění	k1gNnSc2	poranění
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sešití	sešití	k1gNnPc2	sešití
dvou	dva	k4xCgInPc2	dva
nervů	nerv	k1gInPc2	nerv
a	a	k8xC	a
šlach	šlacha	k1gFnPc2	šlacha
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
středních	střední	k2eAgInPc2d1	střední
článků	článek	k1gInPc2	článek
prstů	prst	k1gInPc2	prst
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Pooperační	pooperační	k2eAgFnSc1d1	pooperační
prognóza	prognóza	k1gFnSc1	prognóza
uváděla	uvádět	k5eAaImAgFnS	uvádět
minimálně	minimálně	k6eAd1	minimálně
šestiměsíční	šestiměsíční	k2eAgFnSc4d1	šestiměsíční
rekonvalescenci	rekonvalescence	k1gFnSc4	rekonvalescence
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
vyšel	vyjít	k5eAaPmAgMnS	vyjít
na	na	k7c6	na
kupónu	kupón	k1gInSc6	kupón
poštovní	poštovní	k2eAgFnSc2d1	poštovní
známky	známka	k1gFnSc2	známka
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
10	[number]	k4	10
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
Gratulační	gratulační	k2eAgFnSc2d1	gratulační
kytice	kytice	k1gFnSc2	kytice
<g/>
)	)	kIx)	)
přítisk	přítisk	k1gInSc1	přítisk
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
fotografie	fotografie	k1gFnSc1	fotografie
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
s	s	k7c7	s
wimbledonskou	wimbledonský	k2eAgFnSc7d1	wimbledonská
trofejí	trofej	k1gFnSc7	trofej
mísou	mísa	k1gFnSc7	mísa
Venus	Venus	k1gMnSc1	Venus
Rosewater	Rosewater	k1gMnSc1	Rosewater
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
<g />
.	.	kIx.	.
</s>
<s>
pořízena	pořízen	k2eAgFnSc1d1	pořízena
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
vítězství	vítězství	k1gNnSc6	vítězství
ve	v	k7c6	v
wimbledonském	wimbledonský	k2eAgNnSc6d1	wimbledonské
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
tenistkou	tenistka	k1gFnSc7	tenistka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dokázala	dokázat	k5eAaPmAgFnS	dokázat
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vyhrát	vyhrát	k5eAaPmF	vyhrát
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
<g/>
,	,	kIx,	,
Turnaj	turnaj	k1gInSc4	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
i	i	k8xC	i
Fed	Fed	k1gFnSc1	Fed
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
stejná	stejný	k2eAgFnSc1d1	stejná
věc	věc	k1gFnSc1	věc
podařila	podařit	k5eAaPmAgFnS	podařit
pouze	pouze	k6eAd1	pouze
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
pak	pak	k6eAd1	pak
totéž	týž	k3xTgNnSc1	týž
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
Američanka	Američanka	k1gFnSc1	Američanka
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
Davenportová	Davenportová	k1gFnSc1	Davenportová
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Státní	státní	k2eAgFnSc2d1	státní
opery	opera	k1gFnSc2	opera
Praha	Praha	k1gFnSc1	Praha
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
předávání	předávání	k1gNnSc2	předávání
cen	cena	k1gFnPc2	cena
vítězům	vítěz	k1gMnPc3	vítěz
hudební	hudební	k2eAgFnSc2d1	hudební
ankety	anketa	k1gFnSc2	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Předávala	předávat	k5eAaImAgFnS	předávat
zde	zde	k6eAd1	zde
cenu	cena	k1gFnSc4	cena
nejlepší	dobrý	k2eAgFnSc3d3	nejlepší
zpěvačce	zpěvačka	k1gFnSc3	zpěvačka
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
Lucii	Lucie	k1gFnSc6	Lucie
Bílé	bílý	k2eAgFnSc6d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
na	na	k7c4	na
jeviště	jeviště	k1gNnSc4	jeviště
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
potleskem	potlesk	k1gInSc7	potlesk
vestoje	vestoje	k6eAd1	vestoje
<g/>
,	,	kIx,	,
obdržela	obdržet	k5eAaPmAgFnS	obdržet
historicky	historicky	k6eAd1	historicky
prvního	první	k4xOgInSc2	první
mimohudebního	mimohudební	k2eAgInSc2d1	mimohudební
Českého	český	k2eAgInSc2d1	český
slavíka	slavík	k1gInSc2	slavík
za	za	k7c4	za
tenis	tenis	k1gInSc4	tenis
<g/>
,	,	kIx,	,
od	od	k7c2	od
Lucie	Lucie	k1gFnSc2	Lucie
Bílé	bílé	k1gNnSc1	bílé
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
navíc	navíc	k6eAd1	navíc
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
její	její	k3xOp3gFnSc4	její
kytici	kytice	k1gFnSc4	kytice
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
se	se	k3xPyFc4	se
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
předávání	předávání	k1gNnSc2	předávání
cen	cena	k1gFnPc2	cena
vítězům	vítěz	k1gMnPc3	vítěz
tradiční	tradiční	k2eAgFnSc2d1	tradiční
divácké	divácký	k2eAgFnSc2d1	divácká
ankety	anketa	k1gFnSc2	anketa
TýTý	TýTý	k1gFnSc2	TýTý
<g/>
.	.	kIx.	.
</s>
<s>
Předávala	předávat	k5eAaImAgFnS	předávat
tu	tu	k6eAd1	tu
zakřivené	zakřivený	k2eAgNnSc4d1	zakřivené
zrcátko	zrcátko	k1gNnSc4	zrcátko
vítězi	vítěz	k1gMnPc7	vítěz
kategorie	kategorie	k1gFnSc2	kategorie
Sportovní	sportovní	k2eAgMnSc1d1	sportovní
moderátor	moderátor	k1gMnSc1	moderátor
Vojtěchu	Vojtěch	k1gMnSc3	Vojtěch
Bernatskému	Bernatský	k2eAgMnSc3d1	Bernatský
<g/>
.	.	kIx.	.
</s>
<s>
Přítomní	přítomný	k2eAgMnPc1d1	přítomný
diváci	divák	k1gMnPc1	divák
ji	on	k3xPp3gFnSc4	on
ocenili	ocenit	k5eAaPmAgMnP	ocenit
mohutným	mohutný	k2eAgInSc7d1	mohutný
potleskem	potlesk	k1gInSc7	potlesk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kanár	kanár	k1gInSc1	kanár
a	a	k8xC	a
Ceny	cena	k1gFnPc1	cena
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kanár	kanár	k1gInSc1	kanár
-	-	kIx~	-
talent	talent	k1gInSc1	talent
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
WTA	WTA	kA	WTA
-	-	kIx~	-
nováček	nováček	k1gMnSc1	nováček
roku	rok	k1gInSc2	rok
Zlatý	zlatý	k2eAgInSc4d1	zlatý
kanár	kanár	k1gInSc4	kanár
-	-	kIx~	-
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
tenistka	tenistka	k1gFnSc1	tenistka
roku	rok	k1gInSc2	rok
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
posun	posun	k1gInSc1	posun
na	na	k7c4	na
žebřičku	žebřička	k1gFnSc4	žebřička
WTA	WTA	kA	WTA
2011	[number]	k4	2011
ITF	ITF	kA	ITF
-	-	kIx~	-
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
WTA	WTA	kA	WTA
-	-	kIx~	-
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hráčka	hráčka	k1gFnSc1	hráčka
roku	rok	k1gInSc2	rok
WTA	WTA	kA	WTA
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
hráčka	hráčka	k1gFnSc1	hráčka
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
zlepšením	zlepšení	k1gNnSc7	zlepšení
WTA	WTA	kA	WTA
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Karen	Karna	k1gFnPc2	Karna
Krantzckeové	Krantzckeové	k2eAgInPc2d1	Krantzckeové
za	za	k7c4	za
sportovní	sportovní	k2eAgNnSc4d1	sportovní
chování	chování	k1gNnSc4	chování
WTA	WTA	kA	WTA
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
fanoušků	fanoušek	k1gMnPc2	fanoušek
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
pokrok	pokrok	k1gInSc4	pokrok
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
ČR	ČR	kA	ČR
-	-	kIx~	-
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
sportovec	sportovec	k1gMnSc1	sportovec
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
rozdílem	rozdíl	k1gInSc7	rozdíl
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
ankety	anketa	k1gFnSc2	anketa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
kolektiv	kolektiv	k1gInSc1	kolektiv
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
členka	členka	k1gFnSc1	členka
fedcupového	fedcupový	k2eAgInSc2d1	fedcupový
týmu	tým	k1gInSc2	tým
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kanár	kanár	k1gInSc1	kanár
-	-	kIx~	-
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kanár	kanár	k1gInSc1	kanár
a	a	k8xC	a
hráčka	hráčka	k1gFnSc1	hráčka
roku	rok	k1gInSc2	rok
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
sportovec	sportovec	k1gMnSc1	sportovec
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
Cena	cena	k1gFnSc1	cena
Jiřího	Jiří	k1gMnSc2	Jiří
Gutha-Jarkovského	Gutha-Jarkovský	k2eAgMnSc2d1	Gutha-Jarkovský
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
sportovní	sportovní	k2eAgInSc4d1	sportovní
výkon	výkon	k1gInSc4	výkon
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
2012	[number]	k4	2012
Světová	světový	k2eAgFnSc1d1	světová
sportovní	sportovní	k2eAgFnSc1d1	sportovní
cena	cena	k1gFnSc1	cena
Laureus	Laureus	k1gMnSc1	Laureus
-	-	kIx~	-
nominace	nominace	k1gFnSc1	nominace
<g/>
,	,	kIx,	,
sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kanár	kanár	k1gInSc1	kanár
-	-	kIx~	-
hráčka	hráčka	k1gFnSc1	hráčka
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kanár	kanár	k1gInSc1	kanár
-	-	kIx~	-
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g />
.	.	kIx.	.
</s>
<s>
tenistka	tenistka	k1gFnSc1	tenistka
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
WTA	WTA	kA	WTA
-	-	kIx~	-
Diamond	Diamond	k1gInSc1	Diamond
Aces	Acesa	k1gFnPc2	Acesa
za	za	k7c4	za
propagaci	propagace	k1gFnSc4	propagace
tenisu	tenis	k1gInSc2	tenis
WTA	WTA	kA	WTA
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Karen	Karna	k1gFnPc2	Karna
Krantzckeové	Krantzckeové	k2eAgInPc2d1	Krantzckeové
za	za	k7c4	za
sportovní	sportovní	k2eAgNnSc4d1	sportovní
chování	chování	k1gNnSc4	chování
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
s	s	k7c7	s
nejmenším	malý	k2eAgInSc7d3	nejmenší
rozdílem	rozdíl	k1gInSc7	rozdíl
-	-	kIx~	-
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
ankety	anketa	k1gFnSc2	anketa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
kolektiv	kolektiv	k1gInSc1	kolektiv
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
členka	členka	k1gFnSc1	členka
fedcupového	fedcupový	k2eAgInSc2d1	fedcupový
týmu	tým	k1gInSc2	tým
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kanár	kanár	k1gInSc1	kanár
-	-	kIx~	-
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kanár	kanár	k1gInSc1	kanár
a	a	k8xC	a
hráčka	hráčka	k1gFnSc1	hráčka
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
WTA	WTA	kA	WTA
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Karen	Karna	k1gFnPc2	Karna
Krantzckeové	Krantzckeové	k2eAgInPc2d1	Krantzckeové
za	za	k7c4	za
sportovní	sportovní	k2eAgNnSc4d1	sportovní
chování	chování	k1gNnSc4	chování
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kanár	kanár	k1gInSc1	kanár
-	-	kIx~	-
hráčka	hráčka	k1gFnSc1	hráčka
roku	rok	k1gInSc2	rok
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Hráčské	hráčský	k2eAgFnSc2d1	hráčská
statistiky	statistika	k1gFnSc2	statistika
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
a	a	k8xC	a
Seznam	seznam	k1gInSc4	seznam
odehraných	odehraný	k2eAgInPc2d1	odehraný
turnajů	turnaj	k1gInPc2	turnaj
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
<g/>
.	.	kIx.	.
</s>
<s>
Petra	Petra	k1gFnSc1	Petra
Kvitová	Kvitová	k1gFnSc1	Kvitová
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
devatenáct	devatenáct	k4xCc4	devatenáct
turnajů	turnaj	k1gInPc2	turnaj
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
sedm	sedm	k4xCc1	sedm
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ITF	ITF	kA	ITF
<g/>
.	.	kIx.	.
</s>
