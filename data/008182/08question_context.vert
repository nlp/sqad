<s>
Obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
mírou	míra	k1gFnSc7	míra
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
charakteristikou	charakteristika	k1gFnSc7	charakteristika
velikosti	velikost	k1gFnSc2	velikost
<g/>
)	)	kIx)	)
dané	daný	k2eAgFnSc6d1	daná
dvourozměrné	dvourozměrný	k2eAgFnSc6d1	dvourozměrná
části	část	k1gFnSc6	část
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
písmenem	písmeno	k1gNnSc7	písmeno
S	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
