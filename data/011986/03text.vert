<p>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívaná	využívaný	k2eAgFnSc1d1	využívaná
půda	půda	k1gFnSc1	půda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
využívaná	využívaný	k2eAgFnSc1d1	využívaná
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
zemědělství	zemědělství	k1gNnSc2	zemědělství
či	či	k8xC	či
pastevectví	pastevectví	k1gNnSc2	pastevectví
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
pole	pole	k1gNnSc4	pole
(	(	kIx(	(
<g/>
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
<g/>
)	)	kIx)	)
a	a	k8xC	a
louky	louka	k1gFnPc4	louka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pastviny	pastvina	k1gFnPc1	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
též	též	k9	též
tato	tento	k3xDgFnSc1	tento
půda	půda	k1gFnSc1	půda
využívána	využívat	k5eAaImNgFnS	využívat
ovocnými	ovocný	k2eAgInPc7d1	ovocný
sady	sad	k1gInPc7	sad
<g/>
,	,	kIx,	,
vinicemi	vinice	k1gFnPc7	vinice
či	či	k8xC	či
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
určitou	určitý	k2eAgFnSc7d1	určitá
vrstvou	vrstva	k1gFnSc7	vrstva
úrodné	úrodný	k2eAgFnSc2d1	úrodná
půdy	půda	k1gFnSc2	půda
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
zvané	zvaný	k2eAgFnSc2d1	zvaná
ornice	ornice	k1gFnSc2	ornice
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
pod	pod	k7c7	pod
zápojem	zápoj	k1gInSc7	zápoj
porostu	porost	k1gInSc2	porost
na	na	k7c6	na
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pod	pod	k7c7	pod
travnatými	travnatý	k2eAgInPc7d1	travnatý
ekosystémy	ekosystém	k1gInPc7	ekosystém
vznikají	vznikat	k5eAaImIp3nP	vznikat
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
černozemě	černozemě	k1gFnSc1	černozemě
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednak	jednak	k8xC	jednak
dáno	dán	k2eAgNnSc1d1	dáno
prokořeněním	prokořenění	k1gNnSc7	prokořenění
a	a	k8xC	a
přítomností	přítomnost	k1gFnSc7	přítomnost
žížal	žížala	k1gFnPc2	žížala
<g/>
,	,	kIx,	,
krtků	krtek	k1gMnPc2	krtek
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pomalým	pomalý	k2eAgInSc7d1	pomalý
rozkladem	rozklad	k1gInSc7	rozklad
zbytků	zbytek	k1gInPc2	zbytek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
či	či	k8xC	či
vazbou	vazba	k1gFnSc7	vazba
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
dusíku	dusík	k1gInSc2	dusík
symbiotickými	symbiotický	k2eAgFnPc7d1	symbiotická
bakteriemi	bakterie	k1gFnPc7	bakterie
jetelovin	jetelovina	k1gFnPc2	jetelovina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
tyto	tento	k3xDgFnPc1	tento
půdy	půda	k1gFnPc1	půda
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
do	do	k7c2	do
bonitovaných	bonitovaný	k2eAgFnPc2d1	bonitovaná
půdně	půdně	k6eAd1	půdně
ekologických	ekologický	k2eAgFnPc2d1	ekologická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
legislativního	legislativní	k2eAgInSc2d1	legislativní
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
právě	právě	k9	právě
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
historického	historický	k2eAgInSc2d1	historický
úhlu	úhel	k1gInSc2	úhel
pohledu	pohled	k1gInSc2	pohled
můžeme	moct	k5eAaImIp1nP	moct
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
různé	různý	k2eAgInPc4d1	různý
systémy	systém	k1gInPc4	systém
hospodaření	hospodaření	k1gNnSc2	hospodaření
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xS	jak
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
k	k	k7c3	k
postupnému	postupný	k2eAgInSc3d1	postupný
vzniku	vznik	k1gInSc3	vznik
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
primitivní	primitivní	k2eAgFnPc1d1	primitivní
soustavy	soustava	k1gFnPc1	soustava
hospodaření	hospodaření	k1gNnSc2	hospodaření
(	(	kIx(	(
<g/>
stepní	stepní	k2eAgInSc1d1	stepní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
žďárový	žďárový	k2eAgInSc1d1	žďárový
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
záplavový	záplavový	k2eAgInSc1d1	záplavový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
úhorové	úhorový	k2eAgInPc1d1	úhorový
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
systémy	systém	k1gInPc1	systém
střídavého	střídavý	k2eAgNnSc2d1	střídavé
hospodaření	hospodaření	k1gNnSc2	hospodaření
charakterizované	charakterizovaný	k2eAgNnSc1d1	charakterizované
vznikem	vznik	k1gInSc7	vznik
osevních	osevní	k2eAgInPc2d1	osevní
postupů	postup	k1gInPc2	postup
udržující	udržující	k2eAgFnSc4d1	udržující
rovnováhu	rovnováha	k1gFnSc4	rovnováha
a	a	k8xC	a
stálou	stálý	k2eAgFnSc4d1	stálá
úrodnost	úrodnost	k1gFnSc4	úrodnost
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
půd	půda	k1gFnPc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
agrochemie	agrochemie	k1gFnSc2	agrochemie
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	s	k7c7	s
systémy	systém	k1gInPc7	systém
zemědělsko-průmyslovými	zemědělskorůmyslový	k2eAgInPc7d1	zemědělsko-průmyslový
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xS	jako
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zonalitě	zonalita	k1gFnSc3	zonalita
a	a	k8xC	a
stupni	stupeň	k1gInSc3	stupeň
sociálně-ekonomického	sociálněkonomický	k2eAgInSc2d1	sociálně-ekonomický
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
výše	vysoce	k6eAd2	vysoce
jmenovanými	jmenovaný	k2eAgInPc7d1	jmenovaný
postupy	postup	k1gInPc7	postup
<g/>
.	.	kIx.	.
</s>
<s>
Přechodným	přechodný	k2eAgInSc7d1	přechodný
systémem	systém	k1gInSc7	systém
mezi	mezi	k7c7	mezi
lesnicky	lesnicky	k6eAd1	lesnicky
využívanou	využívaný	k2eAgFnSc7d1	využívaná
půdou	půda	k1gFnSc7	půda
a	a	k8xC	a
systémy	systém	k1gInPc7	systém
hospodaření	hospodaření	k1gNnSc2	hospodaření
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
půdách	půda	k1gFnPc6	půda
je	být	k5eAaImIp3nS	být
agrolesnictví	agrolesnictví	k1gNnSc1	agrolesnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Plužina	Plužina	k1gFnSc1	Plužina
</s>
</p>
<p>
<s>
Rajonizace	rajonizace	k1gFnSc1	rajonizace
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
výroby	výroba	k1gFnSc2	výroba
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šuta	šuta	k1gFnSc1	šuta
<g/>
:	:	kIx,	:
Evropská	evropský	k2eAgFnSc1d1	Evropská
půda	půda	k1gFnSc1	půda
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
–	–	k?	–
opomíjený	opomíjený	k2eAgInSc4d1	opomíjený
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
respekt	respekt	k1gInSc4	respekt
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Stejskal	Stejskal	k1gMnSc1	Stejskal
<g/>
:	:	kIx,	:
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Vašků	Vašek	k1gMnPc2	Vašek
<g/>
:	:	kIx,	:
Půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
nenahraditelná	nahraditelný	k2eNgFnSc1d1	nenahraditelná
<g/>
,	,	kIx,	,
ekolist	ekolist	k1gInSc1	ekolist
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
</s>
</p>
