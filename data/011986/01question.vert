<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
využívaná	využívaný	k2eAgFnSc1d1	využívaná
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
zemědělství	zemědělství	k1gNnSc2	zemědělství
či	či	k8xC	či
pastevectví	pastevectví	k1gNnSc2	pastevectví
<g/>
?	?	kIx.	?
</s>
