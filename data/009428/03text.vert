<p>
<s>
Úřední	úřední	k2eAgFnSc1d1	úřední
osoba	osoba	k1gFnSc1	osoba
je	být	k5eAaImIp3nS	být
právní	právní	k2eAgInSc4d1	právní
pojem	pojem	k1gInSc4	pojem
<g/>
,	,	kIx,	,
zavedený	zavedený	k2eAgInSc4d1	zavedený
českým	český	k2eAgInSc7d1	český
trestním	trestní	k2eAgInSc7d1	trestní
zákoníkem	zákoník	k1gInSc7	zákoník
pro	pro	k7c4	pro
určité	určitý	k2eAgFnPc4d1	určitá
kategorie	kategorie	k1gFnPc4	kategorie
profesí	profes	k1gFnPc2	profes
a	a	k8xC	a
ústavních	ústavní	k2eAgMnPc2d1	ústavní
činitelů	činitel	k1gMnPc2	činitel
vykonávajících	vykonávající	k2eAgMnPc2d1	vykonávající
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
je	být	k5eAaImIp3nS	být
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
poskytována	poskytovat	k5eAaImNgFnS	poskytovat
určitá	určitý	k2eAgFnSc1d1	určitá
ochrana	ochrana	k1gFnSc1	ochrana
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Trestá	trestat	k5eAaImIp3nS	trestat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jak	jak	k8xS	jak
násilí	násilí	k1gNnSc4	násilí
proti	proti	k7c3	proti
úřední	úřední	k2eAgFnSc3d1	úřední
osobě	osoba	k1gFnSc3	osoba
nebo	nebo	k8xC	nebo
vyhrožování	vyhrožování	k1gNnSc4	vyhrožování
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
svou	svůj	k3xOyFgFnSc4	svůj
pravomoc	pravomoc	k1gFnSc4	pravomoc
zneužila	zneužít	k5eAaPmAgFnS	zneužít
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
takový	takový	k3xDgInSc4	takový
případný	případný	k2eAgInSc4d1	případný
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
pravomocí	pravomoc	k1gFnSc7	pravomoc
a	a	k8xC	a
odpovědností	odpovědnost	k1gFnSc7	odpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
nebo	nebo	k8xC	nebo
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
v	v	k7c6	v
podobném	podobný	k2eAgNnSc6d1	podobné
postavení	postavení	k1gNnSc6	postavení
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
úřední	úřední	k2eAgFnSc4d1	úřední
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tak	tak	k6eAd1	tak
stanoví	stanovit	k5eAaPmIp3nS	stanovit
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
<g/>
Pojem	pojem	k1gInSc1	pojem
úřední	úřední	k2eAgFnSc2d1	úřední
osoby	osoba	k1gFnSc2	osoba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
nahradil	nahradit	k5eAaPmAgInS	nahradit
dřívější	dřívější	k2eAgNnSc4d1	dřívější
označení	označení	k1gNnSc4	označení
veřejný	veřejný	k2eAgInSc1d1	veřejný
činitel	činitel	k1gInSc1	činitel
<g/>
.	.	kIx.	.
<g/>
Pojem	pojem	k1gInSc1	pojem
úřední	úřední	k2eAgFnSc1d1	úřední
osoba	osoba	k1gFnSc1	osoba
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
právní	právní	k2eAgInPc4d1	právní
předpisy	předpis	k1gInPc4	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
správní	správní	k2eAgInSc1d1	správní
řád	řád	k1gInSc1	řád
za	za	k7c4	za
úřední	úřední	k2eAgFnSc4d1	úřední
osobu	osoba	k1gFnSc4	osoba
považuje	považovat	k5eAaImIp3nS	považovat
každou	každý	k3xTgFnSc4	každý
osobu	osoba	k1gFnSc4	osoba
bezprostředně	bezprostředně	k6eAd1	bezprostředně
se	se	k3xPyFc4	se
podílející	podílející	k2eAgMnSc1d1	podílející
na	na	k7c6	na
výkonu	výkon	k1gInSc6	výkon
pravomoci	pravomoc	k1gFnSc2	pravomoc
správního	správní	k2eAgInSc2d1	správní
orgánu	orgán	k1gInSc2	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
úřední	úřední	k2eAgFnPc1d1	úřední
osoby	osoba	k1gFnPc1	osoba
==	==	k?	==
</s>
</p>
<p>
<s>
Úřední	úřední	k2eAgFnSc7d1	úřední
osobou	osoba	k1gFnSc7	osoba
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
orgánu	orgán	k1gInSc6	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
člen	člen	k1gMnSc1	člen
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
a	a	k8xC	a
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
úředník	úředník	k1gMnSc1	úředník
orgánu	orgán	k1gInSc2	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
např.	např.	kA	např.
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
okolností	okolnost	k1gFnPc2	okolnost
i	i	k9	i
vykonavatel	vykonavatel	k1gMnSc1	vykonavatel
soudního	soudní	k2eAgMnSc2d1	soudní
exekutora	exekutor	k1gMnSc2	exekutor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
příslušník	příslušník	k1gMnSc1	příslušník
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
strážník	strážník	k1gMnSc1	strážník
obecní	obecní	k2eAgFnSc2d1	obecní
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
soudní	soudní	k2eAgMnSc1d1	soudní
exekutor	exekutor	k1gMnSc1	exekutor
(	(	kIx(	(
<g/>
v	v	k7c6	v
exekučním	exekuční	k2eAgNnSc6d1	exekuční
řízení	řízení	k1gNnSc6	řízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
notář	notář	k1gMnSc1	notář
(	(	kIx(	(
<g/>
v	v	k7c6	v
dědickém	dědický	k2eAgNnSc6d1	dědické
řízení	řízení	k1gNnSc6	řízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
finanční	finanční	k2eAgMnSc1d1	finanční
arbitr	arbitr	k1gMnSc1	arbitr
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
byl	být	k5eAaImAgMnS	být
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
lesní	lesní	k2eAgFnSc7d1	lesní
stráží	stráž	k1gFnSc7	stráž
<g/>
,	,	kIx,	,
stráží	stráž	k1gFnSc7	stráž
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
mysliveckou	myslivecký	k2eAgFnSc7d1	myslivecká
stráží	stráž	k1gFnSc7	stráž
nebo	nebo	k8xC	nebo
rybářskou	rybářský	k2eAgFnSc7d1	rybářská
stráží	stráž	k1gFnSc7	stráž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
FRYŠTÁK	FRYŠTÁK	kA	FRYŠTÁK
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
<g/>
;	;	kIx,	;
KUCHTA	Kuchta	k1gMnSc1	Kuchta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
PROVAZNÍK	Provazník	k1gMnSc1	Provazník
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
ČEP	Čep	k1gMnSc1	Čep
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
úřední	úřední	k2eAgFnPc1d1	úřední
osoby	osoba	k1gFnPc1	osoba
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Wolters	Wolters	k1gInSc1	Wolters
Kluwer	Kluwra	k1gFnPc2	Kluwra
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
312	[number]	k4	312
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7552	[number]	k4	7552
<g/>
-	-	kIx~	-
<g/>
622	[number]	k4	622
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
