<s>
Úřední	úřední	k2eAgFnSc1d1	úřední
osoba	osoba	k1gFnSc1	osoba
je	být	k5eAaImIp3nS	být
právní	právní	k2eAgInSc4d1	právní
pojem	pojem	k1gInSc4	pojem
<g/>
,	,	kIx,	,
zavedený	zavedený	k2eAgInSc4d1	zavedený
českým	český	k2eAgInSc7d1	český
trestním	trestní	k2eAgInSc7d1	trestní
zákoníkem	zákoník	k1gInSc7	zákoník
pro	pro	k7c4	pro
určité	určitý	k2eAgFnPc4d1	určitá
kategorie	kategorie	k1gFnPc4	kategorie
profesí	profes	k1gFnPc2	profes
a	a	k8xC	a
ústavních	ústavní	k2eAgMnPc2d1	ústavní
činitelů	činitel	k1gMnPc2	činitel
vykonávajících	vykonávající	k2eAgMnPc2d1	vykonávající
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
je	být	k5eAaImIp3nS	být
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
poskytována	poskytovat	k5eAaImNgFnS	poskytovat
určitá	určitý	k2eAgFnSc1d1	určitá
ochrana	ochrana	k1gFnSc1	ochrana
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
<g/>
.	.	kIx.	.
</s>
