<s>
Autoreference	Autoreference	k1gFnSc1
</s>
<s>
Bájný	bájný	k2eAgMnSc1d1
had	had	k1gMnSc1
Úroboros	Úroborosa	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
kouše	kousat	k5eAaImIp3nS
do	do	k7c2
ocasu	ocas	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
symbolem	symbol	k1gInSc7
autoreference	autoreference	k1gFnSc2
</s>
<s>
Autoreference	Autoreference	k1gFnSc1
je	být	k5eAaImIp3nS
přímý	přímý	k2eAgInSc4d1
nebo	nebo	k8xC
nepřímý	přímý	k2eNgInSc4d1
odkaz	odkaz	k1gInSc4
(	(	kIx(
<g/>
reference	reference	k1gFnPc4
<g/>
)	)	kIx)
k	k	k7c3
sobě	se	k3xPyFc3
samému	samý	k3xTgMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohl	moct	k5eAaImAgMnS
by	by	kYmCp3nS
označovat	označovat	k5eAaImF
i	i	k9
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
hovoří	hovořit	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
ale	ale	k8xC
užívá	užívat	k5eAaImIp3nS
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
o	o	k7c6
výpovědích	výpověď	k1gFnPc6
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
vztahují	vztahovat	k5eAaImIp3nP
samy	sám	k3xTgInPc1
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
i	i	k9
článek	článek	k1gInSc1
Wikipedie	Wikipedie	k1gFnSc2
o	o	k7c6
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Autoreferenční	Autoreferenční	k2eAgFnPc1d1
výpovědi	výpověď	k1gFnPc1
</s>
<s>
Znak	znak	k1gInSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
právě	právě	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
něčemu	něco	k3yInSc3
odkazuje	odkazovat	k5eAaImIp3nS
(	(	kIx(
<g/>
něco	něco	k3yInSc1
označuje	označovat	k5eAaImIp3nS
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
vyjadřuje	vyjadřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
symbolizuje	symbolizovat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazykové	jazykový	k2eAgFnPc4d1
výpovědi	výpověď	k1gFnPc4
mohou	moct	k5eAaImIp3nP
odkazovat	odkazovat	k5eAaImF
k	k	k7c3
nejrůznějším	různý	k2eAgFnPc3d3
skutečnostem	skutečnost	k1gFnPc3
nebo	nebo	k8xC
představám	představa	k1gFnPc3
a	a	k8xC
ve	v	k7c6
zvláštním	zvláštní	k2eAgInSc6d1
případě	případ	k1gInSc6
i	i	k9
k	k	k7c3
sobě	se	k3xPyFc3
samým	samý	k3xTgMnPc3
<g/>
:	:	kIx,
</s>
<s>
“	“	k?
<g/>
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
krátká	krátký	k2eAgFnSc1d1
věta	věta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Uvedený	uvedený	k2eAgInSc1d1
příklad	příklad	k1gInSc1
je	být	k5eAaImIp3nS
tautologicky	tautologicky	k6eAd1
pravdivý	pravdivý	k2eAgInSc4d1
<g/>
,	,	kIx,
následující	následující	k2eAgInPc4d1
dva	dva	k4xCgInPc4
tautologicky	tautologicky	k6eAd1
nepravdivé	pravdivý	k2eNgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
“	“	k?
<g/>
Tato	tento	k3xDgFnSc1
věta	věta	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
zápor	zápor	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
“	“	k?
<g/>
Tato	tento	k3xDgFnSc1
věta	věta	k1gFnSc1
neobsahuje	obsahovat	k5eNaImIp3nS
zápor	zápor	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Ve	v	k7c6
filosofii	filosofie	k1gFnSc6
jazyka	jazyk	k1gInSc2
se	se	k3xPyFc4
rozlišuje	rozlišovat	k5eAaImIp3nS
mezi	mezi	k7c7
rovinou	rovina	k1gFnSc7
jazykovou	jazykový	k2eAgFnSc7d1
(	(	kIx(
<g/>
výpovědi	výpověď	k1gFnSc6
o	o	k7c6
něčem	něco	k3yInSc6
<g/>
)	)	kIx)
a	a	k8xC
metajazykovou	metajazykový	k2eAgFnSc4d1
(	(	kIx(
<g/>
výpovědi	výpověď	k1gFnSc6
o	o	k7c6
výpovědích	výpověď	k1gFnPc6
<g/>
)	)	kIx)
a	a	k8xC
autoreferenci	autoreferenec	k1gMnPc1
lze	lze	k6eAd1
potom	potom	k6eAd1
také	také	k9
popsat	popsat	k5eAaPmF
jako	jako	k8xS,k8xC
směšování	směšování	k1gNnSc3
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
rovin	rovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukazovací	ukazovací	k2eAgInSc4d1
zájmeno	zájmeno	k1gNnSc1
„	„	k?
<g/>
toto	tento	k3xDgNnSc4
<g/>
“	“	k?
běžně	běžně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
odkaz	odkaz	k1gInSc1
na	na	k7c4
něco	něco	k3yInSc4
<g/>
,	,	kIx,
nač	nač	k6eAd1
například	například	k6eAd1
ukazuji	ukazovat	k5eAaImIp1nS
prstem	prst	k1gInSc7
(	(	kIx(
<g/>
jazyková	jazykový	k2eAgFnSc1d1
rovina	rovina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
uvedených	uvedený	k2eAgInPc6d1
příkladech	příklad	k1gInPc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
však	však	k9
rozumí	rozumět	k5eAaImIp3nS
jako	jako	k9
odkazu	odkaz	k1gInSc2
na	na	k7c4
výpověď	výpověď	k1gFnSc4
samu	sám	k3xTgFnSc4
–	–	k?
a	a	k8xC
tedy	tedy	k9
jako	jako	k9
výpovědi	výpověď	k1gFnPc4
metajazykové	metajazykový	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Příbuzné	příbuzný	k2eAgInPc1d1
jevy	jev	k1gInPc1
</s>
<s>
Belgický	belgický	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
René	René	k1gMnSc1
Magritte	Magritt	k1gInSc5
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgInS
obrazem	obraz	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
je	být	k5eAaImIp3nS
namalována	namalován	k2eAgFnSc1d1
dýmka	dýmka	k1gFnSc1
a	a	k8xC
pod	pod	k7c7
ní	on	k3xPp3gFnSc7
nápis	nápis	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Toto	tento	k3xDgNnSc1
není	být	k5eNaImIp3nS
dýmka	dýmka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Paradox	paradox	k1gInSc1
lze	lze	k6eAd1
vyložit	vyložit	k5eAaPmF
například	například	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
toto	tento	k3xDgNnSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
obraz	obraz	k1gInSc4
a	a	k8xC
nikoli	nikoli	k9
dýmka	dýmka	k1gFnSc1
<g/>
,	,	kIx,
účinek	účinek	k1gInSc1
obrazu	obraz	k1gInSc2
se	se	k3xPyFc4
však	však	k9
zakládá	zakládat	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
divák	divák	k1gMnSc1
zde	zde	k6eAd1
paradox	paradox	k1gInSc4
cítí	cítit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
autoreferencí	autoreference	k1gFnSc7
souvisí	souviset	k5eAaImIp3nS
slavný	slavný	k2eAgInSc1d1
paradox	paradox	k1gInSc1
lháře	lhář	k1gMnSc4
<g/>
,	,	kIx,
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
říká	říkat	k5eAaImIp3nS
„	„	k?
<g/>
já	já	k3xPp1nSc1
lžu	lhát	k5eAaImIp1nS
<g/>
“	“	k?
<g/>
:	:	kIx,
pokud	pokud	k8xS
lže	lhát	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
výpověď	výpověď	k1gFnSc1
pravdivá	pravdivý	k2eAgFnSc1d1
–	–	k?
a	a	k8xC
tedy	tedy	k9
nelže	lhát	k5eNaImIp3nS
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
pokud	pokud	k8xS
nelže	lhát	k5eNaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
výpověď	výpověď	k1gFnSc1
nepravdivá	pravdivý	k2eNgFnSc1d1
–	–	k?
a	a	k8xC
tedy	tedy	k9
lže	lhát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
zde	zde	k6eAd1
se	se	k3xPyFc4
ovšem	ovšem	k9
směšuje	směšovat	k5eAaImIp3nS
vlastní	vlastní	k2eAgInSc4d1
„	„	k?
<g/>
předmět	předmět	k1gInSc4
<g/>
“	“	k?
jeho	jeho	k3xOp3gFnSc3
výpovědi	výpověď	k1gFnSc3
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
o	o	k7c6
čem	co	k3yRnSc6,k3yInSc6,k3yQnSc6
lže	lhát	k5eAaImIp3nS
či	či	k8xC
nelže	lhát	k5eNaImIp3nS
<g/>
)	)	kIx)
s	s	k7c7
výpovědí	výpověď	k1gFnSc7
o	o	k7c6
této	tento	k3xDgFnSc6
(	(	kIx(
<g/>
nevyslovené	vyslovený	k2eNgFnSc6d1
<g/>
)	)	kIx)
výpovědi	výpověď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
lety	léto	k1gNnPc7
bylo	být	k5eAaImAgNnS
na	na	k7c6
roletě	roleta	k1gFnSc6
obchodu	obchod	k1gInSc2
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
Kaprově	kaprův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
zvenčí	zvenčí	k6eAd1
napsáno	napsat	k5eAaPmNgNnS,k5eAaBmNgNnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Tato	tento	k3xDgFnSc1
mlékárna	mlékárna	k1gFnSc1
není	být	k5eNaImIp3nS
nikdy	nikdy	k6eAd1
zavřená	zavřený	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
Nejvíce	hodně	k6eAd3,k6eAd1
utajované	utajovaný	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
tajných	tajný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
se	se	k3xPyFc4
prý	prý	k9
označují	označovat	k5eAaImIp3nP
poznámkou	poznámka	k1gFnSc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Před	před	k7c7
otevřením	otevření	k1gNnSc7
zničit	zničit	k5eAaPmF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémy	problém	k1gInPc4
s	s	k7c7
autoreferencí	autoreference	k1gFnSc7
mohou	moct	k5eAaImIp3nP
nastat	nastat	k5eAaPmF
v	v	k7c6
právu	právo	k1gNnSc6
<g/>
,	,	kIx,
autoreferencí	autoreference	k1gFnSc7
jsou	být	k5eAaImIp3nP
i	i	k9
rekurze	rekurze	k1gFnPc1
v	v	k7c6
počítačovém	počítačový	k2eAgInSc6d1
kódu	kód	k1gInSc6
</s>
<s>
funkce	funkce	k1gFnSc1
F	F	kA
<g/>
:	:	kIx,
</s>
<s>
...	...	k?
</s>
<s>
volej	volej	k1gInSc1
funkci	funkce	k1gFnSc4
F	F	kA
</s>
<s>
Širokou	široký	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
obdobných	obdobný	k2eAgFnPc2d1
a	a	k8xC
často	často	k6eAd1
paradoxních	paradoxní	k2eAgInPc2d1
jevů	jev	k1gInPc2
zpracoval	zpracovat	k5eAaPmAgMnS
Douglas	Douglas	k1gMnSc1
Hofstadter	Hofstadter	k1gMnSc1
ve	v	k7c6
slavné	slavný	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
Gödel	Gödela	k1gFnPc2
<g/>
,	,	kIx,
Escher	Eschra	k1gFnPc2
<g/>
,	,	kIx,
Bach	Bacha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Tautologie	tautologie	k1gFnSc1
</s>
<s>
Definice	definice	k1gFnSc1
kruhem	kruh	k1gInSc7
</s>
<s>
Rekurze	rekurze	k1gFnSc1
</s>
<s>
Fraktál	fraktál	k1gInSc1
</s>
<s>
Contradictio	Contradictio	k1gNnSc1
in	in	k?
adjecto	adjecto	k1gNnSc1
</s>
<s>
Paradox	paradox	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
R.	R.	kA
Smullyan	Smullyan	k1gInSc1
<g/>
,	,	kIx,
Navěky	navěky	k6eAd1
nerozhodnuto	rozhodnut	k2eNgNnSc1d1
<g/>
:	:	kIx,
úvod	úvod	k1gInSc1
do	do	k7c2
logiky	logika	k1gFnSc2
a	a	k8xC
zábavný	zábavný	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
ke	k	k7c3
Gödelovým	Gödelův	k2eAgInPc3d1
objevům	objev	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2003	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
D.	D.	kA
Hofstadter	Hofstadtra	k1gFnPc2
<g/>
:	:	kIx,
Gödel	Gödela	k1gFnPc2
<g/>
,	,	kIx,
Escher	Eschra	k1gFnPc2
<g/>
,	,	kIx,
Bach	Bacha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
1979	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gFnSc1
Paradox	paradox	k1gInSc1
of	of	k?
Self-Amendment	Self-Amendment	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
Study	stud	k1gInPc1
of	of	k?
Logic	Logice	k1gFnPc2
<g/>
,	,	kIx,
Law	Law	k1gFnSc1
<g/>
,	,	kIx,
Omnipotence	omnipotence	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
Change	change	k1gFnSc1
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
o	o	k7c6
autoreferencích	autoreference	k1gFnPc6
v	v	k7c6
právu	právo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Skutečné	skutečný	k2eAgInPc1d1
a	a	k8xC
zdánlivé	zdánlivý	k2eAgInPc1d1
autoreference	autoreferenec	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Elegancelessness	Elegancelessness	k1gInSc1
Paul	Paula	k1gFnPc2
Niquette	Niquett	k1gInSc5
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
autoreferencí	autoreference	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Parodické	parodický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
self-reference	self-reference	k1gFnSc2
nabízí	nabízet	k5eAaImIp3nS
příklady	příklad	k1gInPc4
autoreferencí	autoreference	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Autoreferenční	Autoreferenční	k2eAgInPc4d1
vtipy	vtip	k1gInPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4138396-5	4138396-5	k4
</s>
