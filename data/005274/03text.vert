<s>
Religionistika	religionistika	k1gFnSc1	religionistika
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
religio	religio	k1gNnSc4	religio
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
předmětem	předmět	k1gInSc7	předmět
není	být	k5eNaImIp3nS	být
rozhodování	rozhodování	k1gNnSc1	rozhodování
o	o	k7c6	o
pravdivosti	pravdivost	k1gFnSc6	pravdivost
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
popis	popis	k1gInSc1	popis
a	a	k8xC	a
klasifikace	klasifikace	k1gFnSc1	klasifikace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
náboženských	náboženský	k2eAgFnPc2d1	náboženská
skutečností	skutečnost	k1gFnPc2	skutečnost
i	i	k8xC	i
celých	celý	k2eAgInPc2d1	celý
náboženských	náboženský	k2eAgInPc2d1	náboženský
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
však	však	k9	však
i	i	k9	i
samu	sám	k3xTgFnSc4	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
meze	mez	k1gFnPc4	mez
a	a	k8xC	a
metodologické	metodologický	k2eAgFnPc4d1	metodologická
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
problémů	problém	k1gInPc2	problém
religionistiky	religionistika	k1gFnSc2	religionistika
jako	jako	k8xC	jako
vědy	věda	k1gFnSc2	věda
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
definice	definice	k1gFnSc1	definice
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojený	spojený	k2eAgInSc1d1	spojený
problém	problém	k1gInSc1	problém
definice	definice	k1gFnSc2	definice
samotné	samotný	k2eAgFnSc2d1	samotná
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
sama	sám	k3xTgFnSc1	sám
používá	používat	k5eAaImIp3nS	používat
různých	různý	k2eAgFnPc2d1	různá
metod	metoda	k1gFnPc2	metoda
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
věnuje	věnovat	k5eAaPmIp3nS	věnovat
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
pramení	pramenit	k5eAaImIp3nP	pramenit
i	i	k9	i
jisté	jistý	k2eAgInPc1d1	jistý
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
přístupy	přístup	k1gInPc7	přístup
v	v	k7c6	v
religionistice	religionistika	k1gFnSc6	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
základním	základní	k2eAgInPc3d1	základní
religionistickým	religionistický	k2eAgInPc3d1	religionistický
postupům	postup	k1gInPc3	postup
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
náboženství	náboženství	k1gNnSc2	náboženství
patří	patřit	k5eAaImIp3nS	patřit
historická	historický	k2eAgFnSc1d1	historická
a	a	k8xC	a
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
(	(	kIx(	(
<g/>
komparativní	komparativní	k2eAgFnSc1d1	komparativní
<g/>
)	)	kIx)	)
religionistika	religionistika	k1gFnSc1	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Religionistika	religionistika	k1gFnSc1	religionistika
také	také	k9	také
hojně	hojně	k6eAd1	hojně
využívá	využívat	k5eAaPmIp3nS	využívat
hledisek	hledisko	k1gNnPc2	hledisko
jiných	jiný	k2eAgFnPc2d1	jiná
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgMnPc2	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
relativně	relativně	k6eAd1	relativně
samostatné	samostatný	k2eAgFnPc1d1	samostatná
disciplíny	disciplína	k1gFnPc1	disciplína
jako	jako	k8xC	jako
filozofie	filozofie	k1gFnSc1	filozofie
či	či	k8xC	či
sociologie	sociologie	k1gFnSc1	sociologie
náboženství	náboženství	k1gNnSc2	náboženství
(	(	kIx(	(
<g/>
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
kontextuální	kontextuální	k2eAgFnSc4d1	kontextuální
religionistiku	religionistika	k1gFnSc4	religionistika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Formování	formování	k1gNnSc1	formování
religionistiky	religionistika	k1gFnSc2	religionistika
jako	jako	k8xC	jako
vědy	věda	k1gFnSc2	věda
souviselo	souviset	k5eAaImAgNnS	souviset
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
religionistiky	religionistika	k1gFnSc2	religionistika
jako	jako	k8xS	jako
samostatného	samostatný	k2eAgInSc2d1	samostatný
oboru	obor	k1gInSc2	obor
studia	studio	k1gNnSc2	studio
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
za	za	k7c2	za
jejího	její	k3xOp3gMnSc2	její
zakladatele	zakladatel	k1gMnSc2	zakladatel
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
Friedrich	Friedrich	k1gMnSc1	Friedrich
Max	Max	k1gMnSc1	Max
Müller	Müller	k1gMnSc1	Müller
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vyčleňovat	vyčleňovat	k5eAaImF	vyčleňovat
na	na	k7c6	na
teologických	teologický	k2eAgFnPc6d1	teologická
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
brzy	brzy	k6eAd1	brzy
dokázala	dokázat	k5eAaPmAgFnS	dokázat
prosadit	prosadit	k5eAaPmF	prosadit
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c4	na
teologii	teologie	k1gFnSc4	teologie
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
zejména	zejména	k9	zejména
na	na	k7c6	na
filozofických	filozofický	k2eAgFnPc6d1	filozofická
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Objektivizující	objektivizující	k2eAgInSc1d1	objektivizující
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
náboženství	náboženství	k1gNnSc4	náboženství
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
až	až	k9	až
na	na	k7c6	na
určitém	určitý	k2eAgInSc6d1	určitý
stupni	stupeň	k1gInSc6	stupeň
sociálního	sociální	k2eAgInSc2d1	sociální
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
schopna	schopen	k2eAgFnSc1d1	schopna
reflektovat	reflektovat	k5eAaImF	reflektovat
cizí	cizí	k2eAgFnSc3d1	cizí
a	a	k8xC	a
zejména	zejména	k9	zejména
své	svůj	k3xOyFgNnSc4	svůj
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
také	také	k9	také
schopna	schopen	k2eAgFnSc1d1	schopna
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
náboženským	náboženský	k2eAgInSc7d1	náboženský
a	a	k8xC	a
profánním	profánní	k2eAgInSc7d1	profánní
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
dějinách	dějiny	k1gFnPc6	dějiny
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
výrazněji	výrazně	k6eAd2	výrazně
poprvé	poprvé	k6eAd1	poprvé
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
a	a	k8xC	a
římské	římský	k2eAgFnSc6d1	římská
antice	antika	k1gFnSc6	antika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
známé	známý	k2eAgInPc1d1	známý
i	i	k8xC	i
dřívější	dřívější	k2eAgInPc1d1	dřívější
možné	možný	k2eAgInPc1d1	možný
mezináboženské	mezináboženský	k2eAgInPc1d1	mezináboženský
kontakty	kontakt	k1gInPc1	kontakt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
produktem	produkt	k1gInSc7	produkt
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
politických	politický	k2eAgFnPc2d1	politická
cest	cesta	k1gFnPc2	cesta
či	či	k8xC	či
vojenských	vojenský	k2eAgNnPc2d1	vojenské
tažení	tažení	k1gNnPc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Starověké	starověký	k2eAgNnSc1d1	starověké
Řecko	Řecko	k1gNnSc1	Řecko
a	a	k8xC	a
Řím	Řím	k1gInSc1	Řím
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
kolébku	kolébka	k1gFnSc4	kolébka
náboženských	náboženský	k2eAgInPc2d1	náboženský
výzkumů	výzkum	k1gInPc2	výzkum
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
vznikaly	vznikat	k5eAaImAgFnP	vznikat
první	první	k4xOgFnPc1	první
teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
původu	původ	k1gInSc6	původ
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
první	první	k4xOgFnPc1	první
filozofické	filozofický	k2eAgFnPc1d1	filozofická
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
podstatě	podstata	k1gFnSc6	podstata
náboženství	náboženství	k1gNnSc2	náboženství
apod.	apod.	kA	apod.
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
rysem	rys	k1gInSc7	rys
reflexe	reflexe	k1gFnSc2	reflexe
náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
byla	být	k5eAaImAgFnS	být
kritika	kritika	k1gFnSc1	kritika
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
velké	velký	k2eAgFnPc1d1	velká
kritiky	kritika	k1gFnPc1	kritika
náboženství	náboženství	k1gNnSc2	náboženství
směřovaly	směřovat	k5eAaImAgFnP	směřovat
ke	k	k7c3	k
kritice	kritika	k1gFnSc3	kritika
náboženského	náboženský	k2eAgInSc2d1	náboženský
antropomorfismu	antropomorfismus	k1gInSc2	antropomorfismus
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnSc7	jejich
představitelem	představitel	k1gMnSc7	představitel
je	být	k5eAaImIp3nS	být
Xenofanés	Xenofanés	k1gInSc4	Xenofanés
z	z	k7c2	z
Kolofóntu	Kolofónt	k1gInSc2	Kolofónt
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
známým	známý	k2eAgNnSc7d1	známé
přirovnáním	přirovnání	k1gNnSc7	přirovnání
o	o	k7c6	o
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
,	,	kIx,	,
lidech	člověk	k1gMnPc6	člověk
a	a	k8xC	a
bozích	bůh	k1gMnPc6	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Filozof	filozof	k1gMnSc1	filozof
Prodikos	Prodikos	k1gMnSc1	Prodikos
z	z	k7c2	z
Keu	Keu	k1gFnSc2	Keu
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
první	první	k4xOgFnSc4	první
známou	známý	k2eAgFnSc4d1	známá
racionalistickou	racionalistický	k2eAgFnSc4d1	racionalistická
teorii	teorie	k1gFnSc4	teorie
vzniku	vznik	k1gInSc2	vznik
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
v	v	k7c4	v
dávné	dávný	k2eAgFnPc4d1	dávná
minulosti	minulost	k1gFnPc4	minulost
lidé	člověk	k1gMnPc1	člověk
uctívali	uctívat	k5eAaImAgMnP	uctívat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejvíce	nejvíce	k6eAd1	nejvíce
napomáhaly	napomáhat	k5eAaImAgFnP	napomáhat
životu	život	k1gInSc2	život
(	(	kIx(	(
<g/>
slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
či	či	k8xC	či
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
kritika	kritika	k1gFnSc1	kritika
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
např.	např.	kA	např.
Sisyfa	Sisyfos	k1gMnSc4	Sisyfos
a	a	k8xC	a
Kritia	Kritius	k1gMnSc4	Kritius
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholným	vrcholný	k2eAgMnSc7d1	vrcholný
představitelem	představitel	k1gMnSc7	představitel
antického	antický	k2eAgNnSc2d1	antické
zkoumání	zkoumání	k1gNnSc2	zkoumání
vlastního	vlastní	k2eAgNnSc2d1	vlastní
náboženství	náboženství	k1gNnSc2	náboženství
byl	být	k5eAaImAgMnS	být
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
z	z	k7c2	z
Halikarnásu	Halikarnás	k1gInSc2	Halikarnás
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
dějin	dějiny	k1gFnPc2	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
mnoho	mnoho	k6eAd1	mnoho
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
poznatky	poznatek	k1gInPc4	poznatek
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
jsou	být	k5eAaImIp3nP	být
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
pokusem	pokus	k1gInSc7	pokus
o	o	k7c6	o
zaznamenání	zaznamenání	k1gNnSc6	zaznamenání
dějin	dějiny	k1gFnPc2	dějiny
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
helénistickém	helénistický	k2eAgNnSc6d1	helénistické
období	období	k1gNnSc6	období
byli	být	k5eAaImAgMnP	být
Řekové	Řek	k1gMnPc1	Řek
přímo	přímo	k6eAd1	přímo
konfrontováni	konfrontovat	k5eAaBmNgMnP	konfrontovat
s	s	k7c7	s
odlišnými	odlišný	k2eAgInPc7d1	odlišný
náboženskými	náboženský	k2eAgInPc7d1	náboženský
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
následkem	následkem	k7c2	následkem
vojenských	vojenský	k2eAgNnPc2d1	vojenské
tažení	tažení	k1gNnPc2	tažení
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
spisy	spis	k1gInPc1	spis
Cicerona	Cicero	k1gMnSc2	Cicero
i	i	k8xC	i
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
různá	různý	k2eAgNnPc4d1	různé
náboženství	náboženství	k1gNnPc4	náboženství
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
náboženství	náboženství	k1gNnSc2	náboženství
řeckého	řecký	k2eAgNnSc2d1	řecké
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
středověk	středověk	k1gInSc1	středověk
se	se	k3xPyFc4	se
mimokřesťanskými	mimokřesťanský	k2eAgInPc7d1	mimokřesťanský
náboženskými	náboženský	k2eAgInPc7d1	náboženský
systémy	systém	k1gInPc7	systém
příliš	příliš	k6eAd1	příliš
nezabýval	zabývat	k5eNaImAgInS	zabývat
a	a	k8xC	a
soustředil	soustředit	k5eAaPmAgInS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
jiných	jiný	k2eAgNnPc6d1	jiné
náboženstvích	náboženství	k1gNnPc6	náboženství
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
zejména	zejména	k9	zejména
v	v	k7c6	v
zápiscích	zápisek	k1gInPc6	zápisek
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
misionářů	misionář	k1gMnPc2	misionář
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
navázat	navázat	k5eAaPmF	navázat
významnější	významný	k2eAgInSc4d2	významnější
mezináboženský	mezináboženský	k2eAgInSc4d1	mezináboženský
kontakt	kontakt	k1gInSc4	kontakt
byl	být	k5eAaImAgMnS	být
Ramón	Ramón	k1gMnSc1	Ramón
Llull	Llull	k1gMnSc1	Llull
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
mladé	mladý	k2eAgMnPc4d1	mladý
misionáře	misionář	k1gMnPc4	misionář
vyučovat	vyučovat	k5eAaImF	vyučovat
orientálním	orientální	k2eAgMnPc3d1	orientální
jazykům	jazyk	k1gMnPc3	jazyk
a	a	k8xC	a
mezináboženské	mezináboženský	k2eAgFnSc3d1	mezináboženská
diplomacii	diplomacie	k1gFnSc3	diplomacie
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
však	však	k9	však
jeho	jeho	k3xOp3gFnSc3	jeho
snaze	snaha	k1gFnSc3	snaha
o	o	k7c4	o
navázání	navázání	k1gNnSc4	navázání
dialogu	dialog	k1gInSc2	dialog
nepřála	přát	k5eNaImAgFnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Llull	Llull	k1gInSc1	Llull
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
ať	ať	k9	ať
už	už	k6eAd1	už
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
mnoho	mnoho	k4c1	mnoho
následovníků	následovník	k1gMnPc2	následovník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Kusánského	Kusánský	k2eAgMnSc2d1	Kusánský
<g/>
,	,	kIx,	,
Giordana	Giordan	k1gMnSc2	Giordan
Bruna	Bruno	k1gMnSc2	Bruno
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osvícenství	osvícenství	k1gNnSc6	osvícenství
se	se	k3xPyFc4	se
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
opět	opět	k6eAd1	opět
soustředil	soustředit	k5eAaPmAgInS	soustředit
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
především	především	k9	především
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
deismus	deismus	k1gInSc1	deismus
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
populární	populární	k2eAgInSc1d1	populární
zejména	zejména	k9	zejména
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Deismus	deismus	k1gInSc1	deismus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
kritiky	kritik	k1gMnPc4	kritik
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Voltaire	Voltair	k1gInSc5	Voltair
či	či	k8xC	či
J.	J.	kA	J.
J.	J.	kA	J.
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
vznikaly	vznikat	k5eAaImAgInP	vznikat
první	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
sekularizaci	sekularizace	k1gFnSc4	sekularizace
a	a	k8xC	a
racionalistickou	racionalistický	k2eAgFnSc4d1	racionalistická
kritiku	kritika	k1gFnSc4	kritika
dějin	dějiny	k1gFnPc2	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Brosses	Brosses	k1gMnSc1	Brosses
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
koncept	koncept	k1gInSc4	koncept
fetišismu	fetišismus	k1gInSc2	fetišismus
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
diskutovat	diskutovat	k5eAaImF	diskutovat
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
evoluce	evoluce	k1gFnSc2	evoluce
či	či	k8xC	či
degenerace	degenerace	k1gFnSc2	degenerace
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
sehrála	sehrát	k5eAaPmAgFnS	sehrát
německá	německý	k2eAgFnSc1d1	německá
osvícenská	osvícenský	k2eAgFnSc1d1	osvícenská
kritika	kritika	k1gFnSc1	kritika
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Christian	Christian	k1gMnSc1	Christian
Wolff	Wolff	k1gMnSc1	Wolff
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Salomo	Saloma	k1gFnSc5	Saloma
Semler	Semler	k1gMnSc1	Semler
<g/>
,	,	kIx,	,
Gotthold	Gotthold	k1gMnSc1	Gotthold
Ephraim	Ephraima	k1gFnPc2	Ephraima
Lessing	Lessing	k1gInSc1	Lessing
a	a	k8xC	a
Hermann	Hermann	k1gMnSc1	Hermann
Samuel	Samuel	k1gMnSc1	Samuel
Reimarus	Reimarus	k1gMnSc1	Reimarus
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
přínos	přínos	k1gInSc1	přínos
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
náboženské	náboženský	k2eAgFnSc2d1	náboženská
kritiky	kritika	k1gFnSc2	kritika
měl	mít	k5eAaImAgMnS	mít
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc4	Hume
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
pracemi	práce	k1gFnPc7	práce
The	The	k1gFnSc2	The
Natural	Natural	k?	Natural
History	Histor	k1gInPc1	Histor
of	of	k?	of
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
Přirozené	přirozený	k2eAgFnPc1d1	přirozená
dějiny	dějiny	k1gFnPc1	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1757	[number]	k4	1757
a	a	k8xC	a
Dialogues	Dialogues	k1gInSc1	Dialogues
Concerning	Concerning	k1gInSc1	Concerning
Natural	Natural	k?	Natural
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
Rozmluvy	rozmluva	k1gFnPc1	rozmluva
o	o	k7c6	o
náboženství	náboženství	k1gNnSc6	náboženství
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
právě	právě	k6eAd1	právě
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc4	Hume
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Immanuelem	Immanuel	k1gMnSc7	Immanuel
Kantem	Kant	k1gMnSc7	Kant
a	a	k8xC	a
G.	G.	kA	G.
W.	W.	kA	W.
Leibnizem	Leibniz	k1gMnSc7	Leibniz
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
položili	položit	k5eAaPmAgMnP	položit
základy	základ	k1gInPc4	základ
teorie	teorie	k1gFnSc2	teorie
tzv.	tzv.	kA	tzv.
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
existovat	existovat	k5eAaImF	existovat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
až	až	k9	až
díky	díky	k7c3	díky
postupnému	postupný	k2eAgInSc3d1	postupný
úpadku	úpadek	k1gInSc3	úpadek
přetvořilo	přetvořit	k5eAaPmAgNnS	přetvořit
do	do	k7c2	do
nynějších	nynější	k2eAgInPc2d1	nynější
náboženských	náboženský	k2eAgInPc2d1	náboženský
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
romantismem	romantismus	k1gInSc7	romantismus
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
východní	východní	k2eAgFnPc4d1	východní
náboženské	náboženský	k2eAgFnPc4d1	náboženská
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
započal	započnout	k5eAaPmAgInS	započnout
výzkum	výzkum	k1gInSc1	výzkum
mytologií	mytologie	k1gFnPc2	mytologie
mimoevropských	mimoevropský	k2eAgInPc2d1	mimoevropský
národů	národ	k1gInPc2	národ
a	a	k8xC	a
mysterijních	mysterijní	k2eAgInPc2d1	mysterijní
kultů	kult	k1gInPc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Klíčový	klíčový	k2eAgInSc4d1	klíčový
význam	význam	k1gInSc4	význam
měl	mít	k5eAaImAgMnS	mít
Johann	Johann	k1gMnSc1	Johann
Gottfried	Gottfried	k1gMnSc1	Gottfried
Herder	Herder	k1gMnSc1	Herder
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tzv.	tzv.	kA	tzv.
göttingenský	göttingenský	k2eAgInSc4d1	göttingenský
kroužek	kroužek	k1gInSc4	kroužek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zasadil	zasadit	k5eAaPmAgInS	zasadit
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
teologie	teologie	k1gFnSc1	teologie
začala	začít	k5eAaPmAgFnS	začít
zajímat	zajímat	k5eAaImF	zajímat
i	i	k9	i
o	o	k7c4	o
nekřesťanské	křesťanský	k2eNgInPc4d1	nekřesťanský
náboženské	náboženský	k2eAgInPc4d1	náboženský
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
orientace	orientace	k1gFnPc4	orientace
náboženského	náboženský	k2eAgNnSc2d1	náboženské
bádání	bádání	k1gNnSc2	bádání
<g/>
.	.	kIx.	.
</s>
<s>
Směr	směr	k1gInSc1	směr
kolem	kolem	k7c2	kolem
F.	F.	kA	F.
D.	D.	kA	D.
E.	E.	kA	E.
Schleiermachera	Schleiermachera	k1gFnSc1	Schleiermachera
a	a	k8xC	a
G.	G.	kA	G.
W.	W.	kA	W.
F.	F.	kA	F.
Hegela	Hegel	k1gMnSc2	Hegel
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
pochopení	pochopení	k1gNnSc4	pochopení
náboženství	náboženství	k1gNnSc2	náboženství
na	na	k7c6	na
metafyzicko-teologickém	metafyzickoeologický	k2eAgInSc6d1	metafyzicko-teologický
základu	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
stáli	stát	k5eAaImAgMnP	stát
Ludwig	Ludwig	k1gMnSc1	Ludwig
Feuerbach	Feuerbach	k1gMnSc1	Feuerbach
a	a	k8xC	a
Karl	Karl	k1gMnSc1	Karl
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
o	o	k7c4	o
uchopení	uchopení	k1gNnSc4	uchopení
náboženství	náboženství	k1gNnSc2	náboženství
jako	jako	k8xS	jako
antropologické	antropologický	k2eAgFnSc6d1	antropologická
či	či	k8xC	či
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Religionistika	religionistika	k1gFnSc1	religionistika
mohla	moct	k5eAaImAgFnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
až	až	k9	až
když	když	k8xS	když
se	se	k3xPyFc4	se
dostatečně	dostatečně	k6eAd1	dostatečně
rozvinuly	rozvinout	k5eAaPmAgFnP	rozvinout
jiné	jiný	k2eAgInPc4d1	jiný
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jí	on	k3xPp3gFnSc3	on
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
potřebný	potřebný	k2eAgInSc4d1	potřebný
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
religionistiky	religionistika	k1gFnSc2	religionistika
jako	jako	k8xS	jako
akademické	akademický	k2eAgFnSc2d1	akademická
disciplíny	disciplína	k1gFnSc2	disciplína
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
Pierre	Pierr	k1gInSc5	Pierr
Daniel	Daniel	k1gMnSc1	Daniel
Chantepie	Chantepie	k1gFnSc2	Chantepie
de	de	k?	de
la	la	k1gNnSc1	la
Saussaye	Saussaye	k1gFnSc1	Saussaye
<g/>
,	,	kIx,	,
Cornelis	Cornelis	k1gFnSc1	Cornelis
Petrus	Petrus	k1gMnSc1	Petrus
Tiele	Tiele	k1gInSc1	Tiele
a	a	k8xC	a
především	především	k9	především
Friedrich	Friedrich	k1gMnSc1	Friedrich
Max	Max	k1gMnSc1	Max
Müller	Müller	k1gMnSc1	Müller
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Max	Max	k1gMnSc1	Max
Müller	Müller	k1gMnSc1	Müller
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
své	svůj	k3xOyFgFnSc2	svůj
čtyřdílné	čtyřdílný	k2eAgFnSc2d1	čtyřdílná
práce	práce	k1gFnSc2	práce
Chips	chips	k1gInSc1	chips
from	from	k1gMnSc1	from
a	a	k8xC	a
German	German	k1gMnSc1	German
Workshop	workshop	k1gInSc1	workshop
(	(	kIx(	(
<g/>
Střípky	střípek	k1gInPc7	střípek
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
dílny	dílna	k1gFnSc2	dílna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Müller	Müller	k1gMnSc1	Müller
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
volá	volat	k5eAaImIp3nS	volat
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
samostatného	samostatný	k2eAgInSc2d1	samostatný
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
studiem	studio	k1gNnSc7	studio
náboženství	náboženství	k1gNnPc2	náboženství
(	(	kIx(	(
<g/>
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
objevuje	objevovat	k5eAaImIp3nS	objevovat
termín	termín	k1gInSc1	termín
science	science	k1gFnSc2	science
of	of	k?	of
religion	religion	k1gInSc1	religion
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
věda	věda	k1gFnSc1	věda
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
"	"	kIx"	"
–	–	k?	–
religionistika	religionistika	k1gFnSc1	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Müller	Müller	k1gMnSc1	Müller
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
přednáškách	přednáška	k1gFnPc6	přednáška
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
přednesl	přednést	k5eAaPmAgMnS	přednést
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
knižně	knižně	k6eAd1	knižně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Science	Scienec	k1gInPc1	Scienec
of	of	k?	of
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
anebo	anebo	k8xC	anebo
rok	rok	k1gInSc4	rok
vydání	vydání	k1gNnSc2	vydání
Střípků	střípek	k1gInPc2	střípek
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
dílny	dílna	k1gFnSc2	dílna
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
datum	datum	k1gNnSc1	datum
vzniku	vznik	k1gInSc2	vznik
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
velkému	velký	k2eAgInSc3d1	velký
ohlasu	ohlas	k1gInSc3	ohlas
<g/>
,	,	kIx,	,
jaký	jaký	k9	jaký
jeho	jeho	k3xOp3gFnPc1	jeho
přednášky	přednáška	k1gFnPc1	přednáška
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Müller	Müller	k1gMnSc1	Müller
dál	daleko	k6eAd2	daleko
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
nového	nový	k2eAgInSc2d1	nový
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
vypsal	vypsat	k5eAaPmAgInS	vypsat
přednášky	přednáška	k1gFnSc2	přednáška
o	o	k7c6	o
počátcích	počátek	k1gInPc6	počátek
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
je	být	k5eAaImIp3nS	být
opakovat	opakovat	k5eAaImF	opakovat
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kapacita	kapacita	k1gFnSc1	kapacita
posluchárny	posluchárna	k1gFnSc2	posluchárna
nestačila	stačit	k5eNaBmAgFnS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Završením	završení	k1gNnSc7	završení
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
akademické	akademický	k2eAgFnSc3d1	akademická
disciplíně	disciplína	k1gFnSc3	disciplína
byly	být	k5eAaImAgFnP	být
tzv.	tzv.	kA	tzv.
Giffordovy	Giffordův	k2eAgFnPc4d1	Giffordův
přednášky	přednáška	k1gFnPc4	přednáška
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Gifford	Gifford	k1gMnSc1	Gifford
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
určil	určit	k5eAaPmAgMnS	určit
částku	částka	k1gFnSc4	částka
80	[number]	k4	80
000	[number]	k4	000
liber	libra	k1gFnPc2	libra
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
přednášek	přednáška	k1gFnPc2	přednáška
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
konány	konat	k5eAaImNgFnP	konat
přísně	přísně	k6eAd1	přísně
vědeckým	vědecký	k2eAgInSc7d1	vědecký
<g/>
,	,	kIx,	,
nestranným	nestranný	k2eAgInSc7d1	nestranný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
finančně	finančně	k6eAd1	finančně
zajistit	zajistit	k5eAaPmF	zajistit
i	i	k9	i
příjezd	příjezd	k1gInSc4	příjezd
badatelů	badatel	k1gMnPc2	badatel
do	do	k7c2	do
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
přednášky	přednáška	k1gFnSc2	přednáška
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
tak	tak	k6eAd1	tak
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
i	i	k9	i
Cornelis	Cornelis	k1gInSc1	Cornelis
Petrus	Petrus	k1gInSc1	Petrus
Tiele	Tiel	k1gMnSc2	Tiel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
formování	formování	k1gNnSc3	formování
religionistiky	religionistika	k1gFnSc2	religionistika
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
o	o	k7c6	o
komparativních	komparativní	k2eAgFnPc6d1	komparativní
dějinách	dějiny	k1gFnPc6	dějiny
starověkých	starověký	k2eAgFnPc2d1	starověká
náboženství	náboženství	k1gNnSc4	náboženství
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
napomohl	napomoct	k5eAaPmAgMnS	napomoct
k	k	k7c3	k
rozvinutí	rozvinutí	k1gNnSc3	rozvinutí
koncepce	koncepce	k1gFnSc2	koncepce
dějin	dějiny	k1gFnPc2	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
katedry	katedra	k1gFnPc1	katedra
dějin	dějiny	k1gFnPc2	dějiny
náboženství	náboženství	k1gNnPc2	náboženství
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
a	a	k8xC	a
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
se	se	k3xPyFc4	se
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
otevíraly	otevírat	k5eAaImAgFnP	otevírat
katedry	katedra	k1gFnPc1	katedra
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
teologických	teologický	k2eAgFnPc6d1	teologická
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Leidenu	Leideno	k1gNnSc6	Leideno
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
postavil	postavit	k5eAaPmAgMnS	postavit
C.	C.	kA	C.
P.	P.	kA	P.
Tiele	Tiele	k1gInSc4	Tiele
a	a	k8xC	a
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
vedoucím	vedoucí	k1gMnSc7	vedoucí
P.	P.	kA	P.
D.	D.	kA	D.
Chantepie	Chantepie	k1gFnSc1	Chantepie
de	de	k?	de
la	la	k1gNnSc7	la
Saussaye	Saussay	k1gFnSc2	Saussay
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
zřízena	zřídit	k5eAaPmNgFnS	zřídit
první	první	k4xOgFnSc1	první
profesura	profesura	k1gFnSc1	profesura
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obdržel	obdržet	k5eAaPmAgInS	obdržet
A.	A.	kA	A.
Réville	Réville	k1gFnSc2	Réville
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgFnP	být
otevírány	otevírán	k2eAgFnPc1d1	otevírán
katedry	katedra	k1gFnPc1	katedra
religionistiky	religionistika	k1gFnSc2	religionistika
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Norsku	Norsko	k1gNnSc6	Norsko
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
religionistika	religionistika	k1gFnSc1	religionistika
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k9	i
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
–	–	k?	–
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
si	se	k3xPyFc3	se
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
např.	např.	kA	např.
na	na	k7c6	na
Hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
a	a	k8xC	a
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
probíhala	probíhat	k5eAaImAgNnP	probíhat
kongresová	kongresový	k2eAgNnPc1d1	Kongresové
setkání	setkání	k1gNnPc1	setkání
religionistů	religionista	k1gMnPc2	religionista
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
oficiální	oficiální	k2eAgInSc1d1	oficiální
výbor	výbor	k1gInSc1	výbor
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
až	až	k9	až
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
název	název	k1gInSc4	název
International	International	k1gFnSc3	International
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
the	the	k?	the
Study	stud	k1gInPc1	stud
of	of	k?	of
the	the	k?	the
History	Histor	k1gInPc1	Histor
of	of	k?	of
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
asociace	asociace	k1gFnSc1	asociace
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
dějin	dějiny	k1gFnPc2	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
IASHR	IASHR	kA	IASHR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
holandský	holandský	k2eAgMnSc1d1	holandský
religionista	religionista	k1gMnSc1	religionista
Gerardus	Gerardus	k1gMnSc1	Gerardus
van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Leeuw	Leeuw	k1gMnSc6	Leeuw
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
funkce	funkce	k1gFnPc4	funkce
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
Raffaele	Raffaela	k1gFnSc3	Raffaela
Pettazzoni	Pettazzon	k1gMnPc1	Pettazzon
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
asociace	asociace	k1gFnSc1	asociace
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
International	International	k1gFnSc4	International
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
the	the	k?	the
History	Histor	k1gInPc1	Histor
of	of	k?	of
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
asociace	asociace	k1gFnSc1	asociace
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
)	)	kIx)	)
se	s	k7c7	s
zkratkou	zkratka	k1gFnSc7	zkratka
IAHR	IAHR	kA	IAHR
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
Evropská	evropský	k2eAgFnSc1d1	Evropská
asociace	asociace	k1gFnSc1	asociace
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
European	European	k1gInSc1	European
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
the	the	k?	the
Study	stud	k1gInPc1	stud
of	of	k?	of
Religions	Religions	k1gInSc1	Religions
(	(	kIx(	(
<g/>
EASR	EASR	kA	EASR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
se	se	k3xPyFc4	se
religionistické	religionistický	k2eAgNnSc1d1	religionistické
bádání	bádání	k1gNnSc1	bádání
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
do	do	k7c2	do
téměř	téměř	k6eAd1	téměř
nepřehledné	přehledný	k2eNgFnSc2d1	nepřehledná
šíře	šíř	k1gFnSc2	šíř
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
religionistů	religionista	k1gMnPc2	religionista
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
určité	určitý	k2eAgNnSc4d1	určité
velmi	velmi	k6eAd1	velmi
úzké	úzký	k2eAgNnSc4d1	úzké
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
je	být	k5eAaImIp3nS	být
prací	práce	k1gFnSc7	práce
syntetického	syntetický	k2eAgInSc2d1	syntetický
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
moderním	moderní	k2eAgMnSc7d1	moderní
syntetikem	syntetik	k1gMnSc7	syntetik
byl	být	k5eAaImAgMnS	být
Mircea	Mircea	k1gMnSc1	Mircea
Eliade	Eliad	k1gInSc5	Eliad
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
hojně	hojně	k6eAd1	hojně
diskutována	diskutován	k2eAgFnSc1d1	diskutována
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
například	například	k6eAd1	například
francouzský	francouzský	k2eAgMnSc1d1	francouzský
badatel	badatel	k1gMnSc1	badatel
Georges	Georges	k1gMnSc1	Georges
Dumézil	Dumézil	k1gMnSc1	Dumézil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
religionistiku	religionistika	k1gFnSc4	religionistika
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
badatelé	badatel	k1gMnPc1	badatel
jako	jako	k8xC	jako
např.	např.	kA	např.
Jacques	Jacques	k1gMnSc1	Jacques
Waardenburg	Waardenburg	k1gMnSc1	Waardenburg
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
E.	E.	kA	E.
Paden	Paden	k1gInSc1	Paden
a	a	k8xC	a
Jonathan	Jonathan	k1gMnSc1	Jonathan
Z.	Z.	kA	Z.
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Religionistika	religionistika	k1gFnSc1	religionistika
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
obecně	obecně	k6eAd1	obecně
platné	platný	k2eAgFnSc2d1	platná
definice	definice	k1gFnSc2	definice
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
problematizovalo	problematizovat	k5eAaImAgNnS	problematizovat
definici	definice	k1gFnSc4	definice
<g/>
,	,	kIx,	,
že	že	k8xS	že
religionistika	religionistika	k1gFnSc1	religionistika
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgNnSc1d2	přesnější
je	být	k5eAaImIp3nS	být
definovat	definovat	k5eAaBmF	definovat
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
vědu	věda	k1gFnSc4	věda
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
a	a	k8xC	a
produktech	produkt	k1gInPc6	produkt
jeho	jeho	k3xOp3gNnSc2	jeho
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
nazýváme	nazývat	k5eAaImIp1nP	nazývat
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
religionistice	religionistika	k1gFnSc6	religionistika
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
množství	množství	k1gNnSc1	množství
různorodých	různorodý	k2eAgFnPc2d1	různorodá
metod	metoda	k1gFnPc2	metoda
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
filozofické	filozofický	k2eAgInPc4d1	filozofický
přístupy	přístup	k1gInPc4	přístup
<g/>
,	,	kIx,	,
sociologické	sociologický	k2eAgInPc4d1	sociologický
a	a	k8xC	a
historické	historický	k2eAgFnPc4d1	historická
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
indukce	indukce	k1gFnSc1	indukce
<g/>
,	,	kIx,	,
pozorování	pozorování	k1gNnSc1	pozorování
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Dílčí	dílčí	k2eAgInPc1d1	dílčí
podobory	podobor	k1gInPc1	podobor
navíc	navíc	k6eAd1	navíc
využívají	využívat	k5eAaPmIp3nP	využívat
své	svůj	k3xOyFgFnPc4	svůj
speciální	speciální	k2eAgFnPc4d1	speciální
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
psychologie	psychologie	k1gFnSc1	psychologie
náboženství	náboženství	k1gNnSc2	náboženství
fakta	faktum	k1gNnSc2	faktum
z	z	k7c2	z
osobních	osobní	k2eAgInPc2d1	osobní
dotazníků	dotazník	k1gInPc2	dotazník
<g/>
,	,	kIx,	,
sociometrie	sociometrie	k1gFnSc2	sociometrie
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Východiska	východisko	k1gNnSc2	východisko
pro	pro	k7c4	pro
systematizaci	systematizace	k1gFnSc4	systematizace
religionistiky	religionistika	k1gFnSc2	religionistika
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
historická	historický	k2eAgFnSc1d1	historická
religionistika	religionistika	k1gFnSc1	religionistika
<g/>
,	,	kIx,	,
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
religionistika	religionistika	k1gFnSc1	religionistika
<g/>
,	,	kIx,	,
kontextuální	kontextuální	k2eAgFnSc1d1	kontextuální
religionistika	religionistika	k1gFnSc1	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
místo	místo	k1gNnSc4	místo
patří	patřit	k5eAaImIp3nS	patřit
fenomenologii	fenomenologie	k1gFnSc3	fenomenologie
a	a	k8xC	a
hermeneutice	hermeneutika	k1gFnSc3	hermeneutika
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
si	se	k3xPyFc3	se
vydobyla	vydobýt	k5eAaPmAgFnS	vydobýt
i	i	k8xC	i
kognitivní	kognitivní	k2eAgFnSc1d1	kognitivní
religionistika	religionistika	k1gFnSc1	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
religionistika	religionistika	k1gFnSc1	religionistika
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
projevy	projev	k1gInPc4	projev
náboženství	náboženství	k1gNnPc2	náboženství
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
formách	forma	k1gFnPc6	forma
náboženství	náboženství	k1gNnSc2	náboženství
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
z	z	k7c2	z
dějinného	dějinný	k2eAgNnSc2d1	dějinné
hlediska	hledisko	k1gNnSc2	hledisko
uchopit	uchopit	k5eAaPmF	uchopit
utvářející	utvářející	k2eAgFnSc4d1	utvářející
se	se	k3xPyFc4	se
formy	forma	k1gFnPc4	forma
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
historické	historický	k2eAgFnSc2d1	historická
religionistiky	religionistika	k1gFnSc2	religionistika
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
dějiny	dějiny	k1gFnPc4	dějiny
se	se	k3xPyFc4	se
promítl	promítnout	k5eAaPmAgInS	promítnout
i	i	k9	i
do	do	k7c2	do
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
dějiny	dějiny	k1gFnPc4	dějiny
mimokřesťanských	mimokřesťanský	k2eAgNnPc2d1	mimokřesťanské
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
momentem	moment	k1gInSc7	moment
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
History	Histor	k1gInPc1	Histor
of	of	k?	of
Religion	religion	k1gInSc4	religion
na	na	k7c4	na
History	Histor	k1gInPc4	Histor
of	of	k?	of
Religions	Religions	k1gInSc1	Religions
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
badatelů	badatel	k1gMnPc2	badatel
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
obrátil	obrátit	k5eAaPmAgMnS	obrátit
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
pramenných	pramenný	k2eAgInPc2d1	pramenný
textů	text	k1gInPc2	text
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
náboženství	náboženství	k1gNnPc2	náboženství
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
podpořit	podpořit	k5eAaPmF	podpořit
tehdy	tehdy	k6eAd1	tehdy
populární	populární	k2eAgFnPc1d1	populární
hypotézy	hypotéza	k1gFnPc1	hypotéza
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
historické	historický	k2eAgNnSc4d1	historické
uchopení	uchopení	k1gNnSc4	uchopení
náboženství	náboženství	k1gNnSc2	náboženství
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
mnoha	mnoho	k4c2	mnoho
religionistů	religionista	k1gMnPc2	religionista
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Rudolfa	Rudolfa	k1gFnSc1	Rudolfa
Otta	Otta	k1gMnSc1	Otta
<g/>
,	,	kIx,	,
Mircea	Mircea	k1gMnSc1	Mircea
Eliadeho	Eliade	k1gMnSc2	Eliade
a	a	k8xC	a
Jacques	Jacques	k1gMnSc1	Jacques
Waardenburga	Waardenburga	k1gFnSc1	Waardenburga
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
religionistika	religionistika	k1gFnSc1	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
neboli	neboli	k8xC	neboli
komparativní	komparativní	k2eAgFnSc1d1	komparativní
religionistika	religionistika	k1gFnSc1	religionistika
různé	různý	k2eAgInPc4d1	různý
náboženské	náboženský	k2eAgInPc4d1	náboženský
jevy	jev	k1gInPc4	jev
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
srovnávání	srovnávání	k1gNnSc2	srovnávání
však	však	k9	však
nekončí	končit	k5eNaImIp3nS	končit
nalezením	nalezení	k1gNnSc7	nalezení
rozdílů	rozdíl	k1gInPc2	rozdíl
a	a	k8xC	a
podobností	podobnost	k1gFnPc2	podobnost
<g/>
;	;	kIx,	;
jakmile	jakmile	k8xS	jakmile
jsou	být	k5eAaImIp3nP	být
detailně	detailně	k6eAd1	detailně
prozkoumány	prozkoumán	k2eAgFnPc1d1	prozkoumána
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
náboženské	náboženský	k2eAgFnPc1d1	náboženská
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
sestavení	sestavení	k1gNnSc4	sestavení
obecných	obecný	k2eAgInPc2d1	obecný
vzorců	vzorec	k1gInPc2	vzorec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
lze	lze	k6eAd1	lze
zpětně	zpětně	k6eAd1	zpětně
aplikovat	aplikovat	k5eAaBmF	aplikovat
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc4d1	podobný
náboženské	náboženský	k2eAgInPc4d1	náboženský
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nP	by
bez	bez	k7c2	bez
těchto	tento	k3xDgInPc2	tento
vzorců	vzorec	k1gInPc2	vzorec
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
nesrozumitelné	srozumitelný	k2eNgFnPc1d1	nesrozumitelná
<g/>
.	.	kIx.	.
</s>
<s>
Srovnávací	srovnávací	k2eAgNnSc1d1	srovnávací
studium	studium	k1gNnSc1	studium
náboženství	náboženství	k1gNnSc2	náboženství
tak	tak	k9	tak
můžeme	moct	k5eAaImIp1nP	moct
vystopovat	vystopovat	k5eAaPmF	vystopovat
až	až	k9	až
k	k	k7c3	k
samotnému	samotný	k2eAgMnSc3d1	samotný
zakladateli	zakladatel	k1gMnSc3	zakladatel
religionistiky	religionistika	k1gFnSc2	religionistika
Maxu	Maxa	k1gMnSc4	Maxa
Müllerovi	Müller	k1gMnSc6	Müller
a	a	k8xC	a
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
snad	snad	k9	snad
neexistují	existovat	k5eNaImIp3nP	existovat
badatelé	badatel	k1gMnPc1	badatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
srovnávací	srovnávací	k2eAgFnSc3d1	srovnávací
perspektivě	perspektiva	k1gFnSc3	perspektiva
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
obtížné	obtížný	k2eAgNnSc1d1	obtížné
určit	určit	k5eAaPmF	určit
hlavní	hlavní	k2eAgMnPc4d1	hlavní
představitele	představitel	k1gMnPc4	představitel
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
by	by	kYmCp3nP	by
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
jistě	jistě	k9	jistě
již	již	k6eAd1	již
zmiňovaný	zmiňovaný	k2eAgMnSc1d1	zmiňovaný
Friedrich	Friedrich	k1gMnSc1	Friedrich
Max	Max	k1gMnSc1	Max
Müller	Müller	k1gMnSc1	Müller
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Cornelis	Cornelis	k1gFnSc1	Cornelis
Petrus	Petrus	k1gInSc1	Petrus
Tiele	Tiele	k1gFnSc1	Tiele
<g/>
,	,	kIx,	,
Georges	Georges	k1gMnSc1	Georges
Dumézil	Dumézil	k1gFnSc2	Dumézil
<g/>
,	,	kIx,	,
Raffaele	Raffaela	k1gFnSc6	Raffaela
Pettazzoni	Pettazzoň	k1gFnSc6	Pettazzoň
<g/>
,	,	kIx,	,
Mircea	Mircea	k1gMnSc1	Mircea
Eliade	Eliad	k1gInSc5	Eliad
či	či	k8xC	či
James	James	k1gMnSc1	James
Frazer	Frazer	k1gMnSc1	Frazer
<g/>
.	.	kIx.	.
</s>
<s>
Kontextuální	kontextuální	k2eAgFnSc1d1	kontextuální
religionistika	religionistika	k1gFnSc1	religionistika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
náboženství	náboženství	k1gNnSc4	náboženství
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
člověk	člověk	k1gMnSc1	člověk
získá	získat	k5eAaPmIp3nS	získat
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
vždy	vždy	k6eAd1	vždy
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
jaké	jaký	k3yIgFnSc2	jaký
perspektivy	perspektiva	k1gFnSc2	perspektiva
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Filozofie	filozofie	k1gFnSc1	filozofie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Filozofie	filozofie	k1gFnSc1	filozofie
náboženství	náboženství	k1gNnSc2	náboženství
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vztahem	vztah	k1gInSc7	vztah
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
filozofického	filozofický	k2eAgNnSc2d1	filozofické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgInSc4d1	určitý
typ	typ	k1gInSc4	typ
filozofie	filozofie	k1gFnSc2	filozofie
náboženství	náboženství	k1gNnPc2	náboženství
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
už	už	k6eAd1	už
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
filozofické	filozofický	k2eAgFnSc2d1	filozofická
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
uchopení	uchopení	k1gNnSc4	uchopení
náboženství	náboženství	k1gNnSc2	náboženství
se	se	k3xPyFc4	se
soustředily	soustředit	k5eAaPmAgFnP	soustředit
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
západní	západní	k2eAgFnPc4d1	západní
náboženské	náboženský	k2eAgFnPc4d1	náboženská
tradice	tradice	k1gFnPc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platilo	platit	k5eAaImAgNnS	platit
i	i	k9	i
pro	pro	k7c4	pro
středověk	středověk	k1gInSc4	středověk
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
"	"	kIx"	"
<g/>
moderní	moderní	k2eAgFnSc1d1	moderní
<g/>
"	"	kIx"	"
filozofie	filozofie	k1gFnSc1	filozofie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
omezení	omezení	k1gNnSc1	omezení
na	na	k7c6	na
výhradně	výhradně	k6eAd1	výhradně
západní	západní	k2eAgNnSc1d1	západní
zkoumání	zkoumání	k1gNnSc1	zkoumání
náboženství	náboženství	k1gNnSc2	náboženství
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
západní	západní	k2eAgFnSc4d1	západní
filozofii	filozofie	k1gFnSc4	filozofie
chyběla	chybět	k5eAaImAgFnS	chybět
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
srovnávací	srovnávací	k2eAgNnSc4d1	srovnávací
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Filozofie	filozofie	k1gFnSc1	filozofie
náboženství	náboženství	k1gNnSc2	náboženství
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
religionistických	religionistický	k2eAgFnPc2d1	religionistická
otázek	otázka	k1gFnPc2	otázka
jak	jak	k8xC	jak
analýzou	analýza	k1gFnSc7	analýza
pojmů	pojem	k1gInPc2	pojem
a	a	k8xC	a
zdrojů	zdroj	k1gInPc2	zdroj
náboženského	náboženský	k2eAgNnSc2d1	náboženské
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
sobě	se	k3xPyFc3	se
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
metodologií	metodologie	k1gFnPc2	metodologie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ranou	raný	k2eAgFnSc4d1	raná
religionistiku	religionistika	k1gFnSc4	religionistika
znamenala	znamenat	k5eAaImAgFnS	znamenat
filozofie	filozofie	k1gFnSc1	filozofie
náboženství	náboženství	k1gNnSc2	náboženství
mocný	mocný	k2eAgInSc4d1	mocný
impulz	impulz	k1gInSc4	impulz
<g/>
.	.	kIx.	.
</s>
<s>
Novodobí	novodobý	k2eAgMnPc1d1	novodobý
představitelé	představitel	k1gMnPc1	představitel
filozofie	filozofie	k1gFnSc2	filozofie
náboženství	náboženství	k1gNnSc2	náboženství
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Rudolf	Rudolf	k1gMnSc1	Rudolf
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Heiler	Heiler	k1gMnSc1	Heiler
či	či	k8xC	či
Břetislav	Břetislav	k1gMnSc1	Břetislav
Horyna	Horyna	k1gMnSc1	Horyna
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sociologie	sociologie	k1gFnSc2	sociologie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Sociologie	sociologie	k1gFnSc1	sociologie
náboženství	náboženství	k1gNnSc2	náboženství
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
sociální	sociální	k2eAgFnSc1d1	sociální
determinovanost	determinovanost	k1gFnSc1	determinovanost
(	(	kIx(	(
<g/>
předurčenost	předurčenost	k1gFnSc1	předurčenost
<g/>
)	)	kIx)	)
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
důvod	důvod	k1gInSc1	důvod
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
působení	působení	k1gNnSc2	působení
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
funkcí	funkce	k1gFnSc7	funkce
a	a	k8xC	a
rolí	role	k1gFnSc7	role
náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
sociálních	sociální	k2eAgInPc6d1	sociální
systémech	systém	k1gInPc6	systém
včetně	včetně	k7c2	včetně
zpětného	zpětný	k2eAgInSc2d1	zpětný
vlivu	vliv	k1gInSc2	vliv
sociálních	sociální	k2eAgInPc2d1	sociální
systémů	systém	k1gInPc2	systém
na	na	k7c4	na
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vlastní	vlastní	k2eAgMnPc4d1	vlastní
zakladatele	zakladatel	k1gMnPc4	zakladatel
sociologie	sociologie	k1gFnSc2	sociologie
náboženství	náboženství	k1gNnSc2	náboženství
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
Émile	Émile	k1gFnPc1	Émile
Durkheim	Durkheimo	k1gNnPc2	Durkheimo
a	a	k8xC	a
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc4	její
dějiny	dějiny	k1gFnPc4	dějiny
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
období	období	k1gNnPc2	období
<g/>
:	:	kIx,	:
předklasické	předklasický	k2eAgNnSc1d1	předklasický
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Spencer	Spencer	k1gMnSc1	Spencer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klasické	klasický	k2eAgNnSc4d1	klasické
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
Émile	Émile	k1gFnSc1	Émile
Durkheim	Durkheim	k1gMnSc1	Durkheim
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
Bronisław	Bronisław	k1gMnSc1	Bronisław
Malinowski	Malinowsk	k1gFnSc2	Malinowsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
současné	současný	k2eAgNnSc4d1	současné
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
Joachim	Joachim	k1gMnSc1	Joachim
Wach	Wach	k1gMnSc1	Wach
<g/>
,	,	kIx,	,
Niklas	Niklas	k1gMnSc1	Niklas
Luhmann	Luhmann	k1gMnSc1	Luhmann
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Milton	Milton	k1gInSc1	Milton
Yinger	Yinger	k1gInSc1	Yinger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychologie	psychologie	k1gFnSc2	psychologie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
náboženství	náboženství	k1gNnSc2	náboženství
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
američtí	americký	k2eAgMnPc1d1	americký
badatelé	badatel	k1gMnPc1	badatel
Edwin	Edwin	k1gMnSc1	Edwin
Diller	Diller	k1gMnSc1	Diller
Starbuck	Starbuck	k1gMnSc1	Starbuck
a	a	k8xC	a
James	James	k1gMnSc1	James
Henry	Henry	k1gMnSc1	Henry
Leuba	Leuba	k1gMnSc1	Leuba
prováděli	provádět	k5eAaImAgMnP	provádět
dotazníkový	dotazníkový	k2eAgInSc4d1	dotazníkový
průzkum	průzkum	k1gInSc4	průzkum
o	o	k7c6	o
konverzích	konverze	k1gFnPc6	konverze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
vydal	vydat	k5eAaPmAgInS	vydat
William	William	k1gInSc1	William
James	James	k1gMnSc1	James
knihu	kniha	k1gFnSc4	kniha
Druhy	druh	k1gInPc7	druh
náboženské	náboženský	k2eAgFnSc2d1	náboženská
zkušenosti	zkušenost	k1gFnSc2	zkušenost
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
varieties	varieties	k1gMnSc1	varieties
of	of	k?	of
religious	religious	k1gMnSc1	religious
experience	experience	k1gFnSc2	experience
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k6eAd1	George
Wobbermin	Wobbermin	k2eAgInSc1d1	Wobbermin
ji	on	k3xPp3gFnSc4	on
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
zakladatelem	zakladatel	k1gMnSc7	zakladatel
psychologie	psychologie	k1gFnSc2	psychologie
náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
přínos	přínos	k1gInSc4	přínos
měla	mít	k5eAaImAgFnS	mít
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
a	a	k8xC	a
hlubinná	hlubinný	k2eAgFnSc1d1	hlubinná
psychologie	psychologie	k1gFnSc1	psychologie
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
představitelé	představitel	k1gMnPc1	představitel
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
a	a	k8xC	a
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
vnímal	vnímat	k5eAaImAgInS	vnímat
náboženství	náboženství	k1gNnSc4	náboženství
jako	jako	k8xS	jako
iluzi	iluze	k1gFnSc4	iluze
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
sám	sám	k3xTgInSc1	sám
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
měl	mít	k5eAaImAgMnS	mít
Jung	Jung	k1gMnSc1	Jung
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
afirmativní	afirmativní	k2eAgInSc4d1	afirmativní
(	(	kIx(	(
<g/>
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
<g/>
)	)	kIx)	)
poměr	poměr	k1gInSc4	poměr
<g/>
,	,	kIx,	,
a	a	k8xC	a
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
studnici	studnice	k1gFnSc4	studnice
archetypů	archetyp	k1gInPc2	archetyp
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
může	moct	k5eAaImIp3nS	moct
lékař	lékař	k1gMnSc1	lékař
čerpat	čerpat	k5eAaImF	čerpat
užitečné	užitečný	k2eAgFnPc4d1	užitečná
paralely	paralela	k1gFnPc4	paralela
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
psychologie	psychologie	k1gFnSc2	psychologie
náboženství	náboženství	k1gNnSc2	náboženství
metodologicky	metodologicky	k6eAd1	metodologicky
vycházeli	vycházet	k5eAaImAgMnP	vycházet
badatelé	badatel	k1gMnPc1	badatel
jako	jako	k8xC	jako
Rudolf	Rudolf	k1gMnSc1	Rudolf
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Gerardus	Gerardus	k1gMnSc1	Gerardus
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Leeuw	Leeuw	k1gMnSc1	Leeuw
či	či	k8xC	či
Friedrich	Friedrich	k1gMnSc1	Friedrich
Heiler	Heiler	k1gMnSc1	Heiler
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Antropologie	antropologie	k1gFnSc2	antropologie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Antropologie	antropologie	k1gFnSc1	antropologie
náboženství	náboženství	k1gNnSc2	náboženství
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
religionistika	religionistika	k1gFnSc1	religionistika
i	i	k8xC	i
antropologie	antropologie	k1gFnSc1	antropologie
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgInPc1d1	samostatný
obory	obor	k1gInPc1	obor
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
rozvoj	rozvoj	k1gInSc1	rozvoj
podpořil	podpořit	k5eAaPmAgInS	podpořit
zejména	zejména	k9	zejména
evolucionismus	evolucionismus	k1gInSc1	evolucionismus
navazující	navazující	k2eAgInSc1d1	navazující
na	na	k7c4	na
Charlese	Charles	k1gMnSc4	Charles
Darwina	Darwin	k1gMnSc4	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Antropologie	antropologie	k1gFnSc1	antropologie
náboženství	náboženství	k1gNnSc2	náboženství
staví	stavit	k5eAaBmIp3nS	stavit
především	především	k9	především
na	na	k7c6	na
etnologických	etnologický	k2eAgInPc6d1	etnologický
a	a	k8xC	a
antropologických	antropologický	k2eAgInPc6d1	antropologický
výzkumech	výzkum	k1gInPc6	výzkum
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
objasnit	objasnit	k5eAaPmF	objasnit
takové	takový	k3xDgInPc4	takový
jevy	jev	k1gInPc4	jev
jako	jako	k8xS	jako
šamanismus	šamanismus	k1gInSc1	šamanismus
<g/>
,	,	kIx,	,
animismus	animismus	k1gInSc1	animismus
<g/>
,	,	kIx,	,
magie	magie	k1gFnSc1	magie
<g/>
,	,	kIx,	,
kult	kult	k1gInSc1	kult
předků	předek	k1gMnPc2	předek
apod.	apod.	kA	apod.
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
antropologie	antropologie	k1gFnSc2	antropologie
náboženství	náboženství	k1gNnSc2	náboženství
byl	být	k5eAaImAgMnS	být
Edward	Edward	k1gMnSc1	Edward
Burnett	Burnett	k1gMnSc1	Burnett
Tylor	Tylor	k1gMnSc1	Tylor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
oblasti	oblast	k1gFnSc6	oblast
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
představitelům	představitel	k1gMnPc3	představitel
Leo	Leo	k1gMnSc1	Leo
Frobenius	Frobenius	k1gMnSc1	Frobenius
a	a	k8xC	a
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
.	.	kIx.	.
</s>
<s>
Antropologie	antropologie	k1gFnSc1	antropologie
náboženství	náboženství	k1gNnSc2	náboženství
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
hlavních	hlavní	k2eAgInPc2d1	hlavní
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
strukturálně	strukturálně	k6eAd1	strukturálně
funkcionalistický	funkcionalistický	k2eAgInSc4d1	funkcionalistický
<g/>
,	,	kIx,	,
reprezentovaný	reprezentovaný	k2eAgInSc4d1	reprezentovaný
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
Bronislawem	Bronislawem	k1gInSc4	Bronislawem
Malinowskim	Malinowski	k1gNnSc7	Malinowski
a	a	k8xC	a
Edwardem	Edward	k1gMnSc7	Edward
Evans-Pritchardem	Evans-Pritchard	k1gMnSc7	Evans-Pritchard
<g/>
,	,	kIx,	,
představitelem	představitel	k1gMnSc7	představitel
sociální	sociální	k2eAgFnSc2d1	sociální
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
směr	směr	k1gInSc1	směr
staví	stavit	k5eAaPmIp3nS	stavit
na	na	k7c4	na
strukturální	strukturální	k2eAgFnSc4d1	strukturální
antropologii	antropologie	k1gFnSc4	antropologie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
Claude	Claud	k1gInSc5	Claud
Lévi-Straussem	Lévi-Strauss	k1gMnSc7	Lévi-Strauss
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
antropologie	antropologie	k1gFnSc2	antropologie
náboženství	náboženství	k1gNnSc2	náboženství
výrazně	výrazně	k6eAd1	výrazně
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
i	i	k9	i
James	James	k1gMnSc1	James
Frazer	Frazer	k1gInSc1	Frazer
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ratolest	ratolest	k1gFnSc1	ratolest
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Golden	Goldna	k1gFnPc2	Goldna
Bough	Bougha	k1gFnPc2	Bougha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnSc4d2	novější
generaci	generace	k1gFnSc4	generace
antropologů	antropolog	k1gMnPc2	antropolog
náboženství	náboženství	k1gNnSc2	náboženství
představují	představovat	k5eAaImIp3nP	představovat
např.	např.	kA	např.
Mary	Mary	k1gFnSc4	Mary
Douglas	Douglasa	k1gFnPc2	Douglasa
či	či	k8xC	či
Clifford	Clifford	k1gMnSc1	Clifford
Geertz	Geertz	k1gMnSc1	Geertz
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
symbolického	symbolický	k2eAgInSc2d1	symbolický
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Geografie	geografie	k1gFnSc1	geografie
náboženství	náboženství	k1gNnPc2	náboženství
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
spojitosti	spojitost	k1gFnPc4	spojitost
mezi	mezi	k7c7	mezi
zeměpisným	zeměpisný	k2eAgNnSc7d1	zeměpisné
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
,	,	kIx,	,
půdy	půda	k1gFnSc2	půda
apod.	apod.	kA	apod.
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
náboženské	náboženský	k2eAgInPc4d1	náboženský
procesy	proces	k1gInPc4	proces
a	a	k8xC	a
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
geografie	geografie	k1gFnSc2	geografie
náboženství	náboženství	k1gNnSc2	náboženství
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
zejména	zejména	k9	zejména
církevní	církevní	k2eAgFnPc4d1	církevní
geografie	geografie	k1gFnPc4	geografie
(	(	kIx(	(
<g/>
mapování	mapování	k1gNnSc4	mapování
expanze	expanze	k1gFnSc2	expanze
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
)	)	kIx)	)
a	a	k8xC	a
biblická	biblický	k2eAgFnSc1d1	biblická
geografie	geografie	k1gFnSc1	geografie
(	(	kIx(	(
<g/>
pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
lokalizaci	lokalizace	k1gFnSc6	lokalizace
biblických	biblický	k2eAgNnPc2d1	biblické
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
geografie	geografie	k1gFnSc2	geografie
náboženství	náboženství	k1gNnSc2	náboženství
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
v	v	k7c6	v
podobném	podobný	k2eAgMnSc6d1	podobný
duchu	duch	k1gMnSc6	duch
až	až	k9	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
nastal	nastat	k5eAaPmAgInS	nastat
průlom	průlom	k1gInSc1	průlom
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
americký	americký	k2eAgMnSc1d1	americký
badatel	badatel	k1gMnSc1	badatel
D.	D.	kA	D.
E.	E.	kA	E.
Sopher	Sophra	k1gFnPc2	Sophra
a	a	k8xC	a
německý	německý	k2eAgMnSc1d1	německý
etnograf	etnograf	k1gMnSc1	etnograf
M.	M.	kA	M.
Büttner	Büttner	k1gInSc1	Büttner
začali	začít	k5eAaPmAgMnP	začít
dovolávat	dovolávat	k5eAaImF	dovolávat
většího	veliký	k2eAgNnSc2d2	veliký
a	a	k8xC	a
širšího	široký	k2eAgNnSc2d2	širší
využití	využití	k1gNnSc2	využití
geografie	geografie	k1gFnSc2	geografie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Büttner	Büttner	k1gInSc1	Büttner
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
tzv.	tzv.	kA	tzv.
bochumský	bochumský	k2eAgInSc4d1	bochumský
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
právě	právě	k9	právě
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
působením	působení	k1gNnSc7	působení
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
geografického	geografický	k2eAgNnSc2d1	geografické
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
geografie	geografie	k1gFnSc1	geografie
náboženství	náboženství	k1gNnSc2	náboženství
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
představitelům	představitel	k1gMnPc3	představitel
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
patří	patřit	k5eAaImIp3nS	patřit
Manfred	Manfred	k1gMnSc1	Manfred
Büttner	Büttner	k1gMnSc1	Büttner
<g/>
,	,	kIx,	,
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
Antoni	Anton	k1gMnPc1	Anton
Jackowski	Jackowski	k1gNnSc2	Jackowski
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
D.	D.	kA	D.
E.	E.	kA	E.
Sopher	Sophra	k1gFnPc2	Sophra
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fenomenologie	fenomenologie	k1gFnSc2	fenomenologie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
historické	historický	k2eAgFnSc2d1	historická
religionistiky	religionistika	k1gFnSc2	religionistika
se	se	k3xPyFc4	se
fenomenologie	fenomenologie	k1gFnSc1	fenomenologie
náboženství	náboženství	k1gNnPc2	náboženství
zabývá	zabývat	k5eAaImIp3nS	zabývat
vzájemným	vzájemný	k2eAgInSc7d1	vzájemný
vztahem	vztah	k1gInSc7	vztah
a	a	k8xC	a
uspořádáním	uspořádání	k1gNnSc7	uspořádání
náboženských	náboženský	k2eAgInPc2d1	náboženský
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
užil	užít	k5eAaPmAgMnS	užít
výraz	výraz	k1gInSc4	výraz
fenomenologie	fenomenologie	k1gFnSc2	fenomenologie
náboženství	náboženství	k1gNnSc2	náboženství
Pierre	Pierr	k1gInSc5	Pierr
Daniel	Daniel	k1gMnSc1	Daniel
Chantepie	Chantepie	k1gFnSc2	Chantepie
de	de	k?	de
la	la	k1gNnSc1	la
Saussaye	Saussay	k1gInSc2	Saussay
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
;	;	kIx,	;
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
neujal	ujmout	k5eNaPmAgMnS	ujmout
ani	ani	k8xC	ani
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Uznání	uznání	k1gNnSc1	uznání
se	se	k3xPyFc4	se
oboru	obor	k1gInSc6	obor
dostalo	dostat	k5eAaPmAgNnS	dostat
až	až	k6eAd1	až
když	když	k8xS	když
takřka	takřka	k6eAd1	takřka
o	o	k7c4	o
čtyřicet	čtyřicet	k4xCc4	čtyřicet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgInS	vydat
Gerardus	Gerardus	k1gInSc1	Gerardus
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Leeuw	Leeuw	k1gFnSc3	Leeuw
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
fenomenologie	fenomenologie	k1gFnSc2	fenomenologie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Leeuw	Leeuw	k1gFnPc1	Leeuw
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
vznik	vznik	k1gInSc4	vznik
byl	být	k5eAaImAgInS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
nárůst	nárůst	k1gInSc1	nárůst
etnografických	etnografický	k2eAgInPc2d1	etnografický
<g/>
,	,	kIx,	,
archeologických	archeologický	k2eAgInPc2d1	archeologický
a	a	k8xC	a
filologických	filologický	k2eAgInPc2d1	filologický
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
shromážděného	shromážděný	k2eAgInSc2d1	shromážděný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yQgFnPc3	který
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
systematicky	systematicky	k6eAd1	systematicky
pracovat	pracovat	k5eAaImF	pracovat
a	a	k8xC	a
uspořádat	uspořádat	k5eAaPmF	uspořádat
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Fenomenologie	fenomenologie	k1gFnSc1	fenomenologie
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jevila	jevit	k5eAaImAgFnS	jevit
jako	jako	k9	jako
dobrý	dobrý	k2eAgInSc4d1	dobrý
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
představitelům	představitel	k1gMnPc3	představitel
patří	patřit	k5eAaImIp3nP	patřit
mimo	mimo	k7c4	mimo
van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Leeuwa	Leeuw	k1gInSc2	Leeuw
i	i	k9	i
Friedrich	Friedrich	k1gMnSc1	Friedrich
Heiler	Heiler	k1gMnSc1	Heiler
<g/>
,	,	kIx,	,
Joachim	Joachim	k1gMnSc1	Joachim
Wach	Wach	k1gMnSc1	Wach
či	či	k8xC	či
Mircea	Mircea	k1gMnSc1	Mircea
Eliade	Eliad	k1gInSc5	Eliad
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hermeneutika	hermeneutika	k1gFnSc1	hermeneutika
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Hermeneutická	hermeneutický	k2eAgFnSc1d1	hermeneutická
religionistika	religionistika	k1gFnSc1	religionistika
neboli	neboli	k8xC	neboli
hermeneutika	hermeneutika	k1gFnSc1	hermeneutika
náboženství	náboženství	k1gNnSc2	náboženství
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vyložit	vyložit	k5eAaPmF	vyložit
smysl	smysl	k1gInSc4	smysl
a	a	k8xC	a
význam	význam	k1gInSc4	význam
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
dílčích	dílčí	k2eAgInPc6d1	dílčí
aspektech	aspekt	k1gInPc6	aspekt
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
globálním	globální	k2eAgNnSc6d1	globální
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
vzniku	vznik	k1gInSc6	vznik
hráli	hrát	k5eAaImAgMnP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
filozofové	filozof	k1gMnPc1	filozof
Wilhelm	Wilhelma	k1gFnPc2	Wilhelma
Dilthey	Dilthea	k1gFnSc2	Dilthea
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Heidegger	Heidegger	k1gMnSc1	Heidegger
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
klasické	klasický	k2eAgFnSc2d1	klasická
hermeneutiky	hermeneutika	k1gFnSc2	hermeneutika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
klasické	klasický	k2eAgMnPc4d1	klasický
představitele	představitel	k1gMnPc4	představitel
hermeneutiky	hermeneutika	k1gFnSc2	hermeneutika
náboženství	náboženství	k1gNnSc2	náboženství
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
Joachima	Joachim	k1gMnSc4	Joachim
Wacha	Wach	k1gMnSc4	Wach
a	a	k8xC	a
Mircea	Mirceus	k1gMnSc4	Mirceus
Eliadeho	Eliade	k1gMnSc4	Eliade
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
současné	současný	k2eAgFnSc2d1	současná
hermeneutiky	hermeneutika	k1gFnSc2	hermeneutika
náboženství	náboženství	k1gNnSc2	náboženství
je	být	k5eAaImIp3nS	být
Jacques	Jacques	k1gMnSc1	Jacques
Waardenburg	Waardenburg	k1gMnSc1	Waardenburg
<g/>
,	,	kIx,	,
zastánce	zastánce	k1gMnSc1	zastánce
tzv.	tzv.	kA	tzv.
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
hermeneutiky	hermeneutika	k1gFnSc2	hermeneutika
<g/>
.	.	kIx.	.
</s>
<s>
Formování	formování	k1gNnSc1	formování
religionistiky	religionistika	k1gFnSc2	religionistika
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
badatelského	badatelský	k2eAgNnSc2d1	badatelské
úsilí	úsilí	k1gNnSc2	úsilí
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Otakar	Otakar	k1gMnSc1	Otakar
Pertold	Pertold	k1gMnSc1	Pertold
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
napsal	napsat	k5eAaBmAgInS	napsat
Základy	základ	k1gInPc1	základ
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
vědy	věda	k1gFnSc2	věda
náboženské	náboženský	k2eAgFnSc2d1	náboženská
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
prací	práce	k1gFnPc2	práce
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
Filosofie	filosofie	k1gFnSc1	filosofie
náboženství	náboženství	k1gNnSc2	náboženství
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Josefa	Josef	k1gMnSc4	Josef
Tvrdého	Tvrdý	k1gMnSc4	Tvrdý
či	či	k8xC	či
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
publikace	publikace	k1gFnSc1	publikace
Emiliána	Emilián	k1gMnSc2	Emilián
Soukupa	Soukup	k1gMnSc2	Soukup
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdý	Tvrdý	k1gMnSc1	Tvrdý
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
náboženství	náboženství	k1gNnSc1	náboženství
mělo	mít	k5eAaImAgNnS	mít
studovat	studovat	k5eAaImF	studovat
"	"	kIx"	"
<g/>
zvnějšku	zvnějšku	k6eAd1	zvnějšku
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Soukup	Soukup	k1gMnSc1	Soukup
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
záležitostí	záležitost	k1gFnSc7	záležitost
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gInSc4	on
prožívají	prožívat	k5eAaImIp3nP	prožívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
specializovaný	specializovaný	k2eAgInSc1d1	specializovaný
výzkum	výzkum	k1gInSc1	výzkum
konkrétních	konkrétní	k2eAgNnPc2d1	konkrétní
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
českoslovenští	československý	k2eAgMnPc1d1	československý
výzkumníci	výzkumník	k1gMnPc1	výzkumník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zakladatel	zakladatel	k1gMnSc1	zakladatel
československé	československý	k2eAgFnSc2d1	Československá
egyptologie	egyptologie	k1gFnSc2	egyptologie
František	František	k1gMnSc1	František
Lexa	Lexa	k1gMnSc1	Lexa
<g/>
,	,	kIx,	,
asyrolog	asyrolog	k1gMnSc1	asyrolog
Václav	Václav	k1gMnSc1	Václav
Hazuka	hazuka	k1gFnSc1	hazuka
či	či	k8xC	či
Bedřich	Bedřich	k1gMnSc1	Bedřich
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	on	k3xPp3gNnSc4	on
znám	znát	k5eAaImIp1nS	znát
rozluštěním	rozluštění	k1gNnSc7	rozluštění
chetitštiny	chetitština	k1gFnSc2	chetitština
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc4d3	veliký
zastoupení	zastoupení	k1gNnSc4	zastoupení
měla	mít	k5eAaImAgFnS	mít
československá	československý	k2eAgFnSc1d1	Československá
religionistika	religionistika	k1gFnSc1	religionistika
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
indologie	indologie	k1gFnSc2	indologie
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
Vincenc	Vincenc	k1gMnSc1	Vincenc
Lesný	lesný	k2eAgMnSc1d1	lesný
<g/>
,	,	kIx,	,
Leopold	Leopold	k1gMnSc1	Leopold
Procházka	Procházka	k1gMnSc1	Procházka
či	či	k8xC	či
sám	sám	k3xTgMnSc1	sám
Otakar	Otakar	k1gMnSc1	Otakar
Pertold	Pertold	k1gMnSc1	Pertold
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
a	a	k8xC	a
nastávajícího	nastávající	k2eAgInSc2d1	nastávající
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
nastalo	nastat	k5eAaPmAgNnS	nastat
pronásledování	pronásledování	k1gNnSc1	pronásledování
a	a	k8xC	a
útlak	útlak	k1gInSc1	útlak
<g/>
.	.	kIx.	.
</s>
<s>
Záviš	Záviš	k1gMnSc1	Záviš
Kalandra	Kalandra	k1gFnSc1	Kalandra
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
publikací	publikace	k1gFnPc2	publikace
o	o	k7c6	o
pohanských	pohanský	k2eAgInPc6d1	pohanský
kultech	kult	k1gInPc6	kult
a	a	k8xC	a
staroslovanském	staroslovanský	k2eAgNnSc6d1	staroslovanské
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
popraven	popravit	k5eAaPmNgInS	popravit
po	po	k7c6	po
zinscenovaném	zinscenovaný	k2eAgInSc6d1	zinscenovaný
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Religionistika	religionistika	k1gFnSc1	religionistika
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
na	na	k7c4	na
bezmála	bezmála	k6eAd1	bezmála
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neoficiálně	neoficiálně	k6eAd1	neoficiálně
našla	najít	k5eAaPmAgFnS	najít
útočiště	útočiště	k1gNnSc4	útočiště
na	na	k7c6	na
teologických	teologický	k2eAgFnPc6d1	teologická
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
v	v	k7c6	v
religionistickém	religionistický	k2eAgInSc6d1	religionistický
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Specializované	specializovaný	k2eAgInPc1d1	specializovaný
výzkumy	výzkum	k1gInPc1	výzkum
náboženství	náboženství	k1gNnPc2	náboženství
však	však	k9	však
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
egyptologie	egyptologie	k1gFnSc1	egyptologie
<g/>
,	,	kIx,	,
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
v	v	k7c6	v
Zbyňku	Zbyněk	k1gMnSc6	Zbyněk
Žábovi	Žába	k1gMnSc6	Žába
a	a	k8xC	a
Miroslavu	Miroslav	k1gMnSc6	Miroslav
Vernerovi	Verner	k1gMnSc6	Verner
<g/>
,	,	kIx,	,
či	či	k8xC	či
indologie	indologie	k1gFnSc1	indologie
<g/>
,	,	kIx,	,
reprezentovaná	reprezentovaný	k2eAgFnSc1d1	reprezentovaná
osobností	osobnost	k1gFnSc7	osobnost
Dušana	Dušan	k1gMnSc2	Dušan
Zbavitele	zbavitel	k1gMnSc2	zbavitel
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
religionistika	religionistika	k1gFnSc1	religionistika
přežívala	přežívat	k5eAaImAgFnS	přežívat
i	i	k9	i
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dobovému	dobový	k2eAgNnSc3d1	dobové
pozadí	pozadí	k1gNnSc3	pozadí
není	být	k5eNaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
československou	československý	k2eAgFnSc4d1	Československá
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
emigrantům	emigrant	k1gMnPc3	emigrant
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
indolog	indolog	k1gMnSc1	indolog
Karel	Karel	k1gMnSc1	Karel
Werner	Werner	k1gMnSc1	Werner
<g/>
,	,	kIx,	,
orientalista	orientalista	k1gMnSc1	orientalista
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Krejčí	Krejčí	k1gMnSc1	Krejčí
či	či	k8xC	či
egyptolog	egyptolog	k1gMnSc1	egyptolog
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
religionistika	religionistika	k1gFnSc1	religionistika
opět	opět	k6eAd1	opět
ožila	ožít	k5eAaPmAgFnS	ožít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
společnost	společnost	k1gFnSc4	společnost
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
náboženství	náboženství	k1gNnSc2	náboženství
(	(	kIx(	(
<g/>
ČSSN	ČSSN	kA	ČSSN
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
religionistiku	religionistika	k1gFnSc4	religionistika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
Jan	Jan	k1gMnSc1	Jan
Heller	Heller	k1gMnSc1	Heller
(	(	kIx(	(
<g/>
dalšími	další	k2eAgMnPc7d1	další
předsedy	předseda	k1gMnPc7	předseda
společnosti	společnost	k1gFnSc2	společnost
byli	být	k5eAaImAgMnP	být
Břetislav	Břetislav	k1gMnSc1	Břetislav
Horyna	Horyna	k1gMnSc1	Horyna
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Václavík	Václavík	k1gMnSc1	Václavík
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Bubík	Bubík	k1gMnSc1	Bubík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
v	v	k7c6	v
Liblicích	Liblice	k1gFnPc6	Liblice
1	[number]	k4	1
<g/>
.	.	kIx.	.
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
současný	současný	k2eAgInSc4d1	současný
stav	stav	k1gInSc4	stav
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
religionistiky	religionistika	k1gFnSc2	religionistika
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
náboženství	náboženství	k1gNnSc2	náboženství
přijata	přijmout	k5eAaPmNgFnS	přijmout
za	za	k7c2	za
člena	člen	k1gMnSc4	člen
The	The	k1gMnSc2	The
International	International	k1gMnSc2	International
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
the	the	k?	the
History	Histor	k1gInPc1	Histor
of	of	k?	of
Religions	Religions	k1gInSc1	Religions
(	(	kIx(	(
<g/>
IAHR	IAHR	kA	IAHR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
ČSSN	ČSSN	kA	ČSSN
vydává	vydávat	k5eAaImIp3nS	vydávat
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
časopis	časopis	k1gInSc1	časopis
Religio	Religio	k6eAd1	Religio
<g/>
:	:	kIx,	:
Revue	revue	k1gFnSc1	revue
pro	pro	k7c4	pro
religionistiku	religionistika	k1gFnSc4	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
vědeckým	vědecký	k2eAgInSc7d1	vědecký
časopisem	časopis	k1gInSc7	časopis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vydáván	vydávat	k5eAaImNgInS	vydávat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
české	český	k2eAgFnSc2d1	Česká
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
časopis	časopis	k1gInSc1	časopis
Pantheon	Pantheon	k1gInSc1	Pantheon
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
Jan	Jan	k1gMnSc1	Jan
Heller	Heller	k1gMnSc1	Heller
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
funkce	funkce	k1gFnSc2	funkce
ČSSN	ČSSN	kA	ČSSN
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gMnSc4	on
Břetislav	Břetislav	k1gMnSc1	Břetislav
Horyna	Horyna	k1gMnSc1	Horyna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
zářím	zářit	k5eAaImIp1nS	zářit
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
konala	konat	k5eAaImAgFnS	konat
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
konference	konference	k1gFnSc1	konference
Evropské	evropský	k2eAgFnSc2d1	Evropská
asociace	asociace	k1gFnSc2	asociace
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
byl	být	k5eAaImAgMnS	být
obor	obor	k1gInSc4	obor
religionistika	religionistika	k1gFnSc1	religionistika
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
na	na	k7c6	na
Ústavu	ústav	k1gInSc6	ústav
religionistiky	religionistika	k1gFnSc2	religionistika
Filosofické	filosofický	k2eAgFnSc2d1	filosofická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
religionistiku	religionistika	k1gFnSc4	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
pracoviště	pracoviště	k1gNnSc4	pracoviště
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
Husitské	husitský	k2eAgFnSc3d1	husitská
teologické	teologický	k2eAgFnSc3d1	teologická
fakultě	fakulta	k1gFnSc3	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Pardubické	pardubický	k2eAgFnSc2d1	pardubická
Univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
předmět	předmět	k1gInSc1	předmět
se	se	k3xPyFc4	se
religionistika	religionistika	k1gFnSc1	religionistika
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
například	například	k6eAd1	například
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
či	či	k8xC	či
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Religionistika	religionistika	k1gFnSc1	religionistika
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
vyučována	vyučován	k2eAgFnSc1d1	vyučována
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Bratislavské	bratislavský	k2eAgFnSc2d1	Bratislavská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
jako	jako	k8xS	jako
předmět	předmět	k1gInSc1	předmět
společného	společný	k2eAgInSc2d1	společný
základu	základ	k1gInSc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
obor	obor	k1gInSc4	obor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
"	"	kIx"	"
<g/>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
náboženství	náboženství	k1gNnSc2	náboženství
při	při	k7c6	při
SAV	SAV	kA	SAV
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
spoločnosť	spoločnostit	k5eAaPmRp2nS	spoločnostit
pre	pre	k?	pre
štúdium	štúdium	k1gNnSc4	štúdium
náboženstiev	náboženstiev	k1gFnSc1	náboženstiev
pri	pri	k?	pri
SAV	SAV	kA	SAV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c4	na
FF	ff	kA	ff
UK	UK	kA	UK
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
samostatná	samostatný	k2eAgFnSc1d1	samostatná
katedra	katedra	k1gFnSc1	katedra
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
religionistiky	religionistika	k1gFnSc2	religionistika
(	(	kIx(	(
<g/>
Katedra	katedra	k1gFnSc1	katedra
porovnávacej	porovnávacat	k5eAaPmRp2nS	porovnávacat
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
religionistika	religionistika	k1gFnSc1	religionistika
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
badateli	badatel	k1gMnPc7	badatel
jako	jako	k8xC	jako
Ján	Ján	k1gMnSc1	Ján
Komorovský	Komorovský	k2eAgMnSc1d1	Komorovský
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Kováč	Kováč	k1gMnSc1	Kováč
či	či	k8xC	či
Attila	Attila	k1gMnSc1	Attila
Kovács	Kovácsa	k1gFnPc2	Kovácsa
<g/>
.	.	kIx.	.
</s>
<s>
Teologie	teologie	k1gFnSc1	teologie
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
nauka	nauka	k1gFnSc1	nauka
(	(	kIx(	(
<g/>
řeč	řeč	k1gFnSc1	řeč
<g/>
)	)	kIx)	)
o	o	k7c6	o
Bohu	bůh	k1gMnSc6	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastním	vlastní	k2eAgInSc7d1	vlastní
předmětem	předmět	k1gInSc7	předmět
teologie	teologie	k1gFnSc2	teologie
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
religionistiky	religionistika	k1gFnSc2	religionistika
je	být	k5eAaImIp3nS	být
náboženství	náboženství	k1gNnSc3	náboženství
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
možných	možný	k2eAgFnPc6d1	možná
podobách	podoba	k1gFnPc6	podoba
a	a	k8xC	a
projevech	projev	k1gInPc6	projev
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
teologie	teologie	k1gFnSc1	teologie
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
existence	existence	k1gFnSc2	existence
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
religionistika	religionistika	k1gFnSc1	religionistika
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
vědecké	vědecký	k2eAgNnSc4d1	vědecké
zkoumání	zkoumání	k1gNnSc4	zkoumání
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
porovnávání	porovnávání	k1gNnSc4	porovnávání
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
a	a	k8xC	a
které	který	k3yQgNnSc1	který
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
"	"	kIx"	"
<g/>
pravdivosti	pravdivost	k1gFnSc6	pravdivost
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
nepravdivosti	nepravdivost	k1gFnPc1	nepravdivost
<g/>
"	"	kIx"	"
náboženství	náboženství	k1gNnPc1	náboženství
nepatří	patřit	k5eNaImIp3nP	patřit
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
přistupovat	přistupovat	k5eAaImF	přistupovat
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
bez	bez	k7c2	bez
předsudků	předsudek	k1gInPc2	předsudek
<g/>
.	.	kIx.	.
</s>
<s>
Religionista	Religionista	k1gMnSc1	Religionista
je	být	k5eAaImIp3nS	být
především	především	k9	především
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
náboženské	náboženský	k2eAgFnPc4d1	náboženská
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
především	především	k6eAd1	především
náboženský	náboženský	k2eAgMnSc1d1	náboženský
specialista	specialista	k1gMnSc1	specialista
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
religionistiky	religionistika	k1gFnSc2	religionistika
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
se	se	k3xPyFc4	se
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Overbeck	Overbeck	k1gMnSc1	Overbeck
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
de	de	k?	de
Lagarde	Lagard	k1gMnSc5	Lagard
<g/>
,	,	kIx,	,
představitelé	představitel	k1gMnPc1	představitel
tzv.	tzv.	kA	tzv.
náboženskohistorické	náboženskohistorický	k2eAgFnSc2d1	náboženskohistorický
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
religionsgeschichtliche	religionsgeschichtlichat	k5eAaPmIp3nS	religionsgeschichtlichat
Schule	Schule	k1gFnSc1	Schule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
teologie	teologie	k1gFnSc1	teologie
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nikdy	nikdy	k6eAd1	nikdy
nedostojí	dostát	k5eNaPmIp3nP	dostát
požadavku	požadavek	k1gInSc3	požadavek
na	na	k7c4	na
bezpředsudkovost	bezpředsudkovost	k1gFnSc4	bezpředsudkovost
<g/>
.	.	kIx.	.
</s>
<s>
Požadovali	požadovat	k5eAaImAgMnP	požadovat
zrušení	zrušení	k1gNnSc4	zrušení
teologických	teologický	k2eAgFnPc2d1	teologická
fakult	fakulta	k1gFnPc2	fakulta
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
místo	místo	k1gNnSc4	místo
měly	mít	k5eAaImAgFnP	mít
nastoupit	nastoupit	k5eAaPmF	nastoupit
religionistické	religionistický	k2eAgFnSc2d1	religionistická
katedry	katedra	k1gFnSc2	katedra
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
závislostí	závislost	k1gFnSc7	závislost
teologických	teologický	k2eAgFnPc2d1	teologická
fakult	fakulta	k1gFnPc2	fakulta
na	na	k7c6	na
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plynoucí	plynoucí	k2eAgFnPc4d1	plynoucí
předpojatostí	předpojatost	k1gFnSc7	předpojatost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
postoj	postoj	k1gInSc1	postoj
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
dobovým	dobový	k2eAgInSc7d1	dobový
filozofickým	filozofický	k2eAgInSc7d1	filozofický
pozitivizmem	pozitivizmus	k1gInSc7	pozitivizmus
<g/>
,	,	kIx,	,
protestantskou	protestantský	k2eAgFnSc7d1	protestantská
teologií	teologie	k1gFnSc7	teologie
a	a	k8xC	a
procesem	proces	k1gInSc7	proces
sekularizace	sekularizace	k1gFnSc2	sekularizace
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
významný	významný	k2eAgMnSc1d1	významný
představitel	představitel	k1gMnSc1	představitel
náboženskohistorické	náboženskohistorický	k2eAgFnSc2d1	náboženskohistorický
školy	škola	k1gFnSc2	škola
Ernst	Ernst	k1gMnSc1	Ernst
Troeltsch	Troeltsch	k1gMnSc1	Troeltsch
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
křesťanství	křesťanství	k1gNnSc4	křesťanství
sice	sice	k8xC	sice
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřekračuje	překračovat	k5eNaImIp3nS	překračovat
své	svůj	k3xOyFgFnPc4	svůj
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
založit	založit	k5eAaPmF	založit
teologii	teologie	k1gFnSc3	teologie
na	na	k7c6	na
širší	široký	k2eAgFnSc6d2	širší
a	a	k8xC	a
obecnější	obecní	k2eAgFnSc6d2	obecní
základně	základna	k1gFnSc6	základna
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
na	na	k7c6	na
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
dějinném	dějinný	k2eAgInSc6d1	dějinný
vývoji	vývoj	k1gInSc6	vývoj
<g/>
,	,	kIx,	,
a	a	k8xC	a
slučoval	slučovat	k5eAaImAgMnS	slučovat
tak	tak	k6eAd1	tak
prvky	prvek	k1gInPc4	prvek
teologie	teologie	k1gFnSc2	teologie
i	i	k8xC	i
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvojakost	dvojakost	k1gFnSc1	dvojakost
zaměření	zaměření	k1gNnSc2	zaměření
Troeltschovy	Troeltschův	k2eAgFnSc2d1	Troeltschův
školy	škola	k1gFnSc2	škola
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
kritiku	kritika	k1gFnSc4	kritika
z	z	k7c2	z
teologické	teologický	k2eAgFnSc2d1	teologická
i	i	k8xC	i
religionistické	religionistický	k2eAgFnSc2d1	religionistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
historickonáboženské	historickonáboženský	k2eAgFnSc2d1	historickonáboženský
školy	škola	k1gFnSc2	škola
dopomohly	dopomoct	k5eAaPmAgInP	dopomoct
k	k	k7c3	k
novým	nový	k2eAgFnPc3d1	nová
diskuzím	diskuze	k1gFnPc3	diskuze
mezi	mezi	k7c4	mezi
religionisty	religionista	k1gMnPc4	religionista
a	a	k8xC	a
teology	teolog	k1gMnPc4	teolog
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
byly	být	k5eAaImAgInP	být
prospěšné	prospěšný	k2eAgInPc1d1	prospěšný
oběma	dva	k4xCgFnPc3	dva
stranám	strana	k1gFnPc3	strana
a	a	k8xC	a
trvaly	trvat	k5eAaImAgInP	trvat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
Karl	Karl	k1gMnSc1	Karl
Barth	Barth	k1gMnSc1	Barth
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
koncepce	koncepce	k1gFnSc2	koncepce
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc4	všechen
náboženství	náboženství	k1gNnSc2	náboženství
lidským	lidský	k2eAgInSc7d1	lidský
výtvorem	výtvor	k1gInSc7	výtvor
<g/>
,	,	kIx,	,
jen	jen	k9	jen
křesťanství	křesťanství	k1gNnSc1	křesťanství
je	být	k5eAaImIp3nS	být
Božím	boží	k2eAgNnSc7d1	boží
Zjevením	zjevení	k1gNnSc7	zjevení
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
jako	jako	k9	jako
náboženství	náboženství	k1gNnSc1	náboženství
zkoumáno	zkoumán	k2eAgNnSc1d1	zkoumáno
<g/>
.	.	kIx.	.
</s>
<s>
Religionistika	religionistika	k1gFnSc1	religionistika
se	se	k3xPyFc4	se
nemá	mít	k5eNaImIp3nS	mít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
k	k	k7c3	k
předmětu	předmět	k1gInSc3	předmět
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
Zjevení	zjevení	k1gNnSc1	zjevení
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
snahy	snaha	k1gFnSc2	snaha
o	o	k7c6	o
konstituování	konstituování	k1gNnSc6	konstituování
teologie	teologie	k1gFnSc2	teologie
na	na	k7c6	na
religionistických	religionistický	k2eAgInPc6d1	religionistický
základech	základ	k1gInPc6	základ
vzbuzují	vzbuzovat	k5eAaImIp3nP	vzbuzovat
jen	jen	k9	jen
"	"	kIx"	"
<g/>
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
hněv	hněv	k1gInSc4	hněv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
barthovského	barthovský	k2eAgInSc2d1	barthovský
vlivu	vliv	k1gInSc2	vliv
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
nové	nový	k2eAgInPc1d1	nový
teologické	teologický	k2eAgInPc1d1	teologický
postoje	postoj	k1gInPc1	postoj
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
nakloněné	nakloněný	k2eAgNnSc1d1	nakloněné
mezináboženskému	mezináboženský	k2eAgInSc3d1	mezináboženský
dialogu	dialog	k1gInSc3	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
teologických	teologický	k2eAgInPc2d1	teologický
postojů	postoj	k1gInPc2	postoj
byl	být	k5eAaImAgInS	být
Druhý	druhý	k4xOgInSc1	druhý
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
koncil	koncil	k1gInSc1	koncil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
religionistice	religionistika	k1gFnSc6	religionistika
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
religionistika	religionistika	k1gFnSc1	religionistika
a	a	k8xC	a
teologie	teologie	k1gFnPc1	teologie
nejsou	být	k5eNaImIp3nP	být
partnery	partner	k1gMnPc4	partner
ani	ani	k8xC	ani
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dvěma	dva	k4xCgInPc7	dva
samostatnými	samostatný	k2eAgInPc7d1	samostatný
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
disponují	disponovat	k5eAaBmIp3nP	disponovat
navzájem	navzájem	k6eAd1	navzájem
nezaměnitelnými	zaměnitelný	k2eNgFnPc7d1	nezaměnitelná
racionalitami	racionalita	k1gFnPc7	racionalita
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
religio	religio	k6eAd1	religio
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc1d1	široký
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
antice	antika	k1gFnSc6	antika
byl	být	k5eAaImAgInS	být
sporný	sporný	k2eAgInSc1d1	sporný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Břetislava	Břetislav	k1gMnSc2	Břetislav
Horyny	Horyna	k1gMnSc2	Horyna
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
péči	péče	k1gFnSc4	péče
o	o	k7c6	o
kultické	kultický	k2eAgFnSc6d1	kultická
záležitosti	záležitost	k1gFnSc6	záležitost
<g/>
,	,	kIx,	,
plnění	plnění	k1gNnSc6	plnění
náboženských	náboženský	k2eAgInPc2d1	náboženský
příkazů	příkaz	k1gInPc2	příkaz
<g/>
,	,	kIx,	,
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
svědomitost	svědomitost	k1gFnSc4	svědomitost
<g/>
,	,	kIx,	,
starostlivost	starostlivost	k1gFnSc4	starostlivost
<g/>
,	,	kIx,	,
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc4	úzkost
až	až	k8xS	až
pověrečnou	pověrečný	k2eAgFnSc4d1	pověrečná
úzkost	úzkost	k1gFnSc4	úzkost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
zbožnost	zbožnost	k1gFnSc4	zbožnost
jako	jako	k8xS	jako
postoj	postoj	k1gInSc4	postoj
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
božskému	božský	k2eAgMnSc3d1	božský
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Etymologie	etymologie	k1gFnSc1	etymologie
slova	slovo	k1gNnSc2	slovo
religio	religio	k6eAd1	religio
také	také	k9	také
není	být	k5eNaImIp3nS	být
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
.	.	kIx.	.
</s>
<s>
Cicero	Cicero	k1gMnSc1	Cicero
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
De	De	k?	De
natura	natura	k1gFnSc1	natura
deorum	deorum	k1gInSc1	deorum
religio	religio	k6eAd1	religio
odvozoval	odvozovat	k5eAaImAgInS	odvozovat
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
relegere	relegrat	k5eAaPmIp3nS	relegrat
(	(	kIx(	(
<g/>
sebrat	sebrat	k5eAaPmF	sebrat
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
sebrat	sebrat	k5eAaPmF	sebrat
<g/>
,	,	kIx,	,
brát	brát	k5eAaImF	brát
na	na	k7c4	na
něco	něco	k3yInSc4	něco
ohled	ohled	k1gInSc1	ohled
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lactantius	Lactantius	k1gInSc1	Lactantius
vyšel	vyjít	k5eAaPmAgInS	vyjít
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
religare	religar	k1gMnSc5	religar
(	(	kIx(	(
<g/>
svazovat	svazovat	k5eAaImF	svazovat
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
svazovat	svazovat	k5eAaImF	svazovat
<g/>
,	,	kIx,	,
spojit	spojit	k5eAaPmF	spojit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
vztah	vztah	k1gInSc4	vztah
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
panují	panovat	k5eAaImIp3nP	panovat
odlišné	odlišný	k2eAgInPc1d1	odlišný
názory	názor	k1gInPc1	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
vůbec	vůbec	k9	vůbec
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
jmenovat	jmenovat	k5eAaBmF	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
terminologii	terminologie	k1gFnSc6	terminologie
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Religionswissenschaft	Religionswissenschaft	k1gInSc1	Religionswissenschaft
a	a	k8xC	a
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Sciences	Sciences	k1gInSc4	Sciences
de	de	k?	de
la	la	k1gNnSc2	la
religion	religion	k1gInSc1	religion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
mluvilo	mluvit	k5eAaImAgNnS	mluvit
o	o	k7c6	o
srovnávací	srovnávací	k2eAgFnSc6d1	srovnávací
vědě	věda	k1gFnSc6	věda
náboženské	náboženský	k2eAgFnSc6d1	náboženská
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
název	název	k1gInSc1	název
religionistika	religionistika	k1gFnSc1	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
názvy	název	k1gInPc1	název
science	scienec	k1gInSc2	scienec
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
of	of	k?	of
religion	religion	k1gInSc1	religion
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
comparative	comparativ	k1gInSc5	comparativ
(	(	kIx(	(
<g/>
study	stud	k1gInPc4	stud
of	of	k?	of
<g/>
)	)	kIx)	)
religion	religion	k1gInSc1	religion
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
history	histor	k1gInPc1	histor
(	(	kIx(	(
<g/>
study	stud	k1gInPc1	stud
<g/>
)	)	kIx)	)
of	of	k?	of
religion	religion	k1gInSc1	religion
či	či	k8xC	či
religious	religious	k1gMnSc1	religious
studies	studies	k1gMnSc1	studies
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
přitom	přitom	k6eAd1	přitom
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
religions	religions	k1gInSc1	religions
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
myšleno	myslet	k5eAaImNgNnS	myslet
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
nelze	lze	k6eNd1	lze
uplatnit	uplatnit	k5eAaPmF	uplatnit
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Müller	Müller	k1gMnSc1	Müller
použil	použít	k5eAaPmAgMnS	použít
názvu	název	k1gInSc3	název
science	scienec	k1gInSc2	scienec
of	of	k?	of
religion	religion	k1gInSc1	religion
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
chtěl	chtít	k5eAaImAgMnS	chtít
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
vědeckost	vědeckost	k1gFnSc4	vědeckost
nového	nový	k2eAgInSc2d1	nový
oboru	obor	k1gInSc2	obor
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
neujal	ujmout	k5eNaPmAgMnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
rozdíly	rozdíl	k1gInPc1	rozdíl
amerického	americký	k2eAgNnSc2d1	americké
a	a	k8xC	a
britského	britský	k2eAgNnSc2d1	Britské
chápání	chápání	k1gNnSc2	chápání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
pojmů	pojem	k1gInPc2	pojem
v	v	k7c6	v
názvu	název	k1gInSc2	název
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
postojů	postoj	k1gInPc2	postoj
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc1	jaký
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
religionistice	religionistika	k1gFnSc3	religionistika
zaujímány	zaujímán	k2eAgFnPc1d1	zaujímán
<g/>
.	.	kIx.	.
</s>
<s>
BUBÍK	BUBÍK	kA	BUBÍK
<g/>
,	,	kIx,	,
TOMÁŠ	Tomáš	k1gMnSc1	Tomáš
<g/>
;	;	kIx,	;
HOFFMANN	Hoffmann	k1gMnSc1	Hoffmann
<g/>
,	,	kIx,	,
HENRYK	HENRYK	kA	HENRYK
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Studying	Studying	k1gInSc1	Studying
Religions	Religions	k1gInSc1	Religions
with	with	k1gInSc1	with
the	the	k?	the
Iron	iron	k1gInSc1	iron
Curtain	Curtain	k1gMnSc1	Curtain
Closed	Closed	k1gMnSc1	Closed
and	and	k?	and
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Academic	Academic	k1gMnSc1	Academic
Study	stud	k1gInPc1	stud
of	of	k?	of
Religion	religion	k1gInSc1	religion
in	in	k?	in
Eastern	Eastern	k1gInSc1	Eastern
Europe	Europ	k1gInSc5	Europ
<g/>
,	,	kIx,	,
Brill	Brill	k1gInSc1	Brill
Publishing	Publishing	k1gInSc4	Publishing
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Leiden	Leidno	k1gNnPc2	Leidno
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-9004283077	[number]	k4	978-9004283077
BUBÍK	BUBÍK	kA	BUBÍK
<g/>
,	,	kIx,	,
TOMÁŠ	Tomáš	k1gMnSc1	Tomáš
<g/>
:	:	kIx,	:
České	český	k2eAgNnSc1d1	české
bádání	bádání	k1gNnSc1	bádání
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87378	[number]	k4	87378
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
HELLER	HELLER	kA	HELLER
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
MRÁZEK	Mrázek	k1gMnSc1	Mrázek
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Nástin	nástin	k1gInSc1	nástin
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Kalich	kalich	k1gInSc1	kalich
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7306	[number]	k4	7306
<g/>
-	-	kIx~	-
<g/>
132	[number]	k4	132
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
HORYNA	Horyna	k1gMnSc1	Horyna
<g/>
,	,	kIx,	,
Břetislav	Břetislav	k1gMnSc1	Břetislav
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
OIKÚMENÉ	OIKÚMENÉ	kA	OIKÚMENÉ
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85241	[number]	k4	85241
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
HORYNA	Horyna	k1gMnSc1	Horyna
<g/>
,	,	kIx,	,
Břetislav	Břetislav	k1gMnSc1	Břetislav
<g/>
;	;	kIx,	;
PAVLINCOVÁ	PAVLINCOVÁ	kA	PAVLINCOVÁ
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
:	:	kIx,	:
antologie	antologie	k1gFnSc2	antologie
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7182	[number]	k4	7182
<g/>
-	-	kIx~	-
<g/>
123	[number]	k4	123
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
KOMOROVSKÝ	KOMOROVSKÝ	kA	KOMOROVSKÝ
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
<g/>
.	.	kIx.	.
</s>
<s>
Religionistika	religionistika	k1gFnSc1	religionistika
<g/>
:	:	kIx,	:
Věda	věda	k1gFnSc1	věda
o	o	k7c6	o
náboženstvách	náboženstva	k1gFnPc6	náboženstva
sveta	svet	k1gInSc2	svet
a	a	k8xC	a
jej	on	k3xPp3gMnSc4	on
pomocné	pomocný	k2eAgFnPc1d1	pomocná
disciplíny	disciplína	k1gFnPc1	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
223	[number]	k4	223
<g/>
-	-	kIx~	-
<g/>
1535	[number]	k4	1535
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
PADEN	PADEN	kA	PADEN
<g/>
,	,	kIx,	,
William	William	k1gInSc4	William
E.	E.	kA	E.
Bádání	bádání	k1gNnSc1	bádání
o	o	k7c6	o
posvátnu	posvátno	k1gNnSc6	posvátno
<g/>
:	:	kIx,	:
náboženství	náboženství	k1gNnSc1	náboženství
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
interpretací	interpretace	k1gFnPc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
2977	[number]	k4	2977
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
PALS	PALS	kA	PALS
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
L.	L.	kA	L.
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
teorií	teorie	k1gFnPc2	teorie
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
ExOriente	ExOrient	k1gMnSc5	ExOrient
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905211	[number]	k4	905211
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
SKALICKÝ	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
neznámého	známý	k2eNgMnSc2d1	neznámý
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Svitavy	Svitava	k1gFnPc1	Svitava
:	:	kIx,	:
Trinitas	Trinitas	k1gInSc1	Trinitas
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86036	[number]	k4	86036
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
WAARDENBURG	WAARDENBURG	kA	WAARDENBURG
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
<g/>
.	.	kIx.	.
</s>
<s>
Bohové	bůh	k1gMnPc1	bůh
zblízka	zblízka	k6eAd1	zblízka
<g/>
:	:	kIx,	:
Systematický	systematický	k2eAgInSc1d1	systematický
úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902197	[number]	k4	902197
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
religionistika	religionistika	k1gFnSc1	religionistika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
religionistika	religionistika	k1gFnSc1	religionistika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
kognitivní	kognitivní	k2eAgNnSc4d1	kognitivní
studium	studium	k1gNnSc4	studium
kultury	kultura	k1gFnSc2	kultura
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
religionistiku	religionistika	k1gFnSc4	religionistika
Sekce	sekce	k1gFnSc2	sekce
sociologie	sociologie	k1gFnSc2	sociologie
náboženství	náboženství	k1gNnPc2	náboženství
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
české	český	k2eAgFnSc2d1	Česká
sociologické	sociologický	k2eAgFnSc2d1	sociologická
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
AAR	AAR	kA	AAR
American	Americany	k1gInPc2	Americany
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CSSR	CSSR	kA	CSSR
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Council	Council	k1gMnSc1	Council
of	of	k?	of
Societies	Societies	k1gMnSc1	Societies
for	forum	k1gNnPc2	forum
the	the	k?	the
Study	stud	k1gInPc1	stud
of	of	k?	of
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
EASR	EASR	kA	EASR
European	European	k1gInSc1	European
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
the	the	k?	the
Study	stud	k1gInPc1	stud
of	of	k?	of
Religions	Religions	k1gInSc1	Religions
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
IAHR	IAHR	kA	IAHR
International	International	k1gFnSc1	International
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
the	the	k?	the
History	Histor	k1gInPc1	Histor
of	of	k?	of
Religions	Religions	k1gInSc1	Religions
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
NAASR	NAASR	kA	NAASR
North	North	k1gMnSc1	North
American	American	k1gMnSc1	American
Association	Association	k1gInSc4	Association
for	forum	k1gNnPc2	forum
the	the	k?	the
Study	stud	k1gInPc1	stud
of	of	k?	of
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SSSR	SSSR	kA	SSSR
Society	societa	k1gFnSc2	societa
for	forum	k1gNnPc2	forum
the	the	k?	the
Scientific	Scientific	k1gMnSc1	Scientific
Study	stud	k1gInPc1	stud
of	of	k?	of
Religion	religion	k1gInSc1	religion
Katedra	katedra	k1gFnSc1	katedra
religionistiky	religionistika	k1gFnSc2	religionistika
Evangelické	evangelický	k2eAgFnSc2d1	evangelická
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
UK	UK	kA	UK
Katedra	katedra	k1gFnSc1	katedra
religionistiky	religionistika	k1gFnSc2	religionistika
Husitské	husitský	k2eAgFnSc2d1	husitská
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
UK	UK	kA	UK
Katedra	katedra	k1gFnSc1	katedra
religionistiky	religionistika	k1gFnSc2	religionistika
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
UP	UP	kA	UP
Ústav	ústav	k1gInSc1	ústav
filosofie	filosofie	k1gFnSc1	filosofie
a	a	k8xC	a
religionistiky	religionistika	k1gFnPc1	religionistika
Filosofické	filosofický	k2eAgFnSc2d1	filosofická
fakulty	fakulta	k1gFnSc2	fakulta
UK	UK	kA	UK
Ústav	ústav	k1gInSc4	ústav
religionistiky	religionistika	k1gFnSc2	religionistika
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
MU	MU	kA	MU
Pantheon	Pantheon	k1gInSc1	Pantheon
<g/>
,	,	kIx,	,
religionistický	religionistický	k2eAgInSc1d1	religionistický
odborný	odborný	k2eAgInSc1d1	odborný
časopis	časopis	k1gInSc1	časopis
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
Univerzitou	univerzita	k1gFnSc7	univerzita
Pardubice	Pardubice	k1gInPc1	Pardubice
<g />
.	.	kIx.	.
</s>
<s>
Popularizační	popularizační	k2eAgInSc1d1	popularizační
časopis	časopis	k1gInSc1	časopis
Dingir	Dingir	k1gMnSc1	Dingir
Religio	Religio	k1gMnSc1	Religio
<g/>
:	:	kIx,	:
Revue	revue	k1gFnSc1	revue
pro	pro	k7c4	pro
religionistiku	religionistika	k1gFnSc4	religionistika
<g/>
,	,	kIx,	,
odborný	odborný	k2eAgInSc1d1	odborný
časopis	časopis	k1gInSc1	časopis
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
Českou	český	k2eAgFnSc7d1	Česká
společností	společnost	k1gFnSc7	společnost
pro	pro	k7c4	pro
religionistiku	religionistika	k1gFnSc4	religionistika
Sacra	Sacr	k1gInSc2	Sacr
<g/>
,	,	kIx,	,
studentský	studentský	k2eAgInSc1d1	studentský
religionistický	religionistický	k2eAgInSc1d1	religionistický
odborný	odborný	k2eAgInSc1d1	odborný
časopis	časopis	k1gInSc1	časopis
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
studenty	student	k1gMnPc7	student
ÚR	Úro	k1gNnPc2	Úro
FF	ff	kA	ff
MU	MU	kA	MU
David	David	k1gMnSc1	David
Zbíral	Zbíral	k1gMnSc1	Zbíral
<g/>
:	:	kIx,	:
religionistika	religionistika	k1gFnSc1	religionistika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Studying	Studying	k1gInSc1	Studying
Religion	religion	k1gInSc1	religion
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Introduction	Introduction	k1gInSc1	Introduction
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
religionistiku	religionistika	k1gFnSc4	religionistika
a	a	k8xC	a
multikulturní	multikulturní	k2eAgFnSc4d1	multikulturní
edukaci	edukace	k1gFnSc4	edukace
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
sekt	sekta	k1gFnPc2	sekta
a	a	k8xC	a
nových	nový	k2eAgInPc2d1	nový
náboženských	náboženský	k2eAgInPc2d1	náboženský
směrů	směr	k1gInPc2	směr
Religionistické	Religionistický	k2eAgNnSc1d1	Religionistické
poradenské	poradenský	k2eAgNnSc1d1	poradenské
centrum	centrum	k1gNnSc1	centrum
Večerní	večerní	k2eAgFnSc2d1	večerní
akademie	akademie	k1gFnSc2	akademie
religionistiky	religionistika	k1gFnSc2	religionistika
-	-	kIx~	-
přednášky	přednáška	k1gFnSc2	přednáška
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
Webový	webový	k2eAgInSc1d1	webový
rozcestník	rozcestník	k1gInSc1	rozcestník
Religionistika	religionistika	k1gFnSc1	religionistika
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
