<s>
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
WoW	WoW	k1gMnSc2	WoW
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fantasy	fantas	k1gInPc4	fantas
MMORPG	MMORPG	kA	MMORPG
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
společností	společnost	k1gFnSc7	společnost
Blizzard	Blizzard	k1gMnSc1	Blizzard
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
také	také	k9	také
předchozí	předchozí	k2eAgInPc4d1	předchozí
díly	díl	k1gInPc4	díl
série	série	k1gFnSc2	série
Warcraft	Warcrafta	k1gFnPc2	Warcrafta
<g/>
.	.	kIx.	.
</s>
<s>
World	World	k1gInSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
k	k	k7c3	k
desátému	desátý	k4xOgInSc3	desátý
výročí	výročí	k1gNnSc3	výročí
série	série	k1gFnSc1	série
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
původní	původní	k2eAgFnSc3d1	původní
hře	hra	k1gFnSc3	hra
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc1	šest
datadisků	datadisk	k1gInPc2	datadisk
<g/>
:	:	kIx,	:
The	The	k1gFnPc2	The
Burning	Burning	k1gInSc1	Burning
Crusade	Crusad	k1gInSc5	Crusad
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wrath	Wrath	k1gMnSc1	Wrath
of	of	k?	of
the	the	k?	the
Lich	Lich	k?	Lich
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cataclysm	Cataclysm	k1gMnSc1	Cataclysm
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mists	Mists	k1gInSc1	Mists
of	of	k?	of
Pandaria	Pandarium	k1gNnSc2	Pandarium
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Warlords	Warlords	k1gInSc1	Warlords
of	of	k?	of
Draenor	Draenor	k1gInSc1	Draenor
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
Legion	legion	k1gInSc1	legion
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
drží	držet	k5eAaImIp3nS	držet
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
Guinessově	Guinessův	k2eAgFnSc6d1	Guinessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
za	za	k7c4	za
nejpopulárnější	populární	k2eAgFnSc4d3	nejpopulárnější
MMORPG	MMORPG	kA	MMORPG
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
hru	hra	k1gFnSc4	hra
hrálo	hrát	k5eAaImAgNnS	hrát
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
datadisku	datadisek	k1gInSc2	datadisek
Cataclysm	Cataclysma	k1gFnPc2	Cataclysma
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
přes	přes	k7c4	přes
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
nejhranější	hraný	k2eAgFnSc1d3	nejhranější
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
měsíčně	měsíčně	k6eAd1	měsíčně
platících	platící	k2eAgMnPc2d1	platící
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
i	i	k8xC	i
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2008	[number]	k4	2008
a	a	k8xC	a
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
servery	server	k1gInPc1	server
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
<g/>
,	,	kIx,	,
evropské	evropský	k2eAgFnSc6d1	Evropská
<g/>
,	,	kIx,	,
čínské	čínský	k2eAgFnSc6d1	čínská
<g/>
,	,	kIx,	,
taiwanské	taiwanský	k2eAgFnSc6d1	taiwanská
a	a	k8xC	a
jihoasijské	jihoasijský	k2eAgFnSc6d1	jihoasijská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
serverů	server	k1gInPc2	server
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
realmů	realm	k1gInPc2	realm
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgFnPc1d1	Evropská
realmy	realma	k1gFnPc1	realma
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnSc6d1	německá
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnSc6d1	francouzská
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgFnSc6d1	ruská
a	a	k8xC	a
španělské	španělský	k2eAgFnSc6d1	španělská
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zakládání	zakládání	k1gNnSc6	zakládání
charakteru	charakter	k1gInSc2	charakter
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
vybrat	vybrat	k5eAaPmF	vybrat
<g/>
,	,	kIx,	,
na	na	k7c6	na
jakém	jaký	k3yIgInSc6	jaký
realmu	realm	k1gInSc6	realm
chcete	chtít	k5eAaImIp2nP	chtít
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc4d1	oficiální
servery	server	k1gInPc4	server
sice	sice	k8xC	sice
nemají	mít	k5eNaImIp3nP	mít
českou	český	k2eAgFnSc4d1	Česká
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
realmy	realma	k1gFnPc1	realma
s	s	k7c7	s
početnou	početný	k2eAgFnSc7d1	početná
českou	český	k2eAgFnSc7d1	Česká
a	a	k8xC	a
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
komunitou	komunita	k1gFnSc7	komunita
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Drak	drak	k1gMnSc1	drak
<g/>
'	'	kIx"	'
<g/>
Thul	Thul	k1gMnSc1	Thul
<g/>
,	,	kIx,	,
Thunderhorn	Thunderhorn	k1gMnSc1	Thunderhorn
<g/>
,	,	kIx,	,
Burning	Burning	k1gInSc1	Burning
Blade	Blad	k1gInSc5	Blad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hraní	hraní	k1gNnSc4	hraní
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
předplacených	předplacený	k2eAgFnPc2d1	předplacená
karet	kareta	k1gFnPc2	kareta
nebo	nebo	k8xC	nebo
pomocí	pomoc	k1gFnPc2	pomoc
internetové	internetový	k2eAgFnSc2d1	internetová
platební	platební	k2eAgFnSc2d1	platební
karty	karta	k1gFnSc2	karta
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
hraní	hraní	k1gNnSc2	hraní
stojí	stát	k5eAaImIp3nS	stát
250	[number]	k4	250
<g/>
-	-	kIx~	-
<g/>
400	[number]	k4	400
Kč	Kč	kA	Kč
dle	dle	k7c2	dle
typu	typ	k1gInSc2	typ
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
platby	platba	k1gFnSc2	platba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
založit	založit	k5eAaPmF	založit
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
Starter	Startra	k1gFnPc2	Startra
Edition	Edition	k1gInSc1	Edition
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
omezenou	omezený	k2eAgFnSc7d1	omezená
verzí	verze	k1gFnSc7	verze
kompletní	kompletní	k2eAgFnSc2d1	kompletní
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Jistou	jistý	k2eAgFnSc7d1	jistá
alternativou	alternativa	k1gFnSc7	alternativa
je	být	k5eAaImIp3nS	být
hraní	hraní	k1gNnSc4	hraní
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
free	freat	k5eAaPmIp3nS	freat
serverech	server	k1gInPc6	server
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
servery	server	k1gInPc1	server
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgInP	provozovat
samotnými	samotný	k2eAgMnPc7d1	samotný
hráči	hráč	k1gMnPc7	hráč
nebo	nebo	k8xC	nebo
skupinou	skupina	k1gFnSc7	skupina
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
spřízněni	spřízněn	k2eAgMnPc1d1	spřízněn
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Blizzard	Blizzarda	k1gFnPc2	Blizzarda
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
hraní	hranit	k5eAaImIp3nS	hranit
na	na	k7c6	na
takových	takový	k3xDgInPc6	takový
serverech	server	k1gInPc6	server
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
rizikové	rizikový	k2eAgFnSc2d1	riziková
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
free	free	k6eAd1	free
serverech	server	k1gInPc6	server
sice	sice	k8xC	sice
nejsou	být	k5eNaImIp3nP	být
vyžadovány	vyžadovat	k5eAaImNgInP	vyžadovat
žádné	žádný	k3yNgInPc1	žádný
měsíční	měsíční	k2eAgInPc1d1	měsíční
poplatky	poplatek	k1gInPc1	poplatek
<g/>
,	,	kIx,	,
daní	danit	k5eAaImIp3nS	danit
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
velká	velký	k2eAgFnSc1d1	velká
míra	míra	k1gFnSc1	míra
výskytu	výskyt	k1gInSc2	výskyt
chyb	chyba	k1gFnPc2	chyba
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
bugů	bug	k1gInPc2	bug
-	-	kIx~	-
které	který	k3yIgInPc1	který
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
ze	z	k7c2	z
samotné	samotný	k2eAgFnSc2d1	samotná
podstaty	podstata	k1gFnSc2	podstata
free	fre	k1gFnSc2	fre
serverů	server	k1gInPc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
provozovány	provozovat	k5eAaImNgInP	provozovat
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
emulátorech	emulátor	k1gInPc6	emulátor
-	-	kIx~	-
komunitně	komunitně	k6eAd1	komunitně
vyvíjeném	vyvíjený	k2eAgInSc6d1	vyvíjený
softwaru	software	k1gInSc6	software
snažícím	snažící	k2eAgInSc6d1	snažící
se	se	k3xPyFc4	se
funkčností	funkčnost	k1gFnSc7	funkčnost
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
přiblížit	přiblížit	k5eAaPmF	přiblížit
oficiálním	oficiální	k2eAgInPc3d1	oficiální
serverům	server	k1gInPc3	server
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
nemožné	možný	k2eNgNnSc1d1	nemožné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Legálnost	legálnost	k1gFnSc1	legálnost
free	free	k1gNnSc2	free
serverů	server	k1gInPc2	server
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
diskutabilní	diskutabilní	k2eAgFnSc2d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
samotnou	samotný	k2eAgFnSc7d1	samotná
editací	editace	k1gFnSc7	editace
souboru	soubor	k1gInSc2	soubor
realmlist	realmlist	k1gInSc1	realmlist
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
klienta	klient	k1gMnSc2	klient
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
free	free	k1gFnSc4	free
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
porušována	porušovat	k5eAaImNgFnS	porušovat
EULA	EULA	kA	EULA
Blizzardu	Blizzard	k1gInSc6	Blizzard
<g/>
.	.	kIx.	.
</s>
<s>
WoW	WoW	k?	WoW
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
všech	všecek	k3xTgMnPc2	všecek
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
hra	hra	k1gFnSc1	hra
typu	typ	k1gInSc2	typ
RTS	RTS	kA	RTS
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
žánr	žánr	k1gInSc4	žánr
MMORPG	MMORPG	kA	MMORPG
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
vaše	váš	k3xOp2gFnSc1	váš
postava	postava	k1gFnSc1	postava
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozlehlém	rozlehlý	k2eAgInSc6d1	rozlehlý
virtuálním	virtuální	k2eAgInSc6d1	virtuální
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
baví	bavit	k5eAaImIp3nS	bavit
se	se	k3xPyFc4	se
a	a	k8xC	a
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
hráči	hráč	k1gMnPc7	hráč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
NPC	NPC	kA	NPC
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
k	k	k7c3	k
zabíjení	zabíjení	k1gNnSc3	zabíjení
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
zadávají	zadávat	k5eAaImIp3nP	zadávat
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
questy	questa	k1gFnPc1	questa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
hráč	hráč	k1gMnSc1	hráč
postupně	postupně	k6eAd1	postupně
prováděn	provádět	k5eAaImNgMnS	provádět
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
ras	rasa	k1gFnPc2	rasa
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
ve	v	k7c6	v
Warcraftu	Warcrafto	k1gNnSc6	Warcrafto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
WoW	WoW	k1gFnSc6	WoW
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
13	[number]	k4	13
hratelných	hratelný	k2eAgFnPc2d1	hratelná
ras	rasa	k1gFnPc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
rozlišeny	rozlišit	k5eAaPmNgFnP	rozlišit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
podle	podle	k7c2	podle
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
bojují	bojovat	k5eAaImIp3nP	bojovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
Alianci	aliance	k1gFnSc6	aliance
(	(	kIx(	(
<g/>
Alliance	Allianka	k1gFnSc6	Allianka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hordu	horda	k1gFnSc4	horda
(	(	kIx(	(
<g/>
Horde	Hord	k1gMnSc5	Hord
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rasy	rasa	k1gFnSc2	rasa
Aliance	aliance	k1gFnPc1	aliance
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
celkem	celkem	k6eAd1	celkem
podobné	podobný	k2eAgFnPc1d1	podobná
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
ideologický	ideologický	k2eAgInSc4d1	ideologický
základ	základ	k1gInSc4	základ
-	-	kIx~	-
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
dobro	dobro	k1gNnSc4	dobro
a	a	k8xC	a
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
odmítajíce	odmítat	k5eAaImSgMnP	odmítat
temnotu	temnota	k1gFnSc4	temnota
<g/>
.	.	kIx.	.
</s>
<s>
Frakce	frakce	k1gFnSc1	frakce
Horda	horda	k1gFnSc1	horda
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
ras	rasa	k1gFnPc2	rasa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
hledají	hledat	k5eAaImIp3nP	hledat
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
a	a	k8xC	a
ctí	ctít	k5eAaImIp3nS	ctít
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
frakce	frakce	k1gFnPc1	frakce
nedají	dát	k5eNaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dobro	dobro	k1gNnSc4	dobro
a	a	k8xC	a
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
od	od	k7c2	od
každého	každý	k3xTgInSc2	každý
trochu	trocha	k1gFnSc4	trocha
<g/>
.	.	kIx.	.
</s>
<s>
Rasa	rasa	k1gFnSc1	rasa
Pandaren	Pandarna	k1gFnPc2	Pandarna
je	být	k5eAaImIp3nS	být
nejdříve	dříve	k6eAd3	dříve
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k8xC	jak
dokončíte	dokončit	k5eAaPmIp2nP	dokončit
poslední	poslední	k2eAgInSc4d1	poslední
quest	quest	k1gInSc4	quest
tak	tak	k6eAd1	tak
jsi	být	k5eAaImIp2nS	být
zvolíte	zvolit	k5eAaPmIp2nP	zvolit
jestli	jestli	k8xS	jestli
chcete	chtít	k5eAaImIp2nP	chtít
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
Hordu	horda	k1gFnSc4	horda
nebo	nebo	k8xC	nebo
Alianci	aliance	k1gFnSc4	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rasy	rasa	k1gFnSc2	rasa
si	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
zvolí	zvolit	k5eAaPmIp3nS	zvolit
také	také	k9	také
povolání	povolání	k1gNnPc4	povolání
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgNnPc2	jenž
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
jedenáct	jedenáct	k4xCc1	jedenáct
<g/>
.	.	kIx.	.
</s>
<s>
Povolání	povolání	k1gNnSc1	povolání
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
ovlivňujícím	ovlivňující	k2eAgInSc7d1	ovlivňující
styl	styl	k1gInSc4	styl
vašeho	váš	k3xOp2gNnSc2	váš
hraní	hraní	k1gNnSc2	hraní
a	a	k8xC	a
roli	role	k1gFnSc4	role
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
více	hodně	k6eAd2	hodně
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgNnPc4d1	různé
povolání	povolání	k1gNnPc4	povolání
mají	mít	k5eAaImIp3nP	mít
svá	svůj	k3xOyFgNnPc4	svůj
jedinečná	jedinečný	k2eAgNnPc4d1	jedinečné
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
různé	různý	k2eAgFnPc4d1	různá
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
nosit	nosit	k5eAaImF	nosit
různou	různý	k2eAgFnSc4d1	různá
zbroj	zbroj	k1gFnSc4	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
Tank	tank	k1gInSc1	tank
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc4	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dostává	dostávat	k5eAaImIp3nS	dostávat
největší	veliký	k2eAgNnSc4d3	veliký
poškození	poškození	k1gNnSc4	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
udržet	udržet	k5eAaPmF	udržet
pozornost	pozornost	k1gFnSc4	pozornost
(	(	kIx(	(
<g/>
aggro	aggro	k6eAd1	aggro
<g/>
)	)	kIx)	)
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
neútočili	útočit	k5eNaImAgMnP	útočit
na	na	k7c4	na
ostatní	ostatní	k2eAgMnPc4d1	ostatní
členy	člen	k1gMnPc4	člen
party	parta	k1gFnSc2	parta
<g/>
,	,	kIx,	,
když	když	k8xS	když
umře	umřít	k5eAaPmIp3nS	umřít
většinou	většina	k1gFnSc7	většina
umřou	umřít	k5eAaPmIp3nP	umřít
všichni	všechen	k3xTgMnPc1	všechen
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
nezvládne	zvládnout	k5eNaPmIp3nS	zvládnout
Off-Tank	Off-Tank	k1gInSc1	Off-Tank
<g/>
.	.	kIx.	.
</s>
<s>
Melee	Melee	k1gFnSc1	Melee
DPS	DPS	kA	DPS
(	(	kIx(	(
<g/>
damage	damagat	k5eAaPmIp3nS	damagat
dealer	dealer	k1gMnSc1	dealer
<g/>
)	)	kIx)	)
-	-	kIx~	-
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
nepřátele	nepřítel	k1gMnPc4	nepřítel
pomocí	pomocí	k7c2	pomocí
útoků	útok	k1gInPc2	útok
z	z	k7c2	z
blízka	blízko	k1gNnSc2	blízko
<g/>
.	.	kIx.	.
</s>
<s>
Ranged	Ranged	k1gMnSc1	Ranged
DPS	DPS	kA	DPS
-	-	kIx~	-
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
nepřátele	nepřítel	k1gMnPc4	nepřítel
pomocí	pomocí	k7c2	pomocí
útoků	útok	k1gInPc2	útok
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Healer	Healer	k1gInSc1	Healer
-	-	kIx~	-
léčí	léčit	k5eAaImIp3nS	léčit
členy	člen	k1gMnPc4	člen
party	parta	k1gFnSc2	parta
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
primárním	primární	k2eAgInSc7d1	primární
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
držet	držet	k5eAaImF	držet
při	při	k7c6	při
životě	život	k1gInSc6	život
tanka	tanek	k1gInSc2	tanek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
smrt	smrt	k1gFnSc1	smrt
obvykle	obvykle	k6eAd1	obvykle
znamená	znamenat	k5eAaImIp3nS	znamenat
smrt	smrt	k1gFnSc4	smrt
celé	celý	k2eAgFnSc2d1	celá
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Off-Tank	Off-Tank	k1gInSc1	Off-Tank
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
záložní	záložní	k2eAgInSc4d1	záložní
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
aggruje	aggrovat	k5eAaImIp3nS	aggrovat
addy	adda	k1gFnPc4	adda
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
povolání	povolání	k1gNnSc4	povolání
může	moct	k5eAaImIp3nS	moct
zastávat	zastávat	k5eAaImF	zastávat
danou	daný	k2eAgFnSc4d1	daná
roli	role	k1gFnSc4	role
v	v	k7c6	v
partě	parta	k1gFnSc6	parta
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
rasa	rasa	k1gFnSc1	rasa
nemůže	moct	k5eNaImIp3nS	moct
zastávat	zastávat	k5eAaImF	zastávat
každé	každý	k3xTgNnSc4	každý
povolání	povolání	k1gNnSc4	povolání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
možné	možný	k2eAgFnPc1d1	možná
kombinace	kombinace	k1gFnPc1	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
organizovanému	organizovaný	k2eAgMnSc3d1	organizovaný
PvE	PvE	k1gMnSc3	PvE
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
World	Worldo	k1gNnPc2	Worldo
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
určeny	určen	k2eAgFnPc4d1	určena
instance	instance	k1gFnPc4	instance
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
40	[number]	k4	40
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hromadným	hromadný	k2eAgFnPc3d1	hromadná
PvP	PvP	k1gFnPc3	PvP
bitvám	bitva	k1gFnPc3	bitva
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
WoW	WoW	k1gFnSc6	WoW
určeny	určit	k5eAaPmNgInP	určit
tzv.	tzv.	kA	tzv.
battlegroundy	battleground	k1gInPc1	battleground
(	(	kIx(	(
<g/>
bojiště	bojiště	k1gNnSc1	bojiště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
exteriéry	exteriér	k1gInPc1	exteriér
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
outdoor	outdoor	k1gMnSc1	outdoor
PvP	PvP	k1gMnSc1	PvP
<g/>
)	)	kIx)	)
a	a	k8xC	a
arény	aréna	k1gFnSc2	aréna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgInPc6	ten
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
bojují	bojovat	k5eAaImIp3nP	bojovat
vždy	vždy	k6eAd1	vždy
dva	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
<g/>
,	,	kIx,	,
tří	tři	k4xCgNnPc2	tři
nebo	nebo	k8xC	nebo
pěti	pět	k4xCc2	pět
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
WoW	WoW	k?	WoW
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zatím	zatím	k6eAd1	zatím
8	[number]	k4	8
battlegroundů	battleground	k1gMnPc2	battleground
a	a	k8xC	a
5	[number]	k4	5
arén	aréna	k1gFnPc2	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
WoW	WoW	k1gFnSc6	WoW
lokace	lokace	k1gFnPc4	lokace
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Wintergrasp	Wintergrasp	k1gInSc1	Wintergrasp
(	(	kIx(	(
<g/>
Určeno	určit	k5eAaPmNgNnS	určit
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
80	[number]	k4	80
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráči	hráč	k1gMnPc1	hráč
dobývají	dobývat	k5eAaImIp3nP	dobývat
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pevnost	pevnost	k1gFnSc1	pevnost
je	být	k5eAaImIp3nS	být
bráněna	bránit	k5eAaImNgFnS	bránit
opačnou	opačný	k2eAgFnSc7d1	opačná
frakcí	frakce	k1gFnSc7	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Wintergrasp	Wintergrasp	k1gInSc1	Wintergrasp
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
máte	mít	k5eAaImIp2nP	mít
dobytou	dobytý	k2eAgFnSc4d1	dobytá
tuto	tento	k3xDgFnSc4	tento
pevnost	pevnost	k1gFnSc4	pevnost
můžete	moct	k5eAaImIp2nP	moct
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
instance	instance	k1gFnSc2	instance
Vault	Vault	k1gInSc1	Vault
of	of	k?	of
Archavon	Archavon	k1gInSc1	Archavon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgMnPc1	čtyři
bossové	boss	k1gMnPc1	boss
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
získat	získat	k5eAaPmF	získat
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
zbroj	zbroj	k1gFnSc4	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
jako	jako	k8xC	jako
Wintergrasp	Wintergrasp	k1gMnSc1	Wintergrasp
je	být	k5eAaImIp3nS	být
také	také	k9	také
battleground	battleground	k1gInSc1	battleground
Tol	tol	k1gInSc1	tol
Barad	Barad	k1gInSc1	Barad
(	(	kIx(	(
<g/>
Určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
85	[number]	k4	85
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
dobytí	dobytí	k1gNnSc6	dobytí
máte	mít	k5eAaImIp2nP	mít
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
instance	instance	k1gFnSc2	instance
Baradin	Baradin	k2eAgInSc4d1	Baradin
Hold	hold	k1gInSc4	hold
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgMnPc1	tři
bossové	boss	k1gMnPc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
desátém	desátý	k4xOgInSc6	desátý
levelu	level	k1gInSc6	level
počínaje	počínaje	k7c7	počínaje
desátým	desátý	k4xOgNnSc7	desátý
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
hráč	hráč	k1gMnSc1	hráč
možnost	možnost	k1gFnSc4	možnost
zvolit	zvolit	k5eAaPmF	zvolit
si	se	k3xPyFc3	se
jeden	jeden	k4xCgInSc1	jeden
talent	talent	k1gInSc1	talent
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
talentových	talentový	k2eAgInPc2d1	talentový
stromů	strom	k1gInPc2	strom
<g/>
(	(	kIx(	(
<g/>
u	u	k7c2	u
druidů	druid	k1gMnPc2	druid
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
možné	možný	k2eAgFnPc4d1	možná
specializace	specializace	k1gFnPc4	specializace
daného	daný	k2eAgNnSc2d1	dané
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
povolání	povolání	k1gNnSc1	povolání
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
talentové	talentový	k2eAgInPc4d1	talentový
stromy	strom	k1gInPc4	strom
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
něco	něco	k3yInSc4	něco
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
léčení	léčení	k1gNnSc4	léčení
<g/>
,	,	kIx,	,
kontaktní	kontaktní	k2eAgInSc4d1	kontaktní
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
u	u	k7c2	u
mágů	mág	k1gMnPc2	mág
např.	např.	kA	např.
posílení	posílení	k1gNnSc1	posílení
ohnivých	ohnivý	k2eAgNnPc2d1	ohnivé
nebo	nebo	k8xC	nebo
ledových	ledový	k2eAgNnPc2d1	ledové
kouzel	kouzlo	k1gNnPc2	kouzlo
apod.	apod.	kA	apod.
Talenty	talent	k1gInPc1	talent
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
odnaučit	odnaučit	k5eAaPmF	odnaučit
a	a	k8xC	a
provést	provést	k5eAaPmF	provést
tzv.	tzv.	kA	tzv.
respecializaci	respecializace	k1gFnSc4	respecializace
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
změnu	změna	k1gFnSc4	změna
specializace	specializace	k1gFnSc2	specializace
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
službu	služba	k1gFnSc4	služba
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
trenéři	trenér	k1gMnPc1	trenér
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
povolání	povolání	k1gNnPc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
povolání	povolání	k1gNnSc4	povolání
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgMnSc1d1	efektivní
a	a	k8xC	a
mezi	mezi	k7c7	mezi
hráči	hráč	k1gMnPc7	hráč
oblíbená	oblíbený	k2eAgNnPc1d1	oblíbené
rozložení	rozložení	k1gNnSc4	rozložení
talentů	talent	k1gInPc2	talent
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
buildy	build	k1gInPc7	build
<g/>
.	.	kIx.	.
</s>
<s>
Buildy	Buildy	k6eAd1	Buildy
tak	tak	k9	tak
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
prakticky	prakticky	k6eAd1	prakticky
vymizely	vymizet	k5eAaPmAgInP	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Profese	profes	k1gFnPc1	profes
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
hráčům	hráč	k1gMnPc3	hráč
těžit	těžit	k5eAaImF	těžit
přírodní	přírodní	k2eAgFnPc4d1	přírodní
suroviny	surovina	k1gFnPc4	surovina
a	a	k8xC	a
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
nebo	nebo	k8xC	nebo
vyrábět	vyrábět	k5eAaImF	vyrábět
nové	nový	k2eAgInPc4d1	nový
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
rase	rasa	k1gFnSc6	rasa
<g/>
,	,	kIx,	,
levelu	level	k1gInSc6	level
či	či	k8xC	či
povolání	povolání	k1gNnSc6	povolání
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
naučit	naučit	k5eAaPmF	naučit
maximálně	maximálně	k6eAd1	maximálně
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
profese	profes	k1gFnPc4	profes
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
vedlejších	vedlejší	k2eAgMnPc2d1	vedlejší
není	být	k5eNaImIp3nS	být
omezen	omezen	k2eAgInSc1d1	omezen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
značně	značně	k6eAd1	značně
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
obchod	obchod	k1gInSc1	obchod
mezi	mezi	k7c7	mezi
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
maximální	maximální	k2eAgFnSc2d1	maximální
zkušenosti	zkušenost	k1gFnSc2	zkušenost
u	u	k7c2	u
profesí	profes	k1gFnPc2	profes
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
profesi	profese	k1gFnSc4	profese
tzv.	tzv.	kA	tzv.
vylepšit	vylepšit	k5eAaPmF	vylepšit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
Alchemie	Alchemie	k1gFnSc2	Alchemie
hráč	hráč	k1gMnSc1	hráč
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
několika	několik	k4yIc2	několik
úkolů	úkol	k1gInPc2	úkol
získá	získat	k5eAaPmIp3nS	získat
specializaci	specializace	k1gFnSc3	specializace
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
má	mít	k5eAaImIp3nS	mít
určitou	určitý	k2eAgFnSc4d1	určitá
procentuální	procentuální	k2eAgFnSc4d1	procentuální
šanci	šance	k1gFnSc4	šance
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
více	hodně	k6eAd2	hodně
lahviček	lahvička	k1gFnPc2	lahvička
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
profesí	profes	k1gFnPc2	profes
Minning	Minning	k1gInSc1	Minning
<g/>
,	,	kIx,	,
Skinning	Skinning	k1gInSc1	Skinning
a	a	k8xC	a
Herbalism	Herbalism	k1gMnSc1	Herbalism
hráč	hráč	k1gMnSc1	hráč
získává	získávat	k5eAaImIp3nS	získávat
postupně	postupně	k6eAd1	postupně
při	při	k7c6	při
zlepšování	zlepšování	k1gNnSc6	zlepšování
své	svůj	k3xOyFgFnSc2	svůj
profese	profes	k1gFnSc2	profes
následující	následující	k2eAgFnSc1d1	následující
výhody	výhoda	k1gFnPc1	výhoda
<g/>
:	:	kIx,	:
Minning	Minning	k1gInSc1	Minning
-	-	kIx~	-
zvyšování	zvyšování	k1gNnSc1	zvyšování
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Skinning	Skinning	k1gInSc1	Skinning
-	-	kIx~	-
vyšší	vysoký	k2eAgFnSc1d2	vyšší
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
kritický	kritický	k2eAgInSc4d1	kritický
úder	úder	k1gInSc4	úder
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
<g/>
,	,	kIx,	,
Herbalism	Herbalism	k1gMnSc1	Herbalism
-	-	kIx~	-
"	"	kIx"	"
<g/>
Lifeblood	Lifeblood	k1gInSc1	Lifeblood
<g/>
"	"	kIx"	"
-	-	kIx~	-
léčení	léčení	k1gNnSc1	léčení
sílou	síla	k1gFnSc7	síla
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
profesemi	profes	k1gFnPc7	profes
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Těžení	těžení	k1gNnSc1	těžení
rud	ruda	k1gFnPc2	ruda
(	(	kIx(	(
<g/>
Mining	Mining	k1gInSc1	Mining
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sbírací	Sbírací	k2eAgFnSc1d1	Sbírací
profese	profese	k1gFnSc1	profese
umožňující	umožňující	k2eAgFnSc4d1	umožňující
těžbu	těžba	k1gFnSc4	těžba
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rud	ruda	k1gFnPc2	ruda
a	a	k8xC	a
slévání	slévání	k1gNnPc2	slévání
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Kovářství	kovářství	k1gNnSc1	kovářství
(	(	kIx(	(
<g/>
Blacksmithing	Blacksmithing	k1gInSc1	Blacksmithing
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vyráběcí	Vyráběcí	k2eAgFnSc1d1	Vyráběcí
profese	profese	k1gFnSc1	profese
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
pomocí	pomoc	k1gFnSc7	pomoc
lze	lze	k6eAd1	lze
z	z	k7c2	z
kovů	kov	k1gInPc2	kov
vyrábět	vyrábět	k5eAaImF	vyrábět
těžká	těžký	k2eAgNnPc4d1	těžké
brnění	brnění	k1gNnPc4	brnění
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
klíče	klíč	k1gInPc4	klíč
a	a	k8xC	a
různá	různý	k2eAgNnPc4d1	různé
vylepšení	vylepšení	k1gNnPc4	vylepšení
zbroje	zbroj	k1gFnSc2	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Inženýrství	inženýrství	k1gNnSc1	inženýrství
(	(	kIx(	(
<g/>
Engineering	Engineering	k1gInSc1	Engineering
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
výrobu	výroba	k1gFnSc4	výroba
výbušnin	výbušnina	k1gFnPc2	výbušnina
a	a	k8xC	a
z	z	k7c2	z
kovových	kovový	k2eAgFnPc2d1	kovová
součástek	součástka	k1gFnPc2	součástka
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
různé	různý	k2eAgInPc1d1	různý
stroje	stroj	k1gInPc1	stroj
(	(	kIx(	(
<g/>
mechanické	mechanický	k2eAgMnPc4d1	mechanický
mazlíčky	mazlíček	k1gMnPc4	mazlíček
<g/>
,	,	kIx,	,
dálkové	dálkový	k2eAgNnSc4d1	dálkové
ovládání	ovládání	k1gNnSc4	ovládání
na	na	k7c4	na
roboty	robota	k1gFnPc4	robota
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
však	však	k9	však
často	často	k6eAd1	často
dokáže	dokázat	k5eAaPmIp3nS	dokázat
používat	používat	k5eAaImF	používat
pouze	pouze	k6eAd1	pouze
sám	sám	k3xTgMnSc1	sám
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
využívaná	využívaný	k2eAgFnSc1d1	využívaná
profese	profese	k1gFnSc1	profese
v	v	k7c6	v
PvP	PvP	k1gFnSc6	PvP
(	(	kIx(	(
<g/>
nitro	nitro	k1gNnSc1	nitro
boty	bota	k1gFnSc2	bota
zvyšující	zvyšující	k2eAgFnSc1d1	zvyšující
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
raketa	raketa	k1gFnSc1	raketa
v	v	k7c6	v
rukavici	rukavice	k1gFnSc6	rukavice
<g/>
,	,	kIx,	,
padák	padák	k1gInSc1	padák
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stahování	stahování	k1gNnSc1	stahování
Kůží	kůže	k1gFnPc2	kůže
(	(	kIx(	(
<g/>
Skinning	Skinning	k1gInSc1	Skinning
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sbírací	Sbírací	k2eAgFnPc4d1	Sbírací
profese	profes	k1gFnPc4	profes
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
získávání	získávání	k1gNnSc1	získávání
kůží	kůže	k1gFnPc2	kůže
z	z	k7c2	z
mrtvých	mrtvý	k2eAgNnPc2d1	mrtvé
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Kožedělnictví	Kožedělnictví	k1gNnSc1	Kožedělnictví
(	(	kIx(	(
<g/>
Leatherworking	Leatherworking	k1gInSc1	Leatherworking
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
kůže	kůže	k1gFnSc1	kůže
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
střední	střední	k2eAgMnPc1d1	střední
brnění	brnění	k1gNnSc3	brnění
a	a	k8xC	a
"	"	kIx"	"
<g/>
záplaty	záplata	k1gFnSc2	záplata
<g/>
"	"	kIx"	"
na	na	k7c6	na
brnění	brnění	k1gNnSc6	brnění
<g/>
.	.	kIx.	.
</s>
<s>
Krejčovství	krejčovství	k1gNnSc1	krejčovství
(	(	kIx(	(
<g/>
Tailoring	Tailoring	k1gInSc1	Tailoring
<g/>
)	)	kIx)	)
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
šití	šití	k1gNnSc4	šití
batohů	batoh	k1gInPc2	batoh
a	a	k8xC	a
lehkého	lehký	k2eAgNnSc2d1	lehké
brnění	brnění	k1gNnSc2	brnění
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
kouzelnická	kouzelnický	k2eAgNnPc4d1	kouzelnické
povolání	povolání	k1gNnPc4	povolání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylinkářství	bylinkářství	k1gNnSc1	bylinkářství
(	(	kIx(	(
<g/>
Herbalism	Herbalism	k1gInSc1	Herbalism
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sbírací	Sbírací	k2eAgFnPc4d1	Sbírací
profese	profes	k1gFnPc4	profes
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
nacházet	nacházet	k5eAaImF	nacházet
a	a	k8xC	a
sbírat	sbírat	k5eAaImF	sbírat
byliny	bylina	k1gFnPc4	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Alchymie	alchymie	k1gFnSc1	alchymie
(	(	kIx(	(
<g/>
Alchemy	Alchem	k1gInPc1	Alchem
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
mícháním	míchání	k1gNnSc7	míchání
kouzelných	kouzelný	k2eAgInPc2d1	kouzelný
lektvarů	lektvar	k1gInPc2	lektvar
z	z	k7c2	z
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Očarovávání	očarovávání	k1gNnSc1	očarovávání
(	(	kIx(	(
<g/>
Enchanting	Enchanting	k1gInSc1	Enchanting
<g/>
)	)	kIx)	)
-	-	kIx~	-
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rozložit	rozložit	k5eAaPmF	rozložit
předmět	předmět	k1gInSc4	předmět
na	na	k7c4	na
magický	magický	k2eAgInSc4d1	magický
prach	prach	k1gInSc4	prach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
očarování	očarování	k1gNnSc3	očarování
(	(	kIx(	(
<g/>
zlepšování	zlepšování	k1gNnSc3	zlepšování
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
)	)	kIx)	)
jiných	jiný	k2eAgInPc2d1	jiný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Klenotnictví	klenotnictví	k1gNnSc1	klenotnictví
(	(	kIx(	(
<g/>
Jewelcrafting	Jewelcrafting	k1gInSc1	Jewelcrafting
<g/>
)	)	kIx)	)
-	-	kIx~	-
Od	od	k7c2	od
The	The	k1gFnSc2	The
Burning	Burning	k1gInSc1	Burning
Crusade	Crusad	k1gInSc5	Crusad
<g/>
.	.	kIx.	.
</s>
<s>
Klenotník	klenotník	k1gMnSc1	klenotník
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyrábět	vyrábět	k5eAaImF	vyrábět
prsteny	prsten	k1gInPc4	prsten
<g/>
,	,	kIx,	,
náhrdelníky	náhrdelník	k1gInPc4	náhrdelník
a	a	k8xC	a
drahé	drahý	k2eAgInPc4d1	drahý
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Písařství	písařství	k1gNnSc1	písařství
(	(	kIx(	(
<g/>
Inscription	Inscription	k1gInSc1	Inscription
<g/>
)	)	kIx)	)
-	-	kIx~	-
Od	od	k7c2	od
Wrath	Wratha	k1gFnPc2	Wratha
of	of	k?	of
the	the	k?	the
Lich	Lich	k?	Lich
King	King	k1gMnSc1	King
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
svitky	svitek	k1gInPc4	svitek
které	který	k3yRgInPc1	který
posilují	posilovat	k5eAaImIp3nP	posilovat
atributy	atribut	k1gInPc4	atribut
a	a	k8xC	a
Glyphy	Glypha	k1gFnPc4	Glypha
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yRgFnPc3	který
můžete	moct	k5eAaImIp2nP	moct
vylepšit	vylepšit	k5eAaPmF	vylepšit
vaše	váš	k3xOp2gNnPc4	váš
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
profese	profese	k1gFnSc1	profese
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc4	první
pomoc	pomoc	k1gFnSc4	pomoc
(	(	kIx(	(
<g/>
First	First	k1gFnSc4	First
Aid	Aida	k1gFnPc2	Aida
<g/>
)	)	kIx)	)
-	-	kIx~	-
Tuto	tento	k3xDgFnSc4	tento
profesi	profese	k1gFnSc4	profese
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
obvazů	obvaz	k1gInPc2	obvaz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
léčí	léčit	k5eAaImIp3nP	léčit
zranění	zranění	k1gNnPc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Rybaření	rybařený	k2eAgMnPc1d1	rybařený
(	(	kIx(	(
<g/>
Fishing	Fishing	k1gInSc4	Fishing
<g/>
)	)	kIx)	)
-	-	kIx~	-
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
prutu	prut	k1gInSc2	prut
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
návnad	návnada	k1gFnPc2	návnada
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
chytat	chytat	k5eAaImF	chytat
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Vaření	vaření	k1gNnSc1	vaření
(	(	kIx(	(
<g/>
Cooking	Cooking	k1gInSc1	Cooking
<g/>
)	)	kIx)	)
-	-	kIx~	-
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
přeměnit	přeměnit	k5eAaPmF	přeměnit
syrové	syrový	k2eAgNnSc4d1	syrové
zvířecí	zvířecí	k2eAgNnSc4d1	zvířecí
maso	maso	k1gNnSc4	maso
nebo	nebo	k8xC	nebo
ryby	ryba	k1gFnPc4	ryba
na	na	k7c4	na
poživatelné	poživatelný	k2eAgNnSc4d1	poživatelné
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Archeologie	archeologie	k1gFnSc1	archeologie
(	(	kIx(	(
<g/>
Archaeology	Archaeolog	k1gMnPc7	Archaeolog
<g/>
)	)	kIx)	)
-	-	kIx~	-
Od	od	k7c2	od
datadisku	datadisek	k1gInSc2	datadisek
Cataclysm	Cataclysma	k1gFnPc2	Cataclysma
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
vykopávat	vykopávat	k5eAaImF	vykopávat
různé	různý	k2eAgInPc4d1	různý
artefakty	artefakt	k1gInPc4	artefakt
<g/>
.	.	kIx.	.
</s>
<s>
World	World	k1gInSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
představen	představit	k5eAaPmNgInS	představit
na	na	k7c6	na
ECTS	ECTS	kA	ECTS
v	v	k7c6	v
září	září	k1gNnSc6	září
2001	[number]	k4	2001
a	a	k8xC	a
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
hry	hra	k1gFnSc2	hra
trval	trvat	k5eAaImAgInS	trvat
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
3D	[number]	k4	3D
grafika	grafika	k1gFnSc1	grafika
ve	v	k7c4	v
World	World	k1gInSc4	World
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
využívá	využívat	k5eAaPmIp3nS	využívat
prvky	prvek	k1gInPc4	prvek
herního	herní	k2eAgMnSc2d1	herní
engine	enginout	k5eAaPmIp3nS	enginout
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
použité	použitý	k2eAgFnPc1d1	použitá
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Warcraft	Warcraft	k1gMnSc1	Warcraft
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
jako	jako	k8xS	jako
otevřený	otevřený	k2eAgInSc4d1	otevřený
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
hráči	hráč	k1gMnPc1	hráč
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Questy	Quest	k1gInPc1	Quest
jsou	být	k5eAaImIp3nP	být
nepovinné	povinný	k2eNgInPc1d1	nepovinný
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohly	pomoct	k5eAaPmAgInP	pomoct
vést	vést	k5eAaImF	vést
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožní	umožnit	k5eAaPmIp3nS	umožnit
vývoj	vývoj	k1gInSc1	vývoj
jeho	jeho	k3xOp3gFnSc2	jeho
herní	herní	k2eAgFnSc2d1	herní
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Rozhraní	rozhraní	k1gNnSc1	rozhraní
hry	hra	k1gFnSc2	hra
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hráčům	hráč	k1gMnPc3	hráč
měnit	měnit	k5eAaImF	měnit
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
ovládání	ovládání	k1gNnSc4	ovládání
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
instalovat	instalovat	k5eAaBmF	instalovat
doplňky	doplněk	k1gInPc4	doplněk
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
modifikace	modifikace	k1gFnPc4	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
World	World	k1gInSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
pro	pro	k7c4	pro
platformy	platforma	k1gFnPc4	platforma
OS	osa	k1gFnPc2	osa
X	X	kA	X
a	a	k8xC	a
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Krabicové	krabicový	k2eAgFnPc1d1	krabicová
verze	verze	k1gFnPc1	verze
hry	hra	k1gFnSc2	hra
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
instalaci	instalace	k1gFnSc4	instalace
tzv.	tzv.	kA	tzv.
hybridní	hybridní	k2eAgMnSc1d1	hybridní
CD	CD	kA	CD
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
eliminují	eliminovat	k5eAaBmIp3nP	eliminovat
potřebu	potřeba	k1gFnSc4	potřeba
samostatného	samostatný	k2eAgInSc2d1	samostatný
CD	CD	kA	CD
pro	pro	k7c4	pro
Mac	Mac	kA	Mac
a	a	k8xC	a
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hráčům	hráč	k1gMnPc3	hráč
hrát	hrát	k5eAaImF	hrát
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
není	být	k5eNaImIp3nS	být
World	World	k1gInSc4	World
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
oficiálně	oficiálně	k6eAd1	oficiálně
dostupný	dostupný	k2eAgInSc1d1	dostupný
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
platformy	platforma	k1gFnPc4	platforma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc4d1	možný
jeho	on	k3xPp3gInSc4	on
běh	běh	k1gInSc4	běh
pod	pod	k7c7	pod
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
Linux	Linux	kA	Linux
a	a	k8xC	a
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
Wine	Win	k1gInSc2	Win
<g/>
,	,	kIx,	,
speciálního	speciální	k2eAgInSc2d1	speciální
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
překlad	překlad	k1gInSc1	překlad
Windows	Windows	kA	Windows
API	API	kA	API
na	na	k7c4	na
nativní	nativní	k2eAgNnSc4d1	nativní
systémové	systémový	k2eAgNnSc4d1	systémové
volání	volání	k1gNnSc4	volání
<g/>
.	.	kIx.	.
</s>
<s>
Komunita	komunita	k1gFnSc1	komunita
českých	český	k2eAgMnPc2d1	český
hráčů	hráč	k1gMnPc2	hráč
Hordy	horda	k1gFnSc2	horda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
evropském	evropský	k2eAgNnSc6d1	Evropské
realmu	realmo	k1gNnSc6	realmo
s	s	k7c7	s
názvem	název	k1gInSc7	název
Drak	drak	k1gMnSc1	drak
<g/>
'	'	kIx"	'
<g/>
thul	thul	k1gMnSc1	thul
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
českých	český	k2eAgMnPc2d1	český
hráčů	hráč	k1gMnPc2	hráč
Alliance	Allianec	k1gInSc2	Allianec
pak	pak	k6eAd1	pak
na	na	k7c6	na
realmu	realm	k1gInSc6	realm
Burning	Burning	k1gInSc1	Burning
Blade	Blad	k1gInSc5	Blad
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
realmy	realm	k1gInPc1	realm
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
kompletně	kompletně	k6eAd1	kompletně
propojeny	propojen	k2eAgInPc1d1	propojen
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
virtual	virtual	k1gMnSc1	virtual
realm	realm	k1gMnSc1	realm
-	-	kIx~	-
prakticky	prakticky	k6eAd1	prakticky
již	již	k6eAd1	již
tedy	tedy	k9	tedy
nezáleží	záležet	k5eNaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
realmů	realm	k1gInPc2	realm
hráč	hráč	k1gMnSc1	hráč
zvolí	zvolit	k5eAaPmIp3nS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
realm	realm	k1gInSc4	realm
Drak	drak	k1gInSc1	drak
<g/>
'	'	kIx"	'
<g/>
thul	thulum	k1gNnPc2	thulum
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
sbíhat	sbíhat	k5eAaImF	sbíhat
česká	český	k2eAgFnSc1d1	Česká
komunita	komunita	k1gFnSc1	komunita
po	po	k7c6	po
umožnění	umožnění	k1gNnSc6	umožnění
migrace	migrace	k1gFnSc2	migrace
z	z	k7c2	z
realmu	realm	k1gInSc2	realm
Shadowmoon	Shadowmoona	k1gFnPc2	Shadowmoona
na	na	k7c4	na
Drak	drak	k1gInSc4	drak
<g/>
'	'	kIx"	'
<g/>
thul	thul	k1gMnSc1	thul
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
ale	ale	k8xC	ale
dosaženo	dosažen	k2eAgNnSc4d1	dosaženo
obrovské	obrovský	k2eAgFnPc4d1	obrovská
nevyváženosti	nevyváženost	k1gFnPc4	nevyváženost
příslušníků	příslušník	k1gMnPc2	příslušník
frakcí	frakce	k1gFnPc2	frakce
Alliance	Allianec	k1gInSc2	Allianec
a	a	k8xC	a
Hordy	horda	k1gFnSc2	horda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přimělo	přimět	k5eAaPmAgNnS	přimět
hráče	hráč	k1gMnPc4	hráč
Alliance	Alliance	k1gFnSc2	Alliance
na	na	k7c6	na
realmu	realm	k1gInSc6	realm
Drak	drak	k1gMnSc1	drak
<g/>
'	'	kIx"	'
<g/>
thul	thul	k1gMnSc1	thul
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
přejít	přejít	k5eAaPmF	přejít
na	na	k7c4	na
realm	realm	k1gInSc4	realm
Burning	Burning	k1gInSc4	Burning
Blade	Blad	k1gMnSc5	Blad
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
byla	být	k5eAaImAgFnS	být
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
migrace	migrace	k1gFnSc1	migrace
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
české	český	k2eAgFnSc2d1	Česká
komunity	komunita	k1gFnSc2	komunita
na	na	k7c4	na
2	[number]	k4	2
realmy	realma	k1gFnSc2	realma
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gMnSc1	drak
<g/>
'	'	kIx"	'
<g/>
thul	thul	k1gMnSc1	thul
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
nedostatkem	nedostatek	k1gInSc7	nedostatek
Alliančních	Allianční	k2eAgMnPc2d1	Allianční
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
nehraje	hrát	k5eNaImIp3nS	hrát
žádnou	žádný	k3yNgFnSc4	žádný
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
realmů	realm	k1gInPc2	realm
Drak	drak	k1gMnSc1	drak
<g/>
'	'	kIx"	'
<g/>
thul	thul	k1gInSc1	thul
a	a	k8xC	a
Burning	Burning	k1gInSc1	Burning
Blade	Blad	k1gInSc5	Blad
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
virtual	virtual	k1gMnSc1	virtual
realm	realm	k1gMnSc1	realm
<g/>
.	.	kIx.	.
</s>
<s>
Realmy	Realm	k1gInPc1	Realm
sice	sice	k8xC	sice
stále	stále	k6eAd1	stále
např.	např.	kA	např.
ve	v	k7c6	v
volbě	volba	k1gFnSc6	volba
realmu	realm	k1gInSc2	realm
figurují	figurovat	k5eAaImIp3nP	figurovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
herním	herní	k2eAgInSc6d1	herní
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
však	však	k9	však
vše	všechen	k3xTgNnSc1	všechen
propojeno	propojen	k2eAgNnSc1d1	propojeno
-	-	kIx~	-
hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
obou	dva	k4xCgMnPc2	dva
realmů	realm	k1gMnPc2	realm
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vzájemně	vzájemně	k6eAd1	vzájemně
potkávat	potkávat	k5eAaImF	potkávat
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
instance	instance	k1gFnPc4	instance
<g/>
,	,	kIx,	,
obchodovat	obchodovat	k5eAaImF	obchodovat
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
guildách	guilda	k1gFnPc6	guilda
atd.	atd.	kA	atd.
Česká	český	k2eAgFnSc1d1	Česká
komunita	komunita	k1gFnSc1	komunita
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
realmech	realm	k1gInPc6	realm
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Thunderhorn	Thunderhorna	k1gFnPc2	Thunderhorna
<g/>
,	,	kIx,	,
Wildhammer	Wildhammra	k1gFnPc2	Wildhammra
či	či	k8xC	či
Darksorrow	Darksorrow	k1gFnPc2	Darksorrow
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
