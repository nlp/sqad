<p>
<s>
Hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
také	také	k9	také
látka	látka	k1gFnSc1	látka
nebo	nebo	k8xC	nebo
materie	materie	k1gFnSc1	materie
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
materia	materium	k1gNnSc2	materium
<g/>
,	,	kIx,	,
řec.	řec.	k?	řec.
hylé	hylá	k1gFnPc1	hylá
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
matter	matter	k1gInSc1	matter
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
filosofický	filosofický	k2eAgInSc1d1	filosofický
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
stálou	stálý	k2eAgFnSc4d1	stálá
<g/>
,	,	kIx,	,
beztvarou	beztvarý	k2eAgFnSc4d1	beztvará
a	a	k8xC	a
spíše	spíše	k9	spíše
netečnou	tečný	k2eNgFnSc4d1	netečná
stránku	stránka	k1gFnSc4	stránka
smyslové	smyslový	k2eAgFnSc2d1	smyslová
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
základním	základní	k2eAgInPc3d1	základní
pojmům	pojem	k1gInPc3	pojem
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
stránku	stránka	k1gFnSc4	stránka
různé	různý	k2eAgInPc4d1	různý
filosofické	filosofický	k2eAgInPc4d1	filosofický
směry	směr	k1gInPc4	směr
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
<g/>
:	:	kIx,	:
u	u	k7c2	u
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
a	a	k8xC	a
ve	v	k7c6	v
scholastice	scholastika	k1gFnSc6	scholastika
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
v	v	k7c6	v
novověkém	novověký	k2eAgInSc6d1	novověký
dualismu	dualismus	k1gInSc6	dualismus
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
filosofii	filosofie	k1gFnSc6	filosofie
vědy	věda	k1gFnSc2	věda
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
předfilosofickém	předfilosofický	k2eAgNnSc6d1	předfilosofický
chápání	chápání	k1gNnSc6	chápání
pojmu	pojem	k1gInSc2	pojem
svědčí	svědčit	k5eAaImIp3nS	svědčit
původ	původ	k1gInSc4	původ
odpovídajících	odpovídající	k2eAgNnPc2d1	odpovídající
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
jak	jak	k8xC	jak
řecké	řecký	k2eAgNnSc1d1	řecké
hylé	hylé	k1gNnSc1	hylé
<g/>
,	,	kIx,	,
tak	tak	k9	tak
latinské	latinský	k2eAgNnSc4d1	latinské
materia	materium	k1gNnSc2	materium
znamenaly	znamenat	k5eAaImAgFnP	znamenat
asi	asi	k9	asi
původně	původně	k6eAd1	původně
dříví	dříví	k1gNnSc1	dříví
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
jako	jako	k8xC	jako
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
nebo	nebo	k8xC	nebo
topení	topení	k1gNnSc4	topení
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
latinské	latinský	k2eAgNnSc4d1	latinské
materia	materium	k1gNnSc2	materium
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
České	český	k2eAgFnSc2d1	Česká
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
snad	snad	k9	snad
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
praslovanského	praslovanský	k2eAgInSc2d1	praslovanský
kořene	kořen	k1gInSc2	kořen
gomo-	gomo-	k?	gomo-
(	(	kIx(	(
<g/>
staročeské	staročeský	k2eAgNnSc4d1	staročeské
homota	homot	k1gMnSc2	homot
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
hmotnost	hmotnost	k1gFnSc1	hmotnost
či	či	k8xC	či
massa	massa	k1gFnSc1	massa
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
od	od	k7c2	od
hroudy	hrouda	k1gFnSc2	hrouda
hněteného	hnětený	k2eAgNnSc2d1	hnětené
těsta	těsto	k1gNnSc2	těsto
či	či	k8xC	či
chleba	chléb	k1gInSc2	chléb
<g/>
/	/	kIx~	/
<g/>
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
znamenal	znamenat	k5eAaImAgInS	znamenat
náplň	náplň	k1gFnSc4	náplň
nebo	nebo	k8xC	nebo
výplň	výplň	k1gFnSc4	výplň
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
<g/>
:	:	kIx,	:
forma	forma	k1gFnSc1	forma
na	na	k7c4	na
těsto	těsto	k1gNnSc4	těsto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
češtině	čeština	k1gFnSc6	čeština
slovo	slovo	k1gNnSc1	slovo
znamenalo	znamenat	k5eAaImAgNnS	znamenat
také	také	k9	také
hnis	hnis	k1gInSc4	hnis
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
latinské	latinský	k2eAgNnSc4d1	latinské
materia	materium	k1gNnPc4	materium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srovnej	srovnat	k5eAaPmRp2nS	srovnat
slovo	slovo	k1gNnSc1	slovo
hníst	hníst	k5eAaImF	hníst
<g/>
/	/	kIx~	/
<g/>
mást	mást	k5eAaImF	mást
<g/>
/	/	kIx~	/
<g/>
mísit	mísit	k5eAaImF	mísit
</s>
</p>
<p>
<s>
==	==	k?	==
Látka	látka	k1gFnSc1	látka
a	a	k8xC	a
hmota	hmota	k1gFnSc1	hmota
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgMnPc1	první
řečtí	řecký	k2eAgMnPc1d1	řecký
filosofové	filosof	k1gMnPc1	filosof
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
o	o	k7c6	o
živlech	živel	k1gInPc6	živel
<g/>
,	,	kIx,	,
ne	ne	k9	ne
o	o	k7c6	o
obecné	obecný	k2eAgFnSc6d1	obecná
hmotě	hmota	k1gFnSc6	hmota
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
tento	tento	k3xDgInSc4	tento
filosoficky	filosoficky	k6eAd1	filosoficky
důležitý	důležitý	k2eAgInSc4d1	důležitý
pojem	pojem	k1gInSc4	pojem
zavedl	zavést	k5eAaPmAgInS	zavést
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
patrně	patrně	k6eAd1	patrně
Anaximandros	Anaximandrosa	k1gFnPc2	Anaximandrosa
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
atomisté	atomista	k1gMnPc1	atomista
(	(	kIx(	(
<g/>
Leukippos	Leukippos	k1gMnSc1	Leukippos
<g/>
,	,	kIx,	,
Démokritos	Démokritos	k1gMnSc1	Démokritos
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
nepatrných	patrný	k2eNgInPc2d1	patrný
<g/>
,	,	kIx,	,
nedělitelných	dělitelný	k2eNgInPc2d1	nedělitelný
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
atomos	atomos	k1gInSc1	atomos
<g/>
)	)	kIx)	)
a	a	k8xC	a
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
částeček	částečka	k1gFnPc2	částečka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nahodile	nahodile	k6eAd1	nahodile
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
se	se	k3xPyFc4	se
jsoucna	jsoucno	k1gNnSc2	jsoucno
tvoří	tvořit	k5eAaImIp3nS	tvořit
spojením	spojení	k1gNnSc7	spojení
beztvaré	beztvarý	k2eAgFnSc2d1	beztvará
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
eidos	eidos	k1gInSc1	eidos
<g/>
,	,	kIx,	,
forma	forma	k1gFnSc1	forma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plótinos	Plótinos	k1gInSc1	Plótinos
a	a	k8xC	a
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
platónismus	platónismus	k1gInSc1	platónismus
se	se	k3xPyFc4	se
na	na	k7c4	na
hmotu	hmota	k1gFnSc4	hmota
dívali	dívat	k5eAaImAgMnP	dívat
s	s	k7c7	s
podezřením	podezření	k1gNnSc7	podezření
<g/>
,	,	kIx,	,
gnóze	gnóze	k6eAd1	gnóze
ji	on	k3xPp3gFnSc4	on
pokládala	pokládat	k5eAaImAgFnS	pokládat
za	za	k7c4	za
hrob	hrob	k1gInSc4	hrob
či	či	k8xC	či
vězení	vězení	k1gNnSc4	vězení
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Scholastika	scholastika	k1gFnSc1	scholastika
převzala	převzít	k5eAaPmAgFnS	převzít
Aristotelovo	Aristotelův	k2eAgNnSc4d1	Aristotelovo
pojetí	pojetí	k1gNnSc4	pojetí
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
ještě	ještě	k9	ještě
u	u	k7c2	u
Kanta	Kant	k1gMnSc2	Kant
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
rozlišil	rozlišit	k5eAaPmAgMnS	rozlišit
dvojí	dvojí	k4xRgFnSc4	dvojí
substanci	substance	k1gFnSc4	substance
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
rozlehlou	rozlehlý	k2eAgFnSc4d1	rozlehlá
a	a	k8xC	a
myslící	myslící	k2eAgFnSc4d1	myslící
<g/>
:	:	kIx,	:
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
nemyslí	myslet	k5eNaImIp3nS	myslet
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
myslící	myslící	k2eAgFnSc1d1	myslící
nemá	mít	k5eNaImIp3nS	mít
rozměry	rozměr	k1gInPc7	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osvícenství	osvícenství	k1gNnSc6	osvícenství
i	i	k8xC	i
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
empirismu	empirismus	k1gInSc6	empirismus
rostou	růst	k5eAaImIp3nP	růst
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
"	"	kIx"	"
<g/>
myslící	myslící	k2eAgFnSc6d1	myslící
substanci	substance	k1gFnSc6	substance
<g/>
"	"	kIx"	"
a	a	k8xC	a
o	o	k7c6	o
duchovním	duchovní	k2eAgInSc6d1	duchovní
světě	svět	k1gInSc6	svět
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
šíří	šíř	k1gFnPc2	šíř
mezi	mezi	k7c7	mezi
vzdělanými	vzdělaný	k2eAgMnPc7d1	vzdělaný
lidmi	člověk	k1gMnPc7	člověk
materialismus	materialismus	k1gInSc4	materialismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pak	pak	k6eAd1	pak
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obecně	obecně	k6eAd1	obecně
převládl	převládnout	k5eAaPmAgInS	převládnout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
fyzikové	fyzik	k1gMnPc1	fyzik
silně	silně	k6eAd1	silně
zkomplikovali	zkomplikovat	k5eAaPmAgMnP	zkomplikovat
názornou	názorný	k2eAgFnSc4d1	názorná
a	a	k8xC	a
zdánlivě	zdánlivě	k6eAd1	zdánlivě
samozřejmou	samozřejmý	k2eAgFnSc4d1	samozřejmá
představu	představa	k1gFnSc4	představa
pevné	pevný	k2eAgFnSc2d1	pevná
<g/>
,	,	kIx,	,
neprostupné	prostupný	k2eNgFnSc2d1	neprostupná
a	a	k8xC	a
netečné	tečný	k2eNgFnSc2d1	netečná
"	"	kIx"	"
<g/>
hmoty	hmota	k1gFnSc2	hmota
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
biologové	biolog	k1gMnPc1	biolog
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
snažili	snažit	k5eAaImAgMnP	snažit
vybavit	vybavit	k5eAaPmF	vybavit
také	také	k9	také
životem	život	k1gInSc7	život
(	(	kIx(	(
<g/>
hylozoismus	hylozoismus	k1gInSc1	hylozoismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
filosofii	filosofie	k1gFnSc6	filosofie
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
hmoty	hmota	k1gFnSc2	hmota
užívá	užívat	k5eAaImIp3nS	užívat
spíše	spíše	k9	spíše
polemicky	polemicky	k6eAd1	polemicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Idealistické	idealistický	k2eAgInPc1d1	idealistický
pohledy	pohled	k1gInPc1	pohled
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
filozofické	filozofický	k2eAgInPc1d1	filozofický
směry	směr	k1gInPc1	směr
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
vědecký	vědecký	k2eAgInSc4d1	vědecký
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
okolní	okolní	k2eAgInSc1d1	okolní
svět	svět	k1gInSc1	svět
a	a	k8xC	a
hmota	hmota	k1gFnSc1	hmota
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
naše	naši	k1gMnPc4	naši
vnímání	vnímání	k1gNnSc1	vnímání
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
signály	signál	k1gInPc1	signál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nám	my	k3xPp1nPc3	my
přichází	přicházet	k5eAaImIp3nP	přicházet
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tak	tak	k6eAd1	tak
smyslové	smyslový	k2eAgInPc4d1	smyslový
vjemy	vjem	k1gInPc4	vjem
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
čichu	čich	k1gInSc2	čich
<g/>
,	,	kIx,	,
hmatu	hmat	k1gInSc2	hmat
<g/>
,	,	kIx,	,
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
objektivní	objektivní	k2eAgMnPc1d1	objektivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naše	náš	k3xOp1gNnSc1	náš
subjektivní	subjektivní	k2eAgNnSc1d1	subjektivní
vnímání	vnímání	k1gNnSc1	vnímání
reality	realita	k1gFnSc2	realita
vytvářené	vytvářený	k2eAgFnSc2d1	vytvářená
mozkem	mozek	k1gInSc7	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Atomismus	atomismus	k1gInSc1	atomismus
</s>
</p>
<p>
<s>
Hmota	hmota	k1gFnSc1	hmota
</s>
</p>
<p>
<s>
Látka	látka	k1gFnSc1	látka
</s>
</p>
<p>
<s>
Materialismus	materialismus	k1gInSc1	materialismus
</s>
</p>
<p>
<s>
Pralátka	pralátka	k1gFnSc1	pralátka
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Duch	duch	k1gMnSc1	duch
a	a	k8xC	a
hmota	hmota	k1gFnSc1	hmota
v	v	k7c6	v
theorii	theorie	k1gFnSc6	theorie
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Rupp	Rupp	k1gMnSc1	Rupp
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
</s>
</p>
<p>
<s>
H.	H.	kA	H.
Bergson	Bergson	k1gNnSc1	Bergson
<g/>
,	,	kIx,	,
Hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
paměť	paměť	k1gFnSc1	paměť
<g/>
:	:	kIx,	:
esej	esej	k1gFnSc1	esej
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
těla	tělo	k1gNnSc2	tělo
k	k	k7c3	k
duchu	duch	k1gMnSc3	duch
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
-	-	kIx~	-
191	[number]	k4	191
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-7298-065-3	[number]	k4	80-7298-065-3
</s>
</p>
<p>
<s>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelský	nakladatelský	k2eAgInSc4d1	nakladatelský
dům	dům	k1gInSc4	dům
OP	op	k1gMnSc1	op
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
-	-	kIx~	-
64	[number]	k4	64
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-85841-27-4	[number]	k4	80-85841-27-4
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
11	[number]	k4	11
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
386	[number]	k4	386
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
nové	nový	k2eAgNnSc4d1	nové
doby	doba	k1gFnPc4	doba
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc4	heslo
Hmota	hmota	k1gFnSc1	hmota
s	s	k7c7	s
hlediska	hledisko	k1gNnSc2	hledisko
filosofického	filosofický	k2eAgNnSc2d1	filosofické
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
1155	[number]	k4	1155
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
