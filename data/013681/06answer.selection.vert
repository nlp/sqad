<s desamb="1">
Emden	Emden	k1gInSc1
přepadal	přepadat	k5eAaImAgInS
během	během	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
1914	#num#	k4
především	především	k6eAd1
spojenecké	spojenecký	k2eAgFnSc2d1
zásobovací	zásobovací	k2eAgFnSc2d1
a	a	k8xC
válečné	válečný	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
celého	celý	k2eAgInSc2d1
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
,	,	kIx,
jichž	jenž	k3xRgInPc2
sám	sám	k3xTgMnSc1
bez	bez	k7c2
pomoci	pomoc	k1gFnSc2
potopil	potopit	k5eAaPmAgInS
celkem	celkem	k6eAd1
třicet	třicet	k4xCc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1914	#num#	k4
však	však	k9
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
kapitánem	kapitán	k1gMnSc7
cíleně	cíleně	k6eAd1
naveden	naveden	k2eAgMnSc1d1
na	na	k7c4
mělčinu	mělčina	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
zabráněno	zabráněn	k2eAgNnSc1d1
potopení	potopení	k1gNnSc1
lodi	loď	k1gFnSc2
z	z	k7c2
důvodu	důvod	k1gInSc2
těžkého	těžký	k2eAgNnSc2d1
poškození	poškození	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
utrpěla	utrpět	k5eAaPmAgFnS
u	u	k7c2
Kokosových	kokosový	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
ve	v	k7c6
střetu	střet	k1gInSc6
s	s	k7c7
australským	australský	k2eAgInSc7d1
křižníkem	křižník	k1gInSc7
HMAS	HMAS	kA
Sydney	Sydney	k1gNnSc1
<g/>
.	.	kIx.
</s>