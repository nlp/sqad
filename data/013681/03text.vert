<s>
SMS	SMS	kA
Emden	Emden	k1gInSc1
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Typ	typ	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
</s>
<s>
Třída	třída	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
třída	třída	k1gFnSc1
Dresden	Dresdna	k1gFnPc2
</s>
<s>
Jméno	jméno	k1gNnSc1
podle	podle	k7c2
<g/>
:	:	kIx,
</s>
<s>
Emden	Emden	k1gInSc1
</s>
<s>
Zahájení	zahájení	k1gNnSc1
stavby	stavba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1906	#num#	k4
</s>
<s>
Spuštěna	spuštěn	k2eAgNnPc1d1
na	na	k7c4
vodu	voda	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1908	#num#	k4
</s>
<s>
Uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1909	#num#	k4
</s>
<s>
Osud	osud	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
potopen	potopen	k2eAgMnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1914	#num#	k4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kokosových	kokosový	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
</s>
<s>
Takticko-technická	takticko-technický	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
Výtlak	výtlak	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
3	#num#	k4
364	#num#	k4
tun	tuna	k1gFnPc2
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
118	#num#	k4
m	m	kA
</s>
<s>
Šířka	šířka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
13,4	13,4	k4
m	m	kA
</s>
<s>
Ponor	ponor	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
5,3	5,3	k4
m	m	kA
</s>
<s>
Pohon	pohon	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
12	#num#	k4
kotlů	kotel	k1gInPc2
<g/>
2	#num#	k4
parní	parní	k2eAgInSc1d1
stroje	stroj	k1gInSc2
<g/>
12	#num#	k4
MW	MW	kA
</s>
<s>
Rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
23	#num#	k4
uzlů	uzel	k1gInPc2
</s>
<s>
Dosah	dosah	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
3700	#num#	k4
nám.	nám.	k?
mil	míle	k1gFnPc2
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
18	#num#	k4
důstojníků	důstojník	k1gMnPc2
<g/>
343	#num#	k4
námořníků	námořník	k1gMnPc2
</s>
<s>
Pancíř	pancíř	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
boční	boční	k2eAgInSc4d1
51	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
paluba	paluba	k1gFnSc1
13	#num#	k4
mm	mm	kA
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
10	#num#	k4
<g/>
×	×	k?
105	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
10	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
×	×	k?
450	#num#	k4
<g/>
mm	mm	kA
torpédomet	torpédomet	k1gInSc1
</s>
<s>
Operační	operační	k2eAgNnSc1d1
nasazení	nasazení	k1gNnSc1
</s>
<s>
Nasazení	nasazení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
SMS	SMS	kA
Emden	Emden	k1gInSc1
byl	být	k5eAaImAgInS
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
německého	německý	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
bojově	bojově	k6eAd1
nasazen	nasadit	k5eAaPmNgInS
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
dvou	dva	k4xCgFnPc2
postavených	postavený	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
třídy	třída	k1gFnSc2
Dresden	Dresdna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emden	Emden	k1gInSc1
přepadal	přepadat	k5eAaImAgInS
během	během	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
1914	#num#	k4
především	především	k6eAd1
spojenecké	spojenecký	k2eAgFnSc2d1
zásobovací	zásobovací	k2eAgFnSc2d1
a	a	k8xC
válečné	válečný	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
celého	celý	k2eAgInSc2d1
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
,	,	kIx,
jichž	jenž	k3xRgInPc2
sám	sám	k3xTgMnSc1
bez	bez	k7c2
pomoci	pomoc	k1gFnSc2
potopil	potopit	k5eAaPmAgInS
celkem	celkem	k6eAd1
třicet	třicet	k4xCc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1914	#num#	k4
však	však	k9
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
kapitánem	kapitán	k1gMnSc7
cíleně	cíleně	k6eAd1
naveden	naveden	k2eAgMnSc1d1
na	na	k7c4
mělčinu	mělčina	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
zabráněno	zabráněn	k2eAgNnSc1d1
potopení	potopení	k1gNnSc1
lodi	loď	k1gFnSc2
z	z	k7c2
důvodu	důvod	k1gInSc2
těžkého	těžký	k2eAgNnSc2d1
poškození	poškození	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
utrpěla	utrpět	k5eAaPmAgFnS
u	u	k7c2
Kokosových	kokosový	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
ve	v	k7c6
střetu	střet	k1gInSc6
s	s	k7c7
australským	australský	k2eAgInSc7d1
křižníkem	křižník	k1gInSc7
HMAS	HMAS	kA
Sydney	Sydney	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Kýl	kýl	k1gInSc1
lodě	loď	k1gFnSc2
byl	být	k5eAaImAgInS
položen	položen	k2eAgInSc1d1
dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1906	#num#	k4
v	v	k7c6
Kaiserliche	Kaiserliche	k1gNnSc6
Werft	Werfta	k1gFnPc2
v	v	k7c6
Gdaňsku	Gdaňsk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
bylo	být	k5eAaImAgNnS
zvoleno	zvolit	k5eAaPmNgNnS
podle	podle	k7c2
města	město	k1gNnSc2
Emden	Emdna	k1gFnPc2
na	na	k7c6
řece	řeka	k1gFnSc6
Ems	Ems	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
služby	služba	k1gFnSc2
vstoupila	vstoupit	k5eAaPmAgFnS
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1909	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výtlak	výtlak	k1gInSc1
lodi	loď	k1gFnSc2
byl	být	k5eAaImAgInS
3	#num#	k4
364	#num#	k4
tun	tuna	k1gFnPc2
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
118	#num#	k4
m	m	kA
<g/>
,	,	kIx,
šířka	šířka	k1gFnSc1
13,4	13,4	k4
m	m	kA
<g/>
,	,	kIx,
ponor	ponor	k1gInSc1
5,3	5,3	k4
m.	m.	k?
Pohon	pohon	k1gInSc1
byl	být	k5eAaImAgInS
zajišťován	zajišťovat	k5eAaImNgInS
parními	parní	k2eAgInPc7d1
stroji	stroj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výkonu	výkon	k1gInSc6
12	#num#	k4
MW	MW	kA
a	a	k8xC
tří	tři	k4xCgFnPc2
komínech	komín	k1gInPc6
umožňovaly	umožňovat	k5eAaImAgFnP
rychlost	rychlost	k1gFnSc4
23	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
42,6	42,6	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emden	Emdna	k1gFnPc2
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
loď	loď	k1gFnSc1
poháněná	poháněný	k2eAgFnSc1d1
parními	parní	k2eAgInPc7d1
stroji	stroj	k1gInPc7
<g/>
,	,	kIx,
všechny	všechen	k3xTgInPc1
další	další	k2eAgInPc1d1
již	již	k6eAd1
byly	být	k5eAaImAgInP
vybaveny	vybavit	k5eAaPmNgInP
turbínami	turbína	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
výzbroj	výzbroj	k1gInSc4
tvořilo	tvořit	k5eAaImAgNnS
10	#num#	k4
děl	dělo	k1gNnPc2
ráže	ráže	k1gFnSc1
105	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
loď	loď	k1gFnSc1
dále	daleko	k6eAd2
nesla	nést	k5eAaImAgFnS
dva	dva	k4xCgInPc4
450	#num#	k4
<g/>
mm	mm	kA
torpédomety	torpédomet	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádka	posádka	k1gFnSc1
sestávala	sestávat	k5eAaImAgFnS
z	z	k7c2
360	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
velitel	velitel	k1gMnSc1
byl	být	k5eAaImAgMnS
jmenován	jmenován	k2eAgMnSc1d1
korvetní	korvetní	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
Karl	Karl	k1gMnSc1
von	von	k1gInSc4
Müller	Müller	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Služba	služba	k1gFnSc1
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1910	#num#	k4
byl	být	k5eAaImAgInS
Emden	Emden	k1gInSc1
zařazen	zařadit	k5eAaPmNgInS
do	do	k7c2
Německé	německý	k2eAgFnSc2d1
východoasijské	východoasijský	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
<g/>
,	,	kIx,
operující	operující	k2eAgMnSc1d1
pod	pod	k7c7
velením	velení	k1gNnSc7
viceadmirála	viceadmirál	k1gMnSc2
hraběte	hrabě	k1gMnSc2
Maximiliana	Maximilian	k1gMnSc2
von	von	k1gInSc4
Spee	Spee	k1gNnSc2
ze	z	k7c2
základny	základna	k1gFnSc2
v	v	k7c6
Tsing-Tau	Tsing-Taus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
dostal	dostat	k5eAaPmAgInS
Emden	Emden	k2eAgInSc1d1
rozkaz	rozkaz	k1gInSc1
připojit	připojit	k5eAaPmF
se	se	k3xPyFc4
k	k	k7c3
hlavním	hlavní	k2eAgFnPc3d1
silám	síla	k1gFnPc3
eskadry	eskadra	k1gFnSc2
při	při	k7c6
Karolínském	karolínský	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
Pagan	Pagana	k1gFnPc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1914	#num#	k4
zajal	zajmout	k5eAaPmAgMnS
při	při	k7c6
své	svůj	k3xOyFgFnSc6
plavbě	plavba	k1gFnSc6
první	první	k4xOgFnSc4
loď	loď	k1gFnSc4
a	a	k8xC
to	ten	k3xDgNnSc4
ruský	ruský	k2eAgInSc1d1
parník	parník	k1gInSc1
Rjazaň	Rjazaň	k1gFnSc1
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
vyplula	vyplout	k5eAaPmAgFnS
von	von	k1gInSc4
Speeova	Speeův	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
s	s	k7c7
cílem	cíl	k1gInSc7
obeplout	obeplout	k5eAaPmF
Hornův	Hornův	k2eAgInSc4d1
mys	mys	k1gInSc4
a	a	k8xC
probít	probít	k5eAaPmF
se	se	k3xPyFc4
zpět	zpět	k6eAd1
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eskadra	eskadra	k1gFnSc1
sice	sice	k8xC
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
střetu	střet	k1gInSc6
s	s	k7c7
britskou	britský	k2eAgFnSc7d1
flotilou	flotila	k1gFnSc7
v	v	k7c6
námořní	námořní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Coronelu	Coronel	k1gInSc2
u	u	k7c2
pobřeží	pobřeží	k1gNnSc2
Chile	Chile	k1gNnSc2
zvítězila	zvítězit	k5eAaPmAgFnS
<g/>
,	,	kIx,
následně	následně	k6eAd1
však	však	k9
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
při	při	k7c6
Falklandských	Falklandský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Počátek	počátek	k1gInSc1
korzárské	korzárský	k2eAgFnSc2d1
plavby	plavba	k1gFnSc2
</s>
<s>
Trasa	trasa	k1gFnSc1
plavby	plavba	k1gFnSc2
Emdenu	Emden	k1gInSc2
v	v	k7c6
Indickém	indický	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
od	od	k7c2
září	září	k1gNnSc2
do	do	k7c2
listopadu	listopad	k1gInSc2
1914	#num#	k4
</s>
<s>
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1914	#num#	k4
dostal	dostat	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
Müller	Müller	k1gMnSc1
rozkaz	rozkaz	k1gInSc4
proniknout	proniknout	k5eAaPmF
do	do	k7c2
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
a	a	k8xC
narušovat	narušovat	k5eAaImF
zde	zde	k6eAd1
spojenecké	spojenecký	k2eAgFnPc4d1
obchodní	obchodní	k2eAgFnPc4d1
trasy	trasa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
druhým	druhý	k4xOgInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
rozptýlit	rozptýlit	k5eAaPmF
síly	síla	k1gFnPc4
Dohody	dohoda	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
usnadnit	usnadnit	k5eAaPmF
únik	únik	k1gInSc1
hlavních	hlavní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
východoasijské	východoasijský	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
cíle	cíl	k1gInPc1
byly	být	k5eAaImAgInP
splněny	splnit	k5eAaPmNgInP
a	a	k8xC
Emden	Emden	k2eAgInSc1d1
ke	k	k7c3
konci	konec	k1gInSc3
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
plavby	plavba	k1gFnPc4
hledalo	hledat	k5eAaImAgNnS
78	#num#	k4
lodí	loď	k1gFnPc2
čtyř	čtyři	k4xCgInPc2
států	stát	k1gInPc2
(	(	kIx(
<g/>
Francouzské	francouzský	k2eAgFnSc2d1
<g/>
,	,	kIx,
Britské	britský	k2eAgFnSc2d1
<g/>
,	,	kIx,
Ruské	ruský	k2eAgFnSc2d1
a	a	k8xC
Australské	australský	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emden	Emden	k2eAgInSc4d1
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
misi	mise	k1gFnSc4
doprovázela	doprovázet	k5eAaImAgFnS
německá	německý	k2eAgFnSc1d1
nákladní	nákladní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Tannenfels	Tannenfelsa	k1gFnPc2
se	se	k3xPyFc4
zásobami	zásoba	k1gFnPc7
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
lodě	loď	k1gFnPc1
společně	společně	k6eAd1
propluly	proplout	k5eAaPmAgFnP
Lombockou	Lombocký	k2eAgFnSc7d1
úžinou	úžina	k1gFnSc7
z	z	k7c2
Tichého	Tichého	k2eAgMnSc2d1
do	do	k7c2
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
a	a	k8xC
zamířili	zamířit	k5eAaPmAgMnP
do	do	k7c2
Bengálského	bengálský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
se	se	k3xPyFc4
Emdenu	Emden	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
zajmout	zajmout	k5eAaPmF
řecký	řecký	k2eAgInSc4d1
parník	parník	k1gInSc4
Pontoporos	Pontoporosa	k1gFnPc2
a	a	k8xC
v	v	k7c6
průběhu	průběh	k1gInSc6
týdne	týden	k1gInSc2
dalších	další	k2eAgFnPc2d1
sedm	sedm	k4xCc1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
činnost	činnost	k1gFnSc1
ochromila	ochromit	k5eAaPmAgFnS
námořní	námořní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
Müller	Müller	k1gMnSc1
usoudil	usoudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
oblast	oblast	k1gFnSc4
budou	být	k5eAaImBp3nP
prohledávat	prohledávat	k5eAaImF
spojenecké	spojenecký	k2eAgInPc4d1
křižníky	křižník	k1gInPc4
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgMnS
na	na	k7c4
spojnici	spojnice	k1gFnSc4
Singapur-Rangún	Singapur-Rangúna	k1gFnPc2
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
připlul	připlout	k5eAaPmAgMnS
k	k	k7c3
Madrasu	madras	k1gInSc3
a	a	k8xC
ve	v	k7c6
21.30	21.30	k4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
vypálil	vypálit	k5eAaPmAgMnS
na	na	k7c4
přístav	přístav	k1gInSc4
130	#num#	k4
ran	rána	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
zasáhnout	zasáhnout	k5eAaPmF
zásobníky	zásobník	k1gInPc4
ropy	ropa	k1gFnSc2
spadající	spadající	k2eAgFnSc2d1
pod	pod	k7c4
britskou	britský	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Burmah	Burmaha	k1gFnPc2
Oil	Oil	k1gFnSc2
Company	Compana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
požáru	požár	k1gInSc3
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgMnSc6
shořelo	shořet	k5eAaPmAgNnS
kolem	kolem	k6eAd1
dvou	dva	k4xCgInPc2
milionů	milion	k4xCgInPc2
litrů	litr	k1gInPc2
ropy	ropa	k1gFnSc2
<g/>
,	,	kIx,
největší	veliký	k2eAgFnPc4d3
zásoby	zásoba	k1gFnPc4
této	tento	k3xDgFnSc2
komodity	komodita	k1gFnSc2
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
akci	akce	k1gFnSc6
zamířil	zamířit	k5eAaPmAgMnS
Müller	Müller	k1gMnSc1
k	k	k7c3
Cejlonu	Cejlon	k1gInSc3
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc3d1
Srí	Srí	k1gFnSc3
Lance	lance	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
zničil	zničit	k5eAaPmAgInS
pět	pět	k4xCc4
obchodních	obchodní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
zajaté	zajatý	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
umístil	umístit	k5eAaPmAgMnS
na	na	k7c4
šestou	šestý	k4xOgFnSc4
zajatou	zajatý	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
25	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
do	do	k7c2
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
provedla	provést	k5eAaPmAgFnS
posádka	posádka	k1gFnSc1
nutné	nutný	k2eAgFnSc2d1
opravy	oprava	k1gFnSc2
a	a	k8xC
údržbu	údržba	k1gFnSc4
při	při	k7c6
souostroví	souostroví	k1gNnSc6
Lakadivy	Lakadivy	k1gFnPc4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
byl	být	k5eAaImAgMnS
Emden	Emden	k2eAgMnSc1d1
znovu	znovu	k6eAd1
na	na	k7c6
moři	moře	k1gNnSc6
při	při	k7c6
západním	západní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Srí	Srí	k1gFnSc2
Lanky	lanko	k1gNnPc7
a	a	k8xC
následně	následně	k6eAd1
při	při	k7c6
Maledivách	Maledivy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
týdne	týden	k1gInSc2
zajal	zajmout	k5eAaPmAgMnS
dalších	další	k2eAgFnPc2d1
sedm	sedm	k4xCc1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádky	posádka	k1gFnPc1
byly	být	k5eAaImAgFnP
soustředěny	soustředit	k5eAaPmNgFnP
na	na	k7c4
jednu	jeden	k4xCgFnSc4
a	a	k8xC
ostatní	ostatní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
byly	být	k5eAaImAgFnP
potopeny	potopit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Penangu	Penang	k1gInSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
u	u	k7c2
Penangu	Penang	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Model	model	k1gInSc1
křižníku	křižník	k1gInSc2
SMS	SMS	kA
Emden	Emden	k2eAgInSc4d1
</s>
<s>
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c6
3.00	3.00	k4
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
před	před	k7c7
přístavem	přístav	k1gInSc7
George	George	k1gInSc1
Town	Towna	k1gFnPc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Penang	Penanga	k1gFnPc2
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
<g/>
,	,	kIx,
zamaskovaný	zamaskovaný	k2eAgInSc1d1
čtvrtým	čtvrtý	k4xOgInSc7
falešným	falešný	k2eAgInSc7d1
komínem	komín	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vypadal	vypadat	k5eAaImAgInS,k5eAaPmAgInS
jako	jako	k9
britský	britský	k2eAgInSc1d1
křižník	křižník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
britské	britský	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
měly	mít	k5eAaImAgInP
čtyři	čtyři	k4xCgInPc1
komíny	komín	k1gInPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
německé	německý	k2eAgFnPc1d1
měly	mít	k5eAaImAgFnP
pouze	pouze	k6eAd1
tři	tři	k4xCgFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlídky	hlídka	k1gFnSc2
sice	sice	k8xC
loď	loď	k1gFnSc4
zpozorovaly	zpozorovat	k5eAaPmAgInP
<g/>
,	,	kIx,
lest	lest	k1gFnSc4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
však	však	k9
povedla	povést	k5eAaPmAgFnS
a	a	k8xC
lodi	loď	k1gFnSc2
tedy	tedy	k9
nikdo	nikdo	k3yNnSc1
nevěnoval	věnovat	k5eNaImAgInS
zvláštní	zvláštní	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emden	Emden	k1gInSc1
náhle	náhle	k6eAd1
zahájil	zahájit	k5eAaPmAgInS
palbu	palba	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
přístavu	přístav	k1gInSc6
zničil	zničit	k5eAaPmAgInS
ruský	ruský	k2eAgInSc1d1
chráněný	chráněný	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Žemčug	Žemčuga	k1gFnPc2
a	a	k8xC
při	při	k7c6
úniku	únik	k1gInSc6
z	z	k7c2
přístavu	přístav	k1gInSc2
též	též	k9
hlídkující	hlídkující	k2eAgMnSc1d1
francouzský	francouzský	k2eAgMnSc1d1
torpédoborec	torpédoborec	k1gMnSc1
Mousquet	Mousquet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
na	na	k7c4
Žemčug	Žemčug	k1gInSc4
si	se	k3xPyFc3
vyžádal	vyžádat	k5eAaPmAgInS
87	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
135	#num#	k4
raněných	raněný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Mousqete	Mousqe	k1gNnSc2
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
40	#num#	k4
námořníků	námořník	k1gMnPc2
včetně	včetně	k7c2
kapitána	kapitán	k1gMnSc2
<g/>
.	.	kIx.
35	#num#	k4
trosečníků	trosečník	k1gMnPc2
Emden	Emden	k1gInSc4
zachránil	zachránit	k5eAaPmAgInS
a	a	k8xC
následně	následně	k6eAd1
odevzdal	odevzdat	k5eAaPmAgMnS
na	na	k7c4
neutrální	neutrální	k2eAgFnSc4d1
norskou	norský	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
ruského	ruský	k2eAgInSc2d1
Žemčugu	Žemčug	k1gInSc2
baron	baron	k1gMnSc1
Čerkasov	Čerkasov	k1gInSc4
nebyl	být	k5eNaImAgMnS
během	během	k7c2
útoku	útok	k1gInSc2
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
u	u	k7c2
své	svůj	k3xOyFgFnSc2
přítelkyně	přítelkyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
pro	pro	k7c4
nedbalost	nedbalost	k1gFnSc4
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
k	k	k7c3
3,5	3,5	k4
rokům	rok	k1gInPc3
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Potopení	potopení	k1gNnSc1
lodi	loď	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
u	u	k7c2
Kokosových	kokosový	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vrak	vrak	k1gInSc1
Emdenu	Emden	k1gInSc2
</s>
<s>
105	#num#	k4
<g/>
mm	mm	kA
dělo	dít	k5eAaImAgNnS,k5eAaBmAgNnS
z	z	k7c2
Emdenu	Emden	k1gInSc2
nacházející	nacházející	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
Hyde	Hyde	k1gNnSc6
Parku	park	k1gInSc2
v	v	k7c6
Sydney	Sydney	k1gNnSc6
</s>
<s>
Po	po	k7c6
této	tento	k3xDgFnSc6
akci	akce	k1gFnSc6
Emden	Emdna	k1gFnPc2
zamířil	zamířit	k5eAaPmAgInS
ke	k	k7c3
Kokosovým	kokosový	k2eAgInPc3d1
ostrovům	ostrov	k1gInPc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
Müller	Müller	k1gInSc1
v	v	k7c6
plánu	plán	k1gInSc6
zničit	zničit	k5eAaPmF
vysílací	vysílací	k2eAgFnSc3d1
stanici	stanice	k1gFnSc3
Eastern	Eastern	k1gMnSc1
Telegraph	Telegraph	k1gMnSc1
Company	Compana	k1gFnSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Direction	Direction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
výrazně	výrazně	k6eAd1
narušena	narušit	k5eAaPmNgFnS
spojenecká	spojenecký	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
v	v	k7c6
Indickém	indický	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
plánoval	plánovat	k5eAaImAgInS
proplout	proplout	k5eAaPmF
Idickým	Idický	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
až	až	k9
k	k	k7c3
nejvýchodnějšímu	východní	k2eAgInSc3d3
cípu	cíp	k1gInSc3
Afriky	Afrika	k1gFnSc2
při	při	k7c6
Sokotře	Sokotra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
této	tento	k3xDgFnSc6
plavbě	plavba	k1gFnSc6
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
narušit	narušit	k5eAaPmF
námořní	námořní	k2eAgInSc1d1
transport	transport	k1gInSc1
Dohody	dohoda	k1gFnSc2
na	na	k7c6
linii	linie	k1gFnSc6
Bombaj-Aden	Bombaj-Adna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1914	#num#	k4
se	se	k3xPyFc4
přiblížil	přiblížit	k5eAaPmAgMnS
k	k	k7c3
ostrovu	ostrov	k1gInSc3
Direction	Direction	k1gInSc4
a	a	k8xC
pokusil	pokusit	k5eAaPmAgMnS
se	se	k3xPyFc4
zopakovat	zopakovat	k5eAaPmF
úspěšný	úspěšný	k2eAgInSc4d1
trik	trik	k1gInSc4
s	s	k7c7
falešným	falešný	k2eAgInSc7d1
komínem	komín	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádka	posádka	k1gFnSc1
ostrova	ostrov	k1gInSc2
se	se	k3xPyFc4
však	však	k9
již	již	k6eAd1
oklamat	oklamat	k5eAaPmF
nenechala	nechat	k5eNaPmAgFnS
a	a	k8xC
stihla	stihnout	k5eAaPmAgFnS
odvysílat	odvysílat	k5eAaPmF
volání	volání	k1gNnSc4
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílání	vysílání	k1gNnSc1
sice	sice	k8xC
Emden	Emden	k1gInSc4
zachytil	zachytit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
odpověď	odpověď	k1gFnSc1
přišla	přijít	k5eAaPmAgFnS
až	až	k9
z	z	k7c2
britského	britský	k2eAgInSc2d1
křižníku	křižník	k1gInSc2
HMS	HMS	kA
Hampshire	Hampshir	k1gInSc5
<g/>
,	,	kIx,
nacházejícího	nacházející	k2eAgMnSc2d1
se	se	k3xPyFc4
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
220	#num#	k4
mil	míle	k1gFnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
byl	být	k5eAaImAgMnS
Müller	Müller	k1gMnSc1
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
2	#num#	k4
až	až	k9
3	#num#	k4
hodiny	hodina	k1gFnPc4
času	čas	k1gInSc2
na	na	k7c6
zničení	zničení	k1gNnSc6
stanice	stanice	k1gFnSc2
a	a	k8xC
následný	následný	k2eAgInSc4d1
únik	únik	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
55	#num#	k4
mil	míle	k1gFnPc2
severně	severně	k6eAd1
v	v	k7c4
tu	ten	k3xDgFnSc4
dobu	doba	k1gFnSc4
proplouval	proplouvat	k5eAaImAgInS
spojenecký	spojenecký	k2eAgInSc1d1
konvoj	konvoj	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gFnPc6
palubách	paluba	k1gFnPc6
byli	být	k5eAaImAgMnP
do	do	k7c2
Evropy	Evropa	k1gFnSc2
transportováni	transportován	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
vysílačky	vysílačka	k1gFnSc2
na	na	k7c4
volání	volání	k1gNnSc4
o	o	k7c4
pomoc	pomoc	k1gFnSc4
nereagovaly	reagovat	k5eNaBmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
doprovodu	doprovod	k1gInSc2
se	se	k3xPyFc4
odpojil	odpojit	k5eAaPmAgInS
pouze	pouze	k6eAd1
australský	australský	k2eAgInSc1d1
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
HMAS	HMAS	kA
Sydney	Sydney	k1gNnSc4
a	a	k8xC
zamířil	zamířit	k5eAaPmAgMnS
na	na	k7c4
pomoc	pomoc	k1gFnSc4
ostrovu	ostrov	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlídky	hlídka	k1gFnSc2
na	na	k7c6
Emdenu	Emden	k1gMnSc6
loď	loď	k1gFnSc1
zpozorovaly	zpozorovat	k5eAaPmAgInP
na	na	k7c6
horizontu	horizont	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
jejich	jejich	k3xOp3gInSc2
úhlu	úhel	k1gInSc2
pohledu	pohled	k1gInSc2
šlo	jít	k5eAaImAgNnS
o	o	k7c4
jednokomínový	jednokomínový	k2eAgInSc4d1
parník	parník	k1gInSc4
a	a	k8xC
domnívaly	domnívat	k5eAaImAgFnP
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
německou	německý	k2eAgFnSc4d1
zásobovací	zásobovací	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Buresk	Buresk	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
křižníku	křižník	k1gInSc3
doplnit	doplnit	k5eAaPmF
palivo	palivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
když	když	k8xS
Sydney	Sydney	k1gNnSc4
změnil	změnit	k5eAaPmAgInS
kurz	kurz	k1gInSc1
a	a	k8xC
bylo	být	k5eAaImAgNnS
vidět	vidět	k5eAaImF
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
celý	celý	k2eAgInSc4d1
profil	profil	k1gInSc4
si	se	k3xPyFc3
uvědomili	uvědomit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
nim	on	k3xPp3gFnPc3
pluje	plout	k5eAaImIp3nS
válečná	válečný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
útěk	útěk	k1gInSc4
však	však	k9
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
v	v	k7c6
tomto	tento	k3xDgInSc6
okamžiku	okamžik	k1gInSc6
pozdě	pozdě	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Mezitím	mezitím	k6eAd1
Emden	Emden	k1gInSc1
provedl	provést	k5eAaPmAgInS
výsadek	výsadek	k1gInSc4
44	#num#	k4
námořníků	námořník	k1gMnPc2
a	a	k8xC
tří	tři	k4xCgMnPc2
důstojníků	důstojník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
obsadili	obsadit	k5eAaPmAgMnP
stanici	stanice	k1gFnSc4
a	a	k8xC
začali	začít	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
ničit	ničit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
přerušit	přerušit	k5eAaPmF
vysílání	vysílání	k1gNnSc2
spojující	spojující	k2eAgFnSc6d1
Austrálii	Austrálie	k1gFnSc6
a	a	k8xC
Indii	Indie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
9.30	9.30	k4
hod	hod	k1gInSc1
<g/>
.	.	kIx.
se	se	k3xPyFc4
ozvala	ozvat	k5eAaPmAgFnS
siréna	siréna	k1gFnSc1
vyzývající	vyzývající	k2eAgFnSc1d1
k	k	k7c3
návratu	návrat	k1gInSc3
na	na	k7c4
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
doběhli	doběhnout	k5eAaPmAgMnP
ke	k	k7c3
člunům	člun	k1gInPc3
<g/>
,	,	kIx,
odplouval	odplouvat	k5eAaImAgMnS
již	již	k6eAd1
Emden	Emden	k2eAgMnSc1d1
pryč	pryč	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
9.40	9.40	k4
nařídil	nařídit	k5eAaPmAgMnS
velitel	velitel	k1gMnSc1
Sydney	Sydney	k1gNnSc2
<g/>
,	,	kIx,
kapitán	kapitán	k1gMnSc1
John	John	k1gMnSc1
Glossop	Glossop	k1gInSc4
<g/>
,	,	kIx,
zahájit	zahájit	k5eAaPmF
palbu	palba	k1gFnSc4
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
9	#num#	k4
400	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c4
což	což	k3yQnSc4,k3yRnSc4
Němci	Němec	k1gMnPc1
okamžitě	okamžitě	k6eAd1
odpověděli	odpovědět	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sydney	Sydney	k1gNnSc2
byl	být	k5eAaImAgMnS
vyzbrojen	vyzbrojit	k5eAaPmNgMnS
děly	dít	k5eAaBmAgFnP,k5eAaImAgFnP
ráže	ráže	k1gFnPc1
152	#num#	k4
mm	mm	kA
a	a	k8xC
měl	mít	k5eAaImAgInS
trojnásobnou	trojnásobný	k2eAgFnSc4d1
palebnou	palebný	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádka	posádka	k1gFnSc1
Emdenu	Emden	k1gInSc2
nebyla	být	k5eNaImAgFnS
schopna	schopen	k2eAgFnSc1d1
plně	plně	k6eAd1
využít	využít	k5eAaPmF
své	svůj	k3xOyFgNnSc4
dělostřelectvo	dělostřelectvo	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
část	část	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
obsluhy	obsluha	k1gFnSc2
byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
výsadku	výsadek	k1gInSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emden	Emden	k2eAgInSc4d1
za	za	k7c4
20	#num#	k4
minut	minuta	k1gFnPc2
přestal	přestat	k5eAaPmAgMnS
odpovídat	odpovídat	k5eAaImF
na	na	k7c4
nepřátelskou	přátelský	k2eNgFnSc4d1
palbu	palba	k1gFnSc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
dvou	dva	k4xCgFnPc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
zničeny	zničit	k5eAaPmNgInP
všechny	všechen	k3xTgMnPc4
děla	dělo	k1gNnPc1
<g/>
,	,	kIx,
torpédomety	torpédomet	k1gInPc1
<g/>
,	,	kIx,
dva	dva	k4xCgInPc4
komíny	komín	k1gInPc4
a	a	k8xC
kormidlo	kormidlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
11.00	11.00	k4
přestal	přestat	k5eAaPmAgInS
pálit	pálit	k5eAaImF
a	a	k8xC
kapitán	kapitán	k1gMnSc1
Müller	Müller	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
11.15	11.15	k4
snažil	snažit	k5eAaImAgMnS
dostat	dostat	k5eAaPmF
loď	loď	k1gFnSc4
na	na	k7c4
břeh	břeh	k1gInSc4
ostrova	ostrov	k1gInSc2
North	North	k1gInSc1
Keeling	Keeling	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
sedla	sedlo	k1gNnSc2
na	na	k7c4
mělčinu	mělčina	k1gFnSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
obdržela	obdržet	k5eAaPmAgFnS
přes	přes	k7c4
100	#num#	k4
zásahů	zásah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sydney	Sydney	k1gNnPc7
pokračoval	pokračovat	k5eAaImAgMnS
v	v	k7c6
palbě	palba	k1gFnSc6
ještě	ještě	k6eAd1
pět	pět	k4xCc1
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
jeho	jeho	k3xOp3gFnSc1
posádka	posádka	k1gFnSc1
nezpozorovala	zpozorovat	k5eNaPmAgFnS
na	na	k7c6
obzoru	obzor	k1gInSc6
kouř	kouř	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glossop	Glossop	k1gInSc4
mu	on	k3xPp3gMnSc3
zamířil	zamířit	k5eAaPmAgMnS
vstříc	vstříc	k6eAd1
v	v	k7c6
domnění	domnění	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
německý	německý	k2eAgInSc1d1
křižník	křižník	k1gInSc1
SMS	SMS	kA
Königsberg	Königsberg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázalo	ukázat	k5eAaPmAgNnS
se	se	k3xPyFc4
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
Buresk	Buresk	k1gInSc4
<g/>
,	,	kIx,
zásobovací	zásobovací	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Emdenu	Emden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
varovném	varovný	k2eAgInSc6d1
výstřelu	výstřel	k1gInSc6
16	#num#	k4
<g/>
členná	členný	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
svou	svůj	k3xOyFgFnSc4
loď	loď	k1gFnSc1
potopila	potopit	k5eAaPmAgFnS
a	a	k8xC
vzdala	vzdát	k5eAaPmAgFnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sydney	Sydney	k1gNnPc7
se	se	k3xPyFc4
poté	poté	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
k	k	k7c3
Emdenu	Emden	k1gInSc3
a	a	k8xC
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
palbě	palba	k1gFnSc6
do	do	k7c2
hořícího	hořící	k2eAgInSc2d1
vraku	vrak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
minutách	minuta	k1gFnPc6
posádka	posádka	k1gFnSc1
stáhla	stáhnout	k5eAaPmAgFnS
válečnou	válečný	k2eAgFnSc4d1
zástavu	zástava	k1gFnSc4
a	a	k8xC
vyvěsila	vyvěsit	k5eAaPmAgFnS
bílou	bílý	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
na	na	k7c6
znamení	znamení	k1gNnSc6
kapitulace	kapitulace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sydney	Sydney	k1gNnPc7
poslal	poslat	k5eAaPmAgMnS
k	k	k7c3
vraku	vrak	k1gInSc3
záchranný	záchranný	k2eAgInSc1d1
člun	člun	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
vlnobití	vlnobití	k1gNnSc3
na	na	k7c6
útesech	útes	k1gInPc6
se	se	k3xPyFc4
mu	on	k3xPp3gInSc3
ke	k	k7c3
ztroskotané	ztroskotaný	k2eAgFnSc3d1
lodi	loď	k1gFnSc3
přiblížit	přiblížit	k5eAaPmF
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
Sydney	Sydney	k1gNnSc4
odplul	odplout	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zkontroloval	zkontrolovat	k5eAaPmAgMnS
radiovou	radiový	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vraku	vrak	k1gInSc3
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
druhý	druhý	k4xOgInSc1
den	den	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zachránil	zachránit	k5eAaPmAgInS
případné	případný	k2eAgMnPc4d1
přeživší	přeživší	k2eAgMnPc4d1
námořníky	námořník	k1gMnPc4
z	z	k7c2
Emdenu	Emden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
134	#num#	k4
Němců	Němec	k1gMnPc2
<g/>
,	,	kIx,
65	#num#	k4
bylo	být	k5eAaImAgNnS
raněných	raněný	k1gMnPc2
<g/>
,	,	kIx,
ztráty	ztráta	k1gFnPc1
Australanů	Australan	k1gMnPc2
činily	činit	k5eAaImAgFnP
4	#num#	k4
mrtvé	mrtvý	k2eAgFnSc2d1
a	a	k8xC
17	#num#	k4
raněných	raněný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Křižníku	křižník	k1gInSc2
Sydney	Sydney	k1gNnSc2
se	se	k3xPyFc4
však	však	k9
nepodařilo	podařit	k5eNaPmAgNnS
zajmout	zajmout	k5eAaPmF
celou	celý	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
Emdenu	Emden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oddíl	oddíl	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vedl	vést	k5eAaImAgInS
útok	útok	k1gInSc4
na	na	k7c4
telegrafní	telegrafní	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
boje	boj	k1gInSc2
zmocnil	zmocnit	k5eAaPmAgMnS
škuneru	škuner	k1gInSc2
Ayesha	Ayesh	k1gMnSc2
o	o	k7c6
výtlaku	výtlak	k1gInSc6
123	#num#	k4
tun	tuna	k1gFnPc2
a	a	k8xC
odplul	odplout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velel	velet	k5eAaImAgMnS
jim	on	k3xPp3gMnPc3
námořní	námořní	k2eAgMnSc1d1
poručík	poručík	k1gMnSc1
von	von	k1gInSc4
Mücke	Mück	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
komplikované	komplikovaný	k2eAgFnSc6d1
plavbě	plavba	k1gFnSc6
bez	bez	k7c2
navigačních	navigační	k2eAgFnPc2d1
pomůcek	pomůcka	k1gFnPc2
a	a	k8xC
map	mapa	k1gFnPc2
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
dnešního	dnešní	k2eAgInSc2d1
Jemenu	Jemen	k1gInSc2
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgFnSc2d1
součásti	součást	k1gFnSc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
německým	německý	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
a	a	k8xC
přes	přes	k7c4
Istanbul	Istanbul	k1gInSc4
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
se	se	k3xPyFc4
dost	dost	k6eAd1
podstatně	podstatně	k6eAd1
rozcházejí	rozcházet	k5eAaImIp3nP
v	v	k7c6
údajích	údaj	k1gInPc6
<g/>
,	,	kIx,
kolik	kolik	k4yQc4,k4yIc4,k4yRc4
lodí	loď	k1gFnPc2
Emden	Emdna	k1gFnPc2
zničil	zničit	k5eAaPmAgInS
<g/>
,	,	kIx,
nebo	nebo	k8xC
zajal	zajmout	k5eAaPmAgMnS
<g/>
,	,	kIx,
podle	podle	k7c2
nejspolehlivějších	spolehlivý	k2eAgInPc2d3
německých	německý	k2eAgInPc2d1
údajů	údaj	k1gInPc2
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
25	#num#	k4
obchodních	obchodní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
dvě	dva	k4xCgFnPc4
válečné	válečná	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poručík	poručík	k1gMnSc1
von	von	k1gInSc4
Mücke	Mück	k1gInSc2
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
císařem	císař	k1gMnSc7
a	a	k8xC
mimořádně	mimořádně	k6eAd1
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c4
fregatního	fregatní	k2eAgMnSc4d1
kapitána	kapitán	k1gMnSc4
<g/>
,	,	kIx,
von	von	k1gInSc1
Müller	Müller	k1gMnSc1
zkázu	zkáza	k1gFnSc4
Emden	Emdna	k1gFnPc2
přežil	přežít	k5eAaPmAgMnS
a	a	k8xC
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
do	do	k7c2
zajetí	zajetí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
něho	on	k3xPp3gInSc2
byl	být	k5eAaImAgMnS
v	v	k7c6
nepřítomnosti	nepřítomnost	k1gFnSc6
císařem	císař	k1gMnSc7
mimořádně	mimořádně	k6eAd1
povýšen	povýšen	k2eAgMnSc1d1
na	na	k7c4
námořního	námořní	k2eAgMnSc4d1
kapitána	kapitán	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
na	na	k7c4
následky	následek	k1gInPc4
malárie	malárie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kapitán	kapitán	k1gMnSc1
von	von	k1gInSc4
Müller	Müller	k1gInSc4
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
činnosti	činnost	k1gFnSc2
jednal	jednat	k5eAaImAgMnS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
Haagskými	haagský	k2eAgFnPc7d1
konvencemi	konvence	k1gFnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
respektovali	respektovat	k5eAaImAgMnP
i	i	k8xC
jeho	jeho	k3xOp3gMnPc1
protivníci	protivník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
šetřit	šetřit	k5eAaImF
životy	život	k1gInPc4
civilních	civilní	k2eAgMnPc2d1
námořníků	námořník	k1gMnPc2
a	a	k8xC
jednat	jednat	k5eAaImF
s	s	k7c7
nimi	on	k3xPp3gMnPc7
lidsky	lidsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
vrak	vrak	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
byl	být	k5eAaImAgInS
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
cílem	cíl	k1gInSc7
hledačů	hledač	k1gMnPc2
pokladů	poklad	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
rozšířené	rozšířený	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
na	na	k7c6
palubě	paluba	k1gFnSc6
válečnou	válečný	k2eAgFnSc4d1
pokladnu	pokladna	k1gFnSc4
s	s	k7c7
2	#num#	k4
500	#num#	k4
000	#num#	k4
marek	marka	k1gFnPc2
ve	v	k7c6
zlatě	zlato	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1
přímé	přímý	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
způsobené	způsobený	k2eAgFnPc1d1
křižníkem	křižník	k1gInSc7
Emden	Emden	k1gInSc4
němečtí	německý	k2eAgMnPc1d1
ekonomové	ekonom	k1gMnPc1
vyčíslili	vyčíslit	k5eAaPmAgMnP
na	na	k7c4
15	#num#	k4
milionů	milion	k4xCgInPc2
předválečných	předválečný	k2eAgFnPc2d1
marek	marka	k1gFnPc2
<g/>
,	,	kIx,
nepřímé	přímý	k2eNgFnPc1d1
ztráty	ztráta	k1gFnPc1
způsobené	způsobený	k2eAgFnPc1d1
narušením	narušení	k1gNnSc7
civilní	civilní	k2eAgFnSc2d1
plavby	plavba	k1gFnSc2
jsou	být	k5eAaImIp3nP
obtížně	obtížně	k6eAd1
vyčíslitelné	vyčíslitelný	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
pravděpodobně	pravděpodobně	k6eAd1
ještě	ještě	k6eAd1
vyšší	vysoký	k2eAgFnPc1d2
než	než	k8xS
přímé	přímý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
SMS	SMS	kA
Emden	Emden	k1gInSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
SMS	SMS	kA
Emden	Emdno	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4137726-6	4137726-6	k4
</s>
