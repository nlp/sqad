<s>
Samhain	Samhain	k1gInSc4	Samhain
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
"	"	kIx"	"
<g/>
sauə	sauə	k?	sauə
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ze	z	k7c2	z
staroirského	staroirský	k2eAgMnSc2d1	staroirský
samain	samain	k1gInSc1	samain
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
keltského	keltský	k2eAgInSc2d1	keltský
svátku	svátek	k1gInSc2	svátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
keltském	keltský	k2eAgInSc6d1	keltský
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
tímto	tento	k3xDgNnSc7	tento
slovem	slovo	k1gNnSc7	slovo
označoval	označovat	k5eAaImAgInS	označovat
také	také	k6eAd1	také
měsíc	měsíc	k1gInSc1	měsíc
listopad	listopad	k1gInSc1	listopad
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skotské	skotský	k2eAgFnSc6d1	skotská
gaelštině	gaelština	k1gFnSc6	gaelština
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
píše	psát	k5eAaImIp3nS	psát
jako	jako	k9	jako
Samhainn	Samhainn	k1gNnSc1	Samhainn
nebo	nebo	k8xC	nebo
Samhuinn	Samhuinn	k1gNnSc1	Samhuinn
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
listopad	listopad	k1gInSc1	listopad
jako	jako	k8xS	jako
an	an	k?	an
t-Samhain	t-Samhain	k1gInSc1	t-Samhain
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
galském	galský	k2eAgInSc6d1	galský
kalendáři	kalendář	k1gInSc6	kalendář
z	z	k7c2	z
Coligny	Coligna	k1gFnSc2	Coligna
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	být	k5eAaImIp3nS	být
Samhain	Samhain	k1gMnSc1	Samhain
uveden	uvést	k5eAaPmNgMnS	uvést
jako	jako	k9	jako
Samonios	Samonios	k1gMnSc1	Samonios
<g/>
.	.	kIx.	.
</s>
<s>
Samhain	Samhain	k1gInSc1	Samhain
byl	být	k5eAaImAgInS	být
hranicí	hranice	k1gFnSc7	hranice
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hlavními	hlavní	k2eAgFnPc7d1	hlavní
periodami	perioda	k1gFnPc7	perioda
(	(	kIx(	(
<g/>
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
zimou	zima	k1gFnSc7	zima
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Kelty	Kelt	k1gMnPc4	Kelt
byl	být	k5eAaImAgMnS	být
začátkem	začátek	k1gInSc7	začátek
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vnímán	vnímat	k5eAaImNgInS	vnímat
také	také	k9	také
jako	jako	k8xS	jako
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stírá	stírat	k5eAaImIp3nS	stírat
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
světem	svět	k1gInSc7	svět
živých	živý	k1gMnPc2	živý
a	a	k8xC	a
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
duše	duše	k1gFnSc1	duše
zesnulých	zesnulý	k1gMnPc2	zesnulý
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
čas	čas	k1gInSc4	čas
vracejí	vracet	k5eAaImIp3nP	vracet
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
živí	živý	k1gMnPc1	živý
mohou	moct	k5eAaImIp3nP	moct
navštívit	navštívit	k5eAaPmF	navštívit
podsvětí	podsvětí	k1gNnSc4	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
keltská	keltský	k2eAgFnSc1d1	keltská
území	území	k1gNnSc6	území
dobyta	dobýt	k5eAaPmNgFnS	dobýt
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
prolínat	prolínat	k5eAaImF	prolínat
keltské	keltský	k2eAgInPc4d1	keltský
a	a	k8xC	a
římské	římský	k2eAgInPc4d1	římský
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
ke	k	k7c3	k
svátku	svátek	k1gInSc3	svátek
Samhain	Samhaina	k1gFnPc2	Samhaina
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
slavit	slavit	k5eAaImF	slavit
i	i	k9	i
konec	konec	k1gInSc4	konec
úrody	úroda	k1gFnSc2	úroda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
symbolům	symbol	k1gInPc3	symbol
Samhainu	Samhain	k1gInSc2	Samhain
přibylo	přibýt	k5eAaPmAgNnS	přibýt
obilí	obilí	k1gNnSc1	obilí
a	a	k8xC	a
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	Svátek	k1gMnSc1	Svátek
Samhain	Samhain	k1gMnSc1	Samhain
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
v	v	k7c4	v
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
"	"	kIx"	"
<g/>
čtvrtící	čtvrtící	k2eAgInSc4d1	čtvrtící
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
cross-quarter	crossuarter	k1gInSc1	cross-quarter
day	day	k?	day
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
den	den	k1gInSc4	den
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
mezi	mezi	k7c7	mezi
rovnodenností	rovnodennost	k1gFnSc7	rovnodennost
(	(	kIx(	(
<g/>
den	den	k1gInSc1	den
stejně	stejně	k6eAd1	stejně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
jako	jako	k8xC	jako
noc	noc	k1gFnSc1	noc
<g/>
)	)	kIx)	)
a	a	k8xC	a
slunovratem	slunovrat	k1gInSc7	slunovrat
(	(	kIx(	(
<g/>
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
nejkratší	krátký	k2eAgInSc1d3	nejkratší
den	den	k1gInSc1	den
a	a	k8xC	a
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
noc	noc	k1gFnSc1	noc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
čtvrtícím	čtvrtící	k2eAgInSc7d1	čtvrtící
dnem	den	k1gInSc7	den
jsou	být	k5eAaImIp3nP	být
Hromnice	Hromnice	k1gFnPc4	Hromnice
(	(	kIx(	(
<g/>
v	v	k7c6	v
keltském	keltský	k2eAgInSc6d1	keltský
kalendáři	kalendář	k1gInSc6	kalendář
svátek	svátek	k1gInSc1	svátek
Imbolc	Imbolc	k1gInSc1	Imbolc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samhain	Samhain	k1gInSc1	Samhain
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
jako	jako	k9	jako
protipól	protipól	k1gInSc1	protipól
Beltainu	Beltain	k1gInSc2	Beltain
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
po	po	k7c4	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
například	například	k6eAd1	například
prostírat	prostírat	k5eAaImF	prostírat
u	u	k7c2	u
večeře	večeře	k1gFnSc2	večeře
i	i	k9	i
pro	pro	k7c4	pro
zesnulé	zesnulý	k2eAgMnPc4d1	zesnulý
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
čekalo	čekat	k5eAaImAgNnS	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijdou	přijít	k5eAaPmIp3nP	přijít
rodinu	rodina	k1gFnSc4	rodina
navštívit	navštívit	k5eAaPmF	navštívit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vystavovat	vystavovat	k5eAaImF	vystavovat
za	za	k7c7	za
oknem	okno	k1gNnSc7	okno
zapálenou	zapálený	k2eAgFnSc4d1	zapálená
svíčku	svíčka	k1gFnSc4	svíčka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
vyřezané	vyřezaný	k2eAgFnSc6d1	vyřezaná
řepě	řepa	k1gFnSc6	řepa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
bloudícím	bloudící	k2eAgFnPc3d1	bloudící
duším	duše	k1gFnPc3	duše
mrtvých	mrtvý	k1gMnPc2	mrtvý
posvítit	posvítit	k5eAaPmF	posvítit
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
také	také	k9	také
převlékali	převlékat	k5eAaImAgMnP	převlékat
do	do	k7c2	do
starých	starý	k2eAgInPc2d1	starý
cárů	cár	k1gInPc2	cár
a	a	k8xC	a
malovali	malovat	k5eAaImAgMnP	malovat
si	se	k3xPyFc3	se
obličeje	obličej	k1gInPc4	obličej
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
uchránili	uchránit	k5eAaPmAgMnP	uchránit
před	před	k7c7	před
zlými	zlý	k2eAgMnPc7d1	zlý
duchy	duch	k1gMnPc7	duch
<g/>
.	.	kIx.	.
</s>
<s>
Samhain	Samhain	k2eAgInSc1d1	Samhain
blízký	blízký	k2eAgInSc1d1	blízký
jeho	jeho	k3xOp3gFnSc6	jeho
původní	původní	k2eAgFnSc6d1	původní
keltské	keltský	k2eAgFnSc6d1	keltská
podobě	podoba	k1gFnSc6	podoba
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slaví	slavit	k5eAaImIp3nP	slavit
příslušníci	příslušník	k1gMnPc1	příslušník
některých	některý	k3yIgNnPc2	některý
novopohanských	novopohanský	k2eAgNnPc2d1	novopohanské
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
keltského	keltský	k2eAgNnSc2d1	keltské
novopohanství	novopohanství	k1gNnSc2	novopohanství
a	a	k8xC	a
Wiccy	Wicca	k1gFnSc2	Wicca
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
wiccanského	wiccanský	k2eAgMnSc2d1	wiccanský
Boha	bůh	k1gMnSc2	bůh
se	se	k3xPyFc4	se
o	o	k7c6	o
Samhainu	Samhain	k1gInSc6	Samhain
stává	stávat	k5eAaImIp3nS	stávat
Pán	pán	k1gMnSc1	pán
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
usedá	usedat	k5eAaImIp3nS	usedat
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
podsvětí	podsvětí	k1gNnSc6	podsvětí
jako	jako	k8xC	jako
vládce	vládce	k1gMnSc2	vládce
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
boku	bok	k1gInSc6	bok
sedí	sedit	k5eAaImIp3nP	sedit
Bohyně	bohyně	k1gFnPc1	bohyně
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
tradic	tradice	k1gFnPc2	tradice
spojí	spojit	k5eAaPmIp3nS	spojit
a	a	k8xC	a
počne	počnout	k5eAaPmIp3nS	počnout
syna	syn	k1gMnSc4	syn
(	(	kIx(	(
<g/>
nového	nový	k2eAgMnSc2d1	nový
Boha	bůh	k1gMnSc2	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Bohyně	bohyně	k1gFnSc1	bohyně
a	a	k8xC	a
mladý	mladý	k2eAgMnSc1d1	mladý
Bůh	bůh	k1gMnSc1	bůh
se	se	k3xPyFc4	se
o	o	k7c4	o
Imbolcu	Imbolca	k1gFnSc4	Imbolca
vracejí	vracet	k5eAaImIp3nP	vracet
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
starý	starý	k2eAgMnSc1d1	starý
Bůh	bůh	k1gMnSc1	bůh
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
podsvětní	podsvětní	k2eAgFnSc6d1	podsvětní
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
pomalu	pomalu	k6eAd1	pomalu
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
ho	on	k3xPp3gMnSc4	on
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
jeho	jeho	k3xOp3gInSc1	jeho
vlastní	vlastní	k2eAgFnPc1d1	vlastní
tma	tma	k1gFnSc1	tma
a	a	k8xC	a
nevědomí	vědomý	k2eNgMnPc1d1	nevědomý
<g/>
.	.	kIx.	.
</s>
