<s>
Hemocyanin	Hemocyanin	k2eAgMnSc1d1	Hemocyanin
je	být	k5eAaImIp3nS	být
dýchací	dýchací	k2eAgInSc1d1	dýchací
(	(	kIx(	(
<g/>
respirační	respirační	k2eAgInSc1d1	respirační
<g/>
)	)	kIx)	)
protein	protein	k1gInSc1	protein
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vázat	vázat	k5eAaImF	vázat
molekulu	molekula	k1gFnSc4	molekula
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
metaloproteiny	metaloprotein	k1gInPc7	metaloprotein
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
molekule	molekula	k1gFnSc6	molekula
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
atomy	atom	k1gInPc4	atom
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Hemocyanin	Hemocyanin	k2eAgMnSc1d1	Hemocyanin
je	být	k5eAaImIp3nS	být
obsažen	obsažen	k2eAgInSc1d1	obsažen
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
<g/>
)	)	kIx)	)
v	v	k7c6	v
hemolymfě	hemolymf	k1gInSc6	hemolymf
měkkýšů	měkkýš	k1gMnPc2	měkkýš
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
vodních	vodní	k2eAgMnPc2d1	vodní
plžů	plž	k1gMnPc2	plž
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
okružákovití	okružákovitý	k2eAgMnPc1d1	okružákovitý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hemolymfě	hemolymf	k1gInSc6	hemolymf
krevní	krevní	k2eAgNnSc1d1	krevní
barvivo	barvivo	k1gNnSc1	barvivo
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
členovců	členovec	k1gMnPc2	členovec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
krabů	krab	k1gInPc2	krab
<g/>
.	.	kIx.	.
</s>
<s>
Oxidovaná	oxidovaný	k2eAgFnSc1d1	oxidovaná
forma	forma	k1gFnSc1	forma
(	(	kIx(	(
<g/>
CuII	CuII	k1gFnSc1	CuII
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
neoxidovaná	oxidovaný	k2eNgFnSc1d1	oxidovaný
forma	forma	k1gFnSc1	forma
(	(	kIx(	(
<g/>
CuI	CuI	k1gFnSc1	CuI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vázat	vázat	k5eAaImF	vázat
jen	jen	k9	jen
asi	asi	k9	asi
70	[number]	k4	70
ml	ml	kA	ml
kyslíku	kyslík	k1gInSc2	kyslík
na	na	k7c4	na
1	[number]	k4	1
litr	litr	k1gInSc4	litr
hemolymfy	hemolymf	k1gInPc7	hemolymf
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
molekula	molekula	k1gFnSc1	molekula
O2	O2	k1gFnSc2	O2
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
2	[number]	k4	2
atomy	atom	k1gInPc7	atom
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Hemoglobin	hemoglobin	k1gInSc1	hemoglobin
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vázat	vázat	k5eAaImF	vázat
až	až	k9	až
200	[number]	k4	200
ml	ml	kA	ml
na	na	k7c4	na
1	[number]	k4	1
litr	litr	k1gInSc4	litr
hemolymfy	hemolymf	k1gInPc1	hemolymf
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
mědi	měď	k1gFnSc2	měď
hemocyaninu	hemocyanin	k2eAgFnSc4d1	hemocyanin
jsou	být	k5eAaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
jako	jako	k8xS	jako
prostetická	prostetický	k2eAgFnSc1d1	prostetická
skupina	skupina	k1gFnSc1	skupina
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
histidinových	histidinový	k2eAgInPc2d1	histidinový
peptidů	peptid	k1gInPc2	peptid
<g/>
.	.	kIx.	.
</s>
<s>
Hemocyanin	Hemocyanin	k2eAgMnSc1d1	Hemocyanin
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
samostatných	samostatný	k2eAgFnPc2d1	samostatná
proteinových	proteinový	k2eAgFnPc2d1	proteinová
podjednotek	podjednotka	k1gFnPc2	podjednotka
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2	[number]	k4	2
atomy	atom	k1gInPc7	atom
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vázat	vázat	k5eAaImF	vázat
jednu	jeden	k4xCgFnSc4	jeden
molekulu	molekula	k1gFnSc4	molekula
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
podjednotka	podjednotka	k1gFnSc1	podjednotka
má	mít	k5eAaImIp3nS	mít
atomovou	atomový	k2eAgFnSc4d1	atomová
hmotnost	hmotnost	k1gFnSc4	hmotnost
asi	asi	k9	asi
75	[number]	k4	75
kilodaltonů	kilodalton	k1gInPc2	kilodalton
(	(	kIx(	(
<g/>
kDa	kDa	k?	kDa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podjednotky	podjednotka	k1gFnPc1	podjednotka
jsou	být	k5eAaImIp3nP	být
navázány	navázat	k5eAaPmNgFnP	navázat
podle	podle	k7c2	podle
živočišného	živočišný	k2eAgInSc2d1	živočišný
druhu	druh	k1gInSc2	druh
buď	buď	k8xC	buď
jako	jako	k9	jako
dimery	dimera	k1gFnPc1	dimera
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
hexamery	hexamer	k1gInPc1	hexamer
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
řetězit	řetězit	k5eAaImF	řetězit
a	a	k8xC	a
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
tak	tak	k6eAd1	tak
atomovou	atomový	k2eAgFnSc4d1	atomová
hmotnost	hmotnost	k1gFnSc4	hmotnost
1	[number]	k4	1
500	[number]	k4	500
kDa	kDa	k?	kDa
<g/>
.	.	kIx.	.
</s>
<s>
Podjednotky	podjednotka	k1gFnPc1	podjednotka
jsou	být	k5eAaImIp3nP	být
homogenní	homogenní	k2eAgFnPc1d1	homogenní
nebo	nebo	k8xC	nebo
heterogenní	heterogenní	k2eAgFnPc1d1	heterogenní
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
při	při	k7c6	při
získávání	získávání	k1gNnSc6	získávání
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
:	:	kIx,	:
Hemocyanin	Hemocyanina	k1gFnPc2	Hemocyanina
z	z	k7c2	z
plžů	plž	k1gMnPc2	plž
nadčeledi	nadčeleď	k1gFnSc2	nadčeleď
Fissurelloidea	Fissurelloidea	k1gMnSc1	Fissurelloidea
se	se	k3xPyFc4	se
naváže	navázat	k5eAaPmIp3nS	navázat
na	na	k7c4	na
protein	protein	k1gInSc4	protein
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
našeho	náš	k3xOp1gInSc2	náš
zájmu	zájem	k1gInSc2	zájem
a	a	k8xC	a
vpraví	vpravit	k5eAaPmIp3nS	vpravit
se	se	k3xPyFc4	se
laboratornímu	laboratorní	k2eAgMnSc3d1	laboratorní
živočichovi	živočich	k1gMnSc3	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
protilátku	protilátka	k1gFnSc4	protilátka
anti-ubiquitin	antibiquitina	k1gFnPc2	anti-ubiquitina
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
stimulant	stimulant	k1gInSc4	stimulant
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
živočich	živočich	k1gMnSc1	živočich
produkuje	produkovat	k5eAaImIp3nS	produkovat
protilátky	protilátka	k1gFnPc4	protilátka
proti	proti	k7c3	proti
tomu	ten	k3xDgInSc3	ten
určitému	určitý	k2eAgInSc3d1	určitý
proteinu	protein	k1gInSc3	protein
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
izolujeme	izolovat	k5eAaBmIp1nP	izolovat
monoklonální	monoklonální	k2eAgFnPc4d1	monoklonální
protilátky	protilátka	k1gFnPc4	protilátka
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
:	:	kIx,	:
Cévní	cévní	k2eAgFnSc1d1	cévní
a	a	k8xC	a
vylučovací	vylučovací	k2eAgFnSc1d1	vylučovací
soustava	soustava	k1gFnSc1	soustava
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hemocyanin	Hemocyanina	k1gFnPc2	Hemocyanina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Molecular	Molecular	k1gMnSc1	Molecular
Evolution	Evolution	k1gInSc1	Evolution
of	of	k?	of
Arthropod	Arthropod	k1gInSc1	Arthropod
&	&	k?	&
Molluscan	Molluscan	k1gInSc1	Molluscan
Hemocyanin	Hemocyanina	k1gFnPc2	Hemocyanina
</s>
