<s>
Patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
metaloproteiny	metaloprotein	k1gInPc7	metaloprotein
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
molekule	molekula	k1gFnSc6	molekula
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
atomy	atom	k1gInPc4	atom
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
