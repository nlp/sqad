<s>
Newton	Newton	k1gMnSc1	Newton
se	se	k3xPyFc4	se
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
přel	přít	k5eAaImAgMnS	přít
s	s	k7c7	s
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Leibnizem	Leibniz	k1gMnSc7	Leibniz
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
objevil	objevit	k5eAaPmAgMnS	objevit
kalkulus	kalkulus	k1gMnSc1	kalkulus
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
