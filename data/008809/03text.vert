<p>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Výborná	výborný	k2eAgFnSc1d1	výborná
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1969	[number]	k4	1969
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
moderátorka	moderátorka	k1gFnSc1	moderátorka
a	a	k8xC	a
novinářka	novinářka	k1gFnSc1	novinářka
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
knihy	kniha	k1gFnSc2	kniha
Fit	fit	k2eAgFnSc1d1	fit
maminka	maminka	k1gFnSc1	maminka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
profese	profese	k1gFnSc1	profese
==	==	k?	==
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
jako	jako	k8xS	jako
studentka	studentka	k1gFnSc1	studentka
Fakulty	fakulta	k1gFnSc2	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
UK	UK	kA	UK
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
redakci	redakce	k1gFnSc6	redakce
televizních	televizní	k2eAgFnPc2d1	televizní
novin	novina	k1gFnPc2	novina
v	v	k7c6	v
ČT	ČT	kA	ČT
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
moderovala	moderovat	k5eAaBmAgFnS	moderovat
na	na	k7c6	na
</s>
</p>
<p>
<s>
ČT1	ČT1	k4	ČT1
hudební	hudební	k2eAgInSc1d1	hudební
pořad	pořad	k1gInSc1	pořad
Bago	Bago	k?	Bago
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
začínající	začínající	k2eAgFnSc1d1	začínající
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
moderovala	moderovat	k5eAaBmAgFnS	moderovat
pořady	pořad	k1gInPc4	pořad
Hejbni	Hejbni	k1gMnPc2	Hejbni
kostrou	kostra	k1gFnSc7	kostra
<g/>
,	,	kIx,	,
Tutovka	tutovka	k1gFnSc1	tutovka
<g/>
,	,	kIx,	,
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
kometa	kometa	k1gFnSc1	kometa
<g/>
,	,	kIx,	,
Rozhovory	rozhovor	k1gInPc1	rozhovor
s	s	k7c7	s
V.I.	V.I.	k1gFnSc7	V.I.
<g/>
P.	P.	kA	P.
osobnostmi	osobnost	k1gFnPc7	osobnost
a	a	k8xC	a
především	především	k9	především
Snídani	snídaně	k1gFnSc4	snídaně
s	s	k7c7	s
Novou	Nová	k1gFnSc7	Nová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zároveň	zároveň	k6eAd1	zároveň
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
moderuje	moderovat	k5eAaBmIp3nS	moderovat
na	na	k7c6	na
českých	český	k2eAgNnPc6d1	české
rozhlasových	rozhlasový	k2eAgNnPc6d1	rozhlasové
médiích	médium	k1gNnPc6	médium
−	−	k?	−
vyzkoušela	vyzkoušet	k5eAaPmAgFnS	vyzkoušet
rádio	rádio	k1gNnSc4	rádio
Bonton	bonton	k1gInSc1	bonton
<g/>
,	,	kIx,	,
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
usadila	usadit	k5eAaPmAgFnS	usadit
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
Frekvence	frekvence	k1gFnSc2	frekvence
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
moderovala	moderovat	k5eAaBmAgFnS	moderovat
pořad	pořad	k1gInSc4	pořad
Kavárna	kavárna	k1gFnSc1	kavárna
u	u	k7c2	u
Lucie	Lucie	k1gFnSc2	Lucie
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
talkshow	talkshow	k?	talkshow
Kočkovaná	kočkovaný	k2eAgFnSc1d1	kočkovaná
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
také	také	k9	také
nedělní	nedělní	k2eAgFnPc1d1	nedělní
podvečerní	podvečerní	k2eAgFnPc1d1	podvečerní
talkshow	talkshow	k?	talkshow
Osobnosti	osobnost	k1gFnPc1	osobnost
na	na	k7c6	na
Frekvenci	frekvence	k1gFnSc6	frekvence
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Jiřího	Jiří	k1gMnSc2	Jiří
Grossmana	Grossman	k1gMnSc2	Grossman
v	v	k7c6	v
talkshow	talkshow	k?	talkshow
Meloucháři	melouchář	k1gMnPc1	melouchář
s	s	k7c7	s
Martinou	Martina	k1gFnSc7	Martina
Kociánovou	Kociánová	k1gFnSc7	Kociánová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Frekvence	frekvence	k1gFnSc2	frekvence
1	[number]	k4	1
odešla	odejít	k5eAaPmAgFnS	odejít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
znovu	znovu	k6eAd1	znovu
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
s	s	k7c7	s
pořady	pořad	k1gInPc7	pořad
Když	když	k8xS	když
se	se	k3xPyFc4	se
řekne	říct	k5eAaPmIp3nS	říct
profese	profese	k1gFnSc1	profese
a	a	k8xC	a
Český	český	k2eAgMnSc1d1	český
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
moderovala	moderovat	k5eAaBmAgFnS	moderovat
Koncerty	koncert	k1gInPc4	koncert
Lidí	člověk	k1gMnPc2	člověk
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
z	z	k7c2	z
Velehradu	Velehrad	k1gInSc2	Velehrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stala	stát	k5eAaPmAgFnS	stát
šéfredaktorkou	šéfredaktorka	k1gFnSc7	šéfredaktorka
týdeníku	týdeník	k1gInSc2	týdeník
Story	story	k1gFnSc2	story
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
Prima	prima	k6eAd1	prima
jako	jako	k8xS	jako
moderátorka	moderátorka	k1gFnSc1	moderátorka
pořadu	pořad	k1gInSc2	pořad
Extra	extra	k2eAgInSc2d1	extra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
do	do	k7c2	do
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
moderátorkou	moderátorka	k1gFnSc7	moderátorka
dopoledního	dopolední	k2eAgNnSc2d1	dopolední
vysílání	vysílání	k1gNnSc2	vysílání
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
1	[number]	k4	1
Radiožurnálu	radiožurnál	k1gInSc2	radiožurnál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvádí	uvádět	k5eAaImIp3nS	uvádět
dopolední	dopolední	k2eAgInSc1d1	dopolední
blok	blok	k1gInSc1	blok
mezi	mezi	k7c7	mezi
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
hodinou	hodina	k1gFnSc7	hodina
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
den	den	k1gInSc4	den
také	také	k9	také
s	s	k7c7	s
hostem	host	k1gMnSc7	host
po	po	k7c6	po
deváté	devátý	k4xOgFnSc6	devátý
hodině	hodina	k1gFnSc6	hodina
(	(	kIx(	(
<g/>
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
po	po	k7c6	po
desáté	desátá	k1gFnSc6	desátá
hodině	hodina	k1gFnSc6	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
moderovala	moderovat	k5eAaBmAgFnS	moderovat
na	na	k7c6	na
ČT1	ČT1	k1gFnSc6	ČT1
kvíz	kvíz	k1gInSc4	kvíz
Autoškola	autoškola	k1gFnSc1	autoškola
národa	národ	k1gInSc2	národ
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
také	také	k9	také
vodní	vodní	k2eAgInSc4d1	vodní
špektákl	špektákl	k1gInSc4	špektákl
Svatojanské	svatojanský	k2eAgFnSc2d1	Svatojanská
Navalis	Navalis	k1gFnSc2	Navalis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
je	být	k5eAaImIp3nS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Poříz	poříz	k1gMnSc1	poříz
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vicemistr	vicemistr	k1gMnSc1	vicemistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Sumó	Sumó	k1gFnSc6	Sumó
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dceru	dcera	k1gFnSc4	dcera
Vanessu	Vaness	k1gInSc2	Vaness
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lucie	Lucie	k1gFnSc1	Lucie
Výborná	výborný	k2eAgFnSc1d1	výborná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Výborná	výborný	k2eAgFnSc1d1	výborná
–	–	k?	–
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
