<s>
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
KrálZákladní	KrálZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Nástroje	nástroj	k1gInPc1
</s>
<s>
klavír	klavír	k1gInSc4
Web	web	k1gInSc1
</s>
<s>
http://zdenekkral.net/	http://zdenekkral.net/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1971	#num#	k4
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
klavírista	klavírista	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
držitelem	držitel	k1gMnSc7
ceny	cena	k1gFnSc2
Classic	Classic	k2eAgFnSc1d1
Prague	Prague	k1gFnSc1
Awards	Awards	k1gFnPc1
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
skladbu	skladba	k1gFnSc4
vážné	vážný	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
http://classicpragueawards.eu/vitezove-2019/	http://classicpragueawards.eu/vitezove-2019/	k4
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
hudby	hudba	k1gFnSc2
k	k	k7c3
osmi	osm	k4xCc2
desítkám	desítka	k1gFnPc3
divadelních	divadelní	k2eAgInPc2d1
<g/>
,	,	kIx,
filmových	filmový	k2eAgInPc2d1
a	a	k8xC
rozhlasových	rozhlasový	k2eAgInPc2d1
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
autorem	autor	k1gMnSc7
desítky	desítka	k1gFnSc2
komorních	komorní	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
(	(	kIx(
<g/>
tria	trio	k1gNnSc2
<g/>
,	,	kIx,
kvartety	kvartet	k1gInPc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
symfonických	symfonický	k2eAgFnPc2d1
a	a	k8xC
sborových	sborový	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
<g/>
,	,	kIx,
písní	píseň	k1gFnPc2
i	i	k8xC
šansonů	šanson	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natočil	natočit	k5eAaBmAgMnS
4	#num#	k4
sólová	sólový	k2eAgNnPc1d1
klavírní	klavírní	k2eAgNnPc1d1
alba	album	k1gNnPc1
<g/>
,	,	kIx,
asi	asi	k9
50	#num#	k4
alb	album	k1gNnPc2
s	s	k7c7
různými	různý	k2eAgMnPc7d1
interprety	interpret	k1gMnPc7
<g/>
,	,	kIx,
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
scénickou	scénický	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
k	k	k7c3
70	#num#	k4
představením	představení	k1gNnPc3
<g/>
,	,	kIx,
hudbu	hudba	k1gFnSc4
k	k	k7c3
filmům	film	k1gInPc3
Velkofilm	velkofilm	k1gInSc4
a	a	k8xC
Jako	jako	k9
nikdy	nikdy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Martinem	Martin	k1gMnSc7
Krajíčkem	Krajíček	k1gMnSc7
(	(	kIx(
<g/>
mandolína	mandolína	k1gFnSc1
<g/>
)	)	kIx)
tvořil	tvořit	k5eAaImAgInS
duo	duo	k1gNnSc4
KK	KK	kA
Band	banda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
divadla	divadlo	k1gNnSc2
Komediograf	komediograf	k1gMnSc1
<g/>
,	,	kIx,
členem	člen	k1gMnSc7
seskupení	seskupení	k1gNnSc2
Inspektor	inspektor	k1gMnSc1
Kluzó	Kluzó	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
dětské	dětský	k2eAgNnSc4d1
představení	představení	k1gNnSc4
Edison	Edison	k1gMnSc1
(	(	kIx(
<g/>
prem	prem	k1gMnSc1
<g/>
.	.	kIx.
19.6	19.6	k4
<g/>
.2019	.2019	k4
ND	ND	kA
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
knížku	knížka	k1gFnSc4
pohádek	pohádka	k1gFnPc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
Tři	tři	k4xCgMnPc1
drakouni	drakoun	k1gMnPc1
<g/>
,	,	kIx,
sborník	sborník	k1gInSc1
logopedických	logopedický	k2eAgFnPc2d1
písniček	písnička	k1gFnPc2
Žvaní	žvanit	k5eAaImIp3nS
žabák	žabák	k1gInSc1
u	u	k7c2
louže	louž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zpěvníky	zpěvník	k1gInPc1
úprav	úprava	k1gFnPc2
lidových	lidový	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
(	(	kIx(
<g/>
Písničky	písnička	k1gFnPc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
Písničky	písnička	k1gFnPc4
o	o	k7c6
zvířátkách	zvířátko	k1gNnPc6
<g/>
,	,	kIx,
Nejkrásnější	krásný	k2eAgFnPc4d3
písničky	písnička	k1gFnPc4
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výběr	výběr	k1gInSc1
z	z	k7c2
tvorby	tvorba	k1gFnSc2
</s>
<s>
Baron	baron	k1gMnSc1
na	na	k7c6
stromě	strom	k1gInSc6
(	(	kIx(
<g/>
symfonický	symfonický	k2eAgInSc4d1
orchestr	orchestr	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Missa	Missa	k1gFnSc1
brevis	brevis	k1gFnSc1
(	(	kIx(
<g/>
smíšený	smíšený	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
,	,	kIx,
sóla	sólo	k1gNnPc1
<g/>
,	,	kIx,
komorní	komorní	k2eAgInSc1d1
ansámbl	ansámbl	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Magnificat	Magnificat	k1gNnSc1
(	(	kIx(
<g/>
smíšený	smíšený	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
,	,	kIx,
varhany	varhany	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Call	Call	k1gMnSc1
me	me	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Klavírní	klavírní	k2eAgInSc1d1
koncert	koncert	k1gInSc1
č.	č.	k?
1	#num#	k4
</s>
<s>
Carmina	Carmin	k2eAgMnSc2d1
burana	buran	k1gMnSc2
2	#num#	k4
-	-	kIx~
O	o	k7c6
lidské	lidský	k2eAgFnSc6d1
duši	duše	k1gFnSc6
(	(	kIx(
<g/>
dětský	dětský	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
,	,	kIx,
sóla	sólo	k1gNnPc1
<g/>
,	,	kIx,
komorní	komorní	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
<g/>
,	,	kIx,
big-beatová	big-beatový	k2eAgFnSc1d1
kapela	kapela	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ave	ave	k1gNnPc4
verum	verum	k1gInSc1
corpus	corpus	k1gInSc2
(	(	kIx(
<g/>
soprán	soprán	k1gInSc1
<g/>
,	,	kIx,
smyčcový	smyčcový	k2eAgInSc1d1
kvartet	kvartet	k1gInSc1
<g/>
,	,	kIx,
kontrabas	kontrabas	k1gInSc1
<g/>
,	,	kIx,
klavír	klavír	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kde	kde	k6eAd1
domov	domov	k1gInSc1
náš	náš	k3xOp1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
klavírní	klavírní	k2eAgNnSc1d1
trio	trio	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Kvartet	kvartet	k1gInSc1
(	(	kIx(
<g/>
smyčcový	smyčcový	k2eAgInSc1d1
kvartet	kvartet	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Otáčím	otáčet	k5eAaImIp1nS
se	se	k3xPyFc4
(	(	kIx(
<g/>
smyčcový	smyčcový	k2eAgInSc4d1
kvartet	kvartet	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Nekonečné	konečný	k2eNgInPc4d1
příběhy	příběh	k1gInPc4
(	(	kIx(
<g/>
smyčcový	smyčcový	k2eAgInSc4d1
kvartet	kvartet	k1gInSc4
<g/>
,	,	kIx,
klavír	klavír	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Oratorium	oratorium	k1gNnSc4
Vánoční	vánoční	k2eAgInSc1d1
zázrak	zázrak	k1gInSc1
</s>
<s>
muzikál	muzikál	k1gInSc1
Orfeus	Orfeus	k1gMnSc1
Superstar	superstar	k1gFnSc1
</s>
<s>
Shakespearovské	shakespearovský	k2eAgFnPc1d1
variace	variace	k1gFnPc1
</s>
<s>
Výběr	výběr	k1gInSc1
z	z	k7c2
CD	CD	kA
</s>
<s>
KK	KK	kA
Band	band	k1gInSc1
<g/>
:	:	kIx,
Šoulet	šoulet	k1gInSc1
</s>
<s>
KK	KK	kA
Band	band	k1gInSc1
<g/>
:	:	kIx,
Ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
na	na	k7c6
jaře	jaro	k1gNnSc6
</s>
<s>
Minach	Minach	k1gInSc1
<g/>
:	:	kIx,
Balada	balada	k1gFnSc1
pro	pro	k7c4
Bardotku	Bardotka	k1gFnSc4
</s>
<s>
Minach	Minach	k1gInSc1
<g/>
:	:	kIx,
Zimomriavky	Zimomriavka	k1gFnPc1
</s>
<s>
NAHA	naho	k1gNnPc1
</s>
<s>
Možnosti	možnost	k1gFnPc1
<g/>
/	/	kIx~
<g/>
Possibilities	Possibilities	k1gInSc1
</s>
<s>
You	You	k?
<g/>
...	...	k?
</s>
<s>
Zodiac	Zodiac	k6eAd1
</s>
<s>
Expectation	Expectation	k1gInSc1
</s>
<s>
Missa	Missa	k1gFnSc1
brevis	brevis	k1gFnSc2
</s>
<s>
Andrea	Andrea	k1gFnSc1
Buršová	Buršová	k1gFnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
Indigo	indigo	k1gNnSc1
Quartet	Quarteta	k1gFnPc2
<g/>
:	:	kIx,
Nůž	nůž	k1gInSc1
na	na	k7c4
ticho	ticho	k1gNnSc4
</s>
<s>
Ivana	Ivana	k1gFnSc1
Odehnalová	Odehnalová	k1gFnSc1
<g/>
:	:	kIx,
Fantigo	Fantigo	k6eAd1
</s>
<s>
Zadržitelný	zadržitelný	k2eAgInSc1d1
vzestup	vzestup	k1gInSc1
Artura	Artur	k1gMnSc2
Uie	Uie	k1gMnSc2
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
:	:	kIx,
Konžert	Konžert	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
:	:	kIx,
Anežka	Anežka	k1gFnSc1
chce	chtít	k5eAaImIp3nS
tančit	tančit	k5eAaImF
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
Indigo	indigo	k1gNnSc1
Quartet	Quarteta	k1gFnPc2
<g/>
:	:	kIx,
Out	Out	k1gFnSc1
of	of	k?
Blue	Blue	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
/	/	kIx~
</s>
<s>
Inspektor	inspektor	k1gMnSc1
Kluzó	Kluzó	k1gMnSc1
<g/>
:	:	kIx,
Dáma	dáma	k1gFnSc1
a	a	k8xC
pes	pes	k1gMnSc1
</s>
<s>
Kennedyho	Kennedyze	k6eAd1
děti	dítě	k1gFnPc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
Mario	Mario	k1gMnSc1
Buzzi	Buzze	k1gFnSc4
<g/>
:	:	kIx,
Komediograf	komediograf	k1gMnSc1
</s>
<s>
Carmina	Carmin	k2eAgMnSc2d1
burana	buran	k1gMnSc2
2	#num#	k4
-	-	kIx~
O	o	k7c6
lidské	lidský	k2eAgFnSc6d1
duši	duše	k1gFnSc6
</s>
<s>
Knihy	kniha	k1gFnPc1
a	a	k8xC
zpěvníky	zpěvník	k1gInPc1
</s>
<s>
Písničky	písnička	k1gFnPc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
</s>
<s>
Písničky	písnička	k1gFnPc1
o	o	k7c6
zvířátkách	zvířátko	k1gNnPc6
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
Yvetta	Yvetta	k1gFnSc1
Voráčová	Voráčová	k1gFnSc1
<g/>
:	:	kIx,
Hýbánky	Hýbánek	k1gInPc1
</s>
<s>
Nejkrásnější	krásný	k2eAgFnPc1d3
písničky	písnička	k1gFnPc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
Miroslava	Miroslava	k1gFnSc1
Palečková	Palečková	k1gFnSc1
<g/>
:	:	kIx,
Žvaní	žvanit	k5eAaImIp3nS
žabák	žabák	k1gMnSc1
u	u	k7c2
louže	louž	k1gFnSc2
</s>
<s>
Tati	tati	k1gMnSc1
<g/>
,	,	kIx,
ty	ty	k3xPp2nSc1
mě	já	k3xPp1nSc4
přivedeš	přivést	k5eAaPmIp2nS
do	do	k7c2
hrobu	hrob	k1gInSc2
</s>
<s>
Písničky	písnička	k1gFnPc1
pro	pro	k7c4
malé	malý	k2eAgMnPc4d1
zpěváčky	zpěváček	k1gMnPc4
</s>
<s>
Drakouni	Drakoun	k1gMnPc1
</s>
<s>
Zpěvník	zpěvník	k1gInSc1
s	s	k7c7
omalovánkami	omalovánka	k1gFnPc7
</s>
<s>
Výběr	výběr	k1gInSc1
scénické	scénický	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1
Nemesis	Nemesis	k1gFnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
David	David	k1gMnSc1
Jařab	Jařab	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Komediograf	komediograf	k1gMnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luboš	Luboš	k1gMnSc1
Balák	Balák	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Deník	deník	k1gInSc1
Anne	Ann	k1gFnSc2
Frankové	Franková	k1gFnSc2
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoja	Zoja	k1gFnSc1
Mikotová	Mikotový	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Největší	veliký	k2eAgMnSc1d3
z	z	k7c2
Pierotů	pierot	k1gMnPc2
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoja	Zoja	k1gFnSc1
Mikotová	Mikotový	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Edison	Edison	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hana	Hana	k1gFnSc1
Mikolášková	Mikolášková	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vertigo	Vertigo	k1gNnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexandr	Alexandr	k1gMnSc1
Minajev	Minajev	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Idiot	idiot	k1gMnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexandr	Alexandr	k1gMnSc1
MInajev	MInajev	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Revizor	revizor	k1gMnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexandr	Alexandr	k1gMnSc1
MInajev	MInajev	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jednou	jednou	k9
budem	budem	k?
dál	daleko	k6eAd2
(	(	kIx(
<g/>
režie	režie	k1gFnSc1
Petr	Petr	k1gMnSc1
Michálek	Michálek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Tvrz	tvrz	k1gFnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luboš	Luboš	k1gMnSc1
Balák	Balák	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Vlaková	vlakový	k2eAgFnSc1d1
pohádka	pohádka	k1gFnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Aladin	Aladin	k2eAgInSc1d1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Moj	Moj	k?
malý	malý	k2eAgMnSc1d1
princ	princ	k1gMnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Gašpariáda	Gašpariáda	k1gFnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Najmenší	Najmenší	k2eAgInSc1d1
cirkus	cirkus	k1gInSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Pohádka	pohádka	k1gFnSc1
o	o	k7c6
nevyřáděném	vyřáděný	k2eNgMnSc6d1
dědečkovi	dědeček	k1gMnSc6
(	(	kIx(
<g/>
rež	rež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Hop	hop	k0
(	(	kIx(
<g/>
rež	rež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Dlouhý	Dlouhý	k1gMnSc1
<g/>
,	,	kIx,
Široký	Široký	k1gMnSc1
a	a	k8xC
Bystrozraký	bystrozraký	k2eAgMnSc1d1
(	(	kIx(
<g/>
rež	rež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Dášeňka	Dášeňka	k1gFnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
pejskách	pejskách	k?
a	a	k8xC
kočičkách	kočička	k1gFnPc6
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Shakespyré	Shakespyrý	k2eAgNnSc1d1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Raskolnikov	Raskolnikov	k1gInSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
České	český	k2eAgNnSc1d1
moře	moře	k1gNnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vladimír	Vladimír	k1gMnSc1
Morávek	Morávek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Pižďuši	Pižďuše	k1gFnSc4
(	(	kIx(
<g/>
rež	rež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vladimír	Vladimír	k1gMnSc1
Morávek	Morávek	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Dlouhý	Dlouhý	k1gMnSc1
<g/>
,	,	kIx,
Široký	Široký	k1gMnSc1
a	a	k8xC
Bystrozraký	bystrozraký	k2eAgMnSc1d1
(	(	kIx(
<g/>
rež	rež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jolana	Jolana	k1gFnSc1
Kubíková	Kubíková	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1930	#num#	k4
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jolana	Jolana	k1gFnSc1
Kubíková	Kubíková	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hamlet	Hamlet	k1gMnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jolana	Jolana	k1gFnSc1
Kubíková	Kubíková	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Princezna	princezna	k1gFnSc1
Pampeliška	pampeliška	k1gFnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jolana	Jolana	k1gFnSc1
Kubíková	Kubíková	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Amadeus	Amadeus	k1gMnSc1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Šimák	Šimák	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Červený	červený	k2eAgInSc1d1
a	a	k8xC
černý	černý	k2eAgInSc1d1
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Šimák	Šimák	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Vlnobití	vlnobití	k1gNnSc1
Andrey	Andrea	k1gFnSc2
Buršové	burš	k1gMnPc5
(	(	kIx(
<g/>
rež	rež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodo	dodo	k1gMnSc1
Gombár	Gombár	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ceny	cena	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
Cenu	cena	k1gFnSc4
komediantů	komediant	k1gMnPc2
za	za	k7c4
scénickou	scénický	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
k	k	k7c3
inscenaci	inscenace	k1gFnSc3
Deník	deník	k1gInSc1
Anne	Anne	k1gNnSc6
Frankové	Franková	k1gFnSc2
Slováckého	slovácký	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
finalistou	finalista	k1gMnSc7
v	v	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
skladatelské	skladatelský	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
The	The	k1gMnSc3
UK	UK	kA
Songwriting	Songwriting	k1gInSc1
Contest	Contest	k1gFnSc1
2017	#num#	k4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
instrumentální	instrumentální	k2eAgFnSc2d1
skladby	skladba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
finalistou	finalista	k1gMnSc7
mezinárodní	mezinárodní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
Pianista	pianista	k1gMnSc1
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
duem	duo	k1gNnSc7
KK	KK	kA
Band	banda	k1gFnPc2
získal	získat	k5eAaPmAgMnS
cenu	cena	k1gFnSc4
Krteček	krteček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
držitelem	držitel	k1gMnSc7
ceny	cena	k1gFnSc2
Classic	Classice	k1gFnPc2
Prague	Prague	k1gFnSc7
Awards	Awardsa	k1gFnPc2
za	za	k7c4
skladbu	skladba	k1gFnSc4
Magnificat	Magnificat	k1gNnSc2
pro	pro	k7c4
smíšený	smíšený	k2eAgInSc4d1
sbor	sbor	k1gInSc4
a	a	k8xC
varhany	varhany	k1gInPc4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
držitelem	držitel	k1gMnSc7
ceny	cena	k1gFnSc2
Osa	osa	k1gFnSc1
za	za	k7c4
nejhranějšího	hraný	k2eAgMnSc4d3
skladatele	skladatel	k1gMnSc4
vážné	vážný	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.songwritingcontest.co.uk/2017-results.html	http://www.songwritingcontest.co.uk/2017-results.html	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Seznam	seznam	k1gInSc1
filmové	filmový	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
na	na	k7c6
portálu	portál	k1gInSc6
ČSFD	ČSFD	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
