<s>
XXVIII	XXVIII	kA	XXVIII
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2004	[number]	k4	2004
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
řeckých	řecký	k2eAgFnPc6d1	řecká
Athénách	Athéna	k1gFnPc6	Athéna
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Účastnilo	účastnit	k5eAaImAgNnS	účastnit
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
10625	[number]	k4	10625
sportovců	sportovec	k1gMnPc2	sportovec
z	z	k7c2	z
201	[number]	k4	201
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
příprav	příprava	k1gFnPc2	příprava
na	na	k7c4	na
olympiádu	olympiáda	k1gFnSc4	olympiáda
2004	[number]	k4	2004
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
pořadatelé	pořadatel	k1gMnPc1	pořadatel
oslovili	oslovit	k5eAaPmAgMnP	oslovit
Tiësta	Tiëst	k1gMnSc4	Tiëst
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
by	by	kYmCp3nS	by
zahrál	zahrát	k5eAaPmAgMnS	zahrát
na	na	k7c6	na
uváděcím	uváděcí	k2eAgInSc6d1	uváděcí
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgNnSc7	první
DJem	DJe	k1gNnSc7	DJe
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyskytl	vyskytnout	k5eAaPmAgInS	vyskytnout
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Atén	Atény	k1gFnPc2	Atény
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
schůzky	schůzka	k1gFnSc2	schůzka
s	s	k7c7	s
pořadateli	pořadatel	k1gMnPc7	pořadatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
poslech	poslech	k1gInSc1	poslech
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
před	před	k7c7	před
prázdným	prázdný	k2eAgInSc7d1	prázdný
stadionem	stadion	k1gInSc7	stadion
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
den	den	k1gInSc4	den
si	se	k3xPyFc3	se
Tiësta	Tiëst	k1gMnSc4	Tiëst
přišlo	přijít	k5eAaPmAgNnS	přijít
poslechnout	poslechnout	k5eAaPmF	poslechnout
35	[number]	k4	35
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
někteří	některý	k3yIgMnPc1	některý
rozpoznávali	rozpoznávat	k5eAaImAgMnP	rozpoznávat
melodie	melodie	k1gFnPc4	melodie
písní	píseň	k1gFnPc2	píseň
Traffic	Traffice	k1gInPc2	Traffice
nebo	nebo	k8xC	nebo
právě	právě	k9	právě
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
Adagia	adagio	k1gNnSc2	adagio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
zkoušce	zkouška	k1gFnSc6	zkouška
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
asi	asi	k9	asi
60	[number]	k4	60
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
technické	technický	k2eAgInPc1d1	technický
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Mixážní	mixážní	k2eAgInSc1d1	mixážní
pult	pult	k1gInSc1	pult
se	se	k3xPyFc4	se
poničil	poničit	k5eAaPmAgInS	poničit
<g/>
,	,	kIx,	,
monitory	monitor	k1gInPc1	monitor
několikrát	několikrát	k6eAd1	několikrát
vypadly	vypadnout	k5eAaPmAgInP	vypadnout
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
na	na	k7c6	na
stadiónu	stadión	k1gInSc6	stadión
nebyla	být	k5eNaImAgFnS	být
pořád	pořád	k6eAd1	pořád
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
národnosti	národnost	k1gFnPc4	národnost
během	během	k7c2	během
slavnosti	slavnost	k1gFnSc2	slavnost
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2004	[number]	k4	2004
představily	představit	k5eAaPmAgInP	představit
své	svůj	k3xOyFgMnPc4	svůj
atlety	atlet	k1gMnPc4	atlet
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
10500	[number]	k4	10500
a	a	k8xC	a
v	v	k7c6	v
publiku	publikum	k1gNnSc6	publikum
bylo	být	k5eAaImAgNnS	být
80	[number]	k4	80
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
75	[number]	k4	75
000	[number]	k4	000
vědělo	vědět	k5eAaImAgNnS	vědět
o	o	k7c6	o
taneční	taneční	k2eAgFnSc6d1	taneční
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jeho	on	k3xPp3gNnSc2	on
představení	představení	k1gNnSc2	představení
začali	začít	k5eAaPmAgMnP	začít
nizozemští	nizozemský	k2eAgMnPc1d1	nizozemský
atleti	atlet	k1gMnPc1	atlet
tancovat	tancovat	k5eAaImF	tancovat
přímo	přímo	k6eAd1	přímo
před	před	k7c7	před
DJ	DJ	kA	DJ
kioskem	kiosek	k1gInSc7	kiosek
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
museli	muset	k5eAaImAgMnP	muset
být	být	k5eAaImF	být
zpacifikování	zpacifikování	k1gNnSc4	zpacifikování
pořadateli	pořadatel	k1gMnSc3	pořadatel
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
rovněž	rovněž	k9	rovněž
nové	nový	k2eAgInPc4d1	nový
songy	song	k1gInPc4	song
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
speciální	speciální	k2eAgFnSc4d1	speciální
událost	událost	k1gFnSc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
písně	píseň	k1gFnPc1	píseň
měly	mít	k5eAaImAgFnP	mít
speciální	speciální	k2eAgInSc4d1	speciální
účel	účel	k1gInSc4	účel
a	a	k8xC	a
duch	duch	k1gMnSc1	duch
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vyšly	vyjít	k5eAaPmAgInP	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Parade	Parad	k1gInSc5	Parad
of	of	k?	of
the	the	k?	the
Athletes	Athletes	k1gInSc1	Athletes
veškeré	veškerý	k3xTgFnSc2	veškerý
skladby	skladba	k1gFnSc2	skladba
hrané	hraný	k2eAgInPc4d1	hraný
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
mixu	mix	k1gInSc6	mix
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
ale	ale	k9	ale
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tracky	track	k1gInPc1	track
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
zahrál	zahrát	k5eAaPmAgInS	zahrát
<g/>
,	,	kIx,	,
nesplňovaly	splňovat	k5eNaImAgFnP	splňovat
přesně	přesně	k6eAd1	přesně
pravidla	pravidlo	k1gNnSc2	pravidlo
zadané	zadaná	k1gFnSc2	zadaná
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
organizací	organizace	k1gFnSc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
úspěchu	úspěch	k1gInSc6	úspěch
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
veškeré	veškerý	k3xTgFnPc4	veškerý
skladby	skladba	k1gFnPc4	skladba
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
délce	délka	k1gFnSc6	délka
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
sportovcem	sportovec	k1gMnSc7	sportovec
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
americký	americký	k2eAgMnSc1d1	americký
plavec	plavec	k1gMnSc1	plavec
Michael	Michael	k1gMnSc1	Michael
Phelps	Phelps	k1gInSc4	Phelps
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získal	získat	k5eAaPmAgInS	získat
6	[number]	k4	6
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
2	[number]	k4	2
bronzové	bronzový	k2eAgFnSc2d1	bronzová
medaile	medaile	k1gFnSc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
202	[number]	k4	202
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
Kiribati	Kiribati	k1gFnPc2	Kiribati
a	a	k8xC	a
Východní	východní	k2eAgInSc4d1	východní
Timor	Timor	k1gInSc4	Timor
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
udávají	udávat	k5eAaImIp3nP	udávat
počty	počet	k1gInPc1	počet
sportovců	sportovec	k1gMnPc2	sportovec
zastupujících	zastupující	k2eAgFnPc2d1	zastupující
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
