<s>
Šinkansen	Šinkansen	k1gInSc1
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
série	série	k1gFnSc2
E5	E5	k1gFnSc2
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
新	新	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
síť	síť	k1gFnSc1
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1
železnic	železnice	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
osmi	osm	k4xCc2
tratí	trať	k1gFnPc2
a	a	k8xC
obsluhuje	obsluhovat	k5eAaImIp3nS
většinu	většina	k1gFnSc4
významných	významný	k2eAgNnPc2d1
japonských	japonský	k2eAgNnPc2d1
velkoměst	velkoměsto	k1gNnPc2
na	na	k7c6
ostrovech	ostrov	k1gInPc6
Honšú	Honšú	k1gMnPc2
a	a	k8xC
Kjúšú	Kjúšú	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
dosahovaná	dosahovaný	k2eAgFnSc1d1
provozní	provozní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
320	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
na	na	k7c6
trati	trať	k1gFnSc6
Tóhoku	Tóhok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tunel	tunel	k1gInSc1
Seikan	Seikany	k1gInPc2
hostí	hostit	k5eAaImIp3nS
tunelové	tunelový	k2eAgNnSc4d1
propojení	propojení	k1gNnSc4
na	na	k7c4
ostrov	ostrov	k1gInSc4
Hokkaidó	Hokkaidó	k1gFnSc2
<g/>
,	,	kIx,
plánuje	plánovat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
spojení	spojení	k1gNnSc1
s	s	k7c7
ostrovem	ostrov	k1gInSc7
Šikoku	Šikok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
šinkansen	šinkansen	k1gInSc1
v	v	k7c6
doslovném	doslovný	k2eAgInSc6d1
překladu	překlad	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
"	"	kIx"
<g/>
nová	nový	k2eAgFnSc1d1
páteřní	páteřní	k2eAgFnSc1d1
trať	trať	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
anglofonních	anglofonní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
se	se	k3xPyFc4
lze	lze	k6eAd1
často	často	k6eAd1
setkat	setkat	k5eAaPmF
s	s	k7c7
označením	označení	k1gNnSc7
bullet	bullet	k1gInSc1
train	train	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
překladem	překlad	k1gInSc7
japonského	japonský	k2eAgInSc2d1
kódového	kódový	k2eAgInSc2d1
názvu	název	k1gInSc2
dangan	dangan	k1gMnSc1
rešša	rešša	k1gMnSc1
(	(	kIx(
<g/>
弾	弾	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formálně	formálně	k6eAd1
se	se	k3xPyFc4
pojem	pojem	k1gInSc1
šinkansen	šinkansna	k1gFnPc2
vztahuje	vztahovat	k5eAaImIp3nS
výlučně	výlučně	k6eAd1
na	na	k7c4
vysokorychlostní	vysokorychlostní	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
ale	ale	k8xC
označuje	označovat	k5eAaImIp3nS
i	i	k9
vlaky	vlak	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
po	po	k7c6
nich	on	k3xPp3gInPc6
jezdí	jezdit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
série	série	k1gFnSc2
0	#num#	k4
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
první	první	k4xOgFnSc7
zemí	zem	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vybudovala	vybudovat	k5eAaPmAgFnS
železnici	železnice	k1gFnSc4
určenou	určený	k2eAgFnSc4d1
výlučně	výlučně	k6eAd1
pro	pro	k7c4
rychlou	rychlý	k2eAgFnSc4d1
osobní	osobní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
důsledek	důsledek	k1gInSc4
stavu	stav	k1gInSc2
japonské	japonský	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
po	po	k7c6
skončení	skončení	k1gNnSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
tamní	tamní	k2eAgFnSc2d1
úzkorozchodné	úzkorozchodný	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
svojí	svůj	k3xOyFgFnSc7
kapacitou	kapacita	k1gFnSc7
nedostačovaly	dostačovat	k5eNaImAgInP
stoupající	stoupající	k2eAgFnSc4d1
poptávce	poptávka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInPc1d1
plány	plán	k1gInPc1
ze	z	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
počítaly	počítat	k5eAaImAgFnP
s	s	k7c7
úplnou	úplný	k2eAgFnSc7d1
přestavbou	přestavba	k1gFnSc7
části	část	k1gFnPc1
státem	stát	k1gInSc7
vlastněných	vlastněný	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
na	na	k7c4
normální	normální	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
kolejí	kolej	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
finanční	finanční	k2eAgFnSc3d1
náročnosti	náročnost	k1gFnSc3
k	k	k7c3
tomu	ten	k3xDgNnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namísto	namísto	k7c2
toho	ten	k3xDgMnSc2
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
vznikl	vzniknout	k5eAaPmAgInS
projekt	projekt	k1gInSc1
na	na	k7c4
vybudování	vybudování	k1gNnSc4
separátních	separátní	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
pro	pro	k7c4
rychlou	rychlý	k2eAgFnSc4d1
osobní	osobní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
„	„	k?
<g/>
tramvajového	tramvajový	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
spojnice	spojnice	k1gFnSc1
dvou	dva	k4xCgNnPc2
největších	veliký	k2eAgNnPc2d3
japonských	japonský	k2eAgNnPc2d1
velkoměst	velkoměsto	k1gNnPc2
-	-	kIx~
Tokia	Tokio	k1gNnSc2
a	a	k8xC
Ósaky	Ósaka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
otevřena	otevřen	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1964	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
Tókaidó-šinkansen	Tókaidó-šinkansna	k1gFnPc2
<g/>
,	,	kIx,
10	#num#	k4
dní	den	k1gInPc2
před	před	k7c7
začátkem	začátek	k1gInSc7
letní	letní	k2eAgFnSc2d1
olympiády	olympiáda	k1gFnSc2
v	v	k7c6
Tokiu	Tokio	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
na	na	k7c6
trati	trať	k1gFnSc6
byla	být	k5eAaImAgFnS
210	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Tato	tento	k3xDgFnSc1
trať	trať	k1gFnSc1
byla	být	k5eAaImAgFnS
okamžitým	okamžitý	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
<g/>
,	,	kIx,
již	již	k6eAd1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
zahájení	zahájení	k1gNnSc6
provozu	provoz	k1gInSc2
bylo	být	k5eAaImAgNnS
přepraveno	přepravit	k5eAaPmNgNnS
100	#num#	k4
milionů	milion	k4xCgInPc2
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
téměř	téměř	k6eAd1
okamžitě	okamžitě	k6eAd1
pokračovala	pokračovat	k5eAaImAgFnS
rozšiřováním	rozšiřování	k1gNnSc7
tratě	trať	k1gFnSc2
Tókaidó	Tókaidó	k1gFnSc2
dále	daleko	k6eAd2
na	na	k7c4
západ	západ	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
trať	trať	k1gFnSc1
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
pod	pod	k7c7
názvem	název	k1gInSc7
Sanjó-šinkansen	Sanjó-šinkansen	k1gInSc1
<g/>
)	)	kIx)
své	svůj	k3xOyFgFnPc4
současné	současný	k2eAgFnPc4d1
konečné	konečná	k1gFnPc4
ve	v	k7c6
městě	město	k1gNnSc6
Fukuoka	Fukuoek	k1gInSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Kjúšú	Kjúšú	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovaly	následovat	k5eAaImAgInP
dvě	dva	k4xCgFnPc4
tratě	trať	k1gFnPc4
z	z	k7c2
Tokia	Tokio	k1gNnSc2
severním	severní	k2eAgInSc7d1
směrem	směr	k1gInSc7
-	-	kIx~
Džóecu	Džóeca	k1gFnSc4
do	do	k7c2
Niigaty	Niigata	k1gFnSc2
a	a	k8xC
Tóhoku	Tóhok	k1gInSc2
do	do	k7c2
Morioky	Moriok	k1gInPc1
<g/>
,	,	kIx,
dokončené	dokončený	k2eAgInPc1d1
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
vlaky	vlak	k1gInPc1
na	na	k7c6
těchto	tento	k3xDgFnPc6
dvou	dva	k4xCgFnPc6
tratích	trať	k1gFnPc6
odjížděly	odjíždět	k5eAaImAgFnP
z	z	k7c2
tokijského	tokijský	k2eAgNnSc2d1
severního	severní	k2eAgNnSc2d1
předměstí	předměstí	k1gNnSc2
Ómija	Ómijum	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
tokijského	tokijský	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
nádraží	nádraží	k1gNnSc2
jezdí	jezdit	k5eAaImIp3nP
až	až	k9
od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP
přestavby	přestavba	k1gFnPc1
dvou	dva	k4xCgFnPc2
existujících	existující	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
a	a	k8xC
vznikly	vzniknout	k5eAaPmAgInP
tak	tak	k9
Jamagata-šinkansen	Jamagata-šinkansna	k1gFnPc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Akita-šinkansen	Akita-šinkansen	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pojmenované	pojmenovaný	k2eAgInPc1d1
podle	podle	k7c2
tehdejších	tehdejší	k2eAgFnPc2d1
konečných	konečný	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
byla	být	k5eAaImAgFnS
také	také	k9
při	při	k7c6
příležitosti	příležitost	k1gFnSc6
zimní	zimní	k2eAgFnSc2d1
olympiády	olympiáda	k1gFnSc2
v	v	k7c6
Naganu	Nagano	k1gNnSc6
postavena	postaven	k2eAgFnSc1d1
odbočka	odbočka	k1gFnSc1
trati	trať	k1gFnSc2
Džóecu	Džóecus	k1gInSc2
z	z	k7c2
Takasaki	Takasak	k1gFnSc2
do	do	k7c2
Nagana	Nagano	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2015	#num#	k4
prodloužena	prodloužit	k5eAaPmNgFnS
až	až	k9
do	do	k7c2
Kanazawy	Kanazawa	k1gFnSc2
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc2d1
na	na	k7c6
severním	severní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
a	a	k8xC
při	při	k7c6
té	ten	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
přejmenována	přejmenován	k2eAgMnSc4d1
na	na	k7c4
Hokuriku-šinkansen	Hokuriku-šinkansen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linka	linka	k1gFnSc1
Tóhoku-šinkansen	Tóhoku-šinkansna	k1gFnPc2
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2002	#num#	k4
na	na	k7c6
severním	severní	k2eAgInSc6d1
konci	konec	k1gInSc6
prodloužena	prodloužen	k2eAgFnSc1d1
do	do	k7c2
Hačinohe	Hačinoh	k1gInSc2
a	a	k8xC
roku	rok	k1gInSc2
2010	#num#	k4
až	až	k9
do	do	k7c2
Aomori	Aomor	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
ni	on	k3xPp3gFnSc4
roku	rok	k1gInSc2
2016	#num#	k4
napojila	napojit	k5eAaPmAgFnS
linka	linka	k1gFnSc1
Hokkaidó-šinkansen	Hokkaidó-šinkansen	k1gInSc1
<g/>
,	,	kIx,
spojující	spojující	k2eAgInSc1d1
v	v	k7c6
první	první	k4xOgFnSc6
etapě	etapa	k1gFnSc6
Aomori	Aomor	k1gFnSc2
a	a	k8xC
Hakodate	Hakodat	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
linky	linka	k1gFnSc2
Kjúšú-šinkansen	Kjúšú-šinkansna	k1gFnPc2
<g/>
,	,	kIx,
spojující	spojující	k2eAgFnSc4d1
Kagošimu	Kagošima	k1gFnSc4
a	a	k8xC
Jacuširo	Jacuširo	k1gNnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2011	#num#	k4
pak	pak	k6eAd1
trať	trať	k1gFnSc4
dosáhla	dosáhnout	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
Fukuoky	Fukuok	k1gInPc4
a	a	k8xC
propojila	propojit	k5eAaPmAgFnS
se	se	k3xPyFc4
se	s	k7c7
Sanjó-šinkansenem	Sanjó-šinkansen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
začala	začít	k5eAaPmAgFnS
výstavba	výstavba	k1gFnSc1
Čúó-šinkansenu	Čúó-šinkansen	k2eAgFnSc4d1
<g/>
,	,	kIx,
druhé	druhý	k4xOgFnSc2
vysokorychlostní	vysokorychlostní	k2eAgFnSc2d1
spojnice	spojnice	k1gFnSc2
Tokia	Tokio	k1gNnSc2
<g/>
,	,	kIx,
Nagoje	Nagoj	k1gInSc2
a	a	k8xC
Ósaky	Ósaka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trať	trať	k1gFnSc1
bude	být	k5eAaImBp3nS
využívat	využívat	k5eAaImF,k5eAaPmF
technologie	technologie	k1gFnSc1
maglev	maglet	k5eAaPmDgInS
<g/>
,	,	kIx,
provozní	provozní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
má	mít	k5eAaImIp3nS
činit	činit	k5eAaImF
505	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Provoz	provoz	k1gInSc1
na	na	k7c6
prvním	první	k4xOgInSc6
úseku	úsek	k1gInSc6
mezi	mezi	k7c7
Tokiem	Tokio	k1gNnSc7
a	a	k8xC
Nagojou	Nagoja	k1gFnSc7
by	by	kYmCp3nP
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
zahájen	zahájit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2029	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tratě	trať	k1gFnPc1
</s>
<s>
Mapa	mapa	k1gFnSc1
sítě	síť	k1gFnSc2
Šinkansen	Šinkansna	k1gFnPc2
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
má	mít	k5eAaImIp3nS
síť	síť	k1gFnSc4
Šinkansen	Šinkansna	k1gFnPc2
osm	osm	k4xCc1
hlavních	hlavní	k2eAgFnPc2d1
linií	linie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc4
<g/>
:	:	kIx,
</s>
<s>
Tókaidó-šinkansen	Tókaidó-šinkansen	k1gInSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
-	-	kIx~
Ósaka	Ósaka	k1gFnSc1
<g/>
,	,	kIx,
515	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojuje	spojovat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
největší	veliký	k2eAgFnPc4d3
japonské	japonský	k2eAgFnPc4d1
aglomerace	aglomerace	k1gFnPc4
<g/>
,	,	kIx,
vede	vést	k5eAaImIp3nS
souvisle	souvisle	k6eAd1
osídlenou	osídlený	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
ostrova	ostrov	k1gInSc2
Honšú	Honšú	k1gFnSc1
<g/>
,	,	kIx,
název	název	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
východní	východní	k2eAgFnSc1d1
mořská	mořský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
města	město	k1gNnSc2
<g/>
:	:	kIx,
Tokio	Tokio	k1gNnSc1
<g/>
,	,	kIx,
Nagoja	Nagoja	k1gFnSc1
<g/>
,	,	kIx,
Kjóto	Kjóto	k1gNnSc1
<g/>
,	,	kIx,
Ósaka	Ósaka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sanjó-šinkansen	Sanjó-šinkansen	k1gInSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
,	,	kIx,
Ósaka	Ósaka	k1gFnSc1
-	-	kIx~
Fukuoka	Fukuoka	k1gMnSc1
<g/>
,	,	kIx,
554	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
města	město	k1gNnSc2
<g/>
:	:	kIx,
Ósaka	Ósaka	k1gFnSc1
<g/>
,	,	kIx,
Kóbe	Kóbe	k1gNnSc1
<g/>
,	,	kIx,
Hirošima	Hirošima	k1gFnSc1
<g/>
,	,	kIx,
Fukuoka	Fukuoka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozní	provozní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
až	až	k9
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Ve	v	k7c6
Fukuoce	Fukuoka	k1gFnSc6
trať	trať	k1gFnSc1
končí	končit	k5eAaImIp3nS
na	na	k7c4
terminálu	terminála	k1gFnSc4
Hakata	Hakat	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Tóhoku-šinkansen	Tóhoku-šinkansen	k1gInSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
-	-	kIx~
Aomori	Aomori	k1gNnSc1
<g/>
,	,	kIx,
675	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počátcích	počátek	k1gInPc6
provozu	provoz	k1gInSc2
vlaky	vlak	k1gInPc1
odjížděly	odjíždět	k5eAaImAgFnP
z	z	k7c2
tokijského	tokijský	k2eAgNnSc2d1
předměstí	předměstí	k1gNnSc2
Ómija	Ómij	k1gInSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
trať	trať	k1gFnSc1
prodloužena	prodloužit	k5eAaPmNgFnS
až	až	k9
na	na	k7c4
tokijské	tokijský	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
severovýchodní	severovýchodní	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
páteřní	páteřní	k2eAgFnSc1d1
trať	trať	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
města	město	k1gNnSc2
<g/>
:	:	kIx,
Tokio	Tokio	k1gNnSc1
<g/>
,	,	kIx,
Sendai	Sendai	k1gNnSc1
<g/>
,	,	kIx,
Morioka	Morioka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
2013	#num#	k4
je	být	k5eAaImIp3nS
na	na	k7c6
části	část	k1gFnSc6
trati	trať	k1gFnSc2
maximální	maximální	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
320	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
s	s	k7c7
novými	nový	k2eAgFnPc7d1
soupravami	souprava	k1gFnPc7
ALFA-X	ALFA-X	k1gMnPc2
by	by	kYmCp3nS
rychlost	rychlost	k1gFnSc1
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
měla	mít	k5eAaImAgFnS
vzrůst	vzrůst	k5eAaPmF
až	až	k9
na	na	k7c4
360	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Džóecu-šinkansen	Džóecu-šinkansen	k1gInSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
,	,	kIx,
Ómiya	Ómiya	k1gFnSc1
-	-	kIx~
Niigata	Niigata	k1gFnSc1
<g/>
,	,	kIx,
270	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc1
provozu	provoz	k1gInSc2
stejné	stejný	k2eAgInPc1d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
trať	trať	k1gFnSc1
Tóhoku-šinkansen	Tóhoku-šinkansna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
města	město	k1gNnSc2
<g/>
:	:	kIx,
Takasaki	Takasaki	k1gNnSc1
<g/>
,	,	kIx,
Niigata	Niigata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hokuriku-šinkansen	Hokuriku-šinkansen	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
,	,	kIx,
Takasaki	Takasaki	k1gNnSc1
-	-	kIx~
Kanazawa	Kanazawa	k1gMnSc1
<g/>
,	,	kIx,
345	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
města	město	k1gNnSc2
<g/>
:	:	kIx,
Takasaki	Takasaki	k1gNnSc1
<g/>
,	,	kIx,
Nagano	Nagano	k1gNnSc1
<g/>
,	,	kIx,
Kanazawa	Kanazawa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kjúšú-šinkansen	Kjúšú-šinkansen	k1gInSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
,	,	kIx,
Fukuoka	Fukuoka	k1gFnSc1
-	-	kIx~
Kagošima	Kagošima	k1gNnSc1
<g/>
,	,	kIx,
257	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
města	město	k1gNnSc2
<g/>
:	:	kIx,
Fukuoka	Fukuoka	k1gFnSc1
<g/>
,	,	kIx,
Kumamoto	Kumamota	k1gFnSc5
<g/>
,	,	kIx,
Kagošima	Kagošim	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Hokkaidó-šinkansen	Hokkaidó-šinkansen	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
Aomori	Aomori	k1gNnSc1
-	-	kIx~
Hakodate	Hakodat	k1gMnSc5
<g/>
,	,	kIx,
149	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prochází	procházet	k5eAaImIp3nS
skrz	skrz	k7c4
Tunel	tunel	k1gInSc4
Seikan	Seikany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Mini-šinkansen	Mini-šinkansen	k1gInSc1
</s>
<s>
Jamagata-šinkansen	Jamagata-šinkansen	k1gInSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
,	,	kIx,
Fukušima	Fukušima	k1gFnSc1
-	-	kIx~
Šindžó	Šindžó	k1gMnSc1
<g/>
,	,	kIx,
149	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trať	trať	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
přestavbou	přestavba	k1gFnSc7
původní	původní	k2eAgFnSc1d1
úzkorozchodné	úzkorozchodný	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
na	na	k7c4
standardní	standardní	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
kolejí	kolej	k1gFnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
klasifikována	klasifikován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
mini-šinkansen	mini-šinkansen	k1gInSc1
<g/>
,	,	kIx,
povolená	povolený	k2eAgFnSc1d1
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
130	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Byla	být	k5eAaImAgFnS
zde	zde	k6eAd1
navíc	navíc	k6eAd1
ponechána	ponechat	k5eAaPmNgFnS
původní	původní	k2eAgFnSc1d1
elektrická	elektrický	k2eAgFnSc1d1
napájecí	napájecí	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
pro	pro	k7c4
napětí	napětí	k1gNnSc4
20	#num#	k4
kV	kV	k?
50	#num#	k4
Hz	Hz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
města	město	k1gNnSc2
<g/>
:	:	kIx,
Fukušima	Fukušim	k1gMnSc4
<g/>
,	,	kIx,
Jamagata	Jamagat	k1gMnSc4
<g/>
,	,	kIx,
Šindžó	Šindžó	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Akita-šinkansen	Akita-šinkansen	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
,	,	kIx,
Morioka	Morioka	k1gFnSc1
-	-	kIx~
Akita	Akita	k1gMnSc1
<g/>
,	,	kIx,
127	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
zde	zde	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
mini-šinkansen	mini-šinkansen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
města	město	k1gNnSc2
<g/>
:	:	kIx,
Morioka	Morioek	k1gMnSc4
<g/>
,	,	kIx,
Akita	Akit	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
těchto	tento	k3xDgInPc2
osmi	osm	k4xCc2
tratí	trať	k1gFnPc2
dále	daleko	k6eAd2
existují	existovat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
tratě	trať	k1gFnPc1
s	s	k7c7
normálním	normální	k2eAgInSc7d1
rozchodem	rozchod	k1gInSc7
kolejí	kolej	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
sice	sice	k8xC
nejsou	být	k5eNaImIp3nP
klasifikovány	klasifikovat	k5eAaImNgInP
jako	jako	k8xC,k8xS
Šinkansen	Šinkansen	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
provoz	provoz	k1gInSc4
na	na	k7c6
nich	on	k3xPp3gNnPc6
zajišťují	zajišťovat	k5eAaImIp3nP
stejné	stejný	k2eAgInPc1d1
typy	typ	k1gInPc1
vlaků	vlak	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
na	na	k7c6
výše	vysoce	k6eAd2
uvedených	uvedený	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
Linka	linka	k1gFnSc1
Hakata-minami	Hakata-mina	k1gFnPc7
Původně	původně	k6eAd1
manipulační	manipulační	k2eAgFnSc1d1
trať	trať	k1gFnSc1
z	z	k7c2
terminálu	terminál	k1gInSc2
Hakata	Hakat	k1gMnSc2
ve	v	k7c6
Fukuoce	Fukuoka	k1gFnSc6
do	do	k7c2
hlavního	hlavní	k2eAgNnSc2d1
depa	depo	k1gNnSc2
vysokorychlostních	vysokorychlostní	k2eAgInPc2d1
vlaků	vlak	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
nacházelo	nacházet	k5eAaImAgNnS
v	v	k7c6
řídce	řídce	k6eAd1
osídlené	osídlený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
v	v	k7c6
sousedství	sousedství	k1gNnSc6
města	město	k1gNnSc2
Kasuga	Kasug	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
se	se	k3xPyFc4
ale	ale	k9
rychle	rychle	k6eAd1
rozrostlo	rozrůst	k5eAaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
řešit	řešit	k5eAaImF
jeho	jeho	k3xOp3gFnSc4
dopravní	dopravní	k2eAgFnSc4d1
obslužnost	obslužnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsnazší	snadný	k2eAgNnSc1d3
bylo	být	k5eAaImAgNnS
postavit	postavit	k5eAaPmF
na	na	k7c4
již	již	k6eAd1
existující	existující	k2eAgFnPc4d1
trati	trať	k1gFnPc4
stanici	stanice	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
nazvána	nazvat	k5eAaBmNgFnS,k5eAaPmNgFnS
Hakata-minami	Hakata-mina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provoz	provoz	k1gInSc1
zde	zde	k6eAd1
zajišťují	zajišťovat	k5eAaImIp3nP
starší	starý	k2eAgInPc1d2
typy	typ	k1gInPc1
vlaků	vlak	k1gInPc2
<g/>
,	,	kIx,
doba	doba	k1gFnSc1
jízdy	jízda	k1gFnSc2
je	být	k5eAaImIp3nS
asi	asi	k9
10	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
jízdenka	jízdenka	k1gFnSc1
stojí	stát	k5eAaImIp3nS
290	#num#	k4
jenů	jen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tuto	tento	k3xDgFnSc4
trať	trať	k1gFnSc4
bude	být	k5eAaImBp3nS
po	po	k7c6
dokončení	dokončení	k1gNnSc6
napojen	napojen	k2eAgInSc4d1
Kjúšú-šinkansen	Kjúšú-šinkansen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Linka	linka	k1gFnSc1
Gala-Juzawa	Gala-Juzawum	k1gNnSc2
Tato	tento	k3xDgFnSc1
trať	trať	k1gFnSc1
je	být	k5eAaImIp3nS
odbočkou	odbočka	k1gFnSc7
z	z	k7c2
Džóecu-šinkansenu	Džóecu-šinkansen	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
dopravní	dopravní	k2eAgFnSc1d1
obslužnost	obslužnost	k1gFnSc1
lyžařského	lyžařský	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
blízko	blízko	k7c2
města	město	k1gNnSc2
Juzawa	Juzawum	k1gNnSc2
<g/>
,	,	kIx,
asi	asi	k9
130	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Niigaty	Niigata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Začátek	začátek	k1gInSc1
</s>
<s>
Konec	konec	k1gInSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
tratě	trať	k1gFnSc2
</s>
<s>
Délka	délka	k1gFnSc1
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Dopravce	dopravce	k1gMnSc1
</s>
<s>
Otevření	otevření	k1gNnSc1
</s>
<s>
Roční	roční	k2eAgInSc1d1
počet	počet	k1gInSc1
pasažérů	pasažér	k1gMnPc2
</s>
<s>
Tókaidó	Tókaidó	k?
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
</s>
<s>
Tokio	Tokio	k1gNnSc1
</s>
<s>
Šin-Ósaka	Šin-Ósak	k1gMnSc4
</s>
<s>
515,4	515,4	k4
</s>
<s>
Středojaponská	Středojaponský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
1964	#num#	k4
</s>
<s>
143	#num#	k4
015	#num#	k4
000	#num#	k4
</s>
<s>
Sanjó	Sanjó	k?
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
</s>
<s>
Šin-Ósaka	Šin-Ósak	k1gMnSc4
</s>
<s>
Hakata	Hakata	k1gFnSc1
</s>
<s>
553.7	553.7	k4
</s>
<s>
Západojaponská	Západojaponský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
</s>
<s>
64	#num#	k4
355	#num#	k4
000	#num#	k4
</s>
<s>
Tóhoku	Tóhok	k1gInSc3
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
</s>
<s>
Tokio	Tokio	k1gNnSc1
</s>
<s>
Šin-Aomori	Šin-Aomori	k6eAd1
</s>
<s>
674,9	674,9	k4
</s>
<s>
Východojaponská	Východojaponský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
1982	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
</s>
<s>
76	#num#	k4
177	#num#	k4
000	#num#	k4
</s>
<s>
Džóecu	Džóecu	k6eAd1
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
</s>
<s>
Ómija	Ómija	k6eAd1
</s>
<s>
Niigata	Niigata	k1gFnSc1
</s>
<s>
269,5	269,5	k4
</s>
<s>
1982	#num#	k4
</s>
<s>
34	#num#	k4
831	#num#	k4
000	#num#	k4
</s>
<s>
Hokuriku	Hokurika	k1gFnSc4
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
</s>
<s>
Takasaki	Takasaki	k6eAd1
</s>
<s>
Kanazawa	Kanazawa	k6eAd1
</s>
<s>
345,4	345,4	k4
</s>
<s>
Východojaponská	Východojaponský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Západojaponská	Západojaponský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
1997	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
</s>
<s>
9	#num#	k4
420	#num#	k4
000	#num#	k4
</s>
<s>
Kjúšú	Kjúšú	k?
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
</s>
<s>
Hakata	Hakata	k1gFnSc1
</s>
<s>
Kagoshima-Chū	Kagoshima-Chū	k?
</s>
<s>
256,8	256,8	k4
</s>
<s>
Kjúšúská	Kjúšúský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
</s>
<s>
12	#num#	k4
143	#num#	k4
000	#num#	k4
</s>
<s>
Hokkaidó	Hokkaidó	k?
Šinkansen	Šinkansen	k1gInSc1
</s>
<s>
Šin-Aomori	Šin-Aomori	k6eAd1
</s>
<s>
Šin-Hakodate-Hokuto	Šin-Hakodate-Hokut	k2eAgNnSc1d1
</s>
<s>
148,9	148,9	k4
</s>
<s>
Hokkaidská	Hokkaidský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Technika	technika	k1gFnSc1
</s>
<s>
Koleje	kolej	k1gFnPc1
</s>
<s>
Tratě	trať	k1gFnPc4
systému	systém	k1gInSc2
Šinkansen	Šinkansna	k1gFnPc2
mají	mít	k5eAaImIp3nP
normální	normální	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
kolejí	kolej	k1gFnPc2
(	(	kIx(
<g/>
1435	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
a	a	k8xC
jsou	být	k5eAaImIp3nP
elektrifikovány	elektrifikován	k2eAgFnPc1d1
napětími	napětí	k1gNnPc7
25	#num#	k4
kV	kV	k?
60	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
západně	západně	k6eAd1
od	od	k7c2
Tokia	Tokio	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
25	#num#	k4
kV	kV	k?
50	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
východně	východně	k6eAd1
od	od	k7c2
Tokia	Tokio	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
20	#num#	k4
kV	kV	k?
50	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
trať	trať	k1gFnSc1
Jamagata-šinkansen	Jamagata-šinkansna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
bezpečnosti	bezpečnost	k1gFnSc3
a	a	k8xC
rychlosti	rychlost	k1gFnSc3
provozu	provoz	k1gInSc2
mají	mít	k5eAaImIp3nP
tratě	trať	k1gFnSc2
výhradně	výhradně	k6eAd1
mimoúrovňová	mimoúrovňový	k2eAgNnPc1d1
křížení	křížení	k1gNnPc1
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
druhy	druh	k1gInPc7
dopravy	doprava	k1gFnSc2
i	i	k8xC
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
železnicemi	železnice	k1gFnPc7
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
vedou	vést	k5eAaImIp3nP
po	po	k7c6
mostech	most	k1gInPc6
a	a	k8xC
v	v	k7c6
tunelech	tunel	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
trať	trať	k1gFnSc1
vedla	vést	k5eAaImAgFnS
přímo	přímo	k6eAd1
a	a	k8xC
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
co	co	k9
možná	možná	k9
největších	veliký	k2eAgInPc2d3
poloměrů	poloměr	k1gInPc2
výškových	výškový	k2eAgInPc2d1
a	a	k8xC
směrových	směrový	k2eAgInPc2d1
oblouků	oblouk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
opatření	opatření	k1gNnPc4
dovolují	dovolovat	k5eAaImIp3nP
dosahovat	dosahovat	k5eAaImF
vysokých	vysoký	k2eAgInPc2d1
provozních	provozní	k2eAgInPc2d1
rychlostí	rychlost	k1gFnSc7
<g/>
,	,	kIx,
standardem	standard	k1gInSc7
je	být	k5eAaImIp3nS
na	na	k7c6
většině	většina	k1gFnSc6
tratí	tratit	k5eAaImIp3nS
240	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Vlakové	vlakový	k2eAgFnPc1d1
soupravy	souprava	k1gFnPc1
</s>
<s>
Provoz	provoz	k1gInSc1
na	na	k7c6
síti	síť	k1gFnSc6
Šinkansen	Šinkansen	k1gInSc1
je	být	k5eAaImIp3nS
zajišťován	zajišťovat	k5eAaImNgInS
vysokorychlostními	vysokorychlostní	k2eAgFnPc7d1
elektrickými	elektrický	k2eAgFnPc7d1
článkovými	článkový	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
se	se	k3xPyFc4
za	za	k7c4
dobu	doba	k1gFnSc4
provozu	provoz	k1gInSc2
na	na	k7c6
síti	síť	k1gFnSc6
objevilo	objevit	k5eAaPmAgNnS
14	#num#	k4
různých	různý	k2eAgInPc2d1
typů	typ	k1gInPc2
souprav	souprava	k1gFnPc2
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
,	,	kIx,
další	další	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
typy	typ	k1gInPc4
slouží	sloužit	k5eAaImIp3nS
ke	k	k7c3
sledování	sledování	k1gNnSc3
stavu	stav	k1gInSc2
kolejí	kolej	k1gFnPc2
a	a	k8xC
na	na	k7c6
několika	několik	k4yIc6
dalších	další	k2eAgInPc6d1
typech	typ	k1gInPc6
byly	být	k5eAaImAgInP
prováděny	provádět	k5eAaImNgInP
vývojové	vývojový	k2eAgInPc1d1
testy	test	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Společným	společný	k2eAgInSc7d1
parametrem	parametr	k1gInSc7
pro	pro	k7c4
většinu	většina	k1gFnSc4
souprav	souprava	k1gFnPc2
je	být	k5eAaImIp3nS
délka	délka	k1gFnSc1
vozu	vůz	k1gInSc2
přibližně	přibližně	k6eAd1
25	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
na	na	k7c6
tratích	trať	k1gFnPc6
Tókaidó	Tókaidó	k1gMnSc2
a	a	k8xC
Sanjó	Sanjó	k1gMnSc2
jsou	být	k5eAaImIp3nP
soupravy	souprava	k1gFnPc1
běžně	běžně	k6eAd1
tvořeny	tvořit	k5eAaImNgFnP
šestnácti	šestnáct	k4xCc7
vozy	vůz	k1gInPc7
(	(	kIx(
<g/>
včetně	včetně	k7c2
čelních	čelní	k2eAgFnPc2d1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
se	se	k3xPyFc4
také	také	k9
vozí	vozit	k5eAaImIp3nP
cestující	cestující	k1gMnPc1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
českém	český	k2eAgInSc6d1
Pendolinu	Pendolin	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
souprava	souprava	k1gFnSc1
má	mít	k5eAaImIp3nS
pak	pak	k6eAd1
délku	délka	k1gFnSc4
téměř	téměř	k6eAd1
400	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jiných	jiný	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
jsou	být	k5eAaImIp3nP
soupravy	souprava	k1gFnPc1
řazeny	řadit	k5eAaImNgFnP
do	do	k7c2
šesti-	šesti-	k?
<g/>
,	,	kIx,
sedmi-	sedmi-	k?
nebo	nebo	k8xC
osmivozových	osmivozový	k2eAgFnPc2d1
formací	formace	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
některých	některý	k3yIgFnPc6
tratích	trať	k1gFnPc6
jezdí	jezdit	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
(	(	kIx(
<g/>
nejvýše	vysoce	k6eAd3,k6eAd1
osmivozové	osmivozový	k2eAgFnPc4d1
<g/>
)	)	kIx)
jednotky	jednotka	k1gFnPc4
spojené	spojený	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Váha	váha	k1gFnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
prázdných	prázdný	k2eAgInPc2d1
vozů	vůz	k1gInPc2
se	se	k3xPyFc4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
typu	typ	k1gInSc6
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
40	#num#	k4
a	a	k8xC
55	#num#	k4
tunami	tuna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavostí	zajímavost	k1gFnPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
žádný	žádný	k3yNgInSc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
používaný	používaný	k2eAgInSc1d1
typ	typ	k1gInSc1
vlaků	vlak	k1gInPc2
nemá	mít	k5eNaImIp3nS
naklápěcí	naklápěcí	k2eAgFnPc4d1
vozové	vozový	k2eAgFnPc4d1
skříně	skříň	k1gFnPc4
<g/>
,	,	kIx,
naklonění	naklonění	k1gNnSc4
vlaku	vlak	k1gInSc2
ve	v	k7c6
směrových	směrový	k2eAgInPc6d1
obloucích	oblouk	k1gInPc6
je	být	k5eAaImIp3nS
řešeno	řešit	k5eAaImNgNnS
přímo	přímo	k6eAd1
při	při	k7c6
stavbě	stavba	k1gFnSc6
tratí	trať	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlak	vlak	k1gInSc1
s	s	k7c7
naklápěcími	naklápěcí	k2eAgFnPc7d1
vozovými	vozový	k2eAgFnPc7d1
skříněmi	skříň	k1gFnPc7
je	být	k5eAaImIp3nS
zatím	zatím	k6eAd1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
typem	typ	k1gInSc7
vlaku	vlak	k1gInSc2
na	na	k7c6
trati	trať	k1gFnSc6
Tókaidó	Tókaidó	k1gFnSc2
a	a	k8xC
historicky	historicky	k6eAd1
prvním	první	k4xOgInSc7
vysokorychlostním	vysokorychlostní	k2eAgInSc7d1
vlakem	vlak	k1gInSc7
na	na	k7c6
světě	svět	k1gInSc6
byly	být	k5eAaImAgFnP
soupravy	souprava	k1gFnPc1
řady	řada	k1gFnSc2
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
nejvyšší	vysoký	k2eAgFnSc4d3
rychlost	rychlost	k1gFnSc4
byla	být	k5eAaImAgFnS
210	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
váha	váha	k1gFnSc1
vozu	vůz	k1gInSc2
55	#num#	k4
tun	tuna	k1gFnPc2
<g/>
,	,	kIx,
celkový	celkový	k2eAgInSc1d1
výkon	výkon	k1gInSc1
11	#num#	k4
840	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
16	#num#	k4
<g/>
-vozová	-vozový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
)	)	kIx)
poháněné	poháněný	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
nápravy	náprava	k1gFnPc1
v	v	k7c6
soupravě	souprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
již	již	k6eAd1
dávno	dávno	k6eAd1
vyřazen	vyřadit	k5eAaPmNgMnS
z	z	k7c2
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
poslední	poslední	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
dosluhují	dosluhovat	k5eAaImIp3nP
na	na	k7c6
nejpomalejších	pomalý	k2eAgFnPc6d3
spojích	spoj	k1gFnPc6
na	na	k7c4
Sanjó-šinkansenu	Sanjó-šinkansen	k2eAgFnSc4d1
a	a	k8xC
na	na	k7c4
zmíněné	zmíněný	k2eAgFnPc4d1
trati	trať	k1gFnPc4
Hakata-minami	Hakata-mina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Provoz	provoz	k1gInSc1
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
Východojaponské	Východojaponský	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
Západojaponské	Západojaponský	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
Středojaponské	Středojaponský	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Šinkansen	Šinkansen	k1gInSc1
Kjúšúské	Kjúšúský	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Síť	síť	k1gFnSc1
Šinkansen	Šinkansna	k1gFnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
provozu	provoz	k1gInSc6
denně	denně	k6eAd1
od	od	k7c2
6.00	6.00	k4
do	do	k7c2
24.00	24.00	k4
<g/>
,	,	kIx,
noční	noční	k2eAgFnSc1d1
doba	doba	k1gFnSc1
je	být	k5eAaImIp3nS
vyhrazena	vyhradit	k5eAaPmNgFnS
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
a	a	k8xC
opravy	oprava	k1gFnPc4
tratí	trať	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Provozovatelé	provozovatel	k1gMnPc1
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
vzniku	vznik	k1gInSc2
prvních	první	k4xOgFnPc2
tratí	trať	k1gFnPc2
existovala	existovat	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
celostátní	celostátní	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Japonské	japonský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
spravovala	spravovat	k5eAaImAgFnS
většinu	většina	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
všechny	všechen	k3xTgInPc4
<g/>
,	,	kIx,
železniční	železniční	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
kvůli	kvůli	k7c3
předlužení	předlužení	k1gNnSc3
zprivatizována	zprivatizovat	k5eAaPmNgFnS
a	a	k8xC
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
šest	šest	k4xCc4
osobních	osobní	k2eAgInPc2d1
a	a	k8xC
jednu	jeden	k4xCgFnSc4
nákladní	nákladní	k2eAgFnSc4d1
železniční	železniční	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současný	současný	k2eAgInSc1d1
provoz	provoz	k1gInSc1
na	na	k7c6
tratích	trať	k1gFnPc6
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
zabezpečen	zabezpečit	k5eAaPmNgInS
takto	takto	k6eAd1
:	:	kIx,
</s>
<s>
Tókaidó-šinkansen	Tókaidó-šinkansen	k1gInSc1
<g/>
:	:	kIx,
Západojaponská	Západojaponský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
(	(	kIx(
<g/>
Ósaka	Ósaka	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Středojaponská	Středojaponský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
(	(	kIx(
<g/>
Nagoja	Nagoja	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Sanjó-šinkansen	Sanjó-šinkansen	k1gInSc1
<g/>
:	:	kIx,
Západojaponská	Západojaponský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Tóhoku	Tóhok	k1gInSc2
<g/>
,	,	kIx,
Džóecu	Džóecus	k1gInSc2
<g/>
,	,	kIx,
Akita	Akita	k1gFnSc1
<g/>
,	,	kIx,
Jamagata	Jamagata	k1gFnSc1
<g/>
,	,	kIx,
Nagano-šinkansen	Nagano-šinkansen	k1gInSc1
<g/>
:	:	kIx,
Východojaponská	Východojaponský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
(	(	kIx(
<g/>
Tokio	Tokio	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Kjúšú-šinkansen	Kjúšú-šinkansen	k1gInSc1
<g/>
:	:	kIx,
Kjúšúská	Kjúšúský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
(	(	kIx(
<g/>
Fukuoka	Fukuoka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
vlakových	vlakový	k2eAgInPc2d1
spojů	spoj	k1gInPc2
</s>
<s>
Na	na	k7c6
většině	většina	k1gFnSc6
tratí	trať	k1gFnPc2
je	být	k5eAaImIp3nS
provoz	provoz	k1gInSc1
organizován	organizován	k2eAgInSc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
druhů	druh	k1gInPc2
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
se	se	k3xPyFc4
mezi	mezi	k7c7
sebou	se	k3xPyFc7
liší	lišit	k5eAaImIp3nP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
často	často	k6eAd1
zastavují	zastavovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Tókaidó-šinkansen	Tókaidó-šinkansen	k1gInSc1
a	a	k8xC
Sanjó-šinkansen	Sanjó-šinkansen	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Nozomi	Nozo	k1gFnPc7
je	být	k5eAaImIp3nS
nejrychlejším	rychlý	k2eAgInSc7d3
spojem	spoj	k1gInSc7
<g/>
,	,	kIx,
„	„	k?
<g/>
expresem	expres	k1gInSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Tokiem	Tokio	k1gNnSc7
a	a	k8xC
Ósakou	Ósaka	k1gFnSc7
(	(	kIx(
<g/>
515,4	515,4	k4
km	km	kA
<g/>
)	)	kIx)
zastavuje	zastavovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
Šinagawě	Šinagawa	k1gFnSc6
(	(	kIx(
<g/>
6,8	6,8	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jokohamě	Jokohama	k1gFnSc6
(	(	kIx(
<g/>
25,5	25,5	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nagoji	Nagoj	k1gInSc3
(	(	kIx(
<g/>
342	#num#	k4
km	km	kA
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Kjótu	Kjóto	k1gNnSc6
(	(	kIx(
<g/>
476	#num#	k4
km	km	kA
od	od	k7c2
Tokia	Tokio	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Tokia	Tokio	k1gNnSc2
jede	jet	k5eAaImIp3nS
Nozomi	Nozo	k1gFnPc7
čtyřikrát	čtyřikrát	k6eAd1
až	až	k9
sedmkrát	sedmkrát	k6eAd1
za	za	k7c4
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
naděje	naděje	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Hikari	Hikari	k6eAd1
zastavuje	zastavovat	k5eAaImIp3nS
jako	jako	k9
„	„	k?
<g/>
rychlík	rychlík	k1gInSc1
<g/>
“	“	k?
v	v	k7c6
hlavních	hlavní	k2eAgFnPc6d1
stanicích	stanice	k1gFnPc6
na	na	k7c6
trati	trať	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Tokiem	Tokio	k1gNnSc7
a	a	k8xC
Ósakou	Ósaka	k1gFnSc7
zastavuje	zastavovat	k5eAaImIp3nS
6	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
krát	krát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Tokia	Tokio	k1gNnSc2
jde	jít	k5eAaImIp3nS
každou	každý	k3xTgFnSc4
půlhodinu	půlhodina	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
název	název	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
paprsek	paprsek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Hikari	Hikari	k6eAd1
Rail	Rail	k1gInSc1
Star	Star	kA
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
Sanjó-šinkansen	Sanjó-šinkansen	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Kodama	Kodama	k1gFnSc1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
zastávkový	zastávkový	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
staví	stavit	k5eAaBmIp3nS,k5eAaPmIp3nS,k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
stanicích	stanice	k1gFnPc6
na	na	k7c6
trati	trať	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soupravy	souprava	k1gFnPc1
jezdí	jezdit	k5eAaImIp3nP
zvlášť	zvlášť	k6eAd1
z	z	k7c2
Tokia	Tokio	k1gNnSc2
do	do	k7c2
Ósaky	Ósaka	k1gFnSc2
a	a	k8xC
z	z	k7c2
Ósaky	Ósaka	k1gFnSc2
do	do	k7c2
Hakaty	Hakata	k1gFnSc2
a	a	k8xC
zpravidla	zpravidla	k6eAd1
na	na	k7c4
sebe	sebe	k3xPyFc4
nenavazují	navazovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úsek	úsek	k1gInSc1
Tokio	Tokio	k1gNnSc4
–	–	k?
Ósaka	Ósaka	k1gFnSc1
má	mít	k5eAaImIp3nS
15	#num#	k4
mezilehlých	mezilehlý	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
průměru	průměr	k1gInSc6
asi	asi	k9
30	#num#	k4
km	km	kA
na	na	k7c4
stanici	stanice	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
úsek	úsek	k1gInSc1
z	z	k7c2
Ósaky	Ósaka	k1gFnSc2
do	do	k7c2
Hakaty	Hakata	k1gFnSc2
jich	on	k3xPp3gMnPc2
má	mít	k5eAaImIp3nS
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Tokia	Tokio	k1gNnSc2
jede	jet	k5eAaImIp3nS
Kodama	Kodama	k1gFnSc1
každou	každý	k3xTgFnSc4
půlhodinu	půlhodina	k1gFnSc4
nebo	nebo	k8xC
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
ozvěna	ozvěna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kjúšú-šinkansen	Kjúšú-šinkansen	k1gInSc1
a	a	k8xC
Sanjó-šinkansen	Sanjó-šinkansen	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Mizuho	Mizuze	k6eAd1
</s>
<s>
Sakura	sakura	k1gFnSc1
</s>
<s>
Cubame	Cubam	k1gInSc5
znamená	znamenat	k5eAaImIp3nS
vlaštovka	vlaštovka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
symbolem	symbol	k1gInSc7
tohoto	tento	k3xDgInSc2
vlaku	vlak	k1gInSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
Kjúšú-šinkansen	Kjúšú-šinkansen	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Džóetsu-šinkansen	Džóetsu-šinkansen	k1gInSc1
</s>
<s>
Toki	Toki	k6eAd1
z	z	k7c2
Tokia	Tokio	k1gNnSc2
do	do	k7c2
Takasaki	Takasaki	k1gNnSc2
řadu	řada	k1gFnSc4
stanic	stanice	k1gFnPc2
projíždí	projíždět	k5eAaImIp3nP
<g/>
,	,	kIx,
ve	v	k7c6
zbytku	zbytek	k1gInSc6
své	svůj	k3xOyFgFnSc2
trasy	trasa	k1gFnSc2
je	být	k5eAaImIp3nS
zastávkový	zastávkový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazývá	nazývat	k5eAaImIp3nS
se	se	k3xPyFc4
podle	podle	k7c2
druhu	druh	k1gInSc2
japonského	japonský	k2eAgMnSc2d1
ptáka	pták	k1gMnSc2
–	–	k?
ibise	ibis	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
symbolem	symbol	k1gInSc7
pro	pro	k7c4
expres	expres	k1gInSc4
přecházející	přecházející	k2eAgInSc4d1
na	na	k7c6
původní	původní	k2eAgFnSc6d1
trati	trať	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tanigawa	Tanigawa	k6eAd1
je	být	k5eAaImIp3nS
zastávkový	zastávkový	k2eAgInSc1d1
vlak	vlak	k1gInSc1
vedený	vedený	k2eAgInSc1d1
pouze	pouze	k6eAd1
mezi	mezi	k7c7
Takasaki	Takasaki	k1gNnSc7
a	a	k8xC
Tokiem	Tokio	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Oba	dva	k4xCgInPc1
spoje	spoj	k1gInPc1
existují	existovat	k5eAaImIp3nP
také	také	k9
ve	v	k7c6
variantách	varianta	k1gFnPc6
MAX-Toki	MAX-Toki	k1gNnSc2
a	a	k8xC
MAX-Tanigawa	MAX-Tanigawum	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
zajišťovány	zajišťovat	k5eAaImNgInP
dvoupodlažními	dvoupodlažní	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
typu	typ	k1gInSc2
E1	E1	k1gFnPc2
nebo	nebo	k8xC
E	E	kA
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tóhoku-šinkansen	Tóhoku-šinkansen	k1gInSc1
</s>
<s>
Hajabusa	Hajabus	k1gMnSc4
</s>
<s>
Hajate	Hajat	k1gMnSc5
je	být	k5eAaImIp3nS
expresní	expresní	k2eAgInSc4d1
vlak	vlak	k1gInSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
trati	trať	k1gFnSc2
</s>
<s>
Jamabiko	Jamabika	k1gFnSc5
a	a	k8xC
MAX-Jamabiko	MAX-Jamabika	k1gFnSc5
jsou	být	k5eAaImIp3nP
expresy	expres	k1gInPc7
mezi	mezi	k7c7
Tokiem	Tokio	k1gNnSc7
a	a	k8xC
městem	město	k1gNnSc7
Sendai	Senda	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
zastávkové	zastávkový	k2eAgInPc1d1
do	do	k7c2
Morioky	Moriok	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Nasuno	Nasuna	k1gFnSc5
a	a	k8xC
MAX-Nasuno	MAX-Nasuna	k1gFnSc5
jsou	být	k5eAaImIp3nP
zastávkové	zastávkový	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
mezi	mezi	k7c7
Tokiem	Tokio	k1gNnSc7
a	a	k8xC
Korijamou	Korijama	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Akita-šinkansen	Akita-šinkansen	k1gInSc1
</s>
<s>
Komači	Komač	k1gInSc3
jede	jet	k5eAaImIp3nS
mezi	mezi	k7c7
Tokiem	Tokio	k1gNnSc7
a	a	k8xC
Moriokou	Morioký	k2eAgFnSc7d1
spojen	spojit	k5eAaPmNgInS
se	s	k7c7
soupravou	souprava	k1gFnSc7
vlaku	vlak	k1gInSc2
Jamabiko	Jamabika	k1gFnSc5
<g/>
,	,	kIx,
dále	daleko	k6eAd2
jako	jako	k9
zastávkový	zastávkový	k2eAgMnSc1d1
do	do	k7c2
Akity	Akita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jamagata-šinkansen	Jamagata-šinkansen	k1gInSc1
</s>
<s>
Cubasa	Cubasa	k1gFnSc1
jede	jet	k5eAaImIp3nS
mezi	mezi	k7c7
Tokiem	Tokio	k1gNnSc7
a	a	k8xC
Fukušimou	Fukušima	k1gFnSc7
spojen	spojit	k5eAaPmNgInS
se	s	k7c7
soupravou	souprava	k1gFnSc7
vlaku	vlak	k1gInSc2
Jamabiko	Jamabika	k1gFnSc5
<g/>
,	,	kIx,
dále	daleko	k6eAd2
jako	jako	k9
zastávkový	zastávkový	k2eAgMnSc1d1
do	do	k7c2
města	město	k1gNnSc2
Šindžó	Šindžó	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
křídla	křídlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Nagano-šinkansen	Nagano-šinkansen	k1gInSc1
</s>
<s>
Asama	Asama	k?
je	být	k5eAaImIp3nS
mezi	mezi	k7c7
Tokiem	Tokio	k1gNnSc7
a	a	k8xC
Takasaki	Takasaki	k1gNnSc7
veden	vést	k5eAaImNgMnS
jako	jako	k9
expres	expres	k6eAd1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
do	do	k7c2
Nagana	Nagano	k1gNnSc2
jako	jako	k8xC,k8xS
zastávkový	zastávkový	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmenován	pojmenovat	k5eAaPmNgInS
podle	podle	k7c2
sopky	sopka	k1gFnSc2
blízko	blízko	k7c2
Nagana	Nagano	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Tókaidó	Tókaidó	k?
honsen	honsen	k1gInSc1
<g/>
,	,	kIx,
nejzatíženější	zatížený	k2eAgFnSc1d3
klasická	klasický	k2eAgFnSc1d1
japonská	japonský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
o	o	k7c6
rozchodu	rozchod	k1gInSc6
1	#num#	k4
067	#num#	k4
mm	mm	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Chuo	Chuo	k6eAd1
Shinkansen	Shinkansen	k2eAgMnSc1d1
Maglev	Maglev	k1gMnSc1
Line	linout	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Railway	Railwa	k2eAgMnPc4d1
Technology	technolog	k1gMnPc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RYCHLÍK	Rychlík	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgInPc1d1
levitující	levitující	k2eAgInPc1d1
magnetické	magnetický	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
mají	mít	k5eAaImIp3nP
už	už	k6eAd1
za	za	k7c4
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
jezdit	jezdit	k5eAaImF
rychlostí	rychlost	k1gFnSc7
500	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Třikrát	třikrát	k6eAd1
rychleji	rychle	k6eAd2
než	než	k8xS
nejrychlejší	rychlý	k2eAgInPc1d3
vlaky	vlak	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
(	(	kIx(
<g/>
IHNED	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-12-06	2019-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JR	JR	kA
East	East	k1gInSc1
to	ten	k3xDgNnSc4
build	build	k6eAd1
ALFA-X	ALFA-X	k1gFnSc1
360	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
Shinkansen	Shinkansen	k2eAgInSc4d1
testbed	testbed	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Railway	Railwaum	k1gNnPc7
Gazette	Gazett	k1gMnSc5
International	International	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Railway	Railwaum	k1gNnPc7
Gazette	Gazett	k1gInSc5
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2017-07-07	2017-07-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
http://www.jreast-timetable.jp/1012/timetable/tt1039/1039010.html%5B%5D1	http://www.jreast-timetable.jp/1012/timetable/tt1039/1039010.html%5B%5D1	k4
2	#num#	k4
3	#num#	k4
http://www.hyperdia.com/	http://www.hyperdia.com/	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šinkansen	Šinkansna	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Šinkansen	Šinkansna	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vysokorychlostní	vysokorychlostní	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
>	>	kIx)
<g/>
249	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
AGV	AGV	kA
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
AVE	ave	k1gNnSc4
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
401	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
450	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
CRH	CRH	kA
(	(	kIx(
<g/>
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
460	#num#	k4
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
480	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
500	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
600	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
,	,	kIx,
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
610	#num#	k4
(	(	kIx(
<g/>
CH	Ch	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
Eurostar	Eurostar	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
/	/	kIx~
F	F	kA
/	/	kIx~
B	B	kA
<g/>
)	)	kIx)
•	•	k?
ICE	ICE	kA
(	(	kIx(
<g/>
D	D	kA
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
F	F	kA
/	/	kIx~
B	B	kA
/	/	kIx~
NL	NL	kA
/	/	kIx~
A	A	kA
/	/	kIx~
CH	Ch	kA
/	/	kIx~
E	E	kA
/	/	kIx~
RU	RU	kA
/	/	kIx~
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
KTX	KTX	kA
(	(	kIx(
<g/>
KR	KR	kA
<g/>
)	)	kIx)
•	•	k?
RENFE	RENFE	kA
S-120	S-120	k1gFnSc2
(	(	kIx(
<g/>
E	E	kA
/	/	kIx~
TR	TR	kA
<g/>
)	)	kIx)
•	•	k?
Šinkansen	Šinkansen	k1gInSc1
(	(	kIx(
<g/>
J	J	kA
/	/	kIx~
TW	TW	kA
<g/>
,	,	kIx,
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
Talgo	Talgo	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
E	E	kA
<g/>
)	)	kIx)
•	•	k?
TGV	TGV	kA
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
D	D	kA
/	/	kIx~
CH	Ch	kA
/	/	kIx~
I	I	kA
/	/	kIx~
E	E	kA
/	/	kIx~
B	B	kA
/	/	kIx~
LX	LX	kA
<g/>
)	)	kIx)
•	•	k?
Thalys	Thalysa	k1gFnPc2
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
B	B	kA
/	/	kIx~
NL	NL	kA
/	/	kIx~
D	D	kA
<g/>
)	)	kIx)
•	•	k?
V	v	k7c6
250	#num#	k4
(	(	kIx(
<g/>
B	B	kA
/	/	kIx~
NL	NL	kA
<g/>
)	)	kIx)
•	•	k?
Zefiro	Zefiro	k1gNnSc4
(	(	kIx(
<g/>
CN	CN	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
</s>
<s>
Transrapid	Transrapid	k1gInSc1
<g/>
*	*	kIx~
(	(	kIx(
<g/>
D	D	kA
/	/	kIx~
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
JR-Maglev	JR-Maglev	k1gFnSc1
<g/>
*	*	kIx~
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
•	•	k?
Swissmetro	Swissmetro	k1gNnSc1
<g/>
**	**	k?
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
)	)	kIx)
*	*	kIx~
<g/>
technologie	technologie	k1gFnSc1
Maglev	Maglev	k1gMnSc1
**	**	k?
<g/>
Maglev	Maglev	k1gMnSc1
ve	v	k7c6
vakuovém	vakuový	k2eAgInSc6d1
tunelu	tunel	k1gInSc6
(	(	kIx(
<g/>
Vactrain	Vactrain	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Železnice	železnice	k1gFnSc1
</s>
