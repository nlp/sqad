<s>
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
neaktivní	aktivní	k2eNgFnSc1d1	neaktivní
štítová	štítový	k2eAgFnSc1d1	štítová
sopka	sopka	k1gFnSc1	sopka
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Havaj	Havaj	k1gFnSc1	Havaj
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
celých	celý	k2eAgInPc2d1	celý
Havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
