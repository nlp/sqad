<s>
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
neaktivní	aktivní	k2eNgFnSc1d1	neaktivní
štítová	štítový	k2eAgFnSc1d1	štítová
sopka	sopka	k1gFnSc1	sopka
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Havaj	Havaj	k1gFnSc1	Havaj
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
celých	celý	k2eAgInPc2d1	celý
Havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
oceánu	oceán	k1gInSc2	oceán
ční	čnět	k5eAaImIp3nP	čnět
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
4205	[number]	k4	4205
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
podvodní	podvodní	k2eAgFnSc7d1	podvodní
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
vysoká	vysoká	k1gFnSc1	vysoká
10	[number]	k4	10
205	[number]	k4	205
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nP	činit
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
od	od	k7c2	od
báze	báze	k1gFnSc2	báze
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
i	i	k9	i
Everest	Everest	k1gInSc1	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
štítovou	štítový	k2eAgFnSc4d1	štítová
sopku	sopka	k1gFnSc4	sopka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nad	nad	k7c7	nad
horkou	horký	k2eAgFnSc7d1	horká
skvrnou	skvrna	k1gFnSc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
posunu	posun	k1gInSc2	posun
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
posunula	posunout	k5eAaPmAgFnS	posunout
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
přívodního	přívodní	k2eAgInSc2d1	přívodní
kanálu	kanál	k1gInSc2	kanál
magmatu	magma	k1gNnSc2	magma
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vyhasnutí	vyhasnutí	k1gNnSc3	vyhasnutí
sopky	sopka	k1gFnSc2	sopka
a	a	k8xC	a
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nové	nový	k2eAgFnSc2d1	nová
sopky	sopka	k1gFnSc2	sopka
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
chrlila	chrlit	k5eAaImAgFnS	chrlit
lávu	láva	k1gFnSc4	láva
několikrát	několikrát	k6eAd1	několikrát
<g/>
,	,	kIx,	,
naposled	naposled	k6eAd1	naposled
asi	asi	k9	asi
před	před	k7c7	před
3600	[number]	k4	3600
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
jiné	jiný	k2eAgFnPc1d1	jiná
sopky	sopka	k1gFnPc1	sopka
např.	např.	kA	např.
Mauna	Maun	k1gMnSc4	Maun
Loa	Loa	k1gMnSc4	Loa
<g/>
,	,	kIx,	,
Hualā	Hualā	k1gMnSc4	Hualā
<g/>
,	,	kIx,	,
Mahukona	Mahukon	k1gMnSc4	Mahukon
<g/>
,	,	kIx,	,
Kohala	Kohal	k1gMnSc4	Kohal
a	a	k8xC	a
Kilauea	Kilaueus	k1gMnSc4	Kilaueus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
12	[number]	k4	12
<g/>
.	.	kIx.	.
až	až	k9	až
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
první	první	k4xOgMnPc1	první
starověcí	starověký	k2eAgMnPc1d1	starověký
obyvatelé	obyvatel	k1gMnPc1	obyvatel
havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
usazovat	usazovat	k5eAaImF	usazovat
i	i	k9	i
na	na	k7c4	na
úbočí	úbočí	k1gNnPc4	úbočí
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nacházeli	nacházet	k5eAaImAgMnP	nacházet
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
vhodný	vhodný	k2eAgInSc4d1	vhodný
úkryt	úkryt	k1gInSc4	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
sopky	sopka	k1gFnSc2	sopka
přeměnili	přeměnit	k5eAaPmAgMnP	přeměnit
lesy	les	k1gInPc4	les
na	na	k7c4	na
pastviny	pastvina	k1gFnPc4	pastvina
a	a	k8xC	a
vyhubili	vyhubit	k5eAaPmAgMnP	vyhubit
některá	některý	k3yIgNnPc4	některý
tam	tam	k6eAd1	tam
žijící	žijící	k2eAgNnPc4d1	žijící
zvířata	zvíře	k1gNnPc4	zvíře
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
nelétavých	létavý	k2eNgMnPc2d1	nelétavý
ptáků	pták	k1gMnPc2	pták
<g/>
)	)	kIx)	)
neznající	znající	k2eNgMnSc1d1	neznající
dosud	dosud	k6eAd1	dosud
predátory	predátor	k1gMnPc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
hora	hora	k1gFnSc1	hora
hrála	hrát	k5eAaImAgFnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
bájesloví	bájesloví	k1gNnSc6	bájesloví
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
uctívána	uctíván	k2eAgFnSc1d1	uctívána
jako	jako	k8xS	jako
posvátná	posvátný	k2eAgFnSc1d1	posvátná
a	a	k8xC	a
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
směli	smět	k5eAaImAgMnP	smět
jen	jen	k9	jen
náčelníci	náčelník	k1gMnPc1	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
svahů	svah	k1gInPc2	svah
odlesnili	odlesnit	k5eAaPmAgMnP	odlesnit
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
novodobí	novodobý	k2eAgMnPc1d1	novodobý
osadníci	osadník	k1gMnPc1	osadník
pro	pro	k7c4	pro
výsadbu	výsadba	k1gFnSc4	výsadba
plantáží	plantáž	k1gFnPc2	plantáž
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
pro	pro	k7c4	pro
potřebu	potřeba	k1gFnSc4	potřeba
dřeva	dřevo	k1gNnSc2	dřevo
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
postavena	postaven	k2eAgFnSc1d1	postavena
silnice	silnice	k1gFnSc1	silnice
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
stala	stát	k5eAaPmAgFnS	stát
velmi	velmi	k6eAd1	velmi
vyhledávanou	vyhledávaný	k2eAgFnSc7d1	vyhledávaná
turistickou	turistický	k2eAgFnSc7d1	turistická
oblastí	oblast	k1gFnSc7	oblast
s	s	k7c7	s
dosud	dosud	k6eAd1	dosud
ještě	ještě	k9	ještě
místy	místy	k6eAd1	místy
bohatou	bohatý	k2eAgFnSc7d1	bohatá
exotickou	exotický	k2eAgFnSc7d1	exotická
faunou	fauna	k1gFnSc7	fauna
a	a	k8xC	a
florou	flora	k1gFnSc7	flora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc4	její
vrchol	vrchol	k1gInSc4	vrchol
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
sněhem	sníh	k1gInSc7	sníh
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
k	k	k7c3	k
lyžování	lyžování	k1gNnSc3	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mauna	Maun	k1gMnSc4	Maun
Kea	Kea	k1gMnSc4	Kea
jsou	být	k5eAaImIp3nP	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
vynikajícím	vynikající	k2eAgFnPc3d1	vynikající
přírodním	přírodní	k2eAgFnPc3d1	přírodní
i	i	k8xC	i
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
podmínkám	podmínka	k1gFnPc3	podmínka
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Mauna	Maun	k1gInSc2	Maun
Kea	Kea	k1gMnSc1	Kea
Observatories	Observatories	k1gMnSc1	Observatories
mnohé	mnohý	k2eAgFnPc4d1	mnohá
významné	významný	k2eAgFnPc4d1	významná
astronomické	astronomický	k2eAgFnPc4d1	astronomická
observatoře	observatoř	k1gFnPc4	observatoř
<g/>
:	:	kIx,	:
Caltech	Calt	k1gInPc6	Calt
Submillimeter	Submillimeter	k1gInSc1	Submillimeter
Observatory	Observator	k1gInPc1	Observator
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Canada-France-Hawaii	Canada-France-Hawaie	k1gFnSc6	Canada-France-Hawaie
Telescope	Telescop	k1gInSc5	Telescop
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Gemini	Gemin	k1gMnPc1	Gemin
Observatory	Observator	k1gInPc1	Observator
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
James	James	k1gInSc1	James
Clerk	Clerk	k1gInSc1	Clerk
Maxwell	maxwell	k1gInSc1	maxwell
Telescope	Telescop	k1gInSc5	Telescop
[	[	kIx(	[
<g/>
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
]	]	kIx)	]
Keck	Keck	k1gInSc1	Keck
Observatory	Observator	k1gInPc1	Observator
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
NASA	NASA	kA	NASA
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Infrared	Infrared	k1gInSc1	Infrared
Telescope	Telescop	k1gInSc5	Telescop
Facility	Facilita	k1gFnPc1	Facilita
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
Subaru	Subar	k1gInSc2	Subar
Telescope	Telescop	k1gInSc5	Telescop
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
Submillimeter	Submillimeter	k1gInSc1	Submillimeter
Array	Arraa	k1gFnSc2	Arraa
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
United	United	k1gInSc1	United
Kingdom	Kingdom	k1gInSc1	Kingdom
Infrared	Infrared	k1gInSc1	Infrared
Telescope	Telescop	k1gInSc5	Telescop
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
University	universita	k1gFnSc2	universita
of	of	k?	of
Hawaii	Hawaie	k1gFnSc4	Hawaie
24	[number]	k4	24
<g/>
-inch	ncha	k1gFnPc2	-incha
and	and	k?	and
88	[number]	k4	88
<g/>
-inch	nch	k1gInSc1	-inch
telescope	telescop	k1gInSc5	telescop
[	[	kIx(	[
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
RUBIN	RUBIN	kA	RUBIN
<g/>
,	,	kIx,	,
Kenneth	Kenneth	k1gMnSc1	Kenneth
H.	H.	kA	H.
Mauna	Mauna	k1gFnSc1	Mauna
Kea	Kea	k1gFnSc1	Kea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Hawaii	Hawaie	k1gFnSc4	Hawaie
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Volcanology	Volcanolog	k1gMnPc4	Volcanolog
<g/>
,	,	kIx,	,
University	universita	k1gFnSc2	universita
of	of	k?	of
Hawaii	Hawaie	k1gFnSc4	Hawaie
<g/>
,	,	kIx,	,
Honolulu	Honolulu	k1gNnSc4	Honolulu
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
25.02	[number]	k4	25.02
<g/>
.2008	.2008	k4	.2008
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Highest	Highest	k1gFnSc1	Highest
Mountain	Mountain	k1gInSc1	Mountain
in	in	k?	in
the	the	k?	the
World	World	k1gInSc1	World
<g/>
:	:	kIx,	:
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Geological	Geological	k1gFnSc1	Geological
Society	societa	k1gFnSc2	societa
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
,	,	kIx,	,
Boulder	Boulder	k1gMnSc1	Boulder
<g/>
,	,	kIx,	,
CO	co	k6eAd1	co
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
HON	hon	k1gInSc1	hon
<g/>
,	,	kIx,	,
Ken	Ken	k1gMnSc1	Ken
<g/>
.	.	kIx.	.
</s>
<s>
Geology	geolog	k1gMnPc4	geolog
of	of	k?	of
the	the	k?	the
Hawaiian	Hawaiian	k1gInSc1	Hawaiian
islands	islands	k1gInSc1	islands
<g/>
:	:	kIx,	:
Mauna	Mauen	k2eAgFnSc1d1	Mauna
Kea	Kea	k1gFnSc1	Kea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
Hawaii	Hawaie	k1gFnSc3	Hawaie
at	at	k?	at
Hilo	Hilo	k1gMnSc1	Hilo
<g/>
,	,	kIx,	,
Hilo	Hilo	k1gMnSc1	Hilo
<g/>
,,	,,	k?	,,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
WEST	WEST	kA	WEST
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
J.	J.	kA	J.
The	The	k1gMnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
Astronomy	astronom	k1gMnPc4	astronom
on	on	k3xPp3gMnSc1	on
Mauna	Mauen	k2eAgMnSc4d1	Mauen
Kea	Kea	k1gMnSc4	Kea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
J.	J.	kA	J.
West	West	k1gMnSc1	West
<g/>
,	,	kIx,	,
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mauna	Maun	k1gInSc2	Maun
Kea	Kea	k1gMnPc2	Kea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
