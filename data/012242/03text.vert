<p>
<s>
Minibus	minibus	k1gInSc1	minibus
je	být	k5eAaImIp3nS	být
silniční	silniční	k2eAgNnSc4d1	silniční
vozidlo	vozidlo	k1gNnSc4	vozidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
počtem	počet	k1gInSc7	počet
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
mikrobus	mikrobus	k1gInSc4	mikrobus
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
autobus	autobus	k1gInSc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
je	on	k3xPp3gFnPc4	on
častěji	často	k6eAd2	často
výrobci	výrobce	k1gMnPc1	výrobce
dodávkových	dodávkový	k2eAgInPc2d1	dodávkový
automobilů	automobil	k1gInPc2	automobil
než	než	k8xS	než
výrobci	výrobce	k1gMnPc1	výrobce
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
minibusy	minibus	k1gInPc1	minibus
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
s	s	k7c7	s
dodávkovými	dodávkový	k2eAgInPc7d1	dodávkový
vozy	vůz	k1gInPc7	vůz
shodnou	shodnout	k5eAaPmIp3nP	shodnout
konstrukci	konstrukce	k1gFnSc3	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minibusy	minibus	k1gInPc1	minibus
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
italské	italský	k2eAgFnSc2d1	italská
dodávky	dodávka	k1gFnSc2	dodávka
Iveco	Iveco	k1gNnSc1	Iveco
Daily	Daila	k1gFnSc2	Daila
nabízí	nabízet	k5eAaImIp3nS	nabízet
firma	firma	k1gFnSc1	firma
Irisbus	Irisbus	k1gMnSc1	Irisbus
Iveco	Iveco	k1gMnSc1	Iveco
<g/>
,	,	kIx,	,
nástupce	nástupce	k1gMnSc1	nástupce
vysokomýtské	vysokomýtský	k2eAgFnSc2d1	vysokomýtská
Karosy	karosa	k1gFnSc2	karosa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
české	český	k2eAgFnPc1d1	Česká
firmy	firma	k1gFnPc1	firma
nabízejí	nabízet	k5eAaImIp3nP	nabízet
minibusy	minibus	k1gInPc4	minibus
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
přestavbou	přestavba	k1gFnSc7	přestavba
různých	různý	k2eAgFnPc2d1	různá
evropských	evropský	k2eAgFnPc2d1	Evropská
dodávek	dodávka	k1gFnPc2	dodávka
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc1d3	nejčastější
cca	cca	kA	cca
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
místné	místný	k2eAgInPc4d1	místný
minibusy	minibus	k1gInPc4	minibus
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
dodávek	dodávka	k1gFnPc2	dodávka
Mercedes	mercedes	k1gInSc1	mercedes
Benz	Benz	k1gMnSc1	Benz
Sprinter	sprinter	k1gMnSc1	sprinter
<g/>
,	,	kIx,	,
Iveco	Iveco	k1gMnSc1	Iveco
Daily	Daila	k1gFnSc2	Daila
<g/>
,	,	kIx,	,
Volkswagen	volkswagen	k1gInSc1	volkswagen
LT	LT	kA	LT
<g/>
/	/	kIx~	/
<g/>
Crafter	Crafter	k1gMnSc1	Crafter
<g/>
,	,	kIx,	,
Ford	ford	k1gInSc1	ford
Transit	transit	k1gInSc1	transit
(	(	kIx(	(
<g/>
provedení	provedení	k1gNnSc1	provedení
"	"	kIx"	"
<g/>
Bus	bus	k1gInSc1	bus
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
menší	malý	k2eAgMnSc1d2	menší
<g/>
,	,	kIx,	,
cca	cca	kA	cca
dvanáctimístné	dvanáctimístný	k2eAgFnPc1d1	dvanáctimístná
<g/>
,	,	kIx,	,
minibusy	minibus	k1gInPc1	minibus
VW	VW	kA	VW
Transporter	Transporter	k1gInSc4	Transporter
<g/>
,	,	kIx,	,
Fiat	fiat	k1gInSc4	fiat
Ducato	Ducat	k2eAgNnSc1d1	Ducato
(	(	kIx(	(
<g/>
Peugeot	peugeot	k1gInSc1	peugeot
Boxer	boxer	k1gInSc1	boxer
<g/>
,	,	kIx,	,
Citroën	Citroën	k1gInSc1	Citroën
Jumper	jumper	k1gInSc1	jumper
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ford	ford	k1gInSc1	ford
Transit	transit	k1gInSc1	transit
(	(	kIx(	(
<g/>
do	do	k7c2	do
r.	r.	kA	r.
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Terminologie	terminologie	k1gFnSc1	terminologie
je	být	k5eAaImIp3nS	být
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
ustálená	ustálený	k2eAgFnSc1d1	ustálená
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
zdrojích	zdroj	k1gInPc6	zdroj
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
podskupinu	podskupina	k1gFnSc4	podskupina
minibusů	minibus	k1gInPc2	minibus
považovány	považován	k2eAgFnPc1d1	považována
i	i	k8xC	i
midibusy	midibus	k1gInPc7	midibus
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
10,5	[number]	k4	10,5
či	či	k8xC	či
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
typické	typický	k2eAgInPc1d1	typický
minibusy	minibus	k1gInPc1	minibus
i	i	k9	i
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
zdrojích	zdroj	k1gInPc6	zdroj
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
mikrobusy	mikrobus	k1gInPc1	mikrobus
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Mercedes	mercedes	k1gInSc1	mercedes
Benz	Benza	k1gFnPc2	Benza
Sprinter	sprinter	k1gMnSc1	sprinter
<g/>
,	,	kIx,	,
Iveco	Iveco	k1gMnSc1	Iveco
Daily	Daila	k1gFnSc2	Daila
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Samořídící	Samořídící	k2eAgInSc1d1	Samořídící
minibus	minibus	k1gInSc1	minibus
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
samořídícím	samořídící	k2eAgInSc7d1	samořídící
minibusem	minibus	k1gInSc7	minibus
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
mikrobusem	mikrobus	k1gInSc7	mikrobus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Olli	Olle	k1gFnSc4	Olle
<g/>
.	.	kIx.	.
</s>
<s>
Přepravuje	přepravovat	k5eAaImIp3nS	přepravovat
cestující	cestující	k1gMnPc4	cestující
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
firemním	firemní	k2eAgInSc6d1	firemní
areálu	areál	k1gInSc6	areál
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
8	[number]	k4	8
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
cestovní	cestovní	k2eAgFnSc1d1	cestovní
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
9	[number]	k4	9
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Překážkám	překážka	k1gFnPc3	překážka
se	se	k3xPyFc4	se
neumí	umět	k5eNaImIp3nS	umět
vyhnout	vyhnout	k5eAaPmF	vyhnout
a	a	k8xC	a
tak	tak	k6eAd1	tak
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
<g/>
;	;	kIx,	;
občas	občas	k6eAd1	občas
"	"	kIx"	"
<g/>
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
"	"	kIx"	"
překážku	překážka	k1gFnSc4	překážka
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
žádná	žádný	k3yNgNnPc1	žádný
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
autobus	autobus	k1gInSc1	autobus
</s>
</p>
<p>
<s>
mikrobus	mikrobus	k1gInSc1	mikrobus
</s>
</p>
<p>
<s>
midibus	midibus	k1gMnSc1	midibus
</s>
</p>
<p>
<s>
dodávkový	dodávkový	k2eAgInSc1d1	dodávkový
automobil	automobil	k1gInSc1	automobil
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
minibus	minibus	k1gInSc1	minibus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
