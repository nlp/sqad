<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
silniční	silniční	k2eAgNnSc1d1	silniční
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
počtem	počet	k1gInSc7	počet
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
mikrobus	mikrobus	k1gInSc4	mikrobus
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
autobus	autobus	k1gInSc4	autobus
<g/>
?	?	kIx.	?
</s>
