<s>
Morfologie	morfologie	k1gFnSc1	morfologie
(	(	kIx(	(
<g/>
tvarové	tvarový	k2eAgNnSc1d1	tvarové
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
)	)	kIx)	)
těla	tělo	k1gNnSc2	tělo
řádně	řádně	k6eAd1	řádně
spiralizovaného	spiralizovaný	k2eAgInSc2d1	spiralizovaný
chromozomu	chromozom	k1gInSc2	chromozom
je	být	k5eAaImIp3nS	být
nejlépe	dobře	k6eAd3	dobře
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
metafáze	metafáze	k1gFnSc2	metafáze
nebo	nebo	k8xC	nebo
rané	raný	k2eAgFnSc2d1	raná
anafáze	anafáze	k1gFnSc2	anafáze
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
fázích	fág	k1gInPc6	fág
jaderného	jaderný	k2eAgNnSc2d1	jaderné
dělení	dělení	k1gNnSc2	dělení
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
zkreslován	zkreslovat	k5eAaImNgMnS	zkreslovat
despiralizací	despiralizace	k1gFnSc7	despiralizace
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
též	též	k9	též
dekondezací	dekondezací	k1gFnSc2	dekondezací
<g/>
)	)	kIx)	)
chromozomu	chromozom	k1gInSc2	chromozom
<g/>
.	.	kIx.	.
</s>
