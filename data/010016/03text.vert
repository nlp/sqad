<p>
<s>
Christiane	Christian	k1gMnSc5	Christian
F.	F.	kA	F.
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Christiane	Christian	k1gMnSc5	Christian
Vera	Vera	k1gFnSc1	Vera
Felscherinow	Felscherinow	k1gMnPc3	Felscherinow
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgFnSc1d1	ústřední
postava	postava	k1gFnSc1	postava
knihy	kniha	k1gFnSc2	kniha
My	my	k3xPp1nPc1	my
děti	dítě	k1gFnPc4	dítě
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
ZOO	zoo	k1gFnSc2	zoo
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Christiane	Christian	k1gMnSc5	Christian
F.	F.	kA	F.
je	být	k5eAaImIp3nS	být
autentická	autentický	k2eAgFnSc1d1	autentická
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
životní	životní	k2eAgInSc1d1	životní
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
knihu	kniha	k1gFnSc4	kniha
i	i	k9	i
pro	pro	k7c4	pro
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
autobiografické	autobiografický	k2eAgNnSc4d1	autobiografické
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
osudu	osud	k1gInSc6	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Christiane	Christian	k1gMnSc5	Christian
F.	F.	kA	F.
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
násilí	násilí	k1gNnSc1	násilí
a	a	k8xC	a
alkoholismus	alkoholismus	k1gInSc1	alkoholismus
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
otce	otka	k1gFnSc3	otka
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
pořadu	pořad	k1gInSc6	pořad
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přistěhovala	přistěhovat	k5eAaPmAgFnS	přistěhovat
do	do	k7c2	do
již	již	k9	již
tehdy	tehdy	k6eAd1	tehdy
problematického	problematický	k2eAgNnSc2d1	problematické
sídliště	sídliště	k1gNnSc2	sídliště
Gropiusstadt	Gropiusstadta	k1gFnPc2	Gropiusstadta
v	v	k7c6	v
západoberlínské	západoberlínský	k2eAgFnSc6d1	západoberlínská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Neukölln	Neuköllna	k1gFnPc2	Neuköllna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
příležitosti	příležitost	k1gFnSc6	příležitost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
otec	otec	k1gMnSc1	otec
fyzicky	fyzicky	k6eAd1	fyzicky
ohrožoval	ohrožovat	k5eAaImAgMnS	ohrožovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
bezúspěšně	bezúspěšně	k6eAd1	bezúspěšně
<g/>
)	)	kIx)	)
pokusila	pokusit	k5eAaPmAgFnS	pokusit
skočit	skočit	k5eAaPmF	skočit
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Rodinné	rodinný	k2eAgInPc1d1	rodinný
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
násilí	násilí	k1gNnSc4	násilí
proti	proti	k7c3	proti
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
ještě	ještě	k9	ještě
něčím	něco	k3yInSc7	něco
<g/>
,	,	kIx,	,
čemu	co	k3yInSc3	co
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
tabu	tabu	k1gNnSc1	tabu
<g/>
.	.	kIx.	.
</s>
<s>
Christiane	Christian	k1gMnSc5	Christian
F.	F.	kA	F.
neviděla	vidět	k5eNaImAgFnS	vidět
východiska	východisko	k1gNnSc2	východisko
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
dvanácti	dvanáct	k4xCc2	dvanáct
let	léto	k1gNnPc2	léto
do	do	k7c2	do
drogové	drogový	k2eAgFnSc2d1	drogová
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Napřed	napřed	k6eAd1	napřed
hašiš	hašiš	k1gInSc1	hašiš
<g/>
,	,	kIx,	,
marihuana	marihuana	k1gFnSc1	marihuana
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
heroinem	heroin	k1gInSc7	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnSc2	svůj
dávky	dávka	k1gFnSc2	dávka
mohla	moct	k5eAaImAgFnS	moct
opatřit	opatřit	k5eAaPmF	opatřit
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
prostituovat	prostituovat	k5eAaBmF	prostituovat
na	na	k7c6	na
nechvalně	chvalně	k6eNd1	chvalně
známém	známý	k2eAgNnSc6d1	známé
západoberlínském	západoberlínský	k2eAgNnSc6d1	západoberlínský
nádraží	nádraží	k1gNnSc6	nádraží
Bahnhof	Bahnhof	k1gInSc4	Bahnhof
ZOO	zoo	k1gNnSc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
mezitím	mezitím	k6eAd1	mezitím
rozvedená	rozvedený	k2eAgFnSc1d1	rozvedená
matka	matka	k1gFnSc1	matka
si	se	k3xPyFc3	se
toho	ten	k3xDgMnSc2	ten
všimla	všimnout	k5eAaPmAgFnS	všimnout
až	až	k9	až
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
šestnácti	šestnáct	k4xCc2	šestnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vypovídala	vypovídat	k5eAaImAgFnS	vypovídat
Christiane	Christian	k1gMnSc5	Christian
F.	F.	kA	F.
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
procesu	proces	k1gInSc6	proces
jako	jako	k8xS	jako
svědkyně	svědkyně	k1gFnSc2	svědkyně
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výpověď	výpověď	k1gFnSc1	výpověď
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
dva	dva	k4xCgMnPc4	dva
reportéry	reportér	k1gMnPc4	reportér
známého	známý	k1gMnSc2	známý
časopisu	časopis	k1gInSc2	časopis
Stern	sternum	k1gNnPc2	sternum
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
ji	on	k3xPp3gFnSc4	on
pozvali	pozvat	k5eAaPmAgMnP	pozvat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
k	k	k7c3	k
rozhovoru	rozhovor	k1gInSc3	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
dva	dva	k4xCgInPc1	dva
měsíce	měsíc	k1gInPc1	měsíc
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
autobiografické	autobiografický	k2eAgNnSc4d1	autobiografické
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
zvrátilo	zvrátit	k5eAaPmAgNnS	zvrátit
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
názory	názor	k1gInPc4	názor
západoněmecké	západoněmecký	k2eAgFnSc2d1	západoněmecká
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
mladistvé	mladistvý	k1gMnPc4	mladistvý
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
časopisu	časopis	k1gInSc2	časopis
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
<g/>
.	.	kIx.	.
</s>
<s>
Christiane	Christian	k1gMnSc5	Christian
měla	mít	k5eAaImAgNnP	mít
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
narkomani	narkoman	k1gMnPc1	narkoman
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
už	už	k6eAd1	už
konečně	konečně	k6eAd1	konečně
bylo	být	k5eAaImAgNnS	být
prolomeno	prolomen	k2eAgNnSc4d1	prolomeno
mlčení	mlčení	k1gNnSc4	mlčení
o	o	k7c6	o
drogové	drogový	k2eAgFnSc6d1	drogová
závislosti	závislost	k1gFnSc6	závislost
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
narkomanské	narkomanský	k2eAgFnSc2d1	narkomanská
party	parta	k1gFnSc2	parta
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dosud	dosud	k6eAd1	dosud
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
podpořili	podpořit	k5eAaPmAgMnP	podpořit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Christane	Christat	k5eAaPmIp3nS	Christat
byla	být	k5eAaImAgFnS	být
ve	v	k7c4	v
svých	svůj	k3xOyFgFnPc2	svůj
14	[number]	k4	14
letech	let	k1gInPc6	let
poslána	poslat	k5eAaPmNgFnS	poslat
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
blízko	blízko	k7c2	blízko
Hamburku	Hamburk	k1gInSc2	Hamburk
k	k	k7c3	k
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
budoucím	budoucí	k2eAgMnSc7d1	budoucí
přítelem	přítel	k1gMnSc7	přítel
Alexandrem	Alexandr	k1gMnSc7	Alexandr
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
pak	pak	k6eAd1	pak
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
–	–	k?	–
potom	potom	k6eAd1	potom
ji	on	k3xPp3gFnSc4	on
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
pobývala	pobývat	k5eAaImAgFnS	pobývat
asi	asi	k9	asi
6	[number]	k4	6
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
zde	zde	k6eAd1	zde
poznala	poznat	k5eAaPmAgFnS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
turbulentních	turbulentní	k2eAgInPc6d1	turbulentní
letech	let	k1gInPc6	let
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zpátky	zpátky	k6eAd1	zpátky
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Jan-Niklas	Jan-Niklas	k1gMnSc1	Jan-Niklas
Philip	Philip	k1gMnSc1	Philip
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc7	její
heroinová	heroinový	k2eAgFnSc1d1	heroinová
"	"	kIx"	"
<g/>
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
"	"	kIx"	"
pořád	pořád	k6eAd1	pořád
opakovala	opakovat	k5eAaImAgFnS	opakovat
a	a	k8xC	a
než	než	k8xS	než
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
,	,	kIx,	,
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
několik	několik	k4yIc4	několik
léčení	léčení	k1gNnPc2	léčení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
propadla	propadnout	k5eAaPmAgFnS	propadnout
znovu	znovu	k6eAd1	znovu
drogám	droga	k1gFnPc3	droga
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
jedenáctiletý	jedenáctiletý	k2eAgMnSc1d1	jedenáctiletý
syn	syn	k1gMnSc1	syn
jí	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
odebrán	odebrán	k2eAgInSc1d1	odebrán
<g/>
.	.	kIx.	.
<g/>
Detlef	Detlef	k1gInSc1	Detlef
(	(	kIx(	(
<g/>
její	její	k3xOp3gMnSc1	její
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
přítel	přítel	k1gMnSc1	přítel
v	v	k7c6	v
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgFnSc2d3	veliký
závislosti	závislost	k1gFnSc2	závislost
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
řidič	řidič	k1gMnSc1	řidič
autobusu	autobus	k1gInSc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
interview	interview	k1gNnSc6	interview
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Christiane	Christian	k1gMnSc5	Christian
nikdy	nikdy	k6eAd1	nikdy
nemiloval	milovat	k5eNaImAgInS	milovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Christiane	Christian	k1gMnSc5	Christian
F.	F.	kA	F.
Community	Communit	k2eAgMnPc4d1	Communit
</s>
</p>
