<s>
Wolfram	wolfram	k1gInSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
W	W	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Wolframium	Wolframium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šedý	šedý	k2eAgInSc1d1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
těžký	těžký	k2eAgInSc1d1
a	a	k8xC
mimořádně	mimořádně	k6eAd1
obtížně	obtížně	k6eAd1
tavitelný	tavitelný	k2eAgInSc1d1
kov	kov	k1gInSc1
(	(	kIx(
<g/>
jeho	jeho	k3xOp3gFnSc1
teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc1d3
ze	z	k7c2
všech	všecek	k3xTgInPc2
kovů	kov	k1gInPc2
a	a	k8xC
po	po	k7c6
uhlíku	uhlík	k1gInSc6
druhá	druhý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
z	z	k7c2
prvků	prvek	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>