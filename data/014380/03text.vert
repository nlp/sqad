<s>
Wolfram	wolfram	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
chemickém	chemický	k2eAgInSc6d1
prvku	prvek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Wolfram	wolfram	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Wolfram	wolfram	k1gInSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
4	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
W	W	kA
</s>
<s>
74	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Wolfram	wolfram	k1gInSc1
<g/>
,	,	kIx,
W	W	kA
<g/>
,	,	kIx,
74	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Wolframium	Wolframium	k1gNnSc1
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Světle	světle	k6eAd1
šedý	šedý	k2eAgInSc1d1
až	až	k8xS
bílý	bílý	k2eAgInSc1d1
lesklý	lesklý	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-33-7	7440-33-7	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
183,84	183,84	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
193	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
162	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
4	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
−	−	k?
<g/>
II	II	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
VI	VI	kA
<g/>
,	,	kIx,
VI	VI	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2,36	2,36	k4
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Prostorově	prostorově	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
krychlová	krychlový	k2eAgFnSc1d1
mřížka	mřížka	k1gFnSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
19,25	19,25	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
(	(	kIx(
<g/>
17,6	17,6	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
při	při	k7c6
teplotě	teplota	k1gFnSc6
tání	tání	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
7,5	7,5	k4
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
173	#num#	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Součinitel	součinitel	k1gInSc1
délkové	délkový	k2eAgFnSc2d1
roztažnosti	roztažnost	k1gFnSc2
</s>
<s>
4,5	4,5	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
3422	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
695,15	695,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
5660	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
5	#num#	k4
933,15	933,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
4,3	4,3	k4
MJ	mj	kA
<g/>
⋅	⋅	k?
<g/>
kg	kg	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
134	#num#	k4
J	J	kA
<g/>
⋅	⋅	k?
<g/>
kg	kg	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
při	při	k7c6
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
172	#num#	k4
J	J	kA
<g/>
⋅	⋅	k?
<g/>
kg	kg	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
při	při	k7c6
2000	#num#	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
18	#num#	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
52,8	52,8	k4
μ	μ	k?
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetické	paramagnetický	k2eAgNnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mo	Mo	k?
<g/>
⋏	⋏	k?
</s>
<s>
Tantal	tantal	k1gInSc1
≺	≺	k?
<g/>
W	W	kA
<g/>
≻	≻	k?
Rhenium	rhenium	k1gNnSc1
</s>
<s>
⋎	⋎	k?
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
Wolfram	wolfram	k1gInSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
W	W	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Wolframium	Wolframium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šedý	šedý	k2eAgInSc1d1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
těžký	těžký	k2eAgInSc1d1
a	a	k8xC
mimořádně	mimořádně	k6eAd1
obtížně	obtížně	k6eAd1
tavitelný	tavitelný	k2eAgInSc1d1
kov	kov	k1gInSc1
(	(	kIx(
<g/>
jeho	jeho	k3xOp3gFnSc1
teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc1d3
ze	z	k7c2
všech	všecek	k3xTgInPc2
kovů	kov	k1gInPc2
a	a	k8xC
po	po	k7c6
uhlíku	uhlík	k1gInSc6
druhá	druhý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
z	z	k7c2
prvků	prvek	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
uplatnění	uplatnění	k1gNnSc1
nalézá	nalézat	k5eAaImIp3nS
jako	jako	k9
složka	složka	k1gFnSc1
různých	různý	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
čisté	čistý	k2eAgFnSc6d1
formě	forma	k1gFnSc6
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gInSc7
běžně	běžně	k6eAd1
setkáváme	setkávat	k5eAaImIp1nP
jako	jako	k9
s	s	k7c7
materiálem	materiál	k1gInSc7
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
žárovkových	žárovkový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Kovový	kovový	k2eAgInSc1d1
slinovaný	slinovaný	k2eAgInSc1d1
wolfram	wolfram	k1gInSc1
</s>
<s>
Wolfram	wolfram	k1gInSc1
byl	být	k5eAaImAgInS
objeven	objeven	k2eAgInSc1d1
roku	rok	k1gInSc2
1781	#num#	k4
švédským	švédský	k2eAgMnSc7d1
chemikem	chemik	k1gMnSc7
Carlem	Carl	k1gMnSc7
Wilhelmem	Wilhelm	k1gMnSc7
Scheelem	Scheel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izolován	izolován	k2eAgInSc1d1
byl	být	k5eAaImAgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1783	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izolovali	izolovat	k5eAaBmAgMnP
ho	on	k3xPp3gMnSc4
Juan	Juan	k1gMnSc1
Jose	Jose	k1gNnSc2
D	D	kA
<g/>
'	'	kIx"
<g/>
Elhuyar	Elhuyar	k1gMnSc1
a	a	k8xC
Fausto	Fausta	k1gMnSc5
D	D	kA
<g/>
'	'	kIx"
<g/>
Elhuyar	Elhuyar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1555	#num#	k4
užil	užít	k5eAaPmAgMnS
rektor	rektor	k1gMnSc1
latinské	latinský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Johannes	Johannes	k1gMnSc1
Mathesius	Mathesius	k1gMnSc1
v	v	k7c6
Jáchymově	Jáchymov	k1gInSc6
v	v	k7c6
knize	kniha	k1gFnSc6
kázání	kázání	k1gNnSc2
Sarepta	Sarept	k1gInSc2
pro	pro	k7c4
šedý	šedý	k2eAgInSc4d1
<g/>
,	,	kIx,
obtížně	obtížně	k6eAd1
tavitelný	tavitelný	k2eAgInSc1d1
kov	kov	k1gInSc1
název	název	k1gInSc1
wolform	wolform	k1gInSc4
(	(	kIx(
<g/>
třetí	třetí	k4xOgFnSc1
modlitba	modlitba	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
roku	rok	k1gInSc2
1559	#num#	k4
wolfrumb	wolfrumba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Georgius	Georgius	k1gInSc1
Agricola	Agricola	k1gFnSc1
zmiňuje	zmiňovat	k5eAaImIp3nS
latinskou	latinský	k2eAgFnSc4d1
obdobu	obdoba	k1gFnSc4
názvu	název	k1gInSc2
lupi	lupi	k1gNnSc2
spuma	spumum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglická	anglický	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
názvu	název	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
švédského	švédský	k2eAgMnSc2d1
tung	tung	k1gInSc4
sten	sten	k1gInSc1
(	(	kIx(
<g/>
těžký	těžký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Wolfram	wolfram	k1gInSc1
je	být	k5eAaImIp3nS
šedý	šedý	k2eAgInSc1d1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
mimořádně	mimořádně	k6eAd1
obtížně	obtížně	k6eAd1
tavitelný	tavitelný	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
bod	bod	k1gInSc1
tavení	tavení	k1gNnSc2
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc1d3
ze	z	k7c2
všech	všecek	k3xTgInPc2
kovových	kovový	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
jeho	jeho	k3xOp3gFnSc1
vysoká	vysoký	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
některé	některý	k3yIgInPc4
drahé	drahý	k2eAgInPc4d1
kovy	kov	k1gInPc4
jako	jako	k8xC,k8xS
např.	např.	kA
zlato	zlato	k1gNnSc1
<g/>
,	,	kIx,
platina	platina	k1gFnSc1
<g/>
,	,	kIx,
iridium	iridium	k1gNnSc1
a	a	k8xC
osmium	osmium	k1gNnSc1
jsou	být	k5eAaImIp3nP
těžší	těžký	k2eAgInPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc2
vlastnosti	vlastnost	k1gFnSc2
je	být	k5eAaImIp3nS
využíváno	využívat	k5eAaImNgNnS,k5eAaPmNgNnS
při	při	k7c6
falšování	falšování	k1gNnSc6
zlatých	zlatý	k2eAgFnPc2d1
cihel	cihla	k1gFnPc2
(	(	kIx(
<g/>
slitků	slitek	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
slitku	slitek	k1gInSc2
jsou	být	k5eAaImIp3nP
vyvrtány	vyvrtán	k2eAgInPc1d1
otvory	otvor	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
zaplněny	zaplnit	k5eAaPmNgInP
wolframem	wolfram	k1gInSc7
a	a	k8xC
následně	následně	k6eAd1
zality	zalít	k5eAaPmNgInP
zlatem	zlato	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhalení	odhalení	k1gNnSc1
tohoto	tento	k3xDgNnSc2
falšování	falšování	k1gNnSc2
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
snadné	snadný	k2eAgNnSc1d1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
příslušné	příslušný	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlato	zlato	k1gNnSc1
je	být	k5eAaImIp3nS
diamagnetické	diamagnetický	k2eAgNnSc1d1
<g/>
,	,	kIx,
wolfram	wolfram	k1gInSc1
paramagnetický	paramagnetický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
se	se	k3xPyFc4
projeví	projevit	k5eAaPmIp3nS
při	při	k7c6
nedestruktivním	destruktivní	k2eNgNnSc6d1
měření	měření	k1gNnSc6
elektrické	elektrický	k2eAgFnSc2d1
vodivosti	vodivost	k1gFnSc2
střídavým	střídavý	k2eAgInSc7d1
proudem	proud	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
supernízkých	supernízký	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
pod	pod	k7c4
0,0012	0,0012	k4
K	k	k7c3
je	být	k5eAaImIp3nS
supravodičem	supravodič	k1gInSc7
I	i	k8xC
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Chemicky	chemicky	k6eAd1
je	být	k5eAaImIp3nS
kovový	kovový	k2eAgInSc1d1
wolfram	wolfram	k1gInSc1
velmi	velmi	k6eAd1
stálý	stálý	k2eAgInSc1d1
–	–	k?
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
netečný	netečný	k2eAgMnSc1d1,k2eNgMnSc1d1
k	k	k7c3
působení	působení	k1gNnSc3
vody	voda	k1gFnSc2
a	a	k8xC
atmosférických	atmosférický	k2eAgInPc2d1
plynů	plyn	k1gInPc2
a	a	k8xC
odolává	odolávat	k5eAaImIp3nS
působení	působení	k1gNnSc1
většiny	většina	k1gFnSc2
běžných	běžný	k2eAgFnPc2d1
minerálních	minerální	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
kyslíkem	kyslík	k1gInSc7
a	a	k8xC
halogeny	halogen	k1gInPc7
reaguje	reagovat	k5eAaBmIp3nS
až	až	k9
za	za	k7c2
značně	značně	k6eAd1
vysokých	vysoký	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
rozpouštění	rozpouštění	k1gNnSc4
je	být	k5eAaImIp3nS
nejúčinnější	účinný	k2eAgFnSc1d3
směs	směs	k1gFnSc1
kyseliny	kyselina	k1gFnSc2
dusičné	dusičný	k2eAgFnSc2d1
a	a	k8xC
kyseliny	kyselina	k1gFnSc2
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsnáze	snadno	k6eAd3
se	se	k3xPyFc4
kovový	kovový	k2eAgInSc1d1
wolfram	wolfram	k1gInSc1
rozkládá	rozkládat	k5eAaImIp3nS
alkalickým	alkalický	k2eAgNnSc7d1
tavením	tavení	k1gNnSc7
například	například	k6eAd1
se	se	k3xPyFc4
směsí	směs	k1gFnSc7
dusičnanu	dusičnan	k1gInSc2
draselného	draselný	k2eAgInSc2d1
a	a	k8xC
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
(	(	kIx(
<g/>
KNO	KNO	kA
<g/>
3	#num#	k4
+	+	kIx~
NaOH	NaOH	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
řadě	řada	k1gFnSc6
různých	různý	k2eAgFnPc2d1
mocenství	mocenství	k1gNnSc4
od	od	k7c2
WII	WII	kA
<g/>
+	+	kIx~
a	a	k8xC
po	po	k7c6
WVI	WVI	kA
<g/>
+	+	kIx~
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
sloučeniny	sloučenina	k1gFnPc1
WVI	WVI	kA
<g/>
+	+	kIx~
jsou	být	k5eAaImIp3nP
nejstálejší	stálý	k2eAgFnPc1d3
a	a	k8xC
nejvíce	nejvíce	k6eAd1,k6eAd3
prakticky	prakticky	k6eAd1
využívané	využívaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Izotopy	izotop	k1gInPc4
wolframu	wolfram	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Přirozeně	přirozeně	k6eAd1
se	se	k3xPyFc4
wolfram	wolfram	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
ve	v	k7c6
čtyřech	čtyři	k4xCgInPc6
stabilních	stabilní	k2eAgInPc6d1
izotopech	izotop	k1gInPc6
(	(	kIx(
<g/>
182	#num#	k4
<g/>
W	W	kA
<g/>
,	,	kIx,
183	#num#	k4
<g/>
W	W	kA
<g/>
,	,	kIx,
184	#num#	k4
a	a	k8xC
186	#num#	k4
<g/>
W	W	kA
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
jediném	jediný	k2eAgInSc6d1
radioizotopu	radioizotop	k1gInSc6
180W	180W	k4
s	s	k7c7
dlouhým	dlouhý	k2eAgInSc7d1
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
výroba	výroba	k1gFnSc1
</s>
<s>
Wolframit	wolframit	k1gInSc1
</s>
<s>
Wolfram	wolfram	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c4
Zemi	zem	k1gFnSc4
poměrně	poměrně	k6eAd1
vzácný	vzácný	k2eAgInSc4d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
1,5	1,5	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
se	se	k3xPyFc4
wolfram	wolfram	k1gInSc1
nachází	nacházet	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
koncentraci	koncentrace	k1gFnSc6
0,0001	0,0001	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
wolframu	wolfram	k1gInSc2
na	na	k7c4
300	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1
minerály	minerál	k1gInPc7
wolframu	wolfram	k1gInSc2
v	v	k7c6
přírodě	příroda	k1gFnSc6
jsou	být	k5eAaImIp3nP
wolframit	wolframit	k1gInSc4
–	–	k?
wolframan	wolframan	k1gInSc1
železnato-manganatý	železnato-manganatý	k2eAgInSc1d1
(	(	kIx(
<g/>
Fe	Fe	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Mn	Mn	k1gMnSc1
<g/>
)	)	kIx)
<g/>
WO	WO	kA
<g/>
4	#num#	k4
(	(	kIx(
<g/>
přechodný	přechodný	k2eAgMnSc1d1
člen	člen	k1gMnSc1
řady	řada	k1gFnSc2
ferberit	ferberit	k1gInSc1
FeWO	FeWO	k1gFnSc4
<g/>
4	#num#	k4
hübneritové	hübneritový	k2eAgFnSc6d1
MnWO	MnWO	k1gFnSc6
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
wolframan	wolframan	k1gInSc1
vápenatý	vápenatý	k2eAgInSc1d1
<g/>
,	,	kIx,
scheelit	scheelit	k1gInSc1
CaWO	CaWO	k1gFnSc1
<g/>
4	#num#	k4
a	a	k8xC
stolzit	stolzit	k1gInSc1
<g/>
,	,	kIx,
wolframan	wolframan	k1gInSc1
olovnatý	olovnatý	k2eAgInSc1d1
<g/>
,	,	kIx,
PbWO	PbWO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
metalurgické	metalurgický	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
wolframu	wolfram	k1gInSc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
nejprve	nejprve	k6eAd1
mechanicky	mechanicky	k6eAd1
separují	separovat	k5eAaBmIp3nP
těžké	těžký	k2eAgFnPc4d1
frakce	frakce	k1gFnPc4
rudy	ruda	k1gFnSc2
a	a	k8xC
výsledný	výsledný	k2eAgInSc1d1
koncentrát	koncentrát	k1gInSc1
se	se	k3xPyFc4
taví	tavit	k5eAaImIp3nS
s	s	k7c7
hydroxidem	hydroxid	k1gInSc7
sodným	sodný	k2eAgInSc7d1
(	(	kIx(
<g/>
NaOH	NaOH	k1gMnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tavenina	tavenina	k1gFnSc1
se	se	k3xPyFc4
louží	loužit	k5eAaImIp3nS
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
do	do	k7c2
níž	jenž	k3xRgFnSc2
přechází	přecházet	k5eAaImIp3nS
vzniklý	vzniklý	k2eAgInSc1d1
wolframan	wolframan	k1gInSc1
sodný	sodný	k2eAgInSc1d1
<g/>
,	,	kIx,
Na	na	k7c4
<g/>
2	#num#	k4
<g/>
WO	WO	kA
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okyselením	okyselení	k1gNnSc7
tohoto	tento	k3xDgInSc2
roztoku	roztok	k1gInSc2
vypadává	vypadávat	k5eAaImIp3nS
sraženina	sraženina	k1gFnSc1
hydratovaného	hydratovaný	k2eAgInSc2d1
oxidu	oxid	k1gInSc2
wolframového	wolframový	k2eAgInSc2d1
WO	WO	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1
wolfram	wolfram	k1gInSc1
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
molybden	molybden	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
získá	získat	k5eAaPmIp3nS
redukcí	redukce	k1gFnSc7
oxidu	oxid	k1gInSc2
wolframového	wolframový	k2eAgInSc2d1
vodíkem	vodík	k1gInSc7
při	při	k7c6
teplotě	teplota	k1gFnSc6
850	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
:	:	kIx,
</s>
<s>
WO3	WO3	k4
+	+	kIx~
3	#num#	k4
H2	H2	k1gFnSc2
→	→	k?
W	W	kA
+	+	kIx~
3	#num#	k4
H2O	H2O	k1gFnPc2
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Wolframové	wolframový	k2eAgNnSc1d1
vlákno	vlákno	k1gNnSc1
v	v	k7c6
halogenové	halogenový	k2eAgFnSc6d1
žárovce	žárovka	k1gFnSc6
</s>
<s>
Wolframová	wolframový	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
při	při	k7c6
technologii	technologie	k1gFnSc6
WIG	WIG	kA
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1
použití	použití	k1gNnSc1
wolframu	wolfram	k1gInSc2
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
od	od	k7c2
jeho	jeho	k3xOp3gFnSc2
vysoké	vysoký	k2eAgFnSc2d1
hustoty	hustota	k1gFnSc2
a	a	k8xC
obtížné	obtížný	k2eAgFnSc2d1
tavitelnosti	tavitelnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Běžně	běžně	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
wolframem	wolfram	k1gInSc7
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
jako	jako	k9
s	s	k7c7
materiálem	materiál	k1gInSc7
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
žárovkových	žárovkový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgInSc1d1
po	po	k7c6
tisíce	tisíc	k4xCgInPc1
pracovních	pracovní	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
snášet	snášet	k5eAaImF
teploty	teplota	k1gFnPc4
výrazně	výrazně	k6eAd1
přes	přes	k7c4
1	#num#	k4
000	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Vysoké	vysoký	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
vlákno	vlákno	k1gNnSc1
dosahuje	dosahovat	k5eAaImIp3nS
průchodem	průchod	k1gInSc7
elektrického	elektrický	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vnitřní	vnitřní	k2eAgInSc1d1
prostor	prostor	k1gInSc1
žárovky	žárovka	k1gFnSc2
je	být	k5eAaImIp3nS
naplněn	naplnit	k5eAaPmNgInS
inertním	inertní	k2eAgInSc7d1
plynem	plyn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
wolfram	wolfram	k1gInSc1
totiž	totiž	k9
není	být	k5eNaImIp3nS
natolik	natolik	k6eAd1
inertní	inertní	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
za	za	k7c2
těchto	tento	k3xDgFnPc2
podmínek	podmínka	k1gFnPc2
nedocházelo	docházet	k5eNaImAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
oxidaci	oxidace	k1gFnSc6
vzdušným	vzdušný	k2eAgInSc7d1
kyslíkem	kyslík	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
elektrotechnice	elektrotechnika	k1gFnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
materiál	materiál	k1gInSc4
pro	pro	k7c4
anodu	anoda	k1gFnSc4
(	(	kIx(
<g/>
terčík	terčík	k1gInSc4
<g/>
)	)	kIx)
rentgenky	rentgenka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wolfram	wolfram	k1gInSc1
má	mít	k5eAaImIp3nS
vysokou	vysoký	k2eAgFnSc4d1
elektronovou	elektronový	k2eAgFnSc4d1
hustotu	hustota	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
dopadající	dopadající	k2eAgInPc1d1
elektrony	elektron	k1gInPc1
jsou	být	k5eAaImIp3nP
velkou	velký	k2eAgFnSc7d1
odpudivou	odpudivý	k2eAgFnSc7d1
silou	síla	k1gFnSc7
prudce	prudko	k6eAd1
brzděny	brzdit	k5eAaImNgInP
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
podle	podle	k7c2
zákonitostí	zákonitost	k1gFnPc2
elektrodynamiky	elektrodynamika	k1gFnSc2
část	část	k1gFnSc4
jejich	jejich	k3xOp3gFnSc2
kinetické	kinetický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
mění	měnit	k5eAaImIp3nS
v	v	k7c4
brzdné	brzdný	k2eAgNnSc4d1
elektromagnetické	elektromagnetický	k2eAgNnSc4d1
záření	záření	k1gNnSc4
–	–	k?
fotony	foton	k1gInPc4
X-záření	X-záření	k1gNnSc2
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
svařování	svařování	k1gNnSc6
kovů	kov	k1gInPc2
elektrickým	elektrický	k2eAgInSc7d1
obloukem	oblouk	k1gInSc7
za	za	k7c4
použití	použití	k1gNnSc4
wolframových	wolframový	k2eAgFnPc2d1
elektrod	elektroda	k1gFnPc2
(	(	kIx(
<g/>
metoda	metoda	k1gFnSc1
TIG	TIG	kA
<g/>
,	,	kIx,
tungsten	tungsten	k2eAgInSc1d1
inert	inert	k1gInSc1
gas	gas	k?
<g/>
)	)	kIx)
způsobuje	způsobovat	k5eAaImIp3nS
elektrický	elektrický	k2eAgInSc4d1
proud	proud	k1gInSc4
procházející	procházející	k2eAgInSc4d1
mezi	mezi	k7c7
elektrodami	elektroda	k1gFnPc7
v	v	k7c6
inertní	inertní	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
argon	argon	k1gInSc1
<g/>
)	)	kIx)
roztavení	roztavení	k1gNnSc1
zpracovávaných	zpracovávaný	k2eAgInPc2d1
kovů	kov	k1gInPc2
bez	bez	k7c2
úbytku	úbytek	k1gInSc2
materiálu	materiál	k1gInSc2
elektrod	elektroda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
slitinách	slitina	k1gFnPc6
se	se	k3xPyFc4
přísada	přísada	k1gFnSc1
wolframu	wolfram	k1gInSc2
projeví	projevit	k5eAaPmIp3nS
především	především	k9
zvýšením	zvýšení	k1gNnSc7
tvrdosti	tvrdost	k1gFnSc2
a	a	k8xC
mechanické	mechanický	k2eAgFnSc2d1
i	i	k8xC
tepelné	tepelný	k2eAgFnSc2d1
odolnosti	odolnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlořezné	rychlořezný	k2eAgFnPc4d1
oceli	ocel	k1gFnPc4
nabízené	nabízený	k2eAgFnPc4d1
pod	pod	k7c7
značkou	značka	k1gFnSc7
Stellite	Stellit	k1gInSc5
obsahují	obsahovat	k5eAaImIp3nP
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
až	až	k9
18	#num#	k4
%	%	kIx~
wolframu	wolfram	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
nich	on	k3xPp3gInPc6
kovoobráběcí	kovoobráběcí	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
<g/>
,	,	kIx,
vrtné	vrtný	k2eAgFnPc1d1
hlavice	hlavice	k1gFnPc1
geologických	geologický	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
lopatky	lopatka	k1gFnSc2
parních	parní	k2eAgFnPc2d1
turbín	turbína	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
vysoce	vysoce	k6eAd1
teplotně	teplotně	k6eAd1
a	a	k8xC
mechanicky	mechanicky	k6eAd1
namáhané	namáhaný	k2eAgFnPc1d1
součástky	součástka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
vysoké	vysoký	k2eAgFnSc3d1
hustotě	hustota	k1gFnSc3
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
materiál	materiál	k1gInSc1
penetračních	penetrační	k2eAgInPc2d1
projektilů	projektil	k1gInPc2
(	(	kIx(
<g/>
penetrátorů	penetrátor	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgInP
již	již	k6eAd1
od	od	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
pro	pro	k7c4
prorážení	prorážení	k1gNnSc4
pancíře	pancíř	k1gInSc2
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
stěn	stěna	k1gFnPc2
bunkrů	bunkr	k1gInPc2
a	a	k8xC
opevnění	opevnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pseudoslitiny	Pseudoslitin	k1gInPc1
wolframu	wolfram	k1gInSc2
(	(	kIx(
<g/>
s	s	k7c7
niklem	nikl	k1gInSc7
<g/>
,	,	kIx,
železem	železo	k1gNnSc7
a	a	k8xC
kobaltem	kobalt	k1gInSc7
<g/>
,	,	kIx,
obsah	obsah	k1gInSc4
wolframu	wolfram	k1gInSc2
91	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
hm	hm	k?
<g/>
.	.	kIx.
<g/>
%	%	kIx~
<g/>
)	)	kIx)
vyrobené	vyrobený	k2eAgNnSc1d1
práškovou	práškový	k2eAgFnSc7d1
metalurgií	metalurgie	k1gFnSc7
se	se	k3xPyFc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
dobré	dobrý	k2eAgFnSc3d1
schopnosti	schopnost	k1gFnSc3
odstínit	odstínit	k5eAaPmF
rentgenové	rentgenový	k2eAgNnSc4d1
záření	záření	k1gNnSc4
a	a	k8xC
záření	záření	k1gNnSc1
gama	gama	k1gNnSc1
jako	jako	k8xC,k8xS
materiál	materiál	k1gInSc1
pro	pro	k7c4
radiační	radiační	k2eAgNnSc4d1
stínění	stínění	k1gNnSc4
např.	např.	kA
v	v	k7c6
kobaltových	kobaltový	k2eAgInPc6d1
ozařovačích	ozařovač	k1gInPc6
<g/>
,	,	kIx,
používaných	používaný	k2eAgInPc2d1
k	k	k7c3
ozařování	ozařování	k1gNnSc3
zhoubných	zhoubný	k2eAgInPc2d1
nádorů	nádor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Wolfram	wolfram	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
sloučenin	sloučenina	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
nejstálejší	stálý	k2eAgMnPc1d3
vykazují	vykazovat	k5eAaImIp3nP
oxidační	oxidační	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
VI	VI	kA
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
dále	daleko	k6eAd2
oxidační	oxidační	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
II	II	kA
<g/>
+	+	kIx~
<g/>
,	,	kIx,
III	III	kA
<g/>
+	+	kIx~
<g/>
,	,	kIx,
IV	IV	kA
<g/>
+	+	kIx~
<g/>
,	,	kIx,
V	V	kA
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktický	praktický	k2eAgInSc4d1
význam	význam	k1gInSc4
nalézají	nalézat	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnPc4
sloučeniny	sloučenina	k1gFnPc1
při	při	k7c6
přípravě	příprava	k1gFnSc6
katalyzátorů	katalyzátor	k1gInPc2
pro	pro	k7c4
petrochemický	petrochemický	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
výrobě	výroba	k1gFnSc6
různých	různý	k2eAgInPc2d1
barevných	barevný	k2eAgInPc2d1
pigmentů	pigment	k1gInPc2
a	a	k8xC
teplotně	teplotně	k6eAd1
odolných	odolný	k2eAgInPc2d1
lubrikantů	lubrikant	k1gInPc2
a	a	k8xC
maziv	mazivo	k1gNnPc2
(	(	kIx(
<g/>
sulfidy	sulfid	k1gInPc1
wolframu	wolfram	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
oxidů	oxid	k1gInPc2
wolframu	wolfram	k1gInSc2
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgFnPc1d1
oxid	oxid	k1gInSc1
wolframový	wolframový	k2eAgMnSc1d1
WO3	WO3	k1gMnSc1
a	a	k8xC
oxid	oxid	k1gInSc1
wolframičitý	wolframičitý	k2eAgInSc1d1
<g/>
,	,	kIx,
WO	WO	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
významnou	významný	k2eAgFnSc7d1
sloučeninou	sloučenina	k1gFnSc7
wolframu	wolfram	k1gInSc2
je	být	k5eAaImIp3nS
kyselina	kyselina	k1gFnSc1
wolframová	wolframový	k2eAgFnSc1d1
<g/>
,	,	kIx,
H	H	kA
<g/>
2	#num#	k4
<g/>
WO	WO	kA
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nP
jednoduché	jednoduchý	k2eAgFnPc1d1
soli	sůl	k1gFnPc1
<g/>
,	,	kIx,
wolframany	wolframan	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
značně	značně	k6eAd1
složitých	složitý	k2eAgFnPc2d1
komplexních	komplexní	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Technicky	technicky	k6eAd1
důležitými	důležitý	k2eAgFnPc7d1
sloučeninami	sloučenina	k1gFnPc7
wolframu	wolfram	k1gInSc2
jsou	být	k5eAaImIp3nP
karbidy	karbid	k1gInPc1
o	o	k7c4
složení	složení	k1gNnSc4
WC	WC	kA
a	a	k8xC
W	W	kA
<g/>
2	#num#	k4
<g/>
C.	C.	kA
Vyznačují	vyznačovat	k5eAaImIp3nP
se	se	k3xPyFc4
mimořádnou	mimořádný	k2eAgFnSc7d1
tvrdostí	tvrdost	k1gFnSc7
a	a	k8xC
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
se	se	k3xPyFc4
jako	jako	k9
součásti	součást	k1gFnPc1
brusiv	brusivo	k1gNnPc2
pro	pro	k7c4
kovoobrábění	kovoobrábění	k1gNnPc4
a	a	k8xC
geologické	geologický	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
je	být	k5eAaImIp3nS
připravit	připravit	k5eAaPmF
například	například	k6eAd1
redukcí	redukce	k1gFnSc7
oxidu	oxid	k1gInSc2
wolframového	wolframový	k2eAgInSc2d1
uhlíkem	uhlík	k1gInSc7
<g/>
:	:	kIx,
</s>
<s>
WO3	WO3	k4
+	+	kIx~
4	#num#	k4
C	C	kA
→	→	k?
WC	WC	kA
+	+	kIx~
3	#num#	k4
CO	co	k8xS
</s>
<s>
Biologický	biologický	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Díky	díky	k7c3
velmi	velmi	k6eAd1
nízké	nízký	k2eAgFnSc3d1
rozpustnosti	rozpustnost	k1gFnSc3
wolframu	wolfram	k1gInSc2
ve	v	k7c6
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
v	v	k7c6
živých	živý	k2eAgInPc6d1
organizmech	organizmus	k1gInPc6
velmi	velmi	k6eAd1
nízký	nízký	k2eAgMnSc1d1
a	a	k8xC
wolfram	wolfram	k1gInSc1
tedy	tedy	k9
nepatří	patřit	k5eNaImIp3nS
mezi	mezi	k7c4
biogenní	biogenní	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc4
nedostatek	nedostatek	k1gInSc1
ve	v	k7c6
stravě	strava	k1gFnSc6
výrazně	výrazně	k6eAd1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
fyziologický	fyziologický	k2eAgInSc4d1
stav	stav	k1gInSc4
organismu	organismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
wolfram	wolfram	k1gInSc4
obsažený	obsažený	k2eAgInSc4d1
v	v	k7c6
tkáních	tkáň	k1gFnPc6
živých	živý	k2eAgInPc2d1
organismů	organismus	k1gInPc2
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
molybden	molybden	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
například	například	k6eAd1
potvrzena	potvrdit	k5eAaPmNgFnS
jeho	jeho	k3xOp3gFnSc1
role	role	k1gFnSc1
v	v	k7c6
enzymatickém	enzymatický	k2eAgInSc6d1
systému	systém	k1gInSc6
oxidoreduktázy	oxidoreduktáza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
nejsou	být	k5eNaImIp3nP
známy	znám	k2eAgInPc1d1
případy	případ	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
by	by	kYmCp3nS
přebytek	přebytek	k1gInSc1
wolframu	wolfram	k1gInSc2
v	v	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
dlouhodobě	dlouhodobě	k6eAd1
negativně	negativně	k6eAd1
ovlivňoval	ovlivňovat	k5eAaImAgMnS
lidské	lidský	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Řada	řada	k1gFnSc1
enzymů	enzym	k1gInPc2
hypertermofilních	hypertermofilní	k2eAgFnPc2d1
archeí	arche	k1gFnPc2
je	být	k5eAaImIp3nS
schopná	schopný	k2eAgFnSc1d1
wolfram	wolfram	k1gInSc1
využívat	využívat	k5eAaImF,k5eAaPmF
místo	místo	k1gNnSc4
molybdenu	molybden	k1gInSc2
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
aktivních	aktivní	k2eAgNnPc6d1
centrech	centrum	k1gNnPc6
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
enzymy	enzym	k1gInPc1
ovšem	ovšem	k9
dokážou	dokázat	k5eAaPmIp3nP
využívat	využívat	k5eAaPmF,k5eAaImF
výhradně	výhradně	k6eAd1
wolfram	wolfram	k1gInSc4
a	a	k8xC
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
je	on	k3xPp3gInPc4
nahradit	nahradit	k5eAaPmF
molybdenem	molybden	k1gInSc7
nebo	nebo	k8xC
vanadem	vanad	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Why	Why	k1gFnSc1
does	does	k1gInSc4
Tungsten	Tungsten	k2eAgInSc1d1
not	nota	k1gFnPc2
'	'	kIx"
<g/>
Kick	Kick	k1gInSc1
<g/>
'	'	kIx"
up	up	k?
an	an	k?
electron	electron	k1gInSc1
from	from	k1gInSc1
the	the	k?
s	s	k7c7
sublevel	sublevela	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Why	Why	k1gFnSc1
does	does	k1gInSc4
Tungsten	Tungsten	k2eAgInSc1d1
not	nota	k1gFnPc2
'	'	kIx"
<g/>
Kick	Kick	k1gInSc1
<g/>
'	'	kIx"
up	up	k?
an	an	k?
electron	electron	k1gInSc1
from	from	k1gInSc1
the	the	k?
s	s	k7c7
sublevel	sublevela	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
RNDr.	RNDr.	kA
Vojtěch	Vojtěch	k1gMnSc1
Ullmann	Ullmann	k1gMnSc1
<g/>
:	:	kIx,
Detekce	detekce	k1gFnSc1
a	a	k8xC
aplikace	aplikace	k1gFnSc1
ionizujícího	ionizující	k2eAgNnSc2d1
záření	záření	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
astronuklfyzika	astronuklfyzika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
L	L	kA
<g/>
'	'	kIx"
<g/>
VOV	VOV	kA
<g/>
,	,	kIx,
NP	NP	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
NOSIKOV	NOSIKOV	kA
<g/>
,	,	kIx,
AN	AN	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
ANTIPOV	ANTIPOV	kA
<g/>
,	,	kIx,
AN	AN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tungsten-containing	Tungsten-containing	k1gInSc1
enzymes	enzymes	k1gInSc4
<g/>
..	..	k?
Biochemistry	Biochemistr	k1gMnPc7
(	(	kIx(
<g/>
Mosc	Mosc	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Feb	Feb	k1gFnSc1
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
67	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
196	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
11952415	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
wolfram	wolfram	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
wolfram	wolfram	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4066862-9	4066862-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5770	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85138607	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85138607	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
