<p>
<s>
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1205	[number]	k4	1205
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1253	[number]	k4	1253
Počaply	Počaply	k1gMnPc2	Počaply
u	u	k7c2	u
Berouna	Beroun	k1gInSc2	Beroun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
též	též	k9	též
Jednooký	jednooký	k2eAgInSc1d1	jednooký
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
manželky	manželka	k1gFnPc1	manželka
Konstancie	Konstancie	k1gFnSc2	Konstancie
Uherské	uherský	k2eAgNnSc1d1	Uherské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Potomci	potomek	k1gMnPc1	potomek
Přemysla	Přemysl	k1gMnSc2	Přemysl
a	a	k8xC	a
Konstancie	Konstancie	k1gFnSc2	Konstancie
projevovali	projevovat	k5eAaImAgMnP	projevovat
v	v	k7c6	v
přemyslovském	přemyslovský	k2eAgInSc6d1	přemyslovský
rodě	rod	k1gInSc6	rod
až	až	k6eAd1	až
nezvyklou	zvyklý	k2eNgFnSc4d1	nezvyklá
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
soudržnost	soudržnost	k1gFnSc4	soudržnost
(	(	kIx(	(
<g/>
přesto	přesto	k8xC	přesto
mezi	mezi	k7c7	mezi
sourozenci	sourozenec	k1gMnPc7	sourozenec
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc4	několik
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
řešily	řešit	k5eAaImAgFnP	řešit
vojensky	vojensky	k6eAd1	vojensky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Václavovými	Václavův	k2eAgFnPc7d1	Václavova
sestrami	sestra	k1gFnPc7	sestra
byly	být	k5eAaImAgFnP	být
Anežka	Anežka	k1gFnSc1	Anežka
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
–	–	k?	–
později	pozdě	k6eAd2	pozdě
svatá	svatý	k2eAgFnSc1d1	svatá
Anežka	Anežka	k1gFnSc1	Anežka
Česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
kněžna	kněžna	k1gFnSc1	kněžna
Anna	Anna	k1gFnSc1	Anna
Lehnická	Lehnický	k2eAgFnSc1d1	Lehnická
a	a	k8xC	a
korutanská	korutanský	k2eAgFnSc1d1	Korutanská
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Judita	Judita	k1gFnSc1	Judita
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
byli	být	k5eAaImAgMnP	být
moravskými	moravský	k2eAgNnPc7d1	Moravské
markrabaty	markrabě	k1gNnPc7	markrabě
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
Vladislav	Vladislav	k1gMnSc1	Vladislav
a	a	k8xC	a
poté	poté	k6eAd1	poté
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Korunovace	korunovace	k1gFnSc2	korunovace
==	==	k?	==
</s>
</p>
<p>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
chtěl	chtít	k5eAaImAgMnS	chtít
zajistit	zajistit	k5eAaPmF	zajistit
ještě	ještě	k9	ještě
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
titul	titul	k1gInSc1	titul
krále	král	k1gMnSc2	král
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Václava	Václav	k1gMnSc4	Václav
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
pochyby	pochyba	k1gFnPc1	pochyba
o	o	k7c6	o
legitimitě	legitimita	k1gFnSc6	legitimita
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
plán	plán	k1gInSc1	plán
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
shromáždění	shromáždění	k1gNnSc6	shromáždění
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1216	[number]	k4	1216
někdo	někdo	k3yInSc1	někdo
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
zvolení	zvolení	k1gNnSc1	zvolení
Václava	Václav	k1gMnSc2	Václav
coby	coby	k?	coby
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
připojit	připojit	k5eAaPmF	připojit
i	i	k9	i
bratr	bratr	k1gMnSc1	bratr
Přemysla	Přemysl	k1gMnSc2	Přemysl
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Jakoby	jakoby	k8xS	jakoby
překvapený	překvapený	k2eAgMnSc1d1	překvapený
král	král	k1gMnSc1	král
bude	být	k5eAaImBp3nS	být
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
záměrem	záměr	k1gInSc7	záměr
souhlasit	souhlasit	k5eAaImF	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tento	tento	k3xDgInSc4	tento
akt	akt	k1gInSc4	akt
nepopudil	popudit	k5eNaPmAgMnS	popudit
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
mohla	moct	k5eAaImAgFnS	moct
volit	volit	k5eAaImF	volit
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plán	plán	k1gInSc1	plán
do	do	k7c2	do
písmene	písmeno	k1gNnSc2	písmeno
vyšel	vyjít	k5eAaPmAgMnS	vyjít
a	a	k8xC	a
hned	hned	k6eAd1	hned
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
vyslal	vyslat	k5eAaPmAgMnS	vyslat
král	král	k1gMnSc1	král
za	za	k7c7	za
císařem	císař	k1gMnSc7	císař
Fridrichem	Fridrich	k1gMnSc7	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
posla	posel	k1gMnSc2	posel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
informoval	informovat	k5eAaBmAgMnS	informovat
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1216	[number]	k4	1216
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
(	(	kIx(	(
<g/>
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
mu	on	k3xPp3gNnSc3	on
ani	ani	k9	ani
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
buly	bula	k1gFnSc2	bula
sicilské	sicilský	k2eAgFnSc2d1	sicilská
nezbývalo	zbývat	k5eNaImAgNnS	zbývat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volbou	volba	k1gFnSc7	volba
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
tak	tak	k8xC	tak
definitivně	definitivně	k6eAd1	definitivně
padl	padnout	k5eAaPmAgInS	padnout
starobylý	starobylý	k2eAgInSc1d1	starobylý
princip	princip	k1gInSc1	princip
seniorátu	seniorát	k1gInSc2	seniorát
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
beztak	beztak	k9	beztak
prakticky	prakticky	k6eAd1	prakticky
nedodržoval	dodržovat	k5eNaImAgInS	dodržovat
<g/>
)	)	kIx)	)
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgMnSc4	který
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
nastoupit	nastoupit	k5eAaPmF	nastoupit
vždy	vždy	k6eAd1	vždy
nejstarší	starý	k2eAgMnSc1d3	nejstarší
Přemyslovec	Přemyslovec	k1gMnSc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
trůnu	trůn	k1gInSc2	trůn
tak	tak	k9	tak
byli	být	k5eAaImAgMnP	být
odstaveni	odstaven	k2eAgMnPc1d1	odstaven
příslušníci	příslušník	k1gMnPc1	příslušník
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
větví	větev	k1gFnPc2	větev
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
byl	být	k5eAaImAgMnS	být
korunován	korunován	k2eAgMnSc1d1	korunován
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1228	[number]	k4	1228
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
mladší	mladý	k2eAgMnSc1d2	mladší
král	král	k1gMnSc1	král
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obřad	obřad	k1gInSc1	obřad
provedl	provést	k5eAaPmAgInS	provést
mohučský	mohučský	k2eAgMnSc1d1	mohučský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Siegfried	Siegfried	k1gMnSc1	Siegfried
z	z	k7c2	z
Eppenštejna	Eppenštejno	k1gNnSc2	Eppenštejno
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
upuštěno	upustit	k5eAaPmNgNnS	upustit
od	od	k7c2	od
starodávného	starodávný	k2eAgInSc2d1	starodávný
slovanského	slovanský	k2eAgInSc2d1	slovanský
rituálu	rituál	k1gInSc2	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
pražským	pražský	k2eAgMnSc7d1	pražský
a	a	k8xC	a
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
i	i	k8xC	i
velmoži	velmož	k1gMnPc1	velmož
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
stanovili	stanovit	k5eAaPmAgMnP	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnPc1	jejich
nástupci	nástupce	k1gMnPc1	nástupce
budou	být	k5eAaImBp3nP	být
korunováni	korunovat	k5eAaBmNgMnP	korunovat
arcibiskupy	arcibiskup	k1gMnPc4	arcibiskup
z	z	k7c2	z
Mohuče	Mohuč	k1gFnSc2	Mohuč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zástupce	zástupce	k1gMnSc1	zástupce
mohučské	mohučský	k2eAgFnSc2d1	mohučská
diecéze	diecéze	k1gFnSc2	diecéze
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
biskupa	biskup	k1gMnSc2	biskup
vykonajícího	vykonající	k2eAgInSc2d1	vykonající
obřad	obřad	k1gInSc4	obřad
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
vybrat	vybrat	k5eAaPmF	vybrat
český	český	k2eAgMnSc1d1	český
monarcha	monarcha	k1gMnSc1	monarcha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jednooký	jednooký	k2eAgMnSc1d1	jednooký
král	král	k1gMnSc1	král
==	==	k?	==
</s>
</p>
<p>
<s>
Samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
sestry	sestra	k1gFnSc2	sestra
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
vládnout	vládnout	k5eAaImF	vládnout
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1230	[number]	k4	1230
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
ženou	žena	k1gFnSc7	žena
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1224	[number]	k4	1224
dcera	dcera	k1gFnSc1	dcera
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
Švábského	švábský	k2eAgMnSc2d1	švábský
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
Štaufská	Štaufský	k2eAgFnSc1d1	Štaufská
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
byl	být	k5eAaImAgMnS	být
zasnouben	zasnoubit	k5eAaPmNgMnS	zasnoubit
už	už	k6eAd1	už
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
dvou	dva	k4xCgFnPc6	dva
a	a	k8xC	a
jejích	její	k3xOp3gNnPc2	její
snad	snad	k9	snad
sedmi	sedm	k4xCc6	sedm
letech	let	k1gInPc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
měli	mít	k5eAaImAgMnP	mít
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
–	–	k?	–
neurotický	neurotický	k2eAgInSc1d1	neurotický
<g/>
,	,	kIx,	,
možná	možná	k9	možná
psychicky	psychicky	k6eAd1	psychicky
chorý	chorý	k2eAgMnSc1d1	chorý
<g/>
.	.	kIx.	.
</s>
<s>
Miloval	milovat	k5eAaImAgMnS	milovat
potulky	potulka	k1gFnSc2	potulka
hlubokým	hluboký	k2eAgInSc7d1	hluboký
lesem	les	k1gInSc7	les
<g/>
,	,	kIx,	,
vášnivě	vášnivě	k6eAd1	vášnivě
lovil	lovit	k5eAaImAgMnS	lovit
a	a	k8xC	a
právě	právě	k6eAd1	právě
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
lovů	lov	k1gInPc2	lov
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
levé	levý	k2eAgNnSc4d1	levé
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštně	zvláštně	k6eAd1	zvláštně
zní	znět	k5eAaImIp3nS	znět
informace	informace	k1gFnPc4	informace
kronikářů	kronikář	k1gMnPc2	kronikář
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
nesnášel	snášet	k5eNaImAgMnS	snášet
zvuk	zvuk	k1gInSc4	zvuk
kostelních	kostelní	k2eAgInPc2d1	kostelní
zvonů	zvon	k1gInPc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Vyvolával	vyvolávat	k5eAaImAgInS	vyvolávat
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
kruté	krutý	k2eAgNnSc4d1	kruté
bolesti	bolest	k1gFnPc4	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
jakési	jakýsi	k3yIgInPc4	jakýsi
záchvaty	záchvat	k1gInPc4	záchvat
podobné	podobný	k2eAgFnSc3d1	podobná
epilepsii	epilepsie	k1gFnSc3	epilepsie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
kdekoliv	kdekoliv	k6eAd1	kdekoliv
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
přítomnosti	přítomnost	k1gFnSc2	přítomnost
nesměly	smět	k5eNaImAgInP	smět
znít	znít	k5eAaImF	znít
zvony	zvon	k1gInPc1	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Lékařskou	lékařský	k2eAgFnSc7d1	lékařská
terminologií	terminologie	k1gFnSc7	terminologie
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
choroba	choroba	k1gFnSc1	choroba
nazývá	nazývat	k5eAaImIp3nS	nazývat
idiosynkrazie	idiosynkrazie	k1gFnSc1	idiosynkrazie
–	–	k?	–
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
přecitlivělost	přecitlivělost	k1gFnSc1	přecitlivělost
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
frekvence	frekvence	k1gFnPc4	frekvence
zvukového	zvukový	k2eAgNnSc2d1	zvukové
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
přichází	přicházet	k5eAaImIp3nS	přicházet
gotický	gotický	k2eAgInSc1d1	gotický
životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
rytířská	rytířský	k2eAgFnSc1d1	rytířská
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
turnaje	turnaj	k1gInPc1	turnaj
<g/>
,	,	kIx,	,
šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
obliba	obliba	k1gFnSc1	obliba
dvorské	dvorský	k2eAgFnSc2d1	dvorská
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
zpěvů	zpěv	k1gInPc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
upevněním	upevnění	k1gNnSc7	upevnění
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
zvětšením	zvětšení	k1gNnSc7	zvětšení
českého	český	k2eAgInSc2d1	český
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
vzestupem	vzestup	k1gInSc7	vzestup
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
výstavbou	výstavba	k1gFnSc7	výstavba
a	a	k8xC	a
rozvojem	rozvoj	k1gInSc7	rozvoj
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
příchod	příchod	k1gInSc1	příchod
etnických	etnický	k2eAgMnPc2d1	etnický
Němců	Němec	k1gMnPc2	Němec
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
privilegia	privilegium	k1gNnPc4	privilegium
Židům	Žid	k1gMnPc3	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
ovšem	ovšem	k9	ovšem
platili	platit	k5eAaImAgMnP	platit
nemalé	malý	k2eNgFnPc4d1	nemalá
částky	částka	k1gFnPc4	částka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Boje	boj	k1gInPc1	boj
s	s	k7c7	s
rakouským	rakouský	k2eAgMnSc7d1	rakouský
vévodou	vévoda	k1gMnSc7	vévoda
a	a	k8xC	a
markrabím	markrabí	k1gMnSc7	markrabí
Přemyslem	Přemysl	k1gMnSc7	Přemysl
==	==	k?	==
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
roku	rok	k1gInSc2	rok
1231	[number]	k4	1231
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vévoda	vévoda	k1gMnSc1	vévoda
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bojovný	bojovný	k2eAgMnSc1d1	bojovný
zapudil	zapudit	k5eAaPmAgMnS	zapudit
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
Žofii	Žofie	k1gFnSc4	Žofie
<g/>
,	,	kIx,	,
sestru	sestra	k1gFnSc4	sestra
uherské	uherský	k2eAgFnSc2d1	uherská
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Václavova	Václavův	k2eAgFnSc1d1	Václavova
matka	matka	k1gFnSc1	matka
také	také	k9	také
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
poté	poté	k6eAd1	poté
využil	využít	k5eAaPmAgMnS	využít
rozporu	rozpora	k1gFnSc4	rozpora
mezi	mezi	k7c7	mezi
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
<g/>
,	,	kIx,	,
moravským	moravský	k2eAgMnSc7d1	moravský
markrabím	markrabí	k1gMnSc7	markrabí
Přemyslem	Přemysl	k1gMnSc7	Přemysl
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1233	[number]	k4	1233
pronikl	proniknout	k5eAaPmAgInS	proniknout
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
obsadil	obsadit	k5eAaPmAgInS	obsadit
hrad	hrad	k1gInSc1	hrad
Bítov	Bítov	k1gInSc1	Bítov
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ale	ale	k9	ale
rakouský	rakouský	k2eAgMnSc1d1	rakouský
vévoda	vévoda	k1gMnSc1	vévoda
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
a	a	k8xC	a
odtáhl	odtáhnout	k5eAaPmAgMnS	odtáhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
donutil	donutit	k5eAaPmAgMnS	donutit
bratra	bratr	k1gMnSc4	bratr
k	k	k7c3	k
poslušnosti	poslušnost	k1gFnSc3	poslušnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
uvrhl	uvrhnout	k5eAaPmAgMnS	uvrhnout
Fridricha	Fridrich	k1gMnSc2	Fridrich
Bojovného	bojovný	k2eAgMnSc2d1	bojovný
do	do	k7c2	do
říšské	říšský	k2eAgFnSc2d1	říšská
klatby	klatba	k1gFnSc2	klatba
(	(	kIx(	(
<g/>
1236	[number]	k4	1236
<g/>
)	)	kIx)	)
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
rozšířit	rozšířit	k5eAaPmF	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Pověřil	pověřit	k5eAaPmAgMnS	pověřit
Václava	Václav	k1gMnSc2	Václav
vedením	vedení	k1gNnSc7	vedení
války	válka	k1gFnSc2	válka
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
Dolního	dolní	k2eAgNnSc2d1	dolní
Rakouska	Rakousko	k1gNnSc2	Rakousko
i	i	k8xC	i
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
ovšem	ovšem	k9	ovšem
stěží	stěží	k6eAd1	stěží
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
potěšen	potěšit	k5eAaPmNgMnS	potěšit
tak	tak	k6eAd1	tak
zjevnou	zjevný	k2eAgFnSc7d1	zjevná
expanzí	expanze	k1gFnSc7	expanze
císařské	císařský	k2eAgFnSc2d1	císařská
autority	autorita	k1gFnSc2	autorita
tak	tak	k8xC	tak
blízko	blízko	k7c2	blízko
svých	svůj	k3xOyFgFnPc2	svůj
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Diplomacie	diplomacie	k1gFnSc1	diplomacie
říšských	říšský	k2eAgMnPc2d1	říšský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
obávajících	obávající	k2eAgMnPc2d1	obávající
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
císař	císař	k1gMnSc1	císař
ziskem	zisk	k1gInSc7	zisk
Rakous	Rakousy	k1gInPc2	Rakousy
příliš	příliš	k6eAd1	příliš
posílil	posílit	k5eAaPmAgInS	posílit
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
usmíření	usmíření	k1gNnSc3	usmíření
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
s	s	k7c7	s
Fridrichem	Fridrich	k1gMnSc7	Fridrich
Bojovným	bojovný	k2eAgMnSc7d1	bojovný
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1237	[number]	k4	1237
klatbu	klatba	k1gFnSc4	klatba
raději	rád	k6eAd2	rád
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
riskoval	riskovat	k5eAaBmAgMnS	riskovat
další	další	k2eAgFnSc4d1	další
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
bojovou	bojový	k2eAgFnSc4d1	bojová
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
Bojovný	bojovný	k2eAgMnSc1d1	bojovný
nakonec	nakonec	k6eAd1	nakonec
zaslíbil	zaslíbit	k5eAaPmAgMnS	zaslíbit
svou	svůj	k3xOyFgFnSc4	svůj
neteř	neteř	k1gFnSc4	neteř
Gertrudu	Gertrud	k1gMnSc3	Gertrud
Václavovu	Václavův	k2eAgMnSc3d1	Václavův
synovi	syn	k1gMnSc3	syn
Vladislavovi	Vladislav	k1gMnSc3	Vladislav
a	a	k8xC	a
jako	jako	k9	jako
věno	věno	k1gNnSc4	věno
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
pod	pod	k7c4	pod
českou	český	k2eAgFnSc4d1	Česká
správu	správa	k1gFnSc4	správa
území	území	k1gNnSc2	území
Rakous	Rakousy	k1gInPc2	Rakousy
severně	severně	k6eAd1	severně
od	od	k7c2	od
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1233	[number]	k4	1233
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
1237	[number]	k4	1237
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Přemyslem	Přemysl	k1gMnSc7	Přemysl
<g/>
,	,	kIx,	,
markrabětem	markrabě	k1gMnSc7	markrabě
moravským	moravský	k2eAgMnSc7d1	moravský
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
sporu	spor	k1gInSc2	spor
bylo	být	k5eAaImAgNnS	být
předání	předání	k1gNnSc1	předání
Břeclavska	Břeclavsko	k1gNnSc2	Břeclavsko
Oldřichu	Oldřich	k1gMnSc3	Oldřich
Korutanskému	korutanský	k2eAgMnSc3d1	korutanský
<g/>
,	,	kIx,	,
synovi	syn	k1gMnSc3	syn
nejstarší	starý	k2eAgFnSc2d3	nejstarší
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
sestry	sestra	k1gFnSc2	sestra
Judity	Judita	k1gFnSc2	Judita
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
s	s	k7c7	s
početným	početný	k2eAgNnSc7d1	početné
vojskem	vojsko	k1gNnSc7	vojsko
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
ke	k	k7c3	k
králi	král	k1gMnSc3	král
Bélovi	Béla	k1gMnSc3	Béla
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Přičiněním	přičinění	k1gNnSc7	přičinění
Bély	Béla	k1gMnSc2	Béla
a	a	k8xC	a
královny	královna	k1gFnSc2	královna
vdovy	vdova	k1gFnSc2	vdova
Konstancie	Konstancie	k1gFnSc2	Konstancie
se	se	k3xPyFc4	se
bratři	bratr	k1gMnPc1	bratr
smířili	smířit	k5eAaPmAgMnP	smířit
a	a	k8xC	a
Přemyslova	Přemyslův	k2eAgFnSc1d1	Přemyslova
vláda	vláda	k1gFnSc1	vláda
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
jen	jen	k9	jen
na	na	k7c4	na
Olomoucko	Olomoucko	k1gNnSc4	Olomoucko
a	a	k8xC	a
Opavsko	Opavsko	k1gNnSc4	Opavsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
Mongolům	Mongol	k1gMnPc3	Mongol
==	==	k?	==
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
odražení	odražení	k1gNnSc4	odražení
mongolského	mongolský	k2eAgInSc2d1	mongolský
vpádu	vpád	k1gInSc2	vpád
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
značné	značný	k2eAgNnSc1d1	značné
ušetření	ušetření	k1gNnSc1	ušetření
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
před	před	k7c7	před
Mongoly	Mongol	k1gMnPc7	Mongol
<g/>
.	.	kIx.	.
</s>
<s>
Opevnil	opevnit	k5eAaPmAgInS	opevnit
pohraniční	pohraniční	k2eAgFnSc4d1	pohraniční
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
knížeti	kníže	k1gMnSc3	kníže
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pobožnému	pobožný	k2eAgMnSc3d1	pobožný
<g/>
,	,	kIx,	,
svému	svůj	k1gMnSc3	svůj
švagrovi	švagr	k1gMnSc3	švagr
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
ovšem	ovšem	k9	ovšem
na	na	k7c4	na
Václava	Václav	k1gMnSc4	Václav
nechtěl	chtít	k5eNaImAgMnS	chtít
čekat	čekat	k5eAaImF	čekat
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1241	[number]	k4	1241
svedl	svést	k5eAaPmAgMnS	svést
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Lehnice	Lehnice	k1gFnSc2	Lehnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdy	tehdy	k6eAd1	tehdy
voj	voj	k1gInSc4	voj
nájezdníků	nájezdník	k1gMnPc2	nájezdník
protáhl	protáhnout	k5eAaPmAgMnS	protáhnout
od	od	k7c2	od
severu	sever	k1gInSc6	sever
Moravou	Morava	k1gFnSc7	Morava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
poplenil	poplenit	k5eAaPmAgMnS	poplenit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
tak	tak	k6eAd1	tak
došel	dojít	k5eAaPmAgInS	dojít
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Václavovi	Václav	k1gMnSc3	Václav
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
německým	německý	k2eAgMnPc3d1	německý
spojencům	spojenec	k1gMnPc3	spojenec
se	se	k3xPyFc4	se
koncem	konec	k1gInSc7	konec
roku	rok	k1gInSc2	rok
1241	[number]	k4	1241
(	(	kIx(	(
<g/>
když	když	k8xS	když
zamrzlý	zamrzlý	k2eAgInSc1d1	zamrzlý
Dunaj	Dunaj	k1gInSc1	Dunaj
dovolil	dovolit	k5eAaPmAgInS	dovolit
Mongolům	Mongol	k1gMnPc3	Mongol
přejít	přejít	k5eAaPmF	přejít
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
břeh	břeh	k1gInSc4	břeh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
zabránit	zabránit	k5eAaPmF	zabránit
postupu	postup	k1gInSc3	postup
Mongolů	Mongol	k1gMnPc2	Mongol
vedeným	vedený	k2eAgMnSc7d1	vedený
chánem	chán	k1gMnSc7	chán
Bátú	Bátú	k1gFnPc2	Bátú
k	k	k7c3	k
Vídni	Vídeň	k1gFnSc3	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
obranou	obrana	k1gFnSc7	obrana
velmi	velmi	k6eAd1	velmi
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
jeho	jeho	k3xOp3gFnSc4	jeho
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
potom	potom	k6eAd1	potom
k	k	k7c3	k
Bátúovi	Bátúa	k1gMnSc3	Bátúa
dorazili	dorazit	k5eAaPmAgMnP	dorazit
poslové	posel	k1gMnPc1	posel
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
velkého	velký	k2eAgMnSc2d1	velký
chána	chán	k1gMnSc2	chán
Ögedeje	Ögedej	k1gMnSc2	Ögedej
a	a	k8xC	a
s	s	k7c7	s
povoláním	povolání	k1gNnSc7	povolání
na	na	k7c4	na
Kurultaj	Kurultaj	k1gInSc4	Kurultaj
<g/>
.	.	kIx.	.
</s>
<s>
Bátú	Bátú	k?	Bátú
musel	muset	k5eAaImAgMnS	muset
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
invaze	invaze	k1gFnSc2	invaze
na	na	k7c4	na
západ	západ	k1gInSc4	západ
upustit	upustit	k5eAaPmF	upustit
a	a	k8xC	a
vydat	vydat	k5eAaPmF	vydat
se	se	k3xPyFc4	se
na	na	k7c6	na
Karakorumu	Karakorum	k1gInSc6	Karakorum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbě	volba	k1gFnSc6	volba
nového	nový	k2eAgMnSc2d1	nový
chána	chán	k1gMnSc2	chán
se	se	k3xPyFc4	se
Mongolové	Mongol	k1gMnPc1	Mongol
o	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
již	již	k6eAd1	již
nepokusili	pokusit	k5eNaPmAgMnP	pokusit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzpoura	vzpoura	k1gFnSc1	vzpoura
mladšího	mladý	k2eAgMnSc2d2	mladší
syna	syn	k1gMnSc2	syn
==	==	k?	==
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bojovný	bojovný	k2eAgInSc1d1	bojovný
neteř	neteř	k1gFnSc4	neteř
Gertrudu	Gertrud	k1gInSc2	Gertrud
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
přímo	přímo	k6eAd1	přímo
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Václavovi	Václav	k1gMnSc3	Václav
slíbil	slíbit	k5eAaPmAgMnS	slíbit
pomoc	pomoc	k1gFnSc4	pomoc
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Béla	Béla	k1gMnSc1	Béla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
Fridrich	Fridrich	k1gMnSc1	Fridrich
Bojovný	bojovný	k2eAgInSc1d1	bojovný
odňal	odnít	k5eAaPmAgInS	odnít
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
za	za	k7c2	za
vpádu	vpád	k1gInSc2	vpád
Tatarů	Tatar	k1gMnPc2	Tatar
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Ota	Ota	k1gMnSc1	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bavorský	bavorský	k2eAgInSc1d1	bavorský
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
vojsko	vojsko	k1gNnSc1	vojsko
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nepodpořeno	podpořen	k2eNgNnSc1d1	podpořen
a	a	k8xC	a
poraženo	poražen	k2eAgNnSc1d1	poraženo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
český	český	k2eAgMnSc1d1	český
princ	princ	k1gMnSc1	princ
a	a	k8xC	a
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
Vladislav	Vladislav	k1gMnSc1	Vladislav
roku	rok	k1gInSc2	rok
1246	[number]	k4	1246
s	s	k7c7	s
Gertrudou	Gertrudý	k2eAgFnSc7d1	Gertrudý
Babenberskou	babenberský	k2eAgFnSc7d1	Babenberská
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
vévoda	vévoda	k1gMnSc1	vévoda
Fridrich	Fridrich	k1gMnSc1	Fridrich
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
rakouské	rakouský	k2eAgFnSc2d1	rakouská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc1	předchůdce
následoval	následovat	k5eAaImAgMnS	následovat
už	už	k6eAd1	už
o	o	k7c4	o
půl	půl	k1xP	půl
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1247	[number]	k4	1247
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
královna	královna	k1gFnSc1	královna
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
zemřela	zemřít	k5eAaPmAgFnS	zemřít
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
1248	[number]	k4	1248
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těžce	těžce	k6eAd1	těžce
zkoušený	zkoušený	k2eAgMnSc1d1	zkoušený
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
k	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
nelibosti	nelibost	k1gFnSc3	nelibost
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
i	i	k8xC	i
druhorozeného	druhorozený	k2eAgMnSc2d1	druhorozený
syna	syn	k1gMnSc2	syn
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
postupně	postupně	k6eAd1	postupně
přišel	přijít	k5eAaPmAgInS	přijít
a	a	k8xC	a
i	i	k9	i
jinak	jinak	k6eAd1	jinak
zanedbával	zanedbávat	k5eAaImAgMnS	zanedbávat
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtici	šlechtic	k1gMnPc1	šlechtic
proti	proti	k7c3	proti
Václavovi	Václavův	k2eAgMnPc1d1	Václavův
povstali	povstat	k5eAaPmAgMnP	povstat
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
královým	králův	k2eAgMnSc7d1	králův
synem	syn	k1gMnSc7	syn
Přemyslem	Přemysl	k1gMnSc7	Přemysl
(	(	kIx(	(
<g/>
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
naplno	naplno	k6eAd1	naplno
roku	rok	k1gInSc2	rok
1248	[number]	k4	1248
se	s	k7c7	s
střídavými	střídavý	k2eAgInPc7d1	střídavý
úspěchy	úspěch	k1gInPc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
opanovat	opanovat	k5eAaPmF	opanovat
celé	celý	k2eAgFnPc4d1	celá
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
,	,	kIx,	,
když	když	k8xS	když
Václav	Václav	k1gMnSc1	Václav
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
obležení	obležení	k1gNnSc6	obležení
Prahy	Praha	k1gFnSc2	Praha
musel	muset	k5eAaImAgInS	muset
odejít	odejít	k5eAaPmF	odejít
na	na	k7c4	na
panství	panství	k1gNnSc4	panství
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
věrných	věrný	k2eAgMnPc2d1	věrný
Boreše	Boreše	k1gMnPc2	Boreše
z	z	k7c2	z
Rýzmburka	Rýzmburek	k1gMnSc2	Rýzmburek
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
u	u	k7c2	u
Mostu	most	k1gInSc2	most
se	se	k3xPyFc4	se
obratnou	obratný	k2eAgFnSc7d1	obratná
politikou	politika	k1gFnSc7	politika
podařilo	podařit	k5eAaPmAgNnS	podařit
Václavovi	Václav	k1gMnSc3	Václav
donutit	donutit	k5eAaPmF	donutit
syna	syn	k1gMnSc2	syn
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
byl	být	k5eAaImAgMnS	být
krátce	krátce	k6eAd1	krátce
vězněn	věznit	k5eAaImNgMnS	věznit
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Přimda	Přimdo	k1gNnSc2	Přimdo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
spojenci	spojenec	k1gMnPc1	spojenec
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
omilostněn	omilostnit	k5eAaPmNgMnS	omilostnit
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jako	jako	k9	jako
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Babenberské	babenberský	k2eAgNnSc1d1	babenberské
dědictví	dědictví	k1gNnSc1	dědictví
==	==	k?	==
</s>
</p>
<p>
<s>
Gertruda	Gertruda	k1gFnSc1	Gertruda
Babenberská	babenberský	k2eAgFnSc1d1	Babenberská
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
nárocích	nárok	k1gInPc6	nárok
na	na	k7c4	na
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
dalších	další	k2eAgMnPc2d1	další
manželů	manžel	k1gMnPc2	manžel
se	se	k3xPyFc4	se
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
jednotně	jednotně	k6eAd1	jednotně
a	a	k8xC	a
babenberské	babenberský	k2eAgNnSc1d1	babenberské
dědictví	dědictví	k1gNnSc1	dědictví
jim	on	k3xPp3gMnPc3	on
nakonec	nakonec	k6eAd1	nakonec
spadlo	spadnout	k5eAaPmAgNnS	spadnout
do	do	k7c2	do
klína	klín	k1gInSc2	klín
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
rakouská	rakouský	k2eAgFnSc1d1	rakouská
šlechta	šlechta	k1gFnSc1	šlechta
zvolila	zvolit	k5eAaPmAgFnS	zvolit
za	za	k7c4	za
nového	nový	k2eAgMnSc4d1	nový
vévodu	vévoda	k1gMnSc4	vévoda
Přemysla	Přemysl	k1gMnSc2	Přemysl
(	(	kIx(	(
<g/>
1251	[number]	k4	1251
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgMnSc1d1	budoucí
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Markétou	Markéta	k1gFnSc7	Markéta
Babenberskou	babenberský	k2eAgFnSc7d1	Babenberská
<g/>
,	,	kIx,	,
sestrou	sestra	k1gFnSc7	sestra
vévody	vévoda	k1gMnSc2	vévoda
Fridricha	Fridrich	k1gMnSc2	Fridrich
Bojovného	bojovný	k2eAgMnSc2d1	bojovný
a	a	k8xC	a
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
Jindřichu	Jindřich	k1gMnSc6	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
tak	tak	k9	tak
na	na	k7c4	na
příští	příští	k2eAgFnSc4d1	příští
čtvrt	čtvrt	k1gFnSc4	čtvrt
století	století	k1gNnSc2	století
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
rakouské	rakouský	k2eAgFnPc4d1	rakouská
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
Jednooký	jednooký	k2eAgMnSc1d1	jednooký
zemřel	zemřít	k5eAaPmAgMnS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1253	[number]	k4	1253
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
dvoře	dvůr	k1gInSc6	dvůr
v	v	k7c6	v
Počáplích	Počáple	k1gFnPc6	Počáple
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
byly	být	k5eAaImAgFnP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Počaplích	Počapl	k1gInPc6	Počapl
a	a	k8xC	a
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
pochováno	pochovat	k5eAaPmNgNnS	pochovat
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
v	v	k7c6	v
Anežském	anežský	k2eAgInSc6d1	anežský
klášteře	klášter	k1gInSc6	klášter
(	(	kIx(	(
<g/>
opuková	opukový	k2eAgFnSc1d1	opuková
hrobka	hrobka	k1gFnSc1	hrobka
pod	pod	k7c7	pod
podlahou	podlaha	k1gFnSc7	podlaha
kostela	kostel	k1gInSc2	kostel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
historických	historický	k2eAgInPc2d1	historický
románů	román	k1gInPc2	román
Dítě	Dítě	k2eAgInPc2d1	Dítě
z	z	k7c2	z
Apulie	Apulie	k1gFnSc2	Apulie
a	a	k8xC	a
Zapomenutý	zapomenutý	k2eAgMnSc1d1	zapomenutý
Král	Král	k1gMnSc1	Král
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Ludmily	Ludmila	k1gFnSc2	Ludmila
Vaňkové	Vaňková	k1gFnSc2	Vaňková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Václavovou	Václavův	k2eAgFnSc7d1	Václavova
jedinou	jediný	k2eAgFnSc7d1	jediná
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1224	[number]	k4	1224
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
Štaufská	Štaufský	k2eAgFnSc1d1	Štaufská
(	(	kIx(	(
<g/>
1200	[number]	k4	1200
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
1248	[number]	k4	1248
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
(	(	kIx(	(
<g/>
1227	[number]	k4	1227
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1247	[number]	k4	1247
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
∞	∞	k?	∞
1246	[number]	k4	1246
rakouská	rakouský	k2eAgFnSc1d1	rakouská
a	a	k8xC	a
štýrská	štýrský	k2eAgFnSc1d1	štýrská
titulární	titulární	k2eAgFnSc1d1	titulární
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Gertruda	Gertruda	k1gFnSc1	Gertruda
Babenberská	babenberský	k2eAgFnSc1d1	Babenberská
(	(	kIx(	(
<g/>
1226	[number]	k4	1226
<g/>
–	–	k?	–
<g/>
1288	[number]	k4	1288
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Božena	Božena	k1gFnSc1	Božena
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
1227	[number]	k4	1227
a	a	k8xC	a
1230	[number]	k4	1230
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1290	[number]	k4	1290
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1243	[number]	k4	1243
braniborský	braniborský	k2eAgMnSc1d1	braniborský
markrabě	markrabě	k1gMnSc1	markrabě
Ota	Ota	k1gMnSc1	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Braniborský	braniborský	k2eAgMnSc1d1	braniborský
(	(	kIx(	(
<g/>
1215	[number]	k4	1215
<g/>
–	–	k?	–
<g/>
1267	[number]	k4	1267
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anežka	Anežka	k1gFnSc1	Anežka
(	(	kIx(	(
<g/>
po	po	k7c6	po
1227	[number]	k4	1227
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1268	[number]	k4	1268
<g/>
)	)	kIx)	)
∞	∞	k?	∞
míšeňský	míšeňský	k2eAgMnSc1d1	míšeňský
markrabě	markrabě	k1gMnSc1	markrabě
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1215	[number]	k4	1215
<g/>
/	/	kIx~	/
<g/>
1216	[number]	k4	1216
<g/>
–	–	k?	–
<g/>
1288	[number]	k4	1288
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1233	[number]	k4	1233
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1278	[number]	k4	1278
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
∞	∞	k?	∞
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
/	/	kIx~	/
1252	[number]	k4	1252
rakouská	rakouský	k2eAgFnSc1d1	rakouská
a	a	k8xC	a
štýrská	štýrský	k2eAgFnSc1d1	štýrská
titulární	titulární	k2eAgFnSc1d1	titulární
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Markéta	Markéta	k1gFnSc1	Markéta
Babenberská	babenberský	k2eAgFnSc1d1	Babenberská
(	(	kIx(	(
<g/>
1204	[number]	k4	1204
<g/>
/	/	kIx~	/
<g/>
1205	[number]	k4	1205
<g/>
–	–	k?	–
<g/>
1266	[number]	k4	1266
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
/	/	kIx~	/
1261	[number]	k4	1261
princezna	princezna	k1gFnSc1	princezna
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
Uherská	uherský	k2eAgFnSc1d1	uherská
(	(	kIx(	(
<g/>
1245	[number]	k4	1245
<g/>
–	–	k?	–
<g/>
1285	[number]	k4	1285
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Věnceslava	Věnceslava	k1gFnSc1	Věnceslava
(	(	kIx(	(
<g/>
†	†	k?	†
před	před	k7c7	před
1248	[number]	k4	1248
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Královská	královský	k2eAgNnPc1d1	královské
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
<g/>
)	)	kIx)	)
založená	založený	k2eAgFnSc1d1	založená
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Čechy	Čech	k1gMnPc7	Čech
===	===	k?	===
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pražské	pražský	k2eAgNnSc1d1	Pražské
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1235	[number]	k4	1235
<g/>
–	–	k?	–
<g/>
1245	[number]	k4	1245
</s>
</p>
<p>
<s>
Stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1240	[number]	k4	1240
</s>
</p>
<p>
<s>
Loket	loket	k1gInSc1	loket
<g/>
,	,	kIx,	,
1240	[number]	k4	1240
<g/>
–	–	k?	–
<g/>
1253	[number]	k4	1253
</s>
</p>
<p>
<s>
Žatec	Žatec	k1gInSc1	Žatec
<g/>
,	,	kIx,	,
snad	snad	k9	snad
už	už	k9	už
1249	[number]	k4	1249
<g/>
,	,	kIx,	,
určitě	určitě	k6eAd1	určitě
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1265	[number]	k4	1265
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
privilegium	privilegium	k1gNnSc1	privilegium
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
před	před	k7c7	před
1250	[number]	k4	1250
</s>
</p>
<p>
<s>
===	===	k?	===
Morava	Morava	k1gFnSc1	Morava
===	===	k?	===
</s>
</p>
<p>
<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
</s>
</p>
<p>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
ca	ca	kA	ca
1240	[number]	k4	1240
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1238	[number]	k4	1238
<g/>
,	,	kIx,	,
1240	[number]	k4	1240
–	–	k?	–
nejstarší	starý	k2eAgInSc1d3	nejstarší
doklad	doklad	k1gInSc1	doklad
rychtáře	rychtář	k1gMnSc2	rychtář
1243	[number]	k4	1243
zakládací	zakládací	k2eAgFnSc1d1	zakládací
listina	listina	k1gFnSc1	listina
</s>
</p>
<p>
<s>
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
1252	[number]	k4	1252
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
za	za	k7c2	za
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1192	[number]	k4	1192
<g/>
–	–	k?	–
<g/>
1253	[number]	k4	1253
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
432	[number]	k4	432
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
446	[number]	k4	446
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BÁRTA	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Smíření	smíření	k1gNnSc1	smíření
otce	otec	k1gMnSc2	otec
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Uzavření	uzavření	k1gNnSc1	uzavření
sporu	spor	k1gInSc2	spor
krále	král	k1gMnSc4	král
Václava	Václav	k1gMnSc4	Václav
I.	I.	kA	I.
s	s	k7c7	s
markrabětem	markrabě	k1gMnSc7	markrabě
Přemyslem	Přemysl	k1gMnSc7	Přemysl
roku	rok	k1gInSc2	rok
1249	[number]	k4	1249
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
NODL	NODL	kA	NODL
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Rituál	rituál	k1gInSc1	rituál
smíření	smíření	k1gNnSc2	smíření
:	:	kIx,	:
konflikt	konflikt	k1gInSc1	konflikt
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
řešení	řešení	k1gNnSc1	řešení
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
:	:	kIx,	:
sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
konané	konaný	k2eAgFnSc2d1	konaná
ve	v	k7c6	v
dnech	den	k1gInPc6	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86488	[number]	k4	86488
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
101	[number]	k4	101
<g/>
–	–	k?	–
<g/>
108	[number]	k4	108
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUTHAN	KUTHAN	k?	KUTHAN
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Dvorské	Dvorské	k2eAgNnSc1d1	Dvorské
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
zakladatelské	zakladatelský	k2eAgNnSc1d1	zakladatelské
dílo	dílo	k1gNnSc1	dílo
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
a	a	k8xC	a
rozkvět	rozkvět	k1gInSc1	rozkvět
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
KUTHAN	KUTHAN	k?	KUTHAN
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Splendor	Splendor	k1gInSc1	Splendor
et	et	k?	et
Gloria	Gloria	k1gFnSc1	Gloria
Regni	Regeň	k1gFnSc3	Regeň
Bohemiae	Bohemia	k1gInSc2	Bohemia
:	:	kIx,	:
umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
jako	jako	k8xS	jako
projev	projev	k1gInSc1	projev
vladařské	vladařský	k2eAgFnSc2d1	vladařská
reprezentace	reprezentace	k1gFnSc2	reprezentace
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
státní	státní	k2eAgFnSc2d1	státní
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
dějin	dějiny	k1gFnPc2	dějiny
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
umění	umění	k1gNnSc2	umění
Katolické	katolický	k2eAgFnSc2d1	katolická
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87082	[number]	k4	87082
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
1208	[number]	k4	1208
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
královské	královský	k2eAgFnPc1d1	královská
za	za	k7c2	za
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1197	[number]	k4	1197
<g/>
–	–	k?	–
<g/>
1253	[number]	k4	1253
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
1085	[number]	k4	1085
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MIKA	Mik	k1gMnSc4	Mik
<g/>
,	,	kIx,	,
Norbert	Norbert	k1gMnSc1	Norbert
<g/>
.	.	kIx.	.
</s>
<s>
Walka	Walka	k1gFnSc1	Walka
o	o	k7c4	o
spadek	spadek	k1gInSc4	spadek
po	po	k7c4	po
Babenbergach	Babenbergach	k1gInSc4	Babenbergach
1246	[number]	k4	1246
<g/>
–	–	k?	–
<g/>
1278	[number]	k4	1278
<g/>
.	.	kIx.	.
</s>
<s>
Racibórz	Racibórz	k1gMnSc1	Racibórz
<g/>
:	:	kIx,	:
WAW	WAW	kA	WAW
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
136	[number]	k4	136
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
919765	[number]	k4	919765
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PEKAŘ	Pekař	k1gMnSc1	Pekař
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
povstání	povstání	k1gNnSc6	povstání
kralevice	kralevic	k1gMnSc2	kralevic
Přemysla	Přemysl	k1gMnSc2	Přemysl
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
Václavovi	Václav	k1gMnSc3	Václav
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Katolický	katolický	k2eAgInSc1d1	katolický
Literární	literární	k2eAgInSc1d1	literární
Klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
25	[number]	k4	25
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOVADINA	SOVADINA	kA	SOVADINA
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
.	.	kIx.	.
</s>
<s>
Dvůr	Dvůr	k1gInSc1	Dvůr
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
archivních	archivní	k2eAgFnPc2d1	archivní
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
45	[number]	k4	45
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
5246	[number]	k4	5246
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VANÍČEK	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
II	II	kA	II
<g/>
.	.	kIx.	.
1197	[number]	k4	1197
<g/>
–	–	k?	–
<g/>
1250	[number]	k4	1250
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
582	[number]	k4	582
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
273	[number]	k4	273
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VANÍČEK	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
III	III	kA	III
<g/>
.	.	kIx.	.
1250	[number]	k4	1250
<g/>
–	–	k?	–
<g/>
1310	[number]	k4	1310
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
760	[number]	k4	760
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
433	[number]	k4	433
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Století	století	k1gNnSc1	století
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
412	[number]	k4	412
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
281	[number]	k4	281
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
Čech	Čechy	k1gFnPc2	Čechy
královských	královský	k2eAgFnPc2d1	královská
1198	[number]	k4	1198
<g/>
–	–	k?	–
<g/>
1253	[number]	k4	1253
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
964	[number]	k4	964
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
140	[number]	k4	140
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
93	[number]	k4	93
<g/>
–	–	k?	–
<g/>
101	[number]	k4	101
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Václav	Václava	k1gFnPc2	Václava
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Václav	Václav	k1gMnSc1	Václav
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
na	na	k7c6	na
e-stredovek	etredovka	k1gFnPc2	e-stredovka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Listina	listina	k1gFnSc1	listina
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
Anežce	Anežka	k1gFnSc6	Anežka
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1236	[number]	k4	1236
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Listina	listina	k1gFnSc1	listina
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
Konstancii	Konstancie	k1gFnSc6	Konstancie
Uherské	uherský	k2eAgNnSc1d1	Uherské
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1233	[number]	k4	1233
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Listina	listina	k1gFnSc1	listina
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
a	a	k8xC	a
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
Štaufské	Štaufský	k2eAgFnSc2d1	Štaufská
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1238	[number]	k4	1238
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1230	[number]	k4	1230
<g/>
–	–	k?	–
<g/>
1253	[number]	k4	1253
<g/>
)	)	kIx)	)
</s>
</p>
