<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Austrálie	Austrálie	k1gFnSc2	Austrálie
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
modrého	modrý	k2eAgInSc2d1	modrý
obdélníku	obdélník	k1gInSc2	obdélník
<g/>
,	,	kIx,	,
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
Union	union	k1gInSc1	union
Jack	Jack	k1gInSc1	Jack
–	–	k?	–
symbol	symbol	k1gInSc4	symbol
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
–	–	k?	–
připomínající	připomínající	k2eAgFnSc2d1	připomínající
vazby	vazba	k1gFnSc2	vazba
Austrálie	Austrálie	k1gFnSc2	Austrálie
na	na	k7c4	na
britskou	britský	k2eAgFnSc4d1	britská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
sedmicípá	sedmicípý	k2eAgFnSc1d1	sedmicípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Federation	Federation	k1gInSc4	Federation
Star	Star	kA	Star
(	(	kIx(	(
<g/>
Hvězda	Hvězda	k1gMnSc1	Hvězda
federace	federace	k1gFnSc2	federace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
šest	šest	k4xCc1	šest
cípů	cíp	k1gInPc2	cíp
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
šest	šest	k4xCc1	šest
svazových	svazový	k2eAgInPc2d1	svazový
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
sedmý	sedmý	k4xOgMnSc1	sedmý
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgMnSc1d1	společný
pro	pro	k7c4	pro
dvě	dva	k4xCgNnPc4	dva
nezávislá	závislý	k2eNgNnPc4d1	nezávislé
federální	federální	k2eAgNnPc4d1	federální
teritoria	teritorium	k1gNnPc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
polovině	polovina	k1gFnSc6	polovina
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
zobrazeno	zobrazen	k2eAgNnSc1d1	zobrazeno
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Jižního	jižní	k2eAgInSc2d1	jižní
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Austrálií	Austrálie	k1gFnSc7	Austrálie
často	často	k6eAd1	často
spojováno	spojován	k2eAgNnSc1d1	spojováno
<g/>
,	,	kIx,	,
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
nejjasnější	jasný	k2eAgFnSc7d3	nejjasnější
hvězdou	hvězda	k1gFnSc7	hvězda
Acrux	Acrux	k1gInSc1	Acrux
(	(	kIx(	(
<g/>
zcela	zcela	k6eAd1	zcela
dole	dole	k6eAd1	dole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Austrálie	Austrálie	k1gFnSc2	Austrálie
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1909	[number]	k4	1909
na	na	k7c6	na
základě	základ	k1gInSc6	základ
veřejné	veřejný	k2eAgFnSc2d1	veřejná
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
diskuse	diskuse	k1gFnPc1	diskuse
ohledně	ohledně	k7c2	ohledně
výměny	výměna	k1gFnSc2	výměna
Union	union	k1gInSc1	union
Jacku	Jack	k1gInSc2	Jack
za	za	k7c4	za
jiný	jiný	k2eAgInSc4d1	jiný
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
designérů	designér	k1gMnPc2	designér
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Franck	Franck	k1gMnSc1	Franck
Gentil	Gentil	k1gMnSc1	Gentil
s	s	k7c7	s
designem	design	k1gInSc7	design
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yQgInSc6	který
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
britský	britský	k2eAgInSc1d1	britský
kanton	kanton	k1gInSc1	kanton
hvězdou	hvězda	k1gFnSc7	hvězda
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnSc2	vlajka
australských	australský	k2eAgInPc2d1	australský
nižších	nízký	k2eAgInPc2d2	nižší
celků	celek	k1gInPc2	celek
==	==	k?	==
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
šest	šest	k4xCc4	šest
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
tři	tři	k4xCgNnPc4	tři
kontinentální	kontinentální	k2eAgNnPc4d1	kontinentální
teritoria	teritorium	k1gNnPc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
celky	celek	k1gInPc1	celek
kromě	kromě	k7c2	kromě
malého	malý	k2eAgNnSc2d1	malé
teritoria	teritorium	k1gNnSc2	teritorium
Jervisova	Jervisův	k2eAgFnSc1d1	Jervisův
zátoka	zátoka	k1gFnSc1	zátoka
užívají	užívat	k5eAaImIp3nP	užívat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
k	k	k7c3	k
Austrálii	Austrálie	k1gFnSc3	Austrálie
patří	patřit	k5eAaImIp3nS	patřit
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
(	(	kIx(	(
<g/>
ostrovních	ostrovní	k2eAgFnPc2d1	ostrovní
či	či	k8xC	či
vnějších	vnější	k2eAgFnPc2d1	vnější
<g/>
)	)	kIx)	)
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vlajku	vlajka	k1gFnSc4	vlajka
užívají	užívat	k5eAaImIp3nP	užívat
jen	jen	k6eAd1	jen
některá	některý	k3yIgNnPc1	některý
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
užívají	užívat	k5eAaImIp3nP	užívat
vlajku	vlajka	k1gFnSc4	vlajka
australskou	australský	k2eAgFnSc4d1	australská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajky	vlajka	k1gFnPc4	vlajka
australských	australský	k2eAgInPc2d1	australský
států	stát	k1gInPc2	stát
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajky	vlajka	k1gFnPc4	vlajka
australských	australský	k2eAgNnPc2d1	Australské
federálních	federální	k2eAgNnPc2d1	federální
teritorií	teritorium	k1gNnPc2	teritorium
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajky	vlajka	k1gFnPc4	vlajka
australských	australský	k2eAgNnPc2d1	Australské
vnějších	vnější	k2eAgNnPc2d1	vnější
teritorií	teritorium	k1gNnPc2	teritorium
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Austrálie	Austrálie	k1gFnSc2	Austrálie
</s>
</p>
<p>
<s>
Australská	australský	k2eAgFnSc1d1	australská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Austrálie	Austrálie	k1gFnSc2	Austrálie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Australská	australský	k2eAgFnSc1d1	australská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
