<p>
<s>
Emma	Emma	k1gFnSc1	Emma
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
červnajul	červnajout	k5eAaPmAgInS	červnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1869	[number]	k4	1869
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
anarchistka	anarchistka	k1gFnSc1	anarchistka
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Proslavila	proslavit	k5eAaPmAgFnS	proslavit
se	se	k3xPyFc4	se
politickým	politický	k2eAgInSc7d1	politický
aktivismem	aktivismus	k1gInSc7	aktivismus
<g/>
,	,	kIx,	,
spisy	spis	k1gInPc7	spis
a	a	k8xC	a
přednáškami	přednáška	k1gFnPc7	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sehrála	sehrát	k5eAaPmAgFnS	sehrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
anarchistické	anarchistický	k2eAgFnSc2d1	anarchistická
politické	politický	k2eAgFnSc2d1	politická
filozofie	filozofie	k1gFnSc2	filozofie
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Kovně	Kovna	k1gFnSc6	Kovna
na	na	k7c6	na
území	území	k1gNnSc6	území
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Kaunas	Kaunas	k1gInSc1	Kaunas
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
anarchismu	anarchismus	k1gInSc3	anarchismus
ji	on	k3xPp3gFnSc4	on
přivedly	přivést	k5eAaPmAgFnP	přivést
události	událost	k1gFnSc3	událost
haymarketského	haymarketský	k2eAgInSc2d1	haymarketský
masakru	masakr	k1gInSc2	masakr
<g/>
,	,	kIx,	,
krvavého	krvavý	k2eAgInSc2d1	krvavý
střetu	střet	k1gInSc2	střet
chicagských	chicagský	k2eAgMnPc2d1	chicagský
demonstrantů	demonstrant	k1gMnPc2	demonstrant
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
zabitím	zabití	k1gNnSc7	zabití
desítek	desítka	k1gFnPc2	desítka
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
odsouzením	odsouzení	k1gNnSc7	odsouzení
několika	několik	k4yIc2	několik
anarchistů	anarchista	k1gMnPc2	anarchista
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
přednášky	přednáška	k1gFnPc1	přednáška
o	o	k7c6	o
anarchistické	anarchistický	k2eAgFnSc6d1	anarchistická
filozofii	filozofie	k1gFnSc6	filozofie
<g/>
,	,	kIx,	,
ženských	ženský	k2eAgNnPc6d1	ženské
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
aktuálních	aktuální	k2eAgInPc6d1	aktuální
sociálních	sociální	k2eAgInPc6d1	sociální
problémech	problém	k1gInPc6	problém
se	se	k3xPyFc4	se
těšily	těšit	k5eAaImAgFnP	těšit
návštěvám	návštěva	k1gFnPc3	návštěva
tisíců	tisíc	k4xCgInPc2	tisíc
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
anarchistickým	anarchistický	k2eAgMnSc7d1	anarchistický
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Berkmanem	Berkman	k1gMnSc7	Berkman
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
celoživotním	celoživotní	k2eAgMnSc7d1	celoživotní
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
plánovala	plánovat	k5eAaImAgFnS	plánovat
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
průmyslníka	průmyslník	k1gMnSc4	průmyslník
a	a	k8xC	a
finančníka	finančník	k1gMnSc4	finančník
Henryho	Henry	k1gMnSc4	Henry
Claye	Clay	k1gMnSc4	Clay
Fricka	Fricek	k1gMnSc4	Fricek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
proveden	provést	k5eAaPmNgInS	provést
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
propaganda	propaganda	k1gFnSc1	propaganda
činem	čin	k1gInSc7	čin
<g/>
.	.	kIx.	.
</s>
<s>
Frick	Frick	k6eAd1	Frick
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
vraždu	vražda	k1gFnSc4	vražda
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
přežil	přežít	k5eAaPmAgInS	přežít
a	a	k8xC	a
Berkmana	Berkman	k1gMnSc4	Berkman
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
k	k	k7c3	k
22	[number]	k4	22
letům	let	k1gInPc3	let
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
několikrát	několikrát	k6eAd1	několikrát
vězněna	věznit	k5eAaImNgFnS	věznit
za	za	k7c2	za
"	"	kIx"	"
<g/>
podněcování	podněcování	k1gNnSc2	podněcování
k	k	k7c3	k
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
"	"	kIx"	"
a	a	k8xC	a
ilegální	ilegální	k2eAgNnSc4d1	ilegální
šíření	šíření	k1gNnSc4	šíření
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
antikoncepčních	antikoncepční	k2eAgFnPc6d1	antikoncepční
metodách	metoda	k1gFnPc6	metoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
založila	založit	k5eAaPmAgFnS	založit
anarchistický	anarchistický	k2eAgInSc4d1	anarchistický
měsíčník	měsíčník	k1gInSc4	měsíčník
Mother	Mothra	k1gFnPc2	Mothra
Earth	Eartha	k1gFnPc2	Eartha
(	(	kIx(	(
<g/>
Matka	matka	k1gFnSc1	matka
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byli	být	k5eAaImAgMnP	být
Goldmanová	Goldmanový	k2eAgNnPc4d1	Goldmanový
s	s	k7c7	s
Berkmanem	Berkman	k1gInSc7	Berkman
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
ke	k	k7c3	k
dvěma	dva	k4xCgNnPc3	dva
letům	léto	k1gNnPc3	léto
vězení	vězení	k1gNnSc2	vězení
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nově	nově	k6eAd1	nově
nařízenými	nařízený	k2eAgInPc7d1	nařízený
odvody	odvod	k1gInPc7	odvod
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
"	"	kIx"	"
<g/>
nabádali	nabádat	k5eAaBmAgMnP	nabádat
spoluobčany	spoluobčan	k1gMnPc7	spoluobčan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhýbali	vyhýbat	k5eAaImAgMnP	vyhýbat
registraci	registrace	k1gFnSc3	registrace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
byli	být	k5eAaImAgMnP	být
společně	společně	k6eAd1	společně
se	s	k7c7	s
stovkami	stovka	k1gFnPc7	stovka
dalších	další	k2eAgFnPc2d1	další
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
a	a	k8xC	a
deportováni	deportovat	k5eAaBmNgMnP	deportovat
do	do	k7c2	do
rodného	rodný	k2eAgNnSc2d1	rodné
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
zpočátku	zpočátku	k6eAd1	zpočátku
podporovala	podporovat	k5eAaImAgFnS	podporovat
bolševickou	bolševický	k2eAgFnSc4d1	bolševická
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
po	po	k7c6	po
kronštadtském	kronštadtský	k2eAgNnSc6d1	Kronštadtské
povstání	povstání	k1gNnSc6	povstání
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
organizovali	organizovat	k5eAaBmAgMnP	organizovat
levicoví	levicový	k2eAgMnPc1d1	levicový
oponenti	oponent	k1gMnPc1	oponent
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
za	za	k7c4	za
násilné	násilný	k2eAgNnSc4d1	násilné
potlačování	potlačování	k1gNnSc4	potlačování
hlasu	hlas	k1gInSc2	hlas
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
své	svůj	k3xOyFgFnSc2	svůj
zkušenosti	zkušenost	k1gFnSc2	zkušenost
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
My	my	k3xPp1nPc1	my
Disillusionment	Disillusionment	k1gMnSc1	Disillusionment
in	in	k?	in
Russia	Russia	k1gFnSc1	Russia
(	(	kIx(	(
<g/>
Jak	jak	k8xC	jak
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
zklamala	zklamat	k5eAaPmAgFnS	zklamat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
života	život	k1gInSc2	život
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Francii	Francie	k1gFnSc3	Francie
napsala	napsat	k5eAaBmAgFnS	napsat
autobiografii	autobiografie	k1gFnSc4	autobiografie
Living	Living	k1gInSc1	Living
My	my	k3xPp1nPc1	my
Life	Life	k1gNnPc3	Life
(	(	kIx(	(
<g/>
Žít	žít	k5eAaImF	žít
můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
propukla	propuknout	k5eAaPmAgFnS	propuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
tam	tam	k6eAd1	tam
odjela	odjet	k5eAaPmAgFnS	odjet
podpořit	podpořit	k5eAaPmF	podpořit
anarchistickou	anarchistický	k2eAgFnSc4d1	anarchistická
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdivovatelé	obdivovatel	k1gMnPc1	obdivovatel
ji	on	k3xPp3gFnSc4	on
za	za	k7c2	za
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
oslavovali	oslavovat	k5eAaImAgMnP	oslavovat
jako	jako	k9	jako
volnomyšlenkářskou	volnomyšlenkářský	k2eAgFnSc4d1	volnomyšlenkářská
rebelku	rebelka	k1gFnSc4	rebelka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kritici	kritik	k1gMnPc1	kritik
ji	on	k3xPp3gFnSc4	on
odsuzovali	odsuzovat	k5eAaImAgMnP	odsuzovat
jako	jako	k9	jako
zastánkyni	zastánkyně	k1gFnSc4	zastánkyně
politicky	politicky	k6eAd1	politicky
motivovaných	motivovaný	k2eAgFnPc2d1	motivovaná
vražd	vražda	k1gFnPc2	vražda
a	a	k8xC	a
násilné	násilný	k2eAgFnSc2d1	násilná
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
a	a	k8xC	a
přednáškách	přednáška	k1gFnPc6	přednáška
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaImAgFnS	věnovat
širokému	široký	k2eAgNnSc3d1	široké
poli	pole	k1gNnSc6	pole
témat	téma	k1gNnPc2	téma
od	od	k7c2	od
věznění	věznění	k1gNnSc2	věznění
přes	přes	k7c4	přes
ateismus	ateismus	k1gInSc4	ateismus
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
militarismus	militarismus	k1gInSc4	militarismus
<g/>
,	,	kIx,	,
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc4	manželství
a	a	k8xC	a
volnou	volný	k2eAgFnSc4d1	volná
lásku	láska	k1gFnSc4	láska
až	až	k9	až
po	po	k7c4	po
homosexualitu	homosexualita	k1gFnSc4	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
sice	sice	k8xC	sice
s	s	k7c7	s
první	první	k4xOgFnSc7	první
vlnou	vlna	k1gFnSc7	vlna
feminismu	feminismus	k1gInSc2	feminismus
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
bojem	boj	k1gInSc7	boj
za	za	k7c4	za
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
politickou	politický	k2eAgFnSc4d1	politická
otázku	otázka	k1gFnSc4	otázka
genderu	gendera	k1gFnSc4	gendera
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
nově	nově	k6eAd1	nově
propojit	propojit	k5eAaPmF	propojit
s	s	k7c7	s
anarchismem	anarchismus	k1gInSc7	anarchismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
Kovně	Kovna	k1gFnSc6	Kovna
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Kaunas	Kaunas	k1gInSc1	Kaunas
<g/>
)	)	kIx)	)
v	v	k7c6	v
pobaltské	pobaltský	k2eAgFnSc6d1	pobaltská
části	část	k1gFnSc6	část
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
sourozenci	sourozenec	k1gMnPc1	sourozenec
neposlouchali	poslouchat	k5eNaImAgMnP	poslouchat
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
bil	bít	k5eAaImAgMnS	bít
a	a	k8xC	a
na	na	k7c4	na
nejvzdornější	vzdorný	k2eAgFnSc4d3	vzdorný
Emmu	Emma	k1gFnSc4	Emma
dokonce	dokonce	k9	dokonce
vytahoval	vytahovat	k5eAaImAgInS	vytahovat
bič	bič	k1gInSc1	bič
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
děti	dítě	k1gFnPc4	dítě
bránila	bránit	k5eAaImAgFnS	bránit
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
sestrám	sestra	k1gFnPc3	sestra
<g/>
,	,	kIx,	,
Heleně	Helena	k1gFnSc3	Helena
a	a	k8xC	a
Leně	Lena	k1gFnSc3	Lena
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
rozdílný	rozdílný	k2eAgInSc4d1	rozdílný
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
Helena	Helena	k1gFnSc1	Helena
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
byla	být	k5eAaImAgFnS	být
potřebnou	potřebný	k2eAgFnSc7d1	potřebná
oporou	opora	k1gFnSc7	opora
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Lena	Lena	k1gFnSc1	Lena
byla	být	k5eAaImAgFnS	být
citově	citově	k6eAd1	citově
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
také	také	k9	také
tři	tři	k4xCgMnPc1	tři
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
6	[number]	k4	6
letech	let	k1gInPc6	let
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Herman	Herman	k1gMnSc1	Herman
a	a	k8xC	a
Moishe	Moishe	k1gFnSc1	Moishe
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
strávila	strávit	k5eAaPmAgFnS	strávit
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
útlém	útlý	k2eAgInSc6d1	útlý
věku	věk	k1gInSc6	věk
zažila	zažít	k5eAaPmAgFnS	zažít
antisemitské	antisemitský	k2eAgInPc4d1	antisemitský
útoky	útok	k1gInPc4	útok
a	a	k8xC	a
setkala	setkat	k5eAaPmAgFnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
krutostí	krutost	k1gFnSc7	krutost
vojenských	vojenský	k2eAgInPc2d1	vojenský
odvodů	odvod	k1gInPc2	odvod
<g/>
.	.	kIx.	.
</s>
<s>
Dospívající	dospívající	k2eAgFnSc4d1	dospívající
Goldmanovou	Goldmanová	k1gFnSc4	Goldmanová
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
názory	názor	k1gInPc1	názor
nihilistů	nihilista	k1gMnPc2	nihilista
a	a	k8xC	a
narodniků	narodnik	k1gMnPc2	narodnik
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
jejich	jejich	k3xOp3gFnPc2	jejich
kolegyň	kolegyně	k1gFnPc2	kolegyně
<g/>
)	)	kIx)	)
a	a	k8xC	a
nadšena	nadchnout	k5eAaPmNgFnS	nadchnout
myšlenkou	myšlenka	k1gFnSc7	myšlenka
"	"	kIx"	"
<g/>
chození	chození	k1gNnSc1	chození
mezi	mezi	k7c4	mezi
lid	lid	k1gInSc4	lid
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
dělnicí	dělnice	k1gFnSc7	dělnice
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
korzety	korzet	k1gInPc4	korzet
<g/>
.	.	kIx.	.
<g/>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
však	však	k9	však
s	s	k7c7	s
nevlastní	vlastní	k2eNgFnSc7d1	nevlastní
sestrou	sestra	k1gFnSc7	sestra
Helenou	Helena	k1gFnSc7	Helena
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1885	[number]	k4	1885
připluly	připlout	k5eAaPmAgInP	připlout
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usídlily	usídlit	k5eAaPmAgInP	usídlit
v	v	k7c6	v
Rochesteru	Rochester	k1gInSc6	Rochester
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1887	[number]	k4	1887
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
kolegu	kolega	k1gMnSc4	kolega
dělníka	dělník	k1gMnSc4	dělník
Jacoba	Jacoba	k1gFnSc1	Jacoba
Kersnera	Kersner	k1gMnSc2	Kersner
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získala	získat	k5eAaPmAgFnS	získat
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
a	a	k8xC	a
opětovném	opětovný	k2eAgNnSc6d1	opětovné
sezdání	sezdání	k1gNnSc6	sezdání
spolu	spolu	k6eAd1	spolu
ovšem	ovšem	k9	ovšem
už	už	k6eAd1	už
dál	daleko	k6eAd2	daleko
nežili	žít	k5eNaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Emma	Emma	k1gFnSc1	Emma
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
stala	stát	k5eAaPmAgFnS	stát
anarchistkou	anarchistka	k1gFnSc7	anarchistka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zklamání	zklamání	k1gNnSc4	zklamání
z	z	k7c2	z
poměrů	poměr	k1gInPc2	poměr
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
procesem	proces	k1gInSc7	proces
s	s	k7c7	s
anarchisty	anarchista	k1gMnPc7	anarchista
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
haymarketském	haymarketský	k2eAgInSc6d1	haymarketský
masakru	masakr	k1gInSc6	masakr
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gFnSc3	jejichž
popravě	poprava	k1gFnSc3	poprava
došlo	dojít	k5eAaPmAgNnS	dojít
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Solidarita	solidarita	k1gFnSc1	solidarita
s	s	k7c7	s
oběťmi	oběť	k1gFnPc7	oběť
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
ztotožnění	ztotožnění	k1gNnSc3	ztotožnění
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
názory	názor	k1gInPc7	názor
nejen	nejen	k6eAd1	nejen
Goldmanovou	Goldmanův	k2eAgFnSc7d1	Goldmanova
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
sblížila	sblížit	k5eAaPmAgFnS	sblížit
s	s	k7c7	s
newyorskými	newyorský	k2eAgMnPc7d1	newyorský
anarchisty	anarchista	k1gMnPc7	anarchista
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
kolem	kolem	k7c2	kolem
Johanna	Johann	k1gInSc2	Johann
Mosta	Most	k1gInSc2	Most
a	a	k8xC	a
časopisu	časopis	k1gInSc2	časopis
Die	Die	k1gMnSc1	Die
Freiheit	Freiheit	k1gMnSc1	Freiheit
(	(	kIx(	(
<g/>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
anarchistou	anarchista	k1gMnSc7	anarchista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rovněž	rovněž	k9	rovněž
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Berkmanem	Berkman	k1gMnSc7	Berkman
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
milostný	milostný	k2eAgInSc1d1	milostný
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přetavil	přetavit	k5eAaPmAgInS	přetavit
v	v	k7c4	v
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Zařadila	zařadit	k5eAaPmAgFnS	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
americké	americký	k2eAgMnPc4d1	americký
anarchistické	anarchistický	k2eAgMnPc4d1	anarchistický
komunisty	komunista	k1gMnPc4	komunista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vedle	vedle	k7c2	vedle
kolektivismu	kolektivismus	k1gInSc2	kolektivismus
kladla	klást	k5eAaImAgFnS	klást
důraz	důraz	k1gInSc4	důraz
i	i	k9	i
na	na	k7c6	na
možnosti	možnost	k1gFnSc6	možnost
jedince	jedinec	k1gMnSc4	jedinec
a	a	k8xC	a
všestranný	všestranný	k2eAgInSc4d1	všestranný
rozvoj	rozvoj	k1gInSc4	rozvoj
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ohrazovala	ohrazovat	k5eAaImAgFnS	ohrazovat
se	se	k3xPyFc4	se
také	také	k9	také
proti	proti	k7c3	proti
puritánským	puritánský	k2eAgInPc3d1	puritánský
rysům	rys	k1gInPc3	rys
chování	chování	k1gNnSc2	chování
některých	některý	k3yIgMnPc2	některý
anarchistů	anarchista	k1gMnPc2	anarchista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
ji	on	k3xPp3gFnSc4	on
bývá	bývat	k5eAaImIp3nS	bývat
připisován	připisován	k2eAgInSc1d1	připisován
výrok	výrok	k1gInSc1	výrok
"	"	kIx"	"
<g/>
když	když	k8xS	když
nemohu	moct	k5eNaImIp1nS	moct
tančit	tančit	k5eAaImF	tančit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
má	můj	k3xOp1gFnSc1	můj
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
jeho	jeho	k3xOp3gFnPc1	jeho
varianty	varianta	k1gFnPc1	varianta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
nikdy	nikdy	k6eAd1	nikdy
nevyslovila	vyslovit	k5eNaPmAgFnS	vyslovit
<g/>
.	.	kIx.	.
<g/>
Americkou	americký	k2eAgFnSc4d1	americká
veřejnost	veřejnost	k1gFnSc4	veřejnost
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
odcizovala	odcizovat	k5eAaImAgFnS	odcizovat
především	především	k9	především
postojem	postoj	k1gInSc7	postoj
k	k	k7c3	k
politickému	politický	k2eAgNnSc3d1	politické
násilí	násilí	k1gNnSc3	násilí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1892	[number]	k4	1892
soukromá	soukromý	k2eAgFnSc1d1	soukromá
ochranka	ochranka	k1gFnSc1	ochranka
<g/>
,	,	kIx,	,
najatá	najatý	k2eAgFnSc1d1	najatá
továrníkem	továrník	k1gMnSc7	továrník
Henrym	Henry	k1gMnSc7	Henry
Frickem	Fricek	k1gMnSc7	Fricek
<g/>
,	,	kIx,	,
zastřelila	zastřelit	k5eAaPmAgFnS	zastřelit
devět	devět	k4xCc4	devět
stávkujících	stávkující	k2eAgMnPc2d1	stávkující
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
s	s	k7c7	s
Berkmanem	Berkman	k1gInSc7	Berkman
se	se	k3xPyFc4	se
je	on	k3xPp3gNnSc4	on
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pomstít	pomstít	k5eAaPmF	pomstít
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
ten	ten	k3xDgInSc1	ten
měsíc	měsíc	k1gInSc1	měsíc
Berkman	Berkman	k1gMnSc1	Berkman
na	na	k7c6	na
Fricka	Fricek	k1gMnSc2	Fricek
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kanceláři	kancelář	k1gFnSc6	kancelář
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
ho	on	k3xPp3gMnSc4	on
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
22	[number]	k4	22
<g/>
letého	letý	k2eAgInSc2d1	letý
trestu	trest	k1gInSc2	trest
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Goldmanovou	Goldmanová	k1gFnSc4	Goldmanová
vyšetřovali	vyšetřovat	k5eAaImAgMnP	vyšetřovat
za	za	k7c4	za
spoluvinu	spoluvina	k1gFnSc4	spoluvina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
veřejnosti	veřejnost	k1gFnSc2	veřejnost
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
uškodilo	uškodit	k5eAaPmAgNnS	uškodit
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
vycestovala	vycestovat	k5eAaPmAgFnS	vycestovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
)	)	kIx)	)
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
jako	jako	k9	jako
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
sestra	sestra	k1gFnSc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
přednášky	přednáška	k1gFnPc4	přednáška
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
později	pozdě	k6eAd2	pozdě
vzpomínala	vzpomínat	k5eAaImAgNnP	vzpomínat
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
–	–	k?	–
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gNnSc4	jeho
tvrzení	tvrzení	k1gNnSc4	tvrzení
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
mezi	mezi	k7c4	mezi
problémy	problém	k1gInPc4	problém
jedinců	jedinec	k1gMnPc2	jedinec
či	či	k8xC	či
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
potlačováním	potlačování	k1gNnSc7	potlačování
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
tužeb	tužba	k1gFnPc2	tužba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
USA	USA	kA	USA
nadále	nadále	k6eAd1	nadále
bojovala	bojovat	k5eAaImAgFnS	bojovat
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
projevu	projev	k1gInSc2	projev
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
propagovat	propagovat	k5eAaImF	propagovat
volnou	volný	k2eAgFnSc4d1	volná
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
základem	základ	k1gInSc7	základ
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
úcta	úcta	k1gFnSc1	úcta
mezi	mezi	k7c7	mezi
partnery	partner	k1gMnPc7	partner
(	(	kIx(	(
<g/>
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
modelem	model	k1gInSc7	model
maloměšťácké	maloměšťácký	k2eAgFnSc2d1	maloměšťácká
rodiny	rodina	k1gFnSc2	rodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emma	Emma	k1gFnSc1	Emma
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
vydávala	vydávat	k5eAaImAgFnS	vydávat
časopis	časopis	k1gInSc4	časopis
Mother	Mothra	k1gFnPc2	Mothra
Earth	Eartha	k1gFnPc2	Eartha
(	(	kIx(	(
<g/>
Matka	matka	k1gFnSc1	matka
Zem	zem	k1gFnSc1	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
díly	díl	k1gInPc7	díl
publikovala	publikovat	k5eAaBmAgFnS	publikovat
knihu	kniha	k1gFnSc4	kniha
Anarchism	Anarchisma	k1gFnPc2	Anarchisma
and	and	k?	and
Other	Other	k1gInSc1	Other
Essays	Essays	k1gInSc1	Essays
(	(	kIx(	(
<g/>
Anarchismus	anarchismus	k1gInSc1	anarchismus
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zasazovala	zasazovat	k5eAaImAgFnS	zasazovat
se	se	k3xPyFc4	se
za	za	k7c4	za
zveřejňování	zveřejňování	k1gNnSc4	zveřejňování
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
antikoncepci	antikoncepce	k1gFnSc6	antikoncepce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
protestovala	protestovat	k5eAaBmAgFnS	protestovat
proti	proti	k7c3	proti
nucenému	nucený	k2eAgInSc3d1	nucený
odvodu	odvod	k1gInSc3	odvod
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
dostala	dostat	k5eAaPmAgFnS	dostat
(	(	kIx(	(
<g/>
i	i	k9	i
s	s	k7c7	s
Berkmanem	Berkman	k1gInSc7	Berkman
<g/>
)	)	kIx)	)
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
deportováni	deportovat	k5eAaBmNgMnP	deportovat
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
ruské	ruský	k2eAgFnSc3d1	ruská
bolševické	bolševický	k2eAgFnSc3d1	bolševická
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
kronštadtském	kronštadtský	k2eAgNnSc6d1	Kronštadtské
povstání	povstání	k1gNnSc6	povstání
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
za	za	k7c4	za
násilné	násilný	k2eAgNnSc4d1	násilné
potlačování	potlačování	k1gNnSc4	potlačování
hlasu	hlas	k1gInSc2	hlas
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Berkman	Berkman	k1gMnSc1	Berkman
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1936	[number]	k4	1936
po	po	k7c6	po
komplikovaných	komplikovaný	k2eAgFnPc6d1	komplikovaná
operacích	operace	k1gFnPc6	operace
a	a	k8xC	a
následných	následný	k2eAgFnPc6d1	následná
nesnesitelných	snesitelný	k2eNgFnPc6d1	nesnesitelná
bolestech	bolest	k1gFnPc6	bolest
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
sebevraždě	sebevražda	k1gFnSc6	sebevražda
vytrhla	vytrhnout	k5eAaPmAgFnS	vytrhnout
Goldmanovou	Goldmanová	k1gFnSc4	Goldmanová
ze	z	k7c2	z
zármutku	zármutek	k1gInSc2	zármutek
až	až	k9	až
španělská	španělský	k2eAgFnSc1d1	španělská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
navštívila	navštívit	k5eAaPmAgFnS	navštívit
frontu	fronta	k1gFnSc4	fronta
i	i	k8xC	i
kolektivizované	kolektivizovaný	k2eAgFnPc4d1	kolektivizovaný
továrny	továrna	k1gFnPc4	továrna
a	a	k8xC	a
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
poslední	poslední	k2eAgNnPc4d1	poslední
léta	léto	k1gNnPc4	léto
věnovala	věnovat	k5eAaImAgFnS	věnovat
právě	právě	k6eAd1	právě
podpoře	podpora	k1gFnSc3	podpora
tamních	tamní	k2eAgMnPc2d1	tamní
anarchistů	anarchista	k1gMnPc2	anarchista
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
například	například	k6eAd1	například
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
španělské	španělský	k2eAgFnSc2d1	španělská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadách	řada	k1gFnPc6	řada
antifašistické	antifašistický	k2eAgFnSc2d1	Antifašistická
koalice	koalice	k1gFnSc2	koalice
ale	ale	k8xC	ale
tvrdě	tvrdě	k6eAd1	tvrdě
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
hlavně	hlavně	k9	hlavně
stalinisty	stalinista	k1gMnPc4	stalinista
<g/>
.	.	kIx.	.
<g/>
Emma	Emma	k1gFnSc1	Emma
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
zemřela	zemřít	k5eAaPmAgFnS	zemřít
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
nedožitých	dožitý	k2eNgNnPc2d1	nedožité
71	[number]	k4	71
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
poslední	poslední	k2eAgInSc1d1	poslední
článkem	článek	k1gInSc7	článek
zůstal	zůstat	k5eAaPmAgInS	zůstat
text	text	k1gInSc1	text
The	The	k1gMnSc1	The
Individual	Individual	k1gMnSc1	Individual
<g/>
,	,	kIx,	,
Society	societa	k1gFnPc1	societa
and	and	k?	and
the	the	k?	the
State	status	k1gInSc5	status
(	(	kIx(	(
<g/>
Jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
stát	stát	k1gInSc1	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyzdvihovala	vyzdvihovat	k5eAaImAgFnS	vyzdvihovat
individualismus	individualismus	k1gInSc4	individualismus
proti	proti	k7c3	proti
autoritářství	autoritářství	k1gNnSc3	autoritářství
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
varovala	varovat	k5eAaImAgFnS	varovat
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
zaměňováním	zaměňování	k1gNnSc7	zaměňování
s	s	k7c7	s
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
liberalismem	liberalismus	k1gInSc7	liberalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filozofie	filozofie	k1gFnSc1	filozofie
==	==	k?	==
</s>
</p>
<p>
<s>
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
vydatně	vydatně	k6eAd1	vydatně
přednášela	přednášet	k5eAaImAgFnS	přednášet
a	a	k8xC	a
psala	psát	k5eAaImAgFnS	psát
o	o	k7c6	o
nejrůznějších	různý	k2eAgNnPc6d3	nejrůznější
tématech	téma	k1gNnPc6	téma
<g/>
.	.	kIx.	.
</s>
<s>
Odmítala	odmítat	k5eAaImAgFnS	odmítat
ortodoxní	ortodoxní	k2eAgInPc4d1	ortodoxní
a	a	k8xC	a
fundamentalistické	fundamentalistický	k2eAgInPc4d1	fundamentalistický
postoje	postoj	k1gInPc4	postoj
a	a	k8xC	a
výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
přispěla	přispět	k5eAaPmAgFnS	přispět
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
oblastí	oblast	k1gFnPc2	oblast
moderní	moderní	k2eAgFnSc2d1	moderní
politické	politický	k2eAgFnSc2d1	politická
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
ji	on	k3xPp3gFnSc4	on
řada	řada	k1gFnSc1	řada
myslitelů	myslitel	k1gMnPc2	myslitel
a	a	k8xC	a
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
Michail	Michail	k1gMnSc1	Michail
Bakunin	Bakunin	k1gInSc1	Bakunin
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
David	David	k1gMnSc1	David
Thoreau	Thoreaa	k1gFnSc4	Thoreaa
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Kropotkin	Kropotkin	k1gMnSc1	Kropotkin
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gMnSc1	Ralph
Waldo	Waldo	k1gNnSc1	Waldo
Emerson	Emerson	k1gMnSc1	Emerson
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Černyševskij	Černyševskij	k1gMnSc1	Černyševskij
a	a	k8xC	a
Mary	Mary	k1gFnSc1	Mary
Wollstonecraft	Wollstonecrafta	k1gFnPc2	Wollstonecrafta
<g/>
.	.	kIx.	.
</s>
<s>
Inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
ji	on	k3xPp3gFnSc4	on
rovněž	rovněž	k6eAd1	rovněž
Friedrich	Friedrich	k1gMnSc1	Friedrich
Nietzsche	Nietzsch	k1gFnSc2	Nietzsch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
autobiografii	autobiografie	k1gFnSc6	autobiografie
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nietzsche	Nietzsche	k1gNnSc1	Nietzsche
nebyl	být	k5eNaImAgMnS	být
sociální	sociální	k2eAgMnSc1d1	sociální
teoretik	teoretik	k1gMnSc1	teoretik
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc1	ten
poeta	poeta	k1gMnSc1	poeta
<g/>
,	,	kIx,	,
rebel	rebel	k1gMnSc1	rebel
a	a	k8xC	a
inovátor	inovátor	k1gMnSc1	inovátor
<g/>
.	.	kIx.	.
</s>
<s>
Aristokracii	aristokracie	k1gFnSc4	aristokracie
neměl	mít	k5eNaImAgMnS	mít
ani	ani	k8xC	ani
v	v	k7c6	v
rodokmenu	rodokmen	k1gInSc6	rodokmen
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
peněžence	peněženka	k1gFnSc6	peněženka
<g/>
,	,	kIx,	,
aristokratický	aristokratický	k2eAgInSc1d1	aristokratický
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
duch	duch	k1gMnSc1	duch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
byl	být	k5eAaImAgMnS	být
Nietzsche	Nietzsche	k1gFnSc4	Nietzsche
anarchista	anarchista	k1gMnSc1	anarchista
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
skuteční	skutečný	k2eAgMnPc1d1	skutečný
anarchisté	anarchista	k1gMnPc1	anarchista
jsou	být	k5eAaImIp3nP	být
aristokraté	aristokrat	k1gMnPc1	aristokrat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Anarchismus	anarchismus	k1gInSc4	anarchismus
===	===	k?	===
</s>
</p>
<p>
<s>
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
nazírala	nazírat	k5eAaImAgFnS	nazírat
svět	svět	k1gInSc4	svět
skrze	skrze	k?	skrze
anarchismus	anarchismus	k1gInSc1	anarchismus
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
osobností	osobnost	k1gFnPc2	osobnost
historie	historie	k1gFnSc2	historie
tohoto	tento	k3xDgInSc2	tento
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Přitahoval	přitahovat	k5eAaImAgMnS	přitahovat
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
perzekuci	perzekuce	k1gFnSc3	perzekuce
anarchistů	anarchista	k1gMnPc2	anarchista
po	po	k7c6	po
haymarketském	haymarketský	k2eAgInSc6d1	haymarketský
masakru	masakr	k1gInSc6	masakr
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
hlas	hlas	k1gInSc1	hlas
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
často	často	k6eAd1	často
stával	stávat	k5eAaImAgInS	stávat
hlasem	hlas	k1gInSc7	hlas
anarchismu	anarchismus	k1gInSc2	anarchismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
eseji	esej	k1gFnSc6	esej
knihy	kniha	k1gFnSc2	kniha
Anarchism	Anarchism	k1gInSc1	Anarchism
and	and	k?	and
Other	Other	k1gInSc1	Other
Essays	Essays	k1gInSc1	Essays
(	(	kIx(	(
<g/>
Anarchismus	anarchismus	k1gInSc1	anarchismus
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tedy	tedy	k9	tedy
anarchismus	anarchismus	k1gInSc1	anarchismus
hlásá	hlásat	k5eAaImIp3nS	hlásat
osvobození	osvobození	k1gNnSc4	osvobození
lidské	lidský	k2eAgFnSc2d1	lidská
mysli	mysl	k1gFnSc2	mysl
od	od	k7c2	od
nadvlády	nadvláda	k1gFnSc2	nadvláda
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
osvobození	osvobození	k1gNnSc4	osvobození
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
od	od	k7c2	od
nadvlády	nadvláda	k1gFnSc2	nadvláda
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
osvobození	osvobození	k1gNnSc2	osvobození
od	od	k7c2	od
okovů	okov	k1gInPc2	okov
a	a	k8xC	a
omezování	omezování	k1gNnSc2	omezování
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimiž	jenž	k3xRgInPc7	jenž
stojí	stát	k5eAaImIp3nS	stát
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Anarchismus	anarchismus	k1gInSc1	anarchismus
hlásá	hlásat	k5eAaImIp3nS	hlásat
sociální	sociální	k2eAgInSc1d1	sociální
řád	řád	k1gInSc1	řád
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
volném	volný	k2eAgNnSc6d1	volné
sdružování	sdružování	k1gNnSc6	sdružování
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc2	který
společnosti	společnost	k1gFnSc2	společnost
přináší	přinášet	k5eAaImIp3nS	přinášet
skutečné	skutečný	k2eAgNnSc4d1	skutečné
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
každému	každý	k3xTgMnSc3	každý
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
volný	volný	k2eAgInSc1d1	volný
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
půdě	půda	k1gFnSc3	půda
a	a	k8xC	a
podle	podle	k7c2	podle
tužeb	tužba	k1gFnPc2	tužba
<g/>
,	,	kIx,	,
zálib	záliba	k1gFnPc2	záliba
a	a	k8xC	a
sklonů	sklon	k1gInPc2	sklon
každého	každý	k3xTgMnSc2	každý
jednoho	jeden	k4xCgMnSc2	jeden
člověka	člověk	k1gMnSc2	člověk
i	i	k8xC	i
plný	plný	k2eAgInSc4d1	plný
požitek	požitek	k1gInSc4	požitek
z	z	k7c2	z
životních	životní	k2eAgFnPc2d1	životní
nezbytností	nezbytnost	k1gFnPc2	nezbytnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anarchismus	anarchismus	k1gInSc1	anarchismus
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Goldmanovou	Goldmanová	k1gFnSc4	Goldmanová
vrcholně	vrcholně	k6eAd1	vrcholně
osobní	osobní	k2eAgFnSc7d1	osobní
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Domnívala	domnívat	k5eAaImAgFnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
anarchističtí	anarchistický	k2eAgMnPc1d1	anarchistický
myslitelé	myslitel	k1gMnPc1	myslitel
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
přetavovali	přetavovat	k5eAaImAgMnP	přetavovat
v	v	k7c4	v
činy	čin	k1gInPc4	čin
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yRnSc6	co
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
dávali	dávat	k5eAaImAgMnP	dávat
najevo	najevo	k6eAd1	najevo
každým	každý	k3xTgInSc7	každý
skutkem	skutek	k1gInSc7	skutek
i	i	k8xC	i
slovem	slovo	k1gNnSc7	slovo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nezajímá	zajímat	k5eNaImIp3nS	zajímat
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
zítra	zítra	k6eAd1	zítra
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
či	či	k8xC	či
<g />
.	.	kIx.	.
</s>
<s>
onen	onen	k3xDgMnSc1	onen
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
správnou	správný	k2eAgFnSc4d1	správná
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
psala	psát	k5eAaImAgFnS	psát
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
dnes	dnes	k6eAd1	dnes
dlí	dlít	k5eAaImIp3nS	dlít
správný	správný	k2eAgMnSc1d1	správný
duch	duch	k1gMnSc1	duch
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Anarchismus	anarchismus	k1gInSc4	anarchismus
a	a	k8xC	a
volné	volný	k2eAgNnSc4d1	volné
sdružování	sdružování	k1gNnSc4	sdružování
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
představovaly	představovat	k5eAaImAgFnP	představovat
logickou	logický	k2eAgFnSc4d1	logická
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
hranice	hranice	k1gFnPc4	hranice
vytvářené	vytvářený	k2eAgNnSc1d1	vytvářené
státní	státní	k2eAgFnSc7d1	státní
kontrolou	kontrola	k1gFnSc7	kontrola
a	a	k8xC	a
kapitalismem	kapitalismus	k1gInSc7	kapitalismus
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Právě	právě	k9	právě
<g />
.	.	kIx.	.
</s>
<s>
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
mě	já	k3xPp1nSc2	já
nové	nový	k2eAgFnPc4d1	nová
způsoby	způsob	k1gInPc7	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsala	napsat	k5eAaPmAgFnS	napsat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
staré	starý	k2eAgInPc4d1	starý
jimi	on	k3xPp3gMnPc7	on
nahradíme	nahradit	k5eAaPmIp1nP	nahradit
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
budeme	být	k5eAaImBp1nP	být
hlásat	hlásat	k5eAaImF	hlásat
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
budeme	být	k5eAaImBp1nP	být
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
budeme	být	k5eAaImBp1nP	být
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Součástí	součást	k1gFnSc7	součást
její	její	k3xOp3gFnSc2	její
filozofie	filozofie	k1gFnSc2	filozofie
byla	být	k5eAaImAgFnS	být
i	i	k8xC	i
opozice	opozice	k1gFnSc1	opozice
vůči	vůči	k7c3	vůči
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
volbám	volba	k1gFnPc3	volba
<g/>
.	.	kIx.	.
</s>
<s>
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
stát	stát	k1gInSc1	stát
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
a	a	k8xC	a
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
podstaty	podstata	k1gFnSc2	podstata
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
ovládání	ovládání	k1gNnSc3	ovládání
a	a	k8xC	a
dominanci	dominance	k1gFnSc3	dominance
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlasování	hlasování	k1gNnSc1	hlasování
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
je	být	k5eAaImIp3nS	být
přinejlepším	přinejlepším	k6eAd1	přinejlepším
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
<g/>
,	,	kIx,	,
přinejhorším	přinejhorším	k6eAd1	přinejhorším
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
psala	psát	k5eAaImAgFnS	psát
<g/>
,	,	kIx,	,
dodávají	dodávat	k5eAaImIp3nP	dodávat
lidem	člověk	k1gMnPc3	člověk
iluzi	iluze	k1gFnSc4	iluze
participace	participace	k1gFnPc1	participace
a	a	k8xC	a
zakrývají	zakrývat	k5eAaImIp3nP	zakrývat
struktury	struktura	k1gFnPc4	struktura
skutečného	skutečný	k2eAgNnSc2d1	skutečné
rozhodování	rozhodování	k1gNnSc2	rozhodování
<g/>
.	.	kIx.	.
</s>
<s>
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
podporovala	podporovat	k5eAaImAgFnS	podporovat
cílený	cílený	k2eAgInSc4d1	cílený
vzdor	vzdor	k1gInSc4	vzdor
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
stávek	stávka	k1gFnPc2	stávka
<g/>
,	,	kIx,	,
protestů	protest	k1gInPc2	protest
a	a	k8xC	a
"	"	kIx"	"
<g/>
přímé	přímý	k2eAgFnPc4d1	přímá
akce	akce	k1gFnPc4	akce
proti	proti	k7c3	proti
invazivní	invazivní	k2eAgFnSc3d1	invazivní
<g/>
,	,	kIx,	,
dotěrné	dotěrný	k2eAgFnSc3d1	dotěrná
autoritě	autorita	k1gFnSc3	autorita
naší	náš	k3xOp1gFnSc2	náš
morálky	morálka	k1gFnSc2	morálka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasný	souhlasný	k2eNgInSc4d1	nesouhlasný
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
volbám	volba	k1gFnPc3	volba
zastávala	zastávat	k5eAaImAgFnS	zastávat
i	i	k9	i
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
řada	řada	k1gFnSc1	řada
španělských	španělský	k2eAgMnPc2d1	španělský
anarchosyndikalistů	anarchosyndikalista	k1gMnPc2	anarchosyndikalista
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
pro	pro	k7c4	pro
ustavení	ustavení	k1gNnSc4	ustavení
liberální	liberální	k2eAgFnSc2d1	liberální
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
psala	psát	k5eAaImAgFnS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
anarchisté	anarchista	k1gMnPc1	anarchista
měli	mít	k5eAaImAgMnP	mít
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
třímá	třímat	k5eAaImIp3nS	třímat
jako	jako	k9	jako
v	v	k7c6	v
bloku	blok	k1gInSc6	blok
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
využít	využít	k5eAaPmF	využít
ke	k	k7c3	k
stávkování	stávkování	k1gNnSc3	stávkování
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
s	s	k7c7	s
hnutím	hnutí	k1gNnSc7	hnutí
za	za	k7c4	za
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
eseji	esej	k1gInSc6	esej
Woman	Woman	k1gInSc1	Woman
Suffrage	Suffrag	k1gInPc1	Suffrag
(	(	kIx(	(
<g/>
Ženské	ženský	k2eAgNnSc4d1	ženské
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
)	)	kIx)	)
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zapojení	zapojení	k1gNnSc1	zapojení
žen	žena	k1gFnPc2	žena
vneslo	vnést	k5eAaPmAgNnS	vnést
do	do	k7c2	do
demokratického	demokratický	k2eAgInSc2d1	demokratický
státu	stát	k1gInSc2	stát
více	hodně	k6eAd2	hodně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jako	jako	k9	jako
kdyby	kdyby	kYmCp3nP	kdyby
ženy	žena	k1gFnPc1	žena
nezaprodávaly	zaprodávat	k5eNaImAgFnP	zaprodávat
své	svůj	k3xOyFgInPc4	svůj
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nS	kdyby
političky	politička	k1gFnPc4	politička
nešlo	jít	k5eNaImAgNnS	jít
koupit	koupit	k5eAaPmF	koupit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Se	s	k7c7	s
sufražetkami	sufražetka	k1gFnPc7	sufražetka
se	se	k3xPyFc4	se
shodovala	shodovat	k5eAaImAgFnS	shodovat
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc4	žena
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rovné	rovný	k2eAgInPc4d1	rovný
s	s	k7c7	s
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
nemyslela	myslet	k5eNaImAgFnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
pouhá	pouhý	k2eAgFnSc1d1	pouhá
participace	participace	k1gFnSc1	participace
žen	žena	k1gFnPc2	žena
učinila	učinit	k5eAaImAgFnS	učinit
stát	stát	k5eAaPmF	stát
spravedlivějším	spravedlivý	k2eAgMnSc7d2	spravedlivější
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
bychom	by	kYmCp1nP	by
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
žena	žena	k1gFnSc1	žena
dokáže	dokázat	k5eAaPmIp3nS	dokázat
očistit	očistit	k5eAaPmF	očistit
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
očistě	očista	k1gFnSc3	očista
nepodvolí	podvolit	k5eNaPmIp3nS	podvolit
<g/>
,	,	kIx,	,
přisuzovali	přisuzovat	k5eAaImAgMnP	přisuzovat
bychom	by	kYmCp1nP	by
jí	on	k3xPp3gFnSc3	on
nadpřirozené	nadpřirozený	k2eAgFnSc3d1	nadpřirozená
schopnosti	schopnost	k1gFnSc3	schopnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Feminismus	feminismus	k1gInSc1	feminismus
a	a	k8xC	a
sexualita	sexualita	k1gFnSc1	sexualita
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
ostře	ostro	k6eAd1	ostro
odmítala	odmítat	k5eAaImAgFnS	odmítat
cíle	cíl	k1gInPc4	cíl
boje	boj	k1gInSc2	boj
za	za	k7c4	za
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
vedeného	vedený	k2eAgInSc2d1	vedený
první	první	k4xOgFnSc7	první
vlnou	vlna	k1gFnSc7	vlna
feminismu	feminismus	k1gInSc2	feminismus
<g/>
,	,	kIx,	,
ženská	ženská	k1gFnSc1	ženská
práva	právo	k1gNnSc2	právo
jako	jako	k8xS	jako
taková	takový	k3xDgFnSc1	takový
vášnivě	vášnivě	k6eAd1	vášnivě
hájila	hájit	k5eAaImAgFnS	hájit
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
pokládána	pokládán	k2eAgFnSc1d1	pokládána
za	za	k7c4	za
zakladatelku	zakladatelka	k1gFnSc4	zakladatelka
anarchofeminismu	anarchofeminismus	k1gInSc2	anarchofeminismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
brojí	brojit	k5eAaImIp3nS	brojit
proti	proti	k7c3	proti
patriarchátu	patriarchát	k1gInSc3	patriarchát
jako	jako	k8xS	jako
formě	forma	k1gFnSc3	forma
hierarchie	hierarchie	k1gFnSc2	hierarchie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
státní	státní	k2eAgFnSc3d1	státní
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
třídnímu	třídní	k2eAgNnSc3d1	třídní
rozdělení	rozdělení	k1gNnSc3	rozdělení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Požaduji	požadovat	k5eAaImIp1nS	požadovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc4	její
právo	právo	k1gNnSc4	právo
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
na	na	k7c4	na
vlastní	vlastní	k2eAgFnPc4d1	vlastní
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
žít	žít	k5eAaImF	žít
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
milovat	milovat	k5eAaImF	milovat
<g/>
,	,	kIx,	,
koho	kdo	k3yQnSc4	kdo
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zachce	zachtít	k5eAaPmIp3nS	zachtít
a	a	k8xC	a
kolik	kolik	k9	kolik
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zachce	zachtít	k5eAaPmIp3nS	zachtít
<g/>
.	.	kIx.	.
</s>
<s>
Požaduji	požadovat	k5eAaImIp1nS	požadovat
svobodu	svoboda	k1gFnSc4	svoboda
pro	pro	k7c4	pro
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
se	se	k3xPyFc4	se
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
jako	jako	k9	jako
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
sestra	sestra	k1gFnSc1	sestra
a	a	k8xC	a
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
první	první	k4xOgNnSc4	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
žen	žena	k1gFnPc2	žena
ohledně	ohledně	k7c2	ohledně
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
mnoho	mnoho	k4c4	mnoho
jejích	její	k3xOp3gFnPc2	její
feministických	feministický	k2eAgFnPc2d1	feministická
současnic	současnice	k1gFnPc2	současnice
považovala	považovat	k5eAaImAgFnS	považovat
interrupci	interrupce	k1gFnSc4	interrupce
za	za	k7c4	za
tragický	tragický	k2eAgInSc4d1	tragický
následek	následek	k1gInSc4	následek
sociálních	sociální	k2eAgFnPc2d1	sociální
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
plánované	plánovaný	k2eAgNnSc4d1	plánované
rodičovství	rodičovství	k1gNnSc4	rodičovství
viděla	vidět	k5eAaImAgFnS	vidět
jako	jako	k8xC	jako
přínosnou	přínosný	k2eAgFnSc4d1	přínosná
alternativu	alternativa	k1gFnSc4	alternativa
<g/>
.	.	kIx.	.
</s>
<s>
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
také	také	k9	také
hájila	hájit	k5eAaImAgFnS	hájit
volnou	volný	k2eAgFnSc4d1	volná
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
silně	silně	k6eAd1	silně
kritizovala	kritizovat	k5eAaImAgNnP	kritizovat
manželství	manželství	k1gNnPc1	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Průkopnice	průkopnice	k1gFnPc1	průkopnice
feminismu	feminismus	k1gInSc2	feminismus
měly	mít	k5eAaImAgFnP	mít
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
zúžené	zúžený	k2eAgFnSc2d1	zúžená
pole	pole	k1gFnSc2	pole
působnosti	působnost	k1gFnSc2	působnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nechaly	nechat	k5eAaPmAgInP	nechat
svazovat	svazovat	k5eAaImF	svazovat
společenskou	společenský	k2eAgFnSc7d1	společenská
silou	síla	k1gFnSc7	síla
puritánství	puritánství	k1gNnSc2	puritánství
a	a	k8xC	a
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
se	se	k3xPyFc4	se
vytrhnout	vytrhnout	k5eAaPmF	vytrhnout
starým	starý	k2eAgFnPc3d1	stará
tradicím	tradice	k1gFnPc3	tradice
a	a	k8xC	a
zvykům	zvyk	k1gInPc3	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
emancipaci	emancipace	k1gFnSc4	emancipace
žen	žena	k1gFnPc2	žena
učinilo	učinit	k5eAaPmAgNnS	učinit
tímto	tento	k3xDgInSc7	tento
směrem	směr	k1gInSc7	směr
zatím	zatím	k6eAd1	zatím
pouze	pouze	k6eAd1	pouze
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Otevřeně	otevřeně	k6eAd1	otevřeně
také	také	k9	také
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
předpojatost	předpojatost	k1gFnSc4	předpojatost
vůči	vůči	k7c3	vůči
homosexuálům	homosexuál	k1gMnPc3	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
společenské	společenský	k2eAgNnSc1d1	společenské
osvobození	osvobození	k1gNnSc1	osvobození
mělo	mít	k5eAaImAgNnS	mít
vztahovat	vztahovat	k5eAaImF	vztahovat
také	také	k9	také
na	na	k7c4	na
gaye	gay	k1gMnPc4	gay
a	a	k8xC	a
lesby	lesba	k1gFnPc4	lesba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
–	–	k?	–
dokonce	dokonce	k9	dokonce
i	i	k9	i
mezi	mezi	k7c7	mezi
anarchisty	anarchista	k1gMnPc7	anarchista
–	–	k?	–
prakticky	prakticky	k6eAd1	prakticky
neslýchaný	slýchaný	k2eNgMnSc1d1	neslýchaný
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
napsal	napsat	k5eAaBmAgMnS	napsat
německý	německý	k2eAgMnSc1d1	německý
sexuolog	sexuolog	k1gMnSc1	sexuolog
Magnus	Magnus	k1gMnSc1	Magnus
Hirschfeld	Hirschfeld	k1gMnSc1	Hirschfeld
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
vlastně	vlastně	k9	vlastně
prvním	první	k4xOgNnSc7	první
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
člověkem	člověk	k1gMnSc7	člověk
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
širokou	široký	k2eAgFnSc7d1	široká
veřejností	veřejnost	k1gFnSc7	veřejnost
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
homosexuální	homosexuální	k2eAgFnSc2d1	homosexuální
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Právo	právo	k1gNnSc1	právo
gayů	gay	k1gMnPc2	gay
a	a	k8xC	a
leseb	lesba	k1gFnPc2	lesba
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
milovali	milovat	k5eAaImAgMnP	milovat
koho	kdo	k3yInSc4	kdo
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
,	,	kIx,	,
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
projevů	projev	k1gInPc2	projev
a	a	k8xC	a
dopisů	dopis	k1gInPc2	dopis
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
odsuzovala	odsuzovat	k5eAaImAgFnS	odsuzovat
strach	strach	k1gInSc4	strach
a	a	k8xC	a
stigma	stigma	k1gNnSc4	stigma
spojené	spojený	k2eAgNnSc4d1	spojené
s	s	k7c7	s
homosexualitou	homosexualita	k1gFnSc7	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Hirschfeldovi	Hirschfeld	k1gMnSc3	Hirschfeld
psala	psát	k5eAaImAgFnS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
tragickou	tragický	k2eAgFnSc7d1	tragická
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
domnívám	domnívat	k5eAaImIp1nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
různé	různý	k2eAgFnSc2d1	různá
sexuality	sexualita	k1gFnSc2	sexualita
polapeni	polapit	k5eAaPmNgMnP	polapit
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
vůči	vůči	k7c3	vůči
homosexuálům	homosexuál	k1gMnPc3	homosexuál
projevuje	projevovat	k5eAaImIp3nS	projevovat
tak	tak	k9	tak
málo	málo	k6eAd1	málo
pochopení	pochopení	k1gNnSc4	pochopení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
bezohledně	bezohledně	k6eAd1	bezohledně
lhostejný	lhostejný	k2eAgInSc4d1	lhostejný
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
nuancím	nuance	k1gFnPc3	nuance
a	a	k8xC	a
varietám	varieta	k1gFnPc3	varieta
genderu	gender	k1gInSc2	gender
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
obrovsky	obrovsky	k6eAd1	obrovsky
důležitý	důležitý	k2eAgInSc1d1	důležitý
[	[	kIx(	[
<g/>
gender	gender	k1gInSc1	gender
<g/>
]	]	kIx)	]
pro	pro	k7c4	pro
život	život	k1gInSc4	život
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
Anarchism	Anarchism	k1gInSc1	Anarchism
and	and	k?	and
Other	Other	k1gInSc1	Other
Essays	Essays	k1gInSc1	Essays
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Mother	Mothra	k1gFnPc2	Mothra
Earth	Earth	k1gInSc1	Earth
Publishing	Publishing	k1gInSc1	Publishing
Association	Association	k1gInSc1	Association
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Social	Social	k1gInSc1	Social
Significance	Significance	k1gFnSc1	Significance
of	of	k?	of
the	the	k?	the
Modern	Modern	k1gInSc1	Modern
Drama	drama	k1gFnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
<g/>
:	:	kIx,	:
Gorham	Gorham	k1gInSc1	Gorham
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
My	my	k3xPp1nPc1	my
Disillusionment	Disillusionment	k1gMnSc1	Disillusionment
in	in	k?	in
Russia	Russia	k1gFnSc1	Russia
<g/>
.	.	kIx.	.
</s>
<s>
Garden	Gardna	k1gFnPc2	Gardna
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Doubleday	Doubledaa	k1gFnPc1	Doubledaa
<g/>
,	,	kIx,	,
Page	Page	k1gInSc1	Page
and	and	k?	and
Co	co	k3yQnSc4	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
My	my	k3xPp1nPc1	my
Further	Furthra	k1gFnPc2	Furthra
Disillusionment	Disillusionment	k1gMnSc1	Disillusionment
in	in	k?	in
Russia	Russia	k1gFnSc1	Russia
<g/>
.	.	kIx.	.
</s>
<s>
Garden	Gardna	k1gFnPc2	Gardna
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Doubleday	Doubledaa	k1gFnPc1	Doubledaa
<g/>
,	,	kIx,	,
Page	Page	k1gInSc1	Page
and	and	k?	and
Co	co	k3yQnSc4	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Living	Living	k1gInSc1	Living
My	my	k3xPp1nPc1	my
Life	Lif	k1gMnPc4	Lif
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Knopf	Knopf	k1gInSc1	Knopf
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Voltairine	Voltairin	k1gInSc5	Voltairin
de	de	k?	de
Cleyre	Cleyr	k1gInSc5	Cleyr
<g/>
.	.	kIx.	.
</s>
<s>
Berkeley	Berkelea	k1gFnPc1	Berkelea
Heights	Heightsa	k1gFnPc2	Heightsa
<g/>
,	,	kIx,	,
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
:	:	kIx,	:
Oriole	Oriole	k1gFnSc1	Oriole
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
===	===	k?	===
</s>
</p>
<p>
<s>
GOLDMAN	GOLDMAN	kA	GOLDMAN
<g/>
,	,	kIx,	,
Emma	Emma	k1gFnSc1	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
je	být	k5eAaImIp3nS	být
duší	duše	k1gFnSc7	duše
každého	každý	k3xTgInSc2	každý
pokroku	pokrok	k1gInSc2	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Tomek	Tomek	k1gMnSc1	Tomek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Manibus	Manibus	k1gInSc1	Manibus
propriis	propriis	k1gFnSc2	propriis
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
197	[number]	k4	197
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
5578	[number]	k4	5578
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Emma	Emma	k1gFnSc1	Emma
Goldman	Goldman	k1gMnSc1	Goldman
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FALK	FALK	kA	FALK
<g/>
,	,	kIx,	,
Candace	Candace	k1gFnSc1	Candace
<g/>
.	.	kIx.	.
</s>
<s>
Emma	Emma	k1gFnSc1	Emma
Goldman	Goldman	k1gMnSc1	Goldman
<g/>
:	:	kIx,	:
A	A	kA	A
documentary	documentara	k1gFnSc2	documentara
history	histor	k1gInPc4	histor
of	of	k?	of
the	the	k?	the
American	American	k1gInSc1	American
years	years	k1gInSc1	years
<g/>
.	.	kIx.	.
</s>
<s>
Urbana	Urban	k1gMnSc2	Urban
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Illinois	Illinois	k1gInSc1	Illinois
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Svazky	svazek	k1gInPc1	svazek
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
TOMEK	Tomek	k1gMnSc1	Tomek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
SLAČÁLEK	SLAČÁLEK	kA	SLAČÁLEK
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Anarchismus	anarchismus	k1gInSc1	anarchismus
:	:	kIx,	:
svoboda	svoboda	k1gFnSc1	svoboda
proti	proti	k7c3	proti
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
781	[number]	k4	781
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
s.	s.	k?	s.
381	[number]	k4	381
<g/>
–	–	k?	–
<g/>
404	[number]	k4	404
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Emma	Emma	k1gFnSc1	Emma
Goldman	Goldman	k1gMnSc1	Goldman
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Emma	Emma	k1gFnSc1	Emma
Goldmanová	Goldmanový	k2eAgFnSc1d1	Goldmanový
</s>
</p>
<p>
<s>
VÝROČÍ	výročí	k1gNnSc4	výročí
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
si	se	k3xPyFc3	se
připomeneme	připomenout	k5eAaPmIp1nP	připomenout
61	[number]	k4	61
let	léto	k1gNnPc2	léto
od	od	k7c2	od
úmrtí	úmrtí	k1gNnSc2	úmrtí
anarchistické	anarchistický	k2eAgFnSc2d1	anarchistická
myslitelky	myslitelka	k1gFnSc2	myslitelka
a	a	k8xC	a
revolucionářky	revolucionářka	k1gFnSc2	revolucionářka
Emmy	Emma	k1gFnSc2	Emma
Goldman	Goldman	k1gMnSc1	Goldman
–	–	k?	–
Feministická	feministický	k2eAgFnSc5d1	feministická
skupina	skupina	k1gFnSc1	skupina
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Emma	Emma	k1gFnSc1	Emma
Goldman	Goldman	k1gMnSc1	Goldman
–	–	k?	–
Anarchy	Anarcha	k1gFnSc2	Anarcha
Archives	Archivesa	k1gFnPc2	Archivesa
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
