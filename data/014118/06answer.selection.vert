<s>
Eduard	Eduard	k1gMnSc1
Deisenhofer	Deisenhofer	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1909	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
a	a	k8xC
velitel	velitel	k1gMnSc1
Waffen-SS	Waffen-SS	k1gMnSc1
v	v	k7c6
hodnosti	hodnost	k1gFnSc6
SS-Oberführer	SS-Oberführer	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
sloužil	sloužit	k5eAaImAgInS
s	s	k7c7
několika	několik	k4yIc7
divizemi	divize	k1gFnPc7
jak	jak	k8xS,k8xC
na	na	k7c6
východní	východní	k2eAgFnSc6d1
<g/>
,	,	kIx,
tak	tak	k9
na	na	k7c6
západní	západní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>