<s>
Eduard	Eduard	k1gMnSc1
Deisenhofer	Deisenhofer	k1gMnSc1
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
jur	jura	k1gFnPc2
Eduard	Eduard	k1gMnSc1
Deisenhofer	Deisenhofer	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
1909	#num#	k4
Freising	Freising	k1gInSc1
<g/>
,	,	kIx,
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
,	,	kIx,
1945	#num#	k4
Arnswalde	Arnswald	k1gInSc5
<g/>
,	,	kIx,
Třetí	třetí	k4xOgFnSc1
říše	říše	k1gFnSc1
Vojenská	vojenský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
Hodnost	hodnost	k1gFnSc1
</s>
<s>
SS-Oberführer	SS-Oberführer	k1gMnSc1
Doba	doba	k1gFnSc1
služby	služba	k1gFnSc2
</s>
<s>
1932	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
Sloužil	sloužit	k5eAaImAgInS
</s>
<s>
Výmarská	výmarský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1933	#num#	k4
<g/>
)	)	kIx)
Třetí	třetí	k4xOgFnSc1
říše	říše	k1gFnSc1
Složka	složka	k1gFnSc1
</s>
<s>
Waffen-SS	Waffen-SS	k?
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
Války	válka	k1gFnSc2
</s>
<s>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
Bitvy	bitva	k1gFnSc2
</s>
<s>
Invaze	invaze	k1gFnSc1
do	do	k7c2
PolskaBitva	PolskaBitvo	k1gNnSc2
o	o	k7c6
FranciiObležení	FranciiObležení	k1gNnSc6
LeningraduDěmjanský	LeningraduDěmjanský	k2eAgMnSc1d1
kotelBitva	kotelBitva	k6eAd1
u	u	k7c2
Kamence	Kamenec	k1gInSc2
PodolskéhoBitva	PodolskéhoBitvo	k1gNnSc2
o	o	k7c4
NormandiiVýchodní	NormandiiVýchodní	k2eAgNnSc4d1
fronta	fronta	k1gFnSc1
Vyznamenání	vyznamenání	k1gNnSc2
</s>
<s>
Rytířský	rytířský	k2eAgInSc1d1
kříž	kříž	k1gInSc1
železného	železný	k2eAgMnSc2d1
křížeNěmecký	křížeNěmecký	k2eAgInSc4d1
kříž	kříž	k1gInSc4
ve	v	k7c6
zlatě	zlato	k1gNnSc6
</s>
<s>
Eduard	Eduard	k1gMnSc1
Deisenhofer	Deisenhofer	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1909	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
a	a	k8xC
velitel	velitel	k1gMnSc1
Waffen-SS	Waffen-SS	k1gMnSc1
v	v	k7c6
hodnosti	hodnost	k1gFnSc6
SS-Oberführer	SS-Oberführer	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
sloužil	sloužit	k5eAaImAgInS
s	s	k7c7
několika	několik	k4yIc7
divizemi	divize	k1gFnPc7
jak	jak	k8xS,k8xC
na	na	k7c6
východní	východní	k2eAgFnSc6d1
<g/>
,	,	kIx,
tak	tak	k9
na	na	k7c6
západní	západní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgMnS
vzdělaným	vzdělaný	k2eAgMnSc7d1
důstojníkem	důstojník	k1gMnSc7
SS	SS	kA
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
zároveň	zároveň	k6eAd1
sloužil	sloužit	k5eAaImAgInS
v	v	k7c6
SD	SD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
byl	být	k5eAaImAgInS
držitelem	držitel	k1gMnSc7
mnoha	mnoho	k4c2
vojenských	vojenský	k2eAgNnPc2d1
vyznamenání	vyznamenání	k1gNnPc2
včetně	včetně	k7c2
Rytířského	rytířský	k2eAgInSc2d1
kříže	kříž	k1gInSc2
železného	železný	k2eAgInSc2d1
kříže	kříž	k1gInSc2
nebo	nebo	k8xC
Německého	německý	k2eAgInSc2d1
kříže	kříž	k1gInSc2
ve	v	k7c6
zlatě	zlato	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
a	a	k8xC
předválečná	předválečný	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
Deisenhofer	Deisenhofer	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
ve	v	k7c6
městě	město	k1gNnSc6
Freising	Freising	k1gInSc1
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
do	do	k7c2
rodiny	rodina	k1gFnSc2
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
na	na	k7c6
základní	základní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
exceloval	excelovat	k5eAaImAgInS
mezi	mezi	k7c7
ostatními	ostatní	k2eAgMnPc7d1
žáky	žák	k1gMnPc7
a	a	k8xC
proto	proto	k6eAd1
hned	hned	k6eAd1
po	po	k7c6
dokončení	dokončení	k1gNnSc6
začal	začít	k5eAaPmAgMnS
studovat	studovat	k5eAaImF
ekonomii	ekonomie	k1gFnSc4
a	a	k8xC
politické	politický	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
zajímal	zajímat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jeho	jeho	k3xOp3gNnPc2
studia	studio	k1gNnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
kontaktu	kontakt	k1gInSc2
s	s	k7c7
nacistickou	nacistický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
a	a	k8xC
brzy	brzy	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
požádal	požádat	k5eAaPmAgInS
o	o	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
jednotek	jednotka	k1gFnPc2
SA	SA	kA
Po	po	k7c6
několika	několik	k4yIc6
měsících	měsíc	k1gInPc6
se	se	k3xPyFc4
Deisenhofer	Deisenhofer	k1gInSc1
rozhodnl	rozhodnnout	k5eAaPmAgInS
přestoupit	přestoupit	k5eAaPmF
k	k	k7c3
jednotkám	jednotka	k1gFnPc3
SS	SS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc1
služba	služba	k1gFnSc1
zde	zde	k6eAd1
začala	začít	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SS-Brigadeführer	SS-Brigadeführer	k1gMnSc1
Gottfried	Gottfried	k1gMnSc1
Klingemann	Klingemann	k1gMnSc1
ho	on	k3xPp3gMnSc4
popsal	popsat	k5eAaPmAgMnS
jako	jako	k9
čistě	čistě	k6eAd1
myslícího	myslící	k2eAgNnSc2d1
a	a	k8xC
správného	správný	k2eAgMnSc2d1
muže	muž	k1gMnSc2
s	s	k7c7
energickým	energický	k2eAgInSc7d1
a	a	k8xC
houževnatým	houževnatý	k2eAgInSc7d1
charakterem	charakter	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
tak	tak	k9
SS-Brigadeführer	SS-Brigadeführer	k1gMnSc1
Heinz	Heinz	k1gMnSc1
Harmel	Harmel	k1gMnSc1
charakterizoval	charakterizovat	k5eAaBmAgMnS
Deisenhofera	Deisenhofer	k1gMnSc4
jako	jako	k9
pracovitého	pracovitý	k2eAgMnSc2d1
a	a	k8xC
velice	velice	k6eAd1
kvalifikovaného	kvalifikovaný	k2eAgMnSc4d1
vojáka	voják	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Deisenhofer	Deisenhofer	k1gInSc1
během	během	k7c2
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
zastával	zastávat	k5eAaImAgMnS
mnoho	mnoho	k4c4
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
služby	služba	k1gFnSc2
v	v	k7c6
pluku	pluk	k1gInSc6
Leibstandarte	Leibstandart	k1gInSc5
SS	SS	kA
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
slouží	sloužit	k5eAaImIp3nS
i	i	k9
v	v	k7c6
koncentračním	koncentrační	k2eAgInSc6d1
táboře	tábor	k1gInSc6
Dachau	Dachaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
také	také	k9
dosáhl	dosáhnout	k5eAaPmAgInS
hodnosti	hodnost	k1gFnSc2
SS-Hauptsturmführer	SS-Hauptsturmführer	k1gInSc1
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
roku	rok	k1gInSc2
1936	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Edeltraut	Edeltraut	k1gMnSc1
Holzapfel	Holzapfel	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
měl	mít	k5eAaImAgMnS
tři	tři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jedna	jeden	k4xCgFnSc1
byla	být	k5eAaImAgFnS
zabita	zabít	k5eAaPmNgFnS
během	během	k7c2
bombového	bombový	k2eAgInSc2d1
náletu	nálet	k1gInSc2
v	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
byl	být	k5eAaImAgInS
převelen	převelet	k5eAaPmNgInS
do	do	k7c2
SS-Wachtruppe	SS-Wachtrupp	k1gInSc5
Oberbayern	Oberbayerno	k1gNnPc2
a	a	k8xC
během	během	k7c2
následujících	následující	k2eAgNnPc2d1
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
slouží	sloužit	k5eAaImIp3nP
u	u	k7c2
jednotek	jednotka	k1gFnPc2
Totenkopfverbände	Totenkopfverbänd	k1gInSc5
SS	SS	kA
Verbande	Verband	k1gInSc5
Sachsen	Sachsno	k1gNnPc2
a	a	k8xC
SS	SS	kA
Standarte	Standart	k1gInSc5
Thuringen	Thuringen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Počátky	počátek	k1gInPc4
válečného	válečný	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
</s>
<s>
Během	během	k7c2
vypuknutí	vypuknutí	k1gNnSc2
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
převelen	převelet	k5eAaPmNgInS
zpět	zpět	k6eAd1
do	do	k7c2
SS-Wachtruppe	SS-Wachtrupp	k1gInSc5
Oberbayern	Oberbayerna	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
rozšířena	rozšířit	k5eAaPmNgFnS
na	na	k7c4
pluk	pluk	k1gInSc4
SS	SS	kA
Totenkopf	Totenkopf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SS-Wachtruppe	SS-Wachtrupp	k1gInSc5
Oberbayern	Oberbayern	k1gNnSc1
je	být	k5eAaImIp3nS
také	také	k9
spojována	spojovat	k5eAaImNgFnS
s	s	k7c7
policejními	policejní	k2eAgNnPc7d1
a	a	k8xC
ochrannými	ochranný	k2eAgNnPc7d1
opatřeními	opatření	k1gNnPc7
během	během	k7c2
Invaze	invaze	k1gFnSc2
do	do	k7c2
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
polského	polský	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
byla	být	k5eAaImAgFnS
SS-Wachtruppe	SS-Wachtrupp	k1gInSc5
Oberbayern	Oberbayern	k1gNnSc1
převelena	převelen	k2eAgFnSc1d1
do	do	k7c2
nově	nově	k6eAd1
zformované	zformovaný	k2eAgFnSc2d1
SS	SS	kA
Division	Division	k1gInSc1
Totenkopf	Totenkopf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deisenhofer	Deisenhofra	k1gFnPc2
<g/>
,	,	kIx,
už	už	k6eAd1
SS-Sturmabannführer	SS-Sturmabannführer	k1gMnSc1
(	(	kIx(
<g/>
Major	major	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
velitelem	velitel	k1gMnSc7
2	#num#	k4
<g/>
.	.	kIx.
praporu	prapor	k1gInSc2
SS-Totenkopf-Infanterie-Regiment	SS-Totenkopf-Infanterie-Regiment	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
jednotku	jednotka	k1gFnSc4
vedl	vést	k5eAaImAgMnS
i	i	k9
během	během	k7c2
tažení	tažení	k1gNnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
obdržel	obdržet	k5eAaPmAgInS
Železný	železný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
za	za	k7c4
statečnost	statečnost	k1gFnSc4
v	v	k7c6
boji	boj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
mu	on	k3xPp3gMnSc3
byly	být	k5eAaImAgInP
dány	dát	k5eAaPmNgInP
na	na	k7c4
starost	starost	k1gFnSc4
nově	nově	k6eAd1
zformované	zformovaný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
belgických	belgický	k2eAgMnPc2d1
a	a	k8xC
holandských	holandský	k2eAgMnPc2d1
dobrovolníků	dobrovolník	k1gMnPc2
SS-Freiwilligen-Standarte	SS-Freiwilligen-Standart	k1gInSc5
Nordwest	Nordwest	k1gInSc4
a	a	k8xC
poté	poté	k6eAd1
je	být	k5eAaImIp3nS
převelen	převelen	k2eAgInSc4d1
SS-Ersatz-Bataillon	SS-Ersatz-Bataillon	k1gInSc4
Ost	Ost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
působí	působit	k5eAaImIp3nS
jako	jako	k9
záložní	záložní	k2eAgInSc1d1
a	a	k8xC
výcvikový	výcvikový	k2eAgInSc1d1
prapor	prapor	k1gInSc1
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1941	#num#	k4
byl	být	k5eAaImAgInS
Deisenhofer	Deisenhofer	k1gInSc1
převelen	převelet	k5eAaPmNgInS
ke	k	k7c3
I.	I.	kA
<g/>
/	/	kIx~
<g/>
SS-Infanterie-Regiment	SS-Infanterie-Regiment	k1gInSc1
9	#num#	k4
Germania	germanium	k1gNnSc2
<g/>
,	,	kIx,
jednomu	jeden	k4xCgMnSc3
z	z	k7c2
pěchotních	pěchotní	k2eAgMnPc2d1
pluků	pluk	k1gInPc2
SS	SS	kA
Divize	divize	k1gFnSc1
Wiking	Wiking	k1gInSc1
právě	právě	k6eAd1
sloužící	sloužící	k2eAgMnSc1d1
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deisenhofer	Deisenhofer	k1gInSc1
velel	velet	k5eAaImAgInS
praporu	prapor	k1gInSc2
během	během	k7c2
zimy	zima	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
byl	být	k5eAaImAgInS
v	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
1942	#num#	k4
poslán	poslán	k2eAgInSc4d1
zpět	zpět	k6eAd1
k	k	k7c3
divizi	divize	k1gFnSc3
Totenkopf	Totenkopf	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Divize	divize	k1gFnSc1
Totenkopf	Totenkopf	k1gInSc1
byla	být	k5eAaImAgFnS
připojena	připojit	k5eAaPmNgFnS
ke	k	k7c3
skupině	skupina	k1gFnSc3
armád	armáda	k1gFnPc2
Sever	sever	k1gInSc1
postupujícím	postupující	k2eAgMnSc7d1
na	na	k7c4
Leningrad	Leningrad	k1gInSc4
pod	pod	k7c7
velením	velení	k1gNnSc7
polního	polní	k2eAgMnSc2d1
maršála	maršál	k1gMnSc2
Wilhelma	Wilhelma	k1gFnSc1
von	von	k1gInSc1
Leeb	Leeb	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divize	divize	k1gFnSc1
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
bojů	boj	k1gInPc2
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Demjansk	Demjansk	k1gInSc1
a	a	k8xC
během	během	k7c2
bitvy	bitva	k1gFnSc2
byla	být	k5eAaImAgFnS
zcela	zcela	k6eAd1
obklíčena	obklíčit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
výpovědi	výpověď	k1gFnSc2
očitých	očitý	k2eAgMnPc2d1
svědků	svědek	k1gMnPc2
však	však	k9
Deisenhofer	Deisenhofer	k1gMnSc1
vedl	vést	k5eAaImAgMnS
své	svůj	k3xOyFgInPc4
může	moct	k5eAaImIp3nS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
počínal	počínat	k5eAaImAgMnS
si	se	k3xPyFc3
hrdinsky	hrdinsky	k6eAd1
a	a	k8xC
prokazoval	prokazovat	k5eAaImAgMnS
velkou	velký	k2eAgFnSc4d1
odvahu	odvaha	k1gFnSc4
pod	pod	k7c7
palbou	palba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
silně	silně	k6eAd1
oslabený	oslabený	k2eAgInSc4d1
prapor	prapor	k1gInSc4
začleněn	začleněn	k2eAgInSc4d1
do	do	k7c2
větší	veliký	k2eAgFnSc2d2
bojové	bojový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
(	(	kIx(
<g/>
Kampfgruppe	Kampfgrupp	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Deisenhoferovi	Deisenhofer	k1gMnSc3
předáno	předán	k2eAgNnSc1d1
velení	velení	k1gNnSc1
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
úspěšně	úspěšně	k6eAd1
prorazil	prorazit	k5eAaPmAgMnS
z	z	k7c2
obklíčení	obklíčení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
roku	rok	k1gInSc2
1942	#num#	k4
byl	být	k5eAaImAgInS
povýšen	povýšit	k5eAaPmNgMnS
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
SS-Obersturmbannführer	SS-Obersturmbannführer	k1gMnSc1
(	(	kIx(
<g/>
podplukovník	podplukovník	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
vyznamenán	vyznamenat	k5eAaPmNgInS
rytířským	rytířský	k2eAgInSc7d1
křížem	kříž	k1gInSc7
za	za	k7c4
akce	akce	k1gFnPc4
v	v	k7c6
Demjanské	Demjanský	k2eAgFnSc6d1
kapse	kapsa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytky	zbytek	k1gInPc7
vysílené	vysílený	k2eAgFnSc2d1
a	a	k8xC
oslabené	oslabený	k2eAgFnSc2d1
divize	divize	k1gFnSc2
Totenkopf	Totenkopf	k1gInSc1
byly	být	k5eAaImAgFnP
staženy	stáhnout	k5eAaPmNgInP
do	do	k7c2
Francie	Francie	k1gFnSc2
k	k	k7c3
doplnění	doplnění	k1gNnSc3
stavů	stav	k1gInPc2
a	a	k8xC
Deisenhofer	Deisenhofer	k1gInSc1
odvelen	odvelen	k2eAgInSc1d1
zpět	zpět	k6eAd1
do	do	k7c2
Berlína	Berlín	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
udělena	udělen	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Povinnosti	povinnost	k1gFnPc1
ve	v	k7c6
výcviku	výcvik	k1gInSc6
</s>
<s>
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgMnS
Deisenhofer	Deisenhofer	k1gMnSc1
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
velitelem	velitel	k1gMnSc7
záložního	záložní	k2eAgInSc2d1
SS	SS	kA
motopraporu	motoprapor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vedl	vést	k5eAaImAgInS
od	od	k7c2
chvíle	chvíle	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
převelen	převelet	k5eAaPmNgInS
do	do	k7c2
SS-Junkerschule	SS-Junkerschule	k1gFnSc2
v	v	k7c6
Bad	Bad	k1gFnSc6
Tölz	Tölza	k1gFnPc2
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Zde	zde	k6eAd1
byl	být	k5eAaImAgInS
dosazen	dosadit	k5eAaPmNgInS
do	do	k7c2
funkce	funkce	k1gFnSc2
velitele	velitel	k1gMnSc2
výcvikové	výcvikový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
a	a	k8xC
musel	muset	k5eAaImAgMnS
přejíždět	přejíždět	k5eAaImF
mezi	mezi	k7c7
několika	několik	k4yIc7
odlišnými	odlišný	k2eAgInPc7d1
výcvikovými	výcvikový	k2eAgNnPc7d1
stanovišti	stanoviště	k1gNnPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
výcvikového	výcvikový	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
pro	pro	k7c4
obrněné	obrněný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
výcvikové	výcvikový	k2eAgFnPc4d1
školy	škola	k1gFnPc4
pro	pro	k7c4
lehkou	lehký	k2eAgFnSc4d1
pěchotu	pěchota	k1gFnSc4
a	a	k8xC
výcvikového	výcvikový	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
pancéřových	pancéřový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
ve	v	k7c6
městě	město	k1gNnSc6
Wünsdorf	Wünsdorf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
dubna	duben	k1gInSc2
roku	rok	k1gInSc2
1943	#num#	k4
byl	být	k5eAaImAgInS
převelen	převelet	k5eAaPmNgInS
opět	opět	k6eAd1
zpět	zpět	k6eAd1
do	do	k7c2
Berlína	Berlín	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
jako	jako	k8xC,k8xS
výcvikový	výcvikový	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
pro	pro	k7c4
pěchotu	pěchota	k1gFnSc4
SS	SS	kA
a	a	k8xC
horské	horský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
v	v	k7c6
oddělení	oddělení	k1gNnSc6
inspekce	inspekce	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
částí	část	k1gFnSc7
SS	SS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
roku	rok	k1gInSc2
1943	#num#	k4
byl	být	k5eAaImAgMnS
poslán	poslat	k5eAaPmNgMnS
k	k	k7c3
11	#num#	k4
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
jako	jako	k8xS,k8xC
výcvikový	výcvikový	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Divizní	divizní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
března	březen	k1gInSc2
roku	rok	k1gInSc2
1944	#num#	k4
byl	být	k5eAaImAgInS
Deisenhofer	Deisenhofer	k1gInSc1
osvobozen	osvobodit	k5eAaPmNgInS
od	od	k7c2
povinností	povinnost	k1gFnPc2
ve	v	k7c6
výcvikové	výcvikový	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Bad	Bad	k1gFnSc6
Tölzu	Tölz	k1gInSc2
a	a	k8xC
poslán	poslán	k2eAgMnSc1d1
zpět	zpět	k6eAd1
do	do	k7c2
bojové	bojový	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
ke	k	k7c3
SS-Panzergrenadier-Regiment	SS-Panzergrenadier-Regiment	k1gInSc4
21	#num#	k4
ze	z	k7c2
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SS	SS	kA
Panzer	Panzer	k1gInSc1
Division	Division	k1gInSc1
„	„	k?
<g/>
Frundsberg	Frundsberg	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Deisenhofer	Deisenhofer	k1gInSc1
velel	velet	k5eAaImAgInS
pluku	pluk	k1gInSc3
SS-Panzergrenadier-Regiment	SS-Panzergrenadier-Regiment	k1gInSc1
21	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
úspěšném	úspěšný	k2eAgInSc6d1
pokusu	pokus	k1gInSc6
divize	divize	k1gFnSc1
Frundsberg	Frundsberg	k1gInSc1
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SS-Panzer	SS-Panzer	k1gInSc1
Division	Division	k1gInSc4
„	„	k?
<g/>
Hohenstaufen	Hohenstaufen	k1gInSc1
<g/>
“	“	k?
o	o	k7c6
proražení	proražení	k1gNnSc6
obklíčené	obklíčený	k2eAgFnSc2d1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panzerarmee	Panzerarmee	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
velel	velet	k5eAaImAgMnS
generálplukovník	generálplukovník	k1gMnSc1
Hans-Valentin	Hans-Valentin	k1gMnSc1
Hube	Hube	k1gFnSc4
v	v	k7c6
kapse	kapsa	k1gFnSc6
u	u	k7c2
Kamence	Kamenec	k1gInSc2
Podolského	podolský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
byl	být	k5eAaImAgInS
povýšen	povýšit	k5eAaPmNgMnS
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
SS-Standartenführer	SS-Standartenführer	k1gMnSc1
(	(	kIx(
<g/>
plukovník	plukovník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Divize	divize	k1gFnSc1
Frundsberg	Frundsberg	k1gInSc1
se	se	k3xPyFc4
však	však	k9
měla	mít	k5eAaImAgFnS
zapojit	zapojit	k5eAaPmF
do	do	k7c2
bitvy	bitva	k1gFnSc2
o	o	k7c4
Normandii	Normandie	k1gFnSc4
a	a	k8xC
pokusit	pokusit	k5eAaPmF
se	se	k3xPyFc4
zastavit	zastavit	k5eAaPmF
postup	postup	k1gInSc1
21	#num#	k4
<g/>
.	.	kIx.
skupiny	skupina	k1gFnSc2
armád	armáda	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
právě	právě	k6eAd1
postupovala	postupovat	k5eAaImAgFnS
pod	pod	k7c7
velením	velení	k1gNnSc7
britského	britský	k2eAgMnSc2d1
polního	polní	k2eAgMnSc2d1
maršála	maršál	k1gMnSc2
Bernarda	Bernard	k1gMnSc2
Law	Law	k1gMnSc2
Montgomeryho	Montgomery	k1gMnSc2
na	na	k7c4
město	město	k1gNnSc4
Caen	Caena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deisenhofer	Deisenhofra	k1gFnPc2
poté	poté	k6eAd1
velel	velet	k5eAaImAgMnS
svému	svůj	k3xOyFgInSc3
pluku	pluk	k1gInSc3
v	v	k7c6
těžkých	těžký	k2eAgInPc6d1
bojích	boj	k1gInPc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
bitvy	bitva	k1gFnSc2
u	u	k7c2
kóty	kóta	k1gFnSc2
112	#num#	k4
a	a	k8xC
i	i	k9
během	během	k7c2
operace	operace	k1gFnSc2
Epsom	Epsom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zhruba	zhruba	k6eAd1
v	v	k7c6
polovině	polovina	k1gFnSc6
července	červenec	k1gInSc2
byl	být	k5eAaImAgInS
Deisenhofer	Deisenhofer	k1gInSc1
odvelen	odvelet	k5eAaPmNgInS
zpět	zpět	k6eAd1
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
opět	opět	k6eAd1
převzal	převzít	k5eAaPmAgInS
velení	velení	k1gNnSc4
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SS-Panzer	SS-Panzer	k1gInSc1
Division	Division	k1gInSc4
„	„	k?
<g/>
Wiking	Wiking	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zrovna	zrovna	k6eAd1
bojuje	bojovat	k5eAaImIp3nS
poblíž	poblíž	k7c2
vesnice	vesnice	k1gFnSc2
Modlin	Modlina	k1gFnPc2
nedaleko	nedaleko	k7c2
Varšavy	Varšava	k1gFnSc2
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prostředku	prostředek	k1gInSc6
srpna	srpen	k1gInSc2
byl	být	k5eAaImAgInS
Deisenhofer	Deisenhofer	k1gInSc1
odvelen	odvelet	k5eAaPmNgInS
opět	opět	k6eAd1
zpět	zpět	k6eAd1
do	do	k7c2
Berlína	Berlín	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
čekal	čekat	k5eAaImAgMnS
<g/>
,	,	kIx,
až	až	k8xS
mu	on	k3xPp3gMnSc3
bude	být	k5eAaImBp3nS
přidělena	přidělen	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
měsíce	měsíc	k1gInSc2
převzal	převzít	k5eAaPmAgInS
velení	velení	k1gNnSc4
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SS-Panzergrenadier	SS-Panzergrenadier	k1gInSc1
Division	Division	k1gInSc4
„	„	k?
<g/>
Götz	Götz	k1gInSc1
von	von	k1gInSc1
Berlichingen	Berlichingen	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
těžce	těžce	k6eAd1
bojovala	bojovat	k5eAaImAgFnS
v	v	k7c6
Sársku	Sársko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deisenhofer	Deisenhofer	k1gInSc1
odrazil	odrazit	k5eAaPmAgInS
útoky	útok	k1gInPc4
amerických	americký	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
na	na	k7c6
řece	řeka	k1gFnSc6
Mosela	Mosel	k1gMnSc2
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
stáhl	stáhnout	k5eAaPmAgMnS
k	k	k7c3
Metám	Mety	k1gFnPc3
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
září	září	k1gNnSc2
byl	být	k5eAaImAgInS
Deisenhofer	Deisenhofer	k1gInSc1
zraněn	zranit	k5eAaPmNgInS
v	v	k7c6
boji	boj	k1gInSc6
a	a	k8xC
odvelen	odvelet	k5eAaPmNgMnS
do	do	k7c2
Berlína	Berlín	k1gInSc2
k	k	k7c3
odpočinku	odpočinek	k1gInSc3
a	a	k8xC
zotavení	zotavení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
1944	#num#	k4
byl	být	k5eAaImAgInS
pověřen	pověřit	k5eAaPmNgInS
sestavením	sestavení	k1gNnSc7
bojové	bojový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
(	(	kIx(
<g/>
Kampfgruppe	Kampfgrupp	k1gInSc5
<g/>
)	)	kIx)
z	z	k7c2
branců	branec	k1gMnPc2
SS-Truppenubungsplatz	SS-Truppenubungsplatza	k1gFnPc2
k	k	k7c3
obraně	obrana	k1gFnSc3
Bad	Bad	k1gFnSc2
Saarow	Saarow	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
povýšen	povýšit	k5eAaPmNgInS
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
SS-Oberführer	SS-Oberführer	k1gMnSc1
(	(	kIx(
<g/>
starší	starší	k1gMnSc1
plukovník	plukovník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ke	k	k7c3
konci	konec	k1gInSc3
ledna	leden	k1gInSc2
roku	rok	k1gInSc2
1945	#num#	k4
byl	být	k5eAaImAgInS
Deisenhofer	Deisenhofer	k1gInSc1
odvelen	odvelet	k5eAaPmNgInS
do	do	k7c2
Pomořanska	Pomořansko	k1gNnSc2
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Arnswalde	Arnswald	k1gInSc5
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
převzal	převzít	k5eAaPmAgInS
velení	velení	k1gNnSc4
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Waffen-Grenadier	Waffen-Grenadier	k1gInSc1
Division	Division	k1gInSc4
der	drát	k5eAaImRp2nS
SS	SS	kA
(	(	kIx(
<g/>
lettische	lettisch	k1gMnSc2
Nr	Nr	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
nahradil	nahradit	k5eAaPmAgMnS
předchozího	předchozí	k2eAgMnSc4d1
velitele	velitel	k1gMnSc4
SS-Brigadeführera	SS-Brigadeführer	k1gMnSc2
Herberta	Herbert	k1gMnSc2
von	von	k1gInSc1
Obwurzer	Obwurzer	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
zajat	zajmout	k5eAaPmNgInS
v	v	k7c6
boji	boj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
k	k	k7c3
divizi	divize	k1gFnSc3
již	již	k6eAd1
nedorazil	dorazit	k5eNaPmAgMnS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc4
štábní	štábní	k2eAgNnSc1d1
vozidlo	vozidlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
zasaženo	zasáhnout	k5eAaPmNgNnS
dělostřeleckým	dělostřelecký	k2eAgInSc7d1
útokem	útok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
Deisenhofer	Deisenhofer	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
pobočník	pobočník	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
řidič	řidič	k1gMnSc1
byli	být	k5eAaImAgMnP
zabiti	zabít	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
Shrnutí	shrnutí	k1gNnSc1
vojenské	vojenský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
</s>
<s>
Data	datum	k1gNnPc1
povýšení	povýšení	k1gNnSc2
</s>
<s>
SS-Scharführer	SS-Scharführer	k1gInSc1
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
,	,	kIx,
1932	#num#	k4
</s>
<s>
SS-Truppführer	SS-Truppführer	k1gInSc1
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
,	,	kIx,
1933	#num#	k4
</s>
<s>
SS-Obertruppführer	SS-Obertruppführer	k1gInSc1
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
,	,	kIx,
1933	#num#	k4
</s>
<s>
SS-Sturmführer	SS-Sturmführer	k1gInSc1
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
1933	#num#	k4
</s>
<s>
SS-Obersturmführer	SS-Obersturmführer	k1gInSc1
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
1934	#num#	k4
</s>
<s>
SS-Hauptsturmführer	SS-Hauptsturmführer	k1gInSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
<g/>
,	,	kIx,
1935	#num#	k4
</s>
<s>
SS-Sturmbannführer	SS-Sturmbannführer	k1gInSc1
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
1938	#num#	k4
</s>
<s>
SS-Obersturmbannführer	SS-Obersturmbannführer	k1gInSc1
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
1942	#num#	k4
</s>
<s>
SS-Standartenführer	SS-Standartenführer	k1gInSc1
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
1944	#num#	k4
</s>
<s>
SS-Oberführer	SS-Oberführer	k1gInSc1
—	—	k?
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
,	,	kIx,
1945	#num#	k4
</s>
<s>
Významná	významný	k2eAgNnPc1d1
vyznamenání	vyznamenání	k1gNnPc1
</s>
<s>
Rytířský	rytířský	k2eAgInSc1d1
kříž	kříž	k1gInSc1
železného	železný	k2eAgInSc2d1
kříže	kříž	k1gInSc2
(	(	kIx(
<g/>
988	#num#	k4
<g/>
.	.	kIx.
držitel	držitel	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
květen	květena	k1gFnPc2
<g/>
,	,	kIx,
1942	#num#	k4
</s>
<s>
Německý	německý	k2eAgInSc1d1
kříž	kříž	k1gInSc1
ve	v	k7c6
zlatě	zlato	k1gNnSc6
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
<g/>
,	,	kIx,
1942	#num#	k4
</s>
<s>
Železný	železný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
I.	I.	kA
třídy	třída	k1gFnSc2
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
,	,	kIx,
1940	#num#	k4
</s>
<s>
Železný	železný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
,	,	kIx,
1940	#num#	k4
</s>
<s>
Spona	spona	k1gFnSc1
za	za	k7c4
boj	boj	k1gInSc4
zblízka	zblízka	k6eAd1
v	v	k7c6
bronzu	bronz	k1gInSc6
</s>
<s>
Finský	finský	k2eAgInSc1d1
řád	řád	k1gInSc1
kříže	kříž	k1gInSc2
svobody	svoboda	k1gFnSc2
III	III	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
s	s	k7c7
meči	meč	k1gInPc7
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
<g/>
,	,	kIx,
1941	#num#	k4
</s>
<s>
Medaile	medaile	k1gFnSc1
za	za	k7c4
východní	východní	k2eAgFnSc4d1
frontu	fronta	k1gFnSc4
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
<g/>
,	,	kIx,
1942	#num#	k4
</s>
<s>
Útočný	útočný	k2eAgInSc1d1
odznak	odznak	k1gInSc1
pěchoty	pěchota	k1gFnSc2
ve	v	k7c6
stříbře	stříbro	k1gNnSc6
</s>
<s>
Odznak	odznak	k1gInSc1
za	za	k7c4
zranění	zranění	k1gNnSc4
ve	v	k7c6
stříbře	stříbro	k1gNnSc6
</s>
<s>
Odznak	odznak	k1gInSc1
za	za	k7c4
zranění	zranění	k1gNnPc4
v	v	k7c6
černém	černý	k2eAgInSc6d1
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
,	,	kIx,
1940	#num#	k4
</s>
<s>
Medaile	medaile	k1gFnSc1
za	za	k7c4
Anschluss	Anschluss	k1gInSc4
</s>
<s>
Sudetská	sudetský	k2eAgFnSc1d1
pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Děmjanský	Děmjanský	k2eAgInSc1d1
štít	štít	k1gInSc1
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
odznak	odznak	k1gInSc1
ve	v	k7c6
stříbře	stříbro	k1gNnSc6
</s>
<s>
Totenkopfring	Totenkopfring	k1gInSc1
</s>
<s>
Čestná	čestný	k2eAgFnSc1d1
dýka	dýka	k1gFnSc1
Reichsführera-SS	Reichsführera-SS	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Axis	Axis	k1gInSc1
History	Histor	k1gInPc7
Factbook	Factbook	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Feldgrau	Feldgraum	k1gNnSc3
-	-	kIx~
Historie	historie	k1gFnSc1
německých	německý	k2eAgInPc2d1
obrněných	obrněný	k2eAgInPc2d1
vojsk	vojsko	k1gNnPc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
125102291	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
1851	#num#	k4
2515	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
25554657	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
