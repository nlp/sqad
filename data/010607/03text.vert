<p>
<s>
Ekonomie	ekonomie	k1gFnSc1	ekonomie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
popisem	popis	k1gInSc7	popis
a	a	k8xC	a
analýzou	analýza	k1gFnSc7	analýza
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
distribuce	distribuce	k1gFnSc2	distribuce
a	a	k8xC	a
spotřeby	spotřeba	k1gFnSc2	spotřeba
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
statků	statek	k1gInPc2	statek
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
zboží	zboží	k1gNnSc1	zboží
<g/>
,	,	kIx,	,
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
peněz	peníze	k1gInPc2	peníze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgInPc1d1	omezený
zdroje	zdroj	k1gInPc1	zdroj
alokovány	alokovat	k5eAaImNgInP	alokovat
mezi	mezi	k7c4	mezi
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
alternativní	alternativní	k2eAgNnPc4d1	alternativní
využití	využití	k1gNnPc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
směry	směr	k1gInPc4	směr
(	(	kIx(	(
<g/>
školy	škola	k1gFnPc4	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
odlišné	odlišný	k2eAgFnPc1d1	odlišná
definice	definice	k1gFnPc1	definice
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekonomie	ekonomie	k1gFnSc1	ekonomie
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
účetnictví	účetnictví	k1gNnSc1	účetnictví
<g/>
,	,	kIx,	,
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
ekonometrie	ekonometrie	k1gFnSc1	ekonometrie
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
,	,	kIx,	,
politologie	politologie	k1gFnSc1	politologie
<g/>
,	,	kIx,	,
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
geografie	geografie	k1gFnSc1	geografie
<g/>
,	,	kIx,	,
ekologie	ekologie	k1gFnSc1	ekologie
či	či	k8xC	či
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
ekonomie	ekonomie	k1gFnSc1	ekonomie
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
výrazu	výraz	k1gInSc2	výraz
oikonomia	oikonomia	k1gFnSc1	oikonomia
<g/>
:	:	kIx,	:
oikos	oikos	k1gInSc1	oikos
–	–	k?	–
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
nomos	nomos	k1gInSc1	nomos
–	–	k?	–
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
původně	původně	k6eAd1	původně
znamenal	znamenat	k5eAaImAgInS	znamenat
"	"	kIx"	"
<g/>
vedení	vedení	k1gNnSc3	vedení
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
definice	definice	k1gFnSc1	definice
podstaty	podstata	k1gFnSc2	podstata
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
zvaného	zvaný	k2eAgInSc2d1	zvaný
všeobecně	všeobecně	k6eAd1	všeobecně
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
definic	definice	k1gFnPc2	definice
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
jsou	být	k5eAaImIp3nP	být
odrazem	odraz	k1gInSc7	odraz
vývoje	vývoj	k1gInSc2	vývoj
pohledu	pohled	k1gInSc2	pohled
vědců	vědec	k1gMnPc2	vědec
či	či	k8xC	či
jejich	jejich	k3xOp3gInPc2	jejich
odlišných	odlišný	k2eAgInPc2d1	odlišný
přístupů	přístup	k1gInPc2	přístup
k	k	k7c3	k
podstatě	podstata	k1gFnSc3	podstata
zkoumaných	zkoumaný	k2eAgInPc2d1	zkoumaný
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
<g/>
Skotský	skotský	k2eAgMnSc1d1	skotský
filozof	filozof	k1gMnSc1	filozof
Adam	Adam	k1gMnSc1	Adam
Smith	Smith	k1gMnSc1	Smith
představil	představit	k5eAaPmAgMnS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
definici	definice	k1gFnSc4	definice
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
označen	označit	k5eAaPmNgInS	označit
jako	jako	k8xC	jako
politická	politický	k2eAgFnSc1d1	politická
ekonomie	ekonomie	k1gFnSc1	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
Pojednání	pojednání	k1gNnSc6	pojednání
o	o	k7c6	o
podstatě	podstata	k1gFnSc6	podstata
a	a	k8xC	a
původu	původ	k1gInSc2	původ
bohatství	bohatství	k1gNnSc2	bohatství
národů	národ	k1gInPc2	národ
ekonomii	ekonomie	k1gFnSc3	ekonomie
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Say	Say	k1gMnSc1	Say
definoval	definovat	k5eAaBmAgInS	definovat
ekonomii	ekonomie	k1gFnSc4	ekonomie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
při	při	k7c6	při
rozlišení	rozlišení	k1gNnSc6	rozlišení
tohoto	tento	k3xDgInSc2	tento
pojmu	pojem	k1gInSc2	pojem
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
veřejně	veřejně	k6eAd1	veřejně
politického	politický	k2eAgNnSc2d1	politické
použití	použití	k1gNnSc2	použití
jako	jako	k8xC	jako
vědu	věda	k1gFnSc4	věda
o	o	k7c6	o
produkci	produkce	k1gFnSc6	produkce
<g/>
,	,	kIx,	,
distribuci	distribuce	k1gFnSc6	distribuce
a	a	k8xC	a
spotřebě	spotřeba	k1gFnSc6	spotřeba
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Carlyle	Carlyl	k1gInSc5	Carlyl
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
razil	razit	k5eAaImAgInS	razit
satirický	satirický	k2eAgInSc1d1	satirický
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
ponuré	ponurý	k2eAgFnSc2d1	ponurá
vědy	věda	k1gFnSc2	věda
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
epiteton	epiteton	k1gNnSc4	epiteton
ke	k	k7c3	k
klasické	klasický	k2eAgFnSc3d1	klasická
ekonomii	ekonomie	k1gFnSc3	ekonomie
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
běžně	běžně	k6eAd1	běžně
odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
na	na	k7c4	na
pesimistické	pesimistický	k2eAgFnPc4d1	pesimistická
analýzy	analýza	k1gFnPc4	analýza
Thomase	Thomas	k1gMnSc2	Thomas
Roberta	Robert	k1gMnSc2	Robert
Malthuse	Malthuse	k1gFnSc2	Malthuse
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Stuart	Stuarta	k1gFnPc2	Stuarta
Mill	Mill	k1gMnSc1	Mill
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
definoval	definovat	k5eAaBmAgInS	definovat
ekonomii	ekonomie	k1gFnSc4	ekonomie
v	v	k7c6	v
sociálním	sociální	k2eAgInSc6d1	sociální
kontextu	kontext	k1gInSc6	kontext
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Marshall	Marshall	k1gMnSc1	Marshall
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
The	The	k1gMnSc1	The
Principles	Principles	k1gMnSc1	Principles
of	of	k?	of
Economics	Economics	k1gInSc1	Economics
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
definici	definice	k1gFnSc4	definice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
analýzu	analýza	k1gFnSc4	analýza
za	za	k7c4	za
rámec	rámec	k1gInSc4	rámec
zkoumání	zkoumání	k1gNnSc2	zkoumání
vzniku	vznik	k1gInSc2	vznik
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
přesunula	přesunout	k5eAaPmAgFnS	přesunout
ji	on	k3xPp3gFnSc4	on
ze	z	k7c2	z
společenské	společenský	k2eAgFnSc2d1	společenská
úrovně	úroveň	k1gFnSc2	úroveň
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
mikroekonomickou	mikroekonomický	k2eAgFnSc4d1	mikroekonomická
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lionel	Lionel	k1gInSc1	Lionel
Robbins	Robbins	k1gInSc1	Robbins
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
poučku	poučka	k1gFnSc4	poučka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
nazvána	nazvat	k5eAaPmNgFnS	nazvat
"	"	kIx"	"
<g/>
možná	možná	k9	možná
nejvíce	hodně	k6eAd3	hodně
přijímanou	přijímaný	k2eAgFnSc7d1	přijímaná
současnou	současný	k2eAgFnSc7d1	současná
definicí	definice	k1gFnSc7	definice
(	(	kIx(	(
<g/>
tohoto	tento	k3xDgInSc2	tento
<g/>
)	)	kIx)	)
subjektu	subjekt	k1gInSc2	subjekt
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Robbins	Robbins	k6eAd1	Robbins
popsal	popsat	k5eAaPmAgMnS	popsat
svou	svůj	k3xOyFgFnSc4	svůj
definici	definice	k1gFnSc4	definice
ekonomie	ekonomie	k1gFnSc2	ekonomie
jako	jako	k8xC	jako
neklasifikační	klasifikační	k2eNgFnSc3d1	klasifikační
při	pře	k1gFnSc3	pře
"	"	kIx"	"
<g/>
vyzvednutí	vyzvednutí	k1gNnSc2	vyzvednutí
určitých	určitý	k2eAgInPc2d1	určitý
druhů	druh	k1gInPc2	druh
chování	chování	k1gNnSc2	chování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
více	hodně	k6eAd2	hodně
analytickou	analytický	k2eAgFnSc7d1	analytická
v	v	k7c6	v
"	"	kIx"	"
<g/>
soustředění	soustředění	k1gNnSc6	soustředění
pozornosti	pozornost	k1gFnSc2	pozornost
na	na	k7c6	na
zejména	zejména	k9	zejména
ty	ten	k3xDgFnPc4	ten
vlastnosti	vlastnost	k1gFnPc4	vlastnost
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
formované	formovaný	k2eAgInPc1d1	formovaný
nedostatkem	nedostatek	k1gInSc7	nedostatek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Robbins	Robbins	k1gInSc4	Robbins
dále	daleko	k6eAd2	daleko
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
předchozí	předchozí	k2eAgMnPc1d1	předchozí
ekonomové	ekonom	k1gMnPc1	ekonom
se	se	k3xPyFc4	se
při	při	k7c6	při
svých	svůj	k3xOyFgNnPc6	svůj
pozorováních	pozorování	k1gNnPc6	pozorování
většinou	většinou	k6eAd1	většinou
soustřeďovali	soustřeďovat	k5eAaImAgMnP	soustřeďovat
na	na	k7c4	na
analýzu	analýza	k1gFnSc4	analýza
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
:	:	kIx,	:
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
bohatství	bohatství	k1gNnSc1	bohatství
vytvářeno	vytvářen	k2eAgNnSc1d1	vytvářeno
(	(	kIx(	(
<g/>
produkce	produkce	k1gFnSc1	produkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
distribuováno	distribuován	k2eAgNnSc1d1	distribuováno
a	a	k8xC	a
spotřebováváno	spotřebováván	k2eAgNnSc1d1	spotřebováváno
<g/>
,	,	kIx,	,
a	a	k8xC	a
jak	jak	k8xS	jak
bohatství	bohatství	k1gNnSc4	bohatství
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
věda	věda	k1gFnSc1	věda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc1	nástroj
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
událostí	událost	k1gFnPc2	událost
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
se	se	k3xPyFc4	se
ekonomická	ekonomický	k2eAgNnPc1d1	ekonomické
bádání	bádání	k1gNnPc1	bádání
obvykle	obvykle	k6eAd1	obvykle
nedotýkají	dotýkat	k5eNaImIp3nP	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zvítězit	zvítězit	k5eAaPmF	zvítězit
(	(	kIx(	(
<g/>
což	což	k3yQnSc4	což
chtějí	chtít	k5eAaImIp3nP	chtít
všichni	všechen	k3xTgMnPc1	všechen
její	její	k3xOp3gMnPc1	její
účastníci	účastník	k1gMnPc1	účastník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
však	však	k9	však
současně	současně	k6eAd1	současně
náklady	náklad	k1gInPc1	náklad
(	(	kIx(	(
<g/>
a	a	k8xC	a
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnSc4	vítěz
i	i	k8xC	i
přínosy	přínos	k1gInPc4	přínos
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
využití	využití	k1gNnSc1	využití
omezených	omezený	k2eAgInPc2d1	omezený
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
)	)	kIx)	)
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
onoho	onen	k3xDgInSc2	onen
vítězného	vítězný	k2eAgInSc2d1	vítězný
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
válku	válka	k1gFnSc4	válka
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
očekávány	očekávat	k5eAaImNgInP	očekávat
vyšší	vysoký	k2eAgInPc1d2	vyšší
náklady	náklad	k1gInPc1	náklad
než	než	k8xS	než
přínosy	přínos	k1gInPc1	přínos
<g/>
,	,	kIx,	,
rozhodující	rozhodující	k2eAgInPc1d1	rozhodující
činitelé	činitel	k1gInPc1	činitel
(	(	kIx(	(
<g/>
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
racionální	racionální	k2eAgNnSc4d1	racionální
<g/>
)	)	kIx)	)
nesmí	smět	k5eNaImIp3nS	smět
válku	válka	k1gFnSc4	válka
vyvolat	vyvolat	k5eAaPmF	vyvolat
(	(	kIx(	(
<g/>
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
raději	rád	k6eAd2	rád
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
jiné	jiný	k2eAgFnPc4d1	jiná
alternativy	alternativa	k1gFnPc4	alternativa
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
strategických	strategický	k2eAgInPc2d1	strategický
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Robbinse	Robbinse	k1gFnSc2	Robbinse
bychom	by	kYmCp1nP	by
neměli	mít	k5eNaImAgMnP	mít
definovat	definovat	k5eAaBmF	definovat
ekonomii	ekonomie	k1gFnSc4	ekonomie
jako	jako	k8xS	jako
vědu	věda	k1gFnSc4	věda
studující	studující	k1gFnSc2	studující
pouze	pouze	k6eAd1	pouze
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
zločin	zločin	k1gInSc4	zločin
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
hlediska	hledisko	k1gNnSc2	hledisko
analyzovat	analyzovat	k5eAaImF	analyzovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
vědu	věda	k1gFnSc4	věda
zabývající	zabývající	k2eAgFnSc4d1	zabývající
se	se	k3xPyFc4	se
každodenním	každodenní	k2eAgInSc7d1	každodenní
postojem	postoj	k1gInSc7	postoj
každého	každý	k3xTgInSc2	každý
subjektu	subjekt	k1gInSc2	subjekt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
používají	používat	k5eAaImIp3nP	používat
omezené	omezený	k2eAgInPc4d1	omezený
zdroje	zdroj	k1gInPc4	zdroj
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
svých	svůj	k3xOyFgInPc2	svůj
zvolených	zvolený	k2eAgInPc2d1	zvolený
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
komentáře	komentář	k1gInPc1	komentář
odsuzovaly	odsuzovat	k5eAaImAgInP	odsuzovat
známé	známý	k2eAgNnSc4d1	známé
definice	definice	k1gFnPc4	definice
ekonomie	ekonomie	k1gFnSc2	ekonomie
jako	jako	k8xS	jako
příliš	příliš	k6eAd1	příliš
obšírné	obšírný	k2eAgFnPc1d1	obšírná
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
jejího	její	k3xOp3gNnSc2	její
zaměření	zaměření	k1gNnSc2	zaměření
na	na	k7c4	na
pouhou	pouhý	k2eAgFnSc4d1	pouhá
analýzu	analýza	k1gFnSc4	analýza
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
takové	takový	k3xDgNnSc4	takový
komentáře	komentář	k1gInPc1	komentář
polevily	polevit	k5eAaPmAgInP	polevit
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
teorie	teorie	k1gFnSc1	teorie
maximalizace	maximalizace	k1gFnSc2	maximalizace
zisku	zisk	k1gInSc2	zisk
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
racionální	racionální	k2eAgFnSc2d1	racionální
volby	volba	k1gFnSc2	volba
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
oblast	oblast	k1gFnSc4	oblast
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
kritické	kritický	k2eAgInPc1d1	kritický
ohlasy	ohlas	k1gInPc1	ohlas
např.	např.	kA	např.
na	na	k7c6	na
nezohlednění	nezohlednění	k1gNnSc6	nezohlednění
vysoké	vysoký	k2eAgFnSc2d1	vysoká
míry	míra	k1gFnSc2	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
v	v	k7c6	v
makroekonomice	makroekonomika	k1gFnSc6	makroekonomika
<g/>
.	.	kIx.	.
<g/>
Gary	Gar	k2eAgFnPc1d1	Gara
Stanley	Stanlea	k1gFnPc1	Stanlea
Becker	Becker	k1gMnSc1	Becker
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
ekonomie	ekonomie	k1gFnSc2	ekonomie
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
popisoval	popisovat	k5eAaImAgMnS	popisovat
a	a	k8xC	a
upřednostňoval	upřednostňovat	k5eAaImAgMnS	upřednostňovat
svůj	svůj	k3xOyFgInSc4	svůj
přístup	přístup	k1gInSc4	přístup
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
kombinaci	kombinace	k1gFnSc4	kombinace
předpokladu	předpoklad	k1gInSc2	předpoklad
maximalizace	maximalizace	k1gFnSc2	maximalizace
zisku	zisk	k1gInSc2	zisk
<g/>
,	,	kIx,	,
stálých	stálý	k2eAgFnPc2d1	stálá
preferencí	preference	k1gFnPc2	preference
a	a	k8xC	a
rovnováhy	rovnováha	k1gFnSc2	rovnováha
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
užívanou	užívaný	k2eAgFnSc4d1	užívaná
neoblomně	oblomně	k6eNd1	oblomně
a	a	k8xC	a
neohroženě	ohroženě	k6eNd1	ohroženě
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Gary	Gara	k1gMnSc2	Gara
Becker	Becker	k1gMnSc1	Becker
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ekonomie	ekonomie	k1gFnSc1	ekonomie
nedefinuje	definovat	k5eNaBmIp3nS	definovat
předmětem	předmět	k1gInSc7	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svou	svůj	k3xOyFgFnSc7	svůj
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc7d1	založená
na	na	k7c6	na
marginální	marginální	k2eAgFnSc6d1	marginální
analýze	analýza	k1gFnSc6	analýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
ekonomie	ekonomie	k1gFnSc2	ekonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
ekonomie	ekonomie	k1gFnSc1	ekonomie
<g/>
:	:	kIx,	:
objektivní	objektivní	k2eAgNnSc1d1	objektivní
(	(	kIx(	(
<g/>
hodnotově	hodnotově	k6eAd1	hodnotově
neutrální	neutrální	k2eAgInPc1d1	neutrální
<g/>
)	)	kIx)	)
rozbor	rozbor	k1gInSc1	rozbor
a	a	k8xC	a
popis	popis	k1gInSc1	popis
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
chování	chování	k1gNnSc2	chování
reálné	reálný	k2eAgFnSc2d1	reálná
ekonomiky	ekonomika	k1gFnSc2	ekonomika
bez	bez	k7c2	bez
kvalitativního	kvalitativní	k2eAgNnSc2d1	kvalitativní
hodnocení	hodnocení	k1gNnSc2	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
Jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Normativní	normativní	k2eAgFnSc1d1	normativní
ekonomie	ekonomie	k1gFnSc1	ekonomie
<g/>
:	:	kIx,	:
kvalitativní	kvalitativní	k2eAgNnSc1d1	kvalitativní
hodnocení	hodnocení	k1gNnSc1	hodnocení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
hledání	hledání	k1gNnSc1	hledání
ideálního	ideální	k2eAgInSc2d1	ideální
stavu	stav	k1gInSc2	stav
fungujícího	fungující	k2eAgInSc2d1	fungující
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
stávající	stávající	k2eAgInPc4d1	stávající
systémy	systém	k1gInPc4	systém
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
ekonomie	ekonomie	k1gFnSc2	ekonomie
je	být	k5eAaImIp3nS	být
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
<g/>
,	,	kIx,	,
odráží	odrážet	k5eAaImIp3nS	odrážet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
morální	morální	k2eAgFnSc6d1	morální
a	a	k8xC	a
sociální	sociální	k2eAgInPc4d1	sociální
kořeny	kořen	k1gInPc4	kořen
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
Jak	jak	k8xC	jak
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
<g/>
?	?	kIx.	?
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
ekonomie	ekonomie	k1gFnSc1	ekonomie
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zbavit	zbavit	k5eAaPmF	zbavit
normativních	normativní	k2eAgInPc2d1	normativní
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
čistě	čistě	k6eAd1	čistě
pozitivní	pozitivní	k2eAgFnSc7d1	pozitivní
vědou	věda	k1gFnSc7	věda
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zkoumání	zkoumání	k1gNnSc2	zkoumání
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
rozhodování	rozhodování	k1gNnSc4	rozhodování
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
firem	firma	k1gFnPc2	firma
(	(	kIx(	(
<g/>
podniků	podnik	k1gInPc2	podnik
<g/>
)	)	kIx)	)
či	či	k8xC	či
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
ekonomie	ekonomie	k1gFnSc1	ekonomie
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
mikroekonomii	mikroekonomie	k1gFnSc4	mikroekonomie
a	a	k8xC	a
makroekonomii	makroekonomie	k1gFnSc4	makroekonomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mikroekonomie	mikroekonomie	k1gFnSc1	mikroekonomie
<g/>
:	:	kIx,	:
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
chováním	chování	k1gNnSc7	chování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
např.	např.	kA	např.
relativní	relativní	k2eAgFnPc4d1	relativní
ceny	cena	k1gFnPc4	cena
statků	statek	k1gInPc2	statek
a	a	k8xC	a
výrobních	výrobní	k2eAgInPc2d1	výrobní
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc4	vliv
daní	daň	k1gFnPc2	daň
na	na	k7c4	na
podnik	podnik	k1gInSc4	podnik
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Makroekonomie	makroekonomie	k1gFnSc1	makroekonomie
<g/>
:	:	kIx,	:
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
agregáty	agregát	k1gInPc4	agregát
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
např.	např.	kA	např.
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
cenovou	cenový	k2eAgFnSc4d1	cenová
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
inflaci	inflace	k1gFnSc4	inflace
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
Nadále	nadále	k6eAd1	nadále
aktuální	aktuální	k2eAgInSc1d1	aktuální
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc1	obor
politická	politický	k2eAgFnSc1d1	politická
ekonomie	ekonomie	k1gFnSc1	ekonomie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
komplexní	komplexní	k2eAgNnSc4d1	komplexní
zkoumání	zkoumání	k1gNnSc4	zkoumání
sociologických	sociologický	k2eAgInPc2d1	sociologický
<g/>
,	,	kIx,	,
politických	politický	k2eAgInPc2d1	politický
a	a	k8xC	a
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
jevů	jev	k1gInPc2	jev
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
potřeba	potřeba	k1gFnSc1	potřeba
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
subjekt	subjekt	k1gInSc1	subjekt
cítí	cítit	k5eAaImIp3nS	cítit
nedostatek	nedostatek	k1gInSc4	nedostatek
a	a	k8xC	a
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
úsilí	úsilí	k1gNnSc1	úsilí
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
překonání	překonání	k1gNnSc6	překonání
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
subjekt	subjekt	k1gInSc1	subjekt
spotřebou	spotřeba	k1gFnSc7	spotřeba
statků	statek	k1gInPc2	statek
získává	získávat	k5eAaImIp3nS	získávat
užitek	užitek	k1gInSc1	užitek
<g/>
.	.	kIx.	.
</s>
<s>
Užitek	užitek	k1gInSc1	užitek
je	být	k5eAaImIp3nS	být
subjektivní	subjektivní	k2eAgInSc4d1	subjektivní
pojem	pojem	k1gInSc4	pojem
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
statků	statek	k1gInPc2	statek
plyne	plynout	k5eAaImIp3nS	plynout
různým	různý	k2eAgInPc3d1	různý
subjektům	subjekt	k1gInPc3	subjekt
různý	různý	k2eAgInSc4d1	různý
užitek	užitek	k1gInSc4	užitek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Statek	statek	k1gInSc1	statek
v	v	k7c6	v
ekonomii	ekonomie	k1gFnSc6	ekonomie
znamená	znamenat	k5eAaImIp3nS	znamenat
schopnost	schopnost	k1gFnSc4	schopnost
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
předmětů	předmět	k1gInPc2	předmět
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
vyhovění	vyhovění	k1gNnSc3	vyhovění
potřebám	potřeba	k1gFnPc3	potřeba
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
zamýšlených	zamýšlený	k2eAgFnPc2d1	zamýšlená
činností	činnost	k1gFnPc2	činnost
<g/>
)	)	kIx)	)
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
subjektů	subjekt	k1gInPc2	subjekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrobní	výrobní	k2eAgInSc1d1	výrobní
faktor	faktor	k1gInSc1	faktor
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
statků	statek	k1gInPc2	statek
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
směny	směna	k1gFnSc2	směna
přináší	přinášet	k5eAaImIp3nS	přinášet
svému	svůj	k3xOyFgMnSc3	svůj
majiteli	majitel	k1gMnSc3	majitel
důchod	důchod	k1gInSc4	důchod
(	(	kIx(	(
<g/>
přínos	přínos	k1gInSc4	přínos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
poskytování	poskytování	k1gNnSc1	poskytování
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přinášejí	přinášet	k5eAaImIp3nP	přinášet
současný	současný	k2eAgInSc4d1	současný
nebo	nebo	k8xC	nebo
budoucí	budoucí	k2eAgInSc4d1	budoucí
užitek	užitek	k1gInSc4	užitek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Směna	směna	k1gFnSc1	směna
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
si	se	k3xPyFc3	se
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
subjekty	subjekt	k1gInPc4	subjekt
navzájem	navzájem	k6eAd1	navzájem
vyměňují	vyměňovat	k5eAaImIp3nP	vyměňovat
statky	statek	k1gInPc1	statek
<g/>
.	.	kIx.	.
</s>
<s>
Informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc4	jaký
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
množství	množství	k1gNnSc3	množství
jednoho	jeden	k4xCgInSc2	jeden
statku	statek	k1gInSc2	statek
za	za	k7c4	za
statek	statek	k1gInSc4	statek
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
sjednávané	sjednávaný	k2eAgFnPc4d1	sjednávaná
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
opakované	opakovaný	k2eAgInPc4d1	opakovaný
statky	statek	k1gInPc4	statek
se	se	k3xPyFc4	se
ceny	cena	k1gFnPc1	cena
formují	formovat	k5eAaImIp3nP	formovat
na	na	k7c6	na
trzích	trh	k1gInPc6	trh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
střetnutí	střetnutí	k1gNnSc2	střetnutí
nabídky	nabídka	k1gFnSc2	nabídka
a	a	k8xC	a
poptávky	poptávka	k1gFnSc2	poptávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dělba	dělba	k1gFnSc1	dělba
práce	práce	k1gFnSc2	práce
označuje	označovat	k5eAaImIp3nS	označovat
specializaci	specializace	k1gFnSc4	specializace
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
subjektů	subjekt	k1gInPc2	subjekt
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
činnostech	činnost	k1gFnPc6	činnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vyšší	vysoký	k2eAgFnSc3d2	vyšší
produkci	produkce	k1gFnSc3	produkce
a	a	k8xC	a
růstu	růst	k1gInSc3	růst
produktivity	produktivita	k1gFnSc2	produktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
základních	základní	k2eAgFnPc2d1	základní
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
teorií	teorie	k1gFnPc2	teorie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Starověké	starověký	k2eAgNnSc1d1	starověké
Řecko	Řecko	k1gNnSc1	Řecko
–	–	k?	–
Homér	Homér	k1gMnSc1	Homér
<g/>
,	,	kIx,	,
Platón	Platón	k1gMnSc1	Platón
<g/>
,	,	kIx,	,
Xenofon	Xenofon	k1gInSc1	Xenofon
<g/>
,	,	kIx,	,
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
</s>
</p>
<p>
<s>
Starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
–	–	k?	–
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
názory	názor	k1gInPc1	názor
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
různými	různý	k2eAgInPc7d1	různý
autory	autor	k1gMnPc4	autor
zejména	zejména	k9	zejména
ve	v	k7c6	v
spisech	spis	k1gInPc6	spis
týkajících	týkající	k2eAgInPc6d1	týkající
se	s	k7c7	s
zemědělství	zemědělství	k1gNnSc2	zemědělství
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Scholastika	scholastika	k1gFnSc1	scholastika
</s>
</p>
<p>
<s>
Kanonisté	kanonista	k1gMnPc1	kanonista
–	–	k?	–
církevní	církevní	k2eAgMnPc1d1	církevní
myslitelé	myslitel	k1gMnPc1	myslitel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c4	na
vypracování	vypracování	k1gNnSc4	vypracování
církevního	církevní	k2eAgInSc2d1	církevní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kanonického	kanonický	k2eAgNnSc2d1	kanonické
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Novověk	novověk	k1gInSc4	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
Merkantilismus	merkantilismus	k1gInSc1	merkantilismus
–	–	k?	–
bohatství	bohatství	k1gNnSc2	bohatství
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
množstvím	množství	k1gNnSc7	množství
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
obchodu	obchod	k1gInSc2	obchod
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
obchodní	obchodní	k2eAgFnSc1d1	obchodní
bilance	bilance	k1gFnSc1	bilance
</s>
</p>
<p>
<s>
Fyziokratismus	fyziokratismus	k1gInSc1	fyziokratismus
–	–	k?	–
bohatství	bohatství	k1gNnSc2	bohatství
země	zem	k1gFnSc2	zem
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
zemědělství	zemědělství	k1gNnSc2	zemědělství
</s>
</p>
<p>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
škola	škola	k1gFnSc1	škola
–	–	k?	–
zakladatelem	zakladatel	k1gMnSc7	zakladatel
počátků	počátek	k1gInPc2	počátek
moderní	moderní	k2eAgFnSc2d1	moderní
ekonomie	ekonomie	k1gFnSc2	ekonomie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Adam	Adam	k1gMnSc1	Adam
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
1723	[number]	k4	1723
<g/>
–	–	k?	–
<g/>
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
dílem	dílo	k1gNnSc7	dílo
Pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
podstatě	podstata	k1gFnSc6	podstata
a	a	k8xC	a
původu	původ	k1gInSc2	původ
bohatství	bohatství	k1gNnSc2	bohatství
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Ricardem	Ricard	k1gMnSc7	Ricard
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
Stuartem	Stuart	k1gMnSc7	Stuart
Millem	Mill	k1gMnSc7	Mill
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
průkopníka	průkopník	k1gMnSc4	průkopník
klasické	klasický	k2eAgFnSc2d1	klasická
politické	politický	k2eAgFnSc2d1	politická
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
ovládána	ovládán	k2eAgFnSc1d1	ovládána
svými	svůj	k3xOyFgInPc7	svůj
vlastními	vlastní	k2eAgInPc7d1	vlastní
zákony	zákon	k1gInPc7	zákon
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
ruka	ruka	k1gFnSc1	ruka
trhu	trh	k1gInSc2	trh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
zásady	zásada	k1gFnSc2	zásada
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
liberalismu	liberalismus	k1gInSc2	liberalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
škola	škola	k1gFnSc1	škola
–	–	k?	–
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
liberální	liberální	k2eAgInPc4d1	liberální
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
především	především	k9	především
lidské	lidský	k2eAgNnSc1d1	lidské
jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc1	činnost
státu	stát	k1gInSc2	stát
považuje	považovat	k5eAaImIp3nS	považovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
za	za	k7c4	za
nežádoucí	žádoucí	k2eNgNnSc4d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neoklasická	neoklasický	k2eAgFnSc1d1	neoklasická
škola	škola	k1gFnSc1	škola
–	–	k?	–
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
klasickou	klasický	k2eAgFnSc4d1	klasická
politickou	politický	k2eAgFnSc4d1	politická
ekonomii	ekonomie	k1gFnSc4	ekonomie
počátky	počátek	k1gInPc1	počátek
neoklasické	neoklasický	k2eAgFnSc2d1	neoklasická
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
klasické	klasický	k2eAgFnSc3d1	klasická
politické	politický	k2eAgFnSc3d1	politická
ekonomii	ekonomie	k1gFnSc3	ekonomie
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
používat	používat	k5eAaImF	používat
marginální	marginální	k2eAgFnPc4d1	marginální
veličiny	veličina	k1gFnPc4	veličina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
dodnes	dodnes	k6eAd1	dodnes
používaný	používaný	k2eAgInSc1d1	používaný
analytický	analytický	k2eAgInSc1d1	analytický
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgInPc4	první
neoklasiky	neoklasik	k1gInPc4	neoklasik
patří	patřit	k5eAaImIp3nS	patřit
Alfred	Alfred	k1gMnSc1	Alfred
Marshall	Marshall	k1gMnSc1	Marshall
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
prosadil	prosadit	k5eAaPmAgInS	prosadit
monetarismus	monetarismus	k1gInSc1	monetarismus
<g/>
,	,	kIx,	,
navazující	navazující	k2eAgInSc1d1	navazující
na	na	k7c4	na
neoklasickou	neoklasický	k2eAgFnSc4d1	neoklasická
ekonomii	ekonomie	k1gFnSc4	ekonomie
a	a	k8xC	a
doporučující	doporučující	k2eAgFnSc4d1	doporučující
opětovnou	opětovný	k2eAgFnSc4d1	opětovná
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
liberalizaci	liberalizace	k1gFnSc4	liberalizace
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
A.	A.	kA	A.
Samuelson	Samuelson	k1gMnSc1	Samuelson
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kenneth	Kenneth	k1gInSc1	Kenneth
Arrowem	Arrow	k1gInSc7	Arrow
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnPc4	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
neoklasické	neoklasický	k2eAgFnSc2d1	neoklasická
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1	Keynesiánství
–	–	k?	–
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Angličan	Angličan	k1gMnSc1	Angličan
John	John	k1gMnSc1	John
Maynard	Maynard	k1gMnSc1	Maynard
Keynes	Keynes	k1gMnSc1	Keynes
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
narušovala	narušovat	k5eAaImAgFnS	narušovat
důvěru	důvěra	k1gFnSc4	důvěra
ve	v	k7c4	v
samoregulační	samoregulační	k2eAgInPc4d1	samoregulační
tržní	tržní	k2eAgInPc4d1	tržní
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
M	M	kA	M
Keynes	Keynes	k1gMnSc1	Keynes
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
teorii	teorie	k1gFnSc4	teorie
keynesovské	keynesovský	k2eAgFnSc2d1	keynesovská
makroekonomie	makroekonomie	k1gFnSc2	makroekonomie
(	(	kIx(	(
<g/>
keynesiánství	keynesiánství	k1gNnSc1	keynesiánství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
stabilizaci	stabilizace	k1gFnSc4	stabilizace
kapitalistických	kapitalistický	k2eAgFnPc2d1	kapitalistická
ekonomik	ekonomika	k1gFnPc2	ekonomika
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
státních	státní	k2eAgInPc2d1	státní
zásahů	zásah	k1gInPc2	zásah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současnost	současnost	k1gFnSc4	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
ekonomie	ekonomie	k1gFnSc1	ekonomie
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
součástí	součást	k1gFnSc7	součást
neoklasického	neoklasický	k2eAgNnSc2d1	neoklasické
paradigmatu	paradigma	k1gNnSc2	paradigma
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
marginální	marginální	k2eAgFnSc4d1	marginální
analýzu	analýza	k1gFnSc4	analýza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soudobém	soudobý	k2eAgInSc6d1	soudobý
diskurzu	diskurz	k1gInSc6	diskurz
převládá	převládat	k5eAaImIp3nS	převládat
tzv.	tzv.	kA	tzv.
Nová	nový	k2eAgFnSc1d1	nová
Keynesiánská	keynesiánský	k2eAgFnSc1d1	keynesiánská
ekonomie	ekonomie	k1gFnSc1	ekonomie
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
většina	většina	k1gFnSc1	většina
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
současných	současný	k2eAgMnPc2d1	současný
ekonomů	ekonom	k1gMnPc2	ekonom
(	(	kIx(	(
<g/>
Krugman	Krugman	k1gMnSc1	Krugman
<g/>
,	,	kIx,	,
Stiglitz	Stiglitz	k1gMnSc1	Stiglitz
<g/>
,	,	kIx,	,
Mankiw	Mankiw	k1gMnSc1	Mankiw
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současní	současný	k2eAgMnPc1d1	současný
ekonomové	ekonom	k1gMnPc1	ekonom
čerpají	čerpat	k5eAaImIp3nP	čerpat
z	z	k7c2	z
keynesiánství	keynesiánství	k1gNnSc2	keynesiánství
i	i	k8xC	i
neoklasické	neoklasický	k2eAgFnSc2d1	neoklasická
ekonomie	ekonomie	k1gFnSc2	ekonomie
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
stavějí	stavět	k5eAaImIp3nP	stavět
za	za	k7c4	za
institucionální	institucionální	k2eAgFnSc4d1	institucionální
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc4d1	nový
proud	proud	k1gInSc4	proud
zdůrazňující	zdůrazňující	k2eAgInSc4d1	zdůrazňující
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
roli	role	k1gFnSc4	role
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
čase	čas	k1gInSc6	čas
si	se	k3xPyFc3	se
získává	získávat	k5eAaImIp3nS	získávat
poměrně	poměrně	k6eAd1	poměrně
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
behaviorální	behaviorální	k2eAgFnSc2d1	behaviorální
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
biofyzikální	biofyzikální	k2eAgFnSc2d1	biofyzikální
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
ekologická	ekologický	k2eAgFnSc1d1	ekologická
ekonomie	ekonomie	k1gFnSc1	ekonomie
či	či	k8xC	či
ekonomie	ekonomie	k1gFnSc1	ekonomie
udržitelného	udržitelný	k2eAgInSc2d1	udržitelný
nerůstu	nerůst	k1gInSc2	nerůst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SAMUELSON	SAMUELSON	kA	SAMUELSON
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
A.	A.	kA	A.
<g/>
;	;	kIx,	;
NORDHAUS	NORDHAUS	kA	NORDHAUS
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
D.	D.	kA	D.
Ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NS	NS	kA	NS
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
590	[number]	k4	590
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
1011	[number]	k4	1011
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MANKIW	MANKIW	kA	MANKIW
<g/>
,	,	kIx,	,
Gregory	Gregor	k1gMnPc4	Gregor
<g/>
.	.	kIx.	.
</s>
<s>
Zásady	zásada	k1gFnPc1	zásada
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
891	[number]	k4	891
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
763	[number]	k4	763
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOLMAN	Holman	k1gMnSc1	Holman
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
C.H.	C.H.	k1gMnSc1	C.H.
<g/>
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7179	[number]	k4	7179
<g/>
-	-	kIx~	-
<g/>
380	[number]	k4	380
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
540	[number]	k4	540
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
Švédské	švédský	k2eAgFnSc2d1	švédská
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
vědy	věda	k1gFnSc2	věda
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
(	(	kIx(	(
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
myšlení	myšlení	k1gNnSc2	myšlení
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ekonomie	ekonomie	k1gFnSc2	ekonomie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ekonomie	ekonomie	k1gFnSc2	ekonomie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Ekonomie	ekonomie	k1gFnSc2	ekonomie
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
The	The	k?	The
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Economics	Economicsa	k1gFnPc2	Economicsa
and	and	k?	and
Liberty	Libert	k1gInPc1	Libert
–	–	k?	–
informační	informační	k2eAgInPc1d1	informační
zdroje	zdroj	k1gInPc1	zdroj
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
pro	pro	k7c4	pro
vědecké	vědecký	k2eAgMnPc4d1	vědecký
a	a	k8xC	a
výzkumné	výzkumný	k2eAgMnPc4d1	výzkumný
pracovníky	pracovník	k1gMnPc4	pracovník
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
