<s>
Ekonomie	ekonomie	k1gFnSc1	ekonomie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
popisem	popis	k1gInSc7	popis
a	a	k8xC	a
analýzou	analýza	k1gFnSc7	analýza
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
distribuce	distribuce	k1gFnSc2	distribuce
a	a	k8xC	a
spotřeby	spotřeba	k1gFnSc2	spotřeba
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
statků	statek	k1gInPc2	statek
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
zboží	zboží	k1gNnSc1	zboží
<g/>
,	,	kIx,	,
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
peněz	peníze	k1gInPc2	peníze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgInPc1d1	omezený
zdroje	zdroj	k1gInPc1	zdroj
alokovány	alokovat	k5eAaImNgInP	alokovat
mezi	mezi	k7c4	mezi
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
alternativní	alternativní	k2eAgNnPc4d1	alternativní
využití	využití	k1gNnPc4	využití
<g/>
.	.	kIx.	.
</s>
