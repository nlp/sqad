<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
popisem	popis	k1gInSc7	popis
a	a	k8xC	a
analýzou	analýza	k1gFnSc7	analýza
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
distribuce	distribuce	k1gFnSc2	distribuce
a	a	k8xC	a
spotřeby	spotřeba	k1gFnSc2	spotřeba
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
statků	statek	k1gInPc2	statek
<g/>
?	?	kIx.	?
</s>
