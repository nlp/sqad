<s>
Malajálamská	Malajálamský	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
</s>
<s>
Malajálamská	Malajálamský	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Malajálamské	Malajálamský	k2eAgFnSc2d1
WikipedieURL	WikipedieURL	k1gFnSc2
</s>
<s>
ml.	ml.	kA
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
org	org	k?
Komerční	komerční	k2eAgFnSc2d1
</s>
<s>
ne	ne	k9
Charakter	charakter	k1gInSc1
stránky	stránka	k1gFnSc2
</s>
<s>
internetová	internetový	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
Registrace	registrace	k1gFnSc1
</s>
<s>
nepovinná	povinný	k2eNgFnSc1d1
V	v	k7c6
jazyce	jazyk	k1gInSc6
</s>
<s>
malajálamština	malajálamština	k1gFnSc1
Licence	licence	k1gFnSc2
obsahu	obsah	k1gInSc2
</s>
<s>
CC-BY-SA	CC-BY-SA	k?
3.0	3.0	k4
Unported	Unported	k1gMnSc1
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Wikimedia	Wikimedium	k1gNnPc4
Foundation	Foundation	k1gInSc1
Aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
</s>
<s>
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Malajálamská	Malajálamský	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
je	být	k5eAaImIp3nS
jazyková	jazykový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Wikipedie	Wikipedie	k1gFnSc2
v	v	k7c6
malajálamštině	malajálamština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2020	#num#	k4
obsahovala	obsahovat	k5eAaImAgFnS
přes	přes	k7c4
67	#num#	k4
000	#num#	k4
článků	článek	k1gInPc2
a	a	k8xC
pracovalo	pracovat	k5eAaImAgNnS
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
21	#num#	k4
správců	správce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Registrováno	registrovat	k5eAaBmNgNnS
bylo	být	k5eAaImAgNnS
přes	přes	k7c4
131	#num#	k4
000	#num#	k4
uživatelů	uživatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
bylo	být	k5eAaImAgNnS
asi	asi	k9
340	#num#	k4
aktivních	aktivní	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počtu	počet	k1gInSc6
článků	článek	k1gInPc2
byla	být	k5eAaImAgFnS
80	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgFnSc2d3
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jimmy	Jimma	k1gFnPc4
Wales	Wales	k1gInSc1
představuje	představovat	k5eAaImIp3nS
offline	offlin	k1gInSc5
verzi	verze	k1gFnSc6
malajálamské	malajálamský	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
</s>
<s>
Zájem	zájem	k1gInSc1
o	o	k7c4
tuto	tento	k3xDgFnSc4
jazykovou	jazykový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
a	a	k8xC
počet	počet	k1gInSc4
jejích	její	k3xOp3gInPc2
článků	článek	k1gInPc2
prudce	prudko	k6eAd1
stoupl	stoupnout	k5eAaPmAgInS
po	po	k7c6
propagačním	propagační	k2eAgInSc6d1
článku	článek	k1gInSc6
v	v	k7c6
novinách	novina	k1gFnPc6
Mathrubhumi	Mathrubhu	k1gFnPc7
2	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
Jimmy	Jimma	k1gFnSc2
Wales	Wales	k1gInSc1
představil	představit	k5eAaPmAgInS
vydání	vydání	k1gNnSc4
offline	offlin	k1gInSc5
verze	verze	k1gFnSc1
malajálamské	malajálamský	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
na	na	k7c4
CD	CD	kA
s	s	k7c7
vybranými	vybraný	k2eAgNnPc7d1
500	#num#	k4
články	článek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
první	první	k4xOgNnSc4
takové	takový	k3xDgInPc4
vydání	vydání	k1gNnSc4
v	v	k7c6
jazyce	jazyk	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
nepoužívá	používat	k5eNaImIp3nS
latinku	latinka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
malajálamská	malajálamský	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Malajálamská	Malajálamský	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Jazykové	jazykový	k2eAgFnSc2d1
verze	verze	k1gFnSc2
Wikipedie	Wikipedie	k1gFnSc2
podle	podle	k7c2
počtu	počet	k1gInSc2
článků	článek	k1gInPc2
5	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
anglická	anglický	k2eAgFnSc1d1
(	(	kIx(
<g/>
en	en	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
cebuánská	cebuánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ceb	ceb	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
2	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
švédská	švédský	k2eAgFnSc1d1
(	(	kIx(
<g/>
sv	sv	kA
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
německá	německý	k2eAgFnSc1d1
(	(	kIx(
<g/>
de	de	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
francouzská	francouzský	k2eAgFnSc1d1
(	(	kIx(
<g/>
fr	fr	k0
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
nizozemská	nizozemský	k2eAgFnSc1d1
(	(	kIx(
<g/>
nl	nl	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
1	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
ruská	ruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ru	ru	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
italská	italský	k2eAgFnSc1d1
(	(	kIx(
<g/>
it	it	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
španělská	španělský	k2eAgFnSc1d1
(	(	kIx(
<g/>
es	es	k1gNnPc2
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
polská	polský	k2eAgFnSc1d1
(	(	kIx(
<g/>
pl	pl	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
warajská	warajský	k2eAgFnSc1d1
(	(	kIx(
<g/>
war	war	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
vietnamská	vietnamský	k2eAgFnSc1d1
(	(	kIx(
<g/>
vi	vi	k?
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
japonská	japonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ja	ja	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
egyptsko	egyptsko	k6eAd1
arabská	arabský	k2eAgFnSc1d1
(	(	kIx(
<g/>
arz	arz	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
čínská	čínský	k2eAgFnSc1d1
(	(	kIx(
<g/>
zh	zh	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
arabská	arabský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ar	ar	k1gInSc1
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
ukrajinská	ukrajinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
uk	uk	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
pt	pt	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
750	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
perská	perský	k2eAgNnPc1d1
(	(	kIx(
<g/>
fa	fa	k1gNnPc1
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
katalánská	katalánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ca	ca	kA
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
srbská	srbský	k2eAgFnSc1d1
(	(	kIx(
<g/>
sr	sr	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
indonéská	indonéský	k2eAgFnSc1d1
(	(	kIx(
<g/>
id	ido	k1gNnPc2
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
norská	norský	k2eAgFnSc1d1
(	(	kIx(
<g/>
bokmå	bokmå	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
no	no	k9
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
korejská	korejský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ko	ko	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
finská	finský	k2eAgFnSc1d1
(	(	kIx(
<g/>
fi	fi	k0
<g/>
:	:	kIx,
<g/>
)	)	kIx)
250	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
maďarská	maďarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
hu	hu	k0
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
česká	český	k2eAgFnSc1d1
(	(	kIx(
<g/>
cs	cs	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
srbochorvatská	srbochorvatský	k2eAgFnSc1d1
(	(	kIx(
<g/>
sh	sh	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
Min	min	kA
Nan	Nan	k1gMnSc1
(	(	kIx(
<g/>
zh-min-nan	zh-min-nan	k1gMnSc1
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
rumunská	rumunský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ro	ro	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
turecká	turecký	k2eAgFnSc1d1
(	(	kIx(
<g/>
tr	tr	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
baskická	baskický	k2eAgFnSc1d1
(	(	kIx(
<g/>
eu	eu	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
čečenská	čečenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ce	ce	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
malajská	malajský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ms	ms	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
esperantská	esperantský	k2eAgFnSc1d1
(	(	kIx(
<g/>
eo	eo	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
hebrejská	hebrejský	k2eAgFnSc1d1
(	(	kIx(
<g/>
he	he	k0
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
arménská	arménský	k2eAgFnSc1d1
(	(	kIx(
<g/>
hy	hy	k0
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
bulharská	bulharský	k2eAgFnSc1d1
(	(	kIx(
<g/>
bg	bg	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
tatarská	tatarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
tt	tt	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
dánská	dánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
da	da	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
jihoázerbájdžánská	jihoázerbájdžánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
azb	azb	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
slovenská	slovenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
sk	sk	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
kazašská	kazašský	k2eAgFnSc1d1
(	(	kIx(
<g/>
kk	kk	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
minangkabauská	minangkabauskat	k5eAaPmIp3nS
(	(	kIx(
<g/>
min	min	kA
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
estonská	estonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
et	et	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
chorvatská	chorvatský	k2eAgFnSc1d1
(	(	kIx(
<g/>
hr	hr	k2eAgFnSc1d1
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
běloruská	běloruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
be	be	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
litevská	litevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
lt	lt	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
řecká	řecký	k2eAgFnSc1d1
(	(	kIx(
<g/>
el	ela	k1gFnPc2
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
zjednodušená	zjednodušený	k2eAgFnSc1d1
angličtina	angličtina	k1gFnSc1
(	(	kIx(
<g/>
simple	simple	k6eAd1
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
ázerbájdžánská	ázerbájdžánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
az	az	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
galicijská	galicijský	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
gl	gl	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
slovinská	slovinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
sl	sl	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
urdská	urdský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ur	ur	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
norská	norský	k2eAgFnSc1d1
(	(	kIx(
<g/>
nynorsk	nynorsk	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
nn	nn	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
gruzínská	gruzínský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ka	ka	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
hindská	hindský	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
hi	hi	k0
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
thajská	thajský	k2eAgFnSc1d1
(	(	kIx(
<g/>
th	th	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
uzbecká	uzbecký	k2eAgFnSc1d1
(	(	kIx(
<g/>
uz	uz	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
tamilská	tamilský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ta	ten	k3xDgFnSc1
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
latinská	latinský	k2eAgNnPc4d1
(	(	kIx(
<g/>
la	la	k1gNnPc4
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
velšská	velšský	k2eAgFnSc1d1
(	(	kIx(
<g/>
cy	cy	k?
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
volapük	volapük	k1gInSc1
(	(	kIx(
<g/>
vo	vo	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
asturská	asturský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ast	ast	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
makedonská	makedonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
mk	mk	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
kantonská	kantonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
zh-yue	zh-yue	k1gFnSc1
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
lotyšská	lotyšský	k2eAgFnSc1d1
(	(	kIx(
<g/>
lv	lv	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
bengálská	bengálský	k2eAgFnSc1d1
(	(	kIx(
<g/>
bn	bn	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
tádžická	tádžický	k2eAgFnSc1d1
(	(	kIx(
<g/>
tg	tg	kA
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
barmská	barmský	k2eAgFnSc1d1
(	(	kIx(
<g/>
my	my	k3xPp1nPc1
<g/>
:	:	kIx,
<g/>
)	)	kIx)
75	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
afrikánská	afrikánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
af	af	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
malgašská	malgašský	k2eAgFnSc1d1
(	(	kIx(
<g/>
mg	mg	kA
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
okcitánská	okcitánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
oc	oc	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
bosenská	bosenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
bs	bs	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
albánská	albánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
sq	sq	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
dolnoněmecká	dolnoněmecký	k2eAgFnSc1d1
(	(	kIx(
<g/>
nds	nds	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
kyrgyzská	kyrgyzský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ky	ky	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
běloruská	běloruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
taraškevica	taraškevica	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
be-tarask	be-tarask	k1gInSc1
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
malajálamská	malajálamský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ml	ml	kA
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
névárská	névárský	k2eAgFnSc1d1
(	(	kIx(
<g/>
new	new	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
maráthská	maráthská	k1gFnSc1
(	(	kIx(
<g/>
mr	mr	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
telužská	telužský	k2eAgFnSc1d1
(	(	kIx(
<g/>
te	te	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
bretonská	bretonský	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
br	br	k0
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
benátská	benátský	k2eAgFnSc1d1
(	(	kIx(
<g/>
vec	vec	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
piemontská	piemontský	k2eAgFnSc1d1
(	(	kIx(
<g/>
pms	pms	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
javánská	javánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
jv	jv	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
haitská	haitský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ht	ht	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
západopaňdžábská	západopaňdžábský	k2eAgFnSc1d1
(	(	kIx(
<g/>
pnb	pnb	k?
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
svahilská	svahilský	k2eAgFnSc1d1
(	(	kIx(
<g/>
sw	sw	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
sundská	sundský	k2eAgFnSc1d1
(	(	kIx(
<g/>
su	su	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
lucemburská	lucemburský	k2eAgFnSc1d1
(	(	kIx(
<g/>
lb	lb	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
tagaložská	tagaložský	k2eAgFnSc1d1
(	(	kIx(
<g/>
tl	tl	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
baškirská	baškirský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ba	ba	k9
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
irská	irský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ga	ga	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
slezská	slezský	k2eAgFnSc1d1
(	(	kIx(
<g/>
szl	szl	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
islandská	islandský	k2eAgFnSc1d1
(	(	kIx(
<g/>
is	is	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
25	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
lombardská	lombardský	k2eAgFnSc1d1
(	(	kIx(
<g/>
lmo	lmo	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
čuvašská	čuvašský	k2eAgFnSc1d1
(	(	kIx(
<g/>
cv	cv	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
západofríská	západofrískat	k5eAaPmIp3nS
(	(	kIx(
<g/>
fy	fy	kA
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
wu	wu	k?
(	(	kIx(
<g/>
wuu	wuu	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
skotská	skotská	k1gFnSc1
(	(	kIx(
<g/>
sco	sco	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
zazaki	zazak	k1gFnSc2
(	(	kIx(
<g/>
diq	diq	k?
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
aragonská	aragonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
an	an	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
kurdská	kurdský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ku	k	k7c3
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
paňdžábská	paňdžábský	k2eAgFnSc1d1
(	(	kIx(
<g/>
pa	pa	k0
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
jorubská	jorubský	k2eAgFnSc1d1
(	(	kIx(
<g/>
yo	yo	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
nepálská	nepálský	k2eAgFnSc1d1
(	(	kIx(
<g/>
ne	ne	k9
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
bavorská	bavorský	k2eAgFnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
bar	bar	k1gInSc1
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
ido	ido	k1gNnSc1
(	(	kIx(
<g/>
io	io	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
sorání	sorání	k1gNnSc1
(	(	kIx(
<g/>
ckb	ckb	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
gudžarátská	gudžarátský	k2eAgFnSc1d1
(	(	kIx(
<g/>
gu	gu	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
alemanská	alemanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
als	als	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
kannadská	kannadský	k2eAgFnSc1d1
(	(	kIx(
<g/>
scn	scn	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
sicilská	sicilský	k2eAgFnSc1d1
(	(	kIx(
<g/>
scn	scn	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
•	•	k?
bišnuprijskomanipurská	bišnuprijskomanipurský	k2eAgFnSc1d1
(	(	kIx(
<g/>
bpy	bpy	k?
<g/>
:	:	kIx,
<g/>
)	)	kIx)
Stav	stav	k1gInSc1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
dubnu	duben	k1gInSc6
2021	#num#	k4
•	•	k?
Seznam	seznam	k1gInSc1
všech	všecek	k3xTgFnPc2
jazykových	jazykový	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
Wikipedie	Wikipedie	k1gFnSc2
s	s	k7c7
aktuální	aktuální	k2eAgFnSc7d1
statistikou	statistika	k1gFnSc7
</s>
