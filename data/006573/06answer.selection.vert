<s>
Lidem	člověk	k1gMnPc3	člověk
s	s	k7c7	s
takto	takto	k6eAd1	takto
vysokým	vysoký	k2eAgInSc7d1	vysoký
cholesterolem	cholesterol	k1gInSc7	cholesterol
hrozí	hrozit	k5eAaImIp3nS	hrozit
větší	veliký	k2eAgNnSc1d2	veliký
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
srdečně-cévních	srdečněévní	k2eAgNnPc2d1	srdečně-cévní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
