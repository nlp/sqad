<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gInSc1	Universitas
Masarykiana	Masarykiana	k1gFnSc1	Masarykiana
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
Univerzita	univerzita	k1gFnSc1	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
Muni	Muni	k?	Muni
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
MUNI	MUNI	k?	MUNI
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
používaná	používaný	k2eAgFnSc1d1	používaná
jakožto	jakožto	k8xS	jakožto
internetová	internetový	k2eAgFnSc1d1	internetová
doména	doména	k1gFnSc1	doména
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
především	především	k9	především
v	v	k7c6	v
méně	málo	k6eAd2	málo
formálních	formální	k2eAgInPc6d1	formální
kontextech	kontext	k1gInPc6	kontext
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
"	"	kIx"	"
<g/>
MU	MU	kA	MU
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
ve	v	k7c6	v
statutu	statut	k1gInSc6	statut
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
studentů	student	k1gMnPc2	student
v	v	k7c6	v
akreditovaných	akreditovaný	k2eAgInPc6d1	akreditovaný
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
devět	devět	k4xCc4	devět
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
své	své	k1gNnSc4	své
Mendelovo	Mendelův	k2eAgNnSc4d1	Mendelovo
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
kino	kino	k1gNnSc4	kino
Scala	scát	k5eAaImAgFnS	scát
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
centrum	centrum	k1gNnSc4	centrum
v	v	k7c6	v
Telči	Telč	k1gFnSc6	Telč
a	a	k8xC	a
polární	polární	k2eAgFnSc6d1	polární
stanici	stanice	k1gFnSc6	stanice
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
světových	světový	k2eAgFnPc2d1	světová
univerzit	univerzita	k1gFnPc2	univerzita
QS	QS	kA	QS
TopUniversities	TopUniversities	k1gInSc1	TopUniversities
<g/>
.	.	kIx.	.
</s>
<s>
Počtem	počet	k1gInSc7	počet
studentů	student	k1gMnPc2	student
v	v	k7c6	v
akreditovaných	akreditovaný	k2eAgInPc6d1	akreditovaný
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gMnSc7	její
rektorem	rektor	k1gMnSc7	rektor
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Bek	bek	k1gMnSc1	bek
<g/>
.	.	kIx.	.
</s>
