<p>
<s>
Cremona	Cremona	k1gFnSc1	Cremona
(	(	kIx(	(
<g/>
Provincia	Provincia	k1gFnSc1	Provincia
di	di	k?	di
Cremona	Cremona	k1gFnSc1	Cremona
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
italská	italský	k2eAgFnSc1d1	italská
provincie	provincie	k1gFnSc1	provincie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Lombardie	Lombardie	k1gFnSc2	Lombardie
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
provinciemi	provincie	k1gFnPc7	provincie
Bergamo	Bergamo	k1gNnSc1	Bergamo
a	a	k8xC	a
Brescia	Brescia	k1gFnSc1	Brescia
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
provincií	provincie	k1gFnSc7	provincie
Mantova	Mantova	k1gFnSc1	Mantova
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
provinciemi	provincie	k1gFnPc7	provincie
Parma	Parma	k1gFnSc1	Parma
a	a	k8xC	a
Piacenza	Piacenza	k1gFnSc1	Piacenza
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
provinciemi	provincie	k1gFnPc7	provincie
Lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
Milano	Milana	k1gFnSc5	Milana
<g/>
.	.	kIx.	.
</s>
</p>
