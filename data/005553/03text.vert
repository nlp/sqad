<s>
Televizní	televizní	k2eAgNnSc1d1	televizní
vysílání	vysílání	k1gNnSc1	vysílání
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
telekomunikačních	telekomunikační	k2eAgNnPc2d1	telekomunikační
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
a	a	k8xC	a
přijímání	přijímání	k1gNnSc4	přijímání
rychlého	rychlý	k2eAgInSc2d1	rychlý
sledu	sled	k1gInSc2	sled
obrázků	obrázek	k1gInPc2	obrázek
a	a	k8xC	a
zvuku	zvuk	k1gInSc2	zvuk
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
formuloval	formulovat	k5eAaImAgMnS	formulovat
Alexandr	Alexandr	k1gMnSc1	Alexandr
Bain	Bain	k1gMnSc1	Bain
(	(	kIx(	(
<g/>
skotský	skotský	k2eAgMnSc1d1	skotský
hodinář	hodinář	k1gMnSc1	hodinář
<g/>
)	)	kIx)	)
tři	tři	k4xCgInPc1	tři
principy	princip	k1gInPc1	princip
přenosu	přenos	k1gInSc2	přenos
obrazu	obraz	k1gInSc2	obraz
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
:	:	kIx,	:
Rozklad	rozklad	k1gInSc1	rozklad
obrazu	obraz	k1gInSc2	obraz
na	na	k7c4	na
řádky	řádek	k1gInPc4	řádek
a	a	k8xC	a
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Světelné	světelný	k2eAgInPc4d1	světelný
body	bod	k1gInPc4	bod
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
elektrické	elektrický	k2eAgInPc4d1	elektrický
impulsy	impuls	k1gInPc4	impuls
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
i	i	k8xC	i
skládání	skládání	k1gNnSc1	skládání
musí	muset	k5eAaImIp3nS	muset
probíhat	probíhat	k5eAaImF	probíhat
synchronizovaně	synchronizovaně	k6eAd1	synchronizovaně
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Nipkow	Nipkow	k1gMnSc1	Nipkow
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
elektrický	elektrický	k2eAgInSc1d1	elektrický
teleskop	teleskop	k1gInSc1	teleskop
-	-	kIx~	-
princip	princip	k1gInSc1	princip
mechanického	mechanický	k2eAgInSc2d1	mechanický
rozkladu	rozklad	k1gInSc2	rozklad
obrazu	obraz	k1gInSc2	obraz
na	na	k7c4	na
světelné	světelný	k2eAgInPc4d1	světelný
body	bod	k1gInPc4	bod
pomocí	pomocí	k7c2	pomocí
rotujícího	rotující	k2eAgInSc2d1	rotující
kotouče	kotouč	k1gInSc2	kotouč
(	(	kIx(	(
<g/>
spirálově	spirálově	k6eAd1	spirálově
uspořádané	uspořádaný	k2eAgInPc4d1	uspořádaný
body	bod	k1gInPc4	bod
<g/>
)	)	kIx)	)
-	-	kIx~	-
počátek	počátek	k1gInSc1	počátek
mechanického	mechanický	k2eAgInSc2d1	mechanický
systému	systém	k1gInSc2	systém
přenosu	přenos	k1gInSc2	přenos
(	(	kIx(	(
<g/>
předvedeno	předvést	k5eAaPmNgNnS	předvést
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Braun	Braun	k1gMnSc1	Braun
katodový	katodový	k2eAgInSc4d1	katodový
oscilograf	oscilograf	k1gInSc4	oscilograf
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Kosma	Kosma	k1gMnSc1	Kosma
Zworykin	Zworykin	k1gMnSc1	Zworykin
snímací	snímací	k2eAgFnSc4d1	snímací
elektronku	elektronka	k1gFnSc4	elektronka
-	-	kIx~	-
ikonoskop	ikonoskop	k1gInSc4	ikonoskop
přijímací	přijímací	k2eAgFnSc2d1	přijímací
elektronky	elektronka	k1gFnSc2	elektronka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
televizní	televizní	k2eAgFnSc1d1	televizní
společnost	společnost	k1gFnSc1	společnost
Television	Television	k1gInSc1	Television
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
založil	založit	k5eAaPmAgMnS	založit
John	John	k1gMnSc1	John
Baird	Baird	k1gMnSc1	Baird
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
předvedl	předvést	k5eAaPmAgMnS	předvést
členům	člen	k1gMnPc3	člen
Royal	Royal	k1gMnSc1	Royal
Institutu	institut	k1gInSc6	institut
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanický	mechanický	k2eAgInSc1d1	mechanický
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
nespolehlivý	spolehlivý	k2eNgInSc1d1	nespolehlivý
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
rychlost	rychlost	k1gFnSc4	rychlost
otáčení	otáčení	k1gNnSc2	otáčení
kotouče	kotouč	k1gInSc2	kotouč
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získává	získávat	k5eAaImIp3nS	získávat
převahu	převaha	k1gFnSc4	převaha
elektronický	elektronický	k2eAgInSc4d1	elektronický
princip	princip	k1gInSc4	princip
<g/>
.	.	kIx.	.
</s>
<s>
Mechanický	mechanický	k2eAgMnSc1d1	mechanický
dokázal	dokázat	k5eAaPmAgMnS	dokázat
přenést	přenést	k5eAaPmF	přenést
1000	[number]	k4	1000
bodů	bod	k1gInPc2	bod
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Elektronický	elektronický	k2eAgInSc1d1	elektronický
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
až	až	k9	až
12,5	[number]	k4	12,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
bodů	bod	k1gInPc2	bod
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
probíhají	probíhat	k5eAaImIp3nP	probíhat
první	první	k4xOgFnSc4	první
experimentální	experimentální	k2eAgFnSc4d1	experimentální
TV	TV	kA	TV
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1935	[number]	k4	1935
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
vůbec	vůbec	k9	vůbec
první	první	k4xOgNnSc4	první
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Vysílalo	vysílat	k5eAaImAgNnS	vysílat
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1936	[number]	k4	1936
začíná	začínat	k5eAaImIp3nS	začínat
pravidelně	pravidelně	k6eAd1	pravidelně
vysílat	vysílat	k5eAaImF	vysílat
BBC	BBC	kA	BBC
(	(	kIx(	(
<g/>
na	na	k7c4	na
300	[number]	k4	300
majitelů	majitel	k1gMnPc2	majitel
TV	TV	kA	TV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
prostředek	prostředek	k1gInSc1	prostředek
masové	masový	k2eAgFnSc2d1	masová
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
doc.	doc.	kA	doc.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šafránek	Šafránek	k1gMnSc1	Šafránek
(	(	kIx(	(
<g/>
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
ústav	ústav	k1gInSc1	ústav
UK	UK	kA	UK
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
předvádí	předvádět	k5eAaImIp3nS	předvádět
přijímací	přijímací	k2eAgFnSc4d1	přijímací
televizní	televizní	k2eAgFnSc4d1	televizní
aparaturu	aparatura	k1gFnSc4	aparatura
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
30	[number]	k4	30
řádků	řádek	k1gInPc2	řádek
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
mechanického	mechanický	k2eAgInSc2d1	mechanický
rozkladu	rozklad	k1gInSc2	rozklad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
rozhlasové	rozhlasový	k2eAgFnSc6d1	rozhlasová
výstavě	výstava	k1gFnSc6	výstava
(	(	kIx(	(
<g/>
MEVRO	Mevro	k1gNnSc1	Mevro
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
prezentován	prezentován	k2eAgInSc1d1	prezentován
úplný	úplný	k2eAgInSc1d1	úplný
kamerový	kamerový	k2eAgInSc1d1	kamerový
řetězec	řetězec	k1gInSc1	řetězec
v	v	k7c6	v
elektronickém	elektronický	k2eAgInSc6d1	elektronický
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
vyrábět	vyrábět	k5eAaImF	vyrábět
televizní	televizní	k2eAgInSc4d1	televizní
přijímač	přijímač	k1gInSc4	přijímač
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
1	[number]	k4	1
<g/>
.	.	kIx.	.
přímý	přímý	k2eAgInSc1d1	přímý
televizní	televizní	k2eAgInSc1d1	televizní
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
XI	XI	kA	XI
<g/>
.	.	kIx.	.
všesokolského	všesokolský	k2eAgInSc2d1	všesokolský
sletu	slet	k1gInSc2	slet
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Strahově	Strahov	k1gInSc6	Strahov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
začíná	začínat	k5eAaImIp3nS	začínat
první	první	k4xOgNnSc4	první
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
zkušební	zkušební	k2eAgNnSc4d1	zkušební
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnPc1	studio
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
1961	[number]	k4	1961
má	mít	k5eAaImIp3nS	mít
koncesi	koncese	k1gFnSc4	koncese
1	[number]	k4	1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
majitelů	majitel	k1gMnPc2	majitel
televizních	televizní	k2eAgInPc2d1	televizní
přijímačů	přijímač	k1gInPc2	přijímač
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
1,5	[number]	k4	1,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
majitelů	majitel	k1gMnPc2	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
vysílá	vysílat	k5eAaImIp3nS	vysílat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
3	[number]	k4	3
dny	den	k1gInPc7	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
4	[number]	k4	4
dny	den	k1gInPc7	den
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
5	[number]	k4	5
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
7	[number]	k4	7
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
je	být	k5eAaImIp3nS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
první	první	k4xOgNnSc4	první
televizní	televizní	k2eAgNnSc4d1	televizní
reklamní	reklamní	k2eAgNnSc4d1	reklamní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
začíná	začínat	k5eAaImIp3nS	začínat
vysílat	vysílat	k5eAaImF	vysílat
druhý	druhý	k4xOgInSc1	druhý
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
je	být	k5eAaImIp3nS	být
spuštěno	spuštěn	k2eAgNnSc4d1	spuštěno
barevné	barevný	k2eAgNnSc4d1	barevné
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
provozováno	provozován	k2eAgNnSc4d1	provozováno
digitální	digitální	k2eAgNnSc4d1	digitální
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
standard	standard	k1gInSc1	standard
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
od	od	k7c2	od
třicetiřádkového	třicetiřádkový	k2eAgInSc2d1	třicetiřádkový
obrazu	obraz	k1gInSc2	obraz
(	(	kIx(	(
<g/>
Telehor	Telehor	k1gInSc1	Telehor
30	[number]	k4	30
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
francouzský	francouzský	k2eAgInSc4d1	francouzský
standard	standard	k1gInSc4	standard
HDTV	HDTV	kA	HDTV
(	(	kIx(	(
<g/>
1080	[number]	k4	1080
řádků	řádek	k1gInPc2	řádek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
dodnes	dodnes	k6eAd1	dodnes
používaným	používaný	k2eAgInSc7d1	používaný
standardem	standard	k1gInSc7	standard
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
NTSC	NTSC	kA	NTSC
(	(	kIx(	(
<g/>
525	[number]	k4	525
řádků	řádek	k1gInPc2	řádek
<g/>
,	,	kIx,	,
29,97	[number]	k4	29,97
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
prokládaný	prokládaný	k2eAgMnSc1d1	prokládaný
<g/>
)	)	kIx)	)
existující	existující	k2eAgFnPc1d1	existující
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
norma	norma	k1gFnSc1	norma
625	[number]	k4	625
řádek	řádka	k1gFnPc2	řádka
<g/>
,	,	kIx,	,
25	[number]	k4	25
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
prokládané	prokládaný	k2eAgFnPc1d1	prokládaná
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
PAL	pal	k1gInSc1	pal
a	a	k8xC	a
starším	starý	k2eAgMnSc7d2	starší
SECAM	SECAM	kA	SECAM
pro	pro	k7c4	pro
barevný	barevný	k2eAgInSc4d1	barevný
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
DVB-T	DVB-T	k?	DVB-T
Digitální	digitální	k2eAgNnSc1d1	digitální
pozemní	pozemní	k2eAgNnSc1d1	pozemní
vysílání	vysílání	k1gNnSc1	vysílání
DVB-S	DVB-S	k1gFnSc2	DVB-S
Digitální	digitální	k2eAgNnSc1d1	digitální
satelitní	satelitní	k2eAgNnSc1d1	satelitní
vysílání	vysílání	k1gNnSc1	vysílání
DVB-C	DVB-C	k1gFnSc2	DVB-C
Digitální	digitální	k2eAgNnSc1d1	digitální
vysílání	vysílání	k1gNnSc1	vysílání
přes	přes	k7c4	přes
televizní	televizní	k2eAgInSc4d1	televizní
kabelový	kabelový	k2eAgInSc4d1	kabelový
rozvod	rozvod	k1gInSc4	rozvod
IPTV	IPTV	kA	IPTV
Digitální	digitální	k2eAgNnSc1d1	digitální
vysílání	vysílání	k1gNnSc1	vysílání
přes	přes	k7c4	přes
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
síť	síť	k1gFnSc4	síť
Internetové	internetový	k2eAgNnSc1d1	internetové
streamování	streamování	k1gNnSc1	streamování
-	-	kIx~	-
možnost	možnost	k1gFnSc1	možnost
sledovat	sledovat	k5eAaImF	sledovat
televizi	televize	k1gFnSc4	televize
přes	přes	k7c4	přes
běžný	běžný	k2eAgInSc4d1	běžný
internetový	internetový	k2eAgInSc4d1	internetový
prohlížeč	prohlížeč	k1gInSc4	prohlížeč
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
začaly	začít	k5eAaPmAgFnP	začít
USA	USA	kA	USA
a	a	k8xC	a
Británie	Británie	k1gFnSc2	Británie
vysílat	vysílat	k5eAaImF	vysílat
terestriálně	terestriálně	k6eAd1	terestriálně
digitálně	digitálně	k6eAd1	digitálně
(	(	kIx(	(
<g/>
DVB-T	DVB-T	k1gFnSc1	DVB-T
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
digitálně	digitálně	k6eAd1	digitálně
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
satelitů	satelit	k1gMnPc2	satelit
(	(	kIx(	(
<g/>
DVB-S	DVB-S	k1gMnPc2	DVB-S
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přes	přes	k7c4	přes
kabel	kabel	k1gInSc4	kabel
(	(	kIx(	(
<g/>
DVB-C	DVB-C	k1gFnSc4	DVB-C
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
bylo	být	k5eAaImAgNnS	být
digitální	digitální	k2eAgNnSc1d1	digitální
vysílání	vysílání	k1gNnSc1	vysílání
(	(	kIx(	(
<g/>
DVB-T	DVB-T	k1gFnPc1	DVB-T
<g/>
)	)	kIx)	)
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Analogové	analogový	k2eAgNnSc4d1	analogové
pozemní	pozemní	k2eAgNnSc4d1	pozemní
vysílání	vysílání	k1gNnSc4	vysílání
Analogové	analogový	k2eAgNnSc4d1	analogové
satelitní	satelitní	k2eAgNnSc4d1	satelitní
vysílání	vysílání	k1gNnSc4	vysílání
Monoskop	monoskop	k1gInSc1	monoskop
Televizní	televizní	k2eAgFnSc1d1	televizní
norma	norma	k1gFnSc1	norma
</s>
