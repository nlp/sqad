<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midway	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midway	k1gFnSc2
<g/>
;	;	kIx,
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
ミ	ミ	k?
<g/>
,	,	kIx,
Middové	Middové	k2eAgInSc1d1
Kaisen	Kaisen	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
významná	významný	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
Tichomořského	tichomořský	k2eAgNnSc2d1
bojiště	bojiště	k1gNnSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
během	během	k7c2
3	#num#	k4
<g/>
.	.	kIx.
až	až	k9
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
<g />
.	.	kIx.
</s>