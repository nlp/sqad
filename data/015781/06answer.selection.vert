<s>
Americké	americký	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
admirálů	admirál	k1gMnPc2
Chestera	Chester	k1gMnSc2
W.	W.	kA
Nimitze	Nimitz	k1gFnSc2
<g/>
,	,	kIx,
Franka	Frank	k1gMnSc2
J.	J.	kA
Fletchera	Fletcher	k1gMnSc2
a	a	k8xC
Raymonda	Raymond	k1gMnSc2
A.	A.	kA
Spruance	Spruance	k1gMnSc2
porazilo	porazit	k5eAaPmAgNnS
útočící	útočící	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
japonského	japonský	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
admirálů	admirál	k1gMnPc2
Isoroku	Isoroku	k1gInSc2
Jamamota	Jamamoto	k1gMnSc2
<g/>
,	,	kIx,
Čúiči	Čúiči	k1gInPc7
Naguma	Nagumo	k1gNnSc2
a	a	k8xC
Nobutake	Nobutake	k1gNnSc2
Kondóa	Kondó	k1gInSc2
poblíž	poblíž	k7c2
atolu	atol	k1gInSc2
Midway	Midway	k1gMnSc2
a	a	k8xC
způsobilo	způsobit	k5eAaPmAgNnS
japonským	japonský	k2eAgFnPc3d1
letadlovým	letadlový	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
zničující	zničující	k2eAgFnSc2d1
škody	škoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
