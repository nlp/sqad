<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Americké	americký	k2eAgInPc1d1
střemhlavé	střemhlavý	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
SBD-3	SBD-3	k1gFnPc2
Dauntless	Dauntlessa	k1gFnPc2
eskadry	eskadra	k1gFnSc2
VS-8	VS-8	k1gMnPc2
z	z	k7c2
USS	USS	kA
Hornet	Hornet	k1gInSc1
nad	nad	k7c7
hořícím	hořící	k2eAgInSc7d1
japonským	japonský	k2eAgInSc7d1
křižníkem	křižník	k1gInSc7
Mikuma	Mikum	k1gMnSc2
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
západně	západně	k6eAd1
od	od	k7c2
atolu	atol	k1gInSc2
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
Tichý	tichý	k2eAgInSc1d1
oceán	oceán	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Americké	americký	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Isoroku	Isorok	k1gInSc2
JamamotoČúiči	JamamotoČúič	k1gMnPc1
NagumoTamon	NagumoTamon	k1gInSc4
Jamaguči	Jamaguč	k1gInSc3
<g/>
†	†	k?
<g/>
Nobutake	Nobutak	k1gMnSc2
Kondó	Kondó	k1gMnSc2
</s>
<s>
Chester	Chester	k1gMnSc1
NimitzFrank	NimitzFrank	k1gMnSc1
J.	J.	kA
FletcherRaymond	FletcherRaymond	k1gMnSc1
A.	A.	kA
Spruance	Spruance	k1gFnSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
Kidó	Kidó	k?
butai	butai	k1gNnSc1
+	+	kIx~
doprovod	doprovod	k1gInSc1
<g/>
:	:	kIx,
<g/>
4	#num#	k4
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
248	#num#	k4
palubních	palubní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
rezerv	rezerva	k1gFnPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
těžké	těžký	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
<g/>
1	#num#	k4
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
16	#num#	k4
hydroplánů	hydroplán	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
11	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
+	+	kIx~
vzdálené	vzdálený	k2eAgNnSc4d1
krytí	krytí	k1gNnSc4
<g/>
+	+	kIx~
invazní	invazní	k2eAgFnPc1d1
a	a	k8xC
podpůrné	podpůrný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
<g/>
+	+	kIx~
15	#num#	k4
ponorek	ponorka	k1gFnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
TF16	TF16	k4
a	a	k8xC
TF	tf	k0wR
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
234	#num#	k4
palubních	palubní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
7	#num#	k4
těžkých	těžký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
14	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
až	až	k9
20	#num#	k4
ponorek	ponorka	k1gFnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
Midway	Midwaa	k1gFnSc2
<g/>
:	:	kIx,
<g/>
32	#num#	k4
hydroplánů	hydroplán	k1gInPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
94	#num#	k4
pozemních	pozemní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
+	+	kIx~
zásobovací	zásobovací	k2eAgFnPc1d1
a	a	k8xC
podpůrné	podpůrný	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
4	#num#	k4
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
1	#num#	k4
těžký	těžký	k2eAgInSc4d1
křižník	křižník	k1gInSc4
<g/>
248	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
8	#num#	k4
hydroplánů	hydroplán	k1gInPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
3057	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
1	#num#	k4
torpédoborecasi	torpédoborecas	k1gMnPc1
150	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
307	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midway	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midway	k1gFnSc2
<g/>
;	;	kIx,
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
ミ	ミ	k?
<g/>
,	,	kIx,
Middové	Middové	k2eAgInSc1d1
Kaisen	Kaisen	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
významná	významný	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
Tichomořského	tichomořský	k2eAgNnSc2d1
bojiště	bojiště	k1gNnSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
během	během	k7c2
3	#num#	k4
<g/>
.	.	kIx.
až	až	k9
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
po	po	k7c6
japonském	japonský	k2eAgInSc6d1
útoku	útok	k1gInSc6
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gFnPc2
a	a	k8xC
měsíc	měsíc	k1gInSc4
po	po	k7c6
bitvě	bitva	k1gFnSc6
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americké	americký	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
admirálů	admirál	k1gMnPc2
Chestera	Chester	k1gMnSc2
W.	W.	kA
Nimitze	Nimitz	k1gFnSc2
<g/>
,	,	kIx,
Franka	Frank	k1gMnSc2
J.	J.	kA
Fletchera	Fletcher	k1gMnSc2
a	a	k8xC
Raymonda	Raymond	k1gMnSc2
A.	A.	kA
Spruance	Spruance	k1gMnSc2
porazilo	porazit	k5eAaPmAgNnS
útočící	útočící	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
japonského	japonský	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
admirálů	admirál	k1gMnPc2
Isoroku	Isoroku	k1gInSc2
Jamamota	Jamamoto	k1gMnSc2
<g/>
,	,	kIx,
Čúiči	Čúiči	k1gInPc7
Naguma	Nagumo	k1gNnSc2
a	a	k8xC
Nobutake	Nobutake	k1gNnSc2
Kondóa	Kondó	k1gInSc2
poblíž	poblíž	k7c2
atolu	atol	k1gInSc2
Midway	Midway	k1gMnSc2
a	a	k8xC
způsobilo	způsobit	k5eAaPmAgNnS
japonským	japonský	k2eAgFnPc3d1
letadlovým	letadlový	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
zničující	zničující	k2eAgFnSc2d1
škody	škoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
MI	já	k3xPp1nSc3
(	(	kIx(
<g/>
MI作	MI作	k1gFnSc1
MI	já	k3xPp1nSc3
sakusen	sakusen	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
dřívější	dřívější	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
za	za	k7c4
cíl	cíl	k1gInSc4
eliminovat	eliminovat	k5eAaBmF
strategický	strategický	k2eAgInSc4d1
vliv	vliv	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
Japonskému	japonský	k2eAgInSc3d1
císařství	císařství	k1gNnSc1
poskytlo	poskytnout	k5eAaPmAgNnS
volnou	volný	k2eAgFnSc4d1
ruku	ruka	k1gFnSc4
při	při	k7c6
budování	budování	k1gNnSc6
své	svůj	k3xOyFgFnSc2
Velké	velký	k2eAgFnSc2d1
východoasijské	východoasijský	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
vzájemné	vzájemný	k2eAgFnSc2d1
prosperity	prosperita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
doufali	doufat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
další	další	k2eAgFnSc1d1
demoralizující	demoralizující	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
donutí	donutit	k5eAaPmIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
k	k	k7c3
uzavření	uzavření	k1gNnSc3
míru	mír	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
zajistí	zajistit	k5eAaPmIp3nS
japonskou	japonský	k2eAgFnSc4d1
dominanci	dominance	k1gFnSc4
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přilákání	přilákání	k1gNnPc1
amerických	americký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
do	do	k7c2
pasti	past	k1gFnSc2
a	a	k8xC
obsazení	obsazení	k1gNnSc1
Midway	Midwaa	k1gFnSc2
tvořilo	tvořit	k5eAaImAgNnS
součást	součást	k1gFnSc4
celkové	celkový	k2eAgFnSc2d1
strategie	strategie	k1gFnSc2
„	„	k?
<g/>
bariéry	bariéra	k1gFnSc2
<g/>
“	“	k?
rozšiřující	rozšiřující	k2eAgInSc1d1
japonský	japonský	k2eAgInSc1d1
obranný	obranný	k2eAgInSc1d1
perimetr	perimetr	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
reakce	reakce	k1gFnSc1
na	na	k7c4
Doolittlův	Doolittlův	k2eAgInSc4d1
nálet	nálet	k1gInSc4
na	na	k7c4
Tokio	Tokio	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
také	také	k9
krokem	krok	k1gInSc7
k	k	k7c3
dalším	další	k2eAgFnPc3d1
ofenzivám	ofenziva	k1gFnPc3
proti	proti	k7c3
Fidži	Fidž	k1gFnSc3
<g/>
,	,	kIx,
Samoi	Samoa	k1gFnSc3
a	a	k8xC
případně	případně	k6eAd1
Havajským	havajský	k2eAgInPc3d1
ostrovům	ostrov	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
plán	plán	k1gInSc1
měl	mít	k5eAaImAgInS
nedostatky	nedostatek	k1gInPc4
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgInSc4d1
chybný	chybný	k2eAgInSc4d1
odhad	odhad	k1gInSc4
americké	americký	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
a	a	k8xC
špatné	špatný	k2eAgNnSc1d1
počáteční	počáteční	k2eAgNnSc1d1
rozmístění	rozmístění	k1gNnSc1
japonských	japonský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodujícím	rozhodující	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
američtí	americký	k2eAgMnPc1d1
kryptoanalytici	kryptoanalytik	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
určit	určit	k5eAaPmF
datum	datum	k1gNnSc4
a	a	k8xC
místo	místo	k1gNnSc4
plánovaného	plánovaný	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožnilo	umožnit	k5eAaPmAgNnS
předem	předem	k6eAd1
varovanému	varovaný	k2eAgNnSc3d1
americkému	americký	k2eAgNnSc3d1
loďstvu	loďstvo	k1gNnSc3
připravit	připravit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
léčku	léčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitvy	bitva	k1gFnPc1
se	se	k3xPyFc4
zúčastnily	zúčastnit	k5eAaPmAgFnP
čtyři	čtyři	k4xCgFnPc1
japonské	japonský	k2eAgFnPc1d1
a	a	k8xC
tři	tři	k4xCgFnPc1
americké	americký	k2eAgFnPc1d1
těžké	těžký	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
měli	mít	k5eAaImAgMnP
navíc	navíc	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
celkem	celkem	k6eAd1
126	#num#	k4
letadel	letadlo	k1gNnPc2
z	z	k7c2
Midwaye	Midway	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
byly	být	k5eAaImAgFnP
čtyři	čtyři	k4xCgFnPc1
japonské	japonský	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
–	–	k?
Akagi	Akagi	k1gNnSc1
<g/>
,	,	kIx,
Kaga	Kaga	k1gFnSc1
<g/>
,	,	kIx,
Sórjú	Sórjú	k1gFnPc1
a	a	k8xC
Hirjú	Hirjú	k1gFnPc1
potopeny	potopen	k2eAgFnPc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
těžký	těžký	k2eAgInSc4d1
křižník	křižník	k1gInSc4
Mikuma	Mikumum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
ztratily	ztratit	k5eAaPmAgInP
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
USS	USS	kA
Yorktown	Yorktown	k1gNnSc1
a	a	k8xC
torpédoborec	torpédoborec	k1gInSc1
USS	USS	kA
Hammann	Hammann	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Midway	Midwaa	k1gFnSc2
a	a	k8xC
vyčerpávajících	vyčerpávající	k2eAgInPc6d1
bojích	boj	k1gInPc6
o	o	k7c4
Šalamounovy	Šalamounův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
přestalo	přestat	k5eAaPmAgNnS
být	být	k5eAaImF
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
schopné	schopný	k2eAgNnSc1d1
rychle	rychle	k6eAd1
nahrazovat	nahrazovat	k5eAaImF
ztracený	ztracený	k2eAgInSc4d1
materiál	materiál	k1gInSc4
(	(	kIx(
<g/>
zejména	zejména	k9
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
muže	muž	k1gMnSc4
(	(	kIx(
<g/>
zejména	zejména	k9
vycvičené	vycvičený	k2eAgFnPc1d1
piloty	pilota	k1gFnPc1
a	a	k8xC
technický	technický	k2eAgInSc1d1
personál	personál	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
díky	díky	k7c3
svým	svůj	k3xOyFgFnPc3
ohromným	ohromný	k2eAgFnPc3d1
průmyslovým	průmyslový	k2eAgFnPc3d1
a	a	k8xC
výcvikovým	výcvikový	k2eAgFnPc3d1
kapacitám	kapacita	k1gFnPc3
<g/>
,	,	kIx,
nahradily	nahradit	k5eAaPmAgFnP
ztráty	ztráta	k1gFnPc1
mnohem	mnohem	k6eAd1
snáze	snadno	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
o	o	k7c4
Midway	Midwa	k1gMnPc4
je	být	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
Guadalcanalskou	Guadalcanalský	k2eAgFnSc7d1
kampaní	kampaň	k1gFnSc7
obecně	obecně	k6eAd1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
bod	bod	k1gInSc4
zvratu	zvrat	k1gInSc2
ve	v	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
následujících	následující	k2eAgInPc6d1
asi	asi	k9
šedesát	šedesát	k4xCc1
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
bitva	bitva	k1gFnSc1
líčením	líčení	k1gNnSc7
západních	západní	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
a	a	k8xC
japonského	japonský	k2eAgMnSc2d1
letce	letec	k1gMnSc2
Micuo	Micuo	k6eAd1
Fučidy	Fučid	k1gInPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
bitvu	bitva	k1gFnSc4
popisovali	popisovat	k5eAaImAgMnP
převážně	převážně	k6eAd1
z	z	k7c2
amerického	americký	k2eAgInSc2d1
úhlu	úhel	k1gInSc2
pohledu	pohled	k1gInSc2
a	a	k8xC
v	v	k7c6
zásadě	zásada	k1gFnSc6
bez	bez	k7c2
znalosti	znalost	k1gFnSc2
japonských	japonský	k2eAgInPc2d1
postupů	postup	k1gInPc2
a	a	k8xC
pramenů	pramen	k1gInPc2
<g/>
,	,	kIx,
zahalena	zahalen	k2eAgFnSc1d1
do	do	k7c2
legend	legenda	k1gFnPc2
<g/>
,	,	kIx,
polopravd	polopravda	k1gFnPc2
a	a	k8xC
pohádek	pohádka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Pozdější	pozdní	k2eAgInPc1d2
výzkumy	výzkum	k1gInPc1
(	(	kIx(
<g/>
Lundstrom	Lundstrom	k1gInSc1
<g/>
,	,	kIx,
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
Parshall	Parshall	k1gMnSc1
a	a	k8xC
Tully	Tulla	k1gFnPc1
<g/>
)	)	kIx)
ale	ale	k8xC
ukázaly	ukázat	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
bitva	bitva	k1gFnSc1
odehrávala	odehrávat	k5eAaImAgFnS
na	na	k7c6
japonské	japonský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
jak	jak	k8xC,k8xS
ji	on	k3xPp3gFnSc4
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
západní	západní	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
popisovali	popisovat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Rozsah	rozsah	k1gInSc1
japonské	japonský	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
expanze	expanze	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
,	,	kIx,
duben	duben	k1gInSc4
1942	#num#	k4
</s>
<s>
Po	po	k7c6
zahájení	zahájení	k1gNnSc6
války	válka	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
v	v	k7c6
prosinci	prosinec	k1gInSc6
1941	#num#	k4
se	se	k3xPyFc4
Japonskému	japonský	k2eAgInSc3d1
císařství	císařství	k1gNnPc4
podařilo	podařit	k5eAaPmAgNnS
rychle	rychle	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
svých	svůj	k3xOyFgInPc2
počátečních	počáteční	k2eAgInPc2d1
strategických	strategický	k2eAgInPc2d1
cílů	cíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsadilo	obsadit	k5eAaPmAgNnS
Filipíny	Filipíny	k1gFnPc4
<g/>
,	,	kIx,
Malajsko	Malajsko	k1gNnSc4
<g/>
,	,	kIx,
Singapur	Singapur	k1gInSc4
a	a	k8xC
především	především	k9
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc4d1
Indonésii	Indonésie	k1gFnSc4
<g/>
)	)	kIx)
s	s	k7c7
jejími	její	k3xOp3gNnPc7
rozsáhlými	rozsáhlý	k2eAgNnPc7d1
ropnými	ropný	k2eAgNnPc7d1
nalezišti	naleziště	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
pro	pro	k7c4
Japonce	Japonec	k1gMnPc4
životně	životně	k6eAd1
důležitá	důležitý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
rychlému	rychlý	k2eAgInSc3d1
postupu	postup	k1gInSc3
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
předběžné	předběžný	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
druhé	druhý	k4xOgFnSc2
fáze	fáze	k1gFnSc2
operací	operace	k1gFnPc2
zahájeno	zahájit	k5eAaPmNgNnS
již	již	k9
v	v	k7c6
lednu	leden	k1gInSc6
1942	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
neshodám	neshoda	k1gFnPc3
mezi	mezi	k7c7
císařskou	císařský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
a	a	k8xC
císařským	císařský	k2eAgNnSc7d1
námořnictvem	námořnictvo	k1gNnSc7
ohledně	ohledně	k7c2
další	další	k2eAgFnSc2d1
strategie	strategie	k1gFnSc2
a	a	k8xC
sporům	spor	k1gInPc3
mezi	mezi	k7c7
námořním	námořní	k2eAgInSc7d1
generálním	generální	k2eAgInSc7d1
štábem	štáb	k1gInSc7
(	(	kIx(
<g/>
軍	軍	k?
Gunreibu	Gunreib	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
Spojeným	spojený	k2eAgNnSc7d1
loďstvem	loďstvo	k1gNnSc7
(	(	kIx(
<g/>
連	連	k?
Rengó	Rengó	k1gFnSc1
Kantai	Kanta	k1gFnSc2
<g/>
)	)	kIx)
taišó	taišó	k?
(	(	kIx(
<g/>
大	大	k?
~	~	kIx~
admirál	admirál	k1gMnSc1
<g/>
)	)	kIx)
Isoroku	Isorok	k1gInSc2
Jamamota	Jamamota	k1gFnSc1
byl	být	k5eAaImAgInS
příští	příští	k2eAgInSc1d1
strategický	strategický	k2eAgInSc1d1
plán	plán	k1gInSc1
vytvořen	vytvořit	k5eAaPmNgInS
teprve	teprve	k6eAd1
v	v	k7c6
dubnu	duben	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
1942	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Taišó	Taišó	k1gFnSc1
Jamamoto	Jamamota	k1gFnSc5
nakonec	nakonec	k6eAd1
v	v	k7c6
byrokratickém	byrokratický	k2eAgInSc6d1
boji	boj	k1gInSc6
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
,	,	kIx,
díky	díky	k7c3
lehce	lehko	k6eAd1
zastřené	zastřený	k2eAgFnSc3d1
hrozbě	hrozba	k1gFnSc3
rezignace	rezignace	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
jeho	jeho	k3xOp3gInSc1
plán	plán	k1gInSc1
pro	pro	k7c4
střední	střední	k2eAgInSc4d1
Pacifik	Pacifik	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taišó	Taišó	k?
Isoroku	Isorok	k1gInSc2
Jamamoto	Jamamota	k1gFnSc5
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
a	a	k8xC
duchovní	duchovní	k2eAgMnSc1d1
otec	otec	k1gMnSc1
operace	operace	k1gFnSc2
</s>
<s>
Jamamotovým	Jamamotův	k2eAgInSc7d1
prvořadým	prvořadý	k2eAgInSc7d1
strategickým	strategický	k2eAgInSc7d1
cílem	cíl	k1gInSc7
byla	být	k5eAaImAgFnS
eliminace	eliminace	k1gFnSc1
amerických	americký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
hlavní	hlavní	k2eAgFnSc4d1
hrozbu	hrozba	k1gFnSc4
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
tichomořskou	tichomořský	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
obavy	obava	k1gFnPc4
značně	značně	k6eAd1
posílil	posílit	k5eAaPmAgInS
Doolittlův	Doolittlův	k2eAgInSc1d1
nálet	nálet	k1gInSc1
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
16	#num#	k4
bombardérů	bombardér	k1gInPc2
B-25B	B-25B	k1gFnSc2
Mitchell	Mitchella	k1gFnPc2
amerického	americký	k2eAgNnSc2d1
armádního	armádní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
(	(	kIx(
<g/>
USAAF	USAAF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
odstartovaly	odstartovat	k5eAaPmAgFnP
z	z	k7c2
letadlové	letadlový	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
USS	USS	kA
Hornet	Hornet	k1gMnSc1
<g/>
,	,	kIx,
bombardovalo	bombardovat	k5eAaImAgNnS
cíle	cíl	k1gInPc4
v	v	k7c6
Tokiu	Tokio	k1gNnSc6
a	a	k8xC
několika	několik	k4yIc7
dalších	další	k2eAgNnPc6d1
japonských	japonský	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nálet	nálet	k1gInSc1
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
z	z	k7c2
vojenského	vojenský	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
bezvýznamný	bezvýznamný	k2eAgMnSc1d1
<g/>
,	,	kIx,
japonské	japonský	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
šokoval	šokovat	k5eAaBmAgMnS
a	a	k8xC
odhalil	odhalit	k5eAaPmAgMnS
existenci	existence	k1gFnSc4
mezery	mezera	k1gFnSc2
v	v	k7c6
obraně	obrana	k1gFnSc6
japonských	japonský	k2eAgInPc2d1
domácích	domácí	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
a	a	k8xC
také	také	k9
zranitelnost	zranitelnost	k1gFnSc4
japonského	japonský	k2eAgNnSc2d1
území	území	k1gNnSc2
vůči	vůči	k7c3
americkým	americký	k2eAgInPc3d1
bombardérům	bombardér	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tento	tento	k3xDgMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
úspěšné	úspěšný	k2eAgInPc4d1
nájezdy	nájezd	k1gInPc4
amerických	americký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
v	v	k7c6
jihozápadním	jihozápadní	k2eAgInSc6d1
Pacifiku	Pacifik	k1gInSc6
ve	v	k7c6
stylu	styl	k1gInSc6
„	„	k?
<g/>
udeř	udeřit	k5eAaPmRp2nS
a	a	k8xC
uteč	utéct	k5eAaPmRp2nS
<g/>
“	“	k?
prokázaly	prokázat	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
americké	americký	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
stále	stále	k6eAd1
představují	představovat	k5eAaImIp3nP
hrozbu	hrozba	k1gFnSc4
<g/>
,	,	kIx,
přestože	přestože	k8xS
se	se	k3xPyFc4
zdánlivě	zdánlivě	k6eAd1
vyhýbaly	vyhýbat	k5eAaImAgFnP
zavlečení	zavlečení	k1gNnSc4
do	do	k7c2
generální	generální	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Jamamoto	Jamamota	k1gFnSc5
usoudil	usoudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
další	další	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
útok	útok	k1gInSc1
na	na	k7c4
hlavní	hlavní	k2eAgFnSc4d1
americkou	americký	k2eAgFnSc4d1
námořní	námořní	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
v	v	k7c6
Pearl	Pearla	k1gFnPc2
Harboru	Harbor	k1gInSc2
by	by	kYmCp3nS
přiměl	přimět	k5eAaPmAgInS
americké	americký	k2eAgFnPc4d1
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
k	k	k7c3
vyplutí	vyplutí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
však	však	k9
opakovaný	opakovaný	k2eAgInSc4d1
přímý	přímý	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gInSc1
zavrhl	zavrhnout	k5eAaPmAgInS
jako	jako	k9
příliš	příliš	k6eAd1
riskantní	riskantní	k2eAgMnSc1d1
<g/>
,	,	kIx,
vzhledem	vzhled	k1gInSc7
k	k	k7c3
posílení	posílení	k1gNnSc3
amerických	americký	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
na	na	k7c6
Havajských	havajský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
od	od	k7c2
útoku	útok	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jamamoto	Jamamota	k1gFnSc5
místo	místo	k7c2
toho	ten	k3xDgMnSc4
zvolil	zvolit	k5eAaPmAgMnS
Midway	Midwa	k2eAgMnPc4d1
<g/>
,	,	kIx,
malý	malý	k2eAgInSc4d1
atol	atol	k1gInSc4
na	na	k7c6
severozápadním	severozápadní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
řetězu	řetěz	k1gInSc2
Havajských	havajský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
1100	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
2100	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
)	)	kIx)
od	od	k7c2
Oahu	Oahus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Midway	Midway	k1gInPc4
byl	být	k5eAaImAgInS
mimo	mimo	k7c4
efektivní	efektivní	k2eAgInSc4d1
dosah	dosah	k1gInSc4
téměř	téměř	k6eAd1
všech	všecek	k3xTgNnPc2
amerických	americký	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
umístěných	umístěný	k2eAgNnPc2d1
na	na	k7c6
hlavních	hlavní	k2eAgInPc6d1
Havajských	havajský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
širším	široký	k2eAgNnSc6d2
schématu	schéma	k1gNnSc6
japonských	japonský	k2eAgInPc2d1
záměrů	záměr	k1gInPc2
nebyl	být	k5eNaImAgInS
atol	atol	k1gInSc1
zvlášť	zvlášť	k6eAd1
významný	významný	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
Japonci	Japonec	k1gMnPc1
cítili	cítit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Američané	Američan	k1gMnPc1
Midway	Midwaa	k1gFnSc2
považují	považovat	k5eAaImIp3nP
za	za	k7c4
důležitý	důležitý	k2eAgInSc4d1
odrazový	odrazový	k2eAgInSc4d1
můstek	můstek	k1gInSc4
k	k	k7c3
Pearl	Pearlum	k1gNnPc2
Harboru	Harbora	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
jej	on	k3xPp3gMnSc4
budou	být	k5eAaImBp3nP
zuřivě	zuřivě	k6eAd1
bránit	bránit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Američané	Američan	k1gMnPc1
si	se	k3xPyFc3
skutečně	skutečně	k6eAd1
uvědomovali	uvědomovat	k5eAaImAgMnP
význam	význam	k1gInSc4
Midwaye	Midway	k1gFnSc2
<g/>
:	:	kIx,
po	po	k7c6
bitvě	bitva	k1gFnSc6
tam	tam	k6eAd1
vybudovali	vybudovat	k5eAaPmAgMnP
ponorkovou	ponorkový	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgInPc4d1
ponorkám	ponorka	k1gFnPc3
operujícím	operující	k2eAgInSc6d1
z	z	k7c2
Pearl	Pearla	k1gFnPc2
Harboru	Harbor	k1gInSc2
doplnit	doplnit	k5eAaPmF
palivo	palivo	k1gNnSc4
a	a	k8xC
zásoby	zásoba	k1gFnPc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnSc4
operační	operační	k2eAgInSc1d1
rádius	rádius	k1gInSc1
zvětšil	zvětšit	k5eAaPmAgInS
o	o	k7c4
asi	asi	k9
1000	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
1852	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atol	atol	k1gInSc4
také	také	k9
sloužil	sloužit	k5eAaImAgMnS
jako	jako	k8xS,k8xC
základna	základna	k1gFnSc1
hydroplánů	hydroplán	k1gInPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
přistávací	přistávací	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
umožňovaly	umožňovat	k5eAaImAgInP
nálety	nálet	k1gInPc1
bombardérů	bombardér	k1gMnPc2
na	na	k7c4
Japonci	Japonec	k1gMnPc7
obsazený	obsazený	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
Wake	Wak	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jamamotův	Jamamotův	k2eAgInSc1d1
plán	plán	k1gInSc1
<g/>
:	:	kIx,
operace	operace	k1gFnPc1
MI	já	k3xPp1nSc3
</s>
<s>
Atol	atol	k1gInSc1
Midway	Midwaa	k1gFnSc2
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
před	před	k7c7
bitvou	bitva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
popředí	popředí	k1gNnSc6
Eastern	Easterna	k1gFnPc2
Island	Island	k1gInSc4
s	s	k7c7
letištěm	letiště	k1gNnSc7
<g/>
,	,	kIx,
za	za	k7c7
ním	on	k3xPp3gInSc7
západně	západně	k6eAd1
je	být	k5eAaImIp3nS
větší	veliký	k2eAgInSc1d2
Sand	Sand	k1gInSc1
Island	Island	k1gInSc1
</s>
<s>
Jak	jak	k6eAd1
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
japonské	japonský	k2eAgNnSc4d1
námořní	námořní	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
typické	typický	k2eAgNnSc1d1
<g/>
,	,	kIx,
Jamamotův	Jamamotův	k2eAgInSc4d1
bojový	bojový	k2eAgInSc4d1
plán	plán	k1gInSc4
pro	pro	k7c4
obsazení	obsazení	k1gNnSc4
Midway	Midwaa	k1gFnSc2
(	(	kIx(
<g/>
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
operace	operace	k1gFnPc4
MI	já	k3xPp1nSc3
<g/>
)	)	kIx)
se	se	k3xPyFc4
vyznačoval	vyznačovat	k5eAaImAgInS
mimořádnou	mimořádný	k2eAgFnSc7d1
složitostí	složitost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Vyžadoval	vyžadovat	k5eAaImAgInS
pečlivou	pečlivý	k2eAgFnSc4d1
a	a	k8xC
dobře	dobře	k6eAd1
načasovanou	načasovaný	k2eAgFnSc4d1
součinnost	součinnost	k1gFnSc4
několika	několik	k4yIc2
bojových	bojový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
rozptýlených	rozptýlený	k2eAgInPc2d1
na	na	k7c6
stovkách	stovka	k1gFnPc6
čtverečných	čtverečný	k2eAgInPc2d1
kilometrů	kilometr	k1gInPc2
volného	volný	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
návrh	návrh	k1gInSc1
byl	být	k5eAaImAgInS
také	také	k9
založen	založit	k5eAaPmNgInS
na	na	k7c6
optimistických	optimistický	k2eAgFnPc6d1
zpravodajských	zpravodajský	k2eAgFnPc6d1
informacích	informace	k1gFnPc6
<g/>
,	,	kIx,
podle	podle	k7c2
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
americké	americký	k2eAgNnSc1d1
tichomořské	tichomořský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
disponovalo	disponovat	k5eAaBmAgNnS
pouze	pouze	k6eAd1
letadlovými	letadlový	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
USS	USS	kA
Enterprise	Enterprise	k1gFnSc2
a	a	k8xC
USS	USS	kA
Hornet	Hornet	k1gInSc1
<g/>
,	,	kIx,
tvořícími	tvořící	k2eAgFnPc7d1
Task	Task	k1gInSc4
Force	force	k1gFnPc4
16	#num#	k4
(	(	kIx(
<g/>
TF	tf	k0wR
16	#num#	k4
~	~	kIx~
16	#num#	k4
<g/>
.	.	kIx.
operační	operační	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
bitvy	bitva	k1gFnSc2
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
o	o	k7c4
měsíc	měsíc	k1gInSc4
dříve	dříve	k6eAd2
byl	být	k5eAaImAgInS
USS	USS	kA
Lexington	Lexington	k1gInSc4
potopen	potopen	k2eAgInSc1d1
a	a	k8xC
USS	USS	kA
Yorktown	Yorktown	k1gMnSc1
utrpěl	utrpět	k5eAaPmAgMnS
takové	takový	k3xDgFnPc4
škody	škoda	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
Japonci	Japonec	k1gMnPc1
považovali	považovat	k5eAaImAgMnP
též	též	k9
za	za	k7c4
ztracený	ztracený	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
několikadenní	několikadenní	k2eAgFnSc6d1
provizorní	provizorní	k2eAgFnSc6d1
opravě	oprava	k1gFnSc6
v	v	k7c4
Pearl	Pearl	k1gInSc4
Harboru	Harbor	k1gInSc2
se	se	k3xPyFc4
však	však	k9
Yorktown	Yorktown	k1gMnSc1
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
služby	služba	k1gFnSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
u	u	k7c2
Midwaye	Midway	k1gInSc2
sehrál	sehrát	k5eAaPmAgMnS
rozhodující	rozhodující	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
objevení	objevení	k1gNnSc6
a	a	k8xC
následném	následný	k2eAgNnSc6d1
zničení	zničení	k1gNnSc6
japonských	japonský	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
konečně	konečně	k6eAd1
<g/>
,	,	kIx,
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
Jamamotových	Jamamotův	k2eAgInPc2d1
plánů	plán	k1gInPc2
byla	být	k5eAaImAgFnS
–	–	k?
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
tehdejším	tehdejší	k2eAgNnSc7d1
obecným	obecný	k2eAgNnSc7d1
přesvědčením	přesvědčení	k1gNnSc7
japonského	japonský	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
–	–	k?
založena	založit	k5eAaPmNgFnS
na	na	k7c6
hrubě	hrubě	k6eAd1
nesprávném	správný	k2eNgInSc6d1
odhadu	odhad	k1gInSc6
bojové	bojový	k2eAgFnSc2d1
morálky	morálka	k1gFnSc2
Američanů	Američan	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
oslabená	oslabený	k2eAgFnSc1d1
šňůrou	šňůra	k1gFnSc7
japonských	japonský	k2eAgFnPc2d1
vítězství	vítězství	k1gNnSc4
během	během	k7c2
předchozích	předchozí	k2eAgInPc2d1
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jamamoto	Jamamota	k1gFnSc5
cítil	cítit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
nalákání	nalákání	k1gNnSc3
americké	americký	k2eAgFnSc2d1
floty	flota	k1gFnSc2
do	do	k7c2
smrtící	smrtící	k2eAgFnSc2d1
pasti	past	k1gFnSc2
bude	být	k5eAaImBp3nS
zapotřebí	zapotřebí	k6eAd1
lsti	lest	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
rozptýlil	rozptýlit	k5eAaPmAgMnS
své	svůj	k3xOyFgInPc4
síly	síl	k1gInPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jejich	jejich	k3xOp3gInSc4
plný	plný	k2eAgInSc4d1
rozsah	rozsah	k1gInSc4
(	(	kIx(
<g/>
zejména	zejména	k9
jeho	jeho	k3xOp3gFnPc1
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
Američané	Američan	k1gMnPc1
před	před	k7c7
bitvou	bitva	k1gFnSc7
neodhalili	odhalit	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jamamotovy	Jamamotův	k2eAgFnPc1d1
podpůrné	podpůrný	k2eAgFnPc1d1
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
a	a	k8xC
křižníky	křižník	k1gInPc1
sledovaly	sledovat	k5eAaImAgInP
hlavní	hlavní	k2eAgInSc4d1
úderný	úderný	k2eAgInSc4d1
svaz	svaz	k1gInSc4
čúdžó	čúdžó	k?
(	(	kIx(
<g/>
中	中	k?
~	~	kIx~
viceadmirál	viceadmirál	k1gMnSc1
<g/>
)	)	kIx)
Čúiči	Čúič	k1gFnSc6
Naguma	Naguma	k1gFnSc1
v	v	k7c6
odstupu	odstup	k1gInSc6
několika	několik	k4yIc2
set	sto	k4xCgNnPc2
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americké	americký	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc4
spěchající	spěchající	k2eAgNnSc4d1
na	na	k7c4
pomoc	pomoc	k1gFnSc4
Midwayi	Midwayi	k1gNnSc2
měly	mít	k5eAaImAgFnP
nejprve	nejprve	k6eAd1
oslabit	oslabit	k5eAaPmF
letecké	letecký	k2eAgInPc4d1
útoky	útok	k1gInPc4
z	z	k7c2
Nagumových	Nagumův	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
objevit	objevit	k5eAaPmF
Jamamotovy	Jamamotův	k2eAgFnPc4d1
těžké	těžký	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
a	a	k8xC
zničit	zničit	k5eAaPmF
zbytky	zbytek	k1gInPc4
amerických	americký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
denní	denní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
taktika	taktika	k1gFnSc1
byla	být	k5eAaImAgFnS
doktrínou	doktrína	k1gFnSc7
většiny	většina	k1gFnSc2
hlavních	hlavní	k2eAgFnPc2d1
námořnictev	námořnictva	k1gFnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jamamoto	Jamamota	k1gFnSc5
však	však	k9
netušil	tušit	k5eNaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nepříteli	nepřítel	k1gMnSc3
povedlo	povést	k5eAaPmAgNnS
rozluštit	rozluštit	k5eAaPmF
části	část	k1gFnPc4
hlavního	hlavní	k2eAgInSc2d1
japonského	japonský	k2eAgInSc2d1
námořního	námořní	k2eAgInSc2d1
kódu	kód	k1gInSc2
(	(	kIx(
<g/>
Američany	Američan	k1gMnPc4
označovaného	označovaný	k2eAgMnSc4d1
JN-	JN-	k1gMnSc4
<g/>
25	#num#	k4
<g/>
b	b	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mu	on	k3xPp3gMnSc3
prozradilo	prozradit	k5eAaPmAgNnS
řadu	řada	k1gFnSc4
podrobností	podrobnost	k1gFnPc2
o	o	k7c6
jeho	jeho	k3xOp3gInSc6
plánu	plán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důraz	důraz	k1gInSc1
kladený	kladený	k2eAgInSc1d1
na	na	k7c4
rozptýlení	rozptýlení	k1gNnSc4
sil	síla	k1gFnPc2
také	také	k9
znamenal	znamenat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnPc1
formace	formace	k1gFnPc1
nemohly	moct	k5eNaImAgFnP
navzájem	navzájem	k6eAd1
podporovat	podporovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Například	například	k6eAd1
navzdory	navzdory	k7c3
skutečnosti	skutečnost	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
od	od	k7c2
Nagumových	Nagumův	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
očekávalo	očekávat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
provedou	provést	k5eAaPmIp3nP
údery	úder	k1gInPc4
proti	proti	k7c3
Midwayi	Midwayi	k1gNnSc3
a	a	k8xC
ponesou	ponést	k5eAaPmIp3nP,k5eAaImIp3nP
první	první	k4xOgInSc4
nápor	nápor	k1gInSc4
amerických	americký	k2eAgInPc2d1
protiútoků	protiútok	k1gInPc2
<g/>
,	,	kIx,
jediný	jediný	k2eAgInSc4d1
jejich	jejich	k3xOp3gInSc4
doprovod	doprovod	k1gInSc4
tvořily	tvořit	k5eAaImAgFnP
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc4
rychlé	rychlý	k2eAgFnPc4d1
bitevní	bitevní	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
třídy	třída	k1gFnSc2
Kongó	Kongó	k1gFnSc4
<g/>
,	,	kIx,
dva	dva	k4xCgInPc1
těžké	těžký	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
a	a	k8xC
dvanáct	dvanáct	k4xCc1
torpédoborců	torpédoborec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k6eAd1
tomu	ten	k3xDgNnSc3
Jamamoto	Jamamota	k1gFnSc5
a	a	k8xC
čúdžó	čúdžó	k?
Kondó	Kondó	k1gMnPc1
měli	mít	k5eAaImAgMnP
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
uskupeních	uskupení	k1gNnPc6
dvě	dva	k4xCgFnPc1
lehké	lehký	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
pět	pět	k4xCc4
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgInPc4
těžké	těžký	k2eAgInPc4d1
křižníky	křižník	k1gInPc4
a	a	k8xC
dva	dva	k4xCgInPc4
lehké	lehký	k2eAgInPc4d1
křižníky	křižník	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
žádná	žádný	k3yNgFnSc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
lodí	loď	k1gFnPc2
do	do	k7c2
bitvy	bitva	k1gFnSc2
nezasáhla	zasáhnout	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Lehké	Lehké	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
krycích	krycí	k2eAgFnPc2d1
sil	síla	k1gFnPc2
a	a	k8xC
Jamamotovy	Jamamotův	k2eAgFnPc1d1
tři	tři	k4xCgFnPc1
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
nebyly	být	k5eNaImAgFnP
schopny	schopen	k2eAgFnPc1d1
držet	držet	k5eAaImF
tempo	tempo	k1gNnSc4
s	s	k7c7
letadlovými	letadlový	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
Kidó	Kidó	k1gMnPc2
butai	buta	k1gFnSc2
(	(	kIx(
<g/>
機	機	k?
~	~	kIx~
mobilní	mobilní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
proto	proto	k8xC
s	s	k7c7
nimi	on	k3xPp3gMnPc7
nemohly	moct	k5eNaImAgFnP
plout	plout	k5eAaImF
v	v	k7c6
jedné	jeden	k4xCgFnSc6
formaci	formace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
mezi	mezi	k7c7
Jamamotem	Jamamot	k1gInSc7
a	a	k8xC
Kondóovými	Kondóův	k2eAgFnPc7d1
silami	síla	k1gFnPc7
a	a	k8xC
Nagumovými	Nagumův	k2eAgFnPc7d1
letadlovými	letadlový	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
znamenala	znamenat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Nagumo	Naguma	k1gFnSc5
přišel	přijít	k5eAaPmAgInS
o	o	k7c4
významný	významný	k2eAgInSc4d1
průzkumný	průzkumný	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
letounů	letoun	k1gInPc2
operujících	operující	k2eAgInPc2d1
z	z	k7c2
Kondóových	Kondóův	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
a	a	k8xC
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
protiletadlovou	protiletadlový	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
křižníků	křižník	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
dvou	dva	k4xCgFnPc2
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
třídy	třída	k1gFnSc2
Kongó	Kongó	k1gFnSc2
krycích	krycí	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
bitvy	bitva	k1gFnSc2
vážné	vážný	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aleutská	aleutský	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Boje	boj	k1gInSc2
o	o	k7c4
Aleutské	aleutský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Operace	operace	k1gFnSc1
v	v	k7c6
západním	západní	k2eAgNnSc6d1
Tichomoří	Tichomoří	k1gNnSc6
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
</s>
<s>
Aby	aby	kYmCp3nP
pro	pro	k7c4
Midwayskou	Midwayský	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
získalo	získat	k5eAaPmAgNnS
podporu	podpora	k1gFnSc4
císařské	císařský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
souhlasilo	souhlasit	k5eAaImAgNnS
japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
podpořit	podpořit	k5eAaPmF
její	její	k3xOp3gFnSc4
invazi	invaze	k1gFnSc4
na	na	k7c6
území	území	k1gNnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
prostřednictvím	prostřednictvím	k7c2
Aleutských	aleutský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
Attu	Attus	k1gInSc2
a	a	k8xC
Kiska	Kisek	k1gInSc2
<g/>
,	,	kIx,
součástí	součást	k1gFnPc2
začleněného	začleněný	k2eAgNnSc2d1
teritoria	teritorium	k1gNnSc2
Aljaška	Aljaška	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
tyto	tento	k3xDgInPc4
ostrovy	ostrov	k1gInPc4
obsadila	obsadit	k5eAaPmAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
udržela	udržet	k5eAaPmAgFnS
domácí	domácí	k2eAgInPc4d1
japonské	japonský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
mimo	mimo	k7c4
dosah	dosah	k1gInSc4
amerických	americký	k2eAgInPc2d1
dálkových	dálkový	k2eAgInPc2d1
pozemních	pozemní	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
operujících	operující	k2eAgInPc2d1
z	z	k7c2
oblasti	oblast	k1gFnSc2
Aljašky	Aljaška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stalo	stát	k5eAaPmAgNnS
první	první	k4xOgFnSc7
cizí	cizí	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
okupovala	okupovat	k5eAaBmAgFnS
půdu	půda	k1gFnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
od	od	k7c2
americko-britské	americko-britský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1812	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
většina	většina	k1gFnSc1
Američanů	Američan	k1gMnPc2
obávala	obávat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
okupované	okupovaný	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
budou	být	k5eAaImBp3nP
použity	použít	k5eAaPmNgInP
jako	jako	k9
základny	základna	k1gFnSc2
pro	pro	k7c4
japonské	japonský	k2eAgInPc4d1
bombardéry	bombardér	k1gInPc4
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
strategické	strategický	k2eAgInPc4d1
cíle	cíl	k1gInPc4
a	a	k8xC
města	město	k1gNnPc4
na	na	k7c6
západním	západní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
japonské	japonský	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
na	na	k7c6
Aleutských	aleutský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
(	(	kIx(
<g/>
operace	operace	k1gFnPc1
AL	ala	k1gFnPc2
<g/>
)	)	kIx)
odčerpala	odčerpat	k5eAaPmAgFnS
ještě	ještě	k6eAd1
více	hodně	k6eAd2
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jinak	jinak	k6eAd1
mohly	moct	k5eAaImAgFnP
posílit	posílit	k5eAaPmF
loďstvo	loďstvo	k1gNnSc4
útočící	útočící	k2eAgNnSc4d1
na	na	k7c4
Midway	Midwa	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
dřívějších	dřívější	k2eAgFnPc2d1
historických	historický	k2eAgFnPc2d1
prací	práce	k1gFnPc2
považovalo	považovat	k5eAaImAgNnS
aleutskou	aleutský	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
za	za	k7c4
diverzi	diverze	k1gFnSc4
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
odlákání	odlákání	k1gNnSc3
amerických	americký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
původního	původní	k2eAgInSc2d1
japonského	japonský	k2eAgInSc2d1
bojového	bojový	k2eAgInSc2d1
plánu	plán	k1gInSc2
však	však	k9
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
AL	ala	k1gFnPc2
zahájena	zahájit	k5eAaPmNgFnS
současně	současně	k6eAd1
s	s	k7c7
útokem	útok	k1gInSc7
na	na	k7c4
Midway	Midwa	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednodenní	jednodenní	k2eAgFnSc7d1
zpoždění	zpoždění	k1gNnSc4
plavby	plavba	k1gFnSc2
Nagumova	Nagumův	k2eAgInSc2d1
úderného	úderný	k2eAgInSc2d1
svazu	svaz	k1gInSc2
způsobilo	způsobit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
operace	operace	k1gFnSc1
AL	ala	k1gFnPc2
začala	začít	k5eAaPmAgFnS
den	den	k1gInSc4
před	před	k7c7
útokem	útok	k1gInSc7
na	na	k7c4
Midway	Midwa	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předehra	předehra	k1gFnSc1
</s>
<s>
Americké	americký	k2eAgFnPc1d1
posily	posila	k1gFnPc1
</s>
<s>
USS	USS	kA
Yorktown	Yorktown	k1gMnSc1
opravovaný	opravovaný	k2eAgMnSc1d1
v	v	k7c6
suchém	suchý	k2eAgInSc6d1
doku	dok	k1gInSc6
v	v	k7c4
Pearl	Pearl	k1gInSc4
Harboru	Harbor	k1gInSc2
několik	několik	k4yIc4
dní	den	k1gInPc2
před	před	k7c7
bitvou	bitva	k1gFnSc7
</s>
<s>
ADM	ADM	kA
(	(	kIx(
<g/>
~	~	kIx~
admirál	admirál	k1gMnSc1
<g/>
)	)	kIx)
Chester	Chester	k1gMnSc1
W.	W.	kA
Nimitz	Nimitz	k1gMnSc1
<g/>
,	,	kIx,
vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
tichomořské	tichomořský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
potřeboval	potřebovat	k5eAaImAgMnS
každou	každý	k3xTgFnSc4
dostupnou	dostupný	k2eAgFnSc4d1
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
pustit	pustit	k5eAaPmF
do	do	k7c2
boje	boj	k1gInSc2
s	s	k7c7
nepřítelem	nepřítel	k1gMnSc7
<g/>
,	,	kIx,
o	o	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
disponovat	disponovat	k5eAaBmF
čtyřmi	čtyři	k4xCgNnPc7
nebo	nebo	k8xC
pěti	pět	k4xCc7
letadlovými	letadlový	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
měl	mít	k5eAaImAgInS
po	po	k7c6
ruce	ruka	k1gFnSc6
operační	operační	k2eAgInSc1d1
svaz	svaz	k1gInSc1
dvou	dva	k4xCgFnPc2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
(	(	kIx(
<g/>
Enterprise	Enterprise	k1gFnSc1
a	a	k8xC
Hornet	Hornet	k1gMnSc1
<g/>
)	)	kIx)
VADM	VADM	kA
(	(	kIx(
<g/>
~	~	kIx~
viceadmirál	viceadmirál	k1gMnSc1
<g/>
)	)	kIx)
Williama	William	k1gMnSc4
Halseyho	Halsey	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
však	však	k9
postihla	postihnout	k5eAaPmAgFnS
těžká	těžký	k2eAgFnSc1d1
dermatitida	dermatitida	k1gFnSc1
a	a	k8xC
musel	muset	k5eAaImAgMnS
jej	on	k3xPp3gMnSc4
nahradit	nahradit	k5eAaPmF
kontradmirál	kontradmirál	k1gMnSc1
Raymond	Raymond	k1gMnSc1
A.	A.	kA
Spruance	Spruance	k1gFnPc1
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
Halseyových	Halseyův	k2eAgInPc2d1
eskortních	eskortní	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Nimitz	Nimitz	k1gMnSc1
také	také	k9
narychlo	narychlo	k6eAd1
povolal	povolat	k5eAaPmAgMnS
z	z	k7c2
jihozápadního	jihozápadní	k2eAgInSc2d1
Pacifiku	Pacifik	k1gInSc2
Task	Task	k1gInSc1
Force	force	k1gFnSc1
17	#num#	k4
(	(	kIx(
<g/>
TF	tf	k0wR
17	#num#	k4
<g/>
)	)	kIx)
RADM	RADM	kA
(	(	kIx(
<g/>
~	~	kIx~
kontradmirál	kontradmirál	k1gMnSc1
<g/>
)	)	kIx)
Franka	Frank	k1gMnSc2
Jacka	Jacek	k1gMnSc2
Fletchera	Fletcher	k1gMnSc2
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnSc4d1
poškozenou	poškozený	k2eAgFnSc4d1
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Yorktown	Yorktowna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestože	přestože	k8xS
poškození	poškození	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
Yorktown	Yorktown	k1gNnSc4
utrpěl	utrpět	k5eAaPmAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
si	se	k3xPyFc3
podle	podle	k7c2
odhadů	odhad	k1gInPc2
mělo	mít	k5eAaImAgNnS
vyžadovat	vyžadovat	k5eAaImF
několik	několik	k4yIc1
měsíců	měsíc	k1gInPc2
oprav	oprava	k1gFnPc2
v	v	k7c6
námořní	námořní	k2eAgFnSc6d1
loděnici	loděnice	k1gFnSc6
v	v	k7c6
Puget	Puget	k?
Sound	Sound	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc1
letecké	letecký	k2eAgInPc1d1
výtahy	výtah	k1gInPc1
byly	být	k5eAaImAgInP
neporušené	porušený	k2eNgInPc1d1
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
většina	většina	k1gFnSc1
letové	letový	k2eAgFnSc2d1
paluby	paluba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
připlutí	připlutí	k1gNnSc1
Yorktownu	Yorktown	k1gInSc2
do	do	k7c2
doku	dok	k1gInSc2
v	v	k7c4
Pearl	Pearl	k1gInSc4
Harboru	Harbor	k1gInSc2
byly	být	k5eAaImAgFnP
okamžitě	okamžitě	k6eAd1
zahájeny	zahájit	k5eAaPmNgFnP
opravy	oprava	k1gFnPc1
a	a	k8xC
po	po	k7c6
72	#num#	k4
hodinách	hodina	k1gFnPc6
nepřetržitých	přetržitý	k2eNgFnPc2d1
prací	práce	k1gFnPc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
loď	loď	k1gFnSc4
opět	opět	k6eAd1
zbojeschopnit	zbojeschopnit	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gInSc1
stav	stav	k1gInSc1
byl	být	k5eAaImAgInS
shledán	shledat	k5eAaPmNgInS
dostatečným	dostatečný	k2eAgInSc7d1
na	na	k7c4
dva	dva	k4xCgInPc4
nebo	nebo	k8xC
tři	tři	k4xCgInPc4
týdny	týden	k1gInPc4
operací	operace	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc4
Nimitz	Nimitz	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Letová	letový	k2eAgFnSc1d1
paluba	paluba	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
záplaty	záplata	k1gFnSc2
<g/>
,	,	kIx,
celé	celý	k2eAgFnSc2d1
vnitřní	vnitřní	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
byly	být	k5eAaImAgInP
vyříznuty	vyříznout	k5eAaPmNgInP
a	a	k8xC
vyměněny	vyměnit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opravy	oprava	k1gFnPc1
pokračovaly	pokračovat	k5eAaImAgFnP
ještě	ještě	k9
po	po	k7c6
jejím	její	k3xOp3gNnSc6
vyplutí	vyplutí	k1gNnSc6
<g/>
,	,	kIx,
prováděly	provádět	k5eAaImAgInP
je	on	k3xPp3gFnPc4
pracovní	pracovní	k2eAgFnPc4d1
čety	četa	k1gFnPc4
z	z	k7c2
dílenské	dílenský	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
USS	USS	kA
Vestal	Vestal	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
sama	sám	k3xTgFnSc1
poškozena	poškozen	k2eAgFnSc1d1
při	při	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gInSc4
před	před	k7c7
šesti	šest	k4xCc7
měsíci	měsíc	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
Devastatorů	Devastator	k1gMnPc2
eskadry	eskadra	k1gFnSc2
VT-8	VT-8	k1gFnSc2
na	na	k7c4
USS	USS	kA
Hornet	Hornet	k1gInSc4
v	v	k7c6
květnu	květen	k1gInSc6
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
stroj	stroj	k1gInSc1
T-5	T-5	k1gFnSc2
(	(	kIx(
<g/>
BuNo	buna	k1gFnSc5
0	#num#	k4
<g/>
308	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
ztracen	ztratit	k5eAaPmNgMnS
při	při	k7c6
útoku	útok	k1gInSc6
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
</s>
<s>
Částečně	částečně	k6eAd1
zničenou	zničený	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
Yorktownu	Yorktown	k1gInSc2
doplnili	doplnit	k5eAaPmAgMnP
pomocí	pomocí	k7c2
jakýchkoli	jakýkoli	k3yIgNnPc2
letadel	letadlo	k1gNnPc2
a	a	k8xC
pilotů	pilot	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
daly	dát	k5eAaPmAgInP
najít	najít	k5eAaPmF
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
průzkumnou	průzkumný	k2eAgFnSc4d1
eskadru	eskadra	k1gFnSc4
(	(	kIx(
<g/>
VS-	VS-	k1gFnSc1
<g/>
5	#num#	k4
<g/>
)	)	kIx)
nahradila	nahradit	k5eAaPmAgFnS
3	#num#	k4
<g/>
.	.	kIx.
bombardovací	bombardovací	k2eAgFnSc2d1
(	(	kIx(
<g/>
VB-	VB-	k1gFnSc2
<g/>
3	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
USS	USS	kA
Saratoga	Saratoga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
5	#num#	k4
<g/>
.	.	kIx.
torpédovou	torpédový	k2eAgFnSc4d1
eskadru	eskadra	k1gFnSc4
(	(	kIx(
<g/>
VT-	VT-	k1gFnSc1
<g/>
5	#num#	k4
<g/>
)	)	kIx)
nahradila	nahradit	k5eAaPmAgFnS
3	#num#	k4
<g/>
.	.	kIx.
torpédová	torpédový	k2eAgFnSc1d1
(	(	kIx(
<g/>
VT-	VT-	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
stíhací	stíhací	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
(	(	kIx(
<g/>
VF-	VF-	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
potopeného	potopený	k2eAgInSc2d1
USS	USS	kA
Lexington	Lexington	k1gInSc1
s	s	k7c7
LCDR	LCDR	kA
(	(	kIx(
<g/>
~	~	kIx~
komandér	komandér	k1gMnSc1
<g/>
–	–	k?
<g/>
poručík	poručík	k1gMnSc1
<g/>
)	)	kIx)
Johnem	John	k1gMnSc7
S.	S.	kA
„	„	k?
<g/>
Jimmy	Jimm	k1gInPc1
<g/>
“	“	k?
Thachem	Thach	k1gInSc7
byla	být	k5eAaImAgFnS
přeskupena	přeskupen	k2eAgFnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nahradila	nahradit	k5eAaPmAgFnS
VF-	VF-	k1gFnSc1
<g/>
42	#num#	k4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
nesl	nést	k5eAaImAgMnS
Yorktown	Yorktown	k1gMnSc1
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyři	čtyři	k4xCgInPc4
původní	původní	k2eAgInPc4d1
piloty	pilot	k1gInPc4
z	z	k7c2
VF-3	VF-3	k1gFnSc2
doplnilo	doplnit	k5eAaPmAgNnS
šestnáct	šestnáct	k4xCc1
veteránů	veterán	k1gMnPc2
z	z	k7c2
VF-42	VF-42	k1gFnPc2
a	a	k8xC
osm	osm	k4xCc1
nováčků	nováček	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
letci	letec	k1gMnPc1
postrádali	postrádat	k5eAaImAgMnP
zkušenosti	zkušenost	k1gFnPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mohlo	moct	k5eAaImAgNnS
přispět	přispět	k5eAaPmF
k	k	k7c3
nehodě	nehoda	k1gFnSc3
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
zahynul	zahynout	k5eAaPmAgMnS
Thachův	Thachův	k2eAgMnSc1d1
výkonný	výkonný	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
LCDR	LCDR	kA
Donald	Donald	k1gMnSc1
Lovelace	Lovelace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Navzdory	navzdory	k7c3
snahám	snaha	k1gFnPc3
připravit	připravit	k5eAaPmF
do	do	k7c2
bitvy	bitva	k1gFnSc2
také	také	k9
Saratogu	Saratoga	k1gFnSc4
(	(	kIx(
<g/>
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
procházela	procházet	k5eAaImAgFnS
opravami	oprava	k1gFnPc7
na	na	k7c6
americkém	americký	k2eAgNnSc6d1
západním	západní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zdržení	zdržení	k1gNnSc1
vyvolané	vyvolaný	k2eAgNnSc1d1
doplňováním	doplňování	k1gNnSc7
zásob	zásoba	k1gFnPc2
a	a	k8xC
shromažďováním	shromažďování	k1gNnSc7
dostatečných	dostatečný	k2eAgFnPc2d1
eskortních	eskortní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
k	k	k7c3
Midway	Midwaum	k1gNnPc7
dostala	dostat	k5eAaPmAgFnS
až	až	k9
po	po	k7c6
bitvě	bitva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
atolu	atol	k1gInSc6
Midway	Midwaa	k1gFnSc2
mělo	mít	k5eAaImAgNnS
americké	americký	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
k	k	k7c3
4	#num#	k4
<g/>
.	.	kIx.
červnu	červen	k1gInSc6
umístěny	umístěn	k2eAgFnPc4d1
čtyři	čtyři	k4xCgFnPc4
eskadry	eskadra	k1gFnPc4
Consolidated	Consolidated	k1gMnSc1
PBY	PBY	kA
Catalina	Catalina	k1gFnSc1
–	–	k?
celkem	celek	k1gInSc7
31	#num#	k4
letadel	letadlo	k1gNnPc2
od	od	k7c2
VP-	VP-	k1gFnSc2
<g/>
23	#num#	k4
<g/>
,	,	kIx,
VP-	VP-	k1gFnSc1
<g/>
24	#num#	k4
<g/>
,	,	kIx,
VP-44	VP-44	k1gFnSc1
a	a	k8xC
VP-51	VP-51	k1gFnSc1
–	–	k?
pro	pro	k7c4
úkoly	úkol	k1gInPc4
dálkového	dálkový	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
a	a	k8xC
šest	šest	k4xCc4
zbrusu	zbrusu	k6eAd1
nových	nový	k2eAgInPc2d1
torpédových	torpédový	k2eAgInPc2d1
Grumman	Grumman	k1gMnSc1
TBF	TBF	kA
Avenger	Avenger	k1gMnSc1
od	od	k7c2
VT-8	VT-8	k1gFnSc2
z	z	k7c2
Hornetu	Hornet	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
zde	zde	k6eAd1
měla	mít	k5eAaImAgFnS
19	#num#	k4
Douglas	Douglas	k1gInSc4
SBD	SBD	kA
Dauntless	Dauntless	k1gInSc1
a	a	k8xC
17	#num#	k4
Vought	Vought	k1gMnSc1
SB2U	SB2U	k1gMnSc1
Vindicator	Vindicator	k1gMnSc1
od	od	k7c2
VMSB-241	VMSB-241	k1gFnPc2
a	a	k8xC
sedm	sedm	k4xCc1
F4F-3	F4F-3	k1gFnPc2
Wildcat	Wildcat	k1gMnPc2
a	a	k8xC
21	#num#	k4
Brewster	Brewster	k1gMnSc1
F2A	F2A	k1gMnSc1
Buffalo	Buffalo	k1gMnSc1
od	od	k7c2
VMF-	VMF-	k1gFnSc2
<g/>
221	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
USAAF	USAAF	kA
přispěla	přispět	k5eAaPmAgFnS
sedmnácti	sedmnáct	k4xCc6
Boeing	boeing	k1gInSc4
B-17	B-17	k1gMnPc2
Flying	Flying	k1gInSc1
Fortress	Fortressa	k1gFnPc2
a	a	k8xC
čtyřmi	čtyři	k4xCgFnPc7
Martin	Martin	k1gMnSc1
B-26	B-26	k1gFnSc4
Marauder	Maraudra	k1gFnPc2
vyzbrojenými	vyzbrojený	k2eAgNnPc7d1
torpédy	torpédo	k1gNnPc7
<g/>
:	:	kIx,
celkem	celkem	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházelo	nacházet	k5eAaImAgNnS
126	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
F2A	F2A	k1gFnPc1
a	a	k8xC
SB2U	SB2U	k1gFnPc1
byly	být	k5eAaImAgFnP
již	již	k6eAd1
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
zastaralé	zastaralý	k2eAgNnSc4d1
<g/>
,	,	kIx,
námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
jiná	jiný	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
k	k	k7c3
dispozici	dispozice	k1gFnSc3
neměla	mít	k5eNaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonská	japonský	k2eAgNnPc1d1
pochybení	pochybení	k1gNnPc1
</s>
<s>
Akagi	Akagi	k1gNnSc1
<g/>
,	,	kIx,
vlajková	vlajkový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
japonského	japonský	k2eAgInSc2d1
úderného	úderný	k2eAgInSc2d1
svazu	svaz	k1gInSc2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
účastnila	účastnit	k5eAaImAgFnS
útoku	útok	k1gInSc3
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
úderů	úder	k1gInPc2
na	na	k7c6
Darwin	Darwin	k1gMnSc1
<g/>
,	,	kIx,
Rabaul	Rabaul	k1gInSc1
a	a	k8xC
Colombo	Colomba	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografie	fotografie	k1gFnSc1
z	z	k7c2
dubna	duben	k1gInSc2
1942	#num#	k4
</s>
<s>
Během	během	k7c2
bitvy	bitva	k1gFnSc2
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
o	o	k7c4
měsíc	měsíc	k1gInSc4
dříve	dříve	k6eAd2
Japonci	Japonec	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
lehkou	lehký	k2eAgFnSc4d1
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Šóhó	Šóhó	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
těžká	těžký	k2eAgFnSc1d1
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Šókaku	Šókak	k1gInSc2
byla	být	k5eAaImAgFnS
vážně	vážně	k6eAd1
poškozena	poškodit	k5eAaPmNgFnS
třemi	tři	k4xCgInPc7
zásahy	zásah	k1gInPc7
leteckých	letecký	k2eAgFnPc2d1
pum	puma	k1gFnPc2
a	a	k8xC
musela	muset	k5eAaImAgFnS
podstoupit	podstoupit	k5eAaPmF
několikaměsíční	několikaměsíční	k2eAgFnPc4d1
opravy	oprava	k1gFnPc4
v	v	k7c6
suchém	suchý	k2eAgInSc6d1
doku	dok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Zuikaku	Zuikak	k1gInSc2
vyvázla	vyváznout	k5eAaPmAgFnS
z	z	k7c2
bitvy	bitva	k1gFnSc2
bez	bez	k7c2
poškození	poškození	k1gNnSc2
<g/>
,	,	kIx,
ztratila	ztratit	k5eAaPmAgFnS
téměř	téměř	k6eAd1
polovinu	polovina	k1gFnSc4
své	svůj	k3xOyFgFnSc2
letecké	letecký	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
a	a	k8xC
nacházela	nacházet	k5eAaImAgFnS
se	se	k3xPyFc4
v	v	k7c6
přístavu	přístav	k1gInSc6
v	v	k7c6
Kure	kur	k1gMnSc5
<g/>
,	,	kIx,
očekávajíc	očekávat	k5eAaImSgFnS
náhradní	náhradní	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
a	a	k8xC
piloty	pilota	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
žádné	žádný	k3yNgFnPc1
letecké	letecký	k2eAgFnPc1d1
posádky	posádka	k1gFnPc1
nebyly	být	k5eNaImAgFnP
okamžitě	okamžitě	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
,	,	kIx,
lze	lze	k6eAd1
přičíst	přičíst	k5eAaPmF
selhání	selhání	k1gNnSc4
výcvikového	výcvikový	k2eAgInSc2d1
programu	program	k1gInSc2
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
již	již	k9
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
vykazoval	vykazovat	k5eAaImAgMnS
známky	známka	k1gFnPc4
neschopnosti	neschopnost	k1gFnPc4
nahradit	nahradit	k5eAaPmF
vzrůstající	vzrůstající	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nápravě	náprava	k1gFnSc3
tohoto	tento	k3xDgInSc2
stavu	stav	k1gInSc2
byli	být	k5eAaImAgMnP
povoláni	povolat	k5eAaPmNgMnP
také	také	k9
instruktoři	instruktor	k1gMnPc1
z	z	k7c2
Jokosuka	Jokosuk	k1gMnSc2
kaigun	kaigun	k1gInSc4
kókútai	kókútai	k1gNnSc2
(	(	kIx(
<g/>
横	横	k?
~	~	kIx~
jokosucká	jokosucký	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historikové	historik	k1gMnPc1
Jonathan	Jonathan	k1gMnSc1
Parshall	Parshall	k1gMnSc1
a	a	k8xC
Anthony	Anthona	k1gFnPc1
Tully	Tulla	k1gFnSc2
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
kdyby	kdyby	kYmCp3nP
se	se	k3xPyFc4
shromáždila	shromáždit	k5eAaPmAgNnP
přeživší	přeživší	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
a	a	k8xC
piloti	pilot	k1gMnPc1
z	z	k7c2
Šókaku	Šókak	k1gInSc2
a	a	k8xC
Zuikaku	Zuikak	k1gInSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
stačilo	stačit	k5eAaBmAgNnS
na	na	k7c4
vybavení	vybavení	k1gNnSc4
Zuikaku	Zuikak	k1gInSc2
téměř	téměř	k6eAd1
celou	celý	k2eAgFnSc7d1
kompletní	kompletní	k2eAgFnSc7d1
leteckou	letecký	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neopomínají	opomínat	k5eNaImIp3nP
však	však	k9
také	také	k9
dodat	dodat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
postup	postup	k1gInSc1
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
platnou	platný	k2eAgFnSc7d1
doktrínou	doktrína	k1gFnSc7
japonských	japonský	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
kladla	klást	k5eAaImAgFnS
důraz	důraz	k1gInSc4
na	na	k7c4
výcvik	výcvik	k1gInSc4
a	a	k8xC
nasazení	nasazení	k1gNnSc4
letadlové	letadlový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
letecké	letecký	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
jako	jako	k8xC,k8xS
jednoho	jeden	k4xCgInSc2
celku	celek	k1gInSc2
(	(	kIx(
<g/>
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
americké	americký	k2eAgFnPc1d1
letecké	letecký	k2eAgFnPc1d1
eskadry	eskadra	k1gFnPc1
neměly	mít	k5eNaImAgFnP
„	„	k?
<g/>
mateřskou	mateřský	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
“	“	k?
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
je	on	k3xPp3gMnPc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
přesunout	přesunout	k5eAaPmF
na	na	k7c4
jinou	jiný	k2eAgFnSc4d1
–	–	k?
jak	jak	k8xS,k8xC
u	u	k7c2
Midway	Midwaa	k1gFnSc2
demonstrovala	demonstrovat	k5eAaBmAgFnS
VF-	VF-	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každém	každý	k3xTgInSc6
případě	případ	k1gInSc6
Japonci	Japonec	k1gMnPc1
zjevně	zjevně	k6eAd1
nepodnikli	podniknout	k5eNaPmAgMnP
vážný	vážný	k2eAgInSc4d1
pokus	pokus	k1gInSc4
připravit	připravit	k5eAaPmF
Zuikaku	Zuikak	k1gInSc2
do	do	k7c2
nadcházející	nadcházející	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
5	#num#	k4
<g/>
.	.	kIx.
kókú	kókú	k?
sentai	senta	k1gFnSc2
(	(	kIx(
<g/>
航	航	k?
~	~	kIx~
divize	divize	k1gFnSc1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
ze	z	k7c2
dvou	dva	k4xCgFnPc2
výše	vysoce	k6eAd2
uvedených	uvedený	k2eAgFnPc2d1
nejmodernějších	moderní	k2eAgFnPc2d3
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
Kidó	Kidó	k1gFnSc1
butai	butai	k6eAd1
nebyla	být	k5eNaImAgFnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
čúdžó	čúdžó	k?
Nagumo	Naguma	k1gFnSc5
mohl	moct	k5eAaImAgInS
použít	použít	k5eAaPmF
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc1
třetiny	třetina	k1gFnPc1
stavu	stav	k1gInSc2
těžkých	těžký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
císařského	císařský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
:	:	kIx,
Kaga	Kagum	k1gNnSc2
a	a	k8xC
Akagi	Akag	k1gFnSc2
tvořící	tvořící	k2eAgFnSc2d1
1	#num#	k4
<g/>
.	.	kIx.
kókú	kókú	k?
sentai	senta	k1gInPc7
a	a	k8xC
Hirjú	Hirjú	k1gFnPc7
a	a	k8xC
Sórjú	Sórjú	k1gFnPc7
jako	jako	k9
2	#num#	k4
<g/>
.	.	kIx.
kókú	kókú	k?
sentai	sentai	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
zde	zde	k6eAd1
sehrála	sehrát	k5eAaPmAgFnS
také	také	k9
únava	únava	k1gFnSc1
<g/>
;	;	kIx,
od	od	k7c2
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
japonské	japonský	k2eAgFnSc2d1
letadlové	letadlový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
setrvávaly	setrvávat	k5eAaImAgInP
v	v	k7c6
téměř	téměř	k6eAd1
nepřetržitém	přetržitý	k2eNgNnSc6d1
bojovém	bojový	k2eAgNnSc6d1
nasazení	nasazení	k1gNnSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
náletů	nálet	k1gInPc2
na	na	k7c6
Darwin	Darwin	k1gMnSc1
a	a	k8xC
Colombo	Colomba	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
Dai	Dai	k1gMnSc1
<g/>
–	–	k?
<g/>
iči	iči	k?
Kidó	Kidó	k1gFnSc2
butai	buta	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kidó	Kidó	k1gFnSc1
butai	butai	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
rozlišení	rozlišení	k1gNnSc1
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kidó	Kidó	k1gFnSc1
butai	buta	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
účastnila	účastnit	k5eAaImAgFnS
souběžné	souběžný	k2eAgFnPc4d1
operace	operace	k1gFnPc4
AL	ala	k1gFnPc2
<g/>
)	)	kIx)
vyplula	vyplout	k5eAaPmAgFnS
s	s	k7c7
248	#num#	k4
dostupnými	dostupný	k2eAgNnPc7d1
letadly	letadlo	k1gNnPc7
na	na	k7c6
čtyřech	čtyři	k4xCgInPc6
nosičích	nosič	k1gInPc6
(	(	kIx(
<g/>
60	#num#	k4
na	na	k7c6
Akagi	Akag	k1gFnSc6
<g/>
,	,	kIx,
74	#num#	k4
na	na	k7c6
Kaga	Kaga	k1gFnSc1
(	(	kIx(
<g/>
posílená	posílený	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
letounů	letoun	k1gInPc2
B	B	kA
<g/>
5	#num#	k4
<g/>
N	N	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
57	#num#	k4
na	na	k7c4
Hirjú	Hirjú	k1gFnSc4
a	a	k8xC
57	#num#	k4
na	na	k7c6
Sórjú	Sórjú	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1
údernou	úderný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
japonských	japonský	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
tvořily	tvořit	k5eAaImAgInP
střemhlavé	střemhlavý	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
Aiči	Aič	k1gFnSc2
D3A1	D3A1	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Val	val	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
torpédové	torpédový	k2eAgNnSc1d1
Nakadžima	Nakadžima	k1gNnSc1
B5N2	B5N2	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Kate	kat	k1gInSc5
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
použitelné	použitelný	k2eAgFnPc1d1
i	i	k9
ke	k	k7c3
klasickému	klasický	k2eAgNnSc3d1
horizontálnímu	horizontální	k2eAgNnSc3d1
bombardování	bombardování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stíhací	stíhací	k2eAgFnSc4d1
složku	složka	k1gFnSc4
zastupoval	zastupovat	k5eAaImAgInS
rychlý	rychlý	k2eAgInSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
obratný	obratný	k2eAgInSc1d1
Micubiši	Micubiše	k1gFnSc3
A6M2	A6M2	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Zeke	Zeke	k1gInSc1
<g/>
“	“	k?
či	či	k8xC
„	„	k?
<g/>
Zero	Zero	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
D3A	D3A	k1gFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
řady	řada	k1gFnSc2
příčin	příčina	k1gFnPc2
drasticky	drasticky	k6eAd1
omezena	omezit	k5eAaPmNgFnS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
B5N	B5N	k1gFnPc1
se	se	k3xPyFc4
přestaly	přestat	k5eAaPmAgFnP
vyrábět	vyrábět	k5eAaImF
vůbec	vůbec	k9
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgMnSc4
chyběly	chybět	k5eAaImAgInP
rezervní	rezervní	k2eAgInPc1d1
stroje	stroj	k1gInPc1
k	k	k7c3
nahrazení	nahrazení	k1gNnSc3
ztrát	ztráta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Mnoho	mnoho	k4c1
letadel	letadlo	k1gNnPc2
používaných	používaný	k2eAgNnPc2d1
během	během	k7c2
operací	operace	k1gFnPc2
v	v	k7c6
červnu	červen	k1gInSc6
1942	#num#	k4
bylo	být	k5eAaImAgNnS
navíc	navíc	k6eAd1
v	v	k7c6
provozu	provoz	k1gInSc6
od	od	k7c2
konce	konec	k1gInSc2
listopadu	listopad	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
přestože	přestože	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
dostávalo	dostávat	k5eAaImAgNnS
dobré	dobrý	k2eAgFnPc4d1
údržby	údržba	k1gFnPc4
<g/>
,	,	kIx,
řada	řada	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
opotřebovaná	opotřebovaný	k2eAgFnSc1d1
a	a	k8xC
stále	stále	k6eAd1
nespolehlivější	spolehlivý	k2eNgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
těchto	tento	k3xDgInPc2
důvodů	důvod	k1gInPc2
nesly	nést	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Kidó	Kidó	k1gFnSc2
butai	buta	k1gFnSc2
méně	málo	k6eAd2
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
než	než	k8xS
činil	činit	k5eAaImAgInS
normální	normální	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
jejich	jejich	k3xOp3gInPc6
hangárech	hangár	k1gInPc6
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
jen	jen	k9
málo	málo	k4c1
rezervních	rezervní	k2eAgInPc2d1
strojů	stroj	k1gInPc2
či	či	k8xC
náhradních	náhradní	k2eAgInPc2d1
dílů	díl	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
palubní	palubní	k2eAgInSc1d1
útočný	útočný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typu	typ	k1gInSc2
97	#num#	k4
<g/>
,	,	kIx,
neboli	neboli	k8xC
torpédový	torpédový	k2eAgInSc1d1
bombardér	bombardér	k1gInSc1
Nakadžima	Nakadžim	k1gMnSc2
B5N2	B5N2	k1gMnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Kate	kat	k1gInSc5
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
obranyschopnost	obranyschopnost	k1gFnSc4
Nagumova	Nagumův	k2eAgInSc2d1
svazu	svaz	k1gInSc2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
podkopávalo	podkopávat	k5eAaImAgNnS
několik	několik	k4yIc1
nedostatků	nedostatek	k1gInPc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
nimž	jenž	k3xRgFnPc3
jej	on	k3xPp3gNnSc4
Mark	Mark	k1gMnSc1
Peattie	Peattie	k1gFnSc2
přirovnává	přirovnávat	k5eAaImIp3nS
k	k	k7c3
„	„	k?
<g/>
boxerovi	boxer	k1gMnSc3
se	se	k3xPyFc4
skleněnou	skleněný	k2eAgFnSc7d1
čelistí	čelist	k1gFnSc7
<g/>
“	“	k?
<g/>
:	:	kIx,
„	„	k?
<g/>
dokáže	dokázat	k5eAaPmIp3nS
údery	úder	k1gInPc4
rozdávat	rozdávat	k5eAaImF
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
přijímat	přijímat	k5eAaImF
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Japonské	japonský	k2eAgFnSc2d1
palubní	palubní	k2eAgFnSc2d1
protiletadlové	protiletadlový	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
systémy	systém	k1gInPc1
řízení	řízení	k1gNnSc2
palby	palba	k1gFnSc2
trpěly	trpět	k5eAaImAgFnP
technickými	technický	k2eAgInPc7d1
a	a	k8xC
jinými	jiný	k2eAgInPc7d1
nedostatky	nedostatek	k1gInPc7
(	(	kIx(
<g/>
poplatnými	poplatný	k2eAgInPc7d1
době	doba	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
snižovaly	snižovat	k5eAaImAgFnP
jejich	jejich	k3xOp3gFnSc4
účinnost	účinnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Efektivitu	efektivita	k1gFnSc4
leteckého	letecký	k2eAgNnSc2d1
hlídkování	hlídkování	k1gNnSc2
omezoval	omezovat	k5eAaImAgInS
příliš	příliš	k6eAd1
malý	malý	k2eAgInSc1d1
počet	počet	k1gInSc1
stíhacích	stíhací	k2eAgInPc2d1
letounů	letoun	k1gInPc2
<g/>
,	,	kIx,
nedostatečný	dostatečný	k2eNgInSc1d1
systém	systém	k1gInSc1
včasného	včasný	k2eAgNnSc2d1
varování	varování	k1gNnSc2
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
absence	absence	k1gFnSc2
radaru	radar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nízká	nízký	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
rádiové	rádiový	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
se	s	k7c7
stíhacími	stíhací	k2eAgInPc7d1
letouny	letoun	k1gInPc7
znemožňovala	znemožňovat	k5eAaImAgFnS
účinné	účinný	k2eAgNnSc4d1
navádění	navádění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eskortní	eskortní	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
plavidla	plavidlo	k1gNnPc1
byla	být	k5eAaImAgNnP
rozmístěna	rozmístit	k5eAaPmNgNnP
v	v	k7c6
širokém	široký	k2eAgInSc6d1
kruhu	kruh	k1gInSc6
okolo	okolo	k7c2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
jako	jako	k8xC,k8xS
vizuální	vizuální	k2eAgFnSc2d1
hlídky	hlídka	k1gFnSc2
<g/>
,	,	kIx,
místo	místo	k1gNnSc4
aby	aby	kYmCp3nS
tvořila	tvořit	k5eAaImAgFnS
jejich	jejich	k3xOp3gInSc4
blízký	blízký	k2eAgInSc4d1
protiletadlový	protiletadlový	k2eAgInSc4d1
doprovod	doprovod	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
jim	on	k3xPp3gMnPc3
k	k	k7c3
tomu	ten	k3xDgMnSc3
chyběl	chybět	k5eAaImAgInS
výcvik	výcvik	k1gInSc1
<g/>
,	,	kIx,
vhodná	vhodný	k2eAgFnSc1d1
doktrína	doktrína	k1gFnSc1
a	a	k8xC
dostatek	dostatek	k1gInSc1
protiletadlových	protiletadlový	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
nepořádku	nepořádek	k1gInSc6
byl	být	k5eAaImAgMnS
také	také	k9
japonský	japonský	k2eAgInSc4d1
strategický	strategický	k2eAgInSc4d1
průzkum	průzkum	k1gInSc4
před	před	k7c7
bitvou	bitva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
japonských	japonský	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
měly	mít	k5eAaImAgFnP
vytvořit	vytvořit	k5eAaPmF
hlídkový	hlídkový	k2eAgInSc4d1
kordón	kordón	k1gInSc4
v	v	k7c6
předkládaných	předkládaný	k2eAgFnPc6d1
přístupových	přístupový	k2eAgFnPc6d1
trasách	trasa	k1gFnPc6
amerických	americký	k2eAgFnPc2d1
plavidel	plavidlo	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
na	na	k7c4
pozice	pozice	k1gFnPc4
nedostala	dostat	k5eNaPmAgFnS
včas	včas	k6eAd1
(	(	kIx(
<g/>
částečně	částečně	k6eAd1
kvůli	kvůli	k7c3
Jamomotovu	Jamomotův	k2eAgInSc3d1
spěchu	spěch	k1gInSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
americkým	americký	k2eAgFnPc3d1
letadlovým	letadlový	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
umožnilo	umožnit	k5eAaPmAgNnS
dosáhnout	dosáhnout	k5eAaPmF
jejich	jejich	k3xOp3gNnSc2
předbitevního	předbitevní	k2eAgNnSc2d1
shromaždiště	shromaždiště	k1gNnSc2
(	(	kIx(
<g/>
známého	známý	k1gMnSc2
jako	jako	k8xS,k8xC
Point	pointa	k1gFnPc2
Luck	Lucka	k1gFnPc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
je	on	k3xPp3gNnSc4
Japonci	Japonec	k1gMnPc1
zpozorovali	zpozorovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
druhého	druhý	k4xOgInSc2
pokusu	pokus	k1gInSc2
o	o	k7c4
průzkum	průzkum	k1gInSc4
<g/>
,	,	kIx,
součásti	součást	k1gFnPc4
operace	operace	k1gFnSc2
K	K	kA
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
skupina	skupina	k1gFnSc1
čtyřmotorových	čtyřmotorový	k2eAgInPc2d1
létajících	létající	k2eAgInPc2d1
člunů	člun	k1gInPc2
Kawaniši	Kawaniše	k1gFnSc4
H8K	H8K	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Emily	Emil	k1gMnPc4
<g/>
“	“	k?
<g/>
)	)	kIx)
před	před	k7c7
bitvou	bitva	k1gFnSc7
prozkoumat	prozkoumat	k5eAaPmF
Pearl	Pearla	k1gFnPc2
Harbor	Harbora	k1gFnPc2
a	a	k8xC
zjistit	zjistit	k5eAaPmF
přítomnost	přítomnost	k1gFnSc4
amerických	americký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
tento	tento	k3xDgInSc1
záměr	záměr	k1gInSc1
byl	být	k5eAaImAgInS
zmařen	zmařen	k2eAgMnSc1d1
<g/>
,	,	kIx,
když	když	k8xS
japonské	japonský	k2eAgFnPc1d1
ponorky	ponorka	k1gFnPc1
mající	mající	k2eAgFnPc1d1
za	za	k7c4
úkol	úkol	k1gInSc4
doplnit	doplnit	k5eAaPmF
palivo	palivo	k1gNnSc4
průzkumným	průzkumný	k2eAgFnPc3d1
hydroplánům	hydroplán	k1gInPc3
zjistily	zjistit	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
plánovaném	plánovaný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
tankování	tankování	k1gNnSc4
–	–	k?
dosud	dosud	k6eAd1
opuštěné	opuštěný	k2eAgFnSc3d1
laguně	laguna	k1gFnSc3
na	na	k7c6
Francouzských	francouzský	k2eAgFnPc6d1
fregatních	fregatní	k2eAgFnPc6d1
mělčinách	mělčina	k1gFnPc6
(	(	kIx(
<g/>
atol	atol	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
Havajského	havajský	k2eAgNnSc2d1
souostroví	souostroví	k1gNnSc2
<g/>
)	)	kIx)
–	–	k?
se	se	k3xPyFc4
nyní	nyní	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
americké	americký	k2eAgFnPc1d1
válečné	válečný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
totiž	totiž	k9
stejnou	stejný	k2eAgFnSc4d1
misi	mise	k1gFnSc4
provedli	provést	k5eAaPmAgMnP
již	již	k6eAd1
v	v	k7c6
březnu	březen	k1gInSc6
<g/>
,	,	kIx,
Američané	Američan	k1gMnPc1
si	se	k3xPyFc3
následně	následně	k6eAd1
dovodili	dovodit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Francouzské	francouzský	k2eAgFnPc1d1
fregatní	fregatní	k2eAgFnPc1d1
mělčiny	mělčina	k1gFnPc1
sloužily	sloužit	k5eAaImAgFnP
Japoncům	Japonec	k1gMnPc3
k	k	k7c3
doplnění	doplnění	k1gNnSc3
paliva	palivo	k1gNnSc2
a	a	k8xC
oblast	oblast	k1gFnSc4
proto	proto	k8xC
preventivně	preventivně	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
tak	tak	k6eAd1
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
možnost	možnost	k1gFnSc4
získat	získat	k5eAaPmF
jakékoliv	jakýkoliv	k3yIgFnPc4
informace	informace	k1gFnPc4
o	o	k7c6
pohybu	pohyb	k1gInSc6
a	a	k8xC
výskytu	výskyt	k1gInSc3
amerických	americký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
bezprostředně	bezprostředně	k6eAd1
před	před	k7c7
bitvou	bitva	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
rádiový	rádiový	k2eAgInSc1d1
odposlech	odposlech	k1gInSc1
zaznamenal	zaznamenat	k5eAaPmAgInS
nárůst	nárůst	k1gInSc4
aktivity	aktivita	k1gFnSc2
amerických	americký	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
i	i	k8xC
rádiové	rádiový	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
informaci	informace	k1gFnSc4
Jamamoto	Jamamota	k1gFnSc5
obdržel	obdržet	k5eAaPmAgMnS
před	před	k7c7
bitvou	bitva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgInPc1d1
plány	plán	k1gInPc1
se	se	k3xPyFc4
nezměnily	změnit	k5eNaPmAgInP
<g/>
;	;	kIx,
Jamamoto	Jamamota	k1gFnSc5
<g/>
,	,	kIx,
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
se	se	k3xPyFc4
již	již	k6eAd1
plavící	plavící	k2eAgFnSc1d1
na	na	k7c6
své	svůj	k3xOyFgFnSc6
vlajkové	vlajkový	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
Jamato	Jamat	k2eAgNnSc1d1
<g/>
,	,	kIx,
předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Nagumo	Naguma	k1gFnSc5
stejnou	stejný	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
z	z	k7c2
Tokia	Tokio	k1gNnSc2
obdržel	obdržet	k5eAaPmAgMnS
také	také	k9
a	a	k8xC
nekomunikoval	komunikovat	k5eNaImAgMnS
s	s	k7c7
ním	on	k3xPp3gNnSc7
rádiem	rádio	k1gNnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
neodhalil	odhalit	k5eNaPmAgInS
svou	svůj	k3xOyFgFnSc4
pozici	pozice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
dřívějšího	dřívější	k2eAgInSc2d1
názoru	názor	k1gInSc2
historiků	historik	k1gMnPc2
Nagumo	Naguma	k1gFnSc5
tyto	tento	k3xDgFnPc4
zprávy	zpráva	k1gFnPc4
před	před	k7c7
začátkem	začátek	k1gInSc7
bitvy	bitva	k1gFnSc2
skutečně	skutečně	k6eAd1
obdržel	obdržet	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nejasných	jasný	k2eNgInPc2d1
důvodů	důvod	k1gInPc2
své	svůj	k3xOyFgInPc4
plány	plán	k1gInPc4
nezměnil	změnit	k5eNaPmAgInS
ani	ani	k8xC
nepřijal	přijmout	k5eNaPmAgInS
jiná	jiný	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozluštění	rozluštění	k1gNnSc1
japonského	japonský	k2eAgInSc2d1
kódu	kód	k1gInSc2
</s>
<s>
CDR	CDR	kA
Joseph	Joseph	k1gInSc1
Rochefort	Rochefort	k1gInSc1
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
týmu	tým	k1gInSc2
kryptoanalytiků	kryptoanalytik	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
rozbili	rozbít	k5eAaPmAgMnP
japonský	japonský	k2eAgInSc4d1
námořní	námořní	k2eAgInSc4d1
kód	kód	k1gInSc4
JN-	JN-	k1gFnPc2
<g/>
25	#num#	k4
<g/>
b	b	k?
</s>
<s>
ADM	ADM	kA
Nimitz	Nimitz	k1gMnSc1
měl	mít	k5eAaImAgMnS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
straně	strana	k1gFnSc6
jednu	jeden	k4xCgFnSc4
zásadní	zásadní	k2eAgFnSc4d1
výhodu	výhoda	k1gFnSc4
<g/>
:	:	kIx,
dešifranti	dešifrant	k1gMnPc1
vojenského	vojenský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
na	na	k7c6
havajské	havajský	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
HYPO	HYPO	kA
částečně	částečně	k6eAd1
rozluštili	rozluštit	k5eAaPmAgMnP
kód	kód	k1gInSc4
japonského	japonský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
JN-	JN-	k1gFnSc2
<g/>
25	#num#	k4
<g/>
b.	b.	k?
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
počátku	počátek	k1gInSc2
roku	rok	k1gInSc2
1942	#num#	k4
Američané	Američan	k1gMnPc1
dekódovali	dekódovat	k5eAaBmAgMnP
zprávy	zpráva	k1gFnPc4
<g/>
,	,	kIx,
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
plánované	plánovaný	k2eAgFnSc2d1
operace	operace	k1gFnSc2
s	s	k7c7
cílem	cíl	k1gInSc7
označeným	označený	k2eAgInSc7d1
„	„	k?
<g/>
AF	AF	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
netušili	tušit	k5eNaImAgMnP
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	s	k7c7
„	„	k?
<g/>
AF	AF	kA
<g/>
“	“	k?
nachází	nacházet	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
kryptoanalytik	kryptoanalytik	k1gMnSc1
CDR	CDR	kA
(	(	kIx(
<g/>
~	~	kIx~
komandér	komandér	k1gMnSc1
<g/>
)	)	kIx)
Joseph	Joseph	k1gInSc1
Rochefort	Rochefort	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
tým	tým	k1gInSc1
na	na	k7c4
stanici	stanice	k1gFnSc4
HYPO	HYPO	kA
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
potvrdit	potvrdit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
Midway	Midwaa	k1gFnPc4
<g/>
:	:	kIx,
CAPT	CAPT	kA
(	(	kIx(
<g/>
~	~	kIx~
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Wilfred	Wilfred	k1gMnSc1
Holmes	Holmes	k1gMnSc1
k	k	k7c3
tomu	ten	k3xDgMnSc3
použil	použít	k5eAaPmAgMnS
léčku	léčka	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
základnu	základna	k1gFnSc4
na	na	k7c4
Midway	Midwaa	k1gFnPc4
instruoval	instruovat	k5eAaBmAgMnS
(	(	kIx(
<g/>
bezpečným	bezpečný	k2eAgInSc7d1
podmořským	podmořský	k2eAgInSc7d1
kabelem	kabel	k1gInSc7
<g/>
)	)	kIx)
odvysílat	odvysílat	k5eAaPmF
nekódovanou	kódovaný	k2eNgFnSc4d1
rádiovou	rádiový	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
uvádějící	uvádějící	k2eAgFnSc7d1
<g/>
,	,	kIx,
že	že	k8xS
odsolovací	odsolovací	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
pitné	pitný	k2eAgFnSc2d1
vody	voda	k1gFnSc2
má	mít	k5eAaImIp3nS
poruchu	porucha	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
24	#num#	k4
hodin	hodina	k1gFnPc2
dešifranti	dešifrant	k1gMnPc1
zachytili	zachytit	k5eAaPmAgMnP
japonskou	japonský	k2eAgFnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
AF	AF	kA
má	mít	k5eAaImIp3nS
nedostatek	nedostatek	k1gInSc1
vody	voda	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Žádnému	žádný	k3yNgInSc3
z	z	k7c2
japonských	japonský	k2eAgInPc2d1
radiooperátorů	radiooperátor	k1gInPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
zprávu	zpráva	k1gFnSc4
zachytili	zachytit	k5eAaPmAgMnP
<g/>
,	,	kIx,
zjevně	zjevně	k6eAd1
nepřišlo	přijít	k5eNaPmAgNnS
podezřelé	podezřelý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Američané	Američan	k1gMnPc1
vysílají	vysílat	k5eAaImIp3nP
nezašifrovanou	zašifrovaný	k2eNgFnSc4d1
zprávu	zpráva	k1gFnSc4
týkající	týkající	k2eAgFnSc4d1
se	se	k3xPyFc4
nedostatku	nedostatek	k1gInSc3
vody	voda	k1gFnSc2
na	na	k7c6
významné	významný	k2eAgFnSc6d1
námořní	námořní	k2eAgFnSc6d1
základně	základna	k1gFnSc6
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
přímém	přímý	k2eAgNnSc6d1
japonském	japonský	k2eAgNnSc6d1
ohrožení	ohrožení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
japonské	japonský	k2eAgFnSc3d1
rozvědce	rozvědka	k1gFnSc3
napovědět	napovědět	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
úmyslný	úmyslný	k2eAgInSc4d1
pokus	pokus	k1gInSc4
zmást	zmást	k5eAaPmF
nepřítele	nepřítel	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
HYPO	HYPO	kA
také	také	k9
dokázalo	dokázat	k5eAaPmAgNnS
určit	určit	k5eAaPmF
datum	datum	k1gNnSc1
útoku	útok	k1gInSc2
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
nebo	nebo	k8xC
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
a	a	k8xC
poskytnout	poskytnout	k5eAaPmF
Nimitzovi	Nimitz	k1gMnSc3
kompletní	kompletní	k2eAgFnSc4d1
bojovou	bojový	k2eAgFnSc4d1
sestavu	sestava	k1gFnSc4
japonských	japonský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonci	Japonec	k1gMnPc1
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
měli	mít	k5eAaImAgMnP
novou	nový	k2eAgFnSc4d1
kódovací	kódovací	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gNnSc1
zavedení	zavedení	k1gNnSc1
se	se	k3xPyFc4
odložilo	odložit	k5eAaPmAgNnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožnilo	umožnit	k5eAaPmAgNnS
dešifrantům	dešifrant	k1gMnPc3
HYPA	HYPA	kA
číst	číst	k5eAaImF
zprávy	zpráva	k1gFnPc4
po	po	k7c4
několik	několik	k4yIc4
klíčových	klíčový	k2eAgInPc2d1
dnů	den	k1gInPc2
před	před	k7c7
bitvou	bitva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
kód	kód	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
prolomení	prolomení	k1gNnSc1
trvalo	trvat	k5eAaImAgNnS
několik	několik	k4yIc4
dní	den	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
používat	používat	k5eAaImF
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
mezitím	mezitím	k6eAd1
už	už	k9
Američané	Američan	k1gMnPc1
důležité	důležitý	k2eAgFnSc2d1
informace	informace	k1gFnSc2
získaly	získat	k5eAaPmAgFnP
ze	z	k7c2
zpráv	zpráva	k1gFnPc2
šifrovaných	šifrovaný	k2eAgFnPc2d1
starým	starý	k2eAgInSc7d1
kódem	kód	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Američané	Američan	k1gMnPc1
šli	jít	k5eAaImAgMnP
do	do	k7c2
bitvy	bitva	k1gFnSc2
s	s	k7c7
dobrou	dobrý	k2eAgFnSc7d1
představou	představa	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
a	a	k8xC
v	v	k7c6
jaké	jaký	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
síle	síla	k1gFnSc6
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
objeví	objevit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nimitz	Nimitz	k1gMnSc1
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nepřítel	nepřítel	k1gMnSc1
připravil	připravit	k5eAaPmAgMnS
o	o	k7c4
početní	početní	k2eAgFnSc4d1
výhodu	výhoda	k1gFnSc4
rozdělením	rozdělení	k1gNnSc7
svých	svůj	k3xOyFgNnPc2
plavidel	plavidlo	k1gNnPc2
do	do	k7c2
čtyř	čtyři	k4xCgFnPc2
samostatných	samostatný	k2eAgFnPc2d1
operačních	operační	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
příliš	příliš	k6eAd1
široce	široko	k6eAd1
rozprostřených	rozprostřený	k2eAgInPc2d1
<g/>
,	,	kIx,
než	než	k8xS
aby	aby	kYmCp3nP
se	se	k3xPyFc4
dokázaly	dokázat	k5eAaPmAgFnP
navzájem	navzájem	k6eAd1
podporovat	podporovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
rozptýlení	rozptýlení	k1gNnSc1
způsobilo	způsobit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
doprovodu	doprovod	k1gInSc3
úderného	úderný	k2eAgInSc2d1
svazu	svaz	k1gInSc2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
zůstalo	zůstat	k5eAaPmAgNnS
pouze	pouze	k6eAd1
několik	několik	k4yIc1
rychlých	rychlý	k2eAgFnPc2d1
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
méně	málo	k6eAd2
protiletadlových	protiletadlový	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
k	k	k7c3
ochraně	ochrana	k1gFnSc3
nosičů	nosič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nimitz	Nimitz	k1gMnSc1
si	se	k3xPyFc3
spočítal	spočítat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
počty	počet	k1gInPc1
letounů	letoun	k1gInPc2
na	na	k7c6
jeho	jeho	k3xOp3gFnPc6
třech	tři	k4xCgFnPc6
letadlových	letadlový	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
spolu	spolu	k6eAd1
s	s	k7c7
těmi	ten	k3xDgMnPc7
na	na	k7c6
atolu	atol	k1gInSc6
Midway	Midwaa	k1gFnSc2
se	se	k3xPyFc4
zhruba	zhruba	k6eAd1
vyrovnají	vyrovnat	k5eAaPmIp3nP,k5eAaBmIp3nP
těm	ten	k3xDgMnPc3
na	na	k7c6
čtyřech	čtyři	k4xCgFnPc6
Jamamotových	Jamamotův	k2eAgFnPc6d1
letadlových	letadlový	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
<g/>
,	,	kIx,
především	především	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
americké	americký	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
nesly	nést	k5eAaImAgFnP
větší	veliký	k2eAgFnPc1d2
letecké	letecký	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
než	než	k8xS
japonské	japonský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
Japonci	Japonec	k1gMnPc1
ani	ani	k8xC
po	po	k7c6
zahájení	zahájení	k1gNnSc6
bitvy	bitva	k1gFnSc2
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
neznali	neznat	k5eAaImAgMnP,k5eNaImAgMnP
skutečnou	skutečný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
a	a	k8xC
pozici	pozice	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
protivníka	protivník	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
</s>
<s>
Pohyby	pohyb	k1gInPc1
hlavních	hlavní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
dle	dle	k7c2
publikace	publikace	k1gFnSc2
Epic	Epic	k1gInSc1
Sea	Sea	k1gMnSc1
Battles	Battles	k1gMnSc1
od	od	k7c2
Williama	William	k1gMnSc2
Koeniga	Koenig	k1gMnSc2
</s>
<s>
Organizace	organizace	k1gFnSc1
japonských	japonský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
</s>
<s>
Organizace	organizace	k1gFnSc1
japonského	japonský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
pro	pro	k7c4
útok	útok	k1gInSc4
na	na	k7c4
Midway	Midwaa	k1gFnPc4
vypadala	vypadat	k5eAaPmAgFnS,k5eAaImAgFnS
zjednodušeně	zjednodušeně	k6eAd1
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
pod	pod	k7c7
velením	velení	k1gNnSc7
taišó	taišó	k?
Jamamota	Jamamot	k1gMnSc2
<g/>
.	.	kIx.
3	#num#	k4
bitevní	bitevní	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Jamato	Jamat	k2eAgNnSc1d1
<g/>
,	,	kIx,
Nagato	Nagat	k2eAgNnSc1d1
a	a	k8xC
Mucu	Mucus	k1gInSc2
<g/>
,	,	kIx,
lehká	lehký	k2eAgFnSc1d1
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Hóšó	Hóšó	k1gFnSc2
(	(	kIx(
<g/>
19	#num#	k4
letounů	letoun	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Sendai	Senda	k1gFnSc2
<g/>
,	,	kIx,
2	#num#	k4
mateřské	mateřský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
hydroplánů	hydroplán	k1gInPc2
Čijoda	Čijoda	k1gFnSc1
a	a	k8xC
Niššin	Niššina	k1gFnPc2
(	(	kIx(
<g/>
nesly	nést	k5eAaImAgFnP
trpasličí	trpasličí	k2eAgFnPc1d1
ponorky	ponorka	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
8	#num#	k4
torpédoborců	torpédoborec	k1gMnPc2
a	a	k8xC
2	#num#	k4
tankery	tanker	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Úderný	úderný	k2eAgInSc1d1
svaz	svaz	k1gInSc1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
pod	pod	k7c7
velením	velení	k1gNnSc7
čúdžó	čúdžó	k?
Naguma	Nagum	k1gMnSc2
<g/>
.	.	kIx.
4	#num#	k4
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Akagi	Akag	k1gFnSc2
<g/>
,	,	kIx,
Kaga	Kaga	k1gMnSc1
<g/>
,	,	kIx,
Hirjú	Hirjú	k1gMnSc1
<g/>
,	,	kIx,
Sórjú	Sórjú	k1gMnSc1
(	(	kIx(
<g/>
234	#num#	k4
letounů	letoun	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Haruna	Haruna	k1gFnSc1
a	a	k8xC
Kirišima	Kirišima	k1gFnSc1
<g/>
,	,	kIx,
těžké	těžký	k2eAgInPc4d1
křižníky	křižník	k1gInPc4
Tone	tonout	k5eAaImIp3nS
a	a	k8xC
Čikuma	Čikuma	k1gFnSc1
<g/>
,	,	kIx,
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Nagara	Nagar	k1gMnSc2
<g/>
,	,	kIx,
12	#num#	k4
torpédoborců	torpédoborec	k1gMnPc2
a	a	k8xC
5	#num#	k4
tankerů	tanker	k1gInPc2
</s>
<s>
Okupační	okupační	k2eAgInSc1d1
svaz	svaz	k1gInSc1
pro	pro	k7c4
Midway	Midwaa	k1gFnPc4
pod	pod	k7c7
velením	velení	k1gNnSc7
čúdžó	čúdžó	k?
Kondóa	Kondóus	k1gMnSc2
<g/>
.	.	kIx.
2	#num#	k4
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Kongó	Kongó	k1gFnSc2
a	a	k8xC
Hiei	Hie	k1gFnSc2
<g/>
,	,	kIx,
lehká	lehký	k2eAgFnSc1d1
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Zuihó	Zuihó	k1gFnSc2
(	(	kIx(
<g/>
24	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
8	#num#	k4
těžkých	těžký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
Atago	Atago	k6eAd1
<g/>
,	,	kIx,
Čókai	Čóka	k1gFnSc5
<g/>
,	,	kIx,
Mjókó	Mjókó	k1gFnSc5
<g/>
,	,	kIx,
Haguro	Hagura	k1gFnSc5
<g/>
,	,	kIx,
Kumano	Kumana	k1gFnSc5
<g/>
,	,	kIx,
Sujuza	Sujuz	k1gMnSc4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Mikuma	Mikuma	k1gNnSc1
<g/>
,	,	kIx,
Mogami	Moga	k1gFnPc7
<g/>
,	,	kIx,
2	#num#	k4
lehké	lehký	k2eAgInPc4d1
křižníky	křižník	k1gInPc4
Jura	jura	k1gFnSc1
a	a	k8xC
Džincú	Džincú	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
mateřské	mateřský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
hydroplánů	hydroplán	k1gInPc2
Čitose	Čitosa	k1gFnSc6
a	a	k8xC
Kamikawa	Kamikaw	k2eAgFnSc1d1
Maru	Maru	k1gFnSc1
<g/>
,	,	kIx,
19	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
<g/>
,	,	kIx,
12	#num#	k4
transportních	transportní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
4	#num#	k4
transportní	transportní	k2eAgInPc1d1
torpédoborce	torpédoborec	k1gInPc1
<g/>
,	,	kIx,
6	#num#	k4
tankerů	tanker	k1gInPc2
<g/>
,	,	kIx,
dílenská	dílenský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
a	a	k8xC
5000	#num#	k4
mužů	muž	k1gMnPc2
výsadku	výsadek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Časová	časový	k2eAgFnSc1d1
osa	osa	k1gFnSc1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Midway	Midwaa	k1gFnSc2
<g/>
(	(	kIx(
<g/>
podle	podle	k7c2
Williama	William	k1gMnSc2
Koeniga	Koenig	k1gMnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
</s>
<s>
0	#num#	k4
<g/>
4.30	4.30	k4
vzlet	vzlet	k1gInSc4
první	první	k4xOgFnSc2
japonské	japonský	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
(	(	kIx(
<g/>
Tomonaga	Tomonaga	k1gFnSc1
<g/>
)	)	kIx)
proti	proti	k7c3
Midway	Midwa	k2eAgInPc1d1
</s>
<s>
0	#num#	k4
<g/>
4.30	4.30	k4
10	#num#	k4
průzkumných	průzkumný	k2eAgInPc2d1
letounů	letoun	k1gInPc2
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
začíná	začínat	k5eAaImIp3nS
hledat	hledat	k5eAaImF
japonské	japonský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
</s>
<s>
0	#num#	k4
<g/>
5.34	5.34	k4
japonské	japonský	k2eAgFnPc1d1
lodi	loď	k1gFnPc1
spatřeny	spatřen	k2eAgFnPc1d1
Catalinou	Catalina	k1gFnSc7
z	z	k7c2
Midway	Midwaa	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
7.10	7.10	k4
útočí	útočit	k5eAaImIp3nS
6	#num#	k4
Avengerů	Avenger	k1gMnPc2
a	a	k8xC
4	#num#	k4
armádní	armádní	k2eAgFnSc4d1
B-26	B-26	k1gFnSc4
z	z	k7c2
Midway	Midwaa	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
7.40	7.40	k4
americký	americký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
objeven	objevit	k5eAaPmNgInS
průzkumným	průzkumný	k2eAgInSc7d1
letounem	letoun	k1gInSc7
č.	č.	k?
4	#num#	k4
z	z	k7c2
Tone	tonout	k5eAaImIp3nS
</s>
<s>
0	#num#	k4
<g/>
7.50	7.50	k4
vzlétá	vzlétat	k5eAaImIp3nS
67	#num#	k4
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
<g/>
,	,	kIx,
29	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
<g/>
,	,	kIx,
20	#num#	k4
stíhaček	stíhačka	k1gFnPc2
Wildcat	Wildcat	k1gFnPc2
(	(	kIx(
<g/>
Spruance	Spruance	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
<g/>
7.55	7.55	k4
útočí	útočit	k5eAaImIp3nP
16	#num#	k4
střemhlavých	střemhlavý	k2eAgMnPc2d1
bombardérů	bombardér	k1gMnPc2
námořnictva	námořnictvo	k1gNnSc2
z	z	k7c2
Midway	Midwaa	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
8.10	8.10	k4
útočí	útočit	k5eAaImIp3nP
17	#num#	k4
výškových	výškový	k2eAgMnPc2d1
B-17	B-17	k1gMnPc2
z	z	k7c2
Midway	Midwaa	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
8.20	8.20	k4
útočí	útočit	k5eAaImIp3nS
11	#num#	k4
bombardérů	bombardér	k1gInPc2
námořnictva	námořnictvo	k1gNnSc2
z	z	k7c2
Midway	Midwaa	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
8.20	8.20	k4
letoun	letoun	k1gInSc1
č.	č.	k?
4	#num#	k4
z	z	k7c2
Tone	tonout	k5eAaImIp3nS
hlásí	hlásit	k5eAaImIp3nP
„	„	k?
<g/>
Jedno	jeden	k4xCgNnSc1
z	z	k7c2
nepřátelských	přátelský	k2eNgNnPc2d1
plavidel	plavidlo	k1gNnPc2
se	se	k3xPyFc4
zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
být	být	k5eAaImF
letadlovou	letadlový	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
<g/>
“	“	k?
</s>
<s>
0	#num#	k4
<g/>
9.06	9.06	k4
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
vzlétá	vzlétat	k5eAaImIp3nS
12	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
<g/>
,	,	kIx,
17	#num#	k4
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
<g/>
,	,	kIx,
6	#num#	k4
Wildcatů	Wildcat	k1gInPc2
</s>
<s>
0	#num#	k4
<g/>
9.10	9.10	k4
Tomonagovy	Tomonagův	k2eAgInPc1d1
letouny	letoun	k1gInPc1
bezpečně	bezpečně	k6eAd1
přistávají	přistávat	k5eAaImIp3nP
</s>
<s>
0	#num#	k4
<g/>
9.18	9.18	k4
Nagumo	Naguma	k1gFnSc5
mění	měnit	k5eAaImIp3nS
kurz	kurz	k1gInSc1
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
</s>
<s>
0	#num#	k4
<g/>
9.25	9.25	k4
útočí	útočit	k5eAaImIp3nS
15	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
z	z	k7c2
Hornetu	Hornet	k1gInSc2
</s>
<s>
0	#num#	k4
<g/>
9.30	9.30	k4
útočí	útočit	k5eAaImIp3nS
14	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
z	z	k7c2
Enterprise	Enterprise	k1gFnSc2
</s>
<s>
10.00	10.00	k4
útočí	útočit	k5eAaImIp3nS
12	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
</s>
<s>
10.25	10.25	k4
30	#num#	k4
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
z	z	k7c2
Enterprise	Enterprise	k1gFnSc2
útočí	útočit	k5eAaImIp3nS
na	na	k7c4
Akagi	Akag	k1gFnPc4
a	a	k8xC
Kaga	Kagum	k1gNnPc4
</s>
<s>
10.25	10.25	k4
17	#num#	k4
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
útočí	útočit	k5eAaImIp3nS
na	na	k7c4
Sórjú	Sórjú	k1gFnSc4
</s>
<s>
11.00	11.00	k4
18	#num#	k4
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
a	a	k8xC
6	#num#	k4
stíhaček	stíhačka	k1gFnPc2
vzlétá	vzlétat	k5eAaImIp3nS
z	z	k7c2
Hirjú	Hirjú	k1gFnSc2
</s>
<s>
11.30	11.30	k4
10	#num#	k4
průzkumných	průzkumný	k2eAgInPc2d1
letounů	letoun	k1gInPc2
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
startuje	startovat	k5eAaBmIp3nS
hledat	hledat	k5eAaImF
zbývající	zbývající	k2eAgFnPc4d1
japonské	japonský	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
</s>
<s>
12.05	12.05	k4
první	první	k4xOgFnSc6
nálet	nálet	k1gInSc1
na	na	k7c4
Yorktown	Yorktown	k1gNnSc4
</s>
<s>
13.30	13.30	k4
Hirjú	Hirjú	k1gFnSc1
nalezena	nalézt	k5eAaBmNgFnS,k5eAaPmNgFnS
letounem	letoun	k1gInSc7
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
<g/>
;	;	kIx,
24	#num#	k4
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
vzlétá	vzlétat	k5eAaImIp3nS
proti	proti	k7c3
Hirjú	Hirjú	k1gFnSc3
(	(	kIx(
<g/>
Spruance	Spruance	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
13.31	13.31	k4
10	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
a	a	k8xC
6	#num#	k4
stíhaček	stíhačka	k1gFnPc2
vzlétá	vzlétat	k5eAaImIp3nS
z	z	k7c2
Hirjú	Hirjú	k1gFnSc2
</s>
<s>
13.40	13.40	k4
Yorktown	Yorktown	k1gNnSc1
opět	opět	k6eAd1
bojeschopný	bojeschopný	k2eAgMnSc1d1
<g/>
,	,	kIx,
rychlost	rychlost	k1gFnSc1
18	#num#	k4
uzlů	uzel	k1gInPc2
</s>
<s>
14.30	14.30	k4
druhý	druhý	k4xOgInSc1
nálet	nálet	k1gInSc1
na	na	k7c4
Yorktown	Yorktown	k1gNnSc4
</s>
<s>
15.00	15.00	k4
Yorktown	Yorktown	k1gInSc1
opuštěn	opuštěn	k2eAgInSc1d1
</s>
<s>
16.10	16.10	k4
Sórjú	Sórjú	k1gFnSc1
potopena	potopen	k2eAgFnSc1d1
</s>
<s>
17.00	17.00	k4
střemhlavé	střemhlavý	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
útočí	útočit	k5eAaImIp3nP
na	na	k7c6
Hirjú	Hirjú	k1gFnSc6
</s>
<s>
19.25	19.25	k4
Kaga	Kag	k2eAgFnSc1d1
potopena	potopen	k2eAgFnSc1d1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
</s>
<s>
0	#num#	k4
<g/>
5.00	5.00	k4
Akagi	Akagi	k1gNnPc2
potopena	potopit	k5eAaPmNgFnS
</s>
<s>
0	#num#	k4
<g/>
9.00	9.00	k4
Hirjú	Hirjú	k1gFnPc2
potopena	potopit	k5eAaPmNgFnS
</s>
<s>
Počáteční	počáteční	k2eAgInPc4d1
letecké	letecký	k2eAgInPc4d1
útoky	útok	k1gInPc4
</s>
<s>
Asi	asi	k9
v	v	k7c4
9.00	9.00	k4
dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
praporčík	praporčík	k1gMnSc1
Jack	Jack	k1gMnSc1
Reid	Reid	k1gMnSc1
<g/>
,	,	kIx,
pilotující	pilotující	k2eAgFnSc1d1
PBY	PBY	kA
Catalina	Catalina	k1gFnSc1
hlídkové	hlídkový	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
amerického	americký	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
VP-	VP-	k1gFnSc2
<g/>
44	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
spatřil	spatřit	k5eAaPmAgMnS
japonské	japonský	k2eAgFnPc4d1
výsadkové	výsadkový	k2eAgFnPc4d1
síly	síla	k1gFnPc4
500	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
930	#num#	k4
km	km	kA
<g/>
)	)	kIx)
západo-jihozápadně	západo-jihozápadně	k6eAd1
od	od	k7c2
Midwaye	Midway	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omylem	omylem	k6eAd1
identifikoval	identifikovat	k5eAaBmAgMnS
tuto	tento	k3xDgFnSc4
skupinu	skupina	k1gFnSc4
jako	jako	k8xS,k8xC
hlavní	hlavní	k2eAgInSc4d1
svaz	svaz	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
12.30	12.30	k4
vzlétlo	vzlétnout	k5eAaPmAgNnS
z	z	k7c2
Midway	Midwaa	k1gFnSc2
devět	devět	k4xCc4
B-17	B-17	k1gFnPc2
k	k	k7c3
prvnímu	první	k4xOgInSc3
leteckému	letecký	k2eAgInSc3d1
útoku	útok	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
tři	tři	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
později	pozdě	k6eAd2
našly	najít	k5eAaPmAgInP
Tanakovu	Tanakův	k2eAgFnSc4d1
transportní	transportní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
okupačního	okupační	k2eAgInSc2d1
svazu	svaz	k1gInSc2
pro	pro	k7c4
Midway	Midwaa	k1gFnPc4
<g/>
,	,	kIx,
570	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
1060	#num#	k4
km	km	kA
<g/>
)	)	kIx)
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Bombardéry	bombardér	k1gInPc1
shodily	shodit	k5eAaPmAgFnP
pumy	puma	k1gFnPc1
za	za	k7c4
silné	silný	k2eAgFnPc4d1
protiletadlové	protiletadlový	k2eAgFnPc4d1
palby	palba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
jejich	jejich	k3xOp3gFnPc1
posádky	posádka	k1gFnPc1
hlásily	hlásit	k5eAaImAgFnP
zasažení	zasažení	k1gNnSc4
čtyř	čtyři	k4xCgFnPc2
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c4
skutečnosti	skutečnost	k1gFnPc4
žádná	žádný	k3yNgFnSc1
z	z	k7c2
bomb	bomba	k1gFnPc2
netrefila	trefit	k5eNaPmAgFnS
a	a	k8xC
lodě	loď	k1gFnPc1
neutrpěly	utrpět	k5eNaPmAgFnP
významnější	významný	k2eAgNnSc4d2
poškození	poškození	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnPc1
škody	škoda	k1gFnPc1
utrpěly	utrpět	k5eAaPmAgFnP
japonské	japonský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
brzy	brzy	k6eAd1
zrána	zrána	k6eAd1
následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
japonský	japonský	k2eAgInSc4d1
tanker	tanker	k1gInSc4
Akebono	Akebona	k1gFnSc5
Maru	Maru	k1gFnSc6
kolem	kolem	k7c2
0	#num#	k4
<g/>
1.00	1.00	k4
zasáhlo	zasáhnout	k5eAaPmAgNnS
torpédo	torpédo	k1gNnSc1
z	z	k7c2
útočícího	útočící	k2eAgInSc2d1
PBY	PBY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poškození	poškození	k1gNnSc1
nebylo	být	k5eNaImAgNnS
tak	tak	k6eAd1
vážné	vážný	k2eAgNnSc4d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zabránilo	zabránit	k5eAaPmAgNnS
tankeru	tanker	k1gInSc3
v	v	k7c6
další	další	k2eAgFnSc6d1
plavbě	plavba	k1gFnSc6
ve	v	k7c6
svazu	svaz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
úspěšný	úspěšný	k2eAgInSc4d1
americký	americký	k2eAgInSc4d1
letecký	letecký	k2eAgInSc4d1
torpédový	torpédový	k2eAgInSc4d1
útok	útok	k1gInSc4
za	za	k7c4
celou	celý	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
0	#num#	k4
<g/>
4.30	4.30	k4
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
zahájil	zahájit	k5eAaPmAgMnS
Nagumo	Naguma	k1gFnSc5
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
nálet	nálet	k1gInSc4
na	na	k7c4
samotný	samotný	k2eAgInSc4d1
Midway	Midway	k1gInPc4
<g/>
,	,	kIx,
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
z	z	k7c2
36	#num#	k4
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
D3A	D3A	k1gFnPc2
a	a	k8xC
36	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
B5N	B5N	k1gFnPc2
vyzbrojených	vyzbrojený	k2eAgFnPc2d1
pumami	puma	k1gFnPc7
v	v	k7c6
doprovodu	doprovod	k1gInSc6
36	#num#	k4
stíhaček	stíhačka	k1gFnPc2
A	a	k8xC
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
,	,	kIx,
pod	pod	k7c7
vedením	vedení	k1gNnSc7
poručíka	poručík	k1gMnSc2
Džóiči	Džóič	k1gInSc3
Tomonagy	Tomonag	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
osm	osm	k4xCc4
průzkumných	průzkumný	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
(	(	kIx(
<g/>
jedno	jeden	k4xCgNnSc1
z	z	k7c2
těžkého	těžký	k2eAgInSc2d1
křižníku	křižník	k1gInSc2
Tone	tonout	k5eAaImIp3nS
vystartovalo	vystartovat	k5eAaPmAgNnS
o	o	k7c4
30	#num#	k4
minut	minuta	k1gFnPc2
později	pozdě	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonský	japonský	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
měl	mít	k5eAaImAgInS
mezery	mezera	k1gFnPc4
<g/>
,	,	kIx,
pro	pro	k7c4
adekvátní	adekvátní	k2eAgNnSc4d1
pokrytí	pokrytí	k1gNnSc4
přidělených	přidělený	k2eAgFnPc2d1
pátracích	pátrací	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
bylo	být	k5eAaImAgNnS
vyčleněno	vyčlenit	k5eAaPmNgNnS
příliš	příliš	k6eAd1
málo	málo	k4c1
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
splnění	splnění	k1gNnSc4
úkolů	úkol	k1gInPc2
navíc	navíc	k6eAd1
stěžovaly	stěžovat	k5eAaImAgFnP
špatné	špatný	k2eAgFnPc1d1
povětrnostní	povětrnostní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
severovýchodně	severovýchodně	k6eAd1
a	a	k8xC
východně	východně	k6eAd1
od	od	k7c2
operačního	operační	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
se	s	k7c7
vzletem	vzlet	k1gInSc7
Nagumových	Nagumův	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
a	a	k8xC
stíhaček	stíhačka	k1gFnPc2
opouštělo	opouštět	k5eAaImAgNnS
Midway	Midwa	k1gMnPc4
jedenáct	jedenáct	k4xCc4
Catalin	Catalina	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zahájily	zahájit	k5eAaPmAgInP
průzkum	průzkum	k1gInSc4
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
sektorech	sektor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
5.34	5.34	k4
jedna	jeden	k4xCgFnSc1
PBY	PBY	kA
ohlásila	ohlásit	k5eAaPmAgFnS
spatření	spatření	k1gNnSc4
dvou	dva	k4xCgFnPc2
japonských	japonský	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
jednu	jeden	k4xCgFnSc4
další	další	k2eAgFnSc4d1
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
o	o	k7c4
10	#num#	k4
minut	minuta	k1gFnPc2
později	pozdě	k6eAd2
přicházející	přicházející	k2eAgInSc1d1
nálet	nálet	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radar	radar	k1gInSc1
na	na	k7c4
Midway	Midwaa	k1gFnPc4
zachytil	zachytit	k5eAaPmAgMnS
nepřítele	nepřítel	k1gMnSc4
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
několika	několik	k4yIc2
mil	míle	k1gFnPc2
a	a	k8xC
následně	následně	k6eAd1
z	z	k7c2
letových	letový	k2eAgFnPc2d1
drah	draha	k1gFnPc2
odstartovaly	odstartovat	k5eAaPmAgFnP
stíhačky	stíhačka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nich	on	k3xPp3gInPc6
následovaly	následovat	k5eAaImAgFnP
bombardéry	bombardér	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
osamoceně	osamoceně	k6eAd1
vyrazily	vyrazit	k5eAaPmAgFnP
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
japonské	japonský	k2eAgFnPc4d1
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
stíhací	stíhací	k2eAgInSc1d1
doprovod	doprovod	k1gInSc1
zůstal	zůstat	k5eAaPmAgInS
k	k	k7c3
obraně	obrana	k1gFnSc3
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
6.20	6.20	k4
japonská	japonský	k2eAgFnSc1d1
palubní	palubní	k2eAgFnSc1d1
letadla	letadlo	k1gNnSc2
americkou	americký	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
bombardovala	bombardovat	k5eAaImAgFnS
a	a	k8xC
těžce	těžce	k6eAd1
poškodila	poškodit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stíhačky	stíhačka	k1gFnPc4
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
vedené	vedený	k2eAgFnSc2d1
majorem	major	k1gMnSc7
Floydem	Floyd	k1gMnSc7
B.	B.	kA
Parkem	park	k1gInSc7
<g/>
,	,	kIx,
sestávající	sestávající	k2eAgFnSc1d1
ze	z	k7c2
šesti	šest	k4xCc2
F4F	F4F	k1gMnPc2
a	a	k8xC
20	#num#	k4
F	F	kA
<g/>
2	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
postavily	postavit	k5eAaPmAgFnP
Japoncům	Japonec	k1gMnPc3
do	do	k7c2
cesty	cesta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utrpěly	utrpět	k5eAaPmAgInP
přitom	přitom	k6eAd1
těžké	těžký	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
sestřelit	sestřelit	k5eAaPmF
čtyři	čtyři	k4xCgMnPc4
B5N	B5N	k1gMnPc4
a	a	k8xC
jeden	jeden	k4xCgInSc1
A	a	k9
<g/>
6	#num#	k4
<g/>
M.	M.	kA
Během	během	k7c2
prvních	první	k4xOgMnPc2
několika	několik	k4yIc2
minut	minuta	k1gFnPc2
byly	být	k5eAaImAgInP
sestřeleny	sestřelen	k2eAgInPc1d1
dva	dva	k4xCgInPc1
F4F	F4F	k1gFnPc2
a	a	k8xC
13	#num#	k4
F2A	F2A	k1gFnPc2
a	a	k8xC
většina	většina	k1gFnSc1
přeživších	přeživší	k2eAgNnPc2d1
amerických	americký	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
poškozena	poškodit	k5eAaPmNgFnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
pouze	pouze	k6eAd1
dvě	dva	k4xCgNnPc4
zůstala	zůstat	k5eAaPmAgFnS
bojeschopná	bojeschopný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intenzivní	intenzivní	k2eAgFnSc1d1
a	a	k8xC
přesná	přesný	k2eAgFnSc1d1
americká	americký	k2eAgFnSc1d1
protiletadlová	protiletadlový	k2eAgFnSc1d1
palba	palba	k1gFnSc1
zničila	zničit	k5eAaPmAgFnS
další	další	k2eAgInSc4d1
tři	tři	k4xCgNnPc1
japonská	japonský	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgMnPc2d1
poškodila	poškodit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sand	Sand	k1gInSc1
Island	Island	k1gInSc1
v	v	k7c6
atolu	atol	k1gInSc6
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
následky	následek	k1gInPc4
japonského	japonský	k2eAgInSc2d1
náletu	nálet	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
</s>
<s>
Ze	z	k7c2
108	#num#	k4
japonských	japonský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
zapojených	zapojený	k2eAgNnPc2d1
do	do	k7c2
tohoto	tento	k3xDgInSc2
útoku	útok	k1gInSc2
bylo	být	k5eAaImAgNnS
11	#num#	k4
zničeno	zničit	k5eAaPmNgNnS
(	(	kIx(
<g/>
včetně	včetně	k7c2
tří	tři	k4xCgNnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nouzově	nouzově	k6eAd1
přistály	přistát	k5eAaImAgFnP,k5eAaPmAgFnP
na	na	k7c6
vodě	voda	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
14	#num#	k4
poškozeno	poškodit	k5eAaPmNgNnS
těžce	těžce	k6eAd1
a	a	k8xC
29	#num#	k4
lehce	lehko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počáteční	počáteční	k2eAgInSc1d1
japonský	japonský	k2eAgInSc1d1
útok	útok	k1gInSc1
nedokázal	dokázat	k5eNaPmAgInS
Midway	Midwaa	k1gFnSc2
zneškodnit	zneškodnit	k5eAaPmF
<g/>
:	:	kIx,
americké	americký	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
mohly	moct	k5eAaImAgInP
stále	stále	k6eAd1
používat	používat	k5eAaImF
letiště	letiště	k1gNnSc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
paliva	palivo	k1gNnSc2
a	a	k8xC
útokům	útok	k1gInPc3
na	na	k7c4
japonské	japonský	k2eAgInPc4d1
invazní	invazní	k2eAgInPc4d1
síly	síl	k1gInPc4
a	a	k8xC
většina	většina	k1gFnSc1
pozemních	pozemní	k2eAgFnPc2d1
obranných	obranný	k2eAgFnPc2d1
instalací	instalace	k1gFnPc2
zůstala	zůstat	k5eAaPmAgFnS
neporušená	porušený	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonští	japonský	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
hlásili	hlásit	k5eAaImAgMnP
Nagumovi	Nagumův	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
mají	mít	k5eAaImIp3nP
invazní	invazní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
vylodit	vylodit	k5eAaPmF
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
nutné	nutný	k2eAgNnSc1d1
proti	proti	k7c3
Midway	Midwaa	k1gFnPc4
provést	provést	k5eAaPmF
druhý	druhý	k4xOgInSc4
letecký	letecký	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americké	americký	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
z	z	k7c2
Midwaye	Midway	k1gInSc2
odstartovaly	odstartovat	k5eAaPmAgFnP
před	před	k7c7
japonským	japonský	k2eAgInSc7d1
náletem	nálet	k1gInSc7
<g/>
,	,	kIx,
podnikly	podniknout	k5eAaPmAgInP
na	na	k7c4
japonský	japonský	k2eAgInSc4d1
úderný	úderný	k2eAgInSc4d1
svaz	svaz	k1gInSc4
několik	několik	k4yIc4
útoků	útok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřilo	patřit	k5eAaImAgNnS
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
šest	šest	k4xCc1
Avengerů	Avenger	k1gInPc2
<g/>
,	,	kIx,
detašovaných	detašovaný	k2eAgInPc2d1
na	na	k7c4
Midway	Midwa	k1gMnPc4
z	z	k7c2
eskadry	eskadra	k1gFnSc2
VT-8	VT-8	k1gFnSc2
Hornetu	Hornet	k1gInSc2
(	(	kIx(
<g/>
Midwayská	Midwayský	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
byla	být	k5eAaImAgFnS
bojovým	bojový	k2eAgInSc7d1
debutem	debut	k1gInSc7
jak	jak	k8xC,k8xS
VT-	VT-	k1gFnSc7
<g/>
8	#num#	k4
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
samotného	samotný	k2eAgInSc2d1
typu	typ	k1gInSc2
TBF	TBF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
241	#num#	k4
<g/>
.	.	kIx.
průzkumná-bombardovací	průzkumná-bombardovací	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
(	(	kIx(
<g/>
VMSB-	VMSB-	k1gFnSc1
<g/>
241	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
z	z	k7c2
11	#num#	k4
SB2U-3	SB2U-3	k1gMnPc2
a	a	k8xC
16	#num#	k4
SBD	SBD	kA
<g/>
,	,	kIx,
plus	plus	k6eAd1
čtyři	čtyři	k4xCgFnPc1
armádní	armádní	k2eAgFnPc1d1
B-26	B-26	k1gFnPc1
vyzbrojené	vyzbrojený	k2eAgFnPc1d1
torpédy	torpédo	k1gNnPc7
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
průzkumné	průzkumný	k2eAgFnSc2d1
a	a	k8xC
69	#num#	k4
<g/>
.	.	kIx.
bombardovací	bombardovací	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
a	a	k8xC
15	#num#	k4
B-17	B-17	k1gMnPc2
z	z	k7c2
31	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
72	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
431	#num#	k4
<g/>
.	.	kIx.
bombardovací	bombardovací	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
tyto	tento	k3xDgInPc4
útoky	útok	k1gInPc4
odrazili	odrazit	k5eAaPmAgMnP
<g/>
,	,	kIx,
ztratili	ztratit	k5eAaPmAgMnP
přitom	přitom	k6eAd1
tři	tři	k4xCgFnPc4
stíhačky	stíhačka	k1gFnPc4
a	a	k8xC
sestřelili	sestřelit	k5eAaPmAgMnP
pět	pět	k4xCc4
TBF	TBF	kA
<g/>
,	,	kIx,
dva	dva	k4xCgInPc4
SB	sb	kA
<g/>
2	#num#	k4
<g/>
U	U	kA
<g/>
,	,	kIx,
osm	osm	k4xCc1
SBD	SBD	kA
a	a	k8xC
dvě	dva	k4xCgFnPc4
B-	B-	k1gFnPc4
<g/>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
padlými	padlý	k1gMnPc7
byl	být	k5eAaImAgMnS
major	major	k1gMnSc1
Lofton	Lofton	k1gInSc4
R.	R.	kA
Henderson	Henderson	k1gNnSc1
z	z	k7c2
VMSB-	VMSB-	k1gFnSc2
<g/>
241	#num#	k4
<g/>
,	,	kIx,
zabitý	zabitý	k2eAgInSc1d1
při	při	k7c6
vedení	vedení	k1gNnSc6
své	svůj	k3xOyFgFnSc2
nezkušené	zkušený	k2eNgFnSc2d1
eskadry	eskadra	k1gFnSc2
Dauntlessů	Dauntless	k1gMnPc2
do	do	k7c2
akce	akce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
1942	#num#	k4
po	po	k7c6
něm	on	k3xPp3gMnSc6
bylo	být	k5eAaImAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
letiště	letiště	k1gNnSc1
na	na	k7c4
Guadalcanalu	Guadalcanala	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeden	jeden	k4xCgMnSc1
B-	B-	k1gMnSc1
<g/>
26	#num#	k4
<g/>
,	,	kIx,
pilotovaný	pilotovaný	k2eAgInSc1d1
poručíkem	poručík	k1gMnSc7
Jamesem	James	k1gMnSc7
Murim	Murim	k1gInSc4
<g/>
,	,	kIx,
po	po	k7c6
odhození	odhození	k1gNnSc6
torpéda	torpédo	k1gNnSc2
a	a	k8xC
ve	v	k7c6
snaze	snaha	k1gFnSc6
najít	najít	k5eAaPmF
bezpečnější	bezpečný	k2eAgFnSc4d2
únikovou	únikový	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
<g/>
,	,	kIx,
zamířil	zamířit	k5eAaPmAgMnS
směrem	směr	k1gInSc7
k	k	k7c3
Akagi	Akagi	k1gNnSc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jej	on	k3xPp3gMnSc4
pronásledovaly	pronásledovat	k5eAaImAgFnP
stíhačky	stíhačka	k1gFnPc1
a	a	k8xC
protiletadlová	protiletadlový	k2eAgFnSc1d1
palba	palba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
však	však	k9
musela	muset	k5eAaImAgFnS
přestat	přestat	k5eAaPmF
střílet	střílet	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nezasáhla	zasáhnout	k5eNaPmAgFnS
vlastní	vlastní	k2eAgFnSc4d1
vlajkovou	vlajkový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
průletu	průlet	k1gInSc2
podél	podél	k7c2
Akagi	Akag	k1gFnSc2
ji	on	k3xPp3gFnSc4
B-26	B-26	k1gFnSc4
postřelovala	postřelovat	k5eAaImAgFnS
a	a	k8xC
přitom	přitom	k6eAd1
zabila	zabít	k5eAaPmAgFnS
dva	dva	k4xCgMnPc4
muže	muž	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
B-	B-	k1gFnPc2
<g/>
26	#num#	k4
<g/>
,	,	kIx,
vážně	vážně	k6eAd1
poškozený	poškozený	k2eAgInSc1d1
protiletadlovou	protiletadlový	k2eAgFnSc7d1
palbou	palba	k1gFnSc7
<g/>
,	,	kIx,
nevybral	vybrat	k5eNaPmAgMnS
klesání	klesání	k1gNnSc4
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgInSc2
zamířil	zamířit	k5eAaPmAgInS
přímo	přímo	k6eAd1
k	k	k7c3
velitelskému	velitelský	k2eAgInSc3d1
můstku	můstek	k1gInSc3
Akagi	Akag	k1gFnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Letoun	letoun	k1gInSc1
<g/>
,	,	kIx,
buď	buď	k8xC
v	v	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c4
sebevražedný	sebevražedný	k2eAgInSc4d1
zásah	zásah	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
neovladatelný	ovladatelný	k2eNgMnSc1d1
v	v	k7c6
důsledku	důsledek	k1gInSc6
utrpěného	utrpěný	k2eAgNnSc2d1
poškození	poškození	k1gNnSc2
či	či	k8xC
zraněného	zraněný	k1gMnSc4
nebo	nebo	k8xC
zabitého	zabitý	k1gMnSc4
pilota	pilot	k1gMnSc4
<g/>
,	,	kIx,
těsně	těsně	k6eAd1
minul	minout	k5eAaImAgMnS
lodní	lodní	k2eAgInSc4d1
můstek	můstek	k1gInSc4
s	s	k7c7
Nagumem	Nagum	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
štábem	štáb	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
zřítil	zřítit	k5eAaPmAgInS
se	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
možná	možná	k9
přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
Nagumovu	Nagumův	k2eAgNnSc3d1
odhodlání	odhodlání	k1gNnSc3
uskutečnit	uskutečnit	k5eAaPmF
další	další	k2eAgInSc4d1
nálet	nálet	k1gInSc4
na	na	k7c4
Midway	Midwaa	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
přímém	přímý	k2eAgInSc6d1
rozporu	rozpor	k1gInSc6
s	s	k7c7
Jamamotovým	Jamamotův	k2eAgInSc7d1
rozkazem	rozkaz	k1gInSc7
ponechat	ponechat	k5eAaPmF
si	se	k3xPyFc3
záložní	záložní	k2eAgFnSc4d1
údernou	úderný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
vyzbrojenou	vyzbrojený	k2eAgFnSc4d1
k	k	k7c3
protilodním	protilodní	k2eAgInPc3d1
útokům	útok	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nagumovo	Nagumův	k2eAgNnSc1d1
dilema	dilema	k1gNnSc1
</s>
<s>
V	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
Jamamotovými	Jamamotův	k2eAgInPc7d1
rozkazy	rozkaz	k1gInPc7
pro	pro	k7c4
operaci	operace	k1gFnSc4
MI	já	k3xPp1nSc3
si	se	k3xPyFc3
admirál	admirál	k1gMnSc1
Nagumo	Naguma	k1gFnSc5
ponechal	ponechat	k5eAaPmAgInS
polovinu	polovina	k1gFnSc4
letadel	letadlo	k1gNnPc2
v	v	k7c6
záloze	záloha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
dvě	dva	k4xCgFnPc4
eskadry	eskadra	k1gFnPc4
<g/>
,	,	kIx,
sestávající	sestávající	k2eAgFnSc1d1
každá	každý	k3xTgFnSc1
ze	z	k7c2
střemhlavých	střemhlavý	k2eAgInPc2d1
a	a	k8xC
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střemhlavé	střemhlavý	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
byly	být	k5eAaImAgInP
dosud	dosud	k6eAd1
nevyzbrojené	vyzbrojený	k2eNgInPc1d1
(	(	kIx(
<g/>
ačkoli	ačkoli	k8xS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
předpisy	předpis	k1gInPc7
<g/>
:	:	kIx,
střemhlavé	střemhlavý	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
vyzbrojovány	vyzbrojovat	k5eAaImNgInP
na	na	k7c6
letové	letový	k2eAgFnSc6d1
palubě	paluba	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torpédové	torpédový	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
nesly	nést	k5eAaImAgInP
torpéda	torpédo	k1gNnPc4
<g/>
,	,	kIx,
pro	pro	k7c4
případ	případ	k1gInSc4
výskytu	výskyt	k1gInSc2
amerických	americký	k2eAgFnPc2d1
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
Nagumo	Naguma	k1gFnSc5
vydal	vydat	k5eAaPmAgInS
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
přezbrojení	přezbrojení	k1gNnSc3
rezervních	rezervní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
kontaktními	kontaktní	k2eAgFnPc7d1
víceúčelovými	víceúčelový	k2eAgFnPc7d1
pumami	puma	k1gFnPc7
určenými	určený	k2eAgFnPc7d1
pro	pro	k7c4
použití	použití	k1gNnSc4
proti	proti	k7c3
pozemním	pozemní	k2eAgInPc3d1
cílům	cíl	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodnutí	rozhodnutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
reakcí	reakce	k1gFnSc7
na	na	k7c4
útoky	útok	k1gInPc4
bombardérů	bombardér	k1gInPc2
z	z	k7c2
Midwaye	Midway	k1gInSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
na	na	k7c4
doporučení	doporučení	k1gNnSc4
velitele	velitel	k1gMnSc4
ranního	ranní	k2eAgInSc2d1
náletu	nálet	k1gInSc2
k	k	k7c3
druhému	druhý	k4xOgInSc3
útoku	útok	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přezbrojení	přezbrojení	k1gNnSc1
probíhalo	probíhat	k5eAaImAgNnS
asi	asi	k9
30	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
0	#num#	k4
<g/>
7.40	7.40	k4
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
opožděné	opožděný	k2eAgNnSc1d1
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
z	z	k7c2
křižníku	křižník	k1gInSc2
Tone	tonout	k5eAaImIp3nS
ohlásilo	ohlásit	k5eAaPmAgNnS
zpozorování	zpozorování	k1gNnSc4
velké	velký	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
amerických	americký	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
opomnělo	opomnět	k5eAaPmAgNnS
uvést	uvést	k5eAaPmF
její	její	k3xOp3gNnSc4
složení	složení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedávno	nedávno	k6eAd1
nalezené	nalezený	k2eAgInPc1d1
důkazy	důkaz	k1gInPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Nagumo	Naguma	k1gFnSc5
neobdržel	obdržet	k5eNaPmAgMnS
zprávu	zpráva	k1gFnSc4
o	o	k7c4
pozorování	pozorování	k1gNnPc4
dříve	dříve	k6eAd2
než	než	k8xS
v	v	k7c6
8.00	8.00	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonský	japonský	k2eAgMnSc1d1
admirál	admirál	k1gMnSc1
rychle	rychle	k6eAd1
zrušil	zrušit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
rozkaz	rozkaz	k1gInSc4
vyzbrojit	vyzbrojit	k5eAaPmF
bombardéry	bombardér	k1gMnPc4
trhavými	trhavý	k2eAgFnPc7d1
pumami	puma	k1gFnPc7
a	a	k8xC
nařídil	nařídit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
průzkumný	průzkumný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
upřesnil	upřesnit	k5eAaPmAgInS
složení	složení	k1gNnSc4
amerického	americký	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uplynulo	uplynout	k5eAaPmAgNnS
dalších	další	k2eAgInPc2d1
20	#num#	k4
až	až	k9
40	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
než	než	k8xS
průzkumník	průzkumník	k1gMnSc1
z	z	k7c2
Tone	tonout	k5eAaImIp3nS
konečně	konečně	k6eAd1
odvysílal	odvysílat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
jedno	jeden	k4xCgNnSc1
z	z	k7c2
nepřátelských	přátelský	k2eNgNnPc2d1
plavidel	plavidlo	k1gNnPc2
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
být	být	k5eAaImF
letadlovou	letadlový	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
jeden	jeden	k4xCgInSc1
z	z	k7c2
nosičů	nosič	k1gInPc2
Task	Task	k1gInSc4
Force	force	k1gFnSc2
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
nezpozorována	zpozorovat	k5eNaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nagumo	Nagumo	k6eAd1
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
dál	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontradmirál	kontradmirál	k1gMnSc1
Tamon	Tamon	k1gMnSc1
Jamaguči	Jamaguč	k1gFnSc5
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
(	(	kIx(
<g/>
Hirjú	Hirjú	k1gMnSc1
a	a	k8xC
Sórjú	Sórjú	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
doporučoval	doporučovat	k5eAaImAgInS
okamžitě	okamžitě	k6eAd1
udeřit	udeřit	k5eAaPmF
se	s	k7c7
všemi	všecek	k3xTgFnPc7
dostupnými	dostupný	k2eAgFnPc7d1
silami	síla	k1gFnPc7
<g/>
:	:	kIx,
16	#num#	k4
střemhlavými	střemhlavý	k2eAgInPc7d1
bombardéry	bombardér	k1gInPc7
D3A1	D3A1	k1gFnSc2
na	na	k7c4
Sórjú	Sórjú	k1gFnSc4
a	a	k8xC
18	#num#	k4
na	na	k7c6
Hirjú	Hirjú	k1gFnSc6
a	a	k8xC
polovinou	polovina	k1gFnSc7
připravených	připravený	k2eAgFnPc2d1
stíhaček	stíhačka	k1gFnPc2
letecké	letecký	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
Příležitost	příležitost	k1gFnSc4
k	k	k7c3
napadení	napadení	k1gNnSc3
amerických	americký	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
navíc	navíc	k6eAd1
omezoval	omezovat	k5eAaImAgMnS
blížící	blížící	k2eAgInSc4d1
se	se	k3xPyFc4
návrat	návrat	k1gInSc4
útočné	útočný	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
od	od	k7c2
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vracející	vracející	k2eAgInSc1d1
se	se	k3xPyFc4
letadla	letadlo	k1gNnSc2
s	s	k7c7
docházejícím	docházející	k2eAgNnSc7d1
palivem	palivo	k1gNnSc7
potřebovala	potřebovat	k5eAaImAgFnS
rychle	rychle	k6eAd1
přistát	přistát	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
by	by	kYmCp3nS
musela	muset	k5eAaImAgFnS
skončit	skončit	k5eAaPmF
v	v	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
neustálému	neustálý	k2eAgInSc3d1
provozu	provoz	k1gInSc3
na	na	k7c6
letové	letový	k2eAgFnSc6d1
palubě	paluba	k1gFnSc6
v	v	k7c6
uplynulé	uplynulý	k2eAgFnSc6d1
hodině	hodina	k1gFnSc6
kvůli	kvůli	k7c3
operacím	operace	k1gFnPc3
stíhací	stíhací	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
neměli	mít	k5eNaImAgMnP
Japonci	Japonec	k1gMnPc1
nikdy	nikdy	k6eAd1
příležitost	příležitost	k1gFnSc4
vyvézt	vyvézt	k5eAaPmF
svá	svůj	k3xOyFgNnPc4
rezervní	rezervní	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
na	na	k7c4
letovou	letový	k2eAgFnSc4d1
palubu	paluba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Těch	ten	k3xDgMnPc2
několik	několik	k4yIc1
málo	málo	k6eAd1
letadel	letadlo	k1gNnPc2
na	na	k7c6
japonských	japonský	k2eAgFnPc6d1
letových	letový	k2eAgFnPc6d1
palubách	paluba	k1gFnPc6
v	v	k7c6
době	doba	k1gFnSc6
útoku	útok	k1gInSc6
byly	být	k5eAaImAgFnP
buď	buď	k8xC
obranné	obranný	k2eAgFnPc1d1
stíhačky	stíhačka	k1gFnPc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
případě	případ	k1gInSc6
Sórjú	Sórjú	k1gFnSc2
stíhačky	stíhačka	k1gFnSc2
připravované	připravovaný	k2eAgFnSc2d1
k	k	k7c3
posílení	posílení	k1gNnSc3
letecké	letecký	k2eAgFnSc2d1
hlídky	hlídka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Vytažení	vytažení	k1gNnSc1
strojů	stroj	k1gInPc2
na	na	k7c4
letovou	letový	k2eAgFnSc4d1
palubu	paluba	k1gFnSc4
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
vyslání	vyslání	k1gNnSc4
do	do	k7c2
vzduchu	vzduch	k1gInSc2
by	by	kYmCp3nP
si	se	k3xPyFc3
vyžadovalo	vyžadovat	k5eAaImAgNnS
nejméně	málo	k6eAd3
30	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
by	by	kYmCp3nS
okamžité	okamžitý	k2eAgNnSc1d1
vyslání	vyslání	k1gNnSc1
letadel	letadlo	k1gNnPc2
znamenalo	znamenat	k5eAaImAgNnS
vrhnout	vrhnout	k5eAaPmF,k5eAaImF
do	do	k7c2
boje	boj	k1gInSc2
veškeré	veškerý	k3xTgFnPc4
rezervy	rezerva	k1gFnPc4
bez	bez	k7c2
patřičné	patřičný	k2eAgFnSc2d1
protilodní	protilodní	k2eAgFnSc2d1
výzbroje	výzbroj	k1gFnSc2
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
i	i	k9
bez	bez	k7c2
stíhací	stíhací	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nagumo	Naguma	k1gFnSc5
právě	právě	k9
na	na	k7c4
vlastní	vlastní	k2eAgNnPc4d1
oči	oko	k1gNnPc4
viděl	vidět	k5eAaImAgMnS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
snadno	snadno	k6eAd1
byla	být	k5eAaImAgNnP
sestřelována	sestřelován	k2eAgNnPc1d1
útočící	útočící	k2eAgNnPc1d1
americká	americký	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
nechráněná	chráněný	k2eNgNnPc1d1
stíhačkami	stíhačka	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
doktrína	doktrína	k1gFnSc1
upřednostňovala	upřednostňovat	k5eAaImAgFnS
útok	útok	k1gInSc4
kompletní	kompletní	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
formace	formace	k1gFnSc2
před	před	k7c7
vysíláním	vysílání	k1gNnSc7
jednotlivých	jednotlivý	k2eAgInPc2d1
fragmentů	fragment	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jak	jak	k6eAd1
byly	být	k5eAaImAgFnP
zrovna	zrovna	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemajíc	mít	k5eNaImSgFnS
jasno	jasno	k1gNnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
objevená	objevený	k2eAgNnPc1d1
americká	americký	k2eAgNnPc1d1
plavidla	plavidlo	k1gNnPc1
zahrnují	zahrnovat	k5eAaImIp3nP
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
(	(	kIx(
<g/>
potvrzení	potvrzení	k1gNnSc1
přišlo	přijít	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
8.20	8.20	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nagumo	Naguma	k1gFnSc5
postupoval	postupovat	k5eAaImAgInS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
doktrínou	doktrína	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
další	další	k2eAgInSc4d1
nálet	nálet	k1gInSc4
letounů	letoun	k1gInPc2
z	z	k7c2
Midway	Midwaa	k1gFnSc2
v	v	k7c4
7.53	7.53	k4
opět	opět	k6eAd1
dodal	dodat	k5eAaPmAgMnS
váhu	váha	k1gFnSc4
požadavku	požadavek	k1gInSc2
dalšího	další	k2eAgInSc2d1
útoku	útok	k1gInSc2
na	na	k7c4
atol	atol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nagumo	Naguma	k1gFnSc5
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgInS
počkat	počkat	k5eAaPmF
na	na	k7c4
přistání	přistání	k1gNnSc4
své	svůj	k3xOyFgFnSc2
první	první	k4xOgFnSc2
útočné	útočný	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
odstartovat	odstartovat	k5eAaPmF
rezervní	rezervní	k2eAgInPc4d1
stroje	stroj	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
mezitím	mezitím	k6eAd1
budou	být	k5eAaImBp3nP
přezbrojeny	přezbrojit	k5eAaPmNgInP
torpédy	torpédo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2
rozbory	rozbor	k1gInPc1
bitvy	bitva	k1gFnSc2
ukázaly	ukázat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
už	už	k9
na	na	k7c4
budoucí	budoucí	k2eAgInSc4d1
osud	osud	k1gInSc4
japonských	japonský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
nemělo	mít	k5eNaImAgNnS
žádný	žádný	k3yNgInSc4
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americké	americký	k2eAgInPc1d1
palubní	palubní	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
začaly	začít	k5eAaPmAgInP
z	z	k7c2
Fletcherových	Fletcherův	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
vzlétat	vzlétat	k5eAaImF
v	v	k7c6
7.00	7.00	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
paluby	paluba	k1gFnSc2
Enterprise	Enterprise	k1gFnSc2
a	a	k8xC
Hornetu	Hornet	k1gInSc2
všechny	všechen	k3xTgInPc1
letouny	letoun	k1gInPc1
opustily	opustit	k5eAaPmAgInP
do	do	k7c2
0	#num#	k4
<g/>
7.55	7.55	k4
<g/>
,	,	kIx,
u	u	k7c2
Yorktownu	Yorktown	k1gInSc2
však	však	k9
teprve	teprve	k6eAd1
v	v	k7c4
0	#num#	k4
<g/>
9.08	9.08	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadla	letadlo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
japonskému	japonský	k2eAgInSc3d1
svazu	svaz	k1gInSc3
uštědřit	uštědřit	k5eAaPmF
smrtící	smrtící	k2eAgInSc4d1
úder	úder	k1gInSc4
již	již	k6eAd1
tedy	tedy	k9
byla	být	k5eAaImAgFnS
na	na	k7c6
cestě	cesta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
Nagumo	Naguma	k1gFnSc5
striktně	striktně	k6eAd1
nedržel	držet	k5eNaImAgMnS
platné	platný	k2eAgFnSc2d1
doktríny	doktrína	k1gFnSc2
<g/>
,	,	kIx,
startu	start	k1gInSc2
amerických	americký	k2eAgNnPc2d1
palubních	palubní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
zabránit	zabránit	k5eAaPmF
nemohl	moct	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Útoky	útok	k1gInPc4
na	na	k7c4
japonské	japonský	k2eAgNnSc4d1
loďstvo	loďstvo	k1gNnSc4
</s>
<s>
Pumy	puma	k1gFnSc2
B-17	B-17	k1gFnPc2
míjí	míjet	k5eAaImIp3nS
Hirjú	Hirjú	k1gFnSc1
<g/>
;	;	kIx,
fotografie	fotografie	k1gFnSc1
pořízena	pořízen	k2eAgFnSc1d1
při	při	k7c6
útoku	útok	k1gInSc6
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
mezi	mezi	k7c4
8.00	8.00	k4
<g/>
–	–	k?
<g/>
8.30	8.30	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poblíž	poblíž	k7c2
můstku	můstek	k1gInSc2
stojí	stát	k5eAaImIp3nS
vyrovnaná	vyrovnaný	k2eAgFnSc1d1
šótai	šótai	k6eAd1
tří	tři	k4xCgFnPc2
Zer	Zer	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
bojových	bojový	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
hlídek	hlídka	k1gFnPc2
vyslaných	vyslaný	k2eAgFnPc2d1
během	během	k7c2
dne	den	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Američané	Američan	k1gMnPc1
již	již	k6eAd1
vyslali	vyslat	k5eAaPmAgMnP
palubní	palubní	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
proti	proti	k7c3
Japoncům	Japonec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fletcher	Fletchra	k1gFnPc2
<g/>
,	,	kIx,
velící	velící	k2eAgFnSc3d1
celé	celý	k2eAgFnSc3d1
operaci	operace	k1gFnSc3
z	z	k7c2
můstku	můstek	k1gInSc2
Yorktownu	Yorktown	k1gInSc2
a	a	k8xC
těžící	těžící	k2eAgFnPc1d1
z	z	k7c2
ranních	ranní	k1gFnPc2
hlášení	hlášení	k1gNnSc2
průzkumných	průzkumný	k2eAgFnPc2d1
Catalin	Catalina	k1gFnPc2
o	o	k7c4
pozici	pozice	k1gFnSc4
nepřítele	nepřítel	k1gMnSc4
<g/>
,	,	kIx,
nařídil	nařídit	k5eAaPmAgInS
Spruanceovi	Spruanceus	k1gMnSc3
zahájit	zahájit	k5eAaPmF
údery	úder	k1gInPc4
proti	proti	k7c3
Japoncům	Japonec	k1gMnPc3
jakmile	jakmile	k8xS
to	ten	k3xDgNnSc1
bude	být	k5eAaImBp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
zatím	zatím	k6eAd1
ponechat	ponechat	k5eAaPmF
Yorktown	Yorktown	k1gInSc4
v	v	k7c6
záloze	záloha	k1gFnSc6
pro	pro	k7c4
případ	případ	k1gInSc4
objevení	objevení	k1gNnSc2
dalších	další	k2eAgFnPc2d1
japonských	japonský	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spruance	Spruanka	k1gFnSc3
usoudil	usoudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
když	když	k8xS
jsou	být	k5eAaImIp3nP
Japonci	Japonec	k1gMnPc1
ještě	ještě	k6eAd1
velmi	velmi	k6eAd1
daleko	daleko	k6eAd1
<g/>
,	,	kIx,
úder	úder	k1gInSc1
má	mít	k5eAaImIp3nS
šanci	šance	k1gFnSc4
na	na	k7c4
úspěch	úspěch	k1gInSc4
a	a	k8xC
vydal	vydat	k5eAaPmAgInS
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
zahájení	zahájení	k1gNnSc3
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitána	kapitán	k1gMnSc2
Milese	Milese	k1gFnSc2
Browninga	Browning	k1gMnSc2
<g/>
,	,	kIx,
náčelníka	náčelník	k1gMnSc2
Halseyho	Halsey	k1gMnSc2
štábu	štáb	k1gInSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
pověřil	pověřit	k5eAaPmAgInS
dopracováním	dopracování	k1gNnSc7
podrobností	podrobnost	k1gFnPc2
akce	akce	k1gFnSc2
a	a	k8xC
dohledem	dohled	k1gInSc7
na	na	k7c4
vyslání	vyslání	k1gNnSc4
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadla	letadlo	k1gNnPc1
potřebovala	potřebovat	k5eAaImAgFnS
startovat	startovat	k5eAaBmF
proti	proti	k7c3
větru	vítr	k1gInSc3
<g/>
,	,	kIx,
takže	takže	k8xS
vzhledem	vzhledem	k7c3
ke	k	k7c3
slabému	slabý	k2eAgInSc3d1
jihovýchodnímu	jihovýchodní	k2eAgInSc3d1
větru	vítr	k1gInSc3
by	by	kYmCp3nP
musely	muset	k5eAaImAgFnP
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
plout	plout	k5eAaImF
plnou	plný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
směrem	směr	k1gInSc7
od	od	k7c2
japonského	japonský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Browning	browning	k1gInSc4
proto	proto	k8xC
navrhl	navrhnout	k5eAaPmAgMnS
posunout	posunout	k5eAaPmF
čas	čas	k1gInSc4
startu	start	k1gInSc2
na	na	k7c4
7.00	7.00	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
lodím	loď	k1gFnPc3
plujícím	plující	k2eAgFnPc3d1
rychlostí	rychlost	k1gFnSc7
25	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
46	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
poskytlo	poskytnout	k5eAaPmAgNnS
další	další	k2eAgFnSc4d1
hodinu	hodina	k1gFnSc4
na	na	k7c4
přiblížení	přiblížení	k1gNnSc4
k	k	k7c3
Japoncům	Japonec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
by	by	kYmCp3nP
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgFnP
na	na	k7c4
asi	asi	k9
155	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
287	#num#	k4
km	km	kA
<g/>
)	)	kIx)
od	od	k7c2
japonské	japonský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
zůstala	zůstat	k5eAaPmAgFnS
na	na	k7c6
stejném	stejný	k2eAgInSc6d1
kurzu	kurz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnPc1
letadlo	letadlo	k1gNnSc1
opustilo	opustit	k5eAaPmAgNnS
Spruanceovy	Spruanceův	k2eAgInPc4d1
nosiče	nosič	k1gInPc4
Enterprise	Enterprise	k1gFnSc2
a	a	k8xC
Hornet	Hornet	k1gInSc1
několik	několik	k4yIc1
minut	minuta	k1gFnPc2
po	po	k7c6
sedmé	sedmý	k4xOgFnSc6
hodině	hodina	k1gFnSc6
ranní	ranní	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
Fletcher	Fletchra	k1gFnPc2
na	na	k7c4
Yorktownu	Yorktowna	k1gMnSc4
<g/>
,	,	kIx,
čekající	čekající	k2eAgMnPc4d1
na	na	k7c4
návrat	návrat	k1gInSc4
svých	svůj	k3xOyFgNnPc2
průzkumných	průzkumný	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
zahájil	zahájit	k5eAaPmAgInS
start	start	k1gInSc4
útočné	útočný	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
v	v	k7c6
8.00	8.00	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fletcher	Fletchra	k1gFnPc2
spolu	spolu	k6eAd1
s	s	k7c7
velícím	velící	k2eAgMnSc7d1
důstojníkem	důstojník	k1gMnSc7
Yorktownu	Yorktown	k1gInSc2
<g/>
,	,	kIx,
kapitánem	kapitán	k1gMnSc7
Elliottem	Elliott	k1gMnSc7
Buckmasterem	Buckmaster	k1gMnSc7
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
štáby	štáb	k1gInPc1
<g/>
,	,	kIx,
získali	získat	k5eAaPmAgMnP
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
z	z	k7c2
první	první	k4xOgFnSc2
ruky	ruka	k1gFnSc2
zkušenosti	zkušenost	k1gFnSc2
<g/>
,	,	kIx,
potřebné	potřebný	k2eAgFnSc2d1
k	k	k7c3
organizování	organizování	k1gNnSc3
a	a	k8xC
vyslání	vyslání	k1gNnSc3
hromadného	hromadný	k2eAgInSc2d1
leteckého	letecký	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyl	být	k5eNaImAgMnS
však	však	k9
čas	čas	k1gInSc4
předat	předat	k5eAaPmF
tyto	tento	k3xDgInPc4
poznatky	poznatek	k1gInPc4
dál	daleko	k6eAd2
na	na	k7c4
Enterprise	Enterprise	k1gFnPc4
a	a	k8xC
Hornet	Horneta	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
měly	mít	k5eAaImAgFnP
za	za	k7c4
úkol	úkol	k1gInSc4
zahájit	zahájit	k5eAaPmF
první	první	k4xOgInSc4
úder	úder	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Spruance	Spruanec	k1gInSc2
nařídil	nařídit	k5eAaPmAgMnS
letadlům	letadlo	k1gNnPc3
okamžitě	okamžitě	k6eAd1
po	po	k7c6
startu	start	k1gInSc6
letět	letět	k5eAaImF
k	k	k7c3
cíli	cíl	k1gInSc3
<g/>
,	,	kIx,
místo	místo	k6eAd1
aby	aby	kYmCp3nP
ztrácely	ztrácet	k5eAaImAgInP
čas	čas	k1gInSc4
čekáním	čekání	k1gNnSc7
na	na	k7c6
shromáždění	shromáždění	k1gNnSc6
úderné	úderný	k2eAgFnSc2d1
formace	formace	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
zneškodnění	zneškodnění	k1gNnSc1
nepřátelských	přátelský	k2eNgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
bylo	být	k5eAaImAgNnS
klíčem	klíč	k1gInSc7
k	k	k7c3
přežití	přežití	k1gNnSc3
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
vlastního	vlastní	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatímco	zatímco	k8xS
Japonci	Japonec	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
vyslat	vyslat	k5eAaPmF
do	do	k7c2
vzduchu	vzduch	k1gInSc2
108	#num#	k4
letadel	letadlo	k1gNnPc2
za	za	k7c4
pouhých	pouhý	k2eAgNnPc2d1
sedm	sedm	k4xCc4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
trval	trvat	k5eAaImAgInS
vzlet	vzlet	k1gInSc1
117	#num#	k4
strojů	stroj	k1gInPc2
z	z	k7c2
lodí	loď	k1gFnPc2
Enterprise	Enterprise	k1gFnSc2
a	a	k8xC
Hornet	Hornet	k1gInSc4
celou	celý	k2eAgFnSc4d1
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Spruance	Spruanec	k1gInSc2
usoudil	usoudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
udeřit	udeřit	k5eAaPmF
na	na	k7c4
nepřítele	nepřítel	k1gMnSc4
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
nejdříve	dříve	k6eAd3
je	být	k5eAaImIp3nS
důležitější	důležitý	k2eAgMnSc1d2
než	než	k8xS
koordinovaný	koordinovaný	k2eAgInSc1d1
útok	útok	k1gInSc1
letadel	letadlo	k1gNnPc2
různých	různý	k2eAgInPc2d1
typů	typ	k1gInPc2
a	a	k8xC
rychlostí	rychlost	k1gFnSc7
(	(	kIx(
<g/>
stíhačky	stíhačka	k1gFnPc1
<g/>
,	,	kIx,
střemhlavé	střemhlavý	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
a	a	k8xC
torpédové	torpédový	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
americké	americký	k2eAgFnPc1d1
eskadry	eskadra	k1gFnPc1
vzlétaly	vzlétat	k5eAaImAgFnP
po	po	k7c6
částech	část	k1gFnPc6
a	a	k8xC
směřovaly	směřovat	k5eAaImAgFnP
k	k	k7c3
cíli	cíl	k1gInSc3
v	v	k7c6
několika	několik	k4yIc6
různých	různý	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
nedostatečná	dostatečný	k2eNgFnSc1d1
koordinace	koordinace	k1gFnSc1
omezí	omezit	k5eAaPmIp3nS
důraz	důraz	k1gInSc4
útoků	útok	k1gInPc2
letadel	letadlo	k1gNnPc2
a	a	k8xC
zvýší	zvýšit	k5eAaPmIp3nP
jejich	jejich	k3xOp3gFnPc4
ztráty	ztráta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spruance	Spruanec	k1gInSc2
však	však	k9
kalkuloval	kalkulovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
výhody	výhoda	k1gFnPc1
převyšují	převyšovat	k5eAaImIp3nP
riziko	riziko	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
udržování	udržování	k1gNnSc1
Japonců	Japonec	k1gMnPc2
pod	pod	k7c7
tlakem	tlak	k1gInSc7
neustálých	neustálý	k2eAgInPc2d1
leteckých	letecký	k2eAgInPc2d1
útoků	útok	k1gInPc2
naruší	narušit	k5eAaPmIp3nS
jejich	jejich	k3xOp3gFnSc4
schopnost	schopnost	k1gFnSc4
zahájit	zahájit	k5eAaPmF
protiútok	protiútok	k1gInSc4
(	(	kIx(
<g/>
japonská	japonský	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
preferovala	preferovat	k5eAaImAgFnS
údery	úder	k1gInPc4
plnou	plný	k2eAgFnSc7d1
a	a	k8xC
zkoordinovanou	zkoordinovaný	k2eAgFnSc7d1
silou	síla	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vsadil	vsadit	k5eAaPmAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
zastihne	zastihnout	k5eAaPmIp3nS
Nagumovy	Nagumův	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
v	v	k7c6
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
budou	být	k5eAaImBp3nP
jejich	jejich	k3xOp3gFnPc1
letové	letový	k2eAgFnPc1d1
paluby	paluba	k1gFnPc1
nejzranitelnější	zranitelný	k2eAgFnPc1d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Devastatory	Devastator	k1gInPc1
VT-6	VT-6	k1gFnSc2
připravované	připravovaný	k2eAgFnSc2d1
ke	k	k7c3
startu	start	k1gInSc3
na	na	k7c6
palubě	paluba	k1gFnSc6
USS	USS	kA
Enterprise	Enterprise	k1gFnPc1
ráno	ráno	k1gNnSc1
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
</s>
<s>
Americká	americký	k2eAgNnPc1d1
palubní	palubní	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
měla	mít	k5eAaImAgNnP
potíže	potíž	k1gFnPc4
najít	najít	k5eAaPmF
cíle	cíl	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
navzdory	navzdory	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
dostala	dostat	k5eAaPmAgFnS
jejich	jejich	k3xOp3gFnPc4
souřadnice	souřadnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útočná	útočný	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
z	z	k7c2
Hornetu	Hornet	k1gInSc2
vedená	vedený	k2eAgFnSc1d1
Stanhopeem	Stanhopeum	k1gNnSc7
C.	C.	kA
Ringem	ring	k1gInSc7
následovala	následovat	k5eAaImAgFnS
nesprávný	správný	k2eNgInSc4d1
kurz	kurz	k1gInSc4
265	#num#	k4
stupňů	stupeň	k1gInPc2
namísto	namísto	k7c2
240	#num#	k4
stupňů	stupeň	k1gInPc2
uvedených	uvedený	k2eAgInPc2d1
v	v	k7c6
průzkumném	průzkumný	k2eAgNnSc6d1
hlášení	hlášení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
střemhlavé	střemhlavý	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
8	#num#	k4
<g/>
.	.	kIx.
letecké	letecký	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
japonské	japonský	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
minuly	minout	k5eAaImAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
8	#num#	k4
<g/>
.	.	kIx.
torpédová	torpédový	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
(	(	kIx(
<g/>
VT-	VT-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
Hornetu	Hornet	k1gInSc2
<g/>
)	)	kIx)
vedená	vedený	k2eAgFnSc1d1
poručíkem	poručík	k1gMnSc7
Johnem	John	k1gMnSc7
C.	C.	kA
Waldronem	Waldron	k1gMnSc7
opustila	opustit	k5eAaPmAgFnS
Ringovu	Ringův	k2eAgFnSc4d1
formaci	formace	k1gFnSc4
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
se	se	k3xPyFc4
správným	správný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deseti	deset	k4xCc3
Wildcatům	Wildcat	k1gMnPc3
z	z	k7c2
Hornetu	Hornet	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
palivo	palivo	k1gNnSc1
a	a	k8xC
musely	muset	k5eAaImAgFnP
přistát	přistát	k5eAaPmF,k5eAaImF
na	na	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Praporčík	praporčík	k1gMnSc1
George	Georg	k1gMnSc2
H.	H.	kA
Gay	gay	k1gMnSc1
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jediný	jediný	k2eAgInSc1d1
přeživší	přeživší	k2eAgNnSc4d1
eskadry	eskadra	k1gFnPc4
VT-8	VT-8	k1gFnSc2
před	před	k7c7
svým	svůj	k3xOyFgInSc7
letounem	letoun	k1gInSc7
TBD	TBD	kA
Devastator	Devastator	k1gInSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
</s>
<s>
Waldronova	Waldronův	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
nalezla	naleznout	k5eAaPmAgFnS,k5eAaBmAgFnS
nepřátelské	přátelský	k2eNgFnPc4d1
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
a	a	k8xC
v	v	k7c6
9.20	9.20	k4
zahájila	zahájit	k5eAaPmAgFnS
útok	útok	k1gInSc4
<g/>
,	,	kIx,
následovaná	následovaný	k2eAgFnSc1d1
v	v	k7c6
9.40	9.40	k4
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
stroji	stroj	k1gInSc6
VF-6	VF-6	k1gFnSc2
z	z	k7c2
Enterprise	Enterprise	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnPc4
doprovodné	doprovodný	k2eAgFnPc4d1
stíhačky	stíhačka	k1gFnPc4
Wildcat	Wildcat	k1gFnSc2
s	s	k7c7
ní	on	k3xPp3gFnSc7
ztratily	ztratit	k5eAaPmAgFnP
kontakt	kontakt	k1gInSc4
<g/>
,	,	kIx,
vyčerpaly	vyčerpat	k5eAaPmAgInP
palivo	palivo	k1gNnSc4
a	a	k8xC
musely	muset	k5eAaImAgFnP
se	se	k3xPyFc4
vrátit	vrátit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
Bez	bez	k7c2
ochrany	ochrana	k1gFnSc2
stíhaček	stíhačka	k1gFnPc2
bylo	být	k5eAaImAgNnS
všech	všecek	k3xTgMnPc2
15	#num#	k4
Devastatorů	Devastator	k1gMnPc2
VT-8	VT-8	k1gFnSc2
sestřeleno	sestřelit	k5eAaPmNgNnS
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
způsobit	způsobit	k5eAaPmF
jakékoli	jakýkoli	k3yIgFnPc4
škody	škoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praporčík	praporčík	k1gMnSc1
George	Georg	k1gInSc2
H.	H.	kA
Gay	gay	k1gMnSc1
byl	být	k5eAaImAgMnS
jediným	jediný	k2eAgMnSc7d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
ze	z	k7c2
30	#num#	k4
mužů	muž	k1gMnPc2
posádek	posádka	k1gFnPc2
VT-8	VT-8	k1gMnPc2
přežil	přežít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
byl	být	k5eAaImAgMnS
sestřelen	sestřelit	k5eAaPmNgMnS
<g/>
,	,	kIx,
dokončil	dokončit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
torpédový	torpédový	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Sórjú	Sórjú	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
loď	loď	k1gFnSc1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gNnSc3
torpédu	torpédo	k1gNnSc3
vyhnula	vyhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
Mezitím	mezitím	k6eAd1
VT-6	VT-6	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
LCDR	LCDR	kA
(	(	kIx(
<g/>
komandér-poručík	komandér-poručík	k1gMnSc1
<g/>
)	)	kIx)
Eugenem	Eugen	k1gMnSc7
E.	E.	kA
Lindseym	Lindseym	k1gInSc1
ztratila	ztratit	k5eAaPmAgNnP
devět	devět	k4xCc1
ze	z	k7c2
svých	svůj	k3xOyFgMnPc2
14	#num#	k4
Devastatorů	Devastator	k1gMnPc2
(	(	kIx(
<g/>
jeden	jeden	k4xCgMnSc1
později	pozdě	k6eAd2
nouzově	nouzově	k6eAd1
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
v	v	k7c6
moři	moře	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
10	#num#	k4
z	z	k7c2
12	#num#	k4
Devastatorů	Devastator	k1gMnPc2
z	z	k7c2
VT-3	VT-3	k1gFnSc2
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
(	(	kIx(
<g/>
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zaútočily	zaútočit	k5eAaPmAgFnP
v	v	k7c6
10.10	10.10	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
sestřeleno	sestřelit	k5eAaPmNgNnS
bez	bez	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
za	za	k7c4
své	svůj	k3xOyFgNnSc4
úsilí	úsilí	k1gNnSc4
dočkaly	dočkat	k5eAaPmAgFnP
jediného	jediný	k2eAgInSc2d1
úspěšného	úspěšný	k2eAgInSc2d1
zásahu	zásah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc1
na	na	k7c6
neúspěchu	neúspěch	k1gInSc6
měla	mít	k5eAaImAgFnS
také	také	k9
nespolehlivost	nespolehlivost	k1gFnSc1
jejich	jejich	k3xOp3gNnPc2
torpéd	torpédo	k1gNnPc2
Mark	Mark	k1gMnSc1
13	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
Midwayská	Midwayský	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
byla	být	k5eAaImAgFnS
posledním	poslední	k2eAgNnSc7d1
bojovým	bojový	k2eAgNnSc7d1
představením	představení	k1gNnSc7
Devastatorů	Devastator	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
bojová	bojový	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
hlídka	hlídka	k1gFnSc1
<g/>
,	,	kIx,
pilotující	pilotující	k2eAgFnSc1d1
stroje	stroj	k1gInPc4
A6M2	A6M2	k1gMnPc2
„	„	k?
<g/>
Zero	Zero	k6eAd1
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
si	se	k3xPyFc3
snadno	snadno	k6eAd1
poradila	poradit	k5eAaPmAgFnS
se	s	k7c7
špatně	špatně	k6eAd1
vyzbrojenými	vyzbrojený	k2eAgInPc7d1
a	a	k8xC
bez	bez	k7c2
stíhacího	stíhací	k2eAgInSc2d1
doprovodu	doprovod	k1gInSc2
letícími	letící	k2eAgInPc7d1
Devastatory	Devastator	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
Devastatorů	Devastator	k1gMnPc2
se	se	k3xPyFc4
před	před	k7c7
svržením	svržení	k1gNnSc7
torpéd	torpédo	k1gNnPc2
dokázalo	dokázat	k5eAaPmAgNnS
dostat	dostat	k5eAaPmF
ke	k	k7c3
svým	svůj	k3xOyFgMnPc3
cílům	cíl	k1gInPc3
na	na	k7c4
několik	několik	k4yIc4
lodních	lodní	k2eAgFnPc2d1
délek	délka	k1gFnPc2
–	–	k?
dostatečně	dostatečně	k6eAd1
blízko	blízko	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohly	moct	k5eAaImAgFnP
nepřátelské	přátelský	k2eNgFnPc1d1
lodě	loď	k1gFnPc1
postřelovat	postřelovat	k5eAaImF
z	z	k7c2
palubních	palubní	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
a	a	k8xC
přinutit	přinutit	k5eAaPmF
japonské	japonský	k2eAgInPc4d1
nosiče	nosič	k1gInPc4
provádět	provádět	k5eAaImF
ostré	ostrý	k2eAgInPc1d1
úhybné	úhybný	k2eAgInPc1d1
manévry	manévr	k1gInPc1
–	–	k?
ale	ale	k8xC
všechna	všechen	k3xTgNnPc4
jejich	jejich	k3xOp3gNnPc4
torpéda	torpédo	k1gNnPc4
buď	buď	k8xC
minula	minulo	k1gNnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
nevybuchla	vybuchnout	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
Pozoruhodné	pozoruhodný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
odpovědné	odpovědný	k2eAgMnPc4d1
činitele	činitel	k1gMnPc4
námořnictva	námořnictvo	k1gNnSc2
a	a	k8xC
výzbrojní	výzbrojní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
neznepokojilo	znepokojit	k5eNaPmAgNnS
<g/>
,	,	kIx,
proč	proč	k6eAd1
půl	půl	k1xP
tuctu	tucet	k1gInSc2
torpéd	torpédo	k1gNnPc2
vypuštěných	vypuštěný	k2eAgFnPc2d1
tak	tak	k9
blízko	blízko	k7c2
japonských	japonský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
nedosáhlo	dosáhnout	k5eNaPmAgNnS
žádného	žádný	k3yNgMnSc4
výsledku	výsledek	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
Výkon	výkon	k1gInSc1
amerických	americký	k2eAgNnPc2d1
torpéd	torpédo	k1gNnPc2
v	v	k7c6
prvních	první	k4xOgInPc6
měsících	měsíc	k1gInPc6
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
skandální	skandální	k2eAgMnSc1d1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
protože	protože	k8xS
jedno	jeden	k4xCgNnSc1
za	za	k7c2
druhým	druhý	k4xOgNnSc7
selhávalo	selhávat	k5eAaImAgNnS
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k9
podplutím	podplutí	k1gNnSc7
cíle	cíl	k1gInSc2
(	(	kIx(
<g/>
hlouběji	hluboko	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
bylo	být	k5eAaImAgNnS
nastaveno	nastavit	k5eAaPmNgNnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
předčasným	předčasný	k2eAgInSc7d1
výbuchem	výbuch	k1gInSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
naopak	naopak	k6eAd1
když	když	k8xS
po	po	k7c6
zásahu	zásah	k1gInSc6
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
se	s	k7c7
slyšitelným	slyšitelný	k2eAgNnSc7d1
kovovým	kovový	k2eAgNnSc7d1
zařinčením	zařinčení	k1gNnSc7
<g/>
)	)	kIx)
nedokázalo	dokázat	k5eNaPmAgNnS
explodovat	explodovat	k5eAaBmF
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestože	přestože	k8xS
útoky	útok	k1gInPc1
torpédových	torpédový	k2eAgMnPc2d1
bombardérů	bombardér	k1gMnPc2
nezaznamenaly	zaznamenat	k5eNaPmAgFnP
žádný	žádný	k3yNgInSc4
zásah	zásah	k1gInSc4
<g/>
,	,	kIx,
dosáhly	dosáhnout	k5eAaPmAgInP
tří	tři	k4xCgInPc2
důležitých	důležitý	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
prvé	prvý	k4xOgFnSc6
nutily	nutit	k5eAaImAgInP
japonské	japonský	k2eAgInPc4d1
nosiče	nosič	k1gInPc4
manévrovat	manévrovat	k5eAaImF
a	a	k8xC
bránily	bránit	k5eAaImAgFnP
jim	on	k3xPp3gFnPc3
tak	tak	k9
v	v	k7c6
přípravě	příprava	k1gFnSc6
a	a	k8xC
startu	start	k1gInSc6
vlastní	vlastní	k2eAgFnSc2d1
útočné	útočný	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
druhé	druhý	k4xOgNnSc4
<g/>
,	,	kIx,
špatné	špatný	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
japonského	japonský	k2eAgNnSc2d1
leteckého	letecký	k2eAgNnSc2d1
hlídkování	hlídkování	k1gNnSc2
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
stíhači	stíhač	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
nevhodných	vhodný	k2eNgFnPc6d1
pozicích	pozice	k1gFnPc6
pro	pro	k7c4
zachycení	zachycení	k1gNnSc4
následných	následný	k2eAgInPc2d1
amerických	americký	k2eAgInPc2d1
útoků	útok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
třetí	třetí	k4xOgFnSc4
<g/>
,	,	kIx,
mnoha	mnoho	k4c3
Zerům	Zerum	k1gNnPc3
docházela	docházet	k5eAaImAgFnS
munice	munice	k1gFnSc2
a	a	k8xC
palivo	palivo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
V	V	kA
10.00	10.00	k4
přišel	přijít	k5eAaPmAgMnS
od	od	k7c2
jihovýchodu	jihovýchod	k1gInSc2
třetí	třetí	k4xOgMnSc1
torpédový	torpédový	k2eAgInSc1d1
útok	útok	k1gInSc1
eskadry	eskadra	k1gFnSc2
VT-3	VT-3	k1gFnSc2
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
<g/>
,	,	kIx,
vedený	vedený	k2eAgInSc1d1
LCDR	LCDR	kA
Lance	lance	k1gNnSc2
Edwardem	Edward	k1gMnSc7
Masseyem	Massey	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
přitáhl	přitáhnout	k5eAaPmAgMnS
většinu	většina	k1gFnSc4
japonské	japonský	k2eAgFnSc2d1
stíhací	stíhací	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
do	do	k7c2
jihovýchodního	jihovýchodní	k2eAgInSc2d1
kvadrantu	kvadrant	k1gInSc2
flotily	flotila	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
Lepší	dobrý	k2eAgFnSc1d2
disciplína	disciplína	k1gFnSc1
a	a	k8xC
nasazení	nasazení	k1gNnSc1
většího	veliký	k2eAgInSc2d2
počtu	počet	k1gInSc2
Zer	Zer	k1gFnSc2
do	do	k7c2
leteckého	letecký	k2eAgNnSc2d1
hlídkování	hlídkování	k1gNnSc2
by	by	kYmCp3nS
Nagumovi	Nagumův	k2eAgMnPc1d1
bývalo	bývat	k5eAaImAgNnS
mohlo	moct	k5eAaImAgNnS
pomoci	pomoc	k1gFnSc3
zabránit	zabránit	k5eAaPmF
(	(	kIx(
<g/>
nebo	nebo	k8xC
alespoň	alespoň	k9
zmírnit	zmírnit	k5eAaPmF
<g/>
)	)	kIx)
škody	škoda	k1gFnPc4
způsobené	způsobený	k2eAgFnPc4d1
nadcházejícími	nadcházející	k2eAgInPc7d1
americkými	americký	k2eAgInPc7d1
útoky	útok	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dauntless	Dauntless	k1gInSc1
eskadry	eskadra	k1gFnSc2
VB-8	VB-8	k1gFnSc2
na	na	k7c6
palubě	paluba	k1gFnSc6
USS	USS	kA
Hornet	Hornet	k1gInSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
</s>
<s>
Shodou	shoda	k1gFnSc7
okolností	okolnost	k1gFnPc2
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Japonci	Japonec	k1gMnPc1
zpozorovali	zpozorovat	k5eAaPmAgMnP
eskadru	eskadra	k1gFnSc4
VT-	VT-	k1gFnSc2
<g/>
3	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
od	od	k7c2
jihozápadu	jihozápad	k1gInSc2
a	a	k8xC
severovýchodu	severovýchod	k1gInSc2
přibližovaly	přibližovat	k5eAaImAgFnP
tři	tři	k4xCgFnPc1
eskadry	eskadra	k1gFnPc1
Dauntlessů	Dauntless	k1gInPc2
z	z	k7c2
Enterprise	Enterprise	k1gFnSc2
a	a	k8xC
Yorktown	Yorktowna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eskadra	eskadra	k1gFnSc1
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
(	(	kIx(
<g/>
VB-	VB-	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
letěla	letět	k5eAaImAgFnS
těsně	těsně	k6eAd1
za	za	k7c4
VT-	VT-	k1gFnSc4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
rozhodla	rozhodnout	k5eAaPmAgFnS
se	se	k3xPyFc4
zaútočit	zaútočit	k5eAaPmF
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
směru	směr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběma	dva	k4xCgMnPc3
eskadrám	eskadra	k1gFnPc3
z	z	k7c2
Enterprise	Enterprise	k1gFnSc2
(	(	kIx(
<g/>
VB-	VB-	k1gFnSc1
<g/>
6	#num#	k4
a	a	k8xC
VS-	VS-	k1gMnSc3
<g/>
6	#num#	k4
<g/>
)	)	kIx)
docházelo	docházet	k5eAaImAgNnS
palivo	palivo	k1gNnSc1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
času	čas	k1gInSc3
strávenému	strávený	k2eAgInSc3d1
hledáním	hledání	k1gNnSc7
nepřítele	nepřítel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitel	velitel	k1gMnSc1
letecké	letecký	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
C.	C.	kA
Wade	Wade	k?
McClusky	McCluska	k1gFnSc2
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
hledání	hledání	k1gNnSc6
a	a	k8xC
náhodou	náhodou	k6eAd1
spatřil	spatřit	k5eAaPmAgMnS
vodní	vodní	k2eAgFnSc4d1
brázdu	brázda	k1gFnSc4
japonského	japonský	k2eAgInSc2d1
torpédoborce	torpédoborec	k1gInSc2
Araši	Araše	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
spěchal	spěchat	k5eAaImAgMnS
plnou	plný	k2eAgFnSc7d1
parou	para	k1gFnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
znovu	znovu	k6eAd1
připojil	připojit	k5eAaPmAgInS
k	k	k7c3
Nagumovým	Nagumův	k2eAgFnPc3d1
letadlovým	letadlový	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
neúspěšně	úspěšně	k6eNd1
napadl	napadnout	k5eAaPmAgMnS
hlubinnými	hlubinný	k2eAgFnPc7d1
pumami	puma	k1gFnPc7
americkou	americký	k2eAgFnSc4d1
ponorku	ponorka	k1gFnSc4
Nautilus	Nautilus	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
předtím	předtím	k6eAd1
také	také	k9
bez	bez	k7c2
úspěchu	úspěch	k1gInSc2
zaútočila	zaútočit	k5eAaPmAgFnS
na	na	k7c4
bitevní	bitevní	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Kirišima	Kirišimum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
Několik	několik	k4yIc1
bombardérů	bombardér	k1gMnPc2
bylo	být	k5eAaImAgNnS
ztraceno	ztratit	k5eAaPmNgNnS
ještě	ještě	k9
před	před	k7c7
zahájením	zahájení	k1gNnSc7
útoku	útok	k1gInSc2
následkem	následkem	k7c2
vyčerpání	vyčerpání	k1gNnSc2
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
McCluskyho	McCluskyze	k6eAd1
rozhodnutí	rozhodnutí	k1gNnSc4
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
pátrání	pátrání	k1gNnSc6
a	a	k8xC
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
úsudek	úsudek	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
názoru	názor	k1gInSc2
admirála	admirál	k1gMnSc2
Chestera	Chester	k1gMnSc2
Nimitze	Nimitze	k1gFnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
rozhodly	rozhodnout	k5eAaPmAgInP
o	o	k7c6
osudu	osud	k1gInSc6
našich	náš	k3xOp1gFnPc2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
našich	náš	k3xOp1gFnPc2
sil	síla	k1gFnPc2
na	na	k7c6
Midwayi	Midway	k1gFnSc6
<g/>
…	…	k?
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgFnPc4
tři	tři	k4xCgFnPc4
americké	americký	k2eAgFnPc4d1
eskadry	eskadra	k1gFnPc4
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
(	(	kIx(
<g/>
VB-	VB-	k1gFnPc2
<g/>
6	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
VS-6	VS-6	k1gFnSc1
a	a	k8xC
VB-	VB-	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
dorazily	dorazit	k5eAaPmAgInP
téměř	téměř	k6eAd1
současně	současně	k6eAd1
ve	v	k7c4
správný	správný	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
,	,	kIx,
místo	místo	k1gNnSc4
a	a	k8xC
výšku	výška	k1gFnSc4
k	k	k7c3
útoku	útok	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
Většina	většina	k1gFnSc1
japonské	japonský	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
soustředila	soustředit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
pozornost	pozornost	k1gFnSc4
na	na	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
torpédové	torpédový	k2eAgInPc1d1
bombardéry	bombardér	k1gInPc1
VT-3	VT-3	k1gFnSc3
a	a	k8xC
nacházela	nacházet	k5eAaImAgFnS
se	se	k3xPyFc4
mimo	mimo	k7c4
vhodnou	vhodný	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
<g/>
;	;	kIx,
mezitím	mezitím	k6eAd1
se	se	k3xPyFc4
hangárové	hangárový	k2eAgFnPc1d1
paluby	paluba	k1gFnPc1
japonských	japonský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
zaplnily	zaplnit	k5eAaPmAgFnP
vyzbrojenými	vyzbrojený	k2eAgInPc7d1
letouny	letoun	k1gInPc7
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
ve	v	k7c6
spěchu	spěch	k1gInSc6
dokončovanému	dokončovaný	k2eAgNnSc3d1
tankování	tankování	k1gNnSc3
se	se	k3xPyFc4
napříč	napříč	k7c7
palubami	paluba	k1gFnPc7
táhly	táhnout	k5eAaImAgFnP
palivové	palivový	k2eAgFnPc4d1
hadice	hadice	k1gFnPc4
a	a	k8xC
opakované	opakovaný	k2eAgNnSc1d1
přezbrojování	přezbrojování	k1gNnSc1
zavinilo	zavinit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
kolem	kolem	k7c2
hangárů	hangár	k1gInPc2
povalovaly	povalovat	k5eAaImAgFnP
bomby	bomba	k1gFnPc1
a	a	k8xC
torpéda	torpédo	k1gNnPc1
<g/>
,	,	kIx,
místo	místo	k6eAd1
aby	aby	kYmCp3nP
byly	být	k5eAaImAgInP
bezpečně	bezpečně	k6eAd1
uložené	uložený	k2eAgInPc1d1
v	v	k7c6
muničních	muniční	k2eAgNnPc6d1
skladištích	skladiště	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
činilo	činit	k5eAaImAgNnS
japonské	japonský	k2eAgInPc4d1
nosiče	nosič	k1gInPc4
mimořádně	mimořádně	k6eAd1
zranitelnými	zranitelný	k2eAgMnPc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počínaje	počínaje	k7c7
10.22	10.22	k4
se	se	k3xPyFc4
dvě	dva	k4xCgFnPc1
eskadry	eskadra	k1gFnPc1
z	z	k7c2
letecké	letecký	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Enterprise	Enterprise	k1gFnSc2
rozdělily	rozdělit	k5eAaPmAgFnP
<g/>
,	,	kIx,
s	s	k7c7
úmyslem	úmysl	k1gInSc7
vyslat	vyslat	k5eAaPmF
jednu	jeden	k4xCgFnSc4
eskadru	eskadra	k1gFnSc4
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
Kagu	Kaga	k1gFnSc4
a	a	k8xC
druhou	druhý	k4xOgFnSc7
na	na	k7c4
Akagi	Akage	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedorozumění	nedorozumění	k1gNnSc1
způsobilo	způsobit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
obě	dva	k4xCgFnPc1
eskadry	eskadra	k1gFnPc1
vrhly	vrhnout	k5eAaImAgFnP,k5eAaPmAgFnP
střemhlav	střemhlav	k6eAd1
na	na	k7c6
Kagu	Kagus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
si	se	k3xPyFc3
poručík	poručík	k1gMnSc1
Richard	Richard	k1gMnSc1
Halsey	Halsea	k1gFnSc2
Best	Best	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnPc4
dvě	dva	k4xCgNnPc4
čísla	číslo	k1gNnPc4
všimli	všimnout	k5eAaPmAgMnP
omylu	omyl	k1gInSc2
<g/>
,	,	kIx,
dokázali	dokázat	k5eAaPmAgMnP
útok	útok	k1gInSc4
přerušit	přerušit	k5eAaPmF
a	a	k8xC
vybrat	vybrat	k5eAaPmF
klesání	klesání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
usoudili	usoudit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Kaga	Kaga	k1gFnSc1
je	být	k5eAaImIp3nS
odsouzena	odsoudit	k5eAaPmNgFnS,k5eAaImNgFnS
k	k	k7c3
zániku	zánik	k1gInSc3
<g/>
,	,	kIx,
zamířili	zamířit	k5eAaPmAgMnP
na	na	k7c4
sever	sever	k1gInSc4
a	a	k8xC
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
Akagi	Akage	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaga	Kaga	k1gFnSc1
pod	pod	k7c7
náporem	nápor	k1gInSc7
bomb	bomba	k1gFnPc2
od	od	k7c2
téměř	téměř	k6eAd1
dvou	dva	k4xCgFnPc2
kompletních	kompletní	k2eAgFnPc2d1
eskader	eskadra	k1gFnPc2
utrpěla	utrpět	k5eAaPmAgFnS
nejméně	málo	k6eAd3
čtyři	čtyři	k4xCgInPc4
přímé	přímý	k2eAgInPc4d1
zásahy	zásah	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
způsobily	způsobit	k5eAaPmAgFnP
těžké	těžký	k2eAgFnPc1d1
škody	škoda	k1gFnPc1
a	a	k8xC
propuknutí	propuknutí	k1gNnSc1
několika	několik	k4yIc2
požárů	požár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
bomb	bomba	k1gFnPc2
dopadla	dopadnout	k5eAaPmAgFnS
na	na	k7c4
můstek	můstek	k1gInSc4
nebo	nebo	k8xC
přímo	přímo	k6eAd1
před	před	k7c7
něj	on	k3xPp3gMnSc2
a	a	k8xC
zabila	zabít	k5eAaPmAgFnS
kapitána	kapitán	k1gMnSc4
Džisaku	Džisak	k1gMnSc3
Okadu	Okada	k1gFnSc4
a	a	k8xC
většinu	většina	k1gFnSc4
vyšších	vysoký	k2eAgMnPc2d2
důstojníků	důstojník	k1gMnPc2
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
Poručík	poručík	k1gMnSc1
Clarence	Clarenec	k1gMnSc2
E.	E.	kA
Dickinson	Dickinson	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
součástí	součást	k1gFnSc7
McCluskyho	McClusky	k1gMnSc2
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
vzpomínal	vzpomínat	k5eAaImAgMnS
<g/>
:	:	kIx,
</s>
<s>
Šli	jít	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
dolů	dolů	k6eAd1
jsme	být	k5eAaImIp1nP
ze	z	k7c2
všech	všecek	k3xTgInPc2
směrů	směr	k1gInPc2
na	na	k7c4
levobok	levobok	k1gInSc4
lodi	loď	k1gFnSc2
…	…	k?
identifikoval	identifikovat	k5eAaBmAgInS
jsem	být	k5eAaImIp1nS
ji	on	k3xPp3gFnSc4
jako	jako	k9
Kaga	Kag	k2eAgFnSc1d1
<g/>
;	;	kIx,
a	a	k8xC
byla	být	k5eAaImAgFnS
ohromná	ohromný	k2eAgFnSc1d1
…	…	k?
Cíl	cíl	k1gInSc1
byl	být	k5eAaImAgInS
naprosto	naprosto	k6eAd1
uspokojivý	uspokojivý	k2eAgMnSc1d1
…	…	k?
Spatřil	spatřit	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
zásah	zásah	k1gInSc4
bomby	bomba	k1gFnSc2
těsně	těsně	k6eAd1
za	za	k7c7
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
kam	kam	k6eAd1
jsem	být	k5eAaImIp1nS
mířil	mířit	k5eAaImAgMnS
…	…	k?
Sledoval	sledovat	k5eAaImAgInS
jsem	být	k5eAaImIp1nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
paluba	paluba	k1gFnSc1
kroutí	kroutit	k5eAaImIp3nS
a	a	k8xC
rozevírá	rozevírat	k5eAaImIp3nS
na	na	k7c4
všechny	všechen	k3xTgFnPc4
strany	strana	k1gFnPc4
a	a	k8xC
odhaluje	odhalovat	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
hangáru	hangár	k1gInSc2
pod	pod	k7c7
ní	on	k3xPp3gFnSc7
<g/>
…	…	k?
Viděl	vidět	k5eAaImAgInS
jsem	být	k5eAaImIp1nS
[	[	kIx(
<g/>
svou	svůj	k3xOyFgFnSc4
<g/>
]	]	kIx)
pětisetliberní	pětisetliberní	k?
bombu	bomba	k1gFnSc4
[	[	kIx(
<g/>
230	#num#	k4
kg	kg	kA
<g/>
]	]	kIx)
dopadnout	dopadnout	k5eAaPmF
přímo	přímo	k6eAd1
vedle	vedle	k7c2
ostrova	ostrov	k1gInSc2
[	[	kIx(
<g/>
letadlové	letadlový	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc4
stoliberní	stoliberní	k?
[	[	kIx(
<g/>
45	#num#	k4
kg	kg	kA
<g/>
]	]	kIx)
bomby	bomba	k1gFnPc1
spadly	spadnout	k5eAaPmAgFnP
na	na	k7c4
přední	přední	k2eAgFnSc4d1
část	část	k1gFnSc4
paluby	paluba	k1gFnSc2
mezi	mezi	k7c4
zaparkovaná	zaparkovaný	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
…	…	k?
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
několik	několik	k4yIc4
minut	minuta	k1gFnPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
Best	Best	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnPc4
dvě	dva	k4xCgNnPc4
čísla	číslo	k1gNnPc4
vrhli	vrhnout	k5eAaPmAgMnP,k5eAaImAgMnP
na	na	k7c4
Akagi	Akage	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Micuo	Micuo	k1gNnSc1
Fučida	Fučida	k1gFnSc1
<g/>
,	,	kIx,
japonský	japonský	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vedl	vést	k5eAaImAgMnS
útok	útok	k1gInSc4
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
přítomen	přítomen	k2eAgMnSc1d1
na	na	k7c6
Akagi	Akag	k1gFnSc6
v	v	k7c6
době	doba	k1gFnSc6
jejího	její	k3xOp3gInSc2
zásahu	zásah	k1gInSc2
a	a	k8xC
útok	útok	k1gInSc4
popsal	popsat	k5eAaPmAgMnS
<g/>
:	:	kIx,
</s>
<s>
Hlídka	hlídka	k1gFnSc1
zakřičela	zakřičet	k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Bombardéry	bombardér	k1gInPc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Vzhlédl	vzhlédnout	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
a	a	k8xC
uviděl	uvidět	k5eAaPmAgMnS
tři	tři	k4xCgInPc4
černé	černý	k2eAgInPc4d1
nepřátelské	přátelský	k2eNgInPc4d1
letouny	letoun	k1gInPc4
<g/>
,	,	kIx,
řítící	řítící	k2eAgInPc4d1
se	se	k3xPyFc4
z	z	k7c2
nebe	nebe	k1gNnSc2
na	na	k7c4
naši	náš	k3xOp1gFnSc4
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pár	pár	k4xCyI
našich	náš	k3xOp1gInPc2
kulometů	kulomet	k1gInPc2
na	na	k7c4
ně	on	k3xPp3gInPc4
dokázalo	dokázat	k5eAaPmAgNnS
vystřelit	vystřelit	k5eAaPmF
několik	několik	k4yIc4
zběsilých	zběsilý	k2eAgFnPc2d1
dávek	dávka	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
už	už	k6eAd1
příliš	příliš	k6eAd1
pozdě	pozdě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bachraté	bachratý	k2eAgFnPc4d1
siluety	silueta	k1gFnPc4
amerických	americký	k2eAgMnPc2d1
Dauntlessů	Dauntless	k1gMnPc2
se	se	k3xPyFc4
rychle	rychle	k6eAd1
zvětšovaly	zvětšovat	k5eAaImAgFnP
a	a	k8xC
poté	poté	k6eAd1
náhle	náhle	k6eAd1
zpod	zpod	k7c2
jejich	jejich	k3xOp3gNnPc2
křídel	křídlo	k1gNnPc2
vyklouzla	vyklouznout	k5eAaPmAgFnS
spousta	spousta	k1gFnSc1
děsivých	děsivý	k2eAgInPc2d1
temných	temný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestože	přestože	k8xS
Akagi	Akagi	k1gNnSc4
utrpěla	utrpět	k5eAaPmAgFnS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
přímý	přímý	k2eAgInSc4d1
zásah	zásah	k1gInSc4
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
jistě	jistě	k9
pumou	puma	k1gFnSc7
poručíka	poručík	k1gMnSc2
Besta	Best	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ukázal	ukázat	k5eAaPmAgInS
se	se	k3xPyFc4
jako	jako	k9
smrtelný	smrtelný	k2eAgInSc1d1
<g/>
:	:	kIx,
bomba	bomba	k1gFnSc1
zasáhla	zasáhnout	k5eAaPmAgFnS
okraj	okraj	k1gInSc4
palubního	palubní	k2eAgInSc2d1
výtahu	výtah	k1gInSc2
ve	v	k7c6
středolodí	středolodí	k1gNnSc6
a	a	k8xC
pronikla	proniknout	k5eAaPmAgFnS
na	na	k7c4
horní	horní	k2eAgFnSc4d1
hangárovou	hangárový	k2eAgFnSc4d1
palubu	paluba	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
vybuchla	vybuchnout	k5eAaPmAgFnS
mezi	mezi	k7c7
vyzbrojenými	vyzbrojený	k2eAgNnPc7d1
a	a	k8xC
natankovanými	natankovaný	k2eAgNnPc7d1
letadly	letadlo	k1gNnPc7
okolo	okolo	k7c2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náčelník	náčelník	k1gInSc1
Nagumova	Nagumův	k2eAgInSc2d1
štábu	štáb	k1gInSc2
<g/>
,	,	kIx,
Rjúnosuke	Rjúnosuk	k1gMnSc2
Kusaka	Kusak	k1gMnSc2
<g/>
,	,	kIx,
si	se	k3xPyFc3
vybavil	vybavit	k5eAaPmAgInS
„	„	k?
<g/>
strašlivý	strašlivý	k2eAgInSc1d1
požár	požár	k1gInSc1
…	…	k?
všude	všude	k6eAd1
těla	tělo	k1gNnSc2
…	…	k?
letadla	letadlo	k1gNnSc2
stála	stát	k5eAaImAgFnS
ocasem	ocas	k1gInSc7
vzhůru	vzhůru	k6eAd1
<g/>
,	,	kIx,
chrlila	chrlit	k5eAaImAgFnS
líté	lítý	k2eAgInPc4d1
plameny	plamen	k1gInPc4
a	a	k8xC
mraky	mrak	k1gInPc4
černého	černý	k2eAgInSc2d1
dýmu	dým	k1gInSc2
<g/>
,	,	kIx,
dostat	dostat	k5eAaPmF
požáry	požár	k1gInPc4
pod	pod	k7c4
kontrolu	kontrola	k1gFnSc4
bylo	být	k5eAaImAgNnS
nemožné	možný	k2eNgNnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnSc1d1
bomba	bomba	k1gFnSc1
explodovala	explodovat	k5eAaBmAgFnS
pod	pod	k7c7
vodou	voda	k1gFnSc7
těsně	těsně	k6eAd1
za	za	k7c2
zádí	zádí	k1gNnSc2
<g/>
;	;	kIx,
následný	následný	k2eAgInSc1d1
gejzír	gejzír	k1gInSc1
ohnul	ohnout	k5eAaPmAgInS
letovou	letový	k2eAgFnSc4d1
palubu	paluba	k1gFnSc4
vzhůru	vzhůru	k6eAd1
„	„	k?
<g/>
do	do	k7c2
groteskních	groteskní	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
<g/>
“	“	k?
a	a	k8xC
silně	silně	k6eAd1
poškodil	poškodit	k5eAaPmAgMnS
kormidlo	kormidlo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současně	současně	k6eAd1
eskadra	eskadra	k1gFnSc1
VB-3	VB-3	k1gFnSc1
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
<g/>
,	,	kIx,
pod	pod	k7c7
velením	velení	k1gNnSc7
Maxe	Max	k1gMnSc2
Leslieho	Leslie	k1gMnSc2
<g/>
,	,	kIx,
zaútočila	zaútočit	k5eAaPmAgFnS
na	na	k7c4
Sórjú	Sórjú	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosáhla	dosáhnout	k5eAaPmAgFnS
nejméně	málo	k6eAd3
tří	tři	k4xCgInPc2
přímých	přímý	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
způsobily	způsobit	k5eAaPmAgFnP
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznícený	vznícený	k2eAgInSc1d1
benzín	benzín	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
ohnivé	ohnivý	k2eAgNnSc4d1
inferno	inferno	k1gNnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
všude	všude	k6eAd1
vybuchovaly	vybuchovat	k5eAaImAgFnP
uložené	uložený	k2eAgFnPc1d1
pumy	puma	k1gFnPc1
a	a	k8xC
munice	munice	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
VT-3	VT-3	k1gFnSc1
si	se	k3xPyFc3
vybrala	vybrat	k5eAaPmAgFnS
za	za	k7c4
cíl	cíl	k1gInSc4
Hirjú	Hirjú	k1gFnSc2
<g/>
,	,	kIx,
plující	plující	k2eAgFnSc2d1
mezi	mezi	k7c4
Sórjú	Sórjú	k1gFnSc4
<g/>
,	,	kIx,
Kagou	Kagá	k1gFnSc4
a	a	k8xC
Akagi	Akage	k1gFnSc4
<g/>
,	,	kIx,
nepodařil	podařit	k5eNaPmAgMnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
však	však	k9
žádný	žádný	k3yNgInSc4
zásah	zásah	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
šesti	šest	k4xCc2
minut	minuta	k1gFnPc2
se	se	k3xPyFc4
Sórjú	Sórjú	k1gFnSc1
a	a	k8xC
Kaga	Kaga	k1gFnSc1
ocitly	ocitnout	k5eAaPmAgInP
v	v	k7c6
plamenech	plamen	k1gInPc6
od	od	k7c2
přídě	příď	k1gFnSc2
po	po	k7c4
záď	záď	k1gFnSc4
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgNnSc7
jak	jak	k9
požáry	požár	k1gInPc1
postupovaly	postupovat	k5eAaImAgInP
lodí	loď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akagi	Akagi	k1gNnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
zasáhla	zasáhnout	k5eAaPmAgFnS
pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
puma	puma	k1gFnSc1
<g/>
,	,	kIx,
trvalo	trvat	k5eAaImAgNnS
zapálení	zapálení	k1gNnSc3
déle	dlouho	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
vzniklé	vzniklý	k2eAgInPc1d1
požáry	požár	k1gInPc1
se	se	k3xPyFc4
rychle	rychle	k6eAd1
rozšířily	rozšířit	k5eAaPmAgFnP
a	a	k8xC
brzy	brzy	k6eAd1
se	se	k3xPyFc4
ukázaly	ukázat	k5eAaPmAgFnP
jako	jako	k9
neuhasitelné	uhasitelný	k2eNgFnPc1d1
<g/>
;	;	kIx,
i	i	k8xC
ji	on	k3xPp3gFnSc4
nakonec	nakonec	k6eAd1
pohltily	pohltit	k5eAaPmAgInP
plameny	plamen	k1gInPc4
a	a	k8xC
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
opuštěna	opuštěn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nagumo	Naguma	k1gFnSc5
<g/>
,	,	kIx,
začínající	začínající	k2eAgMnPc1d1
si	se	k3xPyFc3
uvědomovat	uvědomovat	k5eAaImF
nesmírný	smírný	k2eNgInSc4d1
rozsah	rozsah	k1gInSc4
toho	ten	k3xDgNnSc2
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
se	se	k3xPyFc4
právě	právě	k9
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
upadl	upadnout	k5eAaPmAgMnS
do	do	k7c2
šoku	šok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svědci	svědek	k1gMnPc1
ho	on	k3xPp3gMnSc4
viděli	vidět	k5eAaImAgMnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
stojí	stát	k5eAaImIp3nS
poblíž	poblíž	k7c2
lodního	lodní	k2eAgInSc2d1
kompasu	kompas	k1gInSc2
a	a	k8xC
jakoby	jakoby	k8xS
v	v	k7c6
transu	trans	k1gInSc6
zírá	zírat	k5eAaImIp3nS
do	do	k7c2
plamenů	plamen	k1gInPc2
stravujících	stravující	k2eAgInPc2d1
jeho	jeho	k3xOp3gMnPc2
lodě	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
jej	on	k3xPp3gMnSc4
žádali	žádat	k5eAaImAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
opustil	opustit	k5eAaPmAgMnS
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
Nagumo	Naguma	k1gFnSc5
se	se	k3xPyFc4
nehýbal	hýbat	k5eNaImAgInS
a	a	k8xC
zdráhal	zdráhat	k5eAaImAgInS
se	se	k3xPyFc4
Akagi	Akag	k1gFnSc2
opustit	opustit	k5eAaPmF
<g/>
,	,	kIx,
mumlaje	mumlat	k5eAaImSgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Ještě	ještě	k9
není	být	k5eNaImIp3nS
čas	čas	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Kontradmirál	kontradmirál	k1gMnSc1
Kusaka	Kusak	k1gMnSc2
ho	on	k3xPp3gMnSc4
dokázal	dokázat	k5eAaPmAgMnS
přesvědčit	přesvědčit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
opustil	opustit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
kriticky	kriticky	k6eAd1
poškozenou	poškozený	k2eAgFnSc4d1
vlajkovou	vlajkový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sotva	sotva	k6eAd1
znatelným	znatelný	k2eAgNnSc7d1
přikývnutím	přikývnutí	k1gNnSc7
a	a	k8xC
se	s	k7c7
slzami	slza	k1gFnPc7
v	v	k7c6
očích	oko	k1gNnPc6
svolil	svolit	k5eAaPmAgMnS
Nagumo	Naguma	k1gFnSc5
k	k	k7c3
odchodu	odchod	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
10.46	10.46	k4
admirál	admirál	k1gMnSc1
přenesl	přenést	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
vlajku	vlajka	k1gFnSc4
na	na	k7c4
lehký	lehký	k2eAgInSc4d1
křižník	křižník	k1gInSc4
Nagara	Nagara	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgFnPc4
tři	tři	k4xCgFnPc4
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
zatím	zatím	k6eAd1
zůstávaly	zůstávat	k5eAaImAgInP
na	na	k7c6
hladině	hladina	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
žádná	žádný	k3yNgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
neutrpěla	utrpět	k5eNaPmAgFnS
poškození	poškození	k1gNnSc4
pod	pod	k7c7
čarou	čára	k1gFnSc7
ponoru	ponor	k1gInSc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
poničeného	poničený	k2eAgNnSc2d1
kormidla	kormidlo	k1gNnSc2
Akagi	Akag	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
si	se	k3xPyFc3
Japonci	Japonec	k1gMnPc1
zprvu	zprvu	k6eAd1
dělali	dělat	k5eAaImAgMnP
naděje	naděje	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
Akagi	Akagi	k1gNnSc1
bude	být	k5eAaImBp3nS
možné	možný	k2eAgNnSc1d1
zachránit	zachránit	k5eAaPmF
nebo	nebo	k8xC
alespoň	alespoň	k9
odtáhnout	odtáhnout	k5eAaPmF
zpět	zpět	k6eAd1
do	do	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
všechny	všechen	k3xTgInPc4
tři	tři	k4xCgInPc4
nosiče	nosič	k1gInPc4
opustili	opustit	k5eAaPmAgMnP
a	a	k8xC
potopili	potopit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonské	japonský	k2eAgInPc1d1
protiútoky	protiútok	k1gInPc1
</s>
<s>
USS	USS	kA
Yorktown	Yorktown	k1gNnSc1
<g/>
,	,	kIx,
oprava	oprava	k1gFnSc1
škod	škoda	k1gFnPc2
způsobených	způsobený	k2eAgFnPc2d1
prvním	první	k4xOgInSc7
náletem	nálet	k1gInSc7
z	z	k7c2
Hirjú	Hirjú	k1gFnSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
</s>
<s>
Hirjú	Hirjú	k?
<g/>
,	,	kIx,
jediná	jediný	k2eAgFnSc1d1
přeživší	přeživší	k2eAgFnSc1d1
japonská	japonský	k2eAgFnSc1d1
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
bez	bez	k7c2
otálení	otálení	k1gNnSc2
vyslala	vyslat	k5eAaPmAgFnS
protiútok	protiútok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
útočná	útočný	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
Hirjú	Hirjú	k1gFnSc2
<g/>
,	,	kIx,
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
z	z	k7c2
18	#num#	k4
D3A	D3A	k1gMnPc2
a	a	k8xC
šesti	šest	k4xCc2
doprovodných	doprovodný	k2eAgFnPc2d1
stíhaček	stíhačka	k1gFnPc2
<g/>
,	,	kIx,
sledovala	sledovat	k5eAaImAgFnS
vracející	vracející	k2eAgMnSc1d1
se	se	k3xPyFc4
americké	americký	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
a	a	k8xC
zaútočila	zaútočit	k5eAaPmAgFnS
na	na	k7c4
první	první	k4xOgFnSc4
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
našla	najít	k5eAaPmAgFnS
–	–	k?
Yorktown	Yorktowna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasáhla	zasáhnout	k5eAaPmAgFnS
jej	on	k3xPp3gInSc4
třemi	tři	k4xCgFnPc7
pumami	puma	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
prorazily	prorazit	k5eAaPmAgFnP
velký	velký	k2eAgInSc4d1
otvor	otvor	k1gInSc4
v	v	k7c6
letové	letový	k2eAgFnSc6d1
palubě	paluba	k1gFnSc6
<g/>
,	,	kIx,
zhasly	zhasnout	k5eAaPmAgInP
všechny	všechen	k3xTgInPc1
kotle	kotel	k1gInPc1
kromě	kromě	k7c2
jednoho	jeden	k4xCgInSc2
a	a	k8xC
zničily	zničit	k5eAaPmAgInP
jedno	jeden	k4xCgNnSc4
postavení	postavení	k1gNnSc4
protiletadlových	protiletadlový	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poškození	poškození	k1gNnSc1
také	také	k6eAd1
donutilo	donutit	k5eAaPmAgNnS
admirála	admirál	k1gMnSc4
Fletchera	Fletchero	k1gNnSc2
přesunout	přesunout	k5eAaPmF
své	svůj	k3xOyFgNnSc4
velitelství	velitelství	k1gNnSc4
na	na	k7c4
těžký	těžký	k2eAgInSc4d1
křižník	křižník	k1gInSc4
Astoria	Astorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záchranné	záchranný	k2eAgFnPc1d1
čety	četa	k1gFnPc1
dokázaly	dokázat	k5eAaPmAgFnP
během	během	k7c2
jedné	jeden	k4xCgFnSc2
hodiny	hodina	k1gFnSc2
provizorně	provizorně	k6eAd1
opravit	opravit	k5eAaPmF
letovou	letový	k2eAgFnSc4d1
palubu	paluba	k1gFnSc4
a	a	k8xC
opětovně	opětovně	k6eAd1
spustit	spustit	k5eAaPmF
několik	několik	k4yIc4
kotlů	kotel	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
lodi	loď	k1gFnPc4
umožnilo	umožnit	k5eAaPmAgNnS
udržet	udržet	k5eAaPmF
rychlost	rychlost	k1gFnSc4
19	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
35	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
a	a	k8xC
dovolilo	dovolit	k5eAaPmAgNnS
jí	on	k3xPp3gFnSc3
obnovit	obnovit	k5eAaPmF
letecký	letecký	k2eAgInSc4d1
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yorktown	Yorktown	k1gInSc1
stáhla	stáhnout	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
žlutou	žlutý	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
znamenající	znamenající	k2eAgFnSc1d1
„	„	k?
<g/>
vážná	vážný	k2eAgFnSc1d1
porucha	porucha	k1gFnSc1
<g/>
“	“	k?
a	a	k8xC
nahoru	nahoru	k6eAd1
šla	jít	k5eAaImAgFnS
nová	nový	k2eAgFnSc1d1
–	–	k?
„	„	k?
<g/>
moje	můj	k3xOp1gNnSc1
rychlost	rychlost	k1gFnSc1
5	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
Kapitán	kapitán	k1gMnSc1
Buckmaster	Buckmaster	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
signalisty	signalista	k1gMnPc4
vyvěsit	vyvěsit	k5eAaPmF
na	na	k7c6
předním	přední	k2eAgInSc6d1
stožáru	stožár	k1gInSc6
novou	nový	k2eAgFnSc4d1
obrovskou	obrovský	k2eAgFnSc4d1
(	(	kIx(
<g/>
10	#num#	k4
stop	stopa	k1gFnPc2
širokou	široký	k2eAgFnSc4d1
a	a	k8xC
15	#num#	k4
stop	stop	k2eAgFnSc4d1
dlouhou	dlouhý	k2eAgFnSc4d1
<g/>
)	)	kIx)
americkou	americký	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třináct	třináct	k4xCc1
japonských	japonský	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
a	a	k8xC
tři	tři	k4xCgFnPc1
stíhačky	stíhačka	k1gFnPc1
byly	být	k5eAaImAgFnP
při	při	k7c6
tomto	tento	k3xDgInSc6
útoku	útok	k1gInSc6
ztraceny	ztracen	k2eAgFnPc1d1
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
doprovodné	doprovodný	k2eAgFnPc1d1
stíhačky	stíhačka	k1gFnPc1
se	se	k3xPyFc4
vrátily	vrátit	k5eAaPmAgFnP
předčasně	předčasně	k6eAd1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
poškození	poškození	k1gNnSc3
utrpěných	utrpěný	k2eAgFnPc2d1
během	během	k7c2
útoku	útok	k1gInSc2
na	na	k7c4
několik	několik	k4yIc4
Dauntlessů	Dauntless	k1gInPc2
z	z	k7c2
Enterprise	Enterprise	k1gFnSc2
<g/>
,	,	kIx,
vracejících	vracející	k2eAgMnPc2d1
se	se	k3xPyFc4
z	z	k7c2
náletu	nálet	k1gInSc2
na	na	k7c4
japonské	japonský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
USS	USS	kA
Yorktown	Yorktown	k1gNnSc1
zasažena	zasažen	k2eAgFnSc1d1
torpédem	torpédo	k1gNnSc7
během	během	k7c2
druhé	druhý	k4xOgFnSc2
vlny	vlna	k1gFnSc2
náletů	nálet	k1gInPc2
z	z	k7c2
Hirjú	Hirjú	k1gFnSc2
</s>
<s>
Přibližně	přibližně	k6eAd1
o	o	k7c4
hodinu	hodina	k1gFnSc4
později	pozdě	k6eAd2
dorazila	dorazit	k5eAaPmAgFnS
k	k	k7c3
Yorktownu	Yorktowna	k1gFnSc4
druhá	druhý	k4xOgFnSc1
útočná	útočný	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
z	z	k7c2
Hirjú	Hirjú	k1gFnSc2
<g/>
,	,	kIx,
tvořená	tvořený	k2eAgFnSc1d1
deseti	deset	k4xCc6
B5N	B5N	k1gFnPc6
doprovázených	doprovázený	k2eAgNnPc2d1
šesti	šest	k4xCc2
A	a	k9
<g/>
6	#num#	k4
<g/>
M	M	kA
<g/>
;	;	kIx,
při	při	k7c6
likvidaci	likvidace	k1gFnSc6
škod	škoda	k1gFnPc2
odvedla	odvést	k5eAaPmAgFnS
posádka	posádka	k1gFnSc1
lodi	loď	k1gFnSc2
natolik	natolik	k6eAd1
efektivní	efektivní	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
japonští	japonský	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
považovali	považovat	k5eAaImAgMnP
Yorktown	Yorktown	k1gInSc4
za	za	k7c4
jiný	jiný	k2eAgInSc4d1
<g/>
,	,	kIx,
nepoškozený	poškozený	k2eNgInSc4d1
nosič	nosič	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
Zaútočili	zaútočit	k5eAaPmAgMnP
a	a	k8xC
ochromili	ochromit	k5eAaPmAgMnP
jej	on	k3xPp3gMnSc4
dvěma	dva	k4xCgNnPc7
torpédy	torpédo	k1gNnPc7
<g/>
;	;	kIx,
loď	loď	k1gFnSc1
ztratila	ztratit	k5eAaPmAgFnS
veškerou	veškerý	k3xTgFnSc4
energii	energie	k1gFnSc4
a	a	k8xC
postupně	postupně	k6eAd1
se	se	k3xPyFc4
naklonila	naklonit	k5eAaPmAgFnS
o	o	k7c4
23	#num#	k4
stupňů	stupeň	k1gInPc2
na	na	k7c4
levobok	levobok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tomto	tento	k3xDgInSc6
útoku	útok	k1gInSc6
bylo	být	k5eAaImAgNnS
sestřeleno	sestřelit	k5eAaPmNgNnS
pět	pět	k4xCc4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
a	a	k8xC
dvě	dva	k4xCgFnPc4
stíhačky	stíhačka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zprávy	zpráva	k1gFnPc1
o	o	k7c6
dvou	dva	k4xCgInPc6
úderech	úder	k1gInPc6
spolu	spolu	k6eAd1
s	s	k7c7
mylnými	mylný	k2eAgNnPc7d1
hlášeními	hlášení	k1gNnPc7
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgInSc1
potopil	potopit	k5eAaPmAgInS
americkou	americký	k2eAgFnSc4d1
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
Japoncům	Japonec	k1gMnPc3
značně	značně	k6eAd1
pozdvihly	pozdvihnout	k5eAaPmAgFnP
morálku	morálka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těch	ten	k3xDgMnPc2
několik	několik	k4yIc1
málo	málo	k6eAd1
přeživších	přeživší	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
postupně	postupně	k6eAd1
přistálo	přistát	k5eAaImAgNnS,k5eAaPmAgNnS
na	na	k7c4
Hirjú	Hirjú	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
velké	velký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
Japonci	Japonec	k1gMnPc1
věřili	věřit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
dokážou	dokázat	k5eAaPmIp3nP
shromáždit	shromáždit	k5eAaPmF
dostatek	dostatek	k1gInSc4
letadel	letadlo	k1gNnPc2
na	na	k7c4
další	další	k2eAgInSc4d1
úder	úder	k1gInSc4
proti	proti	k7c3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
věřili	věřit	k5eAaImAgMnP
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc1d1
zbývající	zbývající	k2eAgFnPc4d1
americké	americký	k2eAgFnPc4d1
letadlové	letadlový	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americký	americký	k2eAgInSc1d1
protiútok	protiútok	k1gInSc1
</s>
<s>
Pozdě	pozdě	k6eAd1
odpoledne	odpoledne	k6eAd1
průzkumné	průzkumný	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
nalezlo	nalézt	k5eAaBmAgNnS,k5eAaPmAgNnS
Hirjú	Hirjú	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
přimělo	přimět	k5eAaPmAgNnS
Enterprise	Enterprise	k1gFnPc4
vyslat	vyslat	k5eAaPmF
poslední	poslední	k2eAgFnSc4d1
vlnu	vlna	k1gFnSc4
24	#num#	k4
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
šesti	šest	k4xCc2
SBD	SBD	kA
z	z	k7c2
VS-	VS-	k1gFnSc2
<g/>
6	#num#	k4
<g/>
,	,	kIx,
čtyř	čtyři	k4xCgMnPc2
SBD	SBD	kA
z	z	k7c2
VB-6	VB-6	k1gFnSc2
a	a	k8xC
14	#num#	k4
SBD	SBD	kA
z	z	k7c2
VB-3	VB-3	k1gFnSc2
Yorktownu	Yorktown	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
Hirjú	Hirjú	k1gMnPc2
bránilo	bránit	k5eAaImAgNnS
silné	silný	k2eAgNnSc4d1
stíhací	stíhací	k2eAgNnSc4d1
krytí	krytí	k1gNnSc4
více	hodně	k6eAd2
než	než	k8xS
tuctu	tucet	k1gInSc2
Zer	Zer	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
útok	útok	k1gInSc1
bombardérů	bombardér	k1gInPc2
Enterprise	Enterprise	k1gFnSc2
a	a	k8xC
osiřelého	osiřelý	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
Yorktownu	Yorktown	k1gInSc2
startujícího	startující	k2eAgInSc2d1
z	z	k7c2
Enterprise	Enterprise	k1gFnSc2
úspěšný	úspěšný	k2eAgMnSc1d1
<g/>
:	:	kIx,
Hirjú	Hirjú	k1gFnPc1
zasáhly	zasáhnout	k5eAaPmAgFnP
čtyři	čtyři	k4xCgFnPc4
bomby	bomba	k1gFnPc4
(	(	kIx(
<g/>
možná	možná	k9
pět	pět	k4xCc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
ji	on	k3xPp3gFnSc4
zanechaly	zanechat	k5eAaPmAgInP
v	v	k7c6
plamenech	plamen	k1gInPc6
a	a	k8xC
neschopnou	schopný	k2eNgFnSc4d1
leteckého	letecký	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útočná	útočný	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
z	z	k7c2
Hornetu	Hornet	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
odstartovala	odstartovat	k5eAaPmAgFnS
pozdě	pozdě	k6eAd1
kvůli	kvůli	k7c3
chybě	chyba	k1gFnSc3
v	v	k7c6
komunikaci	komunikace	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
soustředila	soustředit	k5eAaPmAgFnS
na	na	k7c4
zbývající	zbývající	k2eAgFnPc4d1
doprovodné	doprovodný	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
nepodařilo	podařit	k5eNaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
docílit	docílit	k5eAaPmF
žádného	žádný	k3yNgInSc2
zásahu	zásah	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Opuštěná	opuštěný	k2eAgFnSc1d1
Hirjú	Hirjú	k1gFnSc1
ráno	ráno	k1gNnSc1
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohled	pohled	k1gInSc1
od	od	k7c2
přídě	příď	k1gFnSc2
na	na	k7c4
zhroucenou	zhroucený	k2eAgFnSc4d1
letovou	letový	k2eAgFnSc4d1
palubu	paluba	k1gFnSc4
nad	nad	k7c7
předním	přední	k2eAgInSc7d1
hangárem	hangár	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografie	fotografie	k1gFnSc1
pořízená	pořízený	k2eAgFnSc1d1
letounem	letoun	k1gInSc7
Jokosuka	Jokosuk	k1gMnSc2
B4Y	B4Y	k1gMnSc2
z	z	k7c2
letadlové	letadlový	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Hóšó	Hóšó	k1gFnSc2
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
marných	marný	k2eAgInPc6d1
pokusech	pokus	k1gInPc6
zvládnout	zvládnout	k5eAaPmF
požáry	požár	k1gInPc4
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
zbývající	zbývající	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
Hirjú	Hirjú	k1gFnSc1
evakuována	evakuovat	k5eAaBmNgFnS
a	a	k8xC
zbytek	zbytek	k1gInSc1
flotily	flotila	k1gFnSc2
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
plavbě	plavba	k1gFnSc6
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
snaze	snaha	k1gFnSc6
najít	najít	k5eAaPmF
americké	americký	k2eAgFnPc4d1
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
úmyslem	úmysl	k1gInSc7
hořící	hořící	k2eAgInSc4d1
vrak	vrak	k1gInSc4
potopit	potopit	k5eAaPmF
jej	on	k3xPp3gInSc4
japonský	japonský	k2eAgInSc4d1
torpédoborec	torpédoborec	k1gInSc4
zasáhl	zasáhnout	k5eAaPmAgMnS
torpédem	torpédo	k1gNnSc7
a	a	k8xC
poté	poté	k6eAd1
rychle	rychle	k6eAd1
odplul	odplout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hirjú	Hirjú	k1gMnSc1
se	se	k3xPyFc4
přesto	přesto	k8xC
držela	držet	k5eAaImAgFnS
na	na	k7c6
hladině	hladina	k1gFnSc6
ještě	ještě	k6eAd1
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
den	dna	k1gFnPc2
ráno	ráno	k6eAd1
ji	on	k3xPp3gFnSc4
objevilo	objevit	k5eAaPmAgNnS
letadlo	letadlo	k1gNnSc1
z	z	k7c2
doprovodné	doprovodný	k2eAgFnSc2d1
letadlové	letadlový	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Hóšó	Hóšó	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vzbudilo	vzbudit	k5eAaPmAgNnS
naději	naděje	k1gFnSc4
na	na	k7c4
její	její	k3xOp3gFnSc4
záchranu	záchrana	k1gFnSc4
nebo	nebo	k8xC
alespoň	alespoň	k9
odtažení	odtažený	k2eAgMnPc1d1
zpět	zpět	k6eAd1
do	do	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
spatřena	spatřen	k2eAgNnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
však	však	k9
Hirjú	Hirjú	k1gFnSc1
potopila	potopit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
lodním	lodní	k2eAgMnSc7d1
kapitánem	kapitán	k1gMnSc7
Tomeo	Tomeo	k1gMnSc1
Kakou	Kaka	k1gMnSc7
na	na	k7c6
ní	on	k3xPp3gFnSc6
dobrovolně	dobrovolně	k6eAd1
zůstal	zůstat	k5eAaPmAgMnS
také	také	k9
kontradmirál	kontradmirál	k1gMnSc1
Tamon	Tamon	k1gInSc1
Jamaguči	Jamaguč	k1gFnSc3
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
Japonsko	Japonsko	k1gNnSc1
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
možná	možná	k6eAd1
nejlepšího	dobrý	k2eAgMnSc2d3
důstojníka	důstojník	k1gMnSc2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
příchodem	příchod	k1gInSc7
soumraku	soumrak	k1gInSc2
začaly	začít	k5eAaPmAgFnP
obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
hodnotit	hodnotit	k5eAaImF
svoji	svůj	k3xOyFgFnSc4
situaci	situace	k1gFnSc4
a	a	k8xC
připravovat	připravovat	k5eAaImF
předběžné	předběžný	k2eAgInPc4d1
plány	plán	k1gInPc4
pro	pro	k7c4
další	další	k2eAgInSc4d1
boj	boj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Admirál	admirál	k1gMnSc1
Fletcher	Fletchra	k1gFnPc2
<g/>
,	,	kIx,
musel	muset	k5eAaImAgInS
opustit	opustit	k5eAaPmF
ochromený	ochromený	k2eAgInSc1d1
Yorktown	Yorktown	k1gInSc1
a	a	k8xC
protože	protože	k8xS
cítil	cítit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nedokáže	dokázat	k5eNaPmIp3nS
adekvátně	adekvátně	k6eAd1
velet	velet	k5eAaImF
z	z	k7c2
paluby	paluba	k1gFnSc2
křižníku	křižník	k1gInSc2
<g/>
,	,	kIx,
předal	předat	k5eAaPmAgMnS
operační	operační	k2eAgNnSc4d1
velení	velení	k1gNnSc4
Spruanceovi	Spruanceův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
si	se	k3xPyFc3
uvědomoval	uvědomovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
dosáhly	dosáhnout	k5eAaPmAgInP
velkého	velký	k2eAgNnSc2d1
vítězství	vítězství	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
stále	stále	k6eAd1
si	se	k3xPyFc3
nebyl	být	k5eNaImAgMnS
jistý	jistý	k2eAgMnSc1d1
<g/>
,	,	kIx,
kolik	kolik	k4yIc4,k4yRc4,k4yQc4
sil	síla	k1gFnPc2
Japoncům	Japonec	k1gMnPc3
zůstalo	zůstat	k5eAaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
byl	být	k5eAaImAgInS
odhodlán	odhodlán	k2eAgMnSc1d1
chránit	chránit	k5eAaImF
Midway	Midwaa	k1gFnPc4
i	i	k8xC
své	svůj	k3xOyFgFnPc4
letadlové	letadlový	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
pomohl	pomoct	k5eAaPmAgMnS
svým	svůj	k3xOyFgMnPc3
pilotům	pilot	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
museli	muset	k5eAaImAgMnP
překonat	překonat	k5eAaPmF
extrémní	extrémní	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
<g/>
,	,	kIx,
udržoval	udržovat	k5eAaImAgMnS
během	během	k7c2
dne	den	k1gInSc2
kurz	kurz	k1gInSc1
směrem	směr	k1gInSc7
k	k	k7c3
Nagumovi	Nagum	k1gMnSc3
a	a	k8xC
vytrval	vytrvat	k5eAaPmAgInS
v	v	k7c6
něm	on	k3xPp3gNnSc6
i	i	k9
s	s	k7c7
příchodem	příchod	k1gInSc7
noci	noc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
obavám	obava	k1gFnPc3
z	z	k7c2
možného	možný	k2eAgInSc2d1
nočního	noční	k2eAgInSc2d1
střetu	střet	k1gInSc2
s	s	k7c7
japonskými	japonský	k2eAgFnPc7d1
hladinovými	hladinový	k2eAgFnPc7d1
silami	síla	k1gFnPc7
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
přesvědčený	přesvědčený	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Jamamoto	Jamamota	k1gFnSc5
stále	stále	k6eAd1
zamýšlí	zamýšlet	k5eAaImIp3nS
provést	provést	k5eAaPmF
invazi	invaze	k1gFnSc4
na	na	k7c4
Midway	Midwaa	k1gFnPc4
(	(	kIx(
<g/>
částečně	částečně	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
nepřesného	přesný	k2eNgNnSc2d1
hlášení	hlášení	k1gNnSc2
ponorky	ponorka	k1gFnSc2
Tambor	tambor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
nakonec	nakonec	k6eAd1
Spruance	Spruance	k1gFnSc2
změnil	změnit	k5eAaPmAgInS
kurz	kurz	k1gInSc1
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
o	o	k7c6
půlnoci	půlnoc	k1gFnSc6
obrátil	obrátit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
na	na	k7c4
západ	západ	k1gInSc4
k	k	k7c3
nepříteli	nepřítel	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
Jamamoto	Jamamota	k1gFnSc5
byl	být	k5eAaImAgMnS
zpočátku	zpočátku	k6eAd1
rozhodnut	rozhodnout	k5eAaPmNgMnS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
operaci	operace	k1gFnSc6
a	a	k8xC
vyslal	vyslat	k5eAaPmAgMnS
své	svůj	k3xOyFgFnPc4
zbývající	zbývající	k2eAgFnPc4d1
hladinové	hladinový	k2eAgFnPc4d1
síly	síla	k1gFnPc4
na	na	k7c4
východ	východ	k1gInSc4
hledat	hledat	k5eAaImF
americké	americký	k2eAgFnPc4d1
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
vyčlenil	vyčlenit	k5eAaPmAgInS
skupinu	skupina	k1gFnSc4
křižníků	křižník	k1gInPc2
s	s	k7c7
úkolem	úkol	k1gInSc7
ostřelovat	ostřelovat	k5eAaImF
atol	atol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
nedokázaly	dokázat	k5eNaPmAgFnP
s	s	k7c7
Američany	Američan	k1gMnPc7
navázat	navázat	k5eAaPmF
kontakt	kontakt	k1gInSc4
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
Spruanceovu	Spruanceův	k2eAgNnSc3d1
rozhodnutí	rozhodnutí	k1gNnSc3
nakrátko	nakrátko	k6eAd1
se	se	k3xPyFc4
stáhnout	stáhnout	k5eAaPmF
k	k	k7c3
východu	východ	k1gInSc3
a	a	k8xC
Jamamoto	Jamamota	k1gFnSc5
nařídil	nařídit	k5eAaPmAgMnS
celkový	celkový	k2eAgInSc4d1
ústup	ústup	k1gInSc4
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
Američany	Američan	k1gMnPc4
bylo	být	k5eAaImAgNnS
štěstím	štěstí	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Spruance	Spruanec	k1gMnSc4
nepokračoval	pokračovat	k5eNaImAgMnS
v	v	k7c6
pronásledování	pronásledování	k1gNnSc6
japonských	japonský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
by	by	kYmCp3nS
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
kontaktu	kontakt	k1gInSc2
s	s	k7c7
Jamamotovými	Jamamotův	k2eAgFnPc7d1
bitevními	bitevní	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
<g/>
,	,	kIx,
zahrnujícími	zahrnující	k2eAgInPc7d1
i	i	k9
Jamato	Jamat	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nastalé	nastalý	k2eAgFnSc6d1
noční	noční	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
<g/>
,	,	kIx,
bez	bez	k7c2
pomoci	pomoc	k1gFnSc2
letadel	letadlo	k1gNnPc2
a	a	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
tehdejší	tehdejší	k2eAgFnSc3d1
nadřazenosti	nadřazenost	k1gFnSc3
japonského	japonský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
v	v	k7c6
taktice	taktika	k1gFnSc6
nočních	noční	k2eAgInPc2d1
útoků	útok	k1gInPc2
<g/>
,	,	kIx,
by	by	kYmCp3nP
s	s	k7c7
velmi	velmi	k6eAd1
vysokou	vysoký	k2eAgFnSc7d1
pravděpodobností	pravděpodobnost	k1gFnSc7
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zničení	zničení	k1gNnSc3
amerických	americký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
křižníků	křižník	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
Spruanceovi	Spruanceus	k1gMnSc3
nepodařilo	podařit	k5eNaPmAgNnS
obnovit	obnovit	k5eAaPmF
kontakt	kontakt	k1gInSc4
s	s	k7c7
Jamamotovými	Jamamotův	k2eAgFnPc7d1
silami	síla	k1gFnPc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
navzdory	navzdory	k7c3
rozsáhlému	rozsáhlý	k2eAgNnSc3d1
pátrání	pátrání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdním	pozdní	k2eAgNnSc6d1
odpoledni	odpoledne	k1gNnSc6
vyslal	vyslat	k5eAaPmAgMnS
průzkumně-bojovou	průzkumně-bojový	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
vlnu	vlna	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
najít	najít	k5eAaPmF
a	a	k8xC
zničit	zničit	k5eAaPmF
zbytky	zbytek	k1gInPc4
Nagumova	Nagumův	k2eAgInSc2d1
úderného	úderný	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc3
formaci	formace	k1gFnSc3
těsně	těsně	k6eAd1
uniklo	uniknout	k5eAaPmAgNnS
odhalení	odhalení	k1gNnSc4
Jamamotových	Jamamotův	k2eAgFnPc2d1
hlavních	hlavní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
a	a	k8xC
nedokázala	dokázat	k5eNaPmAgFnS
zasáhnout	zasáhnout	k5eAaPmF
osamělý	osamělý	k2eAgInSc4d1
japonský	japonský	k2eAgInSc4d1
torpédoborec	torpédoborec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úderná	úderný	k2eAgFnSc1d1
letadla	letadlo	k1gNnSc2
se	se	k3xPyFc4
vracela	vracet	k5eAaImAgFnS
na	na	k7c4
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
až	až	k9
po	po	k7c6
setmění	setmění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spruance	Spruanec	k1gInSc2
nechal	nechat	k5eAaPmAgMnS
na	na	k7c6
Enterprise	Enterpris	k1gInSc6
a	a	k8xC
Hornetu	Hornet	k1gInSc3
rozsvítit	rozsvítit	k5eAaPmF
světla	světlo	k1gNnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pomohl	pomoct	k5eAaPmAgMnS
vracejícím	vracející	k2eAgInPc3d1
se	se	k3xPyFc4
pilotům	pilot	k1gInPc3
s	s	k7c7
přistáním	přistání	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
noci	noc	k1gFnSc6
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
ve	v	k7c4
0	#num#	k4
<g/>
2.15	2.15	k4
učinila	učinit	k5eAaImAgFnS,k5eAaPmAgFnS
ponorka	ponorka	k1gFnSc1
Tambor	tambor	k1gInSc1
pod	pod	k7c7
velením	velení	k1gNnSc7
Johna	John	k1gMnSc2
Murphyho	Murphy	k1gMnSc2
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	s	k7c7
90	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
170	#num#	k4
km	km	kA
<g/>
)	)	kIx)
západně	západně	k6eAd1
od	od	k7c2
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
ze	z	k7c2
dvou	dva	k4xCgInPc2
hlavních	hlavní	k2eAgInPc2d1
příspěvků	příspěvek	k1gInPc2
ponorkových	ponorkový	k2eAgFnPc2d1
sil	síla	k1gFnPc2
k	k	k7c3
výsledku	výsledek	k1gInSc3
bitvy	bitva	k1gFnSc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
dopad	dopad	k1gInSc4
silně	silně	k6eAd1
otupil	otupit	k5eAaPmAgMnS
samotný	samotný	k2eAgMnSc1d1
Murphy	Murpha	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
Ponorka	ponorka	k1gFnSc1
zpozorovala	zpozorovat	k5eAaPmAgFnS
několik	několik	k4yIc4
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
Murphy	Murpha	k1gFnPc4
ani	ani	k8xC
jeho	jeho	k3xOp3gMnSc1
výkonný	výkonný	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
Edward	Edward	k1gMnSc1
Spruance	Spruance	k1gFnSc1
(	(	kIx(
<g/>
syn	syn	k1gMnSc1
admirála	admirál	k1gMnSc2
Spruance	Spruanec	k1gInSc2
<g/>
)	)	kIx)
je	on	k3xPp3gFnPc4
nedokázali	dokázat	k5eNaPmAgMnP
identifikovat	identifikovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murphy	Murpha	k1gFnPc4
si	se	k3xPyFc3
nebyl	být	k5eNaImAgInS
jistý	jistý	k2eAgMnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
vlastní	vlastní	k2eAgInPc1d1
nebo	nebo	k8xC
nepřátelské	přátelský	k2eNgInPc1d1
<g/>
,	,	kIx,
a	a	k8xC
neodvažoval	odvažovat	k5eNaImAgMnS
se	se	k3xPyFc4
přiblížit	přiblížit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ověřil	ověřit	k5eAaPmAgMnS
jejich	jejich	k3xOp3gInSc4
kurz	kurz	k1gInSc4
či	či	k8xC
typ	typ	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
pouze	pouze	k6eAd1
zaslal	zaslat	k5eAaPmAgMnS
neurčité	určitý	k2eNgNnSc4d1
hlášení	hlášení	k1gNnSc4
o	o	k7c6
„	„	k?
<g/>
čtyřech	čtyři	k4xCgFnPc6
velkých	velký	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
<g/>
“	“	k?
admirálovi	admirál	k1gMnSc3
Robertovi	Robert	k1gMnSc3
Englishovi	English	k1gMnSc3
<g/>
,	,	kIx,
veliteli	velitel	k1gMnPc7
ponorkových	ponorkový	k2eAgFnPc2d1
sil	síla	k1gFnPc2
tichomořské	tichomořský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
(	(	kIx(
<g/>
COMSUBPAC	COMSUBPAC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
English	English	k1gInSc1
tuto	tento	k3xDgFnSc4
zprávu	zpráva	k1gFnSc4
předal	předat	k5eAaPmAgMnS
Nimitzovi	Nimitz	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ji	on	k3xPp3gFnSc4
poté	poté	k6eAd1
postoupil	postoupit	k5eAaPmAgMnS
Spruancovi	Spruanec	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalého	bývalý	k2eAgMnSc2d1
ponorkového	ponorkový	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
Spruance	Spruance	k1gFnSc2
vágnost	vágnost	k1gFnSc1
Murphyho	Murphy	k1gMnSc2
zprávy	zpráva	k1gFnSc2
„	„	k?
<g/>
pochopitelně	pochopitelně	k6eAd1
rozzuřila	rozzuřit	k5eAaPmAgFnS
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
ho	on	k3xPp3gInSc4
ponechala	ponechat	k5eAaPmAgFnS
tápajícího	tápající	k2eAgMnSc4d1
a	a	k8xC
bez	bez	k7c2
konkrétní	konkrétní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
založit	založit	k5eAaPmF
přípravy	příprava	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
Bez	bez	k7c2
znalosti	znalost	k1gFnSc2
přesné	přesný	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
Jamamotova	Jamamotův	k2eAgInSc2d1
hlavního	hlavní	k2eAgInSc2d1
svazu	svaz	k1gInSc2
(	(	kIx(
<g/>
problém	problém	k1gInSc1
trvající	trvající	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
od	od	k7c2
okamžiku	okamžik	k1gInSc6
prvního	první	k4xOgNnSc2
objevení	objevení	k1gNnSc2
Japonců	Japonec	k1gMnPc2
průzkumnou	průzkumný	k2eAgFnSc7d1
Catalinou	Catalina	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
Spruance	Spruanec	k1gInPc4
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
čtyři	čtyři	k4xCgFnPc1
velké	velký	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
“	“	k?
hlášené	hlášený	k2eAgFnPc1d1
Tamborem	tambor	k1gInSc7
představují	představovat	k5eAaImIp3nP
hlavní	hlavní	k2eAgFnPc4d1
invazní	invazní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
a	a	k8xC
tak	tak	k6eAd1
změnil	změnit	k5eAaPmAgInS
kurz	kurz	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
Japonce	Japonec	k1gMnPc4
zablokoval	zablokovat	k5eAaPmAgMnS
a	a	k8xC
přitom	přitom	k6eAd1
zůstával	zůstávat	k5eAaImAgInS
100	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
190	#num#	k4
km	km	kA
<g/>
)	)	kIx)
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Křižník	křižník	k1gInSc1
Mikuma	Mikumum	k1gNnSc2
krátce	krátce	k6eAd1
před	před	k7c7
potopením	potopení	k1gNnSc7
</s>
<s>
Lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
Tambor	tambor	k1gInSc1
spatřil	spatřit	k5eAaPmAgInS
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
odřad	odřad	k1gInSc1
čtyř	čtyři	k4xCgInPc2
křižníků	křižník	k1gInPc2
a	a	k8xC
dvou	dva	k4xCgInPc2
torpédoborců	torpédoborec	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
Jamamoto	Jamamota	k1gFnSc5
vyslal	vyslat	k5eAaPmAgMnS
ostřelovat	ostřelovat	k5eAaImF
Midway	Midwaa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
0	#num#	k4
<g/>
2.55	2.55	k4
tyto	tento	k3xDgFnPc1
lodě	loď	k1gFnPc1
obdržely	obdržet	k5eAaPmAgFnP
Jamamotův	Jamamotův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
k	k	k7c3
ústupu	ústup	k1gInSc3
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
obrátily	obrátit	k5eAaPmAgFnP
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
Přibližně	přibližně	k6eAd1
ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
změně	změna	k1gFnSc3
kurzu	kurz	k1gInSc2
<g/>
,	,	kIx,
spatřily	spatřit	k5eAaPmAgFnP
Tambor	tambor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lodě	loď	k1gFnSc2
zahájily	zahájit	k5eAaPmAgInP
únikové	únikový	k2eAgInPc1d1
manévry	manévr	k1gInPc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
vyhnuly	vyhnout	k5eAaPmAgInP
předpokládanému	předpokládaný	k2eAgInSc3d1
torpédovému	torpédový	k2eAgInSc3d1
útoku	útok	k1gInSc3
ponorky	ponorka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžké	těžký	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
Mogami	Moga	k1gFnPc7
a	a	k8xC
Mikuma	Mikuma	k1gFnSc1
se	se	k3xPyFc4
přitom	přitom	k6eAd1
srazily	srazit	k5eAaPmAgFnP
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Mogami	Moga	k1gFnPc7
utrpěl	utrpět	k5eAaPmAgMnS
těžké	těžký	k2eAgNnSc1d1
poškození	poškození	k1gNnSc1
přídě	příď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Méně	málo	k6eAd2
poškozený	poškozený	k2eAgInSc4d1
Mikuma	Mikumum	k1gNnPc4
zpomalil	zpomalit	k5eAaPmAgInS
na	na	k7c4
12	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
22	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
držel	držet	k5eAaImAgInS
krok	krok	k1gInSc4
s	s	k7c7
poničeným	poničený	k2eAgInSc7d1
Mogami	Moga	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
Teprve	teprve	k6eAd1
v	v	k7c4
0	#num#	k4
<g/>
4.12	4.12	k4
se	se	k3xPyFc4
rozjasnilo	rozjasnit	k5eAaPmAgNnS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
měl	mít	k5eAaImAgMnS
Murphy	Murpha	k1gFnPc4
jistotu	jistota	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
lodě	loď	k1gFnPc1
jsou	být	k5eAaImIp3nP
japonské	japonský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
byl	být	k5eAaImAgInS
pobyt	pobyt	k1gInSc1
na	na	k7c6
hladině	hladina	k1gFnSc6
nebezpečný	bezpečný	k2eNgInSc1d1
a	a	k8xC
Tambor	tambor	k1gInSc1
se	se	k3xPyFc4
ponořil	ponořit	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
dostatečně	dostatečně	k6eAd1
přiblížit	přiblížit	k5eAaPmF
do	do	k7c2
palebné	palebný	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
skončil	skončit	k5eAaPmAgInS
neúspěchem	neúspěch	k1gInSc7
a	a	k8xC
kolem	kolem	k7c2
6.00	6.00	k4
Murphy	Murpha	k1gFnSc2
konečně	konečně	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
hlášení	hlášení	k1gNnSc4
o	o	k7c6
dvou	dva	k4xCgInPc6
křižnících	křižník	k1gInPc6
třídy	třída	k1gFnSc2
Mogami	Moga	k1gFnPc7
směřujících	směřující	k2eAgInPc2d1
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tambor	tambor	k1gMnSc1
se	se	k3xPyFc4
znovu	znovu	k6eAd1
ponořil	ponořit	k5eAaPmAgInS
a	a	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
už	už	k6eAd1
nesehrál	sehrát	k5eNaPmAgMnS
žádnou	žádný	k3yNgFnSc4
další	další	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
Mogami	Moga	k1gFnPc7
a	a	k8xC
Mikuma	Mikuma	k1gFnSc1
<g/>
,	,	kIx,
belhající	belhající	k2eAgFnSc7d1
se	se	k3xPyFc4
rychlostí	rychlost	k1gFnSc7
12	#num#	k4
uzlů	uzel	k1gInPc2
–	–	k?
zhruba	zhruba	k6eAd1
jedné	jeden	k4xCgFnSc2
třetině	třetina	k1gFnSc3
jejich	jejich	k3xOp3gFnSc2
nejvyšší	vysoký	k2eAgFnSc2d3
rychlosti	rychlost	k1gFnSc2
–	–	k?
a	a	k8xC
na	na	k7c6
přímém	přímý	k2eAgInSc6d1
kurzu	kurz	k1gInSc6
<g/>
,	,	kIx,
tvořily	tvořit	k5eAaImAgFnP
téměř	téměř	k6eAd1
dokonalý	dokonalý	k2eAgInSc4d1
terč	terč	k1gInSc4
pro	pro	k7c4
ponorkový	ponorkový	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
Tamboru	tambor	k1gInSc2
do	do	k7c2
přístavu	přístav	k1gInSc2
Spruance	Spruanec	k1gMnSc2
zbavil	zbavit	k5eAaPmAgMnS
Murphyho	Murphy	k1gMnSc4
velení	velení	k1gNnSc2
a	a	k8xC
převelel	převelet	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
na	na	k7c4
pevninu	pevnina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odůvodnil	odůvodnit	k5eAaPmAgMnS
to	ten	k3xDgNnSc1
jeho	jeho	k3xOp3gNnSc7
matoucím	matoucí	k2eAgNnSc7d1
hlášením	hlášení	k1gNnSc7
<g/>
,	,	kIx,
špatnou	špatný	k2eAgFnSc7d1
střelbou	střelba	k1gFnSc7
torpéd	torpédo	k1gNnPc2
během	během	k7c2
útoku	útok	k1gInSc2
a	a	k8xC
celkovým	celkový	k2eAgInSc7d1
nedostatkem	nedostatek	k1gInSc7
bojové	bojový	k2eAgFnSc2d1
agresivity	agresivita	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
Nautilem	nautilus	k1gInSc7
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc1d3
z	z	k7c2
12	#num#	k4
ponorek	ponorka	k1gFnPc2
na	na	k7c4
Midway	Midwaa	k1gFnPc4
a	a	k8xC
jedinou	jediný	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
úspěšně	úspěšně	k6eAd1
torpédovala	torpédovat	k5eAaPmAgFnS
nepřátelskou	přátelský	k2eNgFnSc4d1
loď	loď	k1gFnSc4
(	(	kIx(
<g/>
přestože	přestože	k8xS
torpédo	torpédo	k1gNnSc1
nevybuchlo	vybuchnout	k5eNaPmAgNnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
následujících	následující	k2eAgInPc2d1
dvou	dva	k4xCgInPc2
dnů	den	k1gInPc2
Američané	Američan	k1gMnPc1
vyslali	vyslat	k5eAaPmAgMnP
několik	několik	k4yIc4
leteckých	letecký	k2eAgInPc2d1
úderů	úder	k1gInPc2
proti	proti	k7c3
opozdilcům	opozdilec	k1gMnPc3
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
z	z	k7c2
Midwaye	Midway	k1gInSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
ze	z	k7c2
Spruanceových	Spruanceův	k2eAgInPc2d1
nosičů	nosič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikumu	Mikum	k1gInSc2
nakonec	nakonec	k6eAd1
Dauntlessy	Dauntlessa	k1gFnPc1
potopily	potopit	k5eAaPmAgFnP
<g/>
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
vážně	vážně	k6eAd1
poničený	poničený	k2eAgInSc1d1
Mogami	Moga	k1gFnPc7
přežil	přežít	k5eAaPmAgMnS
a	a	k8xC
doplul	doplout	k5eAaPmAgMnS
domů	domů	k6eAd1
k	k	k7c3
opravě	oprava	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
posledního	poslední	k2eAgMnSc2d1
z	z	k7c2
těchto	tento	k3xDgInPc2
útoků	útok	k1gInPc2
byly	být	k5eAaImAgFnP
bombardovány	bombardovat	k5eAaImNgFnP
a	a	k8xC
postřelovány	postřelován	k2eAgFnPc1d1
rovněž	rovněž	k9
torpédoborce	torpédoborec	k1gMnSc4
Arašio	Arašio	k6eAd1
a	a	k8xC
Asašio	Asašio	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
167	#num#	k4
<g/>
]	]	kIx)
Kapitán	kapitán	k1gMnSc1
Richard	Richard	k1gMnSc1
E.	E.	kA
Fleming	Fleming	k1gInSc1
<g/>
,	,	kIx,
letec	letec	k1gMnSc1
americké	americký	k2eAgFnSc2d1
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
,	,	kIx,
zahynul	zahynout	k5eAaPmAgMnS
během	během	k7c2
provádění	provádění	k1gNnSc2
klouzavého	klouzavý	k2eAgInSc2d1
bombardovacího	bombardovací	k2eAgInSc2d1
útoku	útok	k1gInSc2
na	na	k7c6
Mikumu	Mikum	k1gInSc6
a	a	k8xC
posmrtně	posmrtně	k6eAd1
obdržel	obdržet	k5eAaPmAgMnS
Medaili	medaile	k1gFnSc4
cti	čest	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Yorktown	Yorktown	k1gMnSc1
potápějící	potápějící	k2eAgMnSc1d1
se	se	k3xPyFc4
kýlem	kýl	k1gInSc7
vzhůru	vzhůru	k6eAd1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
levé	levý	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
snímku	snímek	k1gInSc2
viditelný	viditelný	k2eAgInSc1d1
velký	velký	k2eAgInSc1d1
otvor	otvor	k1gInSc1
po	po	k7c6
zásahu	zásah	k1gInSc6
torpédem	torpédo	k1gNnSc7
I-168	I-168	k1gFnSc2
</s>
<s>
Mezitím	mezitím	k6eAd1
záchranné	záchranný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
na	na	k7c6
Yorktownu	Yorktown	k1gInSc6
přinášely	přinášet	k5eAaImAgInP
povzbudivé	povzbudivý	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
a	a	k8xC
zatím	zatím	k6eAd1
nepohyblivou	pohyblivý	k2eNgFnSc4d1
loď	loď	k1gFnSc4
vzal	vzít	k5eAaPmAgMnS
do	do	k7c2
vleku	vlek	k1gInSc2
oceánský	oceánský	k2eAgInSc1d1
remorkér	remorkér	k1gInSc1
USS	USS	kA
Vireo	Vireo	k6eAd1
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdě	pozdě	k6eAd1
odpoledne	odpoledne	k6eAd1
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
však	však	k9
japonská	japonský	k2eAgFnSc1d1
ponorka	ponorka	k1gFnSc1
I-168	I-168	k1gFnSc1
šósa	šósa	k1gFnSc1
(	(	kIx(
<g/>
少	少	k?
~	~	kIx~
komandér	komandér	k1gMnSc1
<g/>
–	–	k?
<g/>
poručík	poručík	k1gMnSc1
<g/>
/	/	kIx~
<g/>
korvetní	korvetní	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
Jahači	Jahač	k1gMnSc5
Tanabeho	Tanabeha	k1gMnSc5
proklouzla	proklouznout	k5eAaPmAgFnS
kordonem	kordon	k1gInSc7
torpédoborců	torpédoborec	k1gInPc2
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
díky	díky	k7c3
spoustě	spousta	k1gFnSc3
trosek	troska	k1gFnPc2
v	v	k7c6
okolních	okolní	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
<g/>
)	)	kIx)
a	a	k8xC
vystřelila	vystřelit	k5eAaPmAgFnS
salvu	salva	k1gFnSc4
torpéd	torpédo	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
dvě	dva	k4xCgFnPc1
zasáhla	zasáhnout	k5eAaPmAgFnS
Yorktown	Yorktown	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
palubě	paluba	k1gFnSc6
letadlové	letadlový	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
způsobila	způsobit	k5eAaPmAgFnS
jen	jen	k9
málo	málo	k4c4
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
většina	většina	k1gFnSc1
posádky	posádka	k1gFnSc2
již	již	k6eAd1
byla	být	k5eAaImAgFnS
evakuována	evakuován	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
třetí	třetí	k4xOgNnSc4
torpédo	torpédo	k1gNnSc4
z	z	k7c2
této	tento	k3xDgFnSc2
salvy	salva	k1gFnSc2
zasáhlo	zasáhnout	k5eAaPmAgNnS
torpédoborec	torpédoborec	k1gInSc4
USS	USS	kA
Hammann	Hammann	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zajišťoval	zajišťovat	k5eAaImAgInS
Yorktownu	Yorktowen	k2eAgFnSc4d1
dodávku	dodávka	k1gFnSc4
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hammann	Hammann	k1gMnSc1
se	se	k3xPyFc4
rozlomil	rozlomit	k5eAaPmAgMnS
v	v	k7c6
půli	půle	k1gFnSc6
a	a	k8xC
klesl	klesnout	k5eAaPmAgInS
ke	k	k7c3
dnu	dno	k1gNnSc3
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
80	#num#	k4
životů	život	k1gInPc2
<g/>
,	,	kIx,
především	především	k9
následkem	následek	k1gInSc7
exploze	exploze	k1gFnSc2
jeho	jeho	k3xOp3gFnPc2
vlastních	vlastní	k2eAgFnPc2d1
hlubinných	hlubinný	k2eAgFnPc2d1
pum	puma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnSc1
záchranné	záchranný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
byly	být	k5eAaImAgInP
považovány	považován	k2eAgInPc1d1
za	za	k7c2
beznadějné	beznadějný	k2eAgFnSc2d1
a	a	k8xC
zbývající	zbývající	k2eAgFnSc2d1
záchranné	záchranný	k2eAgFnSc2d1
čety	četa	k1gFnSc2
Yorktown	Yorktowna	k1gFnPc2
opustily	opustit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
celou	celý	k2eAgFnSc4d1
noc	noc	k1gFnSc4
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
a	a	k8xC
ráno	ráno	k6eAd1
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
zůstávala	zůstávat	k5eAaImAgFnS
loď	loď	k1gFnSc1
na	na	k7c6
hladině	hladina	k1gFnSc6
<g/>
;	;	kIx,
ale	ale	k9
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
v	v	k7c4
5.30	5.30	k4
svědkové	svědek	k1gMnPc1
zaznamenali	zaznamenat	k5eAaPmAgMnP
rychle	rychle	k6eAd1
rostoucí	rostoucí	k2eAgInSc4d1
náklon	náklon	k1gInSc4
na	na	k7c4
levobok	levobok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
nato	nato	k6eAd1
se	se	k3xPyFc4
převrátila	převrátit	k5eAaPmAgFnS
na	na	k7c4
levý	levý	k2eAgInSc4d1
bok	bok	k1gInSc4
a	a	k8xC
odhalila	odhalit	k5eAaPmAgFnS
torpédem	torpédo	k1gNnSc7
způsobený	způsobený	k2eAgInSc1d1
otvor	otvor	k1gInSc1
v	v	k7c6
pravoboku	pravobok	k1gInSc6
–	–	k?
výsledek	výsledek	k1gInSc1
ponorkového	ponorkový	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
kapitána	kapitán	k1gMnSc2
Buckmastera	Buckmaster	k1gMnSc2
stále	stále	k6eAd1
vlála	vlát	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgFnPc1
lodě	loď	k1gFnPc1
na	na	k7c4
pozdrav	pozdrav	k1gInSc4
spustily	spustit	k5eAaPmAgFnP
vlajky	vlajka	k1gFnPc1
na	na	k7c4
půl	půl	k1xP
žerdi	žerď	k1gFnSc2
<g/>
;	;	kIx,
celé	celá	k1gFnPc1
jejich	jejich	k3xOp3gFnSc2
posádky	posádka	k1gFnSc2
stály	stát	k5eAaImAgFnP
na	na	k7c6
palubách	paluba	k1gFnPc6
v	v	k7c6
pozoru	pozor	k1gInSc6
<g/>
,	,	kIx,
se	s	k7c7
sejmutými	sejmutý	k2eAgFnPc7d1
čepicemi	čepice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
7.01	7.01	k4
se	se	k3xPyFc4
loď	loď	k1gFnSc1
převrátila	převrátit	k5eAaPmAgFnS
kýlem	kýl	k1gInSc7
vzhůru	vzhůru	k6eAd1
a	a	k8xC
pomalu	pomalu	k6eAd1
se	se	k3xPyFc4
potopila	potopit	k5eAaPmAgFnS
zádí	záď	k1gFnSc7
napřed	napřed	k6eAd1
<g/>
,	,	kIx,
s	s	k7c7
bojovou	bojový	k2eAgFnSc7d1
zástavou	zástava	k1gFnSc7
stále	stále	k6eAd1
vlající	vlající	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
170	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonské	japonský	k2eAgFnPc1d1
a	a	k8xC
americké	americký	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
</s>
<s>
Během	během	k7c2
celé	celý	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
život	život	k1gInSc4
3057	#num#	k4
Japonců	Japonec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztráty	ztráta	k1gFnSc2
na	na	k7c6
palubách	paluba	k1gFnPc6
čtyř	čtyři	k4xCgInPc2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
byly	být	k5eAaImAgFnP
následující	následující	k2eAgFnPc1d1
<g/>
:	:	kIx,
Akagi	Akag	k1gFnPc1
<g/>
:	:	kIx,
267	#num#	k4
<g/>
;	;	kIx,
Kaga	Kag	k1gInSc2
<g/>
:	:	kIx,
811	#num#	k4
<g/>
;	;	kIx,
Hirjú	Hirjú	k1gFnSc2
<g/>
:	:	kIx,
392	#num#	k4
(	(	kIx(
<g/>
včetně	včetně	k7c2
kontradmirála	kontradmirál	k1gMnSc2
Jamagučiho	Jamaguči	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
šel	jít	k5eAaImAgMnS
dobrovolně	dobrovolně	k6eAd1
ke	k	k7c3
dnu	dno	k1gNnSc3
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
lodí	loď	k1gFnSc7
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Sórjú	Sórjú	k1gMnSc1
<g/>
:	:	kIx,
711	#num#	k4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
včetně	včetně	k7c2
kapitána	kapitán	k1gMnSc2
Janagimota	Janagimot	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
zůstat	zůstat	k5eAaPmF
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
)	)	kIx)
<g/>
;	;	kIx,
celkem	celkem	k6eAd1
2181	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
172	#num#	k4
<g/>
]	]	kIx)
Dalších	další	k2eAgInPc2d1
792	#num#	k4
mrtvých	mrtvý	k1gMnPc2
připadlo	připadnout	k5eAaPmAgNnS
na	na	k7c4
těžké	těžký	k2eAgInPc4d1
křižníky	křižník	k1gInPc4
Mikuma	Mikum	k1gMnSc2
(	(	kIx(
<g/>
potopen	potopit	k5eAaPmNgInS
<g/>
;	;	kIx,
700	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Mogami	Moga	k1gFnPc7
(	(	kIx(
<g/>
těžce	těžce	k6eAd1
poškozen	poškodit	k5eAaPmNgInS
<g/>
;	;	kIx,
92	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
173	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
toho	ten	k3xDgMnSc2
byly	být	k5eAaImAgInP
během	během	k7c2
leteckých	letecký	k2eAgInPc2d1
útoků	útok	k1gInPc2
poškozeny	poškozen	k2eAgInPc4d1
torpédoborce	torpédoborec	k1gInPc4
Arašio	Arašio	k6eAd1
(	(	kIx(
<g/>
bombardován	bombardován	k2eAgMnSc1d1
<g/>
;	;	kIx,
35	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
Asašio	Asašio	k6eAd1
(	(	kIx(
<g/>
postřelován	postřelovat	k5eAaImNgInS
palubními	palubní	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
<g/>
;	;	kIx,
21	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
průzkumné	průzkumný	k2eAgInPc4d1
hydroplány	hydroplán	k1gInPc4
přišly	přijít	k5eAaPmAgFnP
křižníky	křižník	k1gInPc4
Čikuma	Čikum	k1gMnSc2
(	(	kIx(
<g/>
3	#num#	k4
muži	muž	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
Tone	tonout	k5eAaImIp3nS
(	(	kIx(
<g/>
2	#num#	k4
muži	muž	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývajících	zbývající	k2eAgInPc2d1
23	#num#	k4
obětí	oběť	k1gFnPc2
tvořili	tvořit	k5eAaImAgMnP
padlí	padlý	k2eAgMnPc1d1
na	na	k7c6
palubě	paluba	k1gFnSc6
torpédoborců	torpédoborec	k1gInPc2
Tanikaze	Tanikaha	k1gFnSc6
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Araši	Araše	k1gFnSc4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kazagumo	Kazaguma	k1gFnSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
cisternové	cisternový	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
Akebono	Akebona	k1gFnSc5
Maru	Maru	k1gFnSc7
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
během	během	k7c2
bitvy	bitva	k1gFnSc2
ztratily	ztratit	k5eAaPmAgFnP
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Yorktown	Yorktown	k1gMnSc1
a	a	k8xC
torpédoborec	torpédoborec	k1gMnSc1
Hammann	Hammann	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
307	#num#	k4
Američanů	Američan	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
generálmajora	generálmajor	k1gMnSc2
Clarence	Clarence	k1gFnSc2
L.	L.	kA
Tinkera	Tinker	k1gMnSc2
<g/>
,	,	kIx,
velitele	velitel	k1gMnSc2
7	#num#	k4
<g/>
.	.	kIx.
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
osobně	osobně	k6eAd1
vedl	vést	k5eAaImAgInS
bombardovací	bombardovací	k2eAgInSc1d1
nálet	nálet	k1gInSc1
z	z	k7c2
Havaje	Havaj	k1gFnSc2
proti	proti	k7c3
ustupujícím	ustupující	k2eAgFnPc3d1
japonským	japonský	k2eAgFnPc3d1
silám	síla	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
havárii	havárie	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
letadla	letadlo	k1gNnSc2
poblíž	poblíž	k7c2
ostrova	ostrov	k1gInSc2
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
bitvě	bitva	k1gFnSc6
</s>
<s>
Tento	tento	k3xDgMnSc1
SBD-2	SBD-2	k1gMnSc1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
šestnácti	šestnáct	k4xCc2
střemhlavých	střemhlavý	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
VMSB-	VMSB-	k1gFnSc2
<g/>
241	#num#	k4
<g/>
,	,	kIx,
vyslaných	vyslaný	k2eAgFnPc2d1
z	z	k7c2
Midway	Midwaa	k1gFnSc2
ráno	ráno	k6eAd1
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
Hirjú	Hirjú	k1gFnSc4
na	na	k7c6
něm	on	k3xPp3gNnSc6
napočítali	napočítat	k5eAaPmAgMnP
219	#num#	k4
děr	děra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Renovovaný	renovovaný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
sbírkách	sbírka	k1gFnPc6
National	National	k1gFnSc2
Naval	navalit	k5eAaPmRp2nS
Aviation	Aviation	k1gInSc4
Museum	museum	k1gNnSc1
v	v	k7c6
Pensacole	Pensacola	k1gFnSc6
na	na	k7c6
Floridě	Florida	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
175	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Američané	Američan	k1gMnPc1
vybojovali	vybojovat	k5eAaPmAgMnP
jasné	jasný	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
další	další	k2eAgNnSc1d1
pronásledování	pronásledování	k1gNnSc1
ustupujících	ustupující	k2eAgMnPc2d1
Japonců	Japonec	k1gMnPc2
stalo	stát	k5eAaPmAgNnS
příliš	příliš	k6eAd1
riskantním	riskantní	k2eAgInSc7d1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
nebezpečí	nebezpečí	k1gNnSc3
leteckých	letecký	k2eAgInPc2d1
útoků	útok	k1gInPc2
z	z	k7c2
blízkého	blízký	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
Wake	Wak	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
176	#num#	k4
<g/>
]	]	kIx)
otočili	otočit	k5eAaPmAgMnP
k	k	k7c3
návratu	návrat	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spruance	Spruanec	k1gInSc2
se	se	k3xPyFc4
opět	opět	k6eAd1
stáhl	stáhnout	k5eAaPmAgMnS
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
doplnil	doplnit	k5eAaPmAgMnS
palivo	palivo	k1gNnSc4
na	na	k7c6
svých	svůj	k3xOyFgInPc6
torpédoborcích	torpédoborec	k1gInPc6
a	a	k8xC
setkal	setkat	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
letadlovou	letadlový	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
USS	USS	kA
Saratoga	Saratog	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
přivážela	přivážet	k5eAaImAgFnS
tolik	tolik	k6eAd1
potřebná	potřebný	k2eAgNnPc4d1
náhradní	náhradní	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpoledne	odpoledne	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
Fletcher	Fletchra	k1gFnPc2
přenesl	přenést	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
vlajku	vlajka	k1gFnSc4
na	na	k7c4
Saratogu	Saratoga	k1gFnSc4
a	a	k8xC
znovu	znovu	k6eAd1
převzal	převzít	k5eAaPmAgInS
velení	velení	k1gNnSc4
nad	nad	k7c7
operačním	operační	k2eAgInSc7d1
svazem	svaz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
zbytek	zbytek	k1gInSc4
dne	den	k1gInSc2
a	a	k8xC
celý	celý	k2eAgInSc4d1
následující	následující	k2eAgInSc4d1
den	den	k1gInSc4
9	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
,	,	kIx,
Fletcher	Fletchra	k1gFnPc2
pokračoval	pokračovat	k5eAaImAgInS
s	s	k7c7
vysíláním	vysílání	k1gNnSc7
pátracích	pátrací	k2eAgInPc2d1
letů	let	k1gInPc2
ze	z	k7c2
všech	všecek	k3xTgInPc2
tří	tři	k4xCgInPc2
nosičů	nosič	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
ujistil	ujistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
k	k	k7c3
Midway	Midwa	k2eAgMnPc4d1
nevrací	vracet	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdě	pozdě	k6eAd1
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
opustit	opustit	k5eAaPmF
bojovou	bojový	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
a	a	k8xC
americké	americký	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
vrátily	vrátit	k5eAaPmAgFnP
do	do	k7c2
Pearl	Pearlum	k1gNnPc2
Harboru	Harbor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
177	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historik	historik	k1gMnSc1
Samuel	Samuel	k1gMnSc1
E.	E.	kA
Morison	Morison	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
Spruance	Spruance	k1gFnSc1
stihla	stihnout	k5eAaPmAgFnS
značná	značný	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
nepronásledoval	pronásledovat	k5eNaImAgMnS
ustupující	ustupující	k2eAgMnPc4d1
Japonce	Japonec	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
umožnil	umožnit	k5eAaPmAgMnS
jejich	jejich	k3xOp3gInSc6
loďstvu	loďstvo	k1gNnSc6
uniknout	uniknout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
178	#num#	k4
<g/>
]	]	kIx)
Clay	Claa	k1gFnSc2
Blair	Blair	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
admirálovo	admirálův	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
bránil	bránit	k5eAaImAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
že	že	k8xS
kdyby	kdyby	kYmCp3nS
Spruance	Spruanec	k1gInPc4
vytrval	vytrvat	k5eAaPmAgMnS
v	v	k7c6
pronásledování	pronásledování	k1gNnSc6
<g/>
,	,	kIx,
s	s	k7c7
příchodem	příchod	k1gInSc7
noci	noc	k1gFnSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
připravil	připravit	k5eAaPmAgInS
o	o	k7c4
možnost	možnost	k1gFnSc4
nasazení	nasazení	k1gNnSc2
palubních	palubní	k2eAgInPc2d1
letounů	letoun	k1gInPc2
a	a	k8xC
jeho	jeho	k3xOp3gInPc7
křižníky	křižník	k1gInPc7
by	by	kYmCp3nP
podlehly	podlehnout	k5eAaPmAgFnP
Jamamotovým	Jamamotův	k2eAgFnPc3d1
mocným	mocný	k2eAgFnPc3d1
hladinovým	hladinový	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
<g/>
,	,	kIx,
zahrnujícím	zahrnující	k2eAgFnPc3d1
i	i	k8xC
největší	veliký	k2eAgFnSc4d3
bitevní	bitevní	k2eAgFnSc4d1
loď	loď	k1gFnSc4
světa	svět	k1gInSc2
Jamato	Jamat	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
176	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
americké	americký	k2eAgFnPc1d1
letecké	letecký	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
utrpěly	utrpět	k5eAaPmAgFnP
značné	značný	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
většiny	většina	k1gFnSc2
svých	svůj	k3xOyFgInPc2
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
značně	značně	k6eAd1
snižovalo	snižovat	k5eAaImAgNnS
pravděpodobnost	pravděpodobnost	k1gFnSc4
úspěchu	úspěch	k1gInSc2
při	při	k7c6
náletu	nálet	k1gInSc6
proti	proti	k7c3
japonským	japonský	k2eAgFnPc3d1
bitevním	bitevní	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
<g/>
,	,	kIx,
i	i	k8xC
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
je	on	k3xPp3gFnPc4
zastihnout	zastihnout	k5eAaPmF
za	za	k7c2
denního	denní	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
179	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
touto	tento	k3xDgFnSc7
dobou	doba	k1gFnSc7
zůstávalo	zůstávat	k5eAaImAgNnS
Spruanceovým	Spruanceův	k2eAgMnPc3d1
torpédoborcům	torpédoborec	k1gMnPc3
kriticky	kriticky	k6eAd1
málo	málo	k6eAd1
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
180	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
181	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
styčné	styčný	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
císařských	císařský	k2eAgFnPc2d1
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
podalo	podat	k5eAaPmAgNnS
nekompletní	kompletní	k2eNgNnSc1d1
vylíčení	vylíčení	k1gNnSc1
výsledků	výsledek	k1gInPc2
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podrobné	podrobný	k2eAgFnPc4d1
bitevní	bitevní	k2eAgNnPc4d1
hlášení	hlášení	k1gNnPc4
zhotovené	zhotovený	k2eAgFnSc2d1
Čúiči	Čúič	k1gInPc7
Nagumem	Nagum	k1gInSc7
bylo	být	k5eAaImAgNnS
vrchnímu	vrchní	k1gMnSc3
velení	velení	k1gNnSc2
předloženo	předložit	k5eAaPmNgNnS
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
určeno	určit	k5eAaPmNgNnS
pouze	pouze	k6eAd1
pro	pro	k7c4
nejužší	úzký	k2eAgInPc4d3
kruhy	kruh	k1gInPc4
japonského	japonský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
a	a	k8xC
vlády	vláda	k1gFnSc2
a	a	k8xC
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
války	válka	k1gFnSc2
zůstávalo	zůstávat	k5eAaImAgNnS
přísně	přísně	k6eAd1
střeženým	střežený	k2eAgNnSc7d1
tajemstvím	tajemství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgNnSc7
z	z	k7c2
pozoruhodnějších	pozoruhodný	k2eAgNnPc2d2
konstatování	konstatování	k1gNnPc2
v	v	k7c6
něm	on	k3xPp3gNnSc6
uvedených	uvedený	k2eAgFnPc2d1
je	být	k5eAaImIp3nS
komentář	komentář	k1gInSc4
k	k	k7c3
odhadům	odhad	k1gInPc3
velitele	velitel	k1gMnSc2
mobilního	mobilní	k2eAgInSc2d1
svazu	svaz	k1gInSc2
(	(	kIx(
<g/>
Nagumo	Naguma	k1gFnSc5
<g/>
)	)	kIx)
<g/>
:	:	kIx,
„	„	k?
<g/>
Nepřítel	nepřítel	k1gMnSc1
si	se	k3xPyFc3
není	být	k5eNaImIp3nS
vědom	vědom	k2eAgMnSc1d1
našich	náš	k3xOp1gMnPc2
plánů	plán	k1gInPc2
(	(	kIx(
<g/>
odhalili	odhalit	k5eAaPmAgMnP
nás	my	k3xPp1nPc4
teprve	teprve	k6eAd1
časně	časně	k6eAd1
zrána	zrána	k6eAd1
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
182	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
byla	být	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
operace	operace	k1gFnSc1
prozrazena	prozrazen	k2eAgFnSc1d1
na	na	k7c6
samém	samý	k3xTgInSc6
počátku	počátek	k1gInSc6
<g/>
,	,	kIx,
vinou	vinou	k7c2
rozbití	rozbití	k1gNnSc2
japonského	japonský	k2eAgInSc2d1
námořního	námořní	k2eAgInSc2d1
kódu	kód	k1gInSc2
nepřítelem	nepřítel	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
183	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Před	před	k7c7
japonskou	japonský	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
a	a	k8xC
velkou	velký	k2eAgFnSc7d1
částí	část	k1gFnSc7
vojenské	vojenský	k2eAgFnSc2d1
velitelské	velitelský	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
byl	být	k5eAaImAgInS
rozsah	rozsah	k1gInSc1
porážky	porážka	k1gFnSc2
důsledně	důsledně	k6eAd1
skrýván	skrývat	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgInPc4d1
sdělovací	sdělovací	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
hlásaly	hlásat	k5eAaImAgFnP
velké	velký	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
císař	císař	k1gMnSc1
Hirohito	Hirohit	k2eAgNnSc1d1
a	a	k8xC
nejvyšší	vysoký	k2eAgMnSc1d3
velitelé	velitel	k1gMnPc1
námořnictva	námořnictvo	k1gNnSc2
byli	být	k5eAaImAgMnP
pravdivě	pravdivě	k6eAd1
informováni	informovat	k5eAaBmNgMnP
o	o	k7c6
ztrátách	ztráta	k1gFnPc6
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
pilotů	pilot	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
i	i	k9
japonská	japonský	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
alespoň	alespoň	k9
po	po	k7c4
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
věřila	věřit	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
loďstvo	loďstvo	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
dobrém	dobrý	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
184	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
návratu	návrat	k1gInSc6
japonské	japonský	k2eAgFnSc2d1
floty	flota	k1gFnSc2
do	do	k7c2
přístavu	přístav	k1gInSc2
Haširadžima	Haširadžimum	k1gNnSc2
dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
byli	být	k5eAaImAgMnP
ranění	raněný	k1gMnPc1
okamžitě	okamžitě	k6eAd1
převezeni	převézt	k5eAaPmNgMnP
do	do	k7c2
nemocnic	nemocnice	k1gFnPc2
vojenského	vojenský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
zařadili	zařadit	k5eAaPmAgMnP
jako	jako	k9
„	„	k?
<g/>
tajné	tajný	k2eAgMnPc4d1
pacienty	pacient	k1gMnPc4
<g/>
“	“	k?
<g/>
,	,	kIx,
umístili	umístit	k5eAaPmAgMnP
na	na	k7c4
zvláštní	zvláštní	k2eAgNnSc4d1
oddělení	oddělení	k1gNnSc4
a	a	k8xC
izolovali	izolovat	k5eAaBmAgMnP
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
pacientů	pacient	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
vlastních	vlastní	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
předešlo	předejít	k5eAaPmAgNnS
vyzrazení	vyzrazení	k1gNnSc1
velké	velký	k2eAgFnSc2d1
porážky	porážka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
185	#num#	k4
<g/>
]	]	kIx)
Zbývající	zbývající	k2eAgMnPc4d1
důstojníky	důstojník	k1gMnPc4
a	a	k8xC
muže	muž	k1gMnPc4
rychle	rychle	k6eAd1
rozptýlili	rozptýlit	k5eAaPmAgMnP
mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
možnosti	možnost	k1gFnSc2
shledat	shledat	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
rodinami	rodina	k1gFnPc7
nebo	nebo	k8xC
přáteli	přítel	k1gMnPc7
byli	být	k5eAaImAgMnP
odesláni	odeslat	k5eAaPmNgMnP
k	k	k7c3
jednotkám	jednotka	k1gFnPc3
umístěným	umístěný	k2eAgFnPc3d1
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Tichomoří	Tichomoří	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
následně	následně	k6eAd1
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
zahynula	zahynout	k5eAaPmAgFnS
v	v	k7c6
bojích	boj	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
186	#num#	k4
<g/>
]	]	kIx)
Žádný	žádný	k3yNgMnSc1
z	z	k7c2
vlajkových	vlajkový	k2eAgMnPc2d1
důstojníků	důstojník	k1gMnPc2
nebo	nebo	k8xC
štábu	štáb	k1gInSc2
Spojeného	spojený	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
nebyl	být	k5eNaImAgMnS
potrestán	potrestat	k5eAaPmNgMnS
a	a	k8xC
Naguma	Naguma	k1gNnSc4
později	pozdě	k6eAd2
pověřili	pověřit	k5eAaPmAgMnP
velením	velení	k1gNnSc7
rekonstituovaného	rekonstituovaný	k2eAgInSc2d1
svazu	svaz	k1gInSc2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
187	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
zkušeností	zkušenost	k1gFnPc2
získaných	získaný	k2eAgFnPc2d1
během	během	k7c2
bitvy	bitva	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přijetí	přijetí	k1gNnSc3
nových	nový	k2eAgFnPc2d1
směrnic	směrnice	k1gFnPc2
<g/>
,	,	kIx,
kladoucích	kladoucí	k2eAgInPc2d1
větší	veliký	k2eAgInSc4d2
důraz	důraz	k1gInSc4
na	na	k7c6
tankování	tankování	k1gNnSc6
a	a	k8xC
zbrojení	zbrojení	k1gNnSc6
japonských	japonský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
na	na	k7c6
letové	letový	k2eAgFnSc6d1
palubě	paluba	k1gFnSc6
spíše	spíše	k9
než	než	k8xS
na	na	k7c4
hangárové	hangárový	k2eAgNnSc4d1
<g/>
,	,	kIx,
a	a	k8xC
byla	být	k5eAaImAgFnS
zavedena	zavést	k5eAaPmNgFnS
praxe	praxe	k1gFnSc1
vypouštění	vypouštění	k1gNnSc2
veškerých	veškerý	k3xTgNnPc2
nepoužívaných	používaný	k2eNgNnPc2d1
palivových	palivový	k2eAgNnPc2d1
potrubí	potrubí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plány	plán	k1gInPc4
nově	nova	k1gFnSc3
stavěných	stavěný	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
doznaly	doznat	k5eAaPmAgInP
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
týkajících	týkající	k2eAgFnPc2d1
se	se	k3xPyFc4
instalace	instalace	k1gFnSc1
pouze	pouze	k6eAd1
dvou	dva	k4xCgInPc2
výtahů	výtah	k1gInPc2
na	na	k7c6
letové	letový	k2eAgFnSc6d1
palubě	paluba	k1gFnSc6
a	a	k8xC
vylepšeného	vylepšený	k2eAgNnSc2d1
protipožárního	protipožární	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
členů	člen	k1gInPc2
posádky	posádka	k1gFnSc2
se	se	k3xPyFc4
zacvičovalo	zacvičovat	k5eAaImAgNnS
v	v	k7c6
postupech	postup	k1gInPc6
zvládání	zvládání	k1gNnSc2
škod	škoda	k1gFnPc2
a	a	k8xC
likvidace	likvidace	k1gFnSc2
požárů	požár	k1gInPc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
pozdější	pozdní	k2eAgFnPc4d2
ztráty	ztráta	k1gFnPc4
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
Šókaku	Šókak	k1gInSc2
<g/>
,	,	kIx,
Hijó	Hijó	k1gFnSc2
a	a	k8xC
zejména	zejména	k9
Taihó	Taihó	k1gMnPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
stále	stále	k6eAd1
přetrvávaly	přetrvávat	k5eAaImAgInP
problémy	problém	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
188	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
potřebě	potřeba	k1gFnSc3
co	co	k9
nejrychlejšího	rychlý	k2eAgNnSc2d3
nahrazení	nahrazení	k1gNnSc2
ztracených	ztracený	k2eAgMnPc2d1
letců	letec	k1gMnPc2
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zkrácení	zkrácení	k1gNnSc3
výcvikových	výcvikový	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
prudkému	prudký	k2eAgInSc3d1
poklesu	pokles	k1gInSc3
kvality	kvalita	k1gFnSc2
nových	nový	k2eAgMnPc2d1
pilotů	pilot	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
nezkušení	zkušený	k2eNgMnPc1d1
piloti	pilot	k1gMnPc1
odcházeli	odcházet	k5eAaImAgMnP
přímo	přímo	k6eAd1
na	na	k7c4
frontu	fronta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veteráni	veterán	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
přežili	přežít	k5eAaPmAgMnP
akce	akce	k1gFnPc4
u	u	k7c2
Midway	Midwaa	k1gFnSc2
a	a	k8xC
Šalamounových	Šalamounových	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
,	,	kIx,
museli	muset	k5eAaImAgMnP
sdílet	sdílet	k5eAaImF
stále	stále	k6eAd1
rostoucí	rostoucí	k2eAgFnSc4d1
bojovou	bojový	k2eAgFnSc4d1
zátěž	zátěž	k1gFnSc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
situace	situace	k1gFnSc1
na	na	k7c6
bojištích	bojiště	k1gNnPc6
vyvíjela	vyvíjet	k5eAaImAgFnS
pro	pro	k7c4
Japonské	japonský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
stále	stále	k6eAd1
nepříznivěji	příznivě	k6eNd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
málo	málo	k4c1
z	z	k7c2
nich	on	k3xPp3gMnPc2
dostalo	dostat	k5eAaPmAgNnS
možnost	možnost	k1gFnSc4
odpočinout	odpočinout	k5eAaPmF
si	se	k3xPyFc3
v	v	k7c6
zázemí	zázemí	k1gNnSc6
nebo	nebo	k8xC
na	na	k7c6
domácích	domácí	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
úroveň	úroveň	k1gFnSc1
japonského	japonský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
jako	jako	k8xS,k8xC
celku	celek	k1gInSc2
během	během	k7c2
války	válka	k1gFnSc2
postupně	postupně	k6eAd1
zhoršovala	zhoršovat	k5eAaImAgFnS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jejich	jejich	k3xOp3gMnPc1
američtí	americký	k2eAgMnPc1d1
protivníci	protivník	k1gMnPc1
se	se	k3xPyFc4
neustále	neustále	k6eAd1
zlepšovali	zlepšovat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
189	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Američtí	americký	k2eAgMnPc1d1
zajatci	zajatec	k1gMnPc1
</s>
<s>
Během	během	k7c2
bitvy	bitva	k1gFnSc2
byli	být	k5eAaImAgMnP
zajati	zajmout	k5eAaPmNgMnP
tři	tři	k4xCgMnPc1
američtí	americký	k2eAgMnPc1d1
letci	letec	k1gMnPc1
<g/>
:	:	kIx,
praporčík	praporčík	k1gMnSc1
Wesley	Weslea	k1gFnSc2
Osmus	Osmus	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
190	#num#	k4
<g/>
]	]	kIx)
pilot	pilot	k1gMnSc1
z	z	k7c2
Yorktownu	Yorktown	k1gInSc2
<g/>
;	;	kIx,
praporčík	praporčík	k1gMnSc1
Frank	Frank	k1gMnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Flaherty	Flahert	k1gMnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
191	#num#	k4
<g/>
]	]	kIx)
pilot	pilot	k1gMnSc1
z	z	k7c2
Enterprise	Enterprise	k1gFnSc2
<g/>
;	;	kIx,
a	a	k8xC
vrchní	vrchní	k2eAgMnSc1d1
letecký	letecký	k2eAgMnSc1d1
strojník	strojník	k1gMnSc1
Bruno	Bruno	k1gMnSc1
Peter	Peter	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Gaido	Gaido	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
192	#num#	k4
<g/>
]	]	kIx)
O	O	kA
<g/>
'	'	kIx"
<g/>
Flahertyho	Flaherty	k1gMnSc2
radista-střelec	radista-střelec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
193	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
194	#num#	k4
<g/>
]	]	kIx)
Osmuse	Osmuse	k1gFnSc2
zajal	zajmout	k5eAaPmAgInS
torpédoborec	torpédoborec	k1gInSc1
Araši	Araše	k1gFnSc4
<g/>
;	;	kIx,
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Flahertyho	Flaherty	k1gMnSc4
a	a	k8xC
Gaida	Gaid	k1gMnSc4
křižník	křižník	k1gInSc4
Nagara	Nagar	k1gMnSc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
torpédoborec	torpédoborec	k1gMnSc1
Makigumo	Makiguma	k1gFnSc5
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
zdroje	zdroj	k1gInPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
<g/>
)	)	kIx)
<g/>
;	;	kIx,
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Flahertyho	Flaherty	k1gMnSc4
a	a	k8xC
Gaida	Gaida	k1gMnSc1
Japonci	Japonec	k1gMnPc1
vyslýchali	vyslýchat	k5eAaImAgMnP
a	a	k8xC
poté	poté	k6eAd1
je	on	k3xPp3gFnPc4
přivázali	přivázat	k5eAaPmAgMnP
k	k	k7c3
barelům	barel	k1gInPc3
naplněným	naplněný	k2eAgInPc3d1
vodou	voda	k1gFnSc7
a	a	k8xC
hodili	hodit	k5eAaPmAgMnP,k5eAaImAgMnP
přes	přes	k7c4
palubu	paluba	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
utopili	utopit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
195	#num#	k4
<g/>
]	]	kIx)
Osmuse	Osmuse	k1gFnSc2
stihl	stihnout	k5eAaPmAgInS
stejný	stejný	k2eAgInSc1d1
osud	osud	k1gInSc1
<g/>
;	;	kIx,
kladl	klást	k5eAaImAgInS
však	však	k9
<g />
.	.	kIx.
</s>
<s hack="1">
odpor	odpor	k1gInSc4
a	a	k8xC
byl	být	k5eAaImAgMnS
zabit	zabít	k5eAaPmNgMnS
požární	požární	k2eAgFnSc7d1
sekerou	sekera	k1gFnSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeho	jeho	k3xOp3gNnSc4
tělo	tělo	k1gNnSc4
hodili	hodit	k5eAaPmAgMnP,k5eAaImAgMnP
přes	přes	k7c4
palubu	paluba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
196	#num#	k4
<g/>
]	]	kIx)
Nagumem	Nagum	k1gInSc7
zhotovené	zhotovený	k2eAgNnSc1d1
hlášení	hlášení	k1gNnSc1
stručně	stručně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
praporčík	praporčík	k1gMnSc1
Osmus	Osmus	k1gMnSc1
„	„	k?
<g/>
…	…	k?
zemřel	zemřít	k5eAaPmAgInS
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
moři	moře	k1gNnSc6
<g/>
“	“	k?
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
197	#num#	k4
<g/>
]	]	kIx)
O	O	kA
<g/>
'	'	kIx"
<g/>
Flahertyho	Flaherty	k1gMnSc2
a	a	k8xC
Gaidovy	Gaidův	k2eAgInPc4d1
osudy	osud	k1gInPc4
nebyly	být	k5eNaImAgFnP
v	v	k7c6
Nagumově	Nagumův	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
zmíněny	zmíněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
198	#num#	k4
<g/>
]	]	kIx)
Uvedený	uvedený	k2eAgInSc1d1
způsob	způsob	k1gInSc1
popravy	poprava	k1gFnSc2
praporčíka	praporčík	k1gMnSc2
Wesleyho	Wesley	k1gMnSc2
Osmuse	Osmuse	k1gFnSc2
zjevně	zjevně	k6eAd1
nařídil	nařídit	k5eAaPmAgMnS
kapitán	kapitán	k1gMnSc1
Araši	Araše	k1gFnSc4
Watanabe	Watanab	k1gInSc5
Yasumasa	Yasumasa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
potopení	potopení	k1gNnSc6
torpédoborce	torpédoborec	k1gMnSc2
Numakaze	Numakaha	k1gFnSc6
v	v	k7c6
prosinci	prosinec	k1gInSc6
1943	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
pokud	pokud	k8xS
by	by	kYmCp3nS
válku	válka	k1gFnSc4
přežil	přežít	k5eAaPmAgMnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
by	by	kYmCp3nS
pravděpodobně	pravděpodobně	k6eAd1
souzen	soudit	k5eAaImNgMnS
jako	jako	k8xC,k8xS
válečný	válečný	k2eAgMnSc1d1
zločinec	zločinec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
199	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonští	japonský	k2eAgMnPc1d1
zajatci	zajatec	k1gMnPc1
</s>
<s>
Japonští	japonský	k2eAgMnPc1d1
trosečníci	trosečník	k1gMnPc1
z	z	k7c2
Hirjú	Hirjú	k1gFnSc2
na	na	k7c6
palubě	paluba	k1gFnSc6
USS	USS	kA
Ballard	Ballard	k1gInSc1
</s>
<s>
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
zachránila	zachránit	k5eAaPmAgFnS
ponorka	ponorka	k1gFnSc1
USS	USS	kA
Trout	trout	k5eAaImF
dva	dva	k4xCgMnPc4
námořníky	námořník	k1gMnPc4
z	z	k7c2
Mikumy	Mikum	k1gInPc1
na	na	k7c6
záchranném	záchranný	k2eAgInSc6d1
voru	vor	k1gInSc6
a	a	k8xC
přepravila	přepravit	k5eAaPmAgFnS
je	být	k5eAaImIp3nS
do	do	k7c2
Pearl	Pearla	k1gFnPc2
Harboru	Harbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
lékařském	lékařský	k2eAgNnSc6d1
ošetření	ošetření	k1gNnSc6
nejméně	málo	k6eAd3
jeden	jeden	k4xCgMnSc1
z	z	k7c2
těchto	tento	k3xDgMnPc2
námořníků	námořník	k1gMnPc2
při	při	k7c6
výslechu	výslech	k1gInSc6
spolupracoval	spolupracovat	k5eAaImAgMnS
a	a	k8xC
poskytl	poskytnout	k5eAaPmAgMnS
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
200	#num#	k4
<g/>
]	]	kIx)
Dalších	další	k2eAgInPc2d1
35	#num#	k4
členů	člen	k1gInPc2
posádky	posádka	k1gFnSc2
Hirjú	Hirjú	k1gFnSc2
na	na	k7c6
záchranném	záchranný	k2eAgInSc6d1
člunu	člun	k1gInSc6
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
torpédoborec	torpédoborec	k1gInSc1
USS	USS	kA
Ballard	Ballard	k1gInSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
si	se	k3xPyFc3
jich	on	k3xPp3gInPc2
všimlo	všimnout	k5eAaPmAgNnS
americké	americký	k2eAgNnSc1d1
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
převezeni	převézt	k5eAaPmNgMnP
na	na	k7c4
Midway	Midwa	k2eAgFnPc4d1
a	a	k8xC
poté	poté	k6eAd1
dopraveni	dopravit	k5eAaPmNgMnP
do	do	k7c2
Pearl	Pearla	k1gFnPc2
Harboru	Harbor	k1gInSc2
na	na	k7c6
transportní	transportní	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
USS	USS	kA
Sirius	Sirius	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
201	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
202	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dopady	dopad	k1gInPc1
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Midway	Midway	k1gInPc4
se	se	k3xPyFc4
často	často	k6eAd1
označuje	označovat	k5eAaImIp3nS
za	za	k7c4
„	„	k?
<g/>
bod	bod	k1gInSc4
zvratu	zvrat	k1gInSc2
války	válka	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
203	#num#	k4
<g/>
]	]	kIx)
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc4
první	první	k4xOgNnSc4
velké	velký	k2eAgNnSc4d1
námořní	námořní	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
Spojenců	spojenec	k1gMnPc2
nad	nad	k7c7
Japonskem	Japonsko	k1gNnSc7
<g/>
[	[	kIx(
<g/>
204	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
dosažené	dosažený	k2eAgInPc1d1
nad	nad	k7c7
silnějším	silný	k2eAgInSc7d2
a	a	k8xC
zkušenějším	zkušený	k2eAgMnSc7d2
protivníkem	protivník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	kYmCp3nS
Japonsko	Japonsko	k1gNnSc1
vyhrálo	vyhrát	k5eAaPmAgNnS
bitvu	bitva	k1gFnSc4
tak	tak	k6eAd1
jednoznačně	jednoznačně	k6eAd1
jako	jako	k8xS,k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
mohlo	moct	k5eAaImAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařit	podařit	k5eAaPmF
dobýt	dobýt	k5eAaPmF
ostrov	ostrov	k1gInSc4
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američanům	Američan	k1gMnPc3
by	by	kYmCp3nS
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
zůstala	zůstat	k5eAaPmAgFnS
jediná	jediný	k2eAgFnSc1d1
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
USS	USS	kA
Saratoga	Saratoga	k1gFnSc1
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
žádný	žádný	k3yNgInSc1
z	z	k7c2
rozestavěných	rozestavěný	k2eAgInPc2d1
nosičů	nosič	k1gInPc2
by	by	kYmCp3nP
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
1942	#num#	k4
nestihli	stihnout	k5eNaPmAgMnP
dokončit	dokončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
usilovaly	usilovat	k5eAaImAgInP
uzavřít	uzavřít	k5eAaPmF
mír	mír	k1gInSc4
s	s	k7c7
Japonskem	Japonsko	k1gNnSc7
<g/>
,	,	kIx,
v	v	k7c4
což	což	k3yRnSc4,k3yQnSc4
Jamomoto	Jamomota	k1gFnSc5
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
země	země	k1gFnSc1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
obnovit	obnovit	k5eAaPmF
operaci	operace	k1gFnSc4
FS	FS	kA
<g/>
,	,	kIx,
tedy	tedy	k8xC
obsazení	obsazení	k1gNnSc1
Fidži	Fidž	k1gFnSc3
a	a	k8xC
Samoy	Samoa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
by	by	kYmCp3nP
také	také	k9
mohli	moct	k5eAaImAgMnP
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Austrálii	Austrálie	k1gFnSc4
<g/>
,	,	kIx,
Aljašku	Aljaška	k1gFnSc4
a	a	k8xC
Cejlon	Cejlon	k1gInSc4
<g/>
;	;	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
dokonce	dokonce	k9
pokusit	pokusit	k5eAaPmF
dobýt	dobýt	k5eAaPmF
Havaj	Havaj	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
205	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
i	i	k8xC
nadále	nadále	k6eAd1
pokoušeli	pokoušet	k5eAaImAgMnP
ovládnout	ovládnout	k5eAaPmF
další	další	k2eAgNnSc4d1
území	území	k1gNnSc4
a	a	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
dosaženou	dosažený	k2eAgFnSc4d1
rovnováhu	rovnováha	k1gFnSc4
sil	síla	k1gFnPc2
přesunuly	přesunout	k5eAaPmAgFnP
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
teprve	teprve	k6eAd1
po	po	k7c6
několika	několik	k4yIc6
měsících	měsíc	k1gInPc6
tvrdých	tvrdý	k2eAgInPc2d1
bojů	boj	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
206	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
204	#num#	k4
<g/>
]	]	kIx)
díky	díky	k7c3
Midway	Midwaa	k1gFnPc1
získali	získat	k5eAaPmAgMnP
Spojenci	spojenec	k1gMnPc1
strategickou	strategický	k2eAgFnSc4d1
iniciativu	iniciativa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydláždila	vydláždit	k5eAaPmAgFnS
cestu	cesta	k1gFnSc4
pro	pro	k7c4
vylodění	vylodění	k1gNnSc4
na	na	k7c4
Guadalcanalu	Guadalcanala	k1gFnSc4
a	a	k8xC
zdlouhavé	zdlouhavý	k2eAgInPc4d1
opotřebovací	opotřebovací	k2eAgInPc4d1
boje	boj	k1gInPc4
o	o	k7c4
Šalamounovy	Šalamounův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
vítězství	vítězství	k1gNnSc3
mohli	moct	k5eAaImAgMnP
začít	začít	k5eAaPmF
útočit	útočit	k5eAaImF
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
než	než	k8xS
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1942	#num#	k4
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
služby	služba	k1gFnSc2
první	první	k4xOgFnSc2
z	z	k7c2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
třídy	třída	k1gFnSc2
Essex	Essex	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
207	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
bitvu	bitva	k1gFnSc4
o	o	k7c6
Guadalcanal	Guadalcanal	k1gFnPc6
někteří	některý	k3yIgMnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
bod	bod	k1gInSc4
obratu	obrat	k1gInSc2
ve	v	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
208	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Památník	památník	k1gInSc1
bitvy	bitva	k1gFnSc2
na	na	k7c6
atolu	atol	k1gInSc6
Midway	Midwaa	k1gFnSc2
</s>
<s>
Někteří	některý	k3yIgMnPc1
dřívější	dřívější	k2eAgMnPc1d1
autoři	autor	k1gMnPc1
uváděli	uvádět	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
velké	velký	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
zkušených	zkušený	k2eAgMnPc2d1
letců	letec	k1gMnPc2
u	u	k7c2
Midway	Midwaa	k1gFnSc2
trvale	trvale	k6eAd1
oslabily	oslabit	k5eAaPmAgFnP
Japonské	japonský	k2eAgNnSc4d1
císařské	císařský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
209	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
203	#num#	k4
<g/>
]	]	kIx)
Novější	nový	k2eAgFnSc2d2
práce	práce	k1gFnSc2
Parshalla	Parshall	k1gMnSc2
a	a	k8xC
Tullyho	Tully	k1gMnSc2
tento	tento	k3xDgInSc4
pohled	pohled	k1gInSc1
reviduje	revidovat	k5eAaImIp3nS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
že	že	k8xS
velké	velký	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
zkušených	zkušený	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
posádek	posádka	k1gFnPc2
(	(	kIx(
<g/>
110	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
necelých	celý	k2eNgNnPc2d1
25	#num#	k4
%	%	kIx~
stavu	stav	k1gInSc6
letců	letec	k1gMnPc2
na	na	k7c6
čtyřech	čtyři	k4xCgFnPc6
letadlových	letadlový	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
210	#num#	k4
<g/>
]	]	kIx)
nebyly	být	k5eNaImAgInP
zdrcující	zdrcující	k2eAgInPc1d1
pro	pro	k7c4
japonské	japonský	k2eAgNnSc4d1
námořní	námořní	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
jako	jako	k8xS,k8xC
celek	celek	k1gInSc4
<g/>
;	;	kIx,
na	na	k7c6
začátku	začátek	k1gInSc6
války	válka	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
japonské	japonský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
disponovalo	disponovat	k5eAaBmAgNnS
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
000	#num#	k4
muži	muž	k1gMnPc7
leteckých	letecký	k2eAgFnPc2d1
posádek	posádka	k1gFnPc2
s	s	k7c7
kvalifikací	kvalifikace	k1gFnSc7
pro	pro	k7c4
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
211	#num#	k4
<g/>
]	]	kIx)
Ztráta	ztráta	k1gFnSc1
čtyř	čtyři	k4xCgFnPc2
těžkých	těžký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
40	#num#	k4
%	%	kIx~
stavu	stav	k1gInSc2
vyškolených	vyškolený	k2eAgMnPc2d1
leteckých	letecký	k2eAgMnPc2d1
mechaniků	mechanik	k1gMnPc2
a	a	k8xC
techniků	technik	k1gMnPc2
<g/>
,	,	kIx,
plus	plus	k1gNnSc1
nepostradatelného	postradatelný	k2eNgInSc2d1
palubního	palubní	k2eAgInSc2d1
leteckého	letecký	k2eAgInSc2d1
personálu	personál	k1gInSc2
a	a	k8xC
zbrojířů	zbrojíř	k1gMnPc2
a	a	k8xC
ztráta	ztráta	k1gFnSc1
organizačních	organizační	k2eAgFnPc2d1
znalostí	znalost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
tento	tento	k3xDgInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
vysoce	vysoce	k6eAd1
kvalifikovaný	kvalifikovaný	k2eAgInSc1d1
personál	personál	k1gInSc1
nesl	nést	k5eAaImAgInS
v	v	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
přesto	přesto	k8xC
byly	být	k5eAaImAgFnP
pro	pro	k7c4
japonské	japonský	k2eAgNnSc4d1
palubní	palubní	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
těžkou	těžký	k2eAgFnSc7d1
ranou	rána	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
212	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
]	]	kIx)
Několik	několik	k4yIc1
měsíců	měsíc	k1gInPc2
po	po	k7c4
Midway	Midway	k1gInPc4
utrpělo	utrpět	k5eAaPmAgNnS
japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
námořní	námořní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
podobné	podobný	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
na	na	k7c6
životech	život	k1gInPc6
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
východních	východní	k2eAgMnPc2d1
Šalomounů	Šalomoun	k1gMnPc2
a	a	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
ostrovů	ostrov	k1gInPc2
Santa	Santo	k1gNnSc2
Cruz	Cruza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
tyto	tento	k3xDgFnPc1
bitvy	bitva	k1gFnPc1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
neustálým	neustálý	k2eAgInSc7d1
úbytkem	úbytek	k1gInSc7
veteránů	veterán	k1gMnPc2
v	v	k7c6
opotřebovacích	opotřebovací	k2eAgInPc6d1
bojích	boj	k1gInPc6
kolem	kolem	k6eAd1
Šalomounových	Šalomounův	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
spustily	spustit	k5eAaPmAgFnP
nekontrolovatelný	kontrolovatelný	k2eNgInSc4d1
propad	propad	k1gInSc4
operačních	operační	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
212	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
Midwayské	Midwayský	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
zůstaly	zůstat	k5eAaPmAgInP
z	z	k7c2
původního	původní	k2eAgInSc2d1
svazu	svaz	k1gInSc2
velkých	velký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
provedly	provést	k5eAaPmAgFnP
útok	útok	k1gInSc4
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
Šókaku	Šókak	k1gInSc2
a	a	k8xC
Zuikaku	Zuikak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc7d1
velkou	velký	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
mohla	moct	k5eAaImAgFnS
rovnocenně	rovnocenně	k6eAd1
operovat	operovat	k5eAaImF
ve	v	k7c6
svazu	svaz	k1gInSc6
s	s	k7c7
Šókaku	Šókak	k1gInSc3
a	a	k8xC
Zuikaku	Zuikak	k1gInSc3
byla	být	k5eAaImAgFnS
Taihó	Taihó	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
však	však	k9
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
služby	služba	k1gFnSc2
až	až	k6eAd1
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rjúdžó	Rjúdžó	k1gFnPc2
a	a	k8xC
Zuihó	Zuihó	k1gFnPc2
byly	být	k5eAaImAgFnP
lehké	lehký	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Džunjó	Džunjó	k1gFnSc1
a	a	k8xC
Hijó	Hijó	k1gFnSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
formálně	formálně	k6eAd1
klasifikovány	klasifikován	k2eAgInPc4d1
jako	jako	k8xC,k8xS
těžké	těžký	k2eAgInPc4d1
nosiče	nosič	k1gInPc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
druhořadá	druhořadý	k2eAgNnPc1d1
plavidla	plavidlo	k1gNnPc1
s	s	k7c7
omezenými	omezený	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
213	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
Japonsku	Japonsko	k1gNnSc6
trvalo	trvat	k5eAaImAgNnS
postavit	postavit	k5eAaPmF
tři	tři	k4xCgInPc4
nosiče	nosič	k1gInPc4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
získalo	získat	k5eAaPmAgNnS
americké	americký	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
více	hodně	k6eAd2
než	než	k8xS
dvě	dva	k4xCgFnPc1
desítky	desítka	k1gFnPc1
těžkých	těžký	k2eAgFnPc2d1
a	a	k8xC
lehkých	lehký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
mnoho	mnoho	k4c1
eskortních	eskortní	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
214	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
roku	rok	k1gInSc3
1942	#num#	k4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
již	již	k6eAd1
třetím	třetí	k4xOgInSc7
rokem	rok	k1gInSc7
probíhal	probíhat	k5eAaImAgInS
program	program	k1gInSc1
výstavby	výstavba	k1gFnSc2
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
zahájený	zahájený	k2eAgMnSc1d1
Second	Second	k1gMnSc1
Vinson	Vinson	k1gMnSc1
Act	Act	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
215	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oba	dva	k4xCgMnPc1
protivníci	protivník	k1gMnPc1
zrychlili	zrychlit	k5eAaPmAgMnP
výcvik	výcvik	k1gInSc4
posádek	posádka	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
využívaly	využívat	k5eAaPmAgInP,k5eAaImAgInP
efektivnější	efektivní	k2eAgInSc4d2
systém	systém	k1gInSc4
rotace	rotace	k1gFnSc2
pilotů	pilot	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
přežilo	přežít	k5eAaPmAgNnS
více	hodně	k6eAd2
veteránů	veterán	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
následně	následně	k6eAd1
odcházeli	odcházet	k5eAaImAgMnP
na	na	k7c4
pozice	pozice	k1gFnPc4
instruktorů	instruktor	k1gMnPc2
nebo	nebo	k8xC
velitelů	velitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
mohli	moct	k5eAaImAgMnP
nováčkům	nováček	k1gMnPc3
dále	daleko	k6eAd2
předávat	předávat	k5eAaImF
své	svůj	k3xOyFgFnPc4
bojové	bojový	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
a	a	k8xC
poznatky	poznatek	k1gInPc4
<g/>
,	,	kIx,
místo	místo	k6eAd1
aby	aby	kYmCp3nP
zůstali	zůstat	k5eAaPmAgMnP
na	na	k7c6
frontě	fronta	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
každá	každý	k3xTgFnSc1
chyba	chyba	k1gFnSc1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
fatální	fatální	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
216	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
bitvy	bitva	k1gFnSc2
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
v	v	k7c6
červnu	červen	k1gInSc6
1944	#num#	k4
Japonci	Japonec	k1gMnPc1
nasadili	nasadit	k5eAaPmAgMnP
rekonstruované	rekonstruovaný	k2eAgNnSc4d1
palubní	palubní	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
početně	početně	k6eAd1
vyrovnalo	vyrovnat	k5eAaBmAgNnS,k5eAaPmAgNnS
původní	původní	k2eAgFnSc3d1
síle	síla	k1gFnSc3
<g/>
,	,	kIx,
avšak	avšak	k8xC
řada	řada	k1gFnSc1
typů	typ	k1gInPc2
již	již	k6eAd1
byla	být	k5eAaImAgFnS
zastaralá	zastaralý	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
jejich	jejich	k3xOp3gFnPc6
kabinách	kabina	k1gFnPc6
seděli	sedět	k5eAaImAgMnP
převážně	převážně	k6eAd1
nezkušení	zkušený	k2eNgMnPc1d1
a	a	k8xC
špatně	špatně	k6eAd1
vycvičení	vycvičený	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
také	také	k9
prokázala	prokázat	k5eAaPmAgFnS
hodnotu	hodnota	k1gFnSc4
již	již	k6eAd1
před	před	k7c7
válkou	válka	k1gFnSc7
zahájené	zahájený	k2eAgFnSc2d1
námořní	námořní	k2eAgFnSc2d1
kryptoanalýzy	kryptoanalýza	k1gFnSc2
a	a	k8xC
sběru	sběr	k1gInSc2
zpravodajských	zpravodajský	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
úsilí	úsilí	k1gNnSc1
pokračovalo	pokračovat	k5eAaImAgNnS
a	a	k8xC
během	během	k7c2
války	válka	k1gFnSc2
expandovalo	expandovat	k5eAaImAgNnS
jak	jak	k6eAd1
na	na	k7c6
tichomořském	tichomořský	k2eAgNnSc6d1
bojišti	bojiště	k1gNnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
v	v	k7c6
Atlantiku	Atlantik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
zpravodajská	zpravodajský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
četných	četný	k2eAgMnPc2d1
a	a	k8xC
významných	významný	k2eAgMnPc2d1
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
dešifrování	dešifrování	k1gNnSc4
japonské	japonský	k2eAgFnSc2d1
depeše	depeše	k1gFnSc2
umožnilo	umožnit	k5eAaPmAgNnS
sestřelení	sestřelení	k1gNnSc1
letadla	letadlo	k1gNnSc2
admirála	admirál	k1gMnSc2
Jamamota	Jamamot	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
218	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Kódová	kódový	k2eAgFnSc1d1
jména	jméno	k1gNnSc2
„	„	k?
<g/>
Val	val	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Kate	kat	k1gMnSc5
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Zeke	Zeke	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
Zero	Zero	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pod	pod	k7c7
kterými	který	k3yIgFnPc7,k3yQgFnPc7,k3yRgFnPc7
jsou	být	k5eAaImIp3nP
tato	tento	k3xDgNnPc1
letadla	letadlo	k1gNnPc1
běžně	běžně	k6eAd1
známa	znám	k2eAgNnPc1d1
<g/>
,	,	kIx,
Spojenci	spojenec	k1gMnPc1
zavedli	zavést	k5eAaPmAgMnP
až	až	k9
koncem	koncem	k7c2
roku	rok	k1gInSc2
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
D3A	D3A	k1gFnPc2
Japonci	Japonec	k1gMnPc1
běžně	běžně	k6eAd1
označovali	označovat	k5eAaImAgMnP
jako	jako	k9
„	„	k?
<g/>
palubní	palubní	k2eAgMnSc1d1
bombardér	bombardér	k1gMnSc1
typu	typ	k1gInSc2
99	#num#	k4
<g/>
“	“	k?
<g/>
,	,	kIx,
B5N	B5N	k1gMnSc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
palubní	palubní	k2eAgInSc1d1
útočný	útočný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typu	typ	k1gInSc2
97	#num#	k4
<g/>
“	“	k?
a	a	k8xC
A6M	A6M	k1gMnSc1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
palubní	palubní	k2eAgInSc1d1
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typu	typ	k1gInSc2
0	#num#	k4
<g/>
“	“	k?
<g/>
;	;	kIx,
ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
Spojenci	spojenec	k1gMnSc6
hovorově	hovorově	k6eAd1
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
nula	nula	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Zero	Zero	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Jiné	jiné	k1gNnSc1
zdroje	zdroj	k1gInSc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
přímý	přímý	k2eAgInSc1d1
zásah	zásah	k1gInSc1
zádi	záď	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tulla	k1gFnPc1
argumentují	argumentovat	k5eAaImIp3nP
pro	pro	k7c4
těsné	těsný	k2eAgNnSc4d1
minutí	minutí	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
poškození	poškození	k1gNnSc4
kormidla	kormidlo	k1gNnSc2
zapříčinil	zapříčinit	k5eAaPmAgInS
výbuch	výbuch	k1gInSc1
bomby	bomba	k1gFnSc2
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
142	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
vyvozují	vyvozovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
Kagu	Kaga	k1gFnSc4
odtáhnout	odtáhnout	k5eAaPmF
zpět	zpět	k6eAd1
do	do	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc1
konstrukce	konstrukce	k1gFnSc1
byla	být	k5eAaImAgFnS
nenávratně	návratně	k6eNd1
narušena	narušit	k5eAaPmNgFnS
požáry	požár	k1gInPc4
a	a	k8xC
nezbylo	zbýt	k5eNaPmAgNnS
by	by	kYmCp3nP
než	než	k8xS
loď	loď	k1gFnSc1
sešrotovat	sešrotovat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Původně	původně	k6eAd1
minolovka	minolovka	k1gFnSc1
<g/>
,	,	kIx,
překlasifikovaná	překlasifikovaný	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c4
remorkér	remorkér	k1gInSc4
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červnu	červen	k1gInSc3
1942	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Údaje	údaj	k1gInPc1
o	o	k7c6
japonských	japonský	k2eAgFnPc6d1
obětech	oběť	k1gFnPc6
sestavila	sestavit	k5eAaPmAgFnS
Sawaichi	Sawaich	k1gFnPc1
Hisae	Hisae	k1gNnPc2
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
knihu	kniha	k1gFnSc4
„	„	k?
<g/>
Midowei	Midowe	k1gFnSc2
Kaisen	Kaisna	k1gFnPc2
<g/>
:	:	kIx,
Kiroku	Kirok	k1gInSc2
<g/>
“	“	k?
str	str	kA
<g/>
.	.	kIx.
550	#num#	k4
<g/>
:	:	kIx,
seznam	seznam	k1gInSc1
byl	být	k5eAaImAgInS
sestaven	sestavit	k5eAaPmNgInS
na	na	k7c6
základě	základ	k1gInSc6
japonských	japonský	k2eAgInPc2d1
prefekturních	prefekturní	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
nejpřesnější	přesný	k2eAgMnSc1d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
174	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Předválečné	předválečný	k2eAgNnSc1d1
Japonsko	Japonsko	k1gNnSc1
nedosahovalo	dosahovat	k5eNaImAgNnS
úrovně	úroveň	k1gFnPc4
mechanizace	mechanizace	k1gFnSc2
srovnatelné	srovnatelný	k2eAgFnPc4d1
se	s	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dobře	dobře	k6eAd1
vycvičené	vycvičený	k2eAgMnPc4d1
a	a	k8xC
zkušené	zkušený	k2eAgMnPc4d1
letecké	letecký	k2eAgMnPc4d1
mechaniky	mechanik	k1gMnPc4
<g/>
,	,	kIx,
údržbáře	údržbář	k1gMnPc4
a	a	k8xC
techniky	technika	k1gFnPc4
<g/>
,	,	kIx,
ztracené	ztracený	k2eAgFnPc4d1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
téměř	téměř	k6eAd1
nemožné	možný	k2eNgNnSc1d1
získat	získat	k5eAaPmF
a	a	k8xC
vyškolit	vyškolit	k5eAaPmF
rovnocennou	rovnocenný	k2eAgFnSc4d1
náhradu	náhrada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k6eAd1
tomu	ten	k3xDgMnSc3
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
používání	používání	k1gNnSc1
strojů	stroj	k1gInPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
populace	populace	k1gFnSc2
měla	mít	k5eAaImAgFnS
technické	technický	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
nebo	nebo	k8xC
zkušenosti	zkušenost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
211	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Šinano	Šinana	k1gFnSc5
se	s	k7c7
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1944	#num#	k4
stala	stát	k5eAaPmAgFnS
teprve	teprve	k6eAd1
čtvrtou	čtvrtý	k4xOgFnSc7
těžkou	těžký	k2eAgFnSc7d1
letadlovou	letadlový	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
Japonsko	Japonsko	k1gNnSc1
během	během	k7c2
války	válka	k1gFnSc2
zařadilo	zařadit	k5eAaPmAgNnS
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
nosičích	nosič	k1gInPc6
Taihó	Taihó	k1gFnSc2
<g/>
,	,	kIx,
Unrjú	Unrjú	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Amagi	Amag	k1gMnPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
217	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Parshall	Parshall	k1gMnSc1
&	&	k?
Tully	Tulla	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
90	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
a	a	k8xC
450	#num#	k4
<g/>
–	–	k?
<g/>
452	#num#	k4
<g/>
↑	↑	k?
Parshall	Parshall	k1gMnSc1
&	&	k?
Tully	Tulla	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
108	#num#	k4
a	a	k8xC
popisek	popisek	k1gInSc1
fotografie	fotografia	k1gFnSc2
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
křižníku	křižník	k1gInSc2
Tone	tonout	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
133	#num#	k4
<g/>
↑	↑	k?
Parshall	Parshall	k1gMnSc1
&	&	k?
Tully	Tulla	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
4581	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
HORAN	Horan	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Worth	Worth	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Williams	Williams	k1gInSc1
a	a	k8xC
Richard	Richard	k1gMnSc1
Leonard	Leonard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Order	Order	k1gMnSc1
of	of	k?
Battle	Battle	k1gFnSc1
<g/>
:	:	kIx,
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
and	and	k?
Aleutians	Aleutians	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
June	jun	k1gMnSc5
1942	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2004-09-16	2004-09-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Parshall	Parshall	k1gMnSc1
&	&	k?
Tully	Tulla	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
524	#num#	k4
<g/>
↑	↑	k?
Parshall	Parshall	k1gMnSc1
&	&	k?
Tully	Tulla	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
18	#num#	k4
citující	citující	k2eAgFnSc2d1
Sawachi	Sawach	k1gFnSc2
Hisae	Hisa	k1gFnSc2
<g/>
:	:	kIx,
Midowei	Midowei	k1gNnSc1
Kaisen	Kaisna	k1gFnPc2
<g/>
:	:	kIx,
Kiroku	Kirok	k1gInSc2
<g/>
1	#num#	k4
2	#num#	k4
The	The	k1gMnPc2
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
June	jun	k1gMnSc5
3	#num#	k4
–	–	k?
6	#num#	k4
<g/>
,	,	kIx,
1942	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Publication	Publication	k1gInSc1
Section	Section	k1gInSc4
<g/>
,	,	kIx,
Combat	Combat	k1gMnSc1
Intelligence	Intelligence	k1gFnSc2
Branch	Branch	k1gMnSc1
<g/>
,	,	kIx,
Office	Office	kA
of	of	k?
Naval	navalit	k5eAaPmRp2nS
Intelligence	Intelligence	k1gFnSc2
<g/>
,	,	kIx,
Unitid	Unitid	k1gInSc1
States	States	k1gInSc1
Navy	Navy	k?
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2003-03-05	2003-03-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Summary	Summara	k1gFnSc2
of	of	k?
our	our	k?
losses	losses	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
:	:	kIx,
June	jun	k1gMnSc5
4	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
1942	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval	navalit	k5eAaPmRp2nS
History	Histor	k1gMnPc7
&	&	k?
Heritage	Heritag	k1gInSc2
Command	Commanda	k1gFnPc2
<g/>
,	,	kIx,
26	#num#	k4
March	March	k1gInSc1
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Dull	Dulla	k1gFnPc2
1978	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
166	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
A	a	k8xC
Brief	Brief	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Aircraft	Aircraft	k1gInSc1
Carriers	Carriers	k1gInSc1
<g/>
:	:	kIx,
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
12	#num#	k4
June	jun	k1gMnSc5
2007	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Parshall	Parshall	k1gMnSc1
&	&	k?
Tully	Tulla	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
229	#num#	k4
<g/>
–	–	k?
<g/>
231	#num#	k4
<g/>
↑	↑	k?
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
od	od	k7c2
koho	kdo	k3yQnSc2,k3yInSc2,k3yRnSc2
opisoval	opisovat	k5eAaImAgInS
legendu	legenda	k1gFnSc4
o	o	k7c6
pěti	pět	k4xCc6
osudových	osudový	k2eAgFnPc6d1
minutách	minuta	k1gFnPc6
viz	vidět	k5eAaImRp2nS
LANSDALE	LANSDALE	kA
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
<g/>
;	;	kIx,
TULLY	TULLY	kA
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BATTLE	BATTLE	kA
of	of	k?
MIDWAY	MIDWAY	kA
<g/>
:	:	kIx,
PARSHALL	PARSHALL	kA
<g/>
/	/	kIx~
<g/>
FUCHIDA	FUCHIDA	kA
Discrepancy	Discrepanc	k2eAgFnPc4d1
In	In	k1gFnPc4
Accounts	Accountsa	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
j-aircraft	j-aircraft	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
2012-01-01	2012-01-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
LANSDALE	LANSDALE	kA
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
HIRYU	HIRYU	kA
KanKo	KanKo	k1gNnSc1
Loss	Loss	k1gInSc1
<g/>
:	:	kIx,
Five	Five	k1gFnSc1
Different	Different	k1gInSc1
Historical	Historical	k1gMnSc1
Accounts	Accounts	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
j-aircraft	j-aircraft	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
2011-12-29	2011-12-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lundstrom	Lundstrom	k1gInSc1
v	v	k7c6
předmluvě	předmluva	k1gFnSc6
k	k	k7c3
Parshall	Parshallum	k1gNnPc2
&	&	k?
Tully	Tulla	k1gFnSc2
<g/>
↑	↑	k?
Parshall	Parshall	k1gMnSc1
&	&	k?
Tully	Tulla	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
431	#num#	k4
<g/>
–	–	k?
<g/>
443	#num#	k4
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
22	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prange	Prange	k1gInSc1
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
13	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
,	,	kIx,
21	#num#	k4
<g/>
–	–	k?
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
39	#num#	k4
<g/>
–	–	k?
<g/>
49	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
22	#num#	k4
<g/>
–	–	k?
<g/>
38.1	38.1	k4
2	#num#	k4
Parshall	Parshalla	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prange	Prange	k1gInSc1
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
23	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prange	Prange	k1gInSc1
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
22	#num#	k4
<g/>
–	–	k?
<g/>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
s.	s.	k?
31	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
66	#num#	k4
<g/>
–	–	k?
<g/>
67	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
After	After	k1gInSc1
the	the	k?
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Midway	Midwaum	k1gNnPc7
Atoll	Atolla	k1gFnPc2
National	National	k1gMnSc5
Wildlife	Wildlif	k1gMnSc5
Refuge	Refugus	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
15	#num#	k4
January	Januara	k1gFnSc2
2009	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prange	Prange	k1gInSc1
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
375	#num#	k4
<g/>
–	–	k?
<g/>
379	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
110	#num#	k4
<g/>
–	–	k?
<g/>
117	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
52	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
63	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
Tully	Tulla	k1gFnSc2
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
50	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
53.1	53.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
51	#num#	k4
<g/>
,	,	kIx,
55.1	55.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
43	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Oil	Oil	k1gFnSc1
and	and	k?
Japanese	Japanese	k1gFnSc1
Strategy	Stratega	k1gFnSc2
in	in	k?
the	the	k?
Solomons	Solomons	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Postulate	Postulat	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
–	–	k?
<g/>
56	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prange	Prange	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
80	#num#	k4
<g/>
–	–	k?
<g/>
81	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cressman	Cressman	k1gMnSc1
1990	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
37	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lord	lord	k1gMnSc1
1967	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
23	#num#	k4
<g/>
–	–	k?
<g/>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
337	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cressman	Cressman	k1gMnSc1
1990	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
37	#num#	k4
<g/>
–	–	k?
<g/>
45.1	45.1	k4
2	#num#	k4
Lord	lord	k1gMnSc1
1967	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
37	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
338	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZIMMERMAN	ZIMMERMAN	kA
<g/>
,	,	kIx,
Dwight	Dwight	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
:	:	kIx,
Repairing	Repairing	k1gInSc1
the	the	k?
Yorktown	Yorktown	k1gInSc1
After	After	k1gInSc1
the	the	k?
Battle	Battle	k1gFnSc2
of	of	k?
the	the	k?
Coral	Coral	k1gMnSc1
Sea	Sea	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faircount	Faircount	k1gInSc1
Media	medium	k1gNnSc2
Group	Group	k1gInSc1
<g/>
,	,	kIx,
26	#num#	k4
May	May	k1gMnSc1
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lord	lord	k1gMnSc1
1967	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
39	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
340	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LUNDSTROM	LUNDSTROM	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
B.	B.	kA
The	The	k1gMnSc1
First	First	k1gMnSc1
Team	team	k1gInSc1
<g/>
:	:	kIx,
Pacific	Pacific	k1gMnSc1
Naval	navalit	k5eAaPmRp2nS
Air	Air	k1gMnSc1
Combat	Combat	k1gMnSc1
from	from	k1gMnSc1
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
to	ten	k3xDgNnSc4
Midway	Midwa	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
159114471	#num#	k4
<g/>
X.	X.	kA
S.	S.	kA
316	#num#	k4
až	až	k9
320	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
340	#num#	k4
<g/>
–	–	k?
<g/>
341	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
93	#num#	k4
<g/>
–	–	k?
<g/>
94	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Scrivner	Scrivner	k1gInSc1
1987	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
8	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
96	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
101	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
65	#num#	k4
<g/>
–	–	k?
<g/>
67	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
63	#num#	k4
<g/>
–	–	k?
<g/>
64	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
450	#num#	k4
<g/>
–	–	k?
<g/>
451	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
89	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
89	#num#	k4
<g/>
–	–	k?
<g/>
91	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parshall	Parshalla	k1gFnPc2
a	a	k8xC
Tully	Tulla	k1gFnSc2
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
78	#num#	k4
<g/>
–	–	k?
<g/>
80	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Peattie	Peattie	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
159	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
85	#num#	k4
<g/>
,	,	kIx,
136	#num#	k4
<g/>
–	–	k?
<g/>
145	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Peattie	Peattie	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
155	#num#	k4
<g/>
–	–	k?
<g/>
159	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stille	Stille	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
351	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
98	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
99	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
99	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
102	#num#	k4
<g/>
–	–	k?
<g/>
104	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
349	#num#	k4
<g/>
–	–	k?
<g/>
351	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
101	#num#	k4
<g/>
–	–	k?
<g/>
102	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Smith	Smith	k1gMnSc1
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
134	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
National	National	k1gFnSc1
Park	park	k1gInSc1
Service	Service	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
:	:	kIx,
Turning	Turning	k1gInSc1
the	the	k?
Tide	Tide	k1gInSc1
in	in	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Out	Out	k1gFnSc1
of	of	k?
Obscurity	Obscurita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
8	#num#	k4
March	March	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
AF	AF	kA
Is	Is	k1gFnSc2
Short	Short	k1gInSc1
of	of	k?
Water	Water	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historical	Historical	k1gFnSc1
Publications	Publicationsa	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BAKER	BAKER	kA
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	Whata	k1gFnPc2
If	If	k1gFnPc2
Japan	japan	k1gInSc1
Had	had	k1gMnSc1
Won	won	k1gInSc1
The	The	k1gMnSc4
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
January	Januara	k1gFnSc2
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Smith	Smith	k1gMnSc1
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
138	#num#	k4
<g/>
–	–	k?
<g/>
141.1	141.1	k4
2	#num#	k4
Willmott	Willmotta	k1gFnPc2
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
304	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
409	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Koenig	Koenig	k1gInSc1
1975	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
212	#num#	k4
<g/>
-	-	kIx~
<g/>
231	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WATSON	WATSON	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VP-44	VP-44	k1gFnSc1
at	at	k?
Ford	ford	k1gInSc1
Island	Island	k1gInSc1
and	and	k?
the	the	k?
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
7	#num#	k4
December	December	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lundstrom	Lundstrom	k1gInSc1
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
238.1	238.1	k4
2	#num#	k4
NIMITZ	NIMITZ	kA
<g/>
,	,	kIx,
Chester	Chester	k1gInSc1
A.	A.	kA
<g/>
,	,	kIx,
Admiral	Admiral	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HyperWar	HyperWar	k1gInSc1
Foundation	Foundation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
CINCPAC	CINCPAC	kA
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
28	#num#	k4
June	jun	k1gMnSc5
1942	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
HyperWar	HyperWar	k1gInSc1
Foundation	Foundation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interrogation	Interrogation	k1gInSc1
of	of	k?
<g/>
:	:	kIx,
Captain	Captain	k1gInSc1
Toyama	Toyamum	k1gNnSc2
<g/>
,	,	kIx,
Yasumi	Yasu	k1gFnPc7
<g/>
,	,	kIx,
IJN	IJN	kA
<g/>
;	;	kIx,
Chief	Chief	k1gMnSc1
of	of	k?
Staff	Staff	k1gMnSc1
Second	Second	k1gMnSc1
Destroyer	Destroyer	k1gMnSc1
Squadron	Squadron	k1gMnSc1
<g/>
,	,	kIx,
flagship	flagship	k1gMnSc1
Jintsu	Jints	k1gInSc2
(	(	kIx(
<g/>
CL	CL	kA
<g/>
)	)	kIx)
<g/>
</s>
<s>
,	,	kIx,
at	at	k?
Midway	Midwaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Strategic	Strategic	k1gMnSc1
Bombing	Bombing	k1gInSc4
Survey	Survea	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
107	#num#	k4
<g/>
–	–	k?
<g/>
112	#num#	k4
<g/>
,	,	kIx,
126	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
128	#num#	k4
<g/>
,	,	kIx,
132	#num#	k4
<g/>
–	–	k?
<g/>
134	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stephen	Stephen	k1gInSc1
1988	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
166	#num#	k4
<g/>
–	–	k?
<g/>
167	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
200	#num#	k4
<g/>
–	–	k?
<g/>
204	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lord	lord	k1gMnSc1
1967	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
110	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
149.1	149.1	k4
2	#num#	k4
Prange	Prange	k1gFnPc2
<g/>
,	,	kIx,
Goldstein	Goldsteina	k1gFnPc2
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
207	#num#	k4
<g/>
–	–	k?
<g/>
212.1	212.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
149	#num#	k4
<g/>
–	–	k?
<g/>
152	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Office	Office	kA
of	of	k?
Naval	navalit	k5eAaPmRp2nS
Intelligence	Intelligenka	k1gFnSc3
Combat	Combat	k1gMnSc1
Narrative	Narrativ	k1gInSc5
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Midway	Midwaa	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Attack	Attack	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Enemy	Enem	k1gInPc1
Carriers	Carriers	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
176	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
151	#num#	k4
<g/>
–	–	k?
<g/>
153	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
Clair	Clair	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
WWII	WWII	kA
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
pilot	pilot	k1gMnSc1
honored	honored	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Billings	Billings	k1gInSc1
Gazette	Gazett	k1gInSc5
<g/>
.	.	kIx.
4	#num#	k4
June	jun	k1gMnSc5
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
December	December	k1gInSc1
2020	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lord	lord	k1gMnSc1
1967	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
116	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
118	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
549	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
151	#num#	k4
<g/>
–	–	k?
<g/>
152	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
130	#num#	k4
<g/>
–	–	k?
<g/>
132	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
156	#num#	k4
<g/>
–	–	k?
<g/>
159	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Isom	Isom	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
129	#num#	k4
<g/>
–	–	k?
<g/>
139	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prange	Prange	k1gInSc1
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
216	#num#	k4
<g/>
–	–	k?
<g/>
217	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Parshall	Parshalla	k1gFnPc2
a	a	k8xC
Tully	Tulla	k1gFnSc2
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
159	#num#	k4
<g/>
–	–	k?
<g/>
161	#num#	k4
<g/>
,	,	kIx,
183	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bicheno	Bichen	k2eAgNnSc1d1
2001	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
134	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
165	#num#	k4
<g/>
–	–	k?
<g/>
170	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
s.	s.	k?
168	#num#	k4
<g/>
–	–	k?
<g/>
173	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
231	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
121	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prange	Prange	k1gInSc1
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
233	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prange	Prange	k1gInSc1
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
217	#num#	k4
<g/>
–	–	k?
<g/>
218	#num#	k4
<g/>
,	,	kIx,
372	#num#	k4
<g/>
–	–	k?
<g/>
373.1	373.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
170	#num#	k4
<g/>
–	–	k?
<g/>
173	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prange	Prange	k1gInSc1
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
s.	s.	k?
231	#num#	k4
<g/>
–	–	k?
<g/>
237.1	237.1	k4
2	#num#	k4
Willmott	Willmotta	k1gFnPc2
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
395	#num#	k4
<g/>
–	–	k?
<g/>
398	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
174	#num#	k4
<g/>
–	–	k?
<g/>
175	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
182.1	182.1	k4
2	#num#	k4
3	#num#	k4
Shepherd	Shepherda	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lundstrom	Lundstrom	k1gInSc1
1984	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
332	#num#	k4
<g/>
–	–	k?
<g/>
333.1	333.1	k4
2	#num#	k4
3	#num#	k4
Cressman	Cressman	k1gMnSc1
1990	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
84	#num#	k4
<g/>
–	–	k?
<g/>
89.1	89.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
215	#num#	k4
<g/>
–	–	k?
<g/>
216	#num#	k4
<g/>
,	,	kIx,
226	#num#	k4
<g/>
–	–	k?
<g/>
227.1	227.1	k4
2	#num#	k4
3	#num#	k4
Buell	Buella	k1gFnPc2
1987	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
494	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SHEPHERD	SHEPHERD	kA
<g/>
,	,	kIx,
Joel	Joel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
USS	USS	kA
ENTERPRISE	ENTERPRISE	kA
CV-	CV-	k1gFnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
1942	#num#	k4
-	-	kIx~
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
174	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mrazek	Mrazek	k?
2008	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
113	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lundstrom	Lundstrom	k1gInSc1
1984	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
341.1	341.1	k4
2	#num#	k4
Ewing	Ewinga	k1gFnPc2
2004	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
71	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
86	#num#	k4
<g/>
,	,	kIx,
307	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cressman	Cressman	k1gMnSc1
1990	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
91	#num#	k4
<g/>
–	–	k?
<g/>
94	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Recollections	Recollections	k1gInSc1
of	of	k?
Lieutenant	Lieutenant	k1gInSc1
George	George	k1gFnSc1
Gay	gay	k1gMnSc1
<g/>
,	,	kIx,
USNR	USNR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval	navalit	k5eAaPmRp2nS
History	Histor	k1gMnPc7
and	and	k?
Heritage	Heritag	k1gInSc2
Command	Commanda	k1gFnPc2
<g/>
,	,	kIx,
September	Septembra	k1gFnPc2
21	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Blair	Blair	k1gMnSc1
1975	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
238	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Douglas	Douglas	k1gInSc1
TBD	TBD	kA
Devastator	Devastator	k1gInSc1
Torpedo	Torpedo	k1gNnSc1
Bomber	Bomber	k1gInSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
7	#num#	k4
September	September	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Thruelsen	Thruelsen	k1gInSc1
1976	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
186	#num#	k4
<g/>
,	,	kIx,
189	#num#	k4
<g/>
,	,	kIx,
190	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SHEPHERD	SHEPHERD	kA
<g/>
,	,	kIx,
Joel	Joel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
USS	USS	kA
ENTERPRISE	ENTERPRISE	kA
CV-	CV-	k1gFnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
1942	#num#	k4
-	-	kIx~
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Crenshaw	Crenshaw	k1gFnSc1
1995	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
158	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Morison	Morison	k1gInSc1
1949	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
230	#num#	k4
<g/>
–	–	k?
<g/>
232	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PATRICK	PATRICK	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Hard	Hard	k1gMnSc1
Lessons	Lessonsa	k1gFnPc2
of	of	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
Torpedo	Torpedo	k1gNnSc1
Failures	Failures	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Undersea	Underse	k1gInSc2
Warfare	Warfar	k1gMnSc5
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
23	#num#	k4
July	Jula	k1gFnSc2
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
226	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
227	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bicheno	Bichen	k2eAgNnSc1d1
2001	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
62	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
IJN	IJN	kA
Kirishima	Kirishima	k1gFnSc1
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
combinedfleet	combinedfleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
10	#num#	k4
June	jun	k1gMnSc5
2007	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tillman	Tillman	k1gMnSc1
1976	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
69	#num#	k4
<g/>
–	–	k?
<g/>
73	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SHEPHERD	SHEPHERD	kA
<g/>
,	,	kIx,
Joel	Joel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
USS	USS	kA
ENTERPRISE	ENTERPRISE	kA
CV-	CV-	k1gFnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
1942	#num#	k4
-	-	kIx~
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Accounts	Accountsa	k1gFnPc2
–	–	k?
C.	C.	kA
Wade	Wade	k?
McClusky	McCluska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Prange	Prange	k1gInSc1
<g/>
,	,	kIx,
Goldstein	Goldstein	k1gInSc1
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
259	#num#	k4
<g/>
–	–	k?
<g/>
261	#num#	k4
<g/>
,	,	kIx,
267	#num#	k4
<g/>
–	–	k?
<g/>
269	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Cressman	Cressman	k1gMnSc1
1990	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
96	#num#	k4
<g/>
–	–	k?
<g/>
97	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Parshall	Parshalla	k1gFnPc2
a	a	k8xC
Tully	Tulla	k1gFnSc2
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
250	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
235	#num#	k4
<g/>
–	–	k?
<g/>
236	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Miller	Miller	k1gMnSc1
2001	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
123.1	123.1	k4
2	#num#	k4
Beevor	Beevora	k1gFnPc2
2012	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
310.1	310.1	k4
2	#num#	k4
Keegan	Keegana	k1gFnPc2
2004	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
216	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parshall	Parshalla	k1gFnPc2
a	a	k8xC
Tully	Tulla	k1gFnSc2
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
253	#num#	k4
<g/>
–	–	k?
<g/>
259.1	259.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
330	#num#	k4
<g/>
–	–	k?
<g/>
353	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lord	lord	k1gMnSc1
1967	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
183	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
260	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BOB	Bob	k1gMnSc1
HACKETT	HACKETT	kA
&	&	k?
SANDER	Sandra	k1gFnPc2
KINGSEPP	KINGSEPP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
HIJMS	HIJMS	kA
Nagara	Nagara	k1gFnSc1
<g/>
:	:	kIx,
Tabular	Tabular	k1gMnSc1
Record	Record	k1gMnSc1
of	of	k?
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parshall	Parshalla	k1gFnPc2
a	a	k8xC
Tully	Tulla	k1gFnSc2
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
337	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lord	lord	k1gMnSc1
1967	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
216	#num#	k4
<g/>
–	–	k?
<g/>
217	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
262	#num#	k4
<g/>
,	,	kIx,
292	#num#	k4
<g/>
–	–	k?
<g/>
299	#num#	k4
<g/>
,	,	kIx,
312	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
Tully	Tulla	k1gFnSc2
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
312	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
311	#num#	k4
<g/>
,	,	kIx,
316	#num#	k4
<g/>
,	,	kIx,
318	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
323.1	323.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
328	#num#	k4
<g/>
–	–	k?
<g/>
329	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
354	#num#	k4
<g/>
–	–	k?
<g/>
359	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
356.1	356.1	k4
2	#num#	k4
Potter	Pottra	k1gFnPc2
a	a	k8xC
Nimitz	Nimitza	k1gFnPc2
1960	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
682.1	682.1	k4
2	#num#	k4
3	#num#	k4
Blair	Blair	k1gMnSc1
1975	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
246	#num#	k4
<g/>
–	–	k?
<g/>
247	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
344	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
382	#num#	k4
<g/>
–	–	k?
<g/>
383	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
381	#num#	k4
<g/>
–	–	k?
<g/>
382	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
364	#num#	k4
<g/>
–	–	k?
<g/>
365.1	365.1	k4
2	#num#	k4
Blair	Blair	k1gMnSc1
1975	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
s.	s.	k?
250.1	250.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
359.1	359.1	k4
2	#num#	k4
Prange	Prange	k1gFnPc2
<g/>
,	,	kIx,
Goldstein	Goldsteina	k1gFnPc2
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
320.1	320.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
345	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
345	#num#	k4
<g/>
–	–	k?
<g/>
346	#num#	k4
<g/>
,	,	kIx,
diagram	diagram	k1gInSc1
347	#num#	k4
<g/>
,	,	kIx,
348	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ALLEN	Allen	k1gMnSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
B.	B.	kA
Return	Return	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
National	National	k1gFnPc7
Geographic	Geographice	k1gInPc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gMnSc1
<g/>
:	:	kIx,
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
<g/>
,	,	kIx,
April	April	k1gMnSc1
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
80	#num#	k4
<g/>
–	–	k?
<g/>
103	#num#	k4
(	(	kIx(
<g/>
p.	p.	k?
89	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
11	#num#	k4
October	October	k1gInSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27	#num#	k4
<g/>
-	-	kIx~
<g/>
9358	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
377	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
362	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lord	lord	k1gMnSc1
1967	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
280	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Yorktown	Yorktown	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
7	#num#	k4
April	April	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
374	#num#	k4
<g/>
–	–	k?
<g/>
375	#num#	k4
<g/>
,	,	kIx,
383	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
476	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
378	#num#	k4
<g/>
,	,	kIx,
380	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parshall	Parshalla	k1gFnPc2
a	a	k8xC
Tully	Tulla	k1gFnSc2
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
114	#num#	k4
<g/>
,	,	kIx,
365	#num#	k4
<g/>
,	,	kIx,
377	#num#	k4
<g/>
–	–	k?
<g/>
380	#num#	k4
<g/>
,	,	kIx,
476	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SBD-2	SBD-2	k1gMnSc1
Aircraft	Aircraft	k1gMnSc1
<g/>
,	,	kIx,
Bureau	Bureaa	k1gFnSc4
Number	Numbero	k1gNnPc2
2106	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Naval	navalit	k5eAaPmRp2nS
Aviation	Aviation	k1gInSc1
Museum	museum	k1gNnSc1
Collections	Collections	k1gInSc1
<g/>
,	,	kIx,
13	#num#	k4
January	Januara	k1gFnSc2
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
29	#num#	k4
June	jun	k1gMnSc5
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Blair	Blair	k1gMnSc1
1975	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
247	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lundstrom	Lundstrom	k1gInSc1
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
293	#num#	k4
<g/>
–	–	k?
<g/>
296	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Morison	Morison	k1gInSc1
1949	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
142	#num#	k4
<g/>
–	–	k?
<g/>
143	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
330	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
382	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Toll	Toll	k1gInSc1
2012	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
471	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NAGUMO	NAGUMO	kA
<g/>
,	,	kIx,
Chū	Chū	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval	navalit	k5eAaPmRp2nS
History	Histor	k1gMnPc7
and	and	k?
Heritage	Heritag	k1gInSc2
Command	Commanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Japanese	Japanese	k1gFnSc1
Story	story	k1gFnSc1
of	of	k?
the	the	k?
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
Translation	Translation	k1gInSc4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foreward	Foreward	k1gMnSc1
by	by	k9
Ralph	Ralph	k1gMnSc1
A.	A.	kA
Ofstie	Ofstie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Office	Office	kA
of	of	k?
Naval	navalit	k5eAaPmRp2nS
Intelligence	Intelligence	k1gFnSc2
<g/>
,	,	kIx,
15	#num#	k4
June	jun	k1gMnSc5
1942	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
92	#num#	k4
<g/>
–	–	k?
<g/>
93	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bix	Bix	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
2001	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
449	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
386	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
386	#num#	k4
<g/>
–	–	k?
<g/>
387	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
388	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
388	#num#	k4
<g/>
–	–	k?
<g/>
389	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
390	#num#	k4
<g/>
–	–	k?
<g/>
391	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Navy	Navy	k?
<g/>
.	.	kIx.
<g/>
togetherweserved	togetherweserved	k1gMnSc1
<g/>
:	:	kIx,
Osmus	Osmus	k1gInSc1
<g/>
,	,	kIx,
Wesley	Wesley	k1gInPc1
<g/>
,	,	kIx,
ENS	ENS	kA
<g/>
"	"	kIx"
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
"	"	kIx"
<g/>
Navy	Navy	k?
<g/>
.	.	kIx.
<g/>
togetherweserved	togetherweserved	k1gMnSc1
<g/>
:	:	kIx,
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Flaherty	Flahert	k1gMnPc4
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
Woodrow	Woodrow	k1gMnSc1
<g/>
,	,	kIx,
ENS	ENS	kA
<g/>
"	"	kIx"
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Navy	Navy	k?
<g/>
.	.	kIx.
<g/>
togetherweserved	togetherweserved	k1gInSc1
<g/>
:	:	kIx,
Gaido	Gaido	k1gNnSc1
<g/>
,	,	kIx,
Bruno	Bruno	k1gMnSc1
Peter	Peter	k1gMnSc1
<g/>
,	,	kIx,
PO	Po	kA
<g/>
1	#num#	k4
<g/>
"	"	kIx"
<g/>
↑	↑	k?
"	"	kIx"
<g/>
War	War	k1gMnSc1
crimes	crimes	k1gMnSc1
of	of	k?
the	the	k?
Imperial	Imperial	k1gInSc1
Japanese	Japanese	k1gFnSc1
Navy	Navy	k?
<g/>
"	"	kIx"
<g/>
↑	↑	k?
SHEPHERD	SHEPHERD	kA
<g/>
,	,	kIx,
Joel	Joel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
USS	USS	kA
ENTERPRISE	ENTERPRISE	kA
CV-	CV-	k1gFnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
1942	#num#	k4
-	-	kIx~
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Prisoners	Prisonersa	k1gFnPc2
of	of	k?
War	War	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Barde	bard	k1gMnSc5
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
188	#num#	k4
<g/>
–	–	k?
<g/>
192	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
War	War	k1gMnSc1
crimes	crimes	k1gMnSc1
of	of	k?
the	the	k?
Imperial	Imperial	k1gInSc1
Japanese	Japanese	k1gFnSc1
Navy	Navy	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
583	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
566	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
584	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Naval	navalit	k5eAaPmRp2nS
History	Histor	k1gMnPc7
and	and	k?
Heritage	Heritag	k1gInSc2
Command	Command	k1gInSc1
<g/>
,	,	kIx,
Interrogation	Interrogation	k1gInSc1
of	of	k?
Japanese	Japanese	k1gFnSc2
Prisoners	Prisonersa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Naval	navalit	k5eAaPmRp2nS
History	Histor	k1gMnPc7
and	and	k?
Heritage	Heritag	k1gInSc2
Command	Commanda	k1gFnPc2
<g/>
,	,	kIx,
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Naval	navalit	k5eAaPmRp2nS
History	Histor	k1gMnPc7
and	and	k?
Heritage	Heritag	k1gInSc2
Command	Commanda	k1gFnPc2
<g/>
,	,	kIx,
Survivors	Survivorsa	k1gFnPc2
of	of	k?
Hiryu	Hiryus	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Prange	Prange	k1gFnPc2
<g/>
,	,	kIx,
Goldstein	Goldsteina	k1gFnPc2
a	a	k8xC
Dillon	Dillon	k1gInSc1
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
395.1	395.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
416	#num#	k4
<g/>
–	–	k?
<g/>
430	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BAKER	BAKER	kA
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	Whata	k1gFnPc2
If	If	k1gFnPc2
Japan	japan	k1gInSc1
Had	had	k1gMnSc1
Won	won	k1gInSc1
The	The	k1gMnSc4
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Diplomat	diplomat	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
January	Januara	k1gFnSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
522	#num#	k4
<g/>
–	–	k?
<g/>
523	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
422	#num#	k4
<g/>
–	–	k?
<g/>
423	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FISHER	FISHER	kA
<g/>
,	,	kIx,
Scott	Scott	k1gMnSc1
<g/>
;	;	kIx,
FORNEY	FORNEY	kA
<g/>
,	,	kIx,
Nathan	Nathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnPc2
Turning	Turning	k1gInSc4
Point	pointa	k1gFnPc2
of	of	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
War	War	k1gMnSc1
<g/>
:	:	kIx,
Two	Two	k1gFnSc1
Views	Views	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CombinedFleet	CombinedFleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1996	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Willmott	Willmott	k1gInSc1
1983	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
519	#num#	k4
<g/>
–	–	k?
<g/>
523	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshalla	k1gFnPc2
a	a	k8xC
Tully	Tulla	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
432	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
417.1	417.1	k4
2	#num#	k4
Parshall	Parshallum	k1gNnPc2
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
416	#num#	k4
<g/>
–	–	k?
<g/>
417	#num#	k4
<g/>
,	,	kIx,
432	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
421	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Why	Why	k1gFnSc1
Japan	japan	k1gInSc1
Really	Realla	k1gFnSc2
Lost	Lost	k1gMnSc1
The	The	k1gMnSc1
War	War	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CombinedFleet	CombinedFleet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Davidson	Davidson	k1gInSc1
1996	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
21	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Parshall	Parshall	k1gInSc1
a	a	k8xC
Tully	Tull	k1gInPc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
390	#num#	k4
<g/>
–	–	k?
<g/>
392	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chesneau	Chesneaus	k1gInSc2
1980	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
169	#num#	k4
<g/>
–	–	k?
<g/>
170	#num#	k4
<g/>
,	,	kIx,
183	#num#	k4
<g/>
–	–	k?
<g/>
184	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZIMMERMAN	ZIMMERMAN	kA
<g/>
,	,	kIx,
Dwight	Dwight	k2eAgMnSc1d1
Jon	Jon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operation	Operation	k1gInSc1
Vengeance	Vengeanec	k1gInSc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Mission	Mission	k1gInSc1
to	ten	k3xDgNnSc4
Kill	Kill	k1gMnSc1
Admiral	Admiral	k1gMnSc1
Yamamoto	Yamamota	k1gFnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
15	#num#	k4
September	September	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cizojazyčná	cizojazyčný	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
BARDE	bard	k1gMnSc5
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
E.	E.	kA
<g/>
,	,	kIx,
December	December	k1gInSc1
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Midway	Midway	k1gInPc4
<g/>
:	:	kIx,
Tarnished	Tarnished	k1gMnSc1
Victory	Victor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Military	Militara	k1gFnSc2
Affairs	Affairsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
899	#num#	k4
<g/>
-	-	kIx~
<g/>
3718	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
BEEVOR	BEEVOR	kA
<g/>
,	,	kIx,
Antony	anton	k1gInPc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Second	Second	k1gMnSc1
World	World	k1gMnSc1
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Back	Back	k1gInSc1
Bay	Bay	k1gFnSc2
Books	Booksa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
316	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2375	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
BICHENO	BICHENO	kA
<g/>
,	,	kIx,
Hugh	Hugh	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Midway	Midwaum	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Orion	orion	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
304	#num#	k4
<g/>
-	-	kIx~
<g/>
35715	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
BIX	BIX	kA
<g/>
,	,	kIx,
Herbert	Herbert	k1gMnSc1
P.	P.	kA
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hirohito	Hirohit	k2eAgNnSc1d1
and	and	k?
the	the	k?
Making	Making	k1gInSc1
of	of	k?
Modern	Modern	k1gInSc1
Japan	japan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Perennial	Perennial	k1gInSc1
/	/	kIx~
HarperCollinsPublishers	HarperCollinsPublishers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
19314	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
BLAIR	Blair	k1gMnSc1
<g/>
,	,	kIx,
Clay	Claa	k1gFnPc1
<g/>
,	,	kIx,
Jr	Jr	k1gFnPc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silent	Silent	k1gInSc1
Victory	Victor	k1gMnPc7
<g/>
:	:	kIx,
The	The	k1gMnSc7
U.	U.	kA
<g/>
S.	S.	kA
Submarine	Submarin	k1gInSc5
War	War	k1gFnPc7
Against	Against	k1gFnSc1
Japan	japan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philadelphia	Philadelphia	k1gFnSc1
<g/>
:	:	kIx,
J.B.	J.B.	k1gMnSc1
Lippincott	Lippincott	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
397	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
753	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
BUELL	BUELL	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
B.	B.	kA
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Quiet	Quiet	k1gMnSc1
Warrior	Warrior	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Biography	Biographa	k1gFnSc2
of	of	k?
Admiral	Admiral	k1gMnSc1
Raymond	Raymond	k1gMnSc1
A.	A.	kA
Spruance	Spruanec	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Press	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87021	#num#	k4
<g/>
-	-	kIx~
<g/>
562	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
CHESNEAU	CHESNEAU	kA
<g/>
,	,	kIx,
Roger	Roger	k1gMnSc1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Conway	Conwaa	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
All	All	k1gFnSc7
the	the	k?
World	Worlda	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Fighting	Fighting	k1gInSc1
Ships	Shipsa	k1gFnPc2
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Conway	Conway	k1gInPc1
Maritime	Maritim	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
85177	#num#	k4
<g/>
-	-	kIx~
<g/>
146	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
CRENSHAW	CRENSHAW	kA
<g/>
,	,	kIx,
Russell	Russell	k1gMnSc1
Sydnor	Sydnor	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Battle	Battle	k1gFnSc1
of	of	k?
Tassafaronga	Tassafaronga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baltimore	Baltimore	k1gInSc1
<g/>
,	,	kIx,
Maryland	Maryland	k1gInSc1
<g/>
:	:	kIx,
Nautical	Nautical	k1gFnSc1
&	&	k?
Aviation	Aviation	k1gInSc4
Pub	Pub	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k8xS
<g/>
.	.	kIx.
of	of	k?
America	America	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
877853	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
CRESSMAN	CRESSMAN	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
J.	J.	kA
<g/>
;	;	kIx,
EWING	EWING	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
;	;	kIx,
TILLMAN	TILLMAN	kA
<g/>
,	,	kIx,
Barrett	Barrett	k1gMnSc1
<g/>
;	;	kIx,
HORAN	Horan	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
;	;	kIx,
REYNOLDS	REYNOLDS	kA
<g/>
,	,	kIx,
Clark	Clark	k1gInSc1
<g/>
;	;	kIx,
COHEN	COHEN	kA
<g/>
,	,	kIx,
Stan	stan	k1gInSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
"	"	kIx"
<g/>
A	a	k9
Glorious	Glorious	k1gMnSc1
Page	Pag	k1gFnSc2
in	in	k?
our	our	k?
History	Histor	k1gMnPc7
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Adm.	Adm.	k1gMnSc1
Chester	Chester	k1gMnSc1
Nimitz	Nimitz	k1gMnSc1
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnSc1
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
June	jun	k1gMnSc5
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missoula	Missoula	k1gFnSc1
<g/>
,	,	kIx,
Montana	Montana	k1gFnSc1
<g/>
:	:	kIx,
Pictorial	Pictorial	k1gMnSc1
Histories	Histories	k1gMnSc1
Pub	Pub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k8xS
<g/>
..	..	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
929521	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
DAVIDSON	DAVIDSON	kA
<g/>
,	,	kIx,
Joel	Joel	k1gInSc1
R.	R.	kA
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Unsinkable	Unsinkable	k1gMnSc1
Fleet	Fleet	k1gMnSc1
<g/>
:	:	kIx,
the	the	k?
Politics	Politics	k1gInSc1
of	of	k?
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
Expansion	Expansion	k1gInSc1
in	in	k?
World	World	k1gInSc1
War	War	k1gMnSc1
II	II	kA
<g/>
..	..	k?
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
55750	#num#	k4
<g/>
-	-	kIx~
<g/>
156	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
DULL	DULL	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
S.	S.	kA
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Battle	Battle	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Imperial	Imperial	k1gInSc1
Japanese	Japanese	k1gFnSc1
Navy	Navy	k?
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Press	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
219	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
EWING	EWING	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thach	Thach	k1gInSc1
Weave	Weaev	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Life	Lif	k1gFnSc2
of	of	k?
Jimmie	Jimmie	k1gFnSc2
Thach	Thacha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
248	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
ISOM	ISOM	kA
<g/>
,	,	kIx,
Dallas	Dallas	k1gInSc1
Woodbury	Woodbura	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Midway	Midwaa	k1gFnSc2
Inquest	Inquest	k1gFnSc1
<g/>
:	:	kIx,
Why	Why	k1gFnSc1
the	the	k?
Japanese	Japanese	k1gFnSc2
Lost	Lost	k1gMnSc1
the	the	k?
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomington	Bloomington	k1gInSc1
<g/>
,	,	kIx,
Indiana	Indiana	k1gFnSc1
<g/>
:	:	kIx,
Indiana	Indiana	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
253	#num#	k4
<g/>
-	-	kIx~
<g/>
34904	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
KEEGAN	KEEGAN	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intelligence	Intelligenec	k1gInSc2
in	in	k?
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Vintage	Vintage	k1gNnSc1
Books	Booksa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
375	#num#	k4
<g/>
-	-	kIx~
<g/>
70046	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
KEEGAN	KEEGAN	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Second	Second	k1gMnSc1
World	World	k1gMnSc1
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Penguin	Penguin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
303573	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
904565693	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
KOENIG	KOENIG	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
J.	J.	kA
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epic	Epic	k1gFnSc1
Sea	Sea	k1gFnSc2
Battles	Battlesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Peerage	Peerage	k1gFnSc1
Books	Booksa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
907408	#num#	k4
<g/>
-	-	kIx~
<g/>
43	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
70866344	#num#	k4
Kapitola	kapitola	k1gFnSc1
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
LORD	lord	k1gMnSc1
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Incredible	Incredible	k1gFnPc2
Victory	Victor	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Harper	Harper	k1gInSc1
and	and	k?
Row	Row	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
58080	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
59	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
LUNDSTROM	LUNDSTROM	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
B.	B.	kA
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
First	First	k1gFnSc1
Team	team	k1gInSc1
<g/>
:	:	kIx,
Pacific	Pacific	k1gMnSc1
Naval	navalit	k5eAaPmRp2nS
Air	Air	k1gMnSc1
Combat	Combat	k1gMnSc1
from	from	k1gMnSc1
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
to	ten	k3xDgNnSc4
Midway	Midwa	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
471	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
LUNDSTROM	LUNDSTROM	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
B.	B.	kA
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Black	Black	k1gMnSc1
Shoe	Sho	k1gInSc2
Carrier	Carrier	k1gMnSc1
Admiral	Admiral	k1gMnSc1
<g/>
:	:	kIx,
Frank	Frank	k1gMnSc1
Jack	Jack	k1gMnSc1
Fletcher	Fletchra	k1gFnPc2
at	at	k?
Coral	Coral	k1gMnSc1
Sea	Sea	k1gMnSc1
<g/>
,	,	kIx,
Midway	Midway	k1gInPc1
<g/>
,	,	kIx,
and	and	k?
Guadalcanal	Guadalcanal	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Press	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
475	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
62782215	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
L.	L.	kA
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Story	story	k1gFnSc2
of	of	k?
World	World	k1gMnSc1
War	War	k1gFnSc3
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Simon	Simon	k1gMnSc1
&	&	k?
Schuster	Schuster	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7432	#num#	k4
<g/>
-	-	kIx~
<g/>
2718	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
MORISON	MORISON	kA
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
E.	E.	kA
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Coral	Coral	k1gMnSc1
Sea	Sea	k1gMnSc1
<g/>
,	,	kIx,
Midway	Midwaa	k1gFnPc1
and	and	k?
Submarine	Submarin	k1gInSc5
Actions	Actions	k1gInSc1
<g/>
:	:	kIx,
May	May	k1gMnSc1
1942	#num#	k4
–	–	k?
August	August	k1gMnSc1
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boston	Boston	k1gInSc1
<g/>
:	:	kIx,
Little	Little	k1gFnSc1
<g/>
,	,	kIx,
Brown	Brown	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
History	Histor	k1gInPc1
of	of	k?
United	United	k1gInSc1
States	States	k1gInSc1
Naval	navalit	k5eAaPmRp2nS
Operations	Operations	k1gInSc1
in	in	k?
World	World	k1gInSc1
War	War	k1gMnSc1
II	II	kA
<g/>
;	;	kIx,
sv.	sv.	kA
Volume	volum	k1gInSc5
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
316	#num#	k4
<g/>
-	-	kIx~
<g/>
58304	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
MRAZEK	MRAZEK	k?
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Dawn	Dawn	k1gMnSc1
Like	Lik	k1gFnSc2
Thunder	Thunder	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
True	True	k1gFnSc1
Story	story	k1gFnSc1
of	of	k?
Torpedo	Torpedo	k1gNnSc1
Squadron	Squadron	k1gMnSc1
Eight	Eight	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Little	Little	k1gFnSc1
<g/>
,	,	kIx,
Brown	Brown	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
316	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2139	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
225870332	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
PARSHALL	PARSHALL	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
;	;	kIx,
TULLY	TULLY	kA
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shattered	Shattered	k1gMnSc1
Sword	Sword	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Untold	Untold	k1gMnSc1
Story	story	k1gFnSc2
of	of	k?
the	the	k?
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dulles	Dulles	k1gInSc1
<g/>
,	,	kIx,
Virginia	Virginium	k1gNnPc1
<g/>
:	:	kIx,
Potomac	Potomac	k1gFnSc1
Books	Booksa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
57488	#num#	k4
<g/>
-	-	kIx~
<g/>
923	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
PEATTIE	PEATTIE	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
R.	R.	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sunburst	Sunburst	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Rise	Ris	k1gFnSc2
of	of	k?
Japanese	Japanese	k1gFnSc2
Naval	navalit	k5eAaPmRp2nS
Air	Air	k1gMnSc1
Power	Power	k1gMnSc1
<g/>
,	,	kIx,
1909	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
664	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
POTTER	POTTER	kA
<g/>
,	,	kIx,
E.	E.	kA
B.	B.	kA
<g/>
;	;	kIx,
NIMITZ	NIMITZ	kA
<g/>
,	,	kIx,
Chester	Chester	k1gInSc1
W.	W.	kA
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sea	Sea	k1gMnSc1
Power	Power	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Naval	navalit	k5eAaPmRp2nS
History	Histor	k1gMnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Prentice-Hall	Prentice-Hall	k1gInSc1
Englewood	Englewooda	k1gFnPc2
Cliffs	Cliffsa	k1gFnPc2
<g/>
,	,	kIx,
New	New	k1gFnSc2
Jersey	Jersea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
395062	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
PRANGE	PRANGE	kA
<g/>
,	,	kIx,
Gordon	Gordon	k1gMnSc1
W.	W.	kA
<g/>
;	;	kIx,
GOLDSTEIN	GOLDSTEIN	kA
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
DILLON	DILLON	kA
<g/>
,	,	kIx,
Katherine	Katherin	k1gInSc5
V.	V.	kA
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miracle	Miracle	k1gFnSc1
at	at	k?
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
McGraw-Hill	McGraw-Hill	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
50672	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
SCRIVNER	SCRIVNER	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
L.	L.	kA
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TBM	TBM	kA
<g/>
/	/	kIx~
<g/>
TBF	TBF	kA
Avenger	Avenger	k1gInSc1
in	in	k?
Action	Action	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carrollton	Carrollton	k1gInSc1
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
<g/>
:	:	kIx,
Squadron	Squadron	k1gInSc1
<g/>
/	/	kIx~
<g/>
Signal	Signal	k1gFnSc1
Publications	Publicationsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
89747	#num#	k4
<g/>
-	-	kIx~
<g/>
197	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
SHEPHERD	SHEPHERD	kA
<g/>
,	,	kIx,
Joel	Joel	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
:	:	kIx,
June	jun	k1gMnSc5
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
1942	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
SMITH	SMITH	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Emperor	Emperor	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Codes	Codesa	k1gFnPc2
<g/>
:	:	kIx,
Bletchley	Bletchlea	k1gMnSc2
Park	park	k1gInSc1
and	and	k?
the	the	k?
Breaking	Breaking	k1gInSc1
of	of	k?
Japan	japan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Secret	Secret	k1gInSc1
Ciphers	Ciphers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Bantam	bantam	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
593	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4642	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
STEPHEN	STEPHEN	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sea	Sea	k1gMnSc1
Battles	Battles	k1gMnSc1
in	in	k?
Close-up	Close-up	k1gMnSc1
<g/>
:	:	kIx,
World	World	k1gMnSc1
War	War	k1gMnSc1
Two	Two	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Ian	Ian	k1gMnSc1
Allan	Allan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7110	#num#	k4
<g/>
-	-	kIx~
<g/>
1596	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
STILLE	STILLE	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
USN	USN	kA
Carriers	Carriers	k1gInSc1
vs	vs	k?
IJN	IJN	kA
Carriers	Carriers	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Pacific	Pacifice	k1gInPc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Osprey	Osprea	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84603	#num#	k4
<g/>
-	-	kIx~
<g/>
248	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
SYMONDS	SYMONDS	kA
<g/>
,	,	kIx,
Craig	Craig	k1gInSc1
L.	L.	kA
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gMnSc1
War	War	k1gMnSc1
Two	Two	k1gMnSc1
at	at	k?
Sea	Sea	k1gMnSc1
<g/>
:	:	kIx,
A	a	k9
Global	globat	k5eAaImAgMnS
History	Histor	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
1902	#num#	k4
<g/>
-	-	kIx~
<g/>
4367	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
THRUELSEN	THRUELSEN	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Grumman	Grumman	k1gMnSc1
Story	story	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Praeger	Praeger	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
275	#num#	k4
<g/>
-	-	kIx~
<g/>
54260	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
TILLMAN	TILLMAN	kA
<g/>
,	,	kIx,
Barrett	Barrett	k1gMnSc1
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Dauntless	Dauntlessa	k1gFnPc2
Dive-bomber	Dive-bomber	k1gMnSc1
of	of	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
Two	Two	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Press	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87021	#num#	k4
<g/>
-	-	kIx~
<g/>
569	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
TOLL	TOLL	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
W.	W.	kA
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pacific	Pacific	k1gMnSc1
Crucible	Crucible	k1gMnSc1
<g/>
:	:	kIx,
War	War	k1gMnSc1
at	at	k?
Sea	Sea	k1gMnSc1
in	in	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
W.W.	W.W.	k1gFnSc1
Norton	Norton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
393	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6813	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
WILLMOTT	WILLMOTT	kA
<g/>
,	,	kIx,
H.	H.	kA
P.	P.	kA
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Barrier	Barrier	k1gMnSc1
and	and	k?
the	the	k?
Javelin	Javelina	k1gFnPc2
<g/>
:	:	kIx,
Japanese	Japanese	k1gFnSc1
and	and	k?
Allied	Allied	k1gMnSc1
Strategies	Strategies	k1gMnSc1
<g/>
,	,	kIx,
February	Februara	k1gFnPc1
to	ten	k3xDgNnSc1
June	jun	k1gMnSc5
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
949	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
FUČIDA	FUČIDA	kA
<g/>
,	,	kIx,
Micuo	Micuo	k1gMnSc1
<g/>
;	;	kIx,
OKUMIJA	OKUMIJA	kA
<g/>
,	,	kIx,
Masatake	Masatake	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Midway	Midwaa	k1gFnPc1
:	:	kIx,
Osudová	osudový	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
japonského	japonský	k2eAgNnSc2d1
válečného	válečný	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Robert	Robert	k1gMnSc1
Miller	Miller	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
(	(	kIx(
<g/>
v	v	k7c6
Pasece	paseka	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
-	-	kIx~
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
205	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
349	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
překlad	překlad	k1gInSc4
anglického	anglický	k2eAgInSc2d1
překladu	překlad	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
editoval	editovat	k5eAaImAgInS
Roger	Roger	k1gInSc4
Pineau	Pineaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
HEALY	HEALY	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Midway	Midwaa	k1gFnSc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bod	bod	k1gInSc1
zlomu	zlom	k1gInSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
(	(	kIx(
<g/>
původním	původní	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Midway	Midwaa	k1gFnSc2
1942	#num#	k4
<g/>
:	:	kIx,
Turning-point	Turning-point	k1gInSc1
in	in	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osprey	Osprey	k1gInPc4
Publishing	Publishing	k1gInSc1
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Amercom	Amercom	k1gInSc1
SA	SA	kA
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
261	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
376	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HUBÁČEK	Hubáček	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pacifik	Pacifik	k1gInSc1
v	v	k7c6
plamenech	plamen	k1gInPc6
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
436	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
642	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavé	krvavý	k2eAgInPc1d1
oceány	oceán	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
plánu	plán	k1gInSc2
„	„	k?
<g/>
Barbarossa	Barbarossa	k1gMnSc1
<g/>
“	“	k?
k	k	k7c3
bitvě	bitva	k1gFnSc3
u	u	k7c2
Midway	Midwaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
391	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KODET	KODET	kA
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Midway	Midwa	k2eAgFnPc4d1
1942	#num#	k4
<g/>
:	:	kIx,
zlom	zlom	k1gInSc1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
války	válka	k1gFnSc2
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
98	#num#	k4
<g/>
–	–	k?
<g/>
111	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
6097	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SAUNDERS	SAUNDERS	kA
<g/>
,	,	kIx,
Hrowe	Hrowe	k1gInSc1
H.	H.	kA
Duel	duel	k1gInSc1
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Václav	Václav	k1gMnSc1
Pauer	Pauer	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Mustang	mustang	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Military	Militara	k1gFnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
5	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85831	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Bitvy	bitva	k1gFnSc2
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
u	u	k7c2
Midway	Midwaa	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
94	#num#	k4
<g/>
-	-	kIx~
<g/>
101	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SMITH	SMITH	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
C.	C.	kA
Pěst	pěst	k1gFnSc1
z	z	k7c2
nebe	nebe	k1gNnSc2
(	(	kIx(
<g/>
Životopis	životopis	k1gInSc1
námořního	námořní	k2eAgMnSc2d1
kapitána	kapitán	k1gMnSc2
Takešige	Takešig	k1gMnSc2
Egusy	Egusa	k1gFnSc2
<g/>
,	,	kIx,
Císařské	císařský	k2eAgNnSc4d1
japonské	japonský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
Vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
970	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
</s>
<s>
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
:	:	kIx,
June	jun	k1gMnSc5
4	#num#	k4
-	-	kIx~
6	#num#	k4
<g/>
,	,	kIx,
1942	#num#	k4
na	na	k7c4
cv	cv	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
org	org	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Battle	Battle	k1gFnSc1
of	of	k?
Midway	Midwaa	k1gFnSc2
<g/>
:	:	kIx,
4-7	4-7	k4
June	jun	k1gMnSc5
1942	#num#	k4
<g/>
:	:	kIx,
Composition	Composition	k1gInSc1
of	of	k?
U.	U.	kA
S.	S.	kA
Forces	Forces	k1gInSc1
na	na	k7c4
history	histor	k1gMnPc4
<g/>
.	.	kIx.
<g/>
navy	navy	k?
<g/>
.	.	kIx.
<g/>
mil	míle	k1gFnPc2
–	–	k?
Složení	složení	k1gNnSc4
amerických	americký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
A	a	k9
Critical	Critical	k1gMnSc1
Revisit	Revisit	k1gInSc1
to	ten	k3xDgNnSc4
The	The	k1gFnSc7
Battle	Battle	k1gFnSc2
of	of	k?
Midway	Midwaa	k1gFnSc2
–	–	k?
blog	blog	k1gMnSc1
pilota	pilot	k1gMnSc2
střemhlavého	střemhlavý	k2eAgInSc2d1
bombardéru	bombardér	k1gInSc2
George	Georg	k1gMnSc4
Walshe	Walsh	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
vyprávění	vyprávění	k1gNnSc4
o	o	k7c6
vývoji	vývoj	k1gInSc6
legendy	legenda	k1gFnSc2
o	o	k7c4
Midway	Midwaa	k1gFnPc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Malajsii	Malajsie	k1gFnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
•	•	k?
Boje	boj	k1gInPc4
o	o	k7c4
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
Guadalcanal	Guadalcanal	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrova	ostrov	k1gInSc2
Savo	Savo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
východních	východní	k2eAgMnPc2d1
Šalomounů	Šalomoun	k1gMnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
mysu	mys	k1gInSc2
Esperance	Esperance	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrovů	ostrov	k1gInPc2
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Guadalcanalu	Guadalcanal	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tassafarongy	Tassafarong	k1gInPc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Rennellova	Rennellův	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Bismarckově	Bismarckův	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Komandorských	Komandorský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Tarawu	Tarawus	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Operace	operace	k1gFnSc1
Forager	Forager	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Saipan	Saipan	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Peleliu	Pelelium	k1gNnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Leyte	Leyt	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Manilu	Manila	k1gFnSc4
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Iwodžimu	Iwodžimo	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Okinawu	Okinawa	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
117885	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4119941-8	4119941-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85085053	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85085053	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
|	|	kIx~
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Japonsko	Japonsko	k1gNnSc1
</s>
