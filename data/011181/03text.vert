<p>
<s>
Neonatologie	Neonatologie	k1gFnSc1	Neonatologie
je	být	k5eAaImIp3nS	být
podobor	podobor	k1gInSc4	podobor
pediatrie	pediatrie	k1gFnSc2	pediatrie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
fyziologické	fyziologický	k2eAgMnPc4d1	fyziologický
i	i	k8xC	i
patologické	patologický	k2eAgMnPc4d1	patologický
novorozence	novorozenec	k1gMnPc4	novorozenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neonatolog	Neonatolog	k1gMnSc1	Neonatolog
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgInPc2d1	současný
předpisů	předpis	k1gInPc2	předpis
je	být	k5eAaImIp3nS	být
neonatologie	neonatologie	k1gFnPc4	neonatologie
specializační	specializační	k2eAgFnSc1d1	specializační
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jehož	jenž	k3xRgNnSc4	jenž
absolvování	absolvování	k1gNnSc4	absolvování
je	být	k5eAaImIp3nS	být
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
nejméně	málo	k6eAd3	málo
pětiletá	pětiletý	k2eAgFnSc1d1	pětiletá
praxe	praxe	k1gFnSc1	praxe
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
oboru	obor	k1gInSc2	obor
pediatrie	pediatrie	k1gFnSc2	pediatrie
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
rok	rok	k1gInSc4	rok
praxe	praxe	k1gFnSc2	praxe
na	na	k7c6	na
akreditovaném	akreditovaný	k2eAgNnSc6d1	akreditované
neonatologickém	onatologický	k2eNgNnSc6d1	neonatologické
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neonatolog	Neonatolog	k1gMnSc1	Neonatolog
je	být	k5eAaImIp3nS	být
nejvýše	nejvýše	k6eAd1	nejvýše
kvalifikovaným	kvalifikovaný	k2eAgMnSc7d1	kvalifikovaný
odborníkem	odborník	k1gMnSc7	odborník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
novorozence	novorozenec	k1gMnSc4	novorozenec
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
úsecích	úsek	k1gInPc6	úsek
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
novorozence	novorozenec	k1gMnSc4	novorozenec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
neonatologických	onatologický	k2eNgNnPc6d1	neonatologické
pracovištích	pracoviště	k1gNnPc6	pracoviště
III	III	kA	III
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
perinatologických	perinatologický	k2eAgNnPc6d1	perinatologické
centrech	centrum	k1gNnPc6	centrum
III	III	kA	III
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získaná	získaný	k2eAgFnSc1d1	získaná
specializace	specializace	k1gFnSc1	specializace
opravňuje	opravňovat	k5eAaImIp3nS	opravňovat
držitele	držitel	k1gMnPc4	držitel
ucházet	ucházet	k5eAaImF	ucházet
se	se	k3xPyFc4	se
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
vedoucího	vedoucí	k2eAgMnSc2d1	vedoucí
lékaře	lékař	k1gMnSc2	lékař
na	na	k7c6	na
neonatologických	onatologický	k2eNgNnPc6d1	neonatologické
pracovištích	pracoviště	k1gNnPc6	pracoviště
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
a	a	k8xC	a
přednosty	přednosta	k1gMnSc2	přednosta
na	na	k7c6	na
neonatologických	onatologický	k2eNgNnPc6d1	neonatologické
odděleních	oddělení	k1gNnPc6	oddělení
III	III	kA	III
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
získává	získávat	k5eAaImIp3nS	získávat
odbornou	odborný	k2eAgFnSc4d1	odborná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
pregraduálním	pregraduální	k2eAgNnSc6d1	pregraduální
a	a	k8xC	a
postgraduálním	postgraduální	k2eAgNnSc6d1	postgraduální
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
neonatologie	neonatologie	k1gFnSc2	neonatologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novorozenec	novorozenec	k1gMnSc1	novorozenec
===	===	k?	===
</s>
</p>
<p>
<s>
Novorozenecký	novorozenecký	k2eAgInSc1d1	novorozenecký
věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k9	jako
věk	věk	k1gInSc1	věk
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
porodu	porod	k1gInSc3	porod
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
novorozenec	novorozenec	k1gMnSc1	novorozenec
zdravý	zdravý	k2eAgMnSc1d1	zdravý
a	a	k8xC	a
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
40	[number]	k4	40
<g/>
.	.	kIx.	.
týden	týden	k1gInSc4	týden
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
vykazovat	vykazovat	k5eAaImF	vykazovat
následující	následující	k2eAgFnPc4d1	následující
známky	známka	k1gFnPc4	známka
donošenosti	donošenost	k1gFnSc2	donošenost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
výška	výška	k1gFnSc1	výška
50	[number]	k4	50
cm	cm	kA	cm
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
2500	[number]	k4	2500
-	-	kIx~	-
4800	[number]	k4	4800
g	g	kA	g
</s>
</p>
<p>
<s>
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
podkožní	podkožní	k2eAgInSc1d1	podkožní
tuk	tuk	k1gInSc1	tuk
</s>
</p>
<p>
<s>
kůže	kůže	k1gFnSc1	kůže
je	být	k5eAaImIp3nS	být
růžová	růžový	k2eAgFnSc1d1	růžová
a	a	k8xC	a
krytá	krytý	k2eAgFnSc1d1	krytá
mázkem	mázek	k1gInSc7	mázek
</s>
</p>
<p>
<s>
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
jsou	být	k5eAaImIp3nP	být
jemné	jemný	k2eAgInPc4d1	jemný
vlasy	vlas	k1gInPc4	vlas
</s>
</p>
<p>
<s>
nehty	nehet	k1gInPc1	nehet
překrývají	překrývat	k5eAaImIp3nP	překrývat
konce	konec	k1gInPc4	konec
prstů	prst	k1gInPc2	prst
</s>
</p>
<p>
<s>
plosky	ploska	k1gFnPc1	ploska
nohou	noha	k1gFnSc7	noha
jsou	být	k5eAaImIp3nP	být
rýhované	rýhovaný	k2eAgInPc1d1	rýhovaný
</s>
</p>
<p>
<s>
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
</s>
</p>
<p>
<s>
genitál	genitál	k1gInSc1	genitál
je	být	k5eAaImIp3nS	být
zralý	zralý	k2eAgInSc1d1	zralý
</s>
</p>
<p>
<s>
==	==	k?	==
Neonatologická	Neonatologický	k2eAgNnPc1d1	Neonatologické
pracoviště	pracoviště	k1gNnPc1	pracoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Neonatologická	Neonatologický	k2eAgNnPc1d1	Neonatologické
pracoviště	pracoviště	k1gNnPc1	pracoviště
jsou	být	k5eAaImIp3nP	být
hierarchicky	hierarchicky	k6eAd1	hierarchicky
organizována	organizovat	k5eAaBmNgNnP	organizovat
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
stupňů	stupeň	k1gInPc2	stupeň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pracoviště	pracoviště	k1gNnSc1	pracoviště
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
-	-	kIx~	-
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
fyziologického	fyziologický	k2eAgMnSc4d1	fyziologický
novorozence	novorozenec	k1gMnSc4	novorozenec
</s>
</p>
<p>
<s>
pracoviště	pracoviště	k1gNnSc1	pracoviště
II	II	kA	II
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
-	-	kIx~	-
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
péči	péče	k1gFnSc4	péče
i	i	k9	i
o	o	k7c4	o
novorozence	novorozenec	k1gMnPc4	novorozenec
se	s	k7c7	s
středně	středně	k6eAd1	středně
těžkou	těžký	k2eAgFnSc7d1	těžká
adaptací	adaptace	k1gFnSc7	adaptace
</s>
</p>
<p>
<s>
pracoviště	pracoviště	k1gNnSc1	pracoviště
III	III	kA	III
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInPc1	stupeň
(	(	kIx(	(
<g/>
perinatologická	perinatologický	k2eAgNnPc1d1	perinatologické
centra	centrum	k1gNnPc1	centrum
<g/>
)	)	kIx)	)
-	-	kIx~	-
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
péči	péče	k1gFnSc4	péče
i	i	k9	i
pro	pro	k7c4	pro
novorozence	novorozenec	k1gMnSc4	novorozenec
s	s	k7c7	s
těžkou	těžký	k2eAgFnSc7d1	těžká
adaptací	adaptace	k1gFnSc7	adaptace
</s>
</p>
<p>
<s>
===	===	k?	===
Pracoviště	pracoviště	k1gNnSc2	pracoviště
I.	I.	kA	I.
<g/>
stupně	stupeň	k1gInPc1	stupeň
===	===	k?	===
</s>
</p>
<p>
<s>
Pracoviště	pracoviště	k1gNnSc1	pracoviště
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
regionální	regionální	k2eAgNnPc1d1	regionální
pracoviště	pracoviště	k1gNnPc1	pracoviště
<g/>
,	,	kIx,	,
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
poporodní	poporodní	k2eAgFnSc4d1	poporodní
péči	péče	k1gFnSc4	péče
o	o	k7c6	o
zdravé	zdravá	k1gFnSc6	zdravá
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
lehce	lehko	k6eAd1	lehko
komplikované	komplikovaný	k2eAgMnPc4d1	komplikovaný
novorozence	novorozenec	k1gMnPc4	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
porodnic	porodnice	k1gFnPc2	porodnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
115	[number]	k4	115
novorozeneckých	novorozenecký	k2eAgNnPc2d1	novorozenecké
oddělení	oddělení	k1gNnPc2	oddělení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pracoviště	pracoviště	k1gNnPc1	pracoviště
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
stupně	stupeň	k1gInSc2	stupeň
===	===	k?	===
</s>
</p>
<p>
<s>
Pracoviště	pracoviště	k1gNnSc1	pracoviště
II	II	kA	II
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
intermediární	intermediární	k2eAgNnPc1d1	intermediární
pracoviště	pracoviště	k1gNnPc1	pracoviště
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
spádovými	spádový	k2eAgNnPc7d1	spádové
pracovišti	pracoviště	k1gNnPc7	pracoviště
pro	pro	k7c4	pro
pracoviště	pracoviště	k1gNnSc4	pracoviště
I.	I.	kA	I.
stupně	stupeň	k1gInPc1	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
novorozence	novorozenec	k1gMnPc4	novorozenec
se	s	k7c7	s
středně	středně	k6eAd1	středně
těžkou	těžký	k2eAgFnSc7d1	těžká
adaptací	adaptace	k1gFnSc7	adaptace
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
o	o	k7c4	o
předčasně	předčasně	k6eAd1	předčasně
narozené	narozený	k2eAgFnPc4d1	narozená
děti	dítě	k1gFnPc4	dítě
narozené	narozený	k2eAgFnPc4d1	narozená
po	po	k7c6	po
32	[number]	k4	32
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
zajistit	zajistit	k5eAaPmF	zajistit
krátkodobou	krátkodobý	k2eAgFnSc4d1	krátkodobá
podporu	podpora	k1gFnSc4	podpora
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
však	však	k9	však
vybavení	vybavení	k1gNnSc4	vybavení
pro	pro	k7c4	pro
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Takovýchto	takovýto	k3xDgNnPc2	takovýto
pracovišť	pracoviště	k1gNnPc2	pracoviště
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
ČR	ČR	kA	ČR
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pracoviště	pracoviště	k1gNnPc1	pracoviště
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
stupně	stupeň	k1gInSc2	stupeň
===	===	k?	===
</s>
</p>
<p>
<s>
Pracoviště	pracoviště	k1gNnSc1	pracoviště
III	III	kA	III
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
perinatologická	perinatologický	k2eAgNnPc1d1	perinatologické
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
péči	péče	k1gFnSc4	péče
novorozencům	novorozenec	k1gMnPc3	novorozenec
s	s	k7c7	s
těžkou	těžký	k2eAgFnSc7d1	těžká
adaptací	adaptace	k1gFnSc7	adaptace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nejzávažnějších	závažný	k2eAgInPc2d3	nejzávažnější
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pracoviště	pracoviště	k1gNnPc1	pracoviště
mohou	moct	k5eAaImIp3nP	moct
zajistit	zajistit	k5eAaPmF	zajistit
i	i	k9	i
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
péči	péče	k1gFnSc4	péče
včetně	včetně	k7c2	včetně
umělé	umělý	k2eAgFnSc2d1	umělá
plicní	plicní	k2eAgFnSc2d1	plicní
ventilace	ventilace	k1gFnSc2	ventilace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
pracovištích	pracoviště	k1gNnPc6	pracoviště
je	být	k5eAaImIp3nS	být
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
zajistit	zajistit	k5eAaPmF	zajistit
mimotělní	mimotělní	k2eAgNnSc4d1	mimotělní
okysličování	okysličování	k1gNnSc4	okysličování
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Takovýchto	takovýto	k3xDgNnPc2	takovýto
pracovišť	pracoviště	k1gNnPc2	pracoviště
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
ČR	ČR	kA	ČR
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
výhodný	výhodný	k2eAgMnSc1d1	výhodný
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
hrozícím	hrozící	k2eAgInSc6d1	hrozící
předčasném	předčasný	k2eAgInSc6d1	předčasný
porodu	porod	k1gInSc6	porod
převoz	převoz	k1gInSc1	převoz
matky	matka	k1gFnSc2	matka
na	na	k7c4	na
pracoviště	pracoviště	k1gNnSc4	pracoviště
II	II	kA	II
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
III	III	kA	III
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInPc1	stupeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
novorozenci	novorozenec	k1gMnPc7	novorozenec
dostane	dostat	k5eAaPmIp3nS	dostat
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
adekvátní	adekvátní	k2eAgFnSc2d1	adekvátní
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
tzv.	tzv.	kA	tzv.
transport	transport	k1gInSc4	transport
in	in	k?	in
utero	utero	k6eAd1	utero
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
v	v	k7c6	v
děloze	děloha	k1gFnSc6	děloha
<g/>
)	)	kIx)	)
podstatně	podstatně	k6eAd1	podstatně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
naději	naděje	k1gFnSc4	naděje
novorozence	novorozenec	k1gMnSc2	novorozenec
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
přežití	přežití	k1gNnSc6	přežití
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
minimalizaci	minimalizace	k1gFnSc4	minimalizace
následků	následek	k1gInPc2	následek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neonatologie	Neonatologie	k1gFnSc1	Neonatologie
bývá	bývat	k5eAaImIp3nS	bývat
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přežití	přežití	k1gNnSc4	přežití
i	i	k9	i
těžce	těžce	k6eAd1	těžce
postiženým	postižený	k2eAgFnPc3d1	postižená
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
zátěž	zátěž	k1gFnSc1	zátěž
pro	pro	k7c4	pro
systém	systém	k1gInSc4	systém
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
péče	péče	k1gFnSc1	péče
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
(	(	kIx(	(
<g/>
ústní	ústní	k2eAgNnSc1d1	ústní
sdělení	sdělení	k1gNnSc1	sdělení
<g/>
)	)	kIx)	)
však	však	k9	však
počet	počet	k1gInSc1	počet
zdravotně	zdravotně	k6eAd1	zdravotně
postižených	postižený	k2eAgFnPc2d1	postižená
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
špatné	špatný	k2eAgFnSc2d1	špatná
poporodní	poporodní	k2eAgFnSc2d1	poporodní
adaptace	adaptace	k1gFnSc2	adaptace
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejný	stejný	k2eAgInSc1d1	stejný
-	-	kIx~	-
neonatologie	neonatologie	k1gFnSc1	neonatologie
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přežít	přežít	k5eAaPmF	přežít
i	i	k9	i
těžce	těžce	k6eAd1	těžce
postiženým	postižený	k1gMnPc3	postižený
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
bez	bez	k7c2	bez
péče	péče	k1gFnSc2	péče
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
díky	díky	k7c3	díky
péči	péče	k1gFnSc3	péče
neonatologů	neonatolog	k1gMnPc2	neonatolog
mnohé	mnohý	k2eAgFnPc1d1	mnohá
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
skončily	skončit	k5eAaPmAgFnP	skončit
se	se	k3xPyFc4	se
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
postižením	postižení	k1gNnSc7	postižení
<g/>
,	,	kIx,	,
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
života	život	k1gInSc2	život
s	s	k7c7	s
minimálním	minimální	k2eAgNnSc7d1	minimální
nebo	nebo	k8xC	nebo
s	s	k7c7	s
žádným	žádný	k3yNgNnSc7	žádný
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Lebl	Lebl	k1gMnSc1	Lebl
J.	J.	kA	J.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Preklinická	Preklinický	k2eAgFnSc1d1	Preklinická
pediatrie	pediatrie	k1gFnSc1	pediatrie
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Galén	Galén	k1gInSc1	Galén
<g/>
/	/	kIx~	/
<g/>
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Institut	institut	k1gInSc1	institut
postgraduálního	postgraduální	k2eAgNnSc2d1	postgraduální
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
pracovníků	pracovník	k1gMnPc2	pracovník
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
-	-	kIx~	-
mj.	mj.	kA	mj.
i	i	k8xC	i
seznam	seznam	k1gInSc4	seznam
oborů	obor	k1gInPc2	obor
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
neonatologická	onatologický	k2eNgFnSc1d1	neonatologická
společnost	společnost	k1gFnSc1	společnost
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Gynekologie	gynekologie	k1gFnSc1	gynekologie
a	a	k8xC	a
porodnictví	porodnictví	k1gNnSc1	porodnictví
</s>
</p>
<p>
<s>
Inkubátor	inkubátor	k1gInSc1	inkubátor
</s>
</p>
<p>
<s>
Porod	porod	k1gInSc1	porod
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
neonatologie	neonatologie	k1gFnSc2	neonatologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
