<s>
Neonatologie	Neonatologie	k1gFnSc1	Neonatologie
je	být	k5eAaImIp3nS	být
podobor	podobor	k1gInSc4	podobor
pediatrie	pediatrie	k1gFnSc2	pediatrie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
fyziologické	fyziologický	k2eAgMnPc4d1	fyziologický
i	i	k8xC	i
patologické	patologický	k2eAgMnPc4d1	patologický
novorozence	novorozenec	k1gMnPc4	novorozenec
<g/>
.	.	kIx.	.
</s>
