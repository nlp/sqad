<s>
Emigrace	emigrace	k1gFnSc1	emigrace
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
ex-migrare	exigrar	k1gMnSc5	ex-migrar
<g/>
,	,	kIx,	,
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
opuštění	opuštění	k1gNnSc1	opuštění
(	(	kIx(	(
<g/>
či	či	k8xC	či
útěk	útěk	k1gInSc4	útěk
ze	z	k7c2	z
<g/>
)	)	kIx)	)
země	zem	k1gFnSc2	zem
původu	původ	k1gInSc2	původ
a	a	k8xC	a
přestěhování	přestěhování	k1gNnSc2	přestěhování
se	se	k3xPyFc4	se
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
imigrace	imigrace	k1gFnSc1	imigrace
znamená	znamenat	k5eAaImIp3nS	znamenat
přistěhování	přistěhování	k1gNnSc4	přistěhování
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
totéž	týž	k3xTgNnSc1	týž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
cílové	cílový	k2eAgFnSc2d1	cílová
země	zem	k1gFnSc2	zem
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedobrovolný	dobrovolný	k2eNgInSc4d1	nedobrovolný
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
slovo	slovo	k1gNnSc4	slovo
exil	exil	k1gInSc1	exil
a	a	k8xC	a
exulant	exulant	k1gMnSc1	exulant
či	či	k8xC	či
vyhnanec	vyhnanec	k1gMnSc1	vyhnanec
<g/>
.	.	kIx.	.
</s>
<s>
Migrace	migrace	k1gFnSc1	migrace
je	být	k5eAaImIp3nS	být
stěhování	stěhování	k1gNnSc1	stěhování
skupin	skupina	k1gFnPc2	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
emigrace	emigrace	k1gFnSc2	emigrace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
a	a	k8xC	a
sociální	sociální	k2eAgInPc4d1	sociální
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
politické	politický	k2eAgNnSc4d1	politické
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgNnSc4d1	náboženské
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgNnSc4d1	jiné
pronásledování	pronásledování	k1gNnSc4	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
Uprchlíky	uprchlík	k1gMnPc7	uprchlík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žádají	žádat	k5eAaImIp3nP	žádat
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
či	či	k8xC	či
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
zemi	zem	k1gFnSc6	zem
hrozí	hrozit	k5eAaImIp3nS	hrozit
pronásledování	pronásledování	k1gNnSc1	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
obava	obava	k1gFnSc1	obava
odůvodněná	odůvodněný	k2eAgFnSc1d1	odůvodněná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
účastnické	účastnický	k2eAgFnPc1d1	účastnická
země	zem	k1gFnPc1	zem
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
právním	právní	k2eAgNnSc6d1	právní
postavení	postavení	k1gNnSc6	postavení
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
r.	r.	kA	r.
1951	[number]	k4	1951
povinny	povinen	k2eAgInPc1d1	povinen
jim	on	k3xPp3gMnPc3	on
přiznat	přiznat	k5eAaPmF	přiznat
status	status	k1gInSc4	status
azylanta	azylant	k1gMnSc2	azylant
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
ČR	ČR	kA	ČR
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
upravuje	upravovat	k5eAaImIp3nS	upravovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
325	[number]	k4	325
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
azylu	azyl	k1gInSc6	azyl
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomickými	ekonomický	k2eAgMnPc7d1	ekonomický
a	a	k8xC	a
sociálními	sociální	k2eAgMnPc7d1	sociální
migranty	migrant	k1gMnPc7	migrant
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
emigrují	emigrovat	k5eAaBmIp3nP	emigrovat
z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
pobytu	pobyt	k1gInSc6	pobyt
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
nesplňují	splňovat	k5eNaImIp3nP	splňovat
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
přiznání	přiznání	k1gNnSc4	přiznání
azylu	azyl	k1gInSc2	azyl
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jim	on	k3xPp3gMnPc3	on
stát	stát	k5eAaPmF	stát
udělit	udělit	k5eAaPmF	udělit
doplňkovou	doplňkový	k2eAgFnSc4d1	doplňková
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
emigrace	emigrace	k1gFnSc2	emigrace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
v	v	k7c6	v
ekologii	ekologie	k1gFnSc6	ekologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
označuje	označovat	k5eAaImIp3nS	označovat
přesun	přesun	k1gInSc1	přesun
jedince	jedinec	k1gMnSc2	jedinec
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnSc2	skupina
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
z	z	k7c2	z
příslušné	příslušný	k2eAgFnSc2d1	příslušná
populace	populace	k1gFnSc2	populace
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
včela	včela	k1gFnSc1	včela
medonosná	medonosný	k2eAgFnSc1d1	medonosná
při	při	k7c6	při
rojení	rojení	k1gNnSc6	rojení
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
králíci	králík	k1gMnPc1	králík
odcházející	odcházející	k2eAgMnPc1d1	odcházející
založit	založit	k5eAaPmF	založit
novou	nový	k2eAgFnSc4d1	nová
kolonii	kolonie	k1gFnSc4	kolonie
apod.	apod.	kA	apod.
Rovnice	rovnice	k1gFnSc1	rovnice
populačního	populační	k2eAgInSc2d1	populační
růstu	růst	k1gInSc2	růst
užívaná	užívaný	k2eAgFnSc1d1	užívaná
v	v	k7c6	v
ekologii	ekologie	k1gFnSc6	ekologie
nebo	nebo	k8xC	nebo
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
demografii	demografie	k1gFnSc6	demografie
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dá	dát	k5eAaPmIp3nS	dát
psát	psát	k5eAaImF	psát
jako	jako	k9	jako
Nt	Nt	k1gFnSc7	Nt
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
=	=	kIx~	=
Nt	Nt	k1gMnSc1	Nt
+	+	kIx~	+
B	B	kA	B
−	−	k?	−
D	D	kA	D
+	+	kIx~	+
I	I	kA	I
−	−	k?	−
E	E	kA	E
kde	kde	k6eAd1	kde
Nt	Nt	k1gFnSc1	Nt
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc2	množství
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
v	v	k7c6	v
čase	čas	k1gInSc6	čas
t	t	k?	t
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
Nt	Nt	k1gFnPc2	Nt
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
v	v	k7c6	v
čase	čas	k1gInSc6	čas
t	t	k?	t
B	B	kA	B
je	být	k5eAaImIp3nS	být
porodnost	porodnost	k1gFnSc1	porodnost
(	(	kIx(	(
<g/>
natalita	natalita	k1gFnSc1	natalita
<g/>
)	)	kIx)	)
D	D	kA	D
je	být	k5eAaImIp3nS	být
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
(	(	kIx(	(
<g/>
mortalita	mortalita	k1gFnSc1	mortalita
<g/>
)	)	kIx)	)
I	i	k9	i
je	být	k5eAaImIp3nS	být
imigrace	imigrace	k1gFnSc1	imigrace
E	E	kA	E
je	být	k5eAaImIp3nS	být
emigrace	emigrace	k1gFnPc4	emigrace
Emigrace	emigrace	k1gFnSc2	emigrace
v	v	k7c6	v
uvedené	uvedený	k2eAgFnSc6d1	uvedená
rovnici	rovnice	k1gFnSc6	rovnice
označuje	označovat	k5eAaImIp3nS	označovat
počet	počet	k1gInSc1	počet
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
opustí	opustit	k5eAaPmIp3nP	opustit
určité	určitý	k2eAgNnSc4d1	určité
území	území	k1gNnSc4	území
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
<g/>
,	,	kIx,	,
územně	územně	k6eAd1	územně
vymezenou	vymezený	k2eAgFnSc4d1	vymezená
populaci	populace	k1gFnSc4	populace
<g/>
)	)	kIx)	)
za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
míra	míra	k1gFnSc1	míra
emigrace	emigrace	k1gFnSc2	emigrace
přepočítává	přepočítávat	k5eAaImIp3nS	přepočítávat
na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
nebo	nebo	k8xC	nebo
100	[number]	k4	100
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
tedy	tedy	k8xC	tedy
míra	míra	k1gFnSc1	míra
emigrace	emigrace	k1gFnSc2	emigrace
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
státě	stát	k1gInSc6	stát
např.	např.	kA	např.
4	[number]	k4	4
promile	promile	k1gNnPc2	promile
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
každých	každý	k3xTgMnPc2	každý
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
roce	rok	k1gInSc6	rok
4	[number]	k4	4
opustili	opustit	k5eAaPmAgMnP	opustit
daný	daný	k2eAgInSc4d1	daný
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
tento	tento	k3xDgInSc4	tento
stát	stát	k1gInSc4	stát
např.	např.	kA	např.
10	[number]	k4	10
000	[number]	k4	000
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
emigrujících	emigrující	k2eAgMnPc2d1	emigrující
obyvatel	obyvatel	k1gMnPc2	obyvatel
rovno	roven	k2eAgNnSc4d1	rovno
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgInSc4d1	veliký
kulturní	kulturní	k2eAgInSc4d1	kulturní
i	i	k8xC	i
politický	politický	k2eAgInSc4d1	politický
význam	význam	k1gInSc4	význam
měly	mít	k5eAaImAgFnP	mít
už	už	k6eAd1	už
historické	historický	k2eAgFnPc1d1	historická
emigrační	emigrační	k2eAgFnPc1d1	emigrační
vlny	vlna	k1gFnPc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlediska	hledisko	k1gNnSc2	hledisko
spíše	spíše	k9	spíše
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
emigrací	emigrace	k1gFnSc7	emigrace
byla	být	k5eAaImAgFnS	být
kolonizace	kolonizace	k1gFnSc1	kolonizace
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
vlny	vlna	k1gFnPc1	vlna
emigrace	emigrace	k1gFnSc2	emigrace
z	z	k7c2	z
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
přinesly	přinést	k5eAaPmAgFnP	přinést
občanské	občanský	k2eAgFnPc1d1	občanská
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc1d1	náboženská
války	válka	k1gFnPc1	válka
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
např.	např.	kA	např.
židé	žid	k1gMnPc1	žid
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
čeští	český	k2eAgMnPc1d1	český
nekatolíci	nekatolík	k1gMnPc1	nekatolík
<g/>
,	,	kIx,	,
hugenoti	hugenot	k1gMnPc1	hugenot
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1685	[number]	k4	1685
a	a	k8xC	a
šlechta	šlechta	k1gFnSc1	šlechta
za	za	k7c4	za
Revoluce	revoluce	k1gFnPc4	revoluce
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
náboženští	náboženský	k2eAgMnPc1d1	náboženský
emigranti	emigrant	k1gMnPc1	emigrant
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
dobou	doba	k1gFnSc7	doba
masové	masový	k2eAgFnSc2d1	masová
emigrace	emigrace	k1gFnSc2	emigrace
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hromadný	hromadný	k2eAgInSc1d1	hromadný
politický	politický	k2eAgInSc1d1	politický
exil	exil	k1gInSc1	exil
následoval	následovat	k5eAaImAgInS	následovat
po	po	k7c6	po
Ruské	ruský	k2eAgFnSc6d1	ruská
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918-1923	[number]	k4	1918-1923
i	i	k8xC	i
po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
nacisty	nacista	k1gMnSc2	nacista
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
emigrace	emigrace	k1gFnSc1	emigrace
Židů	Žid	k1gMnPc2	Žid
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
za	za	k7c2	za
války	válka	k1gFnSc2	válka
měla	mít	k5eAaImAgFnS	mít
významné	významný	k2eAgInPc4d1	významný
důsledky	důsledek	k1gInPc4	důsledek
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
kulturní	kulturní	k2eAgInSc4d1	kulturní
a	a	k8xC	a
vědecký	vědecký	k2eAgInSc4d1	vědecký
vzestup	vzestup	k1gInSc4	vzestup
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
Druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
muselo	muset	k5eAaImAgNnS	muset
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
východních	východní	k2eAgFnPc2d1	východní
částí	část	k1gFnPc2	část
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
území	území	k1gNnPc2	území
12	[number]	k4	12
až	až	k6eAd1	až
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
Poláků	polák	k1gInPc2	polák
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
milionů	milion	k4xCgInPc2	milion
Židů	Žid	k1gMnPc2	Žid
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
do	do	k7c2	do
státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odešlo	odejít	k5eAaPmAgNnS	odejít
asi	asi	k9	asi
650	[number]	k4	650
tisíc	tisíc	k4xCgInPc2	tisíc
Arabů	Arab	k1gMnPc2	Arab
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
Židů	Žid	k1gMnPc2	Žid
byl	být	k5eAaImAgInS	být
vypuzen	vypuzen	k2eAgInSc1d1	vypuzen
z	z	k7c2	z
okolních	okolní	k2eAgInPc2d1	okolní
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
muselo	muset	k5eAaImAgNnS	muset
přes	přes	k7c4	přes
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
muslimů	muslim	k1gMnPc2	muslim
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Pakistánu	Pakistán	k1gInSc2	Pakistán
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
Indů	Ind	k1gMnPc2	Ind
a	a	k8xC	a
Sikhů	sikh	k1gMnPc2	sikh
naopak	naopak	k6eAd1	naopak
z	z	k7c2	z
Pakistánu	Pakistán	k1gInSc2	Pakistán
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
požádalo	požádat	k5eAaPmAgNnS	požádat
o	o	k7c4	o
asyl	asyl	k1gInSc4	asyl
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
672	[number]	k4	672
tisíc	tisíc	k4xCgInSc1	tisíc
migrantů	migrant	k1gMnPc2	migrant
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
počty	počet	k1gInPc4	počet
řadu	řad	k1gInSc2	řad
let	léto	k1gNnPc2	léto
klesaly	klesat	k5eAaImAgFnP	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počet	počet	k1gInSc1	počet
žadatelů	žadatel	k1gMnPc2	žadatel
626	[number]	k4	626
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
Afganistánu	Afganistán	k1gInSc2	Afganistán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
ze	z	k7c2	z
západního	západní	k2eAgInSc2d1	západní
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
70	[number]	k4	70
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
požádalo	požádat	k5eAaPmAgNnS	požádat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
45	[number]	k4	45
%	%	kIx~	%
žádostí	žádost	k1gFnSc7	žádost
bylo	být	k5eAaImAgNnS	být
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
země	zem	k1gFnSc2	zem
původu	původ	k1gInSc2	původ
získalo	získat	k5eAaPmAgNnS	získat
asyl	asyl	k1gInSc4	asyl
nejvíce	hodně	k6eAd3	hodně
uprchlíků	uprchlík	k1gMnPc2	uprchlík
ze	z	k7c2	z
Syrie	Syrie	k1gFnSc2	Syrie
<g/>
,	,	kIx,	,
Eritreje	Eritrea	k1gFnSc2	Eritrea
<g/>
,	,	kIx,	,
Afganistánu	Afganistán	k1gInSc2	Afganistán
a	a	k8xC	a
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
vlny	vlna	k1gFnPc1	vlna
emigrace	emigrace	k1gFnSc2	emigrace
následovaly	následovat	k5eAaImAgFnP	následovat
po	po	k7c6	po
prudkých	prudký	k2eAgFnPc6d1	prudká
politických	politický	k2eAgFnPc6d1	politická
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc6d1	náboženská
změnách	změna	k1gFnPc6	změna
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
husitské	husitský	k2eAgFnSc6d1	husitská
době	doba	k1gFnSc6	doba
a	a	k8xC	a
zejména	zejména	k9	zejména
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
a	a	k8xC	a
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
Obnoveného	obnovený	k2eAgNnSc2d1	obnovené
zřízení	zřízení	k1gNnSc2	zřízení
zemského	zemský	k2eAgInSc2d1	zemský
roku	rok	k1gInSc2	rok
1627	[number]	k4	1627
-	-	kIx~	-
tehdy	tehdy	k6eAd1	tehdy
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
odešly	odejít	k5eAaPmAgFnP	odejít
desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
typ	typ	k1gInSc1	typ
poměrně	poměrně	k6eAd1	poměrně
masové	masový	k2eAgFnSc2d1	masová
emigrace	emigrace	k1gFnSc2	emigrace
z	z	k7c2	z
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
důvodů	důvod	k1gInPc2	důvod
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
demografického	demografický	k2eAgInSc2d1	demografický
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc2	nedostatek
půdy	půda	k1gFnSc2	půda
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
a	a	k8xC	a
slovenští	slovenský	k2eAgMnPc1d1	slovenský
emigranti	emigrant	k1gMnPc1	emigrant
odcházeli	odcházet	k5eAaImAgMnP	odcházet
zejména	zejména	k9	zejména
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
historii	historie	k1gFnSc3	historie
bohaté	bohatý	k2eAgFnSc3d1	bohatá
na	na	k7c6	na
různé	různý	k2eAgFnSc6d1	různá
okupace	okupace	k1gFnSc1	okupace
opustilo	opustit	k5eAaPmAgNnS	opustit
Československo	Československo	k1gNnSc1	Československo
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
několika	několik	k4yIc6	několik
emigračních	emigrační	k2eAgFnPc6d1	emigrační
vlnách	vlna	k1gFnPc6	vlna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
nacistickou	nacistický	k2eAgFnSc7d1	nacistická
okupací	okupace	k1gFnSc7	okupace
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odcházeli	odcházet	k5eAaImAgMnP	odcházet
jednak	jednak	k8xC	jednak
lidé	člověk	k1gMnPc1	člověk
politicky	politicky	k6eAd1	politicky
angažovaní	angažovaný	k2eAgMnPc1d1	angažovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vyhubení	vyhubení	k1gNnSc1	vyhubení
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
(	(	kIx(	(
<g/>
holokaust	holokaust	k1gInSc1	holokaust
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
exulanti	exulant	k1gMnPc1	exulant
bojovali	bojovat	k5eAaImAgMnP	bojovat
v	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
armádách	armáda	k1gFnPc6	armáda
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bojovali	bojovat	k5eAaImAgMnP	bojovat
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
armádách	armáda	k1gFnPc6	armáda
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
zpravidla	zpravidla	k6eAd1	zpravidla
diskriminováni	diskriminovat	k5eAaBmNgMnP	diskriminovat
a	a	k8xC	a
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
vlnu	vlna	k1gFnSc4	vlna
politické	politický	k2eAgFnSc2d1	politická
emigrace	emigrace	k1gFnSc2	emigrace
tvořili	tvořit	k5eAaImAgMnP	tvořit
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
něm.	něm.	k?	něm.
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
lidí	člověk	k1gMnPc2	člověk
považovala	považovat	k5eAaImAgFnS	považovat
svůj	svůj	k3xOyFgInSc4	svůj
exil	exil	k1gInSc4	exil
za	za	k7c4	za
dočasný	dočasný	k2eAgInSc4d1	dočasný
<g/>
,	,	kIx,	,
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
režim	režim	k1gInSc1	režim
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
Železnou	železný	k2eAgFnSc4d1	železná
oponu	opona	k1gFnSc4	opona
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
emigraci	emigrace	k1gFnSc4	emigrace
podstatně	podstatně	k6eAd1	podstatně
ztížilo	ztížit	k5eAaPmAgNnS	ztížit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vlně	vlna	k1gFnSc6	vlna
odešlo	odejít	k5eAaPmAgNnS	odejít
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
přibližně	přibližně	k6eAd1	přibližně
čtyřicet	čtyřicet	k4xCc4	čtyřicet
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
emigraci	emigrace	k1gFnSc3	emigrace
se	se	k3xPyFc4	se
také	také	k9	také
říkalo	říkat	k5eAaImAgNnS	říkat
osmačtyřicátníci	osmačtyřicátník	k1gMnPc5	osmačtyřicátník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
emigrační	emigrační	k2eAgFnSc1d1	emigrační
vlna	vlna	k1gFnSc1	vlna
přišla	přijít	k5eAaPmAgFnS	přijít
po	po	k7c6	po
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc3	srpen
1968	[number]	k4	1968
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Invaze	invaze	k1gFnSc2	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
vlně	vlna	k1gFnSc3	vlna
emigrace	emigrace	k1gFnSc2	emigrace
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
osmašedesátníci	osmašedesátník	k1gMnPc1	osmašedesátník
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
byly	být	k5eAaImAgFnP	být
hranice	hranice	k1gFnPc1	hranice
poměrně	poměrně	k6eAd1	poměrně
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
emigraci	emigrace	k1gFnSc4	emigrace
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
<g/>
.	.	kIx.	.
</s>
<s>
Odhadem	odhad	k1gInSc7	odhad
odešlo	odejít	k5eAaPmAgNnS	odejít
z	z	k7c2	z
ČSSR	ČSSR	kA	ČSSR
v	v	k7c6	v
letech	let	k1gInPc6	let
1968-1969	[number]	k4	1968-1969
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
číslem	číslo	k1gNnSc7	číslo
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
jen	jen	k9	jen
maďarská	maďarský	k2eAgFnSc1d1	maďarská
emigrace	emigrace	k1gFnSc1	emigrace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odešlo	odejít	k5eAaPmAgNnS	odejít
300	[number]	k4	300
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
téměř	téměř	k6eAd1	téměř
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1969	[number]	k4	1969
až	až	k9	až
1989	[number]	k4	1989
odešlo	odejít	k5eAaPmAgNnS	odejít
z	z	k7c2	z
ČSSR	ČSSR	kA	ČSSR
dalších	další	k2eAgFnPc2d1	další
140	[number]	k4	140
až	až	k9	až
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
24	[number]	k4	24
%	%	kIx~	%
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
41	[number]	k4	41
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
16-30	[number]	k4	16-30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
UNHCR	UNHCR	kA	UNHCR
uvádí	uvádět	k5eAaImIp3nS	uvádět
až	až	k9	až
250	[number]	k4	250
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
socialistickém	socialistický	k2eAgNnSc6d1	socialistické
Československu	Československo	k1gNnSc6	Československo
emigrace	emigrace	k1gFnSc2	emigrace
trestná	trestný	k2eAgFnSc1d1	trestná
<g/>
,	,	kIx,	,
majetky	majetek	k1gInPc4	majetek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
tu	ten	k3xDgFnSc4	ten
emigranti	emigrant	k1gMnPc1	emigrant
zanechali	zanechat	k5eAaPmAgMnP	zanechat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
konfiskovány	konfiskován	k2eAgInPc1d1	konfiskován
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc4	jejich
příbuzní	příbuzný	k1gMnPc1	příbuzný
měli	mít	k5eAaImAgMnP	mít
záznam	záznam	k1gInSc4	záznam
v	v	k7c6	v
kádrových	kádrový	k2eAgInPc6d1	kádrový
posudcích	posudek	k1gInPc6	posudek
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
emigranty	emigrant	k1gMnPc7	emigrant
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
trestáni	trestat	k5eAaImNgMnP	trestat
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
směrnice	směrnice	k1gFnSc1	směrnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
emigrantům	emigrant	k1gMnPc3	emigrant
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
upravit	upravit	k5eAaPmF	upravit
si	se	k3xPyFc3	se
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
legalizaci	legalizace	k1gFnSc4	legalizace
právního	právní	k2eAgInSc2d1	právní
vztahu	vztah	k1gInSc2	vztah
emigranta	emigrant	k1gMnSc2	emigrant
a	a	k8xC	a
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
buď	buď	k8xC	buď
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
zůstat	zůstat	k5eAaPmF	zůstat
československým	československý	k2eAgMnSc7d1	československý
občanem	občan	k1gMnSc7	občan
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
legálně	legálně	k6eAd1	legálně
zrušit	zrušit	k5eAaPmF	zrušit
své	svůj	k3xOyFgNnSc4	svůj
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
cizince	cizinec	k1gMnPc4	cizinec
a	a	k8xC	a
jako	jako	k9	jako
na	na	k7c4	na
takového	takový	k3xDgMnSc4	takový
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
československé	československý	k2eAgFnPc1d1	Československá
úřady	úřad	k1gInPc4	úřad
od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
okamžiku	okamžik	k1gInSc2	okamžik
pohlížely	pohlížet	k5eAaImAgFnP	pohlížet
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
mohli	moct	k5eAaImAgMnP	moct
využít	využít	k5eAaPmF	využít
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
politicky	politicky	k6eAd1	politicky
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
neangažovali	angažovat	k5eNaBmAgMnP	angažovat
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
či	či	k8xC	či
se	se	k3xPyFc4	se
dostatečně	dostatečně	k6eAd1	dostatečně
káli	kát	k5eAaImAgMnP	kát
a	a	k8xC	a
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
slíbili	slíbit	k5eAaPmAgMnP	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
angažovat	angažovat	k5eAaBmF	angažovat
nebudou	být	k5eNaImBp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Krajan	krajan	k1gMnSc1	krajan
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
příslušníka	příslušník	k1gMnSc4	příslušník
druhé	druhý	k4xOgNnSc1	druhý
a	a	k8xC	a
následovné	následovný	k2eAgFnPc1d1	následovná
generace	generace	k1gFnPc1	generace
imigrantů	imigrant	k1gMnPc2	imigrant
do	do	k7c2	do
cílové	cílový	k2eAgFnSc2d1	cílová
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
transitní	transitní	k2eAgFnSc6d1	transitní
nebo	nebo	k8xC	nebo
cílové	cílový	k2eAgFnSc6d1	cílová
zemi	zem	k1gFnSc6	zem
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
český	český	k2eAgInSc1d1	český
nebo	nebo	k8xC	nebo
slovenský	slovenský	k2eAgInSc1d1	slovenský
původ	původ	k1gInSc1	původ
<g/>
.	.	kIx.	.
</s>
<s>
Krajan	krajan	k1gMnSc1	krajan
alespoň	alespoň	k9	alespoň
pasivně	pasivně	k6eAd1	pasivně
ovládá	ovládat	k5eAaImIp3nS	ovládat
český	český	k2eAgInSc1d1	český
nebo	nebo	k8xC	nebo
slovenský	slovenský	k2eAgInSc1d1	slovenský
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
neasimiloval	asimilovat	k5eNaBmAgMnS	asimilovat
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Krajan	krajan	k1gMnSc1	krajan
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
svým	svůj	k3xOyFgInSc7	svůj
národním	národní	k2eAgInSc7d1	národní
původem	původ	k1gInSc7	původ
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
původní	původní	k2eAgFnSc7d1	původní
vlastí	vlast	k1gFnSc7	vlast
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
tradice	tradice	k1gFnPc1	tradice
přivezené	přivezený	k2eAgFnPc1d1	přivezená
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
krajanských	krajanský	k2eAgInPc6d1	krajanský
spolcích	spolek	k1gInPc6	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnPc1d3	nejznámější
krajanská	krajanský	k2eAgNnPc1d1	krajanské
centra	centrum	k1gNnPc1	centrum
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
jsou	být	k5eAaImIp3nP	být
Tábor	Tábor	k1gInSc4	Tábor
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Dakotě	Dakota	k1gFnSc6	Dakota
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
,	,	kIx,	,
New	New	k1gFnSc6	New
Prague	Prague	k1gFnSc6	Prague
a	a	k8xC	a
Veseli	Vesel	k1gInSc6	Vesel
v	v	k7c6	v
Minnesotě	Minnesota	k1gFnSc6	Minnesota
nebo	nebo	k8xC	nebo
Cicero	Cicero	k1gMnSc1	Cicero
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
krajanské	krajanský	k2eAgInPc1d1	krajanský
spolky	spolek	k1gInPc1	spolek
a	a	k8xC	a
činnosti	činnost	k1gFnPc1	činnost
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
žijí	žít	k5eAaImIp3nP	žít
čeští	český	k2eAgMnPc1d1	český
krajané	krajan	k1gMnPc1	krajan
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
,	,	kIx,	,
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
,	,	kIx,	,
v	v	k7c6	v
moldavské	moldavský	k2eAgFnSc6d1	Moldavská
vesnici	vesnice	k1gFnSc6	vesnice
Holubinka	holubinka	k1gFnSc1	holubinka
<g/>
,	,	kIx,	,
v	v	k7c6	v
několika	několik	k4yIc6	několik
horských	horský	k2eAgFnPc6d1	horská
vesnicích	vesnice	k1gFnPc6	vesnice
v	v	k7c6	v
rumunském	rumunský	k2eAgInSc6d1	rumunský
Banátu	Banát	k1gInSc6	Banát
<g/>
,	,	kIx,	,
v	v	k7c6	v
přilehlé	přilehlý	k2eAgFnSc6d1	přilehlá
nížinaté	nížinatý	k2eAgFnSc6d1	nížinatá
oblasti	oblast	k1gFnSc6	oblast
Banátu	Banát	k1gInSc2	Banát
na	na	k7c6	na
Vojvodině	Vojvodina	k1gFnSc6	Vojvodina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Bela	Bela	k1gFnSc1	Bela
Crkva	Crkva	k1gFnSc1	Crkva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vesnice	vesnice	k1gFnSc1	vesnice
Selo	selo	k1gNnSc1	selo
<g/>
,	,	kIx,	,
Kruščice	Kruščice	k1gFnSc1	Kruščice
a	a	k8xC	a
Gáj	Gáj	k1gFnSc1	Gáj
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
bosenských	bosenský	k2eAgFnPc6d1	bosenská
obcích	obec	k1gFnPc6	obec
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
nebo	nebo	k8xC	nebo
Mačino	Mačin	k2eAgNnSc1d1	Mačin
Brdo	brdo	k1gNnSc1	brdo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
Banja	banjo	k1gNnSc2	banjo
Luky	luk	k1gInPc1	luk
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
česká	český	k2eAgFnSc1d1	Česká
menšina	menšina	k1gFnSc1	menšina
žije	žít	k5eAaImIp3nS	žít
též	též	k9	též
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Daruvaru	Daruvar	k1gInSc2	Daruvar
<g/>
.	.	kIx.	.
</s>
<s>
Exulant	exulant	k1gMnSc1	exulant
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
exul	exul	k1gMnSc1	exul
<g/>
,	,	kIx,	,
vypovězený	vypovězený	k2eAgMnSc1d1	vypovězený
<g/>
)	)	kIx)	)
odešel	odejít	k5eAaPmAgMnS	odejít
nedobrovolně	dobrovolně	k6eNd1	dobrovolně
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
dočasně	dočasně	k6eAd1	dočasně
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
známým	známý	k2eAgInSc7d1	známý
příkladem	příklad	k1gInSc7	příklad
exulanta	exulant	k1gMnSc2	exulant
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Ámos	Ámos	k1gMnSc1	Ámos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
opět	opět	k6eAd1	opět
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
systému	systém	k1gInSc2	systém
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
uloženka	uloženka	k1gFnSc1	uloženka
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
osoba	osoba	k1gFnSc1	osoba
poslaná	poslaný	k2eAgFnSc1d1	poslaná
v	v	k7c6	v
období	období	k1gNnSc6	období
komunistické	komunistický	k2eAgFnSc2d1	komunistická
diktatury	diktatura	k1gFnSc2	diktatura
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
emigračních	emigrační	k2eAgFnPc6d1	emigrační
vlnách	vlna	k1gFnPc6	vlna
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1968	[number]	k4	1968
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Uloženka	Uloženka	k1gFnSc1	Uloženka
<g/>
"	"	kIx"	"
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
jako	jako	k9	jako
běžný	běžný	k2eAgMnSc1d1	běžný
emigrant	emigrant	k1gMnSc1	emigrant
nevzbuzující	vzbuzující	k2eNgFnSc4d1	nevzbuzující
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
cílové	cílový	k2eAgFnSc6d1	cílová
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
nejdříve	dříve	k6eAd3	dříve
získat	získat	k5eAaPmF	získat
nové	nový	k2eAgNnSc4d1	nové
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
a	a	k8xC	a
profesní	profesní	k2eAgNnSc4d1	profesní
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
být	být	k5eAaImF	být
připravený	připravený	k2eAgMnSc1d1	připravený
na	na	k7c4	na
pozdější	pozdní	k2eAgNnSc4d2	pozdější
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Uloženek	Uloženka	k1gFnPc2	Uloženka
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
ČSSR	ČSSR	kA	ČSSR
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
odesláno	odeslat	k5eAaPmNgNnS	odeslat
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
termínu	termín	k1gInSc6	termín
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
poprvé	poprvé	k6eAd1	poprvé
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Ivan	Ivan	k1gMnSc1	Ivan
Sviták	Sviták	k1gMnSc1	Sviták
<g/>
,	,	kIx,	,
pracovník	pracovník	k1gMnSc1	pracovník
MZV	MZV	kA	MZV
ČSSR	ČSSR	kA	ČSSR
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Statích	stať	k1gFnPc6	stať
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
reemigrace	reemigrace	k1gFnSc2	reemigrace
označuje	označovat	k5eAaImIp3nS	označovat
návrat	návrat	k1gInSc4	návrat
emigranta	emigrant	k1gMnSc2	emigrant
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
jeho	jeho	k3xOp3gMnPc2	jeho
potomků	potomek	k1gMnPc2	potomek
<g/>
)	)	kIx)	)
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
