<s>
Britská	britský	k2eAgFnSc1d1
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1
kočka	kočka	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Britská	britský	k2eAgFnSc1d1
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1
kočka	kočka	k1gFnSc1
Britská	britský	k2eAgFnSc1d1
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1
kočka	kočka	k1gFnSc1
-	-	kIx~
modrá	modrý	k2eAgFnSc1d1
bicolorZákladní	bicolorZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Využití	využití	k1gNnSc1
</s>
<s>
kočka	kočka	k1gFnSc1
domácnosti	domácnost	k1gFnSc2
Tělesná	tělesný	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
Typ	typ	k1gInSc1
srsti	srst	k1gFnSc2
</s>
<s>
dlouhá	dlouhý	k2eAgFnSc1d1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1
kočka	kočka	k1gFnSc1
<g/>
,	,	kIx,
Britannica	Britannica	k1gFnSc1
je	být	k5eAaImIp3nS
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
Britské	britský	k2eAgFnSc2d1
krátkosrsté	krátkosrstý	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
světě	svět	k1gInSc6
znají	znát	k5eAaImIp3nP
britské	britský	k2eAgFnPc1d1
dlouhosrsté	dlouhosrstý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
pod	pod	k7c7
různými	různý	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
-	-	kIx~
v	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
někdy	někdy	k6eAd1
používá	používat	k5eAaImIp3nS
označení	označení	k1gNnSc1
"	"	kIx"
<g/>
Britannica	Britannica	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
v	v	k7c6
Americe	Amerika	k1gFnSc6
"	"	kIx"
<g/>
Lowlander	Lowlander	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
u	u	k7c2
nás	my	k3xPp1nPc2
jednoduše	jednoduše	k6eAd1
"	"	kIx"
<g/>
britská	britský	k2eAgFnSc1d1
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1
kočka	kočka	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
října	říjen	k1gInSc2
2017	#num#	k4
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
plemeno	plemeno	k1gNnSc1
dočkalo	dočkat	k5eAaPmAgNnS
plného	plný	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
i	i	k9
u	u	k7c2
v	v	k7c6
Česku	Česko	k1gNnSc6
nejrozšířenější	rozšířený	k2eAgFnSc2d3
organizace	organizace	k1gFnSc2
FIFe	FIF	k1gFnSc2
(	(	kIx(
<g/>
s	s	k7c7
kódem	kód	k1gInSc7
BLH	BLH	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
zatím	zatím	k6eAd1
toto	tento	k3xDgNnSc4
plemeno	plemeno	k1gNnSc4
velmi	velmi	k6eAd1
vzácné	vzácný	k2eAgNnSc4d1
a	a	k8xC
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gInSc3
jen	jen	k9
hrstka	hrstka	k1gFnSc1
chovatelských	chovatelský	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Původ	původ	k1gInSc1
dlouhé	dlouhý	k2eAgFnSc2d1
srsti	srst	k1gFnSc2
u	u	k7c2
Britské	britský	k2eAgFnSc2d1
dlouhosrsté	dlouhosrstý	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
je	být	k5eAaImIp3nS
způsoben	způsobit	k5eAaPmNgInS
v	v	k7c6
minulosti	minulost	k1gFnSc6
křížením	křížení	k1gNnSc7
Britské	britský	k2eAgFnSc2d1
a	a	k8xC
Perské	perský	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
důvodu	důvod	k1gInSc2
kvalitnější	kvalitní	k2eAgFnSc2d2
srsti	srst	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občas	občas	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
vrhu	vrh	k1gInSc2
narodí	narodit	k5eAaPmIp3nS
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
Britské	britský	k2eAgFnSc2d1
krátkosrsté	krátkosrstý	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
jsou	být	k5eAaImIp3nP
oba	dva	k4xCgMnPc1
rodiče	rodič	k1gMnPc1
krátkosrstí	krátkosrstý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Charakter	charakter	k1gInSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1
kočka	kočka	k1gFnSc1
je	být	k5eAaImIp3nS
milé	milá	k1gFnSc2
a	a	k8xC
přítulné	přítulný	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
koťata	kotě	k1gNnPc1
jsou	být	k5eAaImIp3nP
velice	velice	k6eAd1
hravá	hravý	k2eAgNnPc1d1
<g/>
,	,	kIx,
zvídavá	zvídavý	k2eAgNnPc1d1
a	a	k8xC
hyperaktivní	hyperaktivní	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	čí	k3xOyRgNnSc7,k3xOyQgNnSc7
je	být	k5eAaImIp3nS
kočka	kočka	k1gFnSc1
starší	starý	k2eAgFnSc1d2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
klidnější	klidný	k2eAgFnSc1d2
-	-	kIx~
až	až	k9
na	na	k7c4
vzácné	vzácný	k2eAgInPc4d1
případy	případ	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
velice	velice	k6eAd1
inteligentní	inteligentní	k2eAgFnPc4d1
kočky	kočka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
života	život	k1gInSc2
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
života	život	k1gInSc2
Britské	britský	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
15	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
u	u	k7c2
ní	on	k3xPp3gFnSc2
však	však	k9
objeví	objevit	k5eAaPmIp3nS
některé	některý	k3yIgNnSc4
z	z	k7c2
onemocnění	onemocnění	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
způsobena	způsobit	k5eAaPmNgFnS
šlechtěním	šlechtění	k1gNnSc7
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
délka	délka	k1gFnSc1
života	život	k1gInSc2
kočky	kočka	k1gFnSc2
kratší	krátký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
onemocnění	onemocnění	k1gNnPc4
jsou	být	k5eAaImIp3nP
dána	dán	k2eAgFnSc1d1
geneticky	geneticky	k6eAd1
a	a	k8xC
jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
dědičná	dědičný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
je	být	k5eAaImIp3nS
lepší	dobrý	k2eAgFnSc1d2
této	tento	k3xDgFnSc3
situaci	situace	k1gFnSc3
předejít	předejít	k5eAaPmF
a	a	k8xC
raději	rád	k6eAd2
neotálet	otálet	k5eNaImF
s	s	k7c7
návštěvou	návštěva	k1gFnSc7
veterináře	veterinář	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
předčasnému	předčasný	k2eAgNnSc3d1
úmrtí	úmrtí	k1gNnSc3
kočky	kočka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Britská	britský	k2eAgFnSc1d1
dlouhosrstá	dlouhosrstý	k2eAgFnSc1d1
kočka	kočka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
