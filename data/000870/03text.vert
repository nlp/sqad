<s>
Eduard	Eduard	k1gMnSc1	Eduard
Bass	Bass	k1gMnSc1	Bass
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Eduard	Eduard	k1gMnSc1	Eduard
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1888	[number]	k4	1888
Praha-Malá	Praha-Malý	k2eAgFnSc1d1	Praha-Malá
Strana	strana	k1gFnSc1	strana
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1946	[number]	k4	1946
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
fejetonista	fejetonista	k1gMnSc1	fejetonista
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
recitátor	recitátor	k1gMnSc1	recitátor
<g/>
,	,	kIx,	,
konferenciér	konferenciér	k1gMnSc1	konferenciér
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
<g/>
.	.	kIx.	.
</s>
<s>
Proslul	proslout	k5eAaPmAgMnS	proslout
zejména	zejména	k9	zejména
knihami	kniha	k1gFnPc7	kniha
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
a	a	k8xC	a
Cirkus	cirkus	k1gInSc1	cirkus
Humberto	Humberta	k1gFnSc5	Humberta
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
staroměstskou	staroměstský	k2eAgFnSc4d1	Staroměstská
reálku	reálka	k1gFnSc4	reálka
<g/>
,	,	kIx,	,
vyučil	vyučit	k5eAaPmAgMnS	vyučit
se	se	k3xPyFc4	se
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
kartáčnictví	kartáčnictví	k1gNnSc2	kartáčnictví
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
recitátor	recitátor	k1gMnSc1	recitátor
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
v	v	k7c6	v
Schöblově	Schöblův	k2eAgInSc6d1	Schöblův
kabaretu	kabaret	k1gInSc6	kabaret
U	u	k7c2	u
Bílé	bílý	k2eAgFnSc2d1	bílá
labutě	labuť	k1gFnSc2	labuť
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
spolutvůrce	spolutvůrce	k1gMnSc4	spolutvůrce
literárního	literární	k2eAgInSc2d1	literární
kabaretu	kabaret	k1gInSc2	kabaret
Červená	červený	k2eAgFnSc1d1	červená
sedma	sedma	k1gFnSc1	sedma
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
satirickými	satirický	k2eAgInPc7d1	satirický
časopisy	časopis	k1gInPc7	časopis
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
karikaturisty	karikaturista	k1gMnPc7	karikaturista
vydával	vydávat	k5eAaImAgMnS	vydávat
Letáky	leták	k1gInPc7	leták
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
fejetonista	fejetonista	k1gMnSc1	fejetonista
<g/>
,	,	kIx,	,
reportér	reportér	k1gMnSc1	reportér
<g/>
,	,	kIx,	,
soudničkář	soudničkář	k1gMnSc1	soudničkář
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Vydával	vydávat	k5eAaImAgMnS	vydávat
kabaretní	kabaretní	k2eAgInPc4d1	kabaretní
texty	text	k1gInPc4	text
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Syrinx	syrinx	k1gInSc4	syrinx
a	a	k8xC	a
satirický	satirický	k2eAgInSc4d1	satirický
časopis	časopis	k1gInSc4	časopis
Šibeničky	šibenička	k1gFnSc2	šibenička
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
ředitelem	ředitel	k1gMnSc7	ředitel
v	v	k7c6	v
pražských	pražský	k2eAgInPc6d1	pražský
kabaretech	kabaret	k1gInPc6	kabaret
Červená	červený	k2eAgFnSc1d1	červená
sedma	sedma	k1gFnSc1	sedma
a	a	k8xC	a
Rokoko	rokoko	k1gNnSc1	rokoko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
května	květen	k1gInSc2	květen
1933	[number]	k4	1933
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gMnSc7	jejich
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Pegas	Pegas	k1gMnSc1	Pegas
k	k	k7c3	k
drožce	drožka	k1gFnSc3	drožka
připřažený	připřažený	k2eAgInSc1d1	připřažený
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
kabaret	kabaret	k1gInSc1	kabaret
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
Náhrdelník	náhrdelník	k1gInSc1	náhrdelník
<g/>
:	:	kIx,	:
Detektivní	detektivní	k2eAgFnSc1d1	detektivní
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
aktu	akt	k1gInSc6	akt
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
Fanynka	Fanynka	k1gFnSc1	Fanynka
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
humoresky	humoreska	k1gFnPc1	humoreska
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
Letáky	leták	k1gInPc1	leták
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Letohrádek	letohrádek	k1gInSc1	letohrádek
Jeho	jeho	k3xOp3gFnSc2	jeho
Milosti	milost	k1gFnSc2	milost
a	a	k8xC	a
20	[number]	k4	20
jiných	jiný	k2eAgFnPc2d1	jiná
humoresek	humoreska	k1gFnPc2	humoreska
z	z	k7c2	z
vojny	vojna	k1gFnSc2	vojna
vojenské	vojenský	k2eAgFnSc2d1	vojenská
a	a	k8xC	a
občanské	občanský	k2eAgFnSc2d1	občanská
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Případ	případ	k1gInSc4	případ
čísla	číslo	k1gNnSc2	číslo
128	[number]	k4	128
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
historky	historka	k1gFnPc1	historka
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
-	-	kIx~	-
příběh	příběh	k1gInSc1	příběh
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
jedenáct	jedenáct	k4xCc4	jedenáct
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
profesionální	profesionální	k2eAgInSc4d1	profesionální
<g/>
,	,	kIx,	,
neporazitelný	porazitelný	k2eNgInSc4d1	neporazitelný
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Bass	Bass	k1gMnSc1	Bass
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
humornou	humorný	k2eAgFnSc7d1	humorná
formou	forma	k1gFnSc7	forma
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
i	i	k9	i
nad	nad	k7c7	nad
etikou	etika	k1gFnSc7	etika
a	a	k8xC	a
profesionalitou	profesionalita	k1gFnSc7	profesionalita
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
natočil	natočit	k5eAaBmAgMnS	natočit
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
Ladislav	Ladislav	k1gMnSc1	Ladislav
Brom	brom	k1gInSc4	brom
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
Eduard	Eduard	k1gMnSc1	Eduard
Hofman	Hofman	k1gMnSc1	Hofman
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Potulky	potulka	k1gFnPc1	potulka
pražského	pražský	k2eAgMnSc2d1	pražský
reportéra	reportér	k1gMnSc2	reportér
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Šest	šest	k4xCc1	šest
děvčat	děvče	k1gNnPc2	děvče
Williamsonových	Williamsonových	k2eAgFnSc2d1	Williamsonových
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
historky	historka	k1gFnSc2	historka
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
To	to	k9	to
Arbes	Arbes	k1gMnSc1	Arbes
nenapsal	napsat	k5eNaPmAgMnS	napsat
<g/>
,	,	kIx,	,
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
nebásnil	básnit	k5eNaImAgMnS	básnit
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
Umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
mecenáši	mecenáš	k1gMnPc1	mecenáš
a	a	k8xC	a
jiná	jiný	k2eAgFnSc1d1	jiná
čeládka	čeládka	k1gFnSc1	čeládka
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
Holandský	holandský	k2eAgInSc1d1	holandský
deníček	deníček	k1gInSc1	deníček
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Divoký	divoký	k2eAgInSc1d1	divoký
život	život	k1gInSc1	život
Alexandra	Alexandr	k1gMnSc2	Alexandr
Staviského	Staviský	k1gMnSc2	Staviský
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
Čtení	čtení	k1gNnSc6	čtení
o	o	k7c6	o
roce	rok	k1gInSc6	rok
osmačtyřicátém	osmačtyřicátý	k4xOgInSc6	osmačtyřicátý
-	-	kIx~	-
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
Potulky	potulka	k1gFnSc2	potulka
starou	starý	k2eAgFnSc7d1	stará
Prahou	Praha	k1gFnSc7	Praha
-	-	kIx~	-
vycházelo	vycházet	k5eAaImAgNnS	vycházet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921	[number]	k4	1921
až	až	k9	až
1941	[number]	k4	1941
Cirkus	cirkus	k1gInSc1	cirkus
Humberto	Humberta	k1gFnSc5	Humberta
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
zamýšlen	zamýšlet	k5eAaImNgInS	zamýšlet
jako	jako	k8xC	jako
povzbuzení	povzbuzení	k1gNnSc1	povzbuzení
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
vyzdvihovány	vyzdvihován	k2eAgInPc1d1	vyzdvihován
jsou	být	k5eAaImIp3nP	být
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
,	,	kIx,	,
přímost	přímost	k1gFnSc4	přímost
<g/>
,	,	kIx,	,
ctižádost	ctižádost	k1gFnSc4	ctižádost
<g/>
,	,	kIx,	,
...	...	k?	...
Bass	Bass	k1gMnSc1	Bass
zde	zde	k6eAd1	zde
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
cirkusu	cirkus	k1gInSc2	cirkus
sleduje	sledovat	k5eAaImIp3nS	sledovat
tři	tři	k4xCgFnPc1	tři
generace	generace	k1gFnPc1	generace
cirkusáků	cirkusák	k1gMnPc2	cirkusák
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
Vašek	Vašek	k1gMnSc1	Vašek
Karas	Karas	k1gMnSc1	Karas
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
šumavského	šumavský	k2eAgMnSc2d1	šumavský
zedníka	zedník	k1gMnSc2	zedník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odejde	odejít	k5eAaPmIp3nS	odejít
k	k	k7c3	k
cirkusu	cirkus	k1gInSc3	cirkus
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ožení	oženit	k5eAaPmIp3nS	oženit
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
ředitele	ředitel	k1gMnSc2	ředitel
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stane	stanout	k5eAaPmIp3nS	stanout
ředitelem	ředitel	k1gMnSc7	ředitel
cirkusu	cirkus	k1gInSc2	cirkus
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
však	však	k9	však
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgFnPc2d1	různá
profesí	profes	k1gFnPc2	profes
u	u	k7c2	u
cirkusu	cirkus	k1gInSc2	cirkus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
toho	ten	k3xDgInSc2	ten
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
maringotek	maringotka	k1gFnPc2	maringotka
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
soubor	soubor	k1gInSc4	soubor
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
cirkusového	cirkusový	k2eAgNnSc2d1	cirkusové
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
cirkusový	cirkusový	k2eAgInSc4d1	cirkusový
dekameron	dekameron	k1gInSc4	dekameron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kohoutkem	kohoutek	k1gInSc7	kohoutek
svatovítským	svatovítský	k2eAgInSc7d1	svatovítský
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
-	-	kIx~	-
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
Kázáníčka	kázáníčko	k1gNnSc2	kázáníčko
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Křižovatka	křižovatka	k1gFnSc1	křižovatka
u	u	k7c2	u
Prašné	prašný	k2eAgFnSc2d1	prašná
brány	brána	k1gFnSc2	brána
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
Povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
Rozhlásky	rozhlásek	k1gInPc1	rozhlásek
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Pražské	pražský	k2eAgFnSc2d1	Pražská
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c7	za
pohádkou	pohádka	k1gFnSc7	pohádka
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Kukátko	kukátko	k1gNnSc4	kukátko
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
oddíly	oddíl	k1gInPc1	oddíl
Rozmarná	rozmarný	k2eAgNnPc4d1	Rozmarné
vyprávění	vyprávění	k1gNnSc4	vyprávění
<g/>
,	,	kIx,	,
Kukátko	kukátko	k1gNnSc4	kukátko
<g/>
,	,	kIx,	,
Staropražské	staropražský	k2eAgInPc4d1	staropražský
motivy	motiv	k1gInPc4	motiv
Postavy	postava	k1gFnSc2	postava
a	a	k8xC	a
siluety	silueta	k1gFnSc2	silueta
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Moje	můj	k3xOp1gFnSc1	můj
kronika	kronika	k1gFnSc1	kronika
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
životopisná	životopisný	k2eAgFnSc1d1	životopisná
koláž	koláž	k1gFnSc1	koláž
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
Malá	malý	k2eAgFnSc1d1	malá
skleněná	skleněný	k2eAgFnSc1d1	skleněná
gilotinka	gilotinka	k1gFnSc1	gilotinka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
broumovských	broumovský	k2eAgMnPc2d1	broumovský
medvědů	medvěd	k1gMnPc2	medvěd
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Koráb	koráb	k1gInSc1	koráb
pohádek	pohádka	k1gFnPc2	pohádka
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ladislav	Ladislav	k1gMnSc1	Ladislav
Brom	brom	k1gInSc1	brom
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Theodor	Theodor	k1gMnSc1	Theodor
Pištěk	Pištěk	k1gMnSc1	Pištěk
<g/>
,	,	kIx,	,
Antonie	Antonie	k1gFnSc1	Antonie
Nedošinská	Nedošinský	k2eAgFnSc1d1	Nedošinská
<g/>
,	,	kIx,	,
Fanda	Fanda	k1gMnSc1	Fanda
Mrázek	Mrázek	k1gMnSc1	Mrázek
<g/>
,	,	kIx,	,
Raoul	Raoul	k1gInSc1	Raoul
Schránil	schránit	k5eAaBmAgInS	schránit
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
maringotek	maringotka	k1gFnPc2	maringotka
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Jozef	Jozef	k1gMnSc1	Jozef
Kroner	Kroner	k1gMnSc1	Kroner
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Tříska	Tříska	k1gMnSc1	Tříska
<g/>
,	,	kIx,	,
Emília	Emília	k1gFnSc1	Emília
Vášáryová	Vášáryová	k1gFnSc1	Vášáryová
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Růžek	Růžek	k1gMnSc1	Růžek
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
Medřická	Medřická	k1gFnSc1	Medřická
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Eduard	Eduard	k1gMnSc1	Eduard
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Jiří	Jiří	k1gMnSc1	Jiří
Sovák	Sovák	k1gMnSc1	Sovák
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Chramostová	Chramostová	k1gFnSc1	Chramostová
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hlinomaz	hlinomaz	k1gMnSc1	hlinomaz
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
,	,	kIx,	,
Jiřina	Jiřina	k1gFnSc1	Jiřina
Bohdalová	Bohdalová	k1gFnSc1	Bohdalová
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Ustinov	Ustinov	k1gInSc1	Ustinov
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Cirkus	cirkus	k1gInSc1	cirkus
Humberto	Humberta	k1gFnSc5	Humberta
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česko-německý	českoěmecký	k2eAgInSc1d1	česko-německý
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Martin	Martin	k1gMnSc1	Martin
Růžek	Růžek	k1gMnSc1	Růžek
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
Veškrnová	Veškrnová	k1gFnSc1	Veškrnová
<g/>
,	,	kIx,	,
Radoslav	Radoslav	k1gMnSc1	Radoslav
Brzobohatý	Brzobohatý	k1gMnSc1	Brzobohatý
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hanzlík	Hanzlík	k1gMnSc1	Hanzlík
<g/>
,	,	kIx,	,
Werner	Werner	k1gMnSc1	Werner
Possardt	Possardt	k1gMnSc1	Possardt
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
