<s>
Sir	sir	k1gMnSc1	sir
Timothy	Timotha	k1gFnSc2	Timotha
"	"	kIx"	"
<g/>
Tim	Tim	k?	Tim
<g/>
"	"	kIx"	"
John	John	k1gMnSc1	John
Berners-Lee	Berners-Le	k1gFnSc2	Berners-Le
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1955	[number]	k4	1955
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvůrce	tvůrce	k1gMnSc1	tvůrce
World	World	k1gMnSc1	World
Wide	Wide	k1gInSc4	Wide
Webu	web	k1gInSc2	web
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
konsorcia	konsorcium	k1gNnSc2	konsorcium
W	W	kA	W
<g/>
3	[number]	k4	3
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dohlíží	dohlížet	k5eAaImIp3nP	dohlížet
na	na	k7c4	na
pokračující	pokračující	k2eAgInSc4d1	pokračující
vývoj	vývoj	k1gInSc4	vývoj
webu	web	k1gInSc2	web
<g/>
.	.	kIx.	.
</s>
