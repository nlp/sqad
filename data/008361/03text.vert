<p>
<s>
Sir	sir	k1gMnSc1	sir
Timothy	Timotha	k1gFnSc2	Timotha
"	"	kIx"	"
<g/>
Tim	Tim	k?	Tim
<g/>
"	"	kIx"	"
John	John	k1gMnSc1	John
Berners-Lee	Berners-Le	k1gFnSc2	Berners-Le
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1955	[number]	k4	1955
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvůrce	tvůrce	k1gMnSc1	tvůrce
World	World	k1gMnSc1	World
Wide	Wide	k1gInSc4	Wide
Webu	web	k1gInSc2	web
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
konsorcia	konsorcium	k1gNnSc2	konsorcium
W	W	kA	W
<g/>
3	[number]	k4	3
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dohlíží	dohlížet	k5eAaImIp3nP	dohlížet
na	na	k7c4	na
pokračující	pokračující	k2eAgInSc4d1	pokračující
vývoj	vývoj	k1gInSc4	vývoj
webu	web	k1gInSc2	web
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
Conwayovi	Conwayův	k2eAgMnPc1d1	Conwayův
Berners-Lee	Berners-Lee	k1gNnSc3	Berners-Lee
a	a	k8xC	a
Mary	Mary	k1gFnSc3	Mary
Lee	Lea	k1gFnSc3	Lea
Woods	Woodsa	k1gFnPc2	Woodsa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
matematici	matematik	k1gMnPc1	matematik
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
společně	společně	k6eAd1	společně
zaměstnáni	zaměstnat	k5eAaPmNgMnP	zaměstnat
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
postavil	postavit	k5eAaPmAgMnS	postavit
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
Mark	Mark	k1gMnSc1	Mark
I.	I.	kA	I.
Rodiče	rodič	k1gMnPc1	rodič
ho	on	k3xPp3gMnSc4	on
učili	učít	k5eAaPmAgMnP	učít
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohli	moct	k5eAaImAgMnP	moct
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
u	u	k7c2	u
večeře	večeře	k1gFnSc2	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Queen	Queen	k1gInSc4	Queen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
a	a	k8xC	a
Oxfordskou	oxfordský	k2eAgFnSc4d1	Oxfordská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c4	v
Plessey	Plessea	k1gFnPc4	Plessea
Telecommunications	Telecommunicationsa	k1gFnPc2	Telecommunicationsa
Limited	limited	k2eAgFnSc2d1	limited
jako	jako	k8xC	jako
programátor	programátor	k1gInSc1	programátor
<g/>
;	;	kIx,	;
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
Jane	Jan	k1gMnSc5	Jan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
D.G.	D.G.	k1gFnSc6	D.G.
Nash	Nash	k1gInSc4	Nash
Limited	limited	k2eAgInSc4d1	limited
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaBmAgInS	napsat
software	software	k1gInSc1	software
pro	pro	k7c4	pro
sazbu	sazba	k1gFnSc4	sazba
textu	text	k1gInSc2	text
a	a	k8xC	a
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
působil	působit	k5eAaImAgMnS	působit
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
organizaci	organizace	k1gFnSc6	organizace
pro	pro	k7c4	pro
jaderný	jaderný	k2eAgInSc4d1	jaderný
výzkum	výzkum	k1gInSc4	výzkum
(	(	kIx(	(
<g/>
CERN	CERN	kA	CERN
<g/>
)	)	kIx)	)
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaPmAgInS	napsat
program	program	k1gInSc1	program
jménem	jméno	k1gNnSc7	jméno
ENQUIRE	ENQUIRE	kA	ENQUIRE
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgInS	mít
pomáhat	pomáhat	k5eAaImF	pomáhat
při	při	k7c6	při
dokumentaci	dokumentace	k1gFnSc6	dokumentace
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
okamžiků	okamžik	k1gInPc2	okamžik
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
a	a	k8xC	a
spojovat	spojovat	k5eAaImF	spojovat
je	on	k3xPp3gInPc4	on
s	s	k7c7	s
asociacemi	asociace	k1gFnPc7	asociace
k	k	k7c3	k
nejrůznějším	různý	k2eAgInPc3d3	nejrůznější
komplexům	komplex	k1gInPc3	komplex
úloh	úloha	k1gFnPc2	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zásobě	zásoba	k1gFnSc3	zásoba
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
exempláři	exemplář	k1gInSc6	exemplář
a	a	k8xC	a
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
jej	on	k3xPp3gNnSc4	on
napadlo	napadnout	k5eAaPmAgNnS	napadnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
multimediální	multimediální	k2eAgInSc4d1	multimediální
informační	informační	k2eAgInSc4d1	informační
systém	systém	k1gInSc4	systém
s	s	k7c7	s
dálkovou	dálkový	k2eAgFnSc7d1	dálková
sítí	síť	k1gFnSc7	síť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
zdrojům	zdroj	k1gInPc3	zdroj
existoval	existovat	k5eAaImAgMnS	existovat
přístup	přístup	k1gInSc4	přístup
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Berners-Lee	Berners-Lee	k6eAd1	Berners-Lee
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
plánu	plán	k1gInSc6	plán
na	na	k7c4	na
World	World	k1gInSc4	World
Wide	Wid	k1gInSc2	Wid
Web	web	k1gInSc1	web
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
pohodlně	pohodlně	k6eAd1	pohodlně
data	datum	k1gNnPc4	datum
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
fyziků	fyzik	k1gMnPc2	fyzik
věnujících	věnující	k2eAgMnPc2d1	věnující
se	se	k3xPyFc4	se
výzkumu	výzkum	k1gInSc3	výzkum
vysokoenergetických	vysokoenergetický	k2eAgInPc2d1	vysokoenergetický
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
roztroušení	roztroušení	k1gNnSc4	roztroušení
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gFnPc2	Berners-Lee
společně	společně	k6eAd1	společně
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Cailliauem	Cailliau	k1gMnSc7	Cailliau
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
první	první	k4xOgInSc4	první
návrh	návrh	k1gInSc4	návrh
projektu	projekt	k1gInSc2	projekt
vytvoření	vytvoření	k1gNnSc4	vytvoření
distribuovaného	distribuovaný	k2eAgInSc2d1	distribuovaný
hypertextového	hypertextový	k2eAgInSc2d1	hypertextový
systému	systém	k1gInSc2	systém
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
zahájen	zahájen	k2eAgInSc4d1	zahájen
projekt	projekt	k1gInSc4	projekt
WWW	WWW	kA	WWW
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
napsal	napsat	k5eAaBmAgMnS	napsat
první	první	k4xOgMnSc1	první
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
WorldWideWeb	WorldWideWba	k1gFnPc2	WorldWideWba
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
přejmenovaný	přejmenovaný	k2eAgInSc1d1	přejmenovaný
na	na	k7c4	na
Nexus	nexus	k1gInSc4	nexus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
počítač	počítač	k1gInSc4	počítač
NeXT	NeXT	k1gFnSc2	NeXT
v	v	k7c6	v
CERNu	CERNus	k1gInSc6	CERNus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
(	(	kIx(	(
<g/>
Order	Order	k1gMnSc1	Order
of	of	k?	of
Merit	meritum	k1gNnPc2	meritum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
zásluhy	zásluha	k1gFnSc2	zásluha
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
webových	webový	k2eAgFnPc2d1	webová
technologii	technologie	k1gFnSc3	technologie
udělena	udělen	k2eAgFnSc1d1	udělena
Turingova	Turingův	k2eAgFnSc1d1	Turingova
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gInSc1	Berners-Lee
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gFnPc2	Berners-Lee
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
Tima	Timus	k1gMnSc2	Timus
Berners-Leeho	Berners-Lee	k1gMnSc2	Berners-Lee
</s>
</p>
<p>
<s>
Blog	Blog	k1gInSc1	Blog
Tima	Timus	k1gMnSc2	Timus
Berners-Leeho	Berners-Lee	k1gMnSc2	Berners-Lee
</s>
</p>
