<p>
<s>
Ostřice	ostřice	k1gFnSc1	ostřice
blešní	blešnit	k5eAaPmIp3nS	blešnit
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
pulicaris	pulicaris	k1gFnSc2	pulicaris
L.	L.	kA	L.
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Vignea	Vigne	k2eAgFnSc1d1	Vigne
pulicaris	pulicaris	k1gFnSc1	pulicaris
(	(	kIx(	(
<g/>
L.	L.	kA	L.
<g/>
)	)	kIx)	)
Rchb	Rchb	k1gMnSc1	Rchb
<g/>
.	.	kIx.	.
in	in	k?	in
Mössler	Mössler	k1gInSc1	Mössler
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jednoděložné	jednoděložný	k2eAgFnSc2d1	jednoděložná
rostliny	rostlina	k1gFnSc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
šáchorovité	šáchorovitý	k2eAgFnSc2d1	šáchorovitá
(	(	kIx(	(
<g/>
Cyperaceae	Cyperacea	k1gFnSc2	Cyperacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
drobnou	drobný	k2eAgFnSc4d1	drobná
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
řídké	řídký	k2eAgInPc4d1	řídký
trsy	trs	k1gInPc4	trs
<g/>
.	.	kIx.	.
</s>
<s>
Lodyha	lodyha	k1gFnSc1	lodyha
je	být	k5eAaImIp3nS	být
tupě	tupě	k6eAd1	tupě
trojhranná	trojhranný	k2eAgFnSc1d1	trojhranná
až	až	k8xS	až
oblá	oblý	k2eAgFnSc1d1	oblá
<g/>
,	,	kIx,	,
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
střídavé	střídavý	k2eAgInPc1d1	střídavý
<g/>
,	,	kIx,	,
přisedlé	přisedlý	k2eAgInPc1d1	přisedlý
<g/>
,	,	kIx,	,
s	s	k7c7	s
listovými	listový	k2eAgFnPc7d1	listová
pochvami	pochva	k1gFnPc7	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Čepele	čepel	k1gFnPc1	čepel
listu	list	k1gInSc2	list
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
0,5	[number]	k4	0,5
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
mm	mm	kA	mm
široké	široký	k2eAgFnPc1d1	široká
<g/>
,	,	kIx,	,
štětinovité	štětinovitý	k2eAgFnPc1d1	štětinovitá
<g/>
.	.	kIx.	.
</s>
<s>
Pochvy	pochva	k1gFnPc1	pochva
bazálních	bazální	k2eAgInPc2d1	bazální
listů	list	k1gInPc2	list
jsou	být	k5eAaImIp3nP	být
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Ostřice	ostřice	k1gFnSc1	ostřice
blešní	blešnit	k5eAaPmIp3nS	blešnit
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jednoklasé	jednoklasý	k2eAgFnPc4d1	jednoklasý
ostřice	ostřice	k1gFnPc4	ostřice
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
lodyhy	lodyha	k1gFnSc2	lodyha
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
klásek	klásek	k1gInSc4	klásek
<g/>
,	,	kIx,	,
cca	cca	kA	cca
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2,5	[number]	k4	2,5
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
rostlina	rostlina	k1gFnSc1	rostlina
jednodomá	jednodomý	k2eAgFnSc1d1	jednodomá
<g/>
,	,	kIx,	,
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
klásku	klásek	k1gInSc2	klásek
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
samčí	samčí	k2eAgInPc1d1	samčí
květy	květ	k1gInPc1	květ
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
samičí	samičí	k2eAgFnSc6d1	samičí
<g/>
,	,	kIx,	,
kterých	který	k3yRgNnPc2	který
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Okvětí	okvětí	k1gNnSc1	okvětí
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samčích	samčí	k2eAgInPc6d1	samčí
květech	květ	k1gInPc6	květ
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
3	[number]	k4	3
tyčinky	tyčinka	k1gFnSc2	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Čnělky	čnělka	k1gFnPc1	čnělka
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
mošnička	mošnička	k1gFnSc1	mošnička
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5,5	[number]	k4	5,5
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
jen	jen	k6eAd1	jen
zúžená	zúžený	k2eAgFnSc1d1	zúžená
<g/>
,	,	kIx,	,
zobánek	zobánek	k1gInSc1	zobánek
prakticky	prakticky	k6eAd1	prakticky
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
mošnička	mošnička	k1gFnSc1	mošnička
je	být	k5eAaImIp3nS	být
podepřená	podepřený	k2eAgFnSc1d1	podepřená
plevou	pleva	k1gFnSc7	pleva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gFnPc4	on
rezavě	rezavě	k6eAd1	rezavě
hnědá	hnědat	k5eAaImIp3nS	hnědat
se	s	k7c7	s
zeleným	zelený	k2eAgInSc7d1	zelený
kýlem	kýl	k1gInSc7	kýl
a	a	k8xC	a
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
blanitě	blanitě	k6eAd1	blanitě
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
<g/>
.	.	kIx.	.
</s>
<s>
Plevy	pleva	k1gFnPc1	pleva
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
zralosti	zralost	k1gFnPc4	zralost
opadané	opadaný	k2eAgFnPc4d1	opadaná
a	a	k8xC	a
mošničky	mošnička	k1gFnPc1	mošnička
pak	pak	k6eAd1	pak
směřují	směřovat	k5eAaImIp3nP	směřovat
kolmo	kolmo	k6eAd1	kolmo
do	do	k7c2	do
boku	bok	k1gInSc2	bok
až	až	k9	až
jsou	být	k5eAaImIp3nP	být
skloněny	sklonit	k5eAaPmNgInP	sklonit
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k6eAd1	až
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
chromozómů	chromozóm	k1gInPc2	chromozóm
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
58	[number]	k4	58
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
vhodných	vhodný	k2eAgNnPc6d1	vhodné
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
polovině	polovina	k1gFnSc6	polovina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
a	a	k8xC	a
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
těchto	tento	k3xDgNnPc2	tento
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
také	také	k9	také
na	na	k7c4	na
Island	Island	k1gInSc4	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
celkem	celkem	k6eAd1	celkem
vzácný	vzácný	k2eAgMnSc1d1	vzácný
a	a	k8xC	a
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
(	(	kIx(	(
<g/>
kategorie	kategorie	k1gFnSc2	kategorie
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
druh	druh	k1gInSc1	druh
flóry	flóra	k1gFnSc2	flóra
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
rašeliništích	rašeliniště	k1gNnPc6	rašeliniště
a	a	k8xC	a
na	na	k7c6	na
rašelinných	rašelinný	k2eAgFnPc6d1	rašelinná
loukách	louka	k1gFnPc6	louka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
absencí	absence	k1gFnSc7	absence
těchto	tento	k3xDgInPc2	tento
biotopů	biotop	k1gInPc2	biotop
zcela	zcela	k6eAd1	zcela
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Grulich	Grulich	k1gMnSc1	Grulich
V.	V.	kA	V.
et	et	k?	et
Řepka	řepka	k1gFnSc1	řepka
V	v	k7c6	v
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Carex	Carex	k1gInSc1	Carex
L.	L.	kA	L.
In	In	k1gFnSc2	In
<g/>
:	:	kIx,	:
Klíč	klíč	k1gInSc1	klíč
ke	k	k7c3	k
Květeně	květena	k1gFnSc3	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Kubát	Kubát	k1gMnSc1	Kubát
K.	K.	kA	K.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Květena	květena	k1gFnSc1	květena
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Dostál	Dostál	k1gMnSc1	Dostál
J.	J.	kA	J.
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ostřice	ostřice	k1gFnSc2	ostřice
blešní	blešnit	k5eAaPmIp3nS	blešnit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
-	-	kIx~	-
mapky	mapka	k1gFnPc1	mapka
rozšíření	rozšíření	k1gNnSc2	rozšíření
</s>
</p>
<p>
<s>
Carex	Carex	k1gInSc1	Carex
interactive	interactiv	k1gInSc5	interactiv
identification	identification	k1gInSc1	identification
key	key	k?	key
</s>
</p>
