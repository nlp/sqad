<s>
Kung	Kung	k1gMnSc1	Kung
Fu	fu	k0	fu
Panda	panda	k1gFnSc1	panda
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Režírovali	režírovat	k5eAaImAgMnP	režírovat
jej	on	k3xPp3gMnSc4	on
John	John	k1gMnSc1	John
Stevenson	Stevenson	k1gMnSc1	Stevenson
a	a	k8xC	a
Mark	Mark	k1gMnSc1	Mark
Osborne	Osborn	k1gInSc5	Osborn
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
postavy	postava	k1gFnPc4	postava
namluvili	namluvit	k5eAaBmAgMnP	namluvit
známí	známý	k2eAgMnPc1d1	známý
američtí	americký	k2eAgMnPc1d1	americký
a	a	k8xC	a
čínští	čínský	k2eAgMnPc1d1	čínský
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Dustin	Dustin	k1gMnSc1	Dustin
Hoffman	Hoffman	k1gMnSc1	Hoffman
<g/>
,	,	kIx,	,
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
<g/>
,	,	kIx,	,
Jackie	Jackie	k1gFnSc1	Jackie
Chan	Chana	k1gFnPc2	Chana
<g/>
,	,	kIx,	,
Lucy	Luc	k2eAgFnPc1d1	Luca
Liu	Liu	k1gFnPc1	Liu
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
a	a	k8xC	a
dočkal	dočkat	k5eAaPmAgMnS	dočkat
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
příznivého	příznivý	k2eAgNnSc2d1	příznivé
přijetí	přijetí	k1gNnSc2	přijetí
jak	jak	k8xC	jak
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
tak	tak	k9	tak
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
kasovním	kasovní	k2eAgInSc7d1	kasovní
trhákem	trhák	k1gInSc7	trhák
<g/>
,	,	kIx,	,
když	když	k8xS	když
celosvětově	celosvětově	k6eAd1	celosvětově
vydělal	vydělat	k5eAaPmAgInS	vydělat
631,7	[number]	k4	631,7
miliónu	milión	k4xCgInSc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
volné	volný	k2eAgNnSc1d1	volné
pokračování	pokračování	k1gNnSc1	pokračování
Kung	Kunga	k1gFnPc2	Kunga
Fu	fu	k0	fu
Panda	panda	k1gFnSc1	panda
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
je	být	k5eAaImIp3nS	být
mladý	mladý	k2eAgMnSc1d1	mladý
nemotorný	nemotorný	k2eAgMnSc1d1	nemotorný
panda	panda	k1gFnSc1	panda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypomáhá	vypomáhat	k5eAaImIp3nS	vypomáhat
(	(	kIx(	(
<g/>
nevlastnímu	vlastní	k2eNgInSc3d1	nevlastní
<g/>
)	)	kIx)	)
husímu	husí	k2eAgMnSc3d1	husí
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
panu	pan	k1gMnSc3	pan
Pingovi	Ping	k1gMnSc3	Ping
<g/>
,	,	kIx,	,
s	s	k7c7	s
roznašením	roznašení	k1gNnSc7	roznašení
jídla	jídlo	k1gNnSc2	jídlo
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
kung	kung	k1gMnSc1	kung
fu	fu	k0	fu
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
idoly	idol	k1gInPc4	idol
jsou	být	k5eAaImIp3nP	být
členové	člen	k1gMnPc1	člen
"	"	kIx"	"
<g/>
Pěti	pět	k4xCc2	pět
postrachů	postrach	k1gInPc2	postrach
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tygřice	tygřice	k1gFnSc1	tygřice
<g/>
,	,	kIx,	,
Opičák	opičák	k1gMnSc1	opičák
<g/>
,	,	kIx,	,
Zmije	zmije	k1gFnSc1	zmije
<g/>
,	,	kIx,	,
Jeřáb	jeřáb	k1gInSc1	jeřáb
a	a	k8xC	a
Kudlanka	kudlanka	k1gFnSc1	kudlanka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pětice	pětice	k1gFnSc1	pětice
mistrů	mistr	k1gMnPc2	mistr
Kung	Kunga	k1gFnPc2	Kunga
Fu	fu	k0	fu
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Mistra	mistr	k1gMnSc2	mistr
Shifua	Shifuus	k1gMnSc2	Shifuus
ochraňují	ochraňovat	k5eAaImIp3nP	ochraňovat
Údolí	údolí	k1gNnSc4	údolí
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
má	mít	k5eAaImIp3nS	mít
Shifuův	Shifuův	k2eAgMnSc1d1	Shifuův
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
starý	starý	k2eAgMnSc1d1	starý
velmistr	velmistr	k1gMnSc1	velmistr
Oogway	Oogwaa	k1gFnSc2	Oogwaa
<g/>
,	,	kIx,	,
vizi	vize	k1gFnSc4	vize
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgInPc4	který
sněžný	sněžný	k2eAgMnSc1d1	sněžný
levhart	levhart	k1gMnSc1	levhart
Tai	Tai	k1gMnSc1	Tai
Lung	Lung	k1gMnSc1	Lung
unikne	uniknout	k5eAaPmIp3nS	uniknout
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
přijde	přijít	k5eAaPmIp3nS	přijít
se	se	k3xPyFc4	se
do	do	k7c2	do
Údolí	údolí	k1gNnSc2	údolí
pomstít	pomstít	k5eAaPmF	pomstít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mu	on	k3xPp3gMnSc3	on
velmistr	velmistr	k1gMnSc1	velmistr
Oogway	Oogway	k1gInPc4	Oogway
kdysi	kdysi	k6eAd1	kdysi
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
předat	předat	k5eAaPmF	předat
Dračí	dračí	k2eAgInSc1d1	dračí
svitek	svitek	k1gInSc1	svitek
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tajemství	tajemství	k1gNnSc1	tajemství
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
Shifu	Shif	k1gInSc2	Shif
vyšle	vyslat	k5eAaPmIp3nS	vyslat
posla	posel	k1gMnSc4	posel
až	až	k9	až
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Tai	Tai	k1gMnSc1	Tai
Lung	Lung	k1gMnSc1	Lung
držen	držet	k5eAaImNgMnS	držet
<g/>
,	,	kIx,	,
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
stráží	stráž	k1gFnPc2	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vybrat	vybrat	k5eAaPmF	vybrat
nového	nový	k2eAgMnSc4d1	nový
Dračího	dračí	k2eAgMnSc4d1	dračí
bojovníka	bojovník	k1gMnSc4	bojovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Tai	Tai	k1gMnSc3	Tai
Lungovi	Lung	k1gMnSc3	Lung
postavil	postavit	k5eAaPmAgMnS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
překvapení	překvapení	k1gNnSc3	překvapení
velmistr	velmistr	k1gMnSc1	velmistr
Oogway	Oogwaa	k1gFnSc2	Oogwaa
vybere	vybrat	k5eAaPmIp3nS	vybrat
právě	právě	k9	právě
Poa	Poa	k1gFnSc1	Poa
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
vinou	vinout	k5eAaImIp3nP	vinout
právě	právě	k6eAd1	právě
husího	husí	k2eAgMnSc4d1	husí
posla	posel	k1gMnSc4	posel
<g/>
,	,	kIx,	,
Tai	Tai	k1gMnSc1	Tai
Lung	Lung	k1gMnSc1	Lung
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
prchá	prchat	k5eAaImIp3nS	prchat
<g/>
.	.	kIx.	.
</s>
<s>
Po	Po	kA	Po
tedy	tedy	k9	tedy
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
tréninkem	trénink	k1gInSc7	trénink
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
Shifu	Shif	k1gMnSc3	Shif
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
nejprve	nejprve	k6eAd1	nejprve
snaží	snažit	k5eAaImIp3nS	snažit
odradit	odradit	k5eAaPmF	odradit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
členové	člen	k1gMnPc1	člen
Zuřivé	zuřivý	k2eAgFnSc2d1	zuřivá
pětky	pětka	k1gFnSc2	pětka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Po	po	k7c4	po
se	se	k3xPyFc4	se
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
naučit	naučit	k5eAaPmF	naučit
ani	ani	k8xC	ani
základům	základ	k1gInPc3	základ
bojového	bojový	k2eAgNnSc2d1	bojové
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
však	však	k9	však
Shifuovi	Shifuův	k2eAgMnPc1d1	Shifuův
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
motivace	motivace	k1gFnSc2	motivace
jídlem	jídlo	k1gNnSc7	jídlo
podaří	podařit	k5eAaPmIp3nS	podařit
Poa	Poa	k1gFnSc1	Poa
vyučit	vyučit	k5eAaPmF	vyučit
sice	sice	k8xC	sice
improvizovanému	improvizovaný	k2eAgInSc3d1	improvizovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
účinnému	účinný	k2eAgInSc3d1	účinný
bojovému	bojový	k2eAgInSc3d1	bojový
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Tai	Tai	k1gMnSc1	Tai
Lung	Lung	k1gMnSc1	Lung
dorazí	dorazit	k5eAaPmIp3nS	dorazit
<g/>
,	,	kIx,	,
Shifu	Shif	k1gInSc3	Shif
chce	chtít	k5eAaImIp3nS	chtít
vyjevit	vyjevit	k5eAaPmF	vyjevit
Poovi	Pooev	k1gFnSc3	Pooev
tajemství	tajemství	k1gNnSc2	tajemství
Dračího	dračí	k2eAgInSc2d1	dračí
svitku	svitek	k1gInSc2	svitek
<g/>
,	,	kIx,	,
ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
svitek	svitek	k1gInSc1	svitek
je	být	k5eAaImIp3nS	být
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
Tai	Tai	k1gFnSc1	Tai
Lunga	Lunga	k1gFnSc1	Lunga
zastavit	zastavit	k5eAaPmF	zastavit
Zuřivá	zuřivý	k2eAgFnSc1d1	zuřivá
pětka	pětka	k1gFnSc1	pětka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Tai	Tai	k1gMnSc1	Tai
Lung	Lung	k1gMnSc1	Lung
je	být	k5eAaImIp3nS	být
jednoho	jeden	k4xCgMnSc4	jeden
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
porazí	porazit	k5eAaPmIp3nS	porazit
pomocí	pomocí	k7c2	pomocí
své	svůj	k3xOyFgFnSc2	svůj
techniky	technika	k1gFnSc2	technika
úderů	úder	k1gInPc2	úder
na	na	k7c4	na
nervové	nervový	k2eAgInPc4d1	nervový
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zoufalství	zoufalství	k1gNnSc6	zoufalství
nařídí	nařídit	k5eAaPmIp3nS	nařídit
mistr	mistr	k1gMnSc1	mistr
Shifu	Shif	k1gInSc2	Shif
evakuaci	evakuace	k1gFnSc4	evakuace
údolí	údolí	k1gNnSc2	údolí
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
Tai	Tai	k1gMnSc2	Tai
Lunga	Lung	k1gMnSc4	Lung
zadržet	zadržet	k5eAaPmF	zadržet
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
půjde	jít	k5eAaImIp3nS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
(	(	kIx(	(
<g/>
adoptivním	adoptivní	k2eAgMnSc7d1	adoptivní
<g/>
)	)	kIx)	)
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
věci	věc	k1gFnPc1	věc
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
lidé	člověk	k1gMnPc1	člověk
věří	věřit	k5eAaImIp3nP	věřit
že	že	k8xS	že
výjimečné	výjimečný	k2eAgInPc1d1	výjimečný
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Po	Po	kA	Po
pochopí	pochopit	k5eAaPmIp3nS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
princip	princip	k1gInSc1	princip
Dračího	dračí	k2eAgInSc2d1	dračí
svitku	svitek	k1gInSc2	svitek
<g/>
.	.	kIx.	.
</s>
<s>
Vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Tai	Tai	k1gMnSc1	Tai
Lung	Lung	k1gMnSc1	Lung
mezitím	mezitím	k6eAd1	mezitím
málem	málem	k6eAd1	málem
zabil	zabít	k5eAaPmAgMnS	zabít
Shifua	Shifua	k1gMnSc1	Shifua
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
tuhý	tuhý	k2eAgInSc4d1	tuhý
souboj	souboj	k1gInSc4	souboj
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
Po	po	k7c6	po
využívá	využívat	k5eAaPmIp3nS	využívat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
Tai	Tai	k1gFnSc7	Tai
Lungovy	Lungův	k2eAgInPc1d1	Lungův
nervové	nervový	k2eAgInPc1d1	nervový
údery	úder	k1gInPc1	úder
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
tloušťce	tloušťka	k1gFnSc3	tloušťka
neúčinkují	účinkovat	k5eNaImIp3nP	účinkovat
a	a	k8xC	a
také	také	k9	také
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
neortodoxní	ortodoxní	k2eNgInSc1d1	neortodoxní
bojový	bojový	k2eAgInSc1d1	bojový
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Tai	Tai	k1gFnSc4	Tai
Lunga	Lung	k1gMnSc2	Lung
neznámý	známý	k2eNgMnSc1d1	neznámý
a	a	k8xC	a
matoucí	matoucí	k2eAgMnSc1d1	matoucí
<g/>
.	.	kIx.	.
</s>
<s>
Tai	Tai	k?	Tai
Lung	Lung	k1gInSc1	Lung
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
boje	boj	k1gInSc2	boj
zmocní	zmocnit	k5eAaPmIp3nS	zmocnit
Dračího	dračí	k2eAgInSc2d1	dračí
svitku	svitek	k1gInSc2	svitek
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
schopen	schopen	k2eAgMnSc1d1	schopen
pochopit	pochopit	k5eAaPmF	pochopit
jeho	jeho	k3xOp3gInSc4	jeho
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
jej	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
porazí	porazit	k5eAaPmIp3nS	porazit
mocnou	mocný	k2eAgFnSc7d1	mocná
technikou	technika	k1gFnSc7	technika
Skadoosh	Skadoosha	k1gFnPc2	Skadoosha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
zachráncem	zachránce	k1gMnSc7	zachránce
Údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
Zuřivé	zuřivý	k2eAgFnSc2d1	zuřivá
pětky	pětka	k1gFnSc2	pětka
i	i	k9	i
Mistr	mistr	k1gMnSc1	mistr
Shifu	Shif	k1gInSc2	Shif
jej	on	k3xPp3gMnSc4	on
uznají	uznat	k5eAaPmIp3nP	uznat
jako	jako	k8xC	jako
opravdového	opravdový	k2eAgMnSc4d1	opravdový
mistra	mistr	k1gMnSc4	mistr
kung	kung	k1gInSc4	kung
fu	fu	k0	fu
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
animovaný	animovaný	k2eAgInSc1d1	animovaný
<g/>
,	,	kIx,	,
následující	následující	k2eAgInSc1d1	následující
přehled	přehled	k1gInSc1	přehled
tedy	tedy	k9	tedy
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
v	v	k7c6	v
originále	originál	k1gInSc6	originál
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
hlas	hlas	k1gInSc1	hlas
dané	daný	k2eAgFnSc6d1	daná
postavě	postava	k1gFnSc6	postava
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
přehled	přehled	k1gInSc1	přehled
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
kterou	který	k3yRgFnSc4	který
postavu	postava	k1gFnSc4	postava
namluvil	namluvit	k5eAaBmAgMnS	namluvit
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
dabingu	dabing	k1gInSc6	dabing
<g/>
.	.	kIx.	.
</s>
