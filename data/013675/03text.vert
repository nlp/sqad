<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
O	o	k7c6
odbojovém	odbojový	k2eAgInSc6d1
orgánu	orgán	k1gInSc6
z	z	k7c2
konce	konec	k1gInSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
odboj	odboj	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
zkratkou	zkratka	k1gFnSc7
ČNR	ČNR	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1969	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1992	#num#	k4
zákonodárným	zákonodárný	k2eAgInSc7d1
sborem	sbor	k1gInSc7
České	český	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
republiky	republika	k1gFnSc2
byla	být	k5eAaImAgFnS
nejvyšším	vysoký	k2eAgInSc7d3
orgánem	orgán	k1gInSc7
státní	státní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1993	#num#	k4
se	se	k3xPyFc4
spolu	spolu	k6eAd1
s	s	k7c7
osamostatněním	osamostatnění	k1gNnSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
podle	podle	k7c2
čl	čl	kA
<g/>
.	.	kIx.
106	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
Ústavy	ústava	k1gFnSc2
transformovala	transformovat	k5eAaBmAgFnS
na	na	k7c4
Poslaneckou	poslanecký	k2eAgFnSc4d1
sněmovnu	sněmovna	k1gFnSc4
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
ČNR	ČNR	kA
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1968	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
č.	č.	k?
77	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
přípravě	příprava	k1gFnSc6
federativního	federativní	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
„	„	k?
<g/>
prozatímní	prozatímní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
ústavní	ústavní	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
českého	český	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
“	“	k?
se	s	k7c7
150	#num#	k4
poslanci	poslanec	k1gMnPc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
na	na	k7c4
starosti	starost	k1gFnPc4
přípravné	přípravný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
s	s	k7c7
ustavením	ustavení	k1gNnSc7
budoucí	budoucí	k2eAgFnSc2d1
částečně	částečně	k6eAd1
autonomní	autonomní	k2eAgFnSc2d1
České	český	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gInSc2
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yRgMnPc3,k3yQgMnPc3
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
právě	právě	k6eAd1
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnPc1
poslanci	poslanec	k1gMnPc1
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1968	#num#	k4
nepřímo	přímo	k6eNd1
Národním	národní	k2eAgNnSc7d1
shromážděním	shromáždění	k1gNnSc7
na	na	k7c4
návrh	návrh	k1gInSc4
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
z	z	k7c2
poslanců	poslanec	k1gMnPc2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
zvolených	zvolený	k2eAgMnPc2d1
v	v	k7c6
českých	český	k2eAgInPc6d1
krajích	kraj	k1gInPc6
a	a	k8xC
z	z	k7c2
význačných	význačný	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
českého	český	k2eAgInSc2d1
veřejného	veřejný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
její	její	k3xOp3gMnSc1
předseda	předseda	k1gMnSc1
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
místopředsedové	místopředseda	k1gMnPc1
<g/>
,	,	kIx,
předsednictvo	předsednictvo	k1gNnSc1
a	a	k8xC
výbory	výbor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
sídlila	sídlit	k5eAaImAgFnS
v	v	k7c6
Thunovském	Thunovský	k2eAgInSc6d1
paláci	palác	k1gInSc6
ve	v	k7c6
Sněmovní	sněmovní	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
<g/>
,	,	kIx,
sídle	sídlo	k1gNnSc6
bývalého	bývalý	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
i	i	k8xC
sídle	sídlo	k1gNnSc6
budoucí	budoucí	k2eAgFnSc2d1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Volby	volba	k1gFnPc1
do	do	k7c2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1969	#num#	k4
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1992	#num#	k4
se	se	k3xPyFc4
pak	pak	k6eAd1
už	už	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
č.	č.	k?
143	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Československé	československý	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
<g/>
,	,	kIx,
jednalo	jednat	k5eAaImAgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
formálně	formálně	k6eAd1
federalizované	federalizovaný	k2eAgInPc1d1
ČSSR	ČSSR	kA
(	(	kIx(
<g/>
později	pozdě	k6eAd2
ČSFR	ČSFR	kA
<g/>
)	)	kIx)
o	o	k7c4
parlament	parlament	k1gInSc4
České	český	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
200	#num#	k4
poslanci	poslanec	k1gMnPc7
volenými	volený	k2eAgMnPc7d1
na	na	k7c6
období	období	k1gNnSc3
nejdříve	dříve	k6eAd3
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
po	po	k7c6
novelizaci	novelizace	k1gFnSc6
ústavním	ústavní	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
č.	č.	k?
43	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
pěti	pět	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosavadní	dosavadní	k2eAgInPc1d1
ČNR	ČNR	kA
byla	být	k5eAaImAgNnP
kooptací	kooptace	k1gFnPc2
doplněna	doplnit	k5eAaPmNgFnS
o	o	k7c4
dalších	další	k2eAgInPc2d1
50	#num#	k4
poslanců	poslanec	k1gMnPc2
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1968	#num#	k4
<g/>
,	,	kIx,
další	další	k2eAgNnPc1d1
<g/>
,	,	kIx,
už	už	k6eAd1
přímé	přímý	k2eAgFnPc1d1
volby	volba	k1gFnPc1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
1971	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
a	a	k8xC
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Postavení	postavení	k1gNnSc1
ČNR	ČNR	kA
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
volila	volit	k5eAaImAgFnS
a	a	k8xC
odvolávala	odvolávat	k5eAaImAgFnS
své	svůj	k3xOyFgNnSc4
předsednictvo	předsednictvo	k1gNnSc4
<g/>
,	,	kIx,
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
z	z	k7c2
předsedy	předseda	k1gMnSc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
místopředsedů	místopředseda	k1gMnPc2
a	a	k8xC
z	z	k7c2
dalších	další	k2eAgInPc2d1
členů	člen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsednictvo	předsednictvo	k1gNnSc1
fungovalo	fungovat	k5eAaImAgNnS
nepřetržitě	přetržitě	k6eNd1
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
rozpuštění	rozpuštění	k1gNnSc1
ČNR	ČNR	kA
bylo	být	k5eAaImAgNnS
oprávněno	oprávněn	k2eAgNnSc1d1
vykonávat	vykonávat	k5eAaImF
její	její	k3xOp3gFnPc4
pravomoce	pravomoc	k1gFnPc4
<g/>
,	,	kIx,
vyjma	vyjma	k7c2
přijímání	přijímání	k1gNnSc2
ústavních	ústavní	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
a	a	k8xC
rozpočtu	rozpočet	k1gInSc2
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namísto	namísto	k7c2
zákonů	zákon	k1gInPc2
mohlo	moct	k5eAaImAgNnS
v	v	k7c6
takové	takový	k3xDgFnSc6
době	doba	k1gFnSc6
vydávat	vydávat	k5eAaImF,k5eAaPmF
tzv.	tzv.	kA
zákonná	zákonný	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
se	se	k3xPyFc4
vyhlašovala	vyhlašovat	k5eAaImAgFnS
stejně	stejně	k6eAd1
jako	jako	k9
zákony	zákon	k1gInPc1
a	a	k8xC
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
na	na	k7c6
nejbližším	blízký	k2eAgNnSc6d3
zasedání	zasedání	k1gNnSc6
ČNR	ČNR	kA
schválena	schválit	k5eAaPmNgNnP
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
pozbývala	pozbývat	k5eAaImAgFnS
další	další	k2eAgFnPc4d1
platnosti	platnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatnou	podstatný	k2eAgFnSc7d1
pravomocí	pravomoc	k1gFnSc7
předsednictva	předsednictvo	k1gNnSc2
bylo	být	k5eAaImAgNnS
jmenování	jmenování	k1gNnSc4
a	a	k8xC
odvolávání	odvolávání	k1gNnSc4
předsedy	předseda	k1gMnSc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
členů	člen	k1gMnPc2
vlády	vláda	k1gFnSc2
republiky	republika	k1gFnSc2
a	a	k8xC
pověřování	pověřování	k1gNnSc2
je	být	k5eAaImIp3nS
řízením	řízení	k1gNnSc7
republikových	republikový	k2eAgNnPc2d1
ministerstev	ministerstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlašovalo	vyhlašovat	k5eAaImAgNnS
také	také	k6eAd1
volby	volba	k1gFnSc2
do	do	k7c2
ČNR	ČNR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
předseda	předseda	k1gMnSc1
pak	pak	k6eAd1
zastupoval	zastupovat	k5eAaImAgMnS
Českou	český	k2eAgFnSc4d1
národní	národní	k2eAgFnSc4d1
radu	rada	k1gFnSc4
navenek	navenek	k6eAd1
<g/>
,	,	kIx,
svolával	svolávat	k5eAaImAgMnS
a	a	k8xC
řídil	řídit	k5eAaImAgMnS
její	její	k3xOp3gNnPc4
zasedání	zasedání	k1gNnPc4
<g/>
,	,	kIx,
podepisoval	podepisovat	k5eAaImAgMnS
zákony	zákon	k1gInPc4
a	a	k8xC
přijímal	přijímat	k5eAaImAgInS
sliby	slib	k1gInPc4
členů	člen	k1gMnPc2
republikové	republikový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
předsednictva	předsednictvo	k1gNnSc2
si	se	k3xPyFc3
ČNR	ČNR	kA
zřizovala	zřizovat	k5eAaImAgFnS
i	i	k9
různé	různý	k2eAgInPc1d1
výbory	výbor	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
ústavně-právní	ústavně-právní	k2eAgFnSc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
své	svůj	k3xOyFgInPc4
iniciativní	iniciativní	k2eAgInPc4d1
a	a	k8xC
kontrolní	kontrolní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
mohla	moct	k5eAaImAgFnS
vydávat	vydávat	k5eAaPmF,k5eAaImF
vlastní	vlastní	k2eAgInPc4d1
zákony	zákon	k1gInPc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
nešlo	jít	k5eNaImAgNnS
o	o	k7c4
věci	věc	k1gFnPc4
patřící	patřící	k2eAgFnPc4d1
do	do	k7c2
kompetencí	kompetence	k1gFnPc2
celé	celý	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
zákona	zákon	k1gInSc2
mohli	moct	k5eAaImAgMnP
podávat	podávat	k5eAaImF
jak	jak	k6eAd1
poslanci	poslanec	k1gMnPc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
celé	celý	k2eAgInPc1d1
výbory	výbor	k1gInPc1
i	i	k8xC
vláda	vláda	k1gFnSc1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
byla	být	k5eAaImAgFnS
usnášeníschopná	usnášeníschopný	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
byla	být	k5eAaImAgFnS
přítomna	přítomen	k2eAgFnSc1d1
nadpoloviční	nadpoloviční	k2eAgFnSc1d1
většina	většina	k1gFnSc1
všech	všecek	k3xTgMnPc2
poslanců	poslanec	k1gMnPc2
<g/>
,	,	kIx,
k	k	k7c3
přijetí	přijetí	k1gNnSc3
zákona	zákon	k1gInSc2
bylo	být	k5eAaImAgNnS
pak	pak	k6eAd1
potřeba	potřeba	k6eAd1
souhlasu	souhlas	k1gInSc2
nadpoloviční	nadpoloviční	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
přítomných	přítomný	k2eAgMnPc2d1
<g/>
,	,	kIx,
u	u	k7c2
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
třípětinové	třípětinový	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
všech	všecek	k3xTgMnPc2
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijatý	přijatý	k2eAgInSc4d1
zákon	zákon	k1gInSc4
podepisoval	podepisovat	k5eAaImAgMnS
předseda	předseda	k1gMnSc1
ČNR	ČNR	kA
a	a	k8xC
předseda	předseda	k1gMnSc1
republikové	republikový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
a	a	k8xC
vyhlašovalo	vyhlašovat	k5eAaImAgNnS
je	být	k5eAaImIp3nS
předsednictvo	předsednictvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
zatímco	zatímco	k8xS
v	v	k7c6
letech	let	k1gInPc6
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
33	#num#	k4
zákonů	zákon	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
následujícím	následující	k2eAgNnSc6d1
období	období	k1gNnSc6
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
už	už	k6eAd1
jen	jen	k9
97	#num#	k4
zákonů	zákon	k1gInPc2
<g/>
,	,	kIx,
především	především	k6eAd1
každoročních	každoroční	k2eAgInPc2d1
státních	státní	k2eAgInPc2d1
rozpočtů	rozpočet	k1gInPc2
a	a	k8xC
národohospodářských	národohospodářský	k2eAgInPc2d1
plánů	plán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
mohla	moct	k5eAaImAgFnS
jednat	jednat	k5eAaImF
o	o	k7c6
zásadních	zásadní	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
vnitřní	vnitřní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
schvalovat	schvalovat	k5eAaImF
rozpočet	rozpočet	k1gInSc4
a	a	k8xC
státní	státní	k2eAgInSc4d1
závěrečný	závěrečný	k2eAgInSc4d1
účet	účet	k1gInSc4
republiky	republika	k1gFnSc2
a	a	k8xC
podávat	podávat	k5eAaImF
Federálnímu	federální	k2eAgNnSc3d1
shromáždění	shromáždění	k1gNnSc3
návrhy	návrh	k1gInPc4
celostátních	celostátní	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
normalizace	normalizace	k1gFnSc2
nicméně	nicméně	k8xC
nejčastěji	často	k6eAd3
volila	volit	k5eAaImAgFnS
soudce	soudce	k1gMnSc4
z	z	k7c2
povolání	povolání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
republikové	republikový	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
platilo	platit	k5eAaImAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
ačkoli	ačkoli	k8xS
jejího	její	k3xOp3gMnSc4
předsedu	předseda	k1gMnSc4
a	a	k8xC
další	další	k2eAgInPc4d1
její	její	k3xOp3gInPc4
členy	člen	k1gInPc4
jmenovalo	jmenovat	k5eAaBmAgNnS,k5eAaImAgNnS
předsednictvo	předsednictvo	k1gNnSc1
ČNR	ČNR	kA
<g/>
,	,	kIx,
důvěru	důvěra	k1gFnSc4
musela	muset	k5eAaImAgFnS
vláda	vláda	k1gFnSc1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
jmenování	jmenování	k1gNnSc6
získat	získat	k5eAaPmF
od	od	k7c2
celé	celý	k2eAgFnSc2d1
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
pak	pak	k6eAd1
mohla	moct	k5eAaImAgFnS
činnost	činnost	k1gFnSc4
vlády	vláda	k1gFnSc2
kontrolovat	kontrolovat	k5eAaImF
<g/>
,	,	kIx,
rušit	rušit	k5eAaImF
její	její	k3xOp3gNnSc4
protizákonné	protizákonný	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
či	či	k8xC
usnesení	usnesení	k1gNnSc4
<g/>
,	,	kIx,
interpelovat	interpelovat	k5eAaBmF
ji	on	k3xPp3gFnSc4
i	i	k9
jednotlivé	jednotlivý	k2eAgInPc4d1
její	její	k3xOp3gInPc4
členy	člen	k1gInPc4
a	a	k8xC
mohla	moct	k5eAaImAgFnS
jí	jíst	k5eAaImIp3nS
také	také	k9
vyslovit	vyslovit	k5eAaPmF
nedůvěru	nedůvěra	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
následovala	následovat	k5eAaImAgFnS
demise	demise	k1gFnSc1
celé	celý	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
mohla	moct	k5eAaImAgFnS
vyslovit	vyslovit	k5eAaPmF
nedůvěru	nedůvěra	k1gFnSc4
i	i	k9
jen	jen	k9
jednomu	jeden	k4xCgNnSc3
členovi	člen	k1gMnSc3
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Politické	politický	k2eAgFnPc1d1
změny	změna	k1gFnPc1
v	v	k7c6
letech	léto	k1gNnPc6
1989	#num#	k4
a	a	k8xC
1990	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1989	#num#	k4
rezignovali	rezignovat	k5eAaBmAgMnP
předseda	předseda	k1gMnSc1
<g/>
,	,	kIx,
místopředsedové	místopředseda	k1gMnPc1
<g/>
,	,	kIx,
předsedové	předseda	k1gMnPc1
výborů	výbor	k1gInPc2
a	a	k8xC
další	další	k2eAgMnSc1d1
členové	člen	k1gMnPc1
předsednictva	předsednictvo	k1gNnSc2
ČNR	ČNR	kA
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Josef	Josef	k1gMnSc1
Kempný	Kempný	k2eAgMnSc1d1
svou	svůj	k3xOyFgFnSc7
rezignaci	rezignace	k1gFnSc6
poslal	poslat	k5eAaPmAgMnS
dopisem	dopis	k1gInSc7
ČNR	ČNR	kA
datovaným	datovaný	k2eAgFnPc3d1
už	už	k6eAd1
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nová	k1gFnSc6
vedení	vedení	k1gNnSc1
ČNR	ČNR	kA
bylo	být	k5eAaImAgNnS
zvoleno	zvolen	k2eAgNnSc1d1
ještě	ještě	k6eAd1
tentýž	týž	k3xTgInSc4
den	den	k1gInSc4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
novým	nový	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
Jaroslav	Jaroslav	k1gMnSc1
Šafařík	Šafařík	k1gMnSc1
(	(	kIx(
<g/>
člen	člen	k1gMnSc1
ČSS	ČSS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
z	z	k7c2
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
14	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
odvolávání	odvolávání	k1gNnSc6
poslanců	poslanec	k1gMnPc2
zastupitelských	zastupitelský	k2eAgInPc2d1
sborů	sbor	k1gInPc2
a	a	k8xC
volbě	volba	k1gFnSc3
nových	nový	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
,	,	kIx,
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
odvoláni	odvolat	k5eAaPmNgMnP
svou	svůj	k3xOyFgFnSc7
politickou	politický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
orgánem	orgán	k1gInSc7
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
v	v	k7c6
dohodě	dohoda	k1gFnSc6
s	s	k7c7
Občanským	občanský	k2eAgNnSc7d1
fórem	fórum	k1gNnSc7
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
poslanci	poslanec	k1gMnPc1
ČNR	ČNR	kA
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
v	v	k7c6
zájmu	zájem	k1gInSc6
vyrovnání	vyrovnání	k1gNnSc2
rozložení	rozložení	k1gNnSc2
politických	politický	k2eAgFnPc2d1
sil	síla	k1gFnPc2
nebo	nebo	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
dosavadnímu	dosavadní	k2eAgNnSc3d1
působení	působení	k1gNnSc3
neskýtali	skýtat	k5eNaImAgMnP
záruky	záruka	k1gFnPc4
rozvoje	rozvoj	k1gInSc2
politické	politický	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
do	do	k7c2
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1990	#num#	k4
rezignovalo	rezignovat	k5eAaBmAgNnS
46	#num#	k4
poslanců	poslanec	k1gMnPc2
a	a	k8xC
18	#num#	k4
jich	on	k3xPp3gFnPc2
bylo	být	k5eAaImAgNnS
odvoláno	odvolat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
bylo	být	k5eAaImAgNnS
zvoleno	zvolit	k5eAaPmNgNnS
nových	nový	k2eAgMnPc2d1
64	#num#	k4
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předsedové	předseda	k1gMnPc1
ČNR	ČNR	kA
</s>
<s>
1968	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
Čestmír	Čestmír	k1gMnSc1
Císař	Císař	k1gMnSc1
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
Evžen	Evžen	k1gMnSc1
Erban	Erban	k1gMnSc1
</s>
<s>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
Josef	Josefa	k1gFnPc2
Kempný	Kempný	k2eAgMnSc1d1
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Šafařík	Šafařík	k1gMnSc1
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
Dagmar	Dagmar	k1gFnSc1
Burešová	Burešová	k1gFnSc1
</s>
<s>
1992	#num#	k4
Milan	Milan	k1gMnSc1
Uhde	Uhde	k1gFnSc4
</s>
<s>
Složení	složení	k1gNnSc1
</s>
<s>
Z	z	k7c2
prvních	první	k4xOgInPc2
150	#num#	k4
poslanců	poslanec	k1gMnPc2
ČNR	ČNR	kA
bylo	být	k5eAaImAgNnS
85	#num#	k4
členů	člen	k1gMnPc2
KSČ	KSČ	kA
<g/>
,	,	kIx,
22	#num#	k4
ČSS	ČSS	kA
<g/>
,	,	kIx,
22	#num#	k4
ČSL	ČSL	kA
a	a	k8xC
21	#num#	k4
nestranických	stranický	k2eNgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Volby	volba	k1gFnPc1
do	do	k7c2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
33	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
124	#num#	k4
</s>
<s>
KSČ	KSČ	kA
</s>
<s>
ČSL	ČSL	kA
</s>
<s>
HSD-SMS	HSD-SMS	k?
</s>
<s>
OF	OF	kA
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
1992	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Volby	volba	k1gFnPc1
do	do	k7c2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
35	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
66	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
LB	LB	kA
</s>
<s>
ČSSD	ČSSD	kA
</s>
<s>
LSU	LSU	kA
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
</s>
<s>
HSD-SMS	HSD-SMS	k?
</s>
<s>
ODA	ODA	kA
</s>
<s>
KDS	KDS	kA
</s>
<s>
ODS	ODS	kA
</s>
<s>
SPR-RSČ	SPR-RSČ	k?
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
čl	čl	kA
<g/>
.	.	kIx.
151	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
č.	č.	k?
143	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
československé	československý	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
<g/>
↑	↑	k?
čl	čl	kA
<g/>
.	.	kIx.
102	#num#	k4
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
č.	č.	k?
143	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
↑	↑	k?
http://tf.webz.cz/jw/cnr.rtf	http://tf.webz.cz/jw/cnr.rtf	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
2	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
poslední	poslední	k2eAgInSc1d1
odstavec	odstavec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
členů	člen	k1gMnPc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
členů	člen	k1gMnPc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
členů	člen	k1gMnPc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
členů	člen	k1gMnPc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
členů	člen	k1gMnPc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
členů	člen	k1gMnPc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
členů	člen	k1gMnPc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Česká	český	k2eAgNnPc1d1
politická	politický	k2eAgNnPc1d1
zastoupení	zastoupení	k1gNnPc1
od	od	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
Rakouské	rakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
<g/>
,	,	kIx,
<g/>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
</s>
<s>
Ústavodarná	Ústavodarný	k2eAgFnSc1d1
rakouská	rakouský	k2eAgFnSc1d1
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
•	•	k?
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
:	:	kIx,
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
a	a	k8xC
Panská	panský	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
</s>
<s>
České	český	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
Království	království	k1gNnSc2
českého	český	k2eAgNnSc2d1
<g/>
,	,	kIx,
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
Markrabství	markrabství	k1gNnSc2
moravského	moravský	k2eAgNnSc2d1
<g/>
,	,	kIx,
slezský	slezský	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Revoluční	revoluční	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
•	•	k?
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
<g/>
:	:	kIx,
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
a	a	k8xC
Senát	senát	k1gInSc1
•	•	k?
Prozatímní	prozatímní	k2eAgNnSc1d1
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
•	•	k?
Ústavodárné	ústavodárný	k2eAgNnSc1d1
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
•	•	k?
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
•	•	k?
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
ČSSR	ČSSR	kA
•	•	k?
Federální	federální	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
ČSSR	ČSSR	kA
<g/>
/	/	kIx~
<g/>
ČSFR	ČSFR	kA
<g/>
:	:	kIx,
Sněmovna	sněmovna	k1gFnSc1
lidu	lid	k1gInSc2
a	a	k8xC
Sněmovna	sněmovna	k1gFnSc1
národů	národ	k1gInPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
a	a	k8xC
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Parlament	parlament	k1gInSc1
ČR	ČR	kA
<g/>
:	:	kIx,
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
a	a	k8xC
Senát	senát	k1gInSc1
EU	EU	kA
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
•	•	k?
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
•	•	k?
Rada	rada	k1gFnSc1
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
|	|	kIx~
Právo	právo	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
91024336	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
142391727	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
91024336	#num#	k4
</s>
