<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
zkratkou	zkratka	k1gFnSc7
ČNR	ČNR	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1969	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1992	#num#	k4
zákonodárným	zákonodárný	k2eAgInSc7d1
sborem	sbor	k1gInSc7
České	český	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>