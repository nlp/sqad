<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Třeboňsko	Třeboňsko	k1gNnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
na	na	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
a	a	k8xC
částmi	část	k1gFnPc7
okresů	okres	k1gInPc2
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
a	a	k8xC
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
na	na	k7c6
ploše	plocha	k1gFnSc6
700	#num#	k4
km²	km²	k?
a	a	k8xC
je	být	k5eAaImIp3nS
umístěno	umístit	k5eAaPmNgNnS
na	na	k7c6
ploché	plochý	k2eAgFnSc6d1
<g/>
,	,	kIx,
rovinaté	rovinatý	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nejdříve	dříve	k6eAd3
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
zařazena	zařadit	k5eAaPmNgFnS
mezi	mezi	k7c4
biosférické	biosférický	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
UNESCO	UNESCO	kA
v	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
Člověk	člověk	k1gMnSc1
a	a	k8xC
biosféra	biosféra	k1gFnSc1
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
chráněnou	chráněný	k2eAgFnSc7d1
krajinnou	krajinný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
<g/>
.	.	kIx.
</s>