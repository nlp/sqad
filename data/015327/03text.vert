<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Třeboňsko	Třeboňsko	k1gNnSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
TřeboňskoIUCN	TřeboňskoIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
leží	ležet	k5eAaImIp3nS
např.	např.	kA
NPR	NPR	kA
Červené	Červené	k2eAgInSc4d1
blatoZákladní	blatoZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1979	#num#	k4
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
421	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
700	#num#	k4
km²	km²	k?
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Třeboňsko	Třeboňsko	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
0,62	0,62	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
7,79	7,79	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Třeboňsko	Třeboňsko	k1gNnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
32	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.trebonsko.ochranaprirody.cz	www.trebonsko.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Rybník	rybník	k1gInSc1
Rožmberk	Rožmberk	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Třeboňsko	Třeboňsko	k1gNnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
na	na	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
a	a	k8xC
částmi	část	k1gFnPc7
okresů	okres	k1gInPc2
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
a	a	k8xC
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
na	na	k7c6
ploše	plocha	k1gFnSc6
700	#num#	k4
km²	km²	k?
a	a	k8xC
je	být	k5eAaImIp3nS
umístěno	umístit	k5eAaPmNgNnS
na	na	k7c6
ploché	plochý	k2eAgFnSc6d1
<g/>
,	,	kIx,
rovinaté	rovinatý	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nejdříve	dříve	k6eAd3
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
zařazena	zařadit	k5eAaPmNgFnS
mezi	mezi	k7c4
biosférické	biosférický	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
UNESCO	UNESCO	kA
v	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
Člověk	člověk	k1gMnSc1
a	a	k8xC
biosféra	biosféra	k1gFnSc1
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
chráněnou	chráněný	k2eAgFnSc7d1
krajinnou	krajinný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
příklad	příklad	k1gInSc4
ochrany	ochrana	k1gFnSc2
kulturní	kulturní	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
výrazně	výrazně	k6eAd1
pozměněné	pozměněný	k2eAgNnSc1d1
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
přesto	přesto	k8xC
významnou	významný	k2eAgFnSc4d1
ochranářskou	ochranářský	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
dokazuje	dokazovat	k5eAaImIp3nS
i	i	k9
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
bylo	být	k5eAaImAgNnS
celé	celý	k2eAgNnSc1d1
území	území	k1gNnSc1
vyhlášeno	vyhlásit	k5eAaPmNgNnS
biosférickou	biosférický	k2eAgFnSc7d1
rezervací	rezervace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třeboňsko	Třeboňsko	k1gNnSc1
je	být	k5eAaImIp3nS
také	také	k9
ptačí	ptačí	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
a	a	k8xC
významným	významný	k2eAgNnSc7d1
ptačím	ptačí	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
v	v	k7c6
Třeboni	Třeboň	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
i	i	k9
přirozeným	přirozený	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejcennější	cenný	k2eAgFnPc1d3
lokality	lokalita	k1gFnPc1
jsou	být	k5eAaImIp3nP
chráněny	chránit	k5eAaImNgFnP
v	v	k7c6
rámci	rámec	k1gInSc6
33	#num#	k4
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
a	a	k8xC
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
má	mít	k5eAaImIp3nS
5	#num#	k4
území	území	k1gNnPc2
status	status	k1gInSc1
národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Přestože	přestože	k8xS
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
krajina	krajina	k1gFnSc1
dlouhodobě	dlouhodobě	k6eAd1
kultivována	kultivován	k2eAgFnSc1d1
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
zachovala	zachovat	k5eAaPmAgFnS
se	se	k3xPyFc4
zde	zde	k6eAd1
mimořádně	mimořádně	k6eAd1
cenná	cenný	k2eAgFnSc1d1
vegetace	vegetace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
utváření	utváření	k1gNnSc6
krajiny	krajina	k1gFnSc2
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
podílel	podílet	k5eAaImAgMnS
zejména	zejména	k9
úpravami	úprava	k1gFnPc7
vodních	vodní	k2eAgFnPc2d1
poměrů	poměr	k1gInPc2
původní	původní	k2eAgFnSc2d1
močálovité	močálovitý	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
důmyslná	důmyslný	k2eAgFnSc1d1
síť	síť	k1gFnSc1
umělých	umělý	k2eAgFnPc2d1
stok	stoka	k1gFnPc2
a	a	k8xC
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
rybníků	rybník	k1gInPc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yQgFnPc3,k3yRgFnPc3,k3yIgFnPc3
je	být	k5eAaImIp3nS
Třeboňsko	Třeboňsko	k1gNnSc1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
centrum	centrum	k1gNnSc4
českého	český	k2eAgNnSc2d1
rybníkářství	rybníkářství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1
a	a	k8xC
průmysl	průmysl	k1gInSc1
</s>
<s>
Třeboňsko	Třeboňsko	k1gNnSc1
má	mít	k5eAaImIp3nS
převážně	převážně	k6eAd1
zemědělský	zemědělský	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěstují	pěstovat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
obiloviny	obilovina	k1gFnPc1
<g/>
,	,	kIx,
olejniny	olejnina	k1gFnPc1
a	a	k8xC
pícniny	pícnina	k1gFnPc1
<g/>
,	,	kIx,
významná	významný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
též	též	k9
produkce	produkce	k1gFnSc1
brambor	brambora	k1gFnPc2
a	a	k8xC
chová	chovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
především	především	k6eAd1
skot	skot	k1gInSc1
a	a	k8xC
prasata	prase	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouholetou	dlouholetý	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
má	mít	k5eAaImIp3nS
v	v	k7c6
kraji	kraj	k1gInSc6
rybníkářství	rybníkářství	k1gNnSc2
a	a	k8xC
také	také	k9
lesnictví	lesnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těží	těžet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
písek	písek	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Suchdolska	Suchdolsko	k1gNnSc2
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
i	i	k9
zpracovává	zpracovávat	k5eAaImIp3nS
<g/>
,	,	kIx,
také	také	k9
se	se	k3xPyFc4
zde	zde	k6eAd1
na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
těží	těžet	k5eAaImIp3nS
rašelina	rašelina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Jakub	Jakub	k1gMnSc1
Krčín	Krčín	k1gMnSc1
</s>
<s>
První	první	k4xOgFnPc1
zmínky	zmínka	k1gFnPc1
o	o	k7c4
osídlení	osídlení	k1gNnSc4
oblasti	oblast	k1gFnSc2
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
malá	malý	k2eAgFnSc1d1
osada	osada	k1gFnSc1
(	(	kIx(
<g/>
Vítkův	Vítkův	k2eAgInSc1d1
Luh	luh	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
získal	získat	k5eAaPmAgMnS
Vítek	Vítek	k1gMnSc1
z	z	k7c2
Prčic	Prčice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
této	tento	k3xDgFnSc2
osady	osada	k1gFnSc2
později	pozdě	k6eAd2
vzniklo	vzniknout	k5eAaPmAgNnS
město	město	k1gNnSc1
Třeboň	Třeboň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
významné	významný	k2eAgNnSc4d1
století	století	k1gNnSc4
pro	pro	k7c4
Třeboňsko	Třeboňsko	k1gNnSc4
je	být	k5eAaImIp3nS
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	s	k7c7
vlastníky	vlastník	k1gMnPc7
Třeboně	Třeboň	k1gFnSc2
stali	stát	k5eAaPmAgMnP
Rožmberkové	Rožmberkové	k?
a	a	k8xC
Třeboni	Třeboň	k1gFnSc3
bylo	být	k5eAaImAgNnS
uděleno	udělen	k2eAgNnSc1d1
právo	právo	k1gNnSc1
měst	město	k1gNnPc2
královských	královský	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
začaly	začít	k5eAaPmAgFnP
být	být	k5eAaImF
zakládány	zakládán	k2eAgInPc4d1
první	první	k4xOgInPc4
rybníky	rybník	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazný	výrazný	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
rybníkářství	rybníkářství	k1gNnSc2
nastal	nastat	k5eAaPmAgInS
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
za	za	k7c2
vlády	vláda	k1gFnSc2
Petra	Petr	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
pro	pro	k7c4
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
sloužil	sloužit	k5eAaImAgMnS
Josef	Josef	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
Netolický	netolický	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
jako	jako	k9
první	první	k4xOgFnSc7
vložil	vložit	k5eAaPmAgMnS
do	do	k7c2
budování	budování	k1gNnSc2
rybniční	rybniční	k2eAgInSc4d1
systém	systém	k1gInSc4
a	a	k8xC
jehož	jehož	k3xOyRp3gFnSc7
nejslavnější	slavný	k2eAgFnSc7d3
konstrukcí	konstrukce	k1gFnSc7
je	být	k5eAaImIp3nS
umělý	umělý	k2eAgInSc4d1
kanál	kanál	k1gInSc4
Zlatá	zlatý	k2eAgFnSc1d1
stoka	stoka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největšího	veliký	k2eAgInSc2d3
rozkvětu	rozkvět	k1gInSc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
Třeboňsko	Třeboňsko	k1gNnSc1
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
za	za	k7c2
vlády	vláda	k1gFnSc2
posledních	poslední	k2eAgMnPc2d1
Rožmberků	Rožmberk	k1gMnPc2
<g/>
,	,	kIx,
bratrů	bratr	k1gMnPc2
Viléma	Vilém	k1gMnSc2
z	z	k7c2
Rožmberka	Rožmberk	k1gInSc2
(	(	kIx(
<g/>
1535	#num#	k4
–	–	k?
1592	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Petra	Petr	k1gMnSc2
Voka	Vokus	k1gMnSc2
z	z	k7c2
Rožmberka	Rožmberk	k1gInSc2
(	(	kIx(
<g/>
1539	#num#	k4
–	–	k?
1611	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jejich	jejich	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
regent	regent	k1gMnSc1
třeboňského	třeboňský	k2eAgNnSc2d1
panství	panství	k1gNnSc2
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Krčín	Krčín	k1gMnSc1
z	z	k7c2
Jelčan	Jelčan	k1gMnSc1
<g/>
,	,	kIx,
vybudoval	vybudovat	k5eAaPmAgMnS
kromě	kromě	k7c2
četných	četný	k2eAgInPc2d1
rybníků	rybník	k1gInPc2
také	také	k9
vrchnostenské	vrchnostenský	k2eAgInPc4d1
pivovary	pivovar	k1gInPc4
<g/>
,	,	kIx,
zemědělské	zemědělský	k2eAgInPc4d1
dvory	dvůr	k1gInPc4
a	a	k8xC
ovčíny	ovčín	k1gInPc4
<g/>
,	,	kIx,
panské	panský	k2eAgInPc4d1
mlýny	mlýn	k1gInPc4
či	či	k8xC
sklářské	sklářský	k2eAgFnPc4d1
hutě	huť	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poté	poté	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
zažilo	zažít	k5eAaPmAgNnS
město	město	k1gNnSc1
několik	několik	k4yIc4
vojenských	vojenský	k2eAgInPc2d1
útoků	útok	k1gInPc2
<g/>
,	,	kIx,
potom	potom	k6eAd1
co	co	k9
z	z	k7c2
města	město	k1gNnSc2
odešla	odejít	k5eAaPmAgFnS
anglická	anglický	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
a	a	k8xC
na	na	k7c6
celém	celý	k2eAgNnSc6d1
panství	panství	k1gNnSc6
vznikly	vzniknout	k5eAaPmAgFnP
značné	značný	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
ranou	rána	k1gFnSc7
byla	být	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
morová	morový	k2eAgFnSc1d1
epidemie	epidemie	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1640	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
za	za	k7c2
vlády	vláda	k1gFnSc2
Schwarzenbergů	Schwarzenberg	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
město	město	k1gNnSc1
vzpamatovávat	vzpamatovávat	k5eAaImF
z	z	k7c2
válečných	válečný	k2eAgFnPc2d1
ran	rána	k1gFnPc2
a	a	k8xC
z	z	k7c2
četných	četný	k2eAgInPc2d1
požárů	požár	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
zde	zde	k6eAd1
vypukly	vypuknout	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc4
Schwarzenbergů	Schwarzenberg	k1gInPc2
působil	působit	k5eAaImAgMnS
na	na	k7c6
třeboňském	třeboňský	k2eAgNnSc6d1
panství	panství	k1gNnSc6
až	až	k6eAd1
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
na	na	k7c6
Třeboňsku	Třeboňsko	k1gNnSc6
díky	díky	k7c3
důmyslným	důmyslný	k2eAgFnPc3d1
krajinářským	krajinářský	k2eAgFnPc3d1
a	a	k8xC
vodohospodářským	vodohospodářský	k2eAgFnPc3d1
úpravám	úprava	k1gFnPc3
zachovala	zachovat	k5eAaPmAgFnS
spousta	spousta	k1gFnSc1
přírodních	přírodní	k2eAgFnPc2d1
i	i	k8xC
technických	technický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
rybniční	rybniční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příroda	příroda	k1gFnSc1
v	v	k7c6
CHKO	CHKO	kA
</s>
<s>
Mokřady	mokřad	k1gInPc1
</s>
<s>
Třeboňsko	Třeboňsko	k1gNnSc1
vyniká	vynikat	k5eAaImIp3nS
neobyčejnou	obyčejný	k2eNgFnSc7d1
pestrostí	pestrost	k1gFnSc7
biotopů	biotop	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
nejcennějším	cenný	k2eAgInPc3d3
biotopům	biotop	k1gInPc3
Třeboňska	Třeboňsko	k1gNnSc2
patří	patřit	k5eAaImIp3nS
tzv.	tzv.	kA
přechodová	přechodový	k2eAgNnPc1d1
rašeliniště	rašeliniště	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
vzácnými	vzácný	k2eAgNnPc7d1
společenstvy	společenstvo	k1gNnPc7
rostlin	rostlina	k1gFnPc2
a	a	k8xC
na	na	k7c4
ně	on	k3xPp3gMnPc4
navazujícími	navazující	k2eAgFnPc7d1
druhy	druh	k1gInPc1
bezobratlých	bezobratlý	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Druhová	druhový	k2eAgFnSc1d1
pestrost	pestrost	k1gFnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
rozmanitostí	rozmanitost	k1gFnSc7
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
opravdu	opravdu	k6eAd1
veliká	veliký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najdeme	najít	k5eAaPmIp1nP
zde	zde	k6eAd1
kromě	kromě	k7c2
přechodných	přechodný	k2eAgNnPc2d1
rašelinišť	rašeliniště	k1gNnPc2
také	také	k9
rozsáhlé	rozsáhlý	k2eAgInPc4d1
jehličnaté	jehličnatý	k2eAgInPc4d1
a	a	k8xC
listnaté	listnatý	k2eAgInPc4d1
lesy	les	k1gInPc4
<g/>
,	,	kIx,
zbytky	zbytek	k1gInPc4
lužních	lužní	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
,	,	kIx,
teplé	teplý	k2eAgFnSc2d1
stepní	stepní	k2eAgFnSc2d1
lokality	lokalita	k1gFnSc2
<g/>
,	,	kIx,
mokřadní	mokřadní	k2eAgFnPc4d1
louky	louka	k1gFnPc4
a	a	k8xC
rybníky	rybník	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
evropsky	evropsky	k6eAd1
významné	významný	k2eAgInPc1d1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
výskytu	výskyt	k1gInSc2
vodních	vodní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Flora	Flora	k1gFnSc1
</s>
<s>
Roste	růst	k5eAaImIp3nS
zde	zde	k6eAd1
okolo	okolo	k7c2
400	#num#	k4
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
celkem	celkem	k6eAd1
104	#num#	k4
druhů	druh	k1gInPc2
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
chráněné	chráněný	k2eAgFnPc4d1
<g/>
;	;	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
35	#num#	k4
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
34	#num#	k4
silně	silně	k6eAd1
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
a	a	k8xC
34	#num#	k4
kriticky	kriticky	k6eAd1
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lesích	les	k1gInPc6
roste	růst	k5eAaImIp3nS
hlavně	hlavně	k9
borovice	borovice	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
a	a	k8xC
borovice	borovice	k1gFnSc1
blatka	blatka	k1gFnSc1
<g/>
,	,	kIx,
rojovník	rojovník	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
<g/>
,	,	kIx,
kopytník	kopytník	k1gInSc1
evropský	evropský	k2eAgInSc1d1
<g/>
,	,	kIx,
lilie	lilie	k1gFnSc1
zlatohlavá	zlatohlavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
vzácná	vzácný	k2eAgFnSc1d1
vratička	vratička	k1gFnSc1
měsíční	měsíční	k2eAgFnSc1d1
<g/>
,	,	kIx,
hvozdík	hvozdík	k1gInSc1
pyšný	pyšný	k2eAgInSc1d1
<g/>
,	,	kIx,
černýš	černýš	k1gInSc1
hajní	hajní	k2eAgInSc1d1
<g/>
,	,	kIx,
ďáblík	ďáblík	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
<g/>
,	,	kIx,
kapraď	kapraď	k1gFnSc1
hřebenitá	hřebenitý	k2eAgFnSc1d1
nebo	nebo	k8xC
kapradiník	kapradiník	k1gInSc1
bažinný	bažinný	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
loukách	louka	k1gFnPc6
roste	růst	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
vyskytují	vyskytovat	k5eAaImIp3nP
už	už	k6eAd1
jen	jen	k9
málo	málo	k6eAd1
a	a	k8xC
hrozí	hrozit	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
úplné	úplný	k2eAgNnSc1d1
vyhubení	vyhubení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nim	on	k3xPp3gInPc3
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
vstavač	vstavač	k1gInSc4
kukačka	kukačka	k1gFnSc1
<g/>
,	,	kIx,
hlízovec	hlízovec	k1gInSc1
Loeselův	Loeselův	k2eAgInSc1d1
<g/>
,	,	kIx,
blatouch	blatouch	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
či	či	k8xC
hrotnosemenka	hrotnosemenka	k1gFnSc1
bílá	bílý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rašelinné	rašelinný	k2eAgFnPc4d1
louky	louka	k1gFnPc4
se	se	k3xPyFc4
také	také	k6eAd1
vyznačují	vyznačovat	k5eAaImIp3nP
výskytem	výskyt	k1gInSc7
hmyzožravých	hmyzožravý	k2eAgFnPc2d1
rosnatek	rosnatka	k1gFnPc2
okrouhlolistých	okrouhlolistý	k2eAgFnPc2d1
a	a	k8xC
také	také	k9
dlouholistých	dlouholistý	k2eAgFnPc2d1
a	a	k8xC
prostředních	prostřední	k2eAgFnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
území	území	k1gNnSc6
rašeliny	rašelina	k1gFnSc2
také	také	k9
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
vzácné	vzácný	k2eAgInPc1d1
druhy	druh	k1gInPc1
mechů	mech	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
rašeliník	rašeliník	k1gInSc1
tupolistý	tupolistý	k2eAgMnSc1d1
anebo	anebo	k8xC
plstnatec	plstnatec	k1gMnSc1
rašelinný	rašelinný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typické	typický	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
rostliny	rostlina	k1gFnPc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
vřesovcovitých	vřesovcovitý	k2eAgInPc2d1
–	–	k?
rojovník	rojovník	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
<g/>
,	,	kIx,
vlochyně	vlochyně	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
<g/>
,	,	kIx,
brusinka	brusinka	k1gFnSc1
obecná	obecná	k1gFnSc1
či	či	k8xC
klikva	klikva	k1gFnSc1
žoravina	žoravina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mnoho	mnoho	k4c1
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
se	se	k3xPyFc4
také	také	k9
vyskytuje	vyskytovat	k5eAaImIp3nS
mezi	mezi	k7c7
rostlinami	rostlina	k1gFnPc7
stojatých	stojatý	k2eAgFnPc2d1
a	a	k8xC
tekoucích	tekoucí	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
vzácný	vzácný	k2eAgInSc4d1
stulík	stulík	k1gInSc4
malý	malý	k2eAgInSc4d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
stulík	stulík	k1gInSc1
žlutý	žlutý	k2eAgInSc1d1
či	či	k8xC
leknín	leknín	k1gInSc1
bílý	bílý	k2eAgInSc1d1
a	a	k8xC
také	také	k9
rdest	rdest	k1gInSc1
alpský	alpský	k2eAgInSc1d1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
patří	patřit	k5eAaImIp3nS
do	do	k7c2
kategorie	kategorie	k1gFnSc2
silně	silně	k6eAd1
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Díky	díky	k7c3
rozsáhlým	rozsáhlý	k2eAgInPc3d1
rybníkům	rybník	k1gInPc3
<g/>
,	,	kIx,
močálům	močál	k1gInPc3
nebo	nebo	k8xC
mokřadům	mokřad	k1gInPc3
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
spousta	spousta	k1gFnSc1
vodních	vodní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
jako	jako	k9
hohol	hohol	k1gMnSc1
severní	severní	k2eAgMnSc1d1
<g/>
,	,	kIx,
husa	husa	k1gFnSc1
velká	velká	k1gFnSc1
<g/>
,	,	kIx,
labuť	labuť	k1gFnSc1
<g/>
,	,	kIx,
bukač	bukač	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
kvakoš	kvakoš	k1gMnSc1
noční	noční	k2eAgMnSc1d1
<g/>
,	,	kIx,
volavka	volavka	k1gFnSc1
popelavá	popelavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
orel	orel	k1gMnSc1
mořský	mořský	k2eAgMnSc1d1
<g/>
,	,	kIx,
volavka	volavka	k1gFnSc1
červená	červená	k1gFnSc1
<g/>
,	,	kIx,
volavka	volavka	k1gFnSc1
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
čejka	čejka	k1gFnSc1
<g/>
,	,	kIx,
čáp	čáp	k1gMnSc1
bílý	bílý	k1gMnSc1
<g/>
,	,	kIx,
sluka	sluka	k1gFnSc1
otavní	otavní	k2eAgMnPc1d1
a	a	k8xC
další	další	k2eAgMnPc1d1
druhy	druh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Včetně	včetně	k7c2
vodních	vodní	k2eAgInPc2d1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
celkem	celek	k1gInSc7
280	#num#	k4
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
z	z	k7c2
toho	ten	k3xDgNnSc2
také	také	k9
osm	osm	k4xCc4
druhů	druh	k1gInPc2
sov	sova	k1gFnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
například	například	k6eAd1
výr	výr	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
sýc	sýc	k1gMnSc1
rousný	rousný	k2eAgMnSc1d1
a	a	k8xC
kulíšek	kulíšek	k1gMnSc1
nejmenší	malý	k2eAgMnSc1d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Labutě	labutě	k1gNnSc1
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
okolo	okolo	k7c2
50	#num#	k4
druhů	druh	k1gInPc2
savců	savec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzácným	vzácný	k2eAgInSc7d1
druhem	druh	k1gInSc7
je	být	k5eAaImIp3nS
chráněná	chráněný	k2eAgFnSc1d1
vydra	vydra	k1gFnSc1
říční	říční	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
dokonce	dokonce	k9
i	i	k9
los	los	k1gInSc1
evropský	evropský	k2eAgInSc1d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
sem	sem	k6eAd1
dostal	dostat	k5eAaPmAgMnS
při	při	k7c6
migraci	migrace	k1gFnSc6
z	z	k7c2
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
relativně	relativně	k6eAd1
málo	málo	k4c4
druhů	druh	k1gInPc2
plazů	plaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejohroženějším	ohrožený	k2eAgInSc7d3
druhem	druh	k1gInSc7
je	být	k5eAaImIp3nS
užovka	užovka	k1gFnSc1
hladká	hladký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickým	typický	k2eAgInSc7d1
druhem	druh	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zmije	zmije	k1gFnSc1
obecná	obecná	k1gFnSc1
a	a	k8xC
slepýš	slepýš	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
vodním	vodní	k2eAgInPc3d1
ekosystémům	ekosystém	k1gInPc3
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
hojný	hojný	k2eAgInSc1d1
počet	počet	k1gInSc1
obojživelníků	obojživelník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
tu	tu	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
12	#num#	k4
druhů	druh	k1gInPc2
a	a	k8xC
mezi	mezi	k7c4
nejvzácnější	vzácný	k2eAgFnPc4d3
patří	patřit	k5eAaImIp3nS
ropucha	ropucha	k1gFnSc1
krátkonohá	krátkonohý	k2eAgFnSc1d1
<g/>
,	,	kIx,
čolek	čolek	k1gMnSc1
velký	velký	k2eAgMnSc1d1
a	a	k8xC
kuňka	kuňka	k1gFnSc1
ohnivá	ohnivý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oblasti	oblast	k1gFnPc1
CHKO	CHKO	kA
</s>
<s>
CHKO	CHKO	kA
má	mít	k5eAaImIp3nS
čtyři	čtyři	k4xCgFnPc4
národní	národní	k2eAgFnPc4d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
(	(	kIx(
<g/>
NPR	NPR	kA
Velký	velký	k2eAgMnSc1d1
a	a	k8xC
Malý	malý	k2eAgMnSc1d1
Tisý	Tisý	k1gMnSc1
u	u	k7c2
Lomnice	Lomnice	k1gFnSc2
nad	nad	k7c7
Lužnicí	Lužnice	k1gFnSc7
<g/>
,	,	kIx,
Stará	starý	k2eAgFnSc1d1
a	a	k8xC
Nová	nový	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
nedaleko	nedaleko	k7c2
Třeboně	Třeboň	k1gFnSc2
<g/>
,	,	kIx,
NPR	NPR	kA
Červené	červené	k1gNnSc1
blato	blato	k1gNnSc1
a	a	k8xC
NPR	NPR	kA
Žofínka	Žofínka	k1gFnSc1
a	a	k8xC
dvě	dva	k4xCgFnPc1
národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
(	(	kIx(
<g/>
NPP	NPP	kA
Ruda	ruda	k1gFnSc1
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Horusického	horusický	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
a	a	k8xC
NPP	NPP	kA
Vizír	vizír	k1gInSc1
poblíž	poblíž	k7c2
Hamru	hamr	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
zřízeny	zřízen	k2eAgFnPc1d1
i	i	k8xC
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k9
PR	pr	k0
Bukové	bukový	k2eAgFnPc1d1
kopce	kopec	k1gInPc4
nedaleko	nedaleko	k7c2
Chlumu	chlum	k1gInSc2
u	u	k7c2
Třeboně	Třeboň	k1gFnSc2
<g/>
,	,	kIx,
PR	pr	k0
Horní	horní	k2eAgFnSc1d1
Lužnice	Lužnice	k1gFnSc1
na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
Lužnice	Lužnice	k1gFnSc2
u	u	k7c2
Dvorů	Dvůr	k1gInPc2
nad	nad	k7c7
Lužnicí	Lužnice	k1gFnSc7
<g/>
,	,	kIx,
PR	pr	k0
Písečný	písečný	k2eAgInSc1d1
přesyp	přesyp	k1gInSc4
u	u	k7c2
Vlkova	Vlkův	k2eAgMnSc2d1
nedaleko	nedaleko	k7c2
Veselí	veselí	k1gNnSc2
nad	nad	k7c7
Lužnicí	Lužnice	k1gFnSc7
nebo	nebo	k8xC
PR	pr	k0
Záblatské	záblatský	k2eAgNnSc1d1
louky	louka	k1gFnPc4
nedaleko	nedaleko	k7c2
Záblatí	Záblatí	k1gNnSc2
u	u	k7c2
Ponědraže	Ponědraž	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NPR	NPR	kA
Velký	velký	k2eAgMnSc1d1
a	a	k8xC
Malý	malý	k2eAgMnSc1d1
Tisý	Tisý	k1gMnSc1
</s>
<s>
Velký	velký	k2eAgMnSc1d1
Tisý	Tisý	k1gMnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Velký	velký	k2eAgMnSc1d1
a	a	k8xC
Malý	malý	k2eAgMnSc1d1
Tisý	Tisý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
ploše	plocha	k1gFnSc6
615	#num#	k4
ha	ha	kA
a	a	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
rybničních	rybniční	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
ze	z	k7c2
14	#num#	k4
rybníků	rybník	k1gInPc2
a	a	k8xC
mokrých	mokrý	k2eAgFnPc2d1
luk	louka	k1gFnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yRgFnPc3,k3yQgFnPc3,k3yIgFnPc3
zde	zde	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
četné	četný	k2eAgInPc4d1
mokřady	mokřad	k1gInPc4
s	s	k7c7
množstvím	množství	k1gNnSc7
rostlinných	rostlinný	k2eAgInPc2d1
a	a	k8xC
živočišných	živočišný	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgInPc4d3
druhy	druh	k1gInPc4
chráněných	chráněný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
rezervaci	rezervace	k1gFnSc6
vyskytují	vyskytovat	k5eAaImIp3nP
patří	patřit	k5eAaImIp3nS
:	:	kIx,
masnice	masnice	k1gFnSc1
vodní	vodní	k2eAgFnSc1d1
<g/>
,	,	kIx,
kapradiník	kapradiník	k1gInSc1
bažinný	bažinný	k2eAgInSc1d1
<g/>
,	,	kIx,
žebratka	žebratka	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
<g/>
,	,	kIx,
pupečník	pupečník	k1gInSc1
obecný	obecný	k2eAgInSc1d1
<g/>
,	,	kIx,
tolije	tolije	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
<g/>
,	,	kIx,
plavuň	plavuň	k1gFnSc1
pučivá	pučivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
všivec	všivec	k1gInSc1
lesní	lesní	k2eAgInSc1d1
<g/>
,	,	kIx,
bublinatka	bublinatka	k1gFnSc1
prostřední	prostřední	k2eAgFnSc1d1
<g/>
,	,	kIx,
bazanovec	bazanovec	k1gMnSc1
kytkokvětý	kytkokvětý	k2eAgMnSc1d1
<g/>
,	,	kIx,
hadilka	hadilka	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
vemeník	vemeník	k1gInSc1
dvoulistý	dvoulistý	k2eAgInSc1d1
<g/>
,	,	kIx,
vstavač	vstavač	k1gInSc1
kukačka	kukačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
rybníkům	rybník	k1gInPc3
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
hojnost	hojnost	k1gFnSc1
vodního	vodní	k2eAgNnSc2d1
ptactva	ptactvo	k1gNnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
můžeme	moct	k5eAaImIp1nP
zahlédnout	zahlédnout	k5eAaPmF
i	i	k9
ohrožený	ohrožený	k2eAgInSc1d1
druh	druh	k1gInSc1
orla	orel	k1gMnSc2
mořského	mořský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
také	také	k9
pochop	pochop	k1gMnSc1
rákosní	rákosní	k2eAgMnSc1d1
<g/>
,	,	kIx,
rybák	rybák	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
slavík	slavík	k1gMnSc1
modráček	modráček	k1gMnSc1
<g/>
,	,	kIx,
moudivláček	moudivláček	k1gMnSc1
lužní	lužní	k2eAgMnSc1d1
<g/>
,	,	kIx,
luňák	luňák	k1gMnSc1
hnědý	hnědý	k2eAgMnSc1d1
nebo	nebo	k8xC
chřástal	chřástal	k1gMnSc1
vodní	vodní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
v	v	k7c6
mokřadních	mokřadní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
sídlí	sídlet	k5eAaImIp3nP
některé	některý	k3yIgInPc4
druhy	druh	k1gInPc4
ptáků	pták	k1gMnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
volavka	volavka	k1gFnSc1
popelavá	popelavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ostralka	ostralka	k1gFnSc1
štíhlá	štíhlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
bukač	bukač	k1gMnSc1
velký	velký	k2eAgMnSc1d1
nebo	nebo	k8xC
labuť	labuť	k1gFnSc1
velká	velký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rybník	rybník	k1gInSc1
Velký	velký	k2eAgInSc1d1
Tisý	Tisý	k1gFnSc4
funguje	fungovat	k5eAaImIp3nS
také	také	k9
jako	jako	k9
ptačí	ptačí	k2eAgNnSc4d1
shromaždiště	shromaždiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shromažďují	shromažďovat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
například	například	k6eAd1
kachny	kachna	k1gFnPc1
<g/>
,	,	kIx,
husy	husa	k1gFnPc1
nebo	nebo	k8xC
kormoráni	kormorán	k1gMnPc1
velcí	velký	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NPR	NPR	kA
Stará	starý	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Stará	starý	k2eAgFnSc1d1
a	a	k8xC
Nová	nový	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
NPR	NPR	kA
Stará	starý	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
</s>
<s>
Vyhlášena	vyhlášen	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
a	a	k8xC
její	její	k3xOp3gFnSc1
rozloha	rozloha	k1gFnSc1
činí	činit	k5eAaImIp3nS
745	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřízena	zřízen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
hlavně	hlavně	k9
k	k	k7c3
ochraně	ochrana	k1gFnSc3
původního	původní	k2eAgNnSc2d1
koryta	koryto	k1gNnSc2
řeky	řeka	k1gFnSc2
Lužnice	Lužnice	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gNnSc2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
řada	řada	k1gFnSc1
tůní	tůně	k1gFnPc2
<g/>
,	,	kIx,
močálů	močál	k1gInPc2
a	a	k8xC
také	také	k9
dva	dva	k4xCgInPc4
rybníky	rybník	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příroda	příroda	k1gFnSc1
zde	zde	k6eAd1
není	být	k5eNaImIp3nS
původní	původní	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
od	od	k7c2
středověku	středověk	k1gInSc2
člověkem	člověk	k1gMnSc7
přetvářená	přetvářený	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Roste	růst	k5eAaImIp3nS
zde	zde	k6eAd1
hlavně	hlavně	k9
blatouch	blatouch	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
<g/>
,	,	kIx,
bledule	bledule	k1gFnSc1
jarní	jarní	k2eAgFnSc1d1
<g/>
,	,	kIx,
ďáblík	ďáblík	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
vzácné	vzácný	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
v	v	k7c6
rezervaci	rezervace	k1gFnSc6
vyskytují	vyskytovat	k5eAaImIp3nP
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
žebratka	žebratka	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
<g/>
,	,	kIx,
puštička	puštička	k1gFnSc1
rozprostřená	rozprostřený	k2eAgFnSc1d1
<g/>
,	,	kIx,
kosatec	kosatec	k1gInSc1
žlutý	žlutý	k2eAgInSc1d1
nebo	nebo	k8xC
leknín	leknín	k1gInSc1
bělostný	bělostný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
vzácné	vzácný	k2eAgInPc1d1
porosty	porost	k1gInPc1
statných	statný	k2eAgInPc2d1
dubů	dub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezobratlí	bezobratlý	k2eAgMnPc1d1
živočichové	živočich	k1gMnPc1
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupit	k5eAaPmNgInP
vzácnými	vzácný	k2eAgInPc7d1
druhy	druh	k1gInPc1
brouků	brouk	k1gMnPc2
například	například	k6eAd1
tesařík	tesařík	k1gMnSc1
obrovský	obrovský	k2eAgMnSc1d1
nebo	nebo	k8xC
páchník	páchník	k1gMnSc1
hnědý	hnědý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
hojné	hojný	k2eAgInPc4d1
druhy	druh	k1gInPc4
patří	patřit	k5eAaImIp3nS
bělopásek	bělopásek	k1gMnSc1
tavolníkový	tavolníkový	k2eAgMnSc1d1
nebo	nebo	k8xC
pavouk	pavouk	k1gMnSc1
lovčík	lovčík	k1gInSc4
vodní	vodní	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
močálech	močál	k1gInPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
některé	některý	k3yIgInPc1
druhy	druh	k1gInPc1
ptáků	pták	k1gMnPc2
jako	jako	k8xC,k8xS
chřástal	chřástal	k1gMnSc1
vodní	vodní	k2eAgFnSc2d1
<g/>
,	,	kIx,
chřástal	chřástal	k1gMnSc1
kropenatý	kropenatý	k2eAgMnSc1d1
nebo	nebo	k8xC
bekasina	bekasina	k1gFnSc1
otavní	otavní	k2eAgFnSc1d1
<g/>
,	,	kIx,
žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
také	také	k9
ledňáček	ledňáček	k1gMnSc1
říční	říční	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
hnízdí	hnízdit	k5eAaImIp3nS
na	na	k7c6
břehu	břeh	k1gInSc6
Lužnice	Lužnice	k1gFnSc1
nebo	nebo	k8xC
ptáci	pták	k1gMnPc1
hnízdící	hnízdící	k2eAgMnPc1d1
v	v	k7c6
lesích	les	k1gInPc6
jako	jako	k8xC,k8xS
čáp	čáp	k1gMnSc1
černý	černý	k1gMnSc1
<g/>
,	,	kIx,
orel	orel	k1gMnSc1
mořský	mořský	k2eAgMnSc1d1
nebo	nebo	k8xC
jestřáb	jestřáb	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgFnSc7d3
savec	savec	k1gMnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
rezervaci	rezervace	k1gFnSc6
je	být	k5eAaImIp3nS
vydra	vydra	k1gFnSc1
říční	říční	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NPR	NPR	kA
Červené	červené	k1gNnSc1
blato	blato	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Červené	Červené	k2eAgNnSc1d1
blato	blato	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Rezervace	rezervace	k1gFnSc1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
k	k	k7c3
ochraně	ochrana	k1gFnSc3
přirozeného	přirozený	k2eAgInSc2d1
rostlinného	rostlinný	k2eAgInSc2d1
pokryvu	pokryv	k1gInSc2
rašeliniště	rašeliniště	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rašeliniště	rašeliniště	k1gNnSc1
bylo	být	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1774	#num#	k4
do	do	k7c2
začátku	začátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
těženo	těžen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
spousta	spousta	k1gFnSc1
odvodňovacích	odvodňovací	k2eAgFnPc2d1
stok	stoka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
přehrazují	přehrazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
opět	opět	k6eAd1
držela	držet	k5eAaImAgFnS
voda	voda	k1gFnSc1
a	a	k8xC
zůstala	zůstat	k5eAaPmAgFnS
tak	tak	k6eAd1
zachována	zachovat	k5eAaPmNgFnS
původní	původní	k2eAgFnSc1d1
tvářnost	tvářnost	k1gFnSc1
tundry	tundra	k1gFnSc2
a	a	k8xC
druhová	druhový	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
rostlinstva	rostlinstvo	k1gNnSc2
z	z	k7c2
raně	raně	k6eAd1
poledového	poledový	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přirozenými	přirozený	k2eAgNnPc7d1
společenstvy	společenstvo	k1gNnPc7
rašeliniště	rašeliniště	k1gNnSc2
jsou	být	k5eAaImIp3nP
porosty	porost	k1gInPc1
borovice	borovice	k1gFnSc2
blatky	blatka	k1gFnSc2
a	a	k8xC
rojovníku	rojovník	k1gInSc2
bahenního	bahenní	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hojně	hojně	k6eAd1
se	s	k7c7
zde	zde	k6eAd1
také	také	k9
vyskytuje	vyskytovat	k5eAaImIp3nS
brusnice	brusnice	k1gFnSc1
borůvka	borůvka	k1gFnSc1
<g/>
,	,	kIx,
vlochyně	vlochyně	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
<g/>
,	,	kIx,
kyhanka	kyhanka	k1gFnSc1
sivolistá	sivolistý	k2eAgFnSc1d1
<g/>
,	,	kIx,
suchopýr	suchopýr	k1gInSc1
pochvatý	pochvatý	k2eAgInSc1d1
a	a	k8xC
klikva	klikva	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
<g/>
,	,	kIx,
vzácněji	vzácně	k6eAd2
rosnatka	rosnatka	k1gFnSc1
okrouhlolistá	okrouhlolistý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
mechorosty	mechorost	k1gInPc1
<g/>
,	,	kIx,
lišejníky	lišejník	k1gInPc1
a	a	k8xC
houby	houba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rezervace	rezervace	k1gFnSc1
je	být	k5eAaImIp3nS
významná	významný	k2eAgFnSc1d1
hlavně	hlavně	k9
faunou	fauna	k1gFnSc7
bezobratlých	bezobratlý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
například	například	k6eAd1
šídlo	šídlo	k1gNnSc1
rašelinné	rašelinný	k2eAgNnSc1d1
<g/>
,	,	kIx,
vážka	vážka	k1gFnSc1
tmavoskvrnná	tmavoskvrnný	k2eAgFnSc1d1
<g/>
,	,	kIx,
píďalička	píďalička	k1gFnSc1
rojovníková	rojovníková	k1gFnSc1
<g/>
,	,	kIx,
bourec	bourec	k1gMnSc1
borůvkový	borůvkový	k2eAgMnSc1d1
<g/>
,	,	kIx,
černoproužka	černoproužka	k1gFnSc1
březová	březový	k2eAgFnSc1d1
<g/>
,	,	kIx,
píďalka	píďalka	k1gFnSc1
vlnkovaná	vlnkovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
přástevník	přástevník	k1gMnSc1
fialkový	fialkový	k2eAgMnSc1d1
nebo	nebo	k8xC
světlopáska	světlopásek	k1gMnSc4
bahenní	bahenní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lesích	les	k1gInPc6
žijí	žít	k5eAaImIp3nP
řady	řada	k1gFnPc1
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnízdí	hnízdit	k5eAaImIp3nS
zde	zde	k6eAd1
např.	např.	kA
kulíšek	kulíšek	k1gMnSc1
nejmenší	malý	k2eAgMnSc1d3
<g/>
,	,	kIx,
čáp	čáp	k1gMnSc1
černý	černý	k1gMnSc1
<g/>
,	,	kIx,
jestřáb	jestřáb	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
či	či	k8xC
jeřábek	jeřábek	k1gMnSc1
lesní	lesní	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NPR	NPR	kA
Žofinka	Žofinka	k1gFnSc1
</s>
<s>
Borovice	borovice	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Žofinka	Žofinka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
Žofinka	Žofinka	k1gFnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
přibližně	přibližně	k6eAd1
129	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
přechodovým	přechodový	k2eAgNnSc7d1
rašeliništěm	rašeliniště	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mocnost	mocnost	k1gFnSc1
rašeliny	rašelina	k1gFnSc2
je	být	k5eAaImIp3nS
proměnlivá	proměnlivý	k2eAgFnSc1d1
a	a	k8xC
dosahuje	dosahovat	k5eAaImIp3nS
zde	zde	k6eAd1
maximálně	maximálně	k6eAd1
4	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nejčetnější	četný	k2eAgNnPc1d3
a	a	k8xC
nejtypičtější	typický	k2eAgNnPc1d3
společenstva	společenstvo	k1gNnPc1
rostlin	rostlina	k1gFnPc2
pro	pro	k7c4
přechodová	přechodový	k2eAgNnPc4d1
rašeliniště	rašeliniště	k1gNnPc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
hlavně	hlavně	k6eAd1
porosty	porost	k1gInPc7
borovice	borovice	k1gFnSc2
blatky	blatka	k1gFnSc2
a	a	k8xC
rojovníku	rojovník	k1gInSc2
bahenního	bahenní	k2eAgInSc2d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
také	také	k9
borůvky	borůvka	k1gFnSc2
černé	černá	k1gFnSc2
<g/>
,	,	kIx,
vřesu	vřes	k1gInSc2
obecného	obecný	k2eAgInSc2d1
<g/>
,	,	kIx,
klikvy	klikva	k1gFnSc2
bahenní	bahenní	k2eAgFnSc2d1
<g/>
,	,	kIx,
brusinky	brusinka	k1gFnSc2
obecné	obecný	k2eAgFnSc2d1
<g/>
,	,	kIx,
vlochyně	vlochyně	k1gFnSc2
bahenní	bahenní	k2eAgFnSc2d1
nebo	nebo	k8xC
kyhanky	kyhanka	k1gFnSc2
sivolisté	sivolistý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
méně	málo	k6eAd2
početné	početný	k2eAgFnPc4d1
duhy	duha	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
zde	zde	k6eAd1
rostou	růst	k5eAaImIp3nP
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
borovice	borovice	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
<g/>
,	,	kIx,
bříza	bříza	k1gFnSc1
bělokorá	bělokorý	k2eAgFnSc1d1
nebo	nebo	k8xC
také	také	k9
smrk	smrk	k1gInSc1
ztepilý	ztepilý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
bezobratlých	bezobratlý	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
hlavně	hlavně	k9
pavouci	pavouk	k1gMnPc1
nebo	nebo	k8xC
brouci	brouk	k1gMnPc1
jako	jako	k9
střevlík	střevlík	k1gMnSc1
nebo	nebo	k8xC
drabčíci	drabčík	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
obratlovců	obratlovec	k1gMnPc2
je	být	k5eAaImIp3nS
běžná	běžný	k2eAgFnSc1d1
ještěrka	ještěrka	k1gFnSc1
živorodá	živorodý	k2eAgFnSc1d1
<g/>
,	,	kIx,
čáp	čáp	k1gMnSc1
černý	černý	k1gMnSc1
<g/>
,	,	kIx,
jestřáb	jestřáb	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
nebo	nebo	k8xC
datel	datel	k1gMnSc1
černý	černý	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NPP	NPP	kA
Ruda	ruda	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Ruda	ruda	k1gFnSc1
(	(	kIx(
<g/>
národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zřízena	zřízen	k2eAgFnSc1d1
k	k	k7c3
ochraně	ochrana	k1gFnSc3
rašelinišť	rašeliniště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdříve	dříve	k6eAd3
vyhlášena	vyhlásit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
oblast	oblast	k1gFnSc1
o	o	k7c6
velikosti	velikost	k1gFnSc6
0,5	0,5	k4
ha	ha	kA
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
byla	být	k5eAaImAgFnS
rozšířena	rozšířit	k5eAaPmNgFnS
až	až	k9
na	na	k7c4
14,65	14,65	k4
ha	ha	kA
a	a	k8xC
v	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
další	další	k2eAgNnSc4d1
rozšíření	rozšíření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
slatinné	slatinný	k2eAgNnSc4d1
rašeliniště	rašeliniště	k1gNnSc4
s	s	k7c7
četnými	četný	k2eAgInPc7d1
vývěry	vývěr	k1gInPc7
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
železa	železo	k1gNnSc2
a	a	k8xC
významnými	významný	k2eAgNnPc7d1
společenstvy	společenstvo	k1gNnPc7
mokřadních	mokřadní	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
cenné	cenný	k2eAgInPc1d1
rostlinné	rostlinný	k2eAgInPc1d1
druhy	druh	k1gInPc1
jako	jako	k8xC,k8xS
orchidej	orchidej	k1gFnSc1
hlízovec	hlízovec	k1gInSc4
Loeselův	Loeselův	k2eAgInSc1d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
jen	jen	k9
v	v	k7c6
této	tento	k3xDgFnSc6
rezervaci	rezervace	k1gFnSc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
také	také	k9
mezi	mezi	k7c4
cenné	cenný	k2eAgInPc4d1
druhy	druh	k1gInPc4
patří	patřit	k5eAaImIp3nS
ostřice	ostřice	k1gFnSc1
mokřadní	mokřadní	k2eAgFnSc1d1
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
šlahounovitá	šlahounovitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
žebratka	žebratka	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
nebo	nebo	k8xC
kyhanka	kyhanka	k1gFnSc1
sivolistá	sivolistý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
typické	typický	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
patří	patřit	k5eAaImIp3nS
vachta	vachta	k1gFnSc1
trojlistá	trojlistý	k2eAgFnSc1d1
a	a	k8xC
přeslička	přeslička	k1gFnSc1
poříční	poříční	k2eAgFnSc1d1
s	s	k7c7
porosty	porost	k1gInPc7
suchopýru	suchopýr	k1gInSc2
štíhlého	štíhlý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
vrba	vrba	k1gFnSc1
rozmarýnolistá	rozmarýnolistý	k2eAgFnSc1d1
<g/>
,	,	kIx,
klikva	klikva	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
<g/>
,	,	kIx,
ďáblík	ďáblík	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
<g/>
,	,	kIx,
bazanovec	bazanovec	k1gMnSc1
kytkokvětý	kytkokvětý	k2eAgMnSc1d1
<g/>
,	,	kIx,
rosnatka	rosnatka	k1gFnSc1
okrouhlolistá	okrouhlolistý	k2eAgFnSc1d1
<g/>
,	,	kIx,
hrotnosemenka	hrotnosemenka	k1gFnSc1
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
dvounohá	dvounohý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
dvoumužná	dvoumužný	k2eAgFnSc1d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
také	také	k9
borovice	borovice	k1gFnPc1
a	a	k8xC
břízy	bříza	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
naházejí	naházet	k5eAaPmIp3nP,k5eAaBmIp3nP
spíše	spíše	k9
v	v	k7c6
sušších	suchý	k2eAgNnPc6d2
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
těžbě	těžba	k1gFnSc6
rašeliny	rašelina	k1gFnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
utvořila	utvořit	k5eAaPmAgNnP
malá	malý	k2eAgNnPc1d1
jezírka	jezírko	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
hojně	hojně	k6eAd1
zastoupeny	zastoupit	k5eAaPmNgFnP
bublinatkami	bublinatka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
také	také	k9
významný	významný	k2eAgInSc4d1
výskyt	výskyt	k1gInSc4
mechorostů	mechorost	k1gInPc2
jako	jako	k8xS,k8xC
srpnatky	srpnatka	k1gFnSc2
fermežové	fermežový	k2eAgFnSc2d1
a	a	k8xC
plstnatce	plstnatec	k1gMnSc2
rašelinného	rašelinný	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
je	být	k5eAaImIp3nS
zastoupena	zastoupit	k5eAaPmNgFnS
významnými	významný	k2eAgInPc7d1
druhy	druh	k1gInPc7
jako	jako	k8xS,k8xC
skokan	skokan	k1gMnSc1
ostronosý	ostronosý	k2eAgMnSc1d1
<g/>
,	,	kIx,
chřástal	chřástal	k1gMnSc1
vodní	vodní	k2eAgMnSc1d1
<g/>
,	,	kIx,
linduška	linduška	k1gFnSc1
luční	luční	k2eAgFnSc1d1
<g/>
,	,	kIx,
bekasina	bekasina	k1gFnSc1
otavní	otavní	k2eAgFnSc2d1
<g/>
,	,	kIx,
vzácný	vzácný	k2eAgMnSc1d1
krasec	krasec	k1gMnSc1
nebo	nebo	k8xC
bělopásek	bělopásek	k1gMnSc1
tavolníkový	tavolníkový	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
savců	savec	k1gMnPc2
hlavně	hlavně	k9
hraboš	hraboš	k1gMnSc1
mokřadní	mokřadní	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
maloplošná	maloplošný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Bukové	bukový	k2eAgInPc4d1
kopce	kopec	k1gInPc4
</s>
<s>
Dračice	dračice	k1gFnSc1
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Lužnice	Lužnice	k1gFnSc1
</s>
<s>
Horusická	horusický	k2eAgNnPc1d1
blata	blata	k1gNnPc1
</s>
<s>
Krabonošská	Krabonošský	k2eAgFnSc1d1
niva	niva	k1gFnSc1
</s>
<s>
Losí	losí	k2eAgNnSc1d1
blato	blato	k1gNnSc1
u	u	k7c2
Mirochova	Mirochův	k2eAgInSc2d1
</s>
<s>
Meandry	meandr	k1gInPc1
Lužnice	Lužnice	k1gFnSc2
</s>
<s>
Na	na	k7c6
Ivance	Ivanka	k1gFnSc6
</s>
<s>
Novořecké	novořecký	k2eAgInPc1d1
močály	močál	k1gInPc1
</s>
<s>
Olšina	olšina	k1gFnSc1
u	u	k7c2
Přeseky	Přesek	k1gInPc4
</s>
<s>
Pískový	pískový	k2eAgInSc1d1
přesyp	přesyp	k1gInSc1
u	u	k7c2
Vlkova	Vlkův	k2eAgInSc2d1
</s>
<s>
Rašeliniště	rašeliniště	k1gNnSc1
Hovízna	Hovízna	k1gFnSc1
</s>
<s>
Rašeliniště	rašeliniště	k1gNnSc1
Pele	pel	k1gInSc5
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Ruda	ruda	k1gFnSc1
u	u	k7c2
Kojákovic	Kojákovice	k1gFnPc2
</s>
<s>
Rybníky	rybník	k1gInPc1
u	u	k7c2
Vitmanova	Vitmanův	k2eAgInSc2d1
</s>
<s>
Staré	Staré	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
</s>
<s>
Široké	Široké	k2eAgNnSc1d1
blato	blato	k1gNnSc1
</s>
<s>
Trpnouzské	Trpnouzský	k2eAgNnSc1d1
blato	blato	k1gNnSc1
</s>
<s>
Výtopa	výtopa	k1gFnSc1
Rožmberka	Rožmberk	k1gInSc2
</s>
<s>
Záblatské	záblatský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Hliníř	Hliníř	k1gFnSc1
</s>
<s>
Kozí	kozí	k2eAgInSc1d1
vršek	vršek	k1gInSc1
</s>
<s>
Lhota	Lhota	k1gFnSc1
u	u	k7c2
Dynína	Dynín	k1gInSc2
</s>
<s>
Pískovna	pískovna	k1gFnSc1
u	u	k7c2
Dračice	dračice	k1gFnSc2
</s>
<s>
Slepičí	slepičí	k2eAgInSc1d1
vršek	vršek	k1gInSc1
</s>
<s>
Soví	soví	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
http://www.jiznicechy.org/cz/index.php?path=prir/trebon.htm	http://www.jiznicechy.org/cz/index.php?path=prir/trebon.htm	k6eAd1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Třeboňsko	Třeboňsko	k1gNnSc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
http://www.trebonsko.cz/chko-trebonsko1	http://www.trebonsko.cz/chko-trebonsko1	k4
2	#num#	k4
3	#num#	k4
http://www.trebonsko.ochranaprirody.cz	http://www.trebonsko.ochranaprirody.cz	k1gMnSc1
<g/>
↑	↑	k?
http://www.itrebon.cz	http://www.itrebon.cz	k1gMnSc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
http://www.trebonsko.cz/historie-trebonska1	http://www.trebonsko.cz/historie-trebonska1	k4
2	#num#	k4
http://www.trebonsko.cz/flora-chko-trebonsko	http://www.trebonsko.cz/flora-chko-trebonsko	k6eAd1
<g/>
↑	↑	k?
http://www.trebonsko.cz/zivocichove-trebonska-obratlovci	http://www.trebonsko.cz/zivocichove-trebonska-obratlovec	k1gMnSc3
<g/>
↑	↑	k?
http://chkotreb.wz.cz/index.php?id=3	http://chkotreb.wz.cz/index.php?id=3	k4
<g/>
↑	↑	k?
http://www.cittadella.cz/europarc/index.php?p=index&	http://www.cittadella.cz/europarc/index.php?p=index&	k1gMnSc1
<g/>
↑	↑	k?
http://www.cittadella.cz/europarc/index.php?p=index&	http://www.cittadella.cz/europarc/index.php?p=index&	k1gMnSc1
<g/>
↑	↑	k?
http://www.cittadella.cz/europarc/index.php?p=index&	http://www.cittadella.cz/europarc/index.php?p=index&	k1gMnSc1
<g/>
↑	↑	k?
http://www.cittadella.cz/europarc/index.php?p=index&	http://www.cittadella.cz/europarc/index.php?p=index&	k1gMnSc1
<g/>
↑	↑	k?
http://www.cittadella.cz/europarc/index.php?p=index&	http://www.cittadella.cz/europarc/index.php?p=index&	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Třeboň	Třeboň	k1gFnSc1
</s>
<s>
Třeboň	Třeboň	k1gFnSc1
II	II	kA
</s>
<s>
Rožmberk	Rožmberk	k1gMnSc1
</s>
<s>
Masarykovo	Masarykův	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
</s>
<s>
Zámek	zámek	k1gInSc1
Třeboň	Třeboň	k1gFnSc1
</s>
<s>
Třeboňské	třeboňský	k2eAgNnSc1d1
lázeňství	lázeňství	k1gNnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ALBRECHT	Albrecht	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
území	území	k1gNnSc6
ČR	ČR	kA
VIII	VIII	kA
<g/>
.	.	kIx.
-	-	kIx~
Českobudějovicko	Českobudějovicko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
EkoCentrum	EkoCentrum	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
807	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86064	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHKO	CHKO	kA
Třeboňsko	Třeboňsko	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
509	#num#	k4
až	až	k9
575	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Třeboňsko	Třeboňsko	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Třeboňsko	Třeboňsko	k1gNnSc4
na	na	k7c6
OpenStreetMap	OpenStreetMap	k1gInSc1
</s>
<s>
Dokumentární	dokumentární	k2eAgInSc4d1
snímek	snímek	k1gInSc4
Třeboňsko	Třeboňsko	k1gNnSc1
o	o	k7c6
oblasti	oblast	k1gFnSc6
na	na	k7c6
České	český	k2eAgFnSc6d1
Televizi	televize	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Novohradské	novohradský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Brouskův	Brouskův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
•	•	k?
Červené	Červené	k2eAgNnSc4d1
blato	blato	k1gNnSc4
•	•	k?
Žofinka	Žofinka	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Hojná	hojný	k2eAgFnSc1d1
voda	voda	k1gFnSc1
•	•	k?
Ruda	ruda	k1gFnSc1
•	•	k?
Terčino	Terčin	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Dvořiště	dvořiště	k1gNnSc1
•	•	k?
Horusická	horusický	k2eAgNnPc4d1
blata	blata	k1gNnPc4
•	•	k?
Karvanice	Karvanice	k1gFnPc4
•	•	k?
Libochovka	Libochovka	k1gFnSc1
•	•	k?
Mokřiny	mokřina	k1gFnSc2
u	u	k7c2
Vomáčků	Vomáčka	k1gMnPc2
•	•	k?
Radomilická	Radomilický	k2eAgFnSc1d1
mokřina	mokřina	k1gFnSc1
•	•	k?
Ruda	ruda	k1gFnSc1
u	u	k7c2
Kojákovic	Kojákovice	k1gFnPc2
•	•	k?
V	v	k7c6
Rájích	ráj	k1gInPc6
•	•	k?
Velký	velký	k2eAgInSc1d1
a	a	k8xC
Malý	malý	k2eAgInSc1d1
Kamýk	Kamýk	k1gInSc1
•	•	k?
Vrbenské	Vrbenský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
Běta	Běta	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Baba	baba	k1gFnSc1
•	•	k?
Bedřichovský	Bedřichovský	k2eAgInSc4d1
potok	potok	k1gInSc4
•	•	k?
Blana	Blan	k1gInSc2
•	•	k?
Ďáblík	ďáblík	k1gMnSc1
•	•	k?
Děkanec	Děkanec	k1gMnSc1
•	•	k?
Hliníř	Hliníř	k1gFnSc1
•	•	k?
Hlubocké	hlubocký	k2eAgFnPc4d1
hráze	hráz	k1gFnPc4
•	•	k?
Kaliště	kaliště	k1gNnSc1
•	•	k?
Kameník	Kameník	k1gMnSc1
•	•	k?
Lhota	Lhota	k1gMnSc1
u	u	k7c2
Dynína	Dynín	k1gInSc2
•	•	k?
Libnič	Libnič	k1gMnSc1
•	•	k?
Lužnice	Lužnice	k1gFnSc1
•	•	k?
Ohrazení	ohrazení	k1gNnSc2
•	•	k?
Orty	ort	k1gInPc1
•	•	k?
Ostrolovský	Ostrolovský	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
•	•	k?
Pašínovická	Pašínovický	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Přesličkový	přesličkový	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Sokolí	sokolí	k2eAgNnSc4d1
hnízdo	hnízdo	k1gNnSc4
a	a	k8xC
bažantnice	bažantnice	k1gFnPc4
•	•	k?
Tůně	tůně	k1gFnPc4
u	u	k7c2
Špačků	Špaček	k1gMnPc2
•	•	k?
Velký	velký	k2eAgInSc1d1
Karasín	Karasín	k1gInSc1
•	•	k?
Veveřský	Veveřský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Vltava	Vltava	k1gFnSc1
u	u	k7c2
Blanského	blanský	k2eAgInSc2d1
lesa	les	k1gInSc2
•	•	k?
Vrbenská	Vrbenský	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
•	•	k?
Zámek	zámek	k1gInSc1
•	•	k?
Žemlička	žemlička	k1gFnSc1
•	•	k?
Židova	Židův	k2eAgFnSc1d1
strouha	strouha	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
parky	park	k1gInPc4
</s>
<s>
Přírodní	přírodní	k2eAgInSc1d1
park	park	k1gInSc1
Česká	český	k2eAgFnSc1d1
Kanada	Kanada	k1gFnSc1
•	•	k?
Přírodní	přírodní	k2eAgInSc1d1
park	park	k1gInSc1
Homolka	homolka	k1gFnSc1
<g/>
–	–	k?
<g/>
Vojířov	Vojířov	k1gInSc4
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Třeboňsko	Třeboňsko	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Červené	Červené	k2eAgNnSc1d1
blato	blato	k1gNnSc1
•	•	k?
Stará	starat	k5eAaImIp3nS
a	a	k8xC
Nová	nový	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
•	•	k?
Velký	velký	k2eAgMnSc1d1
a	a	k8xC
Malý	malý	k2eAgMnSc1d1
Tisý	Tisý	k1gMnSc1
•	•	k?
Žofinka	Žofinka	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Kaproun	Kaproun	k1gInSc1
•	•	k?
Krvavý	krvavý	k2eAgInSc1d1
a	a	k8xC
Kačležský	Kačležský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Vizír	vizír	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Blanko	blanko	k6eAd1
•	•	k?
Bukové	bukový	k2eAgFnSc3d1
kopce	kopka	k1gFnSc3
•	•	k?
Fabián	Fabián	k1gMnSc1
•	•	k?
Hadí	hadit	k5eAaImIp3nS
vrch	vrch	k1gInSc4
•	•	k?
Horní	horní	k2eAgFnSc2d1
Lužnice	Lužnice	k1gFnSc2
•	•	k?
Hrádeček	hrádeček	k1gInSc1
•	•	k?
Krabonošská	Krabonošský	k2eAgFnSc1d1
niva	niva	k1gFnSc1
•	•	k?
Losí	losí	k2eAgNnSc4d1
blato	blato	k1gNnSc4
u	u	k7c2
Mirochova	Mirochův	k2eAgInSc2d1
•	•	k?
Mutenská	Mutenský	k2eAgFnSc1d1
obora	obora	k1gFnSc1
•	•	k?
Na	na	k7c6
Ivance	Ivanka	k1gFnSc6
•	•	k?
Olšina	olšina	k1gFnSc1
u	u	k7c2
Přeseky	Přesek	k1gInPc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Hovízna	Hovízna	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Pele	pel	k1gInSc5
•	•	k?
Rybníky	rybník	k1gInPc4
u	u	k7c2
Vitmanova	Vitmanův	k2eAgInSc2d1
•	•	k?
Skalák	Skalák	k1gMnSc1
u	u	k7c2
Senotína	Senotín	k1gInSc2
•	•	k?
Staré	Staré	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
•	•	k?
Široké	Široké	k2eAgNnSc4d1
blato	blato	k1gNnSc4
•	•	k?
Výtopa	výtopa	k1gFnSc1
Rožmberka	Rožmberk	k1gInSc2
•	•	k?
Záblatské	záblatský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Branské	branský	k2eAgNnSc1d1
doubí	doubí	k1gNnSc1
•	•	k?
Dědek	Dědek	k1gMnSc1
u	u	k7c2
Slavonic	Slavonice	k1gFnPc2
•	•	k?
Dubová	dubový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Gebhárecký	Gebhárecký	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Jalovce	jalovec	k1gInSc2
u	u	k7c2
Kunžaku	Kunžak	k1gInSc2
•	•	k?
Jalovce	jalovec	k1gInSc2
u	u	k7c2
Valtínova	Valtínův	k2eAgInSc2d1
•	•	k?
Kramářka	kramářka	k1gFnSc1
•	•	k?
Králek	králka	k1gFnPc2
•	•	k?
Kysibl	Kysibl	k1gMnSc1
•	•	k?
Lipina	lipina	k1gFnSc1
•	•	k?
Malý	Malý	k1gMnSc1
Bukač	bukač	k1gMnSc1
•	•	k?
Matenský	Matenský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Moravská	moravský	k2eAgFnSc1d1
Dyje	Dyje	k1gFnSc1
•	•	k?
Olšina	olšina	k1gFnSc1
u	u	k7c2
Volfířova	Volfířův	k2eAgInSc2d1
•	•	k?
Pazourův	Pazourův	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Pískovna	pískovna	k1gFnSc1
na	na	k7c6
cvičišti	cvičiště	k1gNnSc6
•	•	k?
Pískovna	pískovna	k1gFnSc1
u	u	k7c2
Dračice	dračice	k1gFnSc2
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Klenová	klenový	k2eAgFnSc1d1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc4
Mosty	most	k1gInPc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc4
Radlice	radlice	k1gFnSc2
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
u	u	k7c2
Suchdola	Suchdola	k1gFnSc1
•	•	k?
Rybníky	rybník	k1gInPc1
u	u	k7c2
Lovětína	Lovětín	k1gInSc2
•	•	k?
Slepičí	slepičit	k5eAaImIp3nS
vršek	vršek	k1gInSc4
•	•	k?
Toužínské	Toužínský	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Velký	velký	k2eAgInSc1d1
Troubný	Troubný	k2eAgInSc1d1
•	•	k?
Žofina	Žofina	k1gFnSc1
Huť	huť	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Tábor	Tábor	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Černická	Černický	k2eAgFnSc1d1
obora	obora	k1gFnSc1
•	•	k?
Jistebnická	jistebnický	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
•	•	k?
Kukle	kukle	k1gFnSc2
•	•	k?
Plziny	Plzina	k1gMnSc2
•	•	k?
Polánka	Polánek	k1gMnSc2
•	•	k?
Turovecký	Turovecký	k2eAgInSc4d1
les	les	k1gInSc4
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Třeboňsko	Třeboňsko	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Chýnovská	Chýnovský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
•	•	k?
Luční	luční	k2eAgFnSc1d1
•	•	k?
Ruda	ruda	k1gFnSc1
•	•	k?
Stročov	Stročov	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Borkovická	Borkovický	k2eAgNnPc1d1
blata	blata	k1gNnPc1
•	•	k?
Dráchovské	Dráchovský	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Dráchovské	Dráchovský	k2eAgFnSc2d1
tůně	tůně	k1gFnSc2
•	•	k?
Horusická	horusický	k2eAgNnPc4d1
blata	blata	k1gNnPc4
•	•	k?
Choustník	Choustník	k1gMnSc1
•	•	k?
Kladrubská	kladrubský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Kozohlůdky	Kozohlůdka	k1gFnSc2
•	•	k?
Pacova	Pacův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Písečný	písečný	k2eAgInSc4d1
přesyp	přesyp	k1gInSc4
u	u	k7c2
Vlkova	Vlkův	k2eAgNnSc2d1
•	•	k?
Rod	rod	k1gInSc1
•	•	k?
V	v	k7c6
Luhu	luh	k1gInSc6
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Černická	Černický	k2eAgFnSc1d1
obora	obora	k1gFnSc1
•	•	k?
Černýšovické	Černýšovický	k2eAgInPc1d1
jalovce	jalovec	k1gInPc1
•	•	k?
Doubí	doubí	k1gNnSc6
u	u	k7c2
Žíšova	Žíšov	k1gInSc2
•	•	k?
Farářský	farářský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Granátová	granátový	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Hroby	hrob	k1gInPc1
•	•	k?
Jesení	jeseň	k1gFnPc2
•	•	k?
Kozí	kozí	k2eAgInSc1d1
vršek	vršek	k1gInSc1
•	•	k?
Kozlov	Kozlov	k1gInSc4
•	•	k?
Kutiny	kutin	k1gInPc4
•	•	k?
Luna	luna	k1gFnSc1
•	•	k?
Lužnice	Lužnice	k1gFnSc2
•	•	k?
Nový	nový	k2eAgInSc4d1
rybník	rybník	k1gInSc4
u	u	k7c2
Soběslavi	Soběslav	k1gFnSc2
•	•	k?
Ostrov	ostrov	k1gInSc1
Markéta	Markéta	k1gFnSc1
•	•	k?
Stříbrná	stříbrný	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
•	•	k?
Tábor	Tábor	k1gInSc1
-	-	kIx~
Zahrádka	Zahrádka	k1gMnSc1
•	•	k?
Veselská	Veselská	k1gFnSc1
blata	blata	k1gNnPc1
•	•	k?
Vlásenický	Vlásenický	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Vlašimská	vlašimský	k2eAgFnSc1d1
Blanice	Blanice	k1gFnSc1
•	•	k?
Zeman	Zeman	k1gMnSc1
•	•	k?
Židova	Židův	k2eAgFnSc1d1
strouha	strouha	k1gFnSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
|	|	kIx~
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
437332	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
234352117	#num#	k4
</s>
