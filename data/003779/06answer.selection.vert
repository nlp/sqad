<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
předchozí	předchozí	k2eAgFnPc1d1	předchozí
generace	generace	k1gFnPc1	generace
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
iPhone	iPhon	k1gInSc5	iPhon
3G	[number]	k4	3G
procesor	procesor	k1gInSc1	procesor
Samsung	Samsung	kA	Samsung
S5L8900	S5L8900	k1gFnSc2	S5L8900
(	(	kIx(	(
<g/>
412	[number]	k4	412
MHz	Mhz	kA	Mhz
ARM	ARM	kA	ARM
1176	[number]	k4	1176
procesor	procesor	k1gInSc1	procesor
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
koprocesor	koprocesor	k1gInSc1	koprocesor
PowerVR	PowerVR	k1gFnSc2	PowerVR
MBX	MBX	kA	MBX
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
a	a	k8xC	a
operační	operační	k2eAgFnSc4d1	operační
paměť	paměť	k1gFnSc4	paměť
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
128	[number]	k4	128
MB	MB	kA	MB
(	(	kIx(	(
<g/>
eDRAM	eDRAM	k?	eDRAM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
