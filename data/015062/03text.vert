<s>
Imamát	Imamát	k1gInSc1
Omán	Omán	k1gInSc1
</s>
<s>
Imamát	Imamát	k1gMnSc1
Ománʿ	Ománʿ	k1gMnSc1
al-Wusṭ	al-Wusṭ	k?
<g/>
ُ	ُ	k?
<g/>
م	م	k?
<g/>
َ	َ	k?
<g/>
ا	ا	k?
ٱ	ٱ	k?
<g/>
ْ	ْ	k?
<g/>
و	و	k?
<g/>
ُ	ُ	k?
<g/>
س	س	k?
<g/>
ْ	ْ	k?
<g/>
ط	ط	k?
<g/>
َ	َ	k?
<g/>
ى	ى	k?
</s>
<s>
751	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Hymna	hymna	k1gFnSc1
<g/>
:	:	kIx,
As-Salam	As-Salam	k1gInSc1
as-Sultani	as-Sultaň	k1gFnSc3
(	(	kIx(
<g/>
neoficiálně	oficiálně	k6eNd1,k6eAd1
<g/>
,	,	kIx,
<g/>
oficiálně	oficiálně	k6eAd1
žádná	žádný	k3yNgFnSc1
<g/>
)	)	kIx)
</s>
<s>
Motto	motto	k1gNnSc1
<g/>
:	:	kIx,
Allā	Allā	k1gInSc2
Akbar	Akbar	k1gInSc1
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Přibližná	přibližný	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
států	stát	k1gInPc2
Maskat	Maskat	k1gInSc1
a	a	k8xC
Omán	Omán	k1gInSc1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Imamát	Imamát	k1gInSc1
Omán	Omán	k1gInSc1
Sultanát	sultanát	k1gInSc1
Maskat	Maskat	k1gInSc4
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Nazvá	Nazvat	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
arabština	arabština	k1gFnSc1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
ibádíjský	ibádíjský	k2eAgInSc1d1
islám	islám	k1gInSc1
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
teokratická	teokratický	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Maskat	Maskat	k1gInSc1
a	a	k8xC
Omán	Omán	k1gInSc1
</s>
<s>
Imamát	Imamát	k1gInSc1
Omán	Omán	k1gInSc1
byl	být	k5eAaImAgInS
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgInSc2d1
Sultanátu	sultanát	k1gInSc2
Omán	Omán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
již	již	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
islámské	islámský	k2eAgFnSc2d1
expanze	expanze	k1gFnSc2
v	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
a	a	k8xC
jako	jako	k9
stát	stát	k5eAaPmF,k5eAaImF
zřejmě	zřejmě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
751	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imám	imám	k1gMnSc1
měl	mít	k5eAaImAgMnS
zpravidla	zpravidla	k6eAd1
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
vnitrozemí	vnitrozemí	k1gNnSc2
okolo	okolo	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Nazvá	Nazvý	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
v	v	k7c6
období	období	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
pobřeží	pobřeží	k1gNnSc1
ovládáno	ovládat	k5eAaImNgNnS
cizími	cizí	k2eAgFnPc7d1
mocnostmi	mocnost	k1gFnPc7
<g/>
,	,	kIx,
např.	např.	kA
Umajjovci	Umajjovec	k1gMnPc7
nebo	nebo	k8xC
Seldžuky	Seldžuk	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imamát	Imamát	k1gInSc1
téměř	téměř	k6eAd1
po	po	k7c4
celou	celý	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
značně	značně	k6eAd1
charakterizoval	charakterizovat	k5eAaBmAgMnS
izolacionismus	izolacionismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
vrcholné	vrcholný	k2eAgNnSc4d1
období	období	k1gNnSc4
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Maskat	Maskat	k1gInSc1
a	a	k8xC
Omán	Omán	k1gInSc1
</s>
<s>
Maskat	Maskat	k1gInSc1
a	a	k8xC
Omán	Omán	k1gInSc1
byl	být	k5eAaImAgInS
stát	stát	k5eAaImF,k5eAaPmF
zahrnující	zahrnující	k2eAgMnSc1d1
jednak	jednak	k8xC
vnitrozemí	vnitrozemí	k1gNnSc1
ovládané	ovládaný	k2eAgNnSc1d1
imámem	imám	k1gMnSc7
a	a	k8xC
jednak	jednak	k8xC
pobřežní	pobřežní	k2eAgInPc1d1
regiony	region	k1gInPc1
s	s	k7c7
centrem	centrum	k1gNnSc7
v	v	k7c6
Maskatu	Maskat	k1gInSc6
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
vládl	vládnout	k5eAaImAgInS
sultán	sultán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
státy	stát	k1gInPc7
vládlo	vládnout	k5eAaImAgNnS
napětí	napětí	k1gNnSc1
<g/>
,	,	kIx,
kmeny	kmen	k1gInPc1
loajální	loajální	k2eAgMnSc1d1
imámovi	imámův	k2eAgMnPc1d1
někdy	někdy	k6eAd1
útočili	útočit	k5eAaImAgMnP
na	na	k7c4
sultánova	sultánův	k2eAgNnPc4d1
teritoria	teritorium	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
imamát	imamát	k5eAaPmF
více	hodně	k6eAd2
než	než	k8xS
přiblížil	přiblížit	k5eAaPmAgMnS
dobytí	dobytí	k1gNnSc4
sultanátu	sultanát	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1868	#num#	k4
až	až	k9
1871	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
imám	imám	k1gMnSc1
Azzan	Azzan	k1gMnSc1
bin	bin	k?
Kais	Kais	k1gInSc1
Maskat	Maskat	k1gInSc1
dobyl	dobýt	k5eAaPmAgInS
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
v	v	k7c6
roce	rok	k1gInSc6
1895	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
kmeny	kmen	k1gInPc1
věrné	věrný	k2eAgFnSc2d1
imámovi	imám	k1gMnSc3
Salihovi	Salih	k1gMnSc3
bin	bin	k?
Alimu	Alim	k1gInSc2
kontrolovaly	kontrolovat	k5eAaImAgFnP
většinu	většina	k1gFnSc4
Maskatu	Maskat	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
sultán	sultán	k1gMnSc1
Fajsal	Fajsal	k1gFnSc2
bin	bin	k?
Turki	Turk	k1gFnSc2
se	se	k3xPyFc4
uchýlil	uchýlit	k5eAaPmAgMnS
do	do	k7c2
pevnosti	pevnost	k1gFnSc2
Jalali	Jalali	k1gFnSc2
(	(	kIx(
<g/>
Džalali	Džalali	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sultána	sultána	k1gFnSc1
nemile	mile	k6eNd1
překvapila	překvapit	k5eAaPmAgFnS
nečinnost	nečinnost	k1gFnSc1
Britů	Brit	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
nechtěli	chtít	k5eNaImAgMnP
zasahovat	zasahovat	k5eAaImF
v	v	k7c6
Ománu	Omán	k1gInSc6
pakliže	pakliže	k8xS
nemuseli	muset	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kmenoví	kmenový	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
loajální	loajální	k2eAgMnSc1d1
sultanátu	sultanát	k1gInSc2
se	se	k3xPyFc4
střetli	střetnout	k5eAaPmAgMnP
se	s	k7c7
sílou	síla	k1gFnSc7
imáma	imám	k1gMnSc2
<g/>
,	,	kIx,
situace	situace	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
vyrovnaná	vyrovnaný	k2eAgFnSc1d1
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
urovnala	urovnat	k5eAaPmAgFnS
dohodou	dohoda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rebelové	rebel	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
březnu	březen	k1gInSc6
1895	#num#	k4
stáhli	stáhnout	k5eAaPmAgMnP
zpět	zpět	k6eAd1
a	a	k8xC
sultán	sultán	k1gMnSc1
se	se	k3xPyFc4
zavázal	zavázat	k5eAaPmAgMnS
platit	platit	k5eAaImF
tribut	tribut	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
angažovat	angažovat	k5eAaBmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
zde	zde	k6eAd1
udrželi	udržet	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
vliv	vliv	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
projevilo	projevit	k5eAaPmAgNnS
podpisem	podpis	k1gInSc7
Síbské	Síbský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
mezi	mezi	k7c7
imámem	imám	k1gMnSc7
a	a	k8xC
sultánem	sultán	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
imamátu	imamáta	k1gFnSc4
zaručila	zaručit	k5eAaPmAgFnS
autonomii	autonomie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Následující	následující	k2eAgInPc1d1
roky	rok	k1gInPc1
se	se	k3xPyFc4
nesly	nést	k5eAaImAgInP
ve	v	k7c6
znamení	znamení	k1gNnSc6
relativního	relativní	k2eAgInSc2d1
klidu	klid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
ovšem	ovšem	k9
kmenoví	kmenový	k2eAgMnPc1d1
šejkové	šejk	k1gMnPc1
sešli	sejít	k5eAaPmAgMnP
v	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1913	#num#	k4
<g/>
,	,	kIx,
shodně	shodně	k6eAd1
považovali	považovat	k5eAaImAgMnP
Fajsalovu	Fajsalův	k2eAgFnSc4d1
servilnost	servilnost	k1gFnSc4
Evropanům	Evropan	k1gMnPc3
za	za	k7c4
slabost	slabost	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
vymýtit	vymýtit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
vyhlásili	vyhlásit	k5eAaPmAgMnP
džihád	džihád	k1gInSc4
se	se	k3xPyFc4
přesunuli	přesunout	k5eAaPmAgMnP
do	do	k7c2
města	město	k1gNnSc2
Nazvá	Nazvá	k1gFnSc1
–	–	k?
historicky	historicky	k6eAd1
to	ten	k3xDgNnSc1
sídlo	sídlo	k1gNnSc1
imáma	imám	k1gMnSc2
–	–	k?
a	a	k8xC
obsadili	obsadit	k5eAaPmAgMnP
jej	on	k3xPp3gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
zpočátku	zpočátku	k6eAd1
vyhýbali	vyhýbat	k5eAaImAgMnP
přímému	přímý	k2eAgInSc3d1
útoku	útok	k1gInSc3
na	na	k7c4
Maskat	Maskat	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nevyprovokovali	vyprovokovat	k5eNaPmAgMnP
Brity	Brit	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
by	by	kYmCp3nP
v	v	k7c6
takové	takový	k3xDgFnSc6
situaci	situace	k1gFnSc6
zasáhli	zasáhnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
1915	#num#	k4
se	se	k3xPyFc4
však	však	k9
odhodlali	odhodlat	k5eAaPmAgMnP
a	a	k8xC
s	s	k7c7
dvojím	dvojí	k4xRgNnSc7
vojskem	vojsko	k1gNnSc7
se	se	k3xPyFc4
vydali	vydat	k5eAaPmAgMnP
směrem	směr	k1gInSc7
k	k	k7c3
hlavnímu	hlavní	k2eAgNnSc3d1
městu	město	k1gNnSc3
<g/>
,	,	kIx,
než	než	k8xS
byli	být	k5eAaImAgMnP
odraženi	odrazit	k5eAaPmNgMnP
Brity	Brit	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Síbské	Síbský	k2eAgFnSc6d1
dohodě	dohoda	k1gFnSc6
</s>
<s>
Síbská	Síbský	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
pro	pro	k7c4
imamát	imamát	k1gInSc4
autonomii	autonomie	k1gFnSc4
a	a	k8xC
tak	tak	k6eAd1
nadešly	nadejít	k5eAaPmAgFnP
klidné	klidný	k2eAgInPc4d1
roky	rok	k1gInPc4
vzájemného	vzájemný	k2eAgInSc2d1
respektu	respekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
nastala	nastat	k5eAaPmAgFnS
se	s	k7c7
smrtí	smrt	k1gFnSc7
imáma	imám	k1gMnSc2
Mohameda	Mohamed	k1gMnSc2
al-Chaliliho	al-Chalili	k1gMnSc2
a	a	k8xC
nástupem	nástup	k1gInSc7
imáma	imám	k1gMnSc2
Ghaliba	Ghalib	k1gMnSc2
bin	bin	k?
Alího	Alí	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgInS
pod	pod	k7c7
vlivem	vliv	k1gInSc7
svého	svůj	k3xOyFgMnSc2
bratra	bratr	k1gMnSc2
Talib	Taliba	k1gFnPc2
bin	bin	k?
Ali	Ali	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
pokračovaly	pokračovat	k5eAaImAgInP
průzkumné	průzkumný	k2eAgInPc1d1
geologické	geologický	k2eAgInPc1d1
vrty	vrt	k1gInPc1
především	především	k6eAd1
evropských	evropský	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
hledaly	hledat	k5eAaImAgFnP
ropná	ropný	k2eAgFnSc1d1
ložiska	ložisko	k1gNnPc1
už	už	k9
od	od	k7c2
roku	rok	k1gInSc2
1925	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgInP
do	do	k7c2
konfliktu	konflikt	k1gInSc2
imamát	imamát	k5eAaPmNgMnS
s	s	k7c7
kmenem	kmen	k1gInSc7
Duru	Durus	k1gInSc2
u	u	k7c2
dnešního	dnešní	k2eAgNnSc2d1
ropného	ropný	k2eAgNnSc2d1
ložiska	ložisko	k1gNnSc2
Fahúd	Fahúda	k1gFnPc2
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Ibri	Ibri	k1gNnSc2
<g/>
,	,	kIx,
města	město	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
si	se	k3xPyFc3
nárokoval	nárokovat	k5eAaImAgMnS
jak	jak	k8xS,k8xC
imám	imám	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
sultán	sultán	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1955	#num#	k4
sultanát	sultanát	k1gInSc1
obsadil	obsadit	k5eAaPmAgInS
město	město	k1gNnSc4
Ibri	Ibr	k1gFnSc2
a	a	k8xC
vyhnal	vyhnat	k5eAaPmAgInS
Talibova	Talibův	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
<g/>
,	,	kIx,
v	v	k7c6
říjnu	říjen	k1gInSc6
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
obsadila	obsadit	k5eAaPmAgFnS
vojska	vojsko	k1gNnSc2
sultána	sultán	k1gMnSc2
Saída	Saíd	k1gMnSc2
Rustak	Rustak	k1gMnSc1
a	a	k8xC
v	v	k7c6
prosinci	prosinec	k1gInSc6
město	město	k1gNnSc1
Nazvá	Nazvá	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Imám	imám	k1gMnSc1
Ghalib	Ghalib	k1gMnSc1
bin	bin	k?
Alí	Alí	k1gMnSc1
zajistil	zajistit	k5eAaPmAgMnS
podporu	podpora	k1gFnSc4
od	od	k7c2
Egypta	Egypt	k1gInSc2
prezidenta	prezident	k1gMnSc2
Gamála	Gamál	k1gMnSc2
Násira	Násir	k1gMnSc2
a	a	k8xC
především	především	k9
wahhábistické	wahhábistický	k2eAgFnSc2d1
Saúdské	saúdský	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
trvající	trvající	k2eAgInSc4d1
do	do	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
zaměstnával	zaměstnávat	k5eAaImAgMnS
sultána	sultán	k1gMnSc4
Saída	Saíd	k1gMnSc4
bin	bin	k?
Tajmúra	Tajmúra	k1gMnSc1
stejně	stejně	k6eAd1
jako	jako	k9
nacionalističtí	nacionalistický	k2eAgMnPc1d1
a	a	k8xC
marxističtí	marxistický	k2eAgMnPc1d1
povstalci	povstalec	k1gMnPc1
v	v	k7c6
Dafáru	Dafár	k1gInSc6
(	(	kIx(
<g/>
na	na	k7c6
jihu	jih	k1gInSc6
Ománu	Omán	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporu	podpor	k1gInSc2
od	od	k7c2
SSSR	SSSR	kA
<g/>
,	,	kIx,
Číny	Čína	k1gFnSc2
nebo	nebo	k8xC
Jižního	jižní	k2eAgInSc2d1
Jemenu	Jemen	k1gInSc2
ovšem	ovšem	k9
odmítal	odmítat	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1
střet	střet	k1gInSc1
sil	síla	k1gFnPc2
imáma	imám	k1gMnSc2
a	a	k8xC
sultána	sultán	k1gMnSc2
Saída	Saíd	k1gMnSc2
bin	bin	k?
Tajmúra	Tajmúr	k1gMnSc2
(	(	kIx(
<g/>
se	s	k7c7
značnou	značný	k2eAgFnSc7d1
britskou	britský	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
<g/>
)	)	kIx)
nadešel	nadejít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1957	#num#	k4
<g/>
,	,	kIx,
sultán	sultán	k1gMnSc1
získal	získat	k5eAaPmAgMnS
centrum	centrum	k1gNnSc4
imamátu	imamát	k1gInSc2
–	–	k?
město	město	k1gNnSc1
Nazvá	Nazvá	k1gFnSc1
–	–	k?
a	a	k8xC
nakonec	nakonec	k6eAd1
takřka	takřka	k6eAd1
celé	celý	k2eAgNnSc4d1
vnitrozemí	vnitrozemí	k1gNnSc4
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
však	však	k9
ztrácel	ztrácet	k5eAaImAgInS
zájem	zájem	k1gInSc1
o	o	k7c4
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Imám	imám	k1gMnSc1
opustil	opustit	k5eAaPmAgMnS
zemi	zem	k1gFnSc4
a	a	k8xC
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
exilu	exil	k1gInSc2
<g/>
,	,	kIx,
byť	byť	k8xS
část	část	k1gFnSc1
věrných	věrný	k2eAgMnPc2d1
imamátu	imamát	k1gInSc2
vedla	vést	k5eAaImAgFnS
povstání	povstání	k1gNnSc4
do	do	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
zániku	zánik	k1gInSc6
imamátu	imamát	k1gInSc2
</s>
<s>
Imám	imám	k1gMnSc1
Ghalib	Ghalib	k1gMnSc1
bin	bin	k?
Alí	Alí	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
věrní	věrný	k2eAgMnPc1d1
zůstali	zůstat	k5eAaPmAgMnP
v	v	k7c6
exilu	exil	k1gInSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Kábús	Kábús	k1gInSc1
bin	bin	k?
Saíd	Saíd	k1gInSc1
převzal	převzít	k5eAaPmAgInS
titul	titul	k1gInSc4
a	a	k8xC
pozici	pozice	k1gFnSc4
sultána	sultána	k1gFnSc1
namísto	namísto	k7c2
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Saída	Saíd	k1gMnSc2
bin	bin	k?
Tajmúra	Tajmúr	k1gInSc2
poté	poté	k6eAd1
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
s	s	k7c7
britskou	britský	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
porazil	porazit	k5eAaPmAgMnS
rebely	rebel	k1gMnPc4
v	v	k7c6
Dafáru	Dafár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imám	imám	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
otázku	otázka	k1gFnSc4
imamátu	imamát	k1gInSc2
přednést	přednést	k5eAaPmF
na	na	k7c6
půdě	půda	k1gFnSc6
OSN	OSN	kA
<g/>
,	,	kIx,
ale	ale	k8xC
nikdy	nikdy	k6eAd1
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
nějaké	nějaký	k3yIgFnSc3
rezolutní	rezolutní	k2eAgFnSc3d1
akci	akce	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
imámů	imám	k1gMnPc2
od	od	k7c2
roku	rok	k1gInSc2
1856	#num#	k4
</s>
<s>
Thuvajni	Thuvajeň	k1gFnSc3
bin	bin	k?
Saíd	Saíd	k1gInSc1
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
-	-	kIx~
<g/>
1866	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Salim	Salima	k1gFnPc2
bin	bin	k?
Thuwaini	Thuwain	k1gMnPc1
(	(	kIx(
<g/>
1866	#num#	k4
<g/>
-	-	kIx~
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Azzan	Azzan	k1gInSc1
bin	bin	k?
Kais	Kais	k1gInSc1
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
-	-	kIx~
<g/>
1870	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Salim	Salim	k1gMnSc1
bin	bin	k?
Rašíd	Rašíd	k1gMnSc1
al-Charusi	al-Charuse	k1gFnSc4
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
-	-	kIx~
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mohamed	Mohamed	k1gMnSc1
al-Chalili	al-Chalit	k5eAaImAgMnP,k5eAaPmAgMnP
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
-	-	kIx~
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ghalib	Ghalib	k1gMnSc1
bin	bin	k?
Alí	Alí	k1gMnSc1
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
-	-	kIx~
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnSc1d1
</s>
<s>
Maskat	Maskat	k1gInSc1
a	a	k8xC
Omán	Omán	k1gInSc1
</s>
<s>
Protektorát	protektorát	k1gInSc1
Jižní	jižní	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
</s>
<s>
Dafárské	Dafárský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
ománských	ománský	k2eAgMnPc2d1
sultánů	sultán	k1gMnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PETERSON	PETERSON	kA
<g/>
,	,	kIx,
J.E.	J.E.	k1gFnSc1
Oman	oman	k1gInSc1
in	in	k?
the	the	k?
Twentieth	Twentieth	k1gInSc4
Century	Centura	k1gFnSc2
<g/>
:	:	kIx,
Political	Political	k1gFnSc1
Foundations	Foundations	k1gInSc1
of	of	k?
an	an	k?
Emerging	Emerging	k1gInSc1
State	status	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
286	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1317291735	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781317291732	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Introduction	Introduction	k1gInSc1
<g/>
:	:	kIx,
Oman	oman	k1gInSc1
and	and	k?
its	its	k?
People	People	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AL-HAJRI	AL-HAJRI	k1gMnSc1
<g/>
,	,	kIx,
Hilal	Hilal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
British	British	k1gInSc1
Travel-writing	Travel-writing	k1gInSc4
on	on	k3xPp3gMnSc1
Oman	oman	k1gInSc1
<g/>
:	:	kIx,
Orientalism	Orientalism	k1gMnSc1
Reappraised	Reappraised	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Peter	Peter	k1gMnSc1
Lang	Lang	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
317	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3039105353	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9783039105359	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
252	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ALLEN	allen	k1gInSc1
<g/>
,	,	kIx,
Calvin	Calvina	k1gFnPc2
H.	H.	kA
<g/>
;	;	kIx,
RIGSBEE	RIGSBEE	kA
II	II	kA
<g/>
,	,	kIx,
W.	W.	kA
Lynn	Lynn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oman	oman	k1gInSc1
Under	Under	k1gMnSc1
Qaboos	Qaboos	k1gMnSc1
<g/>
:	:	kIx,
From	From	k1gInSc1
Coup	coup	k1gInSc1
to	ten	k3xDgNnSc1
Constitution	Constitution	k1gInSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
-	-	kIx~
<g/>
1996	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Psychology	psycholog	k1gMnPc7
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
251	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
714650013	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780714650012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
AL-KHALILI	AL-KHALILI	k1gFnPc2
<g/>
,	,	kIx,
Majid	Majida	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oman	oman	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Foreign	Foreign	k1gInSc1
Policy	Policy	k1gInPc1
<g/>
:	:	kIx,
Foundation	Foundation	k1gInSc1
and	and	k?
Practice	Practice	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Praeger	Praeger	k1gInSc1
Security	Securita	k1gFnSc2
International	International	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
tisk	tisk	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
184	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
313352240	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780313352249	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
58	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RAZZAQ	RAZZAQ	kA
TAKRITI	TAKRITI	kA
<g/>
,	,	kIx,
Abdel	Abdel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monsoon	Monsoon	k1gInSc1
Revolution	Revolution	k1gInSc4
<g/>
:	:	kIx,
Republicans	Republicans	k1gInSc1
<g/>
,	,	kIx,
Sultans	Sultans	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Empires	Empires	k1gInSc1
in	in	k?
Oman	oman	k1gInSc1
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
-	-	kIx~
<g/>
1976	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
340	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
199674434	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780199674435	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Restoration	Restoration	k1gInSc1
of	of	k?
Omani	Omaeň	k1gFnSc6
rule	rula	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britannica	Britannicum	k1gNnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
JONES	JONES	kA
<g/>
,	,	kIx,
Jeremy	Jerema	k1gFnSc2
<g/>
;	;	kIx,
RIDOUT	RIDOUT	kA
<g/>
,	,	kIx,
Nicholas	Nicholas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
Modern	Modern	k1gInSc1
Oman	oman	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
291	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1107009405	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781107009400	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Omán	Omán	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85346793	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
145493832	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85346793	#num#	k4
</s>
