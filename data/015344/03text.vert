<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
Název	název	k1gInSc1
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
Football	Football	k1gInSc1
Club	club	k1gInSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
The	The	k?
Red	Red	k1gFnSc1
Devils	Devils	k1gInSc1
(	(	kIx(
<g/>
rudí	rudý	k2eAgMnPc1d1
ďáblové	ďábel	k1gMnPc1
<g/>
)	)	kIx)
Země	země	k1gFnSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
Město	město	k1gNnSc1
</s>
<s>
Manchester	Manchester	k1gInSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
1878	#num#	k4
Asociace	asociace	k1gFnSc1
</s>
<s>
Football	Footbalnout	k5eAaPmAgInS
Association	Association	k1gInSc1
Barvy	barva	k1gFnSc2
</s>
<s>
•	•	k?
tmavě	tmavě	k6eAd1
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
1879	#num#	k4
<g/>
–	–	k?
<g/>
1880	#num#	k4
<g/>
,	,	kIx,
1896	#num#	k4
<g/>
–	–	k?
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
•	•	k?
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
zlatá	zlatý	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
1880	#num#	k4
<g/>
–	–	k?
<g/>
1887	#num#	k4
<g/>
,	,	kIx,
1894	#num#	k4
<g/>
–	–	k?
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
•	•	k?
červená	červený	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
od	od	k7c2
1902	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_manutd	_manutd	k1gInSc1
<g/>
1920	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_manutd	_manutd	k1gMnSc1
<g/>
1920	#num#	k4
<g/>
a	a	k8xC
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_manutd	_manutd	k1gInSc1
<g/>
1920	#num#	k4
<g/>
t	t	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Premier	Premier	k1gInSc1
League	League	k1gNnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Old	Olda	k1gFnPc2
Trafford	Trafforda	k1gFnPc2
<g/>
,	,	kIx,
Manchester	Manchester	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
53	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
76	#num#	k4
000	#num#	k4
Vedení	vedení	k1gNnPc2
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Joel	Joel	k1gInSc1
a	a	k8xC
Avram	Avram	k1gInSc1
Glazer	Glazer	k1gMnSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Joel	Joel	k1gInSc1
a	a	k8xC
Avram	Avram	k1gInSc1
Glazer	Glazer	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Ole	Ola	k1gFnSc3
Gunnar	Gunnar	k1gMnSc1
Solskjæ	Solskjæ	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
20	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Anglie	Anglie	k1gFnSc2
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
Domácí	domácí	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
12	#num#	k4
<g/>
×	×	k?
FA	fa	kA
Cup	cup	k1gInSc4
5	#num#	k4
<g/>
×	×	k?
EFL	EFL	kA
Cup	cup	k1gInSc4
21	#num#	k4
<g/>
×	×	k?
Community	Communita	k1gFnSc2
Shield	Shield	k1gMnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
PMEZ	PMEZ	kA
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
1	#num#	k4
<g/>
×	×	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
×	×	k?
Superpohár	superpohár	k1gInSc4
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
×	×	k?
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
<g/>
1	#num#	k4
<g/>
×	×	k?
Interkontinentální	interkontinentální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
Football	Football	k1gInSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
anglický	anglický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
ve	v	k7c6
stejnojmenném	stejnojmenný	k2eAgNnSc6d1
metropolitním	metropolitní	k2eAgNnSc6d1
hrabství	hrabství	k1gNnSc6
Greater	Greater	k1gMnSc1
Manchester	Manchester	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
hraje	hrát	k5eAaImIp3nS
nepřetržitě	přetržitě	k6eNd1
nejvyšší	vysoký	k2eAgFnSc4d3
soutěž	soutěž	k1gFnSc4
v	v	k7c6
Anglii	Anglie	k1gFnSc6
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Premier	Premier	k1gMnSc1
League	Leagu	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gInPc3
největším	veliký	k2eAgInPc3d3
úspěchům	úspěch	k1gInPc3
patří	patřit	k5eAaImIp3nS
tři	tři	k4xCgNnPc4
vítězství	vítězství	k1gNnPc4
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
a	a	k8xC
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
Poháru	pohár	k1gInSc6
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
20	#num#	k4
titulů	titul	k1gInPc2
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
soutěži	soutěž	k1gFnSc6
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
12	#num#	k4
v	v	k7c6
Premier	Premira	k1gFnPc2
League	Leagu	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Newton	Newton	k1gMnSc1
Heath	Heath	k1gMnSc1
LYR	lyra	k1gFnPc2
FC	FC	kA
v	v	k7c6
roce	rok	k1gInSc6
1878	#num#	k4
jako	jako	k8xC,k8xS
tým	tým	k1gInSc1
pracovníků	pracovník	k1gMnPc2
Lancashierských	Lancashierský	k2eAgFnPc2d1
a	a	k8xC
Yorkshirských	yorkshirský	k2eAgFnPc2d1
drah	draha	k1gFnPc2
v	v	k7c6
depu	depo	k1gNnSc6
Newton	Newton	k1gMnSc1
Heath	Heath	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
bylo	být	k5eAaImAgNnS
klubové	klubový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
změněno	změnit	k5eAaPmNgNnS
na	na	k7c6
„	„	k?
<g/>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
byly	být	k5eAaImAgInP
i	i	k9
návrhy	návrh	k1gInPc1
na	na	k7c4
jména	jméno	k1gNnPc4
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Manchester	Manchester	k1gInSc1
Celtic	Celtice	k1gFnPc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Manchester	Manchester	k1gInSc1
Central	Central	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
znám	znát	k5eAaImIp1nS
pod	pod	k7c7
jmény	jméno	k1gNnPc7
„	„	k?
<g/>
Man	Man	k1gMnSc1
United	United	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
nebo	nebo	k8xC
jednoduše	jednoduše	k6eAd1
„	„	k?
<g/>
United	United	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přezdívka	přezdívka	k1gFnSc1
„	„	k?
<g/>
Man	mana	k1gFnPc2
U	u	k7c2
<g/>
“	“	k?
vyjadřuje	vyjadřovat	k5eAaImIp3nS
pohrdání	pohrdání	k1gNnSc1
nad	nad	k7c7
klubem	klub	k1gInSc7
a	a	k8xC
je	být	k5eAaImIp3nS
často	často	k6eAd1
používána	používat	k5eAaImNgFnS
tiskem	tisk	k1gInSc7
a	a	k8xC
fanoušky	fanoušek	k1gMnPc7
protivníků	protivník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Sídlo	sídlo	k1gNnSc4
klubu	klub	k1gInSc2
a	a	k8xC
stadion	stadion	k1gInSc1
nejsou	být	k5eNaImIp3nP
umístěny	umístit	k5eAaPmNgInP
přímo	přímo	k6eAd1
v	v	k7c6
City	city	k1gNnSc6
of	of	k?
Manchester	Manchester	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
čtvrti	čtvrt	k1gFnSc6
Trafford	Trafforda	k1gFnPc2
mezi	mezi	k7c7
městy	město	k1gNnPc7
Manchester	Manchester	k1gInSc1
a	a	k8xC
Salford	Salford	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
United	United	k1gInSc4
jsou	být	k5eAaImIp3nP
jedním	jeden	k4xCgMnSc7
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
a	a	k8xC
nejvíce	nejvíce	k6eAd1,k6eAd3
podporovaných	podporovaný	k2eAgInPc2d1
klubů	klub	k1gInPc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
v	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Sira	sir	k1gMnSc2
Alexe	Alex	k1gMnSc5
Fergusona	Ferguson	k1gMnSc2
naprosto	naprosto	k6eAd1
dominoval	dominovat	k5eAaImAgMnS
anglické	anglický	k2eAgFnSc3d1
lize	liga	k1gFnSc3
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
jaký	jaký	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
naposledy	naposledy	k6eAd1
předvedl	předvést	k5eAaPmAgMnS
Liverpool	Liverpool	k1gInSc4
v	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
a	a	k8xC
osmdesátých	osmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholem	vrchol	k1gInSc7
byla	být	k5eAaImAgFnS
sezóna	sezóna	k1gFnSc1
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
získal	získat	k5eAaPmAgInS
klub	klub	k1gInSc1
treble	treble	k6eAd1
-	-	kIx~
vyhrál	vyhrát	k5eAaPmAgMnS
Premier	Premier	k1gMnSc1
League	Leagu	k1gInSc2
<g/>
,	,	kIx,
Anglický	anglický	k2eAgInSc1d1
pohár	pohár	k1gInSc4
a	a	k8xC
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
definitivně	definitivně	k6eAd1
zařadil	zařadit	k5eAaPmAgMnS
mezi	mezi	k7c4
legendární	legendární	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
týmy	tým	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
stadionu	stadion	k1gInSc6
Old	Olda	k1gFnPc2
Trafford	Trafford	k1gInSc4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
75	#num#	k4
643	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
měl	mít	k5eAaImAgInS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
období	období	k1gNnSc6
čtyři	čtyři	k4xCgFnPc4
slavné	slavný	k2eAgFnPc4d1
éry	éra	k1gFnPc4
-	-	kIx~
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
J.	J.	kA
Ernesta	Ernest	k1gMnSc4
Mangnalla	Mangnall	k1gMnSc4
<g/>
,	,	kIx,
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
a	a	k8xC
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
za	za	k7c4
legendárního	legendární	k2eAgMnSc4d1
Sira	sir	k1gMnSc4
Matta	Matt	k1gMnSc4
Busbyho	Busby	k1gMnSc4
a	a	k8xC
v	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
až	až	k9
do	do	k7c2
současnosti	současnost	k1gFnSc2
pod	pod	k7c7
taktovkou	taktovka	k1gFnSc7
Sira	sir	k1gMnSc2
Alexe	Alex	k1gMnSc5
Fergusona	Ferguson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
těchto	tento	k3xDgNnPc2
úspěšných	úspěšný	k2eAgNnPc2d1
období	období	k1gNnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
dějinách	dějiny	k1gFnPc6
klubu	klub	k1gInSc2
důležitá	důležitý	k2eAgFnSc1d1
událost	událost	k1gFnSc1
známá	známý	k2eAgFnSc1d1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Mnichovské	mnichovský	k2eAgNnSc4d1
letecké	letecký	k2eAgNnSc4d1
neštěstí	neštěstí	k1gNnSc4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgNnSc6,k3yIgNnSc6,k3yQgNnSc6
se	se	k3xPyFc4
krátce	krátce	k6eAd1
po	po	k7c6
startu	start	k1gInSc6
z	z	k7c2
mnichovského	mnichovský	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
zřítilo	zřítit	k5eAaPmAgNnS
letadlo	letadlo	k1gNnSc1
s	s	k7c7
manchesterskými	manchesterský	k2eAgMnPc7d1
fotbalisty	fotbalista	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osm	osm	k4xCc1
jich	on	k3xPp3gMnPc2
tragédii	tragédie	k1gFnSc3
nepřežilo	přežít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stadionu	stadion	k1gInSc6
Old	Olda	k1gFnPc2
Trafford	Trafforda	k1gFnPc2
neustále	neustále	k6eAd1
běží	běžet	k5eAaImIp3nP
hodiny	hodina	k1gFnPc4
měřící	měřící	k2eAgInSc4d1
čas	čas	k1gInSc4
od	od	k7c2
této	tento	k3xDgFnSc2
tragédie	tragédie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Počátky	počátek	k1gInPc1
(	(	kIx(
<g/>
1878	#num#	k4
-	-	kIx~
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
jako	jako	k9
Newton	newton	k1gInSc1
Heath	Heatha	k1gFnPc2
(	(	kIx(
<g/>
Lancashire	Lancashir	k1gInSc5
&	&	k?
Yorkshire	Yorkshir	k1gInSc5
Railway	Railwa	k1gMnPc7
<g/>
)	)	kIx)
pracovníky	pracovník	k1gMnPc7
dráhy	dráha	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1878	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
bylo	být	k5eAaImAgNnS
brzy	brzy	k6eAd1
zkráceno	zkrátit	k5eAaPmNgNnS
na	na	k7c4
Newton	newton	k1gInSc4
Heath	Heatha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1889	#num#	k4
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
Fotbalové	fotbalový	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
(	(	kIx(
<g/>
Football	Football	k1gInSc1
Alliance	Allianec	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
sloučila	sloučit	k5eAaPmAgFnS
s	s	k7c7
Fotbalovou	fotbalový	k2eAgFnSc7d1
ligou	liga	k1gFnSc7
(	(	kIx(
<g/>
Football	Football	k1gInSc1
League	Leagu	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc4
známý	známý	k2eAgInSc4d1
filmový	filmový	k2eAgInSc4d1
záznam	záznam	k1gInSc4
zápasu	zápas	k1gInSc2
United	United	k1gInSc1
je	být	k5eAaImIp3nS
z	z	k7c2
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1902	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
Manchester	Manchester	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
v	v	k7c4
Burnley	Burnle	k2eAgFnPc4d1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
bankrotu	bankrot	k1gInSc2
a	a	k8xC
zachránil	zachránit	k5eAaPmAgMnS
ho	on	k3xPp3gNnSc4
až	až	k9
J.	J.	kA
H.	H.	kA
Davies	Davies	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
zaplatil	zaplatit	k5eAaPmAgMnS
klubový	klubový	k2eAgInSc4d1
dluh	dluh	k1gInSc4
<g/>
,	,	kIx,
změnil	změnit	k5eAaPmAgInS
jméno	jméno	k1gNnSc4
klubu	klub	k1gInSc2
na	na	k7c4
Manchester	Manchester	k1gInSc4
United	United	k1gMnSc1
a	a	k8xC
změnil	změnit	k5eAaPmAgMnS
také	také	k9
barvy	barva	k1gFnPc4
klubu	klub	k1gInSc2
ze	z	k7c2
zlaté	zlatý	k2eAgFnSc2d1
a	a	k8xC
zelené	zelený	k2eAgFnSc2d1
na	na	k7c4
červenou	červený	k2eAgFnSc4d1
a	a	k8xC
bílou	bílý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1908	#num#	k4
klub	klub	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
ligu	liga	k1gFnSc4
a	a	k8xC
s	s	k7c7
Daviesovým	Daviesový	k2eAgNnSc7d1
finančním	finanční	k2eAgNnSc7d1
přispěním	přispění	k1gNnSc7
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
přestěhoval	přestěhovat	k5eAaPmAgInS
na	na	k7c4
nový	nový	k2eAgInSc4d1
stadion	stadion	k1gInSc4
Old	Olda	k1gFnPc2
Trafford	Trafforda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
dvou	dva	k4xCgFnPc2
válek	válka	k1gFnPc2
měl	mít	k5eAaImAgInS
klub	klub	k1gInSc1
velké	velký	k2eAgInPc1d1
problémy	problém	k1gInPc1
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
dlužil	dlužit	k5eAaImAgMnS
70	#num#	k4
000	#num#	k4
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Busbyho	Busbyze	k6eAd1
éra	éra	k1gFnSc1
(	(	kIx(
<g/>
1945	#num#	k4
-	-	kIx~
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Busby	Busba	k1gFnPc1
Babes	Babesa	k1gFnPc2
v	v	k7c6
Dánsku	Dánsko	k1gNnSc6
roku	rok	k1gInSc2
1955	#num#	k4
</s>
<s>
Matt	Matt	k1gMnSc1
Busby	Busba	k1gFnSc2
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
trenérem	trenér	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
a	a	k8xC
zvolil	zvolit	k5eAaPmAgInS
tenkrát	tenkrát	k6eAd1
neslýchaný	slýchaný	k2eNgInSc1d1
přístup	přístup	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
chodil	chodit	k5eAaImAgMnS
s	s	k7c7
hráči	hráč	k1gMnPc7
na	na	k7c4
hřiště	hřiště	k1gNnSc4
během	během	k7c2
tréninku	trénink	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěch	úspěch	k1gInSc1
přišel	přijít	k5eAaPmAgInS
téměř	téměř	k6eAd1
okamžitě	okamžitě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
skončil	skončit	k5eAaPmAgInS
Manchester	Manchester	k1gInSc1
druhý	druhý	k4xOgInSc4
v	v	k7c6
lize	liga	k1gFnSc6
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
dokonce	dokonce	k9
vyhrál	vyhrát	k5eAaPmAgInS
Anglický	anglický	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Busby	Busba	k1gFnPc4
měl	mít	k5eAaImAgInS
taktiku	taktika	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
obsazoval	obsazovat	k5eAaImAgInS
hráče	hráč	k1gMnPc4
z	z	k7c2
mládežnických	mládežnický	k2eAgInPc2d1
klubů	klub	k1gInPc2
kdykoliv	kdykoliv	k6eAd1
to	ten	k3xDgNnSc1
jen	jen	k9
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
klub	klub	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
ligu	liga	k1gFnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
průměrný	průměrný	k2eAgInSc1d1
věk	věk	k1gInSc1
v	v	k7c6
týmu	tým	k1gInSc6
jen	jen	k9
22	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgInSc6d1
roce	rok	k1gInSc6
United	United	k1gInSc1
znovu	znovu	k6eAd1
získaly	získat	k5eAaPmAgInP
titul	titul	k1gInSc4
a	a	k8xC
v	v	k7c6
Anglické	anglický	k2eAgFnSc6d1
poháru	pohár	k1gInSc3
prohrály	prohrát	k5eAaPmAgFnP
až	až	k9
ve	v	k7c6
finále	finále	k1gNnSc6
s	s	k7c7
Aston	Aston	k1gMnSc1
Villou	Villa	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staly	stát	k5eAaPmAgInP
se	se	k3xPyFc4
také	také	k9
prvním	první	k4xOgInSc7
klubem	klub	k1gInSc7
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgMnS
Evropského	evropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Tragédie	tragédie	k1gFnSc1
poznamenala	poznamenat	k5eAaPmAgFnS
další	další	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
se	se	k3xPyFc4
vracel	vracet	k5eAaImAgInS
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1958	#num#	k4
letadlem	letadlo	k1gNnSc7
ze	z	k7c2
zápasu	zápas	k1gInSc2
Evropského	evropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
v	v	k7c6
Bělehradě	Bělehrad	k1gInSc6
a	a	k8xC
zastavil	zastavit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
doplnění	doplnění	k1gNnSc4
paliva	palivo	k1gNnSc2
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vzletu	vzlet	k1gInSc6
v	v	k7c6
mlze	mlha	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
letadlo	letadlo	k1gNnSc1
nemohlo	moct	k5eNaImAgNnS
odlepit	odlepit	k5eAaPmF
od	od	k7c2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
přejelo	přejet	k5eAaPmAgNnS
startovací	startovací	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
a	a	k8xC
explodovalo	explodovat	k5eAaBmAgNnS
<g/>
.	.	kIx.
8	#num#	k4
hráčů	hráč	k1gMnPc2
a	a	k8xC
15	#num#	k4
dalších	další	k2eAgMnPc2d1
pasažérů	pasažér	k1gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
téměř	téměř	k6eAd1
zázrak	zázrak	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
přežil	přežít	k5eAaPmAgMnS
trenér	trenér	k1gMnSc1
Busby	Busba	k1gFnSc2
i	i	k8xC
nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
Bobby	Bobba	k1gFnSc2
Charlton	Charlton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovořilo	hovořit	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
rozkládajícím	rozkládající	k2eAgInSc6d1
týmu	tým	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
Matt	Matt	k1gMnSc1
Busby	Busba	k1gFnSc2
se	se	k3xPyFc4
rychle	rychle	k6eAd1
vyléčil	vyléčit	k5eAaPmAgMnS
ze	z	k7c2
zranění	zranění	k1gNnSc2
a	a	k8xC
klub	klub	k1gInSc1
se	se	k3xPyFc4
ještě	ještě	k9
v	v	k7c6
tom	ten	k3xDgInSc6
roce	rok	k1gInSc6
ještě	ještě	k6eAd1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
finále	finále	k1gNnSc2
Poháru	pohár	k1gInSc2
FA	fa	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
podlehl	podlehnout	k5eAaPmAgMnS
Boltonu	Bolton	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Busby	Busba	k1gFnPc1
na	na	k7c6
začátku	začátek	k1gInSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
celý	celý	k2eAgInSc1d1
tým	tým	k1gInSc1
přestavěl	přestavět	k5eAaPmAgInS
a	a	k8xC
podepsal	podepsat	k5eAaPmAgInS
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byl	být	k5eAaImAgInS
například	například	k6eAd1
Denis	Denisa	k1gFnPc2
Law	Law	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
Anglický	anglický	k2eAgInSc1d1
pohár	pohár	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
a	a	k8xC
domácí	domácí	k2eAgFnSc4d1
ligu	liga	k1gFnSc4
v	v	k7c6
letech	léto	k1gNnPc6
1965	#num#	k4
a	a	k8xC
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
porazil	porazit	k5eAaPmAgMnS
ve	v	k7c6
finále	finále	k1gNnSc6
Benficu	Benficus	k1gInSc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
získal	získat	k5eAaPmAgInS
Evropský	evropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
PMEZ	PMEZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
týmu	tým	k1gInSc6
hráli	hrát	k5eAaImAgMnP
tři	tři	k4xCgMnPc1
hráči	hráč	k1gMnPc1
s	s	k7c7
titulem	titul	k1gInSc7
Evropský	evropský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
<g/>
:	:	kIx,
Bobby	Bobba	k1gFnSc2
Charlton	Charlton	k1gInSc1
<g/>
,	,	kIx,
Denis	Denisa	k1gFnPc2
Law	Law	k1gFnSc2
a	a	k8xC
George	Georg	k1gFnSc2
Best	Besta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Busby	Busba	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
další	další	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
-	-	kIx~
1986	#num#	k4
</s>
<s>
Bryan	Bryan	k1gMnSc1
Robson	Robson	k1gMnSc1
<g/>
,	,	kIx,
dlouholetý	dlouholetý	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
Red	Red	k1gFnSc2
Devils	Devils	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
United	United	k1gInSc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
o	o	k7c4
nahrazení	nahrazení	k1gNnSc4
legendárního	legendární	k2eAgInSc2d1
Matta	Matt	k1gInSc2
Busbyho	Busby	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
Wilfu	Wilf	k1gMnSc6
McGuinnessovi	McGuinness	k1gMnSc6
či	či	k8xC
Franku	frank	k1gInSc3
O	O	kA
<g/>
'	'	kIx"
<g/>
Farrellovi	Farrell	k1gMnSc3
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
příliš	příliš	k6eAd1
nepovedlo	povést	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
týmu	tým	k1gInSc3
tedy	tedy	k8xC
přišel	přijít	k5eAaPmAgMnS
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1972	#num#	k4
Tommy	Tomma	k1gFnSc2
Docherty	Dochert	k1gInPc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zachránil	zachránit	k5eAaPmAgMnS
tým	tým	k1gInSc4
od	od	k7c2
sestupu	sestup	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
další	další	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
již	již	k6eAd1
nepovedlo	povést	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
strávil	strávit	k5eAaPmAgInS
v	v	k7c6
druhé	druhý	k4xOgFnSc6
lize	liga	k1gFnSc6
jen	jen	k9
jednu	jeden	k4xCgFnSc4
sezónu	sezóna	k1gFnSc4
a	a	k8xC
hned	hned	k6eAd1
tu	tu	k6eAd1
další	další	k2eAgMnSc1d1
se	se	k3xPyFc4
probojoval	probojovat	k5eAaPmAgMnS
do	do	k7c2
finále	finále	k1gNnSc2
Poháru	pohár	k1gInSc2
FA	fa	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
poražen	porazit	k5eAaPmNgMnS
Southamptonem	Southampton	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finálovou	finálový	k2eAgFnSc4d1
účast	účast	k1gFnSc4
zopakovali	zopakovat	k5eAaPmAgMnP
United	United	k1gMnSc1
hned	hned	k6eAd1
v	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
roku	rok	k1gInSc2
1977	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
dokonce	dokonce	k9
porazili	porazit	k5eAaPmAgMnP
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
suverénní	suverénní	k2eAgInSc1d1
Liverpool	Liverpool	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
zabránily	zabránit	k5eAaPmAgInP
mu	on	k3xPp3gMnSc3
tak	tak	k6eAd1
v	v	k7c6
zisku	zisk	k1gInSc6
prvního	první	k4xOgInSc2
treblu	trebl	k1gInSc2
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
United	United	k1gInSc4
získali	získat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
tomuto	tento	k3xDgInSc3
úspěchu	úspěch	k1gInSc3
a	a	k8xC
velké	velký	k2eAgFnSc3d1
podpoře	podpora	k1gFnSc3
fanoušků	fanoušek	k1gMnPc2
byl	být	k5eAaImAgMnS
Docherty	Dochert	k1gMnPc4
brzy	brzy	k6eAd1
po	po	k7c6
finálovém	finálový	k2eAgInSc6d1
zápase	zápas	k1gInSc6
odvolán	odvolat	k5eAaPmNgMnS
<g/>
,	,	kIx,
když	když	k8xS
vyšla	vyjít	k5eAaPmAgFnS
najevo	najevo	k6eAd1
jeho	jeho	k3xOp3gFnSc1
milostná	milostný	k2eAgFnSc1d1
aféra	aféra	k1gFnSc1
se	s	k7c7
ženou	žena	k1gFnSc7
kolegy	kolega	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
týmu	tým	k1gInSc2
nastoupil	nastoupit	k5eAaPmAgMnS
Dave	Dav	k1gInSc5
Sexton	Sexton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nelibosti	nelibost	k1gFnSc3
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
navyklí	navyklý	k2eAgMnPc1d1
na	na	k7c4
ofenzivní	ofenzivní	k2eAgFnPc4d1
taktiku	taktika	k1gFnSc4
Busbyho	Busby	k1gMnSc2
a	a	k8xC
Dochertyho	Docherty	k1gMnSc2
<g/>
,	,	kIx,
preferoval	preferovat	k5eAaImAgMnS
tento	tento	k3xDgMnSc1
trenér	trenér	k1gMnSc1
defenzivní	defenzivní	k2eAgInSc1d1
styl	styl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepodařilo	podařit	k5eNaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
získat	získat	k5eAaPmF
žádnou	žádný	k3yNgFnSc4
trofej	trofej	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
byl	být	k5eAaImAgInS
propuštěn	propustit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
Ronem	Ron	k1gMnSc7
Atkinsonem	Atkinson	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
krátce	krátce	k6eAd1
po	po	k7c6
příchodu	příchod	k1gInSc6
na	na	k7c4
Old	Olda	k1gFnPc2
Trafford	Trafforda	k1gFnPc2
překonal	překonat	k5eAaPmAgInS
britský	britský	k2eAgInSc1d1
rekord	rekord	k1gInSc1
v	v	k7c6
přestupových	přestupový	k2eAgFnPc6d1
částkách	částka	k1gFnPc6
<g/>
,	,	kIx,
když	když	k8xS
z	z	k7c2
West	Westa	k1gFnPc2
Bromwiche	Bromwich	k1gFnSc2
koupil	koupit	k5eAaPmAgMnS
Bryana	Bryan	k1gMnSc4
Robsona	Robson	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
týmu	tým	k1gInSc6
se	se	k3xPyFc4
objevili	objevit	k5eAaPmAgMnP
noví	nový	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Jesper	Jesper	k1gMnSc1
Olsen	Olsen	k2eAgMnSc1d1
a	a	k8xC
Gordon	Gordon	k1gMnSc1
Strachan	Strachan	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
hráli	hrát	k5eAaImAgMnP
po	po	k7c6
boku	bok	k1gInSc6
s	s	k7c7
odchovanci	odchovanec	k1gMnPc7
jako	jako	k8xS,k8xC
Norman	Norman	k1gMnSc1
Whiteside	Whitesid	k1gInSc5
a	a	k8xC
Mark	Mark	k1gMnSc1
Hughes	Hughes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
a	a	k8xC
1985	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
tým	tým	k1gInSc1
Pohár	pohár	k1gInSc4
FA	fa	kA
a	a	k8xC
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1985-86	1985-86	k4
byl	být	k5eAaImAgMnS
největším	veliký	k2eAgMnSc7d3
kandidátem	kandidát	k1gMnSc7
na	na	k7c4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
dokázal	dokázat	k5eAaPmAgMnS
zvítězit	zvítězit	k5eAaPmF
ve	v	k7c6
všech	všecek	k3xTgFnPc6
prvních	první	k4xOgFnPc6
deseti	deset	k4xCc6
zápasech	zápas	k1gInPc6
sezóny	sezóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc4
měli	mít	k5eAaImAgMnP
na	na	k7c6
začátku	začátek	k1gInSc6
října	říjen	k1gInSc2
náskok	náskok	k1gInSc4
10	#num#	k4
bodů	bod	k1gInPc2
před	před	k7c7
soupeři	soupeř	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
ale	ale	k9
přišel	přijít	k5eAaPmAgInS
pokles	pokles	k1gInSc1
formy	forma	k1gFnSc2
a	a	k8xC
výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
jen	jen	k9
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špatné	špatný	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
měl	mít	k5eAaImAgInS
tým	tým	k1gInSc1
i	i	k8xC
další	další	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
pohyboval	pohybovat	k5eAaImAgInS
na	na	k7c6
hraně	hrana	k1gFnSc6
sestupu	sestup	k1gInSc2
a	a	k8xC
Atkinson	Atkinson	k1gMnSc1
byl	být	k5eAaImAgMnS
odvolán	odvolat	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Éra	éra	k1gFnSc1
Sira	sir	k1gMnSc2
Alexe	Alex	k1gMnSc5
Fergusona	Ferguson	k1gMnSc2
(	(	kIx(
<g/>
1986	#num#	k4
-	-	kIx~
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
</s>
<s>
Atkinsona	Atkinson	k1gMnSc4
nahradil	nahradit	k5eAaPmAgMnS
na	na	k7c6
trenérské	trenérský	k2eAgFnSc6d1
lavičce	lavička	k1gFnSc6
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
a	a	k8xC
získal	získat	k5eAaPmAgMnS
s	s	k7c7
ním	on	k3xPp3gMnSc7
v	v	k7c6
první	první	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k6eAd1
v	v	k7c6
další	další	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
)	)	kIx)
obsadily	obsadit	k5eAaPmAgInP
United	United	k1gInSc4
druhou	druhý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
a	a	k8xC
Brian	Brian	k1gMnSc1
McClair	McClair	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
od	od	k7c2
dob	doba	k1gFnPc2
George	Georg	k1gMnSc2
Besta	Best	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
nastřílel	nastřílet	k5eAaPmAgMnS
v	v	k7c6
rudém	rudý	k2eAgInSc6d1
dresu	dres	k1gInSc6
20	#num#	k4
branek	branka	k1gFnPc2
za	za	k7c4
sezónu	sezóna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Avšak	avšak	k8xC
další	další	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
nepovedla	povést	k5eNaPmAgFnS
a	a	k8xC
nové	nový	k2eAgFnSc2d1
Fergusonovy	Fergusonův	k2eAgFnSc2d1
posily	posila	k1gFnSc2
nesplnily	splnit	k5eNaPmAgInP
očekávání	očekávání	k1gNnSc4
fanoušků	fanoušek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
1990	#num#	k4
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
spekulace	spekulace	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
trenér	trenér	k1gMnSc1
vyhozen	vyhodit	k5eAaPmNgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
výhra	výhra	k1gFnSc1
ve	v	k7c6
třetím	třetí	k4xOgInSc6
kole	kolo	k1gNnSc6
Poháru	pohár	k1gInSc2
FA	fa	kA
udržela	udržet	k5eAaPmAgFnS
Fergusona	Fergusona	k1gFnSc1
v	v	k7c6
pozici	pozice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
pak	pak	k6eAd1
rudí	rudý	k2eAgMnPc1d1
ďáblové	ďábel	k1gMnPc1
tuto	tento	k3xDgFnSc4
trofej	trofej	k1gFnSc4
vyhráli	vyhrát	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Devadesátá	devadesátý	k4xOgNnPc4
léta	léto	k1gNnPc4
představovali	představovat	k5eAaImAgMnP
nejslavnější	slavný	k2eAgFnSc4d3
kapitolu	kapitola	k1gFnSc4
v	v	k7c6
dějinách	dějiny	k1gFnPc6
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vítězství	vítězství	k1gNnSc6
v	v	k7c6
Poháru	pohár	k1gInSc6
FA	fa	k1gNnSc2
navázali	navázat	k5eAaPmAgMnP
United	United	k1gInSc4
v	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
vítězstvím	vítězství	k1gNnSc7
v	v	k7c6
Poháru	pohár	k1gInSc6
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
porazili	porazit	k5eAaPmAgMnP
Barcelonu	Barcelona	k1gFnSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
vše	všechen	k3xTgNnSc1
ještě	ještě	k9
podtrhli	podtrhnout	k5eAaPmAgMnP
ziskem	zisk	k1gInSc7
Superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
proti	proti	k7c3
CZ	CZ	kA
Bělehrad	Bělehrad	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěchy	úspěch	k1gInPc4
v	v	k7c6
pohárových	pohárový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
v	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
a	a	k8xC
osmdesátých	osmdesátý	k4xOgNnPc6
létech	léto	k1gNnPc6
však	však	k9
nemohli	moct	k5eNaImAgMnP
-	-	kIx~
přes	přes	k7c4
nespornou	sporný	k2eNgFnSc4d1
popularitu	popularita	k1gFnSc4
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
-	-	kIx~
vynahradit	vynahradit	k5eAaPmF
vítězství	vítězství	k1gNnSc4
v	v	k7c6
ligové	ligový	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
United	United	k1gInSc1
přes	přes	k7c4
velmi	velmi	k6eAd1
nadějný	nadějný	k2eAgInSc4d1
průběh	průběh	k1gInSc4
nakonec	nakonec	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
ligovou	ligový	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
1991-92	1991-92	k4
(	(	kIx(
<g/>
druzí	druhý	k4xOgMnPc1
za	za	k7c2
Leeds	Leedsa	k1gFnPc2
United	United	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vypadalo	vypadat	k5eAaImAgNnS,k5eAaPmAgNnS
to	ten	k3xDgNnSc1
na	na	k7c6
prokletí	prokletí	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
prostě	prostě	k9
nedokážou	dokázat	k5eNaPmIp3nP
překonat	překonat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Následující	následující	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
však	však	k9
nepřinesla	přinést	k5eNaPmAgFnS
jen	jen	k9
změnu	změna	k1gFnSc4
názvu	název	k1gInSc2
nejvyšší	vysoký	k2eAgFnSc2d3
anglické	anglický	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
(	(	kIx(
<g/>
z	z	k7c2
First	First	k1gFnSc1
Division	Division	k1gInSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
příchod	příchod	k1gInSc1
hráče	hráč	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
pro	pro	k7c4
tým	tým	k1gInSc4
Alexe	Alex	k1gMnSc5
Fergusona	Ferguson	k1gMnSc4
znamenal	znamenat	k5eAaImAgInS
svorník	svorník	k1gInSc1
pevně	pevně	k6eAd1
propojující	propojující	k2eAgFnSc4d1
celou	celý	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
-	-	kIx~
Erica	Eric	k2eAgFnSc1d1
Cantony	Canton	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1992-93	1992-93	k4
United	United	k1gMnSc1
získali	získat	k5eAaPmAgMnP
první	první	k4xOgInSc4
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
po	po	k7c6
26	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
hned	hned	k6eAd1
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
jej	on	k3xPp3gNnSc4
obhájili	obhájit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zisk	zisk	k1gInSc1
druhého	druhý	k4xOgInSc2
titulu	titul	k1gInSc2
v	v	k7c6
řadě	řada	k1gFnSc6
ještě	ještě	k6eAd1
okořenili	okořenit	k5eAaPmAgMnP
vítězstvím	vítězství	k1gNnSc7
v	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
a	a	k8xC
tedy	tedy	k9
prvním	první	k4xOgMnSc7
double	double	k2eAgMnSc7d1
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
;	;	kIx,
v	v	k7c6
téže	tenže	k3xDgFnSc6,k3xTgFnSc6
sezóně	sezóna	k1gFnSc6
těsně	těsně	k6eAd1
přišli	přijít	k5eAaPmAgMnP
o	o	k7c6
„	„	k?
<g/>
domácí	domácí	k1gMnSc1
treble	treble	k6eAd1
<g/>
“	“	k?
prohrou	prohra	k1gFnSc7
ve	v	k7c6
finále	finále	k1gNnSc6
Ligového	ligový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
United	United	k1gMnSc1
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
evropské	evropský	k2eAgFnSc2d1
klubové	klubový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
-	-	kIx~
Ligy	liga	k1gFnPc1
Mistrů	mistr	k1gMnPc2
-	-	kIx~
ve	v	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
však	však	k9
při	při	k7c6
svých	svůj	k3xOyFgFnPc6
dvou	dva	k4xCgFnPc6
prvních	první	k4xOgFnPc6
účastech	účast	k1gFnPc6
neuspěli	uspět	k5eNaPmAgMnP
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
kvůli	kvůli	k7c3
malým	malý	k2eAgFnPc3d1
mezinárodním	mezinárodní	k2eAgFnPc3d1
zkušenostem	zkušenost	k1gFnPc3
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
také	také	k9
kvůli	kvůli	k7c3
limitovanému	limitovaný	k2eAgInSc3d1
počtu	počet	k1gInSc3
cizinců	cizinec	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
počítali	počítat	k5eAaImAgMnP
i	i	k9
Irové	Ir	k1gMnPc1
<g/>
,	,	kIx,
Velšané	Velšan	k1gMnPc1
a	a	k8xC
Skotové	Skot	k1gMnPc1
a	a	k8xC
tým	tým	k1gInSc1
tak	tak	k6eAd1
nemohl	moct	k5eNaImAgInS
nastupovat	nastupovat	k5eAaImF
v	v	k7c6
nejsilnější	silný	k2eAgFnSc6d3
sestavě	sestava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezóna	sezóna	k1gFnSc1
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
byla	být	k5eAaImAgFnS
pro	pro	k7c4
klub	klub	k1gInSc4
dost	dost	k6eAd1
nešťastná	šťastný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezónu	sezóna	k1gFnSc4
nedohrál	dohrát	k5eNaPmAgMnS
Eric	Eric	k1gInSc4
Cantona	Canton	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgInS
vyloučen	vyloučit	k5eAaPmNgMnS
na	na	k7c4
Crystal	Crystal	k1gFnSc4
Palace	Palace	k1gFnSc2
a	a	k8xC
při	při	k7c6
odchodu	odchod	k1gInSc6
ze	z	k7c2
hřiště	hřiště	k1gNnSc2
napadl	napadnout	k5eAaPmAgMnS
fanouška	fanoušek	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
jej	on	k3xPp3gNnSc4
hrubě	hrubě	k6eAd1
urážel	urážet	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
;	;	kIx,
Cantona	Cantona	k1gFnSc1
byl	být	k5eAaImAgInS
distancován	distancován	k2eAgMnSc1d1
až	až	k9
do	do	k7c2
podzimu	podzim	k1gInSc2
a	a	k8xC
United	United	k1gInSc4
přišli	přijít	k5eAaPmAgMnP
jak	jak	k8xS,k8xC
o	o	k7c4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c4
poslední	poslední	k2eAgInSc4d1
den	den	k1gInSc4
sezóny	sezóna	k1gFnSc2
jen	jen	k9
remizovali	remizovat	k5eAaPmAgMnP
<g/>
,	,	kIx,
jakékoli	jakýkoli	k3yIgNnSc1
vítězství	vítězství	k1gNnSc1
by	by	kYmCp3nS
znamenalo	znamenat	k5eAaImAgNnS
třetí	třetí	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
o	o	k7c6
FA	fa	k1gNnSc6
Cup	cup	k1gInSc4
-	-	kIx~
ve	v	k7c6
finále	finále	k1gNnSc6
soutěže	soutěž	k1gFnSc2
prohráli	prohrát	k5eAaPmAgMnP
s	s	k7c7
tehdy	tehdy	k6eAd1
výrazně	výrazně	k6eAd1
slabším	slabý	k2eAgInSc7d2
Evertonem	Everton	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
sezóny	sezóna	k1gFnSc2
byl	být	k5eAaImAgInS
navíc	navíc	k6eAd1
poznamenán	poznamenat	k5eAaPmNgInS
odchodem	odchod	k1gInSc7
tří	tři	k4xCgFnPc2
opor	opora	k1gFnPc2
-	-	kIx~
Hughese	Hughese	k1gFnSc1
<g/>
,	,	kIx,
Kančelskise	Kančelskise	k1gFnSc1
a	a	k8xC
Ince	Inka	k1gFnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
nekompenzoval	kompenzovat	k5eNaBmAgMnS
drahými	drahý	k2eAgInPc7d1
nákupy	nákup	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
nasazením	nasazení	k1gNnSc7
neostřílených	ostřílený	k2eNgMnPc2d1
mladíčků	mladíček	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
Ryanu	Ryan	k1gMnSc3
Giggsovi	Giggs	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
týmu	tým	k1gInSc6
zářil	zářit	k5eAaImAgMnS
již	již	k6eAd1
několik	několik	k4yIc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
přidali	přidat	k5eAaPmAgMnP
další	další	k2eAgMnPc1d1
odchovanci	odchovanec	k1gMnPc1
-	-	kIx~
bratři	bratr	k1gMnPc1
Nevillové	Nevillový	k2eAgFnSc2d1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
<g/>
,	,	kIx,
Nicky	nicka	k1gFnPc1
Butt	Buttum	k1gNnPc2
a	a	k8xC
David	David	k1gMnSc1
Beckham	Beckham	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
porážku	porážka	k1gFnSc4
v	v	k7c6
úvodním	úvodní	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
dalšího	další	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
bývalý	bývalý	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
FC	FC	kA
Liverpool	Liverpool	k1gInSc1
Alan	alan	k1gInSc1
Hansen	Hansen	k1gInSc4
komentoval	komentovat	k5eAaBmAgInS
slovy	slovo	k1gNnPc7
„	„	k?
<g/>
s	s	k7c7
dětmi	dítě	k1gFnPc7
nemůžete	moct	k5eNaImIp2nP
vyhrávat	vyhrávat	k5eAaImF
trofeje	trofej	k1gFnPc1
<g/>
“	“	k?
se	se	k3xPyFc4
sezóna	sezóna	k1gFnSc1
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
proměnila	proměnit	k5eAaPmAgFnS
v	v	k7c4
další	další	k2eAgInSc4d1
úspěšný	úspěšný	k2eAgInSc4d1
rok	rok	k1gInSc4
-	-	kIx~
po	po	k7c6
návratu	návrat	k1gInSc6
Erica	Eric	k1gInSc2
Cantony	Canton	k1gInPc1
United	United	k1gMnSc1
získali	získat	k5eAaPmAgMnP
další	další	k2eAgInSc4d1
titul	titul	k1gInSc4
resp.	resp.	kA
double	double	k2eAgMnSc1d1
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
gólem	gól	k1gInSc7
Cantony	Canton	k1gInPc1
<g/>
)	)	kIx)
porazili	porazit	k5eAaPmAgMnP
-	-	kIx~
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
tým	tým	k1gInSc1
opět	opět	k6eAd1
obhájil	obhájit	k5eAaPmAgInS
titul	titul	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
poměrně	poměrně	k6eAd1
nešťastně	šťastně	k6eNd1
vypadl	vypadnout	k5eAaPmAgMnS
v	v	k7c6
semifinále	semifinále	k1gNnSc6
Ligy	liga	k1gFnSc2
Mistrů	mistr	k1gMnPc2
s	s	k7c7
pozdějším	pozdní	k2eAgMnSc7d2
vítězem	vítěz	k1gMnSc7
Dortmundem	Dortmund	k1gInSc7
a	a	k8xC
hlavně	hlavně	k9
Eric	Eric	k1gFnSc4
Cantona	Canton	k1gMnSc2
nečekaně	nečekaně	k6eAd1
ukončil	ukončit	k5eAaPmAgMnS
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
po	po	k7c6
30	#num#	k4
letech	léto	k1gNnPc6
</s>
<s>
Ryan	Ryan	k1gMnSc1
Giggs	Giggsa	k1gFnPc2
</s>
<s>
Do	do	k7c2
sezóny	sezóna	k1gFnSc2
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
klub	klub	k1gInSc1
vstoupil	vstoupit	k5eAaPmAgInS
klub	klub	k1gInSc4
velmi	velmi	k6eAd1
nadějně	nadějně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
lednu	leden	k1gInSc6
United	United	k1gInSc1
vedli	vést	k5eAaImAgMnP
suverénně	suverénně	k6eAd1
ligu	liga	k1gFnSc4
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
-	-	kIx~
po	po	k7c6
hladkém	hladký	k2eAgInSc6d1
průběhu	průběh	k1gInSc6
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
-	-	kIx~
zajištěno	zajištěn	k2eAgNnSc1d1
čtvrtfinále	čtvrtfinále	k1gNnSc1
Ligy	liga	k1gFnSc2
Mistrů	mistr	k1gMnPc2
a	a	k8xC
když	když	k8xS
v	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
vedli	vést	k5eAaImAgMnP
v	v	k7c6
průběhu	průběh	k1gInSc6
zápasu	zápas	k1gInSc2
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c7
Chelsea	Chelseum	k1gNnSc2
na	na	k7c4
Stamford	Stamford	k1gInSc4
Bridge	Bridg	k1gFnSc2
<g/>
,	,	kIx,
vypadalo	vypadat	k5eAaPmAgNnS,k5eAaImAgNnS
to	ten	k3xDgNnSc1
na	na	k7c4
další	další	k2eAgFnPc4d1
triumfální	triumfální	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaro	jaro	k6eAd1
však	však	k9
přineslo	přinést	k5eAaPmAgNnS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
zranění	zranění	k1gNnSc2
a	a	k8xC
když	když	k8xS
v	v	k7c6
jednom	jeden	k4xCgInSc6
týdnu	týden	k1gInSc6
United	United	k1gInSc1
prohráli	prohrát	k5eAaPmAgMnP
v	v	k7c6
lize	liga	k1gFnSc6
doma	doma	k6eAd1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
s	s	k7c7
mocně	mocně	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
tabulce	tabulka	k1gFnSc6
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
dotahujícím	dotahující	k2eAgInSc7d1
Arsenalem	Arsenal	k1gInSc7
a	a	k8xC
následně	následně	k6eAd1
vypadli	vypadnout	k5eAaPmAgMnP
z	z	k7c2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
s	s	k7c7
AS	as	k9
Monaco	Monaco	k6eAd1
<g/>
,	,	kIx,
přišla	přijít	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
nazmar	nazmar	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
sezónou	sezóna	k1gFnSc7
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
jednou	jednou	k6eAd1
z	z	k7c2
nejúžasnějších	úžasný	k2eAgInPc2d3
v	v	k7c6
dějinách	dějiny	k1gFnPc6
klubového	klubový	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
-	-	kIx~
United	United	k1gMnSc1
posílili	posílit	k5eAaPmAgMnP
Jaap	Jaap	k1gMnSc1
Stam	Stam	k1gMnSc1
-	-	kIx~
nizozemský	nizozemský	k2eAgInSc1d1
pilíř	pilíř	k1gInSc1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
Jesper	Jesper	k1gMnSc1
Blomqvist	Blomqvist	k1gMnSc1
-	-	kIx~
skvělá	skvělý	k2eAgFnSc1d1
alternace	alternace	k1gFnSc1
k	k	k7c3
Ryanu	Ryan	k1gMnSc3
Giggsovi	Giggs	k1gMnSc3
a	a	k8xC
Dwight	Dwight	k2eAgInSc1d1
Yorke	Yorke	k1gInSc1
-	-	kIx~
trinidadský	trinidadský	k2eAgMnSc1d1
střelec	střelec	k1gMnSc1
získaný	získaný	k2eAgMnSc1d1
z	z	k7c2
Aston	Astona	k1gFnPc2
Villy	Villa	k1gFnSc2
za	za	k7c2
tehdy	tehdy	k6eAd1
rekordních	rekordní	k2eAgFnPc2d1
12,6	12,6	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
United	United	k1gMnSc1
získali	získat	k5eAaPmAgMnP
„	„	k?
<g/>
treble	treble	k6eAd1
<g/>
“	“	k?
(	(	kIx(
<g/>
tzn.	tzn.	kA
vyhráli	vyhrát	k5eAaPmAgMnP
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
domácí	domácí	k2eAgFnPc4d1
soutěže	soutěž	k1gFnPc4
plus	plus	k6eAd1
titul	titul	k1gInSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
resp.	resp.	kA
PMEZ	PMEZ	kA
<g/>
)	)	kIx)
jako	jako	k9
čtvrtý	čtvrtý	k4xOgInSc1
evropský	evropský	k2eAgInSc1d1
klub	klub	k1gInSc1
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
ním	on	k3xPp3gNnSc7
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
zdařilo	zdařit	k5eAaPmAgNnS
pouze	pouze	k6eAd1
skotskému	skotský	k2eAgMnSc3d1
Celticu	Celtic	k1gMnSc3
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
nizozemským	nizozemský	k2eAgInPc3d1
celkům	celek	k1gInPc3
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgMnSc1d1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trofeje	trofej	k1gFnPc1
z	z	k7c2
mistrovského	mistrovský	k2eAgInSc2d1
treblu	trebl	k1gInSc2
1999	#num#	k4
</s>
<s>
Boj	boj	k1gInSc1
o	o	k7c4
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
s	s	k7c7
Arsenalem	Arsenal	k1gInSc7
rozhodl	rozhodnout	k5eAaPmAgInS
Manchester	Manchester	k1gInSc1
až	až	k9
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
poháru	pohár	k1gInSc6
FA	fa	k1gNnSc2
museli	muset	k5eAaImAgMnP
jeho	jeho	k3xOp3gMnPc1
hráči	hráč	k1gMnPc1
porazit	porazit	k5eAaPmF
v	v	k7c6
podstatě	podstata	k1gFnSc6
všechny	všechen	k3xTgInPc1
špičkové	špičkový	k2eAgInPc1d1
týmy	tým	k1gInPc1
Anglie	Anglie	k1gFnSc2
(	(	kIx(
<g/>
Liverpool	Liverpool	k1gInSc1
<g/>
,	,	kIx,
Chelsea	Chelsea	k1gMnSc1
<g/>
,	,	kIx,
Arsenal	Arsenal	k1gMnSc1
a	a	k8xC
Newcastle	Newcastle	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
zejména	zejména	k9
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
s	s	k7c7
Liverpoolem	Liverpool	k1gInSc7
(	(	kIx(
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
góly	gól	k1gInPc4
v	v	k7c6
posledních	poslední	k2eAgFnPc6d1
minutách	minuta	k1gFnPc6
zápasu	zápas	k1gInSc2
-	-	kIx~
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
vítězství	vítězství	k1gNnSc1
v	v	k7c6
opakovaném	opakovaný	k2eAgNnSc6d1
semifinále	semifinále	k1gNnSc6
s	s	k7c7
Arsenalem	Arsenal	k1gInSc7
(	(	kIx(
<g/>
za	za	k7c2
stavu	stav	k1gInSc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
minutě	minuta	k1gFnSc6
chytil	chytit	k5eAaPmAgMnS
Peter	Peter	k1gMnSc1
Schmeichel	Schmeichel	k1gMnSc1
penaltu	penalta	k1gFnSc4
Dennisi	Dennis	k1gMnSc3
Bergkampovi	Bergkamp	k1gMnSc3
a	a	k8xC
v	v	k7c6
nastavení	nastavení	k1gNnSc6
-	-	kIx~
kdy	kdy	k6eAd1
United	United	k1gMnSc1
již	již	k6eAd1
byli	být	k5eAaImAgMnP
bez	bez	k7c2
vyloučeného	vyloučený	k2eAgInSc2d1
R.	R.	kA
Keanea	Keanea	k1gMnSc1
-	-	kIx~
dal	dát	k5eAaPmAgMnS
Ryan	Ryan	k1gMnSc1
Giggs	Giggs	k1gInSc4
po	po	k7c6
sólu	sólo	k1gNnSc6
přes	přes	k7c4
6	#num#	k4
hráčů	hráč	k1gMnPc2
vítězný	vítězný	k2eAgInSc4d1
gól	gól	k1gInSc4
na	na	k7c4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
vešly	vejít	k5eAaPmAgFnP
do	do	k7c2
dějin	dějiny	k1gFnPc2
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholem	vrchol	k1gInSc7
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
účinkování	účinkování	k1gNnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
Mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
postupu	postup	k1gInSc6
ze	z	k7c2
základní	základní	k2eAgFnSc2d1
„	„	k?
<g/>
skupiny	skupina	k1gFnSc2
smrti	smrt	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
s	s	k7c7
Bayernem	Bayern	k1gInSc7
Mnichov	Mnichov	k1gInSc1
a	a	k8xC
Barcelonou	Barcelona	k1gFnSc7
<g/>
)	)	kIx)
United	United	k1gInSc1
přešli	přejít	k5eAaPmAgMnP
přes	přes	k7c4
Inter	Inter	k1gInSc4
Milán	Milán	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Juventus	Juventus	k1gInSc1
Turín	Turín	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
doma	doma	k6eAd1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
v	v	k7c6
Turíně	Turín	k1gInSc6
<g/>
)	)	kIx)
až	až	k9
do	do	k7c2
finále	finále	k1gNnSc2
-	-	kIx~
opět	opět	k6eAd1
proti	proti	k7c3
Bayernem	Bayern	k1gInSc7
Mnichov	Mnichov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záloha	záloha	k1gFnSc1
Giggs	Giggsa	k1gFnPc2
-	-	kIx~
Keane	Kean	k1gInSc5
-	-	kIx~
Scholes	Scholes	k1gMnSc1
-	-	kIx~
Beckham	Beckham	k1gInSc1
se	se	k3xPyFc4
díky	díky	k7c3
předvedeným	předvedený	k2eAgInPc3d1
výkonům	výkon	k1gInPc3
ještě	ještě	k9
„	„	k?
<g/>
za	za	k7c2
života	život	k1gInSc2
<g/>
“	“	k?
stala	stát	k5eAaPmAgFnS
legendou	legenda	k1gFnSc7
a	a	k8xC
útočný	útočný	k2eAgInSc1d1
tandem	tandem	k1gInSc1
Dwight	Dwight	k2eAgInSc1d1
Yorke	Yorke	k1gInSc1
-	-	kIx~
Andy	Anda	k1gFnPc1
Cole	cola	k1gFnSc6
rovněž	rovněž	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgNnSc1d1
finále	finále	k1gNnSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
nebylo	být	k5eNaImAgNnS
z	z	k7c2
pohledu	pohled	k1gInSc2
United	United	k1gMnSc1
nejlepším	dobrý	k2eAgInSc7d3
zápasem	zápas	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chyběli	chybět	k5eAaImAgMnP
po	po	k7c6
semifinále	semifinále	k1gNnSc6
distancovaní	distancovaný	k2eAgMnPc1d1
Keane	Kean	k1gInSc5
a	a	k8xC
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
a	a	k8xC
improvizovaná	improvizovaný	k2eAgFnSc1d1
záloha	záloha	k1gFnSc1
nepodala	podat	k5eNaPmAgFnS
srovnatelný	srovnatelný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bayern	Bayern	k1gInSc1
se	se	k3xPyFc4
navíc	navíc	k6eAd1
brzy	brzy	k6eAd1
a	a	k8xC
lacině	lacině	k6eAd1
ujal	ujmout	k5eAaPmAgInS
vedení	vedení	k1gNnSc4
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
další	další	k2eAgFnPc4d1
šance	šance	k1gFnPc4
(	(	kIx(
<g/>
tyč	tyč	k1gFnSc4
a	a	k8xC
břevno	břevno	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
ještě	ještě	k9
na	na	k7c6
začátku	začátek	k1gInSc6
nastavení	nastavení	k1gNnSc2
vedl	vést	k5eAaImAgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
však	však	k9
po	po	k7c6
dvou	dva	k4xCgInPc6
rozích	roh	k1gInPc6
Davida	David	k1gMnSc2
Beckhama	Beckhamum	k1gNnSc2
během	během	k7c2
dvou	dva	k4xCgFnPc2
minut	minuta	k1gFnPc2
zápas	zápas	k1gInSc4
otočili	otočit	k5eAaPmAgMnP
náhradníci	náhradník	k1gMnPc1
Teddy	Tedda	k1gFnSc2
Sheringham	Sheringham	k1gInSc1
a	a	k8xC
Ole	Ola	k1gFnSc6
Gunnar	Gunnara	k1gFnPc2
Solskjæ	Solskjæ	k1gInSc1
<g/>
,	,	kIx,
United	United	k1gInSc1
zvítězili	zvítězit	k5eAaPmAgMnP
ve	v	k7c4
finále	finále	k1gNnSc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
ziskem	zisk	k1gInSc7
„	„	k?
<g/>
treble	treble	k6eAd1
<g/>
“	“	k?
připravili	připravit	k5eAaPmAgMnP
fanouškům	fanoušek	k1gMnPc3
klubu	klub	k1gInSc2
největší	veliký	k2eAgInSc4d3
zážitek	zážitek	k1gInSc4
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finále	finále	k1gNnSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
navíc	navíc	k6eAd1
symbolicky	symbolicky	k6eAd1
tento	tento	k3xDgInSc4
rok	rok	k1gInSc4
připadlo	připadnout	k5eAaPmAgNnS
přesně	přesně	k6eAd1
na	na	k7c4
26	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1999	#num#	k4
a	a	k8xC
tedy	tedy	k9
na	na	k7c4
90	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc4
narození	narození	k1gNnSc2
Matta	Matt	k1gMnSc2
Busbyho	Busby	k1gMnSc2
(	(	kIx(
<g/>
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Sir	sir	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Matthew	Matthew	k1gMnSc1
Busby	Busba	k1gFnSc2
<g/>
)	)	kIx)
pod	pod	k7c7
jehož	jehož	k3xOyRp3gFnSc7
taktovkou	taktovka	k1gFnSc7
kdysi	kdysi	k6eAd1
Manchester	Manchester	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
nejprestižnější	prestižní	k2eAgFnSc6d3
evropské	evropský	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
předchůdce	předchůdce	k1gMnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Treble	Treble	k1gFnSc1
<g/>
"	"	kIx"
navíc	navíc	k6eAd1
korunovali	korunovat	k5eAaBmAgMnP
na	na	k7c6
sklonku	sklonek	k1gInSc6
roku	rok	k1gInSc2
vítězstvím	vítězství	k1gNnSc7
v	v	k7c6
Interkontinentálním	interkontinentální	k2eAgInSc6d1
poháru	pohár	k1gInSc6
nad	nad	k7c4
Palmeiras	Palmeiras	k1gInSc4
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
(	(	kIx(
<g/>
gólem	gól	k1gInSc7
Roye	Roy	k1gMnSc2
Keanea	Keaneus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
historicky	historicky	k6eAd1
překonali	překonat	k5eAaPmAgMnP
své	svůj	k3xOyFgMnPc4
předchůdce	předchůdce	k1gMnPc4
z	z	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
United	United	k1gInSc4
prohráli	prohrát	k5eAaPmAgMnP
po	po	k7c6
vítězství	vítězství	k1gNnSc6
v	v	k7c6
PMEZ	PMEZ	kA
s	s	k7c7
Estudiantes	Estudiantesa	k1gFnPc2
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
-	-	kIx~
2005	#num#	k4
</s>
<s>
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
dvě	dva	k4xCgFnPc1
sezóny	sezóna	k1gFnPc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
a	a	k8xC
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
měli	mít	k5eAaImAgMnP
velmi	velmi	k6eAd1
podobný	podobný	k2eAgInSc4d1
průběh	průběh	k1gInSc4
-	-	kIx~
United	United	k1gInSc1
poměrně	poměrně	k6eAd1
s	s	k7c7
přehledem	přehled	k1gInSc7
vyhráli	vyhrát	k5eAaPmAgMnP
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
Lize	liga	k1gFnSc6
Mistrů	mistr	k1gMnPc2
vypadli	vypadnout	k5eAaPmAgMnP
vždy	vždy	k6eAd1
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
hráli	hrát	k5eAaImAgMnP
vynikající	vynikající	k2eAgInSc4d1
fotbal	fotbal	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
přece	přece	k9
jen	jen	k9
trpěli	trpět	k5eAaImAgMnP
odchodem	odchod	k1gInSc7
brankářské	brankářský	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
P.	P.	kA
Schmeichela	Schmeichel	k1gMnSc2
<g/>
,	,	kIx,
kterého	který	k3yIgNnSc2,k3yQgNnSc2,k3yRgNnSc2
nikdo	nikdo	k3yNnSc1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
nástupců	nástupce	k1gMnPc2
(	(	kIx(
<g/>
M.	M.	kA
Bosnich	Bosnich	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Gouw	Gouw	k1gFnSc2
<g/>
,	,	kIx,
M.	M.	kA
Taibi	Taib	k1gFnSc2
<g/>
)	)	kIx)
nedokázal	dokázat	k5eNaPmAgMnS
nahradit	nahradit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angažováním	angažování	k1gNnSc7
nizozemského	nizozemský	k2eAgMnSc2d1
kanonýra	kanonýr	k1gMnSc2
Ruuda	Ruuda	k1gFnSc1
van	van	k1gInSc1
Nistelrooije	Nistelrooije	k1gFnSc2
a	a	k8xC
postupným	postupný	k2eAgInSc7d1
odchodem	odchod	k1gInSc7
(	(	kIx(
<g/>
mj.	mj.	kA
<g/>
)	)	kIx)
Jaapa	Jaap	k1gMnSc2
Stama	Stam	k1gMnSc2
<g/>
,	,	kIx,
Dwighta	Dwight	k1gMnSc2
Yorkea	Yorkeus	k1gMnSc2
a	a	k8xC
Andy	Anda	k1gFnSc2
Colea	Cole	k1gInSc2
se	se	k3xPyFc4
složení	složení	k1gNnSc1
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
měl	mít	k5eAaImAgInS
stále	stále	k6eAd1
velký	velký	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
<g/>
,	,	kIx,
přece	přece	k9
jen	jen	k9
začalo	začít	k5eAaPmAgNnS
měnit	měnit	k5eAaImF
<g/>
;	;	kIx,
velké	velký	k2eAgFnPc4d1
naděje	naděje	k1gFnPc4
vzbudil	vzbudit	k5eAaPmAgInS
příchod	příchod	k1gInSc1
francouzského	francouzský	k2eAgMnSc2d1
brankáře	brankář	k1gMnSc2
Fabiena	Fabien	k1gMnSc2
Bartheze	Bartheze	k1gFnSc2
(	(	kIx(
<g/>
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
i	i	k9
jeho	jeho	k3xOp3gFnSc1
kariéra	kariéra	k1gFnSc1
v	v	k7c4
United	United	k1gInSc4
byla	být	k5eAaImAgFnS
vroubena	vroubit	k5eAaImNgFnS
řadou	řada	k1gFnSc7
fatálních	fatální	k2eAgFnPc2d1
chyb	chyba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
klub	klub	k1gInSc1
postoupil	postoupit	k5eAaPmAgInS
do	do	k7c2
semifinále	semifinále	k1gNnSc2
Ligy	liga	k1gFnSc2
Mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
smolně	smolně	k6eAd1
vypadl	vypadnout	k5eAaPmAgMnS
s	s	k7c7
Bayerem	Bayer	k1gMnSc7
Leverkusen	Leverkusen	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
vyhrál	vyhrát	k5eAaPmAgMnS
po	po	k7c6
skvělém	skvělý	k2eAgInSc6d1
finiši	finiš	k1gInSc6
Premier	Premiero	k1gNnPc2
League	League	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
šla	jít	k5eAaImAgFnS
křivka	křivka	k1gFnSc1
jeho	jeho	k3xOp3gInPc2
úspěchů	úspěch	k1gInPc2
poněkud	poněkud	k6eAd1
dolů	dolů	k6eAd1
a	a	k8xC
jedinou	jediný	k2eAgFnSc7d1
trofejí	trofej	k1gFnSc7
ze	z	k7c2
sezón	sezóna	k1gFnPc2
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
a	a	k8xC
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
byl	být	k5eAaImAgMnS
FA	fa	k1gNnSc4
Cup	cup	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odchodem	odchod	k1gInSc7
legend	legenda	k1gFnPc2
Davida	David	k1gMnSc2
Beckhama	Beckham	k1gMnSc2
(	(	kIx(
<g/>
2003	#num#	k4
-	-	kIx~
do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Roye	Roy	k1gMnSc4
Keanea	Keaneus	k1gMnSc4
(	(	kIx(
<g/>
2005	#num#	k4
-	-	kIx~
do	do	k7c2
Celticu	Celticus	k1gInSc2
Glasgow	Glasgow	k1gInSc1
<g/>
)	)	kIx)
zůstali	zůstat	k5eAaPmAgMnP
v	v	k7c6
klubu	klub	k1gInSc6
poslední	poslední	k2eAgMnPc1d1
mohykáni	mohykán	k1gMnPc1
z	z	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
<g/>
:	:	kIx,
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
<g/>
,	,	kIx,
Gary	Gar	k2eAgFnPc4d1
Neville	Neville	k1gFnPc4
a	a	k8xC
Ryan	Ryan	k1gInSc4
Giggs	Giggsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
nástupem	nástup	k1gInSc7
peněz	peníze	k1gInPc2
Romana	Roman	k1gMnSc4
Abramoviče	Abramovič	k1gMnSc4
do	do	k7c2
Chelsea	Chelse	k1gInSc2
FC	FC	kA
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
dominantní	dominantní	k2eAgInSc4d1
postavení	postavení	k1gNnSc4
Manchesteru	Manchester	k1gInSc2
United	United	k1gInSc1
(	(	kIx(
<g/>
soupeřícího	soupeřící	k2eAgNnSc2d1
v	v	k7c6
období	období	k1gNnSc6
1998	#num#	k4
-	-	kIx~
2003	#num#	k4
v	v	k7c6
podstatě	podstata	k1gFnSc6
jen	jen	k9
s	s	k7c7
Arsenalem	Arsenal	k1gMnSc7
<g/>
)	)	kIx)
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
skončilo	skončit	k5eAaPmAgNnS
<g/>
,	,	kIx,
klubem	klub	k1gInSc7
otřásla	otřást	k5eAaPmAgFnS
změna	změna	k1gFnSc1
vlastnických	vlastnický	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
,	,	kIx,
koupil	koupit	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
v	v	k7c6
létě	léto	k1gNnSc6
2005	#num#	k4
za	za	k7c2
velkého	velký	k2eAgInSc2d1
odporu	odpor	k1gInSc2
fanoušků	fanoušek	k1gMnPc2
americký	americký	k2eAgMnSc1d1
miliardář	miliardář	k1gMnSc1
Malcolm	Malcolm	k1gMnSc1
Glazer	Glazer	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2005-2006	2005-2006	k4
navíc	navíc	k6eAd1
United	United	k1gInSc4
poprvé	poprvé	k6eAd1
za	za	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
nepostoupili	postoupit	k5eNaPmAgMnP
ze	z	k7c2
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
hráčů	hráč	k1gMnPc2
(	(	kIx(
<g/>
vedle	vedle	k7c2
již	již	k6eAd1
zkušených	zkušený	k2eAgNnPc2d1
Ruuda	Ruudo	k1gNnSc2
van	van	k1gInSc4
Nistelrooye	Nistelrooye	k1gNnSc2
a	a	k8xC
R.	R.	kA
Ferdinanda	Ferdinand	k1gMnSc2
zejména	zejména	k9
Wayne	Wayn	k1gInSc5
Rooney	Rooney	k1gInPc1
a	a	k8xC
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
vyvolává	vyvolávat	k5eAaImIp3nS
neutuchající	utuchající	k2eNgInSc1d1
zájem	zájem	k1gInSc1
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
<g/>
byť	byť	k8xS
po	po	k7c6
převzetí	převzetí	k1gNnSc6
M.	M.	kA
Glazerem	Glazer	k1gMnSc7
přece	přece	k9
jen	jen	k9
trochu	trochu	k6eAd1
utlumený	utlumený	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
přes	přes	k7c4
určitou	určitý	k2eAgFnSc4d1
stagnaci	stagnace	k1gFnSc4
stále	stále	k6eAd1
vynikající	vynikající	k2eAgInPc1d1
ekonomické	ekonomický	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
byly	být	k5eAaImAgInP
však	však	k9
příslibem	příslib	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Rudí	rudý	k2eAgMnPc1d1
ďáblové	ďábel	k1gMnPc1
z	z	k7c2
Old	Olda	k1gFnPc2
Trafford	Trafforda	k1gFnPc2
dokážou	dokázat	k5eAaPmIp3nP
vrátit	vrátit	k5eAaPmF
zpět	zpět	k6eAd1
na	na	k7c4
pozice	pozice	k1gFnPc4
nejvyšší	vysoký	k2eAgFnPc4d3
jak	jak	k6eAd1
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
evropském	evropský	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
-	-	kIx~
2010	#num#	k4
</s>
<s>
A	a	k9
tak	tak	k6eAd1
se	se	k3xPyFc4
také	také	k9
stalo	stát	k5eAaPmAgNnS
po	po	k7c6
dvou	dva	k4xCgInPc6
titulech	titul	k1gInPc6
Chelsea	Chelseum	k1gNnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
United	United	k1gInSc4
vrátili	vrátit	k5eAaPmAgMnP
titul	titul	k1gInSc4
na	na	k7c4
Old	Olda	k1gFnPc2
Trafford	Trafford	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
ne	ne	k9
jen	jen	k9
jednou	jeden	k4xCgFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
titulu	titul	k1gInSc6
v	v	k7c6
sezoně	sezona	k1gFnSc6
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
přišla	přijít	k5eAaPmAgFnS
další	další	k2eAgFnSc1d1
skvělá	skvělý	k2eAgFnSc1d1
sezona	sezona	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
téměř	téměř	k6eAd1
vyrovnala	vyrovnat	k5eAaPmAgFnS,k5eAaBmAgFnS
té	ten	k3xDgFnSc6
památné	památný	k2eAgFnSc6d1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
na	na	k7c6
úspěchu	úspěch	k1gInSc6
měl	mít	k5eAaImAgMnS
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vstřelil	vstřelit	k5eAaPmAgMnS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
soutěžích	soutěž	k1gFnPc6
42	#num#	k4
gólů	gól	k1gInPc2
a	a	k8xC
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
také	také	k9
držitelem	držitel	k1gMnSc7
Zlatého	zlatý	k2eAgInSc2d1
míče	míč	k1gInSc2
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
hrajícího	hrající	k2eAgMnSc4d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
sezoně	sezona	k1gFnSc6
měl	mít	k5eAaImAgInS
skvělý	skvělý	k2eAgInSc4d1
podzim	podzim	k1gInSc4
Arsenal	Arsenal	k1gFnSc2
<g/>
,	,	kIx,
hrající	hrající	k2eAgInSc4d1
nejlepší	dobrý	k2eAgInSc4d3
fotbal	fotbal	k1gInSc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
jeho	jeho	k3xOp3gNnSc1
velmi	velmi	k6eAd1
mladý	mladý	k2eAgInSc1d1
tým	tým	k1gInSc1
nejspíš	nejspíš	k9
neustál	ustát	k5eNaPmAgInS
tlak	tlak	k1gInSc1
<g/>
,	,	kIx,
po	po	k7c6
přelomu	přelom	k1gInSc6
roku	rok	k1gInSc2
se	se	k3xPyFc4
propadl	propadnout	k5eAaPmAgMnS
a	a	k8xC
musel	muset	k5eAaImAgMnS
se	se	k3xPyFc4
spokojit	spokojit	k5eAaPmF
s	s	k7c7
třetím	třetí	k4xOgNnSc7
místem	místo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
titul	titul	k1gInSc4
tak	tak	k6eAd1
bojovali	bojovat	k5eAaImAgMnP
Chelsea	Chelsea	k1gMnSc1
a	a	k8xC
Manchester	Manchester	k1gInSc1
<g/>
;	;	kIx,
nakonec	nakonec	k6eAd1
tento	tento	k3xDgInSc4
boj	boj	k1gInSc4
dopadl	dopadnout	k5eAaPmAgMnS
úspěšněji	úspěšně	k6eAd2
právě	právě	k9
pro	pro	k7c4
United	United	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
semifinále	semifinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
se	se	k3xPyFc4
Manchester	Manchester	k1gInSc1
setkal	setkat	k5eAaPmAgInS
s	s	k7c7
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
přes	přes	k7c4
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
postoupil	postoupit	k5eAaPmAgInS
díky	díky	k7c3
jedinému	jediný	k2eAgInSc3d1
gólu	gól	k1gInSc3
dvojzápasu	dvojzápas	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
vstřelil	vstřelit	k5eAaPmAgMnS
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
utkal	utkat	k5eAaPmAgMnS
s	s	k7c7
Chelsea	Chelsea	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
mohla	moct	k5eAaImAgFnS
nahradit	nahradit	k5eAaPmF
vítězstvím	vítězství	k1gNnSc7
ve	v	k7c6
finále	finála	k1gFnSc6
LM	LM	kA
neúspěch	neúspěch	k1gInSc1
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
poločase	poločas	k1gInSc6
zápasu	zápas	k1gInSc2
padly	padnout	k5eAaImAgInP,k5eAaPmAgInP
oba	dva	k4xCgInPc1
dva	dva	k4xCgInPc1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
za	za	k7c4
United	United	k1gInSc4
se	se	k3xPyFc4
trefil	trefit	k5eAaPmAgMnS
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
konci	konec	k1gInSc6
poločasu	poločas	k1gInSc2
srovnal	srovnat	k5eAaPmAgMnS
Frank	Frank	k1gMnSc1
Lampard	Lampard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
góly	gól	k1gInPc1
už	už	k6eAd1
nepadly	padnout	k5eNaPmAgInP
a	a	k8xC
po	po	k7c6
prodloužení	prodloužení	k1gNnSc3
pak	pak	k6eAd1
rozhodovaly	rozhodovat	k5eAaImAgFnP
penalty	penalta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Čech	Čech	k1gMnSc1
v	v	k7c6
nich	on	k3xPp3gFnPc6
vychytal	vychytat	k5eAaPmAgMnS
Cristiana	Cristian	k1gMnSc4
Ronalda	Ronald	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
sérii	série	k1gFnSc6
mohl	moct	k5eAaImAgMnS
rozhodnout	rozhodnout	k5eAaPmF
kapitán	kapitán	k1gMnSc1
Chelsea	Chelse	k1gInSc2
John	John	k1gMnSc1
Terry	Terra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
ovšem	ovšem	k9
v	v	k7c6
klíčovém	klíčový	k2eAgInSc6d1
okamžiku	okamžik	k1gInSc6
trefil	trefit	k5eAaPmAgMnS
tyč	tyč	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Penaltu	penalta	k1gFnSc4
pak	pak	k6eAd1
proměnil	proměnit	k5eAaPmAgMnS
veterán	veterán	k1gMnSc1
Ryan	Ryan	k1gMnSc1
Giggs	Giggs	k1gInSc1
a	a	k8xC
když	když	k8xS
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Sar	Sar	k1gFnPc7
vychytal	vychytat	k5eAaPmAgMnS
Anelku	Anelka	k1gFnSc4
<g/>
,	,	kIx,
mohla	moct	k5eAaImAgFnS
propuknout	propuknout	k5eAaPmF
červená	červený	k2eAgFnSc1d1
radost	radost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediný	jediný	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
oproti	oproti	k7c3
roku	rok	k1gInSc3
1999	#num#	k4
byl	být	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
United	United	k1gMnSc1
nezískali	získat	k5eNaPmAgMnP
treble	treble	k6eAd1
a	a	k8xC
také	také	k6eAd1
nezískali	získat	k5eNaPmAgMnP
obě	dva	k4xCgFnPc4
trofeje	trofej	k1gFnPc4
tak	tak	k8xS,k8xC
strhujícím	strhující	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
jako	jako	k8xS,k8xC
na	na	k7c6
konci	konec	k1gInSc6
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
také	také	k9
zvítězili	zvítězit	k5eAaPmAgMnP
na	na	k7c6
MS	MS	kA
klubů	klub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nani	Nani	k6eAd1
</s>
<s>
I	i	k9
další	další	k2eAgFnSc1d1
sezona	sezona	k1gFnSc1
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
ještě	ještě	k6eAd1
úspěšnější	úspěšný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
v	v	k7c6
lednu	leden	k1gInSc6
zvítězili	zvítězit	k5eAaPmAgMnP
ve	v	k7c6
finále	finále	k1gNnSc6
Carling	Carling	k1gInSc4
Cupu	cup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
předvedli	předvést	k5eAaPmAgMnP
několik	několik	k4yIc4
skvělých	skvělý	k2eAgInPc2d1
obratů	obrat	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
s	s	k7c7
Tottenhamem	Tottenham	k1gInSc7
prohrávali	prohrávat	k5eAaImAgMnP
po	po	k7c6
poločase	poločas	k1gInSc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
zvítězili	zvítězit	k5eAaPmAgMnP
5	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
Aston	Aston	k1gMnSc1
Villou	Villa	k1gMnSc7
prohrávali	prohrávat	k5eAaImAgMnP
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
desetiminutovce	desetiminutovka	k1gFnSc6
srovnali	srovnat	k5eAaPmAgMnP
stav	stav	k1gInSc4
a	a	k8xC
pak	pak	k6eAd1
dal	dát	k5eAaPmAgMnS
rozhodující	rozhodující	k2eAgInSc4d1
gól	gól	k1gInSc4
mladíček	mladíček	k1gMnSc1
Federico	Federico	k1gMnSc1
Macheda	Macheda	k1gMnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
něhož	jenž	k3xRgNnSc4
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
první	první	k4xOgInSc4
soutěžní	soutěžní	k2eAgInSc4d1
zápas	zápas	k1gInSc4
v	v	k7c6
A-týmu	A-týmo	k1gNnSc6
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc4
v	v	k7c6
této	tento	k3xDgFnSc6
sezoně	sezona	k1gFnSc6
oslavili	oslavit	k5eAaPmAgMnP
třetí	třetí	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
řadě	řada	k1gFnSc6
ještě	ještě	k9
s	s	k7c7
krátkým	krátký	k2eAgInSc7d1
předstihem	předstih	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
přešli	přejít	k5eAaPmAgMnP
přes	přes	k7c4
Inter	Inter	k1gInSc4
Milán	Milán	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
semifinále	semifinále	k1gNnSc6
porazili	porazit	k5eAaPmAgMnP
Arsenal	Arsenal	k1gMnSc1
a	a	k8xC
ve	v	k7c6
finále	finále	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
Římě	Řím	k1gInSc6
utkali	utkat	k5eAaPmAgMnP
s	s	k7c7
Barcelonou	Barcelona	k1gFnSc7
<g/>
,	,	kIx,
vítězem	vítěz	k1gMnSc7
španělské	španělský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
i	i	k8xC
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
v	v	k7c6
této	tento	k3xDgFnSc6
sezoně	sezona	k1gFnSc6
ovšem	ovšem	k9
přinesla	přinést	k5eAaPmAgFnS
zajímavější	zajímavý	k2eAgFnSc1d2
zápasy	zápas	k1gInPc7
než	než	k8xS
finále	finále	k1gNnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
čtvrtfinále	čtvrtfinále	k1gNnSc2
mezi	mezi	k7c7
Liverpoolem	Liverpool	k1gInSc7
a	a	k8xC
Chelsea	Chelse	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
postupové	postupový	k2eAgFnSc2d1
misky	miska	k1gFnSc2
vah	váha	k1gFnPc2
neustále	neustále	k6eAd1
přesívaly	přesívat	k5eAaImAgFnP
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
a	a	k8xC
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
skončilo	skončit	k5eAaPmAgNnS
4	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
semifinále	semifinále	k1gNnSc1
Barcelony	Barcelona	k1gFnSc2
s	s	k7c7
Chelsea	Chelse	k2eAgNnPc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
první	první	k4xOgInSc4
zápas	zápas	k1gInSc4
na	na	k7c6
barcelonském	barcelonský	k2eAgMnSc6d1
Nou	Nou	k1gMnSc6
Campu	camp	k1gInSc2
skončil	skončit	k5eAaPmAgInS
bez	bez	k7c2
branek	branka	k1gFnPc2
a	a	k8xC
v	v	k7c6
odvetě	odveta	k1gFnSc6
pak	pak	k6eAd1
Chelsea	Chelsea	k1gFnSc1
vstřelila	vstřelit	k5eAaPmAgFnS
brzy	brzy	k6eAd1
gól	gól	k1gInSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
svědomitě	svědomitě	k6eAd1
bránila	bránit	k5eAaImAgFnS
a	a	k8xC
až	až	k9
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
vstřelil	vstřelit	k5eAaPmAgMnS
<g/>
,	,	kIx,
z	z	k7c2
první	první	k4xOgFnSc2
barcelonské	barcelonský	k2eAgFnSc2d1
střely	střela	k1gFnSc2
na	na	k7c4
branku	branka	k1gFnSc4
<g/>
,	,	kIx,
postupový	postupový	k2eAgInSc4d1
gól	gól	k1gInSc4
Barcelony	Barcelona	k1gFnSc2
Iniesta	Iniesta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
s	s	k7c7
Manchesterem	Manchester	k1gInSc7
vstřelil	vstřelit	k5eAaPmAgInS
brzy	brzy	k6eAd1
gól	gól	k1gInSc4
barcelonský	barcelonský	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Samuel	Samuel	k1gMnSc1
Eto	Eto	k1gMnSc1
<g/>
'	'	kIx"
<g/>
o	o	k0
<g/>
,	,	kIx,
vedení	vedení	k1gNnSc4
Barcy	Barca	k1gMnSc2
pak	pak	k6eAd1
potvrdil	potvrdit	k5eAaPmAgMnS
v	v	k7c6
druhém	druhý	k4xOgInSc6
poločase	poločas	k1gInSc6
hlavou	hlava	k1gFnSc7
Lionel	Lionel	k1gInSc1
Messi	Mess	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
závěru	závěr	k1gInSc6
roku	rok	k1gInSc2
zvítězil	zvítězit	k5eAaPmAgMnS
ve	v	k7c6
Zlatém	zlatý	k2eAgInSc6d1
míči	míč	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
nepříliš	příliš	k6eNd1
kvalitnímu	kvalitní	k2eAgInSc3d1
výkonu	výkon	k1gInSc3
United	United	k1gInSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
zápase	zápas	k1gInSc6
tak	tak	k6eAd1
Manchester	Manchester	k1gInSc1
musel	muset	k5eAaImAgInS
skousnout	skousnout	k5eAaPmF
porážku	porážka	k1gFnSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
nestal	stát	k5eNaPmAgInS
se	se	k3xPyFc4
tak	tak	k9
prvním	první	k4xOgInSc7
týmem	tým	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
by	by	kYmCp3nS
obhájil	obhájit	k5eAaPmAgInS
vítězství	vítězství	k1gNnSc4
v	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
předvedl	předvést	k5eAaPmAgMnS
i	i	k9
v	v	k7c6
této	tento	k3xDgFnSc6
sezoně	sezona	k1gFnSc6
povedené	povedený	k2eAgInPc4d1
výkony	výkon	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
sezonu	sezona	k1gFnSc4
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
nenavázal	navázat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
-	-	kIx~
2013	#num#	k4
</s>
<s>
Wayne	Waynout	k5eAaImIp3nS,k5eAaPmIp3nS
Rooney	Roonea	k1gFnPc4
a	a	k8xC
Mikael	Mikael	k1gInSc4
Silvestre	Silvestr	k1gInSc5
během	během	k7c2
semifinále	semifinále	k1gNnSc4
Ligy	liga	k1gFnPc1
Mistrů	mistr	k1gMnPc2
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
sezony	sezona	k1gFnSc2
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
vstupovali	vstupovat	k5eAaImAgMnP
United	United	k1gInSc4
s	s	k7c7
trochu	trochu	k6eAd1
menšími	malý	k2eAgFnPc7d2
očekáváními	očekávání	k1gNnPc7
kvůli	kvůli	k7c3
odchodu	odchod	k1gInSc3
hvězdy	hvězda	k1gFnSc2
Cristiana	Cristian	k1gMnSc2
Ronalda	Ronald	k1gMnSc2
do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
a	a	k8xC
útočníka	útočník	k1gMnSc4
Carlose	Carlosa	k1gFnSc6
Téveze	Téveze	k1gFnSc1
do	do	k7c2
konkurenčního	konkurenční	k2eAgInSc2d1
Manchesteru	Manchester	k1gInSc2
City	city	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
příchodem	příchod	k1gInSc7
byl	být	k5eAaImAgInS
Luis	Luisa	k1gFnPc2
Valencia	Valencius	k1gMnSc2
z	z	k7c2
Wiganu	Wigan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
Rooney	Roonea	k1gFnPc1
na	na	k7c4
sebe	sebe	k3xPyFc4
převzal	převzít	k5eAaPmAgMnS
po	po	k7c6
Ronaldovi	Ronald	k1gMnSc6
úlohu	úloha	k1gFnSc4
střelce	střelec	k1gMnPc4
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
Valencia	Valencia	k1gFnSc1
zase	zase	k9
úlohu	úloha	k1gFnSc4
nahrávače	nahrávač	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediný	jediný	k2eAgInSc4d1
a	a	k8xC
také	také	k9
závažný	závažný	k2eAgInSc1d1
problém	problém	k1gInSc1
nastal	nastat	k5eAaPmAgInS
<g/>
,	,	kIx,
když	když	k8xS
na	na	k7c4
podzim	podzim	k1gInSc4
bylo	být	k5eAaImAgNnS
v	v	k7c4
jednu	jeden	k4xCgFnSc4
chvíli	chvíle	k1gFnSc4
zraněných	zraněný	k2eAgMnPc2d1
8	#num#	k4
z	z	k7c2
9	#num#	k4
kvalitních	kvalitní	k2eAgMnPc2d1
obránců	obránce	k1gMnPc2
a	a	k8xC
na	na	k7c6
postu	post	k1gInSc6
obránců	obránce	k1gMnPc2
tak	tak	k8xC,k8xS
nastupovali	nastupovat	k5eAaImAgMnP
někdy	někdy	k6eAd1
i	i	k8xC
ofenzivní	ofenzivní	k2eAgMnPc1d1
záložníci	záložník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
pochopitelně	pochopitelně	k6eAd1
projevilo	projevit	k5eAaPmAgNnS
i	i	k9
na	na	k7c6
výsledcích	výsledek	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
tak	tak	k9
Manchester	Manchester	k1gInSc1
postoupil	postoupit	k5eAaPmAgInS
ze	z	k7c2
skupin	skupina	k1gFnPc2
LM	LM	kA
a	a	k8xC
držel	držet	k5eAaImAgInS
se	se	k3xPyFc4
v	v	k7c6
popředí	popředí	k1gNnSc6
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
<g/>
,	,	kIx,
po	po	k7c6
návratu	návrat	k1gInSc6
většiny	většina	k1gFnSc2
obránců	obránce	k1gMnPc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
tak	tak	k9
vše	všechen	k3xTgNnSc1
otevřené	otevřený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
titulu	titul	k1gInSc3
měl	mít	k5eAaImAgMnS
blízko	blízko	k7c2
Chelsea	Chelseum	k1gNnSc2
<g/>
,	,	kIx,
Arsenal	Arsenal	k1gFnSc2
i	i	k8xC
Manchester	Manchester	k1gInSc1
a	a	k8xC
na	na	k7c4
fanoušky	fanoušek	k1gMnPc4
tak	tak	k6eAd1
čekal	čekat	k5eAaImAgInS
velmi	velmi	k6eAd1
zajímavý	zajímavý	k2eAgInSc1d1
závěr	závěr	k1gInSc1
sezony	sezona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediný	jediný	k2eAgInSc4d1
výrazný	výrazný	k2eAgInSc4d1
neúspěch	neúspěch	k1gInSc4
zaznamenali	zaznamenat	k5eAaPmAgMnP
United	United	k1gInSc4
ve	v	k7c4
3	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
prohráli	prohrát	k5eAaPmAgMnP
s	s	k7c7
třetiligovým	třetiligový	k2eAgInSc7d1
Leedsem	Leeds	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dvojzápase	dvojzápas	k1gInSc6
v	v	k7c6
semifinále	semifinále	k1gNnSc6
Ligového	ligový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
s	s	k7c7
Manchesterem	Manchester	k1gInSc7
City	city	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
první	první	k4xOgInSc4
zápas	zápas	k1gInSc4
vyhráli	vyhrát	k5eAaPmAgMnP
Citizens	Citizens	k1gInSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
góly	gól	k1gInPc4
Giggs	Giggs	k1gInSc1
-	-	kIx~
2	#num#	k4
<g/>
x	x	k?
Tévez	Téveza	k1gFnPc2
<g/>
)	)	kIx)
pak	pak	k6eAd1
United	United	k1gInSc4
otočili	otočit	k5eAaPmAgMnP
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
hřišti	hřiště	k1gNnSc6
góly	gól	k1gInPc4
Scholese	Scholese	k1gFnSc1
a	a	k8xC
Carricka	Carricka	k1gFnSc1
dvojzápas	dvojzápas	k1gInSc4
ve	v	k7c4
svůj	svůj	k3xOyFgInSc4
prospěch	prospěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tévez	Tévez	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
v	v	k7c6
létě	léto	k1gNnSc6
předtím	předtím	k6eAd1
rozešel	rozejít	k5eAaPmAgInS
s	s	k7c7
United	United	k1gInSc1
ne	ne	k9
zrovna	zrovna	k6eAd1
v	v	k7c6
dobrém	dobrý	k2eAgInSc6d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
ještě	ještě	k9
zdramatizoval	zdramatizovat	k5eAaPmAgInS
zápas	zápas	k1gInSc1
gólem	gól	k1gInSc7
na	na	k7c4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
nastavení	nastavení	k1gNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
u	u	k7c2
United	United	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
hrají	hrát	k5eAaImIp3nP
nejlépe	dobře	k6eAd3
právě	právě	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
zápasu	zápas	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
vytvářejí	vytvářet	k5eAaImIp3nP
drtivý	drtivý	k2eAgInSc4d1
tlak	tlak	k1gInSc4
obvyklé	obvyklý	k2eAgNnSc1d1
<g/>
,	,	kIx,
poslal	poslat	k5eAaPmAgMnS
United	United	k1gMnSc1
do	do	k7c2
finále	finále	k1gNnSc2
s	s	k7c7
Aston	Aston	k1gMnSc1
Villou	Villa	k1gMnSc7
Wayne	Wayn	k1gInSc5
Rooney	Roone	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc4
tak	tak	k9
byli	být	k5eAaImAgMnP
na	na	k7c6
cestě	cesta	k1gFnSc6
k	k	k7c3
obhajobě	obhajoba	k1gFnSc3
trofeje	trofej	k1gFnSc2
v	v	k7c4
Carling	Carling	k1gInSc4
Cupu	cup	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
a	a	k8xC
hlavně	hlavně	k6eAd1
měli	mít	k5eAaImAgMnP
blízko	blízko	k1gNnSc4
ke	k	k7c3
čtvrtému	čtvrtý	k4xOgInSc3
titulu	titul	k1gInSc3
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
by	by	kYmCp3nS
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
jejich	jejich	k3xOp3gInSc4
devatenáctý	devatenáctý	k4xOgInSc4
titul	titul	k1gInSc4
celkem	celkem	k6eAd1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
by	by	kYmCp3nP
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgInS
na	na	k7c4
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
Anglii	Anglie	k1gFnSc6
před	před	k7c7
Liverpoolem	Liverpool	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
jich	on	k3xPp3gInPc2
držel	držet	k5eAaImAgInS
osmnáct	osmnáct	k4xCc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naneštěstí	naneštěstí	k9
Rudí	rudý	k2eAgMnPc1d1
Ďáblové	ďábel	k1gMnPc1
titul	titul	k1gInSc4
neobhájili	obhájit	k5eNaPmAgMnP
a	a	k8xC
vypadli	vypadnout	k5eAaPmAgMnP
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
Ligy	liga	k1gFnSc2
Mistrů	mistr	k1gMnPc2
s	s	k7c7
Bayernem	Bayern	k1gInSc7
Mnichov	Mnichov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venku	venek	k1gInSc2
prohráli	prohrát	k5eAaPmAgMnP
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
poslední	poslední	k2eAgInPc4d1
dva	dva	k4xCgInPc4
góly	gól	k1gInPc4
dostali	dostat	k5eAaPmAgMnP
po	po	k7c6
80	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
navic	navic	k1gMnSc1
se	se	k3xPyFc4
zranil	zranit	k5eAaPmAgMnS
kanonýr	kanonýr	k1gMnSc1
Rooney	Roonea	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
dal	dát	k5eAaPmAgInS
26	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhém	druhý	k4xOgInSc6
zapese	zapes	k1gInSc6
vyhráli	vyhrát	k5eAaPmAgMnP
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
přestože	přestože	k8xS
před	před	k7c7
druhým	druhý	k4xOgInSc7
poločasem	poločas	k1gInSc7
vedli	vést	k5eAaImAgMnP
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasluhu	Zasluh	k1gInSc2
na	na	k7c6
tom	ten	k3xDgNnSc6
mělo	mít	k5eAaImAgNnS
vyloučení	vyloučení	k1gNnSc1
Rafaela	Rafaela	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
United	United	k1gInSc4
stáhli	stáhnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trofej	trofej	k1gFnSc1
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
ovšem	ovšem	k9
taky	taky	k6eAd1
neobhájili	obhájit	k5eNaPmAgMnP
<g/>
,	,	kIx,
skončili	skončit	k5eAaPmAgMnP
o	o	k7c4
bod	bod	k1gInSc4
za	za	k7c2
Chelsea	Chelse	k1gInSc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
Chelsea	Chelsea	k1gFnSc1
překonala	překonat	k5eAaPmAgFnS
rekord	rekord	k1gInSc4
Manchesteru	Manchester	k1gInSc2
v	v	k7c6
počtu	počet	k1gInSc6
vstřelených	vstřelený	k2eAgInPc2d1
gólů	gól	k1gInPc2
ze	z	k7c2
sezony	sezona	k1gFnSc2
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
United	United	k1gInSc4
museli	muset	k5eAaImAgMnP
spokojit	spokojit	k5eAaPmF
pouze	pouze	k6eAd1
s	s	k7c7
Carling	Carling	k1gInSc1
Cupem	cup	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
velmi	velmi	k6eAd1
úspěšné	úspěšný	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
znamenala	znamenat	k5eAaImAgFnS
pro	pro	k7c4
United	United	k1gInSc4
již	již	k6eAd1
jubilejní	jubilejní	k2eAgInSc4d1
dvacátý	dvacátý	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
anglické	anglický	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
po	po	k7c6
bezmála	bezmála	k6eAd1
27	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
kuse	kus	k1gInSc6
z	z	k7c2
trenérské	trenérský	k2eAgFnSc2d1
lavičky	lavička	k1gFnSc2
Sir	sir	k1gMnSc1
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
uzavřela	uzavřít	k5eAaPmAgFnS
jedna	jeden	k4xCgFnSc1
velká	velký	k2eAgFnSc1d1
éra	éra	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
United	United	k1gMnSc1
získali	získat	k5eAaPmAgMnP
32	#num#	k4
domácích	domácí	k2eAgInPc2d1
titulů	titul	k1gInPc2
(	(	kIx(
<g/>
13	#num#	k4
<g/>
x	x	k?
Premier	Premier	k1gMnSc1
League	League	k1gNnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
x	x	k?
FA	fa	kA
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
x	x	k?
Capitol	Capitol	k1gInSc1
One	One	k1gMnSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
11	#num#	k4
<g/>
x	x	k?
Community	Communit	k1gInPc1
Shield	Shield	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
6	#num#	k4
mezinárodních	mezinárodní	k2eAgInPc2d1
titulů	titul	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
x	x	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
x	x	k?
PVP	PVP	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
x	x	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
x	x	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
x	x	k?
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
-	-	kIx~
2016	#num#	k4
</s>
<s>
Po	po	k7c6
odchodu	odchod	k1gInSc6
Sira	sir	k1gMnSc2
Alexe	Alex	k1gMnSc5
Fergusona	Ferguson	k1gMnSc4
přestoupil	přestoupit	k5eAaPmAgMnS
z	z	k7c2
týmu	tým	k1gInSc2
Everton	Everton	k1gInSc1
FC	FC	kA
do	do	k7c2
Manchesteru	Manchester	k1gInSc2
skotský	skotský	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
David	David	k1gMnSc1
Moyes	Moyes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
novým	nový	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
se	se	k3xPyFc4
Devils	Devils	k1gInSc1
moc	moc	k6eAd1
dobře	dobře	k6eAd1
nedařilo	dařit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc4
sbírali	sbírat	k5eAaImAgMnP
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Moyese	Moyese	k1gFnSc2
jeden	jeden	k4xCgInSc4
nelichotivý	lichotivý	k2eNgInSc4d1
rekord	rekord	k1gInSc4
za	za	k7c7
druhým	druhý	k4xOgInSc7
<g/>
,	,	kIx,
např.	např.	kA
<g/>
:	:	kIx,
v	v	k7c6
lize	liga	k1gFnSc6
získali	získat	k5eAaPmAgMnP
nejméně	málo	k6eAd3
bodů	bod	k1gInPc2
v	v	k7c6
klubové	klubový	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
lize	liga	k1gFnSc6
prohráli	prohrát	k5eAaPmAgMnP
třikrát	třikrát	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
doma	doma	k6eAd1
prohráli	prohrát	k5eAaPmAgMnP
v	v	k7c6
lize	liga	k1gFnSc6
celkem	celkem	k6eAd1
sedmkrát	sedmkrát	k6eAd1
a	a	k8xC
také	také	k9
si	se	k3xPyFc3
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
poprvé	poprvé	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
nezahrají	zahrát	k5eNaPmIp3nP
Ligu	liga	k1gFnSc4
Mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
klub	klub	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
třicet	třicet	k4xCc4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
výsledek	výsledek	k1gInSc1
těchto	tento	k3xDgInPc2
výsledků	výsledek	k1gInPc2
nakonec	nakonec	k6eAd1
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
vedení	vedení	k1gNnSc2
United	United	k1gInSc4
oznámilo	oznámit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Moyes	Moyes	k1gMnSc1
jako	jako	k8xS,k8xC
hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
končí	končit	k5eAaImIp3nS
a	a	k8xC
do	do	k7c2
konce	konec	k1gInSc2
sezony	sezona	k1gFnSc2
ho	on	k3xPp3gMnSc4
nahradil	nahradit	k5eAaPmAgInS
stále	stále	k6eAd1
ještě	ještě	k9
hrající	hrající	k2eAgMnSc1d1
Ryan	Ryan	k1gMnSc1
Giggs	Giggsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
byla	být	k5eAaImAgFnS
sezóna	sezóna	k1gFnSc1
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
nejhorší	zlý	k2eAgFnSc2d3
v	v	k7c6
historii	historie	k1gFnSc6
Premier	Premira	k1gFnPc2
League	Leagu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
skončil	skončit	k5eAaPmAgInS
na	na	k7c4
7	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
že	že	k8xS
si	se	k3xPyFc3
United	United	k1gMnSc1
nezahrají	zahrát	k5eNaPmIp3nP
ani	ani	k8xC
Evropskou	evropský	k2eAgFnSc4d1
ligu	liga	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
FA	fa	k1gNnSc6
Cupu	cup	k1gInSc2
vypadl	vypadnout	k5eAaPmAgMnS
hned	hned	k6eAd1
ve	v	k7c6
3	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
se	se	k3xPyFc4
Swansea	Swansea	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
ligovém	ligový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
United	United	k1gInSc4
vypadli	vypadnout	k5eAaPmAgMnP
po	po	k7c6
penaltách	penalta	k1gFnPc6
v	v	k7c6
semifinále	semifinále	k1gNnSc6
proti	proti	k7c3
Sunderlandu	Sunderland	k1gInSc3
a	a	k8xC
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
klub	klub	k1gInSc1
nestačil	stačit	k5eNaBmAgInS
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
na	na	k7c4
obhájce	obhájce	k1gMnSc4
trofeje	trofej	k1gInSc2
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sezóně	sezóna	k1gFnSc6
navíc	navíc	k6eAd1
klub	klub	k1gInSc4
opustili	opustit	k5eAaPmAgMnP
dlouholeté	dlouholetý	k2eAgFnPc4d1
stálice	stálice	k1gFnPc4
v	v	k7c6
obraně	obrana	k1gFnSc6
<g/>
,	,	kIx,
Rio	Rio	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
a	a	k8xC
Nemanja	Nemanja	k1gMnSc1
Vidić	Vidić	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
novým	nový	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
zkušený	zkušený	k2eAgMnSc1d1
nizozemský	nizozemský	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Louis	Louis	k1gMnSc1
van	vana	k1gFnPc2
Gaal	Gaal	k1gMnSc1
<g/>
,	,	kIx,
smlouvu	smlouva	k1gFnSc4
podepsal	podepsat	k5eAaPmAgMnS
na	na	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asistenty	asistent	k1gMnPc4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
stali	stát	k5eAaPmAgMnP
společně	společně	k6eAd1
s	s	k7c7
Ryanem	Ryan	k1gMnSc7
Giggsem	Giggs	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
po	po	k7c6
24	#num#	k4
letech	let	k1gInPc6
v	v	k7c4
United	United	k1gInSc4
ukončil	ukončit	k5eAaPmAgMnS
profesionální	profesionální	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
,	,	kIx,
také	také	k9
Frans	Frans	k1gMnSc1
Hoek	Hoek	k1gMnSc1
a	a	k8xC
Marcel	Marcel	k1gMnSc1
Bout	Bouta	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
si	se	k3xPyFc3
van	van	k1gInSc4
Gaal	Gaal	k1gInSc1
přivedl	přivést	k5eAaPmAgInS
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
bývalého	bývalý	k2eAgNnSc2d1
působiště	působiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
června	červen	k1gInSc2
se	se	k3xPyFc4
klub	klub	k1gInSc1
snažil	snažit	k5eAaImAgInS
o	o	k7c4
rychlou	rychlý	k2eAgFnSc4d1
proměnu	proměna	k1gFnSc4
kádru	kádr	k1gInSc2
a	a	k8xC
návrat	návrat	k1gInSc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvně	Prvně	k?
klub	klub	k1gInSc1
vykoupil	vykoupit	k5eAaPmAgInS
z	z	k7c2
Athletica	Athleticus	k1gMnSc4
Bilbaa	Bilbaus	k1gMnSc2
za	za	k7c4
36	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
záložníka	záložník	k1gMnSc2
Andera	Ander	k1gMnSc2
Herreru	Herrer	k1gInSc2
a	a	k8xC
poté	poté	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
<g/>
,	,	kIx,
po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
přetahování	přetahování	k1gNnSc6
s	s	k7c7
konkurenční	konkurenční	k2eAgNnSc4d1
Chelsea	Chelseum	k1gNnPc4
<g/>
,	,	kIx,
talentovaného	talentovaný	k2eAgMnSc2d1
anglického	anglický	k2eAgMnSc2d1
obránce	obránce	k1gMnSc2
Lukea	Lukeus	k1gMnSc4
Shawa	Shawus	k1gMnSc4
za	za	k7c4
třicet	třicet	k4xCc4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
ze	z	k7c2
Southamptonu	Southampton	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přípravné	přípravný	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
před	před	k7c7
začátkem	začátek	k1gInSc7
sezóny	sezóna	k1gFnSc2
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgInP
ve	v	k7c6
velmi	velmi	k6eAd1
dobrém	dobrý	k2eAgNnSc6d1
světle	světlo	k1gNnSc6
<g/>
,	,	kIx,
týmu	tým	k1gInSc2
se	se	k3xPyFc4
dokonce	dokonce	k9
podařilo	podařit	k5eAaPmAgNnS
vyhrát	vyhrát	k5eAaPmF
prestižní	prestižní	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
hrály	hrát	k5eAaImAgInP
hvězdné	hvězdný	k2eAgInPc1d1
týmy	tým	k1gInPc1
jako	jako	k8xC,k8xS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Inter	Inter	k1gMnSc1
Milan	Milan	k1gMnSc1
a	a	k8xC
nebo	nebo	k8xC
AS	as	k9
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
první	první	k4xOgNnPc4
dvě	dva	k4xCgNnPc4
kola	kolo	k1gNnPc4
přinesla	přinést	k5eAaPmAgFnS
zklamání	zklamání	k1gNnSc4
a	a	k8xC
celkový	celkový	k2eAgInSc4d1
zisk	zisk	k1gInSc4
1	#num#	k4
bodu	bod	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
nejhorší	zlý	k2eAgMnSc1d3
skóre	skóre	k1gNnSc4
po	po	k7c6
dvou	dva	k4xCgNnPc6
kolech	kolo	k1gNnPc6
od	od	k7c2
sezóny	sezóna	k1gFnSc2
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
byl	být	k5eAaImAgInS
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
s	s	k7c7
týmem	tým	k1gInSc7
vyhrál	vyhrát	k5eAaPmAgMnS
FA	fa	k1gNnSc4
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
vyhozen	vyhozen	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Louis	Louis	k1gMnSc1
van	vana	k1gFnPc2
Gaal	Gaal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Pozici	pozice	k1gFnSc6
hlavní	hlavní	k2eAgMnSc1d1
trenéra	trenér	k1gMnSc4
po	po	k7c6
něm	on	k3xPp3gInSc6
převzal	převzít	k5eAaPmAgMnS
portugalský	portugalský	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
José	José	k1gNnSc2
Mourinho	Mourin	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2016	#num#	k4
-	-	kIx~
dosud	dosud	k6eAd1
</s>
<s>
Za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
trenéra	trenér	k1gMnSc2
José	Josá	k1gFnSc2
Mourinha	Mourinh	k1gMnSc2
se	se	k3xPyFc4
týmu	tým	k1gInSc3
podařilo	podařit	k5eAaPmAgNnS
zvítězit	zvítězit	k5eAaPmF
v	v	k7c4
Community	Communita	k1gFnPc4
Shield	Shielda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
také	také	k9
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
týmu	tým	k1gInSc2
jako	jako	k8xC,k8xS
volný	volný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
Zlatan	Zlatan	k1gInSc4
Ibrahimović	Ibrahimović	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
do	do	k7c2
týmu	tým	k1gInSc2
přestoupili	přestoupit	k5eAaPmAgMnP
hráči	hráč	k1gMnPc1
Eric	Eric	k1gFnSc4
Bailly	Bailla	k1gFnSc2
za	za	k7c4
30	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
a	a	k8xC
Paul	Paul	k1gMnSc1
Pogba	Pogba	k1gMnSc1
za	za	k7c4
tehdy	tehdy	k6eAd1
největší	veliký	k2eAgFnSc4d3
přestupovou	přestupový	k2eAgFnSc4d1
částku	částka	k1gFnSc4
90	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závěrem	závěrem	k7c2
sezóny	sezóna	k1gFnSc2
se	se	k3xPyFc4
klubu	klub	k1gInSc2
podařilo	podařit	k5eAaPmAgNnS
porazit	porazit	k5eAaPmF
Ajax	Ajax	k1gInSc4
ve	v	k7c6
finále	finále	k1gNnSc6
Evropské	evropský	k2eAgFnSc2d1
Ligy	liga	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
si	se	k3xPyFc3
zajistit	zajistit	k5eAaPmF
přímý	přímý	k2eAgInSc4d1
postup	postup	k1gInSc4
do	do	k7c2
Ligy	liga	k1gFnSc2
Mistrů	mistr	k1gMnPc2
v	v	k7c6
příští	příští	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Premier	Premira	k1gFnPc2
League	Leagu	k1gInSc2
ovšem	ovšem	k9
tým	tým	k1gInSc1
skončil	skončit	k5eAaPmAgInS
na	na	k7c4
6	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
z	z	k7c2
důvodu	důvod	k1gInSc2
upřednostnění	upřednostnění	k1gNnSc1
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
proti	proti	k7c3
Premier	Premier	k1gInSc4
League	League	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
byl	být	k5eAaImAgMnS
ovšem	ovšem	k9
zraněn	zraněn	k2eAgMnSc1d1
nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
týmu	tým	k1gInSc2
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc1
a	a	k8xC
nebyla	být	k5eNaImAgFnS
mu	on	k3xPp3gMnSc3
prodloužena	prodloužen	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2017	#num#	k4
ve	v	k7c6
finále	finále	k1gNnSc6
Superpoháru	superpohár	k1gInSc2
UEFA	UEFA	kA
byl	být	k5eAaImAgMnS
tým	tým	k1gInSc4
poražen	poražen	k2eAgInSc4d1
Realem	Real	k1gInSc7
Madrid	Madrid	k1gInSc1
s	s	k7c7
výsledkem	výsledek	k1gInSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
sezóny	sezóna	k1gFnSc2
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
týmu	tým	k1gInSc2
Romelu	Romela	k1gFnSc4
Lukaku	Lukak	k1gInSc2
za	za	k7c4
80	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k1gNnSc4
letní	letní	k2eAgFnSc2d1
posily	posila	k1gFnSc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
například	například	k6eAd1
Victor	Victor	k1gMnSc1
Lindelöf	Lindelöf	k1gMnSc1
nebo	nebo	k8xC
Henrikh	Henrikh	k1gMnSc1
Mhkitaryan	Mhkitaryan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2017	#num#	k4
dále	daleko	k6eAd2
José	José	k1gNnSc3
Mourinho	Mourin	k1gMnSc2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Zlatanu	Zlatan	k1gInSc2
Ibrahimovićovi	Ibrahimovića	k1gMnSc3
bude	být	k5eAaImBp3nS
obnovena	obnoven	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
a	a	k8xC
po	po	k7c6
regeneraci	regenerace	k1gFnSc6
zranění	zranění	k1gNnSc1
opět	opět	k6eAd1
nastoupí	nastoupit	k5eAaPmIp3nS
do	do	k7c2
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Začátkem	začátkem	k7c2
sezóny	sezóna	k1gFnSc2
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
tým	tým	k1gInSc1
zaznamenal	zaznamenat	k5eAaPmAgInS
několik	několik	k4yIc4
důležitých	důležitý	k2eAgFnPc2d1
výher	výhra	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
stabilně	stabilně	k6eAd1
drží	držet	k5eAaImIp3nS
na	na	k7c6
druhé	druhý	k4xOgFnSc6
příčce	příčka	k1gFnSc6
tabulky	tabulka	k1gFnSc2
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
hned	hned	k6eAd1
za	za	k7c7
Manchesterem	Manchester	k1gInSc7
City	city	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2018	#num#	k4
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
výměna	výměna	k1gFnSc1
hráčů	hráč	k1gMnPc2
Alexise	Alexise	k1gFnSc1
Sáncheze	Sáncheze	k1gFnSc1
a	a	k8xC
Henrikha	Henrikha	k1gFnSc1
Mhkitaryana	Mhkitaryana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
března	březen	k1gInSc2
2018	#num#	k4
tým	tým	k1gInSc1
prohrál	prohrát	k5eAaPmAgInS
v	v	k7c6
osmifinále	osmifinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
proti	proti	k7c3
Seville	Sevilla	k1gFnSc3
s	s	k7c7
výsledkem	výsledek	k1gInSc7
prvního	první	k4xOgInSc2
zápasu	zápas	k1gInSc2
venku	venku	k6eAd1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
druhého	druhý	k4xOgInSc2
zápasu	zápas	k1gInSc2
doma	doma	k6eAd1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
prohraném	prohraný	k2eAgInSc6d1
zápase	zápas	k1gInSc6
pobouřil	pobouřit	k5eAaPmAgInS
média	médium	k1gNnPc4
a	a	k8xC
fanoušky	fanoušek	k1gMnPc4
kontroverzní	kontroverzní	k2eAgInSc1d1
projev	projev	k1gInSc1
Josého	Josý	k1gMnSc2
Mourinha	Mourinh	k1gMnSc2
na	na	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
byl	být	k5eAaImAgInS
po	po	k7c6
velmi	velmi	k6eAd1
neuspokojivých	uspokojivý	k2eNgInPc6d1
výsledcích	výsledek	k1gInPc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
vrcholem	vrchol	k1gInSc7
byla	být	k5eAaImAgFnS
ostudná	ostudný	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
s	s	k7c7
úhlavním	úhlavní	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
z	z	k7c2
Liverpoolu	Liverpool	k1gInSc2
<g/>
,	,	kIx,
odvolán	odvolán	k2eAgMnSc1d1
kontroverzní	kontroverzní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
José	José	k1gNnSc2
Mourinho	Mourin	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Vyhazov	vyhazov	k1gInSc1
byl	být	k5eAaImAgInS
dle	dle	k7c2
médii	médium	k1gNnPc7
dlouho	dlouho	k6eAd1
očekávanou	očekávaný	k2eAgFnSc7d1
událostí	událost	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
uskutečněna	uskutečnit	k5eAaPmNgFnS
po	po	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
nesplnění	nesplnění	k1gNnSc1
předsezónních	předsezónní	k2eAgInPc2d1
cílů	cíl	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
si	se	k3xPyFc3
klub	klub	k1gInSc1
vytyčil	vytyčit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Prozatímním	prozatímní	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
byla	být	k5eAaImAgFnS
následující	následující	k2eAgInSc4d1
den	den	k1gInSc4
potvrzena	potvrzen	k2eAgFnSc1d1
tehdejší	tehdejší	k2eAgFnSc1d1
dlouhodobá	dlouhodobý	k2eAgFnSc1d1
stálice	stálice	k1gFnSc1
"	"	kIx"
<g/>
fergusonova	fergusonův	k2eAgInSc2d1
kádru	kádr	k1gInSc2
<g/>
"	"	kIx"
Ole	Ola	k1gFnSc6
Gunnar	Gunnara	k1gFnPc2
Solskjæ	Solskjæ	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
do	do	k7c2
klubu	klub	k1gInSc2
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
konce	konec	k1gInSc2
rozehrané	rozehraný	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
z	z	k7c2
norského	norský	k2eAgInSc2d1
Molde	Mold	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
skončili	skončit	k5eAaPmAgMnP
Red	Red	k1gMnPc1
Devils	Devilsa	k1gFnPc2
se	s	k7c7
66	#num#	k4
body	bod	k1gInPc7
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
Premier	Premira	k1gFnPc2
League	League	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
sezóna	sezóna	k1gFnSc1
byla	být	k5eAaImAgFnS
poznamenána	poznamenat	k5eAaPmNgFnS
pandemií	pandemie	k1gFnSc7
Covid-	Covid-	k1gFnSc2
<g/>
19	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
liga	liga	k1gFnSc1
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
na	na	k7c4
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
přerušena	přerušen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezónu	sezóna	k1gFnSc4
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
poznamenal	poznamenat	k5eAaPmAgMnS
kromě	kromě	k7c2
pokračující	pokračující	k2eAgFnSc2d1
pandemie	pandemie	k1gFnSc2
i	i	k8xC
ne	ne	k9
nejlepší	dobrý	k2eAgInSc4d3
vstup	vstup	k1gInSc4
United	United	k1gMnSc1
do	do	k7c2
ní	on	k3xPp3gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetím	třetí	k4xOgInSc6
zápase	zápas	k1gInSc6
utrpěli	utrpět	k5eAaPmAgMnP
zdrcující	zdrcující	k2eAgFnSc4d1
prohru	prohra	k1gFnSc4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
na	na	k7c6
domácím	domácí	k2eAgInSc6d1
stadionu	stadion	k1gInSc6
od	od	k7c2
Tottenhamu	Tottenham	k1gInSc2
a	a	k8xC
po	po	k7c6
prvních	první	k4xOgInPc6
šesti	šest	k4xCc6
zápasech	zápas	k1gInPc6
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgInP
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
formu	forma	k1gFnSc4
zlepšil	zlepšit	k5eAaPmAgMnS
až	až	k9
v	v	k7c6
prosinci	prosinec	k1gInSc6
2020	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
ze	z	k7c2
šesti	šest	k4xCc2
zápasů	zápas	k1gInPc2
4	#num#	k4
krát	krát	k6eAd1
vyhráli	vyhrát	k5eAaPmAgMnP
a	a	k8xC
2	#num#	k4
krát	krát	k6eAd1
zremizovali	zremizovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1878	#num#	k4
–	–	k?
Newton	Newton	k1gMnSc1
Heath	Heath	k1gMnSc1
LYR	lyra	k1gFnPc2
FC	FC	kA
(	(	kIx(
<g/>
Newton	Newton	k1gMnSc1
Heath	Heath	k1gMnSc1
Lancashire	Lancashir	k1gInSc5
and	and	k?
Yorkshire	Yorkshir	k1gInSc5
Railway	Railwaa	k1gFnSc2
Football	Football	k1gInSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1902	#num#	k4
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
Football	Football	k1gInSc1
Club	club	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Úspěchy	úspěch	k1gInPc1
A	a	k8xC
<g/>
–	–	k?
<g/>
týmu	tým	k1gInSc2
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
–	–	k?
1971	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1991	#num#	k4
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1999	#num#	k4
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1968	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
2008	#num#	k4
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
(	(	kIx(
<g/>
First	First	k1gFnSc1
Division	Division	k1gInSc1
<g/>
,	,	kIx,
Premier	Premier	k1gInSc1
League	League	k1gInSc1
<g/>
;	;	kIx,
20	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
68	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
FA	fa	kA
Cup	cup	k1gInSc1
<g/>
;	;	kIx,
12	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
Football	Football	k1gInSc1
League	League	k1gNnSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
EFL	EFL	kA
Cup	cup	k1gInSc1
<g/>
;	;	kIx,
5	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
(	(	kIx(
<g/>
FA	fa	k1gNnSc1
Charity	charita	k1gFnSc2
Shield	Shielda	k1gFnPc2
<g/>
,	,	kIx,
FA	fa	k1gNnSc1
Community	Communita	k1gFnSc2
Shield	Shielda	k1gFnPc2
<g/>
;	;	kIx,
21	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1908	#num#	k4
<g/>
,	,	kIx,
1911	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
*	*	kIx~
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
*	*	kIx~
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
*	*	kIx~
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
*	*	kIx~
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
(	(	kIx(
<g/>
*	*	kIx~
hvězdičkou	hvězdička	k1gFnSc7
jsou	být	k5eAaImIp3nP
označeny	označit	k5eAaPmNgInP
dělené	dělený	k2eAgInPc1d1
tituly	titul	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Menší	malý	k2eAgInPc1d2
úspěchy	úspěch	k1gInPc1
</s>
<s>
Manchester	Manchester	k1gInSc1
Senior	senior	k1gMnSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
regionální	regionální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
pod	pod	k7c7
záštitou	záštita	k1gFnSc7
Manchester	Manchester	k1gInSc1
Football	Football	k1gMnSc1
Association	Association	k1gInSc1
<g/>
;	;	kIx,
6	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Newton	Newton	k1gMnSc1
Heath	Heath	k1gMnSc1
LYR	lyra	k1gFnPc2
FC	FC	kA
<g/>
:	:	kIx,
1885	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1887	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1888	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1889	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1901	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Lancashire	Lancashir	k1gMnSc5
Senior	senior	k1gMnSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
regionální	regionální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
pod	pod	k7c7
záštitou	záštita	k1gFnSc7
Lancashire	Lancashir	k1gInSc5
County	Counta	k1gFnSc2
Football	Football	k1gInSc1
Association	Association	k1gInSc1
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Newton	Newton	k1gMnSc1
Heath	Heath	k1gMnSc1
LYR	lyra	k1gFnPc2
FC	FC	kA
<g/>
:	:	kIx,
1897	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Zaragoza	Zaragoza	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
1982	#num#	k4
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1
Tournament	Tournament	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
2006	#num#	k4
</s>
<s>
Vodacom	Vodacom	k1gInSc1
Challenge	Challeng	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
International	Internationat	k5eAaImAgInS,k5eAaPmAgInS
Champions	Champions	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
:	:	kIx,
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
rezervy	rezerva	k1gFnSc2
a	a	k8xC
mládeže	mládež	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
rezerva	rezerva	k1gFnSc1
a	a	k8xC
akademie	akademie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Blue	Blue	k1gInSc1
Stars	Stars	k1gInSc1
<g/>
/	/	kIx~
<g/>
FIFA	FIFA	kA
Youth	Youth	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
juniorský	juniorský	k2eAgInSc1d1
celosvětový	celosvětový	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
<g/>
;	;	kIx,
18	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
Academy	Academ	k1gInPc1
<g/>
:	:	kIx,
1954	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Premier	Premier	k1gInSc1
League	League	k1gNnSc1
2	#num#	k4
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
liga	liga	k1gFnSc1
do	do	k7c2
21	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
23	#num#	k4
let	léto	k1gNnPc2
<g/>
;	;	kIx,
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
U	u	k7c2
<g/>
21	#num#	k4
<g/>
:	:	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
U	u	k7c2
<g/>
21	#num#	k4
<g/>
:	:	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
FA	fa	kA
Youth	Youth	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
anglický	anglický	k2eAgInSc1d1
pohár	pohár	k1gInSc1
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
<g/>
;	;	kIx,
10	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
U	u	k7c2
<g/>
18	#num#	k4
<g/>
:	:	kIx,
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
Reserve	Reserev	k1gFnSc2
League	Leagu	k1gFnSc2
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
liga	liga	k1gFnSc1
rezervních	rezervní	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
,	,	kIx,
národní	národní	k2eAgNnSc1d1
finále	finále	k1gNnSc1
<g/>
;	;	kIx,
4	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Reserves	Reserves	k1gMnSc1
<g/>
:	:	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Menší	malý	k2eAgInPc1d2
úspěchy	úspěch	k1gInPc1
</s>
<s>
Lancashire	Lancashir	k1gMnSc5
League	Leaguus	k1gMnSc5
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
liga	liga	k1gFnSc1
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
<g/>
;	;	kIx,
12	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
U	u	k7c2
<g/>
18	#num#	k4
<g/>
:	:	kIx,
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gMnSc2
U18	U18	k1gMnSc2
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
liga	liga	k1gFnSc1
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
národní	národní	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
U	u	k7c2
<g/>
18	#num#	k4
<g/>
:	:	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gMnSc2
U18	U18	k1gMnSc2
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
liga	liga	k1gFnSc1
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
U	u	k7c2
<g/>
18	#num#	k4
<g/>
:	:	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Champions	Champions	k6eAd1
Youth	Youth	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
U	u	k7c2
<g/>
18	#num#	k4
<g/>
:	:	kIx,
2007	#num#	k4
</s>
<s>
Milk	Milk	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
U	u	k7c2
<g/>
16	#num#	k4
<g/>
:	:	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
</s>
<s>
The	The	k?
Central	Central	k1gFnSc1
League	League	k1gFnSc1
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
liga	liga	k1gFnSc1
rezervních	rezervní	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
<g/>
;	;	kIx,
9	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Reserves	Reserves	k1gMnSc1
<g/>
:	:	kIx,
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Premier	Premier	k1gMnSc1
Reserve	Reserev	k1gFnSc2
League	League	k1gFnSc1
Northern	Northern	k1gMnSc1
Champions	Champions	k1gInSc1
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
liga	liga	k1gFnSc1
rezervních	rezervní	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
<g/>
;	;	kIx,
5	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Reserves	Reserves	k1gMnSc1
<g/>
:	:	kIx,
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Central	Centrat	k5eAaPmAgInS,k5eAaImAgInS
League	League	k1gInSc1
Cup	cup	k1gInSc4
(	(	kIx(
<g/>
anglický	anglický	k2eAgInSc4d1
pohár	pohár	k1gInSc4
rezervních	rezervní	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Reserves	Reserves	k1gMnSc1
<g/>
:	:	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
Senior	senior	k1gMnSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
regionální	regionální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
pod	pod	k7c7
záštitou	záštita	k1gFnSc7
Manchester	Manchester	k1gInSc1
Football	Football	k1gMnSc1
Association	Association	k1gInSc1
<g/>
;	;	kIx,
28	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Reserves	Reserves	k1gMnSc1
<g/>
:	:	kIx,
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
,	,	kIx,
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Lancashire	Lancashir	k1gMnSc5
Senior	senior	k1gMnSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
regionální	regionální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
pod	pod	k7c7
záštitou	záštita	k1gFnSc7
Lancashire	Lancashir	k1gInSc5
County	Counta	k1gFnSc2
Football	Football	k1gInSc1
Association	Association	k1gInSc1
<g/>
;	;	kIx,
14	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Reserves	Reserves	k1gMnSc1
<g/>
:	:	kIx,
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
(	(	kIx(
<g/>
sdílený	sdílený	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
41	#num#	k4
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Soupiska	soupiska	k1gFnSc1
</s>
<s>
K	k	k7c3
6	#num#	k4
<g/>
.	.	kIx.
srpnu	srpen	k1gInSc3
2020	#num#	k4
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
David	David	k1gMnSc1
de	de	k?
Gea	Gea	k1gMnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Victor	Victor	k1gMnSc1
Lindelöf	Lindelöf	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Eric	Eric	k1gFnSc1
Bailly	Bailla	k1gFnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Phil	Phil	k1gMnSc1
Jones	Jones	k1gMnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Harry	Harra	k1gFnPc1
Maguire	Maguir	k1gInSc5
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Paul	Paul	k1gMnSc1
Pogba	Pogba	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Edinson	Edinson	k1gInSc4
Cavani	Cavan	k1gMnPc1
</s>
<s>
8	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Juan	Juan	k1gMnSc1
Mata	mást	k5eAaImSgInS
</s>
<s>
9	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Anthony	Anthona	k1gFnPc1
Martial	Martial	k1gInSc1
</s>
<s>
10	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Marcus	Marcus	k1gMnSc1
Rashford	Rashford	k1gMnSc1
</s>
<s>
11	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Mason	mason	k1gMnSc1
Greenwood	Greenwooda	k1gFnPc2
</s>
<s>
13	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Lee	Lea	k1gFnSc3
Grant	grant	k1gInSc4
</s>
<s>
17	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Fred	Fred	k1gMnSc1
</s>
<s>
18	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Bruno	Bruno	k1gMnSc1
Fernandes	Fernandes	k1gMnSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
19	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Amad	Amad	k6eAd1
Diallo	Diallo	k1gNnSc1
</s>
<s>
21	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Daniel	Daniel	k1gMnSc1
James	James	k1gMnSc1
</s>
<s>
22	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Sergio	Sergio	k6eAd1
Romero	Romero	k1gNnSc1
</s>
<s>
23	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Luke	Luke	k1gFnSc1
Shaw	Shaw	k1gFnSc2
</s>
<s>
26	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Dean	Dean	k1gMnSc1
Henderson	Henderson	k1gMnSc1
</s>
<s>
27	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Alex	Alex	k1gMnSc1
Telles	Telles	k1gMnSc1
</s>
<s>
29	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Aaron	Aaron	k1gMnSc1
Wan-Bissaka	Wan-Bissak	k1gMnSc2
</s>
<s>
31	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Nemanja	Nemanj	k2eAgFnSc1d1
Matić	Matić	k1gFnSc1
</s>
<s>
33	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Brandon	Brandon	k1gNnSc1
Williams	Williamsa	k1gFnPc2
</s>
<s>
34	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Donny	donna	k1gFnPc1
van	vana	k1gFnPc2
de	de	k?
Beek	Beek	k1gMnSc1
</s>
<s>
38	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Axel	Axelit	k5eAaPmRp2nS
Tuanzebe	Tuanzeb	k1gMnSc5
</s>
<s>
39	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Scott	Scott	k1gMnSc1
McTominay	McTominaa	k1gFnSc2
</s>
<s>
74	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Shola	Shola	k6eAd1
Shoretire	Shoretir	k1gMnSc5
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c6
hostování	hostování	k1gNnSc6
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
14	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Jesse	Jesse	k1gFnSc1
Lingard	Lingarda	k1gFnPc2
(	(	kIx(
<g/>
ve	v	k7c4
West	West	k1gInSc4
Hamu	Hamus	k1gInSc2
United	United	k1gInSc4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
15	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Andreas	Andreas	k1gMnSc1
Pereira	Pereira	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Laziu	Lazium	k1gNnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Diogo	Diogo	k1gMnSc1
Dalot	Dalot	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Milan	Milana	k1gFnPc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
28	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Facundo	Facundo	k1gNnSc1
Pellistri	Pellistr	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
Alavésu	Alavés	k1gInSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
37	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
James	James	k1gMnSc1
Garner	Garner	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Nottinghamu	Nottingham	k1gInSc6
Forest	Forest	k1gFnSc1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
40	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Joel	Joel	k1gMnSc1
Castro	Castro	k1gNnSc1
Pereira	Pereira	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c4
Huddersfield	Huddersfield	k1gInSc4
Townu	Town	k1gInSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
43	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Teden	Teden	k1gInSc1
Mengi	Meng	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
Derby	derby	k1gNnSc6
County	Counta	k1gFnSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
44	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Tahith	Tahith	k1gMnSc1
Chong	Chong	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c4
Club	club	k1gInSc4
Brugge	Brugge	k1gFnPc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rezerva	rezerva	k1gFnSc1
a	a	k8xC
akademie	akademie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
rezerva	rezerva	k1gFnSc1
a	a	k8xC
akademie	akademie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
19	#num#	k4
<g/>
.	.	kIx.
únoru	únor	k1gInSc3
2021	#num#	k4
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
hráčů	hráč	k1gMnPc2
do	do	k7c2
23	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
hráčů	hráč	k1gMnPc2
akademie	akademie	k1gFnSc2
s	s	k7c7
čísly	číslo	k1gNnPc7
dresů	dres	k1gInPc2
v	v	k7c6
A-týmu	A-tým	k1gInSc6
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
30	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Nathan	Nathan	k1gMnSc1
Bishop	Bishop	k1gInSc4
</s>
<s>
32	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Paul	Paul	k1gMnSc1
Woolston	Woolston	k1gInSc4
</s>
<s>
46	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Hannibal	Hannibal	k1gInSc1
Mejbri	Mejbr	k1gFnSc2
</s>
<s>
47	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Arnau	Arnau	k6eAd1
Puigmal	Puigmal	k1gInSc1
</s>
<s>
48	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
William	William	k6eAd1
Fish	Fish	k1gInSc1
</s>
<s>
49	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
D	D	kA
<g/>
'	'	kIx"
<g/>
Mani	Mani	k1gNnPc1
Mellor	Mellora	k1gFnPc2
</s>
<s>
51	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Matěj	Matěj	k1gMnSc1
Kovář	Kovář	k1gMnSc1
</s>
<s>
54	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Ethan	ethan	k1gInSc1
Galbraith	Galbraitha	k1gFnPc2
</s>
<s>
55	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Reece	Reece	k1gMnSc5
Devine	Devin	k1gMnSc5
</s>
<s>
56	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Anthony	Anthona	k1gFnPc1
Elanga	Elang	k1gMnSc2
</s>
<s>
59	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Charlie	Charlie	k1gMnSc1
Wellens	Wellensa	k1gFnPc2
</s>
<s>
60	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Mastný	mastný	k2eAgMnSc1d1
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
61	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Charlie	Charlie	k1gMnSc1
McCann	McCann	k1gMnSc1
</s>
<s>
62	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Connor	Connor	k1gInSc1
Stanley	Stanlea	k1gFnSc2
</s>
<s>
64	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Björn	Björn	k1gInSc1
Hardley	Hardlea	k1gFnSc2
</s>
<s>
68	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Harvey	Harve	k2eAgFnPc1d1
Neville	Neville	k1gFnPc1
</s>
<s>
70	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Noam	Noam	k6eAd1
Emeran	Emeran	k1gInSc1
</s>
<s>
72	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Mark	Mark	k1gMnSc1
Helm	Helm	k1gMnSc1
</s>
<s>
75	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Martin	Martin	k1gMnSc1
Šviderský	Šviderský	k2eAgMnSc1d1
</s>
<s>
78	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Logan	Logan	k1gMnSc1
Pye	Pye	k1gMnSc1
</s>
<s>
79	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Isak	Isak	k6eAd1
Hansen-Aarø	Hansen-Aarø	k2eAgInSc1d1
</s>
<s>
89	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Joe	Joe	k?
Hugill	Hugill	k1gInSc1
</s>
<s>
90	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Álvaro	Álvara	k1gFnSc5
Fernández	Fernández	k1gInSc4
Carreras	Carreras	k1gMnSc1
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1889	#num#	k4
<g/>
–	–	k?
<g/>
1892	#num#	k4
<g/>
:	:	kIx,
Football	Football	k1gInSc1
Alliance	Alliance	k1gFnSc2
</s>
<s>
1892	#num#	k4
<g/>
–	–	k?
<g/>
1894	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1894	#num#	k4
<g/>
–	–	k?
<g/>
1906	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1906	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1937	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1937	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1938	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
Second	Second	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
:	:	kIx,
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Premier	Premier	k1gMnSc1
League	Leagu	k1gFnSc2
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	Z	kA
-	-	kIx~
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
V	v	k7c6
-	-	kIx~
výhry	výhra	k1gFnPc4
<g/>
,	,	kIx,
R	R	kA
-	-	kIx~
remízy	remíz	k1gInPc1
<g/>
,	,	kIx,
P	P	kA
-	-	kIx~
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
-	-	kIx~
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
-	-	kIx~
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
-	-	kIx~
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
-	-	kIx~
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Anglie	Anglie	k1gFnSc1
(	(	kIx(
<g/>
1889	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1889	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
Football	Football	k1gInSc1
Alliance	Alliance	k1gFnSc2
</s>
<s>
–	–	k?
<g/>
2292114044	#num#	k4
<g/>
-	-	kIx~
<g/>
420	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1890	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Football	Football	k1gInSc1
Alliance	Alliance	k1gFnSc2
</s>
<s>
–	–	k?
<g/>
2273123755	#num#	k4
<g/>
-	-	kIx~
<g/>
1817	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1891	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
Football	Football	k1gInSc1
Alliance	Alliance	k1gFnSc2
</s>
<s>
–	–	k?
<g/>
2212736933	#num#	k4
<g/>
+	+	kIx~
<g/>
3631	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
13066185085-3518	13066185085-3518	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1893	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
13062223672-3614	13062223672-3614	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1894	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23015877844	#num#	k4
<g/>
+	+	kIx~
<g/>
3438	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1895	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
230153126657	#num#	k4
<g/>
+	+	kIx~
<g/>
933	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1896	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23017585634	#num#	k4
<g/>
+	+	kIx~
<g/>
2239	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1897	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23016686435	#num#	k4
<g/>
+	+	kIx~
<g/>
2938	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1898	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
234195106743	#num#	k4
<g/>
+	+	kIx~
<g/>
2443	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1899	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
234204106327	#num#	k4
<g/>
+	+	kIx~
<g/>
3644	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1900	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
234144164238	#num#	k4
<g/>
+	+	kIx~
<g/>
432	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1901	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
234116173853-1528	234116173853-1528	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1902	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
234158115338	#num#	k4
<g/>
+	+	kIx~
<g/>
1538	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1903	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23420866533	#num#	k4
<g/>
+	+	kIx~
<g/>
3248	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1904	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23424558130	#num#	k4
<g/>
+	+	kIx~
<g/>
5153	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
23828649028	#num#	k4
<g/>
+	+	kIx~
<g/>
6262	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1906	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138178135356-342	138178135356-342	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
13823698148	#num#	k4
<g/>
+	+	kIx~
<g/>
3352	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138157165868-1037	138157165868-1037	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138197126961	#num#	k4
<g/>
+	+	kIx~
<g/>
845	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
13822887240	#num#	k4
<g/>
+	+	kIx~
<g/>
3252	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1381311144560-1537	1381311144560-1537	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138198116943	#num#	k4
<g/>
+	+	kIx~
<g/>
2646	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138156175262-1036	138156175262-1036	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1914	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138912174662-1630	138912174662-1630	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1915	#num#	k4
až	až	k8xS
1918	#num#	k4
se	se	k3xPyFc4
nehrála	hrát	k5eNaImAgFnS
žádná	žádný	k3yNgFnSc1
pravidelná	pravidelný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421314155450	#num#	k4
<g/>
+	+	kIx~
<g/>
440	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421510176468-440	1421510176468-440	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142812224173-3228	142812224173-3228	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421714115136	#num#	k4
<g/>
+	+	kIx~
<g/>
1548	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421314155244	#num#	k4
<g/>
+	+	kIx~
<g/>
840	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242231185723	#num#	k4
<g/>
+	+	kIx~
<g/>
3457	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142196176673-744	142196176673-744	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421314155264-1240	1421314155264-1240	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142167197280-839	142167197280-839	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421413156676-1041	1421413156676-1041	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142158196788-2138	142158196788-2138	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142782753115-6222	142782753115-6222	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242178177172-142	242178177172-142	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
2421513147168	#num#	k4
<g/>
+	+	kIx~
<g/>
343	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242146225985-2634	242146225985-2634	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242234157655	#num#	k4
<g/>
+	+	kIx~
<g/>
2150	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242221288543	#num#	k4
<g/>
+	+	kIx~
<g/>
4256	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421012205578-2332	1421012205578-2332	k4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
242229118250	#num#	k4
<g/>
+	+	kIx~
<g/>
3253	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421116155765-838	1421116155765-838	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1939	#num#	k4
až	až	k8xS
1945	#num#	k4
se	se	k3xPyFc4
nehrála	hrát	k5eNaImAgFnS
žádná	žádný	k3yNgFnSc1
pravidelná	pravidelný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142221289554	#num#	k4
<g/>
+	+	kIx~
<g/>
4156	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142191498148	#num#	k4
<g/>
+	+	kIx~
<g/>
3352	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422111107744	#num#	k4
<g/>
+	+	kIx~
<g/>
3353	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421814106944	#num#	k4
<g/>
+	+	kIx~
<g/>
2550	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142248107440	#num#	k4
<g/>
+	+	kIx~
<g/>
3456	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142231189552	#num#	k4
<g/>
+	+	kIx~
<g/>
4357	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421810146972-346	1421810146972-346	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421812127358	#num#	k4
<g/>
+	+	kIx~
<g/>
1548	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142207158474	#num#	k4
<g/>
+	+	kIx~
<g/>
1047	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142251078351	#num#	k4
<g/>
+	+	kIx~
<g/>
3260	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142288610354	#num#	k4
<g/>
+	+	kIx~
<g/>
4964	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421611158575	#num#	k4
<g/>
+	+	kIx~
<g/>
1043	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422471110366	#num#	k4
<g/>
+	+	kIx~
<g/>
3955	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421971610280	#num#	k4
<g/>
+	+	kIx~
<g/>
2245	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142189158876	#num#	k4
<g/>
+	+	kIx~
<g/>
1245	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142159187275-339	142159187275-339	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421210206781-1434	1421210206781-1434	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142237129062	#num#	k4
<g/>
+	+	kIx~
<g/>
2853	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
14226978939	#num#	k4
<g/>
+	+	kIx~
<g/>
5061	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142181598459	#num#	k4
<g/>
+	+	kIx~
<g/>
2551	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142241268445	#num#	k4
<g/>
+	+	kIx~
<g/>
3960	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142248108955	#num#	k4
<g/>
+	+	kIx~
<g/>
3456	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421512155753	#num#	k4
<g/>
+	+	kIx~
<g/>
442	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421417116661	#num#	k4
<g/>
+	+	kIx~
<g/>
545	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421611156566-143	1421611156566-143	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421910136961	#num#	k4
<g/>
+	+	kIx~
<g/>
848	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421213174460-1637	1421213174460-1637	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421012203848-1032	1421012203848-1032	k4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
Second	Second	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
24226976630	#num#	k4
<g/>
+	+	kIx~
<g/>
3661	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142231096842	#num#	k4
<g/>
+	+	kIx~
<g/>
2656	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421811137162	#num#	k4
<g/>
+	+	kIx~
<g/>
947	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421610166763	#num#	k4
<g/>
+	+	kIx~
<g/>
442	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421515126063-345	1421515126063-345	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142241086535	#num#	k4
<g/>
+	+	kIx~
<g/>
3058	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142151895136	#num#	k4
<g/>
+	+	kIx~
<g/>
1548	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142221285929	#num#	k4
<g/>
+	+	kIx~
<g/>
3078	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421913105638	#num#	k4
<g/>
+	+	kIx~
<g/>
1870	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142201487141	#num#	k4
<g/>
+	+	kIx~
<g/>
3074	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422210107747	#num#	k4
<g/>
+	+	kIx~
<g/>
3076	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1422210107036	#num#	k4
<g/>
+	+	kIx~
<g/>
3476	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1421414145245	#num#	k4
<g/>
+	+	kIx~
<g/>
756	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
140231257138	#num#	k4
<g/>
+	+	kIx~
<g/>
3381	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1381312134535	#num#	k4
<g/>
+	+	kIx~
<g/>
1051	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
138139164647-148	138139164647-148	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
1381612105845	#num#	k4
<g/>
+	+	kIx~
<g/>
1359	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
First	First	k1gInSc1
Division	Division	k1gInSc1
</s>
<s>
142211566333	#num#	k4
<g/>
+	+	kIx~
<g/>
3078	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
142241266731	#num#	k4
<g/>
+	+	kIx~
<g/>
3684	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
142271148038	#num#	k4
<g/>
+	+	kIx~
<g/>
4292	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
142261067728	#num#	k4
<g/>
+	+	kIx~
<g/>
4988	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13825767335	#num#	k4
<g/>
+	+	kIx~
<g/>
3882	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138211257644	#num#	k4
<g/>
+	+	kIx~
<g/>
3275	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13823877326	#num#	k4
<g/>
+	+	kIx~
<g/>
4777	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138221338037	#num#	k4
<g/>
+	+	kIx~
<g/>
4379	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13828739745	#num#	k4
<g/>
+	+	kIx~
<g/>
5291	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13824867931	#num#	k4
<g/>
+	+	kIx~
<g/>
4880	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13824598745	#num#	k4
<g/>
+	+	kIx~
<g/>
4277	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13825857434	#num#	k4
<g/>
+	+	kIx~
<g/>
4083	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13823696435	#num#	k4
<g/>
+	+	kIx~
<g/>
2975	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138221155826	#num#	k4
<g/>
+	+	kIx~
<g/>
3277	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13825857234	#num#	k4
<g/>
+	+	kIx~
<g/>
3883	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13828558327	#num#	k4
<g/>
+	+	kIx~
<g/>
5689	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13827658022	#num#	k4
<g/>
+	+	kIx~
<g/>
5887	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13828646824	#num#	k4
<g/>
+	+	kIx~
<g/>
4490	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13827478628	#num#	k4
<g/>
+	+	kIx~
<g/>
5885	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138231147837	#num#	k4
<g/>
+	+	kIx~
<g/>
4180	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13828558933	#num#	k4
<g/>
+	+	kIx~
<g/>
5689	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13828558643	#num#	k4
<g/>
+	+	kIx~
<g/>
4389	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138197126443	#num#	k4
<g/>
+	+	kIx~
<g/>
2164	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138201086237	#num#	k4
<g/>
+	+	kIx~
<g/>
2570	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138199104935	#num#	k4
<g/>
+	+	kIx~
<g/>
1466	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138181555429	#num#	k4
<g/>
+	+	kIx~
<g/>
2569	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
13825676828	#num#	k4
<g/>
+	+	kIx~
<g/>
4081	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138199106554	#num#	k4
<g/>
+	+	kIx~
<g/>
1166	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138181286636	#num#	k4
<g/>
+	+	kIx~
<g/>
3066	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Premier	Premier	k1gInSc1
League	Leagu	k1gFnSc2
</s>
<s>
138	#num#	k4
</s>
<s>
Poznámky	poznámka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
1891	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
:	:	kIx,
Klub	klub	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
po	po	k7c6
sezóně	sezóna	k1gFnSc6
členem	člen	k1gMnSc7
Football	Footballa	k1gFnPc2
League	League	k1gInSc1
(	(	kIx(
<g/>
rozšíření	rozšíření	k1gNnSc1
First	First	k1gFnSc1
Division	Division	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
:	:	kIx,
Manchesteru	Manchester	k1gInSc2
United	United	k1gInSc1
byl	být	k5eAaImAgInS
odebrán	odebrat	k5eAaPmNgInS
jeden	jeden	k4xCgInSc1
bod	bod	k1gInSc1
za	za	k7c4
rvačku	rvačka	k1gFnSc4
hráčů	hráč	k1gMnPc2
v	v	k7c6
zápase	zápas	k1gInSc6
s	s	k7c7
londýnským	londýnský	k2eAgInSc7d1
Arsenalem	Arsenal	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Kolo	kolo	k1gNnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Doma	doma	k6eAd1
</s>
<s>
Venku	venku	k6eAd1
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
Předkolo	předkolo	k1gNnSc1
RSC	RSC	kA
Anderlecht	Anderlecht	k1gInSc4
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gNnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
Předkolo	předkolo	k1gNnSc1
Shamrock	Shamrocka	k1gFnPc2
Rovers	Roversa	k1gFnPc2
FC	FC	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Dukla	Dukla	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FK	FK	kA
Crvena	Crven	k2eAgFnSc1d1
zvezda	zvezda	k1gFnSc1
Bělehrad	Bělehrad	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
AC	AC	kA
Milan	Milan	k1gMnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g/>
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemíPředkolo	zemíPředkola	k1gFnSc5
BSC	BSC	kA
Young	Young	k1gInSc1
Boys	boy	k1gMnPc2
</s>
<s>
Kontumačně	kontumačně	k6eAd1
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
Předkolo	předkolo	k1gNnSc1
Willem	Will	k1gMnSc7
II	II	kA
Tilburg	Tilburg	k1gInSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Sporting	Sporting	k1gInSc1
CP	CP	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Djurgå	Djurgå	k1gFnPc2
IF	IF	kA
Fotboll	Fotboll	k1gInSc4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
110	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Everton	Everton	k1gInSc4
FC	FC	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
RC	RC	kA
Strasbourg	Strasbourg	k1gInSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Ferencváros	Ferencvárosa	k1gFnPc2
TC	tc	k0
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
Předkolo	předkolo	k1gNnSc1
HJK	HJK	kA
Helsinki	Helsink	k1gFnSc2
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Vorwärts	Vorwärtsa	k1gFnPc2
Berlin	berlina	k1gFnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FK	FK	kA
Partizan	Partizan	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Hibernians	Hiberniansa	k1gFnPc2
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FK	FK	kA
Sarajevo	Sarajevo	k1gNnSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Górnik	Górnik	k1gInSc1
Zabrze	Zabrze	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1968	#num#	k4
<g/>
Interkontinentální	interkontinentální	k2eAgFnSc6d1
pohárFinále	pohárFinála	k1gFnSc6
Estudiantes	Estudiantesa	k1gFnPc2
de	de	k?
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Waterford	Waterford	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
110	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
RSC	RSC	kA
Anderlecht	Anderlechtum	k1gNnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
SK	Sk	kA
Rapid	rapid	k1gInSc1
Wien	Wien	k1gNnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
AC	AC	kA
Milan	Milan	k1gMnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AFC	AFC	kA
Ajax	Ajax	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Juventus	Juventus	k1gInSc4
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
AS	as	k9
Saint-Étienne	Saint-Étienn	k1gInSc5
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Widzew	Widzew	k1gMnPc2
Łódź	Łódź	k1gFnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Valencia	Valencium	k1gNnSc2
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Dukla	Dukla	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PFK	PFK	kA
Spartak	Spartak	k1gInSc1
Varna	Varna	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Rába	Ráb	k1gInSc2
ETO	ETO	kA
Győr	Győr	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgMnSc1d1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Dundee	Dunde	k1gInSc2
United	United	k1gInSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Videoton	Videoton	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Pécsi	Pécse	k1gFnSc6
MFC	MFC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Wrexham	Wrexham	k1gInSc4
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Montpellier	Montpellira	k1gFnPc2
HSC	HSC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Legia	Legius	k1gMnSc2
Warszawa	Warszawus	k1gMnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1991	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc4
FK	FK	kA
Crvena	Crven	k1gMnSc2
zvezda	zvezd	k1gMnSc2
Bělehrad	Bělehrad	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Athinaikos	Athinaikosa	k1gFnPc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FK	FK	kA
Torpedo	Torpedo	k1gNnSc4
Moskva	Moskva	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Budapest	Budapest	k1gInSc1
Honvéd	honvéd	k1gMnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
IFK	IFK	kA
Göteborg	Göteborg	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Rotor	rotor	k1gInSc1
Volgograd	Volgograd	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SK	Sk	kA
Rapid	rapid	k1gInSc1
Wien	Wien	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Fenerbahçe	Fenerbahçe	k1gFnSc1
SK	Sk	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Košice	Košice	k1gInPc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Feyenoord	Feyenoord	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
AS	as	k9
Monaco	Monaco	k6eAd1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
ŁKS	ŁKS	kA
Łódź	Łódź	k1gFnSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Brø	Brø	k1gInPc1
IF	IF	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Internazionale	Internazionale	k1gMnSc2
Milano	Milana	k1gFnSc5
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1999	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
SS	SS	kA
Lazio	Lazio	k1gNnSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1999	#num#	k4
<g/>
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohárFinále	pohárFinále	k1gNnSc1
Sociedade	Sociedad	k1gInSc5
Esportiva	Esportivum	k1gNnPc1
Palmeiras	Palmeirasa	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Club	club	k1gInSc1
Necaxa	Necaxa	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
CR	cr	k0
Vasco	Vasco	k1gNnSc1
da	da	k?
Gama	gama	k1gNnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
South	South	k1gMnSc1
Melbourne	Melbourne	k1gNnSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
GNK	GNK	kA
Dinamo	Dinama	k1gFnSc5
Zagreb	Zagreb	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SK	Sk	kA
Sturm	Sturm	k1gInSc1
Graz	Graz	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Olympique	Olympique	k6eAd1
de	de	k?
Marseille	Marseille	k1gFnPc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
ACF	ACF	kA
Fiorentina	Fiorentina	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Valencia	Valencia	k1gFnSc1
CF	CF	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FC	FC	kA
Girondins	Girondins	k1gInSc1
de	de	k?
Bordeaux	Bordeaux	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
</s>
<s>
RSC	RSC	kA
Anderlecht	Anderlecht	k1gInSc4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc4d1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Panathinaikos	Panathinaikos	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SK	Sk	kA
Sturm	Sturm	k1gInSc1
Graz	Graz	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Valencia	Valencia	k1gFnSc1
CF	CF	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
</s>
<s>
Lille	Lille	k1gFnSc1
OSC	OSC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Deportivo	Deportiva	k1gFnSc5
de	de	k?
La	la	k1gNnSc6
Coruñ	Coruñ	k1gInSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Olympiacos	Olympiacos	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Boavista	Boavista	k1gMnSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FC	FC	kA
Nantes	Nantes	k1gInSc4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Deportivo	Deportiva	k1gFnSc5
de	de	k?
La	la	k1gNnSc6
Coruñ	Coruñ	k1gInSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Zalaegerszegi	Zalaegerszeg	k1gFnSc2
TE	TE	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
Maccabi	Maccabi	k6eAd1
Haifa	Haifa	k1gFnSc1
FC	FC	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Olympiacos	Olympiacos	k1gInSc1
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
FC	FC	kA
Basel	Basel	k1gInSc4
18931	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Deportivo	Deportiva	k1gFnSc5
de	de	k?
La	la	k1gNnSc6
Coruñ	Coruñ	k1gInSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
Panathinaikos	Panathinaikos	k1gInSc1
FC	FC	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
VfB	VfB	k?
Stuttgart	Stuttgart	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Rangers	Rangers	k6eAd1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
FC	FC	kA
Dinamo	Dinama	k1gFnSc5
Bucureș	Bucureș	k1gNnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
Olympique	Olympique	k1gInSc1
Lyon	Lyon	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Fenerbahçe	Fenerbahçe	k1gFnSc1
SK	Sk	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
AC	AC	kA
Milan	Milan	k1gMnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Debreceni	Debrecen	k2eAgMnPc1d1
VSC	VSC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
Villarreal	Villarreal	k1gInSc1
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SL	SL	kA
Benfica	Benfica	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Lille	Lille	k1gFnSc1
OSC	OSC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SL	SL	kA
Benfica	Benfica	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FC	FC	kA
Kø	Kø	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Lille	Lille	k1gFnSc2
OSC	OSC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
AS	as	k1gNnSc2
Roma	Rom	k1gMnSc2
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
AC	AC	kA
Milan	Milan	k1gMnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
Sporting	Sporting	k1gInSc1
CP	CP	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
AS	as	k1gNnSc1
Roma	Rom	k1gMnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Olympique	Olympique	k1gNnSc2
Lyon	Lyon	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
AS	as	k1gNnSc2
Roma	Rom	k1gMnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
<g/>
Superpohár	superpohár	k1gInSc1
UEFAFinále	UEFAFinále	k1gNnSc2
FK	FK	kA
Zenit	zenit	k1gInSc1
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Gamba	gamba	k1gFnSc1
Osaka	Osaka	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc4
LDU	LDU	kA
Quito	Quito	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
Villarreal	Villarreal	k1gInSc1
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Aalborg	Aalborg	k1gInSc1
BK	BK	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FC	FC	kA
Internazionale	Internazionale	k1gMnSc2
Milano	Milana	k1gFnSc5
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Arsenal	Arsenal	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Beşiktaş	Beşiktaş	k?
JK	JK	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
VfL	VfL	k?
Wolfsburg	Wolfsburg	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
AC	AC	kA
Milan	Milan	k1gMnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Rangers	Rangers	k6eAd1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Valencia	Valencia	k1gFnSc1
CF	CF	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Bursaspor	Bursaspor	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Olympique	Olympiqu	k1gInSc2
de	de	k?
Marseille	Marseille	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Schalke	Schalk	k1gFnSc2
0	#num#	k4
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
SL	SL	kA
Benfica	Benfica	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Basel	Basel	k1gInSc4
18933	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
FC	FC	kA
Oț	Oț	k1gInSc1
Galaț	Galaț	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
AFC	AFC	kA
Ajax	Ajax	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
CFR	CFR	kA
Cluj	Cluj	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
SC	SC	kA
Braga	Braga	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
Šachtar	Šachtar	k1gInSc1
Doněck	Doněck	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Sociedad	Sociedad	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Olympiacos	Olympiacosa	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Club	club	k1gInSc4
Brugge	Brugg	k1gFnSc2
KV	KV	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc4d1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
VfL	VfL	k?
Wolfsburg	Wolfsburg	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
FC	FC	kA
Midtjylland	Midtjylland	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Feyenoord	Feyenoord	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
Zorja	Zorja	k1gFnSc1
Luhansk	Luhansk	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Fenerbahçe	Fenerbahçe	k1gFnSc1
SK	Sk	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Šestnáctifinále	Šestnáctifinále	k1gNnSc1
AS	as	k9
Saint-Étienne	Saint-Étienn	k1gInSc5
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
FK	FK	kA
Rostov	Rostov	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
RSC	RSC	kA
Anderlecht	Anderlecht	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc4
Celta	celta	k1gFnSc1
de	de	k?
Vigo	Vigo	k1gNnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
FC	FC	kA
Basel	Basel	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
SL	SL	kA
Benfica	Benfica	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
</s>
<s>
BSC	BSC	kA
Young	Young	k1gInSc1
Boys	boy	k1gMnPc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Valencia	Valencia	k1gFnSc1
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
L	L	kA
</s>
<s>
FC	FC	kA
Astana	Astana	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
</s>
<s>
AZ	AZ	kA
Alkmaar	Alkmaar	k1gMnSc1
</s>
<s>
FK	FK	kA
Partizan	Partizan	k1gMnSc1
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1
</s>
<s>
Před	před	k7c7
druhou	druhý	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
jezdilo	jezdit	k5eAaImAgNnS
na	na	k7c4
zápasy	zápas	k1gInPc4
venku	venek	k1gInSc2
kvůli	kvůli	k7c3
vysoké	vysoký	k2eAgFnSc3d1
ceně	cena	k1gFnSc3
a	a	k8xC
časovým	časový	k2eAgInPc3d1
nárokům	nárok	k1gInPc3
jen	jen	k9
málo	málo	k1gNnSc1
fanoušků	fanoušek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týmy	tým	k1gInPc7
United	United	k1gMnSc1
a	a	k8xC
City	city	k1gNnSc1
se	se	k3xPyFc4
střídali	střídat	k5eAaImAgMnP
každou	každý	k3xTgFnSc4
sobotu	sobota	k1gFnSc4
v	v	k7c6
domácích	domácí	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
mnoho	mnoho	k4c1
fanoušků	fanoušek	k1gMnPc2
chodilo	chodit	k5eAaImAgNnS
fandit	fandit	k5eAaImF
na	na	k7c4
oba	dva	k4xCgInPc4
stadiony	stadion	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
změnilo	změnit	k5eAaPmAgNnS
až	až	k9
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
týmy	tým	k1gInPc7
vyvinula	vyvinout	k5eAaPmAgFnS
silná	silný	k2eAgFnSc1d1
rivalita	rivalita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
vyhráli	vyhrát	k5eAaPmAgMnP
United	United	k1gMnSc1
ligu	liga	k1gFnSc4
<g/>
,	,	kIx,
překonaly	překonat	k5eAaPmAgFnP
i	i	k9
několik	několik	k4yIc4
let	léto	k1gNnPc2
starý	starý	k2eAgInSc1d1
rekord	rekord	k1gInSc1
Newcastlu	Newcastl	k1gInSc2
v	v	k7c6
průměrné	průměrný	k2eAgFnSc6d1
návštěvnosti	návštěvnost	k1gFnSc6
na	na	k7c4
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
přišla	přijít	k5eAaPmAgFnS
letecká	letecký	k2eAgFnSc1d1
tragédie	tragédie	k1gFnSc1
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
si	se	k3xPyFc3
klub	klub	k1gInSc1
získal	získat	k5eAaPmAgInS
fanoušky	fanoušek	k1gMnPc4
i	i	k9
mimo	mimo	k7c4
Manchesterskou	manchesterský	k2eAgFnSc4d1
aglomeraci	aglomerace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snížily	snížit	k5eAaPmAgInP
se	se	k3xPyFc4
i	i	k9
ceny	cena	k1gFnPc1
za	za	k7c4
dopravu	doprava	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
začalo	začít	k5eAaPmAgNnS
čím	čí	k3xOyRgNnSc7,k3xOyQgNnSc7
dál	daleko	k6eAd2
více	hodně	k6eAd2
fanoušků	fanoušek	k1gMnPc2
cestovat	cestovat	k5eAaImF
za	za	k7c7
svým	svůj	k3xOyFgInSc7
oblíbeným	oblíbený	k2eAgInSc7d1
týmem	tým	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
návštěvnost	návštěvnost	k1gFnSc1
na	na	k7c4
zápasy	zápas	k1gInPc4
United	United	k1gInSc4
skoro	skoro	k6eAd1
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
nejvyšší	vysoký	k2eAgInSc4d3
v	v	k7c6
celém	celý	k2eAgInSc6d1
anglickém	anglický	k2eAgInSc6d1
fotbalu	fotbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
i	i	k9
v	v	k7c6
sezóně	sezóna	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
tým	tým	k1gInSc1
spadl	spadnout	k5eAaPmAgInS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1974	#num#	k4
-	-	kIx~
1975	#num#	k4
do	do	k7c2
druhé	druhý	k4xOgFnSc2
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
bývá	bývat	k5eAaImIp3nS
stadion	stadion	k1gInSc4
na	na	k7c6
většině	většina	k1gFnSc6
zápasů	zápas	k1gInPc2
téměř	téměř	k6eAd1
nebo	nebo	k8xC
úplně	úplně	k6eAd1
vyprodán	vyprodán	k2eAgMnSc1d1
<g/>
,	,	kIx,
návštěva	návštěva	k1gFnSc1
pod	pod	k7c7
70	#num#	k4
000	#num#	k4
je	být	k5eAaImIp3nS
výjimkou	výjimka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Sponzoři	sponzor	k1gMnPc1
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
Výrobce	výrobce	k1gMnSc1
dresů	dres	k1gInPc2
</s>
<s>
Sponzor	sponzor	k1gMnSc1
na	na	k7c6
dresu	dres	k1gInSc6
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
</s>
<s>
Umbro	umbra	k1gFnSc5
</s>
<s>
žádný	žádný	k3yNgInSc1
</s>
<s>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
</s>
<s>
Admiral	Admirat	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
1980	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
</s>
<s>
Adidas	Adidas	k1gMnSc1
</s>
<s>
1982	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
Sharp	sharp	k1gInSc1
Electronics	Electronicsa	k1gFnPc2
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
</s>
<s>
Umbro	umbra	k1gFnSc5
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
</s>
<s>
Vodafone	Vodafon	k1gInSc5
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
</s>
<s>
Nike	Nike	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
</s>
<s>
AIG	AIG	kA
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
</s>
<s>
Aon	Aon	k?
</s>
<s>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
</s>
<s>
Chevrolet	chevrolet	k1gInSc1
</s>
<s>
2015	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Adidas	Adidas	k1gMnSc1
</s>
<s>
Prvním	první	k4xOgMnSc7
sponzorem	sponzor	k1gMnSc7
na	na	k7c6
dresu	dres	k1gInSc6
Manchesteru	Manchester	k1gInSc2
United	United	k1gInSc4
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
Sharp	sharp	k1gInSc4
Electronics	Electronics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
kontrakt	kontrakt	k1gInSc1
byl	být	k5eAaImAgInS
uzavřen	uzavřít	k5eAaPmNgInS
na	na	k7c4
5	#num#	k4
let	léto	k1gNnPc2
za	za	k7c4
částku	částka	k1gFnSc4
500.000	500.000	k4
<g/>
£	£	k?
(	(	kIx(
<g/>
kontrakt	kontrakt	k1gInSc1
začal	začít	k5eAaPmAgInS
na	na	k7c6
začátku	začátek	k1gInSc6
sezony	sezona	k1gFnSc2
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
touto	tento	k3xDgFnSc7
společností	společnost	k1gFnSc7
se	se	k3xPyFc4
United	United	k1gInSc1
rozloučili	rozloučit	k5eAaPmAgMnP
nakonec	nakonec	k6eAd1
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Následným	následný	k2eAgMnSc7d1
sponzorem	sponzor	k1gMnSc7
byl	být	k5eAaImAgMnS
Vodafone	Vodafon	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
podepsal	podepsat	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
4	#num#	k4
roky	rok	k1gInPc4
za	za	k7c4
částku	částka	k1gFnSc4
30.000.000	30.000.000	k4
<g/>
£	£	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvou	dva	k4xCgInPc6
letech	léto	k1gNnPc6
Vodafone	Vodafon	k1gInSc5
prodloužil	prodloužit	k5eAaPmAgInS
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
United	United	k1gInSc4
o	o	k7c4
další	další	k2eAgInPc4d1
2	#num#	k4
roky	rok	k1gInPc4
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
s	s	k7c7
předchozí	předchozí	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
6	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
za	za	k7c4
částku	částka	k1gFnSc4
36.000.000	36.000.000	k4
<g/>
£	£	k?
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
sponzorovala	sponzorovat	k5eAaImAgFnS
tým	tým	k1gInSc4
Americká	americký	k2eAgFnSc1d1
pojišťovací	pojišťovací	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
AIG	AIG	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
uzavřela	uzavřít	k5eAaPmAgFnS
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
4	#num#	k4
roky	rok	k1gInPc4
za	za	k7c4
částku	částka	k1gFnSc4
56.500.000	56.500.000	k4
<g/>
£	£	k?
(	(	kIx(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zatím	zatím	k6eAd1
posledním	poslední	k2eAgMnSc7d1
sponzorem	sponzor	k1gMnSc7
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
Aon	Aon	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
uzavřela	uzavřít	k5eAaPmAgFnS
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
4	#num#	k4
roky	rok	k1gInPc4
za	za	k7c4
cenu	cena	k1gFnSc4
cca	cca	kA
80.000.000	80.000.000	k4
<g/>
£	£	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
smlouva	smlouva	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
tedy	tedy	k9
nejvyšší	vysoký	k2eAgFnSc1d3
ve	v	k7c6
fotbalovém	fotbalový	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
tréninkové	tréninkový	k2eAgFnSc3d1
sadě	sada	k1gFnSc3
dresů	dres	k1gInPc2
se	se	k3xPyFc4
přihlásila	přihlásit	k5eAaPmAgFnS
v	v	k7c6
srpnu	srpen	k1gInSc6
2011	#num#	k4
firma	firma	k1gFnSc1
DHL	DHL	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
za	za	k7c7
4	#num#	k4
roky	rok	k1gInPc7
zaplatí	zaplatit	k5eAaPmIp3nP
cca	cca	kA
40.000.000	40.000.000	k4
<g/>
£	£	k?
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgMnSc7
výrobcem	výrobce	k1gMnSc7
dresů	dres	k1gInPc2
Manchesteru	Manchester	k1gInSc2
United	United	k1gInSc4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Umbro	umbra	k1gFnSc5
<g/>
,	,	kIx,
následovali	následovat	k5eAaImAgMnP
firmy	firma	k1gFnPc4
<g/>
:	:	kIx,
Admiral	Admiral	k1gMnSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Adidas	Adidas	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
opět	opět	k6eAd1
Umbro	umbra	k1gFnSc5
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Nike	Nike	k1gFnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
firmou	firma	k1gFnSc7
Nike	Nike	k1gFnSc2
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
za	za	k7c4
rekordní	rekordní	k2eAgFnSc4d1
částku	částka	k1gFnSc4
302.900.000	302.900.000	k4
<g/>
£	£	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
sponzorují	sponzorovat	k5eAaImIp3nP
také	také	k9
společnosti	společnost	k1gFnPc1
jako	jako	k8xS,k8xC
Chevrolet	chevrolet	k1gInSc1
<g/>
,	,	kIx,
Audi	Audi	k1gNnSc1
<g/>
,	,	kIx,
Epson	Epson	kA
či	či	k8xC
Budweiser	Budweiser	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Významné	významný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
klubu	klub	k1gInSc2
</s>
<s>
Legendární	legendární	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Před	před	k7c4
Busbym	Busbym	k1gInSc4
</s>
<s>
Billy	Bill	k1gMnPc4
Meredith	Meredith	k1gInSc1
</s>
<s>
Joe	Joe	k?
Spence	Spenec	k1gInPc1
</s>
<s>
Sandy	Sandy	k6eAd1
Turnbull	Turnbull	k1gInSc1
</s>
<s>
Padesátá	padesátý	k4xOgNnPc4
a	a	k8xC
šedesátá	šedesátý	k4xOgNnPc4
léta	léto	k1gNnPc4
</s>
<s>
John	John	k1gMnSc1
Aston	Aston	k1gMnSc1
</s>
<s>
George	Georg	k1gMnSc2
Best	Best	k1gMnSc1
</s>
<s>
Shay	Shaa	k1gFnPc1
Brennan	Brennana	k1gFnPc2
</s>
<s>
Roger	Rograt	k5eAaPmRp2nS
Byrne	Byrn	k1gMnSc5
</s>
<s>
Johnny	Johnen	k2eAgFnPc1d1
Carey	Carea	k1gFnPc1
</s>
<s>
Bobby	Bobba	k1gFnPc1
Charlton	Charlton	k1gInSc1
</s>
<s>
Pat	pat	k1gInSc1
Crerand	Creranda	k1gFnPc2
</s>
<s>
Tony	Tony	k1gMnSc5
Dunne	Dunn	k1gMnSc5
</s>
<s>
Duncan	Duncan	k1gInSc1
Edwards	Edwardsa	k1gFnPc2
</s>
<s>
Bill	Bill	k1gMnSc1
Foulkes	Foulkes	k1gMnSc1
</s>
<s>
Denis	Denisa	k1gFnPc2
Law	Law	k1gFnPc2
</s>
<s>
Charlie	Charlie	k1gMnSc1
Mitten	Mitten	k2eAgMnSc1d1
</s>
<s>
Stan	stan	k1gInSc1
Pearson	Pearsona	k1gFnPc2
</s>
<s>
Jack	Jack	k1gMnSc1
Rowley	Rowlea	k1gFnSc2
</s>
<s>
Nobby	Nobba	k1gFnPc1
Stiles	Stilesa	k1gFnPc2
</s>
<s>
Tommy	Tomma	k1gFnPc1
Taylor	Taylora	k1gFnPc2
</s>
<s>
Dennis	Dennis	k1gInSc1
Viollet	Viollet	k1gInSc1
</s>
<s>
David	David	k1gMnSc1
Herd	herda	k1gFnPc2
</s>
<s>
Sedmdesátá	sedmdesátý	k4xOgNnPc4
a	a	k8xC
osmdesátá	osmdesátý	k4xOgNnPc4
léta	léto	k1gNnPc4
</s>
<s>
Arthur	Arthur	k1gMnSc1
Albiston	Albiston	k1gInSc4
</s>
<s>
Clayton	Clayton	k1gInSc1
Blackmore	Blackmor	k1gMnSc5
</s>
<s>
Martin	Martin	k1gMnSc1
Buchan	Buchan	k1gMnSc1
</s>
<s>
Steve	Steve	k1gMnSc1
Coppell	Coppell	k1gMnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Duxbury	Duxbura	k1gFnSc2
</s>
<s>
Sammy	Samma	k1gFnPc1
McIlroy	McIlroa	k1gFnSc2
</s>
<s>
Gordon	Gordon	k1gMnSc1
McQueen	McQuena	k1gFnPc2
</s>
<s>
Lou	Lou	k?
Macari	Macari	k1gNnSc1
</s>
<s>
Alex	Alex	k1gMnSc1
Stepney	Stepnea	k1gFnSc2
</s>
<s>
Gordon	Gordon	k1gMnSc1
Strachan	Strachan	k1gMnSc1
</s>
<s>
Norman	Norman	k1gMnSc1
Whiteside	Whitesid	k1gMnSc5
</s>
<s>
Ray	Ray	k?
Wilkins	Wilkins	k1gInSc1
</s>
<s>
Gary	Gar	k2eAgFnPc1d1
Bailey	Bailea	k1gFnPc1
</s>
<s>
Bryan	Bryan	k1gMnSc1
Robson	Robson	k1gMnSc1
</s>
<s>
Devadesátá	devadesátý	k4xOgNnPc4
léta	léto	k1gNnPc4
</s>
<s>
David	David	k1gMnSc1
Beckham	Beckham	k1gInSc4
</s>
<s>
Steve	Steve	k1gMnSc1
Bruce	Bruce	k1gMnSc1
</s>
<s>
Nicky	nicka	k1gFnPc1
Butt	Butta	k1gFnPc2
</s>
<s>
Eric	Eric	k1gFnSc1
Cantona	Cantona	k1gFnSc1
</s>
<s>
Andrew	Andrew	k?
Cole	cola	k1gFnSc3
</s>
<s>
Ryan	Ryan	k1gMnSc1
Giggs	Giggsa	k1gFnPc2
</s>
<s>
Mark	Mark	k1gMnSc1
Hughes	Hughes	k1gMnSc1
</s>
<s>
Paul	Paul	k1gMnSc1
Ince	Inka	k1gFnSc3
</s>
<s>
Denis	Denisa	k1gFnPc2
Irwin	Irwin	k1gMnSc1
</s>
<s>
Andrei	Andrea	k1gFnSc3
Kanchelskis	Kanchelskis	k1gFnSc1
</s>
<s>
Roy	Roy	k1gMnSc1
Keane	Kean	k1gMnSc5
</s>
<s>
Brian	Brian	k1gMnSc1
McClair	McClair	k1gMnSc1
</s>
<s>
Gary	Gar	k2eAgFnPc1d1
Neville	Neville	k1gFnPc1
</s>
<s>
Gary	Gara	k1gFnPc1
Pallister	Pallistra	k1gFnPc2
</s>
<s>
Peter	Peter	k1gMnSc1
Schmeichel	Schmeichel	k1gMnSc1
</s>
<s>
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
</s>
<s>
Ole	Ola	k1gFnSc3
Gunnar	Gunnar	k1gMnSc1
Solskjæ	Solskjæ	k1gMnSc1
</s>
<s>
Jaap	Jaap	k1gMnSc1
Stam	Stam	k1gMnSc1
</s>
<s>
Dwight	Dwight	k2eAgInSc1d1
Yorke	Yorke	k1gInSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Laurent	Laurent	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Rio	Rio	k?
Ferdinand	Ferdinand	k1gMnSc1
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
</s>
<s>
Wayne	Waynout	k5eAaImIp3nS,k5eAaPmIp3nS
Rooney	Roonea	k1gFnPc4
</s>
<s>
Ruud	Ruud	k6eAd1
van	van	k1gInSc1
Nistelrooy	Nistelrooa	k1gFnSc2
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Sar	Sar	k1gFnPc3
</s>
<s>
Pak	pak	k6eAd1
Či-Song	Či-Song	k1gInSc1
</s>
<s>
Trenéři	trenér	k1gMnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
1878	#num#	k4
<g/>
–	–	k?
<g/>
1892	#num#	k4
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
1892	#num#	k4
<g/>
–	–	k?
<g/>
1900	#num#	k4
</s>
<s>
A.	A.	kA
H.	H.	kA
Albut	Albut	k1gMnSc1
</s>
<s>
1900	#num#	k4
<g/>
–	–	k?
<g/>
1903	#num#	k4
</s>
<s>
James	James	k1gMnSc1
West	West	k1gMnSc1
</s>
<s>
1903	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
</s>
<s>
Ernest	Ernest	k1gMnSc1
Mangnall	Mangnall	k1gMnSc1
</s>
<s>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1914	#num#	k4
</s>
<s>
John	John	k1gMnSc1
Bentley	Bentlea	k1gFnSc2
</s>
<s>
1914	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
</s>
<s>
Jack	Jack	k1gMnSc1
Robson	Robson	k1gMnSc1
</s>
<s>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
</s>
<s>
John	John	k1gMnSc1
Chapman	Chapman	k1gMnSc1
</s>
<s>
První	první	k4xOgMnSc1
trenér	trenér	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
nepochází	pocházet	k5eNaImIp3nS
z	z	k7c2
Anglie	Anglie	k1gFnSc2
</s>
<s>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
</s>
<s>
Lal	Lal	k?
Hilditch	Hilditch	k1gInSc1
</s>
<s>
Nejkratší	krátký	k2eAgFnSc4d3
dobu	doba	k1gFnSc4
působící	působící	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
</s>
<s>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
</s>
<s>
Herbert	Herbert	k1gMnSc1
Bamlett	Bamlett	k1gMnSc1
</s>
<s>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
</s>
<s>
Walter	Walter	k1gMnSc1
Crickmer	Crickmer	k1gMnSc1
</s>
<s>
1932	#num#	k4
<g/>
–	–	k?
<g/>
1937	#num#	k4
</s>
<s>
Scott	Scott	k1gMnSc1
Duncan	Duncan	k1gMnSc1
</s>
<s>
1937	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
</s>
<s>
Walter	Walter	k1gMnSc1
Crickmer	Crickmer	k1gMnSc1
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
</s>
<s>
Matt	Matt	k1gMnSc1
Busby	Busba	k1gFnSc2
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
</s>
<s>
Wilf	Wilf	k1gInSc1
McGuinness	McGuinnessa	k1gFnPc2
</s>
<s>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
</s>
<s>
Matt	Matt	k1gMnSc1
Busby	Busba	k1gFnSc2
</s>
<s>
1971	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
</s>
<s>
Frank	Frank	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Farrell	Farrell	k1gMnSc1
</s>
<s>
První	první	k4xOgMnSc1
trenér	trenér	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
nepochází	pocházet	k5eNaImIp3nS
ze	z	k7c2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
</s>
<s>
Tommy	Tomma	k1gFnPc1
Docherty	Dochert	k1gInPc1
</s>
<s>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
</s>
<s>
Dave	Dav	k1gInSc5
Sexton	Sexton	k1gInSc4
</s>
<s>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
</s>
<s>
Ron	Ron	k1gMnSc1
Atkinson	Atkinson	k1gMnSc1
</s>
<s>
1986	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
</s>
<s>
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
</s>
<s>
Nejdéle	dlouho	k6eAd3
působící	působící	k2eAgFnPc1d1
a	a	k8xC
nejvíce	hodně	k6eAd3,k6eAd1
úspěšný	úspěšný	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
</s>
<s>
David	David	k1gMnSc1
Moyes	Moyes	k1gMnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Ryan	Ryan	k1gMnSc1
Giggs	Giggsa	k1gFnPc2
</s>
<s>
Prozatímní	prozatímní	k2eAgMnSc1d1
hrající	hrající	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
</s>
<s>
Louis	Louis	k1gMnSc1
van	vana	k1gFnPc2
Gaal	Gaal	k1gMnSc1
</s>
<s>
První	první	k4xOgMnSc1
trenér	trenér	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
nepochází	pocházet	k5eNaImIp3nS
ze	z	k7c2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
nebo	nebo	k8xC
Irska	Irsko	k1gNnSc2
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
</s>
<s>
José	José	k1gNnSc1
Mourinho	Mourin	k1gMnSc2
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
</s>
<s>
Ole	Ola	k1gFnSc3
Gunnar	Gunnar	k1gMnSc1
Solskjæ	Solskjæ	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
rezerva	rezerva	k1gFnSc1
a	a	k8xC
akademie	akademie	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
rezerva	rezerva	k1gFnSc1
a	a	k8xC
akademie	akademie	k1gFnSc1
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
WFC	WFC	kA
–	–	k?
ženský	ženský	k2eAgInSc4d1
tým	tým	k1gInSc4
</s>
<s>
Manchester	Manchester	k1gInSc1
Eagles	Eagles	k1gInSc1
–	–	k?
zaniklý	zaniklý	k2eAgInSc1d1
basketbalový	basketbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
;	;	kIx,
v	v	k7c6
letech	let	k1gInPc6
1985	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
a	a	k8xC
vlastnictví	vlastnictví	k1gNnSc1
Manchesteru	Manchester	k1gInSc2
United	United	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
R.	R.	kA
Jelínek	Jelínek	k1gMnSc1
a	a	k8xC
J.	J.	kA
Tomeš	Tomeš	k1gMnSc1
<g/>
:	:	kIx,
První	první	k4xOgInSc1
fotbalový	fotbalový	k2eAgInSc1d1
atlas	atlas	k1gInSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
INFOKART	INFOKART	kA
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
F.	F.	kA
<g/>
C.	C.	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Angus	Angus	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Keith	Keith	k1gInSc1
(	(	kIx(
<g/>
1879	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Sportsman	sportsman	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Year-Book	Year-Book	k1gInSc1
for	forum	k1gNnPc2
1880	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cassell	Cassell	k1gMnSc1
<g/>
,	,	kIx,
Petter	Petter	k1gMnSc1
<g/>
,	,	kIx,
Galpin	Galpin	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
182.1	182.1	k4
2	#num#	k4
Barnes	Barnesa	k1gFnPc2
<g/>
,	,	kIx,
Justyn	Justyn	k1gMnSc1
<g/>
;	;	kIx,
Bostock	Bostock	k1gMnSc1
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
;	;	kIx,
Butler	Butler	k1gMnSc1
<g/>
,	,	kIx,
Cliff	Cliff	k1gMnSc1
<g/>
;	;	kIx,
Ferguson	Ferguson	k1gMnSc1
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
<g/>
;	;	kIx,
Meek	Meek	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
Mitten	Mitten	k2eAgInSc1d1
<g/>
,	,	kIx,
Andy	Anda	k1gFnPc4
<g/>
;	;	kIx,
Pilger	Pilger	k1gMnSc1
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
<g/>
;	;	kIx,
Taylor	Taylor	k1gMnSc1
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
OBE	Ob	k1gInSc5
<g/>
;	;	kIx,
Tyrrell	Tyrrell	k1gMnSc1
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
1998	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Official	Official	k1gMnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
Illustrated	Illustrated	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
rd	rd	k?
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
Books	Books	k1gInSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
233	#num#	k4
<g/>
-	-	kIx~
<g/>
99964	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Why	Why	k1gMnSc1
We	We	k1gMnSc1
Shouldn	Shouldn	k1gMnSc1
<g/>
’	’	k?
<g/>
t	t	k?
Use	usus	k1gInSc5
‘	‘	k?
<g/>
Man	Man	k1gMnSc1
U	u	k7c2
<g/>
’	’	k?
to	ten	k3xDgNnSc1
Refer	Refer	k1gInSc1
to	ten	k3xDgNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
2	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
Wayback	Wayback	k1gInSc1
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Pangeran	Pangeran	k1gInSc1
Siahaan	Siahaan	k1gInSc1
<g/>
↑	↑	k?
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
je	být	k5eAaImIp3nS
nepřemožitelný	přemožitelný	k2eNgInSc1d1
<g/>
,	,	kIx,
vyhrál	vyhrát	k5eAaPmAgInS
i	i	k9
Německý	německý	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
opět	opět	k6eAd1
nevyhrál	vyhrát	k5eNaPmAgMnS
<g/>
,	,	kIx,
Tottenham	Tottenham	k1gInSc4
si	se	k3xPyFc3
zastřílel	zastřílet	k5eAaPmAgMnS
a	a	k8xC
je	být	k5eAaImIp3nS
první	první	k4xOgNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOUIS	louis	k1gInSc1
VAN	van	k1gInSc1
GAAL	GAAL	kA
LEAVES	LEAVES	kA
MANCHESTER	Manchester	k1gInSc4
UNITED	UNITED	kA
<g/>
,	,	kIx,
manutd	manutd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Jose	Jos	k1gMnSc2
Mourinho	Mourin	k1gMnSc2
appointed	appointed	k1gMnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
boss	boss	k1gMnSc1
<g/>
,	,	kIx,
skysports	skysports	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Dlouho	dlouho	k6eAd1
očekávaný	očekávaný	k2eAgInSc4d1
vyhazov	vyhazov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mourinho	Mourin	k1gMnSc2
končí	končit	k5eAaImIp3nS
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
United	United	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
18.12	18.12	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Mourinha	Mourinha	k1gMnSc1
střídá	střídat	k5eAaImIp3nS
oblíbenec	oblíbenec	k1gMnSc1
Solskjaer	Solskjaer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
konce	konec	k1gInSc2
sezony	sezona	k1gFnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
19.12	19.12	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
NEWTON	newton	k1gInSc1
HEATH	HEATH	kA
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fchd	fchd	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
MANCHESTER	Manchester	k1gInSc1
UNITED	UNITED	kA
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fchd	fchd	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
"	"	kIx"
<g/>
European	European	k1gInSc1
Cups	Cupsa	k1gFnPc2
Archive	archiv	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Intercontinental	Intercontinental	k1gMnSc1
Club	club	k1gInSc4
Cup	cup	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
FIFA	FIFA	kA
Club	club	k1gInSc1
World	World	k1gInSc1
Cup	cup	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
England	England	k1gInSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Champions	Champions	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vzdor	vzdor	k7c3
bombám	bomba	k1gFnPc3
<g/>
,	,	kIx,
vzdor	vzdor	k7c3
tragédii	tragédie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
slaví	slavit	k5eAaImIp3nS
podvacáté	podvacáté	k4xO
titul	titul	k1gInSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-04-22	2013-04-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
England	England	k1gInSc1
FA	fa	kA
Challenge	Challenge	k1gInSc1
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
The	The	k1gMnSc3
FA	fa	kA
Cup	cup	k1gInSc1
-	-	kIx~
Final	Final	k1gInSc1
2016	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
skysports	skysports	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
England	England	k1gInSc1
-	-	kIx~
Football	Football	k1gInSc1
League	League	k1gInSc1
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
England	England	k1gInSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
FA	fa	kA
Charity	charita	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Community	Communita	k1gFnSc2
Shield	Shield	k1gMnSc1
Matches	Matches	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Zaragoza	Zaragoza	k1gFnSc1
"	"	kIx"
Memorial	Memorial	k1gInSc1
Carlos	Carlos	k1gMnSc1
Lapetra	Lapetra	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Zaragoza-Spain	Zaragoza-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Amsterdam	Amsterdam	k1gInSc1
Tournament	Tournament	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Vodacom	Vodacom	k1gInSc1
Challenge	Challeng	k1gInSc2
<g/>
:	:	kIx,
A	a	k9
Ten-Year	Ten-Year	k1gInSc1
History	Histor	k1gInPc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
goal	goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
16.07	16.07	k4
<g/>
.2009	.2009	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
beat	beat	k1gInSc1
Liverpool	Liverpool	k1gInSc1
after	after	k1gInSc1
second-half	second-half	k1gInSc1
revival	revival	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
theguardian	theguardian	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
5.08	5.08	k4
<g/>
.2014	.2014	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Man	Man	k1gMnSc1
Utd	Utd	k1gFnSc2
First	First	k1gInSc1
Team	team	k1gInSc1
Squad	Squad	k1gInSc1
&	&	k?
Player	Player	k1gInSc1
Profiles	Profiles	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Man	Man	k1gMnSc1
Utd	Utd	k1gFnSc4
announce	announko	k6eAd1
squad	squad	k1gInSc1
numbers	numbersa	k1gFnPc2
for	forum	k1gNnPc2
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
Premier	Premiero	k1gNnPc2
League	League	k1gFnPc2
season	seasona	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
</s>
<s>
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
9	#num#	k4
August	August	k1gMnSc1
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CARNEY	CARNEY	kA
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maguire	Maguir	k1gInSc5
to	ten	k3xDgNnSc4
be	be	k?
new	new	k?
United	United	k1gMnSc1
captain	captain	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ManUtd	ManUtd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
17	#num#	k4
January	Januara	k1gFnSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
17	#num#	k4
January	Januara	k1gFnSc2
2020	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MARSHALL	MARSHALL	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jesse	Jesse	k1gFnSc1
Lingard	Lingarda	k1gFnPc2
completes	completes	k1gMnSc1
loan	loan	k1gMnSc1
move	movat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ManUtd	ManUtd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
29	#num#	k4
January	Januara	k1gFnSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
January	Januara	k1gFnSc2
2021	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MARSHALL	MARSHALL	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andreas	Andreas	k1gInSc1
set	sto	k4xCgNnPc2
for	forum	k1gNnPc2
new	new	k?
loan	loan	k1gMnSc1
spell	spell	k1gMnSc1
in	in	k?
Italy	Ital	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ManUtd	ManUtd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
1	#num#	k4
October	October	k1gInSc1
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
October	October	k1gInSc1
2020	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BOSTOCK	BOSTOCK	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalot	Dalot	k1gInSc1
Joins	Joins	k1gInSc1
Italian	Italian	k1gInSc1
Giants	Giants	k1gInSc4
on	on	k3xPp3gMnSc1
Loan	Loan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ManUtd	ManUtd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
4	#num#	k4
October	October	k1gInSc1
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
4	#num#	k4
October	October	k1gInSc1
2020	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GANLEY	GANLEY	kA
<g/>
,	,	kIx,
Joe	Joe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Facundo	Facundo	k6eAd1
Pellistri	Pellistr	k1gMnPc1
joins	joins	k6eAd1
Alaves	Alaves	k1gInSc4
on	on	k3xPp3gMnSc1
loan	loan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ManUtd	ManUtd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
31	#num#	k4
January	Januara	k1gFnSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
January	Januara	k1gFnSc2
2021	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MARSHALL	MARSHALL	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Garner	Garner	k1gMnSc1
makes	makes	k1gMnSc1
Championship	Championship	k1gMnSc1
switch	switch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ManUtd	ManUtd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
30	#num#	k4
January	Januara	k1gFnSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
January	Januara	k1gFnSc2
2021	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MARSHALL	MARSHALL	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Confirmed	Confirmed	k1gMnSc1
<g/>
:	:	kIx,
Pereira	Pereira	k1gMnSc1
Completes	Completes	k1gMnSc1
Loan	Loan	k1gMnSc1
Transfer	transfer	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ManUtd	ManUtd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
29	#num#	k4
August	August	k1gMnSc1
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
August	August	k1gMnSc1
2020	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
United	United	k1gInSc1
defender	defendra	k1gFnPc2
Mengi	Meng	k1gFnSc2
links	links	k1gInSc1
up	up	k?
with	with	k1gInSc1
club	club	k1gInSc1
legend	legenda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ManUtd	ManUtd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
1	#num#	k4
February	Februara	k1gFnSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
February	Februara	k1gFnSc2
2021	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MARSHALL	MARSHALL	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chong	Chong	k1gInSc1
seals	seals	k6eAd1
second	second	k1gInSc1
loan	loana	k1gFnPc2
move	mov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ManUtd	ManUtd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
30	#num#	k4
January	Januara	k1gFnSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
January	Januara	k1gFnSc2
2021	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Man	Man	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Union	union	k1gInSc1
of	of	k?
European	European	k1gInSc1
Football	Football	k1gMnSc1
Associations	Associations	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Manchester	Manchester	k1gInSc1
Untied	Untied	k1gInSc1
CZ	CZ	kA
<g/>
/	/	kIx~
<g/>
SK	Sk	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
stadion	stadion	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
-	-	kIx~
Czech	Czech	k1gInSc1
fans	fans	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
-	-	kIx~
Slovak	Slovak	k1gInSc1
fans	fans	k1gInSc1
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
stadionu	stadion	k1gInSc6
-	-	kIx~
FotbaloveStadiony	FotbaloveStadion	k1gInPc7
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Video	video	k1gNnSc4
Letecká	letecký	k2eAgFnSc1d1
havárie	havárie	k1gFnSc1
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1958	#num#	k4
na	na	k7c4
Stream	Stream	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Premier	Premier	k1gInSc1
League	Leagu	k1gInSc2
–	–	k?
anglická	anglický	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ocenění	ocenění	k1gNnSc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Arsenal	Arsenat	k5eAaImAgMnS,k5eAaPmAgMnS
FC	FC	kA
•	•	k?
Aston	Aston	k1gMnSc1
Villa	Villa	k1gMnSc1
FC	FC	kA
•	•	k?
Brighton	Brighton	k1gInSc1
&	&	k?
Hove	Hove	k1gInSc1
Albion	Albion	k1gInSc1
FC	FC	kA
•	•	k?
Burnley	Burnlea	k1gFnPc4
FC	FC	kA
•	•	k?
Chelsea	Chelsea	k1gMnSc1
FC	FC	kA
•	•	k?
Crystal	Crystal	k1gMnSc7
Palace	Palace	k1gFnSc2
FC	FC	kA
•	•	k?
Everton	Everton	k1gInSc1
FC	FC	kA
•	•	k?
Leicester	Leicester	k1gMnSc1
City	City	k1gFnSc2
FC	FC	kA
•	•	k?
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
•	•	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Newcastle	Newcastle	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Sheffield	Sheffield	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Southampton	Southampton	k1gInSc1
FC	FC	kA
•	•	k?
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
•	•	k?
West	West	k1gInSc1
Ham	ham	k0
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
Wolverhampton	Wolverhampton	k1gInSc1
Wanderers	Wanderers	k1gInSc1
FC	FC	kA
•	•	k?
Fulham	Fulham	k1gInSc1
FC	FC	kA
•	•	k?
Leeds	Leeds	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
•	•	k?
West	West	k2eAgInSc4d1
Bromwich	Bromwich	k1gInSc4
Albion	Albion	k1gInSc1
FC	FC	kA
Sezóny	sezóna	k1gFnSc2
</s>
<s>
Football	Football	k1gInSc1
League	Leagu	k1gFnSc2
<g/>
:	:	kIx,
1888	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1889	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1890	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1891	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
FL	FL	kA
First	First	k1gInSc1
Division	Division	k1gInSc1
<g/>
:	:	kIx,
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1893	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1894	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1895	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1896	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1897	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1898	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1899	#num#	k4
<g/>
/	/	kIx~
<g/>
1900	#num#	k4
•	•	k?
1900	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
1901	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
1902	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
•	•	k?
1903	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
1904	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
1906	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
1913	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
1914	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
1915	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
1916	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
1917	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
1918	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
•	•	k?
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
•	•	k?
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
•	•	k?
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
•	•	k?
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
•	•	k?
1925	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
26	#num#	k4
•	•	k?
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
•	•	k?
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
•	•	k?
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
•	•	k?
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
•	•	k?
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
1950	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
51	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
•	•	k?
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
•	•	k?
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
Premier	Premiero	k1gNnPc2
League	League	k1gFnPc2
<g/>
:	:	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Accrington	Accrington	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Darwen	Darwen	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1893	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Glossop	Glossop	k1gInSc1
North	North	k1gMnSc1
End	End	k1gMnSc1
AFC	AFC	kA
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bradford	Bradford	k1gInSc1
Park	park	k1gInSc1
Avenue	avenue	k1gFnSc1
AFC	AFC	kA
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Bury	Bury	k?
FC	FC	kA
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Brentford	Brentford	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Grimsby	Grimsba	k1gFnSc2
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Preston	Preston	k1gInSc1
North	North	k1gMnSc1
End	End	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
61	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leyton	Leyton	k1gInSc1
Orient	Orient	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Northampton	Northampton	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carlisle	Carlisle	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bristol	Bristol	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
80	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxford	Oxford	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Millwall	Millwall	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Luton	Luton	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Notts	Notts	k1gInSc1
County	Counta	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
1991	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oldham	Oldham	k1gInSc1
Athletic	Athletice	k1gFnPc2
AFC	AFC	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Swindon	Swindon	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barnsley	Barnslea	k1gFnPc4
FC	FC	kA
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sheffield	Sheffield	k1gInSc1
Wednesday	Wednesdaa	k1gMnSc2
FC	FC	kA
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wimbledon	Wimbledon	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bradford	Bradford	k1gMnSc1
City	City	k1gFnSc2
AFC	AFC	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Coventry	Coventr	k1gMnPc4
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ipswich	Ipswich	k1gInSc1
Town	Town	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leeds	Leeds	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Charlton	Charlton	k1gInSc1
Athletic	Athletice	k1gFnPc2
FC	FC	kA
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Derby	derby	k1gNnSc1
County	Counta	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portsmouth	Portsmouth	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Birmingham	Birmingham	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Blackpool	Blackpool	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Blackburn	Blackburn	k1gInSc1
Rovers	Rovers	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bolton	Bolton	k1gInSc1
Wanderers	Wanderers	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wigan	Wigan	k1gInSc1
Athletic	Athletice	k1gFnPc2
FC	FC	kA
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Reading	Reading	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Queens	Queens	k1gInSc1
Park	park	k1gInSc1
Rangers	Rangers	k1gInSc4
FC	FC	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hull	Hull	k1gMnSc1
City	City	k1gFnSc2
AFC	AFC	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Middlesbrough	Middlesbrough	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sunderland	Sunderland	k1gInSc1
AFC	AFC	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stoke	Stoke	k1gNnSc2
City	City	k1gFnPc2
FC	FC	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Swansea	Swanseum	k1gNnSc2
City	City	k1gFnPc2
AFC	AFC	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
West	West	k2eAgInSc4d1
Bromwich	Bromwich	k1gInSc4
Albion	Albion	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cardiff	Cardiff	k1gMnSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fulham	Fulham	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Huddersfield	Huddersfield	k1gInSc1
Town	Town	k1gMnSc1
AFC	AFC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AFC	AFC	kA
Bournemouth	Bournemouth	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norwich	Norwich	k1gMnSc1
City	City	k1gFnSc2
FC	FC	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Watford	Watford	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
Trofeje	trofej	k1gInSc2
a	a	k8xC
ocenění	ocenění	k1gNnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
mistrů	mistr	k1gMnPc2
•	•	k?
Golden	Goldno	k1gNnPc2
Boot	Boot	k1gInSc1
•	•	k?
Golden	Goldna	k1gFnPc2
Glove	Gloev	k1gFnSc2
•	•	k?
Manager	manager	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Player	Player	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Goal	Goal	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Playmaker	Playmaker	k1gMnSc1
of	of	k?
the	the	k?
Season	Season	k1gMnSc1
•	•	k?
Manager	manager	k1gMnSc1
of	of	k?
the	the	k?
Month	Month	k1gMnSc1
•	•	k?
Player	Player	k1gMnSc1
of	of	k?
the	the	k?
Month	Month	k1gMnSc1
•	•	k?
Goal	Goal	k1gMnSc1
of	of	k?
the	the	k?
Month	Month	k1gMnSc1
•	•	k?
Hráč	hráč	k1gMnSc1
roku	rok	k1gInSc2
dle	dle	k7c2
FWA	FWA	kA
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Anglický	anglický	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Anglický	anglický	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
•	•	k?
Football	Football	k1gInSc1
League	League	k1gFnSc1
First	First	k1gMnSc1
Division	Division	k1gInSc1
•	•	k?
Premier	Premier	k1gInSc1
League	Leagu	k1gMnSc2
Asia	Asius	k1gMnSc2
Trophy	Tropha	k1gFnSc2
•	•	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
•	•	k?
Professional	Professional	k1gMnSc1
Development	Development	k1gMnSc1
League	League	k1gFnPc2
U18	U18	k1gMnSc1
&	&	k?
U23	U23	k1gMnSc1
•	•	k?
Premier	Premier	k1gMnSc1
League	Leagu	k1gFnSc2
International	International	k1gMnSc1
Cup	cup	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Football	Footballa	k1gFnPc2
Association	Association	k1gInSc1
•	•	k?
Anglická	anglický	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
Fotbal	fotbal	k1gInSc1
ženy	žena	k1gFnSc2
:	:	kIx,
FA	fa	kA
Women	Women	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Super	super	k2eAgNnSc7d1
League	League	k1gNnSc7
•	•	k?
FA	fa	kA
Women	Women	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cup	cup	k1gInSc1
•	•	k?
FA	fa	k1gNnSc1
Women	Womno	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
League	League	k1gNnSc7
Cup	cup	k1gInSc1
•	•	k?
FA	fa	k1gNnSc1
Women	Womno	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Community	Communit	k1gInPc7
Shield	Shielda	k1gFnPc2
•	•	k?
Anglická	anglický	k2eAgFnSc1d1
ženská	ženský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1972	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1973	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1975	#num#	k4
FK	FK	kA
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
•	•	k?
1976	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1977	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
1978	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1979	#num#	k4
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gInSc4
FC	FC	kA
•	•	k?
1980	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
1982	#num#	k4
Aston	Aston	k1gNnSc4
Villa	Villo	k1gNnSc2
FC	FC	kA
•	•	k?
1983	#num#	k4
Aberdeen	Aberdeen	k1gInSc1
FC	FC	kA
•	•	k?
1984	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
KV	KV	kA
Mechelen	Mechelna	k1gFnPc2
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
1992	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1993	#num#	k4
AC	AC	kA
Parma	Parma	k1gFnSc1
•	•	k?
1994	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1995	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1998	#num#	k4
Chelsea	Chelseus	k1gMnSc2
FC	FC	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1999	#num#	k4
Lazio	Lazio	k6eAd1
Řím	Řím	k1gInSc1
•	•	k?
2000	#num#	k4
Galatasaray	Galatasaraa	k1gMnSc2
SK	Sk	kA
•	•	k?
2001	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2004	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
2005	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2006	#num#	k4
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Zenit	zenit	k1gInSc1
Petrohrad	Petrohrad	k1gInSc4
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2013	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2021	#num#	k4
•	•	k?
2022	#num#	k4
•	•	k?
2023	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
-	-	kIx~
předchůdce	předchůdce	k1gMnSc2
</s>
<s>
1960	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1961	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1962	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1963	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1964	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1965	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1966	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1967	#num#	k4
Racing	Racing	k1gInSc1
Club	club	k1gInSc4
•	•	k?
1968	#num#	k4
Estudiantes	Estudiantes	k1gInSc1
de	de	k?
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
•	•	k?
1969	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1970	#num#	k4
Feyenoord	Feyenoord	k1gInSc1
•	•	k?
1971	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1972	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1973	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
•	•	k?
1974	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
1975	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1976	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
1977	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
1978	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1979	#num#	k4
Club	club	k1gInSc1
Olimpia	Olimpium	k1gNnSc2
•	•	k?
1980	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1981	#num#	k4
Flamengo	flamengo	k1gNnSc4
•	•	k?
1982	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1983	#num#	k4
Grê	Grê	k1gNnSc4
•	•	k?
1984	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1985	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
CA	ca	kA
River	Rivra	k1gFnPc2
Plate	plat	k1gInSc5
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
FK	FK	kA
Crvena	Crven	k2eAgFnSc1d1
zvezda	zvezda	k1gFnSc1
•	•	k?
1992	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1993	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1994	#num#	k4
CA	ca	kA
Vélez	Vélez	k1gInSc1
Sarsfield	Sarsfield	k1gInSc1
•	•	k?
1995	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc1
•	•	k?
1998	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1999	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2000	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2001	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2004	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
Mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
</s>
<s>
2000	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2001-2004	2001-2004	k4
•	•	k?
2005	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
2006	#num#	k4
Sport	sport	k1gInSc1
Club	club	k1gInSc4
Internacional	Internacional	k1gFnSc2
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2013	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
pna	pnout	k5eAaImSgInS
<g/>
2005285575	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
6509184-X	6509184-X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2300	#num#	k4
2509	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82237215	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
167809407	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82237215	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
