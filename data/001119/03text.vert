<s>
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1956	[number]	k4	1956
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
československá	československý	k2eAgFnSc1d1	Československá
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
americká	americký	k2eAgFnSc1d1	americká
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
světová	světový	k2eAgFnSc1d1	světová
tenisová	tenisový	k2eAgFnSc1d1	tenisová
jednička	jednička	k1gFnSc1	jednička
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
332	[number]	k4	332
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
nejdelší	dlouhý	k2eAgNnSc4d3	nejdelší
období	období	k1gNnSc4	období
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
(	(	kIx(	(
<g/>
po	po	k7c6	po
Steffi	Steffi	k1gFnSc6	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
tenistek	tenistka	k1gFnPc2	tenistka
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
59	[number]	k4	59
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
18	[number]	k4	18
dvouher	dvouhra	k1gFnPc2	dvouhra
<g/>
,	,	kIx,	,
31	[number]	k4	31
ženských	ženský	k2eAgFnPc2d1	ženská
a	a	k8xC	a
10	[number]	k4	10
smíšených	smíšený	k2eAgFnPc2d1	smíšená
čtyřher	čtyřhra	k1gFnPc2	čtyřhra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
tenisty	tenista	k1gMnPc7	tenista
drží	držet	k5eAaImIp3nP	držet
absolutní	absolutní	k2eAgInSc1d1	absolutní
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vyhraných	vyhraný	k2eAgInPc2d1	vyhraný
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
167	[number]	k4	167
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
177	[number]	k4	177
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
tenistek	tenistka	k1gFnPc2	tenistka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
kompletní	kompletní	k2eAgFnSc4d1	kompletní
sadu	sada	k1gFnSc4	sada
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
titulů	titul	k1gInPc2	titul
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
soutěží	soutěž	k1gFnPc2	soutěž
všech	všecek	k3xTgInPc2	všecek
čtyř	čtyři	k4xCgMnPc2	čtyři
Grand	grand	k1gMnSc1	grand
Slamů	slam	k1gInPc2	slam
<g/>
.	.	kIx.	.
</s>
<s>
Zbývajícími	zbývající	k2eAgFnPc7d1	zbývající
hráčkami	hráčka	k1gFnPc7	hráčka
jsou	být	k5eAaImIp3nP	být
Margaret	Margareta	k1gFnPc2	Margareta
Courtová	Courtový	k2eAgFnSc1d1	Courtový
a	a	k8xC	a
Doris	Doris	k1gFnSc1	Doris
Hartová	Hartová	k1gFnSc1	Hartová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
je	být	k5eAaImIp3nS	být
však	však	k9	však
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
toho	ten	k3xDgMnSc4	ten
kdy	kdy	k6eAd1	kdy
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Billie	Billie	k1gFnSc1	Billie
Jean	Jean	k1gMnSc1	Jean
Kingová	Kingový	k2eAgNnPc4d1	Kingové
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
tenistkou	tenistka	k1gFnSc7	tenistka
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
,	,	kIx,	,
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
i	i	k8xC	i
smíšené	smíšený	k2eAgFnSc2d1	smíšená
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kdy	kdy	k6eAd1	kdy
žila	žít	k5eAaImAgFnS	žít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tenisový	tenisový	k2eAgMnSc1d1	tenisový
novinář	novinář	k1gMnSc1	novinář
Steve	Steve	k1gMnSc1	Steve
Flink	flink	k1gMnSc1	flink
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
The	The	k1gFnSc4	The
Greatest	Greatest	k1gMnSc1	Greatest
Tennis	Tennis	k1gFnPc2	Tennis
Matches	Matches	k1gMnSc1	Matches
of	of	k?	of
the	the	k?	the
Twentieth	Twentieth	k1gInSc4	Twentieth
Century	Centura	k1gFnPc4	Centura
(	(	kIx(	(
<g/>
Největší	veliký	k2eAgInPc4d3	veliký
tenisové	tenisový	k2eAgInPc4d1	tenisový
zápasy	zápas	k1gInPc4	zápas
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hráčku	hráčka	k1gFnSc4	hráčka
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
za	za	k7c7	za
Němkou	Němka	k1gFnSc7	Němka
Steffi	Steffi	k1gFnSc7	Steffi
Grafovou	Grafová	k1gFnSc7	Grafová
<g/>
.	.	kIx.	.
</s>
<s>
Tennis	Tennis	k1gFnSc1	Tennis
Magazine	Magazin	k1gInSc5	Magazin
ji	on	k3xPp3gFnSc4	on
vybral	vybrat	k5eAaPmAgMnS	vybrat
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
tenistku	tenistka	k1gFnSc4	tenistka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
období	období	k1gNnSc6	období
1965	[number]	k4	1965
až	až	k9	až
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
tenisu	tenis	k1gInSc2	tenis
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
Bud	bouda	k1gFnPc2	bouda
Collins	Collinsa	k1gFnPc2	Collinsa
ji	on	k3xPp3gFnSc4	on
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
největší	veliký	k2eAgFnSc7d3	veliký
hráčkou	hráčka	k1gFnSc7	hráčka
nebo	nebo	k8xC	nebo
hráčem	hráč	k1gMnSc7	hráč
tenisu	tenis	k1gInSc2	tenis
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
z	z	k7c2	z
Řevnic	řevnice	k1gFnPc2	řevnice
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
za	za	k7c4	za
místní	místní	k2eAgInSc4d1	místní
tenisový	tenisový	k2eAgInSc4d1	tenisový
oddíl	oddíl	k1gInSc4	oddíl
LTC	LTC	kA	LTC
Řevnice	řevnice	k1gFnSc2	řevnice
(	(	kIx(	(
<g/>
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
klub	klub	k1gInSc4	klub
hrála	hrát	k5eAaImAgFnS	hrát
už	už	k6eAd1	už
i	i	k9	i
její	její	k3xOp3gFnSc1	její
babička	babička	k1gFnSc1	babička
Anežka	Anežka	k1gFnSc1	Anežka
Semanská	Semanský	k2eAgFnSc1d1	Semanská
-	-	kIx~	-
Boučková	Boučková	k1gFnSc1	Boučková
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
čestnou	čestný	k2eAgFnSc7d1	čestná
členkou	členka	k1gFnSc7	členka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
jezdila	jezdit	k5eAaImAgFnS	jezdit
trénovat	trénovat	k5eAaImF	trénovat
do	do	k7c2	do
blízké	blízký	k2eAgFnSc2d1	blízká
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
lépe	dobře	k6eAd2	dobře
rozvinout	rozvinout	k5eAaPmF	rozvinout
její	její	k3xOp3gInSc4	její
talent	talent	k1gInSc4	talent
a	a	k8xC	a
um	um	k1gInSc4	um
<g/>
.	.	kIx.	.
</s>
<s>
Nedokončila	dokončit	k5eNaPmAgFnS	dokončit
studia	studio	k1gNnPc4	studio
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Gymnáziu	gymnázium	k1gNnSc6	gymnázium
Na	na	k7c6	na
Zatlance	Zatlanka	k1gFnSc6	Zatlanka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
americkou	americký	k2eAgFnSc7d1	americká
občankou	občanka	k1gFnSc7	občanka
<g/>
,	,	kIx,	,
české	český	k2eAgNnSc4d1	české
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
obdržela	obdržet	k5eAaPmAgFnS	obdržet
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
dvouhře	dvouhra	k1gFnSc6	dvouhra
získala	získat	k5eAaPmAgFnS	získat
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
rekordních	rekordní	k2eAgInPc2d1	rekordní
9	[number]	k4	9
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
přiznáním	přiznání	k1gNnSc7	přiznání
své	svůj	k3xOyFgFnSc2	svůj
lesbické	lesbický	k2eAgFnSc2d1	lesbická
orientace	orientace	k1gFnSc2	orientace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
učinila	učinit	k5eAaPmAgFnS	učinit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
něco	něco	k3yInSc1	něco
takového	takový	k3xDgMnSc4	takový
nebylo	být	k5eNaImAgNnS	být
zdaleka	zdaleka	k6eAd1	zdaleka
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
participuje	participovat	k5eAaImIp3nS	participovat
a	a	k8xC	a
iniciuje	iniciovat	k5eAaBmIp3nS	iniciovat
mnoho	mnoho	k4c1	mnoho
charitativních	charitativní	k2eAgInPc2d1	charitativní
projektů	projekt	k1gInPc2	projekt
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
od	od	k7c2	od
povodní	povodeň	k1gFnPc2	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
obnově	obnova	k1gFnSc6	obnova
parku	park	k1gInSc2	park
Stromovka	Stromovka	k1gFnSc1	Stromovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
tenisové	tenisový	k2eAgFnSc2d1	tenisová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k8xC	i
soutěže	soutěž	k1gFnPc1	soutěž
družstev	družstvo	k1gNnPc2	družstvo
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Fed	Fed	k1gFnSc1	Fed
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
Československo	Československo	k1gNnSc4	Československo
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
za	za	k7c4	za
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Martina	Martina	k1gFnSc1	Martina
Šubertová	Šubertová	k1gFnSc1	Šubertová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
Martině	Martina	k1gFnSc6	Martina
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Jana	Jana	k1gFnSc1	Jana
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
vzala	vzít	k5eAaPmAgFnS	vzít
Miroslava	Miroslav	k1gMnSc4	Miroslav
Navrátila	Navrátil	k1gMnSc4	Navrátil
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gInPc3	její
prvním	první	k4xOgMnSc6	první
tenisovým	tenisový	k2eAgMnSc7d1	tenisový
trenérem	trenér	k1gMnSc7	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
nevlastním	vlastní	k2eNgMnSc6d1	nevlastní
otci	otec	k1gMnSc6	otec
dostala	dostat	k5eAaPmAgFnS	dostat
příjmení	příjmení	k1gNnSc2	příjmení
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
šestnácti	šestnáct	k4xCc2	šestnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
profesionálkou	profesionálka	k1gFnSc7	profesionálka
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
profesionální	profesionální	k2eAgInSc4d1	profesionální
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Orlandu	Orland	k1gInSc6	Orland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
hrála	hrát	k5eAaImAgFnS	hrát
dvě	dva	k4xCgNnPc4	dva
grandslamová	grandslamový	k2eAgNnPc4d1	grandslamové
finále	finále	k1gNnPc4	finále
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
jak	jak	k6eAd1	jak
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
Evonne	Evonn	k1gInSc5	Evonn
Goolagongové	Goolagongový	k2eAgFnPc4d1	Goolagongový
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gNnSc4	Open
Chris	Chris	k1gFnSc2	Chris
Evertové	Evertová	k1gFnSc2	Evertová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
poté	poté	k6eAd1	poté
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
a	a	k8xC	a
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Zelenou	zelený	k2eAgFnSc4d1	zelená
kartu	karta	k1gFnSc4	karta
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
grandslamový	grandslamový	k2eAgInSc1d1	grandslamový
turnaj	turnaj	k1gInSc1	turnaj
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
žen	žena	k1gFnPc2	žena
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
třísetové	třísetový	k2eAgFnSc6d1	třísetová
bitvě	bitva	k1gFnSc6	bitva
porazila	porazit	k5eAaPmAgFnS	porazit
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
Chris	Chris	k1gFnSc2	Chris
Evertovou	Evertová	k1gFnSc4	Evertová
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
tenisového	tenisový	k2eAgInSc2d1	tenisový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Wimbledonský	wimbledonský	k2eAgInSc4d1	wimbledonský
titul	titul	k1gInSc4	titul
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
získala	získat	k5eAaPmAgFnS	získat
třetí	třetí	k4xOgInSc4	třetí
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
porazila	porazit	k5eAaPmAgFnS	porazit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Evertovou	Evertový	k2eAgFnSc4d1	Evertový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
třísetové	třísetový	k2eAgFnSc6d1	třísetová
finálovém	finálový	k2eAgInSc6d1	finálový
zápase	zápas	k1gInSc6	zápas
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
až	až	k6eAd1	až
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
Tracy	Traca	k1gFnSc2	Traca
Austinové	Austinový	k2eAgFnSc2d1	Austinová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
jak	jak	k6eAd1	jak
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
celografitovou	celografitový	k2eAgFnSc4d1	celografitová
tenisovou	tenisový	k2eAgFnSc4d1	tenisová
raketu	raketa	k1gFnSc4	raketa
Yonex	Yonex	k1gInSc4	Yonex
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
dominantní	dominantní	k2eAgFnSc7d1	dominantní
postavou	postava	k1gFnSc7	postava
ženského	ženský	k2eAgInSc2d1	ženský
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Prohrála	prohrát	k5eAaPmAgFnS	prohrát
sice	sice	k8xC	sice
již	již	k6eAd1	již
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
prvního	první	k4xOgInSc2	první
grandslamového	grandslamový	k2eAgInSc2d1	grandslamový
turnaje	turnaj	k1gInSc2	turnaj
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
(	(	kIx(	(
<g/>
Australian	Australian	k1gInSc1	Australian
Open	Opena	k1gFnPc2	Opena
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
až	až	k9	až
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zbylé	zbylý	k2eAgInPc4d1	zbylý
turnaje	turnaj	k1gInPc4	turnaj
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sezóny	sezóna	k1gFnSc2	sezóna
prohrála	prohrát	k5eAaPmAgFnS	prohrát
jen	jen	k9	jen
zápas	zápas	k1gInSc4	zápas
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
a	a	k8xC	a
s	s	k7c7	s
86	[number]	k4	86
vítězstvími	vítězství	k1gNnPc7	vítězství
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
prohrou	prohra	k1gFnSc7	prohra
drží	držet	k5eAaImIp3nS	držet
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
procentuální	procentuální	k2eAgFnSc6d1	procentuální
úspěšnosti	úspěšnost	k1gFnSc6	úspěšnost
profesionálního	profesionální	k2eAgMnSc2d1	profesionální
tenisty	tenista	k1gMnSc2	tenista
během	během	k7c2	během
jediné	jediný	k2eAgFnSc2d1	jediná
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tří	tři	k4xCgFnPc2	tři
sezón	sezóna	k1gFnPc2	sezóna
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
a	a	k8xC	a
1984	[number]	k4	1984
prohrála	prohrát	k5eAaPmAgFnS	prohrát
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
zápasů	zápas	k1gInPc2	zápas
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
1984	[number]	k4	1984
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
opět	opět	k6eAd1	opět
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
,	,	kIx,	,
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c4	v
French	French	k1gInSc4	French
Open	Open	k1gNnSc4	Open
<g/>
,	,	kIx,	,
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
dokonce	dokonce	k9	dokonce
pošesté	pošesté	k4xO	pošesté
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
jí	on	k3xPp3gFnSc7	on
zbývalo	zbývat	k5eAaImAgNnS	zbývat
zvítězit	zvítězit	k5eAaPmF	zvítězit
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
všechny	všechen	k3xTgInPc4	všechen
grandslamové	grandslamový	k2eAgInPc4d1	grandslamový
turnaje	turnaj	k1gInPc4	turnaj
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
turnaje	turnaj	k1gInSc2	turnaj
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Heleně	Helena	k1gFnSc6	Helena
Sukové	Sukové	k2eAgNnSc1d1	Sukové
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
74	[number]	k4	74
zápasů	zápas	k1gInPc2	zápas
bez	bez	k7c2	bez
porážky	porážka	k1gFnSc2	porážka
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
profesionálním	profesionální	k2eAgInSc7d1	profesionální
rekordem	rekord	k1gInSc7	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
získala	získat	k5eAaPmAgFnS	získat
čistý	čistý	k2eAgInSc4d1	čistý
grandslam	grandslam	k1gInSc4	grandslam
za	za	k7c4	za
roky	rok	k1gInPc4	rok
1984	[number]	k4	1984
a	a	k8xC	a
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nehrálo	hrát	k5eNaImAgNnS	hrát
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gNnSc1	Open
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
změně	změna	k1gFnSc3	změna
termínu	termín	k1gInSc2	termín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ročníky	ročník	k1gInPc4	ročník
1985	[number]	k4	1985
a	a	k8xC	a
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1983-1985	[number]	k4	1983-1985
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
8	[number]	k4	8
deblových	deblový	k2eAgInPc2d1	deblový
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
turnajů	turnaj	k1gInPc2	turnaj
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
nepřekonaný	překonaný	k2eNgInSc1d1	nepřekonaný
rekord	rekord	k1gInSc1	rekord
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
deblovou	deblový	k2eAgFnSc7d1	deblová
spoluhráčkou	spoluhráčka	k1gFnSc7	spoluhráčka
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverová	k1gFnSc7	Shriverová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverová	k1gFnSc7	Shriverová
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
ženskou	ženská	k1gFnSc4	ženská
deblovou	deblový	k2eAgFnSc4d1	deblová
dvojici	dvojice	k1gFnSc4	dvojice
historie	historie	k1gFnSc2	historie
moderního	moderní	k2eAgInSc2d1	moderní
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
20	[number]	k4	20
deblových	deblový	k2eAgInPc2d1	deblový
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
turnajů	turnaj	k1gInPc2	turnaj
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
x	x	k?	x
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
x	x	k?	x
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
x	x	k?	x
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
a	a	k8xC	a
4	[number]	k4	4
<g/>
x	x	k?	x
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10	[number]	k4	10
x	x	k?	x
Turnaj	turnaj	k1gInSc1	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
79	[number]	k4	79
deblových	deblový	k2eAgInPc2d1	deblový
turnajů	turnaj	k1gInPc2	turnaj
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
také	také	k9	také
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
Ženské	ženský	k2eAgFnSc2d1	ženská
tenisové	tenisový	k2eAgFnSc2d1	tenisová
asociace	asociace	k1gFnSc2	asociace
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
obdržela	obdržet	k5eAaPmAgFnS	obdržet
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
neinvazivní	invazivní	k2eNgFnSc1d1	neinvazivní
forma	forma	k1gFnSc1	forma
karcinomu	karcinom	k1gInSc2	karcinom
prsu	prs	k1gInSc2	prs
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
lumpektomický	lumpektomický	k2eAgInSc4d1	lumpektomický
zákrok	zákrok	k1gInSc4	zákrok
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
tenistkou	tenistka	k1gFnSc7	tenistka
a	a	k8xC	a
wimbledonskou	wimbledonský	k2eAgFnSc7d1	wimbledonská
vítězkou	vítězka	k1gFnSc7	vítězka
Janou	Jana	k1gFnSc7	Jana
Novotnou	Novotná	k1gFnSc7	Novotná
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
do	do	k7c2	do
čtyřher	čtyřhra	k1gFnPc2	čtyřhra
na	na	k7c6	na
veteránských	veteránský	k2eAgInPc6d1	veteránský
turnajích	turnaj	k1gInPc6	turnaj
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
spisovatelské	spisovatelský	k2eAgFnSc3d1	spisovatelská
činnosti	činnost	k1gFnSc3	činnost
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
jako	jako	k9	jako
sportovní	sportovní	k2eAgFnSc1d1	sportovní
komentátorka	komentátorka	k1gFnSc1	komentátorka
tenisových	tenisový	k2eAgFnPc2d1	tenisová
soutěží	soutěž	k1gFnPc2	soutěž
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
CNN	CNN	kA	CNN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
natáčela	natáčet	k5eAaImAgFnS	natáčet
pro	pro	k7c4	pro
CNN	CNN	kA	CNN
v	v	k7c6	v
londýnském	londýnský	k2eAgInSc6d1	londýnský
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
televizní	televizní	k2eAgInSc1d1	televizní
pořad	pořad	k1gInSc1	pořad
společně	společně	k6eAd1	společně
Petrou	Petra	k1gFnSc7	Petra
Kvitovou	Kvitová	k1gFnSc7	Kvitová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
velký	velký	k2eAgInSc4d1	velký
dětský	dětský	k2eAgInSc4d1	dětský
vzor	vzor	k1gInSc4	vzor
(	(	kIx(	(
<g/>
poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
obě	dva	k4xCgFnPc1	dva
dvě	dva	k4xCgFnPc1	dva
hrají	hrát	k5eAaImIp3nP	hrát
tenis	tenis	k1gInSc4	tenis
levou	levý	k2eAgFnSc7d1	levá
rukou	ruka	k1gFnSc7	ruka
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
wimbledonský	wimbledonský	k2eAgInSc4d1	wimbledonský
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
partnerkou	partnerka	k1gFnSc7	partnerka
Julií	Julie	k1gFnSc7	Julie
Lemigovovou	Lemigovová	k1gFnSc7	Lemigovová
<g/>
,	,	kIx,	,
o	o	k7c4	o
15	[number]	k4	15
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc4d2	mladší
poslední	poslední	k2eAgFnSc4d1	poslední
Miss	miss	k1gFnSc4	miss
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
vychovávají	vychovávat	k5eAaImIp3nP	vychovávat
dvě	dva	k4xCgFnPc1	dva
dcery	dcera	k1gFnPc1	dcera
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
manželství	manželství	k1gNnSc2	manželství
Lemigovové	Lemigovová	k1gFnSc2	Lemigovová
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
září	září	k1gNnSc2	září
2014	[number]	k4	2014
v	v	k7c6	v
přestávce	přestávka	k1gFnSc6	přestávka
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
semifinálovém	semifinálový	k2eAgInSc6d1	semifinálový
zápase	zápas	k1gInSc6	zápas
mužské	mužský	k2eAgFnSc2d1	mužská
dvouhry	dvouhra	k1gFnSc2	dvouhra
US	US	kA	US
Open	Open	k1gMnSc1	Open
na	na	k7c6	na
newyorském	newyorský	k2eAgInSc6d1	newyorský
kurtu	kurt	k1gInSc6	kurt
Arthura	Arthura	k1gFnSc1	Arthura
Ashe	Ashe	k1gFnSc1	Ashe
ji	on	k3xPp3gFnSc4	on
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
veřejně	veřejně	k6eAd1	veřejně
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
Lemigovová	Lemigovová	k1gFnSc1	Lemigovová
žádost	žádost	k1gFnSc4	žádost
přijala	přijmout	k5eAaPmAgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
byla	být	k5eAaImAgFnS	být
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
v	v	k7c6	v
osmiletém	osmiletý	k2eAgInSc6d1	osmiletý
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Toni	Toni	k1gFnSc7	Toni
Laytonovou	Laytonový	k2eAgFnSc7d1	Laytonová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
pouze	pouze	k6eAd1	pouze
neoficiální	neoficiální	k2eAgInSc4d1	neoficiální
sňatek	sňatek	k1gInSc4	sňatek
v	v	k7c6	v
New	New	k1gFnSc6	New
Hampshire	Hampshir	k1gInSc5	Hampshir
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
partnerkou	partnerka	k1gFnSc7	partnerka
tenistky	tenistka	k1gFnSc2	tenistka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bývalá	bývalý	k2eAgFnSc1d1	bývalá
královna	královna	k1gFnSc1	královna
krásy	krása	k1gFnSc2	krása
Judy	judo	k1gNnPc7	judo
Nelsonová	Nelsonová	k1gFnSc5	Nelsonová
<g/>
.	.	kIx.	.
</s>
<s>
Ohlášený	ohlášený	k2eAgInSc1d1	ohlášený
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
Julií	Julie	k1gFnSc7	Julie
Lemigovovou	Lemigovová	k1gFnSc7	Lemigovová
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2015	[number]	k4	2015
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
trenérského	trenérský	k2eAgInSc2d1	trenérský
týmu	tým	k1gInSc2	tým
polské	polský	k2eAgFnSc2d1	polská
hráčky	hráčka	k1gFnSc2	hráčka
Agnieszky	Agnieszka	k1gFnSc2	Agnieszka
Radwańské	Radwańská	k1gFnSc2	Radwańská
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
částečný	částečný	k2eAgInSc4d1	částečný
úvazek	úvazek	k1gInSc4	úvazek
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
pomoci	pomoc	k1gFnSc2	pomoc
tenistce	tenistka	k1gFnSc3	tenistka
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
k	k	k7c3	k
zisku	zisk	k1gInSc3	zisk
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
Grandslamové	grandslamový	k2eAgInPc1d1	grandslamový
tituly	titul	k1gInPc1	titul
-	-	kIx~	-
s	s	k7c7	s
celkově	celkově	k6eAd1	celkově
59	[number]	k4	59
vítězstvími	vítězství	k1gNnPc7	vítězství
absolutní	absolutní	k2eAgInSc4d1	absolutní
rekord	rekord	k1gInSc4	rekord
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
18	[number]	k4	18
titulů	titul	k1gInPc2	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
za	za	k7c7	za
Steffi	Steffi	k1gFnSc7	Steffi
Grafovou	grafový	k2eAgFnSc7d1	grafová
a	a	k8xC	a
Serenou	Serený	k2eAgFnSc7d1	Serený
Williamsovou	Williamsová	k1gFnSc7	Williamsová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
31	[number]	k4	31
titulů	titul	k1gInPc2	titul
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
(	(	kIx(	(
<g/>
rekord	rekord	k1gInSc1	rekord
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
10	[number]	k4	10
titulů	titul	k1gInPc2	titul
ve	v	k7c6	v
smíšené	smíšený	k2eAgFnSc6d1	smíšená
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
(	(	kIx(	(
<g/>
rekord	rekord	k1gInSc1	rekord
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvouhra	dvouhra	k1gFnSc1	dvouhra
žen	žena	k1gFnPc2	žena
1978	[number]	k4	1978
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1979	[number]	k4	1979
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Andreou	Andrea	k1gFnSc7	Andrea
Jaegerovou	Jaegerův	k2eAgFnSc7d1	Jaegerův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Hanou	Hana	k1gFnSc7	Hana
Mandlíkovou	Mandlíková	k1gFnSc7	Mandlíková
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Steffi	Steffi	k1gFnSc7	Steffi
Grafovou	grafový	k2eAgFnSc7d1	grafová
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Zinou	Zina	k1gFnSc7	Zina
Garrisonovou	Garrisonový	k2eAgFnSc7d1	Garrisonový
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
žen	žena	k1gFnPc2	žena
1976	[number]	k4	1976
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1979	[number]	k4	1979
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Billie	Billie	k1gFnPc1	Billie
Jean	Jean	k1gMnSc1	Jean
Kingovovou	Kingovová	k1gFnSc4	Kingovová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Mix	mix	k1gInSc4	mix
1985	[number]	k4	1985
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Mcnameem	Mcnameus	k1gMnSc7	Mcnameus
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Woodfordem	Woodford	k1gInSc7	Woodford
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Jonathanem	Jonathan	k1gMnSc7	Jonathan
Starkem	Starek	k1gMnSc7	Starek
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Leanderem	Leander	k1gInSc7	Leander
<g />
.	.	kIx.	.
</s>
<s>
Paesem	Paes	k1gInSc7	Paes
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
47	[number]	k4	47
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Dvouhra	dvouhra	k1gFnSc1	dvouhra
žen	žena	k1gFnPc2	žena
1983	[number]	k4	1983
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Helenou	Helena	k1gFnSc7	Helena
Sukovou	Suková	k1gFnSc7	Suková
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
<g />
.	.	kIx.	.
</s>
<s>
Steffi	Steffi	k1gFnSc1	Steffi
Grafovou	Grafová	k1gFnSc7	Grafová
(	(	kIx(	(
<g/>
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
Čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
žen	žena	k1gFnPc2	žena
1977	[number]	k4	1977
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Betty	Betty	k1gFnSc7	Betty
Stoveovou	Stoveův	k2eAgFnSc7d1	Stoveův
nad	nad	k7c7	nad
Renee	Renee	k1gFnSc7	Renee
Richardsovou	Richardsová	k1gFnSc7	Richardsová
a	a	k8xC	a
Bettyann	Bettyann	k1gInSc1	Bettyann
Stuartovou	Stuartová	k1gFnSc4	Stuartová
1978	[number]	k4	1978
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Billie	Billie	k1gFnPc1	Billie
Jean	Jean	k1gMnSc1	Jean
Kingovovou	Kingovová	k1gFnSc4	Kingovová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Kerry	Kerr	k1gInPc7	Kerr
M.	M.	kA	M.
Reidovou	Reidová	k1gFnSc4	Reidová
a	a	k8xC	a
Wendy	Wenda	k1gFnPc1	Wenda
Turnbullovou	Turnbullová	k1gFnSc7	Turnbullová
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
1980	[number]	k4	1980
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Billie	Billie	k1gFnSc2	Billie
Jean	Jean	k1gMnSc1	Jean
Kingovovou	Kingovový	k2eAgFnSc4d1	Kingovový
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Betty	Betty	k1gFnSc7	Betty
Stoveovou	Stoveův	k2eAgFnSc7d1	Stoveův
1983	[number]	k4	1983
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c4	nad
Rosalyn	Rosalyn	k1gInSc4	Rosalyn
Fairbankovou	Fairbanková	k1gFnSc4	Fairbanková
a	a	k8xC	a
Candy	Canda	k1gFnPc4	Canda
Reynoldsovou	Reynoldsová	k1gFnSc4	Reynoldsová
1984	[number]	k4	1984
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Anne	Anne	k1gFnSc7	Anne
Hobbsovou	Hobbsová	k1gFnSc7	Hobbsová
a	a	k8xC	a
Wendy	Wend	k1gInPc1	Wend
Turnbullová	Turnbullová	k1gFnSc1	Turnbullová
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Hanou	Hana	k1gFnSc7	Hana
Mandlíkovou	Mandlíková	k1gFnSc7	Mandlíková
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
Wendy	Wenda	k1gFnPc1	Wenda
Turnbullovou	Turnbullová	k1gFnSc7	Turnbullová
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Kathy	Kath	k1gMnPc7	Kath
Jordanovou	Jordanová	k1gFnSc4	Jordanová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
Smylieovou	Smylieův	k2eAgFnSc7d1	Smylieův
<g />
.	.	kIx.	.
</s>
<s>
1989	[number]	k4	1989
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Hana	Hana	k1gFnSc1	Hana
Mandlíková	Mandlíková	k1gFnSc1	Mandlíková
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Mary	Mary	k1gFnSc7	Mary
Joe	Joe	k1gFnSc7	Joe
Fernandezovou	Fernandezová	k1gFnSc7	Fernandezová
a	a	k8xC	a
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverová	k1gFnSc7	Shriverová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Gigi	Gigi	k1gNnSc7	Gigi
Fernandezovou	Fernandezová	k1gFnSc4	Fernandezová
nad	nad	k7c7	nad
Janou	Jana	k1gFnSc7	Jana
Novotnou	Novotná	k1gFnSc7	Novotná
a	a	k8xC	a
Helenou	Helena	k1gFnSc7	Helena
Sukovou	Suková	k1gFnSc7	Suková
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
Smíšená	smíšený	k2eAgFnSc1d1	smíšená
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
1985	[number]	k4	1985
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Heinzem	Heinz	k1gMnSc7	Heinz
Günthardtem	Günthardt	k1gMnSc7	Günthardt
nad	nad	k7c7	nad
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
Smylieovou	Smylieův	k2eAgFnSc7d1	Smylieův
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
<g />
.	.	kIx.	.
</s>
<s>
Fitzgeraldem	Fitzgerald	k1gInSc7	Fitzgerald
1987	[number]	k4	1987
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Emilio	Emilio	k6eAd1	Emilio
Sanchezem	Sanchez	k1gInSc7	Sanchez
nad	nad	k7c4	nad
Betsy	Bets	k1gMnPc4	Bets
Nagelsenovou	Nagelsenová	k1gFnSc4	Nagelsenová
a	a	k8xC	a
Paulem	Paul	k1gMnSc7	Paul
Annaconem	Annacon	k1gMnSc7	Annacon
2006	[number]	k4	2006
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Bobem	Bob	k1gMnSc7	Bob
Bryanem	Bryan	k1gMnSc7	Bryan
nad	nad	k7c7	nad
Květou	Květa	k1gFnSc7	Květa
Peschkeovou	Peschkeův	k2eAgFnSc7d1	Peschkeův
a	a	k8xC	a
Martinem	Martin	k1gMnSc7	Martin
Dammem	Damm	k1gMnSc7	Damm
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
Dvouhra	dvouhra	k1gFnSc1	dvouhra
žen	žena	k1gFnPc2	žena
1982	[number]	k4	1982
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Andreou	Andrea	k1gFnSc7	Andrea
Jaegerovou	Jaegerův	k2eAgFnSc7d1	Jaegerův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
žen	žena	k1gFnPc2	žena
1975	[number]	k4	1975
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Julií	Julie	k1gFnSc7	Julie
Anthonyovou	Anthonyový	k2eAgFnSc7d1	Anthonyový
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Olgou	Olga	k1gFnSc7	Olga
Morozovovou	Morozovová	k1gFnSc7	Morozovová
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Anne	Anne	k1gFnSc7	Anne
Smithovou	Smithový	k2eAgFnSc7d1	Smithová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Rosemary	Rosemar	k1gMnPc7	Rosemar
Casalsovou	Casalsová	k1gFnSc4	Casalsová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Wendy	Wenda	k1gFnSc2	Wenda
Turnbullovou	Turnbullová	k1gFnSc4	Turnbullová
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Claudií	Claudia	k1gFnSc7	Claudia
Kohdeovou	Kohdeův	k2eAgFnSc7d1	Kohdeův
Kilschovou	Kilschová	k1gFnSc4	Kilschová
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc4	Německo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hanou	Hana	k1gFnSc7	Hana
Mandlíkovou	Mandlíková	k1gFnSc7	Mandlíková
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Claudií	Claudia	k1gFnSc7	Claudia
Kohdeovou	Kohdeův	k2eAgFnSc7d1	Kohdeův
Kilschovou	Kilschová	k1gFnSc7	Kilschová
(	(	kIx(	(
<g/>
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
Helenou	Helena	k1gFnSc7	Helena
Sukovou	Suková	k1gFnSc7	Suková
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Andreou	Andrea	k1gFnSc7	Andrea
Temesvariovou	Temesvariový	k2eAgFnSc7d1	Temesvariový
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Steffi	Steffi	k1gFnSc7	Steffi
Grafovou	Grafová	k1gFnSc7	Grafová
(	(	kIx(	(
<g/>
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gabrielou	Gabriela	k1gFnSc7	Gabriela
Sabatiniovou	Sabatiniový	k2eAgFnSc7d1	Sabatiniová
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Steffi	Steffi	k1gFnSc7	Steffi
Grafovou	grafový	k2eAgFnSc7d1	grafová
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gabrielou	Gabriela	k1gFnSc7	Gabriela
Sabatiniovou	Sabatiniový	k2eAgFnSc7d1	Sabatiniová
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Claudií	Claudia	k1gFnSc7	Claudia
Kohdeovou	Kohdeův	k2eAgFnSc7d1	Kohdeův
Kilschovou	Kilschová	k1gFnSc7	Kilschová
(	(	kIx(	(
<g/>
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Helenou	Helena	k1gFnSc7	Helena
Sukovou	Suková	k1gFnSc7	Suková
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
Smíšená	smíšený	k2eAgFnSc1d1	smíšená
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
1974	[number]	k4	1974
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Ivanem	Ivan	k1gMnSc7	Ivan
Molinou	Molina	k1gMnSc7	Molina
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
)	)	kIx)	)
nad	nad	k7c4	nad
Rosie	Rosi	k1gMnPc4	Rosi
Reyesovou	Reyesový	k2eAgFnSc7d1	Reyesová
Darmonovou	Darmonová	k1gFnSc4	Darmonová
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Marcelem	Marcel	k1gMnSc7	Marcel
Larou	Lara	k1gMnSc7	Lara
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Heinzem	Heinz	k1gMnSc7	Heinz
Günthardtem	Günthardt	k1gInSc7	Günthardt
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
Paulou	Paula	k1gFnSc7	Paula
Smithovou	Smithový	k2eAgFnSc7d1	Smithová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Franciscem	Francisce	k1gMnSc7	Francisce
Gonzálezem	González	k1gInSc7	González
(	(	kIx(	(
<g/>
Parayuay	Parayuay	k1gInPc1	Parayuay
<g/>
)	)	kIx)	)
Dvouhra	dvouhra	k1gFnSc1	dvouhra
žen	žena	k1gFnPc2	žena
1981	[number]	k4	1981
<g />
.	.	kIx.	.
</s>
<s>
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c4	nad
Kathy	Katha	k1gFnPc4	Katha
Jordanovou	Jordanová	k1gFnSc4	Jordanová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
vítězka	vítězka	k1gFnSc1	vítězka
nad	nad	k7c7	nad
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
žen	žena	k1gFnPc2	žena
1980	[number]	k4	1980
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Betsy	Bets	k1gMnPc7	Bets
Nagelsenovou	Nagelsenová	k1gFnSc4	Nagelsenová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1983	[number]	k4	1983
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1989	[number]	k4	1989
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverův	k2eAgFnSc7d1	Shriverův
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Smíšená	smíšený	k2eAgFnSc1d1	smíšená
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
2003	[number]	k4	2003
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Leanderem	Leander	k1gMnSc7	Leander
Paesem	Paes	k1gMnSc7	Paes
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
vítězek	vítězka	k1gFnPc2	vítězka
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
1975	[number]	k4	1975
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Renátou	Renáta	k1gFnSc7	Renáta
Tomanovou	Tomanův	k2eAgFnSc7d1	Tomanova
(	(	kIx(	(
<g/>
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
a	a	k8xC	a
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverová	k1gFnSc7	Shriverová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
vítězka	vítězka	k1gFnSc1	vítězka
s	s	k7c7	s
Chris	Chris	k1gFnSc7	Chris
Evertovou	Evertová	k1gFnSc7	Evertová
<g/>
,	,	kIx,	,
Pam	Pam	k1gFnSc7	Pam
Shriverovou	Shriverová	k1gFnSc7	Shriverová
a	a	k8xC	a
Zinou	Zina	k1gFnSc7	Zina
Garrisonovou	Garrisonová	k1gFnSc7	Garrisonová
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Rekordy	rekord	k1gInPc1	rekord
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
na	na	k7c4	na
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
éru	éra	k1gFnSc4	éra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
kvůli	kvůli	k7c3	kvůli
změně	změna	k1gFnSc3	změna
termínu	termín	k1gInSc2	termín
nekonal	konat	k5eNaImAgInS	konat
<g/>
.	.	kIx.	.
</s>
