<s>
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1956	[number]	k4	1956
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
československá	československý	k2eAgFnSc1d1	Československá
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
americká	americký	k2eAgFnSc1d1	americká
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
světová	světový	k2eAgFnSc1d1	světová
tenisová	tenisový	k2eAgFnSc1d1	tenisová
jednička	jednička	k1gFnSc1	jednička
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
332	[number]	k4	332
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
nejdelší	dlouhý	k2eAgNnSc4d3	nejdelší
období	období	k1gNnSc4	období
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
(	(	kIx(	(
<g/>
po	po	k7c6	po
Steffi	Steffi	k1gFnSc6	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
