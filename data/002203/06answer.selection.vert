<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
,	,	kIx,	,
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Holmia	holmium	k1gNnSc2	holmium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
region	region	k1gInSc1	region
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
;	;	kIx,	;
909	[number]	k4	909
976	[number]	k4	976
lidí	člověk	k1gMnPc2	člověk
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
1,4	[number]	k4	1,4
milionu	milion	k4xCgInSc2	milion
ve	v	k7c4	v
větší	veliký	k2eAgFnPc4d2	veliký
městské	městský	k2eAgFnPc4d1	městská
oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
2,2	[number]	k4	2,2
milionu	milion	k4xCgInSc2	milion
v	v	k7c4	v
metropolitní	metropolitní	k2eAgFnPc4d1	metropolitní
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
