<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
,	,	kIx,	,
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Holmia	holmium	k1gNnSc2	holmium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
region	region	k1gInSc1	region
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
;	;	kIx,	;
909	[number]	k4	909
976	[number]	k4	976
lidí	člověk	k1gMnPc2	člověk
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
1,4	[number]	k4	1,4
milionu	milion	k4xCgInSc2	milion
ve	v	k7c4	v
větší	veliký	k2eAgFnPc4d2	veliký
městské	městský	k2eAgFnPc4d1	městská
oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
2,2	[number]	k4	2,2
milionu	milion	k4xCgInSc2	milion
v	v	k7c4	v
metropolitní	metropolitní	k2eAgFnPc4d1	metropolitní
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
14	[number]	k4	14
ostrovech	ostrov	k1gInPc6	ostrov
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
jihovýchodního	jihovýchodní	k2eAgNnSc2d1	jihovýchodní
Švédska	Švédsko	k1gNnSc2	Švédsko
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
jezera	jezero	k1gNnSc2	jezero
Mälaren	Mälarna	k1gFnPc2	Mälarna
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgFnSc6d1	kamenná
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
6	[number]	k4	6
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1252	[number]	k4	1252
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Birger	Birger	k1gMnSc1	Birger
Jarl	jarl	k1gMnSc1	jarl
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgNnSc4d1	kulturní
<g/>
,	,	kIx,	,
mediální	mediální	k2eAgNnSc4d1	mediální
<g/>
,	,	kIx,	,
politické	politický	k2eAgNnSc4d1	politické
i	i	k8xC	i
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
centrum	centrum	k1gNnSc4	centrum
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
tvoří	tvořit	k5eAaImIp3nS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třetinu	třetina	k1gFnSc4	třetina
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
hrubý	hrubý	k2eAgInSc4d1	hrubý
domácí	domácí	k2eAgInSc4d1	domácí
produkt	produkt	k1gInSc4	produkt
<g/>
)	)	kIx)	)
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
10	[number]	k4	10
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
regionů	region	k1gInPc2	region
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
podle	podle	k7c2	podle
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
globálně	globálně	k6eAd1	globálně
důležité	důležitý	k2eAgNnSc1d1	důležité
město	město	k1gNnSc1	město
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
středisko	středisko	k1gNnSc1	středisko
pro	pro	k7c4	pro
korporační	korporační	k2eAgNnSc4d1	korporační
ředitelství	ředitelství	k1gNnSc4	ředitelství
v	v	k7c6	v
severském	severský	k2eAgInSc6d1	severský
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
některé	některý	k3yIgFnSc2	některý
evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnSc2d1	významná
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Institut	institut	k1gInSc1	institut
Karolinska	Karolinsko	k1gNnSc2	Karolinsko
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
hostí	hostit	k5eAaImIp3nS	hostit
udělování	udělování	k1gNnSc4	udělování
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Nejcennější	cenný	k2eAgNnSc1d3	nejcennější
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Vasa	Vasa	k1gFnSc1	Vasa
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejnavštěvovanější	navštěvovaný	k2eAgNnSc1d3	nejnavštěvovanější
neumělecké	umělecký	k2eNgNnSc1d1	neumělecké
muzeum	muzeum	k1gNnSc1	muzeum
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Stockholmské	stockholmský	k2eAgNnSc1d1	Stockholmské
metro	metro	k1gNnSc1	metro
se	se	k3xPyFc4	se
veřejnosti	veřejnost	k1gFnSc3	veřejnost
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
známé	známý	k2eAgNnSc1d1	známé
pro	pro	k7c4	pro
výzdobu	výzdoba	k1gFnSc4	výzdoba
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
stanic	stanice	k1gFnPc2	stanice
<g/>
;	;	kIx,	;
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
nazývány	nazývat	k5eAaImNgFnP	nazývat
jako	jako	k9	jako
nejdelší	dlouhý	k2eAgFnPc1d3	nejdelší
umělecké	umělecký	k2eAgFnPc1d1	umělecká
galerie	galerie	k1gFnPc1	galerie
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
národní	národní	k2eAgFnSc1d1	národní
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
aréna	aréna	k1gFnSc1	aréna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
Solně	solně	k6eAd1	solně
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
hostitel	hostitel	k1gMnSc1	hostitel
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
a	a	k8xC	a
hostil	hostit	k5eAaImAgInS	hostit
i	i	k9	i
jezdeckou	jezdecký	k2eAgFnSc4d1	jezdecká
část	část	k1gFnSc4	část
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
je	být	k5eAaImIp3nS	být
sídlo	sídlo	k1gNnSc4	sídlo
vlády	vláda	k1gFnSc2	vláda
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
vládních	vládní	k2eAgFnPc2d1	vládní
agentur	agentura	k1gFnPc2	agentura
včetně	včetně	k7c2	včetně
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
i	i	k8xC	i
švédský	švédský	k2eAgMnSc1d1	švédský
monarcha	monarcha	k1gMnSc1	monarcha
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Rosenbad	Rosenbad	k1gInSc4	Rosenbad
<g/>
.	.	kIx.	.
parlament	parlament	k1gInSc4	parlament
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c4	v
Riksdag	Riksdag	k1gInSc4	Riksdag
<g/>
.	.	kIx.	.
</s>
<s>
Stockholmský	stockholmský	k2eAgInSc1d1	stockholmský
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgNnSc4d1	oficiální
bydliště	bydliště	k1gNnSc4	bydliště
švédského	švédský	k2eAgMnSc2d1	švédský
monarchy	monarcha	k1gMnSc2	monarcha
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Drottningholmský	Drottningholmský	k2eAgInSc4d1	Drottningholmský
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
soukromá	soukromý	k2eAgFnSc1d1	soukromá
rezidence	rezidence	k1gFnSc1	rezidence
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnSc6d1	ledová
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
8000	[number]	k4	8000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Stockholmu	Stockholm	k1gInSc2	Stockholm
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
migrace	migrace	k1gFnSc2	migrace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k8xC	jak
teploty	teplota	k1gFnPc1	teplota
klesaly	klesat	k5eAaImAgFnP	klesat
<g/>
,	,	kIx,	,
přesouvala	přesouvat	k5eAaImAgNnP	přesouvat
se	se	k3xPyFc4	se
zvířata	zvíře	k1gNnPc1	zvíře
více	hodně	k6eAd2	hodně
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
snesitelnější	snesitelný	k2eAgNnSc1d2	snesitelnější
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
stala	stát	k5eAaPmAgFnS	stát
úrodná	úrodný	k2eAgFnSc1d1	úrodná
a	a	k8xC	a
kompletně	kompletně	k6eAd1	kompletně
oproštěná	oproštěný	k2eAgFnSc1d1	oproštěná
od	od	k7c2	od
permafrostu	permafrost	k1gInSc2	permafrost
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
i	i	k9	i
sem	sem	k6eAd1	sem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
mezi	mezi	k7c7	mezi
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
jezerem	jezero	k1gNnSc7	jezero
Mälaren	Mälarna	k1gFnPc2	Mälarna
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bychom	by	kYmCp1nP	by
dnes	dnes	k6eAd1	dnes
našli	najít	k5eAaPmAgMnP	najít
Staré	Staré	k2eAgNnSc4d1	Staré
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc1	souostroví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
obýváno	obývat	k5eAaImNgNnS	obývat
vikingy	viking	k1gMnPc7	viking
asi	asi	k9	asi
1	[number]	k4	1
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Vikingové	Viking	k1gMnPc1	Viking
měli	mít	k5eAaImAgMnP	mít
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
mnoho	mnoho	k4c4	mnoho
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tehdejšímu	tehdejší	k2eAgInSc3d1	tehdejší
Stockholmu	Stockholm	k1gInSc3	Stockholm
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
i	i	k9	i
jména	jméno	k1gNnPc1	jméno
ze	z	k7c2	z
skandinávských	skandinávský	k2eAgFnPc2d1	skandinávská
ság	sága	k1gFnPc2	sága
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Agnafit	Agnafit	k1gMnSc1	Agnafit
nebo	nebo	k8xC	nebo
Heimskringla	Heimskringla	k1gMnSc1	Heimskringla
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
legendárním	legendární	k2eAgMnSc7d1	legendární
králem	král	k1gMnSc7	král
jménem	jméno	k1gNnSc7	jméno
Agne	Agnus	k1gMnSc5	Agnus
Skjalfarbonde	Skjalfarbond	k1gMnSc5	Skjalfarbond
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
názvu	název	k1gInSc6	název
Stockholm	Stockholm	k1gInSc1	Stockholm
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1252	[number]	k4	1252
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
hodně	hodně	k6eAd1	hodně
kvetl	kvést	k5eAaImAgMnS	kvést
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
železem	železo	k1gNnSc7	železo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
názvu	název	k1gInSc2	název
(	(	kIx(	(
<g/>
stock	stock	k1gInSc1	stock
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
něco	něco	k6eAd1	něco
jako	jako	k8xC	jako
protokol	protokol	k1gInSc4	protokol
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
také	také	k9	také
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
starým	starý	k2eAgNnSc7d1	staré
německým	německý	k2eAgNnSc7d1	německé
slovem	slovo	k1gNnSc7	slovo
(	(	kIx(	(
<g/>
Stock	Stock	k1gMnSc1	Stock
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
názvu	název	k1gInSc2	název
(	(	kIx(	(
<g/>
Holm	Holm	k1gInSc1	Holm
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
ostrůvek	ostrůvek	k1gInSc4	ostrůvek
a	a	k8xC	a
asi	asi	k9	asi
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
ostrůvek	ostrůvek	k1gInSc4	ostrůvek
Helgeandsholmen	Helgeandsholmen	k2eAgInSc4d1	Helgeandsholmen
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
městu	město	k1gNnSc3	město
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
založil	založit	k5eAaPmAgMnS	založit
jarl	jarl	k1gMnSc1	jarl
Birger	Birger	k1gMnSc1	Birger
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chránil	chránit	k5eAaImAgMnS	chránit
Švédsko	Švédsko	k1gNnSc4	Švédsko
před	před	k7c7	před
invazí	invaze	k1gFnSc7	invaze
cizích	cizí	k2eAgMnPc2d1	cizí
námořníků	námořník	k1gMnPc2	námořník
a	a	k8xC	a
zastavil	zastavit	k5eAaPmAgMnS	zastavit
drancování	drancování	k1gNnSc4	drancování
města	město	k1gNnSc2	město
Sigtuna	Sigtuna	k1gFnSc1	Sigtuna
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
nynějšího	nynější	k2eAgNnSc2d1	nynější
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Gamla	Gamla	k1gFnSc1	Gamla
Stan	stan	k1gInSc1	stan
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
postaven	postaven	k2eAgInSc4d1	postaven
na	na	k7c6	na
centrálním	centrální	k2eAgInSc6d1	centrální
ostrově	ostrov	k1gInSc6	ostrov
vedle	vedle	k6eAd1	vedle
Helgeandsholmen	Helgeandsholmen	k1gInSc4	Helgeandsholmen
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
původně	původně	k6eAd1	původně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
k	k	k7c3	k
obchodním	obchodní	k2eAgInPc3d1	obchodní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
silné	silný	k2eAgFnPc4d1	silná
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc4d1	kulturní
vazby	vazba	k1gFnPc4	vazba
s	s	k7c7	s
městy	město	k1gNnPc7	město
Lübeck	Lübeck	k1gInSc1	Lübeck
<g/>
,	,	kIx,	,
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
<g/>
,	,	kIx,	,
Visby	Visb	k1gInPc1	Visb
<g/>
,	,	kIx,	,
Reval	Reval	k1gInSc1	Reval
a	a	k8xC	a
Riga	Riga	k1gFnSc1	Riga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1296	[number]	k4	1296
až	až	k9	až
1478	[number]	k4	1478
byla	být	k5eAaImAgFnS	být
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
Stockholmu	Stockholm	k1gInSc2	Stockholm
tvořena	tvořit	k5eAaImNgFnS	tvořit
z	z	k7c2	z
24	[number]	k4	24
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
polovina	polovina	k1gFnSc1	polovina
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
německy	německy	k6eAd1	německy
mluvícími	mluvící	k2eAgMnPc7d1	mluvící
obyvateli	obyvatel	k1gMnPc7	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Strategický	strategický	k2eAgInSc1d1	strategický
a	a	k8xC	a
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
význam	význam	k1gInSc1	význam
udělal	udělat	k5eAaPmAgInS	udělat
ze	z	k7c2	z
Stockholmu	Stockholm	k1gInSc2	Stockholm
důležitý	důležitý	k2eAgInSc4d1	důležitý
faktor	faktor	k1gInSc4	faktor
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
dánskými	dánský	k2eAgMnPc7d1	dánský
králi	král	k1gMnPc7	král
z	z	k7c2	z
Kalmarské	Kalmarský	k2eAgFnSc2d1	Kalmarská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
národním	národní	k2eAgNnSc7d1	národní
hnutím	hnutí	k1gNnSc7	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
odehrávalo	odehrávat	k5eAaImAgNnS	odehrávat
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dánský	dánský	k2eAgMnSc1d1	dánský
král	král	k1gMnSc1	král
Christian	Christian	k1gMnSc1	Christian
II	II	kA	II
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1520	[number]	k4	1520
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
strhl	strhnout	k5eAaPmAgInS	strhnout
krvavý	krvavý	k2eAgInSc4d1	krvavý
masakr	masakr	k1gInSc4	masakr
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
povstání	povstání	k1gNnSc3	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
Kalmarské	Kalmarský	k2eAgFnSc2d1	Kalmarská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nastoupením	nastoupení	k1gNnSc7	nastoupení
Gustava	Gustav	k1gMnSc2	Gustav
Vasa	Vasus	k1gMnSc2	Vasus
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1523	[number]	k4	1523
a	a	k8xC	a
zřízení	zřízení	k1gNnSc1	zřízení
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Stockholmu	Stockholm	k1gInSc2	Stockholm
začal	začít	k5eAaPmAgInS	začít
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Švédsko	Švédsko	k1gNnSc1	Švédsko
výrazně	výrazně	k6eAd1	výrazně
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hospodářsky	hospodářsky	k6eAd1	hospodářsky
tak	tak	k9	tak
politicky	politicky	k6eAd1	politicky
<g/>
.	.	kIx.	.
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odráželo	odrážet	k5eAaImAgNnS	odrážet
i	i	k9	i
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1610-1680	[number]	k4	1610-1680
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
narostl	narůst	k5eAaPmAgMnS	narůst
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
6	[number]	k4	6
<g/>
x	x	k?	x
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1610	[number]	k4	1610
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1634	[number]	k4	1634
se	se	k3xPyFc4	se
Stockholm	Stockholm	k1gInSc1	Stockholm
oficiálně	oficiálně	k6eAd1	oficiálně
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Švédské	švédský	k2eAgFnSc2d1	švédská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
pravidla	pravidlo	k1gNnPc1	pravidlo
obchodování	obchodování	k1gNnSc2	obchodování
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
udělovala	udělovat	k5eAaImAgFnS	udělovat
Stockholmu	Stockholm	k1gInSc3	Stockholm
určité	určitý	k2eAgFnSc2d1	určitá
pravomoci	pravomoc	k1gFnSc2	pravomoc
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1710	[number]	k4	1710
zabil	zabít	k5eAaPmAgInS	zabít
mor	mor	k1gInSc1	mor
přes	přes	k7c4	přes
20	[number]	k4	20
000	[number]	k4	000
(	(	kIx(	(
<g/>
36	[number]	k4	36
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnSc1	obyvatel
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
velké	velký	k2eAgFnSc2d1	velká
severní	severní	k2eAgFnSc2d1	severní
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
populační	populační	k2eAgInSc1d1	populační
růst	růst	k1gInSc1	růst
zastavil	zastavit	k5eAaPmAgInS	zastavit
a	a	k8xC	a
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
šoku	šok	k1gInSc6	šok
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
ztratil	ztratit	k5eAaPmAgMnS	ztratit
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
jako	jako	k8xC	jako
kapitál	kapitál	k1gInSc4	kapitál
velmoci	velmoc	k1gFnSc2	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Stockholm	Stockholm	k1gInSc1	Stockholm
udržoval	udržovat	k5eAaImAgInS	udržovat
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
jako	jako	k8xC	jako
politické	politický	k2eAgNnSc4d1	politické
centrum	centrum	k1gNnSc4	centrum
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
ve	v	k7c6	v
vyvíjení	vyvíjení	k1gNnSc6	vyvíjení
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Gustava	Gustav	k1gMnSc2	Gustav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Stockholm	Stockholm	k1gInSc1	Stockholm
získal	získat	k5eAaPmAgInS	získat
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
roli	role	k1gFnSc4	role
ze	z	k7c2	z
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
také	také	k9	také
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
dramaticky	dramaticky	k6eAd1	dramaticky
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
imigrantům	imigrant	k1gMnPc3	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
40	[number]	k4	40
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Stockholmu	Stockholm	k1gInSc2	Stockholm
skutečně	skutečně	k6eAd1	skutečně
narozeno	narodit	k5eAaPmNgNnS	narodit
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
vědeckých	vědecký	k2eAgInPc2d1	vědecký
ústavů	ústav	k1gInPc2	ústav
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
Institut	institut	k1gInSc4	institut
Karolinska	Karolinsko	k1gNnSc2	Karolinsko
Stockholm	Stockholm	k1gInSc1	Stockholm
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
moderní	moderní	k2eAgInSc1d1	moderní
<g/>
,	,	kIx,	,
technologicky	technologicky	k6eAd1	technologicky
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
a	a	k8xC	a
etnicky	etnicky	k6eAd1	etnicky
různorodé	různorodý	k2eAgNnSc4d1	různorodé
město	město	k1gNnSc4	město
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
historických	historický	k2eAgFnPc2d1	historická
budov	budova	k1gFnPc2	budova
bylo	být	k5eAaImAgNnS	být
strženo	strhnout	k5eAaPmNgNnS	strhnout
v	v	k7c6	v
modernistické	modernistický	k2eAgFnSc6d1	modernistická
éře	éra	k1gFnSc6	éra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
podstatných	podstatný	k2eAgFnPc2d1	podstatná
částí	část	k1gFnPc2	část
historické	historický	k2eAgFnSc2d1	historická
čtvrti	čtvrt	k1gFnSc2	čtvrt
Klara	Klar	k1gInSc2	Klar
<g/>
,	,	kIx,	,
a	a	k8xC	a
nahradili	nahradit	k5eAaPmAgMnP	nahradit
je	být	k5eAaImIp3nS	být
moderními	moderní	k2eAgInPc7d1	moderní
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nemálo	málo	k6eNd1	málo
historických	historický	k2eAgFnPc2d1	historická
budov	budova	k1gFnPc2	budova
zachovali	zachovat	k5eAaPmAgMnP	zachovat
i	i	k9	i
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1965	[number]	k4	1965
a	a	k8xC	a
1974	[number]	k4	1974
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
stavěli	stavět	k5eAaImAgMnP	stavět
se	se	k3xPyFc4	se
nové	nový	k2eAgFnSc2d1	nová
předměstské	předměstský	k2eAgFnSc2d1	předměstská
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Rinkeby	Rinkeba	k1gFnPc4	Rinkeba
and	and	k?	and
Tensta	Tensta	k1gMnSc1	Tensta
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
bylo	být	k5eAaImAgNnS	být
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
nudné	nudný	k2eAgInPc1d1	nudný
<g/>
,	,	kIx,	,
šedé	šedá	k1gFnPc1	šedá
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
z	z	k7c2	z
betonových	betonový	k2eAgFnPc2d1	betonová
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
stížnosti	stížnost	k1gFnPc4	stížnost
patří	patřit	k5eAaImIp3nS	patřit
ty	ten	k3xDgMnPc4	ten
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
kriminalitu	kriminalita	k1gFnSc4	kriminalita
a	a	k8xC	a
rasismus	rasismus	k1gInSc4	rasismus
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Švédska	Švédsko	k1gNnSc2	Švédsko
blízko	blízko	k7c2	blízko
sladkovodního	sladkovodní	k2eAgNnSc2d1	sladkovodní
jezera	jezero	k1gNnSc2	jezero
Mälaren	Mälarna	k1gFnPc2	Mälarna
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc1	třetí
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
proudí	proudit	k5eAaPmIp3nS	proudit
do	do	k7c2	do
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc3d1	centrální
části	část	k1gFnSc3	část
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
čtrnáct	čtrnáct	k4xCc4	čtrnáct
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Geografické	geografický	k2eAgNnSc1d1	geografické
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
Riddarfjärdenské	Riddarfjärdenský	k2eAgFnSc6d1	Riddarfjärdenský
zátoce	zátoka	k1gFnSc6	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
%	%	kIx~	%
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
30	[number]	k4	30
%	%	kIx~	%
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
parků	park	k1gInPc2	park
a	a	k8xC	a
zelených	zelený	k2eAgFnPc2d1	zelená
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc4d1	podobné
tomu	ten	k3xDgNnSc3	ten
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
-3,0	-3,0	k4	-3,0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
hodnoty	hodnota	k1gFnPc1	hodnota
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
20	[number]	k4	20
až	až	k9	až
25	[number]	k4	25
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
bylo	být	k5eAaImAgNnS	být
36	[number]	k4	36
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
naměřili	naměřit	k5eAaBmAgMnP	naměřit
byste	by	kYmCp2nP	by
ji	on	k3xPp3gFnSc4	on
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1811	[number]	k4	1811
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
-32	-32	k4	-32
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
padla	padnout	k5eAaImAgFnS	padnout
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
zde	zde	k6eAd1	zde
za	za	k7c4	za
rok	rok	k1gInSc4	rok
naprší	napršet	k5eAaPmIp3nS	napršet
539	[number]	k4	539
mm	mm	kA	mm
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejslunnějších	slunný	k2eAgNnPc2d3	nejslunnější
měst	město	k1gNnPc2	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
se	se	k3xPyFc4	se
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
do	do	k7c2	do
města	město	k1gNnSc2	město
dostane	dostat	k5eAaPmIp3nS	dostat
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
nebo	nebo	k8xC	nebo
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Stockholmu	Stockholm	k1gInSc2	Stockholm
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
sektoru	sektor	k1gInSc6	sektor
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
zhruba	zhruba	k6eAd1	zhruba
85	[number]	k4	85
<g/>
%	%	kIx~	%
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
úplná	úplný	k2eAgFnSc1d1	úplná
absence	absence	k1gFnSc1	absence
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dělá	dělat	k5eAaImIp3nS	dělat
ze	z	k7c2	z
Stockholmu	Stockholm	k1gInSc2	Stockholm
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejčistších	čistý	k2eAgFnPc2d3	nejčistší
světových	světový	k2eAgFnPc2d1	světová
metropolí	metropol	k1gFnPc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
i	i	k9	i
na	na	k7c4	na
technologie	technologie	k1gFnPc4	technologie
hi-	hi-	k?	hi-
<g/>
tech.	tech.	k?	tech.
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
zaměstnavatele	zaměstnavatel	k1gMnPc4	zaměstnavatel
města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nS	patřit
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
Ericsson	Ericsson	kA	Ericsson
a	a	k8xC	a
Electrolux	Electrolux	k1gInSc1	Electrolux
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
je	být	k5eAaImIp3nS	být
finančním	finanční	k2eAgInSc7d1	finanční
centrem	centr	k1gInSc7	centr
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
švédské	švédský	k2eAgFnPc1d1	švédská
banky	banka	k1gFnPc1	banka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Nordea	Nordea	k1gFnSc1	Nordea
<g/>
,	,	kIx,	,
Swedbank	Swedbank	k1gInSc1	Swedbank
<g/>
,	,	kIx,	,
Handelsbanken	Handelsbanken	k1gInSc1	Handelsbanken
a	a	k8xC	a
SEB	SEB	kA	SEB
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnPc1d1	hlavní
pojišťovny	pojišťovna	k1gFnPc1	pojišťovna
Skandia	skandium	k1gNnSc2	skandium
<g/>
,	,	kIx,	,
Folksam	Folksam	k1gInSc1	Folksam
a	a	k8xC	a
Trygg-Hansa	Trygg-Hansa	k1gFnSc1	Trygg-Hansa
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
je	být	k5eAaImIp3nS	být
také	také	k9	také
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
švédskou	švédský	k2eAgFnSc4d1	švédská
přední	přední	k2eAgFnSc4d1	přední
burzu	burza	k1gFnSc4	burza
Stockholm	Stockholm	k1gInSc1	Stockholm
Stock	Stock	k1gMnSc1	Stock
Exchange	Exchange	k1gInSc1	Exchange
(	(	kIx(	(
<g/>
Stockholmsbörsen	Stockholmsbörsen	k1gInSc1	Stockholmsbörsen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
města	město	k1gNnSc2	město
i	i	k9	i
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
je	být	k5eAaImIp3nS	být
hodnocena	hodnocen	k2eAgFnSc1d1	hodnocena
jako	jako	k8xS	jako
10	[number]	k4	10
<g/>
.	.	kIx.	.
nejnavštěvovanější	navštěvovaný	k2eAgNnSc1d3	nejnavštěvovanější
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
za	za	k7c4	za
rok	rok	k1gInSc4	rok
zde	zde	k6eAd1	zde
nabídnou	nabídnout	k5eAaPmIp3nP	nabídnout
ubytování	ubytování	k1gNnSc4	ubytování
přes	přes	k7c4	přes
10	[number]	k4	10
milionům	milion	k4xCgInPc3	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
společnosti	společnost	k1gFnPc1	společnost
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
:	:	kIx,	:
Význam	význam	k1gInSc1	význam
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vzdělání	vzdělání	k1gNnSc2	vzdělání
začali	začít	k5eAaPmAgMnP	začít
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
růst	růst	k1gInSc1	růst
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
různé	různý	k2eAgInPc1d1	různý
výzkumné	výzkumný	k2eAgInPc1d1	výzkumný
instituty	institut	k1gInPc1	institut
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
třeba	třeba	k8wRxS	třeba
Stockholmská	stockholmský	k2eAgFnSc1d1	Stockholmská
observatoř	observatoř	k1gFnSc1	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
lékařství	lékařství	k1gNnSc2	lékařství
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
formovala	formovat	k5eAaImAgFnS	formovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
institut	institut	k1gInSc1	institut
Karolinska	Karolinsko	k1gNnSc2	Karolinsko
<g/>
.	.	kIx.	.
</s>
<s>
Kungliga	Kungliga	k1gFnSc1	Kungliga
Tekniska	Teknisko	k1gNnSc2	Teknisko
högskolan	högskolany	k1gInPc2	högskolany
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
<g/>
-li	i	k?	-li
KTH	KTH	kA	KTH
)	)	kIx)	)
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
jako	jako	k8xS	jako
Institut	institut	k1gInSc1	institut
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
institut	institut	k1gInSc4	institut
technologií	technologie	k1gFnPc2	technologie
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
13	[number]	k4	13
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
a	a	k8xC	a
stavu	stav	k1gInSc3	stav
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
52	[number]	k4	52
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
obecně	obecně	k6eAd1	obecně
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
málo	málo	k4c1	málo
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
tu	tu	k6eAd1	tu
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k9	i
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
Karlberg	Karlberg	k1gMnSc1	Karlberg
nebo	nebo	k8xC	nebo
Gymnastik-	Gymnastik-	k1gMnSc1	Gymnastik-
och	och	k0	och
idrottshögskolan	idrottshögskolany	k1gInPc2	idrottshögskolany
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
sportovní	sportovní	k2eAgFnSc1d1	sportovní
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Stockholmští	stockholmský	k2eAgMnPc1d1	stockholmský
studenti	student	k1gMnPc1	student
si	se	k3xPyFc3	se
nejčastěji	často	k6eAd3	často
stěžují	stěžovat	k5eAaImIp3nP	stěžovat
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
studentského	studentský	k2eAgNnSc2d1	studentské
ubytování	ubytování	k1gNnSc2	ubytování
a	a	k8xC	a
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc4d1	vysoký
nájem	nájem	k1gInSc4	nájem
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Stockholm	Stockholm	k1gInSc1	Stockholm
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Švédska	Švédsko	k1gNnSc2	Švédsko
je	být	k5eAaImIp3nS	být
region	region	k1gInSc1	region
Stockholm	Stockholm	k1gInSc1	Stockholm
domovem	domov	k1gInSc7	domov
tří	tři	k4xCgFnPc2	tři
švédských	švédský	k2eAgFnPc2d1	švédská
světových	světový	k2eAgFnPc2d1	světová
dědictví	dědictví	k1gNnSc3	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
:	:	kIx,	:
Drottningholmského	Drottningholmský	k2eAgInSc2d1	Drottningholmský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
Skogskyrkogå	Skogskyrkogå	k1gFnSc2	Skogskyrkogå
a	a	k8xC	a
Birka	Birko	k1gNnSc2	Birko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
Stockholm	Stockholm	k1gInSc1	Stockholm
jmenován	jmenován	k2eAgInSc1d1	jmenován
Evropským	evropský	k2eAgNnSc7d1	Evropské
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
architektonického	architektonický	k2eAgNnSc2d1	architektonické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
nejzajímavějším	zajímavý	k2eAgNnSc7d3	nejzajímavější
místem	místo	k1gNnSc7	místo
Stockholmu	Stockholm	k1gInSc2	Stockholm
Gamla	Gamlo	k1gNnSc2	Gamlo
Stan	stan	k1gInSc1	stan
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstarší	starý	k2eAgFnSc4d3	nejstarší
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
zní	znět	k5eAaImIp3nS	znět
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nadále	nadále	k6eAd1	nadále
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
představuje	představovat	k5eAaImIp3nS	představovat
typický	typický	k2eAgInSc4d1	typický
středověký	středověký	k2eAgInSc4d1	středověký
vzhled	vzhled	k1gInSc4	vzhled
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Gamla	Gaml	k1gMnSc4	Gaml
Stan	stan	k1gInSc4	stan
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
některé	některý	k3yIgFnPc4	některý
pozoruhodné	pozoruhodný	k2eAgFnPc4d1	pozoruhodná
budovy	budova	k1gFnPc4	budova
<g/>
;	;	kIx,	;
Tyska	Tyska	k1gMnSc1	Tyska
Kyrka	Kyrka	k1gMnSc1	Kyrka
<g/>
,	,	kIx,	,
Riddarhuset	Riddarhuset	k1gMnSc1	Riddarhuset
<g/>
,	,	kIx,	,
Tessin	Tessin	k1gMnSc1	Tessin
palác	palác	k1gInSc1	palác
a	a	k8xC	a
Oxenstiernský	Oxenstiernský	k2eAgInSc1d1	Oxenstiernský
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
stavbou	stavba	k1gFnSc7	stavba
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
je	být	k5eAaImIp3nS	být
Riddarholmskyrkan	Riddarholmskyrkan	k1gInSc1	Riddarholmskyrkan
z	z	k7c2	z
konce	konec	k1gInSc2	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
dominantou	dominanta	k1gFnSc7	dominanta
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Stockholmská	stockholmský	k2eAgFnSc1d1	Stockholmská
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
postaven	postavit	k5eAaPmNgInS	postavit
1911-1923	[number]	k4	1911-1923
architektem	architekt	k1gMnSc7	architekt
Ragnarem	Ragnar	k1gMnSc7	Ragnar
Ostbergem	Ostberg	k1gMnSc7	Ostberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Stockholm	Stockholm	k1gInSc4	Stockholm
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
modernismus	modernismus	k1gInSc4	modernismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
slohu	sloh	k1gInSc6	sloh
se	se	k3xPyFc4	se
stavělo	stavět	k5eAaImAgNnS	stavět
mnoho	mnoho	k4c1	mnoho
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
se	se	k3xPyFc4	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
stockholmského	stockholmský	k2eAgNnSc2d1	Stockholmské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
stanice	stanice	k1gFnSc2	stanice
bývají	bývat	k5eAaImIp3nP	bývat
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
nejkrásnější	krásný	k2eAgInPc1d3	nejkrásnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejneobvyklejších	obvyklý	k2eNgFnPc2d3	nejneobvyklejší
budov	budova	k1gFnPc2	budova
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
je	být	k5eAaImIp3nS	být
Jumbohostel	Jumbohostel	k1gInSc1	Jumbohostel
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Stockholm-Arlanda	Stockholm-Arlando	k1gNnSc2	Stockholm-Arlando
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
překypuje	překypovat	k5eAaImIp3nS	překypovat
i	i	k9	i
muzei	muzeum	k1gNnPc7	muzeum
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
a	a	k8xC	a
které	který	k3yRgNnSc1	který
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejproslulejší	proslulý	k2eAgNnSc1d3	nejproslulejší
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
je	být	k5eAaImIp3nS	být
Nationalmuseum	Nationalmuseum	k1gInSc4	Nationalmuseum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
největší	veliký	k2eAgFnSc1d3	veliký
sbírka	sbírka	k1gFnSc1	sbírka
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
;	;	kIx,	;
16	[number]	k4	16
000	[number]	k4	000
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
30	[number]	k4	30
000	[number]	k4	000
uměleckých	umělecký	k2eAgInPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
<g/>
...	...	k?	...
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
nebo	nebo	k8xC	nebo
<g/>
-li	i	k?	-li
Moderna	Moderna	k1gFnSc1	Moderna
Museet	Museet	k1gInSc1	Museet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
muzeu	muzeum	k1gNnSc6	muzeum
najdeme	najít	k5eAaPmIp1nP	najít
díla	dílo	k1gNnPc1	dílo
slavných	slavný	k2eAgMnPc2d1	slavný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Picasso	Picassa	k1gFnSc5	Picassa
a	a	k8xC	a
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
pozoruhodnými	pozoruhodný	k2eAgFnPc7d1	pozoruhodná
muzei	muzeum	k1gNnPc7	muzeum
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Stockholmské	stockholmský	k2eAgNnSc4d1	Stockholmské
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
Fotografiska	Fotografiska	k1gFnSc1	Fotografiska
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
Skansen	skansen	k1gInSc1	skansen
<g/>
,	,	kIx,	,
Nordic	Nordic	k1gMnSc1	Nordic
Museum	museum	k1gNnSc4	museum
nebo	nebo	k8xC	nebo
Švédské	švédský	k2eAgNnSc4d1	švédské
muzeum	muzeum	k1gNnSc4	muzeum
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
divadlem	divadlo	k1gNnSc7	divadlo
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
je	být	k5eAaImIp3nS	být
Královské	královský	k2eAgNnSc1d1	královské
dramatické	dramatický	k2eAgNnSc1d1	dramatické
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
Kungliga	Kungliga	k1gFnSc1	Kungliga
Dramatiska	Dramatisko	k1gNnSc2	Dramatisko
Teatern	Teaterna	k1gFnPc2	Teaterna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
divadel	divadlo	k1gNnPc2	divadlo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
i	i	k8xC	i
Královská	královský	k2eAgFnSc1d1	královská
opera	opera	k1gFnSc1	opera
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
významná	významný	k2eAgNnPc1d1	významné
muzea	muzeum	k1gNnPc1	muzeum
jsou	být	k5eAaImIp3nP	být
Stockholms	Stockholms	k1gInSc4	Stockholms
stadsteater	stadsteatra	k1gFnPc2	stadsteatra
<g/>
,	,	kIx,	,
Folkoperan	Folkoperana	k1gFnPc2	Folkoperana
<g/>
,	,	kIx,	,
Moderna	Moderna	k1gFnSc1	Moderna
dansteatern	dansteatern	k1gInSc1	dansteatern
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
Čínské	čínský	k2eAgNnSc4d1	čínské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
součást	součást	k1gFnSc4	součást
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
Gröna	Gröno	k1gNnPc4	Gröno
Lund	Lunda	k1gFnPc2	Lunda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Djurgå	Djurgå	k1gFnSc2	Djurgå
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
atrakcí	atrakce	k1gFnPc2	atrakce
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc7d1	populární
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
a	a	k8xC	a
navštíví	navštívit	k5eAaPmIp3nS	navštívit
jej	on	k3xPp3gMnSc4	on
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
od	od	k7c2	od
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Gröna	Gröna	k1gFnSc1	Gröna
Lund	Lunda	k1gFnPc2	Lunda
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mediálního	mediální	k2eAgNnSc2d1	mediální
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
Stockholm	Stockholm	k1gInSc1	Stockholm
mediálním	mediální	k2eAgNnSc7d1	mediální
centrem	centrum	k1gNnSc7	centrum
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tři	tři	k4xCgInPc1	tři
celonárodní	celonárodní	k2eAgInPc1d1	celonárodní
deníky	deník	k1gInPc1	deník
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
i	i	k8xC	i
rádií	rádio	k1gNnPc2	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Hit	hit	k1gInSc1	hit
mezi	mezi	k7c7	mezi
počítačovými	počítačový	k2eAgFnPc7d1	počítačová
hrami	hra	k1gFnPc7	hra
<g/>
,	,	kIx,	,
Minecraft	Minecraft	k2eAgInSc1d1	Minecraft
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
právě	právě	k9	právě
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Jacob	Jacoba	k1gFnPc2	Jacoba
Bjerknes	Bjerknesa	k1gFnPc2	Bjerknesa
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
-	-	kIx~	-
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Alfred	Alfred	k1gMnSc1	Alfred
Nobel	Nobel	k1gMnSc1	Nobel
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
-	-	kIx~	-
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
Carl-Gustav	Carl-Gustav	k1gFnSc6	Carl-Gustav
Arvid	Arvida	k1gFnPc2	Arvida
Rossby	Rossba	k1gFnSc2	Rossba
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
-	-	kIx~	-
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
August	August	k1gMnSc1	August
Strindberg	Strindberg	k1gMnSc1	Strindberg
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
-	-	kIx~	-
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
Bogotá	Bogotá	k1gFnSc1	Bogotá
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Nuuk	Nuuka	k1gFnPc2	Nuuka
<g/>
,	,	kIx,	,
Grónsko	Grónsko	k1gNnSc1	Grónsko
Reykjavík	Reykjavík	k1gInSc1	Reykjavík
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
Riga	Riga	k1gFnSc1	Riga
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Podgorica	Podgoric	k1gInSc2	Podgoric
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Stockholm	Stockholm	k1gInSc1	Stockholm
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stockholm	Stockholm	k1gInSc1	Stockholm
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Stockholm	Stockholm	k1gInSc1	Stockholm
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Stockholm	Stockholm	k1gInSc1	Stockholm
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Stockholm	Stockholm	k1gInSc1	Stockholm
je	být	k5eAaImIp3nS	být
zelená	zelený	k2eAgFnSc1d1	zelená
metropole	metropole	k1gFnSc1	metropole
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
Stockholm	Stockholm	k1gInSc4	Stockholm
a	a	k8xC	a
atrakce	atrakce	k1gFnPc4	atrakce
Slovenský	slovenský	k2eAgInSc1d1	slovenský
cestopis	cestopis	k1gInSc1	cestopis
</s>
