<s>
Johann	Johann	k1gMnSc1	Johann
Elias	Elias	k1gMnSc1	Elias
Schlegel	Schlegel	k1gMnSc1	Schlegel
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1719	[number]	k4	1719
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1749	[number]	k4	1749
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
právo	právo	k1gNnSc4	právo
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1743	[number]	k4	1743
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
tajemníkem	tajemník	k1gInSc7	tajemník
svého	svůj	k3xOyFgMnSc2	svůj
příbuzného	příbuzný	k1gMnSc2	příbuzný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
saským	saský	k2eAgMnSc7d1	saský
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
v	v	k7c6	v
Sorø	Sorø	k1gFnSc6	Sorø
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Přispíval	přispívat	k5eAaImAgMnS	přispívat
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
Bremer	Bremra	k1gFnPc2	Bremra
Beiträge	Beiträg	k1gFnSc2	Beiträg
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
vydával	vydávat	k5eAaPmAgInS	vydávat
týdeník	týdeník	k1gInSc1	týdeník
Der	drát	k5eAaImRp2nS	drát
Fremde	Fremd	k1gInSc5	Fremd
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
veršovaná	veršovaný	k2eAgNnPc1d1	veršované
dramata	drama	k1gNnPc1	drama
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
výstavbu	výstavba	k1gFnSc4	výstavba
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
alexandríny	alexandrín	k1gInPc4	alexandrín
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
zejména	zejména	k9	zejména
dodnes	dodnes	k6eAd1	dodnes
hraná	hraný	k2eAgFnSc1d1	hraná
tragédie	tragédie	k1gFnSc1	tragédie
Canut	Canut	k1gMnSc1	Canut
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čerpající	čerpající	k2eAgInSc4d1	čerpající
z	z	k7c2	z
dánské	dánský	k2eAgFnSc2d1	dánská
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
konverzační	konverzační	k2eAgFnSc1d1	konverzační
komedie	komedie	k1gFnSc1	komedie
die	die	k?	die
Die	Die	k1gFnSc1	Die
stumme	stumit	k5eAaPmRp1nP	stumit
Schönheit	Schönheit	k1gInSc4	Schönheit
(	(	kIx(	(
<g/>
1748	[number]	k4	1748
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
literárně	literárně	k6eAd1	literárně
historická	historický	k2eAgFnSc1d1	historická
práce	práce	k1gFnSc1	práce
Vergleichung	Vergleichung	k1gMnSc1	Vergleichung
Shakespears	Shakespears	k1gInSc1	Shakespears
und	und	k?	und
Andreas	Andreas	k1gInSc1	Andreas
Gryphs	Gryphs	k1gInSc1	Gryphs
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
moderní	moderní	k2eAgFnSc2d1	moderní
literární	literární	k2eAgFnSc2d1	literární
komparatistiky	komparatistika	k1gFnSc2	komparatistika
<g/>
.	.	kIx.	.
</s>
<s>
Vergleichung	Vergleichung	k1gInSc1	Vergleichung
Shakespears	Shakespears	k1gInSc1	Shakespears
und	und	k?	und
Andreas	Andreas	k1gInSc1	Andreas
Gryphs	Gryphs	k1gInSc1	Gryphs
<g/>
.	.	kIx.	.
1741	[number]	k4	1741
Hermann	Hermann	k1gMnSc1	Hermann
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
Trauerspiel	Trauerspiela	k1gFnPc2	Trauerspiela
<g/>
.	.	kIx.	.
1743	[number]	k4	1743
Canut	Canut	k1gInSc4	Canut
<g/>
,	,	kIx,	,
ein	ein	k?	ein
Trauerspiel	Trauerspiela	k1gFnPc2	Trauerspiela
<g/>
.	.	kIx.	.
1746	[number]	k4	1746
Die	Die	k1gFnSc2	Die
stumme	stumit	k5eAaPmRp1nP	stumit
Schönheit	Schönheita	k1gFnPc2	Schönheita
<g/>
.	.	kIx.	.
1748	[number]	k4	1748
Orest	Orest	k1gInSc4	Orest
und	und	k?	und
Pylades	Pylades	k1gMnSc1	Pylades
<g/>
.	.	kIx.	.
</s>
<s>
Veröffentlicht	Veröffentlicht	k1gInSc1	Veröffentlicht
1760	[number]	k4	1760
<g/>
,	,	kIx,	,
Der	drát	k5eAaImRp2nS	drát
Triumph	Triumph	k1gInSc4	Triumph
der	drát	k5eAaImRp2nS	drát
guten	guten	k1gInSc4	guten
Frauen	Frauen	k1gInSc1	Frauen
<g/>
.	.	kIx.	.
1769	[number]	k4	1769
Theoretische	Theoretisch	k1gInSc2	Theoretisch
Texte	text	k1gInSc5	text
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
glückliche	glücklichat	k5eAaPmIp3nS	glücklichat
Insel	Insel	k1gInSc1	Insel
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
geschäftige	geschäftige	k1gNnSc4	geschäftige
Müßiggänger	Müßiggänger	k1gInSc1	Müßiggänger
<g/>
.	.	kIx.	.
</s>
