<s>
Kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Johann	Johann	k1gMnSc1	Johann
Elias	Elias	k1gMnSc1	Elias
Schlegel	Schlegel	k1gMnSc1	Schlegel
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
studoval	studovat	k5eAaImAgMnS	studovat
právo	právo	k1gNnSc4	právo
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
?	?	kIx.	?
</s>
