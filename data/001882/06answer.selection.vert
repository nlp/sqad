<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
bakterií	bakterie	k1gFnPc2	bakterie
jsou	být	k5eAaImIp3nP	být
dle	dle	k7c2	dle
výzkumů	výzkum	k1gInPc2	výzkum
schopny	schopen	k2eAgFnPc1d1	schopna
přežít	přežít	k5eAaPmF	přežít
i	i	k9	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
a	a	k8xC	a
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
Bakterie	bakterie	k1gFnSc1	bakterie
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc4d1	různý
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
významným	významný	k2eAgNnSc7d1	významné
hlediskem	hledisko	k1gNnSc7	hledisko
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
kyselost	kyselost	k1gFnSc1	kyselost
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
