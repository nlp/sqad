<s>
Španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
kastilština	kastilština	k1gFnSc1	kastilština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgInPc2d3	nejrozšířenější
světových	světový	k2eAgInPc2d1	světový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Španělština	španělština	k1gFnSc1	španělština
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
států	stát	k1gInPc2	stát
Jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
v	v	k7c6	v
Rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
Guineji	Guinea	k1gFnSc6	Guinea
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
je	být	k5eAaImIp3nS	být
také	také	k9	také
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
400	[number]	k4	400
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
lidové	lidový	k2eAgFnSc2d1	lidová
latiny	latina	k1gFnSc2	latina
římských	římský	k2eAgFnPc2d1	římská
provincií	provincie	k1gFnPc2	provincie
Hispania	Hispanium	k1gNnSc2	Hispanium
Citerior	Citeriora	k1gFnPc2	Citeriora
a	a	k8xC	a
Hispania	Hispanium	k1gNnSc2	Hispanium
Ulterior	Ulteriora	k1gFnPc2	Ulteriora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vpádu	vpád	k1gInSc6	vpád
Arabů	Arab	k1gMnPc2	Arab
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ustavily	ustavit	k5eAaPmAgInP	ustavit
dva	dva	k4xCgInPc1	dva
regiony	region	k1gInPc1	region
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
jazykovým	jazykový	k2eAgInSc7d1	jazykový
vývojem	vývoj	k1gInSc7	vývoj
<g/>
:	:	kIx,	:
jižní	jižní	k2eAgInSc1d1	jižní
Al-Andalus	Al-Andalus	k1gInSc1	Al-Andalus
hovořil	hovořit	k5eAaImAgInS	hovořit
dialekty	dialekt	k1gInPc4	dialekt
ovlivněnými	ovlivněný	k2eAgFnPc7d1	ovlivněná
arabštinou	arabština	k1gFnSc7	arabština
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
shrnovány	shrnovat	k5eAaImNgInP	shrnovat
pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
mozarabština	mozarabština	k1gFnSc1	mozarabština
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
ovlivněném	ovlivněný	k2eAgInSc6d1	ovlivněný
gótskou	gótský	k2eAgFnSc4d1	gótská
kulturou	kultura	k1gFnSc7	kultura
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
kastilštiny	kastilština	k1gFnSc2	kastilština
postupně	postupně	k6eAd1	postupně
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
katalánština	katalánština	k1gFnSc1	katalánština
<g/>
,	,	kIx,	,
asturština	asturština	k1gFnSc1	asturština
<g/>
,	,	kIx,	,
aragonština	aragonština	k1gFnSc1	aragonština
a	a	k8xC	a
galicijština	galicijština	k1gFnSc1	galicijština
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
kastilský	kastilský	k2eAgInSc1d1	kastilský
dialekt	dialekt	k1gInSc1	dialekt
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
Burgosem	Burgos	k1gInSc7	Burgos
a	a	k8xC	a
Kantábrií	Kantábrie	k1gFnSc7	Kantábrie
<g/>
,	,	kIx,	,
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
jednak	jednak	k8xC	jednak
arabštinou	arabština	k1gFnSc7	arabština
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
baskičtinou	baskičtina	k1gFnSc7	baskičtina
od	od	k7c2	od
severovýchodu	severovýchod	k1gInSc2	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
poloostrově	poloostrov	k1gInSc6	poloostrov
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
díky	díky	k7c3	díky
reconquistě	reconquista	k1gFnSc3	reconquista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
během	během	k7c2	během
procesu	proces	k1gInSc2	proces
sjednocení	sjednocení	k1gNnSc2	sjednocení
španělských	španělský	k2eAgNnPc2d1	španělské
království	království	k1gNnPc2	království
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Antonio	Antonio	k1gMnSc1	Antonio
de	de	k?	de
Nebrija	Nebrija	k1gMnSc1	Nebrija
v	v	k7c6	v
Salamance	Salamanka	k1gFnSc6	Salamanka
svůj	svůj	k3xOyFgInSc4	svůj
spis	spis	k1gInSc4	spis
Grammatica	Grammaticum	k1gNnSc2	Grammaticum
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgNnSc4	první
pojednání	pojednání	k1gNnSc4	pojednání
o	o	k7c6	o
kastilské	kastilský	k2eAgFnSc6d1	Kastilská
gramatice	gramatika	k1gFnSc6	gramatika
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
o	o	k7c4	o
první	první	k4xOgFnSc4	první
gramatiku	gramatika	k1gFnSc4	gramatika
vulgárního	vulgární	k2eAgInSc2d1	vulgární
(	(	kIx(	(
<g/>
lidového	lidový	k2eAgInSc2d1	lidový
<g/>
)	)	kIx)	)
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
textem	text	k1gInSc7	text
v	v	k7c6	v
kastilštině	kastilština	k1gFnSc6	kastilština
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
Glosas	Glosas	k1gInSc4	Glosas
Emilianenses	Emilianensesa	k1gFnPc2	Emilianensesa
<g/>
,	,	kIx,	,
sepsané	sepsaný	k2eAgInPc1d1	sepsaný
baskickými	baskický	k2eAgInPc7d1	baskický
mnichy	mnich	k1gInPc7	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
kastilštinu	kastilština	k1gFnSc4	kastilština
používán	používán	k2eAgMnSc1d1	používán
častěji	často	k6eAd2	často
název	název	k1gInSc1	název
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
jakožto	jakožto	k8xS	jakožto
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
domluví	domluvit	k5eAaPmIp3nP	domluvit
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
sjednocených	sjednocený	k2eAgNnPc2d1	sjednocené
španělských	španělský	k2eAgNnPc2d1	španělské
království	království	k1gNnPc2	království
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
různé	různý	k2eAgFnPc4d1	různá
polemiky	polemika	k1gFnPc4	polemika
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
silou	síla	k1gFnSc7	síla
trvají	trvat	k5eAaImIp3nP	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
Slovník	slovník	k1gInSc1	slovník
španělského	španělský	k2eAgInSc2d1	španělský
jazyka	jazyk	k1gInSc2	jazyk
Královské	královský	k2eAgFnSc2d1	královská
akademie	akademie	k1gFnSc2	akademie
výrazy	výraz	k1gInPc4	výraz
kastilština	kastilština	k1gFnSc1	kastilština
a	a	k8xC	a
španělština	španělština	k1gFnSc1	španělština
jakožto	jakožto	k8xS	jakožto
synonyma	synonymum	k1gNnSc2	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnPc1d3	nejvýraznější
fonologické	fonologický	k2eAgFnPc1d1	fonologická
změny	změna	k1gFnPc1	změna
oproti	oproti	k7c3	oproti
latině	latina	k1gFnSc3	latina
představují	představovat	k5eAaImIp3nP	představovat
hláskové	hláskový	k2eAgFnPc1d1	hlásková
změny	změna	k1gFnPc1	změna
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
vita	vit	k2eAgFnSc1d1	Vita
→	→	k?	→
špaň	špaň	k1gFnSc1	špaň
<g/>
.	.	kIx.	.
vida	vida	k?	vida
–	–	k?	–
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
iuventus	iuventus	k1gInSc1	iuventus
→	→	k?	→
juventud	juventud	k1gInSc1	juventud
–	–	k?	–
mládí	mládí	k1gNnSc2	mládí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diftongizace	diftongizace	k1gFnSc1	diftongizace
(	(	kIx(	(
<g/>
terra	terra	k1gMnSc1	terra
→	→	k?	→
tierra	tierra	k1gMnSc1	tierra
–	–	k?	–
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
či	či	k8xC	či
palatalizace	palatalizace	k1gFnSc1	palatalizace
<g/>
.	.	kIx.	.
</s>
<s>
Španělština	španělština	k1gFnSc1	španělština
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
znak	znak	k1gInSc1	znak
ñ	ñ	k?	ñ
<g/>
;	;	kIx,	;
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
spřežky	spřežka	k1gFnPc1	spřežka
ch	ch	k0	ch
a	a	k8xC	a
ll	ll	k?	ll
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
abeceda	abeceda	k1gFnSc1	abeceda
tak	tak	k6eAd1	tak
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
29	[number]	k4	29
písmen	písmeno	k1gNnPc2	písmeno
<g/>
:	:	kIx,	:
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
(	(	kIx(	(
<g/>
be	be	k?	be
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
C	C	kA	C
(	(	kIx(	(
<g/>
ce	ce	k?	ce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
(	(	kIx(	(
<g/>
che	che	k0	che
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
D	D	kA	D
(	(	kIx(	(
<g/>
de	de	k?	de
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
F	F	kA	F
(	(	kIx(	(
<g/>
efe	efe	k?	efe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
G	G	kA	G
(	(	kIx(	(
<g/>
ge	ge	k?	ge
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
H	H	kA	H
(	(	kIx(	(
<g/>
hache	hach	k1gMnSc2	hach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
I	i	k9	i
(	(	kIx(	(
<g/>
i	i	k8xC	i
latina	latina	k1gFnSc1	latina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
J	J	kA	J
(	(	kIx(	(
<g/>
jota	jota	k1gFnSc1	jota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
K	k	k7c3	k
(	(	kIx(	(
<g/>
ka	ka	k?	ka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
L	L	kA	L
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
ele	ela	k1gFnSc6	ela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
LL	LL	kA	LL
(	(	kIx(	(
<g/>
elle	ell	k1gMnSc2	ell
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
M	M	kA	M
(	(	kIx(	(
<g/>
eme	eme	k?	eme
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
N	N	kA	N
(	(	kIx(	(
<g/>
ene	ene	k?	ene
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ñ	Ñ	k?	Ñ
(	(	kIx(	(
<g/>
eñ	eñ	k?	eñ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
P	P	kA	P
(	(	kIx(	(
<g/>
pe	pe	k?	pe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Q	Q	kA	Q
(	(	kIx(	(
<g/>
cu	cu	k?	cu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
R	R	kA	R
(	(	kIx(	(
<g/>
erre	err	k1gMnSc2	err
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
S	s	k7c7	s
(	(	kIx(	(
<g/>
ese	ese	k?	ese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
T	T	kA	T
(	(	kIx(	(
<g/>
te	te	k?	te
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
V	V	kA	V
(	(	kIx(	(
<g/>
uve	uve	k?	uve
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
W	W	kA	W
(	(	kIx(	(
<g/>
uve	uve	k?	uve
doble	doble	k1gInSc1	doble
/	/	kIx~	/
doble	doble	k1gInSc1	doble
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
X	X	kA	X
(	(	kIx(	(
<g/>
equis	equis	k1gInSc1	equis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Y	Y	kA	Y
(	(	kIx(	(
<g/>
i	i	k8xC	i
griega	griega	k1gFnSc1	griega
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Z	z	k7c2	z
(	(	kIx(	(
<g/>
zeta	zet	k1gMnSc2	zet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přízvučné	přízvučný	k2eAgFnPc1d1	přízvučná
samohlásky	samohláska	k1gFnPc1	samohláska
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
čárkou	čárka	k1gFnSc7	čárka
(	(	kIx(	(
<g/>
á	á	k0	á
<g/>
,	,	kIx,	,
é	é	k0	é
<g/>
,	,	kIx,	,
í	í	k0	í
<g/>
,	,	kIx,	,
ó	ó	k0	ó
<g/>
,	,	kIx,	,
ú	ú	k0	ú
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nečtou	číst	k5eNaImIp3nP	číst
se	se	k3xPyFc4	se
dlouze	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
výslovnosti	výslovnost	k1gFnSc6	výslovnost
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
akcent	akcent	k1gInSc1	akcent
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nestojí	stát	k5eNaImIp3nS	stát
li	li	k8xS	li
na	na	k7c6	na
předposlední	předposlední	k2eAgFnSc6d1	předposlední
slabice	slabika	k1gFnSc6	slabika
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
slabice	slabika	k1gFnSc6	slabika
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
zakončených	zakončený	k2eAgFnPc2d1	zakončená
na	na	k7c4	na
souhlásku	souhláska	k1gFnSc4	souhláska
kromě	kromě	k7c2	kromě
n	n	k0	n
a	a	k8xC	a
s.	s.	k?	s.
Při	při	k7c6	při
výslovnosti	výslovnost	k1gFnSc6	výslovnost
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ch	ch	k0	ch
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
čte	číst	k5eAaImIp3nS	číst
jako	jako	k8xS	jako
/	/	kIx~	/
<g/>
č	č	k0	č
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
j	j	k?	j
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
vždy	vždy	k6eAd1	vždy
jako	jako	k8xS	jako
/	/	kIx~	/
<g/>
ch	ch	k0	ch
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
+	+	kIx~	+
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
vždy	vždy	k6eAd1	vždy
jako	jako	k8xS	jako
/	/	kIx~	/
<g/>
ka	ka	k?	ka
<g/>
,	,	kIx,	,
<g/>
ko	ko	k?	ko
<g/>
,	,	kIx,	,
<g/>
ku	k	k7c3	k
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
+	+	kIx~	+
<g/>
e	e	k0	e
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
i	i	k9	i
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
vždy	vždy	k6eAd1	vždy
jako	jako	k8xC	jako
/	/	kIx~	/
<g/>
θ	θ	k?	θ
<g/>
,	,	kIx,	,
θ	θ	k?	θ
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
+	+	kIx~	+
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
o	o	k0	o
<g/>
,	,	kIx,	,
<g/>
u	u	k7c2	u
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
vždy	vždy	k6eAd1	vždy
jako	jako	k8xC	jako
/	/	kIx~	/
<g/>
ga	ga	k?	ga
<g/>
,	,	kIx,	,
<g/>
go	go	k?	go
<g/>
,	,	kIx,	,
<g/>
gu	gu	k?	gu
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
<g />
.	.	kIx.	.
</s>
<s>
+	+	kIx~	+
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
vždy	vždy	k6eAd1	vždy
jako	jako	k8xS	jako
/	/	kIx~	/
<g/>
che	che	k0	che
<g/>
,	,	kIx,	,
<g/>
chi	chi	k0	chi
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
gua	gua	k?	gua
<g/>
,	,	kIx,	,
guo	guo	k?	guo
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
vždy	vždy	k6eAd1	vždy
jako	jako	k8xS	jako
/	/	kIx~	/
<g/>
gua	gua	k?	gua
<g/>
,	,	kIx,	,
guo	guo	k?	guo
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
gui	gui	k?	gui
<g/>
,	,	kIx,	,
gue	gue	k?	gue
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
vždy	vždy	k6eAd1	vždy
jako	jako	k8xS	jako
/	/	kIx~	/
<g/>
gi	gi	k?	gi
<g/>
,	,	kIx,	,
<g/>
ge	ge	k?	ge
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
h	h	k?	h
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nečte	číst	k5eNaImIp3nS	číst
<g/>
,	,	kIx,	,
qu	qu	k?	qu
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
vždy	vždy	k6eAd1	vždy
jako	jako	k8xC	jako
/	/	kIx~	/
<g/>
k	k	k7c3	k
<g/>
/	/	kIx~	/
z	z	k7c2	z
jako	jako	k9	jako
/	/	kIx~	/
<g/>
θ	θ	k?	θ
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
i	i	k9	i
tzv.	tzv.	kA	tzv.
cedilla	cedilla	k1gFnSc1	cedilla
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
písmeno	písmeno	k1gNnSc4	písmeno
ç	ç	k?	ç
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
či	či	k8xC	či
turečtině	turečtina	k1gFnSc6	turečtina
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
španělština	španělština	k1gFnSc1	španělština
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
užitím	užití	k1gNnSc7	užití
písmena	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
x	x	k?	x
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
indiánských	indiánský	k2eAgInPc2d1	indiánský
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
české	český	k2eAgFnSc3d1	Česká
[	[	kIx(	[
<g/>
š	š	k?	š
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Mexiko	Mexiko	k1gNnSc1	Mexiko
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
jedné	jeden	k4xCgFnSc2	jeden
domorodé	domorodý	k2eAgFnSc2d1	domorodá
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
byli	být	k5eAaImAgMnP	být
Mexikové	Mexikový	k2eAgNnSc4d1	Mexikový
(	(	kIx(	(
<g/>
čteno	čten	k2eAgNnSc4d1	čteno
[	[	kIx(	[
<g/>
Mešikové	Mešikový	k2eAgNnSc4d1	Mešikový
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
dalších	další	k2eAgFnPc2d1	další
hláskotvorných	hláskotvorný	k2eAgFnPc2d1	hláskotvorný
změn	změna	k1gFnPc2	změna
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
Mexiko	Mexiko	k1gNnSc1	Mexiko
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
[	[	kIx(	[
<g/>
Méchiko	Méchika	k1gFnSc5	Méchika
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Mexičané	Mexičan	k1gMnPc1	Mexičan
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
používají	používat	k5eAaImIp3nP	používat
zápis	zápis	k1gInSc4	zápis
Méjico	Méjico	k1gMnSc1	Méjico
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pravidlům	pravidlo	k1gNnPc3	pravidlo
současné	současný	k2eAgFnSc2d1	současná
španělské	španělský	k2eAgFnSc2d1	španělská
fonetiky	fonetika	k1gFnSc2	fonetika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starších	starý	k2eAgInPc6d2	starší
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
písmenem	písmeno	k1gNnSc7	písmeno
"	"	kIx"	"
<g/>
x	x	k?	x
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čteným	čtený	k2eAgMnPc3d1	čtený
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
ch	ch	k0	ch
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Don	Don	k1gMnSc1	Don
Quixote	Quixot	k1gInSc5	Quixot
místo	místo	k1gNnSc4	místo
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
<g/>
)	)	kIx)	)
–	–	k?	–
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
je	být	k5eAaImIp3nS	být
však	však	k9	však
již	již	k6eAd1	již
archaické	archaický	k2eAgNnSc1d1	archaické
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
x	x	k?	x
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
čte	číst	k5eAaImIp3nS	číst
jako	jako	k8xS	jako
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
/	/	kIx~	/
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
výrazů	výraz	k1gInPc2	výraz
původních	původní	k2eAgFnPc2d1	původní
latinskoamerických	latinskoamerický	k2eAgFnPc2d1	latinskoamerická
kultur	kultura	k1gFnPc2	kultura
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Xaman	Xaman	k1gInSc1	Xaman
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
mayštiny	mayština	k1gFnSc2	mayština
a	a	k8xC	a
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
[	[	kIx(	[
<g/>
šaman	šaman	k1gMnSc1	šaman
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
sever	sever	k1gInSc1	sever
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
důležitý	důležitý	k2eAgInSc1d1	důležitý
pojem	pojem	k1gInSc1	pojem
v	v	k7c6	v
mayském	mayský	k2eAgNnSc6d1	mayské
rituálním	rituální	k2eAgNnSc6d1	rituální
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Španělská	španělský	k2eAgFnSc1d1	španělská
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1	španělské
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
rody	rod	k1gInPc4	rod
<g/>
:	:	kIx,	:
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
.	.	kIx.	.
</s>
<s>
Slovesný	slovesný	k2eAgInSc1d1	slovesný
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
bohatý	bohatý	k2eAgInSc1d1	bohatý
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
indikativu	indikativ	k1gInSc2	indikativ
(	(	kIx(	(
<g/>
indicativo	indicativa	k1gFnSc5	indicativa
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
široké	široký	k2eAgNnSc4d1	široké
použití	použití	k1gNnSc4	použití
subjunktiv	subjunktiva	k1gFnPc2	subjunktiva
(	(	kIx(	(
<g/>
subjuntivo	subjuntiva	k1gFnSc5	subjuntiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indikativu	indikativ	k1gInSc6	indikativ
pak	pak	k6eAd1	pak
například	například	k6eAd1	například
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
minulosti	minulost	k1gFnSc2	minulost
existuje	existovat	k5eAaImIp3nS	existovat
pět	pět	k4xCc4	pět
časů	čas	k1gInPc2	čas
-	-	kIx~	-
minulý	minulý	k2eAgInSc1d1	minulý
prostý	prostý	k2eAgInSc1d1	prostý
(	(	kIx(	(
<g/>
indefinido	indefinida	k1gFnSc5	indefinida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc1d1	minulý
složený	složený	k2eAgInSc1d1	složený
(	(	kIx(	(
<g/>
préterito	préterita	k1gFnSc5	préterita
perfecto	perfecta	k1gMnSc5	perfecta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souminulý	souminulý	k2eAgMnSc1d1	souminulý
(	(	kIx(	(
<g/>
imperfecto	imperfecto	k1gNnSc1	imperfecto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předminulý	předminulý	k2eAgInSc1d1	předminulý
(	(	kIx(	(
<g/>
pluscuamperfecto	pluscuamperfect	k2eAgNnSc1d1	pluscuamperfect
<g/>
)	)	kIx)	)
a	a	k8xC	a
minulý	minulý	k2eAgInSc1d1	minulý
průběhový	průběhový	k2eAgInSc1d1	průběhový
(	(	kIx(	(
<g/>
imperfecto	imperfect	k2eAgNnSc1d1	imperfect
continuo	continuo	k1gNnSc1	continuo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Unos	unosit	k5eAaPmRp2nS	unosit
<g/>
,	,	kIx,	,
unas	unas	k6eAd1	unas
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
neurčitého	určitý	k2eNgInSc2d1	neurčitý
členu	člen	k1gInSc2	člen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znamená	znamenat	k5eAaImIp3nS	znamenat
několik	několik	k4yIc1	několik
(	(	kIx(	(
<g/>
unos	unosit	k5eAaPmRp2nS	unosit
hombres	hombres	k1gInSc1	hombres
=	=	kIx~	=
několik	několik	k4yIc4	několik
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Španělština	španělština	k1gFnSc1	španělština
používá	používat	k5eAaImIp3nS	používat
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgNnPc1	tři
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
překládají	překládat	k5eAaImIp3nP	překládat
českým	český	k2eAgInSc7d1	český
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
ser	srát	k5eAaImRp2nS	srát
popisující	popisující	k2eAgFnSc1d1	popisující
vlastnost	vlastnost	k1gFnSc4	vlastnost
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
na	na	k7c4	na
stálo	stát	k5eAaImAgNnS	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
estar	estar	k1gInSc4	estar
popisující	popisující	k2eAgInSc4d1	popisující
současný	současný	k2eAgInSc4d1	současný
stav	stav	k1gInSc4	stav
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hay	hay	k?	hay
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nacházet	nacházet	k5eAaImF	nacházet
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
na	na	k7c6	na
neurčitém	určitý	k2eNgNnSc6d1	neurčité
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelná	pravidelný	k2eAgNnPc1d1	pravidelné
slovesa	sloveso	k1gNnPc1	sloveso
(	(	kIx(	(
<g/>
s	s	k7c7	s
koncovkami	koncovka	k1gFnPc7	koncovka
-ar	r	k?	-ar
<g/>
,	,	kIx,	,
-er	r	k?	-er
<g/>
,	,	kIx,	,
-ir	r	k?	-ir
<g/>
)	)	kIx)	)
Španělština	španělština	k1gFnSc1	španělština
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc4	jazyk
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
nářečí	nářečí	k1gNnSc2	nářečí
španělštiny	španělština	k1gFnSc2	španělština
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dosti	dosti	k6eAd1	dosti
výrazné	výrazný	k2eAgInPc4d1	výrazný
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
následující	následující	k2eAgNnSc4d1	následující
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
nářečí	nářečí	k1gNnSc4	nářečí
španělštiny	španělština	k1gFnSc2	španělština
<g/>
:	:	kIx,	:
Nářečí	nářečí	k1gNnSc4	nářečí
andaluské	andaluský	k2eAgNnSc4d1	andaluské
(	(	kIx(	(
<g/>
Dialecto	Dialecto	k1gNnSc4	Dialecto
andaluz	andaluza	k1gFnPc2	andaluza
<g/>
)	)	kIx)	)
Nářečí	nářečí	k1gNnSc2	nářečí
churro	churro	k6eAd1	churro
(	(	kIx(	(
<g/>
Dialecto	Dialect	k2eAgNnSc4d1	Dialect
churro	churro	k1gNnSc4	churro
<g/>
)	)	kIx)	)
Španělština	španělština	k1gFnSc1	španělština
z	z	k7c2	z
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc6	Españ
canario	canario	k1gNnSc4	canario
<g/>
)	)	kIx)	)
Nářečí	nářečí	k1gNnSc4	nářečí
murcijské	murcijský	k2eAgNnSc4d1	murcijský
(	(	kIx(	(
<g/>
Dialecto	Dialecto	k1gNnSc4	Dialecto
murciano	murciana	k1gFnSc5	murciana
<g/>
)	)	kIx)	)
Nářečí	nářečí	k1gNnSc4	nářečí
extremadurské	extremadurský	k2eAgFnSc2d1	extremadurský
(	(	kIx(	(
<g/>
Extremeńo	Extremeńo	k6eAd1	Extremeńo
<g/>
)	)	kIx)	)
Amazonská	amazonský	k2eAgNnPc4d1	amazonské
španělština	španělština	k1gFnSc1	španělština
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnPc7	Españ
amazónico	amazónico	k6eAd1	amazónico
<g/>
)	)	kIx)	)
Andská	andský	k2eAgFnSc1d1	andská
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
andino	andino	k1gNnSc4	andino
<g/>
)	)	kIx)	)
Španělština	španělština	k1gFnSc1	španělština
z	z	k7c2	z
centrální	centrální	k2eAgFnSc2d1	centrální
Bolívie	Bolívie	k1gFnSc2	Bolívie
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
camba	camba	k1gFnSc1	camba
<g/>
)	)	kIx)	)
Chilská	chilský	k2eAgFnSc1d1	chilská
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
chileno	chilen	k2eAgNnSc1d1	chilen
<g/>
)	)	kIx)	)
Španělština	španělština	k1gFnSc1	španělština
ze	z	k7c2	z
souostroví	souostroví	k1gNnSc2	souostroví
Chiloé	Chiloá	k1gFnSc2	Chiloá
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
(	(	kIx(	(
<g/>
Españ	Españ	k1gMnSc5	Españ
chilote	chilot	k1gMnSc5	chilot
<g/>
)	)	kIx)	)
Karibská	karibský	k2eAgFnSc1d1	karibská
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
caribeñ	caribeñ	k?	caribeñ
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
členěno	členit	k5eAaImNgNnS	členit
Středokolumbijská	Středokolumbijský	k2eAgFnSc1d1	Středokolumbijský
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
cundiboyacense	cundiboyacense	k1gFnSc1	cundiboyacense
<g/>
)	)	kIx)	)
Španělština	španělština	k1gFnSc1	španělština
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
Antioqueñ	Antioqueñ	k1gFnSc2	Antioqueñ
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
antioqueñ	antioqueñ	k?	antioqueñ
<g/>
)	)	kIx)	)
Španělština	španělština	k1gFnSc1	španělština
z	z	k7c2	z
provincií	provincie	k1gFnPc2	provincie
Santander	Santandra	k1gFnPc2	Santandra
a	a	k8xC	a
Norte	Nort	k1gInSc5	Nort
de	de	k?	de
Santander	Santander	k1gMnSc1	Santander
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
santandereano-tachirense	santandereanoachirense	k1gFnSc1	santandereano-tachirense
<g/>
)	)	kIx)	)
Laplatská	Laplatský	k2eAgFnSc1d1	Laplatská
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
rioplatense	rioplatense	k1gFnSc2	rioplatense
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Španělština	španělština	k1gFnSc1	španělština
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Los	los	k1gInSc4	los
Llanos	Llanos	k1gInPc2	Llanos
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc6	Españ
llanero	llanero	k1gNnSc4	llanero
<g/>
)	)	kIx)	)
Mexická	mexický	k2eAgFnSc1d1	mexická
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
mexicano	mexicana	k1gFnSc5	mexicana
<g/>
)	)	kIx)	)
Paraguayská	paraguayský	k2eAgFnSc1d1	paraguayská
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gMnSc1	Españ
paraguayo	paraguayo	k1gMnSc1	paraguayo
<g/>
)	)	kIx)	)
Peruánská	peruánský	k2eAgFnSc1d1	peruánská
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Castellano	Castellana	k1gFnSc5	Castellana
<g/>
)	)	kIx)	)
Španělština	španělština	k1gFnSc1	španělština
ze	z	k7c2	z
severoperuánského	severoperuánský	k2eAgNnSc2d1	severoperuánský
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
peruano	peruana	k1gFnSc5	peruana
ribereñ	ribereñ	k?	ribereñ
<g/>
)	)	kIx)	)
Rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Españ	Españ	k1gMnSc1	Españ
ecuatorial	ecuatorial	k1gMnSc1	ecuatorial
<g/>
)	)	kIx)	)
Středoamerická	středoamerický	k2eAgFnSc1d1	středoamerická
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
centroamericano	centroamericana	k1gFnSc5	centroamericana
<g/>
)	)	kIx)	)
Yucatanská	Yucatanský	k2eAgFnSc1d1	Yucatanský
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gMnSc1	Españ
yucateco	yucateco	k1gMnSc1	yucateco
<g/>
)	)	kIx)	)
Ceutská	Ceutský	k2eAgFnSc1d1	Ceutský
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
ceutí	ceuť	k1gFnPc2	ceuť
<g/>
)	)	kIx)	)
Melillská	Melillský	k2eAgFnSc1d1	Melillský
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
melillense	melillense	k1gFnSc1	melillense
<g/>
)	)	kIx)	)
Španělština	španělština	k1gFnSc1	španělština
z	z	k7c2	z
El	Ela	k1gFnPc2	Ela
Rifu	rif	k1gInSc2	rif
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
rifeñ	rifeñ	k?	rifeñ
<g/>
)	)	kIx)	)
Marocká	marocký	k2eAgFnSc1d1	marocká
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
marroquí	marroquí	k1gFnSc1	marroquí
<g/>
)	)	kIx)	)
Saharská	saharský	k2eAgFnSc1d1	saharská
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
saharauí	saharauí	k1gFnSc1	saharauí
<g/>
)	)	kIx)	)
Rovníkoguinejská	Rovníkoguinejský	k2eAgFnSc1d1	Rovníkoguinejský
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
ecuatoguineano	ecuatoguineana	k1gFnSc5	ecuatoguineana
<g/>
)	)	kIx)	)
Filipínská	filipínský	k2eAgFnSc1d1	filipínská
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
Españ	Españ	k1gFnSc1	Españ
filipino	filipin	k2eAgNnSc1d1	filipino
<g/>
)	)	kIx)	)
</s>
