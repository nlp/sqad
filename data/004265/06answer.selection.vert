<s>
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc4d1	československý
film	film	k1gInSc4	film
režiséra	režisér	k1gMnSc2	režisér
Juraje	Juraj	k1gInSc2	Juraj
Herze	Herze	k1gFnSc2	Herze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc4d1	natočený
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
novely	novela	k1gFnSc2	novela
Ladislava	Ladislav	k1gMnSc2	Ladislav
Fukse	Fuks	k1gMnSc2	Fuks
<g/>
.	.	kIx.	.
</s>
