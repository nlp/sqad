<p>
<s>
Koh-i-Noor	Koh-Noor	k1gInSc1	Koh-i-Noor
<g/>
,	,	kIx,	,
též	též	k9	též
Koh-i-noor	Kohoor	k1gInSc1	Koh-i-noor
<g/>
,	,	kIx,	,
Kohinoor	Kohinoor	k1gInSc1	Kohinoor
<g/>
,	,	kIx,	,
Koh-e	Koh	k1gNnSc1	Koh-e
Noor	Noora	k1gFnPc2	Noora
nebo	nebo	k8xC	nebo
Koh-i-Nur	Koh-Nura	k1gFnPc2	Koh-i-Nura
(	(	kIx(	(
<g/>
v	v	k7c6	v
perštině	perština	k1gFnSc6	perština
ک	ک	k?	ک
ن	ن	k?	ن
[	[	kIx(	[
<g/>
Kúh-e	Kúh	k1gInSc1	Kúh-e
núr	núr	k?	núr
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
telugštině	telugština	k1gFnSc6	telugština
క	క	k?	క
<g/>
ో	ో	k?	ో
<g/>
హ	హ	k?	హ
<g/>
ి	ి	k?	ి
<g/>
న	న	k?	న
<g/>
ూ	ూ	k?	ూ
<g/>
ర	ర	k?	ర
<g/>
ు	ు	k?	ు
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
je	být	k5eAaImIp3nS	být
Hora	hora	k1gFnSc1	hora
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
diamant	diamant	k1gInSc4	diamant
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
105	[number]	k4	105
karátů	karát	k1gInPc2	karát
(	(	kIx(	(
<g/>
21,6	[number]	k4	21,6
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Britských	britský	k2eAgInPc2d1	britský
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
–	–	k?	–
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jejího	její	k3xOp3gNnSc2	její
konání	konání	k1gNnSc2	konání
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
známé	známý	k2eAgInPc4d1	známý
drahokamy	drahokam	k1gInPc4	drahokam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Údajně	údajně	k6eAd1	údajně
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
před	před	k7c7	před
čtyřmi	čtyři	k4xCgNnPc7	čtyři
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
náplavě	náplava	k1gFnSc6	náplava
řeky	řeka	k1gFnSc2	řeka
Godávarí	Godávarí	k1gFnSc2	Godávarí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
indických	indický	k2eAgMnPc2d1	indický
šáhů	šáh	k1gMnPc2	šáh
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Peršanů	peršan	k1gInPc2	peršan
a	a	k8xC	a
potom	potom	k6eAd1	potom
vládců	vládce	k1gMnPc2	vládce
Paňdžábu	Paňdžáb	k1gInSc2	Paňdžáb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Angličané	Angličan	k1gMnPc1	Angličan
Paňdžáb	Paňdžáb	k1gInSc4	Paňdžáb
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
diamant	diamant	k1gInSc1	diamant
uložen	uložit	k5eAaPmNgInS	uložit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kámen	kámen	k1gInSc4	kámen
získala	získat	k5eAaPmAgFnS	získat
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
Viktorie	Viktorie	k1gFnSc1	Viktorie
a	a	k8xC	a
Koh-i-noor	Kohoor	k1gInSc1	Koh-i-noor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
britských	britský	k2eAgInPc2d1	britský
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
–	–	k?	–
byl	být	k5eAaImAgMnS	být
zbroušen	zbrousit	k5eAaPmNgMnS	zbrousit
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
186	[number]	k4	186
karátů	karát	k1gInPc2	karát
na	na	k7c4	na
105	[number]	k4	105
karátů	karát	k1gInPc2	karát
do	do	k7c2	do
kapky	kapka	k1gFnSc2	kapka
podle	podle	k7c2	podle
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Koh-i-Noor	Koh-Noor	k1gInSc1	Koh-i-Noor
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejkrásnější	krásný	k2eAgInPc4d3	nejkrásnější
diamanty	diamant	k1gInPc4	diamant
světa	svět	k1gInSc2	svět
nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
perfektnímu	perfektní	k2eAgInSc3d1	perfektní
stylu	styl	k1gInSc3	styl
vybroušení	vybroušení	k1gNnSc2	vybroušení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc1	odkaz
ve	v	k7c6	v
filmech	film	k1gInPc6	film
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
diamant	diamant	k1gInSc1	diamant
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
díle	dílo	k1gNnSc6	dílo
druhé	druhý	k4xOgFnSc2	druhý
série	série	k1gFnSc2	série
anglického	anglický	k2eAgInSc2d1	anglický
seriálu	seriál	k1gInSc2	seriál
Doctor	Doctor	k1gMnSc1	Doctor
Who	Who	k1gMnSc1	Who
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
Viktorie	Viktorie	k1gFnSc1	Viktorie
ho	on	k3xPp3gInSc4	on
použila	použít	k5eAaPmAgFnS	použít
jako	jako	k8xC	jako
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
vlkodlakem	vlkodlak	k1gMnSc7	vlkodlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
nejvýdělečnějším	výdělečný	k2eAgInSc6d3	nejvýdělečnější
filmu	film	k1gInSc6	film
indické	indický	k2eAgFnSc2d1	indická
historie	historie	k1gFnSc2	historie
Bang	Bang	k1gMnSc1	Bang
Bang	Bang	k1gMnSc1	Bang
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
ukraden	ukraden	k2eAgInSc4d1	ukraden
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
film	film	k1gInSc4	film
se	se	k3xPyFc4	se
právě	právě	k9	právě
kolem	kolem	k7c2	kolem
diamantu	diamant	k1gInSc2	diamant
točí	točit	k5eAaImIp3nS	točit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
remakem	remak	k1gInSc7	remak
amerického	americký	k2eAgInSc2d1	americký
filmu	film	k1gInSc2	film
Zatím	zatím	k6eAd1	zatím
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
živí	živit	k5eAaImIp3nS	živit
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
zahrály	zahrát	k5eAaPmAgFnP	zahrát
indické	indický	k2eAgFnPc1d1	indická
filmové	filmový	k2eAgFnPc1d1	filmová
hvězdy	hvězda	k1gFnPc1	hvězda
Katrina	Katrina	k1gMnSc1	Katrina
Kaif	Kaif	k1gMnSc1	Kaif
a	a	k8xC	a
Hrithik	Hrithik	k1gMnSc1	Hrithik
Roshan	Roshan	k1gMnSc1	Roshan
a	a	k8xC	a
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
seriálu	seriál	k1gInSc6	seriál
The	The	k1gFnSc1	The
Royals	Royals	k1gInSc1	Royals
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Koh	Koh	k?	Koh
<g/>
–	–	k?	–
<g/>
i	i	k8xC	i
<g/>
–	–	k?	–
<g/>
noor	noor	k1gInSc1	noor
–	–	k?	–
světově	světově	k6eAd1	světově
nejznámější	známý	k2eAgInSc1d3	nejznámější
diamant	diamant	k1gInSc1	diamant
|	|	kIx~	|
Jiří	Jiří	k1gMnSc1	Jiří
Glet	Glet	k1gMnSc1	Glet
</s>
</p>
