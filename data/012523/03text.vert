<p>
<s>
Kozí	kozí	k2eAgNnSc1d1	kozí
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
kapalina	kapalina	k1gFnSc1	kapalina
produkovaná	produkovaný	k2eAgFnSc1d1	produkovaná
mléčnými	mléčný	k2eAgFnPc7d1	mléčná
žlázami	žláza	k1gFnPc7	žláza
koz	koza	k1gFnPc2	koza
(	(	kIx(	(
<g/>
ve	v	k7c6	v
středoevropských	středoevropský	k2eAgFnPc6d1	středoevropská
podmínkách	podmínka	k1gFnPc6	podmínka
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kozu	koza	k1gFnSc4	koza
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
u	u	k7c2	u
zdomácnělých	zdomácnělý	k2eAgNnPc2d1	zdomácnělé
zvířat	zvíře	k1gNnPc2	zvíře
však	však	k9	však
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
produkce	produkce	k1gFnSc1	produkce
šlechtěním	šlechtění	k1gNnSc7	šlechtění
navýšena	navýšit	k5eAaPmNgFnS	navýšit
a	a	k8xC	a
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
člověk	člověk	k1gMnSc1	člověk
mohl	moct	k5eAaImAgMnS	moct
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kozí	kozí	k2eAgNnSc1d1	kozí
mléko	mléko	k1gNnSc1	mléko
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
nemůže	moct	k5eNaImIp3nS	moct
konkurovat	konkurovat	k5eAaImF	konkurovat
kravskému	kravský	k2eAgInSc3d1	kravský
a	a	k8xC	a
produkuje	produkovat	k5eAaImIp3nS	produkovat
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
malochovu	malochov	k1gInSc2	malochov
pro	pro	k7c4	pro
domácí	domácí	k2eAgFnSc4d1	domácí
spotřebu	spotřeba	k1gFnSc4	spotřeba
či	či	k8xC	či
přímý	přímý	k2eAgInSc4d1	přímý
prodej	prodej	k1gInSc4	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
důvody	důvod	k1gInPc4	důvod
patří	patřit	k5eAaImIp3nS	patřit
vyšší	vysoký	k2eAgFnSc4d2	vyšší
produkční	produkční	k2eAgFnSc4d1	produkční
schopnost	schopnost	k1gFnSc4	schopnost
krav	kráva	k1gFnPc2	kráva
a	a	k8xC	a
fakt	faktum	k1gNnPc2	faktum
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvalita	kvalita	k1gFnSc1	kvalita
kozího	kozí	k2eAgNnSc2d1	kozí
mléka	mléko	k1gNnSc2	mléko
více	hodně	k6eAd2	hodně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
kvalitě	kvalita	k1gFnSc6	kvalita
krmiva	krmivo	k1gNnSc2	krmivo
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
u	u	k7c2	u
kravského	kravský	k2eAgNnSc2d1	kravské
<g/>
.	.	kIx.	.
</s>
<s>
Koza	koza	k1gFnSc1	koza
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
větší	veliký	k2eAgFnSc4d2	veliký
tendenci	tendence	k1gFnSc4	tendence
převádět	převádět	k5eAaImF	převádět
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
mléka	mléko	k1gNnSc2	mléko
jedy	jed	k1gInPc1	jed
a	a	k8xC	a
choroboplodné	choroboplodný	k2eAgInPc1d1	choroboplodný
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
dostanou	dostat	k5eAaPmIp3nP	dostat
(	(	kIx(	(
<g/>
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenán	k2eAgFnPc1d1	zaznamenána
otravy	otrava	k1gFnPc1	otrava
z	z	k7c2	z
mléka	mléko	k1gNnSc2	mléko
kozy	koza	k1gFnSc2	koza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sežrala	sežrat	k5eAaPmAgFnS	sežrat
rulík	rulík	k1gInSc4	rulík
zlomocný	zlomocný	k2eAgInSc4d1	zlomocný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
epidemie	epidemie	k1gFnPc1	epidemie
klíšťové	klíšťový	k2eAgFnPc4d1	klíšťová
encefalitidy	encefalitida	k1gFnPc4	encefalitida
způsobené	způsobený	k2eAgFnPc1d1	způsobená
kozami	koza	k1gFnPc7	koza
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
poštípala	poštípat	k5eAaPmAgNnP	poštípat
infikovaná	infikovaný	k2eAgNnPc1d1	infikované
klíšťata	klíště	k1gNnPc1	klíště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výše	vysoce	k6eAd2	vysoce
napsané	napsaný	k2eAgNnSc1d1	napsané
ovšem	ovšem	k9	ovšem
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
kozí	kozí	k2eAgNnSc1d1	kozí
mléko	mléko	k1gNnSc1	mléko
bylo	být	k5eAaImAgNnS	být
nekvalitní	kvalitní	k2eNgNnSc1d1	nekvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
kozí	kozí	k2eAgNnSc1d1	kozí
mléko	mléko	k1gNnSc1	mléko
lze	lze	k6eAd1	lze
vyhodnotit	vyhodnotit	k5eAaPmF	vyhodnotit
jako	jako	k9	jako
kvalitnější	kvalitní	k2eAgFnSc4d2	kvalitnější
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
stravitelné	stravitelný	k2eAgFnPc1d1	stravitelná
a	a	k8xC	a
zdravotně	zdravotně	k6eAd1	zdravotně
prospěšnější	prospěšný	k2eAgFnPc1d2	prospěšnější
než	než	k8xS	než
kravské	kravský	k2eAgFnPc1d1	kravská
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dbáno	dbán	k2eAgNnSc1d1	dbáno
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
kvalitu	kvalita	k1gFnSc4	kvalita
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kozí	kozí	k2eAgNnSc1d1	kozí
mléko	mléko	k1gNnSc1	mléko
se	se	k3xPyFc4	se
též	též	k9	též
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
léčitelství	léčitelství	k1gNnSc6	léčitelství
jako	jako	k8xS	jako
lék	lék	k1gInSc1	lék
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
dětských	dětský	k2eAgInPc2d1	dětský
ekzémů	ekzém	k1gInPc2	ekzém
a	a	k8xC	a
vyrážek	vyrážka	k1gFnPc2	vyrážka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
koza	koza	k1gFnSc1	koza
domácí	domácí	k2eAgFnSc2d1	domácí
</s>
</p>
<p>
<s>
mléko	mléko	k1gNnSc1	mléko
</s>
</p>
