<p>
<s>
Pompeje	Pompeje	k1gFnPc1	Pompeje
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Pompei	Pompei	k1gNnSc1	Pompei
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Pompeii	Pompeie	k1gFnSc3	Pompeie
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
starověké	starověký	k2eAgNnSc4d1	starověké
město	město	k1gNnSc4	město
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
metropolitního	metropolitní	k2eAgNnSc2d1	metropolitní
města	město	k1gNnSc2	město
Napoli	Napole	k1gFnSc4	Napole
<g/>
.	.	kIx.	.
</s>
<s>
Staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
starořímských	starořímský	k2eAgNnPc2d1	starořímské
měst	město	k1gNnPc2	město
v	v	k7c6	v
Neapolském	neapolský	k2eAgInSc6d1	neapolský
zálivu	záliv	k1gInSc6	záliv
v	v	k7c6	v
císařské	císařský	k2eAgFnSc6d1	císařská
provincii	provincie	k1gFnSc6	provincie
Italia	Italium	k1gNnSc2	Italium
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
region	region	k1gInSc1	region
Kampánie	Kampánie	k1gFnSc2	Kampánie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
79	[number]	k4	79
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
zničena	zničit	k5eAaPmNgFnS	zničit
výbuchem	výbuch	k1gInSc7	výbuch
sopky	sopka	k1gFnSc2	sopka
Vesuv	Vesuv	k1gInSc1	Vesuv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byly	být	k5eAaImAgInP	být
Pompeje	Pompeje	k1gInPc1	Pompeje
zapsány	zapsat	k5eAaPmNgInP	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
trosek	troska	k1gFnPc2	troska
starověkých	starověký	k2eAgFnPc2d1	starověká
Pompejí	Pompeje	k1gFnPc2	Pompeje
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
současné	současný	k2eAgNnSc1d1	současné
město	město	k1gNnSc1	město
Pompei	Pompe	k1gFnSc2	Pompe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Pompejemi	Pompeje	k1gFnPc7	Pompeje
byla	být	k5eAaImAgFnS	být
zničena	zničen	k2eAgFnSc1d1	zničena
města	město	k1gNnSc2	město
Herculaneum	Herculaneum	k1gNnSc4	Herculaneum
<g/>
,	,	kIx,	,
Stabie	Stabie	k1gFnPc4	Stabie
<g/>
,	,	kIx,	,
Oplontis	Oplontis	k1gFnSc6	Oplontis
a	a	k8xC	a
Boscoreale	Boscoreala	k1gFnSc6	Boscoreala
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
znovuobjevení	znovuobjevení	k1gNnSc1	znovuobjevení
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
Evropě	Evropa	k1gFnSc6	Evropa
kulturní	kulturní	k2eAgInSc4d1	kulturní
šok	šok	k1gInSc4	šok
<g/>
.	.	kIx.	.
</s>
<s>
Všudypřítomnost	všudypřítomnost	k1gFnSc1	všudypřítomnost
erotického	erotický	k2eAgNnSc2d1	erotické
umění	umění	k1gNnSc2	umění
doslova	doslova	k6eAd1	doslova
převrátila	převrátit	k5eAaPmAgFnS	převrátit
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
antickou	antický	k2eAgFnSc4d1	antická
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Vulkanický	vulkanický	k2eAgInSc1d1	vulkanický
popel	popel	k1gInSc1	popel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
města	město	k1gNnSc2	město
zasypal	zasypat	k5eAaPmAgMnS	zasypat
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
zakonzervoval	zakonzervovat	k5eAaPmAgMnS	zakonzervovat
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
předměty	předmět	k1gInPc4	předmět
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
nám	my	k3xPp1nPc3	my
tak	tak	k6eAd1	tak
poznat	poznat	k5eAaPmF	poznat
vzhled	vzhled	k1gInSc4	vzhled
starořímského	starořímský	k2eAgNnSc2d1	starořímské
města	město	k1gNnSc2	město
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
život	život	k1gInSc4	život
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
všudypřítomná	všudypřítomný	k2eAgFnSc1d1	všudypřítomná
erotika	erotika	k1gFnSc1	erotika
vedla	vést	k5eAaImAgFnS	vést
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
historiky	historik	k1gMnPc4	historik
k	k	k7c3	k
naivním	naivní	k2eAgInPc3d1	naivní
pokusům	pokus	k1gInPc3	pokus
prohlásit	prohlásit	k5eAaPmF	prohlásit
obě	dva	k4xCgNnPc1	dva
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
erotika	erotika	k1gFnSc1	erotika
nacházela	nacházet	k5eAaImAgFnS	nacházet
(	(	kIx(	(
<g/>
což	což	k9	což
byly	být	k5eAaImAgInP	být
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
<g/>
)	)	kIx)	)
za	za	k7c4	za
nevěstince	nevěstinec	k1gInPc4	nevěstinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
se	se	k3xPyFc4	se
také	také	k9	také
objevily	objevit	k5eAaPmAgFnP	objevit
zkazky	zkazka	k1gFnPc1	zkazka
o	o	k7c6	o
"	"	kIx"	"
<g/>
Božím	boží	k2eAgInSc6d1	boží
trestu	trest	k1gInSc6	trest
<g/>
"	"	kIx"	"
seslaném	seslaný	k2eAgInSc6d1	seslaný
na	na	k7c4	na
Pompeje	Pompeje	k1gInPc4	Pompeje
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
soudobého	soudobý	k2eAgNnSc2d1	soudobé
poznání	poznání	k1gNnSc2	poznání
se	se	k3xPyFc4	se
však	však	k9	však
Pompeje	Pompeje	k1gFnPc1	Pompeje
nijak	nijak	k6eAd1	nijak
neodlišovaly	odlišovat	k5eNaImAgFnP	odlišovat
od	od	k7c2	od
jiných	jiný	k2eAgNnPc2d1	jiné
antických	antický	k2eAgNnPc2d1	antické
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
a	a	k8xC	a
raná	raný	k2eAgFnSc1d1	raná
historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c4	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
kmenem	kmen	k1gInSc7	kmen
Osků	Osk	k1gMnPc2	Osk
na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
staré	starý	k2eAgFnSc2d1	stará
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
lávy	láva	k1gFnSc2	láva
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Sarno	Sarno	k1gNnSc4	Sarno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
užíváno	užívat	k5eAaImNgNnS	užívat
jako	jako	k8xS	jako
přístav	přístav	k1gInSc1	přístav
řeckými	řecký	k2eAgMnPc7d1	řecký
a	a	k8xC	a
fénickými	fénický	k2eAgMnPc7d1	fénický
mořeplavci	mořeplavec	k1gMnPc7	mořeplavec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
řeckým	řecký	k2eAgInSc7d1	řecký
<g/>
,	,	kIx,	,
řečtí	řecký	k2eAgMnPc1d1	řecký
obchodníci	obchodník	k1gMnPc1	obchodník
využívali	využívat	k5eAaPmAgMnP	využívat
jeho	jeho	k3xOp3gFnPc4	jeho
strategické	strategický	k2eAgFnPc4d1	strategická
polohy	poloha	k1gFnPc4	poloha
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
mezi	mezi	k7c7	mezi
řeckými	řecký	k2eAgFnPc7d1	řecká
osadami	osada	k1gFnPc7	osada
v	v	k7c6	v
Paestu	Paest	k1gInSc6	Paest
a	a	k8xC	a
Cumae	Cumae	k1gFnSc6	Cumae
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
postupně	postupně	k6eAd1	postupně
asimilovaly	asimilovat	k5eAaBmAgInP	asimilovat
a	a	k8xC	a
nahradily	nahradit	k5eAaPmAgInP	nahradit
původní	původní	k2eAgFnSc1d1	původní
oská	oská	k1gFnSc1	oská
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
oského	oského	k2eAgNnSc2d1	oského
slova	slovo	k1gNnSc2	slovo
pope	pop	k1gInSc5	pop
-	-	kIx~	-
pět	pět	k4xCc4	pět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
nacházelo	nacházet	k5eAaImAgNnS	nacházet
pět	pět	k4xCc1	pět
odlišných	odlišný	k2eAgFnPc2d1	odlišná
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
525	[number]	k4	525
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
474	[number]	k4	474
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vlivem	vliv	k1gInSc7	vliv
etruským	etruský	k2eAgInSc7d1	etruský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
474	[number]	k4	474
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
etruské	etruský	k2eAgNnSc1d1	etruské
vojsko	vojsko	k1gNnSc1	vojsko
poraženo	poražen	k2eAgNnSc1d1	poraženo
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Syrakus	Syrakusy	k1gFnPc2	Syrakusy
doba	doba	k1gFnSc1	doba
koexistence	koexistence	k1gFnSc1	koexistence
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
Etrusků	Etrusk	k1gMnPc2	Etrusk
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Pompejí	Pompeje	k1gFnPc2	Pompeje
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
města	město	k1gNnSc2	město
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
silná	silný	k2eAgFnSc1d1	silná
hradba	hradba	k1gFnSc1	hradba
z	z	k7c2	z
lávových	lávový	k2eAgInPc2d1	lávový
bloků	blok	k1gInPc2	blok
obložených	obložený	k2eAgInPc2d1	obložený
zvnějšku	zvnějšku	k6eAd1	zvnějšku
kvádry	kvádr	k1gInPc4	kvádr
sarnského	sarnský	k2eAgInSc2d1	sarnský
vápence	vápenec	k1gInSc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Kampánie	Kampánie	k1gFnSc2	Kampánie
dobyto	dobyt	k2eAgNnSc4d1	dobyto
Samnity	Samnita	k1gFnPc4	Samnita
<g/>
,	,	kIx,	,
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nP	pocházet
samnitská	samnitský	k2eAgNnPc1d1	samnitský
pohřebiště	pohřebiště	k1gNnPc1	pohřebiště
nalezená	nalezený	k2eAgNnPc1d1	nalezené
u	u	k7c2	u
Herkulánské	Herkulánský	k2eAgFnSc2d1	Herkulánský
a	a	k8xC	a
Stabijské	Stabijský	k2eAgFnSc2d1	Stabijský
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Římané	Říman	k1gMnPc1	Říman
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
310	[number]	k4	310
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
pod	pod	k7c7	pod
římským	římský	k2eAgInSc7d1	římský
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
kampánských	kampánský	k2eAgNnPc2d1	kampánský
měst	město	k1gNnPc2	město
Pompeje	Pompeje	k1gFnPc1	Pompeje
totiž	totiž	k9	totiž
respektovaly	respektovat	k5eAaImAgFnP	respektovat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Římem	Řím	k1gInSc7	Řím
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
postavily	postavit	k5eAaPmAgInP	postavit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
épeirskému	épeirský	k2eAgMnSc3d1	épeirský
králi	král	k1gMnSc3	král
Pyrrhovi	Pyrrhos	k1gMnSc3	Pyrrhos
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
216	[number]	k4	216
i	i	k9	i
Hannibalovi	Hannibalův	k2eAgMnPc1d1	Hannibalův
<g/>
.	.	kIx.	.
</s>
<s>
Pompeje	Pompeje	k1gInPc1	Pompeje
se	se	k3xPyFc4	se
zapojily	zapojit	k5eAaPmAgInP	zapojit
do	do	k7c2	do
vzpoury	vzpoura	k1gFnSc2	vzpoura
měst	město	k1gNnPc2	město
Kampánie	Kampánie	k1gFnSc2	Kampánie
proti	proti	k7c3	proti
Římu	Řím	k1gInSc3	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
89	[number]	k4	89
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Sullovo	Sullův	k2eAgNnSc1d1	Sullovo
vojsko	vojsko	k1gNnSc1	vojsko
vzpouru	vzpoura	k1gFnSc4	vzpoura
potlačilo	potlačit	k5eAaPmAgNnS	potlačit
(	(	kIx(	(
<g/>
stopy	stopa	k1gFnPc4	stopa
po	po	k7c6	po
ostřelování	ostřelování	k1gNnSc6	ostřelování
hradeb	hradba	k1gFnPc2	hradba
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
patrné	patrný	k2eAgInPc1d1	patrný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
zbaveno	zbavit	k5eAaPmNgNnS	zbavit
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
kolonie	kolonie	k1gFnSc1	kolonie
Colonia	Colonium	k1gNnSc2	Colonium
Cornelia	Cornelium	k1gNnSc2	Cornelium
Veneria	Venerium	k1gNnSc2	Venerium
Pompeianorum	Pompeianorum	k1gNnSc4	Pompeianorum
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
byla	být	k5eAaImAgFnS	být
oslavena	oslavit	k5eAaPmNgFnS	oslavit
stavbou	stavba	k1gFnSc7	stavba
Venušina	Venušin	k2eAgInSc2d1	Venušin
chrámu	chrám	k1gInSc2	chrám
na	na	k7c4	na
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
střežil	střežit	k5eAaImAgMnS	střežit
cestu	cesta	k1gFnSc4	cesta
vedoucí	vedoucí	k1gMnSc1	vedoucí
do	do	k7c2	do
města	město	k1gNnSc2	město
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Sarno	Saren	k2eAgNnSc1d1	Saren
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
tzv.	tzv.	kA	tzv.
druhý	druhý	k4xOgInSc4	druhý
styl	styl	k1gInSc4	styl
nástěnného	nástěnný	k2eAgNnSc2d1	nástěnné
malířství	malířství	k1gNnSc2	malířství
<g/>
,	,	kIx,	,
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
Faunův	Faunův	k2eAgInSc4d1	Faunův
dům	dům	k1gInSc4	dům
si	se	k3xPyFc3	se
podržel	podržet	k5eAaPmAgMnS	podržet
dekorace	dekorace	k1gFnPc4	dekorace
prvního	první	k4xOgInSc2	první
stylu	styl	k1gInSc2	styl
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Samnité	Samnitý	k2eAgFnPc1d1	Samnitý
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
i	i	k9	i
po	po	k7c4	po
vítězství	vítězství	k1gNnSc4	vítězství
římanů	říman	k1gMnPc2	říman
udrželi	udržet	k5eAaPmAgMnP	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nadále	nadále	k6eAd1	nadále
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
důležitým	důležitý	k2eAgNnSc7d1	důležité
obchodním	obchodní	k2eAgNnSc7d1	obchodní
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
zboží	zboží	k1gNnSc4	zboží
dopravené	dopravený	k2eAgNnSc4d1	dopravené
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
posíláno	posílán	k2eAgNnSc1d1	posíláno
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
nebo	nebo	k8xC	nebo
na	na	k7c4	na
jih	jih	k1gInSc4	jih
po	po	k7c4	po
nedaleké	daleký	k2eNgNnSc4d1	nedaleké
Via	via	k7c4	via
Appia	Appius	k1gMnSc4	Appius
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
obdobím	období	k1gNnPc3	období
největšího	veliký	k2eAgInSc2d3	veliký
rozvoje	rozvoj	k1gInSc2	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vkus	vkus	k1gInSc1	vkus
Říma	Řím	k1gInSc2	Řím
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
radikální	radikální	k2eAgFnPc1d1	radikální
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
uspořádání	uspořádání	k1gNnSc6	uspořádání
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
Trojúhelníkové	trojúhelníkový	k2eAgNnSc1d1	trojúhelníkové
fórum	fórum	k1gNnSc1	fórum
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součastí	součast	k1gFnSc7	součast
divadelního	divadelní	k2eAgInSc2d1	divadelní
komplexu	komplex	k1gInSc2	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
malé	malé	k1gNnSc1	malé
<g/>
,	,	kIx,	,
kryté	krytý	k2eAgNnSc1d1	kryté
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
ódeion	ódeion	k1gNnSc1	ódeion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hrály	hrát	k5eAaImAgFnP	hrát
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
divadle	divadlo	k1gNnSc6	divadlo
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Osků	Osk	k1gInPc2	Osk
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
lázně	lázeň	k1gFnPc1	lázeň
(	(	kIx(	(
<g/>
Lázne	Lázn	k1gInSc5	Lázn
u	u	k7c2	u
Fóra	fórum	k1gNnSc2	fórum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnPc1d2	starší
Stabijské	Stabijský	k2eAgFnPc1d1	Stabijský
lázně	lázeň	k1gFnPc1	lázeň
byly	být	k5eAaImAgFnP	být
přestavěny	přestavět	k5eAaPmNgFnP	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
fóra	fórum	k1gNnSc2	fórum
byl	být	k5eAaImAgInS	být
přestavěn	přestavět	k5eAaPmNgInS	přestavět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
kultu	kult	k1gInSc3	kult
kapitolské	kapitolský	k2eAgFnSc2d1	Kapitolská
trojice	trojice	k1gFnSc2	trojice
Jupiter	Jupiter	k1gMnSc1	Jupiter
-	-	kIx~	-
Juno	Juno	k1gFnSc1	Juno
-	-	kIx~	-
Minerva	Minerva	k1gFnSc1	Minerva
<g/>
.	.	kIx.	.
</s>
<s>
Capitolinum	Capitolinum	k1gNnSc1	Capitolinum
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
římských	římský	k2eAgNnPc6d1	římské
městech	město	k1gNnPc6	město
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
transpozici	transpozice	k1gFnSc4	transpozice
hlavních	hlavní	k2eAgNnPc2d1	hlavní
státních	státní	k2eAgNnPc2d1	státní
božstev	božstvo	k1gNnPc2	božstvo
uctívaných	uctívaný	k2eAgNnPc2d1	uctívané
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
na	na	k7c6	na
římském	římský	k2eAgInSc6d1	římský
Kapitolu	Kapitol	k1gInSc6	Kapitol
<g/>
.	.	kIx.	.
<g/>
Největší	veliký	k2eAgFnSc7d3	veliký
stavbou	stavba	k1gFnSc7	stavba
římské	římský	k2eAgFnSc2d1	římská
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
však	však	k9	však
stavba	stavba	k1gFnSc1	stavba
amfiteatru	amfiteatr	k1gInSc2	amfiteatr
<g/>
,	,	kIx,	,
datovaná	datovaný	k2eAgFnSc1d1	datovaná
do	do	k7c2	do
roku	rok	k1gInSc2	rok
70	[number]	k4	70
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Staré	Staré	k2eAgNnSc2d1	Staré
tržiště	tržiště	k1gNnSc2	tržiště
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Fórem	fórum	k1gNnSc7	fórum
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydlážděné	vydlážděný	k2eAgNnSc1d1	vydlážděné
bílým	bílý	k2eAgInSc7d1	bílý
vápencem	vápenec	k1gInSc7	vápenec
a	a	k8xC	a
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
pro	pro	k7c4	pro
vozovou	vozový	k2eAgFnSc4d1	vozová
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Pompejí	Pompeje	k1gFnPc2	Pompeje
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
nový	nový	k2eAgInSc1d1	nový
vodovod	vodovod	k1gInSc1	vodovod
a	a	k8xC	a
v	v	k7c4	v
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
u	u	k7c2	u
Vesuvské	vesuvský	k2eAgFnSc2d1	vesuvský
brány	brána	k1gFnSc2	brána
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vedly	vést	k5eAaImAgFnP	vést
olověné	olověný	k2eAgFnPc4d1	olověná
trubky	trubka	k1gFnPc4	trubka
k	k	k7c3	k
41	[number]	k4	41
veřejným	veřejný	k2eAgFnPc3d1	veřejná
kašnám	kašna	k1gFnPc3	kašna
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velké	velký	k2eAgNnSc1d1	velké
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
roku	rok	k1gInSc2	rok
62	[number]	k4	62
n.	n.	k?	n.
l.	l.	k?	l.
===	===	k?	===
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
62	[number]	k4	62
<g/>
,	,	kIx,	,
zničilo	zničit	k5eAaPmAgNnS	zničit
prudké	prudký	k2eAgNnSc1d1	prudké
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
znovu	znovu	k6eAd1	znovu
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ještě	ještě	k9	ještě
zdobněji	zdobně	k6eAd2	zdobně
a	a	k8xC	a
bohatěji	bohatě	k6eAd2	bohatě
než	než	k8xS	než
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
veřejné	veřejný	k2eAgFnPc1d1	veřejná
i	i	k8xC	i
soukromé	soukromý	k2eAgFnPc1d1	soukromá
stavby	stavba	k1gFnPc1	stavba
však	však	k9	však
byly	být	k5eAaImAgFnP	být
opraveny	opravit	k5eAaPmNgInP	opravit
jen	jen	k9	jen
zčásti	zčásti	k6eAd1	zčásti
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nebyly	být	k5eNaImAgFnP	být
opraveny	opravit	k5eAaPmNgFnP	opravit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
těžce	těžce	k6eAd1	těžce
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
Fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
Capitolinum	Capitolinum	k1gNnSc1	Capitolinum
a	a	k8xC	a
Basilica	Basilica	k1gFnSc1	Basilica
do	do	k7c2	do
zničení	zničení	k1gNnSc2	zničení
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
79	[number]	k4	79
n.	n.	k?	n.
l.	l.	k?	l.
nedočkaly	dočkat	k5eNaPmAgFnP	dočkat
opravy	oprava	k1gFnPc4	oprava
<g/>
,	,	kIx,	,
Capitolinum	Capitolinum	k1gNnSc4	Capitolinum
dokonce	dokonce	k9	dokonce
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k8xC	jako
kamenická	kamenický	k2eAgFnSc1d1	kamenická
dílna	dílna	k1gFnSc1	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
sociální	sociální	k2eAgInPc4d1	sociální
nepokoje	nepokoj	k1gInPc4	nepokoj
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
předtím	předtím	k6eAd1	předtím
nedotčené	dotčený	k2eNgInPc4d1	nedotčený
mocenskými	mocenský	k2eAgInPc7d1	mocenský
boji	boj	k1gInPc7	boj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
předcházely	předcházet	k5eAaImAgInP	předcházet
Augustově	Augustův	k2eAgFnSc3d1	Augustova
vládě	vláda	k1gFnSc3	vláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vespasian	Vespasian	k1gMnSc1	Vespasian
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
města	město	k1gNnSc2	město
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
Suedia	Suedium	k1gNnPc1	Suedium
Clementa	Clementa	k1gFnSc1	Clementa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obnovil	obnovit	k5eAaPmAgInS	obnovit
pořádek	pořádek	k1gInSc1	pořádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vesuv	Vesuv	k1gInSc4	Vesuv
pohřbívá	pohřbívat	k5eAaImIp3nS	pohřbívat
město	město	k1gNnSc1	město
===	===	k?	===
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Pompejí	Pompeje	k1gFnPc2	Pompeje
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vesuv	Vesuv	k1gInSc1	Vesuv
je	být	k5eAaImIp3nS	být
vyhaslý	vyhaslý	k2eAgInSc1d1	vyhaslý
<g/>
,	,	kIx,	,
a	a	k8xC	a
varovným	varovný	k2eAgInPc3d1	varovný
signálům	signál	k1gInPc3	signál
nevěnovali	věnovat	k5eNaImAgMnP	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Nespojovali	spojovat	k5eNaImAgMnP	spojovat
je	on	k3xPp3gInPc4	on
s	s	k7c7	s
aktivitou	aktivita	k1gFnSc7	aktivita
Vesuvu	Vesuv	k1gInSc2	Vesuv
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
na	na	k7c6	na
drobné	drobná	k1gFnSc6	drobná
otřesy	otřes	k1gInPc4	otřes
země	zem	k1gFnSc2	zem
a	a	k8xC	a
výrony	výron	k1gInPc4	výron
plynů	plyn	k1gInPc2	plyn
z	z	k7c2	z
Vesuvu	Vesuv	k1gInSc2	Vesuv
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
62	[number]	k4	62
byly	být	k5eAaImAgInP	být
otřesy	otřes	k1gInPc1	otřes
natolik	natolik	k6eAd1	natolik
silné	silný	k2eAgInPc1d1	silný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
způsobily	způsobit	k5eAaPmAgInP	způsobit
poškození	poškození	k1gNnSc4	poškození
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
79	[number]	k4	79
vyschly	vyschnout	k5eAaPmAgFnP	vyschnout
všechny	všechen	k3xTgFnPc1	všechen
studny	studna	k1gFnPc1	studna
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
varování	varování	k1gNnSc1	varování
stále	stále	k6eAd1	stále
nebylo	být	k5eNaImAgNnS	být
dostatečně	dostatečně	k6eAd1	dostatečně
silné	silný	k2eAgNnSc1d1	silné
<g/>
;	;	kIx,	;
celý	celý	k2eAgInSc1d1	celý
starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
pak	pak	k6eAd1	pak
strnul	strnout	k5eAaPmAgInS	strnout
hrůzou	hrůza	k1gFnSc7	hrůza
<g/>
,	,	kIx,	,
když	když	k8xS	když
zřejmě	zřejmě	k6eAd1	zřejmě
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
katastrofální	katastrofální	k2eAgFnSc3d1	katastrofální
vulkanické	vulkanický	k2eAgFnSc3d1	vulkanická
erupci	erupce	k1gFnSc3	erupce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pohřbila	pohřbít	k5eAaPmAgFnS	pohřbít
město	město	k1gNnSc4	město
a	a	k8xC	a
zaclonila	zaclonit	k5eAaPmAgFnS	zaclonit
slunce	slunce	k1gNnSc4	slunce
v	v	k7c4	v
pravé	pravý	k2eAgNnSc4d1	pravé
poledne	poledne	k1gNnSc4	poledne
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
hodnověrným	hodnověrný	k2eAgMnSc7d1	hodnověrný
očitým	očitý	k2eAgMnSc7d1	očitý
svědkem	svědek	k1gMnSc7	svědek
erupce	erupce	k1gFnSc2	erupce
byl	být	k5eAaImAgMnS	být
Plinius	Plinius	k1gMnSc1	Plinius
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
událost	událost	k1gFnSc4	událost
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Tacitovi	Tacita	k1gMnSc3	Tacita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
objevuje	objevovat	k5eAaImIp3nS	objevovat
tradičně	tradičně	k6eAd1	tradičně
uváděné	uváděný	k2eAgNnSc1d1	uváděné
datum	datum	k1gNnSc1	datum
erupce	erupce	k1gFnSc2	erupce
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
zřejmě	zřejmě	k6eAd1	zřejmě
chybou	chyba	k1gFnSc7	chyba
opisovačů	opisovač	k1gMnPc2	opisovač
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
erupci	erupce	k1gFnSc3	erupce
kolem	kolem	k7c2	kolem
poledne	poledne	k1gNnSc2	poledne
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
z	z	k7c2	z
Vesuvu	Vesuv	k1gInSc2	Vesuv
vyšlehly	vyšlehnout	k5eAaPmAgInP	vyšlehnout
vysoké	vysoký	k2eAgInPc1d1	vysoký
plameny	plamen	k1gInPc1	plamen
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
velký	velký	k2eAgInSc1d1	velký
černý	černý	k2eAgInSc1d1	černý
mrak	mrak	k1gInSc1	mrak
tvaru	tvar	k1gInSc2	tvar
borovice	borovice	k1gFnSc2	borovice
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zakryl	zakrýt	k5eAaPmAgMnS	zakrýt
slunce	slunce	k1gNnSc4	slunce
a	a	k8xC	a
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c4	v
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Pompeje	Pompeje	k1gFnPc4	Pompeje
dopadaly	dopadat	k5eAaImAgFnP	dopadat
rozžhavené	rozžhavený	k2eAgInPc4d1	rozžhavený
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
bořily	bořit	k5eAaImAgFnP	bořit
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatele	obyvatel	k1gMnPc4	obyvatel
města	město	k1gNnSc2	město
usmrtily	usmrtit	k5eAaPmAgFnP	usmrtit
plyny	plyn	k1gInPc4	plyn
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
pyroklastická	pyroklastický	k2eAgFnSc1d1	pyroklastická
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
mrak	mrak	k1gInSc4	mrak
přehřátých	přehřátý	k2eAgInPc2d1	přehřátý
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
popela	popel	k1gInSc2	popel
a	a	k8xC	a
kamenů	kámen	k1gInPc2	kámen
vyvrhnutých	vyvrhnutý	k2eAgInPc2d1	vyvrhnutý
ze	z	k7c2	z
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgInPc2d1	současný
výzkumů	výzkum	k1gInPc2	výzkum
přitom	přitom	k6eAd1	přitom
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Erupce	erupce	k1gFnSc1	erupce
trvala	trvat	k5eAaImAgFnS	trvat
nepřetržitě	přetržitě	k6eNd1	přetržitě
3	[number]	k4	3
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yRnSc2	což
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc4	město
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
vrstvou	vrstva	k1gFnSc7	vrstva
popela	popel	k1gInSc2	popel
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zničeno	zničen	k2eAgNnSc1d1	zničeno
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
od	od	k7c2	od
Herculanea	Herculane	k1gInSc2	Herculane
až	až	k9	až
po	po	k7c4	po
Stabie	Stabius	k1gMnPc4	Stabius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Mrak	mrak	k1gInSc4	mrak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
Plinius	Plinius	k1gMnSc1	Plinius
mladší	mladý	k2eAgMnSc1d2	mladší
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
interpretován	interpretován	k2eAgInSc1d1	interpretován
právě	právě	k9	právě
jako	jako	k8xC	jako
pyroklastická	pyroklastický	k2eAgFnSc1d1	pyroklastická
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Plinius	Plinius	k1gInSc1	Plinius
rovněž	rovněž	k9	rovněž
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
erupce	erupce	k1gFnSc1	erupce
byla	být	k5eAaImAgFnS	být
současně	současně	k6eAd1	současně
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
a	a	k8xC	a
že	že	k9	že
moře	moře	k1gNnSc1	moře
bylo	být	k5eAaImAgNnS	být
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
odsáváno	odsávat	k5eAaImNgNnS	odsávat
pryč	pryč	k6eAd1	pryč
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
vraceno	vracen	k2eAgNnSc4d1	vraceno
–	–	k?	–
dnes	dnes	k6eAd1	dnes
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
nazýváme	nazývat	k5eAaImIp1nP	nazývat
"	"	kIx"	"
<g/>
tsunami	tsunami	k1gNnSc7	tsunami
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c4	na
veslici	veslice	k1gFnSc4	veslice
dopravit	dopravit	k5eAaPmF	dopravit
do	do	k7c2	do
Stabií	Stabie	k1gFnPc2	Stabie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
úkaz	úkaz	k1gInSc1	úkaz
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
zvědavost	zvědavost	k1gFnSc4	zvědavost
doplatil	doplatit	k5eAaPmAgInS	doplatit
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
mrtvý	mrtvý	k1gMnSc1	mrtvý
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
ve	v	k7c6	v
Stabiích	Stabie	k1gFnPc6	Stabie
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zadušen	zadusit	k5eAaPmNgInS	zadusit
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Záchranné	záchranný	k2eAgFnSc2d1	záchranná
práce	práce	k1gFnSc2	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c4	po
zasypání	zasypání	k1gNnSc4	zasypání
města	město	k1gNnSc2	město
sopečným	sopečný	k2eAgInSc7d1	sopečný
popelem	popel	k1gInSc7	popel
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
pracovat	pracovat	k5eAaImF	pracovat
vojenské	vojenský	k2eAgFnSc2d1	vojenská
čety	četa	k1gFnSc2	četa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
orientovaly	orientovat	k5eAaBmAgFnP	orientovat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podle	podle	k7c2	podle
střech	střecha	k1gFnPc2	střecha
vyčnívajících	vyčnívající	k2eAgInPc2d1	vyčnívající
ze	z	k7c2	z
sutin	sutina	k1gFnPc2	sutina
<g/>
.	.	kIx.	.
</s>
<s>
Záchranáři	záchranář	k1gMnPc1	záchranář
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zanechali	zanechat	k5eAaPmAgMnP	zanechat
nápisy	nápis	k1gInPc4	nápis
na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
při	při	k7c6	při
hloubení	hloubení	k1gNnSc6	hloubení
odkryli	odkrýt	k5eAaPmAgMnP	odkrýt
"	"	kIx"	"
<g/>
Tady	tady	k6eAd1	tady
jsme	být	k5eAaImIp1nP	být
pronikli	proniknout	k5eAaPmAgMnP	proniknout
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Tady	tady	k6eAd1	tady
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
padesát	padesát	k4xCc1	padesát
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
leží	ležet	k5eAaImIp3nS	ležet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
<g/>
"	"	kIx"	"
Když	když	k8xS	když
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nikdo	nikdo	k3yNnSc1	nikdo
nepřežil	přežít	k5eNaPmAgMnS	přežít
<g/>
,	,	kIx,	,
pozornost	pozornost	k1gFnSc1	pozornost
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
obrátila	obrátit	k5eAaPmAgFnS	obrátit
k	k	k7c3	k
cenným	cenný	k2eAgInPc3d1	cenný
materiálům	materiál	k1gInPc3	materiál
a	a	k8xC	a
císařským	císařský	k2eAgFnPc3d1	císařská
sochám	socha	k1gFnPc3	socha
na	na	k7c6	na
Fóru	fórum	k1gNnSc6	fórum
(	(	kIx(	(
<g/>
zmizely	zmizet	k5eAaPmAgFnP	zmizet
dlažby	dlažba	k1gFnPc1	dlažba
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
mramorová	mramorový	k2eAgNnPc1d1	mramorové
obložení	obložení	k1gNnPc1	obložení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
také	také	k9	také
našli	najít	k5eAaPmAgMnP	najít
jen	jen	k9	jen
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
domácích	domácí	k2eAgFnPc2d1	domácí
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
postiženým	postižený	k1gMnPc3	postižený
ovšem	ovšem	k9	ovšem
vláda	vláda	k1gFnSc1	vláda
neučinila	učinit	k5eNaPmAgFnS	učinit
žádné	žádný	k3yNgNnSc4	žádný
reálné	reálný	k2eAgNnSc1d1	reálné
opatření	opatření	k1gNnSc1	opatření
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Titus	Titus	k1gMnSc1	Titus
sice	sice	k8xC	sice
zřídil	zřídit	k5eAaPmAgMnS	zřídit
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
Kampánie	Kampánie	k1gFnSc2	Kampánie
<g/>
,	,	kIx,	,
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
však	však	k9	však
nic	nic	k3yNnSc1	nic
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ztraceno	ztratit	k5eAaPmNgNnS	ztratit
na	na	k7c4	na
šestnáct	šestnáct	k4xCc4	šestnáct
století	století	k1gNnPc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Silná	silný	k2eAgFnSc1d1	silná
vrstva	vrstva	k1gFnSc1	vrstva
popela	popel	k1gInSc2	popel
pokryla	pokrýt	k5eAaPmAgFnS	pokrýt
dvě	dva	k4xCgNnPc4	dva
města	město	k1gNnPc4	město
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
sopky	sopka	k1gFnSc2	sopka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
jména	jméno	k1gNnPc1	jméno
byla	být	k5eAaImAgNnP	být
zapomenuta	zapomenout	k5eAaPmNgNnP	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Herculaneum	Herculaneum	k1gNnSc1	Herculaneum
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1738	[number]	k4	1738
<g/>
,	,	kIx,	,
Pompeje	Pompeje	k1gFnPc1	Pompeje
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1748	[number]	k4	1748
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
archeologické	archeologický	k2eAgFnPc1d1	archeologická
práce	práce	k1gFnPc1	práce
a	a	k8xC	a
odkryto	odkrýt	k5eAaPmNgNnS	odkrýt
mnoho	mnoho	k4c1	mnoho
zachovalých	zachovalý	k2eAgFnPc2d1	zachovalá
budov	budova	k1gFnPc2	budova
s	s	k7c7	s
nástěnnými	nástěnný	k2eAgFnPc7d1	nástěnná
malbami	malba	k1gFnPc7	malba
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
měst	město	k1gNnPc2	město
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1599	[number]	k4	1599
<g/>
,	,	kIx,	,
když	když	k8xS	když
architekt	architekt	k1gMnSc1	architekt
jménem	jméno	k1gNnSc7	jméno
Fontana	Fontana	k1gFnSc1	Fontana
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
nový	nový	k2eAgInSc4d1	nový
tvar	tvar	k1gInSc4	tvar
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
Sarno	Sarno	k6eAd1	Sarno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trvalo	trvat	k5eAaImAgNnS	trvat
téměř	téměř	k6eAd1	téměř
150	[number]	k4	150
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
skutečný	skutečný	k2eAgInSc1d1	skutečný
archeologický	archeologický	k2eAgInSc1d1	archeologický
výzkum	výzkum	k1gInSc1	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
do	do	k7c2	do
výzkumu	výzkum	k1gInSc2	výzkum
zapojil	zapojit	k5eAaPmAgMnS	zapojit
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Fiorelli	Fiorell	k1gMnSc5	Fiorell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
prvních	první	k4xOgFnPc2	první
vykopávek	vykopávka	k1gFnPc2	vykopávka
byly	být	k5eAaImAgInP	být
občas	občas	k6eAd1	občas
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
dutiny	dutina	k1gFnPc1	dutina
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
popela	popel	k1gInSc2	popel
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
lidské	lidský	k2eAgInPc4d1	lidský
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
<g/>
.	.	kIx.	.
</s>
<s>
Giuseppe	Giuseppat	k5eAaPmIp3nS	Giuseppat
Fiorelli	Fiorelle	k1gFnSc4	Fiorelle
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
vyplnit	vyplnit	k5eAaPmF	vyplnit
dutiny	dutina	k1gFnSc2	dutina
sádrou	sádra	k1gFnSc7	sádra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
věrných	věrný	k2eAgFnPc2d1	věrná
podob	podoba	k1gFnPc2	podoba
Pompejanů	Pompejan	k1gMnPc2	Pompejan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nestihli	stihnout	k5eNaPmAgMnP	stihnout
utéct	utéct	k5eAaPmF	utéct
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
posledním	poslední	k2eAgInSc6d1	poslední
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
zděšení	zděšení	k1gNnSc3	zděšení
je	být	k5eAaImIp3nS	být
jasně	jasně	k6eAd1	jasně
viditelný	viditelný	k2eAgInSc1d1	viditelný
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
tvářích	tvář	k1gFnPc6	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
odlitky	odlitek	k1gInPc4	odlitek
je	být	k5eAaImIp3nS	být
šokujícím	šokující	k2eAgMnSc7d1	šokující
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
hlubokým	hluboký	k2eAgInSc7d1	hluboký
zážitkem	zážitek	k1gInSc7	zážitek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fontana	Fontana	k1gFnSc1	Fontana
původně	původně	k6eAd1	původně
odkryl	odkrýt	k5eAaPmAgMnS	odkrýt
některé	některý	k3yIgFnPc4	některý
ze	z	k7c2	z
slavných	slavný	k2eAgFnPc2d1	slavná
erotických	erotický	k2eAgFnPc2d1	erotická
fresek	freska	k1gFnPc2	freska
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
cudnost	cudnost	k1gFnSc4	cudnost
běžnou	běžný	k2eAgFnSc7d1	běžná
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
zakryl	zakrýt	k5eAaPmAgMnS	zakrýt
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
archeologickou	archeologický	k2eAgFnSc4d1	archeologická
cenzuru	cenzura	k1gFnSc4	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
je	být	k5eAaImIp3nS	být
podpořen	podpořen	k2eAgInSc4d1	podpořen
záznamy	záznam	k1gInPc4	záznam
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
archeologů	archeolog	k1gMnPc2	archeolog
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
pracují	pracovat	k5eAaImIp3nP	pracovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
navštívena	navštíven	k2eAgFnSc1d1	navštívena
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
zakryta	zakryt	k2eAgFnSc1d1	zakryta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Forum	forum	k1gNnSc1	forum
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
domů	dům	k1gInPc2	dům
a	a	k8xC	a
vil	vila	k1gFnPc2	vila
bylo	být	k5eAaImAgNnS	být
překvapivě	překvapivě	k6eAd1	překvapivě
dobře	dobře	k6eAd1	dobře
zachováno	zachovat	k5eAaPmNgNnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
nalezen	nalezen	k2eAgInSc1d1	nalezen
hotel	hotel	k1gInSc1	hotel
(	(	kIx(	(
<g/>
1	[number]	k4	1
000	[number]	k4	000
m2	m2	k4	m2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pompeje	Pompeje	k1gInPc1	Pompeje
jsou	být	k5eAaImIp3nP	být
jediné	jediný	k2eAgNnSc4d1	jediné
starověké	starověký	k2eAgNnSc4d1	starověké
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
topografie	topografie	k1gFnSc1	topografie
nebyla	být	k5eNaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
jej	on	k3xPp3gMnSc4	on
vidíme	vidět	k5eAaImIp1nP	vidět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
původně	původně	k6eAd1	původně
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnPc1	ulice
byly	být	k5eAaImAgFnP	být
rovné	rovný	k2eAgFnPc1d1	rovná
<g/>
,	,	kIx,	,
tvořily	tvořit	k5eAaImAgFnP	tvořit
pravoúhlou	pravoúhlý	k2eAgFnSc4d1	pravoúhlá
mřížku	mřížka	k1gFnSc4	mřížka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
dlážděny	dlážděn	k2eAgInPc1d1	dlážděn
mnohostěnnými	mnohostěnný	k2eAgInPc7d1	mnohostěnný
kameny	kámen	k1gInPc7	kámen
a	a	k8xC	a
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
lemovány	lemován	k2eAgInPc4d1	lemován
obchody	obchod	k1gInPc4	obchod
a	a	k8xC	a
lázněmi	lázeň	k1gFnPc7	lázeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
,	,	kIx,	,
sesuvy	sesuv	k1gInPc1	sesuv
země	zem	k1gFnSc2	zem
a	a	k8xC	a
škody	škoda	k1gFnPc4	škoda
způsobené	způsobený	k2eAgFnPc4d1	způsobená
sopkou	sopka	k1gFnSc7	sopka
==	==	k?	==
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
směrů	směr	k1gInPc2	směr
současných	současný	k2eAgInPc2d1	současný
výzkumů	výzkum	k1gInPc2	výzkum
je	být	k5eAaImIp3nS	být
zkoumání	zkoumání	k1gNnSc4	zkoumání
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
erupce	erupce	k1gFnSc2	erupce
právě	právě	k6eAd1	právě
rekonstruovaly	rekonstruovat	k5eAaBmAgFnP	rekonstruovat
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
63	[number]	k4	63
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
staré	starý	k2eAgFnPc1d1	stará
a	a	k8xC	a
poškozené	poškozený	k2eAgFnPc1d1	poškozená
malby	malba	k1gFnPc1	malba
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
zakryty	zakrýt	k5eAaPmNgFnP	zakrýt
novými	nový	k2eAgFnPc7d1	nová
a	a	k8xC	a
vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
snaží	snažit	k5eAaImIp3nP	snažit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
moderních	moderní	k2eAgInPc2d1	moderní
přístrojů	přístroj	k1gInPc2	přístroj
objevit	objevit	k5eAaPmF	objevit
alespoň	alespoň	k9	alespoň
stín	stín	k1gInSc4	stín
dlouho	dlouho	k6eAd1	dlouho
skrytých	skrytý	k2eAgFnPc2d1	skrytá
fresek	freska	k1gFnPc2	freska
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byly	být	k5eAaImAgFnP	být
některé	některý	k3yIgFnPc1	některý
stavby	stavba	k1gFnPc1	stavba
opravovány	opravován	k2eAgFnPc1d1	opravována
ještě	ještě	k9	ještě
17	[number]	k4	17
let	léto	k1gNnPc2	léto
po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zvyšující	zvyšující	k2eAgFnSc1d1	zvyšující
se	se	k3xPyFc4	se
frekvence	frekvence	k1gFnSc1	frekvence
malých	malý	k2eAgInPc2d1	malý
otřesů	otřes	k1gInPc2	otřes
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nakonec	nakonec	k6eAd1	nakonec
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c6	v
erupci	erupce	k1gFnSc6	erupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
archeologických	archeologický	k2eAgFnPc2d1	archeologická
prací	práce	k1gFnPc2	práce
se	se	k3xPyFc4	se
zastavila	zastavit	k5eAaPmAgFnS	zastavit
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
ulice	ulice	k1gFnSc2	ulice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
79	[number]	k4	79
<g/>
.	.	kIx.	.
</s>
<s>
Zemní	zemní	k2eAgFnSc1d1	zemní
práce	práce	k1gFnSc1	práce
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
a	a	k8xC	a
hloubkové	hloubkový	k2eAgInPc1d1	hloubkový
vrty	vrt	k1gInPc1	vrt
však	však	k9	však
zjevily	zjevit	k5eAaPmAgInP	zjevit
vrstvy	vrstva	k1gFnSc2	vrstva
usazeného	usazený	k2eAgInSc2d1	usazený
sedimentu	sediment	k1gInSc2	sediment
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
79	[number]	k4	79
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
přírodními	přírodní	k2eAgFnPc7d1	přírodní
katastrofami	katastrofa	k1gFnPc7	katastrofa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vrstvě	vrstva	k1gFnSc6	vrstva
lávového	lávový	k2eAgNnSc2d1	lávové
podloží	podloží	k1gNnSc2	podloží
pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
byly	být	k5eAaImAgFnP	být
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
tři	tři	k4xCgFnPc4	tři
vrstvy	vrstva	k1gFnPc4	vrstva
sedimentu	sediment	k1gInSc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
vrstvách	vrstva	k1gFnPc6	vrstva
vědci	vědec	k1gMnPc1	vědec
nalezli	naleznout	k5eAaPmAgMnP	naleznout
kosti	kost	k1gFnPc4	kost
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
úlomky	úlomek	k1gInPc1	úlomek
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
otisky	otisk	k1gInPc7	otisk
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
radiokarbonové	radiokarbonový	k2eAgFnSc2d1	radiokarbonová
metody	metoda	k1gFnSc2	metoda
datování	datování	k1gNnSc4	datování
vědci	vědec	k1gMnPc1	vědec
určili	určit	k5eAaPmAgMnP	určit
stáří	stáří	k1gNnSc4	stáří
první	první	k4xOgFnSc2	první
vrstvy	vrstva	k1gFnSc2	vrstva
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
vrstvy	vrstva	k1gFnPc1	vrstva
jsou	být	k5eAaImIp3nP	být
odděleny	oddělit	k5eAaPmNgFnP	oddělit
obdělanou	obdělaný	k2eAgFnSc7d1	obdělaná
půdou	půda	k1gFnSc7	půda
a	a	k8xC	a
kamennými	kamenný	k2eAgFnPc7d1	kamenná
silnicemi	silnice	k1gFnPc7	silnice
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vznik	vznik	k1gInSc1	vznik
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Za	za	k7c7	za
vznikem	vznik	k1gInSc7	vznik
těchto	tento	k3xDgInPc2	tento
sedimentů	sediment	k1gInPc2	sediment
se	se	k3xPyFc4	se
skrývají	skrývat	k5eAaImIp3nP	skrývat
patrně	patrně	k6eAd1	patrně
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
sesuvy	sesuv	k1gInPc1	sesuv
půdy	půda	k1gFnSc2	půda
vlivem	vlivem	k7c2	vlivem
extrémních	extrémní	k2eAgFnPc2d1	extrémní
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Senatore	Senator	k1gMnSc5	Senator
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Geologové	geolog	k1gMnPc1	geolog
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
pomocí	pomocí	k7c2	pomocí
magnetické	magnetický	k2eAgFnSc2d1	magnetická
charakteristiky	charakteristika	k1gFnSc2	charakteristika
kamenů	kámen	k1gInPc2	kámen
z	z	k7c2	z
Pompejí	Pompeje	k1gFnPc2	Pompeje
určit	určit	k5eAaPmF	určit
teplotu	teplota	k1gFnSc4	teplota
pyroklastické	pyroklastický	k2eAgFnSc2d1	pyroklastická
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pohřbila	pohřbít	k5eAaPmAgFnS	pohřbít
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
roztavená	roztavený	k2eAgFnSc1d1	roztavená
láva	láva	k1gFnSc1	láva
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
<g/>
,	,	kIx,	,
magnetické	magnetický	k2eAgInPc4d1	magnetický
minerály	minerál	k1gInPc4	minerál
v	v	k7c4	v
ni	on	k3xPp3gFnSc4	on
obsažené	obsažený	k2eAgInPc1d1	obsažený
se	se	k3xPyFc4	se
orientují	orientovat	k5eAaBmIp3nP	orientovat
shodně	shodně	k6eAd1	shodně
s	s	k7c7	s
zemským	zemský	k2eAgNnSc7d1	zemské
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
materiál	materiál	k1gInSc1	materiál
zahřát	zahřát	k5eAaPmNgInS	zahřát
nad	nad	k7c4	nad
určitou	určitý	k2eAgFnSc4d1	určitá
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xC	jako
Curieho	Curie	k1gMnSc2	Curie
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
změněno	změnit	k5eAaPmNgNnS	změnit
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Analýza	analýza	k1gFnSc1	analýza
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
vulkanických	vulkanický	k2eAgInPc2d1	vulkanický
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
nálezů	nález	k1gInPc2	nález
(	(	kIx(	(
<g/>
např.	např.	kA	např.
střešní	střešní	k2eAgFnPc4d1	střešní
tašky	taška	k1gFnPc4	taška
<g/>
)	)	kIx)	)
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
vrcholu	vrchol	k1gInSc2	vrchol
sopky	sopka	k1gFnSc2	sopka
měl	mít	k5eAaImAgInS	mít
mrak	mrak	k1gInSc1	mrak
teplotu	teplota	k1gFnSc4	teplota
kolem	kolem	k7c2	kolem
850	[number]	k4	850
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
než	než	k8xS	než
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ochladl	ochladnout	k5eAaPmAgInS	ochladnout
na	na	k7c4	na
350	[number]	k4	350
°	°	k?	°
<g/>
C.	C.	kA	C.
Většina	většina	k1gFnSc1	většina
zkoumaných	zkoumaný	k2eAgInPc2d1	zkoumaný
materiálů	materiál	k1gInPc2	materiál
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
teplotě	teplota	k1gFnSc6	teplota
mezi	mezi	k7c4	mezi
240	[number]	k4	240
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
340	[number]	k4	340
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
byla	být	k5eAaImAgFnS	být
teplota	teplota	k1gFnSc1	teplota
ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
180	[number]	k4	180
°	°	k?	°
<g/>
C.	C.	kA	C.
(	(	kIx(	(
<g/>
Cioni	Cioň	k1gMnSc3	Cioň
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jedinečný	jedinečný	k2eAgInSc4d1	jedinečný
pohled	pohled	k1gInSc4	pohled
==	==	k?	==
</s>
</p>
<p>
<s>
Každopádně	každopádně	k6eAd1	každopádně
nám	my	k3xPp1nPc3	my
město	město	k1gNnSc1	město
nabízí	nabízet	k5eAaImIp3nS	nabízet
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
život	život	k1gInSc4	život
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
Pompeje	Pompeje	k1gFnPc1	Pompeje
velmi	velmi	k6eAd1	velmi
živým	živý	k2eAgNnSc7d1	živé
místem	místo	k1gNnSc7	místo
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
zde	zde	k6eAd1	zde
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k8xC	i
ty	ten	k3xDgInPc1	ten
nejmenší	malý	k2eAgInPc1d3	nejmenší
detaily	detail	k1gInPc1	detail
o	o	k7c6	o
každodenním	každodenní	k2eAgInSc6d1	každodenní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
Siricova	Siricův	k2eAgInSc2d1	Siricův
domu	dům	k1gInSc2	dům
<g/>
)	)	kIx)	)
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
nápis	nápis	k1gInSc4	nápis
Salve	Salve	k1gNnSc2	Salve
<g/>
,	,	kIx,	,
lucru	lucr	k1gInSc2	lucr
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vítejte	vítat	k5eAaImRp2nP	vítat
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc4	peníz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
"	"	kIx"	"
<g/>
Vítej	vítat	k5eAaImRp2nS	vítat
<g/>
,	,	kIx,	,
zisku	zisk	k1gInSc2	zisk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
humorném	humorný	k2eAgInSc6d1	humorný
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
sídlila	sídlit	k5eAaImAgFnS	sídlit
obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
Siricem	Siric	k1gMnSc7	Siric
a	a	k8xC	a
Nummianem	Nummian	k1gInSc7	Nummian
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
přezdívku	přezdívka	k1gFnSc4	přezdívka
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
nummus	nummus	k1gInSc1	nummus
znamená	znamenat	k5eAaImIp3nS	znamenat
mince	mince	k1gFnPc4	mince
či	či	k8xC	či
peníze	peníz	k1gInPc4	peníz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
domech	dům	k1gInPc6	dům
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
detaily	detail	k1gInPc4	detail
o	o	k7c6	o
jiných	jiný	k2eAgFnPc6d1	jiná
profesích	profes	k1gFnPc6	profes
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
čistírny	čistírna	k1gFnPc1	čistírna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Fullones	Fullones	k1gInSc1	Fullones
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
džbánech	džbán	k1gInPc6	džbán
s	s	k7c7	s
vínem	víno	k1gNnSc7	víno
byly	být	k5eAaImAgInP	být
nápisy	nápis	k1gInPc1	nápis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
první	první	k4xOgFnPc4	první
reklamní	reklamní	k2eAgFnPc4d1	reklamní
slovní	slovní	k2eAgFnPc4d1	slovní
hříčky	hříčka	k1gFnPc4	hříčka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
Vesuvinum	Vesuvinum	k1gInSc1	Vesuvinum
<g/>
.	.	kIx.	.
</s>
<s>
Graffiti	graffiti	k1gNnSc1	graffiti
na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
nám	my	k3xPp1nPc3	my
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
pravou	pravý	k2eAgFnSc4d1	pravá
pouliční	pouliční	k2eAgFnSc4d1	pouliční
latinu	latina	k1gFnSc4	latina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
erupce	erupce	k1gFnSc2	erupce
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
kolem	kolem	k7c2	kolem
20	[number]	k4	20
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
rekreační	rekreační	k2eAgFnSc1d1	rekreační
oblast	oblast	k1gFnSc1	oblast
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Nalezneme	nalézt	k5eAaBmIp1nP	nalézt
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
staveb	stavba	k1gFnPc2	stavba
občanské	občanský	k2eAgFnSc2d1	občanská
vybavenosti	vybavenost	k1gFnSc2	vybavenost
<g/>
:	:	kIx,	:
Macellum	Macellum	k1gInSc1	Macellum
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
trh	trh	k1gInSc1	trh
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pistrinum	Pistrinum	k1gInSc1	Pistrinum
(	(	kIx(	(
<g/>
mlýn	mlýn	k1gInSc1	mlýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thermopolia	Thermopolia	k1gFnSc1	Thermopolia
(	(	kIx(	(
<g/>
bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
podávány	podávat	k5eAaImNgInP	podávat
teplé	teplý	k2eAgInPc1d1	teplý
i	i	k8xC	i
studené	studený	k2eAgInPc1d1	studený
nápoje	nápoj	k1gInPc1	nápoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cauporioe	cauporio	k1gFnPc4	cauporio
(	(	kIx(	(
<g/>
malé	malý	k2eAgFnPc4d1	malá
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
)	)	kIx)	)
a	a	k8xC	a
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
výzkumy	výzkum	k1gInPc1	výzkum
v	v	k7c4	v
ústí	ústí	k1gNnSc4	ústí
řeky	řeka	k1gFnSc2	řeka
Sarno	Sarno	k6eAd1	Sarno
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přístav	přístav	k1gInSc1	přístav
byl	být	k5eAaImAgInS	být
obydlen	obydlen	k2eAgInSc1d1	obydlen
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
systémech	systém	k1gInPc6	systém
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mnoha	mnoho	k4c3	mnoho
vědcům	vědec	k1gMnPc3	vědec
připomíná	připomínat	k5eAaImIp3nS	připomínat
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výzkumy	výzkum	k1gInPc1	výzkum
jsou	být	k5eAaImIp3nP	být
zatím	zatím	k6eAd1	zatím
na	na	k7c6	na
počátku	počátek	k1gInSc2	počátek
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
teprve	teprve	k6eAd1	teprve
přinesou	přinést	k5eAaPmIp3nP	přinést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dnešní	dnešní	k2eAgFnPc1d1	dnešní
Pompeje	Pompeje	k1gFnPc1	Pompeje
==	==	k?	==
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
vedle	vedle	k7c2	vedle
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
starověkých	starověký	k2eAgFnPc2d1	starověká
Pompejí	Pompeje	k1gFnPc2	Pompeje
město	město	k1gNnSc4	město
Pompei	Pompei	k1gNnSc2	Pompei
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
26	[number]	k4	26
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pompeje	Pompeje	k1gFnPc1	Pompeje
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Pompeje	Pompeje	k1gInPc1	Pompeje
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k8xC	jako
námět	námět	k1gInSc1	námět
pro	pro	k7c4	pro
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
Poslední	poslední	k2eAgInPc4d1	poslední
dny	den	k1gInPc4	den
Pompejí	Pompeje	k1gFnPc2	Pompeje
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
britský	britský	k2eAgInSc4d1	britský
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
Up	Up	k1gFnSc2	Up
Pompeii	Pompeie	k1gFnSc4	Pompeie
a	a	k8xC	a
román	román	k1gInSc4	román
Roberta	Robert	k1gMnSc2	Robert
Harrise	Harrise	k1gFnSc2	Harrise
Pompeje	Pompeje	k1gInPc4	Pompeje
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
je	být	k5eAaImIp3nS	být
slavný	slavný	k2eAgInSc1d1	slavný
obraz	obraz	k1gInSc1	obraz
Poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
Pompejí	Pompeje	k1gFnPc2	Pompeje
ruského	ruský	k2eAgMnSc2d1	ruský
malíře	malíř	k1gMnSc2	malíř
Carlo	Carlo	k1gNnSc1	Carlo
Brulla	Brulla	k1gMnSc1	Brulla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1971	[number]	k4	1971
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
britská	britský	k2eAgFnSc1d1	britská
skupina	skupina	k1gFnSc1	skupina
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
ve	v	k7c4	v
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
starém	starý	k2eAgInSc6d1	starý
amfiteátru	amfiteátr	k1gInSc6	amfiteátr
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
pro	pro	k7c4	pro
publikum	publikum	k1gNnSc4	publikum
složené	složený	k2eAgNnSc4d1	složené
z	z	k7c2	z
kameramanů	kameraman	k1gMnPc2	kameraman
a	a	k8xC	a
filmového	filmový	k2eAgInSc2d1	filmový
štábu	štáb	k1gInSc2	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Videozáznam	videozáznam	k1gInSc1	videozáznam
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
koncertu	koncert	k1gInSc2	koncert
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
a	a	k8xC	a
film	film	k1gInSc1	film
Live	Liv	k1gFnSc2	Liv
at	at	k?	at
Pompeii	Pompeie	k1gFnSc4	Pompeie
–	–	k?	–
záznam	záznam	k1gInSc1	záznam
ze	z	k7c2	z
sólových	sólový	k2eAgInPc2d1	sólový
koncertů	koncert	k1gInPc2	koncert
David	David	k1gMnSc1	David
Gilmoura	Gilmour	k1gMnSc2	Gilmour
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Poslední	poslední	k2eAgFnSc2d1	poslední
dny	dna	k1gFnSc2	dna
Pompejí	Pompeje	k1gFnPc2	Pompeje
nese	nést	k5eAaImIp3nS	nést
také	také	k9	také
opera	opera	k1gFnSc1	opera
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Nova	nova	k1gFnSc1	nova
Mob	Mob	k1gFnSc1	Mob
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
skupina	skupina	k1gFnSc1	skupina
Bastille	Bastille	k1gFnSc2	Bastille
vydala	vydat	k5eAaPmAgFnS	vydat
song	song	k1gInSc4	song
s	s	k7c7	s
názvem	název	k1gInSc7	název
Pompeii	Pompeie	k1gFnSc4	Pompeie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Návštěva	návštěva	k1gFnSc1	návštěva
místa	místo	k1gNnSc2	místo
==	==	k?	==
</s>
</p>
<p>
<s>
Archeologická	archeologický	k2eAgFnSc1d1	archeologická
lokalita	lokalita	k1gFnSc1	lokalita
leží	ležet	k5eAaImIp3nS	ležet
těsně	těsně	k6eAd1	těsně
u	u	k7c2	u
vlakové	vlakový	k2eAgFnSc2d1	vlaková
zastávky	zastávka	k1gFnSc2	zastávka
Pompeii-	Pompeii-	k1gFnSc2	Pompeii-
Scavi	Scaev	k1gFnSc3	Scaev
tratě	trať	k1gFnSc2	trať
Circumvesuviana	Circumvesuvian	k1gMnSc2	Circumvesuvian
ze	z	k7c2	z
Sorrenta	Sorrento	k1gNnSc2	Sorrento
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
z	z	k7c2	z
vlaku	vlak	k1gInSc2	vlak
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dát	dát	k5eAaPmF	dát
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nás	my	k3xPp1nPc4	my
čeká	čekat	k5eAaImIp3nS	čekat
pár	pár	k4xCyI	pár
stánků	stánek	k1gInPc2	stánek
s	s	k7c7	s
nezbytnými	zbytný	k2eNgInPc7d1	zbytný
suvenýry	suvenýr	k1gInPc7	suvenýr
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgNnPc6	jenž
následuje	následovat	k5eAaImIp3nS	následovat
hlavní	hlavní	k2eAgInSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ovšem	ovšem	k9	ovšem
jít	jít	k5eAaImF	jít
i	i	k9	i
vlevo	vlevo	k6eAd1	vlevo
k	k	k7c3	k
méně	málo	k6eAd2	málo
vytížené	vytížený	k2eAgFnSc3d1	vytížená
bráně	brána	k1gFnSc3	brána
Villa	Villo	k1gNnSc2	Villo
dei	dei	k?	dei
Misteri	Misteri	k1gNnSc2	Misteri
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
lze	lze	k6eAd1	lze
zakoupit	zakoupit	k5eAaPmF	zakoupit
vstupenku	vstupenka	k1gFnSc4	vstupenka
do	do	k7c2	do
Pompejí	Pompeje	k1gFnPc2	Pompeje
za	za	k7c4	za
13	[number]	k4	13
eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zvýhodněnou	zvýhodněný	k2eAgFnSc4d1	zvýhodněná
vstupenku	vstupenka	k1gFnSc4	vstupenka
do	do	k7c2	do
Pompejí	Pompeje	k1gFnPc2	Pompeje
<g/>
,	,	kIx,	,
Herculanea	Herculaneum	k1gNnSc2	Herculaneum
<g/>
,	,	kIx,	,
Stabií	Stabie	k1gFnPc2	Stabie
<g/>
,	,	kIx,	,
Oplontis	Oplontis	k1gFnSc6	Oplontis
a	a	k8xC	a
Boscoreale	Boscoreala	k1gFnSc6	Boscoreala
za	za	k7c4	za
20	[number]	k4	20
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
zapůjčit	zapůjčit	k5eAaPmF	zapůjčit
zvukové	zvukový	k2eAgNnSc4d1	zvukové
zařízení	zařízení	k1gNnSc4	zařízení
s	s	k7c7	s
nahranými	nahraný	k2eAgFnPc7d1	nahraná
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
nalezených	nalezený	k2eAgFnPc6d1	nalezená
památkách	památka	k1gFnPc6	památka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ceně	cena	k1gFnSc6	cena
vstupného	vstupné	k1gNnSc2	vstupné
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
návštěvníka	návštěvník	k1gMnSc4	návštěvník
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mapa	mapa	k1gFnSc1	mapa
Pompejí	Pompeje	k1gFnPc2	Pompeje
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
(	(	kIx(	(
<g/>
na	na	k7c4	na
vyžádání	vyžádání	k1gNnSc4	vyžádání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
32	[number]	k4	32
eura	euro	k1gNnSc2	euro
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
pořídit	pořídit	k5eAaPmF	pořídit
ArteCard	ArteCard	k1gInSc4	ArteCard
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc4d1	umožňující
navštívit	navštívit	k5eAaPmF	navštívit
dvě	dva	k4xCgNnPc1	dva
místa	místo	k1gNnPc1	místo
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
předchozích	předchozí	k2eAgFnPc2d1	předchozí
i	i	k8xC	i
muzea	muzeum	k1gNnSc2	muzeum
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
se	s	k7c7	s
slevou	sleva	k1gFnSc7	sleva
asi	asi	k9	asi
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
volně	volně	k6eAd1	volně
cestovat	cestovat	k5eAaImF	cestovat
po	po	k7c6	po
Kampánii	Kampánie	k1gFnSc6	Kampánie
včetně	včetně	k7c2	včetně
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
Platnost	platnost	k1gFnSc1	platnost
této	tento	k3xDgFnSc3	tento
ArteCard	ArteCard	k1gInSc1	ArteCard
je	být	k5eAaImIp3nS	být
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
prvního	první	k4xOgNnSc2	první
použití	použití	k1gNnSc2	použití
do	do	k7c2	do
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
třetího	třetí	k4xOgInSc2	třetí
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
ArteCard	ArteCard	k1gMnSc1	ArteCard
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zlevněná	zlevněný	k2eAgFnSc1d1	zlevněná
pro	pro	k7c4	pro
mladé	mladý	k1gMnPc4	mladý
<g/>
,	,	kIx,	,
platná	platný	k2eAgFnSc1d1	platná
jen	jen	k9	jen
pro	pro	k7c4	pro
Neapol	Neapol	k1gFnSc4	Neapol
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
cestovat	cestovat	k5eAaImF	cestovat
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
místa	místo	k1gNnPc4	místo
k	k	k7c3	k
návštěvě	návštěva	k1gFnSc3	návštěva
===	===	k?	===
</s>
</p>
<p>
<s>
Mořská	mořský	k2eAgFnSc1d1	mořská
brána	brána	k1gFnSc1	brána
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
nejzachovalejší	zachovalý	k2eAgFnSc7d3	nejzachovalejší
bránou	brána	k1gFnSc7	brána
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
bran	brána	k1gFnPc2	brána
(	(	kIx(	(
<g/>
Vesuvská	vesuvský	k2eAgFnSc1d1	vesuvský
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Sarnská	Sarnský	k2eAgFnSc1d1	Sarnský
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
nebyly	být	k5eNaImAgFnP	být
po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
62	[number]	k4	62
kvůli	kvůli	k7c3	kvůli
politickému	politický	k2eAgNnSc3d1	politické
klimatu	klima	k1gNnSc3	klima
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
nebyly	být	k5eNaImAgFnP	být
pevné	pevný	k2eAgFnPc1d1	pevná
hradby	hradba	k1gFnPc1	hradba
nadále	nadále	k6eAd1	nadále
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
opraveny	opraven	k2eAgInPc1d1	opraven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fórum	fórum	k1gNnSc1	fórum
-	-	kIx~	-
Centrum	centrum	k1gNnSc1	centrum
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
Pompejí	Pompeje	k1gFnPc2	Pompeje
<g/>
,	,	kIx,	,
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
severo-jižním	severoižní	k2eAgInSc6d1	severo-jižní
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
fóra	fórum	k1gNnSc2	fórum
stojí	stát	k5eAaImIp3nS	stát
Jupiterův	Jupiterův	k2eAgInSc4d1	Jupiterův
chrám	chrám	k1gInSc4	chrám
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc4d1	vybudovaný
za	za	k7c2	za
samnitské	samnitský	k2eAgFnSc2d1	samnitská
éry	éra	k1gFnSc2	éra
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
století	století	k1gNnSc6	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zemněný	zemněný	k2eAgInSc1d1	zemněný
na	na	k7c4	na
Capitolinum	Capitolinum	k1gInSc4	Capitolinum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
chrámu	chrám	k1gInSc2	chrám
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
oblouky	oblouk	k1gInPc4	oblouk
<g/>
,	,	kIx,	,
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
císařské	císařský	k2eAgFnSc3d1	císařská
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
fóra	fórum	k1gNnSc2	fórum
leží	ležet	k5eAaImIp3nS	ležet
městská	městský	k2eAgFnSc1d1	městská
pokladnice	pokladnice	k1gFnSc1	pokladnice
<g/>
,	,	kIx,	,
sýpky	sýpka	k1gFnPc1	sýpka
a	a	k8xC	a
Apolónův	Apolónův	k2eAgInSc1d1	Apolónův
chrám	chrám	k1gInSc1	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
fóra	fórum	k1gNnSc2	fórum
stála	stát	k5eAaImAgFnS	stát
budova	budova	k1gFnSc1	budova
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
vedle	vedle	k6eAd1	vedle
budova	budova	k1gFnSc1	budova
chrámu	chrám	k1gInSc2	chrám
zasvěceného	zasvěcený	k2eAgInSc2d1	zasvěcený
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Vespasiánovi	Vespasiánův	k2eAgMnPc1d1	Vespasiánův
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
79	[number]	k4	79
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
Vesuvu	Vesuv	k1gInSc2	Vesuv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bazilika	bazilika	k1gFnSc1	bazilika
-	-	kIx~	-
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
okraji	okraj	k1gInSc6	okraj
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Bazilika	bazilika	k1gFnSc1	bazilika
byla	být	k5eAaImAgFnS	být
centerem	center	k1gInSc7	center
finančních	finanční	k2eAgFnPc2d1	finanční
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
koncem	koncem	k7c2	koncem
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
úctyhodné	úctyhodný	k2eAgFnSc6d1	úctyhodná
ploše	plocha	k1gFnSc6	plocha
55	[number]	k4	55
x	x	k?	x
24	[number]	k4	24
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
tři	tři	k4xCgInPc4	tři
vchody	vchod	k1gInPc4	vchod
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavní	hlavní	k2eAgFnSc1d1	hlavní
loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
podepřena	podepřít	k5eAaPmNgFnS	podepřít
dochovanými	dochovaný	k2eAgInPc7d1	dochovaný
sloupy	sloup	k1gInPc7	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
62	[number]	k4	62
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
a	a	k8xC	a
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
zkázy	zkáza	k1gFnSc2	zkáza
Pompejí	Pompeje	k1gFnPc2	Pompeje
nebyla	být	k5eNaImAgFnS	být
opravena	opravit	k5eAaPmNgFnS	opravit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trojúhelníkové	trojúhelníkový	k2eAgNnSc1d1	trojúhelníkové
fórum	fórum	k1gNnSc1	fórum
-	-	kIx~	-
náměstí	náměstí	k1gNnSc1	náměstí
kolem	kolem	k7c2	kolem
kterého	který	k3yQgMnSc2	který
jsou	být	k5eAaImIp3nP	být
seskupeny	seskupen	k2eAgFnPc4d1	seskupena
významné	významný	k2eAgFnPc4d1	významná
stavby	stavba	k1gFnPc4	stavba
-	-	kIx~	-
dvě	dva	k4xCgNnPc4	dva
divadla	divadlo	k1gNnPc4	divadlo
a	a	k8xC	a
tělocvična	tělocvična	k1gFnSc1	tělocvična
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
fóra	fórum	k1gNnSc2	fórum
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc4	zbytek
dórského	dórský	k2eAgInSc2d1	dórský
chrámu	chrám	k1gInSc2	chrám
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
důkazem	důkaz	k1gInSc7	důkaz
styků	styk	k1gInPc2	styk
Pompejí	Pompeje	k1gFnPc2	Pompeje
s	s	k7c7	s
řeckými	řecký	k2eAgNnPc7d1	řecké
městy	město	k1gNnPc7	město
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
v	v	k7c6	v
Cumae	Cumae	k1gFnSc6	Cumae
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
zrestaurován	zrestaurovat	k5eAaPmNgInS	zrestaurovat
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
a	a	k8xC	a
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
na	na	k7c4	na
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
kapli	kaple	k1gFnSc4	kaple
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
a	a	k8xC	a
malé	malý	k2eAgNnSc1d1	malé
divadlo	divadlo	k1gNnSc1	divadlo
-	-	kIx~	-
obě	dva	k4xCgNnPc4	dva
divadla	divadlo	k1gNnPc4	divadlo
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
divadle	divadlo	k1gNnSc6	divadlo
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
tribuna	tribuna	k1gFnSc1	tribuna
-	-	kIx~	-
sloužila	sloužit	k5eAaImAgFnS	sloužit
pro	pro	k7c4	pro
významné	významný	k2eAgFnPc4d1	významná
návštěvy	návštěva	k1gFnPc4	návštěva
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc1d1	velké
divadlo	divadlo	k1gNnSc1	divadlo
bylo	být	k5eAaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
pojmout	pojmout	k5eAaPmF	pojmout
až	až	k9	až
5	[number]	k4	5
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k6eAd1	vedle
ležící	ležící	k2eAgNnSc1d1	ležící
malé	malý	k2eAgNnSc1d1	malé
divadlo	divadlo	k1gNnSc1	divadlo
sloužilo	sloužit	k5eAaImAgNnS	sloužit
pro	pro	k7c4	pro
menší	malý	k2eAgNnSc4d2	menší
představení	představení	k1gNnSc4	představení
(	(	kIx(	(
<g/>
recitace	recitace	k1gFnSc2	recitace
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
bylo	být	k5eAaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
pojmout	pojmout	k5eAaPmF	pojmout
1	[number]	k4	1
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podobě	podoba	k1gFnSc6	podoba
z	z	k7c2	z
Augustovy	Augustův	k2eAgFnSc2d1	Augustova
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc1d1	malé
divadlo	divadlo	k1gNnSc1	divadlo
bylo	být	k5eAaImAgNnS	být
přestavěno	přestavět	k5eAaPmNgNnS	přestavět
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
80	[number]	k4	80
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Amfiteátr	amfiteátr	k1gInSc1	amfiteátr
-	-	kIx~	-
mohl	moct	k5eAaImAgInS	moct
pojmout	pojmout	k5eAaPmF	pojmout
až	až	k9	až
20	[number]	k4	20
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejstarší	starý	k2eAgInSc4d3	nejstarší
známý	známý	k2eAgInSc4d1	známý
římský	římský	k2eAgInSc4d1	římský
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
díky	díky	k7c3	díky
štědrosti	štědrost	k1gFnSc3	štědrost
C.	C.	kA	C.
Q.	Q.	kA	Q.
Valga	Valga	k1gFnSc1	Valga
a	a	k8xC	a
M.	M.	kA	M.
Porcia	Porcia	k1gFnSc1	Porcia
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velká	velký	k2eAgFnSc1d1	velká
tělocvična	tělocvična	k1gFnSc1	tělocvična
(	(	kIx(	(
<g/>
palestra	palestra	k1gFnSc1	palestra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
pro	pro	k7c4	pro
trénink	trénink	k1gInSc4	trénink
gladiátorů	gladiátor	k1gMnPc2	gladiátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
Vettiů	Vetti	k1gMnPc2	Vetti
-	-	kIx~	-
patřil	patřit	k5eAaImAgInS	patřit
rodině	rodina	k1gFnSc3	rodina
bohatého	bohatý	k2eAgMnSc2d1	bohatý
obchodníka	obchodník	k1gMnSc2	obchodník
a	a	k8xC	a
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
bohatší	bohatý	k2eAgFnSc2d2	bohatší
vrstvy	vrstva	k1gFnSc2	vrstva
pompejských	pompejský	k2eAgMnPc2d1	pompejský
občanů	občan	k1gMnPc2	občan
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
stolení	stolení	k1gNnSc2	stolení
n.	n.	k?	n.
l.	l.	k?	l.
U	u	k7c2	u
domu	dům	k1gInSc2	dům
je	být	k5eAaImIp3nS	být
zrekonstruovaná	zrekonstruovaný	k2eAgFnSc1d1	zrekonstruovaná
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
proslul	proslout	k5eAaPmAgMnS	proslout
nástěnnými	nástěnný	k2eAgFnPc7d1	nástěnná
malbami	malba	k1gFnPc7	malba
a	a	k8xC	a
dekoracemi	dekorace	k1gFnPc7	dekorace
stěn	stěna	k1gFnPc2	stěna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
explicitně	explicitně	k6eAd1	explicitně
erotické	erotický	k2eAgFnPc1d1	erotická
(	(	kIx(	(
<g/>
Priapos	Priapos	k1gMnSc1	Priapos
vážící	vážící	k2eAgInSc4d1	vážící
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
falus	falus	k1gInSc4	falus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Faunův	Faunův	k2eAgInSc1d1	Faunův
dům	dům	k1gInSc1	dům
-	-	kIx~	-
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
podle	podle	k7c2	podle
sošky	soška	k1gFnSc2	soška
Fauna	Faun	k1gMnSc2	Faun
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
architekturu	architektura	k1gFnSc4	architektura
samnitské	samnitský	k2eAgFnSc2d1	samnitská
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgNnSc7	svůj
uspořádáním	uspořádání	k1gNnSc7	uspořádání
připomíná	připomínat	k5eAaImIp3nS	připomínat
řecký	řecký	k2eAgInSc1d1	řecký
styl	styl	k1gInSc1	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místnostech	místnost	k1gFnPc6	místnost
je	být	k5eAaImIp3nS	být
mozaiková	mozaikový	k2eAgFnSc1d1	mozaiková
podlaha	podlaha	k1gFnSc1	podlaha
<g/>
,	,	kIx,	,
nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
mozaika	mozaika	k1gFnSc1	mozaika
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
bitvu	bitva	k1gFnSc4	bitva
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
s	s	k7c7	s
perským	perský	k2eAgMnSc7d1	perský
králem	král	k1gMnSc7	král
Dareiem	Dareius	k1gMnSc7	Dareius
(	(	kIx(	(
<g/>
originál	originál	k1gMnSc1	originál
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
Archeologickém	archeologický	k2eAgNnSc6d1	Archeologické
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věřejný	Věřejný	k2eAgInSc1d1	Věřejný
dům	dům	k1gInSc1	dům
-	-	kIx~	-
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
10	[number]	k4	10
místností	místnost	k1gFnPc2	místnost
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
s	s	k7c7	s
kamenným	kamenný	k2eAgNnSc7d1	kamenné
lůžkem	lůžko	k1gNnSc7	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
fresky	freska	k1gFnPc1	freska
s	s	k7c7	s
erotickou	erotický	k2eAgFnSc7d1	erotická
tematikou	tematika	k1gFnSc7	tematika
(	(	kIx(	(
<g/>
originály	originál	k1gInPc1	originál
opět	opět	k6eAd1	opět
v	v	k7c6	v
Archeologickém	archeologický	k2eAgNnSc6d1	Archeologické
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
)	)	kIx)	)
-	-	kIx~	-
např.	např.	kA	např.
Pan	Pan	k1gMnSc1	Pan
a	a	k8xC	a
Hermafrodit	hermafrodit	k1gMnSc1	hermafrodit
<g/>
,	,	kIx,	,
Priapos	Priapos	k1gMnSc1	Priapos
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
penisy	penis	k1gInPc7	penis
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
Tragického	tragický	k2eAgMnSc2d1	tragický
básníka	básník	k1gMnSc2	básník
-	-	kIx~	-
patřil	patřit	k5eAaImAgMnS	patřit
bohatému	bohatý	k2eAgMnSc3d1	bohatý
majiteli	majitel	k1gMnSc3	majitel
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
dva	dva	k4xCgInPc4	dva
obchody	obchod	k1gInPc4	obchod
ve	v	k7c6	v
vestibulu	vestibul	k1gInSc6	vestibul
<g/>
,	,	kIx,	,
proslul	proslout	k5eAaPmAgMnS	proslout
nicméně	nicméně	k8xC	nicméně
především	především	k9	především
mozaikou	mozaika	k1gFnSc7	mozaika
psa	pes	k1gMnSc2	pes
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
vestibulu	vestibul	k1gInSc2	vestibul
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
Cave	Cave	k1gNnSc2	Cave
Canem	Canem	k1gInSc1	Canem
-	-	kIx~	-
Pozor	Pozor	k1gMnSc1	Pozor
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
Venuše	Venuše	k1gFnSc2	Venuše
s	s	k7c7	s
mušlí	mušle	k1gFnSc7	mušle
-	-	kIx~	-
U	u	k7c2	u
domu	dům	k1gInSc2	dům
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
překrásná	překrásný	k2eAgFnSc1d1	překrásná
zahrada	zahrada	k1gFnSc1	zahrada
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
jižní	jižní	k2eAgFnSc6d1	jižní
zdi	zeď	k1gFnSc6	zeď
je	být	k5eAaImIp3nS	být
malba	malba	k1gFnSc1	malba
Venuše	Venuše	k1gFnSc2	Venuše
koupající	koupající	k2eAgFnSc1d1	koupající
se	se	k3xPyFc4	se
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
na	na	k7c6	na
mušli	mušle	k1gFnSc6	mušle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lázně	lázeň	k1gFnPc1	lázeň
-	-	kIx~	-
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Via	via	k7c4	via
delle	delle	k1gInSc4	delle
Terme	term	k1gInSc5	term
a	a	k8xC	a
Via	via	k7c4	via
del	del	k?	del
Foro	Foro	k1gNnSc4	Foro
<g/>
.	.	kIx.	.
</s>
<s>
Vybudovány	vybudován	k2eAgFnPc1d1	vybudována
byly	být	k5eAaImAgFnP	být
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
80	[number]	k4	80
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
době	doba	k1gFnSc6	doba
římské	římský	k2eAgFnSc2d1	římská
kolonizace	kolonizace	k1gFnSc2	kolonizace
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
do	do	k7c2	do
mužské	mužský	k2eAgFnSc2d1	mužská
a	a	k8xC	a
ženské	ženský	k2eAgFnSc2d1	ženská
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
je	být	k5eAaImIp3nS	být
převlékárna	převlékárna	k1gFnSc1	převlékárna
s	s	k7c7	s
výklenky	výklenek	k1gInPc7	výklenek
na	na	k7c4	na
uložení	uložení	k1gNnSc4	uložení
prádla	prádlo	k1gNnSc2	prádlo
<g/>
,	,	kIx,	,
studená	studený	k2eAgFnSc1d1	studená
lázeň	lázeň	k1gFnSc1	lázeň
a	a	k8xC	a
teplý	teplý	k2eAgInSc1d1	teplý
bazén	bazén	k1gInSc1	bazén
<g/>
.	.	kIx.	.
</s>
<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
též	též	k9	též
Calidarium	Calidarium	k1gNnSc1	Calidarium
-	-	kIx~	-
místnost	místnost	k1gFnSc1	místnost
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
cirkuloval	cirkulovat	k5eAaImAgInS	cirkulovat
teplý	teplý	k2eAgInSc1d1	teplý
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předměstské	předměstský	k2eAgFnPc1d1	předměstská
lázně	lázeň	k1gFnPc1	lázeň
-	-	kIx~	-
leží	ležet	k5eAaImIp3nP	ležet
hned	hned	k6eAd1	hned
u	u	k7c2	u
dnešního	dnešní	k2eAgInSc2d1	dnešní
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
před	před	k7c7	před
starověkými	starověký	k2eAgFnPc7d1	starověká
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
malovaná	malovaný	k2eAgFnSc1d1	malovaná
výzdoba	výzdoba	k1gFnSc1	výzdoba
odkládací	odkládací	k2eAgFnSc2d1	odkládací
haly	hala	k1gFnSc2	hala
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
milostných	milostný	k2eAgFnPc2d1	milostná
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
apsidě	apsida	k1gFnSc6	apsida
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
velké	velký	k2eAgNnSc1d1	velké
okno	okno	k1gNnSc1	okno
pro	pro	k7c4	pro
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
Neapolský	neapolský	k2eAgInSc4d1	neapolský
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vila	vila	k1gFnSc1	vila
mystérií	mystérium	k1gNnPc2	mystérium
-	-	kIx~	-
nejobdivovanější	obdivovaný	k2eAgInSc1d3	nejobdivovanější
dům	dům	k1gInSc1	dům
leží	ležet	k5eAaImIp3nS	ležet
mimo	mimo	k7c4	mimo
Pompeje	Pompeje	k1gFnPc4	Pompeje
<g/>
,	,	kIx,	,
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Vykopávky	vykopávka	k1gFnPc1	vykopávka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zde	zde	k6eAd1	zde
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
dosud	dosud	k6eAd1	dosud
dokončeny	dokončen	k2eAgInPc1d1	dokončen
<g/>
.	.	kIx.	.
<g/>
Vila	vila	k1gFnSc1	vila
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staší	staší	k2eAgFnSc2d1	staší
stavby	stavba	k1gFnSc2	stavba
v	v	k7c6	v
Augustově	Augustův	k2eAgFnSc6d1	Augustova
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
po	po	k7c6	po
zeměstřesení	zeměstřesení	k1gNnSc6	zeměstřesení
však	však	k9	však
byla	být	k5eAaImAgFnS	být
opravena	opravit	k5eAaPmNgFnS	opravit
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
venkovská	venkovský	k2eAgFnSc1d1	venkovská
vila	vila	k1gFnSc1	vila
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
vily	vila	k1gFnSc2	vila
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
velké	velký	k2eAgFnPc4d1	velká
malby	malba	k1gFnPc4	malba
v	v	k7c6	v
egyptském	egyptský	k2eAgInSc6d1	egyptský
stylu	styl	k1gInSc6	styl
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
egyptských	egyptský	k2eAgInPc2d1	egyptský
symbolů	symbol	k1gInPc2	symbol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malby	malba	k1gFnPc1	malba
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
zřejmě	zřejmě	k6eAd1	zřejmě
k	k	k7c3	k
některým	některý	k3yIgMnPc3	některý
z	z	k7c2	z
mystérií	mystérie	k1gFnPc2	mystérie
-	-	kIx~	-
starověkým	starověký	k2eAgInPc3d1	starověký
kultovním	kultovní	k2eAgInPc3d1	kultovní
obřadům	obřad	k1gInPc3	obřad
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
účel	účel	k1gInSc4	účel
však	však	k9	však
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známý	známý	k2eAgMnSc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
maleb	malba	k1gFnPc2	malba
mysteriálních	mysteriální	k2eAgInPc2d1	mysteriální
obřadů	obřad	k1gInPc2	obřad
jsou	být	k5eAaImIp3nP	být
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
<g/>
,	,	kIx,	,
Ariadna	Ariadna	k1gFnSc1	Ariadna
a	a	k8xC	a
Satyrové	satyr	k1gMnPc1	satyr
-	-	kIx~	-
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
mystovu	mystův	k2eAgFnSc4d1	mystův
cestu	cesta	k1gFnSc4	cesta
skrze	skrze	k?	skrze
bičování	bičování	k1gNnSc2	bičování
<g/>
,	,	kIx,	,
orgiastické	orgiastický	k2eAgInPc1d1	orgiastický
tance	tanec	k1gInPc1	tanec
až	až	k9	až
k	k	k7c3	k
mostu	most	k1gInSc2	most
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Herculaneum	Herculaneum	k1gInSc1	Herculaneum
</s>
</p>
<p>
<s>
Stabie	Stabie	k1gFnSc1	Stabie
</s>
</p>
<p>
<s>
Oplontis	Oplontis	k1gFnSc1	Oplontis
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pompeje	Pompeje	k1gFnPc1	Pompeje
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Pompeje	Pompeje	k1gFnPc1	Pompeje
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Pompejí	Pompeje	k1gFnPc2	Pompeje
</s>
</p>
<p>
<s>
Pompeje	Pompeje	k1gFnPc1	Pompeje
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
online	onlinout	k5eAaPmIp3nS	onlinout
průvodce	průvodce	k1gMnSc1	průvodce
Pompejemi	Pompeje	k1gInPc7	Pompeje
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
Herculaneum	Herculaneum	k1gInSc1	Herculaneum
<g/>
/	/	kIx~	/
<g/>
Pompeje	Pompeje	k1gFnPc1	Pompeje
<g/>
/	/	kIx~	/
<g/>
Stabie	Stabie	k1gFnPc1	Stabie
</s>
</p>
<p>
<s>
Digitalizace	digitalizace	k1gFnSc1	digitalizace
Pompejí	Pompeje	k1gFnPc2	Pompeje
</s>
</p>
<p>
<s>
Archaeological	Archaeologicat	k5eAaPmAgInS	Archaeologicat
Areas	Areas	k1gInSc1	Areas
of	of	k?	of
Pompei	Pompei	k1gNnSc1	Pompei
<g/>
,	,	kIx,	,
Herculaneum	Herculaneum	k1gInSc1	Herculaneum
and	and	k?	and
Torre	torr	k1gInSc5	torr
Annunziata	Annunziat	k1gMnSc4	Annunziat
(	(	kIx(	(
<g/>
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Překlad	překlad	k1gInSc1	překlad
Pliniových	Pliniový	k2eAgInPc2d1	Pliniový
dopisů	dopis	k1gInPc2	dopis
ohledně	ohledně	k7c2	ohledně
zániku	zánik	k1gInSc2	zánik
PompejíOdkazy	PompejíOdkaz	k1gMnPc4	PompejíOdkaz
na	na	k7c4	na
knihy	kniha	k1gFnPc4	kniha
popisující	popisující	k2eAgFnPc4d1	popisující
a	a	k8xC	a
znázorňující	znázorňující	k2eAgFnPc4d1	znázorňující
nástěnné	nástěnný	k2eAgFnPc4d1	nástěnná
malby	malba	k1gFnPc4	malba
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
digitalizovány	digitalizovat	k5eAaImNgFnP	digitalizovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
služby	služba	k1gFnSc2	služba
eBooks	eBooks	k6eAd1	eBooks
on	on	k3xPp3gInSc1	on
Demand	Demand	k1gInSc1	Demand
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
technické	technický	k2eAgFnSc6d1	technická
knihovně	knihovna	k1gFnSc6	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dipinti	Dipint	k1gMnPc1	Dipint
murali	murat	k5eAaBmAgMnP	murat
di	di	k?	di
Pompei	Pompei	k1gNnSc1	Pompei
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
</s>
</p>
<p>
<s>
Nuovi	Nuoev	k1gFnSc3	Nuoev
scavi	scaev	k1gFnSc3	scaev
di	di	k?	di
Pompei	Pompe	k1gInSc6	Pompe
<g/>
.	.	kIx.	.
</s>
<s>
Casa	Casa	k6eAd1	Casa
dei	dei	k?	dei
Vettii	Vettie	k1gFnSc4	Vettie
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
</s>
</p>
