<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
HK	HK	kA
ČR	ČR	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
zákonným	zákonný	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
podnikatelů	podnikatel	k1gMnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
usilovat	usilovat	k5eAaImF
zejména	zejména	k9
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
podnikatelského	podnikatelský	k2eAgInSc2d1
prostřední	prostřední	k2eAgFnSc4d1
v	v	k7c6
ČR	ČR	kA
i	i	k9
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
<g/>
.	.	kIx.
</s>