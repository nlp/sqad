<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Budova	budova	k1gFnSc1
HK	HK	kA
ČR	ČR	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
Zkratka	zkratka	k1gFnSc1
</s>
<s>
HK	HK	kA
ČR	ČR	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1993	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
Komora	komora	k1gFnSc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
19,99	19,99	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
7,03	7,03	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Prezident	prezident	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Vladimír	Vladimír	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Hlavní	hlavní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
</s>
<s>
Sněm	sněm	k1gInSc1
komory	komora	k1gFnSc2
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.komora.cz	www.komora.cz	k1gInSc1
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
nqab	nqab	k1gInSc1
<g/>
6	#num#	k4
<g/>
b	b	k?
IČO	IČO	kA
</s>
<s>
49279530	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
HK	HK	kA
ČR	ČR	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
zákonným	zákonný	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
podnikatelů	podnikatel	k1gMnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
usilovat	usilovat	k5eAaImF
zejména	zejména	k9
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
podnikatelského	podnikatelský	k2eAgInSc2d1
prostřední	prostřední	k2eAgFnSc4d1
v	v	k7c6
ČR	ČR	kA
i	i	k9
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
HK	HK	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
největším	veliký	k2eAgMnSc7d3
reprezentantem	reprezentant	k1gMnSc7
podnikatelské	podnikatelský	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
v	v	k7c6
ČR	ČR	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
pod	pod	k7c7
její	její	k3xOp3gFnSc7
hlavičkou	hlavička	k1gFnSc7
v	v	k7c6
červenci	červenec	k1gInSc6
2020	#num#	k4
působilo	působit	k5eAaImAgNnS
16	#num#	k4
139	#num#	k4
živnostníků	živnostník	k1gMnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
malých	malý	k2eAgFnPc2d1
<g/>
,	,	kIx,
středních	střední	k2eAgFnPc2d1
a	a	k8xC
velkých	velký	k2eAgFnPc2d1
firem	firma	k1gFnPc2
ve	v	k7c6
všech	všecek	k3xTgInPc6
oborech	obor	k1gInPc6
mimo	mimo	k7c4
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
potravinářství	potravinářství	k1gNnSc4
a	a	k8xC
lesnictví	lesnictví	k1gNnSc4
organizovaných	organizovaný	k2eAgInPc2d1
v	v	k7c6
60	#num#	k4
regionálních	regionální	k2eAgFnPc6d1
komorách	komora	k1gFnPc6
a	a	k8xC
124	#num#	k4
oborových	oborový	k2eAgFnPc6d1
asociacích	asociace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
v	v	k7c6
73	#num#	k4
městech	město	k1gNnPc6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozuje	provozovat	k5eAaImIp3nS
také	také	k9
čtyři	čtyři	k4xCgFnPc4
asistenční	asistenční	k2eAgFnPc4d1
kanceláře	kancelář	k1gFnPc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
-	-	kIx~
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Moldavsku	Moldavsko	k1gNnSc6
<g/>
,	,	kIx,
Bělorusku	Bělorusko	k1gNnSc6
a	a	k8xC
Srbsku	Srbsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Členové	člen	k1gMnPc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
60	#num#	k4
%	%	kIx~
HDP	HDP	kA
ČR	ČR	kA
a	a	k8xC
zaměstnávají	zaměstnávat	k5eAaImIp3nP
3,3	3,3	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
podle	podle	k7c2
zákona	zákon	k1gInSc2
301	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
Hospodářské	hospodářský	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
Agrární	agrární	k2eAgFnSc3d1
komoře	komora	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
statut	statut	k1gInSc1
upravuje	upravovat	k5eAaImIp3nS
základní	základní	k2eAgInPc4d1
vnitřní	vnitřní	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářskou	hospodářský	k2eAgFnSc4d1
komoru	komora	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
její	její	k3xOp3gFnSc2
složky	složka	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
zejména	zejména	k9
okresní	okresní	k2eAgFnSc2d1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
a	a	k8xC
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
obvodní	obvodní	k2eAgFnSc2d1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
resp.	resp.	kA
regionální	regionální	k2eAgFnSc2d1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
a	a	k8xC
u	u	k7c2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
začleněná	začleněný	k2eAgNnPc4d1
živnostenská	živnostenský	k2eAgNnPc4d1
společenstva	společenstvo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
zemědělství	zemědělství	k1gNnSc2
<g/>
,	,	kIx,
potravinářství	potravinářství	k1gNnSc2
a	a	k8xC
lesnictví	lesnictví	k1gNnSc2
působí	působit	k5eAaImIp3nS
Agrární	agrární	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
stejným	stejný	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
jako	jako	k8xC,k8xS
HK	HK	kA
ČR	ČR	kA
a	a	k8xC
vyvíjí	vyvíjet	k5eAaImIp3nS
podobnou	podobný	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
společným	společný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
je	být	k5eAaImIp3nS
Rozhodčí	rozhodčí	k1gMnSc1
soud	soud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
definuje	definovat	k5eAaBmIp3nS
své	svůj	k3xOyFgNnSc4
poslání	poslání	k1gNnSc4
jako	jako	k8xC,k8xS
„	„	k?
<g/>
vytvářet	vytvářet	k5eAaImF
příležitosti	příležitost	k1gFnPc4
pro	pro	k7c4
podnikání	podnikání	k1gNnSc4
<g/>
,	,	kIx,
prosazovat	prosazovat	k5eAaImF
a	a	k8xC
podporovat	podporovat	k5eAaImF
opatření	opatření	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
přispívají	přispívat	k5eAaImIp3nP
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
podnikání	podnikání	k1gNnSc2
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
k	k	k7c3
celkové	celkový	k2eAgFnSc3d1
ekonomické	ekonomický	k2eAgFnSc3d1
stabilitě	stabilita	k1gFnSc3
státu	stát	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
HK	HK	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
povinným	povinný	k2eAgNnSc7d1
připomínkovým	připomínkový	k2eAgNnSc7d1
místem	místo	k1gNnSc7
pro	pro	k7c4
podnikatelskou	podnikatelský	k2eAgFnSc4d1
legislativu	legislativa	k1gFnSc4
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
uplatnila	uplatnit	k5eAaPmAgFnS
podněty	podnět	k1gInPc4
podnikatelů	podnikatel	k1gMnPc2
k	k	k7c3
128	#num#	k4
vznikajícím	vznikající	k2eAgInPc3d1
zákonům	zákon	k1gInPc3
a	a	k8xC
novelám	novela	k1gFnPc3
<g/>
,	,	kIx,
uspěla	uspět	k5eAaPmAgFnS
v	v	k7c6
65	#num#	k4
procentech	procento	k1gNnPc6
případů	případ	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
HK	HK	kA
ČR	ČR	kA
prosazuje	prosazovat	k5eAaImIp3nS
nízké	nízký	k2eAgFnPc4d1
daně	daň	k1gFnPc4
<g/>
,	,	kIx,
efektivní	efektivní	k2eAgInSc4d1
a	a	k8xC
štíhlý	štíhlý	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
,	,	kIx,
uvolňování	uvolňování	k1gNnSc4
podnikání	podnikání	k1gNnSc2
a	a	k8xC
snižování	snižování	k1gNnSc2
byrokracie	byrokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomáhá	pomáhat	k5eAaImIp3nS
těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
nespoléhají	spoléhat	k5eNaImIp3nP
jen	jen	k9
na	na	k7c4
stát	stát	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
aktivní	aktivní	k2eAgMnPc1d1
<g/>
,	,	kIx,
vytvářejí	vytvářet	k5eAaImIp3nP
a	a	k8xC
udržují	udržovat	k5eAaImIp3nP
pracovní	pracovní	k2eAgNnPc4d1
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
zajišťují	zajišťovat	k5eAaImIp3nP
potřeby	potřeba	k1gFnPc4
občanů	občan	k1gMnPc2
a	a	k8xC
přispívají	přispívat	k5eAaImIp3nP
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
venkova	venkov	k1gInSc2
i	i	k8xC
měst	město	k1gNnPc2
a	a	k8xC
dobrému	dobrý	k2eAgNnSc3d1
jménu	jméno	k1gNnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgInPc7
kroky	krok	k1gInPc7
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
zdravý	zdravý	k2eAgInSc4d1
a	a	k8xC
rychlý	rychlý	k2eAgInSc4d1
růst	růst	k1gInSc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
prosazuje	prosazovat	k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
trh	trh	k1gInSc1
<g/>
,	,	kIx,
omezuje	omezovat	k5eAaImIp3nS
nadbytečnou	nadbytečný	k2eAgFnSc4d1
byrokracii	byrokracie	k1gFnSc4
a	a	k8xC
podporuje	podporovat	k5eAaImIp3nS
usnadňování	usnadňování	k1gNnSc1
podnikání	podnikání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
činnosti	činnost	k1gFnSc2
HK	HK	kA
ČR	ČR	kA
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
šíření	šíření	k1gNnSc1
znalostí	znalost	k1gFnPc2
a	a	k8xC
informací	informace	k1gFnPc2
o	o	k7c6
hospodářství	hospodářství	k1gNnSc6
<g/>
,	,	kIx,
ekonomických	ekonomický	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
a	a	k8xC
právních	právní	k2eAgInPc6d1
předpisech	předpis	k1gInPc6
<g/>
,	,	kIx,
týkajících	týkající	k2eAgFnPc2d1
se	se	k3xPyFc4
podnikatelských	podnikatelský	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
a	a	k8xC
hospodářských	hospodářský	k2eAgInPc2d1
styků	styk	k1gInPc2
se	se	k3xPyFc4
zahraničím	zahraničit	k5eAaPmIp1nS
</s>
<s>
poradenské	poradenský	k2eAgFnPc1d1
a	a	k8xC
konzultační	konzultační	k2eAgFnPc1d1
služby	služba	k1gFnPc1
v	v	k7c6
otázkách	otázka	k1gFnPc6
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
podnikatelskou	podnikatelský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
</s>
<s>
vzdělávací	vzdělávací	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
</s>
<s>
spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
orgány	orgán	k1gInPc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
a	a	k8xC
místních	místní	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
</s>
<s>
propagace	propagace	k1gFnSc1
a	a	k8xC
šíření	šíření	k1gNnSc1
informací	informace	k1gFnPc2
o	o	k7c6
podnikatelské	podnikatelský	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
svých	svůj	k3xOyFgMnPc2
členů	člen	k1gMnPc2
</s>
<s>
zřizování	zřizování	k1gNnSc1
zařízení	zařízení	k1gNnPc2
s	s	k7c7
institucemi	instituce	k1gFnPc7
na	na	k7c4
podporu	podpora	k1gFnSc4
rozvoje	rozvoj	k1gInSc2
podnikání	podnikání	k1gNnSc2
a	a	k8xC
vzdělanosti	vzdělanost	k1gFnSc2
<g/>
,	,	kIx,
profesního	profesní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
a	a	k8xC
rekvalifikace	rekvalifikace	k1gFnSc2
</s>
<s>
účast	účast	k1gFnSc1
na	na	k7c4
řešení	řešení	k1gNnSc4
problémů	problém	k1gInPc2
zaměstnanosti	zaměstnanost	k1gFnSc2
a	a	k8xC
na	na	k7c6
odborné	odborný	k2eAgFnSc6d1
přípravě	příprava	k1gFnSc6
k	k	k7c3
výkonu	výkon	k1gInSc3
povolání	povolání	k1gNnSc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
školských	školský	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
zřízených	zřízený	k2eAgNnPc2d1
k	k	k7c3
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
</s>
<s>
Připomínkování	připomínkování	k1gNnSc1
zákonů	zákon	k1gInPc2
</s>
<s>
HK	HK	kA
ČR	ČR	kA
ovlivňuje	ovlivňovat	k5eAaImIp3nS
legislativu	legislativa	k1gFnSc4
v	v	k7c6
ČR	ČR	kA
i	i	k9
v	v	k7c6
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členství	členství	k1gNnSc1
v	v	k7c6
HK	HK	kA
ČR	ČR	kA
umožňuje	umožňovat	k5eAaImIp3nS
podnikatelům	podnikatel	k1gMnPc3
podílet	podílet	k5eAaImF
se	se	k3xPyFc4
na	na	k7c6
vznikajících	vznikající	k2eAgInPc6d1
zákonech	zákon	k1gInPc6
<g/>
,	,	kIx,
vyhláškách	vyhláška	k1gFnPc6
<g/>
,	,	kIx,
nařízeních	nařízení	k1gNnPc6
i	i	k8xC
nové	nový	k2eAgFnSc3d1
legislativě	legislativa	k1gFnSc3
EU	EU	kA
a	a	k8xC
zasazovat	zasazovat	k5eAaImF
se	se	k3xPyFc4
tak	tak	k9
nejen	nejen	k6eAd1
o	o	k7c4
lepší	dobrý	k2eAgFnPc4d2
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
vlastní	vlastní	k2eAgFnSc4d1
podnikatelskou	podnikatelský	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
o	o	k7c6
formování	formování	k1gNnSc6
legislativního	legislativní	k2eAgInSc2d1
rámce	rámec	k1gInSc2
podnikatelského	podnikatelský	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
ČR	ČR	kA
i	i	k8xC
celé	celý	k2eAgFnSc2d1
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovlivnit	ovlivnit	k5eAaPmF
zákony	zákon	k1gInPc1
a	a	k8xC
podnikatelské	podnikatelský	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
může	moct	k5eAaImIp3nS
prostřednictvím	prostřednictvím	k7c2
HK	HK	kA
ČR	ČR	kA
kterýkoli	kterýkoli	k3yIgMnSc1
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
nespokojený	spokojený	k2eNgInSc1d1
s	s	k7c7
podmínkami	podmínka	k1gFnPc7
pro	pro	k7c4
podnikání	podnikání	k1gNnSc4
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
oboru	obor	k1gInSc6
a	a	k8xC
rád	rád	k6eAd1
by	by	kYmCp3nS
vyvinul	vyvinout	k5eAaPmAgInS
iniciativu	iniciativa	k1gFnSc4
vedoucí	vedoucí	k2eAgFnSc4d1
ke	k	k7c3
změně	změna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
HK	HK	kA
ČR	ČR	kA
shromažďuje	shromažďovat	k5eAaImIp3nS
připomínky	připomínka	k1gFnPc4
z	z	k7c2
podnikatelského	podnikatelský	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
upravuje	upravovat	k5eAaImIp3nS
za	za	k7c2
pomoci	pomoc	k1gFnSc2
odborníků	odborník	k1gMnPc2
a	a	k8xC
právních	právní	k2eAgMnPc2d1
expertů	expert	k1gMnPc2
do	do	k7c2
legislativně	legislativně	k6eAd1
přijatelné	přijatelný	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
a	a	k8xC
uplatňuje	uplatňovat	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
jak	jak	k6eAd1
v	v	k7c6
meziresortním	meziresortní	k2eAgNnSc6d1
připomínkovém	připomínkový	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
na	na	k7c6
půdě	půda	k1gFnSc6
vlády	vláda	k1gFnSc2
ČR	ČR	kA
nebo	nebo	k8xC
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zamezila	zamezit	k5eAaPmAgFnS
tvorbě	tvorba	k1gFnSc3
nadbytečné	nadbytečný	k2eAgFnSc2d1
administrativní	administrativní	k2eAgFnSc2d1
zátěže	zátěž	k1gFnSc2
či	či	k8xC
vytváření	vytváření	k1gNnSc2
nových	nový	k2eAgFnPc2d1
bariér	bariéra	k1gFnPc2
vnitřního	vnitřní	k2eAgInSc2d1
trhu	trh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
kvalifikací	kvalifikace	k1gFnPc2
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
je	být	k5eAaImIp3nS
realizátorem	realizátor	k1gMnSc7
státem	stát	k1gInSc7
garantovaného	garantovaný	k2eAgInSc2d1
celorepublikového	celorepublikový	k2eAgInSc2d1
registru	registr	k1gInSc2
profesních	profesní	k2eAgFnPc2d1
kvalifikací	kvalifikace	k1gFnPc2
Národní	národní	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
kvalifikací	kvalifikace	k1gFnPc2
(	(	kIx(
<g/>
NSK	NSK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členství	členství	k1gNnSc1
v	v	k7c6
Hospodářské	hospodářský	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
</s>
<s>
Členem	člen	k1gMnSc7
HK	HK	kA
ČR	ČR	kA
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
každá	každý	k3xTgFnSc1
právnická	právnický	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
nebo	nebo	k8xC
fyzická	fyzický	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
zapsaná	zapsaný	k2eAgFnSc1d1
či	či	k8xC
nezapsaná	zapsaný	k2eNgFnSc1d1
v	v	k7c6
obchodním	obchodní	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
vlastnící	vlastnící	k2eAgNnSc4d1
živnostenské	živnostenský	k2eAgNnSc4d1
oprávnění	oprávnění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc1
podnikání	podnikání	k1gNnSc3
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členský	členský	k2eAgInSc1d1
příspěvek	příspěvek	k1gInSc1
činí	činit	k5eAaImIp3nS
500	#num#	k4
Kč	Kč	kA
ročně	ročně	k6eAd1
pro	pro	k7c4
fyzickou	fyzický	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
5000	#num#	k4
Kč	Kč	kA
ročně	ročně	k6eAd1
hradí	hradit	k5eAaImIp3nS
právnická	právnický	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gInSc7
se	se	k3xPyFc4
podnikatel	podnikatel	k1gMnSc1
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
prostřednictvím	prostřednictvím	k7c2
okresní	okresní	k2eAgFnSc2d1
nebo	nebo	k8xC
regionální	regionální	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
anebo	anebo	k8xC
začleněného	začleněný	k2eAgNnSc2d1
živnostenského	živnostenský	k2eAgNnSc2d1
společenstva	společenstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Současná	současný	k2eAgFnSc1d1
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
činnost	činnost	k1gFnSc4
nejen	nejen	k6eAd1
předválečných	předválečný	k2eAgInPc2d1
obchodních	obchodní	k2eAgInPc2d1
<g/>
,	,	kIx,
průmyslových	průmyslový	k2eAgInPc2d1
nebo	nebo	k8xC
živnostenských	živnostenský	k2eAgInPc2d1
spolků	spolek	k1gInPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
samosprávných	samosprávný	k2eAgNnPc2d1
podnikatelských	podnikatelský	k2eAgNnPc2d1
sdružení	sdružení	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gFnSc1
historie	historie	k1gFnSc1
je	být	k5eAaImIp3nS
daleko	daleko	k6eAd1
starší	starý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Obchodní	obchodní	k2eAgFnPc1d1
a	a	k8xC
živnostenské	živnostenský	k2eAgFnPc1d1
komory	komora	k1gFnPc1
</s>
<s>
Začíná	začínat	k5eAaImIp3nS
historií	historie	k1gFnSc7
obchodních	obchodní	k2eAgFnPc2d1
a	a	k8xC
živnostenských	živnostenský	k2eAgFnPc2d1
komor	komora	k1gFnPc2
Rakouské	rakouský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
byly	být	k5eAaImAgFnP
zřízeny	zřídit	k5eAaPmNgFnP
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
122	#num#	k4
říšského	říšský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1850	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gInSc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
ustavily	ustavit	k5eAaPmAgFnP
všechny	všechen	k3xTgFnPc1
obchodní	obchodní	k2eAgFnPc1d1
komory	komora	k1gFnPc1
na	na	k7c6
celém	celý	k2eAgNnSc6d1
teritoriu	teritorium	k1gNnSc6
zemí	zem	k1gFnPc2
Rakouské	rakouský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1867	#num#	k4
také	také	k9
Rakousko-Uherska	Rakousko-Uhersek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
českého	český	k2eAgInSc2d1
státu	stát	k1gInSc2
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgInP
pražská	pražský	k2eAgFnSc1d1
<g/>
,	,	kIx,
českobudějovická	českobudějovický	k2eAgFnSc1d1
<g/>
,	,	kIx,
plzeňská	plzeňský	k2eAgFnSc1d1
<g/>
,	,	kIx,
chebská	chebský	k2eAgFnSc1d1
<g/>
,	,	kIx,
liberecká	liberecký	k2eAgFnSc1d1
<g/>
,	,	kIx,
brněnská	brněnský	k2eAgFnSc1d1
a	a	k8xC
opavská	opavský	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
a	a	k8xC
živnostenská	živnostenský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
činnosti	činnost	k1gFnSc6
pokračovaly	pokračovat	k5eAaImAgFnP
i	i	k8xC
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Cechy	cech	k1gInPc1
<g/>
,	,	kIx,
bratrstva	bratrstvo	k1gNnPc1
</s>
<s>
V	v	k7c6
živnostenské	živnostenský	k2eAgFnSc6d1
části	část	k1gFnSc6
se	se	k3xPyFc4
komora	komora	k1gFnSc1
hlásí	hlásit	k5eAaImIp3nS
k	k	k7c3
historickému	historický	k2eAgInSc3d1
odkazu	odkaz	k1gInSc3
středověkých	středověký	k2eAgInPc2d1
cechů	cech	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
kořeny	kořen	k1gInPc1
sahají	sahat	k5eAaImIp3nP
v	v	k7c6
Itálii	Itálie	k1gFnSc6
do	do	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
Francii	Francie	k1gFnSc6
do	do	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
do	do	k7c2
10	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
doby	doba	k1gFnSc2
Jana	Jan	k1gMnSc2
Lucemburského	lucemburský	k2eAgMnSc2d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
povolil	povolit	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
v	v	k7c6
Českém	český	k2eAgInSc6d1
království	království	k1gNnSc2
tři	tři	k4xCgInPc4
cechy	cech	k1gInPc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
řeznický	řeznický	k2eAgMnSc1d1
<g/>
,	,	kIx,
krejčovský	krejčovský	k2eAgInSc1d1
a	a	k8xC
zlatnický	zlatnický	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
městech	město	k1gNnPc6
českého	český	k2eAgNnSc2d1
království	království	k1gNnSc2
vznikaly	vznikat	k5eAaImAgInP
první	první	k4xOgInPc1
cechy	cech	k1gInPc1
až	až	k9
za	za	k7c2
Karla	Karel	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
od	od	k7c2
poloviny	polovina	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gNnPc1
práva	právo	k1gNnPc1
byla	být	k5eAaImAgNnP
omezená	omezený	k2eAgFnSc1d1
a	a	k8xC
musely	muset	k5eAaImAgFnP
se	se	k3xPyFc4
nazývat	nazývat	k5eAaImF
bratrstva	bratrstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastupovaly	zastupovat	k5eAaImAgInP
zájmy	zájem	k1gInPc1
jednotlivých	jednotlivý	k2eAgNnPc2d1
řemesel	řemeslo	k1gNnPc2
nebo	nebo	k8xC
sdružených	sdružený	k2eAgFnPc2d1
příbuzných	příbuzný	k2eAgFnPc2d1
profesí	profes	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nahradila	nahradit	k5eAaPmAgFnS
nevyhovující	vyhovující	k2eNgNnSc4d1
cechovní	cechovní	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
Marseille	Marseille	k1gFnSc6
roku	rok	k1gInSc2
1650	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Novodobé	novodobý	k2eAgInPc1d1
cechy	cech	k1gInPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
Hospodářské	hospodářský	k2eAgFnPc1d1
komory	komora	k1gFnPc1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sekce	sekce	k1gFnSc1
a	a	k8xC
pracovní	pracovní	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
ČR	ČR	kA
vytvořila	vytvořit	k5eAaPmAgFnS
v	v	k7c6
zemi	zem	k1gFnSc6
jedinečnou	jedinečný	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
pracovních	pracovní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
a	a	k8xC
sekcí	sekce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
vedeny	veden	k2eAgMnPc4d1
odborníky	odborník	k1gMnPc4
na	na	k7c4
danou	daný	k2eAgFnSc4d1
problematiku	problematika	k1gFnSc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
děním	dění	k1gNnSc7
nejen	nejen	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
legislativy	legislativa	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
předpisů	předpis	k1gInPc2
a	a	k8xC
celkových	celkový	k2eAgFnPc2d1
tendencí	tendence	k1gFnPc2
ve	v	k7c6
veřejné	veřejný	k2eAgFnSc6d1
diskuzi	diskuze	k1gFnSc6
utvářených	utvářený	k2eAgFnPc2d1
ke	k	k7c3
konkrétní	konkrétní	k2eAgFnSc3d1
oblasti	oblast	k1gFnSc3
podnikání	podnikání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
pracovních	pracovní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
a	a	k8xC
sekcí	sekce	k1gFnPc2
je	být	k5eAaImIp3nS
prosazovat	prosazovat	k5eAaImF
požadavky	požadavek	k1gInPc4
podnikatelů	podnikatel	k1gMnPc2
v	v	k7c6
konkrétních	konkrétní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
a	a	k8xC
přispívat	přispívat	k5eAaImF
ke	k	k7c3
zlepšování	zlepšování	k1gNnSc3
podmínek	podmínka	k1gFnPc2
pro	pro	k7c4
hospodářský	hospodářský	k2eAgInSc4d1
růst	růst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
pracovních	pracovní	k2eAgFnPc2d1
sekcí	sekce	k1gFnPc2
a	a	k8xC
skupin	skupina	k1gFnPc2
HK	HK	kA
ČR	ČR	kA
působí	působit	k5eAaImIp3nS
na	na	k7c4
800	#num#	k4
odborníků	odborník	k1gMnPc2
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
zde	zde	k6eAd1
pracovat	pracovat	k5eAaImF
i	i	k9
nečlenové	nečlen	k1gMnPc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
odborných	odborný	k2eAgFnPc2d1
sekcí	sekce	k1gFnPc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
ČR	ČR	kA
</s>
<s>
Sekce	sekce	k1gFnSc1
hospodářské	hospodářský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
</s>
<s>
Energetická	energetický	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
</s>
<s>
Sekce	sekce	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
</s>
<s>
Sekce	sekce	k1gFnSc1
pro	pro	k7c4
stavebnictví	stavebnictví	k1gNnSc4
</s>
<s>
Sekce	sekce	k1gFnSc1
pro	pro	k7c4
vědu	věda	k1gFnSc4
<g/>
,	,	kIx,
výzkum	výzkum	k1gInSc1
a	a	k8xC
inovace	inovace	k1gFnSc1
</s>
<s>
Sekce	sekce	k1gFnSc1
zahraniční	zahraniční	k2eAgFnSc2d1
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
</s>
<s>
Sekce	sekce	k1gFnSc1
pro	pro	k7c4
vzdělávání	vzdělávání	k1gNnSc4
</s>
<s>
Sekce	sekce	k1gFnSc1
zaměstnanosti	zaměstnanost	k1gFnSc2
a	a	k8xC
práce	práce	k1gFnSc2
</s>
<s>
Sekce	sekce	k1gFnSc1
pro	pro	k7c4
oblast	oblast	k1gFnSc4
zdravotnictví	zdravotnictví	k1gNnSc2
</s>
<s>
Sekce	sekce	k1gFnSc1
pro	pro	k7c4
eliminaci	eliminace	k1gFnSc4
byrokracie	byrokracie	k1gFnSc2
</s>
<s>
Sekce	sekce	k1gFnSc1
legislativy	legislativa	k1gFnSc2
</s>
<s>
Sekce	sekce	k1gFnSc1
IT	IT	kA
a	a	k8xC
telekomunikací	telekomunikace	k1gFnPc2
</s>
<s>
Živnostenská	živnostenský	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
</s>
<s>
Sekce	sekce	k1gFnSc1
pro	pro	k7c4
obchod	obchod	k1gInSc4
a	a	k8xC
služby	služba	k1gFnPc4
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
</s>
<s>
Sekce	sekce	k1gFnSc1
kvality	kvalita	k1gFnSc2
</s>
<s>
Sekce	sekce	k1gFnSc1
územního	územní	k2eAgInSc2d1
a	a	k8xC
regionálního	regionální	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
Sekce	sekce	k1gFnSc1
kulturního	kulturní	k2eAgInSc2d1
a	a	k8xC
kreativního	kreativní	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Sekce	sekce	k1gFnSc1
pro	pro	k7c4
vyhrazená	vyhrazený	k2eAgNnPc4d1
technická	technický	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
</s>
<s>
Sekce	sekce	k1gFnSc1
pro	pro	k7c4
podporu	podpora	k1gFnSc4
podnikání	podnikání	k1gNnSc2
a	a	k8xC
digitální	digitální	k2eAgFnSc4d1
agendu	agenda	k1gFnSc4
</s>
<s>
Sekce	sekce	k1gFnSc1
obranného	obranný	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
(	(	kIx(
<g/>
nově	nově	k6eAd1
zřízena	zřízen	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Oborová	oborový	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
</s>
<s>
Oborová	oborový	k2eAgFnSc1d1
část	část	k1gFnSc1
neboli	neboli	k8xC
profesní	profesní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
tvořena	tvořen	k2eAgFnSc1d1
124	#num#	k4
živnostenskými	živnostenský	k2eAgNnPc7d1
společenstvy	společenstvo	k1gNnPc7
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živnostenská	živnostenský	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
jsou	být	k5eAaImIp3nP
dle	dle	k7c2
svých	svůj	k3xOyFgInPc2
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
reprezentují	reprezentovat	k5eAaImIp3nP
<g/>
,	,	kIx,
rozděleny	rozdělit	k5eAaPmNgFnP
do	do	k7c2
tzv.	tzv.	kA
čtyř	čtyři	k4xCgFnPc2
profesních	profesní	k2eAgFnPc2d1
unií	unie	k1gFnPc2
:	:	kIx,
</s>
<s>
Obchod	obchod	k1gInSc1
a	a	k8xC
cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Průmysl	průmysl	k1gInSc1
a	a	k8xC
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Služby	služba	k1gFnPc1
pro	pro	k7c4
podnikání	podnikání	k1gNnSc4
a	a	k8xC
ostatní	ostatní	k1gNnSc4
<g/>
,	,	kIx,
</s>
<s>
Stavebnictví	stavebnictví	k1gNnSc1
<g/>
,	,	kIx,
technická	technický	k2eAgNnPc4d1
řemesla	řemeslo	k1gNnPc4
a	a	k8xC
technická	technický	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
profesní	profesní	k2eAgFnSc6d1
unii	unie	k1gFnSc6
Obchod	obchod	k1gInSc4
a	a	k8xC
cestovní	cestovní	k2eAgInSc4d1
ruch	ruch	k1gInSc4
je	být	k5eAaImIp3nS
sdruženo	sdružit	k5eAaPmNgNnS
11	#num#	k4
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
profesní	profesní	k2eAgFnSc6d1
unii	unie	k1gFnSc6
Průmysl	průmysl	k1gInSc1
a	a	k8xC
doprava	doprava	k1gFnSc1
celkem	celkem	k6eAd1
25	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
profesní	profesní	k2eAgFnSc6d1
unii	unie	k1gFnSc6
Služby	služba	k1gFnSc2
pro	pro	k7c4
podnikání	podnikání	k1gNnSc4
celkem	celkem	k6eAd1
56	#num#	k4
v	v	k7c6
profesní	profesní	k2eAgFnSc6d1
unii	unie	k1gFnSc6
Stavebnictví	stavebnictví	k1gNnSc1
<g/>
,	,	kIx,
technická	technický	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
a	a	k8xC
technická	technický	k2eAgNnPc4d1
řemesla	řemeslo	k1gNnPc4
celkem	celkem	k6eAd1
32	#num#	k4
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Profesní	profesní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Obchod	obchod	k1gInSc1
a	a	k8xC
cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
</s>
<s>
APPLiA	APPLiA	k?
CZ	CZ	kA
–	–	k?
Sdružení	sdružení	k1gNnSc1
evropských	evropský	k2eAgMnPc2d1
výrobců	výrobce	k1gMnPc2
domácích	domácí	k2eAgInPc2d1
spotřebičů	spotřebič	k1gInPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
českého	český	k2eAgInSc2d1
tradičního	tradiční	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
</s>
<s>
Asociace	asociace	k1gFnSc1
českých	český	k2eAgFnPc2d1
překladatelských	překladatelský	k2eAgFnPc2d1
agentur	agentura	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
hotelů	hotel	k1gInPc2
a	a	k8xC
restaurací	restaurace	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Asociace	asociace	k1gFnSc1
odborných	odborný	k2eAgInPc2d1
velkoobchodů	velkoobchod	k1gInPc2
a	a	k8xC
výrobců	výrobce	k1gMnPc2
TZB	TZB	kA
</s>
<s>
Asociace	asociace	k1gFnSc1
výrobců	výrobce	k1gMnPc2
a	a	k8xC
prodejců	prodejce	k1gMnPc2
zbraní	zbraň	k1gFnPc2
a	a	k8xC
střeliva	střelivo	k1gNnSc2
</s>
<s>
Sdružení	sdružení	k1gNnSc1
PROKOS-Sdružení	PROKOS-Sdružení	k1gNnSc2
výrobců	výrobce	k1gMnPc2
<g/>
,	,	kIx,
dovozců	dovozce	k1gMnPc2
<g/>
,	,	kIx,
vývozců	vývozce	k1gMnPc2
a	a	k8xC
prodejců	prodejce	k1gMnPc2
kosmetických	kosmetický	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
surovin	surovina	k1gFnPc2
pro	pro	k7c4
jejich	jejich	k3xOp3gFnSc4
výrobu	výroba	k1gFnSc4
</s>
<s>
SČS	SČS	kA
-	-	kIx~
Unie	unie	k1gFnSc1
nezávislých	závislý	k2eNgMnPc2d1
petrolejářů	petrolejář	k1gMnPc2
ČR	ČR	kA
<g/>
,	,	kIx,
z.	z.	k?
<g/>
s.	s.	k?
</s>
<s>
Svaz	svaz	k1gInSc1
českých	český	k2eAgNnPc2d1
a	a	k8xC
moravských	moravský	k2eAgNnPc2d1
spotřebních	spotřební	k2eAgNnPc2d1
družstev	družstvo	k1gNnPc2
</s>
<s>
Svaz	svaz	k1gInSc1
obchodu	obchod	k1gInSc2
a	a	k8xC
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
UNIHOST	UNIHOST	kA
-	-	kIx~
Sdružení	sdružení	k1gNnSc1
podnikatelů	podnikatel	k1gMnPc2
v	v	k7c6
pohostinství	pohostinství	k1gNnSc6
<g/>
,	,	kIx,
stravovacích	stravovací	k2eAgFnPc6d1
a	a	k8xC
ubytovacích	ubytovací	k2eAgFnPc6d1
službách	služba	k1gFnPc6
ČR	ČR	kA
</s>
<s>
Profesní	profesní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Průmysl	průmysl	k1gInSc1
a	a	k8xC
doprava	doprava	k1gFnSc1
</s>
<s>
Asociace	asociace	k1gFnSc1
akreditovaných	akreditovaný	k2eAgFnPc2d1
a	a	k8xC
autorizovaných	autorizovaný	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
českých	český	k2eAgMnPc2d1
nábytkářů	nábytkář	k1gMnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
nanotechnologického	nanotechnologický	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Asociace	asociace	k1gFnSc1
obranného	obranný	k2eAgInSc2d1
a	a	k8xC
bezpečnostního	bezpečnostní	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Asociace	asociace	k1gFnSc1
prádelen	prádelna	k1gFnPc2
a	a	k8xC
čistíren	čistírna	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
textilního-oděvního-kožedělného	textilního-oděvního-kožedělný	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Asociace	asociace	k1gFnSc1
výrobců	výrobce	k1gMnPc2
a	a	k8xC
dodavatelů	dodavatel	k1gMnPc2
zdravotnických	zdravotnický	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
výrobců	výrobce	k1gMnPc2
hudebních	hudební	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
ČR	ČR	kA
</s>
<s>
Cech	cech	k1gInSc1
KOVO	kovo	k1gNnSc1
</s>
<s>
CQS	CQS	kA
–	–	k?
Sdružení	sdružení	k1gNnSc1
pro	pro	k7c4
certifikaci	certifikace	k1gFnSc4
systémů	systém	k1gInPc2
jakosti	jakost	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
LPG	LPG	kA
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
petrolejářského	petrolejářský	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
</s>
<s>
Český	český	k2eAgInSc1d1
klavírnický	klavírnický	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
strojírenský	strojírenský	k2eAgInSc1d1
klastr	klastr	k1gInSc1
</s>
<s>
Potravinářská	potravinářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Profesní	profesní	k2eAgFnSc1d1
komora	komora	k1gFnSc1
STK	STK	kA
</s>
<s>
Sdružení	sdružení	k1gNnSc1
automobilového	automobilový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Sdružení	sdružení	k1gNnSc1
oboru	obor	k1gInSc2
vodovodů	vodovod	k1gInPc2
a	a	k8xC
kanalizací	kanalizace	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Sdružení	sdružení	k1gNnSc1
výrobců	výrobce	k1gMnPc2
kompozitů	kompozit	k1gInPc2
</s>
<s>
Sdružení	sdružení	k1gNnSc1
železničních	železniční	k2eAgMnPc2d1
nákladních	nákladní	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
ŽESNAD	ŽESNAD	kA
CZ	CZ	kA
<g/>
,	,	kIx,
z.	z.	k?
<g/>
s.	s.	k?
</s>
<s>
Svaz	svaz	k1gInSc1
českých	český	k2eAgNnPc2d1
a	a	k8xC
moravských	moravský	k2eAgNnPc2d1
výrobních	výrobní	k2eAgNnPc2d1
družstev	družstvo	k1gNnPc2
</s>
<s>
Svaz	svaz	k1gInSc1
chemického	chemický	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Svaz	svaz	k1gInSc1
plastikářského	plastikářský	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Svaz	svaz	k1gInSc1
podnikatelů	podnikatel	k1gMnPc2
pro	pro	k7c4
využití	využití	k1gNnSc4
energetických	energetický	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
</s>
<s>
Svaz	svaz	k1gInSc1
strojírenské	strojírenský	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
</s>
<s>
Profesní	profesní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Služby	služba	k1gFnSc2
pro	pro	k7c4
podnikání	podnikání	k1gNnSc4
a	a	k8xC
ostatní	ostatní	k2eAgMnPc1d1
</s>
<s>
Aliance	aliance	k1gFnSc1
Dentálních	dentální	k2eAgFnPc2d1
Korporací	korporace	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
certifikovaných	certifikovaný	k2eAgMnPc2d1
odhadců	odhadce	k1gMnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
českých	český	k2eAgMnPc2d1
herních	herní	k2eAgMnPc2d1
vývojářů	vývojář	k1gMnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
direct	directa	k1gFnPc2
marketingu	marketing	k1gInSc2
<g/>
,	,	kIx,
e-commerce	e-commerka	k1gFnSc6
a	a	k8xC
zásilkového	zásilkový	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
z.	z.	k?
<g/>
s.	s.	k?
(	(	kIx(
<g/>
ADMEZ	ADMEZ	kA
<g/>
)	)	kIx)
</s>
<s>
Asociace	asociace	k1gFnSc1
evropských	evropský	k2eAgMnPc2d1
distributorů	distributor	k1gMnPc2
léčiv	léčivo	k1gNnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
inovativního	inovativní	k2eAgInSc2d1
farmaceutického	farmaceutický	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Asociace	asociace	k1gFnSc1
komerčních	komerční	k2eAgFnPc2d1
televizí	televize	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
komunikačních	komunikační	k2eAgFnPc2d1
agentur	agentura	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Asociace	asociace	k1gFnSc1
péče	péče	k1gFnSc2
o	o	k7c4
seniory	senior	k1gMnPc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Asociace	asociace	k1gFnSc1
podnikatelů	podnikatel	k1gMnPc2
v	v	k7c6
geomatice	geomatika	k1gFnSc6
</s>
<s>
Asociace	asociace	k1gFnSc1
poskytovatelů	poskytovatel	k1gMnPc2
nebankovních	bankovní	k2eNgInPc2d1
úvěrů	úvěr	k1gInPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
poskytovatelů	poskytovatel	k1gMnPc2
personálních	personální	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
Public	publicum	k1gNnPc2
Relations	Relationsa	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
producentů	producent	k1gMnPc2
v	v	k7c6
audiovizi	audiovize	k1gFnSc6
</s>
<s>
Asociace	asociace	k1gFnSc1
pro	pro	k7c4
evropské	evropský	k2eAgInPc4d1
fondy	fond	k1gInPc4
</s>
<s>
Asociace	asociace	k1gFnSc1
pro	pro	k7c4
poradenství	poradenství	k1gNnSc4
</s>
<s>
Asociace	asociace	k1gFnSc1
provozovatelů	provozovatel	k1gMnPc2
kabelových	kabelový	k2eAgFnPc2d1
a	a	k8xC
telekomunikačních	telekomunikační	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Asociace	asociace	k1gFnSc1
renovátorů	renovátor	k1gMnPc2
tonerů	toner	k1gInPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
technických	technický	k2eAgFnPc2d1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
Grémium	grémium	k1gNnSc4
Alarm	alarm	k1gInSc1
</s>
<s>
Asociace	asociace	k1gFnSc1
zaměstnavatelů	zaměstnavatel	k1gMnPc2
zdravotně	zdravotně	k6eAd1
postižených	postižený	k1gMnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Cech	cech	k1gInSc1
mechanických	mechanický	k2eAgInPc2d1
zámkových	zámkový	k2eAgInPc2d1
systémů	systém	k1gInPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
elektronických	elektronický	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
interim	interim	k1gInSc1
managementu	management	k1gInSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
společností	společnost	k1gFnPc2
finančního	finanční	k2eAgNnSc2d1
poradenství	poradenství	k1gNnSc2
a	a	k8xC
zprostředkování	zprostředkování	k1gNnSc2
(	(	kIx(
<g/>
ČASF	ČASF	kA
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
telekomunikací	telekomunikace	k1gFnPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
úklidu	úklid	k1gInSc2
a	a	k8xC
čištění	čištění	k1gNnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
FinTech	FinTech	k1gInSc1
asociace	asociace	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
komora	komora	k1gFnSc1
detektivních	detektivní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
komora	komora	k1gFnSc1
fitness	fitness	k6eAd1
</s>
<s>
Česká	český	k2eAgFnSc1d1
komora	komora	k1gFnSc1
loterního	loterní	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
komora	komora	k1gFnSc1
služeb	služba	k1gFnPc2
ochrany	ochrana	k1gFnSc2
majetku	majetek	k1gInSc2
a	a	k8xC
osob	osoba	k1gFnPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
federace	federace	k1gFnSc2
hudebního	hudební	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
psychoenergetická	psychoenergetický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
klub	klub	k1gInSc1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
pivovarů	pivovar	k1gInPc2
a	a	k8xC
sladoven	sladovna	k1gFnPc2
</s>
<s>
Český	český	k2eAgInSc1d1
telekomunikační	telekomunikační	k2eAgInSc1d1
klastr	klastr	k1gInSc1
</s>
<s>
ICF	ICF	kA
ČR	ČR	kA
/	/	kIx~
International	International	k1gMnSc1
Coach	Coacha	k1gFnPc2
Federation	Federation	k1gInSc1
Czech	Czech	k1gMnSc1
Republic	Republice	k1gFnPc2
</s>
<s>
ICT	ICT	kA
Unie	unie	k1gFnSc1
</s>
<s>
Komora	komora	k1gFnSc1
bezpečnosti	bezpečnost	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
zdraví	zdraví	k1gNnSc2
při	při	k7c6
práci	práce	k1gFnSc6
a	a	k8xC
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Komora	komora	k1gFnSc1
odborných	odborný	k2eAgMnPc2d1
technických	technický	k2eAgMnPc2d1
kontrolorů	kontrolor	k1gMnPc2
<g/>
,	,	kIx,
výrobců	výrobce	k1gMnPc2
<g/>
,	,	kIx,
pracovníků	pracovník	k1gMnPc2
montáží	montáž	k1gFnPc2
a	a	k8xC
</s>
<s>
opravářů	opravář	k1gMnPc2
zařízení	zařízení	k1gNnSc2
hřišť	hřiště	k1gNnPc2
<g/>
,	,	kIx,
dětských	dětský	k2eAgNnPc2d1
hřišť	hřiště	k1gNnPc2
<g/>
,	,	kIx,
tělocvičen	tělocvična	k1gFnPc2
<g/>
,	,	kIx,
sportovišť	sportoviště	k1gNnPc2
a	a	k8xC
posiloven	posilovna	k1gFnPc2
SOTKVO	SOTKVO	kA
</s>
<s>
Komora	komora	k1gFnSc1
podniků	podnik	k1gInPc2
komerční	komerční	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
</s>
<s>
Komora	komora	k1gFnSc1
zaměstnavatelů	zaměstnavatel	k1gMnPc2
zdravotně	zdravotně	k6eAd1
postižených	postižený	k2eAgMnPc2d1
</s>
<s>
Profesní	profesní	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
–	–	k?
Sanitace	sanitace	k1gFnSc2
nápojových	nápojový	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
</s>
<s>
Profesní	profesní	k2eAgFnSc1d1
komora	komora	k1gFnSc1
požární	požární	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
</s>
<s>
Profesní	profesní	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
autoškol	autoškola	k1gFnPc2
a	a	k8xC
školicích	školicí	k2eAgNnPc2d1
středisek	středisko	k1gNnPc2
řidičů	řidič	k1gMnPc2
</s>
<s>
Sdružení	sdružení	k1gNnSc1
agentur	agentura	k1gFnPc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
trhu	trh	k1gInSc2
a	a	k8xC
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
SIMAR	SIMAR	kA
</s>
<s>
Sdružení	sdružení	k1gNnSc1
pracovníků	pracovník	k1gMnPc2
dezinfekce	dezinfekce	k1gFnSc2
<g/>
,	,	kIx,
dezinsekce	dezinsekce	k1gFnSc2
<g/>
,	,	kIx,
deratizace	deratizace	k1gFnSc2
</s>
<s>
Společenstvo	společenstvo	k1gNnSc1
drobného	drobný	k2eAgNnSc2d1
podnikání	podnikání	k1gNnSc2
</s>
<s>
Společenstvo	společenstvo	k1gNnSc4
organizátorů	organizátor	k1gMnPc2
veletržních	veletržní	k2eAgFnPc2d1
a	a	k8xC
výstavních	výstavní	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
</s>
<s>
Střední	střední	k2eAgInSc1d1
podnikatelský	podnikatelský	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
Svaz	svaz	k1gInSc1
českých	český	k2eAgMnPc2d1
knihkupců	knihkupec	k1gMnPc2
a	a	k8xC
nakladatelů	nakladatel	k1gMnPc2
</s>
<s>
Svaz	svaz	k1gInSc1
provozovatelů	provozovatel	k1gMnPc2
venkovní	venkovní	k2eAgFnSc2d1
reklamy	reklama	k1gFnSc2
</s>
<s>
Svaz	svaz	k1gInSc1
učitelů	učitel	k1gMnPc2
tance	tanec	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Unie	unie	k1gFnSc1
filmových	filmový	k2eAgMnPc2d1
distributorů	distributor	k1gMnPc2
</s>
<s>
Unie	unie	k1gFnSc1
kosmetiček	kosmetička	k1gFnPc2
</s>
<s>
Výbor	výbor	k1gInSc1
nezávislého	závislý	k2eNgInSc2d1
ICT	ICT	kA
průmyslu	průmysl	k1gInSc2
</s>
<s>
Profesní	profesní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Stavebnictví	stavebnictví	k1gNnSc1
<g/>
,	,	kIx,
technická	technický	k2eAgNnPc4d1
řemesla	řemeslo	k1gNnPc4
a	a	k8xC
technická	technický	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
</s>
<s>
Asociace	asociace	k1gFnSc1
bazénů	bazén	k1gInPc2
a	a	k8xC
saun	sauna	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
podniků	podnik	k1gInPc2
topenářské	topenářský	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
</s>
<s>
Asociace	asociace	k1gFnSc1
pracovníků	pracovník	k1gMnPc2
tlakových	tlakový	k2eAgMnPc2d1
zařízení	zařízení	k1gNnPc4
</s>
<s>
Asociace	asociace	k1gFnSc1
stavitelů	stavitel	k1gMnPc2
plynovodů	plynovod	k1gInPc2
a	a	k8xC
produktovodů	produktovod	k1gInPc2
</s>
<s>
Cech	cech	k1gInSc1
čalouníků	čalouník	k1gMnPc2
a	a	k8xC
dekoratérů	dekoratér	k1gMnPc2
a	a	k8xC
truhlářů	truhlář	k1gMnPc2
</s>
<s>
Cech	cech	k1gInSc1
instalatérů	instalatér	k1gMnPc2
</s>
<s>
Cech	cech	k1gInSc1
kamnářů	kamnář	k1gMnPc2
</s>
<s>
Cech	cech	k1gInSc1
klempířů	klempíř	k1gMnPc2
<g/>
,	,	kIx,
pokrývačů	pokrývač	k1gMnPc2
a	a	k8xC
tesařů	tesař	k1gMnPc2
</s>
<s>
Cech	cech	k1gInSc1
malířů	malíř	k1gMnPc2
<g/>
,	,	kIx,
lakýrníků	lakýrník	k1gMnPc2
a	a	k8xC
tapetářů	tapetář	k1gMnPc2
</s>
<s>
Cech	cech	k1gInSc1
obkladačů	obkladač	k1gMnPc2
</s>
<s>
Cech	cech	k1gInSc1
parketářů	parketář	k1gMnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Cech	cech	k1gInSc1
podlahářů	podlahář	k1gMnPc2
</s>
<s>
Cech	cech	k1gInSc1
pro	pro	k7c4
zateplování	zateplování	k1gNnSc4
budov	budova	k1gFnPc2
</s>
<s>
Cech	cech	k1gInSc1
suché	suchý	k2eAgFnSc2d1
výstavby	výstavba	k1gFnSc2
</s>
<s>
Cech	cech	k1gInSc1
topenářů	topenář	k1gMnPc2
a	a	k8xC
instalatérů	instalatér	k1gMnPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
komora	komora	k1gFnSc1
lehkých	lehký	k2eAgInPc2d1
obvodových	obvodový	k2eAgInPc2d1
plášťů	plášť	k1gInPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
svářečská	svářečský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ANB	ANB	kA
</s>
<s>
Českomoravská	českomoravský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
lešenářů	lešenář	k1gMnPc2
</s>
<s>
Český	český	k2eAgInSc1d1
plynárenský	plynárenský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Elektrotechnický	elektrotechnický	k2eAgInSc1d1
svaz	svaz	k1gInSc1
český	český	k2eAgInSc1d1
</s>
<s>
Regionální	regionální	k2eAgNnSc1d1
stavební	stavební	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
RUDOLFINEA	RUDOLFINEA	kA
–	–	k?
sdružení	sdružení	k1gNnSc1
pro	pro	k7c4
umělecká	umělecký	k2eAgNnPc4d1
řemesla	řemeslo	k1gNnPc4
</s>
<s>
Sdružení	sdružení	k1gNnSc1
klenotníků	klenotník	k1gMnPc2
a	a	k8xC
hodinářů	hodinář	k1gMnPc2
</s>
<s>
Společenstvo	společenstvo	k1gNnSc1
kominíků	kominík	k1gMnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Společenstvo	společenstvo	k1gNnSc1
techniků	technik	k1gMnPc2
zdvihacích	zdvihací	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
</s>
<s>
Společenstvo	společenstvo	k1gNnSc1
uměleckých	umělecký	k2eAgMnPc2d1
kovářů	kovář	k1gMnPc2
a	a	k8xC
zámečníků	zámečník	k1gMnPc2
<g/>
,	,	kIx,
kovářů-podkovářů	kovářů-podkovář	k1gMnPc2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
</s>
<s>
Svaz	svaz	k1gInSc1
chladící	chladící	k2eAgFnSc2d1
a	a	k8xC
klimatizační	klimatizační	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
</s>
<s>
Svaz	svaz	k1gInSc1
kameníků	kameník	k1gMnPc2
a	a	k8xC
kamenosochařů	kamenosochař	k1gMnPc2
</s>
<s>
Svaz	svaz	k1gInSc1
prodejců	prodejce	k1gMnPc2
a	a	k8xC
opravářů	opravář	k1gMnPc2
motorových	motorový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
Teplárenské	teplárenský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
</s>
<s>
Unie	unie	k1gFnSc1
výtahového	výtahový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
</s>
<s>
Živnostenská	živnostenský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
českých	český	k2eAgMnPc2d1
očních	oční	k2eAgMnPc2d1
optiků	optik	k1gMnPc2
</s>
<s>
Podnikatelské	podnikatelský	k2eAgFnPc4d1
mise	mise	k1gFnPc4
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
zahraniční	zahraniční	k2eAgNnSc1d1
zastoupení	zastoupení	k1gNnSc1
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
je	být	k5eAaImIp3nS
lídrem	lídr	k1gMnSc7
ekonomické	ekonomický	k2eAgFnSc2d1
diplomacie	diplomacie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Udržuje	udržovat	k5eAaImIp3nS
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
bohaté	bohatý	k2eAgInPc4d1
styky	styk	k1gInPc4
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yRgFnPc3,k3yIgFnPc3,k3yQgFnPc3
kompletně	kompletně	k6eAd1
organizuje	organizovat	k5eAaBmIp3nS
doprovodné	doprovodný	k2eAgFnPc4d1
mise	mise	k1gFnPc4
oficiálních	oficiální	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
státu	stát	k1gInSc2
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
uspořádáním	uspořádání	k1gNnSc7
podnikatelských	podnikatelský	k2eAgInPc2d1
seminářů	seminář	k1gInPc2
včetně	včetně	k7c2
zajištění	zajištění	k1gNnSc2
vhodných	vhodný	k2eAgMnPc2d1
partnerů	partner	k1gMnPc2
ke	k	k7c3
kontaktním	kontaktní	k2eAgNnPc3d1
jednáním	jednání	k1gNnPc3
a	a	k8xC
potenciální	potenciální	k2eAgFnSc3d1
spolupráci	spolupráce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zajišťuje	zajišťovat	k5eAaImIp3nS
neformální	formální	k2eNgNnSc1d1
setkání	setkání	k1gNnSc1
podnikatelů	podnikatel	k1gMnPc2
s	s	k7c7
představiteli	představitel	k1gMnPc7
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Organizuje	organizovat	k5eAaBmIp3nS
bilaterální	bilaterální	k2eAgNnPc4d1
podnikatelská	podnikatelský	k2eAgNnPc4d1
setkání	setkání	k1gNnPc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
bez	bez	k7c2
oficiální	oficiální	k2eAgFnSc2d1
účasti	účast	k1gFnSc2
státních	státní	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
setkání	setkání	k1gNnSc4
zabezpečuje	zabezpečovat	k5eAaImIp3nS
ve	v	k7c6
vzájemné	vzájemný	k2eAgFnSc6d1
spolupráci	spolupráce	k1gFnSc6
se	s	k7c7
zahraniční	zahraniční	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
s	s	k7c7
cílem	cíl	k1gInSc7
nalézt	nalézt	k5eAaBmF,k5eAaPmF
strategické	strategický	k2eAgMnPc4d1
partnery	partner	k1gMnPc4
pro	pro	k7c4
české	český	k2eAgMnPc4d1
podnikatele	podnikatel	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
vypravila	vypravit	k5eAaPmAgFnS
14	#num#	k4
podnikatelských	podnikatelský	k2eAgFnPc2d1
delegací	delegace	k1gFnPc2
do	do	k7c2
16	#num#	k4
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
podnikatelských	podnikatelský	k2eAgFnPc2d1
misí	mise	k1gFnPc2
firmy	firma	k1gFnSc2
podepsaly	podepsat	k5eAaPmAgInP
nové	nový	k2eAgInPc1d1
kontrakty	kontrakt	k1gInPc1
za	za	k7c4
stovky	stovka	k1gFnPc4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
v	v	k7c6
méně	málo	k6eAd2
tradičních	tradiční	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
státy	stát	k1gInPc4
Blízkého	blízký	k2eAgInSc2d1
východu	východ	k1gInSc2
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
Mongolsko	Mongolsko	k1gNnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
ICC	ICC	kA
ČR	ČR	kA
</s>
<s>
HK	HK	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
zakládajícím	zakládající	k2eAgInSc7d1
členem	člen	k1gInSc7
Národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
ICC	ICC	kA
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ICC	ICC	kA
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
na	na	k7c6
vytváření	vytváření	k1gNnSc6
mezinárodních	mezinárodní	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
jejich	jejich	k3xOp3gInSc4
dobrovolný	dobrovolný	k2eAgInSc4d1
charakter	charakter	k1gInSc4
(	(	kIx(
<g/>
nemají	mít	k5eNaImIp3nP
povahu	povaha	k1gFnSc4
zákonů	zákon	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
pravidla	pravidlo	k1gNnPc1
používána	používán	k2eAgNnPc1d1
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
styku	styk	k1gInSc6
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k6eAd1
základním	základní	k2eAgInSc7d1
pilířem	pilíř	k1gInSc7
mezinárodního	mezinárodní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
CEBRE	CEBRE	kA
-	-	kIx~
Česká	český	k2eAgFnSc1d1
podnikatelská	podnikatelský	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
při	při	k7c6
EU	EU	kA
</s>
<s>
CEBRE	CEBRE	kA
-	-	kIx~
Česká	český	k2eAgFnSc1d1
podnikatelská	podnikatelský	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
při	při	k7c6
EU	EU	kA
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
Hospodářskou	hospodářský	k2eAgFnSc7d1
komorou	komora	k1gFnSc7
ČR	ČR	kA
<g/>
,	,	kIx,
Svazem	svaz	k1gInSc7
průmyslu	průmysl	k1gInSc2
a	a	k8xC
dopravy	doprava	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
Konfederací	konfederace	k1gFnSc7
zaměstnavatelských	zaměstnavatelský	k2eAgInPc2d1
a	a	k8xC
podnikatelských	podnikatelský	k2eAgInPc2d1
svazů	svaz	k1gInPc2
ČR	ČR	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
prezentovala	prezentovat	k5eAaBmAgFnS
a	a	k8xC
hájila	hájit	k5eAaImAgFnS
české	český	k2eAgInPc4d1
podnikatelské	podnikatelský	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
před	před	k7c7
a	a	k8xC
po	po	k7c6
vstupu	vstup	k1gInSc6
ČR	ČR	kA
do	do	k7c2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
přímo	přímo	k6eAd1
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
naplnění	naplnění	k1gNnSc3
svého	svůj	k3xOyFgInSc2
cíle	cíl	k1gInSc2
CEBRE	CEBRE	kA
získalo	získat	k5eAaPmAgNnS
podporu	podpora	k1gFnSc4
Ministerstva	ministerstvo	k1gNnSc2
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
ČR	ČR	kA
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
prostřednictvím	prostřednictvím	k7c2
agentury	agentura	k1gFnSc2
CzechTrade	CzechTrad	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
přináší	přinášet	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
a	a	k8xC
pomoc	pomoc	k1gFnSc4
podnikatelům	podnikatel	k1gMnPc3
v	v	k7c6
problematice	problematika	k1gFnSc6
EU	EU	kA
<g/>
:	:	kIx,
</s>
<s>
informování	informování	k1gNnSc1
podnikatelů	podnikatel	k1gMnPc2
o	o	k7c6
legislativní	legislativní	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
EU	EU	kA
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
oboru	obor	k1gInSc6
</s>
<s>
podporu	podpora	k1gFnSc4
při	při	k7c6
získávání	získávání	k1gNnSc6
finančních	finanční	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
z	z	k7c2
komunitárních	komunitární	k2eAgInPc2d1
programů	program	k1gInPc2
EU	EU	kA
</s>
<s>
zastupování	zastupování	k1gNnSc1
českých	český	k2eAgFnPc2d1
podnikatelských	podnikatelský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
u	u	k7c2
evropských	evropský	k2eAgFnPc2d1
podnikatelských	podnikatelský	k2eAgFnPc2d1
asociací	asociace	k1gFnPc2
</s>
<s>
poskytování	poskytování	k1gNnSc1
informačních	informační	k2eAgFnPc2d1
a	a	k8xC
asistenčních	asistenční	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
českým	český	k2eAgMnPc3d1
podnikatelům	podnikatel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
členem	člen	k1gInSc7
mezinárodního	mezinárodní	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
obchodních	obchodní	k2eAgFnPc2d1
komor	komora	k1gFnPc2
Eurochambers	Eurochambersa	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zastřešuje	zastřešovat	k5eAaImIp3nS
národní	národní	k2eAgFnPc4d1
komory	komora	k1gFnPc4
ze	z	k7c2
zemí	zem	k1gFnPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
a	a	k8xC
z	z	k7c2
dalších	další	k2eAgFnPc2d1
přidružených	přidružený	k2eAgFnPc2d1
<g/>
,	,	kIx,
kandidátských	kandidátský	k2eAgFnPc2d1
a	a	k8xC
sousedních	sousední	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
EHSV	EHSV	kA
</s>
<s>
HK	HK	kA
ČR	ČR	kA
má	mít	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc2
zástupce	zástupce	k1gMnSc2
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1
hospodářském	hospodářský	k2eAgInSc6d1
a	a	k8xC
sociálním	sociální	k2eAgInSc6d1
výboru	výbor	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
dvou	dva	k4xCgInPc2
poradních	poradní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
EU	EU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Členem	člen	k1gInSc7
za	za	k7c2
HK	HK	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
od	od	k7c2
října	říjen	k1gInSc2
2015	#num#	k4
ekonom	ekonom	k1gMnSc1
Petr	Petr	k1gMnSc1
Zahradník	Zahradník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Brána	brána	k1gFnSc1
do	do	k7c2
Persie	Persie	k1gFnSc2
</s>
<s>
V	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
své	svůj	k3xOyFgFnPc4
předchozí	předchozí	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
a	a	k8xC
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
proces	proces	k1gInSc4
rušení	rušení	k1gNnSc3
sankcí	sankce	k1gFnPc2
vůči	vůči	k7c3
Íránu	Írán	k1gInSc3
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
otevřela	otevřít	k5eAaPmAgFnS
komplexní	komplexní	k2eAgInSc4d1
program	program	k1gInSc4
podpory	podpora	k1gFnSc2
pro	pro	k7c4
české	český	k2eAgFnPc4d1
firmy	firma	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
zajímají	zajímat	k5eAaImIp3nP
o	o	k7c4
rozvíjení	rozvíjení	k1gNnSc4
obchodu	obchod	k1gInSc2
s	s	k7c7
Íránem	Írán	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
zde	zde	k6eAd1
vidí	vidět	k5eAaImIp3nS
potenciál	potenciál	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
těžby	těžba	k1gFnSc2
–	–	k?
nejenom	nejenom	k6eAd1
ropy	ropa	k1gFnSc2
a	a	k8xC
plynu	plyn	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k6eAd1
uhlí	uhlí	k1gNnSc2
<g/>
,	,	kIx,
uranu	uran	k1gInSc2
<g/>
,	,	kIx,
kobaltu	kobalt	k1gInSc2
<g/>
,	,	kIx,
niklu	nikl	k1gInSc2
a	a	k8xC
zlata	zlato	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češi	Čech	k1gMnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
uplatnit	uplatnit	k5eAaPmF
se	s	k7c7
svými	svůj	k3xOyFgInPc7
stroji	stroj	k1gInPc7
<g/>
,	,	kIx,
technologiemi	technologie	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
experty	expert	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgFnPc3d1
oblastem	oblast	k1gFnPc3
patří	patřit	k5eAaImIp3nS
zdravotnictví	zdravotnictví	k1gNnSc1
nebo	nebo	k8xC
letecká	letecký	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
guvernéra	guvernér	k1gMnSc2
íránské	íránský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
Alborz	Alborza	k1gFnPc2
Seyedhamid	Seyedhamida	k1gFnPc2
Tahaeia	Tahaeium	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
Íránu	Írán	k1gInSc6
českým	český	k2eAgMnPc3d1
firmám	firma	k1gFnPc3
otevřely	otevřít	k5eAaPmAgFnP
obchodní	obchodní	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
v	v	k7c6
oblastech	oblast	k1gFnPc6
geotermální	geotermální	k2eAgFnSc2d1
energetiky	energetika	k1gFnSc2
<g/>
,	,	kIx,
stavebnictví	stavebnictví	k1gNnSc6
<g/>
,	,	kIx,
environmentálních	environmentální	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
zavlažovacích	zavlažovací	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
čističek	čistička	k1gFnPc2
odpadních	odpadní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
a	a	k8xC
automotive	automotiv	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
mise	mise	k1gFnSc2
v	v	k7c6
dubnu	duben	k1gInSc6
2016	#num#	k4
společnosti	společnost	k1gFnSc2
Energo-Pro	Energo-Pro	k1gNnSc1
získala	získat	k5eAaPmAgFnS
zakázku	zakázka	k1gFnSc4
za	za	k7c4
15	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
(	(	kIx(
<g/>
405	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c4
dodávku	dodávka	k1gFnSc4
dvou	dva	k4xCgFnPc2
vodních	vodní	k2eAgFnPc2d1
turbín	turbína	k1gFnPc2
pro	pro	k7c4
íránskou	íránský	k2eAgFnSc4d1
vodní	vodní	k2eAgFnSc4d1
elektrárnu	elektrárna	k1gFnSc4
Khoda	Khodo	k1gNnSc2
Afarin	Afarin	k1gInSc1
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opavský	opavský	k2eAgInSc1d1
výrobce	výrobce	k1gMnPc4
důlních	důlní	k2eAgInPc2d1
strojů	stroj	k1gInPc2
Ostroj	ostrojit	k5eAaPmRp2nS
podepsal	podepsat	k5eAaPmAgMnS
memorandum	memorandum	k1gNnSc1
o	o	k7c6
dodávce	dodávka	k1gFnSc6
důlního	důlní	k2eAgInSc2d1
dobývacího	dobývací	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
v	v	k7c6
řádu	řád	k1gInSc6
desítek	desítka	k1gFnPc2
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
podnikatelské	podnikatelský	k2eAgFnSc2d1
mise	mise	k1gFnSc2
konané	konaný	k2eAgFnSc2d1
v	v	k7c6
říjnu	říjen	k1gInSc6
2016	#num#	k4
firma	firma	k1gFnSc1
VA	va	k0wR
TECH	TECH	k?
WABAG	WABAG	kA
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
zabývající	zabývající	k2eAgFnSc4d1
projekcemi	projekce	k1gFnPc7
<g/>
,	,	kIx,
dodávkami	dodávka	k1gFnPc7
<g/>
,	,	kIx,
servisem	servis	k1gInSc7
a	a	k8xC
montáží	montáž	k1gFnSc7
technologických	technologický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
na	na	k7c4
úpravu	úprava	k1gFnSc4
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
setkala	setkat	k5eAaPmAgFnS
s	s	k7c7
mashadskou	mashadský	k2eAgFnSc7d1
vodárenskou	vodárenský	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
ji	on	k3xPp3gFnSc4
předložila	předložit	k5eAaPmAgFnS
šest	šest	k4xCc4
projektů	projekt	k1gInPc2
na	na	k7c4
úpravu	úprava	k1gFnSc4
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
společnost	společnost	k1gFnSc1
mohla	moct	k5eAaImAgFnS
zapojit	zapojit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nové	nový	k2eAgFnSc3d1
zakázce	zakázka	k1gFnSc3
má	mít	k5eAaImIp3nS
nakročeno	nakročen	k2eAgNnSc1d1
i	i	k8xC
Rubena	ruben	k2eAgFnSc1d1
a.s.	a.s.	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyrábí	vyrábět	k5eAaImIp3nS
v	v	k7c6
provozech	provoz	k1gInPc6
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
Náchodě	Náchod	k1gInSc6
<g/>
,	,	kIx,
Velkém	velký	k2eAgNnSc6d1
Poříčí	Poříčí	k1gNnSc6
a	a	k8xC
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
výrobky	výrobek	k1gInPc1
z	z	k7c2
technické	technický	k2eAgFnSc2d1
pryže	pryž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
VTK	VTK	kA
Special	Special	k1gInSc1
spadající	spadající	k2eAgInSc1d1
do	do	k7c2
koncernu	koncern	k1gInSc2
Vítkovice	Vítkovice	k1gInPc1
Holding	holding	k1gInSc1
jednala	jednat	k5eAaImAgFnS
v	v	k7c6
Persii	Persie	k1gFnSc6
o	o	k7c6
renovaci	renovace	k1gFnSc6
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
dodáváno	dodávat	k5eAaImNgNnS
do	do	k7c2
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
o	o	k7c6
programu	program	k1gInSc6
aplikaci	aplikace	k1gFnSc4
zelených	zelený	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
dodavatel	dodavatel	k1gMnSc1
energetických	energetický	k2eAgInPc2d1
celků	celek	k1gInPc2
Škoda	škoda	k1gFnSc1
Praha	Praha	k1gFnSc1
aktuálně	aktuálně	k6eAd1
řeší	řešit	k5eAaImIp3nS
možné	možný	k2eAgFnPc4d1
dodávky	dodávka	k1gFnPc4
elektráren	elektrárna	k1gFnPc2
formou	forma	k1gFnSc7
BOT	bota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k6eAd1
tomu	ten	k3xDgNnSc3
firmě	firma	k1gFnSc3
FOMA	FOMA	kA
Bohemia	bohemia	k1gFnSc1
se	se	k3xPyFc4
s	s	k7c7
tamním	tamní	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
podařilo	podařit	k5eAaPmAgNnS
podepsat	podepsat	k5eAaPmF
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
výhradním	výhradní	k2eAgNnSc6d1
zastoupení	zastoupení	k1gNnSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
dodávek	dodávka	k1gFnPc2
dentálních	dentální	k2eAgInPc2d1
filmů	film	k1gInPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komory	komora	k1gFnPc1
v	v	k7c6
regionech	region	k1gInPc6
</s>
<s>
HK	HK	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
jediná	jediný	k2eAgFnSc1d1
podnikatelská	podnikatelský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
disponuje	disponovat	k5eAaBmIp3nS
celorepublikovou	celorepublikový	k2eAgFnSc7d1
regionální	regionální	k2eAgFnSc7d1
sítí	síť	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
HK	HK	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
v	v	k7c6
regionální	regionální	k2eAgFnSc6d1
části	část	k1gFnSc6
14	#num#	k4
krajskými	krajský	k2eAgFnPc7d1
komorami	komora	k1gFnPc7
(	(	kIx(
<g/>
KHK	KHK	kA
<g/>
)	)	kIx)
a	a	k8xC
46	#num#	k4
okresními	okresní	k2eAgFnPc7d1
a	a	k8xC
oblastními	oblastní	k2eAgFnPc7d1
komorami	komora	k1gFnPc7
(	(	kIx(
<g/>
OHK	OHK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komory	komora	k1gFnPc1
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupit	k5eAaPmNgFnP
v	v	k7c6
73	#num#	k4
městech	město	k1gNnPc6
ČR	ČR	kA
<g/>
,	,	kIx,
tedy	tedy	k8xC
téměř	téměř	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgInPc6
okresech	okres	k1gInPc6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2
regionálních	regionální	k2eAgFnPc2d1
komor	komora	k1gFnPc2
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
poskytuje	poskytovat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
podnikatelům	podnikatel	k1gMnPc3
ve	v	k7c6
všech	všecek	k3xTgInPc6
regionech	region	k1gInPc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
zejména	zejména	k9
poradenské	poradenský	k2eAgFnPc1d1
a	a	k8xC
konzultační	konzultační	k2eAgFnPc1d1
služby	služba	k1gFnPc1
v	v	k7c6
otázkách	otázka	k1gFnPc6
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
podnikatelskou	podnikatelský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
<g/>
,	,	kIx,
vydávání	vydávání	k1gNnSc4
ověřených	ověřený	k2eAgInPc2d1
výpisů	výpis	k1gInPc2
vybraných	vybraný	k2eAgFnPc2d1
agend	agenda	k1gFnPc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
aktivity	aktivita	k1gFnSc2
směřující	směřující	k2eAgFnSc2d1
ke	k	k7c3
vzdělávání	vzdělávání	k1gNnSc3
a	a	k8xC
rozvoji	rozvoj	k1gInSc3
lidských	lidský	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
při	při	k7c6
vstupu	vstup	k1gInSc6
na	na	k7c4
zahraniční	zahraniční	k2eAgInPc4d1
trhy	trh	k1gInPc4
apod.	apod.	kA
</s>
<s>
Orgány	orgán	k1gInPc1
komory	komora	k1gFnSc2
</s>
<s>
Sněm	sněm	k1gInSc1
HK	HK	kA
ČR	ČR	kA
</s>
<s>
Sněm	sněm	k1gInSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgInSc7d3
orgánem	orgán	k1gInSc7
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
všeobecnou	všeobecný	k2eAgFnSc7d1
sněmovnou	sněmovna	k1gFnSc7
složenou	složený	k2eAgFnSc7d1
ze	z	k7c2
zástupců	zástupce	k1gMnPc2
okresních	okresní	k2eAgFnPc2d1
komor	komora	k1gFnPc2
a	a	k8xC
sněmovnou	sněmovna	k1gFnSc7
společenstev	společenstvo	k1gNnPc2
složenou	složený	k2eAgFnSc4d1
ze	z	k7c2
zástupců	zástupce	k1gMnPc2
společenstev	společenstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Působnost	působnost	k1gFnSc1
sněmu	sněm	k1gInSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
ČR	ČR	kA
<g/>
:	:	kIx,
</s>
<s>
rozhodování	rozhodování	k1gNnSc1
o	o	k7c6
statutu	statut	k1gInSc6
<g/>
,	,	kIx,
volebním	volební	k2eAgInSc6d1
a	a	k8xC
jednacím	jednací	k2eAgInSc6d1
řádu	řád	k1gInSc6
komory	komora	k1gFnSc2
a	a	k8xC
o	o	k7c6
volebním	volební	k2eAgInSc6d1
a	a	k8xC
jednacím	jednací	k2eAgInSc6d1
řádu	řád	k1gInSc6
okresních	okresní	k2eAgFnPc2d1
komor	komora	k1gFnPc2
</s>
<s>
stanovování	stanovování	k1gNnSc1
hlavních	hlavní	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
komory	komora	k1gFnSc2
</s>
<s>
volba	volba	k1gFnSc1
a	a	k8xC
odvolání	odvolání	k1gNnSc1
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
viceprezidentů	viceprezident	k1gMnPc2
<g/>
,	,	kIx,
členů	člen	k1gInPc2
představenstva	představenstvo	k1gNnSc2
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
dozorčí	dozorčí	k2eAgFnSc2d1
rady	rada	k1gFnSc2
komory	komora	k1gFnSc2
a	a	k8xC
členů	člen	k1gMnPc2
smírčí	smírčí	k2eAgFnSc2d1
komise	komise	k1gFnSc2
komory	komora	k1gFnSc2
z	z	k7c2
řad	řada	k1gFnPc2
členů	člen	k1gMnPc2
komory	komora	k1gFnSc2
</s>
<s>
schvalování	schvalování	k1gNnSc1
výše	výše	k1gFnSc2,k1gFnSc2wB
a	a	k8xC
způsobu	způsob	k1gInSc2
placení	placení	k1gNnSc2
členských	členský	k2eAgInPc2d1
příspěvků	příspěvek	k1gInPc2
a	a	k8xC
rozhodování	rozhodování	k1gNnSc2
o	o	k7c4
udělení	udělení	k1gNnSc4
výjimek	výjimek	k1gInSc1
z	z	k7c2
jejich	jejich	k3xOp3gNnSc2
placení	placení	k1gNnSc2
schvalování	schvalování	k1gNnSc2
rozpočtu	rozpočet	k1gInSc2
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
roční	roční	k2eAgFnSc2d1
účetní	účetní	k2eAgFnSc2d1
závěrky	závěrka	k1gFnSc2
a	a	k8xC
zprávy	zpráva	k1gFnSc2
představenstva	představenstvo	k1gNnSc2
komory	komora	k1gFnSc2
a	a	k8xC
dozorčí	dozorčí	k2eAgFnSc2d1
rady	rada	k1gFnSc2
komory	komora	k1gFnSc2
o	o	k7c6
činnosti	činnost	k1gFnSc6
a	a	k8xC
hospodaření	hospodaření	k1gNnSc6
komory	komora	k1gFnSc2
</s>
<s>
rozhodování	rozhodování	k1gNnSc1
o	o	k7c6
pozastaveném	pozastavený	k2eAgNnSc6d1
rozhodnutí	rozhodnutí	k1gNnSc6
představenstva	představenstvo	k1gNnSc2
komory	komora	k1gFnSc2
</s>
<s>
schvalování	schvalování	k1gNnSc1
zásad	zásada	k1gFnPc2
odměňování	odměňování	k1gNnPc2
zaměstnanců	zaměstnanec	k1gMnPc2
komory	komora	k1gFnSc2
</s>
<s>
Představenstvo	představenstvo	k1gNnSc1
HK	HK	kA
ČR	ČR	kA
</s>
<s>
Představenstvo	představenstvo	k1gNnSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
řídí	řídit	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
a	a	k8xC
rozhoduje	rozhodovat	k5eAaImIp3nS
o	o	k7c6
všech	všecek	k3xTgFnPc6
záležitostech	záležitost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představenstvo	představenstvo	k1gNnSc1
má	mít	k5eAaImIp3nS
celkem	celkem	k6eAd1
34	#num#	k4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členy	člen	k1gMnPc7
představenstva	představenstvo	k1gNnSc2
jsou	být	k5eAaImIp3nP
prezident	prezident	k1gMnSc1
<g/>
,	,	kIx,
5	#num#	k4
viceprezidentů	viceprezident	k1gMnPc2
a	a	k8xC
další	další	k2eAgMnPc1d1
členové	člen	k1gMnPc1
zvolení	zvolený	k2eAgMnPc1d1
Sněmem	sněm	k1gInSc7
HK	HK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Prezident	prezident	k1gMnSc1
a	a	k8xC
viceprezidenti	viceprezident	k1gMnPc1
HK	HK	kA
ČR	ČR	kA
</s>
<s>
Prezident	prezident	k1gMnSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
zastupuje	zastupovat	k5eAaImIp3nS
Hospodářskou	hospodářský	k2eAgFnSc4d1
komoru	komora	k1gFnSc4
navenek	navenek	k6eAd1
a	a	k8xC
jedná	jednat	k5eAaImIp3nS
jejím	její	k3xOp3gNnSc7
jménem	jméno	k1gNnSc7
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
statutárním	statutární	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
předsedou	předseda	k1gMnSc7
představenstva	představenstvo	k1gNnSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidenta	prezident	k1gMnSc2
i	i	k8xC
viceprezidenta	viceprezident	k1gMnSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
volí	volit	k5eAaImIp3nS
a	a	k8xC
odvolává	odvolávat	k5eAaImIp3nS
sněm	sněm	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
období	období	k1gNnSc6
3	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
je	být	k5eAaImIp3nS
prezidentem	prezident	k1gMnSc7
Vladimír	Vladimír	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
<g/>
,	,	kIx,
viceprezidenty	viceprezident	k1gMnPc4
jsou	být	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
Radek	Radek	k1gMnSc1
Jakubský	jakubský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Prouza	Prouza	k1gFnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Zajíček	Zajíček	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Štefl	Štefl	k1gMnSc1
a	a	k8xC
Roman	Roman	k1gMnSc1
Pommer	Pommer	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
prezidentů	prezident	k1gMnPc2
HK	HK	kA
ČR	ČR	kA
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
–	–	k?
Zdeněk	Zdeněk	k1gMnSc1
Somr	Somr	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
–	–	k?
Jaromír	Jaromír	k1gMnSc1
Drábek	Drábek	k1gMnSc1
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
–	–	k?
Petr	Petr	k1gMnSc1
Kužel	Kužel	k1gMnSc1
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
–	–	k?
Vladimír	Vladimír	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
</s>
<s>
Dozorčí	dozorčí	k2eAgMnSc1d1
rada	rada	k1gMnSc1
HK	HK	kA
ČR	ČR	kA
</s>
<s>
Dozorčí	dozorčí	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
je	být	k5eAaImIp3nS
kontrolním	kontrolní	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
odpovídá	odpovídat	k5eAaImIp3nS
Sněmu	sněm	k1gInSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
a	a	k8xC
podává	podávat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
zprávy	zpráva	k1gFnPc4
o	o	k7c6
své	svůj	k3xOyFgFnSc6
činnosti	činnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
7	#num#	k4
členů	člen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Smírčí	smírčí	k1gMnSc1
komise	komise	k1gFnSc2
HK	HK	kA
ČR	ČR	kA
</s>
<s>
Smírčí	smírčí	k2eAgFnSc1d1
komise	komise	k1gFnSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
je	být	k5eAaImIp3nS
orgánem	orgán	k1gInSc7
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
zřízen	zřízen	k2eAgInSc1d1
zejména	zejména	k9
k	k	k7c3
předcházení	předcházení	k1gNnSc2
obchodním	obchodní	k2eAgInPc3d1
sporům	spor	k1gInPc3
mezi	mezi	k7c7
členy	člen	k1gMnPc7
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smírčí	smírčí	k2eAgFnSc1d1
komise	komise	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
podnětu	podnět	k1gInSc2
člena	člen	k1gMnSc2
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
složky	složka	k1gFnSc2
komory	komora	k1gFnSc2
nebo	nebo	k8xC
představenstva	představenstvo	k1gNnSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
také	také	k9
zabývá	zabývat	k5eAaImIp3nS
řešením	řešení	k1gNnSc7
vzájemných	vzájemný	k2eAgInPc2d1
sporů	spor	k1gInPc2
mezi	mezi	k7c7
členy	člen	k1gMnPc7
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
členy	člen	k1gMnPc7
komory	komora	k1gFnSc2
a	a	k8xC
složkami	složka	k1gFnPc7
nebo	nebo	k8xC
mezi	mezi	k7c7
složkami	složka	k1gFnPc7
komory	komora	k1gFnSc2
a	a	k8xC
předkládá	předkládat	k5eAaImIp3nS
Představenstvu	představenstvo	k1gNnSc3
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
návrh	návrh	k1gInSc4
řešení	řešení	k1gNnSc4
sporu	spor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
členů	člen	k1gMnPc2
Smírčí	smírčí	k2eAgFnSc2d1
komise	komise	k1gFnSc2
stanoví	stanovit	k5eAaPmIp3nS
Sněm	sněm	k1gInSc4
při	při	k7c6
volbě	volba	k1gFnSc6
členů	člen	k1gInPc2
Smírčí	smírčí	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
počet	počet	k1gInSc1
členů	člen	k1gInPc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
lichý	lichý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Úřad	úřad	k1gInSc1
HK	HK	kA
ČR	ČR	kA
</s>
<s>
Výkonným	výkonný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
je	být	k5eAaImIp3nS
Úřad	úřad	k1gInSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřad	úřad	k1gInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
administrativu	administrativa	k1gFnSc4
a	a	k8xC
běžný	běžný	k2eAgInSc4d1
chod	chod	k1gInSc4
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
řídí	řídit	k5eAaImIp3nS
a	a	k8xC
koordinuje	koordinovat	k5eAaBmIp3nS
činnost	činnost	k1gFnSc4
komorové	komorový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
při	při	k7c6
zajišťování	zajišťování	k1gNnSc6
úkolů	úkol	k1gInPc2
schválených	schválený	k2eAgInPc2d1
sněmem	sněm	k1gInSc7
Hospodářské	hospodářský	k2eAgFnPc1d1
komory	komora	k1gFnPc1
a	a	k8xC
při	při	k7c6
zajišťování	zajišťování	k1gNnSc6
činností	činnost	k1gFnPc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
vyplývajících	vyplývající	k2eAgFnPc2d1
ze	z	k7c2
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřad	úřad	k1gInSc1
HK	HK	kA
ČR	ČR	kA
řídí	řídit	k5eAaImIp3nS
tajemník	tajemník	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Vrbík	Vrbík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
zprávy	zpráva	k1gFnSc2
auditora	auditor	k1gMnSc2
Úřadu	úřad	k1gInSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
skončilo	skončit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
její	její	k3xOp3gNnSc1
hospodaření	hospodaření	k1gNnSc1
ziskem	zisk	k1gInSc7
po	po	k7c6
zdanění	zdanění	k1gNnSc6
10	#num#	k4
067	#num#	k4
tis	tis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Dotace	dotace	k1gFnSc1
činily	činit	k5eAaImAgInP
575	#num#	k4
tis	tis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
56	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
uplatňuje	uplatňovat	k5eAaImIp3nS
systém	systém	k1gInSc1
oceňování	oceňování	k1gNnSc2
osobností	osobnost	k1gFnPc2
<g/>
,	,	kIx,
institucí	instituce	k1gFnPc2
a	a	k8xC
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
podpoře	podpora	k1gFnSc3
podnikání	podnikání	k1gNnSc2
a	a	k8xC
k	k	k7c3
činnosti	činnost	k1gFnSc3
HK	HK	kA
ČR	ČR	kA
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
již	již	k6eAd1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
nebo	nebo	k8xC
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgInPc4
účely	účel	k1gInPc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
několik	několik	k4yIc4
druhů	druh	k1gInPc2
ocenění	ocenění	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Merkurovy	Merkurův	k2eAgFnPc1d1
medaile	medaile	k1gFnPc1
</s>
<s>
Toto	tento	k3xDgNnSc1
ocenění	ocenění	k1gNnSc1
má	mít	k5eAaImIp3nS
formu	forma	k1gFnSc4
čestné	čestný	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nese	nést	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
římského	římský	k2eAgMnSc2d1
boha	bůh	k1gMnSc2
obchodu	obchod	k1gInSc2
Merkura	Merkur	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
symbolem	symbol	k1gInSc7
pro	pro	k7c4
komorové	komorový	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
a	a	k8xC
většina	většina	k1gFnSc1
komor	komora	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
včetně	včetně	k7c2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
ČR	ČR	kA
jej	on	k3xPp3gMnSc4
používá	používat	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
znaku	znak	k1gInSc6
jeho	jeho	k3xOp3gFnSc4
symboliku	symbolika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Řád	řád	k1gInSc1
Vavřínu	vavřín	k1gInSc2
</s>
<s>
Řád	řád	k1gInSc1
Vavřínu	vavřín	k1gInSc2
má	mít	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
ocenit	ocenit	k5eAaPmF
významné	významný	k2eAgFnPc4d1
domácí	domácí	k2eAgFnPc4d1
i	i	k8xC
zahraniční	zahraniční	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
pozitivně	pozitivně	k6eAd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
naši	náš	k3xOp1gFnSc4
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
jsou	být	k5eAaImIp3nP
nominovány	nominovat	k5eAaBmNgFnP
širokou	široký	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
a	a	k8xC
zástupci	zástupce	k1gMnPc7
krajských	krajský	k2eAgFnPc2d1
hospodářských	hospodářský	k2eAgFnPc2d1
komor	komora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Laureáty	laureát	k1gMnPc7
vybírá	vybírat	k5eAaImIp3nS
porota	porota	k1gFnSc1
Řádu	řád	k1gInSc2
Vavřínu	vavřín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
oceněním	ocenění	k1gNnSc7
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
vyjadřuje	vyjadřovat	k5eAaImIp3nS
uznání	uznání	k1gNnSc4
a	a	k8xC
respekt	respekt	k1gInSc4
významným	významný	k2eAgFnPc3d1
osobnostem	osobnost	k1gFnPc3
a	a	k8xC
podnikatelským	podnikatelský	k2eAgInPc3d1
subjektům	subjekt	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc1
Vavřínu	vavřín	k1gInSc2
zohledňuje	zohledňovat	k5eAaImIp3nS
především	především	k9
celoživotní	celoživotní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
společenský	společenský	k2eAgInSc4d1
dopad	dopad	k1gInSc4
<g/>
,	,	kIx,
mezinárodní	mezinárodní	k2eAgInSc4d1
nebo	nebo	k8xC
jiný	jiný	k2eAgInSc4d1
přesah	přesah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc1
je	být	k5eAaImIp3nS
udělován	udělovat	k5eAaImNgInS
také	také	k9
osobnostem	osobnost	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
hrdě	hrdě	k6eAd1
reprezentují	reprezentovat	k5eAaImIp3nP
a	a	k8xC
významně	významně	k6eAd1
podporují	podporovat	k5eAaImIp3nP
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
,	,	kIx,
či	či	k8xC
osobnostem	osobnost	k1gFnPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
vytváří	vytvářet	k5eAaImIp3nP,k5eAaPmIp3nP
nadčasové	nadčasový	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
a	a	k8xC
jejichž	jejichž	k3xOyRp3gInPc1
činy	čin	k1gInPc1
nechávají	nechávat	k5eAaImIp3nP
trvalou	trvalý	k2eAgFnSc4d1
stopu	stopa	k1gFnSc4
v	v	k7c6
dědictví	dědictví	k1gNnSc6
lidstva	lidstvo	k1gNnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
osobní	osobní	k2eAgInSc4d1
přínos	přínos	k1gInSc4
oddaně	oddaně	k6eAd1
pracují	pracovat	k5eAaImIp3nP
ve	v	k7c4
prospěch	prospěch	k1gInSc4
ostatních	ostatní	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc1
Vavřínu	vavřín	k1gInSc2
byl	být	k5eAaImAgInS
udělován	udělovat	k5eAaImNgMnS
v	v	k7c6
letech	léto	k1gNnPc6
2012	#num#	k4
-	-	kIx~
2014	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
bylo	být	k5eAaImAgNnS
udělování	udělování	k1gNnSc1
obnoveno	obnovit	k5eAaPmNgNnS
na	na	k7c6
Žofíně	Žofín	k1gInSc6
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čestný	čestný	k2eAgMnSc1d1
člen	člen	k1gMnSc1
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ve	v	k7c6
snaze	snaha	k1gFnSc6
o	o	k7c6
vytvoření	vytvoření	k1gNnSc6
nástroje	nástroj	k1gInSc2
pro	pro	k7c4
morální	morální	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
osobností	osobnost	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
významnou	významný	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
zasluhují	zasluhovat	k5eAaImIp3nP
a	a	k8xC
zasloužili	zasloužit	k5eAaPmAgMnP
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
<g/>
,	,	kIx,
význam	význam	k1gInSc4
a	a	k8xC
existenci	existence	k1gFnSc4
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
zřídila	zřídit	k5eAaPmAgFnS
titul	titul	k1gInSc4
Čestný	čestný	k2eAgMnSc1d1
člen	člen	k1gMnSc1
HK	HK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmínky	podmínka	k1gFnPc1
udělování	udělování	k1gNnSc2
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgFnP
ve	v	k7c6
Statutu	statut	k1gInSc6
čestného	čestný	k2eAgMnSc2d1
člena	člen	k1gMnSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
nejlepším	dobrý	k2eAgMnPc3d3
absolventům	absolvent	k1gMnPc3
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
ČR	ČR	kA
oceňuje	oceňovat	k5eAaImIp3nS
nejlepší	dobrý	k2eAgMnPc4d3
absolventy	absolvent	k1gMnPc4
škol	škola	k1gFnPc2
pro	pro	k7c4
oblast	oblast	k1gFnSc4
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
technických	technický	k2eAgInPc2d1
a	a	k8xC
gastronomických	gastronomický	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
akce	akce	k1gFnSc1
je	být	k5eAaImIp3nS
zvýšit	zvýšit	k5eAaPmF
u	u	k7c2
žáků	žák	k1gMnPc2
zájem	zájem	k1gInSc4
o	o	k7c4
studium	studium	k1gNnSc4
na	na	k7c6
středních	střední	k2eAgFnPc6d1
odborných	odborný	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
podpořit	podpořit	k5eAaPmF
české	český	k2eAgMnPc4d1
podnikatele	podnikatel	k1gMnPc4
v	v	k7c6
získávání	získávání	k1gNnSc6
nových	nový	k2eAgMnPc2d1
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
a	a	k8xC
řemeslné	řemeslný	k2eAgInPc4d1
cechy	cech	k1gInPc4
a	a	k8xC
asociace	asociace	k1gFnPc4
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
se	s	k7c7
školami	škola	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
se	s	k7c7
24	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
ocenění	ocenění	k1gNnSc1
konal	konat	k5eAaImAgInS
v	v	k7c6
Lichtenštejnském	lichtenštejnský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
HK	HK	kA
ČR	ČR	kA
v	v	k7c6
minulosti	minulost	k1gFnSc6
ocenila	ocenit	k5eAaPmAgFnS
již	již	k6eAd1
na	na	k7c4
7	#num#	k4
tisíc	tisíc	k4xCgInSc4
absolventů	absolvent	k1gMnPc2
<g/>
..	..	k?
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Projekty	projekt	k1gInPc1
<g/>
,	,	kIx,
kritika	kritika	k1gFnSc1
a	a	k8xC
kontroverze	kontroverze	k1gFnSc1
</s>
<s>
Právní	právní	k2eAgInSc1d1
elektronický	elektronický	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
podnikatele	podnikatel	k1gMnPc4
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
v	v	k7c6
květnu	květen	k1gInSc6
2016	#num#	k4
informovala	informovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
chystá	chystat	k5eAaImIp3nS
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
podnikatele	podnikatel	k1gMnPc4
upozorňovat	upozorňovat	k5eAaImF
na	na	k7c4
všechny	všechen	k3xTgFnPc4
jeho	jeho	k3xOp3gFnPc4
povinnosti	povinnost	k1gFnPc4
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iniciativa	iniciativa	k1gFnSc1
HK	HK	kA
ČR	ČR	kA
vyústila	vyústit	k5eAaPmAgFnS
v	v	k7c4
návrh	návrh	k1gInSc4
poslanců	poslanec	k1gMnPc2
Zbyňka	Zbyněk	k1gMnSc4
Stanjury	Stanjura	k1gFnSc2
<g/>
,	,	kIx,
Andreje	Andrej	k1gMnSc2
Babiše	Babiš	k1gMnSc2
<g/>
,	,	kIx,
Martina	Martin	k1gMnSc2
Plíška	plíška	k1gFnSc1
<g/>
,	,	kIx,
Petra	Petr	k1gMnSc2
Gazdíka	Gazdík	k1gMnSc2
<g/>
,	,	kIx,
Pavla	Pavel	k1gMnSc2
Bělobrádka	Bělobrádka	k1gFnSc1
<g/>
,	,	kIx,
Romana	Roman	k1gMnSc2
Sklenáka	Sklenák	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
Kováčika	Kováčik	k1gMnSc2
na	na	k7c4
vydání	vydání	k1gNnSc4
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
zákon	zákon	k1gInSc1
č.	č.	k?
301	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hospodářské	hospodářský	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
Agrární	agrární	k2eAgFnSc3d1
komoře	komora	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatou	podstata	k1gFnSc7
návrhu	návrh	k1gInSc2
byla	být	k5eAaImAgFnS
nezbytnost	nezbytnost	k1gFnSc1
vydávat	vydávat	k5eAaImF,k5eAaPmF
každý	každý	k3xTgInSc4
právní	právní	k2eAgInSc4d1
předpis	předpis	k1gInSc4
<g/>
,	,	kIx,
upravující	upravující	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
podnikatelů	podnikatel	k1gMnPc2
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
přílohou	příloha	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
by	by	kYmCp3nS
tvořil	tvořit	k5eAaImAgMnS
strukturovaný	strukturovaný	k2eAgInSc4d1
přehled	přehled	k1gInSc4
těchto	tento	k3xDgFnPc2
povinností	povinnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
stanoveno	stanovit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
spravuje	spravovat	k5eAaImIp3nS
(	(	kIx(
<g/>
neexkluzívní	exkluzívní	k2eNgInSc4d1
<g/>
)	)	kIx)
informační	informační	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
budou	být	k5eAaImBp3nP
tyto	tento	k3xDgFnPc1
přílohy	příloha	k1gFnPc1
názorně	názorně	k6eAd1
prezentovány	prezentovat	k5eAaBmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušný	příslušný	k2eAgInSc1d1
sněmovní	sněmovní	k2eAgInSc1d1
tisk	tisk	k1gInSc1
1089	#num#	k4
byl	být	k5eAaImAgInS
doručen	doručit	k5eAaPmNgInS
poslancům	poslanec	k1gMnPc3
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Mezirezortní	mezirezortní	k2eAgNnSc1d1
připomínkové	připomínkový	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
k	k	k7c3
návrhu	návrh	k1gInSc3
bylo	být	k5eAaImAgNnS
za	za	k7c2
účasti	účast	k1gFnSc2
19	#num#	k4
připomínkových	připomínkový	k2eAgNnPc2d1
míst	místo	k1gNnPc2
ukončeno	ukončit	k5eAaPmNgNnS
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
na	na	k7c6
své	svůj	k3xOyFgFnSc6
schůzi	schůze	k1gFnSc6
návrh	návrh	k1gInSc4
projednala	projednat	k5eAaPmAgFnS
s	s	k7c7
neutrálním	neutrální	k2eAgNnSc7d1
stanoviskem	stanovisko	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Sněmovna	sněmovna	k1gFnSc1
dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2017	#num#	k4
schválila	schválit	k5eAaPmAgFnS
tisk	tisk	k1gInSc1
1089	#num#	k4
s	s	k7c7
legislativně	legislativně	k6eAd1
technickými	technický	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
47	#num#	k4
hlasy	hlas	k1gInPc7
z	z	k7c2
81	#num#	k4
přítomných	přítomný	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Senátu	senát	k1gInSc6
byl	být	k5eAaImAgInS
návrh	návrh	k1gInSc1
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
42	#num#	k4
hlasy	hlas	k1gInPc7
z	z	k7c2
53	#num#	k4
přítomných	přítomný	k2eAgMnPc2d1
senátorů	senátor	k1gMnPc2
zamítnut	zamítnout	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
razantním	razantní	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
návrhu	návrh	k1gInSc2
se	se	k3xPyFc4
přes	přes	k7c4
obecný	obecný	k2eAgInSc4d1
respekt	respekt	k1gInSc4
k	k	k7c3
záměru	záměr	k1gInSc3
předkladatelů	předkladatel	k1gMnPc2
zpřehlednit	zpřehlednit	k5eAaPmF
obecný	obecný	k2eAgInSc4d1
právní	právní	k2eAgInSc4d1
rámec	rámec	k1gInSc4
podnikání	podnikání	k1gNnSc2
vedla	vést	k5eAaImAgFnS
odborná	odborný	k2eAgFnSc1d1
debata	debata	k1gFnSc1
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
možné	možný	k2eAgFnSc6d1
kolizi	kolize	k1gFnSc6
s	s	k7c7
tradičními	tradiční	k2eAgInPc7d1
legislativními	legislativní	k2eAgInPc7d1
postupy	postup	k1gInPc7
a	a	k8xC
některými	některý	k3yIgInPc7
ústavními	ústavní	k2eAgInPc7d1
principy	princip	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nehledě	hledět	k5eNaImSgMnS
na	na	k7c4
finální	finální	k2eAgNnSc4d1
zamítnutí	zamítnutí	k1gNnSc4
návrh	návrh	k1gInSc4
vyvolal	vyvolat	k5eAaPmAgInS
zájem	zájem	k1gInSc1
odborné	odborný	k2eAgFnSc2d1
i	i	k8xC
laické	laický	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
o	o	k7c4
snížení	snížení	k1gNnSc4
regulatorní	regulatorní	k2eAgFnSc2d1
zátěže	zátěž	k1gFnSc2
podnikatelů	podnikatel	k1gMnPc2
i	i	k8xC
obecně	obecně	k6eAd1
o	o	k7c4
zpřehlednění	zpřehlednění	k1gNnSc4
a	a	k8xC
větší	veliký	k2eAgFnSc4d2
srozumitelnost	srozumitelnost	k1gFnSc4
právních	právní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
jejich	jejich	k3xOp3gMnPc4
adresáty	adresát	k1gMnPc4
a	a	k8xC
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
vývoji	vývoj	k1gInSc6
právního	právní	k2eAgInSc2d1
elektronického	elektronický	k2eAgInSc2d1
systému	systém	k1gInSc2
i	i	k8xC
bez	bez	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
legislativního	legislativní	k2eAgNnSc2d1
ukotvení	ukotvení	k1gNnSc2
jako	jako	k8xC,k8xS
prozatím	prozatím	k6eAd1
nezávazné	závazný	k2eNgFnPc4d1
pomůcky	pomůcka	k1gFnPc4
pro	pro	k7c4
podnikatele	podnikatel	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Petice	petice	k1gFnSc1
zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
podepsalo	podepsat	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
Úřadu	úřad	k1gInSc2
komory	komora	k1gFnSc2
petici	petice	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
kritizuje	kritizovat	k5eAaImIp3nS
prezidenta	prezident	k1gMnSc4
komory	komora	k1gFnSc2
Petra	Petr	k1gMnSc4
Kužela	Kužel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytýká	vytýkat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
porušování	porušování	k1gNnSc1
zákonů	zákon	k1gInPc2
<g/>
,	,	kIx,
porušování	porušování	k1gNnSc3
interních	interní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
špatné	špatný	k2eAgNnSc1d1
hospodaření	hospodaření	k1gNnSc1
<g/>
,	,	kIx,
šikanování	šikanování	k1gNnSc1
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
,	,	kIx,
nevhodné	vhodný	k2eNgNnSc4d1
a	a	k8xC
neslušné	slušný	k2eNgNnSc4d1
chování	chování	k1gNnSc4
a	a	k8xC
špatnou	špatný	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
komory	komora	k1gFnSc2
navenek	navenek	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
vyjádřili	vyjádřit	k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
přesvědčení	přesvědčení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Kužel	Kužel	k1gMnSc1
upřednostňuje	upřednostňovat	k5eAaImIp3nS
budování	budování	k1gNnSc4
své	svůj	k3xOyFgFnSc2
politické	politický	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
na	na	k7c4
úkor	úkor	k1gInSc4
podpory	podpora	k1gFnSc2
dobrého	dobrý	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
HK	HK	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Představenstvo	představenstvo	k1gNnSc1
kritiku	kritika	k1gFnSc4
odmítlo	odmítnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Elektronická	elektronický	k2eAgFnSc1d1
karta	karta	k1gFnSc1
pracovníka	pracovník	k1gMnSc2
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2012	#num#	k4
navrhla	navrhnout	k5eAaPmAgFnS
HK	HK	kA
ČR	ČR	kA
zavedení	zavedení	k1gNnPc4
elektronické	elektronický	k2eAgFnSc2d1
karty	karta	k1gFnSc2
pracovníka	pracovník	k1gMnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
zaznamenán	zaznamenat	k5eAaPmNgInS
průběh	průběh	k1gInSc1
zaměstnání	zaměstnání	k1gNnSc2
<g/>
,	,	kIx,
také	také	k9
zda	zda	k8xS
má	mít	k5eAaImIp3nS
daný	daný	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
v	v	k7c6
pořádku	pořádek	k1gInSc6
lékařské	lékařský	k2eAgFnSc2d1
prohlídky	prohlídka	k1gFnSc2
<g/>
,	,	kIx,
či	či	k8xC
zdali	zdali	k8xS
je	být	k5eAaImIp3nS
řádně	řádně	k6eAd1
proškolen	proškolen	k2eAgInSc1d1
a	a	k8xC
to	ten	k3xDgNnSc1
včetně	včetně	k7c2
bezpečnosti	bezpečnost	k1gFnSc2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektronická	elektronický	k2eAgFnSc1d1
karta	karta	k1gFnSc1
by	by	kYmCp3nS
rovněž	rovněž	k9
evidovala	evidovat	k5eAaImAgFnS
<g/>
,	,	kIx,
od	od	k7c2
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
pracovník	pracovník	k1gMnSc1
zaměstnán	zaměstnat	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
karta	karta	k1gFnSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
povinná	povinný	k2eAgFnSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
pracující	pracující	k1gMnPc4
<g/>
,	,	kIx,
tedy	tedy	k9
asi	asi	k9
pět	pět	k4xCc1
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Náklady	náklad	k1gInPc4
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
nést	nést	k5eAaImF
stát	stát	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
návrh	návrh	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
terčem	terč	k1gInSc7
kritiky	kritika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Lidové	lidový	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
přirovnávaly	přirovnávat	k5eAaImAgFnP
kartu	karta	k1gFnSc4
k	k	k7c3
drahým	drahý	k2eAgInPc3d1
a	a	k8xC
nefunkčním	funkční	k2eNgInPc3d1
projektům	projekt	k1gInPc3
Opencard	Opencard	k1gInSc1
<g/>
,	,	kIx,
sKarta	sKarta	k1gFnSc1
či	či	k8xC
elektronická	elektronický	k2eAgFnSc1d1
zdravotní	zdravotní	k2eAgFnSc1d1
knížka	knížka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInPc1d1
kritické	kritický	k2eAgInPc1d1
hlasy	hlas	k1gInPc1
považovaly	považovat	k5eAaImAgInP
návrh	návrh	k1gInSc4
za	za	k7c4
zásah	zásah	k1gInSc4
do	do	k7c2
osobní	osobní	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
zbytečnou	zbytečný	k2eAgFnSc7d1
byrokracii	byrokracie	k1gFnSc4
<g/>
,	,	kIx,
nadměrné	nadměrný	k2eAgInPc4d1
výdaje	výdaj	k1gInPc4
pro	pro	k7c4
stát	stát	k1gInSc4
či	či	k8xC
projev	projev	k1gInSc4
korporativismu	korporativismus	k1gInSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Ministryně	ministryně	k1gFnSc2
práce	práce	k1gFnSc2
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
Ludmila	Ludmila	k1gFnSc1
Müllerová	Müllerová	k1gFnSc1
(	(	kIx(
<g/>
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
návrh	návrh	k1gInSc1
označila	označit	k5eAaPmAgFnS
za	za	k7c4
dobrý	dobrý	k2eAgInSc4d1
nápad	nápad	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
ODS	ODS	kA
se	se	k3xPyFc4
od	od	k7c2
návrhu	návrh	k1gInSc2
distancovala	distancovat	k5eAaBmAgFnS
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Úmysl	úmysl	k1gInSc4
zavést	zavést	k5eAaPmF
karty	karta	k1gFnPc4
kritizoval	kritizovat	k5eAaImAgMnS
i	i	k9
bývalý	bývalý	k2eAgMnSc1d1
evropský	evropský	k2eAgMnSc1d1
komisař	komisař	k1gMnSc1
pro	pro	k7c4
zaměstnanost	zaměstnanost	k1gFnSc4
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnPc4d1
věci	věc	k1gFnPc4
a	a	k8xC
rovné	rovný	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
Vladimír	Vladimír	k1gMnSc1
Špidla	Špidla	k1gMnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
"	"	kIx"
<g/>
Elektronická	elektronický	k2eAgFnSc1d1
karta	karta	k1gFnSc1
a	a	k8xC
pracovní	pracovní	k2eAgFnSc1d1
knížka	knížka	k1gFnSc1
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
jakékoliv	jakýkoliv	k3yIgFnSc6
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
sloužila	sloužit	k5eAaImAgFnS
a	a	k8xC
byla	být	k5eAaImAgFnS
zneužívána	zneužíván	k2eAgFnSc1d1
k	k	k7c3
brutálnímu	brutální	k2eAgInSc3d1
nátlaku	nátlak	k1gInSc3
na	na	k7c4
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Zavedení	zavedení	k1gNnSc2
karty	karta	k1gFnSc2
odmítly	odmítnout	k5eAaPmAgInP
i	i	k9
odbory	odbor	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podvody	podvod	k1gInPc1
při	při	k7c6
získání	získání	k1gNnSc6
dotací	dotace	k1gFnPc2
EU	EU	kA
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2014	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
Evropský	evropský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
podvodům	podvod	k1gInPc3
(	(	kIx(
<g/>
OLAF	OLAF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
ČR	ČR	kA
falšovala	falšovat	k5eAaImAgFnS
údaje	údaj	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
čerpala	čerpat	k5eAaImAgFnS
peníze	peníz	k1gInPc4
z	z	k7c2
fondů	fond	k1gInPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
falšování	falšování	k1gNnSc6
podpisů	podpis	k1gInPc2
a	a	k8xC
čestných	čestný	k2eAgNnPc2d1
prohlášení	prohlášení	k1gNnPc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
údajně	údajně	k6eAd1
měli	mít	k5eAaImAgMnP
pracovat	pracovat	k5eAaImF
na	na	k7c6
projektu	projekt	k1gInSc6
kontroly	kontrola	k1gFnSc2
čerpání	čerpání	k1gNnSc2
peněz	peníze	k1gInPc2
z	z	k7c2
Operačního	operační	k2eAgInSc2d1
programu	program	k1gInSc2
Podnikání	podnikání	k1gNnSc1
a	a	k8xC
inovace	inovace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
OLAF	OLAF	kA
celou	celý	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
postoupil	postoupit	k5eAaPmAgMnS
českým	český	k2eAgMnSc7d1
orgánům	orgán	k1gInPc3
činným	činný	k2eAgInPc3d1
v	v	k7c6
trestním	trestní	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
ČR	ČR	kA
navíc	navíc	k6eAd1
hrozí	hrozit	k5eAaImIp3nS
pokuta	pokuta	k1gFnSc1
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
stovek	stovka	k1gFnPc2
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Případ	případ	k1gInSc1
bývalého	bývalý	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
Petra	Petr	k1gMnSc4
Kužela	Kužel	k1gMnSc4
ale	ale	k8xC
Městský	městský	k2eAgInSc4d1
soud	soud	k1gInSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
již	již	k6eAd1
potřetí	potřetí	k4xO
vrátil	vrátit	k5eAaPmAgMnS
k	k	k7c3
novému	nový	k2eAgNnSc3d1
rozhodnutí	rozhodnutí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudy	soud	k1gInPc4
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
Kužela	Kužel	k1gMnSc2
poslaly	poslat	k5eAaPmAgFnP
podmíněně	podmíněně	k6eAd1
na	na	k7c4
rok	rok	k1gInSc4
do	do	k7c2
vězení	vězení	k1gNnSc2
a	a	k8xC
přikázaly	přikázat	k5eAaPmAgFnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ministerstvu	ministerstvo	k1gNnSc6
zahraničí	zahraničí	k1gNnSc2
uhradil	uhradit	k5eAaPmAgInS
škodu	škoda	k1gFnSc4
480	#num#	k4
tisíc	tisíc	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kužela	Kužel	k1gMnSc4
se	se	k3xPyFc4
ale	ale	k9
následně	následně	k6eAd1
zastal	zastat	k5eAaPmAgInS
Nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
odsuzující	odsuzující	k2eAgInPc4d1
verdikty	verdikt	k1gInPc4
zrušil	zrušit	k5eAaPmAgInS
<g/>
,	,	kIx,
a	a	k8xC
nařídil	nařídit	k5eAaPmAgMnS
prvoinstančnímu	prvoinstanční	k2eAgMnSc3d1
soudu	soud	k1gInSc2
případ	případ	k1gInSc4
znovu	znovu	k6eAd1
projednat	projednat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napodruhé	napodruhé	k6eAd1
soud	soud	k1gInSc1
Kužela	Kužel	k1gMnSc2
osvobodil	osvobodit	k5eAaPmAgInS
<g/>
,	,	kIx,
ale	ale	k8xC
Městský	městský	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
tento	tento	k3xDgInSc4
i	i	k8xC
verdikt	verdikt	k1gInSc4
zrušil	zrušit	k5eAaPmAgMnS
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
případ	případ	k1gInSc4
k	k	k7c3
novému	nový	k2eAgNnSc3d1
projednání	projednání	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
říjnu	říjen	k1gInSc6
2019	#num#	k4
byl	být	k5eAaImAgMnS
Petr	Petr	k1gMnSc1
Kužel	Kužel	k1gMnSc1
Městským	městský	k2eAgInSc7d1
soudem	soud	k1gInSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
definitivně	definitivně	k6eAd1
zproštěn	zproštěn	k2eAgMnSc1d1
obžaloby	obžaloba	k1gFnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFO@AION.CZ	INFO@AION.CZ	k1gMnSc1
<g/>
,	,	kIx,
AION	AION	kA
CS	CS	kA
-	-	kIx~
<g/>
.	.	kIx.
301	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Zákon	zákon	k1gInSc1
o	o	k7c6
Hospodářské	hospodářský	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
ČR	ČR	kA
a	a	k8xC
Agrární	agrární	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákony	zákon	k1gInPc1
pro	pro	k7c4
lidi	člověk	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vladimír	Vladimír	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
bude	být	k5eAaImBp3nS
znovu	znovu	k6eAd1
prezidentem	prezident	k1gMnSc7
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
TÝDEN	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Zrušení	zrušení	k1gNnSc4
karenční	karenční	k2eAgFnSc2d1
doby	doba	k1gFnSc2
nahrálo	nahrát	k5eAaPmAgNnS,k5eAaBmAgNnS
simulantům	simulant	k1gMnPc3
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
komora	komora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
<g/>
,	,	kIx,
hájí	hájit	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
Maláčová	Maláčová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-28	2020-07-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
FINTAG	FINTAG	kA
<g/>
.	.	kIx.
fintag	fintag	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-30	2020-07-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BusinessInfo	BusinessInfo	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WWW.MEDIACENTRIK.CZ	WWW.MEDIACENTRIK.CZ	k1gMnSc1
<g/>
,	,	kIx,
CMS	CMS	kA
<g/>
:	:	kIx,
NetDirect	NetDirect	k1gMnSc1
MediaCentrik	MediaCentrik	k1gMnSc1
<g/>
;	;	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
-	-	kIx~
HKCR	HKCR	kA
|	|	kIx~
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
www.komora.cz	www.komora.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BOHUTÍNSKÁ	BOHUTÍNSKÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
5	#num#	k4
týdnů	týden	k1gInPc2
dovolené	dovolená	k1gFnSc2
povinně	povinně	k6eAd1
i	i	k9
ve	v	k7c6
firmách	firma	k1gFnPc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
Čtěte	číst	k5eAaImRp2nP
podrobnosti	podrobnost	k1gFnPc4
v	v	k7c6
otázkách	otázka	k1gFnPc6
a	a	k8xC
odpovědích	odpověď	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnikatel	podnikatel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WWW.MEDIACENTRIK.CZ	WWW.MEDIACENTRIK.CZ	k1gMnSc1
<g/>
,	,	kIx,
CMS	CMS	kA
<g/>
:	:	kIx,
NetDirect	NetDirect	k1gMnSc1
MediaCentrik	MediaCentrik	k1gMnSc1
<g/>
;	;	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmínky	podmínka	k1gFnPc1
a	a	k8xC
výhody	výhoda	k1gFnPc1
členství	členství	k1gNnSc2
-	-	kIx~
HKCR	HKCR	kA
|	|	kIx~
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
www.komora.cz	www.komora.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WWW.MEDIACENTRIK.CZ	WWW.MEDIACENTRIK.CZ	k1gMnSc1
<g/>
,	,	kIx,
CMS	CMS	kA
<g/>
:	:	kIx,
NetDirect	NetDirect	k1gMnSc1
MediaCentrik	MediaCentrik	k1gMnSc1
<g/>
;	;	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
obchodních	obchodní	k2eAgInPc2d1
a	a	k8xC
živnostenských	živnostenský	k2eAgFnPc2d1
komor	komora	k1gFnPc2
-	-	kIx~
HKCR	HKCR	kA
|	|	kIx~
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
www.komora.cz	www.komora.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zikmund	Zikmund	k1gMnSc1
Winter	Winter	k1gMnSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
řemesel	řemeslo	k1gNnPc2
a	a	k8xC
obchodu	obchod	k1gInSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
ve	v	k7c6
XIV	XIV	kA
<g/>
.	.	kIx.
a	a	k8xC
XV	XV	kA
<g/>
.	.	kIx.
století	století	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1912	#num#	k4
<g/>
↑	↑	k?
Josef	Josef	k1gMnSc1
Gruber	Gruber	k1gMnSc1
<g/>
:	:	kIx,
Obchodní	obchodní	k2eAgFnSc1d1
a	a	k8xC
živnostenská	živnostenský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
prvním	první	k4xOgNnSc6
půlstoletí	půlstoletí	k1gNnSc6
svého	svůj	k3xOyFgNnSc2
trvání	trvání	k1gNnSc2
1850	#num#	k4
<g/>
-	-	kIx~
<g/>
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1900	#num#	k4
<g/>
,	,	kIx,
dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
↑	↑	k?
WWW.MEDIACENTRIK.CZ	WWW.MEDIACENTRIK.CZ	k1gMnSc1
<g/>
,	,	kIx,
CMS	CMS	kA
<g/>
:	:	kIx,
NetDirect	NetDirect	k1gMnSc1
MediaCentrik	MediaCentrik	k1gMnSc1
<g/>
;	;	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oborová	oborový	k2eAgFnSc1d1
společenstva	společenstvo	k1gNnSc2
-	-	kIx~
HKCR	HKCR	kA
|	|	kIx~
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
www.komora.cz	www.komora.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WWW.MEDIACENTRIK.CZ	WWW.MEDIACENTRIK.CZ	k1gMnSc1
<g/>
,	,	kIx,
CMS	CMS	kA
<g/>
:	:	kIx,
NetDirect	NetDirect	k1gMnSc1
MediaCentrik	MediaCentrik	k1gMnSc1
<g/>
;	;	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sekce	sekce	k1gFnSc2
a	a	k8xC
pracovní	pracovní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
-	-	kIx~
HKCR	HKCR	kA
|	|	kIx~
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
www.komora.cz	www.komora.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dlouhý	Dlouhý	k1gMnSc1
dál	daleko	k6eAd2
v	v	k7c6
čele	čelo	k1gNnSc6
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc4
kritici	kritik	k1gMnPc1
neuspěli	uspět	k5eNaPmAgMnP
|	|	kIx~
ČeskéNoviny	ČeskéNovina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.ceskenoviny.cz	www.ceskenoviny.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WWW.MEDIACENTRIK.CZ	WWW.MEDIACENTRIK.CZ	k1gMnSc1
<g/>
,	,	kIx,
CMS	CMS	kA
<g/>
:	:	kIx,
NetDirect	NetDirect	k1gMnSc1
MediaCentrik	MediaCentrik	k1gMnSc1
<g/>
;	;	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnikatelé	podnikatel	k1gMnPc1
z	z	k7c2
celé	celý	k2eAgFnSc2d1
země	zem	k1gFnSc2
si	se	k3xPyFc3
zvolí	zvolit	k5eAaPmIp3nS
svého	svůj	k3xOyFgMnSc4
nového	nový	k2eAgMnSc4d1
prezidenta	prezident	k1gMnSc4
-	-	kIx~
HKCR	HKCR	kA
|	|	kIx~
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
www.komora.cz	www.komora.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HK	HK	kA
ČR	ČR	kA
<g/>
:	:	kIx,
Podnikatelská	podnikatelský	k2eAgFnSc1d1
delegace	delegace	k1gFnSc1
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
doprovodí	doprovodit	k5eAaPmIp3nS
ministra	ministr	k1gMnSc4
zahraničí	zahraničí	k1gNnSc2
do	do	k7c2
Kataru	katar	k1gInSc2
a	a	k8xC
Bahrajnu	Bahrajn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInPc1d1
listy	list	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
podnikatelská	podnikatelský	k2eAgFnSc1d1
delegace	delegace	k1gFnSc1
odlétá	odlétat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
Mongolska	Mongolsko	k1gNnSc2
podepsat	podepsat	k5eAaPmF
několik	několik	k4yIc4
smluv	smlouva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInPc1d1
listy	list	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WWW.MEDIACENTRIK.CZ	WWW.MEDIACENTRIK.CZ	k1gMnSc1
<g/>
,	,	kIx,
CMS	CMS	kA
<g/>
:	:	kIx,
NetDirect	NetDirect	k1gMnSc1
MediaCentrik	MediaCentrik	k1gMnSc1
<g/>
;	;	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činnost	činnost	k1gFnSc1
HK	HK	kA
ČR	ČR	kA
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
-	-	kIx~
HKCR	HKCR	kA
|	|	kIx~
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
www.komora.cz	www.komora.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Brána	brána	k1gFnSc1
do	do	k7c2
Persie	Persie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Téma	téma	k1gNnSc1
|	|	kIx~
Česká	český	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
V	v	k7c6
Íránu	Írán	k1gInSc6
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
po	po	k7c6
českých	český	k2eAgInPc6d1
výrobcích	výrobek	k1gInPc6
<g/>
,	,	kIx,
potvrdil	potvrdit	k5eAaPmAgMnS
tamní	tamní	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
na	na	k7c6
půdě	půda	k1gFnSc6
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Energo-Pro	Energo-Pro	k1gNnSc1
dodá	dodat	k5eAaPmIp3nS
do	do	k7c2
Íránu	Írán	k1gInSc2
vodní	vodní	k2eAgFnSc2d1
turbíny	turbína	k1gFnSc2
za	za	k7c4
400	#num#	k4
milionů	milion	k4xCgInPc2
-	-	kIx~
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprávy	zpráva	k1gFnSc2
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
České	český	k2eAgInPc1d1
autobusy	autobus	k1gInPc1
zamíří	zamířit	k5eAaPmIp3nP
do	do	k7c2
Íránu	Írán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firmy	firma	k1gFnPc1
dohodly	dohodnout	k5eAaPmAgFnP
kontrakt	kontrakt	k1gInSc4
za	za	k7c4
2	#num#	k4
miliardy	miliarda	k4xCgFnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-03-02	2017-03-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Podnikatelská	podnikatelský	k2eAgFnSc1d1
mise	mise	k1gFnSc1
do	do	k7c2
Íránu	Írán	k1gInSc2
<g/>
:	:	kIx,
Češi	Čech	k1gMnPc1
dohodli	dohodnout	k5eAaPmAgMnP
nové	nový	k2eAgInPc4d1
obchody	obchod	k1gInPc4
|	|	kIx~
BusinessInfo	BusinessInfo	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.businessinfo.cz	www.businessinfo.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
zvolila	zvolit	k5eAaPmAgFnS
za	za	k7c2
svého	svůj	k3xOyFgMnSc2
prezidenta	prezident	k1gMnSc2
opět	opět	k6eAd1
Vladimíra	Vladimír	k1gMnSc4
Dlouhého	Dlouhý	k1gMnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-30	2020-07-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Obchodní	obchodní	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
<g/>
,	,	kIx,
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
listina	listina	k1gFnSc1
A	a	k9
8179	#num#	k4
<g/>
/	/	kIx~
<g/>
SL	SL	kA
<g/>
74	#num#	k4
vedená	vedený	k2eAgFnSc1d1
u	u	k7c2
Městského	městský	k2eAgInSc2d1
soudu	soud	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
zpráva	zpráva	k1gFnSc1
auditora	auditor	k1gMnSc2
<g/>
,	,	kIx,
účetní	účetní	k2eAgFnSc1d1
závěrka	závěrka	k1gFnSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOMORA	komora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Udílení	udílení	k1gNnSc1
Řádu	řád	k1gInSc2
Vavřínu	vavřín	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
www.vavriny.cz	www.vavriny.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
S.	S.	kA
<g/>
R.	R.	kA
<g/>
O	O	kA
<g/>
,	,	kIx,
TV	TV	kA
PRAHA	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepší	dobrý	k2eAgMnPc1d3
absolventi	absolvent	k1gMnPc1
odborných	odborný	k2eAgFnPc2d1
škol	škola	k1gFnPc2
byli	být	k5eAaImAgMnP
oceněni	oceněn	k2eAgMnPc1d1
|	|	kIx~
PRAHA	Praha	k1gFnSc1
5	#num#	k4
|	|	kIx~
Zprávy	zpráva	k1gFnPc1
|	|	kIx~
PRAHA	Praha	k1gFnSc1
TV	TV	kA
<g/>
.	.	kIx.
prahatv	prahatv	k1gMnSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Povinnosti	povinnost	k1gFnSc2
podnikatelům	podnikatel	k1gMnPc3
ohlídá	ohlídat	k5eAaImIp3nS,k5eAaPmIp3nS
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naplní	naplnit	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc2
úřady	úřada	k1gMnSc2
<g/>
,	,	kIx,
zprovozní	zprovoznit	k5eAaPmIp3nS
komora	komora	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-05-18	2016-05-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sněmovní	sněmovní	k2eAgInSc4d1
tisk	tisk	k1gInSc4
1089	#num#	k4
<g/>
.	.	kIx.
www.psp.cz	www.psp.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Výsledky	výsledek	k1gInPc1
jednání	jednání	k1gNnSc4
vlády	vláda	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
www.vlada.cz	www.vlada.cza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
:	:	kIx,
Dlouhý	Dlouhý	k1gMnSc1
-	-	kIx~
Zamítnutím	zamítnutí	k1gNnSc7
„	„	k?
<g/>
podnikatelského	podnikatelský	k2eAgMnSc2d1
psa	pes	k1gMnSc2
<g/>
“	“	k?
se	se	k3xPyFc4
problém	problém	k1gInSc1
s	s	k7c7
byrokracií	byrokracie	k1gFnSc7
a	a	k8xC
legislativní	legislativní	k2eAgFnSc7d1
džunglí	džungle	k1gFnSc7
nevyřešil	vyřešit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Petice	petice	k1gFnSc1
zaměstnanců	zaměstnanec	k1gMnPc2
Úřadu	úřad	k1gInSc2
Hospodářské	hospodářský	k2eAgFnPc1d1
komory	komora	k1gFnPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
↑	↑	k?
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
ČR	ČR	kA
<g/>
,	,	kIx,
Stanovisko	stanovisko	k1gNnSc1
představenstva	představenstvo	k1gNnSc2
HK	HK	kA
ČR	ČR	kA
k	k	k7c3
„	„	k?
<g/>
Petici	petice	k1gFnSc3
<g/>
“	“	k?
zaměstnanců	zaměstnanec	k1gMnPc2
úřadu	úřad	k1gInSc2
HK	HK	kA
ČR	ČR	kA
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
Archivováno	archivován	k2eAgNnSc4d1
15	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
ČR	ČR	kA
<g/>
,	,	kIx,
HK	HK	kA
ČR	ČR	kA
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
zavedení	zavedení	k1gNnSc4
elektronické	elektronický	k2eAgFnSc2d1
karty	karta	k1gFnSc2
pracovníka	pracovník	k1gMnSc2
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
Archivováno	archivován	k2eAgNnSc4d1
6	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
VODIČKA	Vodička	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chystá	chystat	k5eAaImIp3nS
se	se	k3xPyFc4
další	další	k2eAgFnSc1d1
chytrá	chytrý	k2eAgFnSc1d1
karta	karta	k1gFnSc1
<g/>
,	,	kIx,
zaměstnancům	zaměstnanec	k1gMnPc3
má	mít	k5eAaImIp3nS
"	"	kIx"
<g/>
usnadnit	usnadnit	k5eAaPmF
život	život	k1gInSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
TŮMA	Tůma	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Očima	oko	k1gNnPc7
expertů	expert	k1gMnPc2
<g/>
:	:	kIx,
Další	další	k2eAgFnSc1d1
kartička	kartička	k1gFnSc1
do	do	k7c2
peněženky	peněženka	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Elektronická	elektronický	k2eAgFnSc1d1
karta	karta	k1gFnSc1
pracovníka	pracovník	k1gMnSc2
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
peníze	peníz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
2217	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HRONÍK	HRONÍK	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karta	karta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vás	vy	k3xPp2nPc4
sleduje	sledovat	k5eAaImIp3nS
v	v	k7c6
práci	práce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
chystá	chystat	k5eAaImIp3nS
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInPc1d1
listy	list	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tu	tu	k6eAd1
projekt	projekt	k1gInSc1
elektronických	elektronický	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
pro	pro	k7c4
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ODS	ODS	kA
je	být	k5eAaImIp3nS
ostře	ostro	k6eAd1
proti	proti	k7c3
<g/>
.	.	kIx.
</s>
<s desamb="1">
IDnes	IDnes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
HOUSKA	houska	k1gFnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
ČR	ČR	kA
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
k	k	k7c3
penězům	peníze	k1gInPc3
z	z	k7c2
evropských	evropský	k2eAgMnPc2d1
fondů	fond	k1gInPc2
falšováním	falšování	k1gNnSc7
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Semináře	seminář	k1gInSc2
naúčtoval	naúčtovat	k5eAaPmAgInS
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
nebyly	být	k5eNaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případ	případ	k1gInSc1
exšéfa	exšéf	k1gMnSc2
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
soud	soud	k1gInSc1
řešit	řešit	k5eAaImF
znovu	znovu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Soud	soud	k1gInSc1
zprostil	zprostit	k5eAaPmAgInS
exšéfa	exšéf	k1gMnSc4
Hospodářské	hospodářský	k2eAgFnSc2d1
komory	komora	k1gFnSc2
obžaloby	obžaloba	k1gFnSc2
z	z	k7c2
podvodu	podvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kužel	kužel	k1gInSc1
teď	teď	k6eAd1
po	po	k7c6
státu	stát	k1gInSc6
žádá	žádat	k5eAaImIp3nS
odškodnění	odškodnění	k1gNnSc1
|	|	kIx~
Domov	domov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-05-07	2020-05-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hospodářská	hospodářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
301	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Hospodářské	hospodářský	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
Agrární	agrární	k2eAgFnSc3d1
komoře	komora	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010724178	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
136515026	#num#	k4
</s>
