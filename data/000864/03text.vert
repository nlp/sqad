<s>
Magnalium	magnalium	k1gNnSc1	magnalium
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
slitiny	slitina	k1gFnSc2	slitina
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
5-50	[number]	k4	5-50
%	%	kIx~	%
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
tvrdosti	tvrdost	k1gFnSc2	tvrdost
slitiny	slitina	k1gFnSc2	slitina
bývají	bývat	k5eAaImIp3nP	bývat
přidávána	přidáván	k2eAgNnPc4d1	přidáváno
i	i	k8xC	i
stopová	stopový	k2eAgNnPc4d1	stopové
množství	množství	k1gNnPc4	množství
jiných	jiný	k2eAgInPc2d1	jiný
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Magnalium	magnalium	k1gNnSc1	magnalium
nachází	nacházet	k5eAaImIp3nS	nacházet
své	svůj	k3xOyFgNnSc4	svůj
využití	využití	k1gNnSc4	využití
ve	v	k7c6	v
strojírenství	strojírenství	k1gNnSc6	strojírenství
a	a	k8xC	a
pyrotechnice	pyrotechnika	k1gFnSc6	pyrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
podílem	podíl	k1gInSc7	podíl
hořčíku	hořčík	k1gInSc2	hořčík
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
vynikají	vynikat	k5eAaImIp3nP	vynikat
velkou	velký	k2eAgFnSc7d1	velká
pevností	pevnost	k1gFnSc7	pevnost
<g/>
,	,	kIx,	,
odolávají	odolávat	k5eAaImIp3nP	odolávat
lépe	dobře	k6eAd2	dobře
korozi	koroze	k1gFnSc3	koroze
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
nižší	nízký	k2eAgFnSc4d2	nižší
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
čistý	čistý	k2eAgInSc4d1	čistý
hliník	hliník	k1gInSc4	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
obrábět	obrábět	k5eAaImF	obrábět
a	a	k8xC	a
svářet	svářet	k5eAaImF	svářet
než	než	k8xS	než
hliník	hliník	k1gInSc1	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
obsahem	obsah	k1gInSc7	obsah
hořčíku	hořčík	k1gInSc2	hořčík
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
křehčí	křehký	k2eAgFnPc1d2	křehčí
a	a	k8xC	a
náchylnější	náchylný	k2eAgFnPc1d2	náchylnější
ke	k	k7c3	k
korozi	koroze	k1gFnSc3	koroze
než	než	k8xS	než
hliník	hliník	k1gInSc1	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
mnohem	mnohem	k6eAd1	mnohem
dražší	drahý	k2eAgInSc1d2	dražší
než	než	k8xS	než
samotný	samotný	k2eAgInSc1d1	samotný
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
obrobitelnost	obrobitelnost	k1gFnSc1	obrobitelnost
slitin	slitina	k1gFnPc2	slitina
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
podílem	podíl	k1gInSc7	podíl
hořčíku	hořčík	k1gInSc2	hořčík
je	on	k3xPp3gMnPc4	on
předurčily	předurčit	k5eAaPmAgFnP	předurčit
k	k	k7c3	k
využití	využití	k1gNnSc3	využití
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
leteckých	letecký	k2eAgFnPc2d1	letecká
a	a	k8xC	a
automobilových	automobilový	k2eAgFnPc2d1	automobilová
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
také	také	k9	také
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
kovových	kovový	k2eAgNnPc2d1	kovové
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
a	a	k8xC	a
speciálních	speciální	k2eAgInPc2d1	speciální
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
kolem	kolem	k7c2	kolem
50	[number]	k4	50
%	%	kIx~	%
hořčíku	hořčík	k1gInSc2	hořčík
se	se	k3xPyFc4	se
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgFnPc3	svůj
vlastnostem	vlastnost	k1gFnPc3	vlastnost
tohoto	tento	k3xDgInSc2	tento
využití	využití	k1gNnSc3	využití
nedočkaly	dočkat	k5eNaPmAgFnP	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hořlavost	hořlavost	k1gFnSc4	hořlavost
v	v	k7c6	v
práškovém	práškový	k2eAgInSc6d1	práškový
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc4d2	vyšší
reaktivitu	reaktivita	k1gFnSc4	reaktivita
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
pyrotechnice	pyrotechnika	k1gFnSc6	pyrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
magnalia	magnalium	k1gNnSc2	magnalium
<g/>
,	,	kIx,	,
amatérské	amatérský	k2eAgFnSc2d1	amatérská
stránky	stránka	k1gFnSc2	stránka
Alana	Alan	k1gMnSc2	Alan
Yatese	Yatese	k1gFnSc2	Yatese
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Magnalium	magnalium	k1gNnSc4	magnalium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
