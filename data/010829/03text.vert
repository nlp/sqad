<p>
<s>
Socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
samostatné	samostatný	k2eAgNnSc1d1	samostatné
třírozměrné	třírozměrný	k2eAgNnSc1d1	třírozměrné
umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
sochařem	sochař	k1gMnSc7	sochař
<g/>
.	.	kIx.	.
</s>
<s>
Výkladový	výkladový	k2eAgInSc1d1	výkladový
slovník	slovník	k1gInSc1	slovník
od	od	k7c2	od
autora	autor	k1gMnSc2	autor
Jana	Jan	k1gMnSc2	Jan
Baleky	Balek	k1gInPc4	Balek
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
plastika	plastika	k1gFnSc1	plastika
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
tvořená	tvořený	k2eAgFnSc1d1	tvořená
technikou	technika	k1gFnSc7	technika
přidávací	přidávací	k2eAgInSc4d1	přidávací
(	(	kIx(	(
<g/>
vymodelované	vymodelovaný	k2eAgFnPc1d1	vymodelovaná
sochy	socha	k1gFnPc1	socha
z	z	k7c2	z
keramiky	keramika	k1gFnSc2	keramika
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
skulptura	skulptura	k1gFnSc1	skulptura
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
tvořená	tvořený	k2eAgFnSc1d1	tvořená
technikou	technika	k1gFnSc7	technika
odebírající	odebírající	k2eAgInSc4d1	odebírající
(	(	kIx(	(
<g/>
tesané	tesaný	k2eAgFnPc1d1	tesaná
sochy	socha	k1gFnPc1	socha
z	z	k7c2	z
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
dřeva	dřevo	k1gNnSc2	dřevo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
slovo	slovo	k1gNnSc1	slovo
plastika	plastik	k1gMnSc2	plastik
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
plastos	plastos	k1gInSc1	plastos
=	=	kIx~	=
výtvor	výtvor	k1gInSc1	výtvor
od	od	k7c2	od
plasso	plassa	k1gFnSc5	plassa
=	=	kIx~	=
tvořím	tvořit	k5eAaImIp1nS	tvořit
<g/>
,	,	kIx,	,
vymýšlím	vymýšlet	k5eAaImIp1nS	vymýšlet
<g/>
,	,	kIx,	,
přetvářím	přetvářit	k5eAaPmIp1nS	přetvářit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
obecné	obecný	k2eAgNnSc1d1	obecné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
trojrozměrná	trojrozměrný	k2eAgNnPc4d1	trojrozměrné
výtvarná	výtvarný	k2eAgNnPc4d1	výtvarné
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
socha	socha	k1gFnSc1	socha
jako	jako	k8xC	jako
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
figurální	figurální	k2eAgFnPc4d1	figurální
plastiky	plastika	k1gFnPc4	plastika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
slovo	slovo	k1gNnSc4	slovo
skulptura	skulptura	k1gFnSc1	skulptura
bývá	bývat	k5eAaImIp3nS	bývat
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
synonymum	synonymum	k1gNnSc4	synonymum
slova	slovo	k1gNnSc2	slovo
plastika	plastik	k1gMnSc2	plastik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
či	či	k8xC	či
proporcemi	proporce	k1gFnPc7	proporce
obvykle	obvykle	k6eAd1	obvykle
vychází	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
lidské	lidský	k2eAgFnSc2d1	lidská
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
pravidlem	pravidlem	k6eAd1	pravidlem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
sochařství	sochařství	k1gNnSc6	sochařství
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
začátku	začátek	k1gInSc2	začátek
abstrakce	abstrakce	k1gFnSc1	abstrakce
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nevycházejí	vycházet	k5eNaImIp3nP	vycházet
z	z	k7c2	z
lidské	lidský	k2eAgFnSc2d1	lidská
figury	figura	k1gFnSc2	figura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
"	"	kIx"	"
<g/>
odlidštěné	odlidštěný	k2eAgFnPc1d1	odlidštěná
<g/>
"	"	kIx"	"
sochy	socha	k1gFnPc1	socha
dají	dát	k5eAaPmIp3nP	dát
nazvat	nazvat	k5eAaPmF	nazvat
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
objekty	objekt	k1gInPc1	objekt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
</s>
<s>
Více	hodně	k6eAd2	hodně
soch	socha	k1gFnPc2	socha
sestavených	sestavený	k2eAgInPc2d1	sestavený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
působily	působit	k5eAaImAgInP	působit
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sousoší	sousoší	k1gNnSc1	sousoší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
historicky	historicky	k6eAd1	historicky
nejběžněji	běžně	k6eAd3	běžně
používané	používaný	k2eAgInPc1d1	používaný
jsou	být	k5eAaImIp3nP	být
keramika	keramika	k1gFnSc1	keramika
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
bronz	bronz	k1gInSc1	bronz
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Sochy	socha	k1gFnPc1	socha
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používány	používat	k5eAaImNgFnP	používat
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
pomníků	pomník	k1gInPc2	pomník
či	či	k8xC	či
kašen	kašna	k1gFnPc2	kašna
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
architektonický	architektonický	k2eAgInSc4d1	architektonický
doplněk	doplněk	k1gInSc4	doplněk
exteriérů	exteriér	k1gInPc2	exteriér
i	i	k8xC	i
interiérů	interiér	k1gInPc2	interiér
budov	budova	k1gFnPc2	budova
i	i	k8xC	i
jako	jako	k9	jako
samostatná	samostatný	k2eAgNnPc1d1	samostatné
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgFnSc1d1	umístěná
kdekoliv	kdekoliv	k6eAd1	kdekoliv
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
či	či	k8xC	či
na	na	k7c6	na
veřejných	veřejný	k2eAgNnPc6d1	veřejné
prostranstvích	prostranství	k1gNnPc6	prostranství
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zpříjemnit	zpříjemnit	k5eAaPmF	zpříjemnit
prostředí	prostředí	k1gNnSc4	prostředí
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
orientační	orientační	k2eAgInPc4d1	orientační
body	bod	k1gInPc4	bod
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
a	a	k8xC	a
v	v	k7c6	v
zástavbě	zástavba	k1gFnSc6	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
častý	častý	k2eAgInSc1d1	častý
prvek	prvek	k1gInSc1	prvek
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
sadovnické	sadovnický	k2eAgFnSc6d1	Sadovnická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
odvětvím	odvětví	k1gNnSc7	odvětví
sochařství	sochařství	k1gNnSc2	sochařství
je	být	k5eAaImIp3nS	být
drobná	drobný	k2eAgFnSc1d1	drobná
plastika	plastika	k1gFnSc1	plastika
medaile	medaile	k1gFnSc2	medaile
nebo	nebo	k8xC	nebo
též	tenž	k3xDgFnSc2	tenž
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
sochařství	sochařství	k1gNnSc2	sochařství
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stejné	stejný	k2eAgInPc4d1	stejný
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
rozdíl	rozdíl	k1gInSc1	rozdíl
bývá	bývat	k5eAaImIp3nS	bývat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
použití	použití	k1gNnSc6	použití
výsledného	výsledný	k2eAgInSc2d1	výsledný
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tak	tak	k6eAd1	tak
odlišný	odlišný	k2eAgInSc4d1	odlišný
obor	obor	k1gInSc4	obor
užitého	užitý	k2eAgNnSc2d1	užité
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tvůrci	tvůrce	k1gMnPc1	tvůrce
medailí	medaile	k1gFnPc2	medaile
a	a	k8xC	a
mincí	mince	k1gFnPc2	mince
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
klasického	klasický	k2eAgNnSc2d1	klasické
sochařství	sochařství	k1gNnSc2	sochařství
vyčlenili	vyčlenit	k5eAaPmAgMnP	vyčlenit
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říkat	říkat	k5eAaImF	říkat
medaileři	medailer	k1gMnPc1	medailer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
soch	socha	k1gFnPc2	socha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Medaile	medaile	k1gFnSc1	medaile
</s>
</p>
<p>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
</s>
</p>
<p>
<s>
Busta	busta	k1gFnSc1	busta
</s>
</p>
<p>
<s>
Jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
socha	socha	k1gFnSc1	socha
</s>
</p>
<p>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
</s>
</p>
<p>
<s>
Figura	figura	k1gFnSc1	figura
</s>
</p>
<p>
<s>
Sochařský	sochařský	k2eAgInSc1d1	sochařský
objekt	objekt	k1gInSc1	objekt
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
architekty	architekt	k1gMnPc4	architekt
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Živá	živý	k2eAgFnSc1d1	živá
socha	socha	k1gFnSc1	socha
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sochařství	sochařství	k1gNnSc1	sochařství
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
socha	socha	k1gFnSc1	socha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
socha	socha	k1gFnSc1	socha
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
