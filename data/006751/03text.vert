<s>
Pistole	pistole	k1gFnSc1	pistole
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
ruční	ruční	k2eAgFnSc1d1	ruční
palná	palný	k2eAgFnSc1d1	palná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
pistole	pistol	k1gFnPc1	pistol
byly	být	k5eAaImAgFnP	být
jednoranové	jednoranový	k2eAgFnPc1d1	jednoranová
a	a	k8xC	a
používaly	používat	k5eAaImAgFnP	používat
černý	černý	k2eAgInSc4d1	černý
střelný	střelný	k2eAgInSc4d1	střelný
prach	prach	k1gInSc4	prach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
víceranové	víceranový	k2eAgFnPc1d1	víceranový
pistole	pistol	k1gFnPc1	pistol
a	a	k8xC	a
jako	jako	k8xS	jako
náplň	náplň	k1gFnSc4	náplň
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
užívat	užívat	k5eAaImF	užívat
nově	nově	k6eAd1	nově
objevený	objevený	k2eAgInSc1d1	objevený
bezdýmný	bezdýmný	k2eAgInSc1d1	bezdýmný
střelný	střelný	k2eAgInSc1d1	střelný
prach	prach	k1gInSc1	prach
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
revolverům	revolver	k1gInPc3	revolver
a	a	k8xC	a
jiným	jiný	k2eAgFnPc3d1	jiná
krátkým	krátký	k2eAgFnPc3d1	krátká
palným	palný	k2eAgFnPc3d1	palná
zbraním	zbraň	k1gFnPc3	zbraň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
derringerům	derringer	k1gInPc3	derringer
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
moderní	moderní	k2eAgFnPc1d1	moderní
pistole	pistol	k1gFnPc1	pistol
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
ve	v	k7c6	v
valné	valný	k2eAgFnSc6d1	valná
většině	většina	k1gFnSc6	většina
samonabíjecí	samonabíjecí	k2eAgFnSc7d1	samonabíjecí
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc1	všechen
moderní	moderní	k2eAgFnPc1d1	moderní
pistole	pistol	k1gFnPc1	pistol
užívají	užívat	k5eAaImIp3nP	užívat
horizontálně	horizontálně	k6eAd1	horizontálně
posuvný	posuvný	k2eAgInSc4d1	posuvný
<g/>
,	,	kIx,	,
blokový	blokový	k2eAgInSc4d1	blokový
závěrový	závěrový	k2eAgInSc4d1	závěrový
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
V.	V.	kA	V.
Machka	Machek	k1gInSc2	Machek
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
pistole	pistol	k1gFnSc2	pistol
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
převzato	převzít	k5eAaPmNgNnS	převzít
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
nebo	nebo	k8xC	nebo
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
pistolet	pistolet	k5eAaImF	pistolet
a	a	k8xC	a
italské	italský	k2eAgFnPc1d1	italská
pistola	pistola	k1gFnSc1	pistola
však	však	k9	však
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
píšťala	píšťala	k1gFnSc1	píšťala
<g/>
,	,	kIx,	,
a	a	k8xC	a
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
byli	být	k5eAaImAgMnP	být
první	první	k4xOgMnPc1	první
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
palné	palný	k2eAgFnPc4d1	palná
zbraně	zbraň	k1gFnPc4	zbraň
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
používali	používat	k5eAaImAgMnP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
výklady	výklad	k1gInPc1	výklad
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
<g/>
:	:	kIx,	:
ze	z	k7c2	z
starofrancouzského	starofrancouzský	k2eAgMnSc2d1	starofrancouzský
pistallo	pistalnout	k5eAaPmAgNnS	pistalnout
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hruška	hruška	k1gFnSc1	hruška
u	u	k7c2	u
sedla	sedlo	k1gNnSc2	sedlo
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
byly	být	k5eAaImAgFnP	být
pistole	pistol	k1gFnPc1	pistol
původně	původně	k6eAd1	původně
nošeny	nošen	k2eAgFnPc1d1	nošena
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
toskánského	toskánský	k2eAgNnSc2d1	toskánské
města	město	k1gNnSc2	město
Pistoia	Pistoium	k1gNnSc2	Pistoium
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pistolí	pistol	k1gFnPc2	pistol
s	s	k7c7	s
jednočinným	jednočinný	k2eAgInSc7d1	jednočinný
spoušťovým	spoušťový	k2eAgInSc7d1	spoušťový
mechanizmem	mechanizmus	k1gInSc7	mechanizmus
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
action	action	k1gInSc1	action
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
před	před	k7c7	před
prvním	první	k4xOgInSc7	první
výstřelem	výstřel	k1gInSc7	výstřel
natáhnout	natáhnout	k5eAaPmF	natáhnout
kohout	kohout	k1gInSc4	kohout
palcem	palec	k1gInSc7	palec
nebo	nebo	k8xC	nebo
pohybem	pohyb	k1gInSc7	pohyb
celého	celý	k2eAgInSc2d1	celý
závěru	závěr	k1gInSc2	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
střelbu	střelba	k1gFnSc4	střelba
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
kohout	kohout	k1gInSc1	kohout
napnut	napnut	k2eAgInSc1d1	napnut
pohybem	pohyb	k1gInSc7	pohyb
závěru	závěr	k1gInSc2	závěr
po	po	k7c6	po
výstřelu	výstřel	k1gInSc6	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stisku	stisk	k1gInSc6	stisk
spouště	spoušť	k1gFnSc2	spoušť
je	být	k5eAaImIp3nS	být
uvolněn	uvolněn	k2eAgInSc1d1	uvolněn
kohout	kohout	k1gInSc1	kohout
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pomocí	pomocí	k7c2	pomocí
zápalníku	zápalník	k1gInSc2	zápalník
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
zápalku	zápalka	k1gFnSc4	zápalka
a	a	k8xC	a
odpálí	odpálit	k5eAaPmIp3nS	odpálit
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgInSc1d1	nazýván
jednočinný	jednočinný	k2eAgInSc1d1	jednočinný
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
action	action	k1gInSc4	action
<g/>
)	)	kIx)	)
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
spoušť	spoušť	k1gFnSc1	spoušť
provádí	provádět	k5eAaImIp3nS	provádět
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
činnost	činnost	k1gFnSc4	činnost
–	–	k?	–
uvolnění	uvolnění	k1gNnSc4	uvolnění
kohoutu	kohout	k1gInSc2	kohout
<g/>
.	.	kIx.	.
</s>
<s>
Chod	chod	k1gInSc1	chod
jednočinné	jednočinný	k2eAgFnSc2d1	jednočinná
spouště	spoušť	k1gFnSc2	spoušť
je	být	k5eAaImIp3nS	být
lehký	lehký	k2eAgInSc1d1	lehký
a	a	k8xC	a
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
režimu	režim	k1gInSc6	režim
lze	lze	k6eAd1	lze
dosahovat	dosahovat	k5eAaImF	dosahovat
velmi	velmi	k6eAd1	velmi
přesných	přesný	k2eAgInPc2d1	přesný
zásahů	zásah	k1gInPc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
nejsou	být	k5eNaImIp3nP	být
jednočinné	jednočinný	k2eAgFnPc1d1	jednočinná
pistole	pistol	k1gFnPc1	pistol
a	a	k8xC	a
revolvery	revolver	k1gInPc1	revolver
příliš	příliš	k6eAd1	příliš
vhodné	vhodný	k2eAgInPc1d1	vhodný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
natahování	natahování	k1gNnSc1	natahování
kohoutu	kohout	k1gInSc2	kohout
může	moct	k5eAaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
rychlost	rychlost	k1gFnSc4	rychlost
prvního	první	k4xOgInSc2	první
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
je	být	k5eAaImIp3nS	být
i	i	k9	i
nošení	nošení	k1gNnSc4	nošení
zajištěné	zajištěný	k2eAgFnSc2d1	zajištěná
pistole	pistol	k1gFnSc2	pistol
s	s	k7c7	s
nábojem	náboj	k1gInSc7	náboj
v	v	k7c6	v
komoře	komora	k1gFnSc6	komora
a	a	k8xC	a
nataženým	natažený	k2eAgInSc7d1	natažený
kohoutem	kohout	k1gInSc7	kohout
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
např.	např.	kA	např.
nevhodnou	vhodný	k2eNgFnSc7d1	nevhodná
manipulací	manipulace	k1gFnSc7	manipulace
mohl	moct	k5eAaImAgInS	moct
náhodně	náhodně	k6eAd1	náhodně
uvolnit	uvolnit	k5eAaPmF	uvolnit
a	a	k8xC	a
zbraň	zbraň	k1gFnSc4	zbraň
vystřelit	vystřelit	k5eAaPmF	vystřelit
<g/>
.	.	kIx.	.
</s>
<s>
Nošení	nošení	k1gNnSc1	nošení
zajištěné	zajištěný	k2eAgFnSc2d1	zajištěná
zbraně	zbraň	k1gFnSc2	zbraň
s	s	k7c7	s
napnutým	napnutý	k2eAgInSc7d1	napnutý
kohoutem	kohout	k1gInSc7	kohout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
oblíbeno	oblíben	k2eAgNnSc1d1	oblíbeno
především	především	k9	především
u	u	k7c2	u
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Cocked	Cocked	k1gInSc1	Cocked
and	and	k?	and
locked	locked	k1gInSc1	locked
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
napnutý	napnutý	k2eAgMnSc1d1	napnutý
a	a	k8xC	a
zajištěný	zajištěný	k2eAgMnSc1d1	zajištěný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
–	–	k?	–
kvůli	kvůli	k7c3	kvůli
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
působení	působení	k1gNnSc3	působení
stresového	stresový	k2eAgInSc2d1	stresový
faktoru	faktor	k1gInSc2	faktor
–	–	k?	–
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jde	jít	k5eAaImIp3nS	jít
člověku	člověk	k1gMnSc3	člověk
o	o	k7c4	o
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
střelec	střelec	k1gMnSc1	střelec
zapomene	zapomnět	k5eAaImIp3nS	zapomnět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zbraň	zbraň	k1gFnSc1	zbraň
je	být	k5eAaImIp3nS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
a	a	k8xC	a
ve	v	k7c6	v
stresu	stres	k1gInSc6	stres
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
včas	včas	k6eAd1	včas
odhalit	odhalit	k5eAaPmF	odhalit
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
momentálně	momentálně	k6eAd1	momentálně
nefunkční	funkční	k2eNgMnSc1d1	nefunkční
<g/>
.	.	kIx.	.
</s>
<s>
Pistole	pistol	k1gFnPc1	pistol
systému	systém	k1gInSc2	systém
SA	SA	kA	SA
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Colt	Colt	k1gInSc4	Colt
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
FN	FN	kA	FN
Browning	browning	k1gInSc1	browning
HP	HP	kA	HP
<g/>
,	,	kIx,	,
Beretta	Beretta	k1gFnSc1	Beretta
70	[number]	k4	70
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pistolí	pistol	k1gFnPc2	pistol
s	s	k7c7	s
dvojčinným	dvojčinný	k2eAgInSc7d1	dvojčinný
spoušťovým	spoušťový	k2eAgInSc7d1	spoušťový
mechanizmem	mechanizmus	k1gInSc7	mechanizmus
(	(	kIx(	(
<g/>
double	double	k2eAgInSc4d1	double
action	action	k1gInSc4	action
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
střílet	střílet	k5eAaImF	střílet
promáčknutím	promáčknutí	k1gNnSc7	promáčknutí
spouště	spoušť	k1gFnSc2	spoušť
bez	bez	k7c2	bez
ručního	ruční	k2eAgNnSc2d1	ruční
napínání	napínání	k1gNnSc2	napínání
kohoutu	kohout	k1gInSc2	kohout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zbraň	zbraň	k1gFnSc1	zbraň
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k8xS	jako
dvojčinná	dvojčinný	k2eAgFnSc1d1	dvojčinná
(	(	kIx(	(
<g/>
DA	DA	kA	DA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
DA	DA	kA	DA
režimu	režim	k1gInSc2	režim
disponuje	disponovat	k5eAaBmIp3nS	disponovat
i	i	k9	i
SA	SA	kA	SA
režimem	režim	k1gInSc7	režim
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
a	a	k8xC	a
nelogické	logický	k2eNgNnSc1d1	nelogické
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
výraz	výraz	k1gInSc1	výraz
SA	SA	kA	SA
<g/>
/	/	kIx~	/
<g/>
DA	DA	kA	DA
<g/>
.	.	kIx.	.
</s>
<s>
Stiskem	stisk	k1gInSc7	stisk
spouště	spoušť	k1gFnSc2	spoušť
se	se	k3xPyFc4	se
napíná	napínat	k5eAaImIp3nS	napínat
kohout	kohout	k1gInSc1	kohout
<g/>
,	,	kIx,	,
když	když	k8xS	když
kohout	kohout	k1gMnSc1	kohout
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
své	svůj	k3xOyFgFnPc4	svůj
zadní	zadní	k2eAgFnPc4d1	zadní
polohy	poloha	k1gFnPc4	poloha
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
a	a	k8xC	a
odpálí	odpálit	k5eAaPmIp3nS	odpálit
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Chod	chod	k1gInSc1	chod
spouště	spoušť	k1gFnSc2	spoušť
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
režimu	režim	k1gInSc6	režim
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc1d2	delší
a	a	k8xC	a
tvrdší	tvrdý	k2eAgFnSc1d2	tvrdší
než	než	k8xS	než
v	v	k7c6	v
jednočinném	jednočinný	k2eAgNnSc6d1	jednočinné
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
přesnost	přesnost	k1gFnSc4	přesnost
střelby	střelba	k1gFnSc2	střelba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
spoušť	spoušť	k1gFnSc1	spoušť
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
strhnutí	strhnutí	k1gNnSc2	strhnutí
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
palby	palba	k1gFnSc2	palba
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
výhodné	výhodný	k2eAgNnSc1d1	výhodné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
obranné	obranný	k2eAgFnSc2d1	obranná
střelby	střelba	k1gFnSc2	střelba
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
DA	DA	kA	DA
pistolí	pistol	k1gFnSc7	pistol
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
palbu	palba	k1gFnSc4	palba
také	také	k9	také
v	v	k7c6	v
SA	SA	kA	SA
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
DA	DA	kA	DA
(	(	kIx(	(
<g/>
SA	SA	kA	SA
<g/>
/	/	kIx~	/
<g/>
DA	DA	kA	DA
<g/>
)	)	kIx)	)
pistolí	pistol	k1gFnSc7	pistol
se	se	k3xPyFc4	se
DA	DA	kA	DA
režim	režim	k1gInSc1	režim
může	moct	k5eAaImIp3nS	moct
použít	použít	k5eAaPmF	použít
jen	jen	k9	jen
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
výstřelu	výstřel	k1gInSc6	výstřel
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
komoře	komora	k1gFnSc6	komora
a	a	k8xC	a
kohout	kohout	k1gMnSc1	kohout
je	být	k5eAaImIp3nS	být
vypuštěný	vypuštěný	k2eAgMnSc1d1	vypuštěný
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
střelba	střelba	k1gFnSc1	střelba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vede	vést	k5eAaImIp3nS	vést
v	v	k7c6	v
SA	SA	kA	SA
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kohout	kohout	k1gMnSc1	kohout
je	být	k5eAaImIp3nS	být
napínán	napínat	k5eAaImNgMnS	napínat
pohybem	pohyb	k1gInSc7	pohyb
závěru	závěr	k1gInSc2	závěr
po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
výstřelu	výstřel	k1gInSc6	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
dvojčinné	dvojčinný	k2eAgFnSc2d1	dvojčinná
pistole	pistol	k1gFnSc2	pistol
je	být	k5eAaImIp3nS	být
CZ	CZ	kA	CZ
75	[number]	k4	75
<g/>
,	,	kIx,	,
Beretta	Beretta	k1gFnSc1	Beretta
92	[number]	k4	92
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pistolí	pistol	k1gFnPc2	pistol
s	s	k7c7	s
výhradně	výhradně	k6eAd1	výhradně
dvojčinným	dvojčinný	k2eAgInSc7d1	dvojčinný
spoušťovým	spoušťový	k2eAgInSc7d1	spoušťový
mechanizmem	mechanizmus	k1gInSc7	mechanizmus
se	se	k3xPyFc4	se
každé	každý	k3xTgNnSc1	každý
napnutí	napnutí	k1gNnSc1	napnutí
bicího	bicí	k2eAgInSc2d1	bicí
mechanizmu	mechanizmus	k1gInSc2	mechanizmus
provádí	provádět	k5eAaImIp3nS	provádět
promáčknutím	promáčknutí	k1gNnSc7	promáčknutí
spouště	spoušť	k1gFnSc2	spoušť
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
přes	přes	k7c4	přes
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc4d1	velký
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
výstřelu	výstřel	k1gInSc6	výstřel
zůstane	zůstat	k5eAaPmIp3nS	zůstat
bicí	bicí	k2eAgInSc1d1	bicí
mechanismus	mechanismus	k1gInSc1	mechanismus
vypuštěný	vypuštěný	k2eAgInSc1d1	vypuštěný
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nenapnutý	napnutý	k2eNgMnSc1d1	napnutý
<g/>
,	,	kIx,	,
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
napínat	napínat	k5eAaImF	napínat
spouští	spoušť	k1gFnSc7	spoušť
<g/>
.	.	kIx.	.
</s>
<s>
DAO	DAO	kA	DAO
spouště	spoušť	k1gFnPc1	spoušť
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
většinou	většina	k1gFnSc7	většina
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
obranných	obranný	k2eAgFnPc2d1	obranná
pistolí	pistol	k1gFnPc2	pistol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravidlem	pravidlem	k6eAd1	pravidlem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pistole	pistol	k1gFnSc2	pistol
CZ	CZ	kA	CZ
100	[number]	k4	100
je	být	k5eAaImIp3nS	být
pistole	pistole	k1gFnSc1	pistole
ráže	ráže	k1gFnSc2	ráže
9	[number]	k4	9
<g/>
mm	mm	kA	mm
Para	para	k1gInPc2	para
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgNnSc2	tento
řešení	řešení	k1gNnSc2	řešení
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
manipulace	manipulace	k1gFnPc4	manipulace
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc4d2	vyšší
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
–	–	k?	–
zbraň	zbraň	k1gFnSc1	zbraň
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
stále	stále	k6eAd1	stále
stejný	stejný	k2eAgInSc1d1	stejný
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc4d1	velký
odpor	odpor	k1gInSc4	odpor
spouště	spoušť	k1gFnSc2	spoušť
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
téměř	téměř	k6eAd1	téměř
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
možnost	možnost	k1gFnSc4	možnost
nechtěného	chtěný	k2eNgInSc2d1	nechtěný
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zbraně	zbraň	k1gFnPc1	zbraň
obvykle	obvykle	k6eAd1	obvykle
nemají	mít	k5eNaImIp3nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
manuální	manuální	k2eAgFnSc4d1	manuální
pojistku	pojistka	k1gFnSc4	pojistka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
výhoda	výhoda	k1gFnSc1	výhoda
pro	pro	k7c4	pro
netrénovaného	trénovaný	k2eNgMnSc4d1	netrénovaný
střelce	střelec	k1gMnSc4	střelec
ve	v	k7c6	v
stresové	stresový	k2eAgFnSc6d1	stresová
situaci	situace	k1gFnSc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Pistolím	pistol	k1gFnPc3	pistol
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
DAO	DAO	kA	DAO
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
nabij	nabít	k5eAaPmRp2nS	nabít
a	a	k8xC	a
zapomeň	zapomenout	k5eAaPmRp2nS	zapomenout
–	–	k?	–
uživatel	uživatel	k1gMnSc1	uživatel
zbraně	zbraň	k1gFnSc2	zbraň
jednoduše	jednoduše	k6eAd1	jednoduše
natáhne	natáhnout	k5eAaPmIp3nS	natáhnout
náboj	náboj	k1gInSc1	náboj
do	do	k7c2	do
komory	komora	k1gFnSc2	komora
a	a	k8xC	a
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
dalšího	další	k2eAgNnSc2d1	další
se	se	k3xPyFc4	se
nestará	starat	k5eNaImIp3nS	starat
–	–	k?	–
zbraň	zbraň	k1gFnSc1	zbraň
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
neustále	neustále	k6eAd1	neustále
vypuštěným	vypuštěný	k2eAgInSc7d1	vypuštěný
bicím	bicí	k2eAgInSc7d1	bicí
mechanizmem	mechanizmus	k1gInSc7	mechanizmus
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
a	a	k8xC	a
přesto	přesto	k8xC	přesto
neustále	neustále	k6eAd1	neustále
v	v	k7c6	v
pohotovosti	pohotovost	k1gFnSc6	pohotovost
–	–	k?	–
stačí	stačit	k5eAaBmIp3nS	stačit
pouze	pouze	k6eAd1	pouze
stisknout	stisknout	k5eAaPmF	stisknout
spoušť	spoušť	k1gFnSc4	spoušť
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
přes	přes	k7c4	přes
větší	veliký	k2eAgInSc4d2	veliký
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
odpadá	odpadat	k5eAaImIp3nS	odpadat
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
další	další	k2eAgFnSc1d1	další
manipulace	manipulace	k1gFnSc1	manipulace
se	s	k7c7	s
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
přesnost	přesnost	k1gFnSc1	přesnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
chod	chod	k1gInSc1	chod
spouště	spoušť	k1gFnSc2	spoušť
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
střelce	střelec	k1gMnPc4	střelec
ke	k	k7c3	k
strhnutí	strhnutí	k1gNnSc3	strhnutí
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
obranné	obranný	k2eAgFnSc2d1	obranná
střelby	střelba	k1gFnSc2	střelba
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
DAO	DAO	kA	DAO
pistolí	pistol	k1gFnSc7	pistol
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
CZ	CZ	kA	CZ
100	[number]	k4	100
<g/>
,	,	kIx,	,
CZ	CZ	kA	CZ
92	[number]	k4	92
<g/>
,	,	kIx,	,
CZ	CZ	kA	CZ
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
variantu	varianta	k1gFnSc4	varianta
DAO	DAO	kA	DAO
spouště	spoušť	k1gFnSc2	spoušť
představuje	představovat	k5eAaImIp3nS	představovat
systém	systém	k1gInSc1	systém
DAK	DAK	kA	DAK
–	–	k?	–
Double	double	k2eAgInSc4d1	double
Action	Action	k1gInSc4	Action
Kellerman	Kellerman	k1gMnSc1	Kellerman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
u	u	k7c2	u
pistolí	pistol	k1gFnPc2	pistol
SIG	SIG	kA	SIG
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
klasický	klasický	k2eAgInSc1d1	klasický
DAO	DAO	kA	DAO
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
vyladěný	vyladěný	k2eAgMnSc1d1	vyladěný
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpor	odpor	k1gInSc1	odpor
spouště	spoušť	k1gFnSc2	spoušť
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
30	[number]	k4	30
Newtonů	newton	k1gInPc2	newton
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
předepnutý	předepnutý	k2eAgInSc1d1	předepnutý
mechanismus	mechanismus	k1gInSc1	mechanismus
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
samonabíjecích	samonabíjecí	k2eAgFnPc2d1	samonabíjecí
pistolí	pistol	k1gFnPc2	pistol
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
výstřelem	výstřel	k1gInSc7	výstřel
je	být	k5eAaImIp3nS	být
bicí	bicí	k2eAgInSc1d1	bicí
mechanizmus	mechanizmus	k1gInSc1	mechanizmus
částečně	částečně	k6eAd1	částečně
napnut	napnut	k2eAgInSc1d1	napnut
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
při	při	k7c6	při
natažení	natažení	k1gNnSc6	natažení
závěru	závěr	k1gInSc2	závěr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stisku	stisk	k1gInSc6	stisk
spouště	spoušť	k1gFnSc2	spoušť
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
napíná	napínat	k5eAaImIp3nS	napínat
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
uvolněn	uvolněn	k2eAgInSc1d1	uvolněn
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
u	u	k7c2	u
DA	DA	kA	DA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Částečné	částečný	k2eAgNnSc1d1	částečné
předepnutí	předepnutí	k1gNnSc1	předepnutí
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
modifikace	modifikace	k1gFnSc1	modifikace
DAO	DAO	kA	DAO
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
příliš	příliš	k6eAd1	příliš
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
chod	chod	k1gInSc4	chod
spouště	spoušť	k1gFnSc2	spoušť
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
tohoto	tento	k3xDgNnSc2	tento
řešení	řešení	k1gNnSc2	řešení
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
Glock	Glocka	k1gFnPc2	Glocka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
používá	používat	k5eAaImIp3nS	používat
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
pistolí	pistol	k1gFnPc2	pistol
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Safe	safe	k1gInSc1	safe
Action	Action	k1gInSc1	Action
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
při	při	k7c6	při
výstřelu	výstřel	k1gInSc6	výstřel
selže	selhat	k5eAaPmIp3nS	selhat
náboj	náboj	k1gInSc1	náboj
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vystřelit	vystřelit	k5eAaPmF	vystřelit
znovu	znovu	k6eAd1	znovu
promáčknutím	promáčknutí	k1gNnSc7	promáčknutí
spouště	spoušť	k1gFnSc2	spoušť
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Glock	Glock	k1gInSc4	Glock
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
předepnutý	předepnutý	k2eAgInSc1d1	předepnutý
hybridní	hybridní	k2eAgInSc1d1	hybridní
mechanismus	mechanismus	k1gInSc1	mechanismus
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
mechanismu	mechanismus	k1gInSc2	mechanismus
Safe	safe	k1gInSc1	safe
Action	Action	k1gInSc1	Action
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
při	při	k7c6	při
výstřelu	výstřel	k1gInSc6	výstřel
selže	selhat	k5eAaPmIp3nS	selhat
náboj	náboj	k1gInSc1	náboj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
promáčknout	promáčknout	k5eAaPmF	promáčknout
spoušť	spoušť	k1gFnSc4	spoušť
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
DA	DA	kA	DA
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
nabízí	nabízet	k5eAaImIp3nS	nabízet
např.	např.	kA	např.
nový	nový	k2eAgInSc1d1	nový
model	model	k1gInSc1	model
Taurus	Taurus	k1gInSc1	Taurus
24	[number]	k4	24
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
jednoranové	jednoranový	k2eAgNnSc1d1	jednoranový
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
buďto	buďto	k8xC	buďto
o	o	k7c6	o
kapesní	kapesní	k2eAgFnSc6d1	kapesní
pistole	pistola	k1gFnSc6	pistola
pro	pro	k7c4	pro
sebeobranu	sebeobrana	k1gFnSc4	sebeobrana
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc4d1	zvaná
též	též	k6eAd1	též
derringery	derringera	k1gFnPc4	derringera
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jednohlavňové	jednohlavňový	k2eAgInPc1d1	jednohlavňový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
častěji	často	k6eAd2	často
dvou	dva	k4xCgFnPc2	dva
či	či	k8xC	či
čtyřhlavňové	čtyřhlavňový	k2eAgInPc1d1	čtyřhlavňový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
velké	velký	k2eAgNnSc4d1	velké
<g />
.	.	kIx.	.
</s>
<s>
pistole	pistole	k1gFnSc1	pistole
na	na	k7c4	na
výkonné	výkonný	k2eAgInPc4d1	výkonný
náboje	náboj	k1gInPc4	náboj
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
i	i	k9	i
puškové	puškový	k2eAgFnPc1d1	pušková
<g/>
)	)	kIx)	)
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
zvěře	zvěř	k1gFnSc2	zvěř
(	(	kIx(	(
<g/>
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
to	ten	k3xDgNnSc1	ten
povoluje	povolovat	k5eAaImIp3nS	povolovat
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
ke	k	k7c3	k
sportu	sport	k1gInSc3	sport
<g/>
.	.	kIx.	.
samonabíjecí	samonabíjecí	k2eAgFnSc3d1	samonabíjecí
–	–	k?	–
po	po	k7c6	po
stisknutí	stisknutí	k1gNnSc6	stisknutí
spouště	spoušť	k1gFnSc2	spoušť
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
výstřelu	výstřel	k1gInSc3	výstřel
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
samočinnému	samočinný	k2eAgNnSc3d1	samočinné
natažení	natažení	k1gNnSc3	natažení
náboje	náboj	k1gInSc2	náboj
do	do	k7c2	do
komory	komora	k1gFnSc2	komora
a	a	k8xC	a
napnutí	napnutí	k1gNnSc2	napnutí
bicího	bicí	k2eAgInSc2d1	bicí
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
.	.	kIx.	.
samočinné	samočinný	k2eAgFnSc3d1	samočinná
(	(	kIx(	(
<g/>
automatické	automatický	k2eAgFnSc3d1	automatická
<g/>
)	)	kIx)	)
–	–	k?	–
umožňující	umožňující	k2eAgFnSc1d1	umožňující
přidržením	přidržení	k1gNnSc7	přidržení
spouště	spoušť	k1gFnSc2	spoušť
střelbu	střelba	k1gFnSc4	střelba
dávkou	dávka	k1gFnSc7	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
výrobcem	výrobce	k1gMnSc7	výrobce
ručních	ruční	k2eAgFnPc2d1	ruční
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
pistolí	pistol	k1gFnPc2	pistol
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
Česká	český	k2eAgFnSc1d1	Česká
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
větší	veliký	k2eAgFnSc1d2	veliký
rychlost	rychlost	k1gFnSc1	rychlost
střelby	střelba	k1gFnSc2	střelba
u	u	k7c2	u
průměrně	průměrně	k6eAd1	průměrně
trénovaného	trénovaný	k2eAgMnSc2d1	trénovaný
střelce	střelec	k1gMnSc2	střelec
rychlejší	rychlý	k2eAgNnSc4d2	rychlejší
přebíjení	přebíjení	k1gNnSc4	přebíjení
u	u	k7c2	u
průměrně	průměrně	k6eAd1	průměrně
trénovaného	trénovaný	k2eAgMnSc2d1	trénovaný
střelce	střelec	k1gMnSc2	střelec
menší	malý	k2eAgFnSc1d2	menší
šířka	šířka	k1gFnSc1	šířka
větší	veliký	k2eAgFnSc1d2	veliký
kapacita	kapacita	k1gFnSc1	kapacita
zásobníku	zásobník	k1gInSc2	zásobník
menší	malý	k2eAgInSc1d2	menší
zpětný	zpětný	k2eAgInSc1d1	zpětný
ráz	ráz	k1gInSc1	ráz
a	a	k8xC	a
zdvih	zdvih	k1gInSc1	zdvih
relativně	relativně	k6eAd1	relativně
složitější	složitý	k2eAgNnSc4d2	složitější
odstraňování	odstraňování	k1gNnSc4	odstraňování
poruch	porucha	k1gFnPc2	porucha
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
střelby	střelba	k1gFnSc2	střelba
teoretická	teoretický	k2eAgFnSc1d1	teoretická
menší	malý	k2eAgFnSc1d2	menší
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
(	(	kIx(	(
<g/>
složitější	složitý	k2eAgInSc1d2	složitější
mechanizmus	mechanizmus	k1gInSc1	mechanizmus
<g/>
,	,	kIx,	,
znovunabití	znovunabití	k1gNnSc1	znovunabití
závisí	záviset	k5eAaImIp3nS	záviset
od	od	k7c2	od
energie	energie	k1gFnSc2	energie
předchozího	předchozí	k2eAgInSc2d1	předchozí
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
)	)	kIx)	)
revolvery	revolver	k1gInPc4	revolver
typicky	typicky	k6eAd1	typicky
používají	používat	k5eAaImIp3nP	používat
silnější	silný	k2eAgNnSc4d2	silnější
střelivo	střelivo	k1gNnSc4	střelivo
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pistole	pistol	k1gFnSc2	pistol
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pistole	pistol	k1gFnSc2	pistol
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
