<p>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Emil	Emil	k1gMnSc1	Emil
Heinrich	Heinrich	k1gMnSc1	Heinrich
Biberstein	Biberstein	k1gMnSc1	Biberstein
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
jako	jako	k8xC	jako
Ernst	Ernst	k1gMnSc1	Ernst
Szymanowski	Szymanowsk	k1gFnSc2	Szymanowsk
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Hilchenbach	Hilchenbach	k1gInSc1	Hilchenbach
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Neumünster	Neumünster	k1gMnSc1	Neumünster
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
evangelický	evangelický	k2eAgMnSc1d1	evangelický
pastor	pastor	k1gMnSc1	pastor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
SS	SS	kA	SS
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
hodnosti	hodnost	k1gFnPc4	hodnost
Obersturmbannführera	Obersturmbannführero	k1gNnSc2	Obersturmbannführero
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
6	[number]	k4	6
<g/>
.	.	kIx.	.
oddílu	oddíl	k1gInSc2	oddíl
skupiny	skupina	k1gFnSc2	skupina
C	C	kA	C
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
jednotek	jednotka	k1gFnPc2	jednotka
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vraždily	vraždit	k5eAaImAgFnP	vraždit
civilní	civilní	k2eAgNnSc4d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
za	za	k7c7	za
frontou	fronta	k1gFnSc7	fronta
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
Židy	Žid	k1gMnPc4	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
oddíl	oddíl	k1gInSc1	oddíl
zavraždil	zavraždit	k5eAaPmAgInS	zavraždit
dva	dva	k4xCgInPc1	dva
až	až	k9	až
tři	tři	k4xCgInPc1	tři
tisíce	tisíc	k4xCgInPc1	tisíc
Židů	Žid	k1gMnPc2	Žid
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
před	před	k7c4	před
americký	americký	k2eAgInSc4d1	americký
vojenský	vojenský	k2eAgInSc4d1	vojenský
tribunál	tribunál	k1gInSc4	tribunál
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
procesu	proces	k1gInSc2	proces
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc4	příslušník
Einsatzgruppen	Einsatzgruppen	k2eAgMnSc1d1	Einsatzgruppen
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
shledán	shledat	k5eAaPmNgMnS	shledat
vinným	vinný	k1gMnSc7	vinný
zločiny	zločin	k1gInPc7	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
,	,	kIx,	,
válečnými	válečný	k2eAgInPc7d1	válečný
zločiny	zločin	k1gInPc7	zločin
a	a	k8xC	a
členstvím	členství	k1gNnSc7	členství
ve	v	k7c6	v
zločineckých	zločinecký	k2eAgFnPc6d1	zločinecká
organizacích	organizace	k1gFnPc6	organizace
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1948	[number]	k4	1948
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
cestou	cestou	k7c2	cestou
milosti	milost	k1gFnSc2	milost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
změněn	změnit	k5eAaPmNgMnS	změnit
na	na	k7c4	na
doživotní	doživotní	k2eAgNnPc4d1	doživotní
vězení	vězení	k1gNnPc4	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dostal	dostat	k5eAaPmAgMnS	dostat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ernst	Ernst	k1gMnSc1	Ernst
Biberstein	Biberstein	k1gMnSc1	Biberstein
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Stephan	Stephan	k1gMnSc1	Stephan
Linck	Linck	k1gMnSc1	Linck
<g/>
:	:	kIx,	:
Ernst	Ernst	k1gMnSc1	Ernst
Symanowski	Symanowsk	k1gFnSc2	Symanowsk
alias	alias	k9	alias
Biberstein	Biberstein	k1gMnSc1	Biberstein
–	–	k?	–
Ein	Ein	k1gMnSc1	Ein
Theologe	Theolog	k1gFnSc2	Theolog
auf	auf	k?	auf
Abwegen	Abwegen	k1gInSc1	Abwegen
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Klaus-Michael	Klaus-Michael	k1gMnSc1	Klaus-Michael
Mallmann	Mallmann	k1gMnSc1	Mallmann
<g/>
,	,	kIx,	,
Gerhard	Gerhard	k1gMnSc1	Gerhard
Paul	Paul	k1gMnSc1	Paul
a	a	k8xC	a
kolektiv	kolektivum	k1gNnPc2	kolektivum
<g/>
:	:	kIx,	:
Karrieren	Karrierna	k1gFnPc2	Karrierna
der	drát	k5eAaImRp2nS	drát
Gewalt	Gewalt	k1gInSc1	Gewalt
–	–	k?	–
Nationalsozialistische	Nationalsozialistische	k1gInSc1	Nationalsozialistische
Täterbiographien	Täterbiographien	k1gInSc1	Täterbiographien
<g/>
,	,	kIx,	,
Wissenschaftliche	Wissenschaftliche	k1gInSc1	Wissenschaftliche
Buchgesellschaft	Buchgesellschaft	k1gInSc1	Buchgesellschaft
<g/>
,	,	kIx,	,
Darmstadt	Darmstadt	k1gInSc1	Darmstadt
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-534-16654-X	[number]	k4	3-534-16654-X
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Bibersteins	Bibersteins	k1gInSc1	Bibersteins
eidestattliche	eidestattlich	k1gFnSc2	eidestattlich
Erklärung	Erklärung	k1gMnSc1	Erklärung
über	über	k1gMnSc1	über
seine	seinout	k5eAaPmIp3nS	seinout
Tätigkeit	Tätigkeit	k1gInSc4	Tätigkeit
im	im	k?	im
Einsatzkommando	Einsatzkommanda	k1gFnSc5	Einsatzkommanda
6	[number]	k4	6
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Pastoren	Pastorna	k1gFnPc2	Pastorna
unterm	unterm	k1gInSc1	unterm
Hakenkreuz	Hakenkreuz	k1gMnSc1	Hakenkreuz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Nordelbische	Nordelbische	k1gNnSc1	Nordelbische
Evangelisch-Lutherischen	Evangelisch-Lutherischna	k1gFnPc2	Evangelisch-Lutherischna
Kirche	Kirche	k1gNnSc2	Kirche
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
Biographie	Biographie	k1gFnSc1	Biographie
und	und	k?	und
Bild	Bild	k1gInSc1	Bild
von	von	k1gInSc1	von
Biberstein	Biberstein	k2eAgInSc1d1	Biberstein
</s>
</p>
