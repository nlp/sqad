<s>
Byl	být	k5eAaImAgMnS	být
shledán	shledat	k5eAaPmNgMnS	shledat
vinným	vinný	k1gMnSc7	vinný
zločiny	zločin	k1gInPc7	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
,	,	kIx,	,
válečnými	válečný	k2eAgInPc7d1	válečný
zločiny	zločin	k1gInPc7	zločin
a	a	k8xC	a
členstvím	členství	k1gNnSc7	členství
ve	v	k7c6	v
zločineckých	zločinecký	k2eAgFnPc6d1	zločinecká
organizacích	organizace	k1gFnPc6	organizace
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1948	[number]	k4	1948
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
cestou	cestou	k7c2	cestou
milosti	milost	k1gFnSc2	milost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
změněn	změnit	k5eAaPmNgMnS	změnit
na	na	k7c4	na
doživotní	doživotní	k2eAgNnPc4d1	doživotní
vězení	vězení	k1gNnPc4	vězení
<g/>
.	.	kIx.	.
</s>
