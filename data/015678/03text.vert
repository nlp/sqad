<s>
Podsvětí	podsvětí	k1gNnSc1
</s>
<s>
Jeden	jeden	k4xCgInSc4
nebo	nebo	k8xC
více	hodně	k6eAd2
uživatelů	uživatel	k1gMnPc2
zpochybnili	zpochybnit	k5eAaPmAgMnP
nezaujatost	nezaujatost	k1gFnSc4
<g/>
,	,	kIx,
ověřitelnost	ověřitelnost	k1gFnSc4
<g/>
,	,	kIx,
faktickou	faktický	k2eAgFnSc4d1
přesnost	přesnost	k1gFnSc4
nebo	nebo	k8xC
encyklopedičnost	encyklopedičnost	k1gFnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k9
další	další	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
uvedeny	uveden	k2eAgInPc1d1
další	další	k2eAgInPc1d1
detaily	detail	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
vkladatele	vkladatel	k1gMnPc4
šablony	šablona	k1gFnSc2
<g/>
:	:	kIx,
Na	na	k7c6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
zdůvodněte	zdůvodnit	k5eAaPmRp2nP
vložení	vložení	k1gNnSc4
šablony	šablona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podsvětí	podsvětí	k1gNnSc1
či	či	k8xC
též	též	k9
říše	říše	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
náboženských	náboženský	k2eAgFnPc6d1
či	či	k8xC
mytologických	mytologický	k2eAgFnPc6d1
představách	představa	k1gFnPc6
místo	místo	k7c2
duší	duše	k1gFnPc2
v	v	k7c6
posmrtném	posmrtný	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
na	na	k7c6
západě	západ	k1gInSc6
(	(	kIx(
<g/>
symbolická	symbolický	k2eAgFnSc1d1
souvislost	souvislost	k1gFnSc1
se	se	k3xPyFc4
západem	západ	k1gInSc7
slunce	slunce	k1gNnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
v	v	k7c6
podzemí	podzemí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Smrt	smrt	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
spojována	spojovat	k5eAaImNgFnS
s	s	k7c7
posmrtným	posmrtný	k2eAgInSc7d1
soudem	soud	k1gInSc7
zemřelých	zemřelý	k1gMnPc2
<g/>
,	,	kIx,
vykonávaným	vykonávaný	k2eAgInSc7d1
bohy	bůh	k1gMnPc7
nebo	nebo	k8xC
soudci	soudce	k1gMnPc7
k	k	k7c3
tomu	ten	k3xDgNnSc3
povolanými	povolaný	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdějších	pozdní	k2eAgFnPc6d2
náboženských	náboženský	k2eAgFnPc6d1
představách	představa	k1gFnPc6
a	a	k8xC
směrech	směr	k1gInPc6
(	(	kIx(
<g/>
křesťanství	křesťanství	k1gNnSc4
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
,	,	kIx,
pozdní	pozdní	k2eAgInSc1d1
judaismus	judaismus	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
mytologický	mytologický	k2eAgInSc1d1
obraz	obraz	k1gInSc1
podsvětí	podsvětí	k1gNnSc2
spojen	spojit	k5eAaPmNgInS
<g/>
,	,	kIx,
respektive	respektive	k9
nahrazen	nahradit	k5eAaPmNgInS
vizí	vize	k1gFnSc7
pekla	peklo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Charónův	Charónův	k2eAgInSc1d1
člun	člun	k1gInSc1
–	–	k?
Aeneas	Aeneas	k1gMnSc1
a	a	k8xC
Sibilla	Sibilla	k1gMnSc1
v	v	k7c6
podsvětí	podsvětí	k1gNnSc6
(	(	kIx(
<g/>
Jacob	Jacoba	k1gFnPc2
van	vana	k1gFnPc2
Swanenburgh	Swanenburgh	k1gInSc4
okolo	okolo	k7c2
r.	r.	kA
1625	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Starověké	starověký	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Řecké	řecký	k2eAgNnSc1d1
podsvětí	podsvětí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
ostatní	ostatní	k2eAgInPc1d1
starověké	starověký	k2eAgInPc1d1
národy	národ	k1gInPc1
východního	východní	k2eAgNnSc2d1
Středomoří	středomoří	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
chápali	chápat	k5eAaImAgMnP
Řekové	Řek	k1gMnPc1
a	a	k8xC
Římani	říman	k1gMnPc1
posmrtný	posmrtný	k2eAgInSc4d1
život	život	k1gInSc4
jen	jen	k9
jako	jako	k8xS,k8xC
stínovou	stínový	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
v	v	k7c6
podsvětí	podsvětí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
řecké	řecký	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
je	být	k5eAaImIp3nS
popisováno	popisovat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
podzemní	podzemní	k2eAgFnSc2d1
říše	říš	k1gFnSc2
obklopená	obklopený	k2eAgFnSc1d1
řekami	řeka	k1gFnPc7
Styx	Styx	k1gInSc1
<g/>
,	,	kIx,
Acherón	Acherón	k1gMnSc1
<g/>
,	,	kIx,
Kokýtos	Kokýtos	k1gMnSc1
<g/>
,	,	kIx,
Pyriflegethón	Pyriflegethón	k1gMnSc1
a	a	k8xC
Léthé	Léthé	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládne	vládnout	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
bůh	bůh	k1gMnSc1
Hádés	Hádés	k1gInSc1
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
Dia	Dia	k1gMnSc1
<g/>
,	,	kIx,
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
Persefonou	Persefona	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
vstupu	vstup	k1gInSc2
do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
projde	projít	k5eAaPmIp3nS
duše	duše	k1gFnSc1
kolem	kolem	k7c2
trojhlavého	trojhlavý	k2eAgMnSc2d1
psa	pes	k1gMnSc2
Kerbera	Kerber	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
do	do	k7c2
říše	říš	k1gFnSc2
mrtvých	mrtvý	k1gMnPc2
nepustí	pustit	k5eNaPmIp3nP
nikoho	nikdo	k3yNnSc4
živého	živý	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duše	duše	k1gFnSc1
poté	poté	k6eAd1
potká	potkat	k5eAaPmIp3nS
převozníka	převozník	k1gMnSc4
Charóna	Charón	k1gMnSc4
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
musí	muset	k5eAaImIp3nS
zaplatit	zaplatit	k5eAaPmF
dvěma	dva	k4xCgFnPc7
mincemi	mince	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převozník	převozník	k1gMnSc1
ji	on	k3xPp3gFnSc4
převeze	převézt	k5eAaPmIp3nS
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Styx	Styx	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
se	se	k3xPyFc4
napije	napít	k5eAaBmIp3nS,k5eAaPmIp3nS
z	z	k7c2
řeky	řeka	k1gFnSc2
Léthé	Léthé	k1gFnSc2
a	a	k8xC
zapomene	zapomnět	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
pozemský	pozemský	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soud	soud	k1gInSc1
nad	nad	k7c7
zemřelými	zemřelá	k1gFnPc7
vykonávají	vykonávat	k5eAaImIp3nP
Aikos	Aikos	k1gInSc4
<g/>
,	,	kIx,
Mínós	Mínós	k1gInSc4
a	a	k8xC
Rhadamanthys	Rhadamanthys	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podzemí	podzemí	k1gNnSc6
se	se	k3xPyFc4
většina	většina	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
potuluje	potulovat	k5eAaImIp3nS
bez	bez	k7c2
radosti	radost	k1gFnSc2
a	a	k8xC
žalu	žal	k1gInSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
stínu	stín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgMnPc1d3
hříšníci	hříšník	k1gMnPc1
si	se	k3xPyFc3
odpykávají	odpykávat	k5eAaImIp3nP
trest	trest	k1gInSc1
udělený	udělený	k2eAgInSc1d1
bohy	bůh	k1gMnPc7
v	v	k7c6
Tartaru	Tartar	k1gInSc6
a	a	k8xC
duše	duše	k1gFnSc2
spravedlivých	spravedlivý	k2eAgFnPc2d1
odpočívali	odpočívat	k5eAaImAgMnP
na	na	k7c6
Elysejských	elysejský	k2eAgNnPc6d1
polích	pole	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Starověký	starověký	k2eAgInSc1d1
Řím	Řím	k1gInSc1
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
podsvětí	podsvětí	k1gNnSc1
od	od	k7c2
antického	antický	k2eAgMnSc2d1
umělce	umělec	k1gMnSc2
(	(	kIx(
<g/>
Mnichovská	mnichovský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
starověkých	starověký	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
Římské	římský	k2eAgNnSc1d1
podsvětí	podsvětí	k1gNnSc1
je	být	k5eAaImIp3nS
podobné	podobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
odlišuje	odlišovat	k5eAaImIp3nS
se	se	k3xPyFc4
hlavně	hlavně	k9
v	v	k7c6
pojmenování	pojmenování	k1gNnSc3
některých	některý	k3yIgNnPc2
míst	místo	k1gNnPc2
a	a	k8xC
bohů	bůh	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
říši	říše	k1gFnSc4
mrtvých	mrtvý	k1gMnPc2
vládne	vládnout	k5eAaImIp3nS
Plutos	Plutos	k1gInSc1
a	a	k8xC
Proserpina	Proserpina	k1gFnSc1
a	a	k8xC
stráží	strážit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
Cerber	Cerber	k1gMnSc1
<g/>
(	(	kIx(
<g/>
us	us	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Detailnější	detailní	k2eAgInSc4d2
popis	popis	k1gInSc4
římských	římský	k2eAgFnPc2d1
představ	představa	k1gFnPc2
o	o	k7c4
podsvětí	podsvětí	k1gNnSc4
nacházíme	nacházet	k5eAaImIp1nP
ve	v	k7c6
Vergiliově	Vergiliův	k2eAgFnSc6d1
Aeneidě	Aeneida	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aeneas	Aeneas	k1gMnSc1
do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
sestupuje	sestupovat	k5eAaImIp3nS
pod	pod	k7c7
ochrannou	ochranný	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
zlaté	zlatý	k2eAgFnSc2d1
ratolesti	ratolest	k1gFnSc2
a	a	k8xC
se	s	k7c7
schopnostmi	schopnost	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
mu	on	k3xPp3gNnSc3
dala	dát	k5eAaPmAgFnS
Sibyla	Sibyla	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
šesté	šestý	k4xOgFnSc6
knize	kniha	k1gFnSc6
Vergiliova	Vergiliův	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odděleny	oddělen	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
duše	duše	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
zahynuly	zahynout	k5eAaPmAgFnP
v	v	k7c6
dětství	dětství	k1gNnSc6
či	či	k8xC
sebevraždou	sebevražda	k1gFnSc7
<g/>
,	,	kIx,
dále	daleko	k6eAd2
stíny	stín	k1gInPc1
zemřelých	zemřelý	k1gMnPc2
z	z	k7c2
nešťastné	šťastný	k2eNgFnSc2d1
lásky	láska	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgInPc7
Aeneas	Aeneas	k1gMnSc1
potkává	potkávat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
milenku	milenka	k1gFnSc4
nešťastnou	šťastný	k2eNgFnSc4d1
Dido	Dido	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
příbytcích	příbytek	k1gInPc6
blažených	blažený	k2eAgInPc6d1
pak	pak	k6eAd1
potkává	potkávat	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
mu	on	k3xPp3gMnSc3
vysvětlí	vysvětlit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
údolí	údolí	k1gNnSc6
Léthé	Léthé	k1gFnSc2
se	se	k3xPyFc4
duše	duše	k1gFnPc1
se	se	k3xPyFc4
očišťují	očišťovat	k5eAaImIp3nP
a	a	k8xC
připravují	připravovat	k5eAaImIp3nP
k	k	k7c3
novému	nový	k2eAgNnSc3d1
zrození	zrození	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Homérova	Homérův	k2eAgInSc2d1
popisu	popis	k1gInSc2
nešťastných	šťastný	k2eNgFnPc2d1
duší	duše	k1gFnPc2
litujících	litující	k2eAgFnPc2d1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
nejsou	být	k5eNaImIp3nP
mezi	mezi	k7c7
živými	živá	k1gFnPc7
<g/>
,	,	kIx,
nacházíme	nacházet	k5eAaImIp1nP
tedy	tedy	k9
v	v	k7c6
Aeneovi	Aeneas	k1gMnSc6
popis	popis	k1gInSc4
jakéhosi	jakýsi	k3yIgNnSc2
směřování	směřování	k1gNnSc2
<g/>
,	,	kIx,
čehosi	cosi	k3yInSc2
mezi	mezi	k7c7
Elysejskými	elysejský	k2eAgNnPc7d1
poli	pole	k1gNnPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
tráví	trávit	k5eAaImIp3nP
čas	čas	k1gInSc1
hrdinové	hrdina	k1gMnPc1
a	a	k8xC
Tartarem	Tartar	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
úpí	úpět	k5eAaImIp3nS
zrádci	zrádce	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anchíses	Anchíses	k1gInSc4
o	o	k7c6
něm	on	k3xPp3gMnSc6
Aeneovi	Aeneas	k1gMnSc6
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
Kerberos	Kerberos	k1gMnSc1
(	(	kIx(
<g/>
Cerberus	Cerberus	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
strážce	strážce	k1gMnSc1
podsvětí	podsvětí	k1gNnSc1
<g/>
„	„	k?
<g/>
…	…	k?
Proto	proto	k8xC
je	být	k5eAaImIp3nS
bolestné	bolestný	k2eAgNnSc1d1
očišťování	očišťování	k1gNnSc1
a	a	k8xC
za	za	k7c4
dávné	dávný	k2eAgInPc4d1
hříchy	hřích	k1gInPc4
</s>
<s>
Snášejí	snášet	k5eAaImIp3nP
všelijaké	všelijaký	k3yIgInPc4
tresty	trest	k1gInPc4
<g/>
:	:	kIx,
z	z	k7c2
nich	on	k3xPp3gFnPc2
některé	některý	k3yIgFnPc1
rozpjaty	rozpjaty	k?
visí	viset	k5eAaImIp3nP
</s>
<s>
Naproti	naproti	k7c3
prázdným	prázdný	k2eAgInPc3d1
větrům	vítr	k1gInPc3
a	a	k8xC
jiným	jiný	k1gMnPc3
je	být	k5eAaImIp3nS
vymýván	vymýván	k2eAgInSc1d1
zločin	zločin	k1gInSc1
</s>
<s>
Pod	pod	k7c7
dravým	dravý	k2eAgInSc7d1
vírem	vír	k1gInSc7
neb	neb	k8xC
také	také	k9
účinkem	účinek	k1gInSc7
ohně	oheň	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Všichni	všechen	k3xTgMnPc1
ti	ty	k3xPp2nSc3
vlastní	vlastnit	k5eAaImIp3nP
svou	svůj	k3xOyFgFnSc4
očistu	očista	k1gFnSc4
máme	mít	k5eAaImIp1nP
<g/>
,	,	kIx,
pak	pak	k6eAd1
do	do	k7c2
rozsáhlého	rozsáhlý	k2eAgInSc2d1
</s>
<s>
Élysia	Élysia	k1gFnSc1
nás	my	k3xPp1nPc4
pošlou	poslat	k5eAaPmIp3nP
<g/>
,	,	kIx,
však	však	k9
málo	málo	k6eAd1
nás	my	k3xPp1nPc4
zůstane	zůstat	k5eAaPmIp3nS
…	…	k?
<g/>
“	“	k?
</s>
<s>
Tato	tento	k3xDgFnSc1
představa	představa	k1gFnSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
evropský	evropský	k2eAgInSc4d1
středověk	středověk	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
představy	představa	k1gFnPc1
spojené	spojený	k2eAgFnPc1d1
se	s	k7c7
záhrobím	záhrobí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Předkřesťanské	předkřesťanský	k2eAgNnSc1d1
podsvětí	podsvětí	k1gNnSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
byla	být	k5eAaImAgFnS
obydlena	obydlet	k5eAaPmNgFnS
řadou	řada	k1gFnSc7
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
si	se	k3xPyFc3
vytvářely	vytvářet	k5eAaImAgFnP
své	svůj	k3xOyFgFnPc4
vlastní	vlastní	k2eAgFnPc4d1
představy	představa	k1gFnPc4
života	život	k1gInSc2
po	po	k7c6
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc5
kopírovaly	kopírovat	k5eAaImAgFnP
drsný	drsný	k2eAgInSc4d1
život	život	k1gInSc4
těchto	tento	k3xDgInPc2
kmenů	kmen	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
spojení	spojení	k1gNnSc1
s	s	k7c7
přírodou	příroda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Zmínit	zmínit	k5eAaPmF
lze	lze	k6eAd1
severské	severský	k2eAgFnPc4d1
báje	báj	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
nacházíme	nacházet	k5eAaImIp1nP
říši	říše	k1gFnSc4
mrtvých	mrtvý	k1gMnPc2
pod	pod	k7c4
kořeny	kořen	k1gInPc4
svět	svět	k1gInSc4
prokládajícího	prokládající	k2eAgInSc2d1
jasanu	jasan	k1gInSc2
Yggdrasilu	Yggdrasil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládne	vládnout	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
Lokiho	Loki	k1gMnSc2
dcera	dcera	k1gFnSc1
Hela	Hela	k1gFnSc1
a	a	k8xC
putují	putovat	k5eAaImIp3nP
sem	sem	k6eAd1
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
zemřeli	zemřít	k5eAaPmAgMnP
náhodou	náhoda	k1gFnSc7
<g/>
,	,	kIx,
nemocí	nemoc	k1gFnSc7
či	či	k8xC
věkem	věk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnoty	hodnota	k1gFnSc2
vojensky	vojensky	k6eAd1
nepochybně	pochybně	k6eNd1
velice	velice	k6eAd1
aktivních	aktivní	k2eAgInPc2d1
severských	severský	k2eAgInPc2d1
národů	národ	k1gInPc2
lze	lze	k6eAd1
vysledovat	vysledovat	k5eAaImF,k5eAaPmF
v	v	k7c6
posmrtném	posmrtný	k2eAgInSc6d1
osudu	osud	k1gInSc6
hrdinů	hrdina	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
ztratili	ztratit	k5eAaPmAgMnP
v	v	k7c6
bitvách	bitva	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
hodují	hodovat	k5eAaImIp3nP
ve	v	k7c6
Valhale	Valhala	k1gFnSc6
<g/>
,	,	kIx,
jedné	jeden	k4xCgFnSc2
z	z	k7c2
Ódinových	Ódinův	k2eAgFnPc2d1
síní	síň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1
a	a	k8xC
podsvětí	podsvětí	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Peklo	peklo	k1gNnSc1
a	a	k8xC
Očistec	očistec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Křesťanské	křesťanský	k2eAgFnPc1d1
představy	představa	k1gFnPc1
neoperují	operovat	k5eNaImIp3nP
přímo	přímo	k6eAd1
s	s	k7c7
podsvětím	podsvětí	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
nebem	nebe	k1gNnSc7
<g/>
,	,	kIx,
peklem	peklo	k1gNnSc7
a	a	k8xC
očistcem	očistec	k1gInSc7
<g/>
,	,	kIx,
očistec	očistec	k1gInSc1
i	i	k8xC
peklo	peklo	k1gNnSc1
mají	mít	k5eAaImIp3nP
ale	ale	k9
přes	přes	k7c4
významové	významový	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
s	s	k7c7
podsvětím	podsvětí	k1gNnSc7
mnoho	mnoho	k6eAd1
společného	společný	k2eAgNnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
V	v	k7c6
křesťanství	křesťanství	k1gNnSc6
zřetelně	zřetelně	k6eAd1
vystupuje	vystupovat	k5eAaImIp3nS
protiklad	protiklad	k1gInSc1
dobra	dobro	k1gNnSc2
<g/>
,	,	kIx,
vedoucího	vedoucí	k2eAgInSc2d1
ke	k	k7c3
spasení	spasení	k1gNnSc3
a	a	k8xC
tedy	tedy	k9
do	do	k7c2
nebe	nebe	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
zla	zlo	k1gNnSc2
<g/>
,	,	kIx,
vedoucího	vedoucí	k2eAgInSc2d1
k	k	k7c3
zatracení	zatracení	k1gNnSc3
do	do	k7c2
pekla	peklo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nebi	nebe	k1gNnSc6
zemřelého	zemřelý	k1gMnSc2
čeká	čekat	k5eAaImIp3nS
blažený	blažený	k2eAgInSc4d1
život	život	k1gInSc4
a	a	k8xC
patření	patření	k1gNnSc4
na	na	k7c4
Boha	bůh	k1gMnSc4
<g/>
,	,	kIx,
o	o	k7c6
záchraně	záchrana	k1gFnSc6
vyvolených	vyvolený	k2eAgMnPc2d1
a	a	k8xC
mukách	muka	k1gFnPc6
zatracených	zatracený	k2eAgFnPc6d1
barvitě	barvitě	k6eAd1
mluví	mluvit	k5eAaImIp3nS
Zjevení	zjevení	k1gNnSc1
Janovo	Janův	k2eAgNnSc1d1
(	(	kIx(
<g/>
Zjv	Zjv	k1gFnSc1
21	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
křesťanství	křesťanství	k1gNnSc6
se	se	k3xPyFc4
zásvětí	zásvětí	k1gNnSc1
hierarchizuje	hierarchizovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
brzy	brzy	k6eAd1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
představa	představa	k1gFnSc1
třetího	třetí	k4xOgMnSc2
místa	místo	k1gNnPc4
mezi	mezi	k7c7
peklem	peklo	k1gNnSc7
a	a	k8xC
nebem	nebe	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
ohněm	oheň	k1gInSc7
a	a	k8xC
vodou	voda	k1gFnSc7
a	a	k8xC
přímluvnými	přímluvný	k2eAgFnPc7d1
modlitbami	modlitba	k1gFnPc7
očisťuje	očisťovat	k5eAaImIp3nS
od	od	k7c2
hříchů	hřích	k1gInPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniká	vznikat	k5eAaImIp3nS
tak	tak	k6eAd1
očistec	očistec	k1gInSc1
jako	jako	k8xS,k8xC
forma	forma	k1gFnSc1
zásvětní	zásvětní	k2eAgFnSc2d1
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
,	,	kIx,
napravující	napravující	k2eAgFnSc4d1
světskou	světský	k2eAgFnSc4d1
nerovnost	nerovnost	k1gFnSc4
a	a	k8xC
nespravedlnost	nespravedlnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
milovanou	milovaný	k2eAgFnSc4d1
Izanami	Izana	k1gFnPc7
(	(	kIx(
<g/>
Tou	ten	k3xDgFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zve	zvát	k5eAaImIp3nS
<g/>
)	)	kIx)
sestoupil	sestoupit	k5eAaPmAgMnS
Izanagi	Izanagi	k1gNnSc4
(	(	kIx(
<g/>
Ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zve	zvát	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bůh	bůh	k1gMnSc1
ze	z	k7c2
sedmé	sedmý	k4xOgFnSc2
generace	generace	k1gFnSc2
bohů	bůh	k1gMnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
založit	založit	k5eAaPmF
na	na	k7c6
vodách	voda	k1gFnPc6
ostrovy	ostrov	k1gInPc4
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
zde	zde	k6eAd1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
představa	představa	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
již	již	k6eAd1
pojedl	pojíst	k5eAaPmAgMnS
temné	temný	k2eAgFnSc2d1
potravy	potrava	k1gFnSc2
podsvětí	podsvětí	k1gNnSc1
<g/>
,	,	kIx,
nemůže	moct	k5eNaImIp3nS
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
zpět	zpět	k6eAd1
na	na	k7c4
svět	svět	k1gInSc4
<g/>
,	,	kIx,
Izanagimu	Izanagim	k1gInSc2
se	se	k3xPyFc4
proto	proto	k8xC
přivést	přivést	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
milou	milá	k1gFnSc4
zpět	zpět	k6eAd1
na	na	k7c4
svět	svět	k1gInSc4
nepodaří	podařit	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Podsvětní	podsvětní	k2eAgFnSc2d1
říše	říš	k1gFnSc2
Jomi	Jom	k1gFnSc2
no	no	k9
kuni	kuni	k6eAd1
(	(	kIx(
<g/>
„	„	k?
<g/>
Země	zem	k1gFnSc2
žlutých	žlutý	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
japonských	japonský	k2eAgFnPc6d1
představách	představa	k1gFnPc6
plná	plný	k2eAgFnSc1d1
zemřelých	zemřelý	k2eAgMnPc6d1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc2
těla	tělo	k1gNnSc2
žerou	žrát	k5eAaImIp3nP
červi	červ	k1gMnPc1
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
svět	svět	k1gInSc4
plný	plný	k2eAgInSc4d1
lepkavé	lepkavý	k2eAgInPc1d1
tmy	tma	k1gFnPc4
a	a	k8xC
kleteb	kletba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
navíc	navíc	k6eAd1
o	o	k7c6
říši	říš	k1gFnSc6
čarodějnic	čarodějnice	k1gFnPc2
a	a	k8xC
démonů	démon	k1gMnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mayové	Mayové	k2eAgFnSc1d1
</s>
<s>
Xibalbá	Xibalbat	k5eAaImIp3nS,k5eAaPmIp3nS
(	(	kIx(
<g/>
Místo	místo	k7c2
děsu	děs	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
dostanou	dostat	k5eAaPmIp3nP
duše	duše	k1gFnPc1
všech	všecek	k3xTgFnPc2
zemřelých	zemřelá	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nezahynuly	zahynout	k5eNaPmAgFnP
násilnou	násilný	k2eAgFnSc7d1
smrtí	smrt	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nijak	nijak	k6eAd1
se	se	k3xPyFc4
nepojilo	pojit	k5eNaImAgNnS
s	s	k7c7
morálními	morální	k2eAgFnPc7d1
představami	představa	k1gFnPc7
o	o	k7c6
lidském	lidský	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
podsvětí	podsvětí	k1gNnSc6
každý	každý	k3xTgMnSc1
zemřelý	zemřelý	k1gMnSc1
podstupoval	podstupovat	k5eAaImAgMnS
sérii	série	k1gFnSc4
zkoušek	zkouška	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgNnPc6
bylo	být	k5eAaImAgNnS
třeba	třeba	k6eAd1
chytrostí	chytrost	k1gFnSc7
zvítězit	zvítězit	k5eAaPmF
nad	nad	k7c7
nepřejícími	přející	k2eNgMnPc7d1
bohy	bůh	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
nad	nad	k7c7
podsvětím	podsvětí	k1gNnSc7
vládli	vládnout	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
zesnulý	zesnulý	k1gMnSc1
zvítězil	zvítězit	k5eAaPmAgMnS
vystoupil	vystoupit	k5eAaPmAgMnS
z	z	k7c2
podsvětí	podsvětí	k1gNnSc2
na	na	k7c4
oblohu	obloha	k1gFnSc4
jako	jako	k8xS,k8xC
nebeské	nebeský	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posvátná	posvátný	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
Popol	Popola	k1gFnPc2
Vuh	Vuh	k1gFnSc1
popisuje	popisovat	k5eAaImIp3nS
příběh	příběh	k1gInSc4
hrdinských	hrdinský	k2eAgNnPc2d1
dvojčat	dvojče	k1gNnPc2
Hunahpua	Hunahpuum	k1gNnSc2
a	a	k8xC
Xbalanqueho	Xbalanque	k1gMnSc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
zvítězili	zvítězit	k5eAaPmAgMnP
nad	nad	k7c7
vládci	vládce	k1gMnPc7
Xibalby	Xibalba	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
za	za	k7c4
odměnu	odměna	k1gFnSc4
se	se	k3xPyFc4
převtělili	převtělit	k5eAaPmAgMnP
ve	v	k7c4
Slunce	slunce	k1gNnSc4
a	a	k8xC
Měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VERGILIUS	Vergilius	k1gMnSc1
<g/>
,	,	kIx,
Publius	Publius	k1gMnSc1
<g/>
,	,	kIx,
Maro	Maro	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aeneis	Aeneis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
KOLEKTIV	kolektivum	k1gNnPc2
AUTORŮ	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mytologie	mytologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Perfekt	perfektum	k1gNnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WINKELHÖFEROVÁ	WINKELHÖFEROVÁ	kA
<g/>
,	,	kIx,
Vlasta	Vlasta	k1gFnSc1
<g/>
;	;	kIx,
LÖWENSTEINOVÁ	LÖWENSTEINOVÁ	kA
<g/>
,	,	kIx,
Miriam	Miriam	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
mytologie	mytologie	k1gFnSc2
Japonska	Japonsko	k1gNnSc2
a	a	k8xC
Koreje	Korea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788072772650	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
76	#num#	k4
<g/>
-	-	kIx~
<g/>
77	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WINKELHÖFEROVÁ	WINKELHÖFEROVÁ	kA
<g/>
,	,	kIx,
Vlasta	Vlasta	k1gFnSc1
<g/>
;	;	kIx,
LÖWENSTEINOVÁ	LÖWENSTEINOVÁ	kA
<g/>
,	,	kIx,
Miriam	Miriam	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
mytologie	mytologie	k1gFnSc2
Japonska	Japonsko	k1gNnSc2
a	a	k8xC
Koreje	Korea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788072772650	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
82	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Mytologie	mytologie	k1gFnSc1
světa	svět	k1gInSc2
:	:	kIx,
ilustrovaný	ilustrovaný	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788072090341	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
249	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
podsvětí	podsvětí	k1gNnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4062048-7	4062048-7	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Posmrtný	posmrtný	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Ráj	ráj	k1gInSc1
•	•	k?
Nebe	nebe	k1gNnSc2
•	•	k?
Očistec	očistec	k1gInSc1
•	•	k?
Předpeklí	předpeklí	k1gNnSc2
•	•	k?
Podsvětí	podsvětí	k1gNnSc2
•	•	k?
Peklo	peklo	k1gNnSc1
•	•	k?
Šeol	Šeol	k1gMnSc1
•	•	k?
Gehenna	gehenna	k1gFnSc1
</s>
<s>
Hádés	Hádés	k1gInSc1
•	•	k?
Tartaros	Tartaros	k1gInSc1
•	•	k?
Duat	Duat	k1gInSc1
•	•	k?
Naraka	Narak	k1gMnSc2
(	(	kIx(
<g/>
hinduismus	hinduismus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Naraka	Narak	k1gMnSc2
(	(	kIx(
<g/>
buddhismus	buddhismus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Valhalla	Valhalla	k1gMnSc1
•	•	k?
Helheim	Helheim	k1gMnSc1
•	•	k?
Nav	navit	k5eAaImRp2nS
</s>
