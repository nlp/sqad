<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
vstupu	vstup	k1gInSc2
do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
projde	projít	k5eAaPmIp3nS
duše	duše	k1gFnSc1
kolem	kolem	k7c2
trojhlavého	trojhlavý	k2eAgMnSc2d1
psa	pes	k1gMnSc2
Kerbera	Kerber	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
do	do	k7c2
říše	říš	k1gFnSc2
mrtvých	mrtvý	k1gMnPc2
nepustí	pustit	k5eNaPmIp3nP
nikoho	nikdo	k3yNnSc4
živého	živý	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duše	duše	k1gFnSc1
poté	poté	k6eAd1
potká	potkat	k5eAaPmIp3nS
převozníka	převozník	k1gMnSc4
Charóna	Charón	k1gMnSc4
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
musí	muset	k5eAaImIp3nS
zaplatit	zaplatit	k5eAaPmF
dvěma	dva	k4xCgFnPc7
mincemi	mince	k1gFnPc7
<g/>
.	.	kIx.
</s>