<s>
Kdo	kdo	k3yQnSc1	kdo
byl	být	k5eAaImAgMnS	být
řecký	řecký	k2eAgMnSc1d1	řecký
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
sepsání	sepsání	k1gNnSc2	sepsání
epických	epický	k2eAgFnPc2d1	epická
básní	báseň	k1gFnPc2	báseň
Ilias	Ilias	k1gFnSc1	Ilias
a	a	k8xC	a
Odysseia	Odysseia	k1gFnSc1	Odysseia
<g/>
?	?	kIx.	?
</s>
