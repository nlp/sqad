<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
atentátu	atentát	k1gInSc6	atentát
československých	československý	k2eAgMnPc2d1	československý
parašutistů	parašutista	k1gMnPc2	parašutista
na	na	k7c4	na
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
Lidice	Lidice	k1gInPc1	Lidice
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
vypálena	vypálit	k5eAaPmNgFnS	vypálit
a	a	k8xC	a
dokonale	dokonale	k6eAd1	dokonale
srovnána	srovnat	k5eAaPmNgFnS	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
veškeré	veškerý	k3xTgNnSc4	veškerý
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
popraveno	popravit	k5eAaPmNgNnS	popravit
nebo	nebo	k8xC	nebo
odsunuto	odsunut	k2eAgNnSc1d1	odsunuto
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
