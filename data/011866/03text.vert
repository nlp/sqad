<p>
<s>
Vyhlazení	vyhlazení	k1gNnSc1	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
byla	být	k5eAaImAgFnS	být
násilná	násilný	k2eAgFnSc1d1	násilná
historická	historický	k2eAgFnSc1d1	historická
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
pří	přít	k5eAaImIp3nS	přít
níž	nízce	k6eAd2	nízce
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
středočeské	středočeský	k2eAgFnSc2d1	Středočeská
vsi	ves	k1gFnSc2	ves
Lidice	Lidice	k1gInPc1	Lidice
a	a	k8xC	a
vyvraždĕ	vyvraždĕ	k?	vyvraždĕ
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
německými	německý	k2eAgMnPc7d1	německý
nacistickými	nacistický	k2eAgMnPc7d1	nacistický
okupanty	okupant	k1gMnPc7	okupant
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
atentátu	atentát	k1gInSc6	atentát
československých	československý	k2eAgMnPc2d1	československý
parašutistů	parašutista	k1gMnPc2	parašutista
na	na	k7c4	na
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
Lidice	Lidice	k1gInPc1	Lidice
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
vypálena	vypálit	k5eAaPmNgFnS	vypálit
a	a	k8xC	a
dokonale	dokonale	k6eAd1	dokonale
srovnána	srovnat	k5eAaPmNgFnS	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
veškeré	veškerý	k3xTgNnSc4	veškerý
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
popraveno	popravit	k5eAaPmNgNnS	popravit
nebo	nebo	k8xC	nebo
odsunuto	odsunut	k2eAgNnSc1d1	odsunuto
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počtu	počet	k1gInSc2	počet
340	[number]	k4	340
(	(	kIx(	(
<g/>
192	[number]	k4	192
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
60	[number]	k4	60
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
88	[number]	k4	88
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Událost	událost	k1gFnSc1	událost
dnes	dnes	k6eAd1	dnes
připomíná	připomínat	k5eAaImIp3nS	připomínat
Památník	památník	k1gInSc1	památník
Lidice	Lidice	k1gInPc4	Lidice
s	s	k7c7	s
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předehra	předehra	k1gFnSc1	předehra
tragédie	tragédie	k1gFnSc2	tragédie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
zastupujícího	zastupující	k2eAgMnSc4d1	zastupující
říšského	říšský	k2eAgMnSc4d1	říšský
protektora	protektor	k1gMnSc4	protektor
SS-Obergruppenführera	SS-Obergruppenführer	k1gMnSc2	SS-Obergruppenführer
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
vystupňoval	vystupňovat	k5eAaPmAgInS	vystupňovat
teror	teror	k1gInSc1	teror
nacistických	nacistický	k2eAgMnPc2d1	nacistický
okupantů	okupant	k1gMnPc2	okupant
proti	proti	k7c3	proti
českému	český	k2eAgInSc3d1	český
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
popravy	poprava	k1gFnPc4	poprava
stovek	stovka	k1gFnPc2	stovka
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
razie	razie	k1gFnSc2	razie
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
pachatele	pachatel	k1gMnPc4	pachatel
atentátu	atentát	k1gInSc2	atentát
dopadnout	dopadnout	k5eAaPmF	dopadnout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Heydrich	Heydricha	k1gFnPc2	Heydricha
na	na	k7c4	na
následky	následek	k1gInPc4	následek
zranění	zranění	k1gNnSc2	zranění
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
nacisté	nacista	k1gMnPc1	nacista
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
k	k	k7c3	k
dosud	dosud	k6eAd1	dosud
neslýchané	slýchaný	k2eNgFnSc3d1	neslýchaná
zastrašovací	zastrašovací	k2eAgFnSc3d1	zastrašovací
akci	akce	k1gFnSc3	akce
<g/>
;	;	kIx,	;
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
plánu	plán	k1gInSc6	plán
vyhlazení	vyhlazení	k1gNnSc2	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
měl	mít	k5eAaImAgMnS	mít
ambiciózní	ambiciózní	k2eAgMnSc1d1	ambiciózní
státní	státní	k2eAgMnSc1d1	státní
tajemník	tajemník	k1gMnSc1	tajemník
úřadu	úřad	k1gInSc2	úřad
říšského	říšský	k2eAgMnSc2d1	říšský
protektora	protektor	k1gMnSc2	protektor
SS-Obergruppenführer	SS-Obergruppenführer	k1gMnSc1	SS-Obergruppenführer
Karl	Karl	k1gMnSc1	Karl
Hermann	Hermann	k1gMnSc1	Hermann
Frank	Frank	k1gMnSc1	Frank
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nicotnou	nicotný	k2eAgFnSc7d1	nicotná
souhrou	souhra	k1gFnSc7	souhra
okolností	okolnost	k1gFnSc7	okolnost
byly	být	k5eAaImAgFnP	být
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
vybrány	vybrat	k5eAaPmNgInP	vybrat
právě	právě	k9	právě
Lidice	Lidice	k1gInPc1	Lidice
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
Jaroslavu	Jaroslav	k1gMnSc3	Jaroslav
Pálovi	Pála	k1gMnSc3	Pála
<g/>
,	,	kIx,	,
majiteli	majitel	k1gMnPc7	majitel
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
baterie	baterie	k1gFnPc4	baterie
ve	v	k7c6	v
Slaném	Slaný	k1gInSc6	Slaný
<g/>
,	,	kIx,	,
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
podezřelý	podezřelý	k2eAgInSc1d1	podezřelý
milostný	milostný	k2eAgInSc1d1	milostný
dopis	dopis	k1gInSc1	dopis
adresovaný	adresovaný	k2eAgInSc1d1	adresovaný
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
zaměstnankyň	zaměstnankyně	k1gFnPc2	zaměstnankyně
továrny	továrna	k1gFnSc2	továrna
<g/>
,	,	kIx,	,
jisté	jistý	k2eAgFnSc3d1	jistá
Anně	Anna	k1gFnSc3	Anna
Marusczákové	Marusczáková	k1gFnSc2	Marusczáková
z	z	k7c2	z
Holous	Holous	k1gInSc1	Holous
u	u	k7c2	u
Brandýska	Brandýsk	k1gInSc2	Brandýsk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
stálo	stát	k5eAaImAgNnS	stát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Drahá	drahý	k2eAgFnSc5d1	drahá
Aničko	Anička	k1gFnSc5	Anička
<g/>
!	!	kIx.	!
</s>
<s>
Promiň	prominout	k5eAaPmRp2nS	prominout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ty	k3xPp2nSc3	ty
píši	psát	k5eAaImIp1nS	psát
tak	tak	k6eAd1	tak
pozdě	pozdě	k6eAd1	pozdě
<g/>
...	...	k?	...
Co	co	k3yRnSc1	co
jsem	být	k5eAaImIp1nS	být
chtěl	chtít	k5eAaImAgMnS	chtít
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jsem	být	k5eAaImIp1nS	být
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Onoho	onen	k3xDgInSc2	onen
osudného	osudný	k2eAgInSc2d1	osudný
dne	den	k1gInSc2	den
jsem	být	k5eAaImIp1nS	být
spal	spát	k5eAaImAgMnS	spát
někde	někde	k6eAd1	někde
na	na	k7c6	na
Čabárně	Čabárna	k1gFnSc6	Čabárna
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
zdráv	zdráv	k2eAgInSc4d1	zdráv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
shledanou	shledaná	k1gFnSc4	shledaná
tento	tento	k3xDgInSc4	tento
týden	týden	k1gInSc4	týden
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
už	už	k6eAd1	už
neuvidíme	uvidět	k5eNaPmIp1nP	uvidět
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pála	Pála	k1gMnSc1	Pála
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
pisatel	pisatel	k1gMnSc1	pisatel
dopisu	dopis	k1gInSc2	dopis
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zapleten	zaplést	k5eAaPmNgInS	zaplést
do	do	k7c2	do
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
obavách	obava	k1gFnPc6	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
provokaci	provokace	k1gFnSc4	provokace
<g/>
,	,	kIx,	,
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
četnictvo	četnictvo	k1gNnSc4	četnictvo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dopis	dopis	k1gInSc4	dopis
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
gestapu	gestapo	k1gNnSc3	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
Marusczáková	Marusczáková	k1gFnSc1	Marusczáková
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
skutečné	skutečný	k2eAgNnSc4d1	skutečné
jméno	jméno	k1gNnSc4	jméno
svého	svůj	k3xOyFgMnSc2	svůj
milence	milenec	k1gMnSc2	milenec
neznala	znát	k5eNaImAgFnS	znát
<g/>
,	,	kIx,	,
při	při	k7c6	při
výslechu	výslech	k1gInSc6	výslech
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zmínila	zmínit	k5eAaPmAgFnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
ji	on	k3xPp3gFnSc4	on
poprosil	poprosit	k5eAaPmAgMnS	poprosit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
Lidicích	Lidice	k1gInPc6	Lidice
rodině	rodina	k1gFnSc3	rodina
Horákových	Horáková	k1gFnPc2	Horáková
a	a	k8xC	a
Stříbrných	stříbrný	k1gInPc2	stříbrný
vyřídila	vyřídit	k5eAaPmAgFnS	vyřídit
pozdrav	pozdrav	k1gInSc4	pozdrav
od	od	k7c2	od
jejich	jejich	k3xOp3gMnPc2	jejich
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sloužili	sloužit	k5eAaImAgMnP	sloužit
u	u	k7c2	u
311	[number]	k4	311
<g/>
.	.	kIx.	.
bombardovací	bombardovací	k2eAgFnSc2d1	bombardovací
perutě	peruť	k1gFnSc2	peruť
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
razie	razie	k1gFnSc1	razie
v	v	k7c6	v
Lidicích	Lidice	k1gInPc6	Lidice
a	a	k8xC	a
zatčení	zatčení	k1gNnSc1	zatčení
rodin	rodina	k1gFnPc2	rodina
Horáků	Horák	k1gMnPc2	Horák
a	a	k8xC	a
Stříbrných	stříbrná	k1gFnPc2	stříbrná
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
synech	syn	k1gMnPc6	syn
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
československém	československý	k2eAgNnSc6d1	Československé
vojsku	vojsko	k1gNnSc6	vojsko
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Domovní	domovní	k2eAgFnPc4d1	domovní
prohlídky	prohlídka	k1gFnPc4	prohlídka
ani	ani	k8xC	ani
výslechy	výslech	k1gInPc4	výslech
však	však	k9	však
neodhalily	odhalit	k5eNaPmAgFnP	odhalit
nic	nic	k3yNnSc1	nic
podezřelého	podezřelý	k2eAgNnSc2d1	podezřelé
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
přítel	přítel	k1gMnSc1	přítel
Marusczákové	Marusczáková	k1gFnSc2	Marusczáková
identifikován	identifikován	k2eAgMnSc1d1	identifikován
Václav	Václav	k1gMnSc1	Václav
Říha	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
dělník	dělník	k1gMnSc1	dělník
z	z	k7c2	z
Vrapic	Vrapice	k1gFnPc2	Vrapice
u	u	k7c2	u
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
chtěl	chtít	k5eAaImAgMnS	chtít
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Marusczákovou	Marusczáková	k1gFnSc7	Marusczáková
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
obával	obávat	k5eAaImAgMnS	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
nevěra	nevěra	k1gFnSc1	nevěra
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
z	z	k7c2	z
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
vsi	ves	k1gFnSc2	ves
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
;	;	kIx,	;
poslal	poslat	k5eAaPmAgMnS	poslat
jí	jíst	k5eAaImIp3nS	jíst
proto	proto	k8xC	proto
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
onen	onen	k3xDgInSc1	onen
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
vyvolat	vyvolat	k5eAaPmF	vyvolat
romantický	romantický	k2eAgInSc1d1	romantický
dojem	dojem	k1gInSc1	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zapojen	zapojit	k5eAaPmNgMnS	zapojit
do	do	k7c2	do
odbojové	odbojový	k2eAgFnSc2d1	odbojová
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
odešel	odejít	k5eAaPmAgMnS	odejít
skrýt	skrýt	k5eAaPmF	skrýt
do	do	k7c2	do
křivoklátských	křivoklátský	k2eAgInPc2d1	křivoklátský
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Kladenskému	kladenský	k2eAgNnSc3d1	kladenské
gestapu	gestapo	k1gNnSc3	gestapo
bylo	být	k5eAaImAgNnS	být
záhy	záhy	k6eAd1	záhy
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Říha	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
Marusczáková	Marusczáková	k1gFnSc1	Marusczáková
ani	ani	k8xC	ani
Horákovi	Horákův	k2eAgMnPc1d1	Horákův
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
atentátem	atentát	k1gInSc7	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
nic	nic	k6eAd1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
;	;	kIx,	;
v	v	k7c6	v
Lidicích	Lidice	k1gInPc6	Lidice
nebyl	být	k5eNaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
žádný	žádný	k3yNgInSc1	žádný
arzenál	arzenál	k1gInSc1	arzenál
<g/>
,	,	kIx,	,
vysílačky	vysílačka	k1gFnSc2	vysílačka
ani	ani	k8xC	ani
skrývané	skrývaný	k2eAgFnSc2d1	skrývaná
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
podáno	podat	k5eAaPmNgNnS	podat
hlášení	hlášení	k1gNnSc4	hlášení
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
Karl	Karl	k1gMnSc1	Karl
Hermann	Hermann	k1gMnSc1	Hermann
Frank	Frank	k1gMnSc1	Frank
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
posílí	posílit	k5eAaPmIp3nS	posílit
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
napomůže	napomoct	k5eAaPmIp3nS	napomoct
svému	svůj	k3xOyFgMnSc3	svůj
jmenování	jmenování	k1gNnSc1	jmenování
příštím	příští	k2eAgMnSc7d1	příští
říšským	říšský	k2eAgMnSc7d1	říšský
protektorem	protektor	k1gMnSc7	protektor
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
využít	využít	k5eAaPmF	využít
Lidic	Lidice	k1gInPc2	Lidice
jako	jako	k9	jako
ukázky	ukázka	k1gFnPc4	ukázka
svého	svůj	k3xOyFgInSc2	svůj
"	"	kIx"	"
<g/>
rázného	rázný	k2eAgInSc2d1	rázný
postupu	postup	k1gInSc2	postup
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
osobně	osobně	k6eAd1	osobně
předložil	předložit	k5eAaPmAgMnS	předložit
při	při	k7c6	při
Heydrichově	Heydrichův	k2eAgInSc6d1	Heydrichův
pohřbu	pohřeb	k1gInSc6	pohřeb
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
Hitlerovi	Hitler	k1gMnSc3	Hitler
a	a	k8xC	a
obratem	obratem	k6eAd1	obratem
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
obdržel	obdržet	k5eAaPmAgMnS	obdržet
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Lidice	Lidice	k1gInPc1	Lidice
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
vypáleny	vypálen	k2eAgInPc1d1	vypálen
a	a	k8xC	a
srovnány	srovnán	k2eAgInPc1d1	srovnán
se	se	k3xPyFc4	se
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
dospělí	dospělý	k2eAgMnPc1d1	dospělý
muži	muž	k1gMnPc1	muž
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
uvězněny	uvězněn	k2eAgFnPc1d1	uvězněna
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
dány	dán	k2eAgFnPc1d1	dána
na	na	k7c4	na
"	"	kIx"	"
<g/>
náležité	náležitý	k2eAgNnSc4d1	náležité
vychování	vychování	k1gNnSc4	vychování
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyhlazení	vyhlazení	k1gNnPc2	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
==	==	k?	==
</s>
</p>
<p>
<s>
Navečer	navečer	k1gInSc1	navečer
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
byly	být	k5eAaImAgInP	být
Lidice	Lidice	k1gInPc1	Lidice
obklíčeny	obklíčen	k2eAgInPc1d1	obklíčen
jednotkami	jednotka	k1gFnPc7	jednotka
SS	SS	kA	SS
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
nesměl	smět	k5eNaImAgMnS	smět
obec	obec	k1gFnSc4	obec
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Organizační	organizační	k2eAgInSc1d1	organizační
štáb	štáb	k1gInSc1	štáb
celé	celý	k2eAgFnSc2d1	celá
akce	akce	k1gFnSc2	akce
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
všemu	všecek	k3xTgNnSc3	všecek
velel	velet	k5eAaImAgInS	velet
<g/>
.	.	kIx.	.
</s>
<s>
Štáb	štáb	k1gInSc4	štáb
tvořili	tvořit	k5eAaImAgMnP	tvořit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
SS-Hauptsturmführer	SS-Hauptsturmführer	k1gMnSc1	SS-Hauptsturmführer
Walter	Walter	k1gMnSc1	Walter
Jacobi	Jacob	k1gFnSc2	Jacob
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
vedoucí	vedoucí	k1gMnSc1	vedoucí
úřadovny	úřadovna	k1gFnSc2	úřadovna
SD	SD	kA	SD
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SS-Hauptsturmführer	SS-Hauptsturmführer	k1gMnSc1	SS-Hauptsturmführer
Harald	Harald	k1gMnSc1	Harald
Wiesmann	Wiesmann	k1gMnSc1	Wiesmann
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
kladenské	kladenský	k2eAgFnSc2d1	kladenská
služebny	služebna	k1gFnSc2	služebna
gestapa	gestapo	k1gNnSc2	gestapo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SS-Standartenführer	SS-Standartenführer	k1gMnSc1	SS-Standartenführer
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hans-Ulrich	Hans-Ulrich	k1gMnSc1	Hans-Ulrich
Geschke	Geschk	k1gMnSc2	Geschk
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
pražského	pražský	k2eAgNnSc2d1	Pražské
gestapa	gestapo	k1gNnSc2	gestapo
Staatspolizeileitstelle	Staatspolizeileitstelle	k1gFnSc2	Staatspolizeileitstelle
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SS-Obersturmführer	SS-Obersturmführer	k1gMnSc1	SS-Obersturmführer
Max	Max	k1gMnSc1	Max
Rostock	Rostock	k1gMnSc1	Rostock
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
SD	SD	kA	SD
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SS-Untersturmführer	SS-Untersturmführer	k1gMnSc1	SS-Untersturmführer
Thomas	Thomas	k1gMnSc1	Thomas
Thomsen	Thomsen	k2eAgMnSc1d1	Thomsen
(	(	kIx(	(
<g/>
zástupce	zástupce	k1gMnSc4	zástupce
Wiesmanna	Wiesmann	k1gMnSc4	Wiesmann
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Major	major	k1gMnSc1	major
der	drát	k5eAaImRp2nS	drát
Schutzpolizei	Schutzpolize	k1gMnPc1	Schutzpolize
August	August	k1gMnSc1	August
Marwedel	Marwedlo	k1gNnPc2	Marwedlo
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
kladenské	kladenský	k2eAgNnSc4d1	kladenské
Schupo	schupo	k1gNnSc4	schupo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SS-Obersturmbannführer	SS-Obersturmbannführer	k1gMnSc1	SS-Obersturmbannführer
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Leimer	Leimer	k1gMnSc1	Leimer
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
protiparašutistického	protiparašutistický	k2eAgNnSc2d1	protiparašutistický
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
<g/>
Starosta	Starosta	k1gMnSc1	Starosta
byl	být	k5eAaImAgMnS	být
přinucen	přinutit	k5eAaPmNgMnS	přinutit
vydat	vydat	k5eAaPmF	vydat
veškeré	veškerý	k3xTgFnPc1	veškerý
obecní	obecní	k2eAgFnPc1d1	obecní
cennosti	cennost	k1gFnPc1	cennost
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
vyváděni	vyváděn	k2eAgMnPc1d1	vyváděn
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
domovů	domov	k1gInPc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
starší	starý	k2eAgMnPc1d2	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
byli	být	k5eAaImAgMnP	být
shromažďováni	shromažďován	k2eAgMnPc1d1	shromažďován
ve	v	k7c6	v
sklepě	sklep	k1gInSc6	sklep
a	a	k8xC	a
chlévě	chlév	k1gInSc6	chlév
Horákova	Horákův	k2eAgInSc2d1	Horákův
statku	statek	k1gInSc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
cennější	cenný	k2eAgFnPc4d2	cennější
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
koně	kůň	k1gMnPc4	kůň
<g/>
,	,	kIx,	,
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
stroje	stroj	k1gInPc4	stroj
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
shromažďovány	shromažďován	k2eAgInPc1d1	shromažďován
a	a	k8xC	a
odváženy	odvážen	k2eAgInPc1d1	odvážen
do	do	k7c2	do
sousedního	sousední	k2eAgInSc2d1	sousední
Buštěhradu	Buštěhrad	k1gInSc2	Buštěhrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ráno	ráno	k6eAd1	ráno
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
se	se	k3xPyFc4	se
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
osobně	osobně	k6eAd1	osobně
dostavil	dostavit	k5eAaPmAgMnS	dostavit
K.	K.	kA	K.
H.	H.	kA	H.
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
likvidaci	likvidace	k1gFnSc4	likvidace
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
byly	být	k5eAaImAgFnP	být
nejprve	nejprve	k6eAd1	nejprve
nahnány	nahnat	k5eAaPmNgFnP	nahnat
do	do	k7c2	do
místní	místní	k2eAgFnSc2d1	místní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
za	za	k7c2	za
úsvitu	úsvit	k1gInSc2	úsvit
pak	pak	k6eAd1	pak
autobusy	autobus	k1gInPc1	autobus
převezeny	převezen	k2eAgInPc1d1	převezen
do	do	k7c2	do
tělocvičny	tělocvična	k1gFnSc2	tělocvična
kladenského	kladenský	k2eAgNnSc2d1	kladenské
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byly	být	k5eAaImAgFnP	být
zdi	zeď	k1gFnPc1	zeď
stodoly	stodola	k1gFnSc2	stodola
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
Horákova	Horákův	k2eAgInSc2d1	Horákův
statku	statek	k1gInSc2	statek
obloženy	obložen	k2eAgInPc1d1	obložen
slamníky	slamník	k1gInPc1	slamník
a	a	k8xC	a
matracemi	matrace	k1gFnPc7	matrace
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
odraženým	odražený	k2eAgFnPc3d1	odražená
střelám	střela	k1gFnPc3	střela
<g/>
)	)	kIx)	)
a	a	k8xC	a
přichystána	přichystán	k2eAgFnSc1d1	přichystána
popravčí	popravčí	k2eAgFnSc1d1	popravčí
četa	četa	k1gFnSc1	četa
<g/>
.	.	kIx.	.
</s>
<s>
Lidičtí	lidický	k2eAgMnPc1d1	lidický
muži	muž	k1gMnPc1	muž
byli	být	k5eAaImAgMnP	být
posléze	posléze	k6eAd1	posléze
vyváděni	vyvádět	k5eAaImNgMnP	vyvádět
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
(	(	kIx(	(
<g/>
zprvu	zprvu	k6eAd1	zprvu
po	po	k7c6	po
pěti	pět	k4xCc6	pět
<g/>
,	,	kIx,	,
pak	pak	k8xC	pak
po	po	k7c6	po
deseti	deset	k4xCc6	deset
<g/>
)	)	kIx)	)
na	na	k7c4	na
přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
zahradu	zahrada	k1gFnSc4	zahrada
a	a	k8xC	a
tam	tam	k6eAd1	tam
stříleni	střílen	k2eAgMnPc1d1	střílen
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
jich	on	k3xPp3gMnPc2	on
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
bylo	být	k5eAaImAgNnS	být
povražděno	povraždit	k5eAaPmNgNnS	povraždit
173	[number]	k4	173
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
lidického	lidický	k2eAgMnSc2d1	lidický
faráře	farář	k1gMnSc2	farář
Josefa	Josef	k1gMnSc2	Josef
Štemberky	Štemberka	k1gFnSc2	Štemberka
<g/>
.	.	kIx.	.
</s>
<s>
Nejstaršímu	starý	k2eAgMnSc3d3	nejstarší
bylo	být	k5eAaImAgNnS	být
84	[number]	k4	84
<g/>
,	,	kIx,	,
nejmladšímu	mladý	k2eAgMnSc3d3	nejmladší
Josefu	Josef	k1gMnSc3	Josef
Hroníkovi	Hroníkův	k2eAgMnPc1d1	Hroníkův
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
domy	dům	k1gInPc1	dům
včetně	včetně	k7c2	včetně
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
fary	fara	k1gFnSc2	fara
byly	být	k5eAaImAgInP	být
polity	polít	k5eAaPmNgInP	polít
benzínem	benzín	k1gInSc7	benzín
a	a	k8xC	a
podpáleny	podpálen	k2eAgFnPc1d1	podpálena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
týdnech	týden	k1gInPc6	týden
a	a	k8xC	a
měsících	měsíc	k1gInPc6	měsíc
byly	být	k5eAaImAgInP	být
zbytky	zbytek	k1gInPc1	zbytek
vypálených	vypálený	k2eAgFnPc2d1	vypálená
budov	budova	k1gFnPc2	budova
vyhozeny	vyhodit	k5eAaPmNgInP	vyhodit
do	do	k7c2	do
povětří	povětří	k1gNnSc2	povětří
<g/>
,	,	kIx,	,
zničen	zničen	k2eAgInSc1d1	zničen
hřbitov	hřbitov	k1gInSc1	hřbitov
včetně	včetně	k7c2	včetně
exhumace	exhumace	k1gFnSc2	exhumace
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
,	,	kIx,	,
vykáceny	vykácen	k2eAgInPc1d1	vykácen
stromy	strom	k1gInPc1	strom
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jediné	jediný	k2eAgFnPc4d1	jediná
hrušně	hrušeň	k1gFnPc4	hrušeň
nedaleko	nedaleko	k7c2	nedaleko
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
Němci	Němec	k1gMnSc3	Němec
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
uschlou	uschlý	k2eAgFnSc4d1	uschlá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sutí	sutit	k5eAaImIp3nS	sutit
zavezen	zavezen	k2eAgInSc1d1	zavezen
Podhorův	Podhorův	k2eAgInSc1d1	Podhorův
rybník	rybník	k1gInSc1	rybník
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
srovnáno	srovnán	k2eAgNnSc1d1	srovnáno
k	k	k7c3	k
nepoznání	nepoznání	k1gNnSc3	nepoznání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odvozu	odvoz	k1gInSc3	odvoz
suti	suť	k1gFnSc2	suť
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zbudována	zbudován	k2eAgFnSc1d1	zbudována
polní	polní	k2eAgFnSc1d1	polní
železnice	železnice	k1gFnSc1	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
posunuto	posunut	k2eAgNnSc1d1	posunuto
koryto	koryto	k1gNnSc1	koryto
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nic	nic	k3yNnSc1	nic
nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgInPc1d1	filmový
záznamy	záznam	k1gInPc1	záznam
<g/>
,	,	kIx,	,
pořízené	pořízený	k2eAgNnSc1d1	pořízené
samotnými	samotný	k2eAgMnPc7d1	samotný
nacisty	nacista	k1gMnPc7	nacista
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
vydávají	vydávat	k5eAaPmIp3nP	vydávat
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
díle	dílo	k1gNnSc6	dílo
zkázy	zkáza	k1gFnSc2	zkáza
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stávaly	stávat	k5eAaImAgInP	stávat
Lidice	Lidice	k1gInPc1	Lidice
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
učiněn	učiněn	k2eAgInSc1d1	učiněn
holým	holý	k2eAgNnSc7d1	holé
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
obce	obec	k1gFnSc2	obec
vymazáno	vymazat	k5eAaPmNgNnS	vymazat
z	z	k7c2	z
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
číslo	číslo	k1gNnSc1	číslo
25	[number]	k4	25
<g/>
/	/	kIx~	/
<g/>
1943	[number]	k4	1943
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
změnách	změna	k1gFnPc6	změna
úředních	úřední	k2eAgInPc2d1	úřední
názvů	název	k1gInPc2	název
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
povolených	povolený	k2eAgFnPc2d1	povolená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
název	název	k1gInSc1	název
bývalé	bývalý	k2eAgFnSc2d1	bývalá
obce	obec	k1gFnSc2	obec
Lidic	Lidice	k1gInPc2	Lidice
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
okresu	okres	k1gInSc6	okres
kladenském	kladenský	k2eAgInSc6d1	kladenský
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
navždy	navždy	k6eAd1	navždy
zaniklý	zaniklý	k2eAgInSc4d1	zaniklý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osud	osud	k1gInSc1	osud
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
střelnici	střelnice	k1gFnSc4	střelnice
v	v	k7c6	v
Praze-Kobylisích	Praze-Kobylise	k1gFnPc6	Praze-Kobylise
popraveno	popraven	k2eAgNnSc1d1	popraveno
dalších	další	k2eAgNnPc6d1	další
26	[number]	k4	26
lidických	lidický	k2eAgMnPc2d1	lidický
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
sedm	sedm	k4xCc4	sedm
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
osm	osm	k4xCc1	osm
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
rodin	rodina	k1gFnPc2	rodina
Horáků	Horák	k1gMnPc2	Horák
a	a	k8xC	a
Stříbrných	stříbrný	k2eAgMnPc2d1	stříbrný
<g/>
,	,	kIx,	,
uvězněných	uvězněný	k2eAgMnPc2d1	uvězněný
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
devět	devět	k4xCc1	devět
dospělých	dospělý	k2eAgMnPc2d1	dospělý
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
osudné	osudný	k2eAgFnPc4d1	osudná
noci	noc	k1gFnPc4	noc
mimo	mimo	k7c4	mimo
Lidice	Lidice	k1gInPc4	Lidice
(	(	kIx(	(
<g/>
sedm	sedm	k4xCc4	sedm
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
skrýval	skrývat	k5eAaImAgInS	skrývat
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
evidenci	evidence	k1gFnSc6	evidence
dětí	dítě	k1gFnPc2	dítě
dodatečně	dodatečně	k6eAd1	dodatečně
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
překročili	překročit	k5eAaPmAgMnP	překročit
15	[number]	k4	15
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Kladně	Kladno	k1gNnSc6	Kladno
byly	být	k5eAaImAgFnP	být
lidické	lidický	k2eAgFnPc1d1	Lidická
děti	dítě	k1gFnPc1	dítě
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
zkoumány	zkoumán	k2eAgInPc4d1	zkoumán
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
"	"	kIx"	"
<g/>
rasového	rasový	k2eAgNnSc2d1	rasové
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
"	"	kIx"	"
vhodné	vhodný	k2eAgNnSc1d1	vhodné
k	k	k7c3	k
"	"	kIx"	"
<g/>
převýchově	převýchova	k1gFnSc3	převýchova
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
byly	být	k5eAaImAgFnP	být
vybrány	vybrat	k5eAaPmNgFnP	vybrat
hned	hned	k6eAd1	hned
na	na	k7c6	na
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
sedm	sedm	k4xCc1	sedm
dětí	dítě	k1gFnPc2	dítě
mladších	mladý	k2eAgMnPc2d2	mladší
jednoho	jeden	k4xCgMnSc4	jeden
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
předáno	předat	k5eAaPmNgNnS	předat
německému	německý	k2eAgInSc3d1	německý
sirotčinci	sirotčinec	k1gInSc3	sirotčinec
v	v	k7c6	v
Praze-Krči	Praze-Krč	k1gFnSc6	Praze-Krč
<g/>
,	,	kIx,	,
ostatních	ostatní	k2eAgMnPc2d1	ostatní
88	[number]	k4	88
bylo	být	k5eAaImAgNnS	být
převezeno	převézt	k5eAaPmNgNnS	převézt
do	do	k7c2	do
Lodže	Lodž	k1gFnSc2	Lodž
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Łódź	Łódź	k1gFnSc1	Łódź
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Lodsch	Lodsch	k1gInSc1	Lodsch
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1940	[number]	k4	1940
Němci	Němec	k1gMnPc1	Němec
město	město	k1gNnSc4	město
přejmenovali	přejmenovat	k5eAaPmAgMnP	přejmenovat
na	na	k7c4	na
Litzmannstadt	Litzmannstadt	k1gInSc4	Litzmannstadt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
poněmčení	poněmčení	k1gNnSc3	poněmčení
určeno	určit	k5eAaPmNgNnS	určit
dalších	další	k2eAgNnPc2d1	další
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Zbývajících	zbývající	k2eAgFnPc2d1	zbývající
81	[number]	k4	81
dětí	dítě	k1gFnPc2	dítě
bylo	být	k5eAaImAgNnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1942	[number]	k4	1942
převezeno	převézt	k5eAaPmNgNnS	převézt
do	do	k7c2	do
nedalekého	daleký	k2eNgInSc2d1	nedaleký
vyhlazovacího	vyhlazovací	k2eAgInSc2d1	vyhlazovací
tábora	tábor	k1gInSc2	tábor
Chełmno	Chełmno	k6eAd1	Chełmno
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Kulmhof	Kulmhof	k1gInSc1	Kulmhof
<g/>
)	)	kIx)	)
a	a	k8xC	a
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
usmrceno	usmrtit	k5eAaPmNgNnS	usmrtit
výfukovými	výfukový	k2eAgInPc7d1	výfukový
plyny	plyn	k1gInPc7	plyn
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
vyhlazovacím	vyhlazovací	k2eAgInSc6d1	vyhlazovací
automobilu	automobil	k1gInSc6	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Dodatečně	dodatečně	k6eAd1	dodatečně
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
do	do	k7c2	do
Chełmna	Chełmn	k1gInSc2	Chełmn
posláno	poslat	k5eAaPmNgNnS	poslat
ještě	ještě	k9	ještě
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
dětí	dítě	k1gFnPc2	dítě
vybraných	vybraná	k1gFnPc2	vybraná
k	k	k7c3	k
poněmčení	poněmčení	k1gNnSc3	poněmčení
hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
přeživší	přeživší	k2eAgFnPc1d1	přeživší
děti	dítě	k1gFnPc1	dítě
s	s	k7c7	s
vynaložením	vynaložení	k1gNnSc7	vynaložení
velkého	velký	k2eAgNnSc2d1	velké
úsilí	úsilí	k1gNnSc2	úsilí
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
rodinách	rodina	k1gFnPc6	rodina
a	a	k8xC	a
ústavech	ústav	k1gInPc6	ústav
vypátrat	vypátrat	k5eAaPmF	vypátrat
a	a	k8xC	a
navrátit	navrátit	k5eAaPmF	navrátit
jejich	jejich	k3xOp3gMnPc3	jejich
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
pouze	pouze	k6eAd1	pouze
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sto	sto	k4xCgNnSc4	sto
osmdesát	osmdesát	k4xCc4	osmdesát
čtyři	čtyři	k4xCgMnPc4	čtyři
lidických	lidický	k2eAgFnPc2d1	Lidická
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
byly	být	k5eAaImAgFnP	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
odtrženy	odtržen	k2eAgFnPc1d1	odtržena
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
několika	několik	k4yIc2	několik
těhotných	těhotný	k2eAgFnPc2d1	těhotná
<g/>
,	,	kIx,	,
ponechaných	ponechaný	k2eAgFnPc2d1	ponechaná
dočasně	dočasně	k6eAd1	dočasně
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
převezeno	převézt	k5eAaPmNgNnS	převézt
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Ravensbrück	Ravensbrück	k1gInSc1	Ravensbrück
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
dalších	další	k1gNnPc2	další
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
Ravensbrücku	Ravensbrück	k1gInSc2	Ravensbrück
posláno	poslán	k2eAgNnSc1d1	Posláno
dodatečně	dodatečně	k6eAd1	dodatečně
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Třiapadesát	třiapadesát	k4xCc1	třiapadesát
lidických	lidický	k2eAgFnPc2d1	Lidická
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
na	na	k7c6	na
pochodech	pochod	k1gInPc6	pochod
smrti	smrt	k1gFnSc2	smrt
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
340	[number]	k4	340
lidických	lidický	k2eAgMnPc2d1	lidický
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
192	[number]	k4	192
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
60	[number]	k4	60
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
88	[number]	k4	88
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odezva	odezva	k1gFnSc1	odezva
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Zločinné	zločinný	k2eAgNnSc1d1	zločinné
vyhlazení	vyhlazení	k1gNnSc1	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
svou	svůj	k3xOyFgFnSc7	svůj
plánovitostí	plánovitost	k1gFnSc7	plánovitost
a	a	k8xC	a
chladnokrevností	chladnokrevnost	k1gFnSc7	chladnokrevnost
pobouřilo	pobouřit	k5eAaPmAgNnS	pobouřit
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnSc2	město
a	a	k8xC	a
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc3	Brazílie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
se	se	k3xPyFc4	se
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
nacistickému	nacistický	k2eAgInSc3d1	nacistický
zločinu	zločin	k1gInSc3	zločin
přejmenovávaly	přejmenovávat	k5eAaImAgInP	přejmenovávat
na	na	k7c4	na
Lidice	Lidice	k1gInPc4	Lidice
<g/>
;	;	kIx,	;
jménem	jméno	k1gNnSc7	jméno
Lidice	Lidice	k1gInPc1	Lidice
byly	být	k5eAaImAgInP	být
křtěny	křtít	k5eAaImNgInP	křtít
právě	právě	k9	právě
narozené	narozený	k2eAgFnPc1d1	narozená
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Britští	britský	k2eAgMnPc1d1	britský
horníci	horník	k1gMnPc1	horník
založili	založit	k5eAaPmAgMnP	založit
hnutí	hnutí	k1gNnSc4	hnutí
Lidice	Lidice	k1gInPc4	Lidice
shall	shall	k1gMnSc1	shall
live	liv	k1gFnSc2	liv
(	(	kIx(	(
<g/>
Lidice	Lidice	k1gInPc1	Lidice
budou	být	k5eAaImBp3nP	být
žít	žít	k5eAaImF	žít
<g/>
)	)	kIx)	)
a	a	k8xC	a
zorganizovali	zorganizovat	k5eAaPmAgMnP	zorganizovat
sbírku	sbírka	k1gFnSc4	sbírka
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Lidice	Lidice	k1gInPc1	Lidice
bojovaly	bojovat	k5eAaImAgInP	bojovat
československé	československý	k2eAgInPc1d1	československý
tanky	tank	k1gInPc1	tank
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
i	i	k8xC	i
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
Lidic	Lidice	k1gInPc2	Lidice
byl	být	k5eAaImAgInS	být
záhy	záhy	k6eAd1	záhy
několikrát	několikrát	k6eAd1	několikrát
zfilmován	zfilmován	k2eAgInSc1d1	zfilmován
–	–	k?	–
snad	snad	k9	snad
nejpůsobivější	působivý	k2eAgInSc1d3	nejpůsobivější
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
The	The	k1gFnSc1	The
Silent	Silent	k1gMnSc1	Silent
Village	Village	k1gInSc1	Village
(	(	kIx(	(
<g/>
Tichá	Tichá	k1gFnSc1	Tichá
ves	ves	k1gFnSc1	ves
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc1d1	natočený
s	s	k7c7	s
čistě	čistě	k6eAd1	čistě
amatérskými	amatérský	k2eAgMnPc7d1	amatérský
herci	herec	k1gMnPc7	herec
v	v	k7c6	v
reálném	reálný	k2eAgNnSc6d1	reálné
prostředí	prostředí	k1gNnSc6	prostředí
velšské	velšský	k2eAgFnSc2d1	velšská
hornické	hornický	k2eAgFnSc2d1	hornická
vsi	ves	k1gFnSc2	ves
Cwmgiedd	Cwmgiedda	k1gFnPc2	Cwmgiedda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Události	událost	k1gFnPc1	událost
v	v	k7c6	v
okupovaném	okupovaný	k2eAgNnSc6d1	okupované
Československu	Československo	k1gNnSc6	Československo
nezůstaly	zůstat	k5eNaPmAgFnP	zůstat
bez	bez	k7c2	bez
politické	politický	k2eAgFnSc2d1	politická
odezvy	odezva	k1gFnSc2	odezva
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
vyvraždění	vyvraždění	k1gNnSc6	vyvraždění
Lidic	Lidice	k1gInPc2	Lidice
a	a	k8xC	a
Ležáků	ležák	k1gMnPc2	ležák
vlády	vláda	k1gFnSc2	vláda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
uznaly	uznat	k5eAaPmAgFnP	uznat
mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
za	za	k7c4	za
neplatnou	platný	k2eNgFnSc4d1	neplatná
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obnovení	obnovení	k1gNnSc1	obnovení
Lidic	Lidice	k1gInPc2	Lidice
==	==	k?	==
</s>
</p>
<p>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
bylo	být	k5eAaImAgNnS	být
místo	místo	k7c2	místo
lidické	lidický	k2eAgFnSc2d1	Lidická
tragédie	tragédie	k1gFnSc2	tragédie
provizorně	provizorně	k6eAd1	provizorně
pietně	pietně	k6eAd1	pietně
upraveno	upravit	k5eAaPmNgNnS	upravit
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
třetího	třetí	k4xOgNnSc2	třetí
výročí	výročí	k1gNnSc2	výročí
<g/>
,	,	kIx,	,
přislíbila	přislíbit	k5eAaPmAgFnS	přislíbit
československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
Lidice	Lidice	k1gInPc4	Lidice
obnovit	obnovit	k5eAaPmF	obnovit
a	a	k8xC	a
vybudovat	vybudovat	k5eAaPmF	vybudovat
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vypsána	vypsat	k5eAaPmNgFnS	vypsat
architektonická	architektonický	k2eAgFnSc1d1	architektonická
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c4	na
pietní	pietní	k2eAgFnSc4d1	pietní
úpravu	úprava	k1gFnSc4	úprava
území	území	k1gNnSc2	území
starých	starý	k2eAgInPc2d1	starý
Lidic	Lidice	k1gInPc2	Lidice
a	a	k8xC	a
výstavbu	výstavba	k1gFnSc4	výstavba
nové	nový	k2eAgFnSc2d1	nová
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
tak	tak	k6eAd1	tak
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
bývalé	bývalý	k2eAgFnSc2d1	bývalá
obce	obec	k1gFnSc2	obec
vyrostly	vyrůst	k5eAaPmAgInP	vyrůst
nové	nový	k2eAgInPc1d1	nový
Lidice	Lidice	k1gInPc1	Lidice
čítající	čítající	k2eAgInPc1d1	čítající
150	[number]	k4	150
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
starých	starý	k2eAgInPc2d1	starý
Lidic	Lidice	k1gInPc2	Lidice
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
celoročně	celoročně	k6eAd1	celoročně
přístupnou	přístupný	k2eAgFnSc7d1	přístupná
expozicí	expozice	k1gFnSc7	expozice
připomínající	připomínající	k2eAgFnSc4d1	připomínající
lidickou	lidický	k2eAgFnSc4d1	Lidická
tragédii	tragédie	k1gFnSc4	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
každého	každý	k3xTgMnSc4	každý
návštěvníka	návštěvník	k1gMnSc4	návštěvník
dojme	dojmout	k5eAaPmIp3nS	dojmout
bronzové	bronzový	k2eAgNnSc1d1	bronzové
sousoší	sousoší	k1gNnSc1	sousoší
82	[number]	k4	82
lidických	lidický	k2eAgFnPc2d1	Lidická
dětí	dítě	k1gFnPc2	dítě
zavražděných	zavražděný	k2eAgFnPc2d1	zavražděná
v	v	k7c6	v
Chełmnu	Chełmn	k1gInSc6	Chełmn
<g/>
,	,	kIx,	,
životní	životní	k2eAgNnSc4d1	životní
dílo	dílo	k1gNnSc4	dílo
sochařky	sochařka	k1gFnSc2	sochařka
Marie	Maria	k1gFnSc2	Maria
Uchytilové	Uchytilová	k1gFnSc2	Uchytilová
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
připomínkou	připomínka	k1gFnSc7	připomínka
všech	všecek	k3xTgFnPc2	všecek
dětských	dětský	k2eAgFnPc2d1	dětská
obětí	oběť	k1gFnPc2	oběť
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgFnSc3d1	podobná
události	událost	k1gFnSc3	událost
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Osud	osud	k1gInSc1	osud
Lidic	Lidice	k1gInPc2	Lidice
nebyl	být	k5eNaImAgInS	být
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
<g/>
;	;	kIx,	;
k	k	k7c3	k
masakrům	masakr	k1gInPc3	masakr
civilistů	civilista	k1gMnPc2	civilista
docházelo	docházet	k5eAaImAgNnS	docházet
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Připomeňme	připomenout	k5eAaPmRp1nP	připomenout
Kortelisy	Kortelis	k1gInPc4	Kortelis
a	a	k8xC	a
Český	český	k2eAgInSc4d1	český
Malín	Malín	k1gInSc4	Malín
(	(	kIx(	(
<g/>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chatyň	Chatyně	k1gFnPc2	Chatyně
(	(	kIx(	(
<g/>
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čenstochová	Čenstochová	k1gFnSc1	Čenstochová
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oradour-sur-Glane	Oradourur-Glan	k1gMnSc5	Oradour-sur-Glan
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marzabotto	Marzabotto	k1gNnSc4	Marzabotto
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Distomo	Distoma	k1gFnSc5	Distoma
a	a	k8xC	a
Kondomari	Kondomari	k1gNnSc4	Kondomari
(	(	kIx(	(
<g/>
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ostrý	ostrý	k2eAgInSc4d1	ostrý
Grúň	Grúň	k?	Grúň
a	a	k8xC	a
Kľak	Kľak	k1gInSc4	Kľak
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
)	)	kIx)	)
či	či	k8xC	či
české	český	k2eAgInPc4d1	český
Ležáky	Ležáky	k1gInPc4	Ležáky
<g/>
,	,	kIx,	,
moravské	moravský	k2eAgFnSc2d1	Moravská
vesnice	vesnice	k1gFnSc2	vesnice
Javoříčko	Javoříčko	k1gNnSc4	Javoříčko
a	a	k8xC	a
Prlov	Prlov	k1gInSc4	Prlov
<g/>
.	.	kIx.	.
</s>
<s>
Žádným	žádný	k3yNgInSc7	žádný
zločinem	zločin	k1gInSc7	zločin
se	se	k3xPyFc4	se
však	však	k9	však
nacisté	nacista	k1gMnPc1	nacista
před	před	k7c7	před
světem	svět	k1gInSc7	svět
nepyšnili	pyšnit	k5eNaImAgMnP	pyšnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vyhlazením	vyhlazení	k1gNnSc7	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
právě	právě	k9	právě
Lidice	Lidice	k1gInPc1	Lidice
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
světové	světový	k2eAgFnSc2d1	světová
veřejnosti	veřejnost	k1gFnSc2	veřejnost
staly	stát	k5eAaPmAgInP	stát
symbolem	symbol	k1gInSc7	symbol
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
německému	německý	k2eAgInSc3d1	německý
nacismu	nacismus	k1gInSc3	nacismus
<g/>
.	.	kIx.	.
<g/>
{{	{{	k?	{{
<g/>
chybí	chybit	k5eAaPmIp3nS	chybit
zdroj	zdroj	k1gInSc1	zdroj
<g/>
}	}	kIx)	}
Vyhlazené	vyhlazený	k2eAgFnSc2d1	vyhlazená
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
uzavřely	uzavřít	k5eAaPmAgInP	uzavřít
Memorandum	memorandum	k1gNnSc4	memorandum
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
spolupráci	spolupráce	k1gFnSc6	spolupráce
vypálených	vypálený	k2eAgFnPc2d1	vypálená
obcí	obec	k1gFnPc2	obec
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LOUDA	Louda	k1gMnSc1	Louda
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
<g/>
.	.	kIx.	.
</s>
<s>
Lidice	Lidice	k1gInPc1	Lidice
<g/>
:	:	kIx,	:
čin	čin	k1gInSc1	čin
krvavého	krvavý	k2eAgInSc2d1	krvavý
teroru	teror	k1gInSc2	teror
a	a	k8xC	a
porušení	porušení	k1gNnSc4	porušení
zákonů	zákon	k1gInPc2	zákon
i	i	k8xC	i
základních	základní	k2eAgNnPc2d1	základní
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
odbor	odbor	k1gInSc1	odbor
pro	pro	k7c4	pro
politické	politický	k2eAgNnSc4d1	politické
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
157	[number]	k4	157
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
popravených	popravený	k2eAgMnPc2d1	popravený
obyvatel	obyvatel	k1gMnPc2	obyvatel
Lidic	Lidice	k1gInPc2	Lidice
</s>
</p>
<p>
<s>
Ležáky	Ležáky	k1gInPc1	Ležáky
</s>
</p>
<p>
<s>
Heydrichiáda	heydrichiáda	k1gFnSc1	heydrichiáda
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Saidl	Saidl	k1gMnSc1	Saidl
</s>
</p>
<p>
<s>
Lidice	Lidice	k1gInPc1	Lidice
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vyhlazení	vyhlazení	k1gNnPc2	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Vyhlazení	vyhlazení	k1gNnSc2	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
Lidice	Lidice	k1gInPc1	Lidice
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
dvou	dva	k4xCgFnPc2	dva
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
:	:	kIx,	:
Lidice	Lidice	k1gInPc1	Lidice
-	-	kIx~	-
Cwmgiedd	Cwmgiedd	k1gInSc1	Cwmgiedd
</s>
</p>
<p>
<s>
Dokumenty	dokument	k1gInPc4	dokument
související	související	k2eAgInPc4d1	související
s	s	k7c7	s
Lidicemi	Lidice	k1gInPc7	Lidice
</s>
</p>
<p>
<s>
Lidice	Lidice	k1gInPc1	Lidice
na	na	k7c6	na
zanikleobce	zanikleobka	k1gFnSc6	zanikleobka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
http://www.soudobedejiny.wz.cz/data/Pala.pdf	[url]	k1gMnSc1	http://www.soudobedejiny.wz.cz/data/Pala.pdf
</s>
</p>
