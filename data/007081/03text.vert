<s>
Textura	textura	k1gFnSc1	textura
<g/>
,	,	kIx,	,
též	též	k9	též
stavba	stavba	k1gFnSc1	stavba
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlastnost	vlastnost	k1gFnSc4	vlastnost
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
prostorové	prostorový	k2eAgNnSc4d1	prostorové
uspořádání	uspořádání	k1gNnSc4	uspořádání
částic	částice	k1gFnPc2	částice
nerostů	nerost	k1gInPc2	nerost
v	v	k7c6	v
hornině	hornina	k1gFnSc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Textura	textura	k1gFnSc1	textura
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
mnohé	mnohé	k1gNnSc4	mnohé
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Nerosty	nerost	k1gInPc1	nerost
nemají	mít	k5eNaImIp3nP	mít
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
dutinky	dutinka	k1gFnPc5	dutinka
<g/>
.	.	kIx.	.
</s>
<s>
Nerosty	nerost	k1gInPc1	nerost
mají	mít	k5eAaImIp3nP	mít
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
dutinky	dutinka	k1gFnPc5	dutinka
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
je	být	k5eAaImIp3nS	být
pórovitá	pórovitý	k2eAgFnSc1d1	pórovitá
textura	textura	k1gFnSc1	textura
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
vyvřelých	vyvřelý	k2eAgFnPc2d1	vyvřelá
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
uvolňování	uvolňování	k1gNnSc6	uvolňování
různých	různý	k2eAgInPc2d1	různý
plynů	plyn	k1gInPc2	plyn
při	při	k7c6	při
snižujícím	snižující	k2eAgInSc6d1	snižující
se	se	k3xPyFc4	se
tlaku	tlak	k1gInSc3	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Nerosty	nerost	k1gInPc1	nerost
mají	mít	k5eAaImIp3nP	mít
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
dutinky	dutinka	k1gFnSc2	dutinka
vyplněné	vyplněný	k2eAgFnPc4d1	vyplněná
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
zčásti	zčásti	k6eAd1	zčásti
druhotnými	druhotný	k2eAgInPc7d1	druhotný
nerosty	nerost	k1gInPc7	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Nerostné	nerostný	k2eAgFnPc1d1	nerostná
součásti	součást	k1gFnPc1	součást
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
nahodile	nahodile	k6eAd1	nahodile
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
orientovány	orientovat	k5eAaBmNgFnP	orientovat
v	v	k7c6	v
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Součásti	součást	k1gFnPc1	součást
také	také	k9	také
nejsou	být	k5eNaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
do	do	k7c2	do
různě	různě	k6eAd1	různě
tvarovaných	tvarovaný	k2eAgInPc2d1	tvarovaný
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
nahodile	nahodile	k6eAd1	nahodile
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
hornin	hornina	k1gFnPc2	hornina
se	s	k7c7	s
všesměrnou	všesměrný	k2eAgFnSc7d1	všesměrná
texturou	textura	k1gFnSc7	textura
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
stejné	stejná	k1gFnSc2	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Nerostné	nerostný	k2eAgFnPc1d1	nerostná
součásti	součást	k1gFnPc1	součást
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
v	v	k7c6	v
kulovitých	kulovitý	k2eAgInPc6d1	kulovitý
útvarech	útvar	k1gInPc6	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Nerostné	nerostný	k2eAgFnPc1d1	nerostná
součásti	součást	k1gFnPc1	součást
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
hornin	hornina	k1gFnPc2	hornina
je	být	k5eAaImIp3nS	být
skryta	skrýt	k5eAaPmNgFnS	skrýt
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
odhalit	odhalit	k5eAaPmF	odhalit
až	až	k9	až
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
<g/>
.	.	kIx.	.
</s>
