<s>
Je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
La	la	k1gNnSc2	la
valse	valse	k1gFnSc2	valse
od	od	k7c2	od
Maurice	Maurika	k1gFnSc6	Maurika
Ravela	Ravel	k1gMnSc2	Ravel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
uváděná	uváděný	k2eAgFnSc1d1	uváděná
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
pro	pro	k7c4	pro
symfonický	symfonický	k2eAgInSc4d1	symfonický
orchestr	orchestr	k1gInSc4	orchestr
<g/>
?	?	kIx.	?
</s>
