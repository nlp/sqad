<p>
<s>
La	la	k1gNnSc1	la
valse	valse	k1gFnSc2	valse
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Valčík	valčík	k1gInSc1	valčík
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
od	od	k7c2	od
Maurice	Maurika	k1gFnSc6	Maurika
Ravela	Ravel	k1gMnSc2	Ravel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
uváděná	uváděný	k2eAgFnSc1d1	uváděná
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
pro	pro	k7c4	pro
symfonický	symfonický	k2eAgInSc4d1	symfonický
orchestr	orchestr	k1gInSc4	orchestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Myšlenku	myšlenka	k1gFnSc4	myšlenka
zkomponovat	zkomponovat	k5eAaPmF	zkomponovat
skladbu	skladba	k1gFnSc4	skladba
evokující	evokující	k2eAgInSc1d1	evokující
valčík	valčík	k1gInSc1	valčík
pojal	pojmout	k5eAaPmAgInS	pojmout
Ravel	Ravel	k1gInSc4	Ravel
poprvé	poprvé	k6eAd1	poprvé
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
jako	jako	k8xS	jako
o	o	k7c6	o
symfonické	symfonický	k2eAgFnSc6d1	symfonická
básni	báseň	k1gFnSc6	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
skladbu	skladba	k1gFnSc4	skladba
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
radost	radost	k1gFnSc4	radost
z	z	k7c2	z
tance	tanec	k1gInSc2	tanec
a	a	k8xC	a
ze	z	k7c2	z
života	život	k1gInSc2	život
vůbec	vůbec	k9	vůbec
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
však	však	k9	však
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
nerealizoval	realizovat	k5eNaBmAgMnS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
až	až	k6eAd1	až
o	o	k7c6	o
řadu	řad	k1gInSc6	řad
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
požádal	požádat	k5eAaPmAgMnS	požádat
ruský	ruský	k2eAgMnSc1d1	ruský
choreograf	choreograf	k1gMnSc1	choreograf
Sergej	Sergej	k1gMnSc1	Sergej
Ďagilev	Ďagilev	k1gMnSc1	Ďagilev
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
tanečního	taneční	k2eAgInSc2d1	taneční
souboru	soubor	k1gInSc2	soubor
Ruský	ruský	k2eAgInSc1d1	ruský
balet	balet	k1gInSc1	balet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
představách	představa	k1gFnPc6	představa
měla	mít	k5eAaImAgFnS	mít
hudba	hudba	k1gFnSc1	hudba
znázorňovat	znázorňovat	k5eAaImF	znázorňovat
císařský	císařský	k2eAgInSc4d1	císařský
dvůr	dvůr	k1gInSc4	dvůr
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
taneční	taneční	k2eAgInSc1d1	taneční
sál	sál	k1gInSc1	sál
osvětlený	osvětlený	k2eAgInSc1d1	osvětlený
lustry	lustr	k1gInPc7	lustr
a	a	k8xC	a
s	s	k7c7	s
vířícími	vířící	k2eAgInPc7d1	vířící
mraky	mrak	k1gInPc7	mrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
Ravel	Ravel	k1gMnSc1	Ravel
začal	začít	k5eAaPmAgMnS	začít
komponovat	komponovat	k5eAaImF	komponovat
La	la	k1gNnSc4	la
valse	valse	k1gFnSc2	valse
s	s	k7c7	s
podtitulkem	podtitulek	k1gInSc7	podtitulek
Poè	Poè	k1gFnSc2	Poè
choreographique	choreographique	k1gFnSc1	choreographique
tj.	tj.	kA	tj.
taneční	taneční	k2eAgFnSc1d1	taneční
báseň	báseň	k1gFnSc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
tři	tři	k4xCgFnPc4	tři
verze	verze	k1gFnPc4	verze
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
a	a	k8xC	a
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
<g/>
;	;	kIx,	;
práci	práce	k1gFnSc4	práce
dokončil	dokončit	k5eAaPmAgMnS	dokončit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
skladba	skladba	k1gFnSc1	skladba
hotova	hotov	k2eAgFnSc1d1	hotova
<g/>
,	,	kIx,	,
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Ravel	Ravel	k1gInSc4	Ravel
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
pianistou	pianista	k1gMnSc7	pianista
verzi	verze	k1gFnSc4	verze
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
objednavateli	objednavatel	k1gMnSc3	objednavatel
-	-	kIx~	-
Ďagilevovi	Ďagileva	k1gMnSc3	Ďagileva
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
něhož	jenž	k3xRgMnSc4	jenž
naslouchali	naslouchat	k5eAaImAgMnP	naslouchat
i	i	k9	i
Igor	Igor	k1gMnSc1	Igor
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
a	a	k8xC	a
Francis	Francis	k1gFnSc1	Francis
Poulenc	Poulenc	k1gFnSc1	Poulenc
<g/>
.	.	kIx.	.
</s>
<s>
Ďagilev	Ďagilet	k5eAaPmDgInS	Ďagilet
ocenil	ocenit	k5eAaPmAgMnS	ocenit
hudební	hudební	k2eAgInSc4d1	hudební
obsah	obsah	k1gInSc4	obsah
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
balet	balet	k1gInSc4	balet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Ravela	Ravela	k1gFnSc1	Ravela
tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
silné	silný	k2eAgNnSc4d1	silné
rozčarování	rozčarování	k1gNnSc4	rozčarování
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
rozchodu	rozchod	k1gInSc3	rozchod
s	s	k7c7	s
Ďagilevem	Ďagilev	k1gInSc7	Ďagilev
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
pak	pak	k6eAd1	pak
předurčila	předurčit	k5eAaPmAgFnS	předurčit
La	la	k1gNnSc4	la
valse	valse	k1gFnSc2	valse
pro	pro	k7c4	pro
koncertní	koncertní	k2eAgNnSc4d1	koncertní
provedení	provedení	k1gNnSc4	provedení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Premiérové	premiér	k1gMnPc1	premiér
uvedení	uvedený	k2eAgMnPc1d1	uvedený
orchestrem	orchestr	k1gInSc7	orchestr
Orchestre	orchestr	k1gInSc5	orchestr
de	de	k?	de
Colonne	Colonn	k1gInSc5	Colonn
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1920	[number]	k4	1920
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Camille	Camill	k1gMnSc2	Camill
Chevillarda	Chevillard	k1gMnSc2	Chevillard
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
o	o	k7c4	o
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
skladba	skladba	k1gFnSc1	skladba
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
baletním	baletní	k2eAgNnSc6d1	baletní
provedení	provedení	k1gNnSc6	provedení
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
Opeře	opera	k1gFnSc6	opera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
choreografii	choreografie	k1gFnSc6	choreografie
Bronislavy	Bronislav	k1gMnPc4	Bronislav
Nižinské	Nižinský	k2eAgFnPc1d1	Nižinský
tančil	tančit	k5eAaImAgMnS	tančit
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
ústřední	ústřední	k2eAgInSc4d1	ústřední
pár	pár	k1gInSc4	pár
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Anatol	Anatol	k1gInSc4	Anatol
Vilžak	Vilžak	k1gMnSc1	Vilžak
a	a	k8xC	a
Ida	Ida	k1gFnSc1	Ida
Rubinsteinová	Rubinsteinová	k1gFnSc1	Rubinsteinová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
tančila	tančit	k5eAaImAgFnS	tančit
také	také	k9	také
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
při	při	k7c6	při
premiéře	premiéra	k1gFnSc6	premiéra
Ravelova	Ravelův	k2eAgNnSc2d1	Ravelovo
Bolera	bolero	k1gNnSc2	bolero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
jednovětá	jednovětý	k2eAgFnSc1d1	jednovětá
<g/>
,	,	kIx,	,
psaná	psaný	k2eAgFnSc1d1	psaná
v	v	k7c6	v
tříčtvrťovém	tříčtvrťový	k2eAgInSc6d1	tříčtvrťový
rytmu	rytmus	k1gInSc6	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
melodií	melodie	k1gFnPc2	melodie
v	v	k7c6	v
hlubokých	hluboký	k2eAgFnPc6d1	hluboká
polohách	poloha	k1gFnPc6	poloha
<g/>
,	,	kIx,	,
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
rytmus	rytmus	k1gInSc1	rytmus
je	být	k5eAaImIp3nS	být
místy	místy	k6eAd1	místy
přerušován	přerušován	k2eAgInSc1d1	přerušován
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vynořují	vynořovat	k5eAaImIp3nP	vynořovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
melodické	melodický	k2eAgInPc4d1	melodický
hudební	hudební	k2eAgInPc4d1	hudební
motivy	motiv	k1gInPc4	motiv
vyjadřující	vyjadřující	k2eAgInSc4d1	vyjadřující
tanec	tanec	k1gInSc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
náhle	náhle	k6eAd1	náhle
ve	v	k7c6	v
forte	forte	k1gNnSc6	forte
přerušovány	přerušovat	k5eAaImNgInP	přerušovat
částečně	částečně	k6eAd1	částečně
disharmonickou	disharmonický	k2eAgFnSc7d1	disharmonická
melodií	melodie	k1gFnSc7	melodie
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
odrážejí	odrážet	k5eAaImIp3nP	odrážet
Ravelovy	Ravelův	k2eAgInPc1d1	Ravelův
dojmy	dojem	k1gInPc1	dojem
z	z	k7c2	z
nedávno	nedávno	k6eAd1	nedávno
skončené	skončený	k2eAgFnSc2d1	skončená
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
občas	občas	k6eAd1	občas
zvolňuje	zvolňovat	k5eAaImIp3nS	zvolňovat
v	v	k7c6	v
pasážích	pasáž	k1gFnPc6	pasáž
s	s	k7c7	s
melancholickým	melancholický	k2eAgNnSc7d1	melancholické
laděním	ladění	k1gNnSc7	ladění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samém	samý	k3xTgInSc6	samý
závěru	závěr	k1gInSc6	závěr
hudba	hudba	k1gFnSc1	hudba
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
forte	forte	k1gNnSc2	forte
<g/>
,	,	kIx,	,
rytmus	rytmus	k1gInSc1	rytmus
se	se	k3xPyFc4	se
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
fortissimu	fortissimo	k1gNnSc6	fortissimo
disharmonickými	disharmonický	k2eAgInPc7d1	disharmonický
akordy	akord	k1gInPc7	akord
evokujícími	evokující	k2eAgInPc7d1	evokující
konec	konec	k1gInSc4	konec
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
starého	starý	k2eAgNnSc2d1	staré
uspořádání	uspořádání	k1gNnSc2	uspořádání
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
skladby	skladba	k1gFnSc2	skladba
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gFnSc1	Vlasta
Reiterová	Reiterová	k1gFnSc1	Reiterová
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
koncertů	koncert	k1gInPc2	koncert
České	český	k2eAgFnSc2d1	Česká
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
</s>
</p>
