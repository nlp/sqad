<s>
Extensible	Extensible	k6eAd1
Markup	Markup	k1gInSc1
Language	language	k1gFnSc2
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
ikˈ	ikˈ	k5eAaPmAgMnS
ˈ	ˈ	k1gMnSc1
ˈ	ˈ	k1gFnSc4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
XML	XML	kA
[	[	kIx(
<g/>
ˌ	ˌ	k1gMnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
rozšiřitelný	rozšiřitelný	k2eAgInSc4d1
značkovací	značkovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
obecný	obecný	k2eAgInSc1d1
značkovací	značkovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
a	a	k8xC
standardizován	standardizovat	k5eAaBmNgInS
konsorciem	konsorcium	k1gNnSc7
W	W	kA
<g/>
3	#num#	k4
<g/>
C.	C.	kA
Je	být	k5eAaImIp3nS
zjednodušenou	zjednodušený	k2eAgFnSc7d1
podobou	podoba	k1gFnSc7
staršího	starý	k2eAgInSc2d2
jazyka	jazyk	k1gInSc2
SGML	SGML	kA
<g/>
.	.	kIx.
</s>