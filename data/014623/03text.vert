<s>
Extensible	Extensible	k6eAd1
Markup	Markup	k1gInSc1
Language	language	k1gFnSc2
</s>
<s>
XML	XML	kA
</s>
<s>
Přípona	přípona	k1gFnSc1
souboru	soubor	k1gInSc2
</s>
<s>
<g/>
xml	xml	k?
Typ	typa	k1gFnPc2
internetového	internetový	k2eAgNnSc2d1
média	médium	k1gNnSc2
</s>
<s>
application	application	k1gInSc1
<g/>
/	/	kIx~
<g/>
xmltext	xmltext	k1gInSc1
<g/>
/	/	kIx~
<g/>
xml	xml	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Uniform	Uniform	k1gInSc1
Type	typ	k1gInSc5
Identifier	Identifier	k1gInSc1
</s>
<s>
public	publicum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
xml	xml	k?
Tvůrce	tvůrce	k1gMnSc1
</s>
<s>
World	World	k1gMnSc1
Wide	Wid	k1gFnSc2
Web	web	k1gInSc1
Consortium	Consortium	k1gNnSc1
Typ	typa	k1gFnPc2
formátu	formát	k1gInSc2
</s>
<s>
značkovací	značkovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Standard	standard	k1gInSc1
<g/>
(	(	kIx(
<g/>
y	y	k?
<g/>
)	)	kIx)
</s>
<s>
1.0	1.0	k4
(	(	kIx(
<g/>
Fifth	Fifth	k1gInSc1
Edition	Edition	k1gInSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
1.1	1.1	k4
(	(	kIx(
<g/>
Second	Second	k1gInSc1
Edition	Edition	k1gInSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2006	#num#	k4
<g/>
)	)	kIx)
Otevřený	otevřený	k2eAgInSc1d1
formát	formát	k1gInSc1
</s>
<s>
ano	ano	k9
Website	Websit	k1gInSc5
</s>
<s>
XML	XML	kA
1.0	1.0	k4
</s>
<s>
Extensible	Extensible	k6eAd1
Markup	Markup	k1gInSc1
Language	language	k1gFnSc2
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
ikˈ	ikˈ	k5eAaPmAgMnS
ˈ	ˈ	k1gMnSc1
ˈ	ˈ	k1gFnSc4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
XML	XML	kA
[	[	kIx(
<g/>
ˌ	ˌ	k1gMnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
rozšiřitelný	rozšiřitelný	k2eAgInSc4d1
značkovací	značkovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
obecný	obecný	k2eAgInSc1d1
značkovací	značkovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
a	a	k8xC
standardizován	standardizovat	k5eAaBmNgInS
konsorciem	konsorcium	k1gNnSc7
W	W	kA
<g/>
3	#num#	k4
<g/>
C.	C.	kA
Je	být	k5eAaImIp3nS
zjednodušenou	zjednodušený	k2eAgFnSc7d1
podobou	podoba	k1gFnSc7
staršího	starý	k2eAgInSc2d2
jazyka	jazyk	k1gInSc2
SGML	SGML	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
snadné	snadný	k2eAgNnSc1d1
vytváření	vytváření	k1gNnSc1
konkrétních	konkrétní	k2eAgInPc2d1
značkovacích	značkovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
aplikací	aplikace	k1gFnPc2
<g/>
)	)	kIx)
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
účely	účel	k1gInPc4
a	a	k8xC
různé	různý	k2eAgInPc4d1
typy	typ	k1gInPc4
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
serializaci	serializace	k1gFnSc4
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
čemž	což	k3yRnSc6,k3yQnSc6
soupeří	soupeřit	k5eAaImIp3nS
např.	např.	kA
s	s	k7c7
JSON	JSON	kA
či	či	k8xC
YAML	YAML	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpracování	zpracování	k1gNnSc1
XML	XML	kA
je	být	k5eAaImIp3nS
podporováno	podporován	k2eAgNnSc1d1
řadou	řada	k1gFnSc7
nástrojů	nástroj	k1gInPc2
a	a	k8xC
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jazyk	jazyk	k1gInSc1
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
především	především	k9
pro	pro	k7c4
výměnu	výměna	k1gFnSc4
dat	datum	k1gNnPc2
mezi	mezi	k7c7
aplikacemi	aplikace	k1gFnPc7
a	a	k8xC
pro	pro	k7c4
publikování	publikování	k1gNnSc4
dokumentů	dokument	k1gInPc2
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
popisuje	popisovat	k5eAaImIp3nS
strukturu	struktura	k1gFnSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
věcného	věcný	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
,	,	kIx,
nezabývá	zabývat	k5eNaImIp3nS
se	s	k7c7
vzhledem	vzhled	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezentace	prezentace	k1gFnSc1
dokumentu	dokument	k1gInSc2
(	(	kIx(
<g/>
vzhled	vzhled	k1gInSc1
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
definována	definovat	k5eAaBmNgFnS
pomocí	pomocí	k7c2
kaskádových	kaskádový	k2eAgInPc2d1
stylů	styl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
zpracování	zpracování	k1gNnSc2
je	být	k5eAaImIp3nS
transformace	transformace	k1gFnSc1
do	do	k7c2
jiného	jiný	k2eAgInSc2d1
typu	typ	k1gInSc2
dokumentu	dokument	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
do	do	k7c2
jiné	jiný	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
XML	XML	kA
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
XML	XML	kA
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
formát	formát	k1gInSc1
pro	pro	k7c4
výměnu	výměna	k1gFnSc4
informací	informace	k1gFnPc2
</s>
<s>
Není	být	k5eNaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
zasílat	zasílat	k5eAaImF
dokumenty	dokument	k1gInPc4
ve	v	k7c6
tvaru	tvar	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
speciální	speciální	k2eAgInSc1d1
software	software	k1gInSc1
konkrétní	konkrétní	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
(	(	kIx(
<g/>
resp.	resp.	kA
nadace	nadace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
např.	např.	kA
formáty	formát	k1gInPc4
DOC	doc	kA
<g/>
,	,	kIx,
XLS	XLS	kA
<g/>
,	,	kIx,
ODT	ODT	kA
nebo	nebo	k8xC
T	T	kA
<g/>
602	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
používána	používán	k2eAgFnSc1d1
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
operačních	operační	k2eAgInPc2d1
a	a	k8xC
informačních	informační	k2eAgInPc2d1
systémů	systém	k1gInPc2
a	a	k8xC
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
uživatel	uživatel	k1gMnSc1
vlastní	vlastnit	k5eAaImIp3nS
příslušný	příslušný	k2eAgInSc4d1
software	software	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS
tak	tak	k9
potřeba	potřeba	k1gFnSc1
vytvořit	vytvořit	k5eAaPmF
nějaký	nějaký	k3yIgInSc4
jednoduchý	jednoduchý	k2eAgInSc4d1
otevřený	otevřený	k2eAgInSc4d1
formát	formát	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
není	být	k5eNaImIp3nS
úzce	úzko	k6eAd1
svázán	svázat	k5eAaPmNgMnS
s	s	k7c7
nějakou	nějaký	k3yIgFnSc7
platformou	platforma	k1gFnSc7
nebo	nebo	k8xC
proprietární	proprietární	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
právě	právě	k9
XML	XML	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
jednoduchém	jednoduchý	k2eAgInSc6d1
textu	text	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
zpracovatelný	zpracovatelný	k2eAgInSc1d1
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
<g/>
)	)	kIx)
libovolným	libovolný	k2eAgInSc7d1
textovým	textový	k2eAgInSc7d1
editorem	editor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Specifikace	specifikace	k1gFnSc1
XML	XML	kA
konsorcia	konsorcium	k1gNnSc2
W3C	W3C	k1gMnSc4
je	být	k5eAaImIp3nS
zdarma	zdarma	k6eAd1
přístupná	přístupný	k2eAgFnSc1d1
všem	všecek	k3xTgNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
tak	tak	k9
může	moct	k5eAaImIp3nS
bez	bez	k7c2
problémů	problém	k1gInPc2
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
aplikací	aplikace	k1gFnPc2
implementovat	implementovat	k5eAaImF
podporu	podpora	k1gFnSc4
XML	XML	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
velký	velký	k2eAgInSc4d1
rozdíl	rozdíl	k1gInSc4
oproti	oproti	k7c3
firemním	firemní	k2eAgInPc3d1
formátům	formát	k1gInPc3
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
není	být	k5eNaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
žádná	žádný	k3yNgFnSc1
dokumentace	dokumentace	k1gFnSc1
a	a	k8xC
navíc	navíc	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
XML	XML	kA
o	o	k7c6
velice	velice	k6eAd1
složité	složitý	k2eAgFnSc6d1
<g/>
,	,	kIx,
často	často	k6eAd1
binární	binární	k2eAgInPc1d1
<g/>
,	,	kIx,
formáty	formát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
</s>
<s>
XML	XML	kA
hned	hned	k6eAd1
od	od	k7c2
samého	samý	k3xTgInSc2
počátku	počátek	k1gInSc2
myslel	myslet	k5eAaImAgMnS
na	na	k7c4
potřeby	potřeba	k1gFnPc4
i	i	k8xC
jiných	jiný	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
než	než	k8xS
je	být	k5eAaImIp3nS
angličtina	angličtina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
znaková	znakový	k2eAgFnSc1d1
sada	sada	k1gFnSc1
se	se	k3xPyFc4
implicitně	implicitně	k6eAd1
používá	používat	k5eAaImIp3nS
ISO	ISO	kA
10646	#num#	k4
(	(	kIx(
<g/>
také	také	k9
Unicode	Unicod	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
XML	XML	kA
proto	proto	k8xC
můžeme	moct	k5eAaImIp1nP
vytvářet	vytvářet	k5eAaImF
dokumenty	dokument	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
obsahují	obsahovat	k5eAaImIp3nP
texty	text	k1gInPc4
v	v	k7c6
mnoha	mnoho	k4c6
jazycích	jazyk	k1gInPc6
najednou	najednou	k6eAd1
–	–	k?
můžeme	moct	k5eAaImIp1nP
přepínat	přepínat	k5eAaImF
mezi	mezi	k7c7
různými	různý	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
v	v	k7c6
jednom	jeden	k4xCgInSc6
dokumentu	dokument	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
je	být	k5eAaImIp3nS
přípustné	přípustný	k2eAgNnSc1d1
i	i	k8xC
jiné	jiný	k2eAgNnSc1d1
libovolné	libovolný	k2eAgNnSc1d1
kódování	kódování	k1gNnSc1
(	(	kIx(
<g/>
např.	např.	kA
CP-	CP-	k1gFnSc1
<g/>
1250	#num#	k4
<g/>
,	,	kIx,
ISO	ISO	kA
8859	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
však	však	k9
být	být	k5eAaImF
v	v	k7c6
každém	každý	k3xTgInSc6
dokumentu	dokument	k1gInSc6
přesně	přesně	k6eAd1
určeno	určit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpadají	odpadat	k5eAaImIp3nP
tak	tak	k6eAd1
problémy	problém	k1gInPc1
s	s	k7c7
konverzí	konverze	k1gFnSc7
z	z	k7c2
jednoho	jeden	k4xCgNnSc2
kódování	kódování	k1gNnSc2
do	do	k7c2
druhého	druhý	k4xOgNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
obsah	obsah	k1gInSc1
</s>
<s>
Pomocí	pomocí	k7c2
XML	XML	kA
značek	značka	k1gFnPc2
(	(	kIx(
<g/>
tagů	tag	k1gInPc2
<g/>
)	)	kIx)
vyznačujeme	vyznačovat	k5eAaImIp1nP
v	v	k7c6
dokumentu	dokument	k1gInSc6
význam	význam	k1gInSc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
textu	text	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokumenty	dokument	k1gInPc1
tak	tak	k8xC,k8xS
obsahují	obsahovat	k5eAaImIp3nP
více	hodně	k6eAd2
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
než	než	k8xS
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
používalo	používat	k5eAaImAgNnS
značkovaní	značkovaný	k2eAgMnPc1d1
zaměřené	zaměřený	k2eAgNnSc4d1
na	na	k7c4
prezentaci	prezentace	k1gFnSc4
(	(	kIx(
<g/>
vzhled	vzhled	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
definice	definice	k1gFnSc2
písma	písmo	k1gNnSc2
<g/>
,	,	kIx,
odsazení	odsazení	k1gNnSc2
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
XML	XML	kA
dokumenty	dokument	k1gInPc4
jsou	být	k5eAaImIp3nP
informačně	informačně	k6eAd1
bohatší	bohatý	k2eAgMnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
lze	lze	k6eAd1
s	s	k7c7
výhodou	výhoda	k1gFnSc7
využít	využít	k5eAaPmF
v	v	k7c6
mnoha	mnoho	k4c6
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
přínos	přínos	k1gInSc1
bude	být	k5eAaImBp3nS
samozřejmě	samozřejmě	k6eAd1
pro	pro	k7c4
prohledávání	prohledávání	k1gNnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
můžeme	moct	k5eAaImIp1nP
určit	určit	k5eAaPmF
i	i	k9
jaký	jaký	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
význam	význam	k1gInSc4
má	mít	k5eAaImIp3nS
mít	mít	k5eAaImF
hledaný	hledaný	k2eAgInSc4d1
text	text	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Snadná	snadný	k2eAgFnSc1d1
konverze	konverze	k1gFnSc1
do	do	k7c2
jiných	jiný	k2eAgInPc2d1
formátů	formát	k1gInPc2
</s>
<s>
Při	při	k7c6
používání	používání	k1gNnSc6
XML	XML	kA
dokumentu	dokument	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
požadováno	požadovat	k5eAaImNgNnS
jeho	jeho	k3xOp3gNnSc1
zobrazení	zobrazení	k1gNnSc1
koncovému	koncový	k2eAgMnSc3d1
uživateli	uživatel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
XML	XML	kA
samo	sám	k3xTgNnSc1
o	o	k7c4
sobě	se	k3xPyFc3
žádné	žádný	k3yNgInPc4
prostředky	prostředek	k1gInPc4
pro	pro	k7c4
definici	definice	k1gFnSc4
vzhledu	vzhled	k1gInSc2
nenabízí	nabízet	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
ale	ale	k9
několik	několik	k4yIc4
stylových	stylový	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
umožňují	umožňovat	k5eAaImIp3nP
definovat	definovat	k5eAaBmF
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
mají	mít	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc1d1
elementy	element	k1gInPc1
zobrazit	zobrazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souboru	soubor	k1gInSc2
pravidel	pravidlo	k1gNnPc2
nebo	nebo	k8xC
příkazů	příkaz	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
definují	definovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
dokument	dokument	k1gInSc1
převede	převést	k5eAaPmIp3nS
do	do	k7c2
jiného	jiný	k2eAgInSc2d1
formátu	formát	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
šablona	šablona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Jeden	jeden	k4xCgInSc4
vytvořený	vytvořený	k2eAgInSc4d1
styl	styl	k1gInSc4
můžeme	moct	k5eAaImIp1nP
aplikovat	aplikovat	k5eAaBmF
na	na	k7c4
mnoho	mnoho	k4c4
dokumentů	dokument	k1gInPc2
stejného	stejný	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
můžeme	moct	k5eAaImIp1nP
na	na	k7c4
jeden	jeden	k4xCgInSc4
dokument	dokument	k1gInSc4
aplikovat	aplikovat	k5eAaBmF
několik	několik	k4yIc4
různých	různý	k2eAgInPc2d1
stylů	styl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
např.	např.	kA
PostScriptový	postscriptový	k2eAgInSc4d1
soubor	soubor	k1gInSc4
<g/>
,	,	kIx,
HTML	HTML	kA
kód	kód	k1gInSc4
nebo	nebo	k8xC
XML	XML	kA
s	s	k7c7
obsahem	obsah	k1gInSc7
původního	původní	k2eAgInSc2d1
dokumentu	dokument	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stylových	stylový	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
existuje	existovat	k5eAaImIp3nS
dnes	dnes	k6eAd1
několik	několik	k4yIc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgInPc4d3
patří	patřit	k5eAaImIp3nS
asi	asi	k9
kaskádové	kaskádový	k2eAgInPc1d1
styly	styl	k1gInPc1
(	(	kIx(
<g/>
CSS	CSS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
lze	lze	k6eAd1
použít	použít	k5eAaPmF
pouze	pouze	k6eAd1
pro	pro	k7c4
jednoduché	jednoduchý	k2eAgNnSc4d1
formátování	formátování	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
dobře	dobře	k6eAd1
poslouží	posloužit	k5eAaPmIp3nS
pro	pro	k7c4
zobrazení	zobrazení	k1gNnSc4
dokumentu	dokument	k1gInSc2
na	na	k7c6
obrazovce	obrazovka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
rodina	rodina	k1gFnSc1
jazyků	jazyk	k1gInPc2
XSL	XSL	kA
(	(	kIx(
<g/>
eXtensible	eXtensible	k6eAd1
Stylesheet	Stylesheet	k1gMnSc1
Language	language	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
dokument	dokument	k1gInSc4
různě	různě	k6eAd1
upravovat	upravovat	k5eAaImF
a	a	k8xC
transformovat	transformovat	k5eAaBmF
–	–	k?
vybírat	vybírat	k5eAaImF
části	část	k1gFnPc4
dokumentu	dokument	k1gInSc2
nebo	nebo	k8xC
generovat	generovat	k5eAaImF
obsahy	obsah	k1gInPc4
a	a	k8xC
rejstříky	rejstřík	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Automatická	automatický	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
struktury	struktura	k1gFnSc2
dokumentu	dokument	k1gInSc2
</s>
<s>
XML	XML	kA
neobsahuje	obsahovat	k5eNaImIp3nS
předdefinované	předdefinovaný	k2eAgFnPc4d1
značky	značka	k1gFnPc4
(	(	kIx(
<g/>
tagy	tag	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
definovat	definovat	k5eAaBmF
vlastní	vlastní	k2eAgFnPc4d1
značky	značka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
budeme	být	k5eAaImBp1nP
používat	používat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgFnPc4
značky	značka	k1gFnPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
(	(	kIx(
<g/>
nepovinně	povinně	k6eNd1
<g/>
)	)	kIx)
definovat	definovat	k5eAaBmF
v	v	k7c6
souboru	soubor	k1gInSc6
DTD	DTD	kA
(	(	kIx(
<g/>
Document	Document	k1gMnSc1
Type	typ	k1gInSc5
Definition	Definition	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
XSD	XSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
automaticky	automaticky	k6eAd1
kontrolovat	kontrolovat	k5eAaImF
<g/>
,	,	kIx,
zda	zda	k8xS
vytvářený	vytvářený	k2eAgInSc1d1
XML	XML	kA
dokument	dokument	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
této	tento	k3xDgFnSc3
definici	definice	k1gFnSc3
(	(	kIx(
<g/>
tj.	tj.	kA
je	být	k5eAaImIp3nS
validní	validní	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Program	program	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
tyto	tento	k3xDgFnPc4
kontroly	kontrola	k1gFnPc4
provádí	provádět	k5eAaImIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
parser	parser	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vývoji	vývoj	k1gInSc6
aplikací	aplikace	k1gFnPc2
můžeme	moct	k5eAaImIp1nP
parser	parser	k1gInSc1
použít	použít	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
ten	ten	k3xDgMnSc1
za	za	k7c2
nás	my	k3xPp1nPc2
detekuje	detekovat	k5eAaImIp3nS
většinu	většina	k1gFnSc4
chyb	chyba	k1gFnPc2
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
DTD	DTD	kA
není	být	k5eNaImIp3nS
jediný	jediný	k2eAgInSc1d1
definiční	definiční	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
pro	pro	k7c4
XML	XML	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neobsahuje	obsahovat	k5eNaImIp3nS
možnost	možnost	k1gFnSc4
kontrolovat	kontrolovat	k5eAaImF
typy	typ	k1gInPc4
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
měnové	měnový	k2eAgInPc1d1
údaje	údaj	k1gInPc1
<g/>
,	,	kIx,
údaje	údaj	k1gInPc1
o	o	k7c6
datu	datum	k1gNnSc6
a	a	k8xC
čase	čas	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
vlastnost	vlastnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
při	při	k7c6
zpracování	zpracování	k1gNnSc6
dat	datum	k1gNnPc2
databázového	databázový	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
pod	pod	k7c7
názvem	název	k1gInSc7
XML	XML	kA
schémata	schéma	k1gNnPc1
XSD	XSD	kA
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c6
půdě	půda	k1gFnSc6
konsorcia	konsorcium	k1gNnSc2
W3C	W3C	k1gFnPc2
na	na	k7c6
vytvoření	vytvoření	k1gNnSc6
jednotného	jednotný	k2eAgInSc2d1
standardu	standard	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
tyto	tento	k3xDgFnPc4
kontroly	kontrola	k1gFnPc4
umožní	umožnit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
standardní	standardní	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
byla	být	k5eAaImAgNnP
postupně	postupně	k6eAd1
vytvořena	vytvořen	k2eAgNnPc1d1
schémata	schéma	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
definují	definovat	k5eAaBmIp3nP
značky	značka	k1gFnPc4
(	(	kIx(
<g/>
názvy	název	k1gInPc1
elementů	element	k1gInPc2
<g/>
)	)	kIx)
pro	pro	k7c4
konkrétní	konkrétní	k2eAgInPc4d1
typy	typ	k1gInPc4
dokumentů	dokument	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
DocBook	DocBook	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
definuje	definovat	k5eAaBmIp3nS
struktury	struktura	k1gFnPc4
pro	pro	k7c4
vytváření	vytváření	k1gNnSc4
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
článků	článek	k1gInPc2
<g/>
,	,	kIx,
vědeckých	vědecký	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
takových	takový	k3xDgFnPc2
aplikací	aplikace	k1gFnPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
současně	současně	k6eAd1
s	s	k7c7
definičními	definiční	k2eAgInPc7d1
soubory	soubor	k1gInPc7
DTD	DTD	kA
je	být	k5eAaImIp3nS
dodávána	dodáván	k2eAgFnSc1d1
sada	sada	k1gFnSc1
stylů	styl	k1gInPc2
(	(	kIx(
<g/>
XSL	XSL	kA
souborů	soubor	k1gInPc2
<g/>
)	)	kIx)
pro	pro	k7c4
následné	následný	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
a	a	k8xC
přípravu	příprava	k1gFnSc4
pro	pro	k7c4
tisk	tisk	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
pro	pro	k7c4
převod	převod	k1gInSc4
do	do	k7c2
jiných	jiný	k2eAgInPc2d1
standardních	standardní	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
(	(	kIx(
<g/>
PostScript	PostScript	k1gMnSc1
<g/>
,	,	kIx,
HTML	HTML	kA
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
XML	XML	kA
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
jednom	jeden	k4xCgInSc6
dokumentu	dokument	k1gInSc6
můžeme	moct	k5eAaImIp1nP
používat	používat	k5eAaImF
najednou	najednou	k6eAd1
nezávisle	závisle	k6eNd1
na	na	k7c6
sobě	sebe	k3xPyFc6
několik	několik	k4yIc4
druhů	druh	k1gInPc2
značkování	značkování	k1gNnSc2
pomocí	pomocí	k7c2
jmenných	jmenný	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
(	(	kIx(
<g/>
namespaces	namespaces	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
kombinovat	kombinovat	k5eAaImF
v	v	k7c6
jednom	jeden	k4xCgInSc6
dokumentů	dokument	k1gInPc2
několik	několik	k4yIc4
různých	různý	k2eAgFnPc2d1
definic	definice	k1gFnPc2
ve	v	k7c6
formě	forma	k1gFnSc6
DTD	DTD	kA
nebo	nebo	k8xC
schémat	schéma	k1gNnPc2
bez	bez	k7c2
konfliktů	konflikt	k1gInPc2
v	v	k7c4
pojmenování	pojmenování	k1gNnSc4
elementů	element	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Hypertext	hypertext	k1gInSc1
a	a	k8xC
odkazy	odkaz	k1gInPc1
</s>
<s>
XML	XML	kA
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
HTML	HTML	kA
umožňuje	umožňovat	k5eAaImIp3nS
vytváření	vytváření	k1gNnSc4
odkazů	odkaz	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
jednoho	jeden	k4xCgInSc2
dokumentu	dokument	k1gInSc2
i	i	k9
mezi	mezi	k7c7
dokumenty	dokument	k1gInPc7
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
však	však	k9
více	hodně	k6eAd2
možností	možnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vytvářet	vytvářet	k5eAaImF
i	i	k9
vícesměrné	vícesměrný	k2eAgInPc4d1
odkazy	odkaz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
spojují	spojovat	k5eAaImIp3nP
více	hodně	k6eAd2
dokumentů	dokument	k1gInPc2
dohromady	dohromady	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvorba	tvorba	k1gFnSc1
odkazů	odkaz	k1gInPc2
je	být	k5eAaImIp3nS
popsána	popsat	k5eAaPmNgFnS
ve	v	k7c6
třech	tři	k4xCgInPc6
standardech	standard	k1gInPc6
–	–	k?
XLink	XLinko	k1gNnPc2
<g/>
,	,	kIx,
XPointer	XPointra	k1gFnPc2
a	a	k8xC
XPath	XPatha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
XPath	XPath	k1gMnSc1
(	(	kIx(
<g/>
XML	XML	kA
Path	Path	k1gInSc1
Language	language	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
adresovat	adresovat	k5eAaBmF
jednotlivé	jednotlivý	k2eAgFnPc4d1
části	část	k1gFnPc4
dokumentu	dokument	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
XPointer	XPointer	k1gMnSc1
(	(	kIx(
<g/>
XML	XML	kA
Pointer	pointer	k1gInSc1
Language	language	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
rozšířením	rozšíření	k1gNnSc7
XPath	XPatha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
k	k	k7c3
určování	určování	k1gNnSc3
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
dokumentu	dokument	k1gInSc2
ve	v	k7c6
stylu	styl	k1gInSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
zajímá	zajímat	k5eAaImIp3nS
mě	já	k3xPp1nSc2
první	první	k4xOgFnSc6
odstavec	odstavec	k1gInSc1
třetí	třetí	k4xOgFnSc2
kapitoly	kapitola	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
nutné	nutný	k2eAgNnSc1d1
ty	ten	k3xDgFnPc4
části	část	k1gFnPc4
dokumentu	dokument	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
chceme	chtít	k5eAaImIp1nP
odkazovat	odkazovat	k5eAaImF
<g/>
,	,	kIx,
explicitně	explicitně	k6eAd1
označovat	označovat	k5eAaImF
pomocí	pomocí	k7c2
návěstí	návěstí	k1gNnSc2
jako	jako	k8xS,k8xC
v	v	k7c6
HTML	HTML	kA
<g/>
.	.	kIx.
</s>
<s>
XLink	XLink	k1gMnSc1
(	(	kIx(
<g/>
XML	XML	kA
Linking	Linking	k1gInSc1
Language	language	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
samotný	samotný	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
odkazů	odkaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
se	se	k3xPyFc4
určují	určovat	k5eAaImIp3nP
pomocí	pomocí	k7c2
jejich	jejich	k3xOp3gFnSc2
URL	URL	kA
adresy	adresa	k1gFnPc1
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
ještě	ještě	k9
XPointer	XPointer	k1gInSc4
pro	pro	k7c4
přesnější	přesný	k2eAgNnSc4d2
určení	určení	k1gNnSc4
části	část	k1gFnSc2
dokumentu	dokument	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Syntaxe	syntaxe	k1gFnSc1
XML	XML	kA
</s>
<s>
XML	XML	kA
dokument	dokument	k1gInSc1
je	být	k5eAaImIp3nS
text	text	k1gInSc4
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
Unicode	Unicod	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
obvykle	obvykle	k6eAd1
kódovaný	kódovaný	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
UTF-	UTF-	k1gMnSc1
<g/>
8	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
přípustná	přípustný	k2eAgNnPc4d1
i	i	k8xC
jiná	jiný	k2eAgNnPc4d1
kódování	kódování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
např.	např.	kA
HTML	HTML	kA
<g/>
,	,	kIx,
efektivita	efektivita	k1gFnSc1
XML	XML	kA
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
struktuře	struktura	k1gFnSc6
<g/>
,	,	kIx,
obsahu	obsah	k1gInSc6
a	a	k8xC
integritě	integrita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
dokument	dokument	k1gInSc1
považován	považován	k2eAgInSc1d1
za	za	k7c4
správně	správně	k6eAd1
strukturovaný	strukturovaný	k2eAgInSc4d1
(	(	kIx(
<g/>
„	„	k?
<g/>
well-formed	well-formed	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
,	,	kIx,
musí	muset	k5eAaImIp3nS
splňovat	splňovat	k5eAaImF
aspoň	aspoň	k9
následující	následující	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
právě	právě	k9
jeden	jeden	k4xCgInSc4
kořenový	kořenový	k2eAgInSc4d1
(	(	kIx(
<g/>
root	root	k2eAgInSc4d1
<g/>
)	)	kIx)
element	element	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Neprázdné	prázdný	k2eNgInPc1d1
elementy	element	k1gInPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
ohraničeny	ohraničit	k5eAaPmNgFnP
startovací	startovací	k2eAgFnSc7d1
a	a	k8xC
ukončovací	ukončovací	k2eAgFnSc7d1
značkou	značka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prázdné	prázdný	k2eAgInPc1d1
elementy	element	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
označeny	označit	k5eAaPmNgInP
tagem	tag	k1gInSc7
„	„	k?
<g/>
prázdný	prázdný	k2eAgInSc1d1
element	element	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgFnPc1
hodnoty	hodnota	k1gFnPc1
atributů	atribut	k1gInPc2
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
uzavřeny	uzavřít	k5eAaPmNgInP
v	v	k7c6
uvozovkách	uvozovka	k1gFnPc6
–	–	k?
jednoduchých	jednoduchý	k2eAgFnPc2d1
(	(	kIx(
<g/>
'	'	kIx"
<g/>
)	)	kIx)
nebo	nebo	k8xC
dvojitých	dvojitý	k2eAgMnPc2d1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
jednoduchá	jednoduchý	k2eAgFnSc1d1
uvozovka	uvozovka	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
uzavřena	uzavřít	k5eAaPmNgFnS
jednoduchou	jednoduchý	k2eAgFnSc7d1
a	a	k8xC
dvojitá	dvojitý	k2eAgFnSc1d1
dvojitou	dvojitý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opačný	opačný	k2eAgInSc4d1
pár	pár	k4xCyI
uvozovek	uvozovka	k1gFnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
použit	použít	k5eAaPmNgInS
uvnitř	uvnitř	k7c2
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Elementy	element	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vnořeny	vnořen	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nemohou	moct	k5eNaImIp3nP
se	se	k3xPyFc4
překrývat	překrývat	k5eAaImF
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
(	(	kIx(
<g/>
ne	ne	k9
kořenový	kořenový	k2eAgInSc4d1
<g/>
)	)	kIx)
element	element	k1gInSc4
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
celý	celý	k2eAgInSc4d1
obsažen	obsažen	k2eAgInSc4d1
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
elementu	element	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Jména	jméno	k1gNnPc1
elementů	element	k1gInPc2
v	v	k7c6
XML	XML	kA
rozlišují	rozlišovat	k5eAaImIp3nP
malá	malý	k2eAgNnPc1d1
a	a	k8xC
velká	velký	k2eAgNnPc1d1
písmena	písmeno	k1gNnPc1
<g/>
:	:	kIx,
např.	např.	kA
„	„	k?
<g/>
<Příklad>
“	“	k?
a	a	k8xC
„	„	k?
</Příklad>
<g/>
“	“	k?
je	být	k5eAaImIp3nS
pár	pár	k4xCyI
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vyhovuje	vyhovovat	k5eAaImIp3nS
správně	správně	k6eAd1
strukturovanému	strukturovaný	k2eAgInSc3d1
dokumentu	dokument	k1gInSc3
<g/>
,	,	kIx,
pár	pár	k4xCyI
„	„	k?
<g/>
<Příklad>
“	“	k?
a	a	k8xC
„	„	k?
</příklad>
<g/>
“	“	k?
je	být	k5eAaImIp3nS
chybný	chybný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Jednoduchý	jednoduchý	k2eAgInSc1d1
recept	recept	k1gInSc1
v	v	k7c6
XML	XML	kA
jako	jako	k8xC,k8xS
příklad	příklad	k1gInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
vypadat	vypadat	k5eAaPmF,k5eAaImF
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
<	<	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
xml	xml	k?
version	version	k1gInSc1
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
1.0	1.0	k4
<g/>
"	"	kIx"
encoding	encoding	k1gInSc1
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
?	?	kIx.
</s>
<s desamb="1">
<g/>
>	>	kIx)
</s>
<s>
<titulek>
Jednoduchý	jednoduchý	k2eAgMnSc1d1
chleba	chléb	k1gInSc2
</titulek>
</s>
<s>
<přísada množství="3" jednotka="šálky">
Mouka	mouka	k1gFnSc1
</přísada>
</s>
<s>
<přísada množství="0,25" jednotka="unce">
Kvasnice	kvasnice	k1gFnPc4
</přísada>
</s>
<s>
<přísada množství="1,5" jednotka="šálku">
Horká	horký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
</přísada>
</s>
<s>
<přísada množství="1" jednotka="kávová lžička">
Sůl	sůl	k1gFnSc1
</přísada>
</s>
<s>
<krok>
Smíchejte	smíchat	k5eAaPmRp2nP
všechny	všechen	k3xTgFnPc4
přísady	přísada	k1gFnPc1
dohromady	dohromady	k6eAd1
a	a	k8xC
dobře	dobře	k6eAd1
prohněťte	prohníst	k5eAaPmRp2nP
<g/>
.	.	kIx.
</krok>
</s>
<s>
<krok>
Zakryjte	zakrýt	k5eAaPmRp2nP
tkaninou	tkanina	k1gFnSc7
a	a	k8xC
nechejte	nechat	k5eAaPmRp2nP
hodinu	hodina	k1gFnSc4
v	v	k7c6
teplé	teplý	k2eAgFnSc6d1
místnosti	místnost	k1gFnSc6
<g/>
.	.	kIx.
</krok>
</s>
<s>
<krok>
Znovu	znovu	k6eAd1
prohněťte	prohníst	k5eAaPmRp2nP
<g/>
,	,	kIx,
umístěte	umístit	k5eAaPmRp2nP
na	na	k7c4
plech	plech	k1gInSc4
a	a	k8xC
pečte	péct	k5eAaImRp2nP
v	v	k7c6
troubě	trouba	k1gFnSc6
<g/>
.	.	kIx.
</krok>
</s>
<s>
Zpracování	zpracování	k1gNnSc1
XML	XML	kA
</s>
<s>
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
nejčastější	častý	k2eAgInPc1d3
přístupy	přístup	k1gInPc1
ke	k	k7c3
zpracování	zpracování	k1gNnSc3
XML	XML	kA
dokumentu	dokument	k1gInSc3
<g/>
:	:	kIx,
</s>
<s>
DOM	DOM	k?
parser	parser	k1gInSc1
(	(	kIx(
<g/>
DOM	DOM	k?
=	=	kIx~
Document	Document	k1gMnSc1
Object	Object	k1gMnSc1
Model	model	k1gInSc1
<g/>
)	)	kIx)
vezme	vzít	k5eAaPmIp3nS
XML	XML	kA
dokument	dokument	k1gInSc1
a	a	k8xC
vyrobí	vyrobit	k5eAaPmIp3nS
z	z	k7c2
něho	on	k3xPp3gMnSc2
obraz	obraz	k1gInSc1
(	(	kIx(
<g/>
strom	strom	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
paměti	paměť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
SAX	sax	k1gInSc1
parser	parser	k1gInSc1
(	(	kIx(
<g/>
SAX	sax	k1gInSc1
=	=	kIx~
Simple	Simple	k1gFnSc2
API	API	kA
for	forum	k1gNnPc2
XML	XML	kA
<g/>
)	)	kIx)
postupně	postupně	k6eAd1
prochází	procházet	k5eAaImIp3nS
XML	XML	kA
dokument	dokument	k1gInSc1
a	a	k8xC
vyvolává	vyvolávat	k5eAaImIp3nS
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
na	na	k7c6
programátorovi	programátor	k1gMnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tyto	tento	k3xDgFnPc4
události	událost	k1gFnPc4
zpracoval	zpracovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Aplikace	aplikace	k1gFnSc1
XML	XML	kA
</s>
<s>
Příklady	příklad	k1gInPc1
aplikace	aplikace	k1gFnSc2
XML	XML	kA
<g/>
:	:	kIx,
</s>
<s>
XHTML	XHTML	kA
–	–	k?
XML	XML	kA
alternativa	alternativa	k1gFnSc1
jazyka	jazyk	k1gInSc2
HTML	HTML	kA
<g/>
.	.	kIx.
</s>
<s>
RDF	RDF	kA
–	–	k?
Resource	Resourka	k1gFnSc3
Description	Description	k1gInSc4
Framework	Framework	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
popsat	popsat	k5eAaPmF
metadata	metadat	k1gMnSc4
a	a	k8xC
zaznamenat	zaznamenat	k5eAaPmF
ontologie	ontologie	k1gFnPc4
<g/>
,	,	kIx,
např.	např.	kA
popsat	popsat	k5eAaPmF
obsah	obsah	k1gInSc4
HTML	HTML	kA
stránky	stránka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
RSS	RSS	kA
–	–	k?
Rodina	rodina	k1gFnSc1
XML	XML	kA
formátů	formát	k1gInPc2
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
pro	pro	k7c4
čtení	čtení	k1gNnSc4
novinek	novinka	k1gFnPc2
na	na	k7c6
webových	webový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
SMIL	smil	k1gInSc1
–	–	k?
Synchronized	Synchronized	k1gInSc1
Multimedia	multimedium	k1gNnSc2
Integration	Integration	k1gInSc1
Language	language	k1gFnSc4
<g/>
,	,	kIx,
popisuje	popisovat	k5eAaImIp3nS
multimedia	multimedium	k1gNnPc4
pomocí	pomocí	k7c2
XML	XML	kA
<g/>
.	.	kIx.
</s>
<s>
MathML	MathML	k?
–	–	k?
Mathematical	Mathematical	k1gFnSc3
Markup	Markup	k1gInSc4
Language	language	k1gFnSc2
je	být	k5eAaImIp3nS
značkovací	značkovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
pro	pro	k7c4
popis	popis	k1gInSc4
matematických	matematický	k2eAgInPc2d1
vzorců	vzorec	k1gInPc2
a	a	k8xC
symbolů	symbol	k1gInPc2
pro	pro	k7c4
použití	použití	k1gNnSc4
na	na	k7c6
webu	web	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
SVG	SVG	kA
–	–	k?
Scalable	Scalable	k1gMnSc1
Vector	Vector	k1gMnSc1
Graphics	Graphics	k1gInSc4
je	být	k5eAaImIp3nS
jazyk	jazyk	k1gInSc1
pro	pro	k7c4
popis	popis	k1gInSc4
dvourozměrné	dvourozměrný	k2eAgFnSc2d1
vektorové	vektorový	k2eAgFnSc2d1
grafiky	grafika	k1gFnSc2
<g/>
,	,	kIx,
statické	statický	k2eAgFnSc2d1
i	i	k8xC
dynamické	dynamický	k2eAgFnSc2d1
(	(	kIx(
<g/>
animace	animace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
DocBook	DocBook	k1gInSc1
–	–	k?
Sada	sada	k1gFnSc1
definic	definice	k1gFnPc2
dokumentů	dokument	k1gInPc2
a	a	k8xC
stylů	styl	k1gInPc2
pro	pro	k7c4
publikační	publikační	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
MusicXML	MusicXML	k?
–	–	k?
XML	XML	kA
soubor	soubor	k1gInSc1
pro	pro	k7c4
notový	notový	k2eAgInSc4d1
zápis	zápis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jabber	Jabber	k1gInSc1
–	–	k?
Protokol	protokol	k1gInSc1
pro	pro	k7c4
Instant	Instant	k1gInSc4
messaging	messaging	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
SOAP	SOAP	kA
–	–	k?
Protokol	protokol	k1gInSc1
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
mezi	mezi	k7c7
webovými	webový	k2eAgFnPc7d1
službami	služba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Office	Office	kA
Open	Openo	k1gNnPc2
XML	XML	kA
<g/>
,	,	kIx,
OpenPocument	OpenPocument	k1gInSc1
–	–	k?
Souborové	souborový	k2eAgInPc1d1
formáty	formát	k1gInPc1
určené	určený	k2eAgInPc1d1
k	k	k7c3
ukládání	ukládání	k1gNnSc3
a	a	k8xC
výměně	výměna	k1gFnSc3
dokumentů	dokument	k1gInPc2
vytvořených	vytvořený	k2eAgInPc2d1
kancelářskými	kancelářský	k2eAgFnPc7d1
aplikacemi	aplikace	k1gFnPc7
(	(	kIx(
<g/>
formát	formát	k1gInSc1
DOCX	DOCX	kA
balíku	balík	k1gInSc2
MS	MS	kA
Office	Office	kA
(	(	kIx(
<g/>
2007	#num#	k4
a	a	k8xC
novější	nový	k2eAgMnSc1d2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
formát	formát	k1gInSc4
ODT	ODT	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
interně	interně	k6eAd1
ZIP	zip	k1gInSc4
archivy	archiv	k1gInPc7
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yQgFnPc6,k3yIgFnPc6,k3yRgFnPc6
jsou	být	k5eAaImIp3nP
ve	v	k7c6
formátu	formát	k1gInSc6
XML	XML	kA
uloženy	uložit	k5eAaPmNgFnP
jednotlivé	jednotlivý	k2eAgFnPc1d1
části	část	k1gFnPc1
daného	daný	k2eAgInSc2d1
dokumentu	dokument	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Mapy	mapa	k1gFnPc1
Google	Google	k1gNnSc2
+	+	kIx~
Google	Google	k1gFnSc1
Earth	Eartha	k1gFnPc2
–	–	k?
uživatelé	uživatel	k1gMnPc1
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
mapové	mapový	k2eAgInPc4d1
projekty	projekt	k1gInPc4
ukládat	ukládat	k5eAaImF
ve	v	k7c6
formě	forma	k1gFnSc6
KML	KML	kA
(	(	kIx(
<g/>
čistá	čistý	k2eAgFnSc1d1
XML	XML	kA
<g/>
)	)	kIx)
a	a	k8xC
KMZ	KMZ	kA
(	(	kIx(
<g/>
komprimovaná	komprimovaný	k2eAgFnSc1d1
XML	XML	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
(	(	kIx(
<g/>
například	například	k6eAd1
Daňový	daňový	k2eAgInSc1d1
portál	portál	k1gInSc1
finanční	finanční	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
správa	správa	k1gFnSc1
sociálního	sociální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgFnSc2d1
zakázky	zakázka	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
rámci	rámec	k1gInSc6
komunikace	komunikace	k1gFnSc2
s	s	k7c7
firmami	firma	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
XML	XML	kA
používá	používat	k5eAaImIp3nS
Ústav	ústav	k1gInSc1
zdravotnických	zdravotnický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
a	a	k8xC
statistiky	statistika	k1gFnSc2
ČR	ČR	kA
pro	pro	k7c4
statistická	statistický	k2eAgNnPc4d1
hlášení	hlášení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgNnSc4d1
je	být	k5eAaImIp3nS
využití	využití	k1gNnSc4
XML	XML	kA
pro	pro	k7c4
služby	služba	k1gFnPc4
v	v	k7c6
online	onlinout	k5eAaPmIp3nS
komunikaci	komunikace	k1gFnSc4
prostřednictvím	prostřednictvím	k7c2
datové	datový	k2eAgFnSc2d1
schránky	schránka	k1gFnSc2
s	s	k7c7
některými	některý	k3yIgFnPc7
databázemi	databáze	k1gFnPc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
na	na	k7c6
vládním	vládní	k2eAgInSc6d1
portálu	portál	k1gInSc6
GOV	GOV	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
výpis	výpis	k1gInSc1
bodů	bod	k1gInPc2
řidičů	řidič	k1gMnPc2
<g/>
,	,	kIx,
výpis	výpis	k1gInSc4
z	z	k7c2
trestního	trestní	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
portálu	portál	k1gInSc6
České	český	k2eAgFnSc2d1
správy	správa	k1gFnSc2
sociálního	sociální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
nemocenská	nemocenská	k1gFnSc1
<g/>
,	,	kIx,
doby	doba	k1gFnPc1
na	na	k7c4
důchod	důchod	k1gInSc4
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výpisy	výpis	k1gInPc1
z	z	k7c2
bank	banka	k1gFnPc2
ve	v	k7c6
formátu	formát	k1gInSc6
XML	XML	kA
(	(	kIx(
<g/>
například	například	k6eAd1
GE	GE	kA
Money	Money	k1gInPc1
Bank	bank	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Verze	verze	k1gFnSc1
XML	XML	kA
</s>
<s>
Aktuální	aktuální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
XML	XML	kA
je	být	k5eAaImIp3nS
1.1	1.1	k4
(	(	kIx(
<g/>
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
verze	verze	k1gFnSc1
XML	XML	kA
1.0	1.0	k4
existuje	existovat	k5eAaImIp3nS
v	v	k7c6
páté	pátý	k4xOgFnSc6
revizi	revize	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
verze	verze	k1gFnPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
v	v	k7c6
požadavcích	požadavek	k1gInPc6
na	na	k7c4
použité	použitý	k2eAgInPc4d1
znaky	znak	k1gInPc4
v	v	k7c6
názvech	název	k1gInPc6
elementů	element	k1gInPc2
<g/>
,	,	kIx,
atributů	atribut	k1gInPc2
atd.	atd.	kA
Verze	verze	k1gFnSc1
1.0	1.0	k4
dovolovala	dovolovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
užívání	užívání	k1gNnSc4
znaků	znak	k1gInPc2
platných	platný	k2eAgInPc2d1
ve	v	k7c6
verzi	verze	k1gFnSc6
Unicode	Unicod	k1gInSc5
2.0	2.0	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
většinu	většina	k1gFnSc4
světových	světový	k2eAgNnPc2d1
písem	písmo	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
neobsahuje	obsahovat	k5eNaImIp3nS
později	pozdě	k6eAd2
přidané	přidaný	k2eAgFnSc2d1
sady	sada	k1gFnSc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
mongolština	mongolština	k1gFnSc1
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
XML	XML	kA
1.1	1.1	k4
zakazuje	zakazovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
řídící	řídící	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
jakékoli	jakýkoli	k3yIgInPc1
jiné	jiný	k2eAgInPc1d1
znaky	znak	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
poznamenat	poznamenat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
omezení	omezení	k1gNnSc1
ve	v	k7c6
verzi	verze	k1gFnSc6
1.0	1.0	k4
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c4
názvy	název	k1gInPc4
elementů	element	k1gInPc2
a	a	k8xC
atributů	atribut	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
obě	dva	k4xCgFnPc1
verze	verze	k1gFnPc1
dovolují	dovolovat	k5eAaImIp3nP
v	v	k7c6
obsahu	obsah	k1gInSc6
dokumentu	dokument	k1gInSc2
jakékoli	jakýkoli	k3yIgInPc4
znaky	znak	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
1.1	1.1	k4
je	být	k5eAaImIp3nS
tedy	tedy	k9
nutná	nutný	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
potřebujeme	potřebovat	k5eAaImIp1nP
psát	psát	k5eAaImF
názvy	název	k1gInPc1
elementů	element	k1gInPc2
v	v	k7c6
jazyku	jazyk	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
přidán	přidat	k5eAaPmNgInS
do	do	k7c2
Unicode	Unicod	k1gInSc5
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc1d1
menší	malý	k2eAgFnSc1d2
změna	změna	k1gFnSc1
mezi	mezi	k7c7
XML	XML	kA
1.0	1.0	k4
a	a	k8xC
1.1	1.1	k4
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
řídící	řídící	k2eAgInPc1d1
znaky	znak	k1gInPc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
mohou	moct	k5eAaImIp3nP
vkládat	vkládat	k5eAaImF
jen	jen	k9
jako	jako	k9
escape	escapat	k5eAaPmIp3nS
sekvence	sekvence	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
se	s	k7c7
speciálními	speciální	k2eAgInPc7d1
znaky	znak	k1gInPc7
„	„	k?
<g/>
form-feed	form-feed	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
zacházet	zacházet	k5eAaImF
jako	jako	k9
s	s	k7c7
bílými	bílý	k2eAgInPc7d1
znaky	znak	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgInPc1
dokumenty	dokument	k1gInPc1
verze	verze	k1gFnSc2
1.0	1.0	k4
budou	být	k5eAaImBp3nP
platné	platný	k2eAgInPc1d1
ve	v	k7c6
verzi	verze	k1gFnSc6
1.1	1.1	k4
s	s	k7c7
jednou	jeden	k4xCgFnSc7
výjimkou	výjimka	k1gFnSc7
<g/>
:	:	kIx,
dokumenty	dokument	k1gInPc4
deklarované	deklarovaný	k2eAgInPc4d1
s	s	k7c7
kódováním	kódování	k1gNnSc7
ISO-	ISO-	k1gFnSc1
<g/>
8859	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
v	v	k7c6
kódu	kód	k1gInSc6
Windows	Windows	kA
CP	CP	kA
<g/>
1252	#num#	k4
<g/>
,	,	kIx,
nemusejí	muset	k5eNaImIp3nP
být	být	k5eAaImF
nyní	nyní	k6eAd1
platné	platný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
CP1252	CP1252	k1gMnPc1
používají	používat	k5eAaImIp3nP
blok	blok	k1gInSc4
řídících	řídící	k2eAgInPc2d1
znaků	znak	k1gInPc2
pro	pro	k7c4
speciální	speciální	k2eAgNnSc4d1
zobrazení	zobrazení	k1gNnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
znaky	znak	k1gInPc1
€	€	k?
<g/>
,	,	kIx,
Œ	Œ	k?
<g/>
,	,	kIx,
a	a	k8xC
™	™	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokumenty	dokument	k1gInPc4
s	s	k7c7
deklarací	deklarace	k1gFnSc7
CP1252	CP1252	k1gFnPc2
zůstávají	zůstávat	k5eAaImIp3nP
platné	platný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
</s>
<s>
Jmenné	jmenný	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
v	v	k7c6
XML	XML	kA
–	–	k?
Umožňují	umožňovat	k5eAaImIp3nP
kombinovat	kombinovat	k5eAaImF
značkování	značkování	k1gNnSc4
podle	podle	k7c2
různých	různý	k2eAgInPc2d1
standardů	standard	k1gInPc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
dokumentu	dokument	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
XML	XML	kA
Schema	schema	k1gNnSc1
<g/>
,	,	kIx,
RELAX	RELAX	kA
NG	NG	kA
<g/>
,	,	kIx,
Schematron	Schematron	k1gInSc1
–	–	k?
Předpis	předpis	k1gInSc1
struktury	struktura	k1gFnSc2
a	a	k8xC
datových	datový	k2eAgInPc2d1
typů	typ	k1gInPc2
pro	pro	k7c4
třídu	třída	k1gFnSc4
dokumentů	dokument	k1gInPc2
v	v	k7c6
XML	XML	kA
<g/>
.	.	kIx.
</s>
<s>
XSL	XSL	kA
–	–	k?
Transformace	transformace	k1gFnSc1
dokumentu	dokument	k1gInSc2
v	v	k7c6
XML	XML	kA
na	na	k7c4
jiný	jiný	k2eAgInSc4d1
<g/>
,	,	kIx,
odvozený	odvozený	k2eAgInSc4d1
dokument	dokument	k1gInSc4
<g/>
,	,	kIx,
např.	např.	kA
XML	XML	kA
<g/>
,	,	kIx,
HTML	HTML	kA
nebo	nebo	k8xC
textový	textový	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
XSLT	XSLT	kA
<g/>
,	,	kIx,
XSL-FO	XSL-FO	k1gFnPc2
a	a	k8xC
XPath	XPatha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
XQuery	XQuera	k1gFnPc1
–	–	k?
Dotazy	dotaz	k1gInPc1
nad	nad	k7c7
daty	datum	k1gNnPc7
v	v	k7c6
XML	XML	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
XML	XML	kA
Media	medium	k1gNnPc1
Types	Types	k1gMnSc1
<g/>
,	,	kIx,
RFC	RFC	kA
7303	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
Engineering	Engineering	k1gInSc4
Task	Task	k1gInSc1
Force	force	k1gFnSc1
<g/>
,	,	kIx,
červenec	červenec	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Charles	Charles	k1gMnSc1
F.	F.	kA
Goldfarb	Goldfarb	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Roots	Roots	k1gInSc1
of	of	k?
SGML	SGML	kA
–	–	k?
A	a	k9
Personal	Personal	k1gMnSc1
Recollection	Recollection	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4501553-3	4501553-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12494	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
97007825	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
97007825	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Extensible	Extensible	k1gFnSc2
Markup	Markup	k1gInSc1
Language	language	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Extensible	Extensible	k6eAd1
Markup	Markup	k1gInSc1
Language	language	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
ZVON	zvon	k1gInSc1
–	–	k?
Vše	všechen	k3xTgNnSc1
o	o	k7c6
XML	XML	kA
a	a	k8xC
jak	jak	k6eAd1
tvořit	tvořit	k5eAaImF
XML	XML	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
www.w3.org/XML	www.w3.org/XML	k4
–	–	k?
Specifikace	specifikace	k1gFnSc2
<g/>
,	,	kIx,
pracovní	pracovní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
W3C	W3C	k1gFnSc2
atd.	atd.	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Slabikář	slabikář	k1gInSc1
XML	XML	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Seriál	seriál	k1gInSc1
o	o	k7c6
XML	XML	kA
pro	pro	k7c4
Softwarové	softwarový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
XML	XML	kA
editors	editors	k6eAd1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
XML-DEV	XML-DEV	k1gFnSc1
Mailing	Mailing	k1gInSc1
List	list	k1gInSc1
</s>
