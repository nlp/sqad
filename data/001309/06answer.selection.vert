<s>
Germanium	germanium	k1gNnSc1	germanium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ge	Ge	k1gFnSc2	Ge
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Germanium	germanium	k1gNnSc4	germanium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc4d1	vzácný
šedobílý	šedobílý	k2eAgInSc4d1	šedobílý
polokovový	polokovový	k2eAgInSc4d1	polokovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
nalézající	nalézající	k2eAgNnSc4d1	nalézající
největší	veliký	k2eAgNnSc4d3	veliký
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
polovodičovém	polovodičový	k2eAgInSc6d1	polovodičový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
