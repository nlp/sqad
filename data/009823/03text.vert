<p>
<s>
Komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
je	být	k5eAaImIp3nS	být
totalitní	totalitní	k2eAgInSc1d1	totalitní
politický	politický	k2eAgInSc1d1	politický
režim	režim	k1gInSc1	režim
států	stát	k1gInPc2	stát
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
stát	stát	k5eAaImF	stát
vybudovat	vybudovat	k5eAaPmF	vybudovat
na	na	k7c6	na
ideologických	ideologický	k2eAgInPc6d1	ideologický
základech	základ	k1gInPc6	základ
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Reálně	reálně	k6eAd1	reálně
však	však	k9	však
nejde	jít	k5eNaImIp3nS	jít
těchto	tento	k3xDgFnPc2	tento
idejí	idea	k1gFnPc2	idea
zcela	zcela	k6eAd1	zcela
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
a	a	k8xC	a
tyto	tento	k3xDgFnPc4	tento
ideje	idea	k1gFnPc1	idea
se	se	k3xPyFc4	se
promítnou	promítnout	k5eAaPmIp3nP	promítnout
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
filosofie	filosofie	k1gFnSc2	filosofie
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nazýván	nazývat	k5eAaImNgInS	nazývat
jako	jako	k8xC	jako
reálný	reálný	k2eAgInSc1d1	reálný
socialismus	socialismus	k1gInSc1	socialismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
krvavým	krvavý	k2eAgInSc7d1	krvavý
převratem	převrat	k1gInSc7	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
bolševiků	bolševik	k1gMnPc2	bolševik
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Leninem	Lenin	k1gMnSc7	Lenin
a	a	k8xC	a
Trockým	Trocký	k1gMnSc7	Trocký
<g/>
.	.	kIx.	.
</s>
<s>
Komunistické	komunistický	k2eAgFnPc1d1	komunistická
strany	strana	k1gFnPc1	strana
dodnes	dodnes	k6eAd1	dodnes
vládnou	vládnout	k5eAaImIp3nP	vládnout
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc6d1	další
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
názorové	názorový	k2eAgInPc1d1	názorový
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
trockisté	trockista	k1gMnPc1	trockista
<g/>
,	,	kIx,	,
tvrdí	tvrdý	k2eAgMnPc1d1	tvrdý
<g/>
,	,	kIx,	,
že	že	k8xS	že
režimy	režim	k1gInPc1	režim
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
od	od	k7c2	od
Stalina	Stalin	k1gMnSc2	Stalin
do	do	k7c2	do
Gorbačova	Gorbačův	k2eAgNnSc2d1	Gorbačův
a	a	k8xC	a
režimy	režim	k1gInPc4	režim
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
včetně	včetně	k7c2	včetně
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
Číny	Čína	k1gFnPc1	Čína
neměly	mít	k5eNaImAgFnP	mít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
komunismu	komunismus	k1gInSc2	komunismus
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
,	,	kIx,	,
a	a	k8xC	a
nazývají	nazývat	k5eAaImIp3nP	nazývat
je	on	k3xPp3gFnPc4	on
proto	proto	k8xC	proto
režimy	režim	k1gInPc7	režim
stalinistickými	stalinistický	k2eAgInPc7d1	stalinistický
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
s	s	k7c7	s
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
se	se	k3xPyFc4	se
samy	sám	k3xTgFnPc1	sám
neoznačovaly	označovat	k5eNaImAgFnP	označovat
jako	jako	k9	jako
komunistické	komunistický	k2eAgFnPc1d1	komunistická
–	–	k?	–
nazývaly	nazývat	k5eAaImAgFnP	nazývat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
jako	jako	k9	jako
lidově	lidově	k6eAd1	lidově
demokratické	demokratický	k2eAgFnSc2d1	demokratická
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Československo	Československo	k1gNnSc1	Československo
v	v	k7c6	v
letech	let	k1gInPc6	let
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgInPc1d1	lidový
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
socialistické	socialistický	k2eAgFnSc2d1	socialistická
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Československo	Československo	k1gNnSc1	Československo
v	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komunisty	komunista	k1gMnPc4	komunista
ovládané	ovládaný	k2eAgInPc1d1	ovládaný
státy	stát	k1gInPc1	stát
vždy	vždy	k6eAd1	vždy
propadaly	propadat	k5eAaPmAgInP	propadat
politické	politický	k2eAgFnPc4d1	politická
despocii	despocie	k1gFnSc4	despocie
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
stagnaci	stagnace	k1gFnSc3	stagnace
a	a	k8xC	a
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
zaostávání	zaostávání	k1gNnSc3	zaostávání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
prosazení	prosazení	k1gNnSc4	prosazení
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
používají	používat	k5eAaImIp3nP	používat
teroristické	teroristický	k2eAgFnPc4d1	teroristická
metody	metoda	k1gFnPc4	metoda
a	a	k8xC	a
podle	podle	k7c2	podle
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
odhadů	odhad	k1gInPc2	odhad
se	se	k3xPyFc4	se
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
počet	počet	k1gInSc1	počet
jejich	jejich	k3xOp3gFnPc2	jejich
obětí	oběť	k1gFnPc2	oběť
může	moct	k5eAaImIp3nS	moct
blížit	blížit	k5eAaImF	blížit
až	až	k9	až
ke	k	k7c3	k
100	[number]	k4	100
milionům	milion	k4xCgInPc3	milion
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
období	období	k1gNnSc6	období
1948	[number]	k4	1948
až	až	k6eAd1	až
1989	[number]	k4	1989
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
ovládala	ovládat	k5eAaImAgFnS	ovládat
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
Československo	Československo	k1gNnSc4	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
198	[number]	k4	198
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1993	[number]	k4	1993
O	o	k7c6	o
protiprávnosti	protiprávnost	k1gFnSc6	protiprávnost
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
o	o	k7c6	o
odporu	odpor	k1gInSc6	odpor
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
označen	označen	k2eAgInSc1d1	označen
za	za	k7c4	za
protiprávní	protiprávní	k2eAgInSc4d1	protiprávní
a	a	k8xC	a
zavrženíhodný	zavrženíhodný	k2eAgInSc4d1	zavrženíhodný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
filozofové	filozof	k1gMnPc1	filozof
Karl	Karl	k1gMnSc1	Karl
Marx	Marx	k1gMnSc1	Marx
a	a	k8xC	a
Friedrich	Friedrich	k1gMnSc1	Friedrich
Engels	Engels	k1gMnSc1	Engels
rozpracovali	rozpracovat	k5eAaPmAgMnP	rozpracovat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
materialistické	materialistický	k2eAgNnSc4d1	materialistické
pojetí	pojetí	k1gNnSc4	pojetí
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
stav	stav	k1gInSc4	stav
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
společnosti	společnost	k1gFnSc6	společnost
nástrojem	nástroj	k1gInSc7	nástroj
útlaku	útlak	k1gInSc2	útlak
jedné	jeden	k4xCgFnSc2	jeden
společenské	společenský	k2eAgFnSc2d1	společenská
třídy	třída	k1gFnSc2	třída
nad	nad	k7c7	nad
druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
bude	být	k5eAaImBp3nS	být
odstraněn	odstranit	k5eAaPmNgInS	odstranit
nastolením	nastolení	k1gNnSc7	nastolení
beztřídní	beztřídní	k2eAgFnSc2d1	beztřídní
<g/>
,	,	kIx,	,
komunistické	komunistický	k2eAgFnSc2d1	komunistická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
státem	stát	k1gInSc7	stát
vedeným	vedený	k2eAgInSc7d1	vedený
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
převratem	převrat	k1gInSc7	převrat
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
marxistické	marxistický	k2eAgNnSc1d1	marxistické
učení	učení	k1gNnSc1	učení
diktatury	diktatura	k1gFnSc2	diktatura
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
ideologem	ideolog	k1gMnSc7	ideolog
a	a	k8xC	a
vůdcem	vůdce	k1gMnSc7	vůdce
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgMnS	být
Vladimir	Vladimir	k1gMnSc1	Vladimir
Iljič	Iljič	k1gMnSc1	Iljič
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
navrátil	navrátit	k5eAaPmAgMnS	navrátit
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
očekávali	očekávat	k5eAaImAgMnP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lenin	Lenin	k1gMnSc1	Lenin
svojí	svůj	k3xOyFgFnSc7	svůj
revolucí	revoluce	k1gFnSc7	revoluce
paralyzuje	paralyzovat	k5eAaBmIp3nS	paralyzovat
bojeschopnost	bojeschopnost	k1gFnSc1	bojeschopnost
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
ukončí	ukončit	k5eAaPmIp3nS	ukončit
válku	válka	k1gFnSc4	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
transport	transport	k1gInSc1	transport
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
právo	právo	k1gNnSc4	právo
exteritoriality	exteritorialita	k1gFnPc4	exteritorialita
<g/>
,	,	kIx,	,
střežili	střežit	k5eAaImAgMnP	střežit
ho	on	k3xPp3gNnSc4	on
němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
revolucionáři	revolucionář	k1gMnPc1	revolucionář
pobírali	pobírat	k5eAaImAgMnP	pobírat
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
od	od	k7c2	od
Německého	německý	k2eAgNnSc2d1	německé
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zbavil	zbavit	k5eAaPmAgMnS	zbavit
opozice	opozice	k1gFnSc2	opozice
a	a	k8xC	a
omezil	omezit	k5eAaPmAgInS	omezit
soukromé	soukromý	k2eAgNnSc4d1	soukromé
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gMnSc4	on
Novou	nový	k2eAgFnSc7d1	nová
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
politikou	politika	k1gFnSc7	politika
(	(	kIx(	(
<g/>
NEP	Nep	k1gInSc1	Nep
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
uvolněné	uvolněný	k2eAgFnPc1d1	uvolněná
formy	forma	k1gFnPc1	forma
státního	státní	k2eAgInSc2d1	státní
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Stalin	Stalin	k1gMnSc1	Stalin
později	pozdě	k6eAd2	pozdě
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgInS	získat
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
materiální	materiální	k2eAgNnSc4d1	materiální
zajištění	zajištění	k1gNnSc4	zajištění
obranné	obranný	k2eAgFnSc2d1	obranná
války	válka	k1gFnSc2	válka
se	s	k7c7	s
západními	západní	k2eAgMnPc7d1	západní
sousedy	soused	k1gMnPc7	soused
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
bolševiky	bolševik	k1gMnPc7	bolševik
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vzniká	vznikat	k5eAaImIp3nS	vznikat
samostatná	samostatný	k2eAgFnSc1d1	samostatná
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
strany	strana	k1gFnPc1	strana
řídily	řídit	k5eAaImAgFnP	řídit
(	(	kIx(	(
<g/>
snad	snad	k9	snad
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
německé	německý	k2eAgFnSc2d1	německá
KS	ks	kA	ks
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
silným	silný	k2eAgNnSc7d1	silné
vedením	vedení	k1gNnSc7	vedení
a	a	k8xC	a
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
KS	ks	kA	ks
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Tita	Tit	k1gInSc2	Tit
<g/>
)	)	kIx)	)
pokyny	pokyn	k1gInPc1	pokyn
a	a	k8xC	a
rozkazy	rozkaz	k1gInPc1	rozkaz
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
řízené	řízený	k2eAgFnSc2d1	řízená
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
internacionály	internacionála	k1gFnSc2	internacionála
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
nebývalé	nebývalý	k2eAgFnSc2d1	nebývalá
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
1946	[number]	k4	1946
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
následoval	následovat	k5eAaImAgInS	následovat
puč	puč	k1gInSc1	puč
a	a	k8xC	a
KSČ	KSČ	kA	KSČ
tak	tak	k6eAd1	tak
postupně	postupně	k6eAd1	postupně
získala	získat	k5eAaPmAgFnS	získat
moc	moc	k6eAd1	moc
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
KS	ks	kA	ks
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
praktikách	praktika	k1gFnPc6	praktika
přiblížily	přiblížit	k5eAaPmAgFnP	přiblížit
KSSS	KSSS	kA	KSSS
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
spřízněny	spřízněn	k2eAgFnPc1d1	spřízněna
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svojit	k5eAaImIp3nS	svojit
vládu	vláda	k1gFnSc4	vláda
vykonávaly	vykonávat	k5eAaImAgInP	vykonávat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zastrašování	zastrašování	k1gNnSc2	zastrašování
a	a	k8xC	a
teroru	teror	k1gInSc2	teror
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
docházelo	docházet	k5eAaImAgNnS	docházet
i	i	k9	i
k	k	k7c3	k
porušování	porušování	k1gNnSc3	porušování
zákonů	zákon	k1gInPc2	zákon
napsaných	napsaný	k2eAgInPc2d1	napsaný
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
socialismu	socialismus	k1gInSc2	socialismus
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
vliv	vliv	k1gInSc1	vliv
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
v	v	k7c6	v
parlamentech	parlament	k1gInPc6	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
reálného	reálný	k2eAgInSc2d1	reálný
socialismu	socialismus	k1gInSc2	socialismus
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
ideji	idea	k1gFnSc6	idea
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
krizí	krize	k1gFnPc2	krize
<g/>
;	;	kIx,	;
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
převychovat	převychovat	k5eAaPmF	převychovat
občany	občan	k1gMnPc4	občan
komunistických	komunistický	k2eAgInPc2d1	komunistický
států	stát	k1gInPc2	stát
podle	podle	k7c2	podle
potřeb	potřeba	k1gFnPc2	potřeba
komunistické	komunistický	k2eAgFnSc2d1	komunistická
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
si	se	k3xPyFc3	se
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
uvědomovala	uvědomovat	k5eAaImAgFnS	uvědomovat
i	i	k9	i
vedení	vedení	k1gNnSc4	vedení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
prosazován	prosazovat	k5eAaImNgInS	prosazovat
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
reálný	reálný	k2eAgInSc1d1	reálný
socialismus	socialismus	k1gInSc1	socialismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
slovníku	slovník	k1gInSc6	slovník
ideologů	ideolog	k1gMnPc2	ideolog
označovat	označovat	k5eAaImF	označovat
jakýsi	jakýsi	k3yIgInSc1	jakýsi
"	"	kIx"	"
<g/>
reálný	reálný	k2eAgInSc1d1	reálný
<g/>
"	"	kIx"	"
předstupeň	předstupeň	k1gInSc1	předstupeň
té	ten	k3xDgFnSc2	ten
pravé	pravý	k2eAgFnSc2d1	pravá
komunistické	komunistický	k2eAgFnSc2d1	komunistická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
umisťována	umisťovat	k5eAaImNgFnS	umisťovat
do	do	k7c2	do
blíže	blízce	k6eAd2	blízce
nespecifikované	specifikovaný	k2eNgFnSc2d1	nespecifikovaná
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
stranami	strana	k1gFnPc7	strana
však	však	k9	však
byla	být	k5eAaImAgFnS	být
ideologie	ideologie	k1gFnSc1	ideologie
pouze	pouze	k6eAd1	pouze
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
udržování	udržování	k1gNnSc4	udržování
nedemokratické	demokratický	k2eNgFnSc2d1	nedemokratická
<g/>
,	,	kIx,	,
totalitní	totalitní	k2eAgFnSc2d1	totalitní
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
plánu	plán	k1gInSc2	plán
"	"	kIx"	"
<g/>
budování	budování	k1gNnSc4	budování
nové	nový	k2eAgFnSc2d1	nová
společnosti	společnost	k1gFnSc2	společnost
<g/>
"	"	kIx"	"
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
konkurenci	konkurence	k1gFnSc6	konkurence
států	stát	k1gInPc2	stát
s	s	k7c7	s
tržní	tržní	k2eAgFnSc7d1	tržní
ekonomikou	ekonomika	k1gFnSc7	ekonomika
tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
neobstály	obstát	k5eNaPmAgInP	obstát
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
navrátily	navrátit	k5eAaPmAgFnP	navrátit
k	k	k7c3	k
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
demokratickým	demokratický	k2eAgFnPc3d1	demokratická
formám	forma	k1gFnPc3	forma
vlády	vláda	k1gFnSc2	vláda
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tak	tak	k6eAd1	tak
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jen	jen	k9	jen
několik	několik	k4yIc1	několik
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
totalitní	totalitní	k2eAgInSc4d1	totalitní
režim	režim	k1gInSc4	režim
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
útlaku	útlak	k1gInSc2	útlak
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
pokračující	pokračující	k2eAgFnSc2d1	pokračující
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
stagnace	stagnace	k1gFnSc2	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
pevninská	pevninský	k2eAgFnSc1d1	pevninská
Čína	Čína	k1gFnSc1	Čína
nebo	nebo	k8xC	nebo
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
země	zem	k1gFnPc1	zem
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
krizím	krize	k1gFnPc3	krize
nuceny	nucen	k2eAgInPc1d1	nucen
systém	systém	k1gInSc4	systém
reformovat	reformovat	k5eAaBmF	reformovat
<g/>
;	;	kIx,	;
v	v	k7c6	v
jisté	jistý	k2eAgFnSc6d1	jistá
míře	míra	k1gFnSc6	míra
povolily	povolit	k5eAaPmAgFnP	povolit
západní	západní	k2eAgFnPc1d1	západní
investice	investice	k1gFnPc1	investice
a	a	k8xC	a
soukromé	soukromý	k2eAgNnSc1d1	soukromé
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
zažívají	zažívat	k5eAaImIp3nP	zažívat
někdy	někdy	k6eAd1	někdy
i	i	k9	i
relativně	relativně	k6eAd1	relativně
velký	velký	k2eAgInSc4d1	velký
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
cenzura	cenzura	k1gFnSc1	cenzura
a	a	k8xC	a
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
například	například	k6eAd1	například
i	i	k9	i
pro	pro	k7c4	pro
názorové	názorový	k2eAgMnPc4d1	názorový
oponenty	oponent	k1gMnPc4	oponent
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
běžně	běžně	k6eAd1	běžně
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
negativa	negativum	k1gNnSc2	negativum
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
komunistickými	komunistický	k2eAgInPc7d1	komunistický
režimy	režim	k1gInPc7	režim
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
porušování	porušování	k1gNnSc3	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
omezování	omezování	k1gNnSc1	omezování
svobody	svoboda	k1gFnSc2	svoboda
projevu	projev	k1gInSc2	projev
a	a	k8xC	a
shromažďování	shromažďování	k1gNnSc2	shromažďování
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
svobodným	svobodný	k2eAgInSc7d1	svobodný
trhem	trh	k1gInSc7	trh
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
s	s	k7c7	s
ekonomikou	ekonomika	k1gFnSc7	ekonomika
neoliberalismu	neoliberalismus	k1gInSc2	neoliberalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Vztah	vztah	k1gInSc1	vztah
komunismu	komunismus	k1gInSc2	komunismus
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
již	již	k9	již
v	v	k7c6	v
sovětských	sovětský	k2eAgFnPc6d1	sovětská
zemích	zem	k1gFnPc6	zem
či	či	k8xC	či
lidově	lidově	k6eAd1	lidově
demokratických	demokratický	k2eAgFnPc2d1	demokratická
<g/>
)	)	kIx)	)
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
je	být	k5eAaImIp3nS	být
vyloženě	vyloženě	k6eAd1	vyloženě
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
<g/>
;	;	kIx,	;
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
celé	celý	k2eAgFnSc2d1	celá
ideologie	ideologie	k1gFnSc2	ideologie
je	být	k5eAaImIp3nS	být
opiem	opium	k1gNnSc7	opium
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dává	dávat	k5eAaImIp3nS	dávat
lidem	lid	k1gInSc7	lid
falešné	falešný	k2eAgNnSc4d1	falešné
uspokojení	uspokojení	k1gNnSc4	uspokojení
ze	z	k7c2	z
zjednodušeného	zjednodušený	k2eAgInSc2d1	zjednodušený
výkladu	výklad	k1gInSc2	výklad
světa	svět	k1gInSc2	svět
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
z	z	k7c2	z
komunistického	komunistický	k2eAgInSc2d1	komunistický
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mohou	moct	k5eAaImIp3nP	moct
dočasně	dočasně	k6eAd1	dočasně
překonávat	překonávat	k5eAaImF	překonávat
třídní	třídní	k2eAgInPc4d1	třídní
rozpory	rozpor	k1gInPc4	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
náboženství	náboženství	k1gNnSc1	náboženství
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
však	však	k9	však
vedlo	vést	k5eAaImAgNnS	vést
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
plíživému	plíživý	k2eAgNnSc3d1	plíživé
nahrazení	nahrazení	k1gNnSc3	nahrazení
uctíváním	uctívání	k1gNnSc7	uctívání
zasloužilých	zasloužilý	k2eAgMnPc2d1	zasloužilý
vůdců	vůdce	k1gMnPc2	vůdce
komunistických	komunistický	k2eAgFnPc2d1	komunistická
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
během	během	k7c2	během
několika	několik	k4yIc2	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rychle	rychle	k6eAd1	rychle
(	(	kIx(	(
<g/>
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
tohoto	tento	k3xDgInSc2	tento
Marxova	Marxův	k2eAgInSc2d1	Marxův
názoru	názor	k1gInSc2	názor
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
během	během	k7c2	během
Stalinovy	Stalinův	k2eAgFnSc2d1	Stalinova
vlády	vláda	k1gFnSc2	vláda
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
myšlenku	myšlenka	k1gFnSc4	myšlenka
"	"	kIx"	"
<g/>
Čím	co	k3yQnSc7	co
více	hodně	k6eAd2	hodně
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
bohů	bůh	k1gMnPc2	bůh
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
Stalin	Stalin	k1gMnSc1	Stalin
svojí	svůj	k3xOyFgFnSc7	svůj
antikomunistickou	antikomunistický	k2eAgFnSc7d1	antikomunistická
interpretací	interpretace	k1gFnSc7	interpretace
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čím	co	k3yRnSc7	co
méně	málo	k6eAd2	málo
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
svobody	svoboda	k1gFnPc4	svoboda
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
komunistických	komunistický	k2eAgInPc6d1	komunistický
režimech	režim	k1gInPc6	režim
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
věřících	věřící	k1gFnPc2	věřící
a	a	k8xC	a
zejména	zejména	k9	zejména
náboženských	náboženský	k2eAgMnPc2d1	náboženský
představitelů	představitel	k1gMnPc2	představitel
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
prosadila	prosadit	k5eAaPmAgFnS	prosadit
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
postupnou	postupný	k2eAgFnSc4d1	postupná
likvidaci	likvidace	k1gFnSc4	likvidace
<g/>
,	,	kIx,	,
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
podmanit	podmanit	k5eAaPmF	podmanit
si	se	k3xPyFc3	se
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
využít	využít	k5eAaPmF	využít
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
věřících	věřící	k1gMnPc2	věřící
k	k	k7c3	k
propagandistickým	propagandistický	k2eAgInPc3d1	propagandistický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
a	a	k8xC	a
církevní	církevní	k2eAgFnPc1d1	církevní
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
podřídily	podřídit	k5eAaPmAgFnP	podřídit
vedení	vedení	k1gNnSc4	vedení
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
vnějších	vnější	k2eAgInPc2d1	vnější
projevů	projev	k1gInPc2	projev
<g/>
,	,	kIx,	,
dostaly	dostat	k5eAaPmAgFnP	dostat
vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
k	k	k7c3	k
činnosti	činnost	k1gFnSc3	činnost
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
ani	ani	k8xC	ani
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
nepředpokládalo	předpokládat	k5eNaImAgNnS	předpokládat
jejich	jejich	k3xOp3gNnSc1	jejich
zachování	zachování	k1gNnSc1	zachování
po	po	k7c6	po
konečném	konečný	k2eAgNnSc6d1	konečné
vítězství	vítězství	k1gNnSc6	vítězství
nového	nový	k2eAgInSc2d1	nový
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nebyly	být	k5eNaImAgInP	být
ochotny	ochoten	k2eAgInPc1d1	ochoten
přistoupit	přistoupit	k5eAaPmF	přistoupit
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
čelit	čelit	k5eAaImF	čelit
krutým	krutý	k2eAgFnPc3d1	krutá
perzekucím	perzekuce	k1gFnPc3	perzekuce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nejnebezpečnější	bezpečný	k2eNgMnSc1d3	nejnebezpečnější
oponent	oponent	k1gMnSc1	oponent
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
komunistů	komunista	k1gMnPc2	komunista
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
její	její	k3xOp3gFnSc1	její
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
struktury	struktura	k1gFnPc1	struktura
sídlily	sídlit	k5eAaImAgFnP	sídlit
mimo	mimo	k7c4	mimo
jejich	jejich	k3xOp3gInSc4	jejich
dosah	dosah	k1gInSc4	dosah
a	a	k8xC	a
podmanit	podmanit	k5eAaPmF	podmanit
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
církev	církev	k1gFnSc1	církev
sama	sám	k3xTgFnSc1	sám
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
nenásilnou	násilný	k2eNgFnSc7d1	nenásilná
formou	forma	k1gFnSc7	forma
komunismus	komunismus	k1gInSc1	komunismus
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
narušovala	narušovat	k5eAaImAgFnS	narušovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolávalo	vyvolávat	k5eAaImAgNnS	vyvolávat
snahy	snaha	k1gFnPc4	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
na	na	k7c4	na
území	území	k1gNnSc4	území
komunistických	komunistický	k2eAgInPc2d1	komunistický
států	stát	k1gInPc2	stát
odštěpenecké	odštěpenecký	k2eAgFnSc2d1	odštěpenecká
národní	národní	k2eAgFnSc2d1	národní
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
většinou	většinou	k6eAd1	většinou
nesetkalo	setkat	k5eNaPmAgNnS	setkat
s	s	k7c7	s
výraznějšími	výrazný	k2eAgInPc7d2	výraznější
úspěchy	úspěch	k1gInPc7	úspěch
(	(	kIx(	(
<g/>
jediným	jediný	k2eAgInSc7d1	jediný
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
státem	stát	k1gInSc7	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
stala	stát	k5eAaPmAgFnS	stát
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potlačovaným	potlačovaný	k2eAgNnSc7d1	potlačované
náboženstvím	náboženství	k1gNnSc7	náboženství
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
také	také	k9	také
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
Povolží	Povolží	k1gNnSc6	Povolží
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
komunistické	komunistický	k2eAgFnSc2d1	komunistická
snahy	snaha	k1gFnSc2	snaha
o	o	k7c6	o
ateizaci	ateizace	k1gFnSc6	ateizace
společnosti	společnost	k1gFnSc2	společnost
nepokročily	pokročit	k5eNaPmAgInP	pokročit
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
jako	jako	k8xS	jako
v	v	k7c6	v
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
aspektem	aspekt	k1gInSc7	aspekt
<g/>
,	,	kIx,	,
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
úspěšným	úspěšný	k2eAgNnSc7d1	úspěšné
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
důsledná	důsledný	k2eAgFnSc1d1	důsledná
ateistická	ateistický	k2eAgFnSc1d1	ateistická
výchova	výchova	k1gFnSc1	výchova
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
v	v	k7c6	v
sociálních	sociální	k2eAgNnPc6d1	sociální
zařízeních	zařízení	k1gNnPc6	zařízení
pro	pro	k7c4	pro
mladistvé	mladistvý	k1gMnPc4	mladistvý
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
jištěna	jistit	k5eAaImNgFnS	jistit
cíleným	cílený	k2eAgNnSc7d1	cílené
vyřazováním	vyřazování	k1gNnSc7	vyřazování
věřících	věřící	k1gMnPc2	věřící
ze	z	k7c2	z
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
procesu	proces	k1gInSc2	proces
(	(	kIx(	(
<g/>
řadě	řada	k1gFnSc3	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
znemožněno	znemožnit	k5eAaPmNgNnS	znemožnit
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
nebo	nebo	k8xC	nebo
i	i	k9	i
středoškolské	středoškolský	k2eAgNnSc4d1	středoškolské
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
vyloučené	vyloučený	k2eAgNnSc1d1	vyloučené
pak	pak	k9	pak
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
aktivní	aktivní	k2eAgMnSc1d1	aktivní
věřící	věřící	k1gMnSc1	věřící
stal	stát	k5eAaPmAgMnS	stát
učitelem	učitel	k1gMnSc7	učitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
sice	sice	k8xC	sice
byla	být	k5eAaImAgFnS	být
svoboda	svoboda	k1gFnSc1	svoboda
náboženství	náboženství	k1gNnSc2	náboženství
garantována	garantovat	k5eAaBmNgFnS	garantovat
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
výuka	výuka	k1gFnSc1	výuka
náboženství	náboženství	k1gNnSc2	náboženství
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
však	však	k9	však
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
mnohými	mnohý	k2eAgFnPc7d1	mnohá
restrikcemi	restrikce	k1gFnPc7	restrikce
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
navštěvování	navštěvování	k1gNnSc4	navštěvování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zaznamenávalo	zaznamenávat	k5eAaImAgNnS	zaznamenávat
do	do	k7c2	do
posudků	posudek	k1gInPc2	posudek
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
znamenalo	znamenat	k5eAaImAgNnS	znamenat
automatické	automatický	k2eAgNnSc1d1	automatické
znemožnění	znemožnění	k1gNnSc1	znemožnění
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přístup	přístup	k1gInSc1	přístup
komunistů	komunista	k1gMnPc2	komunista
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
existence	existence	k1gFnSc2	existence
SSSR	SSSR	kA	SSSR
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
antikomunistů	antikomunista	k1gMnPc2	antikomunista
byl	být	k5eAaImAgInS	být
čistě	čistě	k6eAd1	čistě
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
<g/>
:	:	kIx,	:
Stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
církev	církev	k1gFnSc4	církev
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
platilo	platit	k5eAaImAgNnS	platit
zejména	zejména	k9	zejména
za	za	k7c4	za
Lenina	Lenin	k1gMnSc4	Lenin
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vztah	vztah	k1gInSc1	vztah
mnohem	mnohem	k6eAd1	mnohem
diferencovanější	diferencovaný	k2eAgInSc1d2	diferencovanější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
zabral	zabrat	k5eAaPmAgMnS	zabrat
SSSR	SSSR	kA	SSSR
veškeré	veškerý	k3xTgFnPc4	veškerý
církevní	církevní	k2eAgFnPc4d1	církevní
cennosti	cennost	k1gFnPc4	cennost
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
hladomoru	hladomor	k1gInSc3	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Zbylý	zbylý	k2eAgInSc1d1	zbylý
církevní	církevní	k2eAgInSc1d1	církevní
majetek	majetek	k1gInSc1	majetek
byl	být	k5eAaImAgInS	být
sekvestrován	sekvestrovat	k5eAaBmNgInS	sekvestrovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kněžími	kněz	k1gMnPc7	kněz
se	se	k3xPyFc4	se
našlo	najít	k5eAaPmAgNnS	najít
pár	pár	k4xCyI	pár
kolaborantů	kolaborant	k1gMnPc2	kolaborant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
s	s	k7c7	s
bolševiky	bolševik	k1gMnPc7	bolševik
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vedenskij	Vedenskij	k1gMnSc1	Vedenskij
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Krasnitskij	Krasnitskij	k1gFnSc2	Krasnitskij
zorganizovali	zorganizovat	k5eAaPmAgMnP	zorganizovat
Dočasnou	dočasný	k2eAgFnSc4d1	dočasná
vyšší	vysoký	k2eAgFnSc4d2	vyšší
církevní	církevní	k2eAgFnSc4d1	církevní
správu	správa	k1gFnSc4	správa
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
podporu	podpora	k1gFnSc4	podpora
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
bílými	bílý	k2eAgFnPc7d1	bílá
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ženatými	ženatý	k2eAgMnPc7d1	ženatý
<g/>
)	)	kIx)	)
kněžími	kněz	k1gMnPc7	kněz
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
církevního	církevní	k2eAgNnSc2d1	církevní
práva	právo	k1gNnSc2	právo
zapovězen	zapovědět	k5eAaPmNgInS	zapovědět
postup	postup	k1gInSc1	postup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
biskupa	biskup	k1gMnSc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
"	"	kIx"	"
<g/>
pracující	pracující	k2eAgFnSc2d1	pracující
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pokrokářů	pokrokář	k1gMnPc2	pokrokář
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
tzv.	tzv.	kA	tzv.
Živá	živá	k1gFnSc1	živá
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
Ж	Ж	k?	Ж
ц	ц	k?	ц
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
byl	být	k5eAaImAgMnS	být
vystudovaný	vystudovaný	k2eAgMnSc1d1	vystudovaný
pravoslavný	pravoslavný	k2eAgMnSc1d1	pravoslavný
kněz	kněz	k1gMnSc1	kněz
(	(	kIx(	(
<g/>
nesložil	složit	k5eNaPmAgMnS	složit
pouze	pouze	k6eAd1	pouze
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Znal	znát	k5eAaImAgMnS	znát
proto	proto	k8xC	proto
církev	církev	k1gFnSc1	církev
dobře	dobře	k6eAd1	dobře
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
lze	lze	k6eAd1	lze
jako	jako	k8xS	jako
každou	každý	k3xTgFnSc4	každý
jinou	jiný	k2eAgFnSc4d1	jiná
byrokratickou	byrokratický	k2eAgFnSc4d1	byrokratická
strukturu	struktura	k1gFnSc4	struktura
přimět	přimět	k5eAaPmF	přimět
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
není	být	k5eNaImIp3nS	být
záležitost	záležitost	k1gFnSc4	záležitost
jednoho	jeden	k4xCgMnSc2	jeden
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
společenství	společenství	k1gNnSc1	společenství
mnoha	mnoho	k4c2	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Živou	živý	k2eAgFnSc4d1	živá
církev	církev	k1gFnSc4	církev
zrušil	zrušit	k5eAaPmAgMnS	zrušit
a	a	k8xC	a
její	její	k3xOp3gMnPc4	její
příslušníky	příslušník	k1gMnPc4	příslušník
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
oficiální	oficiální	k2eAgFnSc2d1	oficiální
ruské	ruský	k2eAgFnSc2d1	ruská
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1943	[number]	k4	1943
byl	být	k5eAaImAgInS	být
patriarchou	patriarcha	k1gMnSc7	patriarcha
moskevským	moskevský	k2eAgFnPc3d1	Moskevská
a	a	k8xC	a
vší	všecek	k3xTgFnSc2	všecek
Rusi	Rus	k1gFnSc2	Rus
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Sergej	Sergej	k1gMnSc1	Sergej
(	(	kIx(	(
<g/>
Sergius	Sergius	k1gInSc1	Sergius
<g/>
;	;	kIx,	;
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
Ivan	Ivan	k1gMnSc1	Ivan
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Stragorodskij	Stragorodskij	k1gMnSc1	Stragorodskij
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
mnohými	mnohé	k1gNnPc7	mnohé
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
kolaborace	kolaborace	k1gFnSc2	kolaborace
a	a	k8xC	a
hereze	hereze	k1gFnSc2	hereze
sergianismu	sergianismus	k1gInSc2	sergianismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oběti	oběť	k1gFnPc4	oběť
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Československo	Československo	k1gNnSc1	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
režimu	režim	k1gInSc2	režim
byly	být	k5eAaImAgInP	být
statisíce	statisíce	k1gInPc1	statisíce
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
uvězněny	uvězněn	k2eAgFnPc1d1	uvězněna
<g/>
,	,	kIx,	,
internovány	internován	k2eAgFnPc1d1	internována
nebo	nebo	k8xC	nebo
umístěny	umístit	k5eAaPmNgFnP	umístit
do	do	k7c2	do
pracovních	pracovní	k2eAgInPc2d1	pracovní
či	či	k8xC	či
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
či	či	k8xC	či
PTP	PTP	kA	PTP
a	a	k8xC	a
tisíce	tisíc	k4xCgInPc4	tisíc
dalších	další	k2eAgMnPc2d1	další
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
obětí	oběť	k1gFnSc7	oběť
justičních	justiční	k2eAgFnPc2d1	justiční
vražd	vražda	k1gFnPc2	vražda
nebo	nebo	k8xC	nebo
zemřely	zemřít	k5eAaPmAgFnP	zemřít
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
či	či	k8xC	či
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c6	o
překonání	překonání	k1gNnSc6	překonání
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čína	Čína	k1gFnSc1	Čína
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
:	:	kIx,	:
Devět	devět	k4xCc1	devět
komentářů	komentář	k1gInPc2	komentář
k	k	k7c3	k
Čínské	čínský	k2eAgFnSc3d1	čínská
komunistické	komunistický	k2eAgFnSc3d1	komunistická
straně	strana	k1gFnSc3	strana
9	[number]	k4	9
komentářů	komentář	k1gInPc2	komentář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Tibet	Tibet	k1gInSc1	Tibet
====	====	k?	====
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
:	:	kIx,	:
Potala	Potal	k1gMnSc2	Potal
–	–	k?	–
Nadační	nadační	k2eAgInSc4d1	nadační
fond	fond	k1gInSc4	fond
pro	pro	k7c4	pro
Tibet	Tibet	k1gInSc4	Tibet
Potala	Potal	k1gMnSc4	Potal
<g/>
)	)	kIx)	)
<g/>
Okupace	okupace	k1gFnSc2	okupace
Tibetu	Tibet	k1gInSc2	Tibet
Komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
Číny	Čína	k1gFnSc2	Čína
(	(	kIx(	(
<g/>
od	od	k7c2	od
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
–	–	k?	–
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
armáda	armáda	k1gFnSc1	armáda
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
40	[number]	k4	40
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
východního	východní	k2eAgInSc2d1	východní
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
porazila	porazit	k5eAaPmAgFnS	porazit
osmitisícovou	osmitisícový	k2eAgFnSc4d1	osmitisícová
tibetskou	tibetský	k2eAgFnSc4d1	tibetská
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
–	–	k?	–
Ve	v	k7c6	v
Lhase	Lhasa	k1gFnSc6	Lhasa
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
Čína	Čína	k1gFnSc1	Čína
potlačila	potlačit	k5eAaPmAgFnS	potlačit
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
87	[number]	k4	87
000	[number]	k4	000
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
Tibeťanů	Tibeťan	k1gMnPc2	Tibeťan
<g/>
.	.	kIx.	.
</s>
<s>
Dalajlama	dalajlama	k1gMnSc1	dalajlama
opustil	opustit	k5eAaPmAgMnS	opustit
Tibet	Tibet	k1gInSc4	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
000	[number]	k4	000
Tibeťanů	Tibeťan	k1gMnPc2	Tibeťan
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
dalajlamovi	dalajlama	k1gMnSc3	dalajlama
v	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
–	–	k?	–
97	[number]	k4	97
procent	procento	k1gNnPc2	procento
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
"	"	kIx"	"
<g/>
Tibetské	tibetský	k2eAgFnSc6d1	tibetská
autonomní	autonomní	k2eAgFnSc6d1	autonomní
oblasti	oblast	k1gFnSc6	oblast
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
TAO	TAO	kA	TAO
<g/>
)	)	kIx)	)
a	a	k8xC	a
98	[number]	k4	98
<g/>
–	–	k?	–
<g/>
99	[number]	k4	99
procent	procento	k1gNnPc2	procento
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
tibetských	tibetský	k2eAgFnPc6d1	tibetská
oblastech	oblast	k1gFnPc6	oblast
mimo	mimo	k7c4	mimo
TAO	TAO	kA	TAO
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
buď	buď	k8xC	buď
vylidněno	vylidněn	k2eAgNnSc1d1	vylidněno
nebo	nebo	k8xC	nebo
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
6259	[number]	k4	6259
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
ušlo	ujít	k5eAaPmAgNnS	ujít
zkáze	zkáza	k1gFnSc3	zkáza
pouze	pouze	k6eAd1	pouze
osm	osm	k4xCc1	osm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
–	–	k?	–
Maova	Maovo	k1gNnSc2	Maovo
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
revoluce	revoluce	k1gFnSc1	revoluce
rozpoutává	rozpoutávat	k5eAaImIp3nS	rozpoutávat
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
další	další	k2eAgFnSc4d1	další
vlnu	vlna	k1gFnSc4	vlna
zabíjení	zabíjení	k1gNnSc2	zabíjení
a	a	k8xC	a
ničení	ničení	k1gNnSc2	ničení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čínské	čínský	k2eAgFnSc2d1	čínská
invaze	invaze	k1gFnSc2	invaze
a	a	k8xC	a
okupace	okupace	k1gFnPc4	okupace
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
Tibeťanů	Tibeťan	k1gMnPc2	Tibeťan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Čína	Čína	k1gFnSc1	Čína
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
kampaně	kampaň	k1gFnPc4	kampaň
Tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
úder	úder	k1gInSc1	úder
<g/>
,	,	kIx,	,
Vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
převýchova	převýchova	k1gFnSc1	převýchova
a	a	k8xC	a
Duchovní	duchovní	k2eAgFnSc1d1	duchovní
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
přimět	přimět	k5eAaPmF	přimět
Tibeťany	Tibeťan	k1gMnPc4	Tibeťan
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zřekli	zřeknout	k5eAaPmAgMnP	zřeknout
své	svůj	k3xOyFgInPc4	svůj
víry	vír	k1gInPc4	vír
v	v	k7c4	v
dalajlamu	dalajlama	k1gMnSc4	dalajlama
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
postiženy	postižen	k2eAgInPc1d1	postižen
byly	být	k5eAaImAgInP	být
kláštery	klášter	k1gInPc1	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Praxe	praxe	k1gFnSc1	praxe
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Politiky	politika	k1gFnSc2	politika
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgInPc2d1	různý
důrazů	důraz	k1gInPc2	důraz
<g/>
,	,	kIx,	,
stylů	styl	k1gInPc2	styl
vládnutí	vládnutí	k1gNnSc2	vládnutí
a	a	k8xC	a
kampaní	kampaň	k1gFnSc7	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
SSSR	SSSR	kA	SSSR
například	například	k6eAd1	například
počáteční	počáteční	k2eAgInSc4d1	počáteční
válečný	válečný	k2eAgInSc4d1	válečný
komunismus	komunismus	k1gInSc4	komunismus
následovala	následovat	k5eAaImAgFnS	následovat
uvolněnější	uvolněný	k2eAgFnSc1d2	uvolněnější
Nová	nový	k2eAgFnSc1d1	nová
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
stalinský	stalinský	k2eAgInSc4d1	stalinský
plán	plán	k1gInSc4	plán
industrializace	industrializace	k1gFnSc2	industrializace
a	a	k8xC	a
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
a	a	k8xC	a
léta	léto	k1gNnSc2	léto
extenzivního	extenzivní	k2eAgInSc2d1	extenzivní
rozvoje	rozvoj	k1gInSc2	rozvoj
nakonec	nakonec	k6eAd1	nakonec
Gorbačovova	Gorbačovův	k2eAgFnSc1d1	Gorbačovova
perestrojka	perestrojka	k1gFnSc1	perestrojka
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
soubor	soubor	k1gInSc1	soubor
politik	politika	k1gFnPc2	politika
a	a	k8xC	a
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
většině	většina	k1gFnSc3	většina
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
obvykle	obvykle	k6eAd1	obvykle
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
spisů	spis	k1gInPc2	spis
Marxe	Marx	k1gMnSc2	Marx
<g/>
,	,	kIx,	,
Engelse	Engels	k1gMnSc2	Engels
a	a	k8xC	a
Lenina	Lenin	k1gMnSc2	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
komunisty	komunista	k1gMnPc4	komunista
řízené	řízený	k2eAgInPc1d1	řízený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
často	často	k6eAd1	často
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
míře	míra	k1gFnSc6	míra
a	a	k8xC	a
způsobu	způsob	k1gInSc6	způsob
jejich	jejich	k3xOp3gNnSc2	jejich
uplatňování	uplatňování	k1gNnSc2	uplatňování
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
obvykle	obvykle	k6eAd1	obvykle
tato	tento	k3xDgNnPc4	tento
opatření	opatření	k1gNnPc4	opatření
směřující	směřující	k2eAgNnPc4d1	směřující
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
dosažené	dosažený	k2eAgFnSc2d1	dosažená
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
vybudování	vybudování	k1gNnSc2	vybudování
beztřídní	beztřídní	k2eAgFnSc2d1	beztřídní
společnosti	společnost	k1gFnSc2	společnost
tak	tak	k6eAd1	tak
či	či	k8xC	či
onak	onak	k6eAd1	onak
realizují	realizovat	k5eAaBmIp3nP	realizovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zestátnění	zestátnění	k1gNnSc1	zestátnění
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
klíčových	klíčový	k2eAgInPc2d1	klíčový
podniků	podnik	k1gInPc2	podnik
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
finančnictví	finančnictví	k1gNnSc2	finančnictví
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
bývá	bývat	k5eAaImIp3nS	bývat
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
zejména	zejména	k9	zejména
rolnických	rolnický	k2eAgNnPc2d1	rolnické
družstev	družstvo	k1gNnPc2	družstvo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vznik	vznik	k1gInSc1	vznik
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vynucován	vynucován	k2eAgInSc1d1	vynucován
(	(	kIx(	(
<g/>
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
opatřením	opatření	k1gNnSc7	opatření
proti	proti	k7c3	proti
vykořisťování	vykořisťování	k1gNnSc3	vykořisťování
patří	patřit	k5eAaImIp3nS	patřit
rovněž	rovněž	k9	rovněž
vysoce	vysoce	k6eAd1	vysoce
progresivní	progresivní	k2eAgFnSc1d1	progresivní
daňová	daňový	k2eAgFnSc1d1	daňová
zátěž	zátěž	k1gFnSc1	zátěž
a	a	k8xC	a
značná	značný	k2eAgFnSc1d1	značná
nivelizace	nivelizace	k1gFnSc1	nivelizace
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgNnSc4d1	centrální
řízení	řízení	k1gNnSc4	řízení
všech	všecek	k3xTgInPc2	všecek
aspektů	aspekt	k1gInPc2	aspekt
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
státní	státní	k2eAgInSc1d1	státní
monopol	monopol	k1gInSc1	monopol
a	a	k8xC	a
omezení	omezení	k1gNnSc1	omezení
trhu	trh	k1gInSc2	trh
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
přímé	přímý	k2eAgNnSc4d1	přímé
působení	působení	k1gNnSc4	působení
tržních	tržní	k2eAgFnPc2d1	tržní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
všechny	všechen	k3xTgInPc4	všechen
aspekty	aspekt	k1gInPc4	aspekt
ekonomiky	ekonomika	k1gFnSc2	ekonomika
(	(	kIx(	(
<g/>
ceny	cena	k1gFnPc1	cena
<g/>
,	,	kIx,	,
investice	investice	k1gFnPc1	investice
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
objem	objem	k1gInSc1	objem
výroby	výroba	k1gFnSc2	výroba
<g/>
)	)	kIx)	)
určovány	určovat	k5eAaImNgFnP	určovat
direktivně	direktivně	k6eAd1	direktivně
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Stalinových	Stalinových	k2eAgFnPc2d1	Stalinových
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
pětiletém	pětiletý	k2eAgInSc6d1	pětiletý
cyklu	cyklus	k1gInSc6	cyklus
vyhlašovaly	vyhlašovat	k5eAaImAgInP	vyhlašovat
centrální	centrální	k2eAgInPc1d1	centrální
plány	plán	k1gInPc1	plán
celého	celý	k2eAgNnSc2d1	celé
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
plánování	plánování	k1gNnSc6	plánování
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
těžký	těžký	k2eAgInSc4d1	těžký
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
potřeby	potřeba	k1gFnPc4	potřeba
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgInPc1d1	lidský
zdroje	zdroj	k1gInPc1	zdroj
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
výrobní	výrobní	k2eAgInPc1d1	výrobní
faktory	faktor	k1gInPc1	faktor
řízeny	řídit	k5eAaImNgInP	řídit
centrálně	centrálně	k6eAd1	centrálně
a	a	k8xC	a
dbá	dbát	k5eAaImIp3nS	dbát
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
maximálně	maximálně	k6eAd1	maximálně
využívány	využívat	k5eAaImNgInP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
práci	práce	k1gFnSc3	práce
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
(	(	kIx(	(
<g/>
příživnictví	příživnictví	k1gNnSc2	příživnictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgNnPc1d1	sociální
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
státem	stát	k1gInSc7	stát
financované	financovaný	k2eAgNnSc1d1	financované
školství	školství	k1gNnSc1	školství
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
a	a	k8xC	a
povinný	povinný	k2eAgInSc1d1	povinný
penzijní	penzijní	k2eAgInSc1d1	penzijní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Komunistické	komunistický	k2eAgInPc1d1	komunistický
režimy	režim	k1gInPc1	režim
obvykle	obvykle	k6eAd1	obvykle
centrálně	centrálně	k6eAd1	centrálně
zabezpečují	zabezpečovat	k5eAaImIp3nP	zabezpečovat
i	i	k9	i
dodávky	dodávka	k1gFnPc1	dodávka
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
jiného	jiný	k2eAgNnSc2d1	jiné
základního	základní	k2eAgNnSc2d1	základní
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emancipační	emancipační	k2eAgNnPc1d1	emancipační
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
zrušení	zrušení	k1gNnSc4	zrušení
stavovských	stavovský	k2eAgNnPc2d1	Stavovské
privilegií	privilegium	k1gNnPc2	privilegium
a	a	k8xC	a
nastolení	nastolení	k1gNnSc6	nastolení
rovnosti	rovnost	k1gFnSc2	rovnost
pohlaví	pohlaví	k1gNnSc2	pohlaví
před	před	k7c7	před
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
nikoli	nikoli	k9	nikoli
bez	bez	k7c2	bez
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
usnadnění	usnadnění	k1gNnSc4	usnadnění
rozvodů	rozvod	k1gInPc2	rozvod
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
interrupce	interrupce	k1gFnPc4	interrupce
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Radikální	radikální	k2eAgFnSc1d1	radikální
sexuální	sexuální	k2eAgFnSc1d1	sexuální
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
již	jenž	k3xRgFnSc4	jenž
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
někteří	některý	k3yIgMnPc1	některý
raní	raný	k2eAgMnPc1d1	raný
komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
potlačena	potlačen	k2eAgFnSc1d1	potlačena
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Národnostní	národnostní	k2eAgFnSc1d1	národnostní
politika	politika	k1gFnSc1	politika
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
si	se	k3xPyFc3	se
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
centralismu	centralismus	k1gInSc2	centralismus
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
spíše	spíše	k9	spíše
asimilaci	asimilace	k1gFnSc4	asimilace
než	než	k8xS	než
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
etnická	etnický	k2eAgNnPc4d1	etnické
privilegia	privilegium	k1gNnPc4	privilegium
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
však	však	k9	však
rovněž	rovněž	k9	rovněž
bývají	bývat	k5eAaImIp3nP	bývat
formálně	formálně	k6eAd1	formálně
odstraňována	odstraňován	k2eAgNnPc1d1	odstraňován
<g/>
,	,	kIx,	,
jakkoli	jakkoli	k8xS	jakkoli
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
mohou	moct	k5eAaImIp3nP	moct
přetrvávat	přetrvávat	k5eAaImF	přetrvávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Monopolizace	monopolizace	k1gFnSc1	monopolizace
politické	politický	k2eAgFnSc2d1	politická
moci	moc	k1gFnSc2	moc
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
úloha	úloha	k1gFnSc1	úloha
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
či	či	k8xC	či
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
omezení	omezení	k1gNnSc3	omezení
demokratické	demokratický	k2eAgFnSc2d1	demokratická
politické	politický	k2eAgFnSc2d1	politická
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInPc1d1	vnější
rysy	rys	k1gInPc1	rys
zastupitelské	zastupitelský	k2eAgFnSc2d1	zastupitelská
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
volby	volba	k1gFnSc2	volba
a	a	k8xC	a
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
bývají	bývat	k5eAaImIp3nP	bývat
zachovány	zachován	k2eAgInPc1d1	zachován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
volit	volit	k5eAaImF	volit
jiné	jiný	k2eAgMnPc4d1	jiný
kandidáty	kandidát	k1gMnPc4	kandidát
než	než	k8xS	než
schválené	schválený	k2eAgInPc1d1	schválený
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Stranou	stranou	k6eAd1	stranou
řízený	řízený	k2eAgInSc1d1	řízený
výběr	výběr	k1gInSc1	výběr
kádrů	kádr	k1gInPc2	kádr
<g/>
,	,	kIx,	,
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádné	žádný	k3yNgFnPc4	žádný
vyšší	vysoký	k2eAgFnPc4d2	vyšší
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
ekonomice	ekonomika	k1gFnSc6	ekonomika
a	a	k8xC	a
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
nebudou	být	k5eNaImBp3nP	být
obsazeny	obsadit	k5eAaPmNgInP	obsadit
lidmi	člověk	k1gMnPc7	člověk
bez	bez	k7c2	bez
stranického	stranický	k2eAgNnSc2d1	stranické
schválení	schválení	k1gNnSc2	schválení
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
navíc	navíc	k6eAd1	navíc
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zdvojování	zdvojování	k1gNnSc3	zdvojování
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vedle	vedle	k7c2	vedle
odborného	odborný	k2eAgMnSc2d1	odborný
řídícího	řídící	k2eAgMnSc2d1	řídící
pracovníka	pracovník	k1gMnSc2	pracovník
existuje	existovat	k5eAaImIp3nS	existovat
jeho	jeho	k3xOp3gNnSc1	jeho
politický	politický	k2eAgMnSc1d1	politický
"	"	kIx"	"
<g/>
dvojník	dvojník	k1gMnSc1	dvojník
<g/>
"	"	kIx"	"
z	z	k7c2	z
paralelní	paralelní	k2eAgFnSc2d1	paralelní
stranické	stranický	k2eAgFnSc2d1	stranická
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
velitel	velitel	k1gMnSc1	velitel
vojenské	vojenský	k2eAgFnSc2d1	vojenská
jednotky	jednotka	k1gFnSc2	jednotka
–	–	k?	–
politruk	politruk	k1gMnSc1	politruk
anebo	anebo	k8xC	anebo
ředitel	ředitel	k1gMnSc1	ředitel
závodu	závod	k1gInSc2	závod
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
závodní	závodní	k1gMnSc1	závodní
organizace	organizace	k1gFnSc2	organizace
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
:	:	kIx,	:
předseda	předseda	k1gMnSc1	předseda
místního	místní	k2eAgInSc2d1	místní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
současně	současně	k6eAd1	současně
předseda	předseda	k1gMnSc1	předseda
místní	místní	k2eAgFnSc2d1	místní
organizace	organizace	k1gFnSc2	organizace
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ideologická	ideologický	k2eAgFnSc1d1	ideologická
kontrola	kontrola	k1gFnSc1	kontrola
a	a	k8xC	a
indoktrinace	indoktrinace	k1gFnPc1	indoktrinace
zahrnující	zahrnující	k2eAgFnPc1d1	zahrnující
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
přísnou	přísný	k2eAgFnSc4d1	přísná
cenzuru	cenzura	k1gFnSc4	cenzura
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
podstatné	podstatný	k2eAgNnSc4d1	podstatné
omezení	omezení	k1gNnSc4	omezení
svobody	svoboda	k1gFnSc2	svoboda
projevu	projev	k1gInSc2	projev
a	a	k8xC	a
shromažďování	shromažďování	k1gNnSc2	shromažďování
<g/>
,	,	kIx,	,
předepsané	předepsaný	k2eAgFnPc1d1	předepsaná
osnovy	osnova	k1gFnPc1	osnova
škol	škola	k1gFnPc2	škola
všech	všecek	k3xTgInPc2	všecek
stupňů	stupeň	k1gInPc2	stupeň
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
povinnou	povinný	k2eAgFnSc4d1	povinná
výuku	výuka	k1gFnSc4	výuka
marxismu	marxismus	k1gInSc2	marxismus
<g/>
,	,	kIx,	,
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
náboženství	náboženství	k1gNnSc3	náboženství
a	a	k8xC	a
jiným	jiný	k2eAgMnPc3d1	jiný
ideovým	ideový	k2eAgMnPc3d1	ideový
protivníkům	protivník	k1gMnPc3	protivník
včetně	včetně	k7c2	včetně
příznivců	příznivec	k1gMnPc2	příznivec
"	"	kIx"	"
<g/>
nepravověrných	pravověrný	k2eNgInPc2d1	pravověrný
<g/>
"	"	kIx"	"
směrů	směr	k1gInPc2	směr
marxismu	marxismus	k1gInSc2	marxismus
<g/>
.	.	kIx.	.
</s>
<s>
Heslo	heslo	k1gNnSc4	heslo
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
"	"	kIx"	"
<g/>
Kdo	kdo	k3yQnSc1	kdo
nejde	jít	k5eNaImIp3nS	jít
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
proti	proti	k7c3	proti
nám	my	k3xPp1nPc3	my
<g/>
"	"	kIx"	"
varuje	varovat	k5eAaImIp3nS	varovat
každého	každý	k3xTgMnSc4	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
nechce	chtít	k5eNaImIp3nS	chtít
přidat	přidat	k5eAaPmF	přidat
<g/>
,	,	kIx,	,
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
jenom	jenom	k9	jenom
stát	stát	k5eAaImF	stát
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
diktatura	diktatura	k1gFnSc1	diktatura
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
kolektivismus	kolektivismus	k1gInSc4	kolektivismus
<g/>
:	:	kIx,	:
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
jde	jít	k5eAaImIp3nS	jít
lépe	dobře	k6eAd2	dobře
manipulovat-naproti	manipulovataprot	k1gMnPc1	manipulovat-naprot
tomu	ten	k3xDgNnSc3	ten
potírá	potírat	k5eAaImIp3nS	potírat
individualismus	individualismus	k1gInSc1	individualismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
buržoasní	buržoasní	k2eAgFnSc7d1	buržoasní
manýrou	manýra	k1gFnSc7	manýra
některých	některý	k3yIgMnPc2	některý
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zájmy	zájem	k1gInPc1	zájem
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
se	se	k3xPyFc4	se
podřizují	podřizovat	k5eAaImIp3nP	podřizovat
zájmům	zájem	k1gInPc3	zájem
kolektivu	kolektiv	k1gInSc2	kolektiv
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
heslo	heslo	k1gNnSc1	heslo
vypadá	vypadat	k5eAaPmIp3nS	vypadat
napohled	napohled	k6eAd1	napohled
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
než	než	k8xS	než
ho	on	k3xPp3gInSc4	on
začne	začít	k5eAaPmIp3nS	začít
diktatura	diktatura	k1gFnSc1	diktatura
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
let	léto	k1gNnPc2	léto
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
nikdo	nikdo	k3yNnSc1	nikdo
nechtěl	chtít	k5eNaImAgMnS	chtít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
tam	tam	k6eAd1	tam
malé	malý	k2eAgInPc1d1	malý
výdělky	výdělek	k1gInPc1	výdělek
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
to	ten	k3xDgNnSc4	ten
vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
jednoduše	jednoduše	k6eAd1	jednoduše
<g/>
:	:	kIx,	:
vydala	vydat	k5eAaPmAgFnS	vydat
direktivu	direktiva	k1gFnSc4	direktiva
<g/>
,	,	kIx,	,
že	že	k8xS	že
děti	dítě	k1gFnPc1	dítě
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
také	také	k9	také
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Dostaly	dostat	k5eAaPmAgInP	dostat
se	se	k3xPyFc4	se
jen	jen	k9	jen
na	na	k7c4	na
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
výuční	výuční	k2eAgInPc4d1	výuční
obory	obor	k1gInPc4	obor
nebo	nebo	k8xC	nebo
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
nebylo	být	k5eNaImAgNnS	být
vůbec	vůbec	k9	vůbec
možno	možno	k6eAd1	možno
si	se	k3xPyFc3	se
zakoupit	zakoupit	k5eAaPmF	zakoupit
denní	denní	k2eAgInSc4d1	denní
tisk	tisk	k1gInSc4	tisk
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
MORNING	MORNING	kA	MORNING
STAR	star	k1gFnSc1	star
–	–	k?	–
list	list	k1gInSc4	list
britské	britský	k2eAgFnSc2d1	britská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
VOLKSSTIMME	VOLKSSTIMME	kA	VOLKSSTIMME
–	–	k?	–
list	list	k1gInSc1	list
rakouských	rakouský	k2eAgMnPc2d1	rakouský
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
terorismus	terorismus	k1gInSc1	terorismus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
státem	stát	k1gInSc7	stát
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
teror	teror	k1gInSc1	teror
a	a	k8xC	a
fyzická	fyzický	k2eAgFnSc1d1	fyzická
likvidace	likvidace	k1gFnSc1	likvidace
skutečných	skutečný	k2eAgMnPc2d1	skutečný
nebo	nebo	k8xC	nebo
předpokládaných	předpokládaný	k2eAgMnPc2d1	předpokládaný
protivníků	protivník	k1gMnPc2	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
mají	mít	k5eAaImIp3nP	mít
speciální	speciální	k2eAgInPc1d1	speciální
oddíly	oddíl	k1gInPc1	oddíl
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
řídit	řídit	k5eAaImF	řídit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
síť	síť	k1gFnSc4	síť
"	"	kIx"	"
<g/>
převýchovných	převýchovný	k2eAgInPc2d1	převýchovný
táborů	tábor	k1gInPc2	tábor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Gulag	gulag	k1gInSc1	gulag
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
věznic	věznice	k1gFnPc2	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
komunismu	komunismus	k1gInSc2	komunismus
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
kolem	kolem	k7c2	kolem
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
<g/>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
průvodním	průvodní	k2eAgInSc7d1	průvodní
jevem	jev	k1gInSc7	jev
<g/>
,	,	kIx,	,
že	že	k8xS	že
teoretický	teoretický	k2eAgInSc1d1	teoretický
marxistický	marxistický	k2eAgInSc1d1	marxistický
internacionalismus	internacionalismus	k1gInSc1	internacionalismus
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
reálný	reálný	k2eAgInSc1d1	reálný
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
čeští	český	k2eAgMnPc1d1	český
komunisté	komunista	k1gMnPc1	komunista
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
vysoce	vysoce	k6eAd1	vysoce
preferovali	preferovat	k5eAaImAgMnP	preferovat
Aloise	Alois	k1gMnSc4	Alois
Jiráska	Jirásek	k1gMnSc4	Jirásek
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
tezi	teze	k1gFnSc6	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
dobré	dobrý	k2eAgNnSc1d1	dobré
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
,	,	kIx,	,
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
špatné	špatný	k2eAgNnSc1d1	špatné
od	od	k7c2	od
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komunismus	komunismus	k1gInSc1	komunismus
–	–	k?	–
ideální	ideální	k2eAgFnSc1d1	ideální
společnost	společnost	k1gFnSc1	společnost
i	i	k8xC	i
reálná	reálný	k2eAgFnSc1d1	reálná
politika	politika	k1gFnSc1	politika
–	–	k?	–
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
tématem	téma	k1gNnSc7	téma
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgNnPc2d1	významné
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
a	a	k8xC	a
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
bylo	být	k5eAaImAgNnS	být
živé	živý	k2eAgNnSc1d1	živé
zejména	zejména	k9	zejména
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
ruskou	ruský	k2eAgFnSc7d1	ruská
revolucí	revoluce	k1gFnSc7	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
a	a	k8xC	a
šedesátými	šedesátý	k4xOgNnPc7	šedesátý
léty	léto	k1gNnPc7	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reflexe	reflexe	k1gFnSc2	reflexe
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Stalinem	Stalin	k1gMnSc7	Stalin
a	a	k8xC	a
Ždanovem	Ždanov	k1gInSc7	Ždanov
na	na	k7c4	na
dogma	dogma	k1gNnSc4	dogma
povýšeného	povýšený	k2eAgInSc2d1	povýšený
"	"	kIx"	"
<g/>
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
rutinního	rutinní	k2eAgNnSc2d1	rutinní
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
vznikala	vznikat	k5eAaImAgNnP	vznikat
zejména	zejména	k9	zejména
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
dekádách	dekáda	k1gFnPc6	dekáda
Sovětského	sovětský	k2eAgInSc2d1	sovětský
Svazu	svaz	k1gInSc2	svaz
i	i	k9	i
komunisticky	komunisticky	k6eAd1	komunisticky
angažovaná	angažovaný	k2eAgNnPc1d1	angažované
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
obstála	obstát	k5eAaPmAgNnP	obstát
ve	v	k7c6	v
zkoušce	zkouška	k1gFnSc6	zkouška
času	čas	k1gInSc2	čas
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
například	například	k6eAd1	například
filmy	film	k1gInPc4	film
Sergeje	Sergej	k1gMnSc2	Sergej
Ejzenštejna	Ejzenštejn	k1gMnSc2	Ejzenštejn
<g/>
,	,	kIx,	,
grafiky	grafika	k1gFnSc2	grafika
El	Ela	k1gFnPc2	Ela
Lisického	Lisický	k2eAgMnSc2d1	Lisický
<g/>
,	,	kIx,	,
Šolochovův	Šolochovův	k2eAgInSc4d1	Šolochovův
román	román	k1gInSc4	román
Tichý	Tichý	k1gMnSc1	Tichý
Don	Don	k1gMnSc1	Don
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc1	báseň
Vladimira	Vladimiro	k1gNnSc2	Vladimiro
Majakovského	Majakovský	k2eAgNnSc2d1	Majakovský
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
Isaaka	Isaak	k1gMnSc2	Isaak
Babela	Babel	k1gMnSc2	Babel
nebo	nebo	k8xC	nebo
režijní	režijní	k2eAgInPc4d1	režijní
experimenty	experiment	k1gInPc4	experiment
Vsevoloda	Vsevoloda	k1gMnSc1	Vsevoloda
Mejercholda	Mejercholda	k1gMnSc1	Mejercholda
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
oba	dva	k4xCgMnPc1	dva
posledně	posledně	k6eAd1	posledně
jmenovaní	jmenovaný	k1gMnPc1	jmenovaný
skončili	skončit	k5eAaPmAgMnP	skončit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
tvůrců	tvůrce	k1gMnPc2	tvůrce
před	před	k7c7	před
popravčí	popravčí	k2eAgFnSc7d1	popravčí
četou	četa	k1gFnSc7	četa
<g/>
,	,	kIx,	,
výmluvně	výmluvně	k6eAd1	výmluvně
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
sovětské	sovětský	k2eAgFnSc6d1	sovětská
umění	umění	k1gNnSc4	umění
vznikalo	vznikat	k5eAaImAgNnS	vznikat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
širší	široký	k2eAgInSc1d2	širší
je	být	k5eAaImIp3nS	být
okruh	okruh	k1gInSc1	okruh
významných	významný	k2eAgMnPc2d1	významný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
komunismem	komunismus	k1gInSc7	komunismus
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
mu	on	k3xPp3gMnSc3	on
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
sloužili	sloužit	k5eAaImAgMnP	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Prózy	próza	k1gFnPc1	próza
jako	jako	k8xS	jako
Doktor	doktor	k1gMnSc1	doktor
Živago	Živago	k1gNnSc4	Živago
Borise	Boris	k1gMnSc2	Boris
Pasternaka	Pasternak	k1gMnSc2	Pasternak
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
George	George	k1gNnSc2	George
Orwella	Orwello	k1gNnSc2	Orwello
<g/>
,	,	kIx,	,
Jeden	jeden	k4xCgInSc1	jeden
den	den	k1gInSc1	den
Ivana	Ivan	k1gMnSc2	Ivan
Děnisoviče	Děnisovič	k1gMnSc2	Děnisovič
a	a	k8xC	a
Souostroví	souostroví	k1gNnSc1	souostroví
Gulag	gulag	k1gInSc1	gulag
Alexandra	Alexandra	k1gFnSc1	Alexandra
Solženicyna	Solženicyna	k1gFnSc1	Solženicyna
nebo	nebo	k8xC	nebo
Nesnesitelná	snesitelný	k2eNgFnSc1d1	nesnesitelná
lehkost	lehkost	k1gFnSc1	lehkost
bytí	bytí	k1gNnSc2	bytí
Milana	Milan	k1gMnSc2	Milan
Kundery	Kundera	k1gFnSc2	Kundera
jsou	být	k5eAaImIp3nP	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
pouhou	pouhý	k2eAgFnSc7d1	pouhá
reflexí	reflexe	k1gFnSc7	reflexe
jednoho	jeden	k4xCgInSc2	jeden
politického	politický	k2eAgInSc2d1	politický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
prózy	próza	k1gFnSc2	próza
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
například	například	k6eAd1	například
divadelní	divadelní	k2eAgFnPc1d1	divadelní
hry	hra	k1gFnPc1	hra
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
tvorba	tvorba	k1gFnSc1	tvorba
písničkářů	písničkář	k1gMnPc2	písničkář
Vladimira	Vladimir	k1gInSc2	Vladimir
Vysockého	vysocký	k2eAgMnSc2d1	vysocký
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Kryla	Kryl	k1gMnSc2	Kryl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
industriální	industriální	k2eAgFnSc1d1	industriální
hudba	hudba	k1gFnSc1	hudba
skupiny	skupina	k1gFnSc2	skupina
Laibach	Laibacha	k1gFnPc2	Laibacha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
komunismu	komunismus	k1gInSc2	komunismus
zpracovala	zpracovat	k5eAaPmAgNnP	zpracovat
i	i	k9	i
nesčetná	sčetný	k2eNgNnPc1d1	nesčetné
díla	dílo	k1gNnPc1	dílo
populárního	populární	k2eAgNnSc2d1	populární
umění	umění	k1gNnSc2	umění
od	od	k7c2	od
dětského	dětský	k2eAgInSc2d1	dětský
příběhu	příběh	k1gInSc2	příběh
Neználek	Neználka	k1gFnPc2	Neználka
ve	v	k7c6	v
Slunečním	sluneční	k2eAgNnSc6d1	sluneční
městě	město	k1gNnSc6	město
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Nosova	Nosov	k1gInSc2	Nosov
až	až	k8xS	až
po	po	k7c6	po
bondovky	bondovky	k?	bondovky
Iana	Ianus	k1gMnSc4	Ianus
Fleminga	Fleming	k1gMnSc4	Fleming
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
lidová	lidový	k2eAgFnSc1d1	lidová
demokracie	demokracie	k1gFnSc1	demokracie
</s>
</p>
<p>
<s>
reálný	reálný	k2eAgInSc1d1	reálný
socialismus	socialismus	k1gInSc1	socialismus
</s>
</p>
<p>
<s>
socialistický	socialistický	k2eAgInSc1d1	socialistický
stát	stát	k1gInSc1	stát
</s>
</p>
<p>
<s>
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
socialistických	socialistický	k2eAgInPc2d1	socialistický
států	stát	k1gInPc2	stát
</s>
</p>
