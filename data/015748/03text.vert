<s>
Negace	negace	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
logické	logický	k2eAgFnSc6d1
operaci	operace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
psychoanalytickém	psychoanalytický	k2eAgInSc6d1
pojmu	pojem	k1gInSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
negace	negace	k1gFnSc2
(	(	kIx(
<g/>
obranný	obranný	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Logická	logický	k2eAgFnSc1d1
negace	negace	k1gFnSc1
(	(	kIx(
<g/>
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
symbol	symbol	k1gInSc1
</s>
<s>
¬	¬	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
neg	neg	k?
}	}	kIx)
</s>
<s>
nebo	nebo	k8xC
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
T	T	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
NOT	nota	k1gFnPc2
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
,	,	kIx,
popř.	popř.	kA
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
pruhem	pruh	k1gInSc7
nad	nad	k7c4
proměnnou	proměnná	k1gFnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
unární	unární	k2eAgFnSc1d1
logická	logický	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vezme	vzít	k5eAaPmIp3nS
výrok	výrok	k1gInSc4
"	"	kIx"
<g/>
p	p	k?
<g/>
"	"	kIx"
do	do	k7c2
dalšího	další	k2eAgInSc2d1
výroku	výrok	k1gInSc2
"	"	kIx"
<g/>
ne	ne	k9
p	p	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
psáno	psán	k2eAgNnSc1d1
¬	¬	k?
<g/>
p	p	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
samostatně	samostatně	k6eAd1
interpretován	interpretován	k2eAgInSc1d1
jako	jako	k8xC,k8xS
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
p	p	k?
je	být	k5eAaImIp3nS
nepravda	nepravda	k1gFnSc1
nebo	nebo	k8xC
jako	jako	k9
nepravda	nepravda	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
p	p	k?
je	být	k5eAaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
vstup	vstup	k1gInSc4
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A	a	k9
<g/>
}	}	kIx)
</s>
<s>
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
pravdivostní	pravdivostní	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
negace	negace	k1gFnSc2
následovně	následovně	k6eAd1
(	(	kIx(
<g/>
0	#num#	k4
označuje	označovat	k5eAaImIp3nS
nepravdivé	pravdivý	k2eNgNnSc1d1
tvrzení	tvrzení	k1gNnSc1
<g/>
,	,	kIx,
1	#num#	k4
označuje	označovat	k5eAaImIp3nS
pravdivé	pravdivý	k2eAgNnSc1d1
tvrzení	tvrzení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A	a	k9
<g/>
}	}	kIx)
</s>
<s>
¬	¬	k?
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
neg	neg	k?
A	A	kA
<g/>
}	}	kIx)
</s>
<s>
01	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Negace	negace	k1gFnSc1
výroku	výrok	k1gInSc2
</s>
<s>
Negace	negace	k1gFnSc1
výroku	výrok	k1gInSc2
(	(	kIx(
<g/>
graficky	graficky	k6eAd1
¬	¬	k?
<g/>
,	,	kIx,
′	′	k?
<g/>
;	;	kIx,
textově	textově	k6eAd1
non	non	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
matematické	matematický	k2eAgFnSc6d1
logice	logika	k1gFnSc6
opačná	opačný	k2eAgFnSc1d1
pravdivostní	pravdivostní	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
k	k	k7c3
výroku	výrok	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negace	negace	k1gFnSc1
výroku	výrok	k1gInSc2
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
symbolem	symbol	k1gInSc7
¬	¬	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
symbol	symbol	k1gInSc1
se	se	k3xPyFc4
umísťuje	umísťovat	k5eAaImIp3nS
před	před	k7c4
označení	označení	k1gNnSc4
výroku	výrok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negace	negace	k1gFnSc1
výroku	výrok	k1gInSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
též	též	k9
zapsat	zapsat	k5eAaPmF
textově	textově	k6eAd1
non	non	k?
či	či	k8xC
graficky	graficky	k6eAd1
′	′	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
tedy	tedy	k9
výrok	výrok	k1gInSc4
označen	označit	k5eAaPmNgMnS
jako	jako	k9
A	a	k9
<g/>
,	,	kIx,
negace	negace	k1gFnSc1
takového	takový	k3xDgInSc2
výroku	výrok	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
¬	¬	k?
<g/>
A	a	k9
<g/>
,	,	kIx,
nonA	nonA	k?
či	či	k8xC
A	a	k9
<g/>
′	′	k?
<g/>
.	.	kIx.
</s>
<s>
Výrok	výrok	k1gInSc1
</s>
<s>
Negace	negace	k1gFnSc1
výroku	výrok	k1gInSc2
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1
</s>
<s>
V	v	k7c6
</s>
<s>
¬	¬	k?
<g/>
V	v	k7c6
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
výroku	výrok	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
negace	negace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jsem	být	k5eAaImIp1nS
editor	editor	k1gInSc4
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejsem	být	k5eNaImIp1nS
editor	editor	k1gInSc4
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Negace	negace	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
Není	být	k5eNaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
jsem	být	k5eAaImIp1nS
editor	editor	k1gInSc4
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Haf	haf	k1gInSc1
<g/>
!	!	kIx.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
výrok	výrok	k1gInSc4
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
ho	on	k3xPp3gInSc4
nelze	lze	k6eNd1
negovat	negovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
+	+	kIx~
2	#num#	k4
=	=	kIx~
5	#num#	k4
</s>
<s>
3	#num#	k4
+	+	kIx~
2	#num#	k4
≠	≠	k?
5	#num#	k4
</s>
<s>
3	#num#	k4
+	+	kIx~
2	#num#	k4
=	=	kIx~
6	#num#	k4
</s>
<s>
3	#num#	k4
+	+	kIx~
2	#num#	k4
≠	≠	k?
6	#num#	k4
</s>
<s>
Výrok	výrok	k1gInSc1
je	být	k5eAaImIp3nS
vše	všechen	k3xTgNnSc1
co	co	k9
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
označit	označit	k5eAaPmF
za	za	k7c4
pravdivé	pravdivý	k2eAgFnPc4d1
či	či	k8xC
nepravdivé	pravdivý	k2eNgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
3	#num#	k4
+	+	kIx~
2	#num#	k4
=	=	kIx~
6	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nepravdivý	pravdivý	k2eNgInSc4d1
výrok	výrok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
ho	on	k3xPp3gMnSc4
negujeme	negovat	k5eAaImIp1nP
<g/>
,	,	kIx,
zapíšeme	zapsat	k5eAaPmIp1nP
3	#num#	k4
+	+	kIx~
2	#num#	k4
≠	≠	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
negujeme	negovat	k5eAaImIp1nP
negaci	negace	k1gFnSc4
výroku	výrok	k1gInSc2
<g/>
,	,	kIx,
dostáváme	dostávat	k5eAaImIp1nP
opět	opět	k6eAd1
výrok	výrok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
<g/>
:	:	kIx,
¬	¬	k?
<g/>
(	(	kIx(
<g/>
¬	¬	k?
<g/>
A	A	kA
<g/>
)	)	kIx)
=	=	kIx~
A.	A.	kA
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
¬	¬	k?
</s>
<s>
(	(	kIx(
</s>
<s>
¬	¬	k?
</s>
<s>
A	a	k9
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
neg	neg	k?
(	(	kIx(
<g/>
\	\	kIx~
<g/>
neg	neg	k?
A	A	kA
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
A	a	k9
<g/>
}	}	kIx)
</s>
<s>
¬	¬	k?
</s>
<s>
(	(	kIx(
</s>
<s>
A	a	k9
</s>
<s>
∧	∧	k?
</s>
<s>
B	B	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
¬	¬	k?
</s>
<s>
A	a	k9
</s>
<s>
∨	∨	k?
</s>
<s>
¬	¬	k?
</s>
<s>
B	B	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
neg	neg	k?
(	(	kIx(
<g/>
A	A	kA
<g/>
\	\	kIx~
<g/>
land	land	k1gMnSc1
B	B	kA
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
neg	neg	k?
A	A	kA
<g/>
\	\	kIx~
<g/>
lor	lor	k?
\	\	kIx~
<g/>
neg	neg	k?
B	B	kA
<g/>
}	}	kIx)
</s>
<s>
¬	¬	k?
</s>
<s>
(	(	kIx(
</s>
<s>
A	a	k9
</s>
<s>
∨	∨	k?
</s>
<s>
B	B	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
¬	¬	k?
</s>
<s>
A	a	k9
</s>
<s>
∧	∧	k?
</s>
<s>
¬	¬	k?
</s>
<s>
B	B	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
neg	neg	k?
(	(	kIx(
<g/>
A	A	kA
<g/>
\	\	kIx~
<g/>
lor	lor	k?
B	B	kA
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
neg	neg	k?
A	A	kA
<g/>
\	\	kIx~
<g/>
land	land	k1gMnSc1
\	\	kIx~
<g/>
neg	neg	k?
B	B	kA
<g/>
}	}	kIx)
</s>
<s>
¬	¬	k?
</s>
<s>
(	(	kIx(
</s>
<s>
A	a	k9
</s>
<s>
⟹	⟹	k?
</s>
<s>
B	B	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
A	a	k9
</s>
<s>
∧	∧	k?
</s>
<s>
¬	¬	k?
</s>
<s>
B	B	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
neg	neg	k?
(	(	kIx(
<g/>
A	A	kA
<g/>
\	\	kIx~
<g/>
implies	implies	k1gMnSc1
B	B	kA
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
A	a	k9
<g/>
\	\	kIx~
<g/>
land	land	k1gInSc1
\	\	kIx~
<g/>
neg	neg	k?
B	B	kA
<g/>
}	}	kIx)
</s>
<s>
¬	¬	k?
</s>
<s>
(	(	kIx(
</s>
<s>
A	a	k9
</s>
<s>
↔	↔	k?
</s>
<s>
B	B	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
A	a	k9
</s>
<s>
∨	∨	k?
</s>
<s>
_	_	kIx~
</s>
<s>
B	B	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
neg	neg	k?
(	(	kIx(
<g/>
A	A	kA
<g/>
\	\	kIx~
<g/>
leftrightarrow	leftrightarrow	k?
B	B	kA
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
A	a	k9
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
underline	underlin	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
lor	lor	k?
}}	}}	k?
<g/>
B	B	kA
<g/>
}	}	kIx)
</s>
<s>
(	(	kIx(
<g/>
exkluzivní	exkluzivní	k2eAgFnPc4d1
disjunkce	disjunkce	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Booleova	Booleův	k2eAgFnSc1d1
algebra	algebra	k1gFnSc1
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c4
vyloučení	vyloučení	k1gNnSc4
třetího	třetí	k4xOgNnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
