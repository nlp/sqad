<s>
,	,	kIx,
popř.	popř.	kA
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
pruhem	pruh	k1gInSc7
nad	nad	k7c4
proměnnou	proměnná	k1gFnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
unární	unární	k2eAgFnSc1d1
logická	logický	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vezme	vzít	k5eAaPmIp3nS
výrok	výrok	k1gInSc4
"	"	kIx"
<g/>
p	p	k?
<g/>
"	"	kIx"
do	do	k7c2
dalšího	další	k2eAgInSc2d1
výroku	výrok	k1gInSc2
"	"	kIx"
<g/>
ne	ne	k9
p	p	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
psáno	psán	k2eAgNnSc1d1
¬	¬	k?
<g/>
p	p	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
samostatně	samostatně	k6eAd1
interpretován	interpretován	k2eAgInSc1d1
jako	jako	k8xC,k8xS
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
p	p	k?
je	být	k5eAaImIp3nS
nepravda	nepravda	k1gFnSc1
nebo	nebo	k8xC
jako	jako	k9
nepravda	nepravda	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
p	p	k?
je	být	k5eAaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
.	.	kIx.
</s>