<p>
<s>
Malathion	Malathion	k1gInSc1	Malathion
<g/>
,	,	kIx,	,
též	též	k9	též
malation	malation	k1gInSc1	malation
je	být	k5eAaImIp3nS	být
organofosfát	organofosfát	k5eAaPmF	organofosfát
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
používaný	používaný	k2eAgMnSc1d1	používaný
k	k	k7c3	k
hubení	hubení	k1gNnSc3	hubení
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
vyráběný	vyráběný	k2eAgMnSc1d1	vyráběný
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přítomnosti	přítomnost	k1gFnSc2	přítomnost
menšího	malý	k2eAgNnSc2d2	menší
množství	množství	k1gNnSc2	množství
nečistot	nečistota	k1gFnPc2	nečistota
slabě	slabě	k6eAd1	slabě
zažloutlý	zažloutlý	k2eAgInSc1d1	zažloutlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Malathion	Malathion	k1gInSc1	Malathion
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dvoustupňovou	dvoustupňový	k2eAgFnSc7d1	dvoustupňová
syntézou	syntéza	k1gFnSc7	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kroku	krok	k1gInSc6	krok
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
methanolu	methanol	k1gInSc2	methanol
s	s	k7c7	s
sulfidem	sulfid	k1gInSc7	sulfid
fosforečným	fosforečný	k2eAgInSc7d1	fosforečný
v	v	k7c6	v
toluenu	toluen	k1gInSc6	toluen
připraví	připravit	k5eAaPmIp3nS	připravit
kyselina	kyselina	k1gFnSc1	kyselina
dimethyldithiofosforečná	dimethyldithiofosforečný	k2eAgFnSc1d1	dimethyldithiofosforečný
(	(	kIx(	(
<g/>
DMPDT	DMPDT	kA	DMPDT
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
izolaci	izolace	k1gFnSc6	izolace
z	z	k7c2	z
reakční	reakční	k2eAgFnSc2d1	reakční
směsi	směs	k1gFnSc2	směs
pak	pak	k6eAd1	pak
kondenzací	kondenzace	k1gFnSc7	kondenzace
thiolové	thiolový	k2eAgFnSc2d1	thiolový
skupiny	skupina	k1gFnSc2	skupina
této	tento	k3xDgFnSc2	tento
kyseliny	kyselina	k1gFnSc2	kyselina
s	s	k7c7	s
dvojnou	dvojný	k2eAgFnSc7d1	dvojná
vazbou	vazba	k1gFnSc7	vazba
diethylfumarátu	diethylfumarát	k1gInSc2	diethylfumarát
(	(	kIx(	(
<g/>
diethylester	diethylester	k1gInSc1	diethylester
kyseliny	kyselina	k1gFnSc2	kyselina
fumarové	fumarová	k1gFnSc2	fumarová
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
diethylmaleátu	diethylmaleáta	k1gFnSc4	diethylmaleáta
(	(	kIx(	(
<g/>
diethylester	diethylester	k1gInSc1	diethylester
kyseliny	kyselina	k1gFnSc2	kyselina
maleinové	maleinový	k2eAgFnSc2d1	maleinová
<g/>
)	)	kIx)	)
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
přímo	přímo	k6eAd1	přímo
malathion	malathion	k1gInSc4	malathion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Molekula	molekula	k1gFnSc1	molekula
malathionu	malathion	k1gInSc2	malathion
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
centrum	centrum	k1gNnSc1	centrum
chirality	chiralita	k1gFnSc2	chiralita
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
enentiomerech	enentiomero	k1gNnPc6	enentiomero
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jejich	jejich	k3xOp3gFnSc1	jejich
racemická	racemický	k2eAgFnSc1d1	racemická
směs	směs	k1gFnSc1	směs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působením	působení	k1gNnSc7	působení
tepla	teplo	k1gNnSc2	teplo
isomerizuje	isomerizovat	k5eAaBmIp3nS	isomerizovat
přesunem	přesun	k1gInSc7	přesun
methylové	methylový	k2eAgFnSc2d1	methylová
skupiny	skupina	k1gFnSc2	skupina
z	z	k7c2	z
kyslíku	kyslík	k1gInSc2	kyslík
vázaného	vázané	k1gNnSc2	vázané
na	na	k7c4	na
fosfor	fosfor	k1gInSc4	fosfor
na	na	k7c4	na
atom	atom	k1gInSc4	atom
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
mnohem	mnohem	k6eAd1	mnohem
jedovatější	jedovatý	k2eAgInSc1d2	jedovatější
isomalathion	isomalathion	k1gInSc1	isomalathion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
reakci	reakce	k1gFnSc3	reakce
dochází	docházet	k5eAaImIp3nS	docházet
spontánně	spontánně	k6eAd1	spontánně
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
malathionu	malathion	k1gInSc2	malathion
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
otrav	otrava	k1gFnPc2	otrava
pracovníků	pracovník	k1gMnPc2	pracovník
ve	v	k7c6	v
výrobních	výrobní	k2eAgInPc6d1	výrobní
závodech	závod	k1gInPc6	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
metabolismu	metabolismus	k1gInSc6	metabolismus
malathionu	malathion	k1gInSc2	malathion
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oxidační	oxidační	k2eAgFnSc3d1	oxidační
desulfuraci	desulfurace	k1gFnSc3	desulfurace
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
odstranění	odstranění	k1gNnSc1	odstranění
atomu	atom	k1gInSc2	atom
síry	síra	k1gFnSc2	síra
vázaného	vázané	k1gNnSc2	vázané
dvojnou	dvojný	k2eAgFnSc7d1	dvojná
vazbou	vazba	k1gFnSc7	vazba
na	na	k7c4	na
atom	atom	k1gInSc4	atom
fosforu	fosfor	k1gInSc2	fosfor
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vlastní	vlastní	k2eAgFnSc2d1	vlastní
účinné	účinný	k2eAgFnSc2d1	účinná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
malaoxon	malaoxon	k1gInSc1	malaoxon
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
i	i	k9	i
abiogenicky	abiogenicky	k6eAd1	abiogenicky
působením	působení	k1gNnSc7	působení
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
nebo	nebo	k8xC	nebo
působením	působení	k1gNnSc7	působení
jiných	jiný	k2eAgNnPc2d1	jiné
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
je	být	k5eAaImIp3nS	být
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
i	i	k9	i
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyziologické	fyziologický	k2eAgNnSc4d1	fyziologické
působení	působení	k1gNnSc4	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Malathion	Malathion	k1gInSc1	Malathion
je	být	k5eAaImIp3nS	být
parasympatomimetikum	parasympatomimetikum	k1gNnSc1	parasympatomimetikum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
ireverzibilně	ireverzibilně	k6eAd1	ireverzibilně
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
cholinesterázu	cholinesteráza	k1gFnSc4	cholinesteráza
<g/>
.	.	kIx.	.
</s>
<s>
Malathion	Malathion	k1gInSc1	Malathion
je	být	k5eAaImIp3nS	být
insekticid	insekticid	k1gInSc1	insekticid
(	(	kIx(	(
<g/>
antiparazitický	antiparazitický	k2eAgMnSc1d1	antiparazitický
agent	agent	k1gMnSc1	agent
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
<g/>
)	)	kIx)	)
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
malou	malý	k2eAgFnSc7d1	malá
toxicitou	toxicita	k1gFnSc7	toxicita
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
teplokrevná	teplokrevný	k2eAgNnPc1d1	teplokrevné
zvířata	zvíře	k1gNnPc1	zvíře
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
však	však	k9	však
malathion	malathion	k1gInSc1	malathion
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
malaoxon	malaoxon	k1gInSc4	malaoxon
<g/>
,	,	kIx,	,
šedesátkrát	šedesátkrát	k6eAd1	šedesátkrát
více	hodně	k6eAd2	hodně
toxický	toxický	k2eAgInSc1d1	toxický
než	než	k8xS	než
malathion	malathion	k1gInSc1	malathion
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
jej	on	k3xPp3gMnSc4	on
aplikovat	aplikovat	k5eAaBmF	aplikovat
v	v	k7c6	v
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
prostorách	prostora	k1gFnPc6	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Malaoxon	Malaoxon	k1gInSc1	Malaoxon
také	také	k9	také
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
úpravách	úprava	k1gFnPc6	úprava
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
chlorováním	chlorování	k1gNnSc7	chlorování
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
malathion	malathion	k1gInSc1	malathion
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
desinsekci	desinsekce	k1gFnSc3	desinsekce
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
odběru	odběr	k1gInSc3	odběr
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
je	být	k5eAaImIp3nS	být
také	také	k9	také
isomalathion	isomalathion	k1gInSc1	isomalathion
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
běžnou	běžný	k2eAgFnSc7d1	běžná
příměsí	příměs	k1gFnSc7	příměs
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc7d1	vznikající
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
malathionu	malathion	k1gInSc2	malathion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrtné	smrtný	k2eAgFnPc1d1	Smrtná
dávky	dávka	k1gFnPc1	dávka
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
laboratorních	laboratorní	k2eAgNnPc2d1	laboratorní
zvířat	zvíře	k1gNnPc2	zvíře
při	při	k7c6	při
akutních	akutní	k2eAgFnPc6d1	akutní
otravách	otrava	k1gFnPc6	otrava
malathionem	malathion	k1gInSc7	malathion
značně	značně	k6eAd1	značně
vysoké	vysoký	k2eAgInPc1d1	vysoký
<g/>
;	;	kIx,	;
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
se	se	k3xPyFc4	se
u	u	k7c2	u
krys	krysa	k1gFnPc2	krysa
LD50	LD50	k1gFnPc1	LD50
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
1000	[number]	k4	1000
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
do	do	k7c2	do
10	[number]	k4	10
000	[number]	k4	000
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
u	u	k7c2	u
myší	myš	k1gFnPc2	myš
je	být	k5eAaImIp3nS	být
LD50	LD50	k1gFnSc1	LD50
=	=	kIx~	=
4000	[number]	k4	4000
až	až	k9	až
40	[number]	k4	40
000	[number]	k4	000
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
působení	působení	k1gNnSc6	působení
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
myší	myš	k1gFnPc2	myš
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
mírně	mírně	k6eAd1	mírně
snížená	snížený	k2eAgFnSc1d1	snížená
aktivita	aktivita	k1gFnSc1	aktivita
cholinesterázy	cholinesteráza	k1gFnSc2	cholinesteráza
<g/>
;	;	kIx,	;
při	při	k7c6	při
několikaměsíčním	několikaměsíční	k2eAgNnSc6d1	několikaměsíční
podávání	podávání	k1gNnSc6	podávání
mírných	mírný	k2eAgFnPc2d1	mírná
dávek	dávka	k1gFnPc2	dávka
lidským	lidský	k2eAgMnPc3d1	lidský
dobrovolníkům	dobrovolník	k1gMnPc3	dobrovolník
nebyly	být	k5eNaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
žádné	žádný	k3yNgInPc1	žádný
efekty	efekt	k1gInPc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Teratogenicita	Teratogenicita	k1gFnSc1	Teratogenicita
ani	ani	k8xC	ani
karcinogenicita	karcinogenicita	k1gFnSc1	karcinogenicita
u	u	k7c2	u
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
myší	myš	k1gFnPc2	myš
nebyla	být	k5eNaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
ani	ani	k8xC	ani
po	po	k7c6	po
soustavném	soustavný	k2eAgNnSc6d1	soustavné
tříletém	tříletý	k2eAgNnSc6d1	tříleté
podávání	podávání	k1gNnSc6	podávání
malathionu	malathion	k1gInSc2	malathion
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
březích	březí	k2eAgFnPc2d1	březí
myší	myš	k1gFnPc2	myš
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
toxicita	toxicita	k1gFnSc1	toxicita
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
u	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Otravy	otrava	k1gFnSc2	otrava
==	==	k?	==
</s>
</p>
<p>
<s>
Malathion	Malathion	k1gInSc1	Malathion
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
všemi	všecek	k3xTgInPc7	všecek
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
vdechováním	vdechování	k1gNnSc7	vdechování
<g/>
,	,	kIx,	,
požitím	požití	k1gNnSc7	požití
<g/>
,	,	kIx,	,
sliznicemi	sliznice	k1gFnPc7	sliznice
i	i	k8xC	i
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vdechnutí	vdechnutí	k1gNnSc6	vdechnutí
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
nebo	nebo	k8xC	nebo
při	při	k7c6	při
silném	silný	k2eAgNnSc6d1	silné
zasažení	zasažení	k1gNnSc6	zasažení
pokožky	pokožka	k1gFnSc2	pokožka
a	a	k8xC	a
sliznic	sliznice	k1gFnPc2	sliznice
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ztížené	ztížený	k2eAgNnSc4d1	ztížené
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
,	,	kIx,	,
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
slinění	slinění	k1gNnSc4	slinění
<g/>
,	,	kIx,	,
pocení	pocení	k1gNnSc4	pocení
<g/>
,	,	kIx,	,
závratě	závrať	k1gFnPc4	závrať
až	až	k8xS	až
bezvědomí	bezvědomí	k1gNnSc4	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požití	požití	k1gNnSc6	požití
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
objevují	objevovat	k5eAaImIp3nP	objevovat
bolesti	bolest	k1gFnPc1	bolest
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc1	nevolnost
až	až	k8xS	až
zvracení	zvracení	k1gNnSc1	zvracení
a	a	k8xC	a
průjem	průjem	k1gInSc1	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
přivolat	přivolat	k5eAaPmF	přivolat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malathion	Malathion	k1gInSc1	Malathion
se	se	k3xPyFc4	se
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
odbourává	odbourávat	k5eAaImIp3nS	odbourávat
a	a	k8xC	a
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
močí	močit	k5eAaImIp3nS	močit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
stolicí	stolice	k1gFnSc7	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
nehromadí	hromadit	k5eNaImIp3nP	hromadit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Karcinogenita	karcinogenita	k1gFnSc1	karcinogenita
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
(	(	kIx(	(
<g/>
IARC	IARC	kA	IARC
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
zařadila	zařadit	k5eAaPmAgFnS	zařadit
malathion	malathion	k1gInSc4	malathion
mezi	mezi	k7c7	mezi
karcinogeny	karcinogen	k1gInPc7	karcinogen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Malathion	Malathion	k1gInSc1	Malathion
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
hubení	hubení	k1gNnSc4	hubení
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
vší	veš	k1gFnPc2	veš
a	a	k8xC	a
zákožky	zákožka	k1gFnSc2	zákožka
svrabové	svrabový	k2eAgFnSc2d1	svrabová
(	(	kIx(	(
<g/>
původce	původce	k1gMnSc2	původce
svrabu	svrab	k1gInSc2	svrab
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
i	i	k9	i
pro	pro	k7c4	pro
eliminaci	eliminace	k1gFnSc4	eliminace
západonilského	západonilský	k2eAgInSc2d1	západonilský
viru	vir	k1gInSc2	vir
aj.	aj.	kA	aj.
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
široce	široko	k6eAd1	široko
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
jako	jako	k9	jako
prostředek	prostředek	k1gInSc4	prostředek
k	k	k7c3	k
hubení	hubení	k1gNnSc3	hubení
komárů	komár	k1gMnPc2	komár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
obchodními	obchodní	k2eAgInPc7d1	obchodní
názvy	název	k1gInPc7	název
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Malathion	Malathion	k1gInSc1	Malathion
<g/>
,	,	kIx,	,
Fosfotion	Fosfotion	k1gInSc1	Fosfotion
<g/>
,	,	kIx,	,
Phosphothion	Phosphothion	k1gInSc1	Phosphothion
<g/>
,	,	kIx,	,
Celthion	Celthion	k1gInSc1	Celthion
<g/>
,	,	kIx,	,
Cythion	Cythion	k1gInSc1	Cythion
<g/>
,	,	kIx,	,
Dielathion	Dielathion	k1gInSc1	Dielathion
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
4049	[number]	k4	4049
<g/>
,	,	kIx,	,
Emmaton	Emmaton	k1gInSc1	Emmaton
<g/>
,	,	kIx,	,
Exathios	Exathios	k1gInSc1	Exathios
<g/>
,	,	kIx,	,
Fyfanon	Fyfanon	k1gInSc1	Fyfanon
<g/>
,	,	kIx,	,
Hilthion	Hilthion	k1gInSc1	Hilthion
<g/>
,	,	kIx,	,
Karbofos	Karbofos	k1gInSc1	Karbofos
<g/>
,	,	kIx,	,
Maltox	Maltox	k1gInSc1	Maltox
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Malathion	Malathion	k1gInSc1	Malathion
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Malathion	Malathion	k1gInSc1	Malathion
<g/>
:	:	kIx,	:
Risk	risk	k1gInSc1	risk
Assessments	Assessments	k1gInSc1	Assessments
EPA	EPA	kA	EPA
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pesticide	pesticid	k1gInSc5	pesticid
Information	Information	k1gInSc4	Information
Profiles	Profiles	k1gInSc1	Profiles
-	-	kIx~	-
Malathion	Malathion	k1gInSc1	Malathion
USDA	USDA	kA	USDA
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
</s>
</p>
