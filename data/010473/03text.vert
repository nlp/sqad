<p>
<s>
Shikoku-Inu	Shikoku-Inout	k5eAaPmIp1nS	Shikoku-Inout
nebo	nebo	k8xC	nebo
Kochi-ken	Kochien	k1gInSc1	Kochi-ken
(	(	kIx(	(
<g/>
ken	ken	k?	ken
=	=	kIx~	=
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plemeno	plemeno	k1gNnSc1	plemeno
psa	pes	k1gMnSc2	pes
pocházející	pocházející	k2eAgFnSc7d1	pocházející
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
silného	silný	k2eAgMnSc2d1	silný
a	a	k8xC	a
nezávislého	závislý	k2eNgMnSc2d1	nezávislý
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
japonských	japonský	k2eAgMnPc2d1	japonský
loveckých	lovecký	k2eAgMnPc2d1	lovecký
psů	pes	k1gMnPc2	pes
nejmenší	malý	k2eAgInPc1d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
zejména	zejména	k9	zejména
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
,	,	kIx,	,
divokých	divoký	k2eAgNnPc2d1	divoké
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
japonském	japonský	k2eAgInSc6d1	japonský
venkově	venkov	k1gInSc6	venkov
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
využívá	využívat	k5eAaPmIp3nS	využívat
na	na	k7c4	na
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
spíš	spíš	k9	spíš
jako	jako	k9	jako
společník	společník	k1gMnSc1	společník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
plemeno	plemeno	k1gNnSc1	plemeno
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
středně	středně	k6eAd1	středně
velkých	velký	k2eAgMnPc2d1	velký
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Shikoku	Shikok	k1gInSc3	Shikok
byl	být	k5eAaImAgInS	být
chován	chován	k2eAgInSc1d1	chován
především	především	k9	především
jako	jako	k9	jako
lovecký	lovecký	k2eAgMnSc1d1	lovecký
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
lov	lov	k1gInSc4	lov
divokých	divoký	k2eAgNnPc2d1	divoké
prasat	prase	k1gNnPc2	prase
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Kochi	Koch	k1gFnSc2	Koch
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
tři	tři	k4xCgInPc1	tři
varianty	variant	k1gInPc1	variant
plemene	plemeno	k1gNnSc2	plemeno
<g/>
:	:	kIx,	:
Awa	Awa	k1gMnSc4	Awa
<g/>
,	,	kIx,	,
Hongawa	Hongawus	k1gMnSc4	Hongawus
a	a	k8xC	a
Hata	Hatus	k1gMnSc4	Hatus
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgMnPc4d1	pojmenovaný
podle	podle	k7c2	podle
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
chovány	chován	k2eAgFnPc1d1	chována
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
stupeň	stupeň	k1gInSc4	stupeň
čistokrevnosti	čistokrevnost	k1gFnSc2	čistokrevnost
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
varianta	varianta	k1gFnSc1	varianta
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
Hongawe	Hongawe	k1gFnSc6	Hongawe
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oblast	oblast	k1gFnSc1	oblast
chovu	chov	k1gInSc2	chov
byla	být	k5eAaImAgFnS	být
špatně	špatně	k6eAd1	špatně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgMnSc1d1	odolný
a	a	k8xC	a
hbitý	hbitý	k2eAgMnSc1d1	hbitý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
v	v	k7c6	v
hornatých	hornatý	k2eAgFnPc6d1	hornatá
krajinách	krajina	k1gFnPc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Plemeno	plemeno	k1gNnSc1	plemeno
převzalo	převzít	k5eAaPmAgNnS	převzít
název	název	k1gInSc4	název
ostrova	ostrov	k1gInSc2	ostrov
Shikoku	Shikok	k1gInSc2	Shikok
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
prohlášeno	prohlášen	k2eAgNnSc1d1	prohlášeno
"	"	kIx"	"
<g/>
národní	národní	k2eAgFnSc7d1	národní
památkou	památka	k1gFnSc7	památka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
středně	středně	k6eAd1	středně
velkého	velký	k2eAgMnSc4d1	velký
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
stavbu	stavba	k1gFnSc4	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
kompaktní	kompaktní	k2eAgFnSc4d1	kompaktní
<g/>
,	,	kIx,	,
silnou	silný	k2eAgFnSc4d1	silná
a	a	k8xC	a
pevnou	pevný	k2eAgFnSc4d1	pevná
kostru	kostra	k1gFnSc4	kostra
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
činí	činit	k5eAaImIp3nS	činit
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
cca	cca	kA	cca
52	[number]	k4	52
cm	cm	kA	cm
<g/>
,	,	kIx,	,
u	u	k7c2	u
fen	fena	k1gFnPc2	fena
cca	cca	kA	cca
46	[number]	k4	46
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výchova	výchova	k1gFnSc1	výchova
==	==	k?	==
</s>
</p>
<p>
<s>
Shiba	Shiba	k1gFnSc1	Shiba
je	být	k5eAaImIp3nS	být
mrštná	mrštný	k2eAgFnSc1d1	mrštná
a	a	k8xC	a
odvážná	odvážný	k2eAgFnSc1d1	odvážná
<g/>
,	,	kIx,	,
s	s	k7c7	s
vynikajícím	vynikající	k2eAgInSc7d1	vynikající
orientačním	orientační	k2eAgInSc7d1	orientační
smyslem	smysl	k1gInSc7	smysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terénu	terén	k1gInSc6	terén
odbíhá	odbíhat	k5eAaImIp3nS	odbíhat
od	od	k7c2	od
majitele	majitel	k1gMnSc2	majitel
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
probouzejí	probouzet	k5eAaImIp3nP	probouzet
staré	starý	k2eAgInPc4d1	starý
geny	gen	k1gInPc4	gen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
půjdete	jít	k5eAaImIp2nP	jít
se	s	k7c7	s
shibou	shiba	k1gFnSc7	shiba
na	na	k7c4	na
procházku	procházka	k1gFnSc4	procházka
<g/>
,	,	kIx,	,
smiřte	smířit	k5eAaPmRp2nP	smířit
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjdete	jít	k5eAaImIp2nP	jít
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Shibu	Shiba	k1gFnSc4	Shiba
uvidíte	uvidět	k5eAaPmIp2nP	uvidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Shikoku-Inu	Shikoku-In	k1gInSc2	Shikoku-In
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
krmivo-	krmivo-	k?	krmivo-
<g/>
brit.	brit.	k?	brit.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
plemenu	plemeno	k1gNnSc6	plemeno
</s>
</p>
<p>
<s>
www.ecanis.cz	www.ecanis.cz	k1gInSc1	www.ecanis.cz
-	-	kIx~	-
historie	historie	k1gFnSc1	historie
plemene	plemeno	k1gNnSc2	plemeno
</s>
</p>
<p>
<s>
www.idnes.cz	www.idnes.cz	k1gMnSc1	www.idnes.cz
</s>
</p>
