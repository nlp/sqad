<s>
GNOME	GNOME	kA
(	(	kIx(
<g/>
akronymická	akronymický	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
GNU	gnu	k1gNnSc4
Network	network	k1gInSc2
Object	Object	k1gInSc4
Model	model	k1gInSc1
Environment	Environment	k1gInSc1
<g/>
,	,	kIx,
anglická	anglický	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
gˈ	gˈ	k1gInSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
[	[	kIx(
<g/>
ˈ	ˈ	k1gNnSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prostředí	prostředí	k1gNnSc4
pracovní	pracovní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
pro	pro	k7c4
moderní	moderní	k2eAgInPc4d1
unixové	unixový	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
s	s	k7c7
instalovaným	instalovaný	k2eAgInSc7d1
X	X	kA
serverem	server	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
P.	P.	kA
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
postaveno	postavit	k5eAaPmNgNnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
nad	nad	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
(	(	kIx(
<g/>
The	The	k1gMnSc1
GIMP	GIMP	kA
Toolkit	Toolkit	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
napsána	napsat	k5eAaBmNgFnS,k5eAaPmNgFnS
pro	pro	k7c4
bitmapový	bitmapový	k2eAgInSc4d1
editor	editor	k1gInSc4
GIMP	GIMP	kA
<g/>
.	.	kIx.
</s>