<s>
GNOME	GNOME	kA
</s>
<s>
GNOME	GNOME	kA
</s>
<s>
Pracovní	pracovní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
GNOME	GNOME	kA
ShellVývojář	ShellVývojář	k1gInSc4
</s>
<s>
Nadace	nadace	k1gFnSc1
GNOME	GNOME	kA
První	první	k4xOgInSc4
vydání	vydání	k1gNnSc6
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Aktuální	aktuální	k2eAgFnPc1d1
verze	verze	k1gFnPc1
</s>
<s>
40.0	40.0	k4
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
systémy	systém	k1gInPc1
unixového	unixový	k2eAgInSc2d1
typu	typ	k1gInSc2
s	s	k7c7
X	X	kA
serverem	server	k1gInSc7
(	(	kIx(
<g/>
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
FreeBSD	FreeBSD	k1gFnSc1
<g/>
,	,	kIx,
Solaris	Solaris	k1gFnSc1
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
Vyvíjeno	vyvíjet	k5eAaImNgNnS
v	v	k7c6
</s>
<s>
C	C	kA
<g/>
,	,	kIx,
C	C	kA
<g/>
++	++	k?
<g/>
,	,	kIx,
Vala	Vala	k1gMnSc1
<g/>
,	,	kIx,
Python	Python	k1gMnSc1
<g/>
,	,	kIx,
JavaScript	JavaScript	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
desktopové	desktopový	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
Licence	licence	k1gFnSc2
</s>
<s>
GPL	GPL	kA
a	a	k8xC
LGPL	LGPL	kA
Lokalizace	lokalizace	k1gFnSc1
</s>
<s>
mnohojazyčná	mnohojazyčný	k2eAgFnSc1d1
<g/>
(	(	kIx(
<g/>
40	#num#	k4
jazyků	jazyk	k1gInPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Web	web	k1gInSc1
</s>
<s>
gnome	gnomat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
org	org	k?
Český	český	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
cs	cs	k?
<g/>
.	.	kIx.
<g/>
wiki	wik	k1gFnSc2
<g/>
.	.	kIx.
<g/>
gnome	gnomat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
org	org	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
GNOME	GNOME	kA
(	(	kIx(
<g/>
akronymická	akronymický	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
GNU	gnu	k1gNnSc4
Network	network	k1gInSc2
Object	Object	k1gInSc4
Model	model	k1gInSc1
Environment	Environment	k1gInSc1
<g/>
,	,	kIx,
anglická	anglický	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
gˈ	gˈ	k1gInSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
[	[	kIx(
<g/>
ˈ	ˈ	k1gNnSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prostředí	prostředí	k1gNnSc4
pracovní	pracovní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
pro	pro	k7c4
moderní	moderní	k2eAgInPc4d1
unixové	unixový	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
s	s	k7c7
instalovaným	instalovaný	k2eAgInSc7d1
X	X	kA
serverem	server	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
P.	P.	kA
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
postaveno	postavit	k5eAaPmNgNnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
nad	nad	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
(	(	kIx(
<g/>
The	The	k1gMnSc1
GIMP	GIMP	kA
Toolkit	Toolkit	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
napsána	napsat	k5eAaBmNgFnS,k5eAaPmNgFnS
pro	pro	k7c4
bitmapový	bitmapový	k2eAgInSc4d1
editor	editor	k1gInSc4
GIMP	GIMP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgFnPc4d3
součásti	součást	k1gFnPc4
projektu	projekt	k1gInSc2
GNOME	GNOME	kA
představují	představovat	k5eAaImIp3nP
okenní	okenní	k2eAgMnSc1d1
manažer	manažer	k1gMnSc1
Mutter	Mutter	k1gMnSc1
(	(	kIx(
<g/>
náhrada	náhrada	k1gFnSc1
za	za	k7c4
dřívější	dřívější	k2eAgInPc4d1
metacity	metacit	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
správce	správce	k1gMnSc1
sezení	sezení	k1gNnSc2
GDM	GDM	kA
<g/>
,	,	kIx,
démon	démon	k1gMnSc1
pracující	pracující	k1gMnSc1
s	s	k7c7
databází	databáze	k1gFnSc7
nastavení	nastavení	k1gNnSc2
prostředí	prostředí	k1gNnSc1
dconf	dconf	k1gInSc1
(	(	kIx(
<g/>
náhrada	náhrada	k1gFnSc1
za	za	k7c4
dřívější	dřívější	k2eAgInSc4d1
gConf	gConf	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
virtuální	virtuální	k2eAgInSc1d1
souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
GVFS	GVFS	kA
<g/>
,	,	kIx,
klíčenka	klíčenka	k1gFnSc1
GNOME	GNOME	kA
Keyring	Keyring	k1gInSc1
<g/>
,	,	kIx,
správa	správa	k1gFnSc1
databáze	databáze	k1gFnSc1
MIME	mim	k1gMnSc5
typů	typ	k1gInPc2
<g/>
,	,	kIx,
systém	systém	k1gInSc1
nápovědy	nápověda	k1gFnSc2
<g/>
,	,	kIx,
GNOME	GNOME	kA
Shell	Shell	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
verze	verze	k1gFnSc2
3.0	3.0	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bonobo	Bonoba	k1gFnSc5
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
již	již	k6eAd1
zavržený	zavržený	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
GNOME	GNOME	kA
vzniká	vznikat	k5eAaImIp3nS
množství	množství	k1gNnSc1
uživatelských	uživatelský	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
a	a	k8xC
vývojářských	vývojářský	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
vzhledem	vzhled	k1gInSc7
k	k	k7c3
zaměření	zaměření	k1gNnSc3
GNOME	GNOME	kA
primárně	primárně	k6eAd1
vyvíjeny	vyvíjet	k5eAaImNgFnP
pro	pro	k7c4
unixové	unixový	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
a	a	k8xC
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
ani	ani	k8xC
verze	verze	k1gFnSc1
pro	pro	k7c4
jiné	jiný	k2eAgInPc4d1
systémy	systém	k1gInPc4
neexistují	existovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
populární	populární	k2eAgFnPc1d1
linuxové	linuxový	k2eAgFnPc1d1
distribuce	distribuce	k1gFnPc1
postavené	postavený	k2eAgFnPc1d1
na	na	k7c6
desktopovém	desktopový	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
GNOME	GNOME	kA
obsahují	obsahovat	k5eAaImIp3nP
jen	jen	k9
malou	malý	k2eAgFnSc4d1
část	část	k1gFnSc4
z	z	k7c2
těchto	tento	k3xDgFnPc2
doprovodně	doprovodně	k6eAd1
vyvíjených	vyvíjený	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
hojně	hojně	k6eAd1
používán	používat	k5eAaImNgInS
i	i	k9
název	název	k1gInSc1
Gnome	Gnom	k1gInSc5
(	(	kIx(
<g/>
první	první	k4xOgFnSc6
písmeno	písmeno	k1gNnSc1
velké	velký	k2eAgInPc1d1
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgNnPc1d1
malá	malý	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
GNOME	GNOME	kA
</s>
<s>
GNOME	GNOME	kA
lze	lze	k6eAd1
chápat	chápat	k5eAaImF
ze	z	k7c2
dvou	dva	k4xCgInPc2
pohledů	pohled	k1gInPc2
<g/>
:	:	kIx,
jako	jako	k8xC,k8xS
prostředí	prostředí	k1gNnSc2
pracovní	pracovní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
a	a	k8xC
jako	jako	k9
vývojovou	vývojový	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
GNOME	GNOME	kA
je	být	k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
licencovaný	licencovaný	k2eAgInSc1d1
pod	pod	k7c7
GNU	gnu	k1gNnSc7
Lesser	Lessero	k1gNnPc2
General	General	k1gFnSc2
Public	publicum	k1gNnPc2
License	License	k1gFnSc1
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
(	(	kIx(
<g/>
angl.	angl.	k?
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
nuː	nuː	k?
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
vytvoření	vytvoření	k1gNnSc4
svobodné	svobodný	k2eAgFnSc2d1
vývojové	vývojový	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
a	a	k8xC
pracovního	pracovní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
s	s	k7c7
jednoduchým	jednoduchý	k2eAgNnSc7d1
intuitivním	intuitivní	k2eAgNnSc7d1
ovládáním	ovládání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoduchost	jednoduchost	k1gFnSc1
je	být	k5eAaImIp3nS
upřednostňována	upřednostňovat	k5eAaImNgFnS
před	před	k7c7
rozmanitostí	rozmanitost	k1gFnSc7
nastavení	nastavení	k1gNnSc4
(	(	kIx(
<g/>
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
však	však	k9
lze	lze	k6eAd1
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
dosáhnout	dosáhnout	k5eAaPmF
pomocí	pomocí	k7c2
pokročilých	pokročilý	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
příklad	příklad	k1gInSc1
gTweakUI	gTweakUI	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
pozadí	pozadí	k1gNnSc6
GNOME	GNOME	kA
</s>
<s>
Za	za	k7c7
vývojem	vývoj	k1gInSc7
stojí	stát	k5eAaImIp3nS
GNOME	GNOME	kA
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počátcích	počátek	k1gInPc6
byla	být	k5eAaImAgFnS
tahounem	tahoun	k1gInSc7
vývoje	vývoj	k1gInSc2
společnost	společnost	k1gFnSc1
Ximian	Ximiany	k1gInPc2
Inc	Inc	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
později	pozdě	k6eAd2
koupila	koupit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Novell	Novell	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnými	významný	k2eAgMnPc7d1
přispěvateli	přispěvatel	k1gMnPc7
projektu	projekt	k1gInSc2
GNOME	GNOME	kA
jsou	být	k5eAaImIp3nP
také	také	k9
velcí	velký	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
linuxových	linuxový	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
Red	Red	k1gFnSc1
Hat	hat	k0
<g/>
)	)	kIx)
<g/>
,	,	kIx,
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
vývojářů	vývojář	k1gMnPc2
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
nezávislých	závislý	k2eNgInPc2d1
nebo	nebo	k8xC
zaměstnaných	zaměstnaný	k2eAgInPc2d1
u	u	k7c2
jiných	jiný	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
rostoucí	rostoucí	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
dobrovolných	dobrovolný	k2eAgMnPc2d1
přispěvatelů	přispěvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Prostředí	prostředí	k1gNnSc1
pracovní	pracovní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
</s>
<s>
GNOME	GNOME	kA
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc1
aplikací	aplikace	k1gFnPc2
pokrývajících	pokrývající	k2eAgFnPc2d1
základní	základní	k2eAgFnSc4d1
potřeby	potřeba	k1gFnPc4
a	a	k8xC
očekávání	očekávání	k1gNnPc4
uživatelů	uživatel	k1gMnPc2
grafického	grafický	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
–	–	k?
správa	správa	k1gFnSc1
souborů	soubor	k1gInPc2
(	(	kIx(
<g/>
GNOME	GNOME	kA
Files	Files	k1gInSc1
<g/>
,	,	kIx,
kdysi	kdysi	k6eAd1
Nautilus	nautilus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kancelářské	kancelářský	k2eAgFnPc1d1
aplikace	aplikace	k1gFnPc1
(	(	kIx(
<g/>
GNOME	GNOME	kA
Office	Office	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
práce	práce	k1gFnSc1
s	s	k7c7
internetem	internet	k1gInSc7
(	(	kIx(
<g/>
GNOME	GNOME	kA
Web	web	k1gInSc1
<g/>
,	,	kIx,
Evolution	Evolution	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
práce	práce	k1gFnSc1
s	s	k7c7
dokumenty	dokument	k1gInPc7
(	(	kIx(
<g/>
Evince	Evinka	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
práce	práce	k1gFnPc4
s	s	k7c7
multimédii	multimédium	k1gNnPc7
(	(	kIx(
<g/>
Rhythmbox	Rhythmbox	k1gInSc1
<g/>
,	,	kIx,
GNOME	GNOME	kA
Videos	Videos	k1gInSc1
–	–	k?
předtím	předtím	k6eAd1
Totem	totem	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
aplikace	aplikace	k1gFnPc4
(	(	kIx(
<g/>
programy	program	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
přímo	přímo	k6eAd1
součástí	součást	k1gFnSc7
GNOME	GNOME	kA
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dalších	další	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
spadající	spadající	k2eAgFnSc1d1
pod	pod	k7c4
toto	tento	k3xDgNnSc4
prostředí	prostředí	k1gNnSc4
(	(	kIx(
<g/>
napsané	napsaný	k2eAgFnSc6d1
pod	pod	k7c7
některou	některý	k3yIgFnSc7
z	z	k7c2
GTK	GTK	kA
knihoven	knihovna	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
GIMP	GIMP	kA
(	(	kIx(
<g/>
bitmapový	bitmapový	k2eAgInSc1d1
editor	editor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Inkscape	Inkscap	k1gMnSc5
(	(	kIx(
<g/>
vektorový	vektorový	k2eAgInSc1d1
editor	editor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pidgin	Pidgin	k1gMnSc1
(	(	kIx(
<g/>
instant	instant	k?
messenger	messenger	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Audacious	Audacious	k1gMnSc1
Media	medium	k1gNnSc2
Player	Player	k1gInSc1
(	(	kIx(
<g/>
audiopřehrávač	audiopřehrávač	k1gInSc1
podobný	podobný	k2eAgInSc1d1
programu	program	k1gInSc2
Winamp	Winamp	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Liferea	Liferea	k1gFnSc1
(	(	kIx(
<g/>
správa	správa	k1gFnSc1
RSS	RSS	kA
<g/>
/	/	kIx~
<g/>
Atom	atom	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
GNOME	GNOME	kA
také	také	k9
podporuje	podporovat	k5eAaImIp3nS
různé	různý	k2eAgFnPc4d1
uživatelsky	uživatelsky	k6eAd1
přívětivé	přívětivý	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
(	(	kIx(
<g/>
náhledy	náhled	k1gInPc1
audio	audio	k2eAgInPc2d1
souborů	soubor	k1gInPc2
<g/>
,	,	kIx,
video	video	k1gNnSc1
souborů	soubor	k1gInPc2
<g/>
,	,	kIx,
dokumentů	dokument	k1gInPc2
a	a	k8xC
obrázků	obrázek	k1gInPc2
<g/>
,	,	kIx,
uživatelské	uživatelský	k2eAgNnSc4d1
připojování	připojování	k1gNnSc4
vzdálených	vzdálený	k2eAgInPc2d1
souborových	souborový	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
výborné	výborný	k2eAgFnPc1d1
možnosti	možnost	k1gFnPc1
uživatelských	uživatelský	k2eAgNnPc2d1
nastavení	nastavení	k1gNnPc2
a	a	k8xC
změn	změna	k1gFnPc2
motivů	motiv	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
moderní	moderní	k2eAgMnSc1d1
technologie	technologie	k1gFnPc4
(	(	kIx(
<g/>
vykreslování	vykreslování	k1gNnSc4
prostřednictvím	prostřednictvím	k7c2
SVG	SVG	kA
<g/>
,	,	kIx,
ukládání	ukládání	k1gNnSc3
konfigurace	konfigurace	k1gFnSc2
jako	jako	k8xC,k8xS
XML	XML	kA
dokumentů	dokument	k1gInPc2
<g/>
,	,	kIx,
propojení	propojení	k1gNnSc1
aplikací	aplikace	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
(	(	kIx(
<g/>
licence	licence	k1gFnSc2
GPL	GPL	kA
a	a	k8xC
LGPL	LGPL	kA
<g/>
)	)	kIx)
</s>
<s>
Multiplatformní	multiplatformní	k2eAgFnSc1d1
</s>
<s>
Jednoduché	jednoduchý	k2eAgNnSc1d1
ovládání	ovládání	k1gNnSc1
</s>
<s>
Podpora	podpora	k1gFnSc1
ovládání	ovládání	k1gNnSc2
pro	pro	k7c4
postižené	postižený	k2eAgMnPc4d1
(	(	kIx(
<g/>
framework	framework	k1gInSc1
přístupnosti	přístupnost	k1gFnSc2
ATK	ATK	kA
<g/>
,	,	kIx,
možnosti	možnost	k1gFnPc1
ovládání	ovládání	k1gNnSc1
klávesnicí	klávesnice	k1gFnSc7
<g/>
…	…	k?
<g/>
)	)	kIx)
</s>
<s>
Internacionalizace	internacionalizace	k1gFnSc1
(	(	kIx(
<g/>
47	#num#	k4
jazykových	jazykový	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
80	#num#	k4
%	%	kIx~
přeložených	přeložený	k2eAgInPc2d1
řetězců	řetězec	k1gInPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
15	#num#	k4
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Podpora	podpora	k1gFnSc1
standardů	standard	k1gInPc2
freedesktop	freedesktop	k1gInSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
a	a	k8xC
moderních	moderní	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
</s>
<s>
Podpora	podpora	k1gFnSc1
akcelerace	akcelerace	k1gFnSc2
grafiky	grafika	k1gFnSc2
a	a	k8xC
efektů	efekt	k1gInPc2
pracovního	pracovní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
XRandR	XRandR	k1gFnSc1
1.2	1.2	k4
(	(	kIx(
<g/>
jen	jen	k6eAd1
s	s	k7c7
grafickým	grafický	k2eAgInSc7d1
hardware	hardware	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
ovladač	ovladač	k1gInSc1
toto	tento	k3xDgNnSc4
umožňuje	umožňovat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Získání	získání	k1gNnSc1
GNOME	GNOME	kA
</s>
<s>
GNOME	GNOME	kA
můžete	moct	k5eAaImIp2nP
vyzkoušet	vyzkoušet	k5eAaPmF
pomocí	pomocí	k7c2
Live	Liv	k1gFnSc2
CD	CD	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
veškerý	veškerý	k3xTgInSc1
software	software	k1gInSc1
GNOME	GNOME	kA
na	na	k7c6
jednom	jeden	k4xCgInSc6
disku	disk	k1gInSc6
CD	CD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počítač	počítač	k1gInSc1
lze	lze	k6eAd1
nastartovat	nastartovat	k5eAaPmF
přímo	přímo	k6eAd1
z	z	k7c2
Live	Liv	k1gFnSc2
CD	CD	kA
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
musíte	muset	k5eAaImIp2nP
cokoliv	cokoliv	k3yInSc4
instalovat	instalovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraz	obraz	k1gInSc1
tohoto	tento	k3xDgInSc2
CD	CD	kA
si	se	k3xPyFc3
můžete	moct	k5eAaImIp2nP
stáhnout	stáhnout	k5eAaPmF
přímo	přímo	k6eAd1
na	na	k7c6
torrentové	torrentový	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
GNOME	GNOME	kA
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
chcete	chtít	k5eAaImIp2nP
GNOME	GNOME	kA
nainstalovat	nainstalovat	k5eAaPmF
do	do	k7c2
svého	svůj	k3xOyFgInSc2
počítače	počítač	k1gInSc2
<g/>
,	,	kIx,
doporučujeme	doporučovat	k5eAaImIp1nP
využít	využít	k5eAaPmF
oficiálních	oficiální	k2eAgInPc2d1
balíčků	balíček	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
nabízí	nabízet	k5eAaImIp3nS
vaše	váš	k3xOp2gFnSc1
distribuce	distribuce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
populárních	populární	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
GNOME	GNOME	kA
nabízí	nabízet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
si	se	k3xPyFc3
chtějí	chtít	k5eAaImIp3nP
sestavit	sestavit	k5eAaPmF
GNOME	GNOME	kA
ze	z	k7c2
zdrojových	zdrojový	k2eAgInPc2d1
kódů	kód	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
doporučuje	doporučovat	k5eAaImIp3nS
použít	použít	k5eAaPmF
jeden	jeden	k4xCgInSc4
z	z	k7c2
nástrojů	nástroj	k1gInPc2
pro	pro	k7c4
sestavování	sestavování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
GARNOME	GARNOME	kA
sestavuje	sestavovat	k5eAaImIp3nS
GNOME	GNOME	kA
z	z	k7c2
vydaných	vydaný	k2eAgInPc2d1
tarballů	tarball	k1gInPc2
a	a	k8xC
například	například	k6eAd1
pro	pro	k7c4
sestavení	sestavení	k1gNnSc4
GNOME	GNOME	kA
2.24	2.24	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgInPc1d1
použít	použít	k5eAaPmF
GARMONE	GARMONE	kA
2.24	2.24	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
Také	také	k9
existuje	existovat	k5eAaImIp3nS
nástroj	nástroj	k1gInSc1
JHBuild	JHBuild	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
navržen	navrhnout	k5eAaPmNgInS
pro	pro	k7c4
sestavování	sestavování	k1gNnSc4
nejnovější	nový	k2eAgFnSc2d3
verze	verze	k1gFnSc2
GNOME	GNOME	kA
přímo	přímo	k6eAd1
z	z	k7c2
SVN	SVN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
GNOME	GNOME	kA
2.24	2.24	k4
sestavíte	sestavit	k5eAaPmIp2nP
pomocí	pomocí	k7c2
nástroje	nástroj	k1gInSc2
JHBuild	JHBuilda	k1gFnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
použijete	použít	k5eAaPmIp2nP
modul	modul	k1gInSc4
gnome-	gnome-	k?
<g/>
2.24	2.24	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
sestavit	sestavit	k5eAaPmF
GNOME	GNOME	kA
ručně	ručně	k6eAd1
přímo	přímo	k6eAd1
z	z	k7c2
vydaných	vydaný	k2eAgInPc2d1
tarballů	tarball	k1gInPc2
<g/>
,	,	kIx,
důrazně	důrazně	k6eAd1
se	se	k3xPyFc4
doporučujeme	doporučovat	k5eAaImIp1nP
použít	použít	k5eAaPmF
jeden	jeden	k4xCgInSc4
z	z	k7c2
výše	vysoce	k6eAd2
uvedených	uvedený	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vývojová	vývojový	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
GNOME	GNOME	kA
</s>
<s>
Prostředí	prostředí	k1gNnSc1
GNOME	GNOME	kA
je	být	k5eAaImIp3nS
napsáno	napsat	k5eAaBmNgNnS,k5eAaPmNgNnS
v	v	k7c6
jazyce	jazyk	k1gInSc6
C	C	kA
pod	pod	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
GNOME	GNOME	kA
1	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
je	být	k5eAaImIp3nS
napsáno	napsat	k5eAaPmNgNnS,k5eAaBmNgNnS
pod	pod	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
existují	existovat	k5eAaImIp3nP
GTK	GTK	kA
knihovny	knihovna	k1gFnPc4
umožňující	umožňující	k2eAgNnSc4d1
psaní	psaní	k1gNnSc4
GTK	GTK	kA
aplikací	aplikace	k1gFnPc2
pod	pod	k7c7
C	C	kA
<g/>
++	++	k?
<g/>
,	,	kIx,
C	C	kA
<g/>
#	#	kIx~
(	(	kIx(
<g/>
více	hodně	k6eAd2
viz	vidět	k5eAaImRp2nS
také	také	k9
projekt	projekt	k1gInSc4
Mono	mono	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
jazyce	jazyk	k1gInSc6
Python	Python	k1gMnSc1
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s>
Díky	dík	k1gInPc1
licencí	licence	k1gFnPc2
LGPL	LGPL	kA
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
části	část	k1gFnPc4
GNOME	GNOME	kA
(	(	kIx(
<g/>
například	například	k6eAd1
knihovny	knihovna	k1gFnPc1
<g/>
)	)	kIx)
využívány	využíván	k2eAgFnPc1d1
i	i	k8xC
v	v	k7c6
aplikacích	aplikace	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nejsou	být	k5eNaImIp3nP
pod	pod	k7c7
stejnou	stejný	k2eAgFnSc7d1
licencí	licence	k1gFnSc7
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
kódu	kód	k1gInSc2
pod	pod	k7c7
licencí	licence	k1gFnSc7
GPL	GPL	kA
<g/>
)	)	kIx)
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
upřednostňováno	upřednostňovat	k5eAaImNgNnS
vytváření	vytváření	k1gNnSc1
komerčních	komerční	k2eAgInPc2d1
<g/>
/	/	kIx~
<g/>
uzavřených	uzavřený	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
pod	pod	k7c7
GTK	GTK	kA
<g/>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
GNOME	GNOME	kA
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
GTK	GTK	kA
<g/>
+	+	kIx~
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
například	například	k6eAd1
společnost	společnost	k1gFnSc1
Adobe	Adobe	kA
Systems	Systems	k1gInSc1
(	(	kIx(
<g/>
Acrobat	Acrobat	k1gMnSc1
Reader	Reader	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
VMware	VMwar	k1gMnSc5
(	(	kIx(
<g/>
VMware	VMwar	k1gMnSc5
Player	Playero	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
RealNetworks	RealNetworks	k1gInSc1
(	(	kIx(
<g/>
RealPlayer	RealPlayer	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Platforma	platforma	k1gFnSc1
GNOME	GNOME	kA
Mobile	mobile	k1gNnSc1
</s>
<s>
GNOME	GNOME	kA
Mobile	mobile	k1gNnSc1
spojuje	spojovat	k5eAaImIp3nS
běžné	běžný	k2eAgFnPc4d1
části	část	k1gFnPc4
pracovní	pracovní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
dohromady	dohromady	k6eAd1
za	za	k7c7
účelem	účel	k1gInSc7
poskytnout	poskytnout	k5eAaPmF
distributorům	distributor	k1gMnPc3
a	a	k8xC
výrobcům	výrobce	k1gMnPc3
kapesních	kapesní	k2eAgInPc2d1
počítačů	počítač	k1gInPc2
základní	základní	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
bohatých	bohatý	k2eAgNnPc2d1
programovacích	programovací	k2eAgNnPc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byla	být	k5eAaImAgFnS
platforma	platforma	k1gFnSc1
zahrnuta	zahrnout	k5eAaPmNgFnS
do	do	k7c2
vydání	vydání	k1gNnSc2
GNOME	GNOME	kA
2.24	2.24	k4
<g/>
.0	.0	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc4
GNOME	GNOME	kA
Mobile	mobile	k1gNnSc1
je	být	k5eAaImIp3nS
pokračováním	pokračování	k1gNnSc7
GNOME	GNOME	kA
Mobile	mobile	k1gNnSc3
&	&	k?
Embedded	Embedded	k1gInSc1
Initiative	Initiativ	k1gInSc5
(	(	kIx(
<g/>
GMAE	GMAE	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Platforma	platforma	k1gFnSc1
GNOME	GNOME	kA
Mobile	mobile	k1gNnSc1
je	být	k5eAaImIp3nS
technologickým	technologický	k2eAgNnSc7d1
srdcem	srdce	k1gNnSc7
mnoha	mnoho	k4c3
na	na	k7c6
Linuxu	linux	k1gInSc6
založených	založený	k2eAgNnPc2d1
mobilních	mobilní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
jako	jako	k8xS,k8xC
Maemo	Maema	k1gFnSc5
<g/>
,	,	kIx,
Access	Access	k1gInSc1
Linux	Linux	kA
Platform	Platform	k1gInSc1
<g/>
,	,	kIx,
platforma	platforma	k1gFnSc1
LiMo	limo	k1gNnSc4
<g/>
,	,	kIx,
Ubuntu	Ubunta	k1gFnSc4
Mobile	mobile	k1gNnSc2
<g/>
,	,	kIx,
Moblin	Moblina	k1gFnPc2
nebo	nebo	k8xC
Poky	Poka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
infrastruktura	infrastruktura	k1gFnSc1
</s>
<s>
GLib	GLib	k1gInSc1
–	–	k?
představuje	představovat	k5eAaImIp3nS
základní	základní	k2eAgInSc4d1
kámen	kámen	k1gInSc4
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
přenosných	přenosný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
a	a	k8xC
knihoven	knihovna	k1gFnPc2
napsaných	napsaný	k2eAgFnPc2d1
v	v	k7c6
jazyce	jazyk	k1gInSc6
C.	C.	kA
Poskytuje	poskytovat	k5eAaImIp3nS
základní	základní	k2eAgInSc4d1
systém	systém	k1gInSc4
objektů	objekt	k1gInPc2
používaný	používaný	k2eAgInSc1d1
v	v	k7c6
GNOME	GNOME	kA
<g/>
,	,	kIx,
implementuje	implementovat	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc4d1
smyčku	smyčka	k1gFnSc4
a	a	k8xC
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
sadu	sada	k1gFnSc4
užitečných	užitečný	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
řetězci	řetězec	k1gInPc7
a	a	k8xC
běžnými	běžný	k2eAgFnPc7d1
datovými	datový	k2eAgFnPc7d1
strukturami	struktura	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
DBus	DBus	k1gInSc1
–	–	k?
rozhraní	rozhraní	k1gNnSc2
pro	pro	k7c4
zprávy	zpráva	k1gFnPc4
poskytující	poskytující	k2eAgFnPc4d1
aplikacím	aplikace	k1gFnPc3
jednoduchou	jednoduchý	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
a	a	k8xC
koordinaci	koordinace	k1gFnSc4
procesu	proces	k1gInSc2
po	po	k7c4
dobu	doba	k1gFnSc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
běhu	běh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Infrastruktura	infrastruktura	k1gFnSc1
systému	systém	k1gInSc2
</s>
<s>
BlueZ	BlueZ	k?
–	–	k?
modulární	modulární	k2eAgFnSc4d1
<g/>
,	,	kIx,
kompletní	kompletní	k2eAgFnSc4d1
a	a	k8xC
standardy	standard	k1gInPc4
podložený	podložený	k2eAgInSc4d1
Bluetooth	Bluetooth	k1gInSc4
stack	stack	k6eAd1
obsahující	obsahující	k2eAgFnSc1d1
několik	několik	k4yIc4
grafických	grafický	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
pro	pro	k7c4
GNOME	GNOME	kA
<g/>
;	;	kIx,
</s>
<s>
Evolution	Evolution	k1gInSc1
Data	datum	k1gNnSc2
Server	server	k1gInSc1
(	(	kIx(
<g/>
port	port	k1gInSc1
pro	pro	k7c4
DBus	DBus	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
obsluhuje	obsluhovat	k5eAaImIp3nS
přístup	přístup	k1gInSc4
uživatele	uživatel	k1gMnSc2
ke	k	k7c3
kalendáři	kalendář	k1gInSc3
<g/>
,	,	kIx,
úkolům	úkol	k1gInPc3
<g/>
,	,	kIx,
adresáři	adresář	k1gInSc6
kontaktů	kontakt	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
lokálně	lokálně	k6eAd1
<g/>
,	,	kIx,
tak	tak	k9
prostřednictvím	prostřednictvím	k7c2
populárních	populární	k2eAgInPc2d1
síťových	síťový	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
<g/>
;	;	kIx,
</s>
<s>
Telepathy	Telepath	k1gMnPc4
–	–	k?
unifikovaný	unifikovaný	k2eAgInSc1d1
framework	framework	k1gInSc1
umožňující	umožňující	k2eAgMnSc1d1
uživatelským	uživatelský	k2eAgFnPc3d1
aplikacím	aplikace	k1gFnPc3
využívat	využívat	k5eAaPmF,k5eAaImF
rychlé	rychlý	k2eAgNnSc4d1
zasílání	zasílání	k1gNnSc4
zpráv	zpráva	k1gFnPc2
<g/>
,	,	kIx,
hlasové	hlasový	k2eAgNnSc4d1
a	a	k8xC
video	video	k1gNnSc4
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
VoIP	VoIP	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
sokety	soketa	k1gFnSc2
point-to-point	point-to-point	k1gMnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Tubes	Tubes	k1gMnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
Avahi	Avah	k1gFnSc3
–	–	k?
multicastový	multicastový	k2eAgMnSc1d1
DNS	DNS	kA
stack	stack	k1gInSc4
poskytující	poskytující	k2eAgFnSc4d1
službu	služba	k1gFnSc4
Zeroconf	Zeroconf	k1gInSc4
v	v	k7c6
lokální	lokální	k2eAgFnSc6d1
síti	síť	k1gFnSc6
<g/>
;	;	kIx,
</s>
<s>
GStreamer	GStreamer	k1gInSc1
–	–	k?
bohatý	bohatý	k2eAgInSc1d1
multimediální	multimediální	k2eAgInSc1d1
framework	framework	k1gInSc1
umožňující	umožňující	k2eAgFnSc2d1
drobnosti	drobnost	k1gFnSc2
jako	jako	k8xC,k8xS
přehrávání	přehrávání	k1gNnSc4
audia	audium	k1gNnSc2
a	a	k8xC
videa	video	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
proudové	proudový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
jako	jako	k8xC,k8xS
záznam	záznam	k1gInSc4
<g/>
,	,	kIx,
mixování	mixování	k1gNnSc4
a	a	k8xC
nelineární	lineární	k2eNgFnSc4d1
editaci	editace	k1gFnSc4
<g/>
;	;	kIx,
</s>
<s>
SQLite	SQLit	k1gMnSc5
–	–	k?
jednoduchá	jednoduchý	k2eAgFnSc1d1
a	a	k8xC
výkonná	výkonný	k2eAgFnSc1d1
transakční	transakční	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
typu	typ	k1gInSc2
SQL	SQL	kA
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
lze	lze	k6eAd1
jednoduše	jednoduše	k6eAd1
využít	využít	k5eAaPmF
v	v	k7c6
aplikacích	aplikace	k1gFnPc6
<g/>
;	;	kIx,
</s>
<s>
GConf	GConf	k1gInSc1
–	–	k?
umožňuje	umožňovat	k5eAaImIp3nS
ukládat	ukládat	k5eAaImF
a	a	k8xC
získávat	získávat	k5eAaImF
nastavení	nastavení	k1gNnSc4
konfigurace	konfigurace	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
Matchbox	Matchbox	k1gInSc4
–	–	k?
okenní	okenní	k2eAgMnSc1d1
správce	správce	k1gMnSc1
pro	pro	k7c4
prostředí	prostředí	k1gNnSc4
běžící	běžící	k2eAgFnSc2d1
pod	pod	k7c7
X	X	kA
<g/>
11	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
omezeným	omezený	k2eAgInPc3d1
zdrojům	zdroj	k1gInPc3
nebo	nebo	k8xC
rozlišení	rozlišení	k1gNnSc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
kapesní	kapesní	k2eAgInSc1d1
počítač	počítač	k1gInSc1
<g/>
,	,	kIx,
set-top	set-top	k1gInSc1
box	box	k1gInSc1
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
,	,	kIx,
nepoužívá	používat	k5eNaImIp3nS
klasický	klasický	k2eAgInSc4d1
koncept	koncept	k1gInSc4
pracovní	pracovní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
gio	gio	k?
a	a	k8xC
GVFS	GVFS	kA
–	–	k?
poskytují	poskytovat	k5eAaImIp3nP
API	API	kA
virtuálního	virtuální	k2eAgInSc2d1
systému	systém	k1gInSc2
podporující	podporující	k2eAgFnSc4d1
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
služeb	služba	k1gFnPc2
jako	jako	k8xS,k8xC
FTP	FTP	kA
<g/>
,	,	kIx,
SFTP	SFTP	kA
(	(	kIx(
<g/>
SSH	SSH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
WebDAV	WebDAV	k1gMnSc1
<g/>
,	,	kIx,
NFS	NFS	kA
a	a	k8xC
SMB	SMB	kA
<g/>
/	/	kIx~
<g/>
CIFS	CIFS	kA
<g/>
;	;	kIx,
</s>
<s>
Uživatelské	uživatelský	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
</s>
<s>
GTK	GTK	kA
<g/>
+	+	kIx~
–	–	k?
funkčně	funkčně	k6eAd1
bohatý	bohatý	k2eAgInSc4d1
multiplatformní	multiplatformní	k2eAgInSc4d1
soubor	soubor	k1gInSc4
nástrojů	nástroj	k1gInPc2
(	(	kIx(
<g/>
toolkit	toolkit	k1gMnSc1
[	[	kIx(
<g/>
ˈ	ˈ	k1gMnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
pro	pro	k7c4
vytváření	vytváření	k1gNnSc4
grafických	grafický	k2eAgFnPc2d1
uživatelských	uživatelský	k2eAgFnPc2d1
prostředí	prostředí	k1gNnSc2
se	s	k7c7
snadno	snadno	k6eAd1
použitelným	použitelný	k2eAgInSc7d1
API	API	kA
<g/>
.	.	kIx.
</s>
<s>
Pango	Pango	k6eAd1
–	–	k?
poskytuje	poskytovat	k5eAaImIp3nS
pro	pro	k7c4
GTK	GTK	kA
<g/>
+	+	kIx~
služby	služba	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
sestavování	sestavování	k1gNnSc6
a	a	k8xC
vykreslování	vykreslování	k1gNnSc6
textů	text	k1gInPc2
s	s	k7c7
důrazem	důraz	k1gInSc7
na	na	k7c4
internacionalizaci	internacionalizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
ATK	ATK	kA
(	(	kIx(
<g/>
soubor	soubor	k1gInSc1
nástrojů	nástroj	k1gInPc2
zpřístupnění	zpřístupnění	k1gNnSc2
<g/>
)	)	kIx)
–	–	k?
zajišťuje	zajišťovat	k5eAaImIp3nS
základní	základní	k2eAgNnSc1d1
zpřístupnění	zpřístupnění	k1gNnSc1
všem	všecek	k3xTgMnPc3
widgetům	widget	k1gMnPc3
GTK	GTK	kA
<g/>
+	+	kIx~
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
nástrojům	nástroj	k1gInPc3
pro	pro	k7c4
zpřístupnění	zpřístupnění	k1gNnSc4
plný	plný	k2eAgInSc1d1
přístup	přístup	k1gInSc1
k	k	k7c3
prohlížení	prohlížení	k1gNnSc3
a	a	k8xC
správě	správa	k1gFnSc3
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
cairo	cairo	k1gNnSc1
–	–	k?
2D	2D	k4
vektorová	vektorový	k2eAgFnSc1d1
grafická	grafický	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
navržená	navržený	k2eAgFnSc1d1
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
konzistentního	konzistentní	k2eAgInSc2d1
výstupu	výstup	k1gInSc2
na	na	k7c6
všech	všecek	k3xTgNnPc6
médiích	médium	k1gNnPc6
<g/>
,	,	kIx,
využívající	využívající	k2eAgFnPc4d1
hardwarové	hardwarový	k2eAgFnPc4d1
akcelerace	akcelerace	k1gFnPc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
dostupná	dostupný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cairo	Cairo	k1gNnSc1
(	(	kIx(
<g/>
[	[	kIx(
<g/>
ˈ	ˈ	k6eAd1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
poskytuje	poskytovat	k5eAaImIp3nS
jednoduché	jednoduchý	k2eAgNnSc4d1
API	API	kA
podobné	podobný	k2eAgFnPc1d1
PostScriptu	PostScript	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Vazby	vazba	k1gFnPc1
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
</s>
<s>
C	C	kA
</s>
<s>
C	C	kA
<g/>
++	++	k?
</s>
<s>
Python	Python	k1gMnSc1
</s>
<s>
Všechny	všechen	k3xTgFnPc4
knihovny	knihovna	k1gFnPc4
platformy	platforma	k1gFnSc2
GNOME	GNOME	kA
Mobile	mobile	k1gNnSc1
jsou	být	k5eAaImIp3nP
dostupné	dostupný	k2eAgInPc1d1
přes	přes	k7c4
nativní	nativní	k2eAgInPc4d1
C	C	kA
API	API	kA
<g/>
,	,	kIx,
či	či	k8xC
přes	přes	k7c4
vazby	vazba	k1gFnPc4
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
k	k	k7c3
dispozici	dispozice	k1gFnSc3
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
plný	plný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
knihoven	knihovna	k1gFnPc2
spolu	spolu	k6eAd1
s	s	k7c7
výrazy	výraz	k1gInPc7
známými	známý	k2eAgInPc7d1
vývojářům	vývojář	k1gMnPc3
jiných	jiný	k2eAgInPc2d1
vysokoúrovňových	vysokoúrovňový	k2eAgInPc2d1
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vývojový	vývojový	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
GNOME	GNOME	kA
</s>
<s>
Vývojový	vývojový	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
GNOME	GNOME	kA
je	být	k5eAaImIp3nS
dlouhý	dlouhý	k2eAgInSc1d1
zhruba	zhruba	k6eAd1
šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
hlavními	hlavní	k2eAgNnPc7d1
vydáními	vydání	k1gNnPc7
jsou	být	k5eAaImIp3nP
vytvářeny	vytvářen	k2eAgFnPc4d1
opravy	oprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
větve	větev	k1gFnPc4
<g/>
,	,	kIx,
stabilní	stabilní	k2eAgFnSc4d1
a	a	k8xC
vývojovou	vývojový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stabilní	stabilní	k2eAgFnPc4d1
verze	verze	k1gFnPc4
jsou	být	k5eAaImIp3nP
určeny	určit	k5eAaPmNgFnP
pro	pro	k7c4
nasazení	nasazení	k1gNnSc4
pro	pro	k7c4
běžné	běžný	k2eAgMnPc4d1
uživatele	uživatel	k1gMnPc4
<g/>
,	,	kIx,
vývojová	vývojový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
vývoje	vývoj	k1gInSc2
a	a	k8xC
implementaci	implementace	k1gFnSc4
nových	nový	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
GNOME	GNOME	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgInPc2
údajů	údaj	k1gInPc2
<g/>
:	:	kIx,
X.Y.Z	X.Y.Z	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
X	X	kA
je	být	k5eAaImIp3nS
řada	řada	k1gFnSc1
(	(	kIx(
<g/>
označuje	označovat	k5eAaImIp3nS
<g/>
,	,	kIx,
pod	pod	k7c7
jakou	jaký	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
verzí	verze	k1gFnSc7
GTK	GTK	kA
je	být	k5eAaImIp3nS
GNOME	GNOME	kA
napsáno	napsat	k5eAaBmNgNnS,k5eAaPmNgNnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aktuálně	aktuálně	k6eAd1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Položka	položka	k1gFnSc1
Y	Y	kA
označuje	označovat	k5eAaImIp3nS
verzi	verze	k1gFnSc4
GNOME	GNOME	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
sudé	sudý	k2eAgNnSc1d1
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
stabilní	stabilní	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
liché	lichý	k2eAgInPc4d1
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
vývojovou	vývojový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
číslo	číslo	k1gNnSc1
Z	Z	kA
je	být	k5eAaImIp3nS
„	„	k?
<g/>
číslo	číslo	k1gNnSc4
opravy	oprava	k1gFnSc2
<g/>
“	“	k?
dané	daný	k2eAgFnPc1d1
(	(	kIx(
<g/>
stabilní	stabilní	k2eAgFnPc1d1
<g/>
/	/	kIx~
<g/>
vývojové	vývojový	k2eAgFnPc1d1
<g/>
)	)	kIx)
verze	verze	k1gFnPc1
<g/>
,	,	kIx,
opravy	oprava	k1gFnPc1
a	a	k8xC
úpravy	úprava	k1gFnPc1
zahrnuty	zahrnout	k5eAaPmNgFnP
do	do	k7c2
posledního	poslední	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
vydání	vydání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
verzí	verze	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
1.4	1.4	k4
<g/>
.0	.0	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
stabilní	stabilní	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
GNOME	GNOME	kA
pod	pod	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
1	#num#	k4
(	(	kIx(
<g/>
celkově	celkově	k6eAd1
5	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
2.12	2.12	k4
<g/>
.0	.0	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
stabilní	stabilní	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
GNOME	GNOME	kA
pod	pod	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
2	#num#	k4
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
13	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
2.12	2.12	k4
<g/>
.1	.1	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
stabilní	stabilní	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
GNOME	GNOME	kA
pod	pod	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
2	#num#	k4
<g/>
,	,	kIx,
první	první	k4xOgFnPc1
revize	revize	k1gFnPc1
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
13	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
2.13	2.13	k4
<g/>
.0	.0	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
vývojové	vývojový	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
GNOME	GNOME	kA
pod	pod	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
2	#num#	k4
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
13	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
2.13	2.13	k4
<g/>
.3	.3	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
vývojové	vývojový	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
GNOME	GNOME	kA
pod	pod	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
2	#num#	k4
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnPc1
revize	revize	k1gFnPc1
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
13	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Vývojový	vývojový	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
verze	verze	k1gFnSc2
je	být	k5eAaImIp3nS
zahájen	zahájit	k5eAaPmNgInS
při	při	k7c6
vydání	vydání	k1gNnSc6
předchozí	předchozí	k2eAgFnSc2d1
stabilní	stabilní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
stabilní	stabilní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
je	být	k5eAaImIp3nS
oddělena	oddělen	k2eAgFnSc1d1
větev	větev	k1gFnSc1
a	a	k8xC
prohlášena	prohlášen	k2eAgFnSc1d1
za	za	k7c4
vývojovou	vývojový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ní	on	k3xPp3gFnSc6
jsou	být	k5eAaImIp3nP
prováděny	prováděn	k2eAgFnPc1d1
zásadní	zásadní	k2eAgFnPc1d1
i	i	k8xC
menší	malý	k2eAgFnPc1d2
změny	změna	k1gFnPc1
<g/>
,	,	kIx,
reorganizace	reorganizace	k1gFnSc1
a	a	k8xC
přidávání	přidávání	k1gNnSc1
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
určité	určitý	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
vývoj	vývoj	k1gInSc1
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
vývojovým	vývojový	k2eAgInSc7d1
kalendářem	kalendář	k1gInSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zmraženo	zmražen	k2eAgNnSc4d1
API	API	kA
<g/>
/	/	kIx~
<g/>
ABI	ABI	kA
(	(	kIx(
<g/>
laicky	laicky	k6eAd1
řečeno	řečen	k2eAgNnSc1d1
<g/>
:	:	kIx,
je	být	k5eAaImIp3nS
zastaven	zastavit	k5eAaPmNgInS
vývoj	vývoj	k1gInSc1
<g/>
/	/	kIx~
<g/>
úprava	úprava	k1gFnSc1
programátorského	programátorský	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
UI	UI	kA
(	(	kIx(
<g/>
zastaven	zastaven	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
<g/>
/	/	kIx~
<g/>
úpravy	úprava	k1gFnPc1
uživatelského	uživatelský	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
do	do	k7c2
dalšího	další	k2eAgNnSc2d1
vydání	vydání	k1gNnSc2
stabilní	stabilní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
jsou	být	k5eAaImIp3nP
opravovány	opravován	k2eAgFnPc1d1
chyby	chyba	k1gFnPc1
a	a	k8xC
problémy	problém	k1gInPc1
stávající	stávající	k2eAgFnSc2d1
vývojové	vývojový	k2eAgFnSc2d1
verze	verze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
je	být	k5eAaImIp3nS
vývojová	vývojový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
vydána	vydán	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
stabilní	stabilní	k2eAgFnSc1d1
a	a	k8xC
cyklus	cyklus	k1gInSc1
se	se	k3xPyFc4
opakuje	opakovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Používání	používání	k1gNnSc1
vývojových	vývojový	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
GNOME	GNOME	kA
a	a	k8xC
práci	práce	k1gFnSc6
s	s	k7c7
nimi	on	k3xPp3gNnPc7
napomáhají	napomáhat	k5eAaImIp3nP,k5eAaBmIp3nP
nástroje	nástroj	k1gInPc1
Garnome	Garnom	k1gInSc5
a	a	k8xC
jhbuild	jhbuildo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
GNOME	GNOME	kA
3.0	3.0	k4
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
dlouhodobou	dlouhodobý	k2eAgFnSc7d1
diskuzí	diskuze	k1gFnSc7
vývojářů	vývojář	k1gMnPc2
(	(	kIx(
<g/>
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
eskalovala	eskalovat	k5eAaImAgFnS
během	během	k7c2
konference	konference	k1gFnSc2
GUADEC	GUADEC	kA
2008	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
přechodem	přechod	k1gInSc7
na	na	k7c6
GTK	GTK	kA
<g/>
+	+	kIx~
verze	verze	k1gFnSc1
3.0	3.0	k4
byla	být	k5eAaImAgFnS
nalezena	nalezen	k2eAgFnSc1d1
shoda	shoda	k1gFnSc1
v	v	k7c6
otázce	otázka	k1gFnSc6
dalších	další	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
a	a	k8xC
GNOME	GNOME	kA
2.32	2.32	k4
bylo	být	k5eAaImAgNnS
přečíslováno	přečíslovat	k5eAaPmNgNnS
na	na	k7c4
3.0	3.0	k4
<g/>
,	,	kIx,
princip	princip	k1gInSc1
rozlišování	rozlišování	k1gNnSc1
vývojových	vývojový	k2eAgNnPc2d1
a	a	k8xC
stabilních	stabilní	k2eAgNnPc2d1
vydání	vydání	k1gNnPc2
zůstal	zůstat	k5eAaPmAgInS
stejný	stejný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc4
proto	proto	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
směřuje	směřovat	k5eAaImIp3nS
k	k	k7c3
pročištění	pročištění	k1gNnSc3
celé	celý	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
od	od	k7c2
zastaralých	zastaralý	k2eAgFnPc2d1
součástí	součást	k1gFnPc2
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
libgnome	libgnom	k1gInSc5
<g/>
,	,	kIx,
libgnomeprint	libgnomeprinto	k1gNnPc2
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
dotkne	dotknout	k5eAaPmIp3nS
a	a	k8xC
již	již	k6eAd1
dotýká	dotýkat	k5eAaImIp3nS
velké	velký	k2eAgFnPc4d1
části	část	k1gFnPc4
GNOME	GNOME	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diskuze	diskuze	k1gFnSc1
ohledně	ohledně	k7c2
konkrétních	konkrétní	k2eAgFnPc2d1
změn	změna	k1gFnPc2
je	být	k5eAaImIp3nS
však	však	k9
stále	stále	k6eAd1
otevřena	otevřít	k5eAaPmNgFnS
a	a	k8xC
nelze	lze	k6eNd1
vyloučit	vyloučit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
výsledek	výsledek	k1gInSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
další	další	k2eAgFnSc2d1
verze	verze	k1gFnSc2
bude	být	k5eAaImBp3nS
od	od	k7c2
současného	současný	k2eAgInSc2d1
plánu	plán	k1gInSc2
ještě	ještě	k9
o	o	k7c4
něco	něco	k3yInSc4
odlišnější	odlišný	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
by	by	kYmCp3nS
i	i	k9
tak	tak	k6eAd1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
nadále	nadále	k6eAd1
především	především	k9
evoluční	evoluční	k2eAgFnSc1d1
(	(	kIx(
<g/>
se	s	k7c7
zachováním	zachování	k1gNnSc7
co	co	k9
největší	veliký	k2eAgFnSc2d3
zpětné	zpětný	k2eAgFnSc2d1
kompatibility	kompatibilita	k1gFnSc2
pro	pro	k7c4
aplikace	aplikace	k1gFnPc4
napsané	napsaný	k2eAgFnPc4d1
pod	pod	k7c7
GTK	GTK	kA
<g/>
+	+	kIx~
verze	verze	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
<g/>
)	)	kIx)
spíše	spíše	k9
než	než	k8xS
revoluční	revoluční	k2eAgFnPc1d1
<g/>
,	,	kIx,
ač	ač	k8xS
v	v	k7c6
nové	nový	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
je	být	k5eAaImIp3nS
použit	použit	k2eAgInSc1d1
„	„	k?
<g/>
koncepčně	koncepčně	k6eAd1
odlišný	odlišný	k2eAgMnSc1d1
<g/>
“	“	k?
GNOME	GNOME	kA
Shell	Shell	k1gInSc1
namísto	namísto	k7c2
klasického	klasický	k2eAgInSc2d1
panelu	panel	k1gInSc2
a	a	k8xC
správce	správce	k1gMnSc1
oken	okno	k1gNnPc2
nebo	nebo	k8xC
nový	nový	k2eAgInSc4d1
způsob	způsob	k1gInSc4
přístupu	přístup	k1gInSc2
k	k	k7c3
dokumentům	dokument	k1gInPc3
<g/>
,	,	kIx,
GNOME	GNOME	kA
Zeitgeist	Zeitgeist	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
GNOME	GNOME	kA
3	#num#	k4
opustilo	opustit	k5eAaPmAgNnS
tradiční	tradiční	k2eAgInSc4d1
koncept	koncept	k1gInSc4
desktopu	desktop	k1gInSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
GNOME	GNOME	kA
Shellu	Shell	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
krok	krok	k1gInSc4
vyvolal	vyvolat	k5eAaPmAgInS
smíšené	smíšený	k2eAgFnPc4d1
reakce	reakce	k1gFnPc4
od	od	k7c2
uživatelské	uživatelský	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
nových	nový	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
MATE	mást	k5eAaImIp3nS
<g/>
,	,	kIx,
forkem	forkem	k6eAd1
GNOME	GNOME	kA
verze	verze	k1gFnSc1
2.32	2.32	k4
vystupujícím	vystupující	k2eAgFnPc3d1
pod	pod	k7c7
novým	nový	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
MATE	mást	k5eAaImIp3nS
si	se	k3xPyFc3
dalo	dát	k5eAaPmAgNnS
za	za	k7c4
cíl	cíl	k1gInSc4
zachovat	zachovat	k5eAaPmF
tradiční	tradiční	k2eAgInSc4d1
GNOME	GNOME	kA
2	#num#	k4
rozhraní	rozhraní	k1gNnSc2
při	při	k7c6
zachování	zachování	k1gNnSc6
kompatibility	kompatibilita	k1gFnSc2
s	s	k7c7
GNOME	GNOME	kA
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinou	jiný	k2eAgFnSc7d1
alternativou	alternativa	k1gFnSc7
je	být	k5eAaImIp3nS
prostředí	prostředí	k1gNnSc1
Cinnamon	Cinnamona	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nabízí	nabízet	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc4d1
variaci	variace	k1gFnSc4
k	k	k7c3
prostředí	prostředí	k1gNnSc3
GNOME	GNOME	kA
Shell	Shell	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
GNOME	GNOME	kA
3.8	3.8	k4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
kromě	kromě	k7c2
rozhraní	rozhraní	k1gNnSc2
GNOME	GNOME	kA
Shell	Shell	k1gMnSc1
<g/>
,	,	kIx,
nový	nový	k2eAgMnSc1d1
„	„	k?
<g/>
klasický	klasický	k2eAgInSc1d1
režim	režim	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
obnovuje	obnovovat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
známá	známý	k2eAgNnPc4d1
menu	menu	k1gNnPc4
Aplikace	aplikace	k1gFnSc2
<g/>
,	,	kIx,
Místa	místo	k1gNnSc2
nebo	nebo	k8xC
přepínač	přepínač	k1gInSc1
ploch	plocha	k1gFnPc2
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
obrazovky	obrazovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
GNOME	GNOME	kA
</s>
<s>
GNOME	GNOME	kA
je	být	k5eAaImIp3nS
kritizováno	kritizovat	k5eAaImNgNnS
pro	pro	k7c4
přílišné	přílišný	k2eAgNnSc4d1
zjednodušování	zjednodušování	k1gNnSc4
a	a	k8xC
záměrné	záměrný	k2eAgNnSc4d1
vypouštění	vypouštění	k1gNnSc4
pokročilých	pokročilý	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
v	v	k7c6
zájmu	zájem	k1gInSc6
o	o	k7c4
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
nejjednodušší	jednoduchý	k2eAgFnSc1d3
a	a	k8xC
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
nejméně	málo	k6eAd3
matoucí	matoucí	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
pro	pro	k7c4
nezkušené	zkušený	k2eNgMnPc4d1
uživatele	uživatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhraní	rozhraní	k1gNnSc1
GTK	GTK	kA
aplikací	aplikace	k1gFnPc2
je	být	k5eAaImIp3nS
také	také	k9
kritizováno	kritizován	k2eAgNnSc1d1
za	za	k7c4
některé	některý	k3yIgFnPc4
funkce	funkce	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
otevírací	otevírací	k2eAgInSc1d1
<g/>
/	/	kIx~
<g/>
ukládací	ukládací	k2eAgNnSc1d1
dialogové	dialogový	k2eAgNnSc1d1
okno	okno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Veřejně	veřejně	k6eAd1
známo	znám	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
GNOME	GNOME	kA
2	#num#	k4
(	(	kIx(
<g/>
vytvořené	vytvořený	k2eAgFnSc2d1
pod	pod	k7c7
knihovnou	knihovna	k1gFnSc7
GTK	GTK	kA
<g/>
+	+	kIx~
2	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
výrazně	výrazně	k6eAd1
náročnější	náročný	k2eAgMnSc1d2
a	a	k8xC
pomalejší	pomalý	k2eAgMnSc1d2
než	než	k8xS
verze	verze	k1gFnPc1
předchozí	předchozí	k2eAgFnPc1d1
<g/>
,	,	kIx,
aplikace	aplikace	k1gFnPc1
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
startovaly	startovat	k5eAaBmAgFnP
neúměrně	úměrně	k6eNd1
pomalu	pomalu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
dalších	další	k2eAgInPc2d1
stabilních	stabilní	k2eAgInPc2d1
vydání	vydání	k1gNnSc2
však	však	k9
byly	být	k5eAaImAgInP
učiněny	učiněn	k2eAgInPc4d1
kroky	krok	k1gInPc4
ke	k	k7c3
zvrácení	zvrácení	k1gNnSc3
tohoto	tento	k3xDgInSc2
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
výkon	výkon	k1gInSc1
celého	celý	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
se	se	k3xPyFc4
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
zrychlil	zrychlit	k5eAaPmAgMnS
a	a	k8xC
náročnost	náročnost	k1gFnSc1
se	se	k3xPyFc4
snížila	snížit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
snahy	snaha	k1gFnPc4
také	také	k9
podporuje	podporovat	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
GNOME	GNOME	kA
<g/>
/	/	kIx~
<g/>
GTK	GTK	kA
<g/>
+	+	kIx~
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
předmětem	předmět	k1gInSc7
zájmu	zájem	k1gInSc2
některých	některý	k3yIgFnPc2
firem	firma	k1gFnPc2
na	na	k7c6
mobilním	mobilní	k2eAgInSc6d1
trhu	trh	k1gInSc6
a	a	k8xC
to	ten	k3xDgNnSc1
přináší	přinášet	k5eAaImIp3nS
zpět	zpět	k6eAd1
do	do	k7c2
GNOME	GNOME	kA
opravy	oprava	k1gFnSc2
zvyšující	zvyšující	k2eAgInSc1d1
výkon	výkon	k1gInSc1
a	a	k8xC
snižující	snižující	k2eAgFnSc1d1
náročnost	náročnost	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
cílem	cíl	k1gInSc7
projektů	projekt	k1gInPc2
jako	jako	k8xS,k8xC
Openmoko	Openmoko	k1gNnSc1
je	být	k5eAaImIp3nS
využití	využití	k1gNnSc4
GTK	GTK	kA
<g/>
+	+	kIx~
a	a	k8xC
portování	portování	k1gNnSc1
GTK	GTK	kA
<g/>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
GNOME	GNOME	kA
aplikací	aplikace	k1gFnPc2
na	na	k7c4
svá	svůj	k3xOyFgNnPc4
mobilní	mobilní	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Částečně	částečně	k6eAd1
se	se	k3xPyFc4
také	také	k9
na	na	k7c4
špatné	špatný	k2eAgFnPc4d1
pověsti	pověst	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
výkonu	výkon	k1gInSc2
mohl	moct	k5eAaImAgInS
podepsat	podepsat	k5eAaPmF
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
GNOME	GNOME	kA
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
témat	téma	k1gNnPc2
vzhledu	vzhled	k1gInSc2
založených	založený	k2eAgNnPc2d1
na	na	k7c6
rozličných	rozličný	k2eAgInPc6d1
<g/>
,	,	kIx,
často	často	k6eAd1
nekvalitně	kvalitně	k6eNd1
napsaných	napsaný	k2eAgFnPc2d1
<g/>
,	,	kIx,
základech	základ	k1gInPc6
–	–	k?
gtk-engine	gtk-enginout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
GTK-engine	GTK-engin	k1gInSc5
je	být	k5eAaImIp3nS
pro	pro	k7c4
GTK	GTK	kA
<g/>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
GNOME	GNOME	kA
soubor	soubor	k1gInSc4
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nad	nad	k7c7
daným	daný	k2eAgInSc7d1
postavená	postavený	k2eAgNnPc1d1
témata	téma	k1gNnPc1
mohou	moct	k5eAaImIp3nP
využívat	využívat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
i	i	k9
ubuntulooks	ubuntulooks	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
výchozí	výchozí	k2eAgInSc1d1
stavební	stavební	k2eAgInSc1d1
kámen	kámen	k1gInSc1
témat	téma	k1gNnPc2
distribuce	distribuce	k1gFnSc2
Ubuntu	Ubunt	k1gInSc2
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
v	v	k7c6
některých	některý	k3yIgFnPc6
funkcích	funkce	k1gFnPc6
příliš	příliš	k6eAd1
výkonným	výkonný	k2eAgMnPc3d1
gtk-engine	gtk-enginout	k5eAaPmIp3nS
a	a	k8xC
například	například	k6eAd1
clearlooks	clearlooks	k6eAd1
gtk-engine	gtk-enginout	k5eAaPmIp3nS
je	být	k5eAaImIp3nS
znatelně	znatelně	k6eAd1
výkonnější	výkonný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Komunita	komunita	k1gFnSc1
</s>
<s>
Komunita	komunita	k1gFnSc1
GNOME	GNOME	kA
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
přímým	přímý	k2eAgMnSc7d1
„	„	k?
<g/>
konkurentem	konkurent	k1gMnSc7
<g/>
“	“	k?
KDE	kde	k6eAd1
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Vývojáři	vývojář	k1gMnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
řad	řada	k1gFnPc2
placených	placený	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
firem	firma	k1gFnPc2
jako	jako	k8xS,k8xC
Red	Red	k1gFnPc2
Hat	hat	k0
nebo	nebo	k8xC
Novell	Novell	kA
nebo	nebo	k8xC
z	z	k7c2
firem	firma	k1gFnPc2
nějakým	nějaký	k3yIgInSc7
způsobem	způsob	k1gInSc7
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
GNOME	GNOME	kA
<g/>
,	,	kIx,
například	například	k6eAd1
Openmoko	Openmoko	k1gNnSc1
<g/>
,	,	kIx,
OpenedHand	OpenedHand	k1gInSc1
nebo	nebo	k8xC
Maemo	Maema	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3
část	část	k1gFnSc1
základny	základna	k1gFnSc2
však	však	k9
tvoří	tvořit	k5eAaImIp3nP
dobrovolníci	dobrovolník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
uživateli	uživatel	k1gMnPc7
spíše	spíše	k9
převládají	převládat	k5eAaImIp3nP
méně	málo	k6eAd2
zkušení	zkušení	k1gNnSc1
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
uživatelé	uživatel	k1gMnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nějakým	nějaký	k3yIgInSc7
způsobem	způsob	k1gInSc7
GNOME	GNOME	kA
oslovilo	oslovit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokročilejší	pokročilý	k2eAgMnPc1d2
uživatelé	uživatel	k1gMnPc1
totiž	totiž	k9
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
dávají	dávat	k5eAaImIp3nP
přednost	přednost	k1gFnSc4
KDE	kde	k6eAd1
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
větší	veliký	k2eAgFnSc7d2
přizpůsobivostí	přizpůsobivost	k1gFnSc7
nebo	nebo	k8xC
nějakému	nějaký	k3yIgInSc3
koncepčně	koncepčně	k6eAd1
odlišnému	odlišný	k2eAgInSc3d1
desktopovému	desktopový	k2eAgInSc3d1
základu	základ	k1gInSc3
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
s	s	k7c7
použitím	použití	k1gNnSc7
některých	některý	k3yIgFnPc2
GNOME	GNOME	kA
<g/>
/	/	kIx~
<g/>
GTK	GTK	kA
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
mají	mít	k5eAaImIp3nP
rádi	rád	k2eAgMnPc1d1
GTK	GTK	kA
<g/>
+	+	kIx~
<g/>
,	,	kIx,
ale	ale	k8xC
GNOME	GNOME	kA
jim	on	k3xPp3gMnPc3
přímo	přímo	k6eAd1
nevyhovuje	vyhovovat	k5eNaImIp3nS
<g/>
,	,	kIx,
si	se	k3xPyFc3
často	často	k6eAd1
oblíbí	oblíbit	k5eAaPmIp3nP
na	na	k7c6
knihovně	knihovna	k1gFnSc6
GTK	GTK	kA
<g/>
+	+	kIx~
postavené	postavený	k2eAgFnPc1d1
Xfce	Xfc	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Každoročně	každoročně	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
pořádána	pořádán	k2eAgFnSc1d1
konference	konference	k1gFnSc1
GUADEC	GUADEC	kA
(	(	kIx(
<g/>
GNOME	GNOME	kA
Users	Users	k1gInSc4
And	Anda	k1gFnPc2
Developers	Developersa	k1gFnPc2
European	European	k1gInSc1
Conference	Conferenec	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
stěhuje	stěhovat	k5eAaImIp3nS
do	do	k7c2
jiné	jiný	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akci	akce	k1gFnSc4
zastřešuje	zastřešovat	k5eAaImIp3nS
a	a	k8xC
organizuje	organizovat	k5eAaBmIp3nS
GNOME	GNOME	kA
UserGroup	UserGroup	k1gMnSc1
hostitelské	hostitelský	k2eAgFnSc2d1
země	zem	k1gFnSc2
s	s	k7c7
podporou	podpora	k1gFnSc7
nadace	nadace	k1gFnSc2
GNOME	GNOME	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
na	na	k7c6
konferenci	konference	k1gFnSc6
je	být	k5eAaImIp3nS
umožněn	umožnit	k5eAaPmNgInS
i	i	k9
široké	široký	k2eAgFnSc3d1
veřejnosti	veřejnost	k1gFnSc3
a	a	k8xC
celá	celý	k2eAgFnSc1d1
akce	akce	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
přátelské	přátelský	k2eAgFnSc6d1
a	a	k8xC
uvolněné	uvolněný	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
cílem	cíl	k1gInSc7
není	být	k5eNaImIp3nS
jen	jen	k9
seznámit	seznámit	k5eAaPmF
účastníky	účastník	k1gMnPc4
s	s	k7c7
rozličnými	rozličný	k2eAgNnPc7d1
tématy	téma	k1gNnPc7
v	v	k7c6
přednáškách	přednáška	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
sblížit	sblížit	k5eAaPmF
komunitu	komunita	k1gFnSc4
a	a	k8xC
umožnit	umožnit	k5eAaPmF
osobní	osobní	k2eAgNnPc4d1
setkání	setkání	k1gNnPc4
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
se	se	k3xPyFc4
zájmem	zájem	k1gInSc7
o	o	k7c6
GNOME	GNOME	kA
<g/>
.	.	kIx.
</s>
<s>
Nejzajímavější	zajímavý	k2eAgInPc4d3
programy	program	k1gInPc4
</s>
<s>
Projekt	projekt	k1gInSc1
GNOME	GNOME	kA
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
několika	několik	k4yIc2
základních	základní	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
,	,	kIx,
aplikace	aplikace	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
přímou	přímý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
GNOME	GNOME	kA
<g/>
,	,	kIx,
GNOME	GNOME	kA
Fifth	Fifth	k1gMnSc1
Toe	Toe	k1gMnSc1
(	(	kIx(
<g/>
Pátý	pátý	k4xOgInSc1
prst	prst	k1gInSc1
GNOME	GNOME	kA
–	–	k?
aplikace	aplikace	k1gFnPc4
navíc	navíc	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
GNOME	GNOME	kA
Extras	Extras	k1gInSc1
(	(	kIx(
<g/>
Další	další	k2eAgFnPc1d1
aplikace	aplikace	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
GNOME	GNOME	kA
Office	Office	kA
(	(	kIx(
<g/>
Kancelářské	kancelářský	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
GNOME	GNOME	kA
<g/>
)	)	kIx)
a	a	k8xC
GNOME	GNOME	kA
Infrastructure	Infrastructur	k1gMnSc5
(	(	kIx(
<g/>
Infrastruktura	infrastruktura	k1gFnSc1
GNOME	GNOME	kA
<g/>
,	,	kIx,
především	především	k9
WWW	WWW	kA
aplikace	aplikace	k1gFnPc4
používané	používaný	k2eAgFnPc4d1
projektem	projekt	k1gInSc7
například	například	k6eAd1
k	k	k7c3
prezentaci	prezentace	k1gFnSc3
na	na	k7c6
Internetu	Internet	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
přímé	přímý	k2eAgFnPc4d1
součásti	součást	k1gFnPc4
GNOME	GNOME	kA
platí	platit	k5eAaImIp3nP
přísnější	přísný	k2eAgNnPc4d2
pravidla	pravidlo	k1gNnPc4
<g/>
,	,	kIx,
než	než	k8xS
pro	pro	k7c4
programy	program	k1gInPc4
z	z	k7c2
ostatních	ostatní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
(	(	kIx(
<g/>
aktivní	aktivní	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
<g/>
,	,	kIx,
dodržování	dodržování	k1gNnSc4
vývojového	vývojový	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
GNOME	GNOME	kA
<g/>
,	,	kIx,
dodržování	dodržování	k1gNnSc1
GNOME	GNOME	kA
User	usrat	k5eAaPmRp2nS
Interface	interface	k1gInSc1
Guidelines	Guidelinesa	k1gFnPc2
a	a	k8xC
pod	pod	k7c7
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Desktopové	desktopový	k2eAgInPc1d1
programy	program	k1gInPc1
lze	lze	k6eAd1
samozřejmě	samozřejmě	k6eAd1
používat	používat	k5eAaImF
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgNnPc6d1
prostředích	prostředí	k1gNnPc6
jako	jako	k8xC,k8xS
třeba	třeba	k6eAd1
KDE	kde	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
obecně	obecně	k6eAd1
jsou	být	k5eAaImIp3nP
prezentovány	prezentovat	k5eAaBmNgFnP
jako	jako	k9
„	„	k?
<g/>
vlajkové	vlajkový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
<g/>
“	“	k?
projektu	projekt	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
AbiWord	AbiWord	k1gInSc1
–	–	k?
textový	textový	k2eAgInSc1d1
editor	editor	k1gInSc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
přímo	přímo	k6eAd1
součástí	součást	k1gFnSc7
GNOME	GNOME	kA
<g/>
)	)	kIx)
</s>
<s>
Ekiga	Ekiga	k1gFnSc1
–	–	k?
videotelefonování	videotelefonování	k1gNnSc1
přes	přes	k7c4
internet	internet	k1gInSc4
</s>
<s>
Empathy	Empatha	k1gFnPc1
–	–	k?
klient	klient	k1gMnSc1
rychlé	rychlý	k2eAgFnSc2d1
výměny	výměna	k1gFnSc2
zpráv	zpráva	k1gFnPc2
pro	pro	k7c4
GNOME	GNOME	kA
</s>
<s>
GNOME	GNOME	kA
Web	web	k1gInSc1
–	–	k?
webový	webový	k2eAgInSc1d1
prohlížeč	prohlížeč	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epiphany	Epiphan	k1gInPc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
výchozím	výchozí	k2eAgInSc7d1
prohlížečem	prohlížeč	k1gInSc7
od	od	k7c2
verze	verze	k1gFnSc2
GNOME	GNOME	kA
2.4	2.4	k4
a	a	k8xC
nahradil	nahradit	k5eAaPmAgInS
Galeon	Galeon	k1gInSc1
</s>
<s>
Evince	Evinka	k1gFnSc3
–	–	k?
prohlížeč	prohlížeč	k1gMnSc1
dokumentů	dokument	k1gInPc2
ve	v	k7c6
formátu	formát	k1gInSc6
PDF	PDF	kA
a	a	k8xC
PostScript	PostScript	k1gMnSc1
</s>
<s>
Evolution	Evolution	k1gInSc1
–	–	k?
mailový	mailový	k2eAgInSc1d1
a	a	k8xC
groupwarový	groupwarový	k2eAgMnSc1d1
klient	klient	k1gMnSc1
</s>
<s>
Pidgin	Pidgin	k1gInSc1
–	–	k?
instant	instant	k?
messenger	messenger	k1gInSc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
přímo	přímo	k6eAd1
součástí	součást	k1gFnSc7
GNOME	GNOME	kA
<g/>
)	)	kIx)
</s>
<s>
GIMP	GIMP	kA
–	–	k?
GNU	gnu	k1gMnSc1
Image	image	k1gInSc2
Manipulation	Manipulation	k1gInSc1
Program	program	k1gInSc1
–	–	k?
snad	snad	k9
nejlepší	dobrý	k2eAgFnSc3d3
Open-Source	Open-Sourka	k1gFnSc3
grafický	grafický	k2eAgInSc1d1
editor	editor	k1gInSc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
přímo	přímo	k6eAd1
součástí	součást	k1gFnSc7
GNOME	GNOME	kA
<g/>
)	)	kIx)
</s>
<s>
gedit	gedit	k2eAgInSc1d1
–	–	k?
jednoduchý	jednoduchý	k2eAgInSc1d1
textový	textový	k2eAgInSc1d1
editor	editor	k1gInSc1
</s>
<s>
Gnumeric	Gnumeric	k1gMnSc1
–	–	k?
tabulkový	tabulkový	k2eAgInSc1d1
procesor	procesor	k1gInSc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
přímo	přímo	k6eAd1
součástí	součást	k1gFnSc7
GNOME	GNOME	kA
<g/>
)	)	kIx)
</s>
<s>
Inkscape	Inkscapat	k5eAaPmIp3nS
–	–	k?
vektorový	vektorový	k2eAgInSc1d1
grafický	grafický	k2eAgInSc1d1
editor	editor	k1gInSc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
přímo	přímo	k6eAd1
součástí	součást	k1gFnSc7
GNOME	GNOME	kA
<g/>
)	)	kIx)
</s>
<s>
GNOME	GNOME	kA
Files	Files	k1gMnSc1
–	–	k?
správce	správce	k1gMnSc1
souborů	soubor	k1gInPc2
</s>
<s>
Rhythmbox	Rhythmbox	k1gInSc1
–	–	k?
přehrávač	přehrávač	k1gInSc1
a	a	k8xC
správce	správce	k1gMnSc1
hudebních	hudební	k2eAgInPc2d1
souborů	soubor	k1gInPc2
podobný	podobný	k2eAgInSc1d1
iTunes	iTunes	k1gInSc1
</s>
<s>
GNOME	GNOME	kA
Videos	Videos	k1gInSc1
–	–	k?
multimediální	multimediální	k2eAgInSc1d1
přehrávač	přehrávač	k1gInSc1
</s>
<s>
GNOME	GNOME	kA
Software	software	k1gInSc1
–	–	k?
utilita	utilita	k1gFnSc1
pro	pro	k7c4
instalaci	instalace	k1gFnSc4
a	a	k8xC
aktualizaci	aktualizace	k1gFnSc4
softwaru	software	k1gInSc2
</s>
<s>
GNOME	GNOME	kA
Builder	Builder	k1gMnSc1
–	–	k?
integrované	integrovaný	k2eAgNnSc1d1
vývojové	vývojový	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
<s>
GNOME	GNOME	kA
Boxes	Boxes	k1gInSc1
–	–	k?
přístup	přístup	k1gInSc4
k	k	k7c3
virtualizovaným	virtualizovaný	k2eAgInPc3d1
a	a	k8xC
vzdáleným	vzdálený	k2eAgInPc3d1
systémům	systém	k1gInPc3
</s>
<s>
Snímky	snímek	k1gInPc4
obrazovky	obrazovka	k1gFnSc2
starších	starý	k2eAgFnPc2d2
verzí	verze	k1gFnPc2
Gnome	Gnom	k1gInSc5
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
MATE	mást	k5eAaImIp3nS
–	–	k?
Fork	Fork	k1gMnSc1
prostředí	prostředí	k1gNnSc2
GNOME	GNOME	kA
<g/>
,	,	kIx,
od	od	k7c2
verze	verze	k1gFnSc2
2.32	2.32	k4
</s>
<s>
Unity	unita	k1gMnPc4
–	–	k?
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
</s>
<s>
Cinnamon	Cinnamon	k1gNnSc1
(	(	kIx(
<g/>
uživatelské	uživatelský	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
KDE	kde	k6eAd1
–	–	k?
„	„	k?
<g/>
Konkurenční	konkurenční	k2eAgInSc4d1
<g/>
“	“	k?
pracovní	pracovní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
<s>
Xfce	Xfc	k1gMnSc4
–	–	k?
Odlehčené	odlehčený	k2eAgNnSc1d1
pracovní	pracovní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
založené	založený	k2eAgNnSc1d1
na	na	k7c6
knihovně	knihovna	k1gFnSc6
GTK	GTK	kA
<g/>
+	+	kIx~
</s>
<s>
Fluxbox	Fluxbox	k1gInSc1
–	–	k?
Malý	malý	k2eAgInSc1d1
<g/>
,	,	kIx,
jednoduchý	jednoduchý	k2eAgInSc1d1
a	a	k8xC
rychlý	rychlý	k2eAgMnSc1d1
správce	správce	k1gMnSc1
oken	okno	k1gNnPc2
</s>
<s>
IceWM	IceWM	k?
–	–	k?
Relativně	relativně	k6eAd1
nenáročný	náročný	k2eNgInSc1d1
<g/>
,	,	kIx,
rychlý	rychlý	k2eAgInSc1d1
a	a	k8xC
jednoduchý	jednoduchý	k2eAgMnSc1d1
správce	správce	k1gMnSc1
oken	okno	k1gNnPc2
</s>
<s>
UNIX	UNIX	kA
</s>
<s>
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
–	–	k?
Svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
GNOME	GNOME	kA
lze	lze	k6eAd1
provozovat	provozovat	k5eAaImF
i	i	k9
pod	pod	k7c7
funkčně	funkčně	k6eAd1
příbuznými	příbuzný	k2eAgNnPc7d1
řešeními	řešení	k1gNnPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
například	například	k6eAd1
Wayland	Wayland	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
GNOME	GNOME	kA
1.0	1.0	k4
Released	Released	k1gInSc4
–	–	k?
GNOME	GNOME	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
GNOME	GNOME	kA
Project	Project	k1gMnSc1
<g/>
,	,	kIx,
1999-03-03	1999-03-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
GNOME	GNOME	kA
Open	Open	k1gMnSc1
Source	Source	k1gMnSc1
Project	Project	k1gInSc4
on	on	k3xPp3gMnSc1
Open	Open	k1gMnSc1
Hub	houba	k1gFnPc2
<g/>
:	:	kIx,
Languages	Languages	k1gInSc1
Page	Pag	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Black	Blacka	k1gFnPc2
Duck	Ducka	k1gFnPc2
Software	software	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Internacionalizace	internacionalizace	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
GNOME	GNOME	kA
Project	Project	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Introducing	Introducing	k1gInSc1
GNOME	GNOME	kA
3.12	3.12	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
<g/>
,	,	kIx,
2014-03-26	2014-03-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CLINTON	Clinton	k1gMnSc1
<g/>
,	,	kIx,
Jason	Jason	k1gMnSc1
D.	D.	kA
GNOME	GNOME	kA
3	#num#	k4
<g/>
:	:	kIx,
Fewer	Fewer	k1gInSc1
Interruptions	Interruptions	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
<g/>
,	,	kIx,
2011-04-02	2011-04-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
GNOME	GNOME	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
O	o	k7c6
GNOME	GNOME	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://www.gnome.org/foundation	http://www.gnome.org/foundation	k1gInSc1
–	–	k?
Nadace	nadace	k1gFnSc2
GNOME	GNOME	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://planet.gnome.org	http://planet.gnome.org	k1gInSc1
–	–	k?
Lidé	člověk	k1gMnPc1
okolo	okolo	k7c2
GNOME	GNOME	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://wiki.gnome.org	http://wiki.gnome.org	k1gInSc1
–	–	k?
GNOME	GNOME	kA
wiki	wik	k1gFnSc2
(	(	kIx(
<g/>
projekty	projekt	k1gInPc1
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc1
<g/>
…	…	k?
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
GNOME	GNOME	kA
3	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://wiki.gnome.org/ThreePointZero/Plan	http://wiki.gnome.org/ThreePointZero/Plan	k1gInSc1
–	–	k?
Plán	plán	k1gInSc1
pro	pro	k7c4
verzi	verze	k1gFnSc4
3.0	3.0	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://wiki.gnome.org/Projects/GnomeShell	http://wiki.gnome.org/Projects/GnomeShell	k1gInSc1
–	–	k?
Informace	informace	k1gFnPc1
o	o	k7c6
vývoji	vývoj	k1gInSc6
součásti	součást	k1gFnSc2
GNOME	GNOME	kA
Shell	Shell	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://launchpad.net/gnome-activity-journal	http://launchpad.net/gnome-activity-journat	k5eAaPmAgMnS,k5eAaImAgMnS
–	–	k?
Informace	informace	k1gFnPc4
o	o	k7c6
vývoji	vývoj	k1gInSc6
součásti	součást	k1gFnSc2
GNOME	GNOME	kA
Activity	Activita	k1gFnSc2
Journal	Journal	k1gFnSc2
(	(	kIx(
<g/>
někdejší	někdejší	k2eAgMnSc1d1
GNOME	GNOME	kA
Zeitgeist	Zeitgeist	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
České	český	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
http://wiki.gnome.org/Czech	http://wiki.gnome.org/Cz	k1gInPc6
–	–	k?
GNOME	GNOME	kA
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
Ke	k	k7c3
stažení	stažení	k1gNnSc3
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://www.gnome.org/getting-gnome	http://www.gnome.org/getting-gnom	k1gInSc5
–	–	k?
GNOME	GNOME	kA
LiveCD	LiveCD	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://ftp.acc.umu.se/pub/GNOME	http://ftp.acc.umu.se/pub/GNOME	k?
–	–	k?
FTP	FTP	kA
server	server	k1gInSc1
GNOME	GNOME	kA
</s>
<s>
Novinky	novinka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://www.gnome.org/news	http://www.gnome.org/news	k6eAd1
–	–	k?
Planet	planeta	k1gFnPc2
GNOME	GNOME	kA
</s>
<s>
Komunita	komunita	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://www.guadec.org	http://www.guadec.org	k1gInSc1
–	–	k?
Stránky	stránka	k1gFnSc2
o	o	k7c6
konferenci	konference	k1gFnSc6
GUADEC	GUADEC	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://wiki.gnome.org/UserGroups	http://wiki.gnome.org/UserGroups	k1gInSc1
–	–	k?
GNOME	GNOME	kA
UserGroups	UserGroups	k1gInSc1
</s>
<s>
Aplikace	aplikace	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
GNOME	GNOME	kA
Bugzilla	Bugzilla	k1gMnSc1
–	–	k?
nástroj	nástroj	k1gInSc1
pro	pro	k7c4
hlášení	hlášení	k1gNnSc4
a	a	k8xC
evidenci	evidence	k1gFnSc4
chyb	chyba	k1gFnPc2
GNOME	GNOME	kA
a	a	k8xC
základních	základní	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Deset	deset	k4xCc1
nástrojů	nástroj	k1gInPc2
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
aplikací	aplikace	k1gFnPc2
pro	pro	k7c4
GNOME	GNOME	kA
–	–	k?
Článek	článek	k1gInSc1
o	o	k7c6
vývojářských	vývojářský	k2eAgFnPc6d1
GNOME	GNOME	kA
aplikacích	aplikace	k1gFnPc6
</s>
<s>
Art	Art	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://wiki.gnome.org/Personalization	http://wiki.gnome.org/Personalization	k1gInSc1
–	–	k?
Oficiální	oficiální	k2eAgInSc1d1
archiv	archiv	k1gInSc1
motivů	motiv	k1gInPc2
a	a	k8xC
pozadí	pozadí	k1gNnSc2
pro	pro	k7c4
GNOME	GNOME	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://www.gnome-look.org/browse/ord/latest	http://www.gnome-look.org/browse/ord/latest	k1gFnSc1
–	–	k?
Archiv	archiv	k1gInSc1
motivů	motiv	k1gInPc2
a	a	k8xC
pozadí	pozadí	k1gNnSc2
pro	pro	k7c4
GNOME	GNOME	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Projekt	projekt	k1gInSc1
GNU	gnu	k1gNnSc2
Historie	historie	k1gFnSc1
</s>
<s>
GNU	gnu	k1gNnSc1
Manifest	manifest	k1gInSc1
•	•	k?
Projekt	projekt	k1gInSc1
GNU	gnu	k1gMnSc1
•	•	k?
Free	Free	k1gInSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
•	•	k?
Historie	historie	k1gFnSc1
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
</s>
<s>
Licence	licence	k1gFnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
•	•	k?
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
•	•	k?
Affero	Affero	k1gNnSc1
General	General	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
•	•	k?
GNU	gnu	k1gMnSc1
Free	Fre	k1gFnSc2
Documentation	Documentation	k1gInSc1
License	License	k1gFnSc2
•	•	k?
GPL	GPL	kA
linking	linking	k1gInSc1
exception	exception	k1gInSc1
</s>
<s>
Software	software	k1gInSc1
</s>
<s>
GNU	gnu	k1gMnSc1
•	•	k?
GNU	gnu	k1gMnSc1
Health	Health	k1gMnSc1
•	•	k?
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
•	•	k?
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
NG	NG	kA
•	•	k?
Linux-libre	Linux-libr	k1gInSc5
•	•	k?
GNOME	GNOME	kA
•	•	k?
Gnuzilla	Gnuzilla	k1gMnSc1
•	•	k?
IceCat	IceCat	k1gInSc1
•	•	k?
Gnash	Gnash	k1gInSc1
•	•	k?
Bash	Bash	k1gInSc1
•	•	k?
GCC	GCC	kA
•	•	k?
GNU	gnu	k1gNnSc2
Emacs	Emacsa	k1gFnPc2
•	•	k?
GNU	gnu	k1gMnSc1
C	C	kA
Library	Librara	k1gFnPc1
•	•	k?
Coreutils	Coreutils	k1gInSc1
•	•	k?
GNU	gnu	k1gMnSc1
build	build	k1gMnSc1
system	syst	k1gMnSc7
•	•	k?
gettext	gettext	k1gMnSc1
•	•	k?
Ostatní	ostatní	k2eAgMnSc1d1
GNU	gnu	k1gMnSc1
balíčky	balíček	k1gInPc4
a	a	k8xC
programy	program	k1gInPc4
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1
</s>
<s>
Robert	Robert	k1gMnSc1
J.	J.	kA
Chassell	Chassell	k1gMnSc1
•	•	k?
Loï	Loï	k1gFnSc1
Dachary	Dachara	k1gFnSc2
•	•	k?
Ricardo	Ricardo	k1gNnSc4
Galli	Galle	k1gFnSc3
•	•	k?
Georg	Georg	k1gInSc1
C.	C.	kA
F.	F.	kA
Greve	Greev	k1gFnPc1
•	•	k?
Federico	Federico	k1gMnSc1
Heinz	Heinz	k1gMnSc1
•	•	k?
Benjamin	Benjamin	k1gMnSc1
Mako	mako	k1gNnSc1
Hill	Hill	k1gMnSc1
•	•	k?
Bradley	Bradlea	k1gFnSc2
M.	M.	kA
Kuhn	Kuhn	k1gMnSc1
•	•	k?
Eben	eben	k1gInSc1
Moglen	Moglen	k1gInSc1
•	•	k?
Brett	Brett	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Sullivan	Sullivan	k1gMnSc1
•	•	k?
Leonard	Leonard	k1gMnSc1
H.	H.	kA
Tower	Tower	k1gMnSc1
ml.	ml.	kA
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
-	-	kIx~
spor	spor	k1gInSc1
o	o	k7c4
jméno	jméno	k1gNnSc4
•	•	k?
Revolution	Revolution	k1gInSc1
OS	OS	kA
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Code	Code	k1gFnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4590007-3	4590007-3	k4
</s>
