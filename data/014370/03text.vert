<s>
Pivo	pivo	k1gNnSc1
</s>
<s>
Pivo	pivo	k1gNnSc1
Sklenice	sklenice	k1gFnSc2
pivaKategorie	pivaKategorie	k1gFnSc2
</s>
<s>
Pivo	pivo	k1gNnSc1
Původ	původ	k1gInSc1
</s>
<s>
Mezopotámie	Mezopotámie	k1gFnSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
Vznik	vznik	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Obsah	obsah	k1gInSc1
alk	alk	k?
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
až	až	k6eAd1
7	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
<g/>
)	)	kIx)
Použití	použití	k1gNnSc1
</s>
<s>
konzumuje	konzumovat	k5eAaBmIp3nS
se	se	k3xPyFc4
čisté	čistá	k1gFnSc2
(	(	kIx(
<g/>
vychlazené	vychlazený	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
s	s	k7c7
nealkoholickými	alkoholický	k2eNgInPc7d1
nápoji	nápoj	k1gInPc7
</s>
<s>
Pivo	pivo	k1gNnSc1
je	být	k5eAaImIp3nS
kvašený	kvašený	k2eAgInSc4d1
alkoholický	alkoholický	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
hořké	hořký	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
vyráběný	vyráběný	k2eAgInSc1d1
v	v	k7c6
pivovaru	pivovar	k1gInSc6
z	z	k7c2
obilného	obilný	k2eAgInSc2d1
sladu	slad	k1gInSc2
<g/>
,	,	kIx,
vody	voda	k1gFnSc2
a	a	k8xC
(	(	kIx(
<g/>
nikoli	nikoli	k9
nezbytně	nezbytně	k6eAd1,k6eNd1
<g/>
,	,	kIx,
ale	ale	k8xC
většinou	většina	k1gFnSc7
<g/>
)	)	kIx)
chmele	chmel	k1gInSc2
pomocí	pomocí	k7c2
pivovarských	pivovarský	k2eAgFnPc2d1
kvasinek	kvasinka	k1gFnPc2
(	(	kIx(
<g/>
eventuálně	eventuálně	k6eAd1
divokých	divoký	k2eAgFnPc2d1
kvasinek	kvasinka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
těší	těšit	k5eAaImIp3nS
značné	značný	k2eAgFnSc3d1
oblibě	obliba	k1gFnSc3
v	v	k7c6
Česku	Česko	k1gNnSc6
i	i	k8xC
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
nejkonzumovanější	konzumovaný	k2eAgInSc4d3
alkoholický	alkoholický	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
:	:	kIx,
188,6	188,6	k4
litrů	litr	k1gInPc2
na	na	k7c4
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pivo	pivo	k1gNnSc1
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
českých	český	k2eAgInPc2d1
symbolů	symbol	k1gInPc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
je	být	k5eAaImIp3nS
název	název	k1gInSc1
České	český	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xC,k8xS
zeměpisné	zeměpisný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pivo	pivo	k1gNnSc1
je	být	k5eAaImIp3nS
vařeno	vařen	k2eAgNnSc1d1
již	již	k6eAd1
od	od	k7c2
nepaměti	nepaměť	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
nemožné	možný	k2eNgNnSc1d1
určit	určit	k5eAaPmF
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
uvařeno	uvařen	k2eAgNnSc4d1
první	první	k4xOgNnSc4
pivo	pivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
země	zem	k1gFnPc1
původu	původ	k1gInSc2
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
Mezopotámie	Mezopotámie	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
přibližně	přibližně	k6eAd1
již	již	k6eAd1
v	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Je	být	k5eAaImIp3nS
však	však	k9
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Sumerové	Sumer	k1gMnPc1
připravovali	připravovat	k5eAaImAgMnP
pouze	pouze	k6eAd1
kvas	kvas	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Pivo	pivo	k1gNnSc1
je	být	k5eAaImIp3nS
staroslověnské	staroslověnský	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
označovalo	označovat	k5eAaImAgNnS
„	„	k?
<g/>
nápoj	nápoj	k1gInSc4
nejobyčejnější	obyčejný	k2eAgInSc4d3
a	a	k8xC
nejrozšířenější	rozšířený	k2eAgInSc4d3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
pivo	pivo	k1gNnSc1
konzumováno	konzumovat	k5eAaBmNgNnS
prakticky	prakticky	k6eAd1
na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2008	#num#	k4
drží	držet	k5eAaImIp3nP
obyvatelé	obyvatel	k1gMnPc1
Česka	Česko	k1gNnSc2
přední	přední	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
v	v	k7c6
průměrné	průměrný	k2eAgFnSc6d1
spotřebě	spotřeba	k1gFnSc6
piva	pivo	k1gNnSc2
na	na	k7c4
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
v	v	k7c6
průměru	průměr	k1gInSc6
160	#num#	k4
litrů	litr	k1gInPc2
na	na	k7c4
hlavu	hlava	k1gFnSc4
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Avšak	avšak	k8xC
ve	v	k7c6
věkové	věkový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
35	#num#	k4
až	až	k9
44	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
spotřebou	spotřeba	k1gFnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
týdenní	týdenní	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
alkoholu	alkohol	k1gInSc2
mužů	muž	k1gMnPc2
zhruba	zhruba	k6eAd1
9	#num#	k4
litrů	litr	k1gInPc2
a	a	k8xC
žen	žena	k1gFnPc2
2	#num#	k4
litry	litr	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
což	což	k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
odpovídalo	odpovídat	k5eAaImAgNnS
ekvivalentu	ekvivalent	k1gInSc2
450	#num#	k4
litrů	litr	k1gInPc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
°	°	k?
piv	pivo	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
respektive	respektive	k9
100	#num#	k4
litrů	litr	k1gInPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
WHO	WHO	kA
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
průměrně	průměrně	k6eAd1
přes	přes	k7c4
16	#num#	k4
litrů	litr	k1gInPc2
čistého	čistý	k2eAgInSc2d1
alkoholu	alkohol	k1gInSc2
celkem	celkem	k6eAd1
na	na	k7c4
dospělého	dospělý	k1gMnSc4
(	(	kIx(
<g/>
na	na	k7c4
neabstinujícího	abstinující	k2eNgMnSc4d1
dospělého	dospělý	k2eAgMnSc4d1
muže	muž	k1gMnSc4
dokonce	dokonce	k9
přes	přes	k7c4
26	#num#	k4
litrů	litr	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
přes	přes	k7c4
8	#num#	k4
litrů	litr	k1gInPc2
jen	jen	k9
na	na	k7c4
pivo	pivo	k1gNnSc4
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Pivo	pivo	k1gNnSc1
je	být	k5eAaImIp3nS
tradičním	tradiční	k2eAgInSc7d1
a	a	k8xC
populárním	populární	k2eAgInSc7d1
nápojem	nápoj	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
dlouhou	dlouhý	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadí	řadit	k5eAaImIp3nS
se	se	k3xPyFc4
mezi	mezi	k7c4
alkoholické	alkoholický	k2eAgInPc4d1
nápoje	nápoj	k1gInPc4
s	s	k7c7
relativně	relativně	k6eAd1
nízkým	nízký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
alkoholu	alkohol	k1gInSc2
(	(	kIx(
<g/>
30	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
g	g	kA
v	v	k7c6
jednom	jeden	k4xCgInSc6
litru	litr	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
alkoholu	alkohol	k1gInSc2
obsahuje	obsahovat	k5eAaImIp3nS
pivo	pivo	k1gNnSc4
také	také	k9
přibližně	přibližně	k6eAd1
2	#num#	k4
000	#num#	k4
dalších	další	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
významné	významný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
takže	takže	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
výrazně	výrazně	k6eAd1
zavodňující	zavodňující	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
dále	daleko	k6eAd2
obsahuje	obsahovat	k5eAaImIp3nS
také	také	k9
sacharidy	sacharid	k1gInPc4
v	v	k7c6
podobě	podoba	k1gFnSc6
„	„	k?
<g/>
rychlých	rychlý	k2eAgFnPc2d1
kalorií	kalorie	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
bílkoviny	bílkovina	k1gFnPc1
<g/>
,	,	kIx,
hořké	hořký	k2eAgFnPc1d1
látky	látka	k1gFnPc1
chmele	chmel	k1gInSc2
<g/>
,	,	kIx,
polyfenolické	polyfenolický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
<g/>
,	,	kIx,
oxid	oxid	k1gInSc1
uhličitý	uhličitý	k2eAgInSc1d1
<g/>
,	,	kIx,
vitamíny	vitamín	k1gInPc4
a	a	k8xC
minerální	minerální	k2eAgFnPc4d1
látky	látka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Kombinace	kombinace	k1gFnPc1
těchto	tento	k3xDgFnPc2
složek	složka	k1gFnPc2
dává	dávat	k5eAaImIp3nS
fyziologicky	fyziologicky	k6eAd1
vyrovnaný	vyrovnaný	k2eAgInSc1d1
roztok	roztok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
rovnováze	rovnováha	k1gFnSc6
s	s	k7c7
osmotickým	osmotický	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
krve	krev	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Významné	významný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
zastoupení	zastoupení	k1gNnSc1
minerálů	minerál	k1gInPc2
v	v	k7c6
pivu	pivo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
kromě	kromě	k7c2
draslíku	draslík	k1gInSc2
a	a	k8xC
sodíku	sodík	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
v	v	k7c6
příznivém	příznivý	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
,	,	kIx,
také	také	k9
chloridy	chlorid	k1gInPc1
<g/>
,	,	kIx,
vápník	vápník	k1gInSc1
<g/>
,	,	kIx,
fosfor	fosfor	k1gInSc1
<g/>
,	,	kIx,
hořčík	hořčík	k1gInSc1
a	a	k8xC
křemík	křemík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vitamínů	vitamín	k1gInPc2
obsažených	obsažený	k2eAgInPc2d1
v	v	k7c6
pivu	pivo	k1gNnSc6
jsou	být	k5eAaImIp3nP
nejvýznamnější	významný	k2eAgInPc1d3
vitaminy	vitamin	k1gInPc1
skupiny	skupina	k1gFnSc2
B	B	kA
–	–	k?
thiamin	thiamin	k1gInSc1
(	(	kIx(
<g/>
vitamin	vitamin	k1gInSc1
B	B	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
3	#num#	k4
%	%	kIx~
denní	denní	k2eAgFnSc2d1
spotřeby	spotřeba	k1gFnSc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
litru	litr	k1gInSc6
piva	pivo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
riboflavin	riboflavin	k1gInSc1
(	(	kIx(
<g/>
vitamin	vitamin	k1gInSc1
B	B	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
20	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
pyridoxin	pyridoxin	k1gInSc1
(	(	kIx(
<g/>
vitamin	vitamin	k1gInSc1
B	B	kA
<g/>
6	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
31	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
niacin	niacin	k1gMnSc1
(	(	kIx(
<g/>
45	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
kyselina	kyselina	k1gFnSc1
listová	listový	k2eAgFnSc1d1
(	(	kIx(
<g/>
52	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Jeden	jeden	k4xCgInSc1
litr	litr	k1gInSc1
piva	pivo	k1gNnSc2
obsahuje	obsahovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
400	#num#	k4
–	–	k?
500	#num#	k4
kilokalorií	kilokalorie	k1gFnPc2
představujících	představující	k2eAgFnPc2d1
přibližně	přibližně	k6eAd1
20	#num#	k4
%	%	kIx~
denní	denní	k2eAgFnSc2d1
spotřeby	spotřeba	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
než	než	k8xS
má	mít	k5eAaImIp3nS
například	například	k6eAd1
jablečná	jablečný	k2eAgFnSc1d1
šťáva	šťáva	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
200	#num#	k4
mg	mg	kA
biologicky	biologicky	k6eAd1
aktivních	aktivní	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pivo	pivo	k1gNnSc1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
používat	používat	k5eAaImF
jako	jako	k8xC,k8xS
nápoj	nápoj	k1gInSc4
vhodný	vhodný	k2eAgInSc4d1
k	k	k7c3
utišení	utišení	k1gNnSc3
žízně	žízeň	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
nutriční	nutriční	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
především	především	k6eAd1
vhodnou	vhodný	k2eAgFnSc4d1
vyváženost	vyváženost	k1gFnSc4
iontů	ion	k1gInPc2
a	a	k8xC
minerálních	minerální	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
vitaminů	vitamin	k1gInPc2
a	a	k8xC
polyfenolů	polyfenol	k1gInPc2
(	(	kIx(
<g/>
flavonoidů	flavonoid	k1gInPc2
<g/>
)	)	kIx)
s	s	k7c7
antioxidačním	antioxidační	k2eAgInSc7d1
účinkem	účinek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ovšem	ovšem	k9
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
pozitivní	pozitivní	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Příznivé	příznivý	k2eAgInPc1d1
účinky	účinek	k1gInPc1
piva	pivo	k1gNnSc2
na	na	k7c4
lidský	lidský	k2eAgInSc4d1
organismus	organismus	k1gInSc4
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
projevit	projevit	k5eAaPmF
při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
střídmé	střídmý	k2eAgFnSc6d1
konzumaci	konzumace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nepřevažují	převažovat	k5eNaImIp3nP
negativní	negativní	k2eAgInPc4d1
účinky	účinek	k1gInPc4
alkoholu	alkohol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
i	i	k9
střídmé	střídmý	k2eAgNnSc4d1
pití	pití	k1gNnSc4
však	však	k9
pravděpodobně	pravděpodobně	k6eAd1
škodí	škodit	k5eAaImIp3nP
<g/>
,	,	kIx,
protože	protože	k8xS
studie	studie	k1gFnPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
ovlivněny	ovlivněn	k2eAgInPc1d1
vlivy	vliv	k1gInPc1
systematických	systematický	k2eAgFnPc2d1
chyb	chyba	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
skupiny	skupina	k1gFnSc2
abstinentů	abstinent	k1gMnPc2
například	například	k6eAd1
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
nepijí	pít	k5eNaImIp3nP
ze	z	k7c2
zdravotních	zdravotní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Pivo	pivo	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
hořké	hořký	k2eAgFnPc4d1
chmelové	chmelový	k2eAgFnPc4d1
látky	látka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
mají	mít	k5eAaImIp3nP
pozitivní	pozitivní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
sekreci	sekrece	k1gFnSc4
žluči	žluč	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
přímo	přímo	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
trávení	trávení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
výrazně	výrazně	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
chuť	chuť	k1gFnSc4
k	k	k7c3
jídlu	jídlo	k1gNnSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
může	moct	k5eAaImIp3nS
při	při	k7c6
nestřídmé	střídmý	k2eNgFnSc6d1
konzumaci	konzumace	k1gFnSc6
pokrmů	pokrm	k1gInPc2
vést	vést	k5eAaImF
k	k	k7c3
nárůstu	nárůst	k1gInSc3
tělesné	tělesný	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hořká	hořký	k2eAgFnSc1d1
chuť	chuť	k1gFnSc1
ale	ale	k8xC
lidem	člověk	k1gMnPc3
neimponuje	imponovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
vyšší	vysoký	k2eAgInSc1d2
obsah	obsah	k1gInSc1
flavonoidů	flavonoid	k1gInPc2
pocházejících	pocházející	k2eAgInPc2d1
z	z	k7c2
obilnin	obilnina	k1gFnPc2
a	a	k8xC
chmelu	chmel	k1gInSc2
pivo	pivo	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
vyšší	vysoký	k2eAgInPc4d2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
antioxidační	antioxidační	k2eAgInSc1d1
účinek	účinek	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Obsah	obsah	k1gInSc1
flavonoidů	flavonoid	k1gInPc2
zvyšujeme	zvyšovat	k5eAaImIp1nP
delší	dlouhý	k2eAgFnPc1d2
a	a	k8xC
intenzivnější	intenzivní	k2eAgFnPc1d2
extrakcí	extrakce	k1gFnSc7
sladu	slad	k1gInSc2
<g/>
,	,	kIx,
zvýšením	zvýšení	k1gNnSc7
množství	množství	k1gNnSc2
sladu	slad	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
přidáním	přidání	k1gNnSc7
sladu	slad	k1gInSc2
nebo	nebo	k8xC
výtažku	výtažek	k1gInSc2
z	z	k7c2
dalších	další	k2eAgFnPc2d1
obilnin	obilnina	k1gFnPc2
a	a	k8xC
obilovin	obilovina	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
bylinných	bylinný	k2eAgNnPc2d1
a	a	k8xC
ovocných	ovocný	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
rovněž	rovněž	k9
z	z	k7c2
bylin	bylina	k1gFnPc2
a	a	k8xC
ovoce	ovoce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
ovoce	ovoce	k1gNnPc1
nejvyšší	vysoký	k2eAgInPc4d3
antioxidační	antioxidační	k2eAgInSc4d1
účinky	účinek	k1gInPc4
mají	mít	k5eAaImIp3nP
flavonoidy	flavonoid	k1gInPc4
ostružin	ostružina	k1gFnPc2
a	a	k8xC
malin	malina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvení	barvení	k1gNnSc2
kulérem	kulér	k1gInSc7
(	(	kIx(
<g/>
karamelem	karamel	k1gInSc7
<g/>
)	)	kIx)
nebo	nebo	k8xC
pražením	pražení	k1gNnSc7
sladu	slad	k1gInSc2
je	být	k5eAaImIp3nS
sice	sice	k8xC
pro	pro	k7c4
pivo	pivo	k1gNnSc4
charakteristické	charakteristický	k2eAgNnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
antioxidační	antioxidační	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
piva	pivo	k1gNnSc2
nezvyšuje	zvyšovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Ovšem	ovšem	k9
ovocné	ovocný	k2eAgFnPc1d1
šťávy	šťáva	k1gFnPc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
přímo	přímo	k6eAd1
samotné	samotný	k2eAgNnSc4d1
ovoce	ovoce	k1gNnSc4
<g/>
)	)	kIx)
obsahují	obsahovat	k5eAaImIp3nP
větší	veliký	k2eAgInSc4d2
počet	počet	k1gInSc4
flavonidů	flavonid	k1gMnPc2
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
obsahovaly	obsahovat	k5eAaImAgInP
alkohol	alkohol	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
konzumace	konzumace	k1gFnSc1
piva	pivo	k1gNnSc2
má	mít	k5eAaImIp3nS
příznivé	příznivý	k2eAgInPc4d1
účinky	účinek	k1gInPc4
na	na	k7c4
dobrou	dobrý	k2eAgFnSc4d1
náladu	nálada	k1gFnSc4
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
krevního	krevní	k2eAgInSc2d1
oběhu	oběh	k1gInSc2
<g/>
,	,	kIx,
snížení	snížení	k1gNnSc4
rizika	riziko	k1gNnSc2
srdečních	srdeční	k2eAgFnPc2d1
příhod	příhoda	k1gFnPc2
a	a	k8xC
působí	působit	k5eAaImIp3nS
proti	proti	k7c3
vysokému	vysoký	k2eAgInSc3d1
krevnímu	krevní	k2eAgInSc3d1
tlaku	tlak	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Konzumace	konzumace	k1gFnSc1
piva	pivo	k1gNnSc2
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
alkoholismu	alkoholismus	k1gInSc2
s	s	k7c7
poškozením	poškození	k1gNnSc7
zdraví	zdraví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
nejen	nejen	k6eAd1
alkohol	alkohol	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jiné	jiný	k2eAgFnSc2d1
látky	látka	k1gFnSc2
dlouhodobou	dlouhodobý	k2eAgFnSc7d1
konzumací	konzumace	k1gFnSc7
způsobují	způsobovat	k5eAaImIp3nP
především	především	k6eAd1
tato	tento	k3xDgNnPc4
onemocnění	onemocnění	k1gNnPc4
a	a	k8xC
zvýšenou	zvýšený	k2eAgFnSc4d1
úmrtnost	úmrtnost	k1gFnSc4
<g/>
:	:	kIx,
cirhóza	cirhóza	k1gFnSc1
jater	játra	k1gNnPc2
<g/>
,	,	kIx,
kolorektální	kolorektální	k2eAgInSc1d1
karcinom	karcinom	k1gInSc1
<g/>
,	,	kIx,
karcinom	karcinom	k1gInSc1
prsu	prs	k1gInSc2
<g/>
,	,	kIx,
diabetes	diabetes	k1gInSc1
mellitus	mellitus	k1gInSc1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
IARC	IARC	kA
je	být	k5eAaImIp3nS
každý	každý	k3xTgInSc4
alkoholický	alkoholický	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
považován	považován	k2eAgInSc4d1
za	za	k7c4
prokázaný	prokázaný	k2eAgInSc4d1
karcinogen	karcinogen	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obsah	obsah	k1gInSc1
alkoholu	alkohol	k1gInSc2
</s>
<s>
Pivo	pivo	k1gNnSc1
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
od	od	k7c2
0,5	0,5	k4
do	do	k7c2
cca	cca	kA
20	#num#	k4
procent	procento	k1gNnPc2
alkoholu	alkohol	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
českých	český	k2eAgNnPc6d1
pivech	pivo	k1gNnPc6
je	být	k5eAaImIp3nS
alkoholu	alkohol	k1gInSc2
nejčastěji	často	k6eAd3
mezi	mezi	k7c4
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Orientační	orientační	k2eAgInSc1d1
obsah	obsah	k1gInSc1
alkoholu	alkohol	k1gInSc2
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
stupeň	stupeň	k1gInSc1
</s>
<s>
nealkoholickéradler	nealkoholickéradler	k1gInSc1
<g/>
8	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
°	°	k?
<g/>
speciální	speciální	k2eAgNnPc4d1
piva	pivo	k1gNnPc4
</s>
<s>
podíl	podíl	k1gInSc1
alkoholu	alkohol	k1gInSc2
</s>
<s>
~	~	kIx~
<g/>
0.5	0.5	k4
%	%	kIx~
<g/>
~	~	kIx~
<g/>
1,5	1,5	k4
<g/>
–	–	k?
<g/>
2,5	2,5	k4
<g/>
~	~	kIx~
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
3,4	3,4	k4
%	%	kIx~
<g/>
~	~	kIx~
<g/>
3,5	3,5	k4
<g/>
–	–	k?
<g/>
4,5	4,5	k4
%	%	kIx~
<g/>
~	~	kIx~
<g/>
4,6	4,6	k4
<g/>
–	–	k?
<g/>
4,8	4,8	k4
%	%	kIx~
<g/>
~	~	kIx~
<g/>
4,9	4,9	k4
<g/>
–	–	k?
<g/>
5,3	5,3	k4
%	%	kIx~
<g/>
~	~	kIx~
<g/>
5,5	5,5	k4
a	a	k8xC
více	hodně	k6eAd2
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
schopno	schopen	k2eAgNnSc1d1
odbourávat	odbourávat	k5eAaImF
alkohol	alkohol	k1gInSc4
dle	dle	k7c2
individuálních	individuální	k2eAgFnPc2d1
měřítek	měřítko	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
hmotnosti	hmotnost	k1gFnSc6
<g/>
,	,	kIx,
pohlaví	pohlaví	k1gNnSc6
<g/>
,	,	kIx,
zdravotním	zdravotní	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
stupni	stupeň	k1gInSc6
únavy	únava	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgInPc6d1
faktorech	faktor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
se	se	k3xPyFc4
z	z	k7c2
těla	tělo	k1gNnSc2
odbourá	odbourat	k5eAaPmIp3nS
alkohol	alkohol	k1gInSc4
uvolněný	uvolněný	k2eAgInSc4d1
po	po	k7c6
konzumaci	konzumace	k1gFnSc6
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
jen	jen	k9
nepřesně	přesně	k6eNd1
odhadovat	odhadovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
platí	platit	k5eAaImIp3nS
zásada	zásada	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
obéznější	obézní	k2eAgMnSc1d2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
pomaleji	pomale	k6eAd2
se	se	k3xPyFc4
alkohol	alkohol	k1gInSc1
z	z	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
těla	tělo	k1gNnSc2
odbourává	odbourávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orientačně	orientačně	k6eAd1
se	se	k3xPyFc4
občas	občas	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
čas	čas	k1gInSc4
okolo	okolo	k7c2
2	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
10	#num#	k4
minut	minuta	k1gFnPc2
pro	pro	k7c4
80	#num#	k4
kg	kg	kA
vážícího	vážící	k2eAgMnSc4d1
muže	muž	k1gMnSc4
u	u	k7c2
10	#num#	k4
<g/>
°	°	k?
piva	pivo	k1gNnSc2
a	a	k8xC
3	#num#	k4
hodiny	hodina	k1gFnPc1
a	a	k8xC
50	#num#	k4
minut	minuta	k1gFnPc2
u	u	k7c2
ženy	žena	k1gFnSc2
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
60	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
12	#num#	k4
<g/>
°	°	k?
pivo	pivo	k1gNnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
při	při	k7c6
stejných	stejný	k2eAgInPc6d1
fyziologických	fyziologický	k2eAgInPc6d1
parametrech	parametr	k1gInPc6
doba	doba	k1gFnSc1
2	#num#	k4
hodiny	hodina	k1gFnPc1
a	a	k8xC
50	#num#	k4
minut	minuta	k1gFnPc2
respektive	respektive	k9
4	#num#	k4
hodiny	hodina	k1gFnSc2
a	a	k8xC
30	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Jedním	jeden	k4xCgInSc7
ze	z	k7c2
základních	základní	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
schopnosti	schopnost	k1gFnSc2
odbourávat	odbourávat	k5eAaImF
alkohol	alkohol	k1gInSc4
je	být	k5eAaImIp3nS
enzym	enzym	k1gInSc4
ADH	ADH	kA
–	–	k?
alkoholdehydrogenáza	alkoholdehydrogenáza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
enzym	enzym	k1gInSc4
však	však	k9
některým	některý	k3yIgInPc3
národům	národ	k1gInPc3
úplně	úplně	k6eAd1
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
eskymáci	eskymák	k1gMnPc1
<g/>
,	,	kIx,
indiáni	indián	k1gMnPc1
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
asijské	asijský	k2eAgInPc1d1
národy	národ	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
právním	právní	k2eAgInSc6d1
řádu	řád	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
se	se	k3xPyFc4
pivem	pivo	k1gNnSc7
zabývá	zabývat	k5eAaImIp3nS
zejména	zejména	k9
vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
335	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
provádí	provádět	k5eAaImIp3nS
zákon	zákon	k1gInSc4
o	o	k7c6
potravinách	potravina	k1gFnPc6
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
č.	č.	k?
110	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhláška	vyhláška	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
jak	jak	k8xS,k8xC
definici	definice	k1gFnSc4
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k9
požadavky	požadavek	k1gInPc4
na	na	k7c4
výrobu	výroba	k1gFnSc4
a	a	k8xC
jakost	jakost	k1gFnSc4
<g/>
,	,	kIx,
rozdělení	rozdělení	k1gNnSc4
druhů	druh	k1gInPc2
piva	pivo	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
označování	označování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Složení	složení	k1gNnSc1
</s>
<s>
Voda	voda	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Voda	voda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Voda	voda	k1gFnSc1
je	být	k5eAaImIp3nS
při	při	k7c6
výrobě	výroba	k1gFnSc6
piva	pivo	k1gNnSc2
nejdůležitější	důležitý	k2eAgFnSc1d3
ze	z	k7c2
základních	základní	k2eAgFnPc2d1
surovin	surovina	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
pro	pro	k7c4
samotnou	samotný	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
nápoje	nápoj	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
v	v	k7c6
procesech	proces	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
s	s	k7c7
výrobou	výroba	k1gFnSc7
souvisí	souviset	k5eAaImIp3nS
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
piva	pivo	k1gNnSc2
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
vody	voda	k1gFnSc2
–	–	k?
na	na	k7c4
1	#num#	k4
litr	litr	k1gInSc4
vystaveného	vystavený	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
je	být	k5eAaImIp3nS
spotřebováno	spotřebován	k2eAgNnSc1d1
čtyřikrát	čtyřikrát	k6eAd1
až	až	k9
osmkrát	osmkrát	k6eAd1
více	hodně	k6eAd2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
použití	použití	k1gNnSc1
přímo	přímo	k6eAd1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
kvalitu	kvalita	k1gFnSc4
výsledného	výsledný	k2eAgInSc2d1
produktu	produkt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Pivovary	pivovar	k1gInPc1
jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
budovány	budovat	k5eAaImNgFnP
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
výrazné	výrazný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
kvalitní	kvalitní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chmel	Chmel	k1gMnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Chmel	chmel	k1gInSc1
otáčivý	otáčivý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Chmel	chmel	k1gInSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
základních	základní	k2eAgFnPc2d1
surovin	surovina	k1gFnPc2
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
mu	on	k3xPp3gMnSc3
dodává	dodávat	k5eAaImIp3nS
charakteristickou	charakteristický	k2eAgFnSc4d1
nahořklou	nahořklý	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
pomocí	pomocí	k7c2
chmelových	chmelový	k2eAgFnPc2d1
pryskyřic	pryskyřice	k1gFnPc2
a	a	k8xC
chmelové	chmelový	k2eAgNnSc1d1
aroma	aroma	k1gNnSc1
vlivem	vlivem	k7c2
silic	silice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
přípravu	příprava	k1gFnSc4
piva	pivo	k1gNnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
samičí	samičí	k2eAgFnPc4d1
chmelové	chmelový	k2eAgFnPc4d1
hlávky	hlávka	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
zpracovávají	zpracovávat	k5eAaImIp3nP
na	na	k7c6
tzv.	tzv.	kA
chmelové	chmelový	k2eAgInPc4d1
produkty	produkt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
granule	granule	k1gFnPc4
připravené	připravený	k2eAgFnPc4d1
z	z	k7c2
hlávek	hlávka	k1gFnPc2
po	po	k7c6
usušení	usušení	k1gNnSc6
<g/>
,	,	kIx,
rozemletí	rozemletí	k1gNnSc6
a	a	k8xC
následné	následný	k2eAgFnSc6d1
peletizaci	peletizace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
výrobu	výroba	k1gFnSc4
českých	český	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
chmele	chmel	k1gInSc2
tzv.	tzv.	kA
žatecký	žatecký	k2eAgInSc1d1
poloraný	poloraný	k2eAgInSc1d1
červeňák	červeňák	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
bývá	bývat	k5eAaImIp3nS
řazen	řadit	k5eAaImNgInS
mezi	mezi	k7c4
nejkvalitnější	kvalitní	k2eAgInSc4d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Řadí	řadit	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
skupiny	skupina	k1gFnSc2
jemných	jemný	k2eAgInPc2d1
aromatických	aromatický	k2eAgInPc2d1
chmelů	chmel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chmel	chmel	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
pěstován	pěstovat	k5eAaImNgInS
ve	v	k7c6
třech	tři	k4xCgFnPc6
hlavních	hlavní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
–	–	k?
žatecké	žatecký	k2eAgFnPc4d1
<g/>
,	,	kIx,
úštěcké	úštěcký	k2eAgFnPc4d1
a	a	k8xC
tršické	tršický	k2eAgFnPc4d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
již	již	k6eAd1
od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Chmelové	Chmelové	k2eAgFnSc2d1
hlávky	hlávka	k1gFnSc2
jsou	být	k5eAaImIp3nP
chemicky	chemicky	k6eAd1
složité	složitý	k2eAgInPc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
obsahují	obsahovat	k5eAaImIp3nP
např.	např.	kA
alfa	alfa	k1gNnSc4
kyseliny	kyselina	k1gFnSc2
známé	známý	k2eAgInPc1d1
jako	jako	k8xS,k8xC
humulony	humulon	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Slad	slad	k1gInSc1
</s>
<s>
Slad	slad	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Slad	slad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Slad	slad	k1gInSc1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
ze	z	k7c2
speciálně	speciálně	k6eAd1
vyšlechtěných	vyšlechtěný	k2eAgInPc2d1
druhů	druh	k1gInPc2
obilí	obilí	k1gNnSc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
ječmene	ječmen	k1gInSc2
či	či	k8xC
méně	málo	k6eAd2
často	často	k6eAd1
pšenice	pšenice	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
mají	mít	k5eAaImIp3nP
výrazný	výrazný	k2eAgInSc4d1
podíl	podíl	k1gInSc4
na	na	k7c6
výsledné	výsledný	k2eAgFnSc6d1
chuti	chuť	k1gFnSc6
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc3
barvě	barva	k1gFnSc3
a	a	k8xC
aromatu	aroma	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Rozlišuje	rozlišovat	k5eAaImIp3nS
se	se	k3xPyFc4
světlý	světlý	k2eAgInSc1d1
a	a	k8xC
tmavý	tmavý	k2eAgInSc1d1
slad	slad	k1gInSc1
(	(	kIx(
<g/>
slad	slad	k1gInSc1
plzeňský	plzeňský	k2eAgInSc1d1
a	a	k8xC
bavorský	bavorský	k2eAgMnSc1d1
<g/>
)	)	kIx)
podle	podle	k7c2
barvy	barva	k1gFnSc2
a	a	k8xC
dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
slady	slad	k1gInPc1
speciální	speciální	k2eAgInPc1d1
(	(	kIx(
<g/>
karamelový	karamelový	k2eAgInSc1d1
<g/>
,	,	kIx,
pražený	pražený	k2eAgInSc1d1
<g/>
,	,	kIx,
diastatický	diastatický	k2eAgMnSc1d1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
výrobu	výroba	k1gFnSc4
piva	pivo	k1gNnSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
ječné	ječný	k2eAgNnSc1d1
zrno	zrno	k1gNnSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
obilka	obilka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
obalu	obal	k1gInSc2
<g/>
,	,	kIx,
zárodku	zárodek	k1gInSc2
klíčku	klíček	k1gInSc2
a	a	k8xC
endospermu	endosperm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
sladu	slad	k1gInSc2
se	se	k3xPyFc4
následně	následně	k6eAd1
sleduje	sledovat	k5eAaImIp3nS
hlavně	hlavně	k9
klíčivost	klíčivost	k1gFnSc1
a	a	k8xC
klíčivá	klíčivý	k2eAgFnSc1d1
energie	energie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Příprava	příprava	k1gFnSc1
sladu	slad	k1gInSc2
probíhá	probíhat	k5eAaImIp3nS
ve	v	k7c6
sladovnách	sladovna	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
sklizené	sklizený	k2eAgNnSc4d1
obilí	obilí	k1gNnSc4
v	v	k7c6
případě	případ	k1gInSc6
světlého	světlý	k2eAgInSc2d1
sladu	slad	k1gInSc2
nejprve	nejprve	k6eAd1
přibližně	přibližně	k6eAd1
po	po	k7c4
3	#num#	k4
dny	dna	k1gFnSc2
máčeno	máčet	k5eAaImNgNnS
v	v	k7c6
měkké	měkký	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dosáhlo	dosáhnout	k5eAaPmAgNnS
toho	ten	k3xDgMnSc4
<g/>
,	,	kIx,
že	že	k8xS
zrna	zrno	k1gNnPc1
vstřebají	vstřebat	k5eAaPmIp3nP
přibližně	přibližně	k6eAd1
45	#num#	k4
%	%	kIx~
své	svůj	k3xOyFgFnSc2
váhy	váha	k1gFnSc2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
je	být	k5eAaImIp3nS
rozloženo	rozložen	k2eAgNnSc1d1
ve	v	k7c6
vrstvě	vrstva	k1gFnSc6
silné	silný	k2eAgFnSc6d1
mezi	mezi	k7c4
10	#num#	k4
a	a	k8xC
15	#num#	k4
cm	cm	kA
a	a	k8xC
nechává	nechávat	k5eAaImIp3nS
se	se	k3xPyFc4
naklíčit	naklíčit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
klíčení	klíčení	k1gNnSc2
se	se	k3xPyFc4
zrno	zrno	k1gNnSc1
několikrát	několikrát	k6eAd1
obrací	obracet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčení	klíčení	k1gNnSc1
zrna	zrno	k1gNnSc2
končí	končit	k5eAaImIp3nS
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
klíček	klíček	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
¾	¾	k?
délky	délka	k1gFnSc2
zrna	zrno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniká	vznikat	k5eAaImIp3nS
tak	tak	k6eAd1
zelený	zelený	k2eAgInSc1d1
slad	slad	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
podstupuje	podstupovat	k5eAaImIp3nS
několik	několik	k4yIc4
fází	fáze	k1gFnPc2
sušení	sušení	k1gNnSc2
nejprve	nejprve	k6eAd1
o	o	k7c6
teplotě	teplota	k1gFnSc6
35	#num#	k4
<g/>
,	,	kIx,
pak	pak	k6eAd1
50	#num#	k4
a	a	k8xC
60	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
na	na	k7c4
závěr	závěr	k1gInSc4
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
závěrečná	závěrečný	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
sušení	sušení	k1gNnSc2
za	za	k7c2
stálého	stálý	k2eAgNnSc2d1
větrání	větrání	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
mezi	mezi	k7c7
75	#num#	k4
<g/>
–	–	k?
<g/>
80	#num#	k4
°	°	k?
<g/>
C.	C.	kA
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Kvalitní	kvalitní	k2eAgInSc1d1
sladovnický	sladovnický	k2eAgInSc1d1
ječmen	ječmen	k1gInSc1
po	po	k7c6
vysušení	vysušení	k1gNnSc6
obsahuje	obsahovat	k5eAaImIp3nS
62	#num#	k4
<g/>
–	–	k?
<g/>
65	#num#	k4
%	%	kIx~
škrobu	škrob	k1gInSc2
v	v	k7c6
sušině	sušina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejnižší	nízký	k2eAgFnSc7d3
teplotou	teplota	k1gFnSc7
sušený	sušený	k2eAgInSc1d1
světlý	světlý	k2eAgInSc1d1
slad	slad	k1gInSc1
je	být	k5eAaImIp3nS
vyráběn	vyrábět	k5eAaImNgInS
v	v	k7c6
Česku	Česko	k1gNnSc6
pro	pro	k7c4
piva	pivo	k1gNnPc4
plzeňského	plzeňský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
použití	použití	k1gNnSc6
vyšší	vysoký	k2eAgFnSc2d2
teploty	teplota	k1gFnSc2
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
slad	slad	k1gInSc1
vhodný	vhodný	k2eAgInSc1d1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
piva	pivo	k1gNnSc2
pale	pal	k1gInSc5
ale	ale	k8xC
malt	malta	k1gFnPc2
či	či	k8xC
alternativního	alternativní	k2eAgNnSc2d1
pojmenování	pojmenování	k1gNnSc2
„	„	k?
<g/>
vídeňského	vídeňský	k2eAgMnSc2d1
<g/>
“	“	k?
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
použitá	použitý	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
ještě	ještě	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
<g/>
,	,	kIx,
vzniká	vznikat	k5eAaImIp3nS
slad	slad	k1gInSc4
pro	pro	k7c4
bavorský	bavorský	k2eAgInSc4d1
typ	typ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nárůstu	nárůst	k1gInSc6
teploty	teplota	k1gFnSc2
následuje	následovat	k5eAaImIp3nS
tmavý	tmavý	k2eAgInSc4d1
slad	slad	k1gInSc4
zbarvený	zbarvený	k2eAgInSc4d1
do	do	k7c2
hnědé	hnědý	k2eAgFnSc2d1
až	až	k8xS
karamelové	karamelový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
užívaný	užívaný	k2eAgInSc1d1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
tmavých	tmavý	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
sladu	slad	k1gInSc2
se	se	k3xPyFc4
také	také	k9
praží	pražit	k5eAaImIp3nS
ve	v	k7c6
speciálních	speciální	k2eAgInPc6d1
pražících	pražící	k2eAgInPc6d1
bubnech	buben	k1gInPc6
a	a	k8xC
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
k	k	k7c3
dochucení	dochucení	k1gNnSc3
tmavých	tmavý	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tímto	tento	k3xDgInSc7
procesem	proces	k1gInSc7
se	se	k3xPyFc4
získá	získat	k5eAaPmIp3nS
škrobovitý	škrobovitý	k2eAgInSc1d1
roztok	roztok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc1d1
pro	pro	k7c4
uvolnění	uvolnění	k1gNnSc4
fermentovaných	fermentovaný	k2eAgInPc2d1
cukrů	cukr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvasinky	kvasinka	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Pivovarské	pivovarský	k2eAgFnSc2d1
kvasinky	kvasinka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc4
jsou	být	k5eAaImIp3nP
mikroorganismy	mikroorganismus	k1gInPc1
používané	používaný	k2eAgInPc1d1
v	v	k7c6
biotechnologické	biotechnologický	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
piva	pivo	k1gNnSc2
–	–	k?
pivovarnictví	pivovarnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
druhu	druh	k1gInSc2
(	(	kIx(
<g/>
průběhu	průběh	k1gInSc2
<g/>
)	)	kIx)
fermentace	fermentace	k1gFnPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
svrchní	svrchní	k2eAgNnSc4d1
kvašení	kvašení	k1gNnSc4
(	(	kIx(
<g/>
cca	cca	kA
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
;	;	kIx,
průběhem	průběh	k1gInSc7
fermentace	fermentace	k1gFnSc2
kvasinky	kvasinka	k1gFnSc2
na	na	k7c6
hladině	hladina	k1gFnSc6
<g/>
)	)	kIx)
používány	používán	k2eAgFnPc1d1
kvasinky	kvasinka	k1gFnPc1
Saccharomyces	Saccharomycesa	k1gFnPc2
ceravisiae	ceravisiae	k1gFnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
spodní	spodní	k2eAgNnSc4d1
kvašení	kvašení	k1gNnSc4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
;	;	kIx,
během	během	k7c2
fermentace	fermentace	k1gFnSc2
kvasinky	kvasinka	k1gFnSc2
sedimentují	sedimentovat	k5eAaImIp3nP
<g/>
)	)	kIx)
Saccharomyces	Saccharomyces	k1gInSc1
pastorianus	pastorianus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
piva	pivo	k1gNnPc1
jsou	být	k5eAaImIp3nP
kvašena	kvasit	k5eAaImNgNnP
i	i	k9
spontánním	spontánní	k2eAgNnSc7d1
kvašením	kvašení	k1gNnSc7
<g/>
,	,	kIx,
tedy	tedy	k9
za	za	k7c4
použití	použití	k1gNnSc4
divokých	divoký	k2eAgFnPc2d1
kvasinek	kvasinka	k1gFnPc2
a	a	k8xC
mikroorganismů	mikroorganismus	k1gInPc2
přítomných	přítomný	k2eAgInPc2d1
v	v	k7c6
místě	místo	k1gNnSc6
výroby	výroba	k1gFnSc2
bez	bez	k7c2
dodatečného	dodatečný	k2eAgNnSc2d1
dodání	dodání	k1gNnSc2
čisté	čistý	k2eAgFnSc2d1
kvasničné	kvasničný	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pěna	pěna	k1gFnSc1
</s>
<s>
Pivo	pivo	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
oxid	oxid	k1gInSc4
uhličitý	uhličitý	k2eAgInSc4d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
pivu	pivo	k1gNnSc6
bublinky	bublinka	k1gFnSc2
a	a	k8xC
pěnu	pěna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Čepované	čepovaný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
charakteristické	charakteristický	k2eAgNnSc1d1
krémovou	krémový	k2eAgFnSc7d1
pěnou	pěna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalitní	kvalitní	k2eAgFnSc1d1
pěna	pěna	k1gFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
udržet	udržet	k5eAaPmF
lehčí	lehký	k2eAgFnSc4d2
minci	mince	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Pěna	pěna	k1gFnSc1
tlumí	tlumit	k5eAaImIp3nS
povrchové	povrchový	k2eAgNnSc4d1
vlnění	vlnění	k1gNnSc4
hladiny	hladina	k1gFnSc2
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
piva	pivo	k1gNnSc2
</s>
<s>
Historie	historie	k1gFnSc1
piva	pivo	k1gNnSc2
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Západoněmecká	západoněmecký	k2eAgFnSc1d1
poštovní	poštovní	k2eAgFnSc1d1
známka	známka	k1gFnSc1
s	s	k7c7
motivem	motiv	k1gInSc7
„	„	k?
<g/>
Zákona	zákon	k1gInSc2
o	o	k7c6
čistotě	čistota	k1gFnSc6
piva	pivo	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
přípravy	příprava	k1gFnSc2
je	být	k5eAaImIp3nS
po	po	k7c6
staletí	staletí	k1gNnSc6
stejný	stejný	k2eAgInSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
postupně	postupně	k6eAd1
se	se	k3xPyFc4
významně	významně	k6eAd1
zdokonalovaly	zdokonalovat	k5eAaImAgFnP
jednotlivé	jednotlivý	k2eAgInPc4d1
technologické	technologický	k2eAgInPc4d1
kroky	krok	k1gInPc4
a	a	k8xC
zařízení	zařízení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Primitivní	primitivní	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
piva	pivo	k1gNnSc2
postupně	postupně	k6eAd1
nahradila	nahradit	k5eAaPmAgFnS
řemeslná	řemeslný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
a	a	k8xC
v	v	k7c6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
průmyslová	průmyslový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
kolébku	kolébka	k1gFnSc4
piva	pivo	k1gNnSc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
Mezopotámie	Mezopotámie	k1gFnSc1
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc1
tzv.	tzv.	kA
úrodného	úrodný	k2eAgInSc2d1
půlměsíce	půlměsíc	k1gInSc2
mezi	mezi	k7c7
řekami	řeka	k1gFnPc7
Eufratem	Eufrat	k1gInSc7
a	a	k8xC
Tigridem	Tigris	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc4
před	před	k7c7
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
zde	zde	k6eAd1
pěstovali	pěstovat	k5eAaImAgMnP
obilí	obilí	k1gNnSc4
Sumerové	Sumer	k1gMnPc1
<g/>
,	,	kIx,
Akkadové	Akkadový	k2eAgFnPc1d1
<g/>
,	,	kIx,
Babyloňané	Babyloňan	k1gMnPc1
a	a	k8xC
Asyřané	Asyřan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
pivo	pivo	k1gNnSc1
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
náhodně	náhodně	k6eAd1
<g/>
,	,	kIx,
díky	díky	k7c3
tehdejšímu	tehdejší	k2eAgNnSc3d1
skladování	skladování	k1gNnSc3
obilí	obilí	k1gNnSc2
v	v	k7c6
hliněných	hliněný	k2eAgFnPc6d1
nádobách	nádoba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
některých	některý	k3yIgFnPc2
nádob	nádoba	k1gFnPc2
nejspíše	nejspíše	k9
natekla	natéct	k5eAaPmAgFnS
voda	voda	k1gFnSc1
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
kvašení	kvašení	k1gNnSc3
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
voda	voda	k1gFnSc1
s	s	k7c7
příjemnou	příjemný	k2eAgFnSc7d1
omamnou	omamný	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
z	z	k7c2
obilí	obilí	k1gNnSc2
připravovat	připravovat	k5eAaImF
kvašené	kvašený	k2eAgInPc4d1
nápoje	nápoj	k1gInPc4
cíleně	cíleně	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
jakýsi	jakýsi	k3yIgMnSc1
druh	druh	k1gMnSc1
piva	pivo	k1gNnSc2
Sumery	Sumer	k1gInPc4
nazývaný	nazývaný	k2eAgInSc1d1
kaš	kaša	k1gFnPc2
<g/>
,	,	kIx,
Babyloňany	Babyloňan	k1gMnPc4
šikarum	šikarum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vaření	vaření	k1gNnSc1
piva	pivo	k1gNnSc2
bylo	být	k5eAaImAgNnS
do	do	k7c2
rozvoje	rozvoj	k1gInSc2
technické	technický	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
jeho	jeho	k3xOp3gFnSc2
přípravy	příprava	k1gFnSc2
většinou	většinou	k6eAd1
doménou	doména	k1gFnSc7
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nicméně	nicméně	k8xC
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
i	i	k9
archeologické	archeologický	k2eAgInPc4d1
důkazy	důkaz	k1gInPc4
<g/>
,	,	kIx,
že	že	k8xS
pivo	pivo	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
starší	starý	k2eAgMnPc1d2
a	a	k8xC
že	že	k8xS
původní	původní	k2eAgFnSc7d1
motivací	motivace	k1gFnSc7
výroby	výroba	k1gFnSc2
bylo	být	k5eAaImAgNnS
šlechtění	šlechtění	k1gNnSc1
obilnin	obilnina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sumerské	sumerský	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
kaš	kaša	k1gFnPc2
bylo	být	k5eAaImAgNnS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
současného	současný	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
připravováno	připravovat	k5eAaImNgNnS
bez	bez	k7c2
chmele	chmel	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ještě	ještě	k6eAd1
nebyl	být	k5eNaImAgInS
znám	znám	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaš	kaša	k1gFnPc2
vznikal	vznikat	k5eAaImAgInS
z	z	k7c2
ječného	ječný	k2eAgInSc2d1
chleba	chléb	k1gInSc2
a	a	k8xC
sladu	slad	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
společně	společně	k6eAd1
umístěny	umístit	k5eAaPmNgInP
do	do	k7c2
velikého	veliký	k2eAgInSc2d1
džbánu	džbán	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
docházelo	docházet	k5eAaImAgNnS
ke	k	k7c3
kvašení	kvašení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
přítomnosti	přítomnost	k1gFnSc2
chmelu	chmel	k1gInSc2
kaš	kaša	k1gFnPc2
nezískával	získávat	k5eNaImAgInS
hořkou	hořký	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
a	a	k8xC
pro	pro	k7c4
její	její	k3xOp3gNnSc4
dodání	dodání	k1gNnSc4
se	se	k3xPyFc4
muselo	muset	k5eAaImAgNnS
využívat	využívat	k5eAaPmF,k5eAaImF
jiného	jiný	k2eAgInSc2d1
postupu	postup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
se	se	k3xPyFc4
využívalo	využívat	k5eAaImAgNnS,k5eAaPmAgNnS
pražení	pražení	k1gNnSc1
chleba	chléb	k1gInSc2
v	v	k7c6
horkém	horký	k2eAgInSc6d1
popelu	popel	k1gInSc6
či	či	k8xC
přidávání	přidávání	k1gNnSc6
zelené	zelený	k2eAgFnSc2d1
hořčice	hořčice	k1gFnSc2
či	či	k8xC
sezamových	sezamový	k2eAgNnPc2d1
semínek	semínko	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
objevení	objevení	k1gNnSc6
kvašení	kvašení	k1gNnSc2
sladu	slad	k1gInSc2
začalo	začít	k5eAaPmAgNnS
vznikat	vznikat	k5eAaImF
mnoho	mnoho	k4c4
druhů	druh	k1gInPc2
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
lišily	lišit	k5eAaImAgFnP
od	od	k7c2
sebe	sebe	k3xPyFc4
barvou	barva	k1gFnSc7
a	a	k8xC
chutí	chuť	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
roku	rok	k1gInSc2
3000	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
bylo	být	k5eAaImAgNnS
pití	pití	k1gNnSc3
piva	pivo	k1gNnSc2
rozšířeným	rozšířený	k2eAgInSc7d1
zvykem	zvyk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgInSc4d1
pivo	pivo	k1gNnSc1
nepodstupovalo	podstupovat	k5eNaImAgNnS
proces	proces	k1gInSc4
filtrace	filtrace	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
přítomnost	přítomnost	k1gFnSc1
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
mechanických	mechanický	k2eAgFnPc2d1
částic	částice	k1gFnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
pivo	pivo	k1gNnSc1
nebylo	být	k5eNaImAgNnS
čiré	čirý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
se	se	k3xPyFc4
pro	pro	k7c4
pití	pití	k1gNnSc4
využívalo	využívat	k5eAaImAgNnS,k5eAaPmAgNnS
obilné	obilný	k2eAgNnSc4d1
stéblo	stéblo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
fungovalo	fungovat	k5eAaImAgNnS
jako	jako	k9
slámka	slámka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
tisíc	tisíc	k4xCgInSc4
let	let	k1gInSc4
později	pozdě	k6eAd2
v	v	k7c4
Chammurapiho	Chammurapi	k1gMnSc4
zákoníku	zákoník	k1gInSc2
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
pocházejí	pocházet	k5eAaImIp3nP
i	i	k9
první	první	k4xOgFnPc1
zmínky	zmínka	k1gFnPc1
o	o	k7c6
veřejných	veřejný	k2eAgFnPc6d1
provozovnách	provozovna	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
zakoupit	zakoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákoník	zákoník	k1gInSc1
upravoval	upravovat	k5eAaImAgInS
tresty	trest	k1gInPc4
pro	pro	k7c4
nepoctivé	poctivý	k2eNgFnPc4d1
šenkýřky	šenkýřka	k1gFnPc4
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
trestem	trest	k1gInSc7
vhození	vhození	k1gNnSc2
do	do	k7c2
vody	voda	k1gFnSc2
nebo	nebo	k8xC
dokonce	dokonce	k9
smrt	smrt	k1gFnSc4
utopením	utopení	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
záznamy	záznam	k1gInPc1
o	o	k7c6
přípravě	příprava	k1gFnSc6
piva	pivo	k1gNnSc2
pocházejí	pocházet	k5eAaImIp3nP
ze	z	k7c2
starověkého	starověký	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
občas	občas	k6eAd1
považován	považován	k2eAgInSc1d1
za	za	k7c4
původní	původní	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
objevu	objev	k1gInSc2
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staří	starý	k2eAgMnPc1d1
Egypťané	Egypťan	k1gMnPc1
používali	používat	k5eAaImAgMnP
pro	pro	k7c4
kvasicí	kvasicí	k2eAgFnSc4d1
nádobu	nádoba	k1gFnSc4
používanou	používaný	k2eAgFnSc4d1
při	při	k7c6
výrobě	výroba	k1gFnSc6
slovo	slovo	k1gNnSc1
namset	namseta	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
nejspíše	nejspíše	k9
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
akkadského	akkadský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
namzítu	namzít	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Egypťané	Egypťan	k1gMnPc1
používali	používat	k5eAaImAgMnP
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
piva	pivo	k1gNnSc2
ječmen	ječmen	k1gInSc1
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yIgNnSc2,k3yQgNnSc2,k3yRgNnSc2
vyráběli	vyrábět	k5eAaImAgMnP
slad	slad	k1gInSc4
a	a	k8xC
různé	různý	k2eAgInPc1d1
typy	typ	k1gInPc1
pšenice	pšenice	k1gFnSc2
namísto	namísto	k7c2
chmele	chmel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absence	absence	k1gFnSc1
chmelu	chmel	k1gInSc2
pak	pak	k6eAd1
měla	mít	k5eAaImAgFnS
za	za	k7c4
následek	následek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
pivo	pivo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
nasládlou	nasládlý	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
<g/>
,	,	kIx,
podobnou	podobný	k2eAgFnSc4d1
spíše	spíše	k9
dnešnímu	dnešní	k2eAgInSc3d1
kvasu	kvas	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
antiky	antika	k1gFnSc2
se	se	k3xPyFc4
pivo	pivo	k1gNnSc1
nacházelo	nacházet	k5eAaImAgNnS
na	na	k7c6
okraji	okraj	k1gInSc6
zájmu	zájem	k1gInSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
konzumaci	konzumace	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Středomoří	středomoří	k1gNnSc1
jasně	jasně	k6eAd1
dominovalo	dominovat	k5eAaImAgNnS
víno	víno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staří	starý	k2eAgMnPc1d1
Řekové	Řek	k1gMnPc1
pivo	pivo	k1gNnSc4
nepili	pít	k5eNaImAgMnP
vůbec	vůbec	k9
<g/>
,	,	kIx,
mezi	mezi	k7c4
Římany	Říman	k1gMnPc4
bylo	být	k5eAaImAgNnS
oblíbeno	oblíbit	k5eAaPmNgNnS
pouze	pouze	k6eAd1
u	u	k7c2
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
zvlášť	zvlášť	k6eAd1
v	v	k7c6
pohraničních	pohraniční	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
panoval	panovat	k5eAaImAgInS
nedostatek	nedostatek	k1gInSc1
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblastech	oblast	k1gFnPc6
Evropy	Evropa	k1gFnSc2
obývané	obývaný	k2eAgMnPc4d1
Kelty	Kelt	k1gMnPc4
se	se	k3xPyFc4
piva	pivo	k1gNnSc2
pilo	pít	k5eAaImAgNnS
více	hodně	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
nejváženějším	vážený	k2eAgInSc7d3
nápojem	nápoj	k1gInSc7
Keltů	Kelt	k1gMnPc2
byla	být	k5eAaImAgFnS
medovina	medovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
tomu	ten	k3xDgMnSc3
germánské	germánský	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
i	i	k9
nadále	nadále	k6eAd1
preferovaly	preferovat	k5eAaImAgFnP
výrobu	výroba	k1gFnSc4
a	a	k8xC
konzumaci	konzumace	k1gFnSc4
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
konzumace	konzumace	k1gFnSc1
piva	pivo	k1gNnSc2
hojně	hojně	k6eAd1
rozšířená	rozšířený	k2eAgFnSc1d1
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
oblasti	oblast	k1gFnPc4
pod	pod	k7c7
nadvládou	nadvláda	k1gFnSc7
Vikingů	Viking	k1gMnPc2
v	v	k7c6
oblastech	oblast	k1gFnPc6
severní	severní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pivo	pivo	k1gNnSc1
se	s	k7c7
zde	zde	k6eAd1
ale	ale	k8xC
konzumovalo	konzumovat	k5eAaBmAgNnS
teplé	teplý	k2eAgNnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
např.	např.	kA
v	v	k7c6
Polsku	Polsko	k1gNnSc6
přetrvalo	přetrvat	k5eAaPmAgNnS
i	i	k8xC
do	do	k7c2
mnohem	mnohem	k6eAd1
pozdější	pozdní	k2eAgFnSc2d2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
zde	zde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
objevení	objevení	k1gNnSc3
metody	metoda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
umožňovala	umožňovat	k5eAaImAgFnS
výrobu	výroba	k1gFnSc4
silnějšího	silný	k2eAgNnSc2d2
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
metodu	metoda	k1gFnSc4
tzv.	tzv.	kA
vymrazování	vymrazování	k1gNnSc1
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
během	během	k7c2
kterého	který	k3yRgNnSc2,k3yQgNnSc2,k3yIgNnSc2
pivo	pivo	k1gNnSc1
zmrzlo	zmrznout	k5eAaPmAgNnS
a	a	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
rozdílné	rozdílný	k2eAgFnSc3d1
teplotě	teplota	k1gFnSc3
tání	tání	k1gNnSc2
vody	voda	k1gFnSc2
a	a	k8xC
alkoholu	alkohol	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nárůstu	nárůst	k1gInSc3
obsahu	obsah	k1gInSc2
alkoholu	alkohol	k1gInSc2
v	v	k7c6
pivě	pivo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
obsahem	obsah	k1gInSc7
alkoholu	alkohol	k1gInSc2
blížilo	blížit	k5eAaImAgNnS
k	k	k7c3
dnešním	dnešní	k2eAgInPc3d1
ležákům	ležák	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
Keltové	Kelt	k1gMnPc1
a	a	k8xC
Germáni	Germán	k1gMnPc1
krátce	krátce	k6eAd1
po	po	k7c6
změně	změna	k1gFnSc6
letopočtu	letopočet	k1gInSc2
začali	začít	k5eAaPmAgMnP
do	do	k7c2
piva	pivo	k1gNnSc2
přidávat	přidávat	k5eAaImF
chmel	chmel	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1516	#num#	k4
vévoda	vévoda	k1gMnSc1
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský	bavorský	k2eAgInSc1d1
vydal	vydat	k5eAaPmAgMnS
tzv.	tzv.	kA
Reinheitsgebot	Reinheitsgebot	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
záruka	záruka	k1gFnSc1
čistoty	čistota	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zajistil	zajistit	k5eAaPmAgMnS
produkci	produkce	k1gFnSc4
kvalitního	kvalitní	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Zákon	zákon	k1gInSc1
o	o	k7c6
čistotě	čistota	k1gFnSc6
piva	pivo	k1gNnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
Reinheitsgebot	Reinheitsgebot	k1gInSc1
<g/>
,	,	kIx,
Deutsche	Deutsche	k1gNnSc1
Reinheitsgebot	Reinheitsgebota	k1gFnPc2
nebo	nebo	k8xC
Bayerische	Bayerische	k1gFnPc2
Reinheitsgebot	Reinheitsgebota	k1gFnPc2
<g/>
)	)	kIx)
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pivo	pivo	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
chmel	chmel	k1gInSc4
<g/>
,	,	kIx,
slad	slad	k1gInSc4
<g/>
,	,	kIx,
kvasinky	kvasinka	k1gFnPc4
a	a	k8xC
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Značné	značný	k2eAgFnSc2d1
obliby	obliba	k1gFnSc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
pivo	pivo	k1gNnSc1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hlavnímu	hlavní	k2eAgMnSc3d1
konkurentovi	konkurent	k1gMnSc3
v	v	k7c6
podobě	podoba	k1gFnSc6
vína	víno	k1gNnSc2
hrozilo	hrozit	k5eAaImAgNnS
zničení	zničení	k1gNnSc4
hmyzím	hmyzí	k2eAgMnSc7d1
škůdcem	škůdce	k1gMnSc7
z	z	k7c2
rodu	rod	k1gInSc2
Phylloxera	Phylloxero	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1876	#num#	k4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
Louis	Louis	k1gMnSc1
Pasteur	Pasteur	k1gMnSc1
dílo	dílo	k1gNnSc4
Studie	studie	k1gFnSc1
o	o	k7c6
pivu	pivo	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
podrobně	podrobně	k6eAd1
popisuje	popisovat	k5eAaImIp3nS
novodobou	novodobý	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
piva	pivo	k1gNnSc2
založenou	založený	k2eAgFnSc7d1
na	na	k7c4
filtraci	filtrace	k1gFnSc4
a	a	k8xC
pasterizaci	pasterizace	k1gFnSc4
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Současně	současně	k6eAd1
po	po	k7c6
celé	celá	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
narůstaly	narůstat	k5eAaImAgInP
poznatky	poznatek	k1gInPc1
o	o	k7c6
přípravě	příprava	k1gFnSc6
<g/>
,	,	kIx,
kvašení	kvašení	k1gNnSc4
a	a	k8xC
filtraci	filtrace	k1gFnSc4
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1876	#num#	k4
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
první	první	k4xOgInSc1
pivovar	pivovar	k1gInSc1
určený	určený	k2eAgInSc1d1
k	k	k7c3
masové	masový	k2eAgFnSc3d1
produkci	produkce	k1gFnSc3
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jím	jíst	k5eAaImIp1nS
americký	americký	k2eAgInSc1d1
pivovar	pivovar	k1gInSc1
Budweiser	Budweisra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
změně	změna	k1gFnSc3
trendů	trend	k1gInPc2
vyrábět	vyrábět	k5eAaImF
většinu	většina	k1gFnSc4
piva	pivo	k1gNnSc2
v	v	k7c6
masových	masový	k2eAgInPc6d1
pivovarech	pivovar	k1gInPc6
a	a	k8xC
začaly	začít	k5eAaPmAgFnP
se	se	k3xPyFc4
zakládat	zakládat	k5eAaImF
malé	malý	k2eAgInPc4d1
pivovary	pivovar	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
v	v	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
objevily	objevit	k5eAaPmAgFnP
převážně	převážně	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Unifikovaná	unifikovaný	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
vyráběná	vyráběný	k2eAgFnSc1d1
moderními	moderní	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
vaření	vaření	k1gNnSc2
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
CK	CK	kA
tanky	tank	k1gInPc4
(	(	kIx(
<g/>
cylindrokonické	cylindrokonický	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
,	,	kIx,
CKT	CKT	kA
<g/>
)	)	kIx)
a	a	k8xC
dodatečným	dodatečný	k2eAgNnSc7d1
ředěním	ředění	k1gNnSc7
třinácti-	třinácti-	k?
až	až	k8xS
patnáctistupňového	patnáctistupňový	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
vodou	voda	k1gFnSc7
obohacenou	obohacený	k2eAgFnSc7d1
oxidem	oxid	k1gInSc7
uhličitým	uhličitý	k2eAgInSc7d1
na	na	k7c4
požadovanou	požadovaný	k2eAgFnSc4d1
stupňovitost	stupňovitost	k1gFnSc4
pomocí	pomocí	k7c2
tzv.	tzv.	kA
HGB	HGB	kA
systému	systém	k1gInSc2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
lehce	lehko	k6eAd1
pejorativní	pejorativní	k2eAgNnSc1d1
označení	označení	k1gNnSc1
„	„	k?
<g/>
europivo	europivo	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
piva	pivo	k1gNnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
území	území	k1gNnSc2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
přinesl	přinést	k5eAaPmAgInS
důkazy	důkaz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
dokládají	dokládat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
dřívější	dřívější	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
připravovali	připravovat	k5eAaImAgMnP
kvašené	kvašený	k2eAgInPc4d1
nápoje	nápoj	k1gInPc4
z	z	k7c2
obilí	obilí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
jsou	být	k5eAaImIp3nP
podrobnější	podrobný	k2eAgInPc1d2
údaje	údaj	k1gInPc1
o	o	k7c6
přípravě	příprava	k1gFnSc6
piva	pivo	k1gNnSc2
keltskými	keltský	k2eAgMnPc7d1
Bóji	Bój	k1gMnPc7
<g/>
,	,	kIx,
germánskými	germánský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
Markomanů	Markoman	k1gMnPc2
a	a	k8xC
Kvádů	Kvád	k1gMnPc2
a	a	k8xC
Slovany	Slovan	k1gInPc1
bájného	bájný	k2eAgMnSc2d1
praotce	praotec	k1gMnSc2
Čecha	Čech	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
přišli	přijít	k5eAaPmAgMnP
na	na	k7c6
území	území	k1gNnSc6
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
trvale	trvale	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
usadili	usadit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3
doložený	doložený	k2eAgInSc1d1
český	český	k2eAgInSc1d1
pivovar	pivovar	k1gInSc1
je	být	k5eAaImIp3nS
Břevnovský	břevnovský	k2eAgInSc1d1
klášterní	klášterní	k2eAgInSc1d1
pivovar	pivovar	k1gInSc1
v	v	k7c6
Břevnovském	břevnovský	k2eAgInSc6d1
klášteru	klášter	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
založen	založen	k2eAgInSc1d1
roku	rok	k1gInSc2
993	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prvým	prvý	k4xOgInSc7
dokladem	doklad	k1gInSc7
souvisejícím	související	k2eAgInSc7d1
přímo	přímo	k6eAd1
s	s	k7c7
výrobou	výroba	k1gFnSc7
piva	pivo	k1gNnSc2
je	být	k5eAaImIp3nS
nadační	nadační	k2eAgFnSc1d1
listina	listina	k1gFnSc1
prvního	první	k4xOgMnSc2
českého	český	k2eAgMnSc2d1
krále	král	k1gMnSc2
Vratislava	Vratislav	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
pro	pro	k7c4
vyšehradskou	vyšehradský	k2eAgFnSc4d1
kapitulu	kapitula	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1088	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
mimo	mimo	k7c4
ostatní	ostatní	k2eAgInPc4d1
dary	dar	k1gInPc4
a	a	k8xC
privilegia	privilegium	k1gNnPc4
panovník	panovník	k1gMnSc1
přidělil	přidělit	k5eAaPmAgMnS
kapitule	kapitula	k1gFnSc3
desátek	desátek	k1gInSc4
chmele	chmel	k1gInSc2
na	na	k7c4
vaření	vaření	k1gNnSc4
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarším	starý	k2eAgInSc7d3
dokladem	doklad	k1gInSc7
o	o	k7c4
pěstování	pěstování	k1gNnSc4
chmele	chmel	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
je	být	k5eAaImIp3nS
nadační	nadační	k2eAgFnSc1d1
listina	listina	k1gFnSc1
ze	z	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
knížete	kníže	k1gMnSc2
Břetislava	Břetislav	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Největší	veliký	k2eAgInSc1d3
rozkvět	rozkvět	k1gInSc1
výroby	výroba	k1gFnSc2
piva	pivo	k1gNnSc2
na	na	k7c6
území	území	k1gNnSc6
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
nastal	nastat	k5eAaPmAgInS
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
kdy	kdy	k6eAd1
měl	mít	k5eAaImAgInS
na	na	k7c6
území	území	k1gNnSc6
státu	stát	k1gInSc2
právo	právo	k1gNnSc4
vařit	vařit	k5eAaImF
pivo	pivo	k1gNnSc4
kdokoliv	kdokoliv	k3yInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
těchto	tento	k3xDgFnPc6
starých	starý	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
pivo	pivo	k1gNnSc1
vařily	vařit	k5eAaImAgFnP
velmi	velmi	k6eAd1
primitivním	primitivní	k2eAgInSc7d1
postupem	postup	k1gInSc7
ženy	žena	k1gFnSc2
v	v	k7c6
každé	každý	k3xTgFnSc6
domácnosti	domácnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesloužilo	sloužit	k5eNaImAgNnS
jenom	jenom	k6eAd1
jako	jako	k8xS,k8xC
nápoj	nápoj	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
připravovaly	připravovat	k5eAaImAgInP
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gNnSc2
různé	různý	k2eAgInPc1d1
pokrmy	pokrm	k1gInPc1
jako	jako	k8xS,k8xC
polévky	polévka	k1gFnSc2
<g/>
,	,	kIx,
kaše	kaše	k1gFnSc2
a	a	k8xC
omáčky	omáčka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvoj	rozvoj	k1gInSc1
řemeslné	řemeslný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
piva	pivo	k1gNnSc2
nastal	nastat	k5eAaPmAgInS
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
se	s	k7c7
zakládáním	zakládání	k1gNnSc7
nových	nový	k2eAgNnPc2d1
královských	královský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
dostávala	dostávat	k5eAaImAgFnS
od	od	k7c2
panovníka	panovník	k1gMnSc2
řadu	řad	k1gInSc2
privilegií	privilegium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
pivovarství	pivovarství	k1gNnSc2
bylo	být	k5eAaImAgNnS
důležité	důležitý	k2eAgNnSc1d1
přidělení	přidělení	k1gNnSc1
práva	právo	k1gNnSc2
várečného	várečný	k2eAgNnSc2d1
a	a	k8xC
práva	právo	k1gNnSc2
mílového	mílový	k2eAgNnSc2d1
(	(	kIx(
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1788	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
tato	tento	k3xDgNnPc1
práva	právo	k1gNnPc1
získala	získat	k5eAaPmAgNnP
i	i	k9
poddanská	poddanský	k2eAgNnPc1d1
města	město	k1gNnPc1
od	od	k7c2
příslušné	příslušný	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc1d1
význam	význam	k1gInSc1
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
pivovarství	pivovarství	k1gNnSc2
a	a	k8xC
kvalitu	kvalita	k1gFnSc4
piva	pivo	k1gNnSc2
měly	mít	k5eAaImAgFnP
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
sladovnické	sladovnický	k2eAgInPc1d1
cechy	cech	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určovaly	určovat	k5eAaImAgInP
<g/>
,	,	kIx,
kolik	kolik	k4yQc4,k4yRc4,k4yIc4
piva	pivo	k1gNnSc2
a	a	k8xC
z	z	k7c2
jakého	jaký	k3yIgNnSc2,k3yQgNnSc2,k3yRgNnSc2
množství	množství	k1gNnSc2
sladu	slad	k1gInSc2
smí	smět	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
dům	dům	k1gInSc4
vyrobit	vyrobit	k5eAaPmF
<g/>
,	,	kIx,
kontrolovaly	kontrolovat	k5eAaImAgFnP
jeho	jeho	k3xOp3gFnSc4
kvalitu	kvalita	k1gFnSc4
a	a	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
okolních	okolní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
dohlížely	dohlížet	k5eAaImAgFnP
i	i	k9
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pivo	pivo	k1gNnSc4
vařil	vařit	k5eAaImAgMnS
jen	jen	k9
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
v	v	k7c6
tom	ten	k3xDgInSc6
oboru	obor	k1gInSc6
řádně	řádně	k6eAd1
vyučil	vyučit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byla	být	k5eAaImAgNnP
česká	český	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
již	již	k6eAd1
ve	v	k7c6
středověku	středověk	k1gInSc6
velmi	velmi	k6eAd1
kvalitní	kvalitní	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
hojné	hojný	k2eAgFnSc6d1
míře	míra	k1gFnSc6
se	se	k3xPyFc4
vyvážela	vyvážet	k5eAaImAgFnS
do	do	k7c2
okolních	okolní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
i	i	k9
na	na	k7c4
dvory	dvůr	k1gInPc4
jiných	jiný	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
až	až	k9
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zbohatlí	zbohatlý	k2eAgMnPc1d1
měšťané	měšťan	k1gMnPc1
sdružovali	sdružovat	k5eAaImAgMnP
své	svůj	k3xOyFgInPc4
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
a	a	k8xC
zakládali	zakládat	k5eAaImAgMnP
společné	společný	k2eAgInPc4d1
městské	městský	k2eAgInPc4d1
pivovary	pivovar	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
středověku	středověk	k1gInSc2
a	a	k8xC
kolem	kolem	k7c2
poloviny	polovina	k1gFnSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
rozvíjela	rozvíjet	k5eAaImAgFnS
výroba	výroba	k1gFnSc1
piva	pivo	k1gNnSc2
ve	v	k7c6
šlechtických	šlechtický	k2eAgInPc6d1
pivovarech	pivovar	k1gInPc6
<g/>
,	,	kIx,
stabilně	stabilně	k6eAd1
se	se	k3xPyFc4
udržovala	udržovat	k5eAaImAgFnS
či	či	k8xC
rozšiřovala	rozšiřovat	k5eAaImAgFnS
v	v	k7c6
klášterních	klášterní	k2eAgInPc6d1
pivovarech	pivovar	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
méně	málo	k6eAd2
podléhaly	podléhat	k5eAaImAgInP
vlivům	vliv	k1gInPc3
politických	politický	k2eAgFnPc2d1
a	a	k8xC
hospodářských	hospodářský	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městské	městský	k2eAgNnSc1d1
řemeslné	řemeslný	k2eAgNnSc1d1
pivovarství	pivovarství	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
upadat	upadat	k5eAaPmF,k5eAaImF
po	po	k7c6
roce	rok	k1gInSc6
1547	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
řada	řada	k1gFnSc1
měst	město	k1gNnPc2
vzbouřila	vzbouřit	k5eAaPmAgFnS
proti	proti	k7c3
nadvládě	nadvláda	k1gFnSc3
Habsburků	Habsburk	k1gMnPc2
a	a	k8xC
poté	poté	k6eAd1
jim	on	k3xPp3gMnPc3
byl	být	k5eAaImAgInS
konfiskován	konfiskovat	k5eAaBmNgInS
majetek	majetek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
velkou	velký	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
výroby	výroba	k1gFnSc2
sladu	slad	k1gInSc2
a	a	k8xC
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
prvním	první	k4xOgInSc7
krokem	krok	k1gInSc7
v	v	k7c6
rozvoji	rozvoj	k1gInSc6
typických	typický	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
současných	současný	k2eAgNnPc2d1
českých	český	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
český	český	k2eAgMnSc1d1
sládek	sládek	k1gMnSc1
František	František	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Poupě	Poupě	k1gMnSc1
(	(	kIx(
<g/>
1753	#num#	k4
<g/>
–	–	k?
<g/>
1805	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhl	navrhnout	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
nových	nový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
sladu	slad	k1gInSc2
a	a	k8xC
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
přesvědčoval	přesvědčovat	k5eAaImAgMnS
sládky	sládek	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
používali	používat	k5eAaImAgMnP
výhradně	výhradně	k6eAd1
ječný	ječný	k2eAgInSc4d1
slad	slad	k1gInSc4
<g/>
,	,	kIx,
upravil	upravit	k5eAaPmAgMnS
dávkování	dávkování	k1gNnSc4
chmele	chmel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sklonku	sklonek	k1gInSc6
života	život	k1gInSc2
založil	založit	k5eAaPmAgMnS
v	v	k7c6
Brně	Brno	k1gNnSc6
pivovarskou	pivovarský	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
zřejmě	zřejmě	k6eAd1
první	první	k4xOgInSc4
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
absolvovala	absolvovat	k5eAaPmAgFnS
ji	on	k3xPp3gFnSc4
řada	řada	k1gFnSc1
nejen	nejen	k6eAd1
českých	český	k2eAgMnPc2d1
sládků	sládek	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pivovarníků	pivovarník	k1gMnPc2
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vstupní	vstupní	k2eAgFnSc1d1
brána	brána	k1gFnSc1
plzeňského	plzeňský	k2eAgInSc2d1
pivovaru	pivovar	k1gInSc2
postavená	postavený	k2eAgFnSc1d1
k	k	k7c3
50	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
založení	založení	k1gNnSc2
</s>
<s>
Významným	významný	k2eAgInSc7d1
mezníkem	mezník	k1gInSc7
v	v	k7c6
českém	český	k2eAgNnSc6d1
pivovarství	pivovarství	k1gNnSc6
bylo	být	k5eAaImAgNnS
založení	založení	k1gNnSc1
Měšťanského	měšťanský	k2eAgInSc2d1
pivovaru	pivovar	k1gInSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
(	(	kIx(
<g/>
dnešního	dnešní	k2eAgInSc2d1
Prazdroje	prazdroj	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1842	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vyráběl	vyrábět	k5eAaImAgInS
výhradně	výhradně	k6eAd1
spodně	spodně	k6eAd1
kvašená	kvašený	k2eAgNnPc4d1
piva	pivo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pivo	pivo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
velmi	velmi	k6eAd1
dobrou	dobrý	k2eAgFnSc4d1
kvalitu	kvalita	k1gFnSc4
a	a	k8xC
během	během	k7c2
krátké	krátký	k2eAgFnSc2d1
doby	doba	k1gFnSc2
všechny	všechen	k3xTgInPc1
pivovary	pivovar	k1gInPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
na	na	k7c6
Moravě	Morava	k1gFnSc6
zavedly	zavést	k5eAaPmAgFnP
tuto	tento	k3xDgFnSc4
technologii	technologie	k1gFnSc4
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nastal	nastat	k5eAaPmAgInS
zlatý	zlatý	k2eAgInSc1d1
věk	věk	k1gInSc1
českého	český	k2eAgNnSc2d1
pivovarství	pivovarství	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
silně	silně	k6eAd1
ovlivnilo	ovlivnit	k5eAaPmAgNnS
vývoj	vývoj	k1gInSc4
tohoto	tento	k3xDgInSc2
oboru	obor	k1gInSc2
na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
období	období	k1gNnSc4
zahájení	zahájení	k1gNnSc2
průmyslové	průmyslový	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
sladu	slad	k1gInSc2
a	a	k8xC
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynikající	vynikající	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
českého	český	k2eAgNnSc2d1
pivovarství	pivovarství	k1gNnSc2
a	a	k8xC
kvalita	kvalita	k1gFnSc1
jeho	jeho	k3xOp3gInPc2
výrobků	výrobek	k1gInPc2
byla	být	k5eAaImAgFnS
podpořena	podpořit	k5eAaPmNgFnS
třemi	tři	k4xCgInPc7
základními	základní	k2eAgInPc7d1
faktory	faktor	k1gInPc7
<g/>
:	:	kIx,
optimálními	optimální	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
pro	pro	k7c4
pěstování	pěstování	k1gNnSc4
surovin	surovina	k1gFnPc2
sladovnického	sladovnický	k2eAgInSc2d1
ječmene	ječmen	k1gInSc2
a	a	k8xC
chmele	chmel	k1gInSc2
<g/>
,	,	kIx,
orientací	orientace	k1gFnSc7
rozvíjejícího	rozvíjející	k2eAgMnSc2d1
se	se	k3xPyFc4
strojírenského	strojírenský	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
na	na	k7c4
výrobu	výroba	k1gFnSc4
sladařských	sladařský	k2eAgFnPc2d1
a	a	k8xC
pivovarských	pivovarský	k2eAgFnPc2d1
zařízení	zařízení	k1gNnSc4
(	(	kIx(
<g/>
vyvážela	vyvážet	k5eAaImAgFnS
se	se	k3xPyFc4
záhy	záhy	k6eAd1
do	do	k7c2
400	#num#	k4
zemí	zem	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
zajištěním	zajištění	k1gNnSc7
jak	jak	k6eAd1
středoškolské	středoškolský	k2eAgFnSc2d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
vysokoškolské	vysokoškolský	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
pivovarských	pivovarský	k2eAgMnPc2d1
odborníků	odborník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
uvolnila	uvolnit	k5eAaPmAgFnS
možnost	možnost	k1gFnSc1
výstavby	výstavba	k1gFnSc2
pivovarů	pivovar	k1gInPc2
a	a	k8xC
v	v	k7c6
Čechách	Čechy	k1gFnPc6
vzniklo	vzniknout	k5eAaPmAgNnS
okolo	okolo	k6eAd1
30	#num#	k4
nových	nový	k2eAgMnPc2d1
měšťanských	měšťanský	k2eAgMnPc2d1
<g/>
,	,	kIx,
akciových	akciový	k2eAgInPc2d1
a	a	k8xC
soukromých	soukromý	k2eAgInPc2d1
pivovarů	pivovar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
zvyšovala	zvyšovat	k5eAaImAgFnS
celková	celkový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
piva	pivo	k1gNnSc2
koncentrovaná	koncentrovaný	k2eAgFnSc1d1
do	do	k7c2
větších	veliký	k2eAgInPc2d2
pivovarů	pivovar	k1gInPc2
<g/>
,	,	kIx,
malé	malý	k2eAgInPc1d1
pivovary	pivovar	k1gInPc1
zanikaly	zanikat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pivo	pivo	k1gNnSc1
se	se	k3xPyFc4
vyváželo	vyvážet	k5eAaImAgNnS
prakticky	prakticky	k6eAd1
do	do	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
nová	nový	k2eAgFnSc1d1
Československá	československý	k2eAgFnSc1d1
republika	republika	k1gFnSc1
převzala	převzít	k5eAaPmAgFnS
z	z	k7c2
bývalé	bývalý	k2eAgFnSc2d1
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
asi	asi	k9
60	#num#	k4
%	%	kIx~
výrobního	výrobní	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
pivovarů	pivovar	k1gInPc2
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
562	#num#	k4
pivovarů	pivovar	k1gInPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
poměrně	poměrně	k6eAd1
špatném	špatný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katastrofu	katastrofa	k1gFnSc4
českému	český	k2eAgNnSc3d1
pivovarnictví	pivovarnictví	k1gNnSc3
jako	jako	k8xC,k8xS
veškerému	veškerý	k3xTgInSc3
světovému	světový	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
přinesla	přinést	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
řada	řada	k1gFnSc1
uzavřených	uzavřený	k2eAgInPc2d1
pivovarů	pivovar	k1gInPc2
již	již	k6eAd1
neobnovila	obnovit	k5eNaPmAgFnS
svoji	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pivovarský	pivovarský	k2eAgInSc4d1
a	a	k8xC
sladařský	sladařský	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
byl	být	k5eAaImAgMnS
postupně	postupně	k6eAd1
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Československé	československý	k2eAgFnSc6d1
republice	republika	k1gFnSc6
zestátněn	zestátněn	k2eAgInSc1d1
a	a	k8xC
centrálně	centrálně	k6eAd1
řízen	řídit	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s>
Nálepky	nálepek	k1gInPc1
piv	pivo	k1gNnPc2
z	z	k7c2
pivovaru	pivovar	k1gInSc2
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
z	z	k7c2
dob	doba	k1gFnPc2
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
socialistického	socialistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
nebyly	být	k5eNaImAgFnP
do	do	k7c2
pivovarů	pivovar	k1gInPc2
a	a	k8xC
sladoven	sladovna	k1gFnPc2
vkládány	vkládat	k5eAaImNgInP
potřebné	potřebný	k2eAgInPc1d1
finanční	finanční	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
na	na	k7c4
modernizaci	modernizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
pivovarský	pivovarský	k2eAgInSc1d1
obor	obor	k1gInSc1
zajistil	zajistit	k5eAaPmAgInS
na	na	k7c6
domácím	domácí	k2eAgInSc6d1
trhu	trh	k1gInSc6
dostatek	dostatek	k1gInSc1
piva	pivo	k1gNnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
schopný	schopný	k2eAgMnSc1d1
vyvážet	vyvážet	k5eAaImF
pivo	pivo	k1gNnSc4
a	a	k8xC
slad	slad	k1gInSc4
nejen	nejen	k6eAd1
do	do	k7c2
tzv.	tzv.	kA
socialistických	socialistický	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c4
náročný	náročný	k2eAgInSc4d1
trh	trh	k1gInSc4
kapitalistických	kapitalistický	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgInPc4d1
válce	válec	k1gInPc4
byly	být	k5eAaImAgFnP
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
postaveny	postavit	k5eAaPmNgInP
pouze	pouze	k6eAd1
dva	dva	k4xCgInPc1
pivovary	pivovar	k1gInPc1
<g/>
:	:	kIx,
Radegast	Radegast	k1gMnSc1
a	a	k8xC
Most	most	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgMnSc1
jmenovaný	jmenovaný	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
uzavřen	uzavřít	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sladoven	sladovna	k1gFnPc2
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
pět	pět	k4xCc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
v	v	k7c6
období	období	k1gNnSc6
socialistického	socialistický	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
osm	osm	k4xCc4
pivovarů	pivovar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
privatizace	privatizace	k1gFnSc1
pivovarů	pivovar	k1gInPc2
<g/>
,	,	kIx,
mnohé	mnohý	k2eAgFnPc1d1
zanikly	zaniknout	k5eAaPmAgFnP
<g/>
,	,	kIx,
do	do	k7c2
některých	některý	k3yIgInPc2
vstoupil	vstoupit	k5eAaPmAgInS
zahraniční	zahraniční	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
36	#num#	k4
sladoven	sladovna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
vyrobily	vyrobit	k5eAaPmAgFnP
483	#num#	k4
693	#num#	k4
tun	tuna	k1gFnPc2
sladu	slad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
vývoz	vývoz	k1gInSc1
činil	činit	k5eAaImAgInS
213	#num#	k4
324	#num#	k4
tun	tuna	k1gFnPc2
(	(	kIx(
<g/>
44,10	44,10	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činných	činný	k2eAgInPc2d1
průmyslových	průmyslový	k2eAgInPc2d1
pivovarů	pivovar	k1gInPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
48	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
vyrobily	vyrobit	k5eAaPmAgInP
18	#num#	k4
548	#num#	k4
314	#num#	k4
hl	hl	k?
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgInSc2
bylo	být	k5eAaImAgNnS
vyvezeno	vyvézt	k5eAaPmNgNnS
2	#num#	k4
129	#num#	k4
848	#num#	k4
hl	hl	k?
(	(	kIx(
<g/>
11,48	11,48	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
roční	roční	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
na	na	k7c4
jednoho	jeden	k4xCgMnSc4
obyvatele	obyvatel	k1gMnSc4
činila	činit	k5eAaImAgFnS
160,9	160,9	k4
l	l	kA
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedm	sedm	k4xCc1
největších	veliký	k2eAgInPc2d3
pivovarů	pivovar	k1gInPc2
pokrývá	pokrývat	k5eAaImIp3nS
84	#num#	k4
%	%	kIx~
produkce	produkce	k1gFnSc2
českého	český	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pivovary	pivovar	k1gInPc4
Plzeňský	plzeňský	k2eAgInSc1d1
Prazdroj	prazdroj	k1gInSc1
<g/>
,	,	kIx,
Budějovický	budějovický	k2eAgInSc1d1
Budvar	budvar	k1gInSc1
<g/>
,	,	kIx,
Staropramen	staropramen	k1gInSc1
<g/>
,	,	kIx,
Královský	královský	k2eAgInSc1d1
Pivovar	pivovar	k1gInSc1
Krušovice	Krušovice	k1gFnSc2
<g/>
,	,	kIx,
PMS	PMS	kA
Přerov	Přerov	k1gInSc1
<g/>
,	,	kIx,
Drinks	Drinks	k1gInSc1
Union	union	k1gInSc1
a	a	k8xC
Starobrno	Starobrno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dělení	dělení	k1gNnSc1
piva	pivo	k1gNnSc2
</s>
<s>
Sklenice	sklenice	k1gFnPc1
Měšťanského	měšťanský	k2eAgInSc2d1
pivovaru	pivovar	k1gInSc2
v	v	k7c6
Poličce	Polička	k1gFnSc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Druhy	druh	k1gInPc4
piv	pivo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Pivo	pivo	k1gNnSc1
se	se	k3xPyFc4
rozděluje	rozdělovat	k5eAaImIp3nS
z	z	k7c2
několika	několik	k4yIc2
různých	různý	k2eAgNnPc2d1
hledisek	hledisko	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
nejdůležitější	důležitý	k2eAgMnPc1d3
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
stupňovitost	stupňovitost	k1gFnSc1
<g/>
,	,	kIx,
barva	barva	k1gFnSc1
a	a	k8xC
pivní	pivní	k2eAgInSc1d1
styl	styl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Stupňovitost	stupňovitost	k1gFnSc1
</s>
<s>
Stupňovitost	stupňovitost	k1gFnSc1
piva	pivo	k1gNnSc2
se	se	k3xPyFc4
udává	udávat	k5eAaImIp3nS
v	v	k7c6
EPM	EPM	kA
(	(	kIx(
<g/>
extrakt	extrakt	k1gInSc1
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
také	také	k9
nazývá	nazývat	k5eAaImIp3nS
stupnice	stupnice	k1gFnSc1
Ballinga	Ballinga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
udává	udávat	k5eAaImIp3nS
obsah	obsah	k1gInSc4
všech	všecek	k3xTgFnPc2
extraktivních	extraktivní	k2eAgFnPc2d1
látek	látka	k1gFnPc2
v	v	k7c6
mladině	mladina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
o	o	k7c4
cukry	cukr	k1gInPc4
(	(	kIx(
<g/>
zkvasitelné	zkvasitelný	k2eAgInPc4d1
a	a	k8xC
nezkvasitelné	zkvasitelný	k2eNgInPc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
zkvasitelných	zkvasitelný	k2eAgInPc2d1
cukrů	cukr	k1gInPc2
pak	pak	k6eAd1
při	při	k7c6
kvašení	kvašení	k1gNnSc6
piva	pivo	k1gNnSc2
vzniká	vznikat	k5eAaImIp3nS
alkohol	alkohol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stupňovitost	stupňovitost	k1gFnSc1
piva	pivo	k1gNnSc2
tedy	tedy	k8xC
udává	udávat	k5eAaImIp3nS
plnost	plnost	k1gFnSc4
(	(	kIx(
<g/>
hutnost	hutnost	k1gFnSc4
<g/>
)	)	kIx)
piva	pivo	k1gNnSc2
→	→	k?
10	#num#	k4
<g/>
°	°	k?
tedy	tedy	k9
obsahuje	obsahovat	k5eAaImIp3nS
10	#num#	k4
%	%	kIx~
těchto	tento	k3xDgFnPc2
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
pak	pak	k6eAd1
12	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
atd.	atd.	kA
→	→	k?
čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
více	hodně	k6eAd2
extraktivních	extraktivní	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
plnější	plný	k2eAgNnSc1d2
pivo	pivo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
také	také	k6eAd1
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
stupňovitost	stupňovitost	k1gFnSc1
ve	v	k7c6
většině	většina	k1gFnSc6
pivních	pivní	k2eAgInPc2d1
stylů	styl	k1gInPc2
přímo	přímo	k6eAd1
úměrná	úměrná	k1gFnSc1
obsahu	obsah	k1gInSc2
alkoholu	alkohol	k1gInSc2
(	(	kIx(
<g/>
čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
více	hodně	k6eAd2
zkvasitelných	zkvasitelný	k2eAgInPc2d1
cukrů	cukr	k1gInPc2
mladina	mladina	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
vznikne	vzniknout	k5eAaPmIp3nS
při	při	k7c6
vaření	vaření	k1gNnSc6
piva	pivo	k1gNnSc2
alkoholu	alkohol	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
piva	pivo	k1gNnSc2
dle	dle	k7c2
stupňovitosti	stupňovitost	k1gFnSc2
není	být	k5eNaImIp3nS
jednotné	jednotný	k2eAgNnSc1d1
a	a	k8xC
liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
podle	podle	k7c2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Stolní	stolní	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
1,01	1,01	k4
až	až	k9
6,99	6,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Výčepní	výčepní	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
7,00	7,00	k4
až	až	k9
10,99	10,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Ležák	ležák	k1gInSc1
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
11,00	11,00	k4
až	až	k9
12,99	12,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Speciální	speciální	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
13,00	13,00	k4
až	až	k9
17,99	17,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Porter	porter	k1gInSc1
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
18,00	18,00	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
a	a	k8xC
pak	pak	k6eAd1
také	také	k9
dvě	dva	k4xCgFnPc1
speciální	speciální	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
</s>
<s>
Nealkoholické	alkoholický	k2eNgNnSc1d1
pivo	pivo	k1gNnSc1
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
méně	málo	k6eAd2
než	než	k8xS
0,40	0,40	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Pivo	pivo	k1gNnSc1
se	s	k7c7
sníženým	snížený	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
alkoholu	alkohol	k1gInSc2
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
0,41	0,41	k4
až	až	k9
1,00	1,00	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Takto	takto	k6eAd1
rozděluje	rozdělovat	k5eAaImIp3nS
pivo	pivo	k1gNnSc1
předpis	předpis	k1gInSc1
č.	č.	k?
335	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Ministerstva	ministerstvo	k1gNnSc2
zemědělství	zemědělství	k1gNnSc2
a	a	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
rok	rok	k1gInSc1
2016	#num#	k4
<g/>
)	)	kIx)
probíhá	probíhat	k5eAaImIp3nS
změna	změna	k1gFnSc1
z	z	k7c2
důvodu	důvod	k1gInSc2
jeho	jeho	k3xOp3gFnSc2
nepřesnosti	nepřesnost	k1gFnSc2
a	a	k8xC
zastaralosti	zastaralost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Einfachbier	Einfachbier	k1gInSc1
„	„	k?
<g/>
(	(	kIx(
<g/>
jednoduché	jednoduchý	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
”	”	k?
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
1,51	1,51	k4
až	až	k9
6,99	6,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Schankbier	Schankbier	k1gInSc1
„	„	k?
<g/>
(	(	kIx(
<g/>
výčepní	výčepní	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
”	”	k?
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
7,00	7,00	k4
až	až	k9
10,99	10,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Vollbier	Vollbier	k1gInSc1
„	„	k?
<g/>
(	(	kIx(
<g/>
plné	plný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
”	”	k?
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
11,00	11,00	k4
až	až	k9
15,99	15,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Starkbier	Starkbier	k1gInSc1
„	„	k?
<g/>
(	(	kIx(
<g/>
silné	silný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
”	”	k?
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
16,00	16,00	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Abzugsbier	Abzugsbier	k1gInSc1
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
9,00	9,00	k4
až	až	k9
9,99	9,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Schankbier	Schankbier	k1gInSc1
„	„	k?
<g/>
(	(	kIx(
<g/>
výčepní	výčepní	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
”	”	k?
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
10,00	10,00	k4
až	až	k9
11,99	11,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Vollbier	Vollbier	k1gInSc1
„	„	k?
<g/>
(	(	kIx(
<g/>
plné	plný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
”	”	k?
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
12,00	12,00	k4
až	až	k9
14,99	14,99	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Spezialbier	Spezialbier	k1gInSc1
„	„	k?
<g/>
(	(	kIx(
<g/>
speciální	speciální	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
”	”	k?
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
13,00	13,00	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Starkbier	Starkbier	k1gInSc1
„	„	k?
<g/>
(	(	kIx(
<g/>
silné	silný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
”	”	k?
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
16,00	16,00	k4
%	%	kIx~
hmotnostního	hmotnostní	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
Barva	barva	k1gFnSc1
piva	pivo	k1gNnSc2
popisuje	popisovat	k5eAaImIp3nS
pivo	pivo	k1gNnSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
zrakového	zrakový	k2eAgNnSc2d1
vnímání	vnímání	k1gNnSc2
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
platných	platný	k2eAgInPc2d1
český	český	k2eAgInSc4d1
zákonů	zákon	k1gInPc2
rozlišujeme	rozlišovat	k5eAaImIp1nP
pivo	pivo	k1gNnSc4
světlé	světlý	k2eAgFnSc2d1
<g/>
,	,	kIx,
polotmavé	polotmavý	k2eAgFnSc2d1
<g/>
,	,	kIx,
tmavé	tmavý	k2eAgFnSc2d1
a	a	k8xC
řezané	řezaný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Světlé	světlý	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
–	–	k?
pivo	pivo	k1gNnSc1
vařené	vařený	k2eAgNnSc1d1
především	především	k9
ze	z	k7c2
světlých	světlý	k2eAgInPc2d1
sladů	slad	k1gInPc2
</s>
<s>
Polotmavá	polotmavý	k2eAgNnPc1d1
a	a	k8xC
tmavá	tmavý	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
–	–	k?
pivo	pivo	k1gNnSc4
vařené	vařený	k2eAgFnSc2d1
z	z	k7c2
tmavých	tmavý	k2eAgInPc2d1
<g/>
,	,	kIx,
karamelových	karamelový	k2eAgInPc2d1
<g/>
,	,	kIx,
popř.	popř.	kA
barevných	barevný	k2eAgInPc2d1
sladů	slad	k1gInPc2
spolu	spolu	k6eAd1
se	s	k7c7
světlými	světlý	k2eAgInPc7d1
slady	slad	k1gInPc7
</s>
<s>
Řezané	řezaný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
–	–	k?
pivo	pivo	k1gNnSc1
smíšené	smíšený	k2eAgNnSc1d1
ze	z	k7c2
světlých	světlý	k2eAgNnPc2d1
a	a	k8xC
tmavých	tmavý	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
</s>
<s>
SRM	SRM	kA
<g/>
/	/	kIx~
<g/>
Lovibond	Lovibond	k1gMnSc1
</s>
<s>
Příklad	příklad	k1gInSc1
</s>
<s>
Barva	barva	k1gFnSc1
piva	pivo	k1gNnSc2
</s>
<s>
EBC	EBC	kA
</s>
<s>
2	#num#	k4
</s>
<s>
Pale	pal	k1gInSc5
lager	lager	k1gMnSc1
<g/>
,	,	kIx,
Witbier	Witbier	k1gMnSc1
<g/>
,	,	kIx,
Pilsener	Pilsener	k1gMnSc1
<g/>
,	,	kIx,
Berliner	Berliner	k1gMnSc1
Weisse	Weiss	k1gMnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Maibock	Maibock	k6eAd1
<g/>
,	,	kIx,
Blonde	blond	k1gInSc5
Ale	ale	k9
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Weissbier	Weissbier	k1gMnSc1
</s>
<s>
8	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
American	American	k1gMnSc1
Pale	pal	k1gInSc5
Ale	ale	k9
<g/>
,	,	kIx,
India	indium	k1gNnPc1
Pale	pal	k1gInSc5
Ale	ale	k9
</s>
<s>
12	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Weissbier	Weissbier	k1gMnSc1
<g/>
,	,	kIx,
Saison	Saison	k1gMnSc1
</s>
<s>
16	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
English	English	k1gMnSc1
Bitter	Bitter	k1gMnSc1
<g/>
,	,	kIx,
ESB	ESB	kA
</s>
<s>
20	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Biere	Bier	k1gMnSc5
de	de	k?
Garde	garde	k1gInSc1
<g/>
,	,	kIx,
Double	double	k1gInSc1
IPA	IPA	kA
</s>
<s>
26	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
Dark	Dark	k1gMnSc1
lager	lager	k1gMnSc1
<g/>
,	,	kIx,
Vienna	Vienna	k1gFnSc1
lager	lagra	k1gFnPc2
<g/>
,	,	kIx,
Märzen	Märzna	k1gFnPc2
<g/>
,	,	kIx,
Amber	ambra	k1gFnPc2
Ale	ale	k8xC
</s>
<s>
33	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
Brown	Brown	k1gMnSc1
Ale	ale	k9
<g/>
,	,	kIx,
Bock	Bock	k1gMnSc1
<g/>
,	,	kIx,
Dunkel	Dunkel	k1gMnSc1
<g/>
,	,	kIx,
Dunkelweizen	Dunkelweizen	k2eAgMnSc1d1
</s>
<s>
39	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
Irish	Irish	k1gMnSc1
Dry	Dry	k1gMnSc1
Stout	Stout	k1gMnSc1
<g/>
,	,	kIx,
Doppelbock	Doppelbock	k1gMnSc1
<g/>
,	,	kIx,
Porter	porter	k1gInSc1
</s>
<s>
47	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
Stout	Stout	k1gMnSc1
</s>
<s>
57	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
Foreign	Foreign	k1gMnSc1
Stout	Stout	k1gMnSc1
<g/>
,	,	kIx,
Baltic	Baltice	k1gFnPc2
Porter	porter	k1gInSc1
</s>
<s>
69	#num#	k4
</s>
<s>
40	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Imperial	Imperial	k1gMnSc1
Stout	Stout	k1gMnSc1
</s>
<s>
79	#num#	k4
</s>
<s>
Způsoby	způsob	k1gInPc1
kvašení	kvašení	k1gNnSc2
</s>
<s>
Svrchně	svrchně	k6eAd1
kvašená	kvašený	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
–	–	k?
svrchně	svrchně	k6eAd1
kvašená	kvašený	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
vznikají	vznikat	k5eAaImIp3nP
při	při	k7c6
teplotách	teplota	k1gFnPc6
okolo	okolo	k7c2
15	#num#	k4
–	–	k?
20	#num#	k4
°	°	k?
<g/>
C	C	kA
s	s	k7c7
využitím	využití	k1gNnSc7
kvasinek	kvasinka	k1gFnPc2
Saccharomyces	Saccharomyces	k1gInSc4
cerevisiae	cerevisiaat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
<g/>
,	,	kIx,
pšeničné	pšeničný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
,	,	kIx,
stout	stout	k1gInSc1
<g/>
,	,	kIx,
porter	porter	k1gInSc1
<g/>
,	,	kIx,
trappist	trappist	k1gInSc1
</s>
<s>
Spodně	spodně	k6eAd1
kvašená	kvašený	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
(	(	kIx(
<g/>
ležáky	ležák	k1gInPc1
<g/>
)	)	kIx)
–	–	k?
spodně	spodně	k6eAd1
kvašená	kvašený	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
vznikají	vznikat	k5eAaImIp3nP
za	za	k7c4
kvašení	kvašení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
probíhá	probíhat	k5eAaImIp3nS
při	při	k7c6
nižších	nízký	k2eAgFnPc6d2
teplotách	teplota	k1gFnPc6
pohybujících	pohybující	k2eAgFnPc6d1
se	se	k3xPyFc4
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
8	#num#	k4
–	–	k?
14	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Kvašení	kvašení	k1gNnSc1
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nS
kvasinky	kvasinka	k1gFnSc2
Saccharomyces	Saccharomyces	k1gMnSc1
pastorianus	pastorianus	k1gMnSc1
–	–	k?
Pilsner	Pilsner	k1gMnSc1
<g/>
,	,	kIx,
Bock	Bock	k1gMnSc1
<g/>
,	,	kIx,
Märzen	Märzen	k2eAgMnSc1d1
<g/>
,	,	kIx,
piva	pivo	k1gNnSc2
bavorského	bavorský	k2eAgInSc2d1
typu	typ	k1gInSc2
</s>
<s>
Spontánně	spontánně	k6eAd1
kvašená	kvašený	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
–	–	k?
v	v	k7c6
historii	historie	k1gFnSc6
jediný	jediný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
kvašení	kvašení	k1gNnSc2
piva	pivo	k1gNnSc2
–	–	k?
zkvasí	zkvasit	k5eAaPmIp3nS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
je	být	k5eAaImIp3nS
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
a	a	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
zbude	zbýt	k5eAaPmIp3nS
v	v	k7c6
sudech	sud	k1gInPc6
po	po	k7c6
předchozí	předchozí	k2eAgFnSc6d1
várce	várka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
tato	tento	k3xDgNnPc1
piva	pivo	k1gNnPc1
nejvíce	hodně	k6eAd3,k6eAd1
rozšířena	rozšířen	k2eAgNnPc1d1
v	v	k7c6
Belgii	Belgie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgFnPc6
částech	část	k1gFnPc6
Francie	Francie	k1gFnSc2
a	a	k8xC
Nizozemí	Nizozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lambik	Lambik	k1gInSc1
<g/>
,	,	kIx,
Gueuze	Gueuze	k1gFnSc1
<g/>
,	,	kIx,
Kriek	Kriek	k1gInSc1
<g/>
,	,	kIx,
Frambozen	Frambozen	k2eAgInSc1d1
<g/>
,	,	kIx,
Faro	fara	k1gFnSc5
</s>
<s>
Pivní	pivní	k2eAgInSc1d1
styl	styl	k1gInSc1
</s>
<s>
Svrchně	svrchně	k6eAd1
kvašená	kvašený	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
obvykle	obvykle	k6eAd1
kvasí	kvasit	k5eAaImIp3nP
při	při	k7c6
teplotách	teplota	k1gFnPc6
kolem	kolem	k7c2
15	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
však	však	k9
i	i	k9
vyšších	vysoký	k2eAgFnPc2d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
vytvářejí	vytvářet	k5eAaImIp3nP
na	na	k7c6
povrchu	povrch	k1gInSc6
kvasícího	kvasící	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
pěnu	pěn	k2eAgFnSc4d1
způsobenou	způsobený	k2eAgFnSc4d1
stoupajícím	stoupající	k2eAgMnPc3d1
CO2	CO2	k1gMnPc3
spolu	spolu	k6eAd1
s	s	k7c7
kvasnicemi	kvasnice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
tato	tento	k3xDgNnPc1
piva	pivo	k1gNnPc1
nazývají	nazývat	k5eAaImIp3nP
svrchně	svrchně	k6eAd1
kvašená	kvašený	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doba	doba	k1gFnSc1
kvašení	kvašení	k1gNnSc2
těchto	tento	k3xDgNnPc2
piv	pivo	k1gNnPc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
tří	tři	k4xCgInPc2
týdnů	týden	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
však	však	k9
mohou	moct	k5eAaImIp3nP
kvasit	kvasit	k5eAaImF
i	i	k9
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Ale	ale	k9
(	(	kIx(
<g/>
„	„	k?
<g/>
Ejl	Ejl	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
–	–	k?
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nejběžnější	běžný	k2eAgInSc4d3
styl	styl	k1gInSc4
svrchně	svrchně	k6eAd1
kvašený	kvašený	k2eAgInSc4d1
piv	pivo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piva	pivo	k1gNnPc1
můžou	můžou	k?
mít	mít	k5eAaImF
rozličnou	rozličný	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
i	i	k8xC
stupňovitost	stupňovitost	k1gFnSc4
<g/>
.	.	kIx.
„	„	k?
<g/>
Ejly	Ejla	k1gFnSc2
<g/>
“	“	k?
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
širokou	široký	k2eAgFnSc7d1
škálou	škála	k1gFnSc7
chutí	chuť	k1gFnSc7
a	a	k8xC
vůní	vůně	k1gFnSc7
<g/>
,	,	kIx,
často	často	k6eAd1
zanechávají	zanechávat	k5eAaImIp3nP
po	po	k7c6
vypití	vypití	k1gNnSc6
tóny	tón	k1gInPc4
citrusů	citrus	k1gInPc2
či	či	k8xC
rozličných	rozličný	k2eAgNnPc2d1
koření	koření	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
označením	označení	k1gNnSc7
ale	ale	k8xC
se	se	k3xPyFc4
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
i	i	k9
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
označením	označení	k1gNnSc7
i	i	k8xC
celé	celý	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
svrchně	svrchně	k6eAd1
kvašených	kvašený	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Ejly	Ejla	k1gFnSc2
<g/>
“	“	k?
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
především	především	k9
podle	podle	k7c2
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
uvařeny	uvařen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Ale	ale	k9
anglo-amerického	anglo-americký	k2eAgInSc2d1
typu	typ	k1gInSc2
</s>
<s>
Pale	pal	k1gInSc5
Ale	ale	k9
(	(	kIx(
<g/>
světlý	světlý	k2eAgInSc4d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
plné	plný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
se	s	k7c7
zlatou	zlatá	k1gFnSc7
až	až	k9
tmavě	tmavě	k6eAd1
měděnou	měděný	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
a	a	k8xC
s	s	k7c7
plnou	plný	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
s	s	k7c7
ovocnými	ovocný	k2eAgInPc7d1
tóny	tón	k1gInPc7
</s>
<s>
Brown	Brown	k1gInSc1
Ale	ale	k9
(	(	kIx(
<g/>
tmavý	tmavý	k2eAgInSc4d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
plné	plný	k2eAgNnSc4d1
až	až	k8xS
silné	silný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
tmavě	tmavě	k6eAd1
jantarové	jantarový	k2eAgFnPc1d1
až	až	k8xS
hnědé	hnědý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
chuť	chuť	k1gFnSc1
obvykle	obvykle	k6eAd1
zabarvena	zabarvit	k5eAaPmNgFnS
čokoládově	čokoládově	k6eAd1
či	či	k8xC
oříškově	oříškově	k6eAd1
<g/>
,	,	kIx,
vařeno	vařen	k2eAgNnSc1d1
z	z	k7c2
tmavých	tmavý	k2eAgInPc2d1
sladů	slad	k1gInPc2
</s>
<s>
Mild	Mild	k6eAd1
Ale	ale	k9
(	(	kIx(
<g/>
mladý	mladý	k2eAgMnSc1d1
či	či	k8xC
lehký	lehký	k2eAgInSc1d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
výčepní	výčepní	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
obvykle	obvykle	k6eAd1
světlé	světlý	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
s	s	k7c7
nižším	nízký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
alkoholu	alkohol	k1gInSc2
</s>
<s>
India	indium	k1gNnPc1
Pale	pal	k1gInSc5
Ale	ale	k9
(	(	kIx(
<g/>
světlý	světlý	k2eAgInSc1d1
chmelený	chmelený	k2eAgInSc1d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
vychází	vycházet	k5eAaImIp3nS
k	k	k7c3
klasického	klasický	k2eAgNnSc2d1
pale	pal	k1gInSc5
ale	ale	k8xC
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
chmelené	chmelený	k2eAgNnSc1d1
<g/>
,	,	kIx,
čemuž	což	k3yRnSc3,k3yQnSc3
odpovídá	odpovídat	k5eAaImIp3nS
i	i	k9
jeho	jeho	k3xOp3gFnSc4
chuť	chuť	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
amerických	americký	k2eAgFnPc6d1
verzích	verze	k1gFnPc6
má	mít	k5eAaImIp3nS
pak	pak	k6eAd1
toto	tento	k3xDgNnSc4
pivo	pivo	k1gNnSc4
mnohem	mnohem	k6eAd1
vyšší	vysoký	k2eAgInSc1d2
obsah	obsah	k1gInSc1
alkoholu	alkohol	k1gInSc2
</s>
<s>
Old	Olda	k1gFnPc2
Ale	ale	k8xC
(	(	kIx(
<g/>
starý	starý	k2eAgMnSc1d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
silné	silný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
tmavě	tmavě	k6eAd1
jantarové	jantarový	k2eAgNnSc4d1
až	až	k9
skoro	skoro	k6eAd1
černé	černý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
sladová	sladový	k2eAgFnSc1d1
chuť	chuť	k1gFnSc1
s	s	k7c7
podtóny	podtón	k1gInPc7
rybízu	rybíz	k1gInSc2
či	či	k8xC
hrozinek	hrozinka	k1gFnPc2
<g/>
,	,	kIx,
proces	proces	k1gInSc1
kvašení	kvašení	k1gNnSc2
může	moct	k5eAaImIp3nS
trvat	trvat	k5eAaImF
i	i	k9
několik	několik	k4yIc4
let	léto	k1gNnPc2
</s>
<s>
Cascadian	Cascadian	k1gMnSc1
Dark	Dark	k1gMnSc1
Ale	ale	k8xC
(	(	kIx(
<g/>
tmavý	tmavý	k2eAgMnSc1d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
silné	silný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
tmavé	tmavý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
tmavou	tmavý	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
Indian	Indiana	k1gFnPc2
Pale	pal	k1gInSc5
Ale	ale	k9
</s>
<s>
Strong	Strong	k1gInSc1
Ale	ale	k9
(	(	kIx(
<g/>
silný	silný	k2eAgInSc4d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
silné	silný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
tmavé	tmavý	k2eAgFnSc2d1
jantarové	jantarový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
alkoholu	alkohol	k1gInSc2
</s>
<s>
Amber	ambra	k1gFnPc2
Ale	ale	k8xC
(	(	kIx(
<g/>
polotmavý	polotmavý	k2eAgMnSc1d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
plné	plný	k2eAgNnSc4d1
až	až	k8xS
silné	silný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
měděné	měděný	k2eAgFnSc2d1
až	až	k8xS
hnědé	hnědý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
s	s	k7c7
chmelovou	chmelový	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
</s>
<s>
Ale	ale	k9
belgicko-francouzského	belgicko-francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
</s>
<s>
Tripel	tripel	k1gInSc1
–	–	k?
silné	silný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
žluté	žlutý	k2eAgFnSc2d1
až	až	k8xS
zlaté	zlatý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
alkoholu	alkohol	k1gInSc2
<g/>
,	,	kIx,
chuť	chuť	k1gFnSc1
kvasnicová	kvasnicový	k2eAgFnSc1d1
<g/>
,	,	kIx,
kořenná	kořenný	k2eAgFnSc1d1
</s>
<s>
Strong	Strong	k1gMnSc1
Dark	Dark	k1gMnSc1
Ale	ale	k9
(	(	kIx(
<g/>
silný	silný	k2eAgInSc4d1
tmavý	tmavý	k2eAgInSc4d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
silné	silný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
tmavé	tmavý	k2eAgFnSc2d1
jantarové	jantarový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
alkoholu	alkohol	k1gInSc2
</s>
<s>
Dubbel	Dubbel	k1gMnSc1
–	–	k?
klášterní	klášterní	k2eAgNnSc1d1
silné	silný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
,	,	kIx,
tmavé	tmavý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
s	s	k7c7
výraznou	výrazný	k2eAgFnSc7d1
ovocnou	ovocný	k2eAgFnSc7d1
a	a	k8xC
obilnou	obilný	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
</s>
<s>
Saison	Saison	k1gMnSc1
</s>
<s>
Flanders	Flanders	k1gInSc1
Red	Red	k1gFnSc2
Ale	ale	k8xC
(	(	kIx(
<g/>
vlámský	vlámský	k2eAgInSc1d1
červený	červený	k2eAgInSc1d1
ejl	ejl	k?
<g/>
)	)	kIx)
–	–	k?
plné	plný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
tmavě	tmavě	k6eAd1
červené	červený	k2eAgFnPc1d1
až	až	k8xS
hnědé	hnědý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
trpké	trpký	k2eAgFnSc3d1
až	až	k8xS
kyselé	kyselý	k2eAgFnSc3d1
chuti	chuť	k1gFnSc3
</s>
<s>
Quadrupel	Quadrupel	k1gInSc1
–	–	k?
velmi	velmi	k6eAd1
silné	silný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
<g/>
,	,	kIx,
tmavší	tmavý	k2eAgFnPc4d2
barvy	barva	k1gFnPc4
vyzrálé	vyzrálý	k2eAgFnSc2d1
ovocné	ovocný	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
</s>
<s>
Ale	ale	k9
německého	německý	k2eAgInSc2d1
typu	typ	k1gInSc2
</s>
<s>
Altbier	Altbier	k1gInSc1
(	(	kIx(
<g/>
staré	starý	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
pivo	pivo	k1gNnSc4
bronzové	bronzový	k2eAgFnSc2d1
čiré	čirý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
jemné	jemný	k2eAgFnSc2d1
nahořklé	nahořklý	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
</s>
<s>
Kölsch	Kölsch	k1gInSc1
(	(	kIx(
<g/>
kolínské	kolínský	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
pivo	pivo	k1gNnSc1
s	s	k7c7
bledou	bledý	k2eAgFnSc7d1
slámově	slámově	k6eAd1
žlutou	žlutý	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
a	a	k8xC
jemnou	jemný	k2eAgFnSc7d1
ovocnou	ovocný	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
a	a	k8xC
vůní	vůně	k1gFnSc7
</s>
<s>
Roggenbier	Roggenbier	k1gInSc1
(	(	kIx(
<g/>
žitné	žitný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
pivo	pivo	k1gNnSc4
vařené	vařený	k2eAgFnSc2d1
ze	z	k7c2
sladu	slad	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
obsahuje	obsahovat	k5eAaImIp3nS
vysoký	vysoký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
žita	žito	k1gNnSc2
(	(	kIx(
<g/>
okolo	okolo	k7c2
50	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
65	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Stout	Stout	k1gInSc1
–	–	k?
tato	tento	k3xDgNnPc1
piva	pivo	k1gNnPc4
jsou	být	k5eAaImIp3nP
charakteristická	charakteristický	k2eAgFnSc1d1
hustou	hustý	k2eAgFnSc7d1
pěnou	pěna	k1gFnSc7
<g/>
,	,	kIx,
tmavou	tmavý	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
a	a	k8xC
příchutí	příchuť	k1gFnSc7
po	po	k7c6
praženém	pražený	k2eAgInSc6d1
sladu	slad	k1gInSc6
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
s	s	k7c7
ovocnou	ovocný	k2eAgFnSc7d1
příchutí	příchuť	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
silná	silný	k2eAgNnPc4d1
piva	pivo	k1gNnPc4
(	(	kIx(
<g/>
podíl	podíl	k1gInSc1
alkoholu	alkohol	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
8	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Imperial	Imperial	k1gMnSc1
Stout	Stout	k1gMnSc1
(	(	kIx(
<g/>
silný	silný	k2eAgInSc1d1
stout	stout	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
pivo	pivo	k1gNnSc4
černé	černý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
s	s	k7c7
čokoládovými	čokoládový	k2eAgInPc7d1
tóny	tón	k1gInPc7
</s>
<s>
Milk	Milk	k6eAd1
<g/>
/	/	kIx~
<g/>
Cream	Cream	k1gInSc4
Stout	Stout	k2eAgInSc4d1
(	(	kIx(
<g/>
mléčný	mléčný	k2eAgInSc4d1
<g/>
/	/	kIx~
<g/>
krémový	krémový	k2eAgInSc4d1
stout	stout	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
pivo	pivo	k1gNnSc4
černé	černý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
s	s	k7c7
výrazně	výrazně	k6eAd1
nasládlé	nasládlý	k2eAgInPc4d1
</s>
<s>
Extra	extra	k2eAgInSc4d1
<g/>
/	/	kIx~
<g/>
Foreign	Foreign	k1gInSc4
Stout	Stout	k2eAgInSc4d1
–	–	k?
plné	plný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
černé	černý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
</s>
<s>
Dry	Dry	k?
Stout	Stout	k1gInSc1
(	(	kIx(
<g/>
suchý	suchý	k2eAgInSc1d1
stout	stout	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
silné	silný	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
černé	černý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
</s>
<s>
Oatmetal	Oatmetat	k5eAaPmAgInS,k5eAaImAgInS
Stout	Stout	k2eAgInSc1d1
(	(	kIx(
<g/>
ovesný	ovesný	k2eAgInSc1d1
stout	stout	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
pivo	pivo	k1gNnSc4
černé	černý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
s	s	k7c7
použitím	použití	k1gNnSc7
ovesných	ovesný	k2eAgInPc2d1
sladů	slad	k1gInPc2
</s>
<s>
Pšeničné	pšeničný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
–	–	k?
při	při	k7c6
vaření	vaření	k1gNnSc2
těchto	tento	k3xDgNnPc2
piv	pivo	k1gNnPc2
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
také	také	k9
pšeničných	pšeničný	k2eAgInPc2d1
sladů	slad	k1gInPc2
(	(	kIx(
<g/>
minimálně	minimálně	k6eAd1
30	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piva	pivo	k1gNnPc1
vynikají	vynikat	k5eAaImIp3nP
hustou	hustý	k2eAgFnSc7d1
pěnou	pěna	k1gFnSc7
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
kalnou	kalný	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
a	a	k8xC
obilnou	obilný	k2eAgFnSc7d1
příchutí	příchuť	k1gFnSc7
často	často	k6eAd1
ovocným	ovocný	k2eAgInSc7d1
podtónem	podtón	k1gInSc7
(	(	kIx(
<g/>
citron	citron	k1gInSc1
<g/>
,	,	kIx,
banán	banán	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
WitBier	WitBier	k1gInSc1
(	(	kIx(
<g/>
bílé	bílý	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
plné	plný	k2eAgNnSc4d1
nebo	nebo	k8xC
výčepní	výčepní	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
<g/>
,	,	kIx,
bledé	bledý	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
ovocné	ovocný	k2eAgFnPc4d1
chuti	chuť	k1gFnPc4
<g/>
,	,	kIx,
při	při	k7c6
vaření	vaření	k1gNnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
nesladovaná	sladovaný	k2eNgFnSc1d1
pšenice	pšenice	k1gFnSc1
</s>
<s>
Dunkelweizen	Dunkelweizen	k2eAgInSc1d1
(	(	kIx(
<g/>
tmavé	tmavý	k2eAgNnSc1d1
pšeničné	pšeničný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
tmavé	tmavý	k2eAgNnSc1d1
pšeničné	pšeničný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
vařené	vařený	k2eAgNnSc1d1
za	za	k7c4
užití	užití	k1gNnSc4
tmavých	tmavý	k2eAgInPc2d1
sladů	slad	k1gInPc2
</s>
<s>
Hefewizen	Hefewizen	k2eAgInSc1d1
<g/>
/	/	kIx~
<g/>
Weissbier	Weissbier	k1gInSc1
(	(	kIx(
<g/>
pšeničné	pšeničný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
klasické	klasický	k2eAgNnSc1d1
pšeničné	pšeničný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
původem	původ	k1gInSc7
z	z	k7c2
Bavorska	Bavorsko	k1gNnSc2
</s>
<s>
Kristalweizen	Kristalweizen	k2eAgInSc1d1
(	(	kIx(
<g/>
čiré	čirý	k2eAgNnSc1d1
pšeničné	pšeničný	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Weizenbock	Weizenbock	k1gInSc1
(	(	kIx(
<g/>
pšeničný	pšeničný	k2eAgMnSc1d1
kozel	kozel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Výroba	výroba	k1gFnSc1
piva	pivo	k1gNnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Výroba	výroba	k1gFnSc1
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Principem	princip	k1gInSc7
výroby	výroba	k1gFnSc2
piva	pivo	k1gNnSc2
jako	jako	k8xS,k8xC
alkoholického	alkoholický	k2eAgInSc2d1
nápoje	nápoj	k1gInSc2
z	z	k7c2
obilí	obilí	k1gNnSc2
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgInPc4
procesy	proces	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaprvé	zaprvé	k4xO
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
rozštěpení	rozštěpení	k1gNnSc4
v	v	k7c6
obilných	obilný	k2eAgNnPc6d1
zrnech	zrno	k1gNnPc6
přítomných	přítomný	k1gMnPc2
složitých	složitý	k2eAgInPc2d1
cukrů	cukr	k1gInPc2
(	(	kIx(
<g/>
škrobu	škrob	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c4
jednoduché	jednoduchý	k2eAgInPc4d1
zkvasitelné	zkvasitelný	k2eAgInPc4d1
cukry	cukr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadruhé	zadruhé	k4xO
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
následné	následný	k2eAgNnSc4d1
zkvašení	zkvašení	k1gNnSc4
těchto	tento	k3xDgInPc2
jednoduchých	jednoduchý	k2eAgInPc2d1
cukrů	cukr	k1gInPc2
pomocí	pomocí	k7c2
kultury	kultura	k1gFnSc2
mikroorganismů	mikroorganismus	k1gInPc2
(	(	kIx(
<g/>
zejména	zejména	k9
kvasinek	kvasinka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
výrobního	výrobní	k2eAgInSc2d1
procesu	proces	k1gInSc2
je	být	k5eAaImIp3nS
smíchání	smíchání	k1gNnSc1
surovin	surovina	k1gFnPc2
s	s	k7c7
vodou	voda	k1gFnSc7
a	a	k8xC
tím	ten	k3xDgNnSc7
převedení	převedení	k1gNnSc4
využitelných	využitelný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
do	do	k7c2
vodného	vodný	k2eAgInSc2d1
roztoku	roztok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Základem	základ	k1gInSc7
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
piva	pivo	k1gNnSc2
je	být	k5eAaImIp3nS
slad	slad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
vzniká	vznikat	k5eAaImIp3nS
ve	v	k7c6
sladovně	sladovna	k1gFnSc6
naklíčením	naklíčení	k1gNnSc7
obilných	obilný	k2eAgNnPc2d1
zrn	zrno	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc7
šetrným	šetrný	k2eAgNnSc7d1
usušením	usušení	k1gNnSc7
za	za	k7c4
určité	určitý	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
druhu	druh	k1gInSc6
sladu	slad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
přesun	přesun	k1gInSc1
suroviny	surovina	k1gFnSc2
do	do	k7c2
pivovaru	pivovar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vystírání	vystírání	k1gNnSc1
a	a	k8xC
rmutování	rmutování	k1gNnSc1
</s>
<s>
V	v	k7c6
začátku	začátek	k1gInSc6
samotné	samotný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
piva	pivo	k1gNnSc2
se	se	k3xPyFc4
slad	slad	k1gInSc1
šrotuje	šrotovat	k5eAaImIp3nS
(	(	kIx(
<g/>
rozemele	rozemlít	k5eAaPmIp3nS
<g/>
)	)	kIx)
a	a	k8xC
smísí	smísit	k5eAaPmIp3nP
se	se	k3xPyFc4
s	s	k7c7
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
směsi	směs	k1gFnSc6
se	se	k3xPyFc4
za	za	k7c4
zvyšující	zvyšující	k2eAgFnPc4d1
se	se	k3xPyFc4
teploty	teplota	k1gFnSc2
vlivem	vliv	k1gInSc7
enzymů	enzym	k1gInPc2
štěpí	štěpit	k5eAaImIp3nS
v	v	k7c6
zrně	zrno	k1gNnSc6
obsažený	obsažený	k2eAgInSc4d1
škrob	škrob	k1gInSc4
na	na	k7c4
zkvasitelné	zkvasitelný	k2eAgInPc4d1
cukry	cukr	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
přecházejí	přecházet	k5eAaImIp3nP
do	do	k7c2
roztoku	roztok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
procedura	procedura	k1gFnSc1
je	být	k5eAaImIp3nS
prováděna	provádět	k5eAaImNgFnS
v	v	k7c6
provozu	provoz	k1gInSc6
nazývaném	nazývaný	k2eAgInSc6d1
varna	varna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
zemi	zem	k1gFnSc6
původu	původ	k1gInSc2
se	se	k3xPyFc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
rozdílné	rozdílný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Britských	britský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
je	být	k5eAaImIp3nS
oblíbená	oblíbený	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
spařování	spařování	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
podobná	podobný	k2eAgFnSc1d1
přípravě	příprava	k1gFnSc3
čaje	čaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jedné	jeden	k4xCgFnSc2
až	až	k9
tří	tři	k4xCgFnPc2
hodin	hodina	k1gFnPc2
je	být	k5eAaImIp3nS
slad	slad	k1gInSc1
vystavován	vystavován	k2eAgInSc1d1
teplotě	teplota	k1gFnSc6
okolo	okolo	k7c2
65	#num#	k4
–	–	k?
68	#num#	k4
°	°	k?
<g/>
C.	C.	kA
V	v	k7c6
kontinentální	kontinentální	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
převládá	převládat	k5eAaImIp3nS
tzv.	tzv.	kA
dekokční	dekokční	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
proti	proti	k7c3
spařování	spařování	k1gNnSc3
složitější	složitý	k2eAgFnSc2d2
procedurou	procedura	k1gFnSc7
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Jeho	jeho	k3xOp3gInSc7
výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
roztok	roztok	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
důslednějším	důsledný	k2eAgFnPc3d2
přeměnám	přeměna	k1gFnPc3
škrobu	škrob	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c4
cukry	cukr	k1gInPc4
a	a	k8xC
štěpení	štěpení	k1gNnSc4
bílkovin	bílkovina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Proces	proces	k1gInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c4
přečerpání	přečerpání	k1gNnSc4
části	část	k1gFnSc2
várky	várka	k1gFnSc2
z	z	k7c2
vystírací	vystírací	k2eAgFnSc2d1
kádě	káď	k1gFnSc2
do	do	k7c2
zvláštní	zvláštní	k2eAgFnSc2d1
nádoby	nádoba	k1gFnSc2
rmutovacího	rmutovací	k2eAgInSc2d1
kotle	kotel	k1gInSc2
(	(	kIx(
<g/>
pánve	pánev	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
varu	var	k1gInSc2
a	a	k8xC
dojde	dojít	k5eAaPmIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
k	k	k7c3
narušení	narušení	k1gNnSc3
škrobnatých	škrobnatý	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
lepší	dobrý	k2eAgInSc1d2
přístup	přístup	k1gInSc1
enzymů	enzym	k1gInPc2
k	k	k7c3
molekulám	molekula	k1gFnPc3
škrobu	škrob	k1gInSc2
po	po	k7c6
opětovném	opětovný	k2eAgNnSc6d1
smíchání	smíchání	k1gNnSc6
s	s	k7c7
chladnější	chladný	k2eAgFnSc7d2
částí	část	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
na	na	k7c6
teplotě	teplota	k1gFnSc6
35	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Postupným	postupný	k2eAgNnSc7d1
zvyšováním	zvyšování	k1gNnSc7
teploty	teplota	k1gFnSc2
se	se	k3xPyFc4
dosáhne	dosáhnout	k5eAaPmIp3nS
až	až	k9
maximální	maximální	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
okolo	okolo	k7c2
76	#num#	k4
°	°	k?
<g/>
C.	C.	kA
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rmutování	rmutování	k1gNnSc6
(	(	kIx(
<g/>
klasickém	klasický	k2eAgInSc6d1
i	i	k8xC
dekokčním	dekokční	k2eAgInSc6d1
<g/>
)	)	kIx)
se	se	k3xPyFc4
zařazují	zařazovat	k5eAaImIp3nP
prodlevy	prodleva	k1gFnPc1
při	při	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
teplotách	teplota	k1gFnPc6
(	(	kIx(
<g/>
cukrotvorná	cukrotvorný	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílkovinoštěpná	bílkovinoštěpný	k2eAgFnSc1d1
atd.	atd.	kA
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
enzymatickým	enzymatický	k2eAgInPc3d1
procesům	proces	k1gInPc3
–	–	k?
např.	např.	kA
enzymů	enzym	k1gInPc2
alfa	alfa	k1gNnSc1
a	a	k8xC
beta	beta	k1gNnSc1
amyláza	amyláza	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Scezování	scezování	k1gNnSc1
</s>
<s>
Výsledkem	výsledek	k1gInSc7
obou	dva	k4xCgFnPc2
metod	metoda	k1gFnPc2
je	být	k5eAaImIp3nS
rmut	rmut	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
přečerpán	přečerpán	k2eAgInSc1d1
do	do	k7c2
tak	tak	k6eAd1
zvané	zvaný	k2eAgFnSc2d1
„	„	k?
<g/>
scezovací	scezovací	k2eAgFnSc2d1
kádě	káď	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počáteční	počáteční	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
scezování	scezování	k1gNnSc2
vytvoří	vytvořit	k5eAaPmIp3nP
hrubé	hrubý	k2eAgFnPc1d1
částice	částice	k1gFnPc1
–	–	k?
většinou	většinou	k6eAd1
obaly	obal	k1gInPc4
zrn	zrno	k1gNnPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
pluchy	plucha	k1gFnSc2
–	–	k?
filtrační	filtrační	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
zvanou	zvaný	k2eAgFnSc4d1
mláto	mláto	k1gNnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
opouští	opouštět	k5eAaImIp3nS
čirá	čirý	k2eAgFnSc1d1
sladina	sladina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylé	zbylý	k2eAgInPc4d1
využitelné	využitelný	k2eAgInPc4d1
podíly	podíl	k1gInPc4
zachycené	zachycený	k2eAgInPc4d1
v	v	k7c6
mlátě	mláto	k1gNnSc6
se	se	k3xPyFc4
vymývají	vymývat	k5eAaImIp3nP
skrápěním	skrápění	k1gNnSc7
či	či	k8xC
promýváním	promývání	k1gNnSc7
vrstvy	vrstva	k1gFnSc2
mláta	mláto	k1gNnSc2
teplou	teplý	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
sladká	sladký	k2eAgFnSc1d1
tekutina	tekutina	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
sladina	sladina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vzniká	vznikat	k5eAaImIp3nS
filtrováním	filtrování	k1gNnSc7
(	(	kIx(
<g/>
scezováním	scezování	k1gNnSc7
<g/>
)	)	kIx)
na	na	k7c6
nebo	nebo	k8xC
v	v	k7c6
moderním	moderní	k2eAgNnSc6d1
zařízení	zařízení	k1gNnSc6
zvaném	zvaný	k2eAgNnSc6d1
„	„	k?
<g/>
sladinový	sladinový	k2eAgInSc4d1
filtr	filtr	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Chmelovar	chmelovar	k1gInSc1
</s>
<s>
Chmelovar	chmelovar	k1gInSc1
je	být	k5eAaImIp3nS
proces	proces	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
sladina	sladina	k1gFnSc1
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
varu	var	k1gInSc2
po	po	k7c4
dobu	doba	k1gFnSc4
60	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
do	do	k7c2
vznikajícího	vznikající	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
dodávána	dodáván	k2eAgFnSc1d1
důležitá	důležitý	k2eAgFnSc1d1
surovina	surovina	k1gFnSc1
pro	pro	k7c4
získání	získání	k1gNnSc4
chuti	chuť	k1gFnSc2
–	–	k?
chmel	chmel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chmel	Chmel	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
druhu	druh	k1gInSc6
piva	pivo	k1gNnSc2
a	a	k8xC
preferencích	preference	k1gFnPc6
sládka	sládek	k1gMnSc4
může	moct	k5eAaImIp3nS
přidávat	přidávat	k5eAaImF
jak	jak	k6eAd1
v	v	k7c6
přírodní	přírodní	k2eAgFnSc6d1
formě	forma	k1gFnSc6
šišek	šiška	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
lisovaný	lisovaný	k2eAgMnSc1d1
či	či	k8xC
v	v	k7c6
podobě	podoba	k1gFnSc6
tekutého	tekutý	k2eAgInSc2d1
extraktu	extrakt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chmel	chmel	k1gInSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
do	do	k7c2
vařeného	vařený	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
přidávat	přidávat	k5eAaImF
vícekrát	vícekrát	k6eAd1
než	než	k8xS
jednou	jeden	k4xCgFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
třikrát	třikrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platí	platit	k5eAaImIp3nS
obecné	obecný	k2eAgNnSc4d1
pravidlo	pravidlo	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
dříve	dříve	k6eAd2
je	být	k5eAaImIp3nS
chmel	chmel	k1gInSc1
do	do	k7c2
piva	pivo	k1gNnSc2
přidán	přidat	k5eAaPmNgMnS
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
hořčí	hořč	k1gFnPc2
chuť	chuť	k1gFnSc4
pivo	pivo	k1gNnSc4
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smícháním	smíchání	k1gNnSc7
roztoku	roztok	k1gInSc2
s	s	k7c7
chmelem	chmel	k1gInSc7
vzniká	vznikat	k5eAaImIp3nS
mladina	mladina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dále	daleko	k6eAd2
dochází	docházet	k5eAaImIp3nS
při	při	k7c6
chmelovaru	chmelovar	k1gInSc6
vlivem	vlivem	k7c2
hořkých	hořký	k2eAgFnPc2d1
látek	látka	k1gFnPc2
(	(	kIx(
<g/>
pryskyřic	pryskyřice	k1gFnPc2
a	a	k8xC
kyselin	kyselina	k1gFnPc2
<g/>
)	)	kIx)
k	k	k7c3
vysrážení	vysrážení	k1gNnSc3
tzv.	tzv.	kA
klků	klk	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
chuchvalců	chuchvalec	k1gInPc2
bílkovin	bílkovina	k1gFnPc2
a	a	k8xC
chmelových	chmelový	k2eAgInPc2d1
zbytků	zbytek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Mladina	mladina	k1gFnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
přečerpává	přečerpávat	k5eAaImIp3nS
do	do	k7c2
odkalovacího	odkalovací	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
tzv.	tzv.	kA
štoky	štok	k1gInPc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
například	například	k6eAd1
vířivá	vířivý	k2eAgFnSc1d1
káď	káď	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
odstranění	odstranění	k1gNnSc3
chmelových	chmelový	k2eAgInPc2d1
zbytků	zbytek	k1gInPc2
a	a	k8xC
klků	klk	k1gInPc2
před	před	k7c7
zchlazením	zchlazení	k1gNnSc7
a	a	k8xC
kvašením	kvašení	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Značení	značení	k1gNnSc1
piva	pivo	k1gNnSc2
</s>
<s>
Vedle	vedle	k7c2
obecných	obecný	k2eAgFnPc2d1
náležitostí	náležitost	k1gFnPc2
pro	pro	k7c4
označení	označení	k1gNnSc4
piva	pivo	k1gNnSc2
(	(	kIx(
<g/>
dle	dle	k7c2
vyhlášky	vyhláška	k1gFnSc2
č.	č.	k?
324	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
podle	podle	k7c2
vyhlášky	vyhláška	k1gFnSc2
č.	č.	k?
335	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
pivo	pivo	k1gNnSc4
označit	označit	k5eAaPmF
<g/>
:	:	kIx,
názvem	název	k1gInSc7
druhu	druh	k1gInSc2
a	a	k8xC
skupiny	skupina	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
pivo	pivo	k1gNnSc1
ležák	ležák	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obsahem	obsah	k1gInSc7
alkoholu	alkohol	k1gInSc2
v	v	k7c6
procentech	procento	k1gNnPc6
<g/>
,	,	kIx,
zda	zda	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
světlé	světlý	k2eAgNnSc4d1
<g/>
,	,	kIx,
či	či	k8xC
tmavé	tmavý	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
některými	některý	k3yIgInPc7
dalšími	další	k2eAgInPc7d1
údaji	údaj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesný	přesný	k2eAgInSc1d1
údaj	údaj	k1gInSc1
o	o	k7c6
extraktu	extrakt	k1gInSc6
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
není	být	k5eNaImIp3nS
nutno	nutno	k6eAd1
povinně	povinně	k6eAd1
zveřejňovat	zveřejňovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Označení	označení	k1gNnSc1
stupňovitostí	stupňovitost	k1gFnPc2
bylo	být	k5eAaImAgNnS
opuštěno	opustit	k5eAaPmNgNnS
v	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
stupňů	stupeň	k1gInPc2
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
uvádět	uvádět	k5eAaImF
tzv.	tzv.	kA
extrakt	extrakt	k1gInSc4
původní	původní	k2eAgFnSc2d1
mladiny	mladina	k1gFnSc2
(	(	kIx(
<g/>
EPM	EPM	kA
<g/>
)	)	kIx)
–	–	k?
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
procentech	procento	k1gNnPc6
hmotnostních	hmotnostní	k2eAgNnPc6d1
(	(	kIx(
<g/>
např.	např.	kA
12	#num#	k4
<g/>
%	%	kIx~
pivo	pivo	k1gNnSc1
<g/>
;	;	kIx,
pozor	pozor	k1gInSc1
–	–	k?
tato	tento	k3xDgNnPc1
procenta	procento	k1gNnPc1
si	se	k3xPyFc3
nelze	lze	k6eNd1
plést	plést	k5eAaImF
s	s	k7c7
procenty	procent	k1gInPc7
alkoholu	alkohol	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
u	u	k7c2
běžných	běžný	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
pohybují	pohybovat	k5eAaImIp3nP
mezi	mezi	k7c7
4	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c4
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
výrobce	výrobce	k1gMnSc4
zaručuje	zaručovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
ve	v	k7c6
výrobě	výroba	k1gFnSc6
zaručena	zaručen	k2eAgFnSc1d1
přesnost	přesnost	k1gFnSc1
EPM	EPM	kA
(	(	kIx(
<g/>
resp.	resp.	kA
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
rozhodne	rozhodnout	k5eAaPmIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
tak	tak	k9
výrobce	výrobce	k1gMnSc1
<g/>
,	,	kIx,
např.	např.	kA
z	z	k7c2
důvodů	důvod	k1gInPc2
daňových	daňový	k2eAgInPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
postačí	postačit	k5eAaPmIp3nS
uvedení	uvedení	k1gNnSc1
druhu	druh	k1gInSc2
piva	pivo	k1gNnSc2
podle	podle	k7c2
vyhlášky	vyhláška	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
výše	vysoce	k6eAd2
druhy	druh	k1gInPc4
piva	pivo	k1gNnSc2
podle	podle	k7c2
míry	míra	k1gFnSc2
EPM	EPM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
druhu	druh	k1gInSc2
piva	pivo	k1gNnSc2
je	být	k5eAaImIp3nS
ale	ale	k9
nutno	nutno	k6eAd1
použít	použít	k5eAaPmF
vždy	vždy	k6eAd1
(	(	kIx(
<g/>
i	i	k9
při	při	k7c6
uvedení	uvedení	k1gNnSc6
EPM	EPM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
značení	značení	k1gNnSc2
<g/>
:	:	kIx,
a	a	k8xC
<g/>
)	)	kIx)
nesprávné	správný	k2eNgNnSc1d1
značení	značení	k1gNnSc1
<g/>
:	:	kIx,
pivo	pivo	k1gNnSc1
10	#num#	k4
<g/>
stupňové	stupňový	k2eAgInPc1d1
<g/>
,	,	kIx,
pivo	pivo	k1gNnSc1
12	#num#	k4
<g/>
stupňové	stupňový	k2eAgFnSc6d1
<g/>
,	,	kIx,
jedenáctka	jedenáctka	k1gFnSc1
<g/>
,	,	kIx,
silné	silný	k2eAgFnPc1d1
14	#num#	k4
<g/>
stupňové	stupňový	k2eAgFnPc1d1
pivo	pivo	k1gNnSc4
<g/>
;	;	kIx,
b	b	k?
<g/>
)	)	kIx)
správné	správný	k2eAgNnSc1d1
značení	značení	k1gNnSc1
piva	pivo	k1gNnSc2
<g/>
:	:	kIx,
pivo	pivo	k1gNnSc1
výčepní	výčepní	k1gFnSc1
<g/>
,	,	kIx,
pivo	pivo	k1gNnSc1
ležák	ležák	k1gInSc1
<g/>
,	,	kIx,
speciál	speciál	k1gInSc1
14	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Konzumace	konzumace	k1gFnSc1
piva	pivo	k1gNnSc2
</s>
<s>
Pásy	pás	k1gInPc1
dle	dle	k7c2
typického	typický	k2eAgInSc2d1
druhu	druh	k1gInSc2
alkoholu	alkohol	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
na	na	k7c6
daném	daný	k2eAgNnSc6d1
území	území	k1gNnSc6
konzumuje	konzumovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pás	pás	k1gInSc4
vodky	vodka	k1gFnSc2
Pás	pás	k1gInSc1
piva	pivo	k1gNnSc2
Pás	pás	k1gInSc4
vína	víno	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
světa	svět	k1gInSc2
podle	podle	k7c2
spotřeby	spotřeba	k1gFnSc2
piva	pivo	k1gNnSc2
</s>
<s>
Pivo	pivo	k1gNnSc1
je	být	k5eAaImIp3nS
celosvětově	celosvětově	k6eAd1
rozšířený	rozšířený	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
těší	těšit	k5eAaImIp3nS
velké	velký	k2eAgFnSc3d1
oblibě	obliba	k1gFnSc3
hlavně	hlavně	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
jmenovitě	jmenovitě	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
Slovenska	Slovensko	k1gNnSc2
<g/>
,	,	kIx,
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pak	pak	k6eAd1
v	v	k7c6
zemích	zem	k1gFnPc6
západní	západní	k2eAgFnSc2d1
a	a	k8xC
severní	severní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Dánsko	Dánsko	k1gNnSc4
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
<g/>
,	,	kIx,
Holandsko	Holandsko	k1gNnSc1
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Největším	veliký	k2eAgMnSc7d3
celosvětovým	celosvětový	k2eAgMnSc7d1
producentem	producent	k1gMnSc7
piva	pivo	k1gNnSc2
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
vyrobila	vyrobit	k5eAaPmAgFnS
470	#num#	k4
milionu	milion	k4xCgInSc2
hektolitrů	hektolitr	k1gInPc2
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
odpovídá	odpovídat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
21,1	21,1	k4
%	%	kIx~
světové	světový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
USA	USA	kA
s	s	k7c7
232,8	232,8	k4
miliony	milion	k4xCgInPc7
hektolitry	hektolitr	k1gInPc7
<g/>
,	,	kIx,
následované	následovaný	k2eAgInPc1d1
Ruskem	Rusko	k1gNnSc7
s	s	k7c7
109,8	109,8	k4
miliony	milion	k4xCgInPc7
hektolitry	hektolitr	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Nejprodávanějším	prodávaný	k2eAgNnSc7d3
pivem	pivo	k1gNnSc7
na	na	k7c6
světě	svět	k1gInSc6
je	být	k5eAaImIp3nS
holandské	holandský	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
Heineken	Heinekna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oblíbené	oblíbený	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
na	na	k7c6
území	území	k1gNnSc6
USA	USA	kA
<g/>
,	,	kIx,
Kanady	Kanada	k1gFnSc2
<g/>
,	,	kIx,
Nového	Nového	k2eAgInSc2d1
Zélandu	Zéland	k1gInSc2
a	a	k8xC
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
často	často	k6eAd1
o	o	k7c4
země	zem	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
převládá	převládat	k5eAaImIp3nS
protestantství	protestantství	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
nepovažuje	považovat	k5eNaImIp3nS
víno	víno	k1gNnSc1
za	za	k7c4
krev	krev	k1gFnSc4
Krista	Kristus	k1gMnSc2
pomocí	pomocí	k7c2
transsubstanciace	transsubstanciace	k1gFnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
víno	víno	k1gNnSc1
nemá	mít	k5eNaImIp3nS
podporu	podpora	k1gFnSc4
(	(	kIx(
<g/>
ve	v	k7c6
středověku	středověk	k1gInSc6
převažovala	převažovat	k5eAaImAgFnS
produkce	produkce	k1gFnSc1
vína	vína	k1gFnSc1
mnichy	mnich	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Podle	podle	k7c2
českých	český	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
se	se	k3xPyFc4
pivem	pivo	k1gNnSc7
rozumí	rozumět	k5eAaImIp3nS
<g/>
:	:	kIx,
pěnivý	pěnivý	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
vyrobený	vyrobený	k2eAgInSc4d1
zkvašením	zkvašení	k1gNnSc7
mladiny	mladina	k1gFnSc2
připravené	připravený	k2eAgFnSc2d1
ze	z	k7c2
sladu	slad	k1gInSc2
<g/>
,	,	kIx,
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
neupraveného	upravený	k2eNgInSc2d1
chmele	chmel	k1gInSc2
<g/>
,	,	kIx,
upraveného	upravený	k2eAgInSc2d1
chmele	chmel	k1gInSc2
nebo	nebo	k8xC
chmelových	chmelový	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vedle	vedle	k6eAd1
kvasným	kvasný	k2eAgInSc7d1
procesem	proces	k1gInSc7
vzniklého	vzniklý	k2eAgInSc2d1
alkoholu	alkohol	k1gInSc2
(	(	kIx(
<g/>
ethanolu	ethanol	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
oxidu	oxid	k1gInSc2
uhličitého	uhličitý	k2eAgInSc2d1
obsahuje	obsahovat	k5eAaImIp3nS
i	i	k9
určité	určitý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
neprokvašeného	prokvašený	k2eNgInSc2d1
extraktu	extrakt	k1gInSc2
(	(	kIx(
<g/>
§	§	k?
11	#num#	k4
písm	písmo	k1gNnPc2
<g/>
.	.	kIx.
a	a	k8xC
<g/>
/	/	kIx~
vyhlášky	vyhláška	k1gFnSc2
č.	č.	k?
335	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nejznámější	známý	k2eAgInPc1d3
pivovary	pivovar	k1gInPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
pivovarů	pivovar	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Zdanění	zdanění	k1gNnSc1
piva	pivo	k1gNnSc2
</s>
<s>
Z	z	k7c2
daňových	daňový	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
se	se	k3xPyFc4
pivem	pivo	k1gNnSc7
zabývá	zabývat	k5eAaImIp3nS
především	především	k9
zákon	zákon	k1gInSc1
č.	č.	k?
353	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
spotřebních	spotřební	k2eAgFnPc6d1
daních	daň	k1gFnPc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
upravuje	upravovat	k5eAaImIp3nS
v	v	k7c6
§	§	k?
80	#num#	k4
a	a	k8xC
násl	násnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
spotřební	spotřební	k2eAgFnSc4d1
daň	daň	k1gFnSc4
z	z	k7c2
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daň	daň	k1gFnSc1
není	být	k5eNaImIp3nS
povinna	povinen	k2eAgFnSc1d1
odvádět	odvádět	k5eAaImF
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
domácnosti	domácnost	k1gFnSc6
vaří	vařit	k5eAaImIp3nP
pivo	pivo	k1gNnSc4
pro	pro	k7c4
osobní	osobní	k2eAgFnSc4d1
potřebu	potřeba	k1gFnSc4
do	do	k7c2
200	#num#	k4
litrů	litr	k1gInPc2
za	za	k7c4
rok	rok	k1gInSc4
a	a	k8xC
pivo	pivo	k1gNnSc1
neprodává	prodávat	k5eNaImIp3nS
za	za	k7c4
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
oznámí	oznámit	k5eAaPmIp3nS
celnímu	celní	k2eAgInSc3d1
úřadu	úřad	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštní	zvláštní	k2eAgFnPc1d1
daňové	daňový	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
jsou	být	k5eAaImIp3nP
stanoveny	stanovit	k5eAaPmNgFnP
pro	pro	k7c4
malé	malý	k2eAgInPc4d1
nezávislé	závislý	k2eNgInPc4d1
pivovary	pivovar	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
daně	daň	k1gFnSc2
je	být	k5eAaImIp3nS
množství	množství	k1gNnSc1
piva	pivo	k1gNnSc2
v	v	k7c6
hektolitrech	hektolitr	k1gInPc6
<g/>
,	,	kIx,
základní	základní	k2eAgFnSc1d1
daňová	daňový	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
za	za	k7c4
jeden	jeden	k4xCgInSc4
hektolitr	hektolitr	k1gInSc4
a	a	k8xC
celé	celý	k2eAgNnSc4d1
procento	procento	k1gNnSc4
EPM	EPM	kA
je	být	k5eAaImIp3nS
24	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
prodeji	prodej	k1gInSc6
piva	pivo	k1gNnSc2
se	se	k3xPyFc4
také	také	k9
vedle	vedle	k7c2
spotřební	spotřební	k2eAgFnSc2d1
daně	daň	k1gFnSc2
uplatňuje	uplatňovat	k5eAaImIp3nS
daň	daň	k1gFnSc1
z	z	k7c2
přidané	přidaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
(	(	kIx(
<g/>
DPH	DPH	kA
<g/>
)	)	kIx)
podle	podle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
235	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
rozdílem	rozdíl	k1gInSc7
české	český	k2eAgFnSc2d1
spotřební	spotřební	k2eAgFnSc2d1
daně	daň	k1gFnSc2
z	z	k7c2
piva	pivo	k1gNnSc2
a	a	k8xC
z	z	k7c2
vína	víno	k1gNnSc2
(	(	kIx(
<g/>
vína	víno	k1gNnSc2
mají	mít	k5eAaImIp3nP
nulovou	nulový	k2eAgFnSc4d1
spotřební	spotřební	k2eAgFnSc4d1
daň	daň	k1gFnSc4
<g/>
)	)	kIx)
navrhl	navrhnout	k5eAaPmAgInS
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
malých	malý	k2eAgInPc2d1
nezávislých	závislý	k2eNgInPc2d1
pivovarů	pivovar	k1gInPc2
prodávat	prodávat	k5eAaImF
pivo	pivo	k1gNnSc4
pod	pod	k7c7
názvem	název	k1gInSc7
sladové	sladový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
ceny	cena	k1gFnSc2
</s>
<s>
Cena	cena	k1gFnSc1
piva	pivo	k1gNnSc2
zažila	zažít	k5eAaPmAgFnS
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
značný	značný	k2eAgInSc4d1
nárůst	nárůst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
komunismu	komunismus	k1gInSc2
byla	být	k5eAaImAgFnS
průměrná	průměrný	k2eAgFnSc1d1
cena	cena	k1gFnSc1
piva	pivo	k1gNnSc2
dlouhodobě	dlouhodobě	k6eAd1
až	až	k6eAd1
na	na	k7c4
dvě	dva	k4xCgNnPc4
zdražení	zdražení	k1gNnPc4
téměř	téměř	k6eAd1
neměnná	měnný	k2eNgNnPc4d1,k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
zdražení	zdražení	k1gNnSc4
proběhlo	proběhnout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
cena	cena	k1gFnSc1
10	#num#	k4
<g/>
°	°	k?
lahvového	lahvový	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
zvedla	zvednout	k5eAaPmAgFnS
z	z	k7c2
1,4	1,4	k4
Kčs	Kčs	kA
na	na	k7c4
1,7	1,7	k4
Kčs	Kčs	kA
<g/>
,	,	kIx,
a	a	k8xC
druhé	druhý	k4xOgFnPc1
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
cena	cena	k1gFnSc1
narostla	narůst	k5eAaPmAgFnS
z	z	k7c2
1,7	1,7	k4
Kčs	Kčs	kA
na	na	k7c4
2,5	2,5	k4
Kčs	Kčs	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
nárůst	nárůst	k1gInSc1
ceny	cena	k1gFnSc2
začal	začít	k5eAaPmAgInS
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
a	a	k8xC
trval	trvat	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
cena	cena	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
8,59	8,59	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
mírnému	mírný	k2eAgInSc3d1
poklesu	pokles	k1gInSc3
ceny	cena	k1gFnSc2
až	až	k9
na	na	k7c4
nejnižší	nízký	k2eAgFnSc4d3
hodnotu	hodnota	k1gFnSc4
k	k	k7c3
roku	rok	k1gInSc3
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
stálo	stát	k5eAaImAgNnS
pivo	pivo	k1gNnSc1
8,4	8,4	k4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Množství	množství	k1gNnSc1
</s>
<s>
Roční	roční	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
piva	pivo	k1gNnSc2
na	na	k7c4
hlavu	hlava	k1gFnSc4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česko	Česko	k1gNnSc1
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
na	na	k7c6
přední	přední	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
v	v	k7c6
množství	množství	k1gNnSc6
vypitého	vypitý	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
na	na	k7c4
hlavu	hlava	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
zahrnuje	zahrnovat	k5eAaImIp3nS
všechny	všechen	k3xTgMnPc4
obyvatele	obyvatel	k1gMnPc4
Česka	Česko	k1gNnPc4
včetně	včetně	k7c2
kojenců	kojenec	k1gMnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvenství	prvenství	k1gNnSc1
však	však	k9
před	před	k7c7
2	#num#	k4
<g/>
.	.	kIx.
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
držela	držet	k5eAaImAgFnS
Belgie	Belgie	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
důvodu	důvod	k1gInSc2
vysokých	vysoký	k2eAgFnPc2d1
daní	daň	k1gFnPc2
na	na	k7c4
víno	víno	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
tam	tam	k6eAd1
byla	být	k5eAaImAgFnS
spotřeba	spotřeba	k1gFnSc1
na	na	k7c4
jednoho	jeden	k4xCgMnSc4
obyvatele	obyvatel	k1gMnSc4
228	#num#	k4
litrů	litr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Spotřeba	spotřeba	k1gFnSc1
piva	pivo	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
však	však	k8xC
v	v	k7c6
Belgii	Belgie	k1gFnSc6
neustále	neustále	k6eAd1
klesá	klesat	k5eAaImIp3nS
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
ČR	ČR	kA
se	se	k3xPyFc4
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
ještě	ještě	k9
vedla	vést	k5eAaImAgFnS
NSR	NSR	kA
se	s	k7c7
145	#num#	k4
litry	litr	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Každoročně	každoročně	k6eAd1
každý	každý	k3xTgMnSc1
Čech	Čech	k1gMnSc1
vypije	vypít	k5eAaPmIp3nS
dle	dle	k7c2
statistik	statistika	k1gFnPc2
přibližně	přibližně	k6eAd1
okolo	okolo	k7c2
160	#num#	k4
litrů	litr	k1gInPc2
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
lokálního	lokální	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
připadala	připadat	k5eAaPmAgFnS,k5eAaImAgFnS
na	na	k7c4
každého	každý	k3xTgMnSc4
obyvatele	obyvatel	k1gMnSc4
163,5	163,5	k4
litru	litr	k1gInSc6
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
následoval	následovat	k5eAaImAgInS
propad	propad	k1gInSc1
na	na	k7c4
159,1	159,1	k4
litru	litr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
15	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
%	%	kIx~
se	se	k3xPyFc4
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
spotřebě	spotřeba	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
piva	pivo	k1gNnSc2
podílejí	podílet	k5eAaImIp3nP
zahraniční	zahraniční	k2eAgMnPc1d1
turisté	turist	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Česko	Česko	k1gNnSc1
si	se	k3xPyFc3
tak	tak	k9
udržuje	udržovat	k5eAaImIp3nS
přední	přední	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
i	i	k9
nadále	nadále	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
ve	v	k7c6
spotřebě	spotřeba	k1gFnSc6
piva	pivo	k1gNnSc2
se	se	k3xPyFc4
nově	nově	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
Irové	Ir	k1gMnPc1
se	s	k7c7
130	#num#	k4
litry	litr	k1gInPc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
tak	tak	k6eAd1
na	na	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
odsunuli	odsunout	k5eAaPmAgMnP
Němce	Němec	k1gMnSc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
vypili	vypít	k5eAaPmAgMnP
111,7	111,7	k4
litru	litr	k1gInSc2
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německý	německý	k2eAgInSc1d1
trh	trh	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
ale	ale	k8xC
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
útlumu	útlum	k1gInSc6
a	a	k8xC
množství	množství	k1gNnSc6
vypitého	vypitý	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
zde	zde	k6eAd1
postupně	postupně	k6eAd1
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česko	Česko	k1gNnSc1
je	být	k5eAaImIp3nS
16	#num#	k4
<g/>
.	.	kIx.
největším	veliký	k2eAgMnSc7d3
producentem	producent	k1gMnSc7
piva	pivo	k1gNnSc2
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2007	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
ročním	roční	k2eAgInSc7d1
výstavem	výstav	k1gInSc7
okolo	okolo	k7c2
19,897	19,897	k4
milionu	milion	k4xCgInSc2
hektolitrů	hektolitr	k1gInPc2
piva	pivo	k1gNnSc2
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
devátým	devátý	k4xOgMnSc7
největším	veliký	k2eAgMnSc7d3
celosvětovým	celosvětový	k2eAgMnSc7d1
vývozcem	vývozce	k1gMnSc7
piva	pivo	k1gNnSc2
s	s	k7c7
neustávajícím	ustávající	k2eNgInSc7d1
růstem	růst	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
vyrábí	vyrábět	k5eAaImIp3nS
Plzeňský	plzeňský	k2eAgInSc1d1
Prazdroj	prazdroj	k1gInSc1
(	(	kIx(
<g/>
Gambrinus	Gambrinus	k1gMnSc1
<g/>
,	,	kIx,
Radegast	Radegast	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ovládá	ovládat	k5eAaImIp3nS
SABMiller	SABMiller	k1gInSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
Staropramen	staropramen	k1gInSc1
(	(	kIx(
<g/>
Molson	Molson	k1gInSc1
Coors	Coors	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Heineken	Heineken	k2eAgMnSc1d1
a	a	k8xC
pak	pak	k6eAd1
teprve	teprve	k6eAd1
české	český	k2eAgInPc1d1
pivovary	pivovar	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alergeny	alergen	k1gInPc1
v	v	k7c6
pivu	pivo	k1gNnSc6
</s>
<s>
K	k	k7c3
alergenům	alergen	k1gInPc3
patří	patřit	k5eAaImIp3nS
mykotoxiny	mykotoxin	k1gInPc4
z	z	k7c2
plesnivého	plesnivý	k2eAgInSc2d1
sladu	slad	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
našly	najít	k5eAaPmAgFnP
ve	v	k7c6
dvou	dva	k4xCgFnPc6
třetinách	třetina	k1gFnPc6
českých	český	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
(	(	kIx(
<g/>
zdroj	zdroj	k1gInSc1
časopis	časopis	k1gInSc1
dTest	dTest	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lepek	lepek	k1gInSc1
(	(	kIx(
<g/>
existují	existovat	k5eAaImIp3nP
ale	ale	k9
i	i	k9
bezlepková	bezlepkový	k2eAgNnPc1d1
piva	pivo	k1gNnPc1
<g/>
)	)	kIx)
a	a	k8xC
oxid	oxid	k1gInSc1
siřičitý	siřičitý	k2eAgInSc1d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
do	do	k7c2
piva	pivo	k1gNnSc2
přidává	přidávat	k5eAaImIp3nS
jako	jako	k9
konzervační	konzervační	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Neplést	plést	k5eNaImF
s	s	k7c7
lotyšskou	lotyšský	k2eAgFnSc7d1
značkou	značka	k1gFnSc7
piva	pivo	k1gNnSc2
Euro	euro	k1gNnSc1
Beer	Beer	k1gInSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
ani	ani	k8xC
se	se	k3xPyFc4
soutěží	soutěž	k1gFnSc7
piv	pivo	k1gNnPc2
European	European	k1gMnSc1
Beer	Beer	k1gMnSc1
Star	Star	kA
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.idnes.cz/ekonomika/zahranicni/globalni-spotreba-pivo-narust-ceska-republika-prumerna-spotreba-asie-afrika.A201223_153315_eko-zahranicni_misl	https://www.idnes.cz/ekonomika/zahranicni/globalni-spotreba-pivo-narust-ceska-republika-prumerna-spotreba-asie-afrika.A201223_153315_eko-zahranicni_misl	k1gInSc1
Globální	globální	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
piva	pivo	k1gNnSc2
v	v	k7c6
loňském	loňský	k2eAgInSc6d1
roce	rok	k1gInSc6
vzrostla	vzrůst	k5eAaPmAgFnS
<g/>
,	,	kIx,
jedničkou	jednička	k1gFnSc7
zůstává	zůstávat	k5eAaImIp3nS
Česko	Česko	k1gNnSc1
<g/>
↑	↑	k?
Ministerstvo	ministerstvo	k1gNnSc1
zemědělství	zemědělství	k1gNnSc2
ČR	ČR	kA
-	-	kIx~
České	český	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
je	být	k5eAaImIp3nS
jen	jen	k9
naše	náš	k3xOp1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-07-31	2008-07-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.ceskepivo.cz	http://www.ceskepivo.cz	k1gInSc1
<g/>
↑	↑	k?
http://vtm.zive.cz/jake-pivo-pili-stari-sumerove	http://vtm.zive.cz/jake-pivo-pili-stari-sumerov	k1gInSc5
Archivováno	archivován	k2eAgNnSc4d1
14	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
-	-	kIx~
Jaké	jaký	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
pivo	pivo	k1gNnSc4
pili	pít	k5eAaImAgMnP
staří	starý	k2eAgMnPc1d1
Sumerové	Sumer	k1gMnPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
znamenající	znamenající	k2eAgNnSc1d1
"	"	kIx"
<g/>
pitivo	pitivo	k1gNnSc1
<g/>
"	"	kIx"
od	od	k7c2
"	"	kIx"
<g/>
pít	pít	k5eAaImF
<g/>
"	"	kIx"
Archivováno	archivovat	k5eAaBmNgNnS
22	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Prazdroj	prazdroj	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Pivo	pivo	k1gNnSc1
a	a	k8xC
původ	původ	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
http://www.prazdroj.cz/cz/o-pivu/historie-piva	http://www.prazdroj.cz/cz/o-pivu/historie-piva	k1gFnSc1
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
www.bierserver.at	www.bierserver.at	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ženy	žena	k1gFnPc1
a	a	k8xC
muži	muž	k1gMnPc1
v	v	k7c6
číslech	číslo	k1gNnPc6
zdravotnické	zdravotnický	k2eAgFnSc2d1
statistiky	statistika	k1gFnSc2
2002	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
21	#num#	k4
-	-	kIx~
http://www.uzis.cz/system/files/zeny_muzi_cisl_zdr_stat.pdf	http://www.uzis.cz/system/files/zeny_muzi_cisl_zdr_stat.pdf	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
8	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Global	globat	k5eAaImAgInS
status	status	k1gInSc1
report	report	k1gInSc1
on	on	k3xPp3gMnSc1
alcohol	alcohol	k1gInSc4
and	and	k?
health	health	k1gInSc1
str	str	kA
<g/>
.	.	kIx.
274	#num#	k4
-	-	kIx~
http://www.who.int/substance_abuse/publications/global_alcohol_report/msbgsruprofiles.pdf1	http://www.who.int/substance_abuse/publications/global_alcohol_report/msbgsruprofiles.pdf1	k4
2	#num#	k4
KOUKAL	Koukal	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
stoleti	stolet	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Metabolický	metabolický	k2eAgInSc4d1
syndrom	syndrom	k1gInSc4
se	se	k3xPyFc4
bojí	bát	k5eAaImIp3nP
piva	pivo	k1gNnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
stoleti	stolet	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2007-10-19	2007-10-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Zdrava-vyziva	Zdrava-vyziva	k1gFnSc1
<g/>
.	.	kIx.
<g/>
abecedazdravi	abecedazdraev	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Pivo	pivo	k1gNnSc1
a	a	k8xC
zdraví	zdraví	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prazdroj	prazdroj	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://medicalxpress.com/news/2015-10-antioxidant-cancer.html	http://medicalxpress.com/news/2015-10-antioxidant-cancer.html	k1gInSc1
-	-	kIx~
Study	stud	k1gInPc1
shows	showsa	k1gFnPc2
antioxidant	antioxidant	k1gInSc1
use	usus	k1gInSc5
may	may	k?
promote	promot	k1gInSc5
spread	spread	k6eAd1
of	of	k?
cancer	cancer	k1gInSc1
<g/>
↑	↑	k?
http://medicalxpress.com/news/2016-03-moderate-good.html	http://medicalxpress.com/news/2016-03-moderate-good.html	k1gInSc4
-	-	kIx~
Is	Is	k1gMnSc5
moderate	moderat	k1gMnSc5
drinking	drinking	k1gInSc1
really	realnout	k5eAaImAgInP,k5eAaPmAgInP,k5eAaBmAgInP
good	good	k6eAd1
for	forum	k1gNnPc2
you	you	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
https://sciencemag.cz/pivo-ani-kafe-nam-pry-nechutnaji/	https://sciencemag.cz/pivo-ani-kafe-nam-pry-nechutnaji/	k?
-	-	kIx~
Pivo	pivo	k1gNnSc1
ani	ani	k8xC
kafe	kafe	k?
nám	my	k3xPp1nPc3
prý	prý	k9
nechutnají	chutnat	k5eNaImIp3nP
<g/>
↑	↑	k?
PIAZZON	PIAZZON	kA
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
FORTE	forte	k1gNnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
NARDINI	NARDINI	kA
<g/>
,	,	kIx,
M.	M.	kA
Characterization	Characterization	k1gInSc1
of	of	k?
phenolics	phenolics	k1gInSc1
content	content	k1gInSc1
and	and	k?
antioxidant	antioxidant	k1gInSc1
activity	activita	k1gFnSc2
of	of	k?
different	different	k1gMnSc1
beer	beer	k1gMnSc1
types	types	k1gMnSc1
<g/>
..	..	k?
J	J	kA
Agric	Agric	k1gMnSc1
Food	Food	k1gMnSc1
Chem.	Chem.	k1gMnSc1
Oct	Oct	k1gMnSc1
2010	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
58	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
19	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
10677	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
jf	jf	k?
<g/>
101975	#num#	k4
<g/>
q.	q.	k?
PMID	PMID	kA
20822144	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Procyanidin	Procyanidin	k2eAgInSc4d1
Content	Content	k1gInSc4
and	and	k?
Variation	Variation	k1gInSc1
in	in	k?
Some	Som	k1gFnSc2
Commonly	Commonla	k1gFnSc2
Consumed	Consumed	k1gMnSc1
Foods	Foods	k1gInSc1
-	-	kIx~
http://jn.nutrition.org/content/130/8/2086S.full	http://jn.nutrition.org/content/130/8/2086S.full	k1gInSc1
<g/>
↑	↑	k?
Alcohol	Alcohol	k1gInSc1
Consumption	Consumption	k1gInSc1
and	and	k?
Diabetes	diabetes	k1gInSc1
Mellitus	Mellitus	k1gInSc1
Mortality	mortalita	k1gFnSc2
in	in	k?
Different	Different	k1gMnSc1
Countries	Countries	k1gMnSc1
-	-	kIx~
http://ajph.aphapublications.org/cgi/reprint/73/11/1316.pdf	http://ajph.aphapublications.org/cgi/reprint/73/11/1316.pdf	k1gInSc1
<g/>
↑	↑	k?
http://monographs.iarc.fr/ENG/Classification/ClassificationsGroupOrder.pdf	http://monographs.iarc.fr/ENG/Classification/ClassificationsGroupOrder.pdf	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
25	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
Agents	Agentsa	k1gFnPc2
Classified	Classified	k1gMnSc1
by	by	k9
the	the	k?
IARC	IARC	kA
Monographs	Monographs	k1gInSc1
<g/>
↑	↑	k?
Typy	typa	k1gFnPc1
a	a	k8xC
druhy	druh	k1gInPc1
českého	český	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
TRADIČNÍ	tradiční	k2eAgFnSc2d1
TECHNOLOGIE	technologie	k1gFnSc2
VÝROBY	výroba	k1gFnSc2
PIVA	pivo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sci	sci	k?
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
WAGNER	Wagner	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kam	kam	k6eAd1
na	na	k7c4
Pivo	pivo	k1gNnSc4
-	-	kIx~
Chmel	chmel	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gInPc4
účinky	účinek	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-09-06	2007-09-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pivni-tacky	Pivni-tacka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Chmel	Chmel	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Volvox	Volvox	k1gInSc1
Globator	Globator	k1gInSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85769	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
18	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Volvox	Volvox	k1gInSc1
Globator	Globator	k1gInSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85769	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zajimavosti-pivo	Zajimavosti-pivo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
estranky	estranka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Výroba	výroba	k1gFnSc1
piva	pivo	k1gNnSc2
-	-	kIx~
sklizeň	sklizeň	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výroba	výroba	k1gFnSc1
sladu	slad	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://pivnirecenze.cz/13671-jak-se-ma-spravne-cepovat-pivo-ptali-jsme-se-pivovaru	http://pivnirecenze.cz/13671-jak-se-ma-spravne-cepovat-pivo-ptali-jsme-se-pivovar	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
-	-	kIx~
Jak	jak	k8xC,k8xS
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
správně	správně	k6eAd1
čepovat	čepovat	k5eAaImF
pivo	pivo	k1gNnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Ptali	ptat	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
se	se	k3xPyFc4
pivovarů	pivovar	k1gInPc2
<g/>
↑	↑	k?
http://xman.idnes.cz/silnejsi-pivo-nez-osmnactka-by-se-nemelo-delat-rika-sladek-p3n-/xman-styl.aspx?c=A110801_171113_xman-styl_fro	http://xman.idnes.cz/silnejsi-pivo-nez-osmnactka-by-se-nemelo-delat-rika-sladek-p3n-/xman-styl.aspx?c=A110801_171113_xman-styl_fro	k6eAd1
-	-	kIx~
Silnější	silný	k2eAgNnSc1d2
pivo	pivo	k1gNnSc1
než	než	k8xS
osmnáctka	osmnáctka	k1gFnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
nemělo	mít	k5eNaImAgNnS
dělat	dělat	k5eAaImF
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
sládek	sládek	k1gMnSc1
<g/>
↑	↑	k?
http://phys.org/news/2014-11-coffee-beer-video.html	http://phys.org/news/2014-11-coffee-beer-video.html	k1gMnSc1
-	-	kIx~
Why	Why	k1gMnSc1
does	does	k6eAd1
coffee	coffeat	k5eAaPmIp3nS
spill	spill	k1gInSc1
more	mor	k1gInSc5
often	oftit	k5eAaBmNgMnS,k5eAaPmNgMnS,k5eAaImNgMnS
than	than	k1gMnSc1
beer	beer	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Prazdroj	prazdroj	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Historie	historie	k1gFnSc1
piva	pivo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Pivni-tacky	Pivni-tack	k1gInPc7
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Historie	historie	k1gFnSc1
piva	pivo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://phys.org/news/2018-09-evidence-hypothesis-beer-cultivate-cereals.html	https://phys.org/news/2018-09-evidence-hypothesis-beer-cultivate-cereals.html	k1gMnSc1
-	-	kIx~
New	New	k1gMnSc1
evidence	evidence	k1gFnSc2
supports	supports	k1gInSc1
the	the	k?
hypothesis	hypothesis	k1gInSc1
that	that	k1gMnSc1
beer	beer	k1gMnSc1
may	may	k?
have	have	k1gInSc1
been	been	k1gMnSc1
motivation	motivation	k1gInSc1
to	ten	k3xDgNnSc1
cultivate	cultivat	k1gInSc5
cereals	cereals	k6eAd1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
HOUSER	houser	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Něco	něco	k6eAd1
málo	málo	k6eAd1
z	z	k7c2
historie	historie	k1gFnSc2
piva	pivo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scienceworld	Scienceworlda	k1gFnPc2
<g/>
,	,	kIx,
2001-06-05	2001-06-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
o	o	k7c6
čistotě	čistota	k1gFnSc6
-	-	kIx~
Reinheitsgebot	Reinheitsgebot	k1gInSc1
<g/>
..	..	k?
www.pivoerdinger.cz	www.pivoerdinger.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
o	o	k7c6
čistotě	čistota	k1gFnSc6
piva	pivo	k1gNnSc2
-	-	kIx~
Reinheitsgebot	Reinheitsgebot	k1gInSc1
<g/>
..	..	k?
www.vysebrodskypivovar.cz	www.vysebrodskypivovar.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Tajemství	tajemství	k1gNnSc1
německých	německý	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
s	s	k7c7
příchutěmi	příchuť	k1gFnPc7
bez	bez	k7c2
umělých	umělý	k2eAgNnPc2d1
ochucovadel	ochucovadlo	k1gNnPc2
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Volvox	Volvox	k1gInSc1
Globator	Globator	k1gInSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85769	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euro	euro	k1gNnSc1
Beer	Beera	k1gFnPc2
<g/>
.	.	kIx.
www.carlsberggroup.com	www.carlsberggroup.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	European	k1gMnSc1
Beer	Beer	k1gMnSc1
Star	star	k1gFnSc2
<g/>
↑	↑	k?
Nejstarší	starý	k2eAgInSc1d3
český	český	k2eAgInSc1d1
pivovar	pivovar	k1gInSc1
opět	opět	k6eAd1
vaří	vařit	k5eAaImIp3nS
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Průzkum	průzkum	k1gInSc1
<g/>
:	:	kIx,
ČR	ČR	kA
byla	být	k5eAaImAgFnS
loni	loni	k6eAd1
16	#num#	k4
<g/>
.	.	kIx.
největším	veliký	k2eAgMnSc7d3
producentem	producent	k1gMnSc7
piva	pivo	k1gNnSc2
na	na	k7c6
světě	svět	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČTK	ČTK	kA
<g/>
,	,	kIx,
2008-07-25	2008-07-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FENCL	Fencl	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
piva	pivo	k1gNnSc2
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
prodej	prodej	k1gInSc1
také	také	k9
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2007-12-13	2007-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://www.vscht.cz/popularizace/projekty/balling	https://www.vscht.cz/popularizace/projekty/balling	k1gInSc4
-	-	kIx~
Dej	dát	k5eAaPmRp2nS
si	se	k3xPyFc3
jedno	jeden	k4xCgNnSc1
na	na	k7c6
Ballinga	Ballinga	k1gFnSc1
<g/>
↑	↑	k?
Starobrno	Starobrno	k1gNnSc1
a.s.	a.s.	k?
-	-	kIx~
O	o	k7c6
společnosti	společnost	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starobrno	Starobrno	k1gNnSc1
a.s.	a.s.	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sazba	sazba	k1gFnSc1
daně	daň	k1gFnSc2
z	z	k7c2
piva	pivo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Konec	konec	k1gInSc1
piva	pivo	k1gNnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
"	"	kIx"
<g/>
Chceme	chtít	k5eAaImIp1nP
sladové	sladový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
levnější	levný	k2eAgInSc1d2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tn	tn	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vývoj	vývoj	k1gInSc1
ceny	cena	k1gFnSc2
lahvového	lahvový	k2eAgNnSc2d1
piva	pivo	k1gNnSc2
1960	#num#	k4
-	-	kIx~
2007	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Internationaler	Internationaler	k1gMnSc1
Bier-Pro-Kopf-Verbrauch	Bier-Pro-Kopf-Verbrauch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statistische	Statistische	k1gInSc1
Daten	Daten	k2eAgInSc4d1
über	über	k1gInSc4
die	die	k?
österreichische	österreichische	k1gInSc1
Brauwirtschaft	Brauwirtschaft	k2eAgInSc1d1
1980	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
(	(	kIx(
<g/>
pdf	pdf	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.bierserver.at	www.bierserver.at	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://belgium.beertourism.com/about-beer/history-of-beer	http://belgium.beertourism.com/about-beer/history-of-beer	k1gInSc1
-	-	kIx~
History	Histor	k1gInPc1
of	of	k?
Beer	Beer	k1gInSc1
<g/>
↑	↑	k?
Sociální	sociální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Spotřeba	spotřeba	k1gFnSc1
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
vína	víno	k1gNnSc2
a	a	k8xC
alkoholu	alkohol	k1gInSc2
na	na	k7c4
1	#num#	k4
obyvatele	obyvatel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Belgie	Belgie	k1gFnSc1
228,64	228,64	k4
litru	litr	k1gInSc2
↑	↑	k?
http://www.econ.kuleuven.be/licos/publications/discussion-papers/dp/dp271.pdf	http://www.econ.kuleuven.be/licos/publications/discussion-papers/dp/dp271.pdf	k1gInSc1
-	-	kIx~
Graf	graf	k1gInSc1
na	na	k7c6
obrázku	obrázek	k1gInSc6
1	#num#	k4
na	na	k7c4
str	str	kA
<g/>
.	.	kIx.
31	#num#	k4
<g/>
↑	↑	k?
Spotřeba	spotřeba	k1gFnSc1
piva	pivo	k1gNnSc2
celkem	celek	k1gInSc7
v	v	k7c6
ČR	ČR	kA
v	v	k7c6
letech	let	k1gInPc6
1948	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
17	#num#	k4
<g/>
↑	↑	k?
http://www.pivniobzor.cz/clanky/2008-08-26-pivni-deni-v-letech-1981-1983/	http://www.pivniobzor.cz/clanky/2008-08-26-pivni-deni-v-letech-1981-1983/	k4
-	-	kIx~
Pivní	pivní	k2eAgNnPc1d1
dění	dění	k1gNnPc1
v	v	k7c6
letech	let	k1gInPc6
1981	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
<g/>
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Spotřeba	spotřeba	k1gFnSc1
alkoholických	alkoholický	k2eAgInPc2d1
nápojů	nápoj	k1gInPc2
a	a	k8xC
cigaret	cigareta	k1gFnPc2
na	na	k7c4
1	#num#	k4
obyvatele	obyvatel	k1gMnSc4
v	v	k7c6
ČR	ČR	kA
v	v	k7c6
letech	let	k1gInPc6
1999	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Heineken	Heineken	k1gInSc1
pohltí	pohltit	k5eAaPmIp3nS
Zlatopramen	Zlatopramen	k2eAgInSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
e	e	k0
<g/>
15	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Máme	mít	k5eAaImIp1nP
skoro	skoro	k6eAd1
500	#num#	k4
druhů	druh	k1gInPc2
piv	pivo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybrané	vybraný	k2eAgFnPc4d1
kuriozity	kuriozita	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nova	nova	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Německému	německý	k2eAgInSc3d1
trhu	trh	k1gInSc3
s	s	k7c7
pivem	pivo	k1gNnSc7
hrozí	hrozit	k5eAaImIp3nS
největší	veliký	k2eAgInSc1d3
propad	propad	k1gInSc1
v	v	k7c6
poválečné	poválečný	k2eAgFnSc6d1
historii	historie	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
,	,	kIx,
2008-06-06	2008-06-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.pivo-a-hospody.cz/novinky-ze-sveta-piva/pivni-trh-v-cr-jake-pivovary-ho-ovladaji-a-jak-se-zmenil-postoj-konzumentu/	http://www.pivo-a-hospody.cz/novinky-ze-sveta-piva/pivni-trh-v-cr-jake-pivovary-ho-ovladaji-a-jak-se-zmenil-postoj-konzumentu/	k?
Archivováno	archivován	k2eAgNnSc1d1
22	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
-	-	kIx~
Pivní	pivní	k2eAgFnSc7d1
trh	trh	k1gInSc4
v	v	k7c6
ČR	ČR	kA
–	–	k?
jaké	jaký	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
pivovary	pivovar	k1gInPc4
ho	on	k3xPp3gMnSc4
ovládají	ovládat	k5eAaImIp3nP
a	a	k8xC
jak	jak	k6eAd1
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
postoj	postoj	k1gInSc1
konzumentů	konzument	k1gMnPc2
<g/>
?	?	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
:	:	kIx,
Pivo	pivo	k1gNnSc1
–	–	k?
Průvodce	průvodce	k1gMnSc1
světem	svět	k1gInSc7
piva	pivo	k1gNnSc2
pro	pro	k7c4
laiky	laik	k1gMnPc4
i	i	k8xC
odborníky	odborník	k1gMnPc4
<g/>
,	,	kIx,
Fortuna	Fortuna	k1gFnSc1
Print	Print	k1gInSc1
2001	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86144	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
EAN	EAN	kA
9788086144177	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Gregg	Gregg	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
Carrie	Carrie	k1gFnSc1
Getty	Getta	k1gFnSc2
<g/>
:	:	kIx,
Bible	bible	k1gFnSc1
pijáků	piják	k1gInPc2
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
Pragma	Pragma	k1gFnSc1
2003	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7205	#num#	k4
<g/>
-	-	kIx~
<g/>
669	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
EAN	EAN	kA
9788072056699	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Berry	Berra	k1gFnPc1
Verhoef	Verhoef	k1gInSc1
<g/>
:	:	kIx,
Kompletní	kompletní	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
Rebo	Rebo	k6eAd1
Productions	Productions	k1gInSc1
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7234	#num#	k4
<g/>
-	-	kIx~
<g/>
116	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
EAN	EAN	kA
9788072341160	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Věnek	Věnek	k1gInSc1
Zýbrt	Zýbrt	k1gInSc1
<g/>
:	:	kIx,
Velká	velký	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
piva	pivo	k1gNnSc2
–	–	k?
Vše	všechen	k3xTgNnSc1
o	o	k7c6
pivu	pivo	k1gNnSc6
<g/>
,	,	kIx,
Rubico	Rubico	k6eAd1
2005	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7346	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
54	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
EAN	EAN	kA
9788073460549	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Hana	Hana	k1gFnSc1
Večerková	Večerková	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Kiss	Kiss	k1gInSc1
<g/>
:	:	kIx,
Abeceda	abeceda	k1gFnSc1
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
Rubico	Rubico	k6eAd1
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85005	#num#	k4
<g/>
-	-	kIx~
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
EAN	EAN	kA
9788085005868	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
David	David	k1gMnSc1
Kenning	Kenning	k1gInSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
:	:	kIx,
Pivo	pivo	k1gNnSc1
<g/>
,	,	kIx,
Slovart	Slovart	k1gInSc1
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7209	#num#	k4
<g/>
-	-	kIx~
<g/>
775	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
EAN	EAN	kA
9788072097753	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
ZYKÁN	ZYKÁN	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malý	malý	k2eAgInSc1d1
katechismus	katechismus	k1gInSc1
pivovarnictví	pivovarnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Spolek	spolek	k1gInSc1
sládků	sládek	k1gMnPc2
<g/>
,	,	kIx,
1901	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
BĚLOHOUBEK	BĚLOHOUBEK	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pivovarnictví	pivovarnictví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Antonín	Antonín	k1gMnSc1
Bělohoubek	Bělohoubka	k1gFnPc2
<g/>
,	,	kIx,
1874	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
ZÍBRT	ZÍBRT	kA
<g/>
,	,	kIx,
Čeněk	Čeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pivo	pivo	k1gNnSc1
v	v	k7c6
písních	píseň	k1gFnPc6
lidových	lidový	k2eAgMnPc2d1
a	a	k8xC
znárodnělých	znárodnělý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Zachar	Zachar	k1gMnSc1
<g/>
,	,	kIx,
1909	#num#	k4
<g/>
.	.	kIx.
401	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
České	český	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
</s>
<s>
Výroba	výroba	k1gFnSc1
piva	pivo	k1gNnSc2
</s>
<s>
Tankové	tankový	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
</s>
<s>
Pivní	pivní	k2eAgFnSc1d1
láhev	láhev	k1gFnSc1
</s>
<s>
Podpivek	Podpivek	k1gInSc1
</s>
<s>
Kvas	kvas	k1gInSc1
</s>
<s>
Pivní	pivní	k2eAgFnPc1d1
lázně	lázeň	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
pivo	pivo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Pivo	pivo	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Pivo	pivo	k1gNnSc4
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Výukový	výukový	k2eAgInSc4d1
kurs	kurs	k1gInSc4
Pivo	pivo	k1gNnSc1
ve	v	k7c6
Wikiverzitě	Wikiverzita	k1gFnSc6
</s>
<s>
Téma	téma	k1gNnSc1
Pivo	pivo	k1gNnSc4
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Národní	národní	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
povolání	povolání	k1gNnSc2
<g/>
:	:	kIx,
Pivovarník	pivovarník	k1gMnSc1
a	a	k8xC
sladovník	sladovník	k1gMnSc1
na	na	k7c6
nsp	nsp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Pivní	pivní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
</s>
<s>
Pivovary	pivovar	k1gInPc1
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
</s>
<s>
Výzkumný	výzkumný	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pivovarský	pivovarský	k2eAgInSc1d1
a	a	k8xC
sladařský	sladařský	k2eAgInSc1d1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2	#num#	k4
</s>
<s>
Web	web	k1gInSc1
o	o	k7c6
pivu	pivo	k1gNnSc6
</s>
<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
co	co	k9
dělá	dělat	k5eAaImIp3nS
<g/>
:	:	kIx,
Pivo	pivo	k1gNnSc1
</s>
<s>
Historické	historický	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
s	s	k7c7
tématem	téma	k1gNnSc7
piva	pivo	k1gNnSc2
a	a	k8xC
pivovarnictví	pivovarnictví	k1gNnSc2
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
technické	technický	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
10	#num#	k4
nejzajímavějších	zajímavý	k2eAgMnPc2d3
faktů	fakt	k1gInPc2
o	o	k7c6
pivu	pivo	k1gNnSc6
</s>
<s>
Pivní	pivní	k2eAgInSc1d1
portál	portál	k1gInSc1
ZaPivem	ZaPivo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
objevujte	objevovat	k5eAaImRp2nP
s	s	k7c7
námi	my	k3xPp1nPc7
svět	svět	k1gInSc1
piva	pivo	k1gNnSc2
Archivováno	archivovat	k5eAaBmNgNnS
25	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Pivní	pivní	k2eAgInSc4d1
portál	portál	k1gInSc4
České	český	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
–	–	k?
České	český	k2eAgNnSc1d1
zlato	zlato	k1gNnSc1
</s>
<s>
Pivka	pivko	k1gNnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
katalog	katalog	k1gInSc1
českých	český	k2eAgNnPc2d1
piv	pivo	k1gNnPc2
s	s	k7c7
hodnocením	hodnocení	k1gNnSc7
a	a	k8xC
komentáři	komentář	k1gInPc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Pivo	pivo	k1gNnSc1
ve	v	k7c6
světě	svět	k1gInSc6
-	-	kIx~
Seznam	seznam	k1gInSc1
značek	značka	k1gFnPc2
piva	pivo	k1gNnSc2
Afrika	Afrika	k1gFnSc1
</s>
<s>
Keňa	Keňa	k1gFnSc1
•	•	k?
Maroko	Maroko	k1gNnSc1
•	•	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Asie	Asie	k1gFnSc2
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Hongkong	Hongkong	k1gInSc4
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Laos	Laos	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Singapur	Singapur	k1gInSc1
•	•	k?
Sýrie	Sýrie	k1gFnSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Tibet	Tibet	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Irsko	Irsko	k1gNnSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
Skotsko	Skotsko	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Wales	Wales	k1gInSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Karibik	Karibik	k1gInSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
USA	USA	kA
•	•	k?
Venezuela	Venezuela	k1gFnSc1
Oceánie	Oceánie	k1gFnSc2
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
Značky	značka	k1gFnSc2
piva	pivo	k1gNnSc2
•	•	k?
Seznam	seznam	k1gInSc1
států	stát	k1gInPc2
světa	svět	k1gInSc2
podle	podle	k7c2
spotřeby	spotřeba	k1gFnSc2
piva	pivo	k1gNnSc2
na	na	k7c4
osobu	osoba	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4006537-6	4006537-6	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
8692	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85012832	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85012832	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Pivo	pivo	k1gNnSc1
</s>
