<s>
Významným	významný	k2eAgInSc7d1	významný
mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
Břeclavi	Břeclav	k1gFnSc2	Břeclav
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k6eAd1	též
níže	nízce	k6eAd2	nízce
vývoj	vývoj	k1gInSc4	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
železnice	železnice	k1gFnSc2	železnice
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc2d1	severní
dráhy	dráha	k1gFnSc2	dráha
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
)	)	kIx)	)
-	-	kIx~	-
první	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
přijel	přijet	k5eAaPmAgInS	přijet
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1839	[number]	k4	1839
-	-	kIx~	-
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
vybudování	vybudování	k1gNnSc1	vybudování
prvního	první	k4xOgInSc2	první
železničního	železniční	k2eAgInSc2d1	železniční
uzlu	uzel	k1gInSc2	uzel
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
