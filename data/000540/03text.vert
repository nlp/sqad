<s>
Břeclav	Břeclav	k1gFnSc1	Břeclav
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Lundenburg	Lundenburg	k1gInSc1	Lundenburg
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Brzecław	Brzecław	k1gFnSc1	Brzecław
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Leventevár	Leventevár	k1gInSc1	Leventevár
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
oficiálně	oficiálně	k6eAd1	oficiálně
a	a	k8xC	a
v	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
nářečí	nářečí	k1gNnSc6	nářečí
Břeclava	Břeclava	k1gFnSc1	Břeclava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
jihojihovýchodně	jihojihovýchodně	k6eAd1	jihojihovýchodně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Dyji	Dyje	k1gFnSc6	Dyje
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
77,11	[number]	k4	77,11
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
kolem	kolem	k7c2	kolem
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Břeclav	Břeclav	k1gFnSc1	Břeclav
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
hraničním	hraniční	k2eAgInSc7d1	hraniční
přechodem	přechod	k1gInSc7	přechod
a	a	k8xC	a
železniční	železniční	k2eAgFnSc7d1	železniční
křižovatkou	křižovatka	k1gFnSc7	křižovatka
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
několika	několik	k4yIc2	několik
lokalit	lokalita	k1gFnPc2	lokalita
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Břeclavi	Břeclav	k1gFnSc2	Břeclav
osídlených	osídlený	k2eAgInPc2d1	osídlený
už	už	k6eAd1	už
v	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
Pohansko	Pohansko	k1gNnSc1	Pohansko
(	(	kIx(	(
<g/>
v	v	k7c6	v
lesích	les	k1gInPc6	les
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
významným	významný	k2eAgNnSc7d1	významné
velkomoravským	velkomoravský	k2eAgNnSc7d1	velkomoravské
hradiskem	hradisko	k1gNnSc7	hradisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
opuštěno	opustit	k5eAaPmNgNnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
založil	založit	k5eAaPmAgMnS	založit
kníže	kníže	k1gMnSc1	kníže
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
pohraniční	pohraniční	k2eAgInSc1d1	pohraniční
hrad	hrad	k1gInSc1	hrad
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
jméno	jméno	k1gNnSc1	jméno
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
správních	správní	k2eAgNnPc2d1	správní
center	centrum	k1gNnPc2	centrum
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
je	být	k5eAaImIp3nS	být
zmiňováno	zmiňovat	k5eAaImNgNnS	zmiňovat
jako	jako	k8xS	jako
Laventenburch	Laventenburch	k1gInSc1	Laventenburch
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gNnSc4	on
získala	získat	k5eAaPmAgFnS	získat
královna	královna	k1gFnSc1	královna
Konstancie	Konstancie	k1gFnSc1	Konstancie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
mohutná	mohutný	k2eAgFnSc1d1	mohutná
kamenná	kamenný	k2eAgFnSc1d1	kamenná
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1426	[number]	k4	1426
zde	zde	k6eAd1	zde
sídlila	sídlit	k5eAaImAgFnS	sídlit
husitská	husitský	k2eAgFnSc1d1	husitská
posádka	posádka	k1gFnSc1	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
nedaleké	daleký	k2eNgNnSc1d1	nedaleké
stejnojmenné	stejnojmenný	k2eAgNnSc1d1	stejnojmenné
městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
do	do	k7c2	do
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
blízkosti	blízkost	k1gFnSc2	blízkost
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založili	založit	k5eAaPmAgMnP	založit
(	(	kIx(	(
<g/>
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
centra	centrum	k1gNnSc2	centrum
<g/>
)	)	kIx)	)
městečko	městečko	k1gNnSc1	městečko
nové	nový	k2eAgNnSc1d1	nové
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
Nová	Nová	k1gFnSc1	Nová
Břeclav	Břeclav	k1gFnSc1	Břeclav
-	-	kIx~	-
původní	původní	k2eAgFnSc1d1	původní
lokalita	lokalita	k1gFnSc1	lokalita
nese	nést	k5eAaImIp3nS	nést
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
označení	označení	k1gNnSc2	označení
Stará	starý	k2eAgFnSc1d1	stará
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
získali	získat	k5eAaPmAgMnP	získat
hrad	hrad	k1gInSc4	hrad
i	i	k8xC	i
Novou	nový	k2eAgFnSc4d1	nová
a	a	k8xC	a
Starou	starý	k2eAgFnSc4d1	stará
Břeclav	Břeclav	k1gFnSc4	Břeclav
Žerotínové	Žerotínová	k1gFnSc2	Žerotínová
<g/>
;	;	kIx,	;
hrad	hrad	k1gInSc1	hrad
přestavěli	přestavět	k5eAaPmAgMnP	přestavět
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Velen	velen	k2eAgMnSc1d1	velen
ze	z	k7c2	z
Žerotína	Žerotína	k1gFnSc1	Žerotína
(	(	kIx(	(
<g/>
1589	[number]	k4	1589
<g/>
-	-	kIx~	-
<g/>
1622	[number]	k4	1622
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1618	[number]	k4	1618
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
břeclavské	břeclavský	k2eAgNnSc1d1	Břeclavské
panství	panství	k1gNnSc1	panství
konfiskováno	konfiskovat	k5eAaBmNgNnS	konfiskovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1638	[number]	k4	1638
jej	on	k3xPp3gInSc4	on
získali	získat	k5eAaPmAgMnP	získat
Lichtenštejnové	Lichtenštejn	k1gMnPc1	Lichtenštejn
<g/>
,	,	kIx,	,
vlastnící	vlastnící	k2eAgMnPc1d1	vlastnící
sousedních	sousední	k2eAgFnPc2d1	sousední
Valtic	Valtice	k1gFnPc2	Valtice
a	a	k8xC	a
Lednice	Lednice	k1gFnSc2	Lednice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
válek	válka	k1gFnPc2	válka
s	s	k7c7	s
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
následující	následující	k2eAgInPc1d1	následující
třicetileté	třicetiletý	k2eAgInPc1d1	třicetiletý
války	válek	k1gInPc1	válek
byly	být	k5eAaImAgInP	být
Stará	starý	k2eAgFnSc1d1	stará
i	i	k8xC	i
Nová	nový	k2eAgFnSc1d1	nová
Břeclav	Břeclav	k1gFnSc1	Břeclav
téměř	téměř	k6eAd1	téměř
zničeny	zničit	k5eAaPmNgInP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
stavebních	stavební	k2eAgFnPc2d1	stavební
a	a	k8xC	a
krajinářských	krajinářský	k2eAgFnPc2d1	krajinářská
úprav	úprava	k1gFnPc2	úprava
lichtenštejnského	lichtenštejnský	k2eAgNnSc2d1	Lichtenštejnské
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
daly	dát	k5eAaPmAgFnP	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
tzv.	tzv.	kA	tzv.
Lednicko-valtickému	lednickoaltický	k2eAgInSc3d1	lednicko-valtický
areálu	areál	k1gInSc3	areál
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
břeclavský	břeclavský	k2eAgInSc1d1	břeclavský
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nebyl	být	k5eNaImAgMnS	být
sídelním	sídelní	k2eAgMnSc7d1	sídelní
<g/>
,	,	kIx,	,
přestavěn	přestavět	k5eAaPmNgInS	přestavět
v	v	k7c6	v
romantickém	romantický	k2eAgInSc6d1	romantický
stylu	styl	k1gInSc6	styl
na	na	k7c4	na
umělou	umělý	k2eAgFnSc4d1	umělá
zříceninu	zřícenina	k1gFnSc4	zřícenina
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
Břeclavi	Břeclav	k1gFnSc2	Břeclav
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k6eAd1	též
níže	nízce	k6eAd2	nízce
vývoj	vývoj	k1gInSc4	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
železnice	železnice	k1gFnSc2	železnice
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc2d1	severní
dráhy	dráha	k1gFnSc2	dráha
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
)	)	kIx)	)
-	-	kIx~	-
první	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
přijel	přijet	k5eAaPmAgInS	přijet
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1839	[number]	k4	1839
-	-	kIx~	-
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
vybudování	vybudování	k1gNnSc1	vybudování
prvního	první	k4xOgInSc2	první
železničního	železniční	k2eAgInSc2d1	železniční
uzlu	uzel	k1gInSc2	uzel
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
industrializace	industrializace	k1gFnSc1	industrializace
(	(	kIx(	(
<g/>
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
,	,	kIx,	,
pila	pila	k1gFnSc1	pila
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
Poštorné	Poštorný	k2eAgFnPc1d1	Poštorná
cihelna	cihelna	k1gFnSc1	cihelna
a	a	k8xC	a
chemická	chemický	k2eAgFnSc1d1	chemická
továrna	továrna	k1gFnSc1	továrna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc2d1	železniční
uzel	uzel	k1gInSc4	uzel
se	s	k7c7	s
zázemím	zázemí	k1gNnSc7	zázemí
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
rozšiřován	rozšiřovat	k5eAaImNgInS	rozšiřovat
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
Mikulov	Mikulov	k1gInSc4	Mikulov
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
na	na	k7c4	na
Kúty	Kúty	k1gInPc4	Kúty
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
do	do	k7c2	do
Lednice	Lednice	k1gFnSc2	Lednice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
Břeclav	Břeclav	k1gFnSc1	Břeclav
stala	stát	k5eAaPmAgFnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
soudního	soudní	k2eAgInSc2d1	soudní
okresu	okres	k1gInSc2	okres
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
1872	[number]	k4	1872
byla	být	k5eAaImAgFnS	být
povýšena	povýšit	k5eAaPmNgFnS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
vzrůstaly	vzrůstat	k5eAaImAgInP	vzrůstat
nacionálně	nacionálně	k6eAd1	nacionálně
laděné	laděný	k2eAgInPc4d1	laděný
spory	spor	k1gInPc4	spor
(	(	kIx(	(
<g/>
ve	v	k7c6	v
městě	město	k1gNnSc6	město
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
českojazyčná	českojazyčný	k2eAgFnSc1d1	českojazyčná
většina	většina	k1gFnSc1	většina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc4d1	projevující
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c6	o
radnici	radnice	k1gFnSc6	radnice
po	po	k7c4	po
28	[number]	k4	28
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Připojením	připojení	k1gNnSc7	připojení
Valticka	Valticko	k1gNnSc2	Valticko
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
Československa	Československo	k1gNnSc2	Československo
také	také	k6eAd1	také
dolnorakouské	dolnorakouský	k2eAgFnSc2d1	Dolnorakouská
obce	obec	k1gFnSc2	obec
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
i	i	k9	i
menší	malý	k2eAgFnSc1d2	menší
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
moderního	moderní	k2eAgNnSc2d1	moderní
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Břeclav	Břeclav	k1gFnSc4	Břeclav
s	s	k7c7	s
areálem	areál	k1gInSc7	areál
dnešní	dnešní	k2eAgFnSc2d1	dnešní
břeclavské	břeclavský	k2eAgFnSc2d1	břeclavská
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Charvátská	charvátský	k2eAgFnSc1d1	Charvátská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
Stará	Stará	k1gFnSc1	Stará
Břeclav	Břeclav	k1gFnSc1	Břeclav
a	a	k8xC	a
Břeclav-židovská	Břeclav-židovský	k2eAgFnSc1d1	Břeclav-židovský
obec	obec	k1gFnSc1	obec
sloučeny	sloučen	k2eAgInPc1d1	sloučen
s	s	k7c7	s
Břeclaví	Břeclav	k1gFnSc7	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
-	-	kIx~	-
národnostně	národnostně	k6eAd1	národnostně
velkou	velký	k2eAgFnSc7d1	velká
většinou	většina	k1gFnSc7	většina
české	český	k2eAgInPc1d1	český
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
se	s	k7c7	s
strategickým	strategický	k2eAgInSc7d1	strategický
významem	význam	k1gInSc7	význam
-	-	kIx~	-
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
správních	správní	k2eAgFnPc2d1	správní
reforem	reforma	k1gFnPc2	reforma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
Břeclav	Břeclav	k1gFnSc1	Břeclav
stala	stát	k5eAaPmAgFnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
politického	politický	k2eAgInSc2d1	politický
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Územní	územní	k2eAgFnSc1d1	územní
reorganizace	reorganizace	k1gFnSc1	reorganizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
zrušila	zrušit	k5eAaPmAgFnS	zrušit
okresy	okres	k1gInPc1	okres
Mikulov	Mikulov	k1gInSc1	Mikulov
a	a	k8xC	a
Hustopeče	Hustopeč	k1gFnSc2	Hustopeč
a	a	k8xC	a
připojila	připojit	k5eAaPmAgFnS	připojit
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
Břeclavi	Břeclav	k1gFnSc3	Břeclav
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
stala	stát	k5eAaPmAgFnS	stát
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1974	[number]	k4	1974
a	a	k8xC	a
1976	[number]	k4	1976
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
sousední	sousední	k2eAgFnPc1d1	sousední
obce	obec	k1gFnPc1	obec
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
<g/>
,	,	kIx,	,
Charvátská	charvátský	k2eAgFnSc1d1	Charvátská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
a	a	k8xC	a
Ladná	ladný	k2eAgFnSc1d1	ladná
a	a	k8xC	a
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
byly	být	k5eAaImAgFnP	být
přestavěny	přestavět	k5eAaPmNgFnP	přestavět
v	v	k7c6	v
dobovém	dobový	k2eAgMnSc6d1	dobový
duchu	duch	k1gMnSc6	duch
(	(	kIx(	(
<g/>
panelová	panelový	k2eAgNnPc1d1	panelové
sídliště	sídliště	k1gNnPc1	sídliště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
začal	začít	k5eAaPmAgInS	začít
útlum	útlum	k1gInSc4	útlum
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Ladná	ladný	k2eAgFnSc1d1	ladná
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
souhrnný	souhrnný	k2eAgInSc4d1	souhrnný
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
samostatných	samostatný	k2eAgFnPc2d1	samostatná
obcí	obec	k1gFnPc2	obec
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
Břeclav-židovská	Břeclav-židovský	k2eAgFnSc1d1	Břeclav-židovský
obec	obec	k1gFnSc1	obec
a	a	k8xC	a
Stará	starý	k2eAgFnSc1d1	stará
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
posléze	posléze	k6eAd1	posléze
sjednoceny	sjednotit	k5eAaPmNgFnP	sjednotit
<g/>
.	.	kIx.	.
</s>
<s>
Razantní	razantní	k2eAgInSc1d1	razantní
nárůst	nárůst	k1gInSc1	nárůst
populace	populace	k1gFnSc2	populace
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dokládá	dokládat	k5eAaImIp3nS	dokládat
klíčový	klíčový	k2eAgInSc4d1	klíčový
význam	význam	k1gInSc4	význam
železnice	železnice	k1gFnSc2	železnice
a	a	k8xC	a
navazující	navazující	k2eAgFnSc2d1	navazující
industrializace	industrializace	k1gFnSc2	industrializace
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
přišla	přijít	k5eAaPmAgFnS	přijít
Břeclav	Břeclav	k1gFnSc1	Břeclav
o	o	k7c4	o
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
židovského	židovský	k2eAgNnSc2d1	Židovské
náboženství	náboženství	k1gNnSc2	náboženství
(	(	kIx(	(
<g/>
4,3	[number]	k4	4,3
<g/>
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
11,6	[number]	k4	11,6
<g/>
%	%	kIx~	%
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
zažilo	zažít	k5eAaPmAgNnS	zažít
město	město	k1gNnSc1	město
masivní	masivní	k2eAgInPc1d1	masivní
přesuny	přesun	k1gInPc1	přesun
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
ve	v	k7c6	v
dnech	den	k1gInPc6	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
Břeclav	Břeclav	k1gFnSc1	Břeclav
opustili	opustit	k5eAaPmAgMnP	opustit
příslušníci	příslušník	k1gMnPc1	příslušník
československých	československý	k2eAgInPc2d1	československý
ozbrojených	ozbrojený	k2eAgInPc2d1	ozbrojený
a	a	k8xC	a
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
<g/>
,	,	kIx,	,
pracovníci	pracovník	k1gMnPc1	pracovník
státních	státní	k2eAgInPc2d1	státní
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
zejména	zejména	k9	zejména
vlastenecky	vlastenecky	k6eAd1	vlastenecky
vystupující	vystupující	k2eAgFnPc4d1	vystupující
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
živnostníci	živnostník	k1gMnPc1	živnostník
a	a	k8xC	a
inteligence	inteligence	k1gFnPc1	inteligence
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
německý	německý	k2eAgInSc4d1	německý
režim	režim	k1gInSc4	režim
terorizoval	terorizovat	k5eAaImAgMnS	terorizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
osvobození	osvobození	k1gNnSc2	osvobození
město	město	k1gNnSc4	město
masivně	masivně	k6eAd1	masivně
opustili	opustit	k5eAaPmAgMnP	opustit
obyvatelé	obyvatel	k1gMnPc1	obyvatel
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
před	před	k7c7	před
pomstou	pomsta	k1gFnSc7	pomsta
českého	český	k2eAgNnSc2d1	české
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
osídlení	osídlení	k1gNnSc3	osídlení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zůstal	zůstat	k5eAaPmAgInS	zůstat
pouze	pouze	k6eAd1	pouze
malý	malý	k2eAgInSc1d1	malý
zlomek	zlomek	k1gInSc1	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
sousední	sousední	k2eAgFnPc1d1	sousední
obce	obec	k1gFnPc1	obec
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
<g/>
,	,	kIx,	,
Charvátská	charvátský	k2eAgFnSc1d1	Charvátská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
a	a	k8xC	a
Ladná	ladný	k2eAgFnSc1d1	ladná
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
opět	opět	k6eAd1	opět
oddělila	oddělit	k5eAaPmAgFnS	oddělit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Břeclav	Břeclav	k1gFnSc1	Břeclav
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
a	a	k8xC	a
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
jim	on	k3xPp3gMnPc3	on
odpovídajících	odpovídající	k2eAgFnPc2d1	odpovídající
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Břeclav	Břeclav	k1gFnSc1	Břeclav
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc1d1	východní
polovina	polovina	k1gFnSc1	polovina
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
asi	asi	k9	asi
15	[number]	k4	15
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Charvátská	charvátský	k2eAgFnSc1d1	Charvátská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
(	(	kIx(	(
<g/>
severozápad	severozápad	k1gInSc1	severozápad
<g/>
,	,	kIx,	,
asi	asi	k9	asi
5	[number]	k4	5
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
(	(	kIx(	(
<g/>
jihozápad	jihozápad	k1gInSc1	jihozápad
<g/>
,	,	kIx,	,
asi	asi	k9	asi
6	[number]	k4	6
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jako	jako	k8xS	jako
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
označuje	označovat	k5eAaImIp3nS	označovat
ještě	ještě	k6eAd1	ještě
Stará	starý	k2eAgFnSc1d1	stará
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k8xC	ale
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
Břeclavi	Břeclav	k1gFnSc6	Břeclav
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
-	-	kIx~	-
Ladná	ladný	k2eAgFnSc1d1	ladná
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Okres	okres	k1gInSc1	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
a	a	k8xC	a
Obvod	obvod	k1gInSc1	obvod
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Břeclav	Břeclav	k1gFnSc1	Břeclav
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
63	[number]	k4	63
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
z	z	k7c2	z
18	[number]	k4	18
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Věznice	věznice	k1gFnSc2	věznice
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Věznice	věznice	k1gFnSc1	věznice
Břeclav	Břeclav	k1gFnSc1	Břeclav
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
vězeňský	vězeňský	k2eAgInSc4d1	vězeňský
objekt	objekt	k1gInSc4	objekt
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1997	[number]	k4	1997
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
věznicí	věznice	k1gFnSc7	věznice
vazební	vazební	k2eAgFnSc1d1	vazební
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
vazby	vazba	k1gFnSc2	vazba
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
pro	pro	k7c4	pro
Břeclavsko	Břeclavsko	k1gNnSc4	Břeclavsko
<g/>
,	,	kIx,	,
Hodonínsko	Hodonínsko	k1gNnSc4	Hodonínsko
<g/>
,	,	kIx,	,
Uherskohradišťsko	Uherskohradišťsko	k1gNnSc4	Uherskohradišťsko
a	a	k8xC	a
Zlínsko	Zlínsko	k1gNnSc4	Zlínsko
<g/>
.	.	kIx.	.
</s>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
trestu	trest	k1gInSc2	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
sloužilo	sloužit	k5eAaImAgNnS	sloužit
pro	pro	k7c4	pro
odsouzené	odsouzená	k1gFnPc4	odsouzená
zařazené	zařazený	k2eAgFnPc4d1	zařazená
v	v	k7c6	v
dozoru	dozor	k1gInSc6	dozor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reprofilace	reprofilace	k1gFnSc2	reprofilace
z	z	k7c2	z
vazební	vazební	k2eAgFnSc2d1	vazební
věznice	věznice	k1gFnSc2	věznice
stala	stát	k5eAaPmAgFnS	stát
věznice	věznice	k1gFnSc1	věznice
pro	pro	k7c4	pro
odsouzené	odsouzený	k2eAgMnPc4d1	odsouzený
muže	muž	k1gMnPc4	muž
zařazené	zařazený	k2eAgFnSc2d1	zařazená
do	do	k7c2	do
výkonu	výkon	k1gInSc2	výkon
trestu	trest	k1gInSc2	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
s	s	k7c7	s
dohledem	dohled	k1gInSc7	dohled
a	a	k8xC	a
s	s	k7c7	s
ostrahou	ostraha	k1gFnSc7	ostraha
<g/>
.	.	kIx.	.
</s>
<s>
Břeclav	Břeclav	k1gFnSc1	Břeclav
je	být	k5eAaImIp3nS	být
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
evropského	evropský	k2eAgInSc2d1	evropský
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Kříží	křížit	k5eAaImIp3nS	křížit
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
železniční	železniční	k2eAgInSc4d1	železniční
koridor	koridor	k1gInSc4	koridor
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
tratě	trať	k1gFnPc4	trať
SŽDC	SŽDC	kA	SŽDC
č.	č.	k?	č.
250	[number]	k4	250
a	a	k8xC	a
330	[number]	k4	330
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
vedena	veden	k2eAgFnSc1d1	vedena
dálková	dálkový	k2eAgFnSc1d1	dálková
osobní	osobní	k2eAgFnSc1d1	osobní
i	i	k8xC	i
nákladní	nákladní	k2eAgFnSc1d1	nákladní
doprava	doprava	k1gFnSc1	doprava
ve	v	k7c6	v
směrech	směr	k1gInPc6	směr
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Berlín	Berlín	k1gInSc1	Berlín
-	-	kIx~	-
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
,	,	kIx,	,
Přerov	Přerov	k1gInSc1	Přerov
-	-	kIx~	-
Ostrava	Ostrava	k1gFnSc1	Ostrava
-	-	kIx~	-
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
-	-	kIx~	-
Budapešť	Budapešť	k1gFnSc1	Budapešť
(	(	kIx(	(
<g/>
-	-	kIx~	-
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
Graz	Graz	k1gMnSc1	Graz
/	/	kIx~	/
Villach	Villach	k1gMnSc1	Villach
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
lokální	lokální	k2eAgFnSc1d1	lokální
osobní	osobní	k2eAgFnSc1d1	osobní
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
tratí	trať	k1gFnPc2	trať
ještě	ještě	k9	ještě
Trať	trať	k1gFnSc1	trať
246	[number]	k4	246
směr	směr	k1gInSc1	směr
Mikulov	Mikulov	k1gInSc1	Mikulov
a	a	k8xC	a
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
začleněná	začleněný	k2eAgFnSc1d1	začleněná
do	do	k7c2	do
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
železniční	železniční	k2eAgFnPc1d1	železniční
linky	linka	k1gFnPc1	linka
R	R	kA	R
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
53	[number]	k4	53
<g/>
,	,	kIx,	,
S8	S8	k1gFnSc1	S8
a	a	k8xC	a
S	s	k7c7	s
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
regionální	regionální	k2eAgFnSc6d1	regionální
trati	trať	k1gFnSc6	trať
247	[number]	k4	247
Břeclav	Břeclav	k1gFnSc1	Břeclav
-	-	kIx~	-
Lednice	Lednice	k1gFnSc1	Lednice
je	být	k5eAaImIp3nS	být
sezónní	sezónní	k2eAgInSc4d1	sezónní
turistický	turistický	k2eAgInSc4d1	turistický
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Břeclavi	Břeclav	k1gFnSc2	Břeclav
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ještě	ještě	k6eAd1	ještě
železniční	železniční	k2eAgFnPc1d1	železniční
stanice	stanice	k1gFnPc1	stanice
Boří	bořit	k5eAaImIp3nP	bořit
les	les	k1gInSc4	les
a	a	k8xC	a
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Charvátská	charvátský	k2eAgFnSc1d1	Charvátská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
a	a	k8xC	a
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
katastru	katastr	k1gInSc2	katastr
ještě	ještě	k9	ještě
nákladní	nákladní	k2eAgNnSc1d1	nákladní
nádraží	nádraží	k1gNnSc1	nádraží
Hrušky	hruška	k1gFnSc2	hruška
<g/>
.	.	kIx.	.
</s>
<s>
Břeclav	Břeclav	k1gFnSc1	Břeclav
je	být	k5eAaImIp3nS	být
také	také	k9	také
významnou	významný	k2eAgFnSc7d1	významná
silniční	silniční	k2eAgFnSc7d1	silniční
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
;	;	kIx,	;
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
vede	vést	k5eAaImIp3nS	vést
dálnice	dálnice	k1gFnSc1	dálnice
D2	D2	k1gFnSc1	D2
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
kříží	křížit	k5eAaImIp3nP	křížit
na	na	k7c6	na
exitu	exit	k1gInSc6	exit
48	[number]	k4	48
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
55	[number]	k4	55
(	(	kIx(	(
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
Hodonín	Hodonín	k1gInSc1	Hodonín
-	-	kIx~	-
Břeclav	Břeclav	k1gFnSc1	Břeclav
-	-	kIx~	-
hraniční	hraniční	k2eAgInSc1d1	hraniční
přechod	přechod	k1gInSc1	přechod
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
<g/>
/	/	kIx~	/
<g/>
Reintal	Reintal	k1gMnSc1	Reintal
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
v	v	k7c4	v
Poštorné	Poštorný	k2eAgNnSc4d1	Poštorné
napojuje	napojovat	k5eAaImIp3nS	napojovat
silnice	silnice	k1gFnPc4	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
z	z	k7c2	z
Mikulova	Mikulov	k1gInSc2	Mikulov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
exitu	exit	k1gInSc2	exit
48	[number]	k4	48
je	být	k5eAaImIp3nS	být
napojena	napojen	k2eAgFnSc1d1	napojena
i	i	k9	i
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
425	[number]	k4	425
od	od	k7c2	od
Hustopečí	Hustopeč	k1gFnPc2	Hustopeč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
z	z	k7c2	z
Břeclavi	Břeclav	k1gFnSc2	Břeclav
na	na	k7c4	na
Lanžhot	Lanžhot	k1gInSc4	Lanžhot
<g/>
.	.	kIx.	.
</s>
<s>
Územím	území	k1gNnSc7	území
města	město	k1gNnSc2	město
prochází	procházet	k5eAaImIp3nS	procházet
i	i	k9	i
silnice	silnice	k1gFnSc1	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
:	:	kIx,	:
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
221	[number]	k4	221
Břeclav	Břeclav	k1gFnSc1	Břeclav
-	-	kIx~	-
Ladná	ladný	k2eAgFnSc1d1	ladná
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5528	[number]	k4	5528
k	k	k7c3	k
železniční	železniční	k2eAgFnSc3d1	železniční
stanici	stanice	k1gFnSc3	stanice
Hrušky	hruška	k1gFnSc2	hruška
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5529	[number]	k4	5529
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
-	-	kIx~	-
hraniční	hraniční	k2eAgFnSc1d1	hraniční
přechod	přechod	k1gInSc1	přechod
-	-	kIx~	-
(	(	kIx(	(
<g/>
Bernardshal	Bernardshal	k1gMnSc1	Bernardshal
<g/>
)	)	kIx)	)
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
41417	[number]	k4	41417
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
-	-	kIx~	-
Charv	Charv	k1gMnSc1	Charv
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
ves	ves	k1gFnSc1	ves
-	-	kIx~	-
Lednice	Lednice	k1gFnSc1	Lednice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
4231	[number]	k4	4231
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
425	[number]	k4	425
na	na	k7c4	na
Moravský	moravský	k2eAgInSc4d1	moravský
Žižkov	Žižkov	k1gInSc4	Žižkov
Současná	současný	k2eAgFnSc1d1	současná
dopravní	dopravní	k2eAgFnSc1d1	dopravní
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
je	být	k5eAaImIp3nS	být
problematická	problematický	k2eAgFnSc1d1	problematická
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
existence	existence	k1gFnSc2	existence
jediné	jediný	k2eAgFnSc2d1	jediná
silniční	silniční	k2eAgFnSc2d1	silniční
komunikace	komunikace	k1gFnSc2	komunikace
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
mezi	mezi	k7c7	mezi
místními	místní	k2eAgFnPc7d1	místní
částmi	část	k1gFnPc7	část
i	i	k9	i
pro	pro	k7c4	pro
tranzit	tranzit	k1gInSc4	tranzit
<g/>
.	.	kIx.	.
</s>
<s>
Výstavbu	výstavba	k1gFnSc4	výstavba
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
alternativního	alternativní	k2eAgNnSc2d1	alternativní
spojení	spojení	k1gNnSc2	spojení
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
náročný	náročný	k2eAgInSc1d1	náročný
lužní	lužní	k2eAgInSc1d1	lužní
terén	terén	k1gInSc1	terén
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
okolí	okolí	k1gNnSc6	okolí
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
obchvatu	obchvat	k1gInSc2	obchvat
navíc	navíc	k6eAd1	navíc
dlouholeté	dlouholetý	k2eAgInPc4d1	dlouholetý
spory	spor	k1gInPc4	spor
o	o	k7c4	o
vedení	vedení	k1gNnSc4	vedení
dálničního	dálniční	k2eAgNnSc2d1	dálniční
spojení	spojení	k1gNnSc2	spojení
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
Vídně	Vídeň	k1gFnSc2	Vídeň
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Břeclav	Břeclav	k1gFnSc4	Břeclav
nebo	nebo	k8xC	nebo
přes	přes	k7c4	přes
Mikulov	Mikulov	k1gInSc4	Mikulov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
provozuje	provozovat	k5eAaImIp3nS	provozovat
Bors	Bors	k1gInSc4	Bors
Břeclav	Břeclav	k1gFnSc1	Břeclav
a.	a.	k?	a.
s.	s.	k?	s.
9	[number]	k4	9
linek	linka	k1gFnPc2	linka
městské	městský	k2eAgFnSc2d1	městská
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
začleněných	začleněný	k2eAgFnPc2d1	začleněná
do	do	k7c2	do
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
linky	linka	k1gFnPc1	linka
561	[number]	k4	561
až	až	k9	až
569	[number]	k4	569
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
turistická	turistický	k2eAgFnSc1d1	turistická
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Dyji	Dyje	k1gFnSc6	Dyje
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
linek	linka	k1gFnPc2	linka
na	na	k7c4	na
Janův	Janův	k2eAgInSc4d1	Janův
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
Pohansko	Pohansko	k1gNnSc4	Pohansko
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
zavedena	zavést	k5eAaPmNgFnS	zavést
i	i	k9	i
"	"	kIx"	"
<g/>
městská	městský	k2eAgFnSc1d1	městská
linka	linka	k1gFnSc1	linka
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
Starou	starý	k2eAgFnSc7d1	stará
Břeclaví	Břeclav	k1gFnSc7	Břeclav
a	a	k8xC	a
stavidlem	stavidlo	k1gNnSc7	stavidlo
u	u	k7c2	u
Poštorné	Poštorný	k2eAgFnSc2d1	Poštorná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
pouze	pouze	k6eAd1	pouze
linka	linka	k1gFnSc1	linka
Břeclav	Břeclav	k1gFnSc1	Břeclav
-	-	kIx~	-
Janův	Janův	k2eAgInSc1d1	Janův
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
sídlí	sídlet	k5eAaImIp3nS	sídlet
několik	několik	k4yIc1	několik
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
veřejné	veřejný	k2eAgNnSc1d1	veřejné
<g/>
:	:	kIx,	:
Gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
státní	státní	k2eAgFnSc2d1	státní
jazykové	jazykový	k2eAgFnSc2d1	jazyková
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
,	,	kIx,	,
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
a	a	k8xC	a
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
Břeclav	Břeclav	k1gFnSc1	Břeclav
(	(	kIx(	(
<g/>
administrativně	administrativně	k6eAd1	administrativně
sloučené	sloučený	k2eAgFnPc1d1	sloučená
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
soukromé	soukromý	k2eAgNnSc1d1	soukromé
<g/>
:	:	kIx,	:
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Břeclav	Břeclav	k1gFnSc1	Břeclav
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
manažerská	manažerský	k2eAgFnSc1d1	manažerská
a	a	k8xC	a
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
obou	dva	k4xCgInPc2	dva
stupňů	stupeň	k1gInPc2	stupeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Samospráva	samospráva	k1gFnSc1	samospráva
města	město	k1gNnSc2	město
vyvěšuje	vyvěšovat	k5eAaImIp3nS	vyvěšovat
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
moravskou	moravský	k2eAgFnSc4d1	Moravská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
<g/>
.	.	kIx.	.
renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgInSc2d1	původní
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
motivem	motiv	k1gInSc7	motiv
zámku	zámek	k1gInSc2	zámek
je	být	k5eAaImIp3nS	být
arkádová	arkádový	k2eAgFnSc1d1	arkádová
pavlač	pavlač	k1gFnSc1	pavlač
na	na	k7c6	na
toskánských	toskánský	k2eAgInPc6d1	toskánský
sloupech	sloup	k1gInPc6	sloup
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
romantická	romantický	k2eAgFnSc1d1	romantická
přestavba	přestavba	k1gFnSc1	přestavba
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
umělé	umělý	k2eAgFnSc2d1	umělá
zříceniny	zřícenina	k1gFnSc2	zřícenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc1	zámek
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
,	,	kIx,	,
zchátralý	zchátralý	k2eAgInSc1d1	zchátralý
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nS	řešit
otázka	otázka	k1gFnSc1	otázka
jeho	jeho	k3xOp3gFnSc2	jeho
opravy	oprava	k1gFnSc2	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnSc3	veřejnost
je	být	k5eAaImIp3nS	být
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
zámecká	zámecký	k2eAgFnSc1d1	zámecká
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
provozuje	provozovat	k5eAaImIp3nS	provozovat
ji	on	k3xPp3gFnSc4	on
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
navazuje	navazovat	k5eAaImIp3nS	navazovat
komplex	komplex	k1gInSc1	komplex
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
areálů	areál	k1gInPc2	areál
(	(	kIx(	(
<g/>
Vranův	Vranův	k2eAgInSc1d1	Vranův
mlýn	mlýn	k1gInSc1	mlýn
na	na	k7c6	na
Mlýnském	mlýnský	k2eAgInSc6d1	mlýnský
náhonu	náhon	k1gInSc6	náhon
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zpustlý	zpustlý	k2eAgInSc1d1	zpustlý
<g/>
;	;	kIx,	;
zámecký	zámecký	k2eAgInSc1d1	zámecký
pivovar	pivovar	k1gInSc1	pivovar
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
v	v	k7c6	v
obnoveném	obnovený	k2eAgInSc6d1	obnovený
provozu	provoz	k1gInSc6	provoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
využívaný	využívaný	k2eAgInSc1d1	využívaný
pro	pro	k7c4	pro
turistické	turistický	k2eAgFnPc4d1	turistická
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
volně	volně	k6eAd1	volně
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
lesní	lesní	k2eAgInSc4d1	lesní
komplex	komplex	k1gInSc4	komplex
zvaný	zvaný	k2eAgInSc4d1	zvaný
Kančí	kančí	k2eAgFnSc4d1	kančí
obora	obora	k1gFnSc1	obora
<g/>
.	.	kIx.	.
</s>
<s>
Kančí	kančí	k2eAgFnSc1d1	kančí
obora	obora	k1gFnSc1	obora
-	-	kIx~	-
starý	starý	k2eAgInSc1d1	starý
lužní	lužní	k2eAgInSc1d1	lužní
les	les	k1gInSc1	les
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
starých	starý	k2eAgFnPc2d1	stará
ramen	rameno	k1gNnPc2	rameno
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
,	,	kIx,	,
zachovanými	zachovaný	k2eAgFnPc7d1	zachovaná
lučními	luční	k2eAgFnPc7d1	luční
enklávami	enkláva	k1gFnPc7	enkláva
a	a	k8xC	a
malým	malý	k2eAgNnSc7d1	malé
jezírkem	jezírko	k1gNnSc7	jezírko
Bruksa	Bruksa	k1gFnSc1	Bruksa
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
lovecká	lovecký	k2eAgFnSc1d1	lovecká
obora	obora	k1gFnSc1	obora
farní	farní	k2eAgFnSc1d1	farní
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc4	Masaryk
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
náletu	nálet	k1gInSc6	nálet
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1944	[number]	k4	1944
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
,	,	kIx,	,
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
jeho	jeho	k3xOp3gFnSc1	jeho
místě	místo	k1gNnSc6	místo
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
dokončena	dokončen	k2eAgFnSc1d1	dokončena
moderní	moderní	k2eAgFnSc1d1	moderní
stavba	stavba	k1gFnSc1	stavba
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Kolka	Kolek	k1gMnSc2	Kolek
synagoga	synagoga	k1gFnSc1	synagoga
-	-	kIx~	-
halová	halový	k2eAgFnSc1d1	halová
volně	volně	k6eAd1	volně
stojící	stojící	k2eAgFnSc1d1	stojící
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
upravena	upravit	k5eAaPmNgFnS	upravit
patrně	patrně	k6eAd1	patrně
Maxem	Max	k1gMnSc7	Max
Fleischerem	Fleischer	k1gMnSc7	Fleischer
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
v	v	k7c6	v
novorománském	novorománský	k2eAgInSc6d1	novorománský
stylu	styl	k1gInSc6	styl
s	s	k7c7	s
maurskými	maurský	k2eAgInPc7d1	maurský
prvky	prvek	k1gInPc7	prvek
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starší	starý	k2eAgFnSc2d2	starší
stavby	stavba	k1gFnSc2	stavba
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
v	v	k7c6	v
parku	park	k1gInSc6	park
před	před	k7c7	před
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
,	,	kIx,	,
novogotická	novogotický	k2eAgFnSc1d1	novogotická
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
1856	[number]	k4	1856
<g/>
;	;	kIx,	;
před	před	k7c7	před
postavením	postavení	k1gNnSc7	postavení
nového	nový	k2eAgInSc2d1	nový
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Václava	Václava	k1gFnSc1	Václava
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
provizorium	provizorium	k1gNnSc4	provizorium
místní	místní	k2eAgFnSc2d1	místní
katolické	katolický	k2eAgFnSc2d1	katolická
farnosti	farnost	k1gFnSc2	farnost
kaple	kaple	k1gFnSc2	kaple
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
Páně	páně	k2eAgFnSc1d1	páně
z	z	k7c2	z
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
1875	[number]	k4	1875
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
bývalého	bývalý	k2eAgInSc2d1	bývalý
hřbitova	hřbitov	k1gInSc2	hřbitov
na	na	k7c6	na
Sovadinově	Sovadinův	k2eAgFnSc6d1	Sovadinova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
zrušeného	zrušený	k2eAgInSc2d1	zrušený
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
budova	budova	k1gFnSc1	budova
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
navržena	navržen	k2eAgFnSc1d1	navržena
architektem	architekt	k1gMnSc7	architekt
Ing.	ing.	kA	ing.
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Rösslerem	Rössler	k1gMnSc7	Rössler
ve	v	k7c6	v
funkcionalistickém	funkcionalistický	k2eAgInSc6d1	funkcionalistický
slohu	sloh	k1gInSc6	sloh
Pozdně	pozdně	k6eAd1	pozdně
romantická	romantický	k2eAgFnSc1d1	romantická
tvorba	tvorba	k1gFnSc1	tvorba
lichtenštejnské	lichtenštejnský	k2eAgFnSc2d1	lichtenštejnská
stavební	stavební	k2eAgFnSc2d1	stavební
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
,	,	kIx,	,
typickým	typický	k2eAgInSc7d1	typický
rysem	rys	k1gInSc7	rys
režné	režný	k2eAgNnSc1d1	režné
cihlové	cihlový	k2eAgNnSc1d1	cihlové
zdivo	zdivo	k1gNnSc1	zdivo
a	a	k8xC	a
glazovaná	glazovaný	k2eAgFnSc1d1	glazovaná
keramika	keramika	k1gFnSc1	keramika
z	z	k7c2	z
poštorenské	poštorenský	k2eAgFnSc2d1	poštorenská
knížecí	knížecí	k2eAgFnSc2d1	knížecí
cihelny	cihelna	k1gFnSc2	cihelna
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
později	pozdě	k6eAd2	pozdě
Poštorenské	Poštorenský	k2eAgInPc1d1	Poštorenský
keramické	keramický	k2eAgInPc1d1	keramický
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
založeno	založit	k5eAaPmNgNnS	založit
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
soubor	soubor	k1gInSc1	soubor
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
centru	centr	k1gInSc6	centr
Poštorné	Poštorný	k2eAgNnSc1d1	Poštorné
-	-	kIx~	-
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgNnSc1d1	zdravotní
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
fara	fara	k1gFnSc1	fara
(	(	kIx(	(
<g/>
se	s	k7c7	s
secesními	secesní	k2eAgInPc7d1	secesní
motivy	motiv	k1gInPc7	motiv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
zejména	zejména	k9	zejména
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
Navštívení	navštívení	k1gNnSc2	navštívení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
-	-	kIx~	-
dominantní	dominantní	k2eAgFnSc1d1	dominantní
stavba	stavba	k1gFnSc1	stavba
centrálního	centrální	k2eAgInSc2d1	centrální
půdorysu	půdorys	k1gInSc2	půdorys
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
prstencem	prstenec	k1gInSc7	prstenec
bočních	boční	k2eAgFnPc2d1	boční
kaplí	kaple	k1gFnPc2	kaple
a	a	k8xC	a
polygonálním	polygonální	k2eAgNnSc7d1	polygonální
kněžištěm	kněžiště	k1gNnSc7	kněžiště
<g/>
,	,	kIx,	,
kopulovitá	kopulovitý	k2eAgFnSc1d1	kopulovitá
klenba	klenba	k1gFnSc1	klenba
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
lucernou	lucerna	k1gFnSc7	lucerna
s	s	k7c7	s
ochozem	ochoz	k1gInSc7	ochoz
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc4	dílo
Karla	Karel	k1gMnSc2	Karel
Weinbrennera	Weinbrenner	k1gMnSc2	Weinbrenner
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1898	[number]	k4	1898
budova	budova	k1gFnSc1	budova
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
obřadní	obřadní	k2eAgFnSc1d1	obřadní
síň	síň	k1gFnSc1	síň
židovského	židovský	k2eAgInSc2d1	židovský
hřbitova	hřbitov	k1gInSc2	hřbitov
s	s	k7c7	s
domkem	domek	k1gInSc7	domek
hrobníka	hrobník	k1gMnSc2	hrobník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
;	;	kIx,	;
hřbitov	hřbitov	k1gInSc1	hřbitov
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
staršího	starý	k2eAgInSc2d2	starší
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
<g />
.	.	kIx.	.
</s>
<s>
dochované	dochovaný	k2eAgInPc1d1	dochovaný
náhrobky	náhrobek	k1gInPc1	náhrobek
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
dominantní	dominantní	k2eAgFnSc1d1	dominantní
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
hrobka	hrobka	k1gFnSc1	hrobka
Kuffnerů	Kuffner	k1gMnPc2	Kuffner
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
;	;	kIx,	;
plundrování	plundrování	k1gNnSc2	plundrování
hřbitova	hřbitov	k1gInSc2	hřbitov
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
přečkala	přečkat	k5eAaPmAgFnS	přečkat
přibližně	přibližně	k6eAd1	přibližně
pětina	pětina	k1gFnSc1	pětina
původních	původní	k2eAgInPc2d1	původní
náhrobků	náhrobek	k1gInPc2	náhrobek
malá	malý	k2eAgFnSc1d1	malá
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Rocha	Rocha	k1gFnSc1	Rocha
(	(	kIx(	(
<g/>
před	před	k7c7	před
gymnáziem	gymnázium	k1gNnSc7	gymnázium
<g/>
)	)	kIx)	)
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
na	na	k7c6	na
čtvercovém	čtvercový	k2eAgInSc6d1	čtvercový
půdorysu	půdorys	k1gInSc6	půdorys
<g/>
,	,	kIx,	,
postavena	postaven	k2eAgMnSc4d1	postaven
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
epidemie	epidemie	k1gFnSc2	epidemie
cholery	cholera	k1gFnSc2	cholera
Další	další	k2eAgFnSc2d1	další
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
:	:	kIx,	:
významné	významný	k2eAgNnSc1d1	významné
staroslovanské	staroslovanský	k2eAgNnSc1d1	staroslovanské
hradiště	hradiště	k1gNnSc1	hradiště
Pohansko	Pohansko	k1gNnSc1	Pohansko
cca	cca	kA	cca
2	[number]	k4	2
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
splav	splav	k1gInSc4	splav
na	na	k7c4	na
Dyji	Dyje	k1gFnSc4	Dyje
s	s	k7c7	s
rybochodem	rybochod	k1gInSc7	rybochod
a	a	k8xC	a
podzemní	podzemní	k2eAgFnSc7d1	podzemní
hydroelektrárnou	hydroelektrárna	k1gFnSc7	hydroelektrárna
rekonstruované	rekonstruovaný	k2eAgFnSc2d1	rekonstruovaná
budovy	budova	k1gFnSc2	budova
cukrovaru	cukrovar	k1gInSc2	cukrovar
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
využívané	využívaný	k2eAgFnPc1d1	využívaná
firmou	firma	k1gFnSc7	firma
Racio	Racio	k6eAd1	Racio
ocelový	ocelový	k2eAgInSc1d1	ocelový
most	most	k1gInSc1	most
u	u	k7c2	u
cukrovaru	cukrovar	k1gInSc2	cukrovar
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
železniční	železniční	k2eAgFnSc4d1	železniční
vlečku	vlečka	k1gFnSc4	vlečka
cukrovaru	cukrovar	k1gInSc2	cukrovar
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
-	-	kIx~	-
jediný	jediný	k2eAgInSc1d1	jediný
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
most	most	k1gInSc1	most
v	v	k7c6	v
Břeclavi	Břeclav	k1gFnSc6	Břeclav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vyroben	vyroben	k2eAgMnSc1d1	vyroben
ve	v	k7c6	v
Vítkovicích	Vítkovice	k1gInPc6	Vítkovice
<g/>
.	.	kIx.	.
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
věž	věž	k1gFnSc1	věž
na	na	k7c6	na
Sovadinově	Sovadinův	k2eAgFnSc6d1	Sovadinova
ulici	ulice	k1gFnSc6	ulice
budova	budova	k1gFnSc1	budova
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Žluté	žlutý	k2eAgFnPc1d1	žlutá
školy	škola	k1gFnPc1	škola
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ZŠ	ZŠ	kA	ZŠ
Dukelská	dukelský	k2eAgFnSc1d1	Dukelská
<g/>
)	)	kIx)	)
u	u	k7c2	u
křižovatky	křižovatka	k1gFnSc2	křižovatka
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Sovadinovy	Sovadinův	k2eAgFnSc2d1	Sovadinova
a	a	k8xC	a
J.	J.	kA	J.
Palacha	Palacha	k1gFnSc1	Palacha
kasárna	kasárna	k1gNnPc4	kasárna
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Břeclavi	Břeclav	k1gFnSc6	Břeclav
-	-	kIx~	-
nová	nový	k2eAgNnPc4d1	nové
kasárna	kasárna	k1gNnPc4	kasárna
postavená	postavený	k2eAgFnSc1d1	postavená
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgInSc2d1	původní
barákového	barákový	k2eAgInSc2d1	barákový
tábora	tábor	k1gInSc2	tábor
mezi	mezi	k7c7	mezi
Břeclaví	Břeclav	k1gFnSc7	Břeclav
a	a	k8xC	a
St.	st.	kA	st.
Břeclaví	Břeclav	k1gFnSc7	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Kasárna	kasárna	k1gNnPc1	kasárna
byla	být	k5eAaImAgNnP	být
pojmenována	pojmenovat	k5eAaPmNgNnP	pojmenovat
po	po	k7c6	po
prezidentu	prezident	k1gMnSc3	prezident
T.G.	T.G.	k1gMnSc1	T.G.
Masarykovi	Masaryk	k1gMnSc3	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgNnP	být
kasárna	kasárna	k1gNnPc1	kasárna
pojmenována	pojmenovat	k5eAaPmNgNnP	pojmenovat
Graf	graf	k1gInSc1	graf
Moltke	Moltkus	k1gInSc5	Moltkus
Kaserne	Kasern	k1gInSc5	Kasern
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
přistavěno	přistavěn	k2eAgNnSc1d1	přistavěno
nejvrchnější	vrchní	k2eAgNnSc1d3	nejvrchnější
patro	patro	k1gNnSc1	patro
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
kasárna	kasárna	k1gNnPc4	kasárna
jako	jako	k8xC	jako
bytový	bytový	k2eAgInSc4d1	bytový
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
85	[number]	k4	85
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
žlutá	žlutý	k2eAgFnSc1d1	žlutá
věž	věž	k1gFnSc1	věž
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
podniku	podnik	k1gInSc2	podnik
OTIS	OTIS	kA	OTIS
sloužící	sloužící	k1gFnSc2	sloužící
pro	pro	k7c4	pro
testování	testování	k1gNnSc4	testování
výtahů	výtah	k1gInPc2	výtah
<g/>
,	,	kIx,	,
dominanta	dominanta	k1gFnSc1	dominanta
panoramatu	panorama	k1gNnSc2	panorama
města	město	k1gNnSc2	město
komín	komín	k1gInSc4	komín
závodu	závod	k1gInSc2	závod
Fosfa	Fosf	k1gMnSc2	Fosf
viditelný	viditelný	k2eAgInSc1d1	viditelný
prakticky	prakticky	k6eAd1	prakticky
z	z	k7c2	z
kteréhokoliv	kterýkoliv	k3yIgNnSc2	kterýkoliv
místa	místo	k1gNnSc2	místo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Lednicko-valtický	lednickoaltický	k2eAgInSc1d1	lednicko-valtický
areál	areál	k1gInSc1	areál
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
drobných	drobný	k2eAgFnPc2d1	drobná
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
zapsaný	zapsaný	k2eAgInSc1d1	zapsaný
na	na	k7c6	na
Seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
Břeclavi	Břeclav	k1gFnSc2	Břeclav
je	být	k5eAaImIp3nS	být
rozložen	rozložit	k5eAaPmNgInS	rozložit
na	na	k7c6	na
několika	několik	k4yIc6	několik
ostrovech	ostrov	k1gInPc6	ostrov
vymezených	vymezený	k2eAgInPc2d1	vymezený
Dyjí	Dyje	k1gFnSc7	Dyje
<g/>
,	,	kIx,	,
Poštorenskou	Poštorenský	k2eAgFnSc7d1	Poštorenská
Dyjí	Dyje	k1gFnSc7	Dyje
a	a	k8xC	a
Mlýnským	mlýnský	k2eAgInSc7d1	mlýnský
náhonem	náhon	k1gInSc7	náhon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
několik	několik	k4yIc4	několik
jezírek	jezírko	k1gNnPc2	jezírko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
napájena	napájen	k2eAgNnPc1d1	napájeno
jen	jen	k6eAd1	jen
spodní	spodní	k2eAgFnSc7d1	spodní
<g/>
,	,	kIx,	,
dešťovou	dešťový	k2eAgFnSc7d1	dešťová
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
povodňovou	povodňový	k2eAgFnSc7d1	povodňová
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
několik	několik	k4yIc1	několik
umělých	umělý	k2eAgFnPc2d1	umělá
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgInSc1d3	veliký
je	být	k5eAaImIp3nS	být
rybník	rybník	k1gInSc1	rybník
Včelínek	včelínek	k1gInSc1	včelínek
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Velen	velen	k2eAgMnSc1d1	velen
ze	z	k7c2	z
Žerotína	Žerotína	k1gFnSc1	Žerotína
(	(	kIx(	(
<g/>
1579	[number]	k4	1579
<g/>
-	-	kIx~	-
<g/>
1638	[number]	k4	1638
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
hejtman	hejtman	k1gMnSc1	hejtman
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
protihabsburského	protihabsburský	k2eAgInSc2d1	protihabsburský
odboje	odboj	k1gInSc2	odboj
Wilhelm	Wilhelm	k1gInSc1	Wilhelm
Ellenbogen	Ellenbogen	k1gInSc1	Ellenbogen
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
sociálně	sociálně	k6eAd1	sociálně
demokratický	demokratický	k2eAgMnSc1d1	demokratický
politik	politik	k1gMnSc1	politik
František	František	k1gMnSc1	František
Schäfer	Schäfer	k1gMnSc1	Schäfer
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Gabriela	Gabriela	k1gFnSc1	Gabriela
Dubská	Dubská	k1gFnSc1	Dubská
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
knižní	knižní	k2eAgFnSc1d1	knižní
grafička	grafička	k1gFnSc1	grafička
<g/>
,	,	kIx,	,
ilustrátorka	ilustrátorka	k1gFnSc1	ilustrátorka
a	a	k8xC	a
malířka	malířka	k1gFnSc1	malířka
Jan	Jan	k1gMnSc1	Jan
Skácel	Skácel	k1gMnSc1	Skácel
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
prožil	prožít	k5eAaPmAgMnS	prožít
své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
zdejším	zdejší	k2eAgNnSc6d1	zdejší
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Edgar	Edgar	k1gMnSc1	Edgar
Dutka	Dutka	k1gMnSc1	Dutka
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Jožka	Jožka	k1gMnSc1	Jožka
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidový	lidový	k2eAgMnSc1d1	lidový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Břeclavi	Břeclav	k1gFnSc6	Břeclav
Andrychów	Andrychów	k1gFnSc2	Andrychów
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Brezová	Brezová	k1gFnSc1	Brezová
pod	pod	k7c4	pod
Bradlom	Bradlom	k1gInSc4	Bradlom
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc4	Slovensko
Nový	nový	k2eAgInSc1d1	nový
Bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
Šentjernej	Šentjernej	k1gFnSc1	Šentjernej
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Zwentendorf	Zwentendorf	k1gInSc1	Zwentendorf
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Donau	donau	k1gNnSc1	donau
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
