<s>
Metamfetamin	Metamfetamin	k1gInSc1	Metamfetamin
<g/>
,	,	kIx,	,
též	též	k9	též
metylamfetamin	metylamfetamin	k2eAgInSc4d1	metylamfetamin
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
odborný	odborný	k2eAgInSc4d1	odborný
kontext	kontext	k1gInSc4	kontext
nejčastěji	často	k6eAd3	často
<g/>
)	)	kIx)	)
pervitin	pervitin	k1gInSc1	pervitin
(	(	kIx(	(
<g/>
dřívější	dřívější	k2eAgInSc1d1	dřívější
obchodní	obchodní	k2eAgInSc1d1	obchodní
název	název	k1gInSc1	název
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MA	MA	kA	MA
<g/>
,	,	kIx,	,
desoxyefedrin	desoxyefedrin	k1gInSc1	desoxyefedrin
<g/>
,	,	kIx,	,
methedrin	methedrin	k1gInSc1	methedrin
je	být	k5eAaImIp3nS	být
syntetické	syntetický	k2eAgNnSc4d1	syntetické
stimulancium	stimulancium	k1gNnSc4	stimulancium
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
amfetaminů	amfetamin	k1gInPc2	amfetamin
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
jeho	jeho	k3xOp3gInSc1	jeho
potenciál	potenciál	k1gInSc1	potenciál
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
chorobné	chorobný	k2eAgFnSc2d1	chorobná
spavosti	spavost	k1gFnSc2	spavost
a	a	k8xC	a
astmatu	astma	k1gNnSc2	astma
<g/>
,	,	kIx,	,
masově	masově	k6eAd1	masově
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
amfetaminem	amfetamin	k1gInSc7	amfetamin
použit	použít	k5eAaPmNgInS	použít
jako	jako	k9	jako
povzbuzující	povzbuzující	k2eAgInSc1d1	povzbuzující
prostředek	prostředek	k1gInSc1	prostředek
během	během	k7c2	během
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
osazenstvem	osazenstvo	k1gNnSc7	osazenstvo
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
tanků	tank	k1gInPc2	tank
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
FDA	FDA	kA	FDA
povolen	povolit	k5eAaPmNgInS	povolit
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
syndromu	syndrom	k1gInSc2	syndrom
ADHD	ADHD	kA	ADHD
<g/>
,	,	kIx,	,
ke	k	k7c3	k
krátkodobému	krátkodobý	k2eAgNnSc3d1	krátkodobé
zvládání	zvládání	k1gNnSc3	zvládání
extrémní	extrémní	k2eAgFnSc2d1	extrémní
exogenní	exogenní	k2eAgFnSc2d1	exogenní
obezity	obezita	k1gFnSc2	obezita
a	a	k8xC	a
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
narkolepsie	narkolepsie	k1gFnSc2	narkolepsie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
MA	MA	kA	MA
hlavně	hlavně	k6eAd1	hlavně
zneužíván	zneužíván	k2eAgMnSc1d1	zneužíván
jako	jako	k8xS	jako
snadno	snadno	k6eAd1	snadno
připravitelná	připravitelný	k2eAgFnSc1d1	připravitelná
pouliční	pouliční	k2eAgFnSc1d1	pouliční
droga	droga	k1gFnSc1	droga
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
centrální	centrální	k2eAgFnSc4d1	centrální
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Metamfetamin	Metamfetamin	k1gInSc1	Metamfetamin
velmi	velmi	k6eAd1	velmi
účinně	účinně	k6eAd1	účinně
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
únavu	únava	k1gFnSc4	únava
a	a	k8xC	a
potřebu	potřeba	k1gFnSc4	potřeba
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
psychomotorické	psychomotorický	k2eAgNnSc1d1	psychomotorické
tempo	tempo	k1gNnSc1	tempo
<g/>
,	,	kIx,	,
očekávaným	očekávaný	k2eAgInSc7d1	očekávaný
účinkem	účinek	k1gInSc7	účinek
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
extrémní	extrémní	k2eAgFnSc1d1	extrémní
euforie	euforie	k1gFnSc1	euforie
a	a	k8xC	a
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
sebedůvěra	sebedůvěra	k1gFnSc1	sebedůvěra
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
uvolňováním	uvolňování	k1gNnSc7	uvolňování
dopaminu	dopamin	k1gInSc2	dopamin
<g/>
,	,	kIx,	,
noradrenalinu	noradrenalin	k1gInSc2	noradrenalin
a	a	k8xC	a
serotoninu	serotonin	k1gInSc2	serotonin
do	do	k7c2	do
synaptických	synaptický	k2eAgFnPc2d1	synaptická
štěrbin	štěrbina	k1gFnPc2	štěrbina
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
nadužívání	nadužívání	k1gNnSc1	nadužívání
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
až	až	k8xS	až
zániku	zánik	k1gInSc3	zánik
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
působením	působení	k1gNnSc7	působení
oxidačního	oxidační	k2eAgInSc2d1	oxidační
stresu	stres	k1gInSc2	stres
(	(	kIx(	(
<g/>
neurotoxicita	neurotoxicita	k1gFnSc1	neurotoxicita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zneužíváním	zneužívání	k1gNnSc7	zneužívání
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgInSc1d1	spojen
i	i	k8xC	i
negativní	negativní	k2eAgInSc1d1	negativní
dopad	dopad	k1gInSc1	dopad
na	na	k7c4	na
kardiovaskulární	kardiovaskulární	k2eAgInSc4d1	kardiovaskulární
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Častějším	častý	k2eAgNnSc7d2	častější
užíváním	užívání	k1gNnSc7	užívání
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
tolerance	tolerance	k1gFnSc2	tolerance
a	a	k8xC	a
riziku	riziko	k1gNnSc3	riziko
psychické	psychický	k2eAgFnSc2d1	psychická
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Návyk	návyk	k1gInSc1	návyk
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
provázen	provázet	k5eAaImNgMnS	provázet
depresemi	deprese	k1gFnPc7	deprese
<g/>
,	,	kIx,	,
citovou	citový	k2eAgFnSc7d1	citová
labilitou	labilita	k1gFnSc7	labilita
<g/>
,	,	kIx,	,
největším	veliký	k2eAgNnSc7d3	veliký
rizikem	riziko	k1gNnSc7	riziko
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
toxická	toxický	k2eAgFnSc1d1	toxická
psychóza	psychóza	k1gFnSc1	psychóza
připomínající	připomínající	k2eAgFnSc4d1	připomínající
paranoidní	paranoidní	k2eAgFnSc4d1	paranoidní
schizofrenii	schizofrenie	k1gFnSc4	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těžce	těžce	k6eAd1	těžce
závislých	závislý	k2eAgFnPc2d1	závislá
osob	osoba	k1gFnPc2	osoba
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
popsána	popsán	k2eAgFnSc1d1	popsána
dilatační	dilatační	k2eAgFnSc1d1	dilatační
kardiomyopatie	kardiomyopatie	k1gFnSc1	kardiomyopatie
<g/>
,	,	kIx,	,
parkinsonismus	parkinsonismus	k1gInSc1	parkinsonismus
i	i	k8xC	i
trvalé	trvalý	k2eAgNnSc1d1	trvalé
poškození	poškození	k1gNnSc1	poškození
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
častěji	často	k6eAd2	často
prodáván	prodávat	k5eAaImNgInS	prodávat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
bílého	bílý	k2eAgNnSc2d1	bílé
nebo	nebo	k8xC	nebo
různě	různě	k6eAd1	různě
zbarveného	zbarvený	k2eAgInSc2d1	zbarvený
prášku	prášek	k1gInSc2	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
vyráběn	vyráběn	k2eAgMnSc1d1	vyráběn
jako	jako	k8xS	jako
léčivo	léčivo	k1gNnSc1	léčivo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Desoxyn	Desoxyna	k1gFnPc2	Desoxyna
<g/>
,	,	kIx,	,
indikován	indikován	k2eAgInSc1d1	indikován
zejména	zejména	k9	zejména
k	k	k7c3	k
terapii	terapie	k1gFnSc3	terapie
poruch	porucha	k1gFnPc2	porucha
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Metamfetamin	Metamfetamin	k1gInSc1	Metamfetamin
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nezákonně	zákonně	k6eNd1	zákonně
syntetizován	syntetizován	k2eAgInSc1d1	syntetizován
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
pseudoefedrinu	pseudoefedrin	k1gInSc2	pseudoefedrin
extrahovaného	extrahovaný	k2eAgInSc2d1	extrahovaný
z	z	k7c2	z
léčiv	léčivo	k1gNnPc2	léčivo
proti	proti	k7c3	proti
chřipce	chřipka	k1gFnSc3	chřipka
a	a	k8xC	a
rýmě	rýma	k1gFnSc3	rýma
(	(	kIx(	(
<g/>
Nurofen	Nurofen	k2eAgInSc1d1	Nurofen
stopgrip	stopgrip	k1gInSc1	stopgrip
<g/>
,	,	kIx,	,
Modafen	Modafen	k1gInSc1	Modafen
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
i	i	k9	i
aspirin	aspirin	k1gInSc1	aspirin
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
v	v	k7c6	v
amatérských	amatérský	k2eAgFnPc6d1	amatérská
varnách	varna	k1gFnPc6	varna
<g/>
.	.	kIx.	.
</s>
<s>
Nezákonná	zákonný	k2eNgFnSc1d1	nezákonná
syntéza	syntéza	k1gFnSc1	syntéza
je	být	k5eAaImIp3nS	být
levná	levný	k2eAgFnSc1d1	levná
<g/>
,	,	kIx,	,
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
velmi	velmi	k6eAd1	velmi
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
k	k	k7c3	k
reakci	reakce	k1gFnSc3	reakce
je	být	k5eAaImIp3nS	být
používán	používán	k2eAgInSc1d1	používán
velmi	velmi	k6eAd1	velmi
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
červený	červený	k2eAgInSc1d1	červený
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
žíravá	žíravý	k2eAgFnSc1d1	žíravá
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
i	i	k8xC	i
jedovatá	jedovatý	k2eAgNnPc1d1	jedovaté
ředidla	ředidlo	k1gNnPc1	ředidlo
(	(	kIx(	(
<g/>
toluen	toluen	k1gInSc1	toluen
či	či	k8xC	či
aceton	aceton	k1gInSc1	aceton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
pak	pak	k6eAd1	pak
metamfetamin	metamfetamin	k2eAgInSc1d1	metamfetamin
proniká	pronikat	k5eAaImIp3nS	pronikat
i	i	k9	i
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
probíhal	probíhat	k5eAaImAgMnS	probíhat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
výzkum	výzkum	k1gInSc1	výzkum
tradiční	tradiční	k2eAgFnSc2d1	tradiční
čínské	čínský	k2eAgFnSc2d1	čínská
drogy	droga	k1gFnSc2	droga
Ma	Ma	k1gMnSc1	Ma
Huang	Huang	k1gMnSc1	Huang
(	(	kIx(	(
<g/>
ephedra	ephedra	k1gFnSc1	ephedra
sinica	sinica	k1gFnSc1	sinica
<g/>
,	,	kIx,	,
chvojník	chvojník	k1gInSc1	chvojník
čínský	čínský	k2eAgInSc1d1	čínský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
byl	být	k5eAaImAgMnS	být
izolován	izolován	k2eAgInSc4d1	izolován
efedrin	efedrin	k1gInSc4	efedrin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
chemikovi	chemik	k1gMnSc3	chemik
Nagai	Naga	k1gMnSc3	Naga
Nagayoshimu	Nagayoshim	k1gMnSc3	Nagayoshim
poprvé	poprvé	k6eAd1	poprvé
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
látku	látka	k1gFnSc4	látka
metamfetamin	metamfetamin	k2eAgInSc1d1	metamfetamin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
farmakolog	farmakolog	k1gMnSc1	farmakolog
Akira	Akira	k1gMnSc1	Akira
Nogata	Nogat	k1gMnSc2	Nogat
redukoval	redukovat	k5eAaBmAgInS	redukovat
efedrin	efedrin	k1gInSc4	efedrin
kyselinou	kyselina	k1gFnSc7	kyselina
jodovodíkovou	jodovodíkový	k2eAgFnSc7d1	jodovodíková
a	a	k8xC	a
fosforem	fosfor	k1gInSc7	fosfor
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dal	dát	k5eAaPmAgMnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
krystalickému	krystalický	k2eAgInSc3d1	krystalický
metamfetamin	metamfetamin	k2eAgMnSc1d1	metamfetamin
hydrochloridu	hydrochlorid	k1gInSc2	hydrochlorid
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
-metamfetamin	etamfetamin	k1gInSc1	-metamfetamin
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
prvně	prvně	k?	prvně
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
až	až	k6eAd1	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
;	;	kIx,	;
berlínská	berlínský	k2eAgFnSc1d1	Berlínská
firma	firma	k1gFnSc1	firma
Temmler	Temmler	k1gInSc4	Temmler
Werke	Werk	k1gFnSc2	Werk
patentovala	patentovat	k5eAaBmAgFnS	patentovat
originální	originální	k2eAgFnSc4d1	originální
metodu	metoda	k1gFnSc4	metoda
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
-metamfetaminu	etamfetamin	k1gInSc6	-metamfetamin
hydrogenací	hydrogenace	k1gFnSc7	hydrogenace
hydrochloridu	hydrochlorid	k1gInSc2	hydrochlorid
1	[number]	k4	1
<g/>
-chloroefedrinu	hloroefedrin	k1gInSc2	-chloroefedrin
v	v	k7c6	v
alkoholu	alkohol	k1gInSc6	alkohol
na	na	k7c6	na
platinovém	platinový	k2eAgInSc6d1	platinový
katalyzátoru	katalyzátor	k1gInSc6	katalyzátor
za	za	k7c4	za
mírně	mírně	k6eAd1	mírně
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
a	a	k8xC	a
na	na	k7c4	na
německý	německý	k2eAgInSc4d1	německý
trh	trh	k1gInSc4	trh
uvedla	uvést	k5eAaPmAgFnS	uvést
přípravek	přípravek	k1gInSc4	přípravek
Pervitin	pervitin	k1gInSc1	pervitin
Tabletten	Tabletten	k2eAgInSc1d1	Tabletten
(	(	kIx(	(
<g/>
tablety	tableta	k1gFnPc1	tableta
po	po	k7c4	po
3	[number]	k4	3
mg	mg	kA	mg
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
-metamfetaminium	etamfetaminium	k1gNnSc1	-metamfetaminium
chloridu	chlorid	k1gInSc2	chlorid
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
přibyla	přibýt	k5eAaPmAgNnP	přibýt
injekční	injekční	k2eAgNnPc1d1	injekční
galenika	galenikum	k1gNnPc1	galenikum
a	a	k8xC	a
ampule	ampule	k1gFnPc1	ampule
po	po	k7c6	po
2	[number]	k4	2
ml	ml	kA	ml
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
mg	mg	kA	mg
účinné	účinný	k2eAgFnSc2d1	účinná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Preparát	preparát	k1gInSc1	preparát
Temmlerových	Temmlerův	k2eAgInPc2d1	Temmlerův
závodů	závod	k1gInPc2	závod
Pervitin	pervitin	k1gInSc1	pervitin
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
komerčním	komerční	k2eAgInSc7d1	komerční
preparátem	preparát	k1gInSc7	preparát
dextrometamfetaminu	dextrometamfetamin	k1gInSc2	dextrometamfetamin
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Podávání	podávání	k1gNnSc1	podávání
pervitinových	pervitinový	k2eAgFnPc2d1	pervitinová
tablet	tableta	k1gFnPc2	tableta
vojákům	voják	k1gMnPc3	voják
praktikovala	praktikovat	k5eAaImAgFnS	praktikovat
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
i	i	k8xC	i
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
"	"	kIx"	"
<g/>
zázračná	zázračný	k2eAgFnSc1d1	zázračná
pilulka	pilulka	k1gFnSc1	pilulka
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
údajně	údajně	k6eAd1	údajně
odpovědná	odpovědný	k2eAgNnPc1d1	odpovědné
za	za	k7c4	za
rychlý	rychlý	k2eAgInSc4d1	rychlý
postup	postup	k1gInSc4	postup
a	a	k8xC	a
"	"	kIx"	"
<g/>
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
"	"	kIx"	"
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
byl	být	k5eAaImAgInS	být
MA	MA	kA	MA
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Philopon	Philopon	k1gMnSc1	Philopon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
podáván	podávat	k5eAaImNgMnS	podávat
i	i	k9	i
továrním	tovární	k2eAgMnPc3d1	tovární
dělníkům	dělník	k1gMnPc3	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
rekreační	rekreační	k2eAgNnSc1d1	rekreační
užívání	užívání	k1gNnSc1	užívání
Philoponu	Philopon	k1gInSc2	Philopon
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
Japonsku	Japonsko	k1gNnSc6	Japonsko
velice	velice	k6eAd1	velice
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
začal	začít	k5eAaPmAgInS	začít
inhalovat	inhalovat	k5eAaImF	inhalovat
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obliba	obliba	k1gFnSc1	obliba
metamfetaminu	metamfetamin	k1gInSc2	metamfetamin
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
až	až	k9	až
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
pervitin	pervitin	k1gInSc1	pervitin
nelegálně	legálně	k6eNd1	legálně
v	v	k7c6	v
improvizovaných	improvizovaný	k2eAgFnPc6d1	improvizovaná
podmínkách	podmínka	k1gFnPc6	podmínka
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Užíván	užívat	k5eAaImNgInS	užívat
byl	být	k5eAaImAgInS	být
hlavně	hlavně	k9	hlavně
nitrožilně	nitrožilně	k6eAd1	nitrožilně
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
aplikace	aplikace	k1gFnSc1	aplikace
šňupáním	šňupání	k1gNnSc7	šňupání
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
bílého	bílé	k1gNnSc2	bílé
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
krystalického	krystalický	k2eAgInSc2d1	krystalický
prášku	prášek	k1gInSc2	prášek
<g/>
,	,	kIx,	,
či	či	k8xC	či
větších	veliký	k2eAgInPc2d2	veliký
<g/>
,	,	kIx,	,
světlých	světlý	k2eAgInPc2d1	světlý
krystalů	krystal	k1gInPc2	krystal
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
nahořklé	nahořklý	k2eAgFnSc2d1	nahořklá
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
metamfetamin	metamfetamin	k2eAgInSc1d1	metamfetamin
hydrochlorid	hydrochlorid	k1gInSc1	hydrochlorid
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
v	v	k7c6	v
maximálním	maximální	k2eAgInSc6d1	maximální
možném	možný	k2eAgInSc6d1	možný
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
i	i	k8xC	i
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
účinkem	účinek	k1gInSc7	účinek
několikrát	několikrát	k6eAd1	několikrát
předstihuje	předstihovat	k5eAaImIp3nS	předstihovat
D-amfetamin	Dmfetamin	k1gInSc1	D-amfetamin
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
lipofilní	lipofilní	k2eAgNnSc1d1	lipofilní
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
neurotoxicitu	neurotoxicita	k1gFnSc4	neurotoxicita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
D-metamfetamin	Detamfetamin	k2eAgInSc4d1	D-metamfetamin
hydrochlorid	hydrochlorid	k1gInSc4	hydrochlorid
<g/>
,	,	kIx,	,
či	či	k8xC	či
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
-metamfetaminiumchlorid	etamfetaminiumchlorid	k1gInSc1	-metamfetaminiumchlorid
často	často	k6eAd1	často
produkován	produkovat	k5eAaImNgInS	produkovat
právě	právě	k9	právě
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tříštivých	tříštivý	k2eAgInPc2d1	tříštivý
lesklých	lesklý	k2eAgInPc2d1	lesklý
krystalků	krystalek	k1gInPc2	krystalek
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
ice	ice	k?	ice
(	(	kIx(	(
<g/>
led	led	k1gInSc1	led
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc6d1	vysoká
kvalitě	kvalita	k1gFnSc6	kvalita
(	(	kIx(	(
<g/>
až	až	k9	až
90	[number]	k4	90
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
má	mít	k5eAaImIp3nS	mít
nízký	nízký	k2eAgInSc4d1	nízký
bod	bod	k1gInSc4	bod
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
ice	ice	k?	ice
aplikován	aplikovat	k5eAaBmNgInS	aplikovat
inhalováním	inhalování	k1gNnSc7	inhalování
výparů	výpar	k1gInPc2	výpar
zahřátých	zahřátý	k2eAgInPc2d1	zahřátý
kousků	kousek	k1gInPc2	kousek
metamfetaminu	metamfetamin	k1gInSc2	metamfetamin
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
D-metamfetamin	Detamfetamin	k1gInSc4	D-metamfetamin
HCl	HCl	k1gFnSc2	HCl
<g/>
.	.	kIx.	.
</s>
<s>
Volné	volný	k2eAgFnPc1d1	volná
báze	báze	k1gFnPc1	báze
amfetaminů	amfetamin	k1gInPc2	amfetamin
mívají	mívat	k5eAaImIp3nP	mívat
zpravidla	zpravidla	k6eAd1	zpravidla
podobu	podoba	k1gFnSc4	podoba
těkavé	těkavý	k2eAgFnSc2d1	těkavá
olejovité	olejovitý	k2eAgFnSc2d1	olejovitá
kapaliny	kapalina	k1gFnSc2	kapalina
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Levotočivý	levotočivý	k2eAgInSc1d1	levotočivý
izomer	izomer	k1gInSc1	izomer
L-metamfetamin	Letamfetamin	k2eAgInSc1d1	L-metamfetamin
disponuje	disponovat	k5eAaBmIp3nS	disponovat
hlavně	hlavně	k9	hlavně
analeptickým	analeptický	k2eAgInSc7d1	analeptický
účinkem	účinek	k1gInSc7	účinek
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nízký	nízký	k2eAgInSc1d1	nízký
návykový	návykový	k2eAgInSc1d1	návykový
potenciál	potenciál	k1gInSc1	potenciál
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
často	často	k6eAd1	často
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
inhalátorech	inhalátor	k1gInPc6	inhalátor
proti	proti	k7c3	proti
rýmě	rýma	k1gFnSc3	rýma
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
určitou	určitý	k2eAgFnSc4d1	určitá
schopnost	schopnost	k1gFnSc4	schopnost
potlačovat	potlačovat	k5eAaImF	potlačovat
únavu	únava	k1gFnSc4	únava
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc4	pocit
hladu	hlad	k1gInSc2	hlad
a	a	k8xC	a
potřebu	potřeba	k1gFnSc4	potřeba
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
bývá	bývat	k5eAaImIp3nS	bývat
zneužíván	zneužíván	k2eAgInSc1d1	zneužíván
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
však	však	k9	však
bez	bez	k7c2	bez
pocitů	pocit	k1gInPc2	pocit
bažení	bažení	k1gNnSc2	bažení
po	po	k7c6	po
opakované	opakovaný	k2eAgFnSc6d1	opakovaná
aplikaci	aplikace	k1gFnSc6	aplikace
a	a	k8xC	a
následném	následný	k2eAgInSc6d1	následný
vzniku	vznik	k1gInSc6	vznik
návyku	návyk	k1gInSc2	návyk
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
přítomen	přítomen	k2eAgInSc1d1	přítomen
jako	jako	k8xC	jako
nechtěný	chtěný	k2eNgInSc1d1	nechtěný
produkt	produkt	k1gInSc1	produkt
k	k	k7c3	k
D-metamfetaminu	Detamfetamin	k1gInSc3	D-metamfetamin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
pervitin	pervitin	k1gInSc1	pervitin
(	(	kIx(	(
<g/>
získaný	získaný	k2eAgInSc1d1	získaný
redukcí	redukce	k1gFnPc2	redukce
pseudoefedrinu	pseudoefedrin	k1gInSc2	pseudoefedrin
jodovodíkem	jodovodík	k1gInSc7	jodovodík
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
fosforu	fosfor	k1gInSc2	fosfor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
také	také	k9	také
prakticky	prakticky	k6eAd1	prakticky
čistě	čistě	k6eAd1	čistě
pravotočivým	pravotočivý	k2eAgInSc7d1	pravotočivý
izomerem	izomer	k1gInSc7	izomer
(	(	kIx(	(
<g/>
s	s	k7c7	s
kolísajícími	kolísající	k2eAgFnPc7d1	kolísající
příměsemi	příměse	k1gFnPc7	příměse
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
produktů	produkt	k1gInPc2	produkt
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
iodoefedrin	iodoefedrin	k1gInSc1	iodoefedrin
a	a	k8xC	a
iodobenzen	iodobenzen	k2eAgInSc1d1	iodobenzen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čistota	čistota	k1gFnSc1	čistota
pouličního	pouliční	k2eAgInSc2d1	pouliční
metamfetaminu	metamfetamin	k1gInSc2	metamfetamin
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
45	[number]	k4	45
-	-	kIx~	-
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
nažloutlý	nažloutlý	k2eAgInSc1d1	nažloutlý
<g/>
,	,	kIx,	,
narůžovělý	narůžovělý	k2eAgInSc1d1	narůžovělý
<g/>
,	,	kIx,	,
namodralý	namodralý	k2eAgInSc1d1	namodralý
i	i	k8xC	i
jiný	jiný	k2eAgInSc1d1	jiný
odstín	odstín	k1gInSc1	odstín
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dán	k2eAgNnSc1d1	dáno
výskytem	výskyt	k1gInSc7	výskyt
zbytkových	zbytkový	k2eAgInPc2d1	zbytkový
produktů	produkt	k1gInPc2	produkt
typu	typ	k1gInSc2	typ
fosforu	fosfor	k1gInSc2	fosfor
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Droga	droga	k1gFnSc1	droga
bývá	bývat	k5eAaImIp3nS	bývat
navíc	navíc	k6eAd1	navíc
často	často	k6eAd1	často
ředěna	ředit	k5eAaImNgFnS	ředit
různorodými	různorodý	k2eAgFnPc7d1	různorodá
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kofeinem	kofein	k1gInSc7	kofein
<g/>
,	,	kIx,	,
mléčným	mléčný	k2eAgInSc7d1	mléčný
cukrem	cukr	k1gInSc7	cukr
<g/>
,	,	kIx,	,
pudrem	pudr	k1gInSc7	pudr
<g/>
,	,	kIx,	,
v	v	k7c6	v
horších	zlý	k2eAgInPc6d2	horší
případech	případ	k1gInPc6	případ
i	i	k8xC	i
paracetamolem	paracetamol	k1gInSc7	paracetamol
(	(	kIx(	(
<g/>
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
dávkách	dávka	k1gFnPc6	dávka
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
játra	játra	k1gNnPc1	játra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
omítkou	omítka	k1gFnSc7	omítka
<g/>
,	,	kIx,	,
hnojivy	hnojivo	k1gNnPc7	hnojivo
nebo	nebo	k8xC	nebo
pesticidy	pesticid	k1gInPc7	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
drogu	droga	k1gFnSc4	droga
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
slangových	slangový	k2eAgInPc2d1	slangový
výrazů	výraz	k1gInPc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
českým	český	k2eAgMnPc3d1	český
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
vareso	varesa	k1gFnSc5	varesa
<g/>
,	,	kIx,	,
párno	párno	k?	párno
<g/>
,	,	kIx,	,
perník	perník	k1gInSc1	perník
<g/>
,	,	kIx,	,
piko	piko	k1gNnSc1	piko
či	či	k8xC	či
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
pak	pak	k6eAd1	pak
crystal	crystat	k5eAaPmAgMnS	crystat
<g/>
,	,	kIx,	,
meth	meth	k1gInSc1	meth
<g/>
,	,	kIx,	,
tina	tina	k1gFnSc1	tina
<g/>
,	,	kIx,	,
glass	glass	k1gInSc1	glass
<g/>
,	,	kIx,	,
ice	ice	k?	ice
nebo	nebo	k8xC	nebo
crank	crank	k6eAd1	crank
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
droga	droga	k1gFnSc1	droga
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k9	hlavně
pod	pod	k7c7	pod
přízviskem	přízvisko	k1gNnSc7	přízvisko
crystal	crystat	k5eAaPmAgMnS	crystat
speed	speed	k1gMnSc1	speed
<g/>
,	,	kIx,	,
crystal	crystat	k5eAaPmAgMnS	crystat
nebo	nebo	k8xC	nebo
tschecho	tschecho	k6eAd1	tschecho
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
německého	německý	k2eAgMnSc2d1	německý
MA	MA	kA	MA
totiž	totiž	k9	totiž
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
metamfetamin	metamfetamin	k1gInSc1	metamfetamin
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
barevných	barevný	k2eAgFnPc2d1	barevná
tabletek	tabletka	k1gFnPc2	tabletka
zvaných	zvaný	k2eAgFnPc2d1	zvaná
Yaba	Yab	k2eAgNnPc4d1	Yab
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
dávka	dávka	k1gFnSc1	dávka
(	(	kIx(	(
<g/>
15	[number]	k4	15
až	až	k9	až
100	[number]	k4	100
mg	mg	kA	mg
<g/>
,	,	kIx,	,
závislí	závislý	k2eAgMnPc1d1	závislý
toxikomani	toxikoman	k1gMnPc1	toxikoman
užívají	užívat	k5eAaImIp3nP	užívat
i	i	k9	i
vyšších	vysoký	k2eAgFnPc2d2	vyšší
dávek	dávka	k1gFnPc2	dávka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
užita	užít	k5eAaPmNgFnS	užít
šňupáním	šňupání	k1gNnSc7	šňupání
<g/>
,	,	kIx,	,
nitrožilně	nitrožilně	k6eAd1	nitrožilně
<g/>
,	,	kIx,	,
kouřením	kouření	k1gNnSc7	kouření
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
perorálně	perorálně	k6eAd1	perorálně
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tablet	tableta	k1gFnPc2	tableta
(	(	kIx(	(
<g/>
oblíbeno	oblíbit	k5eAaPmNgNnS	oblíbit
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
pod	pod	k7c7	pod
přízviskem	přízvisko	k1gNnSc7	přízvisko
Yaba	Yabus	k1gMnSc2	Yabus
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
jako	jako	k8xS	jako
lék	lék	k1gInSc1	lék
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
lékařského	lékařský	k2eAgNnSc2d1	lékařské
využití	využití	k1gNnSc2	využití
se	se	k3xPyFc4	se
nepřekračuje	překračovat	k5eNaImIp3nS	překračovat
25	[number]	k4	25
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
až	až	k9	až
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
dávkou	dávka	k1gFnSc7	dávka
mnohem	mnohem	k6eAd1	mnohem
déle	dlouho	k6eAd2	dlouho
(	(	kIx(	(
<g/>
až	až	k9	až
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moči	moč	k1gFnSc6	moč
lze	lze	k6eAd1	lze
MA	MA	kA	MA
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
od	od	k7c2	od
požití	požití	k1gNnSc2	požití
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
užití	užití	k1gNnSc6	užití
nízkých	nízký	k2eAgFnPc2d1	nízká
dávek	dávka	k1gFnPc2	dávka
se	se	k3xPyFc4	se
dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
povznesená	povznesený	k2eAgFnSc1d1	povznesená
nálada	nálada	k1gFnSc1	nálada
<g/>
,	,	kIx,	,
čilost	čilost	k1gFnSc1	čilost
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
bdělost	bdělost	k1gFnSc1	bdělost
a	a	k8xC	a
ostražitost	ostražitost	k1gFnSc1	ostražitost
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
středními	střední	k2eAgFnPc7d1	střední
dávkami	dávka	k1gFnPc7	dávka
uživatel	uživatel	k1gMnSc1	uživatel
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
příval	příval	k1gInSc4	příval
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
vzrušení	vzrušení	k1gNnSc2	vzrušení
<g/>
,	,	kIx,	,
mizí	mizet	k5eAaImIp3nS	mizet
potřeba	potřeba	k1gFnSc1	potřeba
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
hovornost	hovornost	k1gFnSc1	hovornost
<g/>
,	,	kIx,	,
činorodost	činorodost	k1gFnSc1	činorodost
a	a	k8xC	a
družnost	družnost	k1gFnSc1	družnost
<g/>
.	.	kIx.	.
</s>
<s>
Prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
se	se	k3xPyFc4	se
schopnost	schopnost	k1gFnSc1	schopnost
empatie	empatie	k1gFnSc2	empatie
a	a	k8xC	a
soustředění	soustředění	k1gNnSc2	soustředění
<g/>
,	,	kIx,	,
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
se	se	k3xPyFc4	se
tok	tok	k1gInSc1	tok
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
zostřuje	zostřovat	k5eAaImIp3nS	zostřovat
se	se	k3xPyFc4	se
vnímání	vnímání	k1gNnSc1	vnímání
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
tónů	tón	k1gInPc2	tón
a	a	k8xC	a
pachů	pach	k1gInPc2	pach
<g/>
,	,	kIx,	,
výbavnost	výbavnost	k1gFnSc1	výbavnost
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
je	být	k5eAaImIp3nS	být
jasnější	jasný	k2eAgFnSc1d2	jasnější
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
přesnosti	přesnost	k1gFnSc2	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
užitím	užití	k1gNnSc7	užití
vyšších	vysoký	k2eAgFnPc2d2	vyšší
dávek	dávka	k1gFnPc2	dávka
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
euforie	euforie	k1gFnSc1	euforie
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
pohybu	pohyb	k1gInSc6	pohyb
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
úzkost	úzkost	k1gFnSc1	úzkost
<g/>
,	,	kIx,	,
labilita	labilita	k1gFnSc1	labilita
a	a	k8xC	a
hádavost	hádavost	k1gFnSc1	hádavost
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
přítomen	přítomen	k2eAgInSc1d1	přítomen
výrazný	výrazný	k2eAgInSc1d1	výrazný
neklid	neklid	k1gInSc1	neklid
<g/>
,	,	kIx,	,
logorea	logorea	k1gFnSc1	logorea
(	(	kIx(	(
<g/>
přehnaná	přehnaný	k2eAgFnSc1d1	přehnaná
povídavost	povídavost	k1gFnSc1	povídavost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
trysk	trysk	k1gInSc4	trysk
<g/>
,	,	kIx,	,
subjektivní	subjektivní	k2eAgInSc4d1	subjektivní
pocit	pocit	k1gInSc4	pocit
velké	velký	k2eAgFnSc2d1	velká
duševní	duševní	k2eAgFnSc2d1	duševní
a	a	k8xC	a
fyzické	fyzický	k2eAgFnSc2d1	fyzická
výkonnosti	výkonnost	k1gFnSc2	výkonnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
dávkách	dávka	k1gFnPc6	dávka
se	se	k3xPyFc4	se
však	však	k9	však
myšlení	myšlení	k1gNnSc1	myšlení
stává	stávat	k5eAaImIp3nS	stávat
stereotypním	stereotypní	k2eAgInSc7d1	stereotypní
a	a	k8xC	a
bizarním	bizarní	k2eAgInSc7d1	bizarní
<g/>
,	,	kIx,	,
nápadné	nápadný	k2eAgInPc4d1	nápadný
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
svalové	svalový	k2eAgFnPc1d1	svalová
fascikulace	fascikulace	k1gFnPc1	fascikulace
<g/>
,	,	kIx,	,
pohybové	pohybový	k2eAgFnPc1d1	pohybová
stereotypie	stereotypie	k1gFnPc1	stereotypie
a	a	k8xC	a
mimovolní	mimovolní	k2eAgInPc1d1	mimovolní
pohyby	pohyb	k1gInPc1	pohyb
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vykroucenost	vykroucenost	k1gFnSc4	vykroucenost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc4	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
či	či	k8xC	či
světloplachost	světloplachost	k1gFnSc1	světloplachost
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
dávkách	dávka	k1gFnPc6	dávka
iluze	iluze	k1gFnSc1	iluze
<g/>
,	,	kIx,	,
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc1	pocit
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
,	,	kIx,	,
agresivita	agresivita	k1gFnSc1	agresivita
<g/>
,	,	kIx,	,
u	u	k7c2	u
predisponovaných	predisponovaný	k2eAgFnPc2d1	predisponovaná
osob	osoba	k1gFnPc2	osoba
až	až	k9	až
přechodná	přechodný	k2eAgFnSc1d1	přechodná
psychóza	psychóza	k1gFnSc1	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nepřímým	přímý	k2eNgNnSc7d1	nepřímé
drážděním	dráždění	k1gNnSc7	dráždění
sympatiku	sympatik	k1gInSc2	sympatik
je	být	k5eAaImIp3nS	být
spjato	spjat	k2eAgNnSc4d1	spjato
rozšíření	rozšíření	k1gNnSc4	rozšíření
zornic	zornice	k1gFnPc2	zornice
<g/>
,	,	kIx,	,
tachykardie	tachykardie	k1gFnSc1	tachykardie
<g/>
,	,	kIx,	,
pocení	pocení	k1gNnSc1	pocení
<g/>
,	,	kIx,	,
stažení	stažení	k1gNnSc1	stažení
cév	céva	k1gFnPc2	céva
a	a	k8xC	a
následně	následně	k6eAd1	následně
vzestup	vzestup	k1gInSc4	vzestup
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
sucho	sucho	k6eAd1	sucho
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
dávkou	dávka	k1gFnSc7	dávka
bledost	bledost	k1gFnSc1	bledost
<g/>
,	,	kIx,	,
třes	třes	k1gInSc1	třes
<g/>
,	,	kIx,	,
hypertenze	hypertenze	k1gFnSc1	hypertenze
a	a	k8xC	a
riziko	riziko	k1gNnSc1	riziko
dehydratace	dehydratace	k1gFnSc2	dehydratace
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
napětí	napětí	k1gNnSc1	napětí
žvýkacího	žvýkací	k2eAgNnSc2d1	žvýkací
svalstva	svalstvo	k1gNnSc2	svalstvo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
projevuje	projevovat	k5eAaImIp3nS	projevovat
bruxismem	bruxismus	k1gInSc7	bruxismus
(	(	kIx(	(
<g/>
skřípáním	skřípání	k1gNnSc7	skřípání
zubů	zub	k1gInPc2	zub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
účinek	účinek	k1gInSc4	účinek
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
i	i	k9	i
závratě	závrať	k1gFnPc4	závrať
<g/>
,	,	kIx,	,
nystagmus	nystagmus	k1gInSc1	nystagmus
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
bušení	bušení	k1gNnSc1	bušení
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
arytmie	arytmie	k1gFnSc2	arytmie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zimomřivost	zimomřivost	k1gFnSc1	zimomřivost
střídaná	střídaný	k2eAgFnSc1d1	střídaná
pocitem	pocit	k1gInSc7	pocit
horka	horko	k1gNnSc2	horko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
účinků	účinek	k1gInPc2	účinek
nastává	nastávat	k5eAaImIp3nS	nastávat
(	(	kIx(	(
<g/>
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
opakovaných	opakovaný	k2eAgFnPc2d1	opakovaná
dávek	dávka	k1gFnPc2	dávka
<g/>
)	)	kIx)	)
vlivem	vliv	k1gInSc7	vliv
deplece	depleec	k1gInSc2	depleec
(	(	kIx(	(
<g/>
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
)	)	kIx)	)
zúčastněných	zúčastněný	k2eAgMnPc2d1	zúčastněný
neurotransmiterů	neurotransmiter	k1gMnPc2	neurotransmiter
velmi	velmi	k6eAd1	velmi
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
stav	stav	k1gInSc1	stav
zvaný	zvaný	k2eAgInSc1d1	zvaný
dojezd	dojezd	k1gInSc4	dojezd
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
celkovým	celkový	k2eAgNnSc7d1	celkové
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
<g/>
,	,	kIx,	,
obtížemi	obtíž	k1gFnPc7	obtíž
s	s	k7c7	s
usínáním	usínání	k1gNnSc7	usínání
a	a	k8xC	a
nekvalitním	kvalitní	k2eNgInSc7d1	nekvalitní
spánkem	spánek	k1gInSc7	spánek
<g/>
,	,	kIx,	,
depresivním	depresivní	k2eAgNnSc7d1	depresivní
laděním	ladění	k1gNnSc7	ladění
<g/>
,	,	kIx,	,
úzkostmi	úzkost	k1gFnPc7	úzkost
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
nutkáním	nutkání	k1gNnSc7	nutkání
opakovat	opakovat	k5eAaImF	opakovat
dávku	dávka	k1gFnSc4	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Akutní	akutní	k2eAgNnSc1d1	akutní
předávkování	předávkování	k1gNnSc1	předávkování
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
silnými	silný	k2eAgFnPc7d1	silná
bolestmi	bolest	k1gFnPc7	bolest
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
,	,	kIx,	,
agitovaným	agitovaný	k2eAgInSc7d1	agitovaný
neklidem	neklid	k1gInSc7	neklid
<g/>
,	,	kIx,	,
svalovými	svalový	k2eAgFnPc7d1	svalová
křečemi	křeč	k1gFnPc7	křeč
<g/>
,	,	kIx,	,
poruchami	porucha	k1gFnPc7	porucha
srdečního	srdeční	k2eAgInSc2d1	srdeční
rytmu	rytmus	k1gInSc2	rytmus
<g/>
,	,	kIx,	,
halucinacemi	halucinace	k1gFnPc7	halucinace
<g/>
,	,	kIx,	,
deliriem	delirium	k1gNnSc7	delirium
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
až	až	k9	až
několikahodinovým	několikahodinový	k2eAgNnSc7d1	několikahodinové
bezvědomím	bezvědomí	k1gNnSc7	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
hypertenze	hypertenze	k1gFnSc1	hypertenze
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
reflexní	reflexní	k2eAgFnSc1d1	reflexní
hypotenze	hypotenze	k1gFnSc1	hypotenze
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
epileptických	epileptický	k2eAgInPc2d1	epileptický
záchvatů	záchvat	k1gInPc2	záchvat
<g/>
,	,	kIx,	,
k	k	k7c3	k
infarktu	infarkt	k1gInSc3	infarkt
myokardu	myokard	k1gInSc2	myokard
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzácnějších	vzácný	k2eAgInPc6d2	vzácnější
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
rabdomyolýza	rabdomyolýza	k1gFnSc1	rabdomyolýza
(	(	kIx(	(
<g/>
nekróza	nekróza	k1gFnSc1	nekróza
buněk	buňka	k1gFnPc2	buňka
kosterního	kosterní	k2eAgNnSc2d1	kosterní
svalstva	svalstvo	k1gNnSc2	svalstvo
s	s	k7c7	s
vyplavením	vyplavení	k1gNnSc7	vyplavení
myoglobinu	myoglobina	k1gFnSc4	myoglobina
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
kolaps	kolaps	k1gInSc1	kolaps
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
kóma	kóma	k1gNnSc4	kóma
až	až	k8xS	až
smrt	smrt	k1gFnSc4	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
u	u	k7c2	u
těžké	těžký	k2eAgFnSc2d1	těžká
intoxikace	intoxikace	k1gFnSc2	intoxikace
metamfetaminem	metamfetamin	k1gInSc7	metamfetamin
obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
krvácení	krvácení	k1gNnSc2	krvácení
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
kolaps	kolaps	k1gInSc1	kolaps
dechového	dechový	k2eAgNnSc2d1	dechové
centra	centrum	k1gNnSc2	centrum
či	či	k8xC	či
zmiňované	zmiňovaný	k2eAgNnSc4d1	zmiňované
selhání	selhání	k1gNnSc4	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Metamfetamin	Metamfetamin	k1gInSc1	Metamfetamin
(	(	kIx(	(
<g/>
s	s	k7c7	s
částečným	částečný	k2eAgNnSc7d1	částečné
přispěním	přispění	k1gNnSc7	přispění
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
produktů	produkt	k1gInPc2	produkt
amatérské	amatérský	k2eAgFnSc2d1	amatérská
syntézy	syntéza	k1gFnSc2	syntéza
<g/>
)	)	kIx)	)
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
srdce	srdce	k1gNnPc4	srdce
<g/>
,	,	kIx,	,
játra	játra	k1gNnPc4	játra
<g/>
,	,	kIx,	,
ledviny	ledvina	k1gFnPc4	ledvina
a	a	k8xC	a
zubní	zubní	k2eAgFnSc4d1	zubní
sklovinu	sklovina	k1gFnSc4	sklovina
<g/>
,	,	kIx,	,
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
psychickým	psychický	k2eAgNnSc7d1	psychické
rizikem	riziko	k1gNnSc7	riziko
je	být	k5eAaImIp3nS	být
plíživě	plíživě	k6eAd1	plíživě
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
paranoia	paranoia	k1gFnSc1	paranoia
<g/>
,	,	kIx,	,
deprese	deprese	k1gFnSc1	deprese
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc1	úzkost
a	a	k8xC	a
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
opiátů	opiát	k1gInPc2	opiát
(	(	kIx(	(
<g/>
morfin	morfin	k1gInSc1	morfin
<g/>
,	,	kIx,	,
heroin	heroin	k1gInSc1	heroin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
sedativ	sedativum	k1gNnPc2	sedativum
<g/>
,	,	kIx,	,
metamfetamin	metamfetamin	k1gInSc1	metamfetamin
nevede	vést	k5eNaImIp3nS	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
fyzické	fyzický	k2eAgFnSc2d1	fyzická
závislosti	závislost	k1gFnSc2	závislost
<g/>
,	,	kIx,	,
příznačné	příznačný	k2eAgNnSc1d1	příznačné
je	být	k5eAaImIp3nS	být
však	však	k9	však
riziko	riziko	k1gNnSc4	riziko
silné	silný	k2eAgFnSc2d1	silná
závislosti	závislost	k1gFnSc2	závislost
psychické	psychický	k2eAgFnSc2d1	psychická
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
kokainu	kokain	k1gInSc2	kokain
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
heroinu	heroin	k1gInSc2	heroin
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
somatický	somatický	k2eAgInSc1d1	somatický
návyk	návyk	k1gInSc1	návyk
s	s	k7c7	s
typickými	typický	k2eAgInPc7d1	typický
abstinenčními	abstinenční	k2eAgInPc7d1	abstinenční
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
u	u	k7c2	u
budivých	budivý	k2eAgFnPc2d1	budivý
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
vzniku	vznik	k1gInSc2	vznik
návyku	návyk	k1gInSc2	návyk
velmi	velmi	k6eAd1	velmi
individuální	individuální	k2eAgMnPc4d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
hnán	hnát	k5eAaImNgInS	hnát
bažením	bažení	k1gNnSc7	bažení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
craving	craving	k1gInSc1	craving
<g/>
)	)	kIx)	)
po	po	k7c6	po
další	další	k2eAgFnSc6d1	další
dávce	dávka	k1gFnSc6	dávka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pervitin	pervitin	k1gInSc1	pervitin
vyplaví	vyplavit	k5eAaPmIp3nS	vyplavit
několikanásobně	několikanásobně	k6eAd1	několikanásobně
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
dopaminu	dopamin	k1gInSc2	dopamin
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
mozek	mozek	k1gInSc1	mozek
schopný	schopný	k2eAgInSc1d1	schopný
maximálně	maximálně	k6eAd1	maximálně
uvolnit	uvolnit	k5eAaPmF	uvolnit
normálním	normální	k2eAgInSc7d1	normální
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
tedy	tedy	k9	tedy
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
cokoli	cokoli	k3yInSc4	cokoli
jiného	jiný	k2eAgMnSc4d1	jiný
mimo	mimo	k7c4	mimo
drogu	droga	k1gFnSc4	droga
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
závislí	závislý	k2eAgMnPc1d1	závislý
tráví	trávit	k5eAaImIp3nP	trávit
čas	čas	k1gInSc4	čas
zdlouhavými	zdlouhavý	k2eAgFnPc7d1	zdlouhavá
<g/>
,	,	kIx,	,
bezcílnými	bezcílný	k2eAgFnPc7d1	bezcílná
činnostmi	činnost	k1gFnPc7	činnost
(	(	kIx(	(
<g/>
demontáž	demontáž	k1gFnSc4	demontáž
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
,	,	kIx,	,
zapalovačů	zapalovač	k1gInPc2	zapalovač
<g/>
,	,	kIx,	,
psaní	psaní	k1gNnSc3	psaní
sáhodlouhých	sáhodlouhý	k2eAgInPc2d1	sáhodlouhý
<g/>
,	,	kIx,	,
obsahově	obsahově	k6eAd1	obsahově
prázdných	prázdný	k2eAgInPc2d1	prázdný
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
přehrabávaní	přehrabávaný	k2eAgMnPc1d1	přehrabávaný
popelnic	popelnice	k1gFnPc2	popelnice
<g/>
,	,	kIx,	,
zaobírání	zaobírání	k1gNnSc1	zaobírání
se	se	k3xPyFc4	se
paranoidními	paranoidní	k2eAgInPc7d1	paranoidní
prožitky	prožitek	k1gInPc7	prožitek
-	-	kIx~	-
štěnice	štěnice	k1gFnSc1	štěnice
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kamery	kamera	k1gFnPc1	kamera
<g/>
,	,	kIx,	,
tajné	tajný	k2eAgInPc1d1	tajný
čipy	čip	k1gInPc1	čip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
časové	časový	k2eAgInPc4d1	časový
úseky	úsek	k1gInPc4	úsek
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
dávkami	dávka	k1gFnPc7	dávka
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zkracují	zkracovat	k5eAaImIp3nP	zkracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
narušením	narušení	k1gNnSc7	narušení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
systému	systém	k1gInSc2	systém
odměn	odměna	k1gFnPc2	odměna
v	v	k7c6	v
mezimozku	mezimozek	k1gInSc6	mezimozek
(	(	kIx(	(
<g/>
pokles	pokles	k1gInSc1	pokles
počtu	počet	k1gInSc2	počet
dopaminergních	dopaminergní	k2eAgInPc2d1	dopaminergní
receptorů	receptor	k1gInPc2	receptor
<g/>
)	)	kIx)	)
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
potřeba	potřeba	k6eAd1	potřeba
navyšovat	navyšovat	k5eAaImF	navyšovat
dávkování	dávkování	k1gNnSc4	dávkování
i	i	k8xC	i
sklony	sklon	k1gInPc4	sklon
ke	k	k7c3	k
zvláštnímu	zvláštní	k2eAgNnSc3d1	zvláštní
chování	chování	k1gNnSc3	chování
<g/>
,	,	kIx,	,
paranoie	paranoia	k1gFnPc1	paranoia
a	a	k8xC	a
úzkosti	úzkost	k1gFnPc1	úzkost
<g/>
.	.	kIx.	.
</s>
<s>
Tolerance	tolerance	k1gFnSc1	tolerance
na	na	k7c4	na
účinek	účinek	k1gInSc4	účinek
látek	látka	k1gFnPc2	látka
typu	typ	k1gInSc2	typ
amfetaminu	amfetamin	k1gInSc2	amfetamin
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
i	i	k9	i
díky	díky	k7c3	díky
silné	silný	k2eAgFnSc3d1	silná
touze	touha	k1gFnSc3	touha
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
maximální	maximální	k2eAgFnSc1d1	maximální
euforie	euforie	k1gFnSc1	euforie
a	a	k8xC	a
vzrušení	vzrušení	k1gNnSc1	vzrušení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
účinku	účinek	k1gInSc2	účinek
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
množství	množství	k1gNnSc1	množství
zneužívané	zneužívaný	k2eAgFnSc2d1	zneužívaná
látky	látka	k1gFnSc2	látka
zvyšováno	zvyšován	k2eAgNnSc1d1	zvyšováno
<g/>
.	.	kIx.	.
</s>
<s>
Užívané	užívaný	k2eAgFnPc1d1	užívaná
dávky	dávka	k1gFnPc1	dávka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
dávky	dávka	k1gFnPc1	dávka
počáteční	počáteční	k2eAgFnPc1d1	počáteční
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
akutní	akutní	k2eAgFnPc4d1	akutní
chronické	chronický	k2eAgFnPc4d1	chronická
otravy	otrava	k1gFnPc4	otrava
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
gastrointestinální	gastrointestinální	k2eAgFnPc1d1	gastrointestinální
poruchy	porucha	k1gFnPc1	porucha
<g/>
,	,	kIx,	,
bušení	bušení	k1gNnSc1	bušení
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
arytmie	arytmie	k1gFnSc1	arytmie
<g/>
,	,	kIx,	,
vzestup	vzestup	k1gInSc1	vzestup
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
neklid	neklid	k1gInSc1	neklid
<g/>
,	,	kIx,	,
nespavost	nespavost	k1gFnSc1	nespavost
<g/>
,	,	kIx,	,
myšlenkový	myšlenkový	k2eAgInSc1d1	myšlenkový
trysk	trysk	k1gInSc1	trysk
<g/>
,	,	kIx,	,
dráždivost	dráždivost	k1gFnSc1	dráždivost
<g/>
,	,	kIx,	,
kolaps	kolaps	k1gInSc1	kolaps
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Abstinence	abstinence	k1gFnSc1	abstinence
u	u	k7c2	u
závislého	závislý	k2eAgNnSc2d1	závislé
vyúsťuje	vyúsťovat	k5eAaImIp3nS	vyúsťovat
v	v	k7c4	v
odvykací	odvykací	k2eAgInSc4d1	odvykací
syndrom	syndrom	k1gInSc4	syndrom
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
míře	míra	k1gFnSc6	míra
návyku	návyk	k1gInSc2	návyk
setrvávat	setrvávat	k5eAaImF	setrvávat
od	od	k7c2	od
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c4	po
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Příznačná	příznačný	k2eAgFnSc1d1	příznačná
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
únava	únava	k1gFnSc1	únava
<g/>
,	,	kIx,	,
letargie	letargie	k1gFnSc1	letargie
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
chuť	chuť	k1gFnSc1	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
nesoustředěnost	nesoustředěnost	k1gFnSc1	nesoustředěnost
<g/>
,	,	kIx,	,
deprese	deprese	k1gFnSc1	deprese
<g/>
,	,	kIx,	,
podrážděnost	podrážděnost	k1gFnSc1	podrážděnost
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc1	úzkost
<g/>
,	,	kIx,	,
bažení	bažení	k1gNnSc1	bažení
po	po	k7c6	po
droze	droga	k1gFnSc6	droga
<g/>
,	,	kIx,	,
hypersomnie	hypersomnie	k1gFnSc1	hypersomnie
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
živé	živý	k2eAgInPc1d1	živý
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Toxická	toxický	k2eAgFnSc1d1	toxická
psychóza	psychóza	k1gFnSc1	psychóza
<g/>
,	,	kIx,	,
též	též	k9	též
amfetaminová	amfetaminový	k2eAgFnSc1d1	amfetaminový
psychóza	psychóza	k1gFnSc1	psychóza
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kterou	který	k3yIgFnSc7	který
může	moct	k5eAaImIp3nS	moct
užívání	užívání	k1gNnSc1	užívání
amfetaminů	amfetamin	k1gInPc2	amfetamin
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
nápadnými	nápadný	k2eAgFnPc7d1	nápadná
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
<g/>
,	,	kIx,	,
zrakovými	zrakový	k2eAgFnPc7d1	zraková
<g/>
,	,	kIx,	,
sluchovými	sluchový	k2eAgFnPc7d1	sluchová
a	a	k8xC	a
hmatovými	hmatový	k2eAgFnPc7d1	hmatová
halucinacemi	halucinace	k1gFnPc7	halucinace
<g/>
,	,	kIx,	,
paranoidními	paranoidní	k2eAgInPc7d1	paranoidní
bludy	blud	k1gInPc7	blud
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
pronásledování	pronásledování	k1gNnPc2	pronásledování
a	a	k8xC	a
neustálým	neustálý	k2eAgInSc7d1	neustálý
pocitem	pocit	k1gInSc7	pocit
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
stíha	stíh	k1gMnSc2	stíh
<g/>
,	,	kIx,	,
panikou	panika	k1gFnSc7	panika
<g/>
,	,	kIx,	,
agresivitou	agresivita	k1gFnSc7	agresivita
agitovaným	agitovaný	k2eAgInSc7d1	agitovaný
neklidem	neklid	k1gInSc7	neklid
<g/>
,	,	kIx,	,
zmateným	zmatený	k2eAgNnSc7d1	zmatené
myšlením	myšlení	k1gNnSc7	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Toxická	toxický	k2eAgFnSc1d1	toxická
psychóza	psychóza	k1gFnSc1	psychóza
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
těžších	těžký	k2eAgInPc6d2	těžší
případech	případ	k1gInPc6	případ
téměř	téměř	k6eAd1	téměř
nerozlišitelná	rozlišitelný	k2eNgFnSc1d1	nerozlišitelná
od	od	k7c2	od
akutní	akutní	k2eAgFnSc2d1	akutní
paranoidní	paranoidní	k2eAgFnSc2d1	paranoidní
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
detoxifikaci	detoxifikace	k1gFnSc6	detoxifikace
a	a	k8xC	a
splacení	splacení	k1gNnSc2	splacení
často	často	k6eAd1	často
výrazného	výrazný	k2eAgInSc2d1	výrazný
spánkového	spánkový	k2eAgInSc2d1	spánkový
deficitu	deficit	k1gInSc2	deficit
však	však	k9	však
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
odeznívá	odeznívat	k5eAaImIp3nS	odeznívat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jedinců	jedinec	k1gMnPc2	jedinec
s	s	k7c7	s
predispozicemi	predispozice	k1gFnPc7	predispozice
ke	k	k7c3	k
schizofrenii	schizofrenie	k1gFnSc3	schizofrenie
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
chronická	chronický	k2eAgFnSc1d1	chronická
psychóza	psychóza	k1gFnSc1	psychóza
skutečně	skutečně	k6eAd1	skutečně
propuknout	propuknout	k5eAaPmF	propuknout
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
toxické	toxický	k2eAgFnSc2d1	toxická
psychózy	psychóza	k1gFnSc2	psychóza
spočívá	spočívat	k5eAaImIp3nS	spočívat
zejména	zejména	k9	zejména
v	v	k7c6	v
bodu	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nemocný	nemocný	k1gMnSc1	nemocný
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
těchto	tento	k3xDgInPc2	tento
stavů	stav	k1gInPc2	stav
je	být	k5eAaImIp3nS	být
dopaminergní	dopaminergní	k2eAgFnSc1d1	dopaminergní
neurotoxicita	neurotoxicita	k1gFnSc1	neurotoxicita
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnSc1	deprivace
<g/>
.	.	kIx.	.
</s>
<s>
Psychóza	psychóza	k1gFnSc1	psychóza
může	moct	k5eAaImIp3nS	moct
propuknout	propuknout	k5eAaPmF	propuknout
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
předávkování	předávkování	k1gNnSc2	předávkování
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vážnějších	vážní	k2eAgInPc6d2	vážnější
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
podoby	podoba	k1gFnSc2	podoba
amfetaminového	amfetaminový	k2eAgNnSc2d1	amfetaminový
intoxikačního	intoxikační	k2eAgNnSc2d1	intoxikační
deliria	delirium	k1gNnSc2	delirium
s	s	k7c7	s
přehřátím	přehřátí	k1gNnSc7	přehřátí
<g/>
,	,	kIx,	,
křečemi	křeč	k1gFnPc7	křeč
<g/>
,	,	kIx,	,
arytmiemi	arytmie	k1gFnPc7	arytmie
či	či	k8xC	či
rhabdomyolýzou	rhabdomyolýza	k1gFnSc7	rhabdomyolýza
<g/>
.	.	kIx.	.
</s>
<s>
Mnoha	mnoho	k4c3	mnoho
cenami	cena	k1gFnPc7	cena
ověnčený	ověnčený	k2eAgInSc4d1	ověnčený
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
Perníkový	perníkový	k2eAgMnSc1d1	perníkový
táta	táta	k1gMnSc1	táta
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
středoškolském	středoškolský	k2eAgMnSc6d1	středoškolský
učiteli	učitel	k1gMnSc6	učitel
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
rakovinou	rakovina	k1gFnSc7	rakovina
a	a	k8xC	a
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
a	a	k8xC	a
k	k	k7c3	k
zaopatření	zaopatření	k1gNnSc3	zaopatření
rodiny	rodina	k1gFnSc2	rodina
začne	začít	k5eAaPmIp3nS	začít
vydělávat	vydělávat	k5eAaImF	vydělávat
výrobou	výroba	k1gFnSc7	výroba
a	a	k8xC	a
prodejem	prodej	k1gInSc7	prodej
nejčistčího	jčistčí	k2eNgInSc2d1	nejčistčí
metamfetaminu	metamfetamin	k1gInSc2	metamfetamin
Příběh	příběh	k1gInSc1	příběh
člověka	člověk	k1gMnSc2	člověk
závislého	závislý	k2eAgMnSc2d1	závislý
na	na	k7c6	na
pervitinu	pervitin	k1gInSc6	pervitin
popisuje	popisovat	k5eAaImIp3nS	popisovat
kniha	kniha	k1gFnSc1	kniha
Memento	memento	k1gNnSc4	memento
spisovatele	spisovatel	k1gMnSc2	spisovatel
Radka	Radek	k1gMnSc2	Radek
Johna	John	k1gMnSc2	John
<g/>
.	.	kIx.	.
</s>
<s>
Metamfetamin	Metamfetamin	k1gInSc1	Metamfetamin
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
jako	jako	k9	jako
podpůrný	podpůrný	k2eAgInSc1d1	podpůrný
prostředek	prostředek	k1gInSc1	prostředek
agenta	agent	k1gMnSc2	agent
ruské	ruský	k2eAgFnSc2d1	ruská
MGB	MGB	kA	MGB
v	v	k7c6	v
románu	román	k1gInSc6	román
Dítě	Dítě	k2eAgNnSc1d1	Dítě
číslo	číslo	k1gNnSc1	číslo
44	[number]	k4	44
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaPmAgInS	napsat
Tom	Tom	k1gMnSc1	Tom
Rob	roba	k1gFnPc2	roba
Smith	Smith	k1gMnSc1	Smith
Smrt	smrt	k1gFnSc1	smrt
jménem	jméno	k1gNnSc7	jméno
Závislost	závislost	k1gFnSc1	závislost
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Vondrka	Vondrka	k1gMnSc1	Vondrka
Film	film	k1gInSc4	film
Piko	piko	k1gNnSc1	piko
-	-	kIx~	-
Tomáš	Tomáš	k1gMnSc1	Tomáš
Řehořek	Řehořek	k1gMnSc1	Řehořek
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
o	o	k7c6	o
začátcích	začátek	k1gInPc6	začátek
pervitinu	pervitin	k1gInSc2	pervitin
na	na	k7c6	na
československé	československý	k2eAgFnSc6d1	Československá
drogové	drogový	k2eAgFnSc6d1	drogová
scéně	scéna	k1gFnSc6	scéna
Film	film	k1gInSc1	film
Salton	Salton	k1gInSc1	Salton
Sea	Sea	k1gFnSc1	Sea
-	-	kIx~	-
D.J.	D.J.	k1gFnSc1	D.J.
Caruso	Carusa	k1gFnSc5	Carusa
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
thriller	thriller	k1gInSc1	thriller
o	o	k7c6	o
toxikomanovi	toxikoman	k1gMnSc6	toxikoman
a	a	k8xC	a
současně	současně	k6eAd1	současně
výrobci	výrobce	k1gMnPc1	výrobce
pervitinu	pervitin	k1gInSc2	pervitin
Píseň	píseň	k1gFnSc1	píseň
Love	lov	k1gInSc5	lov
and	and	k?	and
Meth	Meth	k1gInSc4	Meth
americké	americký	k2eAgFnSc2d1	americká
skupiny	skupina	k1gFnSc2	skupina
Korn	Korn	k1gMnSc1	Korn
Česko	Česko	k1gNnSc4	Česko
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
ilegálním	ilegální	k2eAgMnSc7d1	ilegální
producentem	producent	k1gMnSc7	producent
pervitinu	pervitin	k1gInSc2	pervitin
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ČR	ČR	kA	ČR
odhalovány	odhalován	k2eAgFnPc4d1	odhalována
stovky	stovka	k1gFnPc4	stovka
varen	varna	k1gFnPc2	varna
této	tento	k3xDgFnSc2	tento
návykové	návykový	k2eAgFnSc2d1	návyková
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
patří	patřit	k5eAaImIp3nS	patřit
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
k	k	k7c3	k
nejčastějším	častý	k2eAgFnPc3d3	nejčastější
drogám	droga	k1gFnPc3	droga
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
léků	lék	k1gInPc2	lék
obsahujících	obsahující	k2eAgInPc2d1	obsahující
pseudoefedrin	pseudoefedrin	k1gInSc4	pseudoefedrin
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
na	na	k7c6	na
pervitinu	pervitin	k1gInSc6	pervitin
závislá	závislý	k2eAgFnSc1d1	závislá
0,7	[number]	k4	0,7
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
64	[number]	k4	64
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
problémových	problémový	k2eAgFnPc2d1	problémová
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
