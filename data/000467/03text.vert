<s>
Diana	Diana	k1gFnSc1	Diana
je	být	k5eAaImIp3nS	být
římská	římský	k2eAgFnSc1d1	římská
panenská	panenský	k2eAgFnSc1d1	panenská
bohyně	bohyně	k1gFnSc1	bohyně
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
obdoba	obdoba	k1gFnSc1	obdoba
bohyně	bohyně	k1gFnSc1	bohyně
Artemis	Artemis	k1gFnSc1	Artemis
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
vyobrazována	vyobrazovat	k5eAaImNgFnS	vyobrazovat
s	s	k7c7	s
lukem	luk	k1gInSc7	luk
a	a	k8xC	a
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
psů	pes	k1gMnPc2	pes
nebo	nebo	k8xC	nebo
lesních	lesní	k2eAgNnPc2d1	lesní
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
patrně	patrně	k6eAd1	patrně
později	pozdě	k6eAd2	pozdě
převzala	převzít	k5eAaPmAgFnS	převzít
řadu	řada	k1gFnSc4	řada
rysů	rys	k1gInPc2	rys
řecké	řecký	k2eAgFnSc2d1	řecká
Artemis	Artemis	k1gFnSc1	Artemis
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Diana	Diana	k1gFnSc1	Diana
původně	původně	k6eAd1	původně
bohyně	bohyně	k1gFnSc1	bohyně
italská	italský	k2eAgFnSc1d1	italská
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
úcta	úcta	k1gFnSc1	úcta
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
italických	italický	k2eAgInPc2d1	italický
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
božská	božská	k1gFnSc1	božská
<g/>
,	,	kIx,	,
nebeská	nebeský	k2eAgFnSc1d1	nebeská
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
téhož	týž	k3xTgInSc2	týž
původu	původ	k1gInSc2	původ
jako	jako	k8xS	jako
latinské	latinský	k2eAgInPc1d1	latinský
divius	divius	k1gInSc1	divius
<g/>
,	,	kIx,	,
dieus	dieus	k1gInSc1	dieus
<g/>
,	,	kIx,	,
divus	divus	k1gInSc1	divus
a	a	k8xC	a
pozdější	pozdní	k2eAgInSc1d2	pozdější
deus	deus	k1gInSc1	deus
(	(	kIx(	(
<g/>
bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
a	a	k8xC	a
dies	dies	k1gInSc1	dies
(	(	kIx(	(
<g/>
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
obloha	obloha	k1gFnSc1	obloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
nebeské	nebeský	k2eAgMnPc4d1	nebeský
bohy	bůh	k1gMnPc4	bůh
<g/>
,	,	kIx,	,
zachovala	zachovat	k5eAaPmAgFnS	zachovat
si	se	k3xPyFc3	se
však	však	k9	však
vždy	vždy	k6eAd1	vždy
také	také	k9	také
nějakou	nějaký	k3yIgFnSc4	nějaký
pozemskou	pozemský	k2eAgFnSc4d1	pozemská
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
bohyní	bohyně	k1gFnSc7	bohyně
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
ochránkyní	ochránkyně	k1gFnSc7	ochránkyně
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
rodiček	rodička	k1gFnPc2	rodička
a	a	k8xC	a
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
také	také	k9	také
pokračování	pokračování	k1gNnSc4	pokračování
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
královského	královský	k2eAgInSc2d1	královský
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
posvátným	posvátný	k2eAgInSc7d1	posvátný
stromem	strom	k1gInSc7	strom
byl	být	k5eAaImAgInS	být
dub	dub	k1gInSc1	dub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
byla	být	k5eAaImAgFnS	být
Artemis	Artemis	k1gFnSc1	Artemis
/	/	kIx~	/
Diana	Diana	k1gFnSc1	Diana
dcera	dcera	k1gFnSc1	dcera
Jupitera	Jupiter	k1gMnSc2	Jupiter
a	a	k8xC	a
Létó	Létó	k1gMnSc2	Létó
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Apollónem	Apollón	k1gMnSc7	Apollón
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Délu	Délos	k1gInSc2	Délos
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
nymfou	nymfa	k1gFnSc7	nymfa
Egerií	Egerie	k1gFnPc2	Egerie
a	a	k8xC	a
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
lesů	les	k1gInPc2	les
Viribus	Viribus	k1gInSc4	Viribus
se	se	k3xPyFc4	se
zapřisáhla	zapřisáhnout	k5eAaPmAgFnS	zapřisáhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zůstane	zůstat	k5eAaPmIp3nS	zůstat
pannou	panna	k1gFnSc7	panna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
římské	římský	k2eAgFnSc6d1	římská
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývala	nazývat	k5eAaImAgFnS	nazývat
dea	dea	k?	dea
trivia	trivium	k1gNnPc4	trivium
(	(	kIx(	(
<g/>
=	=	kIx~	=
trojcestná	trojcestný	k2eAgFnSc1d1	trojcestná
<g/>
)	)	kIx)	)
a	a	k8xC	a
zobrazovala	zobrazovat	k5eAaImAgFnS	zobrazovat
jako	jako	k9	jako
tři	tři	k4xCgFnPc1	tři
postavy	postava	k1gFnPc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c7	mnoho
mýty	mýto	k1gNnPc7	mýto
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejznámější	známý	k2eAgFnSc1d3	nejznámější
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
lovci	lovec	k1gMnSc6	lovec
Acteonovi	Acteon	k1gMnSc6	Acteon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
spatřil	spatřit	k5eAaPmAgMnS	spatřit
při	při	k7c6	při
koupání	koupání	k1gNnSc6	koupání
nahou	nahý	k2eAgFnSc4d1	nahá
<g/>
;	;	kIx,	;
Diana	Diana	k1gFnSc1	Diana
ho	on	k3xPp3gMnSc4	on
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c4	v
jelena	jelen	k1gMnSc4	jelen
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
vlastní	vlastní	k2eAgMnPc1d1	vlastní
psi	pes	k1gMnPc1	pes
ho	on	k3xPp3gNnSc4	on
roztrhali	roztrhat	k5eAaPmAgMnP	roztrhat
<g/>
.	.	kIx.	.
</s>
<s>
Svatyně	svatyně	k1gFnPc1	svatyně
Diany	Diana	k1gFnSc2	Diana
byly	být	k5eAaImAgFnP	být
patrně	patrně	k6eAd1	patrně
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
střední	střední	k2eAgFnSc6d1	střední
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
svátek	svátek	k1gInSc1	svátek
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
o	o	k7c6	o
srpnových	srpnový	k2eAgFnPc6d1	srpnová
Idách	Ida	k1gFnPc6	Ida
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
Servius	Servius	k1gMnSc1	Servius
Tullius	Tullius	k1gMnSc1	Tullius
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zasvětil	zasvětit	k5eAaPmAgInS	zasvětit
její	její	k3xOp3gInSc1	její
chrám	chrám	k1gInSc1	chrám
na	na	k7c6	na
římském	římský	k2eAgInSc6d1	římský
Aventinu	Aventin	k1gInSc6	Aventin
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
sám	sám	k3xTgInSc1	sám
Tullius	Tullius	k1gInSc1	Tullius
byl	být	k5eAaImAgInS	být
původem	původ	k1gInSc7	původ
otrok	otrok	k1gMnSc1	otrok
<g/>
,	,	kIx,	,
slavil	slavit	k5eAaImAgMnS	slavit
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Den	den	k1gInSc1	den
otroků	otrok	k1gMnPc2	otrok
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
servorum	servorum	k1gNnSc1	servorum
dies	diesa	k1gFnPc2	diesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
otroci	otrok	k1gMnPc1	otrok
měli	mít	k5eAaImAgMnP	mít
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
právo	právo	k1gNnSc4	právo
azylu	azyl	k1gInSc2	azyl
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
svatyní	svatyně	k1gFnSc7	svatyně
však	však	k9	však
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
posvátný	posvátný	k2eAgInSc4d1	posvátný
háj	háj	k1gInSc4	háj
cypřišů	cypřiš	k1gInPc2	cypřiš
u	u	k7c2	u
Aricia	Aricium	k1gNnSc2	Aricium
(	(	kIx(	(
<g/>
26	[number]	k4	26
km	km	kA	km
JV	JV	kA	JV
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlil	sídlit	k5eAaImAgInS	sídlit
také	také	k9	také
Dianin	Dianin	k2eAgMnSc1d1	Dianin
velekněz	velekněz	k1gMnSc1	velekněz
<g/>
,	,	kIx,	,
Rex	Rex	k1gMnSc1	Rex
Nemorensis	Nemorensis	k1gFnSc2	Nemorensis
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
vyprávělo	vyprávět	k5eAaImAgNnS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zastával	zastávat	k5eAaImAgMnS	zastávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ho	on	k3xPp3gMnSc4	on
někdo	někdo	k3yInSc1	někdo
silnější	silný	k2eAgMnSc1d2	silnější
nezabil	zabít	k5eNaPmAgMnS	zabít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
chodil	chodit	k5eAaImAgMnS	chodit
neustále	neustále	k6eAd1	neustále
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pověst	pověst	k1gFnSc1	pověst
pak	pak	k6eAd1	pak
hrála	hrát	k5eAaImAgFnS	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
ve	v	k7c6	v
výkladech	výklad	k1gInPc6	výklad
J.	J.	kA	J.
Frazera	Frazero	k1gNnSc2	Frazero
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
helénistické	helénistický	k2eAgFnSc6d1	helénistická
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
slavný	slavný	k2eAgInSc1d1	slavný
chrám	chrám	k1gInSc1	chrám
a	a	k8xC	a
kult	kult	k1gInSc1	kult
bohyně	bohyně	k1gFnSc2	bohyně
Diany	Diana	k1gFnSc2	Diana
(	(	kIx(	(
<g/>
Artemis	Artemis	k1gFnSc1	Artemis
<g/>
)	)	kIx)	)
v	v	k7c6	v
Efesu	Efes	k1gInSc6	Efes
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
také	také	k9	také
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
(	(	kIx(	(
<g/>
Sk	Sk	kA	Sk
19,28	[number]	k4	19,28
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
Diany	Diana	k1gFnSc2	Diana
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
bylo	být	k5eAaImAgNnS	být
nesmírně	smírně	k6eNd1	smírně
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
v	v	k7c6	v
barokním	barokní	k2eAgNnSc6d1	barokní
malířství	malířství	k1gNnSc6	malířství
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Diana	Diana	k1gFnSc1	Diana
se	se	k3xPyFc4	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
Shakespearových	Shakespearových	k2eAgFnPc6d1	Shakespearových
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
u	u	k7c2	u
romantiků	romantik	k1gMnPc2	romantik
a	a	k8xC	a
v	v	k7c6	v
Cocteauově	Cocteauův	k2eAgInSc6d1	Cocteauův
filmu	film	k1gInSc6	film
"	"	kIx"	"
<g/>
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
Dianina	Dianin	k2eAgFnSc1d1	Dianina
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zvíře	zvíře	k1gNnSc4	zvíře
dokázala	dokázat	k5eAaPmAgFnS	dokázat
zkrotit	zkrotit	k5eAaPmF	zkrotit
a	a	k8xC	a
proměnit	proměnit	k5eAaPmF	proměnit
<g/>
.	.	kIx.	.
</s>
