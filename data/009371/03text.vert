<p>
<s>
Petit	petit	k1gInSc1	petit
Palais	Palais	k1gFnSc2	Palais
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Malý	malý	k2eAgInSc1d1	malý
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
muzeum	muzeum	k1gNnSc1	muzeum
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
jako	jako	k9	jako
výstavní	výstavní	k2eAgInSc4d1	výstavní
pavilon	pavilon	k1gInSc4	pavilon
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
protilehlý	protilehlý	k2eAgMnSc1d1	protilehlý
Grand	grand	k1gMnSc1	grand
Palais	Palais	k1gFnSc2	Palais
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
architekt	architekt	k1gMnSc1	architekt
Charles	Charles	k1gMnSc1	Charles
Girault	Girault	k1gMnSc1	Girault
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c4	na
Avenue	avenue	k1gFnSc4	avenue
Winston-Churchill	Winston-Churchilla	k1gFnPc2	Winston-Churchilla
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
historická	historický	k2eAgFnSc1d1	historická
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
pro	pro	k7c4	pro
světovou	světový	k2eAgFnSc4d1	světová
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
bylo	být	k5eAaImAgNnS	být
zprovozněno	zprovoznit	k5eAaPmNgNnS	zprovoznit
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1902	[number]	k4	1902
jako	jako	k8xC	jako
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc1	umění
(	(	kIx(	(
<g/>
Palais	Palais	k1gInSc1	Palais
des	des	k1gNnSc1	des
Beaux-Arts	Beaux-Arts	k1gInSc1	Beaux-Arts
de	de	k?	de
la	la	k1gNnSc7	la
Ville	Ville	k1gFnSc2	Ville
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001-2005	[number]	k4	2001-2005
byl	být	k5eAaImAgInS	být
Petit	petit	k1gInSc1	petit
Palais	Palais	k1gFnSc2	Palais
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Petit	petit	k1gInSc1	petit
Palais	Palais	k1gFnSc2	Palais
je	být	k5eAaImIp3nS	být
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
kolem	kolem	k7c2	kolem
půlkruhové	půlkruhový	k2eAgFnSc2d1	půlkruhová
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Výstavní	výstavní	k2eAgFnPc1d1	výstavní
prostory	prostora	k1gFnPc1	prostora
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
5000	[number]	k4	5000
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
přízemí	přízemí	k1gNnSc1	přízemí
je	být	k5eAaImIp3nS	být
věnováno	věnovat	k5eAaPmNgNnS	věnovat
kancelářím	kancelář	k1gFnPc3	kancelář
a	a	k8xC	a
depozitářům	depozitář	k1gMnPc3	depozitář
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
150	[number]	k4	150
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
monumentální	monumentální	k2eAgInSc4d1	monumentální
portikus	portikus	k1gInSc4	portikus
s	s	k7c7	s
kupolí	kupole	k1gFnSc7	kupole
<g/>
.	.	kIx.	.
</s>
<s>
Jónské	jónský	k2eAgInPc1d1	jónský
sloupy	sloup	k1gInPc1	sloup
zdobí	zdobit	k5eAaImIp3nP	zdobit
venkovní	venkovní	k2eAgFnSc4d1	venkovní
fasádu	fasáda	k1gFnSc4	fasáda
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
půlkruhový	půlkruhový	k2eAgInSc4d1	půlkruhový
peristyl	peristyl	k1gInSc4	peristyl
na	na	k7c6	na
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Výzdobu	výzdoba	k1gFnSc4	výzdoba
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
basreliéfy	basreliéf	k1gInPc4	basreliéf
<g/>
.	.	kIx.	.
</s>
<s>
Osvětlení	osvětlení	k1gNnSc1	osvětlení
prostor	prostora	k1gFnPc2	prostora
je	být	k5eAaImIp3nS	být
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
velká	velký	k2eAgNnPc1d1	velké
okna	okno	k1gNnPc1	okno
a	a	k8xC	a
prosklená	prosklený	k2eAgFnSc1d1	prosklená
kupole	kupole	k1gFnSc1	kupole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sbírky	sbírka	k1gFnSc2	sbírka
==	==	k?	==
</s>
</p>
<p>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
od	od	k7c2	od
antiky	antika	k1gFnSc2	antika
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgInPc1d1	umělecký
předměty	předmět	k1gInPc1	předmět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnSc2	sbírka
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Palais	Palais	k1gFnSc6	Palais
de	de	k?	de
Tokyo	Tokyo	k6eAd1	Tokyo
<g/>
.	.	kIx.	.
</s>
<s>
Sbírky	sbírka	k1gFnPc1	sbírka
byly	být	k5eAaImAgFnP	být
obohaceny	obohacen	k2eAgFnPc1d1	obohacena
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
darů	dar	k1gInPc2	dar
věnovaných	věnovaný	k2eAgFnPc2d1	věnovaná
městu	město	k1gNnSc3	město
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Eugè	Eugè	k?	Eugè
a	a	k8xC	a
Auguste	August	k1gMnSc5	August
Dutuitovi	Dutuitův	k2eAgMnPc1d1	Dutuitův
přenechali	přenechat	k5eAaPmAgMnP	přenechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
muzeu	muzeum	k1gNnSc6	muzeum
svou	svůj	k3xOyFgFnSc4	svůj
sbírku	sbírka	k1gFnSc4	sbírka
20	[number]	k4	20
000	[number]	k4	000
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
např.	např.	kA	např.
Rembrandtův	Rembrandtův	k2eAgInSc1d1	Rembrandtův
autoportrét	autoportrét	k1gInSc1	autoportrét
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
Rubense	Rubense	k1gFnSc1	Rubense
<g/>
,	,	kIx,	,
antické	antický	k2eAgNnSc1d1	antické
umění	umění	k1gNnSc1	umění
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgInPc4d1	umělecký
předměty	předmět	k1gInPc4	předmět
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
po	po	k7c6	po
renesanci	renesance	k1gFnSc6	renesance
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
Edward	Edward	k1gMnSc1	Edward
a	a	k8xC	a
Julia	Julius	k1gMnSc2	Julius
Tuckovi	Tuckův	k2eAgMnPc1d1	Tuckův
odkázali	odkázat	k5eAaPmAgMnP	odkázat
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
svou	svůj	k3xOyFgFnSc4	svůj
sbírku	sbírka	k1gFnSc4	sbírka
francouzského	francouzský	k2eAgNnSc2d1	francouzské
umění	umění	k1gNnSc2	umění
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ambroise	Ambroise	k1gFnSc1	Ambroise
Vollard	Vollarda	k1gFnPc2	Vollarda
věnoval	věnovat	k5eAaPmAgMnS	věnovat
sbírku	sbírka	k1gFnSc4	sbírka
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
mj.	mj.	kA	mj.
obrazy	obraz	k1gInPc7	obraz
Pierra	Pierro	k1gNnSc2	Pierro
Bonnarda	Bonnard	k1gMnSc2	Bonnard
a	a	k8xC	a
Paula	Paul	k1gMnSc2	Paul
Cézanna	Cézann	k1gMnSc2	Cézann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roger	Roger	k1gInSc1	Roger
Cabal	Cabal	k1gInSc1	Cabal
odkázal	odkázat	k5eAaPmAgInS	odkázat
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
sbírku	sbírka	k1gFnSc4	sbírka
ikon	ikona	k1gFnPc2	ikona
<g/>
.	.	kIx.	.
<g/>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
dary	dar	k1gInPc7	dar
muzeum	muzeum	k1gNnSc1	muzeum
získává	získávat	k5eAaImIp3nS	získávat
další	další	k2eAgNnPc4d1	další
díla	dílo	k1gNnPc4	dílo
nákupem	nákup	k1gInSc7	nákup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
grafického	grafický	k2eAgNnSc2d1	grafické
umění	umění	k1gNnSc2	umění
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
18	[number]	k4	18
000	[number]	k4	000
rytin	rytina	k1gFnPc2	rytina
a	a	k8xC	a
9000	[number]	k4	9000
kreseb	kresba	k1gFnPc2	kresba
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
po	po	k7c4	po
moderní	moderní	k2eAgNnSc4d1	moderní
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Kresbami	kresba	k1gFnPc7	kresba
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
Rembrandt	Rembrandta	k1gFnPc2	Rembrandta
<g/>
,	,	kIx,	,
Jacob	Jacoba	k1gFnPc2	Jacoba
van	vana	k1gFnPc2	vana
Ruisdael	Ruisdael	k1gInSc1	Ruisdael
<g/>
,	,	kIx,	,
Adriaen	Adriaen	k2eAgInSc1d1	Adriaen
Van	van	k1gInSc1	van
Ostade	Ostad	k1gInSc5	Ostad
<g/>
,	,	kIx,	,
Anthonis	Anthonis	k1gFnPc3	Anthonis
van	vana	k1gFnPc2	vana
Dyck	Dycka	k1gFnPc2	Dycka
<g/>
,	,	kIx,	,
Claude	Claud	k1gInSc5	Claud
Lorrain	Lorraina	k1gFnPc2	Lorraina
<g/>
,	,	kIx,	,
Antoine	Antoin	k1gInSc5	Antoin
Watteau	Wattea	k1gMnSc6	Wattea
<g/>
,	,	kIx,	,
Jean-Honoré	Jean-Honorý	k2eAgNnSc1d1	Jean-Honorý
Fragonard	Fragonard	k1gInSc4	Fragonard
nebo	nebo	k8xC	nebo
Hubert	Hubert	k1gMnSc1	Hubert
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
rytinami	rytina	k1gFnPc7	rytina
jsou	být	k5eAaImIp3nP	být
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
Martin	Martin	k1gMnSc1	Martin
Schongauer	Schongauer	k1gMnSc1	Schongauer
<g/>
,	,	kIx,	,
Albrecht	Albrecht	k1gMnSc1	Albrecht
Dürer	Dürer	k1gMnSc1	Dürer
<g/>
,	,	kIx,	,
Lucas	Lucas	k1gMnSc1	Lucas
van	vana	k1gFnPc2	vana
Leyden	Leydna	k1gFnPc2	Leydna
<g/>
,	,	kIx,	,
Rembrandt	Rembrandta	k1gFnPc2	Rembrandta
(	(	kIx(	(
<g/>
350	[number]	k4	350
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anthonis	Anthonis	k1gInSc1	Anthonis
van	vana	k1gFnPc2	vana
Dyck	Dycka	k1gFnPc2	Dycka
<g/>
,	,	kIx,	,
Jean-Honoré	Jean-Honorý	k2eAgFnPc1d1	Jean-Honorý
Fragonard	Fragonard	k1gInSc4	Fragonard
a	a	k8xC	a
Antoine	Antoin	k1gInSc5	Antoin
Watteau	Wattea	k2eAgFnSc4d1	Wattea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Petit	petit	k1gInSc1	petit
Palais	Palais	k1gFnSc2	Palais
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Petit	petit	k1gInSc1	petit
Palais	Palais	k1gInSc4	Palais
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
evidenci	evidence	k1gFnSc6	evidence
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
</s>
</p>
