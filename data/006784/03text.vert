<s>
Reggae	Reggae	k1gInSc1	Reggae
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
nejstaršími	starý	k2eAgMnPc7d3	nejstarší
předchůdci	předchůdce	k1gMnPc7	předchůdce
je	být	k5eAaImIp3nS	být
ska	ska	k?	ska
a	a	k8xC	a
rocksteady	rocksteada	k1gFnSc2	rocksteada
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
styly	styl	k1gInPc1	styl
spjaté	spjatý	k2eAgInPc1d1	spjatý
s	s	k7c7	s
americkým	americký	k2eAgNnSc7d1	americké
rhythm	rhyth	k1gNnSc7	rhyth
and	and	k?	and
blues	blues	k1gFnSc2	blues
<g/>
,	,	kIx,	,
jazzem	jazz	k1gInSc7	jazz
a	a	k8xC	a
soulem	soul	k1gInSc7	soul
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
zřídka	zřídka	k6eAd1	zřídka
kdy	kdy	k6eAd1	kdy
objevovala	objevovat	k5eAaImAgFnS	objevovat
rastafariánská	rastafariánský	k2eAgFnSc1d1	rastafariánská
tematika	tematika	k1gFnSc1	tematika
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přišlo	přijít	k5eAaPmAgNnS	přijít
až	až	k6eAd1	až
začátkem	začátkem	k7c2	začátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
texty	text	k1gInPc1	text
týkaly	týkat	k5eAaImAgInP	týkat
zábavy	zábav	k1gInPc4	zábav
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
kritika	kritika	k1gFnSc1	kritika
na	na	k7c4	na
sociální	sociální	k2eAgFnSc4d1	sociální
situaci	situace	k1gFnSc4	situace
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
a	a	k8xC	a
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgFnP	týkat
výtržností	výtržnost	k1gFnSc7	výtržnost
páchaných	páchaný	k2eAgFnPc2d1	páchaná
rude	rude	k1gNnPc2	rude
boys	boy	k1gMnPc1	boy
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
hlavními	hlavní	k2eAgMnPc7d1	hlavní
přívrženci	přívrženec	k1gMnPc1	přívrženec
této	tento	k3xDgFnSc2	tento
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Reggae	Reggae	k6eAd1	Reggae
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vystopovat	vystopovat	k5eAaPmF	vystopovat
až	až	k9	až
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
rock	rock	k1gInSc4	rock
steady	steada	k1gFnSc2	steada
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
novým	nový	k2eAgInSc7d1	nový
rychlejším	rychlý	k2eAgInSc7d2	rychlejší
a	a	k8xC	a
intenzivnějším	intenzivní	k2eAgInSc7d2	intenzivnější
stylem	styl	k1gInSc7	styl
<g/>
:	:	kIx,	:
Reggae	Reggae	k1gInSc1	Reggae
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
vzniku	vznik	k1gInSc2	vznik
reggae	reggae	k6eAd1	reggae
byla	být	k5eAaImAgFnS	být
nejspíš	nejspíš	k9	nejspíš
rivalita	rivalita	k1gFnSc1	rivalita
mezi	mezi	k7c7	mezi
malými	malý	k2eAgMnPc7d1	malý
producenty	producent	k1gMnPc7	producent
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
Bunny	Bunn	k1gInPc4	Bunn
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Leslie	Leslie	k1gFnPc1	Leslie
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
<g/>
Lee	Lea	k1gFnSc3	Lea
Scratch	Scratcha	k1gFnPc2	Scratcha
Perry	Perra	k1gFnSc2	Perra
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
svrhnout	svrhnout	k5eAaPmF	svrhnout
vynalézáním	vynalézání	k1gNnSc7	vynalézání
nových	nový	k2eAgInPc2d1	nový
hudebních	hudební	k2eAgInPc2d1	hudební
postupů	postup	k1gInPc2	postup
hlavní	hlavní	k2eAgMnPc1d1	hlavní
producenty	producent	k1gMnPc4	producent
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Coxone	Coxon	k1gInSc5	Coxon
Dodda	Dodd	k1gMnSc4	Dodd
a	a	k8xC	a
Dueka	Dueek	k1gMnSc4	Dueek
Rieda	Ried	k1gMnSc4	Ried
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
měli	mít	k5eAaImAgMnP	mít
"	"	kIx"	"
<g/>
monopolní	monopolní	k2eAgNnSc4d1	monopolní
<g/>
"	"	kIx"	"
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
jamajském	jamajský	k2eAgInSc6d1	jamajský
hudebním	hudební	k2eAgInSc6d1	hudební
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgInSc1d1	prvotní
reggae	reggae	k1gInSc1	reggae
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
zvané	zvaný	k2eAgMnPc4d1	zvaný
early	earl	k1gMnPc4	earl
reggae	reggae	k1gFnPc2	reggae
<g/>
)	)	kIx)	)
nebylo	být	k5eNaImAgNnS	být
tak	tak	k6eAd1	tak
pomalé	pomalý	k2eAgNnSc1d1	pomalé
jako	jako	k8xC	jako
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
nesly	nést	k5eAaImAgInP	nést
stále	stále	k6eAd1	stále
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
spíše	spíše	k9	spíše
zábavy	zábava	k1gFnSc2	zábava
<g/>
,	,	kIx,	,
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
nějaká	nějaký	k3yIgFnSc1	nějaký
sociální	sociální	k2eAgFnSc1d1	sociální
kritika	kritika	k1gFnSc1	kritika
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
jamajské	jamajský	k2eAgFnSc2d1	jamajská
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
neustálé	neustálý	k2eAgNnSc1d1	neustálé
téma	téma	k1gNnSc1	téma
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
rude	rude	k6eAd1	rude
boys	boy	k1gMnPc2	boy
a	a	k8xC	a
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
využívaly	využívat	k5eAaImAgFnP	využívat
jen	jen	k9	jen
instrumentální	instrumentální	k2eAgFnPc1d1	instrumentální
verze	verze	k1gFnPc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
hlavní	hlavní	k2eAgFnSc7d1	hlavní
interprety	interpret	k1gMnPc7	interpret
prvotního	prvotní	k2eAgNnSc2d1	prvotní
reggae	reggae	k1gNnSc2	reggae
patřily	patřit	k5eAaImAgInP	patřit
Desmond	Desmond	k1gInSc4	Desmond
Dekker	Dekkra	k1gFnPc2	Dekkra
<g/>
,	,	kIx,	,
Toots	Tootsa	k1gFnPc2	Tootsa
and	and	k?	and
the	the	k?	the
Maytals	Maytalsa	k1gFnPc2	Maytalsa
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
svým	svůj	k3xOyFgMnPc3	svůj
rocksteady	rocksteada	k1gFnSc2	rocksteada
hitem	hit	k1gInSc7	hit
Do	do	k7c2	do
the	the	k?	the
Reggay	Reggaa	k1gFnSc2	Reggaa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
reggae	reggae	k6eAd1	reggae
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
ale	ale	k8xC	ale
také	také	k6eAd1	také
Upsetters	Upsettersa	k1gFnPc2	Upsettersa
Lee	Lea	k1gFnSc3	Lea
Perryho	Perry	k1gMnSc2	Perry
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Harry	Harra	k1gFnPc1	Harra
J	J	kA	J
all	all	k?	all
stars	stars	k1gInSc1	stars
<g/>
,	,	kIx,	,
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Symarip	Symarip	k1gMnSc1	Symarip
<g/>
,	,	kIx,	,
Laurel	Laurel	k1gMnSc1	Laurel
Aitken	Aitken	k1gInSc1	Aitken
<g/>
,	,	kIx,	,
King	King	k1gMnSc1	King
Stitt	Stitt	k1gMnSc1	Stitt
<g/>
,	,	kIx,	,
Derrik	Derrik	k1gMnSc1	Derrik
Morgan	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnSc1	Joe
the	the	k?	the
Boss	boss	k1gMnSc1	boss
a	a	k8xC	a
i	i	k9	i
Jimmy	Jimm	k1gInPc1	Jimm
Cliff	Cliff	k1gInSc1	Cliff
a	a	k8xC	a
Pioneers	Pioneers	k1gInSc1	Pioneers
<g/>
.	.	kIx.	.
</s>
<s>
Rastafariánství	Rastafariánství	k1gNnSc1	Rastafariánství
tehdy	tehdy	k6eAd1	tehdy
začalo	začít	k5eAaPmAgNnS	začít
sílit	sílit	k5eAaImF	sílit
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
příznivců	příznivec	k1gMnPc2	příznivec
také	také	k9	také
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
hudebníků	hudebník	k1gMnPc2	hudebník
hrajících	hrající	k2eAgFnPc2d1	hrající
reggae	reggae	k1gFnPc2	reggae
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
nebylo	být	k5eNaImAgNnS	být
pouhou	pouhý	k2eAgFnSc7d1	pouhá
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
sektou	sekta	k1gFnSc7	sekta
ukrývající	ukrývající	k2eAgFnSc7d1	ukrývající
se	se	k3xPyFc4	se
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
za	za	k7c7	za
Kingstonem	Kingston	k1gInSc7	Kingston
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rostl	růst	k5eAaImAgInS	růst
jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
městské	městský	k2eAgNnSc4d1	Městské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
začaly	začít	k5eAaPmAgFnP	začít
touto	tento	k3xDgFnSc7	tento
naukou	nauka	k1gFnSc7	nauka
inspirované	inspirovaný	k2eAgInPc1d1	inspirovaný
texty	text	k1gInPc1	text
v	v	k7c6	v
reggae	reggae	k1gFnSc6	reggae
převažovat	převažovat	k5eAaImF	převažovat
<g/>
,	,	kIx,	,
zpěváci	zpěvák	k1gMnPc1	zpěvák
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zabývali	zabývat	k5eAaImAgMnP	zabývat
myšlenkami	myšlenka	k1gFnPc7	myšlenka
Marcuse	Marcuse	k1gFnSc1	Marcuse
Garveye	Garveye	k1gInSc1	Garveye
(	(	kIx(	(
<g/>
zakladatel	zakladatel	k1gMnSc1	zakladatel
této	tento	k3xDgFnSc2	tento
víry	víra	k1gFnSc2	víra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nápravou	náprava	k1gFnSc7	náprava
Babylonu	babylon	k1gInSc2	babylon
<g/>
,	,	kIx,	,
návratem	návrat	k1gInSc7	návrat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
pravlasti	pravlast	k1gFnSc2	pravlast
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
opojením	opojení	k1gNnSc7	opojení
marihuanou	marihuana	k1gFnSc7	marihuana
(	(	kIx(	(
<g/>
ganja	ganja	k1gFnSc1	ganja
<g/>
,	,	kIx,	,
sensemilia	sensemilia	k1gFnSc1	sensemilia
<g/>
,	,	kIx,	,
herb	herb	k1gMnSc1	herb
<g/>
,	,	kIx,	,
kaya	kaya	k1gMnSc1	kaya
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
rastafariánů-zpěváků	rastafariánůpěvák	k1gMnPc2	rastafariánů-zpěvák
a	a	k8xC	a
dýdžejů	dýdžej	k1gMnPc2	dýdžej
(	(	kIx(	(
<g/>
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k9	jako
MCs	MCs	k1gFnSc1	MCs
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
používali	používat	k5eAaImAgMnP	používat
jako	jako	k8xC	jako
podklad	podklad	k1gInSc4	podklad
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
textům	text	k1gInPc3	text
osvědčené	osvědčený	k2eAgFnPc1d1	osvědčená
rock	rock	k1gInSc4	rock
steady	steada	k1gFnSc2	steada
hity	hit	k1gInPc4	hit
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
využíval	využívat	k5eAaPmAgMnS	využívat
hlavně	hlavně	k9	hlavně
D.J.	D.J.	k1gMnSc1	D.J.
U-Roy	U-Roa	k1gMnSc2	U-Roa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
třeba	třeba	k6eAd1	třeba
od	od	k7c2	od
Boba	Bob	k1gMnSc2	Bob
Marleyho	Marley	k1gMnSc2	Marley
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
věnoval	věnovat	k5eAaPmAgMnS	věnovat
nově	nově	k6eAd1	nově
vznikajícímu	vznikající	k2eAgNnSc3d1	vznikající
reggae	reggae	k1gNnSc3	reggae
<g/>
,	,	kIx,	,
vsadil	vsadit	k5eAaPmAgMnS	vsadit
na	na	k7c4	na
staré	starý	k2eAgInPc4d1	starý
osvědčené	osvědčený	k2eAgInPc4d1	osvědčený
hity	hit	k1gInPc4	hit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
po	po	k7c4	po
mnohá	mnohý	k2eAgNnPc4d1	mnohé
další	další	k2eAgNnPc4d1	další
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Rastafariáni	Rastafarián	k1gMnPc1	Rastafarián
také	také	k9	také
do	do	k7c2	do
reggae	reggae	k1gNnSc2	reggae
vnesli	vnést	k5eAaPmAgMnP	vnést
další	další	k2eAgInSc4d1	další
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gInSc4	on
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
<g/>
,	,	kIx,	,
bubnování	bubnování	k1gNnSc3	bubnování
nyabinghy	nyabingha	k1gFnSc2	nyabingha
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
změnou	změna	k1gFnSc7	změna
v	v	k7c6	v
reggae	reggae	k1gFnSc6	reggae
bylo	být	k5eAaImAgNnS	být
častější	častý	k2eAgNnSc1d2	častější
využívání	využívání	k1gNnSc1	využívání
vokálů	vokál	k1gInPc2	vokál
a	a	k8xC	a
přizvukujících	přizvukující	k2eAgInPc2d1	přizvukující
dechů	dech	k1gInPc2	dech
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c4	v
rock	rock	k1gInSc4	rock
steady	steada	k1gFnSc2	steada
<g/>
.	.	kIx.	.
</s>
<s>
Rock	rock	k1gInSc1	rock
steady	steada	k1gFnSc2	steada
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
reggae	regga	k1gMnSc4	regga
stalo	stát	k5eAaPmAgNnS	stát
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
vzorem	vzor	k1gInSc7	vzor
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
předělávat	předělávat	k5eAaImF	předělávat
staré	starý	k2eAgInPc4d1	starý
hity	hit	k1gInPc4	hit
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
s	s	k7c7	s
rasta	rasta	k1gFnSc1	rasta
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roots	roots	k6eAd1	roots
reggae	regga	k1gInPc4	regga
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
proslavili	proslavit	k5eAaPmAgMnP	proslavit
hudebníci	hudebník	k1gMnPc1	hudebník
jako	jako	k8xS	jako
Dennis	Dennis	k1gFnPc1	Dennis
Brown	Brown	k1gInSc1	Brown
<g/>
,	,	kIx,	,
Burning	Burning	k1gInSc1	Burning
Spear	Speara	k1gFnPc2	Speara
<g/>
,	,	kIx,	,
I-Roy	I-Roa	k1gFnSc2	I-Roa
<g/>
,	,	kIx,	,
Third	Third	k1gMnSc1	Third
World	World	k1gMnSc1	World
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Uhuru	Uhur	k1gInSc2	Uhur
<g/>
,	,	kIx,	,
Gregory	Gregor	k1gMnPc4	Gregor
Isaacs	Isaacsa	k1gFnPc2	Isaacsa
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Holt	Holt	k?	Holt
<g/>
,	,	kIx,	,
Big	Big	k1gMnSc1	Big
Youth	Youth	k1gMnSc1	Youth
<g/>
,	,	kIx,	,
Culture	Cultur	k1gMnSc5	Cultur
<g/>
,	,	kIx,	,
Israel	Israel	k1gInSc1	Israel
Vibration	Vibration	k1gInSc1	Vibration
<g/>
,	,	kIx,	,
Horace	Horace	k1gFnSc1	Horace
Andy	Anda	k1gFnSc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
největší	veliký	k2eAgFnSc7d3	veliký
hvězdou	hvězda	k1gFnSc7	hvězda
byl	být	k5eAaImAgInS	být
bezesporu	bezesporu	k9	bezesporu
Bob	bob	k1gInSc1	bob
Marley	Marlea	k1gMnSc2	Marlea
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
působil	působit	k5eAaImAgMnS	působit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Peterem	Peter	k1gMnSc7	Peter
Toshem	Tosh	k1gInSc7	Tosh
a	a	k8xC	a
Bunny	Bunen	k2eAgInPc1d1	Bunen
Livingstonem	Livingston	k1gInSc7	Livingston
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Wailers	Wailersa	k1gFnPc2	Wailersa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
reggae	reggae	k6eAd1	reggae
proslavila	proslavit	k5eAaPmAgFnS	proslavit
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Wailers	Wailers	k1gInSc1	Wailers
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
přejmenovali	přejmenovat	k5eAaPmAgMnP	přejmenovat
na	na	k7c4	na
Bob	bob	k1gInSc4	bob
Marley	Marlea	k1gFnSc2	Marlea
and	and	k?	and
the	the	k?	the
Wailers	Wailers	k1gInSc1	Wailers
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
Peter	Peter	k1gMnSc1	Peter
a	a	k8xC	a
Bunny	Bunn	k1gInPc4	Bunn
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
kapelu	kapela	k1gFnSc4	kapela
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
je	on	k3xPp3gNnSc4	on
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
vokální	vokální	k2eAgNnSc1d1	vokální
trio	trio	k1gNnSc1	trio
I	i	k9	i
Threes	Threes	k1gInSc1	Threes
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
s	s	k7c7	s
Bobem	bob	k1gInSc7	bob
vydrželi	vydržet	k5eAaPmAgMnP	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Tosh	Tosh	k1gMnSc1	Tosh
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
desku	deska	k1gFnSc4	deska
Legalize	Legalize	k1gFnSc2	Legalize
It	It	k1gFnSc2	It
se	s	k7c7	s
stejnojmenným	stejnojmenný	k2eAgInSc7d1	stejnojmenný
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
nesměl	smět	k5eNaImAgMnS	smět
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Roots	Roots	k6eAd1	Roots
reggae	reggae	k6eAd1	reggae
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
také	také	k9	také
u	u	k7c2	u
punkerů	punker	k1gMnPc2	punker
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
líbilo	líbit	k5eAaImAgNnS	líbit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
hudba	hudba	k1gFnSc1	hudba
staví	stavit	k5eAaPmIp3nS	stavit
k	k	k7c3	k
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
ke	k	k7c3	k
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
válkám	válka	k1gFnPc3	válka
<g/>
,	,	kIx,	,
politikům	politik	k1gMnPc3	politik
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
Marley	Marlea	k1gFnSc2	Marlea
dokonce	dokonce	k9	dokonce
natočil	natočit	k5eAaBmAgMnS	natočit
s	s	k7c7	s
britskou	britský	k2eAgFnSc7d1	britská
reggae	reggae	k1gFnSc7	reggae
kapelou	kapela	k1gFnSc7	kapela
Aswad	Aswad	k1gInSc1	Aswad
písničku	písnička	k1gFnSc4	písnička
Punky	punk	k1gInPc4	punk
Reggae	Reggae	k1gNnSc2	Reggae
Party	parta	k1gFnSc2	parta
a	a	k8xC	a
The	The	k1gFnSc2	The
Clash	Clasha	k1gFnPc2	Clasha
předělali	předělat	k5eAaPmAgMnP	předělat
hit	hit	k1gInSc4	hit
Juniora	junior	k1gMnSc2	junior
Murvina	Murvin	k2eAgFnSc1d1	Murvin
Police	police	k1gFnSc1	police
and	and	k?	and
Thiev	Thiva	k1gFnPc2	Thiva
<g/>
.	.	kIx.	.
</s>
<s>
Dub	dub	k1gInSc1	dub
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
jako	jako	k8xC	jako
reggae	reggae	k1gNnSc7	reggae
<g/>
,	,	kIx,	,
úplnou	úplný	k2eAgFnSc7d1	úplná
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
King	King	k1gMnSc1	King
Tubby	Tubba	k1gFnSc2	Tubba
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
pro	pro	k7c4	pro
Coxona	Coxon	k1gMnSc4	Coxon
Dodda	Dodd	k1gMnSc4	Dodd
nemohl	moct	k5eNaImAgInS	moct
zakomponovat	zakomponovat	k5eAaPmF	zakomponovat
zpěvákův	zpěvákův	k2eAgInSc1d1	zpěvákův
hlas	hlas	k1gInSc1	hlas
do	do	k7c2	do
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nahrála	nahrát	k5eAaBmAgFnS	nahrát
bez	bez	k7c2	bez
vokálů	vokál	k1gInPc2	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
sound	sounda	k1gFnPc2	sounda
systémů	systém	k1gInPc2	systém
měla	mít	k5eAaImAgFnS	mít
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Tubby	Tubba	k1gFnSc2	Tubba
rozhodnul	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
zvukově	zvukově	k6eAd1	zvukově
upravovat	upravovat	k5eAaImF	upravovat
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
verze	verze	k1gFnSc2	verze
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
omezil	omezit	k5eAaPmAgMnS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
bicí	bicí	k2eAgFnSc4d1	bicí
a	a	k8xC	a
basu	basa	k1gFnSc4	basa
<g/>
,	,	kIx,	,
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přidávaly	přidávat	k5eAaImAgFnP	přidávat
i	i	k9	i
složité	složitý	k2eAgInPc4d1	složitý
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
syntezátory	syntezátor	k1gInPc4	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
okamžitě	okamžitě	k6eAd1	okamžitě
využili	využít	k5eAaPmAgMnP	využít
dýdžejové	dýdžej	k1gMnPc1	dýdžej
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mohli	moct	k5eAaImAgMnP	moct
pouštět	pouštět	k5eAaImF	pouštět
hudbu	hudba	k1gFnSc4	hudba
bez	bez	k7c2	bez
přestávky	přestávka	k1gFnSc2	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
se	se	k3xPyFc4	se
dubu	dub	k1gInSc2	dub
říkalo	říkat	k5eAaImAgNnS	říkat
jenom	jenom	k9	jenom
version	version	k1gInSc4	version
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
na	na	k7c6	na
B	B	kA	B
stranách	strana	k1gFnPc6	strana
singlů	singl	k1gInPc2	singl
jako	jako	k9	jako
jejich	jejich	k3xOp3gFnSc1	jejich
upravená	upravený	k2eAgFnSc1d1	upravená
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
King	King	k1gMnSc1	King
Tubby	Tubba	k1gFnSc2	Tubba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
opravdovým	opravdový	k2eAgMnSc7d1	opravdový
králem	král	k1gMnSc7	král
této	tento	k3xDgFnSc2	tento
odnože	odnož	k1gFnSc2	odnož
reggae	regga	k1gFnSc2	regga
muziky	muzika	k1gFnSc2	muzika
<g/>
.	.	kIx.	.
</s>
<s>
Dub	dub	k1gInSc1	dub
byl	být	k5eAaImAgInS	být
populární	populární	k2eAgMnSc1d1	populární
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
i	i	k9	i
Lee	Lea	k1gFnSc3	Lea
Scratch	Scratch	k1gInSc4	Scratch
Perry	Perra	k1gFnSc2	Perra
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiné	k1gNnPc7	jiné
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
také	také	k9	také
Mad	Mad	k1gMnPc1	Mad
Professor	Professor	k1gInSc4	Professor
<g/>
,	,	kIx,	,
Augustus	Augustus	k1gInSc4	Augustus
Pablo	Pablo	k1gNnSc4	Pablo
<g/>
,	,	kIx,	,
Jah	Jah	k1gMnSc1	Jah
Shakka	Shakka	k1gMnSc1	Shakka
a	a	k8xC	a
Dub	Dub	k1gMnSc1	Dub
Syndicate	Syndicat	k1gMnSc5	Syndicat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
1978	[number]	k4	1978
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
teror	teror	k1gInSc4	teror
a	a	k8xC	a
pouliční	pouliční	k2eAgInPc4d1	pouliční
nepokoje	nepokoj	k1gInPc4	nepokoj
<g/>
,	,	kIx,	,
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
vláda	vláda	k1gFnSc1	vláda
omezila	omezit	k5eAaPmAgFnS	omezit
sociální	sociální	k2eAgFnPc4d1	sociální
dávky	dávka	k1gFnPc4	dávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
promítlo	promítnout	k5eAaPmAgNnS	promítnout
v	v	k7c6	v
náladě	nálada	k1gFnSc6	nálada
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
zpěváků	zpěvák	k1gMnPc2	zpěvák
a	a	k8xC	a
dýdžejů	dýdžej	k1gMnPc2	dýdžej
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
už	už	k6eAd1	už
nebyli	být	k5eNaImAgMnP	být
zvědaví	zvědavý	k2eAgMnPc1d1	zvědavý
na	na	k7c4	na
optimistické	optimistický	k2eAgNnSc4d1	optimistické
rasta	rasta	k1gFnSc1	rasta
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
neměli	mít	k5eNaImAgMnP	mít
náladu	nálada	k1gFnSc4	nálada
na	na	k7c4	na
písně	píseň	k1gFnPc4	píseň
o	o	k7c6	o
jednotné	jednotný	k2eAgFnSc6d1	jednotná
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
pravlasti	pravlast	k1gFnSc2	pravlast
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
si	se	k3xPyFc3	se
užívat	užívat	k5eAaImF	užívat
teď	teď	k6eAd1	teď
a	a	k8xC	a
právě	právě	k6eAd1	právě
tady	tady	k6eAd1	tady
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
parketu	parket	k1gInSc6	parket
(	(	kIx(	(
<g/>
dancehall	dancehall	k1gInSc1	dancehall
-	-	kIx~	-
taneční	taneční	k2eAgInSc1d1	taneční
sál	sál	k1gInSc1	sál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomalu	pomalu	k6eAd1	pomalu
odcházející	odcházející	k2eAgFnSc1d1	odcházející
"	"	kIx"	"
<g/>
dobrá	dobrý	k2eAgFnSc1d1	dobrá
nálada	nálada	k1gFnSc1	nálada
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
zpěváci	zpěvák	k1gMnPc1	zpěvák
(	(	kIx(	(
<g/>
at	at	k?	at
<g/>
́	́	k?	́
už	už	k6eAd1	už
byli	být	k5eAaImAgMnP	být
rastamani	rastaman	k1gMnPc1	rastaman
či	či	k8xC	či
ne	ne	k9	ne
<g/>
)	)	kIx)	)
začali	začít	k5eAaPmAgMnP	začít
zabývat	zabývat	k5eAaImF	zabývat
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
více	hodně	k6eAd2	hodně
souvisely	souviset	k5eAaImAgFnP	souviset
s	s	k7c7	s
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
životem	život	k1gInSc7	život
v	v	k7c6	v
ghetu	gheto	k1gNnSc6	gheto
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
důležitější	důležitý	k2eAgMnSc1d2	důležitější
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
v	v	k7c6	v
tanečním	taneční	k2eAgInSc6d1	taneční
sále	sál	k1gInSc6	sál
než	než	k8xS	než
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
nebo	nebo	k8xC	nebo
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
sexistické	sexistický	k2eAgInPc4d1	sexistický
texty	text	k1gInPc4	text
a	a	k8xC	a
texty	text	k1gInPc4	text
týkající	týkající	k2eAgInPc4d1	týkající
se	s	k7c7	s
zbraní	zbraň	k1gFnSc7	zbraň
a	a	k8xC	a
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
na	na	k7c4	na
Jamajku	Jamajka	k1gFnSc4	Jamajka
začal	začít	k5eAaPmAgMnS	začít
pašovat	pašovat	k5eAaImF	pašovat
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
kokain	kokain	k1gInSc4	kokain
a	a	k8xC	a
i	i	k9	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
zpěváci	zpěvák	k1gMnPc1	zpěvák
a	a	k8xC	a
dýdžejové	dýdžej	k1gMnPc1	dýdžej
mu	on	k3xPp3gMnSc3	on
propadli	propadnout	k5eAaPmAgMnP	propadnout
(	(	kIx(	(
<g/>
Dennis	Dennis	k1gFnSc1	Dennis
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
Dillinger	Dillinger	k1gMnSc1	Dillinger
<g/>
,	,	kIx,	,
Gregory	Gregor	k1gMnPc4	Gregor
Isaacs	Isaacsa	k1gFnPc2	Isaacsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k8xC	jako
podklady	podklad	k1gInPc1	podklad
se	se	k3xPyFc4	se
využívaly	využívat	k5eAaImAgInP	využívat
klasické	klasický	k2eAgInPc1d1	klasický
dubové	dubový	k2eAgInPc1d1	dubový
a	a	k8xC	a
roots	roots	k6eAd1	roots
reggae	reggae	k6eAd1	reggae
rytmy	rytmus	k1gInPc1	rytmus
-	-	kIx~	-
riddim	riddim	k1gInSc1	riddim
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
rhythm	rhythm	k1gInSc1	rhythm
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
oblíbenější	oblíbený	k2eAgMnPc1d2	oblíbenější
byli	být	k5eAaImAgMnP	být
dýdžejové	dýdžej	k1gMnPc1	dýdžej
než	než	k8xS	než
zpěváci	zpěvák	k1gMnPc1	zpěvák
např.	např.	kA	např.
<g/>
:	:	kIx,	:
General	General	k1gMnSc2	General
Echo	echo	k1gNnSc4	echo
<g/>
,	,	kIx,	,
Sister	Sister	k1gInSc4	Sister
Nancy	Nancy	k1gFnSc2	Nancy
<g/>
,	,	kIx,	,
Nigger	Nigger	k1gMnSc1	Nigger
Kojak	Kojak	k1gMnSc1	Kojak
<g/>
,	,	kIx,	,
Barrington	Barrington	k1gInSc1	Barrington
Levy	Levy	k?	Levy
<g/>
,	,	kIx,	,
Yellowman	Yellowman	k1gMnSc1	Yellowman
<g/>
,	,	kIx,	,
Eek-a-Mouse	Eek-Mouse	k1gFnSc1	Eek-a-Mouse
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
spíš	spíš	k9	spíš
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
singjaye	singjaye	k1gFnSc4	singjaye
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
napůl	napůl	k6eAd1	napůl
zpívá	zpívat	k5eAaImIp3nS	zpívat
a	a	k8xC	a
toastuje	toastovat	k5eAaImIp3nS	toastovat
<g/>
.	.	kIx.	.
</s>
<s>
Dancehall	Dancehall	k1gMnSc1	Dancehall
dal	dát	k5eAaPmAgMnS	dát
prostor	prostor	k1gInSc4	prostor
i	i	k9	i
pro	pro	k7c4	pro
nové	nový	k2eAgMnPc4d1	nový
producenty	producent	k1gMnPc4	producent
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnSc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Joe	Joe	k1gFnSc1	Joe
Gibs	Gibsa	k1gFnPc2	Gibsa
a	a	k8xC	a
King	Kinga	k1gFnPc2	Kinga
Jammy	Jamma	k1gFnSc2	Jamma
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyprodukoval	vyprodukovat	k5eAaPmAgInS	vyprodukovat
většinu	většina	k1gFnSc4	většina
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
<s>
Rastafariánské	Rastafariánský	k2eAgInPc1d1	Rastafariánský
texty	text	k1gInPc1	text
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nevymizely	vymizet	k5eNaPmAgInP	vymizet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
méně	málo	k6eAd2	málo
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
roots	roots	k6eAd1	roots
reggae	reggae	k6eAd1	reggae
kapely	kapela	k1gFnPc1	kapela
se	se	k3xPyFc4	se
prosadily	prosadit	k5eAaPmAgFnP	prosadit
i	i	k9	i
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Steel	Steel	k1gInSc1	Steel
Pulse	puls	k1gInSc5	puls
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Peter	Peter	k1gMnSc1	Peter
Tosh	Tosh	k1gMnSc1	Tosh
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
aktivní	aktivní	k2eAgMnSc1d1	aktivní
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
tragické	tragický	k2eAgFnSc2d1	tragická
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
umírá	umírat	k5eAaImIp3nS	umírat
Bob	Bob	k1gMnSc1	Bob
Marley	Marlea	k1gFnSc2	Marlea
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
hvězd	hvězda	k1gFnPc2	hvězda
reggae	reggaat	k5eAaPmIp3nS	reggaat
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
upadá	upadat	k5eAaPmIp3nS	upadat
do	do	k7c2	do
zapomnění	zapomnění	k1gNnSc2	zapomnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
dýdžejem	dýdžej	k1gInSc7	dýdžej
Yellowman	Yellowman	k1gMnSc1	Yellowman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zásadně	zásadně	k6eAd1	zásadně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
toasting	toasting	k1gInSc4	toasting
jako	jako	k8xC	jako
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
mu	on	k3xPp3gMnSc3	on
novou	nový	k2eAgFnSc4d1	nová
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
jako	jako	k9	jako
raggamuffin	raggamuffin	k1gMnSc1	raggamuffin
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
poněkud	poněkud	k6eAd1	poněkud
opadla	opadnout	k5eAaPmAgFnS	opadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
půli	půle	k1gFnSc6	půle
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dancehall	dancehall	k1gMnSc1	dancehall
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
hudebně	hudebně	k6eAd1	hudebně
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
od	od	k7c2	od
klasického	klasický	k2eAgInSc2d1	klasický
reggae	regga	k1gInSc2	regga
a	a	k8xC	a
dubu	dub	k1gInSc2	dub
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc1d2	veliký
důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c6	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
na	na	k7c4	na
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
písní	píseň	k1gFnPc2	píseň
Under	Undra	k1gFnPc2	Undra
Mi	já	k3xPp1nSc3	já
Sleng	Sleng	k1gMnSc1	Sleng
Teng	Teng	k1gInSc1	Teng
od	od	k7c2	od
Wayneho	Wayne	k1gMnSc2	Wayne
Smithse	Smiths	k1gMnSc2	Smiths
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
revoluční	revoluční	k2eAgInSc4d1	revoluční
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgInSc7	první
velkým	velký	k2eAgInSc7d1	velký
jamajským	jamajský	k2eAgInSc7d1	jamajský
hitem	hit	k1gInSc7	hit
vytvořeným	vytvořený	k2eAgInSc7d1	vytvořený
pouze	pouze	k6eAd1	pouze
pomocí	pomocí	k7c2	pomocí
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
syntezátorů	syntezátor	k1gInPc2	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
označit	označit	k5eAaPmF	označit
jako	jako	k9	jako
první	první	k4xOgInSc4	první
velký	velký	k2eAgInSc4d1	velký
ragga	ragg	k1gMnSc4	ragg
hit	hit	k1gInSc4	hit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dancehallu	dancehallo	k1gNnSc6	dancehallo
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
začalo	začít	k5eAaPmAgNnS	začít
více	hodně	k6eAd2	hodně
využívat	využívat	k5eAaPmF	využívat
ragga	ragga	k1gFnSc1	ragga
<g/>
,	,	kIx,	,
než	než	k8xS	než
klasické	klasický	k2eAgInPc1d1	klasický
duby	dub	k1gInPc1	dub
a	a	k8xC	a
reggae	regga	k1gInPc1	regga
riddimy	riddima	k1gFnSc2	riddima
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
užívat	užívat	k5eAaImF	užívat
označení	označení	k1gNnSc1	označení
ragga	ragg	k1gMnSc2	ragg
i	i	k9	i
pro	pro	k7c4	pro
dancehall	dancehall	k1gInSc4	dancehall
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
vynořila	vynořit	k5eAaPmAgFnS	vynořit
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
dýdžejů	dýdžej	k1gMnPc2	dýdžej
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
téměř	téměř	k6eAd1	téměř
neznámí	známý	k2eNgMnPc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
asi	asi	k9	asi
Shabba	Shabba	k1gFnSc1	Shabba
Ranks	Ranksa	k1gFnPc2	Ranksa
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Ninjaman	Ninjaman	k1gMnSc1	Ninjaman
a	a	k8xC	a
Supercat	Supercat	k1gMnSc1	Supercat
<g/>
.	.	kIx.	.
</s>
<s>
Digitalizace	digitalizace	k1gFnSc1	digitalizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
dancehall	dancehall	k1gInSc4	dancehall
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nevyhnula	vyhnout	k5eNaPmAgFnS	vyhnout
ani	ani	k8xC	ani
reggae	reggaat	k5eAaPmIp3nS	reggaat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
i	i	k9	i
reggae	reggae	k6eAd1	reggae
hrálo	hrát	k5eAaImAgNnS	hrát
také	také	k9	také
instrumentálně	instrumentálně	k6eAd1	instrumentálně
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ragga	ragg	k1gMnSc2	ragg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
populárním	populární	k2eAgMnSc7d1	populární
D-jayem	Day	k1gMnSc7	D-jay
Buju	Bujus	k1gInSc2	Bujus
Banton	Banton	k1gInSc4	Banton
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
hitem	hit	k1gInSc7	hit
boom	boom	k1gInSc4	boom
bye	bye	k?	bye
bye	bye	k?	bye
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přijal	přijmout	k5eAaPmAgMnS	přijmout
rastafariánství	rastafariánství	k1gNnSc4	rastafariánství
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
jinou	jiný	k2eAgFnSc7d1	jiná
tematikou	tematika	k1gFnSc7	tematika
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Devadesátá	devadesátý	k4xOgNnPc1	devadesátý
léta	léto	k1gNnPc1	léto
přinesla	přinést	k5eAaPmAgNnP	přinést
rasta	rasta	k1gMnSc1	rasta
texty	text	k1gInPc4	text
i	i	k8xC	i
do	do	k7c2	do
ragga	ragg	k1gMnSc2	ragg
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
měli	mít	k5eAaImAgMnP	mít
rastamani	rastaman	k1gMnPc1	rastaman
ze	z	k7c2	z
sekty	sekta	k1gFnSc2	sekta
Bobo	Bobo	k6eAd1	Bobo
Ashanti	Ashant	k1gMnPc1	Ashant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
desetiletích	desetiletí	k1gNnPc6	desetiletí
vůbec	vůbec	k9	vůbec
reggae	reggae	k6eAd1	reggae
nezabývali	zabývat	k5eNaImAgMnP	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
hlavně	hlavně	k9	hlavně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
zakladatel	zakladatel	k1gMnSc1	zakladatel
této	tento	k3xDgFnSc2	tento
sekty	sekta	k1gFnSc2	sekta
Prince	princ	k1gMnSc2	princ
Emanuel	Emanuel	k1gMnSc1	Emanuel
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
reggae	reggae	k6eAd1	reggae
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
babylon	babylon	k1gInSc1	babylon
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Bobo	Bobo	k6eAd1	Bobo
dreadi	dread	k1gMnPc1	dread
zabývali	zabývat	k5eAaImAgMnP	zabývat
jen	jen	k9	jen
nyabinghy	nyabingha	k1gFnSc2	nyabingha
bubnováním	bubnování	k1gNnSc7	bubnování
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
takovým	takový	k3xDgInSc7	takový
známým	známý	k2eAgMnSc7d1	známý
Bobo	Bobo	k1gMnSc1	Bobo
dreadem	dread	k1gInSc7	dread
v	v	k7c4	v
ragga	ragg	k1gMnSc4	ragg
byl	být	k5eAaImAgInS	být
Capleton	Capleton	k1gInSc1	Capleton
<g/>
,	,	kIx,	,
<g/>
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
zabýval	zabývat	k5eAaImAgInS	zabývat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bounty	Bount	k1gMnPc7	Bount
Killerem	Killer	k1gMnSc7	Killer
hip	hip	k0	hip
hopem	hopem	k?	hopem
a	a	k8xC	a
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
na	na	k7c6	na
nějakém	nějaký	k3yIgNnSc6	nějaký
tom	ten	k3xDgNnSc6	ten
hip	hip	k0	hip
hopovém	hopový	k2eAgNnSc6d1	hopové
albu	album	k1gNnSc6	album
jako	jako	k9	jako
hosté	host	k1gMnPc1	host
<g/>
.	.	kIx.	.
</s>
<s>
Spojování	spojování	k1gNnSc1	spojování
raggamuffinu	raggamuffinout	k5eAaPmIp1nS	raggamuffinout
s	s	k7c7	s
hip	hip	k0	hip
hopem	hopem	k?	hopem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
<g/>
,	,	kIx,	,
také	také	k9	také
se	se	k3xPyFc4	se
ragga	ragga	k1gFnSc1	ragga
vokály	vokál	k1gInPc1	vokál
začaly	začít	k5eAaPmAgInP	začít
využívat	využívat	k5eAaImF	využívat
hojně	hojně	k6eAd1	hojně
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
nově	nově	k6eAd1	nově
vznikajícím	vznikající	k2eAgInSc6d1	vznikající
stylu	styl	k1gInSc6	styl
<g/>
:	:	kIx,	:
Jungle	Junglo	k1gNnSc6	Junglo
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
interpretem	interpret	k1gMnSc7	interpret
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
hity	hit	k1gInPc1	hit
se	se	k3xPyFc4	se
v	v	k7c6	v
junglu	jungl	k1gInSc6	jungl
využívaly	využívat	k5eAaPmAgFnP	využívat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
General	General	k1gMnSc1	General
Levy	Levy	k?	Levy
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nekorunovaným	korunovaný	k2eNgMnSc7d1	nekorunovaný
králem	král	k1gMnSc7	král
junglu	junglat	k5eAaPmIp1nS	junglat
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
ragga	ragga	k1gFnSc1	ragga
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zdát	zdát	k5eAaImF	zdát
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
roots	roots	k6eAd1	roots
reggae	reggaat	k5eAaPmIp3nS	reggaat
už	už	k6eAd1	už
nadobro	nadobro	k6eAd1	nadobro
odzvonilo	odzvonit	k5eAaPmAgNnS	odzvonit
ale	ale	k9	ale
i	i	k9	i
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vynořila	vynořit	k5eAaPmAgFnS	vynořit
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
zpěváků	zpěvák	k1gMnPc2	zpěvák
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
<g/>
Evreton	Evreton	k1gInSc1	Evreton
Blender	Blender	k1gInSc1	Blender
<g/>
,	,	kIx,	,
<g/>
Tony	Tony	k1gMnSc1	Tony
Rebel	rebel	k1gMnSc1	rebel
<g/>
,	,	kIx,	,
<g/>
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
,	,	kIx,	,
<g/>
Yammi	Yam	k1gFnPc7	Yam
Bolo	bola	k1gFnSc5	bola
a	a	k8xC	a
stále	stále	k6eAd1	stále
populární	populární	k2eAgMnSc1d1	populární
byl	být	k5eAaImAgInS	být
také	také	k9	také
Cocoa	Coco	k2eAgFnSc1d1	Cocoa
tea	tea	k1gFnSc1	tea
<g/>
,	,	kIx,	,
<g/>
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
na	na	k7c6	na
pódiích	pódium	k1gNnPc6	pódium
objevil	objevit	k5eAaPmAgMnS	objevit
veterán	veterán	k1gMnSc1	veterán
ze	z	k7c2	z
zlaté	zlatý	k2eAgFnSc2d1	zlatá
éry	éra	k1gFnSc2	éra
roots	roots	k6eAd1	roots
reggae	reggaat	k5eAaPmIp3nS	reggaat
Horace	Horace	k1gFnSc1	Horace
Andy	Anda	k1gFnSc2	Anda
<g/>
,	,	kIx,	,
<g/>
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
britskými	britský	k2eAgMnPc7d1	britský
Massive	Massiev	k1gFnPc4	Massiev
attack	attack	k1gInSc1	attack
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
Dennis	Dennis	k1gInSc4	Dennis
Brown	Browno	k1gNnPc2	Browno
a	a	k8xC	a
Gregory	Gregor	k1gMnPc7	Gregor
Isaacs	Isaacsa	k1gFnPc2	Isaacsa
stále	stále	k6eAd1	stále
udávali	udávat	k5eAaImAgMnP	udávat
směr	směr	k1gInSc4	směr
v	v	k7c4	v
roots	roots	k1gInSc4	roots
reggae	regga	k1gFnSc2	regga
ale	ale	k8xC	ale
Dennis	Dennis	k1gFnSc2	Dennis
Brown	Browno	k1gNnPc2	Browno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
Gregory	Gregor	k1gMnPc4	Gregor
Isaacs	Isaacs	k1gInSc4	Isaacs
se	se	k3xPyFc4	se
odmlčel	odmlčet	k5eAaPmAgMnS	odmlčet
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
reggae	reggae	k1gNnSc1	reggae
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
propojeností	propojenost	k1gFnSc7	propojenost
s	s	k7c7	s
dancehallem	dancehall	k1gInSc7	dancehall
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hodně	hodně	k6eAd1	hodně
interpretů	interpret	k1gMnPc2	interpret
především	především	k9	především
ze	z	k7c2	z
sekty	sekta	k1gFnSc2	sekta
Bobo	Bobo	k6eAd1	Bobo
Ashanti	Ashant	k1gMnPc1	Ashant
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
jak	jak	k8xS	jak
dancehallu	dancehallat	k5eAaPmIp1nS	dancehallat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
roots	roots	k6eAd1	roots
reggae	reggaat	k5eAaPmIp3nS	reggaat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
asi	asi	k9	asi
Anthony	Anthona	k1gFnPc4	Anthona
B	B	kA	B
<g/>
,	,	kIx,	,
Sizzla	Sizzla	k1gFnSc1	Sizzla
a	a	k8xC	a
Capleton	Capleton	k1gInSc1	Capleton
<g/>
.	.	kIx.	.
</s>
<s>
Ragga	Ragg	k1gMnSc4	Ragg
značně	značně	k6eAd1	značně
změnilo	změnit	k5eAaPmAgNnS	změnit
svou	svůj	k3xOyFgFnSc4	svůj
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
hudebně	hudebně	k6eAd1	hudebně
se	se	k3xPyFc4	se
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
vzdálilo	vzdálit	k5eAaPmAgNnS	vzdálit
od	od	k7c2	od
reggae	regga	k1gFnSc2	regga
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
opět	opět	k6eAd1	opět
označení	označení	k1gNnSc4	označení
dancehall	dancehalnout	k5eAaPmAgMnS	dancehalnout
<g/>
.	.	kIx.	.
</s>
<s>
Nových	Nových	k2eAgMnPc2d1	Nových
interpretů	interpret	k1gMnPc2	interpret
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
směr	směr	k1gInSc4	směr
udávají	udávat	k5eAaImIp3nP	udávat
především	především	k9	především
rastamani	rastaman	k1gMnPc1	rastaman
ze	z	k7c2	z
sekty	sekta	k1gFnSc2	sekta
Bobo	Bobo	k6eAd1	Bobo
Ashanti	Ashant	k1gMnPc1	Ashant
<g/>
,	,	kIx,	,
v	v	k7c6	v
roots	roots	k6eAd1	roots
reggae	reggae	k6eAd1	reggae
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Richie	Richie	k1gFnSc2	Richie
Spice	Spice	k1gMnSc2	Spice
a	a	k8xC	a
Jah	Jah	k1gMnSc2	Jah
Cure	Cur	k1gMnSc2	Cur
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
populární	populární	k2eAgMnPc1d1	populární
interpreti	interpret	k1gMnPc1	interpret
z	z	k7c2	z
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
Buju	Buja	k1gFnSc4	Buja
Banton	Banton	k1gInSc4	Banton
<g/>
,	,	kIx,	,
v	v	k7c6	v
dancehallu	dancehall	k1gInSc6	dancehall
je	být	k5eAaImIp3nS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
Elephant	Elephant	k1gMnSc1	Elephant
man	man	k1gMnSc1	man
<g/>
,	,	kIx,	,
Lady	lady	k1gFnSc1	lady
Saw	Saw	k1gFnSc1	Saw
a	a	k8xC	a
Beenie	Beenie	k1gFnSc1	Beenie
man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
dnešního	dnešní	k2eAgMnSc4d1	dnešní
krále	král	k1gMnSc4	král
dancehallu	dancehalla	k1gMnSc4	dancehalla
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
v	v	k7c6	v
reggae	reggae	k1gFnSc6	reggae
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
dancehallu	dancehall	k1gInSc6	dancehall
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
využívají	využívat	k5eAaPmIp3nP	využívat
počítače	počítač	k1gInPc1	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
jak	jak	k6eAd1	jak
doma	doma	k6eAd1	doma
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
především	především	k9	především
i	i	k9	i
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
bílí	bílý	k2eAgMnPc1d1	bílý
reggae	reggae	k6eAd1	reggae
interpreti	interpret	k1gMnPc1	interpret
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Gentleman	gentleman	k1gMnSc1	gentleman
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
populární	populární	k2eAgFnSc2d1	populární
Alborosie	Alborosie	k1gFnSc2	Alborosie
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
hitem	hit	k1gInSc7	hit
Kingston	Kingston	k1gInSc1	Kingston
town	towna	k1gFnPc2	towna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
by	by	kYmCp3nS	by
toto	tento	k3xDgNnSc1	tento
bylo	být	k5eAaImAgNnS	být
absolutně	absolutně	k6eAd1	absolutně
nepřípustné	přípustný	k2eNgNnSc1d1	nepřípustné
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
rastamanů	rastaman	k1gMnPc2	rastaman
bílé	bílý	k2eAgFnSc2d1	bílá
už	už	k6eAd1	už
respektovala	respektovat	k5eAaImAgFnS	respektovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
reggae	reggae	k6eAd1	reggae
považovali	považovat	k5eAaImAgMnP	považovat
jen	jen	k9	jen
a	a	k8xC	a
jen	jen	k9	jen
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
a	a	k8xC	a
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
osobností	osobnost	k1gFnSc7	osobnost
reggae	reggae	k1gFnPc2	reggae
byl	být	k5eAaImAgMnS	být
Bob	Bob	k1gMnSc1	Bob
Marley	Marlea	k1gFnSc2	Marlea
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
začíná	začínat	k5eAaImIp3nS	začínat
profesionálně	profesionálně	k6eAd1	profesionálně
zpívat	zpívat	k5eAaImF	zpívat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
The	The	k1gMnPc2	The
Wailers	Wailersa	k1gFnPc2	Wailersa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
Natty	Natta	k1gFnSc2	Natta
dread	dread	k6eAd1	dread
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hvězdou	hvězda	k1gFnSc7	hvězda
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
formátu	formát	k1gInSc2	formát
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
již	již	k6eAd1	již
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
skladeb	skladba	k1gFnPc2	skladba
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Ukázka	ukázka	k1gFnSc1	ukázka
<g/>
:	:	kIx,	:
Redemption	Redemption	k1gInSc1	Redemption
Song	song	k1gInSc1	song
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
osobností	osobnost	k1gFnPc2	osobnost
reggae	reggae	k6eAd1	reggae
je	být	k5eAaImIp3nS	být
Peter	Peter	k1gMnSc1	Peter
Tosh	Tosh	k1gMnSc1	Tosh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
The	The	k1gFnSc2	The
Wailers	Wailersa	k1gFnPc2	Wailersa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	se	k3xPyFc4	se
hudbou	hudba	k1gFnSc7	hudba
reggae	reggaat	k5eAaPmIp3nS	reggaat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
například	například	k6eAd1	například
Marcii	Marcie	k1gFnSc4	Marcie
Grifiths	Grifithsa	k1gFnPc2	Grifithsa
<g/>
,	,	kIx,	,
Dawn	Dawn	k1gMnSc1	Dawn
Penn	Penn	k1gMnSc1	Penn
<g/>
,	,	kIx,	,
Sister	Sister	k1gMnSc1	Sister
Nancy	Nancy	k1gFnSc1	Nancy
<g/>
,	,	kIx,	,
Lady	lady	k1gFnSc1	lady
Saw	Saw	k1gFnSc1	Saw
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zpěváků	zpěvák	k1gMnPc2	zpěvák
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
ale	ale	k8xC	ale
i	i	k8xC	i
např.	např.	kA	např.
Shaggy	Shagg	k1gInPc1	Shagg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
bývalo	bývat	k5eAaImAgNnS	bývat
reggae	reggae	k6eAd1	reggae
spíše	spíše	k9	spíše
menšinovým	menšinový	k2eAgInSc7d1	menšinový
žánrem	žánr	k1gInSc7	žánr
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
obliba	obliba	k1gFnSc1	obliba
stále	stále	k6eAd1	stále
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
českých	český	k2eAgFnPc2d1	Česká
skupin	skupina	k1gFnPc2	skupina
hrající	hrající	k2eAgFnSc2d1	hrající
reggae	regga	k1gFnSc2	regga
je	být	k5eAaImIp3nS	být
Švihadlo	švihadlo	k1gNnSc4	švihadlo
která	který	k3yIgFnSc1	který
existuje	existovat	k5eAaImIp3nS	existovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
opravdových	opravdový	k2eAgFnPc2d1	opravdová
reggae	reggae	k1gFnPc2	reggae
kapel	kapela	k1gFnPc2	kapela
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
skupina	skupina	k1gFnSc1	skupina
Babalet	Babalet	k1gInSc1	Babalet
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nežijící	žijící	k2eNgMnPc1d1	nežijící
"	"	kIx"	"
<g/>
první	první	k4xOgMnSc1	první
český	český	k2eAgMnSc1d1	český
rastaman	rastaman	k1gMnSc1	rastaman
<g/>
"	"	kIx"	"
zpěvák	zpěvák	k1gMnSc1	zpěvák
Aleš	Aleš	k1gMnSc1	Aleš
Drvota	Drvota	k1gFnSc1	Drvota
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
Babalet	Babalet	k1gInSc1	Babalet
působí	působit	k5eAaImIp3nS	působit
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
přestávkami	přestávka	k1gFnPc7	přestávka
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
například	například	k6eAd1	například
zpěváci	zpěvák	k1gMnPc1	zpěvák
Bourama	Bourama	k?	Bourama
Baaji	Baaj	k1gInPc7	Baaj
a	a	k8xC	a
především	především	k6eAd1	především
Martin	Martin	k1gMnSc1	Martin
Tankwey	Tankwea	k1gFnSc2	Tankwea
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
kapela	kapela	k1gFnSc1	kapela
natočila	natočit	k5eAaBmAgFnS	natočit
3	[number]	k4	3
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
kapela	kapela	k1gFnSc1	kapela
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
známá	známý	k2eAgFnSc1d1	známá
skupina	skupina	k1gFnSc1	skupina
Hypnotix	Hypnotix	k1gInSc4	Hypnotix
hrající	hrající	k2eAgInSc1d1	hrající
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Dub-o-Net	Dub-Net	k1gMnSc1	Dub-o-Net
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
známá	známý	k2eAgFnSc1d1	známá
kapela	kapela	k1gFnSc1	kapela
je	být	k5eAaImIp3nS	být
kapela	kapela	k1gFnSc1	kapela
Urban	Urban	k1gMnSc1	Urban
Bushman	Bushman	k1gMnSc1	Bushman
či	či	k8xC	či
Confessions	Confessions	k1gInSc1	Confessions
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
stylu	styl	k1gInSc2	styl
roots	roots	k1gInSc1	roots
reggae	regga	k1gInPc1	regga
(	(	kIx(	(
<g/>
Ukázka	ukázka	k1gFnSc1	ukázka
<g/>
:	:	kIx,	:
Blessing	Blessing	k1gInSc1	Blessing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
mladá	mladý	k2eAgFnSc1d1	mladá
kapela	kapela	k1gFnSc1	kapela
Pub	Pub	k1gFnSc2	Pub
Animals	Animalsa	k1gFnPc2	Animalsa
svým	svůj	k3xOyFgNnSc7	svůj
debutovým	debutový	k2eAgNnSc7d1	debutové
albem	album	k1gNnSc7	album
naskočila	naskočit	k5eAaPmAgFnS	naskočit
na	na	k7c4	na
vlnu	vlna	k1gFnSc4	vlna
reggae	regga	k1gInSc2	regga
a	a	k8xC	a
ska	ska	k?	ska
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
popu	pop	k1gInSc2	pop
<g/>
.	.	kIx.	.
</s>
<s>
United	United	k1gInSc1	United
Flavour	Flavoura	k1gFnPc2	Flavoura
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kapela	kapela	k1gFnSc1	kapela
působící	působící	k2eAgFnSc1d1	působící
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
reggae	reggae	k1gFnSc7	reggae
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
a	a	k8xC	a
Latino	latina	k1gFnSc5	latina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
kapelám	kapela	k1gFnPc3	kapela
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Zion	Zion	k1gInSc1	Zion
Squad	Squad	k1gInSc1	Squad
<g/>
,	,	kIx,	,
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Cocoman	Cocoman	k1gMnSc1	Cocoman
&	&	k?	&
The	The	k1gFnSc1	The
Solid	solid	k1gInSc1	solid
Vibes	Vibes	k1gInSc1	Vibes
<g/>
,	,	kIx,	,
Lion	Lion	k1gMnSc1	Lion
Taxi	taxi	k1gNnSc2	taxi
<g/>
,	,	kIx,	,
Bug	Bug	k1gMnSc1	Bug
&	&	k?	&
Dub	Dub	k1gMnSc1	Dub
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Roots	Rootsa	k1gFnPc2	Rootsa
anebo	anebo	k8xC	anebo
Riddimshot	Riddimshota	k1gFnPc2	Riddimshota
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
nesmíme	smět	k5eNaImIp1nP	smět
opomenout	opomenout	k5eAaPmF	opomenout
projekt	projekt	k1gInSc4	projekt
Sklizeň	sklizeň	k1gFnSc1	sklizeň
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vyklubal	vyklubat	k5eAaPmAgMnS	vyklubat
ze	z	k7c2	z
široké	široký	k2eAgFnSc2d1	široká
spolupráce	spolupráce	k1gFnSc2	spolupráce
zpěváků	zpěvák	k1gMnPc2	zpěvák
okolo	okolo	k7c2	okolo
Cocomanova	Cocomanův	k2eAgMnSc2d1	Cocomanův
producentského	producentský	k2eAgMnSc2d1	producentský
alterega	altereg	k1gMnSc2	altereg
Coco	Coco	k6eAd1	Coco
Jammin	Jammin	k2eAgInSc1d1	Jammin
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
dost	dost	k6eAd1	dost
prosazovat	prosazovat	k5eAaImF	prosazovat
hodně	hodně	k6eAd1	hodně
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
zpěvák	zpěvák	k1gMnSc1	zpěvák
reggae	reggaat	k5eAaPmIp3nS	reggaat
z	z	k7c2	z
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
Colectiv	Colectiva	k1gFnPc2	Colectiva
který	který	k3yQgInSc4	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
rozjet	rozjet	k5eAaPmF	rozjet
nejen	nejen	k6eAd1	nejen
jižní	jižní	k2eAgFnPc4d1	jižní
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
i	i	k9	i
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
scéně	scéna	k1gFnSc6	scéna
jen	jen	k9	jen
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
má	mít	k5eAaImIp3nS	mít
zástup	zástup	k1gInSc4	zástup
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
nové	nový	k2eAgInPc4d1	nový
projekty	projekt	k1gInPc4	projekt
a	a	k8xC	a
doufejme	doufat	k5eAaImRp1nP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
reggae	reggaat	k5eAaPmIp3nS	reggaat
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
stane	stanout	k5eAaPmIp3nS	stanout
stejně	stejně	k6eAd1	stejně
populární	populární	k2eAgInSc1d1	populární
jako	jako	k8xS	jako
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
ska	ska	k?	ska
<g/>
.	.	kIx.	.
</s>
<s>
Komplexní	komplexní	k2eAgInSc4d1	komplexní
přehled	přehled	k1gInSc4	přehled
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
akcí	akce	k1gFnPc2	akce
pořádaných	pořádaný	k2eAgFnPc2d1	pořádaná
v	v	k7c6	v
ČR	ČR	kA	ČR
naleznete	naleznout	k5eAaPmIp2nP	naleznout
na	na	k7c4	na
Rastamasha	Rastamash	k1gMnSc4	Rastamash
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
výrazně	výrazně	k6eAd1	výrazně
je	být	k5eAaImIp3nS	být
reggae	reggae	k6eAd1	reggae
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
v	v	k7c6	v
kulturním	kulturní	k2eAgInSc6d1	kulturní
životě	život	k1gInSc6	život
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
poměrně	poměrně	k6eAd1	poměrně
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
hudebním	hudební	k2eAgInSc7d1	hudební
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pořádají	pořádat	k5eAaImIp3nP	pořádat
reggae	reggae	k6eAd1	reggae
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
hrají	hrát	k5eAaImIp3nP	hrát
pražští	pražský	k2eAgMnPc1d1	pražský
DJs	DJs	k1gMnPc1	DJs
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
klubech	klub	k1gInPc6	klub
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Cross	Cross	k1gInSc1	Cross
<g/>
,	,	kIx,	,
Palác	palác	k1gInSc1	palác
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
Lucerna	lucerna	k1gFnSc1	lucerna
Music	Music	k1gMnSc1	Music
Bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
Wakata	Wake	k1gNnPc1	Wake
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
jezdí	jezdit	k5eAaImIp3nP	jezdit
hrát	hrát	k5eAaImF	hrát
hvězdy	hvězda	k1gFnPc1	hvězda
jamajského	jamajský	k2eAgInSc2d1	jamajský
reggae	regga	k1gInSc2	regga
(	(	kIx(	(
<g/>
Max	Max	k1gMnSc1	Max
Romeo	Romeo	k1gMnSc1	Romeo
<g/>
,	,	kIx,	,
Yellowman	Yellowman	k1gMnSc1	Yellowman
<g/>
,	,	kIx,	,
Daddy	Dadda	k1gFnPc1	Dadda
Rings	Rings	k1gInSc1	Rings
<g/>
,	,	kIx,	,
Sizzla	Sizzla	k1gFnSc1	Sizzla
Kalonji	Kalonje	k1gFnSc4	Kalonje
<g/>
,	,	kIx,	,
Anthony	Anthona	k1gFnPc4	Anthona
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
do	do	k7c2	do
Česko-Slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
superstar	superstar	k1gFnSc2	superstar
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
Michal	Michal	k1gMnSc1	Michal
Šeps	šeps	k1gInSc4	šeps
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dancehall	Dancehall	k1gInSc1	Dancehall
-	-	kIx~	-
prostor	prostor	k1gInSc1	prostor
nebo	nebo	k8xC	nebo
místnost	místnost	k1gFnSc1	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
reprodukovaně	reprodukovaně	k6eAd1	reprodukovaně
hraje	hrát	k5eAaImIp3nS	hrát
hudba	hudba	k1gFnSc1	hudba
nebo	nebo	k8xC	nebo
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
ragga	ragga	k1gFnSc1	ragga
Riddim	Riddim	k1gInSc1	Riddim
-	-	kIx~	-
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
pro	pro	k7c4	pro
dub	dub	k1gInSc4	dub
<g/>
)	)	kIx)	)
Toasting	Toasting	k1gInSc1	Toasting
nebo	nebo	k8xC	nebo
Raggamuffin	Raggamuffin	k1gInSc1	Raggamuffin
-	-	kIx~	-
hlasový	hlasový	k2eAgInSc1d1	hlasový
projev	projev	k1gInSc1	projev
spojující	spojující	k2eAgFnSc4d1	spojující
techniku	technika	k1gFnSc4	technika
rapu	rapa	k1gFnSc4	rapa
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
Rude	Rude	k1gNnSc2	Rude
boy	boa	k1gFnSc2	boa
-	-	kIx~	-
označení	označení	k1gNnSc1	označení
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
výtržníky	výtržník	k1gMnPc4	výtržník
<g/>
,	,	kIx,	,
pouliční	pouliční	k2eAgMnPc4d1	pouliční
zlodějíčky	zlodějíček	k1gMnPc4	zlodějíček
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
populární	populární	k2eAgMnPc1d1	populární
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
D.J.	D.J.	k1gMnPc2	D.J.
-	-	kIx~	-
osoba	osoba	k1gFnSc1	osoba
využívající	využívající	k2eAgNnSc1d1	využívající
toasting	toasting	k1gInSc4	toasting
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
hlasový	hlasový	k2eAgInSc4d1	hlasový
projev	projev	k1gInSc4	projev
Babylon	Babylon	k1gInSc1	Babylon
-	-	kIx~	-
rastafariánské	rastafariánský	k2eAgNnSc1d1	rastafariánské
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgInSc4d1	dnešní
svět	svět	k1gInSc4	svět
nebo	nebo	k8xC	nebo
systém	systém	k1gInSc4	systém
Hudbě	hudba	k1gFnSc3	hudba
reggae	reggae	k1gInSc1	reggae
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
příbuzným	příbuzný	k2eAgInPc3d1	příbuzný
žánrům	žánr	k1gInPc3	žánr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
řada	řada	k1gFnSc1	řada
každoročních	každoroční	k2eAgNnPc2d1	každoroční
reggae	reggae	k1gNnPc2	reggae
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
světově	světově	k6eAd1	světově
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
Sunssplash	Sunssplash	k1gInSc4	Sunssplash
v	v	k7c4	v
Ocho	Ocho	k1gNnSc4	Ocho
Rios	Riosa	k1gFnPc2	Riosa
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
a	a	k8xC	a
Sumfest	Sumfest	k1gInSc4	Sumfest
v	v	k7c6	v
Montegu	Monteg	k1gInSc6	Monteg
Bay	Bay	k1gFnSc2	Bay
také	také	k9	také
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
festivaly	festival	k1gInPc4	festival
<g/>
:	:	kIx,	:
Soča	Soč	k2eAgFnSc1d1	Soča
Reggae	Reggae	k1gFnSc1	Reggae
Riversplash	Riversplasha	k1gFnPc2	Riversplasha
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
,	,	kIx,	,
Notting	Notting	k1gInSc4	Notting
hill	hillum	k1gNnPc2	hillum
Carnival	Carnival	k1gFnSc7	Carnival
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Uppsala	Uppsala	k1gFnSc1	Uppsala
Reggae	Reggae	k1gInSc1	Reggae
Festival	festival	k1gInSc1	festival
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznanějším	význaný	k2eAgFnPc3d3	význaný
reggae	reggae	k1gFnPc3	reggae
festivalům	festival	k1gInPc3	festival
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
patřil	patřit	k5eAaImAgInS	patřit
Reggae	Reggae	k1gInSc1	Reggae
Ethnic	Ethnice	k1gFnPc2	Ethnice
Session	Session	k1gInSc1	Session
v	v	k7c6	v
Heřmaničkách	Heřmanička	k1gFnPc6	Heřmanička
u	u	k7c2	u
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nefunguje	fungovat	k5eNaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Časozběr	Časozběr	k1gInSc1	Časozběr
Reggae	Regga	k1gFnSc2	Regga
a	a	k8xC	a
Mighty	Mighta	k1gFnSc2	Mighta
Sounds	Soundsa	k1gFnPc2	Soundsa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
festivaly	festival	k1gInPc4	festival
Pohodička	pohodička	k1gFnSc1	pohodička
<g/>
,	,	kIx,	,
Reggae	Reggae	k1gFnSc1	Reggae
Meeting	meeting	k1gInSc1	meeting
<g/>
,	,	kIx,	,
Cultural	Cultural	k1gMnSc1	Cultural
Reggae	Regga	k1gInSc2	Regga
Vibez	Vibez	k1gMnSc1	Vibez
v	v	k7c6	v
Hořicích	Hořice	k1gFnPc6	Hořice
<g/>
,	,	kIx,	,
Reggae	Reggae	k1gFnSc1	Reggae
Area	area	k1gFnSc1	area
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Reggae	Reggae	k1gNnSc7	Reggae
v	v	k7c6	v
Parku	park	k1gInSc6	park
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
atd.	atd.	kA	atd.
</s>
