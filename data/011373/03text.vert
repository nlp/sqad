<p>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
dopravní	dopravní	k2eAgFnSc1d1	dopravní
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
pěší	pěší	k2eAgFnSc4d1	pěší
<g/>
,	,	kIx,	,
silniční	silniční	k2eAgFnSc4d1	silniční
nebo	nebo	k8xC	nebo
železniční	železniční	k2eAgFnSc4d1	železniční
cestu	cesta	k1gFnSc4	cesta
případně	případně	k6eAd1	případně
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
překážku	překážka	k1gFnSc4	překážka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
terénní	terénní	k2eAgFnSc1d1	terénní
nerovnost	nerovnost	k1gFnSc1	nerovnost
(	(	kIx(	(
<g/>
údolí	údolí	k1gNnSc1	údolí
<g/>
,	,	kIx,	,
rokle	rokle	k1gFnSc1	rokle
<g/>
,	,	kIx,	,
strž	strž	k1gFnSc1	strž
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgFnSc1d1	jiná
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
za	za	k7c4	za
most	most	k1gInSc4	most
považuje	považovat	k5eAaImIp3nS	považovat
překlenutí	překlenutí	k1gNnSc4	překlenutí
překážky	překážka	k1gFnSc2	překážka
delší	dlouhý	k2eAgFnSc7d2	delší
než	než	k8xS	než
2,0	[number]	k4	2,0
<g/>
m.	m.	k?	m.
Kratší	krátký	k2eAgInSc1d2	kratší
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
propustek	propustek	k1gInSc4	propustek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Prvními	první	k4xOgFnPc7	první
mosty	most	k1gInPc4	most
byly	být	k5eAaImAgFnP	být
kmeny	kmen	k1gInPc4	kmen
stromů	strom	k1gInPc2	strom
spadlých	spadlý	k2eAgInPc2d1	spadlý
přes	přes	k7c4	přes
vodní	vodní	k2eAgInPc4d1	vodní
toky	tok	k1gInPc4	tok
či	či	k8xC	či
jiné	jiný	k2eAgFnPc4d1	jiná
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
toho	ten	k3xDgMnSc4	ten
člověk	člověk	k1gMnSc1	člověk
využil	využít	k5eAaPmAgMnS	využít
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
vlastního	vlastní	k2eAgNnSc2d1	vlastní
přemostění	přemostění	k1gNnSc2	přemostění
z	z	k7c2	z
kmenů	kmen	k1gInPc2	kmen
poražených	poražený	k2eAgInPc2d1	poražený
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zlepšování	zlepšování	k1gNnSc1	zlepšování
technologií	technologie	k1gFnPc2	technologie
obrábění	obrábění	k1gNnSc2	obrábění
dřeva	dřevo	k1gNnSc2	dřevo
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
člověku	člověk	k1gMnSc3	člověk
stavět	stavět	k5eAaImF	stavět
již	již	k9	již
soudobé	soudobý	k2eAgFnPc4d1	soudobá
mostní	mostní	k2eAgFnPc4d1	mostní
konstrukce	konstrukce	k1gFnPc4	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dřevěných	dřevěný	k2eAgFnPc6d1	dřevěná
kládách	kláda	k1gFnPc6	kláda
a	a	k8xC	a
opracovaných	opracovaný	k2eAgInPc6d1	opracovaný
trámech	trám	k1gInPc6	trám
položených	položený	k2eAgInPc6d1	položený
přes	přes	k7c4	přes
překážku	překážka	k1gFnSc4	překážka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
stavěny	stavit	k5eAaImNgInP	stavit
mosty	most	k1gInPc1	most
delší	dlouhý	k2eAgInPc1d2	delší
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
využívaly	využívat	k5eAaPmAgFnP	využívat
kamenné	kamenný	k2eAgInPc4d1	kamenný
pilíře	pilíř	k1gInPc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
byly	být	k5eAaImAgInP	být
dostupnějším	dostupný	k2eAgInSc7d2	dostupnější
materiálem	materiál	k1gInSc7	materiál
liány	liána	k1gFnSc2	liána
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
tzv.	tzv.	kA	tzv.
visuté	visutý	k2eAgInPc1d1	visutý
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Incká	incký	k2eAgFnSc1d1	incká
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
znali	znát	k5eAaImAgMnP	znát
klenbu	klenba	k1gFnSc4	klenba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
stavebních	stavební	k2eAgInPc2d1	stavební
prvků	prvek	k1gInPc2	prvek
nejenom	nejenom	k6eAd1	nejenom
kamenných	kamenný	k2eAgInPc2d1	kamenný
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
akvadukty	akvadukt	k1gInPc1	akvadukt
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
staveb	stavba	k1gFnPc2	stavba
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
říše	říš	k1gFnSc2	říš
stavitelé	stavitel	k1gMnPc1	stavitel
v	v	k7c6	v
nástupnických	nástupnický	k2eAgInPc6d1	nástupnický
státech	stát	k1gInPc6	stát
snažili	snažit	k5eAaImAgMnP	snažit
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
římské	římský	k2eAgFnPc4d1	římská
znalosti	znalost	k1gFnPc4	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Románské	románský	k2eAgInPc1d1	románský
mosty	most	k1gInPc1	most
přímo	přímo	k6eAd1	přímo
vycházely	vycházet	k5eAaImAgInP	vycházet
z	z	k7c2	z
římských	římský	k2eAgInPc2d1	římský
vzorů	vzor	k1gInPc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
úpadku	úpadek	k1gInSc6	úpadek
předávání	předávání	k1gNnSc2	předávání
systematických	systematický	k2eAgFnPc2d1	systematická
znalostí	znalost	k1gFnPc2	znalost
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
<g/>
:	:	kIx,	:
pád	pád	k1gInSc1	pád
Západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
r.	r.	kA	r.
476	[number]	k4	476
a	a	k8xC	a
první	první	k4xOgFnSc1	první
universita	universita	k1gFnSc1	universita
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
r.	r.	kA	r.
1088	[number]	k4	1088
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
gotiky	gotika	k1gFnSc2	gotika
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
mocných	mocní	k1gMnPc2	mocní
království	království	k1gNnSc4	království
konstrukci	konstrukce	k1gFnSc4	konstrukce
mostu	most	k1gInSc2	most
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
<g/>
.	.	kIx.	.
</s>
<s>
Mosty	most	k1gInPc1	most
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
s	s	k7c7	s
většími	veliký	k2eAgInPc7d2	veliký
oblouky	oblouk	k1gInPc7	oblouk
(	(	kIx(	(
<g/>
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
nejstarší	starý	k2eAgInPc4d3	nejstarší
mosty	most	k1gInPc4	most
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
vývoji	vývoj	k1gInSc3	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Pont	Pont	k1gInSc1	Pont
Neuf	Neuf	k1gInSc1	Neuf
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
bez	bez	k7c2	bez
tradičního	tradiční	k2eAgNnSc2d1	tradiční
zastřešení	zastřešení	k1gNnSc2	zastřešení
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
osazen	osadit	k5eAaPmNgInS	osadit
i	i	k9	i
chodníky	chodník	k1gInPc1	chodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
mostů	most	k1gInPc2	most
přinesla	přinést	k5eAaPmAgFnS	přinést
industrializace	industrializace	k1gFnSc1	industrializace
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
železnice	železnice	k1gFnSc2	železnice
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
stavbách	stavba	k1gFnPc6	stavba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
levnější	levný	k2eAgFnPc1d2	levnější
a	a	k8xC	a
odolnější	odolný	k2eAgFnPc1d2	odolnější
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
ještě	ještě	k9	ještě
mosty	most	k1gInPc1	most
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
a	a	k8xC	a
viadukty	viadukt	k1gInPc1	viadukt
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
využívána	využíván	k2eAgFnSc1d1	využívána
litina	litina	k1gFnSc1	litina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Ironbridge	Ironbridge	k1gInSc1	Ironbridge
<g/>
,	,	kIx,	,
z	z	k7c2	z
litiny	litina	k1gFnSc2	litina
byl	být	k5eAaImAgInS	být
postaven	postaven	k2eAgInSc1d1	postaven
roku	rok	k1gInSc2	rok
1779	[number]	k4	1779
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Severn	Severna	k1gFnPc2	Severna
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
zdokonalila	zdokonalit	k5eAaPmAgFnS	zdokonalit
výroba	výroba	k1gFnSc1	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
stavět	stavět	k5eAaImF	stavět
mosty	most	k1gInPc1	most
ocelové	ocelový	k2eAgInPc1d1	ocelový
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
železničních	železniční	k2eAgInPc6d1	železniční
mostech	most	k1gInPc6	most
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tovární	tovární	k2eAgFnSc3d1	tovární
výrobě	výroba	k1gFnSc3	výroba
ze	z	k7c2	z
standardizovaných	standardizovaný	k2eAgInPc2d1	standardizovaný
dílů	díl	k1gInPc2	díl
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
prvky	prvek	k1gInPc1	prvek
příhradových	příhradový	k2eAgInPc2d1	příhradový
nosníků	nosník	k1gInPc2	nosník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
ocel	ocel	k1gFnSc4	ocel
takové	takový	k3xDgFnSc2	takový
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
budovány	budován	k2eAgInPc4d1	budován
mosty	most	k1gInPc4	most
řetězové	řetězový	k2eAgInPc4d1	řetězový
a	a	k8xC	a
lanové	lanový	k2eAgInPc4d1	lanový
o	o	k7c6	o
velkých	velký	k2eAgNnPc6d1	velké
rozpětích	rozpětí	k1gNnPc6	rozpětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
mostů	most	k1gInPc2	most
a	a	k8xC	a
viaduktů	viadukt	k1gInPc2	viadukt
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
železobeton	železobeton	k1gInSc1	železobeton
a	a	k8xC	a
předpjatý	předpjatý	k2eAgInSc1d1	předpjatý
beton	beton	k1gInSc1	beton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
mostů	most	k1gInPc2	most
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
konstrukce	konstrukce	k1gFnSc2	konstrukce
===	===	k?	===
</s>
</p>
<p>
<s>
Prvními	první	k4xOgFnPc7	první
mosty	most	k1gInPc1	most
byly	být	k5eAaImAgInP	být
nejspíše	nejspíše	k9	nejspíše
vhodně	vhodně	k6eAd1	vhodně
položené	položený	k2eAgNnSc1d1	položené
a	a	k8xC	a
dostatečně	dostatečně	k6eAd1	dostatečně
silné	silný	k2eAgInPc1d1	silný
kmeny	kmen	k1gInPc1	kmen
–	–	k?	–
most	most	k1gInSc1	most
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
trám	trám	k1gInSc1	trám
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
trám	trám	k1gInSc1	trám
je	být	k5eAaImIp3nS	být
namáhán	namáhat	k5eAaImNgInS	namáhat
současně	současně	k6eAd1	současně
tlakem	tlak	k1gInSc7	tlak
(	(	kIx(	(
<g/>
při	při	k7c6	při
horním	horní	k2eAgInSc6d1	horní
povrchu	povrch	k1gInSc6	povrch
<g/>
)	)	kIx)	)
a	a	k8xC	a
tahem	tah	k1gInSc7	tah
(	(	kIx(	(
<g/>
při	při	k7c6	při
spodním	spodní	k2eAgInSc6d1	spodní
povrchu	povrch	k1gInSc6	povrch
<g/>
)	)	kIx)	)
–	–	k?	–
neboli	neboli	k8xC	neboli
ohybem	ohyb	k1gInSc7	ohyb
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
průřezu	průřez	k1gInSc2	průřez
trámu	trám	k1gInSc2	trám
je	být	k5eAaImIp3nS	být
namáhána	namáhat	k5eAaImNgFnS	namáhat
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
ušetřit	ušetřit	k5eAaPmF	ušetřit
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
válcovaný	válcovaný	k2eAgInSc1d1	válcovaný
nebo	nebo	k8xC	nebo
svařovaný	svařovaný	k2eAgInSc1d1	svařovaný
nosník	nosník	k1gInSc1	nosník
tvaru	tvar	k1gInSc2	tvar
I	i	k9	i
nebo	nebo	k8xC	nebo
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
rozdělení	rozdělení	k1gNnSc1	rozdělení
výztuže	výztuž	k1gFnSc2	výztuž
v	v	k7c6	v
železobetonu	železobeton	k1gInSc6	železobeton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trám	trám	k1gInSc1	trám
se	se	k3xPyFc4	se
často	často	k6eAd1	často
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
konstrukcemi	konstrukce	k1gFnPc7	konstrukce
mostů	most	k1gInPc2	most
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
jejich	jejich	k3xOp3gFnSc2	jejich
tuhosti	tuhost	k1gFnSc2	tuhost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
materiálů	materiál	k1gInPc2	materiál
lze	lze	k6eAd1	lze
na	na	k7c6	na
vhodných	vhodný	k2eAgNnPc6d1	vhodné
místech	místo	k1gNnPc6	místo
postavit	postavit	k5eAaPmF	postavit
i	i	k9	i
visutý	visutý	k2eAgInSc4d1	visutý
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
zakotvení	zakotvení	k1gNnSc1	zakotvení
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Lano	lano	k1gNnSc1	lano
mostu	most	k1gInSc2	most
je	být	k5eAaImIp3nS	být
namáháno	namáhán	k2eAgNnSc1d1	namáháno
výhradně	výhradně	k6eAd1	výhradně
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
mostních	mostní	k2eAgFnPc2d1	mostní
konstrukcí	konstrukce	k1gFnPc2	konstrukce
je	být	k5eAaImIp3nS	být
klenba	klenba	k1gFnSc1	klenba
(	(	kIx(	(
<g/>
oblouk	oblouk	k1gInSc1	oblouk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
klenba	klenba	k1gFnSc1	klenba
namáhána	namáhat	k5eAaImNgFnS	namáhat
pouze	pouze	k6eAd1	pouze
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
ovšem	ovšem	k9	ovšem
při	při	k7c6	při
nerovnoměrném	rovnoměrný	k2eNgNnSc6d1	nerovnoměrné
zatížení	zatížení	k1gNnSc6	zatížení
nutně	nutně	k6eAd1	nutně
dojde	dojít	k5eAaPmIp3nS	dojít
i	i	k9	i
k	k	k7c3	k
namáhání	namáhání	k1gNnSc4	namáhání
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Smělost	smělost	k1gFnSc1	smělost
(	(	kIx(	(
<g/>
plochost	plochost	k1gFnSc1	plochost
<g/>
)	)	kIx)	)
klenby	klenba	k1gFnPc1	klenba
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nP	měřit
velikostí	velikost	k1gFnSc7	velikost
vodorovné	vodorovný	k2eAgFnPc1d1	vodorovná
síly	síla	k1gFnPc1	síla
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
mocnina	mocnina	k1gFnSc1	mocnina
rozpětí	rozpětí	k1gNnSc1	rozpětí
dělená	dělený	k2eAgFnSc1d1	dělená
vzepětím	vzepětí	k1gNnSc7	vzepětí
klenby	klenba	k1gFnSc2	klenba
<g/>
,	,	kIx,	,
v	v	k7c6	v
metrech	metr	k1gInPc6	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mosty	most	k1gInPc4	most
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
podle	podle	k7c2	podle
konstrukce	konstrukce	k1gFnSc2	konstrukce
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
konzolový	konzolový	k2eAgInSc1d1	konzolový
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
visutý	visutý	k2eAgInSc1d1	visutý
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
obloukový	obloukový	k2eAgInSc1d1	obloukový
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
zavěšený	zavěšený	k2eAgInSc1d1	zavěšený
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
trámový	trámový	k2eAgInSc1d1	trámový
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
most	most	k1gInSc4	most
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
použitého	použitý	k2eAgInSc2d1	použitý
materiálu	materiál	k1gInSc2	materiál
===	===	k?	===
</s>
</p>
<p>
<s>
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
(	(	kIx(	(
<g/>
trám	trám	k1gInSc1	trám
<g/>
,	,	kIx,	,
věšadlo	věšadlo	k1gNnSc1	věšadlo
<g/>
,	,	kIx,	,
vzpěradlo	vzpěradlo	k1gNnSc1	vzpěradlo
<g/>
,	,	kIx,	,
oblouk	oblouk	k1gInSc1	oblouk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kamenné	kamenný	k2eAgNnSc1d1	kamenné
(	(	kIx(	(
<g/>
klenba	klenba	k1gFnSc1	klenba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ocelové	ocelový	k2eAgFnPc1d1	ocelová
(	(	kIx(	(
<g/>
trám	trám	k1gInSc1	trám
–	–	k?	–
příhradový	příhradový	k2eAgInSc1d1	příhradový
nebo	nebo	k8xC	nebo
plnostěnný	plnostěnný	k2eAgInSc1d1	plnostěnný
nosník	nosník	k1gInSc1	nosník
<g/>
,	,	kIx,	,
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
visutý	visutý	k2eAgInSc1d1	visutý
či	či	k8xC	či
zavěšený	zavěšený	k2eAgInSc1d1	zavěšený
most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ocelobetonové	ocelobetonový	k2eAgInPc1d1	ocelobetonový
–	–	k?	–
na	na	k7c4	na
ocelové	ocelový	k2eAgInPc4d1	ocelový
trámové	trámový	k2eAgInPc4d1	trámový
nosníky	nosník	k1gInPc4	nosník
se	se	k3xPyFc4	se
vybetonuje	vybetonovat	k5eAaPmIp3nS	vybetonovat
mostovka	mostovka	k1gFnSc1	mostovka
</s>
</p>
<p>
<s>
beton	beton	k1gInSc1	beton
(	(	kIx(	(
<g/>
klenba	klenba	k1gFnSc1	klenba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
železobeton	železobeton	k1gInSc1	železobeton
(	(	kIx(	(
<g/>
klenba	klenba	k1gFnSc1	klenba
<g/>
,	,	kIx,	,
trám	trám	k1gInSc1	trám
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
předpjatý	předpjatý	k2eAgInSc1d1	předpjatý
beton	beton	k1gInSc1	beton
(	(	kIx(	(
<g/>
trám	trám	k1gInSc1	trám
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
plast	plast	k1gInSc1	plast
(	(	kIx(	(
<g/>
kompozitní	kompozitní	k2eAgInSc1d1	kompozitní
materiál	materiál	k1gInSc1	materiál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
===	===	k?	===
</s>
</p>
<p>
<s>
železniční	železniční	k2eAgFnSc1d1	železniční
</s>
</p>
<p>
<s>
silniční	silniční	k2eAgFnSc1d1	silniční
případně	případně	k6eAd1	případně
dálniční	dálniční	k2eAgInPc4d1	dálniční
</s>
</p>
<p>
<s>
lávky	lávka	k1gFnPc1	lávka
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
</s>
</p>
<p>
<s>
přechody	přechod	k1gInPc1	přechod
pro	pro	k7c4	pro
zvěř	zvěř	k1gFnSc4	zvěř
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
např.	např.	kA	např.
ekodukty	ekodukt	k1gInPc1	ekodukt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgFnPc4d1	průmyslová
instalace	instalace	k1gFnPc4	instalace
(	(	kIx(	(
<g/>
dopravníky	dopravník	k1gInPc4	dopravník
nebo	nebo	k8xC	nebo
inženýrské	inženýrský	k2eAgFnPc4d1	inženýrská
sítě	síť	k1gFnPc4	síť
např.	např.	kA	např.
potrubní	potrubní	k2eAgFnPc4d1	potrubní
a	a	k8xC	a
kabelové	kabelový	k2eAgFnPc4d1	kabelová
lávky	lávka	k1gFnPc4	lávka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
akvadukty	akvadukt	k1gInPc1	akvadukt
</s>
</p>
<p>
<s>
inundační	inundační	k2eAgFnSc1d1	inundační
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
výšky	výška	k1gFnSc2	výška
===	===	k?	===
</s>
</p>
<p>
<s>
Mosty	most	k1gInPc4	most
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
výšky	výška	k1gFnSc2	výška
mostovky	mostovka	k1gFnSc2	mostovka
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vysokovodní	vysokovodní	k2eAgInSc1d1	vysokovodní
most	most	k1gInSc1	most
-	-	kIx~	-
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
provádět	provádět	k5eAaImF	provádět
vodní	vodní	k2eAgFnSc4d1	vodní
dopravu	doprava	k1gFnSc4	doprava
pod	pod	k7c7	pod
mostem	most	k1gInSc7	most
</s>
</p>
<p>
<s>
nízkovodní	nízkovodní	k2eAgInSc1d1	nízkovodní
most	most	k1gInSc1	most
-	-	kIx~	-
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
provádět	provádět	k5eAaImF	provádět
vodní	vodní	k2eAgFnSc4d1	vodní
dopravu	doprava	k1gFnSc4	doprava
pod	pod	k7c7	pod
mostem	most	k1gInSc7	most
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc6	část
mostu	most	k1gInSc2	most
==	==	k?	==
</s>
</p>
<p>
<s>
spodní	spodní	k2eAgFnSc1d1	spodní
stavba	stavba	k1gFnSc1	stavba
-	-	kIx~	-
mostní	mostní	k2eAgFnSc1d1	mostní
opěra	opěra	k1gFnSc1	opěra
<g/>
,	,	kIx,	,
pilíř	pilíř	k1gInSc1	pilíř
<g/>
,	,	kIx,	,
rozražeč	rozražeč	k1gInSc1	rozražeč
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
nosná	nosný	k2eAgFnSc1d1	nosná
konstrukce	konstrukce	k1gFnSc1	konstrukce
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
nosná	nosný	k2eAgFnSc1d1	nosná
konstrukce	konstrukce	k1gFnSc1	konstrukce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
oblouk	oblouk	k1gInSc1	oblouk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mostovka	mostovka	k1gFnSc1	mostovka
<g/>
,	,	kIx,	,
mostní	mostní	k2eAgNnSc1d1	mostní
ložisko	ložisko	k1gNnSc1	ložisko
</s>
</p>
<p>
<s>
mostní	mostní	k2eAgInSc1d1	mostní
svršek	svršek	k1gInSc1	svršek
</s>
</p>
<p>
<s>
vybavení	vybavení	k1gNnSc1	vybavení
-	-	kIx~	-
zábradlí	zábradlí	k1gNnSc1	zábradlí
<g/>
,	,	kIx,	,
svodidla	svodidlo	k1gNnSc2	svodidlo
<g/>
,	,	kIx,	,
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
,	,	kIx,	,
odvodnění	odvodnění	k1gNnSc2	odvodnění
</s>
</p>
<p>
<s>
předpolí	předpolí	k1gNnSc4	předpolí
resp.	resp.	kA	resp.
předmostí	předmostí	k1gNnPc2	předmostí
</s>
</p>
<p>
<s>
==	==	k?	==
Pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
mosty	most	k1gInPc1	most
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
měly	mít	k5eAaImAgInP	mít
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
mosty	most	k1gInPc1	most
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
padací	padací	k2eAgFnPc1d1	padací
<g/>
)	)	kIx)	)
převážně	převážně	k6eAd1	převážně
obranný	obranný	k2eAgInSc1d1	obranný
účel	účel	k1gInSc1	účel
(	(	kIx(	(
<g/>
přerušení	přerušení	k1gNnSc1	přerušení
cesty	cesta	k1gFnSc2	cesta
útočníkům	útočník	k1gMnPc3	útočník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
most	most	k1gInSc1	most
staví	stavit	k5eAaPmIp3nS	stavit
většinou	většina	k1gFnSc7	většina
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
překonává	překonávat	k5eAaImIp3nS	překonávat
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
sloužící	sloužící	k2eAgFnSc1d1	sloužící
jako	jako	k8xS	jako
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
by	by	kYmCp3nP	by
možné	možný	k2eAgNnSc1d1	možné
nebo	nebo	k8xC	nebo
efektivní	efektivní	k2eAgNnSc1d1	efektivní
jej	on	k3xPp3gMnSc4	on
stavět	stavět	k5eAaImF	stavět
tak	tak	k6eAd1	tak
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
lodní	lodní	k2eAgFnSc7d1	lodní
dopravou	doprava	k1gFnSc7	doprava
nekolidoval	kolidovat	k5eNaImAgMnS	kolidovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
sklopný	sklopný	k2eAgInSc1d1	sklopný
most	most	k1gInSc1	most
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
celý	celý	k2eAgInSc1d1	celý
nebo	nebo	k8xC	nebo
po	po	k7c6	po
polovinách	polovina	k1gFnPc6	polovina
sklopí	sklopit	k5eAaPmIp3nS	sklopit
do	do	k7c2	do
téměř	téměř	k6eAd1	téměř
svislé	svislý	k2eAgFnSc2d1	svislá
polohy	poloha	k1gFnSc2	poloha
podle	podle	k7c2	podle
vodorovného	vodorovný	k2eAgInSc2d1	vodorovný
čepu	čep	k1gInSc2	čep
na	na	k7c6	na
opěře	opěra	k1gFnSc6	opěra
nebo	nebo	k8xC	nebo
pilíři	pilíř	k1gInSc6	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
mostem	most	k1gInSc7	most
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
Tower	Tower	k1gInSc1	Tower
Bridge	Bridg	k1gFnSc2	Bridg
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Sklápěcí	sklápěcí	k2eAgInPc1d1	sklápěcí
mosty	most	k1gInPc1	most
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
Nizozemsko	Nizozemsko	k1gNnSc4	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Sklápěcí	sklápěcí	k2eAgInPc1d1	sklápěcí
mosty	most	k1gInPc1	most
se	se	k3xPyFc4	se
ale	ale	k9	ale
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
přístavních	přístavní	k2eAgFnPc6d1	přístavní
<g/>
,	,	kIx,	,
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mosty	most	k1gInPc1	most
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
,	,	kIx,	,
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
<g/>
,	,	kIx,	,
<g/>
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
sklápěcí	sklápěcí	k2eAgInSc1d1	sklápěcí
železniční	železniční	k2eAgInSc1d1	železniční
Skansenský	Skansenský	k2eAgInSc1d1	Skansenský
most	most	k1gInSc1	most
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Trondheim	Trondheima	k1gFnPc2	Trondheima
</s>
</p>
<p>
<s>
otočný	otočný	k2eAgInSc1d1	otočný
most	most	k1gInSc1	most
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
svislý	svislý	k2eAgInSc4d1	svislý
otočný	otočný	k2eAgInSc4d1	otočný
čep	čep	k1gInSc4	čep
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
most	most	k1gInSc1	most
natočí	natočit	k5eAaBmIp3nS	natočit
po	po	k7c6	po
směru	směr	k1gInSc6	směr
řeky	řeka	k1gFnSc2	řeka
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
most	most	k1gInSc1	most
vyvážený	vyvážený	k2eAgInSc1d1	vyvážený
a	a	k8xC	a
otáčí	otáčet	k5eAaImIp3nS	otáčet
se	se	k3xPyFc4	se
na	na	k7c6	na
středovém	středový	k2eAgInSc6d1	středový
pilíři	pilíř	k1gInSc6	pilíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
otočný	otočný	k2eAgInSc1d1	otočný
železniční	železniční	k2eAgInSc1d1	železniční
most	most	k1gInSc1	most
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
Meppel	Meppela	k1gFnPc2	Meppela
</s>
</p>
<p>
<s>
zásuvný	zásuvný	k2eAgInSc1d1	zásuvný
most	most	k1gInSc1	most
<g/>
:	:	kIx,	:
část	část	k1gFnSc1	část
mostovky	mostovka	k1gFnSc2	mostovka
nad	nad	k7c7	nad
trasou	trasa	k1gFnSc7	trasa
plavidel	plavidlo	k1gNnPc2	plavidlo
se	se	k3xPyFc4	se
zasune	zasunout	k5eAaPmIp3nS	zasunout
pod	pod	k7c4	pod
části	část	k1gFnPc4	část
mostovky	mostovka	k1gFnSc2	mostovka
u	u	k7c2	u
kraje	kraj	k1gInSc2	kraj
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zvedací	zvedací	k2eAgInSc1d1	zvedací
(	(	kIx(	(
<g/>
výtahový	výtahový	k2eAgInSc1d1	výtahový
<g/>
)	)	kIx)	)
most	most	k1gInSc1	most
<g/>
:	:	kIx,	:
příslušný	příslušný	k2eAgInSc1d1	příslušný
díl	díl	k1gInSc1	díl
mostovky	mostovka	k1gFnSc2	mostovka
se	se	k3xPyFc4	se
zvedne	zvednout	k5eAaPmIp3nS	zvednout
svislým	svislý	k2eAgInSc7d1	svislý
pohybem	pohyb	k1gInSc7	pohyb
</s>
</p>
<p>
<s>
ponorný	ponorný	k2eAgInSc1d1	ponorný
most	most	k1gInSc1	most
<g/>
:	:	kIx,	:
mostovka	mostovka	k1gFnSc1	mostovka
klesne	klesnout	k5eAaPmIp3nS	klesnout
svislým	svislý	k2eAgInSc7d1	svislý
pohybem	pohyb	k1gInSc7	pohyb
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
pod	pod	k7c4	pod
plavební	plavební	k2eAgInSc4d1	plavební
profil	profil	k1gInSc4	profil
</s>
</p>
<p>
<s>
skládací	skládací	k2eAgInSc1d1	skládací
most	most	k1gInSc1	most
<g/>
:	:	kIx,	:
mostovka	mostovka	k1gFnSc1	mostovka
se	se	k3xPyFc4	se
složí	složit	k5eAaPmIp3nS	složit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jednomu	jeden	k4xCgNnSc3	jeden
z	z	k7c2	z
předmostí	předmostí	k1gNnPc5	předmostí
</s>
</p>
<p>
<s>
svinovací	svinovací	k2eAgInSc1d1	svinovací
most	most	k1gInSc1	most
<g/>
:	:	kIx,	:
mostovka	mostovka	k1gFnSc1	mostovka
se	se	k3xPyFc4	se
sroluje	srolovat	k5eAaPmIp3nS	srolovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jednomu	jeden	k4xCgNnSc3	jeden
z	z	k7c2	z
předmostí	předmostí	k1gNnPc2	předmostí
</s>
</p>
<p>
<s>
naklápěcí	naklápěcí	k2eAgInSc1d1	naklápěcí
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
lávka	lávka	k1gFnSc1	lávka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
stavu	stav	k1gInSc6	stav
má	mít	k5eAaImIp3nS	mít
mostovka	mostovka	k1gFnSc1	mostovka
tvar	tvar	k1gInSc1	tvar
stranového	stranový	k2eAgInSc2d1	stranový
oblouku	oblouk	k1gInSc2	oblouk
<g/>
;	;	kIx,	;
před	před	k7c7	před
průjezdem	průjezd	k1gInSc7	průjezd
lodí	loď	k1gFnPc2	loď
se	se	k3xPyFc4	se
sklopí	sklopit	k5eAaPmIp3nS	sklopit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblouk	oblouk	k1gInSc1	oblouk
se	se	k3xPyFc4	se
zvedne	zvednout	k5eAaPmIp3nS	zvednout
nahoruZvláštními	nahoruZvláštní	k2eAgInPc7d1	nahoruZvláštní
typy	typ	k1gInPc7	typ
mobilních	mobilní	k2eAgInPc2d1	mobilní
mostů	most	k1gInPc2	most
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pontonový	pontonový	k2eAgInSc1d1	pontonový
most	most	k1gInSc1	most
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
plováky	plovák	k1gInPc7	plovák
(	(	kIx(	(
<g/>
pontony	ponton	k1gInPc7	ponton
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
položen	položit	k5eAaPmNgInS	položit
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
uplatnění	uplatnění	k1gNnSc4	uplatnění
například	například	k6eAd1	například
při	při	k7c6	při
havarijních	havarijní	k2eAgFnPc6d1	havarijní
situacích	situace	k1gFnPc6	situace
(	(	kIx(	(
<g/>
poškození	poškození	k1gNnSc6	poškození
stálého	stálý	k2eAgInSc2d1	stálý
mostu	most	k1gInSc2	most
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
operativní	operativní	k2eAgInPc4d1	operativní
vojenské	vojenský	k2eAgInPc4d1	vojenský
účely	účel	k1gInPc4	účel
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
mostní	mostní	k2eAgInSc1d1	mostní
dopravník	dopravník	k1gInSc1	dopravník
neboli	neboli	k8xC	neboli
gondolový	gondolový	k2eAgInSc1d1	gondolový
most	most	k1gInSc1	most
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
jako	jako	k9	jako
lanovka	lanovka	k1gFnSc1	lanovka
</s>
</p>
<p>
<s>
==	==	k?	==
Nejdelší	dlouhý	k2eAgInPc1d3	nejdelší
mosty	most	k1gInPc1	most
světa	svět	k1gInSc2	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
most	most	k1gInSc1	most
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
36	[number]	k4	36
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
přístavy	přístav	k1gInPc4	přístav
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
a	a	k8xC	a
Ning-po	Ninga	k1gFnSc5	Ning-pa
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
2,4	[number]	k4	2,4
kilometru	kilometr	k1gInSc2	kilometr
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
vůbec	vůbec	k9	vůbec
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
most	most	k1gInSc4	most
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
Pontchartrain	Pontchartraina	k1gFnPc2	Pontchartraina
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Louisiana	Louisiana	k1gFnSc1	Louisiana
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
měří	měřit	k5eAaImIp3nS	měřit
38,422	[number]	k4	38,422
kilometru	kilometr	k1gInSc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známé	známý	k2eAgInPc1d1	známý
mosty	most	k1gInPc1	most
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Barrandovský	barrandovský	k2eAgInSc1d1	barrandovský
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Branický	branický	k2eAgInSc1d1	branický
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
most	most	k1gInSc1	most
Inteligence	inteligence	k1gFnSc1	inteligence
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Dálniční	dálniční	k2eAgInSc4d1	dálniční
most	most	k1gInSc4	most
Vysočina	vysočina	k1gFnSc1	vysočina
ve	v	k7c6	v
Velkém	velký	k2eAgNnSc6d1	velké
Meziříčí	Meziříčí	k1gNnSc6	Meziříčí
</s>
</p>
<p>
<s>
Duhový	duhový	k2eAgInSc1d1	duhový
most	most	k1gInSc1	most
v	v	k7c6	v
Bechyni	Bechyně	k1gFnSc6	Bechyně
</s>
</p>
<p>
<s>
Hranické	hranický	k2eAgInPc1d1	hranický
viadukty	viadukt	k1gInPc1	viadukt
</s>
</p>
<p>
<s>
Karlův	Karlův	k2eAgInSc1d1	Karlův
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
</s>
</p>
<p>
<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
most	most	k1gInSc1	most
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
v	v	k7c6	v
Roudnici	Roudnice	k1gFnSc6	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
<p>
<s>
Negrelliho	Negrellize	k6eAd1	Negrellize
viadukt	viadukt	k1gInSc1	viadukt
</s>
</p>
<p>
<s>
Nuselský	nuselský	k2eAgInSc1d1	nuselský
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Stádlecký	Stádlecký	k2eAgInSc1d1	Stádlecký
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
most	most	k1gInSc1	most
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
</s>
</p>
<p>
<s>
Žďákovský	Žďákovský	k2eAgInSc1d1	Žďákovský
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Avignonský	avignonský	k2eAgInSc1d1	avignonský
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Brooklynský	brooklynský	k2eAgInSc1d1	brooklynský
most	most	k1gInSc1	most
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
</s>
</p>
<p>
<s>
Fehmarnsundský	Fehmarnsundský	k2eAgInSc1d1	Fehmarnsundský
most	most	k1gInSc1	most
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
</s>
</p>
<p>
<s>
Forth	Forth	k1gInSc1	Forth
Bridge	Bridg	k1gFnSc2	Bridg
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
</s>
</p>
<p>
<s>
Golden	Goldna	k1gFnPc2	Goldna
Gate	Gat	k1gFnSc2	Gat
Bridge	Bridg	k1gFnSc2	Bridg
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
</s>
</p>
<p>
<s>
Galatský	Galatský	k2eAgInSc1d1	Galatský
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Ironbridge	Ironbridge	k6eAd1	Ironbridge
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgInSc1d3	nejstarší
železný	železný	k2eAgInSc1d1	železný
most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
Akaši-Kaikjó	Akaši-Kaikjó	k1gFnSc2	Akaši-Kaikjó
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
přes	přes	k7c4	přes
Velký	velký	k2eAgInSc4d1	velký
Belt	Belt	k1gInSc4	Belt
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národného	národný	k2eAgNnSc2d1	národné
povstania	povstanium	k1gNnSc2	povstanium
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
</s>
</p>
<p>
<s>
Pont	Pont	k1gInSc1	Pont
Neuf	Neuf	k1gInSc1	Neuf
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
</s>
</p>
<p>
<s>
Ponte	Pont	k1gInSc5	Pont
di	di	k?	di
Rialto	Rialt	k2eAgNnSc4d1	Rialto
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
</s>
</p>
<p>
<s>
Stari	Stari	k6eAd1	Stari
most	most	k1gInSc1	most
v	v	k7c6	v
Mostaru	Mostar	k1gInSc6	Mostar
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
</s>
</p>
<p>
<s>
Svinesundský	Svinesundský	k2eAgInSc1d1	Svinesundský
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Tower	Tower	k1gInSc1	Tower
Bridge	Bridg	k1gFnSc2	Bridg
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
</s>
</p>
<p>
<s>
Viadukt	viadukt	k1gInSc1	viadukt
Millau	Millaus	k1gInSc2	Millaus
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Evidence	evidence	k1gFnSc1	evidence
mostů	most	k1gInPc2	most
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
most	most	k1gInSc1	most
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
most	most	k1gInSc1	most
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
mostů	most	k1gInPc2	most
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
(	(	kIx(	(
<g/>
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Libri	Libr	k1gFnSc2	Libr
<g/>
)	)	kIx)	)
</s>
</p>
