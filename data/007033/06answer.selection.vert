<s>
Americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
na	na	k7c4	na
japonský	japonský	k2eAgInSc4d1	japonský
ostrov	ostrov	k1gInSc4	ostrov
Iwodžima	Iwodžimum	k1gNnSc2	Iwodžimum
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
obsadit	obsadit	k5eAaPmF	obsadit
zdejší	zdejší	k2eAgNnSc4d1	zdejší
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
si	se	k3xPyFc3	se
tak	tak	k9	tak
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
útok	útok	k1gInSc4	útok
na	na	k7c4	na
hlavní	hlavní	k2eAgInPc4d1	hlavní
japonské	japonský	k2eAgInPc4d1	japonský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
