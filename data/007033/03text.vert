<s>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Iwodžimu	Iwodžimo	k1gNnSc6	Iwodžimo
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Battle	Battle	k1gFnSc1	Battle
of	of	k?	of
Iwo	Iwo	k1gMnSc2	Iwo
Jima	Jimus	k1gMnSc2	Jimus
<g/>
;	;	kIx,	;
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
硫	硫	k?	硫
<g/>
,	,	kIx,	,
Iótó	Iótó	k1gFnSc1	Iótó
no	no	k9	no
tatakai	tatakai	k1gNnSc4	tatakai
nebo	nebo	k8xC	nebo
Iódžima	Iódžima	k1gNnSc4	Iódžima
no	no	k9	no
tatakai	tatakai	k6eAd1	tatakai
<g/>
)	)	kIx)	)
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
do	do	k7c2	do
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
na	na	k7c4	na
japonský	japonský	k2eAgInSc4d1	japonský
ostrov	ostrov	k1gInSc4	ostrov
Iwodžima	Iwodžimum	k1gNnSc2	Iwodžimum
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
obsadit	obsadit	k5eAaPmF	obsadit
zdejší	zdejší	k2eAgNnSc4d1	zdejší
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
si	se	k3xPyFc3	se
tak	tak	k9	tak
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
útok	útok	k1gInSc4	útok
na	na	k7c4	na
hlavní	hlavní	k2eAgInPc4d1	hlavní
japonské	japonský	k2eAgInPc4d1	japonský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
případ	případ	k1gInSc4	případ
celé	celý	k2eAgFnSc2d1	celá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
cílem	cíl	k1gInSc7	cíl
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
jednotek	jednotka	k1gFnPc2	jednotka
obsazení	obsazení	k1gNnSc2	obsazení
vlastního	vlastní	k2eAgNnSc2d1	vlastní
japonského	japonský	k2eAgNnSc2d1	Japonské
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
Japonska	Japonsko	k1gNnSc2	Japonsko
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc4	jehož
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
také	také	k6eAd1	také
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
i	i	k9	i
intenzita	intenzita	k1gFnSc1	intenzita
odporu	odpor	k1gInSc2	odpor
japonských	japonský	k2eAgMnPc2d1	japonský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc4d1	malý
ostrov	ostrov	k1gInSc4	ostrov
skončí	skončit	k5eAaPmIp3nS	skončit
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
opak	opak	k1gInSc4	opak
však	však	k9	však
byl	být	k5eAaImAgMnS	být
pravdou	pravda	k1gFnSc7	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Vynalézavost	vynalézavost	k1gFnSc1	vynalézavost
generála	generál	k1gMnSc2	generál
Kuribajašiho	Kuribajaši	k1gMnSc2	Kuribajaši
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
zejména	zejména	k9	zejména
hory	hora	k1gFnPc1	hora
Suribači	Suribač	k1gMnPc1	Suribač
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
podzemních	podzemní	k2eAgFnPc2d1	podzemní
pevností	pevnost	k1gFnPc2	pevnost
celého	celý	k2eAgNnSc2d1	celé
Japonska	Japonsko	k1gNnSc2	Japonsko
(	(	kIx(	(
<g/>
opevnění	opevnění	k1gNnSc1	opevnění
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
18	[number]	k4	18
km	km	kA	km
podzemních	podzemní	k2eAgInPc2d1	podzemní
tunelů	tunel	k1gInPc2	tunel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
odhodlání	odhodlání	k1gNnSc4	odhodlání
japonské	japonský	k2eAgFnSc2d1	japonská
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
od	od	k7c2	od
japonského	japonský	k2eAgNnSc2d1	Japonské
velení	velení	k1gNnSc2	velení
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
boj	boj	k1gInSc1	boj
o	o	k7c4	o
ostrov	ostrov	k1gInSc4	ostrov
navzdory	navzdory	k6eAd1	navzdory
zdrcující	zdrcující	k2eAgFnSc6d1	zdrcující
materiální	materiální	k2eAgFnSc6d1	materiální
i	i	k8xC	i
početní	početní	k2eAgFnSc6d1	početní
převaze	převaha	k1gFnSc6	převaha
Spojenců	spojenec	k1gMnPc2	spojenec
protáhl	protáhnout	k5eAaPmAgInS	protáhnout
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejkrvavějších	krvavý	k2eAgFnPc2d3	nejkrvavější
bitev	bitva	k1gFnPc2	bitva
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
ostrovní	ostrovní	k2eAgFnSc2d1	ostrovní
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vylodění	vylodění	k1gNnSc3	vylodění
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
dorazil	dorazit	k5eAaPmAgInS	dorazit
první	první	k4xOgInSc1	první
americký	americký	k2eAgInSc1d1	americký
vyloďovací	vyloďovací	k2eAgInSc1d1	vyloďovací
člun	člun	k1gInSc1	člun
k	k	k7c3	k
pláži	pláž	k1gFnSc3	pláž
Futacune	Futacun	k1gInSc5	Futacun
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
pěchota	pěchota	k1gFnSc1	pěchota
a	a	k8xC	a
dělostřelectvo	dělostřelectvo	k1gNnSc1	dělostřelectvo
mlčelo	mlčet	k5eAaImAgNnS	mlčet
a	a	k8xC	a
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
nenaráželi	narážet	k5eNaImAgMnP	narážet
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
zmateni	zmást	k5eAaPmNgMnP	zmást
takovýmto	takovýto	k3xDgInSc7	takovýto
vývojem	vývoj	k1gInSc7	vývoj
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
postupovat	postupovat	k5eAaImF	postupovat
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
překážka	překážka	k1gFnSc1	překážka
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
po	po	k7c6	po
300	[number]	k4	300
metrech	metr	k1gInPc6	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
strmém	strmý	k2eAgInSc6d1	strmý
svahu	svah	k1gInSc6	svah
tvořeném	tvořený	k2eAgInSc6d1	tvořený
sopečným	sopečný	k2eAgInPc3d1	sopečný
popelem	popel	k1gInSc7	popel
vojáci	voják	k1gMnPc1	voják
americké	americký	k2eAgFnSc2d1	americká
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
podkluzovali	podkluzovat	k5eAaImAgMnP	podkluzovat
a	a	k8xC	a
padali	padat	k5eAaImAgMnP	padat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
Kuribajašiho	Kuribajaši	k1gMnSc2	Kuribajaši
dělostřelectvo	dělostřelectvo	k1gNnSc1	dělostřelectvo
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
palbu	palba	k1gFnSc4	palba
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
palby	palba	k1gFnSc2	palba
přicházelo	přicházet	k5eAaImAgNnS	přicházet
z	z	k7c2	z
vyhaslé	vyhaslý	k2eAgFnSc2d1	vyhaslá
sopky	sopka	k1gFnSc2	sopka
Suribači	Suribač	k1gMnPc1	Suribač
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
ztráty	ztráta	k1gFnPc1	ztráta
začaly	začít	k5eAaPmAgFnP	začít
narůstat	narůstat	k5eAaImF	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Vylodění	vylodění	k1gNnSc1	vylodění
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
bylo	být	k5eAaImAgNnS	být
nejkrvavějším	krvavý	k2eAgMnSc7d3	nejkrvavější
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
války	válka	k1gFnSc2	válka
o	o	k7c6	o
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
večera	večer	k1gInSc2	večer
padlo	padnout	k5eAaImAgNnS	padnout
600	[number]	k4	600
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
1	[number]	k4	1
200	[number]	k4	200
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
příštího	příští	k2eAgNnSc2d1	příští
rána	ráno	k1gNnSc2	ráno
byly	být	k5eAaImAgFnP	být
pláže	pláž	k1gFnPc1	pláž
obsazeny	obsazen	k2eAgFnPc1d1	obsazena
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
přistály	přistát	k5eAaImAgFnP	přistát
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
první	první	k4xOgFnSc2	první
americké	americký	k2eAgFnSc2d1	americká
posily	posila	k1gFnSc2	posila
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
zahájen	zahájit	k5eAaPmNgInS	zahájit
útok	útok	k1gInSc1	útok
na	na	k7c4	na
sopku	sopka	k1gFnSc4	sopka
Suribači	Suribač	k1gMnSc3	Suribač
<g/>
,	,	kIx,	,
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
urputným	urputný	k2eAgInSc7d1	urputný
odporem	odpor	k1gInSc7	odpor
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
probíhaly	probíhat	k5eAaImAgInP	probíhat
těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
i	i	k9	i
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
4	[number]	k4	4
Kamikaze	kamikaze	k1gMnPc2	kamikaze
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
Saratoga	Saratoga	k1gFnSc1	Saratoga
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tímto	tento	k3xDgInSc7	tento
útokem	útok	k1gInSc7	útok
tak	tak	k6eAd1	tak
vážně	vážně	k6eAd1	vážně
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
musela	muset	k5eAaImAgFnS	muset
odplout	odplout	k5eAaPmF	odplout
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
potopena	potopen	k2eAgFnSc1d1	potopena
eskortní	eskortní	k2eAgFnSc1d1	eskortní
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
USS	USS	kA	USS
Bismarck	Bismarck	k1gMnSc1	Bismarck
sea	sea	k?	sea
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
boje	boj	k1gInPc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
nový	nový	k2eAgInSc1d1	nový
útok	útok	k1gInSc1	útok
proti	proti	k7c3	proti
sopce	sopka	k1gFnSc3	sopka
Suribači	Suribač	k1gMnPc1	Suribač
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útok	útok	k1gInSc1	útok
překvapivě	překvapivě	k6eAd1	překvapivě
uspěl	uspět	k5eAaPmAgInS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
dostávali	dostávat	k5eAaImAgMnP	dostávat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
sopky	sopka	k1gFnSc2	sopka
a	a	k8xC	a
v	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
zde	zde	k6eAd1	zde
šest	šest	k4xCc1	šest
příslušníků	příslušník	k1gMnPc2	příslušník
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
vztyčilo	vztyčit	k5eAaPmAgNnS	vztyčit
vlajku	vlajka	k1gFnSc4	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Obsazení	obsazení	k1gNnSc1	obsazení
sopky	sopka	k1gFnSc2	sopka
Suribači	Suribač	k1gMnSc3	Suribač
nebylo	být	k5eNaImAgNnS	být
úplným	úplný	k2eAgNnSc7d1	úplné
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrazně	výrazně	k6eAd1	výrazně
to	ten	k3xDgNnSc1	ten
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
morálku	morálka	k1gFnSc4	morálka
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Iwodžimu	Iwodžima	k1gFnSc4	Iwodžima
dál	daleko	k6eAd2	daleko
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
probíjela	probíjet	k5eAaImAgFnS	probíjet
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
přistály	přistát	k5eAaPmAgInP	přistát
první	první	k4xOgInPc1	první
bombardéry	bombardér	k1gInPc1	bombardér
B-29	B-29	k1gFnSc7	B-29
na	na	k7c6	na
čerstvě	čerstvě	k6eAd1	čerstvě
dobytých	dobytý	k2eAgNnPc6d1	dobyté
letištích	letiště	k1gNnPc6	letiště
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
okolo	okolo	k6eAd1	okolo
stále	stále	k6eAd1	stále
zuřila	zuřit	k5eAaImAgFnS	zuřit
bitva	bitva	k1gFnSc1	bitva
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byli	být	k5eAaImAgMnP	být
japonští	japonský	k2eAgMnPc1d1	japonský
obránci	obránce	k1gMnPc1	obránce
zatlačeni	zatlačit	k5eAaPmNgMnP	zatlačit
až	až	k6eAd1	až
do	do	k7c2	do
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
části	část	k1gFnSc2	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
půli	půle	k1gFnSc6	půle
března	březen	k1gInSc2	březen
Američané	Američan	k1gMnPc1	Američan
zachytili	zachytit	k5eAaPmAgMnP	zachytit
vysílání	vysílání	k1gNnSc4	vysílání
generála	generál	k1gMnSc2	generál
Kuribajašiho	Kuribajaši	k1gMnSc2	Kuribajaši
<g/>
.	.	kIx.	.
</s>
<s>
Znělo	znět	k5eAaImAgNnS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejedli	jíst	k5eNaImAgMnP	jíst
jsme	být	k5eAaImIp1nP	být
ani	ani	k8xC	ani
nepili	pít	k5eNaImAgMnP	pít
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
náš	náš	k3xOp1gMnSc1	náš
bojový	bojový	k2eAgMnSc1d1	bojový
duch	duch	k1gMnSc1	duch
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Budeme	být	k5eAaImBp1nP	být
bojovat	bojovat	k5eAaImF	bojovat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bitva	bitva	k1gFnSc1	bitva
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zbytky	zbytek	k1gInPc1	zbytek
japonské	japonský	k2eAgFnSc2d1	japonská
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
již	již	k6eAd1	již
neměly	mít	k5eNaImAgFnP	mít
dost	dost	k6eAd1	dost
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
munice	munice	k1gFnSc2	munice
ani	ani	k8xC	ani
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
pro	pro	k7c4	pro
sebevražedný	sebevražedný	k2eAgInSc4d1	sebevražedný
útok	útok	k1gInSc4	útok
na	na	k7c4	na
spojenecké	spojenecký	k2eAgFnPc4d1	spojenecká
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
několika	několik	k4yIc2	několik
set	set	k1gInSc4	set
japonských	japonský	k2eAgMnPc2d1	japonský
vojáků	voják	k1gMnPc2	voják
tak	tak	k6eAd1	tak
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
odpor	odpor	k1gInSc4	odpor
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
a	a	k8xC	a
ostrov	ostrov	k1gInSc4	ostrov
byl	být	k5eAaImAgInS	být
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
prohlášen	prohlášen	k2eAgInSc1d1	prohlášen
za	za	k7c4	za
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
daný	daný	k2eAgInSc1d1	daný
útok	útok	k1gInSc1	útok
nepředstavoval	představovat	k5eNaImAgInS	představovat
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
lepší	dobrý	k2eAgFnSc4d2	lepší
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
poslední	poslední	k2eAgInSc4d1	poslední
japonský	japonský	k2eAgInSc4d1	japonský
úspěch	úspěch	k1gInSc4	úspěch
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
spojenecké	spojenecký	k2eAgFnPc1d1	spojenecká
jednotky	jednotka	k1gFnPc1	jednotka
podcenily	podcenit	k5eAaPmAgFnP	podcenit
rizika	riziko	k1gNnSc2	riziko
takovéto	takovýto	k3xDgFnSc2	takovýto
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
Japoncům	Japonec	k1gMnPc3	Japonec
podařilo	podařit	k5eAaPmAgNnS	podařit
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
prostor	prostora	k1gFnPc2	prostora
letiště	letiště	k1gNnSc2	letiště
a	a	k8xC	a
pobít	pobít	k5eAaPmF	pobít
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
pilotů	pilot	k1gMnPc2	pilot
a	a	k8xC	a
letištního	letištní	k2eAgInSc2d1	letištní
personálu	personál	k1gInSc2	personál
<g/>
,	,	kIx,	,
kteréžto	kteréžto	k?	kteréžto
ztráty	ztráta	k1gFnSc2	ztráta
nebylo	být	k5eNaImAgNnS	být
lehké	lehký	k2eAgNnSc1d1	lehké
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
ukončení	ukončení	k1gNnSc6	ukončení
bitvy	bitva	k1gFnSc2	bitva
zde	zde	k6eAd1	zde
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
japonští	japonský	k2eAgMnPc1d1	japonský
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnPc1d1	poslední
dva	dva	k4xCgMnPc1	dva
(	(	kIx(	(
<g/>
kulometčíci	kulometčík	k1gMnPc1	kulometčík
Macudo	Macudo	k1gNnSc4	Macudo
Linsoki	Linsoki	k1gNnSc2	Linsoki
a	a	k8xC	a
Jamakage	Jamakage	k1gNnSc2	Jamakage
Kufuku	Kufuk	k1gInSc2	Kufuk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vzdali	vzdát	k5eAaPmAgMnP	vzdát
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Suribači	Suribač	k1gMnPc1	Suribač
pořídil	pořídit	k5eAaPmAgInS	pořídit
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
Joe	Joe	k1gMnSc1	Joe
Rosenthal	Rosenthal	k1gMnSc1	Rosenthal
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
fotografií	fotografia	k1gFnPc2	fotografia
celé	celý	k2eAgFnSc2d1	celá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
bitvy	bitva	k1gFnSc2	bitva
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostrov	ostrov	k1gInSc1	ostrov
zůstal	zůstat	k5eAaPmAgInS	zůstat
okupován	okupovat	k5eAaBmNgInS	okupovat
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
the	the	k?	the
Shores	Shores	k1gInSc1	Shores
of	of	k?	of
Iwo	Iwo	k1gFnSc1	Iwo
Jima	Jima	k1gFnSc1	Jima
-	-	kIx~	-
americký	americký	k2eAgInSc1d1	americký
dokument	dokument	k1gInSc1	dokument
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Glamour	Glamour	k1gMnSc1	Glamour
Gal	Gal	k1gMnSc1	Gal
-	-	kIx~	-
americký	americký	k2eAgInSc1d1	americký
propagandistický	propagandistický	k2eAgInSc1d1	propagandistický
dokument	dokument	k1gInSc1	dokument
o	o	k7c4	o
artilérii	artilérie	k1gFnSc4	artilérie
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
Old	Olda	k1gFnPc2	Olda
Glory	Glora	k1gFnSc2	Glora
Flies	Flies	k1gMnSc1	Flies
of	of	k?	of
Iwo	Iwo	k1gMnSc1	Iwo
Jima	Jima	k1gMnSc1	Jima
-	-	kIx~	-
krátký	krátký	k2eAgInSc1d1	krátký
americký	americký	k2eAgInSc1d1	americký
propagandistický	propagandistický	k2eAgInSc1d1	propagandistický
klip	klip	k1gInSc1	klip
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
dostupný	dostupný	k2eAgInSc4d1	dostupný
společně	společně	k6eAd1	společně
s	s	k7c7	s
klipem	klip	k1gInSc7	klip
Carriers	Carriersa	k1gFnPc2	Carriersa
Hit	hit	k1gInSc1	hit
Tokyo	Tokyo	k6eAd1	Tokyo
<g/>
!	!	kIx.	!
</s>
<s>
o	o	k7c6	o
bombardování	bombardování	k1gNnSc6	bombardování
Tokia	Tokio	k1gNnSc2	Tokio
Sands	Sandsa	k1gFnPc2	Sandsa
of	of	k?	of
Iwo	Iwo	k1gMnSc1	Iwo
Jima	Jima	k1gMnSc1	Jima
-	-	kIx~	-
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Wayne	Wayn	k1gMnSc5	Wayn
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Outsider	outsider	k1gMnSc1	outsider
-	-	kIx~	-
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Tony	Tony	k1gMnSc1	Tony
Curtis	Curtis	k1gFnSc2	Curtis
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
našich	náš	k3xOp1gMnPc2	náš
otců	otec	k1gMnPc2	otec
(	(	kIx(	(
<g/>
Flags	Flags	k1gInSc1	Flags
of	of	k?	of
Our	Our	k1gFnSc2	Our
Fathers	Fathersa	k1gFnPc2	Fathersa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dopisy	dopis	k1gInPc1	dopis
z	z	k7c2	z
Iwodžimy	Iwodžima	k1gFnSc2	Iwodžima
(	(	kIx(	(
<g/>
Letters	Letters	k1gInSc1	Letters
from	from	k1gMnSc1	from
Iwo	Iwo	k1gMnSc1	Iwo
Jima	Jima	k1gMnSc1	Jima
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Red	Red	k1gMnPc1	Red
Sun	Sun	kA	Sun
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sand	Sand	k1gMnSc1	Sand
<g/>
)	)	kIx)	)
-	-	kIx~	-
dva	dva	k4xCgInPc4	dva
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
pohledu	pohled	k1gInSc2	pohled
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
z	z	k7c2	z
japonského	japonský	k2eAgMnSc2d1	japonský
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Clint	Clint	k1gMnSc1	Clint
Eastwood	Eastwooda	k1gFnPc2	Eastwooda
<g/>
.	.	kIx.	.
</s>
