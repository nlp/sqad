<s>
Anssi	Ansse	k1gFnSc4
Koivuranta	Koivurant	k1gMnSc2
</s>
<s>
Anssi	Ansse	k1gFnSc4
Koivuranta	Koivuranta	k1gMnSc1
Anssi	Anssi	k1gMnSc1
Koivuranta	Koivurant	k1gMnSc2
Datum	datum	k1gInSc4
narození	narození	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1988	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Kuusamo	Kuusamo	k6eAd1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
172	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
55	#num#	k4
kg	kg	kA
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.anssikoivuranta.fi	www.anssikoivuranta.fi	k6eAd1
Sportovní	sportovní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Sport	sport	k1gInSc1
</s>
<s>
Severská	severský	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
Klub	klub	k1gInSc1
</s>
<s>
Kuusamon	Kuusamon	k1gInSc1
Erä-Veikot	Erä-Veikot	k1gInSc1
Medailový	medailový	k2eAgInSc1d1
zisk	zisk	k1gInSc4
Olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
0	#num#	k4
-	-	kIx~
0	#num#	k4
-	-	kIx~
1	#num#	k4
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
1	#num#	k4
-	-	kIx~
0	#num#	k4
-	-	kIx~
1	#num#	k4
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktualizovány	aktualizován	k2eAgInPc4d1
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Severská	severský	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
na	na	k7c4
ZOH	ZOH	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ZOH	ZOH	kA
2006	#num#	k4
</s>
<s>
Závod	závod	k1gInSc1
družstev	družstvo	k1gNnPc2
-	-	kIx~
4	#num#	k4
×	×	k?
5	#num#	k4
km	km	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
klasickém	klasický	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
2007	#num#	k4
</s>
<s>
Závod	závod	k1gInSc1
družstev	družstvo	k1gNnPc2
-	-	kIx~
4	#num#	k4
×	×	k?
5	#num#	k4
km	km	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
2007	#num#	k4
</s>
<s>
Individuální	individuální	k2eAgInSc1d1
závod	závod	k1gInSc1
na	na	k7c4
15	#num#	k4
km	km	kA
</s>
<s>
Anssi	Ansse	k1gFnSc3
Einar	Einar	k1gMnSc1
Koivuranta	Koivurant	k1gMnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1988	#num#	k4
<g/>
,	,	kIx,
Kuusamo	Kuusama	k1gFnSc5
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
finský	finský	k2eAgMnSc1d1
skokan	skokan	k1gMnSc1
na	na	k7c6
lyžích	lyže	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
se	se	k3xPyFc4
specializoval	specializovat	k5eAaBmAgMnS
na	na	k7c4
Severskou	severský	k2eAgFnSc4d1
kombinaci	kombinace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
je	být	k5eAaImIp3nS
zisk	zisk	k1gInSc1
zlaté	zlatý	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
v	v	k7c6
závodu	závod	k1gInSc6
družstev	družstvo	k1gNnPc2
4	#num#	k4
×	×	k?
5	#num#	k4
km	km	kA
na	na	k7c6
šampionátu	šampionát	k1gInSc6
v	v	k7c6
Sapporu	Sappor	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimoto	mimoto	k6eAd1
je	být	k5eAaImIp3nS
držitelem	držitel	k1gMnSc7
bronzové	bronzový	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
z	z	k7c2
individuálního	individuální	k2eAgInSc2d1
závodu	závod	k1gInSc2
na	na	k7c4
15	#num#	k4
km	km	kA
<g/>
,	,	kIx,
taktéž	taktéž	k?
v	v	k7c6
Sapporu	Sappor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výčet	výčet	k1gInSc1
jeho	jeho	k3xOp3gInPc2
úspěchů	úspěch	k1gInPc2
doplňuje	doplňovat	k5eAaImIp3nS
bronz	bronz	k1gInSc4
ze	z	k7c2
závodu	závod	k1gInSc2
družstev	družstvo	k1gNnPc2
4	#num#	k4
×	×	k?
5	#num#	k4
km	km	kA
na	na	k7c6
zimní	zimní	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Turíně	Turín	k1gInSc6
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Koivuranta	Koivurant	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
závodí	závodit	k5eAaImIp3nS
za	za	k7c4
klub	klub	k1gInSc4
Kuusamon	Kuusamon	k1gMnSc1
Erä	Erä	k1gMnSc1
Veikot	Veikot	k1gInSc4
z	z	k7c2
finského	finský	k2eAgNnSc2d1
města	město	k1gNnSc2
Kuusamo	Kuusama	k1gFnSc5
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
skákat	skákat	k5eAaImF
na	na	k7c6
lyžích	lyže	k1gFnPc6
ve	v	k7c6
věku	věk	k1gInSc6
šesti	šest	k4xCc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
osmi	osm	k4xCc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
začal	začít	k5eAaPmAgMnS
věnovat	věnovat	k5eAaPmF,k5eAaImF
severské	severský	k2eAgFnSc3d1
kombinaci	kombinace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
v	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
debutoval	debutovat	k5eAaBmAgMnS
ve	v	k7c6
skupině	skupina	k1gFnSc6
B	B	kA
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
však	však	k9
diskvalifikován	diskvalifikován	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
2004	#num#	k4
získal	získat	k5eAaPmAgMnS
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
na	na	k7c6
juniorském	juniorský	k2eAgInSc6d1
světovém	světový	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
ve	v	k7c6
Strynu	Strynu	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
opravdový	opravdový	k2eAgInSc4d1
debut	debut	k1gInSc4
Koivuranta	Koivurant	k1gMnSc2
prodělal	prodělat	k5eAaPmAgMnS
ve	v	k7c6
Světovém	světový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
na	na	k7c6
konci	konec	k1gInSc6
sezóny	sezóna	k1gFnSc2
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
závodil	závodit	k5eAaImAgMnS
na	na	k7c6
dvou	dva	k4xCgFnPc6
tratích	trať	k1gFnPc6
v	v	k7c6
Lahti	Lahť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skončil	skončit	k5eAaPmAgMnS
na	na	k7c6
23	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
sezóny	sezóna	k1gFnSc2
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
skončil	skončit	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
první	první	k4xOgFnSc6
desítce	desítka	k1gFnSc6
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
osmý	osmý	k4xOgMnSc1
<g/>
,	,	kIx,
ve	v	k7c6
sprintu	sprint	k1gInSc6
v	v	k7c6
Trondheimu	Trondheim	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svého	své	k1gNnSc2
prvního	první	k4xOgNnSc2
umístění	umístění	k1gNnSc2
na	na	k7c6
stupních	stupeň	k1gInPc6
vítězů	vítěz	k1gMnPc2
dosáhl	dosáhnout	k5eAaPmAgInS
v	v	k7c6
lednu	leden	k1gInSc6
2005	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
skončil	skončit	k5eAaPmAgMnS
na	na	k7c6
bronzové	bronzový	k2eAgFnSc6d1
příčce	příčka	k1gFnSc6
v	v	k7c6
Sapporu	Sappor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
koncem	konec	k1gInSc7
sezóny	sezóna	k1gFnSc2
získal	získat	k5eAaPmAgInS
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
na	na	k7c6
juniorském	juniorský	k2eAgInSc6d1
Světovém	světový	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
v	v	k7c6
Rovaniemi	Rovanie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezónu	sezóna	k1gFnSc4
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
Koivuranta	Koivurant	k1gMnSc2
listopadovým	listopadový	k2eAgFnPc3d1
druhým	druhý	k4xOgNnSc7
místem	místo	k1gNnSc7
v	v	k7c6
závodě	závod	k1gInSc6
v	v	k7c6
Kuusamu	Kuusam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgInSc6d1
průběhu	průběh	k1gInSc6
sezóny	sezóna	k1gFnSc2
dosáhl	dosáhnout	k5eAaPmAgInS
jednou	jeden	k4xCgFnSc7
druhé	druhý	k4xOgFnSc6
a	a	k8xC
dvakrát	dvakrát	k6eAd1
třetí	třetí	k4xOgFnPc4
pozice	pozice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgInS
také	také	k9
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
bronz	bronz	k1gInSc4
z	z	k7c2
velké	velký	k2eAgFnSc2d1
mezinárodní	mezinárodní	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
,	,	kIx,
Zimní	zimní	k2eAgFnSc2d1
olympiády	olympiáda	k1gFnSc2
2006	#num#	k4
v	v	k7c6
Turíně	Turín	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ze	z	k7c2
závodu	závod	k1gInSc2
družstev	družstvo	k1gNnPc2
na	na	k7c4
4	#num#	k4
×	×	k?
5	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
2006	#num#	k4
skončil	skončit	k5eAaPmAgInS
dvakrát	dvakrát	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
ve	v	k7c6
světovém	světový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
pravidelně	pravidelně	k6eAd1
se	se	k3xPyFc4
objevoval	objevovat	k5eAaImAgInS
v	v	k7c6
první	první	k4xOgFnSc6
patnáctce	patnáctka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Světovém	světový	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
získal	získat	k5eAaPmAgMnS
Koivuranta	Koivurant	k1gMnSc2
zlato	zlato	k1gNnSc1
a	a	k8xC
bronz	bronz	k1gInSc4
<g/>
,	,	kIx,
mimoto	mimoto	k6eAd1
skončil	skončit	k5eAaPmAgMnS
čtvrtý	čtvrtý	k4xOgMnSc1
ve	v	k7c6
sprintu	sprint	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
také	také	k6eAd1
zúčastnil	zúčastnit	k5eAaPmAgMnS
svého	svůj	k3xOyFgInSc2
posledního	poslední	k2eAgInSc2d1
juniorského	juniorský	k2eAgInSc2d1
šampionátu	šampionát	k1gInSc2
v	v	k7c6
Travisu	Travis	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
zlato	zlato	k1gNnSc4
a	a	k8xC
stříbro	stříbro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Koivuranta	Koivurant	k1gMnSc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c7
sdruženáři	sdruženář	k1gMnPc7
spíše	spíše	k9
mezi	mezi	k7c4
lepší	dobrý	k2eAgMnPc4d2
skokany	skokan	k1gMnPc4
<g/>
,	,	kIx,
v	v	k7c6
posledních	poslední	k2eAgFnPc6d1
sezónách	sezóna	k1gFnPc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
ale	ale	k9
povedlo	povést	k5eAaPmAgNnS
též	též	k9
výrazně	výrazně	k6eAd1
vylepšit	vylepšit	k5eAaPmF
běžecké	běžecký	k2eAgFnPc4d1
dovednosti	dovednost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
na	na	k7c4
skoky	skok	k1gInPc4
na	na	k7c6
lyžích	lyže	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Anssi	Ansse	k1gFnSc4
Koivuranta	Koivurant	k1gMnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Anssi	Ansse	k1gFnSc4
Koivuranta	Koivurant	k1gMnSc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Finsko	Finsko	k1gNnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
