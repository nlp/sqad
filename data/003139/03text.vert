<s>
Komodor	komodor	k1gMnSc1	komodor
je	být	k5eAaImIp3nS	být
námořní	námořní	k2eAgFnSc1d1	námořní
důstojnická	důstojnický	k2eAgFnSc1d1	důstojnická
armádní	armádní	k2eAgFnSc1d1	armádní
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
námořní	námořní	k2eAgMnSc1d1	námořní
kapitán	kapitán	k1gMnSc1	kapitán
a	a	k8xC	a
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
kontradmirál	kontradmirál	k1gMnSc1	kontradmirál
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
letectvu	letectvo	k1gNnSc6	letectvo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
letectvu	letectvo	k1gNnSc6	letectvo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hodnost	hodnost	k1gFnSc1	hodnost
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
letecký	letecký	k2eAgInSc1d1	letecký
vicemaršál	vicemaršál	k1gInSc1	vicemaršál
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
zařazena	zařazen	k2eAgFnSc1d1	zařazena
jako	jako	k8xS	jako
jednohvězdičková	jednohvězdičkový	k2eAgFnSc1d1	jednohvězdičková
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
kódu	kód	k1gInSc6	kód
NATO	NATO	kA	NATO
OF-	OF-	k1gFnSc1	OF-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
vždy	vždy	k6eAd1	vždy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
vlajkovou	vlajkový	k2eAgFnSc4d1	vlajková
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
zkracována	zkracován	k2eAgFnSc1d1	zkracována
jako	jako	k8xC	jako
Cdre	Cdre	k1gFnSc1	Cdre
<g/>
,	,	kIx,	,
CDRE	CDRE	kA	CDRE
či	či	k8xC	či
COMO	COMO	kA	COMO
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
hodnosti	hodnost	k1gFnSc2	hodnost
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
francouzského	francouzský	k2eAgMnSc2d1	francouzský
commandeur	commandeur	k1gMnSc1	commandeur
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hodností	hodnost	k1gFnPc2	hodnost
v	v	k7c6	v
rytířských	rytířský	k2eAgInPc6d1	rytířský
řádech	řád	k1gInPc6	řád
-	-	kIx~	-
titul	titul	k1gInSc1	titul
komtura	komtur	k1gMnSc2	komtur
-	-	kIx~	-
rytíře	rytíř	k1gMnSc4	rytíř
velícího	velící	k2eAgMnSc4d1	velící
komendě	komenda	k1gFnSc3	komenda
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
řádového	řádový	k2eAgNnSc2d1	řádové
území	území	k1gNnSc2	území
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
jazykového	jazykový	k2eAgInSc2d1	jazykový
základu	základ	k1gInSc2	základ
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
hodnost	hodnost	k1gFnSc4	hodnost
komandor	komandora	k1gFnPc2	komandora
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
užívá	užívat	k5eAaImIp3nS	užívat
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ale	ale	k8xC	ale
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hodnosti	hodnost	k1gFnSc2	hodnost
námořního	námořní	k2eAgMnSc2d1	námořní
kapitána	kapitán	k1gMnSc2	kapitán
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
komodora	komodor	k1gMnSc2	komodor
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
jako	jako	k8xS	jako
dočasný	dočasný	k2eAgInSc4d1	dočasný
titul	titul	k1gInSc4	titul
pro	pro	k7c4	pro
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nadřazena	nadřadit	k5eAaPmNgFnS	nadřadit
kapitánům	kapitán	k1gMnPc3	kapitán
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
lodí	loď	k1gFnPc2	loď
v	v	k7c6	v
eskadře	eskadra	k1gFnSc6	eskadra
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
nahlíženo	nahlížen	k2eAgNnSc1d1	nahlíženo
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
na	na	k7c4	na
vyššího	vysoký	k2eAgMnSc4d2	vyšší
kapitána	kapitán	k1gMnSc4	kapitán
<g/>
,	,	kIx,	,
jiná	jiná	k1gFnSc1	jiná
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
vyzdvihla	vyzdvihnout	k5eAaPmAgFnS	vyzdvihnout
mezi	mezi	k7c4	mezi
vlajkové	vlajkový	k2eAgFnPc4d1	vlajková
hodnosti	hodnost	k1gFnPc4	hodnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
irské	irský	k2eAgFnSc6d1	irská
Námořní	námořní	k2eAgFnSc6d1	námořní
službě	služba	k1gFnSc6	služba
je	být	k5eAaImIp3nS	být
komodor	komodor	k1gMnSc1	komodor
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
hodnost	hodnost	k1gFnSc1	hodnost
zavedena	zavést	k5eAaPmNgFnS	zavést
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zamezilo	zamezit	k5eAaPmAgNnS	zamezit
jmenování	jmenování	k1gNnSc3	jmenování
dalších	další	k2eAgMnPc2d1	další
admirálů	admirál	k1gMnPc2	admirál
-	-	kIx~	-
ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
tak	tak	k6eAd1	tak
velké	velký	k2eAgFnSc6d1	velká
flotě	flota	k1gFnSc6	flota
velmi	velmi	k6eAd1	velmi
nákladná	nákladný	k2eAgFnSc1d1	nákladná
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
vojenské	vojenský	k2eAgNnSc4d1	vojenské
prostředí	prostředí	k1gNnSc4	prostředí
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
může	moct	k5eAaImIp3nS	moct
označovat	označovat	k5eAaImF	označovat
předseda	předseda	k1gMnSc1	předseda
jachtového	jachtový	k2eAgInSc2d1	jachtový
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
služebně	služebně	k6eAd1	služebně
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
kapitánů	kapitán	k1gMnPc2	kapitán
společnosti	společnost	k1gFnSc2	společnost
provozující	provozující	k2eAgFnSc4d1	provozující
komerční	komerční	k2eAgFnSc4d1	komerční
námořní	námořní	k2eAgFnSc4d1	námořní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Commodore	Commodor	k1gInSc5	Commodor
(	(	kIx(	(
<g/>
rank	rank	k1gInSc4	rank
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Admirál	admirál	k1gMnSc1	admirál
flotily	flotila	k1gFnSc2	flotila
Komodor	komodor	k1gMnSc1	komodor
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
Komandor	Komandor	k1gMnSc1	Komandor
Komandér	komandér	k1gMnSc1	komandér
Kapitán	kapitán	k1gMnSc1	kapitán
(	(	kIx(	(
<g/>
námořní	námořní	k2eAgFnSc1d1	námořní
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
)	)	kIx)	)
</s>
