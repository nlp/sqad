<s>
Jánský	jánský	k2eAgInSc1d1	jánský
vrch	vrch	k1gInSc1	vrch
je	být	k5eAaImIp3nS	být
leucititový	leucititový	k2eAgInSc1d1	leucititový
kopec	kopec	k1gInSc1	kopec
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
340	[number]	k4	340
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
severně	severně	k6eAd1	severně
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Korozluky	Korozluk	k1gInPc7	Korozluk
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
