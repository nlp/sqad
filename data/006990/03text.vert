<s>
Jánský	jánský	k2eAgInSc1d1	jánský
vrch	vrch	k1gInSc1	vrch
je	být	k5eAaImIp3nS	být
leucititový	leucititový	k2eAgInSc1d1	leucititový
kopec	kopec	k1gInSc1	kopec
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
340	[number]	k4	340
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
severně	severně	k6eAd1	severně
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Korozluky	Korozluk	k1gInPc7	Korozluk
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
kopce	kopec	k1gInSc2	kopec
mezi	mezi	k7c7	mezi
sousedním	sousední	k2eAgInSc7d1	sousední
vrchem	vrch	k1gInSc7	vrch
Špičák	Špičák	k1gMnSc1	Špičák
(	(	kIx(	(
<g/>
360	[number]	k4	360
m	m	kA	m
<g/>
)	)	kIx)	)
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Dobrčice	Dobrčice	k1gFnSc2	Dobrčice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
lom	lom	k1gInSc1	lom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
těžil	těžit	k5eAaImAgMnS	těžit
porcelanit	porcelanit	k1gInSc4	porcelanit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižních	jižní	k2eAgInPc6d1	jižní
a	a	k8xC	a
západních	západní	k2eAgInPc6d1	západní
svazích	svah	k1gInPc6	svah
Jánského	Jánského	k2eAgInSc2d1	Jánského
vrchu	vrch	k1gInSc2	vrch
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1951	[number]	k4	1951
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
12	[number]	k4	12
ha	ha	kA	ha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
sucho	sucho	k6eAd1	sucho
a	a	k8xC	a
teplomilné	teplomilný	k2eAgFnPc1d1	teplomilná
květeny	květena	k1gFnPc1	květena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
ani	ani	k8xC	ani
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
chráněného	chráněný	k2eAgNnSc2d1	chráněné
území	území	k1gNnSc2	území
nevede	vést	k5eNaImIp3nS	vést
žádná	žádný	k3yNgFnSc1	žádný
turisticky	turisticky	k6eAd1	turisticky
značená	značený	k2eAgFnSc1d1	značená
trasa	trasa	k1gFnSc1	trasa
<g/>
.	.	kIx.	.
</s>
