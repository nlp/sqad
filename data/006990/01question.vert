<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
čedičový	čedičový	k2eAgInSc1d1	čedičový
kopec	kopec	k1gInSc1	kopec
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
340	[number]	k4	340
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
severně	severně	k6eAd1	severně
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Korozluky	Korozluk	k1gInPc7	Korozluk
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Most	most	k1gInSc1	most
<g/>
?	?	kIx.	?
</s>
