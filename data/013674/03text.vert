<s>
Weber	Weber	k1gMnSc1
(	(	kIx(
<g/>
jednotka	jednotka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Weber	Weber	k1gMnSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
Wb	Wb	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
odvozená	odvozený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
soustavy	soustava	k1gFnSc2
SI	SI	kA
udávající	udávající	k2eAgInSc1d1
magnetický	magnetický	k2eAgInSc1d1
tok	tok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Definice	definice	k1gFnSc1
</s>
<s>
1	#num#	k4
Wb	Wb	k1gFnSc1
=	=	kIx~
1	#num#	k4
m	m	kA
<g/>
2	#num#	k4
<g/>
·	·	k?
<g/>
kg	kg	kA
<g/>
·	·	k?
<g/>
s	s	k7c7
<g/>
−	−	k?
<g/>
2	#num#	k4
<g/>
·	·	k?
<g/>
A	a	k8xC
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
základních	základní	k2eAgFnPc6d1
jednotkách	jednotka	k1gFnPc6
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
také	také	k9
1	#num#	k4
V	v	k7c6
<g/>
·	·	k?
<g/>
s	s	k7c7
nebo	nebo	k8xC
1	#num#	k4
T	T	kA
<g/>
·	·	k?
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
soustavě	soustava	k1gFnSc6
CGS	CGS	kA
existovala	existovat	k5eAaImAgFnS
pro	pro	k7c4
popis	popis	k1gInSc4
magnetického	magnetický	k2eAgInSc2d1
toku	tok	k1gInSc2
jednotka	jednotka	k1gFnSc1
maxwell	maxwell	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
Mx	Mx	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
hodnota	hodnota	k1gFnSc1
byla	být	k5eAaImAgFnS
výrazně	výrazně	k6eAd1
nižší	nízký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
Wb	Wb	k1gFnSc1
=	=	kIx~
108	#num#	k4
Mx	Mx	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Soustava	soustava	k1gFnSc1
SI	si	k1gNnSc2
základní	základní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
ampér	ampér	k1gInSc1
</s>
<s>
kandela	kandela	k1gFnSc1
</s>
<s>
kelvin	kelvin	k1gInSc1
</s>
<s>
kilogram	kilogram	k1gInSc1
</s>
<s>
metr	metr	k1gInSc1
</s>
<s>
mol	mol	k1gMnSc1
</s>
<s>
sekunda	sekunda	k1gFnSc1
odvozené	odvozený	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
becquerel	becquerel	k1gInSc1
</s>
<s>
coulomb	coulomb	k1gInSc1
</s>
<s>
farad	farad	k1gInSc1
</s>
<s>
gray	graa	k1gFnPc1
</s>
<s>
henry	henry	k1gInSc1
</s>
<s>
hertz	hertz	k1gInSc1
</s>
<s>
joule	joule	k1gInSc1
</s>
<s>
katal	katal	k1gMnSc1
</s>
<s>
lumen	lumen	k1gMnSc1
</s>
<s>
lux	lux	k1gInSc1
</s>
<s>
newton	newton	k1gInSc1
</s>
<s>
ohm	ohm	k1gInSc1
</s>
<s>
pascal	pascal	k1gInSc1
</s>
<s>
radián	radián	k1gInSc1
</s>
<s>
siemens	siemens	k1gInSc1
</s>
<s>
sievert	sievert	k1gMnSc1
</s>
<s>
steradián	steradián	k1gInSc1
</s>
<s>
stupeň	stupeň	k1gInSc1
Celsia	Celsius	k1gMnSc2
</s>
<s>
tesla	tesla	k1gFnSc1
</s>
<s>
volt	volt	k1gInSc1
</s>
<s>
watt	watt	k1gInSc1
</s>
<s>
weber	weber	k1gInSc1
další	další	k2eAgFnSc2d1
</s>
<s>
předpony	předpona	k1gFnPc1
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
</s>
<s>
systémy	systém	k1gInPc1
měření	měření	k1gNnSc2
</s>
<s>
převody	převod	k1gInPc1
jednotek	jednotka	k1gFnPc2
</s>
<s>
nové	nový	k2eAgFnPc1d1
definice	definice	k1gFnPc1
SI	se	k3xPyFc3
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
</s>
