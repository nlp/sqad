<p>
<s>
Mixolydický	Mixolydický	k2eAgInSc1d1	Mixolydický
modus	modus	k1gInSc1	modus
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
hudební	hudební	k2eAgFnSc2d1	hudební
nauky	nauka	k1gFnSc2	nauka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
posloupnost	posloupnost	k1gFnSc4	posloupnost
tónů	tón	k1gInPc2	tón
diatonické	diatonický	k2eAgFnSc2d1	diatonická
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
zahrané	zahraný	k2eAgFnSc2d1	zahraná
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
pátého	pátý	k4xOgInSc2	pátý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
mixolydického	mixolydický	k2eAgInSc2d1	mixolydický
modu	modus	k1gInSc2	modus
==	==	k?	==
</s>
</p>
<p>
<s>
Mixolydický	Mixolydický	k2eAgInSc1d1	Mixolydický
modus	modus	k1gInSc1	modus
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
z	z	k7c2	z
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
jejím	její	k3xOp3gNnPc3	její
zahráním	zahrání	k1gNnPc3	zahrání
od	od	k7c2	od
pátého	pátý	k4xOgInSc2	pátý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
C	C	kA	C
dur	dur	k1gNnSc6	dur
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
tónem	tón	k1gInSc7	tón
mixolydického	mixolydický	k2eAgInSc2d1	mixolydický
modu	modus	k1gInSc2	modus
G	G	kA	G
a	a	k8xC	a
znění	znění	k1gNnSc6	znění
mixolydického	mixolydický	k2eAgInSc2d1	mixolydický
modu	modus	k1gInSc2	modus
<g/>
:	:	kIx,	:
g-a-h-c-d-e-	g-	k?	g-a-h-c-d-e-
<g/>
f.	f.	k?	f.
Typickým	typický	k2eAgInSc7d1	typický
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
mixolydickém	mixolydický	k2eAgInSc6d1	mixolydický
modusu	modus	k1gInSc6	modus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
fujara	fujara	k1gFnSc1	fujara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
durový	durový	k2eAgInSc4d1	durový
modus	modus	k1gInSc4	modus
(	(	kIx(	(
<g/>
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
tercií	tercie	k1gFnSc7	tercie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
"	"	kIx"	"
<g/>
měkkou	měkký	k2eAgFnSc7d1	měkká
<g/>
"	"	kIx"	"
malou	malý	k2eAgFnSc7d1	malá
septimou	septima	k1gFnSc7	septima
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
běžné	běžný	k2eAgFnSc2d1	běžná
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejbližším	blízký	k2eAgInSc7d3	Nejbližší
tvrdším	tvrdý	k2eAgInSc7d2	tvrdší
modem	modus	k1gInSc7	modus
je	být	k5eAaImIp3nS	být
jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
mixolydického	mixolydický	k2eAgNnSc2d1	mixolydický
liší	lišit	k5eAaImIp3nS	lišit
velkou	velký	k2eAgFnSc7d1	velká
septimou	septima	k1gFnSc7	septima
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližším	blízký	k2eAgInSc7d3	Nejbližší
měkčím	měkký	k2eAgInSc7d2	měkčí
modem	modus	k1gInSc7	modus
je	být	k5eAaImIp3nS	být
dórský	dórský	k2eAgInSc1d1	dórský
modus	modus	k1gInSc1	modus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
mixolydického	mixolydický	k2eAgNnSc2d1	mixolydický
liší	lišit	k5eAaImIp3nS	lišit
malou	malý	k2eAgFnSc7d1	malá
tercií	tercie	k1gFnSc7	tercie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Intervalové	intervalový	k2eAgNnSc4d1	intervalové
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tóninách	tónina	k1gFnPc6	tónina
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
složení	složení	k1gNnSc4	složení
mixolydického	mixolydický	k2eAgInSc2d1	mixolydický
módu	mód	k1gInSc2	mód
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
tóniny	tónina	k1gFnPc4	tónina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
akordy	akord	k1gInPc1	akord
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
mixolydický	mixolydický	k2eAgInSc4d1	mixolydický
mód	mód	k1gInSc4	mód
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
durový	durový	k2eAgInSc1d1	durový
kvintakord	kvintakord	k1gInSc1	kvintakord
<g/>
,	,	kIx,	,
ze	z	k7c2	z
septakordů	septakord	k1gInPc2	septakord
pak	pak	k6eAd1	pak
dominantní	dominantní	k2eAgInSc4d1	dominantní
septakord	septakord	k1gInSc4	septakord
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
tónový	tónový	k2eAgInSc1d1	tónový
materiál	materiál	k1gInSc1	materiál
modu	modus	k1gInSc2	modus
je	být	k5eAaImIp3nS	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
sedmizvukem	sedmizvuk	k1gInSc7	sedmizvuk
(	(	kIx(	(
<g/>
tercdecimovým	tercdecimový	k2eAgInSc7d1	tercdecimový
akordem	akord	k1gInSc7	akord
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
13	[number]	k4	13
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
X	X	kA	X
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
tón	tón	k1gInSc4	tón
mixolydického	mixolydický	k2eAgInSc2d1	mixolydický
modu	modus	k1gInSc2	modus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
akordy	akord	k1gInPc4	akord
mixolydického	mixolydický	k2eAgInSc2d1	mixolydický
modu	modus	k1gInSc2	modus
v	v	k7c6	v
tónině	tónina	k1gFnSc6	tónina
C	C	kA	C
dur	dur	k1gNnSc7	dur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Použitelné	použitelný	k2eAgInPc1d1	použitelný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgInPc1d1	další
akordy	akord	k1gInPc1	akord
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
vynecháním	vynechání	k1gNnSc7	vynechání
některých	některý	k3yIgInPc2	některý
intervalů	interval	k1gInPc2	interval
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
13	[number]	k4	13
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
11	[number]	k4	11
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
add	add	k?	add
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Tónika	tónika	k1gFnSc1	tónika
</s>
</p>
<p>
<s>
Dórský	dórský	k2eAgInSc1d1	dórský
modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Lydický	Lydický	k2eAgInSc1d1	Lydický
modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Durová	durový	k2eAgFnSc1d1	durová
stupnice	stupnice	k1gFnSc1	stupnice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mixolydický	Mixolydický	k2eAgInSc4d1	Mixolydický
modus	modus	k1gInSc4	modus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kytarové	kytarový	k2eAgInPc1d1	kytarový
prstoklady	prstoklad	k1gInPc1	prstoklad
mixolydického	mixolydický	k2eAgInSc2d1	mixolydický
modu	modus	k1gInSc2	modus
</s>
</p>
