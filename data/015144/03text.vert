<s>
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
United	United	k1gMnSc1
States	States	k1gMnSc1
Marine	Marin	k1gInSc5
Corps	corps	k1gInSc1
Kotva	kotva	k1gFnSc1
<g/>
,	,	kIx,
glóbus	glóbus	k1gInSc1
a	a	k8xC
orel	orel	k1gMnSc1
-	-	kIx~
emblém	emblém	k1gInSc1
Mariňáků	Mariňáků	k?
Kotva	kotva	k1gFnSc1
<g/>
,	,	kIx,
glóbus	glóbus	k1gInSc1
a	a	k8xC
orel	orel	k1gMnSc1
-	-	kIx~
emblém	emblém	k1gInSc1
Mariňáků	Mariňáků	k?
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
Existence	existence	k1gFnSc2
</s>
<s>
1775	#num#	k4
–	–	k?
17831798	#num#	k4
–	–	k?
současnost	současnost	k1gFnSc4
Typ	typa	k1gFnPc2
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
Obojživelná	obojživelný	k2eAgFnSc1d1
a	a	k8xC
rychle	rychle	k6eAd1
nasaditelná	nasaditelný	k2eAgFnSc1d1
úderná	úderný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
Velikost	velikost	k1gFnSc1
</s>
<s>
přibližně	přibližně	k6eAd1
245	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
Motto	motto	k1gNnSc1
</s>
<s>
Semper	Sempra	k1gFnPc2
fidelis	fidelis	k1gFnPc2
(	(	kIx(
<g/>
vždy	vždy	k6eAd1
věrní	věrný	k2eAgMnPc1d1
<g/>
)	)	kIx)
Nadřazené	nadřazený	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnSc2d1
síly	síla	k1gFnSc2
USAMinisterstvo	USAMinisterstvo	k1gNnSc1
obranyÚřad	obranyÚřad	k1gInSc1
námořnictva	námořnictvo	k1gNnSc2
Insignie	insignie	k1gFnPc4
Znak	znak	k1gInSc4
</s>
<s>
Červená	červený	k2eAgFnSc1d1
a	a	k8xC
Zlatá	zlatý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
(	(	kIx(
<g/>
United	United	k1gMnSc1
States	States	k1gMnSc1
Marine	Marin	k1gInSc5
Corps	corps	k1gInSc1
<g/>
,	,	kIx,
USMC	USMC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
často	často	k6eAd1
označované	označovaný	k2eAgNnSc4d1
jen	jen	k9
Marines	Marines	k1gInSc4
<g/>
,	,	kIx,
US	US	kA
Marines	Marines	k1gInSc4
je	být	k5eAaImIp3nS
složka	složka	k1gFnSc1
Ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
182	#num#	k4
000	#num#	k4
aktivních	aktivní	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
38	#num#	k4
500	#num#	k4
rezervistů	rezervista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Provozuje	provozovat	k5eAaImIp3nS
1199	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
údaje	údaj	k1gInPc4
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mariňáci	Mariňáci	k?
při	při	k7c6
cvičení	cvičení	k1gNnSc6
<g/>
,	,	kIx,
používající	používající	k2eAgInSc1d1
lesní	lesní	k2eAgInSc4d1
vzor	vzor	k1gInSc4
nové	nový	k2eAgFnSc2d1
uniformy	uniforma	k1gFnSc2
MARPAT	MARPAT	kA
<g/>
.	.	kIx.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1
americké	americký	k2eAgFnSc2d1
Námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
se	se	k3xPyFc4
pokládají	pokládat	k5eAaImIp3nP
za	za	k7c4
nejlepší	dobrý	k2eAgMnPc4d3
americké	americký	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
jsou	být	k5eAaImIp3nP
většinou	většinou	k6eAd1
nasazeni	nasadit	k5eAaPmNgMnP
jako	jako	k8xC,k8xS
první	první	k4xOgInSc1
<g/>
,	,	kIx,
mající	mající	k2eAgInSc1d1
za	za	k7c4
úkol	úkol	k1gInSc4
prorazit	prorazit	k5eAaPmF
nepřátelské	přátelský	k2eNgFnPc4d1
linie	linie	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
teprve	teprve	k6eAd1
mobilizuje	mobilizovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
se	se	k3xPyFc4
proslavila	proslavit	k5eAaPmAgFnS
pomocí	pomocí	k7c2
jejího	její	k3xOp3gInSc2
obojživelného	obojživelný	k2eAgInSc2d1
boje	boj	k1gInSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
proti	proti	k7c3
Japonsku	Japonsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
nepoužívá	používat	k5eNaImIp3nS
jen	jen	k9
pěší	pěší	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
bojové	bojový	k2eAgInPc1d1
letouny	letoun	k1gInPc1
<g/>
,	,	kIx,
vrtulníky	vrtulník	k1gInPc1
<g/>
,	,	kIx,
tanky	tank	k1gInPc1
a	a	k8xC
různá	různý	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
je	být	k5eAaImIp3nS
sice	sice	k8xC
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
šesti	šest	k4xCc2
samostatných	samostatný	k2eAgFnPc2d1
částí	část	k1gFnPc2
amerických	americký	k2eAgFnPc2d1
Ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
administrativně	administrativně	k6eAd1
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
pod	pod	k7c4
Americké	americký	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
námořnictvem	námořnictvo	k1gNnSc7
mívá	mívat	k5eAaImIp3nS
často	často	k6eAd1
společná	společný	k2eAgNnPc4d1
cvičení	cvičení	k1gNnPc4
<g/>
,	,	kIx,
mariňáci	mariňáci	k?
navíc	navíc	k6eAd1
většinou	většinou	k6eAd1
útočí	útočit	k5eAaImIp3nS
z	z	k7c2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
plavidla	plavidlo	k1gNnPc1
amerického	americký	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
USMC	USMC	kA
používá	používat	k5eAaImIp3nS
nové	nový	k2eAgFnPc4d1
uniformy	uniforma	k1gFnPc4
s	s	k7c7
digitální	digitální	k2eAgFnSc7d1
kamufláží	kamufláž	k1gFnSc7
MARPAT	MARPAT	kA
v	v	k7c4
lesní	lesní	k2eAgFnPc4d1
<g/>
,	,	kIx,
pouštní	pouštní	k2eAgFnPc4d1
<g/>
,	,	kIx,
a	a	k8xC
nově	nově	k6eAd1
i	i	k9
sněžné	sněžný	k2eAgFnSc3d1
verzi	verze	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
uniformy	uniforma	k1gFnPc1
nahradily	nahradit	k5eAaPmAgFnP
starší	starý	k2eAgFnPc1d2
Battle	Battle	k1gFnPc1
Dress	Dressa	k1gFnPc2
Uniform	Uniform	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
datum	datum	k1gNnSc4
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1775	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
kapitán	kapitán	k1gMnSc1
Samuel	Samuel	k1gMnSc1
Nicholas	Nicholas	k1gMnSc1
vybudoval	vybudovat	k5eAaPmAgMnS
z	z	k7c2
pověření	pověření	k1gNnSc2
Druhého	druhý	k4xOgInSc2
kontinentálního	kontinentální	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
Continental	Continental	k1gMnSc1
Marines	Marines	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
ovšem	ovšem	k9
byli	být	k5eAaImAgMnP
po	po	k7c6
skončení	skončení	k1gNnSc6
americké	americký	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1783	#num#	k4
rozpuštěni	rozpuštěn	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
obnovení	obnovení	k1gNnSc3
(	(	kIx(
<g/>
znovuzaložení	znovuzaložení	k1gNnSc3
<g/>
)	)	kIx)
Námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
došlo	dojít	k5eAaPmAgNnS
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1798	#num#	k4
s	s	k7c7
počátkem	počátek	k1gInSc7
tzv.	tzv.	kA
kvaziválky	kvaziválka	k1gFnPc4
s	s	k7c7
revoluční	revoluční	k2eAgFnSc7d1
Francií	Francie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Námořní	námořní	k2eAgNnSc1d1
expediční	expediční	k2eAgNnSc1d1
uskupení	uskupení	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Marine	Marin	k1gInSc5
Expeditionary	Expeditionar	k1gMnPc4
Units	Units	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Unikátním	unikátní	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
pro	pro	k7c4
Námořní	námořní	k2eAgFnSc4d1
pěchotu	pěchota	k1gFnSc4
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
armády	armáda	k1gFnSc2
a	a	k8xC
ostatních	ostatní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
amerických	americký	k2eAgFnPc2d1
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
používá	používat	k5eAaImIp3nS
kombinované	kombinovaný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
-	-	kIx~
tzn.	tzn.	kA
jak	jak	k8xC,k8xS
pozemní	pozemní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
vzdušné	vzdušný	k2eAgFnPc1d1
a	a	k8xC
podpůrné	podpůrný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
využíváno	využíván	k2eAgNnSc1d1
hlavně	hlavně	k9
v	v	k7c6
Marine	Marin	k1gInSc5
Expeditionary	Expeditionar	k1gInPc4
Units	Units	k1gInSc1
(	(	kIx(
<g/>
MEU	MEU	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
jednotky	jednotka	k1gFnPc4
o	o	k7c6
počtu	počet	k1gInSc6
zhruba	zhruba	k6eAd1
2200	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
několika	několik	k4yIc7
pozemních	pozemní	k2eAgNnPc2d1
a	a	k8xC
obojživelných	obojživelný	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
helikoptér	helikoptéra	k1gFnPc2
a	a	k8xC
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
rozmístěných	rozmístěný	k2eAgFnPc2d1
na	na	k7c6
letadlových	letadlový	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
<g/>
,	,	kIx,
schopné	schopný	k2eAgFnPc1d1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
kdekoliv	kdekoliv	k6eAd1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Mariňák	Mariňák	k?
s	s	k7c7
lehkým	lehký	k2eAgInSc7d1
kulometem	kulomet	k1gInSc7
M249	M249	k1gFnSc2
SAW	SAW	kA
</s>
<s>
Příslušník	příslušník	k1gMnSc1
Námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
s	s	k7c7
M16	M16	k1gFnSc7
</s>
<s>
Kolona	kolona	k1gFnSc1
bojových	bojový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
HUMVEE	HUMVEE	kA
v	v	k7c6
Iráku	Irák	k1gInSc6
</s>
<s>
Tank	tank	k1gInSc1
M1	M1	k1gFnPc2
Abrams	Abrams	k1gInSc1
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc1d1
Námořní	námořní	k2eAgFnSc7d1
pěchotou	pěchota	k1gFnSc7
</s>
<s>
AV-8B	AV-8B	k4
Harrier	Harrier	k1gInSc1
II	II	kA
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
AH-1	AH-1	k1gFnSc2
Super	super	k2eAgInSc2d1
Cobra	Cobr	k1gInSc2
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgFnSc1d1
útočná	útočný	k2eAgFnSc1d1
helikoptéra	helikoptéra	k1gFnSc1
USMC	USMC	kA
</s>
<s>
Letoun	letoun	k1gMnSc1
F	F	kA
<g/>
/	/	kIx~
<g/>
A-	A-	k1gFnPc2
<g/>
18	#num#	k4
Hornet	Horneta	k1gFnPc2
<g/>
,	,	kIx,
rovněž	rovněž	k9
ve	v	k7c6
výzbroji	výzbroj	k1gInSc6
Námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
</s>
<s>
Mariňáci	Mariňáci	k?
při	při	k7c6
Operaci	operace	k1gFnSc6
Ocelová	ocelový	k2eAgFnSc1d1
opona	opona	k1gFnSc1
</s>
<s>
Přehled	přehled	k1gInSc1
uniforem-zleva	uniforem-zleva	k1gFnSc1
<g/>
:	:	kIx,
bojová	bojový	k2eAgFnSc1d1
uniforma	uniforma	k1gFnSc1
MCCUU	MCCUU	kA
<g/>
,	,	kIx,
blue	bluat	k5eAaPmIp3nS
dress	dress	k1gInSc1
<g/>
,	,	kIx,
služební	služební	k2eAgFnSc1d1
uniforma	uniforma	k1gFnSc1
a	a	k8xC
večerní	večerní	k2eAgFnSc1d1
uniforma	uniforma	k1gFnSc1
</s>
<s>
Obrněný	obrněný	k2eAgInSc1d1
Transportér	transportér	k1gInSc1
LAV-25	LAV-25	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Department	department	k1gInSc1
of	of	k?
Defense	defense	k1gFnSc1
(	(	kIx(
<g/>
DoD	DoD	k1gMnSc1
<g/>
)	)	kIx)
Releases	Releases	k1gMnSc1
Fiscal	Fiscal	k1gMnSc1
Year	Year	k1gInSc4
2017	#num#	k4
President	president	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Budget	budget	k1gInSc1
Proposal	Proposal	k1gFnSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
Defense	defense	k1gFnSc2
<g/>
,	,	kIx,
9	#num#	k4
February	Februara	k1gFnSc2
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
World	World	k1gMnSc1
Air	Air	k1gMnSc1
Forces	Forces	k1gMnSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flightglobal	Flightglobal	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
February	Februara	k1gFnSc2
2017	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
United	United	k1gMnSc1
States	States	k1gMnSc1
Marine	Marin	k1gInSc5
Corps	corps	k1gInSc4
Aviation	Aviation	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgNnSc1d1
náborové	náborový	k2eAgNnSc1d1
video	video	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Náborové	náborový	k2eAgNnSc1d1
video	video	k1gNnSc1
"	"	kIx"
<g/>
No	no	k9
compromises	compromises	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Náborové	náborový	k2eAgNnSc1d1
video	video	k1gNnSc1
"	"	kIx"
<g/>
Leap	Leap	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
U.	U.	kA
<g/>
S.	S.	kA
Marine	Marin	k1gInSc5
Corps	corps	k1gInSc4
in	in	k?
World	World	k1gInSc1
War	War	k1gMnSc1
II	II	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Armáda	armáda	k1gFnSc1
•	•	k?
Námořnictvo	námořnictvo	k1gNnSc1
•	•	k?
Letectvo	letectvo	k1gNnSc1
•	•	k?
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
•	•	k?
Vesmírné	vesmírný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
•	•	k?
Pobřežní	pobřežní	k1gMnSc1
stráž	stráž	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20031205002	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1029267-6	1029267-6	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0405	#num#	k4
1577	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
78095328	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
153063409	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
78095328	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
