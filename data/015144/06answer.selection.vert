<s>
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
(	(	kIx(
<g/>
United	United	k1gMnSc1
States	States	k1gMnSc1
Marine	Marin	k1gInSc5
Corps	corps	k1gInSc1
<g/>
,	,	kIx,
USMC	USMC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
často	často	k6eAd1
označované	označovaný	k2eAgNnSc4d1
jen	jen	k9
Marines	Marines	k1gInSc4
<g/>
,	,	kIx,
US	US	kA
Marines	Marines	k1gInSc4
je	být	k5eAaImIp3nS
složka	složka	k1gFnSc1
Ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>