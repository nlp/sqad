<p>
<s>
Déšť	déšť	k1gInSc1	déšť
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgInSc1d1	přírodní
jev	jev	k1gInSc1	jev
tvořený	tvořený	k2eAgInSc1d1	tvořený
kapkami	kapka	k1gFnPc7	kapka
vody	voda	k1gFnSc2	voda
padajícími	padající	k2eAgFnPc7d1	padající
z	z	k7c2	z
oblaků	oblak	k1gInPc2	oblak
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meteorologii	meteorologie	k1gFnSc6	meteorologie
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
kapalné	kapalný	k2eAgFnPc4d1	kapalná
vertikální	vertikální	k2eAgFnPc4d1	vertikální
srážky	srážka	k1gFnPc4	srážka
a	a	k8xC	a
obecněji	obecně	k6eAd2	obecně
pod	pod	k7c7	pod
hydrometeory	hydrometeor	k1gInPc7	hydrometeor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapky	kapka	k1gFnPc1	kapka
deště	dešť	k1gInSc2	dešť
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
kondenzačních	kondenzační	k2eAgNnPc6d1	kondenzační
jádrech	jádro	k1gNnPc6	jádro
za	za	k7c2	za
vlivu	vliv	k1gInSc2	vliv
povrchového	povrchový	k2eAgNnSc2d1	povrchové
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Déšť	déšť	k1gInSc1	déšť
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
především	především	k6eAd1	především
kapkami	kapka	k1gFnPc7	kapka
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
větším	veliký	k2eAgInSc6d2	veliký
než	než	k8xS	než
0,5	[number]	k4	0,5
mm	mm	kA	mm
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc1	srážka
tvořené	tvořený	k2eAgInPc1d1	tvořený
menšími	malý	k2eAgFnPc7d2	menší
kapkami	kapka	k1gFnPc7	kapka
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
mrholení	mrholení	k1gNnSc3	mrholení
<g/>
.	.	kIx.	.
</s>
<s>
Ojedinělé	ojedinělý	k2eAgNnSc1d1	ojedinělé
vypadávání	vypadávání	k1gNnSc1	vypadávání
dešťových	dešťový	k2eAgFnPc2d1	dešťová
kapek	kapka	k1gFnPc2	kapka
se	se	k3xPyFc4	se
lidově	lidově	k6eAd1	lidově
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
krápání	krápání	k1gNnSc1	krápání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dešťová	dešťový	k2eAgFnSc1d1	dešťová
kapka	kapka	k1gFnSc1	kapka
==	==	k?	==
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
kapky	kapka	k1gFnPc1	kapka
mají	mít	k5eAaImIp3nP	mít
vlivem	vliv	k1gInSc7	vliv
povrchového	povrchový	k2eAgNnSc2d1	povrchové
napětí	napětí	k1gNnSc2	napětí
vody	voda	k1gFnSc2	voda
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
se	s	k7c7	s
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
rychlostí	rychlost	k1gFnSc7	rychlost
proudícím	proudící	k2eAgFnPc3d1	proudící
vzduchem	vzduch	k1gInSc7	vzduch
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
měrou	míra	k1gFnSc7wR	míra
deformován	deformovat	k5eAaImNgMnS	deformovat
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
kapky	kapka	k1gFnPc1	kapka
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
odspodu	odspodu	k6eAd1	odspodu
ploché	plochý	k2eAgFnPc1d1	plochá
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
lehce	lehko	k6eAd1	lehko
duté	dutý	k2eAgNnSc1d1	duté
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
animovaných	animovaný	k2eAgInPc6d1	animovaný
filmech	film	k1gInPc6	film
a	a	k8xC	a
na	na	k7c6	na
dětských	dětský	k2eAgFnPc6d1	dětská
kresbách	kresba	k1gFnPc6	kresba
jsou	být	k5eAaImIp3nP	být
kapky	kapka	k1gFnPc1	kapka
kresleny	kreslen	k2eAgFnPc1d1	kreslena
s	s	k7c7	s
ostrou	ostrý	k2eAgFnSc7d1	ostrá
špičkou	špička	k1gFnSc7	špička
(	(	kIx(	(
<g/>
kapkovitý	kapkovitý	k2eAgInSc1d1	kapkovitý
tvar	tvar	k1gInSc1	tvar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nepřesně	přesně	k6eNd1	přesně
odpozorovaný	odpozorovaný	k2eAgInSc1d1	odpozorovaný
tvar	tvar	k1gInSc1	tvar
odkapávající	odkapávající	k2eAgFnSc2d1	odkapávající
kapky	kapka	k1gFnSc2	kapka
trvající	trvající	k2eAgInSc1d1	trvající
zlomek	zlomek	k1gInSc1	zlomek
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
padající	padající	k2eAgFnSc2d1	padající
kapky	kapka	k1gFnSc2	kapka
lze	lze	k6eAd1	lze
zachytit	zachytit	k5eAaPmF	zachytit
fotografickou	fotografický	k2eAgFnSc7d1	fotografická
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
rychlosti	rychlost	k1gFnSc3	rychlost
a	a	k8xC	a
malé	malý	k2eAgFnSc3d1	malá
velikosti	velikost	k1gFnSc3	velikost
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
přímo	přímo	k6eAd1	přímo
pozorovatelný	pozorovatelný	k2eAgMnSc1d1	pozorovatelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dešťové	dešťový	k2eAgFnPc1d1	dešťová
kapky	kapka	k1gFnPc1	kapka
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc4	průměr
mezi	mezi	k7c7	mezi
0,5	[number]	k4	0,5
a	a	k8xC	a
7	[number]	k4	7
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
ale	ale	k8xC	ale
z	z	k7c2	z
oblaků	oblak	k1gInPc2	oblak
vypadávají	vypadávat	k5eAaImIp3nP	vypadávat
kapky	kapka	k1gFnPc1	kapka
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
dešťové	dešťový	k2eAgFnPc1d1	dešťová
kapky	kapka	k1gFnPc1	kapka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
nad	nad	k7c7	nad
Brazílií	Brazílie	k1gFnSc7	Brazílie
a	a	k8xC	a
Marshallovými	Marshallův	k2eAgInPc7d1	Marshallův
ostrovy	ostrov	k1gInPc7	ostrov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
když	když	k8xS	když
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
průměru	průměr	k1gInSc6	průměr
až	až	k9	až
10	[number]	k4	10
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Meteorologické	meteorologický	k2eAgFnPc4d1	meteorologická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
deště	dešť	k1gInSc2	dešť
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Měření	měření	k1gNnSc1	měření
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
===	===	k?	===
</s>
</p>
<p>
<s>
Dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
značnou	značný	k2eAgFnSc4d1	značná
proměnlivost	proměnlivost	k1gFnSc4	proměnlivost
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odhadu	odhad	k1gInSc3	odhad
úhrnu	úhrn	k1gInSc2	úhrn
a	a	k8xC	a
intenzity	intenzita	k1gFnSc2	intenzita
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
na	na	k7c6	na
větším	veliký	k2eAgNnSc6d2	veliký
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pozemní	pozemní	k2eAgInPc1d1	pozemní
měřicí	měřicí	k2eAgInPc1d1	měřicí
přístroje	přístroj	k1gInPc1	přístroj
(	(	kIx(	(
<g/>
srážkoměry	srážkoměr	k1gInPc1	srážkoměr
<g/>
,	,	kIx,	,
ombrografy	ombrograf	k1gInPc1	ombrograf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Meteorologický	meteorologický	k2eAgInSc1d1	meteorologický
radar	radar	k1gInSc1	radar
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
mapy	mapa	k1gFnPc1	mapa
srážkového	srážkový	k2eAgNnSc2d1	srážkové
pole	pole	k1gNnSc2	pole
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
ČR	ČR	kA	ČR
za	za	k7c4	za
posledních	poslední	k2eAgFnPc2d1	poslední
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Hlásné	hlásný	k2eAgFnSc2d1	hlásná
a	a	k8xC	a
předpovědní	předpovědní	k2eAgFnSc2d1	předpovědní
povodňové	povodňový	k2eAgFnSc2d1	povodňová
služby	služba	k1gFnSc2	služba
ČHMÚ	ČHMÚ	kA	ČHMÚ
<g/>
.	.	kIx.	.
<g/>
Množství	množství	k1gNnSc1	množství
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nP	měřit
srážkoměry	srážkoměr	k1gInPc1	srážkoměr
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
dešťoměry	dešťoměr	k1gInPc4	dešťoměr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
–	–	k?	–
Hellmanův	Hellmanův	k2eAgInSc1d1	Hellmanův
neboli	neboli	k8xC	neboli
<g />
.	.	kIx.	.
</s>
<s>
sifonový	sifonový	k2eAgInSc1d1	sifonový
srážkoměr	srážkoměr	k1gInSc1	srážkoměr
<g/>
,	,	kIx,	,
kapacitní	kapacitní	k2eAgInSc1d1	kapacitní
srážkoměr	srážkoměr	k1gInSc1	srážkoměr
<g/>
,	,	kIx,	,
člunkový	člunkový	k2eAgInSc1d1	člunkový
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
překlopný	překlopný	k2eAgMnSc1d1	překlopný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
srážkoměr	srážkoměr	k1gInSc1	srážkoměr
<g/>
,	,	kIx,	,
váhový	váhový	k2eAgInSc1d1	váhový
srážkoměr	srážkoměr	k1gInSc1	srážkoměr
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgNnSc1d1	vodní
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
vodních	vodní	k2eAgFnPc2d1	vodní
kapek	kapka	k1gFnPc2	kapka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
drop	drop	k1gMnSc1	drop
counter	counter	k1gMnSc1	counter
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Množství	množství	k1gNnSc1	množství
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
udávají	udávat	k5eAaImIp3nP	udávat
výšku	výška	k1gFnSc4	výška
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
o	o	k7c4	o
kolik	kolik	k4yIc4	kolik
milimetrů	milimetr	k1gInPc2	milimetr
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
spadlé	spadlý	k2eAgFnPc1d1	spadlá
srážky	srážka	k1gFnPc1	srážka
hladinu	hladina	k1gFnSc4	hladina
vody	voda	k1gFnPc1	voda
v	v	k7c6	v
nezakryté	zakrytý	k2eNgFnSc6d1	nezakrytá
nádobě	nádoba	k1gFnSc6	nádoba
(	(	kIx(	(
<g/>
plocha	plocha	k1gFnSc1	plocha
není	být	k5eNaImIp3nS	být
určující	určující	k2eAgFnSc1d1	určující
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
mm	mm	kA	mm
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vrstvička	vrstvička	k1gFnSc1	vrstvička
vody	voda	k1gFnSc2	voda
vysoká	vysoká	k1gFnSc1	vysoká
1	[number]	k4	1
milimetr	milimetr	k1gInSc1	milimetr
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
1	[number]	k4	1
čtverečního	čtvereční	k2eAgInSc2d1	čtvereční
metru	metr	k1gInSc2	metr
množství	množství	k1gNnSc4	množství
1	[number]	k4	1
litru	litr	k1gInSc2	litr
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úhrn	úhrn	k1gInSc1	úhrn
a	a	k8xC	a
intenzita	intenzita	k1gFnSc1	intenzita
(	(	kIx(	(
<g/>
dešťových	dešťový	k2eAgFnPc2d1	dešťová
<g/>
)	)	kIx)	)
srážek	srážka	k1gFnPc2	srážka
===	===	k?	===
</s>
</p>
<p>
<s>
Úhrn	úhrn	k1gInSc1	úhrn
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
H	H	kA	H
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
sledovaná	sledovaný	k2eAgFnSc1d1	sledovaná
integrální	integrální	k2eAgFnSc1d1	integrální
veličina	veličina	k1gFnSc1	veličina
a	a	k8xC	a
udává	udávat	k5eAaImIp3nS	udávat
přírůstek	přírůstek	k1gInSc1	přírůstek
výšky	výška	k1gFnSc2	výška
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
mm	mm	kA	mm
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
o	o	k7c6	o
konstantním	konstantní	k2eAgInSc6d1	konstantní
průřezu	průřez	k1gInSc6	průřez
<g/>
,	,	kIx,	,
vystavené	vystavený	k2eAgInPc4d1	vystavený
dešti	dešť	k1gInPc7	dešť
<g/>
,	,	kIx,	,
za	za	k7c4	za
definovaný	definovaný	k2eAgInSc4d1	definovaný
časový	časový	k2eAgInSc4d1	časový
interval	interval	k1gInSc4	interval
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
za	za	k7c4	za
12	[number]	k4	12
nebo	nebo	k8xC	nebo
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
srážek	srážka	k1gFnPc2	srážka
R	R	kA	R
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
časová	časový	k2eAgFnSc1d1	časová
derivace	derivace	k1gFnSc1	derivace
časového	časový	k2eAgInSc2d1	časový
chodu	chod	k1gInSc2	chod
úhrnu	úhrn	k1gInSc2	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
R	R	kA	R
=	=	kIx~	=
dH	dH	k?	dH
<g/>
/	/	kIx~	/
<g/>
dt	dt	k?	dt
<g/>
,	,	kIx,	,
zažitou	zažitý	k2eAgFnSc7d1	zažitá
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Typická	typický	k2eAgFnSc1d1	typická
hodnota	hodnota	k1gFnSc1	hodnota
intenzity	intenzita	k1gFnSc2	intenzita
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
trvalý	trvalý	k2eAgInSc4d1	trvalý
déšť	déšť	k1gInSc4	déšť
1	[number]	k4	1
až	až	k9	až
5	[number]	k4	5
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
při	při	k7c6	při
přeháňce	přeháňka	k1gFnSc6	přeháňka
20	[number]	k4	20
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
extrémně	extrémně	k6eAd1	extrémně
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
přes	přes	k7c4	přes
100	[number]	k4	100
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Většinou	většinou	k6eAd1	většinou
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	čí	k3xOyQgNnSc7	čí
je	být	k5eAaImIp3nS	být
intenzita	intenzita	k1gFnSc1	intenzita
srážek	srážka	k1gFnPc2	srážka
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
trvání	trvání	k1gNnSc1	trvání
dešťové	dešťový	k2eAgFnSc2d1	dešťová
události	událost	k1gFnSc2	událost
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
intenzita	intenzita	k1gFnSc1	intenzita
srážek	srážka	k1gFnPc2	srážka
počítá	počítat	k5eAaImIp3nS	počítat
jako	jako	k9	jako
podíl	podíl	k1gInSc4	podíl
přírůstku	přírůstek	k1gInSc2	přírůstek
úhrnu	úhrn	k1gInSc2	úhrn
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
např.	např.	kA	např.
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
pak	pak	k6eAd1	pak
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
pětiminutové	pětiminutový	k2eAgFnSc6d1	pětiminutová
intenzitě	intenzita	k1gFnSc6	intenzita
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
např.	např.	kA	např.
útlumu	útlum	k1gInSc3	útlum
radiových	radiový	k2eAgFnPc2d1	radiová
vln	vlna	k1gFnPc2	vlna
deštěm	dešť	k1gInSc7	dešť
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ideálně	ideálně	k6eAd1	ideálně
desetivteřinová	desetivteřinový	k2eAgFnSc1d1	desetivteřinová
průměrná	průměrný	k2eAgFnSc1d1	průměrná
intenzita	intenzita	k1gFnSc1	intenzita
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spektrum	spektrum	k1gNnSc1	spektrum
dešťových	dešťový	k2eAgFnPc2d1	dešťová
kapek	kapka	k1gFnPc2	kapka
===	===	k?	===
</s>
</p>
<p>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
dešťových	dešťový	k2eAgFnPc2d1	dešťová
kapek	kapka	k1gFnPc2	kapka
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
déšť	déšť	k1gInSc1	déšť
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
kapek	kapka	k1gFnPc2	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1	charakteristika
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
početné	početný	k2eAgFnPc4d1	početná
různé	různý	k2eAgFnPc4d1	různá
velikosti	velikost	k1gFnPc4	velikost
kapek	kapka	k1gFnPc2	kapka
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
dešti	dešť	k1gInSc6	dešť
a	a	k8xC	a
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
spektrum	spektrum	k1gNnSc1	spektrum
průměruje	průměrovat	k5eAaPmIp3nS	průměrovat
po	po	k7c6	po
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
dešťových	dešťový	k2eAgFnPc2d1	dešťová
kapek	kapka	k1gFnPc2	kapka
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
DSD	DSD	kA	DSD
–	–	k?	–
Drop	drop	k1gMnSc1	drop
Size	Siz	k1gFnSc2	Siz
Distribution	Distribution	k1gInSc1	Distribution
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
ekvivalentního	ekvivalentní	k2eAgInSc2d1	ekvivalentní
průměru	průměr	k1gInSc2	průměr
kapek	kapka	k1gFnPc2	kapka
D	D	kA	D
<g/>
,	,	kIx,	,
když	když	k8xS	když
součin	součin	k1gInSc1	součin
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
dD	dD	k?	dD
udává	udávat	k5eAaImIp3nS	udávat
počet	počet	k1gInSc1	počet
kapek	kapka	k1gFnPc2	kapka
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
D	D	kA	D
až	až	k8xS	až
D	D	kA	D
<g/>
+	+	kIx~	+
<g/>
dD	dD	k?	dD
v	v	k7c6	v
jednotce	jednotka	k1gFnSc6	jednotka
objemu	objem	k1gInSc2	objem
(	(	kIx(	(
<g/>
dD	dD	k?	dD
je	být	k5eAaImIp3nS	být
diferenciál	diferenciál	k1gInSc1	diferenciál
průměru	průměr	k1gInSc2	průměr
kapek	kapka	k1gFnPc2	kapka
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
změnou	změna	k1gFnSc7	změna
průměru	průměr	k1gInSc2	průměr
kapky	kapka	k1gFnSc2	kapka
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
např.	např.	kA	např.
dD	dD	k?	dD
~	~	kIx~	~
0,2	[number]	k4	0,2
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
rozměr	rozměr	k1gInSc1	rozměr
spektra	spektrum	k1gNnSc2	spektrum
kapek	kapka	k1gFnPc2	kapka
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
4	[number]	k4	4
nebo	nebo	k8xC	nebo
mm	mm	kA	mm
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
analytickým	analytický	k2eAgInSc7d1	analytický
modelem	model	k1gInSc7	model
spektra	spektrum	k1gNnSc2	spektrum
kapek	kapka	k1gFnPc2	kapka
je	být	k5eAaImIp3nS	být
dvouparametrový	dvouparametrový	k2eAgInSc1d1	dvouparametrový
model	model	k1gInSc1	model
exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
=	=	kIx~	=
No	no	k9	no
e	e	k0	e
<g/>
−	−	k?	−
<g/>
λ	λ	k?	λ
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
průměrný	průměrný	k2eAgInSc4d1	průměrný
déšť	déšť	k1gInSc4	déšť
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
Marshall-Palmerova	Marshall-Palmerův	k2eAgFnSc1d1	Marshall-Palmerův
parametrizace	parametrizace	k1gFnSc1	parametrizace
parametru	parametr	k1gInSc2	parametr
λ	λ	k?	λ
v	v	k7c6	v
právě	právě	k6eAd1	právě
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
exponenciálním	exponenciální	k2eAgInSc6d1	exponenciální
modelu	model	k1gInSc6	model
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
parametr	parametr	k1gInSc1	parametr
λ	λ	k?	λ
udáván	udáván	k2eAgInSc1d1	udáván
jako	jako	k8xS	jako
funkce	funkce	k1gFnSc1	funkce
intenzity	intenzita	k1gFnSc2	intenzita
srážek	srážka	k1gFnPc2	srážka
R	R	kA	R
[	[	kIx(	[
<g/>
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
modifikace	modifikace	k1gFnPc4	modifikace
Marshall-Palmerovy	Marshall-Palmerův	k2eAgFnPc4d1	Marshall-Palmerův
aproximace	aproximace	k1gFnPc4	aproximace
spekter	spektrum	k1gNnPc2	spektrum
kapek	kapka	k1gFnPc2	kapka
pro	pro	k7c4	pro
4	[number]	k4	4
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
dešťů	dešť	k1gInPc2	dešť
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
trvalý	trvalý	k2eAgInSc4d1	trvalý
déšť	déšť	k1gInSc4	déšť
<g/>
,	,	kIx,	,
bouři	bouře	k1gFnSc4	bouře
<g/>
,	,	kIx,	,
přeháňku	přeháňka	k1gFnSc4	přeháňka
a	a	k8xC	a
mrholení	mrholení	k1gNnSc1	mrholení
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgFnSc7d2	přesnější
aproximací	aproximace	k1gFnSc7	aproximace
spektra	spektrum	k1gNnSc2	spektrum
kapek	kapka	k1gFnPc2	kapka
je	být	k5eAaImIp3nS	být
tříparametrový	tříparametrový	k2eAgInSc1d1	tříparametrový
model	model	k1gInSc1	model
Gamma	Gamm	k1gMnSc2	Gamm
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
=	=	kIx~	=
No	no	k9	no
Dμ	Dμ	k1gFnSc1	Dμ
<g/>
−	−	k?	−
<g/>
λ	λ	k?	λ
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
exponenciálního	exponenciální	k2eAgInSc2d1	exponenciální
modelu	model	k1gInSc2	model
již	již	k6eAd1	již
nenadhodnocuje	nadhodnocovat	k5eNaImIp3nS	nadhodnocovat
počet	počet	k1gInSc1	počet
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc2d1	malá
kapek	kapka	k1gFnPc2	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc4	přístroj
měřící	měřící	k2eAgNnSc4d1	měřící
spektrum	spektrum	k1gNnSc4	spektrum
kapek	kapka	k1gFnPc2	kapka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
distrometr	distrometr	k1gInSc1	distrometr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
videodistrometr	videodistrometr	k1gInSc1	videodistrometr
<g/>
,	,	kIx,	,
elektromechanický	elektromechanický	k2eAgInSc1d1	elektromechanický
(	(	kIx(	(
<g/>
piezoelektrický	piezoelektrický	k2eAgInSc1d1	piezoelektrický
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Jossův-Waldvogelův	Jossův-Waldvogelův	k2eAgInSc4d1	Jossův-Waldvogelův
<g/>
"	"	kIx"	"
distrometr	distrometr	k1gInSc4	distrometr
<g/>
,	,	kIx,	,
laserový	laserový	k2eAgInSc4d1	laserový
(	(	kIx(	(
<g/>
optický	optický	k2eAgInSc4d1	optický
<g/>
)	)	kIx)	)
distrometr	distrometr	k1gInSc4	distrometr
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
deště	dešť	k1gInSc2	dešť
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Význam	význam	k1gInSc1	význam
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
===	===	k?	===
</s>
</p>
<p>
<s>
Déšť	déšť	k1gInSc1	déšť
hraje	hrát	k5eAaImIp3nS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
hydrologickém	hydrologický	k2eAgInSc6d1	hydrologický
cyklu	cyklus	k1gInSc6	cyklus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
vypařená	vypařený	k2eAgFnSc1d1	vypařená
z	z	k7c2	z
oceánů	oceán	k1gInPc2	oceán
přenášena	přenášen	k2eAgFnSc1d1	přenášena
nad	nad	k7c7	nad
jejich	jejich	k3xOp3gFnSc7	jejich
jiné	jiný	k2eAgFnPc4d1	jiná
části	část	k1gFnPc4	část
a	a	k8xC	a
nad	nad	k7c4	nad
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kondenzuje	kondenzovat	k5eAaImIp3nS	kondenzovat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikají	vznikat	k5eAaImIp3nP	vznikat
oblaky	oblak	k1gInPc1	oblak
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vypadávají	vypadávat	k5eAaImIp3nP	vypadávat
srážky	srážka	k1gFnPc1	srážka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
deště	dešť	k1gInSc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
cyklus	cyklus	k1gInSc4	cyklus
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
řeky	řeka	k1gFnPc1	řeka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odvádějí	odvádět	k5eAaImIp3nP	odvádět
dešťovou	dešťový	k2eAgFnSc4d1	dešťová
vodu	voda	k1gFnSc4	voda
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
z	z	k7c2	z
pevniny	pevnina	k1gFnSc2	pevnina
a	a	k8xC	a
které	který	k3yIgNnSc1	který
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
opět	opět	k6eAd1	opět
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
nebo	nebo	k8xC	nebo
nad	nad	k7c7	nad
oceány	oceán	k1gInPc7	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
deštěm	dešť	k1gInSc7	dešť
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
kultury	kultura	k1gFnPc1	kultura
si	se	k3xPyFc3	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
proti	proti	k7c3	proti
dešti	dešť	k1gInSc3	dešť
různé	různý	k2eAgFnSc2d1	různá
ochranné	ochranný	k2eAgFnSc2d1	ochranná
pomůcky	pomůcka	k1gFnSc2	pomůcka
<g/>
:	:	kIx,	:
deštníky	deštník	k1gInPc1	deštník
<g/>
,	,	kIx,	,
pláštěnky	pláštěnka	k1gFnPc1	pláštěnka
<g/>
,	,	kIx,	,
pršipláště	pršiplášť	k1gInPc1	pršiplášť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stavitelství	stavitelství	k1gNnSc6	stavitelství
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
speciální	speciální	k2eAgInPc1d1	speciální
okapové	okapový	k2eAgInPc1d1	okapový
systémy	systém	k1gInPc1	systém
(	(	kIx(	(
<g/>
okapy	okap	k1gInPc1	okap
<g/>
,	,	kIx,	,
žlaby	žlab	k1gInPc1	žlab
<g/>
,	,	kIx,	,
svody	svod	k1gInPc1	svod
<g/>
,	,	kIx,	,
tvary	tvar	k1gInPc1	tvar
střech	střecha	k1gFnPc2	střecha
<g/>
,	,	kIx,	,
chrliče	chrlič	k1gInPc1	chrlič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odvádějí	odvádět	k5eAaImIp3nP	odvádět
vodu	voda	k1gFnSc4	voda
ze	z	k7c2	z
střech	střecha	k1gFnPc2	střecha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
střechy	střecha	k1gFnPc1	střecha
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
déšť	déšť	k1gInSc4	déšť
zůstával	zůstávat	k5eAaImAgInS	zůstávat
<g/>
.	.	kIx.	.
</s>
<s>
Vodu	voda	k1gFnSc4	voda
pak	pak	k6eAd1	pak
obyvatelé	obyvatel	k1gMnPc1	obyvatel
domu	dům	k1gInSc2	dům
pomocí	pomocí	k7c2	pomocí
potrubí	potrubí	k1gNnSc2	potrubí
jímají	jímat	k5eAaImIp3nP	jímat
do	do	k7c2	do
nádob	nádoba	k1gFnPc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
lidé	člověk	k1gMnPc1	člověk
během	během	k7c2	během
deště	dešť	k1gInSc2	dešť
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
skryti	skrýt	k5eAaPmNgMnP	skrýt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
déšť	déšť	k1gInSc1	déšť
doprovázen	doprovázen	k2eAgInSc1d1	doprovázen
bouřkami	bouřka	k1gFnPc7	bouřka
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
extrémní	extrémní	k2eAgNnSc4d1	extrémní
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
monzuny	monzun	k1gInPc1	monzun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vozidlech	vozidlo	k1gNnPc6	vozidlo
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
výhled	výhled	k1gInSc1	výhled
skrz	skrz	k7c4	skrz
sklo	sklo	k1gNnSc4	sklo
karoserie	karoserie	k1gFnSc2	karoserie
nebo	nebo	k8xC	nebo
řídicí	řídicí	k2eAgFnPc4d1	řídicí
kabiny	kabina	k1gFnPc4	kabina
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
stěrače	stěrač	k1gInPc1	stěrač
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
veřejných	veřejný	k2eAgNnPc6d1	veřejné
prostranstvích	prostranství	k1gNnPc6	prostranství
lze	lze	k6eAd1	lze
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
využít	využít	k5eAaPmF	využít
různé	různý	k2eAgInPc4d1	různý
přístřešky	přístřešek	k1gInPc4	přístřešek
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
zastávkové	zastávkový	k2eAgFnPc1d1	zastávková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
altány	altán	k1gInPc1	altán
<g/>
,	,	kIx,	,
podloubí	podloubí	k1gNnPc1	podloubí
<g/>
,	,	kIx,	,
podchody	podchod	k1gInPc1	podchod
<g/>
,	,	kIx,	,
zastřešené	zastřešený	k2eAgInPc1d1	zastřešený
prostory	prostor	k1gInPc1	prostor
u	u	k7c2	u
vchodů	vchod	k1gInPc2	vchod
budov	budova	k1gFnPc2	budova
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgInPc4d1	kulturní
aspekty	aspekt	k1gInPc4	aspekt
===	===	k?	===
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
dešti	dešť	k1gInSc3	dešť
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
mírných	mírný	k2eAgFnPc6d1	mírná
oblastech	oblast	k1gFnPc6	oblast
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
déšť	déšť	k1gInSc1	déšť
tradičně	tradičně	k6eAd1	tradičně
spojován	spojovat	k5eAaImNgInS	spojovat
se	s	k7c7	s
smutnými	smutná	k1gFnPc7	smutná
<g/>
,	,	kIx,	,
splínovými	splínový	k2eAgFnPc7d1	splínový
a	a	k8xC	a
negativně	negativně	k6eAd1	negativně
laděnými	laděný	k2eAgInPc7d1	laděný
významy	význam	k1gInPc7	význam
a	a	k8xC	a
náladami	nálada	k1gFnPc7	nálada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
některá	některý	k3yIgNnPc4	některý
místa	místo	k1gNnPc4	místo
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
déšť	déšť	k1gInSc1	déšť
vítán	vítán	k2eAgInSc1d1	vítán
s	s	k7c7	s
radostí	radost	k1gFnSc7	radost
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Kalahari	Kalahar	k1gFnSc2	Kalahar
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
devizu	deviza	k1gFnSc4	deviza
ve	v	k7c6	v
státním	státní	k2eAgInSc6d1	státní
znaku	znak	k1gInSc6	znak
napsáno	napsat	k5eAaBmNgNnS	napsat
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Pula	Pula	k1gFnSc1	Pula
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Setswana	Setswana	k1gFnSc1	Setswana
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
déšť	déšť	k1gInSc1	déšť
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
požehnání	požehnání	k1gNnSc6	požehnání
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
úspěch	úspěch	k1gInSc1	úspěch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jako	jako	k8xC	jako
přátelský	přátelský	k2eAgInSc1d1	přátelský
pozdrav	pozdrav	k1gInSc1	pozdrav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
místní	místní	k2eAgFnSc1d1	místní
měna	měna	k1gFnSc1	měna
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
botswanská	botswanský	k2eAgFnSc1d1	Botswanská
pula	pula	k1gFnSc1	pula
<g/>
.	.	kIx.	.
<g/>
Mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
cítí	cítit	k5eAaImIp3nP	cítit
během	během	k7c2	během
deště	dešť	k1gInSc2	dešť
a	a	k8xC	a
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
vůni	vůně	k1gFnSc4	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
této	tento	k3xDgFnSc2	tento
vůně	vůně	k1gFnSc2	vůně
jsou	být	k5eAaImIp3nP	být
oleje	olej	k1gInPc1	olej
produkované	produkovaný	k2eAgInPc1d1	produkovaný
rostlinami	rostlina	k1gFnPc7	rostlina
během	během	k7c2	během
období	období	k1gNnSc2	období
sucha	sucho	k1gNnSc2	sucho
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
absorbovány	absorbován	k2eAgFnPc1d1	absorbována
horninami	hornina	k1gFnPc7	hornina
a	a	k8xC	a
půdou	půda	k1gFnSc7	půda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
deště	dešť	k1gInSc2	dešť
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
půda	půda	k1gFnSc1	půda
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Déšť	déšť	k1gInSc1	déšť
může	moct	k5eAaImIp3nS	moct
uměle	uměle	k6eAd1	uměle
vyvolat	vyvolat	k5eAaPmF	vyvolat
například	například	k6eAd1	například
jodid	jodid	k1gInSc1	jodid
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
déšť	déšť	k1gInSc1	déšť
mohou	moct	k5eAaImIp3nP	moct
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
emisí	emise	k1gFnPc2	emise
zintenzivnit	zintenzivnit	k5eAaPmF	zintenzivnit
i	i	k9	i
samotná	samotný	k2eAgNnPc4d1	samotné
letadla	letadlo	k1gNnPc4	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŘEZÁČOVÁ	Řezáčová	k1gFnSc1	Řezáčová
<g/>
,	,	kIx,	,
Daniela	Daniela	k1gFnSc1	Daniela
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Fyzika	fyzika	k1gFnSc1	fyzika
oblaků	oblak	k1gInPc2	oblak
a	a	k8xC	a
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
1	[number]	k4	1
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Gerstner	Gerstner	k1gMnSc1	Gerstner
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1505	[number]	k4	1505
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Mikrofyzika	mikrofyzika	k1gFnSc1	mikrofyzika
oblaků	oblak	k1gInPc2	oblak
<g/>
,	,	kIx,	,
s.	s.	k?	s.
131	[number]	k4	131
<g/>
–	–	k?	–
<g/>
232	[number]	k4	232
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hydrometeor	hydrometeor	k1gInSc1	hydrometeor
</s>
</p>
<p>
<s>
Srážky	srážka	k1gFnPc1	srážka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
déšť	déšť	k1gInSc1	déšť
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
déšť	déšť	k1gInSc4	déšť
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
déšť	déšť	k1gInSc1	déšť
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Déšť	déšť	k1gInSc1	déšť
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
mlh	mlha	k1gFnPc2	mlha
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
</s>
</p>
<p>
<s>
Aktuální	aktuální	k2eAgFnSc1d1	aktuální
mapa	mapa	k1gFnSc1	mapa
srážek	srážka	k1gFnPc2	srážka
z	z	k7c2	z
radaru	radar	k1gInSc2	radar
</s>
</p>
<p>
<s>
Aktuální	aktuální	k2eAgNnSc1d1	aktuální
měření	měření	k1gNnSc1	měření
srážek	srážka	k1gFnPc2	srážka
z	z	k7c2	z
pozemních	pozemní	k2eAgInPc2d1	pozemní
srážkoměrů	srážkoměr	k1gInPc2	srážkoměr
</s>
</p>
