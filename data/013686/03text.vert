<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInSc1d1
atletický	atletický	k2eAgInSc1d1
šampionát	šampionát	k1gInSc1
sportovců	sportovec	k1gMnPc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořádá	pořádat	k5eAaImIp3nS
ho	on	k3xPp3gInSc4
každý	každý	k3xTgInSc4
druhý	druhý	k4xOgInSc4
rok	rok	k1gInSc4
Evropská	evropský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
ročník	ročník	k1gInSc4
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
v	v	k7c6
Tbilisi	Tbilisi	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Událost	událost	k1gFnSc1
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
s	s	k7c7
cílem	cíl	k1gInSc7
podporovat	podporovat	k5eAaImF
sport	sport	k1gInSc4
mezi	mezi	k7c7
mladými	mladý	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každé	každý	k3xTgFnSc6
disciplíně	disciplína	k1gFnSc6
mohou	moct	k5eAaImIp3nP
za	za	k7c4
každou	každý	k3xTgFnSc4
zemi	zem	k1gFnSc4
startovat	startovat	k5eAaBmF
maximálně	maximálně	k6eAd1
dva	dva	k4xCgMnPc1
sportovci	sportovec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
šampionátů	šampionát	k1gInPc2
</s>
<s>
N	N	kA
<g/>
°	°	k?
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Město	město	k1gNnSc1
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Datum	datum	k1gNnSc1
konání	konání	k1gNnSc2
</s>
<s>
Místo	místo	k7c2
konání	konání	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
</s>
<s>
Tbilisi	Tbilisi	k1gNnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
Gruzie	Gruzie	k1gFnSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
</s>
<s>
Athletics	Athletics	k6eAd1
Stadium	stadium	k1gNnSc1
of	of	k?
Tbilisi	Tbilisi	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
</s>
<s>
Győr	Győr	k1gMnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
</s>
<s>
Aqua	Aqua	k1gFnSc1
Sports	Sportsa	k1gFnPc2
Center	centrum	k1gNnPc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
</s>
<s>
Rieti	Rieti	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
</s>
<s>
Stadio	Stadio	k1gMnSc1
Raul	Raul	k1gMnSc1
Guidobaldi	Guidobald	k1gMnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
