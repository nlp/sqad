<s>
Světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
také	také	k9	také
globální	globální	k2eAgFnSc1d1	globální
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vojenský	vojenský	k2eAgInSc4d1	vojenský
konflikt	konflikt	k1gInSc4	konflikt
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
