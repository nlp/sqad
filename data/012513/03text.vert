<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
také	také	k9	také
globální	globální	k2eAgFnSc1d1	globální
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vojenský	vojenský	k2eAgInSc4d1	vojenský
konflikt	konflikt	k1gInSc4	konflikt
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
více	hodně	k6eAd2	hodně
zemí	zem	k1gFnPc2	zem
nebo	nebo	k8xC	nebo
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
válka	válka	k1gFnSc1	válka
má	mít	k5eAaImIp3nS	mít
globální	globální	k2eAgInPc4d1	globální
dopady	dopad	k1gInPc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
konfliktů	konflikt	k1gInPc2	konflikt
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
subjektivně	subjektivně	k6eAd1	subjektivně
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
nebo	nebo	k8xC	nebo
válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pojem	pojem	k1gInSc1	pojem
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
běžně	běžně	k6eAd1	běžně
akceptován	akceptovat	k5eAaBmNgInS	akceptovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
retrospektivního	retrospektivní	k2eAgNnSc2d1	retrospektivní
pojmenování	pojmenování	k1gNnSc2	pojmenování
pouze	pouze	k6eAd1	pouze
dvou	dva	k4xCgInPc2	dva
významných	významný	k2eAgInPc2d1	významný
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
konfliktů	konflikt	k1gInPc2	konflikt
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc2	první
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Války	válka	k1gFnPc4	válka
ovlivňující	ovlivňující	k2eAgFnPc4d1	ovlivňující
více	hodně	k6eAd2	hodně
zemí	zem	k1gFnSc7	zem
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
byly	být	k5eAaImAgFnP	být
pojmenovávány	pojmenováván	k2eAgFnPc1d1	pojmenovávána
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
–	–	k?	–
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
španělské	španělský	k2eAgNnSc4d1	španělské
dědictví	dědictví	k1gNnSc4	dědictví
(	(	kIx(	(
<g/>
1701	[number]	k4	1701
<g/>
–	–	k?	–
<g/>
1714	[number]	k4	1714
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1756	[number]	k4	1756
<g/>
–	–	k?	–
<g/>
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
napoleonské	napoleonský	k2eAgFnSc2d1	napoleonská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
–	–	k?	–
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
globalizace	globalizace	k1gFnSc2	globalizace
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
konflikt	konflikt	k1gInSc4	konflikt
globálních	globální	k2eAgInPc2d1	globální
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc1	termín
užit	užít	k5eAaPmNgInS	užít
skotskými	skotský	k2eAgFnPc7d1	skotská
novinami	novina	k1gFnPc7	novina
The	The	k1gMnSc2	The
People	People	k1gMnSc2	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Journal	Journal	k1gFnSc7	Journal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc1	termín
užit	užít	k5eAaPmNgInS	užít
v	v	k7c6	v
sérii	série	k1gFnSc6	série
článků	článek	k1gInPc2	článek
The	The	k1gFnSc2	The
Class	Classa	k1gFnPc2	Classa
Struggles	Struggles	k1gMnSc1	Struggles
in	in	k?	in
France	Franc	k1gMnSc4	Franc
Karlem	Karel	k1gMnSc7	Karel
Marxem	Marx	k1gMnSc7	Marx
a	a	k8xC	a
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Engelsem	Engels	k1gMnSc7	Engels
<g/>
.	.	kIx.	.
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgInSc7d1	poslední
konfliktem	konflikt	k1gInSc7	konflikt
takových	takový	k3xDgInPc2	takový
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
nedávnými	dávný	k2eNgInPc7d1	nedávný
konflikty	konflikt	k1gInPc7	konflikt
se	se	k3xPyFc4	se
také	také	k6eAd1	také
hojně	hojně	k6eAd1	hojně
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
třetí	třetí	k4xOgFnSc1	třetí
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
hrozící	hrozící	k2eAgInPc1d1	hrozící
globální	globální	k2eAgInPc1d1	globální
konflikty	konflikt	k1gInPc1	konflikt
však	však	k9	však
byly	být	k5eAaImAgInP	být
dosud	dosud	k6eAd1	dosud
vždy	vždy	k6eAd1	vždy
odvráceny	odvrátit	k5eAaPmNgFnP	odvrátit
diplomacií	diplomacie	k1gFnSc7	diplomacie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souvisejícím	související	k2eAgInSc7d1	související
pojmem	pojem	k1gInSc7	pojem
je	být	k5eAaImIp3nS	být
i	i	k9	i
jaderná	jaderný	k2eAgFnSc1d1	jaderná
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
válku	válka	k1gFnSc4	válka
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
zbraně	zbraň	k1gFnSc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
budoucím	budoucí	k2eAgNnSc6d1	budoucí
použití	použití	k1gNnSc6	použití
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
World	World	k1gInSc1	World
war	war	k?	war
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
