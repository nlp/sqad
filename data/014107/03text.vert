<s>
Zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
obcí	obec	k1gFnPc2
Podkletí	Podkletí	k1gNnSc2
</s>
<s>
Zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
obcí	obec	k1gFnPc2
PodkletíForma	PodkletíFormum	k1gNnSc2
</s>
<s>
zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
právnických	právnický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
Předseda	předseda	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Josef	Josef	k1gMnSc1
Troup	troup	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Křemže	Křemže	k6eAd1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2000	#num#	k4
Poloha	poloha	k1gFnSc1
Kraj	kraj	k7c2
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
Kontakt	kontakt	k1gInSc1
E-mail	e-mail	k1gInSc1
</s>
<s>
oukremze@mbox.terms.cz	oukremze@mbox.terms.cz	k1gInSc1
Web	web	k1gInSc1
</s>
<s>
www.podkleti.ois.cz	www.podkleti.ois.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
obcí	obec	k1gFnPc2
Podkletí	Podkletí	k1gNnSc2
je	být	k5eAaImIp3nS
zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
právnických	právnický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
v	v	k7c6
okresu	okres	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
je	být	k5eAaImIp3nS
Křemže	Křemž	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
soustředění	soustředění	k1gNnSc1
sil	síla	k1gFnPc2
a	a	k8xC
prostředků	prostředek	k1gInPc2
při	při	k7c6
prosazování	prosazování	k1gNnSc6
záměrů	záměr	k1gInPc2
přesahujících	přesahující	k2eAgInPc2d1
rozsahem	rozsah	k1gInSc7
a	a	k8xC
významem	význam	k1gInSc7
možnosti	možnost	k1gFnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
účastnických	účastnický	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdružuje	sdružovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
6	#num#	k4
obcí	obec	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
sdružené	sdružený	k2eAgFnPc1d1
v	v	k7c6
mikroregionu	mikroregion	k1gInSc6
</s>
<s>
Srnín	Srnín	k1gMnSc1
</s>
<s>
Brloh	brloh	k1gInSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
</s>
<s>
Křemže	Křemže	k6eAd1
</s>
<s>
Holubov	Holubov	k1gInSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
obcí	obec	k1gFnPc2
Podkletí	Podkletí	k1gNnSc2
na	na	k7c6
Regionálním	regionální	k2eAgInSc6d1
informačním	informační	k2eAgInSc6d1
servisu	servis	k1gInSc6
</s>
<s>
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
