<s>
Zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
obcí	obec	k1gFnPc2
Podkletí	Podkletí	k1gNnSc2
je	být	k5eAaImIp3nS
zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
právnických	právnický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
v	v	k7c6
okresu	okres	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
je	být	k5eAaImIp3nS
Křemže	Křemž	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
soustředění	soustředění	k1gNnSc1
sil	síla	k1gFnPc2
a	a	k8xC
prostředků	prostředek	k1gInPc2
při	při	k7c6
prosazování	prosazování	k1gNnSc6
záměrů	záměr	k1gInPc2
přesahujících	přesahující	k2eAgInPc2d1
rozsahem	rozsah	k1gInSc7
a	a	k8xC
významem	význam	k1gInSc7
možnosti	možnost	k1gFnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
účastnických	účastnický	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>