<s>
Rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
odvozená	odvozený	k2eAgFnSc1d1	odvozená
od	od	k7c2	od
oběhu	oběh	k1gInSc2	oběh
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
trvajícího	trvající	k2eAgNnSc2d1	trvající
přibližně	přibližně	k6eAd1	přibližně
365	[number]	k4	365
a	a	k8xC	a
čtvrt	čtvrt	k1xP	čtvrt
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
kalendáře	kalendář	k1gInSc2	kalendář
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
zaokrouhlen	zaokrouhlen	k2eAgInSc4d1	zaokrouhlen
na	na	k7c4	na
celé	celý	k2eAgInPc4d1	celý
dny	den	k1gInPc4	den
a	a	k8xC	a
nesoulad	nesoulad	k1gInSc1	nesoulad
s	s	k7c7	s
přesnou	přesný	k2eAgFnSc7d1	přesná
dobou	doba	k1gFnSc7	doba
oběhu	oběh	k1gInSc2	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
řešen	řešit	k5eAaImNgInS	řešit
nestejnou	stejný	k2eNgFnSc7d1	nestejná
délkou	délka	k1gFnSc7	délka
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
používaný	používaný	k2eAgInSc1d1	používaný
solární	solární	k2eAgInSc1d1	solární
gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
má	mít	k5eAaImIp3nS	mít
běžně	běžně	k6eAd1	běžně
365	[number]	k4	365
dní	den	k1gInPc2	den
a	a	k8xC	a
rozdíl	rozdíl	k1gInSc1	rozdíl
dorovnává	dorovnávat	k5eAaImIp3nS	dorovnávat
pravidelně	pravidelně	k6eAd1	pravidelně
vkládaným	vkládaný	k2eAgInSc7d1	vkládaný
přestupným	přestupný	k2eAgInSc7d1	přestupný
rokem	rok	k1gInSc7	rok
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
delším	dlouhý	k2eAgInPc3d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Lunisolární	Lunisolární	k2eAgInSc1d1	Lunisolární
kalendář	kalendář	k1gInSc1	kalendář
navíc	navíc	k6eAd1	navíc
rok	rok	k1gInSc4	rok
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
dvanácti	dvanáct	k4xCc2	dvanáct
kalendářních	kalendářní	k2eAgInPc2d1	kalendářní
měsíců	měsíc	k1gInPc2	měsíc
daných	daný	k2eAgInPc2d1	daný
oběhem	oběh	k1gInSc7	oběh
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
trvají	trvat	k5eAaImIp3nP	trvat
přibližně	přibližně	k6eAd1	přibližně
354	[number]	k4	354
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Přestupný	přestupný	k2eAgInSc1d1	přestupný
rok	rok	k1gInSc1	rok
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
13	[number]	k4	13
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
lunisolárního	lunisolární	k2eAgInSc2d1	lunisolární
roku	rok	k1gInSc2	rok
proto	proto	k8xC	proto
výrazněji	výrazně	k6eAd2	výrazně
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
.	.	kIx.	.
</s>
<s>
Lunární	lunární	k2eAgInPc1d1	lunární
kalendáře	kalendář	k1gInPc1	kalendář
užívají	užívat	k5eAaImIp3nP	užívat
pevnou	pevný	k2eAgFnSc4d1	pevná
délku	délka	k1gFnSc4	délka
roku	rok	k1gInSc2	rok
danou	daný	k2eAgFnSc4d1	daná
dvanácti	dvanáct	k4xCc2	dvanáct
oběhy	oběh	k1gInPc4	oběh
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
o	o	k7c4	o
víc	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
11	[number]	k4	11
dní	den	k1gInPc2	den
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
oběh	oběh	k1gInSc4	oběh
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
počátek	počátek	k1gInSc4	počátek
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gMnSc3	on
(	(	kIx(	(
<g/>
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
)	)	kIx)	)
cyklicky	cyklicky	k6eAd1	cyklicky
posouvá	posouvat	k5eAaImIp3nS	posouvat
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
kalendářní	kalendářní	k2eAgInSc1d1	kalendářní
rok	rok	k1gInSc1	rok
má	mít	k5eAaImIp3nS	mít
365	[number]	k4	365
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
dnech	den	k1gInPc6	den
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
délce	délka	k1gFnSc6	délka
tropického	tropický	k2eAgInSc2d1	tropický
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
365,242	[number]	k4	365,242
19	[number]	k4	19
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
době	doba	k1gFnSc3	doba
oběhu	oběh	k1gInSc2	oběh
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Přestupný	přestupný	k2eAgInSc1d1	přestupný
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
trvající	trvající	k2eAgFnSc2d1	trvající
366	[number]	k4	366
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
oproti	oproti	k7c3	oproti
běžnému	běžný	k2eAgInSc3d1	běžný
kalendářnímu	kalendářní	k2eAgInSc3d1	kalendářní
roku	rok	k1gInSc3	rok
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
přestupný	přestupný	k2eAgInSc4d1	přestupný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Přestupný	přestupný	k2eAgInSc1d1	přestupný
rok	rok	k1gInSc1	rok
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
rozdílu	rozdíl	k1gInSc2	rozdíl
délky	délka	k1gFnSc2	délka
běžného	běžný	k2eAgInSc2d1	běžný
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
a	a	k8xC	a
tropického	tropický	k2eAgInSc2d1	tropický
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
přibližně	přibližně	k6eAd1	přibližně
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
0,242	[number]	k4	0,242
19	[number]	k4	19
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
způsobí	způsobit	k5eAaPmIp3nS	způsobit
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
posun	posun	k1gInSc4	posun
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
přestupný	přestupný	k2eAgInSc4d1	přestupný
každý	každý	k3xTgInSc4	každý
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
rozdíl	rozdíl	k1gInSc4	rozdíl
přesněji	přesně	k6eAd2	přesně
vynecháním	vynechání	k1gNnPc3	vynechání
tří	tři	k4xCgInPc2	tři
přestupných	přestupný	k2eAgInPc2d1	přestupný
roků	rok	k1gInPc2	rok
během	během	k7c2	během
každých	každý	k3xTgNnPc2	každý
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
juliánský	juliánský	k2eAgInSc1d1	juliánský
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
roku	rok	k1gInSc2	rok
podle	podle	k7c2	podle
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
365,25	[number]	k4	365,25
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
365	[number]	k4	365
d	d	k?	d
6	[number]	k4	6
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
roku	rok	k1gInSc2	rok
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
365,242	[number]	k4	365,242
<g/>
5	[number]	k4	5
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
365	[number]	k4	365
d	d	k?	d
5	[number]	k4	5
h	h	k?	h
49	[number]	k4	49
min	min	kA	min
12	[number]	k4	12
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
daný	daný	k2eAgInSc1d1	daný
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uběhne	uběhnout	k5eAaPmIp3nS	uběhnout
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
opakováními	opakování	k1gNnPc7	opakování
události	událost	k1gFnSc2	událost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
oběhem	oběh	k1gInSc7	oběh
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Siderický	siderický	k2eAgInSc4d1	siderický
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
též	též	k9	též
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
Země	země	k1gFnSc1	země
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
vzdáleným	vzdálený	k2eAgFnPc3d1	vzdálená
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
365,256	[number]	k4	365,256
363	[number]	k4	363
051	[number]	k4	051
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
365	[number]	k4	365
d	d	k?	d
6	[number]	k4	6
h	h	k?	h
9	[number]	k4	9
min	min	kA	min
9	[number]	k4	9
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tropický	tropický	k2eAgInSc4d1	tropický
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
průchody	průchod	k1gInPc7	průchod
Slunce	slunce	k1gNnSc2	slunce
jarním	jarní	k2eAgInSc7d1	jarní
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
365,242	[number]	k4	365,242
192	[number]	k4	192
129	[number]	k4	129
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
365	[number]	k4	365
d	d	k?	d
5	[number]	k4	5
h	h	k?	h
48	[number]	k4	48
min	min	kA	min
45	[number]	k4	45
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
taky	taky	k6eAd1	taky
perioda	perioda	k1gFnSc1	perioda
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
roční	roční	k2eAgMnPc1d1	roční
období	období	k1gNnSc3	období
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Anomalistický	Anomalistický	k2eAgInSc4d1	Anomalistický
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uplyne	uplynout	k5eAaPmIp3nS	uplynout
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
průchody	průchod	k1gInPc7	průchod
Země	země	k1gFnSc1	země
přísluním	přísluní	k1gNnSc7	přísluní
(	(	kIx(	(
<g/>
perihéliem	perihélium	k1gNnSc7	perihélium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
365,259	[number]	k4	365,259
635	[number]	k4	635
864	[number]	k4	864
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
365	[number]	k4	365
d	d	k?	d
6	[number]	k4	6
h	h	k?	h
13	[number]	k4	13
min	min	kA	min
53	[number]	k4	53
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
rok	rok	k1gInSc1	rok
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobnosti	podobnost	k1gFnSc2	podobnost
nazývají	nazývat	k5eAaImIp3nP	nazývat
i	i	k9	i
doby	doba	k1gFnSc2	doba
oběhu	oběh	k1gInSc2	oběh
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dalších	další	k2eAgFnPc2d1	další
planet	planeta	k1gFnPc2	planeta
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
nebo	nebo	k8xC	nebo
i	i	k9	i
oběhu	oběh	k1gInSc2	oběh
Slunce	slunce	k1gNnSc2	slunce
kolem	kolem	k7c2	kolem
středu	střed	k1gInSc2	střed
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
(	(	kIx(	(
<g/>
galaktický	galaktický	k2eAgInSc4d1	galaktický
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rok	rok	k1gInSc4	rok
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
rok	rok	k1gInSc4	rok
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
