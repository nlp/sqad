<s>
Alicudi	Alicud	k1gMnPc1	Alicud
(	(	kIx(	(
<g/>
5,2	[number]	k4	5,2
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
italsky	italsky	k6eAd1	italsky
Isola	Isola	k1gMnSc1	Isola
Alicudi	Alicud	k1gMnPc1	Alicud
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejzápadnější	západní	k2eAgMnSc1d3	nejzápadnější
z	z	k7c2	z
Liparských	Liparský	k2eAgInPc2d1	Liparský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
administrativně	administrativně	k6eAd1	administrativně
součástí	součást	k1gFnSc7	součást
obce	obec	k1gFnSc2	obec
Lipari	Lipar	k1gFnSc2	Lipar
(	(	kIx(	(
<g/>
comune	comun	k1gInSc5	comun
di	di	k?	di
Lipari	Lipar	k1gMnPc7	Lipar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
malý	malý	k2eAgInSc1d1	malý
ostrov	ostrov	k1gInSc1	ostrov
v	v	k7c6	v
Tyrhénském	tyrhénský	k2eAgNnSc6d1	Tyrhénské
moři	moře	k1gNnSc6	moře
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
sopka	sopka	k1gFnSc1	sopka
(	(	kIx(	(
<g/>
počítáno	počítat	k5eAaImNgNnS	počítat
od	od	k7c2	od
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stará	starat	k5eAaImIp3nS	starat
asi	asi	k9	asi
90	[number]	k4	90
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrcholek	vrcholek	k1gInSc1	vrcholek
Filo	Fila	k1gMnSc5	Fila
dell	dell	k1gMnSc1	dell
<g/>
'	'	kIx"	'
Arpa	Arpa	k1gMnSc1	Arpa
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
nad	nad	k7c4	nad
mořskou	mořský	k2eAgFnSc4d1	mořská
hladinu	hladina	k1gFnSc4	hladina
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
675	[number]	k4	675
m.	m.	k?	m.
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgFnPc1	žádný
silnice	silnice	k1gFnPc1	silnice
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
soustava	soustava	k1gFnSc1	soustava
schodišť	schodiště	k1gNnPc2	schodiště
spojující	spojující	k2eAgFnSc2d1	spojující
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
osady	osada	k1gFnSc2	osada
a	a	k8xC	a
domy	dům	k1gInPc4	dům
roztroušené	roztroušený	k2eAgInPc4d1	roztroušený
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
sopečného	sopečný	k2eAgInSc2d1	sopečný
kužele	kužel	k1gInSc2	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
soumaři	soumar	k1gMnPc1	soumar
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
tvoří	tvořit	k5eAaImIp3nS	tvořit
strmé	strmý	k2eAgInPc4d1	strmý
útesy	útes	k1gInPc4	útes
<g/>
,	,	kIx,	,
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
i	i	k9	i
pláž	pláž	k1gFnSc4	pláž
ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
řada	řada	k1gFnSc1	řada
opuštěných	opuštěný	k2eAgMnPc2d1	opuštěný
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
těch	ten	k3xDgFnPc2	ten
obydlených	obydlený	k2eAgFnPc2d1	obydlená
tvoří	tvořit	k5eAaImIp3nS	tvořit
letní	letní	k2eAgNnPc4d1	letní
sídla	sídlo	k1gNnPc4	sídlo
<g/>
,	,	kIx,	,
především	především	k9	především
Italů	Ital	k1gMnPc2	Ital
ze	z	k7c2	z
Sicílie	Sicílie	k1gFnSc2	Sicílie
a	a	k8xC	a
také	také	k9	také
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Trvale	trvale	k6eAd1	trvale
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
130	[number]	k4	130
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
