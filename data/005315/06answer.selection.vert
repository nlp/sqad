<s>
Schengenskou	schengenský	k2eAgFnSc4d1	Schengenská
smlouvu	smlouva	k1gFnSc4	smlouva
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
Francie	Francie	k1gFnPc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1985	[number]	k4	1985
u	u	k7c2	u
lucemburského	lucemburský	k2eAgInSc2d1	lucemburský
Schengenu	Schengen	k1gInSc2	Schengen
<g/>
.	.	kIx.	.
</s>
