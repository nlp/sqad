<s>
Schengenská	schengenský	k2eAgFnSc1d1	Schengenská
smlouva	smlouva	k1gFnSc1	smlouva
neboli	neboli	k8xC	neboli
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
postupném	postupný	k2eAgNnSc6d1	postupné
rušení	rušení	k1gNnSc6	rušení
kontrol	kontrola	k1gFnPc2	kontrola
na	na	k7c6	na
společných	společný	k2eAgFnPc6d1	společná
hranicích	hranice	k1gFnPc6	hranice
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc1	součást
práva	právo	k1gNnSc2	právo
EU	EU	kA	EU
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
společný	společný	k2eAgInSc4d1	společný
hraniční	hraniční	k2eAgInSc4d1	hraniční
režim	režim	k1gInSc4	režim
a	a	k8xC	a
společnou	společný	k2eAgFnSc4d1	společná
imigrační	imigrační	k2eAgFnSc4d1	imigrační
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
zrušení	zrušení	k1gNnSc4	zrušení
celních	celní	k2eAgFnPc2d1	celní
a	a	k8xC	a
pasových	pasový	k2eAgFnPc2d1	pasová
kontrol	kontrola	k1gFnPc2	kontrola
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
států	stát	k1gInPc2	stát
EU	EU	kA	EU
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
tzv.	tzv.	kA	tzv.
Schengenský	schengenský	k2eAgInSc4d1	schengenský
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Schengenskou	schengenský	k2eAgFnSc4d1	Schengenská
smlouvu	smlouva	k1gFnSc4	smlouva
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
Francie	Francie	k1gFnPc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1985	[number]	k4	1985
u	u	k7c2	u
lucemburského	lucemburský	k2eAgInSc2d1	lucemburský
Schengenu	Schengen	k1gInSc2	Schengen
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
ji	on	k3xPp3gFnSc4	on
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
26	[number]	k4	26
států	stát	k1gInPc2	stát
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgInPc1	všechen
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
EU	EU	kA	EU
kromě	kromě	k7c2	kromě
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
26	[number]	k4	26
států	stát	k1gInPc2	stát
ji	on	k3xPp3gFnSc4	on
již	již	k9	již
provedlo	provést	k5eAaPmAgNnS	provést
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tvoří	tvořit	k5eAaImIp3nP	tvořit
Schengenský	schengenský	k2eAgInSc4d1	schengenský
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Schengenu	Schengen	k1gInSc6	Schengen
byla	být	k5eAaImAgFnS	být
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
podepsána	podepsat	k5eAaPmNgFnS	podepsat
zástupci	zástupce	k1gMnPc7	zástupce
vlád	vláda	k1gFnPc2	vláda
původních	původní	k2eAgInPc2d1	původní
pěti	pět	k4xCc2	pět
signatářských	signatářský	k2eAgInPc2d1	signatářský
států	stát	k1gInPc2	stát
Úmluva	úmluva	k1gFnSc1	úmluva
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
Schengenské	schengenský	k2eAgFnSc2d1	Schengenská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
úmluva	úmluva	k1gFnSc1	úmluva
podrobněji	podrobně	k6eAd2	podrobně
popisuje	popisovat	k5eAaImIp3nS	popisovat
zejména	zejména	k6eAd1	zejména
principy	princip	k1gInPc4	princip
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
Schengenského	schengenský	k2eAgInSc2d1	schengenský
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
všech	všecek	k3xTgNnPc2	všecek
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
dána	dát	k5eAaPmNgFnS	dát
Schengenskou	schengenský	k2eAgFnSc7d1	Schengenská
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
Úmluvou	úmluva	k1gFnSc7	úmluva
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
Schengenské	schengenský	k2eAgFnSc2d1	Schengenská
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
navazujícími	navazující	k2eAgInPc7d1	navazující
předpisy	předpis	k1gInPc7	předpis
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
schengenské	schengenský	k2eAgFnPc4d1	Schengenská
acquis	acquis	k1gFnPc4	acquis
<g/>
.	.	kIx.	.
</s>
<s>
Amsterodamská	amsterodamský	k2eAgFnSc1d1	Amsterodamská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
začlenila	začlenit	k5eAaPmAgFnS	začlenit
Schengenskou	schengenský	k2eAgFnSc4d1	Schengenská
smlouvu	smlouva	k1gFnSc4	smlouva
do	do	k7c2	do
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
nepodepisují	podepisovat	k5eNaImIp3nP	podepisovat
Schengenskou	schengenský	k2eAgFnSc4d1	Schengenská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
okamžikem	okamžik	k1gInSc7	okamžik
vstupu	vstup	k1gInSc2	vstup
stává	stávat	k5eAaImIp3nS	stávat
závazný	závazný	k2eAgInSc1d1	závazný
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
všechny	všechen	k3xTgFnPc1	všechen
další	další	k2eAgFnPc1d1	další
smlouvy	smlouva	k1gFnPc1	smlouva
a	a	k8xC	a
předpisy	předpis	k1gInPc1	předpis
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
země	zem	k1gFnPc1	zem
však	však	k9	však
stávají	stávat	k5eAaImIp3nP	stávat
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
když	když	k8xS	když
splní	splnit	k5eAaPmIp3nP	splnit
všechny	všechen	k3xTgFnPc4	všechen
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
uvedené	uvedený	k2eAgFnPc4d1	uvedená
podmínky	podmínka	k1gFnPc4	podmínka
a	a	k8xC	a
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
schválí	schválit	k5eAaPmIp3nS	schválit
Rada	rada	k1gFnSc1	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
nebyla	být	k5eNaImAgFnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
podepsána	podepsán	k2eAgFnSc1d1	podepsána
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Schengen	Schengen	k1gInSc4	Schengen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
Princesse	Princesse	k1gFnSc2	Princesse
Marie-Astrid	Marie-Astrida	k1gFnPc2	Marie-Astrida
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kotvila	kotvit	k5eAaImAgFnS	kotvit
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Mosele	Mosel	k1gInSc2	Mosel
u	u	k7c2	u
Schengenu	Schengen	k1gInSc2	Schengen
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
vesnice	vesnice	k1gFnSc1	vesnice
Schengen	Schengen	k1gInSc1	Schengen
běžně	běžně	k6eAd1	běžně
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
místo	místo	k7c2	místo
podpisu	podpis	k1gInSc2	podpis
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
i	i	k9	i
památník	památník	k1gInSc1	památník
Schengenské	schengenský	k2eAgFnSc2d1	Schengenská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
