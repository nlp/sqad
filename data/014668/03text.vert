<s>
Bifurkace	bifurkace	k1gFnSc1
(	(	kIx(
<g/>
geografie	geografie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Umělé	umělý	k2eAgNnSc1d1
rozvodí	rozvodit	k5eAaImIp3nS
Bělský	Bělský	k2eAgInSc4d1
potok	potok	k1gInSc4
/	/	kIx~
Ostružník	ostružník	k1gInSc4
pod	pod	k7c7
Děčínským	děčínský	k2eAgInSc7d1
Sněžníkem	Sněžník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Bifurkace	bifurkace	k1gFnSc1
Hase	hasit	k5eAaImSgMnS
u	u	k7c2
Melle	Melle	k1gFnSc2
</s>
<s>
Bifurkace	bifurkace	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
furca	furca	k1gInSc2
<g/>
,	,	kIx,
vidlička	vidlička	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
geografii	geografie	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
rozvětvení	rozvětvení	k1gNnSc1
řeky	řeka	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc4
rozdělení	rozdělení	k1gNnSc1
do	do	k7c2
dvou	dva	k4xCgNnPc2
koryt	koryto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgInSc7
jevem	jev	k1gInSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
zejména	zejména	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
plochých	plochý	k2eAgInPc2d1
rozvodí	rozvodit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
odevzdává	odevzdávat	k5eAaImIp3nS
vodu	voda	k1gFnSc4
různými	různý	k2eAgInPc7d1
směry	směr	k1gInPc7
do	do	k7c2
různých	různý	k2eAgInPc2d1
říčních	říční	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
řeky	řeka	k1gFnSc2
nad	nad	k7c7
místem	místo	k1gNnSc7
bifurkace	bifurkace	k1gFnSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
součástí	součást	k1gFnSc7
obou	dva	k4xCgInPc2
takto	takto	k6eAd1
propojených	propojený	k2eAgInPc2d1
povodí	povodit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1
případem	případ	k1gInSc7
bifurkace	bifurkace	k1gFnSc2
je	být	k5eAaImIp3nS
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
voda	voda	k1gFnSc1
odtéká	odtékat	k5eAaImIp3nS
do	do	k7c2
dvou	dva	k4xCgNnPc2
různých	různý	k2eAgNnPc2d1
povodí	povodí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
finské	finský	k2eAgNnSc1d1
Vesijako	Vesijako	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
voda	voda	k1gFnSc1
odtéká	odtékat	k5eAaImIp3nS
částečně	částečně	k6eAd1
přes	přes	k7c4
Päijänne	Päijänn	k1gInSc5
do	do	k7c2
Finského	finský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
a	a	k8xC
částečně	částečně	k6eAd1
přes	přes	k7c4
Vanajavesi	Vanajavese	k1gFnSc4
a	a	k8xC
Pyhäjärvi	Pyhäjärev	k1gFnSc3
do	do	k7c2
zálivu	záliv	k1gInSc2
Botnického	botnický	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příklady	příklad	k1gInPc1
</s>
<s>
Rozvodí	rozvodí	k1gNnSc1
Lužnice	Lužnice	k1gFnSc2
</s>
<s>
Bělský	Bělský	k2eAgInSc1d1
potok	potok	k1gInSc1
a	a	k8xC
Ostružník	ostružník	k1gInSc1
(	(	kIx(
<g/>
povodí	povodí	k1gNnSc2
Jílovského	jílovský	k2eAgInSc2d1
potoka	potok	k1gInSc2
a	a	k8xC
Labe	Labe	k1gNnSc2
<g/>
)	)	kIx)
u	u	k7c2
města	město	k1gNnSc2
Děčín	Děčín	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
první	první	k4xOgInSc1
obrázek	obrázek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Jílovský	jílovský	k2eAgInSc1d1
potok	potok	k1gInSc1
a	a	k8xC
Klíšský	Klíšský	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
povodí	povodí	k1gNnSc2
Labe	Labe	k1gNnSc2
a	a	k8xC
Bíliny	Bílina	k1gFnSc2
<g/>
)	)	kIx)
u	u	k7c2
Libouchce	Libouchce	k1gFnSc2
</s>
<s>
Casiquiare	Casiquiar	k1gMnSc5
a	a	k8xC
Maturaca	Maturacum	k1gNnPc1
mezi	mezi	k7c7
říčním	říční	k2eAgInSc7d1
systémem	systém	k1gInSc7
Orinoka	Orinoko	k1gNnSc2
a	a	k8xC
Amazonky	Amazonka	k1gFnSc2
ve	v	k7c6
Venezuele	Venezuela	k1gFnSc6
</s>
<s>
Torne	Tornout	k5eAaPmIp3nS,k5eAaImIp3nS
ztrácí	ztrácet	k5eAaImIp3nS
nad	nad	k7c7
Pajalou	Pajala	k1gFnSc7
57	#num#	k4
%	%	kIx~
své	svůj	k3xOyFgFnSc2
vody	voda	k1gFnSc2
odtokem	odtok	k1gInSc7
do	do	k7c2
řeky	řeka	k1gFnSc2
Kalix	Kalix	k1gInSc1
spojovací	spojovací	k2eAgInSc1d1
řekou	řeka	k1gFnSc7
Tärendö	Tärendö	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Západní	západní	k2eAgInSc1d1
a	a	k8xC
Východní	východní	k2eAgInSc1d1
Manyč	Manyč	k1gInSc1
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Rusku	Rusko	k1gNnSc6
</s>
<s>
Kuloj	Kuloj	k1gInSc1
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Rusku	Rusko	k1gNnSc6
</s>
<s>
Obra	obr	k1gMnSc2
a	a	k8xC
Obrzyca	Obrzycus	k1gMnSc2
v	v	k7c6
Polsku	Polsko	k1gNnSc6
</s>
<s>
Assiniboine	Assiniboinout	k5eAaPmIp3nS
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
</s>
<s>
Hase	hasit	k5eAaImSgMnS
do	do	k7c2
Else	Elsa	k1gFnSc3
(	(	kIx(
<g/>
Werra	Werra	k1gFnSc1
<g/>
,	,	kIx,
Vezera	Vezera	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Emže	Emže	k1gFnSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
</s>
<s>
Lužnice	Lužnice	k1gFnSc1
se	se	k3xPyFc4
rozděluje	rozdělovat	k5eAaImIp3nS
na	na	k7c4
Starou	starý	k2eAgFnSc4d1
a	a	k8xC
Novou	nový	k2eAgFnSc4d1
řeku	řeka	k1gFnSc4
(	(	kIx(
<g/>
umělý	umělý	k2eAgInSc4d1
kanál	kanál	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
se	se	k3xPyFc4
vlévá	vlévat	k5eAaImIp3nS
do	do	k7c2
Nežárky	Nežárka	k1gFnSc2
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
později	pozdě	k6eAd2
zpět	zpět	k6eAd1
do	do	k7c2
Lužnice	Lužnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Katra	Katra	k6eAd1
vlévá	vlévat	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
Němenu	Němen	k1gInSc2
a	a	k8xC
Ū	Ū	k1gFnSc2
v	v	k7c6
Litvě	Litva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Arroyo	Arroya	k1gFnSc5
Partido	Partida	k1gFnSc5
v	v	k7c6
Argentině	Argentina	k1gFnSc6
teče	téct	k5eAaImIp3nS
dokonce	dokonce	k9
de	de	k?
dvou	dva	k4xCgInPc2
oceánů	oceán	k1gInPc2
–	–	k?
Tichého	Tichý	k1gMnSc2
a	a	k8xC
Atlantského	atlantský	k2eAgMnSc2d1
<g/>
;	;	kIx,
říčka	říčka	k1gFnSc1
se	se	k3xPyFc4
vlévá	vlévat	k5eAaImIp3nS
do	do	k7c2
Ría	Ría	k1gMnSc2
Negra	negr	k1gMnSc2
a	a	k8xC
do	do	k7c2
Atlantiku	Atlantik	k1gInSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
do	do	k7c2
řeky	řeka	k1gFnSc2
Valdivie	Valdivie	k1gFnSc2
a	a	k8xC
do	do	k7c2
Pacifiku	Pacifik	k1gInSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Vesijako	Vesijako	k1gNnSc1
(	(	kIx(
<g/>
35.784.1.001	35.784.1.001	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Järviwiki	Järviwiki	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
finsky	finsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pajala	Pajala	k1gFnSc1
Municipality	municipalita	k1gFnSc2
-	-	kIx~
The	The	k1gFnSc1
natural	natural	k?
and	and	k?
cultural	culturat	k5eAaPmAgInS,k5eAaImAgInS
landscape	landscapat	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pajala	Pajala	k1gFnSc1
<g/>
:	:	kIx,
Pajala	Pajala	k1gFnSc1
Kommun	Kommun	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
pirátství	pirátství	k1gNnSc1
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
</s>
<s>
náčepní	náčepný	k2eAgMnPc1d1
loket	loket	k1gInSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bifurkace	bifurkace	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
7593	#num#	k4
</s>
