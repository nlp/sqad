<s>
Bifurkace	bifurkace	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
furca	furca	k1gInSc2
<g/>
,	,	kIx,
vidlička	vidlička	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
geografii	geografie	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
rozvětvení	rozvětvení	k1gNnSc1
řeky	řeka	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc4
rozdělení	rozdělení	k1gNnSc1
do	do	k7c2
dvou	dva	k4xCgNnPc2
koryt	koryto	k1gNnPc2
<g/>
.	.	kIx.
</s>