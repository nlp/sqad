<s>
Bitcoin	Bitcoin	k1gMnSc1
</s>
<s>
Bitcoin	Bitcoin	k1gInSc1
Logo	logo	k1gNnSc1
bitcoinuZemě	bitcoinuZemě	k6eAd1
</s>
<s>
mezinárodní	mezinárodní	k2eAgFnSc1d1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
XBT	XBT	kA
(	(	kIx(
<g/>
neoficiální	neoficiální	k2eAgFnSc1d1,k2eNgFnSc1d1
<g/>
)	)	kIx)
Inflace	inflace	k1gFnSc1
</s>
<s>
6.25	6.25	k4
nových	nový	k2eAgMnPc2d1
bitcoinů	bitcoin	k1gInPc2
cca	cca	kA
každých	každý	k3xTgInPc2
10	#num#	k4
minut	minuta	k1gFnPc2
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
2024	#num#	k4
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
BTC	BTC	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
mBTC	mBTC	k?
<g/>
,	,	kIx,
μ	μ	k?
<g/>
,	,	kIx,
satoši	satoš	k1gMnPc1
<g/>
,	,	kIx,
bit	bit	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitcoin	Bitcoin	k1gInSc1
je	být	k5eAaImIp3nS
internetová	internetový	k2eAgFnSc1d1
open-source	open-sourka	k1gFnSc3
P2P	P2P	k1gFnSc4
platební	platební	k2eAgFnSc4d1
síť	síť	k1gFnSc4
a	a	k8xC
také	také	k9
v	v	k7c6
této	tento	k3xDgFnSc6
síti	síť	k1gFnSc6
používaná	používaný	k2eAgFnSc1d1
kryptoměna	kryptoměn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
unikátností	unikátnost	k1gFnSc7
bitcoinu	bitcoin	k1gInSc2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
plná	plný	k2eAgFnSc1d1
decentralizace	decentralizace	k1gFnSc1
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
navržen	navrhnout	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nikdo	nikdo	k3yNnSc1
<g/>
,	,	kIx,
ani	ani	k8xC
autor	autor	k1gMnSc1
nebo	nebo	k8xC
jiní	jiný	k2eAgMnPc1d1
jednotlivci	jednotlivec	k1gMnPc1
<g/>
,	,	kIx,
skupiny	skupina	k1gFnPc1
či	či	k8xC
vlády	vláda	k1gFnPc1
<g/>
,	,	kIx,
nemohl	moct	k5eNaImAgMnS
měnu	měna	k1gFnSc4
ovlivňovat	ovlivňovat	k5eAaImF
<g/>
,	,	kIx,
padělat	padělat	k5eAaBmF
<g/>
,	,	kIx,
zabavovat	zabavovat	k5eAaImF
účty	účet	k1gInPc4
<g/>
,	,	kIx,
ovládat	ovládat	k5eAaImF
peněžní	peněžní	k2eAgInPc4d1
toky	tok	k1gInPc4
nebo	nebo	k8xC
způsobovat	způsobovat	k5eAaImF
inflaci	inflace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
síti	síť	k1gFnSc6
neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
centrální	centrální	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
,	,	kIx,
ani	ani	k8xC
nikdo	nikdo	k3yNnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
o	o	k7c6
síti	síť	k1gFnSc6
rozhodovat	rozhodovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečné	Konečná	k1gFnSc6
množství	množství	k1gNnSc4
bitcoinů	bitcoin	k1gInPc2
je	být	k5eAaImIp3nS
předem	předem	k6eAd1
známo	znám	k2eAgNnSc1d1
a	a	k8xC
uvolňování	uvolňování	k1gNnSc1
bitcoinů	bitcoin	k1gMnPc2
do	do	k7c2
oběhu	oběh	k1gInSc2
je	být	k5eAaImIp3nS
definováno	definovat	k5eAaBmNgNnS
ve	v	k7c6
zdrojovém	zdrojový	k2eAgInSc6d1
kódu	kód	k1gInSc6
protokolu	protokol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
jednoho	jeden	k4xCgInSc2
Bitcoinu	Bitcoin	k1gInSc2
je	být	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
2021	#num#	k4
<g/>
,	,	kIx,
dosahovala	dosahovat	k5eAaImAgFnS
přibližně	přibližně	k6eAd1
800	#num#	k4
tis	tis	k1gInSc4
<g/>
.	.	kIx.
korun	koruna	k1gFnPc2
českých	český	k2eAgFnPc2d1
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
cena	cena	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
překročila	překročit	k5eAaPmAgFnS
1	#num#	k4
milión	milión	k4xCgInSc1
korun	koruna	k1gFnPc2
českých	český	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Síť	síť	k1gFnSc1
funguje	fungovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2008	#num#	k4
ji	on	k3xPp3gFnSc4
popsal	popsat	k5eAaPmAgInS
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
člověk	člověk	k1gMnSc1
nebo	nebo	k8xC
skupina	skupina	k1gFnSc1
lidí	člověk	k1gMnPc2
podepsaná	podepsaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Satoshi	Satoshi	k1gNnSc1
Nakamoto	Nakamota	k1gFnSc5
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
přičemž	přičemž	k6eAd1
ještě	ještě	k9
2	#num#	k4
měsíce	měsíc	k1gInPc4
před	před	k7c7
tím	ten	k3xDgNnSc7
byla	být	k5eAaImAgFnS
zaregistrována	zaregistrován	k2eAgFnSc1d1
doména	doména	k1gFnSc1
bitcoin	bitcoina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
K	k	k7c3
autorství	autorství	k1gNnSc3
se	se	k3xPyFc4
v	v	k7c6
květnu	květen	k1gInSc6
2016	#num#	k4
přihlásil	přihlásit	k5eAaPmAgMnS
Australan	Australan	k1gMnSc1
Craig	Craig	k1gMnSc1
Steven	Steven	k2eAgInSc4d1
Wright	Wright	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
ale	ale	k9
rychle	rychle	k6eAd1
zpochybněno	zpochybnit	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Sahil	Sahil	k1gMnSc1
Gupta	Gupta	k1gMnSc1
označil	označit	k5eAaPmAgMnS
za	za	k7c4
pravděpodobného	pravděpodobný	k2eAgMnSc4d1
autora	autor	k1gMnSc4
bitcoinu	bitcoina	k1gMnSc4
Elona	Elon	k1gMnSc4
Muska	Musek	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
hluboké	hluboký	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
šifrování	šifrování	k1gNnSc2
a	a	k8xC
kódování	kódování	k1gNnSc2
<g/>
,	,	kIx,
Elon	Elon	k1gMnSc1
Musk	Musk	k1gMnSc1
ale	ale	k9
toto	tento	k3xDgNnSc4
tvrzení	tvrzení	k1gNnSc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základem	základ	k1gInSc7
pro	pro	k7c4
vznik	vznik	k1gInSc4
bitcoinu	bitcoin	k1gInSc2
byly	být	k5eAaImAgInP
koncepty	koncept	k1gInPc1
„	„	k?
<g/>
Bit	bit	k1gInSc1
gold	gold	k1gInSc1
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
„	„	k?
<g/>
b-Money	b-Monea	k1gFnSc2
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
pro	pro	k7c4
platby	platba	k1gFnPc4
existuje	existovat	k5eAaImIp3nS
oficiální	oficiální	k2eAgMnSc1d1
softwarový	softwarový	k2eAgMnSc1d1
klient	klient	k1gMnSc1
<g/>
,	,	kIx,
o	o	k7c4
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
se	se	k3xPyFc4
starají	starat	k5eAaImIp3nP
dobrovolníci	dobrovolník	k1gMnPc1
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
několik	několik	k4yIc1
alternativních	alternativní	k2eAgMnPc2d1
klientů	klient	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
se	se	k3xPyFc4
o	o	k7c6
bitcoinu	bitcoino	k1gNnSc6
<g/>
,	,	kIx,
ekonomice	ekonomika	k1gFnSc6
<g/>
,	,	kIx,
možnostech	možnost	k1gFnPc6
a	a	k8xC
důsledcích	důsledek	k1gInPc6
této	tento	k3xDgFnSc2
měny	měna	k1gFnSc2
pořádají	pořádat	k5eAaImIp3nP
konference	konference	k1gFnSc1
–	–	k?
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
,	,	kIx,
Praze	Praha	k1gFnSc6
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Londýně	Londýn	k1gInSc6
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
proběhla	proběhnout	k5eAaPmAgFnS
roku	rok	k1gInSc2
2013	#num#	k4
v	v	k7c6
San	San	k1gFnSc6
Jose	Jos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příští	příští	k2eAgFnSc1d1
konference	konference	k1gFnSc1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
konat	konat	k5eAaImF
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2012	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
Bitcoin	Bitcoin	k2eAgInSc4d1
Foundation	Foundation	k1gInSc4
<g/>
,	,	kIx,
starající	starající	k2eAgInSc4d1
se	se	k3xPyFc4
o	o	k7c4
infrastrukturu	infrastruktura	k1gFnSc4
okolo	okolo	k7c2
bitcoinu	bitcoin	k1gInSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
hlavního	hlavní	k2eAgMnSc2d1
klienta	klient	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sledování	sledování	k1gNnSc1
hrozeb	hrozba	k1gFnPc2
a	a	k8xC
případné	případný	k2eAgNnSc4d1
vylepšování	vylepšování	k1gNnSc4
protokolu	protokol	k1gInSc2
<g/>
,	,	kIx,
zajišťování	zajišťování	k1gNnSc1
konferencí	konference	k1gFnPc2
a	a	k8xC
propagaci	propagace	k1gFnSc4
měny	měna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
decentralizované	decentralizovaný	k2eAgFnSc3d1
povaze	povaha	k1gFnSc3
sítě	síť	k1gFnSc2
však	však	k8xC
nadace	nadace	k1gFnSc2
nemá	mít	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
zvláštní	zvláštní	k2eAgFnPc4d1
pravomoce	pravomoc	k1gFnPc4
<g/>
;	;	kIx,
v	v	k7c6
síti	síť	k1gFnSc6
vždy	vždy	k6eAd1
rozhoduje	rozhodovat	k5eAaImIp3nS
většina	většina	k1gFnSc1
<g/>
,	,	kIx,
nehledě	hledět	k5eNaImSgMnS
na	na	k7c4
Bitcoin	Bitcoin	k2eAgInSc4d1
Foundation	Foundation	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
měn	měna	k1gFnPc2
není	být	k5eNaImIp3nS
bitcoin	bitcoin	k1gInSc1
závislý	závislý	k2eAgInSc1d1
na	na	k7c6
důvěře	důvěra	k1gFnSc6
k	k	k7c3
jejímu	její	k3xOp3gMnSc3
(	(	kIx(
<g/>
centrálnímu	centrální	k2eAgInSc3d1
<g/>
)	)	kIx)
vydavateli	vydavatel	k1gMnSc3
ani	ani	k8xC
prostředníkovi	prostředník	k1gMnSc3
(	(	kIx(
<g/>
bance	banka	k1gFnSc6
<g/>
,	,	kIx,
státu	stát	k1gInSc6
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
provádění	provádění	k1gNnSc4
transakcí	transakce	k1gFnPc2
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
distribuovaná	distribuovaný	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
napříč	napříč	k7c7
uzly	uzel	k1gInPc7
peer-to-peer	peer-to-pera	k1gFnPc2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Měna	měna	k1gFnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
široké	široký	k2eAgFnSc6d1
komunitě	komunita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sociální	sociální	k2eAgNnPc1d1
média	médium	k1gNnPc1
tak	tak	k9
mohou	moct	k5eAaImIp3nP
nepřímo	přímo	k6eNd1
ovlivňovat	ovlivňovat	k5eAaImF
kurz	kurz	k1gInSc4
bitcoinu	bitcoin	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
zabezpečení	zabezpečení	k1gNnSc3
sítě	síť	k1gFnSc2
je	být	k5eAaImIp3nS
využita	využít	k5eAaPmNgFnS
kryptografie	kryptografie	k1gFnSc1
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgFnSc1d1
používat	používat	k5eAaImF
pouze	pouze	k6eAd1
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
daný	daný	k2eAgMnSc1d1
uživatel	uživatel	k1gMnSc1
vlastní	vlastnit	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
zabraňující	zabraňující	k2eAgInSc1d1
opakovanému	opakovaný	k2eAgNnSc3d1
využití	využití	k1gNnSc3
již	již	k6eAd1
utracených	utracený	k2eAgInPc2d1
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předpoklad	předpoklad	k1gInSc1
růstu	růst	k1gInSc2
počtu	počet	k1gInSc2
bitcoinů	bitcoin	k1gMnPc2
do	do	k7c2
roku	rok	k1gInSc2
2033	#num#	k4
Bitcoin	Bitcoina	k1gFnPc2
umožňuje	umožňovat	k5eAaImIp3nS
pseudonymní	pseudonymní	k2eAgNnSc4d1
držení	držení	k1gNnSc4
a	a	k8xC
převod	převod	k1gInSc4
měny	měna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoiny	Bitcoina	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
uloženy	uložit	k5eAaPmNgInP
v	v	k7c6
osobním	osobní	k2eAgInSc6d1
počítači	počítač	k1gInSc6
ve	v	k7c6
formě	forma	k1gFnSc6
souboru	soubor	k1gInSc2
s	s	k7c7
peněženkou	peněženka	k1gFnSc7
nebo	nebo	k8xC
uchovávány	uchovávat	k5eAaImNgInP
pomocí	pomocí	k7c2
služby	služba	k1gFnSc2
třetí	třetí	k4xOgFnSc2
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
však	však	k9
možné	možný	k2eAgNnSc1d1
mít	mít	k5eAaImF
peněženku	peněženka	k1gFnSc4
i	i	k9
zcela	zcela	k6eAd1
offline	offlin	k1gInSc5
(	(	kIx(
<g/>
na	na	k7c6
papíře	papír	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
lze	lze	k6eAd1
zcela	zcela	k6eAd1
offline	offlin	k1gInSc5
adresu	adresa	k1gFnSc4
vygenerovat	vygenerovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peer-to-peer	Peer-to-peer	k1gInSc1
topologie	topologie	k1gFnSc2
a	a	k8xC
chybějící	chybějící	k2eAgFnSc1d1
centrální	centrální	k2eAgFnSc1d1
autorita	autorita	k1gFnSc1
zabraňuje	zabraňovat	k5eAaImIp3nS
komukoliv	kdokoliv	k3yInSc3
manipulovat	manipulovat	k5eAaImF
se	s	k7c7
zásobou	zásoba	k1gFnSc7
této	tento	k3xDgFnSc2
měny	měna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečné	Konečná	k1gFnSc6
množství	množství	k1gNnSc4
bitcoinů	bitcoin	k1gInPc2
v	v	k7c6
oběhu	oběh	k1gInSc6
je	být	k5eAaImIp3nS
předem	předem	k6eAd1
dané	daný	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
vyvolat	vyvolat	k5eAaPmF
umělou	umělý	k2eAgFnSc4d1
inflaci	inflace	k1gFnSc4
vytvořením	vytvoření	k1gNnSc7
množství	množství	k1gNnSc2
většího	veliký	k2eAgNnSc2d2
<g/>
.	.	kIx.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
množství	množství	k1gNnSc1
bitcoinů	bitcoin	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
budou	být	k5eAaImBp3nP
vytěženy	vytěžen	k2eAgFnPc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
21	#num#	k4
000	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
20	#num#	k4
999	#num#	k4
999,976	999,976	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Růst	růst	k1gInSc1
se	se	k3xPyFc4
totiž	totiž	k9
postupně	postupně	k6eAd1
zpomaluje	zpomalovat	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
veškeré	veškerý	k3xTgFnPc1
bitcoiny	bitcoina	k1gFnPc1
budou	být	k5eAaImBp3nP
vytěženy	vytěžit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
2140	#num#	k4
(	(	kIx(
<g/>
drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
však	však	k8xC
cca	cca	kA
v	v	k7c6
roce	rok	k1gInSc6
2030	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
tedy	tedy	k9
bude	být	k5eAaImBp3nS
stále	stále	k6eAd1
stejný	stejný	k2eAgInSc1d1
zájem	zájem	k1gInSc1
o	o	k7c4
novou	nový	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
<g/>
,	,	kIx,
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
deflaci	deflace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
v	v	k7c6
protokolu	protokol	k1gInSc6
počítáno	počítat	k5eAaImNgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
lze	lze	k6eAd1
platit	platit	k5eAaImF
i	i	k9
zlomky	zlomek	k1gInPc4
bitcoinů	bitcoin	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
má	mít	k5eAaImIp3nS
síť	síť	k1gFnSc1
dělitelnost	dělitelnost	k1gFnSc1
na	na	k7c4
8	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
ji	on	k3xPp3gFnSc4
však	však	k9
rozšířit	rozšířit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základní	základní	k2eAgInSc1d1
princip	princip	k1gInSc1
fungování	fungování	k1gNnSc2
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
a	a	k8xC
těžaři	těžař	k1gMnPc1
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1
komunikace	komunikace	k1gFnSc1
v	v	k7c6
síti	síť	k1gFnSc6
probíhá	probíhat	k5eAaImIp3nS
pomocí	pomocí	k7c2
počítačového	počítačový	k2eAgInSc2d1
programu	program	k1gInSc2
(	(	kIx(
<g/>
nebo	nebo	k8xC
jiného	jiný	k2eAgMnSc2d1
klienta	klient	k1gMnSc2
<g/>
,	,	kIx,
např.	např.	kA
na	na	k7c6
mobilu	mobil	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
komunikuje	komunikovat	k5eAaImIp3nS
s	s	k7c7
dalšími	další	k2eAgInPc7d1
uzly	uzel	k1gInPc7
(	(	kIx(
<g/>
účastníky	účastník	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastníci	účastník	k1gMnPc1
jsou	být	k5eAaImIp3nP
dvojího	dvojí	k4xRgMnSc2
druhu	druh	k1gInSc2
<g/>
:	:	kIx,
koncoví	koncový	k2eAgMnPc1d1
uživatelé	uživatel	k1gMnPc1
a	a	k8xC
těžaři	těžař	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
účastník	účastník	k1gMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
koncový	koncový	k2eAgMnSc1d1
uživatel	uživatel	k1gMnSc1
<g/>
,	,	kIx,
těžař	těžař	k1gMnSc1
<g/>
,	,	kIx,
anebo	anebo	k8xC
obojí	obojí	k4xRgInPc1
<g/>
.	.	kIx.
</s>
<s>
koncoví	koncový	k2eAgMnPc1d1
uživatelé	uživatel	k1gMnPc1
jsou	být	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
si	se	k3xPyFc3
posílají	posílat	k5eAaImIp3nP
Bitcoiny	Bitcoina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
uživatel	uživatel	k1gMnSc1
má	mít	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
nebo	nebo	k8xC
více	hodně	k6eAd2
peněženek	peněženka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
slouží	sloužit	k5eAaImIp3nP
jako	jako	k9
adresy	adresa	k1gFnPc1
pro	pro	k7c4
platby	platba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
si	se	k3xPyFc3
také	také	k9
udržují	udržovat	k5eAaImIp3nP
distribuovanou	distribuovaný	k2eAgFnSc4d1
databázi	databáze	k1gFnSc4
všech	všecek	k3xTgFnPc2
proběhlých	proběhlý	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
v	v	k7c6
síti	síť	k1gFnSc6
–	–	k?
tzv.	tzv.	kA
blockchain	blockchain	k1gInSc1
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
plnohodnotného	plnohodnotný	k2eAgMnSc2d1
klienta	klient	k1gMnSc2
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
na	na	k7c6
databázi	databáze	k1gFnSc6
ptát	ptát	k5eAaImF
jiného	jiný	k2eAgInSc2d1
důvěryhodného	důvěryhodný	k2eAgInSc2d1
uzlu	uzel	k1gInSc2
<g/>
,	,	kIx,
popř.	popř.	kA
si	se	k3xPyFc3
udržovat	udržovat	k5eAaImF
jen	jen	k9
část	část	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
každý	každý	k3xTgInSc1
uzel	uzel	k1gInSc1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
mince	mince	k1gFnSc1
<g/>
/	/	kIx~
<g/>
část	část	k1gFnSc1
v	v	k7c6
síti	síť	k1gFnSc6
patří	patřit	k5eAaImIp3nS
které	který	k3yQgFnSc3,k3yRgFnSc3,k3yIgFnSc3
peněžence	peněženka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgNnSc1
peněžence	peněženka	k1gFnSc3
náleží	náležet	k5eAaImIp3nP
soukromý	soukromý	k2eAgInSc4d1
a	a	k8xC
veřejný	veřejný	k2eAgInSc4d1
klíč	klíč	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
chce	chtít	k5eAaImIp3nS
uživatel	uživatel	k1gMnSc1
poslat	poslat	k5eAaPmF
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
vezme	vzít	k5eAaPmIp3nS
patřičný	patřičný	k2eAgInSc4d1
obnos	obnos	k1gInSc4
svých	svůj	k3xOyFgFnPc2
mincí	mince	k1gFnPc2
+	+	kIx~
případný	případný	k2eAgInSc4d1
poplatek	poplatek	k1gInSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
dále	daleko	k6eAd2
<g/>
)	)	kIx)
a	a	k8xC
vytvoří	vytvořit	k5eAaPmIp3nS
transakci	transakce	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
podepíše	podepsat	k5eAaPmIp3nS
soukromým	soukromý	k2eAgInSc7d1
klíčem	klíč	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
informaci	informace	k1gFnSc4
rozešle	rozeslat	k5eAaPmIp3nS
všem	všecek	k3xTgInPc3
uzlům	uzel	k1gInPc3
(	(	kIx(
<g/>
uživatelům	uživatel	k1gMnPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yQgFnPc3,k3yRgFnPc3,k3yIgFnPc3
je	být	k5eAaImIp3nS
připojen	připojit	k5eAaPmNgInS
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
to	ten	k3xDgNnSc4
rozešlou	rozeslat	k5eAaPmIp3nP
dalším	další	k2eAgMnSc6d1
apod.	apod.	kA
do	do	k7c2
celé	celý	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
příjemci	příjemce	k1gMnSc3
tak	tak	k8xS,k8xC
informace	informace	k1gFnPc4
o	o	k7c6
platbě	platba	k1gFnSc6
probublá	probublat	k5eAaPmIp3nS
téměř	téměř	k6eAd1
ihned	ihned	k6eAd1
(	(	kIx(
<g/>
v	v	k7c6
řádu	řád	k1gInSc6
sekund	sekunda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
transakce	transakce	k1gFnSc1
však	však	k9
ještě	ještě	k6eAd1
není	být	k5eNaImIp3nS
tzv.	tzv.	kA
potvrzena	potvrdit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
těžaři	těžař	k1gMnPc1
potvrzují	potvrzovat	k5eAaImIp3nP
transakce	transakce	k1gFnPc4
v	v	k7c6
síti	síť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžař	těžař	k1gMnSc1
seskupí	seskupit	k5eAaPmIp3nS
transakce	transakce	k1gFnPc4
čekající	čekající	k2eAgFnPc4d1
na	na	k7c4
potvrzení	potvrzení	k1gNnPc4
<g/>
,	,	kIx,
přidá	přidat	k5eAaPmIp3nS
k	k	k7c3
nim	on	k3xPp3gFnPc3
odkaz	odkaz	k1gInSc1
na	na	k7c4
předchozí	předchozí	k2eAgInSc4d1
potvrzený	potvrzený	k2eAgInSc4d1
blok	blok	k1gInSc4
transakcí	transakce	k1gFnPc2
a	a	k8xC
údaj	údaj	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
kryptografická	kryptografický	k2eAgFnSc1d1
nonce	nonka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
najít	najít	k5eAaPmF
takovou	takový	k3xDgFnSc4
nonci	nonce	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
hash	hash	k1gInSc1
SHA-2	SHA-2	k1gFnSc2
nového	nový	k2eAgInSc2d1
bloku	blok	k1gInSc2
vešel	vejít	k5eAaPmAgInS
pod	pod	k7c7
sítí	síť	k1gFnSc7
stanovený	stanovený	k2eAgInSc1d1
limit	limit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Limit	limit	k1gInSc1
je	být	k5eAaImIp3nS
nastaven	nastavit	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
v	v	k7c6
celé	celý	k2eAgFnSc6d1
síti	síť	k1gFnSc6
dařilo	dařit	k5eAaImAgNnS
průměrně	průměrně	k6eAd1
jednou	jeden	k4xCgFnSc7
za	za	k7c4
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
nalezení	nalezení	k1gNnPc4
vhodné	vhodný	k2eAgFnPc4d1
nonce	nonce	k1gFnPc4
je	být	k5eAaImIp3nS
obtížné	obtížný	k2eAgNnSc1d1
a	a	k8xC
to	ten	k3xDgNnSc1
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
<g/>
,	,	kIx,
čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
výkonnější	výkonný	k2eAgFnSc1d2
celá	celý	k2eAgFnSc1d1
síť	síť	k1gFnSc1
je	být	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžař	těžař	k1gMnSc1
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
se	se	k3xPyFc4
nalezení	nalezení	k1gNnSc3
nonce	nonec	k1gInSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
vytvoření	vytvoření	k1gNnSc1
a	a	k8xC
potvrzení	potvrzení	k1gNnSc1
nového	nový	k2eAgInSc2d1
bloku	blok	k1gInSc2
transakcí	transakce	k1gFnPc2
podaří	podařit	k5eAaPmIp3nS
<g/>
,	,	kIx,
si	se	k3xPyFc3
ponechá	ponechat	k5eAaPmIp3nS
veškeré	veškerý	k3xTgInPc4
poplatky	poplatek	k1gInPc4
ze	z	k7c2
zahrnutých	zahrnutý	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
a	a	k8xC
odměnu	odměna	k1gFnSc4
za	za	k7c4
potvrzení	potvrzení	k1gNnSc4
bloku	blok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odměna	odměna	k1gFnSc1
za	za	k7c4
potvrzení	potvrzení	k1gNnSc4
bloku	blok	k1gInSc2
je	být	k5eAaImIp3nS
momentálně	momentálně	k6eAd1
6,25	6,25	k4
BTC	BTC	kA
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
první	první	k4xOgNnPc4
půlení	půlení	k1gNnPc4
z	z	k7c2
50	#num#	k4
na	na	k7c4
25	#num#	k4
se	se	k3xPyFc4
odehrálo	odehrát	k5eAaPmAgNnS
28	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
druhé	druhý	k4xOgFnSc2
z	z	k7c2
25	#num#	k4
BTC	BTC	kA
na	na	k7c4
12,5	12,5	k4
BTC	BTC	kA
9	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
a	a	k8xC
poslední	poslední	k2eAgMnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
z	z	k7c2
12,5	12,5	k4
na	na	k7c4
současných	současný	k2eAgInPc2d1
6,25	6,25	k4
<g/>
)	)	kIx)
a	a	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
a	a	k8xC
předem	předem	k6eAd1
stanovený	stanovený	k2eAgInSc1d1
způsob	způsob	k1gInSc1
emise	emise	k1gFnSc2
nových	nový	k2eAgInPc2d1
bitcoinů	bitcoin	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odměna	odměna	k1gFnSc1
se	s	k7c7
však	však	k9
každých	každý	k3xTgInPc2
210	#num#	k4
000	#num#	k4
bloků	blok	k1gInPc2
(	(	kIx(
<g/>
tj.	tj.	kA
každé	každý	k3xTgFnSc6
4	#num#	k4
roky	rok	k1gInPc4
<g/>
)	)	kIx)
snižuje	snižovat	k5eAaImIp3nS
na	na	k7c4
polovinu	polovina	k1gFnSc4
a	a	k8xC
růst	růst	k1gInSc4
množství	množství	k1gNnSc2
peněz	peníze	k1gInPc2
zpomaluje	zpomalovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžař	těžař	k1gMnSc1
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
vybrat	vybrat	k5eAaPmF
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
transakce	transakce	k1gFnPc1
do	do	k7c2
nového	nový	k2eAgInSc2d1
bloku	blok	k1gInSc2
zahrne	zahrnout	k5eAaPmIp3nS
a	a	k8xC
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
ne	ne	k9
(	(	kIx(
<g/>
podle	podle	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
poplatku	poplatek	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
zmínit	zmínit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
protože	protože	k8xS
je	být	k5eAaImIp3nS
síť	síť	k1gFnSc1
anonymní	anonymní	k2eAgFnSc1d1
<g/>
,	,	kIx,
těžaři	těžař	k1gMnPc1
nevědí	vědět	k5eNaImIp3nP
nic	nic	k6eAd1
o	o	k7c6
odesílatelích	odesílatel	k1gMnPc6
ani	ani	k8xC
příjemcích	příjemce	k1gMnPc6
a	a	k8xC
jediným	jediný	k2eAgNnSc7d1
smysluplným	smysluplný	k2eAgNnSc7d1
kritériem	kritérion	k1gNnSc7
pro	pro	k7c4
zahrnutí	zahrnutí	k1gNnSc4
nebo	nebo	k8xC
nezahrnutí	nezahrnutí	k1gNnSc4
transakce	transakce	k1gFnSc2
do	do	k7c2
bloku	blok	k1gInSc2
je	být	k5eAaImIp3nS
právě	právě	k9
zvolený	zvolený	k2eAgInSc4d1
poplatek	poplatek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
těžař	těžař	k1gMnSc1
první	první	k4xOgFnSc6
nalezne	naleznout	k5eAaPmIp3nS,k5eAaBmIp3nS
vhodnou	vhodný	k2eAgFnSc4d1
nonci	nonce	k1gFnSc4
(	(	kIx(
<g/>
a	a	k8xC
rozhoduje	rozhodovat	k5eAaImIp3nS
o	o	k7c4
zařazení	zařazení	k1gNnSc4
transakcí	transakce	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozhoduje	rozhodovat	k5eAaImIp3nS
náhoda	náhoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transakční	transakční	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
a	a	k8xC
odměna	odměna	k1gFnSc1
za	za	k7c4
potvrzení	potvrzení	k1gNnSc4
bloku	blok	k1gInSc2
jsou	být	k5eAaImIp3nP
ekonomickou	ekonomický	k2eAgFnSc7d1
motivací	motivace	k1gFnSc7
činnosti	činnost	k1gFnSc2
těžařů	těžař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snižováním	snižování	k1gNnSc7
odměny	odměna	k1gFnSc2
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
stále	stále	k6eAd1
většímu	veliký	k2eAgNnSc3d2
vyžadování	vyžadování	k1gNnSc3
transakčních	transakční	k2eAgInPc2d1
poplatků	poplatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nikdo	nikdo	k3yNnSc1
jiný	jiný	k2eAgMnSc1d1
kromě	kromě	k7c2
uživatelů	uživatel	k1gMnPc2
a	a	k8xC
těžařů	těžař	k1gMnPc2
v	v	k7c6
síti	síť	k1gFnSc6
nefiguruje	figurovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Potvrzování	potvrzování	k1gNnSc1
transakcí	transakce	k1gFnPc2
</s>
<s>
Těžaři	těžař	k1gMnPc1
řeší	řešit	k5eAaImIp3nP
umělý	umělý	k2eAgInSc4d1
problém	problém	k1gInSc4
nalezení	nalezení	k1gNnSc2
kryptografické	kryptografický	k2eAgFnSc2d1
nonce	nonce	k1gFnSc2
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
potvrzení	potvrzení	k1gNnSc1
nového	nový	k2eAgInSc2d1
bloku	blok	k1gInSc2
bylo	být	k5eAaImAgNnS
velice	velice	k6eAd1
složité	složitý	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
i	i	k9
velice	velice	k6eAd1
obtížně	obtížně	k6eAd1
padělatelné	padělatelný	k2eAgInPc1d1
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
ale	ale	k8xC
snadno	snadno	k6eAd1
ověřitelné	ověřitelný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nP
chtěl	chtít	k5eAaImAgMnS
útočník	útočník	k1gMnSc1
změnit	změnit	k5eAaPmF
platební	platební	k2eAgFnSc4d1
historii	historie	k1gFnSc4
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
by	by	kYmCp3nS
mít	mít	k5eAaImF
k	k	k7c3
dispozici	dispozice	k1gFnSc3
výpočetní	výpočetní	k2eAgInSc4d1
výkon	výkon	k1gInSc4
větší	veliký	k2eAgInSc4d2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
výpočetní	výpočetní	k2eAgInSc4d1
výkon	výkon	k1gInSc4
celého	celý	k2eAgInSc2d1
zbytku	zbytek	k1gInSc2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomuto	tento	k3xDgInSc3
teoretickému	teoretický	k2eAgInSc3d1
útoku	útok	k1gInSc3
se	se	k3xPyFc4
také	také	k9
říká	říkat	k5eAaImIp3nS
51	#num#	k4
<g/>
%	%	kIx~
útok	útok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoin	Bitcoin	k1gInSc1
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
takové	takový	k3xDgNnSc1
množství	množství	k1gNnSc1
výkonu	výkon	k1gInSc2
žádná	žádný	k3yNgFnSc1
jedna	jeden	k4xCgFnSc1
entita	entita	k1gFnSc1
nemá	mít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
ale	ale	k9
někomu	někdo	k3yInSc3
povedlo	povést	k5eAaPmAgNnS
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgInS
by	by	kYmCp3nS
měnit	měnit	k5eAaImF
pouze	pouze	k6eAd1
své	svůj	k3xOyFgFnPc4
vlastní	vlastní	k2eAgFnPc4d1
transakce	transakce	k1gFnPc4
a	a	k8xC
zamezit	zamezit	k5eAaPmF
potvrzování	potvrzování	k1gNnSc4
ostatních	ostatní	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
(	(	kIx(
<g/>
tedy	tedy	k9
nemohl	moct	k5eNaImAgMnS
by	by	kYmCp3nS
například	například	k6eAd1
převádět	převádět	k5eAaImF
cizí	cizí	k2eAgInPc4d1
peníze	peníz	k1gInPc4
k	k	k7c3
sobě	se	k3xPyFc3
ani	ani	k8xC
stávající	stávající	k2eAgFnSc3d1
databázi	databáze	k1gFnSc3
nějak	nějak	k6eAd1
ničit	ničit	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
51	#num#	k4
<g/>
%	%	kIx~
útok	útok	k1gInSc1
je	být	k5eAaImIp3nS
nepravděpodobný	pravděpodobný	k2eNgInSc1d1
<g/>
,	,	kIx,
např.	např.	kA
v	v	k7c6
květnu	květen	k1gInSc6
2013	#num#	k4
byla	být	k5eAaImAgFnS
síť	síť	k1gFnSc1
těžařů	těžař	k1gMnPc2
více	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
<g/>
×	×	k?
výkonnější	výkonný	k2eAgInSc1d2
než	než	k8xS
nejrychlejší	rychlý	k2eAgInSc1d3
superpočítač	superpočítač	k1gInSc1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získat	získat	k5eAaPmF
nadpoloviční	nadpoloviční	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
výpočetního	výpočetní	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
v	v	k7c6
celé	celý	k2eAgFnSc6d1
síti	síť	k1gFnSc6
by	by	kYmCp3nS
navíc	navíc	k6eAd1
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
útočníky	útočník	k1gMnPc4
velmi	velmi	k6eAd1
nákladné	nákladný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
kdyby	kdyby	kYmCp3nP
byli	být	k5eAaImAgMnP
ochotni	ochoten	k2eAgMnPc1d1
investovat	investovat	k5eAaBmF
a	a	k8xC
opravdu	opravdu	k6eAd1
síť	síť	k1gFnSc1
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
napadnout	napadnout	k5eAaPmF
<g/>
,	,	kIx,
klesla	klesnout	k5eAaPmAgFnS
by	by	kYmCp3nS
důvěra	důvěra	k1gFnSc1
v	v	k7c4
samotný	samotný	k2eAgInSc4d1
bitcoin	bitcoin	k1gInSc4
a	a	k8xC
s	s	k7c7
ním	on	k3xPp3gMnSc7
i	i	k9
jeho	jeho	k3xOp3gFnSc1
cena	cena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	dík	k1gInPc7
naprosté	naprostý	k2eAgFnSc2d1
transparentnosti	transparentnost	k1gFnSc2
by	by	kYmCp3nP
totiž	totiž	k9
všichni	všechen	k3xTgMnPc1
uživatelé	uživatel	k1gMnPc1
sítě	síť	k1gFnSc2
snadno	snadno	k6eAd1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
nejdelší	dlouhý	k2eAgFnSc1d3
větev	větev	k1gFnSc1
na	na	k7c4
blockchainu	blockchaina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
platnou	platný	k2eAgFnSc4d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
prodlužována	prodlužovat	k5eAaImNgFnS
pouze	pouze	k6eAd1
jedním	jeden	k4xCgInSc7
uzlem	uzel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	k9
51	#num#	k4
<g/>
%	%	kIx~
útok	útok	k1gInSc1
někdy	někdy	k6eAd1
proběhl	proběhnout	k5eAaPmAgInS
<g/>
,	,	kIx,
jednalo	jednat	k5eAaImAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
spíše	spíše	k9
o	o	k7c4
podkopání	podkopání	k1gNnSc4
důvěry	důvěra	k1gFnSc2
v	v	k7c4
samotnou	samotný	k2eAgFnSc4d1
Bitcoinovou	Bitcoinový	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
snaha	snaha	k1gFnSc1
o	o	k7c4
reálné	reálný	k2eAgNnSc4d1
odcizení	odcizení	k1gNnSc4
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Každý	každý	k3xTgInSc1
nový	nový	k2eAgInSc1d1
blok	blok	k1gInSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
předchozí	předchozí	k2eAgInSc4d1
blok	blok	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
potvrzuje	potvrzovat	k5eAaImIp3nS
i	i	k9
všechny	všechen	k3xTgFnPc4
předešlé	předešlý	k2eAgFnPc4d1
transakce	transakce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tudíž	tudíž	k8xC
každá	každý	k3xTgFnSc1
transakce	transakce	k1gFnSc1
je	být	k5eAaImIp3nS
potvrzena	potvrdit	k5eAaPmNgFnS
tolikrát	tolikrát	k6eAd1
<g/>
,	,	kIx,
kolik	kolik	k4yIc4,k4yRc4,k4yQc4
bloků	blok	k1gInPc2
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
od	od	k7c2
prvního	první	k4xOgNnSc2
zahrnutí	zahrnutí	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
toho	ten	k3xDgInSc2
prvního	první	k4xOgInSc2
bloku	blok	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
více	hodně	k6eAd2
potvrzení	potvrzení	k1gNnPc2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
obtížnější	obtížný	k2eAgFnSc1d2
je	být	k5eAaImIp3nS
padělatelnost	padělatelnost	k1gFnSc1
celého	celý	k2eAgInSc2d1
procesu	proces	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
tedy	tedy	k9
více	hodně	k6eAd2
může	moct	k5eAaImIp3nS
příjemce	příjemce	k1gMnSc1
věřit	věřit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
peníze	peníz	k1gInPc1
skutečně	skutečně	k6eAd1
přišly	přijít	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
nízké	nízký	k2eAgFnPc1d1
částky	částka	k1gFnPc1
(	(	kIx(
<g/>
pití	pití	k1gNnSc1
v	v	k7c6
automatu	automat	k1gInSc6
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
přijímají	přijímat	k5eAaImIp3nP
i	i	k9
bez	bez	k7c2
potvrzení	potvrzení	k1gNnSc2
<g/>
,	,	kIx,
běžné	běžný	k2eAgFnPc1d1
částky	částka	k1gFnPc1
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
potvrzení	potvrzení	k1gNnPc2
<g/>
,	,	kIx,
originální	originální	k2eAgMnSc1d1
klient	klient	k1gMnSc1
bere	brát	k5eAaImIp3nS
transakci	transakce	k1gFnSc4
za	za	k7c4
zcela	zcela	k6eAd1
důvěryhodnou	důvěryhodný	k2eAgFnSc4d1
při	při	k7c6
6	#num#	k4
potvrzeních	potvrzení	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obtížnost	obtížnost	k1gFnSc1
nalezení	nalezení	k1gNnSc2
bloku	blok	k1gInSc2
(	(	kIx(
<g/>
limit	limit	k1gInSc1
hashe	hash	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
každých	každý	k3xTgInPc2
14	#num#	k4
dní	den	k1gInPc2
upravována	upravovat	k5eAaImNgFnS
podle	podle	k7c2
aktuální	aktuální	k2eAgFnSc2d1
výpočetní	výpočetní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
všech	všecek	k3xTgMnPc2
těžařů	těžař	k1gMnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
nový	nový	k2eAgInSc1d1
blok	blok	k1gInSc1
uvolněn	uvolnit	k5eAaPmNgInS
průměrně	průměrně	k6eAd1
každých	každý	k3xTgFnPc2
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Měna	měna	k1gFnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
do	do	k7c2
sítě	síť	k1gFnSc2
uvolňována	uvolňovat	k5eAaImNgFnS
přibližně	přibližně	k6eAd1
stejnou	stejný	k2eAgFnSc4d1
rychlostí	rychlost	k1gFnSc7
<g/>
,	,	kIx,
nehledě	hledět	k5eNaImSgMnS
na	na	k7c4
počet	počet	k1gInSc4
těžařů	těžař	k1gMnPc2
či	či	k8xC
celkový	celkový	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
síti	síť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Více	hodně	k6eAd2
o	o	k7c6
technických	technický	k2eAgFnPc6d1
podrobnostech	podrobnost	k1gFnPc6
fungování	fungování	k1gNnSc2
česky	česky	k6eAd1
viz	vidět	k5eAaImRp2nS
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Peněženky	peněženka	k1gFnPc1
a	a	k8xC
placení	placení	k1gNnSc1
</s>
<s>
Každý	každý	k3xTgMnSc1
uživatel	uživatel	k1gMnSc1
má	mít	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
nebo	nebo	k8xC
více	hodně	k6eAd2
peněženek	peněženka	k1gFnPc2
<g/>
/	/	kIx~
<g/>
adres	adresa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
anonymitě	anonymita	k1gFnSc3
je	být	k5eAaImIp3nS
doporučeno	doporučen	k2eAgNnSc1d1
pro	pro	k7c4
každou	každý	k3xTgFnSc4
příchozí	příchozí	k1gFnSc4
platbu	platba	k1gFnSc4
vygenerovat	vygenerovat	k5eAaPmF
novou	nový	k2eAgFnSc4d1
adresu	adresa	k1gFnSc4
(	(	kIx(
<g/>
odesílatel	odesílatel	k1gMnSc1
pak	pak	k6eAd1
neuvidí	uvidět	k5eNaPmIp3nS
zůstatek	zůstatek	k1gInSc1
příjemce	příjemce	k1gMnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
bude	být	k5eAaImBp3nS
posílat	posílat	k5eAaImF
peníze	peníz	k1gInPc4
na	na	k7c4
prázdnou	prázdný	k2eAgFnSc4d1
adresu	adresa	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
generování	generování	k1gNnSc3
adres	adresa	k1gFnPc2
ale	ale	k8xC
dochází	docházet	k5eAaImIp3nS
i	i	k9
interně	interně	k6eAd1
<g/>
;	;	kIx,
při	při	k7c6
odesílání	odesílání	k1gNnSc6
nelze	lze	k6eNd1
utratit	utratit	k5eAaPmF
pouze	pouze	k6eAd1
část	část	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
automaticky	automaticky	k6eAd1
vytvoří	vytvořit	k5eAaPmIp3nS
další	další	k2eAgFnSc1d1
vlastní	vlastní	k2eAgFnSc1d1
adresa	adresa	k1gFnSc1
a	a	k8xC
platba	platba	k1gFnSc1
se	se	k3xPyFc4
rozdělí	rozdělit	k5eAaPmIp3nS
na	na	k7c4
dva	dva	k4xCgMnPc4
příjemce	příjemce	k1gMnPc4
<g/>
:	:	kIx,
na	na	k7c4
jednoho	jeden	k4xCgMnSc4
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
uživatel	uživatel	k1gMnSc1
zadal	zadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
zbytek	zbytek	k1gInSc1
částky	částka	k1gFnSc2
se	se	k3xPyFc4
pošle	poslat	k5eAaPmIp3nS
zpět	zpět	k6eAd1
uživateli	uživatel	k1gMnPc7
jako	jako	k9
tzv.	tzv.	kA
"	"	kIx"
<g/>
drobné	drobný	k2eAgFnSc2d1
<g/>
"	"	kIx"
na	na	k7c4
nově	nově	k6eAd1
vygenerovanou	vygenerovaný	k2eAgFnSc4d1
adresu	adresa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Tuhle	tuhle	k6eAd1
vlastnost	vlastnost	k1gFnSc4
však	však	k9
většina	většina	k1gFnSc1
klientů	klient	k1gMnPc2
před	před	k7c7
uživatelem	uživatel	k1gMnSc7
zcela	zcela	k6eAd1
skryje	skrýt	k5eAaPmIp3nS
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
vidět	vidět	k5eAaImF
pouze	pouze	k6eAd1
v	v	k7c6
blockchainu	blockchain	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vlastnictví	vlastnictví	k1gNnSc1
peněženky	peněženka	k1gFnSc2
není	být	k5eNaImIp3nS
v	v	k7c6
síti	síť	k1gFnSc6
nijak	nijak	k6eAd1
„	„	k?
<g/>
vidět	vidět	k5eAaImF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
penězi	peníze	k1gInPc7
v	v	k7c6
dotyčné	dotyčný	k2eAgFnSc6d1
peněžence	peněženka	k1gFnSc6
může	moct	k5eAaImIp3nS
nakládat	nakládat	k5eAaImF
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
vlastní	vlastnit	k5eAaImIp3nS
soukromý	soukromý	k2eAgInSc1d1
klíč	klíč	k1gInSc1
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
peněženku	peněženka	k1gFnSc4
vytvořil	vytvořit	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zůstatek	zůstatek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
uživateli	uživatel	k1gMnSc3
zobrazuje	zobrazovat	k5eAaImIp3nS
v	v	k7c6
klientu	klient	k1gMnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
pouze	pouze	k6eAd1
součtem	součet	k1gInSc7
peněz	peníze	k1gInPc2
na	na	k7c6
těch	ten	k3xDgFnPc6
peněženkách	peněženka	k1gFnPc6
<g/>
,	,	kIx,
od	od	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
má	mít	k5eAaImIp3nS
klient	klient	k1gMnSc1
soukromý	soukromý	k2eAgInSc4d1
klíč	klíč	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
příjemce	příjemce	k1gMnPc4
při	při	k7c6
příjmu	příjem	k1gInSc6
peněz	peníze	k1gInPc2
k	k	k7c3
síti	síť	k1gFnSc3
připojen	připojen	k2eAgInSc1d1
<g/>
;	;	kIx,
transakce	transakce	k1gFnSc1
proběhne	proběhnout	k5eAaPmIp3nS
stejně	stejně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klient	klient	k1gMnSc1
si	se	k3xPyFc3
při	při	k7c6
dalším	další	k2eAgNnSc6d1
spuštění	spuštění	k1gNnSc6
jen	jen	k9
zkontroluje	zkontrolovat	k5eAaPmIp3nS
pohyby	pohyb	k1gInPc4
na	na	k7c6
svých	svůj	k3xOyFgFnPc6
peněženkách	peněženka	k1gFnPc6
a	a	k8xC
podle	podle	k7c2
toho	ten	k3xDgNnSc2
upraví	upravit	k5eAaPmIp3nS
zůstatek	zůstatek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
zobrazí	zobrazit	k5eAaPmIp3nS
uživateli	uživatel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Adresa	adresa	k1gFnSc1
je	být	k5eAaImIp3nS
hash	hash	k1gInSc4
veřejného	veřejný	k2eAgInSc2d1
klíče	klíč	k1gInSc2
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dala	dát	k5eAaPmAgFnS
použít	použít	k5eAaPmF
(	(	kIx(
<g/>
poslat	poslat	k5eAaPmF
na	na	k7c4
ni	on	k3xPp3gFnSc4
peníze	peníz	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
nikde	nikde	k6eAd1
v	v	k7c6
síti	síť	k1gFnSc6
registrovaná	registrovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síť	síť	k1gFnSc1
ji	on	k3xPp3gFnSc4
poprvé	poprvé	k6eAd1
uvidí	uvidět	k5eAaPmIp3nS
až	až	k9
při	při	k7c6
vlastním	vlastní	k2eAgNnSc6d1
poslání	poslání	k1gNnSc6
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adresu	adresa	k1gFnSc4
tak	tak	k6eAd1
lze	lze	k6eAd1
vytvořit	vytvořit	k5eAaPmF
zcela	zcela	k6eAd1
offline	offlin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Ztracená	ztracený	k2eAgFnSc1d1
peněženka	peněženka	k1gFnSc1
</s>
<s>
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
zabránilo	zabránit	k5eAaPmAgNnS
překlepům	překlep	k1gInPc3
při	při	k7c6
opisování	opisování	k1gNnSc6
adresy	adresa	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
v	v	k7c6
sobě	se	k3xPyFc3
vestavěný	vestavěný	k2eAgInSc4d1
kontrolní	kontrolní	k2eAgInSc4d1
součet	součet	k1gInSc4
(	(	kIx(
<g/>
nedá	dát	k5eNaPmIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
splést	splést	k5eAaPmF
s	s	k7c7
pravděpodobností	pravděpodobnost	k1gFnSc7
1	#num#	k4
:	:	kIx,
4	#num#	k4
mld.	mld.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nP
se	se	k3xPyFc4
tak	tak	k6eAd1
přesto	přesto	k8xC
stalo	stát	k5eAaPmAgNnS
(	(	kIx(
<g/>
nebo	nebo	k8xC
někdo	někdo	k3yInSc1
schválně	schválně	k6eAd1
dopočítal	dopočítat	k5eAaPmAgInS
kontrolní	kontrolní	k2eAgInSc1d1
součet	součet	k1gInSc1
neexistující	existující	k2eNgInSc1d1
adrese	adresa	k1gFnSc3
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
adresa	adresa	k1gFnSc1
1	#num#	k4
<g/>
BitcoinEaterAddressDontSendf	BitcoinEaterAddressDontSendf	k1gInSc1
<g/>
59	#num#	k4
<g/>
kuE	kuE	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
peníze	peníz	k1gInPc4
na	na	k7c4
tuto	tento	k3xDgFnSc4
adresu	adresa	k1gFnSc4
budou	být	k5eAaImBp3nP
navždy	navždy	k6eAd1
ztraceny	ztratit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
uživatel	uživatel	k1gMnSc1
ztratí	ztratit	k5eAaPmIp3nS
peněženku	peněženka	k1gFnSc4
(	(	kIx(
<g/>
smaže	smazat	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
nemá	mít	k5eNaImIp3nS
zálohu	záloha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bitcoin	bitcoina	k1gFnPc2
síti	síť	k1gFnSc3
neexistuje	existovat	k5eNaImIp3nS
nic	nic	k3yNnSc1
jako	jako	k9
„	„	k?
<g/>
ztracená	ztracený	k2eAgFnSc1d1
peněženka	peněženka	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
nelze	lze	k6eNd1
nijak	nijak	k6eAd1
poznat	poznat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
ní	on	k3xPp3gFnSc6
nikdo	nikdo	k3yNnSc1
nevlastní	vlastnit	k5eNaImIp3nS
soukromý	soukromý	k2eAgInSc1d1
klíč	klíč	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
ostatní	ostatní	k2eAgMnPc4d1
účastníky	účastník	k1gMnPc4
sítě	síť	k1gFnSc2
je	být	k5eAaImIp3nS
taková	takový	k3xDgFnSc1
událost	událost	k1gFnSc1
prospěšná	prospěšný	k2eAgFnSc1d1
<g/>
;	;	kIx,
velice	velice	k6eAd1
mírně	mírně	k6eAd1
se	se	k3xPyFc4
zvýší	zvýšit	k5eAaPmIp3nS
hodnota	hodnota	k1gFnSc1
jejich	jejich	k3xOp3gInPc2
bitcoinů	bitcoin	k1gInPc2
(	(	kIx(
<g/>
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
deflaci	deflace	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analogií	analogie	k1gFnPc2
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
světě	svět	k1gInSc6
je	být	k5eAaImIp3nS
spálení	spálení	k1gNnSc1
bankovek	bankovka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
odhadovalo	odhadovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nenávratně	návratně	k6eNd1
ztraceno	ztratit	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
tři	tři	k4xCgInPc1
miliony	milion	k4xCgInPc1
Bitcoinů	Bitcoin	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
pětina	pětina	k1gFnSc1
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
odhadu	odhad	k1gInSc6
je	být	k5eAaImIp3nS
ale	ale	k9
zahrnut	zahrnout	k5eAaPmNgMnS
i	i	k8xC
milion	milion	k4xCgInSc4
bitcoinů	bitcoin	k1gInPc2
vytěžených	vytěžený	k2eAgInPc2d1
zakladatelem	zakladatel	k1gMnSc7
Satoshim	Satoshim	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Těžařská	těžařský	k2eAgNnPc1d1
uskupení	uskupení	k1gNnPc1
</s>
<s>
Protože	protože	k8xS
je	být	k5eAaImIp3nS
nalezení	nalezení	k1gNnSc4
bloku	blok	k1gInSc2
náhodný	náhodný	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
,	,	kIx,
pro	pro	k7c4
malé	malý	k2eAgMnPc4d1
těžaře	těžař	k1gMnPc4
je	být	k5eAaImIp3nS
výdělek	výdělek	k1gInSc1
velice	velice	k6eAd1
nepředvídatelný	předvídatelný	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžaři	těžař	k1gMnPc1
se	se	k3xPyFc4
proto	proto	k8xC
často	často	k6eAd1
sdružují	sdružovat	k5eAaImIp3nP
do	do	k7c2
tzv.	tzv.	kA
těžařských	těžařský	k2eAgNnPc2d1
uskupení	uskupení	k1gNnPc2
(	(	kIx(
<g/>
mining	mining	k1gInSc1
pool	pool	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
zkoušejí	zkoušet	k5eAaImIp3nP
štěstí	štěstí	k1gNnSc4
společně	společně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
některý	některý	k3yIgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
nalezne	naleznout	k5eAaPmIp3nS,k5eAaBmIp3nS
blok	blok	k1gInSc1
<g/>
,	,	kIx,
odměna	odměna	k1gFnSc1
se	se	k3xPyFc4
rozdělí	rozdělit	k5eAaPmIp3nS
mezi	mezi	k7c4
všechny	všechen	k3xTgMnPc4
těžaře	těžař	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
předvídatelnost	předvídatelnost	k1gFnSc1
<g/>
;	;	kIx,
zatímco	zatímco	k8xS
těžař	těžař	k1gMnSc1
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
grafickou	grafický	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
by	by	kYmCp3nS
našel	najít	k5eAaPmAgInS
blok	blok	k1gInSc1
v	v	k7c6
průměru	průměr	k1gInSc6
až	až	k9
za	za	k7c4
5	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
by	by	kYmCp3nS
nezískal	získat	k5eNaPmAgInS
nic	nic	k6eAd1
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
květen	květen	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
uskupení	uskupení	k1gNnSc6
může	moct	k5eAaImIp3nS
dostávat	dostávat	k5eAaImF
poměrně	poměrně	k6eAd1
malou	malý	k2eAgFnSc4d1
část	část	k1gFnSc4
několikrát	několikrát	k6eAd1
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
obvyklé	obvyklý	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
správcům	správce	k1gMnPc3
uskupení	uskupení	k1gNnSc4
v	v	k7c6
řádu	řád	k1gInSc6
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Technické	technický	k2eAgNnSc1d1
řešení	řešení	k1gNnSc1
těžařského	těžařský	k2eAgNnSc2d1
uskupení	uskupení	k1gNnSc2
</s>
<s>
Na	na	k7c4
princip	princip	k1gInSc4
fungování	fungování	k1gNnSc2
těžařských	těžařský	k2eAgNnPc2d1
uskupení	uskupení	k1gNnPc2
přišel	přijít	k5eAaPmAgMnS
český	český	k2eAgMnSc1d1
programátor	programátor	k1gMnSc1
Marek	Marek	k1gMnSc1
Palatinus	Palatinus	k1gMnSc1
(	(	kIx(
<g/>
slush	slush	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
také	také	k9
dodnes	dodnes	k6eAd1
jedno	jeden	k4xCgNnSc4
z	z	k7c2
nejvýznamnějších	významný	k2eAgNnPc2d3
uskupení	uskupení	k1gNnPc2
provozuje	provozovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Funguje	fungovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
uskupení	uskupení	k1gNnSc1
vytvoří	vytvořit	k5eAaPmIp3nS
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc1d1
blok	blok	k1gInSc1
(	(	kIx(
<g/>
zahrne	zahrnout	k5eAaPmIp3nS
všechny	všechen	k3xTgFnPc4
transakce	transakce	k1gFnPc4
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jediné	jediný	k2eAgNnSc1d1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
kryptografická	kryptografický	k2eAgFnSc1d1
nonce	nonce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
spolu	spolu	k6eAd1
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
údaji	údaj	k1gInPc7
bude	být	k5eAaImBp3nS
dávat	dávat	k5eAaImF
správný	správný	k2eAgInSc4d1
hash	hash	k1gInSc4
(	(	kIx(
<g/>
menší	malý	k2eAgFnSc1d2
než	než	k8xS
hodnota	hodnota	k1gFnSc1
stanovená	stanovený	k2eAgFnSc1d1
sítí	síť	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uskupení	uskupení	k1gNnSc1
pak	pak	k6eAd1
každému	každý	k3xTgMnSc3
těžaři	těžař	k1gMnSc3
přidělí	přidělit	k5eAaPmIp3nS
rozsah	rozsah	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
má	mít	k5eAaImIp3nS
nonci	nonce	k1gMnSc3
hledat	hledat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžař	těžař	k1gMnSc1
hlásí	hlásit	k5eAaImIp3nS
uskupení	uskupení	k1gNnSc4
dílčí	dílčí	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
menší	malý	k2eAgInPc1d2
<g/>
,	,	kIx,
než	než	k8xS
hodnota	hodnota	k1gFnSc1
daná	daný	k2eAgFnSc1d1
uskupením	uskupení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnota	hodnota	k1gFnSc1
daná	daný	k2eAgFnSc1d1
uskupením	uskupení	k1gNnSc7
je	být	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
na	na	k7c4
nový	nový	k2eAgInSc4d1
blok	blok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
dílčích	dílčí	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
tak	tak	k6eAd1
nevede	vést	k5eNaImIp3nS
na	na	k7c4
nový	nový	k2eAgInSc4d1
blok	blok	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
uskupení	uskupení	k1gNnSc1
dostává	dostávat	k5eAaImIp3nS
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
těžař	těžař	k1gMnSc1
na	na	k7c6
problému	problém	k1gInSc6
pracuje	pracovat	k5eAaImIp3nS
a	a	k8xC
podle	podle	k7c2
počtu	počet	k1gInSc2
dílčích	dílčí	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
ho	on	k3xPp3gInSc4
i	i	k9
odměňuje	odměňovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Protože	protože	k8xS
pravidla	pravidlo	k1gNnPc4
bloku	blok	k1gInSc2
určuje	určovat	k5eAaImIp3nS
správce	správce	k1gMnSc1
uskupení	uskupení	k1gNnSc2
<g/>
,	,	kIx,
nikoli	nikoli	k9
těžaři	těžař	k1gMnPc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
správce	správce	k1gMnSc1
uskupení	uskupení	k1gNnSc2
poměrně	poměrně	k6eAd1
velký	velký	k2eAgInSc1d1
vliv	vliv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k6eAd1
důležité	důležitý	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
žádné	žádný	k3yNgNnSc1
uskupení	uskupení	k1gNnSc1
nemělo	mít	k5eNaImAgNnS
nadpoloviční	nadpoloviční	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
těžařů	těžař	k1gMnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
by	by	kYmCp3nS
správce	správce	k1gMnSc1
mohl	moct	k5eAaImAgMnS
rozhodovat	rozhodovat	k5eAaImF
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
transakce	transakce	k1gFnPc4
nezahrnout	zahrnout	k5eNaPmF
a	a	k8xC
síť	síť	k1gFnSc1
by	by	kYmCp3nS
tak	tak	k9
byla	být	k5eAaImAgFnS
částečně	částečně	k6eAd1
centralizována	centralizován	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Statistiky	statistika	k1gFnPc1
uskupení	uskupení	k1gNnSc2
viz	vidět	k5eAaImRp2nS
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Segregated	Segregated	k1gInSc1
Witness	Witness	k1gInSc1
(	(	kIx(
<g/>
Segwit	Segwit	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
SegWit	SegWit	k1gInSc1
je	být	k5eAaImIp3nS
upgrade	upgrade	k1gInSc4
bitcoin	bitcoina	k1gFnPc2
protokolu	protokol	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
aktivován	aktivovat	k5eAaBmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
představen	představit	k5eAaPmNgInS
na	na	k7c6
konferenci	konference	k1gFnSc6
Scaling	Scaling	k1gInSc1
Bitcoin	Bitcoin	k2eAgInSc1d1
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
Bitcoin	Bitcoin	k2eAgInSc4d1
Core	Core	k1gInSc4
vývojářem	vývojář	k1gMnSc7
Pietrem	Pietr	k1gMnSc7
Wuillem	Wuill	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
inovace	inovace	k1gFnSc1
odděluje	oddělovat	k5eAaImIp3nS
podpisová	podpisový	k2eAgFnSc1d1
(	(	kIx(
<g/>
signature	signatur	k1gMnSc5
<g/>
)	)	kIx)
data	datum	k1gNnPc4
z	z	k7c2
transakcí	transakce	k1gFnPc2
jako	jako	k8xC,k8xS
takových	takový	k3xDgMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
přináší	přinášet	k5eAaImIp3nS
několik	několik	k4yIc4
výhod	výhoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
ně	on	k3xPp3gNnSc4
patří	patřit	k5eAaImIp3nS
efektivní	efektivní	k2eAgNnSc4d1
zvětšení	zvětšení	k1gNnSc4
kapacity	kapacita	k1gFnSc2
bloku	blok	k1gInSc2
<g/>
,	,	kIx,
zlevnění	zlevnění	k1gNnSc4
transakcí	transakce	k1gFnPc2
nebo	nebo	k8xC
oprava	oprava	k1gFnSc1
chyby	chyba	k1gFnSc2
nazývané	nazývaný	k2eAgInPc1d1
transaction	transaction	k1gInSc1
malleability	malleabilita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Implementace	implementace	k1gFnSc1
technologie	technologie	k1gFnSc1
SegWit	SegWit	k1gInSc4
umožnila	umožnit	k5eAaPmAgFnS
vývoj	vývoj	k1gInSc4
tzv.	tzv.	kA
second	second	k1gInSc1
layer	layer	k1gInSc1
scaling	scaling	k1gInSc4
řešení	řešení	k1gNnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
lightning	lightning	k1gInSc4
network	network	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Koncoví	koncový	k2eAgMnPc1d1
uživatelé	uživatel	k1gMnPc1
bitcoinu	bitcoin	k1gInSc2
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
vybrat	vybrat	k5eAaPmF
zda	zda	k8xS
ho	on	k3xPp3gNnSc4
používat	používat	k5eAaImF
nebo	nebo	k8xC
ne	ne	k9
výběrem	výběr	k1gInSc7
odvození	odvození	k1gNnSc2
bitcoinových	bitcoinův	k2eAgFnPc2d1
adres	adresa	k1gFnPc2
při	při	k7c6
generování	generování	k1gNnSc6
bitcoinové	bitcoinový	k2eAgFnSc2d1
peněženky	peněženka	k1gFnSc2
z	z	k7c2
privátního	privátní	k2eAgInSc2d1
klíče	klíč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoinové	Bitcoinový	k2eAgFnPc4d1
adresy	adresa	k1gFnPc4
podporující	podporující	k2eAgInSc4d1
SegWit	SegWit	k1gInSc4
začínají	začínat	k5eAaImIp3nP
buď	buď	k8xC
číslicí	číslice	k1gFnSc7
3	#num#	k4
nebo	nebo	k8xC
písmeny	písmeno	k1gNnPc7
bc.	bc.	k?
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
bitcoinu	bitcoinout	k5eAaPmIp1nS
</s>
<s>
Před	před	k7c7
1	#num#	k4
<g/>
.	.	kIx.
srpnem	srpen	k1gInSc7
2017	#num#	k4
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
hlasování	hlasování	k1gNnSc3
těžařů	těžař	k1gMnPc2
o	o	k7c6
budoucí	budoucí	k2eAgFnSc6d1
aktualizaci	aktualizace	k1gFnSc6
protokolu	protokol	k1gInSc2
bitcoinu	bitcoinout	k5eAaPmIp1nS
a	a	k8xC
jelikož	jelikož	k8xS
hlasování	hlasování	k1gNnSc1
nebylo	být	k5eNaImAgNnS
úplně	úplně	k6eAd1
jednotné	jednotný	k2eAgNnSc1d1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
se	se	k3xPyFc4
malá	malý	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
těžařů	těžař	k1gMnPc2
odtrhla	odtrhnout	k5eAaPmAgFnS
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
virtuální	virtuální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
nazývaná	nazývaný	k2eAgFnSc1d1
Bitcoin	Bitcoin	k1gInSc4
Cash	cash	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
její	její	k3xOp3gMnPc1
významní	významný	k2eAgMnPc1d1
podporovatelé	podporovatel	k1gMnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
investor	investor	k1gMnSc1
Roger	Roger	k1gMnSc1
Ver	Ver	k1gMnSc1
<g/>
,	,	kIx,
dokonce	dokonce	k9
označují	označovat	k5eAaImIp3nP
bitcoin	bitcoin	k1gInSc4
cash	cash	k1gFnSc2
za	za	k7c7
„	„	k?
<g/>
ten	ten	k3xDgInSc4
pravý	pravý	k2eAgInSc4d1
bitcoin	bitcoin	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Argumentují	argumentovat	k5eAaImIp3nP
hlavně	hlavně	k9
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
bitcoinem	bitcoino	k1gNnSc7
nižšími	nízký	k2eAgInPc7d2
transakčními	transakční	k2eAgInPc7d1
poplatky	poplatek	k1gInPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
mají	mít	k5eAaImIp3nP
napomoci	napomoct	k5eAaPmF
většímu	veliký	k2eAgNnSc3d2
rozšíření	rozšíření	k1gNnSc3
bitcoin	bitcoin	k1gMnSc1
cash	cash	k1gFnSc4
mezi	mezi	k7c7
uživateli	uživatel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
sedm	sedm	k4xCc4
měsíců	měsíc	k1gInPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
únoru	únor	k1gInSc6
2018	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
přitom	přitom	k6eAd1
výše	výše	k1gFnSc1
poplatků	poplatek	k1gInPc2
za	za	k7c4
transakce	transakce	k1gFnPc4
bitcoinem	bitcoin	k1gInSc7
a	a	k8xC
bitcoin	bitcoin	k1gInSc1
cash	cash	k1gFnSc2
srovnatelná	srovnatelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Později	pozdě	k6eAd2
se	se	k3xPyFc4
od	od	k7c2
bitcoinu	bitcoin	k1gInSc2
oddělila	oddělit	k5eAaPmAgFnS
měna	měna	k1gFnSc1
SegWit	SegWit	k1gInSc1
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
bylo	být	k5eAaImAgNnS
očekáváno	očekávat	k5eAaImNgNnS
oddělení	oddělení	k1gNnSc1
(	(	kIx(
<g/>
hardfork	hardfork	k1gInSc1
<g/>
)	)	kIx)
třetí	třetí	k4xOgFnSc2
měny	měna	k1gFnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
tomuto	tento	k3xDgNnSc3
dělení	dělení	k1gNnSc3
nedojde	dojít	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hodnota	hodnota	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
</s>
<s>
2017	#num#	k4
<g/>
:	:	kIx,
<g/>
Bitcoin	Bitcoin	k1gInSc1
dosáhl	dosáhnout	k5eAaPmAgInS
10000	#num#	k4
<g/>
USD	USD	kA
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitcoin	Bitcoin	k1gInSc1
je	být	k5eAaImIp3nS
samostatná	samostatný	k2eAgFnSc1d1
měna	měna	k1gFnSc1
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
nezávislá	závislý	k2eNgFnSc1d1
na	na	k7c6
tradičních	tradiční	k2eAgFnPc6d1
měnách	měna	k1gFnPc6
jako	jako	k8xC,k8xS
koruna	koruna	k1gFnSc1
<g/>
,	,	kIx,
euro	euro	k1gNnSc1
apod.	apod.	kA
Hodnota	hodnota	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
–	–	k?
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
většiny	většina	k1gFnSc2
ostatních	ostatní	k2eAgFnPc2d1
měn	měna	k1gFnPc2
–	–	k?
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
poptávky	poptávka	k1gFnSc2
a	a	k8xC
nabídky	nabídka	k1gFnSc2
na	na	k7c6
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
tedy	tedy	k9
dána	dát	k5eAaPmNgFnS
ekonomickou	ekonomický	k2eAgFnSc7d1
rovnováhou	rovnováha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoin	Bitcoin	k1gMnSc1
není	být	k5eNaImIp3nS
kryt	krýt	k5eAaImNgMnS
zlatem	zlato	k1gNnSc7
ani	ani	k8xC
jinými	jiný	k2eAgFnPc7d1
komoditami	komodita	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
podobně	podobně	k6eAd1
jako	jako	k9
u	u	k7c2
jiných	jiný	k2eAgFnPc2d1
běžných	běžný	k2eAgFnPc2d1
měn	měna	k1gFnPc2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
hodnota	hodnota	k1gFnSc1
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
důvěře	důvěra	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
ním	on	k3xPp3gNnSc7
bude	být	k5eAaImBp3nS
možno	možno	k6eAd1
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
zaplatit	zaplatit	k5eAaPmF
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
autoři	autor	k1gMnPc1
též	též	k9
poukazují	poukazovat	k5eAaImIp3nP
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
už	už	k9
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
existuje	existovat	k5eAaImIp3nS
měna	měna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
nezávislá	závislý	k2eNgFnSc1d1
na	na	k7c4
rozhodnutí	rozhodnutí	k1gNnSc4
centrálních	centrální	k2eAgFnPc2d1
autorit	autorita	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
sama	sám	k3xTgFnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
hodnotná	hodnotný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k9
jedno	jeden	k4xCgNnSc1
z	z	k7c2
rizik	riziko	k1gNnPc2
budoucí	budoucí	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
Bitcoinu	Bitcoin	k1gInSc2
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgInS
klesající	klesající	k2eAgInSc1d1
podíl	podíl	k1gInSc1
Bitcoinu	Bitcoin	k1gInSc2
na	na	k7c6
celkové	celkový	k2eAgFnSc6d1
tržní	tržní	k2eAgFnSc6d1
kapitalizaci	kapitalizace	k1gFnSc6
alternativních	alternativní	k2eAgFnPc2d1
měn	měna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
do	do	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
2017	#num#	k4
klesl	klesnout	k5eAaPmAgInS
podíl	podíl	k1gInSc1
tržní	tržní	k2eAgFnSc2d1
kapitalizace	kapitalizace	k1gFnSc2
Bitcoinu	Bitcoin	k1gInSc2
oproti	oproti	k7c3
ostatním	ostatní	k2eAgFnPc3d1
kryptoměnám	kryptoměna	k1gFnPc3
ze	z	k7c2
zhruba	zhruba	k6eAd1
95	#num#	k4
%	%	kIx~
na	na	k7c6
méně	málo	k6eAd2
než	než	k8xS
40	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hodnota	hodnota	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
není	být	k5eNaImIp3nS
určena	určit	k5eAaPmNgFnS
počtem	počet	k1gInSc7
či	či	k8xC
výkonem	výkon	k1gInSc7
těžařů	těžař	k1gMnPc2
<g/>
:	:	kIx,
při	při	k7c6
zvýšení	zvýšení	k1gNnSc6
hodnoty	hodnota	k1gFnSc2
se	se	k3xPyFc4
zvedne	zvednout	k5eAaPmIp3nS
výkon	výkon	k1gInSc1
těžařů	těžař	k1gMnPc2
<g/>
,	,	kIx,
ne	ne	k9
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžaři	těžař	k1gMnPc1
neurčují	určovat	k5eNaImIp3nP
hodnotu	hodnota	k1gFnSc4
Bitcoinu	Bitcoina	k1gFnSc4
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
dána	dát	k5eAaPmNgFnS
pouze	pouze	k6eAd1
poměrem	poměr	k1gInSc7
mezi	mezi	k7c7
nabídkou	nabídka	k1gFnSc7
a	a	k8xC
poptávkou	poptávka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Též	též	k6eAd1
bývá	bývat	k5eAaImIp3nS
chybně	chybně	k6eAd1
uváděno	uvádět	k5eAaImNgNnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
kryta	krýt	k5eAaImNgFnS
vzácností	vzácnost	k1gFnSc7
<g/>
,	,	kIx,
nepadělatelností	nepadělatelnost	k1gFnSc7
apod.	apod.	kA
Tyto	tento	k3xDgInPc1
vlivy	vliv	k1gInPc1
nemají	mít	k5eNaImIp3nP
přímou	přímý	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
na	na	k7c4
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
důvěru	důvěra	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
vliv	vliv	k1gInSc4
na	na	k7c4
nabídku	nabídka	k1gFnSc4
a	a	k8xC
poptávku	poptávka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hodnotě	hodnota	k1gFnSc6
se	se	k3xPyFc4
tedy	tedy	k9
podílejí	podílet	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
nepřímo	přímo	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
doposud	doposud	k6eAd1
největší	veliký	k2eAgFnPc4d3
cenové	cenový	k2eAgFnPc4d1
bubliny	bublina	k1gFnPc4
Bitcoinu	Bitcoina	k1gFnSc4
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
překročila	překročit	k5eAaPmAgFnS
hodnota	hodnota	k1gFnSc1
této	tento	k3xDgFnSc2
měny	měna	k1gFnSc2
10	#num#	k4
000	#num#	k4
USD	USD	kA
(	(	kIx(
<g/>
270	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
)	)	kIx)
za	za	k7c4
1	#num#	k4
BTC	BTC	kA
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
dny	den	k1gInPc4
poté	poté	k6eAd1
oznámila	oznámit	k5eAaPmAgFnS
hlavní	hlavní	k2eAgFnSc1d1
americká	americký	k2eAgFnSc1d1
Bitcoinová	Bitcoinový	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
Coinbase	Coinbasa	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
počet	počet	k1gInSc1
otevřených	otevřený	k2eAgInPc2d1
účtů	účet	k1gInPc2
překročil	překročit	k5eAaPmAgInS
13,3	13,3	k4
milionu	milion	k4xCgInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Cena	cena	k1gFnSc1
tehdy	tehdy	k6eAd1
nadále	nadále	k6eAd1
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
růstu	růst	k1gInSc6
až	až	k9
k	k	k7c3
20	#num#	k4
000	#num#	k4
USD	USD	kA
koncem	koncem	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hodnota	hodnota	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
v	v	k7c6
historii	historie	k1gFnSc6
</s>
<s>
Vývoj	vývoj	k1gInSc1
kurzu	kurz	k1gInSc2
k	k	k7c3
americkému	americký	k2eAgInSc3d1
dolaru	dolar	k1gInSc3
podle	podle	k7c2
největší	veliký	k2eAgFnSc2d3
burzy	burza	k1gFnSc2
MtGox	MtGox	k1gInSc1
</s>
<s>
Kurz	kurz	k1gInSc1
Bitcoinu	Bitcoin	k1gInSc2
se	se	k3xPyFc4
občas	občas	k6eAd1
vyznačuje	vyznačovat	k5eAaImIp3nS
vysokou	vysoký	k2eAgFnSc7d1
volatilitou	volatilita	k1gFnSc7
<g/>
,	,	kIx,
tedy	tedy	k9
prudkým	prudký	k2eAgNnSc7d1
kolísáním	kolísání	k1gNnSc7
ceny	cena	k1gFnSc2
v	v	k7c6
krátkém	krátký	k2eAgInSc6d1
časovém	časový	k2eAgInSc6d1
úseku	úsek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
střednědobého	střednědobý	k2eAgNnSc2d1
a	a	k8xC
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
však	však	k9
vykazuje	vykazovat	k5eAaImIp3nS
neustálý	neustálý	k2eAgInSc4d1
nárůst	nárůst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bitcoin	Bitcoin	k1gInSc1
začínal	začínat	k5eAaImAgInS
jako	jako	k9
čistě	čistě	k6eAd1
akademický	akademický	k2eAgInSc4d1
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ho	on	k3xPp3gMnSc4
používali	používat	k5eAaImAgMnP
především	především	k9
odborníci	odborník	k1gMnPc1
a	a	k8xC
zájemci	zájemce	k1gMnPc1
o	o	k7c4
technologii	technologie	k1gFnSc4
samotnou	samotný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technologie	technologie	k1gFnSc1
však	však	k9
zaujala	zaujmout	k5eAaPmAgFnS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
hodnota	hodnota	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
po	po	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
stoupala	stoupat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
přivedlo	přivést	k5eAaPmAgNnS
i	i	k9
zájemce	zájemce	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
investují	investovat	k5eAaBmIp3nP
čistě	čistě	k6eAd1
ze	z	k7c2
spekulativních	spekulativní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnota	hodnota	k1gFnSc1
bitcoinu	bitcoin	k1gInSc2
tak	tak	k6eAd1
zažila	zažít	k5eAaPmAgFnS
za	za	k7c4
svoji	svůj	k3xOyFgFnSc4
krátkou	krátký	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
prudký	prudký	k2eAgInSc4d1
růst	růst	k1gInSc4
<g/>
,	,	kIx,
vrchol	vrchol	k1gInSc4
investiční	investiční	k2eAgFnSc2d1
bubliny	bublina	k1gFnSc2
i	i	k8xC
částečný	částečný	k2eAgInSc4d1
pád	pád	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Obchodování	obchodování	k1gNnSc1
s	s	k7c7
Bitcoinem	Bitcoin	k1gInSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
tří	tři	k4xCgNnPc2
období	období	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
do	do	k7c2
června	červen	k1gInSc2
roku	rok	k1gInSc2
2013	#num#	k4
se	se	k3xPyFc4
Bitcoinem	Bitcoin	k1gInSc7
zabývali	zabývat	k5eAaImAgMnP
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
IT	IT	kA
specialisté	specialista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
si	se	k3xPyFc3
situace	situace	k1gFnSc2
v	v	k7c6
kryptoměnách	kryptoměna	k1gFnPc6
ve	v	k7c6
větším	veliký	k2eAgNnSc6d2
měřítku	měřítko	k1gNnSc6
všimli	všimnout	k5eAaPmAgMnP
profesionální	profesionální	k2eAgMnPc1d1
investoři	investor	k1gMnPc1
<g/>
,	,	kIx,
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
platforem	platforma	k1gFnPc2
zařadili	zařadit	k5eAaPmAgMnP
Bitcoin	Bitcoin	k1gMnSc1
obchodníci	obchodník	k1gMnPc1
s	s	k7c7
deriváty	derivát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2017	#num#	k4
se	se	k3xPyFc4
k	k	k7c3
obchodování	obchodování	k1gNnSc3
s	s	k7c7
bitcoinem	bitcoin	k1gMnSc7
přidala	přidat	k5eAaPmAgFnS
i	i	k9
širší	široký	k2eAgFnSc1d2
veřejnost	veřejnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vzrůst	vzrůst	k1gInSc1
a	a	k8xC
pád	pád	k1gInSc1
hodnoty	hodnota	k1gFnSc2
bitcoinu	bitcoin	k1gInSc2
byl	být	k5eAaImAgInS
dán	dát	k5eAaPmNgInS
různými	různý	k2eAgInPc7d1
podněty	podnět	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
do	do	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
souvisela	souviset	k5eAaImAgFnS
cena	cena	k1gFnSc1
bitcoinu	bitcoina	k1gFnSc4
více	hodně	k6eAd2
s	s	k7c7
dostupností	dostupnost	k1gFnSc7
technologií	technologie	k1gFnSc7
a	a	k8xC
vytěženým	vytěžený	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
kryptoměny	kryptomět	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
byla	být	k5eAaImAgFnS
důležitá	důležitý	k2eAgFnSc1d1
vzrůstající	vzrůstající	k2eAgFnSc1d1
akceptace	akceptace	k1gFnSc1
bitcoinu	bitcoinout	k5eAaPmIp1nS
obchodníky	obchodník	k1gMnPc4
<g/>
,	,	kIx,
zprávy	zpráva	k1gFnPc4
o	o	k7c6
regulaci	regulace	k1gFnSc6
<g/>
,	,	kIx,
případně	případně	k6eAd1
zákazech	zákaz	k1gInPc6
kryptoměn	kryptoměn	k2eAgInSc1d1
a	a	k8xC
podvodech	podvod	k1gInPc6
nebo	nebo	k8xC
krachu	krach	k1gInSc6
bitcoinových	bitcoinův	k2eAgFnPc2d1
burz	burza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projevily	projevit	k5eAaPmAgInP
se	se	k3xPyFc4
také	také	k9
nepopulární	populární	k2eNgInPc1d1
kroky	krok	k1gInPc1
tradičních	tradiční	k2eAgFnPc2d1
bank	banka	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
zdaňování	zdaňování	k1gNnSc1
bankovních	bankovní	k2eAgInPc2d1
vkladů	vklad	k1gInPc2
na	na	k7c6
Kypru	Kypr	k1gInSc6
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
má	mít	k5eAaImIp3nS
podstatný	podstatný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
důvěra	důvěra	k1gFnSc1
v	v	k7c4
další	další	k2eAgInSc4d1
růst	růst	k1gInSc4
a	a	k8xC
přísun	přísun	k1gInSc1
nových	nový	k2eAgMnPc2d1
kupujících	kupující	k1gMnPc2
<g/>
,	,	kIx,
bitcoinové	bitcoinový	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
také	také	k9
hlásí	hlásit	k5eAaImIp3nP
značný	značný	k2eAgInSc4d1
nárůst	nárůst	k1gInSc4
počtu	počet	k1gInSc2
nově	nově	k6eAd1
otevřených	otevřený	k2eAgInPc2d1
obchodních	obchodní	k2eAgInPc2d1
účtů	účet	k1gInPc2
v	v	k7c6
řádu	řád	k1gInSc6
jednotek	jednotka	k1gFnPc2
procent	procento	k1gNnPc2
za	za	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
24.5	24.5	k4
<g/>
.2017	.2017	k4
na	na	k7c4
25.5	25.5	k4
<g/>
.2017	.2017	k4
stoupla	stoupnout	k5eAaPmAgFnS
cena	cena	k1gFnSc1
o	o	k7c4
500	#num#	k4
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
+	+	kIx~
<g/>
22	#num#	k4
p.	p.	k?
b.	b.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
2	#num#	k4
měsíců	měsíc	k1gInPc2
do	do	k7c2
konce	konec	k1gInSc2
listopadu	listopad	k1gInSc2
2017	#num#	k4
vzrostla	vzrůst	k5eAaPmAgFnS
cena	cena	k1gFnSc1
bitcoinu	bitcoin	k1gInSc2
o	o	k7c4
150	#num#	k4
%	%	kIx~
až	až	k9
na	na	k7c4
10	#num#	k4
000	#num#	k4
USD	USD	kA
a	a	k8xC
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2017	#num#	k4
na	na	k7c4
téměř	téměř	k6eAd1
20	#num#	k4
000	#num#	k4
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
stav	stav	k1gInSc1
je	být	k5eAaImIp3nS
přirovnáván	přirovnávat	k5eAaImNgInS
k	k	k7c3
tulipánové	tulipánový	k2eAgFnSc3d1
horečce	horečka	k1gFnSc3
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Danou	daný	k2eAgFnSc4d1
tezi	teze	k1gFnSc4
ještě	ještě	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
následný	následný	k2eAgInSc1d1
prudký	prudký	k2eAgInSc1d1
propad	propad	k1gInSc1
kurzu	kurz	k1gInSc2
bitcoinu	bitcoinout	k5eAaPmIp1nS
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
polovinu	polovina	k1gFnSc4
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
došlo	dojít	k5eAaPmAgNnS
během	během	k7c2
ledna	leden	k1gInSc2
a	a	k8xC
února	únor	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
hedgeové	hedgeové	k2eAgInPc4d1
fondy	fond	k1gInPc4
<g/>
,	,	kIx,
například	například	k6eAd1
Silver	Silver	k1gInSc4
8	#num#	k4
Capital	Capital	k1gMnSc1
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
chtěly	chtít	k5eAaImAgFnP
situace	situace	k1gFnPc1
náhlého	náhlý	k2eAgInSc2d1
cenového	cenový	k2eAgInSc2d1
poklesu	pokles	k1gInSc2
kryptoměn	kryptoměn	k2eAgMnSc1d1
využít	využít	k5eAaPmF
a	a	k8xC
nalákat	nalákat	k5eAaPmF
nové	nový	k2eAgMnPc4d1
investory	investor	k1gMnPc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc1
hodnota	hodnota	k1gFnSc1
opět	opět	k6eAd1
brzy	brzy	k6eAd1
poroste	porůst	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Cenové	cenový	k2eAgFnPc1d1
špičky	špička	k1gFnPc1
na	na	k7c4
Bitcoinu	Bitcoina	k1gFnSc4
a	a	k8xC
vliv	vliv	k1gInSc4
půlení	půlení	k1gNnSc2
</s>
<s>
Cenové	cenový	k2eAgFnPc1d1
špičky	špička	k1gFnPc1
v	v	k7c6
letech	léto	k1gNnPc6
2011	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
a	a	k8xC
2017	#num#	k4
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
obchodování	obchodování	k1gNnSc2
s	s	k7c7
bitcoinem	bitcoin	k1gInSc7
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
tzv.	tzv.	kA
cenové	cenový	k2eAgFnPc4d1
bubliny	bublina	k1gFnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
cena	cena	k1gFnSc1
bitcoinu	bitcoin	k1gInSc2
prudce	prudko	k6eAd1
rostla	růst	k5eAaImAgFnS
na	na	k7c4
několikanásobek	několikanásobek	k1gInSc4
v	v	k7c6
období	období	k1gNnSc6
týdnů	týden	k1gInPc2
až	až	k8xS
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
s	s	k7c7
následnou	následný	k2eAgFnSc7d1
korekcí	korekce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoin	Bitcoin	k1gInSc1
má	mít	k5eAaImIp3nS
však	však	k9
podle	podle	k7c2
analýzy	analýza	k1gFnSc2
na	na	k7c4
Kurzy	kurz	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
dlouhodobě	dlouhodobě	k6eAd1
tendenci	tendence	k1gFnSc4
ke	k	k7c3
stabilizaci	stabilizace	k1gFnSc3
–	–	k?
každá	každý	k3xTgFnSc1
nová	nový	k2eAgFnSc1d1
vzestupná	vzestupný	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
má	mít	k5eAaImIp3nS
nižší	nízký	k2eAgInSc4d2
násobek	násobek	k1gInSc4
změny	změna	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
fáze	fáze	k1gFnPc1
předchozí	předchozí	k2eAgFnPc1d1
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
také	také	k9
zmenšuje	zmenšovat	k5eAaImIp3nS
relativní	relativní	k2eAgInSc1d1
pokles	pokles	k1gInSc1
po	po	k7c6
vzestupné	vzestupný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
cenové	cenový	k2eAgFnPc1d1
fáze	fáze	k1gFnPc1
u	u	k7c2
bitcoinu	bitcoin	k1gInSc2
souvisejí	souviset	k5eAaImIp3nP
s	s	k7c7
cykly	cyklus	k1gInPc7
takzvaného	takzvaný	k2eAgNnSc2d1
půlení	půlení	k1gNnSc2
(	(	kIx(
<g/>
halvingu	halving	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
snižováním	snižování	k1gNnSc7
odměny	odměna	k1gFnSc2
vyplácené	vyplácený	k2eAgFnSc2d1
těžařům	těžař	k1gMnPc3
na	na	k7c4
polovinu	polovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
dochází	docházet	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
jednou	jeden	k4xCgFnSc7
za	za	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
další	další	k2eAgNnSc1d1
půlení	půlení	k1gNnSc1
je	být	k5eAaImIp3nS
naplánováno	naplánovat	k5eAaBmNgNnS
na	na	k7c4
květen	květen	k1gInSc4
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitcoin	Bitcoin	k2eAgMnSc1d1
–	–	k?
porovnání	porovnání	k1gNnSc6
vzestupu	vzestup	k1gInSc2
a	a	k8xC
pádu	pád	k1gInSc2
±	±	k?
100	#num#	k4
dnů	den	k1gInPc2
v	v	k7c6
letech	léto	k1gNnPc6
2011	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
a	a	k8xC
2017	#num#	k4
</s>
<s>
Období	období	k1gNnSc1
/	/	kIx~
datum	datum	k1gNnSc1
</s>
<s>
Kurz	kurz	k1gInSc1
před	před	k7c7
</s>
<s>
Max	Max	k1gMnSc1
</s>
<s>
Kurz	kurz	k1gInSc1
po	po	k7c6
</s>
<s>
Max	max	kA
<g/>
/	/	kIx~
<g/>
Před	před	k7c7
</s>
<s>
Po	po	k7c4
<g/>
/	/	kIx~
<g/>
Před	před	k7c7
</s>
<s>
Max	Max	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Po	po	k7c6
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
</s>
<s>
0,9031	0,9031	k4
<g/>
,917	,917	k4
<g/>
,8135	,8135	k4
<g/>
,3	,3	k4
<g/>
×	×	k?
<g/>
8,6	8,6	k4
<g/>
×	×	k?
<g/>
−	−	k?
<g/>
4,1	4,1	k4
<g/>
×	×	k?
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
</s>
<s>
14,132	14,132	k4
<g/>
66,008	66,008	k4
<g/>
9,251	9,251	k4
<g/>
8,8	8,8	k4
<g/>
×	×	k?
<g/>
6,3	6,3	k4
<g/>
×	×	k?
<g/>
−	−	k?
<g/>
3,0	3,0	k4
<g/>
×	×	k?
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
</s>
<s>
121,171	121,171	k4
165,896	165,896	k4
<g/>
24,899,6	24,899,6	k4
<g/>
×	×	k?
<g/>
5,2	5,2	k4
<g/>
×	×	k?
<g/>
−	−	k?
<g/>
1,9	1,9	k4
<g/>
×	×	k?
</s>
<s>
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
0	#num#	k4
<g/>
87,561	87,561	k4
<g/>
7	#num#	k4
382,64	382,64	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
4,3	4,3	k4
<g/>
×	×	k?
<g/>
??	??	k?
</s>
<s>
Za	za	k7c4
bublinu	bublina	k1gFnSc4
bitcoin	bitcoina	k1gFnPc2
považuje	považovat	k5eAaImIp3nS
např.	např.	kA
investiční	investiční	k2eAgMnSc1d1
poradce	poradce	k1gMnSc1
Jan	Jan	k1gMnSc1
Traxler	Traxler	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Americký	americký	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
Nouriel	Nouriel	k1gMnSc1
Roubini	Roubin	k2eAgMnPc1d1
označil	označit	k5eAaPmAgMnS
Bitcoin	Bitcoin	k1gInSc4
za	za	k7c4
„	„	k?
<g/>
matku	matka	k1gFnSc4
všech	všecek	k3xTgFnPc2
bublin	bublina	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Investování	investování	k1gNnSc1
do	do	k7c2
bitcoinu	bitcoin	k1gInSc2
</s>
<s>
Bitcoin	Bitcoin	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
než	než	k8xS
pro	pro	k7c4
placení	placení	k1gNnSc4
používá	používat	k5eAaImIp3nS
díky	díky	k7c3
rychlému	rychlý	k2eAgInSc3d1
růstu	růst	k1gInSc3
jako	jako	k9
investice	investice	k1gFnPc4
pro	pro	k7c4
zhodnocení	zhodnocení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
investicemi	investice	k1gFnPc7
do	do	k7c2
Bitcoinu	Bitcoin	k1gInSc2
ale	ale	k8xC
varovala	varovat	k5eAaImAgFnS
Deutsche	Deutsch	k1gMnPc4
Bank	banka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
hlavního	hlavní	k2eAgMnSc2d1
investičního	investiční	k2eAgMnSc2d1
stratéga	stratég	k1gMnSc2
banky	banka	k1gFnSc2
Ulricha	Ulrich	k1gMnSc2
Stephana	Stephan	k1gMnSc2
měna	měna	k1gFnSc1
není	být	k5eNaImIp3nS
regulovaná	regulovaný	k2eAgFnSc1d1
a	a	k8xC
prochází	procházet	k5eAaImIp3nS
nadměrnými	nadměrný	k2eAgInPc7d1
výkyvy	výkyv	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náklady	náklad	k1gInPc1
na	na	k7c4
fungování	fungování	k1gNnSc4
bitcoinu	bitcoin	k1gInSc2
</s>
<s>
Součástí	součást	k1gFnSc7
nákladů	náklad	k1gInPc2
na	na	k7c4
fungování	fungování	k1gNnSc4
Bitcoinu	Bitcoin	k1gInSc2
je	být	k5eAaImIp3nS
spotřeba	spotřeba	k1gFnSc1
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
na	na	k7c4
těžení	těžení	k1gNnSc4
nových	nový	k2eAgInPc2d1
Bitcoinů	Bitcoin	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolování	dolování	k1gNnSc1
Bitcoinu	Bitcoin	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
spotřebovalo	spotřebovat	k5eAaPmAgNnS
více	hodně	k6eAd2
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
než	než	k8xS
jaká	jaký	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
spotřeba	spotřeba	k1gFnSc1
20	#num#	k4
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
celosvětovém	celosvětový	k2eAgNnSc6d1
porovnání	porovnání	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
byla	být	k5eAaImAgFnS
spotřeba	spotřeba	k1gFnSc1
energie	energie	k1gFnSc2
na	na	k7c4
dolování	dolování	k1gNnSc4
Bitcoinu	Bitcoin	k1gInSc2
větší	veliký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
spotřeba	spotřeba	k1gFnSc1
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
ve	v	k7c6
159	#num#	k4
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
země	zem	k1gFnPc4
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yQgMnPc2,k3yRgMnPc2,k3yIgMnPc2
při	při	k7c6
porovnání	porovnání	k1gNnSc6
jejich	jejich	k3xOp3gFnSc2
celkové	celkový	k2eAgFnSc2d1
spotřeby	spotřeba	k1gFnSc2
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
a	a	k8xC
celkového	celkový	k2eAgInSc2d1
objemu	objem	k1gInSc2
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
spotřebovaného	spotřebovaný	k2eAgMnSc2d1
na	na	k7c6
těžení	těžení	k1gNnSc6
bitcoinu	bitcoin	k1gInSc2
je	být	k5eAaImIp3nS
spotřeba	spotřeba	k1gFnSc1
energie	energie	k1gFnSc2
na	na	k7c6
dolování	dolování	k1gNnSc6
Bitcoinu	Bitcoin	k1gInSc2
vyšší	vysoký	k2eAgMnSc1d2
<g/>
,	,	kIx,
patřilo	patřit	k5eAaImAgNnS
Irsko	Irsko	k1gNnSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
a	a	k8xC
Island	Island	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
listopadu	listopad	k1gInSc2
2017	#num#	k4
podle	podle	k7c2
odhadů	odhad	k1gInPc2
bitcoin	bitcoina	k1gFnPc2
spotřeboval	spotřebovat	k5eAaPmAgInS
25,76	25,76	k4
TWh	TWh	k1gMnPc2
elektřiny	elektřina	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Kdyby	kdyby	kYmCp3nS
byl	být	k5eAaImAgMnS
bitcoin	bitcoin	k1gMnSc1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
aktuálního	aktuální	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
spotřeby	spotřeba	k1gFnSc2
energie	energie	k1gFnSc2
by	by	kYmCp3nS
umístil	umístit	k5eAaPmAgMnS
na	na	k7c4
68	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
těsně	těsně	k6eAd1
za	za	k7c7
Ománem	Omán	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Energie	energie	k1gFnSc1
spotřebovaná	spotřebovaný	k2eAgFnSc1d1
na	na	k7c4
těžbu	těžba	k1gFnSc4
bitcoinu	bitcoin	k1gInSc2
by	by	kYmCp3nS
tak	tak	k9
pokryla	pokrýt	k5eAaPmAgFnS
asi	asi	k9
36	#num#	k4
%	%	kIx~
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
spotřebované	spotřebovaný	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
vyžadovalo	vyžadovat	k5eAaImAgNnS
ověření	ověření	k1gNnSc1
jedné	jeden	k4xCgFnSc2
transakce	transakce	k1gFnSc2
Bitcoinem	Bitcoin	k1gInSc7
stejné	stejný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
energie	energie	k1gFnSc2
jako	jako	k9
465	#num#	k4
tisíc	tisíc	k4xCgInPc2
plateb	platba	k1gFnPc2
kartou	karta	k1gFnSc7
VISA	viso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zastánci	zastánce	k1gMnPc1
bitcoinu	bitcoin	k1gInSc2
ale	ale	k8xC
argumentují	argumentovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
energetická	energetický	k2eAgFnSc1d1
náročnost	náročnost	k1gFnSc1
má	mít	k5eAaImIp3nS
smysl	smysl	k1gInSc4
sama	sám	k3xTgFnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
brání	bránit	k5eAaImIp3nS
snahám	snaha	k1gFnPc3
manipulovat	manipulovat	k5eAaImF
s	s	k7c7
údaji	údaj	k1gInPc7
v	v	k7c6
blockchainu	blockchaino	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
operace	operace	k1gFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
příliš	příliš	k6eAd1
finančně	finančně	k6eAd1
nákladná	nákladný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
popularizátora	popularizátor	k1gMnSc2
bitcoinu	bitcoinout	k5eAaPmIp1nS
Andrease	Andreasa	k1gFnSc6
Antonopoulose	Antonopoulosa	k1gFnSc6
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
energetickou	energetický	k2eAgFnSc7d1
náročností	náročnost	k1gFnSc7
současného	současný	k2eAgInSc2d1
finančního	finanční	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
by	by	kYmCp3nP
se	se	k3xPyFc4
počítaly	počítat	k5eAaImAgFnP
i	i	k9
náklady	náklad	k1gInPc4
na	na	k7c4
provoz	provoz	k1gInSc4
centrál	centrála	k1gFnPc2
bank	banka	k1gFnPc2
či	či	k8xC
tisk	tisk	k1gInSc1
a	a	k8xC
rozvoz	rozvoz	k1gInSc1
bankovek	bankovka	k1gFnPc2
<g/>
,	,	kIx,
údajně	údajně	k6eAd1
stále	stále	k6eAd1
nejde	jít	k5eNaImIp3nS
o	o	k7c4
tak	tak	k6eAd1
vysoká	vysoký	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
vysoké	vysoký	k2eAgFnSc3d1
spotřebě	spotřeba	k1gFnSc3
pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
těžení	těžení	k1gNnSc4
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
amerického	americký	k2eAgMnSc2d1
ekonoma	ekonom	k1gMnSc2
Nouriela	Nouriel	k1gMnSc2
Roubiniho	Roubini	k1gMnSc2
nejen	nejen	k6eAd1
Bitcoin	Bitcoina	k1gFnPc2
i	i	k8xC
další	další	k2eAgFnSc2d1
digitální	digitální	k2eAgFnSc2d1
měny	měna	k1gFnSc2
přírodní	přírodní	k2eAgFnSc2d1
pohromou	pohroma	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
ovšem	ovšem	k9
dodat	dodat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
podle	podle	k7c2
analýzy	analýza	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
pochází	pocházet	k5eAaImIp3nS
74	#num#	k4
%	%	kIx~
výpočetního	výpočetní	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
vynaloženého	vynaložený	k2eAgMnSc4d1
na	na	k7c4
těžbu	těžba	k1gFnSc4
Bitcoinu	Bitcoin	k1gInSc2
z	z	k7c2
obnovitelných	obnovitelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Okolo	okolo	k7c2
60-70	60-70	k4
%	%	kIx~
vytěžených	vytěžený	k2eAgInPc2d1
Bitcoinů	Bitcoin	k1gInPc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Činy	čina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
sice	sice	k8xC
získává	získávat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
dvě	dva	k4xCgFnPc4
třetiny	třetina	k1gFnPc4
energie	energie	k1gFnSc2
z	z	k7c2
uhelných	uhelný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
těžení	těžení	k1gNnSc1
Bitcoinu	Bitcoin	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
Číně	Čína	k1gFnSc6
odehrává	odehrávat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
bohatých	bohatý	k2eAgMnPc2d1
na	na	k7c4
větrnou	větrný	k2eAgFnSc4d1
a	a	k8xC
vodní	vodní	k2eAgFnSc4d1
energii	energie	k1gFnSc4
<g/>
.	.	kIx.
80	#num#	k4
%	%	kIx~
těžby	těžba	k1gFnSc2
se	se	k3xPyFc4
koncentruje	koncentrovat	k5eAaBmIp3nS
v	v	k7c6
provincii	provincie	k1gFnSc6
Sichuan	Sichuana	k1gFnPc2
bohatou	bohatý	k2eAgFnSc4d1
na	na	k7c4
hydroelektrárny	hydroelektrárna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžení	těžení	k1gNnSc1
v	v	k7c6
těchto	tento	k3xDgFnPc6
oblastech	oblast	k1gFnPc6
absorbuje	absorbovat	k5eAaBmIp3nS
nadprodukci	nadprodukce	k1gFnSc4
hydro-energie	hydro-energie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
jinak	jinak	k6eAd1
přišla	přijít	k5eAaPmAgFnS
na	na	k7c4
zmar	zmar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžbě	těžba	k1gFnSc6
Bitcoinu	Bitcoin	k1gInSc2
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
také	také	k9
dominují	dominovat	k5eAaImIp3nP
obnovitelné	obnovitelný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Island	Island	k1gInSc1
(	(	kIx(
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Québec	Québec	k1gMnSc1
(	(	kIx(
<g/>
99.8	99.8	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Britská	britský	k2eAgFnSc1d1
Kolumbie	Kolumbie	k1gFnSc1
(	(	kIx(
<g/>
98.4	98.4	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
(	(	kIx(
<g/>
98	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
však	však	k9
i	i	k9
optimističtější	optimistický	k2eAgInPc4d2
názory	názor	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
energická	energický	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
již	již	k6eAd1
více	hodně	k6eAd2
neporoste	porůst	k5eNaPmIp3nS,k5eNaImIp3nS
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
bude	být	k5eAaImBp3nS
klesat	klesat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
půlka	půlka	k1gFnSc1
argumentace	argumentace	k1gFnSc2
se	se	k3xPyFc4
opírá	opírat	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
rostoucím	rostoucí	k2eAgInSc7d1
zájmem	zájem	k1gInSc7
lidí	člověk	k1gMnPc2
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
výpočetní	výpočetní	k2eAgInSc1d1
výkon	výkon	k1gInSc1
(	(	kIx(
<g/>
spotřeba	spotřeba	k1gFnSc1
energie	energie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
tom	ten	k3xDgNnSc6
i	i	k9
náročnost	náročnost	k1gFnSc4
těžby	těžba	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
k	k	k7c3
ověření	ověření	k1gNnSc3
jednoho	jeden	k4xCgInSc2
bloku	blok	k1gInSc2
docházelo	docházet	k5eAaImAgNnS
vždy	vždy	k6eAd1
jednou	jeden	k4xCgFnSc7
za	za	k7c4
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
půlka	půlka	k1gFnSc1
argumentace	argumentace	k1gFnSc2
se	se	k3xPyFc4
opírá	opírat	k5eAaImIp3nS
o	o	k7c6
tzv.	tzv.	kA
halving	halving	k1gInSc4
<g/>
,	,	kIx,
tj.	tj.	kA
pravidelné	pravidelný	k2eAgNnSc1d1
půlení	půlení	k1gNnSc1
odměny	odměna	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
je	být	k5eAaImIp3nS
12,5	12,5	k4
BTC	BTC	kA
<g/>
)	)	kIx)
za	za	k7c4
ověření	ověření	k1gNnSc4
bloku	blok	k1gInSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
dochází	docházet	k5eAaImIp3nS
každé	každý	k3xTgInPc4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2019	#num#	k4
náklady	náklad	k1gInPc1
na	na	k7c4
energii	energie	k1gFnSc4
snižovaly	snižovat	k5eAaImAgFnP
zisk	zisk	k1gInSc4
z	z	k7c2
těžby	těžba	k1gFnSc2
Bitcoinu	Bitcoin	k1gInSc2
v	v	k7c6
průměru	průměr	k1gInSc6
o	o	k7c4
48,1	48,1	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
při	při	k7c6
dalším	další	k2eAgInSc6d1
halvingu	halving	k1gInSc6
(	(	kIx(
<g/>
z	z	k7c2
12,5	12,5	k4
na	na	k7c4
6,25	6,25	k4
BTC	BTC	kA
<g/>
)	)	kIx)
v	v	k7c6
květnu	květen	k1gInSc6
2020	#num#	k4
nestoupne	stoupnout	k5eNaPmIp3nS
cena	cena	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
alespoň	alespoň	k9
na	na	k7c4
20	#num#	k4
tisíc	tisíc	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
spotřebovaná	spotřebovaný	k2eAgFnSc1d1
energie	energie	k1gFnSc1
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
sníží	snížit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
ten	ten	k3xDgInSc4
moment	moment	k1gInSc4
se	se	k3xPyFc4
těžařům	těžař	k1gMnPc3
těžba	těžba	k1gFnSc1
příliš	příliš	k6eAd1
nevyplatí	vyplatit	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Směnárny	směnárna	k1gFnPc1
a	a	k8xC
využití	využití	k1gNnSc1
měny	měna	k1gFnSc2
</s>
<s>
Bitcoiny	Bitcoina	k1gFnPc4
lze	lze	k6eAd1
získat	získat	k5eAaPmF
(	(	kIx(
<g/>
nebo	nebo	k8xC
prodat	prodat	k5eAaPmF
<g/>
)	)	kIx)
třemi	tři	k4xCgInPc7
hlavními	hlavní	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
Použití	použití	k1gNnSc1
specializovaných	specializovaný	k2eAgFnPc2d1
směnáren	směnárna	k1gFnPc2
na	na	k7c6
bázi	báze	k1gFnSc6
burzy	burza	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
mezi	mezi	k7c4
největší	veliký	k2eAgFnPc4d3
světové	světový	k2eAgFnPc4d1
burzy	burza	k1gFnPc4
bitcoinů	bitcoin	k1gMnPc2
patří	patřit	k5eAaImIp3nS
Binance	Binanec	k1gInSc2
<g/>
,	,	kIx,
Coinbase	Coinbasa	k1gFnSc6
a	a	k8xC
Kraken	Kraken	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bitcoinové	Bitcoinový	k2eAgFnPc1d1
směnárny	směnárna	k1gFnPc1
</s>
<s>
E-shop	E-shop	k1gInSc1
s	s	k7c7
kryptoměnami	kryptoměna	k1gFnPc7
</s>
<s>
Přímá	přímý	k2eAgFnSc1d1
směna	směna	k1gFnSc1
s	s	k7c7
některým	některý	k3yIgMnSc7
uživatelem	uživatel	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
ochotný	ochotný	k2eAgMnSc1d1
Bitcoiny	Bitcoina	k1gFnPc4
prodat	prodat	k5eAaPmF
<g/>
/	/	kIx~
<g/>
koupit	koupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
LocalBitcoins	LocalBitcoins	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
pro	pro	k7c4
osobní	osobní	k2eAgNnSc4d1
setkání	setkání	k1gNnSc4
i	i	k9
online	onlinout	k5eAaPmIp3nS
převod	převod	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
na	na	k7c6
fóru	fórum	k1gNnSc6
Bitcash	Bitcasha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s>
Těžba	těžba	k1gFnSc1
Bitcoinů	Bitcoin	k1gMnPc2
</s>
<s>
ať	ať	k8xC,k8xS
už	už	k9
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
některém	některý	k3yIgInSc6
z	z	k7c2
uskupení	uskupení	k1gNnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
poolu	poolat	k5eAaPmIp1nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkonný	výkonný	k2eAgInSc1d1
hardware	hardware	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
těžba	těžba	k1gFnSc1
provozuje	provozovat	k5eAaImIp3nS
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
velkou	velký	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
procesory	procesor	k1gInPc1
a	a	k8xC
grafické	grafický	k2eAgFnPc1d1
karty	karta	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
speciální	speciální	k2eAgInPc4d1
programovatelné	programovatelný	k2eAgInPc4d1
obvody	obvod	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžit	těžit	k5eAaImF
bitcoin	bitcoin	k1gInSc4
se	se	k3xPyFc4
podle	podle	k7c2
propočtů	propočet	k1gInPc2
analytiků	analytik	k1gMnPc2
vyplatí	vyplatit	k5eAaPmIp3nS
zhruba	zhruba	k6eAd1
od	od	k7c2
kurzu	kurz	k1gInSc2
šesti	šest	k4xCc2
tisíc	tisíc	k4xCgInPc2
dolarů	dolar	k1gInPc2
za	za	k7c4
jednu	jeden	k4xCgFnSc4
virtuální	virtuální	k2eAgFnSc4d1
minci	mince	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
měny	měna	k1gFnSc2
je	být	k5eAaImIp3nS
zatím	zatím	k6eAd1
poměrně	poměrně	k6eAd1
malé	malý	k2eAgFnPc1d1
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
kvůli	kvůli	k7c3
příliš	příliš	k6eAd1
proměnlivému	proměnlivý	k2eAgInSc3d1
kurzu	kurz	k1gInSc3
a	a	k8xC
nejistým	jistý	k2eNgFnPc3d1
legislativním	legislativní	k2eAgFnPc3d1
otázkám	otázka	k1gFnPc3
obchodníků	obchodník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
začala	začít	k5eAaPmAgFnS
sponzorské	sponzorský	k2eAgInPc4d1
dary	dar	k1gInPc4
v	v	k7c6
bitcoinech	bitcoin	k1gInPc6
přijímat	přijímat	k5eAaImF
Apache	Apache	k1gInSc4
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
i	i	k8xC
Nadace	nadace	k1gFnSc1
Wikimedia	Wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2017	#num#	k4
začal	začít	k5eAaPmAgInS
Bitcoin	Bitcoin	k1gInSc1
přijímat	přijímat	k5eAaImF
internetový	internetový	k2eAgInSc4d1
obchod	obchod	k1gInSc4
Alza	Alz	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
do	do	k7c2
budoucna	budoucno	k1gNnSc2
plánuje	plánovat	k5eAaImIp3nS
i	i	k9
podporu	podpora	k1gFnSc4
jiných	jiný	k2eAgFnPc2d1
kryptoměn	kryptoměn	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Prvním	první	k4xOgInSc7
z	z	k7c2
podporovaných	podporovaný	k2eAgInPc2d1
altcoinů	altcoin	k1gInPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
v	v	k7c6
únoru	únor	k1gInSc6
2018	#num#	k4
litecoin	litecoina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2017	#num#	k4
proběhl	proběhnout	k5eAaPmAgMnS
i	i	k9
první	první	k4xOgInSc4
známý	známý	k2eAgInSc4d1
prodej	prodej	k1gInSc4
bytu	byt	k1gInSc2
za	za	k7c4
bitcoin	bitcoin	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
skrz	skrz	k7c4
českou	český	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
HOME	HOME	kA
Hunters	Hunters	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
světově	světově	k6eAd1
druhou	druhý	k4xOgFnSc4
nemovitost	nemovitost	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c6
tom	ten	k3xDgNnSc6
co	co	k9
v	v	k7c6
září	září	k1gNnSc6
2017	#num#	k4
první	první	k4xOgInSc4
rodinný	rodinný	k2eAgInSc4d1
dům	dům	k1gInSc4
prodali	prodat	k5eAaPmAgMnP
v	v	k7c6
americkém	americký	k2eAgInSc6d1
Texasu	Texas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
viceguvernéra	viceguvernér	k1gMnSc2
ČNB	ČNB	kA
Mojmíra	Mojmír	k1gMnSc2
Hampla	Hampl	k1gMnSc2
bitcoin	bitcoin	k1gMnSc1
jakožto	jakožto	k8xS
deflační	deflační	k2eAgFnSc1d1
měna	měna	k1gFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
běžný	běžný	k2eAgInSc4d1
platební	platební	k2eAgInSc4d1
styk	styk	k1gInSc4
nevhodný	vhodný	k2eNgInSc4d1
a	a	k8xC
velké	velký	k2eAgInPc4d1
výkyvy	výkyv	k1gInPc4
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
ceně	cena	k1gFnSc6
brání	bránit	k5eAaImIp3nS
lidem	lid	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
jej	on	k3xPp3gInSc4
masově	masově	k6eAd1
osvojili	osvojit	k5eAaPmAgMnP
pro	pro	k7c4
nákupy	nákup	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Software	software	k1gInSc1
pro	pro	k7c4
bitcoin	bitcoin	k1gInSc4
(	(	kIx(
<g/>
klienty	klient	k1gMnPc7
<g/>
)	)	kIx)
</s>
<s>
Bitcoin	Bitcoin	k1gMnSc1
</s>
<s>
Vývojář	vývojář	k1gMnSc1
</s>
<s>
Satoshi	Satosh	k1gFnSc5
Nakamoto	Nakamota	k1gFnSc5
První	první	k4xOgFnSc1
vydání	vydání	k1gNnSc6
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
Aktuální	aktuální	k2eAgFnSc2d1
verze	verze	k1gFnSc2
</s>
<s>
0.16	0.16	k4
<g/>
.0	.0	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
multiplatformní	multiplatformní	k2eAgNnSc1d1
Vyvíjeno	vyvíjen	k2eAgNnSc1d1
v	v	k7c4
</s>
<s>
C	C	kA
<g/>
++	++	k?
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
elektronické	elektronický	k2eAgInPc1d1
peníze	peníz	k1gInPc1
Licence	licence	k1gFnSc2
</s>
<s>
MIT	MIT	kA
Web	web	k1gInSc1
</s>
<s>
www.bitcoin.org	www.bitcoin.org	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oficiálním	oficiální	k2eAgMnSc7d1
klientem	klient	k1gMnSc7
je	být	k5eAaImIp3nS
Bitcoin-Qt	Bitcoin-Qt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
„	„	k?
<g/>
úplný	úplný	k2eAgInSc1d1
<g/>
“	“	k?
klient	klient	k1gMnSc1
(	(	kIx(
<g/>
fungující	fungující	k2eAgInSc1d1
i	i	k8xC
jako	jako	k9
serverová	serverový	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
však	však	k9
obsahuje	obsahovat	k5eAaImIp3nS
kompletní	kompletní	k2eAgFnSc4d1
databázi	databáze	k1gFnSc4
transakcí	transakce	k1gFnPc2
(	(	kIx(
<g/>
blockchain	blockchain	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
své	svůj	k3xOyFgFnSc2
náročnosti	náročnost	k1gFnSc2
bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c4
nevhodný	vhodný	k2eNgInSc4d1
pro	pro	k7c4
koncového	koncový	k2eAgMnSc4d1
uživatele	uživatel	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnSc7d1
kategorií	kategorie	k1gFnSc7
jsou	být	k5eAaImIp3nP
tzv.	tzv.	kA
tenké	tenký	k2eAgInPc1d1
klienty	klient	k1gMnPc4
(	(	kIx(
<g/>
Electrum	Electrum	k1gNnSc1
<g/>
,	,	kIx,
MultiBit	MultiBit	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
fungují	fungovat	k5eAaImIp3nP
jako	jako	k9
interface	interface	k1gInSc4
k	k	k7c3
jinému	jiný	k2eAgInSc3d1
serveru	server	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
ale	ale	k8xC
nijak	nijak	k6eAd1
nesnižuje	snižovat	k5eNaImIp3nS
jejich	jejich	k3xOp3gFnSc4
bezpečnost	bezpečnost	k1gFnSc4
<g/>
;	;	kIx,
soukromé	soukromý	k2eAgInPc4d1
klíče	klíč	k1gInPc4
má	mít	k5eAaImIp3nS
uživatel	uživatel	k1gMnSc1
pouze	pouze	k6eAd1
na	na	k7c6
svém	svůj	k3xOyFgInSc6
počítači	počítač	k1gInSc6
a	a	k8xC
nikdy	nikdy	k6eAd1
se	se	k3xPyFc4
nikam	nikam	k6eAd1
neposílají	posílat	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Server	server	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
jakýkoli	jakýkoli	k3yIgInSc4
uzel	uzel	k1gInSc4
Bitcoinu	Bitcoin	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
vázán	vázat	k5eAaImNgMnS
na	na	k7c4
uzel	uzel	k1gInSc4
od	od	k7c2
autora	autor	k1gMnSc2
software	software	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
pokročilé	pokročilý	k2eAgMnPc4d1
uživatele	uživatel	k1gMnPc4
a	a	k8xC
vývojáře	vývojář	k1gMnSc4
je	být	k5eAaImIp3nS
určen	určen	k2eAgMnSc1d1
klient	klient	k1gMnSc1
Armory	Armora	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
běží	běžet	k5eAaImIp3nS
nad	nad	k7c4
Bitcoin-Qt	Bitcoin-Qt	k1gInSc4
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
bohatší	bohatý	k2eAgFnSc4d2
správu	správa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
existují	existovat	k5eAaImIp3nP
aplikace	aplikace	k1gFnSc1
jako	jako	k8xC,k8xS
BitcoinSpinner	BitcoinSpinner	k1gInSc1
nebo	nebo	k8xC
Bitcoin	Bitcoin	k2eAgInSc1d1
Wallet	Wallet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
posílání	posílání	k1gNnSc6
adres	adresa	k1gFnPc2
se	se	k3xPyFc4
často	často	k6eAd1
používají	používat	k5eAaImIp3nP
QR	QR	kA
kódy	kód	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Regulace	regulace	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
Německo	Německo	k1gNnSc1
Bitcoin	Bitcoina	k1gFnPc2
uznalo	uznat	k5eAaPmAgNnS
jako	jako	k9
oficiální	oficiální	k2eAgFnSc4d1
virtuální	virtuální	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
<g/>
,	,	kIx,
zisky	zisk	k1gInPc4
z	z	k7c2
transakcí	transakce	k1gFnPc2
se	se	k3xPyFc4
daní	danit	k5eAaImIp3nS
standardní	standardní	k2eAgFnSc7d1
sazbou	sazba	k1gFnSc7
daně	daň	k1gFnSc2
z	z	k7c2
příjmu	příjem	k1gInSc2
fyzické	fyzický	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaněné	daněný	k2eNgInPc1d1
zisky	zisk	k1gInPc1
lze	lze	k6eAd1
realizovat	realizovat	k5eAaBmF
pouze	pouze	k6eAd1
držením	držení	k1gNnSc7
bitcoinu	bitcoin	k1gInSc2
déle	dlouho	k6eAd2
než	než	k8xS
1	#num#	k4
kalendářní	kalendářní	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdaněny	zdaněn	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
transakce	transakce	k1gFnPc1
mezi	mezi	k7c7
Bitcoinem	Bitcoino	k1gNnSc7
a	a	k8xC
Altcoiny	Altcoin	k1gInPc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
hodnota	hodnota	k1gFnSc1
transakce	transakce	k1gFnSc2
převádí	převádět	k5eAaImIp3nS
na	na	k7c4
aktuální	aktuální	k2eAgInSc4d1
kurz	kurz	k1gInSc4
v	v	k7c6
euru	euro	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdaněna	zdaněn	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
těžba	těžba	k1gFnSc1
bitcoinů	bitcoin	k1gMnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
těžař	těžař	k1gMnSc1
je	být	k5eAaImIp3nS
oprávněn	oprávnit	k5eAaPmNgMnS
odečíst	odečíst	k5eAaPmF
veškeré	veškerý	k3xTgInPc4
náklady	náklad	k1gInPc4
na	na	k7c4
těžbu	těžba	k1gFnSc4
bitcoinu	bitcoin	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
nákup	nákup	k1gInSc4
zařízení	zařízení	k1gNnPc2
či	či	k8xC
spotřeba	spotřeba	k1gFnSc1
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
zatím	zatím	k6eAd1
legislativa	legislativa	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
specificky	specificky	k6eAd1
na	na	k7c4
kryptoměny	kryptoměn	k2eAgMnPc4d1
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
však	však	k9
z	z	k7c2
investorů	investor	k1gMnPc2
nesnímá	snímat	k5eNaImIp3nS
povinnost	povinnost	k1gFnSc4
danit	danit	k5eAaImF
dosažené	dosažený	k2eAgInPc4d1
zisky	zisk	k1gInPc4
podle	podle	k7c2
stávajících	stávající	k2eAgFnPc2d1
regulí	regule	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
slovenské	slovenský	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
financí	finance	k1gFnPc2
se	se	k3xPyFc4
chystá	chystat	k5eAaImIp3nS
zdanit	zdanit	k5eAaPmF
digitální	digitální	k2eAgFnSc2d1
měny	měna	k1gFnSc2
zvláštní	zvláštní	k2eAgFnSc2d1
sazbou	sazba	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2013	#num#	k4
Čínská	čínský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
banka	banka	k1gFnSc1
zakázala	zakázat	k5eAaPmAgFnS
finančním	finanční	k2eAgFnPc3d1
institucím	instituce	k1gFnPc3
používat	používat	k5eAaImF
Bitcoin	Bitcoin	k2eAgMnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gNnSc4
používání	používání	k1gNnSc4
veřejností	veřejnost	k1gFnPc2
povolila	povolit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2015	#num#	k4
americká	americký	k2eAgFnSc1d1
Komise	komise	k1gFnSc1
pro	pro	k7c4
komoditní	komoditní	k2eAgInPc4d1
obchody	obchod	k1gInPc4
(	(	kIx(
<g/>
Commodity	Commodita	k1gFnSc2
Futures	Futures	k1gInSc1
Trading	Trading	k1gInSc1
Commission	Commission	k1gInSc1
<g/>
,	,	kIx,
CFTC	CFTC	kA
<g/>
)	)	kIx)
oficiálně	oficiálně	k6eAd1
označila	označit	k5eAaPmAgFnS
bitcoin	bitcoin	k1gInSc4
za	za	k7c4
komoditu	komodita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
také	také	k9
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
provozovatelé	provozovatel	k1gMnPc1
bitcoinových	bitcoinův	k2eAgFnPc2d1
burz	burza	k1gFnPc2
musejí	muset	k5eAaImIp3nP
registrovat	registrovat	k5eAaBmF
a	a	k8xC
provozovat	provozovat	k5eAaImF
své	svůj	k3xOyFgInPc4
obchody	obchod	k1gInPc4
pod	pod	k7c7
dohledem	dohled	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc2
již	již	k6eAd1
dříve	dříve	k6eAd2
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Bitcoin	Bitcoin	k1gInSc1
je	být	k5eAaImIp3nS
nehmotným	hmotný	k2eNgInSc7d1
aktivem	aktiv	k1gInSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
ho	on	k3xPp3gMnSc4
učinila	učinit	k5eAaImAgFnS,k5eAaPmAgFnS
zdanitelným	zdanitelný	k2eAgMnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odborníci	odborník	k1gMnPc1
časem	časem	k6eAd1
očekávají	očekávat	k5eAaImIp3nP
regulaci	regulace	k1gFnSc4
ze	z	k7c2
strany	strana	k1gFnSc2
dalších	další	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
bank	banka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Obavy	obava	k1gFnPc1
z	z	k7c2
přísnějšího	přísný	k2eAgInSc2d2
postupu	postup	k1gInSc2
států	stát	k1gInPc2
vůči	vůči	k7c3
kryptoměnám	kryptoměna	k1gFnPc3
přispěly	přispět	k5eAaPmAgInP
začátkem	začátek	k1gInSc7
roku	rok	k1gInSc2
2018	#num#	k4
k	k	k7c3
propadu	propad	k1gInSc3
hodnoty	hodnota	k1gFnSc2
bitcoinu	bitcoin	k1gInSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
digitálních	digitální	k2eAgFnPc2d1
měn	měna	k1gFnPc2
až	až	k9
o	o	k7c4
polovinu	polovina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2017	#num#	k4
některé	některý	k3yIgFnSc2
banky	banka	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
blokovaly	blokovat	k5eAaImAgFnP
platby	platba	k1gFnPc1
pro	pro	k7c4
Bitcoinové	Bitcoinový	k2eAgFnPc4d1
směnárny	směnárna	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
a	a	k8xC
rizika	riziko	k1gNnSc2
kryptoměn	kryptoměn	k2eAgMnSc1d1
sleduje	sledovat	k5eAaImIp3nS
rovněž	rovněž	k9
Evropská	evropský	k2eAgFnSc1d1
centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
šéf	šéf	k1gMnSc1
Mario	Mario	k1gMnSc1
Draghi	Dragh	k1gFnSc2
ale	ale	k8xC
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
regulace	regulace	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
není	být	k5eNaImIp3nS
záležitostí	záležitost	k1gFnSc7
této	tento	k3xDgFnSc2
instituce	instituce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
vstoupila	vstoupit	k5eAaPmAgFnS
v	v	k7c4
platnost	platnost	k1gFnSc4
směrnice	směrnice	k1gFnSc2
ALM	ALM	kA
(	(	kIx(
<g/>
Anti	Ante	k1gFnSc4
Money	Monea	k1gFnSc2
Laundering	Laundering	k1gInSc1
<g/>
)	)	kIx)
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
ní	on	k3xPp3gFnSc2
jsou	být	k5eAaImIp3nP
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
EU	EU	kA
povinny	povinen	k2eAgInPc1d1
do	do	k7c2
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
zřídit	zřídit	k5eAaPmF
specializované	specializovaný	k2eAgInPc1d1
registry	registr	k1gInPc1
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
se	se	k3xPyFc4
zaregistrují	zaregistrovat	k5eAaPmIp3nP
všechny	všechen	k3xTgFnPc1
firmy	firma	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nabízí	nabízet	k5eAaImIp3nP
směnu	směna	k1gFnSc4
nebo	nebo	k8xC
ukládání	ukládání	k1gNnSc1
kryptoměn	kryptoměn	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
tuto	tento	k3xDgFnSc4
směrnici	směrnice	k1gFnSc4
rozšířila	rozšířit	k5eAaPmAgFnS
na	na	k7c4
úplně	úplně	k6eAd1
všechny	všechen	k3xTgFnPc1
firmy	firma	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
poskytují	poskytovat	k5eAaImIp3nP
služby	služba	k1gFnPc4
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
kryptoměnami	kryptoměna	k1gFnPc7
<g/>
,	,	kIx,
tj.	tj.	kA
například	například	k6eAd1
i	i	k9
obchodníci	obchodník	k1gMnPc1
přijímající	přijímající	k2eAgFnSc2d1
BTC	BTC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
odborníků	odborník	k1gMnPc2
by	by	kYmCp3nS
tím	ten	k3xDgInSc7
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
znevýhodněni	znevýhodnit	k5eAaPmNgMnP
oproti	oproti	k7c3
konkurenci	konkurence	k1gFnSc3
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Speciální	speciální	k2eAgInSc1d1
registr	registr	k1gInSc1
bude	být	k5eAaImBp3nS
vedený	vedený	k2eAgInSc1d1
pod	pod	k7c7
Živnostenským	živnostenský	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
a	a	k8xC
k	k	k7c3
jeho	jeho	k3xOp3gInPc3
údajům	údaj	k1gInPc3
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
přístup	přístup	k1gInSc4
i	i	k9
Finanční	finanční	k2eAgInSc4d1
analytický	analytický	k2eAgInSc4d1
úřad	úřad	k1gInSc4
bojující	bojující	k2eAgInSc4d1
proti	proti	k7c3
praní	praní	k1gNnSc3
špinavých	špinavý	k2eAgInPc2d1
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Registrace	registrace	k1gFnSc1
má	mít	k5eAaImIp3nS
zahrnovat	zahrnovat	k5eAaImF
také	také	k9
přísnější	přísný	k2eAgFnSc4d2
a	a	k8xC
důslednější	důsledný	k2eAgFnSc4d2
kontrolu	kontrola	k1gFnSc4
klientů	klient	k1gMnPc2
i	i	k8xC
plateb	platba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
nesplnění	nesplnění	k1gNnSc4
těchto	tento	k3xDgInPc2
požadavků	požadavek	k1gInPc2
hrozí	hrozit	k5eAaImIp3nS
subjektům	subjekt	k1gInPc3
pokuta	pokuta	k1gFnSc1
až	až	k9
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
půl	půl	k1xP
miliónu	milión	k4xCgInSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tím	ten	k3xDgNnSc7
bude	být	k5eAaImBp3nS
zatížen	zatížit	k5eAaPmNgMnS
zejména	zejména	k9
klient	klient	k1gMnSc1
služeb	služba	k1gFnPc2
spojených	spojený	k2eAgFnPc2d1
s	s	k7c7
kryptoměnami	kryptoměna	k1gFnPc7
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
z	z	k7c2
toho	ten	k3xDgMnSc2
neplynou	plynout	k5eNaImIp3nP
žádné	žádný	k3yNgFnPc4
výhody	výhoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bude	být	k5eAaImBp3nS
pod	pod	k7c7
podobným	podobný	k2eAgInSc7d1
dohledem	dohled	k1gInSc7
jako	jako	k8xS,k8xC
v	v	k7c6
případě	případ	k1gInSc6
finančních	finanční	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
bez	bez	k7c2
jejich	jejich	k3xOp3gFnPc2
zákonných	zákonný	k2eAgFnPc2d1
garancí	garance	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystavuje	vystavovat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
riziku	riziko	k1gNnSc3
ztráty	ztráta	k1gFnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rizika	riziko	k1gNnPc1
</s>
<s>
Kvalitní	kvalitní	k2eAgNnSc1d1
šifrování	šifrování	k1gNnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
celá	celý	k2eAgFnSc1d1
bitcoinová	bitcoinový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
je	být	k5eAaImIp3nS
technologicky	technologicky	k6eAd1
vyspělá	vyspělý	k2eAgFnSc1d1
a	a	k8xC
platby	platba	k1gFnPc1
probíhají	probíhat	k5eAaImIp3nP
bez	bez	k7c2
závažných	závažný	k2eAgFnPc2d1
komplikací	komplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kryptografie	kryptografie	k1gFnSc1
Bitcoinu	Bitcoin	k1gInSc2
nebyla	být	k5eNaImAgFnS
dosud	dosud	k6eAd1
prolomena	prolomen	k2eAgFnSc1d1
<g/>
,	,	kIx,
ovšem	ovšem	k9
i	i	k9
tak	tak	k6eAd1
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
určitá	určitý	k2eAgNnPc4d1
rizika	riziko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
síť	síť	k1gFnSc1
je	on	k3xPp3gFnPc4
totiž	totiž	k9
online	onlinout	k5eAaPmIp3nS
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
teoreticky	teoreticky	k6eAd1
náchylná	náchylný	k2eAgFnSc1d1
k	k	k7c3
útokům	útok	k1gInPc3
hackerů	hacker	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
rizikem	riziko	k1gNnSc7
je	být	k5eAaImIp3nS
možná	možný	k2eAgFnSc1d1
duplikace	duplikace	k1gFnSc1
bitcoinu	bitcoin	k1gInSc2
jakožto	jakožto	k8xS
digitálního	digitální	k2eAgNnSc2d1
média	médium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
uživatel	uživatel	k1gMnSc1
může	moct	k5eAaImIp3nS
zaplatit	zaplatit	k5eAaPmF
dvakrát	dvakrát	k6eAd1
stejným	stejný	k2eAgInSc7d1
Bitcoinem	Bitcoin	k1gInSc7
(	(	kIx(
<g/>
"	"	kIx"
<g/>
double	double	k2eAgInSc4d1
spending	spending	k1gInSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozí	hrozit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
proces	proces	k1gInSc1
ověření	ověření	k1gNnSc1
plateb	platba	k1gFnPc2
proběhne	proběhnout	k5eAaPmIp3nS
ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
moment	moment	k1gInSc4
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
transakce	transakce	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
validní	validní	k2eAgInSc4d1
(	(	kIx(
<g/>
oběma	dva	k4xCgMnPc7
obchodníky	obchodník	k1gMnPc4
-	-	kIx~
v	v	k7c6
samotné	samotný	k2eAgFnSc6d1
bitcoinové	bitcoinový	k2eAgFnSc6d1
síti	síť	k1gFnSc6
bude	být	k5eAaImBp3nS
po	po	k7c6
určitém	určitý	k2eAgInSc6d1
čase	čas	k1gInSc6
validní	validní	k2eAgNnSc1d1
vždy	vždy	k6eAd1
pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
transakce	transakce	k1gFnSc1
<g/>
,	,	kIx,
proto	proto	k8xC
pro	pro	k7c4
zamezení	zamezení	k1gNnSc4
podvodu	podvod	k1gInSc2
stačí	stačit	k5eAaBmIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
obchodník	obchodník	k1gMnSc1
vždy	vždy	k6eAd1
počkal	počkat	k5eAaPmAgMnS
na	na	k7c4
ověření	ověření	k1gNnSc4
transakce	transakce	k1gFnSc2
v	v	k7c6
BTC	BTC	kA
síti	sít	k5eAaImF
alespoň	alespoň	k9
do	do	k7c2
hloubky	hloubka	k1gFnSc2
tří	tři	k4xCgInPc2
bloků	blok	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
možnou	možný	k2eAgFnSc7d1
hrozbou	hrozba	k1gFnSc7
pro	pro	k7c4
Bitcoin	Bitcoin	k1gInSc4
jsou	být	k5eAaImIp3nP
tzv.	tzv.	kA
kvantové	kvantový	k2eAgInPc4d1
počítače	počítač	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Útoky	útok	k1gInPc1
se	se	k3xPyFc4
dějí	dít	k5eAaImIp3nP,k5eAaBmIp3nP
i	i	k9
na	na	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
burzy	burza	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
byl	být	k5eAaImAgInS
objeven	objevit	k5eAaPmNgInS
kód	kód	k1gInSc1
cílený	cílený	k2eAgInSc1d1
na	na	k7c4
burzu	burza	k1gFnSc4
Gate	Gat	k1gFnSc2
<g/>
.	.	kIx.
<g/>
io	io	k?
v	v	k7c6
trackovacím	trackovací	k2eAgNnSc6d1
skriptu	skriptum	k1gNnSc6
StatCounteru	StatCounter	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
</s>
<s>
Bitcoin	Bitcoin	k1gInSc1
přináší	přinášet	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
nových	nový	k2eAgInPc2d1
přístupů	přístup	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
vzbuzují	vzbuzovat	k5eAaImIp3nP
diskuse	diskuse	k1gFnPc4
a	a	k8xC
otázky	otázka	k1gFnPc4
nad	nad	k7c7
budoucností	budoucnost	k1gFnSc7
měny	měna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Častá	častý	k2eAgFnSc1d1
tvrzení	tvrzení	k1gNnSc4
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
zabudovaná	zabudovaný	k2eAgFnSc1d1
deflace	deflace	k1gFnSc1
měny	měna	k1gFnSc2
<g/>
:	:	kIx,
kvůli	kvůli	k7c3
omezenému	omezený	k2eAgNnSc3d1
množství	množství	k1gNnSc3
peněz	peníze	k1gInPc2
bude	být	k5eAaImBp3nS
docházet	docházet	k5eAaImF
k	k	k7c3
trvalé	trvalý	k2eAgFnSc3d1
deflaci	deflace	k1gFnSc3
<g/>
,	,	kIx,
avšak	avšak	k8xC
mnoho	mnoho	k4c1
ekonomů	ekonom	k1gMnPc2
zastává	zastávat	k5eAaImIp3nS
přínos	přínos	k1gInSc4
spíše	spíše	k9
inflace	inflace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oponenty	oponent	k1gMnPc7
jsou	být	k5eAaImIp3nP
zastánci	zastánce	k1gMnPc1
deflační	deflační	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
krytí	krytí	k1gNnSc1
měny	měna	k1gFnSc2
<g/>
:	:	kIx,
hodnota	hodnota	k1gFnSc1
měny	měna	k1gFnSc2
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
spekulativní	spekulativní	k2eAgFnSc1d1
<g/>
,	,	kIx,
samotná	samotný	k2eAgFnSc1d1
měna	měna	k1gFnSc1
není	být	k5eNaImIp3nS
ničím	nic	k3yNnSc7
kryta	krýt	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Častým	častý	k2eAgInSc7d1
protinázorem	protinázor	k1gInSc7
je	být	k5eAaImIp3nS
poukázání	poukázání	k1gNnSc1
na	na	k7c4
fakt	fakt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
současné	současný	k2eAgFnPc1d1
fiat	fiat	k1gInSc4
měny	měna	k1gFnSc2
taktéž	taktéž	k?
nejsou	být	k5eNaImIp3nP
ničím	nic	k3yNnSc7
kryté	krytý	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
zneužitelnost	zneužitelnost	k1gFnSc1
pro	pro	k7c4
trestnou	trestný	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
:	:	kIx,
měna	měna	k1gFnSc1
je	být	k5eAaImIp3nS
kvůli	kvůli	k7c3
náročné	náročný	k2eAgFnSc3d1
vystopovatelnosti	vystopovatelnost	k1gFnSc3
a	a	k8xC
nemožnosti	nemožnost	k1gFnSc3
kontroly	kontrola	k1gFnSc2
vhodná	vhodný	k2eAgNnPc1d1
k	k	k7c3
trestné	trestný	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
stejnému	stejný	k2eAgInSc3d1
účelu	účel	k1gInSc3
však	však	k9
lze	lze	k6eAd1
zneužít	zneužít	k5eAaPmF
i	i	k9
běžnou	běžný	k2eAgFnSc4d1
hotovost	hotovost	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
také	také	k9
relativně	relativně	k6eAd1
anonymní	anonymní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
také	také	k9
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
komodit	komodita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
neúplná	úplný	k2eNgFnSc1d1
anonymita	anonymita	k1gFnSc1
<g/>
:	:	kIx,
neexistuje	existovat	k5eNaImIp3nS
opravdu	opravdu	k6eAd1
spolehlivé	spolehlivý	k2eAgNnSc1d1
zakrytí	zakrytí	k1gNnSc1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jaké	jaký	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
peníze	peníz	k1gInPc4
odkud	odkud	k6eAd1
přišly	přijít	k5eAaPmAgFnP
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
český	český	k2eAgInSc1d1
směnárenský	směnárenský	k2eAgInSc1d1
server	server	k1gInSc1
Bitcash	Bitcasha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
hacknut	hacknout	k5eAaPmNgInS
<g/>
,	,	kIx,
majitelům	majitel	k1gMnPc3
zmizely	zmizet	k5eAaPmAgFnP
Bitcoiny	Bitcoin	k1gInPc4
za	za	k7c4
několik	několik	k4yIc4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
si	se	k3xPyFc3
uvědomit	uvědomit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
bezpečnost	bezpečnost	k1gFnSc1
jakékoliv	jakýkoliv	k3yIgFnSc2
směnárny	směnárna	k1gFnSc2
nemá	mít	k5eNaImIp3nS
nic	nic	k3yNnSc1
společného	společný	k2eAgNnSc2d1
s	s	k7c7
bezpečnostní	bezpečnostní	k2eAgFnSc7d1
samotného	samotný	k2eAgMnSc4d1
Bitcoinu	Bitcoina	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
majitel	majitel	k1gMnSc1
předá	předat	k5eAaPmIp3nS
své	svůj	k3xOyFgFnPc4
privátní	privátní	k2eAgFnPc4d1
klíče	klíč	k1gInSc2
třetí	třetí	k4xOgFnSc3
straně	strana	k1gFnSc3
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
riskuje	riskovat	k5eAaBmIp3nS
jejich	jejich	k3xOp3gFnSc4
ztrátu	ztráta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
krokem	krok	k1gInSc7
totiž	totiž	k9
vymění	vyměnit	k5eAaPmIp3nS
bezpečnost	bezpečnost	k1gFnSc4
bitcoinového	bitcoinový	k2eAgInSc2d1
protokolu	protokol	k1gInSc2
za	za	k7c4
důvěru	důvěra	k1gFnSc4
v	v	k7c4
poctivost	poctivost	k1gFnSc4
oné	onen	k3xDgFnSc2
třetí	třetí	k4xOgFnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
své	svůj	k3xOyFgMnPc4
Bitcoiny	Bitcoin	k1gMnPc4
svěřil	svěřit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoin	k2eAgInSc1d1
symbol	symbol	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoin	k2eAgInSc1d1
unit	unit	k2eAgInSc1d1
converter	converter	k1gInSc1
<g/>
↑	↑	k?
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
originálního	originální	k2eAgInSc2d1
textu	text	k1gInSc2
"	"	kIx"
<g/>
Bitcoin	Bitcoin	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Peer-to-Peer	Peer-to-Peer	k1gInSc1
Electronic	Electronice	k1gFnPc2
Cash	cash	k1gFnPc2
System	Syst	k1gInSc7
<g/>
"	"	kIx"
jehož	jehož	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
Satoshi	Satoshi	k1gNnPc4
Nakamoto	Nakamota	k1gFnSc5
<g/>
,	,	kIx,
vynálezce	vynálezce	k1gMnSc1
bitcoinu	bitcoin	k1gInSc2
<g/>
↑	↑	k?
Joshua	Joshu	k2eAgFnSc1d1
Davis	Davis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Crypto-Currency	Crypto-Currenca	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
New	New	k1gMnSc1
Yorker	Yorker	k1gMnSc1
<g/>
,	,	kIx,
10	#num#	k4
October	October	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://web.archive.org/web/20180615010015/http://www.businessinsider.com/bitcoin-history-cryptocurrency-satoshi-nakamoto-2017-12	https://web.archive.org/web/20180615010015/http://www.businessinsider.com/bitcoin-history-cryptocurrency-satoshi-nakamoto-2017-12	k4
-	-	kIx~
Everything	Everything	k1gInSc1
you	you	k?
need	need	k1gInSc1
to	ten	k3xDgNnSc1
know	know	k?
about	about	k1gInSc1
Bitcoin	Bitcoin	k1gInSc1
<g/>
,	,	kIx,
its	its	k?
mysterious	mysterious	k1gInSc1
origins	origins	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
the	the	k?
many	mana	k1gFnSc2
alleged	alleged	k1gMnSc1
identities	identities	k1gMnSc1
of	of	k?
its	its	k?
creator	creator	k1gInSc1
<g/>
↑	↑	k?
Forbes	forbes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
Craig	Craig	k1gMnSc1
Wright	Wright	k1gMnSc1
Claims	Claims	k1gInSc4
He	he	k0
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Bitcoin	Bitcoin	k1gInSc1
Creator	Creator	k1gInSc4
Satoshi	Satosh	k1gFnSc2
--	--	k?
Experts	Experts	k1gInSc1
Fear	Feara	k1gFnPc2
An	An	k1gFnPc2
Epic	Epic	k1gInSc1
Scam	Scam	k1gMnSc1
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
vizionář	vizionář	k1gMnSc1
Musk	Musk	k1gMnSc1
popřel	popřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vytvořil	vytvořit	k5eAaPmAgInS
kryptoměnu	kryptoměn	k2eAgFnSc4d1
bitcoin	bitcoin	k1gInSc1
<g/>
.	.	kIx.
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
17	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
překlad	překlad	k1gInSc4
Bit	bit	k2eAgInSc1d1
gold	gold	k1gInSc1
(	(	kIx(
<g/>
Digitální	digitální	k2eAgNnSc1d1
zlato	zlato	k1gNnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
Funguje	fungovat	k5eAaImIp3nS
Bitcoin	Bitcoin	k2eAgInSc1d1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.weidai.com/bmoney.txt1	http://www.weidai.com/bmoney.txt1	k4
2	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoin	k2eAgInSc1d1
client	client	k1gInSc1
software	software	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Konference	konference	k1gFnSc1
v	v	k7c4
New	New	k1gFnSc4
Yorku	York	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
19	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Konference	konference	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
1	#num#	k4
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Lehce	lehko	k6eAd1
neformální	formální	k2eNgFnSc4d1
reportáž	reportáž	k1gFnSc4
z	z	k7c2
Bitcoin	Bitcoina	k1gFnPc2
Conference	Conferenec	k1gInSc2
2011	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoina	k1gFnPc2
konferencia	konferencia	k1gFnSc1
Londýn	Londýn	k1gInSc1
2012	#num#	k4
(	(	kIx(
<g/>
reportáž	reportáž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gMnPc1
Two	Two	k1gFnPc2
Bitcoin	Bitcoin	k2eAgInSc4d1
Conferences	Conferences	k1gInSc4
of	of	k?
2013	#num#	k4
(	(	kIx(
<g/>
BitcoinMagazine	BitcoinMagazin	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoin	k2eAgInSc4d1
Foundation	Foundation	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Vznikla	vzniknout	k5eAaPmAgFnS
Bitcoin	Bitcoin	k2eAgInSc4d1
Foundation	Foundation	k1gInSc4
<g/>
1	#num#	k4
2	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Block	Block	k1gInSc1
chain	chain	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoin	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Block	Block	k1gInSc1
Explorer	Explorer	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
23	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
https://phys.org/news/2018-06-social-media-powerful-silent-majority.html	https://phys.org/news/2018-06-social-media-powerful-silent-majority.html	k1gMnSc1
-	-	kIx~
How	How	k1gMnSc1
social	social	k1gMnSc1
media	medium	k1gNnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
powerful	powerfula	k1gFnPc2
'	'	kIx"
<g/>
silent	silent	k1gInSc1
majority	majorita	k1gFnSc2
<g/>
'	'	kIx"
moves	moves	k1gMnSc1
Bitcoin	Bitcoin	k1gMnSc1
prices	prices	k1gMnSc1
<g/>
1	#num#	k4
2	#num#	k4
Decentralizovaná	decentralizovaný	k2eAgFnSc1d1
kryptoměna	kryptoměn	k2eAgFnSc1d1
Bitcoin	Bitcoin	k1gInSc1
(	(	kIx(
<g/>
abclinuxu	abclinux	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
How	How	k1gMnSc1
long	long	k1gMnSc1
will	will	k1gMnSc1
it	it	k?
take	takat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
generate	generat	k1gInSc5
all	all	k?
the	the	k?
coins	coins	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoin	k2eAgInSc1d1
division	division	k1gInSc1
limit	limit	k1gInSc1
<g/>
↑	↑	k?
1	#num#	k4
2	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Attacker	Attacker	k1gInSc1
has	hasit	k5eAaImRp2nS
a	a	k8xC
lot	lot	k1gInSc1
of	of	k?
computing	computing	k1gInSc1
power	power	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
BitcoinWatch	BitcoinWatch	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Top	topit	k5eAaImRp2nS
500	#num#	k4
Supercomputing	Supercomputing	k1gInSc1
Sites	Sites	k1gInSc4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
BitCoin	BitCoin	k2eAgInSc1d1
Vending	Vending	k1gInSc1
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoin	k2eAgInSc1d1
snack	snack	k1gInSc1
machine	machinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
fast	fast	k5eAaPmF
transaction	transaction	k1gInSc4
problem	problo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
What	What	k1gInSc1
are	ar	k1gInSc5
bitcoin	bitcoin	k2eAgInSc4d1
“	“	k?
<g/>
confirmations	confirmations	k1gInSc4
<g/>
”	”	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Difficulty	Difficult	k1gInPc1
change	change	k1gFnSc2
<g/>
↑	↑	k?
Technologie	technologie	k1gFnSc2
těžení	těžení	k1gNnSc2
bitcoinů	bitcoin	k1gInPc2
(	(	kIx(
<g/>
blog	blog	k1gMnSc1
<g/>
.	.	kIx.
<g/>
bitcoin	bitcoin	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Technical	Technical	k1gFnSc1
background	background	k1gInSc1
of	of	k?
version	version	k1gInSc4
1	#num#	k4
Bitcoin	Bitcoin	k2eAgInSc4d1
addresses	addresses	k1gInSc4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
But	But	k1gFnSc1
if	if	k?
no	no	k9
more	mor	k1gInSc5
coins	coins	k1gInSc1
are	ar	k1gInSc5
generated	generated	k1gInSc1
<g/>
,	,	kIx,
what	what	k2eAgInSc1d1
happens	happens	k1gInSc1
when	whena	k1gFnPc2
Bitcoins	Bitcoinsa	k1gFnPc2
are	ar	k1gInSc5
lost	lost	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Won	won	k1gInSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
that	that	k1gInSc1
be	be	k?
a	a	k8xC
problem	probl	k1gMnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
HEGEDÜŠ	HEGEDÜŠ	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
nejrychlejší	rychlý	k2eAgFnSc1d3
nebo	nebo	k8xC
nejvtipnější	vtipný	k2eAgFnSc1d3
<g/>
?	?	kIx.
</s>
<s desamb="1">
Šampioni	šampion	k1gMnPc1
ze	z	k7c2
světa	svět	k1gInSc2
kryptoměn	kryptoměn	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technet	Technet	k1gInSc1
CZ	CZ	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoin	k2eAgInSc1d1
Mining	Mining	k1gInSc1
Calculator	Calculator	k1gMnSc1
<g/>
↑	↑	k?
České	český	k2eAgNnSc1d1
těžařské	těžařský	k2eAgNnSc1d1
uskupení	uskupení	k1gNnSc1
mining	mining	k1gInSc1
<g/>
.	.	kIx.
<g/>
bitcoin	bitcoin	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Záznam	záznam	k1gInSc1
z	z	k7c2
IRC	IRC	kA
bitcoin	bitcoina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
.	.	kIx.
gribble	gribble	k6eAd1
<g/>
.	.	kIx.
<g/>
dreamhosters	dreamhosters	k6eAd1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Hashrate	Hashrat	k1gInSc5
distribution	distribution	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
19	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
ATNET	ATNET	kA
GLOSSARY	GLOSSARY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Segwit	Segwit	k1gInSc1
(	(	kIx(
<g/>
vs	vs	k?
Legacy	Legaca	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AltcoinTrading	AltcoinTrading	k1gInSc1
<g/>
.	.	kIx.
<g/>
NET	NET	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATNET	ATNET	kA
<g/>
,	,	kIx,
2020-08-26	2020-08-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Roger	Roger	k1gMnSc1
Ver	Ver	k1gMnSc1
Declares	Declares	k1gMnSc1
Bitcoin	Bitcoin	k1gMnSc1
Cash	cash	k1gFnSc2
to	ten	k3xDgNnSc4
Be	Be	k1gFnSc7
True	Tru	k1gFnSc2
Bitcoin	Bitcoin	k1gInSc1
<g/>
,	,	kIx,
Market	market	k1gInSc1
Forces	Forces	k1gMnSc1
Bring	Bring	k1gMnSc1
More	mor	k1gInSc5
Attention	Attention	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cointelegraph	Cointelegrapha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bitcoin	Bitcoin	k1gInSc1
<g/>
:	:	kIx,
transakční	transakční	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
zlevnily	zlevnit	k5eAaPmAgInP
-	-	kIx~
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
WOLF	Wolf	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoin	Bitcoin	k1gInSc1
pod	pod	k7c7
útokem	útok	k1gInSc7
a	a	k8xC
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
zátěžovém	zátěžový	k2eAgInSc6d1
testu	test	k1gInSc6
aneb	aneb	k?
Špinavá	špinavý	k2eAgFnSc1d1
hra	hra	k1gFnSc1
Rogera	Rogera	k1gFnSc1
Vera	Vera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lupa	lupa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
702	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Grafy	graf	k1gInPc1
vývoje	vývoj	k1gInSc2
cen	cena	k1gFnPc2
bitcoinu	bitcoinout	k5eAaPmIp1nS
Vývoj	vývoj	k1gInSc1
hodnoty	hodnota	k1gFnSc2
bitcoinu	bitcoinout	k5eAaPmIp1nS
a	a	k8xC
přepočet	přepočet	k1gInSc1
ceny	cena	k1gFnSc2
bitcoinu	bitcoinout	k5eAaPmIp1nS
do	do	k7c2
různých	různý	k2eAgFnPc2d1
měn	měna	k1gFnPc2
včetně	včetně	k7c2
české	český	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
↑	↑	k?
Reportáž	reportáž	k1gFnSc1
o	o	k7c6
bitcoinu	bitcoino	k1gNnSc6
v	v	k7c6
pořadu	pořad	k1gInSc6
Horizont	horizont	k1gInSc1
na	na	k7c6
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bitcoin	Bitcoin	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
'	'	kIx"
<g/>
s	s	k7c7
Value	Value	k1gFnSc7
is	is	k?
Decentralization	Decentralization	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
15	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Analýza	analýza	k1gFnSc1
rizik	riziko	k1gNnPc2
hodnot	hodnota	k1gFnPc2
kryptoměn	kryptoměn	k2eAgMnSc1d1
<g/>
.	.	kIx.
kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-06-22	2017-06-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pomoc	pomoc	k1gFnSc1
s	s	k7c7
Bitcoin	Bitcoina	k1gFnPc2
(	(	kIx(
<g/>
fórum	fórum	k1gNnSc1
na	na	k7c4
webtrhu	webtrha	k1gFnSc4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Coinbase	Coinbasa	k1gFnSc6
má	mít	k5eAaImIp3nS
více	hodně	k6eAd2
účtů	účet	k1gInPc2
než	než	k8xS
brokerský	brokerský	k2eAgInSc1d1
dům	dům	k1gInSc1
Charles	Charles	k1gMnSc1
Schwab	Schwab	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
cca	cca	kA
13,3	13,3	k4
miliónu	milión	k4xCgInSc2
a	a	k8xC
rychle	rychle	k6eAd1
to	ten	k3xDgNnSc1
roste	růst	k5eAaImIp3nS
<g/>
.	.	kIx.
kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
vez	vézt	k5eAaImRp2nS
<g/>
;	;	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krize	krize	k1gFnSc1
na	na	k7c6
Kypru	Kypr	k1gInSc6
zvedla	zvednout	k5eAaPmAgFnS
hodnotu	hodnota	k1gFnSc4
kybernetické	kybernetický	k2eAgFnSc2d1
měny	měna	k1gFnSc2
bitcoin	bitcoina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
strachu	strach	k1gInSc3
z	z	k7c2
bank	banka	k1gFnPc2
<g/>
.	.	kIx.
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2013-04-01	2013-04-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
7693	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Coinbase	Coinbasa	k1gFnSc6
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
nových	nový	k2eAgInPc2d1
bitcoinových	bitcoinův	k2eAgInPc2d1
účtů	účet	k1gInPc2
exponenciálně	exponenciálně	k6eAd1
roste	růst	k5eAaImIp3nS
<g/>
↑	↑	k?
Cibule	cibule	k1gFnSc2
nebo	nebo	k8xC
bitcoiny	bitcoina	k1gFnSc2
Porovnání	porovnání	k1gNnSc1
průběhu	průběh	k1gInSc2
nejznámějších	známý	k2eAgFnPc2d3
historických	historický	k2eAgFnPc2d1
investičních	investiční	k2eAgFnPc2d1
bublin	bublina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Crypto	Crypto	k1gNnSc1
Hedge	Hedg	k1gInSc2
Fund	fund	k1gInSc1
Opens	Opens	k1gInSc1
Early	earl	k1gMnPc4
as	as	k9
Bulls	Bullsa	k1gFnPc2
Rush	Rush	k1gMnSc1
to	ten	k3xDgNnSc4
Buy	Buy	k1gMnSc1
the	the	k?
Dip	Dip	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomberg	Bloomberg	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kurzycz	Kurzycz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
probíhaly	probíhat	k5eAaImAgFnP
cenové	cenový	k2eAgFnPc4d1
bubliny	bublina	k1gFnPc4
na	na	k7c4
Bitcoinu	Bitcoina	k1gFnSc4
<g/>
.	.	kIx.
kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AliaWeb	AliaWba	k1gFnPc2
<g/>
,	,	kIx,
2017-12-12	2017-12-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1801	#num#	k4
<g/>
-	-	kIx~
<g/>
8688	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bitcoin	Bitcoin	k1gInSc1
halving	halving	k1gInSc1
2020	#num#	k4
–	–	k?
co	co	k9
je	být	k5eAaImIp3nS
půlení	půlení	k1gNnSc4
a	a	k8xC
kam	kam	k6eAd1
půjde	jít	k5eAaImIp3nS
cena	cena	k1gFnSc1
kryptoměny	kryptoměn	k2eAgInPc4d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TRAXLER	TRAXLER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoin	Bitcoina	k1gFnPc2
je	být	k5eAaImIp3nS
ukázková	ukázkový	k2eAgFnSc1d1
bublina	bublina	k1gFnSc1
<g/>
.	.	kIx.
penize	penize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Bitcoin	Bitcoina	k1gFnPc2
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
bublina	bublina	k1gFnSc1
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
vlivný	vlivný	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pátek	pátek	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deutsche	Deutsch	k1gInSc2
Bank	bank	k1gInSc1
varuje	varovat	k5eAaImIp3nS
před	před	k7c7
investicemi	investice	k1gFnPc7
do	do	k7c2
bitcoinu	bitcoin	k1gInSc2
<g/>
.	.	kIx.
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
DOČEKAL	Dočekal	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoiny	Bitcoin	k1gInPc4
energii	energie	k1gFnSc4
nešetří	šetřit	k5eNaImIp3nS
<g/>
,	,	kIx,
seriózní	seriózní	k2eAgInPc1d1
weby	web	k1gInPc1
šmírují	šmírovat	k5eAaImIp3nP
a	a	k8xC
Německo	Německo	k1gNnSc1
zakázalo	zakázat	k5eAaPmAgNnS
chytré	chytrý	k2eAgFnPc4d1
hodinky	hodinka	k1gFnPc4
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lupa	lupa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
702	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MIX	mix	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoin	Bitcoin	k2eAgInSc1d1
mining	mining	k1gInSc1
consumes	consumesa	k1gFnPc2
more	mor	k1gInSc5
electricity	electricita	k1gFnPc1
than	than	k1gNnSc1
20	#num#	k4
<g/>
+	+	kIx~
European	European	k1gMnSc1
countries	countries	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Next	Next	k1gMnSc1
Web	web	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nov	nov	k1gInSc1
23	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
-	-	kIx~
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
kop	kopa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžba	těžba	k1gFnSc1
bitcoinu	bitcoin	k1gInSc2
spotřebuje	spotřebovat	k5eAaPmIp3nS
za	za	k7c4
rok	rok	k1gInSc4
více	hodně	k6eAd2
energie	energie	k1gFnSc1
než	než	k8xS
třetina	třetina	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
jeho	jeho	k3xOp3gFnSc1
cena	cena	k1gFnSc1
letí	letět	k5eAaImIp3nS
nahoru	nahoru	k6eAd1
<g/>
.	.	kIx.
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
AANTONOP	AANTONOP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoin	Bitcoin	k1gInSc1
Q	Q	kA
<g/>
&	&	k?
<g/>
A	a	k9
<g/>
:	:	kIx,
Energy	Energ	k1gInPc4
consumption	consumption	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bitcoin	Bitcoin	k2eAgInSc4d1
Mining	Mining	k1gInSc4
Network	network	k1gInSc1
Report	report	k1gInSc1
June	jun	k1gMnSc5
2019	#num#	k4
|	|	kIx~
Research	Researcha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CoinShares	CoinShares	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bitcoin	Bitcoin	k1gInSc1
energy	energ	k1gInPc1
use	usus	k1gInSc5
-	-	kIx~
mined	mined	k1gInSc1
the	the	k?
gap	gap	k?
–	–	k?
Analysis	Analysis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEA	IEA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
TOMAS-SOUKUP	TOMAS-SOUKUP	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finex	Finex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-25	2019-08-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Těžba	těžba	k1gFnSc1
bitcoinu	bitcoin	k1gInSc2
značně	značně	k6eAd1
ztrácí	ztrácet	k5eAaImIp3nS
na	na	k7c6
výnosnosti	výnosnost	k1gFnSc6
-	-	kIx~
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
http://www.root.cz/zpravicky/apache-software-foundation-prijima-sponzorske-dary-v-bitcoinech/	http://www.root.cz/zpravicky/apache-software-foundation-prijima-sponzorske-dary-v-bitcoinech/	k?
<g/>
↑	↑	k?
GRUWELL	GRUWELL	kA
<g/>
,	,	kIx,
Lisa	Lisa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikimedia	Wikimedium	k1gNnSc2
Foundation	Foundation	k1gInSc1
Now	Now	k1gFnSc2
Accepts	Acceptsa	k1gFnPc2
Bitcoin	Bitcoin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikimedia	Wikimedium	k1gNnPc1
Blog	Bloga	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikimedia	Wikimedium	k1gNnSc2
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
2014-07-30	2014-07-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VÁCLAVÍK	Václavík	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoin	Bitcoin	k1gInSc1
v	v	k7c6
Alze	Alze	k1gFnSc6
je	být	k5eAaImIp3nS
prý	prý	k9
úspěšný	úspěšný	k2eAgMnSc1d1
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
už	už	k9
utratili	utratit	k5eAaPmAgMnP
miliony	milion	k4xCgInPc4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
cnews	cnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2017-06-16	2017-06-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bitcoin	Bitcoin	k1gInSc1
už	už	k6eAd1
není	být	k5eNaImIp3nS
jedinou	jediný	k2eAgFnSc4d1
kryptoměnou	kryptoměný	k2eAgFnSc4d1
podporovanou	podporovaný	k2eAgFnSc4d1
Alza	Alza	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
ČEPEROVÁ	ČEPEROVÁ	kA
<g/>
,	,	kIx,
Klára	Klára	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rus	Rus	k1gMnSc1
koupil	koupit	k5eAaPmAgMnS
v	v	k7c6
Praze	Praha	k1gFnSc6
byt	byt	k1gInSc4
za	za	k7c4
35	#num#	k4
bitcoinů	bitcoin	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
teprve	teprve	k6eAd1
o	o	k7c4
druhou	druhý	k4xOgFnSc4
nemovitost	nemovitost	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
někdo	někdo	k3yInSc1
zaplatil	zaplatit	k5eAaPmAgMnS
virtuální	virtuální	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
<g/>
.	.	kIx.
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2017-12-20	2017-12-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VÁVRA	Vávra	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestovatelská	cestovatelský	k2eAgFnSc1d1
platební	platební	k2eAgFnSc1d1
karta	karta	k1gFnSc1
Revolut	Revolut	k1gInSc1
spouští	spouštět	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
kryptoměn	kryptoměn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
E15	E15	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
•	•	k?
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
8991	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://e-svet.e15.cz/it-byznys/bitcoiny-jsou-penize-rozhodlo-nemecko-1014811	http://e-svet.e15.cz/it-byznys/bitcoiny-jsou-penize-rozhodlo-nemecko-1014811	k4
-	-	kIx~
Bitcoiny	Bitcoina	k1gFnPc1
jsou	být	k5eAaImIp3nP
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
rozhodlo	rozhodnout	k5eAaPmAgNnS
Německo	Německo	k1gNnSc1
<g/>
↑	↑	k?
Zdanění	zdanění	k1gNnSc1
bitcoinu	bitcoinout	k5eAaPmIp1nS
v	v	k7c6
Německu	Německo	k1gNnSc6
-	-	kIx~
německy	německy	k6eAd1
https://blog.bitcoin.de/steuern-fuer-bitcoins	https://blog.bitcoin.de/steuern-fuer-bitcoins	k6eAd1
<g/>
↑	↑	k?
Bitcoin	Bitcoina	k1gFnPc2
a	a	k8xC
daně	daň	k1gFnSc2
-	-	kIx~
jak	jak	k8xS,k8xC
zdanit	zdanit	k5eAaPmF
zisky	zisk	k1gInPc4
-	-	kIx~
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
ONUFEROVÁ	ONUFEROVÁ	kA
<g/>
,	,	kIx,
Marianna	Marianno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kažimír	Kažimír	k1gMnSc1
chce	chtít	k5eAaImIp3nS
začať	začatit	k5eAaPmRp2nS,k5eAaImRp2nS
zdaňovať	zdaňovatit	k5eAaPmRp2nS,k5eAaImRp2nS
bitcoin	bitcoin	k2eAgInSc4d1
aj	aj	kA
ďalšie	ďalšie	k1gFnSc2
kryptomeny	kryptomit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denník	denník	k1gInSc1
N.	N.	kA
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Baidu	Baid	k1gInSc2
Stops	Stops	k1gInSc1
Accepting	Accepting	k1gInSc1
Bitcoins	Bitcoins	k1gInSc1
After	After	k1gInSc1
China	China	k1gFnSc1
Ban	Ban	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomberg	Bloomberg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
7	#num#	k4
December	December	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
December	December	k1gInSc1
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Incestiční	Incestiční	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
,	,	kIx,
Bitcoin	Bitcoin	k1gInSc1
byl	být	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
označen	označit	k5eAaPmNgInS
za	za	k7c4
komoditu	komodita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrátí	vrátit	k5eAaPmIp3nS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
dřívější	dřívější	k2eAgFnSc1d1
sláva	sláva	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
WOLF	Wolf	k1gMnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
regulují	regulovat	k5eAaImIp3nP
bitcoin	bitcoin	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
19.9	19.9	k4
<g/>
.2015	.2015	k4
<g/>
,	,	kIx,
s.	s.	k?
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.investicniweb.cz/2013/9/14/george-selgin-centralni-banky-poslapaly-veskere-zasady-prisel-cas-digitalnich-men/%5B%5D	http://www.investicniweb.cz/2013/9/14/george-selgin-centralni-banky-poslapaly-veskere-zasady-prisel-cas-digitalnich-men/%5B%5D	k4
-	-	kIx~
George	Georg	k1gInSc2
Selgin	Selgin	k2eAgMnSc1d1
<g/>
:	:	kIx,
Centrální	centrální	k2eAgFnPc1d1
banky	banka	k1gFnPc1
pošlapaly	pošlapat	k5eAaPmAgFnP
veškeré	veškerý	k3xTgFnPc1
zásady	zásada	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišel	přijít	k5eAaPmAgMnS
čas	čas	k1gInSc4
digitálních	digitální	k2eAgFnPc2d1
měn	měna	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
Kryptoměny	Kryptoměn	k2eAgInPc1d1
-	-	kIx~
hrozba	hrozba	k1gFnSc1
regulace	regulace	k1gFnSc2
sráží	srážet	k5eAaImIp3nS
kurz	kurz	k1gInSc1
-	-	kIx~
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
VÁVRA	Vávra	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chcete	chtít	k5eAaImIp2nP
nakoupit	nakoupit	k5eAaPmF
bitcoiny	bitcoina	k1gFnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
banky	banka	k1gFnSc2
vám	vy	k3xPp2nPc3
transakci	transakce	k1gFnSc6
zablokují	zablokovat	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
E15	E15	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CN	CN	kA
Invest	Invest	k1gMnSc1
<g/>
,	,	kIx,
2017-02-20	2017-02-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
8991	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
EDITORIAL	EDITORIAL	kA
<g/>
,	,	kIx,
Reuters	Reuters	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ECB	ECB	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Draghi	Draghi	k1gNnSc7
says	saysa	k1gFnPc2
not	nota	k1gFnPc2
his	his	k1gNnSc2
job	job	k?
to	ten	k3xDgNnSc1
regulate	regulat	k1gInSc5
Bitcoin	Bitcoin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.K.	U.K.	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
ENGELMANNOVÁ	ENGELMANNOVÁ	kA
<g/>
,	,	kIx,
Nikola	Nikola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finex	Finex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-09	2019-08-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WOLF	Wolf	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgNnSc1d1
„	„	k?
<g/>
kladivo	kladivo	k1gNnSc1
na	na	k7c4
kryptoměny	kryptoměn	k2eAgFnPc4d1
<g/>
“	“	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
má	mít	k5eAaImIp3nS
změnit	změnit	k5eAaPmF
chystaný	chystaný	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lupa	lupa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bitcoin	Bitcoin	k1gInSc1
<g/>
:	:	kIx,
Praktický	praktický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
&	&	k?
odpovědi	odpověď	k1gFnSc2
na	na	k7c4
13	#num#	k4
zásadních	zásadní	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
-	-	kIx~
LYNX	LYNX	kA
broker	broker	k1gMnSc1
<g/>
↑	↑	k?
Postřehy	postřeh	k1gInPc1
z	z	k7c2
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
:	:	kIx,
batmanův	batmanův	k2eAgInSc1d1
sonar	sonar	k1gInSc1
v	v	k7c6
kapse	kapsa	k1gFnSc6
<g/>
.	.	kIx.
root	root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Is	Is	k1gFnSc1
the	the	k?
cryptocurrency	cryptocurrenca	k1gFnSc2
Bitcoin	Bitcoin	k1gMnSc1
a	a	k8xC
good	good	k1gMnSc1
idea	idea	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Hacker	hacker	k1gMnSc1
News	Newsa	k1gFnPc2
discussion	discussion	k1gInSc4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Deflationary	Deflationara	k1gFnSc2
spiral	spirat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
↑	↑	k?
Nesmyslné	smyslný	k2eNgNnSc1d1
strašidlo	strašidlo	k1gNnSc1
deflace	deflace	k1gFnSc2
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Why	Why	k1gFnSc1
Mainstream	Mainstream	k1gInSc4
Economists	Economists	k1gInSc1
Lie	Lie	k1gFnSc2
About	About	k2eAgInSc1d1
Deflation	Deflation	k1gInSc1
<g/>
↑	↑	k?
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
21.8	21.8	k4
<g/>
.2017	.2017	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kokain	kokain	k1gInSc1
přes	přes	k7c4
internet	internet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijde	přijít	k5eAaPmIp3nS
vám	vy	k3xPp2nPc3
domů	domů	k6eAd1
poštou	pošta	k1gFnSc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Anonymity	anonymita	k1gFnPc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
An	An	k1gFnSc1
Analysis	Analysis	k1gFnSc2
of	of	k?
Anonymity	anonymita	k1gFnSc2
in	in	k?
the	the	k?
Bitcoin	Bitcoina	k1gFnPc2
System	Syst	k1gMnSc7
<g/>
↑	↑	k?
http://phys.org/news/2016-01-bitcoin-reveals-false-beliefs-ease.html	http://phys.org/news/2016-01-bitcoin-reveals-false-beliefs-ease.html	k1gMnSc1
-	-	kIx~
Bitcoin	Bitcoin	k1gMnSc1
study	stud	k1gInPc4
reveals	reveals	k6eAd1
false	false	k6eAd1
beliefs	beliefs	k6eAd1
on	on	k3xPp3gMnSc1
ease	easat	k5eAaPmIp3nS
of	of	k?
use	usus	k1gInSc5
and	and	k?
privacy	privaca	k1gFnSc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Mastering	Mastering	k1gInSc1
Bitcoin	Bitcoin	k2eAgInSc1d1
-	-	kIx~
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
knihy	kniha	k1gFnSc2
https://web.archive.org/web/20171115204006/https://www.bitcoinbook.info/translations/cs/book.pdf	https://web.archive.org/web/20171115204006/https://www.bitcoinbook.info/translations/cs/book.pdf	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Blockchain	Blockchain	k1gMnSc1
</s>
<s>
Bitcoin	Bitcoin	k1gInSc1
arbitráž	arbitráž	k1gFnSc1
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
standard	standard	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bitcoin	Bitcoina	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
bitcoin	bitcoina	k1gFnPc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Bitcoin	Bitcoina	k1gFnPc2
pokořil	pokořit	k5eAaPmAgInS
další	další	k2eAgInSc1d1
rekord	rekord	k1gInSc1
<g/>
:	:	kIx,
3400	#num#	k4
US	US	kA
<g/>
$	$	kIx~
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
(	(	kIx(
<g/>
resp.	resp.	kA
stránka	stránka	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
bitcoin	bitcoin	k1gInSc1
objevil	objevit	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kryptoměny	Kryptoměn	k2eAgInPc1d1
založené	založený	k2eAgInPc1d1
na	na	k7c4
SHA-256	SHA-256	k1gFnSc4
</s>
<s>
Bitcoin	Bitcoin	k1gMnSc1
•	•	k?
Bitcoin	Bitcoin	k1gMnSc1
cash	cash	k1gFnSc2
•	•	k?
Peercoin	Peercoin	k1gInSc1
•	•	k?
Namecoin	Namecoina	k1gFnPc2
založené	založený	k2eAgFnPc4d1
na	na	k7c6
Scryptu	Scrypt	k1gInSc6
</s>
<s>
Auroracoin	Auroracoin	k1gMnSc1
•	•	k?
Dogecoin	Dogecoin	k1gMnSc1
•	•	k?
Litecoin	Litecoin	k1gMnSc1
•	•	k?
PotCoin	PotCoin	k1gMnSc1
založené	založený	k2eAgInPc4d1
na	na	k7c4
Zerocoinu	Zerocoina	k1gFnSc4
</s>
<s>
Zcoin	Zcoin	k1gMnSc1
•	•	k?
ZeroVert	ZeroVert	k1gMnSc1
•	•	k?
Anoncoin	Anoncoin	k1gMnSc1
•	•	k?
SmartCash	SmartCash	k1gMnSc1
•	•	k?
PIVX	PIVX	kA
•	•	k?
Zcash	Zcash	k1gInSc1
•	•	k?
Komodo	komoda	k1gFnSc5
založené	založený	k2eAgInPc1d1
na	na	k7c4
CryptoNote	CryptoNot	k1gInSc5
</s>
<s>
Bytecoin	Bytecoin	k1gMnSc1
•	•	k?
Monero	Monero	k1gNnSc4
•	•	k?
DigitalNote	DigitalNot	k1gInSc5
•	•	k?
Boolberry	Boolberr	k1gInPc1
založené	založený	k2eAgInPc1d1
na	na	k7c4
Ethash	Ethash	k1gInSc4
</s>
<s>
Ethereum	Ethereum	k1gInSc1
•	•	k?
Ethereum	Ethereum	k1gInSc1
Classic	Classic	k1gMnSc1
•	•	k?
Ubiq	Ubiq	k1gMnSc1
Jiné	jiná	k1gFnSc2
proof-of-work	proof-of-work	k1gInSc1
skripty	skript	k1gInPc4
</s>
<s>
Dash	Dash	k1gMnSc1
•	•	k?
Primecoin	Primecoin	k1gMnSc1
•	•	k?
Digibyte	Digibyt	k1gInSc5
Bez	bez	k1gInSc4
proof-of-work	proof-of-work	k1gInSc1
skriptu	skript	k1gInSc2
</s>
<s>
BlackCoin	BlackCoin	k1gMnSc1
•	•	k?
Burstcoin	Burstcoin	k1gMnSc1
•	•	k?
Counterparty	Counterparta	k1gFnSc2
•	•	k?
Enigma	enigma	k1gFnSc1
•	•	k?
FunFair	FunFair	k1gMnSc1
•	•	k?
Gridcoin	Gridcoin	k1gMnSc1
•	•	k?
Lisk	Lisk	k1gMnSc1
•	•	k?
Melonport	Melonport	k1gInSc1
•	•	k?
NEM	NEM	kA
•	•	k?
NEO	NEO	kA
•	•	k?
Nxt	Nxt	k1gMnSc1
•	•	k?
OmiseGO	OmiseGO	k1gMnSc1
•	•	k?
Polkadot	Polkadot	k1gMnSc1
•	•	k?
Qtum	Qtum	k1gMnSc1
•	•	k?
RChain	RChain	k1gMnSc1
•	•	k?
Ripple	Ripple	k1gMnSc1
•	•	k?
Simple	Simple	k1gMnSc1
Token	Token	k2eAgMnSc1d1
•	•	k?
Stellar	Stellar	k1gMnSc1
•	•	k?
Shadow	Shadow	k1gMnSc1
Technologie	technologie	k1gFnSc2
</s>
<s>
Blockchain	Blockchain	k2eAgInSc1d1
•	•	k?
Proof-of-stake	Proof-of-stake	k1gInSc1
•	•	k?
Proof-of-work	Proof-of-work	k1gInSc1
system	syst	k1gInSc7
•	•	k?
Zerocash	Zerocash	k1gInSc1
•	•	k?
Zerocoin	Zerocoin	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Anonymní	anonymní	k2eAgFnSc1d1
internetové	internetový	k2eAgNnSc4d1
bankovnictví	bankovnictví	k1gNnSc4
•	•	k?
Kryptoanarchismus	Kryptoanarchismus	k1gInSc1
•	•	k?
Digitální	digitální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Digital	Digital	kA
currency	currenca	k1gFnPc1
exchanger	exchanger	k1gMnSc1
•	•	k?
Double-spending	Double-spending	k1gInSc1
•	•	k?
Virtuální	virtuální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1041888279	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2015000075	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2015000075	#num#	k4
</s>
