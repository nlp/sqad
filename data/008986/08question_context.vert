<s>
Pluto	plut	k2eAgNnSc4d1	Pluto
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgNnSc4d1	oficiální
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
134340	[number]	k4	134340
<g/>
)	)	kIx)	)
Pluto	Pluto	k1gNnSc4	Pluto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
a	a	k8xC	a
po	po	k7c6	po
Eris	Eris	k1gFnSc1	Eris
druhou	druhý	k4xOgFnSc7	druhý
nejhmotnější	hmotný	k2eAgFnSc7d3	nejhmotnější
známou	známý	k2eAgFnSc7d1	známá
trpasličí	trpasličí	k2eAgFnSc7d1	trpasličí
planetou	planeta	k1gFnSc7	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
deváté	devátý	k4xOgNnSc4	devátý
největší	veliký	k2eAgNnSc4d3	veliký
a	a	k8xC	a
desáté	desátá	k1gFnSc2	desátá
nejhmotnější	hmotný	k2eAgNnSc4d3	nejhmotnější
známé	známý	k2eAgNnSc4d1	známé
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
přímo	přímo	k6eAd1	přímo
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
ho	on	k3xPp3gNnSc4	on
objevil	objevit	k5eAaPmAgMnS	objevit
americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
Clyde	Clyd	k1gInSc5	Clyd
Tombaugh	Tombaugh	k1gMnSc1	Tombaugh
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
kosmické	kosmický	k2eAgNnSc4d1	kosmické
těleso	těleso	k1gNnSc4	těleso
astronomové	astronom	k1gMnPc1	astronom
původně	původně	k6eAd1	původně
řadili	řadit	k5eAaImAgMnP	řadit
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
definice	definice	k1gFnSc2	definice
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
planeta	planeta	k1gFnSc1	planeta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgNnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
26	[number]	k4	26
<g/>
.	.	kIx.	.
valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
a	a	k8xC	a
plutoidy	plutoida	k1gFnPc4	plutoida
<g/>
.	.	kIx.	.
</s>
