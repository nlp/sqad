<s>
Pluto	plut	k2eAgNnSc4d1	Pluto
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgNnSc4d1	oficiální
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
134340	[number]	k4	134340
<g/>
)	)	kIx)	)
Pluto	Pluto	k1gNnSc4	Pluto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
a	a	k8xC	a
po	po	k7c6	po
Eris	Eris	k1gFnSc1	Eris
druhou	druhý	k4xOgFnSc7	druhý
nejhmotnější	hmotný	k2eAgFnSc7d3	nejhmotnější
známou	známý	k2eAgFnSc7d1	známá
trpasličí	trpasličí	k2eAgFnSc7d1	trpasličí
planetou	planeta	k1gFnSc7	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
