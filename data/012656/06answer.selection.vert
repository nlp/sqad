<s>
Poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
Svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
hoře	hora	k1gFnSc6	hora
u	u	k7c2	u
města	město	k1gNnSc2	město
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
stavby	stavba	k1gFnPc4	stavba
barokního	barokní	k2eAgMnSc2d1	barokní
stavitele	stavitel	k1gMnSc2	stavitel
Jana	Jan	k1gMnSc2	Jan
Blažeje	Blažej	k1gMnSc2	Blažej
Santiniho-Aichela	Santiniho-Aichel	k1gMnSc2	Santiniho-Aichel
<g/>
.	.	kIx.	.
</s>
