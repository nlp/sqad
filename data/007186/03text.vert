<s>
Pravda	pravda	k9	pravda
či	či	k8xC	či
pravdivost	pravdivost	k1gFnSc4	pravdivost
je	být	k5eAaImIp3nS	být
shoda	shoda	k1gFnSc1	shoda
tvrzení	tvrzení	k1gNnSc2	tvrzení
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
hraje	hrát	k5eAaImIp3nS	hrát
primární	primární	k2eAgFnSc4d1	primární
úlohu	úloha	k1gFnSc4	úloha
ve	v	k7c6	v
filosofii	filosofie	k1gFnSc6	filosofie
<g/>
,	,	kIx,	,
vědě	věda	k1gFnSc3	věda
<g/>
,	,	kIx,	,
právu	právo	k1gNnSc3	právo
a	a	k8xC	a
náboženství	náboženství	k1gNnSc3	náboženství
(	(	kIx(	(
<g/>
či	či	k8xC	či
teologii	teologie	k1gFnSc4	teologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravda	pravda	k9	pravda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
absolutní	absolutní	k2eAgFnSc1d1	absolutní
-	-	kIx~	-
neměnná	neměnný	k2eAgFnSc1d1	neměnná
relativní	relativní	k2eAgFnSc1d1	relativní
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
relativismus	relativismus	k1gInSc1	relativismus
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
relativismus	relativismus	k1gInSc1	relativismus
nebo	nebo	k8xC	nebo
také	také	k9	také
objektivní	objektivní	k2eAgFnSc1d1	objektivní
-	-	kIx~	-
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
subjektu	subjekt	k1gInSc6	subjekt
subjektivní	subjektivní	k2eAgNnSc1d1	subjektivní
-	-	kIx~	-
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
osobě	osoba	k1gFnSc6	osoba
Pravdě	pravda	k1gFnSc6	pravda
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
formální	formální	k2eAgFnSc1d1	formální
logika	logika	k1gFnSc1	logika
a	a	k8xC	a
epistemologie	epistemologie	k1gFnSc1	epistemologie
<g/>
.	.	kIx.	.
</s>
<s>
Pravdivé	pravdivý	k2eAgInPc1d1	pravdivý
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
výroky	výrok	k1gInPc1	výrok
<g/>
,	,	kIx,	,
premisy	premisa	k1gFnPc1	premisa
<g/>
,	,	kIx,	,
závěry	závěr	k1gInPc1	závěr
<g/>
,	,	kIx,	,
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
nebo	nebo	k8xC	nebo
soudy	soud	k1gInPc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
rozšířených	rozšířený	k2eAgFnPc2d1	rozšířená
teorií	teorie	k1gFnPc2	teorie
pravdy	pravda	k1gFnSc2	pravda
<g/>
:	:	kIx,	:
Adekvační	Adekvační	k2eAgFnPc1d1	Adekvační
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
též	též	k9	též
korespondenční	korespondenční	k2eAgFnSc1d1	korespondenční
teorie	teorie	k1gFnSc1	teorie
pravdy	pravda	k1gFnSc2	pravda
je	být	k5eAaImIp3nS	být
klasický	klasický	k2eAgInSc4d1	klasický
pohled	pohled	k1gInSc4	pohled
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zastával	zastávat	k5eAaImAgInS	zastávat
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
(	(	kIx(	(
<g/>
384	[number]	k4	384
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
322	[number]	k4	322
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
středověká	středověký	k2eAgFnSc1d1	středověká
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
na	na	k7c4	na
pravdu	pravda	k1gFnSc4	pravda
jako	jako	k8xC	jako
shodu	shoda	k1gFnSc4	shoda
mezi	mezi	k7c7	mezi
poznáním	poznání	k1gNnSc7	poznání
a	a	k8xC	a
věcí	věc	k1gFnSc7	věc
(	(	kIx(	(
<g/>
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
je	být	k5eAaImIp3nS	být
říci	říct	k5eAaPmF	říct
o	o	k7c6	o
něčem	něco	k3yInSc6	něco
<g/>
,	,	kIx,	,
co	co	k9	co
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
něčem	něco	k3yInSc6	něco
<g/>
,	,	kIx,	,
co	co	k9	co
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
"	"	kIx"	"
<g/>
Pravda	pravda	k9	pravda
je	být	k5eAaImIp3nS	být
shoda	shoda	k1gFnSc1	shoda
skutečnosti	skutečnost	k1gFnSc2	skutečnost
s	s	k7c7	s
poznáním	poznání	k1gNnSc7	poznání
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Veritas	Veritas	k1gMnSc1	Veritas
est	est	k?	est
adaequatio	adaequatio	k1gMnSc1	adaequatio
rei	rei	k?	rei
et	et	k?	et
intellectus	intellectus	k1gInSc1	intellectus
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
Určitá	určitý	k2eAgFnSc1d1	určitá
modifikace	modifikace	k1gFnSc1	modifikace
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
na	na	k7c4	na
pravdivost	pravdivost	k1gFnSc4	pravdivost
čistě	čistě	k6eAd1	čistě
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
na	na	k7c4	na
pravdu	pravda	k1gFnSc4	pravda
jako	jako	k8xS	jako
shodu	shoda	k1gFnSc4	shoda
informace	informace	k1gFnSc2	informace
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Koherenční	Koherenční	k2eAgFnSc1d1	Koherenční
teorie	teorie	k1gFnSc1	teorie
pravdy	pravda	k1gFnSc2	pravda
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
na	na	k7c4	na
pravdu	pravda	k1gFnSc4	pravda
jako	jako	k8xC	jako
na	na	k7c4	na
koherentní	koherentní	k2eAgFnSc4d1	koherentní
řadu	řada	k1gFnSc4	řada
výroků	výrok	k1gInPc2	výrok
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
výroků	výrok	k1gInPc2	výrok
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
výroky	výrok	k1gInPc4	výrok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejlépe	dobře	k6eAd3	dobře
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
nebo	nebo	k8xC	nebo
nejúplněji	úplně	k6eAd3	úplně
popisují	popisovat	k5eAaImIp3nP	popisovat
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Konsensuální	konsensuální	k2eAgFnSc1d1	konsensuální
teorie	teorie	k1gFnSc1	teorie
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Charles	Charles	k1gMnSc1	Charles
Peirce	Peirce	k1gMnSc1	Peirce
<g/>
)	)	kIx)	)
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
na	na	k7c4	na
pravdu	pravda	k1gFnSc4	pravda
jako	jako	k8xC	jako
cosi	cosi	k3yInSc1	cosi
<g/>
,	,	kIx,	,
na	na	k7c6	na
čem	co	k3yQnSc6	co
se	se	k3xPyFc4	se
shodne	shodnout	k5eAaPmIp3nS	shodnout
určitá	určitý	k2eAgFnSc1d1	určitá
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kompetentní	kompetentní	k2eAgMnPc1d1	kompetentní
odborníci	odborník	k1gMnPc1	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Pragmatismus	pragmatismus	k1gInSc1	pragmatismus
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravda	pravda	k1gFnSc1	pravda
je	být	k5eAaImIp3nS	být
úspěchem	úspěch	k1gInSc7	úspěch
praktických	praktický	k2eAgInPc2d1	praktický
důsledků	důsledek	k1gInPc2	důsledek
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
je	být	k5eAaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
užitečností	užitečnost	k1gFnSc7	užitečnost
<g/>
.	.	kIx.	.
</s>
<s>
Pravdu	pravda	k1gFnSc4	pravda
lze	lze	k6eAd1	lze
také	také	k9	také
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
model	model	k1gInSc4	model
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
100	[number]	k4	100
%	%	kIx~	%
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnPc1	náš
schopnosti	schopnost	k1gFnPc1	schopnost
popsat	popsat	k5eAaPmF	popsat
realitu	realita	k1gFnSc4	realita
mají	mít	k5eAaImIp3nP	mít
omezení	omezení	k1gNnSc4	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
neumíme	umět	k5eNaImIp1nP	umět
skutečnost	skutečnost	k1gFnSc4	skutečnost
dokonale	dokonale	k6eAd1	dokonale
změřit	změřit	k5eAaPmF	změřit
(	(	kIx(	(
<g/>
máme	mít	k5eAaImIp1nP	mít
nedokonalé	dokonalý	k2eNgInPc1d1	nedokonalý
smysly	smysl	k1gInPc1	smysl
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nás	my	k3xPp1nPc4	my
může	moct	k5eAaImIp3nS	moct
limitovat	limitovat	k5eAaBmF	limitovat
vyjadřovací	vyjadřovací	k2eAgFnSc4d1	vyjadřovací
schopnost	schopnost	k1gFnSc4	schopnost
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
poznání	poznání	k1gNnSc6	poznání
reprezentujeme	reprezentovat	k5eAaImIp1nP	reprezentovat
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
zkreslené	zkreslený	k2eAgInPc1d1	zkreslený
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
přesunu	přesun	k1gInSc6	přesun
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
záměrně	záměrně	k6eAd1	záměrně
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
použitím	použití	k1gNnSc7	použití
nespolehlivého	spolehlivý	k2eNgNnSc2d1	nespolehlivé
média	médium	k1gNnSc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatní	ostatní	k2eAgInPc4d1	ostatní
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
přežití	přežití	k1gNnSc4	přežití
činit	činit	k5eAaImF	činit
správná	správný	k2eAgNnPc4d1	správné
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
znát	znát	k5eAaImF	znát
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
často	často	k6eAd1	často
prakticky	prakticky	k6eAd1	prakticky
nedostupná	dostupný	k2eNgNnPc1d1	nedostupné
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
životě	život	k1gInSc6	život
pomáháme	pomáhat	k5eAaImIp1nP	pomáhat
dalšími	další	k2eAgInPc7d1	další
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nám	my	k3xPp1nPc3	my
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
se	se	k3xPyFc4	se
pravdě	pravda	k1gFnSc3	pravda
alespoň	alespoň	k9	alespoň
přiblížit	přiblížit	k5eAaPmF	přiblížit
<g/>
:	:	kIx,	:
Konsenzus	konsenzus	k1gInSc4	konsenzus
-	-	kIx~	-
pokusíme	pokusit	k5eAaPmIp1nP	pokusit
se	se	k3xPyFc4	se
na	na	k7c6	na
pravdě	pravda	k1gFnSc6	pravda
dohodnout	dohodnout	k5eAaPmF	dohodnout
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
-	-	kIx~	-
pravda	pravda	k1gFnSc1	pravda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
čemu	co	k3yRnSc3	co
věří	věřit	k5eAaImIp3nS	věřit
víc	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Autorita	autorita	k1gFnSc1	autorita
-	-	kIx~	-
pravda	pravda	k1gFnSc1	pravda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
řekl	říct	k5eAaPmAgMnS	říct
náčelník	náčelník	k1gMnSc1	náčelník
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgInSc1d1	centrální
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
Bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Vědecké	vědecký	k2eAgFnPc4d1	vědecká
metody	metoda	k1gFnPc4	metoda
-	-	kIx~	-
pravda	pravda	k9	pravda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
opakovaně	opakovaně	k6eAd1	opakovaně
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
dobře	dobře	k6eAd1	dobře
navrženým	navržený	k2eAgInSc7d1	navržený
experimentem	experiment	k1gInSc7	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
uvedené	uvedený	k2eAgInPc1d1	uvedený
nástroje	nástroj	k1gInPc1	nástroj
jsou	být	k5eAaImIp3nP	být
zneužitelné	zneužitelný	k2eAgInPc1d1	zneužitelný
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
vést	vést	k5eAaImF	vést
k	k	k7c3	k
pravdě	pravda	k1gFnSc3	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Konsenzu	konsenz	k1gInSc3	konsenz
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
manipulací	manipulace	k1gFnSc7	manipulace
<g/>
,	,	kIx,	,
autoritu	autorita	k1gFnSc4	autorita
lze	lze	k6eAd1	lze
zkorumpovat	zkorumpovat	k5eAaPmF	zkorumpovat
a	a	k8xC	a
výrok	výrok	k1gInSc4	výrok
"	"	kIx"	"
<g/>
Stokrát	stokrát	k6eAd1	stokrát
opakovaná	opakovaný	k2eAgFnSc1d1	opakovaná
lež	lež	k1gFnSc1	lež
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
pravdou	pravda	k1gFnSc7	pravda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
připisován	připisovat	k5eAaImNgInS	připisovat
Josephu	Joseph	k1gMnSc3	Joseph
Goebbelsovi	Goebbels	k1gMnSc3	Goebbels
<g/>
)	)	kIx)	)
nám	my	k3xPp1nPc3	my
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
s	s	k7c7	s
většinovým	většinový	k2eAgInSc7d1	většinový
názorem	názor	k1gInSc7	názor
lze	lze	k6eAd1	lze
pohnout	pohnout	k5eAaPmF	pohnout
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
logiky	logika	k1gFnSc2	logika
jsou	být	k5eAaImIp3nP	být
pravdivé	pravdivý	k2eAgInPc1d1	pravdivý
nebo	nebo	k8xC	nebo
nepravdivé	pravdivý	k2eNgInPc1d1	nepravdivý
soudy	soud	k1gInPc1	soud
<g/>
,	,	kIx,	,
následující	následující	k2eAgInPc1d1	následující
příklady	příklad	k1gInPc1	příklad
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
klasických	klasický	k2eAgInPc2d1	klasický
paradoxních	paradoxní	k2eAgInPc2d1	paradoxní
logických	logický	k2eAgInPc2d1	logický
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
skupina	skupina	k1gFnSc1	skupina
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgInPc6	který
nelze	lze	k6eNd1	lze
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
:	:	kIx,	:
vypovídají	vypovídat	k5eAaPmIp3nP	vypovídat
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
samých	samý	k3xTgMnPc6	samý
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
předpoklad	předpoklad	k1gInSc1	předpoklad
jejich	jejich	k3xOp3gFnSc2	jejich
pravdivosti	pravdivost	k1gFnSc2	pravdivost
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
jejich	jejich	k3xOp3gFnSc4	jejich
nepravdivost	nepravdivost	k1gFnSc4	nepravdivost
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výroky	výrok	k1gInPc1	výrok
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
sebevyvracející	sebevyvracející	k2eAgInPc1d1	sebevyvracející
výroky	výrok	k1gInPc1	výrok
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
známým	známý	k2eAgInSc7d1	známý
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
paradox	paradox	k1gInSc1	paradox
lháře	lhář	k1gMnSc2	lhář
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tvrzení	tvrzení	k1gNnSc4	tvrzení
Kréťana	Kréťan	k1gMnSc2	Kréťan
Epimenida	Epimenid	k1gMnSc2	Epimenid
(	(	kIx(	(
<g/>
Ἐ	Ἐ	k?	Ἐ
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
Kréťané	Kréťan	k1gMnPc1	Kréťan
jsou	být	k5eAaImIp3nP	být
lháři	lhář	k1gMnPc1	lhář
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
příklad	příklad	k1gInSc1	příklad
z	z	k7c2	z
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
podobný	podobný	k2eAgInSc1d1	podobný
k	k	k7c3	k
Epimenidovu	Epimenidův	k2eAgNnSc3d1	Epimenidův
paradoxu	paradoxon	k1gNnSc3	paradoxon
<g/>
:	:	kIx,	:
když	když	k8xS	když
vstoupíte	vstoupit	k5eAaPmIp2nP	vstoupit
na	na	k7c4	na
webovou	webový	k2eAgFnSc4d1	webová
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
jste	být	k5eAaImIp2nP	být
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
výrok	výrok	k1gInSc1	výrok
zní	znět	k5eAaImIp3nS	znět
paradoxně	paradoxně	k6eAd1	paradoxně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tvrzení	tvrzení	k1gNnSc1	tvrzení
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
,	,	kIx,	,
nevzniká	vznikat	k5eNaImIp3nS	vznikat
paradox	paradox	k1gInSc1	paradox
<g/>
.	.	kIx.	.
</s>
<s>
Silnější	silný	k2eAgFnSc1d2	silnější
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
nerozřešitelný	rozřešitelný	k2eNgInSc1d1	rozřešitelný
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
výrok	výrok	k1gInSc1	výrok
"	"	kIx"	"
<g/>
Teď	teď	k6eAd1	teď
právě	právě	k9	právě
lžu	lhát	k5eAaImIp1nS	lhát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Tato	tento	k3xDgFnSc1	tento
věta	věta	k1gFnSc1	věta
je	být	k5eAaImIp3nS	být
lživá	lživý	k2eAgFnSc1d1	lživá
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
pravdivostní	pravdivostní	k2eAgInPc1d1	pravdivostní
paradoxy	paradox	k1gInPc1	paradox
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
z	z	k7c2	z
reálné	reálný	k2eAgFnSc2d1	reálná
nemožnosti	nemožnost	k1gFnSc2	nemožnost
absolutně	absolutně	k6eAd1	absolutně
definitivně	definitivně	k6eAd1	definitivně
objasnit	objasnit	k5eAaPmF	objasnit
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
pravdivostní	pravdivostní	k2eAgInSc1d1	pravdivostní
výrok	výrok	k1gInSc1	výrok
týká	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
každá	každý	k3xTgFnSc1	každý
"	"	kIx"	"
<g/>
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
relativní	relativní	k2eAgFnSc1d1	relativní
ke	k	k7c3	k
svému	svůj	k1gMnSc3	svůj
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
a	a	k8xC	a
tedy	tedy	k9	tedy
ne	ne	k9	ne
absolutní	absolutní	k2eAgFnSc1d1	absolutní
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
filosofii	filosofie	k1gFnSc6	filosofie
byla	být	k5eAaImAgFnS	být
Alétheia	Alétheia	k1gFnSc1	Alétheia
(	(	kIx(	(
<g/>
ἀ	ἀ	k?	ἀ
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
významu	význam	k1gInSc6	význam
neskrytost	neskrytost	k1gFnSc4	neskrytost
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
znamená	znamenat	k5eAaImIp3nS	znamenat
zjevení	zjevení	k1gNnSc1	zjevení
<g/>
,	,	kIx,	,
živoucí	živoucí	k2eAgFnSc1d1	živoucí
<g/>
,	,	kIx,	,
existující	existující	k2eAgFnSc1d1	existující
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
skutečného	skutečný	k2eAgNnSc2d1	skutečné
poznání	poznání	k1gNnSc2	poznání
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
pouhému	pouhý	k2eAgNnSc3d1	pouhé
zdání	zdání	k1gNnSc3	zdání
(	(	kIx(	(
<g/>
doxa	dox	k1gInSc2	dox
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravda	pravda	k1gFnSc1	pravda
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
jevů	jev	k1gInPc2	jev
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ji	on	k3xPp3gFnSc4	on
intelektuálně	intelektuálně	k6eAd1	intelektuálně
odkrývat	odkrývat	k5eAaImF	odkrývat
<g/>
.	.	kIx.	.
</s>
<s>
Logika	logika	k1gFnSc1	logika
Tautologie	tautologie	k1gFnSc2	tautologie
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pravda	pravda	k1gFnSc1	pravda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pravda	pravda	k1gFnSc1	pravda
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc2	téma
Pravda	pravda	k9	pravda
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Peregrin	Peregrin	k1gInSc1	Peregrin
<g/>
:	:	kIx,	:
Jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
magie	magie	k1gFnSc1	magie
&	&	k?	&
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc4	část
<g/>
:	:	kIx,	:
Teď	teď	k6eAd1	teď
právě	právě	k6eAd1	právě
lžu	lhát	k5eAaImIp1nS	lhát
<g/>
.	.	kIx.	.
</s>
