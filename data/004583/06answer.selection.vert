<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
jemu	on	k3xPp3gMnSc3	on
připsaných	připsaný	k2eAgNnPc2d1	připsané
přibližně	přibližně	k6eAd1	přibližně
38	[number]	k4	38
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
asi	asi	k9	asi
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
154	[number]	k4	154
sonetů	sonet	k1gInPc2	sonet
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
epické	epický	k2eAgFnPc4d1	epická
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
nejistého	jistý	k2eNgNnSc2d1	nejisté
autorství	autorství	k1gNnSc2	autorství
<g/>
.	.	kIx.	.
</s>
