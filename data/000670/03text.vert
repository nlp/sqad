<s>
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Sorbon	Sorbon	k1gMnSc1	Sorbon
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1201	[number]	k4	1201
v	v	k7c6	v
Sorbonu	Sorbon	k1gInSc6	Sorbon
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1274	[number]	k4	1274
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Sorbon	Sorbon	k1gMnSc1	Sorbon
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
venkovské	venkovský	k2eAgFnSc2d1	venkovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
teologii	teologie	k1gFnSc4	teologie
v	v	k7c6	v
Remeši	Remeš	k1gFnSc6	Remeš
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1250	[number]	k4	1250
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
doktorem	doktor	k1gMnSc7	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
místo	místo	k1gNnSc4	místo
kanovníka	kanovník	k1gMnSc2	kanovník
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
v	v	k7c6	v
Cambrai	Cambra	k1gFnSc6	Cambra
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
získal	získat	k5eAaPmAgInS	získat
věhlas	věhlas	k1gInSc1	věhlas
svými	svůj	k3xOyFgNnPc7	svůj
kázáními	kázání	k1gNnPc7	kázání
<g/>
,	,	kIx,	,
jej	on	k3xPp3gNnSc4	on
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
svým	svůj	k3xOyFgMnSc7	svůj
kaplanem	kaplan	k1gMnSc7	kaplan
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zpovědníkem	zpovědník	k1gMnSc7	zpovědník
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Sorbon	Sorbon	k1gMnSc1	Sorbon
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
snížit	snížit	k5eAaPmF	snížit
překážky	překážka	k1gFnPc4	překážka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
žáci	žák	k1gMnPc1	žák
z	z	k7c2	z
chudých	chudý	k2eAgFnPc2d1	chudá
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yQgFnPc1	který
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
zažil	zažít	k5eAaPmAgMnS	zažít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
společnost	společnost	k1gFnSc4	společnost
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nP	by
poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
výukové	výukový	k2eAgFnPc4d1	výuková
lekce	lekce	k1gFnPc4	lekce
zdarma	zdarma	k6eAd1	zdarma
šestnácti	šestnáct	k4xCc7	šestnáct
studentům	student	k1gMnPc3	student
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
Guillaume	Guillaum	k1gInSc5	Guillaum
de	de	k?	de
Bray	Braa	k1gFnPc1	Braa
<g/>
,	,	kIx,	,
arciděkan	arciděkan	k1gMnSc1	arciděkan
z	z	k7c2	z
Remeše	Remeš	k1gFnSc2	Remeš
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Douai	Doua	k1gFnSc2	Doua
<g/>
,	,	kIx,	,
královnin	královnin	k2eAgMnSc1d1	královnin
kanovník	kanovník	k1gMnSc1	kanovník
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
Geoffroi	Geoffro	k1gFnPc1	Geoffro
de	de	k?	de
Bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
kardinál	kardinál	k1gMnSc1	kardinál
a	a	k8xC	a
Guillaume	Guillaum	k1gInSc5	Guillaum
de	de	k?	de
Chartres	Chartres	k1gMnSc1	Chartres
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
králových	králův	k2eAgMnPc2d1	králův
kaplanů	kaplan	k1gMnPc2	kaplan
<g/>
.	.	kIx.	.
</s>
<s>
Výnosem	výnos	k1gInSc7	výnos
ze	z	k7c2	z
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1250	[number]	k4	1250
královna	královna	k1gFnSc1	královna
Blanka	Blanka	k1gFnSc1	Blanka
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vládla	vládnout	k5eAaImAgFnS	vládnout
během	během	k7c2	během
sedmé	sedmý	k4xOgFnSc2	sedmý
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
věnovala	věnovat	k5eAaImAgFnS	věnovat
Robertovi	Robert	k1gMnSc3	Robert
de	de	k?	de
Sorbon	Sorbon	k1gMnSc1	Sorbon
dům	dům	k1gInSc1	dům
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnSc2d1	přilehlá
stáje	stáj	k1gFnSc2	stáj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
mohl	moct	k5eAaImAgInS	moct
vyučovat	vyučovat	k5eAaImF	vyučovat
chudé	chudý	k2eAgMnPc4d1	chudý
žáky	žák	k1gMnPc4	žák
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
královská	královský	k2eAgFnSc1d1	královská
dotace	dotace	k1gFnSc1	dotace
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
písemnou	písemný	k2eAgFnSc7d1	písemná
zprávou	zpráva	k1gFnSc7	zpráva
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
Sorbony	Sorbona	k1gFnSc2	Sorbona
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
kolej	kolej	k1gFnSc1	kolej
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
zakladateli	zakladatel	k1gMnSc6	zakladatel
Collè	Collè	k1gFnSc7	Collè
de	de	k?	de
Sorbonne	Sorbonn	k1gInSc5	Sorbonn
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1253	[number]	k4	1253
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
kolejí	kolej	k1gFnSc7	kolej
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
králem	král	k1gMnSc7	král
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1257	[number]	k4	1257
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1258	[number]	k4	1258
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1263	[number]	k4	1263
Robert	Robert	k1gMnSc1	Robert
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
krále	král	k1gMnSc2	král
další	další	k2eAgInSc4d1	další
dva	dva	k4xCgInPc4	dva
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žáci	žák	k1gMnPc1	žák
jeho	jeho	k3xOp3gFnSc2	jeho
koleje	kolej	k1gFnSc2	kolej
budou	být	k5eAaImBp3nP	být
přijati	přijmout	k5eAaPmNgMnP	přijmout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
nebyli	být	k5eNaImAgMnP	být
vyloučeni	vyloučit	k5eAaPmNgMnP	vyloučit
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
studenti	student	k1gMnPc1	student
z	z	k7c2	z
bohatých	bohatý	k2eAgFnPc2d1	bohatá
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
platili	platit	k5eAaImAgMnP	platit
koleji	kolej	k1gFnSc3	kolej
částku	částka	k1gFnSc4	částka
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
stipendiu	stipendium	k1gNnSc3	stipendium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
dostávali	dostávat	k5eAaImAgMnP	dostávat
chudí	chudý	k2eAgMnPc1d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
koleje	kolej	k1gFnSc2	kolej
měli	mít	k5eAaImAgMnP	mít
rovněž	rovněž	k9	rovněž
přístup	přístup	k1gInSc4	přístup
studenti	student	k1gMnPc1	student
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Teologickou	teologický	k2eAgFnSc4d1	teologická
kolej	kolej	k1gFnSc4	kolej
schválil	schválit	k5eAaPmAgInS	schválit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1259	[number]	k4	1259
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1271	[number]	k4	1271
doplnil	doplnit	k5eAaPmAgMnS	doplnit
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
kolej	kolej	k1gFnSc4	kolej
pro	pro	k7c4	pro
humanitní	humanitní	k2eAgFnPc4d1	humanitní
vědy	věda	k1gFnPc4	věda
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
kolejí	kolej	k1gFnPc2	kolej
stály	stát	k5eAaImAgFnP	stát
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1635	[number]	k4	1635
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	on	k3xPp3gMnPc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
kardinál	kardinál	k1gMnSc1	kardinál
Richelieu	Richelieu	k1gMnSc1	Richelieu
zbořit	zbořit	k5eAaPmF	zbořit
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
místě	místo	k1gNnSc6	místo
vybudovat	vybudovat	k5eAaPmF	vybudovat
nové	nový	k2eAgFnPc4d1	nová
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
barokní	barokní	k2eAgFnSc1d1	barokní
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Uršuly	Uršula	k1gFnSc2	Uršula
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
vůli	vůle	k1gFnSc6	vůle
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1270	[number]	k4	1270
Robert	Robert	k1gMnSc1	Robert
odkázal	odkázat	k5eAaPmAgMnS	odkázat
veškerý	veškerý	k3xTgInSc4	veškerý
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
koleji	kolej	k1gFnSc6	kolej
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Sorbon	Sorbon	k1gMnSc1	Sorbon
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
