<p>
<s>
Charlie	Charlie	k1gMnSc1	Charlie
Irwin	Irwin	k1gMnSc1	Irwin
Sheen	Sheen	k2eAgMnSc1d1	Sheen
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Carlos	Carlos	k1gMnSc1	Carlos
Irwin	Irwin	k1gMnSc1	Irwin
Estévez	Estévez	k1gMnSc1	Estévez
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
New	New	k1gMnPc2	New
York	York	k1gInSc4	York
City	City	k1gFnPc2	City
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svými	svůj	k3xOyFgFnPc7	svůj
rolemi	role	k1gFnPc7	role
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Žhavé	žhavý	k2eAgInPc1d1	žhavý
výstřely	výstřel	k1gInPc1	výstřel
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Četa	četa	k1gFnSc1	četa
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Dva	dva	k4xCgInPc1	dva
a	a	k8xC	a
půl	půl	k1xP	půl
chlapa	chlap	k1gMnSc2	chlap
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
až	až	k9	až
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
umělecké	umělecký	k2eAgFnSc2d1	umělecká
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
Janet	Janet	k1gInSc4	Janet
Estevéz	Estevéza	k1gFnPc2	Estevéza
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Templetonová	Templetonový	k2eAgFnSc1d1	Templetonová
<g/>
)	)	kIx)	)
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Ramón	Ramón	k1gMnSc1	Ramón
Estévez	Estévez	k1gMnSc1	Estévez
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
Martin	Martin	k2eAgInSc4d1	Martin
Sheen	Sheen	k1gInSc4	Sheen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
Charlieho	Charlie	k1gMnSc4	Charlie
narození	narození	k1gNnSc2	narození
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Malibu	Malib	k1gInSc2	Malib
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
Santa	Sant	k1gMnSc2	Sant
Monice	Monika	k1gFnSc6	Monika
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
tamním	tamní	k2eAgInSc6d1	tamní
baseballovém	baseballový	k2eAgInSc6d1	baseballový
týmu	tým	k1gInSc6	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kariéru	kariéra	k1gFnSc4	kariéra
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
televizními	televizní	k2eAgInPc7d1	televizní
seriály	seriál	k1gInPc7	seriál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
dostal	dostat	k5eAaPmAgMnS	dostat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
filmu	film	k1gInSc6	film
Četa	četa	k1gFnSc1	četa
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
filmu	film	k1gInSc6	film
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
získal	získat	k5eAaPmAgInS	získat
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Spin	spina	k1gFnPc2	spina
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
potřetí	potřetí	k4xO	potřetí
rozvedený	rozvedený	k2eAgInSc1d1	rozvedený
<g/>
,	,	kIx,	,
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
Denise	Denisa	k1gFnSc6	Denisa
Richardsovou	Richardsová	k1gFnSc7	Richardsová
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
třetí	třetí	k4xOgFnSc7	třetí
ženou	žena	k1gFnSc7	žena
má	mít	k5eAaImIp3nS	mít
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
představitel	představitel	k1gMnSc1	představitel
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Západní	západní	k2eAgNnSc4d1	západní
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
herců	herc	k1gInPc2	herc
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
hnutí	hnutí	k1gNnPc2	hnutí
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
truth	truth	k1gMnSc1	truth
movement	movement	k1gMnSc1	movement
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevoval	objevovat	k5eAaImAgInS	objevovat
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
Alexe	Alex	k1gMnSc5	Alex
Jonese	Jonesa	k1gFnSc3	Jonesa
a	a	k8xC	a
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
nové	nový	k2eAgInPc4d1	nový
podněty	podnět	k1gInPc4	podnět
k	k	k7c3	k
zamyšlení	zamyšlení	k1gNnSc3	zamyšlení
nad	nad	k7c7	nad
neštěstím	neštěstí	k1gNnSc7	neštěstí
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2001	[number]	k4	2001
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc4	Internet
rychle	rychle	k6eAd1	rychle
oblétlo	oblétnout	k5eAaPmAgNnS	oblétnout
jeho	jeho	k3xOp3gNnSc1	jeho
video	video	k1gNnSc1	video
a	a	k8xC	a
článek	článek	k1gInSc1	článek
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Obamou	Obama	k1gMnSc7	Obama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
jej	on	k3xPp3gInSc4	on
vyloučili	vyloučit	k5eAaPmAgMnP	vyloučit
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
seriálu	seriál	k1gInSc6	seriál
Dva	dva	k4xCgInPc1	dva
a	a	k8xC	a
půl	půl	k1xP	půl
chlapa	chlap	k1gMnSc2	chlap
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Sheen	Sheen	k1gInSc1	Sheen
v	v	k7c4	v
talk	talk	k1gInSc4	talk
show	show	k1gNnSc4	show
televize	televize	k1gFnSc2	televize
NBC	NBC	kA	NBC
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
HIV	HIV	kA	HIV
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybraná	vybraný	k2eAgFnSc1d1	vybraná
filmografie	filmografie	k1gFnSc1	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
