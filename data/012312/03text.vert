<p>
<s>
Akcie	akcie	k1gFnSc1	akcie
je	být	k5eAaImIp3nS	být
cenný	cenný	k2eAgInSc4d1	cenný
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
majitel	majitel	k1gMnSc1	majitel
(	(	kIx(	(
<g/>
držitel	držitel	k1gMnSc1	držitel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akcionář	akcionář	k1gMnSc1	akcionář
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
že	že	k8xS	že
vložil	vložit	k5eAaPmAgInS	vložit
určitý	určitý	k2eAgInSc1d1	určitý
majetkový	majetkový	k2eAgInSc1d1	majetkový
podíl	podíl	k1gInSc1	podíl
(	(	kIx(	(
<g/>
kapitál	kapitál	k1gInSc1	kapitál
<g/>
)	)	kIx)	)
do	do	k7c2	do
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Akcionář	akcionář	k1gMnSc1	akcionář
má	mít	k5eAaImIp3nS	mít
různá	různý	k2eAgNnPc4d1	různé
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgInS	oprávnit
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
společnosti	společnost	k1gFnSc2	společnost
formou	forma	k1gFnSc7	forma
dividendy	dividenda	k1gFnSc2	dividenda
a	a	k8xC	a
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
společnosti	společnost	k1gFnSc2	společnost
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
hlasovat	hlasovat	k5eAaImF	hlasovat
na	na	k7c6	na
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
likvidačním	likvidační	k2eAgInSc6d1	likvidační
zůstatku	zůstatek	k1gInSc6	zůstatek
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
závazky	závazek	k1gInPc4	závazek
společnosti	společnost	k1gFnSc2	společnost
akcionáři	akcionář	k1gMnPc1	akcionář
ručí	ručit	k5eAaImIp3nP	ručit
pouze	pouze	k6eAd1	pouze
svým	svůj	k3xOyFgInSc7	svůj
vloženým	vložený	k2eAgInSc7d1	vložený
podílem	podíl	k1gInSc7	podíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
akciemi	akcie	k1gFnPc7	akcie
se	se	k3xPyFc4	se
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
na	na	k7c6	na
burzách	burza	k1gFnPc6	burza
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nabídky	nabídka	k1gFnSc2	nabídka
a	a	k8xC	a
poptávky	poptávka	k1gFnSc2	poptávka
určuje	určovat	k5eAaImIp3nS	určovat
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Kapitál	kapitál	k1gInSc1	kapitál
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
vzniká	vznikat	k5eAaImIp3nS	vznikat
emisí	emise	k1gFnSc7	emise
akcií	akcie	k1gFnPc2	akcie
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
prodejem	prodej	k1gInSc7	prodej
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
za	za	k7c4	za
nominální	nominální	k2eAgFnSc4d1	nominální
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
akcii	akcie	k1gFnSc4	akcie
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pouhého	pouhý	k2eAgNnSc2d1	pouhé
elektronického	elektronický	k2eAgNnSc2d1	elektronické
zaknihování	zaknihování	k1gNnSc2	zaknihování
oficiálně	oficiálně	k6eAd1	oficiálně
určena	určit	k5eAaPmNgFnS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
akcie	akcie	k1gFnPc1	akcie
bez	bez	k7c2	bez
vyznačené	vyznačený	k2eAgFnSc2d1	vyznačená
nominální	nominální	k2eAgFnSc2d1	nominální
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Akcionáři	akcionář	k1gMnPc1	akcionář
tyto	tento	k3xDgFnPc4	tento
cenné	cenný	k2eAgInPc1d1	cenný
papíry	papír	k1gInPc1	papír
kupují	kupovat	k5eAaImIp3nP	kupovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
očekávají	očekávat	k5eAaImIp3nP	očekávat
zisk	zisk	k1gInSc4	zisk
jednak	jednak	k8xC	jednak
z	z	k7c2	z
vyplácených	vyplácený	k2eAgFnPc2d1	vyplácená
dividend	dividenda	k1gFnPc2	dividenda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
z	z	k7c2	z
růstu	růst	k1gInSc2	růst
kapitalizace	kapitalizace	k1gFnSc2	kapitalizace
akcie	akcie	k1gFnSc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Nákup	nákup	k1gInSc1	nákup
akcií	akcie	k1gFnPc2	akcie
ve	v	k7c6	v
vhodné	vhodný	k2eAgFnSc6d1	vhodná
době	doba	k1gFnSc6	doba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
způsob	způsob	k1gInSc1	způsob
zhodnocení	zhodnocení	k1gNnSc2	zhodnocení
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
hodnota	hodnota	k1gFnSc1	hodnota
akcie	akcie	k1gFnSc2	akcie
může	moct	k5eAaImIp3nS	moct
vzrůst	vzrůst	k5eAaPmF	vzrůst
až	až	k9	až
o	o	k7c4	o
stovky	stovka	k1gFnPc4	stovka
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
možný	možný	k2eAgInSc1d1	možný
pokles	pokles	k1gInSc1	pokles
cen	cena	k1gFnPc2	cena
akcií	akcie	k1gFnPc2	akcie
a	a	k8xC	a
částečná	částečný	k2eAgFnSc1d1	částečná
nebo	nebo	k8xC	nebo
i	i	k9	i
úplná	úplný	k2eAgFnSc1d1	úplná
ztráta	ztráta	k1gFnSc1	ztráta
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
akcionáři	akcionář	k1gMnPc1	akcionář
vložili	vložit	k5eAaPmAgMnP	vložit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
akciemi	akcie	k1gFnPc7	akcie
rizikový	rizikový	k2eAgMnSc1d1	rizikový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
neuvedená	uvedený	k2eNgFnSc1d1	neuvedená
k	k	k7c3	k
obchodování	obchodování	k1gNnSc3	obchodování
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
je	být	k5eAaImIp3nS	být
soukromou	soukromý	k2eAgFnSc7d1	soukromá
firmou	firma	k1gFnSc7	firma
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
akciové	akciový	k2eAgFnPc1d1	akciová
společnosti	společnost	k1gFnPc1	společnost
vznikají	vznikat	k5eAaImIp3nP	vznikat
úpisem	úpis	k1gInSc7	úpis
akcií	akcie	k1gFnPc2	akcie
bez	bez	k7c2	bez
veřejné	veřejný	k2eAgFnSc2d1	veřejná
nabídky	nabídka	k1gFnSc2	nabídka
předem	předem	k6eAd1	předem
určenými	určený	k2eAgMnPc7d1	určený
upisovateli	upisovatel	k1gMnPc7	upisovatel
-	-	kIx~	-
zakladateli	zakladatel	k1gMnPc7	zakladatel
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účastí	účast	k1gFnSc7	účast
a	a	k8xC	a
hlasováním	hlasování	k1gNnSc7	hlasování
na	na	k7c6	na
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
se	se	k3xPyFc4	se
akcionář	akcionář	k1gMnSc1	akcionář
může	moct	k5eAaImIp3nS	moct
podílet	podílet	k5eAaImF	podílet
na	na	k7c4	na
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c6	o
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
jejího	její	k3xOp3gNnSc2	její
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
,	,	kIx,	,
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
každá	každý	k3xTgFnSc1	každý
akcie	akcie	k1gFnSc1	akcie
znamená	znamenat	k5eAaImIp3nS	znamenat
obvykle	obvykle	k6eAd1	obvykle
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
většinový	většinový	k2eAgMnSc1d1	většinový
akcionář	akcionář	k1gMnSc1	akcionář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
"	"	kIx"	"
<g/>
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
balík	balík	k1gInSc4	balík
<g/>
"	"	kIx"	"
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
o	o	k7c6	o
společnosti	společnost	k1gFnSc6	společnost
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
tak	tak	k6eAd1	tak
i	i	k8xC	i
poškodit	poškodit	k5eAaPmF	poškodit
menšinové	menšinový	k2eAgMnPc4d1	menšinový
akcionáře	akcionář	k1gMnPc4	akcionář
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
zájmy	zájem	k1gInPc1	zájem
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nP	muset
chránit	chránit	k5eAaImF	chránit
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
například	například	k6eAd1	například
určit	určit	k5eAaPmF	určit
procentní	procentní	k2eAgFnSc4d1	procentní
výši	výše	k1gFnSc4	výše
držení	držení	k1gNnSc2	držení
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvláštním	zvláštní	k2eAgNnPc3d1	zvláštní
právům	právo	k1gNnPc3	právo
menšinových	menšinový	k2eAgInPc2d1	menšinový
akcionářů	akcionář	k1gMnPc2	akcionář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc4	akcie
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
několika	několik	k4yIc2	několik
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Dělení	dělení	k1gNnSc1	dělení
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
podoby	podoba	k1gFnSc2	podoba
===	===	k?	===
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc1	akcie
listinné	listinný	k2eAgFnSc2d1	listinná
(	(	kIx(	(
<g/>
fyzické	fyzický	k2eAgFnSc2d1	fyzická
listiny	listina	k1gFnSc2	listina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc1	akcie
zaknihované	zaknihovaný	k2eAgFnPc1d1	zaknihovaná
(	(	kIx(	(
<g/>
zápis	zápis	k1gInSc1	zápis
v	v	k7c6	v
evidenci	evidence	k1gFnSc6	evidence
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dělení	dělení	k1gNnSc1	dělení
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
formy	forma	k1gFnSc2	forma
===	===	k?	===
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc1	akcie
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
je	být	k5eAaImIp3nS	být
vydávána	vydáván	k2eAgFnSc1d1	vydávána
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
určité	určitý	k2eAgFnSc2d1	určitá
osoby	osoba	k1gFnSc2	osoba
(	(	kIx(	(
<g/>
právnické	právnický	k2eAgFnPc1d1	právnická
nebo	nebo	k8xC	nebo
fyzické	fyzický	k2eAgFnPc1d1	fyzická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
posuzována	posuzovat	k5eAaImNgFnS	posuzovat
jako	jako	k8xC	jako
cenný	cenný	k2eAgInSc4d1	cenný
papír	papír	k1gInSc4	papír
na	na	k7c4	na
řad	řad	k1gInSc4	řad
<g/>
:	:	kIx,	:
v	v	k7c6	v
listinné	listinný	k2eAgFnSc6d1	listinná
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
převádí	převádět	k5eAaImIp3nS	převádět
rubopisem	rubopis	k1gInSc7	rubopis
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
indosamentem	indosament	k1gInSc7	indosament
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
fyzickým	fyzický	k2eAgNnSc7d1	fyzické
předáním	předání	k1gNnSc7	předání
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tradicí	tradice	k1gFnSc7	tradice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zaknihované	zaknihovaný	k2eAgFnSc6d1	zaknihovaná
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
převádí	převádět	k5eAaImIp3nS	převádět
smlouvou	smlouva	k1gFnSc7	smlouva
a	a	k8xC	a
registrací	registrace	k1gFnSc7	registrace
podle	podle	k7c2	podle
§	§	k?	§
21	[number]	k4	21
zák	zák	k?	zák
<g/>
.	.	kIx.	.
o	o	k7c6	o
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
pap	pap	k?	pap
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgFnSc1d2	lepší
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
odcizení	odcizení	k1gNnSc3	odcizení
<g/>
,	,	kIx,	,
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
obtížnější	obtížný	k2eAgFnSc4d2	obtížnější
prodejnost	prodejnost	k1gFnSc4	prodejnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
na	na	k7c6	na
ceně	cena	k1gFnSc6	cena
<g/>
.	.	kIx.	.
</s>
<s>
Převoditelnost	převoditelnost	k1gFnSc1	převoditelnost
akcií	akcie	k1gFnPc2	akcie
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
lze	lze	k6eAd1	lze
stanovami	stanova	k1gFnPc7	stanova
omezit	omezit	k5eAaPmF	omezit
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
)	)	kIx)	)
a	a	k8xC	a
vázat	vázat	k5eAaImF	vázat
tento	tento	k3xDgInSc4	tento
převod	převod	k1gInSc4	převod
na	na	k7c4	na
souhlas	souhlas	k1gInSc4	souhlas
některého	některý	k3yIgInSc2	některý
z	z	k7c2	z
orgánů	orgán	k1gInPc2	orgán
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc1	akcie
na	na	k7c4	na
majitele	majitel	k1gMnPc4	majitel
je	být	k5eAaImIp3nS	být
posuzována	posuzovat	k5eAaImNgFnS	posuzovat
jako	jako	k8xS	jako
cenný	cenný	k2eAgInSc4d1	cenný
papír	papír	k1gInSc4	papír
na	na	k7c4	na
doručitele	doručitel	k1gMnPc4	doručitel
<g/>
.	.	kIx.	.
</s>
<s>
Držitel	držitel	k1gMnSc1	držitel
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
anonymní	anonymní	k2eAgMnSc1d1	anonymní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zaknihované	zaknihovaný	k2eAgFnSc6d1	zaknihovaná
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
také	také	k9	také
převádějí	převádět	k5eAaImIp3nP	převádět
smlouvou	smlouva	k1gFnSc7	smlouva
a	a	k8xC	a
registrací	registrace	k1gFnSc7	registrace
podle	podle	k7c2	podle
§	§	k?	§
21	[number]	k4	21
zák	zák	k?	zák
<g/>
.	.	kIx.	.
o	o	k7c6	o
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
pap	pap	k?	pap
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listinné	listinný	k2eAgFnSc6d1	listinná
podobě	podoba	k1gFnSc6	podoba
stačí	stačit	k5eAaBmIp3nS	stačit
nicméně	nicméně	k8xC	nicméně
jen	jen	k9	jen
fyzické	fyzický	k2eAgNnSc4d1	fyzické
předání	předání	k1gNnSc4	předání
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tradicí	tradice	k1gFnSc7	tradice
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
třeba	třeba	k9	třeba
indosament	indosament	k1gInSc1	indosament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
snadná	snadný	k2eAgFnSc1d1	snadná
obchodovatelnost	obchodovatelnost	k1gFnSc1	obchodovatelnost
na	na	k7c6	na
sekundárních	sekundární	k2eAgInPc6d1	sekundární
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Akcie	akcie	k1gFnPc1	akcie
na	na	k7c4	na
majitele	majitel	k1gMnPc4	majitel
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
volně	volně	k6eAd1	volně
převoditelné	převoditelný	k2eAgNnSc1d1	převoditelné
<g/>
.	.	kIx.	.
<g/>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
za	za	k7c4	za
další	další	k2eAgFnSc4d1	další
formu	forma	k1gFnSc4	forma
neměly	mít	k5eNaImAgFnP	mít
být	být	k5eAaImF	být
považovány	považován	k2eAgFnPc1d1	považována
i	i	k8xC	i
zaknihované	zaknihovaný	k2eAgFnPc1d1	zaknihovaná
akcie	akcie	k1gFnPc1	akcie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
podoba	podoba	k1gFnSc1	podoba
akcií	akcie	k1gFnPc2	akcie
<g/>
)	)	kIx)	)
-	-	kIx~	-
byly	být	k5eAaImAgFnP	být
by	by	kYmCp3nP	by
tak	tak	k9	tak
nejen	nejen	k6eAd1	nejen
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
podobou	podoba	k1gFnSc7	podoba
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Násobkové	Násobkový	k2eAgFnPc4d1	Násobkový
(	(	kIx(	(
<g/>
hromadné	hromadný	k2eAgFnPc4d1	hromadná
<g/>
)	)	kIx)	)
akcie	akcie	k1gFnPc4	akcie
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
listinných	listinný	k2eAgFnPc2d1	listinná
akcií	akcie	k1gFnPc2	akcie
se	se	k3xPyFc4	se
připouští	připouštět	k5eAaImIp3nS	připouštět
vydání	vydání	k1gNnSc1	vydání
tzv.	tzv.	kA	tzv.
hromadných	hromadný	k2eAgFnPc2d1	hromadná
listin	listina	k1gFnPc2	listina
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
násobkových	násobkový	k2eAgFnPc2d1	násobkový
akcií	akcie	k1gFnPc2	akcie
představující	představující	k2eAgInSc1d1	představující
vždy	vždy	k6eAd1	vždy
určitý	určitý	k2eAgInSc1d1	určitý
počet	počet	k1gInSc1	počet
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
akcií	akcie	k1gFnPc2	akcie
vydaných	vydaný	k2eAgInPc2d1	vydaný
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
listinu	listina	k1gFnSc4	listina
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
násobky	násobek	k1gInPc1	násobek
po	po	k7c4	po
10	[number]	k4	10
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
100	[number]	k4	100
a	a	k8xC	a
více	hodně	k6eAd2	hodně
kusech	kus	k1gInPc6	kus
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
násobků	násobek	k1gInPc2	násobek
bankovek	bankovka	k1gFnPc2	bankovka
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
však	však	k9	však
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
úspor	úspora	k1gFnPc2	úspora
vydává	vydávat	k5eAaImIp3nS	vydávat
násobky	násobek	k1gInPc4	násobek
na	na	k7c4	na
přesný	přesný	k2eAgInSc4d1	přesný
počet	počet	k1gInSc4	počet
akcií	akcie	k1gFnPc2	akcie
připadající	připadající	k2eAgFnSc2d1	připadající
jednomu	jeden	k4xCgNnSc3	jeden
akcionáři	akcionář	k1gMnPc7	akcionář
(	(	kIx(	(
<g/>
např.	např.	kA	např.
3.531	[number]	k4	3.531
ks	ks	kA	ks
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hromadné	hromadný	k2eAgFnPc1d1	hromadná
listiny	listina	k1gFnPc1	listina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nS	muset
při	při	k7c6	při
převodech	převod	k1gInPc6	převod
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
dělení	dělení	k1gNnSc6	dělení
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
nepraktická	praktický	k2eNgFnSc1d1	nepraktická
řada	řada	k1gFnSc1	řada
nerovnoměrných	rovnoměrný	k2eNgInPc2d1	nerovnoměrný
násobků	násobek	k1gInPc2	násobek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
hromadné	hromadný	k2eAgFnSc2d1	hromadná
listiny	listina	k1gFnSc2	listina
za	za	k7c4	za
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
akcie	akcie	k1gFnPc4	akcie
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
zneplatnit	zneplatnit	k5eAaImF	zneplatnit
původní	původní	k2eAgFnPc4d1	původní
akcie	akcie	k1gFnPc4	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
hromadných	hromadný	k2eAgFnPc2d1	hromadná
akcií	akcie	k1gFnPc2	akcie
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
upraveno	upravit	k5eAaPmNgNnS	upravit
ve	v	k7c6	v
stanovách	stanova	k1gFnPc6	stanova
společnosti	společnost	k1gFnSc2	společnost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
hromadné	hromadný	k2eAgFnSc2d1	hromadná
akcie	akcie	k1gFnSc2	akcie
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zpoplatněno	zpoplatnit	k5eAaPmNgNnS	zpoplatnit
žadateli	žadatel	k1gMnPc7	žadatel
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
prvním	první	k4xOgNnSc6	první
vytištění	vytištění	k1gNnSc6	vytištění
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
vydání	vydání	k1gNnSc1	vydání
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
akcií	akcie	k1gFnPc2	akcie
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
hromadné	hromadný	k2eAgNnSc1d1	hromadné
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
akcionářovi	akcionář	k1gMnSc3	akcionář
umožněno	umožnit	k5eAaPmNgNnS	umožnit
kdykoli	kdykoli	k6eAd1	kdykoli
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
akcie	akcie	k1gFnSc1	akcie
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
musí	muset	k5eAaImIp3nS	muset
obdržet	obdržet	k5eAaPmF	obdržet
každý	každý	k3xTgMnSc1	každý
akcionář	akcionář	k1gMnSc1	akcionář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlasování	hlasování	k1gNnPc4	hlasování
u	u	k7c2	u
akcií	akcie	k1gFnPc2	akcie
===	===	k?	===
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
akcie	akcie	k1gFnSc1	akcie
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
společnost	společnost	k1gFnSc1	společnost
vydala	vydat	k5eAaPmAgFnS	vydat
různé	různý	k2eAgFnPc4d1	různá
jmenovité	jmenovitý	k2eAgFnPc4d1	jmenovitá
hodnoty	hodnota	k1gFnPc4	hodnota
(	(	kIx(	(
<g/>
nominální	nominální	k2eAgFnSc2d1	nominální
<g/>
)	)	kIx)	)
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hlasování	hlasování	k1gNnSc1	hlasování
upraveno	upravit	k5eAaPmNgNnS	upravit
např.	např.	kA	např.
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
hodnotě	hodnota	k1gFnSc3	hodnota
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
každých	každý	k3xTgFnPc2	každý
10	[number]	k4	10
Kč	Kč	kA	Kč
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
apod.	apod.	kA	apod.
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vydat	vydat	k5eAaPmF	vydat
i	i	k9	i
podílové	podílový	k2eAgFnPc4d1	podílová
(	(	kIx(	(
<g/>
dílcové	dílcový	k2eAgFnPc4d1	dílcová
<g/>
)	)	kIx)	)
akcie	akcie	k1gFnPc4	akcie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
každé	každý	k3xTgFnSc2	každý
akcie	akcie	k1gFnSc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zakladatelském	zakladatelský	k2eAgNnSc6d1	zakladatelské
tzv.	tzv.	kA	tzv.
grunderském	grunderský	k2eAgNnSc6d1	grunderský
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
nejčastější	častý	k2eAgInSc1d3	nejčastější
nominál	nominál	k1gInSc1	nominál
200	[number]	k4	200
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hodnotě	hodnota	k1gFnSc3	hodnota
řada	řada	k1gFnSc1	řada
firem	firma	k1gFnPc2	firma
vydala	vydat	k5eAaPmAgFnS	vydat
část	část	k1gFnSc1	část
akcií	akcie	k1gFnPc2	akcie
v	v	k7c6	v
polovinách	polovina	k1gFnPc6	polovina
po	po	k7c6	po
100	[number]	k4	100
zl	zl	k?	zl
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
akcie	akcie	k1gFnPc1	akcie
tak	tak	k6eAd1	tak
kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgNnSc2	svůj
čísla	číslo	k1gNnSc2	číslo
byla	být	k5eAaImAgFnS	být
označena	označen	k2eAgFnSc1d1	označena
signaturou	signatura	k1gFnSc7	signatura
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
nebo	nebo	k8xC	nebo
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
(	(	kIx(	(
<g/>
např.	např.	kA	např.
č.	č.	k?	č.
1023	[number]	k4	1023
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
aby	aby	kYmCp3nS	aby
každá	každý	k3xTgFnSc1	každý
polovina	polovina	k1gFnSc1	polovina
měla	mít	k5eAaImAgFnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dělo	dít	k5eAaImAgNnS	dít
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Stanovy	stanova	k1gFnPc1	stanova
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
upravovaly	upravovat	k5eAaImAgInP	upravovat
též	též	k9	též
minimum	minimum	k1gNnSc4	minimum
a	a	k8xC	a
maximum	maximum	k1gNnSc4	maximum
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
na	na	k7c6	na
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
připadalo	připadat	k5eAaImAgNnS	připadat
na	na	k7c4	na
každých	každý	k3xTgFnPc2	každý
10	[number]	k4	10
akcií	akcie	k1gFnPc2	akcie
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Omezování	omezování	k1gNnSc1	omezování
bylo	být	k5eAaImAgNnS	být
většinou	většinou	k6eAd1	většinou
u	u	k7c2	u
rolnických	rolnický	k2eAgInPc2d1	rolnický
cukrovarů	cukrovar	k1gInPc2	cukrovar
např.	např.	kA	např.
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
maximum	maximum	k1gNnSc1	maximum
bylo	být	k5eAaImAgNnS	být
100	[number]	k4	100
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
tedy	tedy	k9	tedy
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1.000	[number]	k4	1.000
akcií	akcie	k1gFnPc2	akcie
připadlo	připadnout	k5eAaPmAgNnS	připadnout
mu	on	k3xPp3gMnSc3	on
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
tak	tak	k6eAd1	tak
jen	jen	k9	jen
100	[number]	k4	100
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
měl	mít	k5eAaImAgInS	mít
jen	jen	k6eAd1	jen
9	[number]	k4	9
akcií	akcie	k1gFnPc2	akcie
neměl	mít	k5eNaImAgMnS	mít
hlas	hlas	k1gInSc4	hlas
žádný	žádný	k1gMnSc1	žádný
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
se	se	k3xPyFc4	se
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
spojit	spojit	k5eAaPmF	spojit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dělení	dělení	k1gNnSc1	dělení
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
druhu	druh	k1gInSc2	druh
===	===	k?	===
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc1	akcie
zakladatelské	zakladatelský	k2eAgFnPc1d1	zakladatelská
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
označeny	označit	k5eAaPmNgInP	označit
všechny	všechen	k3xTgInPc1	všechen
emise	emise	k1gFnPc4	emise
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
emitovány	emitován	k2eAgFnPc1d1	emitována
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
akcie	akcie	k1gFnSc1	akcie
je	být	k5eAaImIp3nS	být
však	však	k9	však
současně	současně	k6eAd1	současně
kmenovou	kmenový	k2eAgFnSc7d1	kmenová
akcií	akcie	k1gFnSc7	akcie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
zvýšení	zvýšení	k1gNnSc4	zvýšení
kapitálu	kapitál	k1gInSc2	kapitál
a	a	k8xC	a
vydání	vydání	k1gNnSc2	vydání
nové	nový	k2eAgFnSc2d1	nová
emise	emise	k1gFnSc2	emise
akcií	akcie	k1gFnPc2	akcie
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
každá	každý	k3xTgFnSc1	každý
další	další	k2eAgFnSc1d1	další
emise	emise	k1gFnSc1	emise
jen	jen	k6eAd1	jen
kmenovou	kmenový	k2eAgFnSc7d1	kmenová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc1	akcie
kmenové	kmenový	k2eAgFnPc1d1	kmenová
(	(	kIx(	(
<g/>
nejběžnější	běžný	k2eAgFnPc1d3	nejběžnější
<g/>
,	,	kIx,	,
s	s	k7c7	s
právem	právem	k6eAd1	právem
<g/>
:	:	kIx,	:
na	na	k7c4	na
dividendu	dividenda	k1gFnSc4	dividenda
(	(	kIx(	(
<g/>
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c4	na
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
-	-	kIx~	-
hlasovat	hlasovat	k5eAaImF	hlasovat
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
obdržet	obdržet	k5eAaPmF	obdržet
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
jednání	jednání	k1gNnSc4	jednání
valné	valný	k2eAgFnSc2d1	valná
hromady	hromada	k1gFnSc2	hromada
<g/>
;	;	kIx,	;
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
návrhy	návrh	k1gInPc4	návrh
či	či	k8xC	či
protinávrhy	protinávrh	k1gInPc4	protinávrh
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
likvidačním	likvidační	k2eAgInSc6d1	likvidační
zůstatku	zůstatek	k1gInSc6	zůstatek
)	)	kIx)	)
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnSc1	akcie
prioritní	prioritní	k2eAgFnSc1d1	prioritní
(	(	kIx(	(
<g/>
s	s	k7c7	s
přednostním	přednostní	k2eAgNnSc7d1	přednostní
právem	právo	k1gNnSc7	právo
na	na	k7c4	na
výplatu	výplata	k1gFnSc4	výplata
dividendy	dividenda	k1gFnSc2	dividenda
či	či	k8xC	či
na	na	k7c4	na
privilegovaný	privilegovaný	k2eAgInSc4d1	privilegovaný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
likvidačním	likvidační	k2eAgInSc6d1	likvidační
zůstatku	zůstatek	k1gInSc6	zůstatek
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
oboje	oboj	k1gFnPc4	oboj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
omezeným	omezený	k2eAgNnSc7d1	omezené
právem	právo	k1gNnSc7	právo
hlasovat	hlasovat	k5eAaImF	hlasovat
-	-	kIx~	-
toto	tento	k3xDgNnSc4	tento
omezení	omezení	k1gNnSc4	omezení
ovšem	ovšem	k9	ovšem
musí	muset	k5eAaImIp3nS	muset
uvádět	uvádět	k5eAaImF	uvádět
stanovy	stanova	k1gFnPc4	stanova
<g/>
,	,	kIx,	,
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prioritní	prioritní	k2eAgFnPc4d1	prioritní
akcie	akcie	k1gFnPc4	akcie
zkrátka	zkrátka	k6eAd1	zkrátka
přináší	přinášet	k5eAaImIp3nS	přinášet
svému	svůj	k3xOyFgMnSc3	svůj
majiteli	majitel	k1gMnSc3	majitel
nějakou	nějaký	k3yIgFnSc4	nějaký
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přednostní	přednostní	k2eAgFnSc4d1	přednostní
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc4d2	vyšší
dividendu	dividenda	k1gFnSc4	dividenda
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
omezením	omezení	k1gNnSc7	omezení
jiného	jiný	k2eAgNnSc2d1	jiné
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hlasovacího	hlasovací	k2eAgInSc2d1	hlasovací
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc1	akcie
zaměstnanecké	zaměstnanecký	k2eAgFnPc1d1	zaměstnanecká
(	(	kIx(	(
<g/>
byly	být	k5eAaImAgFnP	být
vydávány	vydávat	k5eAaImNgFnP	vydávat
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
za	za	k7c4	za
zvýhodněnou	zvýhodněný	k2eAgFnSc4d1	zvýhodněná
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
motivace	motivace	k1gFnSc1	motivace
-	-	kIx~	-
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
vydávány	vydáván	k2eAgInPc1d1	vydáván
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnaneckou	zaměstnanecký	k2eAgFnSc4d1	zaměstnanecká
akcii	akcie	k1gFnSc4	akcie
musel	muset	k5eAaImAgMnS	muset
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
ze	z	k7c2	z
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
zpět	zpět	k6eAd1	zpět
prodat	prodat	k5eAaPmF	prodat
firmě	firma	k1gFnSc3	firma
nebo	nebo	k8xC	nebo
jinému	jiný	k2eAgMnSc3d1	jiný
zaměstnanci	zaměstnanec	k1gMnSc3	zaměstnanec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
odcházel	odcházet	k5eAaImAgMnS	odcházet
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
změny	změna	k1gFnSc2	změna
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
akcii	akcie	k1gFnSc4	akcie
ponechat	ponechat	k5eAaPmF	ponechat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dědicové	dědic	k1gMnPc1	dědic
je	on	k3xPp3gMnPc4	on
museli	muset	k5eAaImAgMnP	muset
opět	opět	k6eAd1	opět
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
sami	sám	k3xTgMnPc1	sám
nebyli	být	k5eNaImAgMnP	být
zaměstnanci	zaměstnanec	k1gMnSc3	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Vydávání	vydávání	k1gNnSc1	vydávání
akcií	akcie	k1gFnPc2	akcie
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
následně	následně	k6eAd1	následně
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
akcií	akcie	k1gFnPc2	akcie
kmenových	kmenový	k2eAgFnPc2d1	kmenová
či	či	k8xC	či
prioritních	prioritní	k2eAgFnPc2d1	prioritní
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
<g/>
.	.	kIx.	.
zaměstnanecké	zaměstnanecký	k2eAgNnSc1d1	zaměstnanecké
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc1	akcie
zlaté	zlatý	k2eAgFnPc1d1	zlatá
(	(	kIx(	(
<g/>
byly	být	k5eAaImAgFnP	být
vydávány	vydávat	k5eAaImNgInP	vydávat
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
privatizací	privatizace	k1gFnSc7	privatizace
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Akciové	akciový	k2eAgFnPc1d1	akciová
společnosti	společnost	k1gFnPc1	společnost
nejsou	být	k5eNaImIp3nP	být
vynálezem	vynález	k1gInSc7	vynález
posledních	poslední	k2eAgInPc2d1	poslední
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
světa	svět	k1gInSc2	svět
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1602	[number]	k4	1602
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
východoindická	východoindický	k2eAgFnSc1d1	Východoindická
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Vereenigte	Vereenigt	k1gInSc5	Vereenigt
Oost-Indische	Oost-Indische	k1gInSc4	Oost-Indische
Compagnie	Compagnie	k1gFnPc4	Compagnie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Význam	význam	k1gInSc1	význam
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
názvu	název	k1gInSc6	název
===	===	k?	===
</s>
</p>
<p>
<s>
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
jako	jako	k8xS	jako
forma	forma	k1gFnSc1	forma
společenského	společenský	k2eAgNnSc2d1	společenské
podnikání	podnikání	k1gNnSc2	podnikání
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
potřebou	potřeba	k1gFnSc7	potřeba
větších	veliký	k2eAgFnPc2d2	veliký
kapitálových	kapitálový	k2eAgFnPc2d1	kapitálová
hodnot	hodnota	k1gFnPc2	hodnota
pro	pro	k7c4	pro
investice	investice	k1gFnPc4	investice
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
zámořských	zámořský	k2eAgFnPc6d1	zámořská
výpravách	výprava	k1gFnPc6	výprava
<g/>
,	,	kIx,	,
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
velkého	velký	k2eAgInSc2d1	velký
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nebyl	být	k5eNaImAgMnS	být
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
schopen	schopen	k2eAgMnSc1d1	schopen
shromáždit	shromáždit	k5eAaPmF	shromáždit
<g/>
.	.	kIx.	.
</s>
<s>
Hrozil	hrozit	k5eAaImAgInS	hrozit
mu	on	k3xPp3gNnSc3	on
také	také	k9	také
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neúspěchu	neúspěch	k1gInSc2	neúspěch
krach	krach	k1gInSc1	krach
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
na	na	k7c4	na
potřebný	potřebný	k2eAgInSc4d1	potřebný
kapitál	kapitál	k1gInSc4	kapitál
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
několik	několik	k4yIc1	několik
dílů	díl	k1gInPc2	díl
(	(	kIx(	(
<g/>
akcií	akcie	k1gFnPc2	akcie
<g/>
)	)	kIx)	)
od	od	k7c2	od
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
každý	každý	k3xTgMnSc1	každý
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
daném	daný	k2eAgInSc6d1	daný
obchodu	obchod	k1gInSc2	obchod
svým	svůj	k3xOyFgNnSc7	svůj
dílem	dílo	k1gNnSc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
jednorázovému	jednorázový	k2eAgInSc3d1	jednorázový
obchodu	obchod	k1gInSc3	obchod
byla	být	k5eAaImAgFnS	být
forma	forma	k1gFnSc1	forma
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
zaměřena	zaměřen	k2eAgFnSc1d1	zaměřena
se	s	k7c7	s
stejnými	stejný	k2eAgNnPc7d1	stejné
pravidly	pravidlo	k1gNnPc7	pravidlo
na	na	k7c4	na
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podoba	podoba	k1gFnSc1	podoba
listinných	listinný	k2eAgFnPc2d1	listinná
akcií	akcie	k1gFnPc2	akcie
===	===	k?	===
</s>
</p>
<p>
<s>
Akcie	akcie	k1gFnPc4	akcie
je	být	k5eAaImIp3nS	být
cenina	cenina	k1gFnSc1	cenina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
taková	takový	k3xDgFnSc1	takový
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
chráněna	chránit	k5eAaImNgFnS	chránit
proti	proti	k7c3	proti
padělání	padělání	k1gNnSc3	padělání
<g/>
.	.	kIx.	.
</s>
<s>
Zájmem	zájem	k1gInSc7	zájem
každé	každý	k3xTgFnSc2	každý
společnosti	společnost	k1gFnSc2	společnost
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
omezení	omezení	k1gNnSc1	omezení
možnosti	možnost	k1gFnSc2	možnost
napadení	napadení	k1gNnSc2	napadení
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Tisk	tisk	k1gInSc1	tisk
cenin	cenina	k1gFnPc2	cenina
v	v	k7c6	v
chráněném	chráněný	k2eAgInSc6d1	chráněný
režimu	režim	k1gInSc6	režim
kontroly	kontrola	k1gFnSc2	kontrola
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nejsou	být	k5eNaImIp3nP	být
volně	volně	k6eAd1	volně
získatelné	získatelný	k2eAgInPc1d1	získatelný
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
doménou	doména	k1gFnSc7	doména
tiskáren	tiskárna	k1gFnPc2	tiskárna
cenin	cenina	k1gFnPc2	cenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
doposud	doposud	k6eAd1	doposud
neplatí	platit	k5eNaImIp3nS	platit
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
povinnosti	povinnost	k1gFnPc4	povinnost
tisku	tisk	k1gInSc2	tisk
akcií	akcie	k1gFnPc2	akcie
v	v	k7c6	v
ceninové	ceninový	k2eAgFnSc6d1	ceninový
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
vypracovalo	vypracovat	k5eAaPmAgNnS	vypracovat
MF	MF	kA	MF
ČR	ČR	kA	ČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ho	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
neuznal	uznat	k5eNaPmAgMnS	uznat
za	za	k7c4	za
podstatný	podstatný	k2eAgInSc4d1	podstatný
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
akcie	akcie	k1gFnSc1	akcie
může	moct	k5eAaImIp3nS	moct
vytisknout	vytisknout	k5eAaPmF	vytisknout
každý	každý	k3xTgInSc4	každý
třeba	třeba	k6eAd1	třeba
na	na	k7c6	na
domácím	domácí	k2eAgInSc6d1	domácí
počítači	počítač	k1gInSc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
vypadají	vypadat	k5eAaImIp3nP	vypadat
akcie	akcie	k1gFnPc1	akcie
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nepovažují	považovat	k5eNaImIp3nP	považovat
akcie	akcie	k1gFnPc4	akcie
za	za	k7c4	za
důležitý	důležitý	k2eAgInSc4d1	důležitý
dokument	dokument	k1gInSc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Akcie	akcie	k1gFnPc4	akcie
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
dokladem	doklad	k1gInSc7	doklad
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
opravňujícím	opravňující	k2eAgInSc7d1	opravňující
dokladem	doklad	k1gInSc7	doklad
akcionáře	akcionář	k1gMnSc2	akcionář
k	k	k7c3	k
výkonu	výkon	k1gInSc2	výkon
jeho	jeho	k3xOp3gNnPc2	jeho
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
bank	banka	k1gFnPc2	banka
dnes	dnes	k6eAd1	dnes
odmítá	odmítat	k5eAaImIp3nS	odmítat
přijímat	přijímat	k5eAaImF	přijímat
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
neceninové	ceninový	k2eNgInPc4d1	ceninový
cenné	cenný	k2eAgInPc4d1	cenný
papíry	papír	k1gInPc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Tisk	tisk	k1gInSc1	tisk
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
otázkou	otázka	k1gFnSc7	otázka
ochranných	ochranný	k2eAgInPc2d1	ochranný
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kontroly	kontrola	k1gFnPc4	kontrola
nad	nad	k7c7	nad
vydaným	vydaný	k2eAgInSc7d1	vydaný
počtem	počet	k1gInSc7	počet
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
vizitkou	vizitka	k1gFnSc7	vizitka
každé	každý	k3xTgFnSc2	každý
alespoň	alespoň	k9	alespoň
trochu	trochu	k6eAd1	trochu
solidní	solidní	k2eAgFnPc1d1	solidní
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
listinné	listinný	k2eAgFnSc3d1	listinná
akcii	akcie	k1gFnSc3	akcie
patří	patřit	k5eAaImIp3nS	patřit
kuponový	kuponový	k2eAgInSc4d1	kuponový
arch	arch	k1gInSc4	arch
k	k	k7c3	k
výplatě	výplata	k1gFnSc3	výplata
dividend	dividenda	k1gFnPc2	dividenda
<g/>
.	.	kIx.	.
</s>
<s>
Kupón	kupón	k1gInSc1	kupón
k	k	k7c3	k
akcii	akcie	k1gFnSc3	akcie
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vydán	vydat	k5eAaPmNgInS	vydat
i	i	k9	i
na	na	k7c4	na
doručitele	doručitel	k1gMnPc4	doručitel
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
dluhopisů	dluhopis	k1gInPc2	dluhopis
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
předložitel	předložitel	k1gMnSc1	předložitel
kuponu	kupon	k1gInSc2	kupon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
vlastník	vlastník	k1gMnSc1	vlastník
akcie	akcie	k1gFnSc2	akcie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výkyvy	výkyv	k1gInPc4	výkyv
cen	cena	k1gFnPc2	cena
==	==	k?	==
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
akcie	akcie	k1gFnSc2	akcie
se	se	k3xPyFc4	se
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nabídky	nabídka	k1gFnSc2	nabídka
a	a	k8xC	a
poptávky	poptávka	k1gFnSc2	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
každá	každý	k3xTgFnSc1	každý
komodita	komodita	k1gFnSc1	komodita
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
je	být	k5eAaImIp3nS	být
i	i	k9	i
cena	cena	k1gFnSc1	cena
akcie	akcie	k1gFnSc1	akcie
tím	ten	k3xDgNnSc7	ten
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
poptávka	poptávka	k1gFnSc1	poptávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
nabídka	nabídka	k1gFnSc1	nabídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
změnách	změna	k1gFnPc6	změna
nabídky	nabídka	k1gFnSc2	nabídka
a	a	k8xC	a
poptávky	poptávka	k1gFnSc2	poptávka
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
řada	řada	k1gFnSc1	řada
různých	různý	k2eAgInPc2d1	různý
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
výsledky	výsledek	k1gInPc4	výsledek
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
očekávání	očekávání	k1gNnSc6	očekávání
trhu	trh	k1gInSc2	trh
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
a	a	k8xC	a
srovnání	srovnání	k1gNnSc6	srovnání
těchto	tento	k3xDgFnPc2	tento
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
úroveň	úroveň	k1gFnSc1	úroveň
důvěry	důvěra	k1gFnSc2	důvěra
v	v	k7c4	v
makroekonomiku	makroekonomika	k1gFnSc4	makroekonomika
<g/>
,	,	kIx,	,
důvěra	důvěra	k1gFnSc1	důvěra
v	v	k7c4	v
segment	segment	k1gInSc4	segment
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
akcie	akcie	k1gFnSc1	akcie
nachází	nacházet	k5eAaImIp3nS	nacházet
či	či	k8xC	či
důvěra	důvěra	k1gFnSc1	důvěra
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
emisi	emise	k1gFnSc6	emise
akcie	akcie	k1gFnSc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
mediální	mediální	k2eAgInSc4d1	mediální
obraz	obraz	k1gInSc4	obraz
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
citací	citace	k1gFnPc2	citace
v	v	k7c6	v
negativních	negativní	k2eAgFnPc6d1	negativní
zprávách	zpráva	k1gFnPc6	zpráva
nebo	nebo	k8xC	nebo
přesun	přesun	k1gInSc4	přesun
investic	investice	k1gFnPc2	investice
mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
instrumenty	instrument	k1gInPc7	instrument
finančních	finanční	k2eAgInPc2d1	finanční
trhů	trh	k1gInPc2	trh
(	(	kIx(	(
<g/>
akcie	akcie	k1gFnPc1	akcie
<g/>
,	,	kIx,	,
dluhopisy	dluhopis	k1gInPc1	dluhopis
<g/>
,	,	kIx,	,
měny	měna	k1gFnPc1	měna
<g/>
,	,	kIx,	,
komodity	komodita	k1gFnPc1	komodita
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
faktory	faktor	k1gInPc1	faktor
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
odhalit	odhalit	k5eAaPmF	odhalit
a	a	k8xC	a
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
dvě	dva	k4xCgFnPc4	dva
disciplíny	disciplína	k1gFnPc4	disciplína
fundamentální	fundamentální	k2eAgFnSc1d1	fundamentální
analýza	analýza	k1gFnSc1	analýza
a	a	k8xC	a
technická	technický	k2eAgFnSc1d1	technická
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
pomocí	pomocí	k7c2	pomocí
jiných	jiný	k2eAgFnPc2d1	jiná
metod	metoda	k1gFnPc2	metoda
a	a	k8xC	a
přístupů	přístup	k1gInPc2	přístup
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fundamentální	fundamentální	k2eAgFnSc1d1	fundamentální
analýza	analýza	k1gFnSc1	analýza
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
dlouhodobé	dlouhodobý	k2eAgMnPc4d1	dlouhodobý
investory	investor	k1gMnPc4	investor
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
technickou	technický	k2eAgFnSc4d1	technická
analýzu	analýza	k1gFnSc4	analýza
využíji	využí	k6eAd2	využí
spíše	spíše	k9	spíše
intradenní	intradenní	k2eAgMnPc1d1	intradenní
či	či	k8xC	či
swingoví	swingový	k2eAgMnPc1d1	swingový
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
TEPPER	TEPPER	kA	TEPPER
<g/>
,	,	kIx,	,
T.	T.	kA	T.
-	-	kIx~	-
KÁPL	kápnout	k5eAaPmAgInS	kápnout
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Peníze	peníz	k1gInPc1	peníz
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
ŽÁK	Žák	k1gMnSc1	Žák
<g/>
,	,	kIx,	,
M.	M.	kA	M.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
KOTÁSEK	KOTÁSEK	kA	KOTÁSEK
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
POKORNÁ	Pokorná	k1gFnSc1	Pokorná
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
<g/>
.	.	kIx.	.
</s>
<s>
Kurs	kurs	k1gInSc1	kurs
obchodního	obchodní	k2eAgNnSc2d1	obchodní
práva	právo	k1gNnSc2	právo
:	:	kIx,	:
právo	právo	k1gNnSc1	právo
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
C.H.	C.H.	k1gMnSc1	C.H.
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgInSc1d1	obchodní
zákoník	zákoník	k1gInSc1	zákoník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
</s>
</p>
<p>
<s>
Konečný	Konečný	k1gMnSc1	Konečný
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
:	:	kIx,	:
Akcie	akcie	k1gFnSc1	akcie
a	a	k8xC	a
burza	burza	k1gFnSc1	burza
-	-	kIx~	-
jediná	jediný	k2eAgFnSc1d1	jediná
kniha	kniha	k1gFnSc1	kniha
kterou	který	k3yQgFnSc4	který
potřebuješ	potřebovat	k5eAaImIp2nS	potřebovat
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
</s>
</p>
<p>
<s>
Index	index	k1gInSc1	index
PX	PX	kA	PX
</s>
</p>
<p>
<s>
Investice	investice	k1gFnPc1	investice
</s>
</p>
<p>
<s>
Investiční	investiční	k2eAgInPc1d1	investiční
certifikáty	certifikát	k1gInPc1	certifikát
</s>
</p>
<p>
<s>
Kapitál	kapitál	k1gInSc1	kapitál
</s>
</p>
<p>
<s>
Kapitálový	kapitálový	k2eAgInSc1d1	kapitálový
trh	trh	k1gInSc1	trh
</s>
</p>
<p>
<s>
Podílový	podílový	k2eAgInSc1d1	podílový
fond	fond	k1gInSc1	fond
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
akcie	akcie	k1gFnSc2	akcie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
akcie	akcie	k1gFnSc2	akcie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Definice	definice	k1gFnPc1	definice
akcie	akcie	k1gFnPc1	akcie
-	-	kIx~	-
RM-SYSTÉM	RM-SYSTÉM	k1gFnSc1	RM-SYSTÉM
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
burza	burza	k1gFnSc1	burza
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
===	===	k?	===
Burzy	burza	k1gFnPc1	burza
a	a	k8xC	a
depozitáře	depozitář	k1gInPc1	depozitář
===	===	k?	===
</s>
</p>
<p>
<s>
Burza	burza	k1gFnSc1	burza
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
RM	RM	kA	RM
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgInSc1d1	centrální
depozitář	depozitář	k1gInSc1	depozitář
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
