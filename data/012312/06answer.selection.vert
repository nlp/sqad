<s>
Akcie	akcie	k1gFnSc1	akcie
je	být	k5eAaImIp3nS	být
cenný	cenný	k2eAgInSc4d1	cenný
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
majitel	majitel	k1gMnSc1	majitel
(	(	kIx(	(
<g/>
držitel	držitel	k1gMnSc1	držitel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akcionář	akcionář	k1gMnSc1	akcionář
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
že	že	k8xS	že
vložil	vložit	k5eAaPmAgInS	vložit
určitý	určitý	k2eAgInSc1d1	určitý
majetkový	majetkový	k2eAgInSc1d1	majetkový
podíl	podíl	k1gInSc1	podíl
(	(	kIx(	(
<g/>
kapitál	kapitál	k1gInSc1	kapitál
<g/>
)	)	kIx)	)
do	do	k7c2	do
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
