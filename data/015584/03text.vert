<s>
Japonské	japonský	k2eAgInPc1d1
válečné	válečný	k2eAgInPc1d1
zločiny	zločin	k1gInPc1
</s>
<s>
Zaživa	zaživa	k6eAd1
pohřbívaní	pohřbívaný	k2eAgMnPc1d1
čínští	čínský	k2eAgMnPc1d1
zajatci	zajatec	k1gMnPc1
</s>
<s>
K	k	k7c3
japonským	japonský	k2eAgMnPc3d1
válečným	válečný	k2eAgMnPc3d1
zločinům	zločin	k1gMnPc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
mnoha	mnoho	k4c6
asijských	asijský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
během	během	k7c2
období	období	k1gNnSc2
japonského	japonský	k2eAgInSc2d1
militarismu	militarismus	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
za	za	k7c2
druhé	druhý	k4xOgFnSc2
čínsko-japonské	čínsko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
těchto	tento	k3xDgInPc2
incidentů	incident	k1gInPc2
jsou	být	k5eAaImIp3nP
popisovány	popisován	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
asijský	asijský	k2eAgInSc1d1
holocaust	holocaust	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
japonská	japonský	k2eAgNnPc1d1
zvěrstva	zvěrstvo	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgNnSc1
z	z	k7c2
válečných	válečný	k2eAgInPc2d1
zločinů	zločin	k1gInPc2
byly	být	k5eAaImAgInP
vojenským	vojenský	k2eAgInSc7d1
personálem	personál	k1gInSc7
japonského	japonský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
spáchány	spáchán	k2eAgFnPc1d1
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
většina	většina	k1gFnSc1
se	se	k3xPyFc4
rozmohla	rozmoct	k5eAaPmAgFnS
během	během	k7c2
první	první	k4xOgFnSc2
části	část	k1gFnSc2
období	období	k1gNnSc2
Šówa	Šówum	k1gNnSc2
<g/>
,	,	kIx,
název	název	k1gInSc1
pro	pro	k7c4
dobu	doba	k1gFnSc4
panování	panování	k1gNnSc2
císaře	císař	k1gMnSc2
Hirohita	Hirohit	k1gMnSc2
<g/>
,	,	kIx,
po	po	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
Japonska	Japonsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historici	historik	k1gMnPc1
a	a	k8xC
vlády	vláda	k1gFnPc1
některých	některý	k3yIgFnPc2
zemí	zem	k1gFnPc2
držených	držený	k2eAgFnPc2d1
japonskými	japonský	k2eAgFnPc7d1
vojenskými	vojenský	k2eAgFnPc7d1
sílami	síla	k1gFnPc7
<g/>
,	,	kIx,
jmenovitě	jmenovitě	k6eAd1
císařskou	císařský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
své	svůj	k3xOyFgFnSc2
tajné	tajný	k2eAgFnSc2d1
policie	policie	k1gFnSc2
Kempeitai	Kempeita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
císařské	císařský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
a	a	k8xC
císařskou	císařský	k2eAgFnSc4d1
rodinu	rodina	k1gFnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
císaře	císař	k1gMnSc4
Hirohita	Hirohit	k1gMnSc4
<g/>
,	,	kIx,
jim	on	k3xPp3gMnPc3
dávají	dávat	k5eAaImIp3nP
odpovědnost	odpovědnost	k1gFnSc4
za	za	k7c4
vraždy	vražda	k1gFnPc4
a	a	k8xC
další	další	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
spáchané	spáchaný	k2eAgInPc4d1
na	na	k7c6
milionech	milion	k4xCgInPc6
civilistech	civilista	k1gMnPc6
a	a	k8xC
válečných	válečný	k2eAgFnPc6d1
<g />
.	.	kIx.
</s>
<s hack="1">
zajatcích	zajatec	k1gMnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
japonští	japonský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
se	se	k3xPyFc4
k	k	k7c3
páchání	páchání	k1gNnSc3
těchto	tento	k3xDgInPc2
zločinů	zločin	k1gInPc2
přiznali	přiznat	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zločiny	zločin	k1gInPc1
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
z	z	k7c2
30	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
je	být	k5eAaImIp3nS
často	často	k6eAd1
přirovnávána	přirovnáván	k2eAgFnSc1d1
k	k	k7c3
armádě	armáda	k1gFnSc3
nacistického	nacistický	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
z	z	k7c2
let	léto	k1gNnPc2
1933-45	1933-45	k4
kvůli	kvůli	k7c3
rozsahu	rozsah	k1gInSc3
utrpení	utrpení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
diskuse	diskuse	k1gFnPc1
o	o	k7c4
roli	role	k1gFnSc4
Japonska	Japonsko	k1gNnSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
točí	točit	k5eAaImIp3nS
kolem	kolem	k7c2
úmrtnosti	úmrtnost	k1gFnSc2
válečných	válečný	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
a	a	k8xC
civilistů	civilista	k1gMnPc2
pod	pod	k7c7
japonskou	japonský	k2eAgFnSc7d1
okupací	okupace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
<g/>
,	,	kIx,
Malajsko	Malajsko	k1gNnSc4
<g/>
,	,	kIx,
Singapur	Singapur	k1gInSc4
a	a	k8xC
Hongkong	Hongkong	k1gInSc4
</s>
<s>
Hořící	hořící	k2eAgFnSc1d1
USS	USS	kA
Arizona	Arizona	k1gFnSc1
během	během	k7c2
útoku	útok	k1gInSc2
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Článek	článek	k1gInSc1
1	#num#	k4
Haagských	haagský	k2eAgFnPc2d1
úmluv	úmluva	k1gFnPc2
III	III	kA
z	z	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
-	-	kIx~
"	"	kIx"
<g/>
zahájení	zahájení	k1gNnSc1
války	válka	k1gFnSc2
<g/>
"	"	kIx"
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
zakázáno	zakázán	k2eAgNnSc4d1
zahájení	zahájení	k1gNnSc4
nepřátelských	přátelský	k2eNgFnPc2d1
akcí	akce	k1gFnPc2
proti	proti	k7c3
neutrálním	neutrální	k2eAgFnPc3d1
mocnostem	mocnost	k1gFnPc3
"	"	kIx"
<g/>
bez	bez	k7c2
předchozího	předchozí	k2eAgNnSc2d1
a	a	k8xC
výslovného	výslovný	k2eAgNnSc2d1
upozornění	upozornění	k1gNnSc2
<g/>
,	,	kIx,
buď	buď	k8xC
ve	v	k7c6
formě	forma	k1gFnSc6
odůvodněného	odůvodněný	k2eAgNnSc2d1
vyhlášení	vyhlášení	k1gNnSc2
války	válka	k1gFnSc2
nebo	nebo	k8xC
ultimáta	ultimátum	k1gNnSc2
s	s	k7c7
podmíněným	podmíněný	k2eAgInSc7d1
vyhlášení	vyhlášení	k1gNnSc4
války	válka	k1gFnSc2
a	a	k8xC
"	"	kIx"
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
článek	článek	k1gInSc1
2	#num#	k4
dále	daleko	k6eAd2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
skutečnost	skutečnost	k1gFnSc1
válečného	válečný	k2eAgInSc2d1
stavu	stav	k1gInSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
neutrálním	neutrální	k2eAgFnPc3d1
mocnostem	mocnost	k1gFnPc3
oznámena	oznámit	k5eAaPmNgFnS
neprodleně	prodleně	k6eNd1
a	a	k8xC
nesmí	smět	k5eNaImIp3nS
vstoupit	vstoupit	k5eAaPmF
v	v	k7c4
platnost	platnost	k1gFnSc4
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
nim	on	k3xPp3gMnPc3
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
po	po	k7c6
obdržení	obdržení	k1gNnSc6
oznámení	oznámení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
však	však	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dáno	dát	k5eAaPmNgNnS
telegraficky	telegraficky	k6eAd1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Japonští	japonský	k2eAgMnPc1d1
diplomaté	diplomat	k1gMnPc1
plánovali	plánovat	k5eAaImAgMnP
oznámit	oznámit	k5eAaPmF
USA	USA	kA
třicet	třicet	k4xCc4
minut	minuta	k1gFnPc2
před	před	k7c7
útokem	útok	k1gInSc7
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
místo	místo	k7c2
toho	ten	k3xDgNnSc2
bylo	být	k5eAaImAgNnS
americké	americký	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
doručeno	doručen	k2eAgNnSc4d1
hodinu	hodina	k1gFnSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
útok	útok	k1gInSc4
skončil	skončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tokio	Tokio	k1gNnSc1
zaslalo	zaslat	k5eAaPmAgNnS
japonskému	japonský	k2eAgNnSc3d1
velvyslanectví	velvyslanectví	k1gNnSc3
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
oznámení	oznámení	k1gNnSc2
o	o	k7c6
5000	#num#	k4
slovech	slovo	k1gNnPc6
(	(	kIx(
<g/>
známé	známý	k2eAgFnPc4d1
jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
14	#num#	k4
<g/>
-Part	-Part	k1gInSc1
Message	Message	k1gInSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
ve	v	k7c6
dvou	dva	k4xCgInPc6
blocích	blok	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
přepis	přepis	k1gInSc1
zprávy	zpráva	k1gFnSc2
trval	trvat	k5eAaImAgInS
příliš	příliš	k6eAd1
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gMnSc4
japonský	japonský	k2eAgMnSc1d1
velvyslanec	velvyslanec	k1gMnSc1
dodal	dodat	k5eAaPmAgMnS
včas	včas	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc4
dokument	dokument	k1gInSc4
byla	být	k5eAaImAgFnS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
zpráva	zpráva	k1gFnSc1
americkým	americký	k2eAgMnPc3d1
představitelům	představitel	k1gMnPc3
<g/>
,	,	kIx,
že	že	k8xS
mírová	mírový	k2eAgNnPc4d1
jednání	jednání	k1gNnPc4
mezi	mezi	k7c7
Japonskem	Japonsko	k1gNnSc7
a	a	k8xC
USA	USA	kA
budou	být	k5eAaImBp3nP
pravděpodobně	pravděpodobně	k6eAd1
ukončena	ukončit	k5eAaPmNgFnS
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
vyhlášení	vyhlášení	k1gNnSc4
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonští	japonský	k2eAgMnPc1d1
úředníci	úředník	k1gMnPc1
si	se	k3xPyFc3
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
byli	být	k5eAaImAgMnP
dobře	dobře	k6eAd1
vědomi	vědom	k2eAgMnPc1d1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
zpráva	zpráva	k1gFnSc1
nebyla	být	k5eNaImAgFnS
řádným	řádný	k2eAgNnSc7d1
vyhlášením	vyhlášení	k1gNnSc7
války	válka	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
Haagská	haagský	k2eAgFnSc1d1
úmluva	úmluva	k1gFnSc1
III	III	kA
z	z	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
"	"	kIx"
<g/>
zahájení	zahájení	k1gNnSc6
války	válka	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodli	rozhodnout	k5eAaPmAgMnP
se	se	k3xPyFc4
nevydat	vydat	k5eNaPmF
řádné	řádný	k2eAgNnSc4d1
vyhlášení	vyhlášení	k1gNnSc4
války	válka	k1gFnSc2
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k6eAd1
se	se	k3xPyFc4
báli	bát	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
takový	takový	k3xDgInSc1
postup	postup	k1gInSc1
odhalil	odhalit	k5eAaPmAgInS
možný	možný	k2eAgInSc1d1
únik	únik	k1gInSc1
tajných	tajný	k2eAgFnPc2d1
operací	operace	k1gFnPc2
Američanům	Američan	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgFnPc4
konspirační	konspirační	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
viní	vinit	k5eAaImIp3nP
amerického	americký	k2eAgMnSc4d1
prezidenta	prezident	k1gMnSc4
Roosevelta	Roosevelt	k1gMnSc4
<g/>
,	,	kIx,
že	že	k8xS
nechal	nechat	k5eAaPmAgMnS
útok	útok	k1gInSc4
ochotně	ochotně	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
umožnit	umožnit	k5eAaPmF
s	s	k7c7
cílem	cíl	k1gInSc7
vytvořit	vytvořit	k5eAaPmF
záminku	záminka	k1gFnSc4
k	k	k7c3
válce	válka	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
toto	tento	k3xDgNnSc4
tvrzení	tvrzení	k1gNnSc4
není	být	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
věrohodný	věrohodný	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Den	den	k1gInSc1
po	po	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
Japonsko	Japonsko	k1gNnSc1
vyhlásilo	vyhlásit	k5eAaPmAgNnS
USA	USA	kA
válku	válka	k1gFnSc4
a	a	k8xC
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
v	v	k7c6
reakci	reakce	k1gFnSc6
vyhlásily	vyhlásit	k5eAaPmAgInP
válku	válka	k1gFnSc4
Japonsku	Japonsko	k1gNnSc3
ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Současně	současně	k6eAd1
s	s	k7c7
útokem	útok	k1gInSc7
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
(	(	kIx(
<g/>
Havajského	havajský	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
)	)	kIx)
napadlo	napadnout	k5eAaPmAgNnS
Japonsko	Japonsko	k1gNnSc1
britskou	britský	k2eAgFnSc4d1
kolonii	kolonie	k1gFnSc4
Malajsko	Malajsko	k1gNnSc1
a	a	k8xC
bombardovalo	bombardovat	k5eAaImAgNnS
Singapur	Singapur	k1gInSc4
a	a	k8xC
Hongkong	Hongkong	k1gInSc4
bez	bez	k7c2
vyhlášení	vyhlášení	k1gNnSc2
války	válka	k1gFnSc2
nebo	nebo	k8xC
ultimáta	ultimátum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
USA	USA	kA
a	a	k8xC
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
v	v	k7c6
době	doba	k1gFnSc6
napadení	napadení	k1gNnSc4
svých	svůj	k3xOyFgFnPc2
území	území	k1gNnSc2
Japonskem	Japonsko	k1gNnSc7
bez	bez	k7c2
výslovného	výslovný	k2eAgNnSc2d1
varování	varování	k1gNnSc2
o	o	k7c6
válečném	válečný	k2eAgInSc6d1
stavu	stav	k1gInSc6
neutrální	neutrální	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Masové	masový	k2eAgNnSc1d1
zabíjení	zabíjení	k1gNnSc1
</s>
<s>
Sü-čou	Sü-čou	k6eAd1
v	v	k7c6
Číně	Čína	k1gFnSc6
roku	rok	k1gInSc2
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkop	příkop	k1gInSc1
plný	plný	k2eAgInSc1d1
čínských	čínský	k2eAgMnPc2d1
civilistů	civilista	k1gMnPc2
zabitých	zabitý	k2eAgFnPc2d1
japonskými	japonský	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
odhadů	odhad	k1gInPc2
R.	R.	kA
J.	J.	kA
Rummela	Rummel	k1gMnSc2
<g/>
,	,	kIx,
profesora	profesor	k1gMnSc2
politických	politický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
na	na	k7c6
havajské	havajský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
zabili	zabít	k5eAaPmAgMnP
japonští	japonský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
v	v	k7c6
letech	léto	k1gNnPc6
1937	#num#	k4
a	a	k8xC
1945	#num#	k4
kolem	kolem	k7c2
3	#num#	k4
až	až	k9
10	#num#	k4
milionů	milion	k4xCgInPc2
a	a	k8xC
více	hodně	k6eAd2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
největší	veliký	k2eAgFnSc7d3
pravděpodobností	pravděpodobnost	k1gFnSc7
6	#num#	k4
milionů	milion	k4xCgInPc2
Číňanů	Číňan	k1gMnPc2
<g/>
,	,	kIx,
obyvatel	obyvatel	k1gMnPc2
Tchaj-wanu	Tchaj-wan	k1gInSc2
<g/>
,	,	kIx,
Singapuru	Singapur	k1gInSc2
<g/>
,	,	kIx,
Malajsie	Malajsie	k1gFnSc2
<g/>
,	,	kIx,
Indonésanů	Indonésan	k1gMnPc2
<g/>
,	,	kIx,
Korejců	Korejec	k1gMnPc2
<g/>
,	,	kIx,
Filipínců	Filipínec	k1gMnPc2
a	a	k8xC
obyvatel	obyvatel	k1gMnPc2
Indočíny	Indočína	k1gFnSc2
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
i	i	k9
západních	západní	k2eAgMnPc2d1
válečných	válečný	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Rummela	Rummel	k1gMnSc2
<g/>
,	,	kIx,
"	"	kIx"
<g/>
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
democida	democida	k1gFnSc1
způsobena	způsobit	k5eAaPmNgFnS
morálně	morálně	k6eAd1
zkrachovalou	zkrachovalý	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
a	a	k8xC
vojenskou	vojenský	k2eAgFnSc4d1
strategií	strategie	k1gFnSc7
<g/>
,	,	kIx,
vojenskou	vojenský	k2eAgFnSc7d1
účelností	účelnost	k1gFnSc7
<g/>
,	,	kIx,
zvyky	zvyk	k1gInPc7
a	a	k8xC
národní	národní	k2eAgFnSc1d1
kulturou	kultura	k1gFnSc7
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
bylo	být	k5eAaImAgNnS
podle	podle	k7c2
Rummela	Rummel	k1gMnSc2
jen	jen	k9
v	v	k7c6
samotné	samotný	k2eAgFnSc6d1
Číně	Čína	k1gFnSc6
v	v	k7c6
letech	let	k1gInPc6
1937	#num#	k4
<g/>
–	–	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
45	#num#	k4
zabito	zabít	k5eAaPmNgNnS
zhruba	zhruba	k6eAd1
3,9	3,9	k4
milionů	milion	k4xCgInPc2
Číňanů	Číňan	k1gMnPc2
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
civilistů	civilista	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
přímý	přímý	k2eAgInSc1d1
důsledek	důsledek	k1gInSc1
japonských	japonský	k2eAgFnPc2d1
operací	operace	k1gFnPc2
a	a	k8xC
10,2	10,2	k4
milionu	milion	k4xCgInSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Asi	asi	k9
nejvíce	nejvíce	k6eAd1,k6eAd3
vešel	vejít	k5eAaPmAgInS
ve	v	k7c4
známost	známost	k1gFnSc4
letech	let	k1gInPc6
1937	#num#	k4
<g/>
–	–	k?
<g/>
38	#num#	k4
Nankingský	nankingský	k2eAgInSc4d1
masakr	masakr	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
podle	podle	k7c2
zjištění	zjištění	k1gNnSc2
tribunálu	tribunál	k1gInSc2
v	v	k7c6
Tokiu	Tokio	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
zmasakrovala	zmasakrovat	k5eAaPmAgFnS
300	#num#	k4
000	#num#	k4
civilistů	civilista	k1gMnPc2
a	a	k8xC
válečných	válečný	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
přijímaný	přijímaný	k2eAgInSc1d1
údaj	údaj	k1gInSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
ve	v	k7c6
stovkách	stovka	k1gFnPc6
tisíců	tisíc	k4xCgInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
čínsko-japonské	čínsko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
Japonci	Japonec	k1gMnPc1
podnikali	podnikat	k5eAaImAgMnP
tzv.	tzv.	kA
"	"	kIx"
<g/>
politiku	politika	k1gFnSc4
zabíjení	zabíjení	k1gNnSc2
<g/>
"	"	kIx"
i	i	k8xC
vůči	vůči	k7c3
menšinám	menšina	k1gFnPc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Chuejové	Chuej	k1gMnPc1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Wan	Wan	k1gFnSc2
Leieː	Leieː	k1gFnSc2
"	"	kIx"
<g/>
Ve	v	k7c6
vesnici	vesnice	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
žili	žít	k5eAaImAgMnP
Chuejové	Chuej	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
kraji	kraj	k1gInSc6
Gaočeng	Gaočenga	k1gFnPc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Che-pej	Che-pej	k1gFnSc2
<g/>
,	,	kIx,
zajali	zajmout	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
dvacet	dvacet	k4xCc4
chuejských	chuejský	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
propustili	propustit	k5eAaPmAgMnP
pouze	pouze	k6eAd1
dva	dva	k4xCgInPc4
mladší	mladý	k2eAgMnSc1d2
muže	muž	k1gMnSc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
"	"	kIx"
<g/>
vykoupili	vykoupit	k5eAaPmAgMnP
<g/>
"	"	kIx"
a	a	k8xC
zbylých	zbylý	k2eAgNnPc2d1
osmnáct	osmnáct	k4xCc4
zaživa	zaživa	k6eAd1
pohřbili	pohřbít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
Mengcun	Mengcuna	k1gFnPc2
v	v	k7c6
Che-peji	Che-pej	k1gInSc6
Japonci	Japonec	k1gMnPc1
za	za	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
okupace	okupace	k1gFnSc2
zabili	zabít	k5eAaPmAgMnP
více	hodně	k6eAd2
než	než	k8xS
1300	#num#	k4
Chuejů	Chuej	k1gInPc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Mešity	mešita	k1gFnPc1
byly	být	k5eAaImAgFnP
znesvěceny	znesvěcen	k2eAgInPc1d1
a	a	k8xC
zničeny	zničen	k2eAgInPc1d1
hřbitovy	hřbitov	k1gInPc1
také	také	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Mnoho	mnoho	k6eAd1
Chuejů	Chuej	k1gInPc2
za	za	k7c2
války	válka	k1gFnSc2
proti	proti	k7c3
Japoncům	Japonec	k1gMnPc3
bojovalo	bojovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
měl	mít	k5eAaImAgMnS
Manilský	manilský	k2eAgInSc4d1
masakr	masakr	k1gInSc4
v	v	k7c6
únoru	únor	k1gInSc6
1945	#num#	k4
za	za	k7c4
následek	následek	k1gInSc4
100	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
alespoň	alespoň	k9
jeden	jeden	k4xCgInSc4
z	z	k7c2
20	#num#	k4
Filipínců	Filipínec	k1gMnPc2
zemřel	zemřít	k5eAaPmAgMnS
během	během	k7c2
okupace	okupace	k1gFnSc2
rukou	ruka	k1gFnPc2
Japonců	Japonec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Singapuru	Singapur	k1gInSc6
docházelo	docházet	k5eAaImAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
února	únor	k1gInSc2
a	a	k8xC
března	březen	k1gInSc2
1942	#num#	k4
k	k	k7c3
systematickému	systematický	k2eAgNnSc3d1
vyhlazování	vyhlazování	k1gNnSc3
nepřátelských	přátelský	k2eNgInPc2d1
elementů	element	k1gInPc2
tamní	tamní	k2eAgFnSc2d1
čínské	čínský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřívější	dřívější	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Singapuru	Singapur	k1gInSc2
Li	li	k8xS
Kuang-jao	Kuang-jao	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
během	během	k7c2
rozhovoru	rozhovor	k1gInSc2
pro	pro	k7c4
National	National	k1gFnSc4
Geographic	Geographice	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c4
život	život	k1gInSc4
přišlo	přijít	k5eAaPmAgNnS
50	#num#	k4
000	#num#	k4
až	až	k9
90	#num#	k4
000	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
zatímco	zatímco	k8xS
podle	podle	k7c2
generálmajora	generálmajor	k1gMnSc2
Kawamury	Kawamura	k1gFnSc2
Sabura	Sabura	k1gFnSc1
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
život	život	k1gInSc4
celkem	celkem	k6eAd1
5	#num#	k4
000	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Pokusy	pokus	k1gInPc1
na	na	k7c6
lidech	člověk	k1gMnPc6
</s>
<s>
Neznámá	známý	k2eNgFnSc1d1
oběť	oběť	k1gFnSc1
pokusů	pokus	k1gInPc2
Jednotky	jednotka	k1gFnSc2
731	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnPc1d1
japonské	japonský	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
prováděly	provádět	k5eAaImAgFnP
v	v	k7c6
Číně	Čína	k1gFnSc6
pokusy	pokus	k1gInPc1
na	na	k7c6
civilistech	civilista	k1gMnPc6
a	a	k8xC
válečných	válečný	k2eAgFnPc6d1
zajatcích	zajatec	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
vešla	vejít	k5eAaPmAgNnP
ve	v	k7c4
známost	známost	k1gFnSc4
Jednotka	jednotka	k1gFnSc1
731	#num#	k4
pod	pod	k7c7
velením	velení	k1gNnSc7
Širó	Širó	k?
Išiiho	Išii	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotka	jednotka	k1gFnSc1
731	#num#	k4
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
na	na	k7c4
příkaz	příkaz	k1gInSc4
samotného	samotný	k2eAgNnSc2d1
Hirohita	Hirohitum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběti	oběť	k1gFnSc6
byly	být	k5eAaImAgInP
podrobovány	podrobovat	k5eAaImNgInP
pokusům	pokus	k1gInPc3
<g/>
,	,	kIx,
včetně	včetně	k7c2
neomezených	omezený	k2eNgFnPc2d1
vivisekcí	vivisekce	k1gFnPc2
<g/>
,	,	kIx,
amputací	amputace	k1gFnSc7
bez	bez	k7c2
anestézie	anestézie	k1gFnSc2
a	a	k8xC
testování	testování	k1gNnSc2
biologických	biologický	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anestezie	anestezie	k1gFnSc1
nebyla	být	k5eNaImAgFnS
používána	používat	k5eAaImNgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
věřilo	věřit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
anestetikum	anestetikum	k1gNnSc1
negativně	negativně	k6eAd1
ovlivnilo	ovlivnit	k5eAaPmAgNnS
výsledky	výsledek	k1gInPc1
experimentů	experiment	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Pro	pro	k7c4
přesné	přesný	k2eAgNnSc4d1
zjišťování	zjišťování	k1gNnSc4
léčby	léčba	k1gFnSc2
omrzlin	omrzlina	k1gFnPc2
byli	být	k5eAaImAgMnP
vězni	vězeň	k1gMnPc1
ponecháni	ponechat	k5eAaPmNgMnP
venku	venku	k6eAd1
v	v	k7c6
mrazivém	mrazivý	k2eAgNnSc6d1
počasí	počasí	k1gNnSc6
s	s	k7c7
odhalenými	odhalený	k2eAgFnPc7d1
pažemi	paže	k1gFnPc7
a	a	k8xC
pravidelně	pravidelně	k6eAd1
poléváni	poléván	k2eAgMnPc1d1
vodou	voda	k1gFnSc7
dokud	dokud	k8xS
paže	paže	k1gFnSc1
neomrzly	omrznout	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paže	paže	k1gFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
amputována	amputován	k2eAgFnSc1d1
<g/>
;	;	kIx,
lékař	lékař	k1gMnSc1
mohl	moct	k5eAaImAgMnS
proces	proces	k1gInSc4
opakovat	opakovat	k5eAaImF
na	na	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
paže	paže	k1gFnSc2
oběti	oběť	k1gFnSc2
po	po	k7c4
rameno	rameno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
byly	být	k5eAaImAgFnP
obě	dva	k4xCgFnPc4
paže	paže	k1gFnPc4
pryč	pryč	k6eAd1
<g/>
,	,	kIx,
se	se	k3xPyFc4
lékaři	lékař	k1gMnPc1
přesunuli	přesunout	k5eAaPmAgMnP
k	k	k7c3
nohám	noha	k1gFnPc3
dokud	dokud	k6eAd1
nezůstala	zůstat	k5eNaPmAgFnS
jen	jen	k9
hlava	hlava	k1gFnSc1
a	a	k8xC
trup	trup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběť	oběť	k1gFnSc1
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
použita	použít	k5eAaPmNgFnS
k	k	k7c3
experimentům	experiment	k1gInPc3
s	s	k7c7
morem	mor	k1gInSc7
a	a	k8xC
patogeny	patogen	k1gInPc7
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Podle	podle	k7c2
jednoho	jeden	k4xCgInSc2
z	z	k7c2
odhadů	odhad	k1gInPc2
mají	mít	k5eAaImIp3nP
pokusy	pokus	k1gInPc1
Jednotky	jednotka	k1gFnSc2
731	#num#	k4
na	na	k7c6
svědomí	svědomí	k1gNnSc6
až	až	k9
3	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
podle	podle	k7c2
mezinárodního	mezinárodní	k2eAgNnSc2d1
sympozia	sympozion	k1gNnSc2
o	o	k7c6
zločinech	zločin	k1gInPc6
bakteriologické	bakteriologický	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
počet	počet	k1gInSc1
osob	osoba	k1gFnPc2
zabitých	zabitý	k2eAgFnPc2d1
císařskou	císařský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
v	v	k7c6
bakteriologické	bakteriologický	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
lidských	lidský	k2eAgInPc6d1
experimentech	experiment	k1gInPc6
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
580	#num#	k4
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
dalších	další	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
"	"	kIx"
<g/>
desítky	desítka	k1gFnPc1
tisíc	tisíc	k4xCgInSc4
až	až	k8xS
400	#num#	k4
000	#num#	k4
Číňanů	Číňan	k1gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
na	na	k7c4
dýmějový	dýmějový	k2eAgInSc4d1
mor	mor	k1gInSc4
<g/>
,	,	kIx,
choleru	cholera	k1gFnSc4
<g/>
,	,	kIx,
antrax	antrax	k1gInSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
nemoce	nemoce	k1gFnPc4
jako	jako	k8xS,k8xC
následek	následek	k1gInSc4
biologické	biologický	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
Nejvyšší	vysoký	k2eAgMnPc1d3
důstojníci	důstojník	k1gMnPc1
jednotky	jednotka	k1gFnSc2
731	#num#	k4
nebyli	být	k5eNaImAgMnP
po	po	k7c6
válce	válka	k1gFnSc6
za	za	k7c4
válečné	válečný	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
stíháni	stíhat	k5eAaImNgMnP
výměnou	výměna	k1gFnSc7
za	za	k7c4
předání	předání	k1gNnSc4
výsledků	výsledek	k1gInPc2
svého	své	k1gNnSc2
výzkumu	výzkum	k1gInSc2
spojencům	spojenec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údajně	údajně	k6eAd1
dostali	dostat	k5eAaPmAgMnP
odpovědné	odpovědný	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
v	v	k7c6
japonském	japonský	k2eAgInSc6d1
farmaceutickém	farmaceutický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
lékařských	lékařský	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
ministerstvu	ministerstvo	k1gNnSc6
zdravotnictví	zdravotnictví	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byl	být	k5eAaImAgInS
zaznamenán	zaznamenat	k5eAaPmNgInS
i	i	k9
případ	případ	k1gInSc1
pokusu	pokus	k1gInSc2
na	na	k7c6
lidech	člověk	k1gMnPc6
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
samotném	samotný	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejméně	málo	k6eAd3
devět	devět	k4xCc1
z	z	k7c2
11	#num#	k4
členů	člen	k1gMnPc2
posádky	posádka	k1gFnSc2
přežilo	přežít	k5eAaPmAgNnS
havárii	havárie	k1gFnSc4
amerického	americký	k2eAgInSc2d1
bombardéru	bombardér	k1gInSc2
B-29	B-29	k1gFnSc2
na	na	k7c6
Kjúšú	Kjúšú	k1gFnSc6
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
letadlo	letadlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
pod	pod	k7c7
velením	velení	k1gNnSc7
poručíka	poručík	k1gMnSc2
Marvina	Marvina	k1gFnSc1
Watkinse	Watkinse	k1gFnSc1
z	z	k7c2
29	#num#	k4
<g/>
.	.	kIx.
bombardovací	bombardovací	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
z	z	k7c2
6	#num#	k4
<g/>
.	.	kIx.
bombardovací	bombardovací	k2eAgFnSc2d1
perutě	peruť	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
Velitel	velitel	k1gMnSc1
bombardéru	bombardér	k1gInSc2
byl	být	k5eAaImAgMnS
od	od	k7c2
posádky	posádka	k1gFnSc2
oddělen	oddělit	k5eAaPmNgMnS
a	a	k8xC
poslán	poslat	k5eAaPmNgMnS
do	do	k7c2
Tokia	Tokio	k1gNnSc2
k	k	k7c3
výslechu	výslech	k1gInSc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ostatní	ostatní	k2eAgMnPc1d1
přeživší	přeživší	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
převezeni	převézt	k5eAaPmNgMnP
do	do	k7c2
anatomického	anatomický	k2eAgNnSc2d1
oddělení	oddělení	k1gNnSc2
university	universita	k1gFnSc2
Kjúšú	Kjúšú	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Fukuoka	Fukuoek	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
vystaveni	vystavit	k5eAaPmNgMnP
vivisekci	vivisekce	k1gFnSc3
nebo	nebo	k8xC
zabiti	zabit	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
plánovalo	plánovat	k5eAaImAgNnS
Japonsko	Japonsko	k1gNnSc1
použít	použít	k5eAaPmF
mor	mor	k1gInSc4
jako	jako	k8xS,k8xC
biologickou	biologický	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
proti	proti	k7c3
civilistům	civilista	k1gMnPc3
přímo	přímo	k6eAd1
v	v	k7c6
USA	USA	kA
v	v	k7c6
San	San	k1gFnSc6
Diegu	Dieg	k1gInSc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
během	během	k7c2
operace	operace	k1gFnSc2
noční	noční	k2eAgMnSc1d1
třešňové	třešňový	k2eAgInPc4d1
květy	květ	k1gInPc4
v	v	k7c4
naději	naděje	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
mor	mor	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
americkou	americký	k2eAgFnSc7d1
populací	populace	k1gFnSc7
jako	jako	k9
teror	teror	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
USA	USA	kA
odrazeni	odradit	k5eAaPmNgMnP
od	od	k7c2
útoku	útok	k1gInSc2
na	na	k7c4
Japonsko	Japonsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akce	akce	k1gFnSc1
byla	být	k5eAaImAgFnS
naplánována	naplánovat	k5eAaBmNgFnS
na	na	k7c4
noc	noc	k1gFnSc4
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1945	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
Japonsko	Japonsko	k1gNnSc1
se	se	k3xPyFc4
o	o	k7c4
pět	pět	k4xCc4
týdnů	týden	k1gInPc2
předtím	předtím	k6eAd1
vzdalo	vzdát	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1948	#num#	k4
bylo	být	k5eAaImAgNnS
30	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
několika	několik	k4yIc2
lékařů	lékař	k1gMnPc2
a	a	k8xC
jedné	jeden	k4xCgFnSc2
zdravotní	zdravotní	k2eAgFnSc2d1
sestry	sestra	k1gFnSc2
<g/>
,	,	kIx,
postaveno	postaven	k2eAgNnSc1d1
před	před	k7c4
spojenecký	spojenecký	k2eAgInSc4d1
tribunál	tribunál	k1gInSc4
pro	pro	k7c4
válečné	válečný	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvinění	obvinění	k1gNnPc1
z	z	k7c2
kanibalismu	kanibalismus	k1gInSc2
byla	být	k5eAaImAgFnS
stažena	stáhnout	k5eAaPmNgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
23	#num#	k4
lidí	člověk	k1gMnPc2
byli	být	k5eAaImAgMnP
shledáno	shledat	k5eAaPmNgNnS
vinných	vinný	k2eAgInPc2d1
z	z	k7c2
vivisekcí	vivisekce	k1gFnPc2
nebo	nebo	k8xC
neoprávněných	oprávněný	k2eNgFnPc2d1
amputací	amputace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc1
bylo	být	k5eAaImAgNnS
odsouzeno	odsouzet	k5eAaImNgNnS,k5eAaPmNgNnS
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgMnPc1
na	na	k7c6
doživotí	doživotí	k1gNnSc6
a	a	k8xC
zbytek	zbytek	k1gInSc1
na	na	k7c4
kratší	krátký	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
žaláře	žalář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
vojenský	vojenský	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
generál	generál	k1gMnSc1
Douglas	Douglas	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
<g/>
,	,	kIx,
všechny	všechen	k3xTgInPc4
tresty	trest	k1gInPc4
smrti	smrt	k1gFnSc2
zrušil	zrušit	k5eAaPmAgInS
a	a	k8xC
většinu	většina	k1gFnSc4
z	z	k7c2
trestu	trest	k1gInSc2
odnětí	odnětí	k1gNnSc2
svobody	svoboda	k1gFnSc2
výrazně	výrazně	k6eAd1
snížil	snížit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
odsouzení	odsouzený	k2eAgMnPc1d1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
univerzitními	univerzitní	k2eAgFnPc7d1
vivisekcemi	vivisekce	k1gFnPc7
byl	být	k5eAaImAgInS
po	po	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
propuštěni	propustit	k5eAaPmNgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Navíc	navíc	k6eAd1
mnoho	mnoho	k4c1
účastníků	účastník	k1gMnPc2
zodpovědných	zodpovědný	k2eAgMnPc2d1
za	za	k7c4
tyto	tento	k3xDgFnPc4
vivisekce	vivisekce	k1gFnPc4
nikdy	nikdy	k6eAd1
nebylo	být	k5eNaImAgNnS
Američany	Američan	k1gMnPc7
nebo	nebo	k8xC
jejich	jejich	k3xOp3gMnPc7
spojenci	spojenec	k1gMnPc7
výměnou	výměna	k1gFnSc7
za	za	k7c4
informace	informace	k1gFnPc4
o	o	k7c6
experimentech	experiment	k1gInPc6
souzeno	souzen	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
bývalý	bývalý	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
císařského	císařský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
Akira	Akir	k1gMnSc2
Makino	Makino	k1gNnSc4
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgMnS
povolán	povolat	k5eAaPmNgMnS
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
svého	svůj	k3xOyFgInSc2
výcviku	výcvik	k1gInSc2
provádět	provádět	k5eAaImF
od	od	k7c2
prosince	prosinec	k1gInSc2
1944	#num#	k4
do	do	k7c2
února	únor	k1gInSc2
1945	#num#	k4
vivisekce	vivisekce	k1gFnSc2
na	na	k7c6
asi	asi	k9
30	#num#	k4
civilních	civilní	k2eAgMnPc2d1
vězních	vězeň	k1gMnPc6
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Operace	operace	k1gFnSc2
zahrnovaly	zahrnovat	k5eAaImAgFnP
amputace	amputace	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Většina	většina	k1gFnSc1
jeho	jeho	k3xOp3gFnSc7
obětí	oběť	k1gFnSc7
byli	být	k5eAaImAgMnP
Morové	morový	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Ken	Ken	k1gFnSc1
Juasa	Juasa	k1gFnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
vojenský	vojenský	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
také	také	k9
přiznal	přiznat	k5eAaPmAgMnS
k	k	k7c3
podobné	podobný	k2eAgFnSc3d1
události	událost	k1gFnSc3
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
byl	být	k5eAaImAgMnS
povinen	povinen	k2eAgMnSc1d1
se	se	k3xPyFc4
účastnit	účastnit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použití	použití	k1gNnSc1
chemických	chemický	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
</s>
<s>
Podle	podle	k7c2
historiků	historik	k1gMnPc2
Jošiakiho	Jošiaki	k1gMnSc2
Jošimi	Joši	k1gFnPc7
a	a	k8xC
Kentara	Kentara	k1gFnSc1
Awaji	Awaje	k1gFnSc4
byly	být	k5eAaImAgFnP
během	během	k7c2
druhá	druhý	k4xOgFnSc1
čínsko-japonské	čínsko-japonský	k2eAgFnPc1d1
války	válka	k1gFnSc2
zbraně	zbraň	k1gFnSc2
jako	jako	k8xS,k8xC
slzný	slzný	k2eAgInSc4d1
plyn	plyn	k1gInSc4
použity	použít	k5eAaPmNgInP
jen	jen	k9
sporadicky	sporadicky	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
1938	#num#	k4
začala	začít	k5eAaPmAgFnS
císařská	císařský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
celoplošně	celoplošně	k6eAd1
používat	používat	k5eAaImF
fosgen	fosgen	k1gInSc1
<g/>
,	,	kIx,
chlor	chlor	k1gInSc1
<g/>
,	,	kIx,
lewisit	lewisit	k1gInSc1
a	a	k8xC
chlorpikrin	chlorpikrin	k1gInSc1
(	(	kIx(
<g/>
červený	červený	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
od	od	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
1939	#num#	k4
<g/>
,	,	kIx,
yperit	yperit	k1gInSc1
(	(	kIx(
<g/>
žlutý	žlutý	k2eAgMnSc1d1
<g/>
)	)	kIx)
jak	jak	k8xC,k8xS
proti	proti	k7c3
Kuomintangu	Kuomintang	k1gInSc3
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
i	i	k8xC
čínským	čínský	k2eAgFnPc3d1
komunistickým	komunistický	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
Jošimi	Joši	k1gFnPc7
a	a	k8xC
Seiji	Seij	k1gMnPc7
Matsuny	Matsuna	k1gFnSc2
podepsal	podepsat	k5eAaPmAgMnS
císař	císař	k1gMnSc1
Hirohito	Hirohit	k2eAgNnSc4d1
objednávky	objednávka	k1gFnPc4
upřesňující	upřesňující	k2eAgNnSc4d1
použití	použití	k1gNnSc4
chemických	chemický	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
<g/>
Například	například	k6eAd1
během	během	k7c2
bitvy	bitva	k1gFnSc2
o	o	k7c4
Wuhan	Wuhan	k1gInSc4
od	od	k7c2
srpna	srpen	k1gInSc2
do	do	k7c2
října	říjen	k1gInSc2
1938	#num#	k4
povolil	povolit	k5eAaPmAgMnS
císař	císař	k1gMnSc1
použití	použití	k1gNnSc2
toxických	toxický	k2eAgInPc2d1
plynů	plyn	k1gInPc2
ve	v	k7c6
375	#num#	k4
různých	různý	k2eAgInPc6d1
případech	případ	k1gInPc6
<g/>
,	,	kIx,
navzdory	navzdory	k7c3
Haagským	haagský	k2eAgFnPc3d1
úmluvám	úmluva	k1gFnPc3
z	z	k7c2
roku	rok	k1gInSc2
1899	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
IV	Iva	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
-	-	kIx~
Prohlášení	prohlášení	k1gNnSc6
o	o	k7c6
používání	používání	k1gNnSc6
projektilů	projektil	k1gInPc2
jejichž	jejichž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
rozptyl	rozptyl	k1gInSc1
dusivých	dusivý	k2eAgInPc2d1
nebo	nebo	k8xC
škodlivých	škodlivý	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
článek	článek	k1gInSc1
23	#num#	k4
Haagských	haagský	k2eAgFnPc2d1
úmluv	úmluva	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
IV	IV	kA
-	-	kIx~
Zákony	zákon	k1gInPc1
a	a	k8xC
zvyky	zvyk	k1gInPc1
pozemní	pozemní	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
usnesení	usnesení	k1gNnSc6
přijatého	přijatý	k2eAgInSc2d1
společností	společnost	k1gFnSc7
národů	národ	k1gInPc2
dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
bylo	být	k5eAaImAgNnS
použití	použití	k1gNnSc1
jedovatého	jedovatý	k2eAgInSc2d1
plynu	plyn	k1gInSc2
Japonskem	Japonsko	k1gNnSc7
odsouzeno	odsoudit	k5eAaPmNgNnS,k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
bitva	bitva	k1gFnSc1
o	o	k7c4
Jičang	Jičang	k1gInSc4
v	v	k7c6
říjnu	říjen	k1gInSc6
1941	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
19	#num#	k4
<g/>
.	.	kIx.
dělostřelecký	dělostřelecký	k2eAgInSc1d1
regiment	regiment	k1gInSc1
pomáhal	pomáhat	k5eAaImAgInS
13	#num#	k4
<g/>
.	.	kIx.
brigádě	brigáda	k1gFnSc3
japonské	japonský	k2eAgFnSc2d1
jedenácté	jedenáctý	k4xOgFnSc2
armády	armáda	k1gFnSc2
1	#num#	k4
000	#num#	k4
granáty	granát	k1gInPc4
žlutého	žlutý	k2eAgInSc2d1
plynu	plyn	k1gInSc2
a	a	k8xC
1	#num#	k4
500	#num#	k4
červeného	červené	k1gNnSc2
proti	proti	k7c3
čínským	čínský	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc4
byla	být	k5eAaImAgFnS
plná	plný	k2eAgFnSc1d1
čínských	čínský	k2eAgMnPc2d1
civilistů	civilista	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
evakuovat	evakuovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
bylo	být	k5eAaImAgNnS
okolo	okolo	k7c2
3	#num#	k4
000	#num#	k4
čínských	čínský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
a	a	k8xC
1	#num#	k4
600	#num#	k4
bylo	být	k5eAaImAgNnS
zasaženo	zasažen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonská	japonský	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
účinek	účinek	k1gInSc1
plynu	plyn	k1gInSc2
se	se	k3xPyFc4
zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
být	být	k5eAaImF
značný	značný	k2eAgInSc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
Jošimi	Joši	k1gFnPc7
a	a	k8xC
Juki	Juki	k1gNnSc7
Tanaka	Tanak	k1gMnSc2
v	v	k7c6
australských	australský	k2eAgInPc6d1
národních	národní	k2eAgInPc6d1
archivech	archiv	k1gInPc6
objevili	objevit	k5eAaPmAgMnP
dokumenty	dokument	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
dokazují	dokazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
kajských	kajský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
v	v	k7c6
souostroví	souostroví	k1gNnSc6
Moluky	Moluky	k1gFnPc4
byl	být	k5eAaImAgInS
v	v	k7c6
listopadu	listopad	k1gInSc6
1944	#num#	k4
na	na	k7c6
australských	australský	k2eAgFnPc6d1
a	a	k8xC
holandských	holandský	k2eAgFnPc6d1
zajatcích	zajatec	k1gMnPc6
testován	testován	k2eAgInSc1d1
plynný	plynný	k2eAgInSc1d1
kyanid	kyanid	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mučení	mučení	k1gNnSc1
válečných	válečný	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
</s>
<s>
Australský	australský	k2eAgMnSc1d1
zajatec	zajatec	k1gMnSc1
<g/>
,	,	kIx,
Sgt	Sgt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leonard	Leonard	k1gMnSc1
Siffleet	Siffleet	k1gMnSc1
<g/>
,	,	kIx,
zajatý	zajatý	k1gMnSc1
na	na	k7c6
Nové	Nové	k2eAgFnSc6d1
Guineji	Guinea	k1gFnSc6
<g/>
,	,	kIx,
těsně	těsně	k6eAd1
před	před	k7c7
setnutím	setnutí	k1gNnSc7
japonským	japonský	k2eAgMnPc3d1
důstojníkem	důstojník	k1gMnSc7
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Japonské	japonský	k2eAgFnPc1d1
císařské	císařský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
široce	široko	k6eAd1
využívaly	využívat	k5eAaImAgFnP,k5eAaPmAgFnP
mučení	mučení	k1gNnPc1
na	na	k7c4
vězních	vězeň	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
ve	v	k7c6
snaze	snaha	k1gFnSc6
rychle	rychle	k6eAd1
shromažďovat	shromažďovat	k5eAaImF
vojenské	vojenský	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Mučení	mučený	k2eAgMnPc1d1
vězni	vězeň	k1gMnPc1
bývali	bývat	k5eAaImAgMnP
později	pozdě	k6eAd2
často	často	k6eAd1
popraveni	popravit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalý	bývalý	k2eAgMnSc1d1
japonský	japonský	k2eAgMnSc1d1
armádní	armádní	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
sloužil	sloužit	k5eAaImAgMnS
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
Uno	Uno	k1gFnSc6
Šintaro	Šintara	k1gFnSc5
<g/>
,	,	kIx,
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
prostředkem	prostředek	k1gInSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
k	k	k7c3
informacím	informace	k1gFnPc3
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
získat	získat	k5eAaPmF
je	on	k3xPp3gInPc4
výslechem	výslech	k1gInSc7
vězňů	vězeň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mučení	mučení	k1gNnSc1
bylo	být	k5eAaImAgNnS
nevyhnutelnou	vyhnutelný	k2eNgFnSc7d1
nutností	nutnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vraždění	vraždění	k1gNnSc1
a	a	k8xC
pohřbívání	pohřbívání	k1gNnSc1
přirozeně	přirozeně	k6eAd1
následuje	následovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžete	moct	k5eAaImIp2nP
to	ten	k3xDgNnSc4
udělat	udělat	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
nebudete	být	k5eNaImBp2nP
odhalen	odhalit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věřil	věřit	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
a	a	k8xC
jednal	jednat	k5eAaImAgMnS
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
protože	protože	k8xS
jsem	být	k5eAaImIp1nS
byl	být	k5eAaImAgMnS
přesvědčen	přesvědčit	k5eAaPmNgMnS
o	o	k7c6
tom	ten	k3xDgNnSc6
co	co	k9
dělám	dělat	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prováděli	provádět	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
své	svůj	k3xOyFgFnPc4
povinnosti	povinnost	k1gFnPc4
dle	dle	k7c2
pokynů	pokyn	k1gInPc2
našich	náš	k3xOp1gMnPc2
pánů	pan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělali	dělat	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
to	ten	k3xDgNnSc4
v	v	k7c6
zájmu	zájem	k1gInSc6
naší	náš	k3xOp1gFnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
naší	náš	k3xOp1gFnSc2
synovskou	synovský	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
k	k	k7c3
našim	náš	k3xOp1gMnPc3
předkům	předek	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bojišti	bojiště	k1gNnSc6
jsme	být	k5eAaImIp1nP
nikdy	nikdy	k6eAd1
Číňany	Číňan	k1gMnPc4
nepovažovali	považovat	k5eNaImAgMnP
za	za	k7c4
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
jsme	být	k5eAaImIp1nP
vyhráli	vyhrát	k5eAaPmAgMnP
<g/>
,	,	kIx,
poražení	poražený	k1gMnPc1
vypadali	vypadat	k5eAaImAgMnP,k5eAaPmAgMnP
opravdu	opravdu	k6eAd1
mizerně	mizerně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došli	dojít	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
rasa	rasa	k1gFnSc1
Jamato	Jamat	k2eAgNnSc1d1
[	[	kIx(
<g/>
tj	tj	kA
<g/>
,	,	kIx,
japonská	japonský	k2eAgFnSc1d1
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
nadřazená	nadřazený	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Účinnost	účinnost	k1gFnSc1
mučení	mučení	k1gNnPc2
ale	ale	k9
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
pro	pro	k7c4
japonské	japonský	k2eAgNnSc4d1
válečné	válečný	k2eAgNnSc4d1
úsilí	úsilí	k1gNnSc4
kontraproduktivní	kontraproduktivní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
svržení	svržení	k1gNnSc6
atomových	atomový	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
na	na	k7c4
Hirošimu	Hirošima	k1gFnSc4
a	a	k8xC
Nagasaki	Nagasaki	k1gNnSc4
mučila	mučit	k5eAaImAgFnS
japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
zajatého	zajatý	k2eAgMnSc2d1
amerického	americký	k2eAgMnSc2d1
pilota	pilot	k1gMnSc2
letounu	letoun	k1gInSc2
P-51	P-51	k1gMnSc4
jménem	jméno	k1gNnSc7
Marcus	Marcus	k1gMnSc1
McDilda	McDilda	k1gMnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
kolik	kolik	k4yQc1,k4yRc1,k4yIc1
atomových	atomový	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
spojenci	spojenec	k1gMnPc1
mají	mít	k5eAaImIp3nP
a	a	k8xC
co	co	k8xS
budou	být	k5eAaImBp3nP
další	další	k2eAgInPc1d1
cíle	cíl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
McDilda	McDilda	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
o	o	k7c6
atomové	atomový	k2eAgFnSc6d1
bombě	bomba	k1gFnSc6
a	a	k8xC
ani	ani	k8xC
projektu	projekt	k1gInSc2
Manhattan	Manhattan	k1gInSc4
nic	nic	k6eAd1
nevěděl	vědět	k5eNaImAgMnS
<g/>
,	,	kIx,
při	při	k7c6
mučení	mučení	k1gNnSc6
"	"	kIx"
<g/>
přiznal	přiznat	k5eAaPmAgMnS
<g/>
"	"	kIx"
že	že	k8xS
USA	USA	kA
mají	mít	k5eAaImIp3nP
100	#num#	k4
atomových	atomový	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
a	a	k8xC
že	že	k8xS
další	další	k2eAgInPc1d1
cíle	cíl	k1gInPc1
budou	být	k5eAaImBp3nP
Tokyo	Tokyo	k6eAd1
a	a	k8xC
Kjóto	Kjóto	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
McDildovo	McDildův	k2eAgNnSc1d1
falešné	falešný	k2eAgNnSc1d1
doznání	doznání	k1gNnSc1
mohlo	moct	k5eAaImAgNnS
mít	mít	k5eAaImF
vliv	vliv	k1gInSc4
na	na	k7c4
rozhodnutí	rozhodnutí	k1gNnSc4
japonských	japonský	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
ke	k	k7c3
kapitulaci	kapitulace	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popravy	poprava	k1gFnPc1
a	a	k8xC
zabíjení	zabíjení	k1gNnSc1
spojeneckých	spojenecký	k2eAgMnPc2d1
letců	letec	k1gMnPc2
</s>
<s>
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
Doolittlova	Doolittlův	k2eAgInSc2d1
náletu	nálet	k1gInSc2
zajatý	zajatý	k2eAgMnSc1d1
Japonci	Japonec	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mnoho	mnoho	k4c1
spojeneckých	spojenecký	k2eAgMnPc2d1
letců	letec	k1gMnPc2
zajatých	zajatá	k1gFnPc2
Japonci	Japonec	k1gMnPc1
na	na	k7c6
pevnině	pevnina	k1gFnSc6
nebo	nebo	k8xC
na	na	k7c6
moři	moře	k1gNnSc6
byly	být	k5eAaImAgInP
popraveno	popravit	k5eAaPmNgNnS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
oficiální	oficiální	k2eAgFnSc7d1
japonskou	japonský	k2eAgFnSc7d1
politikou	politika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
bitvy	bitva	k1gFnSc2
o	o	k7c4
Midway	Midwaa	k1gFnPc4
v	v	k7c6
červnu	červen	k1gInSc6
1942	#num#	k4
byli	být	k5eAaImAgMnP
tři	tři	k4xCgMnPc1
sestřelení	sestřelený	k2eAgMnPc1d1
američtí	americký	k2eAgMnPc1d1
letci	letec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
přistáli	přistát	k5eAaImAgMnP,k5eAaPmAgMnP
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
spatřeni	spatřen	k2eAgMnPc1d1
a	a	k8xC
zajati	zajat	k2eAgMnPc1d1
japonskými	japonský	k2eAgFnPc7d1
válečnými	válečný	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
krátkých	krátký	k2eAgInPc6d1
výsleších	výslech	k1gInPc6
byli	být	k5eAaImAgMnP
dva	dva	k4xCgMnPc1
letci	letec	k1gMnPc1
<g/>
,	,	kIx,
příslušníci	příslušník	k1gMnPc1
průzkumné	průzkumný	k2eAgFnSc2d1
perutě	peruť	k1gFnSc2
VS-6	VS-6	k1gFnSc2
z	z	k7c2
letadlové	letadlový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Enterprise	Enterprise	k1gFnSc2
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
poručík	poručík	k1gMnSc1
Frank	Frank	k1gMnSc1
Woodrow	Woodrow	k1gMnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Flaherty	Flahert	k1gInPc4
a	a	k8xC
radista-střelec	radista-střelec	k1gInSc1
letecký	letecký	k2eAgMnSc1d1
mechanik	mechanik	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
Bruno	Bruno	k1gMnSc1
Peter	Peter	k1gMnSc1
Gaido	Gaido	k1gNnSc4
spoutáni	spoután	k2eAgMnPc1d1
<g/>
,	,	kIx,
přivázáni	přivázán	k2eAgMnPc1d1
k	k	k7c3
pětigalonovým	pětigalonův	k2eAgInPc3d1
sudům	sud	k1gInPc3
na	na	k7c4
petrolej	petrolej	k1gInSc4
naplněných	naplněný	k2eAgInPc2d1
vodou	voda	k1gFnSc7
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
zaživa	zaživa	k6eAd1
hozeni	hodit	k5eAaPmNgMnP
přes	přes	k7c4
palubu	paluba	k1gFnSc4
do	do	k7c2
hlubin	hlubina	k1gFnPc2
oceánu	oceán	k1gInSc2
z	z	k7c2
torpédoborce	torpédoborec	k1gInSc2
Makigumo	Makiguma	k1gFnSc5
<g/>
;	;	kIx,
Třetí	třetí	k4xOgMnSc1
letec	letec	k1gMnSc1
<g/>
,	,	kIx,
příslušník	příslušník	k1gMnSc1
torpédové	torpédový	k2eAgFnSc2d1
perutě	peruť	k1gFnSc2
VT-3	VT-3	k1gFnSc2
z	z	k7c2
letadlové	letadlový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Yorktown	Yorktown	k1gMnSc1
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
poručík	poručík	k1gMnSc1
Wesley	Weslea	k1gFnSc2
Frank	Frank	k1gMnSc1
Osmus	Osmus	k1gMnSc1
když	když	k8xS
viděl	vidět	k5eAaImAgMnS
že	že	k8xS
ho	on	k3xPp3gInSc4
čeká	čekat	k5eAaImIp3nS
stejný	stejný	k2eAgInSc1d1
osud	osud	k1gInSc1
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
bránit	bránit	k5eAaImF
a	a	k8xC
držel	držet	k5eAaImAgMnS
se	se	k3xPyFc4
zábradlí	zábradlí	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c4
ten	ten	k3xDgInSc4
moment	moment	k1gInSc4
japonci	japonek	k1gMnPc1
vzali	vzít	k5eAaPmAgMnP
požární	požární	k2eAgFnSc4d1
sekeru	sekera	k1gFnSc4
a	a	k8xC
jednou	jeden	k4xCgFnSc7
ranou	rána	k1gFnSc7
do	do	k7c2
hlavy	hlava	k1gFnSc2
ho	on	k3xPp3gMnSc4
zabili	zabít	k5eAaPmAgMnP
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
tělo	tělo	k1gNnSc4
hodili	hodit	k5eAaPmAgMnP,k5eAaImAgMnP
přes	přes	k7c4
palubu	paluba	k1gFnSc4
z	z	k7c2
torpédoborce	torpédoborec	k1gInSc2
Araši	Araše	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1942	#num#	k4
Japonsko	Japonsko	k1gNnSc1
schválilo	schválit	k5eAaPmAgNnS
zákon	zákon	k1gInSc4
o	o	k7c6
nepřátelských	přátelský	k2eNgInPc6d1
letcích	letek	k1gInPc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
stanovil	stanovit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
spojenečtí	spojenecký	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
bombardovali	bombardovat	k5eAaImAgMnP
nevojenské	vojenský	k2eNgInPc4d1
cíle	cíl	k1gInPc4
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
japonskými	japonský	k2eAgFnPc7d1
silami	síla	k1gFnPc7
na	na	k7c6
souši	souš	k1gFnSc6
nebo	nebo	k8xC
na	na	k7c6
moři	moře	k1gNnSc6
zajati	zajmout	k5eAaPmNgMnP
<g/>
,	,	kIx,
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
předmětem	předmět	k1gInSc7
soudu	soud	k1gInSc2
a	a	k8xC
trestu	trest	k1gInSc2
i	i	k9
přes	přes	k7c4
absenci	absence	k1gFnSc4
jakéhokoli	jakýkoli	k3yIgNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
mezinárodního	mezinárodní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
ustanovení	ustanovení	k1gNnSc4
týkající	týkající	k2eAgNnSc4d1
se	se	k3xPyFc4
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
legislativa	legislativa	k1gFnSc1
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
Doolittlův	Doolittlův	k2eAgInSc4d1
nálet	nálet	k1gInSc4
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yIgInSc3,k3yQgInSc3,k3yRgInSc3
došlo	dojít	k5eAaPmAgNnS
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
americké	americký	k2eAgInPc1d1
letouny	letoun	k1gInPc1
B-25	B-25	k1gFnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
podplukovníka	podplukovník	k1gMnSc2
Jamese	Jamese	k1gFnSc2
Doolittla	Doolittla	k1gFnSc2
bombardovaly	bombardovat	k5eAaImAgInP
Tokio	Tokio	k1gNnSc4
a	a	k8xC
další	další	k2eAgNnPc4d1
japonská	japonský	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Haagské	haagský	k2eAgFnSc2d1
úmluvy	úmluva	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
(	(	kIx(
<g/>
jediná	jediný	k2eAgFnSc1d1
úmluva	úmluva	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
Japonsko	Japonsko	k1gNnSc1
ratifikovalo	ratifikovat	k5eAaBmAgNnS
<g/>
,	,	kIx,
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
zacházení	zacházení	k1gNnSc4
s	s	k7c7
válečnými	válečný	k2eAgMnPc7d1
zajatci	zajatec	k1gMnPc7
<g/>
)	)	kIx)
se	se	k3xPyFc4
s	s	k7c7
případným	případný	k2eAgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
personálem	personál	k1gInSc7
zajatým	zajatý	k2eAgInSc7d1
na	na	k7c6
zemi	zem	k1gFnSc6
nebo	nebo	k8xC
na	na	k7c6
moři	moře	k1gNnSc6
nepřátelskou	přátelský	k2eNgFnSc7d1
stranou	strana	k1gFnSc7
má	mít	k5eAaImIp3nS
zacházet	zacházet	k5eAaImF
jako	jako	k9
s	s	k7c7
válečným	válečný	k2eAgMnSc7d1
zajatcem	zajatec	k1gMnSc7
a	a	k8xC
nikoli	nikoli	k9
být	být	k5eAaImF
prostě	prostě	k6eAd1
potrestán	potrestat	k5eAaPmNgMnS
jako	jako	k8xC,k8xS
nezákonný	zákonný	k2eNgMnSc1d1
bojovník	bojovník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osm	osm	k4xCc1
zajatých	zajatý	k2eAgMnPc2d1
letců	letec	k1gMnPc2
z	z	k7c2
Doolitlova	Doolitlův	k2eAgInSc2d1
náletu	nálet	k1gInSc2
po	po	k7c6
přistání	přistání	k1gNnSc6
v	v	k7c6
Číně	Čína	k1gFnSc6
(	(	kIx(
<g/>
nevěděl	vědět	k5eNaImAgMnS
o	o	k7c4
existenci	existence	k1gFnSc4
japonského	japonský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
letcích	letek	k1gInPc6
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
prvními	první	k4xOgInPc7
spojeneckými	spojenecký	k2eAgInPc7d1
piloty	pilot	k1gInPc7
předvedenými	předvedený	k2eAgInPc7d1
před	před	k7c4
zinscenovaný	zinscenovaný	k2eAgInSc4d1
soudní	soudní	k2eAgInSc4d1
proces	proces	k1gInSc4
v	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
v	v	k7c6
rámci	rámec	k1gInSc6
zákona	zákon	k1gInSc2
a	a	k8xC
obviněni	obvinit	k5eAaPmNgMnP
z	z	k7c2
údajného	údajný	k2eAgInSc2d1
(	(	kIx(
<g/>
ale	ale	k8xC
neprokázaného	prokázaný	k2eNgMnSc2d1
<g/>
)	)	kIx)
bombardování	bombardování	k1gNnSc1
japonských	japonský	k2eAgMnPc2d1
civilistů	civilista	k1gMnPc2
během	během	k7c2
Doolittlova	Doolittlův	k2eAgInSc2d1
náletu	nálet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmi	osm	k4xCc2
letcům	letec	k1gMnPc3
byla	být	k5eAaImAgFnS
zakázána	zakázat	k5eAaPmNgFnS
jakákoliv	jakýkoliv	k3yIgFnSc1
obrana	obrana	k1gFnSc1
a	a	k8xC
i	i	k9
přes	přes	k7c4
nedostatek	nedostatek	k1gInSc4
legitimních	legitimní	k2eAgInPc2d1
důkazů	důkaz	k1gInPc2
byli	být	k5eAaImAgMnP
shledáni	shledat	k5eAaPmNgMnP
vinnými	vinný	k2eAgFnPc7d1
z	z	k7c2
účasti	účast	k1gFnSc2
v	v	k7c6
leteckých	letecký	k2eAgFnPc6d1
vojenských	vojenský	k2eAgFnPc6d1
operacích	operace	k1gFnPc6
proti	proti	k7c3
Japonsku	Japonsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěti	pět	k4xCc2
letcům	letec	k1gMnPc3
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
zmírněn	zmírnit	k5eAaPmNgInS
na	na	k7c4
doživotí	doživotí	k1gNnSc4
<g/>
;	;	kIx,
ostatní	ostatní	k2eAgMnPc4d1
tři	tři	k4xCgMnPc1
byli	být	k5eAaImAgMnP
převezeni	převézt	k5eAaPmNgMnP
na	na	k7c4
hřbitov	hřbitov	k1gInSc4
mimo	mimo	k7c4
Šanghaj	Šanghaj	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1942	#num#	k4
popraveni	popravit	k5eAaPmNgMnP
zastřelením	zastřelení	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
nepřátelských	přátelský	k2eNgInPc6d1
letcích	letek	k1gInPc6
měl	mít	k5eAaImAgInS
za	za	k7c4
následek	následek	k1gInSc4
úmrtí	úmrtí	k1gNnSc2
stovek	stovka	k1gFnPc2
spojeneckých	spojenecký	k2eAgMnPc2d1
letců	letec	k1gMnPc2
během	během	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
132	#num#	k4
spojeneckých	spojenecký	k2eAgMnPc2d1
letců	letec	k1gMnPc2
sestřelených	sestřelený	k2eAgMnPc2d1
během	během	k7c2
bombardování	bombardování	k1gNnSc2
Japonska	Japonsko	k1gNnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1944-1945	1944-1945	k4
bylo	být	k5eAaImAgNnS
po	po	k7c6
krátkých	krátký	k2eAgInPc6d1
vykonstruovaných	vykonstruovaný	k2eAgInPc6d1
procesech	proces	k1gInPc6
nebo	nebo	k8xC
polních	polní	k2eAgInPc6d1
soudech	soud	k1gInPc6
popraveno	popraven	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonský	japonský	k2eAgInSc4d1
císařský	císařský	k2eAgInSc4d1
vojenský	vojenský	k2eAgInSc4d1
personál	personál	k1gInSc4
záměrně	záměrně	k6eAd1
zabil	zabít	k5eAaPmAgMnS
33	#num#	k4
amerických	americký	k2eAgMnPc2d1
letců	letec	k1gMnPc2
ve	v	k7c6
Fukuoce	Fukuoka	k1gFnSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
patnácti	patnáct	k4xCc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
sťali	stnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
poté	poté	k6eAd1
co	co	k9
japonská	japonský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
uveřejnila	uveřejnit	k5eAaPmAgFnS
záměr	záměr	k1gInSc4
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1945	#num#	k4
kapitulovat	kapitulovat	k5eAaBmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Davy	Dav	k1gInPc1
civilistů	civilista	k1gMnPc2
stačily	stačit	k5eAaBmAgInP
také	také	k9
zabít	zabít	k5eAaPmF
několik	několik	k4yIc4
spojeneckých	spojenecký	k2eAgMnPc2d1
letců	letec	k1gMnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
než	než	k8xS
dorazila	dorazit	k5eAaPmAgFnS
japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
a	a	k8xC
letce	letka	k1gFnSc3
vzala	vzít	k5eAaPmAgFnS
do	do	k7c2
vazby	vazba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Dalších	další	k2eAgInPc2d1
94	#num#	k4
letců	letec	k1gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
v	v	k7c6
japonských	japonský	k2eAgFnPc6d1
vazbách	vazba	k1gFnPc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
52	#num#	k4
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
zabiti	zabít	k5eAaPmNgMnP
když	když	k8xS
záměrně	záměrně	k6eAd1
opustili	opustit	k5eAaPmAgMnP
vězení	vězení	k1gNnSc4
během	během	k7c2
bombardování	bombardování	k1gNnSc2
Tokia	Tokio	k1gNnSc2
24	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kanibalismus	kanibalismus	k1gInSc1
</s>
<s>
Mnoho	mnoho	k4c1
písemných	písemný	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
a	a	k8xC
svědeckých	svědecký	k2eAgFnPc2d1
výpovědí	výpověď	k1gFnPc2
shromážděných	shromážděný	k2eAgFnPc2d1
australskou	australský	k2eAgFnSc7d1
sekcí	sekce	k1gFnSc7
válečných	válečný	k2eAgInPc2d1
zločinů	zločin	k1gInPc2
Tokijského	tokijský	k2eAgInSc2d1
tribunálu	tribunál	k1gInSc2
a	a	k8xC
vyšetřovaných	vyšetřovaný	k2eAgInPc2d1
zástupcem	zástupce	k1gMnSc7
Williamem	William	k1gInSc7
Webbem	Webb	k1gMnSc7
(	(	kIx(
<g/>
budoucím	budoucí	k2eAgMnSc7d1
vrchním	vrchní	k2eAgMnSc7d1
soudcem	soudce	k1gMnSc7
<g/>
)	)	kIx)
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
v	v	k7c6
mnoha	mnoho	k4c6
částech	část	k1gFnPc6
Asie	Asie	k1gFnSc2
a	a	k8xC
Tichomoří	Tichomoří	k1gNnSc2
dopouštěli	dopouštět	k5eAaImAgMnP
na	na	k7c6
spojeneckých	spojenecký	k2eAgFnPc6d1
válečných	válečný	k2eAgFnPc6d1
zajatcích	zajatec	k1gMnPc6
kanibalismu	kanibalismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnoha	mnoho	k4c2
případech	případ	k1gInPc6
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
podníceno	podnícet	k5eAaImNgNnS,k5eAaPmNgNnS
stále	stále	k6eAd1
rostoucími	rostoucí	k2eAgInPc7d1
spojeneckými	spojenecký	k2eAgInPc7d1
útoky	útok	k1gInPc7
na	na	k7c4
japonské	japonský	k2eAgFnPc4d1
zásobovací	zásobovací	k2eAgFnPc4d1
linky	linka	k1gFnPc4
a	a	k8xC
úmrtími	úmrtí	k1gNnPc7
či	či	k8xC
onemocněními	onemocnění	k1gNnPc7
japonského	japonský	k2eAgInSc2d1
personálu	personál	k1gInSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
hladu	hlad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
historika	historik	k1gMnSc2
Jukiho	Juki	k1gMnSc2
Tanaki	Tanak	k1gFnSc2
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Byl	být	k5eAaImAgMnS
kanibalismus	kanibalismus	k1gInSc4
často	často	k6eAd1
systematická	systematický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
prováděná	prováděný	k2eAgFnSc1d1
celými	celý	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
a	a	k8xC
pod	pod	k7c7
vedením	vedení	k1gNnSc7
důstojníků	důstojník	k1gMnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Takto	takto	k6eAd1
často	často	k6eAd1
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
vraždám	vražda	k1gFnPc3
za	za	k7c7
účelem	účel	k1gInSc7
zajištění	zajištění	k1gNnSc3
těl	tělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
indický	indický	k2eAgMnSc1d1
zajatec	zajatec	k1gMnSc1
<g/>
,	,	kIx,
havildar	havildar	k1gMnSc1
Čangdi	Čangd	k1gMnPc1
Ram	Ram	k1gMnSc1
<g/>
,	,	kIx,
dosvědčil	dosvědčit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1944	#num#	k4
<g/>
]	]	kIx)
příslušník	příslušník	k1gMnSc1
Kempeitai	Kempeitae	k1gFnSc4
sťal	stnout	k5eAaPmAgMnS
[	[	kIx(
<g/>
spojeneckého	spojenecký	k2eAgMnSc2d1
<g/>
]	]	kIx)
pilota	pilot	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viděl	vidět	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
to	ten	k3xDgNnSc4
zpoza	zpoza	k7c2
stromu	strom	k1gInSc2
a	a	k8xC
sledoval	sledovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
někteří	některý	k3yIgMnPc1
Japonci	Japonec	k1gMnPc1
odřezávají	odřezávat	k5eAaImIp3nP
maso	maso	k1gNnSc4
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
paží	paže	k1gFnPc2
<g/>
,	,	kIx,
nohou	noha	k1gFnPc2
<g/>
,	,	kIx,
boků	bok	k1gInPc2
<g/>
,	,	kIx,
hýždí	hýždě	k1gFnPc2
a	a	k8xC
odnášejí	odnášet	k5eAaImIp3nP
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
ubikací	ubikace	k1gFnPc2
<g/>
...	...	k?
<g/>
krájeli	krájet	k5eAaImAgMnP
je	on	k3xPp3gFnPc4
na	na	k7c4
malé	malý	k2eAgInPc4d1
kousky	kousek	k1gInPc4
a	a	k8xC
smažili	smažit	k5eAaImAgMnP
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
bylo	být	k5eAaImAgNnS
maso	maso	k1gNnSc1
řezáno	řezat	k5eAaImNgNnS
z	z	k7c2
živých	živý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
:	:	kIx,
jiný	jiný	k2eAgMnSc1d1
indický	indický	k2eAgMnSc1d1
válečný	válečný	k2eAgMnSc1d1
zajatec	zajatec	k1gMnSc1
Lance	lance	k1gNnSc2
Naik	Naik	k1gMnSc1
Hatam	Hatam	k1gInSc4
Ali	Ali	k1gFnSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
občan	občan	k1gMnSc1
Pákistánu	Pákistán	k1gInSc2
<g/>
)	)	kIx)
svědčil	svědčit	k5eAaImAgInS
v	v	k7c6
Nové	Nové	k2eAgFnSc6d1
Guineji	Guinea	k1gFnSc6
a	a	k8xC
uvedl	uvést	k5eAaPmAgMnS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Japonci	Japonec	k1gMnPc1
začali	začít	k5eAaPmAgMnP
vybírat	vybírat	k5eAaImF
vězně	vězeň	k1gMnSc4
a	a	k8xC
každý	každý	k3xTgMnSc1
den	den	k1gInSc4
byl	být	k5eAaImAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
vězňů	vězeň	k1gMnPc2
zabit	zabít	k5eAaPmNgMnS
a	a	k8xC
vojáky	voják	k1gMnPc4
sněden	sněden	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobně	osobně	k6eAd1
jsem	být	k5eAaImIp1nS
to	ten	k3xDgNnSc4
viděl	vidět	k5eAaImAgMnS
a	a	k8xC
na	na	k7c6
tomto	tento	k3xDgInSc6
místě	místo	k1gNnSc6
bylo	být	k5eAaImAgNnS
asi	asi	k9
100	#num#	k4
vězňů	vězeň	k1gMnPc2
Japonci	Japonec	k1gMnPc7
snědeno	sněden	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
z	z	k7c2
nás	my	k3xPp1nPc2
byl	být	k5eAaImAgMnS
převezen	převézt	k5eAaPmNgMnS
na	na	k7c4
jiné	jiné	k1gNnSc4
místo	místo	k7c2
50	#num#	k4
mil	míle	k1gFnPc2
[	[	kIx(
<g/>
80	#num#	k4
km	km	kA
<g/>
]	]	kIx)
dál	daleko	k6eAd2
<g/>
,	,	kIx,
kde	kde	k6eAd1
10	#num#	k4
vězňů	vězeň	k1gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
na	na	k7c4
nemoci	nemoc	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
Japonci	Japonec	k1gMnPc1
opět	opět	k6eAd1
začali	začít	k5eAaPmAgMnP
s	s	k7c7
výběrem	výběr	k1gInSc7
vězňů	vězeň	k1gMnPc2
k	k	k7c3
snědku	snědek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybraní	vybraný	k2eAgMnPc1d1
vězni	vězeň	k1gMnPc1
odešli	odejít	k5eAaPmAgMnP
do	do	k7c2
chýše	chýš	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jim	on	k3xPp3gMnPc3
bylo	být	k5eAaImAgNnS
maso	maso	k1gNnSc1
řezáno	řezat	k5eAaImNgNnS
z	z	k7c2
těl	tělo	k1gNnPc2
zatímco	zatímco	k8xS
byli	být	k5eAaImAgMnP
naživu	naživu	k6eAd1
a	a	k8xC
poté	poté	k6eAd1
byli	být	k5eAaImAgMnP
hozeni	hodit	k5eAaPmNgMnP
do	do	k7c2
příkopu	příkop	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
později	pozdě	k6eAd2
zemřeli	zemřít	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Asi	asi	k9
nejvyšší	vysoký	k2eAgMnSc1d3
důstojník	důstojník	k1gMnSc1
usvědčený	usvědčený	k2eAgMnSc1d1
z	z	k7c2
kanibalismu	kanibalismus	k1gInSc2
byl	být	k5eAaImAgMnS
generálporučík	generálporučík	k1gMnSc1
Jošio	Jošio	k1gMnSc1
Tačibana	Tačibana	k1gFnSc1
(	(	kIx(
<g/>
立	立	k?
花	花	k?
芳	芳	k?
夫	夫	k?
<g/>
,	,	kIx,
Tačibana	Tačibana	k1gFnSc1
Jošio	Jošio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
s	s	k7c7
dalšími	další	k2eAgNnPc7d1
11	#num#	k4
Japonci	Japonec	k1gMnPc7
v	v	k7c6
srpnu	srpen	k1gInSc6
1946	#num#	k4
souzen	soudit	k5eAaImNgMnS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
popravami	poprava	k1gFnPc7
letců	letec	k1gMnPc2
US	US	kA
Navy	Navy	k?
a	a	k8xC
kanibalismu	kanibalismus	k1gInSc3
provedeného	provedený	k2eAgNnSc2d1
na	na	k7c4
minimálně	minimálně	k6eAd1
jednom	jeden	k4xCgInSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
v	v	k7c6
srpnu	srpen	k1gInSc6
1944	#num#	k4
za	za	k7c2
Ogasawarského	Ogasawarský	k2eAgInSc2d1
incidentu	incident	k1gInSc2
na	na	k7c6
Boninských	Boninský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letci	letec	k1gMnPc1
byli	být	k5eAaImAgMnP
na	na	k7c4
Tačibanův	Tačibanův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
sťati	stnout	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
vojenské	vojenský	k2eAgNnSc1d1
a	a	k8xC
mezinárodní	mezinárodní	k2eAgNnSc1d1
právo	právo	k1gNnSc1
konkrétně	konkrétně	k6eAd1
kanibalismus	kanibalismus	k1gInSc1
neřeší	řešit	k5eNaImIp3nS
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
souzeni	soudit	k5eAaImNgMnP
za	za	k7c4
vraždu	vražda	k1gFnSc4
a	a	k8xC
„	„	k?
<g/>
bránění	bránění	k1gNnSc2
důstojnému	důstojný	k2eAgInSc3d1
pohřbu	pohřeb	k1gInSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tačibana	Tačibana	k1gFnSc1
byl	být	k5eAaImAgInS
odsouzen	odsouzen	k2eAgMnSc1d1
k	k	k7c3
smrti	smrt	k1gFnSc3
a	a	k8xC
oběšen	oběšen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nucené	nucený	k2eAgFnPc1d1
práce	práce	k1gFnPc1
</s>
<s>
Australští	australský	k2eAgMnPc1d1
a	a	k8xC
holandští	holandský	k2eAgMnPc1d1
zajatci	zajatec	k1gMnPc1
v	v	k7c6
Tarsau	Tarsaum	k1gNnSc6
v	v	k7c6
Thajsku	Thajsko	k1gNnSc6
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
používala	používat	k5eAaImAgFnS
asijské	asijský	k2eAgFnSc3d1
civilisty	civilista	k1gMnPc4
a	a	k8xC
válečné	válečný	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
k	k	k7c3
nuceným	nucený	k2eAgFnPc3d1
pracím	práce	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
způsobily	způsobit	k5eAaPmAgFnP
mnoho	mnoho	k6eAd1
úmrtí	úmrtí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
společné	společný	k2eAgFnSc2d1
studie	studie	k1gFnSc2
historiků	historik	k1gMnPc2
včetně	včetně	k7c2
Žifena	Žifeno	k1gNnSc2
Ju	ju	k0
<g/>
,	,	kIx,
Mitsujošiho	Mitsujoši	k1gMnSc2
Himety	Himeta	k1gFnSc2
<g/>
,	,	kIx,
Toru	torus	k1gInSc2
Kuboa	Kuboum	k1gNnSc2
a	a	k8xC
Marka	Marek	k1gMnSc2
Peattie	Peattie	k1gFnSc2
bylo	být	k5eAaImAgNnS
více	hodně	k6eAd2
než	než	k8xS
10	#num#	k4
milionů	milion	k4xCgInPc2
čínských	čínský	k2eAgMnPc2d1
civilistů	civilista	k1gMnPc2
mobilizováno	mobilizován	k2eAgNnSc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
východoasijské	východoasijský	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
vzájemné	vzájemný	k2eAgFnSc2d1
prosperity	prosperita	k1gFnSc2
(	(	kIx(
<g/>
resp.	resp.	kA
východoasijského	východoasijský	k2eAgInSc2d1
rozvojového	rozvojový	k2eAgInSc2d1
výboru	výbor	k1gInSc2
-	-	kIx~
Kóain	Kóain	k2eAgMnSc1d1
<g/>
)	)	kIx)
k	k	k7c3
nuceným	nucený	k2eAgFnPc3d1
pracím	práce	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
000	#num#	k4
civilistů	civilista	k1gMnPc2
a	a	k8xC
zajatců	zajatec	k1gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
při	při	k7c6
stavbě	stavba	k1gFnSc6
barmsko-thajské	barmsko-thajský	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Knihovna	knihovna	k1gFnSc1
Kongresu	kongres	k1gInSc2
odhaduje	odhadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Jáva	Jáva	k1gFnSc1
násilně	násilně	k6eAd1
k	k	k7c3
pracem	prace	k1gMnSc7
donutila	donutit	k5eAaPmAgFnS
čtyři	čtyři	k4xCgNnPc4
až	až	k9
deset	deset	k4xCc4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
(	(	kIx(
<g/>
"	"	kIx"
<g/>
manuálních	manuální	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
romuša	romuša	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
<g/>
Asi	asi	k9
270	#num#	k4
000	#num#	k4
z	z	k7c2
těchto	tento	k3xDgMnPc2
dělníků	dělník	k1gMnPc2
z	z	k7c2
Jávy	Jáva	k1gFnSc2
bylo	být	k5eAaImAgNnS
zasláno	zaslat	k5eAaPmNgNnS
do	do	k7c2
jiných	jiný	k2eAgInPc2d1
Japonci	Japonec	k1gMnPc1
držených	držený	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
52	#num#	k4
000	#num#	k4
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
na	na	k7c4
Jávu	Jáva	k1gFnSc4
vrátilo	vrátit	k5eAaPmAgNnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
činí	činit	k5eAaImIp3nS
úmrtnost	úmrtnost	k1gFnSc1
osmdesát	osmdesát	k4xCc4
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
historika	historik	k1gMnSc2
Akiry	Akira	k1gMnSc2
Fudžiwary	Fudžiwara	k1gFnSc2
císař	císař	k1gMnSc1
Hirohito	Hirohit	k2eAgNnSc4d1
osobně	osobně	k6eAd1
schválil	schválit	k5eAaPmAgMnS
rozhodnutí	rozhodnutí	k1gNnSc4
k	k	k7c3
odstranění	odstranění	k1gNnSc3
omezení	omezení	k1gNnSc1
mezinárodního	mezinárodní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
(	(	kIx(
<g/>
Haagské	haagský	k2eAgFnSc2d1
úmluvy	úmluva	k1gFnSc2
<g/>
)	)	kIx)
o	o	k7c4
zacházení	zacházení	k1gNnSc4
s	s	k7c7
čínskými	čínský	k2eAgMnPc7d1
válečnými	válečný	k2eAgMnPc7d1
zajatci	zajatec	k1gMnPc7
ve	v	k7c6
směrnici	směrnice	k1gFnSc6
ze	z	k7c2
dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1937	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
oznámení	oznámení	k1gNnSc1
rovněž	rovněž	k9
štábním	štábní	k2eAgMnPc3d1
důstojníkům	důstojník	k1gMnPc3
doporučuje	doporučovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
přestali	přestat	k5eAaPmAgMnP
používat	používat	k5eAaImF
termín	termín	k1gInSc4
"	"	kIx"
<g/>
váleční	váleční	k2eAgMnPc1d1
zajatci	zajatec	k1gMnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Ženevské	ženevský	k2eAgFnSc2d1
úmluvy	úmluva	k1gFnSc2
osvobozují	osvobozovat	k5eAaImIp3nP
válečné	válečný	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
od	od	k7c2
hodnosti	hodnost	k1gFnSc2
četaře	četař	k1gMnSc2
a	a	k8xC
výš	vysoce	k6eAd2
od	od	k7c2
manuální	manuální	k2eAgFnSc2d1
práce	práce	k1gFnSc2
a	a	k8xC
stanovují	stanovovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
vězňům	vězeň	k1gMnPc3
vykonávající	vykonávající	k2eAgFnSc4d1
práci	práce	k1gFnSc4
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
poskytnuty	poskytnut	k2eAgInPc4d1
příděly	příděl	k1gInPc4
navíc	navíc	k6eAd1
a	a	k8xC
další	další	k2eAgFnSc6d1
nezbytnosti	nezbytnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
nebylo	být	k5eNaImAgNnS
signatářem	signatář	k1gMnSc7
Ženevských	ženevský	k2eAgFnPc2d1
úmluv	úmluva	k1gFnPc2
o	o	k7c6
válečných	válečná	k1gFnPc6
zajatců	zajatec	k1gMnPc2
z	z	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
a	a	k8xC
japonské	japonský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
konvenci	konvence	k1gFnSc4
beztak	beztak	k9
nesledovaly	sledovat	k5eNaImAgFnP
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
měly	mít	k5eAaImAgInP
ratifikovanou	ratifikovaný	k2eAgFnSc4d1
Ženevskou	ženevský	k2eAgFnSc4d1
úmluvu	úmluva	k1gFnSc4
o	o	k7c6
nemocných	nemocný	k1gMnPc2
a	a	k8xC
raněných	raněný	k1gMnPc2
z	z	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Comfort	Comfort	k1gInSc1
women	womna	k1gFnPc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Comfort	Comfort	k1gInSc1
women	women	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Termín	termín	k1gInSc1
"	"	kIx"
<g/>
comfort	comfort	k1gInSc1
women	women	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
慰	慰	k?
ianfu	ianf	k1gInSc2
<g/>
,	,	kIx,
korejsky	korejsky	k6eAd1
위	위	k?
üanbu	üanb	k1gInSc2
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
„	„	k?
<g/>
ženy	žena	k1gFnSc2
pro	pro	k7c4
útěchu	útěcha	k1gFnSc4
<g/>
,	,	kIx,
utěšitelky	utěšitelka	k1gFnPc4
<g/>
“	“	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
eufemismus	eufemismus	k1gInSc1
pro	pro	k7c4
ženy	žena	k1gFnPc4
v	v	k7c6
japonských	japonský	k2eAgInPc6d1
armádních	armádní	k2eAgInPc6d1
bordelech	bordel	k1gInPc6
na	na	k7c6
okupovaných	okupovaný	k2eAgNnPc6d1
územích	území	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
často	často	k6eAd1
přijaty	přijmout	k5eAaPmNgInP
podvodem	podvod	k1gInSc7
nebo	nebo	k8xC
uneseny	unesen	k2eAgFnPc1d1
a	a	k8xC
nuceny	nutit	k5eAaImNgFnP
k	k	k7c3
sexuálnímu	sexuální	k2eAgNnSc3d1
otroctví	otroctví	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
historik	historik	k1gMnSc1
Jošiaki	Jošiake	k1gFnSc4
Jošimi	Joši	k1gFnPc7
zveřejnil	zveřejnit	k5eAaPmAgInS
materiál	materiál	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
bádání	bádání	k1gNnSc4
v	v	k7c6
archivech	archiv	k1gInPc6
v	v	k7c6
japonském	japonský	k2eAgInSc6d1
národním	národní	k2eAgInSc6d1
institutu	institut	k1gInSc6
pro	pro	k7c4
vojenská	vojenský	k2eAgNnPc4d1
studia	studio	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jošimi	Joši	k1gFnPc7
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
existuje	existovat	k5eAaImIp3nS
přímá	přímý	k2eAgFnSc1d1
souvislost	souvislost	k1gFnSc1
mezi	mezi	k7c7
císařskými	císařský	k2eAgFnPc7d1
institucemi	instituce	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byla	být	k5eAaImAgFnS
Kóain	Kóain	k2eAgInSc4d1
(	(	kIx(
<g/>
východoasijský	východoasijský	k2eAgInSc1d1
rozvojový	rozvojový	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
"	"	kIx"
<g/>
komfortními	komfortní	k2eAgFnPc7d1
stanicemi	stanice	k1gFnPc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byly	být	k5eAaImAgInP
Jošimovy	Jošimův	k2eAgInPc1d1
nálezy	nález	k1gInPc1
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1993	#num#	k4
zveřejněny	zveřejnit	k5eAaPmNgFnP
v	v	k7c6
japonských	japonský	k2eAgInPc6d1
sdělovacích	sdělovací	k2eAgInPc6d1
prostředcích	prostředek	k1gInPc6
<g/>
,	,	kIx,
způsobily	způsobit	k5eAaPmAgFnP
rozruch	rozruch	k1gInSc4
a	a	k8xC
přinutily	přinutit	k5eAaPmAgFnP
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
zastupovanou	zastupovaný	k2eAgFnSc4d1
generálním	generální	k2eAgInSc7d1
sekretářem	sekretář	k1gInSc7
Kato	Kato	k1gMnSc1
Koičim	Koičim	k1gMnSc1
<g/>
,	,	kIx,
uznat	uznat	k5eAaPmF
některé	některý	k3yIgFnPc4
skutečnosti	skutečnost	k1gFnPc4
ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
Kiiči	Kiič	k1gFnSc6
Mijazawa	Mijazawa	k1gMnSc1
předložil	předložit	k5eAaPmAgMnS
formální	formální	k2eAgFnSc4d1
omluvu	omluva	k1gFnSc4
za	za	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
utrpení	utrpení	k1gNnSc1
obětí	oběť	k1gFnPc2
během	během	k7c2
cesty	cesta	k1gFnSc2
po	po	k7c6
Jižní	jižní	k2eAgFnSc6d1
Koreji	Korea	k1gFnSc6
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
vydala	vydat	k5eAaPmAgFnS
japonská	japonský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
dvě	dva	k4xCgNnPc4
prohlášení	prohlášení	k1gNnPc4
<g/>
,	,	kIx,
kterými	který	k3yIgMnPc7,k3yRgMnPc7,k3yQgMnPc7
uznala	uznat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
Komfortní	komfortní	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
byly	být	k5eAaImAgFnP
provozovány	provozovat	k5eAaImNgFnP
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
žádost	žádost	k1gFnSc4
tehdejší	tehdejší	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Japonská	japonský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
přímo	přímo	k6eAd1
nebo	nebo	k8xC
nepřímo	přímo	k6eNd1
podílela	podílet	k5eAaImAgFnS
na	na	k7c6
zřizování	zřizování	k1gNnSc6
a	a	k8xC
řízení	řízení	k1gNnSc6
komfortních	komfortní	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
a	a	k8xC
přesunů	přesun	k1gInPc2
žen	žena	k1gFnPc2
<g/>
"	"	kIx"
a	a	k8xC
že	že	k8xS
ženy	žena	k1gFnPc1
byly	být	k5eAaImAgFnP
"	"	kIx"
<g/>
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
přijímány	přijímán	k2eAgInPc4d1
proti	proti	k7c3
jejich	jejich	k3xOp3gFnSc3
vlastní	vlastní	k2eAgFnSc3d1
vůli	vůle	k1gFnSc3
skrze	skrze	k?
přemlouvání	přemlouvání	k1gNnSc4
a	a	k8xC
donucování	donucování	k1gNnSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2007	#num#	k4
byl	být	k5eAaImAgInS
podnícen	podnícen	k2eAgInSc1d1
spor	spor	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
japonský	japonský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Šinzó	Šinzó	k1gMnSc1
Abe	Abe	k1gMnSc1
zmínil	zmínit	k5eAaPmAgMnS
o	o	k7c6
návrhu	návrh	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
americká	americký	k2eAgFnSc1d1
Sněmovna	sněmovna	k1gFnSc1
reprezentantů	reprezentant	k1gMnPc2
vyzývá	vyzývat	k5eAaImIp3nS
japonskou	japonský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
"	"	kIx"
<g/>
omluvila	omluvit	k5eAaPmAgFnS
a	a	k8xC
potvrdila	potvrdit	k5eAaPmAgFnS
<g/>
"	"	kIx"
za	za	k7c4
roli	role	k1gFnSc4
japonské	japonský	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
ve	v	k7c6
válečném	válečný	k2eAgInSc6d1
sexuální	sexuální	k2eAgNnSc4d1
otroctví	otroctví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abe	Abe	k1gMnSc1
popřel	popřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
vztahovalo	vztahovat	k5eAaImAgNnS
na	na	k7c4
"	"	kIx"
<g/>
komfortní	komfortní	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
důkaz	důkaz	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
dokazuje	dokazovat	k5eAaImIp3nS
donucování	donucování	k1gNnSc4
<g/>
,	,	kIx,
nic	nic	k3yNnSc1
to	ten	k3xDgNnSc4
nepodporuje	podporovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Abeho	Abe	k1gMnSc4
připomínky	připomínka	k1gFnSc2
v	v	k7c6
zámoří	zámoří	k1gNnSc6
vyvolaly	vyvolat	k5eAaPmAgFnP
negativní	negativní	k2eAgFnPc1d1
reakce	reakce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
úvodník	úvodník	k1gInSc4
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Nešlo	jít	k5eNaImAgNnS
o	o	k7c4
komerční	komerční	k2eAgInPc4d1
bordely	bordel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síly	síla	k1gFnPc4
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
přímo	přímo	k6eAd1
nebo	nebo	k8xC
nepřímo	přímo	k6eNd1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
k	k	k7c3
přijímání	přijímání	k1gNnSc3
těchto	tento	k3xDgFnPc2
žen	žena	k1gFnPc2
využíváno	využívat	k5eAaPmNgNnS,k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
dělo	dělo	k1gNnSc1
bylo	být	k5eAaImAgNnS
soustavné	soustavný	k2eAgNnSc1d1
znásilňování	znásilňování	k1gNnSc1
<g/>
,	,	kIx,
ne	ne	k9
prostituce	prostituce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapojení	zapojení	k1gNnSc1
japonské	japonský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
je	být	k5eAaImIp3nS
popsáno	popsat	k5eAaPmNgNnS
ve	v	k7c6
vlastních	vlastní	k2eAgInPc6d1
vojenských	vojenský	k2eAgInPc6d1
vládních	vládní	k2eAgInPc6d1
záznamech	záznam	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšší	vysoký	k2eAgInPc1d2
Tokijský	tokijský	k2eAgMnSc1d1
funkcionář	funkcionář	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
více	hodně	k6eAd2
či	či	k8xC
méně	málo	k6eAd2
za	za	k7c4
tyto	tento	k3xDgInPc4
hrozné	hrozný	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
omluvil	omluvit	k5eAaPmAgMnS
<g/>
...	...	k?
<g/>
včera	včera	k6eAd1
zdráhavě	zdráhavě	k6eAd1
uznal	uznat	k5eAaPmAgMnS
kvazi	kvazi	k6eAd1
omluvu	omluva	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
jako	jako	k9
součást	součást	k1gFnSc1
preventivního	preventivní	k2eAgNnSc2d1
prohlášení	prohlášení	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
jeho	jeho	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
hovory	hovor	k1gInPc1
odmítla	odmítnout	k5eAaPmAgFnS
a	a	k8xC
americký	americký	k2eAgInSc1d1
kongres	kongres	k1gInSc1
teď	teď	k6eAd1
projednává	projednávat	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc4d1
omluvu	omluva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amerika	Amerika	k1gFnSc1
není	být	k5eNaImIp3nS
jedinou	jediný	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
zájem	zájem	k1gInSc4
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Japonsko	Japonsko	k1gNnSc1
opožděně	opožděně	k6eAd1
přijalo	přijmout	k5eAaPmAgNnS
plnou	plný	k2eAgFnSc4d1
odpovědnost	odpovědnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korea	Korea	k1gFnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
a	a	k8xC
Filipíny	Filipíny	k1gFnPc1
jsou	být	k5eAaImIp3nP
také	také	k9
z	z	k7c2
let	léto	k1gNnPc2
japonského	japonský	k2eAgNnSc2d1
mlžení	mlžení	k1gNnSc2
rozzuřeny	rozzuřen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
den	den	k1gInSc4
veterán	veterán	k1gMnSc1
Jasudži	Jasudž	k1gFnSc3
Kaneko	Kaneko	k1gNnSc1
listu	list	k1gInSc2
The	The	k1gFnSc2
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
potvrdil	potvrdit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
trpěly	trpět	k5eAaImAgFnP
<g/>
,	,	kIx,
ale	ale	k8xC
nezáleželo	záležet	k5eNaImAgNnS
nám	my	k3xPp1nPc3
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
ženy	žena	k1gFnPc1
žily	žít	k5eAaImAgFnP
nebo	nebo	k8xC
zemřely	zemřít	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
císařovi	císařův	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ať	ať	k8xC,k8xS
už	už	k6eAd1
ve	v	k7c6
vojenských	vojenský	k2eAgInPc6d1
bordelech	bordel	k1gInPc6
nebo	nebo	k8xC
ve	v	k7c6
vesnicích	vesnice	k1gFnPc6
jsme	být	k5eAaImIp1nP
bez	bez	k7c2
zdráhání	zdráhání	k1gNnSc2
znásilňovali	znásilňovat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2007	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
Jošimi	Joši	k1gFnPc7
a	a	k8xC
další	další	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Hirofumi	Hirofu	k1gFnPc7
Hajaši	Hajaše	k1gFnSc6
objev	objev	k1gInSc1
z	z	k7c2
archivů	archiv	k1gInPc2
Tokijského	tokijský	k2eAgInSc2d1
tribunálu	tribunál	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
sedm	sedm	k4xCc4
oficiálních	oficiální	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
císařské	císařský	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
jako	jako	k8xC,k8xS
Tokeitai	Tokeita	k1gFnPc1
(	(	kIx(
<g/>
námořní	námořní	k2eAgFnSc1d1
tajná	tajný	k2eAgFnSc1d1
policie	policie	k1gFnSc1
<g/>
)	)	kIx)
přímo	přímo	k6eAd1
nutily	nutit	k5eAaImAgFnP
ženy	žena	k1gFnPc1
k	k	k7c3
práci	práce	k1gFnSc3
ve	v	k7c6
frontových	frontový	k2eAgInPc6d1
bordelech	bordel	k1gInPc6
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
Indočíně	Indočína	k1gFnSc6
a	a	k8xC
Indonésii	Indonésie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
dokumenty	dokument	k1gInPc4
byly	být	k5eAaImAgInP
původně	původně	k6eAd1
zveřejněny	zveřejnit	k5eAaPmNgInP
u	u	k7c2
soudu	soud	k1gInSc2
pro	pro	k7c4
válečné	válečný	k2eAgMnPc4d1
zločince	zločinec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
citace	citace	k1gFnSc1
jistého	jistý	k2eAgMnSc2d1
poručíka	poručík	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
přiznal	přiznat	k5eAaPmAgMnS
k	k	k7c3
organizaci	organizace	k1gFnSc3
nevěstince	nevěstinec	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
sám	sám	k3xTgMnSc1
používal	používat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
členy	člen	k1gMnPc4
Tokeitai	Tokeita	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
zatýkali	zatýkat	k5eAaImAgMnP
ženy	žena	k1gFnPc4
na	na	k7c6
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
pak	pak	k6eAd1
pod	pod	k7c7
nátlakem	nátlak	k1gInSc7
prošly	projít	k5eAaPmAgFnP
lékařskou	lékařský	k2eAgFnSc7d1
prohlídkou	prohlídka	k1gFnSc7
a	a	k8xC
byly	být	k5eAaImAgInP
umístěny	umístit	k5eAaPmNgInP
do	do	k7c2
bordelu	bordel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
novinář	novinář	k1gMnSc1
Taičiro	Taičiro	k1gNnSc4
Kaidžimura	Kaidžimur	k1gMnSc2
objev	objev	k1gInSc4
30	#num#	k4
holandských	holandský	k2eAgInPc2d1
vládních	vládní	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
předložených	předložený	k2eAgInPc2d1
u	u	k7c2
Tokijského	tokijský	k2eAgInSc2d1
tribunálu	tribunál	k1gInSc2
jako	jako	k8xS,k8xC
důkaz	důkaz	k1gInSc1
nucené	nucený	k2eAgFnSc2d1
prostituce	prostituce	k1gFnSc2
během	během	k7c2
incidentu	incident	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
v	v	k7c6
Magelangu	Magelang	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
jiných	jiný	k2eAgInPc6d1
případech	případ	k1gInPc6
některé	některý	k3yIgFnSc2
oběti	oběť	k1gFnSc2
z	z	k7c2
Východního	východní	k2eAgInSc2d1
Timoru	Timor	k1gInSc2
vypověděly	vypovědět	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
nebyly	být	k5eNaImAgFnP
dost	dost	k6eAd1
staré	starý	k2eAgFnPc1d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
schopné	schopný	k2eAgNnSc4d1
menstruovat	menstruovat	k5eAaImF
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
japonskými	japonský	k2eAgInPc7d1
vojáky	voják	k1gMnPc4
přinuceny	přinucen	k2eAgMnPc4d1
a	a	k8xC
opakovaně	opakovaně	k6eAd1
znásilněny	znásilněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Holandsko-indonéská	holandsko-indonéský	k2eAgFnSc1d1
"	"	kIx"
<g/>
Comfort	Comfort	k1gInSc1
woman	woman	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Ruff-O	Ruff-O	k1gMnSc1
<g/>
'	'	kIx"
<g/>
Hearn	Hearn	k1gMnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
občan	občan	k1gMnSc1
Austrálie	Austrálie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dala	dát	k5eAaPmAgFnS
americkému	americký	k2eAgInSc3d1
výboru	výbor	k1gInSc3
důkaz	důkaz	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
japonská	japonský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
nedokázala	dokázat	k5eNaPmAgFnS
převzít	převzít	k5eAaPmF
odpovědnost	odpovědnost	k1gFnSc4
za	za	k7c4
své	svůj	k3xOyFgInPc4
zločiny	zločin	k1gInPc4
<g/>
,	,	kIx,
nechtěla	chtít	k5eNaImAgFnS
zaplatit	zaplatit	k5eAaPmF
obětem	oběť	k1gFnPc3
kompenzace	kompenzace	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
chce	chtít	k5eAaImIp3nS
přepsat	přepsat	k5eAaPmF
historii	historie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruff-O	Ruff-O	k1gFnPc2
<g/>
'	'	kIx"
<g/>
Hearn	Hearna	k1gFnPc2
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
v	v	k7c6
19	#num#	k4
letech	léto	k1gNnPc6
znásilňována	znásilňován	k2eAgFnSc1d1
japonskými	japonský	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
"	"	kIx"
<g/>
dnem	den	k1gInSc7
i	i	k8xC
nocí	noc	k1gFnSc7
<g/>
"	"	kIx"
po	po	k7c4
dobu	doba	k1gFnSc4
tří	tři	k4xCgInPc2
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
Japonka	Japonka	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
svědectví	svědectví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
bývalá	bývalý	k2eAgFnSc1d1
"	"	kIx"
<g/>
Comfort	Comfort	k1gInSc1
woman	woman	k1gInSc1
<g/>
"	"	kIx"
byla	být	k5eAaImAgFnS
nucena	nutit	k5eAaImNgFnS
k	k	k7c3
práci	práce	k1gFnSc3
pro	pro	k7c4
japonské	japonský	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
na	na	k7c6
Tchaj-wanu	Tchaj-wan	k1gInSc6
a	a	k8xC
své	svůj	k3xOyFgFnSc2
vzpomínky	vzpomínka	k1gFnSc2
publikovala	publikovat	k5eAaBmAgFnS
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
Suzuko	Suzuko	k1gNnSc1
Širota	Širota	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Existují	existovat	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
o	o	k7c6
místu	místo	k1gNnSc3
původu	původ	k1gInSc2
těchto	tento	k3xDgFnPc2
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
některé	některý	k3yIgInPc1
japonské	japonský	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
většina	většina	k1gFnSc1
žen	žena	k1gFnPc2
byla	být	k5eAaImAgFnS
z	z	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
<g/>
,	,	kIx,
včetně	včetně	k7c2
Jošimi	Joši	k1gFnPc7
<g/>
,	,	kIx,
argumentují	argumentovat	k5eAaImIp3nP
že	že	k8xS
do	do	k7c2
těchto	tento	k3xDgFnPc2
aktivit	aktivita	k1gFnPc2
bylo	být	k5eAaImAgNnS
donuceno	donutit	k5eAaPmNgNnS
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
asi	asi	k9
200	#num#	k4
000	#num#	k4
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
většinou	většina	k1gFnSc7
z	z	k7c2
Koreje	Korea	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Filipíny	Filipíny	k1gFnPc1
<g/>
,	,	kIx,
Barma	Barma	k1gFnSc1
<g/>
,	,	kIx,
Nizozemská	nizozemský	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
a	a	k8xC
Austrálie	Austrálie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
2014	#num#	k4
byly	být	k5eAaImAgFnP
zveřejněny	zveřejnit	k5eAaPmNgInP
další	další	k2eAgInPc1d1
oficiální	oficiální	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
z	z	k7c2
archivů	archiv	k1gInPc2
japonské	japonský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
dokumentují	dokumentovat	k5eAaBmIp3nP
sexuální	sexuální	k2eAgNnSc4d1
násilí	násilí	k1gNnSc4
páchaná	páchaný	k2eAgFnSc1d1
japonskými	japonský	k2eAgMnPc7d1
císařskými	císařský	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
ve	v	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Indočíně	Indočína	k1gFnSc6
a	a	k8xC
Indonésii	Indonésie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
americký	americký	k2eAgInSc1d1
výbor	výbor	k1gInSc1
pro	pro	k7c4
zahraniční	zahraniční	k2eAgFnPc4d1
věci	věc	k1gFnPc4
sněmovny	sněmovna	k1gFnSc2
reprezentantů	reprezentant	k1gMnPc2
předložila	předložit	k5eAaPmAgFnS
rezoluci	rezoluce	k1gFnSc4
požadující	požadující	k2eAgFnSc4d1
<g/>
,	,	kIx,
že	že	k8xS
Japonsko	Japonsko	k1gNnSc1
"	"	kIx"
<g/>
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
uznat	uznat	k5eAaPmF
<g/>
,	,	kIx,
omluvit	omluvit	k5eAaPmF
se	se	k3xPyFc4
a	a	k8xC
přijmout	přijmout	k5eAaPmF
historickou	historický	k2eAgFnSc4d1
odpovědnost	odpovědnost	k1gFnSc4
jasně	jasně	k6eAd1
a	a	k8xC
jednoznačně	jednoznačně	k6eAd1
za	za	k7c4
své	svůj	k3xOyFgInPc4
vojenské	vojenský	k2eAgInPc4d1
nátlaky	nátlak	k1gInPc4
na	na	k7c6
žen	žena	k1gFnPc2
do	do	k7c2
sexuálního	sexuální	k2eAgNnSc2d1
otroctví	otroctví	k1gNnSc2
během	během	k7c2
války	válka	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2007	#num#	k4
rezoluce	rezoluce	k1gFnSc2
sněmovnou	sněmovna	k1gFnSc7
reprezentantů	reprezentant	k1gMnPc2
prošla	projít	k5eAaPmAgFnS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Šinzó	Šinzó	k1gMnSc1
Abe	Abe	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
"	"	kIx"
<g/>
politováníhodné	politováníhodný	k2eAgFnPc4d1
<g/>
"	"	kIx"
</s>
<s>
Rabování	rabování	k1gNnSc1
</s>
<s>
Mnoho	mnoho	k4c1
historiků	historik	k1gMnPc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
japonská	japonský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
a	a	k8xC
jednotlivý	jednotlivý	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
personál	personál	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
období	období	k1gNnSc6
1895	#num#	k4
až	až	k9
1945	#num#	k4
zapojen	zapojit	k5eAaPmNgInS
do	do	k7c2
rozsáhlého	rozsáhlý	k2eAgNnSc2d1
rabování	rabování	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Ukradený	ukradený	k2eAgInSc1d1
majetek	majetek	k1gInSc1
zahrnoval	zahrnovat	k5eAaImAgInS
soukromé	soukromý	k2eAgInPc4d1
pozemky	pozemek	k1gInPc4
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
cenných	cenný	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
uloupených	uloupený	k2eAgInPc2d1
z	z	k7c2
bank	banka	k1gFnPc2
<g/>
,	,	kIx,
trezorů	trezor	k1gInPc2
<g/>
,	,	kIx,
chrámů	chrám	k1gInPc2
<g/>
,	,	kIx,
kostelů	kostel	k1gInPc2
<g/>
,	,	kIx,
mešit	mešita	k1gFnPc2
<g/>
,	,	kIx,
muzeí	muzeum	k1gNnPc2
<g/>
,	,	kIx,
jiných	jiný	k2eAgFnPc2d1
podnikatelských	podnikatelský	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
i	i	k8xC
soukromých	soukromý	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Soudy	soud	k1gInPc1
za	za	k7c4
válečné	válečný	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tokijský	tokijský	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BLUMENTHAL	BLUMENTHAL	kA
<g/>
,	,	kIx,
Ralph	Ralph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
<g/>
:	:	kIx,
Revisiting	Revisiting	k1gInSc1
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
Atrocities	Atrocities	k1gMnSc1
<g/>
;	;	kIx,
Comparing	Comparing	k1gInSc1
the	the	k?
Unspeakable	Unspeakable	k1gFnSc2
to	ten	k3xDgNnSc1
the	the	k?
Unthinkable	Unthinkable	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
March	March	k1gInSc1
7	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
World	World	k1gMnSc1
|	|	kIx~
Scarred	Scarred	k1gMnSc1
by	by	k9
history	histor	k1gMnPc4
<g/>
:	:	kIx,
The	The	k1gMnSc1
Rape	rape	k1gNnSc2
of	of	k?
Nanking	Nanking	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SANGER	SANGER	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japanese	Japanese	k1gFnSc1
Edgy	Edga	k1gFnSc2
Over	Over	k1gMnSc1
Emperor	Emperor	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Visit	visita	k1gFnPc2
to	ten	k3xDgNnSc1
China	China	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
October	October	k1gInSc1
22	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Japanese	Japanese	k1gFnSc1
War	War	k1gFnSc2
Criminals	Criminalsa	k1gFnPc2
World	World	k1gMnSc1
War	War	k1gMnSc1
Two	Two	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
National	National	k1gMnSc1
Archives	Archives	k1gMnSc1
(	(	kIx(
<g/>
U.K.	U.K.	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Japanese	Japanese	k1gFnSc2
War	War	k1gMnSc1
Crimes	Crimes	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
National	National	k1gMnSc1
Archives	Archives	k1gMnSc1
(	(	kIx(
<g/>
U.	U.	kA
<g/>
S.	S.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pacific	Pacific	k1gMnSc1
Theater	Theater	k1gMnSc1
Document	Document	k1gMnSc1
Archive	archiv	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
War	War	k1gMnSc1
Crimes	Crimes	k1gMnSc1
Studies	Studies	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
University	universita	k1gFnPc1
of	of	k?
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
Berkeley	Berkelea	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
13	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kafala	Kafala	k1gFnSc1
<g/>
,	,	kIx,
Tarik	Tarik	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	What	k1gInSc1
is	is	k?
a	a	k8xC
war	war	k?
crime	crimat	k5eAaPmIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
news	newsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
October	Octobra	k1gFnPc2
21	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bibliography	Bibliographa	k1gFnSc2
<g/>
:	:	kIx,
War	War	k1gMnSc1
Crimes	Crimes	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sigur	Sigur	k1gMnSc1
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
Asian	Asian	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
,	,	kIx,
George	George	k1gFnSc1
Washington	Washington	k1gInSc1
University	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TABUCHI	TABUCHI	kA
<g/>
,	,	kIx,
Hiroko	Hiroko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japan	japan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Abe	Abe	k1gFnSc7
<g/>
:	:	kIx,
No	no	k9
Proof	Proof	k1gInSc1
of	of	k?
WWII	WWII	kA
Sex	sex	k1gInSc1
Slaves	Slaves	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
Post	post	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Associated	Associated	k1gMnSc1
Press	Press	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
March	March	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Toland	Toland	k1gInSc1
<g/>
,	,	kIx,
Infamy	Infam	k1gInPc1
<g/>
↑	↑	k?
Japan	japan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Military	Militar	k1gMnPc7
Stopped	Stopped	k1gInSc4
Warning	Warning	k1gInSc1
of	of	k?
Pearl	Pearl	k1gInSc1
Harbor	Harbor	k1gMnSc1
Attack	Attack	k1gMnSc1
<g/>
,	,	kIx,
Says	Says	k1gInSc1
Iguchi	Iguch	k1gFnSc2
<g/>
.	.	kIx.
www.law.virginia.edu	www.law.virginia.edu	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
and	and	k?
The	The	k1gMnSc1
Tokyo	Tokyo	k1gMnSc1
Trials	Trials	k1gInSc1
<g/>
.	.	kIx.
faculty	facult	k1gInPc1
<g/>
.	.	kIx.
<g/>
virginia	virginium	k1gNnPc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Martin	Martin	k1gMnSc1
V.	V.	kA
Melosi	Melose	k1gFnSc6
<g/>
,	,	kIx,
The	The	k1gMnSc1
Shadow	Shadow	k1gMnSc1
of	of	k?
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
<g/>
:	:	kIx,
Political	Political	k1gMnSc1
controversy	controversa	k1gFnSc2
over	over	k1gMnSc1
the	the	k?
Surprise	Surprise	k1gFnSc2
Attack	Attacka	k1gFnPc2
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Gordon	Gordon	k1gMnSc1
W.	W.	kA
Prange	Prang	k1gFnPc1
<g/>
,	,	kIx,
etc	etc	k?
<g/>
.	.	kIx.
al	ala	k1gFnPc2
<g/>
,	,	kIx,
At	At	k1gMnSc1
Dawn	Dawn	k1gMnSc1
We	We	k1gMnSc1
Slept	Slept	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
↑	↑	k?
Gordon	Gordon	k1gMnSc1
W.	W.	kA
Prange	Prang	k1gFnPc1
<g/>
,	,	kIx,
etc	etc	k?
<g/>
.	.	kIx.
al	ala	k1gFnPc2
<g/>
,	,	kIx,
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Verdict	Verdict	k1gMnSc1
of	of	k?
History	Histor	k1gInPc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
↑	↑	k?
Antony	anton	k1gInPc1
Best	Besta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britain	Britain	k1gInSc1
<g/>
,	,	kIx,
Japan	japan	k1gInSc1
and	and	k?
Pearl	Pearl	k1gMnSc1
Harbour	Harbour	k1gMnSc1
<g/>
:	:	kIx,
Avoiding	Avoiding	k1gInSc1
War	War	k1gFnSc2
in	in	k?
East	East	k1gMnSc1
Asia	Asia	k1gMnSc1
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gInSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
1	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
415	#num#	k4
<g/>
-	-	kIx~
<g/>
11171	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Study	stud	k1gInPc4
"	"	kIx"
<g/>
Riposte	Ripost	k1gInSc5
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Analytical	Analytical	k1gFnSc1
papers	papersa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Rummell	Rummell	k1gInSc1
<g/>
,	,	kIx,
Statistics	Statistics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hawaii	Hawaie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
China	China	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Bloody	Blood	k1gInPc7
Century	Centura	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hawaii	Hawaie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chang	Chang	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
102	#num#	k4
<g/>
↑	↑	k?
LEI	lei	k1gInPc2
<g/>
,	,	kIx,
Wan	Wan	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Chinese	Chinese	k1gFnSc2
Islamic	Islamice	k1gFnPc2
"	"	kIx"
<g/>
Goodwill	Goodwill	k1gInSc1
Mission	Mission	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Middle	Middle	k1gFnSc3
East	East	k2eAgInSc1d1
<g/>
"	"	kIx"
During	During	k1gInSc1
the	the	k?
Anti-Japanese	Anti-Japanese	k1gFnSc1
War	War	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
DÎVÂN	DÎVÂN	kA
DISIPLINLERARASI	DISIPLINLERARASI	kA
ÇALISMALAR	ÇALISMALAR	kA
DERGISI	DERGISI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
February	Februara	k1gFnSc2
2010	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
139	#num#	k4
<g/>
–	–	k?
<g/>
141	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
19	#num#	k4
June	jun	k1gMnSc5
2014	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Schmidt	Schmidt	k1gMnSc1
1982	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
36	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ramsey	Ramsea	k1gFnSc2
1990	#num#	k4
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
329	#num#	k4
<g/>
–	–	k?
<g/>
330	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Transcript	Transcript	k1gInSc1
of	of	k?
the	the	k?
interview	interview	k1gNnSc2
with	witha	k1gFnPc2
Lee	Lea	k1gFnSc6
Yuan	Yuan	k1gMnSc1
Kew	Kew	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
News	News	k1gInSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
sg	sg	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Japanese	Japanese	k1gFnSc2
Treatment	Treatment	k1gMnSc1
of	of	k?
Chinese	Chinese	k1gFnSc2
Prisoners	Prisonersa	k1gFnPc2
<g/>
,	,	kIx,
1931	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
,	,	kIx,
Hayashi	Hayashi	k1gNnSc4
Hirofumi	Hirofu	k1gFnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geocities	Geocities	k1gInSc1
<g/>
.	.	kIx.
<g/>
jp	jp	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Unmasking	Unmasking	k1gInSc1
Horror	horror	k1gInSc1
<g/>
"	"	kIx"
Nicholas	Nicholas	k1gMnSc1
D.	D.	kA
Kristof	Kristof	k1gMnSc1
(	(	kIx(
<g/>
March	March	k1gInSc1
17	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
)	)	kIx)
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
special	speciat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
report	report	k1gInSc1
<g/>
.	.	kIx.
<g/>
;	;	kIx,
Japan	japan	k1gInSc1
Confronting	Confronting	k1gInSc1
Gruesome	Gruesom	k1gInSc5
War	War	k1gFnSc2
Atrocity	Atrocit	k1gInPc4
<g/>
↑	↑	k?
Byrd	Byrd	k1gInSc4
<g/>
,	,	kIx,
Gregory	Gregor	k1gMnPc4
Dean	Deana	k1gFnPc2
<g/>
,	,	kIx,
General	General	k1gMnPc4
Ishii	Ishie	k1gFnSc4
Shiro	Shiro	k1gNnSc1
<g/>
:	:	kIx,
His	his	k1gNnSc1
Legacy	Legaca	k1gFnSc2
is	is	k?
that	that	k1gInSc1
of	of	k?
a	a	k8xC
Genius	genius	k1gMnSc1
and	and	k?
Madman	Madman	k1gMnSc1
Archivováno	archivován	k2eAgNnSc4d1
8	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
p.	p.	k?
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
PDF	PDF	kA
document	document	k1gInSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
GlobalSecurity	GlobalSecurita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
2005	#num#	k4
"	"	kIx"
<g/>
Biological	Biological	k1gFnSc1
Weapons	Weapons	k1gInSc1
Program	program	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Downloaded	Downloaded	k1gMnSc1
November	November	k1gMnSc1
26	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
↑	↑	k?
Daniel	Daniel	k1gMnSc1
Barenblatt	Barenblatt	k1gMnSc1
<g/>
,	,	kIx,
A	a	k8xC
Plague	Plague	k1gInSc1
upon	upona	k1gFnPc2
Humanity	humanita	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
<g/>
xii	xii	k?
<g/>
,	,	kIx,
173	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Christopher	Christophra	k1gFnPc2
Hudson	Hudson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doctors	Doctors	k1gInSc1
of	of	k?
Depravity	Depravita	k1gFnSc2
<g/>
.	.	kIx.
www.dailymail.co.uk	www.dailymail.co.uk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Daily	Daila	k1gFnPc1
Mail	mail	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
March	March	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Japan	japan	k1gInSc1
digs	digs	k1gInSc1
up	up	k?
site	sitat	k5eAaPmIp3nS
linked	linked	k1gInSc4
to	ten	k3xDgNnSc4
WWII	WWII	kA
human	human	k1gInSc1
experiments	experiments	k1gInSc1
<g/>
.	.	kIx.
www.telegraph.co.uk	www.telegraph.co.uk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Telegraph	Telegraph	k1gMnSc1
<g/>
,	,	kIx,
February	Februar	k1gInPc1
21	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
David	David	k1gMnSc1
McNeill	McNeill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japan	japan	k1gInSc1
confronts	confronts	k6eAd1
truth	truth	k1gInSc1
about	about	k2eAgInSc1d1
its	its	k?
germ	germ	k1gInSc1
warfare	warfar	k1gMnSc5
tests	tests	k6eAd1
on	on	k3xPp3gInSc1
prisoners	prisoners	k1gInSc1
of	of	k?
war	war	k?
<g/>
.	.	kIx.
www.independent.co.uk	www.independent.co.uk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
<g/>
,	,	kIx,
February	Februar	k1gInPc1
22	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
pacificwrecks	pacificwrecks	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gFnSc1
Denver	Denver	k1gInSc1
Post	post	k1gInSc1
<g/>
,	,	kIx,
June	jun	k1gMnSc5
1	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
cited	cited	k1gInSc4
by	by	kYmCp3nS
Gary	Gara	k1gMnSc2
K.	K.	kA
Reynolds	Reynolds	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
"	"	kIx"
<g/>
U.	U.	kA
<g/>
S.	S.	kA
Prisoners	Prisonersa	k1gFnPc2
of	of	k?
War	War	k1gMnSc1
and	and	k?
Civilian	Civilian	k1gMnSc1
American	American	k1gMnSc1
Citizens	Citizensa	k1gFnPc2
Captured	Captured	k1gMnSc1
and	and	k?
Interned	Interned	k1gInSc1
by	by	kYmCp3nS
Japan	japan	k1gInSc4
in	in	k?
World	World	k1gInSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
Issue	Issu	k1gFnSc2
of	of	k?
Compensation	Compensation	k1gInSc1
by	by	kYmCp3nS
Japan	japan	k1gInSc4
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Library	Librar	k1gInPc1
of	of	k?
Congress	Congress	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.house.gov	www.house.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
29	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Landas	Landas	k1gInSc1
<g/>
,	,	kIx,
Marc	Marc	k1gFnSc1
The	The	k1gMnSc1
Fallen	Fallen	k1gInSc1
A	a	k8xC
True	True	k1gInSc1
Story	story	k1gFnSc2
of	of	k?
American	American	k1gInSc1
POWs	POWs	k1gInSc1
and	and	k?
Japanese	Japanese	k1gFnSc2
Wartime	Wartim	k1gInSc5
Atrocities	Atrocities	k1gInSc4
Hoboken	Hoboken	k2eAgMnSc1d1
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
2004	#num#	k4
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
471	#num#	k4
<g/>
-	-	kIx~
<g/>
42119	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
7	#num#	k4
<g/>
↑	↑	k?
Naomi	Nao	k1gFnPc7
Baumslag	Baumslag	k1gMnSc1
<g/>
,	,	kIx,
Murderous	Murderous	k1gMnSc1
Medicine	Medicin	k1gMnSc5
<g/>
:	:	kIx,
Nazi	Naz	k1gMnSc6
Doctors	Doctors	k1gInSc4
<g/>
,	,	kIx,
Human	Human	k1gInSc1
Experimentation	Experimentation	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Typhus	Typhus	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
<g/>
207	#num#	k4
<g/>
↑	↑	k?
Weapons	Weapons	k1gInSc1
of	of	k?
Mass	Mass	k1gInSc1
Destruction	Destruction	k1gInSc1
<g/>
:	:	kIx,
Plague	Plague	k1gFnSc1
as	as	k1gInSc1
Biological	Biological	k1gMnSc1
Weapons	Weaponsa	k1gFnPc2
Agent	agent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GlobalSecurity	GlobalSecurita	k1gFnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Amy	Amy	k1gMnSc1
Stewart	Stewart	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Where	Wher	k1gInSc5
To	to	k9
Find	Find	k?
The	The	k1gMnSc1
World	World	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Most	most	k1gInSc1
'	'	kIx"
<g/>
Wicked	Wicked	k1gInSc1
Bugs	Bugs	k1gInSc1
<g/>
'	'	kIx"
<g/>
:	:	kIx,
Fleas	Fleas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Public	publicum	k1gNnPc2
Radio	radio	k1gNnSc1
<g/>
,	,	kIx,
April	April	k1gInSc1
25	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Russell	Russell	k1gInSc1
Working	Working	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
trial	trial	k1gInSc4
of	of	k?
Unit	Unit	k1gInSc1
731	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnPc2
Japan	japan	k1gInSc4
Times	Timesa	k1gFnPc2
<g/>
,	,	kIx,
June	jun	k1gMnSc5
5	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Landas	Landas	k1gInSc1
p.	p.	k?
<g/>
255	#num#	k4
<g/>
↑	↑	k?
BBC	BBC	kA
"	"	kIx"
<g/>
Japanese	Japanese	k1gFnSc1
doctor	doctor	k1gInSc4
admits	admits	k1gInSc1
POW	POW	kA
abuse	abusus	k1gInSc5
<g/>
"	"	kIx"
Downloaded	Downloaded	k1gInSc1
November	November	k1gInSc1
26	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
12	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
GMT	GMT	kA
<g/>
↑	↑	k?
Kyodo	Kyodo	k1gNnSc1
News	News	k1gInSc4
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Ex-navy	Ex-nava	k1gFnPc1
officer	officero	k1gNnPc2
admits	admits	k6eAd1
to	ten	k3xDgNnSc1
vivisection	vivisection	k1gInSc1
of	of	k?
war	war	k?
prisoners	prisoners	k1gInSc1
in	in	k?
Philippines	Philippines	k1gInSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
reported	reported	k1gMnSc1
in	in	k?
Yahoo	Yahoo	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Asia	Asi	k1gInSc2
News	News	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
A	a	k8xC
life	life	k1gFnSc1
haunted	haunted	k1gInSc1
by	by	kYmCp3nS
WWII	WWII	kA
surgical	surgicat	k5eAaPmAgInS
killings	killings	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Brunei	Brune	k1gFnSc2
Times	Timesa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
AFP	AFP	kA
Japanese	Japanese	k1gFnSc2
veteran	veterana	k1gFnPc2
haunted	haunted	k1gMnSc1
by	by	kYmCp3nS
WWII	WWII	kA
surgical	surgicat	k5eAaPmAgInS
killings	killings	k6eAd1
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Japanese	Japanese	k1gFnSc1
war	war	k?
veteran	veteran	k1gInSc1
speaks	speaks	k1gInSc1
of	of	k?
atrocities	atrocities	k1gInSc1
in	in	k?
the	the	k?
Philippines	Philippines	k1gInSc1
-	-	kIx~
Taipei	Taipei	k1gNnSc1
Times	Timesa	k1gFnPc2
<g/>
.	.	kIx.
www.taipeitimes.com	www.taipeitimes.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Parry	Parra	k1gFnSc2
<g/>
,	,	kIx,
"	"	kIx"
<g/>
The	The	k1gMnSc1
Australian	Australian	k1gMnSc1
<g/>
"	"	kIx"
Dissect	Dissect	k2eAgMnSc1d1
them	them	k1gMnSc1
alive	aliev	k1gFnSc2
<g/>
:	:	kIx,
chilling	chilling	k1gInSc1
Imperial	Imperial	k1gMnSc1
that	that	k1gMnSc1
order	order	k1gMnSc1
could	could	k1gMnSc1
not	nota	k1gFnPc2
be	be	k?
disobeyed	disobeyed	k1gMnSc1
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dissect	Dissect	k2eAgInSc4d1
them	them	k1gInSc4
alive	aliev	k1gFnSc2
<g/>
:	:	kIx,
order	order	k1gInSc1
not	nota	k1gFnPc2
to	ten	k3xDgNnSc1
be	be	k?
disobeyed	disobeyed	k1gInSc1
|	|	kIx~
The	The	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Times	Times	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vivisectionist	Vivisectionist	k1gInSc1
recalls	recallsa	k1gFnPc2
his	his	k1gNnSc2
day	day	k?
of	of	k?
reckoning	reckoning	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japan	japan	k1gInSc1
Times	Times	k1gInSc4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
p.	p.	k?
3	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Yuki	Yuki	k1gNnSc2
Tanaka	Tanak	k1gMnSc2
<g/>
,	,	kIx,
Poison	Poison	k1gNnSc1
Gas	Gas	k1gFnSc2
<g/>
,	,	kIx,
the	the	k?
Story	story	k1gFnSc1
Japan	japan	k1gInSc1
Would	Would	k1gMnSc1
Like	Like	k1gInSc1
to	ten	k3xDgNnSc1
Forget	Forget	k1gInSc1
<g/>
,	,	kIx,
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
Atomic	Atomice	k1gFnPc2
Scientists	Scientists	k1gInSc1
<g/>
,	,	kIx,
October	October	k1gInSc1
1988	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
16	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
↑	↑	k?
Yoshimi	Yoshi	k1gFnPc7
and	and	k?
Matsuno	Matsuna	k1gFnSc5
<g/>
,	,	kIx,
Dokugasusen	Dokugasusen	k1gInSc1
kankei	kanke	k1gFnSc2
shiryô	shiryô	k?
II	II	kA
<g/>
,	,	kIx,
Kaisetsu	Kaisetsa	k1gFnSc4
1997	#num#	k4
<g/>
↑	↑	k?
Laws	Laws	k1gInSc1
of	of	k?
War	War	k1gFnSc1
<g/>
:	:	kIx,
Declaration	Declaration	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Use	usus	k1gInSc5
of	of	k?
Projectiles	Projectiles	k1gMnSc1
the	the	k?
Object	Object	k1gMnSc1
of	of	k?
Which	Which	k1gMnSc1
is	is	k?
the	the	k?
Diffusion	Diffusion	k1gInSc1
of	of	k?
Asphyxiating	Asphyxiating	k1gInSc1
or	or	k?
Deleterious	Deleterious	k1gInSc1
Gases	Gases	k1gInSc1
<g/>
;	;	kIx,
July	Jula	k1gFnSc2
29	#num#	k4
<g/>
,	,	kIx,
1899	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avalon	Avalon	k1gInSc1
<g/>
.	.	kIx.
<g/>
law	law	k?
<g/>
.	.	kIx.
<g/>
yale	yale	k1gFnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Convention	Convention	k1gInSc1
(	(	kIx(
<g/>
IV	IV	kA
<g/>
)	)	kIx)
respecting	respecting	k1gInSc1
the	the	k?
Laws	Laws	k1gInSc1
and	and	k?
Customs	Customs	k1gInSc1
of	of	k?
War	War	k1gFnSc2
on	on	k3xPp3gMnSc1
Land	Land	k1gMnSc1
and	and	k?
its	its	k?
annex	annex	k1gInSc1
<g/>
:	:	kIx,
Regulations	Regulations	k1gInSc1
concerning	concerning	k1gInSc1
the	the	k?
Laws	Laws	k1gInSc1
and	and	k?
Customs	Customs	k1gInSc1
of	of	k?
War	War	k1gFnSc2
on	on	k3xPp3gMnSc1
Land	Land	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Hague	Hague	k1gFnSc1
<g/>
,	,	kIx,
18	#num#	k4
October	October	k1gInSc1
1907	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Červeného	Červeného	k2eAgInSc2d1
kříže	kříž	k1gInSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Yuki	Yuki	k1gNnSc2
Tanaka	Tanak	k1gMnSc2
<g/>
,	,	kIx,
Poison	Poison	k1gNnSc1
Gas	Gas	k1gFnSc2
<g/>
,	,	kIx,
the	the	k?
Story	story	k1gFnSc1
Japan	japan	k1gInSc1
Would	Would	k1gMnSc1
Like	Like	k1gInSc1
to	ten	k3xDgNnSc1
Forget	Forget	k1gInSc1
<g/>
,	,	kIx,
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
Atomic	Atomice	k1gFnPc2
Scientists	Scientists	k1gInSc1
<g/>
,	,	kIx,
October	October	k1gInSc1
1988	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
<g/>
17	#num#	k4
<g/>
↑	↑	k?
Japan	japan	k1gInSc1
tested	tested	k1gMnSc1
chemical	chemicat	k5eAaPmAgMnS
weapons	weapons	k6eAd1
on	on	k3xPp3gMnSc1
Aussie	Aussie	k1gFnPc4
POW	POW	kA
<g/>
:	:	kIx,
new	new	k?
evidence	evidence	k1gFnSc1
<g/>
,	,	kIx,
http://search.japantimes.co.jp/member/nn20040727a9.html	http://search.japantimes.co.jp/member/nn20040727a9.html	k1gInSc1
<g/>
↑	↑	k?
DE	DE	k?
JONG	JONG	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
collapse	collapse	k1gFnSc1
of	of	k?
a	a	k8xC
colonial	colonial	k1gMnSc1
society	societa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Dutch	Dutch	k1gMnSc1
in	in	k?
Indonesia	Indonesia	k1gFnSc1
during	during	k1gInSc1
the	the	k?
Second	Second	k1gInSc1
World	World	k1gMnSc1
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Translation	Translation	k1gInSc1
J.	J.	kA
Kilian	Kilian	k1gInSc1
<g/>
,	,	kIx,
C.	C.	kA
Kist	Kist	k1gInSc1
and	and	k?
J.	J.	kA
Rudge	Rudge	k1gInSc1
<g/>
,	,	kIx,
introduction	introduction	k1gInSc1
J.	J.	kA
Kemperman	Kemperman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leiden	Leidna	k1gFnPc2
<g/>
,	,	kIx,
The	The	k1gFnPc2
Netherlands	Netherlandsa	k1gFnPc2
<g/>
:	:	kIx,
KITLV	KITLV	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Verhandelingen	Verhandelingen	k1gInSc1
van	van	k1gInSc1
het	het	k?
Koninklijk	Koninklijk	k1gInSc1
Instituut	Instituut	k1gMnSc1
voor	voor	k1gMnSc1
Taal-	Taal-	k1gMnSc1
<g/>
,	,	kIx,
Land-	Land-	k1gMnSc1
en	en	k?
Volkenkunde	Volkenkund	k1gInSc5
206	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
90	#num#	k4
<g/>
-	-	kIx~
<g/>
6718	#num#	k4
<g/>
-	-	kIx~
<g/>
203	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
167	#num#	k4
170	#num#	k4
<g/>
–	–	k?
<g/>
173	#num#	k4
181	#num#	k4
<g/>
–	–	k?
<g/>
184	#num#	k4
196	#num#	k4
204	#num#	k4
<g/>
–	–	k?
<g/>
225	#num#	k4
309	#num#	k4
<g/>
–	–	k?
<g/>
314	#num#	k4
323	#num#	k4
<g/>
–	–	k?
<g/>
325	#num#	k4
337	#num#	k4
<g/>
–	–	k?
<g/>
338	#num#	k4
341	#num#	k4
343	#num#	k4
345	#num#	k4
<g/>
–	–	k?
<g/>
346	#num#	k4
380	#num#	k4
407	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
zde	zde	k6eAd1
použita	použít	k5eAaPmNgFnS
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Haruko	Haruko	k1gNnSc1
Taya	Taya	k1gMnSc1
Cook	Cook	k1gMnSc1
&	&	k?
Theodore	Theodor	k1gMnSc5
F.	F.	kA
Cook	Cook	k1gInSc1
<g/>
,	,	kIx,
Japan	japan	k1gInSc1
at	at	k?
War	War	k1gFnSc1
1993	#num#	k4
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
56584	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
39	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
153	#num#	k4
<g/>
↑	↑	k?
Jerome	Jerom	k1gInSc5
T.	T.	kA
Hagen	Hagen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
War	War	k1gFnSc1
in	in	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
<g/>
,	,	kIx,
Chapter	Chapter	k1gMnSc1
25	#num#	k4
"	"	kIx"
<g/>
The	The	k1gMnSc1
Lie	Lie	k1gMnSc1
of	of	k?
Marcus	Marcus	k1gMnSc1
McDilda	McDilda	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Hawaii	Hawaie	k1gFnSc3
Pacific	Pacifice	k1gFnPc2
University	universita	k1gFnSc2
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9653927	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Javier	Javier	k1gMnSc1
Guisández	Guisández	k1gMnSc1
Gómez	Gómez	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Law	Law	k1gMnSc1
of	of	k?
Air	Air	k1gMnSc5
Warfare	Warfar	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Review	Review	k1gMnSc1
of	of	k?
the	the	k?
Red	Red	k1gFnSc2
Cross	Crossa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
June	jun	k1gMnSc5
30	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
347	#num#	k4
<g/>
–	–	k?
<g/>
363	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Clayton	Clayton	k1gInSc1
Chun	Chun	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Doolittle	Doolittle	k1gFnSc1
Raid	raid	k1gInSc1
1942	#num#	k4
<g/>
:	:	kIx,
America	Americ	k1gInSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
first	first	k1gInSc1
strike	strik	k1gMnSc4
back	back	k6eAd1
at	at	k?
Japan	japan	k1gInSc1
(	(	kIx(
<g/>
Campaign	Campaign	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Osprey	Osprea	k1gFnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
January	Januar	k1gInPc1
31	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84176	#num#	k4
<g/>
-	-	kIx~
<g/>
918	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
85	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stewart	Stewart	k1gMnSc1
Halsey	Halsea	k1gFnSc2
Ross	Ross	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strategic	Strategice	k1gInPc2
Bombing	Bombing	k1gInSc1
by	by	kYmCp3nS
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
:	:	kIx,
The	The	k1gMnSc2
Myths	Myths	k1gInSc1
and	and	k?
the	the	k?
Facts	Facts	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Osprey	Osprea	k1gFnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
13	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7864	#num#	k4
<g/>
-	-	kIx~
<g/>
1412	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
59	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Francis	Francis	k1gInSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
471	#num#	k4
<g/>
–	–	k?
<g/>
472	#num#	k4
<g/>
↑	↑	k?
Tillman	Tillman	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
170	#num#	k4
<g/>
↑	↑	k?
Takai	Taka	k1gFnSc2
and	and	k?
Sakaida	Sakaida	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
114	#num#	k4
<g/>
↑	↑	k?
Tillman	Tillman	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pp	pp	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
171	#num#	k4
<g/>
–	–	k?
<g/>
172	#num#	k4
<g/>
↑	↑	k?
Tanaka	Tanak	k1gMnSc4
Hidden	Hiddno	k1gNnPc2
Horrors	Horrors	k1gInSc1
p	p	k?
<g/>
127	#num#	k4
<g/>
↑	↑	k?
Lord	lord	k1gMnSc1
Russell	Russell	k1gMnSc1
of	of	k?
Liverpool	Liverpool	k1gInSc1
(	(	kIx(
<g/>
Edward	Edward	k1gMnSc1
Russell	Russell	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
The	The	k1gMnSc1
Knights	Knightsa	k1gFnPc2
of	of	k?
Bushido	Bushida	k1gFnSc5
<g/>
,	,	kIx,
a	a	k8xC
short	short	k1gInSc1
history	histor	k1gInPc1
of	of	k?
Japanese	Japanese	k1gFnSc2
War	War	k1gMnSc1
Crimes	Crimes	k1gMnSc1
<g/>
,	,	kIx,
Greenhill	Greenhill	k1gMnSc1
books	booksa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
p.	p.	k?
<g/>
236	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lord	lord	k1gMnSc1
Russell	Russell	k1gMnSc1
of	of	k?
Liverpool	Liverpool	k1gInSc1
(	(	kIx(
<g/>
Edward	Edward	k1gMnSc1
Russell	Russell	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
The	The	k1gMnSc1
Knights	Knightsa	k1gFnPc2
of	of	k?
Bushido	Bushida	k1gFnSc5
<g/>
,	,	kIx,
a	a	k8xC
short	short	k1gInSc1
history	histor	k1gInPc1
of	of	k?
Japanese	Japanese	k1gFnSc2
War	War	k1gMnSc1
Crimes	Crimes	k1gMnSc1
<g/>
,	,	kIx,
Greenhill	Greenhill	k1gMnSc1
books	booksa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
<g/>
121	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archivováno	archivován	k2eAgNnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
8	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
''	''	k?
<g/>
Case	Cas	k1gInPc4
No	no	k9
<g/>
.	.	kIx.
21	#num#	k4
Trial	trial	k1gInSc1
Of	Of	k1gFnSc2
General	General	k1gFnSc2
Tomoyuki	Tomoyuk	k1gFnSc2
Yamashita	Yamashita	k1gFnSc1
<g/>
[	[	kIx(
<g/>
,	,	kIx,
<g/>
]	]	kIx)
United	United	k1gInSc1
States	Statesa	k1gFnPc2
Military	Militara	k1gFnSc2
Commission	Commission	k1gInSc1
<g/>
,	,	kIx,
Manila	Manila	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
8	#num#	k4
October-	October-	k1gFnPc2
<g/>
7	#num#	k4
December	Decembra	k1gFnPc2
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
and	and	k?
the	the	k?
Supreme	Suprem	k1gInSc5
Court	Courta	k1gFnPc2
Of	Of	k1gMnSc1
The	The	k1gMnSc1
United	United	k1gMnSc1
States	States	k1gMnSc1
(	(	kIx(
<g/>
Judgments	Judgments	k1gInSc1
Delivered	Delivered	k1gInSc1
On	on	k3xPp3gMnSc1
4	#num#	k4
February	Februara	k1gFnSc2
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Part	part	k1gInSc4
VI	VI	kA
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.ess.uwe.ac.uk	www.ess.uwe.ac.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Retrieved	Retrieved	k1gInSc4
on	on	k3xPp3gMnSc1
December	December	k1gMnSc1
18	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Jeanie	Jeanie	k1gFnSc2
M.	M.	kA
Welch	Welch	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Without	Without	k1gMnSc1
a	a	k8xC
Hangman	Hangman	k1gMnSc1
<g/>
,	,	kIx,
Without	Without	k1gMnSc1
a	a	k8xC
Rope	Rope	k1gFnSc1
<g/>
:	:	kIx,
Navy	Navy	k?
War	War	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Crimes	Crimesa	k1gFnPc2
Trials	Trialsa	k1gFnPc2
After	After	k1gInSc1
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
"	"	kIx"
<g/>
,	,	kIx,
International	International	k1gMnSc1
Journal	Journal	k1gFnSc2
of	of	k?
Naval	navalit	k5eAaPmRp2nS
History	Histor	k1gMnPc4
<g/>
,	,	kIx,
v	v	k7c4
<g/>
.1	.1	k4
<g/>
,	,	kIx,
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
April	April	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
5	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
↑	↑	k?
Zhifen	Zhifna	k1gFnPc2
Ju	ju	k0
<g/>
,	,	kIx,
Japan	japan	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
atrocities	atrocities	k1gMnSc1
of	of	k?
conscripting	conscripting	k1gInSc1
and	and	k?
abusing	abusing	k1gInSc1
north	north	k1gInSc1
China	China	k1gFnSc1
draftees	draftees	k1gMnSc1
after	after	k1gMnSc1
the	the	k?
outbreak	outbreak	k1gMnSc1
of	of	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
War	War	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
↑	↑	k?
links	linksa	k1gFnPc2
for	forum	k1gNnPc2
research	research	k1gMnSc1
<g/>
,	,	kIx,
Allied	Allied	k1gMnSc1
POWs	POWsa	k1gFnPc2
under	under	k1gMnSc1
the	the	k?
Japanese	Japanese	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mansell	Mansell	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Library	Librara	k1gFnSc2
of	of	k?
Congress	Congress	k1gInSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
‘	‘	k?
<g/>
Indonesia	Indonesia	k1gFnSc1
<g/>
:	:	kIx,
World	World	k1gInSc1
War	War	k1gMnSc2
II	II	kA
and	and	k?
<g />
.	.	kIx.
</s>
<s hack="1">
the	the	k?
Struggle	Struggle	k1gNnSc4
For	forum	k1gNnPc2
Independence	Independence	k1gFnSc2
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
<g/>
;	;	kIx,
The	The	k1gFnSc2
Japanese	Japanese	k1gFnSc2
Occupation	Occupation	k1gInSc1
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
<g/>
’	’	k?
Access	Access	k1gInSc1
date	date	k1gInSc1
<g/>
:	:	kIx,
February	Februara	k1gFnSc2
9	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Fujiwara	Fujiwara	k1gFnSc1
<g/>
,	,	kIx,
Nitchū	Nitchū	k1gFnSc1
sensō	sensō	k?
ni	on	k3xPp3gFnSc4
okeru	okera	k1gFnSc4
horyo	horyo	k1gMnSc1
gyakusatsu	gyakusats	k1gInSc2
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
↑	↑	k?
Yoshiaki	Yoshiak	k1gFnSc3
Yoshimi	Yoshi	k1gFnPc7
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Comfort	Comfort	k1gInSc1
Women	Women	k2eAgInSc1d1
<g/>
:	:	kIx,
Sexual	Sexual	k1gInSc1
Slavery	Slavera	k1gFnSc2
in	in	k?
the	the	k?
Japanese	Japanese	k1gFnSc2
Military	Militara	k1gFnSc2
during	during	k1gInSc1
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Columbia	Columbia	k1gFnSc1
University	universita	k1gFnSc2
Press	Press	k1gInSc1
<g/>
↑	↑	k?
TABUCHI	TABUCHI	kA
<g/>
,	,	kIx,
Hiroko	Hiroko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
Post	post	k1gInSc4
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Japan	japan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Abe	Abe	k1gFnSc7
<g/>
:	:	kIx,
no	no	k9
proof	proof	k1gInSc1
of	of	k?
WWII	WWII	kA
sex	sex	k1gInSc4
slaves	slavesa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
www.washingtonpost.com	www.washingtonpost.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washingtonpost	Washingtonpost	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
No	no	k9
comfort	comfort	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
March	March	k1gInSc1
6	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
http://www.nytimes.com/2007/03/06/opinion/06tues3.html,	http://www.nytimes.com/2007/03/06/opinion/06tues3.html,	k4
accessed	accessed	k1gMnSc1
March	March	k1gMnSc1
8	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
↑	↑	k?
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
<g/>
,	,	kIx,
Ibid	Ibid	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Evidence	evidence	k1gFnSc1
documenting	documenting	k1gInSc1
sex-slave	sex-slav	k1gMnSc5
coercion	coercion	k1gInSc1
revealed	revealed	k1gMnSc1
<g/>
.	.	kIx.
search	search	k1gMnSc1
<g/>
.	.	kIx.
<g/>
japantimes	japantimes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
jp	jp	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
19	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Files	Files	k1gMnSc1
<g/>
:	:	kIx,
Females	Females	k1gMnSc1
forced	forced	k1gMnSc1
into	into	k1gMnSc1
sexual	sexuat	k5eAaBmAgMnS,k5eAaImAgMnS,k5eAaPmAgMnS
servitude	servitude	k6eAd1
in	in	k?
wartime	wartimat	k5eAaPmIp3nS
Indonesia	Indonesia	k1gFnSc1
http://search.japantimes.co.jp/cgi-bin/nn20070512a6.html	http://search.japantimes.co.jp/cgi-bin/nn20070512a6.html	k1gInSc1
<g/>
↑	↑	k?
East	East	k2eAgInSc1d1
Timor	Timor	k1gInSc1
former	formra	k1gFnPc2
sex	sex	k1gInSc1
slaves	slaves	k1gMnSc1
speak	speak	k1gMnSc1
out	out	k?
http://search.japantimes.co.jp/cgi-bin/nn20070428f1.html	http://search.japantimes.co.jp/cgi-bin/nn20070428f1.html	k1gMnSc1
<g/>
↑	↑	k?
Todd	Todd	k1gMnSc1
Cardy	Carda	k1gMnSc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Japanese	Japanese	k1gFnSc1
PM	PM	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
denial	denial	k1gInSc1
upsets	upsets	k1gInSc1
'	'	kIx"
<g/>
comfort	comfort	k1gInSc1
woman	woman	k1gInSc1
<g/>
'	'	kIx"
<g/>
"	"	kIx"
(	(	kIx(
<g/>
News	News	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
<g/>
;	;	kIx,
March	March	k1gInSc1
5	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Access	Access	k1gInSc1
date	date	k1gInSc4
<g/>
:	:	kIx,
March	March	k1gInSc1
7	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
China	China	k1gFnSc1
Daily	Daila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Memoir	Memoir	k1gInSc1
of	of	k?
comfort	comfort	k1gInSc1
woman	woman	k1gMnSc1
tells	tellsa	k1gFnPc2
of	of	k?
'	'	kIx"
<g/>
hell	hella	k1gFnPc2
for	forum	k1gNnPc2
women	women	k1gInSc1
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chinadaily	Chinadaila	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
cn	cn	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Yoshimi	Yoshi	k1gFnPc7
<g/>
,	,	kIx,
ibid	ibid	k6eAd1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
http://hnn.us/articles/printfriendly/9954.html	http://hnn.us/articles/printfriendly/9954.html	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
31	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
http://www.jpri.org/publications/workingpapers/wp77.html	http://www.jpri.org/publications/workingpapers/wp77.htmlit	k5eAaPmRp2nS
and	and	k?
http://hnn.us/articles/13533.html	http://hnn.us/articles/13533.htmnout	k5eAaPmAgInS
http://www.zmag.org/content/showarticle.cfm?ItemID=10155	http://www.zmag.org/content/showarticle.cfm?ItemID=10155	k4
Archivováno	archivován	k2eAgNnSc4d1
30	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
&	&	k?
http://www.boston.com/news/nation/articles/2006/10/15/congress_backs_off_of_wartime_japan_rebuke/	http://www.boston.com/news/nation/articles/2006/10/15/congress_backs_off_of_wartime_japan_rebuke/	k4
<g/>
↑	↑	k?
MOYNIHAN	MOYNIHAN	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abe	Abe	k1gMnSc1
ignores	ignores	k1gMnSc1
evidence	evidence	k1gFnSc1
<g/>
,	,	kIx,
say	say	k?
Australia	Australia	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
comfort	comfort	k1gInSc1
women	women	k1gInSc4
<g/>
.	.	kIx.
www.theage.com.au	www.theage.com.au	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Melbourne	Melbourne	k1gNnSc1
<g/>
:	:	kIx,
Theage	Theage	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Kimura	Kimura	k1gFnSc1
<g/>
,	,	kIx,
Kayoko	Kayoko	k1gNnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Stance	stance	k1gFnSc1
on	on	k3xPp3gMnSc1
‘	‘	k?
<g/>
comfort	comfort	k1gInSc1
women	women	k1gInSc1
<g/>
’	’	k?
undermines	undermines	k1gInSc1
fight	fight	k2eAgMnSc1d1
to	ten	k3xDgNnSc1
end	end	k?
wartime	wartimat	k5eAaPmIp3nS
sexual	sexual	k1gInSc1
violence	violence	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Japan	japan	k1gInSc4
Times	Timesa	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
March	March	k1gInSc1
2014	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
8	#num#	k4
<g/>
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
Panel	panel	k1gInSc4
OKs	OKs	k1gFnSc2
sex	sex	k1gInSc4
slave	slavat	k5eAaPmIp3nS
resolution	resolution	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
http://search.japantimes.co.jp/cgi-bin/nn20070628a1.html	http://search.japantimes.co.jp/cgi-bin/nn20070628a1.htmnout	k5eAaPmAgInS
<g/>
↑	↑	k?
Kenneth	Kenneth	k1gInSc1
B.	B.	kA
Lee	Lea	k1gFnSc6
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
Korea	Korea	k1gFnSc1
and	and	k?
East	East	k1gMnSc1
Asia	Asia	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Story	story	k1gFnSc2
of	of	k?
a	a	k8xC
Phoenix	Phoenix	k1gInSc1
<g/>
,	,	kIx,
Westport	Westport	k1gInSc1
<g/>
,	,	kIx,
CT	CT	kA
<g/>
:	:	kIx,
Greenwood	Greenwooda	k1gFnPc2
Publishing	Publishing	k1gInSc1
Group	Group	k1gMnSc1
<g/>
↑	↑	k?
Sterling	sterling	k1gInSc1
&	&	k?
Peggy	Pegga	k1gFnSc2
Seagrave	Seagrav	k1gInSc5
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Gold	Gold	k1gInSc1
warriors	warriors	k1gInSc1
<g/>
:	:	kIx,
America	Americ	k2eAgFnSc1d1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
secret	secret	k1gInSc1
recovery	recovera	k1gFnSc2
of	of	k?
Yamashita	Yamashita	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
gold	gold	k1gInSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
:	:	kIx,
Verso	Versa	k1gFnSc5
Books	Books	k1gInSc1
(	(	kIx(
<g/>
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
85984	#num#	k4
<g/>
-	-	kIx~
<g/>
542	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Jednotka	jednotka	k1gFnSc1
731	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
<g/>
}	}	kIx)
Mezinárodní	mezinárodní	k2eAgNnSc4d1
humanitární	humanitární	k2eAgNnSc4d1
právo	právo	k1gNnSc4
Prameny	pramen	k1gInPc1
mezinárodního	mezinárodní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
</s>
<s>
Haagské	haagský	k2eAgFnSc2d1
úmluvy	úmluva	k1gFnSc2
•	•	k?
Ženevské	ženevský	k2eAgFnSc2d1
úmluvy	úmluva	k1gFnSc2
•	•	k?
Briandův	Briandův	k2eAgInSc1d1
<g/>
–	–	k?
<g/>
Kelloggův	Kelloggův	k2eAgInSc1d1
pakt	pakt	k1gInSc1
•	•	k?
Londýnská	londýnský	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c4
stíhání	stíhání	k1gNnSc4
a	a	k8xC
potrestání	potrestání	k1gNnSc4
hlavních	hlavní	k2eAgMnPc2d1
válečných	válečný	k2eAgMnPc2d1
zločinců	zločinec	k1gMnPc2
Osy	osa	k1gFnSc2
•	•	k?
Charta	charta	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
•	•	k?
Úmluva	úmluva	k1gFnSc1
o	o	k7c6
zabránění	zabránění	k1gNnSc6
a	a	k8xC
trestání	trestání	k1gNnSc6
zločinu	zločin	k1gInSc2
genocidia	genocidium	k1gNnSc2
•	•	k?
Římský	římský	k2eAgInSc1d1
statut	statut	k1gInSc1
•	•	k?
Úmluva	úmluva	k1gFnSc1
proti	proti	k7c3
mučení	mučení	k1gNnSc3
a	a	k8xC
jinému	jiné	k1gNnSc3
krutému	krutý	k2eAgNnSc3d1
<g/>
,	,	kIx,
nelidskému	lidský	k2eNgNnSc3d1
či	či	k8xC
ponižujícímu	ponižující	k2eAgNnSc3d1
zacházení	zacházení	k1gNnSc3
nebo	nebo	k8xC
trestání	trestání	k1gNnSc3
Zločiny	zločin	k1gInPc1
podle	podle	k7c2
mezinárodního	mezinárodní	k2eAgNnSc2d1
trestního	trestní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
</s>
<s>
Zločin	zločin	k1gInSc1
proti	proti	k7c3
míru	mír	k1gInSc3
•	•	k?
Zločin	zločin	k1gInSc1
agrese	agrese	k1gFnSc2
•	•	k?
Válečný	válečný	k2eAgInSc1d1
zločin	zločin	k1gInSc1
•	•	k?
Zločin	zločin	k1gInSc4
proti	proti	k7c3
lidskosti	lidskost	k1gFnSc3
•	•	k?
Genocida	genocida	k1gFnSc1
•	•	k?
Apartheid	apartheid	k1gInSc1
•	•	k?
Otrokářství	otrokářství	k1gNnSc2
•	•	k?
Pirátství	pirátství	k1gNnSc2
•	•	k?
Mučení	mučení	k1gNnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trestní	trestní	k2eAgFnSc2d1
soudy	soud	k1gInPc7
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
tribunál	tribunál	k1gInSc1
v	v	k7c6
Norimberku	Norimberk	k1gInSc6
•	•	k?
Mezinárodní	mezinárodní	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
tribunál	tribunál	k1gInSc1
pro	pro	k7c4
Dálný	dálný	k2eAgInSc4d1
východ	východ	k1gInSc4
•	•	k?
Mezinárodní	mezinárodní	k2eAgInSc1d1
trestní	trestní	k2eAgInSc1d1
tribunál	tribunál	k1gInSc1
pro	pro	k7c4
bývalou	bývalý	k2eAgFnSc4d1
Jugoslávii	Jugoslávie	k1gFnSc4
•	•	k?
Mezinárodní	mezinárodní	k2eAgInSc1d1
trestní	trestní	k2eAgInSc1d1
tribunál	tribunál	k1gInSc1
pro	pro	k7c4
Rwandu	Rwanda	k1gFnSc4
•	•	k?
Mezinárodní	mezinárodní	k2eAgInSc1d1
trestní	trestní	k2eAgInSc1d1
soud	soud	k1gInSc1
Pojmy	pojem	k1gInPc1
válečného	válečný	k2eAgNnSc2d1
práva	právo	k1gNnSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1
trestní	trestní	k2eAgNnSc1d1
právo	právo	k1gNnSc1
•	•	k?
Ius	Ius	k1gFnPc2
in	in	k?
bello	bello	k1gNnSc4
•	•	k?
Ius	Ius	k1gFnPc2
ad	ad	k7c4
bellum	bellum	k1gNnSc4
•	•	k?
Civilista	civilista	k1gMnSc1
•	•	k?
Kombatant	Kombatant	k?
•	•	k?
Velitelská	velitelský	k2eAgFnSc1d1
odpovědnost	odpovědnost	k1gFnSc1
•	•	k?
Nadřízené	nadřízený	k2eAgInPc4d1
rozkazy	rozkaz	k1gInPc4
•	•	k?
Vyhlášení	vyhlášení	k1gNnSc1
války	válka	k1gFnSc2
•	•	k?
Válečný	válečný	k2eAgInSc4d1
stav	stav	k1gInSc4
</s>
