<s>
K	k	k7c3
japonským	japonský	k2eAgMnPc3d1
válečným	válečný	k2eAgMnPc3d1
zločinům	zločin	k1gMnPc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
mnoha	mnoho	k4c6
asijských	asijský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
během	během	k7c2
období	období	k1gNnSc2
japonského	japonský	k2eAgInSc2d1
militarismu	militarismus	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
za	za	k7c2
druhé	druhý	k4xOgFnSc2
čínsko-japonské	čínsko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Japonské	japonský	k2eAgFnPc1d1
císařské	císařský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
široce	široko	k6eAd1
využívaly	využívat	k5eAaImAgFnP,k5eAaPmAgFnP
mučení	mučení	k1gNnPc1
na	na	k7c4
vězních	vězeň	k1gMnPc6
<g/>
.	.	kIx.
</s>