<s>
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1908	#num#	k4
se	se	k3xPyFc4
královský	královský	k2eAgInSc1d1
pár	pár	k1gInSc1
s	s	k7c7
oběma	dva	k4xCgMnPc7
syny	syn	k1gMnPc7
vracel	vracet	k5eAaImAgInS
z	z	k7c2
vánočního	vánoční	k2eAgInSc2d1
oddechu	oddech	k1gInSc2
na	na	k7c6
venkovském	venkovský	k2eAgNnSc6d1
sídle	sídlo	k1gNnSc6
Vila	vila	k1gFnSc1
Viçosa	Viçosa	k1gFnSc1
do	do	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Amélie	Amélie	k1gFnSc1
Orleánská	orleánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
Maria	Mario	k1gMnSc2
Amélia	Amélius	k1gMnSc2
de	de	k?
Orleã	Orleã	k1gMnSc1
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1865	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1951	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc1d1
portugalská	portugalský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
Karla	Karel	k1gMnSc2
I.	I.	kA
Portugalského	portugalský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Karel	Karel	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
starší	starý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
byl	být	k5eAaImAgMnS
smrtelně	smrtelně	k6eAd1
raněn	ranit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>