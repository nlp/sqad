<s desamb="1">
Mladší	mladý	k2eAgMnPc1d2
syn	syn	k1gMnSc1
Manuel	Manuel	k1gMnSc1
byl	být	k5eAaImAgMnS
zraněn	zranit	k5eAaPmNgMnS
kulkou	kulka	k1gFnSc7
do	do	k7c2
ramene	rameno	k1gNnSc2
<g/>
,	,	kIx,
královna	královna	k1gFnSc1
(	(	kIx(
<g/>
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
mladšího	mladý	k2eAgMnSc4d2
syna	syn	k1gMnSc4
snažila	snažit	k5eAaImAgFnS
chránit	chránit	k5eAaImF
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
jako	jako	k9
zázrakem	zázrak	k1gInSc7
nezraněna	zranit	k5eNaPmNgFnS
<g/>
.	.	kIx.
</s>