<s>
Amélie	Amélie	k1gFnSc1
Orleánská	orleánský	k2eAgFnSc1d1
</s>
<s>
Amélie	Amélie	k1gFnSc1
Orleánskáportugalská	Orleánskáportugalský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
Portrét	portrét	k1gInSc4
zřejmě	zřejmě	k6eAd1
z	z	k7c2
let	léto	k1gNnPc2
1886	#num#	k4
<g/>
-	-	kIx~
<g/>
1888	#num#	k4
<g/>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
1889	#num#	k4
-	-	kIx~
1908	#num#	k4
Manžel	manžel	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
Portugalský	portugalský	k2eAgMnSc1d1
Narození	narození	k1gNnSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1865	#num#	k4
Twickenham	Twickenham	k1gInSc1
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1951	#num#	k4
<g/>
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
86	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Le	Le	k1gFnSc1
Chesnay	Chesnaa	k1gFnSc2
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Pia	Pius	k1gMnSc2
Savojská	savojský	k2eAgFnSc5d1
Následník	následník	k1gMnSc1
</s>
<s>
-	-	kIx~
Dynastie	dynastie	k1gFnPc5
</s>
<s>
Bourbon-Orléans	Bourbon-Orléans	k6eAd1
Otec	otec	k1gMnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
Pařížský	pařížský	k2eAgMnSc1d1
Matka	matka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Isabela	Isabela	k1gFnSc1
Orleánská	orleánský	k2eAgFnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Amélie	Amélie	k1gFnSc1
Orleánská	orleánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
Maria	Mario	k1gMnSc2
Amélia	Amélius	k1gMnSc2
de	de	k?
Orleã	Orleã	k1gMnSc1
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1865	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1951	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc1d1
portugalská	portugalský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
Karla	Karel	k1gMnSc2
I.	I.	kA
Portugalského	portugalský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Portugalská	portugalský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
</s>
<s>
Obraz	obraz	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1905	#num#	k4
<g/>
,	,	kIx,
Vittorio	Vittorio	k6eAd1
Matteo	Matteo	k1gMnSc1
Corcos	Corcos	k1gMnSc1
</s>
<s>
Amélie	Amélie	k1gFnSc1
byla	být	k5eAaImAgFnS
nejstarší	starý	k2eAgFnSc7d3
dcerou	dcera	k1gFnSc7
hraběte	hrabě	k1gMnSc2
z	z	k7c2
Paříže	Paříž	k1gFnSc2
Filipa	Filip	k1gMnSc2
Orleánského	orleánský	k2eAgMnSc2d1
a	a	k8xC
Marie	Maria	k1gFnPc4
Isabely	Isabela	k1gFnSc2
Orleánské	orleánský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
se	se	k3xPyFc4
vdala	vdát	k5eAaPmAgFnS
za	za	k7c2
portugalského	portugalský	k2eAgMnSc2d1
prince	princ	k1gMnSc2
Karla	Karel	k1gMnSc2
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Braganza-Sasko-Koburg-Gotha	Braganza-Sasko-Koburg-Goth	k1gMnSc2
a	a	k8xC
spolu	spolu	k6eAd1
pak	pak	k6eAd1
měli	mít	k5eAaImAgMnP
tyto	tento	k3xDgFnPc4
děti	dítě	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
Portugalský	portugalský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1887	#num#	k4
<g/>
–	–	k?
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Infantka	infantka	k1gFnSc1
Maria	Maria	k1gFnSc1
Anna	Anna	k1gFnSc1
Portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
†	†	k?
<g/>
1887	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Manuel	Manuel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
král	král	k1gMnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1908	#num#	k4
až	až	k9
1910	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
králem	král	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1889	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
klesala	klesat	k5eAaImAgFnS
podpora	podpora	k1gFnSc1
portugalské	portugalský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
vzhledem	vzhledem	k7c3
ke	k	k7c3
špatnému	špatný	k2eAgInSc3d1
stavu	stav	k1gInSc3
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amélie	Amélie	k1gFnSc1
jako	jako	k8xC,k8xS
královna	královna	k1gFnSc1
zastávala	zastávat	k5eAaImAgFnS
aktivní	aktivní	k2eAgFnSc4d1
roli	role	k1gFnSc4
a	a	k8xC
poněkud	poněkud	k6eAd1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
dařilo	dařit	k5eAaImAgNnS
zmírnit	zmírnit	k5eAaPmF
rostoucí	rostoucí	k2eAgFnSc4d1
kritiku	kritika	k1gFnSc4
monarchie	monarchie	k1gFnSc2
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
osobní	osobní	k2eAgFnSc3d1
popularitě	popularita	k1gFnSc3
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
sama	sám	k3xTgFnSc1
někdy	někdy	k6eAd1
sklízela	sklízet	k5eAaImAgFnS
kritiku	kritika	k1gFnSc4
na	na	k7c4
vlastní	vlastní	k2eAgInSc4d1
účet	účet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Aktivně	aktivně	k6eAd1
se	se	k3xPyFc4
účastnila	účastnit	k5eAaImAgFnS
mnoha	mnoho	k4c2
sociálních	sociální	k2eAgInPc2d1
a	a	k8xC
charitativních	charitativní	k2eAgInPc2d1
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
například	například	k6eAd1
týkaly	týkat	k5eAaImAgInP
prevence	prevence	k1gFnPc4
a	a	k8xC
léčby	léčba	k1gFnSc2
tuberkulózy	tuberkulóza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
pokládána	pokládat	k5eAaImNgFnS
za	za	k7c4
méně	málo	k6eAd2
formální	formální	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
byla	být	k5eAaImAgFnS
její	její	k3xOp3gFnSc1
tchyně	tchyně	k1gFnSc1
Marie	Maria	k1gFnSc2
Pia	Pius	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
popisována	popisován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
vlídná	vlídný	k2eAgFnSc1d1
a	a	k8xC
klidná	klidný	k2eAgFnSc1d1
žena	žena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naučila	naučit	k5eAaPmAgNnP
se	se	k3xPyFc4
dobře	dobře	k6eAd1
portugalsky	portugalsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímala	zajímat	k5eAaImAgFnS
se	se	k3xPyFc4
o	o	k7c4
literaturu	literatura	k1gFnSc4
<g/>
,	,	kIx,
operu	opera	k1gFnSc4
a	a	k8xC
divadlo	divadlo	k1gNnSc1
a	a	k8xC
sama	sám	k3xTgFnSc1
malovala	malovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
nepřítomnosti	nepřítomnost	k1gFnSc2
svého	svůj	k3xOyFgMnSc2
manžela	manžel	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1895	#num#	k4
sloužila	sloužit	k5eAaImAgFnS
coby	coby	k?
regentka	regentka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
podnikla	podniknout	k5eAaPmAgFnS
plavbu	plavba	k1gFnSc4
po	po	k7c6
Středomoří	středomoří	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
sklidila	sklidit	k5eAaPmAgFnS
kritiku	kritika	k1gFnSc4
jako	jako	k8xS,k8xC
příliš	příliš	k6eAd1
nákladná	nákladný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Atentát	atentát	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1908	#num#	k4
se	se	k3xPyFc4
královský	královský	k2eAgInSc1d1
pár	pár	k1gInSc1
s	s	k7c7
oběma	dva	k4xCgMnPc7
syny	syn	k1gMnPc7
vracel	vracet	k5eAaImAgInS
z	z	k7c2
vánočního	vánoční	k2eAgInSc2d1
oddechu	oddech	k1gInSc2
na	na	k7c6
venkovském	venkovský	k2eAgNnSc6d1
sídle	sídlo	k1gNnSc6
Vila	vila	k1gFnSc1
Viçosa	Viçosa	k1gFnSc1
do	do	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Palácovém	palácový	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
jejich	jejich	k3xOp3gInSc4
vůz	vůz	k1gInSc4
automatickými	automatický	k2eAgInPc7d1
zbraněmi	zbraň	k1gFnPc7
dva	dva	k4xCgMnPc1
revoluční	revoluční	k2eAgMnPc1d1
republikáni	republikán	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Karel	Karel	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
starší	starý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
byl	být	k5eAaImAgMnS
smrtelně	smrtelně	k6eAd1
raněn	ranit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladší	mladý	k2eAgMnPc1d2
syn	syn	k1gMnSc1
Manuel	Manuel	k1gMnSc1
byl	být	k5eAaImAgMnS
zraněn	zranit	k5eAaPmNgMnS
kulkou	kulka	k1gFnSc7
do	do	k7c2
ramene	rameno	k1gNnSc2
<g/>
,	,	kIx,
královna	královna	k1gFnSc1
(	(	kIx(
<g/>
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
mladšího	mladý	k2eAgMnSc4d2
syna	syn	k1gMnSc4
snažila	snažit	k5eAaImAgFnS
chránit	chránit	k5eAaImF
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
jako	jako	k9
zázrakem	zázrak	k1gInSc7
nezraněna	zranit	k5eNaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
atentátníky	atentátník	k1gMnPc7
na	na	k7c6
místě	místo	k1gNnSc6
zastřelila	zastřelit	k5eAaPmAgFnS
královská	královský	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
exilu	exil	k1gInSc6
</s>
<s>
Hned	hned	k6eAd1
druhého	druhý	k4xOgMnSc4
dnes	dnes	k6eAd1
se	se	k3xPyFc4
tak	tak	k9
princ	princ	k1gMnSc1
Manuel	Manuel	k1gMnSc1
v	v	k7c6
18	#num#	k4
letech	léto	k1gNnPc6
stal	stát	k5eAaPmAgMnS
králem	král	k1gMnSc7
Portugalska	Portugalsko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
počátkem	počátkem	k7c2
října	říjen	k1gInSc2
1910	#num#	k4
byl	být	k5eAaImAgInS
revolučním	revoluční	k2eAgInSc7d1
převratem	převrat	k1gInSc7
svržen	svrhnout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amélie	Amélie	k1gFnSc1
spolu	spolu	k6eAd1
se	s	k7c7
zbytkem	zbytek	k1gInSc7
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
opustila	opustit	k5eAaPmAgFnS
Portugalsko	Portugalsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
zbytku	zbytek	k1gInSc2
života	život	k1gInSc2
strávila	strávit	k5eAaPmAgFnS
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
II	II	kA
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
ji	on	k3xPp3gFnSc4
portugalská	portugalský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
pozvala	pozvat	k5eAaPmAgFnS
do	do	k7c2
země	zem	k1gFnSc2
zpět	zpět	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
Amélie	Amélie	k1gFnSc1
odmítla	odmítnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naposledy	naposledy	k6eAd1
Portugalsko	Portugalsko	k1gNnSc1
navštívila	navštívit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
867	#num#	k4
<g/>
.	.	kIx.
dáma	dáma	k1gFnSc1
Řádu	řád	k1gInSc2
královny	královna	k1gFnSc2
Marie	Maria	k1gFnSc2
Luisy	Luisa	k1gFnSc2
–	–	k?
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
1886	#num#	k4
</s>
<s>
Stuha	stuha	k1gFnSc1
tří	tři	k4xCgFnPc2
řádu	řád	k1gInSc2
–	–	k?
Portugalské	portugalský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1909	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
dáma	dáma	k1gFnSc1
Řádu	řád	k1gInSc2
svaté	svatý	k2eAgFnSc2d1
Isabely	Isabela	k1gFnSc2
–	–	k?
Portugalské	portugalský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řád	řád	k1gInSc1
neposkvrněného	poskvrněný	k2eNgNnSc2d1
početí	početí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
Vila	vila	k1gFnSc1
Viçosa	Viçosa	k1gFnSc1
</s>
<s>
dáma	dáma	k1gFnSc1
Řádu	řád	k1gInSc2
hvězdového	hvězdový	k2eAgInSc2d1
kříže	kříž	k1gInSc2
–	–	k?
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
</s>
<s>
dáma	dáma	k1gFnSc1
Řádu	řád	k1gInSc2
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
–	–	k?
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
růže	růže	k1gFnSc1
–	–	k?
Vatikán	Vatikán	k1gInSc1
<g/>
,	,	kIx,
1892	#num#	k4
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orleánský	orleánský	k2eAgInSc1d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Marie	Marie	k1gFnSc1
Adelaida	Adelaida	k1gFnSc1
Bourbonská	bourbonský	k2eAgFnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Filip	Filip	k1gMnSc1
Orleánský	orleánský	k2eAgMnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Amálie	Amálie	k1gFnSc2
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
<g/>
]	]	kIx)
</s>
<s>
Filip	Filip	k1gMnSc1
Orleánský	orleánský	k2eAgMnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
František	František	k1gMnSc1
I.	I.	kA
Meklenbursko-Schwerinský	Meklenbursko-Schwerinský	k2eAgMnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Meklenbursko-Střelický	Meklenbursko-Střelický	k2eAgMnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Sasko-Gothajsko-Altenburská	Sasko-Gothajsko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Helena	Helena	k1gFnSc1
Meklenbursko-Schwerinská	Meklenbursko-Schwerinský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
August	August	k1gMnSc1
Sasko-Výmarsko-Eisenašský	Sasko-Výmarsko-Eisenašský	k2eAgMnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Sasko-Výmarsko-Eisenašská	Sasko-Výmarsko-Eisenašský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Amélie	Amélie	k1gFnSc1
Orleánská	orleánský	k2eAgFnSc1d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orleánský	orleánský	k2eAgInSc1d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Filip	Filip	k1gMnSc1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Marie	Marie	k1gFnSc1
Adelaida	Adelaida	k1gFnSc1
Bourbonská	bourbonský	k2eAgFnSc1d1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Francouzský	francouzský	k2eAgMnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Amálie	Amálie	k1gFnSc2
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
<g/>
]	]	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Isabela	Isabela	k1gFnSc1
Orleánská	orleánský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Marie	Marie	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Parmská	parmský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Fernanda	Fernando	k1gNnSc2
Španělská	španělský	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Isabela	Isabela	k1gFnSc1
Španělská	španělský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Amélie	Amélie	k1gFnSc1
of	of	k?
Orléans	Orléansa	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Filial	Filial	k1gInSc1
Hommage	Hommage	k1gFnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Express	express	k1gInSc1
<g/>
”	”	k?
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1909	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
2	#num#	k4
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
Archivováno	archivován	k2eAgNnSc1d1
30	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Justus	Justus	k1gMnSc1
Perthes	Perthes	k1gMnSc1
<g/>
,	,	kIx,
Almanach	almanach	k1gInSc1
de	de	k?
Gotha	Gotha	k1gFnSc1
1921	#num#	k4
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlin	k1gInSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Amélie	Amélie	k1gFnSc2
Orléanská	Orléanský	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Amélie	Amélie	k1gFnSc1
Orléanská	Orléanský	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Portugalská	portugalský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Pia	Pius	k1gMnSc2
Savojská	savojský	k2eAgNnPc5d1
</s>
<s>
1889	#num#	k4
-	-	kIx~
1908	#num#	k4
Amélie	Amélie	k1gFnSc2
Orleánská	orleánský	k2eAgNnPc4d1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
-	-	kIx~
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
119553155	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1476	#num#	k4
9206	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
92113172	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
87231352	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
92113172	#num#	k4
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Portugalsko	Portugalsko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Monarchie	monarchie	k1gFnSc1
</s>
