<p>
<s>
Klávesnice	klávesnice	k1gFnSc1	klávesnice
je	být	k5eAaImIp3nS	být
vstupní	vstupní	k2eAgNnSc1d1	vstupní
zařízení	zařízení	k1gNnSc1	zařízení
sestávající	sestávající	k2eAgNnSc1d1	sestávající
z	z	k7c2	z
kláves	klávesa	k1gFnPc2	klávesa
(	(	kIx(	(
<g/>
tlačítek	tlačítko	k1gNnPc2	tlačítko
<g/>
,	,	kIx,	,
klapek	klapka	k1gFnPc2	klapka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
zpravidla	zpravidla	k6eAd1	zpravidla
mačkaných	mačkaný	k2eAgInPc2d1	mačkaný
prsty	prst	k1gInPc7	prst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
klávesnice	klávesnice	k1gFnSc1	klávesnice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
na	na	k7c6	na
klávesách	klávesa	k1gFnPc6	klávesa
F	F	kA	F
<g/>
,	,	kIx,	,
J	J	kA	J
a	a	k8xC	a
5	[number]	k4	5
pomocné	pomocný	k2eAgFnPc1d1	pomocná
rysky	ryska	k1gFnPc1	ryska
(	(	kIx(	(
<g/>
vystupující	vystupující	k2eAgFnPc1d1	vystupující
plošky	ploška	k1gFnPc1	ploška
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
rychlejší	rychlý	k2eAgFnSc4d2	rychlejší
orientaci	orientace	k1gFnSc4	orientace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
klávesnic	klávesnice	k1gFnPc2	klávesnice
==	==	k?	==
</s>
</p>
<p>
<s>
alfanumerická	alfanumerický	k2eAgFnSc1d1	alfanumerická
klávesnice	klávesnice	k1gFnSc1	klávesnice
</s>
</p>
<p>
<s>
klávesnice	klávesnice	k1gFnSc1	klávesnice
psacího	psací	k2eAgInSc2d1	psací
stroje	stroj	k1gInSc2	stroj
nebo	nebo	k8xC	nebo
dálnopisu	dálnopis	k1gInSc2	dálnopis
</s>
</p>
<p>
<s>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
klávesnice	klávesnice	k1gFnSc1	klávesnice
</s>
</p>
<p>
<s>
numerická	numerický	k2eAgFnSc1d1	numerická
klávesnice	klávesnice	k1gFnSc1	klávesnice
</s>
</p>
<p>
<s>
klávesnice	klávesnice	k1gFnSc1	klávesnice
kalkulačky	kalkulačka	k1gFnSc2	kalkulačka
</s>
</p>
<p>
<s>
klávesnice	klávesnice	k1gFnSc1	klávesnice
telefonu	telefon	k1gInSc2	telefon
</s>
</p>
<p>
<s>
hudební	hudební	k2eAgFnPc1d1	hudební
klávesnice	klávesnice	k1gFnPc1	klávesnice
(	(	kIx(	(
<g/>
též	též	k9	též
zvaná	zvaný	k2eAgFnSc1d1	zvaná
klaviatura	klaviatura	k1gFnSc1	klaviatura
<g/>
;	;	kIx,	;
např.	např.	kA	např.
u	u	k7c2	u
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
varhan	varhany	k1gFnPc2	varhany
<g/>
,	,	kIx,	,
akordeonu	akordeon	k1gInSc2	akordeon
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jiné	jiný	k2eAgFnPc1d1	jiná
klávesnice	klávesnice	k1gFnPc1	klávesnice
</s>
</p>
<p>
<s>
piktografická	piktografický	k2eAgFnSc1d1	piktografický
klávesnice	klávesnice	k1gFnSc1	klávesnice
–	–	k?	–
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
klávesy	klávesa	k1gFnPc1	klávesa
mají	mít	k5eAaImIp3nP	mít
specifickou	specifický	k2eAgFnSc4d1	specifická
hodnotu	hodnota	k1gFnSc4	hodnota
vyjádřenou	vyjádřený	k2eAgFnSc4d1	vyjádřená
obrázkem	obrázek	k1gInSc7	obrázek
či	či	k8xC	či
piktogramem	piktogram	k1gInSc7	piktogram
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
automatických	automatický	k2eAgFnPc6d1	automatická
potravinářských	potravinářský	k2eAgFnPc6d1	potravinářská
váhách	váha	k1gFnPc6	váha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zvonková	zvonkový	k2eAgFnSc1d1	Zvonková
klávesnice	klávesnice	k1gFnSc1	klávesnice
(	(	kIx(	(
<g/>
též	též	k9	též
zvaná	zvaný	k2eAgFnSc1d1	zvaná
zvonkové	zvonkový	k2eAgNnSc4d1	zvonkové
tablo	tablo	k1gNnSc4	tablo
<g/>
)	)	kIx)	)
<g/>
Klávesnice	klávesnice	k1gFnSc1	klávesnice
může	moct	k5eAaImIp3nS	moct
spadat	spadat	k5eAaImF	spadat
i	i	k9	i
do	do	k7c2	do
více	hodně	k6eAd2	hodně
kategorií	kategorie	k1gFnPc2	kategorie
najednou	najednou	k6eAd1	najednou
–	–	k?	–
např.	např.	kA	např.
klávesnice	klávesnice	k1gFnSc2	klávesnice
dálkového	dálkový	k2eAgNnSc2d1	dálkové
ovládání	ovládání	k1gNnSc2	ovládání
je	být	k5eAaImIp3nS	být
numerická	numerický	k2eAgFnSc1d1	numerická
a	a	k8xC	a
piktografická	piktografický	k2eAgFnSc1d1	piktografický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Klaviatura	klaviatura	k1gFnSc1	klaviatura
</s>
</p>
<p>
<s>
Technologie	technologie	k1gFnSc1	technologie
klávesnic	klávesnice	k1gFnPc2	klávesnice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
klávesnice	klávesnice	k1gFnSc2	klávesnice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
