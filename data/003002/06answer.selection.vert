<s>
Principy	princip	k1gInPc1	princip
integrování	integrování	k1gNnSc2	integrování
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
formulovány	formulovat	k5eAaImNgInP	formulovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
Isaacem	Isaace	k1gMnSc7	Isaace
Newtonem	Newton	k1gMnSc7	Newton
a	a	k8xC	a
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Leibnizem	Leibniz	k1gMnSc7	Leibniz
na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
