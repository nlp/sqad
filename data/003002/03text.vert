<s>
Integrál	integrál	k1gInSc1	integrál
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
pojmů	pojem	k1gInPc2	pojem
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
derivací	derivace	k1gFnSc7	derivace
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
operace	operace	k1gFnPc4	operace
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
integrálu	integrál	k1gInSc2	integrál
je	být	k5eAaImIp3nS	být
zobecněním	zobecnění	k1gNnSc7	zobecnění
pojmů	pojem	k1gInPc2	pojem
jako	jako	k8xC	jako
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
<g/>
,	,	kIx,	,
součet	součet	k1gInSc1	součet
či	či	k8xC	či
suma	suma	k1gFnSc1	suma
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
funkci	funkce	k1gFnSc4	funkce
ƒ	ƒ	k?	ƒ
reálné	reálný	k2eAgInPc4d1	reálný
proměnné	proměnný	k2eAgInPc4d1	proměnný
x	x	k?	x
na	na	k7c6	na
intervalu	interval	k1gInSc6	interval
<	<	kIx(	<
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
>	>	kIx)	>
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
(	(	kIx(	(
<g/>
určitý	určitý	k2eAgInSc4d1	určitý
<g/>
)	)	kIx)	)
integrál	integrál	k1gInSc4	integrál
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
rozumíme	rozumět	k5eAaImIp1nP	rozumět
obsah	obsah	k1gInSc4	obsah
plochy	plocha	k1gFnSc2	plocha
ve	v	k7c6	v
dvojrozměrné	dvojrozměrný	k2eAgFnSc6d1	dvojrozměrná
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
grafem	graf	k1gInSc7	graf
funkce	funkce	k1gFnSc2	funkce
ƒ	ƒ	k?	ƒ
<g/>
,	,	kIx,	,
osou	osa	k1gFnSc7	osa
x	x	k?	x
a	a	k8xC	a
svislými	svislý	k2eAgFnPc7d1	svislá
přímkami	přímka	k1gFnPc7	přímka
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
a	a	k8xC	a
a	a	k8xC	a
x	x	k?	x
=	=	kIx~	=
b.	b.	k?	b.
Pojmem	pojem	k1gInSc7	pojem
integrál	integrál	k1gInSc1	integrál
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
označuje	označovat	k5eAaImIp3nS	označovat
primitivní	primitivní	k2eAgFnSc1d1	primitivní
funkce	funkce	k1gFnSc1	funkce
F	F	kA	F
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
derivací	derivace	k1gFnSc7	derivace
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
ƒ	ƒ	k?	ƒ
To	to	k9	to
celé	celá	k1gFnSc2	celá
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
neurčitý	určitý	k2eNgInSc4d1	neurčitý
integrál	integrál	k1gInSc4	integrál
a	a	k8xC	a
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
=	=	kIx~	=
∫	∫	k?	∫
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x.	x.	k?	x.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Integrály	integrál	k1gInPc1	integrál
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
určité	určitý	k2eAgInPc4d1	určitý
integrály	integrál	k1gInPc4	integrál
<g/>
.	.	kIx.	.
</s>
<s>
Principy	princip	k1gInPc1	princip
integrování	integrování	k1gNnSc2	integrování
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
formulovány	formulovat	k5eAaImNgInP	formulovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
Isaacem	Isaace	k1gMnSc7	Isaace
Newtonem	Newton	k1gMnSc7	Newton
a	a	k8xC	a
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Leibnizem	Leibniz	k1gMnSc7	Leibniz
na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nezávisle	závisle	k6eNd1	závisle
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
základní	základní	k2eAgFnSc4d1	základní
větu	věta	k1gFnSc4	věta
analýzy	analýza	k1gFnSc2	analýza
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
spojili	spojit	k5eAaPmAgMnP	spojit
diferenciální	diferenciální	k2eAgInSc4d1	diferenciální
a	a	k8xC	a
integrální	integrální	k2eAgInSc4d1	integrální
počet	počet	k1gInSc4	počet
<g/>
.	.	kIx.	.
</s>
<s>
Věta	věta	k1gFnSc1	věta
zní	znět	k5eAaImIp3nS	znět
asi	asi	k9	asi
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Nechť	nechť	k9	nechť
ƒ	ƒ	k?	ƒ
je	být	k5eAaImIp3nS	být
spojitá	spojitý	k2eAgFnSc1d1	spojitá
reálná	reálný	k2eAgFnSc1d1	reálná
funkce	funkce	k1gFnSc1	funkce
na	na	k7c6	na
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
intervalu	interval	k1gInSc6	interval
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
]	]	kIx)	]
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
F	F	kA	F
je	být	k5eAaImIp3nS	být
primitivní	primitivní	k2eAgInSc1d1	primitivní
k	k	k7c3	k
funkci	funkce	k1gFnSc3	funkce
ƒ	ƒ	k?	ƒ
Potom	potom	k8xC	potom
hodnota	hodnota	k1gFnSc1	hodnota
(	(	kIx(	(
<g/>
určitého	určitý	k2eAgInSc2d1	určitý
<g/>
)	)	kIx)	)
integrálu	integrál	k1gInSc2	integrál
funkce	funkce	k1gFnSc2	funkce
ƒ	ƒ	k?	ƒ
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
intervalu	interval	k1gInSc6	interval
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
F	F	kA	F
(	(	kIx(	(
b	b	k?	b
)	)	kIx)	)
-	-	kIx~	-
F	F	kA	F
(	(	kIx(	(
a	a	k8xC	a
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
-F	-F	k?	-F
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
∈	∈	k?	∈
⟨	⟨	k?	⟨
a	a	k8xC	a
;	;	kIx,	;
b	b	k?	b
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
a	a	k8xC	a
<g/>
;	;	kIx,	;
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
rangle	rangl	k1gMnSc2	rangl
}	}	kIx)	}
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
F	F	kA	F
(	(	kIx(	(
b	b	k?	b
)	)	kIx)	)
-	-	kIx~	-
F	F	kA	F
(	(	kIx(	(
a	a	k8xC	a
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
-F	-F	k?	-F
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Jednoduše	jednoduše	k6eAd1	jednoduše
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
určitý	určitý	k2eAgInSc4d1	určitý
integrál	integrál	k1gInSc4	integrál
nezáporné	záporný	k2eNgFnSc2d1	nezáporná
<g />
.	.	kIx.	.
</s>
<s hack="1">
funkce	funkce	k1gFnSc1	funkce
jedné	jeden	k4xCgFnSc2	jeden
proměnné	proměnná	k1gFnSc2	proměnná
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
nějakými	nějaký	k3yIgNnPc7	nějaký
dvěma	dva	k4xCgNnPc7	dva
body	bod	k1gInPc1	bod
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
roven	roven	k2eAgInSc1d1	roven
ploše	plocha	k1gFnSc3	plocha
obrazce	obrazec	k1gInSc2	obrazec
omezeného	omezený	k2eAgInSc2d1	omezený
přímkami	přímka	k1gFnPc7	přímka
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
osou	osa	k1gFnSc7	osa
x	x	k?	x
a	a	k8xC	a
křivkou	křivka	k1gFnSc7	křivka
definovanou	definovaný	k2eAgFnSc7d1	definovaná
grafem	graf	k1gInSc7	graf
funkce	funkce	k1gFnSc1	funkce
f.	f.	k?	f.
Formálněji	formálně	k6eAd2	formálně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
takový	takový	k3xDgMnSc1	takový
<g />
.	.	kIx.	.
</s>
<s hack="1">
integrál	integrál	k1gInSc1	integrál
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
míře	míra	k1gFnSc3	míra
množiny	množina	k1gFnSc2	množina
S	s	k7c7	s
definované	definovaný	k2eAgInPc1d1	definovaný
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
{	{	kIx(	{
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
∈	∈	k?	∈
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
≤	≤	k?	≤
x	x	k?	x
≤	≤	k?	≤
b	b	k?	b
,	,	kIx,	,
0	[number]	k4	0
≤	≤	k?	≤
y	y	k?	y
≤	≤	k?	≤
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
:	:	kIx,	:
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
<g />
.	.	kIx.	.
</s>
<s hack="1">
b	b	k?	b
<g/>
,0	,0	k4	,0
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Integrál	integrál	k1gInSc1	integrál
(	(	kIx(	(
<g/>
∫	∫	k?	∫
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
stylizovaným	stylizovaný	k2eAgNnSc7d1	stylizované
protaženým	protažený	k2eAgNnSc7d1	protažené
písmenem	písmeno	k1gNnSc7	písmeno
s	s	k7c7	s
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
s	s	k7c7	s
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
s	s	k7c7	s
<g/>
,	,	kIx,	,
summa	summa	k1gNnSc1	summa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
značení	značení	k1gNnSc4	značení
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Gottfried	Gottfried	k1gMnSc1	Gottfried
Leibniz	Leibniz	k1gMnSc1	Leibniz
<g/>
.	.	kIx.	.
</s>
<s>
Integrál	integrál	k1gInSc1	integrál
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
odstavce	odstavec	k1gInSc2	odstavec
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
značil	značit	k5eAaImAgInS	značit
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
znaménko	znaménko	k1gNnSc1	znaménko
∫	∫	k?	∫
značí	značit	k5eAaImIp3nS	značit
integrování	integrování	k1gNnSc4	integrování
<g/>
,	,	kIx,	,
a	a	k8xC	a
a	a	k8xC	a
b	b	k?	b
jsou	být	k5eAaImIp3nP	být
integrační	integrační	k2eAgFnPc4d1	integrační
meze	mez	k1gFnPc4	mez
(	(	kIx(	(
<g/>
jen	jen	k9	jen
u	u	k7c2	u
určitého	určitý	k2eAgInSc2d1	určitý
integrálu	integrál	k1gInSc2	integrál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dx	dx	k?	dx
označuje	označovat	k5eAaImIp3nS	označovat
proměnnou	proměnná	k1gFnSc4	proměnná
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
integruje	integrovat	k5eAaBmIp3nS	integrovat
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
označovalo	označovat	k5eAaImAgNnS	označovat
infinitezimální	infinitezimální	k2eAgFnSc4d1	infinitezimální
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
však	však	k9	však
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k9	jen
jako	jako	k9	jako
ryze	ryze	k6eAd1	ryze
symbolické	symbolický	k2eAgNnSc1d1	symbolické
označení	označení	k1gNnSc1	označení
bez	bez	k7c2	bez
dalšího	další	k2eAgInSc2d1	další
významu	význam	k1gInSc2	význam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
d	d	k?	d
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
proměnné	proměnná	k1gFnSc2	proměnná
nepíše	psát	k5eNaImIp3nS	psát
kurzívou	kurzíva	k1gFnSc7	kurzíva
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
ekvivalentních	ekvivalentní	k2eAgFnPc2d1	ekvivalentní
definic	definice	k1gFnPc2	definice
určitého	určitý	k2eAgInSc2d1	určitý
integrálu	integrál	k1gInSc2	integrál
<g/>
.	.	kIx.	.
</s>
<s>
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
integrál	integrál	k1gInSc1	integrál
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gFnSc1	jehož
definice	definice	k1gFnSc1	definice
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
neurčitým	určitý	k2eNgInSc7d1	neurčitý
integrálem	integrál	k1gInSc7	integrál
<g/>
)	)	kIx)	)
Zobecněný	zobecněný	k2eAgInSc1d1	zobecněný
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
integrál	integrál	k1gInSc1	integrál
Riemannův	Riemannův	k2eAgInSc4d1	Riemannův
integrál	integrál	k1gInSc4	integrál
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gFnSc1	jehož
definice	definice	k1gFnSc1	definice
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
geometrickou	geometrický	k2eAgFnSc4d1	geometrická
interpretaci	interpretace	k1gFnSc4	interpretace
"	"	kIx"	"
<g/>
plocha	plocha	k1gFnSc1	plocha
pod	pod	k7c7	pod
křivkou	křivka	k1gFnSc7	křivka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Lebesgueův	Lebesgueův	k2eAgInSc1d1	Lebesgueův
integrál	integrál	k1gInSc1	integrál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zintegrovat	zintegrovat	k5eAaBmF	zintegrovat
zvláště	zvláště	k6eAd1	zvláště
širokou	široký	k2eAgFnSc4d1	široká
třídu	třída	k1gFnSc4	třída
funkcí	funkce	k1gFnPc2	funkce
Aj.	aj.	kA	aj.
Tyto	tento	k3xDgFnPc1	tento
definice	definice	k1gFnPc4	definice
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
množinou	množina	k1gFnSc7	množina
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
integrovatelné	integrovatelný	k2eAgMnPc4d1	integrovatelný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
definicí	definice	k1gFnSc7	definice
funkce	funkce	k1gFnSc1	funkce
integrovatelná	integrovatelný	k2eAgFnSc1d1	integrovatelná
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
integrálu	integrál	k1gInSc2	integrál
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Primitivní	primitivní	k2eAgFnSc2d1	primitivní
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Termínem	termín	k1gInSc7	termín
"	"	kIx"	"
<g/>
neurčitý	určitý	k2eNgInSc1d1	neurčitý
integrál	integrál	k1gInSc1	integrál
<g/>
"	"	kIx"	"
funkce	funkce	k1gFnSc1	funkce
f	f	k?	f
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
často	často	k6eAd1	často
rozumí	rozumět	k5eAaImIp3nS	rozumět
množina	množina	k1gFnSc1	množina
jejích	její	k3xOp3gFnPc2	její
primitivních	primitivní	k2eAgFnPc2d1	primitivní
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nejspíše	nejspíše	k9	nejspíše
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
integrálů	integrál	k1gInPc2	integrál
"	"	kIx"	"
<g/>
hezkých	hezký	k2eAgFnPc2d1	hezká
<g/>
"	"	kIx"	"
funkcí	funkce	k1gFnSc7	funkce
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívá	využívat	k5eAaPmIp3nS	využívat
primitivních	primitivní	k2eAgFnPc2d1	primitivní
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
základní	základní	k2eAgFnSc3d1	základní
větě	věta	k1gFnSc3	věta
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Aplikace	aplikace	k1gFnSc2	aplikace
integrálu	integrál	k1gInSc2	integrál
<g/>
.	.	kIx.	.
</s>
<s>
Možnosti	možnost	k1gFnPc1	možnost
použití	použití	k1gNnSc2	použití
určitého	určitý	k2eAgInSc2d1	určitý
integrálu	integrál	k1gInSc2	integrál
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
určitého	určitý	k2eAgInSc2d1	určitý
integrálu	integrál	k1gInSc2	integrál
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
např.	např.	kA	např.
obsah	obsah	k1gInSc4	obsah
rovinného	rovinný	k2eAgInSc2d1	rovinný
obrazce	obrazec	k1gInSc2	obrazec
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
oblouku	oblouk	k1gInSc2	oblouk
rovinné	rovinný	k2eAgFnSc2d1	rovinná
křivky	křivka	k1gFnSc2	křivka
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc4	obsah
rotační	rotační	k2eAgFnSc2d1	rotační
plochy	plocha	k1gFnSc2	plocha
nebo	nebo	k8xC	nebo
objem	objem	k1gInSc1	objem
rotačního	rotační	k2eAgNnSc2d1	rotační
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
pak	pak	k6eAd1	pak
určitý	určitý	k2eAgInSc4d1	určitý
integrál	integrál	k1gInSc4	integrál
můžeme	moct	k5eAaImIp1nP	moct
použít	použít	k5eAaPmF	použít
při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
např.	např.	kA	např.
statických	statický	k2eAgInPc2d1	statický
momentů	moment	k1gInPc2	moment
<g/>
,	,	kIx,	,
momentů	moment	k1gInPc2	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
,	,	kIx,	,
těžiště	těžiště	k1gNnSc2	těžiště
tělesa	těleso	k1gNnSc2	těleso
nebo	nebo	k8xC	nebo
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
primitivní	primitivní	k2eAgFnSc1d1	primitivní
funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
mezí	mez	k1gFnPc2	mez
nemá	mít	k5eNaImIp3nS	mít
limitu	limita	k1gFnSc4	limita
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
integrál	integrál	k1gInSc1	integrál
definuje	definovat	k5eAaBmIp3nS	definovat
pomocí	pomocí	k7c2	pomocí
jednostranné	jednostranný	k2eAgFnSc2d1	jednostranná
limity	limita	k1gFnSc2	limita
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
spodní	spodní	k2eAgFnSc2d1	spodní
meze	mez	k1gFnSc2	mez
takto	takto	k6eAd1	takto
(	(	kIx(	(
<g/>
F	F	kA	F
je	být	k5eAaImIp3nS	být
primitivní	primitivní	k2eAgFnSc1d1	primitivní
funkce	funkce	k1gFnSc1	funkce
k	k	k7c3	k
f	f	k?	f
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
t	t	k?	t
→	→	k?	→
a	a	k8xC	a
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
<g />
.	.	kIx.	.
</s>
<s>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Například	například	k6eAd1	například
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
+	+	kIx~	+
∞	∞	k?	∞
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
mezí	mez	k1gFnPc2	mez
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
t	t	k?	t
→	→	k?	→
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Například	například	k6eAd1	například
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
+	+	kIx~	+
∞	∞	k?	∞
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Integrál	integrál	k1gInSc1	integrál
komplexní	komplexní	k2eAgFnSc2d1	komplexní
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komplexních	komplexní	k2eAgNnPc6d1	komplexní
číslech	číslo	k1gNnPc6	číslo
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
užívají	užívat	k5eAaImIp3nP	užívat
křivkové	křivkový	k2eAgInPc4d1	křivkový
integrály	integrál	k1gInPc4	integrál
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tyto	tento	k3xDgInPc1	tento
integrály	integrál	k1gInPc1	integrál
probíhají	probíhat	k5eAaImIp3nP	probíhat
po	po	k7c6	po
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
křivce	křivka	k1gFnSc6	křivka
v	v	k7c6	v
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
snadno	snadno	k6eAd1	snadno
spočíst	spočíst	k5eAaPmF	spočíst
pomocí	pomocí	k7c2	pomocí
reziduové	reziduové	k2eAgFnSc2d1	reziduové
věty	věta	k1gFnSc2	věta
<g/>
,	,	kIx,	,
Cauchyova	Cauchyův	k2eAgInSc2d1	Cauchyův
vzorce	vzorec	k1gInSc2	vzorec
nebo	nebo	k8xC	nebo
Cauchyovy	Cauchyův	k2eAgFnSc2d1	Cauchyova
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vícerozměrný	vícerozměrný	k2eAgInSc4d1	vícerozměrný
integrál	integrál	k1gInSc4	integrál
<g/>
.	.	kIx.	.
</s>
<s>
Integraci	integrace	k1gFnSc4	integrace
pro	pro	k7c4	pro
funkce	funkce	k1gFnPc4	funkce
více	hodně	k6eAd2	hodně
proměnných	proměnná	k1gFnPc2	proměnná
lze	lze	k6eAd1	lze
zavést	zavést	k5eAaPmF	zavést
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
pro	pro	k7c4	pro
funkce	funkce	k1gFnPc4	funkce
jedné	jeden	k4xCgFnSc2	jeden
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Integrace	integrace	k1gFnSc1	integrace
probíhá	probíhat	k5eAaImIp3nS	probíhat
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
oblasti	oblast	k1gFnSc6	oblast
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Omega	omega	k1gNnPc6	omega
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
funkcí	funkce	k1gFnSc7	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
<g/>
}	}	kIx)	}
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
její	její	k3xOp3gInSc4	její
integrál	integrál	k1gInSc4	integrál
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
n-rozměrné	nozměrný	k2eAgFnSc6d1	n-rozměrná
oblasti	oblast	k1gFnSc6	oblast
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Omega	omega	k1gNnSc2	omega
}	}	kIx)	}
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
vícerozměrný	vícerozměrný	k2eAgInSc4d1	vícerozměrný
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
-rozměrný	ozměrný	k2eAgInSc4d1	-rozměrný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dvourozměrný	dvourozměrný	k2eAgInSc4d1	dvourozměrný
<g/>
,	,	kIx,	,
trojrozměrný	trojrozměrný	k2eAgInSc4d1	trojrozměrný
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
integrál	integrál	k1gInSc4	integrál
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jej	on	k3xPp3gMnSc4	on
zapíšeme	zapsat	k5eAaPmIp1nP	zapsat
některým	některý	k3yIgFnPc3	některý
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
způsobů	způsob	k1gInPc2	způsob
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
⋯	⋯	k?	⋯
∫	∫	k?	∫
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
Ω	Ω	k?	Ω
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
⋯	⋯	k?	⋯
∫	∫	k?	∫
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
⋯	⋯	k?	⋯
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
⋯	⋯	k?	⋯
∫	∫	k?	∫
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iint	iint	k1gInSc1	iint
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
\	\	kIx~	\
<g/>
int	int	k?	int
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gNnSc7	omega
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iint	iint	k1gInSc1	iint
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
\	\	kIx~	\
<g/>
int	int	k?	int
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gNnSc7	omega
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iint	iint	k1gInSc1	iint
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
\	\	kIx~	\
<g/>
int	int	k?	int
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gNnSc7	omega
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
:	:	kIx,	:
Počet	počet	k1gInSc1	počet
integračních	integrační	k2eAgInPc2d1	integrační
znaků	znak	k1gInPc2	znak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
}	}	kIx)	}
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
počtu	počet	k1gInSc2	počet
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
které	který	k3yRgFnPc4	který
integrujeme	integrovat	k5eAaBmIp1nP	integrovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ze	z	k7c2	z
zápisu	zápis	k1gInSc2	zápis
integrálu	integrál	k1gInSc2	integrál
zjevné	zjevný	k2eAgNnSc1d1	zjevné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vícerozměrný	vícerozměrný	k2eAgInSc4d1	vícerozměrný
integrál	integrál	k1gInSc4	integrál
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zapisujeme	zapisovat	k5eAaImIp1nP	zapisovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
integrační	integrační	k2eAgInSc4d1	integrační
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Vícerozměrné	vícerozměrný	k2eAgInPc1d1	vícerozměrný
integrály	integrál	k1gInPc1	integrál
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
řeší	řešit	k5eAaImIp3nP	řešit
převodem	převod	k1gInSc7	převod
na	na	k7c4	na
vícenásobnou	vícenásobný	k2eAgFnSc4d1	vícenásobná
integraci	integrace	k1gFnSc4	integrace
pomocí	pomocí	k7c2	pomocí
Fubiniovy	Fubiniův	k2eAgFnSc2d1	Fubiniova
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Základní	základní	k2eAgFnSc1d1	základní
věta	věta	k1gFnSc1	věta
integrálního	integrální	k2eAgInSc2d1	integrální
počtu	počet	k1gInSc2	počet
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgInSc4d1	určitý
i	i	k8xC	i
neurčitý	určitý	k2eNgInSc4d1	neurčitý
integrál	integrál	k1gInSc4	integrál
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
definic	definice	k1gFnPc2	definice
-	-	kIx~	-
například	například	k6eAd1	například
určitý	určitý	k2eAgInSc1d1	určitý
integrál	integrál	k1gInSc1	integrál
síly	síla	k1gFnSc2	síla
podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
je	být	k5eAaImIp3nS	být
vykonaná	vykonaný	k2eAgFnSc1d1	vykonaná
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
neurčitý	určitý	k2eNgInSc1d1	neurčitý
integrál	integrál	k1gInSc1	integrál
ze	z	k7c2	z
zrychlení	zrychlení	k1gNnSc2	zrychlení
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
apod.	apod.	kA	apod.
Určitý	určitý	k2eAgInSc4d1	určitý
integrál	integrál	k1gInSc4	integrál
z	z	k7c2	z
rychlosti	rychlost	k1gFnSc2	rychlost
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
změně	změna	k1gFnSc3	změna
polohy	poloha	k1gFnSc2	poloha
během	během	k7c2	během
časového	časový	k2eAgInSc2d1	časový
úseku	úsek	k1gInSc2	úsek
od	od	k7c2	od
t	t	k?	t
<g/>
1	[number]	k4	1
do	do	k7c2	do
t	t	k?	t
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
polohu	poloha	k1gFnSc4	poloha
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
čase	čas	k1gInSc6	čas
označíme	označit	k5eAaPmIp1nP	označit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
tedy	tedy	k9	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
-	-	kIx~	-
x	x	k?	x
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-x	-x	k?	-x
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
<g />
.	.	kIx.	.
</s>
<s hack="1">
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
v	v	k7c6	v
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Tento	tento	k3xDgInSc1	tento
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
zobecněním	zobecnění	k1gNnSc7	zobecnění
známého	známý	k2eAgInSc2d1	známý
vztahu	vztah	k1gInSc2	vztah
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
konstantní	konstantní	k2eAgFnSc7d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
-	-	kIx~	-
x	x	k?	x
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
v	v	k7c6	v
⋅	⋅	k?	⋅
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-x	-x	k?	-x
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
v	v	k7c4	v
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgInSc4d1	cdot
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-t_	_	k?	-t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
△	△	k?	△
x	x	k?	x
=	=	kIx~	=
v	v	k7c6	v
⋅	⋅	k?	⋅
△	△	k?	△
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
triangle	triangl	k1gInSc5	triangl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
v	v	k7c4	v
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgInSc4d1	cdot
\	\	kIx~	\
<g/>
triangle	triangl	k1gInSc5	triangl
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Tyto	tento	k3xDgInPc1	tento
vzorce	vzorec	k1gInPc1	vzorec
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
integrál	integrál	k1gInSc4	integrál
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
i	i	k9	i
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
proměnlivou	proměnlivý	k2eAgFnSc7d1	proměnlivá
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Neurčitý	určitý	k2eNgInSc1d1	neurčitý
integrál	integrál	k1gInSc1	integrál
z	z	k7c2	z
rychlosti	rychlost	k1gFnSc2	rychlost
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
poloha	poloha	k1gFnSc1	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Argumentem	argument	k1gInSc7	argument
integrálu	integrál	k1gInSc2	integrál
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
funkce	funkce	k1gFnSc1	funkce
představující	představující	k2eAgFnSc1d1	představující
závislost	závislost	k1gFnSc4	závislost
rychlosti	rychlost	k1gFnSc2	rychlost
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
;	;	kIx,	;
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
závislost	závislost	k1gFnSc4	závislost
polohy	poloha	k1gFnSc2	poloha
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
zvaných	zvaný	k2eAgFnPc2d1	zvaná
primitivní	primitivní	k2eAgMnSc1d1	primitivní
funkce	funkce	k1gFnPc4	funkce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
možnou	možný	k2eAgFnSc4d1	možná
počáteční	počáteční	k2eAgFnSc4d1	počáteční
polohu	poloha	k1gFnSc4	poloha
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
fyzikální	fyzikální	k2eAgFnSc3d1	fyzikální
realitě	realita	k1gFnSc3	realita
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
znalosti	znalost	k1gFnSc2	znalost
rychlosti	rychlost	k1gFnSc2	rychlost
lze	lze	k6eAd1	lze
spočítat	spočítat	k5eAaPmF	spočítat
polohu	poloha	k1gFnSc4	poloha
objektu	objekt	k1gInSc2	objekt
v	v	k7c6	v
čase	čas	k1gInSc6	čas
t	t	k?	t
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
známe	znát	k5eAaImIp1nP	znát
jeho	jeho	k3xOp3gFnSc4	jeho
polohu	poloha	k1gFnSc4	poloha
v	v	k7c6	v
nějakém	nějaký	k3yIgNnSc6	nějaký
<g />
.	.	kIx.	.
</s>
<s hack="1">
čase	čas	k1gInSc6	čas
t	t	k?	t
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
volným	volný	k2eAgInSc7d1	volný
pádem	pád	k1gInSc7	pád
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gFnSc1	jeho
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
=	=	kIx~	=
-	-	kIx~	-
g	g	kA	g
.	.	kIx.	.
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
g.t	g.t	k?	g.t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
a	a	k8xC	a
znaménko	znaménko	k1gNnSc1	znaménko
mínus	mínus	k6eAd1	mínus
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
směr	směr	k1gInSc1	směr
dolů	dol	k1gInPc2	dol
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Zeme	Zem	k1gFnSc2	Zem
se	se	k3xPyFc4	se
nachazi	nachaze	k1gFnSc3	nachaze
pocatek	pocatka	k1gFnPc2	pocatka
souradnic	souradnice	k1gFnPc2	souradnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
polohu	poloha	k1gFnSc4	poloha
pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
=	=	kIx~	=
∫	∫	k?	∫
v	v	k7c4	v
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
-	-	kIx~	-
g	g	kA	g
.	.	kIx.	.
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
=	=	kIx~	=
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
v	v	k7c4	v
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
g.t	g.t	k?	g.t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
gt	gt	k?	gt
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
integrační	integrační	k2eAgFnSc1d1	integrační
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
za	za	k7c4	za
níž	nízce	k6eAd2	nízce
dosazením	dosazení	k1gNnSc7	dosazení
různých	různý	k2eAgFnPc2d1	různá
hodnot	hodnota	k1gFnPc2	hodnota
dostaneme	dostat	k5eAaPmIp1nP	dostat
různé	různý	k2eAgFnPc1d1	různá
možné	možný	k2eAgFnPc1d1	možná
závislosti	závislost	k1gFnPc1	závislost
polohy	poloha	k1gFnSc2	poloha
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
=	=	kIx~	=
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
50	[number]	k4	50
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
gt	gt	k?	gt
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
50	[number]	k4	50
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
popisuje	popisovat	k5eAaImIp3nS	popisovat
volný	volný	k2eAgInSc4d1	volný
pád	pád	k1gInSc4	pád
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgInSc4d1	určitý
integrál	integrál	k1gInSc4	integrál
lze	lze	k6eAd1	lze
spočítat	spočítat	k5eAaPmF	spočítat
jako	jako	k9	jako
rozdíl	rozdíl	k1gInSc1	rozdíl
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
neurčitého	určitý	k2eNgInSc2d1	neurčitý
integrálu	integrál	k1gInSc2	integrál
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
výpočet	výpočet	k1gInSc1	výpočet
dráhy	dráha	k1gFnSc2	dráha
uražené	uražený	k2eAgFnPc4d1	uražená
mezi	mezi	k7c7	mezi
časem	čas	k1gInSc7	čas
3	[number]	k4	3
sekundy	sekunda	k1gFnSc2	sekunda
a	a	k8xC	a
5	[number]	k4	5
sekund	sekunda	k1gFnPc2	sekunda
se	se	k3xPyFc4	se
spočte	spočíst	k5eAaPmIp3nS	spočíst
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvolíme	zvolit	k5eAaPmIp1nP	zvolit
libovolnou	libovolný	k2eAgFnSc7d1	libovolná
z	z	k7c2	z
primitivních	primitivní	k2eAgFnPc2d1	primitivní
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
nejpřirozenější	přirozený	k2eAgNnSc1d3	nejpřirozenější
volit	volit	k5eAaImF	volit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
=	=	kIx~	=
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
gt	gt	k?	gt
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
)	)	kIx)	)
a	a	k8xC	a
spočteme	spočíst	k5eAaPmIp1nP	spočíst
její	její	k3xOp3gInSc4	její
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
časových	časový	k2eAgFnPc6d1	časová
mezích	mez	k1gFnPc6	mez
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
g	g	kA	g
.	.	kIx.	.
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
x	x	k?	x
(	(	kIx(	(
5	[number]	k4	5
)	)	kIx)	)
-	-	kIx~	-
x	x	k?	x
(	(	kIx(	(
3	[number]	k4	3
)	)	kIx)	)
=	=	kIx~	=
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
g.t	g.t	k?	g.t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
-x	-x	k?	-x
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
g	g	kA	g
<g/>
5	[number]	k4	5
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
g	g	kA	g
<g/>
3	[number]	k4	3
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
:	:	kIx,	:
</s>
