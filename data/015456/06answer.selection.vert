<s>
Řízená	řízený	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
CTA	CTA	kA
(	(	kIx(
<g/>
angl.	angl.	k?
Control	Control	k1gInSc1
area	area	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
část	část	k1gFnSc1
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
podléhá	podléhat	k5eAaImIp3nS
řízení	řízení	k1gNnSc4
leteckého	letecký	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>