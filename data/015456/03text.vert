<s>
Řízená	řízený	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Řízená	řízený	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
CTA	CTA	kA
(	(	kIx(
<g/>
angl.	angl.	k?
Control	Control	k1gInSc1
area	area	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
část	část	k1gFnSc1
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
podléhá	podléhat	k5eAaImIp3nS
řízení	řízení	k1gNnSc4
leteckého	letecký	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vertikálně	vertikálně	k6eAd1
sahá	sahat	k5eAaImIp3nS
typicky	typicky	k6eAd1
od	od	k7c2
výšky	výška	k1gFnSc2
1000	#num#	k4
<g/>
ft	ft	k?
(	(	kIx(
<g/>
stop	stopa	k1gFnPc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
300	#num#	k4
m	m	kA
<g/>
)	)	kIx)
nad	nad	k7c7
zemským	zemský	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
do	do	k7c2
letové	letový	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
(	(	kIx(
<g/>
FL	FL	kA
<g/>
)	)	kIx)
660	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
do	do	k7c2
výšky	výška	k1gFnSc2
66000	#num#	k4
stop	stopa	k1gFnPc2
(	(	kIx(
<g/>
19800	#num#	k4
<g/>
m	m	kA
<g/>
)	)	kIx)
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vertikálně	vertikálně	k6eAd1
i	i	k8xC
horizontálně	horizontálně	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
provedeno	proveden	k2eAgNnSc1d1
členění	členění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horizontálně	horizontálně	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
soustavou	soustava	k1gFnSc7
pěti	pět	k4xCc3
CTA	CTA	kA
(	(	kIx(
<g/>
CTA	CTA	kA
2	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
CTA	CTA	kA
1	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
CTA	CTA	kA
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
CTA	CTA	kA
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
CTA	CTA	kA
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
pokryté	pokrytý	k2eAgInPc1d1
beze	beze	k7c2
zbytku	zbytek	k1gInSc2
veškeré	veškerý	k3xTgNnSc4
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
CTA	CTA	kA
jsou	být	k5eAaImIp3nP
vetknuté	vetknutý	k2eAgInPc4d1
ostatní	ostatní	k2eAgInPc4d1
prvky	prvek	k1gInPc4
organizace	organizace	k1gFnSc2
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
TMA	tma	k1gFnSc1
a	a	k8xC
CTR	CTR	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
spodní	spodní	k2eAgFnSc1d1
část	část	k1gFnSc1
CTR	CTR	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
vertikálně	vertikálně	k6eAd1
pod	pod	k7c7
spodní	spodní	k2eAgFnSc7d1
hranicí	hranice	k1gFnSc7
CTA	CTA	kA
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
je	být	k5eAaImIp3nS
provoz	provoz	k1gInSc4
v	v	k7c6
CTA	CTA	kA
jako	jako	k8xS,k8xC
celek	celek	k1gInSc1
řízen	řídit	k5eAaImNgInS
<g/>
,	,	kIx,
ve	v	k7c6
významných	významný	k2eAgFnPc6d1
částech	část	k1gFnPc6
CTA	CTA	kA
je	být	k5eAaImIp3nS
možný	možný	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
letounů	letoun	k1gInPc2
bez	bez	k7c2
neustálého	neustálý	k2eAgInSc2d1
kontaktu	kontakt	k1gInSc2
s	s	k7c7
řídícím	řídící	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
