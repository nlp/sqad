<s>
Verifikace	verifikace	k1gFnSc1	verifikace
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
verum	verum	k1gNnSc4	verum
facere	facrat	k5eAaPmIp3nS	facrat
<g/>
,	,	kIx,	,
činit	činit	k5eAaImF	činit
pravdivým	pravdivý	k2eAgFnPc3d1	pravdivá
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ověřování	ověřování	k1gNnSc1	ověřování
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
pravdivosti	pravdivost	k1gFnSc2	pravdivost
výroku	výrok	k1gInSc2	výrok
<g/>
,	,	kIx,	,
hypotézy	hypotéza	k1gFnSc2	hypotéza
<g/>
,	,	kIx,	,
argumentu	argument	k1gInSc2	argument
<g/>
,	,	kIx,	,
logického	logický	k2eAgInSc2d1	logický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
funkce	funkce	k1gFnSc1	funkce
přístroje	přístroj	k1gInSc2	přístroj
konfrontací	konfrontace	k1gFnPc2	konfrontace
s	s	k7c7	s
fakty	fakt	k1gInPc7	fakt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ověřování	ověřování	k1gNnSc1	ověřování
platnosti	platnost	k1gFnSc2	platnost
úsudku	úsudek	k1gInSc2	úsudek
formální	formální	k2eAgNnSc4d1	formální
analýzou	analýza	k1gFnSc7	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgFnSc1d1	experimentální
verifikace	verifikace	k1gFnSc1	verifikace
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
dokázat	dokázat	k5eAaPmF	dokázat
platnost	platnost	k1gFnSc4	platnost
nějaké	nějaký	k3yIgFnSc2	nějaký
hypotézy	hypotéza	k1gFnSc2	hypotéza
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInPc1	její
důsledky	důsledek	k1gInPc1	důsledek
srovnávají	srovnávat	k5eAaImIp3nP	srovnávat
se	s	k7c7	s
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Soudobá	soudobý	k2eAgFnSc1d1	soudobá
teorie	teorie	k1gFnSc1	teorie
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
Karl	Karl	k1gMnSc1	Karl
Popper	Popper	k1gMnSc1	Popper
<g/>
)	)	kIx)	)
nahlíží	nahlížet	k5eAaImIp3nS	nahlížet
obtížnost	obtížnost	k1gFnSc1	obtížnost
verifikace	verifikace	k1gFnSc2	verifikace
a	a	k8xC	a
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
spíš	spíš	k9	spíš
možnost	možnost	k1gFnSc4	možnost
hypotézy	hypotéza	k1gFnSc2	hypotéza
vyvracet	vyvracet	k5eAaImF	vyvracet
(	(	kIx(	(
<g/>
falzifikovat	falzifikovat	k5eAaImF	falzifikovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgFnSc1d1	formální
verifikace	verifikace	k1gFnSc1	verifikace
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pomocí	pomocí	k7c2	pomocí
logických	logický	k2eAgFnPc2d1	logická
operací	operace	k1gFnPc2	operace
ověří	ověřit	k5eAaPmIp3nP	ověřit
shodu	shoda	k1gFnSc4	shoda
s	s	k7c7	s
přijatými	přijatý	k2eAgInPc7d1	přijatý
axiomy	axiom	k1gInPc7	axiom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
počítačových	počítačový	k2eAgInPc2d1	počítačový
systémů	systém	k1gInPc2	systém
formální	formální	k2eAgFnSc2d1	formální
verifikace	verifikace	k1gFnSc2	verifikace
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
nebo	nebo	k8xC	nebo
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
správnost	správnost	k1gFnSc4	správnost
systému	systém	k1gInSc2	systém
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
formální	formální	k2eAgFnSc3d1	formální
specifikaci	specifikace	k1gFnSc3	specifikace
nebo	nebo	k8xC	nebo
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
použitím	použití	k1gNnSc7	použití
matematických	matematický	k2eAgFnPc2d1	matematická
formálních	formální	k2eAgFnPc2d1	formální
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
verifikace	verifikace	k1gFnSc2	verifikace
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
novopozitivistů	novopozitivista	k1gMnPc2	novopozitivista
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
filozofický	filozofický	k2eAgInSc1d1	filozofický
směr	směr	k1gInSc1	směr
volně	volně	k6eAd1	volně
vyústil	vyústit	k5eAaPmAgInS	vyústit
z	z	k7c2	z
Vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
kroužku	kroužek	k1gInSc2	kroužek
(	(	kIx(	(
<g/>
Schlick	Schlick	k1gMnSc1	Schlick
<g/>
,	,	kIx,	,
Carnap	Carnap	k1gMnSc1	Carnap
<g/>
,	,	kIx,	,
Reichenbach	Reichenbach	k1gMnSc1	Reichenbach
<g/>
,	,	kIx,	,
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
se	se	k3xPyFc4	se
novopozitivismem	novopozitivismus	k1gInSc7	novopozitivismus
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
filozofii	filozofie	k1gFnSc4	filozofie
A.	A.	kA	A.
Comtea	Comteus	k1gMnSc2	Comteus
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
sjednoceného	sjednocený	k2eAgInSc2d1	sjednocený
vědeckého	vědecký	k2eAgInSc2d1	vědecký
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c6	na
základě	základ	k1gInSc6	základ
symbolické	symbolický	k2eAgFnSc2d1	symbolická
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
pravého	pravý	k2eAgInSc2d1	pravý
jazyka	jazyk	k1gInSc2	jazyk
společného	společný	k2eAgInSc2d1	společný
všem	všecek	k3xTgFnPc3	všecek
vědám	věda	k1gFnPc3	věda
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
možnost	možnost	k1gFnSc4	možnost
univerzální	univerzální	k2eAgFnSc2d1	univerzální
báze	báze	k1gFnSc2	báze
všech	všecek	k3xTgInPc2	všecek
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
umožnila	umožnit	k5eAaPmAgFnS	umožnit
verifikaci	verifikace	k1gFnSc4	verifikace
každé	každý	k3xTgFnSc2	každý
výpovědi	výpověď	k1gFnSc2	výpověď
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
naleznou	naleznout	k5eAaPmIp3nP	naleznout
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
spolehlivě	spolehlivě	k6eAd1	spolehlivě
prokázala	prokázat	k5eAaPmAgFnS	prokázat
smysluplnost	smysluplnost	k1gFnSc1	smysluplnost
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
výroku	výrok	k1gInSc2	výrok
<g/>
,	,	kIx,	,
zmizí	zmizet	k5eAaPmIp3nS	zmizet
mnoho	mnoho	k4c4	mnoho
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vážných	vážný	k2eAgInPc2d1	vážný
problémů	problém	k1gInPc2	problém
tradiční	tradiční	k2eAgFnSc2d1	tradiční
filozofie	filozofie	k1gFnSc2	filozofie
(	(	kIx(	(
<g/>
prokáže	prokázat	k5eAaPmIp3nS	prokázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
výroky	výrok	k1gInPc1	výrok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
vyjádřeny	vyjádřen	k2eAgInPc1d1	vyjádřen
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
smysl	smysl	k1gInSc4	smysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
novopozitivismus	novopozitivismus	k1gInSc1	novopozitivismus
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
první	první	k4xOgFnSc4	první
fázi	fáze	k1gFnSc4	fáze
mohutného	mohutný	k2eAgInSc2d1	mohutný
proudu	proud	k1gInSc2	proud
tzv.	tzv.	kA	tzv.
analytické	analytický	k2eAgFnSc2d1	analytická
filozofie	filozofie	k1gFnSc2	filozofie
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
filozofie	filozofie	k1gFnSc1	filozofie
jazyka	jazyk	k1gInSc2	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
filozofické	filozofický	k2eAgFnPc4d1	filozofická
diskuse	diskuse	k1gFnPc4	diskuse
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
podvod	podvod	k1gInSc1	podvod
</s>
