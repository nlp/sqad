<s>
Verifikace	verifikace	k1gFnSc1	verifikace
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
verum	verum	k1gNnSc4	verum
facere	facrat	k5eAaPmIp3nS	facrat
<g/>
,	,	kIx,	,
činit	činit	k5eAaImF	činit
pravdivým	pravdivý	k2eAgFnPc3d1	pravdivá
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ověřování	ověřování	k1gNnSc1	ověřování
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
pravdivosti	pravdivost	k1gFnSc2	pravdivost
výroku	výrok	k1gInSc2	výrok
<g/>
,	,	kIx,	,
hypotézy	hypotéza	k1gFnSc2	hypotéza
<g/>
,	,	kIx,	,
argumentu	argument	k1gInSc2	argument
<g/>
,	,	kIx,	,
logického	logický	k2eAgInSc2d1	logický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
funkce	funkce	k1gFnSc1	funkce
přístroje	přístroj	k1gInSc2	přístroj
konfrontací	konfrontace	k1gFnPc2	konfrontace
s	s	k7c7	s
fakty	fakt	k1gInPc7	fakt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ověřování	ověřování	k1gNnSc1	ověřování
platnosti	platnost	k1gFnSc2	platnost
úsudku	úsudek	k1gInSc2	úsudek
formální	formální	k2eAgNnSc4d1	formální
analýzou	analýza	k1gFnSc7	analýza
<g/>
.	.	kIx.	.
</s>
