<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
atmos	atmos	k1gInSc1	atmos
–	–	k?	–
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
sphaira	sphaira	k1gFnSc1	sphaira
–	–	k?	–
koule	koule	k1gFnSc2	koule
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc4d1	plynný
obal	obal	k1gInSc4	obal
tělesa	těleso	k1gNnSc2	těleso
v	v	k7c6	v
kosmickém	kosmický	k2eAgInSc6d1	kosmický
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obklopeno	obklopen	k2eAgNnSc1d1	obklopeno
atmosférou	atmosféra	k1gFnSc7	atmosféra
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
hmotnost	hmotnost	k1gFnSc4	hmotnost
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
plyn	plyn	k1gInSc4	plyn
vázalo	vázat	k5eAaImAgNnS	vázat
gravitační	gravitační	k2eAgFnSc7d1	gravitační
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
některých	některý	k3yIgFnPc2	některý
plynných	plynný	k2eAgFnPc2d1	plynná
sloučenin	sloučenina	k1gFnPc2	sloučenina
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
splněna	splnit	k5eAaPmNgFnS	splnit
i	i	k9	i
další	další	k2eAgFnSc1d1	další
podmínka	podmínka	k1gFnSc1	podmínka
–	–	k?	–
dostatečně	dostatečně	k6eAd1	dostatečně
nízká	nízký	k2eAgFnSc1d1	nízká
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféru	atmosféra	k1gFnSc4	atmosféra
Země	zem	k1gFnSc2	zem
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
21	[number]	k4	21
%	%	kIx~	%
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
ze	z	k7c2	z
78	[number]	k4	78
%	%	kIx~	%
dusík	dusík	k1gInSc4	dusík
a	a	k8xC	a
1	[number]	k4	1
%	%	kIx~	%
zabírají	zabírat	k5eAaImIp3nP	zabírat
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
plynu	plyn	k1gInSc2	plyn
je	být	k5eAaImIp3nS	být
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Lehčí	lehký	k2eAgInSc1d2	lehčí
plyn	plyn	k1gInSc1	plyn
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vodík	vodík	k1gInSc1	vodík
<g/>
)	)	kIx)	)
neunikne	uniknout	k5eNaPmIp3nS	uniknout
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vázán	vázat	k5eAaImNgInS	vázat
vyšší	vysoký	k2eAgFnSc7d2	vyšší
gravitační	gravitační	k2eAgFnSc7d1	gravitační
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
ochranou	ochrana	k1gFnSc7	ochrana
života	život	k1gInSc2	život
před	před	k7c7	před
kosmickým	kosmický	k2eAgNnSc7d1	kosmické
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gInSc1	Merkur
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
tenkou	tenký	k2eAgFnSc4d1	tenká
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
atomů	atom	k1gInPc2	atom
vyražených	vyražený	k2eAgInPc2d1	vyražený
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
povrchu	povrch	k1gInSc2	povrch
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zapříčiněno	zapříčinit	k5eAaPmNgNnS	zapříčinit
slabým	slabý	k2eAgNnSc7d1	slabé
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
polem	pole	k1gNnSc7	pole
vytvářeným	vytvářený	k2eAgNnSc7d1	vytvářené
poměrně	poměrně	k6eAd1	poměrně
lehkou	lehký	k2eAgFnSc7d1	lehká
planetou	planeta	k1gFnSc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
Merkura	Merkur	k1gMnSc2	Merkur
velmi	velmi	k6eAd1	velmi
horký	horký	k2eAgInSc1d1	horký
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
atomy	atom	k1gInPc1	atom
rychle	rychle	k6eAd1	rychle
unikají	unikat	k5eAaImIp3nP	unikat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
oproti	oproti	k7c3	oproti
Zemi	zem	k1gFnSc3	zem
nebo	nebo	k8xC	nebo
Venuši	Venuše	k1gFnSc6	Venuše
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
atmosféry	atmosféra	k1gFnPc1	atmosféra
jsou	být	k5eAaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
,	,	kIx,	,
Merkurova	Merkurův	k2eAgFnSc1d1	Merkurova
atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
neustále	neustále	k6eAd1	neustále
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
5,10	[number]	k4	5,10
<g/>
−	−	k?	−
<g/>
10	[number]	k4	10
Pa	Pa	kA	Pa
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
pozemských	pozemský	k2eAgNnPc6d1	pozemské
měřítkách	měřítko	k1gNnPc6	měřítko
ultravysoké	ultravysoký	k2eAgNnSc1d1	ultravysoké
vakuum	vakuum	k1gNnSc1	vakuum
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
vyšší	vysoký	k2eAgInSc1d2	vyšší
tlak	tlak	k1gInSc1	tlak
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
vakuum	vakuum	k1gNnSc1	vakuum
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
žárovce	žárovka	k1gFnSc6	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
atmosféry	atmosféra	k1gFnSc2	atmosféra
Venuše	Venuše	k1gFnSc2	Venuše
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
měřeních	měření	k1gNnPc6	měření
uskutečněných	uskutečněný	k2eAgFnPc2d1	uskutečněná
sondami	sonda	k1gFnPc7	sonda
typu	typ	k1gInSc2	typ
Veněra	Veněra	k1gFnSc1	Veněra
<g/>
,	,	kIx,	,
Mariner	Mariner	k1gInSc1	Mariner
<g/>
,	,	kIx,	,
Pioneer-Venus	Pioneer-Venus	k1gInSc1	Pioneer-Venus
<g/>
,	,	kIx,	,
pozemskými	pozemský	k2eAgNnPc7d1	pozemské
pozorováními	pozorování	k1gNnPc7	pozorování
a	a	k8xC	a
teoretickými	teoretický	k2eAgInPc7d1	teoretický
modely	model	k1gInPc7	model
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
odhadovat	odhadovat	k5eAaImF	odhadovat
chování	chování	k1gNnSc4	chování
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
hustou	hustý	k2eAgFnSc7d1	hustá
vrstvou	vrstva	k1gFnSc7	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořená	tvořený	k2eAgFnSc1d1	tvořená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
těchto	tento	k3xDgInPc2	tento
plynů	plyn	k1gInPc2	plyn
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
silného	silný	k2eAgInSc2d1	silný
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
teplotu	teplota	k1gFnSc4	teplota
povrchu	povrch	k1gInSc2	povrch
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
okolo	okolo	k7c2	okolo
rovníku	rovník	k1gInSc2	rovník
dokonce	dokonce	k9	dokonce
až	až	k9	až
o	o	k7c4	o
500	[number]	k4	500
°	°	k?	°
<g/>
C.	C.	kA	C.
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
78	[number]	k4	78
%	%	kIx~	%
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
21	[number]	k4	21
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
argonu	argon	k1gInSc2	argon
a	a	k8xC	a
stopového	stopový	k2eAgNnSc2d1	stopové
množství	množství	k1gNnSc2	množství
jiných	jiný	k2eAgInPc2d1	jiný
plynů	plyn	k1gInPc2	plyn
včetně	včetně	k7c2	včetně
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
chrání	chránit	k5eAaImIp3nS	chránit
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
před	před	k7c7	před
dopadem	dopad	k1gInSc7	dopad
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
složení	složení	k1gNnSc1	složení
je	být	k5eAaImIp3nS	být
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
silně	silně	k6eAd1	silně
ovlivněno	ovlivněn	k2eAgNnSc4d1	ovlivněno
biosférou	biosféra	k1gFnSc7	biosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
volného	volný	k2eAgInSc2d1	volný
dvouatomového	dvouatomový	k2eAgInSc2d1	dvouatomový
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
pozemské	pozemský	k2eAgFnPc1d1	pozemská
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
bez	bez	k7c2	bez
nichž	jenž	k3xRgFnPc2	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
geologicky	geologicky	k6eAd1	geologicky
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
sloučil	sloučit	k5eAaPmAgMnS	sloučit
s	s	k7c7	s
materiály	materiál	k1gInPc7	materiál
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
kyslík	kyslík	k1gInSc1	kyslík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
je	být	k5eAaImIp3nS	být
známkou	známka	k1gFnSc7	známka
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféru	atmosféra	k1gFnSc4	atmosféra
Země	zem	k1gFnSc2	zem
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
:	:	kIx,	:
Troposféra	troposféra	k1gFnSc1	troposféra
–	–	k?	–
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
naše	náš	k3xOp1gFnSc1	náš
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
,	,	kIx,	,
a	a	k8xC	a
veškerý	veškerý	k3xTgInSc1	veškerý
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
život	život	k1gInSc1	život
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
Stratosféra	stratosféra	k1gFnSc1	stratosféra
–	–	k?	–
stratosféra	stratosféra	k1gFnSc1	stratosféra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nad	nad	k7c7	nad
troposférou	troposféra	k1gFnSc7	troposféra
<g/>
,	,	kIx,	,
od	od	k7c2	od
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
km	km	kA	km
až	až	k6eAd1	až
do	do	k7c2	do
50	[number]	k4	50
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
stoupá	stoupat	k5eAaImIp3nS	stoupat
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Mezosféra	Mezosféra	k1gFnSc1	Mezosféra
–	–	k?	–
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
50	[number]	k4	50
km	km	kA	km
až	až	k6eAd1	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
km	km	kA	km
<g/>
.	.	kIx.	.
<g/>
Teplota	teplota	k1gFnSc1	teplota
zde	zde	k6eAd1	zde
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
naopak	naopak	k6eAd1	naopak
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Termosféra	Termosféra	k1gFnSc1	Termosféra
–	–	k?	–
největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
termosféra	termosféra	k1gFnSc1	termosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
km	km	kA	km
až	až	k6eAd1	až
do	do	k7c2	do
500-1000	[number]	k4	500-1000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
zde	zde	k6eAd1	zde
s	s	k7c7	s
narůstající	narůstající	k2eAgFnSc7d1	narůstající
výškou	výška	k1gFnSc7	výška
opět	opět	k6eAd1	opět
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
výšek	výška	k1gFnPc2	výška
nad	nad	k7c7	nad
100	[number]	k4	100
km	km	kA	km
se	se	k3xPyFc4	se
utváří	utvářet	k5eAaImIp3nS	utvářet
např.	např.	kA	např.
polární	polární	k2eAgFnSc1d1	polární
záře	záře	k1gFnSc1	záře
<g/>
.	.	kIx.	.
</s>
<s>
Exosféra	exosféra	k1gFnSc1	exosféra
–	–	k?	–
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
sahající	sahající	k2eAgFnSc1d1	sahající
do	do	k7c2	do
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plynule	plynule	k6eAd1	plynule
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
meziplanetární	meziplanetární	k2eAgInSc4d1	meziplanetární
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k9	již
gravitace	gravitace	k1gFnSc1	gravitace
Země	zem	k1gFnSc2	zem
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
molekuly	molekula	k1gFnPc1	molekula
plynu	plyn	k1gInSc2	plyn
trvaleji	trvale	k6eAd2	trvale
udržet	udržet	k5eAaPmF	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
řídkou	řídký	k2eAgFnSc4d1	řídká
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zadržovat	zadržovat	k5eAaImF	zadržovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
výměnu	výměna	k1gFnSc4	výměna
mezi	mezi	k7c7	mezi
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
okolním	okolní	k2eAgInSc7d1	okolní
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
velké	velký	k2eAgInPc4d1	velký
tepelné	tepelný	k2eAgInPc4d1	tepelný
rozdíly	rozdíl	k1gInPc4	rozdíl
během	během	k7c2	během
dne	den	k1gInSc2	den
a	a	k8xC	a
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
600	[number]	k4	600
až	až	k9	až
1000	[number]	k4	1000
Pa	Pa	kA	Pa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
až	až	k9	až
150	[number]	k4	150
<g/>
krát	krát	k6eAd1	krát
méně	málo	k6eAd2	málo
než	než	k8xS	než
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
či	či	k8xC	či
jako	jako	k9	jako
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
30	[number]	k4	30
km	km	kA	km
nad	nad	k7c7	nad
jejím	její	k3xOp3gInSc7	její
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
sezónních	sezónní	k2eAgInPc6d1	sezónní
výkyvech	výkyv	k1gInPc6	výkyv
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
a	a	k8xC	a
oddaluje	oddalovat	k5eAaImIp3nS	oddalovat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
zmrzne	zmrznout	k5eAaPmIp3nS	zmrznout
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
opět	opět	k6eAd1	opět
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
(	(	kIx(	(
<g/>
95,32	[number]	k4	95,32
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
:	:	kIx,	:
dusík	dusík	k1gInSc1	dusík
(	(	kIx(	(
<g/>
2,7	[number]	k4	2,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
argon	argon	k1gInSc1	argon
(	(	kIx(	(
<g/>
1,6	[number]	k4	1,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
(	(	kIx(	(
<g/>
0,13	[number]	k4	0,13
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
(	(	kIx(	(
<g/>
0,07	[number]	k4	0,07
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
(	(	kIx(	(
<g/>
0,03	[number]	k4	0,03
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
sublimací	sublimace	k1gFnPc2	sublimace
z	z	k7c2	z
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgInPc4d1	ostatní
plyny	plyn	k1gInPc4	plyn
vyskytující	vyskytující	k2eAgInPc4d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
řadí	řadit	k5eAaImIp3nS	řadit
neon	neon	k1gInSc1	neon
<g/>
,	,	kIx,	,
krypton	krypton	k1gInSc1	krypton
<g/>
,	,	kIx,	,
xenon	xenon	k1gInSc1	xenon
<g/>
,	,	kIx,	,
ozón	ozón	k1gInSc1	ozón
a	a	k8xC	a
metan	metan	k1gInSc1	metan
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
možným	možný	k2eAgInSc7d1	možný
indikátorem	indikátor	k1gInSc7	indikátor
života	život	k1gInSc2	život
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podléhá	podléhat	k5eAaImIp3nS	podléhat
rychlému	rychlý	k2eAgInSc3d1	rychlý
rozpadu	rozpad	k1gInSc3	rozpad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
