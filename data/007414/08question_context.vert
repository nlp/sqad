<s>
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
<g/>
,	,	kIx,	,
CBE	CBE	kA	CBE
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1892	[number]	k4	1892
Bloemfontein	Bloemfontein	k1gInSc1	Bloemfontein
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1973	[number]	k4	1973
Bournemouth	Bournemoutha	k1gFnPc2	Bournemoutha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgMnSc1d3	nejznámější
jako	jako	k8xS	jako
autor	autor	k1gMnSc1	autor
Hobita	hobit	k1gMnSc2	hobit
a	a	k8xC	a
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	let	k1gInPc6	let
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
staré	starý	k2eAgFnSc2d1	stará
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>

