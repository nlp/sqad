<s>
Na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	let	k1gInPc6	let
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
staré	starý	k2eAgFnSc2d1	stará
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
profesor	profesor	k1gMnSc1	profesor
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
