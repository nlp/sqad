<p>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
pozměňující	pozměňující	k2eAgFnSc4d1	pozměňující
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
a	a	k8xC	a
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
přijetí	přijetí	k1gNnSc2	přijetí
též	též	k6eAd1	též
Reformní	reformní	k2eAgFnSc1d1	reformní
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
především	především	k6eAd1	především
reformovat	reformovat	k5eAaBmF	reformovat
instituce	instituce	k1gFnPc4	instituce
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
fungování	fungování	k1gNnSc4	fungování
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
<g/>
,	,	kIx,	,
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
byla	být	k5eAaImAgFnS	být
ratifikována	ratifikován	k2eAgFnSc1d1	ratifikována
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Ústavě	ústava	k1gFnSc6	ústava
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Ústavy	ústava	k1gFnSc2	ústava
EU	EU	kA	EU
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
euroústavy	euroústava	k1gFnPc4	euroústava
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odmítnuté	odmítnutý	k2eAgFnSc2d1	odmítnutá
referendy	referendum	k1gNnPc7	referendum
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přikročeno	přikročen	k2eAgNnSc1d1	přikročeno
k	k	k7c3	k
změnám	změna	k1gFnPc3	změna
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
novelizace	novelizace	k1gFnSc2	novelizace
stávajících	stávající	k2eAgFnPc2d1	stávající
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zajistit	zajistit	k5eAaPmF	zajistit
efektivní	efektivní	k2eAgNnSc4d1	efektivní
fungování	fungování	k1gNnSc4	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
ratifikována	ratifikovat	k5eAaBmNgFnS	ratifikovat
všemi	všecek	k3xTgInPc7	všecek
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
EU	EU	kA	EU
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
nabyla	nabýt	k5eAaPmAgFnS	nabýt
účinnosti	účinnost	k1gFnPc4	účinnost
–	–	k?	–
první	první	k4xOgFnSc6	první
den	den	k1gInSc1	den
měsíce	měsíc	k1gInSc2	měsíc
následujícího	následující	k2eAgInSc2d1	následující
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
uložila	uložit	k5eAaPmAgFnS	uložit
svou	svůj	k3xOyFgFnSc4	svůj
ratifikaci	ratifikace	k1gFnSc4	ratifikace
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Ústavě	ústava	k1gFnSc6	ústava
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
–	–	k?	–
která	který	k3yRgFnSc1	který
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
smlouvy	smlouva	k1gFnSc2	smlouva
měla	mít	k5eAaImAgFnS	mít
nahradit	nahradit	k5eAaPmF	nahradit
–	–	k?	–
novelizovala	novelizovat	k5eAaBmAgFnS	novelizovat
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
stávající	stávající	k2eAgFnSc2d1	stávající
evropské	evropský	k2eAgFnSc2d1	Evropská
zakládající	zakládající	k2eAgFnSc2d1	zakládající
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
pozměněné	pozměněný	k2eAgFnSc6d1	pozměněná
podobě	podoba	k1gFnSc6	podoba
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
změn	změna	k1gFnPc2	změna
obsažených	obsažený	k2eAgFnPc2d1	obsažená
v	v	k7c6	v
odmítnuté	odmítnutý	k2eAgFnSc6d1	odmítnutá
Smlouvě	smlouva	k1gFnSc6	smlouva
o	o	k7c6	o
Ústavě	ústava	k1gFnSc6	ústava
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
však	však	k9	však
přejímá	přejímat	k5eAaImIp3nS	přejímat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obsah	obsah	k1gInSc1	obsah
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
byl	být	k5eAaImAgInS	být
vypracován	vypracovat	k5eAaPmNgMnS	vypracovat
v	v	k7c6	v
bilaterálních	bilaterální	k2eAgNnPc6d1	bilaterální
jednáních	jednání	k1gNnPc6	jednání
během	během	k7c2	během
německého	německý	k2eAgNnSc2d1	německé
předsednictví	předsednictví	k1gNnSc2	předsednictví
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
dohodnuty	dohodnout	k5eAaPmNgFnP	dohodnout
a	a	k8xC	a
schváleny	schválit	k5eAaPmNgFnP	schválit
na	na	k7c4	na
zasedání	zasedání	k1gNnSc4	zasedání
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
21	[number]	k4	21
<g/>
.	.	kIx.	.
až	až	k9	až
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
prodlouženého	prodloužený	k2eAgInSc2d1	prodloužený
do	do	k7c2	do
ranních	ranní	k2eAgFnPc2d1	ranní
hodin	hodina	k1gFnPc2	hodina
soboty	sobota	k1gFnSc2	sobota
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
rada	rada	k1gFnSc1	rada
dále	daleko	k6eAd2	daleko
udělila	udělit	k5eAaPmAgFnS	udělit
mandát	mandát	k1gInSc4	mandát
Mezivládní	mezivládní	k2eAgFnSc4d1	mezivládní
konferenci	konference	k1gFnSc4	konference
k	k	k7c3	k
vypracování	vypracování	k1gNnSc3	vypracování
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
ratifikována	ratifikovat	k5eAaBmNgFnS	ratifikovat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
a	a	k8xC	a
Ústava	ústava	k1gFnSc1	ústava
EU	EU	kA	EU
==	==	k?	==
</s>
</p>
<p>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
články	článek	k1gInPc4	článek
pozměňující	pozměňující	k2eAgMnSc1d1	pozměňující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
</s>
</p>
<p>
<s>
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
nově	nově	k6eAd1	nově
na	na	k7c4	na
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unieEvropská	unieEvropský	k2eAgFnSc1d1	unieEvropský
unie	unie	k1gFnSc1	unie
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
právní	právní	k2eAgFnSc4d1	právní
subjektivitu	subjektivita	k1gFnSc4	subjektivita
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
už	už	k6eAd1	už
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
Ústava	ústava	k1gFnSc1	ústava
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Listina	listina	k1gFnSc1	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
sice	sice	k8xC	sice
není	být	k5eNaImIp3nS	být
částí	část	k1gFnSc7	část
zakládajících	zakládající	k2eAgFnPc2d1	zakládající
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nového	nový	k2eAgNnSc2d1	nové
znění	znění	k1gNnSc2	znění
čl	čl	kA	čl
<g/>
.	.	kIx.	.
6	[number]	k4	6
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
"	"	kIx"	"
<g/>
stejnou	stejný	k2eAgFnSc4d1	stejná
právní	právní	k2eAgFnSc4d1	právní
sílu	síla	k1gFnSc4	síla
jako	jako	k8xC	jako
Smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
právní	právní	k2eAgFnSc1d1	právní
závaznost	závaznost	k1gFnSc1	závaznost
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
možná	možný	k2eAgNnPc1d1	možné
i	i	k8xC	i
Česka	Česko	k1gNnPc1	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
opouští	opouštět	k5eAaImIp3nS	opouštět
systém	systém	k1gInSc4	systém
trojí	trojí	k4xRgFnSc2	trojí
většiny	většina	k1gFnSc2	většina
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
(	(	kIx(	(
<g/>
definované	definovaný	k2eAgInPc1d1	definovaný
počtem	počet	k1gInSc7	počet
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc7	jejich
váženými	vážený	k2eAgInPc7d1	vážený
hlasy	hlas	k1gInPc7	hlas
a	a	k8xC	a
podílem	podíl	k1gInSc7	podíl
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
EU	EU	kA	EU
představují	představovat	k5eAaImIp3nP	představovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
zavádí	zavádět	k5eAaImIp3nS	zavádět
systém	systém	k1gInSc4	systém
dvojí	dvojí	k4xRgFnSc2	dvojí
většiny	většina	k1gFnSc2	většina
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
opouští	opouštět	k5eAaImIp3nS	opouštět
kritérium	kritérium	k1gNnSc4	kritérium
vážených	vážený	k2eAgInPc2d1	vážený
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
do	do	k7c2	do
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
však	však	k9	však
mohl	moct	k5eAaImAgInS	moct
kterýkoliv	kterýkoliv	k3yIgInSc1	kterýkoliv
ze	z	k7c2	z
států	stát	k1gInPc2	stát
požádat	požádat	k5eAaPmF	požádat
o	o	k7c6	o
hlasování	hlasování	k1gNnSc6	hlasování
starým	starý	k2eAgInSc7d1	starý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
kompromis	kompromis	k1gInSc1	kompromis
–	–	k?	–
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
zprvu	zprvu	k6eAd1	zprvu
zcela	zcela	k6eAd1	zcela
odmítala	odmítat	k5eAaImAgFnS	odmítat
Evropského	evropský	k2eAgMnSc4d1	evropský
ministra	ministr	k1gMnSc4	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
navrženého	navržený	k2eAgNnSc2d1	navržené
Ústavou	ústava	k1gFnSc7	ústava
EU	EU	kA	EU
–	–	k?	–
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
funkce	funkce	k1gFnSc2	funkce
"	"	kIx"	"
<g/>
Vysoký	vysoký	k2eAgMnSc1d1	vysoký
představitel	představitel	k1gMnSc1	představitel
Unie	unie	k1gFnSc2	unie
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
politiku	politika	k1gFnSc4	politika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
má	mít	k5eAaImIp3nS	mít
smlouva	smlouva	k1gFnSc1	smlouva
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
policejní	policejní	k2eAgFnSc2d1	policejní
a	a	k8xC	a
justiční	justiční	k2eAgFnSc2d1	justiční
spolupráce	spolupráce	k1gFnSc2	spolupráce
v	v	k7c6	v
trestních	trestní	k2eAgFnPc6d1	trestní
věcech	věc	k1gFnPc6	věc
některým	některý	k3yIgMnPc3	některý
členům	člen	k1gMnPc3	člen
umožnit	umožnit	k5eAaPmF	umožnit
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
určitém	určitý	k2eAgInSc6d1	určitý
aktu	akt	k1gInSc6	akt
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiným	jiný	k1gMnPc3	jiný
umožní	umožnit	k5eAaPmIp3nS	umožnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
neúčastnit	účastnit	k5eNaImF	účastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
EU	EU	kA	EU
a	a	k8xC	a
"	"	kIx"	"
<g/>
rámcový	rámcový	k2eAgInSc1d1	rámcový
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
EU	EU	kA	EU
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
nařízení	nařízení	k1gNnSc1	nařízení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
směrnice	směrnice	k1gFnSc1	směrnice
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Ústavy	ústava	k1gFnSc2	ústava
EU	EU	kA	EU
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
žádnou	žádný	k3yNgFnSc4	žádný
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
ústavních	ústavní	k2eAgInPc6d1	ústavní
symbolech	symbol	k1gInPc6	symbol
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vlajka	vlajka	k1gFnSc1	vlajka
nebo	nebo	k8xC	nebo
hymna	hymna	k1gFnSc1	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Vynechán	vynechat	k5eAaPmNgInS	vynechat
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
Ústava	ústava	k1gFnSc1	ústava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
ratifikace	ratifikace	k1gFnPc4	ratifikace
-	-	kIx~	-
poslední	poslední	k2eAgInPc4d1	poslední
a	a	k8xC	a
specifické	specifický	k2eAgInPc4d1	specifický
případy	případ	k1gInPc4	případ
==	==	k?	==
</s>
</p>
<p>
<s>
Ratifikace	ratifikace	k1gFnSc1	ratifikace
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
Evropské	evropský	k2eAgFnSc2d1	Evropská
Unie	unie	k1gFnSc2	unie
končí	končit	k5eAaImIp3nS	končit
uložením	uložení	k1gNnSc7	uložení
příslušně	příslušně	k6eAd1	příslušně
podepsaných	podepsaný	k2eAgFnPc2d1	podepsaná
listin	listina	k1gFnPc2	listina
u	u	k7c2	u
italské	italský	k2eAgFnSc2d1	italská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
moment	moment	k1gInSc4	moment
je	být	k5eAaImIp3nS	být
smlouva	smlouva	k1gFnSc1	smlouva
danou	daný	k2eAgFnSc7d1	daná
zemí	zem	k1gFnSc7	zem
ratifikována	ratifikovat	k5eAaBmNgFnS	ratifikovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Irsko	Irsko	k1gNnSc1	Irsko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
dvě	dva	k4xCgNnPc4	dva
referenda	referendum	k1gNnPc4	referendum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
referendu	referendum	k1gNnSc6	referendum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
následně	následně	k6eAd1	následně
získalo	získat	k5eAaPmAgNnS	získat
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
záruky	záruka	k1gFnSc2	záruka
a	a	k8xC	a
vypsalo	vypsat	k5eAaPmAgNnS	vypsat
druhé	druhý	k4xOgNnSc1	druhý
referendum	referendum	k1gNnSc1	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
referendum	referendum	k1gNnSc1	referendum
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
67,1	[number]	k4	67,1
%	%	kIx~	%
Irů	Ir	k1gMnPc2	Ir
pro	pro	k7c4	pro
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
bylo	být	k5eAaImAgNnS	být
32,9	[number]	k4	32,9
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Voleb	volba	k1gFnPc2	volba
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
58	[number]	k4	58
%	%	kIx~	%
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
prezident	prezident	k1gMnSc1	prezident
Lech	Lech	k1gMnSc1	Lech
Kaczyński	Kaczyńsk	k1gMnSc3	Kaczyńsk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
původně	původně	k6eAd1	původně
vymínil	vymínit	k5eAaPmAgMnS	vymínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
smlouvu	smlouva	k1gFnSc4	smlouva
podepíše	podepsat	k5eAaPmIp3nS	podepsat
až	až	k9	až
po	po	k7c6	po
irském	irský	k2eAgNnSc6d1	irské
referendu	referendum	k1gNnSc6	referendum
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
podepsal	podepsat	k5eAaPmAgMnS	podepsat
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
===	===	k?	===
Průběh	průběh	k1gInSc4	průběh
ratifikace	ratifikace	k1gFnSc2	ratifikace
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
===	===	k?	===
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
že	že	k8xS	že
žalované	žalovaný	k2eAgFnPc1d1	žalovaná
části	část	k1gFnPc1	část
smlouvy	smlouva	k1gFnSc2	smlouva
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
dala	dát	k5eAaPmAgFnS	dát
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
ratifikací	ratifikace	k1gFnSc7	ratifikace
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
dal	dát	k5eAaPmAgInS	dát
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
ratifikací	ratifikace	k1gFnSc7	ratifikace
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
Senát	senát	k1gInSc1	senát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
skupina	skupina	k1gFnSc1	skupina
senátorů	senátor	k1gMnPc2	senátor
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
ODS	ODS	kA	ODS
předložila	předložit	k5eAaPmAgFnS	předložit
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
další	další	k2eAgInSc1d1	další
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
přezkoumání	přezkoumání	k1gNnSc4	přezkoumání
slučitelnosti	slučitelnost	k1gFnSc2	slučitelnost
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
ústavou	ústava	k1gFnSc7	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
pro	pro	k7c4	pro
podpis	podpis	k1gInSc4	podpis
ratifikace	ratifikace	k1gFnSc2	ratifikace
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
výjimku	výjimka	k1gFnSc4	výjimka
(	(	kIx(	(
<g/>
formou	forma	k1gFnSc7	forma
dodatku	dodatek	k1gInSc2	dodatek
<g/>
)	)	kIx)	)
z	z	k7c2	z
Listiny	listina	k1gFnSc2	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
EU	EU	kA	EU
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
zaručí	zaručit	k5eAaPmIp3nS	zaručit
neprolomení	neprolomení	k1gNnSc1	neprolomení
Benešových	Benešových	k2eAgInPc2d1	Benešových
dekretů	dekret	k1gInPc2	dekret
<g/>
.	.	kIx.	.
</s>
<s>
Bernd	Bernd	k1gMnSc1	Bernd
Posselt	Posselt	k1gMnSc1	Posselt
tuto	tento	k3xDgFnSc4	tento
aktivitu	aktivita	k1gFnSc4	aktivita
přivítal	přivítat	k5eAaPmAgMnS	přivítat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Klaus	Klaus	k1gMnSc1	Klaus
naší	náš	k3xOp1gFnSc2	náš
věci	věc	k1gFnSc2	věc
pomohl	pomoct	k5eAaPmAgMnS	pomoct
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
o	o	k7c6	o
nás	my	k3xPp1nPc6	my
<g/>
,	,	kIx,	,
sudetských	sudetský	k2eAgMnPc6d1	sudetský
Němcích	Němec	k1gMnPc6	Němec
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
rasistických	rasistický	k2eAgInPc6d1	rasistický
Benešových	Benešových	k2eAgInPc6d1	Benešových
dekretech	dekret	k1gInPc6	dekret
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Požadavku	požadavek	k1gInSc2	požadavek
ostatní	ostatní	k2eAgInPc1d1	ostatní
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
vyhověly	vyhovět	k5eAaPmAgInP	vyhovět
tím	ten	k3xDgMnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přislíbily	přislíbit	k5eAaPmAgInP	přislíbit
České	český	k2eAgInPc1d1	český
republice	republika	k1gFnSc6	republika
stejnou	stejný	k2eAgFnSc4d1	stejná
výjimku	výjimka	k1gFnSc4	výjimka
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
a	a	k8xC	a
pro	pro	k7c4	pro
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
;	;	kIx,	;
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
řešením	řešení	k1gNnSc7	řešení
uspokojení	uspokojení	k1gNnSc2	uspokojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
zahájil	zahájit	k5eAaPmAgMnS	zahájit
jednání	jednání	k1gNnSc4	jednání
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
v	v	k7c6	v
odročeném	odročený	k2eAgNnSc6d1	odročené
jednání	jednání	k1gNnSc6	jednání
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
9	[number]	k4	9
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žalované	žalovaný	k2eAgFnPc1d1	žalovaná
části	část	k1gFnPc1	část
smlouvy	smlouva	k1gFnSc2	smlouva
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ústavním	ústavní	k2eAgInSc7d1	ústavní
pořádkem	pořádek	k1gInSc7	pořádek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
další	další	k2eAgInPc4d1	další
návrhy	návrh	k1gInPc4	návrh
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
a	a	k8xC	a
v	v	k7c6	v
odůvodnění	odůvodnění	k1gNnSc6	odůvodnění
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
musí	muset	k5eAaImIp3nS	muset
smlouvu	smlouva	k1gFnSc4	smlouva
neodkladně	odkladně	k6eNd1	odkladně
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
v	v	k7c4	v
15	[number]	k4	15
hodin	hodina	k1gFnPc2	hodina
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
Lisabonskou	lisabonský	k2eAgFnSc4d1	Lisabonská
smlouvu	smlouva	k1gFnSc4	smlouva
svým	svůj	k3xOyFgInSc7	svůj
podpisem	podpis	k1gInSc7	podpis
ratifikoval	ratifikovat	k5eAaBmAgMnS	ratifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
ostrý	ostrý	k2eAgInSc4d1	ostrý
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
kvalitou	kvalita	k1gFnSc7	kvalita
i	i	k8xC	i
obsahem	obsah	k1gInSc7	obsah
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstoupením	vstoupení	k1gNnSc7	vstoupení
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
"	"	kIx"	"
<g/>
navzdory	navzdory	k7c3	navzdory
politickému	politický	k2eAgInSc3d1	politický
názoru	názor	k1gInSc3	názor
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
přestane	přestat	k5eAaPmIp3nS	přestat
být	být	k5eAaImF	být
suverénním	suverénní	k2eAgInSc7d1	suverénní
státem	stát	k1gInSc7	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
politická	politický	k2eAgFnSc1d1	politická
scéna	scéna	k1gFnSc1	scéna
==	==	k?	==
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
Lisabonskou	lisabonský	k2eAgFnSc4d1	Lisabonská
smlouvu	smlouva	k1gFnSc4	smlouva
odmítal	odmítat	k5eAaImAgMnS	odmítat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
ratifikace	ratifikace	k1gFnSc2	ratifikace
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
mandátu	mandát	k1gInSc2	mandát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
ale	ale	k8xC	ale
v	v	k7c6	v
interview	interview	k1gNnSc6	interview
s	s	k7c7	s
ČT	ČT	kA	ČT
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
mohu	moct	k5eAaImIp1nS	moct
nahlas	nahlas	k6eAd1	nahlas
opakovat	opakovat	k5eAaImF	opakovat
jeden	jeden	k4xCgInSc4	jeden
svůj	svůj	k3xOyFgInSc4	svůj
výrok	výrok	k1gInSc4	výrok
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
opravdu	opravdu	k9	opravdu
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaPmAgMnP	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgInPc4	takový
zlaté	zlatý	k1gInPc4	zlatý
ořechové	ořechový	k2eAgInPc4d1	ořechový
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
že	že	k8xS	že
být	být	k5eAaImF	být
musí	muset	k5eAaImIp3nS	muset
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
jediná	jediný	k2eAgFnSc1d1	jediná
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
chtěla	chtít	k5eAaImAgFnS	chtít
zablokovat	zablokovat	k5eAaPmF	zablokovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tou	ten	k3xDgFnSc7	ten
osobou	osoba	k1gFnSc7	osoba
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
toto	tento	k3xDgNnSc4	tento
já	já	k3xPp1nSc1	já
neudělám	udělat	k5eNaPmIp1nS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Všechny	všechen	k3xTgFnPc4	všechen
české	český	k2eAgFnPc4d1	Česká
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
KSČM	KSČM	kA	KSČM
Lisabonskou	lisabonský	k2eAgFnSc4d1	Lisabonská
smlouvu	smlouva	k1gFnSc4	smlouva
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
podporovaly	podporovat	k5eAaImAgFnP	podporovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČSSD	ČSSD	kA	ČSSD
Lisabonskou	lisabonský	k2eAgFnSc4d1	Lisabonská
smlouvu	smlouva	k1gFnSc4	smlouva
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
vyslovovala	vyslovovat	k5eAaImAgFnS	vyslovovat
pro	pro	k7c4	pro
referendum	referendum	k1gNnSc4	referendum
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
a	a	k8xC	a
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
výrazně	výrazně	k6eAd1	výrazně
tlačila	tlačit	k5eAaImAgFnS	tlačit
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
schválení	schválení	k1gNnSc4	schválení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KDU-ČSL	KDU-ČSL	k?	KDU-ČSL
a	a	k8xC	a
Strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
smlouvu	smlouva	k1gFnSc4	smlouva
obecně	obecně	k6eAd1	obecně
podporovaly	podporovat	k5eAaImAgInP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
SZ	SZ	kA	SZ
žádala	žádat	k5eAaImAgFnS	žádat
urychlené	urychlený	k2eAgNnSc4d1	urychlené
schválení	schválení	k1gNnSc4	schválení
a	a	k8xC	a
ratifikaci	ratifikace	k1gFnSc4	ratifikace
smlouvy	smlouva	k1gFnSc2	smlouva
ještě	ještě	k9	ještě
před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
začala	začít	k5eAaPmAgFnS	začít
předsedat	předsedat	k5eAaImF	předsedat
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
.	.	kIx.	.
<g/>
KSČM	KSČM	kA	KSČM
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
o	o	k7c6	o
Lisabonské	lisabonský	k2eAgFnSc6d1	Lisabonská
smlouvě	smlouva	k1gFnSc6	smlouva
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předseda	předseda	k1gMnSc1	předseda
ODS	ODS	kA	ODS
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
schválení	schválení	k1gNnSc2	schválení
smlouvy	smlouva	k1gFnSc2	smlouva
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
strany	strana	k1gFnSc2	strana
však	však	k9	však
sdílela	sdílet	k5eAaImAgFnS	sdílet
odmítavý	odmítavý	k2eAgInSc4d1	odmítavý
názor	názor	k1gInSc4	názor
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Senát	senát	k1gInSc1	senát
rovněž	rovněž	k9	rovněž
podal	podat	k5eAaPmAgInS	podat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
k	k	k7c3	k
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
několik	několik	k4yIc4	několik
dotazů	dotaz	k1gInPc2	dotaz
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
možného	možný	k2eAgInSc2d1	možný
rozporu	rozpor	k1gInSc2	rozpor
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
jednomyslným	jednomyslný	k2eAgNnSc7d1	jednomyslné
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
všech	všecek	k3xTgMnPc2	všecek
jeho	jeho	k3xOp3gMnPc2	jeho
soudců	soudce	k1gMnPc2	soudce
shledal	shledat	k5eAaPmAgMnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
podaných	podaný	k2eAgInPc6d1	podaný
dotazech	dotaz	k1gInPc6	dotaz
k	k	k7c3	k
Lisabonské	lisabonský	k2eAgFnSc3d1	Lisabonská
smlouvě	smlouva	k1gFnSc3	smlouva
nenašel	najít	k5eNaPmAgInS	najít
rozporu	rozpor	k1gInSc2	rozpor
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
také	také	k9	také
podotkl	podotknout	k5eAaPmAgMnS	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
neposuzoval	posuzovat	k5eNaImAgInS	posuzovat
Lisabonskou	lisabonský	k2eAgFnSc4d1	Lisabonská
smlouvu	smlouva	k1gFnSc4	smlouva
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pouze	pouze	k6eAd1	pouze
ty	ten	k3xDgFnPc1	ten
její	její	k3xOp3gFnPc1	její
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterým	který	k3yIgInPc3	který
směřovala	směřovat	k5eAaImAgFnS	směřovat
uplatněná	uplatněný	k2eAgFnSc1d1	uplatněná
argumentace	argumentace	k1gFnSc1	argumentace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
senátoři	senátor	k1gMnPc1	senátor
ODS	ODS	kA	ODS
nechali	nechat	k5eAaPmAgMnP	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
podají	podat	k5eAaPmIp3nP	podat
další	další	k2eAgInPc4d1	další
dotazy	dotaz	k1gInPc4	dotaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
představuje	představovat	k5eAaImIp3nS	představovat
pouze	pouze	k6eAd1	pouze
kosmetickou	kosmetický	k2eAgFnSc4d1	kosmetická
úpravu	úprava	k1gFnSc4	úprava
neschválené	schválený	k2eNgFnSc2d1	neschválená
Ústavy	ústava	k1gFnSc2	ústava
EU	EU	kA	EU
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
Nizozemsko	Nizozemsko	k1gNnSc4	Nizozemsko
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
v	v	k7c6	v
referendech	referendum	k1gNnPc6	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
smlouvy	smlouva	k1gFnSc2	smlouva
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
během	během	k7c2	během
německého	německý	k2eAgNnSc2d1	německé
předsednictví	předsednictví	k1gNnSc2	předsednictví
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
požadavky	požadavek	k1gInPc4	požadavek
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
transformován	transformován	k2eAgInSc1d1	transformován
do	do	k7c2	do
klasické	klasický	k2eAgFnSc2d1	klasická
novelizace	novelizace	k1gFnSc2	novelizace
stávajícího	stávající	k2eAgInSc2d1	stávající
smluvního	smluvní	k2eAgInSc2d1	smluvní
rámce	rámec	k1gInSc2	rámec
s	s	k7c7	s
vynecháním	vynechání	k1gNnSc7	vynechání
některých	některý	k3yIgInPc2	některý
ústavních	ústavní	k2eAgInPc2d1	ústavní
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1	evropský
politici	politik	k1gMnPc1	politik
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
otevřeně	otevřeně	k6eAd1	otevřeně
přiznávají	přiznávat	k5eAaImIp3nP	přiznávat
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
bude	být	k5eAaImBp3nS	být
postupně	postupně	k6eAd1	postupně
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
platit	platit	k5eAaImF	platit
tzv.	tzv.	kA	tzv.
systém	systém	k1gInSc4	systém
dvojí	dvojit	k5eAaImIp3nS	dvojit
většiny	většina	k1gFnSc2	většina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
musí	muset	k5eAaImIp3nS	muset
vyslovit	vyslovit	k5eAaPmF	vyslovit
55	[number]	k4	55
%	%	kIx~	%
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
současně	současně	k6eAd1	současně
65	[number]	k4	65
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
tak	tak	k6eAd1	tak
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
čtyři	čtyři	k4xCgFnPc1	čtyři
velké	velký	k2eAgFnPc1d1	velká
země	zem	k1gFnPc1	zem
EU	EU	kA	EU
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
zablokovat	zablokovat	k5eAaPmF	zablokovat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
jim	on	k3xPp3gMnPc3	on
bude	být	k5eAaImBp3nS	být
stačit	stačit	k5eAaBmF	stačit
podpora	podpora	k1gFnSc1	podpora
jedenácti	jedenáct	k4xCc2	jedenáct
libovolně	libovolně	k6eAd1	libovolně
malých	malý	k2eAgInPc2d1	malý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
je	být	k5eAaImIp3nS	být
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
definuje	definovat	k5eAaBmIp3nS	definovat
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
EU	EU	kA	EU
možnost	možnost	k1gFnSc4	možnost
získávat	získávat	k5eAaImF	získávat
nové	nový	k2eAgFnPc4d1	nová
pravomoce	pravomoc	k1gFnPc4	pravomoc
a	a	k8xC	a
rušit	rušit	k5eAaImF	rušit
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
bez	bez	k7c2	bez
další	další	k2eAgFnSc2d1	další
ratifikace	ratifikace	k1gFnSc2	ratifikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
postup	postup	k1gInSc1	postup
pro	pro	k7c4	pro
přijímání	přijímání	k1gNnSc4	přijímání
změn	změna	k1gFnPc2	změna
primárního	primární	k2eAgNnSc2d1	primární
práva	právo	k1gNnSc2	právo
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
48	[number]	k4	48
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
obecná	obecný	k2eAgFnSc1d1	obecná
přechodová	přechodový	k2eAgFnSc1d1	přechodová
klauzule	klauzule	k1gFnSc1	klauzule
(	(	kIx(	(
<g/>
passerelle	passerelle	k1gInSc1	passerelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgInSc7	svůj
sedmým	sedmý	k4xOgInSc7	sedmý
odstavcem	odstavec	k1gInSc7	odstavec
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
Evropské	evropský	k2eAgFnSc3d1	Evropská
radě	rada	k1gFnSc3	rada
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dále	daleko	k6eAd2	daleko
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
počet	počet	k1gInSc4	počet
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
kvalifikovanou	kvalifikovaný	k2eAgFnSc7d1	kvalifikovaná
většinou	většina	k1gFnSc7	většina
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
rozšíření	rozšíření	k1gNnSc3	rozšíření
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
zabránit	zabránit	k5eAaPmF	zabránit
parlament	parlament	k1gInSc4	parlament
kteréhokoliv	kterýkoliv	k3yIgInSc2	kterýkoliv
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
do	do	k7c2	do
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
obdržení	obdržení	k1gNnSc2	obdržení
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
modifikace	modifikace	k1gFnSc2	modifikace
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
svůj	svůj	k3xOyFgInSc4	svůj
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
třetí	třetí	k4xOgFnSc6	třetí
pododstavec	pododstavec	k1gInSc1	pododstavec
odstavce	odstavec	k1gInSc2	odstavec
7	[number]	k4	7
článku	článek	k1gInSc2	článek
48	[number]	k4	48
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Evropské	evropský	k2eAgFnSc6d1	Evropská
Unii	unie	k1gFnSc6	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
LS	LS	kA	LS
dále	daleko	k6eAd2	daleko
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
doložku	doložka	k1gFnSc4	doložka
flexibility	flexibilita	k1gFnSc2	flexibilita
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
čl	čl	kA	čl
<g/>
.	.	kIx.	.
352	[number]	k4	352
SFEU	SFEU	kA	SFEU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
Radě	rada	k1gFnSc3	rada
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Komise	komise	k1gFnSc2	komise
a	a	k8xC	a
po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
souhlasu	souhlas	k1gInSc2	souhlas
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
jednomyslně	jednomyslně	k6eAd1	jednomyslně
přijmout	přijmout	k5eAaPmF	přijmout
vhodná	vhodný	k2eAgNnPc4d1	vhodné
opatření	opatření	k1gNnPc4	opatření
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
cílů	cíl	k1gInPc2	cíl
Unie	unie	k1gFnSc2	unie
i	i	k8xC	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
Smlouva	smlouva	k1gFnSc1	smlouva
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
nedává	dávat	k5eNaImIp3nS	dávat
Unii	unie	k1gFnSc4	unie
nezbytnou	nezbytný	k2eAgFnSc4d1	nezbytná
pravomoc	pravomoc	k1gFnSc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc4	možnost
platila	platit	k5eAaImAgFnS	platit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
cíle	cíl	k1gInSc2	cíl
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
společného	společný	k2eAgInSc2d1	společný
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
cíle	cíl	k1gInPc4	cíl
Unie	unie	k1gFnSc2	unie
vymezené	vymezený	k2eAgInPc1d1	vymezený
zakládajícími	zakládající	k2eAgFnPc7d1	zakládající
smlouvami	smlouva	k1gFnPc7	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
EVROPSKÁ	evropský	k2eAgFnSc1d1	Evropská
RADA	rada	k1gFnSc1	rada
V	v	k7c6	v
BRUSELU	Brusel	k1gInSc6	Brusel
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
ČERVNA	červen	k1gInSc2	červen
2007	[number]	k4	2007
–	–	k?	–
ZÁVĚRY	závěra	k1gFnSc2	závěra
PŘEDSEDNICTVÍ	předsednictví	k1gNnSc2	předsednictví
<g/>
,	,	kIx,	,
s	s	k7c7	s
přílohou	příloha	k1gFnSc7	příloha
I	i	k9	i
–	–	k?	–
NÁVRH	návrh	k1gInSc1	návrh
MANDÁTU	mandát	k1gInSc2	mandát
PRO	pro	k7c4	pro
MEZIVLÁDNÍ	mezivládní	k2eAgFnSc4d1	mezivládní
KONFERENCI	konference	k1gFnSc4	konference
</s>
</p>
<p>
<s>
Znění	znění	k1gNnSc1	znění
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
Úředním	úřední	k2eAgInSc6d1	úřední
věstníku	věstník	k1gInSc6	věstník
Evropské	evropský	k2eAgFnSc2d1	Evropská
Unie	unie	k1gFnSc2	unie
</s>
</p>
<p>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
v	v	k7c6	v
kostce	kostka	k1gFnSc6	kostka
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
EU	EU	kA	EU
</s>
</p>
<p>
<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
-	-	kIx~	-
konsolidovaný	konsolidovaný	k2eAgInSc1d1	konsolidovaný
text	text	k1gInSc1	text
na	na	k7c6	na
webu	web	k1gInSc6	web
Odboru	odbor	k1gInSc2	odbor
informování	informování	k1gNnSc2	informování
o	o	k7c6	o
evropských	evropský	k2eAgFnPc6d1	Evropská
záležitostech	záležitost	k1gFnPc6	záležitost
Úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
