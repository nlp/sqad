<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Kamenička	kamenička	k1gFnSc1	kamenička
je	být	k5eAaImIp3nS	být
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
potoce	potok	k1gInSc6	potok
Kamenička	kamenička	k1gFnSc1	kamenička
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Bečov	Bečovo	k1gNnPc2	Bečovo
obce	obec	k1gFnSc2	obec
Blatno	Blatno	k6eAd1	Blatno
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vodohospodářské	vodohospodářský	k2eAgFnSc2d1	vodohospodářská
soustavy	soustava	k1gFnSc2	soustava
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severočeské	severočeský	k2eAgFnSc2d1	Severočeská
hnědouhelné	hnědouhelný	k2eAgFnSc2d1	hnědouhelná
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Správcem	správce	k1gMnSc7	správce
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc1d1	státní
podnik	podnik	k1gInSc1	podnik
Povodí	povodí	k1gNnSc2	povodí
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Chomutov	Chomutov	k1gInSc1	Chomutov
nedostatečně	dostatečně	k6eNd1	dostatečně
zásobován	zásobován	k2eAgInSc1d1	zásobován
pouze	pouze	k6eAd1	pouze
nekvalitní	kvalitní	k2eNgFnSc7d1	nekvalitní
vodou	voda	k1gFnSc7	voda
čerpanou	čerpaný	k2eAgFnSc7d1	čerpaná
ze	z	k7c2	z
studní	studní	k2eAgFnSc2d1	studní
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
proto	proto	k8xC	proto
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
přehrady	přehrada	k1gFnSc2	přehrada
původně	původně	k6eAd1	původně
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Údolní	údolní	k2eAgFnSc2d1	údolní
přehrada	přehrada	k1gFnSc1	přehrada
Císaře	Císař	k1gMnSc2	Císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Talsperre	Talsperr	k1gInSc5	Talsperr
des	des	k1gNnSc1	des
Kaissers	Kaissers	k1gInSc1	Kaissers
Franz	Franz	k1gMnSc1	Franz
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
kraft	kraft	k1gInSc1	kraft
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Stadtgemainde	Stadtgemaind	k1gInSc5	Stadtgemaind
Komotau	Komotaus	k1gInSc6	Komotaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodohospodářský	vodohospodářský	k2eAgInSc1d1	vodohospodářský
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
počítal	počítat	k5eAaImAgInS	počítat
se	s	k7c7	s
spotřebou	spotřeba	k1gFnSc7	spotřeba
60	[number]	k4	60
l	l	kA	l
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
zpracovala	zpracovat	k5eAaPmAgFnS	zpracovat
teplická	teplický	k2eAgFnSc1d1	Teplická
firma	firma	k1gFnSc1	firma
Rumpel	Rumpel	k1gMnSc1	Rumpel
&	&	k?	&
Niklas	Niklas	k1gMnSc1	Niklas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
přehrady	přehrada	k1gFnSc2	přehrada
na	na	k7c6	na
Chomutovce	Chomutovka	k1gFnSc6	Chomutovka
byla	být	k5eAaImAgNnP	být
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
zamítnuta	zamítnut	k2eAgFnSc1d1	zamítnuta
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
posudku	posudek	k1gInSc2	posudek
profesora	profesor	k1gMnSc2	profesor
Otto	Otto	k1gMnSc1	Otto
Luegera	Lueger	k1gMnSc2	Lueger
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
varianta	varianta	k1gFnSc1	varianta
s	s	k7c7	s
přehradou	přehrada	k1gFnSc7	přehrada
na	na	k7c6	na
Kameničce	kamenička	k1gFnSc6	kamenička
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
pracovalo	pracovat	k5eAaImAgNnS	pracovat
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
dělníků	dělník	k1gMnPc2	dělník
zejména	zejména	k9	zejména
z	z	k7c2	z
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
provedla	provést	k5eAaPmAgFnS	provést
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
firma	firma	k1gFnSc1	firma
G.	G.	kA	G.
A.	A.	kA	A.
Wayss	Wayssa	k1gFnPc2	Wayssa
&	&	k?	&
Cie	Cie	k1gFnPc2	Cie
v	v	k7c6	v
letech	let	k1gInPc6	let
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Obloukovitá	obloukovitý	k2eAgFnSc1d1	obloukovitá
tížná	tížný	k2eAgFnSc1d1	tížná
hráz	hráz	k1gFnSc1	hráz
stojí	stát	k5eAaImIp3nS	stát
1,5	[number]	k4	1,5
km	km	kA	km
před	před	k7c7	před
ústím	ústí	k1gNnSc7	ústí
potoku	potok	k1gInSc3	potok
Kamenička	kamenička	k1gFnSc1	kamenička
do	do	k7c2	do
Chomutovky	Chomutovka	k1gFnSc2	Chomutovka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
153	[number]	k4	153
m	m	kA	m
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
asi	asi	k9	asi
33	[number]	k4	33
m	m	kA	m
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nádrž	nádrž	k1gFnSc1	nádrž
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
6	[number]	k4	6
ha	ha	kA	ha
a	a	k8xC	a
délce	délka	k1gFnSc6	délka
vzdutí	vzdutí	k1gNnSc2	vzdutí
470	[number]	k4	470
m.	m.	k?	m.
Hráz	hráz	k1gFnSc1	hráz
je	být	k5eAaImIp3nS	být
vyzděná	vyzděný	k2eAgFnSc1d1	vyzděná
z	z	k7c2	z
lomového	lomový	k2eAgInSc2d1	lomový
kamene	kámen	k1gInSc2	kámen
na	na	k7c6	na
žulovém	žulový	k2eAgInSc6d1	žulový
podkladu	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
odběrná	odběrný	k2eAgFnSc1d1	odběrná
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
spodních	spodní	k2eAgFnPc2d1	spodní
výpustí	výpust	k1gFnPc2	výpust
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
400	[number]	k4	400
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Odběr	odběr	k1gInSc1	odběr
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
2,5	[number]	k4	2,5
m	m	kA	m
<g/>
,	,	kIx,	,
6,5	[number]	k4	6,5
m	m	kA	m
a	a	k8xC	a
10	[number]	k4	10
m	m	kA	m
nad	nad	k7c7	nad
dnem	dno	k1gNnSc7	dno
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
přetečením	přetečení	k1gNnSc7	přetečení
je	být	k5eAaImIp3nS	být
hráz	hráz	k1gFnSc1	hráz
chráněna	chráněn	k2eAgFnSc1d1	chráněna
asi	asi	k9	asi
24	[number]	k4	24
m	m	kA	m
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
bezpečnostním	bezpečnostní	k2eAgInSc7d1	bezpečnostní
přelivem	přeliv	k1gInSc7	přeliv
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
29	[number]	k4	29
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Voda	voda	k1gFnSc1	voda
z	z	k7c2	z
přelivu	přeliv	k1gInSc2	přeliv
stéká	stékat	k5eAaImIp3nS	stékat
kaskádou	kaskáda	k1gFnSc7	kaskáda
do	do	k7c2	do
koryta	koryto	k1gNnSc2	koryto
pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
přelivu	přeliv	k1gInSc2	přeliv
ústí	ústit	k5eAaImIp3nS	ústit
také	také	k9	také
obtokový	obtokový	k2eAgInSc4d1	obtokový
kanál	kanál	k1gInSc4	kanál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
nádrže	nádrž	k1gFnSc2	nádrž
odvádí	odvádět	k5eAaImIp3nS	odvádět
případnou	případný	k2eAgFnSc4d1	případná
nekvalitní	kvalitní	k2eNgFnSc4d1	nekvalitní
vodu	voda	k1gFnSc4	voda
mimo	mimo	k7c4	mimo
nádrž	nádrž	k1gFnSc4	nádrž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
projektu	projekt	k1gInSc2	projekt
přehrady	přehrada	k1gFnSc2	přehrada
je	být	k5eAaImIp3nS	být
také	také	k9	také
Dieterova	Dieterův	k2eAgFnSc1d1	Dieterova
štola	štola	k1gFnSc1	štola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
při	při	k7c6	při
tání	tání	k1gNnSc6	tání
sněhu	sníh	k1gInSc2	sníh
nebo	nebo	k8xC	nebo
velkých	velký	k2eAgInPc6d1	velký
deštích	dešť	k1gInPc6	dešť
odvádí	odvádět	k5eAaImIp3nS	odvádět
nekvalitní	kvalitní	k2eNgFnSc4d1	nekvalitní
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
horských	horský	k2eAgNnPc2d1	horské
rašelinišť	rašeliniště	k1gNnPc2	rašeliniště
do	do	k7c2	do
Chomutovky	Chomutovka	k1gFnSc2	Chomutovka
v	v	k7c6	v
Bezručově	Bezručův	k2eAgNnSc6d1	Bezručovo
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zásobování	zásobování	k1gNnSc4	zásobování
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
upravuje	upravovat	k5eAaImIp3nS	upravovat
v	v	k7c6	v
úpravně	úpravna	k1gFnSc6	úpravna
vody	voda	k1gFnSc2	voda
III	III	kA	III
<g/>
.	.	kIx.	.
mlýn	mlýn	k1gInSc1	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Přehrada	přehrada	k1gFnSc1	přehrada
je	být	k5eAaImIp3nS	být
technickou	technický	k2eAgFnSc7d1	technická
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc1	přístup
na	na	k7c4	na
hráz	hráz	k1gFnSc4	hráz
a	a	k8xC	a
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
I.	I.	kA	I.
ochranného	ochranný	k2eAgNnSc2d1	ochranné
pásma	pásmo	k1gNnSc2	pásmo
vodního	vodní	k2eAgInSc2d1	vodní
zdroje	zdroj	k1gInSc2	zdroj
není	být	k5eNaImIp3nS	být
povolen	povolit	k5eAaPmNgInS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
souběžně	souběžně	k6eAd1	souběžně
vedou	vést	k5eAaImIp3nP	vést
modře	modro	k6eAd1	modro
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
značka	značka	k1gFnSc1	značka
a	a	k8xC	a
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
č.	č.	k?	č.
3078	[number]	k4	3078
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Kameničky	kamenička	k1gFnSc2	kamenička
se	se	k3xPyFc4	se
asi	asi	k9	asi
1,5	[number]	k4	1,5
km	km	kA	km
nad	nad	k7c7	nad
nádrží	nádrž	k1gFnSc7	nádrž
nachází	nacházet	k5eAaImIp3nS	nacházet
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Buky	buk	k1gInPc4	buk
nad	nad	k7c7	nad
Kameničkou	kamenička	k1gFnSc7	kamenička
s	s	k7c7	s
porostem	porost	k1gInSc7	porost
200	[number]	k4	200
let	léto	k1gNnPc2	léto
starého	starý	k2eAgInSc2d1	starý
bukového	bukový	k2eAgInSc2d1	bukový
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dieterova	Dieterův	k2eAgFnSc1d1	Dieterova
štola	štola	k1gFnSc1	štola
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Kamenička	kamenička	k1gFnSc1	kamenička
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
