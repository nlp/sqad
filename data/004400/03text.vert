<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
(	(	kIx(	(
<g/>
ČNB	ČNB	kA	ČNB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
finančním	finanční	k2eAgInSc7d1	finanční
trhem	trh	k1gInSc7	trh
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
je	být	k5eAaImIp3nS	být
právnickou	právnický	k2eAgFnSc7d1	právnická
osobou	osoba	k1gFnSc7	osoba
veřejného	veřejný	k2eAgNnSc2d1	veřejné
práva	právo	k1gNnSc2	právo
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jí	on	k3xPp3gFnSc7	on
svěřeny	svěřen	k2eAgFnPc4d1	svěřena
kompetence	kompetence	k1gFnPc4	kompetence
správního	správní	k2eAgInSc2d1	správní
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
stanoveném	stanovený	k2eAgInSc6d1	stanovený
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
ale	ale	k9	ale
správním	správní	k2eAgInSc7d1	správní
úřadem	úřad	k1gInSc7	úřad
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Hospodaří	hospodařit	k5eAaImIp3nP	hospodařit
samostatně	samostatně	k6eAd1	samostatně
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
majetkem	majetek	k1gInSc7	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
činnosti	činnost	k1gFnSc2	činnost
lze	lze	k6eAd1	lze
zasahovat	zasahovat	k5eAaImF	zasahovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
98	[number]	k4	98
Ústavy	ústava	k1gFnSc2	ústava
ČR	ČR	kA	ČR
a	a	k8xC	a
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ČNB	ČNB	kA	ČNB
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
její	její	k3xOp3gFnSc2	její
činnosti	činnost	k1gFnSc2	činnost
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
cenovou	cenový	k2eAgFnSc4d1	cenová
stabilitu	stabilita	k1gFnSc4	stabilita
(	(	kIx(	(
<g/>
dřívější	dřívější	k2eAgFnPc1d1	dřívější
znění	znění	k1gNnSc4	znění
Ústavy	ústava	k1gFnSc2	ústava
před	před	k7c7	před
tzv.	tzv.	kA	tzv.
euronovelou	euronovela	k1gFnSc7	euronovela
uvádělo	uvádět	k5eAaImAgNnS	uvádět
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
cíl	cíl	k1gInSc4	cíl
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
měnovou	měnový	k2eAgFnSc4d1	měnová
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosažení	dosažení	k1gNnSc1	dosažení
a	a	k8xC	a
udržení	udržení	k1gNnSc1	udržení
cenové	cenový	k2eAgFnSc2d1	cenová
stability	stabilita	k1gFnSc2	stabilita
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vytváření	vytváření	k1gNnSc4	vytváření
nízkoinflačního	nízkoinflační	k2eAgNnSc2d1	nízkoinflační
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
trvalým	trvalý	k2eAgInSc7d1	trvalý
příspěvkem	příspěvek	k1gInSc7	příspěvek
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
udržitelný	udržitelný	k2eAgInSc4d1	udržitelný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Předpokladem	předpoklad	k1gInSc7	předpoklad
účinnosti	účinnost	k1gFnSc2	účinnost
měnových	měnový	k2eAgInPc2d1	měnový
nástrojů	nástroj	k1gInPc2	nástroj
vedoucích	vedoucí	k1gFnPc2	vedoucí
k	k	k7c3	k
cenové	cenový	k2eAgFnSc3d1	cenová
stabilitě	stabilita	k1gFnSc3	stabilita
je	být	k5eAaImIp3nS	být
nezávislost	nezávislost	k1gFnSc1	nezávislost
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
dále	daleko	k6eAd2	daleko
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
finanční	finanční	k2eAgFnSc4d1	finanční
stabilitu	stabilita	k1gFnSc4	stabilita
a	a	k8xC	a
o	o	k7c4	o
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
fungování	fungování	k1gNnSc4	fungování
finančního	finanční	k2eAgInSc2d1	finanční
systému	systém	k1gInSc2	systém
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
rovněž	rovněž	k9	rovněž
podporuje	podporovat	k5eAaImIp3nS	podporovat
obecnou	obecný	k2eAgFnSc4d1	obecná
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
politiku	politika	k1gFnSc4	politika
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
tento	tento	k3xDgInSc4	tento
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
cíl	cíl	k1gInSc4	cíl
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
hlavním	hlavní	k2eAgInSc7d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
určuje	určovat	k5eAaImIp3nS	určovat
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaImIp3nS	vydávat
bankovky	bankovka	k1gFnPc4	bankovka
a	a	k8xC	a
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
peněžní	peněžní	k2eAgInSc1d1	peněžní
oběh	oběh	k1gInSc1	oběh
<g/>
,	,	kIx,	,
platební	platební	k2eAgInSc1d1	platební
styk	styk	k1gInSc1	styk
a	a	k8xC	a
zúčtování	zúčtování	k1gNnSc1	zúčtování
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
bankovním	bankovní	k2eAgInSc7d1	bankovní
sektorem	sektor	k1gInSc7	sektor
<g/>
,	,	kIx,	,
kapitálovým	kapitálový	k2eAgInSc7d1	kapitálový
trhem	trh	k1gInSc7	trh
<g/>
,	,	kIx,	,
pojišťovnictvím	pojišťovnictví	k1gNnSc7	pojišťovnictví
<g/>
,	,	kIx,	,
penzijním	penzijní	k2eAgNnSc7d1	penzijní
připojištěním	připojištění	k1gNnSc7	připojištění
<g/>
,	,	kIx,	,
družstevními	družstevní	k2eAgFnPc7d1	družstevní
záložnami	záložna	k1gFnPc7	záložna
a	a	k8xC	a
institucemi	instituce	k1gFnPc7	instituce
elektronických	elektronický	k2eAgInPc2d1	elektronický
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
provádí	provádět	k5eAaImIp3nS	provádět
činnost	činnost	k1gFnSc4	činnost
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ČNB	ČNB	kA	ČNB
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
úkoly	úkol	k1gInPc4	úkol
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Její	její	k3xOp3gFnSc7	její
deklarovanou	deklarovaný	k2eAgFnSc7d1	deklarovaná
úlohou	úloha	k1gFnSc7	úloha
je	být	k5eAaImIp3nS	být
zajistit	zajistit	k5eAaPmF	zajistit
stabilitu	stabilita	k1gFnSc4	stabilita
cenového	cenový	k2eAgInSc2d1	cenový
indexu	index	k1gInSc2	index
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
není	být	k5eNaImIp3nS	být
totožné	totožný	k2eAgNnSc1d1	totožné
jako	jako	k8xC	jako
stabilita	stabilita	k1gFnSc1	stabilita
cen	cena	k1gFnPc2	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c6	o
plnění	plnění	k1gNnSc6	plnění
uvedené	uvedený	k2eAgFnSc2d1	uvedená
úlohy	úloha	k1gFnSc2	úloha
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
měnově	měnově	k6eAd1	měnově
politického	politický	k2eAgInSc2d1	politický
režimu	režim	k1gInSc2	režim
nazývaného	nazývaný	k2eAgNnSc2d1	nazývané
cílování	cílování	k1gNnSc2	cílování
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodování	rozhodování	k1gNnSc1	rozhodování
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
ČNB	ČNB	kA	ČNB
o	o	k7c6	o
nastavení	nastavení	k1gNnSc6	nastavení
základních	základní	k2eAgFnPc2d1	základní
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
aktuální	aktuální	k2eAgFnSc2d1	aktuální
makroekonomické	makroekonomický	k2eAgFnSc2d1	makroekonomická
prognózy	prognóza	k1gFnSc2	prognóza
a	a	k8xC	a
vyhodnocení	vyhodnocení	k1gNnSc4	vyhodnocení
rizik	riziko	k1gNnPc2	riziko
jejího	její	k3xOp3gNnSc2	její
nenaplnění	nenaplnění	k1gNnSc2	nenaplnění
<g/>
.	.	kIx.	.
</s>
<s>
Změnami	změna	k1gFnPc7	změna
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
produkcí	produkce	k1gFnSc7	produkce
peněz	peníze	k1gInPc2	peníze
<g/>
)	)	kIx)	)
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
cenu	cena	k1gFnSc4	cena
peněz	peníze	k1gInPc2	peníze
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
předem	předem	k6eAd1	předem
stanoveného	stanovený	k2eAgInSc2d1	stanovený
růstu	růst	k1gInSc2	růst
peněžní	peněžní	k2eAgFnPc1d1	peněžní
zásoby	zásoba	k1gFnPc1	zásoba
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
zvolené	zvolený	k2eAgFnSc2d1	zvolená
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
eurozóny	eurozóna	k1gFnSc2	eurozóna
budou	být	k5eAaImBp3nP	být
inflační	inflační	k2eAgInPc1d1	inflační
cíle	cíl	k1gInPc1	cíl
orientovány	orientován	k2eAgInPc1d1	orientován
na	na	k7c4	na
plnění	plnění	k1gNnPc4	plnění
maastrichtských	maastrichtský	k2eAgNnPc2d1	maastrichtské
konvergenčních	konvergenční	k2eAgNnPc2d1	konvergenční
kritérií	kritérion	k1gNnPc2	kritérion
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cenové	cenový	k2eAgFnSc2d1	cenová
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
dlouhodobých	dlouhodobý	k2eAgFnPc2d1	dlouhodobá
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
eura	euro	k1gNnSc2	euro
se	se	k3xPyFc4	se
ČNB	ČNB	kA	ČNB
vzdá	vzdát	k5eAaPmIp3nS	vzdát
samostatné	samostatný	k2eAgFnSc2d1	samostatná
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souhrnu	souhrn	k1gInSc6	souhrn
tzn.	tzn.	kA	tzn.
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
bankovním	bankovní	k2eAgInSc7d1	bankovní
sektorem	sektor	k1gInSc7	sektor
<g/>
,	,	kIx,	,
kapitálovým	kapitálový	k2eAgInSc7d1	kapitálový
trhem	trh	k1gInSc7	trh
<g/>
,	,	kIx,	,
pojišťovnictvím	pojišťovnictví	k1gNnSc7	pojišťovnictví
a	a	k8xC	a
penzijním	penzijní	k2eAgNnSc7d1	penzijní
připojištěním	připojištění	k1gNnSc7	připojištění
<g/>
,	,	kIx,	,
družstevními	družstevní	k2eAgFnPc7d1	družstevní
záložnami	záložna	k1gFnPc7	záložna
<g/>
,	,	kIx,	,
devizový	devizový	k2eAgInSc1d1	devizový
dohled	dohled	k1gInSc1	dohled
a	a	k8xC	a
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
institucemi	instituce	k1gFnPc7	instituce
elektronických	elektronický	k2eAgMnPc2d1	elektronický
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
stanoví	stanovit	k5eAaPmIp3nS	stanovit
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
stabilitu	stabilita	k1gFnSc4	stabilita
celého	celý	k2eAgInSc2d1	celý
finančního	finanční	k2eAgInSc2d1	finanční
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
reguluje	regulovat	k5eAaImIp3nS	regulovat
<g/>
,	,	kIx,	,
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
<g/>
,	,	kIx,	,
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
a	a	k8xC	a
popřípadě	popřípadě	k6eAd1	popřípadě
postihuje	postihovat	k5eAaImIp3nS	postihovat
nedodržování	nedodržování	k1gNnSc1	nedodržování
stanovených	stanovený	k2eAgNnPc2d1	stanovené
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
integraci	integrace	k1gFnSc3	integrace
dohledu	dohled	k1gInSc2	dohled
nad	nad	k7c7	nad
finančním	finanční	k2eAgInSc7d1	finanční
trhem	trh	k1gInSc7	trh
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
instituce	instituce	k1gFnSc2	instituce
(	(	kIx(	(
<g/>
ČNB	ČNB	kA	ČNB
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
tak	tak	k9	tak
převzala	převzít	k5eAaPmAgFnS	převzít
agendu	agenda	k1gFnSc4	agenda
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
cenné	cenný	k2eAgInPc4d1	cenný
papíry	papír	k1gInPc4	papír
<g/>
,	,	kIx,	,
Úřadu	úřad	k1gInSc2	úřad
státního	státní	k2eAgInSc2d1	státní
dozoru	dozor	k1gInSc2	dozor
v	v	k7c6	v
pojišťovnictví	pojišťovnictví	k1gNnSc6	pojišťovnictví
a	a	k8xC	a
penzijním	penzijní	k2eAgNnSc6d1	penzijní
připojištění	připojištění	k1gNnSc6	připojištění
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
družstevními	družstevní	k2eAgFnPc7d1	družstevní
záložnami	záložna	k1gFnPc7	záložna
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
má	mít	k5eAaImIp3nS	mít
monopolní	monopolní	k2eAgNnSc4d1	monopolní
právo	právo	k1gNnSc4	právo
vydávat	vydávat	k5eAaPmF	vydávat
bankovky	bankovka	k1gFnPc4	bankovka
a	a	k8xC	a
mince	mince	k1gFnPc4	mince
včetně	včetně	k7c2	včetně
mincí	mince	k1gFnPc2	mince
pamětních	pamětní	k2eAgFnPc2d1	pamětní
<g/>
.	.	kIx.	.
</s>
<s>
Dbá	dbát	k5eAaImIp3nS	dbát
o	o	k7c4	o
plynulý	plynulý	k2eAgInSc4d1	plynulý
a	a	k8xC	a
hospodárný	hospodárný	k2eAgInSc4d1	hospodárný
peněžní	peněžní	k2eAgInSc4d1	peněžní
oběh	oběh	k1gInSc4	oběh
<g/>
,	,	kIx,	,
spravuje	spravovat	k5eAaImIp3nS	spravovat
zásoby	zásoba	k1gFnPc4	zásoba
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
stahuje	stahovat	k5eAaImIp3nS	stahovat
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
a	a	k8xC	a
ničí	ničit	k5eAaImIp3nS	ničit
opotřebované	opotřebovaný	k2eAgFnPc4d1	opotřebovaná
bankovky	bankovka	k1gFnPc4	bankovka
a	a	k8xC	a
mince	mince	k1gFnPc4	mince
a	a	k8xC	a
vyměňuje	vyměňovat	k5eAaImIp3nS	vyměňovat
poškozené	poškozený	k2eAgInPc4d1	poškozený
peníze	peníz	k1gInPc4	peníz
za	za	k7c4	za
nové	nový	k2eAgMnPc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
technickou	technický	k2eAgFnSc4d1	technická
a	a	k8xC	a
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
přípravu	příprava	k1gFnSc4	příprava
platidel	platidlo	k1gNnPc2	platidlo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
dodávky	dodávka	k1gFnPc4	dodávka
<g/>
.	.	kIx.	.
</s>
<s>
Podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
právní	právní	k2eAgFnSc2d1	právní
a	a	k8xC	a
technické	technický	k2eAgFnSc2d1	technická
ochrany	ochrana	k1gFnSc2	ochrana
platidel	platidlo	k1gNnPc2	platidlo
proti	proti	k7c3	proti
padělání	padělání	k1gNnSc3	padělání
a	a	k8xC	a
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
realizaci	realizace	k1gFnSc6	realizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ochrany	ochrana	k1gFnSc2	ochrana
měny	měna	k1gFnSc2	měna
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
a	a	k8xC	a
eviduje	evidovat	k5eAaImIp3nS	evidovat
všechny	všechen	k3xTgInPc4	všechen
peníze	peníz	k1gInPc4	peníz
zadržené	zadržený	k2eAgInPc4d1	zadržený
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
padělání	padělání	k1gNnSc2	padělání
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
devizové	devizový	k2eAgFnSc2d1	devizová
činnosti	činnost	k1gFnSc2	činnost
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
správa	správa	k1gFnSc1	správa
devizových	devizový	k2eAgFnPc2d1	devizová
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
,	,	kIx,	,
operace	operace	k1gFnPc1	operace
na	na	k7c6	na
devizovém	devizový	k2eAgInSc6d1	devizový
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
devizová	devizový	k2eAgFnSc1d1	devizová
regulace	regulace	k1gFnSc1	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
devizových	devizový	k2eAgFnPc2d1	devizová
rezerv	rezerva	k1gFnPc2	rezerva
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
udržovat	udržovat	k5eAaImF	udržovat
a	a	k8xC	a
eventuálně	eventuálně	k6eAd1	eventuálně
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
hodnotu	hodnota	k1gFnSc4	hodnota
devizových	devizový	k2eAgFnPc2d1	devizová
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
,	,	kIx,	,
zabezpečovat	zabezpečovat	k5eAaImF	zabezpečovat
devizovou	devizový	k2eAgFnSc4d1	devizová
likviditu	likvidita	k1gFnSc4	likvidita
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Devizové	devizový	k2eAgFnPc1d1	devizová
rezervy	rezerva	k1gFnPc1	rezerva
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
denominovaných	denominovaný	k2eAgInPc2d1	denominovaný
v	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
měnách	měna	k1gFnPc6	měna
<g/>
,	,	kIx,	,
hotovosti	hotovost	k1gFnSc2	hotovost
a	a	k8xC	a
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Mediálně	mediálně	k6eAd1	mediálně
nepopulární	populární	k2eNgFnSc7d1	nepopulární
akcí	akce	k1gFnSc7	akce
ČNB	ČNB	kA	ČNB
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
devizové	devizový	k2eAgFnSc2d1	devizová
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
prodej	prodej	k1gInSc1	prodej
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
tun	tuna	k1gFnPc2	tuna
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
zlatého	zlatý	k2eAgInSc2d1	zlatý
pokladu	poklad	k1gInSc2	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prodej	prodej	k1gInSc1	prodej
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
postupně	postupně	k6eAd1	postupně
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1997	[number]	k4	1997
až	až	k6eAd1	až
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Odůvodněn	odůvodněn	k2eAgInSc1d1	odůvodněn
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
pravděpodobným	pravděpodobný	k2eAgInSc7d1	pravděpodobný
poklesem	pokles	k1gInSc7	pokles
hodnoty	hodnota	k1gFnSc2	hodnota
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
trzích	trh	k1gInPc6	trh
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
zvažovaný	zvažovaný	k2eAgInSc4d1	zvažovaný
prodej	prodej	k1gInSc4	prodej
zlatých	zlatý	k2eAgFnPc2d1	zlatá
rezerv	rezerva	k1gFnPc2	rezerva
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
nově	nově	k6eAd1	nově
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EMU	emu	k1gMnSc1	emu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
devizové	devizový	k2eAgFnPc4d1	devizová
operace	operace	k1gFnPc4	operace
na	na	k7c6	na
domácích	domácí	k2eAgInPc6d1	domácí
trzích	trh	k1gInPc6	trh
patří	patřit	k5eAaImIp3nP	patřit
devizové	devizový	k2eAgFnPc1d1	devizová
intervence	intervence	k1gFnPc1	intervence
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
ČNB	ČNB	kA	ČNB
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
snaží	snažit	k5eAaImIp3nP	snažit
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
výši	výše	k1gFnSc4	výše
kurzu	kurz	k1gInSc2	kurz
koruny	koruna	k1gFnSc2	koruna
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgFnPc3d1	ostatní
měnám	měna	k1gFnPc3	měna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
používá	používat	k5eAaImIp3nS	používat
ČNB	ČNB	kA	ČNB
devizové	devizový	k2eAgFnPc4d1	devizová
intervence	intervence	k1gFnPc4	intervence
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
kurzu	kurz	k1gInSc2	kurz
koruny	koruna	k1gFnSc2	koruna
"	"	kIx"	"
<g/>
poblíž	poblíž	k7c2	poblíž
hladiny	hladina	k1gFnSc2	hladina
27	[number]	k4	27
CZK	CZK	kA	CZK
<g/>
/	/	kIx~	/
<g/>
EUR	euro	k1gNnPc2	euro
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nástroj	nástroj	k1gInSc4	nástroj
místo	místo	k7c2	místo
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
inflačního	inflační	k2eAgInSc2d1	inflační
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
vzrůst	vzrůst	k1gInSc1	vzrůst
devizových	devizový	k2eAgFnPc2d1	devizová
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
stanoví	stanovit	k5eAaPmIp3nS	stanovit
pravidla	pravidlo	k1gNnPc4	pravidlo
provádění	provádění	k1gNnSc2	provádění
platebního	platební	k2eAgInSc2d1	platební
styku	styk	k1gInSc2	styk
bankami	banka	k1gFnPc7	banka
i	i	k8xC	i
nebankovními	bankovní	k2eNgInPc7d1	nebankovní
subjekty	subjekt	k1gInPc7	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
lhůty	lhůta	k1gFnPc4	lhůta
zúčtování	zúčtování	k1gNnSc4	zúčtování
<g/>
,	,	kIx,	,
pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
vydávání	vydávání	k1gNnSc4	vydávání
a	a	k8xC	a
užívání	užívání	k1gNnSc4	užívání
elektronických	elektronický	k2eAgInPc2d1	elektronický
platebních	platební	k2eAgInPc2d1	platební
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
platebních	platební	k2eAgFnPc2d1	platební
karet	kareta	k1gFnPc2	kareta
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
fungování	fungování	k1gNnSc4	fungování
platebních	platební	k2eAgInPc2d1	platební
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
provozuje	provozovat	k5eAaImIp3nS	provozovat
systém	systém	k1gInSc4	systém
mezibankovního	mezibankovní	k2eAgInSc2d1	mezibankovní
platebního	platební	k2eAgInSc2d1	platební
styku	styk	k1gInSc2	styk
CERTIS	CERTIS	kA	CERTIS
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
systém	systém	k1gInSc1	systém
trhu	trh	k1gInSc2	trh
krátkodobých	krátkodobý	k2eAgInPc2d1	krátkodobý
dluhopisů	dluhopis	k1gInPc2	dluhopis
SKD	SKD	kA	SKD
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
vede	vést	k5eAaImIp3nS	vést
účty	účet	k1gInPc4	účet
(	(	kIx(	(
<g/>
ne	ne	k9	ne
běžné	běžný	k2eAgFnSc3d1	běžná
<g/>
)	)	kIx)	)
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
služby	služba	k1gFnSc2	služba
platebního	platební	k2eAgInSc2d1	platební
styku	styk	k1gInSc2	styk
zejména	zejména	k6eAd1	zejména
státu	stát	k1gInSc2	stát
a	a	k8xC	a
bankám	banka	k1gFnPc3	banka
<g/>
.	.	kIx.	.
</s>
<s>
ČNB	ČNB	kA	ČNB
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
bankovní	bankovní	k2eAgFnPc4d1	bankovní
služby	služba	k1gFnPc4	služba
pro	pro	k7c4	pro
stát	stát	k1gInSc4	stát
a	a	k8xC	a
veřejný	veřejný	k2eAgInSc4d1	veřejný
sektor	sektor	k1gInSc4	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
účty	účet	k1gInPc4	účet
organizačním	organizační	k2eAgFnPc3d1	organizační
složkám	složka	k1gFnPc3	složka
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
příspěvkovým	příspěvkový	k2eAgFnPc3d1	příspěvková
organizacím	organizace	k1gFnPc3	organizace
zřízeným	zřízený	k2eAgFnPc3d1	zřízená
organizačními	organizační	k2eAgFnPc7d1	organizační
složkami	složka	k1gFnPc7	složka
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
státním	státní	k2eAgInSc7d1	státní
fondům	fond	k1gInPc3	fond
a	a	k8xC	a
Národnímu	národní	k2eAgInSc3d1	národní
fondu	fond	k1gInSc3	fond
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc6d1	státní
organizaci	organizace	k1gFnSc6	organizace
Správa	správa	k1gFnSc1	správa
železniční	železniční	k2eAgFnSc2d1	železniční
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
územním	územní	k2eAgInPc3d1	územní
samosprávným	samosprávný	k2eAgInPc3d1	samosprávný
celkům	celek	k1gInPc3	celek
(	(	kIx(	(
<g/>
obcím	obec	k1gFnPc3	obec
<g/>
,	,	kIx,	,
krajům	kraj	k1gInPc3	kraj
<g/>
)	)	kIx)	)
a	a	k8xC	a
dobrovolným	dobrovolný	k2eAgInPc3d1	dobrovolný
svazkům	svazek	k1gInPc3	svazek
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
veřejným	veřejný	k2eAgFnPc3d1	veřejná
výzkumným	výzkumný	k2eAgFnPc3d1	výzkumná
institucím	instituce	k1gFnPc3	instituce
<g/>
,	,	kIx,	,
veřejným	veřejný	k2eAgFnPc3d1	veřejná
vysokým	vysoký	k2eAgFnPc3d1	vysoká
školám	škola	k1gFnPc3	škola
<g/>
,	,	kIx,	,
zdravotním	zdravotní	k2eAgFnPc3d1	zdravotní
pojišťovnám	pojišťovna	k1gFnPc3	pojišťovna
a	a	k8xC	a
svazům	svaz	k1gInPc3	svaz
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vede	vést	k5eAaImIp3nS	vést
účty	účet	k1gInPc4	účet
napojené	napojený	k2eAgInPc4d1	napojený
na	na	k7c4	na
rozpočet	rozpočet	k1gInSc4	rozpočet
Evropských	evropský	k2eAgNnPc2d1	Evropské
společenství	společenství	k1gNnPc2	společenství
apod.	apod.	kA	apod.
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
pověření	pověření	k1gNnSc2	pověření
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
provádí	provádět	k5eAaImIp3nS	provádět
operace	operace	k1gFnPc4	operace
spojené	spojený	k2eAgFnPc4d1	spojená
se	s	k7c7	s
státními	státní	k2eAgInPc7d1	státní
cennými	cenný	k2eAgInPc7d1	cenný
papíry	papír	k1gInPc7	papír
<g/>
.	.	kIx.	.
</s>
<s>
Banka	banka	k1gFnSc1	banka
je	být	k5eAaImIp3nS	být
politicky	politicky	k6eAd1	politicky
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc4	její
nezávislost	nezávislost	k1gFnSc4	nezávislost
garantuje	garantovat	k5eAaBmIp3nS	garantovat
Ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
ČNB	ČNB	kA	ČNB
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
řídícím	řídící	k2eAgInSc7d1	řídící
orgánem	orgán	k1gInSc7	orgán
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
je	být	k5eAaImIp3nS	být
sedmičlenná	sedmičlenný	k2eAgFnSc1d1	sedmičlenná
Bankovní	bankovní	k2eAgFnSc1d1	bankovní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
guvernér	guvernér	k1gMnSc1	guvernér
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
viceguvernéři	viceguvernér	k1gMnPc1	viceguvernér
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
čtyři	čtyři	k4xCgMnPc1	čtyři
členové	člen	k1gMnPc1	člen
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc7	člen
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
let	léto	k1gNnPc2	léto
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
kontrasignace	kontrasignace	k1gFnSc2	kontrasignace
nebo	nebo	k8xC	nebo
souhlasu	souhlas	k1gInSc2	souhlas
jiného	jiný	k2eAgInSc2d1	jiný
ústavního	ústavní	k2eAgInSc2d1	ústavní
orgánu	orgán	k1gInSc2	orgán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Odvoláni	odvolán	k2eAgMnPc1d1	odvolán
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
přestanou	přestat	k5eAaPmIp3nP	přestat
splňovat	splňovat	k5eAaImF	splňovat
zákonné	zákonný	k2eAgFnPc4d1	zákonná
podmínky	podmínka	k1gFnPc4	podmínka
výkonu	výkon	k1gInSc2	výkon
funkce	funkce	k1gFnSc2	funkce
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dopustí	dopustit	k5eAaPmIp3nS	dopustit
vážného	vážný	k2eAgNnSc2d1	vážné
pochybení	pochybení	k1gNnSc2	pochybení
<g/>
.	.	kIx.	.
</s>
<s>
Odvolání	odvolání	k1gNnSc1	odvolání
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jmenování	jmenování	k1gNnSc2	jmenování
členů	člen	k1gMnPc2	člen
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
čl	čl	kA	čl
<g/>
.	.	kIx.	.
62	[number]	k4	62
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
tedy	tedy	k8xC	tedy
k	k	k7c3	k
platnosti	platnost	k1gFnSc3	platnost
svého	svůj	k3xOyFgNnSc2	svůj
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
spolupodpis	spolupodpis	k1gInSc4	spolupodpis
(	(	kIx(	(
<g/>
kontrasignaci	kontrasignace	k1gFnSc4	kontrasignace
<g/>
)	)	kIx)	)
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
pověřeného	pověřený	k2eAgMnSc4d1	pověřený
člena	člen	k1gMnSc4	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkami	podmínka	k1gFnPc7	podmínka
pro	pro	k7c4	pro
jmenování	jmenování	k1gNnSc4	jmenování
je	být	k5eAaImIp3nS	být
svéprávnost	svéprávnost	k1gFnSc4	svéprávnost
<g/>
,	,	kIx,	,
bezúhonnost	bezúhonnost	k1gFnSc4	bezúhonnost
<g/>
,	,	kIx,	,
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
uznávanost	uznávanost	k1gFnSc4	uznávanost
v	v	k7c6	v
měnových	měnový	k2eAgFnPc6d1	měnová
záležitostech	záležitost	k1gFnPc6	záležitost
nebo	nebo	k8xC	nebo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
finančního	finanční	k2eAgInSc2d1	finanční
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nesmí	smět	k5eNaImIp3nS	smět
zastávat	zastávat	k5eAaImF	zastávat
funkci	funkce	k1gFnSc4	funkce
člena	člen	k1gMnSc2	člen
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
předsedá	předsedat	k5eAaImIp3nS	předsedat
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
jím	jíst	k5eAaImIp1nS	jíst
pověřený	pověřený	k2eAgMnSc1d1	pověřený
viceguvernér	viceguvernér	k1gMnSc1	viceguvernér
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
přijímá	přijímat	k5eAaImIp3nS	přijímat
svá	svůj	k3xOyFgNnPc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
prostou	prostý	k2eAgFnSc7d1	prostá
většinou	většina	k1gFnSc7	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
usnášeníschopná	usnášeníschopný	k2eAgFnSc1d1	usnášeníschopná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
přítomen	přítomen	k2eAgMnSc1d1	přítomen
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
pověřený	pověřený	k2eAgMnSc1d1	pověřený
předsedající	předsedající	k1gMnSc1	předsedající
viceguvernér	viceguvernér	k1gMnSc1	viceguvernér
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
další	další	k2eAgMnPc1d1	další
tři	tři	k4xCgMnPc1	tři
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
rovnosti	rovnost	k1gFnSc2	rovnost
hlasů	hlas	k1gInPc2	hlas
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
hlas	hlas	k1gInSc1	hlas
předsedajícího	předsedající	k1gMnSc2	předsedající
<g/>
.	.	kIx.	.
</s>
<s>
Jménem	jméno	k1gNnSc7	jméno
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
jedná	jednat	k5eAaImIp3nS	jednat
navenek	navenek	k6eAd1	navenek
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc6	jeho
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
ho	on	k3xPp3gNnSc4	on
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
jím	on	k3xPp3gNnSc7	on
pověřený	pověřený	k2eAgMnSc1d1	pověřený
viceguvernér	viceguvernér	k1gMnSc1	viceguvernér
<g/>
.	.	kIx.	.
</s>
<s>
Guvernér	guvernér	k1gMnSc1	guvernér
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
určený	určený	k2eAgMnSc1d1	určený
viceguvernér	viceguvernér	k1gMnSc1	viceguvernér
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
s	s	k7c7	s
hlasem	hlas	k1gInSc7	hlas
poradním	poradní	k2eAgInSc7d1	poradní
schůze	schůze	k1gFnSc2	schůze
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Guvernér	guvernér	k1gMnSc1	guvernér
také	také	k9	také
předkládá	předkládat	k5eAaImIp3nS	předkládat
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
nejméně	málo	k6eAd3	málo
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
měnovém	měnový	k2eAgInSc6d1	měnový
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgInS	oprávnit
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
schůze	schůze	k1gFnSc2	schůze
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
mu	on	k3xPp3gMnSc3	on
být	být	k5eAaImF	být
uděleno	udělen	k2eAgNnSc4d1	uděleno
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
ústředí	ústředí	k1gNnSc2	ústředí
ČNB	ČNB	kA	ČNB
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
ČNB	ČNB	kA	ČNB
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc7	člen
Bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Rusnok	Rusnok	k1gInSc1	Rusnok
(	(	kIx(	(
<g/>
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
jmenován	jmenován	k2eAgInSc1d1	jmenován
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
Mojmír	Mojmír	k1gMnSc1	Mojmír
Hampl	Hampl	k1gMnSc1	Hampl
(	(	kIx(	(
<g/>
viceguvernér	viceguvernér	k1gMnSc1	viceguvernér
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
jmenován	jmenován	k2eAgInSc1d1	jmenován
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
Vladimír	Vladimír	k1gMnSc1	Vladimír
Tomšík	Tomšík	k1gMnSc1	Tomšík
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
viceguvernér	viceguvernér	k1gMnSc1	viceguvernér
<g/>
)	)	kIx)	)
–	–	k?	–
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
jmenován	jmenován	k2eAgInSc1d1	jmenován
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Benda	Benda	k1gMnSc1	Benda
–	–	k?	–
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
jmenován	jmenován	k2eAgInSc1d1	jmenován
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
Tomáš	Tomáš	k1gMnSc1	Tomáš
Nidetzký	Nidetzký	k2eAgMnSc1d1	Nidetzký
–	–	k?	–
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
Milošem	Miloš	k1gMnSc7	Miloš
<g />
.	.	kIx.	.
</s>
<s>
Zemanem	Zeman	k1gMnSc7	Zeman
Oldřich	Oldřich	k1gMnSc1	Oldřich
Dědek	Dědek	k1gMnSc1	Dědek
–	–	k?	–
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
jmenován	jmenován	k2eAgInSc1d1	jmenován
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
Marek	Marek	k1gMnSc1	Marek
Mora	mora	k1gFnSc1	mora
–	–	k?	–
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
jmenován	jmenován	k2eAgInSc1d1	jmenován
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
Bankovní	bankovní	k2eAgFnSc7d1	bankovní
úřad	úřad	k1gInSc4	úřad
při	při	k7c6	při
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
financí	finance	k1gFnPc2	finance
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
Národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
Československá	československý	k2eAgFnSc1d1	Československá
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
39	[number]	k4	39
a	a	k8xC	a
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
Národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Moravu	Morava	k1gFnSc4	Morava
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
Státní	státní	k2eAgFnSc1d1	státní
banka	banka	k1gFnSc1	banka
československá	československý	k2eAgFnSc1d1	Československá
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
92	[number]	k4	92
Centrální	centrální	k2eAgInSc1d1	centrální
registr	registr	k1gInSc1	registr
úvěrů	úvěr	k1gInPc2	úvěr
ČNB	ČNB	kA	ČNB
Podřízený	podřízený	k2eAgMnSc1d1	podřízený
pojišťovací	pojišťovací	k2eAgMnSc1d1	pojišťovací
zprostředkovatel	zprostředkovatel	k1gMnSc1	zprostředkovatel
Seznam	seznam	k1gInSc4	seznam
členů	člen	k1gMnPc2	člen
Bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
banky	banka	k1gFnSc2	banka
</s>
