<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
(	(	kIx(	(
<g/>
ČNB	ČNB	kA	ČNB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
finančním	finanční	k2eAgInSc7d1	finanční
trhem	trh	k1gInSc7	trh
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
