<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Srbska	Srbsko	k1gNnSc2	Srbsko
je	být	k5eAaImIp3nS	být
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
tradičních	tradiční	k2eAgFnPc2d1	tradiční
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
trikolóra	trikolóra	k1gFnSc1	trikolóra
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnSc2d1	modrá
a	a	k8xC	a
červené	červená	k1gFnSc2	červená
<g/>
.	.	kIx.	.
</s>
