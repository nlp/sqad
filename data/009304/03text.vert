<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Srbska	Srbsko	k1gNnSc2	Srbsko
je	být	k5eAaImIp3nS	být
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
tradičních	tradiční	k2eAgFnPc2d1	tradiční
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
trikolóra	trikolóra	k1gFnSc1	trikolóra
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnSc2d1	modrá
a	a	k8xC	a
červené	červená	k1gFnSc2	červená
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
pruhů	pruh	k1gInPc2	pruh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
interpretováno	interpretovat	k5eAaBmNgNnS	interpretovat
jako	jako	k8xC	jako
obrácená	obrácený	k2eAgFnSc1d1	obrácená
zástava	zástava	k1gFnSc1	zástava
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
;	;	kIx,	;
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
váží	vážit	k5eAaImIp3nP	vážit
dva	dva	k4xCgInPc4	dva
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
prvního	první	k4xOgNnSc2	první
srbského	srbský	k2eAgNnSc2d1	srbské
povstání	povstání	k1gNnSc2	povstání
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
delegace	delegace	k1gFnSc1	delegace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
putovala	putovat	k5eAaImAgFnS	putovat
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
Rusové	Rus	k1gMnPc1	Rus
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
Srbům	Srb	k1gMnPc3	Srb
pomoc	pomoc	k1gFnSc1	pomoc
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
jim	on	k3xPp3gMnPc3	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
chtěli	chtít	k5eAaImAgMnP	chtít
projevit	projevit	k5eAaPmF	projevit
náklonnost	náklonnost	k1gFnSc4	náklonnost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
bojovat	bojovat	k5eAaImF	bojovat
pod	pod	k7c7	pod
ruskou	ruský	k2eAgFnSc7d1	ruská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Srbska	Srbsko	k1gNnSc2	Srbsko
však	však	k9	však
zapomněli	zapomenout	k5eAaPmAgMnP	zapomenout
přinést	přinést	k5eAaPmF	přinést
ruskou	ruský	k2eAgFnSc4d1	ruská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
zapomněli	zapomenout	k5eAaPmAgMnP	zapomenout
uspořádání	uspořádání	k1gNnSc3	uspořádání
pruhů	pruh	k1gInPc2	pruh
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
tak	tak	k9	tak
používat	používat	k5eAaImF	používat
pořadí	pořadí	k1gNnSc4	pořadí
barev	barva	k1gFnPc2	barva
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
dole	dole	k6eAd1	dole
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
verze	verze	k1gFnSc1	verze
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rusové	Rus	k1gMnPc1	Rus
neposkytli	poskytnout	k5eNaPmAgMnP	poskytnout
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zklamaní	zklamaný	k2eAgMnPc1d1	zklamaný
Srbové	Srb	k1gMnPc1	Srb
vrátili	vrátit	k5eAaPmAgMnP	vrátit
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
opovržení	opovržení	k1gNnSc2	opovržení
ruskou	ruský	k2eAgFnSc7d1	ruská
zástavou	zástava	k1gFnSc7	zástava
ji	on	k3xPp3gFnSc4	on
začali	začít	k5eAaPmAgMnP	začít
používat	používat	k5eAaImF	používat
obrácenou	obrácený	k2eAgFnSc4d1	obrácená
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
bývají	bývat	k5eAaImIp3nP	bývat
například	například	k6eAd1	například
vyvěšovány	vyvěšován	k2eAgFnPc1d1	vyvěšována
vlajky	vlajka	k1gFnPc1	vlajka
obráceně	obráceně	k6eAd1	obráceně
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
či	či	k8xC	či
její	její	k3xOp3gFnSc7	její
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Srbskou	srbský	k2eAgFnSc4d1	Srbská
námořní	námořní	k2eAgFnSc4d1	námořní
válečnou	válečný	k2eAgFnSc4d1	válečná
vlajku	vlajka	k1gFnSc4	vlajka
používají	používat	k5eAaImIp3nP	používat
srbské	srbský	k2eAgFnPc1d1	Srbská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
na	na	k7c6	na
říční	říční	k2eAgFnSc6d1	říční
tocích	tok	k1gInPc6	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgFnPc1d1	historická
vlajky	vlajka	k1gFnPc1	vlajka
Srbska	Srbsko	k1gNnSc2	Srbsko
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Srbska	Srbsko	k1gNnSc2	Srbsko
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Srbska	Srbsko	k1gNnSc2	Srbsko
</s>
</p>
<p>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Srbska	Srbsko	k1gNnSc2	Srbsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Srbska	Srbsko	k1gNnSc2	Srbsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Srbská	srbský	k2eAgFnSc1d1	Srbská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
