<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
celého	celý	k2eAgNnSc2d1	celé
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
?	?	kIx.	?
</s>
