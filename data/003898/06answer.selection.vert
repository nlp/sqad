<s>
Nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
je	být	k5eAaImIp3nS	být
Skotská	skotský	k2eAgFnSc1d1	skotská
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
Highlands	Highlands	k1gInSc1	Highlands
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
celého	celý	k2eAgNnSc2d1	celé
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
(	(	kIx(	(
<g/>
1344	[number]	k4	1344
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
