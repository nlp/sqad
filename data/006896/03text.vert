<s>
Schrattenbachův	Schrattenbachův	k2eAgInSc1d1	Schrattenbachův
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgInSc1d1	barokní
palác	palác	k1gInSc1	palác
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
Kobližná	kobližný	k2eAgFnSc1d1	Kobližná
<g/>
,	,	kIx,	,
č.	č.	k?	č.
o.	o.	k?	o.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
centrální	centrální	k2eAgInSc1d1	centrální
objekt	objekt	k1gInSc1	objekt
Knihovny	knihovna	k1gFnSc2	knihovna
Jiřího	Jiří	k1gMnSc2	Jiří
Mahena	Mahen	k1gMnSc2	Mahen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
zapsán	zapsat	k5eAaPmNgInS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
současné	současný	k2eAgFnSc2d1	současná
stavby	stavba	k1gFnSc2	stavba
původně	původně	k6eAd1	původně
stály	stát	k5eAaImAgInP	stát
dva	dva	k4xCgInPc1	dva
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1703	[number]	k4	1703
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
Marie	Marie	k1gFnSc1	Marie
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
hraběnka	hraběnka	k1gFnSc1	hraběnka
Breunerová	Breunerová	k1gFnSc1	Breunerová
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
místě	místo	k1gNnSc6	místo
vystavět	vystavět	k5eAaPmF	vystavět
palác	palác	k1gInSc1	palác
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Christiana	Christian	k1gMnSc4	Christian
Alexandera	Alexander	k1gMnSc4	Alexander
Oedtla	Oedtla	k1gMnSc4	Oedtla
<g/>
,	,	kIx,	,
představitele	představitel	k1gMnSc4	představitel
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
moderny	moderna	k1gFnSc2	moderna
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1725	[number]	k4	1725
získal	získat	k5eAaPmAgMnS	získat
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
kardinál	kardinál	k1gMnSc1	kardinál
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Hannibal	Hannibal	k1gInSc4	Hannibal
hrabě	hrabě	k1gMnSc1	hrabě
ze	z	k7c2	z
Schrattenbachu	Schrattenbach	k1gInSc2	Schrattenbach
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jehož	k3xOyRp3gInSc4	jehož
popud	popud	k1gInSc4	popud
objekt	objekt	k1gInSc4	objekt
v	v	k7c6	v
letech	let	k1gInPc6	let
1735	[number]	k4	1735
<g/>
–	–	k?	–
<g/>
1738	[number]	k4	1738
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
stavitel	stavitel	k1gMnSc1	stavitel
Mořic	Mořic	k1gMnSc1	Mořic
Grimm	Grimm	k1gMnSc1	Grimm
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
zrealizoval	zrealizovat	k5eAaPmAgMnS	zrealizovat
druhé	druhý	k4xOgNnSc4	druhý
patro	patro	k1gNnSc4	patro
<g/>
)	)	kIx)	)
víceméně	víceméně	k9	víceméně
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1767	[number]	k4	1767
a	a	k8xC	a
1768	[number]	k4	1768
zde	zde	k6eAd1	zde
strávil	strávit	k5eAaPmAgMnS	strávit
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Františka	František	k1gMnSc2	František
Antonína	Antonín	k1gMnSc2	Antonín
Schrattenbacha	Schrattenbach	k1gMnSc2	Schrattenbach
vánoční	vánoční	k2eAgInPc1d1	vánoční
svátky	svátek	k1gInPc7	svátek
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
umístěná	umístěný	k2eAgFnSc1d1	umístěná
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
bylo	být	k5eAaImAgNnS	být
přistavěno	přistavěn	k2eAgNnSc1d1	přistavěno
další	další	k2eAgNnSc1d1	další
půlpatro	půlpatro	k1gNnSc1	půlpatro
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
byl	být	k5eAaImAgInS	být
palác	palác	k1gInSc1	palác
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
podnikatele	podnikatel	k1gMnSc2	podnikatel
Theodora	Theodor	k1gMnSc2	Theodor
Bauera	Bauer	k1gMnSc2	Bauer
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
částečně	částečně	k6eAd1	částečně
znehodnocen	znehodnocen	k2eAgInSc1d1	znehodnocen
obchodním	obchodní	k2eAgNnSc7d1	obchodní
využitím	využití	k1gNnSc7	využití
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
sídlila	sídlit	k5eAaImAgFnS	sídlit
Úrazová	úrazový	k2eAgFnSc1d1	Úrazová
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
Zemské	zemský	k2eAgNnSc4d1	zemské
vojenské	vojenský	k2eAgNnSc4d1	vojenské
velitelství	velitelství	k1gNnSc4	velitelství
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Schrattenbachův	Schrattenbachův	k2eAgInSc1d1	Schrattenbachův
palác	palác	k1gInSc1	palác
poškozen	poškodit	k5eAaPmNgInS	poškodit
a	a	k8xC	a
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
zboření	zboření	k1gNnSc6	zboření
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
centrála	centrála	k1gFnSc1	centrála
Knihovny	knihovna	k1gFnSc2	knihovna
Jiřího	Jiří	k1gMnSc2	Jiří
Mahena	Mahen	k1gMnSc2	Mahen
<g/>
,	,	kIx,	,
generální	generální	k2eAgFnSc1d1	generální
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
objektu	objekt	k1gInSc2	objekt
ale	ale	k8xC	ale
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
až	až	k9	až
po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
havarijního	havarijní	k2eAgInSc2d1	havarijní
stavu	stav	k1gInSc2	stav
konstrukcí	konstrukce	k1gFnPc2	konstrukce
v	v	k7c6	v
letech	let	k1gInPc6	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zbořeno	zbořen	k2eAgNnSc4d1	zbořeno
nehodnotné	hodnotný	k2eNgNnSc4d1	nehodnotné
jižní	jižní	k2eAgNnSc4d1	jižní
křídlo	křídlo	k1gNnSc4	křídlo
a	a	k8xC	a
dvorní	dvorní	k2eAgFnSc2d1	dvorní
vestavby	vestavba	k1gFnSc2	vestavba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvoře	dvůr	k1gInSc6	dvůr
byla	být	k5eAaImAgFnS	být
naopak	naopak	k6eAd1	naopak
instalována	instalovat	k5eAaBmNgFnS	instalovat
moderní	moderní	k2eAgFnSc1d1	moderní
ocelová	ocelový	k2eAgFnSc1d1	ocelová
konstrukce	konstrukce	k1gFnSc1	konstrukce
s	s	k7c7	s
ochozy	ochoz	k1gInPc7	ochoz
a	a	k8xC	a
skleněným	skleněný	k2eAgInSc7d1	skleněný
jehlanem	jehlan	k1gInSc7	jehlan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dvůr	dvůr	k1gInSc1	dvůr
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tak	tak	k9	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ústřední	ústřední	k2eAgInSc1d1	ústřední
prostor	prostor	k1gInSc1	prostor
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Schrattenbachův	Schrattenbachův	k2eAgInSc4d1	Schrattenbachův
palác	palác	k1gInSc4	palác
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Historie	historie	k1gFnSc1	historie
Schrattenbachova	Schrattenbachův	k2eAgInSc2d1	Schrattenbachův
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
kjm	kjm	k?	kjm
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
