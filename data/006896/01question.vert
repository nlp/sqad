<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
barokní	barokní	k2eAgInSc1d1	barokní
palác	palác	k1gInSc1	palác
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
centrální	centrální	k2eAgInSc1d1	centrální
objekt	objekt	k1gInSc1	objekt
Knihovny	knihovna	k1gFnSc2	knihovna
Jiřího	Jiří	k1gMnSc2	Jiří
Mahena	Mahen	k1gMnSc2	Mahen
<g/>
?	?	kIx.	?
</s>
