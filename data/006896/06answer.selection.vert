<s>
Schrattenbachův	Schrattenbachův	k2eAgInSc1d1	Schrattenbachův
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgInSc1d1	barokní
palác	palác	k1gInSc1	palác
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
Kobližná	kobližný	k2eAgFnSc1d1	Kobližná
<g/>
,	,	kIx,	,
č.	č.	k?	č.
o.	o.	k?	o.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
