<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1939	[number]	k4	1939
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Velká	velký	k2eAgFnSc1d1	velká
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
globální	globální	k2eAgInSc1d1	globální
válečný	válečný	k2eAgInSc1d1	válečný
konflikt	konflikt	k1gInSc1	konflikt
probíhající	probíhající	k2eAgInSc1d1	probíhající
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
