<p>
<s>
Tran	Tran	k1gMnSc1	Tran
Thu	Thu	k1gFnSc2	Thu
Ha	ha	kA	ha
<g/>
,	,	kIx,	,
vietnamsky	vietnamsky	k6eAd1	vietnamsky
Trầ	Trầ	k1gMnSc1	Trầ
Thu	Thu	k1gMnSc1	Thu
Hà	Hà	k1gMnSc1	Hà
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Hanoj	Hanoj	k1gFnSc1	Hanoj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
umělecké	umělecký	k2eAgNnSc4d1	umělecké
jméno	jméno	k1gNnSc4	jméno
je	být	k5eAaImIp3nS	být
Ha	ha	kA	ha
Tran	Tran	k1gInSc1	Tran
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Tran	Tran	k1gMnSc1	Tran
Hien	Hien	k1gMnSc1	Hien
je	být	k5eAaImIp3nS	být
také	také	k9	také
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Alba	album	k1gNnSc2	album
==	==	k?	==
</s>
</p>
<p>
<s>
Tran	Tran	k1gMnSc1	Tran
Tien	Tien	k1gMnSc1	Tien
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Century	Centura	k1gFnPc1	Centura
of	of	k?	of
love	lov	k1gInSc5	lov
songs	songsa	k1gFnPc2	songsa
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Communication	Communication	k1gInSc1	Communication
06	[number]	k4	06
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ha	ha	kA	ha
Tran	Tran	k1gInSc4	Tran
98-03	[number]	k4	98-03
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
–	–	k?	–
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
</s>
</p>
<p>
<s>
Colors	Colors	k1gInSc1	Colors
–	–	k?	–
Love	lov	k1gInSc5	lov
songs	songsa	k1gFnPc2	songsa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Solar	Solar	k1gInSc1	Solar
Eclipse	Eclipse	k1gFnSc2	Eclipse
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Love	lov	k1gInSc5	lov
song	song	k1gInSc1	song
for	forum	k1gNnPc2	forum
beauty	beaut	k2eAgFnPc1d1	beaut
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Purity	Purita	k1gFnPc1	Purita
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tran	Tran	k1gMnSc1	Tran
Thu	Thu	k1gFnSc4	Thu
Ha	ha	kA	ha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
