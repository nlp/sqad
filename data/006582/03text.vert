<s>
Vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
je	být	k5eAaImIp3nS	být
vitamín	vitamín	k1gInSc4	vitamín
rozpustný	rozpustný	k2eAgInSc4d1	rozpustný
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
K	K	kA	K
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
německého	německý	k2eAgNnSc2d1	německé
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
Koagulation	Koagulation	k1gInSc1	Koagulation
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
procesu	proces	k1gInSc2	proces
srážení	srážení	k1gNnSc2	srážení
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
je	být	k5eAaImIp3nS	být
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
několika	několik	k4yIc2	několik
proteinů	protein	k1gInPc2	protein
podílejících	podílející	k2eAgInPc2d1	podílející
se	se	k3xPyFc4	se
na	na	k7c4	na
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
mineralizace	mineralizace	k1gFnSc2	mineralizace
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
buněčného	buněčný	k2eAgInSc2d1	buněčný
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
metabolismu	metabolismus	k1gInSc2	metabolismus
proteinů	protein	k1gInPc2	protein
cévní	cévní	k2eAgFnSc2d1	cévní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
formy	forma	k1gFnPc1	forma
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
fylochinon	fylochinon	k1gInSc1	fylochinon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
syntetizován	syntetizovat	k5eAaImNgMnS	syntetizovat
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
menachinon	menachinon	k1gInSc1	menachinon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
produkován	produkovat	k5eAaImNgMnS	produkovat
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K2	K2	k1gFnSc2	K2
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
opakujících	opakující	k2eAgFnPc2d1	opakující
se	se	k3xPyFc4	se
izoprenových	izoprenův	k2eAgFnPc2d1	izoprenův
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
bočním	boční	k2eAgInSc6d1	boční
řetězci	řetězec	k1gInSc6	řetězec
molekuly	molekula	k1gFnSc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podtypy	podtyp	k1gInPc4	podtyp
vitamínu	vitamín	k1gInSc2	vitamín
K2	K2	k1gFnSc2	K2
používáme	používat	k5eAaImIp1nP	používat
zkratku	zkratka	k1gFnSc4	zkratka
MK	MK	kA	MK
a	a	k8xC	a
přidáváme	přidávat	k5eAaImIp1nP	přidávat
specifikaci	specifikace	k1gFnSc4	specifikace
číslem	číslo	k1gNnSc7	číslo
n	n	k0	n
(	(	kIx(	(
<g/>
MK-n	MKo	k1gNnPc2	MK-no
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k9	kde
n	n	k0	n
označuje	označovat	k5eAaImIp3nS	označovat
počet	počet	k1gInSc1	počet
izoprenových	izoprenův	k2eAgFnPc2d1	izoprenův
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
jsou	být	k5eAaImIp3nP	být
podtypy	podtyp	k1gInPc1	podtyp
MK-	MK-	k1gFnSc2	MK-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
MK-	MK-	k1gFnSc1	MK-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
MK-	MK-	k1gFnSc1	MK-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
MK-	MK-	k1gFnSc1	MK-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
organizmus	organizmus	k1gInSc1	organizmus
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
bakterie	bakterie	k1gFnSc2	bakterie
přítomné	přítomný	k2eAgFnSc2d1	přítomná
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organizmu	organizmus	k1gInSc6	organizmus
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
MK-	MK-	k1gFnPc1	MK-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
MK-4	MK-4	k4	MK-4
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
bakteriemi	bakterie	k1gFnPc7	bakterie
vyráběn	vyráběn	k2eAgInSc4d1	vyráběn
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
v	v	k7c6	v
dostatečném	dostatečný	k2eAgNnSc6d1	dostatečné
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
syntetický	syntetický	k2eAgInSc1d1	syntetický
vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
menadion	menadion	k1gInSc1	menadion
<g/>
,	,	kIx,	,
přidávaný	přidávaný	k2eAgInSc1d1	přidávaný
do	do	k7c2	do
zvířecích	zvířecí	k2eAgNnPc2d1	zvířecí
krmiv	krmivo	k1gNnPc2	krmivo
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
K2	K2	k1gFnSc1	K2
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
MK-	MK-	k1gFnSc1	MK-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Menadion	Menadion	k1gInSc1	Menadion
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
podávat	podávat	k5eAaImF	podávat
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
K1	K1	k1gMnPc1	K1
a	a	k8xC	a
K2	K2	k1gMnPc1	K2
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
využívány	využíván	k2eAgMnPc4d1	využíván
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
K1	K1	k1gMnSc1	K1
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
především	především	k9	především
pro	pro	k7c4	pro
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
orgánem	orgán	k1gInSc7	orgán
působení	působení	k1gNnSc2	působení
jsou	být	k5eAaImIp3nP	být
játra	játra	k1gNnPc4	játra
<g/>
,	,	kIx,	,
K2	K2	k1gFnPc4	K2
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
nekoagulačních	koagulační	k2eNgInPc6d1	koagulační
dějích	děj	k1gInPc6	děj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
metabolismu	metabolismus	k1gInSc6	metabolismus
a	a	k8xC	a
mineralizaci	mineralizace	k1gFnSc6	mineralizace
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
v	v	k7c6	v
buněčném	buněčný	k2eAgInSc6d1	buněčný
růstu	růst	k1gInSc6	růst
a	a	k8xC	a
v	v	k7c6	v
metabolismu	metabolismus	k1gInSc6	metabolismus
buněk	buňka	k1gFnPc2	buňka
cévní	cévní	k2eAgFnSc2d1	cévní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K2	K2	k1gFnSc2	K2
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
MK-	MK-	k1gMnSc2	MK-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
orgánech	orgán	k1gInPc6	orgán
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
než	než	k8xS	než
fylochinon	fylochinon	k1gInSc4	fylochinon
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
unikátní	unikátní	k2eAgFnSc2d1	unikátní
cesty	cesta	k1gFnSc2	cesta
jeho	jeho	k3xOp3gFnSc2	jeho
syntézy	syntéza	k1gFnSc2	syntéza
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
K2	K2	k1gFnSc1	K2
má	mít	k5eAaImIp3nS	mít
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
biologickou	biologický	k2eAgFnSc4d1	biologická
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
popisována	popisován	k2eAgFnSc1d1	popisována
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
kofaktor	kofaktor	k1gInSc4	kofaktor
pro	pro	k7c4	pro
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
katalyzuje	katalyzovat	k5eAaPmIp3nS	katalyzovat
karboxylaci	karboxylace	k1gFnSc4	karboxylace
kyseliny	kyselina	k1gFnSc2	kyselina
glutamové	glutamový	k2eAgFnSc2d1	glutamová
na	na	k7c4	na
gama-karboxyglutamovou	gamaarboxyglutamový	k2eAgFnSc4d1	gama-karboxyglutamový
(	(	kIx(	(
<g/>
GLA	GLA	kA	GLA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
dependentní	dependentní	k2eAgFnSc2d1	dependentní
gama-karboxylace	gamaarboxylace	k1gFnSc2	gama-karboxylace
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
specifických	specifický	k2eAgFnPc2d1	specifická
reziduí	reziduum	k1gNnPc2	reziduum
kyseliny	kyselina	k1gFnSc2	kyselina
glutamové	glutamový	k2eAgFnSc2d1	glutamová
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tato	tento	k3xDgNnPc1	tento
rezidua	reziduum	k1gNnPc1	reziduum
správně	správně	k6eAd1	správně
vázala	vázat	k5eAaImAgNnP	vázat
vápník	vápník	k1gInSc4	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
vázat	vázat	k5eAaImF	vázat
vápenaté	vápenatý	k2eAgInPc4d1	vápenatý
ionty	ion	k1gInPc4	ion
(	(	kIx(	(
<g/>
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
aktivaci	aktivace	k1gFnSc4	aktivace
sedmi	sedm	k4xCc2	sedm
vitamín	vitamín	k1gInSc4	vitamín
K	K	kA	K
dependentních	dependentní	k2eAgInPc2d1	dependentní
faktorů	faktor	k1gInPc2	faktor
srážlivosti	srážlivost	k1gFnSc2	srážlivost
(	(	kIx(	(
<g/>
proteinů	protein	k1gInPc2	protein
<g/>
)	)	kIx)	)
v	v	k7c6	v
koagulační	koagulační	k2eAgFnSc6d1	koagulační
kaskádě	kaskáda	k1gFnSc6	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
koagulační	koagulační	k2eAgFnSc1d1	koagulační
kaskáda	kaskáda	k1gFnSc1	kaskáda
znamená	znamenat	k5eAaImIp3nS	znamenat
řadu	řada	k1gFnSc4	řada
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
předcházející	předcházející	k2eAgInSc4d1	předcházející
<g/>
,	,	kIx,	,
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zastavení	zastavení	k1gNnSc4	zastavení
krvácení	krvácení	k1gNnSc2	krvácení
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
sraženinu	sraženina	k1gFnSc4	sraženina
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc4	vitamín
K	K	kA	K
dependentní	dependentní	k2eAgFnSc1d1	dependentní
gama-karboxylace	gamaarboxylace	k1gFnSc1	gama-karboxylace
reziduí	reziduum	k1gNnPc2	reziduum
kyseliny	kyselina	k1gFnSc2	kyselina
glutamové	glutamový	k2eAgFnSc2d1	glutamová
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
proteinům	protein	k1gInPc3	protein
vázat	vázat	k5eAaImF	vázat
vápník	vápník	k1gInSc1	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Faktory	faktor	k1gInPc1	faktor
II	II	kA	II
(	(	kIx(	(
<g/>
protrombin	protrombin	k2eAgMnSc1d1	protrombin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
VII	VII	kA	VII
<g/>
,	,	kIx,	,
IX	IX	kA	IX
a	a	k8xC	a
X	X	kA	X
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc4	základ
koagulační	koagulační	k2eAgFnSc2d1	koagulační
kaskády	kaskáda	k1gFnSc2	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Protein	protein	k1gInSc1	protein
Z	Z	kA	Z
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
účinek	účinek	k1gInSc4	účinek
trombinu	trombin	k1gInSc2	trombin
(	(	kIx(	(
<g/>
aktivované	aktivovaný	k2eAgFnSc6d1	aktivovaná
formě	forma	k1gFnSc6	forma
protrombinu	protrombina	k1gFnSc4	protrombina
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporuje	podporovat	k5eAaImIp3nS	podporovat
jeho	jeho	k3xOp3gNnSc1	jeho
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
fosfolipidy	fosfolipid	k1gMnPc7	fosfolipid
v	v	k7c6	v
buněčných	buněčný	k2eAgFnPc6d1	buněčná
membránách	membrána	k1gFnPc6	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Proteiny	protein	k1gInPc1	protein
C	C	kA	C
a	a	k8xC	a
S	s	k7c7	s
jsou	být	k5eAaImIp3nP	být
antikoagulační	antikoagulační	k2eAgFnPc4d1	antikoagulační
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
kontrolu	kontrola	k1gFnSc4	kontrola
a	a	k8xC	a
rovnováhu	rovnováha	k1gFnSc4	rovnováha
v	v	k7c6	v
koagulační	koagulační	k2eAgFnSc6d1	koagulační
kaskádě	kaskáda	k1gFnSc6	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Protein	protein	k1gInSc1	protein
Z	Z	kA	Z
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
antikoagulační	antikoagulační	k2eAgFnSc4d1	antikoagulační
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
dependentní	dependentní	k2eAgInPc1d1	dependentní
koagulační	koagulační	k2eAgInPc1d1	koagulační
faktory	faktor	k1gInPc1	faktor
jsou	být	k5eAaImIp3nP	být
syntetizovány	syntetizovat	k5eAaImNgInP	syntetizovat
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
Závažná	závažný	k2eAgNnPc4d1	závažné
onemocnění	onemocnění	k1gNnPc4	onemocnění
jater	játra	k1gNnPc2	játra
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
hladiny	hladina	k1gFnSc2	hladina
vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
dependentních	dependentní	k2eAgInPc2d1	dependentní
koagulačních	koagulační	k2eAgInPc2d1	koagulační
faktorů	faktor	k1gInPc2	faktor
a	a	k8xC	a
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
riziku	riziko	k1gNnSc3	riziko
nekontrolovaného	kontrolovaný	k2eNgNnSc2d1	nekontrolované
krvácení	krvácení	k1gNnSc2	krvácení
(	(	kIx(	(
<g/>
hemoragie	hemoragie	k1gFnSc1	hemoragie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
tvorby	tvorba	k1gFnSc2	tvorba
sraženin	sraženina	k1gFnPc2	sraženina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
blokovat	blokovat	k5eAaImF	blokovat
průtok	průtok	k1gInSc4	průtok
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
tepnách	tepna	k1gFnPc6	tepna
na	na	k7c6	na
srdci	srdce	k1gNnSc6	srdce
<g/>
,	,	kIx,	,
mozku	mozek	k1gInSc6	mozek
nebo	nebo	k8xC	nebo
plicích	plíce	k1gFnPc6	plíce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
infarktu	infarkt	k1gInSc3	infarkt
<g/>
,	,	kIx,	,
mozkové	mozkový	k2eAgFnSc6d1	mozková
mrtvici	mrtvice	k1gFnSc6	mrtvice
nebo	nebo	k8xC	nebo
plicní	plicní	k2eAgFnSc3d1	plicní
embolii	embolie	k1gFnSc3	embolie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
perorální	perorální	k2eAgFnSc1d1	perorální
antikoagulancia	antikoagulancia	k1gFnSc1	antikoagulancia
(	(	kIx(	(
<g/>
léky	lék	k1gInPc1	lék
snižující	snižující	k2eAgFnSc4d1	snižující
krevní	krevní	k2eAgFnSc4d1	krevní
srážlivost	srážlivost	k1gFnSc4	srážlivost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
warfarin	warfarin	k1gInSc1	warfarin
<g/>
.	.	kIx.	.
</s>
<s>
Warfarin	Warfarin	k1gInSc1	Warfarin
brání	bránit	k5eAaImIp3nS	bránit
regeneraci	regenerace	k1gFnSc4	regenerace
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
jeho	jeho	k3xOp3gInSc4	jeho
nedostatek	nedostatek	k1gInSc4	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
vitamín	vitamín	k1gInSc4	vitamín
K	k	k7c3	k
rozpustný	rozpustný	k2eAgInSc4d1	rozpustný
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
<g/>
,	,	kIx,	,
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
je	být	k5eAaImIp3nS	být
uchováván	uchovávat	k5eAaImNgInS	uchovávat
jen	jen	k9	jen
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
denního	denní	k2eAgInSc2d1	denní
příjmu	příjem	k1gInSc2	příjem
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
zásoba	zásoba	k1gFnSc1	zásoba
rychle	rychle	k6eAd1	rychle
vyčerpána	vyčerpán	k2eAgFnSc1d1	vyčerpána
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
omezené	omezený	k2eAgFnSc3d1	omezená
schopnosti	schopnost	k1gFnSc3	schopnost
uchovávat	uchovávat	k5eAaImF	uchovávat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
tělo	tělo	k1gNnSc1	tělo
dokáže	dokázat	k5eAaPmIp3nS	dokázat
recyklovat	recyklovat	k5eAaBmF	recyklovat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
cyklu	cyklus	k1gInSc2	cyklus
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Regenerační	regenerační	k2eAgInSc4d1	regenerační
cyklus	cyklus	k1gInSc4	cyklus
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
je	být	k5eAaImIp3nS	být
narušen	narušit	k5eAaPmNgInS	narušit
až	až	k8xS	až
zastaven	zastavit	k5eAaPmNgInS	zastavit
působením	působení	k1gNnSc7	působení
perorálních	perorální	k2eAgFnPc2d1	perorální
antikoagulancií	antikoagulancie	k1gFnPc2	antikoagulancie
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
nedostatek	nedostatek	k1gInSc4	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
karboxylace	karboxylace	k1gFnSc1	karboxylace
(	(	kIx(	(
<g/>
aktivace	aktivace	k1gFnSc1	aktivace
<g/>
)	)	kIx)	)
vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
dependentních	dependentní	k2eAgInPc2d1	dependentní
koagulačních	koagulační	k2eAgInPc2d1	koagulační
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Koagulační	koagulační	k2eAgInPc1d1	koagulační
faktory	faktor	k1gInPc1	faktor
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
aktivovány	aktivovat	k5eAaBmNgInP	aktivovat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
brzdí	brzdit	k5eAaImIp3nS	brzdit
tvorba	tvorba	k1gFnSc1	tvorba
krevní	krevní	k2eAgFnSc2d1	krevní
sraženiny	sraženina	k1gFnSc2	sraženina
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
příjem	příjem	k1gInSc1	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
může	moct	k5eAaImIp3nS	moct
překonat	překonat	k5eAaPmF	překonat
antikoagulační	antikoagulační	k2eAgInSc4d1	antikoagulační
účinek	účinek	k1gInSc4	účinek
antagonistů	antagonista	k1gMnPc2	antagonista
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Pacienti	pacient	k1gMnPc1	pacient
užívající	užívající	k2eAgMnPc1d1	užívající
tyto	tento	k3xDgInPc1	tento
léky	lék	k1gInPc1	lék
musí	muset	k5eAaImIp3nP	muset
hlídat	hlídat	k5eAaImF	hlídat
příjem	příjem	k1gInSc4	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
radí	radit	k5eAaImIp3nP	radit
konzumovat	konzumovat	k5eAaBmF	konzumovat
stabilní	stabilní	k2eAgNnSc4d1	stabilní
množství	množství	k1gNnSc4	množství
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
výkyvů	výkyv	k1gInPc2	výkyv
<g/>
.	.	kIx.	.
</s>
<s>
Stabilita	stabilita	k1gFnSc1	stabilita
v	v	k7c6	v
příjmu	příjem	k1gInSc6	příjem
neznamená	znamenat	k5eNaImIp3nS	znamenat
restrikci	restrikce	k1gFnSc4	restrikce
v	v	k7c6	v
příjmu	příjem	k1gInSc6	příjem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
lidé	člověk	k1gMnPc1	člověk
užívající	užívající	k2eAgMnPc1d1	užívající
warfarin	warfarin	k1gInSc1	warfarin
mohou	moct	k5eAaImIp3nP	moct
přijímat	přijímat	k5eAaImF	přijímat
90-120	[number]	k4	90-120
mikrogramů	mikrogram	k1gInPc2	mikrogram
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
současná	současný	k2eAgNnPc4d1	současné
dietní	dietní	k2eAgNnPc4d1	dietní
doporučení	doporučení	k1gNnPc4	doporučení
pro	pro	k7c4	pro
zdravou	zdravý	k2eAgFnSc4d1	zdravá
dospělou	dospělý	k2eAgFnSc4d1	dospělá
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostech	kost	k1gFnPc6	kost
byly	být	k5eAaImAgFnP	být
izolovány	izolovat	k5eAaBmNgFnP	izolovat
tři	tři	k4xCgFnPc1	tři
vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
dependentní	dependentní	k2eAgInPc1d1	dependentní
proteiny	protein	k1gInPc7	protein
<g/>
:	:	kIx,	:
osteokalcin	osteokalcin	k1gInSc1	osteokalcin
<g/>
,	,	kIx,	,
matrix	matrix	k1gInSc1	matrix
GLA	GLA	kA	GLA
protein	protein	k1gInSc1	protein
(	(	kIx(	(
<g/>
MGP	MGP	kA	MGP
<g/>
)	)	kIx)	)
a	a	k8xC	a
protein	protein	k1gInSc4	protein
S.	S.	kA	S.
Osteokalcin	Osteokalcin	k2eAgMnSc1d1	Osteokalcin
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
také	také	k6eAd1	také
kostní	kostní	k2eAgMnSc1d1	kostní
GLA	GLA	kA	GLA
protein	protein	k1gInSc4	protein
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
protein	protein	k1gInSc4	protein
syntetizovaný	syntetizovaný	k2eAgInSc4d1	syntetizovaný
osteoblasty	osteoblast	k1gInPc4	osteoblast
(	(	kIx(	(
<g/>
buňkami	buňka	k1gFnPc7	buňka
tvořícími	tvořící	k2eAgFnPc7d1	tvořící
kost	kost	k1gFnSc4	kost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syntéza	syntéza	k1gFnSc1	syntéza
osteokalcinu	osteokalcin	k1gInSc2	osteokalcin
osteoblasty	osteoblast	k1gInPc1	osteoblast
je	on	k3xPp3gFnPc4	on
regulována	regulován	k2eAgFnSc1d1	regulována
aktivní	aktivní	k2eAgFnSc1d1	aktivní
formu	forma	k1gFnSc4	forma
vitamínu	vitamín	k1gInSc2	vitamín
D	D	kA	D
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
1,25	[number]	k4	1,25
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
kalcitriolem	kalcitriol	k1gInSc7	kalcitriol
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
osteokalcinu	osteokalcin	k1gInSc2	osteokalcin
vázat	vázat	k5eAaImF	vázat
minerály	minerál	k1gInPc4	minerál
je	být	k5eAaImIp3nS	být
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
vitamín	vitamín	k1gInSc4	vitamín
K	K	kA	K
dependentní	dependentní	k2eAgFnSc2d1	dependentní
gama-karboxylací	gamaarboxylace	k1gFnSc7	gama-karboxylace
tří	tři	k4xCgInPc2	tři
zbytků	zbytek	k1gInPc2	zbytek
(	(	kIx(	(
<g/>
reziduí	reziduum	k1gNnPc2	reziduum
<g/>
)	)	kIx)	)
kyseliny	kyselina	k1gFnSc2	kyselina
glutamové	glutamový	k2eAgFnSc2d1	glutamová
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
osteokalcinu	osteokalcin	k1gInSc2	osteokalcin
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
kostní	kostní	k2eAgFnSc7d1	kostní
mineralizací	mineralizace	k1gFnSc7	mineralizace
<g/>
.	.	kIx.	.
</s>
<s>
MGP	MGP	kA	MGP
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
kostech	kost	k1gFnPc6	kost
<g/>
,	,	kIx,	,
chrupavkách	chrupavka	k1gFnPc6	chrupavka
a	a	k8xC	a
měkkých	měkký	k2eAgFnPc6d1	měkká
tkáních	tkáň	k1gFnPc6	tkáň
včetně	včetně	k7c2	včetně
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
studií	studie	k1gFnPc2	studie
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
MGP	MGP	kA	MGP
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
kalcifikaci	kalcifikace	k1gFnSc4	kalcifikace
měkkých	měkký	k2eAgFnPc2d1	měkká
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
chrupavek	chrupavka	k1gFnPc2	chrupavka
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
normální	normální	k2eAgInSc4d1	normální
růst	růst	k1gInSc4	růst
a	a	k8xC	a
zrání	zrání	k1gNnSc4	zrání
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
dependentní	dependentní	k2eAgInSc1d1	dependentní
antikoagulační	antikoagulační	k2eAgInSc1d1	antikoagulační
protein	protein	k1gInSc1	protein
S	s	k7c7	s
je	on	k3xPp3gInPc4	on
syntetizován	syntetizován	k2eAgInSc4d1	syntetizován
osteoblasty	osteoblast	k1gInPc4	osteoblast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
role	role	k1gFnSc1	role
v	v	k7c6	v
kostním	kostní	k2eAgInSc6d1	kostní
metabolismu	metabolismus	k1gInSc6	metabolismus
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
se	s	k7c7	s
zděděnou	zděděný	k2eAgFnSc7d1	zděděná
deficiencí	deficience	k1gFnSc7	deficience
proteinu	protein	k1gInSc2	protein
S	s	k7c7	s
trpí	trpět	k5eAaImIp3nP	trpět
komplikacemi	komplikace	k1gFnPc7	komplikace
souvisejícími	související	k2eAgFnPc7d1	související
se	se	k3xPyFc4	se
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
krevní	krevní	k2eAgFnSc7d1	krevní
srážlivostí	srážlivost	k1gFnSc7	srážlivost
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
hustotou	hustota	k1gFnSc7	hustota
kostního	kostní	k2eAgInSc2d1	kostní
minerálu	minerál	k1gInSc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Gas	Gas	k?	Gas
<g/>
6	[number]	k4	6
je	být	k5eAaImIp3nS	být
vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
dependentní	dependentní	k2eAgInSc1d1	dependentní
protein	protein	k1gInSc1	protein
objevený	objevený	k2eAgInSc1d1	objevený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
nervovém	nervový	k2eAgInSc6d1	nervový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
<g/>
,	,	kIx,	,
plicích	plíce	k1gFnPc6	plíce
<g/>
,	,	kIx,	,
žaludku	žaludek	k1gInSc6	žaludek
<g/>
,	,	kIx,	,
ledvinách	ledvina	k1gFnPc6	ledvina
a	a	k8xC	a
chrupavkách	chrupavka	k1gFnPc6	chrupavka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
přesný	přesný	k2eAgInSc1d1	přesný
mechanismus	mechanismus	k1gInSc1	mechanismus
jeho	jeho	k3xOp3gInSc2	jeho
účinku	účinek	k1gInSc2	účinek
nebyl	být	k5eNaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
<g/>
,	,	kIx,	,
Gas	Gas	k1gFnSc1	Gas
<g/>
6	[number]	k4	6
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
regulační	regulační	k2eAgInSc1d1	regulační
faktor	faktor	k1gInSc1	faktor
buněčného	buněčný	k2eAgInSc2d1	buněčný
růstu	růst	k1gInSc2	růst
s	s	k7c7	s
buněčně-signálními	buněčněignální	k2eAgFnPc7d1	buněčně-signální
aktivitami	aktivita	k1gFnPc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Gas	Gas	k?	Gas
<g/>
6	[number]	k4	6
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
u	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
buněčných	buněčný	k2eAgFnPc2d1	buněčná
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
při	při	k7c6	při
buněčné	buněčný	k2eAgFnSc6d1	buněčná
adhezi	adheze	k1gFnSc6	adheze
<g/>
,	,	kIx,	,
proliferaci	proliferace	k1gFnSc3	proliferace
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
ochraně	ochrana	k1gFnSc3	ochrana
proti	proti	k7c3	proti
apoptóze	apoptóza	k1gFnSc3	apoptóza
<g/>
.6	.6	k4	.6
Může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
stárnutí	stárnutí	k1gNnSc1	stárnutí
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
při	při	k7c6	při
regulaci	regulace	k1gFnSc6	regulace
krevních	krevní	k2eAgFnPc2d1	krevní
destiček	destička	k1gFnPc2	destička
a	a	k8xC	a
při	při	k7c6	při
cévní	cévní	k2eAgFnSc6d1	cévní
homeostáze	homeostáza	k1gFnSc6	homeostáza
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
poruchám	porucha	k1gFnPc3	porucha
krevní	krevní	k2eAgFnSc2d1	krevní
srážlivosti	srážlivost	k1gFnSc2	srážlivost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
prokazují	prokazovat	k5eAaImIp3nP	prokazovat
laboratorní	laboratorní	k2eAgInPc4d1	laboratorní
testy	test	k1gInPc4	test
měřící	měřící	k2eAgInSc4d1	měřící
koagulační	koagulační	k2eAgInSc4d1	koagulační
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
příznaky	příznak	k1gInPc7	příznak
patří	patřit	k5eAaImIp3nP	patřit
modřiny	modřina	k1gFnSc2	modřina
<g/>
,	,	kIx,	,
krvácení	krvácení	k1gNnSc2	krvácení
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
dásní	dáseň	k1gFnPc2	dáseň
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
a	a	k8xC	a
ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
extrémně	extrémně	k6eAd1	extrémně
těžké	těžký	k2eAgNnSc4d1	těžké
menstruační	menstruační	k2eAgNnSc4d1	menstruační
krvácení	krvácení	k1gNnSc4	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kojenců	kojenec	k1gMnPc2	kojenec
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
nedostatek	nedostatek	k1gInSc4	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
k	k	k7c3	k
život	život	k1gInSc4	život
ohrožujícímu	ohrožující	k2eAgNnSc3d1	ohrožující
krvácení	krvácení	k1gNnSc3	krvácení
(	(	kIx(	(
<g/>
k	k	k7c3	k
intrakraniální	intrakraniální	k2eAgFnSc3d1	intrakraniální
hemoragii	hemoragie	k1gFnSc3	hemoragie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
dále	daleko	k6eAd2	daleko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
řídnutím	řídnutí	k1gNnSc7	řídnutí
kostí	kost	k1gFnPc2	kost
(	(	kIx(	(
<g/>
osteoporózou	osteoporóza	k1gFnSc7	osteoporóza
<g/>
)	)	kIx)	)
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
po	po	k7c6	po
menopauze	menopauza	k1gFnSc6	menopauza
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
špatnou	špatný	k2eAgFnSc4d1	špatná
karboxylaci	karboxylace	k1gFnSc4	karboxylace
osteokalcinu	osteokalcin	k2eAgFnSc4d1	osteokalcin
a	a	k8xC	a
sníženou	snížený	k2eAgFnSc4d1	snížená
aktivitu	aktivita	k1gFnSc4	aktivita
osteoblastů	osteoblast	k1gInPc2	osteoblast
(	(	kIx(	(
<g/>
kostních	kostní	k2eAgFnPc2d1	kostní
buněk	buňka	k1gFnPc2	buňka
obnovujících	obnovující	k2eAgMnPc2d1	obnovující
kost	kost	k1gFnSc4	kost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
pomalejší	pomalý	k2eAgFnSc3d2	pomalejší
obnově	obnova	k1gFnSc3	obnova
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
k	k	k7c3	k
nedostatečnému	dostatečný	k2eNgNnSc3d1	nedostatečné
zabudování	zabudování	k1gNnSc3	zabudování
vápníku	vápník	k1gInSc2	vápník
do	do	k7c2	do
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
pak	pak	k6eAd1	pak
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
pevné	pevný	k2eAgFnPc1d1	pevná
a	a	k8xC	a
odolné	odolný	k2eAgFnPc1d1	odolná
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
lámou	lámat	k5eAaImIp3nP	lámat
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
osteoporotické	osteoporotický	k2eAgFnSc2d1	osteoporotická
zlomeniny	zlomenina	k1gFnSc2	zlomenina
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
pro	pro	k7c4	pro
normální	normální	k2eAgNnSc4d1	normální
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
stravujících	stravující	k2eAgFnPc2d1	stravující
zdravých	zdravá	k1gFnPc2	zdravá
dospělých	dospělý	k2eAgFnPc2d1	dospělá
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
z	z	k7c2	z
několika	několik	k4yIc2	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
listové	listový	k2eAgFnSc6d1	listová
zelenině	zelenina	k1gFnSc6	zelenina
2	[number]	k4	2
<g/>
.	.	kIx.	.
cyklus	cyklus	k1gInSc1	cyklus
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
jej	on	k3xPp3gInSc2	on
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
3	[number]	k4	3
<g/>
.	.	kIx.	.
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
normálně	normálně	k6eAd1	normálně
obývají	obývat	k5eAaImIp3nP	obývat
tlusté	tlustý	k2eAgNnSc4d1	tlusté
střevo	střevo	k1gNnSc4	střevo
<g/>
,	,	kIx,	,
syntetizují	syntetizovat	k5eAaImIp3nP	syntetizovat
menachinon	menachinon	k1gInSc4	menachinon
-	-	kIx~	-
vitamín	vitamín	k1gInSc4	vitamín
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dospělým	dospělý	k2eAgMnSc7d1	dospělý
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
nedostatkem	nedostatek	k1gInSc7	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
patří	patřit	k5eAaImIp3nS	patřit
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
užívají	užívat	k5eAaImIp3nP	užívat
antagonisty	antagonista	k1gMnPc7	antagonista
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
(	(	kIx(	(
<g/>
antikoagulační	antikoagulační	k2eAgInPc1d1	antikoagulační
léky	lék	k1gInPc1	lék
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
s	s	k7c7	s
významným	významný	k2eAgNnSc7d1	významné
poškozením	poškození	k1gNnSc7	poškození
nebo	nebo	k8xC	nebo
onemocněním	onemocnění	k1gNnPc3	onemocnění
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
riziku	riziko	k1gNnSc3	riziko
deficience	deficience	k1gFnSc2	deficience
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
jedinci	jedinec	k1gMnSc3	jedinec
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
absorpce	absorpce	k1gFnSc2	absorpce
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
pozornost	pozornost	k1gFnSc1	pozornost
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
věnuje	věnovat	k5eAaPmIp3nS	věnovat
stanovení	stanovení	k1gNnSc4	stanovení
požadavku	požadavek	k1gInSc2	požadavek
na	na	k7c4	na
příjem	příjem	k1gInSc4	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ostatní	ostatní	k2eAgFnSc2d1	ostatní
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadavky	požadavek	k1gInPc1	požadavek
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
zdravého	zdravý	k2eAgInSc2d1	zdravý
metabolismu	metabolismus	k1gInSc2	metabolismus
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
cévní	cévní	k2eAgFnSc2d1	cévní
stěny	stěna	k1gFnSc2	stěna
budou	být	k5eAaImBp3nP	být
patrně	patrně	k6eAd1	patrně
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
koagulace	koagulace	k1gFnSc2	koagulace
a	a	k8xC	a
že	že	k9	že
současná	současný	k2eAgNnPc4d1	současné
doporučení	doporučení	k1gNnPc4	doporučení
nemusejí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Expertní	expertní	k2eAgFnPc1d1	expertní
skupiny	skupina	k1gFnPc1	skupina
dokonce	dokonce	k9	dokonce
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
90	[number]	k4	90
<g/>
%	%	kIx~	%
dospělé	dospělý	k2eAgFnSc2d1	dospělá
západní	západní	k2eAgFnSc2d1	západní
populace	populace	k1gFnSc2	populace
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
nedostatek	nedostatek	k1gInSc4	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
normálního	normální	k2eAgInSc2d1	normální
metabolismu	metabolismus	k1gInSc2	metabolismus
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Novorozenci	novorozenec	k1gMnPc1	novorozenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
výhradně	výhradně	k6eAd1	výhradně
kojeni	kojen	k2eAgMnPc1d1	kojen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
riziku	riziko	k1gNnSc3	riziko
nedostatku	nedostatek	k1gInSc2	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
nízký	nízký	k2eAgInSc1d1	nízký
obsah	obsah	k1gInSc1	obsah
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Novorozenci	novorozenec	k1gMnPc1	novorozenec
mají	mít	k5eAaImIp3nP	mít
obecně	obecně	k6eAd1	obecně
nízkou	nízký	k2eAgFnSc4d1	nízká
hladinu	hladina	k1gFnSc4	hladina
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
se	se	k3xPyFc4	se
špatně	špatně	k6eAd1	špatně
přenáší	přenášet	k5eAaImIp3nS	přenášet
přes	přes	k7c4	přes
placentární	placentární	k2eAgFnSc4d1	placentární
bariéru	bariéra	k1gFnSc4	bariéra
2	[number]	k4	2
<g/>
.	.	kIx.	.
střeva	střevo	k1gNnPc1	střevo
novorozenců	novorozenec	k1gMnPc2	novorozenec
nejsou	být	k5eNaImIp3nP	být
dosud	dosud	k6eAd1	dosud
kolonizována	kolonizován	k2eAgFnSc1d1	kolonizována
bakteriemi	bakterie	k1gFnPc7	bakterie
syntetizujícími	syntetizující	k2eAgFnPc7d1	syntetizující
menachinon	menachinon	k1gNnSc4	menachinon
3	[number]	k4	3
<g/>
.	.	kIx.	.
cyklus	cyklus	k1gInSc1	cyklus
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgFnPc1d1	funkční
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
těch	ten	k3xDgFnPc2	ten
předčasně	předčasně	k6eAd1	předčasně
narozených	narozený	k2eAgFnPc2d1	narozená
<g/>
.	.	kIx.	.
</s>
<s>
Kojenci	kojenec	k1gMnPc1	kojenec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
matky	matka	k1gFnPc1	matka
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
antikolvuzivní	antikolvuzivní	k2eAgFnSc6d1	antikolvuzivní
terapii	terapie	k1gFnSc6	terapie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
nedostatkem	nedostatek	k1gInSc7	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Jeho	jeho	k3xOp3gInSc4	jeho
nedostatek	nedostatek	k1gInSc4	nedostatek
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
ke	k	k7c3	k
krvácení	krvácení	k1gNnSc3	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
takové	takový	k3xDgNnSc4	takový
krvácení	krvácení	k1gNnSc4	krvácení
je	být	k5eAaImIp3nS	být
život	život	k1gInSc1	život
ohrožující	ohrožující	k2eAgMnSc1d1	ohrožující
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dá	dát	k5eAaPmIp3nS	dát
předejít	předejít	k5eAaPmF	předejít
<g/>
,	,	kIx,	,
dostávají	dostávat	k5eAaImIp3nP	dostávat
novorozenci	novorozenec	k1gMnPc1	novorozenec
vitamín	vitamín	k1gInSc4	vitamín
K	k	k7c3	k
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2001	[number]	k4	2001
americký	americký	k2eAgInSc1d1	americký
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
potraviny	potravina	k1gFnPc4	potravina
a	a	k8xC	a
výživu	výživa	k1gFnSc4	výživa
stanovil	stanovit	k5eAaPmAgInS	stanovit
adekvátní	adekvátní	k2eAgInSc1d1	adekvátní
příjem	příjem	k1gInSc1	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
spotřeby	spotřeba	k1gFnSc2	spotřeba
zdravých	zdravý	k2eAgMnPc2d1	zdravý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
AI	AI	kA	AI
pro	pro	k7c4	pro
kojence	kojenec	k1gMnSc4	kojenec
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odhadovaného	odhadovaný	k2eAgInSc2d1	odhadovaný
příjmu	příjem	k1gInSc2	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
z	z	k7c2	z
mateřského	mateřský	k2eAgNnSc2d1	mateřské
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
tzv.	tzv.	kA	tzv.
vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
dependentních	dependentní	k2eAgFnPc2d1	dependentní
bílkovin	bílkovina	k1gFnPc2	bílkovina
v	v	k7c6	v
kostech	kost	k1gFnPc6	kost
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
úlohy	úloha	k1gFnSc2	úloha
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
při	při	k7c6	při
zachovávání	zachovávání	k1gNnSc6	zachovávání
zdravých	zdravý	k2eAgFnPc2d1	zdravá
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Epidemiologické	epidemiologický	k2eAgFnPc1d1	epidemiologická
studie	studie	k1gFnPc1	studie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
vitamínem	vitamín	k1gInSc7	vitamín
K	K	kA	K
a	a	k8xC	a
řídnutím	řídnutí	k1gNnSc7	řídnutí
kostí	kost	k1gFnPc2	kost
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
osteoporózou	osteoporóza	k1gFnSc7	osteoporóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Nurses	Nurses	k1gInSc1	Nurses
<g/>
'	'	kIx"	'
Health	Health	k1gInSc1	Health
Study	stud	k1gInPc4	stud
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
72	[number]	k4	72
000	[number]	k4	000
žen	žena	k1gFnPc2	žena
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
příjem	příjem	k1gInSc1	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
nejnižším	nízký	k2eAgInSc6d3	nejnižší
kvintilu	kvintil	k1gInSc6	kvintil
(	(	kIx(	(
<g/>
pětině	pětina	k1gFnSc6	pětina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
o	o	k7c4	o
30	[number]	k4	30
<g/>
%	%	kIx~	%
vyšší	vysoký	k2eAgNnSc1d2	vyšší
riziko	riziko	k1gNnSc1	riziko
zlomeniny	zlomenina	k1gFnSc2	zlomenina
krčku	krček	k1gInSc2	krček
kosti	kost	k1gFnSc2	kost
stehenní	stehenní	k2eAgFnSc2d1	stehenní
než	než	k8xS	než
ženy	žena	k1gFnSc2	žena
s	s	k7c7	s
příjmem	příjem	k1gInSc7	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
vyšších	vysoký	k2eAgInPc6d2	vyšší
kvintilech	kvintil	k1gInPc6	kvintil
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
800	[number]	k4	800
starších	starý	k2eAgMnPc2d2	starší
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
sedmileté	sedmiletý	k2eAgFnSc6d1	sedmiletá
Framinghamské	Framinghamský	k2eAgFnSc6d1	Framinghamská
studii	studie	k1gFnSc6	studie
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
s	s	k7c7	s
příjmem	příjem	k1gInSc7	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
kvartilu	kvartil	k1gInSc6	kvartil
(	(	kIx(	(
<g/>
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
o	o	k7c6	o
65	[number]	k4	65
<g/>
%	%	kIx~	%
nižší	nízký	k2eAgNnSc1d2	nižší
riziko	riziko	k1gNnSc1	riziko
zlomeniny	zlomenina	k1gFnSc2	zlomenina
krčku	krček	k1gInSc2	krček
kosti	kost	k1gFnSc2	kost
stehenní	stehenní	k2eAgFnSc2d1	stehenní
<g/>
,	,	kIx,	,
než	než	k8xS	než
Ti	ten	k3xDgMnPc1	ten
s	s	k7c7	s
příjmem	příjem	k1gInSc7	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
v	v	k7c6	v
nejnižším	nízký	k2eAgInSc6d3	nejnižší
kvartilu	kvartil	k1gInSc6	kvartil
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
ug	ug	k?	ug
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc4	den
oproti	oproti	k7c3	oproti
50	[number]	k4	50
ug	ug	k?	ug
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
studie	studie	k1gFnPc1	studie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
zvýšení	zvýšení	k1gNnSc4	zvýšení
hustoty	hustota	k1gFnSc2	hustota
kostního	kostní	k2eAgInSc2d1	kostní
minerálu	minerál	k1gInSc2	minerál
(	(	kIx(	(
<g/>
BMD	BMD	kA	BMD
<g/>
)	)	kIx)	)
díky	díky	k7c3	díky
podávání	podávání	k1gNnSc3	podávání
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vitamínem	vitamín	k1gInSc7	vitamín
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
snížený	snížený	k2eAgInSc4d1	snížený
výskyt	výskyt	k1gInSc4	výskyt
zlomenin	zlomenina	k1gFnPc2	zlomenina
obratlů	obratel	k1gInPc2	obratel
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
léčbou	léčba	k1gFnSc7	léčba
bisfosfonáty	bisfosfonát	k1gInPc1	bisfosfonát
<g/>
.	.	kIx.	.
</s>
<s>
Osteokalcin	Osteokalcin	k1gInSc1	Osteokalcin
<g/>
,	,	kIx,	,
bílkovina	bílkovina	k1gFnSc1	bílkovina
cirkulující	cirkulující	k2eAgFnSc1d1	cirkulující
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
citlivým	citlivý	k2eAgInSc7d1	citlivý
ukazatelem	ukazatel	k1gInSc7	ukazatel
stavu	stav	k1gInSc2	stav
kostní	kostní	k2eAgFnSc2d1	kostní
novotvorby	novotvorba	k1gFnSc2	novotvorba
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
pro	pro	k7c4	pro
gama-karboxylaci	gamaarboxylace	k1gFnSc4	gama-karboxylace
osteokalcinu	osteokalcin	k1gInSc2	osteokalcin
<g/>
.	.	kIx.	.
</s>
<s>
Podkarboxylovaný	Podkarboxylovaný	k2eAgInSc1d1	Podkarboxylovaný
osteokalcin	osteokalcin	k1gInSc1	osteokalcin
(	(	kIx(	(
<g/>
ucOC	ucOC	k?	ucOC
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgFnSc1d2	nižší
schopnost	schopnost	k1gFnSc1	schopnost
vázat	vázat	k5eAaImF	vázat
kostní	kostní	k2eAgInSc4d1	kostní
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
gama-karboxylace	gamaarboxylace	k1gFnSc2	gama-karboxylace
osteokalcinu	osteokalcin	k1gInSc2	osteokalcin
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k9	jako
citlivý	citlivý	k2eAgInSc4d1	citlivý
ukazatel	ukazatel	k1gInSc4	ukazatel
hladiny	hladina	k1gFnSc2	hladina
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hladiny	hladina	k1gFnPc1	hladina
ucOC	ucOC	k?	ucOC
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgFnPc1d2	vyšší
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
po	po	k7c6	po
menopauze	menopauza	k1gFnSc6	menopauza
než	než	k8xS	než
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
starších	starší	k1gMnPc2	starší
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
se	s	k7c7	s
195	[number]	k4	195
staršími	starý	k2eAgFnPc7d2	starší
ženami	žena	k1gFnPc7	žena
bylo	být	k5eAaImAgNnS	být
relativní	relativní	k2eAgNnSc1d1	relativní
riziko	riziko	k1gNnSc1	riziko
fraktury	fraktura	k1gFnSc2	fraktura
krčku	krček	k1gInSc2	krček
kosti	kost	k1gFnSc2	kost
stehenní	stehenní	k2eAgFnSc2d1	stehenní
6	[number]	k4	6
<g/>
x	x	k?	x
vyšší	vysoký	k2eAgMnSc1d2	vyšší
u	u	k7c2	u
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
zvýšené	zvýšený	k2eAgFnPc4d1	zvýšená
hladiny	hladina	k1gFnPc4	hladina
ucOC	ucOC	k?	ucOC
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
studie	studie	k1gFnSc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většího	veliký	k2eAgInSc2d2	veliký
vzorku	vzorek	k1gInSc2	vzorek
7	[number]	k4	7
500	[number]	k4	500
samostatně	samostatně	k6eAd1	samostatně
žijících	žijící	k2eAgFnPc2d1	žijící
starších	starý	k2eAgFnPc2d2	starší
žen	žena	k1gFnPc2	žena
hladina	hladina	k1gFnSc1	hladina
ucOC	ucOC	k?	ucOC
také	také	k9	také
predikovala	predikovat	k5eAaBmAgFnS	predikovat
riziko	riziko	k1gNnSc4	riziko
zlomeniny	zlomenina	k1gFnSc2	zlomenina
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
některých	některý	k3yIgFnPc6	některý
orálních	orální	k2eAgFnPc6d1	orální
antikoagulanciích	antikoagulancie	k1gFnPc6	antikoagulancie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
warfarinu	warfarin	k1gInSc2	warfarin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
antagonisté	antagonista	k1gMnPc1	antagonista
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Studie	studie	k1gFnSc1	studie
zkoumající	zkoumající	k2eAgFnSc1d1	zkoumající
chronické	chronický	k2eAgNnSc4d1	chronické
užívání	užívání	k1gNnSc4	užívání
warfarinu	warfarin	k1gInSc2	warfarin
a	a	k8xC	a
riziko	riziko	k1gNnSc4	riziko
fraktur	fraktura	k1gFnPc2	fraktura
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
žen	žena	k1gFnPc2	žena
zjistila	zjistit	k5eAaPmAgFnS	zjistit
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
zlomenin	zlomenina	k1gFnPc2	zlomenina
obratlů	obratel	k1gInPc2	obratel
a	a	k8xC	a
žeber	žebro	k1gNnPc2	žebro
u	u	k7c2	u
uživatelek	uživatelka	k1gFnPc2	uživatelka
warfarinu	warfarin	k1gInSc2	warfarin
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
pacientkami	pacientka	k1gFnPc7	pacientka
bez	bez	k7c2	bez
warfarinu	warfarin	k1gInSc2	warfarin
<g/>
.	.	kIx.	.
</s>
<s>
Fylochinon	fylochinon	k1gInSc1	fylochinon
(	(	kIx(	(
<g/>
vitamín	vitamín	k1gInSc1	vitamín
K	k	k7c3	k
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
listové	listový	k2eAgFnSc6d1	listová
zelenině	zelenina	k1gFnSc6	zelenina
a	a	k8xC	a
některých	některý	k3yIgInPc6	některý
rostlinných	rostlinný	k2eAgInPc6d1	rostlinný
olejích	olej	k1gInPc6	olej
(	(	kIx(	(
<g/>
sojový	sojový	k2eAgInSc1d1	sojový
<g/>
,	,	kIx,	,
olivový	olivový	k2eAgInSc1d1	olivový
<g/>
,	,	kIx,	,
v	v	k7c6	v
bavlníkovém	bavlníkový	k2eAgNnSc6d1	bavlníkové
semenu	semeno	k1gNnSc6	semeno
<g/>
,	,	kIx,	,
řepce	řepka	k1gFnSc6	řepka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hydrogenace	hydrogenace	k1gFnSc1	hydrogenace
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
olejů	olej	k1gInPc2	olej
může	moct	k5eAaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
biologickou	biologický	k2eAgFnSc4d1	biologická
účinnost	účinnost	k1gFnSc4	účinnost
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Některé	některý	k3yIgInPc4	některý
zdroje	zdroj	k1gInPc4	zdroj
vitamínu	vitamín	k1gInSc2	vitamín
K1	K1	k1gFnSc2	K1
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
vitamínu	vitamín	k1gInSc2	vitamín
K1	K1	k1gMnPc2	K1
v	v	k7c6	v
mikrogramech	mikrogram	k1gInPc6	mikrogram
(	(	kIx(	(
<g/>
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
normálně	normálně	k6eAd1	normálně
kolonizují	kolonizovat	k5eAaBmIp3nP	kolonizovat
tlusté	tlustý	k2eAgNnSc4d1	tlusté
střevo	střevo	k1gNnSc4	střevo
<g/>
,	,	kIx,	,
syntetizují	syntetizovat	k5eAaImIp3nP	syntetizovat
menachinon	menachinon	k1gInSc4	menachinon
(	(	kIx(	(
<g/>
vitamín	vitamín	k1gInSc4	vitamín
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
donedávna	donedávna	k6eAd1	donedávna
bylo	být	k5eAaImAgNnS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
50	[number]	k4	50
<g/>
%	%	kIx~	%
potřebného	potřebný	k2eAgInSc2d1	potřebný
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
bakteriální	bakteriální	k2eAgFnSc7d1	bakteriální
syntézou	syntéza	k1gFnSc7	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
výzkum	výzkum	k1gInSc1	výzkum
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podíl	podíl	k1gInSc1	podíl
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
syntézy	syntéza	k1gFnSc2	syntéza
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
doplňcích	doplněk	k1gInPc6	doplněk
stravy	strava	k1gFnSc2	strava
možno	možno	k6eAd1	možno
získat	získat	k5eAaPmF	získat
vitamíny	vitamín	k1gInPc4	vitamín
K1	K1	k1gMnPc3	K1
a	a	k8xC	a
K2	K2	k1gMnPc3	K2
i	i	k9	i
bez	bez	k7c2	bez
lékařského	lékařský	k2eAgInSc2d1	lékařský
předpisu	předpis	k1gInSc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c4	mezi
vitamíny	vitamín	k1gInPc4	vitamín
K1	K1	k1gMnPc2	K1
a	a	k8xC	a
K2	K2	k1gMnPc2	K2
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vstřebávání	vstřebávání	k1gNnSc6	vstřebávání
a	a	k8xC	a
ve	v	k7c6	v
využití	využití	k1gNnSc6	využití
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Vitamínu	vitamín	k1gInSc3	vitamín
K1	K1	k1gMnPc2	K1
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
5	[number]	k4	5
–	–	k?	–
15	[number]	k4	15
<g/>
%	%	kIx~	%
podaného	podaný	k2eAgNnSc2d1	podané
množství	množství	k1gNnSc2	množství
<g/>
,	,	kIx,	,
u	u	k7c2	u
vitamínu	vitamín	k1gInSc2	vitamín
K2	K2	k1gFnSc2	K2
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
95	[number]	k4	95
–	–	k?	–
100	[number]	k4	100
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
K1	K1	k1gFnSc2	K1
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
srážení	srážení	k1gNnSc2	srážení
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
<g/>
,	,	kIx,	,
K2	K2	k1gFnSc1	K2
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgMnS	využívat
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
metabolismus	metabolismus	k1gInSc4	metabolismus
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
uvedenému	uvedený	k2eAgInSc3d1	uvedený
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
podávat	podávat	k5eAaImF	podávat
vitamín	vitamín	k1gInSc4	vitamín
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
alergická	alergický	k2eAgFnSc1d1	alergická
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
známá	známý	k2eAgFnSc1d1	známá
toxicita	toxicita	k1gFnSc1	toxicita
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
dávkami	dávka	k1gFnPc7	dávka
při	při	k7c6	při
podávání	podávání	k1gNnSc6	podávání
vitamínu	vitamín	k1gInSc2	vitamín
K1	K1	k1gFnPc3	K1
nebo	nebo	k8xC	nebo
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
syntetický	syntetický	k2eAgInSc4d1	syntetický
menadion	menadion	k1gInSc4	menadion
(	(	kIx(	(
<g/>
vitamín	vitamín	k1gInSc4	vitamín
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
deriváty	derivát	k1gInPc1	derivát
<g/>
.	.	kIx.	.
</s>
<s>
Menadion	Menadion	k1gInSc1	Menadion
může	moct	k5eAaImIp3nS	moct
narušovat	narušovat	k5eAaImF	narušovat
funkci	funkce	k1gFnSc4	funkce
glutathionu	glutathion	k1gInSc2	glutathion
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
přirozených	přirozený	k2eAgMnPc2d1	přirozený
antioxidantů	antioxidant	k1gInPc2	antioxidant
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organizmu	organizmus	k1gInSc6	organizmus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
oxidační	oxidační	k2eAgNnSc1d1	oxidační
poškození	poškození	k1gNnSc1	poškození
buněčných	buněčný	k2eAgFnPc2d1	buněčná
membrán	membrána	k1gFnPc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Menadion	Menadion	k1gInSc1	Menadion
podávaný	podávaný	k2eAgInSc1d1	podávaný
injekčně	injekčně	k6eAd1	injekčně
indukoval	indukovat	k5eAaBmAgInS	indukovat
jaterní	jaterní	k2eAgFnSc4d1	jaterní
toxicitu	toxicita	k1gFnSc4	toxicita
<g/>
,	,	kIx,	,
žloutenku	žloutenka	k1gFnSc4	žloutenka
a	a	k8xC	a
hemolytickou	hemolytický	k2eAgFnSc4d1	hemolytická
anémii	anémie	k1gFnSc4	anémie
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
menadion	menadion	k1gInSc1	menadion
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
deficience	deficience	k1gFnSc2	deficience
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Velké	velká	k1gFnSc2	velká
dávky	dávka	k1gFnSc2	dávka
vitamínu	vitamín	k1gInSc2	vitamín
A	A	kA	A
a	a	k8xC	a
vitamínu	vitamín	k1gInSc2	vitamín
E	E	kA	E
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
antagonisté	antagonista	k1gMnPc1	antagonista
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Přebytek	přebytek	k1gInSc1	přebytek
vitamínu	vitamín	k1gInSc2	vitamín
A	a	k9	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vstřebávání	vstřebávání	k1gNnSc1	vstřebávání
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Vitamín	vitamín	k1gInSc1	vitamín
E	E	kA	E
může	moct	k5eAaImIp3nS	moct
inhibovat	inhibovat	k5eAaBmF	inhibovat
vitamín	vitamín	k1gInSc4	vitamín
K	K	kA	K
dependentní	dependentní	k2eAgFnSc2d1	dependentní
karboxylázy	karboxyláza	k1gFnSc2	karboxyláza
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
s	s	k7c7	s
normálním	normální	k2eAgInSc7d1	normální
koagulačním	koagulační	k2eAgInSc7d1	koagulační
stavem	stav	k1gInSc7	stav
shledala	shledat	k5eAaPmAgFnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
1000	[number]	k4	1000
IU	IU	kA	IU
vitamínu	vitamín	k1gInSc2	vitamín
E	E	kA	E
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
12	[number]	k4	12
týdnů	týden	k1gInPc2	týden
klesla	klesnout	k5eAaPmAgFnS	klesnout
gama-karboxylace	gamaarboxylace	k1gFnSc1	gama-karboxylace
protrombinu	protrombina	k1gFnSc4	protrombina
<g/>
,	,	kIx,	,
vitamín	vitamín	k1gInSc4	vitamín
K	K	kA	K
dependentního	dependentní	k2eAgInSc2d1	dependentní
proteinu	protein	k1gInSc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Hlášeno	hlášen	k2eAgNnSc1d1	hlášeno
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
krvácení	krvácení	k1gNnSc4	krvácení
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
užívajících	užívající	k2eAgFnPc2d1	užívající
5	[number]	k4	5
mg	mg	kA	mg
warfarinu	warfarin	k1gInSc2	warfarin
a	a	k8xC	a
1200	[number]	k4	1200
IU	IU	kA	IU
vitamínu	vitamín	k1gInSc2	vitamín
E	E	kA	E
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Antikoagulační	Antikoagulační	k2eAgInSc1d1	Antikoagulační
účinek	účinek	k1gInSc1	účinek
antagonistů	antagonista	k1gMnPc2	antagonista
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
(	(	kIx(	(
<g/>
např.	např.	kA	např.
warfarinu	warfarin	k1gInSc2	warfarin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
inhibován	inhibovat	k5eAaBmNgInS	inhibovat
vysokým	vysoký	k2eAgInSc7d1	vysoký
příjmem	příjem	k1gInSc7	příjem
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Jiné	jiný	k2eAgInPc1d1	jiný
léky	lék	k1gInPc1	lék
mohou	moct	k5eAaImIp3nP	moct
interferovat	interferovat	k5eAaImF	interferovat
s	s	k7c7	s
endogenní	endogenní	k2eAgFnSc7d1	endogenní
syntézou	syntéza	k1gFnSc7	syntéza
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
nebo	nebo	k8xC	nebo
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
regeneraci	regenerace	k1gFnSc4	regenerace
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Delší	dlouhý	k2eAgNnSc1d2	delší
používání	používání	k1gNnSc1	používání
širokospektrých	širokospektrý	k2eAgNnPc2d1	širokospektré
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
může	moct	k5eAaImIp3nS	moct
snižovat	snižovat	k5eAaImF	snižovat
syntézu	syntéza	k1gFnSc4	syntéza
vitamínu	vitamín	k1gInSc2	vitamín
K	K	kA	K
střevními	střevní	k2eAgFnPc7d1	střevní
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Cefalosporiny	Cefalosporin	k1gInPc1	Cefalosporin
a	a	k8xC	a
salicyláty	salicylát	k1gInPc1	salicylát
mohou	moct	k5eAaImIp3nP	moct
snižovat	snižovat	k5eAaImF	snižovat
regeneraci	regenerace	k1gFnSc4	regenerace
vitamínu	vitamín	k1gInSc2	vitamín
K	k	k7c3	k
inhibicí	inhibice	k1gFnSc7	inhibice
enzymu	enzym	k1gInSc2	enzym
vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
epoxid	epoxid	k1gInSc4	epoxid
reduktázy	reduktáza	k1gFnSc2	reduktáza
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
cholestyramin	cholestyramin	k1gInSc1	cholestyramin
<g/>
,	,	kIx,	,
cholestipol	cholestipol	k1gInSc1	cholestipol
<g/>
,	,	kIx,	,
orlistat	orlistat	k1gInSc1	orlistat
<g/>
,	,	kIx,	,
minerální	minerální	k2eAgInPc1d1	minerální
oleje	olej	k1gInPc1	olej
a	a	k8xC	a
tuky	tuk	k1gInPc1	tuk
mohou	moct	k5eAaImIp3nP	moct
snížit	snížit	k5eAaPmF	snížit
vstřebávání	vstřebávání	k1gNnSc2	vstřebávání
vitamínu	vitamín	k1gInSc2	vitamín
K.	K.	kA	K.
Vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
</s>
