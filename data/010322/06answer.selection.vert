<s>
Při	při	k7c6	při
přímém	přímý	k2eAgInSc6d1	přímý
pohledu	pohled	k1gInSc6	pohled
působí	působit	k5eAaImIp3nS	působit
Slunce	slunce	k1gNnSc1	slunce
na	na	k7c4	na
sítnici	sítnice	k1gFnSc4	sítnice
výkonem	výkon	k1gInSc7	výkon
asi	asi	k9	asi
4	[number]	k4	4
miliwatty	miliwatt	k1gInPc4	miliwatt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
zahřívání	zahřívání	k1gNnSc3	zahřívání
sítnice	sítnice	k1gFnSc2	sítnice
a	a	k8xC	a
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
možnému	možný	k2eAgNnSc3d1	možné
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
