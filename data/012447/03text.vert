<p>
<s>
Salesiánské	salesiánský	k2eAgNnSc1d1	Salesiánské
středisko	středisko	k1gNnSc1	středisko
mládeže	mládež	k1gFnSc2	mládež
–	–	k?
dům	dům	k1gInSc1	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC
mládeže	mládež	k1gFnSc2	mládež
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
SaSM	SaSM	k1gFnSc1	SaSM
–	–	k?
DDM	DDM	kA
ČB	ČB	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
salesiánské	salesiánský	k2eAgNnSc1d1	Salesiánské
středisko	středisko	k1gNnSc1	středisko
mládeže	mládež	k1gFnSc2	mládež
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4
ve	v	k7c6
farnosti	farnost	k1gFnSc6	farnost
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
v	v	k7c6
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
místa	místo	k1gNnSc2	místo
pro	pro	k7c4
aktivní	aktivní	k2eAgNnSc4d1	aktivní
trávení	trávení	k1gNnSc4	trávení
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6
duchu	duch	k1gMnSc6	duch
salesiánské	salesiánský	k2eAgFnSc2d1	Salesiánská
pedagogiky	pedagogika	k1gFnPc1	pedagogika
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Historie	historie	k1gFnSc1	historie
==	==	k?
</s>
</p>
<p>
<s>
Salesiáni	salesián	k1gMnPc1	salesián
působili	působit	k5eAaImAgMnP
v	v	k7c6
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
neveřejně	veřejně	k6eNd1
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2	rok
1980	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
správa	správa	k1gFnSc1	správa
zdejší	zdejší	k2eAgFnSc2d1	zdejší
farnosti	farnost	k1gFnSc2	farnost
byla	být	k5eAaImAgFnS
salesiánům	salesián	k1gMnPc3	salesián
oficiálně	oficiálně	k6eAd1
svěřena	svěřen	k2eAgFnSc1d1	svěřena
až	až	k9
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2	červenec
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2	září
1995	#num#	k4
bylo	být	k5eAaImAgNnS
Salesiánskou	salesiánský	k2eAgFnSc7d1	Salesiánská
provincií	provincie	k1gFnSc7	provincie
Praha	Praha	k1gFnSc1	Praha
založeno	založit	k5eAaPmNgNnS
Salesiánské	salesiánský	k2eAgNnSc1d1	Salesiánské
středisko	středisko	k1gNnSc1	středisko
mládeže	mládež	k1gFnSc2	mládež
se	s	k7c7
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6
budově	budova	k1gFnSc6	budova
přilehlé	přilehlý	k2eAgFnSc6d1	přilehlá
kostelu	kostel	k1gInSc3	kostel
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
letech	léto	k1gNnPc6	léto
1997	#num#	k4
až	až	k6eAd1
1998	#num#	k4
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
<g/>
.1	.1	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2	září
1999	#num#	k4
středisko	středisko	k1gNnSc4	středisko
zařazeno	zařazen	k2eAgNnSc4d1	zařazeno
na	na	k7c4
seznam	seznam	k1gInSc4	seznam
škol	škola	k1gFnPc2	škola
a	a	k8xC
školských	školská	k1gFnPc2	školská
zařízení	zařízení	k1gNnSc4	zařízení
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2	červen
2006	#num#	k4
bylo	být	k5eAaImAgNnS
zapsáno	zapsat	k5eAaPmNgNnS
de	de	k?
rejstříku	rejstřík	k1gInSc2	rejstřík
školských	školská	k1gFnPc2	školská
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.
<g/>
V	v	k7c6
roce	rok	k1gInSc6	rok
2008	#num#	k4
vybudovalo	vybudovat	k5eAaPmAgNnS
středisko	středisko	k1gNnSc1	středisko
<g />
.	.	kIx.
</s>
<s hack="1">
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7
městem	město	k1gNnSc7	město
kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
centrum	centrum	k1gNnSc4	centrum
Maják	maják	k1gInSc1	maják
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS
zaměřena	zaměřit	k5eAaPmNgFnS
především	především	k9
na	na	k7c4
děti	dítě	k1gFnPc4	dítě
a	a	k8xC
mládež	mládež	k1gFnSc4	mládež
romské	romský	k2eAgFnSc2d1	romská
komunity	komunita	k1gFnSc2	komunita
na	na	k7c6
sídlišti	sídliště	k1gNnSc6	sídliště
Máj	Mája	k1gFnPc2	Mája
<g/>
.	.	kIx.
<g/>
Ve	v	k7c6
dnech	den	k1gInPc6	den
15	#num#	k4
<g/>
.	.	kIx.
až	až	k9
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2	říjen
2010	#num#	k4
připravilo	připravit	k5eAaPmAgNnS
Salesiánské	salesiánský	k2eAgNnSc1d1	Salesiánské
středisko	středisko	k1gNnSc1	středisko
spolu	spolu	k6eAd1
s	s	k7c7
Farností	farnost	k1gFnSc7	farnost
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
třídenní	třídenní	k2eAgInSc1d1	třídenní
program	program	k1gInSc1	program
oslav	oslava	k1gFnPc2	oslava
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oslavy	oslava	k1gFnSc2	oslava
byly	být	k5eAaImAgInP
zakončeny	zakončit	k5eAaPmNgInP
bohoslužbou	bohoslužba	k1gFnSc7	bohoslužba
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
sloužil	sloužit	k5eAaImAgMnS
biskup	biskup	k1gMnSc1	biskup
pražské	pražský	k2eAgFnSc2d1	Pražská
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
Karel	Karel	k1gMnSc1	Karel
Herbst	Herbst	k1gMnSc1	Herbst
<g/>
.	.	kIx.
<g/>
Začátkem	začátkem	k7c2
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
se	se	k3xPyFc4
hlavní	hlavní	k2eAgFnSc2d1	hlavní
činnosti	činnost	k1gFnSc2	činnost
Salesiánského	salesiánský	k2eAgNnSc2d1	Salesiánské
střediska	středisko	k1gNnSc2	středisko
přesunuly	přesunout	k5eAaPmAgFnP
do	do	k7c2
nově	nově	k6eAd1
postaveného	postavený	k2eAgNnSc2d1	postavené
komunitního	komunitní	k2eAgNnSc2d1	komunitní
centra	centrum	k1gNnSc2	centrum
na	na	k7c6
sídlišti	sídliště	k1gNnSc6	sídliště
Máj	Mája	k1gFnPc2	Mája
<g/>
,	,	kIx,
zájmové	zájmový	k2eAgInPc1d1	zájmový
kroužky	kroužek	k1gInPc1	kroužek
zůstaly	zůstat	k5eAaPmAgInP
v	v	k7c6
původní	původní	k2eAgFnSc6d1	původní
budově	budova	k1gFnSc6	budova
u	u	k7c2
Kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Činnost	činnost	k1gFnSc4	činnost
==	==	k?
</s>
</p>
<p>
<s>
Středisko	středisko	k1gNnSc1	středisko
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
práci	práce	k1gFnSc4	práce
s	s	k7c7
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC
mládeží	mládež	k1gFnSc7	mládež
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,
i	i	k8xC
pomoci	pomoct	k5eAaPmF
sociálně	sociálně	k6eAd1
vyloučeným	vyloučený	k2eAgFnPc3d1	vyloučená
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC
mládeži	mládež	k1gFnSc3	mládež
a	a	k8xC
jejich	jejich	k3xOp3gFnPc3
rodinám	rodina	k1gFnPc3	rodina
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	své	k1gNnSc1	své
cíle	cíl	k1gInSc2	cíl
uskutečňuje	uskutečňovat	k5eAaImIp3nS
prostřednictvím	prostřednictvím	k7c2
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
programů	program	k1gInPc2	program
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
zájmové	zájmový	k2eAgInPc1d1	zájmový
kroužky	kroužek	k1gInPc1	kroužek
<g/>
,	,	kIx,
nízkoprahové	nízkoprahový	k2eAgInPc1d1	nízkoprahový
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,
terénní	terénní	k2eAgFnPc1d1	terénní
práce	práce	k1gFnPc1	práce
sociálních	sociální	k2eAgMnPc2d1	sociální
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,
dětské	dětský	k2eAgInPc1d1	dětský
tábory	tábor	k1gInPc1	tábor
(	(	kIx(
<g/>
tzv.	tzv.	kA
Chaloupky	chaloupka	k1gFnSc2	chaloupka
<g/>
)	)	kIx)
a	a	k8xC
pořádá	pořádat	k5eAaImIp3nS
i	i	k9
nepravidelné	pravidelný	k2eNgFnPc4d1	nepravidelná
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
plesy	ples	k1gInPc4	ples
<g/>
,	,	kIx,
Zábavné	zábavný	k2eAgFnSc2d1	zábavná
akademie	akademie	k1gFnSc2	akademie
Dona	Don	k1gMnSc4	Don
Bosca	Boscus	k1gMnSc4	Boscus
<g/>
,	,	kIx,
aj.	aj.	kA
</s>
</p>
<p>
<s>
===	===	k?
Audiovizuální	audiovizuální	k2eAgFnSc1d1	audiovizuální
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?
</s>
</p>
<p>
<s>
Ve	v	k7c6
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
nabídky	nabídka	k1gFnSc2	nabídka
zájmových	zájmový	k2eAgInPc2d1	zájmový
kroužků	kroužek	k1gInPc2	kroužek
poprvé	poprvé	k6eAd1
dostal	dostat	k5eAaPmAgInS
mediální	mediální	k2eAgInSc4d1	mediální
kroužek	kroužek	k1gInSc4	kroužek
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nepravidelně	pravidelně	k6eNd1
dodával	dodávat	k5eAaImAgMnS
reportáže	reportáž	k1gFnPc4	reportáž
pro	pro	k7c4
televizi	televize	k1gFnSc4	televize
Noe	Noe	k1gMnSc1	Noe
a	a	k8xC
jehož	jehož	k3xOyRp3gFnSc1
prvním	první	k4xOgMnSc6
debutem	debut	k1gInSc7	debut
byla	být	k5eAaImAgFnS
krátká	krátký	k2eAgFnSc1d1	krátká
groteska	groteska	k1gFnSc1	groteska
Stan	stan	k1gInSc1	stan
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
představená	představená	k1gFnSc1	představená
na	na	k7c6
konci	konec	k1gInSc6	konec
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
V	v	k7c6
roce	rok	k1gInSc6	rok
2010	#num#	k4
se	se	k3xPyFc4
během	během	k7c2
třídenních	třídenní	k2eAgFnPc2d1	třídenní
oslav	oslava	k1gFnPc2	oslava
20	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3	výročí
působení	působení	k1gNnSc3	působení
salesiánů	salesián	k1gMnPc2	salesián
v	v	k7c6
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
filmařský	filmařský	k2eAgInSc1d1	filmařský
kroužek	kroužek	k1gInSc1	kroužek
podílel	podílet	k5eAaImAgInS
na	na	k7c6
natáčení	natáčení	k1gNnSc6	natáčení
Galakoncertu	galakoncert	k1gInSc2	galakoncert
díků	dík	k1gInPc2	dík
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
přenos	přenos	k1gInSc1	přenos
na	na	k7c4
televizní	televizní	k2eAgFnPc4d1	televizní
obrazovky	obrazovka	k1gFnPc4	obrazovka
v	v	k7c6
interiéru	interiér	k1gInSc6	interiér
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záznam	záznam	k1gInSc1	záznam
tohoto	tento	k3xDgInSc2
koncertu	koncert	k1gInSc2	koncert
byl	být	k5eAaImAgInS
spolu	spolu	k6eAd1
s	s	k7c7
dokumentárním	dokumentární	k2eAgInSc7d1	dokumentární
filmem	film	k1gInSc7	film
Sousedé	soused	k1gMnPc1	soused
od	od	k7c2
věže	věž	k1gFnSc2	věž
v	v	k7c6
roce	rok	k1gInSc6	rok
2011	#num#	k4
vydán	vydán	k2eAgInSc4d1	vydán
na	na	k7c4
DVD	DVD	kA
<g/>
.	.	kIx.
<g/>
Groteska	groteska	k1gFnSc1	groteska
Stan	stan	k1gInSc1	stan
získala	získat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6	rok
2011	#num#	k4
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4	místo
v	v	k7c6
kategorii	kategorie	k1gFnSc6	kategorie
do	do	k7c2
9	#num#	k4
let	léto	k1gNnPc2	léto
na	na	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
festivalu	festival	k1gInSc6	festival
Juniorfilm	Juniorfilm	k1gInSc1	Juniorfilm
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2	rok
začal	začít	k5eAaPmAgInS
filmařský	filmařský	k2eAgInSc1d1	filmařský
kroužek	kroužek	k1gInSc1	kroužek
natáčet	natáčet	k5eAaImF
preventivně	preventivně	k6eAd1
výchovný	výchovný	k2eAgInSc4d1	výchovný
film	film	k1gInSc4	film
pod	pod	k7c7
pracovním	pracovní	k2eAgInSc7d1	pracovní
názvem	název	k1gInSc7	název
Na	na	k7c6
kole	kolo	k1gNnSc6	kolo
bezpečně	bezpečně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
představen	představit	k5eAaPmNgInS
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2	květen
2012	#num#	k4
pod	pod	k7c7
již	již	k6eAd1
pozměněným	pozměněný	k2eAgInSc7d1	pozměněný
názvem	název	k1gInSc7	název
Bezpečně	bezpečně	k6eAd1
na	na	k7c6
kole	kolo	k1gNnSc6	kolo
a	a	k8xC
následně	následně	k6eAd1
byl	být	k5eAaImAgInS
distribuován	distribuovat	k5eAaBmNgInS
do	do	k7c2
několika	několik	k4yIc2
jihočeských	jihočeský	k2eAgFnPc2d1	Jihočeská
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Chaloupky	Chaloupka	k1gMnSc2	Chaloupka
===	===	k?
</s>
</p>
<p>
<s>
Chaloupky	chaloupka	k1gFnPc1	chaloupka
jsou	být	k5eAaImIp3nP
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
dětské	dětský	k2eAgInPc4d1	dětský
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
ale	ale	k9
nejsou	být	k5eNaImIp3nP
určeny	určit	k5eAaPmNgInP
pouze	pouze	k6eAd1
pro	pro	k7c4
děti	dítě	k1gFnPc4	dítě
z	z	k7c2
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
akcí	akce	k1gFnPc2	akce
pro	pro	k7c4
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
organizovali	organizovat	k5eAaBmAgMnP
Salesiáni	salesián	k1gMnPc1	salesián
tajně	tajně	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6	doba
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Otevřené	otevřený	k2eAgInPc1d1	otevřený
kluby	klub	k1gInPc1	klub
===	===	k?
</s>
</p>
<p>
<s>
Salesiánské	salesiánský	k2eAgNnSc1d1	Salesiánské
středisko	středisko	k1gNnSc1	středisko
mádeže	mádež	k1gFnSc2	mádež
provozuje	provozovat	k5eAaImIp3nS
tzv.	tzv.	kA
Otevřené	otevřený	k2eAgInPc1d1	otevřený
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
zaměřeny	zaměřit	k5eAaPmNgInP
na	na	k7c6
práci	práce	k1gFnSc6	práce
s	s	k7c7
neorganizovanými	organizovaný	k2eNgFnPc7d1	neorganizovaná
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC
mládeží	mládež	k1gFnSc7	mládež
a	a	k8xC
s	s	k7c7
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC
mládeží	mládež	k1gFnSc7	mládež
z	z	k7c2
romské	romský	k2eAgFnSc2d1	romská
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
byly	být	k5eAaImAgInP
programy	program	k1gInPc1	program
etnicky	etnicky	k6eAd1
smíšené	smíšený	k2eAgInPc1d1	smíšený
<g/>
,	,	kIx,
později	pozdě	k6eAd2
ale	ale	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
rozdělení	rozdělení	k1gNnSc3	rozdělení
na	na	k7c4
část	část	k1gFnSc4	část
věnující	věnující	k2eAgFnSc4d1	věnující
se	se	k3xPyFc4
Romům	Rom	k1gMnPc3	Rom
a	a	k8xC
na	na	k7c4
část	část	k1gFnSc4	část
věnující	věnující	k2eAgFnSc4d1	věnující
se	se	k3xPyFc4
"	"	kIx"
<g/>
neromům	nerom	k1gInPc3	nerom
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
programech	program	k1gInPc6	program
otevřených	otevřený	k2eAgInPc2d1	otevřený
klubů	klub	k1gInPc2	klub
je	být	k5eAaImIp3nS
zařazeno	zařazen	k2eAgNnSc4d1	zařazeno
doučování	doučování	k1gNnSc4	doučování
<g/>
,	,	kIx,
sociální	sociální	k2eAgNnPc1d1	sociální
poradenství	poradenství	k1gNnPc1	poradenství
<g/>
,	,	kIx,
volnočasové	volnočasový	k2eAgFnPc1d1	volnočasová
aktivity	aktivita	k1gFnPc1	aktivita
a	a	k8xC
terénní	terénní	k2eAgFnPc1d1	terénní
práce	práce	k1gFnPc1	práce
s	s	k7c7
dětmi	dítě	k1gFnPc7	dítě
na	na	k7c6
sídlišti	sídliště	k1gNnSc6	sídliště
Máj	Mája	k1gFnPc2	Mája
i	i	k9
s	s	k7c7
romskými	romský	k2eAgFnPc7d1	romská
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Dobrovolnictví	dobrovolnictví	k1gNnSc2	dobrovolnictví
==	==	k?
</s>
</p>
<p>
<s>
Středisko	středisko	k1gNnSc1	středisko
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
práce	práce	k1gFnPc4	práce
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
a	a	k8xC
praktikantů	praktikant	k1gMnPc2	praktikant
z	z	k7c2
Teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,
ze	z	k7c2
Zdravotně-sociální	Zdravotněociální	k2eAgFnSc2d1	Zdravotně-sociální
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC
z	z	k7c2
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
Univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC
Vyšší	vysoký	k2eAgFnSc2d2	vyšší
odborné	odborný	k2eAgFnSc2d1	odborná
školy	škola	k1gFnSc2	škola
sociální	sociální	k2eAgFnSc2d1	sociální
v	v	k7c6
Prachaticích	Prachatice	k1gFnPc6	Prachatice
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nP
na	na	k7c6
všech	všecek	k3xTgFnPc6
jeho	jeho	k3xOp3gFnPc6
činnostech	činnost	k1gFnPc6	činnost
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středisko	středisko	k1gNnSc1	středisko
je	být	k5eAaImIp3nS
klinickým	klinický	k2eAgNnSc7d1	klinické
pracovištěm	pracoviště	k1gNnSc7	pracoviště
Teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC
Zdravotně	zdravotně	k6eAd1
sociální	sociální	k2eAgFnSc2d1	sociální
fakulty	fakulta	k1gFnSc2	fakulta
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
pracovalo	pracovat	k5eAaImAgNnS
ve	v	k7c6
Středisku	středisko	k1gNnSc6	středisko
pravidelně	pravidelně	k6eAd1
63	#num#	k4
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
a	a	k8xC
53	#num#	k4
praktikantů	praktikant	k1gMnPc2	praktikant
a	a	k8xC
celkem	celkem	k6eAd1
75	#num#	k4
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
se	se	k3xPyFc4
podílelo	podílet	k5eAaImAgNnS
na	na	k7c6
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC
průběhu	průběh	k1gInSc6	průběh
letních	letní	k2eAgInPc2d1	letní
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.
<g/>
V	v	k7c6
roce	rok	k1gInSc6	rok
2011	#num#	k4
se	se	k3xPyFc4
Salesiánské	salesiánský	k2eAgNnSc1d1	Salesiánské
středisko	středisko	k1gNnSc1	středisko
zapojilo	zapojit	k5eAaPmAgNnS
spolu	spolu	k6eAd1
s	s	k7c7
místním	místní	k2eAgNnSc7d1	místní
dobrovolnickým	dobrovolnický	k2eAgNnSc7d1	Dobrovolnické
centrem	centrum	k1gNnSc7	centrum
organizace	organizace	k1gFnSc2	organizace
ADRA	ADRA	kA
<g/>
,	,	kIx,
dobrovolnickým	dobrovolnický	k2eAgNnSc7d1	Dobrovolnické
centrem	centrum	k1gNnSc7	centrum
<g />
.	.	kIx.
</s>
<s hack="1">
Diecézní	diecézní	k2eAgFnPc4d1	diecézní
charity	charita	k1gFnPc4	charita
a	a	k8xC
s	s	k7c7
Ústavem	ústav	k1gInSc7	ústav
zdravotně	zdravotně	k6eAd1
sociální	sociální	k2eAgFnSc2d1	sociální
práce	práce	k1gFnSc2	práce
Zdravotně-sociální	Zdravotněociální	k2eAgFnSc2d1	Zdravotně-sociální
fakulty	fakulta	k1gFnSc2	fakulta
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
univerzity	univerzita	k1gFnSc2	univerzita
do	do	k7c2
projektu	projekt	k1gInSc2	projekt
Dobrovolnictví	dobrovolnictví	k1gNnSc2	dobrovolnictví
pro	pro	k7c4
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6	rámec
byla	být	k5eAaImAgFnS
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
benefiční	benefiční	k2eAgFnSc1d1	benefiční
akce	akce	k1gFnSc1	akce
v	v	k7c6
místním	místní	k2eAgNnSc6d1	místní
Mercury	Mercur	k1gInPc4	Mercur
centru	centr	k1gInSc2	centr
a	a	k8xC
v	v	k7c6
ulicích	ulice	k1gFnPc6	ulice
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
proběhl	proběhnout	k5eAaPmAgInS
tzv.	tzv.	kA
Průvod	průvod	k1gInSc1	průvod
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,
pořádaný	pořádaný	k2eAgMnSc1d1	pořádaný
v	v	k7c6
celkem	celkem	k6eAd1
dvanácti	dvanáct	k4xCc6
městech	město	k1gNnPc6	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
současně	současně	k6eAd1
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?
</s>
</p>
<p>
<s>
===	===	k?
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
svatého	svatý	k2eAgMnSc2d1	svatý
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
Salesiáni	salesián	k1gMnPc1	salesián
Dona	dona	k1gFnSc1	dona
Bosca	Bosca	k1gFnSc1	Bosca
</s>
</p>
<p>
<s>
===	===	k?
Reference	reference	k1gFnPc1	reference
===	===	k?
</s>
</p>
<p>
<s>
===	===	k?
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
SaSM	SaSM	k1gFnSc2	SaSM
–	–	k?
DDM	DDM	kA
ČB	ČB	kA
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC
videa	video	k1gNnSc2	video
k	k	k7c3
tématu	téma	k1gNnSc3	téma
Salesiánské	salesiánský	k2eAgFnPc1d1	Salesiánská
středisko	středisko	k1gNnSc4	středisko
mládeže	mládež	k1gFnSc2	mládež
-	-	kIx~
dům	dům	k1gInSc1	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC
mládeže	mládež	k1gFnSc2	mládež
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
ve	v	k7c4
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Komedie	komedie	k1gFnSc1	komedie
Stan	stan	k1gInSc1	stan
na	na	k7c6
Youtube	Youtub	k1gInSc5	Youtub
</s>
</p>
<p>
<s>
Bezpečně	bezpečně	k6eAd1
na	na	k7c6
kole	kolo	k1gNnSc6	kolo
na	na	k7c6
Youtube	Youtub	k1gInSc5	Youtub
</s>
</p>
<p>
<s>
Sousedé	soused	k1gMnPc1	soused
od	od	k7c2
věže	věž	k1gFnSc2	věž
na	na	k7c4
Internet	Internet	k1gInSc4	Internet
Archive	archiv	k1gInSc5	archiv
</s>
</p>
