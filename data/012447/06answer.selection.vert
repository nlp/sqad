<s>
Salesiáni	salesián	k1gMnPc1	salesián
působili	působit	k5eAaImAgMnP
v	v	k7c6
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
neveřejně	veřejně	k6eNd1
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2	rok
1980	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
správa	správa	k1gFnSc1	správa
zdejší	zdejší	k2eAgFnSc2d1	zdejší
farnosti	farnost	k1gFnSc2	farnost
byla	být	k5eAaImAgFnS
salesiánům	salesián	k1gMnPc3	salesián
oficiálně	oficiálně	k6eAd1
svěřena	svěřen	k2eAgFnSc1d1	svěřena
až	až	k9
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2	červenec
1990	#num#	k4
<g/>
.	.	kIx.
</s>
