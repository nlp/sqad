<p>
<s>
Jazz	jazz	k1gInSc1	jazz
čili	čili	k8xC	čili
džez	džez	k1gInSc1	džez
(	(	kIx(	(
<g/>
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
americký	americký	k2eAgInSc1d1	americký
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
mezi	mezi	k7c7	mezi
afroamerickou	afroamerický	k2eAgFnSc7d1	afroamerická
komunitou	komunita	k1gFnSc7	komunita
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
smíšením	smíšení	k1gNnSc7	smíšení
afrických	africký	k2eAgFnPc2d1	africká
a	a	k8xC	a
evropských	evropský	k2eAgInPc2d1	evropský
hudebních	hudební	k2eAgInPc2d1	hudební
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
africké	africký	k2eAgInPc1d1	africký
kořeny	kořen	k1gInPc1	kořen
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
blues	blues	k1gFnPc2	blues
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
odpovídaček	odpovídačka	k1gFnPc2	odpovídačka
<g/>
,	,	kIx,	,
improvizace	improvizace	k1gFnSc2	improvizace
<g/>
,	,	kIx,	,
polyrytmů	polyrytm	k1gInPc2	polyrytm
<g/>
,	,	kIx,	,
synkop	synkopa	k1gFnPc2	synkopa
a	a	k8xC	a
spláchnutých	spláchnutý	k2eAgInPc2d1	spláchnutý
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přebíral	přebírat	k5eAaImAgMnS	přebírat
i	i	k8xC	i
prvky	prvek	k1gInPc4	prvek
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
této	tento	k3xDgFnSc2	tento
hudby	hudba	k1gFnSc2	hudba
poprvé	poprvé	k6eAd1	poprvé
použito	použít	k5eAaPmNgNnS	použít
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zkomolenou	zkomolený	k2eAgFnSc4d1	zkomolená
zkratku	zkratka	k1gFnSc4	zkratka
jas	jas	k1gInSc1	jas
<g/>
(	(	kIx(	(
<g/>
mine	minout	k5eAaImIp3nS	minout
<g/>
)	)	kIx)	)
v	v	k7c6	v
narážce	narážka	k1gFnSc6	narážka
na	na	k7c4	na
jasmínový	jasmínový	k2eAgInSc4d1	jasmínový
parfém	parfém	k1gInSc4	parfém
populární	populární	k2eAgInSc4d1	populární
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc4	Orleans
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Jazz	jazz	k1gInSc1	jazz
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
počátku	počátek	k1gInSc2	počátek
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
mnoho	mnoho	k4c4	mnoho
podžánrů	podžánr	k1gInPc2	podžánr
<g/>
,	,	kIx,	,
od	od	k7c2	od
New	New	k1gFnSc2	New
Orleánského	orleánský	k2eAgInSc2d1	orleánský
dixielandu	dixieland	k1gInSc2	dixieland
datovaného	datovaný	k2eAgInSc2d1	datovaný
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
10	[number]	k4	10
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
big	big	k?	big
bandovský	bandovský	k2eAgInSc4d1	bandovský
swing	swing	k1gInSc4	swing
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
bebop	bebop	k1gInSc1	bebop
ze	z	k7c2	z
středních	střední	k2eAgInPc2d1	střední
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
Latin-jazzu	Latinazz	k1gInSc2	Latin-jazz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
kombinací	kombinace	k1gFnPc2	kombinace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Afro-Cuban	Afro-Cuban	k1gMnSc1	Afro-Cuban
a	a	k8xC	a
Brazilian	Brazilian	k1gMnSc1	Brazilian
jazz	jazz	k1gInSc4	jazz
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jazz-rock	jazzock	k6eAd1	jazz-rock
fusion	fusion	k1gInSc1	fusion
ze	z	k7c2	z
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
až	až	k9	až
po	po	k7c4	po
pozdější	pozdní	k2eAgInSc4d2	pozdější
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
acid	acid	k1gInSc1	acid
jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
připlulo	připlout	k5eAaPmAgNnS	připlout
do	do	k7c2	do
USA	USA	kA	USA
skoro	skoro	k6eAd1	skoro
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
Afričanů	Afričan	k1gMnPc2	Afričan
–	–	k?	–
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
přivezli	přivézt	k5eAaPmAgMnP	přivézt
s	s	k7c7	s
sebou	se	k3xPyFc7	se
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
kmenovou	kmenový	k2eAgFnSc4d1	kmenová
hudební	hudební	k2eAgFnSc4d1	hudební
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Africká	africký	k2eAgFnSc1d1	africká
hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
účelová	účelový	k2eAgFnSc1d1	účelová
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
nebo	nebo	k8xC	nebo
rituály	rituál	k1gInPc4	rituál
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pracovních	pracovní	k2eAgFnPc2d1	pracovní
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
bojových	bojový	k2eAgInPc2d1	bojový
pokřiků	pokřik	k1gInPc2	pokřik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
africké	africký	k2eAgFnSc2d1	africká
tradice	tradice	k1gFnSc2	tradice
měli	mít	k5eAaImAgMnP	mít
jednohlasou	jednohlasý	k2eAgFnSc4d1	jednohlasá
melodii	melodie	k1gFnSc4	melodie
a	a	k8xC	a
používali	používat	k5eAaImAgMnP	používat
odpovídačky	odpovídačka	k1gFnPc4	odpovídačka
(	(	kIx(	(
<g/>
call-and-response	callndesponse	k6eAd1	call-and-response
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
evropské	evropský	k2eAgFnSc2d1	Evropská
představy	představa	k1gFnSc2	představa
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Rytmy	rytmus	k1gInPc1	rytmus
odrážely	odrážet	k5eAaImAgInP	odrážet
africký	africký	k2eAgInSc4d1	africký
způsob	způsob	k1gInSc4	způsob
mluveného	mluvený	k2eAgInSc2d1	mluvený
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Používali	používat	k5eAaImAgMnP	používat
pentatonické	pentatonický	k2eAgFnPc4d1	pentatonická
stupnice	stupnice	k1gFnPc4	stupnice
prokládané	prokládaný	k2eAgFnSc3d1	prokládaná
blue	blue	k1gFnSc3	blue
tóny	tón	k1gInPc4	tón
v	v	k7c6	v
blues	blues	k1gFnSc6	blues
a	a	k8xC	a
jazzu	jazz	k1gInSc6	jazz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rostl	růst	k5eAaImAgInS	růst
počet	počet	k1gInSc1	počet
černošských	černošský	k2eAgMnPc2d1	černošský
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
učili	učit	k5eAaImAgMnP	učit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
evropské	evropský	k2eAgInPc4d1	evropský
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
především	především	k9	především
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
používali	používat	k5eAaImAgMnP	používat
k	k	k7c3	k
parodování	parodování	k1gNnSc3	parodování
evropské	evropský	k2eAgFnSc2d1	Evropská
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
tradičních	tradiční	k2eAgInPc6d1	tradiční
tancích	tanec	k1gInPc6	tanec
cakewalk	cakewalk	k6eAd1	cakewalk
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
umělci	umělec	k1gMnPc1	umělec
(	(	kIx(	(
<g/>
černě	černě	k6eAd1	černě
namaskovaní	namaskovaný	k2eAgMnPc1d1	namaskovaný
<g/>
)	)	kIx)	)
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
a	a	k8xC	a
amerických	americký	k2eAgFnPc6d1	americká
pěveckých	pěvecký	k2eAgFnPc6d1	pěvecká
show	show	k1gFnPc6	show
rozšiřovali	rozšiřovat	k5eAaImAgMnP	rozšiřovat
černošskou	černošský	k2eAgFnSc4d1	černošská
hudbu	hudba	k1gFnSc4	hudba
mezi	mezi	k7c4	mezi
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
kombinovali	kombinovat	k5eAaImAgMnP	kombinovat
synkopy	synkopa	k1gFnPc4	synkopa
a	a	k8xC	a
evropský	evropský	k2eAgInSc4d1	evropský
harmonický	harmonický	k2eAgInSc4d1	harmonický
doprovod	doprovod	k1gInSc4	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Louis	Louis	k1gMnSc1	Louis
Moreau	Moreaus	k1gInSc2	Moreaus
Gottschalk	Gottschalk	k1gMnSc1	Gottschalk
upravil	upravit	k5eAaPmAgMnS	upravit
africko-americkou	africkomerický	k2eAgFnSc4d1	africko-americký
hudbu	hudba	k1gFnSc4	hudba
cakewalk	cakewalka	k1gFnPc2	cakewalka
<g/>
,	,	kIx,	,
jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
<g/>
,	,	kIx,	,
karibské	karibský	k2eAgFnSc2d1	karibská
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
otrocké	otrocký	k2eAgFnPc4d1	otrocká
melodie	melodie	k1gFnPc4	melodie
jako	jako	k8xS	jako
salonní	salonní	k2eAgFnPc4d1	salonní
písně	píseň	k1gFnPc4	píseň
pro	pro	k7c4	pro
piano	piano	k1gNnSc4	piano
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
přišlo	přijít	k5eAaPmAgNnS	přijít
od	od	k7c2	od
černošských	černošský	k2eAgMnPc2d1	černošský
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
učili	učít	k5eAaPmAgMnP	učít
harmonický	harmonický	k2eAgInSc4d1	harmonický
styl	styl	k1gInSc4	styl
chorálů	chorál	k1gInPc2	chorál
a	a	k8xC	a
včlenili	včlenit	k5eAaPmAgMnP	včlenit
je	on	k3xPp3gMnPc4	on
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
spirituálů	spirituál	k1gInPc2	spirituál
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
blues	blues	k1gNnSc2	blues
je	být	k5eAaImIp3nS	být
nedoložený	doložený	k2eNgMnSc1d1	nedoložený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k8xC	jako
světská	světský	k2eAgFnSc1d1	světská
podoba	podoba	k1gFnSc1	podoba
spirituálů	spirituál	k1gMnPc2	spirituál
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Oliver	Oliver	k1gMnSc1	Oliver
věnoval	věnovat	k5eAaImAgMnS	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
podobě	podoba	k1gFnSc3	podoba
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
funkcí	funkce	k1gFnPc2	funkce
s	s	k7c7	s
grioty	griot	k1gInPc7	griot
v	v	k7c6	v
savaně	savana	k1gFnSc6	savana
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ragtime	ragtime	k1gInSc4	ragtime
===	===	k?	===
</s>
</p>
<p>
<s>
Osvobození	osvobození	k1gNnSc1	osvobození
otroků	otrok	k1gMnPc2	otrok
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
novým	nový	k2eAgFnPc3d1	nová
možnostem	možnost	k1gFnPc3	možnost
vzdělání	vzdělání	k1gNnSc2	vzdělání
osvobozených	osvobozený	k2eAgMnPc2d1	osvobozený
Afroameričanů	Afroameričan	k1gMnPc2	Afroameričan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
striktní	striktní	k2eAgNnSc1d1	striktní
sociální	sociální	k2eAgNnSc1d1	sociální
oddělení	oddělení	k1gNnSc1	oddělení
znamenalo	znamenat	k5eAaImAgNnS	znamenat
omezené	omezený	k2eAgFnPc4d1	omezená
pracovní	pracovní	k2eAgFnPc4d1	pracovní
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Černošští	černošský	k2eAgMnPc1d1	černošský
muzikanti	muzikant	k1gMnPc1	muzikant
připravovali	připravovat	k5eAaImAgMnP	připravovat
pro	pro	k7c4	pro
nižší	nízký	k2eAgFnPc4d2	nižší
společenské	společenský	k2eAgFnPc4d1	společenská
vrstvy	vrstva	k1gFnPc4	vrstva
zábavu	zábav	k1gInSc2	zábav
jako	jako	k8xC	jako
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
pěvecké	pěvecký	k2eAgNnSc1d1	pěvecké
show	show	k1gNnSc1	show
<g/>
,	,	kIx,	,
pianisté	pianista	k1gMnPc1	pianista
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
barech	bar	k1gInPc6	bar
<g/>
,	,	kIx,	,
klubech	klub	k1gInPc6	klub
a	a	k8xC	a
nevěstincích	nevěstinec	k1gInPc6	nevěstinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
první	první	k4xOgMnSc1	první
bělošský	bělošský	k2eAgMnSc1d1	bělošský
skladatel	skladatel	k1gMnSc1	skladatel
William	William	k1gInSc4	William
H.	H.	kA	H.
Krell	Krell	k1gInSc1	Krell
vydal	vydat	k5eAaPmAgInS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
Mississippi	Mississippi	k1gFnSc4	Mississippi
Rag	Rag	k1gFnSc2	Rag
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
jako	jako	k9	jako
první	první	k4xOgNnSc4	první
napsané	napsaný	k2eAgNnSc4d1	napsané
instrumentální	instrumentální	k2eAgInSc4d1	instrumentální
ragtime	ragtime	k1gInSc4	ragtime
pro	pro	k7c4	pro
piano	piano	k1gNnSc4	piano
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
rok	rok	k1gInSc4	rok
napsal	napsat	k5eAaPmAgMnS	napsat
pianista	pianista	k1gMnSc1	pianista
Scott	Scotta	k1gFnPc2	Scotta
Joplin	Joplin	k1gInSc4	Joplin
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Original	Original	k1gFnSc4	Original
Rags	Ragsa	k1gFnPc2	Ragsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
vydal	vydat	k5eAaPmAgInS	vydat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hit	hit	k1gInSc1	hit
"	"	kIx"	"
<g/>
Maple	Maple	k1gFnSc1	Maple
Leaf	Leaf	k1gMnSc1	Leaf
Rag	Rag	k1gMnSc1	Rag
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
řadu	řada	k1gFnSc4	řada
známých	známý	k2eAgInPc2d1	známý
ragtimů	ragtime	k1gInPc2	ragtime
kombinujících	kombinující	k2eAgInPc2d1	kombinující
synkopy	synkopa	k1gFnPc4	synkopa
<g/>
,	,	kIx,	,
banjo	banjo	k1gNnSc4	banjo
figury	figura	k1gFnSc2	figura
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
call-and-response	callndesponse	k6eAd1	call-and-response
<g/>
.	.	kIx.	.
</s>
<s>
Blues	blues	k1gNnPc3	blues
vydal	vydat	k5eAaPmAgMnS	vydat
a	a	k8xC	a
zpopularizoval	zpopularizovat	k5eAaPmAgMnS	zpopularizovat
W.	W.	kA	W.
C.	C.	kA	C.
Handy	Handa	k1gMnSc2	Handa
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
"	"	kIx"	"
<g/>
Memphis	Memphis	k1gFnSc1	Memphis
Blues	blues	k1gFnSc1	blues
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
a	a	k8xC	a
"	"	kIx"	"
<g/>
St.	st.	kA	st.
Louis	louis	k1gInSc1	louis
Blues	blues	k1gNnSc1	blues
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
jazzovými	jazzový	k2eAgInPc7d1	jazzový
standardy	standard	k1gInPc7	standard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Muzika	muzika	k1gFnSc1	muzika
z	z	k7c2	z
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
===	===	k?	===
</s>
</p>
<p>
<s>
Muzika	muzika	k1gFnSc1	muzika
z	z	k7c2	z
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
měla	mít	k5eAaImAgFnS	mít
dalekosáhlý	dalekosáhlý	k2eAgInSc4d1	dalekosáhlý
efekt	efekt	k1gInSc4	efekt
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
raného	raný	k2eAgInSc2d1	raný
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
tamních	tamní	k2eAgMnPc2d1	tamní
jazzových	jazzový	k2eAgMnPc2d1	jazzový
interpretů	interpret	k1gMnPc2	interpret
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
nevěstincích	nevěstinec	k1gInPc6	nevěstinec
a	a	k8xC	a
barech	bar	k1gInPc6	bar
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Storyville	Storyville	k1gFnSc2	Storyville
<g/>
.	.	kIx.	.
</s>
<s>
Pochodové	pochodový	k2eAgFnPc1d1	pochodová
kapely	kapela	k1gFnPc1	kapela
hrály	hrát	k5eAaImAgFnP	hrát
na	na	k7c6	na
pohřbech	pohřeb	k1gInPc6	pohřeb
africké	africký	k2eAgFnSc2d1	africká
komunity	komunita	k1gFnSc2	komunita
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
kapelách	kapela	k1gFnPc6	kapela
a	a	k8xC	a
v	v	k7c6	v
tanečních	taneční	k2eAgFnPc6d1	taneční
kapelách	kapela	k1gFnPc6	kapela
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základními	základní	k2eAgInPc7d1	základní
nástroji	nástroj	k1gInPc7	nástroj
pro	pro	k7c4	pro
jazz	jazz	k1gInSc4	jazz
<g/>
:	:	kIx,	:
žestě	žestě	k1gInPc1	žestě
a	a	k8xC	a
dechové	dechový	k2eAgInPc1d1	dechový
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
samy	sám	k3xTgInPc1	sám
učily	učít	k5eAaPmAgFnP	učít
afroamerické	afroamerický	k2eAgMnPc4d1	afroamerický
muzikanty	muzikant	k1gMnPc4	muzikant
(	(	kIx(	(
<g/>
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
pocházelo	pocházet	k5eAaImAgNnS	pocházet
z	z	k7c2	z
tradic	tradice	k1gFnPc2	tradice
pohřebních	pohřební	k2eAgNnPc2d1	pohřební
procesí	procesí	k1gNnPc2	procesí
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrály	hrát	k5eAaImAgInP	hrát
původní	původní	k2eAgFnSc4d1	původní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc6	rozšíření
raného	raný	k2eAgInSc2d1	raný
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
procestovaly	procestovat	k5eAaPmAgInP	procestovat
naskrz	naskrz	k6eAd1	naskrz
černošské	černošský	k2eAgFnSc2d1	černošská
komunity	komunita	k1gFnSc2	komunita
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
afrokreolští	afrokreolský	k2eAgMnPc1d1	afrokreolský
a	a	k8xC	a
afroameričtí	afroamerický	k2eAgMnPc1d1	afroamerický
muzikanti	muzikant	k1gMnPc1	muzikant
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
a	a	k8xC	a
jižních	jižní	k2eAgNnPc6d1	jižní
městech	město	k1gNnPc6	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Afrokreolský	Afrokreolský	k2eAgMnSc1d1	Afrokreolský
pianista	pianista	k1gMnSc1	pianista
Jelly	Jella	k1gFnSc2	Jella
Roll	Roll	k1gInSc1	Roll
Morton	Morton	k1gInSc1	Morton
začal	začít	k5eAaPmAgInS	začít
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
Storyville	Storyvilla	k1gFnSc6	Storyvilla
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
procestoval	procestovat	k5eAaPmAgInS	procestovat
s	s	k7c7	s
Vaudeville	vaudeville	k1gInSc4	vaudeville
show	show	k1gNnSc4	show
města	město	k1gNnSc2	město
na	na	k7c6	na
severu	sever	k1gInSc6	sever
USA	USA	kA	USA
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Chicaga	Chicago	k1gNnSc2	Chicago
a	a	k8xC	a
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
"	"	kIx"	"
<g/>
Jelly	Jell	k1gInPc1	Jell
Roll	Rolla	k1gFnPc2	Rolla
Blues	blues	k1gNnSc2	blues
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
jako	jako	k9	jako
první	první	k4xOgNnSc4	první
jazzové	jazzový	k2eAgNnSc4d1	jazzové
tištěné	tištěný	k2eAgNnSc4d1	tištěné
aranžmá	aranžmá	k1gNnSc4	aranžmá
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
muzikantů	muzikant	k1gMnPc2	muzikant
to	ten	k3xDgNnSc1	ten
přitáhlo	přitáhnout	k5eAaPmAgNnS	přitáhnout
do	do	k7c2	do
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
USA	USA	kA	USA
se	se	k3xPyFc4	se
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
hraní	hraní	k1gNnSc1	hraní
ragtime	ragtime	k1gInSc1	ragtime
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
James	James	k1gMnSc1	James
Reese	Reese	k1gFnSc2	Reese
Europe	Europ	k1gInSc5	Europ
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
symfonický	symfonický	k2eAgInSc1d1	symfonický
Cleff	Cleff	k1gInSc1	Cleff
Club	club	k1gInSc1	club
orchestr	orchestr	k1gInSc1	orchestr
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c6	na
benefičním	benefiční	k2eAgInSc6d1	benefiční
koncertu	koncert	k1gInSc6	koncert
v	v	k7c4	v
Carnegie	Carnegie	k1gFnPc4	Carnegie
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
"	"	kIx"	"
<g/>
Society	societa	k1gFnPc4	societa
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
nahrávkou	nahrávka	k1gFnSc7	nahrávka
černošské	černošský	k2eAgFnSc2d1	černošská
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Eubie	Eubie	k1gFnPc4	Eubie
Blake	Blake	k1gNnSc2	Blake
ovlivněný	ovlivněný	k2eAgMnSc1d1	ovlivněný
James	James	k1gMnSc1	James
P.	P.	kA	P.
Johnsonem	Johnson	k1gInSc7	Johnson
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
stride	strid	k1gInSc5	strid
piano	piano	k1gNnSc1	piano
playing	playing	k1gInSc4	playing
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
pravá	pravý	k2eAgFnSc1d1	pravá
ruka	ruka	k1gFnSc1	ruka
hraje	hrát	k5eAaImIp3nS	hrát
melodii	melodie	k1gFnSc4	melodie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
levá	levý	k2eAgFnSc1d1	levá
ruka	ruka	k1gFnSc1	ruka
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
rytmus	rytmus	k1gInSc4	rytmus
a	a	k8xC	a
basu	basa	k1gFnSc4	basa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Livery	Liver	k1gMnPc7	Liver
Stable	Stable	k1gFnSc2	Stable
Blues	blues	k1gNnSc1	blues
<g/>
"	"	kIx"	"
od	od	k7c2	od
Original	Original	k1gFnSc2	Original
Dixieland	dixieland	k1gInSc1	dixieland
Jass	Jass	k1gInSc1	Jass
Band	band	k1gInSc1	band
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
nahrávek	nahrávka	k1gFnPc2	nahrávka
raného	raný	k2eAgInSc2d1	raný
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
nespočet	nespočet	k1gInSc1	nespočet
kapel	kapela	k1gFnPc2	kapela
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nahrávky	nahrávka	k1gFnPc4	nahrávka
se	s	k7c7	s
zdůrazněním	zdůraznění	k1gNnSc7	zdůraznění
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
jazz	jazz	k1gInSc1	jazz
<g/>
"	"	kIx"	"
v	v	k7c6	v
názvu	název	k1gInSc6	název
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
spíše	spíše	k9	spíše
ragtime	ragtime	k1gInSc1	ragtime
než	než	k8xS	než
jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1917	[number]	k4	1917
W.	W.	kA	W.
C.	C.	kA	C.
Handy	Handa	k1gFnSc2	Handa
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
Orchestra	orchestra	k1gFnSc1	orchestra
of	of	k?	of
Memphis	Memphis	k1gFnSc1	Memphis
nahrála	nahrát	k5eAaPmAgFnS	nahrát
coververzi	coververze	k1gFnSc4	coververze
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Livery	Livera	k1gFnSc2	Livera
Stable	Stable	k1gFnSc2	Stable
Blues	blues	k1gFnSc2	blues
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1918	[number]	k4	1918
James	James	k1gInSc1	James
Reese	Reese	k1gFnSc2	Reese
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
pěchotní	pěchotní	k2eAgFnSc1d1	pěchotní
kapela	kapela	k1gFnSc1	kapela
"	"	kIx"	"
<g/>
Hellfighters	Hellfighters	k1gInSc1	Hellfighters
<g/>
"	"	kIx"	"
přivezla	přivézt	k5eAaPmAgFnS	přivézt
ragtime	ragtime	k1gInSc4	ragtime
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
nahráli	nahrát	k5eAaBmAgMnP	nahrát
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Darktown	Darktown	k1gMnSc1	Darktown
Strutter	Strutter	k1gMnSc1	Strutter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ball	Balla	k1gFnPc2	Balla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
==	==	k?	==
</s>
</p>
<p>
<s>
Prohibice	prohibice	k1gFnPc1	prohibice
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
nedovolovala	dovolovat	k5eNaImAgFnS	dovolovat
prodej	prodej	k1gInSc4	prodej
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Nezákonné	zákonný	k2eNgFnPc1d1	nezákonná
prodejny	prodejna	k1gFnPc1	prodejna
alkoholu	alkohol	k1gInSc2	alkohol
způsobily	způsobit	k5eAaPmAgFnP	způsobit
ruch	ruch	k1gInSc4	ruch
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
"	"	kIx"	"
<g/>
Jazzového	jazzový	k2eAgInSc2d1	jazzový
věku	věk	k1gInSc2	věk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
populární	populární	k2eAgFnSc1d1	populární
hudba	hudba	k1gFnSc1	hudba
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
aktuální	aktuální	k2eAgFnPc4d1	aktuální
taneční	taneční	k2eAgFnPc4d1	taneční
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnPc4d1	nová
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
divadelní	divadelní	k2eAgFnPc4d1	divadelní
melodie	melodie	k1gFnPc4	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Jazz	jazz	k1gInSc1	jazz
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nemorální	morální	k2eNgInPc4d1	nemorální
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
členů	člen	k1gInPc2	člen
starší	starý	k2eAgFnSc2d2	starší
generace	generace	k1gFnSc2	generace
to	ten	k3xDgNnSc1	ten
vidělo	vidět	k5eAaImAgNnS	vidět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
vyhrožování	vyhrožování	k1gNnSc4	vyhrožování
starým	starý	k2eAgFnPc3d1	stará
zásadám	zásada	k1gFnPc3	zásada
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
prosazování	prosazování	k1gNnSc6	prosazování
nových	nový	k2eAgFnPc2d1	nová
úpadkových	úpadkový	k2eAgFnPc2d1	úpadková
hodnot	hodnota	k1gFnPc2	hodnota
"	"	kIx"	"
<g/>
Roaring	Roaring	k1gInSc1	Roaring
Twenties	Twenties	k1gInSc1	Twenties
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
bouřlivých	bouřlivý	k2eAgNnPc2d1	bouřlivé
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
Kid	Kid	k1gMnPc2	Kid
Oryův	Oryův	k2eAgMnSc1d1	Oryův
Original	Original	k1gFnSc7	Original
Creole	Creole	k1gFnSc2	Creole
Jazz	jazz	k1gInSc1	jazz
Band	band	k1gInSc1	band
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
muzikantů	muzikant	k1gMnPc2	muzikant
z	z	k7c2	z
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Franciska	k1gFnSc4	Franciska
a	a	k8xC	a
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
nahráli	nahrát	k5eAaPmAgMnP	nahrát
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
rozvoje	rozvoj	k1gInSc2	rozvoj
jazzu	jazz	k1gInSc2	jazz
bylo	být	k5eAaImAgNnS	být
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
King	King	k1gInSc1	King
Oliver	Olivra	k1gFnPc2	Olivra
k	k	k7c3	k
sobě	se	k3xPyFc3	se
připojil	připojit	k5eAaPmAgInS	připojit
Billa	Bill	k1gMnSc4	Bill
Johnsona	Johnson	k1gMnSc4	Johnson
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
rok	rok	k1gInSc1	rok
spatřil	spatřit	k5eAaPmAgInS	spatřit
i	i	k9	i
první	první	k4xOgFnSc4	první
nahrávku	nahrávka	k1gFnSc4	nahrávka
Bessie	Bessie	k1gFnSc2	Bessie
Smithové	Smithový	k2eAgFnSc2d1	Smithová
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnSc2d3	nejznámější
bluesové	bluesový	k2eAgFnSc2d1	bluesová
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bix	Bix	k?	Bix
Beiderbecke	Beiderbecke	k1gInSc1	Beiderbecke
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
kapelu	kapela	k1gFnSc4	kapela
The	The	k1gFnSc3	The
Wolverines	Wolverinesa	k1gFnPc2	Wolverinesa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Louis	Louis	k1gMnSc1	Louis
Armstrong	Armstrong	k1gMnSc1	Armstrong
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
taneční	taneční	k2eAgFnSc3d1	taneční
kapele	kapela	k1gFnSc3	kapela
Fletcher	Fletchra	k1gFnPc2	Fletchra
Hendersona	Hendersona	k1gFnSc1	Hendersona
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
sólista	sólista	k1gMnSc1	sólista
na	na	k7c4	na
roh	roh	k1gInSc4	roh
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
zformoval	zformovat	k5eAaPmAgMnS	zformovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
skupinu	skupina	k1gFnSc4	skupina
Hot	hot	k0	hot
Five	Five	k1gInSc4	Five
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
scat	scat	k1gInSc4	scat
singing	singing	k1gInSc1	singing
(	(	kIx(	(
<g/>
pěvecká	pěvecký	k2eAgFnSc1d1	pěvecká
improvizace	improvizace	k1gFnSc1	improvizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelly	Jell	k1gInPc4	Jell
Roll	Roll	k1gMnSc1	Roll
Morton	Morton	k1gInSc4	Morton
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
Rhythm	Rhythm	k1gMnSc1	Rhythm
Kings	Kings	k1gInSc1	Kings
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
zformoval	zformovat	k5eAaPmAgMnS	zformovat
kapelu	kapela	k1gFnSc4	kapela
Red	Red	k1gMnSc1	Red
Hot	hot	k0	hot
Peppers	Peppers	k1gInSc4	Peppers
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
odbyt	odbyt	k1gInSc1	odbyt
výstřední	výstřední	k2eAgFnSc2d1	výstřední
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
hrající	hrající	k2eAgInSc1d1	hrající
bělošskými	bělošský	k2eAgInPc7d1	bělošský
orchestry	orchestr	k1gInPc7	orchestr
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k9	třeba
Orchestr	orchestr	k1gInSc1	orchestr
Jean	Jean	k1gMnSc1	Jean
Goldketta	Goldketta	k1gMnSc1	Goldketta
a	a	k8xC	a
Orchestr	orchestr	k1gInSc1	orchestr
Paul	Paula	k1gFnPc2	Paula
Whitemana	Whiteman	k1gMnSc4	Whiteman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
nacvičil	nacvičit	k5eAaBmAgInS	nacvičit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
Whiteman	Whiteman	k1gMnSc1	Whiteman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Orchestra	orchestra	k1gFnSc1	orchestra
Rhapsody	Rhapsod	k1gInPc1	Rhapsod
in	in	k?	in
Blue	Blu	k1gInSc2	Blu
od	od	k7c2	od
George	Georg	k1gMnSc2	Georg
Gershwina	Gershwin	k1gMnSc2	Gershwin
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
masový	masový	k2eAgInSc1d1	masový
příliv	příliv	k1gInSc1	příliv
hudebních	hudební	k2eAgInPc2d1	hudební
souborů	soubor	k1gInPc2	soubor
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
např.	např.	kA	např.
Fletcher	Fletchra	k1gFnPc2	Fletchra
Henderson	Hendersona	k1gFnPc2	Hendersona
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
band	band	k1gInSc1	band
<g/>
,	,	kIx,	,
Duke	Duke	k1gInSc1	Duke
Ellington	Ellington	k1gInSc1	Ellington
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
band	banda	k1gFnPc2	banda
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Earl	earl	k1gMnSc1	earl
Hines	Hines	k1gMnSc1	Hines
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Band	banda	k1gFnPc2	banda
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
uvedené	uvedený	k2eAgFnPc1d1	uvedená
měly	mít	k5eAaImAgFnP	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
big	big	k?	big
bandovského	bandovský	k2eAgInSc2d1	bandovský
swingu	swing	k1gInSc2	swing
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Swing	swing	k1gInSc4	swing
===	===	k?	===
</s>
</p>
<p>
<s>
Třicátá	třicátý	k4xOgNnPc4	třicátý
léta	léto	k1gNnPc4	léto
patřila	patřit	k5eAaImAgFnS	patřit
populárním	populární	k2eAgFnPc3d1	populární
swingovým	swingový	k2eAgFnPc3d1	swingová
big	big	k?	big
bandům	bando	k1gNnPc3	bando
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
sólisté	sólista	k1gMnPc1	sólista
zapsali	zapsat	k5eAaPmAgMnP	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
jako	jako	k8xS	jako
kapelníci	kapelník	k1gMnPc1	kapelník
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
Count	Count	k1gMnSc1	Count
Basie	Basie	k1gFnSc2	Basie
<g/>
,	,	kIx,	,
Cab	cabit	k5eAaImRp2nS	cabit
Calloway	Callowaa	k1gFnSc2	Callowaa
<g/>
,	,	kIx,	,
Fletcher	Fletchra	k1gFnPc2	Fletchra
Henderson	Hendersona	k1gFnPc2	Hendersona
<g/>
,	,	kIx,	,
Earl	earl	k1gMnSc1	earl
Hines	Hines	k1gMnSc1	Hines
<g/>
,	,	kIx,	,
Duke	Duke	k1gFnSc1	Duke
Ellington	Ellington	k1gInSc1	Ellington
<g/>
,	,	kIx,	,
Artie	Artie	k1gFnSc1	Artie
Shaw	Shaw	k1gFnSc1	Shaw
<g/>
,	,	kIx,	,
Tommy	Tomma	k1gFnPc1	Tomma
Dorsey	Dorsea	k1gFnPc1	Dorsea
<g/>
,	,	kIx,	,
Benny	Benna	k1gFnPc1	Benna
Goodman	Goodman	k1gMnSc1	Goodman
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Sinatra	Sinatra	k1gFnSc1	Sinatra
a	a	k8xC	a
Glenn	Glenn	k1gMnSc1	Glenn
Miller	Miller	k1gMnSc1	Miller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Swing	swing	k1gInSc1	swing
byl	být	k5eAaImAgInS	být
také	také	k9	také
taneční	taneční	k2eAgFnSc1d1	taneční
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rádiích	rádio	k1gNnPc6	rádio
živě	živě	k6eAd1	živě
vysílán	vysílán	k2eAgInSc4d1	vysílán
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Americe	Amerika	k1gFnSc6	Amerika
po	po	k7c4	po
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
nabízel	nabízet	k5eAaImAgInS	nabízet
individuálně	individuálně	k6eAd1	individuálně
muzikantům	muzikant	k1gMnPc3	muzikant
šanci	šance	k1gFnSc4	šance
zahrát	zahrát	k5eAaPmF	zahrát
si	se	k3xPyFc3	se
sóla	sólo	k1gNnPc1	sólo
a	a	k8xC	a
improvizované	improvizovaný	k2eAgFnPc1d1	improvizovaná
melodie	melodie	k1gFnPc1	melodie
<g/>
,	,	kIx,	,
tematická	tematický	k2eAgNnPc1d1	tematické
sóla	sólo	k1gNnPc1	sólo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
leckdy	leckdy	k6eAd1	leckdy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
komplexní	komplexní	k2eAgFnSc7d1	komplexní
a	a	k8xC	a
důležitou	důležitý	k2eAgFnSc7d1	důležitá
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
sociální	sociální	k2eAgFnSc1d1	sociální
kritika	kritika	k1gFnSc1	kritika
rasového	rasový	k2eAgNnSc2d1	rasové
oddělení	oddělení	k1gNnSc2	oddělení
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
normálu	normál	k1gInSc2	normál
a	a	k8xC	a
bělošští	bělošský	k2eAgMnPc1d1	bělošský
kapelníci	kapelník	k1gMnPc1	kapelník
zase	zase	k9	zase
začali	začít	k5eAaPmAgMnP	začít
nabírat	nabírat	k5eAaImF	nabírat
černošské	černošský	k2eAgMnPc4d1	černošský
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
středních	střední	k2eAgNnPc2d1	střední
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Benny	Benno	k1gNnPc7	Benno
Goodman	Goodman	k1gMnSc1	Goodman
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
pianistu	pianista	k1gMnSc4	pianista
Teddyho	Teddy	k1gMnSc4	Teddy
Wilsona	Wilson	k1gMnSc4	Wilson
<g/>
,	,	kIx,	,
vibrafonistu	vibrafonista	k1gMnSc4	vibrafonista
Lionela	Lionel	k1gMnSc4	Lionel
Hamptona	Hampton	k1gMnSc4	Hampton
a	a	k8xC	a
kytaristu	kytarista	k1gMnSc4	kytarista
Charlieho	Charlie	k1gMnSc4	Charlie
Christiana	Christian	k1gMnSc4	Christian
k	k	k7c3	k
připojení	připojení	k1gNnSc3	připojení
do	do	k7c2	do
malého	malý	k2eAgNnSc2d1	malé
seskupení	seskupení	k1gNnSc2	seskupení
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
raných	raný	k2eAgNnPc2d1	rané
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jumping	jumping	k1gInSc1	jumping
the	the	k?	the
blues	blues	k1gInSc1	blues
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
jump	jumpit	k5eAaPmRp2nS	jumpit
blues	blues	k1gNnSc4	blues
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
používali	používat	k5eAaImAgMnP	používat
malá	malý	k2eAgFnSc1d1	malá
komba	komba	k1gFnSc1	komba
(	(	kIx(	(
<g/>
jazzové	jazzový	k2eAgFnPc1d1	jazzová
kapely	kapela	k1gFnPc1	kapela
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
členy	člen	k1gInPc7	člen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
typické	typický	k2eAgNnSc4d1	typické
vysoké	vysoký	k2eAgNnSc4d1	vysoké
tempo	tempo	k1gNnSc4	tempo
skladby	skladba	k1gFnSc2	skladba
a	a	k8xC	a
bluesové	bluesový	k2eAgInPc1d1	bluesový
akordové	akordový	k2eAgInPc1d1	akordový
postupy	postup	k1gInPc1	postup
<g/>
.	.	kIx.	.
</s>
<s>
Kansas	Kansas	k1gInSc1	Kansas
City	city	k1gNnSc1	city
Jazz	jazz	k1gInSc1	jazz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
přemostění	přemostění	k1gNnSc4	přemostění
od	od	k7c2	od
big	big	k?	big
bandů	band	k1gInPc2	band
k	k	k7c3	k
bebopu	bebop	k1gInSc3	bebop
ovlivňující	ovlivňující	k2eAgFnSc1d1	ovlivňující
celá	celý	k2eAgFnSc1d1	celá
40	[number]	k4	40
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazz	jazz	k1gInSc4	jazz
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
USA	USA	kA	USA
začíná	začínat	k5eAaImIp3nS	začínat
jasný	jasný	k2eAgInSc4d1	jasný
evropský	evropský	k2eAgInSc4d1	evropský
styl	styl	k1gInSc4	styl
jazzu	jazz	k1gInSc2	jazz
vznikat	vznikat	k5eAaImF	vznikat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
Quintette	Quintett	k1gInSc5	Quintett
du	du	k?	du
Hot	hot	k0	hot
Club	club	k1gInSc4	club
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Belgický	belgický	k2eAgMnSc1d1	belgický
kytarový	kytarový	k2eAgMnSc1d1	kytarový
virtuos	virtuos	k1gMnSc1	virtuos
Django	Django	k1gMnSc1	Django
Reinhardt	Reinhardt	k1gMnSc1	Reinhardt
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
gypsy	gyps	k1gInPc4	gyps
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
mix	mix	k1gInSc1	mix
amerického	americký	k2eAgInSc2d1	americký
swingu	swing	k1gInSc2	swing
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
francouzských	francouzský	k2eAgFnPc2d1	francouzská
"	"	kIx"	"
<g/>
dud	dudy	k1gFnPc2	dudy
<g/>
"	"	kIx"	"
a	a	k8xC	a
východoevropského	východoevropský	k2eAgInSc2d1	východoevropský
folku	folk	k1gInSc2	folk
s	s	k7c7	s
pomalým	pomalý	k2eAgNnSc7d1	pomalé
<g/>
,	,	kIx,	,
svůdným	svůdný	k2eAgNnSc7d1	svůdné
cítěním	cítění	k1gNnSc7	cítění
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
nástroji	nástroj	k1gInPc7	nástroj
jsou	být	k5eAaImIp3nP	být
steel	steel	k1gInSc4	steel
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
kontrabas	kontrabas	k1gInSc4	kontrabas
<g/>
.	.	kIx.	.
</s>
<s>
Sólové	sólový	k2eAgInPc1d1	sólový
přechody	přechod	k1gInPc1	přechod
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc4	jeden
hráče	hráč	k1gMnSc4	hráč
na	na	k7c4	na
druhého	druhý	k4xOgMnSc4	druhý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
basa	basa	k1gFnSc1	basa
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
roli	role	k1gFnSc4	role
v	v	k7c6	v
rytmické	rytmický	k2eAgFnSc6d1	rytmická
sekci	sekce	k1gFnSc6	sekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dixieland	dixieland	k1gInSc1	dixieland
revival	revival	k1gInSc1	revival
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
začala	začít	k5eAaPmAgFnS	začít
oprašovat	oprašovat	k5eAaImF	oprašovat
dixieland	dixieland	k1gInSc4	dixieland
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
vracela	vracet	k5eAaImAgFnS	vracet
se	se	k3xPyFc4	se
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
kontrapunktivnímu	kontrapunktivní	k2eAgInSc3d1	kontrapunktivní
neworleánskému	worleánský	k2eNgInSc3d1	neworleánský
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
tomu	ten	k3xDgNnSc3	ten
napomáhali	napomáhat	k5eAaImAgMnP	napomáhat
nahrávací	nahrávací	k2eAgFnSc3d1	nahrávací
společnosti	společnost	k1gFnSc3	společnost
publikující	publikující	k2eAgFnSc2d1	publikující
rané	raný	k2eAgFnSc2d1	raná
jazzové	jazzový	k2eAgFnSc2d1	jazzová
klasiky	klasika	k1gFnSc2	klasika
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
Oliverovy	Oliverův	k2eAgFnSc2d1	Oliverova
<g/>
,	,	kIx,	,
Mortonovy	Mortonův	k2eAgFnSc2d1	Mortonova
a	a	k8xC	a
Armstrongovy	Armstrongův	k2eAgFnSc2d1	Armstrongova
kapely	kapela	k1gFnSc2	kapela
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Revivalů	revival	k1gInPc2	revival
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
začínali	začínat	k5eAaImAgMnP	začínat
jejich	jejich	k3xOp3gFnSc4	jejich
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
hráli	hrát	k5eAaImAgMnP	hrát
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
revivaly	revival	k1gInPc4	revival
vedla	vést	k5eAaImAgFnS	vést
skupina	skupina	k1gFnSc1	skupina
Bobcats	Bobcatsa	k1gFnPc2	Bobcatsa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
Bob	Bob	k1gMnSc1	Bob
Crosby	Crosba	k1gFnSc2	Crosba
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
vedoucími	vedoucí	k1gMnPc7	vedoucí
Dixieland	dixieland	k1gInSc4	dixieland
revivalisty	revivalista	k1gMnPc4	revivalista
byli	být	k5eAaImAgMnP	být
Max	Max	k1gMnSc1	Max
Kaminsky	Kaminsky	k1gMnSc1	Kaminsky
<g/>
,	,	kIx,	,
Eddie	Eddie	k1gFnSc1	Eddie
Condon	Condon	k1gMnSc1	Condon
a	a	k8xC	a
Wild	Wild	k1gMnSc1	Wild
Bill	Bill	k1gMnSc1	Bill
Davison	Davison	k1gMnSc1	Davison
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
skupinách	skupina	k1gFnPc6	skupina
pocházelo	pocházet	k5eAaImAgNnS	pocházet
ze	z	k7c2	z
středozápadu	středozápad	k1gInSc2	středozápad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
našel	najít	k5eAaPmAgMnS	najít
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
muzikantů	muzikant	k1gMnPc2	muzikant
z	z	k7c2	z
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
skupinou	skupina	k1gFnSc7	skupina
revivalistů	revivalista	k1gMnPc2	revivalista
byly	být	k5eAaImAgFnP	být
mladí	mladý	k2eAgMnPc1d1	mladý
muzikanti	muzikant	k1gMnPc1	muzikant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
příliš	příliš	k6eAd1	příliš
mladí	mladý	k2eAgMnPc1d1	mladý
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
zasažení	zasažení	k1gNnSc4	zasažení
počátky	počátek	k1gInPc1	počátek
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kteří	který	k3yIgMnPc1	který
nyní	nyní	k6eAd1	nyní
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
styl	styl	k1gInSc4	styl
swingu	swing	k1gInSc2	swing
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
preferovat	preferovat	k5eAaImF	preferovat
tradiční	tradiční	k2eAgFnPc4d1	tradiční
metody	metoda	k1gFnPc4	metoda
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Lu	Lu	k?	Lu
Watters	Watters	k1gInSc1	Watters
band	band	k1gInSc1	band
byl	být	k5eAaImAgInS	být
nejspíše	nejspíše	k9	nejspíše
tím	ten	k3xDgMnSc7	ten
největším	veliký	k2eAgMnSc7d3	veliký
prominentem	prominent	k1gMnSc7	prominent
této	tento	k3xDgFnSc2	tento
druhé	druhý	k4xOgFnSc2	druhý
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Louis	Louis	k1gMnSc1	Louis
Armstrong	Armstrong	k1gMnSc1	Armstrong
zformoval	zformovat	k5eAaPmAgMnS	zformovat
jeho	on	k3xPp3gInSc4	on
Allstars	Allstars	k1gInSc4	Allstars
band	band	k1gInSc1	band
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vedoucím	vedoucí	k2eAgInSc7d1	vedoucí
souborem	soubor	k1gInSc7	soubor
mezi	mezi	k7c4	mezi
dixieland	dixieland	k1gInSc4	dixieland
revivaly	revival	k1gInPc1	revival
<g/>
.	.	kIx.	.
</s>
<s>
Napříč	napříč	k6eAd1	napříč
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
léty	léto	k1gNnPc7	léto
byl	být	k5eAaImAgInS	být
dixieland	dixieland	k1gInSc1	dixieland
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
komerčně	komerčně	k6eAd1	komerčně
známým	známý	k2eAgInSc7d1	známý
jazzovým	jazzový	k2eAgInSc7d1	jazzový
stylem	styl	k1gInSc7	styl
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bebop	bebop	k1gInSc4	bebop
===	===	k?	===
</s>
</p>
<p>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
bebopeři	beboper	k1gMnPc1	beboper
snažili	snažit	k5eAaImAgMnP	snažit
pomoci	pomoct	k5eAaPmF	pomoct
vyzdvihnout	vyzdvihnout	k5eAaPmF	vyzdvihnout
jazz	jazz	k1gInSc4	jazz
z	z	k7c2	z
taneční	taneční	k2eAgFnSc2d1	taneční
populární	populární	k2eAgFnSc2d1	populární
muziky	muzika	k1gFnSc2	muzika
k	k	k7c3	k
více	hodně	k6eAd2	hodně
vyzývavé	vyzývavý	k2eAgFnSc3d1	vyzývavá
"	"	kIx"	"
<g/>
hudební	hudební	k2eAgFnSc3d1	hudební
<g/>
"	"	kIx"	"
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
odlišnosti	odlišnost	k1gFnPc1	odlišnost
od	od	k7c2	od
swingu	swing	k1gInSc2	swing
rozváděli	rozvádět	k5eAaImAgMnP	rozvádět
první	první	k4xOgMnPc1	první
bebopeři	beboper	k1gMnPc1	beboper
prvky	prvek	k1gInPc7	prvek
z	z	k7c2	z
taneční	taneční	k2eAgFnSc2d1	taneční
muziky	muzika	k1gFnSc2	muzika
<g/>
,	,	kIx,	,
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
bebop	bebop	k1gInSc4	bebop
více	hodně	k6eAd2	hodně
jako	jako	k8xC	jako
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
trošku	trošku	k6eAd1	trošku
jako	jako	k8xC	jako
potenciálně	potenciálně	k6eAd1	potenciálně
populární	populární	k2eAgFnSc1d1	populární
a	a	k8xC	a
komerční	komerční	k2eAgFnSc1d1	komerční
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Vlivnými	vlivný	k2eAgMnPc7d1	vlivný
muzikanty	muzikant	k1gMnPc7	muzikant
v	v	k7c6	v
bebopu	bebop	k1gInSc6	bebop
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
saxofonista	saxofonista	k1gMnSc1	saxofonista
Charlie	Charlie	k1gMnSc1	Charlie
Parker	Parker	k1gMnSc1	Parker
<g/>
,	,	kIx,	,
pianista	pianista	k1gMnSc1	pianista
Bud	bouda	k1gFnPc2	bouda
Powell	Powell	k1gMnSc1	Powell
a	a	k8xC	a
Thelonious	Thelonious	k1gMnSc1	Thelonious
Monk	Monk	k1gMnSc1	Monk
<g/>
,	,	kIx,	,
trumpetista	trumpetista	k1gMnSc1	trumpetista
Dizzy	Dizza	k1gFnSc2	Dizza
Gillespie	Gillespie	k1gFnSc2	Gillespie
a	a	k8xC	a
Clifford	Clifford	k1gMnSc1	Clifford
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
basista	basista	k1gMnSc1	basista
Ray	Ray	k1gMnSc1	Ray
Brown	Brown	k1gMnSc1	Brown
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Max	Max	k1gMnSc1	Max
Roach	Roach	k1gMnSc1	Roach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bebopeři	Beboper	k1gMnPc1	Beboper
vnášeli	vnášet	k5eAaImAgMnP	vnášet
do	do	k7c2	do
jazzu	jazz	k1gInSc2	jazz
nové	nový	k2eAgFnSc2d1	nová
formy	forma	k1gFnSc2	forma
barevnosti	barevnost	k1gFnSc2	barevnost
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
nesouzvuku	nesouzvuk	k1gInSc2	nesouzvuk
a	a	k8xC	a
více	hodně	k6eAd2	hodně
abstraktních	abstraktní	k2eAgFnPc2d1	abstraktní
forem	forma	k1gFnPc2	forma
improvizací	improvizace	k1gFnPc2	improvizace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
používali	používat	k5eAaImAgMnP	používat
akordové	akordový	k2eAgInPc4d1	akordový
"	"	kIx"	"
<g/>
mimoně	mimoň	k1gMnPc4	mimoň
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
náhradní	náhradní	k2eAgInPc1d1	náhradní
akordy	akord	k1gInPc1	akord
a	a	k8xC	a
pozměněné	pozměněný	k2eAgInPc1d1	pozměněný
akordy	akord	k1gInPc1	akord
<g/>
.	.	kIx.	.
</s>
<s>
Měnil	měnit	k5eAaImAgMnS	měnit
se	se	k3xPyFc4	se
i	i	k9	i
styl	styl	k1gInSc1	styl
bubnování	bubnování	k1gNnSc2	bubnování
na	na	k7c4	na
více	hodně	k6eAd2	hodně
úskočný	úskočný	k2eAgInSc4d1	úskočný
a	a	k8xC	a
explozivní	explozivní	k2eAgInSc4d1	explozivní
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
byl	být	k5eAaImAgInS	být
činel	činel	k1gInSc1	činel
ride	ride	k6eAd1	ride
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
rytmu	rytmus	k1gInSc2	rytmus
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
snare	snar	k1gInSc5	snar
a	a	k8xC	a
basový	basový	k2eAgInSc1d1	basový
buben	buben	k1gInSc1	buben
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
neočekávané	očekávaný	k2eNgInPc4d1	neočekávaný
akcenty	akcent	k1gInPc4	akcent
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
hlavního	hlavní	k2eAgInSc2d1	hlavní
jazzového	jazzový	k2eAgInSc2d1	jazzový
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
setkávaly	setkávat	k5eAaImAgFnP	setkávat
s	s	k7c7	s
nepochopením	nepochopení	k1gNnSc7	nepochopení
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
byly	být	k5eAaImAgInP	být
nepřátelské	přátelský	k2eNgInPc1d1	nepřátelský
ohlasy	ohlas	k1gInPc1	ohlas
mezi	mezi	k7c7	mezi
fandy	fandy	k?	fandy
a	a	k8xC	a
kolegy	kolega	k1gMnPc7	kolega
muzikanty	muzikant	k1gMnPc7	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k6eAd1	přesto
<g/>
,	,	kIx,	,
po	po	k7c6	po
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
bebop	bebop	k1gInSc1	bebop
zapsal	zapsat	k5eAaPmAgInS	zapsat
do	do	k7c2	do
jazzového	jazzový	k2eAgInSc2d1	jazzový
slovníku	slovník	k1gInSc2	slovník
jako	jako	k8xC	jako
akceptovaná	akceptovaný	k2eAgFnSc1d1	akceptovaná
součást	součást	k1gFnSc1	součást
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cool	Coola	k1gFnPc2	Coola
jazz	jazz	k1gInSc1	jazz
===	===	k?	===
</s>
</p>
<p>
<s>
Cool	Cool	k1gInSc1	Cool
jazz	jazz	k1gInSc1	jazz
byl	být	k5eAaImAgInS	být
vyzdvižen	vyzdvihnout	k5eAaPmNgInS	vyzdvihnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
jako	jako	k8xC	jako
výsledek	výsledek	k1gInSc1	výsledek
kombinování	kombinování	k1gNnSc4	kombinování
převážně	převážně	k6eAd1	převážně
stylu	styl	k1gInSc2	styl
bělošských	bělošský	k2eAgMnPc2d1	bělošský
jazzových	jazzový	k2eAgMnPc2d1	jazzový
muzikantů	muzikant	k1gMnPc2	muzikant
a	a	k8xC	a
černošských	černošský	k2eAgMnPc2d1	černošský
beboperů	beboper	k1gMnPc2	beboper
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávky	nahrávka	k1gFnSc2	nahrávka
cool	coola	k1gFnPc2	coola
jazzu	jazz	k1gInSc2	jazz
od	od	k7c2	od
Chet	Cheta	k1gFnPc2	Cheta
Bakera	Bakero	k1gNnSc2	Bakero
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Brubecka	Brubecko	k1gNnSc2	Brubecko
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Evanse	Evanse	k1gFnSc1	Evanse
<g/>
,	,	kIx,	,
Gil	Gil	k1gFnSc1	Gil
Evanse	Evanse	k1gFnSc1	Evanse
<g/>
,	,	kIx,	,
Stan	stan	k1gInSc1	stan
Getze	Getze	k1gFnSc2	Getze
a	a	k8xC	a
Modern	Modern	k1gMnSc1	Modern
Jazz	jazz	k1gInSc4	jazz
Quartetu	Quartet	k1gInSc2	Quartet
měly	mít	k5eAaImAgInP	mít
obvykle	obvykle	k6eAd1	obvykle
odlehčený	odlehčený	k2eAgInSc4d1	odlehčený
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zabránil	zabránit	k5eAaPmAgMnS	zabránit
agresivnímu	agresivní	k2eAgNnSc3d1	agresivní
tempu	tempo	k1gNnSc3	tempo
a	a	k8xC	a
harmonické	harmonický	k2eAgFnSc3d1	harmonická
abstrakci	abstrakce	k1gFnSc3	abstrakce
z	z	k7c2	z
bebopu	bebop	k1gInSc2	bebop
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
nahrávku	nahrávka	k1gFnSc4	nahrávka
"	"	kIx"	"
<g/>
Birth	Birth	k1gInSc1	Birth
of	of	k?	of
the	the	k?	the
Cool	Cool	k1gMnSc1	Cool
<g/>
"	"	kIx"	"
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
trumpeťák	trumpeťák	k1gMnSc1	trumpeťák
Miles	Miles	k1gMnSc1	Miles
Davis	Davis	k1gFnSc1	Davis
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
jako	jako	k9	jako
třeba	třeba	k6eAd1	třeba
pianista	pianista	k1gMnSc1	pianista
Bill	Bill	k1gMnSc1	Bill
Evans	Evans	k1gInSc4	Evans
začali	začít	k5eAaPmAgMnP	začít
hledat	hledat	k5eAaImF	hledat
novou	nový	k2eAgFnSc4d1	nová
cestu	cesta	k1gFnSc4	cesta
ke	k	k7c3	k
struktuře	struktura	k1gFnSc3	struktura
jejich	jejich	k3xOp3gFnSc1	jejich
improvizace	improvizace	k1gFnSc1	improvizace
objevováním	objevování	k1gNnSc7	objevování
formální	formální	k2eAgFnSc2d1	formální
muziky	muzika	k1gFnSc2	muzika
<g/>
.	.	kIx.	.
</s>
<s>
Cool	Cool	k1gInSc1	Cool
jazz	jazz	k1gInSc1	jazz
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
silně	silně	k6eAd1	silně
spojován	spojovat	k5eAaImNgMnS	spojovat
se	s	k7c7	s
scénou	scéna	k1gFnSc7	scéna
West	West	k2eAgInSc1d1	West
Coast	Coast	k1gInSc1	Coast
jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
to	ten	k3xDgNnSc1	ten
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
pozdější	pozdní	k2eAgInSc4d2	pozdější
průběh	průběh	k1gInSc4	průběh
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
pro	pro	k7c4	pro
bossa	boss	k1gMnSc4	boss
nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
modal	modal	k1gInSc1	modal
jazz	jazz	k1gInSc1	jazz
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
free	free	k1gFnSc4	free
jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hard	Harda	k1gFnPc2	Harda
bop	bop	k1gInSc1	bop
===	===	k?	===
</s>
</p>
<p>
<s>
Hard	Hard	k6eAd1	Hard
bop	bop	k1gInSc1	bop
je	být	k5eAaImIp3nS	být
odbočka	odbočka	k1gFnSc1	odbočka
od	od	k7c2	od
bebopu	bebop	k1gInSc2	bebop
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
bop	bop	k1gInSc1	bop
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
začlenily	začlenit	k5eAaPmAgInP	začlenit
vlivy	vliv	k1gInPc1	vliv
z	z	k7c2	z
rhythm	rhythma	k1gFnPc2	rhythma
and	and	k?	and
blues	blues	k1gNnSc4	blues
<g/>
,	,	kIx,	,
gospel	gospel	k1gInSc4	gospel
music	musice	k1gFnPc2	musice
a	a	k8xC	a
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
saxofonové	saxofonový	k2eAgFnSc6d1	saxofonová
a	a	k8xC	a
klavírní	klavírní	k2eAgFnSc6d1	klavírní
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hard	Hard	k1gInSc1	Hard
bop	bop	k1gInSc1	bop
se	se	k3xPyFc4	se
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
uprostřed	uprostřed	k7c2	uprostřed
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
oblibu	obliba	k1gFnSc4	obliba
cool	coola	k1gFnPc2	coola
jazzu	jazz	k1gInSc2	jazz
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Uskupil	uskupit	k5eAaPmAgMnS	uskupit
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1953	[number]	k4	1953
a	a	k8xC	a
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
se	s	k7c7	s
vzrůstem	vzrůst	k1gInSc7	vzrůst
rhythm	rhythm	k1gInSc4	rhythm
and	and	k?	and
blues	blues	k1gNnSc2	blues
<g/>
.	.	kIx.	.
</s>
<s>
Miles	Miles	k1gMnSc1	Miles
Davisovo	Davisův	k2eAgNnSc1d1	Davisovo
dílo	dílo	k1gNnSc1	dílo
"	"	kIx"	"
<g/>
Walkin	Walkin	k2eAgMnSc1d1	Walkin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
ohlašovalo	ohlašovat	k5eAaImAgNnS	ohlašovat
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
pro	pro	k7c4	pro
svět	svět	k1gInSc4	svět
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Kvintet	kvintet	k1gInSc1	kvintet
Art	Art	k1gMnSc2	Art
Blakey	Blakea	k1gMnSc2	Blakea
and	and	k?	and
the	the	k?	the
Jazz	jazz	k1gInSc1	jazz
Messengers	Messengers	k1gInSc1	Messengers
s	s	k7c7	s
kapelníkem	kapelník	k1gMnSc7	kapelník
Blakeym	Blakeym	k1gInSc4	Blakeym
<g/>
,	,	kIx,	,
pianistou	pianista	k1gMnSc7	pianista
Horace	Horace	k1gFnSc2	Horace
Silverem	Silver	k1gMnSc7	Silver
a	a	k8xC	a
trumpetistou	trumpetista	k1gMnSc7	trumpetista
Clifford	Clifford	k1gMnSc1	Clifford
Brownem	Brown	k1gMnSc7	Brown
byly	být	k5eAaImAgFnP	být
vedoucími	vedoucí	k2eAgMnPc7d1	vedoucí
články	článek	k1gInPc4	článek
v	v	k7c6	v
hard	hard	k6eAd1	hard
bopovém	bopový	k2eAgNnSc6d1	bopový
hnutí	hnutí	k1gNnSc6	hnutí
společně	společně	k6eAd1	společně
s	s	k7c7	s
Davisem	Davis	k1gInSc7	Davis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Free	Fre	k1gInPc4	Fre
jazz	jazz	k1gInSc1	jazz
===	===	k?	===
</s>
</p>
<p>
<s>
Free	Free	k6eAd1	Free
jazz	jazz	k1gInSc1	jazz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
podobná	podobný	k2eAgFnSc1d1	podobná
forma	forma	k1gFnSc1	forma
avant-garde	avantard	k1gMnSc5	avant-gard
jazz	jazz	k1gInSc1	jazz
jsou	být	k5eAaImIp3nP	být
podžánry	podžánr	k1gInPc1	podžánr
jazzu	jazz	k1gInSc2	jazz
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
bebopu	bebop	k1gInSc6	bebop
<g/>
,	,	kIx,	,
používající	používající	k2eAgFnPc1d1	používající
méně	málo	k6eAd2	málo
předepsaných	předepsaný	k2eAgInPc2d1	předepsaný
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
hráči	hráč	k1gMnPc1	hráč
více	hodně	k6eAd2	hodně
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Free	Free	k6eAd1	Free
jazz	jazz	k1gInSc1	jazz
používá	používat	k5eAaImIp3nS	používat
implicitní	implicitní	k2eAgInSc1d1	implicitní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
volnou	volný	k2eAgFnSc4d1	volná
harmonii	harmonie	k1gFnSc4	harmonie
a	a	k8xC	a
tempo	tempo	k1gNnSc4	tempo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Basista	basista	k1gMnSc1	basista
Charles	Charles	k1gMnSc1	Charles
Mingus	Mingus	k1gMnSc1	Mingus
je	být	k5eAaImIp3nS	být
také	také	k9	také
často	často	k6eAd1	často
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
průkopem	průkop	k1gInSc7	průkop
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gFnSc2	jeho
aranže	aranže	k1gFnSc2	aranže
prolínalo	prolínat	k5eAaImAgNnS	prolínat
nespočet	nespočet	k1gInSc4	nespočet
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
větší	veliký	k2eAgNnSc1d2	veliký
pohnutí	pohnutí	k1gNnSc1	pohnutí
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
s	s	k7c7	s
ranými	raný	k2eAgFnPc7d1	raná
pracemi	práce	k1gFnPc7	práce
Ornette	Ornett	k1gInSc5	Ornett
Colemana	Coleman	k1gMnSc4	Coleman
a	a	k8xC	a
Cecil	Cecil	k1gMnSc1	Cecil
Taylora	Taylora	k1gFnSc1	Taylora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
John	John	k1gMnSc1	John
Coltrane	Coltran	k1gInSc5	Coltran
<g/>
,	,	kIx,	,
Archie	Archie	k1gFnPc1	Archie
Shepp	Shepp	k1gInSc1	Shepp
<g/>
,	,	kIx,	,
Sun	Sun	kA	Sun
Ra	ra	k0	ra
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Ayler	Ayler	k1gMnSc1	Ayler
<g/>
,	,	kIx,	,
Pharoah	Pharoah	k1gMnSc1	Pharoah
Sanders	Sanders	k1gInSc1	Sanders
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Free	Free	k6eAd1	Free
jazz	jazz	k1gInSc1	jazz
rychle	rychle	k6eAd1	rychle
našel	najít	k5eAaPmAgInS	najít
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Ayler	Ayler	k1gMnSc1	Ayler
<g/>
,	,	kIx,	,
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
Lacy	Laca	k1gFnSc2	Laca
a	a	k8xC	a
Eric	Eric	k1gFnSc1	Eric
Dolphy	Dolpha	k1gFnSc2	Dolpha
<g/>
,	,	kIx,	,
strávili	strávit	k5eAaPmAgMnP	strávit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Latin	latina	k1gFnPc2	latina
jazz	jazz	k1gInSc1	jazz
===	===	k?	===
</s>
</p>
<p>
<s>
Latin	Latin	k1gMnSc1	Latin
jazz	jazz	k1gInSc4	jazz
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
Afro-Cuban	Afro-Cubany	k1gInPc2	Afro-Cubany
jazz	jazz	k1gInSc1	jazz
a	a	k8xC	a
Brazilian	Brazilian	k1gInSc1	Brazilian
jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Afro-Cuban	Afro-Cuban	k1gInSc1	Afro-Cuban
jazz	jazz	k1gInSc1	jazz
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
USA	USA	kA	USA
hrán	hrát	k5eAaImNgInS	hrát
po	po	k7c6	po
éře	éra	k1gFnSc6	éra
bebopu	bebop	k1gInSc2	bebop
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Brazilian	Brazilian	k1gInSc1	Brazilian
jazz	jazz	k1gInSc1	jazz
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
populární	populární	k2eAgFnSc1d1	populární
během	během	k7c2	během
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Afro-Cuban	Afro-Cuban	k1gInSc1	Afro-Cuban
jazz	jazz	k1gInSc1	jazz
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
jako	jako	k9	jako
styl	styl	k1gInSc1	styl
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
uprostřed	uprostřed	k7c2	uprostřed
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
díky	díky	k7c3	díky
muzikantům	muzikant	k1gMnPc3	muzikant
jako	jako	k8xS	jako
Dizzy	Dizza	k1gFnSc2	Dizza
Gillespie	Gillespie	k1gFnSc2	Gillespie
a	a	k8xC	a
Billy	Bill	k1gMnPc7	Bill
Taylor	Taylora	k1gFnPc2	Taylora
<g/>
,	,	kIx,	,
ovlivněnými	ovlivněný	k2eAgFnPc7d1	ovlivněná
mnoha	mnoho	k4c7	mnoho
kubánskými	kubánský	k2eAgMnPc7d1	kubánský
muzikanty	muzikant	k1gMnPc7	muzikant
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Xavier	Xavier	k1gMnSc1	Xavier
Cugat	Cugat	k1gInSc1	Cugat
<g/>
,	,	kIx,	,
Tito	tento	k3xDgMnPc1	tento
Puente	Puent	k1gInSc5	Puent
a	a	k8xC	a
Arturo	Artura	k1gFnSc5	Artura
Sandoval	Sandoval	k1gMnPc1	Sandoval
<g/>
.	.	kIx.	.
</s>
<s>
Brazilian	Brazilian	k1gInSc1	Brazilian
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
bossa	boss	k1gMnSc4	boss
nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
samby	samba	k1gFnSc2	samba
a	a	k8xC	a
přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
prvky	prvek	k1gInPc7	prvek
z	z	k7c2	z
jazzu	jazz	k1gInSc2	jazz
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
klasických	klasický	k2eAgInPc2d1	klasický
a	a	k8xC	a
populárních	populární	k2eAgInPc2d1	populární
stylů	styl	k1gInPc2	styl
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bossa	boss	k1gMnSc4	boss
je	být	k5eAaImIp3nS	být
vesměs	vesměs	k6eAd1	vesměs
nepříliš	příliš	k6eNd1	příliš
rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
,	,	kIx,	,
s	s	k7c7	s
melodií	melodie	k1gFnSc7	melodie
zpívanou	zpívaný	k2eAgFnSc7d1	zpívaná
v	v	k7c6	v
portugalštině	portugalština	k1gFnSc6	portugalština
nebo	nebo	k8xC	nebo
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníky	průkopník	k1gMnPc4	průkopník
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
byli	být	k5eAaImAgMnP	být
Brazilci	Brazilec	k1gMnPc1	Brazilec
Joao	Joao	k6eAd1	Joao
Gilberto	Gilberta	k1gFnSc5	Gilberta
<g/>
,	,	kIx,	,
Antônio	Antônio	k1gMnSc1	Antônio
Carlos	Carlos	k1gMnSc1	Carlos
Jobim	Jobim	k1gMnSc1	Jobim
<g/>
,	,	kIx,	,
Vinícius	Vinícius	k1gMnSc1	Vinícius
de	de	k?	de
Moraes	Moraes	k1gMnSc1	Moraes
a	a	k8xC	a
spousta	spousta	k1gFnSc1	spousta
dalších	další	k2eAgNnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzným	příbuzný	k2eAgInSc7d1	příbuzný
stylem	styl	k1gInSc7	styl
je	být	k5eAaImIp3nS	být
jazz-samba	jazzamba	k1gFnSc1	jazz-samba
popisovaná	popisovaný	k2eAgFnSc1d1	popisovaná
jako	jako	k8xS	jako
kompozice	kompozice	k1gFnSc1	kompozice
bossa	boss	k1gMnSc2	boss
novy	nova	k1gFnSc2	nova
vložená	vložený	k2eAgFnSc1d1	vložená
do	do	k7c2	do
jazzových	jazzový	k2eAgFnPc2d1	jazzová
frází	fráze	k1gFnPc2	fráze
interpretovaná	interpretovaný	k2eAgFnSc1d1	interpretovaná
například	například	k6eAd1	například
Stan	stan	k1gInSc1	stan
Getzem	Getz	k1gInSc7	Getz
a	a	k8xC	a
Charlie	Charlie	k1gMnSc1	Charlie
Byrdem	Byrd	k1gMnSc7	Byrd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soul	Soul	k1gInSc1	Soul
jazz	jazz	k1gInSc1	jazz
===	===	k?	===
</s>
</p>
<p>
<s>
Soul	Soul	k1gInSc1	Soul
jazz	jazz	k1gInSc1	jazz
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
vývojem	vývoj	k1gInSc7	vývoj
hard	hard	k1gInSc4	hard
bopu	bop	k1gInSc2	bop
<g/>
,	,	kIx,	,
mísil	mísit	k5eAaImAgInS	mísit
silné	silný	k2eAgInPc4d1	silný
vlivy	vliv	k1gInPc4	vliv
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
gospelu	gospel	k1gInSc2	gospel
a	a	k8xC	a
rhythm	rhythm	k1gInSc4	rhythm
and	and	k?	and
blues	blues	k1gNnSc2	blues
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
organ	organon	k1gNnPc2	organon
tria	trio	k1gNnSc2	trio
používajícím	používající	k2eAgFnPc3d1	používající
Hammondových	Hammondový	k2eAgMnPc2d1	Hammondový
varhanů	varhany	k1gInPc2	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hard	harda	k1gFnPc2	harda
bopu	bop	k1gInSc2	bop
<g/>
,	,	kIx,	,
soul	soul	k1gInSc1	soul
jazz	jazz	k1gInSc1	jazz
vesměs	vesměs	k6eAd1	vesměs
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
opakující	opakující	k2eAgMnSc1d1	opakující
se	se	k3xPyFc4	se
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
melodické	melodický	k2eAgFnSc2d1	melodická
oraty	orata	k1gFnSc2	orata
<g/>
,	,	kIx,	,
a	a	k8xC	a
improvizace	improvizace	k1gFnSc1	improvizace
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
méně	málo	k6eAd2	málo
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Horace	Horace	k1gFnSc1	Horace
Silver	Silvra	k1gFnPc2	Silvra
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
soul	soul	k1gInSc1	soul
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
písničkách	písnička	k1gFnPc6	písnička
používá	používat	k5eAaImIp3nS	používat
funky	funk	k1gInPc7	funk
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
improvizaci	improvizace	k1gFnSc4	improvizace
kostelních	kostelní	k2eAgFnPc2d1	kostelní
varhan	varhany	k1gFnPc2	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgInPc4d1	známý
soul	soul	k1gInSc4	soul
jazzové	jazzový	k2eAgMnPc4d1	jazzový
varhaníky	varhaník	k1gMnPc4	varhaník
patří	patřit	k5eAaImIp3nP	patřit
Jimmy	Jimma	k1gFnPc1	Jimma
McGriff	McGriff	k1gInSc1	McGriff
<g/>
,	,	kIx,	,
Jimmy	Jimm	k1gInPc1	Jimm
Smith	Smitha	k1gFnPc2	Smitha
a	a	k8xC	a
Johnny	Johnna	k1gFnSc2	Johnna
Hammond	Hammond	k1gMnSc1	Hammond
Smith	Smith	k1gMnSc1	Smith
a	a	k8xC	a
mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
tenor	tenor	k1gInSc4	tenor
saxofonisty	saxofonista	k1gMnSc2	saxofonista
patří	patřit	k5eAaImIp3nP	patřit
třeba	třeba	k9	třeba
Eddie	Eddie	k1gFnPc1	Eddie
"	"	kIx"	"
<g/>
Lockjaw	Lockjaw	k1gFnPc1	Lockjaw
<g/>
"	"	kIx"	"
Davis	Davis	k1gFnPc1	Davis
a	a	k8xC	a
Stanley	Stanlea	k1gFnPc1	Stanlea
Turrentine	Turrentin	k1gMnSc5	Turrentin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazz	jazz	k1gInSc1	jazz
fusion	fusion	k1gInSc1	fusion
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
tvořit	tvořit	k5eAaImF	tvořit
hybridní	hybridní	k2eAgFnSc1d1	hybridní
forma	forma	k1gFnSc1	forma
jazz	jazz	k1gInSc1	jazz
rocku	rock	k1gInSc2	rock
<g/>
:	:	kIx,	:
jazz	jazz	k1gInSc1	jazz
fusion	fusion	k1gInSc1	fusion
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jazzoví	jazzový	k2eAgMnPc1d1	jazzový
puristé	purista	k1gMnPc1	purista
bránili	bránit	k5eAaImAgMnP	bránit
proti	proti	k7c3	proti
mixování	mixování	k1gNnSc3	mixování
jazzu	jazz	k1gInSc2	jazz
a	a	k8xC	a
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
důležitější	důležitý	k2eAgMnPc1d2	důležitější
jazzoví	jazzový	k2eAgMnPc1d1	jazzový
inovátoři	inovátor	k1gMnPc1	inovátor
přeskočili	přeskočit	k5eAaPmAgMnP	přeskočit
od	od	k7c2	od
dobové	dobový	k2eAgInPc4d1	dobový
hardbop	hardbop	k1gInSc4	hardbop
scény	scéna	k1gFnSc2	scéna
k	k	k7c3	k
fusion	fusion	k1gInSc4	fusion
<g/>
.	.	kIx.	.
</s>
<s>
Jazz	jazz	k1gInSc1	jazz
fusion	fusion	k1gInSc1	fusion
často	často	k6eAd1	často
mísí	mísit	k5eAaImIp3nS	mísit
rytmus	rytmus	k1gInSc1	rytmus
<g/>
,	,	kIx,	,
neobvyklé	obvyklý	k2eNgInPc1d1	neobvyklý
takty	takt	k1gInPc1	takt
<g/>
,	,	kIx,	,
synkopy	synkopa	k1gFnPc1	synkopa
a	a	k8xC	a
složité	složitý	k2eAgInPc1d1	složitý
akordy	akord	k1gInPc1	akord
a	a	k8xC	a
harmonie	harmonie	k1gFnPc1	harmonie
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgInSc1d1	využívající
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
elektrických	elektrický	k2eAgInPc2d1	elektrický
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
elektrická	elektrický	k2eAgFnSc1d1	elektrická
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgNnSc1d1	elektrické
piano	piano	k1gNnSc1	piano
a	a	k8xC	a
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Vynikajícími	vynikající	k2eAgMnPc7d1	vynikající
hráči	hráč	k1gMnPc7	hráč
jazz	jazz	k1gInSc4	jazz
fusion	fusion	k1gInSc4	fusion
byli	být	k5eAaImAgMnP	být
Miles	Miles	k1gInSc4	Miles
Davis	Davis	k1gFnSc2	Davis
<g/>
,	,	kIx,	,
keyboardista	keyboardista	k1gMnSc1	keyboardista
Chick	Chick	k1gMnSc1	Chick
Corea	Corea	k1gMnSc1	Corea
a	a	k8xC	a
Herbie	Herbie	k1gFnSc1	Herbie
Hancock	Hancock	k1gInSc1	Hancock
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Tony	Tony	k1gFnSc2	Tony
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Larry	Larra	k1gFnSc2	Larra
Coryell	Coryell	k1gMnSc1	Coryell
a	a	k8xC	a
John	John	k1gMnSc1	John
McLaughlin	McLaughlina	k1gFnPc2	McLaughlina
<g/>
,	,	kIx,	,
saxofonista	saxofonista	k1gMnSc1	saxofonista
Wayne	Wayn	k1gInSc5	Wayn
Shorter	Shorter	k1gMnSc1	Shorter
a	a	k8xC	a
baskytarový	baskytarový	k2eAgMnSc1d1	baskytarový
skladatel	skladatel	k1gMnSc1	skladatel
Jaco	Jaco	k1gMnSc1	Jaco
Pastorius	Pastorius	k1gMnSc1	Pastorius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Trendy	trend	k1gInPc7	trend
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nastala	nastat	k5eAaPmAgFnS	nastat
obroda	obroda	k1gFnSc1	obroda
zajímavostí	zajímavost	k1gFnPc2	zajímavost
v	v	k7c6	v
jazzu	jazz	k1gInSc6	jazz
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
formách	forma	k1gFnPc6	forma
afroamerické	afroamerický	k2eAgFnSc2d1	afroamerická
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Muzikanti	muzikant	k1gMnPc1	muzikant
jako	jako	k8xC	jako
Pharoah	Pharoah	k1gInSc1	Pharoah
Sanders	Sandersa	k1gFnPc2	Sandersa
<g/>
,	,	kIx,	,
Hubert	Hubert	k1gMnSc1	Hubert
Laws	Laws	k1gInSc1	Laws
a	a	k8xC	a
Wayne	Wayn	k1gInSc5	Wayn
Shorter	Shorter	k1gInSc4	Shorter
začali	začít	k5eAaPmAgMnP	začít
používat	používat	k5eAaImF	používat
kalimbo	kalimba	k1gFnSc5	kalimba
<g/>
,	,	kIx,	,
cowbell	cowbell	k1gMnSc1	cowbell
<g/>
,	,	kIx,	,
shekeire	shekeir	k1gInSc5	shekeir
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
jazz	jazz	k1gInSc4	jazz
ne	ne	k9	ne
zrovna	zrovna	k6eAd1	zrovna
typické	typický	k2eAgNnSc1d1	typické
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
jazzová	jazzový	k2eAgFnSc1d1	jazzová
harfistka	harfistka	k1gFnSc1	harfistka
Alice	Alice	k1gFnSc2	Alice
Coltrane	Coltran	k1gInSc5	Coltran
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgMnSc1d1	jazzový
houslista	houslista	k1gMnSc1	houslista
Jean-Luc	Jean-Luc	k1gFnSc4	Jean-Luc
Ponty	Pont	k1gInPc4	Pont
a	a	k8xC	a
jazzový	jazzový	k2eAgInSc4d1	jazzový
dudák	dudák	k1gInSc4	dudák
Rufus	Rufus	k1gInSc1	Rufus
Harley	harley	k1gInSc1	harley
<g/>
.	.	kIx.	.
</s>
<s>
Jazz	jazz	k1gInSc1	jazz
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
bral	brát	k5eAaImAgMnS	brát
inspiraci	inspirace	k1gFnSc4	inspirace
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
druzích	druh	k1gInPc6	druh
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
world	world	k1gMnSc1	world
music	music	k1gMnSc1	music
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgFnSc1d1	experimentální
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
rock	rock	k1gInSc1	rock
a	a	k8xC	a
pop	pop	k1gMnSc1	pop
music	music	k1gMnSc1	music
<g/>
.	.	kIx.	.
</s>
<s>
Kytarista	kytarista	k1gMnSc1	kytarista
John	John	k1gMnSc1	John
McLaughlin	McLaughlina	k1gFnPc2	McLaughlina
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
Mahavishnu	Mahavishnu	k1gFnSc1	Mahavishnu
Orchestra	orchestra	k1gFnSc1	orchestra
hrála	hrát	k5eAaImAgFnS	hrát
kombinaci	kombinace	k1gFnSc4	kombinace
jazzu	jazz	k1gInSc2	jazz
a	a	k8xC	a
rocku	rock	k1gInSc2	rock
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
muziky	muzika	k1gFnSc2	muzika
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nahrávací	nahrávací	k2eAgFnSc6d1	nahrávací
společnosti	společnost	k1gFnSc6	společnost
ECM	ECM	kA	ECM
natočilo	natočit	k5eAaBmAgNnS	natočit
desky	deska	k1gFnSc2	deska
mnoho	mnoho	k6eAd1	mnoho
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Keith	Keith	k1gMnSc1	Keith
Jarrett	Jarrett	k1gMnSc1	Jarrett
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Bley	Blea	k1gFnSc2	Blea
<g/>
,	,	kIx,	,
The	The	k1gFnSc2	The
Pat	pata	k1gFnPc2	pata
Metheny	Methena	k1gFnSc2	Methena
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Garbarek	Garbarka	k1gFnPc2	Garbarka
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gMnSc1	Ralph
Towner	Towner	k1gMnSc1	Towner
a	a	k8xC	a
Eberhard	Eberhard	k1gMnSc1	Eberhard
Weber	Weber	k1gMnSc1	Weber
<g/>
.	.	kIx.	.
</s>
<s>
Otevřeli	otevřít	k5eAaPmAgMnP	otevřít
tím	ten	k3xDgNnSc7	ten
dveře	dveře	k1gFnPc4	dveře
nové	nový	k2eAgFnSc3d1	nová
estetické	estetický	k2eAgFnSc3d1	estetická
muzice	muzika	k1gFnSc3	muzika
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgMnSc1d1	využívající
převážně	převážně	k6eAd1	převážně
akustické	akustický	k2eAgInPc4d1	akustický
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
smísily	smísit	k5eAaPmAgFnP	smísit
je	on	k3xPp3gNnSc4	on
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
ze	z	k7c2	z
světové	světový	k2eAgFnSc2d1	světová
a	a	k8xC	a
folkové	folkový	k2eAgFnSc2d1	folková
muziky	muzika	k1gFnSc2	muzika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jazzová	jazzový	k2eAgFnSc1d1	jazzová
komunita	komunita	k1gFnSc1	komunita
ztenčila	ztenčit	k5eAaPmAgFnS	ztenčit
a	a	k8xC	a
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
starší	starý	k2eAgNnSc1d2	starší
posluchačstvo	posluchačstvo	k1gNnSc1	posluchačstvo
udržovalo	udržovat	k5eAaImAgNnS	udržovat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
tradiční	tradiční	k2eAgFnPc4d1	tradiční
a	a	k8xC	a
přímé	přímý	k2eAgFnPc4d1	přímá
formy	forma	k1gFnPc4	forma
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Wynton	Wynton	k1gInSc1	Wynton
Marsalis	Marsalis	k1gFnSc2	Marsalis
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
tvořit	tvořit	k5eAaImF	tvořit
hudbu	hudba	k1gFnSc4	hudba
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
víře	víra	k1gFnSc6	víra
v	v	k7c4	v
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
inspiraci	inspirace	k1gFnSc4	inspirace
hledal	hledat	k5eAaImAgMnS	hledat
u	u	k7c2	u
Louise	Louis	k1gMnSc2	Louis
Armstronga	Armstrong	k1gMnSc2	Armstrong
a	a	k8xC	a
Duke	Duk	k1gMnSc2	Duk
Ellingtona	Ellington	k1gMnSc2	Ellington
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
lehce	lehko	k6eAd1	lehko
komerční	komerční	k2eAgFnSc1d1	komerční
forma	forma	k1gFnSc1	forma
jazz	jazz	k1gInSc1	jazz
fusion	fusion	k1gInSc1	fusion
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
pop	pop	k1gMnSc1	pop
fusion	fusion	k1gInSc1	fusion
nebo	nebo	k8xC	nebo
smooth	smooth	k1gInSc1	smooth
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
důležitějších	důležitý	k2eAgNnPc6d2	důležitější
rádiích	rádio	k1gNnPc6	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Význační	význačný	k2eAgMnPc1d1	význačný
smooth	smooth	k1gMnSc1	smooth
jazz	jazz	k1gInSc4	jazz
saxofonisté	saxofonista	k1gMnPc1	saxofonista
byli	být	k5eAaImAgMnP	být
Grover	Grover	k1gInSc4	Grover
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
Jr	Jr	k1gFnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Kenny	Kenn	k1gInPc1	Kenn
G	G	kA	G
a	a	k8xC	a
Najee	Naje	k1gFnSc2	Naje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
začátkem	začátek	k1gInSc7	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
podžánry	podžánra	k1gFnPc1	podžánra
spojily	spojit	k5eAaPmAgFnP	spojit
jazz	jazz	k1gInSc4	jazz
s	s	k7c7	s
populární	populární	k2eAgFnSc7d1	populární
muzikou	muzika	k1gFnSc7	muzika
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
acid	acid	k1gInSc1	acid
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
nu	nu	k9	nu
jazz	jazz	k1gInSc1	jazz
a	a	k8xC	a
jazz	jazz	k1gInSc1	jazz
rap	rapa	k1gFnPc2	rapa
<g/>
.	.	kIx.	.
</s>
<s>
Acid	Acid	k6eAd1	Acid
jazz	jazz	k1gInSc1	jazz
a	a	k8xC	a
nu	nu	k9	nu
jazz	jazz	k1gInSc1	jazz
kombinoval	kombinovat	k5eAaImAgInS	kombinovat
elementy	element	k1gInPc4	element
jazzu	jazz	k1gInSc2	jazz
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
formou	forma	k1gFnSc7	forma
elektrické	elektrický	k2eAgFnSc2d1	elektrická
taneční	taneční	k2eAgFnSc2d1	taneční
muziky	muzika	k1gFnSc2	muzika
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
nu	nu	k9	nu
jazz	jazz	k1gInSc1	jazz
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
jazzové	jazzový	k2eAgFnSc6d1	jazzová
harmonii	harmonie	k1gFnSc6	harmonie
a	a	k8xC	a
melodii	melodie	k1gFnSc6	melodie
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nenajdeme	najít	k5eNaPmIp1nP	najít
improvizaci	improvizace	k1gFnSc4	improvizace
<g/>
.	.	kIx.	.
</s>
<s>
Jazz	jazz	k1gInSc1	jazz
rap	rapa	k1gFnPc2	rapa
spojil	spojit	k5eAaPmAgInS	spojit
jazz	jazz	k1gInSc1	jazz
a	a	k8xC	a
hip	hip	k0	hip
hop	hop	k0	hop
<g/>
.	.	kIx.	.
</s>
<s>
Gang	gang	k1gInSc1	gang
Starr	Starr	k1gMnSc1	Starr
nahráli	nahrát	k5eAaBmAgMnP	nahrát
"	"	kIx"	"
<g/>
Words	Words	k1gInSc1	Words
I	i	k8xC	i
Manifest	manifest	k1gInSc1	manifest
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jazz	jazz	k1gInSc1	jazz
Music	Musice	k1gFnPc2	Musice
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jazz	jazz	k1gInSc1	jazz
Thing	Thing	k1gInSc1	Thing
<g/>
"	"	kIx"	"
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Branford	Branford	k1gInSc1	Branford
Marsalisem	Marsalis	k1gInSc7	Marsalis
a	a	k8xC	a
Terence	Terenec	k1gInSc2	Terenec
Blanchardem	Blanchard	k1gMnSc7	Blanchard
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
raper	raper	k1gMnSc1	raper
Guru	guru	k1gMnSc1	guru
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
Jazzmatazz	Jazzmatazz	k1gInSc4	Jazzmatazz
používali	používat	k5eAaImAgMnP	používat
jazzové	jazzový	k2eAgMnPc4d1	jazzový
muzikanty	muzikant	k1gMnPc4	muzikant
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
studiovým	studiový	k2eAgFnPc3d1	studiová
nahrávkám	nahrávka	k1gFnPc3	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Radiohead	Radiohead	k1gInSc1	Radiohead
<g/>
,	,	kIx,	,
Björk	Björk	k1gInSc1	Björk
a	a	k8xC	a
Portishead	Portishead	k1gInSc1	Portishead
také	také	k9	také
často	často	k6eAd1	často
vkládali	vkládat	k5eAaImAgMnP	vkládat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
muziky	muzika	k1gFnSc2	muzika
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
"	"	kIx"	"
<g/>
přímý	přímý	k2eAgInSc1d1	přímý
<g/>
"	"	kIx"	"
jazz	jazz	k1gInSc1	jazz
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
dovolávání	dovolávání	k1gNnSc6	dovolávání
se	se	k3xPyFc4	se
na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Osvědčení	osvědčený	k2eAgMnPc1d1	osvědčený
jazzoví	jazzový	k2eAgMnPc1d1	jazzový
muzikanti	muzikant	k1gMnPc1	muzikant
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
kariéra	kariéra	k1gFnSc1	kariéra
trvala	trvat	k5eAaImAgFnS	trvat
desetiletí	desetiletí	k1gNnSc4	desetiletí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Chick	Chick	k1gMnSc1	Chick
Corea	Corea	k1gMnSc1	Corea
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
DeJohnette	DeJohnett	k1gInSc5	DeJohnett
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Frisell	Frisell	k1gMnSc1	Frisell
<g/>
,	,	kIx,	,
Charlie	Charlie	k1gMnSc1	Charlie
Haden	Haden	k1gInSc1	Haden
<g/>
,	,	kIx,	,
Herbie	Herbie	k1gFnSc1	Herbie
Hancock	Hancock	k1gMnSc1	Hancock
<g/>
,	,	kIx,	,
Roy	Roy	k1gMnSc1	Roy
Haynes	Haynes	k1gMnSc1	Haynes
<g/>
,	,	kIx,	,
Keith	Keith	k1gMnSc1	Keith
Jarrett	Jarrett	k1gMnSc1	Jarrett
<g/>
,	,	kIx,	,
Wynton	Wynton	k1gInSc1	Wynton
Marsalis	Marsalis	k1gFnSc2	Marsalis
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
McLaughlin	McLaughlina	k1gFnPc2	McLaughlina
<g/>
,	,	kIx,	,
Pat	pata	k1gFnPc2	pata
Metheny	Methena	k1gFnSc2	Methena
<g/>
,	,	kIx,	,
Paquito	Paquit	k2eAgNnSc1d1	Paquito
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Rivera	River	k1gMnSc4	River
<g/>
,	,	kIx,	,
Sonny	Sonno	k1gNnPc7	Sonno
Rollins	Rollins	k1gInSc1	Rollins
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Scofield	Scofield	k1gMnSc1	Scofield
<g/>
,	,	kIx,	,
Wayne	Wayn	k1gInSc5	Wayn
Shorter	Shorter	k1gMnSc1	Shorter
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Surman	Surman	k1gMnSc1	Surman
<g/>
,	,	kIx,	,
Stan	stan	k1gInSc1	stan
Tracey	Tracea	k1gFnSc2	Tracea
a	a	k8xC	a
Jessica	Jessicum	k1gNnSc2	Jessicum
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
,	,	kIx,	,
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c4	v
nahrávání	nahrávání	k1gNnSc4	nahrávání
a	a	k8xC	a
vystupování	vystupování	k1gNnSc4	vystupování
<g/>
.	.	kIx.	.
</s>
<s>
Našlo	najít	k5eAaPmAgNnS	najít
se	se	k3xPyFc4	se
i	i	k9	i
pár	pár	k4xCyI	pár
novátorských	novátorský	k2eAgMnPc2d1	novátorský
jazzových	jazzový	k2eAgMnPc2d1	jazzový
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c6	na
popředí	popředí	k1gNnSc6	popředí
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc4	ten
třeba	třeba	k6eAd1	třeba
The	The	k1gMnSc1	The
Bad	Bad	k1gFnSc2	Bad
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
Brad	brada	k1gFnPc2	brada
Mehldau	Mehldaus	k1gInSc2	Mehldaus
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Glasper	Glasper	k1gMnSc1	Glasper
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
Blade	Blad	k1gInSc5	Blad
<g/>
,	,	kIx,	,
Larry	Larro	k1gNnPc7	Larro
Goldings	Goldings	k1gInSc1	Goldings
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Rosenwinkel	Rosenwinkel	k1gMnSc1	Rosenwinkel
<g/>
,	,	kIx,	,
Gonzalo	Gonzalo	k1gMnSc1	Gonzalo
Rubalcaba	Rubalcaba	k1gMnSc1	Rubalcaba
a	a	k8xC	a
Medeski	Medesk	k1gMnPc1	Medesk
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
&	&	k?	&
Wood	Wood	k1gMnSc1	Wood
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
jazz	jazz	k1gInSc1	jazz
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
pestrou	pestrý	k2eAgFnSc4d1	pestrá
škálu	škála	k1gFnSc4	škála
různých	různý	k2eAgInPc2d1	různý
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
jednotná	jednotný	k2eAgFnSc1d1	jednotná
definice	definice	k1gFnSc1	definice
sjednocující	sjednocující	k2eAgInPc1d1	sjednocující
všechny	všechen	k3xTgInPc1	všechen
styly	styl	k1gInPc1	styl
by	by	kYmCp3nP	by
byla	být	k5eAaImAgFnS	být
zavádějící	zavádějící	k2eAgFnSc1d1	zavádějící
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
fanatikové	fanatik	k1gMnPc1	fanatik
si	se	k3xPyFc3	se
stojí	stát	k5eAaImIp3nP	stát
za	za	k7c7	za
jasnými	jasný	k2eAgFnPc7d1	jasná
definicemi	definice	k1gFnPc7	definice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
z	z	k7c2	z
pojmu	pojem	k1gInSc2	pojem
jazz	jazz	k1gInSc1	jazz
vyjímají	vyjímat	k5eAaImIp3nP	vyjímat
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
hudby	hudba	k1gFnSc2	hudba
od	od	k7c2	od
jazzu	jazz	k1gInSc2	jazz
pouze	pouze	k6eAd1	pouze
odvozené	odvozený	k2eAgNnSc1d1	odvozené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hudebníci	hudebník	k1gMnPc1	hudebník
samotní	samotný	k2eAgMnPc1d1	samotný
odmítají	odmítat	k5eAaImIp3nP	odmítat
definovat	definovat	k5eAaBmF	definovat
hudbu	hudba	k1gFnSc4	hudba
kterou	který	k3yIgFnSc4	který
sami	sám	k3xTgMnPc1	sám
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Duke	Duke	k6eAd1	Duke
Ellington	Ellington	k1gInSc1	Ellington
pojem	pojem	k1gInSc1	pojem
shrnul	shrnout	k5eAaPmAgMnS	shrnout
větou	věta	k1gFnSc7	věta
"	"	kIx"	"
<g/>
Všechno	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hudba	hudba	k1gFnSc1	hudba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kritikové	kritik	k1gMnPc1	kritik
dokonce	dokonce	k9	dokonce
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
Ellingtovona	Ellingtovona	k1gFnSc1	Ellingtovona
hudba	hudba	k1gFnSc1	hudba
nebyla	být	k5eNaImAgFnS	být
jazzem	jazz	k1gInSc7	jazz
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
čistý	čistý	k2eAgInSc1d1	čistý
jazz	jazz	k1gInSc1	jazz
nikdy	nikdy	k6eAd1	nikdy
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
orchestrální	orchestrální	k2eAgFnSc6d1	orchestrální
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
transformace	transformace	k1gFnSc1	transformace
<g/>
"	"	kIx"	"
Ellingtonových	Ellingtonový	k2eAgFnPc2d1	Ellingtonový
nahrávek	nahrávka	k1gFnPc2	nahrávka
v	v	k7c4	v
podání	podání	k1gNnSc4	podání
jeho	on	k3xPp3gMnSc2	on
přítele	přítel	k1gMnSc2	přítel
Earla	earl	k1gMnSc2	earl
Hinese	Hinese	k1gFnSc2	Hinese
(	(	kIx(	(
<g/>
na	na	k7c6	na
albu	album	k1gNnSc6	album
Earl	earl	k1gMnSc1	earl
Hines	Hines	k1gMnSc1	Hines
Plays	Playsa	k1gFnPc2	Playsa
Duke	Duke	k1gFnPc2	Duke
Ellington	Ellington	k1gInSc1	Ellington
<g/>
,	,	kIx,	,
vydaném	vydaný	k2eAgInSc6d1	vydaný
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
hudebním	hudební	k2eAgMnSc7d1	hudební
kritikem	kritik	k1gMnSc7	kritik
Benem	Ben	k1gInSc7	Ben
Ratiffem	Ratiff	k1gInSc7	Ratiff
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
stejně	stejně	k6eAd1	stejně
dobrý	dobrý	k2eAgInSc1d1	dobrý
příklad	příklad	k1gInSc1	příklad
jazzu	jazz	k1gInSc2	jazz
jako	jako	k9	jako
cokoliv	cokoliv	k3yInSc4	cokoliv
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
debatovalo	debatovat	k5eAaImAgNnS	debatovat
o	o	k7c4	o
definici	definice	k1gFnSc4	definice
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
milovníci	milovník	k1gMnPc1	milovník
jazzu	jazz	k1gInSc2	jazz
inovace	inovace	k1gFnSc2	inovace
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
érou	éra	k1gFnSc7	éra
swingu	swing	k1gInSc2	swing
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
odporovaly	odporovat	k5eAaImAgInP	odporovat
improvizačnímu	improvizační	k2eAgInSc3d1	improvizační
stylu	styl	k1gInSc3	styl
spojenému	spojený	k2eAgInSc3d1	spojený
s	s	k7c7	s
"	"	kIx"	"
<g/>
čistým	čistý	k2eAgInSc7d1	čistý
jazzem	jazz	k1gInSc7	jazz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
40	[number]	k4	40
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
spolu	spolu	k6eAd1	spolu
vedli	vést	k5eAaImAgMnP	vést
spory	spor	k1gInPc4	spor
hráči	hráč	k1gMnPc1	hráč
tradičního	tradiční	k2eAgInSc2d1	tradiční
jazzu	jazz	k1gInSc2	jazz
a	a	k8xC	a
bebopeři	beboper	k1gMnPc1	beboper
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
navzájem	navzájem	k6eAd1	navzájem
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	jíst	k5eAaImIp3nS	jíst
k	k	k7c3	k
jazzu	jazz	k1gInSc3	jazz
"	"	kIx"	"
<g/>
něco	něco	k3yInSc1	něco
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
změny	změna	k1gFnPc1	změna
a	a	k8xC	a
transformace	transformace	k1gFnSc1	transformace
jazzu	jazz	k1gInSc2	jazz
pod	pod	k7c7	pod
novými	nový	k2eAgInPc7d1	nový
vlivy	vliv	k1gInPc7	vliv
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
označovány	označovat	k5eAaImNgFnP	označovat
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
znehodnocování	znehodnocování	k1gNnSc4	znehodnocování
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
Gilbert	Gilbert	k1gMnSc1	Gilbert
oponuje	oponovat	k5eAaImIp3nS	oponovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazz	jazz	k1gInSc1	jazz
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
schopnost	schopnost	k1gFnSc4	schopnost
absorbovat	absorbovat	k5eAaBmF	absorbovat
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
vlivy	vliv	k1gInPc4	vliv
<g/>
"	"	kIx"	"
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
různorodých	různorodý	k2eAgInPc2d1	různorodý
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouho	dlouho	k6eAd1	dlouho
byly	být	k5eAaImAgFnP	být
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
formy	forma	k1gFnPc1	forma
jazzu	jazz	k1gInSc2	jazz
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
komerční	komerční	k2eAgNnSc1d1	komerční
populární	populární	k2eAgFnSc7d1	populární
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
bouřili	bouřit	k5eAaImAgMnP	bouřit
hlavně	hlavně	k9	hlavně
bebopeři	beboper	k1gMnPc1	beboper
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
tradičního	tradiční	k2eAgInSc2d1	tradiční
jazzu	jazz	k1gInSc2	jazz
naopak	naopak	k6eAd1	naopak
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
bebop	bebop	k1gInSc4	bebop
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Fusion	Fusion	k1gInSc1	Fusion
era	era	k?	era
<g/>
"	"	kIx"	"
probíhající	probíhající	k2eAgFnSc2d1	probíhající
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
inovace	inovace	k1gFnSc1	inovace
<g/>
,	,	kIx,	,
označujíc	označovat	k5eAaImSgFnS	označovat
je	on	k3xPp3gMnPc4	on
za	za	k7c4	za
znehodnocení	znehodnocení	k1gNnSc4	znehodnocení
jazzové	jazzový	k2eAgFnSc2d1	jazzová
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bruce	Bruce	k1gMnSc2	Bruce
Johnsona	Johnson	k1gMnSc2	Johnson
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
jazz	jazz	k1gInSc1	jazz
odjakživa	odjakživa	k6eAd1	odjakživa
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
mezi	mezi	k7c7	mezi
komerční	komerční	k2eAgFnSc7d1	komerční
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
uměním	umění	k1gNnSc7	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gilbert	Gilbert	k1gMnSc1	Gilbert
podotýká	podotýkat	k5eAaImIp3nS	podotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pojetí	pojetí	k1gNnSc1	pojetí
základních	základní	k2eAgFnPc2d1	základní
zásad	zásada	k1gFnPc2	zásada
jazzu	jazz	k1gInSc2	jazz
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
"	"	kIx"	"
<g/>
plody	plod	k1gInPc1	plod
minulosti	minulost	k1gFnSc2	minulost
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaImF	stát
"	"	kIx"	"
<g/>
nadřazenými	nadřazený	k2eAgNnPc7d1	nadřazené
výstřední	výstřední	k2eAgFnPc1d1	výstřední
kreativitě	kreativita	k1gFnSc3	kreativita
<g/>
"	"	kIx"	"
a	a	k8xC	a
inovacím	inovace	k1gFnPc3	inovace
dnešních	dnešní	k2eAgMnPc2d1	dnešní
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Jazzový	jazzový	k2eAgMnSc1d1	jazzový
kritik	kritik	k1gMnSc1	kritik
Gary	Gara	k1gFnSc2	Gara
Giddins	Giddins	k1gInSc1	Giddins
namítá	namítat	k5eAaImIp3nS	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šíření	šíření	k1gNnSc1	šíření
jazzu	jazz	k1gInSc2	jazz
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
příliš	příliš	k6eAd1	příliš
institucionalizované	institucionalizovaný	k2eAgInPc1d1	institucionalizovaný
a	a	k8xC	a
kontrolované	kontrolovaný	k2eAgInPc1d1	kontrolovaný
velkými	velký	k2eAgFnPc7d1	velká
firmami	firma	k1gFnPc7	firma
hudebního	hudební	k2eAgInSc2d1	hudební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
jazz	jazz	k1gInSc1	jazz
čelí	čelit	k5eAaImIp3nS	čelit
"	"	kIx"	"
<g/>
těžkým	těžký	k2eAgInPc3d1	těžký
časům	čas	k1gInPc3	čas
respektovanosti	respektovanost	k1gFnSc2	respektovanost
a	a	k8xC	a
přijetí	přijetí	k1gNnSc4	přijetí
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
zájmu	zájem	k1gInSc2	zájem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Ake	Ake	k1gMnSc1	Ake
varuje	varovat	k5eAaImIp3nS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytvoření	vytvoření	k1gNnSc1	vytvoření
norem	norma	k1gFnPc2	norma
pro	pro	k7c4	pro
jazz	jazz	k1gInSc4	jazz
bude	být	k5eAaImBp3nS	být
znamenat	znamenat	k5eAaImF	znamenat
vytlačení	vytlačení	k1gNnSc4	vytlačení
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
tvorby	tvorba	k1gFnSc2	tvorba
většiny	většina	k1gFnSc2	většina
nových	nový	k2eAgFnPc2d1	nová
<g/>
,	,	kIx,	,
avantgardních	avantgardní	k2eAgFnPc2d1	avantgardní
forem	forma	k1gFnPc2	forma
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
jak	jak	k8xS	jak
obejít	obejít	k5eAaPmF	obejít
defininí	defininit	k5eAaPmIp3nS	defininit
problémy	problém	k1gInPc4	problém
je	být	k5eAaImIp3nS	být
definovat	definovat	k5eAaBmF	definovat
pojem	pojem	k1gInSc4	pojem
jazz	jazz	k1gInSc4	jazz
více	hodně	k6eAd2	hodně
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Krina	Krin	k1gMnSc2	Krin
Gabbarda	Gabbard	k1gMnSc2	Gabbard
je	být	k5eAaImIp3nS	být
jazz	jazz	k1gInSc4	jazz
předobrazem	předobraz	k1gInSc7	předobraz
<g/>
,	,	kIx,	,
vhodným	vhodný	k2eAgNnSc7d1	vhodné
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
muziky	muzika	k1gFnSc2	muzika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
dost	dost	k6eAd1	dost
společného	společný	k2eAgInSc2d1	společný
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
společnou	společný	k2eAgFnSc4d1	společná
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Travis	Travis	k1gFnSc1	Travis
Jackson	Jacksona	k1gFnPc2	Jacksona
definuje	definovat	k5eAaBmIp3nS	definovat
jazz	jazz	k1gInSc1	jazz
také	také	k9	také
zeširoka	zeširoka	k6eAd1	zeširoka
<g/>
,	,	kIx,	,
tvrzením	tvrzení	k1gNnSc7	tvrzení
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
muzika	muzika	k1gFnSc1	muzika
obsahující	obsahující	k2eAgFnSc2d1	obsahující
kvality	kvalita	k1gFnSc2	kvalita
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
swinging	swinging	k1gInSc4	swinging
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
swingování	swingování	k1gNnSc1	swingování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
improvizace	improvizace	k1gFnSc1	improvizace
<g/>
,	,	kIx,	,
skupinová	skupinový	k2eAgFnSc1d1	skupinová
spolupráce	spolupráce	k1gFnSc1	spolupráce
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc1	vytvoření
si	se	k3xPyFc3	se
"	"	kIx"	"
<g/>
individuálního	individuální	k2eAgInSc2d1	individuální
hlasu	hlas	k1gInSc2	hlas
<g/>
"	"	kIx"	"
a	a	k8xC	a
otevřenost	otevřenost	k1gFnSc4	otevřenost
jiným	jiný	k2eAgFnPc3d1	jiná
hudebním	hudební	k2eAgFnPc3d1	hudební
možnostem	možnost	k1gFnPc3	možnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Improvizace	improvizace	k1gFnSc1	improvizace
==	==	k?	==
</s>
</p>
<p>
<s>
Jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jazzu	jazz	k1gInSc6	jazz
něco	něco	k6eAd1	něco
těžké	těžký	k2eAgNnSc1d1	těžké
definovat	definovat	k5eAaBmF	definovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
určitě	určitě	k6eAd1	určitě
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
věcí	věc	k1gFnPc2	věc
bude	být	k5eAaImBp3nS	být
improvizace	improvizace	k1gFnSc1	improvizace
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
blues	blues	k1gNnSc4	blues
běžně	běžně	k6eAd1	běžně
zakládali	zakládat	k5eAaImAgMnP	zakládat
svá	svůj	k3xOyFgNnPc4	svůj
sóla	sólo	k1gNnPc4	sólo
zčásti	zčásti	k6eAd1	zčásti
na	na	k7c4	na
call-and-response	callndesponse	k1gFnPc4	call-and-response
<g/>
,	,	kIx,	,
pocházejícím	pocházející	k2eAgMnSc7d1	pocházející
z	z	k7c2	z
afroamerické	afroamerický	k2eAgFnSc2d1	afroamerická
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
na	na	k7c6	na
pracovních	pracovní	k2eAgFnPc6d1	pracovní
a	a	k8xC	a
bojovných	bojovný	k2eAgFnPc2d1	bojovná
písní	píseň	k1gFnPc2	píseň
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgInPc1d1	základní
rysy	rys	k1gInPc1	rys
klasického	klasický	k2eAgInSc2d1	klasický
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
co	co	k9	co
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
klasické	klasický	k2eAgFnSc6d1	klasická
muzice	muzika	k1gFnSc6	muzika
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
snaží	snažit	k5eAaImIp3nP	snažit
zahrát	zahrát	k5eAaPmF	zahrát
skladbu	skladba	k1gFnSc4	skladba
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
kudrlinek	kudrlinka	k1gFnPc2	kudrlinka
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
jazzu	jazz	k1gInSc6	jazz
zkušený	zkušený	k2eAgMnSc1d1	zkušený
hráč	hráč	k1gMnSc1	hráč
ztvární	ztvárnit	k5eAaPmIp3nS	ztvárnit
skladbu	skladba	k1gFnSc4	skladba
ve	v	k7c6	v
velice	velice	k6eAd1	velice
individuálním	individuální	k2eAgInSc6d1	individuální
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nezahraje	zahrát	k5eNaPmIp3nS	zahrát
jednu	jeden	k4xCgFnSc4	jeden
skladbu	skladba	k1gFnSc4	skladba
dvakrát	dvakrát	k6eAd1	dvakrát
naprosto	naprosto	k6eAd1	naprosto
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Jazzový	jazzový	k2eAgMnSc1d1	jazzový
hudebník	hudebník	k1gMnSc1	hudebník
mění	měnit	k5eAaImIp3nS	měnit
melodie	melodie	k1gFnPc4	melodie
<g/>
,	,	kIx,	,
souzvuky	souzvuk	k1gInPc4	souzvuk
a	a	k8xC	a
odmlky	odmlk	k1gInPc4	odmlk
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
nálady	nálada	k1gFnSc2	nálada
<g/>
,	,	kIx,	,
osobních	osobní	k2eAgFnPc2d1	osobní
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
muzikanty	muzikant	k1gMnPc7	muzikant
nebo	nebo	k8xC	nebo
i	i	k9	i
s	s	k7c7	s
diváky	divák	k1gMnPc7	divák
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
klasická	klasický	k2eAgFnSc1d1	klasická
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
skladatelovým	skladatelův	k2eAgNnSc7d1	skladatelovo
médiem	médium	k1gNnSc7	médium
<g/>
.	.	kIx.	.
</s>
<s>
Jazz	jazz	k1gInSc1	jazz
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
jako	jako	k8xC	jako
demokratická	demokratický	k2eAgFnSc1d1	demokratická
společná	společný	k2eAgFnSc1d1	společná
kreativita	kreativita	k1gFnSc1	kreativita
<g/>
,	,	kIx,	,
interakce	interakce	k1gFnSc1	interakce
a	a	k8xC	a
spolupráce	spolupráce	k1gFnSc1	spolupráce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
klade	klást	k5eAaImIp3nS	klást
stejný	stejný	k2eAgInSc4d1	stejný
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
přínosy	přínos	k1gInPc4	přínos
skladatelovy	skladatelův	k2eAgInPc4d1	skladatelův
i	i	k9	i
hudebníkovy	hudebníkův	k2eAgFnSc2d1	hudebníkova
<g/>
,	,	kIx,	,
obratně	obratně	k6eAd1	obratně
odlišující	odlišující	k2eAgFnSc1d1	odlišující
jedno	jeden	k4xCgNnSc4	jeden
od	od	k7c2	od
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
"	"	kIx"	"
<g/>
New	New	k1gFnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
dixieland	dixieland	k1gInSc1	dixieland
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
muzikanti	muzikant	k1gMnPc1	muzikant
střídali	střídat	k5eAaImAgMnP	střídat
v	v	k7c4	v
hraní	hraní	k1gNnSc4	hraní
melodie	melodie	k1gFnSc2	melodie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
improvizovali	improvizovat	k5eAaImAgMnP	improvizovat
a	a	k8xC	a
hráli	hrát	k5eAaImAgMnP	hrát
protimelodie	protimelodie	k1gFnPc4	protimelodie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
éry	éra	k1gFnSc2	éra
swingu	swing	k1gInSc2	swing
začaly	začít	k5eAaPmAgFnP	začít
big	big	k?	big
bandy	banda	k1gFnPc1	banda
více	hodně	k6eAd2	hodně
spoléhat	spoléhat	k5eAaImF	spoléhat
a	a	k8xC	a
přiklánět	přiklánět	k5eAaImF	přiklánět
se	se	k3xPyFc4	se
k	k	k7c3	k
předepsané	předepsaný	k2eAgFnSc3d1	předepsaná
hudbě	hudba	k1gFnSc3	hudba
<g/>
:	:	kIx,	:
ta	ten	k3xDgFnSc1	ten
více	hodně	k6eAd2	hodně
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
na	na	k7c4	na
pevný	pevný	k2eAgInSc4d1	pevný
řád	řád	k1gInSc4	řád
a	a	k8xC	a
psané	psaný	k2eAgInPc4d1	psaný
notové	notový	k2eAgInPc4d1	notový
zápisy	zápis	k1gInPc4	zápis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
hudebníci	hudebník	k1gMnPc1	hudebník
buď	buď	k8xC	buď
naučili	naučit	k5eAaPmAgMnP	naučit
nebo	nebo	k8xC	nebo
je	on	k3xPp3gNnSc4	on
odposlouchali	odposlouchat	k5eAaPmAgMnP	odposlouchat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jazzových	jazzový	k2eAgMnPc2d1	jazzový
hudebníků	hudebník	k1gMnPc2	hudebník
neumělo	umět	k5eNaImAgNnS	umět
ani	ani	k8xC	ani
číst	číst	k5eAaImF	číst
noty	nota	k1gFnPc4	nota
<g/>
.	.	kIx.	.
</s>
<s>
Sólisté	sólista	k1gMnPc1	sólista
ale	ale	k9	ale
i	i	k9	i
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
předepsaných	předepsaný	k2eAgFnPc6d1	předepsaná
skladbách	skladba	k1gFnPc6	skladba
improvizovali	improvizovat	k5eAaImAgMnP	improvizovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
při	při	k7c6	při
vzestupu	vzestup	k1gInSc6	vzestup
bebopu	bebop	k1gInSc2	bebop
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
trend	trend	k1gInSc1	trend
soustředil	soustředit	k5eAaPmAgInS	soustředit
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
kapely	kapela	k1gFnPc4	kapela
a	a	k8xC	a
maximální	maximální	k2eAgFnSc4d1	maximální
improvizaci	improvizace	k1gFnSc4	improvizace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
melodie	melodie	k1gFnSc1	melodie
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
téma	téma	k1gNnSc1	téma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nastíněna	nastínit	k5eAaPmNgFnS	nastínit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prostředek	prostředek	k1gInSc1	prostředek
byl	být	k5eAaImAgInS	být
vyplněn	vyplnit	k5eAaPmNgInS	vyplnit
sérií	série	k1gFnSc7	série
improvizací	improvizace	k1gFnPc2	improvizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
styly	styl	k1gInPc1	styl
jako	jako	k8xC	jako
modal	modat	k5eAaPmAgInS	modat
jazz	jazz	k1gInSc1	jazz
opustily	opustit	k5eAaPmAgInP	opustit
striktní	striktní	k2eAgFnSc4d1	striktní
dodržování	dodržování	k1gNnSc4	dodržování
pořadí	pořadí	k1gNnSc2	pořadí
akordů	akord	k1gInPc2	akord
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
daly	dát	k5eAaPmAgFnP	dát
hudebníkům	hudebník	k1gMnPc3	hudebník
ještě	ještě	k9	ještě
větší	veliký	k2eAgInSc4d2	veliký
prostor	prostor	k1gInSc4	prostor
k	k	k7c3	k
improvizacím	improvizace	k1gFnPc3	improvizace
<g/>
.	.	kIx.	.
</s>
<s>
Avantgarde	Avantgard	k1gMnSc5	Avantgard
jazz	jazz	k1gInSc1	jazz
a	a	k8xC	a
free	free	k1gFnSc1	free
jazz	jazz	k1gInSc1	jazz
opouští	opouštět	k5eAaImIp3nS	opouštět
dokonce	dokonce	k9	dokonce
i	i	k9	i
stupnice	stupnice	k1gFnPc1	stupnice
a	a	k8xC	a
rytmy	rytmus	k1gInPc1	rytmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazz	jazz	k1gInSc4	jazz
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
hudbě	hudba	k1gFnSc6	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Jazz	jazz	k1gInSc1	jazz
je	být	k5eAaImIp3nS	být
hudba	hudba	k1gFnSc1	hudba
vážná	vážný	k2eAgFnSc1d1	vážná
beze	beze	k7c2	beze
zpěvu	zpěv	k1gInSc2	zpěv
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
výjimek	výjimka	k1gFnPc2	výjimka
se	se	k3xPyFc4	se
zpěv	zpěv	k1gInSc1	zpěv
objevit	objevit	k5eAaPmF	objevit
může	moct	k5eAaImIp3nS	moct
<g/>
)	)	kIx)	)
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
na	na	k7c4	na
piáno	piáno	k?	piáno
<g/>
,	,	kIx,	,
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc1	klarinet
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
poprvé	poprvé	k6eAd1	poprvé
zřejmě	zřejmě	k6eAd1	zřejmě
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
názvu	název	k1gInSc6	název
skladby	skladba	k1gFnSc2	skladba
skladatele	skladatel	k1gMnSc2	skladatel
Otakara	Otakar	k1gMnSc2	Otakar
Samka	Samek	k1gMnSc2	Samek
"	"	kIx"	"
<g/>
Wentery	Wenter	k1gInPc4	Wenter
Jazz	jazz	k1gInSc1	jazz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
s	s	k7c7	s
jazzem	jazz	k1gInSc7	jazz
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
hudba	hudba	k1gFnSc1	hudba
pramálo	pramálo	k6eAd1	pramálo
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Jazzem	jazz	k1gInSc7	jazz
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
tehdy	tehdy	k6eAd1	tehdy
nazývala	nazývat	k5eAaImAgFnS	nazývat
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
hudba	hudba	k1gFnSc1	hudba
či	či	k8xC	či
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
soupravu	souprava	k1gFnSc4	souprava
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Povědomí	povědomí	k1gNnSc1	povědomí
o	o	k7c6	o
jazzové	jazzový	k2eAgFnSc6d1	jazzová
hudbě	hudba	k1gFnSc6	hudba
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
velmi	velmi	k6eAd1	velmi
kusé	kusý	k2eAgFnPc1d1	kusá
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
mladí	mladý	k2eAgMnPc1d1	mladý
avantgardní	avantgardní	k2eAgMnPc1d1	avantgardní
umělci	umělec	k1gMnPc1	umělec
však	však	k9	však
začali	začít	k5eAaPmAgMnP	začít
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
jezdit	jezdit	k5eAaImF	jezdit
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
kumštýřů	kumštýř	k1gMnPc2	kumštýř
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkávali	setkávat	k5eAaImAgMnP	setkávat
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
rytmy	rytmus	k1gInPc1	rytmus
více	hodně	k6eAd2	hodně
vyhovovaly	vyhovovat	k5eAaImAgInP	vyhovovat
jejich	jejich	k3xOp3gInPc3	jejich
životním	životní	k2eAgInPc3d1	životní
pocitům	pocit	k1gInPc3	pocit
a	a	k8xC	a
celkové	celkový	k2eAgFnSc3d1	celková
atmosféře	atmosféra	k1gFnSc3	atmosféra
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nová	nový	k2eAgFnSc1d1	nová
dravá	dravý	k2eAgFnSc1d1	dravá
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
jazz	jazz	k1gInSc4	jazz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
kniha	kniha	k1gFnSc1	kniha
s	s	k7c7	s
prostým	prostý	k2eAgInSc7d1	prostý
názvem	název	k1gInSc7	název
Jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
evropských	evropský	k2eAgFnPc2d1	Evropská
knih	kniha	k1gFnPc2	kniha
pojednávajících	pojednávající	k2eAgFnPc2d1	pojednávající
o	o	k7c6	o
jazzu	jazz	k1gInSc6	jazz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
osobností	osobnost	k1gFnSc7	osobnost
vyrostlou	vyrostlý	k2eAgFnSc7d1	vyrostlá
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
jazzového	jazzový	k2eAgNnSc2d1	jazzové
podhoubí	podhoubí	k1gNnSc2	podhoubí
však	však	k9	však
byl	být	k5eAaImAgMnS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
jazzová	jazzový	k2eAgFnSc1d1	jazzová
tvorba	tvorba	k1gFnSc1	tvorba
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
raný	raný	k2eAgInSc4d1	raný
český	český	k2eAgInSc4d1	český
swing	swing	k1gInSc4	swing
<g/>
.	.	kIx.	.
</s>
<s>
Ježek	Ježek	k1gMnSc1	Ježek
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
Hlásil	hlásit	k5eAaImAgMnS	hlásit
se	se	k3xPyFc4	se
na	na	k7c4	na
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
<g/>
,	,	kIx,	,
přijímací	přijímací	k2eAgFnSc1d1	přijímací
komise	komise	k1gFnSc1	komise
poznala	poznat	k5eAaPmAgFnS	poznat
jeho	jeho	k3xOp3gNnSc4	jeho
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Ježek	Ježek	k1gMnSc1	Ježek
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
jako	jako	k8xC	jako
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
fašismu	fašismus	k1gInSc2	fašismus
se	se	k3xPyFc4	se
jazzová	jazzový	k2eAgFnSc1d1	jazzová
hudba	hudba	k1gFnSc1	hudba
stala	stát	k5eAaPmAgFnS	stát
nežádoucí	žádoucí	k2eNgFnSc1d1	nežádoucí
jako	jako	k8xS	jako
hudba	hudba	k1gFnSc1	hudba
méněcenné	méněcenný	k2eAgFnSc2d1	méněcenná
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
mnoho	mnoho	k4c1	mnoho
obětí	oběť	k1gFnPc2	oběť
mezi	mezi	k7c7	mezi
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Nesmyslné	smyslný	k2eNgInPc1d1	nesmyslný
zákazy	zákaz	k1gInPc1	zákaz
a	a	k8xC	a
omezení	omezení	k1gNnSc4	omezení
obcházeli	obcházet	k5eAaImAgMnP	obcházet
hudebníci	hudebník	k1gMnPc1	hudebník
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
tanečních	taneční	k2eAgFnPc2d1	taneční
zábav	zábava	k1gFnPc2	zábava
se	se	k3xPyFc4	se
písně	píseň	k1gFnPc1	píseň
dostaly	dostat	k5eAaPmAgFnP	dostat
na	na	k7c4	na
koncertní	koncertní	k2eAgFnPc4d1	koncertní
pódia	pódium	k1gNnPc4	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
tak	tak	k6eAd1	tak
plnit	plnit	k5eAaImF	plnit
i	i	k9	i
poslechovou	poslechový	k2eAgFnSc4d1	poslechová
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
český	český	k2eAgInSc1d1	český
jazz	jazz	k1gInSc1	jazz
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
tvorba	tvorba	k1gFnSc1	tvorba
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitra	Šlitr	k1gMnSc2	Šlitr
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
Suchého	Suchý	k1gMnSc2	Suchý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazz	jazz	k1gInSc1	jazz
obohatil	obohatit	k5eAaPmAgInS	obohatit
českou	český	k2eAgFnSc4d1	Česká
hudbu	hudba	k1gFnSc4	hudba
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgInPc4d1	nový
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
řadu	řada	k1gFnSc4	řada
výtvarníků	výtvarník	k1gMnPc2	výtvarník
<g/>
,	,	kIx,	,
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
divadelníků	divadelník	k1gMnPc2	divadelník
či	či	k8xC	či
filmařů	filmař	k1gMnPc2	filmař
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
naivních	naivní	k2eAgInPc6d1	naivní
začátcích	začátek	k1gInPc6	začátek
a	a	k8xC	a
převzatých	převzatý	k2eAgFnPc6d1	převzatá
skladbách	skladba	k1gFnPc6	skladba
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
české	český	k2eAgFnPc1d1	Česká
písničky	písnička	k1gFnPc1	písnička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jazz	jazz	k1gInSc1	jazz
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
jazzových	jazzový	k2eAgMnPc2d1	jazzový
kytaristů	kytarista	k1gMnPc2	kytarista
</s>
</p>
<p>
<s>
Jazz	jazz	k1gInSc1	jazz
dance	dance	k1gFnSc2	dance
</s>
</p>
<p>
<s>
Step	step	k1gFnSc1	step
(	(	kIx(	(
<g/>
tanec	tanec	k1gInSc1	tanec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
scénický	scénický	k2eAgInSc1d1	scénický
tanec	tanec	k1gInSc1	tanec
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jazz	jazz	k1gInSc1	jazz
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Jazz	jazz	k1gInSc1	jazz
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jazz	jazz	k1gInSc1	jazz
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
jazzová	jazzový	k2eAgFnSc1d1	jazzová
společnost	společnost	k1gFnSc1	společnost
</s>
</p>
<p>
<s>
Konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
a	a	k8xC	a
VOŠ	VOŠ	kA	VOŠ
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Ježka	Ježek	k1gMnSc2	Ježek
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
jazz	jazz	k1gInSc4	jazz
a	a	k8xC	a
muzikál	muzikál	k1gInSc4	muzikál
</s>
</p>
<p>
<s>
ČRo	ČRo	k?	ČRo
Euro	euro	k1gNnSc1	euro
Jazz	jazz	k1gInSc4	jazz
<g/>
,	,	kIx,	,
jazzové	jazzový	k2eAgNnSc4d1	jazzové
internetové	internetový	k2eAgNnSc4d1	internetové
rádio	rádio	k1gNnSc4	rádio
</s>
</p>
<p>
<s>
JazzPort	JazzPort	k1gInSc1	JazzPort
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
portál	portál	k1gInSc1	portál
o	o	k7c6	o
jazzovém	jazzový	k2eAgNnSc6d1	jazzové
dění	dění	k1gNnSc6	dění
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
