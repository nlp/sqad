<s>
Dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
transcendentní	transcendentní	k2eAgNnSc1d1	transcendentní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nelze	lze	k6eNd1	lze
ho	on	k3xPp3gNnSc4	on
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
kořen	kořen	k1gInSc4	kořen
konečného	konečný	k2eAgInSc2d1	konečný
mnohočlenu	mnohočlen	k1gInSc2	mnohočlen
s	s	k7c7	s
celočíselnými	celočíselný	k2eAgInPc7d1	celočíselný
koeficienty	koeficient	k1gInPc7	koeficient
<g/>
.	.	kIx.	.
</s>
