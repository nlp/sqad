<s>
Mnoho	mnoho	k4c1
přírodních	přírodní	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
modelovat	modelovat	k5eAaImF
fraktální	fraktální	k2eAgFnSc7d1
geometrií	geometrie	k1gFnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
hory	hora	k1gFnPc4
<g/>
,	,	kIx,
mraky	mrak	k1gInPc4
<g/>
,	,	kIx,
sněhové	sněhový	k2eAgFnPc4d1
vločky	vločka	k1gFnPc4
<g/>
,	,	kIx,
řeky	řeka	k1gFnPc4
a	a	k8xC
nebo	nebo	k8xC
cévní	cévní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>