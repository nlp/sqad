<s>
Fraktál	fraktál	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Detail	detail	k1gInSc1
Mandelbrotovy	Mandelbrotův	k2eAgFnSc2d1
množiny	množina	k1gFnSc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejznámějších	známý	k2eAgMnPc2d3
fraktálů	fraktál	k1gInPc2
</s>
<s>
Fraktál	fraktál	k1gInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
původní	původní	k2eAgFnSc2d1
Mandelbrotovy	Mandelbrotův	k2eAgFnSc2d1
definice	definice	k1gFnSc2
množina	množina	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
Hausdorffova	Hausdorffův	k2eAgFnSc1d1
dimenze	dimenze	k1gFnSc1
je	být	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
dimenze	dimenze	k1gFnSc1
topologická	topologický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
jej	on	k3xPp3gMnSc4
také	také	k9
definovat	definovat	k5eAaBmF
poněkud	poněkud	k6eAd1
jednodušeji	jednoduše	k6eAd2
(	(	kIx(
<g/>
méně	málo	k6eAd2
obecně	obecně	k6eAd1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
geometrický	geometrický	k2eAgInSc1d1
objekt	objekt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
následující	následující	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
je	být	k5eAaImIp3nS
soběpodobný	soběpodobný	k2eAgMnSc1d1
–	–	k?
znamená	znamenat	k5eAaImIp3nS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
daný	daný	k2eAgInSc4d1
útvar	útvar	k1gInSc4
pozorujeme	pozorovat	k5eAaImIp1nP
v	v	k7c6
jakémkoliv	jakýkoliv	k3yIgNnSc6
měřítku	měřítko	k1gNnSc6
či	či	k8xC
rozlišení	rozlišení	k1gNnSc6
<g/>
,	,	kIx,
pozorujeme	pozorovat	k5eAaImIp1nP
stále	stále	k6eAd1
opakující	opakující	k2eAgMnSc1d1
se	se	k3xPyFc4
určitý	určitý	k2eAgInSc1d1
charakteristický	charakteristický	k2eAgInSc1d1
tvar	tvar	k1gInSc1
(	(	kIx(
<g/>
motiv	motiv	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
mívá	mívat	k5eAaImIp3nS
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
velmi	velmi	k6eAd1
složitý	složitý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
generován	generován	k2eAgInSc1d1
opakovaným	opakovaný	k2eAgNnSc7d1
použitím	použití	k1gNnSc7
jednoduchých	jednoduchý	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Fraktály	fraktál	k1gInPc1
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
coby	coby	k?
nejsložitější	složitý	k2eAgInPc4d3
geometrické	geometrický	k2eAgInPc4d1
objekty	objekt	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
současná	současný	k2eAgFnSc1d1
matematika	matematika	k1gFnSc1
zkoumá	zkoumat	k5eAaImIp3nS
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
však	však	k9
často	často	k6eAd1
překvapivě	překvapivě	k6eAd1
jednoduchou	jednoduchý	k2eAgFnSc4d1
matematickou	matematický	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Termín	termín	k1gInSc1
fraktál	fraktál	k1gInSc1
použil	použít	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
matematik	matematik	k1gMnSc1
Benoît	Benoît	k1gMnSc1
Mandelbrot	Mandelbrot	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
fractus	fractus	k1gInSc1
–	–	k?
rozbitý	rozbitý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgInPc1d1
objekty	objekt	k1gInPc1
byly	být	k5eAaImAgInP
známy	znám	k2eAgInPc1d1
v	v	k7c6
matematice	matematika	k1gFnSc6
již	již	k6eAd1
dlouho	dlouho	k6eAd1
předtím	předtím	k6eAd1
(	(	kIx(
<g/>
např.	např.	kA
Kochova	Kochův	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
B.	B.	kA
Mandelbrot	Mandelbrot	k1gMnSc1
navázal	navázat	k5eAaPmAgMnS
na	na	k7c4
článek	článek	k1gInSc4
Deux	Deux	k1gInSc1
types	types	k1gInSc1
fondamentaux	fondamentaux	k1gInSc1
de	de	k?
distribution	distribution	k1gInSc1
statistique	statistique	k1gFnSc1
(	(	kIx(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS
česky	česky	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
ve	v	k7c6
Statistickém	statistický	k2eAgInSc6d1
obzoru	obzor	k1gInSc6
<g/>
,	,	kIx,
r.	r.	kA
22	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
171	#num#	k4
<g/>
-	-	kIx~
<g/>
222	#num#	k4
<g/>
,	,	kIx,
pod	pod	k7c7
názvem	název	k1gInSc7
Přírodní	přírodní	k2eAgFnSc1d1
dualita	dualita	k1gFnSc1
statistického	statistický	k2eAgNnSc2d1
rozložení	rozložení	k1gNnSc2
<g/>
)	)	kIx)
českého	český	k2eAgMnSc2d1
geografa	geograf	k1gMnSc2
<g/>
,	,	kIx,
demografa	demograf	k1gMnSc2
a	a	k8xC
statistika	statistik	k1gMnSc2
Jaromíra	Jaromír	k1gMnSc2
Korčáka	Korčák	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhy	druh	k1gInPc1
fraktálů	fraktál	k1gInPc2
</s>
<s>
Dokonce	dokonce	k9
2000	#num#	k4
násobné	násobný	k2eAgNnSc1d1
zvětšení	zvětšení	k1gNnSc1
Mandelbrotova	Mandelbrotův	k2eAgInSc2d1
fraktálu	fraktál	k1gInSc2
nesníží	snížit	k5eNaPmIp3nP
kvalitu	kvalita	k1gFnSc4
nejjemnějších	jemný	k2eAgInPc2d3
detailů	detail	k1gInPc2
jež	jenž	k3xRgNnSc4
stále	stále	k6eAd1
mají	mít	k5eAaImIp3nP
charakteristický	charakteristický	k2eAgInSc4d1
tvar	tvar	k1gInSc4
celého	celý	k2eAgInSc2d1
obrazce	obrazec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
tyto	tento	k3xDgInPc1
druhy	druh	k1gInPc1
fraktálních	fraktální	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
L-systémy	L-systém	k1gInPc1
</s>
<s>
IFS	IFS	kA
</s>
<s>
TEA	Tea	k1gFnSc1
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
fraktály	fraktál	k1gInPc1
</s>
<s>
Mnoho	mnoho	k4c1
přírodních	přírodní	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
modelovat	modelovat	k5eAaImF
fraktální	fraktální	k2eAgFnSc7d1
geometrií	geometrie	k1gFnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
hory	hora	k1gFnPc4
<g/>
,	,	kIx,
mraky	mrak	k1gInPc4
<g/>
,	,	kIx,
sněhové	sněhový	k2eAgFnPc4d1
vločky	vločka	k1gFnPc4
<g/>
,	,	kIx,
řeky	řeka	k1gFnPc4
a	a	k8xC
nebo	nebo	k8xC
cévní	cévní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrým	dobrý	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
organického	organický	k2eAgInSc2d1
fraktálu	fraktál	k1gInSc2
je	být	k5eAaImIp3nS
romanesko	romanesko	k1gNnSc1
(	(	kIx(
<g/>
druh	druh	k1gInSc1
květáku	květák	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Často	často	k6eAd1
se	s	k7c7
tvary	tvar	k1gInPc7
stromů	strom	k1gInPc2
a	a	k8xC
kapradiny	kapradina	k1gFnSc2
v	v	k7c6
přírodě	příroda	k1gFnSc6
modelují	modelovat	k5eAaImIp3nP
na	na	k7c6
počítačích	počítač	k1gInPc6
použitím	použití	k1gNnSc7
rekurzivních	rekurzivní	k2eAgInPc2d1
algoritmů	algoritmus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Generování	generování	k1gNnSc1
fraktálů	fraktál	k1gInPc2
</s>
<s>
Fraktály	fraktál	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
jednoduše	jednoduše	k6eAd1
generovány	generován	k2eAgFnPc4d1
na	na	k7c6
počítačích	počítač	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
spousta	spousta	k1gFnSc1
software	software	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
umožňují	umožňovat	k5eAaImIp3nP
generování	generování	k1gNnSc4
fraktálních	fraktální	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Fractint	Fractint	k1gInSc1
(	(	kIx(
<g/>
multiplatformní	multiplatformní	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Sterling	sterling	k1gInSc1
Fractal	Fractal	k1gMnSc1
–	–	k?
Pokročilý	pokročilý	k2eAgInSc4d1
program	program	k1gInSc4
pro	pro	k7c4
generování	generování	k1gNnSc4
fraktálů	fraktál	k1gInPc2
pro	pro	k7c4
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
Microsoft	Microsoft	kA
Windows	Windows	kA
naprogramovaný	naprogramovaný	k2eAgInSc4d1
Stephenem	Stephen	k1gMnSc7
Fergusonem	Ferguson	k1gMnSc7
</s>
<s>
XaoS	XaoS	k?
–	–	k?
Rychlý	rychlý	k2eAgInSc1d1
real-timový	real-timový	k2eAgInSc1d1
prohlížeč	prohlížeč	k1gInSc1
fraktálů	fraktál	k1gInPc2
(	(	kIx(
<g/>
domovská	domovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Electric	Electric	k1gMnSc1
Sheep	Sheep	k1gMnSc1
–	–	k?
opensource	opensourka	k1gFnSc3
distribuovaný	distribuovaný	k2eAgInSc1d1
software	software	k1gInSc1
tvořící	tvořící	k2eAgFnSc2d1
animace	animace	k1gFnSc2
fraktálů	fraktál	k1gInPc2
</s>
<s>
Kalles	Kalles	k1gMnSc1
Fraktaler	Fraktaler	k1gMnSc1
<g/>
,	,	kIx,
MandelMachine	MandelMachin	k1gMnSc5
<g/>
,	,	kIx,
Fractal	Fractal	k1gMnSc1
Explorer	Explorer	k1gMnSc1
<g/>
,	,	kIx,
XaOs	XaOs	k1gInSc1
<g/>
,	,	kIx,
Frax	Frax	k1gInSc1
<g/>
,	,	kIx,
Jux	jux	k1gInSc1
-	-	kIx~
zoom	zoom	k6eAd1
v	v	k7c6
mandelbrotově	mandelbrotův	k2eAgFnSc6d1
množině	množina	k1gFnSc6
</s>
<s>
Flam	Flam	k1gInSc1
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Apophysis	Apophysis	k1gFnSc1
<g/>
,	,	kIx,
jWildfire	jWildfir	k1gMnSc5
<g/>
,	,	kIx,
Chaotica	Chaoticum	k1gNnPc1
<g/>
,	,	kIx,
Fractorium	Fractorium	k1gNnSc1
-	-	kIx~
IFS	IFS	kA
2D	2D	k4
</s>
<s>
Incendia	Incendium	k1gNnPc1
<g/>
,	,	kIx,
Xenodream	Xenodream	k1gInSc1
<g/>
,	,	kIx,
Mandelbulb	Mandelbulb	k1gInSc1
<g/>
3	#num#	k4
<g/>
D	D	kA
<g/>
,	,	kIx,
Mandelbulber	Mandelbulber	k1gInSc4
-	-	kIx~
Escape-time	Escape-tim	k1gMnSc5
<g/>
/	/	kIx~
<g/>
IFS	IFS	kA
3D	3D	k4
</s>
<s>
UltraFractal	UltraFractal	k1gMnSc1
<g/>
,	,	kIx,
Vision	vision	k1gInSc1
Of	Of	k1gMnSc1
Chaos	chaos	k1gInSc1
-	-	kIx~
univerzální	univerzální	k2eAgMnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Fraktály	fraktál	k1gInPc4
<g/>
:	:	kIx,
Tvar	tvar	k1gInSc1
<g/>
,	,	kIx,
náhoda	náhoda	k1gFnSc1
a	a	k8xC
dimenze	dimenze	k1gFnSc1
<g/>
,	,	kIx,
Benoît	Benoît	k1gMnSc1
Mandelbrot	Mandelbrot	k1gMnSc1
(	(	kIx(
<g/>
přeložil	přeložit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Fiala	Fiala	k1gMnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
vydalo	vydat	k5eAaPmAgNnS
nakladatelství	nakladatelství	k1gNnSc4
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BUCHANAN	BUCHANAN	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všeobecný	všeobecný	k2eAgInSc1d1
princip	princip	k1gInSc1
<g/>
:	:	kIx,
věda	věda	k1gFnSc1
o	o	k7c6
historii	historie	k1gFnSc6
<g/>
:	:	kIx,
proč	proč	k6eAd1
je	být	k5eAaImIp3nS
svět	svět	k1gInSc1
jednodušší	jednoduchý	k2eAgInSc1d2
<g/>
,	,	kIx,
než	než	k8xS
si	se	k3xPyFc3
myslíme	myslet	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Baronet	baronet	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7214-644-0	80-7214-644-0	k4
</s>
<s>
MANDELBROT	MANDELBROT	kA
<g/>
,	,	kIx,
Benoît	Benoît	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fraktály	fraktál	k1gInPc4
<g/>
:	:	kIx,
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
náhoda	náhoda	k1gFnSc1
a	a	k8xC
dimenze	dimenze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-204-1009-0	80-204-1009-0	k4
</s>
<s>
ZELINKA	Zelinka	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
VČELAŘ	včelař	k1gMnSc1
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fraktální	fraktální	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
<g/>
:	:	kIx,
principy	princip	k1gInPc1
a	a	k8xC
aplikace	aplikace	k1gFnPc1
<g/>
;	;	kIx,
nakladatelství	nakladatelství	k1gNnSc1
BEN	Ben	k1gInSc1
-	-	kIx~
technická	technický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7300-191-8	80-7300-191-8	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Barnsleyho	Barnsleyze	k6eAd1
kapradí	kapradí	k1gNnSc1
</s>
<s>
Cantorovo	Cantorův	k2eAgNnSc1d1
diskontinuum	diskontinuum	k1gNnSc1
</s>
<s>
Juliova	Juliův	k2eAgFnSc1d1
množina	množina	k1gFnSc1
</s>
<s>
Komplexní	komplexní	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Mandelbrotova	Mandelbrotův	k2eAgFnSc1d1
množina	množina	k1gFnSc1
</s>
<s>
Mocninná	mocninný	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
</s>
<s>
Motýlí	motýlí	k2eAgInSc1d1
efekt	efekt	k1gInSc1
</s>
<s>
Samoorganizace	Samoorganizace	k1gFnSc1
</s>
<s>
Smaragdová	smaragdový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
jest	být	k5eAaImIp3nS
dole	dole	k6eAd1
<g/>
,	,	kIx,
jest	být	k5eAaImIp3nS
jako	jako	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
jest	být	k5eAaImIp3nS
nahoře	nahoře	k6eAd1
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Teorie	teorie	k1gFnSc1
centrálních	centrální	k2eAgNnPc2d1
míst	místo	k1gNnPc2
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
řez	řez	k1gInSc1
</s>
<s>
Juliova	Juliův	k2eAgFnSc1d1
množina	množina	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
fraktál	fraktál	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
fraktál	fraktál	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
fraktál	fraktál	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Seriál	seriál	k1gInSc1
Fraktály	fraktál	k1gInPc1
v	v	k7c6
počítačové	počítačový	k2eAgFnSc6d1
grafice	grafika	k1gFnSc6
na	na	k7c4
Root	Root	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Jemný	jemný	k2eAgInSc1d1
úvod	úvod	k1gInSc1
do	do	k7c2
Fraktálů	fraktál	k1gInPc2
</s>
<s>
Sterling	sterling	k1gInSc4
<g/>
2	#num#	k4
freeware	freeware	k1gInSc1
fractal	fractat	k5eAaImAgInS,k5eAaPmAgInS
generator	generator	k1gInSc4
</s>
<s>
IFS	IFS	kA
Illusions	Illusions	k1gInSc1
</s>
<s>
Audiovizuální	audiovizuální	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
</s>
<s>
Fractals	Fractals	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Colors	Colors	k1gInSc1
of	of	k?
Infinity	Infinita	k1gFnPc1
<g/>
,	,	kIx,
dokument	dokument	k1gInSc1
<g/>
,	,	kIx,
53	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Nigel	Nigel	k1gMnSc1
Lesmoir	Lesmoir	k1gMnSc1
-	-	kIx~
Gordon	Gordon	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
7472	#num#	k4
</s>
