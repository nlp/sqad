<p>
<s>
Roberta	Robert	k1gMnSc2	Robert
Lynn	Lynn	k1gMnSc1	Lynn
Bondarová	Bondarová	k1gFnSc1	Bondarová
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1945	[number]	k4	1945
Sault	Sault	k1gMnSc1	Sault
Ste	Ste	k?	Ste
<g/>
.	.	kIx.	.
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lékařka	lékařka	k1gFnSc1	lékařka
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
astronautka	astronautka	k1gFnSc1	astronautka
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgFnSc1d1	životní
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Guelph	Guelph	k1gMnSc1	Guelph
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
obor	obor	k1gInSc4	obor
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Skončila	skončit	k5eAaPmAgFnS	skončit
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
tříletém	tříletý	k2eAgNnSc6d1	tříleté
postgraduálním	postgraduální	k2eAgNnSc6d1	postgraduální
studiu	studio	k1gNnSc6	studio
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Western	Western	kA	Western
Ontario	Ontario	k1gNnSc4	Ontario
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
experimentální	experimentální	k2eAgFnSc2d1	experimentální
patologie	patologie	k1gFnSc2	patologie
a	a	k8xC	a
neurologie	neurologie	k1gFnSc2	neurologie
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
následovala	následovat	k5eAaImAgFnS	následovat
další	další	k2eAgFnSc1d1	další
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
University	universita	k1gFnSc2	universita
of	of	k?	of
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
zakončila	zakončit	k5eAaPmAgFnS	zakončit
získáním	získání	k1gNnSc7	získání
titulu	titul	k1gInSc2	titul
PhD	PhD	k1gFnSc2	PhD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
a	a	k8xC	a
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
na	na	k7c4	na
McMaster	McMaster	k1gInSc4	McMaster
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
ukončila	ukončit	k5eAaPmAgFnS	ukončit
vzdělávání	vzdělávání	k1gNnSc3	vzdělávání
jako	jako	k8xS	jako
všestranná	všestranný	k2eAgFnSc1d1	všestranná
lékařka	lékařka	k1gFnSc1	lékařka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
jmenované	jmenovaný	k2eAgFnSc6d1	jmenovaná
škole	škola	k1gFnSc6	škola
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1982-1983	[number]	k4	1982-1983
jako	jako	k8xC	jako
asistující	asistující	k2eAgFnSc1d1	asistující
profesorka	profesorka	k1gFnSc1	profesorka
pro	pro	k7c4	pro
obor	obor	k1gInSc4	obor
neurologie	neurologie	k1gFnSc2	neurologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
na	na	k7c4	na
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
do	do	k7c2	do
NRCC	NRCC	kA	NRCC
<g/>
/	/	kIx~	/
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
do	do	k7c2	do
výcvikového	výcvikový	k2eAgNnSc2d1	výcvikové
střediska	středisko	k1gNnSc2	středisko
astronautů	astronaut	k1gMnPc2	astronaut
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
letěla	letět	k5eAaImAgFnS	letět
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
jako	jako	k8xS	jako
18	[number]	k4	18
<g/>
.	.	kIx.	.
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
264	[number]	k4	264
<g/>
.	.	kIx.	.
člověk	člověk	k1gMnSc1	člověk
z	z	k7c2	z
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Strávila	strávit	k5eAaPmAgFnS	strávit
tam	tam	k6eAd1	tam
osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
letu	let	k1gInSc2	let
z	z	k7c2	z
NASA	NASA	kA	NASA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Let	léto	k1gNnPc2	léto
raketoplánem	raketoplán	k1gInSc7	raketoplán
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1992	[number]	k4	1992
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
z	z	k7c2	z
Floridy	Florida	k1gFnSc2	Florida
raketoplán	raketoplán	k1gInSc4	raketoplán
Discovery	Discovera	k1gFnSc2	Discovera
ke	k	k7c3	k
svému	své	k1gNnSc3	své
14	[number]	k4	14
<g/>
.	.	kIx.	.
letu	let	k1gInSc3	let
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
palubě	paluba	k1gFnSc6	paluba
bylo	být	k5eAaImAgNnS	být
sedm	sedm	k4xCc1	sedm
astronautů	astronaut	k1gMnPc2	astronaut
<g/>
:	:	kIx,	:
Ronald	Ronald	k1gMnSc1	Ronald
Grabe	Grab	k1gInSc5	Grab
<g/>
,	,	kIx,	,
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Oswald	Oswald	k1gMnSc1	Oswald
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
Thagard	Thagard	k1gMnSc1	Thagard
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Hilmers	Hilmers	k1gInSc1	Hilmers
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Readdy	Readda	k1gFnSc2	Readda
<g/>
,	,	kIx,	,
Ulf	Ulf	k1gMnSc1	Ulf
Merbold	Merbold	k1gMnSc1	Merbold
a	a	k8xC	a
Roberta	Roberta	k1gFnSc1	Roberta
Bondarová	Bondarová	k1gFnSc1	Bondarová
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sestava	sestava	k1gFnSc1	sestava
vědců	vědec	k1gMnPc2	vědec
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
oživila	oživit	k5eAaPmAgFnS	oživit
laboratoře	laboratoř	k1gFnPc4	laboratoř
Spacelab	Spacelaba	k1gFnPc2	Spacelaba
a	a	k8xC	a
Biorack	Bioracka	k1gFnPc2	Bioracka
a	a	k8xC	a
věnovala	věnovat	k5eAaImAgFnS	věnovat
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
různým	různý	k2eAgInPc3d1	různý
pokusům	pokus	k1gInPc3	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Bondarová	Bondarová	k1gFnSc1	Bondarová
(	(	kIx(	(
<g/>
z	z	k7c2	z
modrého	modrý	k2eAgInSc2d1	modrý
týmu	tým	k1gInSc2	tým
<g/>
)	)	kIx)	)
mj.	mj.	kA	mj.
ve	v	k7c6	v
skleníku	skleník	k1gInSc6	skleník
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
semenáčky	semenáček	k1gMnPc4	semenáček
ovsa	oves	k1gInSc2	oves
<g/>
.	.	kIx.	.
</s>
<s>
Přistáli	přistát	k5eAaPmAgMnP	přistát
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Edwards	Edwardsa	k1gFnPc2	Edwardsa
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
po	po	k7c6	po
osmi	osm	k4xCc6	osm
dnech	den	k1gInPc6	den
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STS-42	STS-42	k4	STS-42
Discovery	Discover	k1gInPc4	Discover
start	start	k1gInSc1	start
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
přistání	přistání	k1gNnSc2	přistání
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Roberta	Robert	k1gMnSc2	Robert
Bondarová	Bondarová	k1gFnSc1	Bondarová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://mek.kosmo.cz/bio/ostatni/00264.htm	[url]	k6eAd1	http://mek.kosmo.cz/bio/ostatni/00264.htm
</s>
</p>
<p>
<s>
http://mek.kosmo.cz/pil_lety/usa/sts/sts-42/index.htm	[url]	k6eAd1	http://mek.kosmo.cz/pil_lety/usa/sts/sts-42/index.htm
</s>
</p>
<p>
<s>
http://www.lib.cas.cz/space.40/INDEX2.HTM	[url]	k4	http://www.lib.cas.cz/space.40/INDEX2.HTM
</s>
</p>
