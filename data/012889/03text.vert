<p>
<s>
Gabriel	Gabriel	k1gMnSc1	Gabriel
da	da	k?	da
Silva	Silva	k1gFnSc1	Silva
je	být	k5eAaImIp3nS	být
brazilský	brazilský	k2eAgMnSc1d1	brazilský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
obránce	obránce	k1gMnSc1	obránce
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
bez	bez	k7c2	bez
angažmá	angažmá	k1gNnSc2	angažmá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
kariéru	kariéra	k1gFnSc4	kariéra
zahájil	zahájit	k5eAaPmAgMnS	zahájit
ve	v	k7c6	v
slavném	slavný	k2eAgInSc6d1	slavný
brazilském	brazilský	k2eAgInSc6d1	brazilský
celku	celek	k1gInSc6	celek
Santos	Santosa	k1gFnPc2	Santosa
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Coritiby	Coritiba	k1gFnSc2	Coritiba
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
do	do	k7c2	do
Taubaté	Taubatá	k1gFnSc2	Taubatá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc7	jeho
novým	nový	k2eAgNnSc7d1	nové
působištěm	působiště	k1gNnSc7	působiště
stala	stát	k5eAaPmAgFnS	stát
EC	EC	kA	EC
Vitória	Vitórium	k1gNnSc2	Vitórium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
si	se	k3xPyFc3	se
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
zahrál	zahrát	k5eAaPmAgMnS	zahrát
za	za	k7c4	za
Clube	club	k1gInSc5	club
Atlético	Atlético	k1gNnSc1	Atlético
Assisense	Assisensa	k1gFnSc3	Assisensa
Sã	Sã	k1gFnSc1	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
FC	FC	kA	FC
Vysočina	vysočina	k1gFnSc1	vysočina
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
vedení	vedení	k1gNnSc4	vedení
Vysočiny	vysočina	k1gFnSc2	vysočina
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
domluvil	domluvit	k5eAaPmAgInS	domluvit
na	na	k7c4	na
působení	působení	k1gNnSc4	působení
ve	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
týmu	tým	k1gInSc6	tým
DAC	DAC	kA	DAC
1904	[number]	k4	1904
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
Streda	Stred	k1gMnSc2	Stred
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
za	za	k7c4	za
celek	celek	k1gInSc4	celek
Grê	Grê	k1gFnSc2	Grê
Barueri	Barueri	k1gNnSc7	Barueri
Futebol	Futebola	k1gFnPc2	Futebola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
opět	opět	k6eAd1	opět
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
angažoval	angažovat	k5eAaBmAgInS	angažovat
chorvatský	chorvatský	k2eAgInSc1d1	chorvatský
klub	klub	k1gInSc1	klub
NK	NK	kA	NK
Bistra	bistro	k1gNnSc2	bistro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
ovšem	ovšem	k9	ovšem
zůstal	zůstat	k5eAaPmAgMnS	zůstat
rok	rok	k1gInSc4	rok
a	a	k8xC	a
následně	následně	k6eAd1	následně
si	se	k3xPyFc3	se
na	na	k7c4	na
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
druhou	druhý	k4xOgFnSc4	druhý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
americkou	americký	k2eAgFnSc4d1	americká
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
za	za	k7c4	za
Carolinu	Carolina	k1gFnSc4	Carolina
RailHawks	RailHawksa	k1gFnPc2	RailHawksa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavní	slavný	k2eAgMnPc1d1	slavný
spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
slavnými	slavný	k2eAgMnPc7d1	slavný
hráči	hráč	k1gMnPc7	hráč
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Diego	Diego	k6eAd1	Diego
a	a	k8xC	a
Rafinha	Rafinha	k1gFnSc1	Rafinha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
FC	FC	kA	FC
Vysočina	vysočina	k1gFnSc1	vysočina
Jihlava	Jihlava	k1gFnSc1	Jihlava
</s>
</p>
<p>
<s>
postup	postup	k1gInSc1	postup
do	do	k7c2	do
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Gabriel	Gabriel	k1gMnSc1	Gabriel
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Vysočiny	vysočina	k1gFnSc2	vysočina
Jihlava	Jihlava	k1gFnSc1	Jihlava
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
<g/>
,	,	kIx,	,
transfermarkt	transfermarkt	k1gInSc1	transfermarkt
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
</s>
</p>
