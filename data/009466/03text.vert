<p>
<s>
Mošt	mošt	k1gInSc1	mošt
je	být	k5eAaImIp3nS	být
tekutina	tekutina	k1gFnSc1	tekutina
z	z	k7c2	z
vinných	vinný	k2eAgInPc2d1	vinný
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
získaná	získaný	k2eAgFnSc1d1	získaná
buď	buď	k8xC	buď
lisováním	lisování	k1gNnSc7	lisování
rmutu	rmut	k1gInSc2	rmut
<g/>
,	,	kIx,	,
lisováním	lisování	k1gNnSc7	lisování
celých	celý	k2eAgMnPc2d1	celý
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
samotokem	samotok	k1gInSc7	samotok
<g/>
.	.	kIx.	.
</s>
<s>
Částečným	částečný	k2eAgNnSc7d1	částečné
zkvašením	zkvašení	k1gNnSc7	zkvašení
moštu	mošt	k1gInSc2	mošt
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
burčák	burčák	k1gInSc1	burčák
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
jablečný	jablečný	k2eAgInSc1d1	jablečný
mošt	mošt	k1gInSc1	mošt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
jablečná	jablečný	k2eAgFnSc1d1	jablečná
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Moštoměr	moštoměr	k1gInSc1	moštoměr
</s>
</p>
