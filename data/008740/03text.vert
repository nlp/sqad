<p>
<s>
Slizoblanka	Slizoblanka	k1gFnSc1	Slizoblanka
(	(	kIx(	(
<g/>
Epigloea	Epigloea	k1gFnSc1	Epigloea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
parazitují	parazitovat	k5eAaImIp3nP	parazitovat
na	na	k7c6	na
řasách	řasa	k1gFnPc6	řasa
(	(	kIx(	(
<g/>
strategie	strategie	k1gFnSc1	strategie
zvaná	zvaný	k2eAgFnSc1d1	zvaná
algikolie	algikolie	k1gFnSc1	algikolie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
typ	typ	k1gInSc4	typ
"	"	kIx"	"
<g/>
lišejníku	lišejník	k1gInSc2	lišejník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
popsán	popsat	k5eAaPmNgInS	popsat
již	již	k9	již
před	před	k7c4	před
sto	sto	k4xCgNnSc4	sto
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
stélka	stélka	k1gFnSc1	stélka
i	i	k8xC	i
plodnice	plodnice	k1gFnPc1	plodnice
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nenápadné	nápadný	k2eNgFnPc1d1	nenápadná
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
řasy	řasa	k1gFnPc1	řasa
<g/>
.	.	kIx.	.
<g/>
Slizoblanka	Slizoblanka	k1gFnSc1	Slizoblanka
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
popisována	popisován	k2eAgFnSc1d1	popisována
z	z	k7c2	z
Alp	Alpy	k1gFnPc2	Alpy
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
Epigloea	Epigloe	k2eAgFnSc1d1	Epigloe
bactrospora	bactrospora	k1gFnSc1	bactrospora
<g/>
,	,	kIx,	,
E.	E.	kA	E.
pleiospora	pleiospora	k1gFnSc1	pleiospora
a	a	k8xC	a
E.	E.	kA	E.
soleiformis	soleiformis	k1gInSc1	soleiformis
<g/>
.	.	kIx.	.
</s>
<s>
Slizoblanka	Slizoblanka	k1gFnSc1	Slizoblanka
prostřední	prostřední	k2eAgFnSc1d1	prostřední
(	(	kIx(	(
<g/>
Epigloea	Epigloe	k2eAgFnSc1d1	Epigloe
medioincrassata	medioincrassata	k1gFnSc1	medioincrassata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
