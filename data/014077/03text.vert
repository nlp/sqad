<s>
Rhenium	rhenium	k1gNnSc1
</s>
<s>
Rhenium	rhenium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
5	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Re	re	kA
</s>
<s>
75	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Rhenium	rhenium	k1gNnSc1
<g/>
,	,	kIx,
Re	re	k9
<g/>
,	,	kIx,
75	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Rhenium	rhenium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
šedobílý	šedobílý	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-15-5	7440-15-5	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
186,207	186,207	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
137	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
151	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
5	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
V	V	kA
<g/>
,	,	kIx,
VI	VI	kA
<g/>
,	,	kIx,
VII	VII	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,9	1,9	k4
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Hexagonální	hexagonální	k2eAgFnSc1d1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
21,02	21,02	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
(	(	kIx(
<g/>
18,9	18,9	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
při	při	k7c6
teplotě	teplota	k1gFnSc6
tání	tání	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
7,0	7,0	k4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
4	#num#	k4
700	#num#	k4
m	m	kA
<g/>
·	·	k?
<g/>
s	s	k7c7
<g/>
−	−	k?
<g/>
1	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
48,0	48,0	k4
W	W	kA
<g/>
·	·	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
·	·	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
3	#num#	k4
185,85	185,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
459	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
5	#num#	k4
595,85	595,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
5	#num#	k4
869	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
193	#num#	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetické	paramagnetický	k2eAgNnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
5	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
neutrony	neutron	k1gInPc7
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
neutrony	neutron	k1gInPc7
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
neutrony	neutron	k1gInPc7
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tc	tc	k0
<g/>
⋏	⋏	k?
</s>
<s>
Wolfram	wolfram	k1gInSc4
≺	≺	k?
<g/>
Re	re	k9
<g/>
≻	≻	k?
Osmium	osmium	k1gNnSc1
</s>
<s>
⋎	⋎	k?
<g/>
Bh	Bh	k1gFnSc1
</s>
<s>
Rhenium	rhenium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Re	re	k9
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Rhenium	rhenium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
vzácný	vzácný	k2eAgInSc4d1
<g/>
,	,	kIx,
těžký	těžký	k2eAgInSc4d1
a	a	k8xC
tvrdý	tvrdý	k2eAgInSc4d1
<g/>
,	,	kIx,
odolný	odolný	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
bodem	bod	k1gInSc7
tání	tání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Rhenium	rhenium	k1gNnSc1
patří	patřit	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
uhlíkem	uhlík	k1gInSc7
a	a	k8xC
wolframem	wolfram	k1gInSc7
mezi	mezi	k7c4
nejobtížněji	obtížně	k6eAd3
tavitelné	tavitelný	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemicky	chemicky	k6eAd1
se	se	k3xPyFc4
podobá	podobat	k5eAaImIp3nS
vzácným	vzácný	k2eAgInPc3d1
kovům	kov	k1gInPc3
skupiny	skupina	k1gFnSc2
platiny	platina	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
svého	svůj	k3xOyFgInSc2
nízkého	nízký	k2eAgInSc2d1
výskytu	výskyt	k1gInSc2
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
řadě	řada	k1gFnSc6
mocenství	mocenství	k1gNnPc2
od	od	k7c2
Re	re	k9
<g/>
−	−	k?
<g/>
1	#num#	k4
po	po	k7c4
Re	re	k9
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstálejší	stálý	k2eAgInPc1d3
jsou	být	k5eAaImIp3nP
však	však	k9
sloučeniny	sloučenina	k1gFnPc1
rhenia	rhenium	k1gNnSc2
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Re	re	k9
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Re	re	k9
<g/>
+	+	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
Re	re	k9
<g/>
+	+	kIx~
<g/>
6	#num#	k4
a	a	k8xC
Re	re	k9
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Objev	objev	k1gInSc1
rhenia	rhenium	k1gNnSc2
je	být	k5eAaImIp3nS
datován	datovat	k5eAaImNgInS
rokem	rok	k1gInSc7
1925	#num#	k4
a	a	k8xC
jako	jako	k9
jeho	jeho	k3xOp3gMnPc1
objevitelé	objevitel	k1gMnPc1
jsou	být	k5eAaImIp3nP
označováni	označovat	k5eAaImNgMnP
Walter	Walter	k1gMnSc1
Noddack	Noddack	k1gMnSc1
<g/>
,	,	kIx,
Ida	Ida	k1gFnSc1
Tackeová	Tackeová	k1gFnSc1
a	a	k8xC
Otto	Otto	k1gMnSc1
C.	C.	kA
Berg	Berg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
výroba	výroba	k1gFnSc1
</s>
<s>
Rhenium	rhenium	k1gNnSc1
je	být	k5eAaImIp3nS
prvkem	prvek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
mimořádně	mimořádně	k6eAd1
nízkým	nízký	k2eAgInSc7d1
výskytem	výskyt	k1gInSc7
na	na	k7c6
Zemi	zem	k1gFnSc6
i	i	k8xC
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
činí	činit	k5eAaImIp3nS
průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
rhenia	rhenium	k1gNnSc2
pouze	pouze	k6eAd1
kolem	kolem	k7c2
1	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
ng	ng	k?
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
koncentrace	koncentrace	k1gFnSc1
neměřitelně	měřitelně	k6eNd1
nízká	nízký	k2eAgFnSc1d1
i	i	k8xC
nejcitlivějšími	citlivý	k2eAgFnPc7d3
analytickými	analytický	k2eAgFnPc7d1
technikami	technika	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
rhenia	rhenium	k1gNnSc2
kolem	kolem	k7c2
1	#num#	k4
bilionu	bilion	k4xCgInSc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
nesetkáme	setkat	k5eNaPmIp1nP
s	s	k7c7
minerály	minerál	k1gInPc7
nebo	nebo	k8xC
rudami	ruda	k1gFnPc7
čistého	čistý	k2eAgNnSc2d1
rhenia	rhenium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získává	získávat	k5eAaImIp3nS
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
rafinací	rafinace	k1gFnSc7
polétavých	polétavý	k2eAgInPc2d1
prachů	prach	k1gInPc2
<g/>
,	,	kIx,
vznikajících	vznikající	k2eAgInPc2d1
při	při	k7c6
úpravě	úprava	k1gFnSc6
molybdenových	molybdenový	k2eAgInPc2d1
koncentrátů	koncentrát	k1gInPc2
<g/>
,	,	kIx,
získávaných	získávaný	k2eAgInPc2d1
z	z	k7c2
původních	původní	k2eAgFnPc2d1
sulfidických	sulfidický	k2eAgFnPc2d1
měděných	měděný	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
zásoby	zásoba	k1gFnPc1
rhenia	rhenium	k1gNnSc2
jsou	být	k5eAaImIp3nP
na	na	k7c6
ostrově	ostrov	k1gInSc6
Iturupu	Iturup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vysoce	vysoce	k6eAd1
čisté	čistý	k2eAgNnSc1d1
rhenium	rhenium	k1gNnSc1
je	být	k5eAaImIp3nS
připravováno	připravovat	k5eAaImNgNnS
termickým	termický	k2eAgInSc7d1
rozkladem	rozklad	k1gInSc7
rhenistanu	rhenistan	k1gInSc2
amonného	amonný	k2eAgInSc2d1
NH	NH	kA
<g/>
4	#num#	k4
<g/>
ReO	Rea	k1gFnSc5
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
rhenium	rhenium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
rhenium	rhenium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4177995-2	4177995-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5774	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85113607	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85113607	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
