<s>
Lakomec	lakomec	k1gMnSc1	lakomec
(	(	kIx(	(
<g/>
Francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
L	L	kA	L
<g/>
'	'	kIx"	'
Avare	Avar	k1gMnSc5	Avar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
pěti	pět	k4xCc6	pět
dějstvích	dějství	k1gNnPc6	dějství
francouzského	francouzský	k2eAgInSc2d1	francouzský
herce	herc	k1gInSc2	herc
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
dramatika	dramatik	k1gMnSc2	dramatik
Moliéra	Moliér	k1gMnSc2	Moliér
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
roku	rok	k1gInSc2	rok
1668	[number]	k4	1668
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
kritické	kritický	k2eAgFnSc2d1	kritická
komedie	komedie	k1gFnSc2	komedie
je	být	k5eAaImIp3nS	být
šedesátiletý	šedesátiletý	k2eAgInSc1d1	šedesátiletý
Harpagon	Harpagon	k1gInSc1	Harpagon
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
harpago	harpago	k6eAd1	harpago
=	=	kIx~	=
loupit	loupit	k5eAaImF	loupit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vdovec	vdovec	k1gMnSc1	vdovec
<g/>
,	,	kIx,	,
lichvář	lichvář	k1gMnSc1	lichvář
a	a	k8xC	a
necitelný	citelný	k2eNgMnSc1d1	necitelný
60	[number]	k4	60
<g/>
letý	letý	k2eAgInSc1d1	letý
lakomec	lakomec	k1gMnSc1	lakomec
<g/>
.	.	kIx.	.
</s>
<s>
Harpagon	Harpagon	k1gMnSc1	Harpagon
je	být	k5eAaImIp3nS	být
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
peníze	peníz	k1gInPc4	peníz
schopen	schopen	k2eAgMnSc1d1	schopen
obětovat	obětovat	k5eAaBmF	obětovat
vše	všechen	k3xTgNnSc4	všechen
–	–	k?	–
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
jeho	jeho	k3xOp3gNnSc2	jeho
bohatství	bohatství	k1gNnSc2	bohatství
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
představuje	představovat	k5eAaImIp3nS	představovat
ztrátu	ztráta	k1gFnSc4	ztráta
smyslu	smysl	k1gInSc2	smysl
bytí	bytí	k1gNnSc2	bytí
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
zdravého	zdravý	k2eAgInSc2d1	zdravý
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
tragikomická	tragikomický	k2eAgFnSc1d1	tragikomická
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
Moliérův	Moliérův	k2eAgInSc4d1	Moliérův
důkaz	důkaz	k1gInSc4	důkaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc1	peníz
přirozeně	přirozeně	k6eAd1	přirozeně
deformují	deformovat	k5eAaImIp3nP	deformovat
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
mezilidské	mezilidský	k2eAgInPc4d1	mezilidský
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
plné	plný	k2eAgNnSc1d1	plné
komediálních	komediální	k2eAgFnPc2d1	komediální
a	a	k8xC	a
důvtipných	důvtipný	k2eAgFnPc2d1	důvtipná
zápletek	zápletka	k1gFnPc2	zápletka
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úsměvným	úsměvný	k2eAgNnPc3d1	úsměvné
nedorozuměním	nedorozumění	k1gNnPc3	nedorozumění
a	a	k8xC	a
hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
detailně	detailně	k6eAd1	detailně
charakterizovány	charakterizován	k2eAgFnPc1d1	charakterizována
<g/>
.	.	kIx.	.
</s>
<s>
Harpagon	Harpagon	k1gInSc1	Harpagon
–	–	k?	–
Moliére	Moliér	k1gMnSc5	Moliér
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
obraz	obraz	k1gInSc4	obraz
lichváře	lichvář	k1gMnSc2	lichvář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
peníze	peníz	k1gInPc4	peníz
ochoten	ochoten	k2eAgMnSc1d1	ochoten
obětovat	obětovat	k5eAaBmF	obětovat
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
.	.	kIx.	.
</s>
<s>
Citovými	citový	k2eAgInPc7d1	citový
vztahy	vztah	k1gInPc7	vztah
počínaje	počínaje	k7c7	počínaje
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc7d1	vlastní
rodinou	rodina	k1gFnSc7	rodina
konče	konče	k7c7	konče
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
peněz	peníze	k1gInPc2	peníze
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znamená	znamenat	k5eAaImIp3nS	znamenat
naprostou	naprostý	k2eAgFnSc4d1	naprostá
tragédii	tragédie	k1gFnSc4	tragédie
<g/>
,	,	kIx,	,
nenávist	nenávist	k1gFnSc1	nenávist
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
živému	živé	k1gNnSc3	živé
<g/>
,	,	kIx,	,
nechuť	nechuť	k1gFnSc1	nechuť
k	k	k7c3	k
životu	život	k1gInSc3	život
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
smyslu	smysl	k1gInSc2	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Čipera	čipera	k1gFnSc1	čipera
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Fleche	Fleche	k1gFnSc1	Fleche
<g/>
,	,	kIx,	,
Šindel	šindel	k1gInSc1	šindel
<g/>
,	,	kIx,	,
Štika	štika	k1gFnSc1	štika
<g/>
)	)	kIx)	)
–	–	k?	–
Je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
oddaný	oddaný	k2eAgMnSc1d1	oddaný
svému	svůj	k3xOyFgMnSc3	svůj
pánu	pán	k1gMnSc3	pán
Kleantovi	Kleant	k1gMnSc3	Kleant
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
svému	svůj	k3xOyFgMnSc3	svůj
pánovi	pán	k1gMnSc3	pán
pomůže	pomoct	k5eAaPmIp3nS	pomoct
<g/>
,	,	kIx,	,
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
Harpagonovi	Harpagon	k1gMnSc3	Harpagon
jeho	jeho	k3xOp3gFnSc4	jeho
skříňku	skříňka	k1gFnSc4	skříňka
s	s	k7c7	s
penězi	peníze	k1gInPc7	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Kleantes	Kleantes	k1gMnSc1	Kleantes
–	–	k?	–
Harpagonův	Harpagonův	k2eAgMnSc1d1	Harpagonův
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Mariany	Mariana	k1gFnSc2	Mariana
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
chudá	chudý	k2eAgFnSc1d1	chudá
<g/>
.	.	kIx.	.
</s>
<s>
Šílená	šílený	k2eAgFnSc1d1	šílená
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
penězům	peníze	k1gInPc3	peníze
jeho	jeho	k3xOp3gFnSc2	jeho
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
příčí	příčit	k5eAaImIp3nS	příčit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jeho	jeho	k3xOp3gInSc7	jeho
pravým	pravý	k2eAgInSc7d1	pravý
opakem	opak	k1gInSc7	opak
<g/>
.	.	kIx.	.
</s>
<s>
Mariana	Mariana	k1gFnSc1	Mariana
–	–	k?	–
Chudá	chudý	k2eAgFnSc1d1	chudá
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
nemocnou	mocný	k2eNgFnSc4d1	mocný
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Kleanta	Kleant	k1gMnSc2	Kleant
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInPc4	jeho
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
povaze	povaha	k1gFnSc3	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Valér	valér	k1gInSc1	valér
–	–	k?	–
sluha	sluha	k1gMnSc1	sluha
Harpagona	Harpagon	k1gMnSc2	Harpagon
<g/>
;	;	kIx,	;
zamilovaný	zamilovaný	k1gMnSc1	zamilovaný
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
Elišky	Eliška	k1gFnSc2	Eliška
<g/>
.	.	kIx.	.
</s>
<s>
Eliška	Eliška	k1gFnSc1	Eliška
–	–	k?	–
dcera	dcera	k1gFnSc1	dcera
Harpagona	Harpagona	k1gFnSc1	Harpagona
<g/>
;	;	kIx,	;
zamilovaná	zamilovaná	k1gFnSc1	zamilovaná
do	do	k7c2	do
Valéra	Valér	k1gMnSc2	Valér
<g/>
.	.	kIx.	.
</s>
<s>
Anselm	Anselm	k1gMnSc1	Anselm
–	–	k?	–
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
si	se	k3xPyFc3	se
vzít	vzít	k5eAaPmF	vzít
Elišku	Eliška	k1gFnSc4	Eliška
proti	proti	k7c3	proti
její	její	k3xOp3gFnSc3	její
vůli	vůle	k1gFnSc3	vůle
<g/>
;	;	kIx,	;
otec	otec	k1gMnSc1	otec
Valéra	Valér	k1gMnSc2	Valér
a	a	k8xC	a
Mariany	Mariana	k1gFnSc2	Mariana
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
hry	hra	k1gFnSc2	hra
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
–	–	k?	–
Harpagonův	Harpagonův	k2eAgMnSc1d1	Harpagonův
kočí	kočí	k1gMnSc1	kočí
a	a	k8xC	a
kuchař	kuchař	k1gMnSc1	kuchař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
falešně	falešně	k6eAd1	falešně
udá	udat	k5eAaPmIp3nS	udat
Valéra	Valéra	k1gFnSc1	Valéra
za	za	k7c4	za
krádež	krádež	k1gFnSc4	krádež
Harpagonova	Harpagonův	k2eAgInSc2d1	Harpagonův
pokladu	poklad	k1gInSc2	poklad
Frosina	Frosina	k1gFnSc1	Frosina
–	–	k?	–
všetečná	všetečný	k2eAgFnSc1d1	všetečná
rádkyně	rádkyně	k1gFnSc1	rádkyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
mazaností	mazanost	k1gFnSc7	mazanost
pomohla	pomoct	k5eAaPmAgFnS	pomoct
nejdříve	dříve	k6eAd3	dříve
Harpagonovi	Harpagon	k1gMnSc3	Harpagon
k	k	k7c3	k
Marianě	Mariana	k1gFnSc3	Mariana
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
Kleantovi	Kleantův	k2eAgMnPc1d1	Kleantův
Děj	děj	k1gInSc1	děj
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
<g/>
.	.	kIx.	.
</s>
<s>
Harpagon	Harpagon	k1gInSc1	Harpagon
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
neuvěřitelný	uvěřitelný	k2eNgMnSc1d1	neuvěřitelný
lakomec	lakomec	k1gMnSc1	lakomec
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
každou	každý	k3xTgFnSc4	každý
návštěvu	návštěva	k1gFnSc4	návštěva
podezírá	podezírat	k5eAaImIp3nS	podezírat
z	z	k7c2	z
krádeže	krádež	k1gFnSc2	krádež
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
skutečně	skutečně	k6eAd1	skutečně
nic	nic	k3yNnSc1	nic
neukradla	ukradnout	k5eNaPmAgFnS	ukradnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
omezuje	omezovat	k5eAaImIp3nS	omezovat
jakbysmet	jakbysmet	k?	jakbysmet
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Elišku	Eliška	k1gFnSc4	Eliška
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Kleanta	Kleant	k1gMnSc4	Kleant
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
nucen	nutit	k5eAaImNgMnS	nutit
si	se	k3xPyFc3	se
půjčovat	půjčovat	k5eAaImF	půjčovat
peníze	peníz	k1gInPc4	peníz
všade	všade	k?	všade
odjinud	odjinud	k6eAd1	odjinud
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ne	ne	k9	ne
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Harpagonova	Harpagonův	k2eAgFnSc1d1	Harpagonova
zlotřilá	zlotřilý	k2eAgFnSc1d1	zlotřilá
povaha	povaha	k1gFnSc1	povaha
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
ještě	ještě	k6eAd1	ještě
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
intrikuje	intrikovat	k5eAaImIp3nS	intrikovat
s	s	k7c7	s
životy	život	k1gInPc4	život
svých	svůj	k3xOyFgMnPc2	svůj
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Kleantes	Kleantes	k1gMnSc1	Kleantes
je	být	k5eAaImIp3nS	být
zamilován	zamilován	k2eAgMnSc1d1	zamilován
do	do	k7c2	do
chudé	chudý	k2eAgFnSc2d1	chudá
dívky	dívka	k1gFnSc2	dívka
Mariany	Mariana	k1gFnSc2	Mariana
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
otce	otec	k1gMnSc4	otec
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
svolení	svolení	k1gNnSc4	svolení
ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tu	tu	k6eAd1	tu
otec	otec	k1gMnSc1	otec
syna	syn	k1gMnSc2	syn
překvapí	překvapit	k5eAaPmIp3nS	překvapit
výrokem	výrok	k1gInSc7	výrok
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
si	se	k3xPyFc3	se
Marianu	Mariana	k1gFnSc4	Mariana
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
!	!	kIx.	!
</s>
<s>
Zato	zato	k6eAd1	zato
svého	svůj	k3xOyFgMnSc4	svůj
Kleanta	Kleant	k1gMnSc4	Kleant
by	by	kYmCp3nS	by
rád	rád	k6eAd1	rád
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
vdovou	vdova	k1gFnSc7	vdova
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Elišku	Eliška	k1gFnSc4	Eliška
<g/>
,	,	kIx,	,
po	po	k7c4	po
uši	ucho	k1gNnPc4	ucho
zamilovanou	zamilovaná	k1gFnSc7	zamilovaná
do	do	k7c2	do
Valéra	Valér	k1gMnSc2	Valér
<g/>
,	,	kIx,	,
zase	zase	k9	zase
provdal	provdat	k5eAaPmAgMnS	provdat
za	za	k7c4	za
stárnoucího	stárnoucí	k2eAgMnSc4d1	stárnoucí
boháče	boháč	k1gMnSc4	boháč
Anselma	Anselm	k1gMnSc4	Anselm
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpoledne	odpoledne	k1gNnSc1	odpoledne
nalezne	nalézt	k5eAaBmIp3nS	nalézt
Kleantův	Kleantův	k2eAgMnSc1d1	Kleantův
sluha	sluha	k1gMnSc1	sluha
Čipera	Čipera	k1gMnSc1	Čipera
poklad	poklad	k1gInSc1	poklad
Harpagonem	Harpagon	k1gInSc7	Harpagon
zakopaný	zakopaný	k2eAgInSc1d1	zakopaný
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
a	a	k8xC	a
uzme	uzmout	k5eAaPmIp3nS	uzmout
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
Kleantovi	Kleant	k1gMnSc3	Kleant
pomohl	pomoct	k5eAaPmAgMnS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Odcizený	odcizený	k2eAgInSc4d1	odcizený
majetek	majetek	k1gInSc4	majetek
v	v	k7c6	v
Harpagonovi	Harpagon	k1gMnSc6	Harpagon
probudí	probudit	k5eAaPmIp3nS	probudit
šílenství	šílenství	k1gNnSc4	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
zběsile	zběsile	k6eAd1	zběsile
pátrat	pátrat	k5eAaImF	pátrat
a	a	k8xC	a
podezřívat	podezřívat	k5eAaImF	podezřívat
každého	každý	k3xTgMnSc4	každý
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
nepřestane	přestat	k5eNaPmIp3nS	přestat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nedozví	dozvědět	k5eNaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Valér	valér	k1gInSc1	valér
se	se	k3xPyFc4	se
zasnoubil	zasnoubit	k5eAaPmAgInS	zasnoubit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
dcerou	dcera	k1gFnSc7	dcera
–	–	k?	–
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
přetéká	přetékat	k5eAaImIp3nS	přetékat
a	a	k8xC	a
rudne	rudnout	k5eAaImIp3nS	rudnout
vzteky	vztek	k1gInPc4	vztek
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Valéra	Valéra	k1gFnSc1	Valéra
oběsí	oběsit	k5eAaPmIp3nS	oběsit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
raději	rád	k6eAd2	rád
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
Anselm	Anselm	k1gMnSc1	Anselm
–	–	k?	–
tedy	tedy	k8xC	tedy
vážený	vážený	k2eAgMnSc1d1	vážený
šlechtic	šlechtic	k1gMnSc1	šlechtic
a	a	k8xC	a
bohatý	bohatý	k2eAgMnSc1d1	bohatý
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
Mariana	Mariana	k1gFnSc1	Mariana
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
Anselmova	Anselmův	k2eAgFnSc1d1	Anselmova
dcera	dcera	k1gFnSc1	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Kleantes	Kleantes	k1gInSc4	Kleantes
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc1	peníz
vrátí	vrátit	k5eAaPmIp3nP	vrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
-li	i	k?	-li
Marianu	Mariana	k1gFnSc4	Mariana
<g/>
.	.	kIx.	.
</s>
<s>
Harpagon	Harpagon	k1gInSc1	Harpagon
tak	tak	k9	tak
obětuje	obětovat	k5eAaBmIp3nS	obětovat
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
jen	jen	k9	jen
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgInS	získat
své	svůj	k3xOyFgInPc4	svůj
peníze	peníz	k1gInPc4	peníz
zpět	zpět	k6eAd1	zpět
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gFnSc1	jejich
ztráta	ztráta	k1gFnSc1	ztráta
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znamenala	znamenat	k5eAaImAgFnS	znamenat
konec	konec	k1gInSc1	konec
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
Mariany	Mariana	k1gFnSc2	Mariana
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
pozná	poznat	k5eAaPmIp3nS	poznat
pravý	pravý	k2eAgInSc4d1	pravý
původ	původ	k1gInSc4	původ
Mariany	Mariana	k1gFnSc2	Mariana
a	a	k8xC	a
Valéra	Valéra	k1gFnSc1	Valéra
a	a	k8xC	a
pochopí	pochopit	k5eAaPmIp3nS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vdavky	vdavka	k1gFnPc1	vdavka
jej	on	k3xPp3gInSc2	on
nebudou	být	k5eNaImBp3nP	být
stát	stát	k5eAaImF	stát
jedinou	jediný	k2eAgFnSc4d1	jediná
minci	mince	k1gFnSc4	mince
a	a	k8xC	a
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
taktéž	taktéž	k?	taktéž
dávat	dávat	k5eAaImF	dávat
žádné	žádný	k3yNgNnSc1	žádný
věno	věno	k1gNnSc1	věno
nemusí	muset	k5eNaImIp3nS	muset
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
svatbě	svatba	k1gFnSc3	svatba
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
najednou	najednou	k6eAd1	najednou
nic	nic	k3yNnSc1	nic
nenamítá	namítat	k5eNaImIp3nS	namítat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Valér	valér	k1gInSc1	valér
ožení	oženit	k5eAaPmIp3nS	oženit
s	s	k7c7	s
Eliškou	Eliška	k1gFnSc7	Eliška
a	a	k8xC	a
Kleantes	Kleantes	k1gMnSc1	Kleantes
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
Marianu	Mariana	k1gFnSc4	Mariana
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Moliére	Moliér	k1gMnSc5	Moliér
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
malomešťáctví	malomešťáctví	k1gNnSc1	malomešťáctví
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
typ	typ	k1gInSc1	typ
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něhož	jenž	k3xRgNnSc4	jenž
peníze	peníz	k1gInPc1	peníz
znamenají	znamenat	k5eAaImIp3nP	znamenat
všechno	všechen	k3xTgNnSc4	všechen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
obětovat	obětovat	k5eAaBmF	obětovat
i	i	k8xC	i
lásku	láska	k1gFnSc4	láska
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
peněz	peníze	k1gInPc2	peníze
si	se	k3xPyFc3	se
ani	ani	k8xC	ani
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
nedovede	dovést	k5eNaPmIp3nS	dovést
představit	představit	k5eAaPmF	představit
<g/>
.	.	kIx.	.
</s>
<s>
Raději	rád	k6eAd2	rád
umře	umřít	k5eAaPmIp3nS	umřít
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
předstíral	předstírat	k5eAaImAgMnS	předstírat
pokrytecké	pokrytecký	k2eAgNnSc4d1	pokrytecké
svatouškovství	svatouškovství	k1gNnSc4	svatouškovství
–	–	k?	–
míří	mířit	k5eAaImIp3nS	mířit
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
špiclovský	špiclovský	k2eAgInSc4d1	špiclovský
systém	systém	k1gInSc4	systém
až	až	k9	až
do	do	k7c2	do
rodin	rodina	k1gFnPc2	rodina
–	–	k?	–
pokrytectví	pokrytectví	k1gNnSc4	pokrytectví
a	a	k8xC	a
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
dědictví	dědictví	k1gNnSc6	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálnost	aktuálnost	k1gFnSc1	aktuálnost
díla	dílo	k1gNnSc2	dílo
<g/>
:	:	kIx,	:
Moliérova	Moliérův	k2eAgNnPc1d1	Moliérovo
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
svou	svůj	k3xOyFgFnSc7	svůj
tematikou	tematika	k1gFnSc7	tematika
velmi	velmi	k6eAd1	velmi
aktuální	aktuální	k2eAgFnSc1d1	aktuální
a	a	k8xC	a
nadčasová	nadčasový	k2eAgFnSc1d1	nadčasová
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
době	doba	k1gFnSc6	doba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známa	známo	k1gNnPc4	známo
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
světových	světový	k2eAgInPc2d1	světový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
i	i	k8xC	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
otevřeně	otevřeně	k6eAd1	otevřeně
projeven	projeven	k2eAgInSc1d1	projeven
demokratický	demokratický	k2eAgInSc1d1	demokratický
a	a	k8xC	a
kritický	kritický	k2eAgInSc1d1	kritický
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
Moliérových	Moliérův	k2eAgFnPc2d1	Moliérova
her	hra	k1gFnPc2	hra
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
.	.	kIx.	.
</s>
<s>
Inspirace	inspirace	k1gFnSc1	inspirace
<g/>
:	:	kIx,	:
Moliére	Moliér	k1gMnSc5	Moliér
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
pro	pro	k7c4	pro
napsání	napsání	k1gNnSc4	napsání
inspirovat	inspirovat	k5eAaBmF	inspirovat
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zrovna	zrovna	k6eAd1	zrovna
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
převládala	převládat	k5eAaImAgFnS	převládat
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1668	[number]	k4	1668
rychle	rychle	k6eAd1	rychle
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
a	a	k8xC	a
zabrala	zabrat	k5eAaPmAgFnS	zabrat
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Lakomec	lakomec	k1gMnSc1	lakomec
je	být	k5eAaImIp3nS	být
také	také	k9	také
značně	značně	k6eAd1	značně
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
dílem	dílo	k1gNnSc7	dílo
Komedie	komedie	k1gFnSc2	komedie
o	o	k7c6	o
hrnci	hrnec	k1gInSc6	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1868	[number]	k4	1868
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
a	a	k8xC	a
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohdan	Bohdan	k1gMnSc1	Bohdan
Kaminský	Kaminský	k2eAgMnSc1d1	Kaminský
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Hry	hra	k1gFnPc1	hra
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1955	[number]	k4	1955
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnPc4	hra
Jiří	Jiří	k1gMnSc1	Jiří
Dudek	Dudek	k1gMnSc1	Dudek
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Prasečkova	Prasečkův	k2eAgInSc2d1	Prasečkův
a	a	k8xC	a
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Erich	Erich	k1gMnSc1	Erich
Adolf	Adolf	k1gMnSc1	Adolf
Saudek	Saudek	k1gMnSc1	Saudek
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Misantrop	misantrop	k1gMnSc1	misantrop
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
<g />
.	.	kIx.	.
</s>
<s>
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
J.	J.	kA	J.
Z.	Z.	kA	Z.
Novák	Novák	k1gMnSc1	Novák
a	a	k8xC	a
František	František	k1gMnSc1	František
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
a	a	k8xC	a
Erich	Erich	k1gMnSc1	Erich
Adolf	Adolf	k1gMnSc1	Adolf
Saudek	Saudek	k1gMnSc1	Saudek
Lakomec	lakomec	k1gMnSc1	lakomec
<g/>
,	,	kIx,	,
Artur	Artur	k1gMnSc1	Artur
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
Lakomec	lakomec	k1gMnSc1	lakomec
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
Avare	Avar	k1gMnSc5	Avar
<g/>
)	)	kIx)	)
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
komedie	komedie	k1gFnSc1	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jean	Jean	k1gMnSc1	Jean
Girault	Girault	k1gMnSc1	Girault
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
de	de	k?	de
Funè	Funè	k1gMnSc1	Funè
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Louis	louis	k1gInSc1	louis
de	de	k?	de
Funè	Funè	k1gMnSc5	Funè
<g/>
,	,	kIx,	,
Claude	Claud	k1gMnSc5	Claud
Gensac	Gensac	k1gInSc1	Gensac
<g/>
,	,	kIx,	,
Michel	Michel	k1gInSc1	Michel
Galabru	Galabr	k1gInSc2	Galabr
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
Lakomec	lakomec	k1gMnSc1	lakomec
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
Avaro	Avaro	k1gNnSc1	Avaro
<g/>
)	)	kIx)	)
Italsko	Italsko	k1gNnSc1	Italsko
<g/>
,	,	kIx,	,
Francouzsko	Francouzsko	k1gNnSc1	Francouzsko
<g/>
,	,	kIx,	,
Španělská	španělský	k2eAgFnSc1d1	španělská
komedie	komedie	k1gFnSc1	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Tonino	Tonino	k1gNnSc1	Tonino
Cervi	Cerev	k1gFnSc3	Cerev
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Alberto	Alberta	k1gFnSc5	Alberta
Sordi	Sord	k1gMnPc1	Sord
<g/>
,	,	kIx,	,
Laura	Laura	k1gFnSc1	Laura
Antonelli	Antonelle	k1gFnSc4	Antonelle
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Lee	Lea	k1gFnSc6	Lea
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
Lakomec	lakomec	k1gMnSc1	lakomec
Česká	český	k2eAgFnSc1d1	Česká
televizní	televizní	k2eAgFnSc1d1	televizní
inscenace	inscenace	k1gFnSc1	inscenace
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Zelenka	Zelenka	k1gMnSc1	Zelenka
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Viktor	Viktor	k1gMnSc1	Viktor
Preiss	Preissa	k1gFnPc2	Preissa
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
<g/>
,	,	kIx,	,
Jiřina	Jiřina	k1gFnSc1	Jiřina
Bohdalová	Bohdalová	k1gFnSc1	Bohdalová
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Vladyka	vladyka	k1gMnSc1	vladyka
<g/>
.	.	kIx.	.
</s>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
2	[number]	k4	2
<g/>
/	/	kIx~	/
M-	M-	k1gMnPc2	M-
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
960	[number]	k4	960
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
459	[number]	k4	459
<g/>
.	.	kIx.	.
</s>
<s>
PRAŽÁKOVÁ	Pražáková	k1gFnSc1	Pražáková
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
včerejška	včerejšek	k1gInSc2	včerejšek
k	k	k7c3	k
zítřku	zítřek	k1gInSc3	zítřek
:	:	kIx,	:
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
dramatě	drama	k1gNnSc6	drama
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Latinská	latinský	k2eAgFnSc1d1	Latinská
předloha	předloha	k1gFnSc1	předloha
Moliérova	Moliérův	k2eAgMnSc2d1	Moliérův
Lakomce	lakomec	k1gMnSc2	lakomec
<g/>
,	,	kIx,	,
s.	s.	k?	s.
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lakomec	lakomec	k1gMnSc1	lakomec
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Lakomec	lakomec	k1gMnSc1	lakomec
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Lakomec	lakomec	k1gMnSc1	lakomec
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
