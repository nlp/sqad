<s>
Pád	Pád	k1gInSc1
Konstantinopole	Konstantinopol	k1gInSc2
do	do	k7c2
rukou	ruka	k1gFnPc2
Osmanských	osmanský	k2eAgInPc2d1
Turků	turek	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c4
nějž	jenž	k3xRgMnSc4
vyústilo	vyústit	k5eAaPmAgNnS
obležení	obležení	k1gNnSc1
z	z	k7c2
roku	rok	k1gInSc2
1453	#num#	k4
(	(	kIx(
<g/>
trvalo	trvat	k5eAaImAgNnS
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
definitivní	definitivní	k2eAgInSc1d1
zánik	zánik	k1gInSc1
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
okamžik	okamžik	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
stala	stát	k5eAaPmAgFnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
velmocí	velmoc	k1gFnPc2
ovládající	ovládající	k2eAgInSc1d1
Balkán	Balkán	k1gInSc1
a	a	k8xC
východní	východní	k2eAgNnSc1d1
Středomoří	středomoří	k1gNnSc1
<g/>
.	.	kIx.
</s>