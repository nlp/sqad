<s>
Pád	Pád	k1gInSc1
Konstantinopole	Konstantinopol	k1gInSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
dobytí	dobytí	k1gNnSc4
Konstantinopole	Konstantinopol	k1gInSc2
Turky	turek	k1gInPc4
v	v	k7c6
roce	rok	k1gInSc6
1453	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Obléhání	obléhání	k1gNnSc2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dobytí	dobytí	k1gNnSc1
Konstantinopole	Konstantinopol	k1gInSc2
Osmanskou	osmanský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Byzantsko-osmanské	byzantsko-osmanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Vstup	vstup	k1gInSc1
Mehmeda	Mehmed	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
Fausto	Fausta	k1gMnSc5
Zonaro	Zonara	k1gFnSc5
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1453	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Konstantinopol	Konstantinopol	k1gInSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Istanbul	Istanbul	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
definitivní	definitivní	k2eAgInSc1d1
zánik	zánik	k1gInSc1
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
</s>
<s>
Morejský	Morejský	k2eAgInSc4d1
despotát	despotát	k1gInSc4
a	a	k8xC
Trapezuntské	trapezuntský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
pokračují	pokračovat	k5eAaImIp3nP
v	v	k7c6
odkazu	odkaz	k1gInSc6
Byzance	Byzanc	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
do	do	k7c2
roku	rok	k1gInSc2
1460	#num#	k4
a	a	k8xC
1461	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
sami	sám	k3xTgMnPc1
Turky	Turek	k1gMnPc4
dobyti	dobyt	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Osmanští	osmanský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
obsazují	obsazovat	k5eAaImIp3nP
poslední	poslední	k2eAgInPc4d1
zbytky	zbytek	k1gInPc4
byzantského	byzantský	k2eAgNnSc2d1
území	území	k1gNnSc2
a	a	k8xC
z	z	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
nové	nový	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
sultanátu	sultanát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Osmanský	osmanský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
Srbský	srbský	k2eAgInSc1d1
despotát	despotát	k1gInSc4
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
podpora	podpora	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
janovští	janovský	k2eAgMnPc1d1
dobrovolníci	dobrovolník	k1gMnPc1
benátští	benátský	k2eAgMnPc1d1
dobrovolníci	dobrovolník	k1gMnPc1
sicilští	sicilský	k2eAgMnPc1d1
dobrovolníciPapežský	dobrovolníciPapežský	k2eAgInSc4d1
stát	stát	k1gInSc4
Papežský	papežský	k2eAgInSc4d1
stát	stát	k1gInSc4
osmanští	osmanský	k2eAgMnPc1d1
dezertéři	dezertér	k1gMnPc1
katalánští	katalánský	k2eAgMnPc1d1
dobrovolníci	dobrovolník	k1gMnPc1
anconští	anconský	k2eAgMnPc1d1
dobrovolníci	dobrovolník	k1gMnPc1
<g/>
(	(	kIx(
<g/>
aj.	aj.	kA
západní	západní	k2eAgMnPc1d1
dobrovolníci	dobrovolník	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Mehmed	Mehmed	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Çandarlı	Çandarlı	k1gMnSc1
Halil	halit	k5eAaImAgMnS
†	†	k?
Zagan	Zagan	k1gMnSc1
Paša	paša	k1gMnSc1
Sulejman	Sulejman	k1gMnSc1
Baltoghlu	Baltoghl	k1gInSc2
Karaca	Karaca	k1gMnSc1
Paša	paša	k1gMnSc1
Hamza	Hamz	k1gMnSc2
Bej	bej	k1gMnSc1
Hasan	Hasan	k1gMnSc1
z	z	k7c2
Ulubatu	Ulubat	k1gInSc2
†	†	k?
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
XI	XI	kA
<g/>
.	.	kIx.
†	†	k?
Loukas	Loukas	k1gMnSc1
Notaras	Notaras	k1gMnSc1
†	†	k?
Theophilos	Theophilos	k1gMnSc1
Palaiologos	Palaiologos	k1gMnSc1
†	†	k?
Demetrios	Demetrios	k1gInSc1
Kantakuzenos	Kantakuzenosa	k1gFnPc2
Giovanni	Giovanen	k2eAgMnPc1d1
Giustiniani	Giustinian	k1gMnPc1
†	†	k?
Maurizio	Maurizia	k1gMnSc5
Cattaneo	Cattanea	k1gMnSc5
Bartolomeo	Bartolomea	k1gMnSc5
Soligo	Soliga	k1gMnSc5
Gabriele	Gabriel	k1gMnSc5
Trevisano	Trevisana	k1gFnSc5
Girolamo	Girolama	k1gFnSc5
Minotto	Minotta	k1gFnSc5
Iacobo	Iacoba	k1gFnSc5
Contariniego	Contariniega	k1gFnSc5
Alviso	Alvisa	k1gFnSc5
Diedo	Diedo	k1gNnSc1
kardinál	kardinál	k1gMnSc1
Isidor	Isidor	k1gMnSc1
Orhan	Orhan	k1gMnSc1
Çelebi	Çeleb	k1gMnSc3
†	†	k?
Don	Don	k1gMnSc1
Francisco	Francisco	k1gMnSc1
de	de	k?
Toledo	Toledo	k1gNnSc4
†	†	k?
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
dle	dle	k7c2
posledních	poslední	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
aosmanských	aosmanský	k2eAgInPc2d1
archivů	archiv	k1gInPc2
<g/>
:	:	kIx,
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
80	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
5	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
000	#num#	k4
janičářů	janičář	k1gInPc2
</s>
<s>
asi	asi	k9
70	#num#	k4
děl	dělo	k1gNnPc2
(	(	kIx(
<g/>
14	#num#	k4
velkéhoa	velkého	k1gInSc2
56	#num#	k4
menšího	malý	k2eAgInSc2d2
kalibru	kalibr	k1gInSc2
<g/>
)	)	kIx)
<g/>
,75	,75	k4
veslic	veslice	k1gFnPc2
<g/>
,	,	kIx,
31	#num#	k4
galér	galéra	k1gFnPc2
</s>
<s>
1	#num#	k4
500	#num#	k4
jezdců	jezdec	k1gMnPc2
</s>
<s>
dobové	dobový	k2eAgInPc1d1
odhady	odhad	k1gInPc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
160	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
300	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
90	#num#	k4
<g/>
–	–	k?
<g/>
126	#num#	k4
lodí	loď	k1gFnPc2
</s>
<s>
až	až	k9
12	#num#	k4
000	#num#	k4
řeckých	řecký	k2eAgMnPc2d1
a	a	k8xC
2	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
dobrovolníků	dobrovolník	k1gMnPc2
ze	z	k7c2
Západupřevážně	Západupřevážně	k1gFnSc2
z	z	k7c2
Janova	Janov	k1gInSc2
a	a	k8xC
Benátek	Benátky	k1gFnPc2
<g/>
:	:	kIx,
7	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
000	#num#	k4
cca	cca	kA
700	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
lodí	loď	k1gFnPc2
cca	cca	kA
800	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
lodí	loď	k1gFnPc2
3	#num#	k4
krétské	krétský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
200	#num#	k4
lučistníkůod	lučistníkůoda	k1gFnPc2
kardinála	kardinál	k1gMnSc4
Isidora	Isidor	k1gMnSc4
600	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
lodí	loď	k1gFnPc2
několik	několik	k4yIc4
lodí	loď	k1gFnPc2
amužů	amuž	k1gMnPc2
neznámého	známý	k2eNgInSc2d1
počtu	počet	k1gInSc2
oddíl	oddíl	k1gInSc4
Katalánců	Katalánec	k1gMnPc2
neznámého	známý	k2eNgInSc2d1
počtu	počet	k1gInSc2
<g/>
,	,	kIx,
1	#num#	k4
loď	loď	k1gFnSc1
1	#num#	k4
anconská	anconský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
1	#num#	k4
provensálská	provensálský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
značné	značný	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
,	,	kIx,
<g/>
přesný	přesný	k2eAgInSc4d1
počet	počet	k1gInSc4
neznámý	známý	k2eNgInSc4d1
</s>
<s>
4	#num#	k4
000	#num#	k4
padlých	padlý	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
a	a	k8xC
civilistů	civilista	k1gMnPc2
<g/>
,30	,30	k4
000	#num#	k4
odvedeno	odveden	k2eAgNnSc4d1
či	či	k8xC
zotročeno	zotročen	k2eAgNnSc4d1
</s>
<s>
Pád	Pád	k1gInSc1
Konstantinopole	Konstantinopol	k1gInSc2
do	do	k7c2
rukou	ruka	k1gFnPc2
Osmanských	osmanský	k2eAgInPc2d1
Turků	turek	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c4
nějž	jenž	k3xRgMnSc4
vyústilo	vyústit	k5eAaPmAgNnS
obležení	obležení	k1gNnSc1
z	z	k7c2
roku	rok	k1gInSc2
1453	#num#	k4
(	(	kIx(
<g/>
trvalo	trvat	k5eAaImAgNnS
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
definitivní	definitivní	k2eAgInSc1d1
zánik	zánik	k1gInSc1
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
okamžik	okamžik	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
stala	stát	k5eAaPmAgFnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
velmocí	velmoc	k1gFnPc2
ovládající	ovládající	k2eAgInSc1d1
Balkán	Balkán	k1gInSc1
a	a	k8xC
východní	východní	k2eAgNnSc1d1
Středomoří	středomoří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
byla	být	k5eAaImAgFnS
poměrně	poměrně	k6eAd1
jednostrannou	jednostranný	k2eAgFnSc7d1
záležitostí	záležitost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
jen	jen	k6eAd1
tragickým	tragický	k2eAgInSc7d1
zbytkem	zbytek	k1gInSc7
kdysi	kdysi	k6eAd1
mocného	mocný	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
a	a	k8xC
byzantský	byzantský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
ovládal	ovládat	k5eAaImAgInS
pouze	pouze	k6eAd1
bezprostřední	bezprostřední	k2eAgNnSc4d1
okolí	okolí	k1gNnSc4
svého	svůj	k3xOyFgNnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
před	před	k7c7
ní	on	k3xPp3gFnSc7
de	de	k?
facto	facta	k1gFnSc5
vazalem	vazal	k1gMnSc7
tureckého	turecký	k2eAgMnSc4d1
sultána	sultán	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
(	(	kIx(
<g/>
nebo	nebo	k8xC
spíš	spíš	k9
záminkou	záminka	k1gFnSc7
<g/>
)	)	kIx)
ukončení	ukončení	k1gNnSc1
příměří	příměří	k1gNnSc2
mezi	mezi	k7c7
Osmany	Osman	k1gMnPc7
a	a	k8xC
Byzancí	Byzanc	k1gFnPc2
a	a	k8xC
finálního	finální	k2eAgInSc2d1
útoku	útok	k1gInSc2
Turků	Turek	k1gMnPc2
na	na	k7c4
město	město	k1gNnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
osmanský	osmanský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Orhan	Orhan	k1gMnSc1
Çelebi	Çeleb	k1gFnSc2
sídlící	sídlící	k2eAgMnSc1d1
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
v	v	k7c6
Byzanci	Byzanc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sultán	sultána	k1gFnPc2
Murad	Murad	k1gInSc4
II	II	kA
<g/>
.	.	kIx.
posílal	posílat	k5eAaImAgMnS
každoročně	každoročně	k6eAd1
3	#num#	k4
000	#num#	k4
zlatých	zlatá	k1gFnPc2
do	do	k7c2
Byzance	Byzanc	k1gFnSc2
pro	pro	k7c4
vydržování	vydržování	k1gNnSc4
svého	svůj	k3xOyFgNnSc2
panství	panství	k1gNnSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Strumy	struma	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
ale	ale	k9
fakticky	fakticky	k6eAd1
spravoval	spravovat	k5eAaImAgMnS
Orhan	Orhan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1440	#num#	k4
byl	být	k5eAaImAgMnS
Orhan	Orhan	k1gMnSc1
jediným	jediný	k2eAgMnSc7d1
následníkem	následník	k1gMnSc7
na	na	k7c4
osmanský	osmanský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1451	#num#	k4
po	po	k7c6
smrti	smrt	k1gFnSc6
Murada	Murada	k1gFnSc1
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
trůn	trůn	k1gInSc4
místo	místo	k7c2
něj	on	k3xPp3gMnSc4
Muradův	Muradův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Mehmed	Mehmed	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
pak	pak	k6eAd1
byzantský	byzantský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Konstantin	Konstantin	k1gMnSc1
XI	XI	kA
<g/>
.	.	kIx.
připomněl	připomnět	k5eAaPmAgMnS
dopisem	dopis	k1gInSc7
Mehmedovi	Mehmedův	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
placení	placení	k1gNnSc6
příslušné	příslušný	k2eAgFnSc2d1
sumy	suma	k1gFnSc2
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Orhan	Orhan	k1gMnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
dalších	další	k2eAgMnPc2d1
uchazečů	uchazeč	k1gMnPc2
o	o	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
Mehmed	Mehmed	k1gInSc1
to	ten	k3xDgNnSc1
pojal	pojmout	k5eAaPmAgInS
jako	jako	k9
záminku	záminka	k1gFnSc4
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
Konstantinopol	Konstantinopol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
miniatura	miniatura	k1gFnSc1
obléhání	obléhání	k1gNnSc2
Konstantinopole	Konstantinopol	k1gInSc2
z	z	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
1452	#num#	k4
dokončili	dokončit	k5eAaPmAgMnP
Turci	Turek	k1gMnPc1
výstavbu	výstavba	k1gFnSc4
Rumelijské	Rumelijský	k2eAgFnSc3d1
pevnosti	pevnost	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
Byzantinci	Byzantinec	k1gMnPc1
pokoušeli	pokoušet	k5eAaImAgMnP
několikrát	několikrát	k6eAd1
sabotovat	sabotovat	k5eAaImF
<g/>
,	,	kIx,
či	či	k8xC
minimálně	minimálně	k6eAd1
zdržet	zdržet	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
na	na	k7c4
dohled	dohled	k1gInSc4
od	od	k7c2
Zlatého	zlatý	k2eAgInSc2d1
rohu	roh	k1gInSc2
byla	být	k5eAaImAgFnS
jasným	jasný	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
neodvratnosti	neodvratnost	k1gFnSc2
ofenzívy	ofenzíva	k1gFnSc2
a	a	k8xC
obléhání	obléhání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
proto	proto	k8xC
nařídil	nařídit	k5eAaPmAgMnS
zavřít	zavřít	k5eAaPmF
brány	brána	k1gFnPc4
a	a	k8xC
připravit	připravit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
úder	úder	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstantin	Konstantin	k1gMnSc1
XI	XI	kA
<g/>
.	.	kIx.
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
vědom	vědom	k2eAgMnSc1d1
závažnosti	závažnost	k1gFnSc3
situace	situace	k1gFnSc2
a	a	k8xC
hlavně	hlavně	k9
faktu	fakt	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
již	již	k6eAd1
sama	sám	k3xTgFnSc1
Byzanc	Byzanc	k1gFnSc1
<g/>
,	,	kIx,
tvořící	tvořící	k2eAgFnSc1d1
v	v	k7c6
podstatě	podstata	k1gFnSc6
jen	jen	k6eAd1
samotná	samotný	k2eAgFnSc1d1
Konstantinopol	Konstantinopol	k1gInSc4
a	a	k8xC
několik	několik	k4yIc4
velmi	velmi	k6eAd1
vzdálených	vzdálený	k2eAgFnPc2d1
malých	malý	k2eAgFnPc2d1
držav	država	k1gFnPc2
<g/>
,	,	kIx,
sama	sám	k3xTgFnSc1
neubrání	ubránit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
se	se	k3xPyFc4
proto	proto	k8xC
marně	marně	k6eAd1
dožadoval	dožadovat	k5eAaImAgMnS
pomoci	pomoc	k1gFnPc4
ostatních	ostatní	k2eAgMnPc2d1
křesťanských	křesťanský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
a	a	k8xC
nepomohlo	pomoct	k5eNaPmAgNnS
mu	on	k3xPp3gMnSc3
<g/>
,	,	kIx,
ani	ani	k8xC
že	že	k8xS
přistoupil	přistoupit	k5eAaPmAgInS
na	na	k7c4
formální	formální	k2eAgNnSc4d1
obnovení	obnovení	k1gNnSc4
jednoty	jednota	k1gFnSc2
pravoslaví	pravoslaví	k1gNnSc2
s	s	k7c7
Římem	Řím	k1gInSc7
(	(	kIx(
<g/>
tzv.	tzv.	kA
Florentskou	florentský	k2eAgFnSc4d1
unii	unie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jako	jako	k9
v	v	k7c6
dobách	doba	k1gFnPc6
silné	silný	k2eAgFnSc2d1
Byzance	Byzanc	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
Araby	Arab	k1gMnPc4
zatlačit	zatlačit	k5eAaPmF
s	s	k7c7
pomocí	pomoc	k1gFnSc7
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
,	,	kIx,
by	by	kYmCp3nS
nyní	nyní	k6eAd1
podobná	podobný	k2eAgFnSc1d1
akce	akce	k1gFnSc1
dokázala	dokázat	k5eAaPmAgFnS
zastavit	zastavit	k5eAaPmF
mnohem	mnohem	k6eAd1
agresivnější	agresivní	k2eAgMnPc4d2
Turky	Turek	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
připlulo	připlout	k5eAaPmAgNnS
z	z	k7c2
italských	italský	k2eAgMnPc2d1
středomořských	středomořský	k2eAgMnPc2d1
států	stát	k1gInPc2
jen	jen	k9
několik	několik	k4yIc4
galér	galéra	k1gFnPc2
a	a	k8xC
oddíl	oddíl	k1gInSc1
žoldnéřů	žoldnéř	k1gMnPc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
slavného	slavný	k2eAgMnSc2d1
janovského	janovský	k2eAgMnSc2d1
kapitána	kapitán	k1gMnSc2
Giustinianiho	Giustiniani	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giustinianiho	Giustinianiha	k1gFnSc5
700	#num#	k4
mužů	muž	k1gMnPc2
představovalo	představovat	k5eAaImAgNnS
velice	velice	k6eAd1
cennou	cenný	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
sami	sám	k3xTgMnPc1
o	o	k7c4
sobě	se	k3xPyFc3
nemohli	moct	k5eNaImAgMnP
stačit	stačit	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
Orhan	Orhan	k1gInSc1
se	se	k3xPyFc4
dobrovolně	dobrovolně	k6eAd1
účastnil	účastnit	k5eAaImAgMnS
obrany	obrana	k1gFnPc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
tureckými	turecký	k2eAgMnPc7d1
přeběhlíky	přeběhlík	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
další	další	k2eAgFnPc4d1
hradby	hradba	k1gFnPc4
okolo	okolo	k7c2
moře	moře	k1gNnSc2
<g/>
;	;	kIx,
ty	ten	k3xDgMnPc4
však	však	k9
Turci	Turek	k1gMnPc1
také	také	k9
zbourali	zbourat	k5eAaPmAgMnP
<g/>
,	,	kIx,
když	když	k8xS
pronikli	proniknout	k5eAaPmAgMnP
do	do	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Navzdory	navzdory	k7c3
naprosto	naprosto	k6eAd1
nedostatečnému	dostatečný	k2eNgInSc3d1
počtu	počet	k1gInSc3
obránců	obránce	k1gMnPc2
vzdorovala	vzdorovat	k5eAaImAgFnS
Konstantinopol	Konstantinopol	k1gInSc4
poměrně	poměrně	k6eAd1
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
svědčí	svědčit	k5eAaImIp3nS
jak	jak	k6eAd1
o	o	k7c6
urputné	urputný	k2eAgFnSc6d1
obraně	obrana	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
o	o	k7c6
kvalitě	kvalita	k1gFnSc6
konstantinopolského	konstantinopolský	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
však	však	k9
Turci	Turek	k1gMnPc1
přece	přece	k9
jen	jen	k6eAd1
pronikli	proniknout	k5eAaPmAgMnP
za	za	k7c4
hlavní	hlavní	k2eAgFnPc4d1
hradby	hradba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
byl	být	k5eAaImAgInS
Giustiniani	Giustiniaň	k1gFnSc6
raněn	ranit	k5eAaPmNgMnS
a	a	k8xC
opustil	opustit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
stanoviště	stanoviště	k1gNnSc4
na	na	k7c6
hradbách	hradba	k1gFnPc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
obránce	obránce	k1gMnSc4
silně	silně	k6eAd1
demoralizovalo	demoralizovat	k5eAaBmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstantin	Konstantin	k1gMnSc1
zahynul	zahynout	k5eAaPmAgMnS
krátce	krátce	k6eAd1
po	po	k7c6
pádu	pád	k1gInSc6
hradeb	hradba	k1gFnPc2
za	za	k7c2
bojů	boj	k1gInPc2
uvnitř	uvnitř	k7c2
města	město	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
smrt	smrt	k1gFnSc1
definitivně	definitivně	k6eAd1
zlomila	zlomit	k5eAaPmAgFnS
odpor	odpor	k1gInSc4
byzantských	byzantský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
tak	tak	k6eAd1
přišli	přijít	k5eAaPmAgMnP
krátce	krátce	k6eAd1
po	po	k7c6
sobě	sebe	k3xPyFc6
o	o	k7c4
oba	dva	k4xCgMnPc4
hlavní	hlavní	k2eAgMnPc4d1
velitele	velitel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giustinianiho	Giustinianiha	k1gFnSc5
odvezli	odvézt	k5eAaPmAgMnP
jeho	jeho	k3xOp3gMnPc7
muži	muž	k1gMnPc7
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
na	na	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
galér	galéra	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
červnu	červen	k1gInSc6
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
následky	následek	k1gInPc4
zranění	zranění	k1gNnPc2
(	(	kIx(
<g/>
patrně	patrně	k6eAd1
na	na	k7c4
sněť	sněť	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orhan	Orhan	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
utéct	utéct	k5eAaPmF
v	v	k7c6
převleku	převlek	k1gInSc6
za	za	k7c4
řeckého	řecký	k2eAgMnSc4d1
mnicha	mnich	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
zajat	zajmout	k5eAaPmNgInS
a	a	k8xC
popraven	popravit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
roku	rok	k1gInSc2
1450	#num#	k4
(	(	kIx(
<g/>
fialově	fialově	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
těsně	těsně	k6eAd1
před	před	k7c7
svým	svůj	k3xOyFgInSc7
koncem	konec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Mehmed	Mehmed	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
čele	čelo	k1gNnSc6
osmanského	osmanský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
Fausto	Fausta	k1gMnSc5
Zonaro	Zonara	k1gFnSc5
</s>
<s>
Přesun	přesun	k1gInSc1
turecké	turecký	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
ke	k	k7c3
Zlatému	zlatý	k2eAgInSc3d1
rohu	roh	k1gInSc3
po	po	k7c6
souši	souš	k1gFnSc6
<g/>
,	,	kIx,
F.	F.	kA
Zonaro	Zonara	k1gFnSc5
</s>
<s>
Turecký	turecký	k2eAgInSc1d1
kanón	kanón	k1gInSc1
z	z	k7c2
bronzu	bronz	k1gInSc2
z	z	k7c2
Dardanel	Dardanely	k1gFnPc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
období	období	k1gNnSc4
shodné	shodný	k2eAgNnSc4d1
s	s	k7c7
dobou	doba	k1gFnSc7
obléhání	obléhání	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
iluminace	iluminace	k1gFnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
mezi	mezi	k7c7
lety	let	k1gInPc7
1470	#num#	k4
<g/>
–	–	k?
<g/>
1479	#num#	k4
<g/>
,	,	kIx,
Kronika	kronika	k1gFnSc1
Karla	Karel	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Vstup	vstup	k1gInSc1
Mehmeda	Mehmed	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
,	,	kIx,
Jean-Joseph	Jean-Joseph	k1gMnSc1
Benjamin-Constant	Benjamin-Constant	k1gMnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NICOLLE	NICOLLE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstantinopol	Konstantinopol	k1gInSc1
1453	#num#	k4
:	:	kIx,
konec	konec	k1gInSc1
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
2881	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RUNCIMAN	RUNCIMAN	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pád	Pád	k1gInSc1
Cařihradu	Cařihrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
223	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86328	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Hradby	hradba	k1gFnPc1
Konstantinopole	Konstantinopol	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pád	Pád	k1gInSc1
Konstantinopole	Konstantinopol	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Dějiny	dějiny	k1gFnPc1
Řecka	Řecko	k1gNnSc2
Starověké	starověký	k2eAgFnSc6d1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Egejská	egejský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
</s>
<s>
Heladská	heladský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
Kykladská	kykladský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
Mínojská	Mínojský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
Mykénská	mykénský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
Formování	formování	k1gNnSc2
řecké	řecký	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
</s>
<s>
Temné	temný	k2eAgNnSc1d1
období	období	k1gNnSc1
•	•	k?
Archaické	archaický	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
•	•	k?
Achájové	Achájový	k2eAgFnSc2d1
•	•	k?
Aiólové	Aiólová	k1gFnSc2
•	•	k?
Dórové	Dór	k1gMnPc1
•	•	k?
Iónové	Ión	k1gMnPc1
Klasické	klasický	k2eAgFnSc2d1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Polis	Polis	k1gInSc1
(	(	kIx(
<g/>
Athény	Athéna	k1gFnPc1
•	•	k?
Sparta	Sparta	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Tyrannis	Tyrannis	k1gFnSc2
•	•	k?
Řecko-perské	řecko-perský	k2eAgFnSc2d1
války	válka	k1gFnSc2
•	•	k?
Peloponéská	peloponéský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
Helénistická	helénistický	k2eAgFnSc1d1
epocha	epocha	k1gFnSc1
</s>
<s>
Makedonské	makedonský	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
•	•	k?
Diadochové	diadoch	k1gMnPc1
(	(	kIx(
<g/>
Antigonovci	Antigonovec	k1gMnPc1
•	•	k?
Ptolemaiovci	Ptolemaiovec	k1gMnSc3
•	•	k?
Seleukovci	Seleukovec	k1gInSc6
<g/>
)	)	kIx)
•	•	k?
Války	válka	k1gFnSc2
diadochů	diadoch	k1gMnPc2
•	•	k?
Makedonské	makedonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
Římské	římský	k2eAgNnSc4d1
období	období	k1gNnSc4
</s>
<s>
Achaia	Achaia	k1gFnSc1
•	•	k?
Macedonia	Macedonium	k1gNnSc2
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Pozdní	pozdní	k2eAgFnSc1d1
antika	antika	k1gFnSc1
•	•	k?
Herakleiovci	Herakleiovec	k1gInSc6
•	•	k?
Syrská	syrský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
•	•	k?
Amorejská	Amorejský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
•	•	k?
Makedonská	makedonský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
•	•	k?
Komnenovci	Komnenovec	k1gMnSc3
•	•	k?
Angelovci	Angelovec	k1gMnSc3
•	•	k?
Čtvrtá	čtvrtý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
nástupnické	nástupnický	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Nikájské	Nikájský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
•	•	k?
Trapezuntské	trapezuntský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
•	•	k?
Epirský	Epirský	k2eAgInSc4d1
despotát	despotát	k1gInSc4
<g/>
;	;	kIx,
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Latinské	latinský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
•	•	k?
Soluňské	soluňský	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Athénské	athénský	k2eAgNnSc4d1
vévodství	vévodství	k1gNnSc4
•	•	k?
Achajské	Achajský	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
<g/>
)	)	kIx)
•	•	k?
Palaiologové	Palaiolog	k1gMnPc1
•	•	k?
Pád	Pád	k1gInSc1
Konstantinopole	Konstantinopol	k1gInSc2
•	•	k?
Morejský	Morejský	k2eAgInSc1d1
despotát	despotát	k1gInSc1
Osmanská	osmanský	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
</s>
<s>
Armatolové	Armatol	k1gMnPc1
•	•	k?
Kleftové	kleft	k1gMnPc1
•	•	k?
Kodžabašijové	Kodžabašijový	k2eAgNnSc1d1
•	•	k?
Fanarioté	Fanariotý	k2eAgFnSc2d1
•	•	k?
Válka	Válka	k1gMnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
Moderní	moderní	k2eAgInSc4d1
řecký	řecký	k2eAgInSc4d1
stát	stát	k1gInSc4
</s>
<s>
První	první	k4xOgFnSc1
helénská	helénský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Řecké	řecký	k2eAgNnSc4d1
království	království	k1gNnSc4
•	•	k?
Druhá	druhý	k4xOgFnSc1
helénská	helénský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Režim	režim	k1gInSc1
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
•	•	k?
Okupace	okupace	k1gFnSc2
(	(	kIx(
<g/>
ELAS	ELAS	kA
•	•	k?
EDES	EDES	kA
<g/>
)	)	kIx)
•	•	k?
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
junta	junta	k1gFnSc1
•	•	k?
Třetí	třetí	k4xOgFnSc1
helénská	helénský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
Nová	nový	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
•	•	k?
PASOK	PASOK	kA
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
311315	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4128316-8	4128316-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85068752	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85068752	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
|	|	kIx~
Historie	historie	k1gFnSc1
|	|	kIx~
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
