<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Mikołaj	Mikołaj	k1gMnSc1	Mikołaj
Kopernik	Kopernik	k1gMnSc1	Kopernik
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1473	[number]	k4	1473
<g/>
,	,	kIx,	,
Toruň	Toruň	k1gFnSc1	Toruň
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1543	[number]	k4	1543
<g/>
,	,	kIx,	,
Frombork	Frombork	k1gInSc1	Frombork
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
polský	polský	k2eAgMnSc1d1	polský
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
stratég	stratég	k1gMnSc1	stratég
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc2	tvůrce
heliocentrické	heliocentrický	k2eAgFnSc2d1	heliocentrická
(	(	kIx(	(
<g/>
sluncestředné	sluncestředný	k2eAgFnSc2d1	sluncestředný
<g/>
)	)	kIx)	)
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
