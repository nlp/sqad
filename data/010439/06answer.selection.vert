<s>
Římští	římský	k2eAgMnPc1d1	římský
císaři	císař	k1gMnPc1	císař
používali	používat	k5eAaImAgMnP	používat
diadém	diadém	k1gInSc4	diadém
nebo	nebo	k8xC	nebo
vavřínový	vavřínový	k2eAgInSc4d1	vavřínový
věnec	věnec	k1gInSc4	věnec
imperátorů	imperátor	k1gMnPc2	imperátor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
