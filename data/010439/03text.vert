<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
corona	coron	k1gMnSc2	coron
–	–	k?	–
věnec	věnec	k1gInSc1	věnec
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
κ	κ	k?	κ
–	–	k?	–
věnec	věnec	k1gInSc1	věnec
<g/>
,	,	kIx,	,
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drahocenn	drahocenn	k1gMnSc1	drahocenn
klenot	klenot	k1gInSc1	klenot
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
jako	jako	k8xS	jako
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
většinou	většinou	k6eAd1	většinou
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
zdobený	zdobený	k2eAgInSc1d1	zdobený
drahými	drahý	k2eAgInPc7d1	drahý
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
panovníky	panovník	k1gMnPc7	panovník
jako	jako	k8xS	jako
odznak	odznak	k1gInSc1	odznak
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
důstojnosti	důstojnost	k1gFnSc2	důstojnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křesťanství	křesťanství	k1gNnSc6	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
symbolem	symbol	k1gInSc7	symbol
vlády	vláda	k1gFnSc2	vláda
těchto	tento	k3xDgMnPc2	tento
panovníků	panovník	k1gMnPc2	panovník
nad	nad	k7c7	nad
určitým	určitý	k2eAgInSc7d1	určitý
národem	národ	k1gInSc7	národ
nebo	nebo	k8xC	nebo
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Přeneseně	přeneseně	k6eAd1	přeneseně
proto	proto	k8xC	proto
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
koruna	koruna	k1gFnSc1	koruna
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
království	království	k1gNnSc1	království
nebo	nebo	k8xC	nebo
císařství	císařství	k1gNnSc1	císařství
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
prohlásíme	prohlásit	k5eAaPmIp1nP	prohlásit
<g/>
-li	i	k?	-li
o	o	k7c6	o
panovníkovi	panovník	k1gMnSc6	panovník
<g/>
,	,	kIx,	,
že	že	k8xS	že
získal	získat	k5eAaPmAgMnS	získat
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
tím	ten	k3xDgNnSc7	ten
na	na	k7c4	na
mysli	mysl	k1gFnPc4	mysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
společně	společně	k6eAd1	společně
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
náležející	náležející	k2eAgNnSc1d1	náležející
území	území	k1gNnSc1	území
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rituál	rituál	k1gInSc1	rituál
používání	používání	k1gNnSc2	používání
koruny	koruna	k1gFnSc2	koruna
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
nosil	nosit	k5eAaImAgMnS	nosit
panovník	panovník	k1gMnSc1	panovník
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
bílo-červenou	bílo-červený	k2eAgFnSc4d1	bílo-červená
korunu	koruna	k1gFnSc4	koruna
horního	horní	k2eAgInSc2d1	horní
a	a	k8xC	a
dolního	dolní	k2eAgInSc2d1	dolní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
nosil	nosit	k5eAaImAgInS	nosit
čelenku	čelenka	k1gFnSc4	čelenka
či	či	k8xC	či
diadém	diadém	k1gInSc4	diadém
<g/>
.	.	kIx.	.
</s>
<s>
Perští	perský	k2eAgMnPc1d1	perský
králové	král	k1gMnPc1	král
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nosili	nosit	k5eAaImAgMnP	nosit
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tiáru	tiára	k1gFnSc4	tiára
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
převzata	převzat	k2eAgFnSc1d1	převzata
byzantskými	byzantský	k2eAgMnPc7d1	byzantský
císaři	císař	k1gMnPc7	císař
a	a	k8xC	a
také	také	k6eAd1	také
papeži	papež	k1gMnPc7	papež
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
používali	používat	k5eAaImAgMnP	používat
trojitou	trojitý	k2eAgFnSc4d1	trojitá
korunu	koruna	k1gFnSc4	koruna
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
papežově	papežův	k2eAgInSc6d1	papežův
znaku	znak	k1gInSc6	znak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
tiára	tiára	k1gFnSc1	tiára
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
biskupskou	biskupský	k2eAgFnSc4d1	biskupská
mitru	mitra	k1gFnSc4	mitra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
vatikánských	vatikánský	k2eAgInPc2d1	vatikánský
úřadů	úřad	k1gInPc2	úřad
se	se	k3xPyFc4	se
tiára	tiára	k1gFnSc1	tiára
nadále	nadále	k6eAd1	nadále
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římští	římský	k2eAgMnPc1d1	římský
císaři	císař	k1gMnPc1	císař
používali	používat	k5eAaImAgMnP	používat
diadém	diadém	k1gInSc4	diadém
nebo	nebo	k8xC	nebo
vavřínový	vavřínový	k2eAgInSc4d1	vavřínový
věnec	věnec	k1gInSc4	věnec
imperátorů	imperátor	k1gMnPc2	imperátor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vládcové	vládce	k1gMnPc1	vládce
Germánů	Germán	k1gMnPc2	Germán
nosili	nosit	k5eAaImAgMnP	nosit
přilbu	přilba	k1gFnSc4	přilba
nebo	nebo	k8xC	nebo
čelenku	čelenka	k1gFnSc4	čelenka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
karolinských	karolinský	k2eAgFnPc2d1	Karolinská
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používaly	používat	k5eAaImAgFnP	používat
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
koruny	koruna	k1gFnPc1	koruna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
překlenuté	překlenutý	k2eAgInPc4d1	překlenutý
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
oblouky	oblouk	k1gInPc1	oblouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Korunovace	korunovace	k1gFnSc2	korunovace
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
propůjčovala	propůjčovat	k5eAaImAgNnP	propůjčovat
panovníkovi	panovník	k1gMnSc3	panovník
jeho	jeho	k3xOp3gFnSc4	jeho
legitimitu	legitimita	k1gFnSc4	legitimita
teprve	teprve	k6eAd1	teprve
řádně	řádně	k6eAd1	řádně
vykonaná	vykonaný	k2eAgFnSc1d1	vykonaná
korunovace	korunovace	k1gFnSc2	korunovace
jedinou	jediný	k2eAgFnSc7d1	jediná
správnou	správný	k2eAgFnSc7d1	správná
korunou	koruna	k1gFnSc7	koruna
na	na	k7c6	na
jediném	jediný	k2eAgNnSc6d1	jediné
správném	správný	k2eAgNnSc6d1	správné
místě	místo	k1gNnSc6	místo
řádným	řádný	k2eAgInSc7d1	řádný
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
oprávněným	oprávněný	k2eAgInSc7d1	oprávněný
korunovačem	korunovač	k1gInSc7	korunovač
(	(	kIx(	(
<g/>
Coronator	Coronator	k1gInSc1	Coronator
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnSc6d1	římská
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
zvolený	zvolený	k2eAgMnSc1d1	zvolený
král	král	k1gMnSc1	král
korunován	korunován	k2eAgMnSc1d1	korunován
výhradně	výhradně	k6eAd1	výhradně
rýnskokolínským	rýnskokolínský	k2eAgMnSc7d1	rýnskokolínský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
–	–	k?	–
a	a	k8xC	a
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
–	–	k?	–
říšskou	říšský	k2eAgFnSc7d1	říšská
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Korunovace	korunovace	k1gFnSc1	korunovace
císaře	císař	k1gMnSc2	císař
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
vykonána	vykonán	k2eAgFnSc1d1	vykonána
–	–	k?	–
též	též	k9	též
až	až	k6eAd1	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
–	–	k?	–
papežem	papež	k1gMnSc7	papež
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
nebo	nebo	k8xC	nebo
papežským	papežský	k2eAgInSc7d1	papežský
legátem	legát	k1gInSc7	legát
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místem	k6eAd1	místem
korunovace	korunovace	k1gFnSc1	korunovace
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
Remešská	remešský	k2eAgFnSc1d1	remešská
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
korun	koruna	k1gFnPc2	koruna
==	==	k?	==
</s>
</p>
<p>
<s>
Vládnoucí	vládnoucí	k2eAgMnPc1d1	vládnoucí
knížata	kníže	k1gMnPc1wR	kníže
a	a	k8xC	a
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podle	podle	k7c2	podle
pravidla	pravidlo	k1gNnSc2	pravidlo
priority	priorita	k1gFnSc2	priorita
byli	být	k5eAaImAgMnP	být
podřízeni	podřídit	k5eAaPmNgMnP	podřídit
císařům	císař	k1gMnPc3	císař
a	a	k8xC	a
králům	král	k1gMnPc3	král
<g/>
,	,	kIx,	,
nosili	nosit	k5eAaImAgMnP	nosit
hodnostní	hodnostní	k2eAgFnSc4d1	hodnostní
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
podoba	podoba	k1gFnSc1	podoba
určovaly	určovat	k5eAaImAgFnP	určovat
postavení	postavení	k1gNnSc6	postavení
nositele	nositel	k1gMnSc4	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
existovaly	existovat	k5eAaImAgFnP	existovat
také	také	k9	také
tzv.	tzv.	kA	tzv.
hodnostní	hodnostní	k2eAgFnSc2d1	hodnostní
perly	perla	k1gFnSc2	perla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
-	-	kIx~	-
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
vzhledově	vzhledově	k6eAd1	vzhledově
jiná	jiný	k2eAgFnSc1d1	jiná
</s>
</p>
<p>
<s>
Říšská	říšský	k2eAgFnSc1d1	říšská
koruna	koruna	k1gFnSc1	koruna
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
</s>
</p>
<p>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
imperátorská	imperátorský	k2eAgFnSc1d1	imperátorská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
heraldický	heraldický	k2eAgInSc1d1	heraldický
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
dynastie	dynastie	k1gFnSc2	dynastie
Pahlaví	Pahlavý	k2eAgMnPc1d1	Pahlavý
(	(	kIx(	(
<g/>
íránská	íránský	k2eAgFnSc1d1	íránská
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
použita	použít	k5eAaPmNgFnS	použít
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Indická	indický	k2eAgFnSc1d1	indická
císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
použita	použít	k5eAaPmNgFnS	použít
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
císařská	císařský	k2eAgFnSc1d1	císařská
tiáraKrálovská	tiáraKrálovský	k2eAgFnSc1d1	tiáraKrálovský
koruna	koruna	k1gFnSc1	koruna
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
oblouků	oblouk	k1gInPc2	oblouk
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
(	(	kIx(	(
<g/>
s	s	k7c7	s
oblouky	oblouk	k1gInPc7	oblouk
<g/>
,	,	kIx,	,
ev.	ev.	k?	ev.
s	s	k7c7	s
říšským	říšský	k2eAgNnSc7d1	říšské
jablkem	jablko	k1gNnSc7	jablko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svatoštěpánská	svatoštěpánský	k2eAgFnSc1d1	Svatoštěpánská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
uherská	uherský	k2eAgFnSc1d1	uherská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
svatého	svatý	k2eAgMnSc2d1	svatý
Eduarda	Eduard	k1gMnSc2	Eduard
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
imperiální	imperiální	k2eAgFnSc1d1	imperiální
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
britské	britský	k2eAgFnSc2d1	britská
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skotská	skotský	k2eAgFnSc1d1	skotská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
francouzská	francouzský	k2eAgFnSc1d1	francouzská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Železná	železný	k2eAgFnSc1d1	železná
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
Lombardie	Lombardie	k1gFnSc2	Lombardie
resp.	resp.	kA	resp.
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čapka	Čapka	k1gMnSc1	Čapka
monomacha	monomacha	k1gMnSc1	monomacha
(	(	kIx(	(
<g/>
moskevský	moskevský	k2eAgInSc1d1	moskevský
carský	carský	k2eAgInSc1d1	carský
klobouk	klobouk	k1gInSc1	klobouk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kazaňská	kazaňský	k2eAgFnSc1d1	Kazaňská
čapka	čapka	k1gFnSc1	čapka
(	(	kIx(	(
<g/>
kazaňský	kazaňský	k2eAgInSc1d1	kazaňský
carský	carský	k2eAgInSc1d1	carský
klobouk	klobouk	k1gInSc1	klobouk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
Fridricha	Fridrich	k1gMnSc2	Fridrich
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Hohenzollernská	hohenzollernský	k2eAgFnSc1d1	hohenzollernský
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
pruské	pruský	k2eAgFnSc2d1	pruská
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Ocelová	ocelový	k2eAgFnSc1d1	ocelová
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
rumunská	rumunský	k2eAgFnSc1d1	rumunská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Černohorská	černohorský	k2eAgFnSc1d1	černohorská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
Štěpána	Štěpán	k1gMnSc2	Štěpán
Dušana	Dušan	k1gMnSc2	Dušan
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
černohorských	černohorský	k2eAgInPc2d1	černohorský
klenotů	klenot	k1gInPc2	klenot
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dánská	dánský	k2eAgFnSc1d1	dánská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
svatého	svatý	k2eAgMnSc2d1	svatý
Erika	Erik	k1gMnSc2	Erik
(	(	kIx(	(
<g/>
švédská	švédský	k2eAgFnSc1d1	švédská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finská	finský	k2eAgFnSc1d1	finská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
použita	použít	k5eAaPmNgFnS	použít
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Havajská	havajský	k2eAgFnSc1d1	Havajská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Tonžská	Tonžský	k2eAgFnSc1d1	Tonžský
královská	královský	k2eAgFnSc1d1	královská
korunaKurfiřtský	korunaKurfiřtský	k2eAgInSc5d1	korunaKurfiřtský
klobouk	klobouk	k1gInSc1	klobouk
</s>
</p>
<p>
<s>
Arcivévodský	arcivévodský	k2eAgInSc1d1	arcivévodský
klobouk	klobouk	k1gInSc1	klobouk
</s>
</p>
<p>
<s>
Vévodská	vévodský	k2eAgFnSc1d1	vévodská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Vévodský	vévodský	k2eAgInSc1d1	vévodský
klobouk	klobouk	k1gInSc1	klobouk
</s>
</p>
<p>
<s>
Knížecí	knížecí	k2eAgFnSc1d1	knížecí
koruna	koruna	k1gFnSc1	koruna
-	-	kIx~	-
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
s	s	k7c7	s
2	[number]	k4	2
oblouky	oblouk	k1gInPc7	oblouk
a	a	k8xC	a
říšským	říšský	k2eAgNnSc7d1	říšské
jablkem	jablko	k1gNnSc7	jablko
</s>
</p>
<p>
<s>
Knížecí	knížecí	k2eAgInSc1d1	knížecí
klobouk	klobouk	k1gInSc1	klobouk
</s>
</p>
<p>
<s>
Hraběcí	hraběcí	k2eAgFnSc1d1	hraběcí
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
9	[number]	k4	9
perel	perla	k1gFnPc2	perla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
svobodných	svobodný	k2eAgMnPc2d1	svobodný
pánů	pan	k1gMnPc2	pan
(	(	kIx(	(
<g/>
7	[number]	k4	7
perel	perla	k1gFnPc2	perla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šlechtická	šlechtický	k2eAgFnSc1d1	šlechtická
koruna	koruna	k1gFnSc1	koruna
nebo	nebo	k8xC	nebo
Rytířská	rytířský	k2eAgFnSc1d1	rytířská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
5	[number]	k4	5
perel	perla	k1gFnPc2	perla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Některé	některý	k3yIgFnPc4	některý
slavné	slavný	k2eAgFnPc4d1	slavná
koruny	koruna	k1gFnPc4	koruna
==	==	k?	==
</s>
</p>
<p>
<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
svatého	svatý	k2eAgMnSc2d1	svatý
Eduarda	Eduard	k1gMnSc2	Eduard
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
imperiální	imperiální	k2eAgFnSc1d1	imperiální
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
britské	britský	k2eAgFnSc2d1	britská
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
koruny	koruna	k1gFnPc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
Istvána	István	k2eAgFnSc1d1	Istvána
Bocskaye	Bocskaye	k1gFnSc1	Bocskaye
</s>
</p>
<p>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Svatoštěpánská	svatoštěpánský	k2eAgFnSc1d1	Svatoštěpánská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
uherská	uherský	k2eAgFnSc1d1	uherská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Říšská	říšský	k2eAgFnSc1d1	říšská
koruna	koruna	k1gFnSc1	koruna
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
</s>
</p>
<p>
<s>
Železná	železný	k2eAgFnSc1d1	železná
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
Lombardie	Lombardie	k1gFnSc2	Lombardie
resp.	resp.	kA	resp.
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ocelová	ocelový	k2eAgFnSc1d1	ocelová
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
koruna	koruna	k1gFnSc1	koruna
rumunská	rumunský	k2eAgFnSc1d1	rumunská
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tiára	tiára	k1gFnSc1	tiára
(	(	kIx(	(
<g/>
papežská	papežský	k2eAgFnSc1d1	Papežská
koruna	koruna	k1gFnSc1	koruna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čapka	Čapka	k1gMnSc1	Čapka
monomacha	monomacha	k1gMnSc1	monomacha
(	(	kIx(	(
<g/>
moskevský	moskevský	k2eAgInSc1d1	moskevský
carský	carský	k2eAgInSc1d1	carský
klobouk	klobouk	k1gInSc1	klobouk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skanderbegova	Skanderbegův	k2eAgFnSc1d1	Skanderbegův
helma	helma	k1gFnSc1	helma
</s>
</p>
<p>
<s>
Západogótská	Západogótský	k2eAgFnSc1d1	Západogótský
svatokoruna	svatokoruna	k1gFnSc1	svatokoruna
z	z	k7c2	z
Guarrazaru	Guarrazar	k1gInSc2	Guarrazar
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jürgen	Jürgen	k1gInSc1	Jürgen
Abeler	Abeler	k1gInSc1	Abeler
<g/>
:	:	kIx,	:
Kronen	Kronen	k1gInSc1	Kronen
<g/>
.	.	kIx.	.
</s>
<s>
Herrschaftszeichen	Herrschaftszeichen	k2eAgInSc4d1	Herrschaftszeichen
der	drát	k5eAaImRp2nS	drát
Welt	Welt	k1gMnSc1	Welt
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Auflage	Auflage	k1gFnSc1	Auflage
<g/>
,	,	kIx,	,
Düsseldorf	Düsseldorf	k1gInSc1	Düsseldorf
<g/>
/	/	kIx~	/
<g/>
Vídeň	Vídeň	k1gFnSc1	Vídeň
(	(	kIx(	(
<g/>
Econ	Econ	k1gInSc1	Econ
<g/>
)	)	kIx)	)
1972	[number]	k4	1972
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
3-430-11002-5	[number]	k4	3-430-11002-5
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Diadém	diadém	k1gInSc1	diadém
</s>
</p>
<p>
<s>
Tiára	tiára	k1gFnSc1	tiára
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
koruna	koruna	k1gFnSc1	koruna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
koruna	koruna	k1gFnSc1	koruna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Koruny	koruna	k1gFnPc1	koruna
evropských	evropský	k2eAgMnPc2d1	evropský
panovníků	panovník	k1gMnPc2	panovník
na	na	k7c6	na
rakouských	rakouský	k2eAgFnPc6d1	rakouská
stránkách	stránka	k1gFnPc6	stránka
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
švédského	švédský	k2eAgMnSc2d1	švédský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
X.	X.	kA	X.
Gustava	Gustav	k1gMnSc2	Gustav
</s>
</p>
