<s>
Rorýs	rorýs	k1gMnSc1	rorýs
obecný	obecný	k2eAgMnSc1d1	obecný
se	se	k3xPyFc4	se
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
hnízdního	hnízdní	k2eAgNnSc2d1	hnízdní
období	období	k1gNnSc2	období
téměř	téměř	k6eAd1	téměř
neustále	neustále	k6eAd1	neustále
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokonce	dokonce	k9	dokonce
i	i	k9	i
spí	spát	k5eAaImIp3nS	spát
<g/>
,	,	kIx,	,
pije	pít	k5eAaImIp3nS	pít
a	a	k8xC	a
páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
střídá	střídat	k5eAaImIp3nS	střídat
přitom	přitom	k6eAd1	přitom
svižný	svižný	k2eAgInSc4d1	svižný
let	let	k1gInSc4	let
s	s	k7c7	s
plachtěním	plachtění	k1gNnSc7	plachtění
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yIgMnSc2	který
nabírá	nabírat	k5eAaImIp3nS	nabírat
výšku	výška	k1gFnSc4	výška
okolo	okolo	k7c2	okolo
jednoho	jeden	k4xCgNnSc2	jeden
až	až	k6eAd1	až
dvou	dva	k4xCgInPc2	dva
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
následně	následně	k6eAd1	následně
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
mohl	moct	k5eAaImAgMnS	moct
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
