<s>
Žloutek	žloutek	k1gInSc1
</s>
<s>
Žloutek	žloutek	k1gInSc1
slepičího	slepičí	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
</s>
<s>
Slepičí	slepičí	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
se	s	k7c7
dvěma	dva	k4xCgInPc7
žloutky	žloutek	k1gInPc7
<g/>
,	,	kIx,
rarita	rarita	k1gFnSc1
</s>
<s>
Žloutek	žloutek	k1gInSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
pro	pro	k7c4
živiny	živina	k1gFnPc4
uskladněné	uskladněný	k2eAgFnPc4d1
ve	v	k7c6
vajíčku	vajíčko	k1gNnSc6
mnohých	mnohý	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nezbytné	nezbytný	k2eAgNnSc1d1,k2eNgNnSc1d1
pro	pro	k7c4
výživu	výživa	k1gFnSc4
vyvíjejícího	vyvíjející	k2eAgInSc2d1
se	se	k3xPyFc4
zárodku	zárodek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
oplodněním	oplodnění	k1gNnSc7
je	být	k5eAaImIp3nS
žloutek	žloutek	k1gInSc1
vlastně	vlastně	k9
tvořen	tvořen	k2eAgInSc1d1
jedinou	jediný	k2eAgFnSc7d1
buňkou	buňka	k1gFnSc7
<g/>
,	,	kIx,
např.	např.	kA
u	u	k7c2
ptačích	ptačí	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
mála	málo	k4c2
případů	případ	k1gInPc2
buňky	buňka	k1gFnSc2
viditelné	viditelný	k2eAgFnSc2d1
prostým	prostý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Žloutek	žloutek	k1gInSc4
mají	mít	k5eAaImIp3nP
mnozí	mnohý	k2eAgMnPc1d1
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
ptáci	pták	k1gMnPc1
<g/>
,	,	kIx,
plazi	plaz	k1gMnPc1
<g/>
,	,	kIx,
ryby	ryba	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
u	u	k7c2
savců	savec	k1gMnPc2
je	být	k5eAaImIp3nS
redukovaný	redukovaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
ho	on	k3xPp3gMnSc4
funkčně	funkčně	k6eAd1
nahrazuje	nahrazovat	k5eAaImIp3nS
placenta	placenta	k1gFnSc1
a	a	k8xC
posléze	posléze	k6eAd1
mateřské	mateřský	k2eAgNnSc1d1
mléko	mléko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
v	v	k7c6
raných	raný	k2eAgFnPc6d1
fázích	fáze	k1gFnPc6
embryogeneze	embryogeneze	k1gFnSc2
přítomen	přítomno	k1gNnPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
tzv.	tzv.	kA
žloutkovém	žloutkový	k2eAgInSc6d1
váčku	váček	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
vajíček	vajíčko	k1gNnPc2
podle	podle	k7c2
obsahu	obsah	k1gInSc2
žloutku	žloutek	k1gInSc2
</s>
<s>
Žloutek	žloutek	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
vajíčku	vajíčko	k1gNnSc6
přítomen	přítomno	k1gNnPc2
ve	v	k7c6
formě	forma	k1gFnSc6
inkluzí	inkluze	k1gFnPc2
<g/>
,	,	kIx,
jakýchsi	jakýsi	k3yIgInPc2
váčků	váček	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vajíčka	vajíčko	k1gNnPc1
různých	různý	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
obsahují	obsahovat	k5eAaImIp3nP
různé	různý	k2eAgInPc1d1
množství	množství	k1gNnSc4
žloutku	žloutek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc1d1
parametr	parametr	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
závisí	záviset	k5eAaImIp3nS
např.	např.	kA
typ	typ	k1gInSc1
rýhování	rýhování	k1gNnSc2
(	(	kIx(
<g/>
násobného	násobný	k2eAgNnSc2d1
dělení	dělení	k1gNnSc2
<g/>
)	)	kIx)
oplozených	oplozený	k2eAgNnPc2d1
vajíček	vajíčko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišují	rozlišovat	k5eAaImIp3nP
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc4
skupiny	skupina	k1gFnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
holoblastická	holoblastický	k2eAgFnSc1d1
–	–	k?
vývoje	vývoj	k1gInSc2
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
</s>
<s>
oligolecitální	oligolecitální	k2eAgInSc1d1
–	–	k?
obsahují	obsahovat	k5eAaImIp3nP
malé	malý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
žloutku	žloutek	k1gInSc2
<g/>
;	;	kIx,
např.	např.	kA
vajíčka	vajíčko	k1gNnPc4
kopinatců	kopinatec	k1gMnPc2
nebo	nebo	k8xC
savců	savec	k1gMnPc2
<g/>
;	;	kIx,
rýhují	rýhovat	k5eAaImIp3nP
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
totálně	totálně	k6eAd1
a	a	k8xC
ekválně	ekválně	k6eAd1
</s>
<s>
mezolecitální	mezolecitální	k2eAgInSc1d1
–	–	k?
obsahují	obsahovat	k5eAaImIp3nP
žloutku	žloutek	k1gInSc3
poněkud	poněkud	k6eAd1
více	hodně	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k8xC
obvykle	obvykle	k6eAd1
stačí	stačit	k5eAaBmIp3nS
jen	jen	k9
na	na	k7c4
rané	raný	k2eAgNnSc4d1
embryonální	embryonální	k2eAgNnSc4d1
období	období	k1gNnSc4
(	(	kIx(
<g/>
larvy	larva	k1gFnPc1
se	se	k3xPyFc4
již	již	k6eAd1
živí	živit	k5eAaImIp3nP
samy	sám	k3xTgFnPc4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
např.	např.	kA
vajíčka	vajíčko	k1gNnPc4
kruhoústých	kruhoústí	k1gMnPc2
<g/>
,	,	kIx,
mnohých	mnohý	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
a	a	k8xC
obojživelníků	obojživelník	k1gMnPc2
<g/>
;	;	kIx,
rýhují	rýhovat	k5eAaImIp3nP
se	se	k3xPyFc4
totálně	totálně	k6eAd1
inekválně	inekválně	k6eAd1
</s>
<s>
meroblastická	meroblastický	k2eAgFnSc1d1
–	–	k?
vývoje	vývoj	k1gInSc2
se	se	k3xPyFc4
neúčastní	účastnit	k5eNaImIp3nS
celé	celý	k2eAgNnSc1d1
embryo	embryo	k1gNnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
jako	jako	k9
zásoba	zásoba	k1gFnSc1
žloutku	žloutek	k1gInSc2
nechána	nechat	k5eAaPmNgFnS
stranou	strana	k1gFnSc7
</s>
<s>
polylecitální	polylecitální	k2eAgInSc1d1
–	–	k?
obsahují	obsahovat	k5eAaImIp3nP
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
žloutku	žloutek	k1gInSc2
<g/>
;	;	kIx,
např.	např.	kA
některé	některý	k3yIgFnPc1
ryby	ryba	k1gFnPc1
<g/>
,	,	kIx,
obojživelníci	obojživelník	k1gMnPc1
<g/>
,	,	kIx,
plazi	plaz	k1gMnPc1
<g/>
,	,	kIx,
vejcorodí	vejcorodý	k2eAgMnPc1d1
savci	savec	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
hlavně	hlavně	k9
ptáci	pták	k1gMnPc1
<g/>
;	;	kIx,
rýhují	rýhovat	k5eAaImIp3nP
se	se	k3xPyFc4
pouze	pouze	k6eAd1
na	na	k7c6
animálním	animální	k2eAgInSc6d1
pólu	pól	k1gInSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
na	na	k7c6
pólu	pól	k1gInSc6
vegetativním	vegetativní	k2eAgInSc6d1
je	být	k5eAaImIp3nS
nahromaděn	nahromadit	k5eAaPmNgInS
žloutek	žloutek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Žloutek	žloutek	k1gInSc1
slepičího	slepičí	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
</s>
<s>
Oplodněné	oplodněný	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
<g/>
,	,	kIx,
embryo	embryo	k1gNnSc1
a	a	k8xC
žloutek	žloutek	k1gInSc1
pod	pod	k7c7
skořápkou	skořápka	k1gFnSc7
</s>
<s>
Žloutek	žloutek	k1gInSc1
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
zásobárnou	zásobárna	k1gFnSc7
živin	živina	k1gFnPc2
pro	pro	k7c4
vyvíjející	vyvíjející	k2eAgInSc4d1
se	se	k3xPyFc4
zárodek	zárodek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
také	také	k9
humorální	humorální	k2eAgFnSc4d1
protilátky	protilátka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
vstřebávají	vstřebávat	k5eAaImIp3nP
do	do	k7c2
krevní	krevní	k2eAgFnSc2d1
cirkulace	cirkulace	k1gFnSc2
embrya	embryo	k1gNnSc2
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
třetině	třetina	k1gFnSc6
inkubace	inkubace	k1gFnSc2
a	a	k8xC
jako	jako	k9
mateřské	mateřský	k2eAgFnPc4d1
protilátky	protilátka	k1gFnPc4
chrání	chránit	k5eAaImIp3nS
vylíhnuté	vylíhnutý	k2eAgNnSc1d1
mládě	mládě	k1gNnSc1
cca	cca	kA
v	v	k7c6
prvních	první	k4xOgInPc6
třech	tři	k4xCgInPc6
týdnech	týden	k1gInPc6
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Žloutek	žloutek	k1gInSc1
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
zploštělé	zploštělý	k2eAgFnSc2d1
koule	koule	k1gFnSc2
o	o	k7c6
průměru	průměr	k1gInSc6
32	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
objem	objem	k1gInSc4
žloutku	žloutek	k1gInSc2
slepičího	slepičí	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
je	být	k5eAaImIp3nS
asi	asi	k9
16	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
cm	cm	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmotnost	hmotnost	k1gFnSc1
a	a	k8xC
velikost	velikost	k1gFnSc1
žloutku	žloutek	k1gInSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
druhu	druh	k1gInSc6
a	a	k8xC
věku	věk	k1gInSc6
nosnice	nosnice	k1gFnSc2
–	–	k?
u	u	k7c2
mladých	mladý	k2eAgFnPc2d1
slepic	slepice	k1gFnPc2
dosahuje	dosahovat	k5eAaImIp3nS
9	#num#	k4
g	g	kA
<g/>
,	,	kIx,
u	u	k7c2
dospělých	dospělý	k2eAgFnPc2d1
15-20	15-20	k4
g	g	kA
<g/>
,	,	kIx,
koncem	koncem	k7c2
snášky	snáška	k1gFnSc2
23	#num#	k4
i	i	k8xC
více	hodně	k6eAd2
gramů	gram	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
podíl	podíl	k1gInSc4
z	z	k7c2
hmotnosti	hmotnost	k1gFnSc2
vejce	vejce	k1gNnSc2
činí	činit	k5eAaImIp3nS
30-35	30-35	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žloutek	žloutek	k1gInSc1
čerstvého	čerstvý	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
má	mít	k5eAaImIp3nS
pH	ph	kA
6,0	6,0	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
po	po	k7c6
snesení	snesení	k1gNnSc6
zvyšuje	zvyšovat	k5eAaImIp3nS
na	na	k7c4
neutrální	neutrální	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koagulace	koagulace	k1gFnSc1
žloutku	žloutek	k1gInSc2
nastává	nastávat	k5eAaImIp3nS
při	při	k7c6
65-70	65-70	k4
°	°	k?
<g/>
C.	C.	kA
Bod	bod	k1gInSc1
mrazu	mráz	k1gInSc2
volného	volný	k2eAgInSc2d1
žloutku	žloutek	k1gInSc2
je	být	k5eAaImIp3nS
-0,58	-0,58	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Stavba	stavba	k1gFnSc1
žloutku	žloutek	k1gInSc2
</s>
<s>
Žloutek	žloutek	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
žloutková	žloutkový	k2eAgFnSc1d1
blána	blána	k1gFnSc1
<g/>
,	,	kIx,
žloutková	žloutkový	k2eAgFnSc1d1
hmota	hmota	k1gFnSc1
<g/>
,	,	kIx,
latebra	latebra	k1gFnSc1
<g/>
,	,	kIx,
Panderovo	Panderův	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
a	a	k8xC
zárodečný	zárodečný	k2eAgInSc1d1
terčík	terčík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Žloutková	žloutkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
vitelinní	vitelinný	k2eAgMnPc1d1
<g/>
)	)	kIx)
membrána	membrána	k1gFnSc1
obaluje	obalovat	k5eAaImIp3nS
žloutkovou	žloutkový	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
poměrně	poměrně	k6eAd1
pevná	pevný	k2eAgFnSc1d1
a	a	k8xC
elastická	elastický	k2eAgFnSc1d1
blána	blána	k1gFnSc1
o	o	k7c6
průměrné	průměrný	k2eAgFnSc6d1
síle	síla	k1gFnSc6
6-15	6-15	k4
µ	µ	k1gFnPc2
<g/>
,	,	kIx,
hmotnosti	hmotnost	k1gFnSc2
kolem	kolem	k7c2
50	#num#	k4
mg	mg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propouští	propouštět	k5eAaImIp3nS
vodu	voda	k1gFnSc4
a	a	k8xC
plyny	plyn	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
nepropouští	propouštět	k5eNaImIp3nP
bílkoviny	bílkovina	k1gFnPc4
a	a	k8xC
tuky	tuk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
80-90	80-90	k4
%	%	kIx~
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
6-8	6-8	k4
%	%	kIx~
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
3	#num#	k4
%	%	kIx~
tuku	tuk	k1gInSc2
a	a	k8xC
menší	malý	k2eAgNnSc4d2
množství	množství	k1gNnSc4
cukrů	cukr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
žloutkovou	žloutkový	k2eAgFnSc7d1
membránou	membrána	k1gFnSc7
je	být	k5eAaImIp3nS
tenká	tenký	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
světlého	světlý	k2eAgInSc2d1
žloutku	žloutek	k1gInSc2
nepřerušená	přerušený	k2eNgFnSc1d1
latebrou	latebra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poklesem	pokles	k1gInSc7
elasticity	elasticita	k1gFnPc1
blány	blána	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
nevhodným	vhodný	k2eNgNnSc7d1
anebo	anebo	k8xC
dlouhodobým	dlouhodobý	k2eAgNnSc7d1
skladováním	skladování	k1gNnSc7
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
žloutek	žloutek	k1gInSc1
zplošťuje	zplošťovat	k5eAaImIp3nS
a	a	k8xC
blána	blána	k1gFnSc1
se	se	k3xPyFc4
trhá	trhat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žloutková	žloutkový	k2eAgFnSc1d1
blána	blána	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
4	#num#	k4
vrstvami	vrstva	k1gFnPc7
<g/>
:	:	kIx,
</s>
<s>
a	a	k8xC
<g/>
)	)	kIx)
dvěma	dva	k4xCgFnPc7
vnitřními	vnitřní	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
(	(	kIx(
<g/>
primární	primární	k2eAgFnSc1d1
<g/>
,	,	kIx,
pravá	pravý	k2eAgFnSc1d1
vitelinní	vitelinný	k2eAgMnPc1d1
membrána	membrána	k1gFnSc1
a	a	k8xC
sekundární	sekundární	k2eAgMnPc1d1
<g/>
,	,	kIx,
perivitelinní	perivitelinný	k2eAgMnPc1d1
membrána	membrána	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
produkovány	produkovat	k5eAaImNgFnP
ve	v	k7c6
vaječníku	vaječník	k1gInSc6
a	a	k8xC
obsahují	obsahovat	k5eAaImIp3nP
protein	protein	k1gInSc4
podobný	podobný	k2eAgInSc4d1
kolagenu	kolagen	k1gInSc2
</s>
<s>
b	b	k?
<g/>
)	)	kIx)
dvěma	dva	k4xCgFnPc7
vnějšími	vnější	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
vznikají	vznikat	k5eAaImIp3nP
ve	v	k7c6
vejcovodu	vejcovod	k1gInSc6
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
infundibulu	infundibul	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
obsahují	obsahovat	k5eAaImIp3nP
bílkoviny	bílkovina	k1gFnPc1
připomínající	připomínající	k2eAgFnSc1d1
směs	směs	k1gFnSc4
lysozymu	lysozym	k1gInSc2
a	a	k8xC
ovotransferinu	ovotransferin	k1gInSc2
(	(	kIx(
<g/>
bílkovin	bílkovina	k1gFnPc2
bílku	bílek	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Žloutková	žloutkový	k2eAgFnSc1d1
hmota	hmota	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
střídajícími	střídající	k2eAgFnPc7d1
se	se	k3xPyFc4
koncentrickými	koncentrický	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
tmavého	tmavý	k2eAgInSc2d1
a	a	k8xC
světlého	světlý	k2eAgInSc2d1
žloutku	žloutek	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
každý	každý	k3xTgInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
několika	několik	k4yIc7
vrstvami	vrstva	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc1
světlého	světlý	k2eAgInSc2d1
žloutku	žloutek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
2-5	2-5	k4
%	%	kIx~
veškeré	veškerý	k3xTgFnSc2
hmoty	hmota	k1gFnSc2
žloutku	žloutek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlý	světlý	k2eAgInSc1d1
žloutek	žloutek	k1gInSc1
se	se	k3xPyFc4
chemicky	chemicky	k6eAd1
liší	lišit	k5eAaImIp3nP
od	od	k7c2
tmavého	tmavý	k2eAgInSc2d1
–	–	k?
obsahuje	obsahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
10-13	10-13	k4
%	%	kIx~
sušiny	sušina	k1gFnSc2
a	a	k8xC
více	hodně	k6eAd2
proteinů	protein	k1gInPc2
než	než	k8xS
lipidů	lipid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odlišná	odlišný	k2eAgFnSc1d1
barva	barva	k1gFnSc1
žloutku	žloutek	k1gInSc2
je	být	k5eAaImIp3nS
způsobena	způsobit	k5eAaPmNgFnS
nerovnoměrným	rovnoměrný	k2eNgNnSc7d1
ukládáním	ukládání	k1gNnSc7
barviv	barvivo	k1gNnPc2
a	a	k8xC
lipidů	lipid	k1gInPc2
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
6	#num#	k4
dnech	den	k1gInPc6
intenzivní	intenzivní	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
žloutku	žloutek	k1gInSc2
ve	v	k7c6
vaječníku	vaječník	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
den	den	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
slepice	slepice	k1gFnPc1
přijímají	přijímat	k5eAaImIp3nP
krmivo	krmivo	k1gNnSc4
<g/>
,	,	kIx,
vznikají	vznikat	k5eAaImIp3nP
tmavší	tmavý	k2eAgFnPc1d2
vrstvy	vrstva	k1gFnPc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
noci	noc	k1gFnSc6
se	se	k3xPyFc4
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
světlé	světlý	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
žloutku	žloutek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nadbytku	nadbytek	k1gInSc6
pigmentů	pigment	k1gInPc2
v	v	k7c6
krmivu	krmivo	k1gNnSc6
se	se	k3xPyFc4
žloutkové	žloutkový	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
netvoří	tvořit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmota	hmota	k1gFnSc1
žloutku	žloutek	k1gInSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
kulovitých	kulovitý	k2eAgNnPc2d1
tělísek	tělísko	k1gNnPc2
(	(	kIx(
<g/>
granulí	granule	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
plazmy	plazma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sušina	sušina	k1gFnSc1
žloutku	žloutek	k1gInSc2
představuje	představovat	k5eAaImIp3nS
kolem	kolem	k7c2
50	#num#	k4
%	%	kIx~
z	z	k7c2
hmotnosti	hmotnost	k1gFnSc2
žloutku	žloutek	k1gInSc2
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
ji	on	k3xPp3gFnSc4
převážně	převážně	k6eAd1
proteiny	protein	k1gInPc1
a	a	k8xC
lipidy	lipid	k1gInPc1
(	(	kIx(
<g/>
v	v	k7c6
poměru	poměr	k1gInSc6
asi	asi	k9
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílkoviny	bílkovina	k1gFnPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
hlavně	hlavně	k9
ve	v	k7c6
formě	forma	k1gFnSc6
lipoproteinů	lipoprotein	k1gMnPc2
<g/>
,	,	kIx,
fosfoproteinů	fosfoprotein	k1gMnPc2
a	a	k8xC
glykoproteinů	glykoprotein	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuky	tuk	k1gInPc7
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
ve	v	k7c6
formě	forma	k1gFnSc6
jednoduchých	jednoduchý	k2eAgInPc2d1
tuků	tuk	k1gInPc2
<g/>
,	,	kIx,
fosfolipidů	fosfolipid	k1gInPc2
a	a	k8xC
lipoproteinů	lipoprotein	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skladba	skladba	k1gFnSc1
tuků	tuk	k1gInPc2
žloutku	žloutek	k1gInSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
značná	značný	k2eAgFnSc1d1
emulgace	emulgace	k1gFnSc1
zabezpečují	zabezpečovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnSc4
vysokou	vysoký	k2eAgFnSc4d1
stravitelnost	stravitelnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výživná	výživný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
a	a	k8xC
vlastnosti	vlastnost	k1gFnPc1
tuků	tuk	k1gInPc2
jsou	být	k5eAaImIp3nP
dány	dát	k5eAaPmNgFnP
skladbou	skladba	k1gFnSc7
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
,	,	kIx,
tvořených	tvořený	k2eAgInPc2d1
ze	z	k7c2
34	#num#	k4
%	%	kIx~
nasycenými	nasycený	k2eAgFnPc7d1
kyselinami	kyselina	k1gFnPc7
(	(	kIx(
<g/>
palmitovou	palmitový	k2eAgFnSc7d1
<g/>
,	,	kIx,
stearovou	stearový	k2eAgFnSc7d1
<g/>
)	)	kIx)
a	a	k8xC
ze	z	k7c2
66	#num#	k4
%	%	kIx~
kyselinami	kyselina	k1gFnPc7
nenasycenými	nasycený	k2eNgFnPc7d1
(	(	kIx(
<g/>
olejovou	olejový	k2eAgFnSc4d1
<g/>
,	,	kIx,
linolovou	linolový	k2eAgFnSc4d1
a	a	k8xC
linolenovou	linolenová	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnou	významný	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
vaječného	vaječný	k2eAgInSc2d1
žloutku	žloutek	k1gInSc2
je	být	k5eAaImIp3nS
cholesterol	cholesterol	k1gInSc1
<g/>
,	,	kIx,
nezbytný	nezbytný	k2eAgInSc1d1,k2eNgInSc1d1
pro	pro	k7c4
normální	normální	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
zárodku	zárodek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
jako	jako	k8xS,k8xC
volný	volný	k2eAgInSc1d1
(	(	kIx(
<g/>
asi	asi	k9
84	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
ve	v	k7c6
formě	forma	k1gFnSc6
esterů	ester	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
obsah	obsah	k1gInSc4
ve	v	k7c6
žloutku	žloutek	k1gInSc6
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
stálý	stálý	k2eAgInSc1d1
<g/>
,	,	kIx,
nezávislý	závislý	k2eNgInSc1d1
na	na	k7c6
krmné	krmný	k2eAgFnSc6d1
dávce	dávka	k1gFnSc6
ani	ani	k8xC
na	na	k7c6
druhu	druh	k1gInSc6
drůbeže	drůbež	k1gFnSc2
a	a	k8xC
činí	činit	k5eAaImIp3nS
v	v	k7c6
sušině	sušina	k1gFnSc6
žloutku	žloutek	k1gInSc2
200	#num#	k4
<g/>
–	–	k?
<g/>
350	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
žloutek	žloutek	k1gInSc1
nebo	nebo	k8xC
1100	#num#	k4
<g/>
–	–	k?
<g/>
1800	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
žloutku	žloutek	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejce	vejce	k1gNnSc1
křepelek	křepelka	k1gFnPc2
obsahuje	obsahovat	k5eAaImIp3nS
ca	ca	kA
<g/>
.	.	kIx.
1685	#num#	k4
mg	mg	kA
cholesterolu	cholesterol	k1gInSc2
ve	v	k7c6
100	#num#	k4
g	g	kA
žloutku	žloutek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
autorů	autor	k1gMnPc2
lze	lze	k6eAd1
jeho	jeho	k3xOp3gNnSc4
množství	množství	k1gNnSc4
ve	v	k7c6
žloutku	žloutek	k1gInSc6
snížit	snížit	k5eAaPmF
zkrmováním	zkrmování	k1gNnSc7
fytosterolů	fytosterol	k1gInPc2
(	(	kIx(
<g/>
sojový	sojový	k2eAgInSc1d1
olej	olej	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
selekcí	selekce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
minerálních	minerální	k2eAgFnPc2d1
látek	látka	k1gFnPc2
ve	v	k7c6
žloutku	žloutek	k1gInSc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
hlavně	hlavně	k9
volné	volný	k2eAgInPc4d1
ionty	ion	k1gInPc4
Na	na	k7c4
<g/>
,	,	kIx,
K	K	kA
a	a	k8xC
chloridy	chlorid	k1gInPc1
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
ostatní	ostatní	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
převážně	převážně	k6eAd1
vázány	vázat	k5eAaImNgFnP
na	na	k7c4
proteiny	protein	k1gInPc4
a	a	k8xC
fosfolipidy	fosfolipida	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vápník	vápník	k1gInSc1
<g/>
,	,	kIx,
železo	železo	k1gNnSc1
a	a	k8xC
hořčík	hořčík	k1gInSc1
jsou	být	k5eAaImIp3nP
vázány	vázat	k5eAaImNgFnP
fosvitinem	fosvitin	k1gInSc7
<g/>
,	,	kIx,
zinek	zinek	k1gInSc1
lipoviteliny	lipovitelina	k1gFnSc2
a	a	k8xC
síra	síra	k1gFnSc1
a	a	k8xC
fosfor	fosfor	k1gInSc1
fosfoproteiny	fosfoproteina	k1gFnSc2
a	a	k8xC
fosfolipidy	fosfolipida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
volný	volný	k2eAgInSc4d1
cukr	cukr	k1gInSc4
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
především	především	k9
D-glukóza	D-glukóza	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
98	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
žloutku	žloutek	k1gInSc6
jsou	být	k5eAaImIp3nP
mimo	mimo	k7c4
vitamín	vitamín	k1gInSc4
C	C	kA
obsaženy	obsáhnout	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
důležité	důležitý	k2eAgInPc1d1
vitamíny	vitamín	k1gInPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
obsah	obsah	k1gInSc1
však	však	k9
kolísá	kolísat	k5eAaImIp3nS
podle	podle	k7c2
kvality	kvalita	k1gFnSc2
výživy	výživa	k1gFnSc2
nosnic	nosnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vitamínů	vitamín	k1gInPc2
rozpustných	rozpustný	k2eAgInPc2d1
v	v	k7c6
tucích	tuk	k1gInPc6
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupit	k5eAaPmNgFnP
hlavně	hlavně	k9
vitamín	vitamín	k1gInSc1
A	A	kA
(	(	kIx(
<g/>
v	v	k7c6
1	#num#	k4
g	g	kA
žloutku	žloutek	k1gInSc2
průměrně	průměrně	k6eAd1
kolem	kolem	k7c2
30	#num#	k4
mj.	mj.	kA
<g/>
)	)	kIx)
a	a	k8xC
vitamín	vitamín	k1gInSc1
D3	D3	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
1	#num#	k4
g	g	kA
asi	asi	k9
1,5	1,5	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
m.	m.	k?
<g/>
j.	j.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInPc1d1
vitamíny	vitamín	k1gInPc1
jsou	být	k5eAaImIp3nP
obsaženy	obsažen	k2eAgMnPc4d1
v	v	k7c6
menším	malý	k2eAgNnSc6d2
množství	množství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barva	barva	k1gFnSc1
vaječného	vaječný	k2eAgInSc2d1
žloutku	žloutek	k1gInSc2
je	být	k5eAaImIp3nS
podmíněna	podmínit	k5eAaPmNgFnS
přítomností	přítomnost	k1gFnSc7
pigmentů	pigment	k1gInPc2
(	(	kIx(
<g/>
asi	asi	k9
0,2	0,2	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oranžově	oranžově	k6eAd1
žlutá	žlutý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
žloutku	žloutek	k1gInSc2
je	být	k5eAaImIp3nS
způsobena	způsobit	k5eAaPmNgFnS
přítomností	přítomnost	k1gFnSc7
malého	malý	k2eAgNnSc2d1
množství	množství	k1gNnSc2
karotenoidních	karotenoidní	k2eAgInPc2d1
pigmentů	pigment	k1gInPc2
(	(	kIx(
<g/>
kolem	kolem	k7c2
13-15	13-15	k4
µ	µ	k1gFnPc2
<g/>
/	/	kIx~
<g/>
žloutek	žloutek	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
především	především	k9
xantofyly	xantofyl	k1gInPc1
(	(	kIx(
<g/>
zeaxantin	zeaxantin	k1gInSc1
<g/>
,	,	kIx,
lutein	lutein	k1gInSc1
<g/>
,	,	kIx,
kryptoxantin	kryptoxantin	k1gInSc1
a	a	k8xC
karoten	karoten	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přirozeně	přirozeně	k6eAd1
se	se	k3xPyFc4
vyskytující	vyskytující	k2eAgMnSc1d1
v	v	k7c6
krmivu	krmivo	k1gNnSc6
drůbeže	drůbež	k1gFnSc2
(	(	kIx(
<g/>
žlutá	žlutý	k2eAgFnSc1d1
kukuřice	kukuřice	k1gFnSc1
<g/>
,	,	kIx,
sušená	sušený	k2eAgFnSc1d1
vojtěška	vojtěška	k1gFnSc1
<g/>
,	,	kIx,
senná	senný	k2eAgFnSc1d1
moučka	moučka	k1gFnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
jsou	být	k5eAaImIp3nP
přirozené	přirozený	k2eAgInPc1d1
pigmenty	pigment	k1gInPc1
nahrazovány	nahrazovat	k5eAaImNgInP
syntetickými	syntetický	k2eAgInPc7d1
(	(	kIx(
<g/>
Carophyl	Carophyl	k1gInSc1
<g/>
,	,	kIx,
Pigmental	Pigmental	k1gMnSc1
<g/>
,	,	kIx,
Papricolor	Papricolor	k1gMnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoké	vysoký	k2eAgFnPc4d1
dávky	dávka	k1gFnPc4
vitamínu	vitamín	k1gInSc2
A	a	k9
v	v	k7c6
krmné	krmný	k2eAgFnSc6d1
směsi	směs	k1gFnSc6
snižují	snižovat	k5eAaImIp3nP
pigmentaci	pigmentace	k1gFnSc4
žloutku	žloutek	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
obsah	obsah	k1gInSc1
vitamínu	vitamín	k1gInSc2
A	a	k9
ve	v	k7c6
žloutku	žloutek	k1gInSc6
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pigmentaci	pigmentace	k1gFnSc6
žloutku	žloutek	k1gInSc2
negativně	negativně	k6eAd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
onemocnění	onemocnění	k1gNnSc4
trávicího	trávicí	k2eAgInSc2d1
aparátu	aparát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intenzita	intenzita	k1gFnSc1
zbarvení	zbarvení	k1gNnSc2
žloutku	žloutek	k1gInSc2
není	být	k5eNaImIp3nS
ukazatelem	ukazatel	k1gInSc7
obsahu	obsah	k1gInSc2
vitamínu	vitamín	k1gInSc2
A	a	k9
a	a	k8xC
karotenu	karoten	k1gInSc2
ve	v	k7c6
žloutku	žloutek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Enzymů	enzym	k1gInPc2
obsahuje	obsahovat	k5eAaImIp3nS
žloutek	žloutek	k1gInSc1
málo	málo	k6eAd1
a	a	k8xC
vesměs	vesměs	k6eAd1
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjištěny	zjištěn	k2eAgFnPc4d1
byly	být	k5eAaImAgFnP
tributyrináza	tributyrináza	k1gFnSc1
<g/>
,	,	kIx,
peptidáza	peptidáza	k1gFnSc1
<g/>
,	,	kIx,
kataláza	kataláza	k1gFnSc1
<g/>
,	,	kIx,
amyláza	amyláza	k1gFnSc1
<g/>
,	,	kIx,
fosfatáza	fosfatáza	k1gFnSc1
<g/>
,	,	kIx,
esteráza	esteráza	k1gFnSc1
a	a	k8xC
proteinázy	proteináza	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
cytoplazmy	cytoplazma	k1gFnSc2
vajíčka	vajíčko	k1gNnSc2
nebo	nebo	k8xC
ze	z	k7c2
stěny	stěna	k1gFnSc2
folikulů	folikul	k1gInPc2
a	a	k8xC
do	do	k7c2
žloutku	žloutek	k1gInSc2
se	se	k3xPyFc4
dostávají	dostávat	k5eAaImIp3nP
v	v	k7c6
pozdějších	pozdní	k2eAgNnPc6d2
stadiích	stadion	k1gNnPc6
embryogeneze	embryogeneze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hormonů	hormon	k1gInPc2
byly	být	k5eAaImAgFnP
prokázány	prokázat	k5eAaPmNgFnP
ve	v	k7c6
žloutku	žloutek	k1gInSc6
estrogeny	estrogen	k1gInPc1
(	(	kIx(
<g/>
max	max	kA
<g/>
.	.	kIx.
0,3	0,3	k4
µ	µ	k1gInSc1
<g/>
/	/	kIx~
<g/>
žloutek	žloutek	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syntéza	syntéza	k1gFnSc1
látek	látka	k1gFnPc2
tvořících	tvořící	k2eAgFnPc2d1
žloutkovou	žloutkový	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
probíhá	probíhat	k5eAaImIp3nS
hlavně	hlavně	k9
v	v	k7c6
játrech	játra	k1gNnPc6
po	po	k7c4
jejich	jejich	k3xOp3gFnSc4
stimulaci	stimulace	k1gFnSc4
estrogeny	estrogen	k1gInPc1
při	při	k7c6
dosažení	dosažení	k1gNnSc6
pohlavní	pohlavní	k2eAgFnSc2d1
dospělosti	dospělost	k1gFnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
jsou	být	k5eAaImIp3nP
krevní	krevní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
deponovány	deponován	k2eAgInPc1d1
do	do	k7c2
folikulů	folikul	k1gInPc2
vaječníku	vaječník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
játra	játra	k1gNnPc1
i	i	k8xC
krevní	krevní	k2eAgFnSc1d1
plazma	plazma	k1gFnSc1
nosnic	nosnice	k1gFnPc2
obsahují	obsahovat	k5eAaImIp3nP
vyšší	vysoký	k2eAgFnSc3d2
koncentraci	koncentrace	k1gFnSc3
proteinů	protein	k1gInPc2
a	a	k8xC
lipidů	lipid	k1gInPc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
pohlavně	pohlavně	k6eAd1
nedospělými	dospělý	k2eNgMnPc7d1
jedinci	jedinec	k1gMnPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
látek	látka	k1gFnPc2
syntetizovaných	syntetizovaný	k2eAgFnPc2d1
de	de	k?
novo	nova	k1gFnSc5
(	(	kIx(
<g/>
lipoviteliny	lipovitelina	k1gFnPc4
a	a	k8xC
fosvitin	fosvitin	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Latebra	Latebra	k1gFnSc1
(	(	kIx(
<g/>
zárodečný	zárodečný	k2eAgInSc1d1
váček	váček	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kulovitý	kulovitý	k2eAgInSc4d1
prostor	prostor	k1gInSc4
ve	v	k7c6
středu	střed	k1gInSc6
žloutku	žloutek	k1gInSc2
o	o	k7c6
průměru	průměr	k1gInSc6
asi	asi	k9
6	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
vyplněný	vyplněný	k2eAgInSc1d1
původní	původní	k2eAgFnSc7d1
protoplazmou	protoplazma	k1gFnSc7
vaječné	vaječný	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
a	a	k8xC
světlým	světlý	k2eAgInSc7d1
žloutkem	žloutek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Latebra	Latebr	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
asi	asi	k9
0,6	0,6	k4
%	%	kIx~
hmotnosti	hmotnost	k1gFnSc2
žloutku	žloutek	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
krčkem	krček	k1gInSc7
latebry	latebra	k1gFnSc2
s	s	k7c7
povrchem	povrch	k1gInSc7
žloutkové	žloutkový	k2eAgFnSc2d1
koule	koule	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Panderovo	Panderův	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
je	být	k5eAaImIp3nS
rozšíření	rozšíření	k1gNnSc4
krčku	krček	k1gInSc2
latebry	latebra	k1gFnSc2
pod	pod	k7c7
povrchem	povrch	k1gInSc7
žloutkové	žloutkový	k2eAgFnSc2d1
koule	koule	k1gFnSc2
(	(	kIx(
<g/>
těsně	těsně	k6eAd1
pod	pod	k7c7
vitelinní	vitelinný	k2eAgMnPc1d1
membránou	membrána	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
ním	on	k3xPp3gNnSc7
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
zárodečný	zárodečný	k2eAgInSc1d1
terčík	terčík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zárodečný	zárodečný	k2eAgInSc1d1
terčík	terčík	k1gInSc1
(	(	kIx(
<g/>
disk	disk	k1gInSc1
<g/>
,	,	kIx,
očko	očko	k1gNnSc1
<g/>
)	)	kIx)
představuje	představovat	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc4d1
samičí	samičí	k2eAgFnSc4d1
zárodečnou	zárodečný	k2eAgFnSc4d1
buňku	buňka	k1gFnSc4
(	(	kIx(
<g/>
vajíčko	vajíčko	k1gNnSc1
<g/>
,	,	kIx,
ovum	ovum	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
uložen	uložit	k5eAaPmNgInS
bezprostředně	bezprostředně	k6eAd1
pod	pod	k7c7
žloutkovou	žloutkový	k2eAgFnSc7d1
membránou	membrána	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
bělavé	bělavý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
a	a	k8xC
kruhovitého	kruhovitý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
průměr	průměr	k1gInSc1
asi	asi	k9
2	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neoplozený	oplozený	k2eNgInSc1d1
zárodečný	zárodečný	k2eAgInSc1d1
terčík	terčík	k1gInSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
blastodisk	blastodisk	k1gInSc1
(	(	kIx(
<g/>
degenerovaná	degenerovaný	k2eAgFnSc1d1
samčí	samčí	k2eAgFnSc1d1
gameta	gameta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oplozený	oplozený	k2eAgInSc1d1
jako	jako	k8xS,k8xC
blastoderm	blastoderm	k1gInSc1
(	(	kIx(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS
vyvíjející	vyvíjející	k2eAgNnSc1d1
se	se	k3xPyFc4
embryo	embryo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
původní	původní	k2eAgFnSc7d1
cytoplazmou	cytoplazma	k1gFnSc7
oocytu	oocyt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
látek	látka	k1gFnPc2
a	a	k8xC
minerálů	minerál	k1gInPc2
</s>
<s>
Žloutek	žloutek	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
především	především	k9
z	z	k7c2
různých	různý	k2eAgInPc2d1
proteinů	protein	k1gInPc2
<g/>
,	,	kIx,
lipidů	lipid	k1gInPc2
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
dále	daleko	k6eAd2
fosfor	fosfor	k1gInSc4
a	a	k8xC
vápník	vápník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
vajíčka	vajíčko	k1gNnSc2
se	se	k3xPyFc4
dostávají	dostávat	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
živiny	živina	k1gFnPc4
především	především	k6eAd1
díky	díky	k7c3
viteleninu	vitelenin	k2eAgInSc3d1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
je	být	k5eAaImIp3nS
produkován	produkovat	k5eAaImNgMnS
v	v	k7c6
játrech	játra	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tabulka	tabulka	k1gFnSc1
udává	udávat	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
živin	živina	k1gFnPc2
<g/>
,	,	kIx,
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
vitamínů	vitamín	k1gInPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
nutričních	nutriční	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
zjištěných	zjištěný	k2eAgInPc2d1
ve	v	k7c6
slepičích	slepičí	k2eAgInPc6d1
vaječných	vaječný	k2eAgInPc6d1
žloutcích	žloutek	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Složka	složka	k1gFnSc1
</s>
<s>
Jednotka	jednotka	k1gFnSc1
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
</s>
<s>
Prvek	prvek	k1gInSc1
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
[	[	kIx(
<g/>
mg	mg	kA
/	/	kIx~
100	#num#	k4
g	g	kA
<g/>
]	]	kIx)
</s>
<s>
Složka	složka	k1gFnSc1
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
[	[	kIx(
<g/>
mg	mg	kA
/	/	kIx~
100	#num#	k4
g	g	kA
<g/>
]	]	kIx)
</s>
<s>
vodag	vodag	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
51,0	51,0	k4
<g/>
Na	na	k7c4
<g/>
50	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
C0	C0	k1gFnSc2
</s>
<s>
bílkovinyg	bílkovinyg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
16,1	16,1	k4
<g/>
K	k	k7c3
<g/>
120	#num#	k4
<g/>
vitamin	vitamin	k1gInSc1
D	D	kA
<g/>
0,0049	0,0049	k4
</s>
<s>
tukyg	tukyg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
30,5	30,5	k4
<g/>
Ca	ca	kA
<g/>
130	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
E	E	kA
<g/>
3,11	3,11	k4
</s>
<s>
cukryg	cukryg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
gstopyMg	gstopyMg	k1gInSc1
<g/>
15	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
B	B	kA
<g/>
60,30	60,30	k4
</s>
<s>
celkový	celkový	k2eAgInSc1d1
dusíkg	dusíkg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
2,58	2,58	k4
<g/>
P	P	kA
<g/>
500	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
B	B	kA
<g/>
120,006	120,006	k4
<g/>
9	#num#	k4
</s>
<s>
vlákninag	vlákninag	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
0	#num#	k4
<g/>
Fe	Fe	k1gFnSc2
<g/>
6,1	6,1	k4
<g/>
karotenstopy	karotenstopa	k1gFnSc2
</s>
<s>
mastné	mastné	k1gNnSc1
kyselinyg	kyselinyga	k1gFnPc2
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
25,3	25,3	k4
<g/>
Cu	Cu	k1gFnSc2
<g/>
0,15	0,15	k4
<g/>
thiamin	thiamin	k1gInSc1
<g/>
0,30	0,30	k4
</s>
<s>
cholesterolmg	cholesterolmg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
1120	#num#	k4
<g/>
Zn	zn	kA
<g/>
3,9	3,9	k4
<g/>
riboflavin	riboflavin	k1gInSc1
<g/>
0,54	0,54	k4
</s>
<s>
Semg	Semg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
0,020	0,020	k4
<g/>
I	i	k9
<g/>
0,14	0,14	k4
<g/>
niacin	niacina	k1gFnPc2
<g/>
0,1	0,1	k4
</s>
<s>
energiekJ	energiekJ	k?
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
1402	#num#	k4
<g/>
Mn	Mn	k1gFnSc2
<g/>
0,1	0,1	k4
<g/>
Cl	Cl	k1gFnSc1
<g/>
140	#num#	k4
</s>
<s>
Kulinářství	Kulinářství	k1gNnSc1
</s>
<s>
Ptačí	ptačit	k5eAaImIp3nS
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
právě	právě	k6eAd1
slepičí	slepičí	k2eAgNnPc4d1
vejce	vejce	k1gNnPc4
<g/>
,	,	kIx,
méně	málo	k6eAd2
často	často	k6eAd1
husí	husí	k2eAgFnSc1d1
<g/>
,	,	kIx,
kachní	kachnit	k5eAaImIp3nS
někdy	někdy	k6eAd1
i	i	k9
jiných	jiný	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
kulinářství	kulinářství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žloutek	žloutek	k1gInSc1
pak	pak	k6eAd1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
pojivo	pojivo	k1gNnSc1
ingrediencí	ingredience	k1gFnPc2
v	v	k7c6
těstě	těsto	k1gNnSc6
<g/>
,	,	kIx,
zahušťovadlo	zahušťovadlo	k1gNnSc1
polévek	polévka	k1gFnPc2
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
majonézy	majonéza	k1gFnSc2
apod.	apod.	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
BRAWAND	BRAWAND	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
Wahli	Wahle	k1gFnSc4
<g/>
,	,	kIx,
Henrik	Henrik	k1gMnSc1
Kaessmann	Kaessmann	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loss	Loss	k1gInSc1
of	of	k?
Egg	Egg	k1gFnSc2
Yolk	Yolk	k1gMnSc1
Genes	Genes	k1gMnSc1
in	in	k?
Mammals	Mammals	k1gInSc1
and	and	k?
the	the	k?
Origin	Origin	k1gInSc1
of	of	k?
Lactation	Lactation	k1gInSc1
and	and	k?
Placentation	Placentation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PLoS	plos	k1gMnSc1
Biology	biolog	k1gMnPc7
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
e	e	k0
<g/>
63	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.137	10.137	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
journal	journat	k5eAaPmAgInS,k5eAaImAgInS
<g/>
.	.	kIx.
<g/>
pbio	pbio	k6eAd1
<g/>
.0060063	.0060063	k4
<g/>
.	.	kIx.
↑	↑	k?
NOVOTNÝ	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Evžen	Evžen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Embryologie	embryologie	k1gFnSc1
pro	pro	k7c4
veterinární	veterinární	k2eAgMnPc4d1
mediky	medik	k1gMnPc4
–	–	k?
vysoká	vysoká	k1gFnSc1
škola	škola	k1gFnSc1
zemědělská	zemědělský	k2eAgFnSc1d1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SPN	SPN	kA
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
McCance	McCance	k1gFnSc1
a	a	k8xC
Widdowson	Widdowson	k1gNnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
<g/>
:	:	kIx,
<g/>
The	The	k1gMnSc2
Composition	Composition	k1gInSc1
of	of	k?
Foods	Foods	k1gInSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Summary	Summara	k1gFnPc1
edition	edition	k1gInSc1
<g/>
,	,	kIx,
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
Chemistry	Chemistr	k1gMnPc4
Cambridge	Cambridge	k1gFnSc2
a	a	k8xC
Food	Food	k1gInSc4
Standard	standard	k1gInSc1
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-85404-428-3	978-0-85404-428-3	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ŠATAVA	ŠATAVA	kA
<g/>
,	,	kIx,
M.	M.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chov	chov	k1gInSc1
drůbeže	drůbež	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SZN	SZN	kA
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
</s>
<s>
J.	J.	kA
Matthias	Matthias	k1gMnSc1
Starck	Starck	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morphology	Morpholog	k1gMnPc4
of	of	k?
the	the	k?
avian	avian	k1gMnSc1
yolk	yolk	k1gMnSc1
sac	sac	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Morphology	Morpholog	k1gMnPc7
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1002/jmor.21262	https://doi.org/10.1002/jmor.21262	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Bílek	Bílek	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
žloutek	žloutek	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
žloutek	žloutek	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
