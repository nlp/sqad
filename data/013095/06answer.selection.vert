<s>
Přehnaná	přehnaný	k2eAgFnSc1d1	přehnaná
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
sobě	se	k3xPyFc3	se
samému	samý	k3xTgMnSc3	samý
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
přehlížením	přehlížení	k1gNnSc7	přehlížení
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
narcismus	narcismus	k1gInSc1	narcismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
báje	báj	k1gFnSc2	báj
o	o	k7c6	o
krásném	krásný	k2eAgMnSc6d1	krásný
mladíkovi	mladík	k1gMnSc6	mladík
Narcisovi	Narcis	k1gMnSc6	Narcis
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
vlastního	vlastní	k2eAgInSc2d1	vlastní
odrazu	odraz	k1gInSc2	odraz
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
studánky	studánka	k1gFnSc2	studánka
<g/>
.	.	kIx.	.
</s>
