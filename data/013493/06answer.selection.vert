<s desamb="1">
K	k	k7c3
jejím	její	k3xOp3gInPc3
typickým	typický	k2eAgInPc3d1
rysům	rys	k1gInPc3
patří	patřit	k5eAaImIp3nS
hojné	hojný	k2eAgNnSc1d1
používání	používání	k1gNnSc1
koření	koření	k1gNnSc2
<g/>
,	,	kIx,
převaha	převaha	k1gFnSc1
vegetariánských	vegetariánský	k2eAgInPc2d1
pokrmů	pokrm	k1gInPc2
<g/>
,	,	kIx,
hlavními	hlavní	k2eAgFnPc7d1
surovinami	surovina	k1gFnPc7
jsou	být	k5eAaImIp3nP
rýže	rýže	k1gFnSc1
<g/>
,	,	kIx,
obilniny	obilnina	k1gFnPc1
<g/>
,	,	kIx,
luštěniny	luštěnina	k1gFnPc1
<g/>
,	,	kIx,
mléčné	mléčný	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
(	(	kIx(
<g/>
jogurty	jogurt	k1gInPc1
<g/>
,	,	kIx,
máslo	máslo	k1gNnSc1
ghí	ghí	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zelenina	zelenina	k1gFnSc1
a	a	k8xC
koření	koření	k1gNnSc1
<g/>
.	.	kIx.
</s>