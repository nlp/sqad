<s>
Indická	indický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Pokrm	pokrm	k1gInSc1
z	z	k7c2
Bengálska	Bengálsko	k1gNnSc2
</s>
<s>
Indická	indický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
je	být	k5eAaImIp3nS
kuchyně	kuchyně	k1gFnSc1
různých	různý	k2eAgNnPc2d1
etnik	etnikum	k1gNnPc2
a	a	k8xC
náboženských	náboženský	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
žijících	žijící	k2eAgFnPc2d1
na	na	k7c4
území	území	k1gNnSc4
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jejím	její	k3xOp3gInPc3
typickým	typický	k2eAgInPc3d1
rysům	rys	k1gInPc3
patří	patřit	k5eAaImIp3nS
hojné	hojný	k2eAgNnSc1d1
používání	používání	k1gNnSc1
koření	koření	k1gNnSc2
<g/>
,	,	kIx,
převaha	převaha	k1gFnSc1
vegetariánských	vegetariánský	k2eAgInPc2d1
pokrmů	pokrm	k1gInPc2
<g/>
,	,	kIx,
hlavními	hlavní	k2eAgFnPc7d1
surovinami	surovina	k1gFnPc7
jsou	být	k5eAaImIp3nP
rýže	rýže	k1gFnSc1
<g/>
,	,	kIx,
obilniny	obilnina	k1gFnPc1
<g/>
,	,	kIx,
luštěniny	luštěnina	k1gFnPc1
<g/>
,	,	kIx,
mléčné	mléčný	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
(	(	kIx(
<g/>
jogurty	jogurt	k1gInPc1
<g/>
,	,	kIx,
máslo	máslo	k1gNnSc1
ghí	ghí	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zelenina	zelenina	k1gFnSc1
a	a	k8xC
koření	koření	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Regionální	regionální	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
</s>
<s>
Indie	Indie	k1gFnSc1
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
země	země	k1gFnSc1
a	a	k8xC
mezi	mezi	k7c7
kuchařskými	kuchařský	k2eAgFnPc7d1
zvyklostmi	zvyklost	k1gFnPc7
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
regionech	region	k1gInPc6
jsou	být	k5eAaImIp3nP
značné	značný	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
těžké	těžký	k2eAgFnPc4d1
indickou	indický	k2eAgFnSc4d1
kuchyni	kuchyně	k1gFnSc4
charakterizovat	charakterizovat	k5eAaBmF
jako	jako	k8xC,k8xS
celek	celek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regionální	regionální	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
se	se	k3xPyFc4
od	od	k7c2
sebe	se	k3xPyFc2
odlišují	odlišovat	k5eAaImIp3nP
používanými	používaný	k2eAgFnPc7d1
surovinami	surovina	k1gFnPc7
<g/>
,	,	kIx,
přísadami	přísada	k1gFnPc7
i	i	k8xC
způsobem	způsob	k1gInSc7
přípravy	příprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
střední	střední	k2eAgFnSc6d1
a	a	k8xC
západní	západní	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc2
placky	placka	k1gFnSc2
<g/>
,	,	kIx,
luštěniny	luštěnina	k1gFnSc2
<g/>
,	,	kIx,
zelenina	zelenina	k1gFnSc1
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
mléčné	mléčný	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
ryby	ryba	k1gFnPc4
<g/>
,	,	kIx,
skopové	skopová	k1gFnPc4
<g/>
,	,	kIx,
kozí	kozí	k2eAgNnSc4d1
nebo	nebo	k8xC
drůbeží	drůbeží	k2eAgNnSc4d1
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
jedí	jíst	k5eAaImIp3nP
hlavně	hlavně	k9
muslimové	muslim	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kašmíru	Kašmír	k1gInSc6
a	a	k8xC
podhimálajské	podhimálajský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
jsou	být	k5eAaImIp3nP
v	v	k7c6
oblibě	obliba	k1gFnSc6
sýry	sýr	k1gInPc4
<g/>
,	,	kIx,
sušené	sušený	k2eAgNnSc1d1
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
plněné	plněný	k2eAgFnPc1d1
taštičky	taštička	k1gFnPc1
a	a	k8xC
luštěniny	luštěnina	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
hodně	hodně	k6eAd1
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
plodů	plod	k1gInPc2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
drůbeže	drůbež	k1gFnSc2
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnSc2
a	a	k8xC
rýže	rýže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jižní	jižní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
jsou	být	k5eAaImIp3nP
charakteristické	charakteristický	k2eAgFnPc4d1
sladkosti	sladkost	k1gFnPc4
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
hojné	hojný	k2eAgNnSc1d1
používání	používání	k1gNnSc1
kokosu	kokos	k1gInSc2
<g/>
,	,	kIx,
rýže	rýže	k1gFnSc2
a	a	k8xC
brambor	brambora	k1gFnPc2
či	či	k8xC
batátů	batáty	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoindické	Jihoindický	k2eAgInPc1d1
pokrmy	pokrm	k1gInPc1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
silně	silně	k6eAd1
kořeněné	kořeněný	k2eAgFnPc1d1
a	a	k8xC
pálivé	pálivý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
a	a	k8xC
střední	střední	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
hlavní	hlavní	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
kolem	kolem	k7c2
poledne	poledne	k1gNnSc2
nebo	nebo	k8xC
po	po	k7c6
poledni	poledne	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
až	až	k9
večer	večer	k6eAd1
<g/>
,	,	kIx,
po	po	k7c6
setmění	setmění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Náboženská	náboženský	k2eAgFnSc1d1
specifika	specifika	k1gFnSc1
</s>
<s>
Hinduisté	hinduista	k1gMnPc1
nikdy	nikdy	k6eAd1
nejedí	jíst	k5eNaImIp3nP
hovězí	hovězí	k2eAgNnSc4d1
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
příslušníci	příslušník	k1gMnPc1
vyšších	vysoký	k2eAgFnPc2d2
kast	kasta	k1gFnPc2
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
vegetariáni	vegetarián	k1gMnPc1
<g/>
,	,	kIx,
také	také	k9
všichni	všechen	k3xTgMnPc1
džinisté	džinista	k1gMnPc1
a	a	k8xC
podstatná	podstatný	k2eAgFnSc1d1
část	část	k1gFnSc1
indických	indický	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
a	a	k8xC
buddhistů	buddhista	k1gMnPc2
jsou	být	k5eAaImIp3nP
vegetariáni	vegetarián	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sikhové	sikh	k1gMnPc1
a	a	k8xC
muslimové	muslim	k1gMnPc1
jedí	jíst	k5eAaImIp3nP
více	hodně	k6eAd2
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
především	především	k9
drůbežího	drůbeží	k2eAgNnSc2d1
a	a	k8xC
skopového	skopové	k1gNnSc2
<g/>
,	,	kIx,
Sikhové	sikh	k1gMnPc1
však	však	k9
odmítají	odmítat	k5eAaImIp3nP
hovězí	hovězí	k2eAgNnSc1d1
maso	maso	k1gNnSc4
a	a	k8xC
na	na	k7c4
muslimy	muslim	k1gMnPc4
se	se	k3xPyFc4
vztahují	vztahovat	k5eAaImIp3nP
rituální	rituální	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
o	o	k7c6
čistotě	čistota	k1gFnSc6
(	(	kIx(
<g/>
halál	halál	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
proto	proto	k8xC
nikdy	nikdy	k6eAd1
nejedí	jíst	k5eNaImIp3nP
vepřové	vepřový	k2eAgNnSc4d1
maso	maso	k1gNnSc4
a	a	k8xC
zpravidla	zpravidla	k6eAd1
nepijí	pít	k5eNaImIp3nP
alkoholické	alkoholický	k2eAgInPc1d1
nápoje	nápoj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
pársové	párs	k1gMnPc1
nejsou	být	k5eNaImIp3nP
ve	v	k7c6
stravování	stravování	k1gNnSc6
limitováni	limitován	k2eAgMnPc1d1
náboženstvím	náboženství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Způsoby	způsob	k1gInPc1
úpravy	úprava	k1gFnSc2
pokrmů	pokrm	k1gInPc2
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3
úpravou	úprava	k1gFnSc7
pokrmů	pokrm	k1gInPc2
v	v	k7c6
indické	indický	k2eAgFnSc6d1
kuchyni	kuchyně	k1gFnSc6
je	být	k5eAaImIp3nS
pomalé	pomalý	k2eAgNnSc1d1
vaření	vaření	k1gNnSc1
nebo	nebo	k8xC
dušení	dušení	k1gNnSc1
na	na	k7c6
mírném	mírný	k2eAgInSc6d1
ohni	oheň	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbené	oblíbený	k2eAgInPc4d1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
smažení	smažení	k1gNnSc1
na	na	k7c6
pánvi	pánev	k1gFnSc6
<g/>
,	,	kIx,
grilování	grilování	k1gNnSc6
nebo	nebo	k8xC
pečení	pečení	k1gNnSc6
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
slouží	sloužit	k5eAaImIp3nS
hliněná	hliněný	k2eAgFnSc1d1
pec	pec	k1gFnSc1
tandoor	tandoora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maso	maso	k1gNnSc1
<g/>
,	,	kIx,
zelenina	zelenina	k1gFnSc1
i	i	k8xC
ostatní	ostatní	k2eAgFnPc1d1
přísady	přísada	k1gFnPc1
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
upravují	upravovat	k5eAaImIp3nP
pokrájené	pokrájený	k2eAgFnPc1d1
na	na	k7c4
kostky	kostka	k1gFnPc4
či	či	k8xC
plátky	plátek	k1gInPc4
<g/>
,	,	kIx,
méně	málo	k6eAd2
často	často	k6eAd1
v	v	k7c6
celku	celek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Stolování	stolování	k1gNnSc1
</s>
<s>
Indové	Ind	k1gMnPc1
tradičně	tradičně	k6eAd1
jedí	jíst	k5eAaImIp3nP
pravou	pravý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
<g/>
,	,	kIx,
často	často	k6eAd1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
kousku	kousek	k1gInSc2
placky	placka	k1gFnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
si	se	k3xPyFc3
nabírají	nabírat	k5eAaImIp3nP
do	do	k7c2
úst	ústa	k1gNnPc2
polévku	polévka	k1gFnSc4
či	či	k8xC
omáčku	omáčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nádobí	nádobí	k1gNnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
málo	málo	k1gNnSc1
<g/>
,	,	kIx,
místo	místo	k1gNnSc1
talířů	talíř	k1gInPc2
se	se	k3xPyFc4
často	často	k6eAd1
používají	používat	k5eAaImIp3nP
banánové	banánový	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
,	,	kIx,
spíchnuté	spíchnutý	k2eAgFnPc1d1
špejlemi	špejle	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
po	po	k7c6
jídle	jídlo	k1gNnSc6
zahodí	zahodit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stole	stol	k1gInSc6
nesmí	smět	k5eNaImIp3nS
chybět	chybět	k5eAaImF
různé	různý	k2eAgFnPc4d1
omáčky	omáčka	k1gFnPc4
<g/>
,	,	kIx,
čatní	čatní	k1gNnSc1
<g/>
,	,	kIx,
jogurt	jogurt	k1gInSc1
<g/>
,	,	kIx,
nakládaná	nakládaný	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
a	a	k8xC
koření	koření	k1gNnSc1
<g/>
,	,	kIx,
jimž	jenž	k3xRgFnPc3
si	se	k3xPyFc3
stolující	stolující	k2eAgMnPc1d1
mohou	moct	k5eAaImIp3nP
dochutit	dochutit	k5eAaPmF
jídlo	jídlo	k1gNnSc4
nebo	nebo	k8xC
do	do	k7c2
nich	on	k3xPp3gInPc2
namáčet	namáčet	k5eAaImF
jednotlivá	jednotlivý	k2eAgNnPc4d1
sousta	sousto	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indové	Ind	k1gMnPc1
servírují	servírovat	k5eAaBmIp3nP
na	na	k7c4
stůl	stůl	k1gInSc4
více	hodně	k6eAd2
jídel	jídlo	k1gNnPc2
zároveň	zároveň	k6eAd1
a	a	k8xC
stolující	stolující	k2eAgMnPc1d1
si	se	k3xPyFc3
nabírají	nabírat	k5eAaImIp3nP
na	na	k7c4
své	svůj	k3xOyFgInPc4
talíře	talíř	k1gInPc4
od	od	k7c2
každého	každý	k3xTgNnSc2
něco	něco	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polévka	polévka	k1gFnSc1
a	a	k8xC
hlavní	hlavní	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
se	se	k3xPyFc4
podávají	podávat	k5eAaImIp3nP
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
hlavním	hlavní	k2eAgNnSc6d1
jídle	jídlo	k1gNnSc6
se	se	k3xPyFc4
podávají	podávat	k5eAaImIp3nP
sladkosti	sladkost	k1gFnPc1
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnPc1
<g/>
,	,	kIx,
čaj	čaj	k1gInSc1
nebo	nebo	k8xC
káva	káva	k1gFnSc1
a	a	k8xC
rovněž	rovněž	k9
betelové	betelový	k2eAgInPc4d1
listy	list	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Přísady	přísada	k1gFnPc1
a	a	k8xC
suroviny	surovina	k1gFnPc1
indické	indický	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
</s>
<s>
Koření	kořenit	k5eAaImIp3nP
k	k	k7c3
dostání	dostání	k1gNnSc3
v	v	k7c6
Indii	Indie	k1gFnSc6
</s>
<s>
Čočka	čočka	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
základní	základní	k2eAgFnPc4d1
potraviny	potravina	k1gFnPc4
</s>
<s>
Chlebové	chlebový	k2eAgFnPc1d1
placky	placka	k1gFnPc1
se	s	k7c7
spoustou	spousta	k1gFnSc7
drobných	drobný	k2eAgInPc2d1
pamlsků	pamlsek	k1gInPc2
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1
potravin	potravina	k1gFnPc2
podle	podle	k7c2
ájurvédy	ájurvéda	k1gFnSc2
</s>
<s>
obilniny	obilnina	k1gFnSc2
(	(	kIx(
<g/>
rýže	rýže	k1gFnSc2
<g/>
,	,	kIx,
pšenice	pšenice	k1gFnSc2
<g/>
,	,	kIx,
proso	proso	k1gNnSc1
<g/>
,	,	kIx,
kukuřice	kukuřice	k1gFnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
</s>
<s>
luštěniny	luštěnina	k1gFnPc1
(	(	kIx(
<g/>
hrách	hrách	k1gInSc1
<g/>
,	,	kIx,
čočka	čočka	k1gFnSc1
<g/>
,	,	kIx,
mungo	mungo	k1gMnSc1
fazole	fazole	k1gFnSc1
<g/>
,	,	kIx,
cizrna	cizrna	k1gFnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
</s>
<s>
maso	maso	k1gNnSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
plodů	plod	k1gInPc2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
drůbeže	drůbež	k1gFnSc2
a	a	k8xC
vajec	vejce	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
listová	listový	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
a	a	k8xC
jedlé	jedlý	k2eAgFnPc1d1
natě	nať	k1gFnPc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgFnPc1d1
části	část	k1gFnPc1
rostlin	rostlina	k1gFnPc2
</s>
<s>
ovoce	ovoce	k1gNnSc1
a	a	k8xC
plodová	plodový	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
(	(	kIx(
<g/>
baklažány	baklažán	k1gInPc1
<g/>
,	,	kIx,
rajčata	rajče	k1gNnPc1
<g/>
,	,	kIx,
papriky	paprika	k1gFnPc1
<g/>
,	,	kIx,
okurky	okurek	k1gInPc1
<g/>
,	,	kIx,
týkve	týkev	k1gFnPc1
aj.	aj.	kA
<g/>
)	)	kIx)
</s>
<s>
kořenová	kořenový	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
a	a	k8xC
jedlé	jedlý	k2eAgFnPc1d1
houby	houba	k1gFnPc1
</s>
<s>
opojné	opojný	k2eAgInPc4d1
nápoje	nápoj	k1gInPc4
</s>
<s>
voda	voda	k1gFnSc1
</s>
<s>
mléko	mléko	k1gNnSc1
a	a	k8xC
mléčné	mléčný	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
</s>
<s>
cukr	cukr	k1gInSc1
<g/>
,	,	kIx,
med	med	k1gInSc1
<g/>
,	,	kIx,
cukrová	cukrový	k2eAgFnSc1d1
třtina	třtina	k1gFnSc1
<g/>
,	,	kIx,
palmový	palmový	k2eAgInSc1d1
cukr	cukr	k1gInSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
sladidla	sladidlo	k1gNnPc1
</s>
<s>
připravené	připravený	k2eAgInPc4d1
pokrmy	pokrm	k1gInPc4
<g/>
:	:	kIx,
oleje	olej	k1gInPc4
<g/>
,	,	kIx,
ocet	ocet	k1gInSc1
<g/>
,	,	kIx,
čatní	čatní	k1gNnPc1
<g/>
,	,	kIx,
zavařeniny	zavařenina	k1gFnPc1
<g/>
,	,	kIx,
ovocné	ovocný	k2eAgFnPc1d1
šťávy	šťáva	k1gFnPc1
aj.	aj.	kA
</s>
<s>
koření	koření	k1gNnSc1
a	a	k8xC
sůl	sůl	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
suroviny	surovina	k1gFnPc1
používané	používaný	k2eAgFnPc4d1
v	v	k7c6
indické	indický	k2eAgFnSc6d1
kuchyni	kuchyně	k1gFnSc6
</s>
<s>
asa	asa	k?
foetida	foetida	k1gFnSc1
</s>
<s>
baklažány	baklažán	k1gInPc1
</s>
<s>
betelové	betelový	k2eAgInPc1d1
listy	list	k1gInPc1
</s>
<s>
cizrna	cizrna	k1gFnSc1
</s>
<s>
čát	čát	k?
masála	masála	k1gFnSc1
</s>
<s>
česnek	česnek	k1gInSc1
</s>
<s>
čočka	čočka	k1gFnSc1
a	a	k8xC
fazole	fazole	k1gFnSc1
mungo	mungo	k1gMnSc1
</s>
<s>
garam	garam	k6eAd1
masála	masála	k1gFnSc1
</s>
<s>
hořčičné	hořčičný	k2eAgNnSc1d1
semínko	semínko	k1gNnSc1
</s>
<s>
jogurt	jogurt	k1gInSc1
</s>
<s>
kardamom	kardamom	k1gInSc1
</s>
<s>
karí	karí	k1gNnSc1
</s>
<s>
kokos	kokos	k1gInSc1
</s>
<s>
koriandr	koriandr	k1gInSc1
<g/>
:	:	kIx,
semeno	semeno	k1gNnSc1
i	i	k8xC
listy	list	k1gInPc1
</s>
<s>
kurkuma	kurkuma	k1gFnSc1
</s>
<s>
mango	mango	k1gNnSc1
</s>
<s>
máslo	máslo	k1gNnSc1
ghí	ghí	k?
</s>
<s>
pepř	pepř	k1gInSc1
</s>
<s>
růžová	růžový	k2eAgFnSc1d1
voda	voda	k1gFnSc1
</s>
<s>
rýže	rýže	k1gFnSc1
</s>
<s>
zázvor	zázvor	k1gInSc1
</s>
<s>
Indická	indický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
a	a	k8xC
lékařství	lékařství	k1gNnSc1
</s>
<s>
Indická	indický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
úzkém	úzký	k2eAgInSc6d1
vztahu	vztah	k1gInSc6
s	s	k7c7
tradičním	tradiční	k2eAgNnSc7d1
indickým	indický	k2eAgNnSc7d1
lékařstvím	lékařství	k1gNnSc7
–	–	k?
ájurvédou	ájurvéda	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
zejména	zejména	k9
v	v	k7c6
používání	používání	k1gNnSc6
koření	koření	k1gNnSc2
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnSc2
a	a	k8xC
přísad	přísada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
ájurvédy	ájurvéda	k1gFnSc2
jídlo	jídlo	k1gNnSc1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
tří	tři	k4xCgFnPc2
tělesných	tělesný	k2eAgFnPc2d1
složek	složka	k1gFnPc2
(	(	kIx(
<g/>
tridóša	tridóša	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
větru	vítr	k1gInSc2
(	(	kIx(
<g/>
váta	vát	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žluči	žluč	k1gFnSc2
(	(	kIx(
<g/>
pitta	pitt	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
hlenu	hlen	k1gInSc2
(	(	kIx(
<g/>
kapha	kapha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
některé	některý	k3yIgFnPc1
potraviny	potravina	k1gFnPc1
vzájemně	vzájemně	k6eAd1
nemají	mít	k5eNaImIp3nP
kombinovat	kombinovat	k5eAaImF
a	a	k8xC
při	při	k7c6
některých	některý	k3yIgFnPc6
chorobách	choroba	k1gFnPc6
se	se	k3xPyFc4
nedoporučuje	doporučovat	k5eNaImIp3nS
požívání	požívání	k1gNnSc1
některých	některý	k3yIgFnPc2
potravin	potravina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nejznámější	známý	k2eAgInPc4d3
indické	indický	k2eAgInPc4d1
pokrmy	pokrm	k1gInPc4
a	a	k8xC
nápoje	nápoj	k1gInPc4
</s>
<s>
kuře	kuře	k1gNnSc1
tandoori	tandoor	k1gFnSc2
je	být	k5eAaImIp3nS
populární	populární	k2eAgNnSc1d1
pečené	pečený	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
</s>
<s>
Thali	Thal	k1gMnPc1
(	(	kIx(
<g/>
podnos	podnos	k1gInSc1
<g/>
)	)	kIx)
z	z	k7c2
Uttarpradéše	Uttarpradéš	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
je	být	k5eAaImIp3nS
naan	naan	k1gMnSc1
<g/>
,	,	kIx,
daal	daal	k1gMnSc1
<g/>
,	,	kIx,
raita	raita	k1gMnSc1
<g/>
,	,	kIx,
shahi	shahi	k6eAd1
paneer	paneer	k1gInSc1
a	a	k8xC
salát	salát	k1gInSc1
</s>
<s>
Vegetariánský	vegetariánský	k2eAgInSc1d1
pokrm	pokrm	k1gInSc1
ze	z	k7c2
státu	stát	k1gInSc2
Ándhrapradéš	Ándhrapradéš	k1gInSc4
podávaný	podávaný	k2eAgInSc4d1
při	při	k7c6
důležitých	důležitý	k2eAgFnPc6d1
událostech	událost	k1gFnPc6
</s>
<s>
Dosa	Dosa	k1gFnSc1
podávaná	podávaný	k2eAgFnSc1d1
se	s	k7c7
sambarem	sambar	k1gInSc7
a	a	k8xC
čatní	čatní	k1gNnSc7
<g/>
,	,	kIx,
populární	populární	k2eAgFnPc1d1
na	na	k7c6
jihu	jih	k1gInSc6
Indie	Indie	k1gFnSc2
</s>
<s>
Kořeněná	kořeněný	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
ze	z	k7c2
státu	stát	k1gInSc2
Kérala	Kéral	k1gMnSc2
</s>
<s>
V	v	k7c4
Karnátace	Karnátace	k1gFnPc4
podavané	podavaný	k2eAgNnSc4d1
jídlo	jídlo	k1gNnSc4
na	na	k7c6
banánovém	banánový	k2eAgInSc6d1
listu	list	k1gInSc6
</s>
<s>
Polévky	polévka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Milanguttanír	Milanguttanír	k1gInSc1
<g/>
:	:	kIx,
jihoindická	jihoindický	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
z	z	k7c2
rýže	rýže	k1gFnSc2
<g/>
,	,	kIx,
kokosu	kokos	k1gInSc2
<g/>
,	,	kIx,
zeleniny	zelenina	k1gFnSc2
<g/>
,	,	kIx,
koření	koření	k1gNnSc2
a	a	k8xC
kuřecího	kuřecí	k2eAgNnSc2d1
nebo	nebo	k8xC
jehněčího	jehněčí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rassam	Rassam	k1gInSc1
<g/>
:	:	kIx,
pálivá	pálivý	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
ze	z	k7c2
zeleniny	zelenina	k1gFnSc2
(	(	kIx(
<g/>
rajčata	rajče	k1gNnPc4
<g/>
,	,	kIx,
papriky	paprika	k1gFnPc4
<g/>
,	,	kIx,
cibule	cibule	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česneku	česnek	k1gInSc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Šórba	Šórba	k1gFnSc1
<g/>
:	:	kIx,
severoindická	severoindický	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
s	s	k7c7
masem	maso	k1gNnSc7
<g/>
,	,	kIx,
luštěninami	luštěnina	k1gFnPc7
a	a	k8xC
cibulí	cibule	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Přílohy	příloha	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Čápátí	Čápátit	k5eAaPmIp3nP
<g/>
:	:	kIx,
tenké	tenký	k2eAgInPc1d1
placky	placek	k1gInPc1
z	z	k7c2
nekynutého	kynutý	k2eNgNnSc2d1
těsta	těsto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Dósa	Dósa	k1gFnSc1
<g/>
:	:	kIx,
smažené	smažený	k2eAgFnSc2d1
placičky	placička	k1gFnSc2
z	z	k7c2
rýže	rýže	k1gFnSc2
<g/>
,	,	kIx,
čočky	čočka	k1gFnSc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Idli	Idli	k1gNnSc1
<g/>
:	:	kIx,
rýžové	rýžový	k2eAgFnSc2d1
placičky	placička	k1gFnSc2
<g/>
,	,	kIx,
vařené	vařený	k2eAgFnSc2d1
v	v	k7c6
páře	pára	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Nán	Nána	k1gFnPc2
<g/>
:	:	kIx,
placky	placka	k1gFnPc1
z	z	k7c2
kynutého	kynutý	k2eAgNnSc2d1
těsta	těsto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
zadělané	zadělaný	k2eAgInPc1d1
s	s	k7c7
přídavkem	přídavek	k1gInSc7
jogurtu	jogurt	k1gInSc2
<g/>
,	,	kIx,
tlustší	tlustý	k2eAgInSc1d2
a	a	k8xC
měkčí	měkký	k2eAgInSc1d2
než	než	k8xS
čapátí	čapátit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pápar	Pápar	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
páppadam	páppadam	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
velmi	velmi	k6eAd1
tenké	tenký	k2eAgInPc1d1
smažené	smažený	k2eAgInPc1d1
placky	placek	k1gInPc1
<g/>
,	,	kIx,
podávají	podávat	k5eAaImIp3nP
se	se	k3xPyFc4
podobně	podobně	k6eAd1
jako	jako	k9
chipsy	chips	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Purí	Purit	k5eAaBmIp3nP,k5eAaPmIp3nP,k5eAaImIp3nP
<g/>
:	:	kIx,
smažené	smažený	k2eAgFnPc1d1
kulaté	kulatý	k2eAgFnPc1d1
placičky	placička	k1gFnPc1
<g/>
,	,	kIx,
podobné	podobný	k2eAgFnPc1d1
lívancům	lívanec	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Vařená	vařený	k2eAgFnSc1d1
rýže	rýže	k1gFnSc1
<g/>
:	:	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
obarvená	obarvený	k2eAgFnSc1d1
nažluto	nažluto	k6eAd1
přidáním	přidání	k1gNnSc7
kurkumy	kurkuma	k1gFnSc2
</s>
<s>
Vegetariánská	vegetariánský	k2eAgNnPc1d1
jídla	jídlo	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
Alú	Alú	k?
gobhi	gobhi	k1gNnSc1
<g/>
:	:	kIx,
pokrm	pokrm	k1gInSc1
z	z	k7c2
brambor	brambora	k1gFnPc2
<g/>
,	,	kIx,
květáku	květák	k1gInSc2
a	a	k8xC
jiné	jiný	k2eAgFnSc2d1
zeleniny	zelenina	k1gFnSc2
(	(	kIx(
<g/>
cibule	cibule	k1gFnSc2
<g/>
,	,	kIx,
rajčata	rajče	k1gNnPc4
<g/>
,	,	kIx,
česnek	česnek	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dál	dál	k1gFnSc1
<g/>
:	:	kIx,
kaše	kaše	k1gFnSc1
z	z	k7c2
luštěnin	luštěnina	k1gFnPc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
čočky	čočka	k1gFnSc2
<g/>
,	,	kIx,
cizrny	cizrna	k1gFnSc2
nebo	nebo	k8xC
fazolí	fazole	k1gFnPc2
mungo	mungo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Khičrí	Khičrí	k1gFnSc1
<g/>
:	:	kIx,
pokrm	pokrm	k1gInSc1
z	z	k7c2
rýže	rýže	k1gFnSc2
<g/>
,	,	kIx,
luštěnin	luštěnina	k1gFnPc2
<g/>
,	,	kIx,
másla	máslo	k1gNnSc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Koottu	Kootta	k1gFnSc4
<g/>
:	:	kIx,
jihoindický	jihoindický	k2eAgInSc1d1
pokrm	pokrm	k1gInSc1
z	z	k7c2
čočky	čočka	k1gFnSc2
<g/>
,	,	kIx,
cizrny	cizrna	k1gFnSc2
<g/>
,	,	kIx,
zeleniny	zelenina	k1gFnSc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pakora	Pakora	k1gFnSc1
<g/>
:	:	kIx,
pečené	pečený	k2eAgFnPc1d1
taštičky	taštička	k1gFnPc1
<g/>
,	,	kIx,
plněné	plněný	k2eAgFnPc1d1
zeleninou	zelenina	k1gFnSc7
nebo	nebo	k8xC
luštěninami	luštěnina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Panír	Panír	k1gInSc1
<g/>
:	:	kIx,
indický	indický	k2eAgInSc1d1
tvarohový	tvarohový	k2eAgInSc1d1
sýr	sýr	k1gInSc1
<g/>
,	,	kIx,
připomíná	připomínat	k5eAaImIp3nS
balkánský	balkánský	k2eAgInSc1d1
sýr	sýr	k1gInSc1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
však	však	k9
tolik	tolik	k6eAd1
slaný	slaný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Plněné	plněný	k2eAgInPc1d1
baklažány	baklažán	k1gInPc1
<g/>
:	:	kIx,
vydlabané	vydlabaný	k2eAgNnSc1d1
a	a	k8xC
naplněné	naplněný	k2eAgNnSc1d1
okořeněnou	okořeněný	k2eAgFnSc7d1
dužinou	dužina	k1gFnSc7
a	a	k8xC
zeleninou	zelenina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Ság	sága	k1gFnPc2
<g/>
:	:	kIx,
špenát	špenát	k1gInSc1
<g/>
,	,	kIx,
podobný	podobný	k2eAgInSc1d1
jako	jako	k9
v	v	k7c6
české	český	k2eAgFnSc6d1
kuchyni	kuchyně	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
více	hodně	k6eAd2
kořeněný	kořeněný	k2eAgInSc4d1
a	a	k8xC
podávaný	podávaný	k2eAgInSc4d1
s	s	k7c7
plackami	placka	k1gFnPc7
nebo	nebo	k8xC
se	s	k7c7
sýrem	sýr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Samósa	Samósa	k1gFnSc1
<g/>
:	:	kIx,
smažené	smažený	k2eAgFnSc2d1
taštičky	taštička	k1gFnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
trojúhelníkové	trojúhelníkový	k2eAgNnSc1d1
<g/>
,	,	kIx,
plněné	plněný	k2eAgNnSc1d1
zeleninou	zelenina	k1gFnSc7
<g/>
,	,	kIx,
bramborami	brambora	k1gFnPc7
<g/>
,	,	kIx,
sýrem	sýr	k1gInSc7
aj.	aj.	kA
</s>
<s>
Uppuma	Uppuma	k1gFnSc1
<g/>
:	:	kIx,
slaná	slaný	k2eAgFnSc1d1
krupičná	krupičný	k2eAgFnSc1d1
kaše	kaše	k1gFnSc1
s	s	k7c7
kořením	koření	k1gNnSc7
a	a	k8xC
zeleninou	zelenina	k1gFnSc7
<g/>
,	,	kIx,
připomínající	připomínající	k2eAgInSc1d1
severoafrický	severoafrický	k2eAgInSc1d1
kuskus	kuskus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jídla	jídlo	k1gNnPc1
z	z	k7c2
masa	maso	k1gNnSc2
a	a	k8xC
drůbeže	drůbež	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Birjání	Birjánět	k5eAaImIp3nP
<g/>
:	:	kIx,
zapékaný	zapékaný	k2eAgInSc4d1
pokrm	pokrm	k1gInSc4
<g/>
,	,	kIx,
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
z	z	k7c2
vrstev	vrstva	k1gFnPc2
rýže	rýže	k1gFnSc2
a	a	k8xC
masové	masový	k2eAgFnSc2d1
nebo	nebo	k8xC
zeleninové	zeleninový	k2eAgFnSc2d1
směsi	směs	k1gFnSc2
s	s	k7c7
kořením	koření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Kabáb	Kabáb	k1gInSc1
<g/>
:	:	kIx,
grilované	grilovaný	k2eAgInPc1d1
kousky	kousek	k1gInPc1
masa	maso	k1gNnSc2
nebo	nebo	k8xC
kuličky	kulička	k1gFnPc1
z	z	k7c2
mletého	mletý	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
cibule	cibule	k1gFnSc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
,	,	kIx,
oblíbený	oblíbený	k2eAgInSc4d1
pokrm	pokrm	k1gInSc4
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Karí	karí	k1gNnSc1
<g/>
:	:	kIx,
není	být	k5eNaImIp3nS
jen	jen	k9
směs	směs	k1gFnSc1
koření	koření	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
kořeněný	kořeněný	k2eAgInSc4d1
pokrm	pokrm	k1gInSc4
z	z	k7c2
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
zeleniny	zelenina	k1gFnSc2
či	či	k8xC
luštěnin	luštěnina	k1gFnPc2
<g/>
,	,	kIx,
dušených	dušený	k2eAgFnPc2d1
v	v	k7c6
omáčce	omáčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kófty	Kófta	k1gFnPc1
<g/>
:	:	kIx,
kuličky	kulička	k1gFnPc1
z	z	k7c2
mletého	mletý	k2eAgNnSc2d1
masa	maso	k1gNnSc2
nebo	nebo	k8xC
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
podávané	podávaný	k2eAgInPc1d1
v	v	k7c6
omáčce	omáčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kórma	Kórmum	k1gNnPc4
<g/>
:	:	kIx,
dušené	dušený	k2eAgNnSc4d1
drůbeží	drůbeží	k2eAgNnSc4d1
nebo	nebo	k8xC
jiné	jiný	k2eAgNnSc4d1
maso	maso	k1gNnSc4
na	na	k7c6
šťávě	šťáva	k1gFnSc6
<g/>
,	,	kIx,
ochucené	ochucený	k2eAgFnSc2d1
jogurtem	jogurt	k1gInSc7
a	a	k8xC
kořením	koření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Maláí	Maláí	k1gFnSc1
<g/>
:	:	kIx,
pokrm	pokrm	k1gInSc1
z	z	k7c2
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
ryb	ryba	k1gFnPc2
nebo	nebo	k8xC
krevet	kreveta	k1gFnPc2
<g/>
,	,	kIx,
vařených	vařený	k2eAgInPc2d1
s	s	k7c7
kokosem	kokos	k1gInSc7
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
nasládlou	nasládlý	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Puláo	Puláo	k1gNnSc1
<g/>
:	:	kIx,
pokrm	pokrm	k1gInSc1
z	z	k7c2
rýže	rýže	k1gFnSc2
<g/>
,	,	kIx,
vařené	vařený	k2eAgFnPc1d1
společně	společně	k6eAd1
se	s	k7c7
zeleninou	zelenina	k1gFnSc7
<g/>
,	,	kIx,
kořením	koření	k1gNnSc7
<g/>
,	,	kIx,
oříšky	oříšek	k1gInPc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
masem	maso	k1gNnSc7
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
nevyvaří	vyvařit	k5eNaPmIp3nS
všechna	všechen	k3xTgFnSc1
tekutina	tekutina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdoba	obdoba	k1gFnSc1
tureckého	turecký	k2eAgInSc2d1
pilafu	pilaf	k1gInSc2
a	a	k8xC
středoasijského	středoasijský	k2eAgNnSc2d1
plovu	plovat	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s>
Singhův	Singhův	k2eAgInSc1d1
turban	turban	k1gInSc1
<g/>
:	:	kIx,
sikhská	sikhský	k2eAgFnSc1d1
specialita	specialita	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jakýsi	jakýsi	k3yIgInSc1
dort	dort	k1gInSc1
z	z	k7c2
vařené	vařený	k2eAgFnSc2d1
rýže	rýže	k1gFnSc2
<g/>
,	,	kIx,
plněný	plněný	k2eAgInSc1d1
nádivkou	nádivka	k1gFnSc7
ze	z	k7c2
skopového	skopový	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
zeleniny	zelenina	k1gFnSc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Tandoori	Tandoori	k1gNnSc1
<g/>
:	:	kIx,
kořeněné	kořeněný	k2eAgNnSc1d1
kuře	kuře	k1gNnSc1
<g/>
,	,	kIx,
grilované	grilovaný	k2eAgNnSc1d1
v	v	k7c6
peci	pec	k1gFnSc6
tandúr	tandúra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tikka	Tikka	k1gFnSc1
<g/>
:	:	kIx,
pokrm	pokrm	k1gInSc1
z	z	k7c2
kousků	kousek	k1gInPc2
kuřete	kuře	k1gNnSc2
<g/>
,	,	kIx,
grilovaných	grilovaný	k2eAgInPc6d1
s	s	k7c7
kořením	koření	k1gNnSc7
a	a	k8xC
česnekem	česnek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Vindaloo	Vindaloo	k1gNnSc1
<g/>
:	:	kIx,
pokrm	pokrm	k1gInSc1
jihoindické	jihoindický	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
z	z	k7c2
vepřového	vepřový	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
česneku	česnek	k1gInSc2
<g/>
,	,	kIx,
octa	ocet	k1gInSc2
a	a	k8xC
chilli	chilli	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
kuře	kuře	k1gNnSc1
Tikka	Tikek	k1gMnSc2
Masala	Masal	k1gMnSc2
<g/>
:	:	kIx,
kuře	kuře	k1gNnSc1
s	s	k7c7
tikka	tikka	k6eAd1
omáčkou	omáčka	k1gFnSc7
</s>
<s>
Saláty	salát	k1gInPc1
<g/>
,	,	kIx,
omáčky	omáčka	k1gFnPc1
a	a	k8xC
přísady	přísada	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Avial	Avial	k1gInSc1
<g/>
:	:	kIx,
salát	salát	k1gInSc1
z	z	k7c2
rýže	rýže	k1gFnSc2
<g/>
,	,	kIx,
baklažánů	baklažán	k1gInPc2
<g/>
,	,	kIx,
cibule	cibule	k1gFnSc2
a	a	k8xC
jiné	jiný	k2eAgFnSc2d1
zeleniny	zelenina	k1gFnSc2
s	s	k7c7
kokosovým	kokosový	k2eAgNnSc7d1
mlékem	mléko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Čatní	čatní	k1gNnSc1
<g/>
:	:	kIx,
silně	silně	k6eAd1
kořeněná	kořeněný	k2eAgFnSc1d1
zavařenina	zavařenina	k1gFnSc1
z	z	k7c2
manga	mango	k1gNnSc2
nebo	nebo	k8xC
jiného	jiný	k2eAgNnSc2d1
ovoce	ovoce	k1gNnSc2
či	či	k8xC
zeleniny	zelenina	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
rajčat	rajče	k1gNnPc2
<g/>
,	,	kIx,
jablek	jablko	k1gNnPc2
<g/>
,	,	kIx,
paprik	paprika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Čjavanpraš	Čjavanpraš	k1gInSc1
<g/>
:	:	kIx,
zavařenina	zavařenina	k1gFnSc1
z	z	k7c2
plodů	plod	k1gInPc2
embliky	emblika	k1gFnSc2
(	(	kIx(
<g/>
Emblica	Emblic	k2eAgFnSc1d1
officinalis	officinalis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
baelu	baelat	k5eAaPmIp1nS
(	(	kIx(
<g/>
Aegle	Aegle	k1gFnSc1
marmelos	marmelos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bylinek	bylinka	k1gFnPc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
léčivé	léčivý	k2eAgInPc4d1
účinky	účinek	k1gInPc4
na	na	k7c4
posílení	posílení	k1gNnSc4
imunitního	imunitní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Raita	Raita	k1gFnSc1
<g/>
:	:	kIx,
omáčka	omáčka	k1gFnSc1
nebo	nebo	k8xC
řídký	řídký	k2eAgInSc1d1
salát	salát	k1gInSc1
z	z	k7c2
jogurtu	jogurt	k1gInSc2
<g/>
,	,	kIx,
bylinek	bylinka	k1gFnPc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
zeleniny	zelenina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sámbar	Sámbar	k1gInSc1
<g/>
:	:	kIx,
omáčka	omáčka	k1gFnSc1
nebo	nebo	k8xC
řídký	řídký	k2eAgInSc1d1
salát	salát	k1gInSc1
z	z	k7c2
čočky	čočka	k1gFnSc2
nebo	nebo	k8xC
jiné	jiný	k2eAgFnSc2d1
luštěniny	luštěnina	k1gFnSc2
<g/>
,	,	kIx,
tamarindu	tamarind	k1gInSc2
<g/>
,	,	kIx,
jogurtu	jogurt	k1gInSc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Dezerty	dezert	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Barfí	Barfí	k1gFnSc1
<g/>
:	:	kIx,
indická	indický	k2eAgFnSc1d1
sladkost	sladkost	k1gFnSc1
ze	z	k7c2
smetany	smetana	k1gFnSc2
nebo	nebo	k8xC
kondenzovaného	kondenzovaný	k2eAgNnSc2d1
mléka	mléko	k1gNnSc2
<g/>
,	,	kIx,
cukru	cukr	k1gInSc2
<g/>
,	,	kIx,
oříšků	oříšek	k1gInPc2
či	či	k8xC
kokosu	kokos	k1gInSc2
a	a	k8xC
růžové	růžový	k2eAgFnSc2d1
vody	voda	k1gFnSc2
</s>
<s>
Džalébí	Džalébit	k5eAaPmIp3nP
<g/>
:	:	kIx,
malé	malý	k2eAgInPc1d1
<g/>
,	,	kIx,
sladké	sladký	k2eAgInPc1d1
<g/>
,	,	kIx,
smažené	smažený	k2eAgInPc1d1
preclíky	preclík	k1gInPc1
<g/>
,	,	kIx,
máčené	máčený	k2eAgInPc1d1
v	v	k7c6
cukrovém	cukrový	k2eAgInSc6d1
sirupu	sirup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Gadžár	Gadžár	k1gMnSc1
ka	ka	k?
halva	halv	k1gMnSc4
<g/>
:	:	kIx,
sladký	sladký	k2eAgInSc4d1
nákyp	nákyp	k1gInSc4
z	z	k7c2
mrkve	mrkev	k1gFnSc2
<g/>
,	,	kIx,
mléka	mléko	k1gNnSc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Guláb	Guláb	k1gMnSc1
džamún	džamún	k1gMnSc1
<g/>
:	:	kIx,
sladké	sladký	k2eAgFnPc1d1
smažené	smažený	k2eAgFnPc1d1
kuličky	kulička	k1gFnPc1
s	s	k7c7
mandlemi	mandle	k1gFnPc7
a	a	k8xC
růžovou	růžový	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Khír	Khír	k1gInSc1
<g/>
:	:	kIx,
sladká	sladký	k2eAgFnSc1d1
kaše	kaše	k1gFnSc1
z	z	k7c2
rýže	rýže	k1gFnSc2
<g/>
,	,	kIx,
cukru	cukr	k1gInSc2
a	a	k8xC
mléka	mléko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Laddú	Laddú	k?
<g/>
:	:	kIx,
kokosky	kokoska	k1gFnPc1
<g/>
,	,	kIx,
kuličky	kulička	k1gFnPc1
z	z	k7c2
kokosu	kokos	k1gInSc2
<g/>
,	,	kIx,
cizrnové	cizrnový	k2eAgFnSc2d1
mouky	mouka	k1gFnSc2
nebo	nebo	k8xC
krupice	krupice	k1gFnSc2
<g/>
,	,	kIx,
cukru	cukr	k1gInSc2
a	a	k8xC
sušeného	sušený	k2eAgNnSc2d1
mléka	mléko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rasgula	Rasgula	k1gFnSc1
<g/>
:	:	kIx,
sladké	sladký	k2eAgFnPc1d1
kuličky	kulička	k1gFnPc1
z	z	k7c2
tvarohu	tvaroh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rasmáláí	Rasmáláit	k5eAaPmIp3nP,k5eAaBmIp3nP,k5eAaImIp3nP
<g/>
:	:	kIx,
sladké	sladký	k2eAgFnPc1d1
kuličky	kulička	k1gFnPc1
z	z	k7c2
tvarohu	tvaroh	k1gInSc2
<g/>
,	,	kIx,
smetany	smetana	k1gFnSc2
a	a	k8xC
kokosu	kokos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Zarda	Zarda	k1gFnSc1
<g/>
:	:	kIx,
sladký	sladký	k2eAgInSc1d1
rýžový	rýžový	k2eAgInSc1d1
nákyp	nákyp	k1gInSc1
<g/>
,	,	kIx,
kořeněný	kořeněný	k2eAgInSc1d1
kurkumou	kurkuma	k1gFnSc7
nebo	nebo	k8xC
šafránem	šafrán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Nápoje	nápoj	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Ám	Ám	k?
panna	panna	k1gFnSc1
<g/>
:	:	kIx,
ovocná	ovocný	k2eAgFnSc1d1
šťáva	šťáva	k1gFnSc1
z	z	k7c2
nezralého	zralý	k2eNgNnSc2d1
manga	mango	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Dáta	Dát	k2eAgFnSc1d1
masala	masala	k1gFnSc1
<g/>
:	:	kIx,
čaj	čaj	k1gInSc1
<g/>
,	,	kIx,
kořeněný	kořeněný	k2eAgInSc1d1
kardamonem	kardamon	k1gInSc7
<g/>
,	,	kIx,
hřebíčkem	hřebíček	k1gInSc7
<g/>
,	,	kIx,
vařený	vařený	k2eAgMnSc1d1
s	s	k7c7
mlékem	mléko	k1gNnSc7
a	a	k8xC
silně	silně	k6eAd1
oslazený	oslazený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Džal	Džal	k1gMnSc1
džíra	džíra	k1gMnSc1
<g/>
:	:	kIx,
limonáda	limonáda	k1gFnSc1
s	s	k7c7
kořením	koření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Falúda	Falúda	k1gFnSc1
<g/>
:	:	kIx,
hustý	hustý	k2eAgInSc4d1
<g/>
,	,	kIx,
sladký	sladký	k2eAgInSc4d1
mléčný	mléčný	k2eAgInSc4d1
koktejl	koktejl	k1gInSc4
<g/>
,	,	kIx,
oblíbený	oblíbený	k2eAgInSc4d1
u	u	k7c2
menšiny	menšina	k1gFnSc2
Pársů	Párs	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Handia	Handium	k1gNnPc4
<g/>
:	:	kIx,
slabé	slabý	k2eAgNnSc4d1
rýžové	rýžový	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
<g/>
,	,	kIx,
připomínající	připomínající	k2eAgInSc1d1
spíše	spíše	k9
ruský	ruský	k2eAgInSc1d1
kvas	kvas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Lassí	Lassí	k1gNnSc1
<g/>
:	:	kIx,
nápoj	nápoj	k1gInSc1
z	z	k7c2
kyselého	kyselý	k2eAgNnSc2d1
mléka	mléko	k1gNnSc2
<g/>
,	,	kIx,
podávaný	podávaný	k2eAgMnSc1d1
buďto	buďto	k8xC
na	na	k7c6
sladko	sladko	k6eAd1
s	s	k7c7
ovocem	ovoce	k1gNnSc7
a	a	k8xC
růžovou	růžový	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
s	s	k7c7
kořením	koření	k1gNnSc7
<g/>
,	,	kIx,
solí	sůl	k1gFnSc7
a	a	k8xC
mátou	máta	k1gFnSc7
či	či	k8xC
česnekem	česnek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Šerbet	šerbet	k1gInSc1
<g/>
:	:	kIx,
sladký	sladký	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
z	z	k7c2
ovoce	ovoce	k1gNnSc2
<g/>
,	,	kIx,
máty	máta	k1gFnSc2
nebo	nebo	k8xC
růžové	růžový	k2eAgFnPc1d1
vody	voda	k1gFnPc1
<g/>
,	,	kIx,
chlazený	chlazený	k2eAgInSc1d1
ledem	led	k1gInSc7
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
alkoholický	alkoholický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Toddy	Toddy	k6eAd1
<g/>
:	:	kIx,
palmové	palmový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
připravené	připravený	k2eAgNnSc1d1
ze	z	k7c2
zkvašené	zkvašený	k2eAgFnSc2d1
palmové	palmový	k2eAgFnSc2d1
mízy	míza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tulsí	Tulsit	k5eAaPmIp3nP
<g/>
:	:	kIx,
bylinkový	bylinkový	k2eAgInSc4d1
čaj	čaj	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
indická	indický	k2eAgFnSc1d1
bazalka	bazalka	k1gFnSc1
(	(	kIx(
<g/>
Ocimum	Ocimum	k1gInSc1
sanctum	sanctum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Miltner	Miltner	k1gMnSc1
<g/>
:	:	kIx,
Indická	indický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Merkur	Merkur	k1gInSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bindu	Bindu	k6eAd1
Singh	Singh	k1gInSc1
<g/>
:	:	kIx,
indická	indický	k2eAgFnSc1d1
hostina	hostina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filip	Filip	k1gMnSc1
Trend	trend	k1gInSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ájurvéda	Ájurvéda	k1gFnSc1
</s>
<s>
Čát	Čát	k?
masála	masála	k1gFnSc1
</s>
<s>
Čatní	čatní	k1gNnSc1
</s>
<s>
Garam	Garam	k6eAd1
masála	masála	k1gFnSc1
</s>
<s>
Karí	karí	k1gNnSc1
</s>
<s>
Naan	Naan	k1gMnSc1
</s>
<s>
Vindaloo	Vindaloo	k6eAd1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Indická	indický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Indická	indický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
na	na	k7c4
DMOZ	DMOZ	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Asijská	asijský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
Nezávislé	závislý	k2eNgFnSc2d1
státy	stát	k1gInPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Korea	Korea	k1gFnSc1
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
SAE	SAE	kA
•	•	k?
Srí	Srí	k1gMnSc1
Lanka	lanko	k1gNnSc2
•	•	k?
Sýrie	Sýrie	k1gFnSc1
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
a	a	k8xC
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Hongkong	Hongkong	k1gInSc1
•	•	k?
Macao	Macao	k1gNnSc1
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Indie	Indie	k1gFnSc1
|	|	kIx~
Gastronomie	gastronomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85031835	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85031835	#num#	k4
</s>
