<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
)	)	kIx)	)
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1844	[number]	k4	1844
Strážovice	Strážovice	k1gFnSc2	Strážovice
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Mirotice	Mirotice	k1gFnSc1	Mirotice
<g/>
,	,	kIx,	,
okr	okr	k1gInSc1	okr
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
skladatel	skladatel	k1gMnSc1	skladatel
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Varhanickou	varhanický	k2eAgFnSc4d1	varhanická
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
žákem	žák	k1gMnSc7	žák
Františka	František	k1gMnSc2	František
Blažka	Blažek	k1gMnSc2	Blažek
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Krejčího	Krejčí	k1gMnSc2	Krejčí
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
soukromě	soukromě	k6eAd1	soukromě
studoval	studovat	k5eAaImAgMnS	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
u	u	k7c2	u
Antonína	Antonín	k1gMnSc2	Antonín
Bennewitze	Bennewitze	k1gFnSc2	Bennewitze
a	a	k8xC	a
Mořice	Mořic	k1gMnSc2	Mořic
Mildnera	Mildner	k1gMnSc2	Mildner
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
houslista	houslista	k1gMnSc1	houslista
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
učitelem	učitel	k1gMnSc7	učitel
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
číslování	číslování	k1gNnSc2	číslování
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
na	na	k7c4	na
60	[number]	k4	60
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
11	[number]	k4	11
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
20	[number]	k4	20
sešitů	sešit	k1gInPc2	sešit
písní	píseň	k1gFnPc2	píseň
Symfonie	symfonie	k1gFnSc2	symfonie
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
svému	svůj	k1gMnSc3	svůj
učiteli	učitel	k1gMnSc3	učitel
Josefu	Josef	k1gMnSc3	Josef
Krejčímu	Krejčí	k1gMnSc3	Krejčí
Der	drát	k5eAaImRp2nS	drát
Gott	Gott	k1gInSc1	Gott
und	und	k?	und
die	die	k?	die
Bajadere	Bajader	k1gInSc5	Bajader
(	(	kIx(	(
<g/>
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
<g/>
)	)	kIx)	)
4	[number]	k4	4
opery	opera	k1gFnSc2	opera
Komorní	komorní	k2eAgFnSc2d1	komorní
skladby	skladba	k1gFnSc2	skladba
Sbory	sbor	k1gInPc1	sbor
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Lied	Lieda	k1gFnPc2	Lieda
der	drát	k5eAaImRp2nS	drát
Deutschen	Deutschen	k1gInSc1	Deutschen
in	in	k?	in
Oesteriech	Oesteriech	k1gInSc1	Oesteriech
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
melodické	melodický	k2eAgInPc1d1	melodický
názvuky	názvuk	k1gInPc1	názvuk
Písně	píseň	k1gFnSc2	píseň
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
Všechny	všechen	k3xTgFnPc1	všechen
skladby	skladba	k1gFnPc1	skladba
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
rukopise	rukopis	k1gInSc6	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
hudební	hudební	k2eAgInSc1d1	hudební
slovník	slovník	k1gInSc1	slovník
I	i	k9	i
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
–	–	k?	–
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
SHV	SHV	kA	SHV
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Jan	Jan	k1gMnSc1	Jan
Löwenbach	Löwenbach	k1gMnSc1	Löwenbach
<g/>
:	:	kIx,	:
Jindřich	Jindřich	k1gMnSc1	Jindřich
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
zapomenutý	zapomenutý	k2eAgMnSc1d1	zapomenutý
romantik	romantik	k1gMnSc1	romantik
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
matice	matice	k1gFnSc1	matice
Umělecké	umělecký	k2eAgFnSc2d1	umělecká
besedy	beseda	k1gFnSc2	beseda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaImAgInS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jindřich	Jindřich	k1gMnSc1	Jindřich
Kafka	Kafka	k1gMnSc1	Kafka
Významná	významný	k2eAgFnSc1d1	významná
výročí	výročí	k1gNnSc4	výročí
regionálních	regionální	k2eAgFnPc2d1	regionální
osobností	osobnost	k1gFnPc2	osobnost
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
</s>
