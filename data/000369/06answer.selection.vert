<s>
Vodík	vodík	k1gInSc1	vodík
objevil	objevit	k5eAaPmAgInS	objevit
roku	rok	k1gInSc2	rok
1766	[number]	k4	1766
Angličan	Angličan	k1gMnSc1	Angličan
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
neušlechtilých	ušlechtilý	k2eNgInPc2d1	neušlechtilý
kovů	kov	k1gInPc2	kov
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
vzniká	vznikat	k5eAaImIp3nS	vznikat
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
,	,	kIx,	,
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
