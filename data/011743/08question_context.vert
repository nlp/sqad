<s>
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
šipka	šipka	k1gFnSc1	šipka
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Hadonoše	hadonoš	k1gMnSc2	hadonoš
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
hvězd	hvězda	k1gFnPc2	hvězda
nejrychlejší	rychlý	k2eAgInSc1d3	nejrychlejší
vlastní	vlastní	k2eAgInSc1d1	vlastní
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
<g/>
:	:	kIx,	:
činí	činit	k5eAaImIp3nS	činit
10,34	[number]	k4	10,34
<g/>
"	"	kIx"	"
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
necelých	celý	k2eNgInPc2d1	necelý
šest	šest	k4xCc4	šest
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
hmotným	hmotný	k2eAgMnSc7d1	hmotný
červeným	červený	k2eAgMnSc7d1	červený
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
M4	M4	k1gFnPc2	M4
a	a	k8xC	a
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
hvězdách	hvězda	k1gFnPc6	hvězda
soustavy	soustava	k1gFnSc2	soustava
Alfa	alfa	k1gNnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>

