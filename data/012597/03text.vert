<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Kapoun	kapoun	k1gMnSc1	kapoun
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgMnSc1d1	československý
basketbalista	basketbalista	k1gMnSc1	basketbalista
<g/>
,	,	kIx,	,
vicemistr	vicemistr	k1gMnSc1	vicemistr
Československa	Československo	k1gNnSc2	Československo
1961	[number]	k4	1961
a	a	k8xC	a
trenér	trenér	k1gMnSc1	trenér
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
basketbalové	basketbalový	k2eAgFnSc6d1	basketbalová
lize	liga	k1gFnSc6	liga
odehrál	odehrát	k5eAaPmAgInS	odehrát
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
sezón	sezóna	k1gFnPc2	sezóna
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
kluby	klub	k1gInPc4	klub
Spartak	Spartak	k1gInSc4	Spartak
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
/	/	kIx~	/
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
11	[number]	k4	11
sezón	sezóna	k1gFnPc2	sezóna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dukla	Dukla	k1gFnSc1	Dukla
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
(	(	kIx(	(
<g/>
1	[number]	k4	1
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dukla	Dukla	k1gFnSc1	Dukla
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
1	[number]	k4	1
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Spartou	Sparta	k1gFnSc7	Sparta
Praha	Praha	k1gFnSc1	Praha
získal	získat	k5eAaPmAgMnS	získat
6	[number]	k4	6
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
stříbrnou	stříbrná	k1gFnSc4	stříbrná
za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
a	a	k8xC	a
pět	pět	k4xCc1	pět
bronzových	bronzový	k2eAgFnPc2d1	bronzová
za	za	k7c4	za
třetí	třetí	k4xOgNnPc4	třetí
místa	místo	k1gNnPc4	místo
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
až	až	k9	až
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
basketbalové	basketbalový	k2eAgFnSc6d1	basketbalová
lize	liga	k1gFnSc6	liga
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
(	(	kIx(	(
<g/>
zavedení	zavedení	k1gNnSc4	zavedení
podrobných	podrobný	k2eAgFnPc2d1	podrobná
statistik	statistika	k1gFnPc2	statistika
zápasů	zápas	k1gInPc2	zápas
<g/>
)	)	kIx)	)
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
1872	[number]	k4	1872
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
týmem	tým	k1gInSc7	tým
Spartak	Spartak	k1gInSc1	Spartak
Sokolovo	Sokolovo	k1gNnSc4	Sokolovo
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Poháru	pohár	k1gInSc2	pohár
evropských	evropský	k2eAgMnPc2d1	evropský
mistrů	mistr	k1gMnPc2	mistr
1960	[number]	k4	1960
<g/>
/	/	kIx~	/
<g/>
61	[number]	k4	61
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
(	(	kIx(	(
<g/>
bilance	bilance	k1gFnSc1	bilance
5	[number]	k4	5
vítězství	vítězství	k1gNnSc4	vítězství
ze	z	k7c2	z
6	[number]	k4	6
zápasů	zápas	k1gInPc2	zápas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
reprezentační	reprezentační	k2eAgNnSc4d1	reprezentační
družstvo	družstvo	k1gNnSc4	družstvo
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1965-1967	[number]	k4	1965-1967
hrál	hrát	k5eAaImAgInS	hrát
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
hráčské	hráčský	k2eAgFnSc2d1	hráčská
kariéry	kariéra	k1gFnSc2	kariéra
byl	být	k5eAaImAgInS	být
funkcionářem	funkcionář	k1gMnSc7	funkcionář
a	a	k8xC	a
trenérem	trenér	k1gMnSc7	trenér
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
SK	Sk	kA	Sk
Aritma	Aritma	k1gFnSc1	Aritma
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
kluby	klub	k1gInPc7	klub
===	===	k?	===
</s>
</p>
<p>
<s>
1960-1961	[number]	k4	1960-1961
Spartak	Spartak	k1gInSc1	Spartak
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1961-1963	[number]	k4	1961-1963
Dukla	Dukla	k1gFnSc1	Dukla
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dukla	Dukla	k1gFnSc1	Dukla
Olomouc	Olomouc	k1gFnSc1	Olomouc
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1963-1973	[number]	k4	1963-1973
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
x	x	k?	x
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
x	x	k?	x
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
sezón	sezóna	k1gFnPc2	sezóna
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1872	[number]	k4	1872
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
6	[number]	k4	6
medailových	medailový	k2eAgNnPc2d1	medailové
umístění	umístění	k1gNnPc2	umístění
</s>
</p>
<p>
<s>
vicemistr	vicemistr	k1gMnSc1	vicemistr
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
x	x	k?	x
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
x	x	k?	x
4	[number]	k4	4
<g/>
.	.	kIx.	.
místoEvropské	místoEvropský	k2eAgInPc1d1	místoEvropský
poháry	pohár	k1gInPc1	pohár
klubů	klub	k1gInPc2	klub
-	-	kIx~	-
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Spartak	Spartak	k1gInSc1	Spartak
Sokolovo	Sokolovo	k1gNnSc4	Sokolovo
</s>
</p>
<p>
<s>
Pohár	pohár	k1gInSc1	pohár
evropských	evropský	k2eAgMnPc2d1	evropský
mistrů	mistr	k1gMnPc2	mistr
1960	[number]	k4	1960
<g/>
/	/	kIx~	/
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
postup	postup	k1gInSc1	postup
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
vyřazeni	vyřazen	k2eAgMnPc1d1	vyřazen
CCA	cca	kA	cca
Bukrešť	Bukrešť	k1gMnSc1	Bukrešť
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
47	[number]	k4	47
<g/>
:	:	kIx,	:
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Československo	Československo	k1gNnSc1	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
reprezentační	reprezentační	k2eAgNnSc4d1	reprezentační
družstvo	družstvo	k1gNnSc4	družstvo
mužů	muž	k1gMnPc2	muž
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1965-1967	[number]	k4	1965-1967
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
federace	federace	k1gFnSc1	federace
•	•	k?	•
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
•	•	k?	•
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
•	•	k?	•
statistiky	statistika	k1gFnSc2	statistika
</s>
</p>
<p>
<s>
BC	BC	kA	BC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
•	•	k?	•
Dukla	Dukla	k1gFnSc1	Dukla
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
•	•	k?	•
Dukla	Dukla	k1gFnSc1	Dukla
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
basketbalu	basketbal	k1gInSc2	basketbal
mužů	muž	k1gMnPc2	muž
web	web	k1gInSc1	web
members	membersa	k1gFnPc2	membersa
<g/>
.	.	kIx.	.
<g/>
chello	chello	k1gNnSc1	chello
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
tabulka	tabulka	k1gFnSc1	tabulka
střelců	střelec	k1gMnPc2	střelec
Sparty	Sparta	k1gFnSc2	Sparta
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
web	web	k1gInSc1	web
members	members	k1gInSc1	members
<g/>
.	.	kIx.	.
<g/>
chello	chello	k1gNnSc1	chello
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
BC	BC	kA	BC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
web	web	k1gInSc1	web
bcsparta	bcsparta	k1gFnSc1	bcsparta
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Aritma	Aritma	k1gFnSc1	Aritma
Praha	Praha	k1gFnSc1	Praha
web	web	k1gInSc1	web
cbf	cbf	k?	cbf
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Aritma	Aritma	k1gFnSc1	Aritma
Basketbal	basketbal	k1gInSc1	basketbal
web	web	k1gInSc1	web
skaritmabasketbal	skaritmabasketbal	k1gInSc4	skaritmabasketbal
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Champions	Champions	k1gInSc1	Champions
Cup	cup	k1gInSc1	cup
1960-61	[number]	k4	1960-61
web	web	k1gInSc1	web
linguasport	linguasport	k1gInSc4	linguasport
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
