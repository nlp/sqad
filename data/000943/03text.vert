<s>
Alfred	Alfred	k1gMnSc1	Alfred
Bernhard	Bernhard	k1gMnSc1	Bernhard
Nobel	Nobel	k1gMnSc1	Nobel
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1833	[number]	k4	1833
Stockholm	Stockholm	k1gInSc1	Stockholm
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1896	[number]	k4	1896
Sanremo	Sanrema	k1gFnSc5	Sanrema
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
dynamitu	dynamit	k1gInSc2	dynamit
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gFnSc2	jeho
závěti	závěť	k1gFnSc2	závěť
jsou	být	k5eAaImIp3nP	být
každoročně	každoročně	k6eAd1	každoročně
udělovány	udělovat	k5eAaImNgFnP	udělovat
Nobelovy	Nobelův	k2eAgFnPc1d1	Nobelova
ceny	cena	k1gFnPc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
nobelium	nobelium	k1gNnSc1	nobelium
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
bohaté	bohatý	k2eAgFnSc6d1	bohatá
stockholmské	stockholmský	k2eAgFnSc6d1	Stockholmská
podnikatelské	podnikatelský	k2eAgFnSc6d1	podnikatelská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
třetím	třetí	k4xOgMnSc7	třetí
synem	syn	k1gMnSc7	syn
inženýra	inženýr	k1gMnSc2	inženýr
Immanuela	Immanuel	k1gMnSc2	Immanuel
Nobela	Nobel	k1gMnSc2	Nobel
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
a	a	k8xC	a
Andriette	Andriett	k1gInSc5	Andriett
Ahlsell	Ahlsell	k1gMnSc1	Ahlsell
Nobelové	Nobelová	k1gFnSc2	Nobelová
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
podnikal	podnikat	k5eAaImAgMnS	podnikat
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc4d1	dobré
soukromé	soukromý	k2eAgNnSc4d1	soukromé
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
v	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
mluvil	mluvit	k5eAaImAgMnS	mluvit
nejen	nejen	k6eAd1	nejen
švédsky	švédsky	k6eAd1	švédsky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
veliké	veliký	k2eAgFnPc4d1	veliká
znalosti	znalost	k1gFnPc4	znalost
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
literatuře	literatura	k1gFnSc3	literatura
a	a	k8xC	a
přírodním	přírodní	k2eAgFnPc3d1	přírodní
vědám	věda	k1gFnPc3	věda
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
vzdělání	vzdělání	k1gNnSc4	vzdělání
dokončil	dokončit	k5eAaPmAgMnS	dokončit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
známého	známý	k2eAgMnSc4d1	známý
chemika	chemik	k1gMnSc4	chemik
Théophila-Jules	Théophila-Jules	k1gMnSc1	Théophila-Jules
Pelouzea	Pelouzeus	k1gMnSc4	Pelouzeus
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
také	také	k9	také
potkal	potkat	k5eAaPmAgInS	potkat
italského	italský	k2eAgMnSc2d1	italský
chemika	chemik	k1gMnSc2	chemik
Ascania	Ascanium	k1gNnSc2	Ascanium
Sobrera	Sobrer	k1gMnSc2	Sobrer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
vysoce	vysoce	k6eAd1	vysoce
výbušnou	výbušný	k2eAgFnSc4d1	výbušná
kapalinu	kapalina	k1gFnSc4	kapalina
-	-	kIx~	-
nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaPmF	věnovat
studiu	studio	k1gNnSc3	studio
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
problematice	problematika	k1gFnSc3	problematika
bezpečné	bezpečný	k2eAgFnSc2d1	bezpečná
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
manipulace	manipulace	k1gFnSc2	manipulace
s	s	k7c7	s
nitroglycerinem	nitroglycerin	k1gInSc7	nitroglycerin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
továrně	továrna	k1gFnSc6	továrna
v	v	k7c6	v
Helenborgu	Helenborg	k1gInSc6	Helenborg
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc2	několik
explozím	exploze	k1gFnPc3	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
zahynul	zahynout	k5eAaPmAgMnS	zahynout
Nobelův	Nobelův	k2eAgMnSc1d1	Nobelův
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Emil	Emil	k1gMnSc1	Emil
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgMnPc2d1	další
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
Nobel	Nobel	k1gMnSc1	Nobel
zapřisáhl	zapřisáhnout	k5eAaPmAgMnS	zapřisáhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
učiní	učinit	k5eAaImIp3nS	učinit
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
nitroglycerinem	nitroglycerin	k1gInSc7	nitroglycerin
bezpečnější	bezpečný	k2eAgMnSc1d2	bezpečnější
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
byla	být	k5eAaImAgFnS	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
a	a	k8xC	a
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
byly	být	k5eAaImAgInP	být
veškeré	veškerý	k3xTgInPc1	veškerý
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
látkou	látka	k1gFnSc7	látka
zakázány	zakázán	k2eAgFnPc1d1	zakázána
<g/>
.	.	kIx.	.
</s>
<s>
Nobel	Nobel	k1gMnSc1	Nobel
proto	proto	k8xC	proto
přesunul	přesunout	k5eAaPmAgMnS	přesunout
svou	svůj	k3xOyFgFnSc4	svůj
laboratoř	laboratoř	k1gFnSc4	laboratoř
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
kotvící	kotvící	k2eAgFnSc1d1	kotvící
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
Mälaren	Mälarna	k1gFnPc2	Mälarna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
si	se	k3xPyFc3	se
nechává	nechávat	k5eAaImIp3nS	nechávat
patentovat	patentovat	k5eAaBmF	patentovat
dynamit	dynamit	k1gInSc4	dynamit
<g/>
.	.	kIx.	.
</s>
<s>
Smíšením	smíšení	k1gNnSc7	smíšení
kapalného	kapalný	k2eAgInSc2d1	kapalný
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
s	s	k7c7	s
hlinkou	hlinka	k1gFnSc7	hlinka
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tvarovatelnou	tvarovatelný	k2eAgFnSc4d1	tvarovatelná
hmotu	hmota	k1gFnSc4	hmota
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
lze	lze	k6eAd1	lze
bezpečně	bezpečně	k6eAd1	bezpečně
manipulovat	manipulovat	k5eAaImF	manipulovat
<g/>
.	.	kIx.	.
</s>
<s>
Sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
také	také	k9	také
rozbušku	rozbuška	k1gFnSc4	rozbuška
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přivede	přivést	k5eAaPmIp3nS	přivést
kusy	kus	k1gInPc4	kus
dynamitu	dynamit	k1gInSc2	dynamit
k	k	k7c3	k
explozi	exploze	k1gFnSc3	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Rozbuška	rozbuška	k1gFnSc1	rozbuška
byla	být	k5eAaImAgFnS	být
odpalována	odpalovat	k5eAaImNgFnS	odpalovat
pomocí	pomocí	k7c2	pomocí
zápalné	zápalný	k2eAgFnSc2d1	zápalná
šňůry	šňůra	k1gFnSc2	šňůra
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
a	a	k8xC	a
dynamit	dynamit	k1gInSc1	dynamit
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
žádaným	žádaný	k2eAgNnSc7d1	žádané
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
času	čas	k1gInSc2	čas
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
továrny	továrna	k1gFnSc2	továrna
na	na	k7c6	na
90	[number]	k4	90
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nobel	Nobel	k1gMnSc1	Nobel
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
rozvoji	rozvoj	k1gInSc3	rozvoj
podnikání	podnikání	k1gNnSc2	podnikání
i	i	k8xC	i
rozvoji	rozvoj	k1gInSc6	rozvoj
technologie	technologie	k1gFnSc2	technologie
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
355	[number]	k4	355
patentů	patent	k1gInPc2	patent
a	a	k8xC	a
nashromáždil	nashromáždit	k5eAaPmAgMnS	nashromáždit
obrovský	obrovský	k2eAgInSc4d1	obrovský
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
vynálezů	vynález	k1gInPc2	vynález
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
například	například	k6eAd1	například
trhavou	trhavý	k2eAgFnSc4d1	trhavá
želatinu	želatina	k1gFnSc4	želatina
(	(	kIx(	(
<g/>
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
a	a	k8xC	a
střelné	střelný	k2eAgFnSc2d1	střelná
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
patentována	patentován	k2eAgFnSc1d1	patentována
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
modifikovanou	modifikovaný	k2eAgFnSc4d1	modifikovaná
dalšími	další	k2eAgFnPc7d1	další
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
dodnes	dodnes	k6eAd1	dodnes
bezdýmý	bezdýmý	k2eAgInSc1d1	bezdýmý
střelný	střelný	k2eAgInSc1d1	střelný
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
opět	opět	k6eAd1	opět
na	na	k7c6	na
kombinaci	kombinace	k1gFnSc6	kombinace
nitroglycerinu	nitroglycerin	k1gInSc2	nitroglycerin
<g/>
,	,	kIx,	,
střelné	střelný	k2eAgFnSc2d1	střelná
bavlny	bavlna	k1gFnSc2	bavlna
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
balistit	balistit	k5eAaPmF	balistit
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgNnSc4d2	pozdější
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgNnPc4d1	používané
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
kordit	kordit	k5eAaPmF	kordit
<g/>
.	.	kIx.	.
</s>
<s>
Nobelův	Nobelův	k2eAgInSc4d1	Nobelův
citát	citát	k1gInSc4	citát
pojící	pojící	k2eAgInSc4d1	pojící
se	se	k3xPyFc4	se
k	k	k7c3	k
dynamitu	dynamit	k1gInSc3	dynamit
"	"	kIx"	"
<g/>
Moje	můj	k3xOp1gFnPc1	můj
továrny	továrna	k1gFnPc1	továrna
na	na	k7c4	na
dynamit	dynamit	k1gInSc4	dynamit
zřejmě	zřejmě	k6eAd1	zřejmě
ukončí	ukončit	k5eAaPmIp3nS	ukončit
války	válka	k1gFnSc2	válka
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
všechny	všechen	k3xTgInPc1	všechen
ty	ten	k3xDgInPc1	ten
vaše	váš	k3xOp2gInPc1	váš
kongresy	kongres	k1gInPc1	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dva	dva	k4xCgInPc1	dva
armádní	armádní	k2eAgInPc1d1	armádní
sbory	sbor	k1gInPc1	sbor
budou	být	k5eAaImBp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
zničit	zničit	k5eAaPmF	zničit
jeden	jeden	k4xCgMnSc1	jeden
druhého	druhý	k4xOgMnSc4	druhý
během	během	k7c2	během
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
civilizované	civilizovaný	k2eAgInPc1d1	civilizovaný
národy	národ	k1gInPc1	národ
s	s	k7c7	s
hrůzou	hrůza	k1gFnSc7	hrůza
odvrátí	odvrátit	k5eAaPmIp3nP	odvrátit
od	od	k7c2	od
války	válka	k1gFnSc2	válka
a	a	k8xC	a
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
své	svůj	k3xOyFgFnSc2	svůj
armády	armáda	k1gFnSc2	armáda
<g/>
"	"	kIx"	"
Poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
známou	známý	k2eAgFnSc7d1	známá
skutečností	skutečnost	k1gFnSc7	skutečnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
napsal	napsat	k5eAaBmAgMnS	napsat
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
Nemesis	Nemesis	k1gFnSc2	Nemesis
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
rouhačskou	rouhačský	k2eAgFnSc4d1	rouhačská
a	a	k8xC	a
málem	málem	k6eAd1	málem
se	se	k3xPyFc4	se
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Nobel	Nobel	k1gMnSc1	Nobel
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
majetek	majetek	k1gInSc1	majetek
bude	být	k5eAaImBp3nS	být
vložen	vložit	k5eAaPmNgInS	vložit
do	do	k7c2	do
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
bude	být	k5eAaImBp3nS	být
každoročně	každoročně	k6eAd1	každoročně
udělována	udělován	k2eAgFnSc1d1	udělována
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
významné	významný	k2eAgInPc4d1	významný
vědecké	vědecký	k2eAgInPc4d1	vědecký
objevy	objev	k1gInPc4	objev
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc4d1	literární
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
mír	mír	k1gInSc4	mír
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
cena	cena	k1gFnSc1	cena
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
finanční	finanční	k2eAgFnSc1d1	finanční
odměna	odměna	k1gFnSc1	odměna
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyplácena	vyplácet	k5eAaImNgFnS	vyplácet
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
peněz	peníze	k1gInPc2	peníze
ze	z	k7c2	z
závěti	závěť	k1gFnSc2	závěť
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
švédská	švédský	k2eAgFnSc1d1	švédská
Akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
Alfréd	Alfréd	k1gMnSc1	Alfréd
Nobel	Nobel	k1gMnSc1	Nobel
ji	on	k3xPp3gFnSc4	on
obdařil	obdařit	k5eAaPmAgMnS	obdařit
částkou	částka	k1gFnSc7	částka
32	[number]	k4	32
miliónů	milión	k4xCgInPc2	milión
švédských	švédský	k2eAgFnPc2d1	švédská
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
jsou	být	k5eAaImIp3nP	být
vypláceny	vyplácet	k5eAaImNgFnP	vyplácet
z	z	k7c2	z
úroků	úrok	k1gInPc2	úrok
(	(	kIx(	(
<g/>
asi	asi	k9	asi
160	[number]	k4	160
000	[number]	k4	000
švédských	švédský	k2eAgFnPc2d1	švédská
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
udělena	udělit	k5eAaPmNgFnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
přidáno	přidat	k5eAaPmNgNnS	přidat
i	i	k9	i
ocenění	ocenění	k1gNnSc3	ocenění
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
obecně	obecně	k6eAd1	obecně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
jakého	jaký	k3yIgMnSc4	jaký
může	moct	k5eAaImIp3nS	moct
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
nebo	nebo	k8xC	nebo
státník	státník	k1gMnSc1	státník
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Alfred	Alfred	k1gMnSc1	Alfred
Nobel	Nobel	k1gMnSc1	Nobel
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alfred	Alfred	k1gMnSc1	Alfred
Nobel	Nobel	k1gMnSc1	Nobel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Alfred	Alfred	k1gMnSc1	Alfred
Nobel	Nobel	k1gMnSc1	Nobel
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Životopis	životopis	k1gInSc4	životopis
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
na	na	k7c6	na
Nobelprize	Nobelpriza	k1gFnSc6	Nobelpriza
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Životopis	životopis	k1gInSc1	životopis
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
na	na	k7c4	na
Converter	Converter	k1gInSc4	Converter
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Museum	museum	k1gNnSc1	museum
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
v	v	k7c6	v
Karlskoze	Karlskoz	k1gInSc5	Karlskoz
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
