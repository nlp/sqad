<s>
Zemský	zemský	k2eAgInSc1d1	zemský
hřebčinec	hřebčinec	k1gInSc1	hřebčinec
Písek	Písek	k1gInSc1	Písek
státní	státní	k2eAgInSc1d1	státní
podnik	podnik	k1gInSc1	podnik
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc1d1	státní
podnik	podnik	k1gInSc1	podnik
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
chovem	chov	k1gInSc7	chov
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Hřebčinec	hřebčinec	k1gInSc1	hřebčinec
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
status	status	k1gInSc4	status
Národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vystavěn	vystavět	k5eAaPmNgInS	vystavět
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
plemenní	plemenný	k2eAgMnPc1d1	plemenný
hřebci	hřebec	k1gMnPc1	hřebec
sem	sem	k6eAd1	sem
byli	být	k5eAaImAgMnP	být
umístěni	umístěn	k2eAgMnPc1d1	umístěn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
historie	historie	k1gFnSc1	historie
chovu	chov	k1gInSc2	chov
koní	kůň	k1gMnPc2	kůň
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Písecký	písecký	k2eAgInSc1d1	písecký
hřebčinec	hřebčinec	k1gInSc1	hřebčinec
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
posledních	poslední	k2eAgInPc2d1	poslední
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
chov	chov	k1gInSc4	chov
koní	koní	k2eAgInSc4d1	koní
(	(	kIx(	(
<g/>
těmi	ten	k3xDgMnPc7	ten
dalšími	další	k2eAgMnPc7d1	další
jsou	být	k5eAaImIp3nP	být
Národní	národní	k2eAgInSc1d1	národní
hřebčín	hřebčín	k1gInSc1	hřebčín
Kladruby	Kladruby	k1gInPc4	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Zemský	zemský	k2eAgInSc1d1	zemský
hřebčinec	hřebčinec	k1gInSc1	hřebčinec
Tlumačov	Tlumačov	k1gInSc1	Tlumačov
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
býval	bývat	k5eAaImAgInS	bývat
posádkovým	posádkový	k2eAgNnSc7d1	posádkové
městem	město	k1gNnSc7	město
a	a	k8xC	a
přítomnost	přítomnost	k1gFnSc1	přítomnost
vojska	vojsko	k1gNnSc2	vojsko
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
také	také	k9	také
přítomnost	přítomnost	k1gFnSc1	přítomnost
hřebčince	hřebčinec	k1gInSc2	hřebčinec
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
armádním	armádní	k2eAgInSc7d1	armádní
stanovištěm	stanoviště	k1gNnSc7	stanoviště
hřebců	hřebec	k1gMnPc2	hřebec
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1811	[number]	k4	1811
Švantlův	Švantlův	k2eAgInSc1d1	Švantlův
dvůr	dvůr	k1gInSc1	dvůr
u	u	k7c2	u
bývalého	bývalý	k2eAgInSc2d1	bývalý
Měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
pivovaru	pivovar	k1gInSc2	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poloha	poloha	k1gFnSc1	poloha
blízko	blízko	k7c2	blízko
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
však	však	k9	však
neumožňovala	umožňovat	k5eNaImAgFnS	umožňovat
jej	on	k3xPp3gInSc4	on
rozšířit	rozšířit	k5eAaPmF	rozšířit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
vybudován	vybudován	k2eAgInSc4d1	vybudován
nový	nový	k2eAgInSc4d1	nový
hřebčinec	hřebčinec	k1gInSc4	hřebčinec
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
směrem	směr	k1gInSc7	směr
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Hřebčinec	hřebčinec	k1gInSc1	hřebčinec
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1901	[number]	k4	1901
-	-	kIx~	-
1902	[number]	k4	1902
vystavěn	vystavět	k5eAaPmNgInS	vystavět
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Františka	František	k1gMnSc2	František
Skopalíka	Skopalík	k1gMnSc2	Skopalík
<g/>
.	.	kIx.	.
</s>
<s>
Chod	chod	k1gInSc4	chod
hřebčince	hřebčinec	k1gInSc2	hřebčinec
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
armáda	armáda	k1gFnSc1	armáda
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přešel	přejít	k5eAaPmAgInS	přejít
plně	plně	k6eAd1	plně
do	do	k7c2	do
civilní	civilní	k2eAgFnSc2d1	civilní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
zabezpečovat	zabezpečovat	k5eAaImF	zabezpečovat
plemenné	plemenný	k2eAgMnPc4d1	plemenný
hřebce	hřebec	k1gMnPc4	hřebec
zprvu	zprvu	k6eAd1	zprvu
pro	pro	k7c4	pro
armádní	armádní	k2eAgInPc4d1	armádní
či	či	k8xC	či
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
až	až	k9	až
ke	k	k7c3	k
koním	kůň	k1gMnPc3	kůň
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgNnSc4d1	sportovní
využití	využití	k1gNnSc4	využití
se	s	k7c7	s
skokovým	skokový	k2eAgInSc7d1	skokový
a	a	k8xC	a
drezurním	drezurní	k2eAgInSc7d1	drezurní
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Hřebci	hřebec	k1gMnPc1	hřebec
byli	být	k5eAaImAgMnP	být
zajišťováni	zajišťovat	k5eAaImNgMnP	zajišťovat
dovozem	dovoz	k1gInSc7	dovoz
jak	jak	k6eAd1	jak
mohutných	mohutný	k2eAgNnPc2d1	mohutné
teplokrevných	teplokrevný	k2eAgNnPc2d1	teplokrevné
plemen	plemeno	k1gNnPc2	plemeno
oldenburských	oldenburský	k2eAgInPc2d1	oldenburský
<g/>
,	,	kIx,	,
hannoverských	hannoverský	k2eAgInPc2d1	hannoverský
či	či	k8xC	či
anglonormanských	anglonormanský	k2eAgInPc2d1	anglonormanský
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
chladnokrevných	chladnokrevný	k2eAgMnPc2d1	chladnokrevný
hřebců	hřebec	k1gMnPc2	hřebec
belgického	belgický	k2eAgInSc2d1	belgický
a	a	k8xC	a
norického	norický	k2eAgInSc2d1	norický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Hřebčinec	hřebčinec	k1gInSc1	hřebčinec
samozřejmě	samozřejmě	k6eAd1	samozřejmě
využíval	využívat	k5eAaPmAgInS	využívat
i	i	k9	i
hřebečky	hřebeček	k1gMnPc4	hřebeček
z	z	k7c2	z
domácího	domácí	k2eAgInSc2d1	domácí
chovu	chov	k1gInSc2	chov
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
sloužila	sloužit	k5eAaImAgFnS	sloužit
například	například	k6eAd1	například
hříbárna	hříbárna	k1gFnSc1	hříbárna
v	v	k7c6	v
nedalekém	daleký	k2eNgInSc6d1	nedaleký
Novém	nový	k2eAgInSc6d1	nový
Dvoře	Dvůr	k1gInSc6	Dvůr
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
přibyl	přibýt	k5eAaPmAgInS	přibýt
objekt	objekt	k1gInSc4	objekt
v	v	k7c6	v
Humňanech	Humňan	k1gMnPc6	Humňan
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
sloužící	sloužící	k2eAgMnSc1d1	sloužící
pro	pro	k7c4	pro
testaci	testace	k1gFnSc4	testace
a	a	k8xC	a
odchov	odchov	k1gInSc4	odchov
hříbat	hříbě	k1gNnPc2	hříbě
<g/>
.	.	kIx.	.
</s>
<s>
Hřebčinec	hřebčinec	k1gInSc1	hřebčinec
zůstával	zůstávat	k5eAaImAgInS	zůstávat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
samostatným	samostatný	k2eAgInSc7d1	samostatný
státním	státní	k2eAgInSc7d1	státní
podnikem	podnik	k1gInSc7	podnik
<g/>
,	,	kIx,	,
než	než	k8xS	než
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
následným	následný	k2eAgFnPc3d1	následná
změnám	změna	k1gFnPc3	změna
jeho	jeho	k3xOp3gNnSc4	jeho
začlenění	začlenění	k1gNnSc4	začlenění
<g/>
:	:	kIx,	:
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgInS	být
začleněn	začlenit	k5eAaPmNgInS	začlenit
pod	pod	k7c4	pod
Státní	státní	k2eAgFnSc4d1	státní
plemenářskou	plemenářský	k2eAgFnSc4d1	plemenářská
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byl	být	k5eAaImAgInS	být
začleněn	začlenit	k5eAaPmNgInS	začlenit
do	do	k7c2	do
Plemenářského	plemenářský	k2eAgInSc2d1	plemenářský
podniku	podnik	k1gInSc2	podnik
Veselí	veselit	k5eAaImIp3nS	veselit
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgInS	být
začleněn	začlenit	k5eAaPmNgInS	začlenit
do	do	k7c2	do
Plemenářského	plemenářský	k2eAgInSc2d1	plemenářský
podniku	podnik	k1gInSc2	podnik
Kladruby	Kladruby	k1gInPc1	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
začleněn	začlenit	k5eAaPmNgInS	začlenit
do	do	k7c2	do
Plemenářského	plemenářský	k2eAgInSc2d1	plemenářský
podniku	podnik	k1gInSc2	podnik
Netolice	Netolice	k1gFnPc4	Netolice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
samostatným	samostatný	k2eAgInSc7d1	samostatný
odštěpným	odštěpný	k2eAgInSc7d1	odštěpný
závodem	závod	k1gInSc7	závod
Státního	státní	k2eAgInSc2d1	státní
plemenářského	plemenářský	k2eAgInSc2d1	plemenářský
podniku	podnik	k1gInSc2	podnik
Praha	Praha	k1gFnSc1	Praha
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
opět	opět	k6eAd1	opět
samostatným	samostatný	k2eAgInSc7d1	samostatný
státním	státní	k2eAgInSc7d1	státní
podnikem	podnik	k1gInSc7	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
píseckého	písecký	k2eAgInSc2d1	písecký
chovu	chov	k1gInSc2	chov
pocházel	pocházet	k5eAaImAgInS	pocházet
i	i	k9	i
kůň	kůň	k1gMnSc1	kůň
Ardo	Ardo	k1gMnSc1	Ardo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stál	stát	k5eAaImAgMnS	stát
modelem	model	k1gInSc7	model
sochaři	sochař	k1gMnPc1	sochař
J.	J.	kA	J.
V.	V.	kA	V.
Myslbekovi	Myslbekův	k2eAgMnPc1d1	Myslbekův
<g/>
,	,	kIx,	,
když	když	k8xS	když
modeloval	modelovat	k5eAaImAgInS	modelovat
Pomník	pomník	k1gInSc4	pomník
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
zdobící	zdobící	k2eAgFnSc4d1	zdobící
pražské	pražský	k2eAgNnSc4d1	Pražské
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
Myslbekových	Myslbekův	k2eAgFnPc2d1	Myslbekova
studií	studie	k1gFnPc2	studie
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
hřebčince	hřebčinec	k1gInSc2	hřebčinec
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
současného	současný	k2eAgInSc2d1	současný
hřebčince	hřebčinec	k1gInSc2	hřebčinec
vznikal	vznikat	k5eAaImAgInS	vznikat
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
zdravé	zdravý	k2eAgNnSc4d1	zdravé
ustájení	ustájení	k1gNnSc4	ustájení
plemenných	plemenný	k2eAgMnPc2d1	plemenný
hřebců	hřebec	k1gMnPc2	hřebec
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
především	především	k9	především
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
<g/>
,	,	kIx,	,
prostorné	prostorný	k2eAgFnSc2d1	prostorná
a	a	k8xC	a
světlé	světlý	k2eAgFnSc2d1	světlá
stáje	stáj	k1gFnSc2	stáj
<g/>
,	,	kIx,	,
krytá	krytý	k2eAgFnSc1d1	krytá
i	i	k8xC	i
otevřená	otevřený	k2eAgFnSc1d1	otevřená
jízdárna	jízdárna	k1gFnSc1	jízdárna
<g/>
,	,	kIx,	,
inseminační	inseminační	k2eAgNnSc1d1	inseminační
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
izolační	izolační	k2eAgFnSc1d1	izolační
stáj	stáj	k1gFnSc1	stáj
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
areálu	areál	k1gInSc2	areál
je	být	k5eAaImIp3nS	být
honosná	honosný	k2eAgFnSc1d1	honosná
správní	správní	k2eAgFnSc1d1	správní
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
byty	byt	k1gInPc7	byt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
,	,	kIx,	,
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
celý	celý	k2eAgInSc4d1	celý
komplex	komplex	k1gInSc4	komplex
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
z	z	k7c2	z
režného	režný	k2eAgNnSc2d1	režné
zdiva	zdivo	k1gNnSc2	zdivo
se	s	k7c7	s
secesními	secesní	k2eAgInPc7d1	secesní
detaily	detail	k1gInPc7	detail
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zemský	zemský	k2eAgInSc4d1	zemský
hřebčinec	hřebčinec	k1gInSc4	hřebčinec
Písek	Písek	k1gInSc1	Písek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Národní	národní	k2eAgInSc1d1	národní
hřebčín	hřebčín	k1gInSc1	hřebčín
Kladruby	Kladruby	k1gInPc4	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
Zemský	zemský	k2eAgInSc1d1	zemský
hřebčinec	hřebčinec	k1gInSc1	hřebčinec
Tlumačov	Tlumačov	k1gInSc1	Tlumačov
</s>
