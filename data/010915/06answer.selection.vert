<s>
Decibel	decibel	k1gInSc1	decibel
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
nejznámější	známý	k2eAgFnSc1d3	nejznámější
svým	svůj	k3xOyFgNnSc7	svůj
použitím	použití	k1gNnSc7	použití
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
hladiny	hladina	k1gFnSc2	hladina
intenzity	intenzita	k1gFnSc2	intenzita
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
obecné	obecný	k2eAgNnSc4d1	obecné
měřítko	měřítko	k1gNnSc4	měřítko
podílu	podíl	k1gInSc2	podíl
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
