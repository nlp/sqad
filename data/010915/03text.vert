<p>
<s>
Decibel	decibel	k1gInSc1	decibel
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
nejznámější	známý	k2eAgFnSc1d3	nejznámější
svým	svůj	k3xOyFgNnSc7	svůj
použitím	použití	k1gNnSc7	použití
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
hladiny	hladina	k1gFnSc2	hladina
intenzity	intenzita	k1gFnSc2	intenzita
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
obecné	obecný	k2eAgNnSc4d1	obecné
měřítko	měřítko	k1gNnSc4	měřítko
podílu	podíl	k1gInSc2	podíl
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednotka	jednotka	k1gFnSc1	jednotka
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
legálního	legální	k2eAgNnSc2d1	legální
měření	měření	k1gNnSc2	měření
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
fyzikálně	fyzikálně	k6eAd1	fyzikálně
bezrozměrnou	bezrozměrný	k2eAgFnSc4d1	bezrozměrná
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
procento	procento	k1gNnSc1	procento
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
decibel	decibel	k1gInSc1	decibel
logaritmická	logaritmický	k2eAgFnSc1d1	logaritmická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
definice	definice	k1gFnSc1	definice
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
objevením	objevení	k1gNnSc7	objevení
Fechner-Weberova	Fechner-Weberův	k2eAgInSc2d1	Fechner-Weberův
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
že	že	k8xS	že
totiž	totiž	k9	totiž
lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc1	tělo
vnímá	vnímat	k5eAaImIp3nS	vnímat
podněty	podnět	k1gInPc4	podnět
logaritmicky	logaritmicky	k6eAd1	logaritmicky
jejich	jejich	k3xOp3gFnSc3	jejich
intenzitě	intenzita	k1gFnSc3	intenzita
(	(	kIx(	(
<g/>
i	i	k8xC	i
velké	velký	k2eAgFnPc1d1	velká
změny	změna	k1gFnPc1	změna
velkých	velký	k2eAgInPc2d1	velký
podnětů	podnět	k1gInPc2	podnět
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
jen	jen	k9	jen
malé	malý	k2eAgFnPc4d1	malá
změny	změna	k1gFnPc4	změna
počitků	počitek	k1gInPc2	počitek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
inženýry	inženýr	k1gMnPc7	inženýr
Bellových	Bellův	k2eAgFnPc2d1	Bellova
laboratoří	laboratoř	k1gFnPc2	laboratoř
původně	původně	k6eAd1	původně
sloužila	sloužit	k5eAaImAgFnS	sloužit
k	k	k7c3	k
udávání	udávání	k1gNnSc3	udávání
útlumu	útlum	k1gInSc2	útlum
telefonního	telefonní	k2eAgNnSc2d1	telefonní
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pokles	pokles	k1gInSc1	pokles
(	(	kIx(	(
<g/>
útlum	útlum	k1gInSc1	útlum
<g/>
)	)	kIx)	)
o	o	k7c4	o
3	[number]	k4	3
dB	db	kA	db
u	u	k7c2	u
výkonu	výkon	k1gInSc2	výkon
značí	značit	k5eAaImIp3nS	značit
poloviční	poloviční	k2eAgInSc4d1	poloviční
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
zisk	zisk	k1gInSc4	zisk
(	(	kIx(	(
<g/>
zesílení	zesílení	k1gNnSc2	zesílení
<g/>
)	)	kIx)	)
o	o	k7c4	o
3	[number]	k4	3
dB	db	kA	db
je	být	k5eAaImIp3nS	být
dvojnásobný	dvojnásobný	k2eAgInSc1d1	dvojnásobný
výkon	výkon	k1gInSc1	výkon
(	(	kIx(	(
<g/>
pozor	pozor	k1gInSc1	pozor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
veličiny	veličina	k1gFnPc4	veličina
jako	jako	k8xS	jako
např.	např.	kA	např.
napěťový	napěťový	k2eAgInSc1d1	napěťový
přenos	přenos	k1gInSc1	přenos
toto	tento	k3xDgNnSc4	tento
nemusí	muset	k5eNaImIp3nS	muset
platit	platit	k5eAaImF	platit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Akustika	akustika	k1gFnSc1	akustika
==	==	k?	==
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
reality	realita	k1gFnSc2	realita
se	se	k3xPyFc4	se
uplatnilo	uplatnit	k5eAaPmAgNnS	uplatnit
zejména	zejména	k9	zejména
v	v	k7c6	v
akustice	akustika	k1gFnSc6	akustika
<g/>
:	:	kIx,	:
na	na	k7c6	na
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
dobrovolníky	dobrovolník	k1gMnPc7	dobrovolník
a	a	k8xC	a
mrtvou	mrtvý	k2eAgFnSc7d1	mrtvá
komorou	komora	k1gFnSc7	komora
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
průměrný	průměrný	k2eAgMnSc1d1	průměrný
jedinec	jedinec	k1gMnSc1	jedinec
slyší	slyšet	k5eAaImIp3nS	slyšet
nejvýrazněji	výrazně	k6eAd3	výrazně
frekvence	frekvence	k1gFnSc1	frekvence
kolem	kolem	k7c2	kolem
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
etalonu	etalon	k1gInSc2	etalon
se	se	k3xPyFc4	se
použil	použít	k5eAaPmAgInS	použít
sinusový	sinusový	k2eAgInSc1d1	sinusový
tón	tón	k1gInSc1	tón
1000	[number]	k4	1000
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pouštěl	pouštět	k5eAaImAgMnS	pouštět
velmi	velmi	k6eAd1	velmi
potichu	potichu	k6eAd1	potichu
v	v	k7c6	v
absolutně	absolutně	k6eAd1	absolutně
tichém	tichý	k2eAgNnSc6d1	tiché
<g/>
,	,	kIx,	,
bezodrazovém	bezodrazový	k2eAgNnSc6d1	bezodrazový
prostředí	prostředí	k1gNnSc6	prostředí
jedincům	jedinec	k1gMnPc3	jedinec
s	s	k7c7	s
odpočatým	odpočatý	k2eAgInSc7d1	odpočatý
sluchem	sluch	k1gInSc7	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Zjistilo	zjistit	k5eAaPmAgNnS	zjistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
průměrný	průměrný	k2eAgMnSc1d1	průměrný
jedinec	jedinec	k1gMnSc1	jedinec
jej	on	k3xPp3gNnSc4	on
začne	začít	k5eAaPmIp3nS	začít
vnímat	vnímat	k5eAaImF	vnímat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
komoře	komora	k1gFnSc6	komora
hladina	hladina	k1gFnSc1	hladina
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
p	p	k?	p
<g/>
0	[number]	k4	0
=	=	kIx~	=
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
5	[number]	k4	5
Pa	Pa	kA	Pa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Logaritmováním	logaritmování	k1gNnSc7	logaritmování
poměru	poměr	k1gInSc2	poměr
zvukového	zvukový	k2eAgInSc2d1	zvukový
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
tohoto	tento	k3xDgInSc2	tento
stanoveného	stanovený	k2eAgInSc2d1	stanovený
nejslabšího	slabý	k2eAgInSc2d3	nejslabší
slyšitelného	slyšitelný	k2eAgInSc2d1	slyšitelný
zvuku	zvuk	k1gInSc2	zvuk
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
relativní	relativní	k2eAgInSc1d1	relativní
(	(	kIx(	(
<g/>
bezrozměrné	bezrozměrný	k2eAgNnSc1d1	bezrozměrné
<g/>
)	)	kIx)	)
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k8xS	jako
bel	bel	k1gInSc1	bel
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
desetkrát	desetkrát	k6eAd1	desetkrát
podrobnější	podrobný	k2eAgFnSc7d2	podrobnější
jednotkou	jednotka	k1gFnSc7	jednotka
decibel	decibel	k1gInSc1	decibel
(	(	kIx(	(
<g/>
odvozená	odvozený	k2eAgFnSc1d1	odvozená
pomocí	pomocí	k7c2	pomocí
předpony	předpona	k1gFnSc2	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
deci	deci	k1gNnSc2	deci
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
skotském	skotský	k1gInSc6	skotský
vynálezci	vynálezce	k1gMnPc1	vynálezce
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
mluvilo	mluvit	k5eAaImAgNnS	mluvit
jako	jako	k9	jako
o	o	k7c4	o
vynálezci	vynálezce	k1gMnPc1	vynálezce
telefonu	telefon	k1gInSc2	telefon
<g/>
,	,	kIx,	,
A.	A.	kA	A.
G.	G.	kA	G.
Bellovi	Bellovi	k1gRnPc1	Bellovi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označíme	označit	k5eAaPmIp1nP	označit
<g/>
-li	i	k?	-li
hladinu	hladina	k1gFnSc4	hladina
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
Lp	Lp	k1gFnSc2	Lp
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
20	[number]	k4	20
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
dB	db	kA	db
<g/>
;	;	kIx,	;
<g/>
Pa	Pa	kA	Pa
<g/>
;	;	kIx,	;
<g/>
Pa	Pa	kA	Pa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Pa	Pa	kA	Pa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gFnSc6	L_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
10	[number]	k4	10
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
20	[number]	k4	20
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
p_	p_	k?	p_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
dB	db	kA	db
<g/>
;	;	kIx,	;
<g/>
Pa	Pa	kA	Pa
<g/>
;	;	kIx,	;
<g/>
Pa	Pa	kA	Pa
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
Pa	Pa	kA	Pa
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
logaritmus	logaritmus	k1gInSc4	logaritmus
o	o	k7c6	o
základu	základ	k1gInSc6	základ
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proč	proč	k6eAd1	proč
druhé	druhý	k4xOgFnPc1	druhý
mocniny	mocnina	k1gFnPc1	mocnina
<g/>
?	?	kIx.	?
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
výhodné	výhodný	k2eAgNnSc1d1	výhodné
zavést	zavést	k5eAaPmF	zavést
jednotku	jednotka	k1gFnSc4	jednotka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pracovala	pracovat	k5eAaImAgFnS	pracovat
primárně	primárně	k6eAd1	primárně
raději	rád	k6eAd2	rád
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
a	a	k8xC	a
výkon	výkon	k1gInSc1	výkon
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
se	s	k7c7	s
čtvercem	čtverec	k1gInSc7	čtverec
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
mikrofony	mikrofon	k1gInPc1	mikrofon
při	při	k7c6	při
měřeních	měření	k1gNnPc6	měření
ovšem	ovšem	k9	ovšem
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
tlak	tlak	k1gInSc4	tlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Definujeme	definovat	k5eAaBmIp1nP	definovat
hladinu	hladina	k1gFnSc4	hladina
intenzity	intenzita	k1gFnSc2	intenzita
LI	li	k8xS	li
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
I	I	kA	I
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
10	[number]	k4	10
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
I	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
I_	I_	k1gMnSc1	I_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
right	right	k1gInSc1	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
[	[	kIx(	[
<g/>
dB	db	kA	db
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
W	W	kA	W
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
W	W	kA	W
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
I_	I_	k1gMnSc1	I_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
W	W	kA	W
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
nevnímá	vnímat	k5eNaImIp3nS	vnímat
stejně	stejně	k6eAd1	stejně
hlasitě	hlasitě	k6eAd1	hlasitě
stejně	stejně	k6eAd1	stejně
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
podněty	podnět	k1gInPc4	podnět
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
frekvencích	frekvence	k1gFnPc6	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
vnímání	vnímání	k1gNnSc1	vnímání
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
mění	měnit	k5eAaImIp3nS	měnit
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
hlasitostech	hlasitost	k1gFnPc6	hlasitost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgNnPc2	tento
měření	měření	k1gNnPc2	měření
jsou	být	k5eAaImIp3nP	být
ISO	ISO	kA	ISO
křivky	křivka	k1gFnSc2	křivka
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Fletcher-Munsonovy	Fletcher-Munsonův	k2eAgFnSc2d1	Fletcher-Munsonův
křivky	křivka	k1gFnSc2	křivka
<g/>
)	)	kIx)	)
stejné	stejný	k2eAgFnSc2d1	stejná
hlasitosti	hlasitost	k1gFnSc2	hlasitost
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Aby	aby	k9	aby
změřené	změřený	k2eAgFnPc1d1	změřená
hodnoty	hodnota	k1gFnPc1	hodnota
více	hodně	k6eAd2	hodně
reflektovaly	reflektovat	k5eAaImAgFnP	reflektovat
lidské	lidský	k2eAgNnSc4d1	lidské
vnímání	vnímání	k1gNnSc4	vnímání
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
analyticky	analyticky	k6eAd1	analyticky
jednoduše	jednoduše	k6eAd1	jednoduše
vyjádřitelné	vyjádřitelný	k2eAgFnPc4d1	vyjádřitelná
korekční	korekční	k2eAgFnPc4d1	korekční
křivky	křivka	k1gFnPc4	křivka
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Křivka	křivka	k1gFnSc1	křivka
C	C	kA	C
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
125	[number]	k4	125
Hz	Hz	kA	Hz
až	až	k9	až
1	[number]	k4	1
kHz	khz	kA	khz
konstantně	konstantně	k6eAd1	konstantně
rovna	roven	k2eAgFnSc1d1	rovna
nule	nula	k1gFnSc3	nula
(	(	kIx(	(
<g/>
nezavádí	zavádět	k5eNaImIp3nS	zavádět
žádnou	žádný	k3yNgFnSc4	žádný
korekci	korekce	k1gFnSc4	korekce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
tímto	tento	k3xDgInSc7	tento
intervalem	interval	k1gInSc7	interval
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
zavádí	zavádět	k5eAaImIp3nS	zavádět
zápornou	záporný	k2eAgFnSc4d1	záporná
korekci	korekce	k1gFnSc4	korekce
<g/>
.	.	kIx.	.
</s>
<s>
Křivka	křivka	k1gFnSc1	křivka
B	B	kA	B
připodobňuje	připodobňovat	k5eAaImIp3nS	připodobňovat
měření	měření	k1gNnSc2	měření
subjektivnímu	subjektivní	k2eAgInSc3d1	subjektivní
vjemu	vjem	k1gInSc3	vjem
hlasitých	hlasitý	k2eAgInPc2d1	hlasitý
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
křivka	křivka	k1gFnSc1	křivka
A	A	kA	A
vjemu	vjem	k1gInSc2	vjem
slabších	slabý	k2eAgInPc2d2	slabší
zvuků	zvuk	k1gInPc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
udáváme	udávat	k5eAaImIp1nP	udávat
veličinu	veličina	k1gFnSc4	veličina
upravenou	upravený	k2eAgFnSc4d1	upravená
pomocí	pomocí	k7c2	pomocí
korekční	korekční	k2eAgFnSc2d1	korekční
křivky	křivka	k1gFnSc2	křivka
<g/>
,	,	kIx,	,
neudáváme	udávat	k5eNaImIp1nP	udávat
už	už	k9	už
hladinu	hladina	k1gFnSc4	hladina
tlaku	tlak	k1gInSc2	tlak
<g/>
/	/	kIx~	/
<g/>
intenzity	intenzita	k1gFnSc2	intenzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hladinu	hladina	k1gFnSc4	hladina
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
za	za	k7c4	za
značku	značka	k1gFnSc4	značka
decibelu	decibel	k1gInSc2	decibel
se	se	k3xPyFc4	se
do	do	k7c2	do
závorky	závorka	k1gFnSc2	závorka
doplní	doplnit	k5eAaPmIp3nS	doplnit
symbol	symbol	k1gInSc1	symbol
použité	použitý	k2eAgFnSc2d1	použitá
korekční	korekční	k2eAgFnSc2d1	korekční
křivky	křivka	k1gFnSc2	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
20	[number]	k4	20
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
dB	db	kA	db
<g/>
;	;	kIx,	;
<g/>
Pa	Pa	kA	Pa
<g/>
;	;	kIx,	;
<g/>
Pa	Pa	kA	Pa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Pa	Pa	kA	Pa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gFnSc6	L_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
20	[number]	k4	20
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
dB	db	kA	db
<g/>
;	;	kIx,	;
<g/>
Pa	Pa	kA	Pa
<g/>
;	;	kIx,	;
<g/>
Pa	Pa	kA	Pa
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
Pa	Pa	kA	Pa
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Zvukoměr	zvukoměr	k1gInSc1	zvukoměr
je	být	k5eAaImIp3nS	být
přístroj	přístroj	k1gInSc4	přístroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měří	měřit	k5eAaImIp3nS	měřit
přesným	přesný	k2eAgInSc7d1	přesný
mikrofonem	mikrofon	k1gInSc7	mikrofon
akustický	akustický	k2eAgInSc4d1	akustický
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
převádí	převádět	k5eAaImIp3nS	převádět
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
střídavé	střídavý	k2eAgNnSc4d1	střídavé
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
potřeb	potřeba	k1gFnPc2	potřeba
měření	měření	k1gNnSc2	měření
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zařazení	zařazení	k1gNnSc4	zařazení
některého	některý	k3yIgInSc2	některý
z	z	k7c2	z
filtrů	filtr	k1gInPc2	filtr
realizujícího	realizující	k2eAgInSc2d1	realizující
křivky	křivka	k1gFnPc4	křivka
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Změřené	změřený	k2eAgNnSc1d1	změřené
napětí	napětí	k1gNnSc1	napětí
pak	pak	k6eAd1	pak
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
na	na	k7c6	na
voltmetru	voltmetr	k1gInSc6	voltmetr
ocejchovaném	ocejchovaný	k2eAgInSc6d1	ocejchovaný
v	v	k7c6	v
decibelech	decibel	k1gInPc6	decibel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přímá	přímý	k2eAgFnSc1d1	přímá
souvislost	souvislost	k1gFnSc1	souvislost
hladiny	hladina	k1gFnSc2	hladina
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
s	s	k7c7	s
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
napětím	napětí	k1gNnSc7	napětí
potřebným	potřebný	k2eAgNnSc7d1	potřebné
k	k	k7c3	k
vybuzení	vybuzení	k1gNnSc3	vybuzení
rádiového	rádiový	k2eAgInSc2d1	rádiový
vysílače	vysílač	k1gInSc2	vysílač
nebo	nebo	k8xC	nebo
elektroakustického	elektroakustický	k2eAgInSc2d1	elektroakustický
měniče	měnič	k1gInSc2	měnič
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvukový	zvukový	k2eAgMnSc1d1	zvukový
mistr	mistr	k1gMnSc1	mistr
pracující	pracující	k1gMnSc1	pracující
se	s	k7c7	s
středoevropským	středoevropský	k2eAgNnSc7d1	středoevropské
zvukovým	zvukový	k2eAgNnSc7d1	zvukové
režijním	režijní	k2eAgNnSc7d1	režijní
zařízením	zařízení	k1gNnSc7	zařízení
má	mít	k5eAaImIp3nS	mít
indikátor	indikátor	k1gInSc1	indikátor
vybuzení	vybuzení	k1gNnSc2	vybuzení
(	(	kIx(	(
<g/>
voltmetr	voltmetr	k1gInSc1	voltmetr
splňující	splňující	k2eAgFnSc1d1	splňující
přesná	přesný	k2eAgFnSc1d1	přesná
kritéria	kritérion	k1gNnSc2	kritérion
chování	chování	k1gNnSc4	chování
<g/>
)	)	kIx)	)
ocejchovaný	ocejchovaný	k2eAgMnSc1d1	ocejchovaný
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
decibelech	decibel	k1gInPc6	decibel
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
korekčních	korekční	k2eAgFnPc2d1	korekční
křivek	křivka	k1gFnPc2	křivka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
L	L	kA	L
značí	značit	k5eAaImIp3nS	značit
obecnou	obecný	k2eAgFnSc4d1	obecná
úroveň	úroveň	k1gFnSc4	úroveň
-	-	kIx~	-
level	level	k1gInSc1	level
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Půjde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
studiové	studiový	k2eAgNnSc4d1	studiové
zařízení	zařízení	k1gNnSc4	zařízení
firmy	firma	k1gFnSc2	firma
z	z	k7c2	z
éry	éra	k1gFnSc2	éra
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
20	[number]	k4	20
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
dB	db	kA	db
<g/>
;	;	kIx,	;
<g/>
V	V	kA	V
<g/>
;	;	kIx,	;
<g/>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
55	[number]	k4	55
</s>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
dB	db	kA	db
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
20	[number]	k4	20
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
right	right	k1gInSc1	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
dB	db	kA	db
<g/>
;	;	kIx,	;
<g/>
V	V	kA	V
<g/>
;	;	kIx,	;
<g/>
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1,55	[number]	k4	1,55
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Moderní	moderní	k2eAgNnPc1d1	moderní
studiová	studiový	k2eAgNnPc1d1	studiové
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
všechna	všechen	k3xTgNnPc1	všechen
telekomunikační	telekomunikační	k2eAgNnPc1d1	telekomunikační
zařízení	zařízení	k1gNnPc1	zařízení
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
referenční	referenční	k2eAgFnSc7d1	referenční
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
20	[number]	k4	20
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
dBu	dBu	k?	dBu
<g/>
;	;	kIx,	;
<g/>
V	V	kA	V
<g/>
;	;	kIx,	;
<g/>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
775	[number]	k4	775
</s>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gFnSc6	L_
<g/>
{	{	kIx(	{
<g/>
dBu	dBu	k?	dBu
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
20	[number]	k4	20
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
right	right	k1gInSc1	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
dBu	dBu	k?	dBu
<g/>
;	;	kIx,	;
<g/>
V	V	kA	V
<g/>
;	;	kIx,	;
<g/>
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0,775	[number]	k4	0,775
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Komerční	komerční	k2eAgNnPc1d1	komerční
zařízení	zařízení	k1gNnPc1	zařízení
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
jinou	jiný	k2eAgFnSc7d1	jiná
referenční	referenční	k2eAgFnSc7d1	referenční
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
20	[number]	k4	20
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
dBV	dBV	k?	dBV
<g/>
;	;	kIx,	;
<g/>
V	V	kA	V
<g/>
;	;	kIx,	;
<g/>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
00	[number]	k4	00
</s>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gFnSc6	L_
<g/>
{	{	kIx(	{
<g/>
dBV	dBV	k?	dBV
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
20	[number]	k4	20
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
right	right	k1gInSc1	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
dBV	dBV	k?	dBV
<g/>
;	;	kIx,	;
<g/>
V	V	kA	V
<g/>
;	;	kIx,	;
<g/>
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1,00	[number]	k4	1,00
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
průměru	průměr	k1gInSc2	průměr
v	v	k7c6	v
čase	čas	k1gInSc6	čas
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
použít	použít	k5eAaPmF	použít
aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
průměr	průměr	k1gInSc4	průměr
argumentu	argument	k1gInSc2	argument
logaritmu	logaritmus	k1gInSc2	logaritmus
<g/>
.	.	kIx.	.
</s>
<s>
Aritmetický	aritmetický	k2eAgInSc1d1	aritmetický
průměr	průměr	k1gInSc1	průměr
decibelů	decibel	k1gInPc2	decibel
(	(	kIx(	(
<g/>
geometrický	geometrický	k2eAgInSc1d1	geometrický
průměr	průměr	k1gInSc1	průměr
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
dává	dávat	k5eAaImIp3nS	dávat
zkreslené	zkreslený	k2eAgInPc4d1	zkreslený
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fyziologické	fyziologický	k2eAgNnSc4d1	fyziologické
porovnávání	porovnávání	k1gNnSc4	porovnávání
hladiny	hladina	k1gFnSc2	hladina
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
úrovně	úroveň	k1gFnSc2	úroveň
akustického	akustický	k2eAgInSc2d1	akustický
hluku	hluk	k1gInSc2	hluk
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
tuto	tento	k3xDgFnSc4	tento
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Práh	práh	k1gInSc1	práh
slyšitelnosti	slyšitelnost	k1gFnSc2	slyšitelnost
je	být	k5eAaImIp3nS	být
0	[number]	k4	0
dB	db	kA	db
</s>
</p>
<p>
<s>
Tichý	tichý	k2eAgInSc1d1	tichý
pokoj	pokoj	k1gInSc1	pokoj
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
33	[number]	k4	33
dB	db	kA	db
</s>
</p>
<p>
<s>
Tikot	tikot	k1gInSc1	tikot
hodin	hodina	k1gFnPc2	hodina
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
35	[number]	k4	35
dB	db	kA	db
</s>
</p>
<p>
<s>
Šum	šum	k1gInSc1	šum
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
40	[number]	k4	40
dB	db	kA	db
</s>
</p>
<p>
<s>
Šepot	šepot	k1gInSc1	šepot
z	z	k7c2	z
10	[number]	k4	10
cm	cm	kA	cm
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
50	[number]	k4	50
dB	db	kA	db
</s>
</p>
<p>
<s>
Šelest	šelest	k1gInSc1	šelest
listí	listí	k1gNnSc2	listí
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
60	[number]	k4	60
dB	db	kA	db
</s>
</p>
<p>
<s>
Kytara	kytara	k1gFnSc1	kytara
z	z	k7c2	z
40	[number]	k4	40
cm	cm	kA	cm
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
70	[number]	k4	70
dB	db	kA	db
</s>
</p>
<p>
<s>
Silný	silný	k2eAgInSc1d1	silný
provoz	provoz	k1gInSc1	provoz
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
80	[number]	k4	80
dB	db	kA	db
</s>
</p>
<p>
<s>
Saxofon	saxofon	k1gInSc1	saxofon
z	z	k7c2	z
40	[number]	k4	40
cm	cm	kA	cm
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
92	[number]	k4	92
dB	db	kA	db
</s>
</p>
<p>
<s>
klavír	klavír	k1gInSc4	klavír
ze	z	k7c2	z
40	[number]	k4	40
cm	cm	kA	cm
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
93	[number]	k4	93
dB	db	kA	db
</s>
</p>
<p>
<s>
Hlasitý	hlasitý	k2eAgInSc1d1	hlasitý
výkřik	výkřik	k1gInSc1	výkřik
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
96	[number]	k4	96
dB	db	kA	db
</s>
</p>
<p>
<s>
Práh	práh	k1gInSc1	práh
nepříjemnosti	nepříjemnost	k1gFnSc2	nepříjemnost
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
102	[number]	k4	102
dB	db	kA	db
</s>
</p>
<p>
<s>
Vzlet	vzlet	k1gInSc1	vzlet
tryskového	tryskový	k2eAgNnSc2d1	tryskové
letadla	letadlo	k1gNnSc2	letadlo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
116	[number]	k4	116
dB	db	kA	db
</s>
</p>
<p>
<s>
Výstřel	výstřel	k1gInSc1	výstřel
z	z	k7c2	z
děla	dělo	k1gNnSc2	dělo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
120	[number]	k4	120
dB	db	kA	db
</s>
</p>
<p>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
dělostřeleckého	dělostřelecký	k2eAgInSc2d1	dělostřelecký
granátu	granát	k1gInSc2	granát
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
asi	asi	k9	asi
132	[number]	k4	132
dB	db	kA	db
</s>
</p>
<p>
<s>
===	===	k?	===
Limity	limit	k1gInPc1	limit
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
stanoví	stanovit	k5eAaPmIp3nS	stanovit
limity	limit	k1gInPc4	limit
hluku	hluk	k1gInSc2	hluk
nařízení	nařízení	k1gNnSc2	nařízení
vlády	vláda	k1gFnSc2	vláda
272	[number]	k4	272
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
hladinou	hladina	k1gFnSc7	hladina
pro	pro	k7c4	pro
venkovní	venkovní	k2eAgFnPc4d1	venkovní
prostory	prostora	k1gFnPc4	prostora
je	být	k5eAaImIp3nS	být
limit	limit	k1gInSc1	limit
50	[number]	k4	50
dB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
o	o	k7c4	o
10	[number]	k4	10
dB	db	kA	db
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
hlukovou	hlukový	k2eAgFnSc4d1	hluková
zátěž	zátěž	k1gFnSc4	zátěž
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
komunikací	komunikace	k1gFnPc2	komunikace
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
až	až	k9	až
o	o	k7c4	o
20	[number]	k4	20
dB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
hladina	hladina	k1gFnSc1	hladina
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
buď	buď	k8xC	buď
měřením	měření	k1gNnSc7	měření
či	či	k8xC	či
výpočtem	výpočet	k1gInSc7	výpočet
podle	podle	k7c2	podle
provozu	provoz	k1gInSc2	provoz
na	na	k7c4	na
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elektřina	elektřina	k1gFnSc1	elektřina
a	a	k8xC	a
elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
decibelu	decibel	k1gInSc2	decibel
se	se	k3xPyFc4	se
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
i	i	k9	i
výkonové	výkonový	k2eAgFnPc1d1	výkonová
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
i	i	k9	i
v	v	k7c6	v
elektrickém	elektrický	k2eAgInSc6d1	elektrický
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vysílače	vysílač	k1gInPc4	vysílač
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
G	G	kA	G
-	-	kIx~	-
angl.	angl.	k?	angl.
gain	gain	k1gInSc1	gain
-	-	kIx~	-
zisk	zisk	k1gInSc1	zisk
<g/>
,	,	kIx,	,
m	m	kA	m
-	-	kIx~	-
miliwatt	miliwatt	k1gInSc1	miliwatt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
si	se	k3xPyFc3	se
opět	opět	k5eAaPmF	opět
povšimnout	povšimnout	k5eAaPmF	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
násobíme	násobit	k5eAaImIp1nP	násobit
desíti	deset	k4xCc6	deset
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
dBm	dBm	k?	dBm
<g/>
;	;	kIx,	;
<g/>
W	W	kA	W
<g/>
;	;	kIx,	;
<g/>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
001	[number]	k4	001
</s>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G_	G_	k1gMnSc1	G_
<g/>
{	{	kIx(	{
<g/>
dB	db	kA	db
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
10	[number]	k4	10
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
P_	P_	k1gMnSc1	P_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
right	right	k1gInSc1	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
dBm	dBm	k?	dBm
<g/>
;	;	kIx,	;
<g/>
W	W	kA	W
<g/>
;	;	kIx,	;
<g/>
W	W	kA	W
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
P_	P_	k1gMnSc1	P_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0,001	[number]	k4	0,001
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
W	W	kA	W
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
označení	označení	k1gNnPc2	označení
L	L	kA	L
<g/>
,	,	kIx,	,
G	G	kA	G
nebo	nebo	k8xC	nebo
AP	ap	kA	ap
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
dán	dát	k5eAaPmNgMnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
L	L	kA	L
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
referenčnímu	referenční	k2eAgInSc3d1	referenční
výkonu	výkon	k1gInSc3	výkon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
P	P	kA	P
<g/>
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
mW	mW	k?	mW
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
G	G	kA	G
a	a	k8xC	a
AP	ap	kA	ap
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
skutečného	skutečný	k2eAgInSc2d1	skutečný
poměru	poměr	k1gInSc2	poměr
2	[number]	k4	2
výkonů	výkon	k1gInPc2	výkon
-	-	kIx~	-
například	například	k6eAd1	například
zesílení	zesílení	k1gNnSc1	zesílení
zesilovače	zesilovač	k1gInSc2	zesilovač
a	a	k8xC	a
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
dBm	dBm	k?	dBm
a	a	k8xC	a
(	(	kIx(	(
<g/>
decibel	decibel	k1gInSc1	decibel
nad	nad	k7c7	nad
miliwattem	miliwatt	k1gInSc7	miliwatt
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
přepočítat	přepočítat	k5eAaPmF	přepočítat
výkon	výkon	k1gInSc4	výkon
na	na	k7c4	na
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
udána	udat	k5eAaPmNgFnS	udat
i	i	k9	i
impedance	impedance	k1gFnSc1	impedance
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
řečeno	říct	k5eAaPmNgNnS	říct
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
normovaná	normovaný	k2eAgFnSc1d1	normovaná
impedance	impedance	k1gFnSc1	impedance
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
600	[number]	k4	600
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
600	[number]	k4	600
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gNnSc2	omega
}	}	kIx)	}
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
výkon	výkon	k1gInSc1	výkon
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
mW	mW	k?	mW
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c6	na
impedanci	impedance	k1gFnSc6	impedance
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
600	[number]	k4	600
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
600	[number]	k4	600
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gNnSc2	omega
}	}	kIx)	}
</s>
</p>
<p>
<s>
přepočítáme	přepočítat	k5eAaPmIp1nP	přepočítat
na	na	k7c4	na
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
hodnotu	hodnota	k1gFnSc4	hodnota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
775	[number]	k4	775
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
0,775	[number]	k4	0,775
<g/>
V	V	kA	V
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zpracovávání	zpracovávání	k1gNnSc2	zpracovávání
zvuku	zvuk	k1gInSc2	zvuk
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
digitální	digitální	k2eAgFnSc2d1	digitální
techniky	technika	k1gFnSc2	technika
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
přirozená	přirozený	k2eAgFnSc1d1	přirozená
další	další	k2eAgFnSc1d1	další
metamorfóza	metamorfóza	k1gFnSc1	metamorfóza
decibelu	decibel	k1gInSc2	decibel
<g/>
:	:	kIx,	:
mějme	mít	k5eAaImRp1nP	mít
analogově	analogově	k6eAd1	analogově
<g/>
/	/	kIx~	/
<g/>
digitální	digitální	k2eAgInSc1d1	digitální
převodník	převodník	k1gInSc1	převodník
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
zpracováváme	zpracovávat	k5eAaImIp1nP	zpracovávat
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
20	[number]	k4	20
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gFnSc6	L_
<g/>
{	{	kIx(	{
<g/>
dBFS	dBFS	k?	dBFS
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
20	[number]	k4	20
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
w_	w_	k?	w_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
[	[	kIx(	[
<g/>
dBFS	dBFS	k?	dBFS
<g/>
;	;	kIx,	;
<g/>
-	-	kIx~	-
<g/>
;	;	kIx,	;
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
<g/>
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
největší	veliký	k2eAgNnSc4d3	veliký
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
převodník	převodník	k1gInSc1	převodník
schopen	schopen	k2eAgInSc1d1	schopen
zpracovat	zpracovat	k5eAaPmF	zpracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
údajů	údaj	k1gInPc2	údaj
převodníků	převodník	k1gMnPc2	převodník
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
fyzikálně	fyzikálně	k6eAd1	fyzikálně
bezrozměrné	bezrozměrný	k2eAgFnPc4d1	bezrozměrná
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnSc2	písmeno
FS	FS	kA	FS
značí	značit	k5eAaImIp3nS	značit
Full	Full	k1gInSc4	Full
Scale	Scale	k1gFnSc2	Scale
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
plný	plný	k2eAgInSc4d1	plný
rozsah	rozsah	k1gInSc4	rozsah
(	(	kIx(	(
<g/>
rozuměj	rozumět	k5eAaImRp2nS	rozumět
převodníku	převodník	k1gMnSc5	převodník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Radiotechnika	radiotechnika	k1gFnSc1	radiotechnika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
radiotechnice	radiotechnika	k1gFnSc6	radiotechnika
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
dBi	dBi	k?	dBi
zisk	zisk	k1gInSc1	zisk
antény	anténa	k1gFnSc2	anténa
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
izotropní	izotropní	k2eAgFnSc7d1	izotropní
anténou	anténa	k1gFnSc7	anténa
<g/>
,	,	kIx,	,
dBd	dBd	k?	dBd
zisk	zisk	k1gInSc1	zisk
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
půlvlnným	půlvlnný	k2eAgInSc7d1	půlvlnný
dipólem	dipól	k1gInSc7	dipól
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
dBd	dBd	k?	dBd
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
setkáme	setkat	k5eAaPmIp1nP	setkat
jenom	jenom	k9	jenom
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
dB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
dBi	dBi	k?	dBi
=	=	kIx~	=
2,16	[number]	k4	2,16
+	+	kIx~	+
dBd	dBd	k?	dBd
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G_	G_	k1gFnSc6	G_
<g/>
{	{	kIx(	{
<g/>
dBi	dBi	k?	dBi
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
10	[number]	k4	10
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
log	log	kA	log
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
P_	P_	k1gMnSc1	P_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
[	[	kIx(	[
<g/>
dBi	dBi	k?	dBi
<g/>
;	;	kIx,	;
<g/>
W	W	kA	W
<g/>
;	;	kIx,	;
<g/>
W	W	kA	W
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
decibel	decibel	k1gInSc1	decibel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
