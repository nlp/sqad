<p>
<s>
Bajka	bajka	k1gFnSc1	bajka
je	být	k5eAaImIp3nS	být
literární	literární	k2eAgInSc4d1	literární
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Za	za	k7c4	za
jejího	její	k3xOp3gMnSc4	její
zakladatele	zakladatel	k1gMnSc4	zakladatel
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
otrok	otrok	k1gMnSc1	otrok
Ezop	Ezop	k1gMnSc1	Ezop
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
prokazatelná	prokazatelný	k2eAgFnSc1d1	prokazatelná
historická	historický	k2eAgFnSc1d1	historická
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
bajky	bajka	k1gFnSc2	bajka
je	být	k5eAaImIp3nS	být
personifikace	personifikace	k1gFnSc1	personifikace
(	(	kIx(	(
<g/>
zosobnění	zosobnění	k1gNnSc1	zosobnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvířata	zvíře	k1gNnPc1	zvíře
mluví	mluvit	k5eAaImIp3nP	mluvit
jako	jako	k9	jako
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
je	on	k3xPp3gInPc4	on
provázejí	provázet	k5eAaImIp3nP	provázet
jiné	jiný	k2eAgInPc1d1	jiný
nepřirozené	přirozený	k2eNgInPc1d1	nepřirozený
jevy	jev	k1gInPc1	jev
<g/>
;	;	kIx,	;
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
jednání	jednání	k1gNnSc2	jednání
vyplyne	vyplynout	k5eAaPmIp3nS	vyplynout
nějaké	nějaký	k3yIgNnSc1	nějaký
poučení	poučení	k1gNnSc1	poučení
či	či	k8xC	či
kritika	kritika	k1gFnSc1	kritika
chyby	chyba	k1gFnSc2	chyba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
bývá	bývat	k5eAaImIp3nS	bývat
čtenářovi	čtenář	k1gMnSc3	čtenář
již	již	k6eAd1	již
známa	známo	k1gNnSc2	známo
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
zvířata	zvíře	k1gNnPc4	zvíře
tím	ten	k3xDgInSc7	ten
získávají	získávat	k5eAaImIp3nP	získávat
lidské	lidský	k2eAgFnPc1d1	lidská
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
stávají	stávat	k5eAaImIp3nP	stávat
typické	typický	k2eAgInPc1d1	typický
(	(	kIx(	(
<g/>
např.	např.	kA	např.
liška	liška	k1gFnSc1	liška
je	být	k5eAaImIp3nS	být
chytrá	chytrý	k2eAgFnSc1d1	chytrá
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vychytralá	vychytralý	k2eAgFnSc1d1	vychytralá
<g/>
,	,	kIx,	,
osel	osel	k1gMnSc1	osel
hloupý	hloupý	k2eAgMnSc1d1	hloupý
<g/>
,	,	kIx,	,
vrána	vrána	k1gFnSc1	vrána
důmyslná	důmyslný	k2eAgFnSc1d1	důmyslná
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
požitkářská	požitkářský	k2eAgFnSc1d1	požitkářská
<g/>
,	,	kIx,	,
páv	páv	k1gMnSc1	páv
marnivý	marnivý	k2eAgMnSc1d1	marnivý
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
zvířatům	zvíře	k1gNnPc3	zvíře
přisuzují	přisuzovat	k5eAaImIp3nP	přisuzovat
standardizované	standardizovaný	k2eAgFnSc2d1	standardizovaná
role	role	k1gFnSc2	role
(	(	kIx(	(
<g/>
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jehně	jehně	k1gNnSc1	jehně
nebo	nebo	k8xC	nebo
kůzle	kůzle	k1gNnSc1	kůzle
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
oběti	oběť	k1gFnSc2	oběť
<g/>
,	,	kIx,	,
vlk	vlk	k1gMnSc1	vlk
zase	zase	k9	zase
jako	jako	k9	jako
škůdce	škůdce	k1gMnSc1	škůdce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Etologicky	etologicky	k6eAd1	etologicky
vzato	vzat	k2eAgNnSc1d1	vzato
zvířata	zvíře	k1gNnPc1	zvíře
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
nositeli	nositel	k1gMnPc7	nositel
těchto	tento	k3xDgFnPc2	tento
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
jim	on	k3xPp3gMnPc3	on
je	on	k3xPp3gNnSc4	on
však	však	k9	však
připisují	připisovat	k5eAaImIp3nP	připisovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zjevu	zjev	k1gInSc2	zjev
nebo	nebo	k8xC	nebo
chybného	chybný	k2eAgNnSc2d1	chybné
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kanonizovaného	kanonizovaný	k2eAgNnSc2d1	kanonizované
pozorování	pozorování	k1gNnSc2	pozorování
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
poznatky	poznatek	k1gInPc1	poznatek
antických	antický	k2eAgMnPc2d1	antický
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
bajkách	bajka	k1gFnPc6	bajka
G.	G.	kA	G.
E.	E.	kA	E.
Lessinga	Lessinga	k1gFnSc1	Lessinga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
u	u	k7c2	u
daného	daný	k2eAgNnSc2d1	dané
zvířete	zvíře	k1gNnSc2	zvíře
zesíleny	zesílen	k2eAgFnPc1d1	zesílena
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
jejich	jejich	k3xOp3gNnSc7	jejich
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
a	a	k8xC	a
archetypem	archetyp	k1gInSc7	archetyp
<g/>
.	.	kIx.	.
</s>
<s>
Bajka	bajka	k1gFnSc1	bajka
tedy	tedy	k9	tedy
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
symbolikou	symbolika	k1gFnSc7	symbolika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
bajky	bajka	k1gFnSc2	bajka
je	být	k5eAaImIp3nS	být
kritizovat	kritizovat	k5eAaImF	kritizovat
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
společenské	společenský	k2eAgInPc4d1	společenský
nešvary	nešvar	k1gInPc4	nešvar
<g/>
,	,	kIx,	,
působit	působit	k5eAaImF	působit
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
vychovávat	vychovávat	k5eAaImF	vychovávat
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
těchto	tento	k3xDgInPc2	tento
nešvarů	nešvar	k1gInPc2	nešvar
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
tak	tak	k9	tak
děje	dít	k5eAaImIp3nS	dít
humornou	humorný	k2eAgFnSc7d1	humorná
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
pomocí	pomoc	k1gFnSc7	pomoc
ironie	ironie	k1gFnSc2	ironie
a	a	k8xC	a
satiry	satira	k1gFnSc2	satira
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bajky	bajka	k1gFnPc1	bajka
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
určeny	určit	k5eAaPmNgInP	určit
dospělým	dospělí	k1gMnPc3	dospělí
posluchačům	posluchač	k1gMnPc3	posluchač
a	a	k8xC	a
čtenářům	čtenář	k1gMnPc3	čtenář
<g/>
,	,	kIx,	,
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
bajky	bajka	k1gFnPc1	bajka
začaly	začít	k5eAaPmAgFnP	začít
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
posouvat	posouvat	k5eAaImF	posouvat
do	do	k7c2	do
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
útvary	útvar	k1gInPc1	útvar
podobné	podobný	k2eAgInPc1d1	podobný
bajkám	bajka	k1gFnPc3	bajka
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jak	jak	k6eAd1	jak
u	u	k7c2	u
jihoamerických	jihoamerický	k2eAgMnPc2d1	jihoamerický
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bajky	bajka	k1gFnSc2	bajka
kmene	kmen	k1gInSc2	kmen
Tupí	tupit	k5eAaImIp3nS	tupit
o	o	k7c6	o
želvě	želva	k1gFnSc6	želva
<g/>
)	)	kIx)	)
i	i	k8xC	i
severoamerických	severoamerický	k2eAgMnPc2d1	severoamerický
Indiánů	Indián	k1gMnPc2	Indián
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čerokíjské	čerokíjský	k2eAgFnPc4d1	čerokíjský
bajky	bajka	k1gFnPc4	bajka
o	o	k7c6	o
králíkovi	králík	k1gMnSc6	králík
a	a	k8xC	a
jelenovi	jelen	k1gMnSc6	jelen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
afrických	africký	k2eAgMnPc2d1	africký
domorodců	domorodec	k1gMnPc2	domorodec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bajky	bajka	k1gFnSc2	bajka
o	o	k7c6	o
promyce	promyka	k1gFnSc6	promyka
ichneumon	ichneumona	k1gFnPc2	ichneumona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
evropská	evropský	k2eAgFnSc1d1	Evropská
liška	liška	k1gFnSc1	liška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
bajky	bajka	k1gFnPc1	bajka
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
lze	lze	k6eAd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgInPc4	některý
rysy	rys	k1gInPc4	rys
společné	společný	k2eAgInPc4d1	společný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
představitelé	představitel	k1gMnPc1	představitel
==	==	k?	==
</s>
</p>
<p>
<s>
Ezop	Ezop	k1gMnSc1	Ezop
–	–	k?	–
asi	asi	k9	asi
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
Starověké	starověký	k2eAgNnSc4d1	starověké
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
nedoložen	doložen	k2eNgInSc4d1	nedoložen
</s>
</p>
<p>
<s>
Bidpaj	Bidpaj	k1gMnSc1	Bidpaj
–	–	k?	–
domnělý	domnělý	k2eAgMnSc1d1	domnělý
staroindický	staroindický	k2eAgMnSc1d1	staroindický
mudrc	mudrc	k1gMnSc1	mudrc
<g/>
,	,	kIx,	,
pokládaný	pokládaný	k2eAgMnSc1d1	pokládaný
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
sbírky	sbírka	k1gFnSc2	sbírka
Pančatantra	Pančatantr	k1gMnSc4	Pančatantr
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
de	de	k?	de
France	Franc	k1gMnSc2	Franc
(	(	kIx(	(
<g/>
1135	[number]	k4	1135
<g/>
–	–	k?	–
<g/>
1200	[number]	k4	1200
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Jean	Jean	k1gMnSc1	Jean
de	de	k?	de
La	la	k1gNnPc2	la
Fontaine	Fontain	k1gInSc5	Fontain
(	(	kIx(	(
<g/>
1621	[number]	k4	1621
<g/>
–	–	k?	–
<g/>
1695	[number]	k4	1695
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Andrejevič	Andrejevič	k1gMnSc1	Andrejevič
Krylov	Krylov	k1gInSc1	Krylov
(	(	kIx(	(
<g/>
1769	[number]	k4	1769
<g/>
–	–	k?	–
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc4	Georg
Orwell	Orwell	k1gMnSc1	Orwell
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Čeští	český	k2eAgMnPc1d1	český
představitelé	představitel	k1gMnPc1	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
Sofie	Sofie	k1gFnSc1	Sofie
Podlipská	Podlipský	k2eAgFnSc1d1	Podlipská
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
–	–	k?	–
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Václav	Václav	k1gMnSc1	Václav
Rais	Rais	k1gMnSc1	Rais
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Říha	Říha	k1gMnSc1	Říha
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Žáček	Žáček	k1gMnSc1	Žáček
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Antropomorfismus	antropomorfismus	k1gInSc1	antropomorfismus
</s>
</p>
<p>
<s>
Báje	báje	k1gFnSc1	báje
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bajka	bajka	k1gFnSc1	bajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Bajka	bajka	k1gFnSc1	bajka
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Bajky	bajka	k1gFnSc2	bajka
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bajka	bajka	k1gFnSc1	bajka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
bajka	bajka	k1gFnSc1	bajka
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
