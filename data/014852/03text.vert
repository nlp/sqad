<s>
Saas-Balen	Saas-Balen	k2eAgInSc1d1
</s>
<s>
Saas-Balen	Saas-Balen	k2eAgInSc1d1
Saas-Balen	Saas-Balen	k2eAgInSc1d1
Saas-Balen	Saas-Balen	k2eAgInSc1d1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
1.483	1.483	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Kanton	Kanton	k1gInSc4
</s>
<s>
Valais	Valais	k1gFnSc1
District	Districta	k1gFnPc2
</s>
<s>
Visp	Visp	k1gMnSc1
</s>
<s>
Saas-Balen	Saas-Balen	k2eAgInSc1d1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
30,18	30,18	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
366	#num#	k4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
12	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.saasbalen.ch	www.saasbalen.ch	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
6289	#num#	k4
PSČ	PSČ	kA
</s>
<s>
3908	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
VS	VS	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Saas-Balen	Saas-Balen	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc4
v	v	k7c6
německy	německy	k6eAd1
mluvící	mluvící	k2eAgFnSc6d1
části	část	k1gFnSc6
švýcarského	švýcarský	k2eAgInSc2d1
kantonu	kanton	k1gInSc2
Valais	Valais	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
okrese	okres	k1gInSc6
Visp	Visp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nejmenší	malý	k2eAgFnSc7d3
obcí	obec	k1gFnSc7
v	v	k7c6
údolí	údolí	k1gNnSc6
Saastal	Saastal	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgNnPc7d1
středisky	středisko	k1gNnPc7
Saas-Fee	Saas-Fee	k1gNnSc2
<g/>
,	,	kIx,
Saas-Grund	Saas-Grunda	k1gFnPc2
a	a	k8xC
Saas-Almagell	Saas-Almagella	k1gFnPc2
nabízejí	nabízet	k5eAaImIp3nP
145	#num#	k4
km	km	kA
sjezdovek	sjezdovka	k1gFnPc2
začínajících	začínající	k2eAgFnPc2d1
ve	v	k7c6
výšce	výška	k1gFnSc6
3600	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
poprvé	poprvé	k6eAd1
zmiňována	zmiňovat	k5eAaImNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1304	#num#	k4
jako	jako	k8xC,k8xS
Baln	Baln	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
vesnicí	vesnice	k1gFnSc7
Saas-Balen	Saas-Balen	k2eAgInSc1d1
<g/>
,	,	kIx,
osadami	osada	k1gFnPc7
Niedergut	Niedergut	k2eAgInSc1d1
(	(	kIx(
<g/>
Ausser-Balen	Ausser-Balen	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bidermatten	Bidermatten	k2eAgInSc4d1
a	a	k8xC
část	část	k1gFnSc4
osady	osada	k1gFnSc2
Tamatten	Tamatten	k2eAgMnSc1d1
(	(	kIx(
<g/>
Inner-Balen	Inner-Balen	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
obci	obec	k1gFnSc6
366	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
hovořilo	hovořit	k5eAaImAgNnS
98,2	98,2	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnSc1
obce	obec	k1gFnSc2
německy	německy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
římskokatolické	římskokatolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
95,2	95,2	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
ke	k	k7c3
švýcarské	švýcarský	k2eAgFnSc3d1
reformované	reformovaný	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
2,0	2,0	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Součástí	součást	k1gFnSc7
seznamu	seznam	k1gInSc2
švýcarského	švýcarský	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
je	být	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
osada	osada	k1gFnSc1
Bidermatten	Bidermattno	k1gNnPc2
a	a	k8xC
kruhový	kruhový	k2eAgInSc4d1
kostel	kostel	k1gInSc4
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
je	být	k5eAaImIp3nS
také	také	k9
zajímavý	zajímavý	k2eAgInSc1d1
třídílny	třídílny	k?
vodopád	vodopád	k1gInSc1
Fellbach	Fellbacha	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
spodní	spodní	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
z	z	k7c2
obce	obec	k1gFnSc2
a	a	k8xC
k	k	k7c3
horní	horní	k2eAgFnSc3d1
části	část	k1gFnSc3
vede	vést	k5eAaImIp3nS
značená	značený	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Interiér	interiér	k1gInSc1
kostela	kostel	k1gInSc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Vodopád	vodopád	k1gInSc1
Fellbach	Fellbacha	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Saas-Balen	Saas-Balen	k2eAgInSc4d1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
švýcarských	švýcarský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Saas-Balen	Saas-Balen	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Gemeinde	Gemeind	k1gMnSc5
Saas-Balen	Saas-Balen	k2eAgInSc4d1
</s>
<s>
Turistika	turistika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4394345-7	4394345-7	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
235652193	#num#	k4
</s>
