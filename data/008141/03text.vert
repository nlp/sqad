<s>
Joanne	Joannout	k5eAaPmIp3nS	Joannout
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
OBE	Ob	k1gInSc5	Ob
FRSL	FRSL	kA	FRSL
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
Joanne	Joann	k1gInSc5	Joann
Rowling	Rowling	k1gInSc4	Rowling
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1965	[number]	k4	1965
v	v	k7c6	v
Yate	Yate	k1gFnSc6	Yate
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
píšící	píšící	k2eAgFnSc1d1	píšící
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
(	(	kIx(	(
<g/>
J.	J.	kA	J.
K.	K.	kA	K.
Rowling	Rowling	k1gInSc1	Rowling
<g/>
)	)	kIx)	)
či	či	k8xC	či
pseudonymem	pseudonym	k1gInSc7	pseudonym
Robert	Roberta	k1gFnPc2	Roberta
Galbraith	Galbraitha	k1gFnPc2	Galbraitha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
sedmidílné	sedmidílný	k2eAgFnSc3d1	sedmidílná
řadě	řada	k1gFnSc3	řada
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
čarodějnickém	čarodějnický	k2eAgMnSc6d1	čarodějnický
učni	učeň	k1gMnSc6	učeň
Harry	Harra	k1gFnSc2	Harra
Potterovi	Potter	k1gMnSc3	Potter
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
úspěch	úspěch	k1gInSc4	úspěch
včetně	včetně	k7c2	včetně
řady	řada	k1gFnSc2	řada
ocenění	ocenění	k1gNnSc2	ocenění
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
psaní	psaní	k1gNnSc2	psaní
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
známá	známá	k1gFnSc1	známá
také	také	k9	také
svým	svůj	k3xOyFgInSc7	svůj
životním	životní	k2eAgInSc7d1	životní
příběhem	příběh	k1gInSc7	příběh
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
života	život	k1gInSc2	život
na	na	k7c6	na
sociálních	sociální	k2eAgFnPc6d1	sociální
dávkách	dávka	k1gFnPc6	dávka
stala	stát	k5eAaPmAgFnS	stát
během	během	k7c2	během
pěti	pět	k4xCc3	pět
let	léto	k1gNnPc2	léto
multimilionářkou	multimilionářka	k1gFnSc7	multimilionářka
<g/>
.	.	kIx.	.
</s>
<s>
Nedělník	nedělník	k1gInSc1	nedělník
The	The	k1gMnSc2	The
Sunday	Sundaa	k1gMnSc2	Sundaa
Times	Times	k1gMnSc1	Times
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
její	její	k3xOp3gInSc4	její
majetek	majetek	k1gInSc4	majetek
na	na	k7c4	na
560	[number]	k4	560
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
1,1	[number]	k4	1,1
miliard	miliarda	k4xCgFnPc2	miliarda
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
ji	on	k3xPp3gFnSc4	on
za	za	k7c7	za
12	[number]	k4	12
<g/>
.	.	kIx.	.
nejbohatší	bohatý	k2eAgFnSc4d3	nejbohatší
ženu	žena	k1gFnSc4	žena
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Forbes	forbes	k1gInSc1	forbes
ji	on	k3xPp3gFnSc4	on
označil	označit	k5eAaPmAgInS	označit
jako	jako	k8xC	jako
48	[number]	k4	48
<g/>
.	.	kIx.	.
nejvlivnější	vlivný	k2eAgFnSc1d3	nejvlivnější
celebritu	celebrita	k1gFnSc4	celebrita
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
filantropkou	filantropka	k1gFnSc7	filantropka
<g/>
,	,	kIx,	,
podporuje	podporovat	k5eAaImIp3nS	podporovat
organizace	organizace	k1gFnSc1	organizace
jako	jako	k8xS	jako
Comic	Comic	k1gMnSc1	Comic
Relief	Relief	k1gMnSc1	Relief
<g/>
,	,	kIx,	,
One	One	k1gMnSc1	One
Parent	Parent	k1gMnSc1	Parent
Families	Families	k1gMnSc1	Families
a	a	k8xC	a
Multiple	multipl	k1gInSc5	multipl
Sclerosis	Sclerosis	k1gFnPc2	Sclerosis
Society	societa	k1gFnSc2	societa
of	of	k?	of
Great	Great	k2eAgInSc4d1	Great
Britain	Britain	k1gInSc4	Britain
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgNnSc1d1	úřední
jméno	jméno	k1gNnSc1	jméno
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
je	být	k5eAaImIp3nS	být
Joanne	Joann	k1gInSc5	Joann
Rowling	Rowling	k1gInSc4	Rowling
<g/>
(	(	kIx(	(
<g/>
ová	ová	k?	ová
<g/>
)	)	kIx)	)
nikoliv	nikoliv	k9	nikoliv
Joanne	Joann	k1gInSc5	Joann
Kathleen	Kathleen	k1gInSc4	Kathleen
Rowling	Rowling	k1gInSc4	Rowling
<g/>
(	(	kIx(	(
<g/>
ová	ová	k?	ová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
totiž	totiž	k9	totiž
při	při	k7c6	při
vydání	vydání	k1gNnSc3	vydání
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
chtělo	chtít	k5eAaImAgNnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
křestní	křestní	k2eAgNnSc1d1	křestní
jméno	jméno	k1gNnSc1	jméno
autorky	autorka	k1gFnSc2	autorka
na	na	k7c6	na
obálkách	obálka	k1gFnPc6	obálka
objevilo	objevit	k5eAaPmAgNnS	objevit
jen	jen	k6eAd1	jen
jako	jako	k8xS	jako
dvě	dva	k4xCgFnPc4	dva
iniciály	iniciála	k1gFnPc4	iniciála
<g/>
.	.	kIx.	.
</s>
<s>
Obávalo	obávat	k5eAaImAgNnS	obávat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
menšího	malý	k2eAgInSc2d2	menší
prodeje	prodej	k1gInSc2	prodej
úvodního	úvodní	k2eAgInSc2d1	úvodní
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Joanne	Joann	k1gInSc5	Joann
se	se	k3xPyFc4	se
zkrátilo	zkrátit	k5eAaPmAgNnS	zkrátit
na	na	k7c6	na
J.	J.	kA	J.
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgNnSc4	žádný
prostřední	prostřední	k2eAgNnSc4d1	prostřední
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
použila	použít	k5eAaPmAgFnS	použít
jako	jako	k9	jako
druhou	druhý	k4xOgFnSc4	druhý
iniciálu	iniciála	k1gFnSc4	iniciála
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
babičce	babička	k1gFnSc6	babička
Kathleen	Kathleen	k2eAgMnSc1d1	Kathleen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obálkách	obálka	k1gFnPc6	obálka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
objevilo	objevit	k5eAaPmAgNnS	objevit
J.	J.	kA	J.
K.	K.	kA	K.
Rowling	Rowling	k1gInSc1	Rowling
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
křestním	křestní	k2eAgNnSc7d1	křestní
jménem	jméno	k1gNnSc7	jméno
nechávala	nechávat	k5eAaImAgFnS	nechávat
říkat	říkat	k5eAaImF	říkat
"	"	kIx"	"
<g/>
Jo	jo	k9	jo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Joanne	Joannout	k5eAaImIp3nS	Joannout
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Yate	Yate	k1gFnSc6	Yate
<g/>
,	,	kIx,	,
Gloucestershire	Gloucestershir	k1gInSc5	Gloucestershir
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
Dianne	Diann	k1gInSc5	Diann
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Winterbourne	Winterbourn	k1gInSc5	Winterbourn
<g/>
,	,	kIx,	,
Bristolu	Bristol	k1gInSc6	Bristol
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
Tutshillu	Tutshill	k1gInSc2	Tutshill
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
Wyedeanskou	Wyedeanský	k2eAgFnSc4d1	Wyedeanský
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
často	často	k6eAd1	často
svým	svůj	k3xOyFgMnPc3	svůj
přátelům	přítel	k1gMnPc3	přítel
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
45	[number]	k4	45
let	let	k1gInSc1	let
dlouhotrvajícímu	dlouhotrvající	k2eAgInSc3d1	dlouhotrvající
boji	boj	k1gInSc3	boj
s	s	k7c7	s
roztroušenou	roztroušený	k2eAgFnSc7d1	roztroušená
sklerózou	skleróza	k1gFnSc7	skleróza
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
zaměření	zaměření	k1gNnSc1	zaměření
si	se	k3xPyFc3	se
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
rodičů	rodič	k1gMnPc2	rodič
vybrala	vybrat	k5eAaPmAgFnS	vybrat
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
později	pozdě	k6eAd2	pozdě
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
životní	životní	k2eAgFnSc4d1	životní
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ročním	roční	k2eAgInSc6d1	roční
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
jejích	její	k3xOp3gFnPc2	její
vysokoškolských	vysokoškolský	k2eAgFnPc2d1	vysokoškolská
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracovala	pracovat	k5eAaImAgFnS	pracovat
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnSc2	International
<g/>
.	.	kIx.	.
</s>
<s>
Nápad	nápad	k1gInSc1	nápad
na	na	k7c4	na
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
kouzelnickém	kouzelnický	k2eAgMnSc6d1	kouzelnický
učni	učeň	k1gMnSc6	učeň
dostala	dostat	k5eAaPmAgFnS	dostat
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
vlakem	vlak	k1gInSc7	vlak
z	z	k7c2	z
Manchesteru	Manchester	k1gInSc2	Manchester
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cílové	cílový	k2eAgFnSc6d1	cílová
zastávce	zastávka	k1gFnSc6	zastávka
měla	mít	k5eAaImAgFnS	mít
již	již	k6eAd1	již
vymyšlenou	vymyšlený	k2eAgFnSc4d1	vymyšlená
většinu	většina	k1gFnSc4	většina
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
základní	základní	k2eAgInSc1d1	základní
děj	děj	k1gInSc1	děj
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
série	série	k1gFnSc2	série
–	–	k?	–
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c2	za
portugalského	portugalský	k2eAgMnSc2d1	portugalský
televizního	televizní	k2eAgMnSc2d1	televizní
reportéra	reportér	k1gMnSc2	reportér
Jorga	Jorga	k1gFnSc1	Jorga
Arantese	Arantese	k1gFnSc1	Arantese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Jessica	Jessic	k1gInSc2	Jessic
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
Arantesová	Arantesový	k2eAgFnSc1d1	Arantesový
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
ale	ale	k9	ale
nevydrželo	vydržet	k5eNaPmAgNnS	vydržet
a	a	k8xC	a
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
před	před	k7c7	před
rozvodem	rozvod	k1gInSc7	rozvod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vrátila	vrátit	k5eAaPmAgFnS	vrátit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
do	do	k7c2	do
Edinburgu	Edinburg	k1gInSc2	Edinburg
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnaná	zaměstnaný	k2eNgFnSc1d1	nezaměstnaná
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
dokončila	dokončit	k5eAaPmAgFnS	dokončit
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
skotském	skotský	k2eAgNnSc6d1	skotské
městě	město	k1gNnSc6	město
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
dílu	díl	k1gInSc6	díl
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgFnSc2	některý
části	část	k1gFnSc2	část
psala	psát	k5eAaImAgFnS	psát
po	po	k7c6	po
edinburských	edinburský	k2eAgFnPc6d1	edinburská
kavárnách	kavárna	k1gFnPc6	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Koluje	kolovat	k5eAaImIp3nS	kolovat
pověst	pověst	k1gFnSc4	pověst
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
kavárnách	kavárna	k1gFnPc6	kavárna
psala	psát	k5eAaImAgFnS	psát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
svému	svůj	k3xOyFgInSc3	svůj
nevytápěnému	vytápěný	k2eNgInSc3d1	nevytápěný
bytu	byt	k1gInSc3	byt
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgMnSc4	sám
to	ten	k3xDgNnSc1	ten
však	však	k9	však
popřela	popřít	k5eAaPmAgFnS	popřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obrovském	obrovský	k2eAgInSc6d1	obrovský
úspěchu	úspěch	k1gInSc6	úspěch
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
napsala	napsat	k5eAaBmAgFnS	napsat
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
6	[number]	k4	6
dalších	další	k2eAgNnPc2d1	další
pokračování	pokračování	k1gNnPc2	pokračování
a	a	k8xC	a
3	[number]	k4	3
doplňkové	doplňkový	k2eAgFnSc2d1	doplňková
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
nadace	nadace	k1gFnSc2	nadace
Comic	Comice	k1gFnPc2	Comice
Relief	Relief	k1gMnSc1	Relief
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
knihy	kniha	k1gFnSc2	kniha
udělal	udělat	k5eAaPmAgInS	udělat
z	z	k7c2	z
Rowlingové	Rowlingový	k2eAgFnPc4d1	Rowlingová
multimilionářku	multimilionářka	k1gFnSc4	multimilionářka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
si	se	k3xPyFc3	se
tak	tak	k9	tak
mohla	moct	k5eAaImAgFnS	moct
koupit	koupit	k5eAaPmF	koupit
luxusní	luxusní	k2eAgNnSc4d1	luxusní
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
u	u	k7c2	u
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
Tay	Tay	k1gFnPc2	Tay
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Neila	Neil	k1gMnSc2	Neil
Murraye	Murray	k1gMnSc2	Murray
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
o	o	k7c4	o
Harry	Harra	k1gFnPc4	Harra
Potterovi	Potter	k1gMnSc3	Potter
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
7	[number]	k4	7
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Překladatelem	překladatel	k1gMnSc7	překladatel
většiny	většina	k1gFnSc2	většina
částí	část	k1gFnPc2	část
je	být	k5eAaImIp3nS	být
Pavel	Pavel	k1gMnSc1	Pavel
Medek	Medek	k1gMnSc1	Medek
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
světových	světový	k2eAgInPc2d1	světový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
ČR	ČR	kA	ČR
na	na	k7c6	na
tuto	tento	k3xDgFnSc4	tento
sérii	série	k1gFnSc6	série
exkluzivní	exkluzivní	k2eAgNnPc1d1	exkluzivní
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
český	český	k2eAgInSc4d1	český
překlad	překlad	k1gInSc4	překlad
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
snad	snad	k9	snad
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gMnSc3	on
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
nevyhnula	vyhnout	k5eNaPmAgFnS	vyhnout
ani	ani	k8xC	ani
řada	řada	k1gFnSc1	řada
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
pátého	pátý	k4xOgInSc2	pátý
dílu	díl	k1gInSc2	díl
(	(	kIx(	(
<g/>
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
kvůli	kvůli	k7c3	kvůli
nařčení	nařčení	k1gNnSc3	nařčení
z	z	k7c2	z
plagiátorství	plagiátorství	k1gNnSc2	plagiátorství
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
Nancy	Nancy	k1gFnSc7	Nancy
Stoufferovou	Stoufferová	k1gFnSc7	Stoufferová
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Joanne	Joann	k1gInSc5	Joann
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
si	se	k3xPyFc3	se
také	také	k9	také
chtěla	chtít	k5eAaImAgFnS	chtít
od	od	k7c2	od
psaní	psaní	k1gNnSc2	psaní
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
odpočinout	odpočinout	k5eAaPmF	odpočinout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
během	během	k7c2	během
psaní	psaní	k1gNnSc3	psaní
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
dílu	díl	k1gInSc2	díl
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgFnS	cítit
přepracovaná	přepracovaný	k2eAgFnSc1d1	přepracovaná
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
donutila	donutit	k5eAaPmAgFnS	donutit
nakladavatelství	nakladavatelství	k1gNnSc4	nakladavatelství
posunout	posunout	k5eAaPmF	posunout
datum	datum	k1gNnSc4	datum
publikace	publikace	k1gFnSc2	publikace
a	a	k8xC	a
na	na	k7c4	na
pátý	pátý	k4xOgInSc4	pátý
díl	díl	k1gInSc4	díl
celé	celý	k2eAgFnSc2d1	celá
série	série	k1gFnSc2	série
tak	tak	k6eAd1	tak
měla	mít	k5eAaImAgFnS	mít
celé	celý	k2eAgInPc4d1	celý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
klidného	klidný	k2eAgNnSc2d1	klidné
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
tohoto	tento	k3xDgInSc2	tento
času	čas	k1gInSc2	čas
strávila	strávit	k5eAaPmAgFnS	strávit
i	i	k9	i
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
jiných	jiný	k2eAgFnPc2d1	jiná
knih	kniha	k1gFnPc2	kniha
než	než	k8xS	než
pokračování	pokračování	k1gNnSc1	pokračování
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
dostala	dostat	k5eAaPmAgFnS	dostat
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
nabídku	nabídka	k1gFnSc4	nabídka
televizního	televizní	k2eAgMnSc2d1	televizní
producenta	producent	k1gMnSc2	producent
Russella	Russello	k1gNnSc2	Russello
T.	T.	kA	T.
Daviese	Daviese	k1gFnSc2	Daviese
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
slavného	slavný	k2eAgNnSc2d1	slavné
britského	britský	k2eAgNnSc2d1	Britské
sci-fi	scii	k1gNnSc2	sci-fi
seriálu	seriál	k1gInSc2	seriál
Doctor	Doctor	k1gMnSc1	Doctor
Who	Who	k1gMnSc1	Who
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
nabídkou	nabídka	k1gFnSc7	nabídka
nadšená	nadšený	k2eAgFnSc1d1	nadšená
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
ji	on	k3xPp3gFnSc4	on
nakonec	nakonec	k6eAd1	nakonec
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k6eAd1	už
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
dílu	díl	k1gInSc6	díl
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nese	nést	k5eAaImIp3nS	nést
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jméno	jméno	k1gNnSc4	jméno
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Half-blood	Halflood	k1gInSc1	Half-blood
Prince	princ	k1gMnSc2	princ
(	(	kIx(	(
<g/>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
Vánoci	Vánoce	k1gFnPc7	Vánoce
2004	[number]	k4	2004
oznámila	oznámit	k5eAaPmAgFnS	oznámit
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
dokončení	dokončení	k1gNnSc4	dokončení
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
vydání	vydání	k1gNnSc2	vydání
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
nelegálně	legálně	k6eNd1	legálně
se	se	k3xPyFc4	se
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
prodávat	prodávat	k5eAaImF	prodávat
už	už	k6eAd1	už
dřív	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
se	se	k3xPyFc4	se
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
literárních	literární	k2eAgFnPc2d1	literární
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgFnPc2d1	hudební
či	či	k8xC	či
filmových	filmový	k2eAgFnPc2d1	filmová
osobností	osobnost	k1gFnPc2	osobnost
-	-	kIx~	-
krátce	krátce	k6eAd1	krátce
objevila	objevit	k5eAaPmAgFnS	objevit
jako	jako	k9	jako
postavička	postavička	k1gFnSc1	postavička
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
kresleném	kreslený	k2eAgInSc6d1	kreslený
sitcomu	sitcom	k1gInSc6	sitcom
Simpsonovi	Simpson	k1gMnSc3	Simpson
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Philosopher	Philosophra	k1gFnPc2	Philosophra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Stone	ston	k1gInSc5	ston
Harry	Harr	k1gInPc1	Harr
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Chamber	Chamber	k1gInSc1	Chamber
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xC	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
the	the	k?	the
Prisoner	Prisoner	k1gMnSc1	Prisoner
of	of	k?	of
Azkaban	Azkaban	k1gMnSc1	Azkaban
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xC	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Goblet	Goblet	k1gInSc1	Goblet
of	of	k?	of
Fire	Fir	k1gMnSc2	Fir
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
the	the	k?	the
Phoenix	Phoenix	k1gInSc1	Phoenix
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k9	jako
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
and	and	k?	and
the	the	k?	the
Half-Blood	Half-Blood	k1gInSc1	Half-Blood
Prince	princ	k1gMnSc2	princ
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Deathly	Deathly	k1gFnSc2	Deathly
Hallows	Hallowsa	k1gFnPc2	Hallowsa
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
prokleté	prokletý	k2eAgNnSc1d1	prokleté
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
Cursed	Cursed	k1gMnSc1	Cursed
Child	Child	k1gMnSc1	Child
(	(	kIx(	(
<g/>
divadelní	divadelní	k2eAgInSc1d1	divadelní
scénář	scénář	k1gInSc1	scénář
<g/>
)	)	kIx)	)
Fantastická	fantastický	k2eAgNnPc1d1	fantastické
zvířata	zvíře	k1gNnPc1	zvíře
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
původní	původní	k2eAgInSc1d1	původní
scénář	scénář	k1gInSc1	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Famfrpál	Famfrpál	k1gInSc1	Famfrpál
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
věků	věk	k1gInPc2	věk
Fantastická	fantastický	k2eAgNnPc4d1	fantastické
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
Bajky	bajka	k1gFnPc4	bajka
barda	bard	k1gMnSc2	bard
Beedleho	Beedle	k1gMnSc2	Beedle
První	první	k4xOgMnPc1	první
dvě	dva	k4xCgFnPc4	dva
doplňkové	doplňkový	k2eAgFnPc4d1	doplňková
knihy	kniha	k1gFnPc4	kniha
napsala	napsat	k5eAaPmAgFnS	napsat
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
pod	pod	k7c4	pod
pseudonymy	pseudonym	k1gInPc4	pseudonym
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
skutečné	skutečný	k2eAgFnPc4d1	skutečná
verze	verze	k1gFnPc4	verze
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zmíněny	zmínit	k5eAaPmNgInP	zmínit
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
příběhu	příběh	k1gInSc6	příběh
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
<g/>
.	.	kIx.	.
</s>
<s>
Fantastická	fantastický	k2eAgNnPc4d1	fantastické
zvířata	zvíře	k1gNnPc4	zvíře
je	být	k5eAaImIp3nS	být
učebnice	učebnice	k1gFnSc1	učebnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
abecedním	abecední	k2eAgNnSc6d1	abecední
pořadí	pořadí	k1gNnSc6	pořadí
uvedeni	uveden	k2eAgMnPc1d1	uveden
různí	různit	k5eAaImIp3nP	různit
fantastičtí	fantastický	k2eAgMnPc1d1	fantastický
tvorové	tvor	k1gMnPc1	tvor
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Famfrpál	Famfrpál	k1gInSc1	Famfrpál
je	být	k5eAaImIp3nS	být
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
knihou	kniha	k1gFnSc7	kniha
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
bradavické	bradavický	k2eAgFnSc6d1	bradavická
školní	školní	k2eAgFnSc6d1	školní
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Vytištěné	vytištěný	k2eAgFnPc1d1	vytištěná
jsou	být	k5eAaImIp3nP	být
včetně	včetně	k7c2	včetně
poznámek	poznámka	k1gFnPc2	poznámka
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
listů	list	k1gInPc2	list
a	a	k8xC	a
instrukcí	instrukce	k1gFnPc2	instrukce
od	od	k7c2	od
Albuse	Albuse	k1gFnSc2	Albuse
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bajkách	bajka	k1gFnPc6	bajka
barda	bard	k1gMnSc2	bard
Beedleho	Beedle	k1gMnSc2	Beedle
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
přečíst	přečíst	k5eAaPmF	přečíst
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
čarodějové	čaroděj	k1gMnPc1	čaroděj
čtou	číst	k5eAaImIp3nP	číst
svým	svůj	k3xOyFgFnPc3	svůj
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Veškerý	veškerý	k3xTgInSc1	veškerý
výtěžek	výtěžek	k1gInSc1	výtěžek
jde	jít	k5eAaImIp3nS	jít
na	na	k7c4	na
konto	konto	k1gNnSc4	konto
nejznámější	známý	k2eAgFnSc2d3	nejznámější
britské	britský	k2eAgFnSc2d1	britská
charitativní	charitativní	k2eAgFnSc2d1	charitativní
nadace	nadace	k1gFnSc2	nadace
-	-	kIx~	-
Comic	Comic	k1gMnSc1	Comic
Relief	Relief	k1gMnSc1	Relief
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
obdarovala	obdarovat	k5eAaPmAgFnS	obdarovat
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgFnPc2d1	různá
charitativních	charitativní	k2eAgFnPc2d1	charitativní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
však	však	k9	však
přispívá	přispívat	k5eAaImIp3nS	přispívat
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
léčení	léčení	k1gNnSc4	léčení
roztroušené	roztroušený	k2eAgFnSc2d1	roztroušená
sklerózy	skleróza	k1gFnSc2	skleróza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
spisovatelčina	spisovatelčin	k2eAgFnSc1d1	spisovatelčina
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Rowlingové	Rowlingový	k2eAgFnSc2d1	Rowlingová
matčina	matčin	k2eAgFnSc1d1	matčina
smrt	smrt	k1gFnSc4	smrt
hluboce	hluboko	k6eAd1	hluboko
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
její	její	k3xOp3gInSc4	její
autorský	autorský	k2eAgInSc4d1	autorský
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
verze	verze	k1gFnSc1	verze
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Kámen	kámen	k1gInSc4	kámen
mudrců	mudrc	k1gMnPc2	mudrc
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
přišel	přijít	k5eAaPmAgInS	přijít
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
i	i	k8xC	i
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
diváckým	divácký	k2eAgInSc7d1	divácký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
kritika	kritika	k1gFnSc1	kritika
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
vytýká	vytýkat	k5eAaImIp3nS	vytýkat
řadu	řada	k1gFnSc4	řada
nedostatků	nedostatek	k1gInPc2	nedostatek
a	a	k8xC	a
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nebyly	být	k5eNaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
ani	ani	k8xC	ani
fanoušky	fanoušek	k1gMnPc7	fanoušek
knižní	knižní	k2eAgFnSc2d1	knižní
předlohy	předloha	k1gFnSc2	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
režiséra	režisér	k1gMnSc2	režisér
přišla	přijít	k5eAaPmAgFnS	přijít
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
verzi	verze	k1gFnSc6	verze
třetího	třetí	k4xOgInSc2	třetí
dílu	díl	k1gInSc2	díl
mnohem	mnohem	k6eAd1	mnohem
temnější	temný	k2eAgFnSc1d2	temnější
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
a	a	k8xC	a
vězně	vězeň	k1gMnSc4	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
(	(	kIx(	(
<g/>
uveden	uveden	k2eAgInSc1d1	uveden
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
režíroval	režírovat	k5eAaImAgMnS	režírovat
Alfonso	Alfonso	k1gMnSc1	Alfonso
Cuarón	Cuarón	k1gMnSc1	Cuarón
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
práci	práce	k1gFnSc4	práce
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
obdivovala	obdivovat	k5eAaImAgFnS	obdivovat
již	již	k6eAd1	již
před	před	k7c7	před
natáčením	natáčení	k1gNnSc7	natáčení
Harryho	Harry	k1gMnSc2	Harry
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
označila	označit	k5eAaPmAgFnS	označit
adaptaci	adaptace	k1gFnSc4	adaptace
třetího	třetí	k4xOgInSc2	třetí
dílu	díl	k1gInSc2	díl
jako	jako	k8xC	jako
svoji	svůj	k3xOyFgFnSc4	svůj
nejoblíbenější	oblíbený	k2eAgFnSc4d3	nejoblíbenější
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
díl	díl	k1gInSc4	díl
(	(	kIx(	(
<g/>
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vypuštěn	vypuštěn	k2eAgMnSc1d1	vypuštěn
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
ČR	ČR	kA	ČR
0	[number]	k4	0
<g/>
1.12	[number]	k4	1.12
<g/>
.2005	.2005	k4	.2005
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgInSc1	pátý
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
<g/>
)	)	kIx)	)
19.07	[number]	k4	19.07
<g/>
.2007	.2007	k4	.2007
<g/>
,	,	kIx,	,
šestý	šestý	k4xOgInSc1	šestý
<g/>
,	,	kIx,	,
předposlední	předposlední	k2eAgInSc1d1	předposlední
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
16.07	[number]	k4	16.07
<g/>
.2009	.2009	k4	.2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
Harry	Harro	k1gNnPc7	Harro
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
filmovém	filmový	k2eAgInSc6d1	filmový
zpracování	zpracování	k1gNnSc2	zpracování
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
dílů	díl	k1gInPc2	díl
režírovanách	režírovana	k1gFnPc6	režírovana
Davidem	David	k1gMnSc7	David
Yatesem	Yates	k1gMnSc7	Yates
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
18.11	[number]	k4	18.11
<g/>
.2010	.2010	k4	.2010
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
14.07	[number]	k4	14.07
<g/>
.2011	.2011	k4	.2011
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
návrhy	návrh	k1gInPc7	návrh
filmařů	filmař	k1gMnPc2	filmař
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
filmy	film	k1gInPc1	film
natáčeny	natáčen	k2eAgInPc1d1	natáčen
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
nebo	nebo	k8xC	nebo
s	s	k7c7	s
americkými	americký	k2eAgMnPc7d1	americký
herci	herec	k1gMnPc7	herec
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
neochotně	ochotně	k6eNd1	ochotně
dovolila	dovolit	k5eAaPmAgFnS	dovolit
vydavatelům	vydavatel	k1gMnPc3	vydavatel
změnit	změnit	k5eAaPmF	změnit
název	název	k1gInSc4	název
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
na	na	k7c4	na
"	"	kIx"	"
<g/>
američtější	americký	k2eAgInSc4d2	američtější
<g/>
"	"	kIx"	"
Sorcerer	Sorcerer	k1gInSc4	Sorcerer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ještě	ještě	k6eAd1	ještě
omezila	omezit	k5eAaPmAgFnS	omezit
jen	jen	k9	jen
na	na	k7c6	na
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
na	na	k7c6	na
britských	britský	k2eAgInPc6d1	britský
hercích	herc	k1gInPc6	herc
trvala	trvalo	k1gNnSc2	trvalo
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
sérii	série	k1gFnSc4	série
nakonec	nakonec	k6eAd1	nakonec
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
režírovat	režírovat	k5eAaImF	režírovat
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
scénářů	scénář	k1gInPc2	scénář
je	být	k5eAaImIp3nS	být
Steven	Steven	k2eAgInSc1d1	Steven
Kloves	Kloves	k1gInSc1	Kloves
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
asistuje	asistovat	k5eAaImIp3nS	asistovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ujistila	ujistit	k5eAaPmAgFnS	ujistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
děj	děj	k1gInSc1	děj
filmů	film	k1gInPc2	film
nebude	být	k5eNaImBp3nS	být
odporovat	odporovat	k5eAaImF	odporovat
budoucímu	budoucí	k2eAgInSc3d1	budoucí
ději	děj	k1gInSc3	děj
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
prozradila	prozradit	k5eAaPmAgFnS	prozradit
Klovesovi	Klovesův	k2eAgMnPc1d1	Klovesův
o	o	k7c6	o
zbývajících	zbývající	k2eAgInPc6d1	zbývající
dílech	díl	k1gInPc6	díl
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
komukoliv	kdokoliv	k3yInSc3	kdokoliv
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
prý	prý	k9	prý
řekla	říct	k5eAaPmAgFnS	říct
několik	několik	k4yIc4	několik
detailů	detail	k1gInPc2	detail
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
jejich	jejich	k3xOp3gFnPc2	jejich
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
Alanu	Alan	k1gMnSc6	Alan
Rickmanovi	Rickman	k1gMnSc6	Rickman
a	a	k8xC	a
Robiemu	Robiem	k1gMnSc6	Robiem
Coltraneovi	Coltraneus	k1gMnSc6	Coltraneus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
filmové	filmový	k2eAgFnSc2d1	filmová
ságy	sága	k1gFnSc2	sága
Fantastická	fantastický	k2eAgNnPc1d1	fantastické
zvířata	zvíře	k1gNnPc1	zvíře
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ujal	ujmout	k5eAaPmAgMnS	ujmout
David	David	k1gMnSc1	David
Yates	Yates	k1gMnSc1	Yates
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
je	být	k5eAaImIp3nS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
příprava	příprava	k1gFnSc1	příprava
dalších	další	k2eAgNnPc2d1	další
dvou	dva	k4xCgNnPc2	dva
pokračování	pokračování	k1gNnPc2	pokračování
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
autorka	autorka	k1gFnSc1	autorka
by	by	kYmCp3nS	by
ráda	rád	k2eAgFnSc1d1	ráda
uvedla	uvést	k5eAaPmAgFnS	uvést
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
série	série	k1gFnSc2	série
o	o	k7c6	o
Potterovi	Potter	k1gMnSc6	Potter
napsala	napsat	k5eAaBmAgFnS	napsat
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
i	i	k8xC	i
další	další	k2eAgNnPc1d1	další
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
kouzelnickým	kouzelnický	k2eAgInSc7d1	kouzelnický
světem	svět	k1gInSc7	svět
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
zasazeny	zasadit	k5eAaPmNgFnP	zasadit
do	do	k7c2	do
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
anglické	anglický	k2eAgFnPc1d1	anglická
reálie	reálie	k1gFnPc1	reálie
<g/>
,	,	kIx,	,
existující	existující	k2eAgFnPc1d1	existující
osoby	osoba	k1gFnPc1	osoba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Gordon	Gordon	k1gMnSc1	Gordon
Brown	Brown	k1gMnSc1	Brown
je	být	k5eAaImIp3nS	být
zmiňován	zmiňovat	k5eAaImNgMnS	zmiňovat
ve	v	k7c6	v
Volání	volání	k1gNnSc6	volání
kukačky	kukačka	k1gFnSc2	kukačka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Prázdné	prázdný	k2eAgNnSc1d1	prázdné
místo	místo	k1gNnSc1	místo
(	(	kIx(	(
<g/>
The	The	k1gFnPc1	The
Casual	Casual	k1gInSc4	Casual
Vacancy	Vacanca	k1gFnSc2	Vacanca
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Knihou	kniha	k1gFnSc7	kniha
Volání	volání	k1gNnSc2	volání
kukačky	kukačka	k1gFnSc2	kukačka
začala	začít	k5eAaPmAgFnS	začít
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
psát	psát	k5eAaImF	psát
sérii	série	k1gFnSc4	série
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
soukromém	soukromý	k2eAgInSc6d1	soukromý
detektivovi	detektiv	k1gMnSc3	detektiv
Cormoranu	Cormoran	k1gInSc2	Cormoran
Strikeovi	Strikeus	k1gMnSc3	Strikeus
<g/>
.	.	kIx.	.
</s>
<s>
Volání	volání	k1gNnSc1	volání
kukačky	kukačka	k1gFnSc2	kukačka
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Cuckoo	Cuckoo	k1gMnSc1	Cuckoo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Calling	Calling	k1gInSc1	Calling
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Hedvábník	hedvábník	k1gInSc1	hedvábník
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Silkworm	Silkworm	k1gInSc1	Silkworm
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
zla	zlo	k1gNnSc2	zlo
(	(	kIx(	(
<g/>
Career	Career	k1gInSc1	Career
of	of	k?	of
Evil	Evil	k1gInSc1	Evil
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
vedla	vést	k5eAaImAgFnS	vést
kvůli	kvůli	k7c3	kvůli
Harrymu	Harrymum	k1gNnSc3	Harrymum
Potterovi	Potter	k1gMnSc3	Potter
několik	několik	k4yIc4	několik
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
Nancy	Nancy	k1gFnSc1	Nancy
Stoufferová	Stoufferová	k1gFnSc1	Stoufferová
veřejně	veřejně	k6eAd1	veřejně
prohlašovat	prohlašovat	k5eAaImF	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
série	série	k1gFnSc1	série
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potter	k1gMnSc3	Potter
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgNnPc4d1	postaveno
na	na	k7c6	na
jejích	její	k3xOp3gFnPc6	její
knihách	kniha	k1gFnPc6	kniha
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
The	The	k1gMnSc1	The
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
Rah	Rah	k1gMnSc1	Rah
and	and	k?	and
the	the	k?	the
Muggles	Mugglesa	k1gFnPc2	Mugglesa
a	a	k8xC	a
Larry	Larra	k1gFnSc2	Larra
Potter	Pottra	k1gFnPc2	Pottra
and	and	k?	and
His	his	k1gNnSc1	his
Best	Best	k1gMnSc1	Best
Friend	Friend	k1gMnSc1	Friend
Lilly	Lilla	k1gFnSc2	Lilla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
<g/>
,	,	kIx,	,
Scholastic	Scholastice	k1gFnPc2	Scholastice
Press	Press	k1gInSc1	Press
(	(	kIx(	(
<g/>
americký	americký	k2eAgMnSc1d1	americký
vydavatel	vydavatel	k1gMnSc1	vydavatel
série	série	k1gFnSc2	série
<g/>
)	)	kIx)	)
a	a	k8xC	a
Warner	Warner	k1gMnSc1	Warner
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
přenechali	přenechat	k5eAaPmAgMnP	přenechat
zjišťování	zjišťování	k1gNnSc3	zjišťování
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
porušení	porušení	k1gNnSc3	porušení
copyrightu	copyright	k1gInSc2	copyright
či	či	k8xC	či
registrovaných	registrovaný	k2eAgFnPc2d1	registrovaná
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
Strofferové	Strofferové	k2eAgFnPc2d1	Strofferové
<g/>
,	,	kIx,	,
na	na	k7c6	na
soudu	soud	k1gInSc6	soud
<g/>
.	.	kIx.	.
</s>
<s>
Právníci	právník	k1gMnPc1	právník
Rowlingové	Rowlingový	k2eAgFnSc2d1	Rowlingová
označili	označit	k5eAaPmAgMnP	označit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
tvrzení	tvrzení	k1gNnSc2	tvrzení
Stroufferové	Stroufferové	k2eAgNnSc2d1	Stroufferové
za	za	k7c4	za
podvrh	podvrh	k1gInSc4	podvrh
a	a	k8xC	a
požadovali	požadovat	k5eAaImAgMnP	požadovat
uvalení	uvalení	k1gNnSc4	uvalení
sankcí	sankce	k1gFnPc2	sankce
na	na	k7c4	na
Stroufferovou	Stroufferová	k1gFnSc4	Stroufferová
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zaplacením	zaplacení	k1gNnSc7	zaplacení
svých	svůj	k3xOyFgInPc2	svůj
soudních	soudní	k2eAgInPc2d1	soudní
výdajů	výdaj	k1gInPc2	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Rowlingové	Rowlingový	k2eAgInPc4d1	Rowlingový
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stroufferová	Stroufferová	k1gFnSc1	Stroufferová
vypovídala	vypovídat	k5eAaImAgFnS	vypovídat
lživě	lživě	k6eAd1	lživě
a	a	k8xC	a
zfalšovala	zfalšovat	k5eAaPmAgFnS	zfalšovat
podklady	podklad	k1gInPc4	podklad
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podporovaly	podporovat	k5eAaImAgFnP	podporovat
její	její	k3xOp3gNnSc4	její
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Stroufferová	Stroufferová	k1gFnSc1	Stroufferová
pak	pak	k6eAd1	pak
dostala	dostat	k5eAaPmAgFnS	dostat
pokutu	pokuta	k1gFnSc4	pokuta
50	[number]	k4	50
000	[number]	k4	000
USD	USD	kA	USD
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
i	i	k9	i
část	část	k1gFnSc4	část
soudních	soudní	k2eAgInPc2d1	soudní
výdajů	výdaj	k1gInPc2	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
soud	soud	k1gInSc1	soud
sdělil	sdělit	k5eAaPmAgInS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
odvolání	odvolání	k1gNnSc4	odvolání
Stroufferové	Stroufferová	k1gFnSc2	Stroufferová
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
soudu	soud	k1gInSc2	soud
<g/>
)	)	kIx)	)
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
záměně	záměna	k1gFnSc3	záměna
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
znaků	znak	k1gInPc2	znak
obou	dva	k4xCgInPc2	dva
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
autorky	autorka	k1gFnPc1	autorka
používají	používat	k5eAaImIp3nP	používat
sporné	sporný	k2eAgInPc1d1	sporný
termíny	termín	k1gInPc1	termín
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
významem	význam	k1gInSc7	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dílech	dílo	k1gNnPc6	dílo
Rowlingové	Rowlingový	k2eAgNnSc1d1	Rowlingové
znamená	znamenat	k5eAaImIp3nS	znamenat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Mudla	Mudla	k1gFnSc1	Mudla
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
"	"	kIx"	"
<g/>
Muggle	Muggle	k1gFnSc1	Muggle
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
běžného	běžný	k2eAgMnSc4d1	běžný
člověka	člověk	k1gMnSc4	člověk
bez	bez	k7c2	bez
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Stroufferová	Stroufferová	k1gFnSc1	Stroufferová
tímto	tento	k3xDgNnSc7	tento
slovem	slovem	k6eAd1	slovem
označuje	označovat	k5eAaImIp3nS	označovat
malé	malý	k2eAgFnPc4d1	malá
<g/>
,	,	kIx,	,
bezvlasé	bezvlasý	k2eAgFnPc4d1	bezvlasá
postavičky	postavička	k1gFnPc4	postavička
s	s	k7c7	s
našpičatělými	našpičatělý	k2eAgFnPc7d1	našpičatělý
hlavami	hlava	k1gFnPc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc4	kniha
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
mají	mít	k5eAaImIp3nP	mít
rozsah	rozsah	k1gInSc4	rozsah
románu	román	k1gInSc2	román
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
cílovou	cílový	k2eAgFnSc4d1	cílová
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
zejména	zejména	k9	zejména
starší	starý	k2eAgFnPc1d2	starší
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Stroufferová	Stroufferová	k1gFnSc1	Stroufferová
tvořila	tvořit	k5eAaImAgFnS	tvořit
krátké	krátký	k2eAgFnPc4d1	krátká
knížky	knížka	k1gFnPc4	knížka
určené	určený	k2eAgFnPc4d1	určená
nejmenším	malý	k2eAgFnPc3d3	nejmenší
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgMnSc4	ten
předchozí	předchozí	k2eAgInSc1d1	předchozí
soud	soud	k1gInSc1	soud
správně	správně	k6eAd1	správně
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
požadavky	požadavek	k1gInPc4	požadavek
Stroufferové	Stroufferová	k1gFnSc2	Stroufferová
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
oznámila	oznámit	k5eAaPmAgFnS	oznámit
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
vydavatelem	vydavatel	k1gMnSc7	vydavatel
Scholastic	Scholastice	k1gFnPc2	Scholastice
<g/>
,	,	kIx,	,
že	že	k8xS	že
podávají	podávat	k5eAaImIp3nP	podávat
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
Daily	Daila	k1gFnSc2	Daila
News	News	k1gInSc4	News
o	o	k7c4	o
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tyto	tento	k3xDgFnPc1	tento
noviny	novina	k1gFnPc1	novina
otiskly	otisknout	k5eAaPmAgFnP	otisknout
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
pátém	pátý	k4xOgInSc6	pátý
dílu	díl	k1gInSc6	díl
Harry	Harra	k1gFnPc4	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc4d1	fénixův
řád	řád	k1gInSc4	řád
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
vydáním	vydání	k1gNnSc7	vydání
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
deník	deník	k1gInSc1	deník
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
souhrn	souhrn	k1gInSc4	souhrn
děje	děj	k1gInPc4	děj
a	a	k8xC	a
několik	několik	k4yIc4	několik
krátkých	krátký	k2eAgFnPc2d1	krátká
citací	citace	k1gFnPc2	citace
<g/>
.	.	kIx.	.
</s>
<s>
Doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
fotografie	fotografie	k1gFnSc1	fotografie
zobrazovala	zobrazovat	k5eAaImAgFnS	zobrazovat
dvě	dva	k4xCgFnPc4	dva
stránky	stránka	k1gFnPc4	stránka
knihy	kniha	k1gFnSc2	kniha
s	s	k7c7	s
čitelným	čitelný	k2eAgInSc7d1	čitelný
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
novinář	novinář	k1gMnSc1	novinář
deníku	deník	k1gInSc2	deník
knihu	kniha	k1gFnSc4	kniha
koupil	koupit	k5eAaPmAgMnS	koupit
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
prodavač	prodavač	k1gMnSc1	prodavač
si	se	k3xPyFc3	se
údajně	údajně	k6eAd1	údajně
neuvědomil	uvědomit	k5eNaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
knihu	kniha	k1gFnSc4	kniha
může	moct	k5eAaImIp3nS	moct
začít	začít	k5eAaPmF	začít
prodávat	prodávat	k5eAaImF	prodávat
až	až	k9	až
po	po	k7c6	po
datu	datum	k1gNnSc6	datum
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
za	za	k7c4	za
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Neila	Neil	k1gMnSc2	Neil
Murraye	Murray	k1gMnSc2	Murray
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1	soukromý
obřad	obřad	k1gInSc1	obřad
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
ve	v	k7c6	v
spisovatelčině	spisovatelčin	k2eAgNnSc6d1	spisovatelčino
sídle	sídlo	k1gNnSc6	sídlo
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
David	David	k1gMnSc1	David
Gordon	Gordon	k1gMnSc1	Gordon
Rowling	Rowling	k1gInSc4	Rowling
Murray	Murraa	k1gFnSc2	Murraa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
dcera	dcera	k1gFnSc1	dcera
Mackenzie	Mackenzie	k1gFnSc2	Mackenzie
Jean	Jean	k1gMnSc1	Jean
Rowling	Rowling	k1gInSc4	Rowling
Murray	Murraa	k1gFnSc2	Murraa
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
se	se	k3xPyFc4	se
tak	tak	k9	tak
splnil	splnit	k5eAaPmAgMnS	splnit
životní	životní	k2eAgInSc4d1	životní
sen	sen	k1gInSc4	sen
mít	mít	k5eAaImF	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Joanne	Joann	k1gInSc5	Joann
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Joanne	Joann	k1gInSc5	Joann
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgFnPc6	který
často	často	k6eAd1	často
vyvrací	vyvracet	k5eAaImIp3nP	vyvracet
domněnky	domněnka	k1gFnPc4	domněnka
a	a	k8xC	a
nepravdivé	pravdivý	k2eNgFnPc4d1	nepravdivá
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
knihách	kniha	k1gFnPc6	kniha
i	i	k8xC	i
svém	svůj	k3xOyFgInSc6	svůj
soukromém	soukromý	k2eAgInSc6d1	soukromý
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
často	často	k6eAd1	často
uveřejňuje	uveřejňovat	k5eAaImIp3nS	uveřejňovat
dosud	dosud	k6eAd1	dosud
neznámé	známý	k2eNgNnSc1d1	neznámé
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
okrajové	okrajový	k2eAgFnPc4d1	okrajová
<g/>
)	)	kIx)	)
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
budoucím	budoucí	k2eAgInSc6d1	budoucí
ději	děj	k1gInSc6	děj
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Legie	legie	k1gFnSc1	legie
<g/>
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
-	-	kIx~	-
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc4	povídka
Ukradla	ukradnout	k5eAaPmAgFnS	ukradnout
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
spisovateli	spisovatel	k1gMnSc6	spisovatel
Jacobsovi	Jacobs	k1gMnSc6	Jacobs
<g/>
?	?	kIx.	?
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
soud	soud	k1gInSc1	soud
Rowling	Rowling	k1gInSc1	Rowling
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Fanouškovský	fanouškovský	k2eAgInSc1d1	fanouškovský
web	web	k1gInSc1	web
o	o	k7c4	o
J.K.	J.K.	k1gFnSc4	J.K.
Rowlingové	Rowlingový	k2eAgFnSc2d1	Rowlingová
</s>
