<p>
<s>
Otmar	Otmar	k1gMnSc1	Otmar
Daniel	Daniel	k1gMnSc1	Daniel
Zinke	Zinke	k1gFnSc1	Zinke
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1664	[number]	k4	1664
Strzegom	Strzegom	k1gInSc1	Strzegom
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1738	[number]	k4	1738
Broumov	Broumov	k1gInSc1	Broumov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
benediktin	benediktin	k1gMnSc1	benediktin
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1700	[number]	k4	1700
<g/>
–	–	k?	–
<g/>
1738	[number]	k4	1738
opat	opat	k1gMnSc1	opat
břevnovsko-broumovského	břevnovskoroumovský	k2eAgNnSc2d1	břevnovsko-broumovský
opatství	opatství	k1gNnSc2	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
všestranným	všestranný	k2eAgMnSc7d1	všestranný
milovníkem	milovník	k1gMnSc7	milovník
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
obchodníka	obchodník	k1gMnSc2	obchodník
Daniela	Daniel	k1gMnSc2	Daniel
Zinkeho	Zinke	k1gMnSc2	Zinke
ve	v	k7c6	v
Střehomi	Střeho	k1gFnPc7	Střeho
<g/>
,	,	kIx,	,
v	v	k7c6	v
knížectví	knížectví	k1gNnSc6	knížectví
svídnickém	svídnický	k2eAgNnSc6d1	svídnický
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Broumova	Broumov	k1gInSc2	Broumov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Otmar	Otmar	k1gMnSc1	Otmar
Daniel	Daniel	k1gMnSc1	Daniel
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
klášterní	klášterní	k2eAgNnSc4d1	klášterní
gymnázium	gymnázium	k1gNnSc4	gymnázium
a	a	k8xC	a
v	v	k7c6	v
Broumově	Broumov	k1gInSc6	Broumov
také	také	k9	také
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
k	k	k7c3	k
benediktinům	benediktin	k1gMnPc3	benediktin
do	do	k7c2	do
noviciátu	noviciát	k1gInSc2	noviciát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c6	na
arcibiskupském	arcibiskupský	k2eAgInSc6d1	arcibiskupský
semináři	seminář	k1gInSc6	seminář
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
opatům	opat	k1gMnPc3	opat
břevnovsko-broumovských	břevnovskoroumovský	k2eAgMnPc2d1	břevnovsko-broumovský
benediktinů	benediktin	k1gMnPc2	benediktin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
opatem	opat	k1gMnSc7	opat
<g/>
,	,	kIx,	,
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
obnovu	obnova	k1gFnSc4	obnova
břevnovského	břevnovský	k2eAgInSc2d1	břevnovský
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
přestavbu	přestavba	k1gFnSc4	přestavba
broumovského	broumovský	k2eAgInSc2d1	broumovský
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
broumovské	broumovský	k2eAgFnSc2d1	Broumovská
skupiny	skupina	k1gFnSc2	skupina
barokních	barokní	k2eAgInPc2d1	barokní
kostelů	kostel	k1gInPc2	kostel
stavitelů	stavitel	k1gMnPc2	stavitel
Kryštofa	Kryštof	k1gMnSc2	Kryštof
a	a	k8xC	a
Kiliána	Kilián	k1gMnSc2	Kilián
Ignáce	Ignác	k1gMnSc2	Ignác
Dientzenhoferových	Dientzenhoferových	k2eAgMnSc2d1	Dientzenhoferových
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
broumovské	broumovský	k2eAgFnSc2d1	Broumovská
skupiny	skupina	k1gFnSc2	skupina
barokních	barokní	k2eAgInPc2d1	barokní
kostelů	kostel	k1gInPc2	kostel
patří	patřit	k5eAaImIp3nS	patřit
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Máří	Máří	k?	Máří
Magdaleny	Magdalena	k1gFnSc2	Magdalena
(	(	kIx(	(
<g/>
Božanov	Božanov	k1gInSc1	Božanov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc1	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
(	(	kIx(	(
<g/>
Heřmánkovice	Heřmánkovice	k1gFnSc1	Heřmánkovice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Martina	Martin	k1gMnSc2	Martin
(	(	kIx(	(
<g/>
Martínkovice	Martínkovice	k1gFnSc1	Martínkovice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
(	(	kIx(	(
<g/>
Otovice	Otovice	k1gFnSc1	Otovice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
(	(	kIx(	(
<g/>
Ruprechtice	Ruprechtice	k1gFnSc1	Ruprechtice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Markéty	Markéta	k1gFnSc2	Markéta
(	(	kIx(	(
<g/>
Šonov	Šonov	k1gInSc1	Šonov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Michaela	Michael	k1gMnSc2	Michael
archanděla	archanděl	k1gMnSc2	archanděl
(	(	kIx(	(
<g/>
Vernéřovice	Vernéřovice	k1gFnSc1	Vernéřovice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
Vižňov	Vižňov	k1gInSc1	Vižňov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
(	(	kIx(	(
<g/>
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Broumov	Broumov	k1gInSc1	Broumov
</s>
</p>
<p>
<s>
Cestování	cestování	k1gNnSc1	cestování
<g/>
.	.	kIx.	.
<g/>
idnes	idnes	k1gInSc1	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
