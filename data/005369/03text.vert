<s>
Osová	osový	k2eAgFnSc1d1	Osová
souměrnost	souměrnost	k1gFnSc1	souměrnost
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
geometrického	geometrický	k2eAgNnSc2d1	geometrické
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Osová	osový	k2eAgFnSc1d1	Osová
souměrnost	souměrnost	k1gFnSc1	souměrnost
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
(	(	kIx(	(
<g/>
i	i	k8xC	i
úhly	úhel	k1gInPc1	úhel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
druh	druh	k1gInSc4	druh
shodnosti	shodnost	k1gFnSc2	shodnost
<g/>
.	.	kIx.	.
</s>
<s>
Osová	osový	k2eAgFnSc1d1	Osová
souměrnost	souměrnost	k1gFnSc1	souměrnost
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
nebo	nebo	k8xC	nebo
prostoru	prostor	k1gInSc6	prostor
s	s	k7c7	s
přímkou	přímka	k1gFnSc7	přímka
o	o	k7c4	o
jako	jako	k9	jako
osou	osý	k2eAgFnSc4d1	osá
(	(	kIx(	(
<g/>
souměrnosti	souměrnost	k1gFnSc6	souměrnost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc1	takový
zobrazení	zobrazení	k1gNnSc1	zobrazení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
prvky	prvek	k1gInPc4	prvek
osy	osa	k1gFnSc2	osa
o	o	k7c4	o
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
samé	samý	k3xTgNnSc1	samý
a	a	k8xC	a
bod	bod	k1gInSc1	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
ležící	ležící	k2eAgFnSc1d1	ležící
mimo	mimo	k7c4	mimo
osu	osa	k1gFnSc4	osa
o	o	k7c6	o
s	s	k7c7	s
průmětem	průmět	k1gInSc7	průmět
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
do	do	k7c2	do
osy	osa	k1gFnSc2	osa
o	o	k7c4	o
na	na	k7c4	na
bod	bod	k1gInSc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s hack="1">
polopřímce	polopřímka	k1gFnSc6	polopřímka
opačné	opačný	k2eAgFnSc6d1	opačná
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
SA	SA	kA	SA
<g/>
}	}	kIx)	}
ve	v	k7c4	v
stejné	stejný	k2eAgFnPc4d1	stejná
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
jako	jako	k8xC	jako
bod	bod	k1gInSc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
S	s	k7c7	s
A	A	kA	A
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
SA	SA	kA	SA
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
SA	SA	kA	SA
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Útvar	útvar	k1gInSc1	útvar
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
již	již	k6eAd1	již
na	na	k7c6	na
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
nebo	nebo	k8xC	nebo
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
)	)	kIx)	)
označujeme	označovat	k5eAaImIp1nP	označovat
za	za	k7c4	za
osově	osově	k6eAd1	osově
souměrný	souměrný	k2eAgInSc4d1	souměrný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
osové	osový	k2eAgFnSc6d1	Osová
souměrnosti	souměrnost	k1gFnSc6	souměrnost
obrazem	obraz	k1gInSc7	obraz
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Osu	osa	k1gFnSc4	osa
této	tento	k3xDgFnSc2	tento
souměrnosti	souměrnost	k1gFnSc2	souměrnost
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
osou	osa	k1gFnSc7	osa
útvaru	útvar	k1gInSc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Osové	osový	k2eAgFnPc1d1	Osová
souměrnosti	souměrnost	k1gFnPc1	souměrnost
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
každá	každý	k3xTgFnSc1	každý
shodnost	shodnost	k1gFnSc1	shodnost
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
složit	složit	k5eAaPmF	složit
z	z	k7c2	z
nejvýše	nejvýše	k6eAd1	nejvýše
tří	tři	k4xCgFnPc2	tři
osových	osový	k2eAgFnPc2d1	Osová
souměrností	souměrnost	k1gFnPc2	souměrnost
<g/>
.	.	kIx.	.
</s>
<s>
Osovou	osový	k2eAgFnSc4d1	Osová
souměrnost	souměrnost	k1gFnSc4	souměrnost
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
i	i	k9	i
v	v	k7c6	v
euklidovském	euklidovský	k2eAgInSc6d1	euklidovský
prostoru	prostor	k1gInSc6	prostor
vyšších	vysoký	k2eAgFnPc2d2	vyšší
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
obecně	obecně	k6eAd1	obecně
souměrnost	souměrnost	k1gFnSc4	souměrnost
podle	podle	k7c2	podle
přímky	přímka	k1gFnSc2	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Úsečka	úsečka	k1gFnSc1	úsečka
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
jedinou	jediný	k2eAgFnSc4d1	jediná
osu	osa	k1gFnSc4	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
-	-	kIx~	-
kolmici	kolmice	k1gFnSc4	kolmice
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Přímka	přímka	k1gFnSc1	přímka
je	být	k5eAaImIp3nS	být
osově	osově	k6eAd1	osově
souměrná	souměrný	k2eAgFnSc1d1	souměrná
<g/>
,	,	kIx,	,
osou	osa	k1gFnSc7	osa
je	být	k5eAaImIp3nS	být
libovolná	libovolný	k2eAgFnSc1d1	libovolná
různoběžná	různoběžný	k2eAgFnSc1d1	různoběžná
kolmice	kolmice	k1gFnSc1	kolmice
<g/>
.	.	kIx.	.
</s>
<s>
Rovnoramenný	rovnoramenný	k2eAgInSc1d1	rovnoramenný
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
není	být	k5eNaImIp3nS	být
rovnostranný	rovnostranný	k2eAgInSc1d1	rovnostranný
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jedinou	jediný	k2eAgFnSc4d1	jediná
osu	osa	k1gFnSc4	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
<g/>
,	,	kIx,	,
osu	osa	k1gFnSc4	osa
jeho	jeho	k3xOp3gFnSc2	jeho
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
rovnoramenný	rovnoramenný	k2eAgInSc1d1	rovnoramenný
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
osově	osově	k6eAd1	osově
souměrný	souměrný	k2eAgInSc1d1	souměrný
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
mnohoúhelníky	mnohoúhelník	k1gInPc1	mnohoúhelník
jsou	být	k5eAaImIp3nP	být
osově	osově	k6eAd1	osově
souměrné	souměrný	k2eAgFnPc1d1	souměrná
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
různých	různý	k2eAgFnPc2d1	různá
os	osa	k1gFnPc2	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
počtu	počet	k1gInSc3	počet
vrcholů	vrchol	k1gInPc2	vrchol
mnohoúhelníka	mnohoúhelník	k1gInSc2	mnohoúhelník
-	-	kIx~	-
například	například	k6eAd1	například
rovnostranný	rovnostranný	k2eAgInSc1d1	rovnostranný
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc1	tři
osy	osa	k1gFnPc1	osa
souměrnosti	souměrnost	k1gFnPc1	souměrnost
<g/>
,	,	kIx,	,
čtverec	čtverec	k1gInSc1	čtverec
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
,	,	kIx,	,
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
šestiúhelník	šestiúhelník	k1gInSc1	šestiúhelník
šest	šest	k4xCc1	šest
<g/>
.	.	kIx.	.
</s>
<s>
Kruh	kruh	k1gInSc1	kruh
(	(	kIx(	(
<g/>
i	i	k9	i
kružnice	kružnice	k1gFnSc2	kružnice
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
příkladem	příklad	k1gInSc7	příklad
útvarů	útvar	k1gInPc2	útvar
s	s	k7c7	s
nekonečně	konečně	k6eNd1	konečně
mnoha	mnoho	k4c7	mnoho
osami	osa	k1gFnPc7	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
-	-	kIx~	-
každá	každý	k3xTgFnSc1	každý
přímka	přímka	k1gFnSc1	přímka
(	(	kIx(	(
<g/>
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
rovině	rovina	k1gFnSc6	rovina
<g/>
)	)	kIx)	)
procházející	procházející	k2eAgFnSc1d1	procházející
středem	střed	k1gInSc7	střed
je	být	k5eAaImIp3nS	být
osou	osý	k2eAgFnSc4d1	osá
souměrnosti	souměrnost	k1gFnPc4	souměrnost
<g/>
.	.	kIx.	.
</s>
<s>
Hyperbola	hyperbola	k1gFnSc1	hyperbola
<g/>
,	,	kIx,	,
elipsa	elipsa	k1gFnSc1	elipsa
i	i	k8xC	i
parabola	parabola	k1gFnSc1	parabola
jsou	být	k5eAaImIp3nP	být
dalšími	další	k2eAgInPc7d1	další
příklady	příklad	k1gInPc7	příklad
osově	osově	k6eAd1	osově
souměrných	souměrný	k2eAgMnPc2d1	souměrný
rovinných	rovinný	k2eAgMnPc2d1	rovinný
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Krychle	krychle	k1gFnSc1	krychle
<g/>
,	,	kIx,	,
koule	koule	k1gFnSc1	koule
<g/>
,	,	kIx,	,
kužel	kužel	k1gInSc1	kužel
nebo	nebo	k8xC	nebo
válec	válec	k1gInSc1	válec
jsou	být	k5eAaImIp3nP	být
příkladem	příklad	k1gInSc7	příklad
osově	osově	k6eAd1	osově
souměrného	souměrný	k2eAgInSc2d1	souměrný
prostorového	prostorový	k2eAgInSc2d1	prostorový
útvaru	útvar	k1gInSc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Jehlan	jehlan	k1gInSc1	jehlan
je	být	k5eAaImIp3nS	být
osově	osově	k6eAd1	osově
souměrný	souměrný	k2eAgInSc1d1	souměrný
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
základna	základna	k1gFnSc1	základna
je	být	k5eAaImIp3nS	být
středově	středově	k6eAd1	středově
souměrný	souměrný	k2eAgInSc1d1	souměrný
rovinný	rovinný	k2eAgInSc1d1	rovinný
útvar	útvar	k1gInSc1	útvar
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vrchol	vrchol	k1gInSc1	vrchol
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
kolmici	kolmice	k1gFnSc4	kolmice
na	na	k7c4	na
rovinu	rovina	k1gFnSc4	rovina
základny	základna	k1gFnSc2	základna
procházející	procházející	k2eAgFnSc2d1	procházející
středem	středem	k7c2	středem
souměrnosti	souměrnost	k1gFnSc2	souměrnost
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Osová	osový	k2eAgFnSc1d1	Osová
souměrnost	souměrnost	k1gFnSc1	souměrnost
je	být	k5eAaImIp3nS	být
sama	sám	k3xTgFnSc1	sám
sobě	se	k3xPyFc3	se
inverzním	inverzní	k2eAgNnSc7d1	inverzní
zobrazením	zobrazení	k1gNnSc7	zobrazení
-	-	kIx~	-
složením	složení	k1gNnSc7	složení
dvou	dva	k4xCgFnPc2	dva
osových	osový	k2eAgFnPc2d1	Osová
souměrností	souměrnost	k1gFnPc2	souměrnost
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
osou	osa	k1gFnSc7	osa
vzniká	vznikat	k5eAaImIp3nS	vznikat
identita	identita	k1gFnSc1	identita
<g/>
.	.	kIx.	.
</s>
<s>
Osová	osový	k2eAgFnSc1d1	Osová
souměrnost	souměrnost	k1gFnSc1	souměrnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
každá	každý	k3xTgFnSc1	každý
souměrnost	souměrnost	k1gFnSc1	souměrnost
<g/>
)	)	kIx)	)
involucí	involuce	k1gFnPc2	involuce
<g/>
.	.	kIx.	.
</s>
<s>
Osová	osový	k2eAgFnSc1d1	Osová
souměrnost	souměrnost	k1gFnSc1	souměrnost
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
mění	měnit	k5eAaImIp3nS	měnit
orientaci	orientace	k1gFnSc4	orientace
útvarů	útvar	k1gInPc2	útvar
-	-	kIx~	-
pokud	pokud	k8xS	pokud
bylo	být	k5eAaImAgNnS	být
pořadí	pořadí	k1gNnSc4	pořadí
vrcholů	vrchol	k1gInPc2	vrchol
v	v	k7c6	v
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pořadí	pořadí	k1gNnSc1	pořadí
jejich	jejich	k3xOp3gInPc2	jejich
obrazů	obraz	k1gInPc2	obraz
v	v	k7c6	v
osové	osový	k2eAgFnSc6d1	Osová
souměrnosti	souměrnost	k1gFnSc6	souměrnost
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Osová	osový	k2eAgFnSc1d1	Osová
souměrnost	souměrnost	k1gFnSc1	souměrnost
v	v	k7c6	v
(	(	kIx(	(
<g/>
trojrozměrném	trojrozměrný	k2eAgInSc6d1	trojrozměrný
<g/>
)	)	kIx)	)
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
otočením	otočení	k1gNnSc7	otočení
o	o	k7c4	o
180	[number]	k4	180
stupňů	stupeň	k1gInPc2	stupeň
okolo	okolo	k7c2	okolo
stejné	stejný	k2eAgFnSc2d1	stejná
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přímá	přímý	k2eAgFnSc1d1	přímá
shodnost	shodnost	k1gFnSc1	shodnost
(	(	kIx(	(
<g/>
přemístění	přemístění	k1gNnSc1	přemístění
<g/>
)	)	kIx)	)
a	a	k8xC	a
orientaci	orientace	k1gFnSc4	orientace
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc4	bod
ležící	ležící	k2eAgInPc4d1	ležící
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
všechny	všechen	k3xTgInPc4	všechen
její	její	k3xOp3gInPc4	její
samodružné	samodružný	k2eAgInPc4d1	samodružný
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
přímky	přímka	k1gFnPc1	přímka
kolmé	kolmý	k2eAgFnPc1d1	kolmá
k	k	k7c3	k
ose	osa	k1gFnSc3	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
jsou	být	k5eAaImIp3nP	být
samodružné	samodružný	k2eAgFnPc1d1	samodružná
<g/>
.	.	kIx.	.
</s>
<s>
Osová	osový	k2eAgFnSc1d1	Osová
souměrnost	souměrnost	k1gFnSc1	souměrnost
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
samodružné	samodružný	k2eAgInPc4d1	samodružný
směry	směr	k1gInPc4	směr
-	-	kIx~	-
směr	směr	k1gInSc1	směr
její	její	k3xOp3gFnSc2	její
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
směr	směr	k1gInSc1	směr
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
kolmý	kolmý	k2eAgMnSc1d1	kolmý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
samodružný	samodružný	k2eAgInSc4d1	samodružný
směr	směr	k1gInSc4	směr
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
k	k	k7c3	k
ní	on	k3xPp3gFnSc6	on
kolmý	kolmý	k2eAgInSc5d1	kolmý
<g/>
.	.	kIx.	.
</s>
<s>
Středová	středový	k2eAgFnSc1d1	středová
souměrnost	souměrnost	k1gFnSc1	souměrnost
Rovinová	rovinový	k2eAgFnSc1d1	rovinová
souměrnost	souměrnost	k1gFnSc1	souměrnost
Shodné	shodný	k2eAgNnSc1d1	shodné
zobrazení	zobrazení	k1gNnSc4	zobrazení
</s>
