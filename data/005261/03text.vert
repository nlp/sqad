<p>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
de	de	k?	de
Lully	Lull	k1gMnPc7	Lull
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
ʒ	ʒ	k?	ʒ
<g/>
.	.	kIx.	.
<g/>
ba	ba	k9	ba
<g/>
.	.	kIx.	.
<g/>
tist	tist	k?	tist
de	de	k?	de
ly	ly	k?	ly
<g/>
.	.	kIx.	.
<g/>
li	li	k8xS	li
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Giovanni	Giovanň	k1gMnSc3	Giovanň
Battista	Battista	k1gMnSc1	Battista
Lulli	Lull	k1gMnSc3	Lull
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1632	[number]	k4	1632
Florencie	Florencie	k1gFnSc2	Florencie
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1687	[number]	k4	1687
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1662	[number]	k4	1662
zcela	zcela	k6eAd1	zcela
ovládal	ovládat	k5eAaImAgInS	ovládat
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
hudební	hudební	k2eAgFnSc4d1	hudební
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
francouzským	francouzský	k2eAgMnPc3d1	francouzský
skladatelům	skladatel	k1gMnPc3	skladatel
barokního	barokní	k2eAgNnSc2d1	barokní
období	období	k1gNnSc2	období
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
faktickým	faktický	k2eAgMnSc7d1	faktický
tvůrcem	tvůrce	k1gMnSc7	tvůrce
francouzské	francouzský	k2eAgFnSc2d1	francouzská
národní	národní	k2eAgFnSc2d1	národní
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
první	první	k4xOgFnSc2	první
francouzské	francouzský	k2eAgFnSc2d1	francouzská
opery	opera	k1gFnSc2	opera
Cadmus	Cadmus	k1gMnSc1	Cadmus
et	et	k?	et
Hermione	Hermion	k1gInSc5	Hermion
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
<g/>
)	)	kIx)	)
a	a	k8xC	a
francouzského	francouzský	k2eAgInSc2d1	francouzský
typu	typ	k1gInSc2	typ
operní	operní	k2eAgFnSc2d1	operní
ouvertury	ouvertura	k1gFnSc2	ouvertura
<g/>
.	.	kIx.	.
<g/>
Lully	Lulla	k1gFnSc2	Lulla
také	také	k9	také
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
recitativ	recitativ	k1gInSc1	recitativ
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
recitativo	recitativa	k1gFnSc5	recitativa
secco	secco	k1gNnSc1	secco
(	(	kIx(	(
<g/>
suchý	suchý	k2eAgInSc1d1	suchý
recitativ	recitativ	k1gInSc1	recitativ
<g/>
)	)	kIx)	)
novou	nový	k2eAgFnSc7d1	nová
formou	forma	k1gFnSc7	forma
recitativo	recitativa	k1gFnSc5	recitativa
accompagnato	accompagnato	k6eAd1	accompagnato
(	(	kIx(	(
<g/>
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
recitativ	recitativ	k1gInSc1	recitativ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgNnSc1d1	typické
rytmické	rytmický	k2eAgInPc1d1	rytmický
variace	variace	k1gFnPc4	variace
a	a	k8xC	a
přesná	přesný	k2eAgNnPc4d1	přesné
umístění	umístění	k1gNnSc4	umístění
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
deklamace	deklamace	k1gFnSc2	deklamace
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
přesně	přesně	k6eAd1	přesně
uzpůsoben	uzpůsobit	k5eAaPmNgMnS	uzpůsobit
pro	pro	k7c4	pro
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zpěvu	zpěv	k1gInSc6	zpěv
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
invenci	invence	k1gFnSc3	invence
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
árií	árie	k1gFnSc7	árie
a	a	k8xC	a
recitativem	recitativ	k1gInSc7	recitativ
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
pak	pak	k6eAd1	pak
zní	znět	k5eAaImIp3nS	znět
více	hodně	k6eAd2	hodně
kontinuálně	kontinuálně	k6eAd1	kontinuálně
<g/>
.	.	kIx.	.
<g/>
Patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
několik	několik	k4yIc4	několik
málo	málo	k6eAd1	málo
důvěrných	důvěrný	k2eAgInPc2d1	důvěrný
přátel	přítel	k1gMnPc2	přítel
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejvlivnějším	vlivný	k2eAgMnSc7d3	nejvlivnější
a	a	k8xC	a
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
skladatelem	skladatel	k1gMnSc7	skladatel
v	v	k7c6	v
období	období	k1gNnSc6	období
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
ve	v	k7c6	v
francouzských	francouzský	k2eAgFnPc6d1	francouzská
dějinách	dějiny	k1gFnPc6	dějiny
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
<g/>
Téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgNnPc4	tři
staletí	staletí	k1gNnPc4	staletí
byl	být	k5eAaImAgInS	být
Lully	Lulla	k1gFnSc2	Lulla
znám	znám	k2eAgInSc1d1	znám
spíše	spíše	k9	spíše
kvůli	kvůli	k7c3	kvůli
neobvyklým	obvyklý	k2eNgFnPc3d1	neobvyklá
okolnostem	okolnost	k1gFnPc3	okolnost
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dirigování	dirigování	k1gNnSc6	dirigování
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
zkoušek	zkouška	k1gFnPc2	zkouška
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1687	[number]	k4	1687
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Lully	Lull	k1gInPc7	Lull
prudce	prudko	k6eAd1	prudko
udeřil	udeřit	k5eAaPmAgInS	udeřit
bodcem	bodec	k1gInSc7	bodec
těžké	těžký	k2eAgFnSc2d1	těžká
barokní	barokní	k2eAgFnSc2d1	barokní
taktovky	taktovka	k1gFnSc2	taktovka
do	do	k7c2	do
palce	palec	k1gInSc2	palec
pravé	pravý	k2eAgFnSc2d1	pravá
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
hygieny	hygiena	k1gFnSc2	hygiena
zanítil	zanítit	k5eAaPmAgMnS	zanítit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
odmítal	odmítat	k5eAaImAgMnS	odmítat
včasnou	včasný	k2eAgFnSc4d1	včasná
amputaci	amputace	k1gFnSc4	amputace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
zastavení	zastavení	k1gNnSc4	zastavení
gangrény	gangréna	k1gFnSc2	gangréna
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc1	zánět
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgFnSc1d1	pozdní
amputace	amputace	k1gFnSc1	amputace
nohy	noha	k1gFnSc2	noha
již	již	k6eAd1	již
nepomohla	pomoct	k5eNaPmAgFnS	pomoct
a	a	k8xC	a
Lully	Lulla	k1gFnPc1	Lulla
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
nový	nový	k2eAgInSc4d1	nový
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
barokní	barokní	k2eAgFnSc4d1	barokní
hudbu	hudba	k1gFnSc4	hudba
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
hlubšímu	hluboký	k2eAgNnSc3d2	hlubší
poznání	poznání	k1gNnSc3	poznání
Lullyho	Lully	k1gMnSc2	Lully
děl	dít	k5eAaBmAgMnS	dít
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
Lullyho	Lully	k1gMnSc4	Lully
nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Moliè	Moliè	k1gMnSc7	Moliè
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
a	a	k8xC	a
Psyché	psyché	k1gFnSc7	psyché
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Quinaultem	Quinault	k1gInSc7	Quinault
pak	pak	k6eAd1	pak
královská	královský	k2eAgFnSc1d1	královská
opera	opera	k1gFnSc1	opera
Atys	Atys	k1gInSc1	Atys
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
muzikantů	muzikant	k1gMnPc2	muzikant
Isis	Isis	k1gFnSc1	Isis
a	a	k8xC	a
mistrovské	mistrovský	k2eAgNnSc1d1	mistrovské
dílo	dílo	k1gNnSc1	dílo
Armida	Armida	k1gFnSc1	Armida
<g/>
.	.	kIx.	.
<g/>
Jeho	on	k3xPp3gInSc4	on
kompoziční	kompoziční	k2eAgInSc4d1	kompoziční
styl	styl	k1gInSc4	styl
napodobovali	napodobovat	k5eAaImAgMnP	napodobovat
soudobí	soudobý	k2eAgMnPc1d1	soudobý
hudební	hudební	k2eAgMnPc1d1	hudební
skladatelé	skladatel	k1gMnPc1	skladatel
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
rozsáhlému	rozsáhlý	k2eAgInSc3d1	rozsáhlý
dílu	díl	k1gInSc3	díl
a	a	k8xC	a
hudebním	hudební	k2eAgFnPc3d1	hudební
invencím	invence	k1gFnPc3	invence
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
Lully	Lulla	k1gFnPc4	Lulla
i	i	k8xC	i
řadu	řada	k1gFnSc4	řada
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
G.	G.	kA	G.
F.	F.	kA	F.
Händela	Händel	k1gMnSc2	Händel
<g/>
,	,	kIx,	,
J.	J.	kA	J.
S.	S.	kA	S.
Bacha	Bacha	k?	Bacha
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
P.	P.	kA	P.
Rameaua	Rameaua	k1gMnSc1	Rameaua
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Giovanni	Giovanň	k1gMnSc3	Giovanň
Battista	Battista	k1gMnSc1	Battista
Lulli	Lull	k1gMnSc3	Lull
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
toskánské	toskánský	k2eAgFnSc6d1	Toskánská
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Giovanniho	Giovannize	k6eAd1	Giovannize
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
mlynář	mlynář	k1gMnSc1	mlynář
Lorenzo	Lorenza	k1gFnSc5	Lorenza
di	di	k?	di
Maldo	Malda	k1gMnSc5	Malda
Lulli	Lull	k1gMnSc5	Lull
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
Caterina	Caterina	k1gFnSc1	Caterina
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
del	del	k?	del
Sera	srát	k5eAaImSgMnS	srát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
projevoval	projevovat	k5eAaImAgInS	projevovat
velké	velký	k2eAgNnSc4d1	velké
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yQgMnSc3	který
nemusel	muset	k5eNaImAgMnS	muset
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
otcově	otcův	k2eAgInSc6d1	otcův
mlýně	mlýn	k1gInSc6	mlýn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
patřičného	patřičný	k2eAgNnSc2d1	patřičné
vzdělání	vzdělání	k1gNnSc2	vzdělání
u	u	k7c2	u
františkánů	františkán	k1gMnPc2	františkán
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
naučili	naučit	k5eAaPmAgMnP	naučit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
cembalo	cembalo	k1gNnSc4	cembalo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1646	[number]	k4	1646
si	se	k3xPyFc3	se
nadaného	nadaný	k2eAgMnSc4d1	nadaný
třináctiletého	třináctiletý	k2eAgMnSc4d1	třináctiletý
chlapce	chlapec	k1gMnSc4	chlapec
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
Roger	Roger	k1gMnSc1	Roger
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
de	de	k?	de
Guise	Guis	k1gMnSc4	Guis
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Florencii	Florencie	k1gFnSc4	Florencie
během	během	k7c2	během
místních	místní	k2eAgFnPc2d1	místní
slavností	slavnost	k1gFnPc2	slavnost
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Malty	Malta	k1gFnSc2	Malta
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
hledal	hledat	k5eAaImAgMnS	hledat
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
konverzovat	konverzovat	k5eAaImF	konverzovat
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
neteří	neteř	k1gFnSc7	neteř
Annou	Anna	k1gFnSc7	Anna
Marií	Maria	k1gFnSc7	Maria
Louisou	Louisa	k1gFnSc7	Louisa
Orleánskou	orleánský	k2eAgFnSc7d1	Orleánská
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Gastona	Gaston	k1gMnSc2	Gaston
Orleánského	orleánský	k2eAgMnSc2d1	orleánský
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k1gMnSc1	mladý
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
jej	on	k3xPp3gMnSc4	on
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
natolik	natolik	k6eAd1	natolik
nejspíše	nejspíše	k9	nejspíše
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ovládal	ovládat	k5eAaImAgInS	ovládat
zpěv	zpěv	k1gInSc4	zpěv
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc4	tanec
i	i	k8xC	i
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
<s>
Rytíř	Rytíř	k1gMnSc1	Rytíř
de	de	k?	de
Guise	Guise	k1gFnSc2	Guise
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pážetem	páže	k1gNnSc7	páže
a	a	k8xC	a
učitelem	učitel	k1gMnSc7	učitel
italštiny	italština	k1gFnSc2	italština
Anny	Anna	k1gFnSc2	Anna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Tuilerieském	Tuilerieský	k2eAgInSc6d1	Tuilerieský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlil	sídlit	k5eAaImAgInS	sídlit
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mladý	mladý	k1gMnSc1	mladý
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
rychle	rychle	k6eAd1	rychle
naučil	naučit	k5eAaPmAgMnS	naučit
francouzským	francouzský	k2eAgMnPc3d1	francouzský
zvykům	zvyk	k1gInPc3	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
princezna	princezna	k1gFnSc1	princezna
Anna	Anna	k1gFnSc1	Anna
Marie	Marie	k1gFnSc1	Marie
neměla	mít	k5eNaImAgFnS	mít
vlastní	vlastní	k2eAgMnPc4d1	vlastní
hudebníky	hudebník	k1gMnPc4	hudebník
či	či	k8xC	či
zpěváky	zpěvák	k1gMnPc4	zpěvák
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
Battista	Battista	k1gMnSc1	Battista
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
umělci	umělec	k1gMnPc7	umělec
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
zapojoval	zapojovat	k5eAaImAgInS	zapojovat
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
hudebních	hudební	k2eAgFnPc2d1	hudební
produkcí	produkce	k1gFnPc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
frondě	fronda	k1gFnSc6	fronda
<g/>
)	)	kIx)	)
však	však	k9	však
musel	muset	k5eAaImAgInS	muset
Gaston	Gaston	k1gInSc1	Gaston
Orleánský	orleánský	k2eAgInSc1d1	orleánský
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Anna	Anna	k1gFnSc1	Anna
Marie	Marie	k1gFnSc1	Marie
vyhoštěna	vyhoštěn	k2eAgFnSc1d1	vyhoštěna
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
v	v	k7c6	v
Saint-Fargeau	Saint-Fargeaum	k1gNnSc6	Saint-Fargeaum
<g/>
.	.	kIx.	.
<g/>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
známostem	známost	k1gFnPc3	známost
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
významnými	významný	k2eAgMnPc7d1	významný
hudebníky	hudebník	k1gMnPc7	hudebník
dostal	dostat	k5eAaPmAgMnS	dostat
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Battista	Battista	k1gMnSc1	Battista
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1653	[number]	k4	1653
příležitost	příležitost	k1gFnSc4	příležitost
tančit	tančit	k5eAaImF	tančit
ve	v	k7c6	v
velkolepém	velkolepý	k2eAgInSc6d1	velkolepý
Cambefortově	Cambefortův	k2eAgInSc6d1	Cambefortův
baletu	balet	k1gInSc6	balet
Ballet	Ballet	k1gInSc1	Ballet
de	de	k?	de
la	la	k1gNnSc1	la
Nuit	Nuit	k1gMnSc1	Nuit
(	(	kIx(	(
<g/>
Balet	balet	k1gInSc1	balet
Noci	noc	k1gFnSc2	noc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
v	v	k7c6	v
roli	role	k1gFnSc6	role
boha	bůh	k1gMnSc2	bůh
Apollóna	Apollón	k1gMnSc2	Apollón
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
si	se	k3xPyFc3	se
povšiml	povšimnout	k5eAaPmAgInS	povšimnout
Lulliho	Lulli	k1gMnSc4	Lulli
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ocenil	ocenit	k5eAaPmAgMnS	ocenit
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
mladý	mladý	k2eAgMnSc1d1	mladý
Giovanni	Giovaneň	k1gFnSc6	Giovaneň
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
nejoblíbenějších	oblíbený	k2eAgMnPc2d3	nejoblíbenější
tanečníků	tanečník	k1gMnPc2	tanečník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1653	[number]	k4	1653
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
královským	královský	k2eAgMnSc7d1	královský
skladatelem	skladatel	k1gMnSc7	skladatel
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
Compositeur	Compositeura	k1gFnPc2	Compositeura
de	de	k?	de
la	la	k1gNnSc4	la
Musique	Musiqu	k1gFnSc2	Musiqu
instrumentale	instrumental	k1gMnSc5	instrumental
de	de	k?	de
la	la	k0	la
chambre	chambr	k1gMnSc5	chambr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc4	jeho
kariéra	kariéra	k1gFnSc1	kariéra
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kariéra	kariéra	k1gFnSc1	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Dvorní	dvorní	k2eAgInSc1d1	dvorní
balet	balet	k1gInSc1	balet
a	a	k8xC	a
Moliè	Moliè	k1gFnSc1	Moliè
====	====	k?	====
</s>
</p>
<p>
<s>
Dvorní	dvorní	k2eAgInSc1d1	dvorní
balet	balet	k1gInSc1	balet
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
ballet	ballet	k1gInSc1	ballet
de	de	k?	de
cour	cour	k?	cour
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
francouzského	francouzský	k2eAgInSc2d1	francouzský
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Velkého	velký	k2eAgNnSc2d1	velké
století	století	k1gNnSc2	století
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
<g/>
;	;	kIx,	;
v	v	k7c6	v
baletech	balet	k1gInPc6	balet
tančil	tančit	k5eAaImAgMnS	tančit
sám	sám	k3xTgMnSc1	sám
král	král	k1gMnSc1	král
i	i	k8xC	i
dvořané	dvořan	k1gMnPc1	dvořan
<g/>
.	.	kIx.	.
</s>
<s>
Lulli	Lull	k1gMnPc1	Lull
se	se	k3xPyFc4	se
dvorních	dvorní	k2eAgNnPc2d1	dvorní
představení	představení	k1gNnPc2	představení
účastnil	účastnit	k5eAaImAgMnS	účastnit
jako	jako	k9	jako
tanečník	tanečník	k1gMnSc1	tanečník
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1658	[number]	k4	1658
začal	začít	k5eAaPmAgMnS	začít
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
dvorní	dvorní	k2eAgInPc4d1	dvorní
balety	balet	k1gInPc4	balet
sám	sám	k3xTgInSc4	sám
skládat	skládat	k5eAaImF	skládat
na	na	k7c4	na
texty	text	k1gInPc4	text
Isaaca	Isaaca	k1gMnSc1	Isaaca
de	de	k?	de
Benserada	Benserada	k1gFnSc1	Benserada
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
soustavně	soustavně	k6eAd1	soustavně
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1669	[number]	k4	1669
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1661	[number]	k4	1661
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
superintendantem	superintendant	k1gMnSc7	superintendant
královy	králův	k2eAgFnSc2d1	králova
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
Surintendant	Surintendant	k1gInSc1	Surintendant
de	de	k?	de
la	la	k1gNnSc2	la
Musique	Musique	k1gFnPc2	Musique
de	de	k?	de
la	la	k1gNnSc4	la
Chambre	Chambr	k1gInSc5	Chambr
du	du	k?	du
Roi	Roi	k1gMnPc7	Roi
<g/>
)	)	kIx)	)
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
francouzské	francouzský	k2eAgFnSc2d1	francouzská
naturalizační	naturalizační	k2eAgFnSc2d1	naturalizační
listiny	listina	k1gFnSc2	listina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
pofrancouzštil	pofrancouzštit	k5eAaPmAgMnS	pofrancouzštit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lull	k1gInPc4	Lull
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1664	[number]	k4	1664
<g/>
–	–	k?	–
<g/>
1671	[number]	k4	1671
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
zejména	zejména	k9	zejména
s	s	k7c7	s
Moliè	Moliè	k1gFnSc7	Moliè
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Poquelin	Poquelin	k2eAgMnSc1d1	Poquelin
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
společně	společně	k6eAd1	společně
tvořili	tvořit	k5eAaImAgMnP	tvořit
comédie-ballety	comédieallet	k2eAgFnPc4d1	comédie-ballet
(	(	kIx(	(
<g/>
baletní	baletní	k2eAgFnPc4d1	baletní
komedie	komedie	k1gFnPc4	komedie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Přátelství	přátelství	k1gNnSc1	přátelství
s	s	k7c7	s
Moliè	Moliè	k1gFnSc7	Moliè
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Lullyho	Lully	k1gMnSc4	Lully
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
klíčové	klíčový	k2eAgNnSc1d1	klíčové
<g/>
;	;	kIx,	;
popularita	popularita	k1gFnSc1	popularita
Moliè	Moliè	k1gFnSc2	Moliè
děl	dělo	k1gNnPc2	dělo
s	s	k7c7	s
Lullyho	Lullyha	k1gFnSc5	Lullyha
hudbou	hudba	k1gFnSc7	hudba
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc3d1	vysoká
úrovni	úroveň	k1gFnSc3	úroveň
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
oba	dva	k4xCgMnPc1	dva
upevňovali	upevňovat	k5eAaImAgMnP	upevňovat
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
známí	známý	k1gMnPc1	známý
jako	jako	k8xC	jako
Les	les	k1gInSc4	les
deux	deux	k1gInSc1	deux
Jean-Baptistes	Jean-Baptistes	k1gInSc1	Jean-Baptistes
(	(	kIx(	(
<g/>
dva	dva	k4xCgMnPc1	dva
Jean-Baptisté	Jean-Baptista	k1gMnPc1	Jean-Baptista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
spolupráci	spolupráce	k1gFnSc4	spolupráce
dovedli	dovést	k5eAaPmAgMnP	dovést
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Le	Le	k1gFnSc2	Le
Bourgeois	Bourgeois	k1gFnPc1	Bourgeois
gentilhomme	gentilhomit	k5eAaPmRp1nP	gentilhomit
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
,	,	kIx,	,
1670	[number]	k4	1670
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
Psyché	psyché	k1gFnSc1	psyché
<g/>
,	,	kIx,	,
sepsaná	sepsaný	k2eAgFnSc1d1	sepsaná
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Philippe	Philipp	k1gInSc5	Philipp
Quinaultem	Quinault	k1gInSc7	Quinault
a	a	k8xC	a
Pierre	Pierr	k1gInSc5	Pierr
Corneillem	Corneill	k1gInSc7	Corneill
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1671	[number]	k4	1671
<g/>
,	,	kIx,	,
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
Moliè	Moliè	k1gFnSc4	Moliè
jeho	on	k3xPp3gInSc4	on
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
znamenala	znamenat	k5eAaImAgFnS	znamenat
konec	konec	k1gInSc4	konec
přátelského	přátelský	k2eAgInSc2d1	přátelský
vztahu	vztah	k1gInSc2	vztah
a	a	k8xC	a
spolupráce	spolupráce	k1gFnSc2	spolupráce
mezi	mezi	k7c4	mezi
Lullym	Lullym	k1gInSc4	Lullym
a	a	k8xC	a
Moliè	Moliè	k1gMnSc2	Moliè
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
následně	následně	k6eAd1	následně
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Lullyho	Lullyha	k1gFnSc5	Lullyha
rivalem	rival	k1gMnSc7	rival
Marc-Antoine	Marc-Antoin	k1gInSc5	Marc-Antoin
Charpentierem	Charpentier	k1gInSc7	Charpentier
<g/>
.	.	kIx.	.
<g/>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
viděl	vidět	k5eAaImAgInS	vidět
potenciál	potenciál	k1gInSc1	potenciál
v	v	k7c6	v
něčem	něco	k3yInSc6	něco
úplně	úplně	k6eAd1	úplně
jiném	jiný	k2eAgMnSc6d1	jiný
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
dvorní	dvorní	k2eAgInSc1d1	dvorní
balet	balet	k1gInSc1	balet
či	či	k8xC	či
baletní	baletní	k2eAgFnSc1d1	baletní
komedie	komedie	k1gFnSc1	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Perrin	Perrin	k1gInSc1	Perrin
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
libretista	libretista	k1gMnSc1	libretista
získal	získat	k5eAaPmAgMnS	získat
privilegium	privilegium	k1gNnSc4	privilegium
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
Académie	Académie	k1gFnSc2	Académie
royale	royala	k1gFnSc3	royala
de	de	k?	de
musique	musique	k1gFnSc1	musique
(	(	kIx(	(
<g/>
Královská	královský	k2eAgFnSc1d1	královská
hudební	hudební	k2eAgFnSc1d1	hudební
akademie	akademie	k1gFnSc1	akademie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
produkce	produkce	k1gFnSc1	produkce
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
po	po	k7c6	po
počátečních	počáteční	k2eAgFnPc6d1	počáteční
nejistotách	nejistota	k1gFnPc6	nejistota
velice	velice	k6eAd1	velice
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přitáhlo	přitáhnout	k5eAaPmAgNnS	přitáhnout
Lullyho	Lully	k1gMnSc4	Lully
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
;	;	kIx,	;
zpočátku	zpočátku	k6eAd1	zpočátku
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
opera	opera	k1gFnSc1	opera
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
je	být	k5eAaImIp3nS	být
nesmysl	nesmysl	k1gInSc1	nesmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
změnil	změnit	k5eAaPmAgInS	změnit
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
když	když	k8xS	když
viděl	vidět	k5eAaImAgMnS	vidět
zájem	zájem	k1gInSc4	zájem
krále	král	k1gMnSc2	král
i	i	k8xC	i
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Tragédie	tragédie	k1gFnSc1	tragédie
en	en	k?	en
musique	musique	k1gFnPc2	musique
a	a	k8xC	a
Quinault	Quinaulta	k1gFnPc2	Quinaulta
====	====	k?	====
</s>
</p>
<p>
<s>
Perrin	Perrin	k1gInSc1	Perrin
dostal	dostat	k5eAaPmAgInS	dostat
výhradní	výhradní	k2eAgNnSc4d1	výhradní
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
uvádění	uvádění	k1gNnSc6	uvádění
oper	opera	k1gFnPc2	opera
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gMnSc7	jeho
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Robert	Robert	k1gMnSc1	Robert
Cambert	Cambert	k1gMnSc1	Cambert
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
sdílel	sdílet	k5eAaImAgMnS	sdílet
post	post	k1gInSc4	post
ředitele	ředitel	k1gMnSc2	ředitel
Académie	Académie	k1gFnSc2	Académie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
společná	společný	k2eAgFnSc1d1	společná
opera	opera	k1gFnSc1	opera
Pomone	Pomon	k1gInSc5	Pomon
(	(	kIx(	(
<g/>
Pomona	Pomona	k1gFnSc1	Pomona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1671	[number]	k4	1671
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
počin	počin	k1gInSc1	počin
Les	les	k1gInSc1	les
peines	peines	k1gInSc1	peines
et	et	k?	et
plaisirs	plaisirs	k1gInSc1	plaisirs
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
amour	amour	k1gMnSc1	amour
sice	sice	k8xC	sice
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
značný	značný	k2eAgInSc4d1	značný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Lullyho	Lully	k1gMnSc4	Lully
nátlak	nátlak	k1gInSc4	nátlak
na	na	k7c4	na
krále	král	k1gMnPc4	král
a	a	k8xC	a
bezohledné	bezohledný	k2eAgNnSc4d1	bezohledné
chování	chování	k1gNnSc4	chování
Perrinových	Perrinová	k1gFnPc2	Perrinová
dvou	dva	k4xCgMnPc2	dva
dalších	další	k2eAgMnPc2d1	další
partnerů	partner	k1gMnPc2	partner
<g/>
,	,	kIx,	,
spekulantů	spekulant	k1gMnPc2	spekulant
markýze	markýz	k1gMnSc2	markýz
de	de	k?	de
Sourdéac	Sourdéac	k1gFnSc4	Sourdéac
a	a	k8xC	a
sira	sir	k1gMnSc4	sir
de	de	k?	de
Champeron	Champeron	k1gMnSc1	Champeron
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Perrin	Perrin	k1gInSc1	Perrin
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
pro	pro	k7c4	pro
dlužníky	dlužník	k1gMnPc4	dlužník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
jej	on	k3xPp3gMnSc4	on
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Lully	Lull	k1gMnPc4	Lull
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
mu	on	k3xPp3gMnSc3	on
pomoc	pomoc	k1gFnSc4	pomoc
(	(	kIx(	(
<g/>
doživotní	doživotní	k2eAgFnSc4d1	doživotní
rentu	renta	k1gFnSc4	renta
<g/>
)	)	kIx)	)
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
převedení	převedení	k1gNnSc4	převedení
privilegií	privilegium	k1gNnPc2	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1672	[number]	k4	1672
schválil	schválit	k5eAaPmAgMnS	schválit
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
převedení	převedení	k1gNnSc1	převedení
privilegia	privilegium	k1gNnSc2	privilegium
pro	pro	k7c4	pro
uvádění	uvádění	k1gNnSc4	uvádění
oper	opera	k1gFnPc2	opera
na	na	k7c4	na
Lullyho	Lully	k1gMnSc4	Lully
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
převzal	převzít	k5eAaPmAgInS	převzít
Perrinovu	Perrinův	k2eAgFnSc4d1	Perrinova
a	a	k8xC	a
Cambertovu	Cambertův	k2eAgFnSc4d1	Cambertův
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
ředitelem	ředitel	k1gMnSc7	ředitel
Académie	Académie	k1gFnSc2	Académie
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zavedl	zavést	k5eAaPmAgMnS	zavést
profesionální	profesionální	k2eAgMnSc1d1	profesionální
a	a	k8xC	a
přísný	přísný	k2eAgInSc1d1	přísný
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
i	i	k9	i
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
osobním	osobní	k2eAgInSc7d1	osobní
životem	život	k1gInSc7	život
členů	člen	k1gMnPc2	člen
souboru	soubor	k1gInSc2	soubor
–	–	k?	–
ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
nemohli	moct	k5eNaImAgMnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
opilství	opilství	k1gNnSc4	opilství
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
také	také	k9	také
zavedl	zavést	k5eAaPmAgMnS	zavést
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
obsazování	obsazování	k1gNnSc4	obsazování
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
tanečních	taneční	k2eAgFnPc6d1	taneční
rolích	role	k1gFnPc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc4d1	následující
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
Perrinovi	Perrinův	k2eAgMnPc1d1	Perrinův
společníci	společník	k1gMnPc1	společník
snažili	snažit	k5eAaImAgMnP	snažit
o	o	k7c4	o
právní	právní	k2eAgNnSc4d1	právní
zpochybnění	zpochybnění	k1gNnSc4	zpochybnění
převodu	převod	k1gInSc2	převod
privilegií	privilegium	k1gNnPc2	privilegium
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
privilegia	privilegium	k1gNnSc2	privilegium
se	se	k3xPyFc4	se
Lully	Lulla	k1gFnSc2	Lulla
stal	stát	k5eAaPmAgMnS	stát
doslova	doslova	k6eAd1	doslova
francouzským	francouzský	k2eAgMnSc7d1	francouzský
operním	operní	k2eAgMnSc7d1	operní
monarchou	monarcha	k1gMnSc7	monarcha
<g/>
;	;	kIx,	;
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gInSc2	jeho
souhlasu	souhlas	k1gInSc2	souhlas
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
veřejně	veřejně	k6eAd1	veřejně
uvedena	uvést	k5eAaPmNgFnS	uvést
žádná	žádný	k3yNgFnSc1	žádný
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
řadu	řada	k1gFnSc4	řada
mocných	mocný	k2eAgMnPc2d1	mocný
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1674	[number]	k4	1674
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
proveden	proveden	k2eAgInSc1d1	proveden
pokus	pokus	k1gInSc1	pokus
ho	on	k3xPp3gMnSc4	on
otrávit	otrávit	k5eAaPmF	otrávit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jakožto	jakožto	k8xS	jakožto
důvěrný	důvěrný	k2eAgMnSc1d1	důvěrný
přítel	přítel	k1gMnSc1	přítel
krále	král	k1gMnSc4	král
měl	mít	k5eAaImAgMnS	mít
zajištěnu	zajištěn	k2eAgFnSc4d1	zajištěna
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
<g/>
Tragédie	tragédie	k1gFnSc1	tragédie
en	en	k?	en
musique	musiquat	k5eAaPmIp3nS	musiquat
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
pozdějším	pozdní	k2eAgInSc7d2	pozdější
názvem	název	k1gInSc7	název
tragédie	tragédie	k1gFnSc2	tragédie
lyrique	lyriqu	k1gFnSc2	lyriqu
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
typ	typ	k1gInSc4	typ
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
uvedl	uvést	k5eAaPmAgMnS	uvést
Lully	Lulla	k1gFnSc2	Lulla
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Philippe	Philipp	k1gInSc5	Philipp
Quinaultem	Quinault	k1gInSc7	Quinault
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
rozčleněn	rozčlenit	k5eAaPmNgInS	rozčlenit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
dějství	dějství	k1gNnPc2	dějství
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
předchází	předcházet	k5eAaImIp3nS	předcházet
prolog	prolog	k1gInSc1	prolog
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
alegorií	alegorie	k1gFnSc7	alegorie
na	na	k7c4	na
aktuální	aktuální	k2eAgFnSc4d1	aktuální
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
největšího	veliký	k2eAgInSc2d3	veliký
z	z	k7c2	z
hrdinů	hrdina	k1gMnPc2	hrdina
–	–	k?	–
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc7	první
tragédií	tragédie	k1gFnSc7	tragédie
en	en	k?	en
musique	musique	k1gInSc4	musique
<g/>
,	,	kIx,	,
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1673	[number]	k4	1673
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
opera	opera	k1gFnSc1	opera
Cadmus	Cadmus	k1gMnSc1	Cadmus
et	et	k?	et
Hermione	Hermion	k1gInSc5	Hermion
(	(	kIx(	(
<g/>
Kadmos	Kadmosa	k1gFnPc2	Kadmosa
a	a	k8xC	a
Harmonia	harmonium	k1gNnSc2	harmonium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
první	první	k4xOgFnSc4	první
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
sklidili	sklidit	k5eAaPmAgMnP	sklidit
Lully	Lulla	k1gFnPc4	Lulla
s	s	k7c7	s
Quinaultem	Quinault	k1gInSc7	Quinault
velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
spolupráce	spolupráce	k1gFnSc1	spolupráce
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
uvedl	uvést	k5eAaPmAgMnS	uvést
Lully	Lulla	k1gFnSc2	Lulla
každoročně	každoročně	k6eAd1	každoročně
jednu	jeden	k4xCgFnSc4	jeden
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
roku	rok	k1gInSc2	rok
1681	[number]	k4	1681
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nenapsal	napsat	k5eNaPmAgMnS	napsat
žádnou	žádný	k3yNgFnSc4	žádný
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1686	[number]	k4	1686
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sepsal	sepsat	k5eAaPmAgMnS	sepsat
opery	opera	k1gFnSc2	opera
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
opera	opera	k1gFnSc1	opera
Alceste	Alcest	k1gInSc5	Alcest
<g/>
,	,	kIx,	,
ou	ou	k0	ou
Le	Le	k1gFnPc3	Le
triomphe	triomphe	k1gInSc1	triomphe
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Alcide	Alcid	k1gInSc5	Alcid
(	(	kIx(	(
<g/>
Alcesta	Alcesta	k1gFnSc1	Alcesta
aneb	aneb	k?	aneb
Triumf	triumf	k1gInSc1	triumf
Alcidy	Alcida	k1gFnSc2	Alcida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1675	[number]	k4	1675
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
Thésée	Thésée	k1gNnSc1	Thésée
(	(	kIx(	(
<g/>
Théseus	Théseus	k1gInSc1	Théseus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1676	[number]	k4	1676
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
premiéra	premiéra	k1gFnSc1	premiéra
opery	opera	k1gFnSc2	opera
Atys	Atysa	k1gFnPc2	Atysa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
známou	známá	k1gFnSc4	známá
pod	pod	k7c7	pod
přízviskem	přízvisko	k1gNnSc7	přízvisko
královská	královský	k2eAgFnSc1d1	královská
opera	opera	k1gFnSc1	opera
<g/>
;	;	kIx,	;
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejoblíbenější	oblíbený	k2eAgFnSc4d3	nejoblíbenější
operu	opera	k1gFnSc4	opera
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
autoři	autor	k1gMnPc1	autor
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
s	s	k7c7	s
Atysem	Atys	k1gInSc7	Atys
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
zopakovat	zopakovat	k5eAaPmF	zopakovat
i	i	k9	i
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
operou	opera	k1gFnSc7	opera
Isis	Isis	k1gFnSc1	Isis
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
provedení	provedení	k1gNnSc1	provedení
vystoupaly	vystoupat	k5eAaPmAgFnP	vystoupat
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
výše	výše	k1gFnSc2	výše
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Atyse	Atyse	k1gFnSc1	Atyse
(	(	kIx(	(
<g/>
suma	suma	k1gFnSc1	suma
vyplacená	vyplacený	k2eAgFnSc1d1	vyplacená
na	na	k7c4	na
premiéru	premiéra	k1gFnSc4	premiéra
Isis	Isis	k1gFnSc1	Isis
činila	činit	k5eAaImAgFnS	činit
151	[number]	k4	151
780	[number]	k4	780
livrů	livr	k1gInPc2	livr
60	[number]	k4	60
sous	sousa	k1gFnPc2	sousa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudebně	hudebně	k6eAd1	hudebně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Lullyho	Lully	k1gMnSc4	Lully
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
operu	opera	k1gFnSc4	opera
(	(	kIx(	(
<g/>
získala	získat	k5eAaPmAgFnS	získat
přízvisko	přízvisko	k1gNnSc4	přízvisko
opera	opera	k1gFnSc1	opera
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
zcela	zcela	k6eAd1	zcela
propadla	propadnout	k5eAaPmAgFnS	propadnout
kvůli	kvůli	k7c3	kvůli
skandálu	skandál	k1gInSc3	skandál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
její	její	k3xOp3gFnSc4	její
uvedení	uvedení	k1gNnSc1	uvedení
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Atyse	Atyse	k1gFnSc2	Atyse
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
veřejné	veřejný	k2eAgFnPc1d1	veřejná
i	i	k8xC	i
soukromé	soukromý	k2eAgFnPc1d1	soukromá
kritiky	kritika	k1gFnPc1	kritika
libretisty	libretista	k1gMnSc2	libretista
Philippa	Philipp	k1gMnSc2	Philipp
Quinaulta	Quinault	k1gMnSc2	Quinault
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
kulminovaly	kulminovat	k5eAaImAgFnP	kulminovat
právě	právě	k6eAd1	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
premiéry	premiéra	k1gFnSc2	premiéra
Isis	Isis	k1gFnSc1	Isis
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
tedy	tedy	k9	tedy
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jinak	jinak	k6eAd1	jinak
konvenční	konvenční	k2eAgFnSc6d1	konvenční
zápletce	zápletka	k1gFnSc6	zápletka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
postavě	postava	k1gFnSc6	postava
nesympatické	sympatický	k2eNgFnSc6d1	nesympatická
<g/>
,	,	kIx,	,
chorobně	chorobně	k6eAd1	chorobně
žárlivé	žárlivý	k2eAgFnPc1d1	žárlivá
Junony	Juno	k1gFnPc1	Juno
spatřovány	spatřován	k2eAgInPc4d1	spatřován
rysy	rys	k1gInPc4	rys
madame	madame	k1gFnSc2	madame
de	de	k?	de
Montespan	Montespan	k1gInSc1	Montespan
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
rivalka	rivalka	k1gFnSc1	rivalka
<g/>
,	,	kIx,	,
markýza	markýza	k1gFnSc1	markýza
de	de	k?	de
Ludres	Ludresa	k1gFnPc2	Ludresa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
čase	čas	k1gInSc6	čas
milenkou	milenka	k1gFnSc7	milenka
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rozpoznána	rozpoznat	k5eAaPmNgFnS	rozpoznat
v	v	k7c6	v
postavě	postava	k1gFnSc6	postava
Íó	Íó	k1gFnSc1	Íó
/	/	kIx~	/
Isis	Isis	k1gFnSc1	Isis
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
král	král	k1gMnSc1	král
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
přirozeně	přirozeně	k6eAd1	přirozeně
ztotožňován	ztotožňován	k2eAgMnSc1d1	ztotožňován
s	s	k7c7	s
Jupiterem	Jupiter	k1gMnSc7	Jupiter
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
pojetí	pojetí	k1gNnSc1	pojetí
také	také	k6eAd1	také
nebylo	být	k5eNaImAgNnS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
lichotivé	lichotivý	k2eAgNnSc1d1	lichotivé
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
belgického	belgický	k2eAgMnSc2d1	belgický
historika	historik	k1gMnSc2	historik
Manuela	Manuel	k1gMnSc2	Manuel
Couvreura	Couvreur	k1gMnSc2	Couvreur
je	být	k5eAaImIp3nS	být
Jupiter	Jupiter	k1gMnSc1	Jupiter
v	v	k7c6	v
Isis	Isis	k1gFnSc1	Isis
"	"	kIx"	"
<g/>
karikaturou	karikatura	k1gFnSc7	karikatura
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
směšnou	směšný	k2eAgFnSc7d1	směšná
postavou	postava	k1gFnSc7	postava
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
výklad	výklad	k1gInSc4	výklad
skutečně	skutečně	k6eAd1	skutečně
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
<g/>
,	,	kIx,	,
dokládá	dokládat	k5eAaImIp3nS	dokládat
například	například	k6eAd1	například
dobová	dobový	k2eAgFnSc1d1	dobová
korespondence	korespondence	k1gFnSc1	korespondence
madame	madame	k1gFnSc2	madame
de	de	k?	de
Sévigné	Sévigná	k1gFnSc2	Sévigná
<g/>
,	,	kIx,	,
a	a	k8xC	a
jak	jak	k8xS	jak
informoval	informovat	k5eAaBmAgMnS	informovat
Svatého	svatý	k2eAgMnSc4d1	svatý
otce	otec	k1gMnSc4	otec
apoštolský	apoštolský	k2eAgMnSc1d1	apoštolský
nuncius	nuncius	k1gMnSc1	nuncius
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zalíbení	zalíbení	k1gNnSc4	zalíbení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
madame	madame	k1gFnSc1	madame
de	de	k?	de
Montespan	Montespan	k1gMnSc1	Montespan
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
opeře	opera	k1gFnSc6	opera
nalezla	naleznout	k5eAaPmAgFnS	naleznout
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
natolik	natolik	k6eAd1	natolik
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g />
.	.	kIx.	.
</s>
<s>
ji	on	k3xPp3gFnSc4	on
zhlédla	zhlédnout	k5eAaPmAgFnS	zhlédnout
jen	jen	k9	jen
několikrát	několikrát	k6eAd1	několikrát
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnPc4	některý
situace	situace	k1gFnPc4	situace
zápletky	zápletka	k1gFnSc2	zápletka
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
neoceňuje	oceňovat	k5eNaImIp3nS	oceňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
je	on	k3xPp3gInPc4	on
shledává	shledávat	k5eAaImIp3nS	shledávat
nepříjemnými	příjemný	k2eNgMnPc7d1	nepříjemný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Kvůli	kvůli	k7c3	kvůli
domnělým	domnělý	k2eAgFnPc3d1	domnělá
narážkám	narážka	k1gFnPc3	narážka
v	v	k7c6	v
textu	text	k1gInSc6	text
na	na	k7c4	na
skutečné	skutečný	k2eAgFnPc4d1	skutečná
osoby	osoba	k1gFnPc4	osoba
ztratil	ztratit	k5eAaPmAgMnS	ztratit
libretista	libretista	k1gMnSc1	libretista
Quinault	Quinault	k1gMnSc1	Quinault
dočasně	dočasně	k6eAd1	dočasně
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
Lully	Lulla	k1gFnPc4	Lulla
najít	najít	k5eAaPmF	najít
jiného	jiný	k2eAgMnSc4d1	jiný
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Lully	Lull	k1gMnPc4	Lull
také	také	k9	také
propustil	propustit	k5eAaPmAgMnS	propustit
svého	svůj	k3xOyFgMnSc2	svůj
pomocného	pomocný	k2eAgMnSc2d1	pomocný
dirigenta	dirigent	k1gMnSc2	dirigent
Jeana-Françoise	Jeana-Françoise	k1gFnSc2	Jeana-Françoise
Lallouetta	Lallouett	k1gMnSc2	Lallouett
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
vychloubal	vychloubat	k5eAaImAgMnS	vychloubat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
částí	část	k1gFnPc2	část
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
uvedl	uvést	k5eAaPmAgInS	uvést
Lully	Lull	k1gInPc7	Lull
operu	opera	k1gFnSc4	opera
Psyché	psyché	k1gFnSc2	psyché
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
jako	jako	k8xS	jako
libreto	libreto	k1gNnSc4	libreto
použil	použít	k5eAaPmAgInS	použít
přepracovaný	přepracovaný	k2eAgInSc1d1	přepracovaný
text	text	k1gInSc1	text
z	z	k7c2	z
dřívější	dřívější	k2eAgFnSc2d1	dřívější
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Moliè	Moliè	k1gMnSc1	Moliè
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Quinaultem	Quinault	k1gInSc7	Quinault
a	a	k8xC	a
P.	P.	kA	P.
Corneillem	Corneillo	k1gNnSc7	Corneillo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1671	[number]	k4	1671
<g/>
;	;	kIx,	;
úpravu	úprava	k1gFnSc4	úprava
textu	text	k1gInSc2	text
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
provedl	provést	k5eAaPmAgMnS	provést
Thomas	Thomas	k1gMnSc1	Thomas
Corneille	Corneille	k1gFnSc2	Corneille
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgNnSc4d1	nové
libreto	libreto	k1gNnSc4	libreto
Lully	Lulla	k1gFnSc2	Lulla
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1679	[number]	k4	1679
<g/>
)	)	kIx)	)
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Bellérophona	Bellérophon	k1gMnSc2	Bellérophon
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
opět	opět	k6eAd1	opět
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Thomas	Thomas	k1gMnSc1	Thomas
Corneille	Corneille	k1gFnSc2	Corneille
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bernardem	Bernard	k1gMnSc7	Bernard
de	de	k?	de
Fontenelle	Fontenelle	k1gFnSc1	Fontenelle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1680	[number]	k4	1680
se	se	k3xPyFc4	se
již	již	k6eAd1	již
mohl	moct	k5eAaImAgInS	moct
Phillippe	Phillipp	k1gInSc5	Phillipp
Quinault	Quinault	k2eAgInSc1d1	Quinault
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
veřejně	veřejně	k6eAd1	veřejně
"	"	kIx"	"
<g/>
se	s	k7c7	s
slzami	slza	k1gFnPc7	slza
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
<g/>
"	"	kIx"	"
požádal	požádat	k5eAaPmAgInS	požádat
krále	král	k1gMnPc4	král
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
tak	tak	k6eAd1	tak
opět	opět	k6eAd1	opět
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
Lullym	Lullymum	k1gNnPc2	Lullymum
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nová	nový	k2eAgFnSc1d1	nová
opera	opera	k1gFnSc1	opera
Proserpine	Proserpin	k1gInSc5	Proserpin
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vzorovou	vzorový	k2eAgFnSc7d1	vzorová
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
tragédií	tragédie	k1gFnSc7	tragédie
en	en	k?	en
musique	musiqu	k1gFnSc2	musiqu
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovině	polovina	k1gFnSc3	polovina
celkové	celkový	k2eAgFnSc2d1	celková
délky	délka	k1gFnSc2	délka
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
opera	opera	k1gFnSc1	opera
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
bohatou	bohatý	k2eAgFnSc7d1	bohatá
orchestrací	orchestrace	k1gFnSc7	orchestrace
<g/>
.	.	kIx.	.
<g/>
Až	až	k9	až
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1682	[number]	k4	1682
<g/>
)	)	kIx)	)
uvedl	uvést	k5eAaPmAgMnS	uvést
Lully	Lull	k1gInPc4	Lull
s	s	k7c7	s
Quinaultem	Quinault	k1gInSc7	Quinault
své	svůj	k3xOyFgNnSc4	svůj
další	další	k2eAgNnSc4d1	další
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
operu	opera	k1gFnSc4	opera
Persée	Persé	k1gFnSc2	Persé
(	(	kIx(	(
<g/>
Perseus	Perseus	k1gMnSc1	Perseus
<g/>
)	)	kIx)	)
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Palais-Royal	Palais-Royal	k1gInSc1	Palais-Royal
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Saint-Honoré	Saint-Honorý	k2eAgFnSc6d1	Saint-Honorý
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
narození	narození	k1gNnSc2	narození
Ludvíka	Ludvík	k1gMnSc4	Ludvík
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
burgundského	burgundský	k2eAgMnSc2d1	burgundský
<g/>
,	,	kIx,	,
vnuka	vnuk	k1gMnSc2	vnuk
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgInS	přesunout
královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
do	do	k7c2	do
zámku	zámek	k1gInSc2	zámek
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
premiéra	premiéra	k1gFnSc1	premiéra
opery	opera	k1gFnSc2	opera
Phaëton	Phaëton	k1gInSc1	Phaëton
(	(	kIx(	(
<g/>
Faethón	Faethón	k1gMnSc1	Faethón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
známou	známá	k1gFnSc4	známá
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
lidová	lidový	k2eAgFnSc1d1	lidová
opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Opéra	Opéra	k1gMnSc1	Opéra
du	du	k?	du
Peuple	Peuple	k1gMnSc1	Peuple
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
Amadis	Amadis	k1gFnSc2	Amadis
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
námět	námět	k1gInSc4	námět
vybral	vybrat	k5eAaPmAgMnS	vybrat
sám	sám	k3xTgMnSc1	sám
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vymyká	vymykat	k5eAaImIp3nS	vymykat
klasickému	klasický	k2eAgInSc3d1	klasický
modelu	model	k1gInSc3	model
tragédie	tragédie	k1gFnSc2	tragédie
en	en	k?	en
musique	musique	k1gInSc1	musique
<g/>
;	;	kIx,	;
námět	námět	k1gInSc1	námět
se	se	k3xPyFc4	se
neopírá	opírat	k5eNaImIp3nS	opírat
o	o	k7c4	o
římskou	římský	k2eAgFnSc4d1	římská
mytologii	mytologie	k1gFnSc4	mytologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
rytířský	rytířský	k2eAgInSc4d1	rytířský
román	román	k1gInSc4	román
Amadís	Amadís	k1gInSc1	Amadís
Waleský	waleský	k2eAgInSc1d1	waleský
<g/>
;	;	kIx,	;
Amadis	Amadis	k1gInSc1	Amadis
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1684	[number]	k4	1684
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
árie	árie	k1gFnSc1	árie
Bois	Boisa	k1gFnPc2	Boisa
épais	épais	k1gInSc4	épais
z	z	k7c2	z
opery	opera	k1gFnSc2	opera
Amadis	Amadis	k1gFnSc2	Amadis
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
uváděna	uvádět	k5eAaImNgFnS	uvádět
samostatně	samostatně	k6eAd1	samostatně
jako	jako	k8xC	jako
typická	typický	k2eAgFnSc1d1	typická
ukázka	ukázka	k1gFnSc1	ukázka
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
Lullyho	Lully	k1gMnSc2	Lully
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
monologových	monologový	k2eAgFnPc2d1	monologový
árií	árie	k1gFnPc2	árie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1685	[number]	k4	1685
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
Roland	Roland	k1gInSc1	Roland
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Amadis	Amadis	k1gFnSc1	Amadis
není	být	k5eNaImIp3nS	být
dějově	dějově	k6eAd1	dějově
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
Ariostově	Ariostův	k2eAgFnSc6d1	Ariostův
básni	báseň	k1gFnSc6	báseň
Orlando	Orlanda	k1gFnSc5	Orlanda
furioso	furioso	k1gNnSc1	furioso
(	(	kIx(	(
<g/>
Zuřivý	zuřivý	k2eAgInSc1d1	zuřivý
Roland	Roland	k1gInSc1	Roland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1686	[number]	k4	1686
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
Armide	Armid	k1gInSc5	Armid
(	(	kIx(	(
<g/>
Armida	Armida	k1gFnSc1	Armida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
Lullyho	Lully	k1gMnSc2	Lully
mistrovské	mistrovský	k2eAgNnSc1d1	mistrovské
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
oper	opera	k1gFnPc2	opera
Amadis	Amadis	k1gFnPc2	Amadis
a	a	k8xC	a
Roland	Rolanda	k1gFnPc2	Rolanda
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
Armidy	Armida	k1gFnSc2	Armida
neopírá	opírat	k5eNaImIp3nS	opírat
o	o	k7c4	o
předlohu	předloha	k1gFnSc4	předloha
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
Tassův	Tassův	k2eAgInSc4d1	Tassův
epos	epos	k1gInSc4	epos
La	la	k1gNnSc2	la
Gerusalemme	Gerusalemme	k1gMnSc2	Gerusalemme
liberata	liberat	k1gMnSc2	liberat
(	(	kIx(	(
<g/>
Osvobozený	osvobozený	k2eAgInSc1d1	osvobozený
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nikdy	nikdy	k6eAd1	nikdy
Armidu	Armida	k1gFnSc4	Armida
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
Lully	Lulla	k1gMnSc2	Lulla
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgFnPc3	svůj
homosexuálním	homosexuální	k2eAgFnPc3d1	homosexuální
aférám	aféra	k1gFnPc3	aféra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
roku	rok	k1gInSc2	rok
1685	[number]	k4	1685
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Madame	madame	k1gFnSc1	madame
de	de	k?	de
Maintenon	Maintenona	k1gFnPc2	Maintenona
<g/>
,	,	kIx,	,
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
oblíbeného	oblíbený	k2eAgMnSc2d1	oblíbený
skladatele	skladatel	k1gMnSc2	skladatel
distancoval	distancovat	k5eAaBmAgInS	distancovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
od	od	k7c2	od
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
<g/>
Lullyho	Lully	k1gMnSc2	Lully
poslední	poslední	k2eAgFnSc1d1	poslední
opera	opera	k1gFnSc1	opera
Achille	Achilles	k1gMnSc5	Achilles
et	et	k?	et
Polyxè	Polyxè	k1gMnSc1	Polyxè
(	(	kIx(	(
<g/>
Achilles	Achilles	k1gMnSc1	Achilles
a	a	k8xC	a
Polyxena	Polyxena	k1gFnSc1	Polyxena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
začal	začít	k5eAaPmAgInS	začít
skládat	skládat	k5eAaImF	skládat
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
úspěchu	úspěch	k1gInSc6	úspěch
Armidy	Armida	k1gFnSc2	Armida
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
úspěch	úspěch	k1gInSc4	úspěch
navázat	navázat	k5eAaPmF	navázat
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc4	libreto
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Jean	Jean	k1gMnSc1	Jean
Galbert	Galbert	k1gMnSc1	Galbert
de	de	k?	de
Campistron	Campistron	k1gInSc1	Campistron
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
pro	pro	k7c4	pro
Lullyho	Lully	k1gMnSc4	Lully
sepsal	sepsat	k5eAaPmAgMnS	sepsat
libreto	libreto	k1gNnSc4	libreto
již	již	k6eAd1	již
předchozí	předchozí	k2eAgInSc1d1	předchozí
rok	rok	k1gInSc1	rok
k	k	k7c3	k
pastorale	pastorale	k6eAd1	pastorale
héroï	héroï	k?	héroï
Acis	Acis	k1gInSc1	Acis
et	et	k?	et
Galatée	Galatée	k1gInSc1	Galatée
(	(	kIx(	(
<g/>
Ákis	Ákis	k1gInSc1	Ákis
a	a	k8xC	a
Galateia	Galateia	k1gFnSc1	Galateia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lully	Lulla	k1gFnPc4	Lulla
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
nestihl	stihnout	k5eNaPmAgMnS	stihnout
operu	opera	k1gFnSc4	opera
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
,	,	kIx,	,
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
jí	jíst	k5eAaImIp3nS	jíst
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
po	po	k7c4	po
Lullyho	Lully	k1gMnSc4	Lully
smrti	smrt	k1gFnSc2	smrt
dokončil	dokončit	k5eAaPmAgMnS	dokončit
jeho	jeho	k3xOp3gMnSc1	jeho
pomocný	pomocný	k2eAgMnSc1d1	pomocný
dirigent	dirigent	k1gMnSc1	dirigent
Pascal	Pascal	k1gMnSc1	Pascal
Collasse	Collasse	k1gFnSc1	Collasse
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
pracoval	pracovat	k5eAaImAgMnS	pracovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1677	[number]	k4	1677
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
neměl	mít	k5eNaImAgInS	mít
přílišný	přílišný	k2eAgInSc1d1	přílišný
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
brzy	brzy	k6eAd1	brzy
stažena	stažen	k2eAgFnSc1d1	stažena
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
Lully	Lulla	k1gFnSc2	Lulla
prakticky	prakticky	k6eAd1	prakticky
jediným	jediný	k2eAgMnSc7d1	jediný
autorem	autor	k1gMnSc7	autor
oper	opera	k1gFnPc2	opera
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
tedy	tedy	k9	tedy
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
národní	národní	k2eAgInSc4d1	národní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgMnPc2d1	další
evropských	evropský	k2eAgMnPc2d1	evropský
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
pro	pro	k7c4	pro
Rameaua	Rameauus	k1gMnSc4	Rameauus
či	či	k8xC	či
Purcella	Purcell	k1gMnSc4	Purcell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
Lully	Lull	k1gMnPc4	Lull
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Francouzem	Francouz	k1gMnSc7	Francouz
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1661	[number]	k4	1661
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
naturalizační	naturalizační	k2eAgFnSc1d1	naturalizační
listina	listina	k1gFnSc1	listina
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
pofrancouzštělé	pofrancouzštělý	k2eAgNnSc1d1	pofrancouzštělý
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
de	de	k?	de
Lully	Lull	k1gMnPc7	Lull
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1662	[number]	k4	1662
se	se	k3xPyFc4	se
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Eustacha	Eustach	k1gMnSc2	Eustach
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Madeleine	Madeleine	k1gFnSc7	Madeleine
Lambert	Lambert	k1gMnSc1	Lambert
(	(	kIx(	(
<g/>
1642	[number]	k4	1642
<g/>
–	–	k?	–
<g/>
1720	[number]	k4	1720
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Michela	Michel	k1gMnSc2	Michel
Lamberta	Lambert	k1gMnSc2	Lambert
<g/>
,	,	kIx,	,
významného	významný	k2eAgMnSc2d1	významný
francouzského	francouzský	k2eAgMnSc2d1	francouzský
pěveckého	pěvecký	k2eAgMnSc2d1	pěvecký
mistra	mistr	k1gMnSc2	mistr
a	a	k8xC	a
theorbisty	theorbista	k1gMnSc2	theorbista
působícího	působící	k2eAgMnSc2d1	působící
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
a	a	k8xC	a
Lullyho	Lully	k1gMnSc4	Lully
blízkého	blízký	k2eAgMnSc4d1	blízký
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
a	a	k8xC	a
spoluautora	spoluautor	k1gMnSc4	spoluautor
<g/>
,	,	kIx,	,
a	a	k8xC	a
neteří	teřet	k5eNaImIp3nP	teřet
významné	významný	k2eAgFnPc4d1	významná
dvorní	dvorní	k2eAgFnPc4d1	dvorní
zpěvačky	zpěvačka	k1gFnPc4	zpěvačka
Hilaire	Hilair	k1gInSc5	Hilair
Dupuis	Dupuis	k1gInSc4	Dupuis
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
souhlas	souhlas	k1gInSc4	souhlas
se	s	k7c7	s
sňatkem	sňatek	k1gInSc7	sňatek
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
podpisem	podpis	k1gInSc7	podpis
svatební	svatební	k2eAgFnSc2d1	svatební
smlouvy	smlouva	k1gFnSc2	smlouva
i	i	k8xC	i
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
královny	královna	k1gFnSc2	královna
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
či	či	k8xC	či
ministr	ministr	k1gMnSc1	ministr
Colbert	Colbert	k1gMnSc1	Colbert
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
šest	šest	k4xCc4	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
narozené	narozený	k2eAgFnPc4d1	narozená
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
v	v	k7c6	v
letech	let	k1gInPc6	let
1663	[number]	k4	1663
<g/>
–	–	k?	–
<g/>
1668	[number]	k4	1668
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
synové	syn	k1gMnPc1	syn
(	(	kIx(	(
<g/>
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
<g/>
,	,	kIx,	,
Jean-Louis	Jean-Louis	k1gFnPc6	Jean-Louis
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
hudebníky	hudebník	k1gMnPc7	hudebník
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
<g/>
Lully	Lulla	k1gFnSc2	Lulla
předstíral	předstírat	k5eAaImAgMnS	předstírat
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
původ	původ	k1gInSc4	původ
již	již	k6eAd1	již
od	od	k7c2	od
příchodu	příchod	k1gInSc2	příchod
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
veřejně	veřejně	k6eAd1	veřejně
zpochybňován	zpochybňovat	k5eAaImNgInS	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
nepřímo	přímo	k6eNd1	přímo
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
když	když	k8xS	když
povolil	povolit	k5eAaPmAgInS	povolit
roku	rok	k1gInSc2	rok
1681	[number]	k4	1681
Lullymu	Lullym	k1gInSc2	Lullym
zakoupit	zakoupit	k5eAaPmF	zakoupit
hodnost	hodnost	k1gFnSc4	hodnost
sekretářem	sekretář	k1gMnSc7	sekretář
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
Sécrétaire	Sécrétair	k1gInSc5	Sécrétair
du	du	k?	du
roi	roi	k?	roi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
šlechtictvím	šlechtictví	k1gNnSc7	šlechtictví
a	a	k8xC	a
dávala	dávat	k5eAaImAgFnS	dávat
mu	on	k3xPp3gMnSc3	on
nárok	nárok	k1gInSc4	nárok
na	na	k7c6	na
oslovení	oslovení	k1gNnSc6	oslovení
Monsieur	Monsieur	k1gMnSc1	Monsieur
de	de	k?	de
Lully	Lulla	k1gFnSc2	Lulla
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
jako	jako	k9	jako
Monsieur	Monsieur	k1gMnSc1	Monsieur
de	de	k?	de
Lully	Lulla	k1gFnSc2	Lulla
<g/>
,	,	kIx,	,
escuyer	escuyer	k1gMnSc1	escuyer
<g/>
,	,	kIx,	,
conseiller	conseiller	k1gMnSc1	conseiller
<g/>
,	,	kIx,	,
Secrétaire	Secrétair	k1gInSc5	Secrétair
du	du	k?	du
Roy	Roy	k1gMnSc1	Roy
<g/>
,	,	kIx,	,
Maison	Maison	k1gMnSc1	Maison
<g/>
,	,	kIx,	,
Couronne	Couronn	k1gInSc5	Couronn
de	de	k?	de
France	Franc	k1gMnSc2	Franc
&	&	k?	&
de	de	k?	de
ses	ses	k?	ses
Finances	Finances	k1gInSc1	Finances
<g/>
,	,	kIx,	,
&	&	k?	&
Sur-Intendant	Sur-Intendant	k1gMnSc1	Sur-Intendant
de	de	k?	de
la	la	k1gNnPc2	la
Musique	Musique	k1gNnPc2	Musique
de	de	k?	de
sa	sa	k?	sa
Majesté	Majestý	k2eAgFnSc6d1	Majestý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozdější	pozdní	k2eAgFnSc1d2	pozdější
snaha	snaha	k1gFnSc1	snaha
zakoupit	zakoupit	k5eAaPmF	zakoupit
statek	statek	k1gInSc4	statek
a	a	k8xC	a
titul	titul	k1gInSc4	titul
hraběte	hrabě	k1gMnSc2	hrabě
de	de	k?	de
Grignon	Grignon	k1gInSc1	Grignon
vyšla	vyjít	k5eAaPmAgFnS	vyjít
naprázdno	naprázdno	k6eAd1	naprázdno
<g/>
.	.	kIx.	.
<g/>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
několik	několik	k4yIc4	několik
málo	málo	k6eAd1	málo
důvěrných	důvěrný	k2eAgInPc2d1	důvěrný
přátel	přítel	k1gMnPc2	přítel
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
měl	mít	k5eAaImAgMnS	mít
Lully	Lull	k1gInPc4	Lull
množství	množství	k1gNnSc2	množství
sexuálních	sexuální	k2eAgMnPc2d1	sexuální
i	i	k8xC	i
finančních	finanční	k2eAgInPc2d1	finanční
skandálů	skandál	k1gInPc2	skandál
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yIgInPc3	který
několikrát	několikrát	k6eAd1	několikrát
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
královu	králův	k2eAgFnSc4d1	králova
nemilost	nemilost	k1gFnSc4	nemilost
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
dokázal	dokázat	k5eAaPmAgMnS	dokázat
nalézt	nalézt	k5eAaPmF	nalézt
cestu	cesta	k1gFnSc4	cesta
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
královy	králův	k2eAgFnSc2d1	králova
přízně	přízeň	k1gFnSc2	přízeň
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gNnSc1	jejich
přátelství	přátelství	k1gNnSc1	přátelství
trvalo	trvat	k5eAaImAgNnS	trvat
téměř	téměř	k6eAd1	téměř
až	až	k9	až
do	do	k7c2	do
Lullyho	Lully	k1gMnSc2	Lully
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g />
.	.	kIx.	.
</s>
<s>
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
trvalejší	trvalý	k2eAgNnSc4d2	trvalejší
ochlazení	ochlazení	k1gNnSc4	ochlazení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
nedotklo	dotknout	k5eNaPmAgNnS	dotknout
Lullyho	Lully	k1gMnSc2	Lully
postavení	postavení	k1gNnSc1	postavení
u	u	k7c2	u
králova	králův	k2eAgInSc2d1	králův
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
<g/>
Lully	Lulla	k1gFnPc1	Lulla
se	se	k3xPyFc4	se
v	v	k7c6	v
intimní	intimní	k2eAgFnSc6d1	intimní
rovině	rovina	k1gFnSc6	rovina
projevoval	projevovat	k5eAaImAgMnS	projevovat
jako	jako	k9	jako
bisexuál	bisexuál	k1gMnSc1	bisexuál
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gInPc1	jeho
sklony	sklon	k1gInPc1	sklon
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
staly	stát	k5eAaPmAgFnP	stát
veřejným	veřejný	k2eAgNnSc7d1	veřejné
tajemstvím	tajemství	k1gNnSc7	tajemství
již	již	k6eAd1	již
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
sňatkem	sňatek	k1gInSc7	sňatek
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
svědčí	svědčit	k5eAaImIp3nS	svědčit
jarmareční	jarmareční	k2eAgFnSc1d1	jarmareční
píseň	píseň	k1gFnSc1	píseň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1662	[number]	k4	1662
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
skandál	skandál	k1gInSc1	skandál
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
propukl	propuknout	k5eAaPmAgInS	propuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1685	[number]	k4	1685
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšel	vyjít	k5eAaPmAgInS	vyjít
najevo	najevo	k6eAd1	najevo
Lullyho	Lully	k1gMnSc2	Lully
milenecký	milenecký	k2eAgInSc4d1	milenecký
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
pážetem	páže	k1gNnSc7	páže
jménem	jméno	k1gNnSc7	jméno
Brunet	brunet	k1gMnSc1	brunet
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
žil	žít	k5eAaImAgMnS	žít
coby	coby	k?	coby
student	student	k1gMnSc1	student
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
neváhal	váhat	k5eNaImAgMnS	váhat
dát	dát	k5eAaPmF	dát
najevo	najevo	k6eAd1	najevo
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
skladateli	skladatel	k1gMnSc3	skladatel
<g/>
.	.	kIx.	.
<g/>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
i	i	k9	i
Lully	Lulla	k1gFnPc4	Lulla
hlásil	hlásit	k5eAaImAgMnS	hlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
sodomitu	sodomita	k1gMnSc4	sodomita
a	a	k8xC	a
muže	muž	k1gMnPc4	muž
bludařských	bludařský	k2eAgInPc2d1	bludařský
sklonů	sklon	k1gInPc2	sklon
<g/>
.	.	kIx.	.
</s>
<s>
Výhrady	výhrada	k1gFnPc4	výhrada
církve	církev	k1gFnSc2	církev
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gMnSc3	on
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
spor	spor	k1gInSc1	spor
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Lully	Lulla	k1gFnSc2	Lulla
umíral	umírat	k5eAaImAgInS	umírat
<g/>
:	:	kIx,	:
kněz	kněz	k1gMnSc1	kněz
mu	on	k3xPp3gMnSc3	on
vyčetl	vyčíst	k5eAaPmAgMnS	vyčíst
zhýralý	zhýralý	k2eAgInSc4d1	zhýralý
život	život	k1gInSc4	život
a	a	k8xC	a
především	především	k6eAd1	především
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
poskytnout	poskytnout	k5eAaPmF	poskytnout
svaté	svatý	k2eAgNnSc4d1	svaté
přijímání	přijímání	k1gNnSc4	přijímání
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
neslíbí	slíbit	k5eNaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zanechá	zanechat	k5eAaPmIp3nS	zanechat
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Lullyho	Lully	k1gMnSc4	Lully
blízcí	blízký	k2eAgMnPc1d1	blízký
hrozili	hrozit	k5eAaImAgMnP	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
budou	být	k5eAaImBp3nP	být
stěžovat	stěžovat	k5eAaImF	stěžovat
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pařížský	pařížský	k2eAgMnSc1d1	pařížský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
François	François	k1gFnSc2	François
Harlay	Harlay	k1gInPc4	Harlay
de	de	k?	de
Champvallon	Champvallon	k1gInSc4	Champvallon
zpovědníkův	zpovědníkův	k2eAgInSc4d1	zpovědníkův
příkaz	příkaz	k1gInSc4	příkaz
schválil	schválit	k5eAaPmAgInS	schválit
a	a	k8xC	a
Lully	Lulla	k1gMnSc2	Lulla
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
podřídit	podřídit	k5eAaPmF	podřídit
<g/>
.	.	kIx.	.
</s>
<s>
Lullyho	Lullyze	k6eAd1	Lullyze
tvorba	tvorba	k1gFnSc1	tvorba
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
většině	většina	k1gFnSc3	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
barokních	barokní	k2eAgMnPc2d1	barokní
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
děl	dělo	k1gNnPc2	dělo
s	s	k7c7	s
duchovní	duchovní	k2eAgFnSc7d1	duchovní
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jde	jít	k5eAaImIp3nS	jít
většinou	většina	k1gFnSc7	většina
o	o	k7c4	o
dvorské	dvorský	k2eAgFnPc4d1	dvorská
zakázky	zakázka	k1gFnPc4	zakázka
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smrt	smrt	k1gFnSc1	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
operace	operace	k1gFnSc2	operace
análního	anální	k2eAgInSc2d1	anální
píštělu	píštěl	k1gInSc2	píštěl
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1686	[number]	k4	1686
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
následného	následný	k2eAgNnSc2d1	následné
uzdravení	uzdravení	k1gNnSc2	uzdravení
bylo	být	k5eAaImAgNnS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1687	[number]	k4	1687
znovu	znovu	k6eAd1	znovu
uvedeno	uvést	k5eAaPmNgNnS	uvést
Lullyho	Lully	k1gMnSc2	Lully
Te	Te	k1gMnSc1	Te
Deum	Deum	k1gMnSc1	Deum
v	v	k7c6	v
konventu	konvent	k1gInSc6	konvent
feuillantů	feuillant	k1gMnPc2	feuillant
<g/>
,	,	kIx,	,
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
více	hodně	k6eAd2	hodně
než	než	k8xS	než
150	[number]	k4	150
zpěváků	zpěvák	k1gMnPc2	zpěvák
a	a	k8xC	a
instrumentalistů	instrumentalista	k1gMnPc2	instrumentalista
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dirigování	dirigování	k1gNnSc6	dirigování
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
zkoušek	zkouška	k1gFnPc2	zkouška
se	se	k3xPyFc4	se
Lully	Lull	k1gInPc7	Lull
prudce	prudko	k6eAd1	prudko
udeřil	udeřit	k5eAaPmAgInS	udeřit
bodcem	bodec	k1gInSc7	bodec
těžké	těžký	k2eAgFnSc2d1	těžká
barokní	barokní	k2eAgFnSc2d1	barokní
taktovky	taktovka	k1gFnSc2	taktovka
do	do	k7c2	do
palce	palec	k1gInSc2	palec
pravé	pravý	k2eAgFnSc2d1	pravá
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
hygieny	hygiena	k1gFnSc2	hygiena
zanítil	zanítit	k5eAaPmAgMnS	zanítit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Lully	Lulla	k1gFnSc2	Lulla
odmítal	odmítat	k5eAaImAgMnS	odmítat
amputaci	amputace	k1gFnSc4	amputace
probodnutého	probodnutý	k2eAgInSc2d1	probodnutý
prstu	prst	k1gInSc2	prst
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
celé	celý	k2eAgFnPc4d1	celá
nohy	noha	k1gFnPc4	noha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
místní	místní	k2eAgFnSc1d1	místní
i	i	k8xC	i
z	z	k7c2	z
ciziny	cizina	k1gFnSc2	cizina
přivolaní	přivolaný	k2eAgMnPc1d1	přivolaný
lékaři	lékař	k1gMnPc1	lékař
radili	radit	k5eAaImAgMnP	radit
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zánět	zánět	k1gInSc1	zánět
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
otravě	otrava	k1gFnSc3	otrava
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zlepšení	zlepšení	k1gNnSc6	zlepšení
stavu	stav	k1gInSc2	stav
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
února	únor	k1gInSc2	únor
nastalo	nastat	k5eAaPmAgNnS	nastat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
měsíce	měsíc	k1gInSc2	měsíc
rychlé	rychlý	k2eAgNnSc4d1	rychlé
zhoršení	zhoršení	k1gNnSc4	zhoršení
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1687	[number]	k4	1687
k	k	k7c3	k
sobě	se	k3xPyFc3	se
Lully	Lull	k1gMnPc4	Lull
povolal	povolat	k5eAaPmAgMnS	povolat
kněze	kněz	k1gMnSc4	kněz
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
udělení	udělení	k1gNnSc4	udělení
posledního	poslední	k2eAgNnSc2d1	poslední
pomazání	pomazání	k1gNnSc2	pomazání
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
udělením	udělení	k1gNnSc7	udělení
pomazání	pomazání	k1gNnSc2	pomazání
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
pokání	pokání	k1gNnSc1	pokání
spálí	spálit	k5eAaPmIp3nS	spálit
rozpracovanou	rozpracovaný	k2eAgFnSc4d1	rozpracovaná
partituru	partitura	k1gFnSc4	partitura
své	svůj	k3xOyFgFnSc2	svůj
poslední	poslední	k2eAgFnSc2d1	poslední
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
Achilles	Achilles	k1gMnSc1	Achilles
a	a	k8xC	a
Polyxena	Polyxena	k1gFnSc1	Polyxena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lully	Lull	k1gMnPc4	Lull
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
partituru	partitura	k1gFnSc4	partitura
vhodil	vhodit	k5eAaPmAgInS	vhodit
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
kněze	kněz	k1gMnSc2	kněz
dotázán	dotázán	k2eAgMnSc1d1	dotázán
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
Lully	Lull	k1gInPc4	Lull
mu	on	k3xPp3gMnSc3	on
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
kopii	kopie	k1gFnSc4	kopie
partitury	partitura	k1gFnSc2	partitura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
<g/>
,	,	kIx,	,
po	po	k7c6	po
posledním	poslední	k2eAgNnSc6d1	poslední
pomazání	pomazání	k1gNnSc6	pomazání
<g/>
,	,	kIx,	,
složil	složit	k5eAaPmAgMnS	složit
Lully	Lulla	k1gFnSc2	Lulla
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
pětihlasý	pětihlasý	k2eAgInSc4d1	pětihlasý
kánon	kánon	k1gInSc4	kánon
Il	Il	k1gMnSc1	Il
faut	faut	k1gMnSc1	faut
mourir	mourir	k1gMnSc1	mourir
<g/>
,	,	kIx,	,
pécheur	pécheur	k1gMnSc1	pécheur
(	(	kIx(	(
<g/>
Umíráš	umírat	k5eAaImIp2nS	umírat
<g/>
,	,	kIx,	,
hříšníku	hříšník	k1gMnSc5	hříšník
<g/>
,	,	kIx,	,
umíráš	umírat	k5eAaImIp2nS	umírat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lulla	k1gFnPc1	Lulla
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
vůli	vůle	k1gFnSc4	vůle
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnSc6d1	jiná
odkázal	odkázat	k5eAaPmAgInS	odkázat
své	svůj	k3xOyFgNnSc4	svůj
operní	operní	k2eAgNnSc4d1	operní
privilegium	privilegium	k1gNnSc4	privilegium
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
a	a	k8xC	a
potomkům	potomek	k1gMnPc3	potomek
a	a	k8xC	a
svěřil	svěřit	k5eAaPmAgMnS	svěřit
řízení	řízení	k1gNnSc4	řízení
Královské	královský	k2eAgFnSc2d1	královská
hudební	hudební	k2eAgFnSc2d1	hudební
akademie	akademie	k1gFnSc2	akademie
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
Madelaine	Madelaine	k1gFnSc2	Madelaine
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
François	François	k1gFnSc4	François
Frichet	Frichet	k1gMnSc1	Frichet
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
Pascal	Pascal	k1gMnSc1	Pascal
Collasse	Collasse	k1gFnSc1	Collasse
<g/>
;	;	kIx,	;
nástupnictví	nástupnictví	k1gNnSc1	nástupnictví
ve	v	k7c6	v
funkcích	funkce	k1gFnPc6	funkce
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
zajištěno	zajištěn	k2eAgNnSc4d1	zajištěno
jeho	jeho	k3xOp3gNnSc4	jeho
syn	syn	k1gMnSc1	syn
Jean-Louis	Jean-Louis	k1gFnSc2	Jean-Louis
Lully	Lulla	k1gFnSc2	Lulla
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1687	[number]	k4	1687
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
gangrény	gangréna	k1gFnSc2	gangréna
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hlavní	hlavní	k2eAgFnSc6d1	hlavní
rezidenci	rezidence	k1gFnSc6	rezidence
<g/>
,	,	kIx,	,
domě	dům	k1gInSc6	dům
na	na	k7c6	na
pařížském	pařížský	k2eAgNnSc6d1	pařížské
předměstí	předměstí	k1gNnSc6	předměstí
Ville	Ville	k1gFnSc2	Ville
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Évê	Évê	k1gMnSc1	Évê
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pohřbu	pohřeb	k1gInSc6	pohřeb
v	v	k7c6	v
místně	místně	k6eAd1	místně
příslušném	příslušný	k2eAgInSc6d1	příslušný
farním	farní	k2eAgInSc6d1	farní
kostele	kostel	k1gInSc6	kostel
kostele	kostel	k1gInSc6	kostel
svaté	svatá	k1gFnSc2	svatá
Máří	Máří	k?	Máří
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
(	(	kIx(	(
<g/>
zbořen	zbořen	k2eAgInSc1d1	zbořen
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nahrazen	nahradit	k5eAaPmNgInS	nahradit
kostelem	kostel	k1gInSc7	kostel
novým	nový	k2eAgInSc7d1	nový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
zaznělo	zaznít	k5eAaPmAgNnS	zaznít
jeho	jeho	k3xOp3gNnSc1	jeho
Dies	Dies	k1gInSc4	Dies
irae	ira	k1gMnSc2	ira
(	(	kIx(	(
<g/>
Den	den	k1gInSc1	den
hněvu	hněv	k1gInSc2	hněv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
tělo	tělo	k1gNnSc1	tělo
–	–	k?	–
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
vyjádřeným	vyjádřený	k2eAgNnSc7d1	vyjádřené
v	v	k7c6	v
závěti	závěť	k1gFnSc6	závěť
–	–	k?	–
uloženo	uložen	k2eAgNnSc4d1	uloženo
do	do	k7c2	do
tomby	tomba	k1gFnSc2	tomba
v	v	k7c6	v
augustiniánském	augustiniánský	k2eAgInSc6d1	augustiniánský
kostele	kostel	k1gInSc6	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Vítězné	vítězný	k2eAgFnSc2d1	vítězná
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
na	na	k7c6	na
vnitřnosti	vnitřnost	k1gFnSc6	vnitřnost
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zůstaly	zůstat	k5eAaPmAgInP	zůstat
uloženy	uložit	k5eAaPmNgInP	uložit
u	u	k7c2	u
Máří	Máří	k?	Máří
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
<g/>
.	.	kIx.	.
</s>
<s>
Lullyho	Lullyze	k6eAd1	Lullyze
vdova	vdova	k1gFnSc1	vdova
dala	dát	k5eAaPmAgFnS	dát
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
pohřben	pohřben	k2eAgInSc1d1	pohřben
<g/>
,	,	kIx,	,
zřídit	zřídit	k5eAaPmF	zřídit
výstavný	výstavný	k2eAgInSc4d1	výstavný
kenotaf	kenotaf	k1gInSc4	kenotaf
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
bez	bez	k7c2	bez
kovových	kovový	k2eAgFnPc2d1	kovová
a	a	k8xC	a
štukových	štukový	k2eAgFnPc2d1	štuková
součástí	součást	k1gFnPc2	součást
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
za	za	k7c2	za
velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
<g/>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lull	k1gInPc1	Lull
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zanechal	zanechat	k5eAaPmAgMnS	zanechat
čtyři	čtyři	k4xCgInPc4	čtyři
domy	dům	k1gInPc4	dům
–	–	k?	–
totiž	totiž	k9	totiž
svou	svůj	k3xOyFgFnSc4	svůj
rezidenci	rezidence	k1gFnSc4	rezidence
ve	v	k7c6	v
Ville	Villo	k1gNnSc6	Villo
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Évê	Évê	k1gMnSc2	Évê
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
pařížský	pařížský	k2eAgInSc1d1	pařížský
dům	dům	k1gInSc1	dům
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Sainte-Anne	Sainte-Ann	k1gInSc5	Sainte-Ann
<g/>
,	,	kIx,	,
nájemní	nájemní	k2eAgInSc4d1	nájemní
dům	dům	k1gInSc4	dům
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Royale	Royala	k1gFnSc6	Royala
a	a	k8xC	a
venkovské	venkovský	k2eAgNnSc4d1	venkovské
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c4	v
Puteaux	Puteaux	k1gInSc4	Puteaux
a	a	k8xC	a
nemalý	malý	k2eNgInSc4d1	nemalý
movitý	movitý	k2eAgInSc4d1	movitý
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
překvapivé	překvapivý	k2eAgNnSc4d1	překvapivé
množství	množství	k1gNnSc4	množství
hotovosti	hotovost	k1gFnSc2	hotovost
<g/>
.	.	kIx.	.
</s>
<s>
Novinové	novinový	k2eAgFnPc1d1	novinová
zprávy	zpráva	k1gFnPc1	zpráva
uváděly	uvádět	k5eAaImAgFnP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
truhlách	truhla	k1gFnPc6	truhla
našlo	najít	k5eAaPmAgNnS	najít
až	až	k9	až
760	[number]	k4	760
000	[number]	k4	000
livrů	livr	k1gInPc2	livr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pětisetnásobek	pětisetnásobek	k1gInSc4	pětisetnásobek
ročního	roční	k2eAgInSc2d1	roční
platu	plat	k1gInSc2	plat
průměrného	průměrný	k2eAgMnSc2d1	průměrný
dvorního	dvorní	k2eAgMnSc2d1	dvorní
hudebníka	hudebník	k1gMnSc2	hudebník
<g/>
;	;	kIx,	;
pozůstalostní	pozůstalostní	k2eAgInSc1d1	pozůstalostní
soupis	soupis	k1gInSc1	soupis
však	však	k9	však
uvádí	uvádět	k5eAaImIp3nS	uvádět
266	[number]	k4	266
562	[number]	k4	562
livrů	livr	k1gInPc2	livr
v	v	k7c6	v
hotovosti	hotovost	k1gFnSc6	hotovost
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nábytkem	nábytek	k1gInSc7	nábytek
a	a	k8xC	a
cennostmi	cennost	k1gFnPc7	cennost
387	[number]	k4	387
000	[number]	k4	000
livrů	livr	k1gInPc2	livr
a	a	k8xC	a
po	po	k7c6	po
připočtení	připočtení	k1gNnSc6	připočtení
ceny	cena	k1gFnSc2	cena
nemovitostí	nemovitost	k1gFnPc2	nemovitost
zhruba	zhruba	k6eAd1	zhruba
650	[number]	k4	650
000	[number]	k4	000
livrů	livr	k1gInPc2	livr
celkové	celkový	k2eAgFnSc2d1	celková
hodnoty	hodnota	k1gFnSc2	hodnota
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hodnocení	hodnocení	k1gNnSc1	hodnocení
umělecké	umělecký	k2eAgFnSc2d1	umělecká
činnosti	činnost	k1gFnSc2	činnost
===	===	k?	===
</s>
</p>
<p>
<s>
Lullyho	Lullyze	k6eAd1	Lullyze
pověst	pověst	k1gFnSc1	pověst
dvorního	dvorní	k2eAgMnSc2d1	dvorní
intrikána	intrikán	k1gMnSc2	intrikán
neblaze	blaze	k6eNd1	blaze
poškodila	poškodit	k5eAaPmAgFnS	poškodit
jeho	jeho	k3xOp3gNnSc4	jeho
hudební	hudební	k2eAgNnSc4d1	hudební
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
autentické	autentický	k2eAgNnSc4d1	autentické
uvádění	uvádění	k1gNnSc4	uvádění
staré	starý	k2eAgFnSc2d1	stará
hudby	hudba	k1gFnSc2	hudba
způsobila	způsobit	k5eAaPmAgFnS	způsobit
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
několika	několik	k4yIc2	několik
polozapomenutých	polozapomenutý	k2eAgFnPc2d1	polozapomenutá
jevištních	jevištní	k2eAgFnPc2d1	jevištní
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Uvedení	uvedení	k1gNnSc1	uvedení
opery	opera	k1gFnSc2	opera
Atys	Atysa	k1gFnPc2	Atysa
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
novodobý	novodobý	k2eAgInSc4d1	novodobý
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
barokní	barokní	k2eAgFnSc4d1	barokní
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Atyse	Atyse	k1gFnSc2	Atyse
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
i	i	k9	i
scénického	scénický	k2eAgNnSc2d1	scénické
provedení	provedení	k1gNnSc2	provedení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
Lullyho	Lully	k1gMnSc2	Lully
oper	opera	k1gFnPc2	opera
prováděna	provádět	k5eAaImNgFnS	provádět
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
koncertním	koncertní	k2eAgNnSc6d1	koncertní
provedení	provedení	k1gNnSc6	provedení
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
Isis	Isis	k1gFnSc1	Isis
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
oper	opera	k1gFnPc2	opera
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
Lullyho	Lully	k1gMnSc4	Lully
významná	významný	k2eAgNnPc4d1	významné
duchovní	duchovní	k2eAgNnPc4d1	duchovní
díla	dílo	k1gNnPc4	dílo
11	[number]	k4	11
malých	malý	k2eAgMnPc2d1	malý
a	a	k8xC	a
11	[number]	k4	11
velkých	velký	k2eAgNnPc2d1	velké
motet	moteto	k1gNnPc2	moteto
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
text	text	k1gInSc1	text
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
žalmů	žalm	k1gInPc2	žalm
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Quare	Quar	k1gInSc5	Quar
fremuerunt	fremuerunta	k1gFnPc2	fremuerunta
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
žalmu	žalm	k1gInSc2	žalm
č.	č.	k?	č.
2	[number]	k4	2
<g/>
)	)	kIx)	)
či	či	k8xC	či
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
motivy	motiv	k1gInPc4	motiv
(	(	kIx(	(
<g/>
Plaude	Plaud	k1gMnSc5	Plaud
Laetare	Laetar	k1gMnSc5	Laetar
Gallia	Gallium	k1gNnPc1	Gallium
<g/>
,	,	kIx,	,
Raduj	radovat	k5eAaImRp2nS	radovat
se	se	k3xPyFc4	se
a	a	k8xC	a
zpívej	zpívat	k5eAaImRp2nS	zpívat
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
Lullyho	Lully	k1gMnSc4	Lully
hudební	hudební	k2eAgMnSc1d1	hudební
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
rytmická	rytmický	k2eAgFnSc1d1	rytmická
preciznost	preciznost	k1gFnSc1	preciznost
<g/>
,	,	kIx,	,
dramatický	dramatický	k2eAgInSc4d1	dramatický
patos	patos	k1gInSc4	patos
<g/>
;	;	kIx,	;
velké	velký	k2eAgNnSc4d1	velké
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
melodické	melodický	k2eAgFnSc2d1	melodická
linky	linka	k1gFnSc2	linka
árií	árie	k1gFnPc2	árie
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc4d1	velké
zastoupení	zastoupení	k1gNnSc4	zastoupení
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
bohatá	bohatý	k2eAgFnSc1d1	bohatá
choreografie	choreografie	k1gFnSc1	choreografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dochování	dochování	k1gNnSc1	dochování
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc1	seznam
a	a	k8xC	a
edice	edice	k1gFnSc1	edice
děl	dělo	k1gNnPc2	dělo
===	===	k?	===
</s>
</p>
<p>
<s>
Lullyho	Lullyze	k6eAd1	Lullyze
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
současníky	současník	k1gMnPc7	současník
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
v	v	k7c6	v
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
schopný	schopný	k2eAgMnSc1d1	schopný
podnikatel	podnikatel	k1gMnSc1	podnikatel
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
propagačního	propagační	k2eAgInSc2d1	propagační
významu	význam	k1gInSc2	význam
publikace	publikace	k1gFnSc2	publikace
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
ji	on	k3xPp3gFnSc4	on
financovat	financovat	k5eAaBmF	financovat
<g/>
,	,	kIx,	,
všechna	všechen	k3xTgNnPc1	všechen
rozměrná	rozměrný	k2eAgNnPc1d1	rozměrné
díla	dílo	k1gNnPc1	dílo
jeho	on	k3xPp3gNnSc2	on
zralého	zralý	k2eAgNnSc2d1	zralé
období	období	k1gNnSc2	období
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
autorských	autorský	k2eAgFnPc2d1	autorská
tištěných	tištěný	k2eAgFnPc2d1	tištěná
partitur	partitura	k1gFnPc2	partitura
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Lullyho	Lully	k1gMnSc2	Lully
smrti	smrt	k1gFnSc6	smrt
navíc	navíc	k6eAd1	navíc
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
nařídil	nařídit	k5eAaPmAgMnS	nařídit
shromáždit	shromáždit	k5eAaPmF	shromáždit
všechna	všechen	k3xTgNnPc1	všechen
zachovaná	zachovaný	k2eAgNnPc1d1	zachované
Lullyho	Lully	k1gMnSc4	Lully
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
úlohy	úloha	k1gFnSc2	úloha
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
skladatel	skladatel	k1gMnSc1	skladatel
André	André	k1gMnSc1	André
Danican	Danican	k1gMnSc1	Danican
Philidor	Philidor	k1gMnSc1	Philidor
(	(	kIx(	(
<g/>
1652	[number]	k4	1652
<g/>
–	–	k?	–
<g/>
1730	[number]	k4	1730
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
králův	králův	k2eAgMnSc1d1	králův
hudební	hudební	k2eAgMnSc1d1	hudební
knihovník	knihovník	k1gMnSc1	knihovník
a	a	k8xC	a
kopista	kopista	k1gMnSc1	kopista
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
řada	řada	k1gFnSc1	řada
skladeb	skladba	k1gFnPc2	skladba
ze	z	k7c2	z
začátků	začátek	k1gInPc2	začátek
Lullyho	Lully	k1gMnSc2	Lully
působení	působení	k1gNnSc1	působení
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
opisy	opis	k1gInPc7	opis
Lullyho	Lullyha	k1gFnSc5	Lullyha
děl	dělo	k1gNnPc2	dělo
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
i	i	k8xC	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
rukopisných	rukopisný	k2eAgFnPc6d1	rukopisná
sbírkách	sbírka	k1gFnPc6	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Philidorovy	Philidorův	k2eAgInPc1d1	Philidorův
opisy	opis	k1gInPc1	opis
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
spolehlivé	spolehlivý	k2eAgInPc1d1	spolehlivý
<g/>
:	:	kIx,	:
neváhal	váhat	k5eNaImAgMnS	váhat
například	například	k6eAd1	například
nechat	nechat	k5eAaPmF	nechat
skladby	skladba	k1gFnPc4	skladba
jen	jen	k9	jen
torzovité	torzovitý	k2eAgNnSc1d1	torzovité
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	k9	by
chybějící	chybějící	k2eAgInPc4d1	chybějící
úseky	úsek	k1gInPc4	úsek
nebo	nebo	k8xC	nebo
hlasy	hlas	k1gInPc4	hlas
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
však	však	k9	však
spolehlivé	spolehlivý	k2eAgNnSc1d1	spolehlivé
přiřčení	přiřčení	k1gNnSc1	přiřčení
Lullymu	Lullym	k1gInSc2	Lullym
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
skladeb	skladba	k1gFnPc2	skladba
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
napsali	napsat	k5eAaBmAgMnP	napsat
jiní	jiný	k2eAgMnPc1d1	jiný
skladatelé	skladatel	k1gMnPc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Philidor	Philidor	k1gMnSc1	Philidor
i	i	k9	i
pozdější	pozdní	k2eAgMnPc1d2	pozdější
badatelé	badatel	k1gMnPc1	badatel
občas	občas	k6eAd1	občas
podléhali	podléhat	k5eAaImAgMnP	podléhat
jednak	jednak	k8xC	jednak
tendenci	tendence	k1gFnSc4	tendence
připisovat	připisovat	k5eAaImF	připisovat
slavnému	slavný	k2eAgNnSc3d1	slavné
Lullymu	Lullymum	k1gNnSc3	Lullymum
i	i	k8xC	i
skladby	skladba	k1gFnPc1	skladba
jeho	jeho	k3xOp3gMnPc2	jeho
méně	málo	k6eAd2	málo
známých	známý	k2eAgMnPc2d1	známý
současníků	současník	k1gMnPc2	současník
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
tendenci	tendence	k1gFnSc4	tendence
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
Lullyho	Lully	k1gMnSc4	Lully
pověst	pověst	k1gFnSc4	pověst
a	a	k8xC	a
připisovat	připisovat	k5eAaImF	připisovat
mu	on	k3xPp3gMnSc3	on
skladby	skladba	k1gFnPc4	skladba
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
skladeb	skladba	k1gFnPc2	skladba
méně	málo	k6eAd2	málo
zdařilých	zdařilý	k2eAgFnPc2d1	zdařilá
jeho	jeho	k3xOp3gInSc6	jeho
autorství	autorství	k1gNnSc6	autorství
popírat	popírat	k5eAaImF	popírat
<g/>
.	.	kIx.	.
<g/>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
dobových	dobový	k2eAgFnPc2d1	dobová
zpráv	zpráva	k1gFnPc2	zpráva
známa	znám	k2eAgFnSc1d1	známa
<g />
.	.	kIx.	.
</s>
<s>
Lullyho	Lullyze	k6eAd1	Lullyze
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
nezachovala	zachovat	k5eNaPmAgNnP	zachovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
baletní	baletní	k2eAgFnSc1d1	baletní
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnPc1	první
novodobé	novodobý	k2eAgFnPc1d1	novodobá
edice	edice	k1gFnPc1	edice
Lullyho	Lully	k1gMnSc2	Lully
děl	dělo	k1gNnPc2	dělo
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
například	například	k6eAd1	například
francouzský	francouzský	k2eAgMnSc1d1	francouzský
skladatel	skladatel	k1gMnSc1	skladatel
Théodor	Théodor	k1gMnSc1	Théodor
de	de	k?	de
Lajarte	Lajart	k1gInSc5	Lajart
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc1	edice
sedmi	sedm	k4xCc2	sedm
oper	opera	k1gFnPc2	opera
v	v	k7c6	v
letech	let	k1gInPc6	let
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
německý	německý	k2eAgMnSc1d1	německý
muzikolog	muzikolog	k1gMnSc1	muzikolog
Robert	Robert	k1gMnSc1	Robert
Eitner	Eitner	k1gMnSc1	Eitner
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc1	edice
Armidy	Armida	k1gFnSc2	Armida
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Edice	edice	k1gFnSc1	edice
Lullyho	Lully	k1gMnSc2	Lully
souborného	souborný	k2eAgNnSc2d1	souborné
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Henry	Henry	k1gMnSc1	Henry
Pruniè	Pruniè	k1gFnSc2	Pruniè
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zastavila	zastavit	k5eAaPmAgFnS	zastavit
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
muzikolog	muzikolog	k1gMnSc1	muzikolog
Herbert	Herbert	k1gMnSc1	Herbert
Schneider	Schneider	k1gMnSc1	Schneider
sestavil	sestavit	k5eAaPmAgMnS	sestavit
seznam	seznam	k1gInSc4	seznam
Lullyho	Lully	k1gMnSc2	Lully
děl	dělo	k1gNnPc2	dělo
Lully	Lulla	k1gFnSc2	Lulla
Werke	Werke	k1gFnSc1	Werke
Verzeichnis	Verzeichnis	k1gFnSc1	Verzeichnis
(	(	kIx(	(
<g/>
LWV	LWV	kA	LWV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
utvořen	utvořit	k5eAaPmNgInS	utvořit
chronologicky	chronologicky	k6eAd1	chronologicky
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
tematicky	tematicky	k6eAd1	tematicky
=	=	kIx~	=
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
BWV	BWV	kA	BWV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
prvního	první	k4xOgNnSc2	první
uvedení	uvedení	k1gNnSc2	uvedení
skladby	skladba	k1gFnSc2	skladba
(	(	kIx(	(
<g/>
premiéry	premiéra	k1gFnPc1	premiéra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schneider	Schneider	k1gMnSc1	Schneider
stojí	stát	k5eAaImIp3nS	stát
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jerômem	Jerôm	k1gInSc7	Jerôm
de	de	k?	de
La	la	k1gNnSc3	la
Gorce	Gorce	k1gFnSc2	Gorce
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nové	nový	k2eAgFnSc2d1	nová
kritické	kritický	k2eAgFnSc2d1	kritická
edice	edice	k1gFnSc2	edice
souborného	souborný	k2eAgMnSc2d1	souborný
Lullyho	Lully	k1gMnSc2	Lully
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vydává	vydávat	k5eAaImIp3nS	vydávat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Georg	Georg	k1gInSc1	Georg
Olms	Olms	k1gInSc1	Olms
Verlag	Verlag	k1gInSc4	Verlag
<g/>
.	.	kIx.	.
<g/>
Objemově	objemově	k6eAd1	objemově
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
Lullyho	Lully	k1gMnSc2	Lully
díla	dílo	k1gNnSc2	dílo
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
jeho	jeho	k3xOp3gFnPc4	jeho
opery	opera	k1gFnPc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
balety	balet	k1gInPc1	balet
a	a	k8xC	a
příbuzná	příbuzný	k2eAgNnPc1d1	příbuzné
jevištní	jevištní	k2eAgNnPc1d1	jevištní
díla	dílo	k1gNnPc1	dílo
(	(	kIx(	(
<g/>
comédie-ballet	comédieallet	k1gInSc1	comédie-ballet
<g/>
,	,	kIx,	,
tragédie-ballet	tragédieallet	k1gInSc1	tragédie-ballet
<g/>
,	,	kIx,	,
divertissement	divertissement	k1gInSc1	divertissement
<g/>
)	)	kIx)	)
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
pro	pro	k7c4	pro
dvůr	dvůr	k1gInSc4	dvůr
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
duchovní	duchovní	k2eAgFnSc1d1	duchovní
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
výhradně	výhradně	k6eAd1	výhradně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
moteta	moteto	k1gNnSc2	moteto
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
velkého	velký	k2eAgMnSc2d1	velký
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
malého	malý	k2eAgNnSc2d1	malé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lullyho	Lully	k1gMnSc2	Lully
dílo	dílo	k1gNnSc1	dílo
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
drobných	drobný	k2eAgFnPc2d1	drobná
skladeb	skladba	k1gFnPc2	skladba
vokálních	vokální	k2eAgFnPc2d1	vokální
a	a	k8xC	a
instrumentálních	instrumentální	k2eAgFnPc2d1	instrumentální
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
tanečních	taneční	k2eAgFnPc2d1	taneční
<g/>
.	.	kIx.	.
</s>
<s>
Zachovaly	zachovat	k5eAaPmAgFnP	zachovat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
kopiích	kopie	k1gFnPc6	kopie
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nelze	lze	k6eNd1	lze
přesně	přesně	k6eAd1	přesně
datovat	datovat	k5eAaImF	datovat
ani	ani	k8xC	ani
určit	určit	k5eAaPmF	určit
jejich	jejich	k3xOp3gInSc4	jejich
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
účel	účel	k1gInSc4	účel
či	či	k8xC	či
adresáta	adresát	k1gMnSc4	adresát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moteta	moteto	k1gNnSc2	moteto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
duchovní	duchovní	k2eAgFnSc2d1	duchovní
hudby	hudba	k1gFnSc2	hudba
tvoří	tvořit	k5eAaImIp3nP	tvořit
Lullyho	Lully	k1gMnSc4	Lully
příspěvek	příspěvek	k1gInSc4	příspěvek
soubor	soubor	k1gInSc4	soubor
motet	moteto	k1gNnPc2	moteto
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
francouzské	francouzský	k2eAgNnSc1d1	francouzské
moteto	moteto	k1gNnSc1	moteto
se	se	k3xPyFc4	se
konstituovalo	konstituovat	k5eAaBmAgNnS	konstituovat
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
skladatelovým	skladatelův	k2eAgInSc7d1	skladatelův
příchodem	příchod	k1gInSc7	příchod
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
základní	základní	k2eAgInPc1d1	základní
stylové	stylový	k2eAgInPc1d1	stylový
rysy	rys	k1gInPc1	rys
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
ustálily	ustálit	k5eAaPmAgFnP	ustálit
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
Formého	Formý	k2eAgMnSc2d1	Formý
<g/>
,	,	kIx,	,
Guillauma	Guillaum	k1gMnSc2	Guillaum
Bouzignaca	Bouzignacus	k1gMnSc2	Bouzignacus
<g/>
,	,	kIx,	,
Thomase	Thomas	k1gMnSc2	Thomas
Goberta	Gobert	k1gMnSc2	Gobert
a	a	k8xC	a
Jeana	Jean	k1gMnSc2	Jean
Veillota	Veillot	k1gMnSc2	Veillot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
definoval	definovat	k5eAaBmAgMnS	definovat
Henri	Henr	k1gMnSc3	Henr
du	du	k?	du
Mont	Mont	k1gMnSc1	Mont
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
žánry	žánr	k1gInPc4	žánr
"	"	kIx"	"
<g/>
velkého	velký	k2eAgNnSc2d1	velké
moteta	moteto	k1gNnSc2	moteto
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
malého	malý	k2eAgNnSc2d1	malé
moteta	moteto	k1gNnSc2	moteto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lully	Lulla	k1gFnPc1	Lulla
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
přinesl	přinést	k5eAaPmAgInS	přinést
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
inovací	inovace	k1gFnPc2	inovace
než	než	k8xS	než
v	v	k7c6	v
jevištních	jevištní	k2eAgInPc6d1	jevištní
žánrech	žánr	k1gInPc6	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
baletu	balet	k1gInSc2	balet
a	a	k8xC	a
opery	opera	k1gFnSc2	opera
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
zdatné	zdatný	k2eAgMnPc4d1	zdatný
konkurenty	konkurent	k1gMnPc4	konkurent
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
du	du	k?	du
Monta	Mont	k1gInSc2	Mont
zejména	zejména	k9	zejména
Marka-Antoina	Marka-Antoin	k2eAgFnSc1d1	Marka-Antoin
Charpentiera	Charpentiera	k1gFnSc1	Charpentiera
a	a	k8xC	a
Michela-Richarda	Michela-Richarda	k1gFnSc1	Michela-Richarda
Delalanda	Delalanda	k1gFnSc1	Delalanda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgInPc4d1	velký
církevní	církevní	k2eAgInPc4d1	církevní
obřady	obřad	k1gInPc4	obřad
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Lullyho	Lully	k1gMnSc4	Lully
významnou	významný	k2eAgFnSc7d1	významná
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
pověst	pověst	k1gFnSc4	pověst
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
mohlo	moct	k5eAaImAgNnS	moct
účastnit	účastnit	k5eAaImF	účastnit
i	i	k9	i
více	hodně	k6eAd2	hodně
posluchačů	posluchač	k1gMnPc2	posluchač
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dvorských	dvorský	k2eAgFnPc2d1	dvorská
zábav	zábava	k1gFnPc2	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
nabízelo	nabízet	k5eAaImAgNnS	nabízet
využití	využití	k1gNnSc1	využití
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
hudebníků	hudebník	k1gMnPc2	hudebník
než	než	k8xS	než
při	při	k7c6	při
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
jiné	jiný	k2eAgFnSc6d1	jiná
příležitosti	příležitost	k1gFnSc6	příležitost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
obřadech	obřad	k1gInPc6	obřad
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
spojit	spojit	k5eAaPmF	spojit
hudební	hudební	k2eAgInSc4d1	hudební
personál	personál	k1gInSc4	personál
"	"	kIx"	"
<g/>
královské	královský	k2eAgFnSc2d1	královská
kaple	kaple	k1gFnSc2	kaple
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
musique	musiquat	k5eAaPmIp3nS	musiquat
de	de	k?	de
la	la	k1gNnSc7	la
Chapelle	Chapelle	k1gFnSc2	Chapelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyhrazený	vyhrazený	k2eAgInSc1d1	vyhrazený
pro	pro	k7c4	pro
duchovní	duchovní	k2eAgFnSc4d1	duchovní
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
se	s	k7c7	s
světskými	světský	k2eAgNnPc7d1	světské
tělesy	těleso	k1gNnPc7	těleso
(	(	kIx(	(
<g/>
musique	musique	k1gNnSc1	musique
de	de	k?	de
la	la	k1gNnPc2	la
Chambre	Chambr	k1gMnSc5	Chambr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k9	již
roku	rok	k1gInSc2	rok
1663	[number]	k4	1663
se	s	k7c7	s
premiéry	premiéra	k1gFnSc2	premiéra
Lullyho	Lully	k1gMnSc2	Lully
Miserere	Miserere	k1gNnSc2	Miserere
účastnilo	účastnit	k5eAaImAgNnS	účastnit
přes	přes	k7c4	přes
140	[number]	k4	140
pěvců	pěvec	k1gMnPc2	pěvec
a	a	k8xC	a
instrumentalistů	instrumentalista	k1gMnPc2	instrumentalista
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
Lullyho	Lullyha	k1gFnSc5	Lullyha
motetem	moteto	k1gNnSc7	moteto
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
–	–	k?	–
autorství	autorství	k1gNnSc1	autorství
není	být	k5eNaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
s	s	k7c7	s
naprostou	naprostý	k2eAgFnSc7d1	naprostá
jistotou	jistota	k1gFnSc7	jistota
–	–	k?	–
Jubilate	Jubilat	k1gMnSc5	Jubilat
Deo	Deo	k1gMnSc5	Deo
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k6eAd1	též
Motet	moteto	k1gNnPc2	moteto
de	de	k?	de
la	la	k1gNnPc7	la
Paix	Paix	k1gInSc1	Paix
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1660	[number]	k4	1660
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
dosažení	dosažení	k1gNnSc2	dosažení
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
brzy	brzy	k6eAd1	brzy
zapomenuto	zapomenout	k5eAaPmNgNnS	zapomenout
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
následující	následující	k2eAgNnSc4d1	následující
moteto	moteto	k1gNnSc4	moteto
Miserere	Miserere	k1gNnSc2	Miserere
<g/>
,	,	kIx,	,
napsané	napsaný	k2eAgInPc1d1	napsaný
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pro	pro	k7c4	pro
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
roku	rok	k1gInSc2	rok
1663	[number]	k4	1663
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
opakováno	opakovat	k5eAaImNgNnS	opakovat
při	při	k7c6	při
řadě	řada	k1gFnSc6	řada
příležitostí	příležitost	k1gFnPc2	příležitost
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Menšího	malý	k2eAgInSc2d2	menší
ohlasu	ohlas	k1gInSc2	ohlas
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
O	o	k7c4	o
lachrymae	lachrymae	k1gFnSc4	lachrymae
a	a	k8xC	a
Benedictus	Benedictus	k1gInSc4	Benedictus
<g/>
,	,	kIx,	,
napsané	napsaný	k2eAgInPc4d1	napsaný
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
letech	let	k1gInPc6	let
1663	[number]	k4	1663
<g/>
–	–	k?	–
<g/>
1664	[number]	k4	1664
<g/>
.	.	kIx.	.
</s>
<s>
Moteto	moteto	k1gNnSc1	moteto
Plaude	Plaud	k1gInSc5	Plaud
laetare	laetar	k1gMnSc5	laetar
Gallia	Gallius	k1gMnSc2	Gallius
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1668	[number]	k4	1668
<g/>
,	,	kIx,	,
napsané	napsaný	k2eAgNnSc1d1	napsané
ke	k	k7c3	k
slavnostnímu	slavnostní	k2eAgInSc3d1	slavnostní
křtu	křest	k1gInSc3	křest
Velkého	velký	k2eAgMnSc2d1	velký
dauphina	dauphin	k1gMnSc2	dauphin
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nadlouho	nadlouho	k6eAd1	nadlouho
poslední	poslední	k2eAgNnSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Lully	Lull	k1gMnPc7	Lull
napsal	napsat	k5eAaBmAgMnS	napsat
jediné	jediný	k2eAgNnSc4d1	jediné
moteto	moteto	k1gNnSc4	moteto
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
nejslavnějších	slavný	k2eAgNnPc2d3	nejslavnější
<g/>
:	:	kIx,	:
Te	Te	k1gFnSc1	Te
Deum	Deum	k1gInSc1	Deum
ke	k	k7c3	k
křtu	křest	k1gInSc3	křest
vlastního	vlastní	k2eAgMnSc2d1	vlastní
syna	syn	k1gMnSc2	syn
Louise	Louis	k1gMnSc2	Louis
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
ve	v	k7c6	v
Fontainebleau	Fontainebleaus	k1gInSc6	Fontainebleaus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
kmotrem	kmotr	k1gMnSc7	kmotr
sám	sám	k3xTgMnSc1	sám
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
1677	[number]	k4	1677
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Lully	Lulla	k1gFnSc2	Lulla
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
motetům	moteto	k1gNnPc3	moteto
až	až	k8xS	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1683	[number]	k4	1683
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začaly	začít	k5eAaPmAgFnP	začít
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Madame	madame	k1gFnSc1	madame
de	de	k?	de
Maintenon	Maintenon	k1gInSc1	Maintenon
nad	nad	k7c7	nad
zábavami	zábava	k1gFnPc7	zábava
převládat	převládat	k5eAaImF	převládat
projevy	projev	k1gInPc4	projev
zbožnosti	zbožnost	k1gFnSc2	zbožnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lully	Lulla	k1gFnPc4	Lulla
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
udržet	udržet	k5eAaPmF	udržet
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1683	[number]	k4	1683
a	a	k8xC	a
1687	[number]	k4	1687
napsal	napsat	k5eAaPmAgInS	napsat
druhou	druhý	k4xOgFnSc4	druhý
šestici	šestice	k1gFnSc4	šestice
velkých	velký	k2eAgNnPc2d1	velké
motet	moteto	k1gNnPc2	moteto
<g/>
:	:	kIx,	:
De	De	k?	De
profundis	profundis	k1gInSc1	profundis
<g/>
,	,	kIx,	,
Dies	Dies	k1gInSc1	Dies
irae	irae	k1gFnSc1	irae
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
královny	královna	k1gFnSc2	královna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Quare	Quar	k1gInSc5	Quar
fremerunt	fremerunta	k1gFnPc2	fremerunta
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
žalmu	žalm	k1gInSc2	žalm
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Domine	Domin	k1gInSc5	Domin
salvum	salvum	k1gNnSc4	salvum
fac	fac	k?	fac
regem	regem	k1gInSc1	regem
<g/>
,	,	kIx,	,
Notus	Notus	k1gMnSc1	Notus
in	in	k?	in
Judaea	Judaea	k1gMnSc1	Judaea
<g />
.	.	kIx.	.
</s>
<s>
Deus	Deus	k6eAd1	Deus
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
žalmu	žalm	k1gInSc2	žalm
76	[number]	k4	76
<g/>
)	)	kIx)	)
a	a	k8xC	a
Exaudiat	Exaudiat	k2eAgInSc1d1	Exaudiat
te	te	k?	te
Dominus	Dominus	k1gInSc1	Dominus
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
žalmu	žalm	k1gInSc2	žalm
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Texty	text	k1gInPc4	text
těchto	tento	k3xDgNnPc2	tento
velkých	velký	k2eAgNnPc2d1	velké
motet	moteto	k1gNnPc2	moteto
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
liturgických	liturgický	k2eAgInPc2d1	liturgický
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
z	z	k7c2	z
Knihy	kniha	k1gFnSc2	kniha
žalmů	žalm	k1gInPc2	žalm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
však	však	k9	však
napsal	napsat	k5eAaBmAgInS	napsat
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
Pierre	Pierr	k1gInSc5	Pierr
Perin	Perin	k2eAgMnSc5d1	Perin
<g/>
.	.	kIx.	.
</s>
<s>
Zpívaný	zpívaný	k2eAgInSc1d1	zpívaný
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
rozložen	rozložit	k5eAaPmNgInS	rozložit
mezi	mezi	k7c4	mezi
velký	velký	k2eAgInSc4d1	velký
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
sbor	sbor	k1gInSc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
sbor	sbor	k1gInSc1	sbor
je	být	k5eAaImIp3nS	být
pětihlasý	pětihlasý	k2eAgInSc1d1	pětihlasý
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hlasy	hlas	k1gInPc4	hlas
"	"	kIx"	"
<g/>
dessus	dessus	k1gMnSc1	dessus
<g/>
,	,	kIx,	,
haute-contre	hauteontr	k1gMnSc5	haute-contr
<g/>
,	,	kIx,	,
taille	taille	k1gFnSc6	taille
<g/>
,	,	kIx,	,
basse-taille	basseailla	k1gFnSc6	basse-tailla
a	a	k8xC	a
basse	bassa	k1gFnSc6	bassa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
soprán	soprán	k1gInSc1	soprán
<g/>
,	,	kIx,	,
kontratenor	kontratenor	k1gInSc1	kontratenor
<g/>
,	,	kIx,	,
tenor	tenor	k1gInSc1	tenor
<g/>
,	,	kIx,	,
baryton	baryton	k1gInSc1	baryton
a	a	k8xC	a
bas	bas	k1gInSc1	bas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
výhradně	výhradně	k6eAd1	výhradně
mužský	mužský	k2eAgMnSc1d1	mužský
–	–	k?	–
soprány	soprán	k1gInPc4	soprán
zpívali	zpívat	k5eAaImAgMnP	zpívat
chlapci	chlapec	k1gMnPc1	chlapec
–	–	k?	–
a	a	k8xC	a
s	s	k7c7	s
dominancí	dominance	k1gFnSc7	dominance
nižších	nízký	k2eAgInPc2d2	nižší
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
sbor	sbor	k1gInSc1	sbor
u	u	k7c2	u
Lullyho	Lully	k1gMnSc2	Lully
tvoří	tvořit	k5eAaImIp3nP	tvořit
čtyři	čtyři	k4xCgFnPc4	čtyři
nebo	nebo	k8xC	nebo
pět	pět	k4xCc4	pět
sólistů	sólista	k1gMnPc2	sólista
<g/>
:	:	kIx,	:
tři	tři	k4xCgInPc4	tři
mužské	mužský	k2eAgInPc4d1	mužský
hlasy	hlas	k1gInPc4	hlas
(	(	kIx(	(
<g/>
haute-contre	hauteontr	k1gMnSc5	haute-contr
<g/>
,	,	kIx,	,
taille	taille	k1gFnSc1	taille
<g/>
,	,	kIx,	,
basse	basse	k1gFnSc1	basse
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
či	či	k8xC	či
dva	dva	k4xCgInPc4	dva
ženské	ženský	k2eAgInPc4d1	ženský
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
dessus	dessus	k1gMnSc1	dessus
či	či	k8xC	či
bas-dessus	basessus	k1gMnSc1	bas-dessus
(	(	kIx(	(
<g/>
alt	alt	k1gInSc1	alt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
sólisté	sólista	k1gMnPc1	sólista
byli	být	k5eAaImAgMnP	být
zpravidla	zpravidla	k6eAd1	zpravidla
profesionální	profesionální	k2eAgMnPc1d1	profesionální
pěvci	pěvec	k1gMnPc1	pěvec
<g/>
.	.	kIx.	.
</s>
<s>
Orchestrální	orchestrální	k2eAgInSc1d1	orchestrální
doprovod	doprovod	k1gInSc1	doprovod
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
velký	velký	k2eAgInSc1d1	velký
sbor	sbor	k1gInSc1	sbor
pětihlasý	pětihlasý	k2eAgMnSc1d1	pětihlasý
a	a	k8xC	a
nástroje	nástroj	k1gInPc1	nástroj
většinou	většinou	k6eAd1	většinou
dublují	dublovat	k5eAaImIp3nP	dublovat
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
hlasů	hlas	k1gInPc2	hlas
<g/>
;	;	kIx,	;
od	od	k7c2	od
Te	Te	k1gFnSc2	Te
Deum	Deum	k1gInSc1	Deum
zařadil	zařadit	k5eAaPmAgMnS	zařadit
skladatel	skladatel	k1gMnSc1	skladatel
do	do	k7c2	do
orchestru	orchestr	k1gInSc2	orchestr
trubky	trubka	k1gFnSc2	trubka
a	a	k8xC	a
tympány	tympán	k1gInPc4	tympán
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
hudbě	hudba	k1gFnSc6	hudba
nepoužívané	používaný	k2eNgFnSc6d1	nepoužívaná
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
zpívaných	zpívaný	k2eAgFnPc2d1	zpívaná
částí	část	k1gFnPc2	část
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
Lullyho	Lully	k1gMnSc4	Lully
moteta	moteto	k1gNnSc2	moteto
rozměrné	rozměrný	k2eAgFnSc2d1	rozměrná
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
symphonies	symphonies	k1gInSc1	symphonies
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
ritournelles	ritournelles	k1gInSc1	ritournelles
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Zatímco	zatímco	k8xS	zatímco
první	první	k4xOgNnPc4	první
velká	velký	k2eAgNnPc4d1	velké
moteta	moteto	k1gNnPc4	moteto
měla	mít	k5eAaImAgFnS	mít
tradičně	tradičně	k6eAd1	tradičně
uměřenou	uměřený	k2eAgFnSc4d1	uměřená
a	a	k8xC	a
vyváženou	vyvážený	k2eAgFnSc4d1	vyvážená
melodicko-harmonickou	melodickoarmonický	k2eAgFnSc4d1	melodicko-harmonický
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
od	od	k7c2	od
Plaude	Plaud	k1gInSc5	Plaud
laetare	laetar	k1gMnSc5	laetar
Gallia	Gallium	k1gNnPc1	Gallium
Lully	Lull	k1gInPc4	Lull
výrazně	výrazně	k6eAd1	výrazně
posílil	posílit	k5eAaPmAgInS	posílit
harmonickou	harmonický	k2eAgFnSc4d1	harmonická
složku	složka	k1gFnSc4	složka
a	a	k8xC	a
využíval	využívat	k5eAaPmAgInS	využívat
kontrapunkt	kontrapunkt	k1gInSc1	kontrapunkt
a	a	k8xC	a
imitace	imitace	k1gFnSc1	imitace
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
zkušeností	zkušenost	k1gFnPc2	zkušenost
z	z	k7c2	z
jevištní	jevištní	k2eAgFnSc2d1	jevištní
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
jeho	on	k3xPp3gNnSc2	on
moteta	moteto	k1gNnSc2	moteto
stávaly	stávat	k5eAaImAgFnP	stávat
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
divadelnějšími	divadelní	k2eAgMnPc7d2	divadelnější
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zejména	zejména	k9	zejména
platí	platit	k5eAaImIp3nS	platit
o	o	k7c6	o
poslední	poslední	k2eAgFnSc6d1	poslední
šestici	šestice	k1gFnSc6	šestice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
použité	použitý	k2eAgInPc4d1	použitý
prostředky	prostředek	k1gInPc4	prostředek
patřily	patřit	k5eAaImAgFnP	patřit
dynamické	dynamický	k2eAgInPc1d1	dynamický
a	a	k8xC	a
harmonické	harmonický	k2eAgInPc1d1	harmonický
kontrasty	kontrast	k1gInPc1	kontrast
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
rytmického	rytmický	k2eAgInSc2d1	rytmický
základu	základ	k1gInSc2	základ
<g/>
,	,	kIx,	,
čím	čí	k3xOyQgNnSc7	čí
dále	daleko	k6eAd2	daleko
"	"	kIx"	"
<g/>
opernější	operný	k2eAgNnSc4d2	operný
<g/>
"	"	kIx"	"
využívání	využívání	k1gNnSc4	využívání
malého	malý	k2eAgInSc2d1	malý
sboru	sbor	k1gInSc2	sbor
k	k	k7c3	k
áriím	árie	k1gFnPc3	árie
<g/>
,	,	kIx,	,
duetům	duet	k1gInPc3	duet
či	či	k8xC	či
tercetům	tercet	k1gInPc3	tercet
<g/>
,	,	kIx,	,
individuálnější	individuální	k2eAgNnSc4d2	individuálnější
využívání	využívání	k1gNnSc4	využívání
zejména	zejména	k9	zejména
dechových	dechový	k2eAgInPc2d1	dechový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Dramatického	dramatický	k2eAgInSc2d1	dramatický
účinku	účinek	k1gInSc2	účinek
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
také	také	k9	také
častými	častý	k2eAgFnPc7d1	častá
změnami	změna	k1gFnPc7	změna
tempa	tempo	k1gNnSc2	tempo
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c4	v
Dies	Dies	k1gInSc4	Dies
irae	ira	k1gInSc2	ira
27	[number]	k4	27
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
italské	italský	k2eAgFnSc2d1	italská
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
zejména	zejména	k9	zejména
v	v	k7c6	v
chromatismech	chromatismus	k1gInPc6	chromatismus
v	v	k7c6	v
pasážích	pasáž	k1gFnPc6	pasáž
připomínajících	připomínající	k2eAgFnPc6d1	připomínající
žalozpěv	žalozpěv	k1gInSc4	žalozpěv
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
slábne	slábnout	k5eAaImIp3nS	slábnout
<g/>
.	.	kIx.	.
<g/>
Malá	Malá	k1gFnSc1	Malá
moteta	moteto	k1gNnSc2	moteto
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
často	často	k6eAd1	často
v	v	k7c6	v
pozdních	pozdní	k2eAgInPc6d1	pozdní
opisech	opis	k1gInPc6	opis
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
spojeny	spojen	k2eAgFnPc1d1	spojena
různé	různý	k2eAgFnPc4d1	různá
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
třinácti	třináct	k4xCc2	třináct
malých	malý	k2eAgNnPc2d1	malé
motet	moteto	k1gNnPc2	moteto
původně	původně	k6eAd1	původně
připisovaných	připisovaný	k2eAgNnPc2d1	připisované
Lullymu	Lullym	k1gInSc6	Lullym
je	on	k3xPp3gInPc4	on
dnes	dnes	k6eAd1	dnes
u	u	k7c2	u
dvou	dva	k4xCgNnPc2	dva
jeho	jeho	k3xOp3gNnPc2	jeho
autorství	autorství	k1gNnSc2	autorství
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
a	a	k8xC	a
u	u	k7c2	u
třetího	třetí	k4xOgNnSc2	třetí
<g/>
,	,	kIx,	,
Exaudi	Exaud	k1gMnPc1	Exaud
Deus	Deusa	k1gFnPc2	Deusa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
stylových	stylový	k2eAgInPc2d1	stylový
důvodů	důvod	k1gInPc2	důvod
pochybné	pochybný	k2eAgNnSc1d1	pochybné
<g/>
.	.	kIx.	.
</s>
<s>
Nesporných	sporný	k2eNgInPc2d1	nesporný
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
deset	deset	k4xCc1	deset
<g/>
:	:	kIx,	:
Anima	animo	k1gNnPc1	animo
Christi	Christ	k1gMnPc1	Christ
<g/>
,	,	kIx,	,
Ave	ave	k1gNnSc6	ave
coeli	coenout	k5eAaPmAgMnP	coenout
munus	munus	k1gMnSc1	munus
supernum	supernum	k1gInSc4	supernum
<g/>
,	,	kIx,	,
Dixit	Dixit	k2eAgInSc4d1	Dixit
Dominus	Dominus	k1gInSc4	Dominus
<g/>
,	,	kIx,	,
Domine	Domin	k1gInSc5	Domin
salvum	salvum	k1gNnSc4	salvum
fac	fac	k?	fac
regem	regem	k1gInSc1	regem
<g/>
,	,	kIx,	,
Laudate	Laudat	k1gMnSc5	Laudat
pueri	puer	k1gMnSc5	puer
Domium	Domium	k1gNnSc1	Domium
<g/>
,	,	kIx,	,
O	o	k7c6	o
dulcissime	dulcissim	k1gMnSc5	dulcissim
Domine	Domin	k1gMnSc5	Domin
<g/>
,	,	kIx,	,
Omnes	Omnes	k1gMnSc1	Omnes
gentes	gentes	k1gMnSc1	gentes
plaudite	plaudit	k1gInSc5	plaudit
manibus	manibus	k1gInSc4	manibus
<g/>
,	,	kIx,	,
O	o	k7c4	o
sapientia	sapientius	k1gMnSc4	sapientius
in	in	k?	in
misterio	misterio	k1gMnSc1	misterio
<g/>
,	,	kIx,	,
Regina	Regina	k1gFnSc1	Regina
coeli	coele	k1gFnSc3	coele
a	a	k8xC	a
Salve	Salve	k1gNnSc3	Salve
Regina	Regina	k1gFnSc1	Regina
<g/>
.	.	kIx.	.
</s>
<s>
Pocházejí	pocházet	k5eAaImIp3nP	pocházet
všechna	všechen	k3xTgNnPc4	všechen
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1683	[number]	k4	1683
<g/>
–	–	k?	–
<g/>
1686	[number]	k4	1686
a	a	k8xC	a
podle	podle	k7c2	podle
Philidorových	Philidorův	k2eAgInPc2d1	Philidorův
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1729	[number]	k4	1729
byly	být	k5eAaImAgFnP	být
složeny	složit	k5eAaPmNgInP	složit
pro	pro	k7c4	pro
klášter	klášter	k1gInSc4	klášter
dcer	dcera	k1gFnPc2	dcera
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
kostele	kostel	k1gInSc6	kostel
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
slavné	slavný	k2eAgFnPc1d1	slavná
hudební	hudební	k2eAgFnPc1d1	hudební
produkce	produkce	k1gFnPc1	produkce
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
i	i	k9	i
profesionální	profesionální	k2eAgFnPc1d1	profesionální
pěvkyně	pěvkyně	k1gFnPc1	pěvkyně
či	či	k8xC	či
pěvci	pěvec	k1gMnPc1	pěvec
z	z	k7c2	z
blízké	blízký	k2eAgFnSc2d1	blízká
Opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Lullyho	Lullyze	k6eAd1	Lullyze
malá	malý	k2eAgNnPc1d1	malé
moteta	moteto	k1gNnPc1	moteto
jsou	být	k5eAaImIp3nP	být
složena	složit	k5eAaPmNgNnP	složit
pro	pro	k7c4	pro
tři	tři	k4xCgInPc4	tři
hlasy	hlas	k1gInPc4	hlas
–	–	k?	–
většinou	většinou	k6eAd1	většinou
ženské	ženská	k1gFnPc1	ženská
<g/>
,	,	kIx,	,
soprány	soprán	k1gInPc1	soprán
(	(	kIx(	(
<g/>
dessus	dessus	k1gInSc1	dessus
<g/>
)	)	kIx)	)
a	a	k8xC	a
alty	alt	k1gInPc1	alt
(	(	kIx(	(
<g/>
bas-dessus	basessus	k1gInSc1	bas-dessus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tři	tři	k4xCgMnPc4	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
i	i	k9	i
mužské	mužský	k2eAgInPc4d1	mužský
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Doprovod	doprovod	k1gInSc1	doprovod
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k6eAd1	continuo
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
violoncello	violoncello	k1gNnSc1	violoncello
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
při	při	k7c6	při
předehrách	předehra	k1gFnPc6	předehra
a	a	k8xC	a
mezihrách	mezihra	k1gFnPc6	mezihra
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
"	"	kIx"	"
<g/>
dessus	dessus	k1gInSc1	dessus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
housle	housle	k1gFnPc4	housle
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
violon	violon	k1gInSc1	violon
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
viola	viola	k1gFnSc1	viola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
latinské	latinský	k2eAgInPc1d1	latinský
<g/>
,	,	kIx,	,
vzaté	vzatý	k2eAgInPc1d1	vzatý
ze	z	k7c2	z
žalmů	žalm	k1gInPc2	žalm
či	či	k8xC	či
tradiční	tradiční	k2eAgFnSc2d1	tradiční
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
jiné	jiná	k1gFnSc6	jiná
Lullyho	Lully	k1gMnSc2	Lully
tvorbě	tvorba	k1gFnSc3	tvorba
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gNnPc6	on
objevují	objevovat	k5eAaImIp3nP	objevovat
italské	italský	k2eAgInPc1d1	italský
rysy	rys	k1gInPc1	rys
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
chromatismy	chromatismus	k1gInPc1	chromatismus
<g/>
,	,	kIx,	,
vokalízy	vokalíza	k1gFnPc1	vokalíza
a	a	k8xC	a
recitativy	recitativ	k1gInPc1	recitativ
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
rytmu	rytmus	k1gInSc6	rytmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnPc4d1	ostatní
práce	práce	k1gFnPc4	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Scénická	scénický	k2eAgNnPc1d1	scénické
díla	dílo	k1gNnPc1	dílo
a	a	k8xC	a
moteta	moteto	k1gNnPc1	moteto
tvoří	tvořit	k5eAaImIp3nP	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
Lullyho	Lully	k1gMnSc2	Lully
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
drobnější	drobný	k2eAgNnPc4d2	drobnější
vokální	vokální	k2eAgNnPc4d1	vokální
a	a	k8xC	a
instrumentální	instrumentální	k2eAgNnPc4d1	instrumentální
díla	dílo	k1gNnPc4	dílo
příležitostné	příležitostný	k2eAgFnSc2d1	příležitostná
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
komponovaná	komponovaný	k2eAgFnSc1d1	komponovaná
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
příležitosti	příležitost	k1gFnPc4	příležitost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
Lullyho	Lullyha	k1gFnSc5	Lullyha
funkcemi	funkce	k1gFnPc7	funkce
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
pochodů	pochod	k1gInPc2	pochod
(	(	kIx(	(
<g/>
Premiè	Premiè	k1gMnSc1	Premiè
marche	marchat	k5eAaPmIp3nS	marchat
des	des	k1gNnSc4	des
mousquetaires	mousquetaires	k1gInSc1	mousquetaires
1658	[number]	k4	1658
<g/>
,	,	kIx,	,
Marches	Marches	k1gMnSc1	Marches
et	et	k?	et
batteries	batteries	k1gMnSc1	batteries
de	de	k?	de
tambour	tambour	k1gMnSc1	tambour
1670	[number]	k4	1670
<g/>
,	,	kIx,	,
Marches	Marches	k1gMnSc1	Marches
pour	pour	k1gMnSc1	pour
le	le	k?	le
régiment	régiment	k1gMnSc1	régiment
de	de	k?	de
Savoie	Savoie	k1gFnSc2	Savoie
<g/>
,	,	kIx,	,
1685	[number]	k4	1685
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
54	[number]	k4	54
trií	trio	k1gNnPc2	trio
pro	pro	k7c4	pro
dvoje	dvoje	k4xRgFnPc4	dvoje
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k6eAd1	continuo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
i	i	k9	i
hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
kolotoče	kolotoč	k1gInSc2	kolotoč
králova	králův	k2eAgMnSc4d1	králův
bratra	bratr	k1gMnSc4	bratr
(	(	kIx(	(
<g/>
Airs	Airs	k1gInSc1	Airs
pour	pour	k1gMnSc1	pour
le	le	k?	le
Carrousel	Carrousel	k1gMnSc1	Carrousel
de	de	k?	de
Monseigneur	monseigneur	k1gMnSc1	monseigneur
<g/>
,	,	kIx,	,
1686	[number]	k4	1686
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
řady	řada	k1gFnSc2	řada
drobných	drobný	k2eAgFnPc2d1	drobná
vokálních	vokální	k2eAgFnPc2d1	vokální
skladeb	skladba	k1gFnPc2	skladba
označených	označený	k2eAgFnPc2d1	označená
většinou	většinou	k6eAd1	většinou
jako	jako	k9	jako
"	"	kIx"	"
<g/>
air	air	k?	air
<g/>
"	"	kIx"	"
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
Philippa	Philipp	k1gMnSc2	Philipp
Quinaulta	Quinault	k1gMnSc2	Quinault
<g/>
,	,	kIx,	,
Isaaca	Isaac	k1gInSc2	Isaac
de	de	k?	de
Benserade	Benserad	k1gInSc5	Benserad
<g/>
,	,	kIx,	,
Pierra	Pierr	k1gMnSc2	Pierr
Perrina	Perrin	k1gMnSc2	Perrin
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
tanců	tanec	k1gInPc2	tanec
(	(	kIx(	(
<g/>
allemande	allemand	k1gInSc5	allemand
<g/>
,	,	kIx,	,
bourrée	bourrée	k1gNnPc1	bourrée
<g/>
,	,	kIx,	,
sarabande	saraband	k1gInSc5	saraband
<g/>
,	,	kIx,	,
chaconne	chaconn	k1gMnSc5	chaconn
<g/>
,	,	kIx,	,
především	především	k9	především
však	však	k9	však
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
zvláště	zvláště	k6eAd1	zvláště
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
courante	courant	k1gMnSc5	courant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
bez	bez	k7c2	bez
spolehlivé	spolehlivý	k2eAgFnSc2d1	spolehlivá
datace	datace	k1gFnSc2	datace
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
dvorských	dvorský	k2eAgInPc2d1	dvorský
baletů	balet	k1gInPc2	balet
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
divertissements	divertissements	k1gInSc1	divertissements
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
od	od	k7c2	od
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
stylově	stylově	k6eAd1	stylově
neodlišují	odlišovat	k5eNaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
hudba	hudba	k1gFnSc1	hudba
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
taneční	taneční	k2eAgFnSc1d1	taneční
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Lullyho	Lully	k1gMnSc2	Lully
díle	díl	k1gInSc6	díl
výjimkou	výjimka	k1gFnSc7	výjimka
(	(	kIx(	(
<g/>
Plusieurs	Plusieurs	k1gInSc1	Plusieurs
piè	piè	k?	piè
de	de	k?	de
symphonie	symphonie	k1gFnSc2	symphonie
<g/>
,	,	kIx,	,
1685	[number]	k4	1685
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sólová	sólový	k2eAgFnSc1d1	sólová
hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
nástroje	nástroj	k1gInPc4	nástroj
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
Král	Král	k1gMnSc1	Král
tančí	tančit	k5eAaImIp3nS	tančit
líčí	líčit	k5eAaImIp3nS	líčit
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
studie	studie	k1gFnSc2	studie
Lully	Lulla	k1gFnSc2	Lulla
ou	ou	k0	ou
le	le	k?	le
musicien	musicien	k2eAgInSc4d1	musicien
du	du	k?	du
soleil	soleil	k1gInSc4	soleil
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Lully	Lulla	k1gFnPc1	Lulla
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
od	od	k7c2	od
Philippe	Philipp	k1gInSc5	Philipp
Beaussanta	Beaussant	k1gMnSc2	Beaussant
<g/>
,	,	kIx,	,
Lullyho	Lully	k1gMnSc2	Lully
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
králi	král	k1gMnSc3	král
a	a	k8xC	a
k	k	k7c3	k
Moliè	Moliè	k1gFnSc3	Moliè
a	a	k8xC	a
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
Lullyho	Lully	k1gMnSc4	Lully
život	život	k1gInSc4	život
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
pouze	pouze	k6eAd1	pouze
Lullyho	Lully	k1gMnSc4	Lully
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
kladen	kladen	k2eAgInSc1d1	kladen
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BEAUSSANT	BEAUSSANT	kA	BEAUSSANT
<g/>
,	,	kIx,	,
Philippe	Philipp	k1gInSc5	Philipp
<g/>
.	.	kIx.	.
</s>
<s>
Lully	Lulla	k1gFnPc1	Lulla
ou	ou	k0	ou
Le	Le	k1gFnSc7	Le
musicien	musiciet	k5eAaImNgInS	musiciet
du	du	k?	du
Soleil	Soleil	k1gInSc1	Soleil
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
:	:	kIx,	:
Theatre	Theatr	k1gMnSc5	Theatr
des	des	k1gNnPc1	des
Champs-Elysees	Champs-Elysees	k1gMnSc1	Champs-Elysees
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
893	[number]	k4	893
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
2070724789	[number]	k4	2070724789
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BLUCHE	BLUCHE	kA	BLUCHE
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
časů	čas	k1gInPc2	čas
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
675	[number]	k4	675
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BURROWS	BURROWS	kA	BURROWS
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
528	[number]	k4	528
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7391	[number]	k4	7391
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DE	DE	k?	DE
LA	la	k1gNnSc1	la
GORCE	GORCE	kA	GORCE
<g/>
,	,	kIx,	,
Jérôme	Jérôm	k1gInSc5	Jérôm
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lulla	k1gFnSc2	Lulla
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
:	:	kIx,	:
Fayard	Fayard	k1gMnSc1	Fayard
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
910	[number]	k4	910
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
2213607085	[number]	k4	2213607085
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GAMMOND	GAMMOND	kA	GAMMOND
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
skladatelé	skladatel	k1gMnPc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svojtka	Svojtka	k1gFnSc1	Svojtka
&	&	k?	&
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
208	[number]	k4	208
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7237	[number]	k4	7237
<g/>
-	-	kIx~	-
<g/>
590	[number]	k4	590
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
opery	opera	k1gFnSc2	opera
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lull	k1gInPc7	Lull
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jean-Baptiste	Jean-Baptist	k1gInSc5	Jean-Baptist
Lully	Lullo	k1gNnPc7	Lullo
</s>
</p>
<p>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lull	k1gMnPc7	Lull
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
<p>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lulla	k1gMnSc2	Lulla
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
