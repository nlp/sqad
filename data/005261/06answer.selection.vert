<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
de	de	k?	de
Lully	Lull	k1gMnPc7	Lull
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
ʒ	ʒ	k?	ʒ
<g/>
.	.	kIx.	.
<g/>
ba	ba	k9	ba
<g/>
.	.	kIx.	.
<g/>
tist	tist	k?	tist
de	de	k?	de
ly	ly	k?	ly
<g/>
.	.	kIx.	.
<g/>
li	li	k8xS	li
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Giovanni	Giovanň	k1gMnSc3	Giovanň
Battista	Battista	k1gMnSc1	Battista
Lulli	Lull	k1gMnSc3	Lull
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1632	[number]	k4	1632
Florencie	Florencie	k1gFnSc2	Florencie
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1687	[number]	k4	1687
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1662	[number]	k4	1662
zcela	zcela	k6eAd1	zcela
ovládal	ovládat	k5eAaImAgInS	ovládat
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
hudební	hudební	k2eAgFnSc4d1	hudební
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
