<s>
Chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
100	[number]	k4	100
km2	km2	k4	km2
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
jeskyněmi	jeskyně	k1gFnPc7	jeskyně
Naracoorte	Naracoort	k1gInSc5	Naracoort
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
