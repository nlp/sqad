<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Guatemaly	Guatemala	k1gFnSc2	Guatemala
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vertikální	vertikální	k2eAgInPc1d1	vertikální
pruhy	pruh	k1gInPc7	pruh
-	-	kIx~	-
světle	světle	k6eAd1	světle
modrým	modré	k1gNnSc7	modré
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgNnSc7d1	bílé
a	a	k8xC	a
světle	světle	k6eAd1	světle
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
znak	znak	k1gInSc1	znak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
základ	základ	k1gInSc1	základ
znaku	znak	k1gInSc2	znak
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
barvách	barva	k1gFnPc6	barva
quetzal	quetzal	k1gInSc1	quetzal
(	(	kIx(	(
<g/>
Kvesal	kvesal	k1gMnSc1	kvesal
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
<g/>
,	,	kIx,	,
guatemalský	guatemalský	k2eAgMnSc1d1	guatemalský
národní	národní	k2eAgMnSc1d1	národní
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
trogonů	trogon	k1gInPc2	trogon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
svírá	svírat	k5eAaImIp3nS	svírat
pergamenový	pergamenový	k2eAgInSc1d1	pergamenový
svitek	svitek	k1gInSc1	svitek
s	s	k7c7	s
textem	text	k1gInSc7	text
LIBERTAD	LIBERTAD	kA	LIBERTAD
15	[number]	k4	15
DE	DE	k?	DE
SEPTIEMBRE	SEPTIEMBRE	kA	SEPTIEMBRE
DE	DE	k?	DE
1821	[number]	k4	1821
(	(	kIx(	(
<g/>
Svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
den	den	k1gInSc1	den
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
jsou	být	k5eAaImIp3nP	být
zkřížené	zkřížený	k2eAgFnPc1d1	zkřížená
pušky	puška	k1gFnPc1	puška
značky	značka	k1gFnSc2	značka
Remington	remington	k1gInSc4	remington
s	s	k7c7	s
nasazenými	nasazený	k2eAgInPc7d1	nasazený
bodáky	bodák	k1gInPc7	bodák
a	a	k8xC	a
zkřížené	zkřížený	k2eAgFnPc1d1	zkřížená
šavle	šavle	k1gFnPc1	šavle
uvnitř	uvnitř	k7c2	uvnitř
vavřínového	vavřínový	k2eAgInSc2d1	vavřínový
věnce	věnec	k1gInSc2	věnec
<g/>
.	.	kIx.	.
<g/>
Barvy	barva	k1gFnPc1	barva
vlajky	vlajka	k1gFnPc1	vlajka
upomínají	upomínat	k5eAaImIp3nP	upomínat
na	na	k7c4	na
Spojené	spojený	k2eAgFnPc4d1	spojená
provincie	provincie	k1gFnPc4	provincie
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
Guatemala	Guatemala	k1gFnSc1	Guatemala
v	v	k7c6	v
letech	let	k1gInPc6	let
1823	[number]	k4	1823
<g/>
–	–	k?	–
<g/>
1839	[number]	k4	1839
patřila	patřit	k5eAaImAgFnS	patřit
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Hondurasem	Honduras	k1gInSc7	Honduras
<g/>
,	,	kIx,	,
Kostarikou	Kostarika	k1gFnSc7	Kostarika
<g/>
,	,	kIx,	,
Nikaraguou	Nikaragua	k1gFnSc7	Nikaragua
a	a	k8xC	a
Salvadorem	Salvador	k1gInSc7	Salvador
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jejich	jejich	k3xOp3gFnPc2	jejich
vlajek	vlajka	k1gFnPc2	vlajka
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
pruhy	pruh	k1gInPc4	pruh
ve	v	k7c6	v
vertikální	vertikální	k2eAgFnSc6d1	vertikální
poloze	poloha	k1gFnSc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
uvedených	uvedený	k2eAgFnPc6d1	uvedená
vlajkách	vlajka	k1gFnPc6	vlajka
to	ten	k3xDgNnSc1	ten
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
polohu	poloha	k1gFnSc4	poloha
federace	federace	k1gFnSc2	federace
mezi	mezi	k7c7	mezi
Karibským	karibský	k2eAgNnSc7d1	Karibské
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
znamená	znamenat	k5eAaImIp3nS	znamenat
nezávislost	nezávislost	k1gFnSc4	nezávislost
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Guatemaly	Guatemala	k1gFnSc2	Guatemala
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
Mayské	mayský	k2eAgFnSc2d1	mayská
říše	říš	k1gFnSc2	říš
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1523	[number]	k4	1523
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
dobyto	dobýt	k5eAaPmNgNnS	dobýt
Španěly	Španěl	k1gMnPc7	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Generálního	generální	k2eAgInSc2d1	generální
kapitanátu	kapitanát	k1gInSc2	kapitanát
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
,	,	kIx,	,
Belize	Belize	k1gFnSc1	Belize
<g/>
,	,	kIx,	,
Honduras	Honduras	k1gInSc1	Honduras
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gInSc1	Salvador
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
a	a	k8xC	a
Kostarika	Kostarika	k1gFnSc1	Kostarika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
Panamy	Panama	k1gFnSc2	Panama
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
vlajkami	vlajka	k1gFnPc7	vlajka
v	v	k7c6	v
pokolumbovské	pokolumbovský	k2eAgFnSc6d1	pokolumbovský
éře	éra	k1gFnSc6	éra
tedy	tedy	k9	tedy
byly	být	k5eAaImAgFnP	být
španělské	španělský	k2eAgFnPc1d1	španělská
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
státní	státní	k2eAgFnSc1d1	státní
a	a	k8xC	a
válečná	válečný	k2eAgFnSc1d1	válečná
vlajka	vlajka	k1gFnSc1	vlajka
Španělského	španělský	k2eAgNnSc2d1	španělské
království	království	k1gNnSc2	království
<g/>
.15	.15	k4	.15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1821	[number]	k4	1821
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Guatemala	Guatemala	k1gFnSc1	Guatemala
nezávislost	nezávislost	k1gFnSc1	nezávislost
a	a	k8xC	a
vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
španělská	španělský	k2eAgFnSc1d1	španělská
vlajka	vlajka	k1gFnSc1	vlajka
bez	bez	k7c2	bez
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
.5	.5	k4	.5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1822	[number]	k4	1822
byla	být	k5eAaImAgFnS	být
Guatemala	Guatemala	k1gFnSc1	Guatemala
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
Mexickému	mexický	k2eAgNnSc3d1	mexické
císařství	císařství	k1gNnSc3	císařství
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
vyvěšovat	vyvěšovat	k5eAaImF	vyvěšovat
jeho	jeho	k3xOp3gFnPc4	jeho
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
,	,	kIx,	,
po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
císaře	císař	k1gMnSc2	císař
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1823	[number]	k4	1823
pak	pak	k9	pak
vlajky	vlajka	k1gFnPc1	vlajka
mexické	mexický	k2eAgFnSc2d1	mexická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
císařství	císařství	k1gNnSc2	císařství
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vyvěšovat	vyvěšovat	k5eAaImF	vyvěšovat
neoficiální	oficiální	k2eNgFnPc1d1	neoficiální
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
:	:	kIx,	:
modro-bílo-modré	modroíloodrý	k2eAgFnPc1d1	modro-bílo-modrý
vlajky	vlajka	k1gFnPc1	vlajka
s	s	k7c7	s
horizontálními	horizontální	k2eAgInPc7d1	horizontální
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1823	[number]	k4	1823
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Guatemala	Guatemala	k1gFnSc1	Guatemala
znovu	znovu	k6eAd1	znovu
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
základ	základ	k1gInSc4	základ
Spojených	spojený	k2eAgFnPc2d1	spojená
provincií	provincie	k1gFnPc2	provincie
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
konfederace	konfederace	k1gFnSc1	konfederace
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
založena	založen	k2eAgFnSc1d1	založena
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkou	vlajka	k1gFnSc7	vlajka
konfederace	konfederace	k1gFnSc2	konfederace
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
list	list	k1gInSc1	list
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgNnSc7d1	bílé
a	a	k8xC	a
modrým	modré	k1gNnSc7	modré
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
znakem	znak	k1gInSc7	znak
Spojených	spojený	k2eAgFnPc2d1	spojená
středoamerických	středoamerický	k2eAgFnPc2d1	středoamerická
provincií	provincie	k1gFnPc2	provincie
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
v	v	k7c6	v
kruhovém	kruhový	k2eAgNnSc6d1	kruhové
poli	pole	k1gNnSc6	pole
<g/>
.22	.22	k4	.22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1824	[number]	k4	1824
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
konfederace	konfederace	k1gFnSc2	konfederace
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
Federativní	federativní	k2eAgFnSc6d1	federativní
republika	republika	k1gFnSc1	republika
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
Středoamerická	středoamerický	k2eAgFnSc1d1	středoamerická
spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
oficiálně	oficiálně	k6eAd1	oficiálně
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
byla	být	k5eAaImAgFnS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1825	[number]	k4	1825
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
pouze	pouze	k6eAd1	pouze
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
oválném	oválný	k2eAgNnSc6d1	oválné
poli	pole	k1gNnSc6	pole
<g/>
.13	.13	k4	.13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1839	[number]	k4	1839
Guatemala	Guatemala	k1gFnSc1	Guatemala
z	z	k7c2	z
federace	federace	k1gFnSc2	federace
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nezávislý	závislý	k2eNgInSc4d1	nezávislý
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
byla	být	k5eAaImAgFnS	být
změněna	změněn	k2eAgFnSc1d1	změněna
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zase	zase	k9	zase
pouhou	pouhý	k2eAgFnSc7d1	pouhá
změnou	změna	k1gFnSc7	změna
znaku	znak	k1gInSc2	znak
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
s	s	k7c7	s
modro-bílo-modrými	modroíloodrý	k2eAgFnPc7d1	modro-bílo-modrý
<g/>
,	,	kIx,	,
horizontálními	horizontální	k2eAgInPc7d1	horizontální
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1843	[number]	k4	1843
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
promítla	promítnout	k5eAaPmAgFnS	promítnout
i	i	k9	i
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
prezidenta	prezident	k1gMnSc2	prezident
Mariana	Marian	k1gMnSc2	Marian
Paredese	Paredese	k1gFnSc2	Paredese
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
kongres	kongres	k1gInSc4	kongres
změnit	změnit	k5eAaPmF	změnit
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
a	a	k8xC	a
zavedena	zaveden	k2eAgFnSc1d1	zavedena
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
tvořily	tvořit	k5eAaImAgFnP	tvořit
tři	tři	k4xCgFnPc1	tři
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
široké	široký	k2eAgInPc4d1	široký
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Prostřední	prostřednět	k5eAaImIp3nS	prostřednět
celý	celý	k2eAgInSc1d1	celý
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
a	a	k8xC	a
krajní	krajní	k2eAgFnSc3d1	krajní
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
v	v	k7c6	v
žerďové	žerďové	k2eAgFnSc6d1	žerďové
a	a	k8xC	a
vlající	vlající	k2eAgFnSc6d1	vlající
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
červený	červený	k2eAgInSc1d1	červený
v	v	k7c4	v
žerďové	žerďové	k2eAgInSc4d1	žerďové
a	a	k8xC	a
modrý	modrý	k2eAgInSc4d1	modrý
ve	v	k7c4	v
vlající	vlající	k2eAgFnPc4d1	vlající
části	část	k1gFnPc4	část
a	a	k8xC	a
dolní	dolní	k2eAgFnSc4d1	dolní
ve	v	k7c6	v
žluté	žlutý	k2eAgFnSc6d1	žlutá
a	a	k8xC	a
modré	modrý	k2eAgFnSc6d1	modrá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
umístěn	umístit	k5eAaPmNgInS	umístit
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.31	.31	k4	.31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1858	[number]	k4	1858
byla	být	k5eAaImAgFnS	být
nezvykle	zvykle	k6eNd1	zvykle
řešená	řešený	k2eAgFnSc1d1	řešená
vlajka	vlajka	k1gFnSc1	vlajka
změněna	změnit	k5eAaPmNgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
tvořilo	tvořit	k5eAaImAgNnS	tvořit
sedm	sedm	k4xCc1	sedm
horizontálních	horizontální	k2eAgInPc2d1	horizontální
pruhů	pruh	k1gInPc2	pruh
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
šířek	šířka	k1gFnPc2	šířka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
(	(	kIx(	(
<g/>
odshora	odshora	k6eAd1	odshora
<g/>
)	)	kIx)	)
modrá-bílá-červená-žlutá-červená-bílá-modrá	modráílá-červená-žlutá-červenáíláodrý	k2eAgFnSc1d1	modrá-bílá-červená-žlutá-červená-bílá-modrý
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
listu	list	k1gInSc2	list
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
umístěn	umístit	k5eAaPmNgInS	umístit
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.17	.17	k4	.17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1871	[number]	k4	1871
zavedl	zavést	k5eAaPmAgMnS	zavést
guatemalský	guatemalský	k2eAgMnSc1d1	guatemalský
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
Miguel	Miguel	k1gMnSc1	Miguel
García	García	k1gMnSc1	García
Granados	Granados	k1gMnSc1	Granados
dekretem	dekret	k1gInSc7	dekret
č.	č.	k?	č.
12	[number]	k4	12
novou	nový	k2eAgFnSc4d1	nová
národní	národní	k2eAgFnSc4d1	národní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
plnila	plnit	k5eAaImAgFnS	plnit
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
tvořily	tvořit	k5eAaImAgFnP	tvořit
tři	tři	k4xCgFnPc1	tři
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
široké	široký	k2eAgInPc4d1	široký
vertikální	vertikální	k2eAgInPc4d1	vertikální
pruhy	pruh	k1gInPc4	pruh
<g/>
:	:	kIx,	:
světle	světle	k6eAd1	světle
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
světle	světle	k6eAd1	světle
modrý	modrý	k2eAgMnSc1d1	modrý
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byla	být	k5eAaImAgFnS	být
dekretem	dekret	k1gInSc7	dekret
č.	č.	k?	č.
33	[number]	k4	33
zavedena	zaveden	k2eAgFnSc1d1	zavedena
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
přidáním	přidání	k1gNnSc7	přidání
znaku	znak	k1gInSc2	znak
ve	v	k7c6	v
světle	světle	k6eAd1	světle
modrém	modrý	k2eAgInSc6d1	modrý
štítu	štít	k1gInSc6	štít
doprostřed	doprostřed	k7c2	doprostřed
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
zobrazená	zobrazený	k2eAgFnSc1d1	zobrazená
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
světlejší	světlý	k2eAgInSc1d2	světlejší
odstín	odstín	k1gInSc1	odstín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
modrý	modrý	k2eAgInSc1d1	modrý
štít	štít	k1gInSc1	štít
pod	pod	k7c7	pod
znakem	znak	k1gInSc7	znak
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1968	[number]	k4	1968
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc3d1	poslední
změně	změna	k1gFnSc3	změna
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
odstín	odstín	k1gInSc1	odstín
modrých	modrý	k2eAgInPc2d1	modrý
pruhů	pruh	k1gInPc2	pruh
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
světlejší	světlý	k2eAgInSc4d2	světlejší
a	a	k8xC	a
znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
zmenšen	zmenšit	k5eAaPmNgInS	zmenšit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
znaku	znak	k1gInSc6	znak
byly	být	k5eAaImAgFnP	být
zaměněny	zaměněn	k2eAgFnPc1d1	zaměněna
ratolesti	ratolest	k1gFnPc1	ratolest
kávovníku	kávovník	k1gInSc2	kávovník
za	za	k7c4	za
vavřín	vavřín	k1gInSc4	vavřín
a	a	k8xC	a
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
byla	být	k5eAaImAgFnS	být
i	i	k9	i
modrobílá	modrobílý	k2eAgFnSc1d1	modrobílá
stužka	stužka	k1gFnSc1	stužka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gFnPc4	on
svazovala	svazovat	k5eAaImAgFnS	svazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1997	[number]	k4	1997
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
změně	změna	k1gFnSc3	změna
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
státní	státní	k2eAgFnPc4d1	státní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Quetzal	quetzal	k1gInSc1	quetzal
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hledí	hledět	k5eAaImIp3nS	hledět
heraldicky	heraldicky	k6eAd1	heraldicky
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
slovo	slovo	k1gNnSc1	slovo
setiembre	setiembr	k1gInSc5	setiembr
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
septiembre	septiembr	k1gInSc5	septiembr
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
se	se	k3xPyFc4	se
psal	psát	k5eAaImAgInS	psát
název	název	k1gInSc1	název
měsíce	měsíc	k1gInSc2	měsíc
září	září	k1gNnSc2	září
bez	bez	k7c2	bez
písmena	písmeno	k1gNnSc2	písmeno
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
bývalých	bývalý	k2eAgInPc2d1	bývalý
členů	člen	k1gInPc2	člen
Spojených	spojený	k2eAgFnPc2d1	spojená
provincií	provincie	k1gFnPc2	provincie
==	==	k?	==
</s>
</p>
<p>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
bývalých	bývalý	k2eAgMnPc2d1	bývalý
členů	člen	k1gMnPc2	člen
Spojených	spojený	k2eAgFnPc2d1	spojená
provincií	provincie	k1gFnPc2	provincie
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
Guatemala	Guatemala	k1gFnSc1	Guatemala
v	v	k7c6	v
letech	let	k1gInPc6	let
1823	[number]	k4	1823
<g/>
–	–	k?	–
<g/>
1839	[number]	k4	1839
patřila	patřit	k5eAaImAgFnS	patřit
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
dodnes	dodnes	k6eAd1	dodnes
podobný	podobný	k2eAgInSc4d1	podobný
design	design	k1gInSc4	design
<g/>
.	.	kIx.	.
</s>
<s>
Dominují	dominovat	k5eAaImIp3nP	dominovat
jim	on	k3xPp3gFnPc3	on
modré	modrý	k2eAgInPc1d1	modrý
a	a	k8xC	a
bíle	bíle	k6eAd1	bíle
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
guatemalské	guatemalský	k2eAgFnSc2d1	guatemalská
vlajky	vlajka	k1gFnSc2	vlajka
však	však	k9	však
horizontální	horizontální	k2eAgFnSc1d1	horizontální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
guatemalských	guatemalský	k2eAgInPc2d1	guatemalský
departementů	departement	k1gInPc2	departement
==	==	k?	==
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
administrativně	administrativně	k6eAd1	administrativně
rozčleněn	rozčleněn	k2eAgMnSc1d1	rozčleněn
na	na	k7c4	na
22	[number]	k4	22
</s>
</p>
<p>
<s>
departementů	departement	k1gInPc2	departement
(	(	kIx(	(
<g/>
depatamento	depatamento	k1gNnSc1	depatamento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Guatemaly	Guatemala	k1gFnSc2	Guatemala
</s>
</p>
<p>
<s>
Guatemalská	guatemalský	k2eAgFnSc1d1	guatemalská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Guatemalská	guatemalský	k2eAgFnSc1d1	guatemalská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
