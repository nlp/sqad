<p>
<s>
Gian-Carlo	Gian-Carlo	k1gNnSc1	Gian-Carlo
Coppola	Coppola	k1gFnSc1	Coppola
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
Gio	Gio	k1gFnSc1	Gio
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1963	[number]	k4	1963
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvorozeným	prvorozený	k2eAgMnSc7d1	prvorozený
synem	syn	k1gMnSc7	syn
režiséra	režisér	k1gMnSc2	režisér
Francisa	Francis	k1gMnSc2	Francis
Forda	ford	k1gMnSc2	ford
Coppoly	Coppola	k1gFnSc2	Coppola
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Eleanor	Eleanora	k1gFnPc2	Eleanora
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
dva	dva	k4xCgMnPc4	dva
mladší	mladý	k2eAgMnPc4d2	mladší
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stali	stát	k5eAaPmAgMnP	stát
režiséry	režisér	k1gMnPc4	režisér
–	–	k?	–
bratra	bratr	k1gMnSc4	bratr
Romana	Roman	k1gMnSc4	Roman
a	a	k8xC	a
sestru	sestra	k1gFnSc4	sestra
Sofii	Sofia	k1gFnSc4	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
měl	mít	k5eAaImAgInS	mít
malou	malý	k2eAgFnSc4d1	malá
roli	role	k1gFnSc4	role
v	v	k7c6	v
otcově	otcův	k2eAgInSc6d1	otcův
filmu	film	k1gInSc6	film
Kmotr	kmotr	k1gMnSc1	kmotr
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dalších	další	k2eAgInPc6d1	další
snímcích	snímek	k1gInPc6	snímek
Rozhovor	rozhovor	k1gInSc1	rozhovor
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Apokalypsa	apokalypsa	k1gFnSc1	apokalypsa
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
;	;	kIx,	;
pouze	pouze	k6eAd1	pouze
jinak	jinak	k6eAd1	jinak
sestříhaná	sestříhaný	k2eAgFnSc1d1	sestříhaná
verze	verze	k1gFnSc1	verze
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
Now	Now	k1gFnSc2	Now
Redux	Redux	k1gInSc4	Redux
uvedená	uvedený	k2eAgFnSc1d1	uvedená
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dravé	dravý	k2eAgFnPc1d1	dravá
ryby	ryba	k1gFnPc1	ryba
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
lodní	lodní	k2eAgFnSc6d1	lodní
nehodě	nehoda	k1gFnSc6	nehoda
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
22	[number]	k4	22
let	léto	k1gNnPc2	léto
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
snoubenka	snoubenka	k1gFnSc1	snoubenka
Jacqui	Jacqui	k1gNnSc2	Jacqui
de	de	k?	de
la	la	k1gNnSc2	la
Fontaine	Fontain	k1gInSc5	Fontain
těhotná	těhotný	k2eAgFnSc1d1	těhotná
a	a	k8xC	a
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1987	[number]	k4	1987
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Gia	Gia	k1gFnSc1	Gia
Coppola	Coppola	k1gFnSc1	Coppola
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
následně	následně	k6eAd1	následně
věnoval	věnovat	k5eAaImAgMnS	věnovat
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
snímek	snímek	k1gInSc4	snímek
Tucker	Tucker	k1gMnSc1	Tucker
<g/>
:	:	kIx,	:
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
sen	sen	k1gInSc1	sen
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Rodokmen	rodokmen	k1gInSc1	rodokmen
Coppolových	Coppolový	k2eAgFnPc2d1	Coppolová
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Gian-Carlo	Gian-Carlo	k1gNnSc1	Gian-Carlo
Coppola	Coppola	k1gFnSc1	Coppola
v	v	k7c6	v
Internet	Internet	k1gInSc1	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
