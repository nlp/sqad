<s>
Gian-Carlo	Gian-Carlo	k1gNnSc1	Gian-Carlo
Coppola	Coppola	k1gFnSc1	Coppola
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
Gio	Gio	k1gFnSc1	Gio
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1963	[number]	k4	1963
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvorozeným	prvorozený	k2eAgMnSc7d1	prvorozený
synem	syn	k1gMnSc7	syn
režiséra	režisér	k1gMnSc2	režisér
Francisa	Francis	k1gMnSc2	Francis
Forda	ford	k1gMnSc2	ford
Coppoly	Coppola	k1gFnSc2	Coppola
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Eleanor	Eleanora	k1gFnPc2	Eleanora
<g/>
.	.	kIx.	.
</s>
