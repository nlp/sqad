<s>
Stanley	Stanlea	k1gFnPc1	Stanlea
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
též	též	k9	též
Stanleyův	Stanleyův	k2eAgInSc4d1	Stanleyův
pohár	pohár	k1gInSc4	pohár
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hokejová	hokejový	k2eAgFnSc1d1	hokejová
trofej	trofej	k1gFnSc1	trofej
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
každoročně	každoročně	k6eAd1	každoročně
vítěznému	vítězný	k2eAgInSc3d1	vítězný
týmu	tým	k1gInSc3	tým
playoff	playoff	k1gInSc4	playoff
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
též	též	k9	též
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
The	The	k1gMnSc1	The
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Ten	ten	k3xDgInSc1	ten
Pohár	pohár	k1gInSc1	pohár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Holy	hola	k1gFnSc2	hola
Grail	Grail	k1gMnSc1	Grail
(	(	kIx(	(
<g/>
Svatý	svatý	k2eAgInSc1d1	svatý
grál	grál	k1gInSc1	grál
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Lord	lord	k1gMnSc1	lord
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Mug	Mug	k1gFnSc7	Mug
(	(	kIx(	(
<g/>
Džbánek	džbánek	k1gInSc1	džbánek
Lorda	lord	k1gMnSc2	lord
Stanleyho	Stanley	k1gMnSc2	Stanley
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lord	lord	k1gMnSc1	lord
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
Pohár	pohár	k1gInSc1	pohár
Lorda	lord	k1gMnSc2	lord
Stanleyho	Stanley	k1gMnSc2	Stanley
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
třech	tři	k4xCgFnPc6	tři
hlavních	hlavní	k2eAgFnPc6d1	hlavní
severoamerických	severoamerický	k2eAgFnPc6d1	severoamerická
soutěžích	soutěž	k1gFnPc6	soutěž
(	(	kIx(	(
<g/>
NBA	NBA	kA	NBA
<g/>
,	,	kIx,	,
MLB	MLB	kA	MLB
a	a	k8xC	a
NFL	NFL	kA	NFL
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pohár	pohár	k1gInSc1	pohár
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
nový	nový	k2eAgInSc1d1	nový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Stanley	Stanle	k2eAgInPc1d1	Stanle
Cup	cup	k1gInSc1	cup
koluje	kolovat	k5eAaImIp3nS	kolovat
mezi	mezi	k7c7	mezi
týmy	tým	k1gInPc7	tým
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
ponechat	ponechat	k5eAaPmF	ponechat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
ho	on	k3xPp3gMnSc4	on
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
jiný	jiný	k2eAgInSc1d1	jiný
tým	tým	k1gInSc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podstavec	podstavec	k1gInSc4	podstavec
poháru	pohár	k1gInSc2	pohár
jsou	být	k5eAaImIp3nP	být
vyrývána	vyrýván	k2eAgNnPc1d1	vyrýván
jména	jméno	k1gNnPc1	jméno
trenérů	trenér	k1gMnPc2	trenér
<g/>
,	,	kIx,	,
managementu	management	k1gInSc2	management
a	a	k8xC	a
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
poháru	pohár	k1gInSc2	pohár
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
týmy	tým	k1gInPc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
pohár	pohár	k1gInSc1	pohár
je	být	k5eAaImIp3nS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
ze	z	k7c2	z
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
výšku	výška	k1gFnSc4	výška
18,5	[number]	k4	18,5
centimetru	centimetr	k1gInSc2	centimetr
a	a	k8xC	a
průměr	průměr	k1gInSc1	průměr
29	[number]	k4	29
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
současná	současný	k2eAgFnSc1d1	současná
trofej	trofej	k1gFnSc1	trofej
<g/>
,	,	kIx,	,
podstavec	podstavec	k1gInSc1	podstavec
osazený	osazený	k2eAgInSc1d1	osazený
kopií	kopie	k1gFnSc7	kopie
originálního	originální	k2eAgInSc2d1	originální
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
ze	z	k7c2	z
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
slitiny	slitina	k1gFnSc2	slitina
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
89,54	[number]	k4	89,54
centimetrů	centimetr	k1gInPc2	centimetr
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
15,5	[number]	k4	15,5
kilogramu	kilogram	k1gInSc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
zněl	znět	k5eAaImAgInS	znět
Dominion	dominion	k1gNnSc4	dominion
Hockey	Hockea	k1gFnSc2	Hockea
Challenge	Challenge	k1gFnPc2	Challenge
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
trofej	trofej	k1gFnSc1	trofej
byla	být	k5eAaImAgFnS	být
věnována	věnován	k2eAgFnSc1d1	věnována
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
kanadským	kanadský	k2eAgMnSc7d1	kanadský
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
Lordem	lord	k1gMnSc7	lord
Stanleym	Stanleym	k1gInSc4	Stanleym
z	z	k7c2	z
Prestonu	Preston	k1gInSc2	Preston
jako	jako	k8xS	jako
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
nejlépe	dobře	k6eAd3	dobře
umístěný	umístěný	k2eAgInSc4d1	umístěný
hokejový	hokejový	k2eAgInSc4d1	hokejový
amatérský	amatérský	k2eAgInSc4d1	amatérský
klub	klub	k1gInSc4	klub
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
profesionální	profesionální	k2eAgFnPc1d1	profesionální
ligy	liga	k1gFnPc1	liga
<g/>
,	,	kIx,	,
NHA	NHA	kA	NHA
a	a	k8xC	a
PCHA	pcha	k0wR	pcha
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
Stanley	Stanle	k2eAgInPc4d1	Stanle
Cup	cup	k1gInSc4	cup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
době	doba	k1gFnSc6	doba
slučování	slučování	k1gNnSc6	slučování
a	a	k8xC	a
odstupování	odstupování	k1gNnSc6	odstupování
týmů	tým	k1gInPc2	tým
se	se	k3xPyFc4	se
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
stal	stát	k5eAaPmAgInS	stát
trofejí	trofej	k1gFnSc7	trofej
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnPc4	vítěz
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
jmenování	jmenování	k1gNnSc6	jmenování
kanadským	kanadský	k2eAgMnPc3d1	kanadský
guvernérem	guvernér	k1gMnSc7	guvernér
královnou	královna	k1gFnSc7	královna
Viktorií	Viktoria	k1gFnSc7	Viktoria
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1888	[number]	k4	1888
se	se	k3xPyFc4	se
Lord	lord	k1gMnSc1	lord
Stanley	Stanlea	k1gFnSc2	Stanlea
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
přímo	přímo	k6eAd1	přímo
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
na	na	k7c6	na
Zimním	zimní	k2eAgInSc6d1	zimní
karnevalu	karneval	k1gInSc6	karneval
v	v	k7c6	v
Montréalu	Montréal	k1gInSc6	Montréal
viděl	vidět	k5eAaImAgInS	vidět
poprvé	poprvé	k6eAd1	poprvé
hokejový	hokejový	k2eAgInSc1d1	hokejový
zápas	zápas	k1gInSc1	zápas
mezi	mezi	k7c4	mezi
Montreal	Montreal	k1gInSc4	Montreal
Victorias	Victoriasa	k1gFnPc2	Victoriasa
a	a	k8xC	a
Montreal	Montreal	k1gInSc4	Montreal
Hockey	Hockea	k1gFnSc2	Hockea
Club	club	k1gInSc1	club
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
ještě	ještě	k9	ještě
ligový	ligový	k2eAgInSc4d1	ligový
hokej	hokej	k1gInSc4	hokej
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
Montreal	Montreal	k1gInSc1	Montreal
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Ottawa	Ottawa	k1gFnSc1	Ottawa
měly	mít	k5eAaImAgFnP	mít
něco	něco	k3yInSc4	něco
podobajícího	podobající	k2eAgNnSc2d1	podobající
se	se	k3xPyFc4	se
ligám	liga	k1gFnPc3	liga
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
Lorda	lord	k1gMnSc2	lord
Stanleyho	Stanley	k1gMnSc2	Stanley
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Arthur	Arthura	k1gFnPc2	Arthura
a	a	k8xC	a
Algernon	Algernona	k1gFnPc2	Algernona
založili	založit	k5eAaPmAgMnP	založit
nový	nový	k2eAgInSc4d1	nový
tým	tým	k1gInSc4	tým
zvaný	zvaný	k2eAgInSc4d1	zvaný
Ottawa	Ottawa	k1gFnSc1	Ottawa
Rideau	Rideaus	k1gInSc2	Rideaus
Rebels	Rebelsa	k1gFnPc2	Rebelsa
a	a	k8xC	a
Arthur	Arthura	k1gFnPc2	Arthura
navíc	navíc	k6eAd1	navíc
hrál	hrát	k5eAaImAgMnS	hrát
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgFnPc2d1	klíčová
rolí	role	k1gFnPc2	role
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
Ontario	Ontario	k1gNnSc4	Ontario
Hockey	Hockea	k1gFnSc2	Hockea
Association	Association	k1gInSc4	Association
a	a	k8xC	a
také	také	k9	také
přinesl	přinést	k5eAaPmAgInS	přinést
hokej	hokej	k1gInSc1	hokej
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
pak	pak	k6eAd1	pak
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
věnoval	věnovat	k5eAaImAgMnS	věnovat
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
tým	tým	k1gInSc4	tým
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaImAgMnS	učinit
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
si	se	k3xPyFc3	se
Lord	lord	k1gMnSc1	lord
Stanley	Stanlea	k1gFnSc2	Stanlea
koupil	koupit	k5eAaPmAgMnS	koupit
punčovou	punčový	k2eAgFnSc4d1	punčová
mísu	mísa	k1gFnSc4	mísa
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
v	v	k7c4	v
Sheffieldu	Sheffielda	k1gFnSc4	Sheffielda
od	od	k7c2	od
londýnského	londýnský	k2eAgMnSc2d1	londýnský
stříbrníka	stříbrník	k1gMnSc2	stříbrník
G.	G.	kA	G.
R.	R.	kA	R.
Collise	Collise	k1gFnSc1	Collise
za	za	k7c4	za
sumu	suma	k1gFnSc4	suma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
padesáti	padesát	k4xCc3	padesát
americkým	americký	k2eAgInPc3d1	americký
dolarům	dolar	k1gInPc3	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
vyryl	vyrýt	k5eAaPmAgMnS	vyrýt
jméno	jméno	k1gNnSc4	jméno
Dominion	dominion	k1gNnSc4	dominion
Hockey	Hockea	k1gFnSc2	Hockea
Challenge	Challenge	k1gNnSc2	Challenge
Cup	cup	k1gInSc1	cup
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Od	od	k7c2	od
Stanleyho	Stanley	k1gMnSc2	Stanley
z	z	k7c2	z
Prestonu	Preston	k1gInSc2	Preston
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Stanley	Stanlea	k1gMnSc2	Stanlea
nikdy	nikdy	k6eAd1	nikdy
neviděl	vidět	k5eNaImAgMnS	vidět
zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
Stanley	Stanle	k2eAgInPc4d1	Stanle
Cup	cup	k1gInSc4	cup
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
pohár	pohár	k1gInSc1	pohár
nepředával	předávat	k5eNaImAgInS	předávat
vítězi	vítěz	k1gMnSc3	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
působení	působení	k1gNnSc1	působení
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
guvernéra	guvernér	k1gMnSc2	guvernér
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
září	září	k1gNnSc6	září
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
totiž	totiž	k9	totiž
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Edward	Edward	k1gMnSc1	Edward
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
Derby	derby	k1gNnSc4	derby
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lord	lord	k1gMnSc1	lord
Stanley	Stanlea	k1gFnSc2	Stanlea
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
éře	éra	k1gFnSc6	éra
všechny	všechen	k3xTgFnPc4	všechen
ligy	liga	k1gFnPc4	liga
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
soutěžilo	soutěžit	k5eAaImAgNnS	soutěžit
o	o	k7c4	o
Stanley	Stanlea	k1gFnPc4	Stanlea
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
neměly	mít	k5eNaImAgFnP	mít
playoff	playoff	k1gInSc4	playoff
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
pohár	pohár	k1gInSc1	pohár
připadl	připadnout	k5eAaPmAgInS	připadnout
vítězi	vítěz	k1gMnPc7	vítěz
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
ale	ale	k9	ale
čtyři	čtyři	k4xCgInPc4	čtyři
z	z	k7c2	z
pěti	pět	k4xCc2	pět
týmů	tým	k1gInPc2	tým
AHAC	AHAC	kA	AHAC
podaly	podat	k5eAaPmAgFnP	podat
stejný	stejný	k2eAgInSc4d1	stejný
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
stejně	stejně	k6eAd1	stejně
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
liga	liga	k1gFnSc1	liga
neměla	mít	k5eNaImAgFnS	mít
žádný	žádný	k3yNgInSc4	žádný
rozhodovací	rozhodovací	k2eAgInSc4d1	rozhodovací
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
jednáních	jednání	k1gNnPc6	jednání
a	a	k8xC	a
odstoupení	odstoupení	k1gNnSc4	odstoupení
Quebecu	Quebecus	k1gInSc2	Quebecus
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zbývající	zbývající	k2eAgInPc1d1	zbývající
tři	tři	k4xCgInPc1	tři
týmy	tým	k1gInPc1	tým
utkají	utkat	k5eAaPmIp3nP	utkat
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
a	a	k8xC	a
Ottawa	Ottawa	k1gFnSc1	Ottawa
bude	být	k5eAaImBp3nS	být
automaticky	automaticky	k6eAd1	automaticky
posunuta	posunout	k5eAaPmNgFnS	posunout
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
jediným	jediný	k2eAgInSc7d1	jediný
týmem	tým	k1gInSc7	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hrál	hrát	k5eAaImAgInS	hrát
nejméně	málo	k6eAd3	málo
zápasů	zápas	k1gInPc2	zápas
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
playoff	playoff	k1gInSc1	playoff
o	o	k7c4	o
Stanley	Stanlea	k1gFnPc4	Stanlea
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
Montreal	Montreal	k1gInSc4	Montreal
Hockey	Hockea	k1gFnSc2	Hockea
Club	club	k1gInSc1	club
porazil	porazit	k5eAaPmAgInS	porazit
Montreal	Montreal	k1gInSc1	Montreal
Victorias	Victorias	k1gInSc1	Victorias
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
finálovém	finálový	k2eAgInSc6d1	finálový
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
Montreal	Montreal	k1gInSc4	Montreal
Hockey	Hockea	k1gFnSc2	Hockea
Club	club	k1gInSc1	club
porazil	porazit	k5eAaPmAgMnS	porazit
Ottawa	Ottawa	k1gFnSc1	Ottawa
Generals	Generals	k1gInSc1	Generals
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Queen	Queen	k1gInSc1	Queen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
University	universita	k1gFnSc2	universita
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
přebrat	přebrat	k5eAaPmF	přebrat
Stanley	Stanlea	k1gMnSc2	Stanlea
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
trochu	trochu	k6eAd1	trochu
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Montreal	Montreal	k1gInSc1	Montreal
Victorias	Victorias	k1gMnSc1	Victorias
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
základní	základní	k2eAgFnSc4d1	základní
část	část	k1gFnSc4	část
sezony	sezona	k1gFnSc2	sezona
a	a	k8xC	a
komisaři	komisar	k1gMnPc1	komisar
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
pohár	pohár	k1gInSc4	pohár
budou	být	k5eAaImBp3nP	být
hrát	hrát	k5eAaImF	hrát
bývalí	bývalý	k2eAgMnPc1d1	bývalý
vítězové	vítěz	k1gMnPc1	vítěz
Montreal	Montreal	k1gInSc4	Montreal
Hockey	Hockea	k1gMnSc2	Hockea
Club	club	k1gInSc4	club
proti	proti	k7c3	proti
Queen	Queno	k1gNnPc2	Queno
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
University	universita	k1gFnPc1	universita
a	a	k8xC	a
že	že	k8xS	že
když	když	k8xS	když
Montreal	Montreal	k1gInSc1	Montreal
zápas	zápas	k1gInSc1	zápas
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
,	,	kIx,	,
Victorias	Victorias	k1gInSc1	Victorias
pohár	pohár	k1gInSc1	pohár
získají	získat	k5eAaPmIp3nP	získat
<g/>
.	.	kIx.	.
</s>
<s>
Montreal	Montreal	k1gInSc4	Montreal
Hockey	Hockea	k1gFnSc2	Hockea
Club	club	k1gInSc1	club
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
městští	městský	k2eAgMnPc1d1	městský
rivalové	rival	k1gMnPc1	rival
získali	získat	k5eAaPmAgMnP	získat
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
týmem	tým	k1gInSc7	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
minulým	minulý	k2eAgMnSc7d1	minulý
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Winnipeg	Winnipeg	k1gInSc4	Winnipeg
Victorias	Victoriasa	k1gFnPc2	Victoriasa
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1896	[number]	k4	1896
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stali	stát	k5eAaPmAgMnP	stát
prvním	první	k4xOgInSc7	první
týmem	tým	k1gInSc7	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
a	a	k8xC	a
nepocházel	pocházet	k5eNaImAgMnS	pocházet
z	z	k7c2	z
AHAC	AHAC	kA	AHAC
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
prestiží	prestiž	k1gFnSc7	prestiž
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
rostla	růst	k5eAaImAgFnS	růst
potřeba	potřeba	k1gFnSc1	potřeba
vrcholových	vrcholový	k2eAgMnPc2d1	vrcholový
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Komisaři	komisar	k1gMnPc1	komisar
přijali	přijmout	k5eAaPmAgMnP	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
ECAHA	ECAHA	kA	ECAHA
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
nejkvalitnější	kvalitní	k2eAgFnSc7d3	nejkvalitnější
hokejovou	hokejový	k2eAgFnSc7d1	hokejová
ligou	liga	k1gFnSc7	liga
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenším	malý	k2eAgNnSc7d3	nejmenší
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
kdy	kdy	k6eAd1	kdy
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Kenora	Kenora	k1gFnSc1	Kenora
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
čítala	čítat	k5eAaImAgFnS	čítat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
výhry	výhra	k1gFnSc2	výhra
čtyři	čtyři	k4xCgInPc4	čtyři
tisíce	tisíc	k4xCgInPc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Stanley	Stanlea	k1gFnPc1	Stanlea
Cup	cup	k1gInSc1	cup
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Kenora	Kenor	k1gMnSc4	Kenor
Thistles	Thistles	k1gInSc4	Thistles
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1907	[number]	k4	1907
a	a	k8xC	a
pomohli	pomoct	k5eAaPmAgMnP	pomoct
jí	on	k3xPp3gFnSc3	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
pozdější	pozdní	k2eAgMnPc1d2	pozdější
členové	člen	k1gMnPc1	člen
Hokejové	hokejový	k2eAgFnSc2d1	hokejová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Art	Art	k1gFnSc2	Art
Ross	Ross	k1gInSc1	Ross
a	a	k8xC	a
Joe	Joe	k1gMnSc1	Joe
Hall	Hall	k1gMnSc1	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Thistles	Thistles	k1gInSc4	Thistles
získali	získat	k5eAaPmAgMnP	získat
pohár	pohár	k1gInSc4	pohár
od	od	k7c2	od
Montreal	Montreal	k1gInSc1	Montreal
Wanderers	Wanderers	k1gInSc1	Wanderers
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
Stanley	Stanlea	k1gFnPc1	Stanlea
Cup	cup	k1gInSc4	cup
ubránit	ubránit	k5eAaPmF	ubránit
proti	proti	k7c3	proti
Brandon	Brandon	k1gNnSc4	Brandon
Elks	Elks	k1gInSc4	Elks
<g/>
.	.	kIx.	.
</s>
<s>
Wanderers	Wanderers	k6eAd1	Wanderers
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1907	[number]	k4	1907
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
na	na	k7c4	na
odvetu	odveta	k1gFnSc4	odveta
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Thistles	Thistles	k1gInSc4	Thistles
posílili	posílit	k5eAaPmAgMnP	posílit
kádr	kádr	k1gInSc4	kádr
<g/>
,	,	kIx,	,
Wanderers	Wanderers	k1gInSc4	Wanderers
jim	on	k3xPp3gMnPc3	on
pohár	pohár	k1gInSc4	pohár
sebrali	sebrat	k5eAaPmAgMnP	sebrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
se	se	k3xPyFc4	se
Allan	Allan	k1gMnSc1	Allan
Cup	cup	k1gInSc4	cup
stal	stát	k5eAaPmAgMnS	stát
trofejí	trofej	k1gFnSc7	trofej
pro	pro	k7c4	pro
amatérské	amatérský	k2eAgInPc4d1	amatérský
týmy	tým	k1gInPc4	tým
a	a	k8xC	a
Stanley	Stanle	k2eAgFnPc1d1	Stanle
Cup	cup	k1gInSc4	cup
trofejí	trofej	k1gFnPc2	trofej
pro	pro	k7c4	pro
profesionály	profesionál	k1gMnPc4	profesionál
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgInS	začít
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
Stanley	Stanlea	k1gFnPc4	Stanlea
Cup	cup	k1gInSc4	cup
první	první	k4xOgInSc4	první
tým	tým	k1gInSc4	tým
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
profesionálů	profesionál	k1gMnPc2	profesionál
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
Trolley	Trollea	k1gFnSc2	Trollea
Leaguers	Leaguersa	k1gFnPc2	Leaguersa
z	z	k7c2	z
OPHL	OPHL	kA	OPHL
(	(	kIx(	(
<g/>
Ontario	Ontario	k1gNnSc1	Ontario
Professional	Professional	k1gFnSc2	Professional
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
poslední	poslední	k2eAgInPc4d1	poslední
amatérské	amatérský	k2eAgInPc4d1	amatérský
týmy	tým	k1gInPc4	tým
Montreal	Montreal	k1gInSc1	Montreal
Hockey	Hockey	k1gInPc1	Hockey
Club	club	k1gInSc1	club
a	a	k8xC	a
Montreal	Montreal	k1gInSc1	Montreal
Victorias	Victoriasa	k1gFnPc2	Victoriasa
odstoupily	odstoupit	k5eAaPmAgInP	odstoupit
z	z	k7c2	z
ECAHA	ECAHA	kA	ECAHA
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
také	také	k6eAd1	také
profesionální	profesionální	k2eAgFnSc7d1	profesionální
ligou	liga	k1gFnSc7	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
NHA	NHA	kA	NHA
(	(	kIx(	(
<g/>
National	National	k1gMnSc1	National
Hockey	Hockea	k1gFnSc2	Hockea
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
stala	stát	k5eAaPmAgFnS	stát
tou	ten	k3xDgFnSc7	ten
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
a	a	k8xC	a
udržela	udržet	k5eAaPmAgFnS	udržet
Stanley	Stanle	k2eAgInPc4d1	Stanle
Cup	cup	k1gInSc4	cup
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
moci	moc	k1gFnSc6	moc
po	po	k7c4	po
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
zápasy	zápas	k1gInPc1	zápas
o	o	k7c4	o
přebrání	přebrání	k1gNnSc4	přebrání
konat	konat	k5eAaImF	konat
v	v	k7c4	v
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
čas	čas	k1gInSc4	čas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
komisaři	komisar	k1gMnPc1	komisar
ale	ale	k8xC	ale
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
zavedli	zavést	k5eAaPmAgMnP	zavést
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohár	pohár	k1gInSc1	pohár
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přebrat	přebrat	k5eAaPmF	přebrat
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
sezony	sezona	k1gFnSc2	sezona
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gNnSc4	on
nyní	nyní	k6eAd1	nyní
drží	držet	k5eAaImIp3nS	držet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
se	s	k7c7	s
NHA	NHA	kA	NHA
a	a	k8xC	a
PCHA	pcha	k0wR	pcha
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnPc1	jejich
šampioni	šampion	k1gMnPc1	šampion
se	se	k3xPyFc4	se
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
utkají	utkat	k5eAaPmIp3nP	utkat
o	o	k7c4	o
Stanley	Stanle	k2eAgInPc4d1	Stanle
Cup	cup	k1gInSc4	cup
<g/>
.	.	kIx.	.
</s>
<s>
Komisaři	komisar	k1gMnPc1	komisar
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
Allan	Allan	k1gMnSc1	Allan
Cup	cup	k1gInSc4	cup
stal	stát	k5eAaPmAgMnS	stát
trofejí	trofej	k1gFnSc7	trofej
pro	pro	k7c4	pro
amatérské	amatérský	k2eAgInPc4d1	amatérský
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
dvou	dva	k4xCgFnPc6	dva
ligách	liga	k1gFnPc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
finále	finále	k1gNnSc1	finále
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
tým	tým	k1gInSc4	tým
PCHA	pcha	k0wR	pcha
Vancouver	Vancouver	k1gInSc1	Vancouver
Millionaires	Millionaires	k1gInSc1	Millionaires
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
stali	stát	k5eAaPmAgMnP	stát
částí	část	k1gFnPc2	část
PCHA	pcha	k0wR	pcha
Portland	Portland	k1gInSc1	Portland
Rosebuds	Rosebudsa	k1gFnPc2	Rosebudsa
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
komisaři	komisar	k1gMnPc1	komisar
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohár	pohár	k1gInSc1	pohár
už	už	k6eAd1	už
nebude	být	k5eNaImBp3nS	být
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
tým	tým	k1gInSc4	tým
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
tým	tým	k1gInSc4	tým
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
Rosebuds	Rosebuds	k1gInSc1	Rosebuds
poprvé	poprvé	k6eAd1	poprvé
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Stanleyův	Stanleyův	k2eAgInSc1d1	Stanleyův
pohár	pohár	k1gInSc1	pohár
první	první	k4xOgMnSc1	první
americký	americký	k2eAgInSc1d1	americký
tým	tým	k1gInSc1	tým
Seattle	Seattle	k1gFnSc2	Seattle
Metropolitans	Metropolitansa	k1gFnPc2	Metropolitansa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
sezoně	sezona	k1gFnSc6	sezona
byla	být	k5eAaImAgFnS	být
NHA	NHA	kA	NHA
rozpuštěna	rozpustit	k5eAaPmNgFnS	rozpustit
a	a	k8xC	a
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
epidemie	epidemie	k1gFnSc1	epidemie
španělské	španělský	k2eAgFnSc2d1	španělská
chřipky	chřipka	k1gFnSc2	chřipka
donutila	donutit	k5eAaPmAgFnS	donutit
zrušit	zrušit	k5eAaPmF	zrušit
finále	finále	k1gNnSc7	finále
mezi	mezi	k7c4	mezi
Montreal	Montreal	k1gInSc4	Montreal
Canadiens	Canadiensa	k1gFnPc2	Canadiensa
a	a	k8xC	a
Seattle	Seattle	k1gFnSc2	Seattle
Metropolitans	Metropolitansa	k1gFnPc2	Metropolitansa
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poslední	poslední	k2eAgInSc4d1	poslední
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgInSc4	pátý
zápas	zápas	k1gInSc4	zápas
se	se	k3xPyFc4	se
nehrál	hrát	k5eNaImAgMnS	hrát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
manažer	manažer	k1gMnSc1	manažer
Canadiens	Canadiensa	k1gFnPc2	Canadiensa
George	Georg	k1gMnSc2	Georg
Kennedy	Kenneda	k1gMnSc2	Kenneda
a	a	k8xC	a
hráči	hráč	k1gMnPc1	hráč
Joe	Joe	k1gMnSc1	Joe
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
Billy	Bill	k1gMnPc4	Bill
Coutu	Couta	k1gMnSc4	Couta
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
McDonald	McDonald	k1gMnSc1	McDonald
a	a	k8xC	a
Newsy	Newsa	k1gFnPc1	Newsa
Lalonde	Lalond	k1gInSc5	Lalond
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
chřipkou	chřipka	k1gFnSc7	chřipka
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
dny	den	k1gInPc1	den
po	po	k7c6	po
zrušeném	zrušený	k2eAgInSc6d1	zrušený
zápase	zápas	k1gInSc6	zápas
Hall	Hall	k1gMnSc1	Hall
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Formát	formát	k1gInSc1	formát
finále	finále	k1gNnSc2	finále
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
nová	nový	k2eAgFnSc1d1	nová
Western	Western	kA	Western
Canada	Canada	k1gFnSc1	Canada
Hockey	Hockea	k1gFnSc2	Hockea
League	Leagu	k1gFnSc2	Leagu
(	(	kIx(	(
<g/>
WCHL	WCHL	kA	WCHL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
o	o	k7c4	o
pohár	pohár	k1gInSc4	pohár
utkávaly	utkávat	k5eAaImAgInP	utkávat
týmy	tým	k1gInPc1	tým
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
lig	liga	k1gFnPc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
šampioni	šampion	k1gMnPc1	šampion
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
v	v	k7c6	v
jakési	jakýsi	k3yIgFnSc6	jakýsi
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
do	do	k7c2	do
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
Stanley	Stanle	k2eAgInPc4d1	Stanle
Cup	cup	k1gInSc4	cup
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
trvalo	trvat	k5eAaImAgNnS	trvat
tři	tři	k4xCgFnPc4	tři
sezony	sezona	k1gFnPc4	sezona
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
PCHA	pcha	k0wR	pcha
a	a	k8xC	a
WCHL	WCHL	kA	WCHL
spojily	spojit	k5eAaPmAgFnP	spojit
ve	v	k7c6	v
Western	Western	kA	Western
Hockey	Hocke	k2eAgInPc4d1	Hocke
League	Leagu	k1gInPc4	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
tým	tým	k1gInSc1	tým
Victoria	Victorium	k1gNnSc2	Victorium
Cougars	Cougars	k1gInSc1	Cougars
stal	stát	k5eAaPmAgMnS	stát
posledním	poslední	k2eAgMnSc7d1	poslední
vítězem	vítěz	k1gMnSc7	vítěz
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nehrál	hrát	k5eNaImAgMnS	hrát
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Western	Western	kA	Western
Hockey	Hockea	k1gFnSc2	Hockea
League	Leagu	k1gFnSc2	Leagu
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
existuje	existovat	k5eAaImIp3nS	existovat
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
juniorská	juniorský	k2eAgFnSc1d1	juniorská
liga	liga	k1gFnSc1	liga
WHL	WHL	kA	WHL
<g/>
)	)	kIx)	)
a	a	k8xC	a
NHL	NHL	kA	NHL
tak	tak	k9	tak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Týmy	tým	k1gInPc1	tým
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
soutěží	soutěž	k1gFnPc2	soutěž
se	se	k3xPyFc4	se
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
přebrat	přebrat	k5eAaPmF	přebrat
Stanley	Stanlea	k1gFnPc4	Stanlea
Cup	cup	k1gInSc4	cup
z	z	k7c2	z
držení	držení	k1gNnSc2	držení
nyní	nyní	k6eAd1	nyní
nejslavnější	slavný	k2eAgFnSc2d3	nejslavnější
ligy	liga	k1gFnSc2	liga
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
nikdy	nikdy	k6eAd1	nikdy
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
tak	tak	k6eAd1	tak
komisaři	komisar	k1gMnPc1	komisar
stanovili	stanovit	k5eAaPmAgMnP	stanovit
nové	nový	k2eAgNnSc4d1	nové
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc4d1	umožňující
týmům	tým	k1gInPc3	tým
NHL	NHL	kA	NHL
nepřijmout	přijmout	k5eNaPmF	přijmout
výzvy	výzva	k1gFnPc4	výzva
od	od	k7c2	od
týmů	tým	k1gInPc2	tým
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
lig	liga	k1gFnPc2	liga
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
chtěly	chtít	k5eAaImAgFnP	chtít
pohár	pohár	k1gInSc4	pohár
přebrat	přebrat	k5eAaPmF	přebrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
soutěžení	soutěžení	k1gNnSc2	soutěžení
o	o	k7c4	o
Stanleyův	Stanleyův	k2eAgInSc4d1	Stanleyův
pohár	pohár	k1gInSc4	pohár
ucházela	ucházet	k5eAaImAgFnS	ucházet
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
World	World	k1gInSc4	World
Hockey	Hockea	k1gFnSc2	Hockea
Association	Association	k1gInSc4	Association
<g/>
,	,	kIx,	,
asi	asi	k9	asi
jediný	jediný	k2eAgMnSc1d1	jediný
vážný	vážný	k2eAgMnSc1d1	vážný
konkurent	konkurent	k1gMnSc1	konkurent
NHL	NHL	kA	NHL
od	od	k7c2	od
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Komisaři	komisar	k1gMnPc1	komisar
ale	ale	k8xC	ale
zamítli	zamítnout	k5eAaPmAgMnP	zamítnout
její	její	k3xOp3gFnSc4	její
žádost	žádost	k1gFnSc4	žádost
a	a	k8xC	a
WHA	WHA	kA	WHA
brzo	brzo	k6eAd1	brzo
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
Pohár	pohár	k1gInSc1	pohár
byl	být	k5eAaImAgInS	být
udělován	udělovat	k5eAaImNgInS	udělovat
šampionům	šampion	k1gMnPc3	šampion
každý	každý	k3xTgMnSc1	každý
rok	rok	k1gInSc4	rok
až	až	k9	až
do	do	k7c2	do
pracovního	pracovní	k2eAgInSc2d1	pracovní
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
NHL	NHL	kA	NHL
a	a	k8xC	a
NHLPA	NHLPA	kA	NHLPA
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnSc1	organizace
slučující	slučující	k2eAgMnPc4d1	slučující
hráče	hráč	k1gMnPc4	hráč
hrající	hrající	k2eAgFnSc2d1	hrající
NHL	NHL	kA	NHL
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
nebyl	být	k5eNaImAgInS	být
pohár	pohár	k1gInSc1	pohár
udělen	udělit	k5eAaPmNgInS	udělit
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
pandemie	pandemie	k1gFnSc2	pandemie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
fanoušků	fanoušek	k1gMnPc2	fanoušek
pak	pak	k6eAd1	pak
začala	začít	k5eAaPmAgFnS	začít
zpochybňovat	zpochybňovat	k5eAaImF	zpochybňovat
exkluzivní	exkluzivní	k2eAgNnSc4d1	exkluzivní
právo	právo	k1gNnSc4	právo
NHL	NHL	kA	NHL
na	na	k7c4	na
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
generální	generální	k2eAgFnSc1d1	generální
guvernérka	guvernérka	k1gFnSc1	guvernérka
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Adrienne	Adrienn	k1gInSc5	Adrienn
Clarksonová	Clarksonová	k1gFnSc1	Clarksonová
dokonce	dokonce	k9	dokonce
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
změnu	změna	k1gFnSc4	změna
poháru	pohár	k1gInSc2	pohár
na	na	k7c4	na
pohár	pohár	k1gInSc4	pohár
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
moc	moc	k6eAd1	moc
populární	populární	k2eAgInSc1d1	populární
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
Clarkson	Clarkson	k1gInSc1	Clarkson
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
ontarijská	ontarijský	k2eAgFnSc1d1	Ontarijská
skupina	skupina	k1gFnSc1	skupina
"	"	kIx"	"
<g/>
Wednesday	Wednesday	k1gInPc1	Wednesday
Nighters	Nightersa	k1gFnPc2	Nightersa
<g/>
"	"	kIx"	"
podala	podat	k5eAaPmAgFnS	podat
žádost	žádost	k1gFnSc4	žádost
na	na	k7c4	na
Ontarijský	ontarijský	k2eAgInSc4d1	ontarijský
hlavní	hlavní	k2eAgInSc4d1	hlavní
soud	soud	k1gInSc4	soud
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
komisaři	komisar	k1gMnPc1	komisar
překročili	překročit	k5eAaPmAgMnP	překročit
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
trofej	trofej	k1gFnSc1	trofej
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
udělena	udělit	k5eAaPmNgFnS	udělit
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
sezonu	sezona	k1gFnSc4	sezona
nehrála	hrát	k5eNaImAgFnS	hrát
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
se	s	k7c7	s
NHL	NHL	kA	NHL
a	a	k8xC	a
navrhovatelé	navrhovatel	k1gMnPc1	navrhovatel
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
NHL	NHL	kA	NHL
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
sezonu	sezona	k1gFnSc4	sezona
hrát	hrát	k5eAaImF	hrát
nebude	být	k5eNaImBp3nS	být
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vedení	vedení	k1gNnSc1	vedení
NHL	NHL	kA	NHL
předat	předat	k5eAaPmF	předat
pohár	pohár	k1gInSc4	pohár
i	i	k9	i
týmu	tým	k1gInSc2	tým
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c4	na
Grey	Grea	k1gFnPc4	Grea
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
určený	určený	k2eAgMnSc1d1	určený
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnSc4	vítěz
CFL	CFL	kA	CFL
(	(	kIx(	(
<g/>
kanadská	kanadský	k2eAgFnSc1d1	kanadská
liga	liga	k1gFnSc1	liga
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k9	i
na	na	k7c4	na
Stanley	Stanle	k2eAgInPc4d1	Stanle
Cup	cup	k1gInSc4	cup
se	se	k3xPyFc4	se
vyrývají	vyrývat	k5eAaImIp3nP	vyrývat
jména	jméno	k1gNnPc4	jméno
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
trenérů	trenér	k1gMnPc2	trenér
<g/>
,	,	kIx,	,
managementu	management	k1gInSc2	management
a	a	k8xC	a
personálu	personál	k1gInSc2	personál
vítězného	vítězný	k2eAgInSc2d1	vítězný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
pravidel	pravidlo	k1gNnPc2	pravidlo
Lorda	lord	k1gMnSc2	lord
Stanleyho	Stanley	k1gMnSc2	Stanley
znělo	znět	k5eAaImAgNnS	znět
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
tým	tým	k1gInSc1	tým
může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
přidat	přidat	k5eAaPmF	přidat
kruhový	kruhový	k2eAgInSc4d1	kruhový
prstenec	prstenec	k1gInSc4	prstenec
na	na	k7c4	na
dolní	dolní	k2eAgFnSc4d1	dolní
část	část	k1gFnSc4	část
poháru	pohár	k1gInSc2	pohár
k	k	k7c3	k
připomenutí	připomenutí	k1gNnSc3	připomenutí
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
prstenec	prstenec	k1gInSc1	prstenec
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
poháru	pohár	k1gInSc3	pohár
ze	z	k7c2	z
spodu	spod	k1gInSc2	spod
týmem	tým	k1gInSc7	tým
Montreal	Montreal	k1gInSc1	Montreal
Hockey	Hockea	k1gFnSc2	Hockea
Club	club	k1gInSc1	club
<g/>
.	.	kIx.	.
</s>
<s>
Týmy	tým	k1gInPc1	tým
doplňovaly	doplňovat	k5eAaImAgFnP	doplňovat
svoje	svůj	k3xOyFgNnSc4	svůj
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
"	"	kIx"	"
<g/>
NÁZEV	název	k1gInSc1	název
TÝMU	tým	k1gInSc2	tým
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
VÍTĚZNÁ	vítězný	k2eAgFnSc1d1	vítězná
SEZONA	sezona	k1gFnSc1	sezona
<g/>
"	"	kIx"	"
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
jeden	jeden	k4xCgInSc4	jeden
prstenec	prstenec	k1gInSc4	prstenec
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
plným	plný	k2eAgNnSc7d1	plné
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
prostorem	prostor	k1gInSc7	prostor
k	k	k7c3	k
vyrývání	vyrývání	k1gNnSc3	vyrývání
a	a	k8xC	a
neochotou	neochota	k1gFnSc7	neochota
k	k	k7c3	k
zaplacení	zaplacení	k1gNnSc3	zaplacení
dalšího	další	k2eAgInSc2d1	další
prstence	prstenec	k1gInSc2	prstenec
začaly	začít	k5eAaPmAgInP	začít
týmy	tým	k1gInPc1	tým
vyrývat	vyrývat	k5eAaImF	vyrývat
svá	svůj	k3xOyFgNnPc4	svůj
jména	jméno	k1gNnPc4	jméno
na	na	k7c4	na
mísu	mísa	k1gFnSc4	mísa
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
týmem	tým	k1gInSc7	tým
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
podepsal	podepsat	k5eAaPmAgMnS	podepsat
<g/>
"	"	kIx"	"
zevnitř	zevnitř	k6eAd1	zevnitř
do	do	k7c2	do
misky	miska	k1gFnSc2	miska
byli	být	k5eAaImAgMnP	být
Montreal	Montreal	k1gInSc4	Montreal
Wanderers	Wanderersa	k1gFnPc2	Wanderersa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
i	i	k9	i
prvními	první	k4xOgNnPc7	první
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vyryli	vyrýt	k5eAaPmAgMnP	vyrýt
jména	jméno	k1gNnSc2	jméno
všech	všecek	k3xTgFnPc2	všecek
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
Wanderers	Wanderersa	k1gFnPc2	Wanderersa
z	z	k7c2	z
neznámých	neznámá	k1gFnPc2	neznámá
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
museli	muset	k5eAaImAgMnP	muset
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
nevyryli	vyrýt	k5eNaPmAgMnP	vyrýt
na	na	k7c4	na
pohár	pohár	k1gInSc4	pohár
své	svůj	k3xOyFgNnSc1	svůj
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
Ottawa	Ottawa	k1gFnSc1	Ottawa
Senators	Senators	k1gInSc1	Senators
přidali	přidat	k5eAaPmAgMnP	přidat
na	na	k7c4	na
Stanley	Stanlea	k1gFnPc4	Stanlea
Cup	cup	k1gInSc4	cup
druhý	druhý	k4xOgInSc1	druhý
prstenec	prstenec	k1gInSc1	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
Wanderers	Wanderersa	k1gFnPc2	Wanderersa
ani	ani	k8xC	ani
Senators	Senatorsa	k1gFnPc2	Senatorsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
nezapsali	zapsat	k5eNaPmAgMnP	zapsat
svá	svůj	k3xOyFgNnPc4	svůj
vítězství	vítězství	k1gNnPc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
se	se	k3xPyFc4	se
Vancouver	Vancouver	k1gMnSc1	Vancouver
Millionaires	Millionaires	k1gMnSc1	Millionaires
stali	stát	k5eAaPmAgMnP	stát
druhým	druhý	k4xOgNnSc7	druhý
týmem	tým	k1gInSc7	tým
který	který	k3yIgMnSc1	který
zapsal	zapsat	k5eAaPmAgInS	zapsat
i	i	k9	i
jména	jméno	k1gNnPc1	jméno
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
do	do	k7c2	do
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
mísy	mísa	k1gFnSc2	mísa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
právě	právě	k9	právě
Millionaires	Millionaires	k1gInSc1	Millionaires
zaplnili	zaplnit	k5eAaPmAgMnP	zaplnit
poslední	poslední	k2eAgNnSc4d1	poslední
volné	volný	k2eAgNnSc4d1	volné
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
prstenec	prstenec	k1gInSc1	prstenec
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
byl	být	k5eAaImAgInS	být
koupen	koupen	k2eAgInSc1d1	koupen
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
týmem	tým	k1gInSc7	tým
Montreal	Montreal	k1gInSc1	Montreal
Canadiens	Canadiensa	k1gFnPc2	Canadiensa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
vyrývání	vyrývání	k1gNnSc4	vyrývání
hráčů	hráč	k1gMnPc2	hráč
týmu	tým	k1gInSc2	tým
na	na	k7c4	na
pohár	pohár	k1gInSc4	pohár
tradicí	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
vítězové	vítěz	k1gMnPc1	vítěz
přidali	přidat	k5eAaPmAgMnP	přidat
nový	nový	k2eAgInSc4d1	nový
prstenec	prstenec	k1gInSc4	prstenec
což	což	k9	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
růst	růst	k1gInSc4	růst
Stanley	Stanlea	k1gMnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tak	tak	k6eAd1	tak
říkat	říkat	k5eAaImF	říkat
"	"	kIx"	"
<g/>
Stovepipe	Stovepip	k1gMnSc5	Stovepip
Cup	cup	k0	cup
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
stovepipe	stovepipat	k5eAaPmIp3nS	stovepipat
je	on	k3xPp3gNnSc4	on
česky	česky	k6eAd1	česky
kouřová	kouřový	k2eAgFnSc1d1	kouřová
roura	roura	k1gFnSc1	roura
<g/>
)	)	kIx)	)
protože	protože	k8xS	protože
začínal	začínat	k5eAaImAgMnS	začínat
připomínat	připomínat	k5eAaImF	připomínat
výfukovou	výfukový	k2eAgFnSc4d1	výfuková
trubku	trubka	k1gFnSc4	trubka
u	u	k7c2	u
kamen	kamna	k1gNnPc2	kamna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
design	design	k1gInSc1	design
se	se	k3xPyFc4	se
jevil	jevit	k5eAaImAgInS	jevit
jako	jako	k9	jako
nepraktický	praktický	k2eNgInSc1d1	nepraktický
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
pohár	pohár	k1gInSc4	pohár
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
předělán	předělat	k5eAaPmNgMnS	předělat
na	na	k7c4	na
dvoudílnou	dvoudílný	k2eAgFnSc4d1	dvoudílná
trofej	trofej	k1gFnSc4	trofej
doutníkovitého	doutníkovitý	k2eAgInSc2d1	doutníkovitý
tvaru	tvar	k1gInSc2	tvar
s	s	k7c7	s
odstranitelnou	odstranitelný	k2eAgFnSc7d1	odstranitelná
mísou	mísa	k1gFnSc7	mísa
a	a	k8xC	a
límečkem	límeček	k1gInSc7	límeček
<g/>
.	.	kIx.	.
</s>
<s>
Komisaři	komisar	k1gMnPc1	komisar
také	také	k9	také
připsali	připsat	k5eAaPmAgMnP	připsat
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
pohár	pohár	k1gInSc4	pohár
"	"	kIx"	"
<g/>
nepodepsaly	podepsat	k5eNaPmAgInP	podepsat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
pohár	pohár	k1gInSc1	pohár
podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
několik	několik	k4yIc4	několik
menších	malý	k2eAgFnPc2d2	menší
úprav	úprava	k1gFnPc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
mísa	mísa	k1gFnSc1	mísa
a	a	k8xC	a
límeček	límeček	k1gInSc1	límeček
byly	být	k5eAaImAgInP	být
moc	moc	k6eAd1	moc
křehké	křehký	k2eAgFnPc1d1	křehká
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
a	a	k8xC	a
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
nový	nový	k2eAgInSc1d1	nový
jednodílný	jednodílný	k2eAgInSc1d1	jednodílný
pohár	pohár	k1gInSc1	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
válec	válec	k1gInSc1	válec
byl	být	k5eAaImAgInS	být
odstraněn	odstranit	k5eAaPmNgInS	odstranit
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
ho	on	k3xPp3gMnSc4	on
nový	nový	k2eAgMnSc1d1	nový
s	s	k7c7	s
pěti	pět	k4xCc7	pět
prstenci	prstenec	k1gInPc7	prstenec
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
měl	mít	k5eAaImAgInS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
třináct	třináct	k4xCc4	třináct
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
pohár	pohár	k1gInSc1	pohár
navržen	navrhnout	k5eAaPmNgInS	navrhnout
aby	aby	kYmCp3nP	aby
prstence	prstenec	k1gInPc1	prstenec
byly	být	k5eAaImAgInP	být
plné	plný	k2eAgInPc1d1	plný
ve	v	k7c6	v
stém	stý	k4xOgInSc6	stý
roce	rok	k1gInSc6	rok
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byl	být	k5eAaImAgInS	být
špatně	špatně	k6eAd1	špatně
vyryt	vyryt	k2eAgInSc1d1	vyryt
tým	tým	k1gInSc1	tým
Canadiens	Canadiensa	k1gFnPc2	Canadiensa
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
přepsán	přepsat	k5eAaPmNgMnS	přepsat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
prstenců	prstenec	k1gInPc2	prstenec
pouze	pouze	k6eAd1	pouze
12	[number]	k4	12
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
prstence	prstenec	k1gInPc1	prstenec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
naplnily	naplnit	k5eAaPmAgInP	naplnit
<g/>
,	,	kIx,	,
Hokejová	hokejový	k2eAgFnSc1d1	hokejová
síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
si	se	k3xPyFc3	se
ten	ten	k3xDgInSc4	ten
vrchní	vrchní	k1gFnSc1	vrchní
vzala	vzít	k5eAaPmAgFnS	vzít
a	a	k8xC	a
na	na	k7c4	na
spodek	spodek	k1gInSc4	spodek
připevnila	připevnit	k5eAaPmAgFnS	připevnit
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
prázdný	prázdný	k2eAgInSc4d1	prázdný
prstenec	prstenec	k1gInSc4	prstenec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pohár	pohár	k1gInSc4	pohár
už	už	k6eAd1	už
dál	daleko	k6eAd2	daleko
růst	růst	k5eAaImF	růst
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
nový	nový	k2eAgInSc1d1	nový
prstenec	prstenec	k1gInSc1	prstenec
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
přidán	přidat	k5eAaPmNgInS	přidat
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
sezony	sezona	k1gFnSc2	sezona
2004-05	[number]	k4	2004-05
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
stávce	stávka	k1gFnSc3	stávka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
následující	následující	k2eAgFnSc6d1	následující
sezoně	sezona	k1gFnSc6	sezona
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Carolina	Carolin	k2eAgFnSc1d1	Carolina
Hurricanes	Hurricanes	k1gInSc1	Hurricanes
<g/>
.	.	kIx.	.
</s>
<s>
Zrušená	zrušený	k2eAgFnSc1d1	zrušená
sezona	sezona	k1gFnSc1	sezona
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
jako	jako	k9	jako
nehraná	hraný	k2eNgFnSc1d1	nehraná
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
je	být	k5eAaImIp3nS	být
Stanley	Stanley	k1gInPc4	Stanley
Cup	cup	k1gInSc1	cup
89,5	[number]	k4	89,5
centimetru	centimetr	k1gInSc2	centimetr
vysoký	vysoký	k2eAgMnSc1d1	vysoký
a	a	k8xC	a
15	[number]	k4	15
a	a	k8xC	a
půl	půl	k1xP	půl
kilogramu	kilogram	k1gInSc2	kilogram
těžký	těžký	k2eAgMnSc1d1	těžký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Stanleyův	Stanleyův	k2eAgInSc4d1	Stanleyův
pohár	pohár	k1gInSc4	pohár
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyryta	vyrýt	k5eAaPmNgFnS	vyrýt
pouze	pouze	k6eAd1	pouze
jména	jméno	k1gNnPc1	jméno
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
za	za	k7c4	za
tým	tým	k1gInSc4	tým
odehráli	odehrát	k5eAaPmAgMnP	odehrát
41	[number]	k4	41
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
a	a	k8xC	a
nepřestoupili	přestoupit	k5eNaPmAgMnP	přestoupit
pryč	pryč	k6eAd1	pryč
nebo	nebo	k8xC	nebo
hráli	hrát	k5eAaImAgMnP	hrát
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
zápas	zápas	k1gInSc4	zápas
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Konstantinov	Konstantinov	k1gInSc1	Konstantinov
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
kariéra	kariéra	k1gFnSc1	kariéra
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
při	při	k7c6	při
autonehodě	autonehoda	k1gFnSc6	autonehoda
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1997	[number]	k4	1997
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc2	jeho
týmu	tým	k1gInSc2	tým
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gFnSc2	Red
Wings	Wingsa	k1gFnPc2	Wingsa
podařilo	podařit	k5eAaPmAgNnS	podařit
pohár	pohár	k1gInSc4	pohár
ubránit	ubránit	k5eAaPmF	ubránit
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
Beliveau	Beliveaus	k1gInSc2	Beliveaus
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
nejvícekrát	jvícekrát	k6eNd1	jvícekrát
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
krát	krát	k6eAd1	krát
jako	jako	k9	jako
hráč	hráč	k1gMnSc1	hráč
a	a	k8xC	a
7	[number]	k4	7
<g/>
krát	krát	k6eAd1	krát
jako	jako	k8xC	jako
manažer	manažer	k1gMnSc1	manažer
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
17	[number]	k4	17
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
Henri	Henri	k1gNnSc4	Henri
Richard	Richard	k1gMnSc1	Richard
drží	držet	k5eAaImIp3nS	držet
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
hráčských	hráčský	k2eAgInPc2d1	hráčský
zápisů	zápis	k1gInPc2	zápis
na	na	k7c6	na
SC	SC	kA	SC
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
jej	on	k3xPp3gNnSc4	on
11	[number]	k4	11
<g/>
krát	krát	k6eAd1	krát
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
Montreal	Montreal	k1gInSc1	Montreal
Canadiens	Canadiensa	k1gFnPc2	Canadiensa
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
na	na	k7c6	na
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
Red	Red	k1gFnSc2	Red
Wings	Wings	k1gInSc1	Wings
Marguerite	Marguerit	k1gInSc5	Marguerit
Norris	Norris	k1gFnPc4	Norris
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
a	a	k8xC	a
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
Kanaďankou	Kanaďanka	k1gFnSc7	Kanaďanka
je	být	k5eAaImIp3nS	být
Sonia	Sonia	k1gFnSc1	Sonia
Scurfield	Scurfieldo	k1gNnPc2	Scurfieldo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
spoluvlastníkem	spoluvlastník	k1gMnSc7	spoluvlastník
Calgary	Calgary	k1gNnSc2	Calgary
Flames	Flames	k1gMnSc1	Flames
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poháru	pohár	k1gInSc6	pohár
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nebyly	být	k5eNaImAgFnP	být
opraveny	opravit	k5eAaPmNgFnP	opravit
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
příkladů	příklad	k1gInPc2	příklad
<g/>
:	:	kIx,	:
Pat	pat	k1gInSc1	pat
McReavy	McReava	k1gFnSc2	McReava
je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaBmNgInS	napsat
jako	jako	k9	jako
McCeavy	McCeava	k1gFnSc2	McCeava
-	-	kIx~	-
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
Boston	Boston	k1gInSc1	Boston
Bruins	Bruinsa	k1gFnPc2	Bruinsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
Dickie	Dickie	k1gFnSc1	Dickie
Moore	Moor	k1gInSc5	Moor
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
pohár	pohár	k1gInSc4	pohár
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaPmNgNnS	napsat
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pěti	pět	k4xCc7	pět
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
-	-	kIx~	-
D.	D.	kA	D.
Moore	Moor	k1gInSc5	Moor
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Dickie	Dickius	k1gMnSc5	Dickius
Moore	Moor	k1gMnSc5	Moor
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
Rich	Rich	k1gMnSc1	Rich
Moore	Moor	k1gInSc5	Moor
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
MooreGlenn	MooreGlenn	k1gMnSc1	MooreGlenn
Hall	Hall	k1gMnSc1	Hall
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
jako	jako	k8xC	jako
Glin	Glin	k1gMnSc1	Glin
Hall	Halla	k1gFnPc2	Halla
Bob	Bob	k1gMnSc1	Bob
Gainey	Gainea	k1gFnSc2	Gainea
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
jako	jako	k8xS	jako
Bob	bob	k1gInSc1	bob
GainyTed	GainyTed	k1gInSc1	GainyTed
Kennedy	Kenneda	k1gMnSc2	Kenneda
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
čtyřicatých	čtyřicatý	k2eAgNnPc6d1	čtyřicatý
letech	léto	k1gNnPc6	léto
zapsán	zapsán	k2eAgMnSc1d1	zapsán
jako	jako	k8xC	jako
Ted	Ted	k1gMnSc1	Ted
KenneddyyToronto	KenneddyyToront	k2eAgNnSc4d1	KenneddyyToront
Maple	Maple	k1gNnSc4	Maple
Leafs	Leafsa	k1gFnPc2	Leafsa
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g />
.	.	kIx.	.
</s>
<s>
zapsáni	zapsán	k2eAgMnPc1d1	zapsán
jako	jako	k9	jako
Toronto	Toronto	k1gNnSc1	Toronto
Maple	Maple	k1gFnSc2	Maple
Leaes	Leaes	k1gInSc1	Leaes
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc4	Bruins
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
zapsáni	zapsat	k5eAaPmNgMnP	zapsat
jako	jako	k8xS	jako
Bqstqn	Bqstqn	k1gNnSc1	Bqstqn
BruinsNew	BruinsNew	k1gMnPc2	BruinsNew
York	York	k1gInSc4	York
Islanders	Islandersa	k1gFnPc2	Islandersa
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
zapsáni	zapsán	k2eAgMnPc1d1	zapsán
jako	jako	k8xS	jako
New	New	k1gFnSc1	New
York	York	k1gInSc4	York
Ilanders	Ilanders	k1gInSc1	Ilanders
Jméno	jméno	k1gNnSc1	jméno
otce	otec	k1gMnSc2	otec
vlastníka	vlastník	k1gMnSc2	vlastník
Edmonton	Edmonton	k1gInSc1	Edmonton
Oilers	Oilers	k1gInSc1	Oilers
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
Peta	Petus	k1gMnSc4	Petus
Pocklingtona	Pocklington	k1gMnSc4	Pocklington
<g/>
,	,	kIx,	,
Basila	Basil	k1gMnSc4	Basil
bylo	být	k5eAaImAgNnS	být
přepsáno	přepsán	k2eAgNnSc1d1	přepsáno
sérii	série	k1gFnSc4	série
písmen	písmeno	k1gNnPc2	písmeno
X.	X.	kA	X.
Pár	pár	k4xCyI	pár
jmen	jméno	k1gNnPc2	jméno
bylo	být	k5eAaImAgNnS	být
i	i	k8xC	i
opraveno	opraven	k2eAgNnSc4d1	opraveno
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jako	jako	k8xC	jako
útočník	útočník	k1gMnSc1	útočník
Colorado	Colorado	k1gNnSc4	Colorado
Avalanche	Avalanch	k1gMnSc2	Avalanch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
Adam	Adam	k1gMnSc1	Adam
Deadmarsh	Deadmarsh	k1gMnSc1	Deadmarsh
(	(	kIx(	(
<g/>
napsán	napsat	k5eAaPmNgInS	napsat
jako	jako	k9	jako
Deadmarch	Deadmarch	k1gInSc1	Deadmarch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brankář	brankář	k1gMnSc1	brankář
Red	Red	k1gFnSc2	Red
Wings	Wings	k1gInSc1	Wings
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
Manny	Mann	k1gMnPc7	Mann
Legace	legace	k1gFnSc2	legace
(	(	kIx(	(
<g/>
napsán	napsat	k5eAaBmNgInS	napsat
jako	jako	k8xC	jako
Lagace	Lagace	k1gFnSc1	Lagace
<g/>
)	)	kIx)	)
a	a	k8xC	a
útočník	útočník	k1gMnSc1	útočník
Carolina	Carolina	k1gFnSc1	Carolina
Hurricanes	Hurricanesa	k1gFnPc2	Hurricanesa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Eric	Eric	k1gInSc1	Eric
Staal	Staal	k1gInSc4	Staal
(	(	kIx(	(
<g/>
napsán	napsat	k5eAaBmNgInS	napsat
jako	jako	k9	jako
Staaal	Staaal	k1gInSc1	Staaal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vítězstvím	vítězství	k1gNnSc7	vítězství
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc4	cup
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
mnoho	mnoho	k4c1	mnoho
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
nejstarších	starý	k2eAgMnPc2d3	nejstarší
<g/>
,	,	kIx,	,
vymyšlená	vymyšlený	k2eAgFnSc1d1	vymyšlená
týmem	tým	k1gInSc7	tým
Winnipeg	Winnipeg	k1gInSc1	Winnipeg
Victorias	Victoriasa	k1gFnPc2	Victoriasa
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pití	pití	k1gNnSc1	pití
šampaňského	šampaňské	k1gNnSc2	šampaňské
z	z	k7c2	z
mísy	mísa	k1gFnSc2	mísa
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
tradic	tradice	k1gFnPc2	tradice
je	být	k5eAaImIp3nS	být
předávání	předávání	k1gNnSc1	předávání
poháru	pohár	k1gInSc2	pohár
kapitánovi	kapitán	k1gMnSc3	kapitán
vítězného	vítězný	k2eAgInSc2d1	vítězný
týmu	tým	k1gInSc2	tým
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
vítězného	vítězný	k2eAgInSc2d1	vítězný
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gMnSc4	on
po	po	k7c6	po
obkroužení	obkroužení	k1gNnSc6	obkroužení
ledové	ledový	k2eAgFnSc2d1	ledová
plochy	plocha	k1gFnSc2	plocha
předá	předat	k5eAaPmIp3nS	předat
dalšímu	další	k2eAgNnSc3d1	další
a	a	k8xC	a
pohár	pohár	k1gInSc1	pohár
pak	pak	k6eAd1	pak
koluje	kolovat	k5eAaImIp3nS	kolovat
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
krouží	kroužit	k5eAaImIp3nP	kroužit
po	po	k7c6	po
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
pohár	pohár	k1gInSc1	pohár
předán	předat	k5eAaPmNgInS	předat
už	už	k6eAd1	už
na	na	k7c6	na
ledě	led	k1gInSc6	led
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
týmu	tým	k1gInSc3	tým
Toronto	Toronto	k1gNnSc1	Toronto
Maple	Maple	k1gFnSc2	Maple
Leafs	Leafsa	k1gFnPc2	Leafsa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
tradicí	tradice	k1gFnPc2	tradice
až	až	k8xS	až
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ted	Ted	k1gMnSc1	Ted
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
Detroit	Detroit	k1gInSc4	Detroit
Red	Red	k1gFnSc2	Red
Wings	Wingsa	k1gFnPc2	Wingsa
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaPmAgMnS	učinit
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
udělal	udělat	k5eAaPmAgInS	udělat
pro	pro	k7c4	pro
lepší	dobrý	k2eAgInSc4d2	lepší
pohled	pohled	k1gInSc4	pohled
fanoušků	fanoušek	k1gMnPc2	fanoušek
na	na	k7c4	na
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Tradici	tradice	k1gFnSc4	tradice
porušil	porušit	k5eAaPmAgMnS	porušit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
kapitán	kapitán	k1gMnSc1	kapitán
Colorado	Colorado	k1gNnSc4	Colorado
Avalanche	Avalanch	k1gMnSc2	Avalanch
Joe	Joe	k1gMnSc1	Joe
Sakic	Sakic	k1gMnSc1	Sakic
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
sezony	sezona	k1gFnSc2	sezona
<g/>
,	,	kIx,	,
Ray	Ray	k1gMnSc2	Ray
Bourque	Bourqu	k1gMnSc2	Bourqu
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
celou	celý	k2eAgFnSc4d1	celá
kariéru	kariéra	k1gFnSc4	kariéra
za	za	k7c4	za
Boston	Boston	k1gInSc4	Boston
Bruins	Bruinsa	k1gFnPc2	Bruinsa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyhrát	vyhrát	k5eAaPmF	vyhrát
Stanley	Stanley	k1gInPc4	Stanley
Cup	cup	k1gInSc1	cup
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povedlo	povést	k5eAaPmAgNnS	povést
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
poslední	poslední	k2eAgFnSc6d1	poslední
sezoně	sezona	k1gFnSc6	sezona
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Avs	Avs	k1gFnSc2	Avs
<g/>
.	.	kIx.	.
</s>
<s>
Bourque	Bourque	k6eAd1	Bourque
dostal	dostat	k5eAaPmAgMnS	dostat
pohár	pohár	k1gInSc4	pohár
od	od	k7c2	od
Sakica	Sakic	k1gInSc2	Sakic
hned	hned	k6eAd1	hned
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
led	led	k1gInSc4	led
obkroužil	obkroužit	k5eAaPmAgMnS	obkroužit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
poslední	poslední	k2eAgInSc1d1	poslední
zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
už	už	k9	už
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gFnSc2	Red
Wings	Wingsa	k1gFnPc2	Wingsa
zdolali	zdolat	k5eAaPmAgMnP	zdolat
Washington	Washington	k1gInSc4	Washington
Capitals	Capitalsa	k1gFnPc2	Capitalsa
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
Steve	Steve	k1gMnSc1	Steve
Yzerman	Yzerman	k1gMnSc1	Yzerman
předal	předat	k5eAaPmAgMnS	předat
trofej	trofej	k1gFnSc4	trofej
nejprve	nejprve	k6eAd1	nejprve
Vladimiru	Vladimira	k1gFnSc4	Vladimira
Konstantinovovi	Konstantinova	k1gMnSc3	Konstantinova
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
ukončila	ukončit	k5eAaPmAgFnS	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
autonehoda	autonehoda	k1gFnSc1	autonehoda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
úcty	úcta	k1gFnSc2	úcta
ke	k	k7c3	k
Stanley	Stanley	k1gInPc7	Stanley
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
nesmějí	smát	k5eNaImIp3nP	smát
dotknout	dotknout	k5eAaPmF	dotknout
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
oslavném	oslavný	k2eAgNnSc6d1	oslavné
kroužení	kroužení	k1gNnSc6	kroužení
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
spatřila	spatřit	k5eAaPmAgFnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
nová	nový	k2eAgFnSc1d1	nová
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
hráči	hráč	k1gMnPc1	hráč
vítězného	vítězný	k2eAgInSc2d1	vítězný
týmu	tým	k1gInSc2	tým
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
vypůjčit	vypůjčit	k5eAaPmF	vypůjčit
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Pohár	pohár	k1gInSc1	pohár
ale	ale	k9	ale
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
také	také	k9	také
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
Hokejové	hokejový	k2eAgFnSc2d1	hokejová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vítězové	vítěz	k1gMnPc1	vítěz
použili	použít	k5eAaPmAgMnP	použít
pohár	pohár	k1gInSc4	pohár
k	k	k7c3	k
pokřtění	pokřtění	k1gNnSc3	pokřtění
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
Clark	Clark	k1gInSc1	Clark
Gillies	Gilliesa	k1gFnPc2	Gilliesa
z	z	k7c2	z
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Islanders	Islandersa	k1gFnPc2	Islandersa
a	a	k8xC	a
Sean	Seano	k1gNnPc2	Seano
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Donnell	Donnell	k1gInSc4	Donnell
z	z	k7c2	z
Anaheim	Anaheima	k1gFnPc2	Anaheima
Ducks	Ducksa	k1gFnPc2	Ducksa
dokonce	dokonce	k9	dokonce
dali	dát	k5eAaPmAgMnP	dát
svým	svůj	k3xOyFgMnPc3	svůj
psům	pes	k1gMnPc3	pes
do	do	k7c2	do
poháru	pohár	k1gInSc2	pohár
žrádlo	žrádlo	k1gNnSc1	žrádlo
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
tři	tři	k4xCgInPc4	tři
Stanley	Stanle	k2eAgInPc4d1	Stanle
Cupy	cup	k1gInPc4	cup
-	-	kIx~	-
původní	původní	k2eAgFnSc1d1	původní
mísa	mísa	k1gFnSc1	mísa
<g/>
,	,	kIx,	,
ověřený	ověřený	k2eAgInSc1d1	ověřený
pohár	pohár	k1gInSc1	pohár
a	a	k8xC	a
replika	replika	k1gFnSc1	replika
v	v	k7c6	v
Hokejové	hokejový	k2eAgFnSc6d1	hokejová
síni	síň	k1gFnSc6	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
mísa	mísa	k1gFnSc1	mísa
koupená	koupený	k2eAgFnSc1d1	koupená
Lordem	lord	k1gMnSc7	lord
Stanleym	Stanleym	k1gInSc1	Stanleym
byla	být	k5eAaImAgFnS	být
předávána	předávat	k5eAaImNgFnS	předávat
vítězům	vítěz	k1gMnPc3	vítěz
po	po	k7c6	po
prvních	první	k4xOgInPc6	první
71	[number]	k4	71
let	léto	k1gNnPc2	léto
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
v	v	k7c6	v
Hokejové	hokejový	k2eAgFnSc6d1	hokejová
síni	síň	k1gFnSc6	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
montrealským	montrealský	k2eAgInSc7d1	montrealský
stříbrníkem	stříbrník	k1gInSc7	stříbrník
Carlem	Carl	k1gMnSc7	Carl
Petersenem	Petersen	k1gMnSc7	Petersen
<g/>
.	.	kIx.	.
</s>
<s>
Ověřena	ověřen	k2eAgFnSc1d1	ověřena
je	být	k5eAaImIp3nS	být
znakem	znak	k1gInSc7	znak
Hokejové	hokejový	k2eAgFnSc2d1	hokejová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
na	na	k7c6	na
spodku	spodek	k1gInSc6	spodek
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
když	když	k8xS	když
hráč	hráč	k1gMnSc1	hráč
zvedne	zvednout	k5eAaPmIp3nS	zvednout
pohár	pohár	k1gInSc4	pohár
nad	nad	k7c4	nad
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
trofeje	trofej	k1gFnPc4	trofej
byla	být	k5eAaImAgFnS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
v	v	k7c4	v
utajení	utajení	k1gNnSc4	utajení
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
až	až	k9	až
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Replika	replika	k1gFnSc1	replika
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
jiným	jiný	k2eAgInSc7d1	jiný
montrealským	montrealský	k2eAgInSc7d1	montrealský
stříbrníkem	stříbrník	k1gInSc7	stříbrník
<g/>
,	,	kIx,	,
Louisem	Louis	k1gMnSc7	Louis
St.	st.	kA	st.
Jacquesem	Jacques	k1gMnSc7	Jacques
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
k	k	k7c3	k
vystavení	vystavení	k1gNnSc3	vystavení
v	v	k7c6	v
Hokejové	hokejový	k2eAgFnSc6d1	hokejová
síni	síň	k1gFnSc6	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
když	když	k8xS	když
zrovna	zrovna	k6eAd1	zrovna
není	být	k5eNaImIp3nS	být
současná	současný	k2eAgFnSc1d1	současná
oficiální	oficiální	k2eAgFnSc1d1	oficiální
verze	verze	k1gFnSc1	verze
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
verzemi	verze	k1gFnPc7	verze
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málo	málo	k4c1	málo
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychlejší	rychlý	k2eAgFnSc7d3	nejrychlejší
cestou	cesta	k1gFnSc7	cesta
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
rozeznat	rozeznat	k5eAaPmF	rozeznat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
kádr	kádr	k1gInSc4	kádr
vítězných	vítězný	k2eAgFnPc2d1	vítězná
Edmonton	Edmonton	k1gInSc1	Edmonton
Oilers	Oilersa	k1gFnPc2	Oilersa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
verzi	verze	k1gFnSc6	verze
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
výše	výše	k1gFnSc2	výše
zmiňovaného	zmiňovaný	k2eAgMnSc2d1	zmiňovaný
Basila	Basil	k1gMnSc2	Basil
Pocklingtona	Pocklington	k1gMnSc2	Pocklington
vymazáno	vymazat	k5eAaPmNgNnS	vymazat
písmeny	písmeno	k1gNnPc7	písmeno
X	X	kA	X
<g/>
,	,	kIx,	,
na	na	k7c6	na
replice	replika	k1gFnSc6	replika
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Stanleyův	Stanleyův	k2eAgInSc1d1	Stanleyův
pohár	pohár	k1gInSc1	pohár
byl	být	k5eAaImAgInS	být
také	také	k9	také
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xS	jako
významná	významný	k2eAgFnSc1d1	významná
podpora	podpora	k1gFnSc1	podpora
morálky	morálka	k1gFnSc2	morálka
kanadských	kanadský	k2eAgMnPc2d1	kanadský
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
spojenců	spojenec	k1gMnPc2	spojenec
z	z	k7c2	z
NATO	NATO	kA	NATO
na	na	k7c6	na
MacDillově	MacDillův	k2eAgFnSc6d1	MacDillův
letecké	letecký	k2eAgFnSc6d1	letecká
základně	základna	k1gFnSc6	základna
nedaleko	nedaleko	k7c2	nedaleko
Tampy	Tampa	k1gFnSc2	Tampa
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
pohár	pohár	k1gInSc4	pohár
na	na	k7c6	na
Základně	základna	k1gFnSc6	základna
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
Lejeune	Lejeun	k1gInSc5	Lejeun
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Zranění	zraněný	k2eAgMnPc1d1	zraněný
příslušníci	příslušník	k1gMnPc1	příslušník
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
měli	mít	k5eAaImAgMnP	mít
příležitost	příležitost	k1gFnSc4	příležitost
se	se	k3xPyFc4	se
vyfotit	vyfotit	k5eAaPmF	vyfotit
s	s	k7c7	s
pohárem	pohár	k1gInSc7	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
si	se	k3xPyFc3	se
pohár	pohár	k1gInSc1	pohár
udělal	udělat	k5eAaPmAgInS	udělat
první	první	k4xOgInSc4	první
výlet	výlet	k1gInSc4	výlet
do	do	k7c2	do
bojové	bojový	k2eAgFnSc2d1	bojová
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
přebýval	přebývat	k5eAaImAgMnS	přebývat
od	od	k7c2	od
druhého	druhý	k4xOgNnSc2	druhý
do	do	k7c2	do
šestého	šestý	k4xOgInSc2	šestý
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
byla	být	k5eAaImAgFnS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
Hokejovou	hokejový	k2eAgFnSc7d1	hokejová
síní	síň	k1gFnSc7	síň
slávy	sláva	k1gFnSc2	sláva
a	a	k8xC	a
kanadským	kanadský	k2eAgNnSc7d1	kanadské
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
pohár	pohár	k1gInSc1	pohár
dostal	dostat	k5eAaPmAgInS	dostat
pod	pod	k7c4	pod
raketový	raketový	k2eAgInSc4d1	raketový
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyvázl	vyváznout	k5eAaPmAgMnS	vyváznout
bez	bez	k7c2	bez
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
vítězů	vítěz	k1gMnPc2	vítěz
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cupat	k5eAaImIp1nS	cupat
</s>
