<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
setkáme	setkat	k5eAaPmIp1nP	setkat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
moravských	moravský	k2eAgFnPc6d1	Moravská
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
vrchovinách	vrchovina	k1gFnPc6	vrchovina
(	(	kIx(	(
<g/>
Moravskoslezské	moravskoslezský	k2eAgInPc1d1	moravskoslezský
Beskydy	Beskyd	k1gInPc1	Beskyd
<g/>
,	,	kIx,	,
Hostýnsko-vsetínská	hostýnskosetínský	k2eAgFnSc1d1	hostýnsko-vsetínský
hornatina	hornatina	k1gFnSc1	hornatina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
osídluje	osídlovat	k5eAaImIp3nS	osídlovat
Balkán	Balkán	k1gInSc1	Balkán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
hojný	hojný	k2eAgMnSc1d1	hojný
<g/>
,	,	kIx,	,
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
Evropu	Evropa	k1gFnSc4	Evropa
až	až	k9	až
po	po	k7c4	po
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
<g/>
.	.	kIx.	.
</s>
