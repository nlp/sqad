<s>
Fidel	Fidel	k1gMnSc1	Fidel
Alejandro	Alejandra	k1gFnSc5	Alejandra
Castro	Castra	k1gMnSc5	Castra
Ruz	Ruz	k1gMnSc5	Ruz
(	(	kIx(	(
audio	audio	k2eAgFnSc7d1	audio
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1926	[number]	k4	1926
Birán	Birána	k1gFnPc2	Birána
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
Havana	havana	k1gNnSc2	havana
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
kubánský	kubánský	k2eAgMnSc1d1	kubánský
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
premiér	premiér	k1gMnSc1	premiér
<g/>
,	,	kIx,	,
revolucionář	revolucionář	k1gMnSc1	revolucionář
a	a	k8xC	a
první	první	k4xOgMnSc1	první
tajemník	tajemník	k1gMnSc1	tajemník
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
svržen	svržen	k2eAgMnSc1d1	svržen
generál	generál	k1gMnSc1	generál
Fulgencio	Fulgencio	k1gMnSc1	Fulgencio
Batista	Batista	k1gMnSc1	Batista
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
těžce	těžce	k6eAd1	těžce
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
funkce	funkce	k1gFnSc1	funkce
kromě	kromě	k7c2	kromě
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Raúl	Raúla	k1gFnPc2	Raúla
Castro	Castro	k1gNnSc4	Castro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
i	i	k9	i
funkce	funkce	k1gFnPc4	funkce
prvního	první	k4xOgMnSc2	první
tajemníka	tajemník	k1gMnSc2	tajemník
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Castrova	Castrův	k2eAgFnSc1d1	Castrova
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
zapsala	zapsat	k5eAaPmAgFnS	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
země	zem	k1gFnSc2	zem
výstavbou	výstavba	k1gFnSc7	výstavba
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
rozvojem	rozvoj	k1gInSc7	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
vzrůstem	vzrůst	k1gInSc7	vzrůst
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
,	,	kIx,	,
integrací	integrace	k1gFnSc7	integrace
do	do	k7c2	do
RVHP	RVHP	kA	RVHP
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
cenzurou	cenzura	k1gFnSc7	cenzura
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
porušováním	porušování	k1gNnSc7	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
zadržováním	zadržování	k1gNnSc7	zadržování
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
či	či	k8xC	či
neúspěšným	úspěšný	k2eNgNnPc3d1	neúspěšné
vyrovnáním	vyrovnání	k1gNnPc3	vyrovnání
se	se	k3xPyFc4	se
s	s	k7c7	s
chudobou	chudoba	k1gFnSc7	chudoba
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1926	[number]	k4	1926
na	na	k7c6	na
statku	statek	k1gInSc6	statek
v	v	k7c6	v
Biránu	Birán	k1gInSc6	Birán
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Mayarí	Mayarí	k1gFnSc2	Mayarí
<g/>
,	,	kIx,	,
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Holguín	Holguína	k1gFnPc2	Holguína
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
potomek	potomek	k1gMnSc1	potomek
Ángela	Ángel	k1gMnSc2	Ángel
Castra	Castrum	k1gNnSc2	Castrum
y	y	k?	y
Argiz	Argiz	k1gInSc1	Argiz
<g/>
,	,	kIx,	,
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
podnikatele	podnikatel	k1gMnSc4	podnikatel
s	s	k7c7	s
cukrovou	cukrový	k2eAgFnSc7d1	cukrová
třtinou	třtina	k1gFnSc7	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Lina	Lina	k1gFnSc1	Lina
Ruz	Ruz	k1gFnSc1	Ruz
González	González	k1gInSc4	González
byla	být	k5eAaImAgFnS	být
služka	služka	k1gFnSc1	služka
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Castrův	Castrův	k2eAgMnSc1d1	Castrův
otec	otec	k1gMnSc1	otec
Angel	angel	k1gMnSc1	angel
Castro	Castro	k1gNnSc4	Castro
byl	být	k5eAaImAgMnS	být
chudý	chudý	k2eAgMnSc1d1	chudý
galicijský	galicijský	k2eAgMnSc1d1	galicijský
imigrant	imigrant	k1gMnSc1	imigrant
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
na	na	k7c4	na
bohatého	bohatý	k2eAgMnSc4d1	bohatý
farmáře	farmář	k1gMnSc4	farmář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
Castrův	Castrův	k2eAgMnSc1d1	Castrův
otec	otec	k1gMnSc1	otec
bojoval	bojovat	k5eAaImAgMnS	bojovat
jako	jako	k9	jako
španělský	španělský	k2eAgMnSc1d1	španělský
voják	voják	k1gMnSc1	voják
ve	v	k7c6	v
španělsko-americké	španělskomerický	k2eAgFnSc6d1	španělsko-americká
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yRgMnPc4	který
Kuba	Kuba	k1gFnSc1	Kuba
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
silný	silný	k2eAgInSc4d1	silný
politický	politický	k2eAgInSc4d1	politický
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
vliv	vliv	k1gInSc4	vliv
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Fidelovou	Fidelův	k2eAgFnSc7d1	Fidelova
matkou	matka	k1gFnSc7	matka
se	se	k3xPyFc4	se
Angel	angel	k1gMnSc1	angel
Castro	Castro	k1gNnSc4	Castro
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
až	až	k8xS	až
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Fidelovi	Fidelův	k2eAgMnPc1d1	Fidelův
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
žil	žít	k5eAaImAgMnS	žít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
Marií	Maria	k1gFnSc7	Maria
Luisou	Luisa	k1gFnSc7	Luisa
Argotou	Argota	k1gFnSc7	Argota
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
měl	mít	k5eAaImAgMnS	mít
dva	dva	k4xCgInPc4	dva
poloviční	poloviční	k2eAgInPc4d1	poloviční
sourozence	sourozenec	k1gMnPc4	sourozenec
Lidii	Lidie	k1gFnSc4	Lidie
a	a	k8xC	a
Pedra	Pedr	k1gMnSc4	Pedr
Emilia	Emilius	k1gMnSc4	Emilius
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
zesnulí	zesnulý	k2eAgMnPc1d1	zesnulý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
měl	mít	k5eAaImAgMnS	mít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
Linou	linout	k5eAaImIp3nP	linout
následovalo	následovat	k5eAaImAgNnS	následovat
dalších	další	k2eAgFnPc2d1	další
šest	šest	k4xCc1	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
starší	starý	k2eAgMnSc1d2	starší
Ramón	Ramón	k1gMnSc1	Ramón
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
otcově	otcův	k2eAgFnSc6d1	otcova
farmářské	farmářský	k2eAgFnSc6d1	farmářská
tradici	tradice	k1gFnSc6	tradice
<g/>
,	,	kIx,	,
Fidel	Fidel	k1gMnSc1	Fidel
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
Raúl	Raúl	k1gMnSc1	Raúl
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
(	(	kIx(	(
<g/>
Angela	Angela	k1gFnSc1	Angela
<g/>
,	,	kIx,	,
Juanita	Juanita	k1gFnSc1	Juanita
a	a	k8xC	a
Emma	Emma	k1gFnSc1	Emma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
z	z	k7c2	z
nemanželského	manželský	k2eNgInSc2d1	nemanželský
svazku	svazek	k1gInSc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
nemanželský	manželský	k2eNgInSc4d1	nemanželský
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
vysmívaným	vysmívaný	k2eAgNnSc7d1	vysmívané
dítětem	dítě	k1gNnSc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
až	až	k9	až
sňatkem	sňatek	k1gInSc7	sňatek
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
Linou	linout	k5eAaImIp3nP	linout
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
Castrovým	Castrův	k2eAgInSc7d1	Castrův
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
také	také	k9	také
mohl	moct	k5eAaImAgMnS	moct
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
jménu	jméno	k1gNnSc3	jméno
připsat	připsat	k5eAaPmF	připsat
i	i	k8xC	i
mateřské	mateřský	k2eAgNnSc1d1	mateřské
příjmení	příjmení	k1gNnSc1	příjmení
"	"	kIx"	"
<g/>
Ruz	Ruz	k1gFnSc1	Ruz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
sourozenci	sourozenec	k1gMnPc1	sourozenec
se	se	k3xPyFc4	se
dožili	dožít	k5eAaPmAgMnP	dožít
znárodnění	znárodnění	k1gNnSc2	znárodnění
svých	svůj	k3xOyFgInPc2	svůj
pozemků	pozemek	k1gInPc2	pozemek
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
reformou	reforma	k1gFnSc7	reforma
Fidelovy	Fidelův	k2eAgFnSc2d1	Fidelova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
rodného	rodný	k2eAgInSc2d1	rodný
domu	dům	k1gInSc2	dům
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Lidia	Lidia	k1gFnSc1	Lidia
Castro	Castro	k1gNnSc4	Castro
obdržela	obdržet	k5eAaPmAgFnS	obdržet
kdysi	kdysi	k6eAd1	kdysi
od	od	k7c2	od
Raúla	Raúl	k1gMnSc2	Raúl
Castra	Castr	k1gMnSc2	Castr
přidělený	přidělený	k2eAgInSc4d1	přidělený
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
znárodněných	znárodněný	k2eAgInPc2d1	znárodněný
domů	dům	k1gInPc2	dům
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
Castro	Castro	k1gNnSc1	Castro
studoval	studovat	k5eAaImAgInS	studovat
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
Havanské	havanský	k2eAgFnSc6d1	Havanská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
politické	politický	k2eAgInPc1d1	politický
názory	názor	k1gInPc1	názor
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
americké	americký	k2eAgFnPc1d1	americká
vojenské	vojenský	k2eAgFnPc1d1	vojenská
intervence	intervence	k1gFnPc1	intervence
a	a	k8xC	a
okupace	okupace	k1gFnPc1	okupace
v	v	k7c6	v
několika	několik	k4yIc6	několik
státech	stát	k1gInPc6	stát
Karibiku	Karibik	k1gInSc2	Karibik
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
z	z	k7c2	z
první	první	k4xOgFnSc2	první
třetiny	třetina	k1gFnSc2	třetina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tzv.	tzv.	kA	tzv.
Banánové	banánový	k2eAgFnSc2d1	banánová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protiamerické	protiamerický	k2eAgInPc4d1	protiamerický
postoje	postoj	k1gInPc4	postoj
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
také	také	k6eAd1	také
americká	americký	k2eAgFnSc1d1	americká
podpora	podpora	k1gFnSc1	podpora
Batistova	Batistův	k2eAgInSc2d1	Batistův
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
revolucí	revoluce	k1gFnSc7	revoluce
členem	člen	k1gMnSc7	člen
Strany	strana	k1gFnSc2	strana
kubánského	kubánský	k2eAgInSc2d1	kubánský
lidu	lid	k1gInSc2	lid
-	-	kIx~	-
ortodoxní	ortodoxní	k2eAgMnSc1d1	ortodoxní
(	(	kIx(	(
<g/>
Partido	Partida	k1gFnSc5	Partida
Ortodoxo	Ortodoxa	k1gMnSc5	Ortodoxa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
kasárna	kasárna	k1gNnPc4	kasárna
Moncada	Moncada	k1gFnSc1	Moncada
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1953	[number]	k4	1953
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Hnutí	hnutí	k1gNnSc2	hnutí
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
útoku	útok	k1gInSc2	útok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
partyzánské	partyzánský	k2eAgNnSc1d1	partyzánské
hnutí	hnutí	k1gNnSc1	hnutí
zaměřující	zaměřující	k2eAgNnSc1d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
Batistovu	Batistův	k2eAgInSc3d1	Batistův
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
skupiny	skupina	k1gFnSc2	skupina
svých	svůj	k3xOyFgMnPc2	svůj
přívrženců	přívrženec	k1gMnPc2	přívrženec
(	(	kIx(	(
<g/>
Hnutí	hnutí	k1gNnSc1	hnutí
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
předchozího	předchozí	k2eAgMnSc4d1	předchozí
autoritativního	autoritativní	k2eAgMnSc4d1	autoritativní
vůdce	vůdce	k1gMnSc4	vůdce
Fulgencia	Fulgencius	k1gMnSc4	Fulgencius
Batistu	batist	k1gInSc2	batist
<g/>
,	,	kIx,	,
zastával	zastávat	k5eAaImAgMnS	zastávat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
funkci	funkce	k1gFnSc4	funkce
premiéra	premiér	k1gMnSc2	premiér
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gInSc4	jeho
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
však	však	k9	však
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnPc4d2	veliký
než	než	k8xS	než
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
hlavy	hlava	k1gFnPc4	hlava
státu	stát	k1gInSc2	stát
-	-	kIx~	-
prezidenta	prezident	k1gMnSc2	prezident
Osvalda	Osvalda	k1gFnSc1	Osvalda
Dorticóse	Dorticóse	k1gFnSc1	Dorticóse
Torrada	Torrada	k1gFnSc1	Torrada
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
Castro	Castro	k1gNnSc4	Castro
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
ekvivalentní	ekvivalentní	k2eAgInSc4d1	ekvivalentní
prezidentovi	prezident	k1gMnSc3	prezident
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
<g/>
)	)	kIx)	)
-	-	kIx~	-
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
-	-	kIx~	-
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
téměř	téměř	k6eAd1	téměř
padesátileté	padesátiletý	k2eAgFnSc2d1	padesátiletá
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejkontroverznějších	kontroverzní	k2eAgMnPc2d3	nejkontroverznější
politiků	politik	k1gMnPc2	politik
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
sliboval	slibovat	k5eAaImAgMnS	slibovat
demokratické	demokratický	k2eAgFnPc4d1	demokratická
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
znárodnil	znárodnit	k5eAaPmAgInS	znárodnit
veškeré	veškerý	k3xTgFnPc4	veškerý
pozemky	pozemka	k1gFnPc4	pozemka
a	a	k8xC	a
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
ovládány	ovládat	k5eAaImNgInP	ovládat
hlavně	hlavně	k9	hlavně
Američany	Američan	k1gMnPc4	Američan
<g/>
,	,	kIx,	,
a	a	k8xC	a
nastolil	nastolit	k5eAaPmAgMnS	nastolit
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
autoritativní	autoritativní	k2eAgInSc4d1	autoritativní
režim	režim	k1gInSc4	režim
<g/>
;	;	kIx,	;
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgMnS	označit
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
a	a	k8xC	a
následně	následně	k6eAd1	následně
navázal	navázat	k5eAaPmAgInS	navázat
blízké	blízký	k2eAgInPc4d1	blízký
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
označovaly	označovat	k5eAaImAgInP	označovat
jako	jako	k9	jako
socialistické	socialistický	k2eAgInPc1d1	socialistický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
znárodnění	znárodnění	k1gNnSc6	znárodnění
majetku	majetek	k1gInSc2	majetek
amerických	americký	k2eAgFnPc2d1	americká
firem	firma	k1gFnPc2	firma
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
USA	USA	kA	USA
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1960	[number]	k4	1960
embargo	embargo	k1gNnSc1	embargo
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
amerických	americký	k2eAgInPc2d1	americký
výrobků	výrobek	k1gInPc2	výrobek
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dále	daleko	k6eAd2	daleko
posílily	posílit	k5eAaPmAgFnP	posílit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1962	[number]	k4	1962
embargem	embargo	k1gNnSc7	embargo
na	na	k7c4	na
veškerý	veškerý	k3xTgInSc4	veškerý
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	sviní	k2eAgFnSc6d1	sviní
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1961	[number]	k4	1961
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
CIA	CIA	kA	CIA
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
financovala	financovat	k5eAaBmAgFnS	financovat
a	a	k8xC	a
podporovala	podporovat	k5eAaImAgFnS	podporovat
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
svržení	svržení	k1gNnSc1	svržení
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
<g/>
,	,	kIx,	,
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
ještě	ještě	k6eAd1	ještě
většímu	veliký	k2eAgNnSc3d2	veliký
sblížení	sblížení	k1gNnSc3	sblížení
Kuby	Kuba	k1gFnSc2	Kuba
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sovětském	sovětský	k2eAgInSc6d1	sovětský
vzoru	vzor	k1gInSc6	vzor
Castro	Castro	k1gNnSc4	Castro
zaváděl	zavádět	k5eAaImAgInS	zavádět
rychle	rychle	k6eAd1	rychle
školství	školství	k1gNnSc4	školství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
gramotnost	gramotnost	k1gFnSc4	gramotnost
Kubánců	Kubánec	k1gMnPc2	Kubánec
<g/>
,	,	kIx,	,
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
též	též	k9	též
zavedena	zavést	k5eAaPmNgFnS	zavést
i	i	k9	i
bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
lékařská	lékařský	k2eAgFnSc1d1	lékařská
péče	péče	k1gFnSc1	péče
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
si	se	k3xPyFc3	se
nejdříve	dříve	k6eAd3	dříve
získal	získat	k5eAaPmAgMnS	získat
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kuby	Kuba	k1gFnSc2	Kuba
velkou	velký	k2eAgFnSc4d1	velká
oblibu	obliba	k1gFnSc4	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Vysláním	vyslání	k1gNnSc7	vyslání
kubánských	kubánský	k2eAgFnPc2d1	kubánská
jednotek	jednotka	k1gFnPc2	jednotka
do	do	k7c2	do
Angoly	Angola	k1gFnSc2	Angola
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
vládní	vládní	k2eAgFnSc2d1	vládní
MPLA	MPLA	kA	MPLA
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
z	z	k7c2	z
UNITA	unita	k1gMnSc1	unita
a	a	k8xC	a
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
existovaly	existovat	k5eAaImAgFnP	existovat
levicové	levicový	k2eAgFnPc4d1	levicová
vlády	vláda	k1gFnPc4	vláda
nebo	nebo	k8xC	nebo
vojenské	vojenský	k2eAgInPc4d1	vojenský
oddíly	oddíl	k1gInPc4	oddíl
<g/>
,	,	kIx,	,
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
odhodlanost	odhodlanost	k1gFnSc4	odhodlanost
země	zem	k1gFnSc2	zem
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
komunismus	komunismus	k1gInSc4	komunismus
kdekoliv	kdekoliv	k6eAd1	kdekoliv
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Populárním	populární	k2eAgMnPc3d1	populární
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
též	též	k9	též
i	i	k9	i
v	v	k7c6	v
Hnutí	hnutí	k1gNnSc6	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
podpořil	podpořit	k5eAaPmAgMnS	podpořit
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
(	(	kIx(	(
<g/>
člena	člen	k1gMnSc4	člen
Hnutí	hnutí	k1gNnSc2	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gFnSc1	jeho
podpora	podpora	k1gFnSc1	podpora
silně	silně	k6eAd1	silně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
i	i	k9	i
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
Castro	Castro	k1gNnSc4	Castro
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
americkou	americký	k2eAgFnSc4d1	americká
invazi	invaze	k1gFnSc4	invaze
na	na	k7c4	na
Grenadu	Grenada	k1gFnSc4	Grenada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sandinistické	Sandinistický	k2eAgFnSc6d1	Sandinistická
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
Castro	Castro	k1gNnSc1	Castro
podporoval	podporovat	k5eAaImAgInS	podporovat
boj	boj	k1gInSc4	boj
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
Sandinistů	sandinista	k1gMnPc2	sandinista
s	s	k7c7	s
povstaleckými	povstalecký	k2eAgFnPc7d1	povstalecká
skupinami	skupina	k1gFnPc7	skupina
"	"	kIx"	"
<g/>
contras	contras	k1gInSc1	contras
<g/>
"	"	kIx"	"
podporovanými	podporovaný	k2eAgFnPc7d1	podporovaná
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Rozčaroval	rozčarovat	k5eAaPmAgMnS	rozčarovat
a	a	k8xC	a
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
zklamal	zklamat	k5eAaPmAgMnS	zklamat
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
svým	svůj	k3xOyFgNnSc7	svůj
hrubým	hrubý	k2eAgNnSc7d1	hrubé
porušováním	porušování	k1gNnSc7	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
potlačováním	potlačování	k1gNnSc7	potlačování
a	a	k8xC	a
pronásledováním	pronásledování	k1gNnSc7	pronásledování
opozičních	opoziční	k2eAgMnPc2d1	opoziční
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
neobratnou	obratný	k2eNgFnSc7d1	neobratná
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
cenzurou	cenzura	k1gFnSc7	cenzura
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
také	také	k9	také
zákazem	zákaz	k1gInSc7	zákaz
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Statisíce	statisíce	k1gInPc1	statisíce
Kubánců	Kubánec	k1gMnPc2	Kubánec
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
diktatury	diktatura	k1gFnSc2	diktatura
emigrovaly	emigrovat	k5eAaBmAgFnP	emigrovat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Neznámé	známý	k2eNgNnSc1d1	neznámé
množství	množství	k1gNnSc1	množství
se	se	k3xPyFc4	se
jich	on	k3xPp3gInPc2	on
utopilo	utopit	k5eAaPmAgNnS	utopit
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
přeplout	přeplout	k5eAaPmF	přeplout
pomocí	pomocí	k7c2	pomocí
vratkých	vratký	k2eAgNnPc2d1	vratké
plavidel	plavidlo	k1gNnPc2	plavidlo
moře	moře	k1gNnSc2	moře
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
Floridu	Florida	k1gFnSc4	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Desetitisíce	desetitisíce	k1gInPc1	desetitisíce
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
však	však	k9	však
vrátily	vrátit	k5eAaPmAgFnP	vrátit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
USA	USA	kA	USA
nechtěly	chtít	k5eNaImAgFnP	chtít
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
Kuba	Kuba	k1gFnSc1	Kuba
závislá	závislý	k2eAgFnSc1d1	závislá
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
země	zem	k1gFnSc2	zem
prudce	prudko	k6eAd1	prudko
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
bylo	být	k5eAaImAgNnS	být
zásobování	zásobování	k1gNnSc2	zásobování
ropou	ropa	k1gFnSc7	ropa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Castro	Castro	k1gNnSc4	Castro
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
zavést	zavést	k5eAaPmF	zavést
drastická	drastický	k2eAgNnPc4d1	drastické
opatření	opatření	k1gNnPc4	opatření
na	na	k7c4	na
uchránění	uchránění	k1gNnSc4	uchránění
ekonomiky	ekonomika	k1gFnSc2	ekonomika
Kuby	Kuba	k1gFnSc2	Kuba
před	před	k7c7	před
krachem	krach	k1gInSc7	krach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
a	a	k8xC	a
1996	[number]	k4	1996
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
zpřísnily	zpřísnit	k5eAaPmAgFnP	zpřísnit
obchodní	obchodní	k2eAgNnSc4d1	obchodní
embargo	embargo	k1gNnSc4	embargo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dokázal	dokázat	k5eAaPmAgInS	dokázat
Castro	Castro	k1gNnSc4	Castro
opět	opět	k6eAd1	opět
obnovit	obnovit	k5eAaPmF	obnovit
spojenectví	spojenectví	k1gNnSc4	spojenectví
se	s	k7c7	s
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
levicovými	levicový	k2eAgFnPc7d1	levicová
vládami	vláda	k1gFnPc7	vláda
-	-	kIx~	-
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
latinskoamerickými	latinskoamerický	k2eAgFnPc7d1	latinskoamerická
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
Venezuelou	Venezuela	k1gFnSc7	Venezuela
<g/>
,	,	kIx,	,
Bolívií	Bolívie	k1gFnSc7	Bolívie
<g/>
,	,	kIx,	,
Nikaraguou	Nikaragua	k1gFnSc7	Nikaragua
a	a	k8xC	a
Ekvádorem	Ekvádor	k1gInSc7	Ekvádor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
silně	silně	k6eAd1	silně
poškozenému	poškozený	k2eAgNnSc3d1	poškozené
kubánskému	kubánský	k2eAgNnSc3d1	kubánské
hospodářství	hospodářství	k1gNnSc3	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2006	[number]	k4	2006
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
jeho	jeho	k3xOp3gInSc4	jeho
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
a	a	k8xC	a
prodělal	prodělat	k5eAaPmAgMnS	prodělat
komplikovanou	komplikovaný	k2eAgFnSc4d1	komplikovaná
operaci	operace	k1gFnSc4	operace
a	a	k8xC	a
řízení	řízení	k1gNnSc4	řízení
státu	stát	k1gInSc2	stát
na	na	k7c4	na
čas	čas	k1gInSc4	čas
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Raúl	Raúl	k1gMnSc1	Raúl
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
prohlášení	prohlášení	k1gNnPc2	prohlášení
kubánských	kubánský	k2eAgInPc2d1	kubánský
úřadů	úřad	k1gInPc2	úřad
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc3	jeho
stav	stav	k1gInSc1	stav
zlepšoval	zlepšovat	k5eAaImAgInS	zlepšovat
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
tisk	tisk	k1gInSc1	tisk
ale	ale	k9	ale
spekuloval	spekulovat	k5eAaImAgInS	spekulovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
kubánské	kubánský	k2eAgInPc1d1	kubánský
úřady	úřad	k1gInPc1	úřad
oznámily	oznámit	k5eAaPmAgInP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Castro	Castro	k1gNnSc4	Castro
dočasně	dočasně	k6eAd1	dočasně
předal	předat	k5eAaPmAgMnS	předat
pravomoci	pravomoc	k1gFnPc4	pravomoc
bratru	bratr	k1gMnSc3	bratr
Raúlovi	Raúl	k1gMnSc3	Raúl
<g/>
,	,	kIx,	,
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
z	z	k7c2	z
operace	operace	k1gFnSc2	operace
trávicího	trávicí	k2eAgInSc2d1	trávicí
<g />
.	.	kIx.	.
</s>
<s>
traktu	trakt	k1gInSc2	trakt
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
časopis	časopis	k1gInSc1	časopis
Time	Time	k1gInSc4	Time
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Castro	Castro	k1gNnSc1	Castro
je	být	k5eAaImIp3nS	být
léčen	léčit	k5eAaImNgMnS	léčit
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
stadiu	stadion	k1gNnSc6	stadion
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
je	být	k5eAaImIp3nS	být
přinejmenším	přinejmenším	k6eAd1	přinejmenším
spekulativní	spekulativní	k2eAgFnSc1d1	spekulativní
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
Raúl	Raúl	k1gMnSc1	Raúl
Castro	Castro	k1gNnSc4	Castro
oznámil	oznámit	k5eAaPmAgMnS	oznámit
před	před	k7c7	před
tiskem	tisk	k1gInSc7	tisk
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
neumírá	umírat	k5eNaImIp3nS	umírat
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
ze	z	k7c2	z
dne	den	k1gInSc2	den
na	na	k7c4	na
den	den	k1gInSc4	den
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
kubánská	kubánský	k2eAgFnSc1d1	kubánská
televize	televize	k1gFnSc1	televize
záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
návštěvy	návštěva	k1gFnSc2	návštěva
Huga	Hugo	k1gMnSc2	Hugo
Cháveze	Cháveze	k1gFnSc2	Cháveze
u	u	k7c2	u
zotavujícího	zotavující	k2eAgMnSc2d1	zotavující
se	se	k3xPyFc4	se
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nebude	být	k5eNaImBp3nS	být
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
úřad	úřad	k1gInSc4	úřad
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
představitele	představitel	k1gMnSc2	představitel
Kuby	Kuba	k1gMnSc2	Kuba
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Castrově	Castrův	k2eAgFnSc6d1	Castrova
nemoci	nemoc	k1gFnSc6	nemoc
byly	být	k5eAaImAgInP	být
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
označeny	označit	k5eAaPmNgInP	označit
jako	jako	k8xC	jako
státní	státní	k2eAgNnSc1d1	státní
tajemství	tajemství	k1gNnSc1	tajemství
kubánské	kubánský	k2eAgFnSc2d1	kubánská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
opět	opět	k6eAd1	opět
začal	začít	k5eAaPmAgInS	začít
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
vydávat	vydávat	k5eAaImF	vydávat
veřejná	veřejný	k2eAgNnPc4d1	veřejné
prohlášení	prohlášení	k1gNnPc4	prohlášení
a	a	k8xC	a
dávat	dávat	k5eAaImF	dávat
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
vyhošťování	vyhošťování	k1gNnPc4	vyhošťování
Romů	Rom	k1gMnPc2	Rom
bez	bez	k7c2	bez
povolení	povolení	k1gNnSc2	povolení
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
případ	případ	k1gInSc4	případ
rasového	rasový	k2eAgInSc2d1	rasový
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
kubánského	kubánský	k2eAgInSc2d1	kubánský
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Castrův	Castrův	k2eAgInSc1d1	Castrův
oficiální	oficiální	k2eAgInSc1d1	oficiální
titul	titul	k1gInSc1	titul
zněl	znět	k5eAaImAgInS	znět
Comandante	Comandant	k1gMnSc5	Comandant
en	en	k?	en
Jefe	Jefe	k1gNnPc2	Jefe
<g/>
,	,	kIx,	,
Primer	primera	k1gFnPc2	primera
Secretario	Secretario	k6eAd1	Secretario
del	del	k?	del
Partido	Partida	k1gFnSc5	Partida
y	y	k?	y
Presidente	president	k1gMnSc5	president
de	de	k?	de
los	los	k1gInSc1	los
Consejos	Consejos	k1gInSc1	Consejos
de	de	k?	de
Estado	Estada	k1gFnSc5	Estada
y	y	k?	y
de	de	k?	de
Ministros	Ministrosa	k1gFnPc2	Ministrosa
de	de	k?	de
la	la	k1gNnSc1	la
República	República	k1gMnSc1	República
de	de	k?	de
Cuba	Cuba	k1gMnSc1	Cuba
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Vrchní	vrchní	k1gMnSc1	vrchní
velitel	velitel	k1gMnSc1	velitel
(	(	kIx(	(
<g/>
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
Kubánské	kubánský	k2eAgFnSc2d1	kubánská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc4	titul
předseda	předseda	k1gMnSc1	předseda
státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
nahradil	nahradit	k5eAaPmAgInS	nahradit
titul	titul	k1gInSc1	titul
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
kubánské	kubánský	k2eAgFnSc6d1	kubánská
ústavě	ústava	k1gFnSc6	ústava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
Castro	Castro	k1gNnSc4	Castro
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
po	po	k7c6	po
Osvaldu	Osvald	k1gInSc6	Osvald
Dorticósovi	Dorticós	k1gMnSc3	Dorticós
Torradovi	Torrada	k1gMnSc3	Torrada
(	(	kIx(	(
<g/>
prezidentovi	prezident	k1gMnSc3	prezident
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
někdy	někdy	k6eAd1	někdy
zkráceně	zkráceně	k6eAd1	zkráceně
nazýván	nazývat	k5eAaImNgMnS	nazývat
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
býval	bývat	k5eAaImAgInS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
Comandante	Comandant	k1gMnSc5	Comandant
en	en	k?	en
Jefe	Jefe	k1gFnPc7	Jefe
(	(	kIx(	(
<g/>
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Líder	Líder	k1gMnSc1	Líder
Máximo	Máxima	k1gFnSc5	Máxima
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgMnPc4d3	nejvyšší
vůdce	vůdce	k1gMnPc4	vůdce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kubánci	Kubánec	k1gMnPc1	Kubánec
Castra	Castrum	k1gNnSc2	Castrum
většinou	většinou	k6eAd1	většinou
označují	označovat	k5eAaImIp3nP	označovat
jeho	jeho	k3xOp3gNnSc7	jeho
osobním	osobní	k2eAgNnSc7d1	osobní
jménem	jméno	k1gNnSc7	jméno
Fidel	Fidel	k1gMnSc1	Fidel
nebo	nebo	k8xC	nebo
slovem	slovo	k1gNnSc7	slovo
El	Ela	k1gFnPc2	Ela
Caballo	Caballo	k1gNnSc1	Caballo
-	-	kIx~	-
Kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
devět	devět	k4xCc4	devět
dětí	dítě	k1gFnPc2	dítě
-	-	kIx~	-
sedm	sedm	k4xCc1	sedm
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Mirta	Mirta	k1gFnSc1	Mirta
Díaz-Balart	Díaz-Balarta	k1gFnPc2	Díaz-Balarta
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
syna	syn	k1gMnSc4	syn
Fidela	Fidel	k1gMnSc4	Fidel
Ángela	Ángel	k1gMnSc4	Ángel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Rozvedli	rozvést	k5eAaPmAgMnP	rozvést
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
syn	syn	k1gMnSc1	syn
po	po	k7c4	po
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
vedl	vést	k5eAaImAgMnS	vést
kubánskou	kubánský	k2eAgFnSc4d1	kubánská
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
-	-	kIx~	-
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
odvolal	odvolat	k5eAaPmAgMnS	odvolat
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
měl	mít	k5eAaImAgInS	mít
však	však	k9	však
poměr	poměr	k1gInSc1	poměr
s	s	k7c7	s
Natalií	Natalie	k1gFnSc7	Natalie
Revuelta	Revuelt	k1gInSc2	Revuelt
Clews	Clewsa	k1gFnPc2	Clewsa
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Alina	Alina	k1gFnSc1	Alina
Fernández-Revuelta	Fernández-Revuelta	k1gFnSc1	Fernández-Revuelta
<g/>
.	.	kIx.	.
</s>
<s>
Alina	Alina	k1gFnSc1	Alina
později	pozdě	k6eAd2	pozdě
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
politiku	politika	k1gFnSc4	politika
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Kubu	Kuba	k1gFnSc4	Kuba
opustila	opustit	k5eAaPmAgFnS	opustit
v	v	k7c6	v
převlečení	převlečení	k1gNnSc6	převlečení
za	za	k7c4	za
španělskou	španělský	k2eAgFnSc4d1	španělská
turistku	turistka	k1gFnSc4	turistka
a	a	k8xC	a
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Dalií	Dalie	k1gFnSc7	Dalie
Soto	Soto	k1gMnSc1	Soto
del	del	k?	del
Valle	Vall	k1gMnSc4	Vall
měl	mít	k5eAaImAgMnS	mít
Fidel	Fidel	k1gMnSc1	Fidel
dalších	další	k2eAgMnPc2d1	další
pět	pět	k4xCc1	pět
synů	syn	k1gMnPc2	syn
<g/>
:	:	kIx,	:
Antonia	Antonio	k1gMnSc2	Antonio
<g/>
,	,	kIx,	,
Alejandra	Alejandr	k1gMnSc2	Alejandr
<g/>
,	,	kIx,	,
Alexise	Alexise	k1gFnSc2	Alexise
<g/>
,	,	kIx,	,
Alexandera	Alexander	k1gMnSc2	Alexander
a	a	k8xC	a
Ángela	Ángel	k1gMnSc2	Ángel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
ženami	žena	k1gFnPc7	žena
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc4	syn
Jorgeho	Jorge	k1gMnSc4	Jorge
Ángela	Ángel	k1gMnSc4	Ángel
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Franciscu	Franciscus	k1gInSc2	Franciscus
Pupo	pupa	k1gFnSc5	pupa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
Miami	Miami	k1gNnSc6	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
bratra	bratr	k1gMnSc2	bratr
Raúla	Raúl	k1gMnSc2	Raúl
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
sestru	sestra	k1gFnSc4	sestra
Juanitu	Juanit	k1gInSc2	Juanit
Castro	Castro	k1gNnSc1	Castro
(	(	kIx(	(
<g/>
*	*	kIx~	*
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
špionkou	špionka	k1gFnSc7	špionka
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Fidelem	Fidel	k1gMnSc7	Fidel
a	a	k8xC	a
Raúlem	Raúl	k1gInSc7	Raúl
se	se	k3xPyFc4	se
rozešla	rozejít	k5eAaPmAgFnS	rozejít
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vraždili	vraždit	k5eAaImAgMnP	vraždit
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
varovat	varovat	k5eAaImF	varovat
a	a	k8xC	a
skrývat	skrývat	k5eAaImF	skrývat
kubánské	kubánský	k2eAgMnPc4d1	kubánský
disidenty	disident	k1gMnPc4	disident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Ramón	Ramón	k1gMnSc1	Ramón
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
91	[number]	k4	91
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
ČSSR	ČSSR	kA	ČSSR
vyznamenán	vyznamenán	k2eAgInSc4d1	vyznamenán
Řádem	řád	k1gInSc7	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
a	a	k8xC	a
také	také	k9	také
obdržel	obdržet	k5eAaPmAgMnS	obdržet
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vedly	vést	k5eAaImAgFnP	vést
debaty	debata	k1gFnPc1	debata
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
odebrán	odebrat	k5eAaPmNgInS	odebrat
či	či	k8xC	či
ne	ne	k9	ne
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
kvůli	kvůli	k7c3	kvůli
platnému	platný	k2eAgInSc3d1	platný
právnímu	právní	k2eAgInSc3d1	právní
řádu	řád	k1gInSc3	řád
nejde	jít	k5eNaImIp3nS	jít
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Castra	Castr	k1gMnSc4	Castr
bylo	být	k5eAaImAgNnS	být
připravováno	připravován	k2eAgNnSc1d1	připravováno
či	či	k8xC	či
plánováno	plánován	k2eAgNnSc1d1	plánováno
638	[number]	k4	638
atentátů	atentát	k1gInPc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
CIA	CIA	kA	CIA
chtěla	chtít	k5eAaImAgFnS	chtít
použít	použít	k5eAaPmF	použít
například	například	k6eAd1	například
otrávené	otrávený	k2eAgFnPc4d1	otrávená
pilule	pilule	k1gFnPc4	pilule
<g/>
,	,	kIx,	,
vybuchující	vybuchující	k2eAgInPc4d1	vybuchující
doutníky	doutník	k1gInPc4	doutník
<g/>
,	,	kIx,	,
otrávený	otrávený	k2eAgInSc4d1	otrávený
neoprén	neoprén	k1gInSc4	neoprén
nebo	nebo	k8xC	nebo
měkkýše	měkkýš	k1gMnPc4	měkkýš
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jehož	k3xOyRp3gFnSc2	jehož
škeble	škeble	k1gFnSc2	škeble
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
schovala	schovat	k5eAaPmAgFnS	schovat
nálož	nálož	k1gFnSc1	nálož
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vědělo	vědět	k5eAaImAgNnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Castro	Castro	k1gNnSc1	Castro
je	být	k5eAaImIp3nS	být
vášnivý	vášnivý	k2eAgInSc1d1	vášnivý
potápěč	potápěč	k1gInSc1	potápěč
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
podporoval	podporovat	k5eAaImAgInS	podporovat
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
apartheidu	apartheid	k1gInSc3	apartheid
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
vzor	vzor	k1gInSc4	vzor
ho	on	k3xPp3gInSc4	on
uvedli	uvést	k5eAaPmAgMnP	uvést
například	například	k6eAd1	například
Ahmed	Ahmed	k1gInSc4	Ahmed
Ben	Ben	k1gInSc1	Ben
Bella	Bella	k1gFnSc1	Bella
<g/>
,	,	kIx,	,
bojovník	bojovník	k1gMnSc1	bojovník
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
na	na	k7c6	na
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bojovník	bojovník	k1gMnSc1	bojovník
proti	proti	k7c3	proti
apartheidu	apartheid	k1gInSc3	apartheid
Nelson	Nelson	k1gMnSc1	Nelson
Mandela	Mandel	k1gMnSc2	Mandel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
prezidentem	prezident	k1gMnSc7	prezident
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
udělil	udělit	k5eAaPmAgMnS	udělit
Castrovi	Castr	k1gMnSc3	Castr
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
jihoafrické	jihoafrický	k2eAgNnSc4d1	jihoafrické
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Kubu	Kuba	k1gFnSc4	Kuba
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
papežově	papežův	k2eAgFnSc6d1	papežova
mši	mše	k1gFnSc6	mše
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
kubánské	kubánský	k2eAgFnSc2d1	kubánská
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtvrt	čtvrt	k1xP	čtvrt
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
jak	jak	k8xS	jak
marxismus	marxismus	k1gInSc4	marxismus
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
prosazování	prosazování	k1gNnSc3	prosazování
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
mši	mše	k1gFnSc4	mše
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
i	i	k8xC	i
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc1	Castro
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
"	"	kIx"	"
<g/>
Za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
jste	být	k5eAaImIp2nP	být
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
za	za	k7c4	za
ta	ten	k3xDgNnPc4	ten
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
bych	by	kYmCp1nS	by
mohl	moct	k5eAaImAgInS	moct
nesouhlasit	souhlasit	k5eNaImF	souhlasit
<g/>
,	,	kIx,	,
Vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
Svatý	svatý	k1gMnSc5	svatý
otče	otec	k1gMnSc5	otec
<g/>
,	,	kIx,	,
děkuji	děkovat	k5eAaImIp1nS	děkovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Castro	Castro	k1gNnSc1	Castro
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
a	a	k8xC	a
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
solidaritu	solidarita	k1gFnSc4	solidarita
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
americkou	americký	k2eAgFnSc4d1	americká
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
NATO	NATO	kA	NATO
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
</s>
