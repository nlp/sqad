<s>
Chevrolet	chevrolet	k1gInSc1	chevrolet
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
Chevy	Cheva	k1gFnSc2	Cheva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
firma	firma	k1gFnSc1	firma
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
automobily	automobil	k1gInPc7	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
koncernu	koncern	k1gInSc2	koncern
General	General	k1gFnSc2	General
Motors	Motorsa	k1gFnPc2	Motorsa
<g/>
.	.	kIx.	.
</s>
<s>
Chevrolet	chevrolet	k1gInSc1	chevrolet
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
Louisem	louis	k1gInSc7	louis
Chevroletem	chevrolet	k1gInSc7	chevrolet
a	a	k8xC	a
Williamem	William	k1gInSc7	William
Durantem	Durant	k1gMnSc7	Durant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
však	však	k9	však
mezi	mezi	k7c7	mezi
Chevroletem	chevrolet	k1gInSc7	chevrolet
a	a	k8xC	a
Durantem	Durant	k1gMnSc7	Durant
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
roztržce	roztržka	k1gFnSc3	roztržka
a	a	k8xC	a
Louis	louis	k1gInPc4	louis
Chevrolet	chevrolet	k1gInSc1	chevrolet
Durantovi	Durantův	k2eAgMnPc1d1	Durantův
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
prodal	prodat	k5eAaPmAgMnS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
William	William	k1gInSc1	William
Durant	Duranta	k1gFnPc2	Duranta
sloučil	sloučit	k5eAaPmAgInS	sloučit
Chevrolet	chevrolet	k1gInSc1	chevrolet
Motor	motor	k1gInSc4	motor
Car	car	k1gMnSc1	car
Company	Compana	k1gFnSc2	Compana
s	s	k7c7	s
koncernem	koncern	k1gInSc7	koncern
General	General	k1gFnSc2	General
Motors	Motors	k1gInSc1	Motors
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
kapotě	kapota	k1gFnSc6	kapota
logo	logo	k1gNnSc4	logo
Chevroletu	chevrolet	k1gInSc2	chevrolet
také	také	k6eAd1	také
vozy	vůz	k1gInPc1	vůz
Daewoo	Daewoo	k6eAd1	Daewoo
a	a	k8xC	a
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
také	také	k9	také
modely	model	k1gInPc4	model
prodávané	prodávaný	k2eAgInPc1d1	prodávaný
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Opel	opel	k1gInSc4	opel
-	-	kIx~	-
například	například	k6eAd1	například
Corsa	Corsa	k1gFnSc1	Corsa
<g/>
,	,	kIx,	,
Astra	astra	k1gFnSc1	astra
či	či	k8xC	či
Vectra	Vectra	k1gFnSc1	Vectra
<g/>
.	.	kIx.	.
</s>
<s>
HHR	HHR	kA	HHR
SSR	SSR	kA	SSR
Camaro	Camara	k1gFnSc5	Camara
Cobalt	Cobalt	k1gInSc1	Cobalt
Corvette	Corvett	k1gInSc5	Corvett
Impala	Impal	k1gMnSc2	Impal
Malibu	Malib	k1gInSc2	Malib
Avalanche	Avalanche	k1gNnSc2	Avalanche
Colorado	Colorado	k1gNnSc1	Colorado
Silverado	Silverada	k1gFnSc5	Silverada
Equinox	Equinox	k1gInSc4	Equinox
Suburban	Suburban	k1gMnSc1	Suburban
Tahoe	Taho	k1gInSc2	Taho
TrailBlazer	TrailBlazer	k1gMnSc1	TrailBlazer
Lumina	lumen	k1gNnSc2	lumen
TransSport	TransSport	k1gInSc1	TransSport
Express	express	k1gInSc1	express
Uplander	Uplander	k1gInSc1	Uplander
Orlando	Orlanda	k1gFnSc5	Orlanda
Cruze	Cruze	k1gFnSc2	Cruze
Spark	Spark	k1gInSc4	Spark
Aveo	Aveo	k6eAd1	Aveo
Lacetti	Lacetť	k1gFnPc1	Lacetť
Tacuma	Tacum	k1gMnSc2	Tacum
Epica	Epica	k1gFnSc1	Epica
Captiva	Captiva	k1gFnSc1	Captiva
</s>
