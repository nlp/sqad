<s>
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
,	,	kIx,	,
křtěn	křtěn	k2eAgMnSc1d1	křtěn
Jan	Jan	k1gMnSc1	Jan
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1787	[number]	k4	1787
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
křestní	křestní	k2eAgFnSc2d1	křestní
matriky	matrika	k1gFnSc2	matrika
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
Libochovice	Libochovice	k1gFnPc1	Libochovice
(	(	kIx(	(
<g/>
zámek	zámek	k1gInSc1	zámek
<g/>
)	)	kIx)	)
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1869	[number]	k4	1869
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
fyziolog	fyziolog	k1gMnSc1	fyziolog
<g/>
,	,	kIx,	,
anatom	anatom	k1gMnSc1	anatom
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
<g/>
;	;	kIx,	;
otec	otec	k1gMnSc1	otec
malíře	malíř	k1gMnSc2	malíř
Karla	Karel	k1gMnSc2	Karel
Purkyně	Purkyně	k1gFnSc2	Purkyně
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
příspěvkem	příspěvek	k1gInSc7	příspěvek
o	o	k7c6	o
živočišných	živočišný	k2eAgFnPc6d1	živočišná
tkáních	tkáň	k1gFnPc6	tkáň
složených	složený	k2eAgInPc2d1	složený
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
s	s	k7c7	s
jádry	jádro	k1gNnPc7	jádro
(	(	kIx(	(
<g/>
v	v	k7c6	v
Karolinu	Karolinum	k1gNnSc6	Karolinum
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
spoluzakladatelů	spoluzakladatel	k1gMnPc2	spoluzakladatel
cytologie	cytologie	k1gFnSc2	cytologie
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
správcem	správce	k1gMnSc7	správce
libochovického	libochovický	k2eAgNnSc2d1	Libochovické
šlechtického	šlechtický	k2eAgNnSc2d1	šlechtické
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Janu	Jana	k1gFnSc4	Jana
Evangelistovi	evangelista	k1gMnSc3	evangelista
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
Purkyně	Purkyně	k1gFnSc2	Purkyně
odešel	odejít	k5eAaPmAgMnS	odejít
za	za	k7c7	za
vzděláním	vzdělání	k1gNnSc7	vzdělání
k	k	k7c3	k
piaristům	piarista	k1gMnPc3	piarista
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
piaristickém	piaristický	k2eAgNnSc6d1	Piaristické
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Mikulově	Mikulov	k1gInSc6	Mikulov
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
také	také	k9	také
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Vodě	voda	k1gFnSc6	voda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Františka	František	k1gMnSc2	František
Hanela	Hanel	k1gMnSc2	Hanel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
však	však	k9	však
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Živil	živit	k5eAaImAgInS	živit
se	se	k3xPyFc4	se
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
v	v	k7c6	v
šlechtických	šlechtický	k2eAgFnPc6d1	šlechtická
rodinách	rodina	k1gFnPc6	rodina
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
nasměroval	nasměrovat	k5eAaPmAgInS	nasměrovat
Purkyňův	Purkyňův	k2eAgInSc1d1	Purkyňův
zájem	zájem	k1gInSc1	zájem
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
lékařství	lékařství	k1gNnSc3	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
pražského	pražský	k2eAgNnSc2d1	Pražské
studia	studio	k1gNnSc2	studio
mu	on	k3xPp3gMnSc3	on
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vyšla	vyjít	k5eAaPmAgFnS	vyjít
první	první	k4xOgFnSc1	první
báseň	báseň	k1gFnSc1	báseň
Orážení	orážení	k1gNnSc4	orážení
obrazu	obraz	k1gInSc2	obraz
bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Doktorskou	doktorský	k2eAgFnSc4d1	doktorská
práci	práce	k1gFnSc4	práce
O	o	k7c6	o
zření	zření	k1gNnSc6	zření
v	v	k7c6	v
ohledu	ohled	k1gInSc6	ohled
subjektivním	subjektivní	k2eAgInSc6d1	subjektivní
obhájil	obhájit	k5eAaPmAgInS	obhájit
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
asistentem	asistent	k1gMnSc7	asistent
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vlastenecké	vlastenecký	k2eAgInPc4d1	vlastenecký
názory	názor	k1gInPc4	názor
se	se	k3xPyFc4	se
ale	ale	k9	ale
marně	marně	k6eAd1	marně
ucházel	ucházet	k5eAaImAgInS	ucházet
o	o	k7c4	o
profesuru	profesura	k1gFnSc4	profesura
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
na	na	k7c4	na
mocnou	mocný	k2eAgFnSc4d1	mocná
přímluvu	přímluva	k1gFnSc4	přímluva
několika	několik	k4yIc2	několik
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
pruský	pruský	k2eAgMnSc1d1	pruský
ministr	ministr	k1gMnSc1	ministr
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
Karl	Karl	k1gMnSc1	Karl
vom	vom	k?	vom
Stein	Stein	k1gMnSc1	Stein
zum	zum	k?	zum
Altenstein	Altenstein	k1gMnSc1	Altenstein
(	(	kIx(	(
<g/>
1770	[number]	k4	1770
<g/>
–	–	k?	–
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
Johann	Johann	k1gMnSc1	Johann
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Rust	Rust	k1gMnSc1	Rust
(	(	kIx(	(
<g/>
1775	[number]	k4	1775
<g/>
–	–	k?	–
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
pruského	pruský	k2eAgNnSc2d1	pruské
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
oddělení	oddělení	k1gNnSc2	oddělení
Johannes	Johannes	k1gMnSc1	Johannes
Schulze	Schulz	k1gMnSc2	Schulz
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
<g/>
–	–	k?	–
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Karl	Karla	k1gFnPc2	Karla
Asmund	Asmund	k1gMnSc1	Asmund
Rudolphi	Rudolph	k1gFnSc2	Rudolph
(	(	kIx(	(
<g/>
1771	[number]	k4	1771
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
i	i	k9	i
na	na	k7c6	na
přímluvou	přímluva	k1gFnSc7	přímluva
Goethovu	Goethova	k1gFnSc4	Goethova
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
studoval	studovat	k5eAaImAgInS	studovat
proces	proces	k1gInSc1	proces
vidění	vidění	k1gNnSc2	vidění
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
angažován	angažovat	k5eAaBmNgInS	angažovat
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
mimorakouskou	mimorakouský	k2eAgFnSc4d1	mimorakouský
<g/>
,	,	kIx,	,
do	do	k7c2	do
tehdy	tehdy	k6eAd1	tehdy
pruské	pruský	k2eAgFnSc2d1	pruská
Vratislavi	Vratislav	k1gFnSc2	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Julií	Julie	k1gFnSc7	Julie
Rudolphi	Rudolph	k1gFnSc2	Rudolph
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
<g/>
–	–	k?	–
<g/>
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
svého	svůj	k3xOyFgMnSc2	svůj
zastánce	zastánce	k1gMnSc2	zastánce
<g/>
,	,	kIx,	,
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
působícího	působící	k2eAgMnSc2d1	působící
přírodovědce	přírodovědec	k1gMnSc2	přírodovědec
švédského	švédský	k2eAgInSc2d1	švédský
původu	původ	k1gInSc2	původ
Karla	Karel	k1gMnSc2	Karel
Asmunda	Asmund	k1gMnSc2	Asmund
Rudolphi	Rudolph	k1gFnSc2	Rudolph
(	(	kIx(	(
<g/>
1771	[number]	k4	1771
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
choleru	cholera	k1gFnSc4	cholera
zemřela	zemřít	k5eAaPmAgFnS	zemřít
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
obě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
zbyli	zbýt	k5eAaPmAgMnP	zbýt
mu	on	k3xPp3gMnSc3	on
dva	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
syn	syn	k1gMnSc1	syn
Emanuel	Emanuel	k1gMnSc1	Emanuel
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
–	–	k?	–
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
přírodovědcem	přírodovědec	k1gMnSc7	přírodovědec
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
jako	jako	k9	jako
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
jej	on	k3xPp3gInSc4	on
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
finančně	finančně	k6eAd1	finančně
podporoval	podporovat	k5eAaImAgMnS	podporovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
výkonný	výkonný	k2eAgInSc1d1	výkonný
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
objev	objev	k1gInSc1	objev
prezentoval	prezentovat	k5eAaBmAgInS	prezentovat
Purkyně	Purkyně	k1gFnSc2	Purkyně
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
německých	německý	k2eAgMnPc2d1	německý
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
a	a	k8xC	a
lékařů	lékař	k1gMnPc2	lékař
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Karolinu	Karolinum	k1gNnSc6	Karolinum
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
přisoudil	přisoudit	k5eAaPmAgInS	přisoudit
buňkám	buňka	k1gFnPc3	buňka
jejich	jejich	k3xOp3gInSc4	jejich
stěžejní	stěžejní	k2eAgInSc4d1	stěžejní
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
také	také	k9	také
odváděl	odvádět	k5eAaImAgMnS	odvádět
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
badatelskou	badatelský	k2eAgFnSc4d1	badatelská
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
zřejmě	zřejmě	k6eAd1	zřejmě
nejfrekventovanějšího	frekventovaný	k2eAgMnSc4d3	nejfrekventovanější
eponyma	eponym	k1gMnSc4	eponym
české	český	k2eAgFnSc2d1	Česká
vědy	věda	k1gFnSc2	věda
<g/>
:	:	kIx,	:
Purkyňova	Purkyňův	k2eAgNnSc2d1	Purkyňovo
vlákna	vlákno	k1gNnSc2	vlákno
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
<g/>
,	,	kIx,	,
Purkyňovy	Purkyňův	k2eAgFnPc1d1	Purkyňova
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
mozku	mozek	k1gInSc6	mozek
<g/>
,	,	kIx,	,
Purkyňovy	Purkyňův	k2eAgInPc4d1	Purkyňův
obrázky	obrázek	k1gInPc4	obrázek
(	(	kIx(	(
<g/>
vnímání	vnímání	k1gNnSc1	vnímání
světla	světlo	k1gNnSc2	světlo
okem	oke	k1gNnSc7	oke
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Purkyňův	Purkyňův	k2eAgInSc1d1	Purkyňův
jev	jev	k1gInSc1	jev
(	(	kIx(	(
<g/>
vnímání	vnímání	k1gNnSc1	vnímání
barev	barva	k1gFnPc2	barva
okem	oke	k1gNnSc7	oke
<g/>
)	)	kIx)	)
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nazvaných	nazvaný	k2eAgMnPc2d1	nazvaný
útvarů	útvar	k1gInPc2	útvar
a	a	k8xC	a
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
O	o	k7c6	o
spánku	spánek	k1gInSc6	spánek
<g/>
,	,	kIx,	,
snech	sen	k1gInPc6	sen
a	a	k8xC	a
stavech	stav	k1gInPc6	stav
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
Purkyně	Purkyně	k1gFnSc2	Purkyně
zdůraznil	zdůraznit	k5eAaPmAgInS	zdůraznit
osvěžující	osvěžující	k2eAgFnSc4d1	osvěžující
a	a	k8xC	a
léčivou	léčivý	k2eAgFnSc4d1	léčivá
funkci	funkce	k1gFnSc4	funkce
snů	sen	k1gInPc2	sen
pro	pro	k7c4	pro
duševní	duševní	k2eAgFnSc4d1	duševní
rovnováhu	rovnováha	k1gFnSc4	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Předjímá	předjímat	k5eAaImIp3nS	předjímat
tím	ten	k3xDgNnSc7	ten
budoucí	budoucí	k2eAgFnSc1d1	budoucí
Freudovy	Freudův	k2eAgFnPc4d1	Freudova
úvahy	úvaha	k1gFnPc4	úvaha
o	o	k7c6	o
snu	sen	k1gInSc6	sen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Jungovu	Jungův	k2eAgFnSc4d1	Jungova
teorii	teorie	k1gFnSc4	teorie
kompenzační	kompenzační	k2eAgFnSc2d1	kompenzační
funkce	funkce	k1gFnSc2	funkce
snu	sen	k1gInSc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stati	stať	k1gFnSc6	stať
Individuální	individuální	k2eAgFnSc1d1	individuální
duševní	duševní	k2eAgFnSc1d1	duševní
ústroj	ústroj	k1gFnSc1	ústroj
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
Purkyně	Purkyně	k1gFnSc2	Purkyně
rozlišil	rozlišit	k5eAaPmAgMnS	rozlišit
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
metody	metoda	k1gFnPc4	metoda
zkoumání	zkoumání	k1gNnSc2	zkoumání
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
–	–	k?	–
antropologickou	antropologický	k2eAgFnSc4d1	antropologická
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
souvislost	souvislost	k1gFnSc4	souvislost
duševních	duševní	k2eAgFnPc2d1	duševní
zvláštností	zvláštnost	k1gFnPc2	zvláštnost
s	s	k7c7	s
tělesnými	tělesný	k2eAgInPc7d1	tělesný
<g/>
,	,	kIx,	,
a	a	k8xC	a
psychologickou	psychologický	k2eAgFnSc4d1	psychologická
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
vlastní	vlastní	k2eAgFnPc4d1	vlastní
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
na	na	k7c4	na
sebepozorování	sebepozorování	k1gNnSc4	sebepozorování
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
fenomenologická	fenomenologický	k2eAgFnSc1d1	fenomenologická
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hledá	hledat	k5eAaImIp3nS	hledat
souvislosti	souvislost	k1gFnPc4	souvislost
mezi	mezi	k7c7	mezi
výjevy	výjev	k1gInPc7	výjev
duševního	duševní	k2eAgInSc2d1	duševní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
českých	český	k2eAgMnPc2d1	český
vědců	vědec	k1gMnPc2	vědec
přijal	přijmout	k5eAaPmAgMnS	přijmout
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
Darwinovu	Darwinův	k2eAgFnSc4d1	Darwinova
evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
otevřenosti	otevřenost	k1gFnSc6	otevřenost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
Purkyně	Purkyně	k1gFnSc2	Purkyně
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
fyziologický	fyziologický	k2eAgInSc1d1	fyziologický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
výzkumem	výzkum	k1gInSc7	výzkum
už	už	k6eAd1	už
ale	ale	k8xC	ale
převládala	převládat	k5eAaImAgFnS	převládat
činnost	činnost	k1gFnSc4	činnost
organizační	organizační	k2eAgFnSc1d1	organizační
a	a	k8xC	a
buditelská	buditelský	k2eAgFnSc1d1	buditelská
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
londýnské	londýnský	k2eAgFnSc2d1	londýnská
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
a	a	k8xC	a
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
poslancem	poslanec	k1gMnSc7	poslanec
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
nositelem	nositel	k1gMnSc7	nositel
Leopoldova	Leopoldův	k2eAgInSc2d1	Leopoldův
rytířského	rytířský	k2eAgInSc2d1	rytířský
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
byl	být	k5eAaImAgInS	být
rakouským	rakouský	k2eAgMnSc7d1	rakouský
císařem	císař	k1gMnSc7	císař
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
rytířského	rytířský	k2eAgInSc2d1	rytířský
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
založil	založit	k5eAaPmAgInS	založit
časopis	časopis	k1gInSc1	časopis
Živa	Živa	k1gFnSc1	Živa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Purkyně	Purkyně	k1gFnSc2	Purkyně
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
kinesiskop	kinesiskop	k1gInSc4	kinesiskop
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgMnSc2	jenž
použil	použít	k5eAaPmAgMnS	použít
rotační	rotační	k2eAgFnSc4d1	rotační
závěrku	závěrka	k1gFnSc4	závěrka
používanou	používaný	k2eAgFnSc4d1	používaná
následně	následně	k6eAd1	následně
u	u	k7c2	u
kinematografů	kinematograf	k1gInPc2	kinematograf
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
za	za	k7c4	za
průkopníka	průkopník	k1gMnSc4	průkopník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
nechal	nechat	k5eAaPmAgMnS	nechat
podle	podle	k7c2	podle
svých	svůj	k3xOyFgInPc2	svůj
nákresů	nákres	k1gInPc2	nákres
vyrobit	vyrobit	k5eAaPmF	vyrobit
kotouč	kotouč	k1gInSc4	kotouč
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
animovanou	animovaný	k2eAgFnSc4d1	animovaná
sekvenci	sekvence	k1gFnSc4	sekvence
práce	práce	k1gFnSc2	práce
lidského	lidský	k2eAgNnSc2d1	lidské
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
známý	známý	k2eAgInSc1d1	známý
případ	případ	k1gInSc1	případ
užití	užití	k1gNnSc2	užití
této	tento	k3xDgFnSc2	tento
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
považované	považovaný	k2eAgNnSc1d1	považované
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
za	za	k7c4	za
zábavnou	zábavný	k2eAgFnSc4d1	zábavná
hračku	hračka	k1gFnSc4	hračka
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
vědeckého	vědecký	k2eAgInSc2d1	vědecký
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
ale	ale	k9	ale
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
stane	stanout	k5eAaPmIp3nS	stanout
svébytným	svébytný	k2eAgInSc7d1	svébytný
druhem	druh	k1gInSc7	druh
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Zapojil	zapojit	k5eAaPmAgInS	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
vzniku	vznik	k1gInSc2	vznik
tělovýchovné	tělovýchovný	k2eAgFnSc2d1	Tělovýchovná
organizace	organizace	k1gFnSc2	organizace
Sokol	Sokol	k1gMnSc1	Sokol
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Spálené	spálený	k2eAgFnSc6d1	spálená
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
připomíná	připomínat	k5eAaImIp3nS	připomínat
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
poblíž	poblíž	k7c2	poblíž
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Národní	národní	k2eAgFnSc1d1	národní
třída	třída	k1gFnSc1	třída
<g/>
.	.	kIx.	.
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1	pochován
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velkou	velký	k2eAgFnSc7d1	velká
národní	národní	k2eAgFnSc7d1	národní
manifestací	manifestace	k1gFnSc7	manifestace
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc2	Purkyně
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
věnoval	věnovat	k5eAaPmAgInS	věnovat
také	také	k9	také
básnické	básnický	k2eAgFnSc3d1	básnická
tvorbě	tvorba	k1gFnSc3	tvorba
(	(	kIx(	(
<g/>
v	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
měl	mít	k5eAaImAgMnS	mít
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
se	se	k3xPyFc4	se
stát	stát	k1gInSc1	stát
výlučně	výlučně	k6eAd1	výlučně
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
doloženě	doloženě	k6eAd1	doloženě
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
podepsal	podepsat	k5eAaPmAgInS	podepsat
J.	J.	kA	J.
Purkyně	Purkyně	k1gFnSc2	Purkyně
<g/>
,	,	kIx,	,
Dichter	Dichter	k1gMnSc1	Dichter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
básně	báseň	k1gFnPc1	báseň
a	a	k8xC	a
básnické	básnický	k2eAgInPc1d1	básnický
překlady	překlad	k1gInPc1	překlad
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
jedenáctém	jedenáctý	k4xOgInSc6	jedenáctý
svazku	svazek	k1gInSc6	svazek
jeho	jeho	k3xOp3gMnPc2	jeho
sebraných	sebraný	k2eAgMnPc2d1	sebraný
spisů	spis	k1gInPc2	spis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
publikoval	publikovat	k5eAaBmAgInS	publikovat
spíše	spíše	k9	spíše
jen	jen	k9	jen
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
básnická	básnický	k2eAgFnSc1d1	básnická
tvorba	tvorba	k1gFnSc1	tvorba
(	(	kIx(	(
<g/>
převažuje	převažovat	k5eAaImIp3nS	převažovat
reflexivní	reflexivní	k2eAgFnSc1d1	reflexivní
a	a	k8xC	a
příležitostná	příležitostný	k2eAgFnSc1d1	příležitostná
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc1	báseň
oslavující	oslavující	k2eAgFnSc2d1	oslavující
pantheistický	pantheistický	k2eAgInSc4d1	pantheistický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
popisná	popisný	k2eAgFnSc1d1	popisná
lyrika	lyrika	k1gFnSc1	lyrika
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
M.	M.	kA	M.
Z.	Z.	kA	Z.
Poláka	Polák	k1gMnSc4	Polák
a	a	k8xC	a
také	také	k9	také
satirické	satirický	k2eAgInPc4d1	satirický
epigramy	epigram	k1gInPc4	epigram
makarónským	makarónský	k2eAgInSc7d1	makarónský
veršem	verš	k1gInSc7	verš
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Purkyně	Purkyně	k1gFnSc2	Purkyně
nazýval	nazývat	k5eAaImAgMnS	nazývat
hybridka	hybridek	k1gMnSc4	hybridek
–	–	k?	–
hybrid	hybrid	k1gInSc1	hybrid
dvou	dva	k4xCgInPc2	dva
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zapomenuta	zapomnět	k5eAaImNgFnS	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Okrajově	okrajově	k6eAd1	okrajově
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
také	také	k9	také
básnické	básnický	k2eAgFnSc3d1	básnická
tvorbě	tvorba	k1gFnSc3	tvorba
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Františka	František	k1gMnSc2	František
Douchy	Doucha	k1gMnSc2	Doucha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ale	ale	k9	ale
významným	významný	k2eAgMnSc7d1	významný
překladatelem	překladatel	k1gMnSc7	překladatel
poesie	poesie	k1gFnSc2	poesie
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
lyrických	lyrický	k2eAgFnPc2d1	lyrická
básní	báseň	k1gFnPc2	báseň
Friedricha	Friedrich	k1gMnSc2	Friedrich
Schillera	Schiller	k1gMnSc2	Schiller
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc7	první
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Osvobozený	osvobozený	k2eAgInSc1d1	osvobozený
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
Torquata	Torquat	k1gMnSc2	Torquat
Tassa	Tass	k1gMnSc2	Tass
(	(	kIx(	(
<g/>
z	z	k7c2	z
překladu	překlad	k1gInSc2	překlad
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
publikován	publikovat	k5eAaBmNgInS	publikovat
jen	jen	k9	jen
první	první	k4xOgInSc1	první
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
také	také	k9	také
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
spisy	spis	k1gInPc4	spis
a	a	k8xC	a
slavistické	slavistický	k2eAgFnPc4d1	slavistická
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
překážkou	překážka	k1gFnSc7	překážka
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
jeho	jeho	k3xOp3gInPc2	jeho
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
původních	původní	k2eAgInPc2d1	původní
či	či	k8xC	či
přeložených	přeložený	k2eAgInPc2d1	přeložený
pro	pro	k7c4	pro
moderního	moderní	k2eAgMnSc4d1	moderní
čtenáře	čtenář	k1gMnSc4	čtenář
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hojné	hojný	k2eAgNnSc1d1	hojné
užívání	užívání	k1gNnSc1	užívání
mnohdy	mnohdy	k6eAd1	mnohdy
velmi	velmi	k6eAd1	velmi
nepřirozených	přirozený	k2eNgInPc2d1	nepřirozený
neologismů	neologismus	k1gInPc2	neologismus
a	a	k8xC	a
podivných	podivný	k2eAgInPc2d1	podivný
slovních	slovní	k2eAgInPc2d1	slovní
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
i	i	k8xC	i
přeložené	přeložený	k2eAgFnSc6d1	přeložená
tvorbě	tvorba	k1gFnSc6	tvorba
využívá	využívat	k5eAaImIp3nS	využívat
zejména	zejména	k9	zejména
časoměrný	časoměrný	k2eAgInSc1d1	časoměrný
verš	verš	k1gInSc1	verš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
již	již	k6eAd1	již
česká	český	k2eAgFnSc1d1	Česká
poesie	poesie	k1gFnSc1	poesie
nevyužívá	využívat	k5eNaImIp3nS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Purkyně	Purkyně	k1gFnSc1	Purkyně
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
vadě	vada	k1gFnSc6	vada
věděl	vědět	k5eAaImAgMnS	vědět
a	a	k8xC	a
proto	proto	k8xC	proto
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
publikoval	publikovat	k5eAaBmAgMnS	publikovat
jen	jen	k6eAd1	jen
několik	několik	k4yIc4	několik
svých	svůj	k3xOyFgFnPc2	svůj
básní	báseň	k1gFnPc2	báseň
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnSc2	jeho
básnické	básnický	k2eAgFnSc2d1	básnická
tvorby	tvorba	k1gFnSc2	tvorba
proto	proto	k8xC	proto
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
rukopise	rukopis	k1gInSc6	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
překlady	překlad	k1gInPc1	překlad
z	z	k7c2	z
Friedricha	Friedrich	k1gMnSc2	Friedrich
Schillera	Schiller	k1gMnSc2	Schiller
nicméně	nicméně	k8xC	nicméně
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nepřekonány	překonán	k2eNgInPc1d1	nepřekonán
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc2	Purkyně
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
pod	pod	k7c7	pod
okny	okno	k1gNnPc7	okno
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k1gNnPc7	další
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Dvaasedmdesát	dvaasedmdesát	k4xCc4	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Purkyňovo	Purkyňův	k2eAgNnSc4d1	Purkyňovo
jméno	jméno	k1gNnSc4	jméno
nesla	nést	k5eAaImAgFnS	nést
v	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
brněnská	brněnský	k2eAgFnSc1d1	brněnská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
fungující	fungující	k2eAgInPc1d1	fungující
opět	opět	k6eAd1	opět
pod	pod	k7c7	pod
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Univerzita	univerzita	k1gFnSc1	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
přejmenováním	přejmenování	k1gNnSc7	přejmenování
někdejší	někdejší	k2eAgFnSc2d1	někdejší
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
nová	nový	k2eAgFnSc1d1	nová
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
osm	osm	k4xCc1	osm
fakult	fakulta	k1gFnPc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Purkyňovo	Purkyňův	k2eAgNnSc1d1	Purkyňovo
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
také	také	k9	také
Česká	český	k2eAgFnSc1d1	Česká
lékařská	lékařský	k2eAgFnSc1d1	lékařská
společnost	společnost	k1gFnSc1	společnost
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc2	Purkyně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
J.	J.	kA	J.
E.	E.	kA	E.
Purkyně	Purkyně	k1gFnSc2	Purkyně
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
kráter	kráter	k1gInSc1	kráter
Purkyně	Purkyně	k1gFnSc2	Purkyně
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
planetka	planetka	k1gFnSc1	planetka
3701	[number]	k4	3701
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
.	.	kIx.	.
v	v	k7c6	v
Lesnickém	lesnický	k2eAgInSc6d1	lesnický
Slavíně	Slavín	k1gInSc6	Slavín
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
památník	památník	k1gInSc1	památník
s	s	k7c7	s
žulovou	žulový	k2eAgFnSc7d1	Žulová
deskou	deska	k1gFnSc7	deska
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
Purkyňovo	Purkyňův	k2eAgNnSc1d1	Purkyňovo
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
data	datum	k1gNnSc2	datum
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc2	úmrtí
a	a	k8xC	a
rok	rok	k1gInSc1	rok
zbudování	zbudování	k1gNnSc2	zbudování
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
