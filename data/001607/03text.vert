<s>
Edward	Edward	k1gMnSc1	Edward
Mills	Millsa	k1gFnPc2	Millsa
Purcell	Purcell	k1gMnSc1	Purcell
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1912	[number]	k4	1912
Taylorville	Taylorville	k1gNnSc2	Taylorville
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1997	[number]	k4	1997
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
nezávislý	závislý	k2eNgInSc1d1	nezávislý
výzkum	výzkum	k1gInSc1	výzkum
nukleární	nukleární	k2eAgFnSc2d1	nukleární
magnetické	magnetický	k2eAgFnSc2d1	magnetická
rezonance	rezonance	k1gFnSc2	rezonance
v	v	k7c6	v
kapalinách	kapalina	k1gFnPc6	kapalina
a	a	k8xC	a
pevných	pevný	k2eAgFnPc6d1	pevná
látkách	látka	k1gFnPc6	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nukleární	nukleární	k2eAgFnSc1d1	nukleární
magnetická	magnetický	k2eAgFnSc1d1	magnetická
rezonance	rezonance	k1gFnSc1	rezonance
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
široce	široko	k6eAd1	široko
využívanou	využívaný	k2eAgFnSc7d1	využívaná
metodou	metoda	k1gFnSc7	metoda
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
struktury	struktura	k1gFnSc2	struktura
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Edward	Edward	k1gMnSc1	Edward
Mills	Millsa	k1gFnPc2	Millsa
Purcell	Purcella	k1gFnPc2	Purcella
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Edward	Edward	k1gMnSc1	Edward
Mills	Millsa	k1gFnPc2	Millsa
Purcell	Purcella	k1gFnPc2	Purcella
na	na	k7c4	na
nobel-winners	nobelinners	k1gInSc4	nobel-winners
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
