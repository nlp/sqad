<p>
<s>
Diopsid	Diopsid	k1gInSc1	Diopsid
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
horninotvorný	horninotvorný	k2eAgInSc1d1	horninotvorný
minerál	minerál	k1gInSc1	minerál
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
monoklinických	monoklinický	k2eAgInPc2d1	monoklinický
pyroxenů	pyroxen	k1gInPc2	pyroxen
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
formaci	formace	k1gFnSc6	formace
s	s	k7c7	s
hedenbergitem	hedenbergit	k1gInSc7	hedenbergit
a	a	k8xC	a
augitem	augit	k1gInSc7	augit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nS	různit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zastoupení	zastoupení	k1gNnPc2	zastoupení
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
hořčíku	hořčík	k1gInSc2	hořčík
nebo	nebo	k8xC	nebo
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
od	od	k7c2	od
bezbarvé	bezbarvý	k2eAgFnSc2d1	bezbarvá
variety	varieta	k1gFnSc2	varieta
až	až	k9	až
po	po	k7c6	po
krásně	krásně	k6eAd1	krásně
sytě	sytě	k6eAd1	sytě
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
pocházející	pocházející	k2eAgNnSc1d1	pocházející
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
slov	slovo	k1gNnPc2	slovo
dis	dis	k1gNnSc2	dis
a	a	k8xC	a
ò	ò	k?	ò
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
doslovně	doslovně	k6eAd1	doslovně
znamená	znamenat	k5eAaImIp3nS	znamenat
dvě	dva	k4xCgFnPc4	dva
tváře	tvář	k1gFnPc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
schopnost	schopnost	k1gFnSc4	schopnost
orientace	orientace	k1gFnSc2	orientace
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
rovinách	rovina	k1gFnPc6	rovina
{	{	kIx(	{
<g/>
100	[number]	k4	100
<g/>
}	}	kIx)	}
a	a	k8xC	a
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Diopsid	Diopsid	k1gInSc1	Diopsid
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
objeven	objevit	k5eAaPmNgInS	objevit
a	a	k8xC	a
popsán	popsat	k5eAaPmNgInS	popsat
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
brazilským	brazilský	k2eAgMnSc7d1	brazilský
státníkem	státník	k1gMnSc7	státník
a	a	k8xC	a
přírodovědcem	přírodovědec	k1gMnSc7	přírodovědec
Jose	Jose	k1gNnSc2	Jose
Bonifacio	Bonifacio	k1gMnSc1	Bonifacio
de	de	k?	de
Andrada	Andrada	k1gFnSc1	Andrada
e	e	k0	e
Silvou	Silva	k1gFnSc7	Silva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Diopsid	Diopsid	k1gInSc1	Diopsid
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
metamorfního	metamorfní	k2eAgInSc2d1	metamorfní
původu	původ	k1gInSc2	původ
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
bohatých	bohatý	k2eAgMnPc2d1	bohatý
na	na	k7c4	na
vápník	vápník	k1gInSc4	vápník
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
skarny	skarn	k1gInPc1	skarn
<g/>
,	,	kIx,	,
pyroxenické	pyroxenický	k2eAgFnPc1d1	pyroxenický
ruly	rula	k1gFnPc1	rula
<g/>
,	,	kIx,	,
mramory	mramor	k1gInPc1	mramor
nebo	nebo	k8xC	nebo
v	v	k7c6	v
bazických	bazický	k2eAgFnPc6d1	bazická
<g/>
,	,	kIx,	,
ultramafických	ultramafický	k2eAgFnPc6d1	ultramafický
<g/>
,	,	kIx,	,
magmatických	magmatický	k2eAgFnPc6d1	magmatická
horninách	hornina	k1gFnPc6	hornina
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kimberlit	kimberlit	k5eAaImF	kimberlit
nebo	nebo	k8xC	nebo
peridotit	peridotit	k5eAaPmF	peridotit
většinou	většinou	k6eAd1	většinou
chudé	chudý	k2eAgNnSc1d1	chudé
na	na	k7c6	na
SiO	SiO	k1gFnSc6	SiO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
5,5	[number]	k4	5,5
až	až	k9	až
6,5	[number]	k4	6,5
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
3,27	[number]	k4	3,27
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
dobrá	dobrá	k1gFnSc1	dobrá
dle	dle	k7c2	dle
{	{	kIx(	{
<g/>
110	[number]	k4	110
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
je	být	k5eAaImIp3nS	být
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
až	až	k6eAd1	až
nerovný	rovný	k2eNgInSc1d1	nerovný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgInPc2	všecek
odstínů	odstín	k1gInPc2	odstín
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
diopsidy	diopsid	k1gInPc1	diopsid
bezbarvé	bezbarvý	k2eAgInPc1d1	bezbarvý
<g/>
,	,	kIx,	,
mléčně	mléčně	k6eAd1	mléčně
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
šedé	šedý	k2eAgInPc1d1	šedý
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
violanu	violan	k1gInSc2	violan
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
dokonce	dokonce	k9	dokonce
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
má	mít	k5eAaImIp3nS	mít
skelný	skelný	k2eAgMnSc1d1	skelný
<g/>
,	,	kIx,	,
průhlednost	průhlednost	k1gFnSc1	průhlednost
<g/>
:	:	kIx,	:
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
až	až	k8xS	až
opakní	opakní	k2eAgInSc1d1	opakní
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Ca	ca	kA	ca
18,51	[number]	k4	18,51
%	%	kIx~	%
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
11,22	[number]	k4	11,22
%	%	kIx~	%
<g/>
,	,	kIx,	,
Si	se	k3xPyFc3	se
25,94	[number]	k4	25,94
%	%	kIx~	%
<g/>
,	,	kIx,	,
O	o	k7c6	o
44,33	[number]	k4	44,33
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
příměsi	příměs	k1gFnPc4	příměs
Fe	Fe	k1gFnSc2	Fe
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
Cr	cr	k0	cr
<g/>
,	,	kIx,	,
Mn	Mn	k1gMnSc1	Mn
<g/>
,	,	kIx,	,
Zn	zn	kA	zn
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
Na	na	k7c4	na
nebo	nebo	k8xC	nebo
K.	K.	kA	K.
Je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Složení	složení	k1gNnSc1	složení
diopsidu	diopsid	k1gInSc2	diopsid
(	(	kIx(	(
<g/>
a	a	k8xC	a
jemu	on	k3xPp3gMnSc3	on
chemicky	chemicky	k6eAd1	chemicky
podobným	podobný	k2eAgInPc3d1	podobný
minerálům	minerál	k1gInPc3	minerál
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
indikátorem	indikátor	k1gInSc7	indikátor
PT	PT	kA	PT
podmínek	podmínka	k1gFnPc2	podmínka
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
také	také	k9	také
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
mateřské	mateřský	k2eAgFnSc2d1	mateřská
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
chróm	chróm	k1gInSc4	chróm
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
chromdiopsid	chromdiopsid	k1gInSc1	chromdiopsid
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
využívaná	využívaný	k2eAgFnSc1d1	využívaná
ve	v	k7c6	v
šperkařském	šperkařský	k2eAgInSc6d1	šperkařský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
zkoumaným	zkoumaný	k2eAgInSc7d1	zkoumaný
produktem	produkt	k1gInSc7	produkt
jsou	být	k5eAaImIp3nP	být
sklokeramické	sklokeramický	k2eAgInPc1d1	sklokeramický
materiály	materiál	k1gInPc1	materiál
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
diopsidu	diopsid	k1gInSc2	diopsid
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
"	"	kIx"	"
<g/>
silceram	silceram	k1gInSc1	silceram
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
potenciální	potenciální	k2eAgMnSc1d1	potenciální
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
technologických	technologický	k2eAgFnPc6d1	technologická
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Chromdiopsid	Chromdiopsid	k1gInSc4	Chromdiopsid
===	===	k?	===
</s>
</p>
<p>
<s>
Chromdiopsid	Chromdiopsid	k1gInSc1	Chromdiopsid
je	být	k5eAaImIp3nS	být
nádherná	nádherný	k2eAgFnSc1d1	nádherná
smaragdově	smaragdově	k6eAd1	smaragdově
zelená	zelený	k2eAgFnSc1d1	zelená
odrůda	odrůda	k1gFnSc1	odrůda
diopsidu	diopsid	k1gInSc2	diopsid
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
na	na	k7c6	na
metamorfních	metamorfní	k2eAgNnPc6d1	metamorfní
ložiskách	ložisko	k1gNnPc6	ložisko
bohatých	bohatý	k2eAgMnPc2d1	bohatý
na	na	k7c4	na
vápník	vápník	k1gInSc4	vápník
a	a	k8xC	a
chróm	chróm	k1gInSc4	chróm
(	(	kIx(	(
právě	právě	k9	právě
chróm	chróm	k1gInSc1	chróm
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
typické	typický	k2eAgNnSc4d1	typické
zelené	zelené	k1gNnSc4	zelené
zabarvení	zabarvení	k1gNnSc2	zabarvení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
naleziště	naleziště	k1gNnSc4	naleziště
drahokamového	drahokamový	k2eAgInSc2d1	drahokamový
chromdiopsidu	chromdiopsid	k1gInSc2	chromdiopsid
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
drahokam	drahokam	k1gInSc4	drahokam
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
překrásné	překrásný	k2eAgFnSc3d1	překrásná
barvě	barva	k1gFnSc3	barva
jeho	jeho	k3xOp3gFnSc1	jeho
oblíbenost	oblíbenost	k1gFnSc1	oblíbenost
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
stále	stále	k6eAd1	stále
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
kvůli	kvůli	k7c3	kvůli
nižší	nízký	k2eAgFnSc3d2	nižší
tvrdosti	tvrdost	k1gFnSc3	tvrdost
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
osazován	osazovat	k5eAaImNgInS	osazovat
do	do	k7c2	do
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
denní	denní	k2eAgNnSc4d1	denní
nošení	nošení	k1gNnSc4	nošení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fassait	Fassait	k2eAgMnSc1d1	Fassait
===	===	k?	===
</s>
</p>
<p>
<s>
Fassait	Fassait	k1gInSc1	Fassait
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
diopsidu	diopsid	k1gInSc2	diopsid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
navíc	navíc	k6eAd1	navíc
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
augitu	augit	k1gInSc6	augit
a	a	k8xC	a
tak	tak	k6eAd1	tak
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
zařazován	zařazován	k2eAgInSc1d1	zařazován
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
odrůda	odrůda	k1gFnSc1	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
lokality	lokalita	k1gFnSc2	lokalita
Fassa	Fass	k1gMnSc2	Fass
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vysokoteplotní	vysokoteplotní	k2eAgInSc4d1	vysokoteplotní
minerál	minerál	k1gInSc4	minerál
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
na	na	k7c6	na
kontaktech	kontakt	k1gInPc6	kontakt
vulkanických	vulkanický	k2eAgFnPc2d1	vulkanická
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
mramorů	mramor	k1gInPc2	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
temně	temně	k6eAd1	temně
zelená	zelený	k2eAgFnSc1d1	zelená
až	až	k8xS	až
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Violan	Violan	k1gInSc4	Violan
===	===	k?	===
</s>
</p>
<p>
<s>
Violan	Violan	k1gInSc1	Violan
je	být	k5eAaImIp3nS	být
zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
a	a	k8xC	a
překrásnou	překrásný	k2eAgFnSc7d1	překrásná
odrůdou	odrůda	k1gFnSc7	odrůda
diopsidu	diopsid	k1gInSc2	diopsid
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
obsahu	obsah	k1gInSc3	obsah
manganu	mangan	k1gInSc2	mangan
má	mít	k5eAaImIp3nS	mít
fialovou	fialový	k2eAgFnSc4d1	fialová
až	až	k8xS	až
modrofialovou	modrofialový	k2eAgFnSc4d1	modrofialová
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
byl	být	k5eAaImAgMnS	být
také	také	k9	také
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
Augustem	August	k1gMnSc7	August
Breithauptem	Breithaupt	k1gInSc7	Breithaupt
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nepříliš	příliš	k6eNd1	příliš
častý	častý	k2eAgInSc4d1	častý
minerál	minerál	k1gInSc4	minerál
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
sbírkovou	sbírkový	k2eAgFnSc4d1	sbírková
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Inagli	Inagl	k1gMnPc1	Inagl
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
výskyt	výskyt	k1gInSc1	výskyt
drahokamových	drahokamový	k2eAgFnPc2d1	drahokamová
odrůd	odrůda	k1gFnPc2	odrůda
chromdiopsidu	chromdiopsid	k1gInSc2	chromdiopsid
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ala	ala	k1gFnSc1	ala
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
dokonale	dokonale	k6eAd1	dokonale
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
bezbarvé	bezbarvý	k2eAgInPc1d1	bezbarvý
krystaly	krystal	k1gInPc1	krystal
diopsidu	diopsid	k1gInSc2	diopsid
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
údolí	údolí	k1gNnSc1	údolí
Šigar	Šigara	k1gFnPc2	Šigara
<g/>
,	,	kIx,	,
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Karákóram	Karákóram	k1gInSc1	Karákóram
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
(	(	kIx(	(
<g/>
nádherné	nádherný	k2eAgNnSc1d1	nádherné
a	a	k8xC	a
dokonale	dokonale	k6eAd1	dokonale
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
estetické	estetický	k2eAgInPc1d1	estetický
krystaly	krystal	k1gInPc1	krystal
tmavě	tmavě	k6eAd1	tmavě
zelené	zelený	k2eAgFnSc2d1	zelená
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
diopsid	diopsida	k1gFnPc2	diopsida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
chromdiopsid	chromdiopsid	k1gInSc1	chromdiopsid
na	na	k7c6	na
webu	web	k1gInSc6	web
gemdat	gemdat	k5eAaPmF	gemdat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
diopsid	diopsid	k1gInSc1	diopsid
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
diopsid	diopsid	k1gInSc1	diopsid
na	na	k7c6	na
webu	web	k1gInSc6	web
webmineral	webminerat	k5eAaPmAgMnS	webminerat
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
violan	violan	k1gMnSc1	violan
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
