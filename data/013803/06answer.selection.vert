<s>
Zemský	zemský	k2eAgInSc1d1
plášť	plášť	k1gInSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
vrstev	vrstva	k1gFnPc2
Země	Země	k1gFnSc2
<g/>
,	,	kIx,
shora	shora	k6eAd1
vymezená	vymezený	k2eAgFnSc1d1
zemskou	zemský	k2eAgFnSc7d1
kůrou	kůra	k1gFnSc7
a	a	k8xC
zespodu	zespodu	k6eAd1
zemským	zemský	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
<g/>
.	.	kIx.
</s>